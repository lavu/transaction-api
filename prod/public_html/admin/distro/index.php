<?php

	//error_reporting(E_ALL);
	ini_set("display_errors",1);
	session_start();
	if(isset($_POST['old_redirect'])){
		$_SESSION['old_redirect'] = 'old_request';
	}
	if(!isset($_SESSION['old_redirect'])){
		header("Location: https://admin.poslavu.com/distro/beta/"); 
	}
	
	require_once("/home/poslavu/public_html/admin/cp/resources/core_functions.php");
	//require_once("/home/poslavu/public_html/admin/cp/resources/lavuquery.php");
	require_once("/home/poslavu/public_html/register/authnet_functions.php");
	$maindb = "poslavu_MAIN_db";
	
	if(isset($_GET['logout']) && $_GET['logout']==1)
	{
		add_to_distro_history("logged out","Logged Out",$_SESSION['posdistro_id'],$_SESSION['posdistro_loggedin']);
		$_SESSION['posdistro_loggedin'] = false;
		$_SESSION['posdistro_fullname'] = false;
		$_SESSION['posdistro_email'] = false;
		$_SESSION['posdistro_access'] = false;
		$_SESSION['posdistro_id'] = false;
	}
	
	if (isset($_GET['admin_login']) && strpos($_SESSION['posdistro_access'],"admin") !== false){
		$account_query = mlavu_query("select * from `poslavu_MAIN_db`.`resellers` where `username`='[1]'",$_GET['admin_login']);
		if(mysqli_num_rows($account_query))
		{
			$account_read = mysqli_fetch_assoc($account_query);
			$loggedin = $account_read['username'];
			$loggedin_fullname = trim($account_read['f_name'] . " " . $account_read['l_name']);
			$loggedin_email = $account_read['email'];
			$_SESSION['posdistro_loggedin'] = $loggedin;
			$_SESSION['posdistro_fullname'] = $loggedin_fullname;
			$_SESSION['posdistro_email'] = $loggedin_email;
		}
	}
	
	$loggedin = (isset($_SESSION['posdistro_loggedin']))?$_SESSION['posdistro_loggedin']:false;
	if($loggedin=="") $loggedin = false;
	$loggedin_fullname = (isset($_SESSION['posdistro_fullname']))?$_SESSION['posdistro_fullname']:false;
	if($loggedin_fullname=="") $loggedin_fullname = $loggedin;
	$loggedin_email = (isset($_SESSION['posdistro_email']))?$_SESSION['posdistro_email']:false;
	if($loggedin_email=="") $loggedin_email = $loggedin;
	$loggedin_access = (isset($_SESSION['posdistro_access']))?$_SESSION['posdistro_access']:false;
	if($loggedin_access=="") $loggedin_access = "";
	//Admin Login
	
	
	//if($_SERVER['SERVER_PORT']!=443) {
	//if (!isset($_SERVER['HTTPS']) || $_SERVER['HTTPS'] == "") {
	//	header("Location: https://admin.poslavu.com/distro/");
	//}


	function account_loggedin()
	{
		global $loggedin;
		return $loggedin;
	}
	
	function distro_admin_info($var)
	{
		$sessvar_name = 'posdistro_'.$var;
		if(isset($_SESSION[$sessvar_name]))
			return $_SESSION[$sessvar_name];
		else
			return "";
	}
	
	function add_to_distro_history($action,$details,$reseller_id,$reseller_name)
	{
		$vars = array();
		$vars['action'] = $action;
		$vars['details'] = $details;
		$vars['reseller_id'] = $reseller_id;
		$vars['reseller_name'] = $reseller_name;
		$vars['datetime'] = date("Y-m-d H:i:s");

		mlavu_query("insert into `poslavu_MAIN_db`.`reseller_history` (`action`,`details`,`reseller_id`,`reseller_name`,`datetime`) values ('[action]','[details]','[reseller_id]','[reseller_name]','[datetime]')",$vars);
	}
	
	function display_datetime($dt,$type="both")
	{
		$dt_parts = explode(" ",$dt);
		if(count($dt_parts) > 1)
		{
			$d_parts = explode("-",$dt_parts[0]);
			$t_parts = explode(":",$dt_parts[1]);
			if(count($d_parts) > 2 && count($t_parts) > 2)
			{
				$ts = mktime($t_parts[0],$t_parts[1],$t_parts[2],$d_parts[1],$d_parts[2],$d_parts[0]);
				if($type=="date")
					return date("m/d/y",$ts);
				else if($type=="time")
					return date("h:i a",$ts);
				else
					return date("m/d/y h:i a",$ts);
			}
			else return $dt;
		}
		else return $dt;
	}
	
	function get_reseller_billing_info($distro_code)
	{
		$cust_info = array();
		$cust_info['company_name'] = "";
		$cust_info['first_name'] = "";
		$cust_info['last_name'] = "";
		$cust_info['address'] = "";
		$cust_info['city'] = "";
		$cust_info['state'] = "";
		$cust_info['zip'] = "";
		$cust_info['phone'] = "";
		$cust_info['email'] = "";
		$cust_info['distro_id'] = "";
		
		$distro_query = mlavu_query("select * from `poslavu_MAIN_db`.`resellers` where `username`='[1]'",$distro_code);
		if(mysqli_num_rows($distro_query))
		{
			$signup_read = mysqli_fetch_assoc($distro_query);
			$cust_info['company_name'] = $signup_read['company'];
			$cust_info['first_name'] = $signup_read['f_name'];
			$cust_info['last_name'] = $signup_read['l_name'];
			$cust_info['address'] = $signup_read['address'];
			$cust_info['city'] = $signup_read['city'];
			$cust_info['state'] = $signup_read['state'];
			$cust_info['zip'] = $signup_read['postal_code'];
			$cust_info['phone'] = $signup_read['phone'];
			$cust_info['email'] = $signup_read['email'];
			$cust_info['distro_id'] = $signup_read['id'];
		}
		
		return $cust_info;
	}
	
	function buy_form($lpercent)
	{
		$loggedin = account_loggedin();
		$lpercent = str_replace("%","",$lpercent);
		
		$license_types = array();
		$license_types[] = array("Silver","895.00","895.00");
		$license_types[] = array("Gold","1495.00","1495.00");
		$license_types[] = array("Platinum","3495.00","3495.00");
		
		$license_select = array();
		$license_select[] = "";
		for($i=0; $i<count($license_types); $i++)
		{
			$lic_discount = ($license_types[$i][2] * 1) * ($lpercent * 1 / 100);
			$lic_amount = round($license_types[$i][2] * 1 - $lic_discount,2);
			//echo $license_types[$i][2] . " - " . $lic_discount . " - " . $lic_amount . "<br>";
			
			$license_types[$i][1] = $lic_amount;
			$license_select[] = $license_types[$i][0] . " - $" . number_format($license_types[$i][1] * 1,2);
		}
		if($loggedin=="zephyrhardware" || $loggedin=="martin")
		{
			$silver_promo_amount = 562.50;
			$license_types[] = array("Silver Promo",$silver_promo_amount,"625.00");
			$license_select[] = "Silver Promo - $" . number_format($silver_promo_amount,2);
		}
		
		$str = "";
		$fields = array();
		$fields[] = array("Credit Card Info","title");
		$fields[] = array("License","select","license_type",true,$license_select);
		$fields[] = array("Card Number","card_number","card_number",true);
		$fields[] = array("Card Expiration","card_expiration","card_expiration",true);
		$fields[] = array("CVV Code","card_code","card_code",true);
		
		$show_form = true;
		$pay_form = new CForm($fields,"index.php?mode=buy_license");
		if(isset($_POST['posted']))
		{
			$show_message = "";
			
			$card_info = array();
			$license_info = $_POST['license_type'];
			$license_info = explode("-",$license_info);
			$license_name = trim($license_info[0]);
			//echo $license_name;
			$license_cost = 0;
			$license_value = 0;
			for($i=0; $i<count($license_types); $i++)
			{
				if(strtolower($license_name)==strtolower($license_types[$i][0]))
				{
					$license_cost = $license_types[$i][1];
					$license_value = $license_types[$i][2];
				}
			}
			
			if($license_cost > 0 && $license_value > 0)
			{
				$collect_amount = $license_cost;
				$card_info['number'] = $_POST['card_number'];
				$card_info['expiration'] = $_POST['card_expiration'];
				$card_info['code'] = $_POST['card_code'];
				
				$str .= "Purchasing $license_name license for $collect_amount dollars with a value of $license_value<br><br>";
				
				$customer_info = get_reseller_billing_info($loggedin);
				
				if($card_info['number']=="8484" && $card_info['expiration']=="8484" && $card_info['code']=="8484")
				{
					$auth_result = array();
					$auth_result['success'] = true;
					$auth_result['response'] = "Test Mode";
				}else{
					$auth_result = authorize_card_for_transaction("charge","DS_" . $loggedin,"DS_" . $loggedin,$collect_amount,$card_info,$customer_info);
					//$auth_result['success'] = true;

				}
				
				if($auth_result['success'])
				{
					$show_message = "<font color='#008800'><b>Success!</b> <br>Your Payment of " . $collect_amount . " has been processed</font><br><br>";
					
					$vars = array();
					$vars['type'] = $license_name;
					$vars['resellerid'] = $customer_info['distro_id'];
					$vars['resellername'] = $loggedin;
					$vars['restaurantid'] = "";
					$vars['cost'] = $collect_amount;
					$vars['value'] = $license_value;
					$vars['purchased'] = date("Y-m-d H:i:s");
					$vars['applied'] = "";
					mlavu_query("insert into `poslavu_MAIN_db`.`reseller_licenses` (`type`,`resellerid`,`resellername`,`restaurantid`,`cost`,`value`,`purchased`,`applied`) values ('[type]','[resellerid]','[resellername]','[restaurantid]','[cost]','[value]','[purchased]','[applied]')",$vars);
					add_to_distro_history("bought license","type: $license_name\ncollected: $collect_amount\nvalue: $license_value",$vars['resellerid'],$vars['resellername']);
					//mlavu_query("update `poslavu_MAIN_db`.`payment_status` set `collection_taken`='1' where `restaurantid`='[1]'",$p_companyid);
					
					$show_form = false;
				}
				else
				{
					$show_message = "<font color='#880000'><b>* Error:</b> ".$auth_result['response']."</p></font>";
				}							
				if($show_message!="")
				{
					$str .= "<table width='100%' cellpadding=6><td width='100%' align='center' >";
					$str .= $show_message;
					if(!$show_form)
					{
						$str .= "<br><br><input type='button' value='Continue >>' onclick='window.location = \"index.php\"' />";
					}
					$str .= "</td></table>";
				}
			}
		}
		if($show_form)
			$str .= $pay_form->form_code();
		return $str;
	}
	function upgrade_form($lpercent, $licenseType, $licenseID, $companyName){
		
		if($_SERVER['REMOTE_ADDR']!="74.95.18.90")
		{
			exit();
		}
		
		$distro_code = distro_admin_info("loggedin");
		
		$loggedin = account_loggedin();
		$lpercent = str_replace("%","",$lpercent);
		$license_types = array();
		$query = mlavu_query("select `distro_license_com` from `poslavu_MAIN_db`.`resellers` where `username`='[1]'", $distro_code);
		if(mysqli_num_rows($query))
		{
			$commision=mysqli_fetch_assoc($query);
			$com= $commision['distro_license_com'];
		}
		else
		{
			echo "Distro $distro_code cannot be found";
			exit();
		}
		
		echo "LICID: $licenseID<br>";
		
		$silverToGold=(1-($lpercent/100))*600;

		$silverToPlatinum= (1-($lpercent/100))*2600;
		$goldToPlatinum= (1-($lpercent/100))*2000;
		
		if( $license_type==''){
			echo $companyName . "<br>";//$signup_query = mlavu_query();
		}
		
		if( $licenseType==''){
			$license_types[] = array("Silver to Gold",$silverToGold,$silverToGold);
			$license_types[] = array("Silver to Platinum",$silverToPlatinum,$silverToPlatinum);
		}
		else if($licenseType == 'Silver'){
			$license_types[] = array("Silver to Gold",$silverToGold,$silverToGold);
			$license_types[] = array("Silver to Platinum",$silverToPlatinum,$silverToPlatinum);
		}
		else if ($licenseType=='Gold'){
			$license_types[] = array("Gold to Platinum",$goldToPlatinum, $goldToPlatinum);
		}
		$license_select = array();
		$license_select[] = "";
		for($i=0; $i<count($license_types); $i++)
		{
			$lic_discount=0;
			//$lic_discount = ($license_types[$i][2] * 1) * ($lpercent * 1 / 100);
			$lic_amount = round($license_types[$i][2] * 1 - $lic_discount,2);
			$license_types[$i][1] = $lic_amount;
			$license_select[] = $license_types[$i][0] . " - $" . number_format($license_types[$i][1] * 1,2);
		}

		$str = "";
		$fields = array();
		$fields[] = array("Credit Card Info","title");
		$fields[] = array("License","select","license_type",true,$license_select);
		$fields[] = array("Card Number","card_number","card_number",true);
		$fields[] = array("Card Expiration","card_expiration","card_expiration",true);
		$fields[] = array("CVV Code","card_code","card_code",true);
		
		$get_applyto = (isset($_GET['applyto']))?$_GET['applyto']:"";
		$get_license_type = (isset($_GET['license_type']))?$_GET['license_type']:"";
		
		$show_form = true;
		$pay_form = new CForm($fields,"index.php?mode=upgrade_license&applyto=$get_applyto&license_type=$get_license_type&resturant_id=".$licenseID);
		if(isset($_POST['posted']))
		{
			$show_message = "";	
			$card_info = array();
			$license_info = $_POST['license_type'];
			$licenseID = $_GET['resturant_id'];
			$license_info = explode("-",$license_info);
			$license_name = trim($license_info[0]);
			
			$license_cost = 0;
			$license_value = 0;
			
			if( $license_name=='Gold to Platinum'){
				$license_cost=$goldToPlatinum;
				$license_value= 2000.00;
			}else if ($license_name=='Silver to Platinum'){
				$license_cost=$silverToPlatinum;
				$license_value= 2600.00;
			}else {
				$license_cost=$silverToGold;
				$license_value=600.00;
			}	
			if($license_cost > 0 && $license_value > 0)
			{
				$collect_amount = $license_cost;
				$card_info['number'] = $_POST['card_number'];
				$card_info['expiration'] = $_POST['card_expiration'];
				$card_info['code'] = $_POST['card_code'];
				
				$str .= "<center> Upgrading $license_name</center><br><br>";
				$customer_info = get_reseller_billing_info($loggedin);
				
				if($card_info['number']=="8484" && $card_info['expiration']=="8484" && $card_info['code']=="8484"){
					$auth_result = array();
					$auth_result['success'] = true;
					$auth_result['response'] = "Test Mode";
				}else{
					$auth_result = authorize_card_for_transaction("charge","DS_" . $loggedin,"DS_" . $loggedin,$collect_amount,$card_info,$customer_info);
				}
				
				if($auth_result['success']){
											
					$temp= explode('to ', $license_name);
					$temp= explode(' ', $temp[1]);
					$new_license_name= $temp[0];

					//echo "update `reseller_licenses` set type='".$license_name."', cost= '".$collect_amount."', value='".$license_value."', purchased='".date("Y-m-d H:i:s")."' where id='".$licenseID."' ";
					
					$show_message = "<font color='#008800'><b>Success!</b> <br>Your Payment of $" . $collect_amount . ".00 has been processed</font><br><br>";
					
					$licenseQuery = mlavu_query("select * from `poslavu_MAIN_db`.`reseller_licenses` where `id`='[1]'",$licenseID);
					if(mysqli_num_rows($licenseQuery))
					{
						$licRead = mysqli_fetch_assoc($licenseQuery);
						if($licRead['restaurantid']!='')
						{
							$vars = array();
							$vars['type'] = $license_name;
							$vars['resellerid'] = $customer_info['distro_id'];
							$vars['resellername'] = $loggedin;
							$vars['restaurantid'] = $licRead['restaurantid'];
							$vars['licenseid'] = $licenseID;
							$vars['cost'] = $collect_amount;
							$vars['value'] = $license_value;
							$vars['purchased'] = date("Y-m-d H:i:s");
							$vars['applied'] = date("Y-m-d H:i:s");
						
							$packageType=1;
							if($new_license_name=='Gold')
								$packageType=2;
							else if($new_license_name=='Platinum')
								$packageType=3;
								
							$tquery = "insert into `poslavu_MAIN_db`.`reseller_licenses` (`type`,`resellerid`,`resellername`,`restaurantid`,`cost`,`value`,`purchased`,`applied`) values ('[type]','[resellerid]','[resellername]','[restaurantid]','[cost]','[value]','[purchased]','[applied]')";
							/*foreach($vars as $tkey => $tval)
								$tquery = str_replace("[".$tkey."]",$tval,$tquery);
							echo $tquery . "<br>";
							exit();*/
		
							mlavu_query($tquery, $vars);
							//echo "select * from `poslavu_MAIN_db`.`reseller_licenses` where id='".$licenseID."'";
							//echo "select * from `poslavu_MAIN_db`.`reseller_licenses` where id='".$licenseID."'";
								
							mlavu_query("update `poslavu_MAIN_db`.`signups` set `package`='[1]' where `restaurantid`='[2]'",$packageType,$licRead['restaurantid']);
							mlavu_query("update `poslavu_MAIN_db`.`payment_status` set `license_type`='[1]' where `restaurantid`='[2]'",$new_license_name,$licRead['restaurantid']);
							
							add_to_distro_history("Upgraded license","type: $license_name\ncollected: $collect_amount\nvalue: $license_value",$vars['resellerid'],$vars['resellername']);
							$show_form = false;
						}
						else
						{
							$show_message = "<font color='#880000'><b>* Error:</b> Restaurant not specified in original license</p></font>";
						}
					}
					else
					{
						$show_message = "<font color='#880000'><b>* Error:</b> Original license can not be found</p></font>";
					}
				}
				else{
					$show_message = "<font color='#880000'><b>* Error:</b> ".$auth_result['response']."</p></font>";
				}							
				if($show_message!=""){
					$str .= "<table width='100%' cellpadding=6><td width='100%' align='center'>";
					$str .= $show_message;
					if(!$show_form){
						$str .= "<br><br><input type='button' value='Continue >>' onclick='window.location = \"index.php\"' />";
					}
					$str .= "</td></table>";
				}
			}
		}
		//else
			//echo "no posting...";
		
		if($show_form)
			$str .= $pay_form->form_code();
		return $str;
	}
	
	function create_account_area($mode)
	{
		global $loggedin;
		
		if ($mode=="create_account")
		{
			echo "<a href='index.php'>Back to Main Menu</a><br><br>";
			
			function confirm_restaurant_shard($data_name)
			{
				$str = "";
				$letter = substr($data_name,0,1);
				if(ord($letter) >= ord("a") && ord($letter) <= ord("z"))
					$letter = $letter;
				else
					$letter = "OTHER";
				$tablename = "rest_" . $letter;
				
				$mainrest_query = mlavu_query("select * from `poslavu_MAINREST_db`.`$tablename` where `data_name`='[1]'",$data_name);
				if(mysqli_num_rows($mainrest_query))
				{
					$str .= $letter . ": " . $data_name . " exists";
				}
				else
				{
					$str .= $letter . ": " . $data_name . " inserting";
					
					$rest_query = mlavu_query("select `data_access`,`id`,`last_activity`,`jkey`,`data_name` from `poslavu_MAIN_db`.`restaurants` where `data_name`='[1]'",$data_name);
					if(mysqli_num_rows($rest_query))
					{
						$rest_read = mysqli_fetch_assoc($rest_query);
						
						$success = mlavu_query("insert into `poslavu_MAINREST_db`.`$tablename` (`data_access`,`last_activity`,`jkey`,`data_name`,`companyid`) values ('[data_access]','[last_activity]','[jkey]','[data_name]','[id]')",$rest_read);
						if($success) $str .= " <font color='green'>success</font>";
						else $str .= " <font color='red'>failed</font>";
					}
				}
				return $str;
			}
			
			ini_set("dislay_errors","1");
			if(isset($_POST['company']) && isset($_POST['email']))
			{
				$company = $_POST['company'];
				$email = $_POST['email'];
				$phone = $_POST['phone'];
				$firstname = $_POST['firstname'];
				$lastname = $_POST['lastname'];
				$address = $_POST['address'];
				$city = $_POST['city'];
				$state = $_POST['state'];
				$zip = $_POST['zip'];
				$default_menu = $_POST['default_menu'];
				
				//require_once("/home/poslavu/public_html/admin/cp/resources/core_functions.php");
				$maindb = "poslavu_MAIN_db";
				require_once("/home/poslavu/public_html/register/create_account.php");
				$result = create_new_poslavu_account($company,$email,$phone,$firstname,$lastname,$address,$city,$state,$zip,$default_menu,"");
				$success = $result[0];
				if($success)
				{
					$username = $result[1];
					$password = $result[2];
					$dataname = $result[3];
					
					echo "<br>username: $username";
					echo "<br>password: $password";
					echo "<br>dataname: $dataname";
					
					$credvars['username'] = $username;
					$credvars['dataname'] = $dataname;
					$credvars['password'] = $password;
					$credvars['company'] = $company;
					$credvars['email'] = $email;
					$credvars['phone'] = $phone;
					$credvars['firstname'] = $firstname;
					$credvars['lastname'] = $lastname;
					$credvars['address'] = $address;
					$credvars['city'] = $city;
					$credvars['state'] = $state;
					$credvars['zip'] = $zip;
					$credvars['default_menu'] = $default_menu;
					
					mlavu_query("insert into `poslavu_MAIN_db`.`signups` (`username`,`dataname`,`password`,`company`,`email`,`phone`,`firstname`,`lastname`,`address`,`city`,`state`,`zip`,`status`,`default_menu`) values ('[username]','[dataname]',AES_ENCRYPT('[password]','password'),'[company]','[email]','[phone]','[firstname]','[lastname]','[address]','[city]','[state]','[zip]','manual','[default_menu]')",$credvars);
					confirm_restaurant_shard($dataname);
					
					mlavu_query("update `poslavu_MAIN_db`.`restaurants` set `distro_code`='[1]' where `data_name`='[2]' limit 1",account_loggedin(),$dataname);
					
					$new_rdb = "poslavu_".$dataname."_db";
					mlavu_query("update `[1]`.`users` set `active`='1'",$new_rdb);
					
					$history_str = "";
					foreach($credvars as $key => $val) $history_str .= $key . ": " . $val . "\n";
					add_to_distro_history("created new account",$history_str,distro_admin_info('id'),distro_admin_info('loggedin'));
					
					$set_rest_id = 0;
					$rest_query = mlavu_query("select * from `poslavu_MAIN_db`.`restaurants` where `data_name`='[1]'",$dataname);
					if(mysqli_num_rows($rest_query))
					{
						$rest_read = mysqli_fetch_assoc($rest_query);
						$set_rest_id = $rest_read['id'];
						
						$stat_query = mlavu_query("select * from `poslavu_MAIN_db`.`payment_status` where `dataname`='[1]'",$dataname);
						if(mysqli_num_rows($stat_query))
						{
							$stat_read = mysqli_fetch_assoc($stat_query);
							$status_id = $stat_read['id'];
						}
						else
						{
							mlavu_query("insert into `poslavu_MAIN_db`.`payment_status` (`restaurantid`,`dataname`) values ('[1]','[2]')",$rest_read['id'],$dataname);
							$status_id = mlavu_insert_id();
						}
						
						$trial_length = 14;
						$trial_start = date("Y-m-d H:i:s");
						$trial_end = date("Y-m-d H:i:s",mktime(0,0,0,date("m"),date("d") + $trial_length,date("Y")));
						mlavu_query("update `poslavu_MAIN_db`.`payment_status` set `trial_start`='[1]', `trial_end`='[2]' where `id`='[3]'",$trial_start,$trial_end,$status_id);
					}
					
					if($default_menu=="ersdemo" || $default_menu=="poslavu_ersdemo_db")
					{
						require_once("/home/poslavu/public_html/admin/sa_cp/tools/convert_product.php");
						update_account_product($set_rest_id,"lasertag");
					}
				}
				else echo "Failed to create account";
			}
			else
			{
				//../lib/create_demo_account.php?m=2' method='POST'>
				$creation_fields = array("company","email","phone","firstname","lastname","address","city","state","zip");
				echo "<script language='javascript'>";
				echo "function submit_create_account() { ";
				for($n=0; $n<count($creation_fields); $n++)
				{
					$crfield = $creation_fields[$n];
					echo " if(document.create_account.".$crfield.".value=='') alert('Please fill out the entire form'); else";
				}
				echo " document.create_account.submit(); ";
				echo "} ";
				
				$extra_options = "";
				if($loggedin=="robw") $extra_options .= "<option value='ersdemo'>LaserTag</option>";
				
				echo "</script>";
				echo "<form action='index.php?mode=create_account' method='POST' name='create_account'>
					<table cellspacing='0' cellpadding='5'>
						<tr><td align='right'>Starter Menu:</td><td align='left'><select name='default_menu'>$extra_options
							<option value='default_coffee'>Coffee Shop</option>
							<option value='defaultbar'>Bar/Lounge</option>
							<option value='default_pizza_'>Pizza Shop</option>
							<option value='default_restau'>Restaurant</option>
							<option value='sushi_town'>Original Sushi</option>
						</select></td></tr>
						<tr><td align='right'>Company Name:</td><td align='left'><input type='text' name='company' size='30' /></td></tr>
						<tr><td align='right'>Email:</td><td align='left'><input type='text' name='email' size='30' /></td></tr>
						<tr><td align='right'>Phone:</td><td align='left'><input type='text' name='phone' size='30' /></td></tr>
						<tr><td align='right'>First Name:</td><td align='left'><input type='text' name='firstname' size='30' /></td></tr>
						<tr><td align='right'>Last Name:</td><td align='left'><input type='text' name='lastname' size='30' /></td></tr>
						<tr><td align='right'>Address:</td><td align='left'><input type='text' name='address' size='30' /></td></tr>
						<tr><td align='right'>City:</td><td align='left'><input type='text' name='city' size='30' /></td></tr>
						<tr><td align='right'>State:</td><td align='left'><input type='text' name='state' size='30' /></td></tr>
						<tr><td align='right'>Zip:</td><td align='left'><input type='text' name='zip' size='30' /></td></tr>
						<tr><td align='center' colspan='2'><input type='button' value='Create Account' onclick='submit_create_account()' /></td></tr>
					</table>
				</form>";
			}
		}
		else if ($mode=="account_created")
		{
			if ($_REQUEST['s'] == 1) {
				echo "New account successfully created...<br><br>Username: ".$_REQUEST['new_un']."<br>Password: ".$_REQUEST['new_pw'];
				mail($_SESSION['posadmin_email'], "New POSLavu Account: ".$_REQUEST['new_co'], "New account successfully created...\n\nUsername: ".$_REQUEST['new_un']."\nPassword: ".$_REQUEST['new_pw'], "From: info@poslavu.com");
			} else if ($_REQUEST['s'] == 1) {
				echo "An error occurred while trying to create new account...";
			}
			echo "<br><br><a href='index.php'>Back to Main Menu</a>";		
		}
	}
	
	if($loggedin) // logged in
	{
		require_once("/home/poslavu/public_html/admin/cp/areas/billing.php");
		$mode = (isset($_GET['mode']))?$_GET['mode']:"";
		
		echo "<head>";
		echo "<title>POSLavu Distributor Admin</title>";
		echo "<style>";
		echo "  body, table { font-family:Verdana,Arial; font-size:12px; } ";
		echo "  a:link, a:visited { text-decoration:none; color:#000088; } ";
		echo "  a:hover { text-decoration:none; color:#4444aa; } ";
		echo "  .datetime { color:#008800; } ";
		echo "</style>";
		echo "</head>";
		echo "<body>";
		
		if($mode=="create_account" || $mode=="account_created")
		{
			create_account_area($mode);
		}
		else if($mode=="autologin")
		{
			$loginto = (isset($_GET['loginto']))?$_GET['loginto']:"";
			echo "loginto=".$loginto.", loggedin=".$loggedin;
			$cust_query = mlavu_query("select * from `poslavu_MAIN_db`.`restaurants` where `data_name`='[1]' and `distro_code`='[2]'",$loginto,$loggedin);
			if(mysqli_num_rows($cust_query))
			{
				$cust_read = mysqli_fetch_assoc($cust_query);

				add_to_distro_history("autologin",$loginto . "\n" . $cust_read['company_name'],distro_admin_info('id'),distro_admin_info('loggedin'));
				
				$dataname = $cust_read['company_code'];
				$userid = 1001;
				$username = "poslavu_distro_".distro_admin_info("loggedin");
				$full_username = "POSLavu Distro " . distro_admin_info("loggedin");
				$email = "info@poslavu.com";
				$companyid = $cust_read['id'];
				admin_login($dataname, $userid, $username, $full_username, $email, $companyid, "", "", "4", "", "", "0", "0");
				
				echo "<script language='javascript'>";
				echo "window.location.replace('/cp/index.php'); ";
				echo "</script>";
			}
		}
		else
		{	
			$readHandle=fopen("./distroMessages/messages.txt", 'r');
			$contents = fread($readHandle, filesize("./distroMessages/messages.txt"));
			fclose($readHandle);
			
			$cont=explode("|", $contents);
			$contents=$cont[1];
			
			echo "<table width='100%' cellspacing=0 cellpadding=8 style='border:solid 1px #777777' bgcolor='#eeeeee'><tr><td width='90%' align='left'>";
			echo "Distributor Logged in: <b>" . $loggedin_fullname ."
			<div class='alert' style= 'position:relative; left:30%; width:60%'>Distributor Alert:</b>" .$contents. "</div>";
			echo "</td><td width='50%' align='right'><a href='index.php?logout=1'>(logout)</a>";
			echo "</td></tr></table>";
			echo "<br>";
			echo "<br>";
			
			$distro_query = mlavu_query("select * from `poslavu_MAIN_db`.`resellers` where `username`='[1]'",$loggedin);
			if(mysqli_num_rows($distro_query))
			{
				$distro_read = mysqli_fetch_assoc($distro_query);
				$distro_credits = $distro_read['credits'];
				$distro_license_com = $distro_read['distro_license_com'];
				if($distro_credits=="") $distro_credits = 0;
				if($distro_license_com=="") $distro_license_com = 0;
				//echo "mode is ".$mode;
				if($mode!="" && $mode!="menu")
				{
					
					echo "<a href='index.php'><< Back</a><br><br>";
				}	
				if($mode=="buy_license"){
				
					echo "<b>Buy New License</b>";
					echo "<br><br>";
					echo buy_form($distro_license_com);
					
				}else if( $mode=="upgrade_license"){
				
					$type= (isset($_GET['license_type']))?$_GET['license_type']:"";
					
					$custName= (isset($_GET['customerName']))?$_GET['customerName']:"";
					$restID= (isset($_GET['resturant_id']))?$_GET['resturant_id']:"";
					echo upgrade_form($distro_license_com, $type, $restID, $custName);
					
				}else if( $mode=="updateInfo"){
				
					updateInfo();
				
				}else if($mode=='updatePassword'){
					$uname= $_GET['username'];
					
					updatePassword($uname);
				
				}else if($mode=='doPassUpdate'){
			
					doPassUpdate();
						
				}else if( $mode=='uploadImage'){
					
					uploadImage();
				}else if($mode=='updateDistroMessage'){
				
					updateMessage();
				}else if($mode=="apply_license"){
					$applyto = (isset($_GET['applyto']))?$_GET['applyto']:"";
					$use_license_id = (isset($_GET['use_license_id']))?$_GET['use_license_id']:"";
					
					$rest_query = mlavu_query("select * from `poslavu_MAIN_db`.`restaurants` where `data_name`='[1]' and `distro_code`='[2]'",$applyto,$loggedin);
					if(mysqli_num_rows($rest_query))
					{
						$rest_read = mysqli_fetch_assoc($rest_query);
						
						$license_applied = "";
						$license_resellerid = "";
						$status_query = mlavu_query("select * from `poslavu_MAIN_db`.`payment_status` where `restaurantid`='[1]'",$rest_read['id']);
						if(mysqli_num_rows($status_query))
						{
							$status_read = mysqli_fetch_assoc($status_query);
							$license_applied = $status_read['license_applied'];
							$license_resellerid = $status_read['license_resellerid'];
							$status_id = $status_read['id'];
						}
						else
						{
							mlavu_query("insert into `poslavu_MAIN_db`.`payment_status` (`restaurantid`,`dataname`) values ('[1]','[2]')",$rest_read['id'],$rest_read['data_name']);
							$status_id = mlavu_insert_id();
						}
						
						if($license_applied=="")
						{
							if($use_license_id!="")
							{
								$lic_query = mlavu_query("select * from `poslavu_MAIN_db`.`reseller_licenses` where `resellerid`='[1]' and `restaurantid`='' and `id`='[2]'",$distro_read['id'],$use_license_id);
								if(mysqli_num_rows($lic_query))
								{
									$lic_read = mysqli_fetch_assoc($lic_query);
									mlavu_query("update `poslavu_MAIN_db`.`reseller_licenses` set `restaurantid`='[1]', `applied`='[2]' where `id`='[3]'",$rest_read['id'],date("Y-m-d H:i:s"),$use_license_id);
									mlavu_query("update `poslavu_MAIN_db`.`payment_status` set `license_applied`='[1]', `license_type`='[2]', `license_resellerid`='[3]' where `id`='[4]'",$use_license_id,$lic_read['type'],$distro_read['id'],$status_id);
									
									add_to_distro_history("applied license","applyto: $applyto\ncompany_name: ".$rest_read['company_name']."\nlicense_id: $use_license_id\nlicense_type: ".$lic_read['type'],distro_admin_info('id'),distro_admin_info('loggedin'));
									
									echo "License Applied<br><br>";
									echo "<input type='button' value='Continue >>' onclick='window.location = \"index.php\"' />";
								}
							}
							else
							{
								echo "Apply License to: <b>" . $rest_read['company_name'] . "</b>";
								
								$lic_query = mlavu_query("select * from `poslavu_MAIN_db`.`reseller_licenses` where `resellerid`='[1]' and `restaurantid`=''",$distro_read['id']);
								echo "<input type='hidden' name='set_license_id' id='set_license_id' value=''>";
								echo "<table  cellspacing=0 cellpadding=4>";
								$counter=0;
								while($lic_read = mysqli_fetch_assoc($lic_query))
								{
									echo "<tr>";
									echo "<td><input type='radio' name='use_license' value='".$lic_read['id']."' onclick='document.getElementById(\"set_license_id\").value = this.value'></td>";
									echo "<td align='left'>" . $lic_read['type'] . "</td>";
									echo "<td>".display_datetime($lic_read['purchased'])."</td>";
									echo "</tr>";
									$counter++;
								}
								if( $counter==0){
									echo "<tr><td> You do not have any licenses to apply. <a href= 'index.php?mode=buy_license'> click here</a>  to purchase a license.</tr></td>";
									echo "</table>";
								}else{
									echo "</table>";
								
									echo "<input type='button' value='Apply >>' onclick='setlic = document.getElementById(\"set_license_id\").value; if(setlic==\"\") alert(\"Please choose the license you would like to apply\"); else if(confirm(\"Are you sure you want to apply this license?  Once applied you cannot remove it.\")) window.location = \"index.php?mode=apply_license&applyto=$applyto&use_license_id=\" + setlic'>";
								}	 
							}
						}
					}
				}
				else
				{
					
					if (strpos($loggedin_access,"admin") !== false){
						echo "<p><a href=\"accounts.php\">Manage Accounts</a></p>";
					}
					echo "<center><div style='margin:20px;text-decoration:underline;'><a href='./beta/index.php'>new Reseller Portal link</a></div>
					<table >
						<tr>
							<td valign='top'>
								<table cellspacing=0 cellpadding=4 style='border:solid 1px #888888'>
									<tr bgcolor='#eeeeee'>
										<td align='center'><b>Companies</b></td><td width='6'>&nbsp;
										</td>
										<td align='center'>Created</td>
										<td width='6'>&nbsp;</td>
										<td align='center'>Good Until</td>
										<td colspan='6'>&nbsp;</td>
									</tr>";
					$rest_query = mlavu_query("select * from `poslavu_MAIN_db`.`restaurants` where `distro_code`='[1]'",$loggedin);
					while($rest_read = mysqli_fetch_assoc($rest_query))
					{
						$resturant_ID="";
						$license_applied = "";
						$license_id='';
						$license_resellerid = "";
						$trial_end = "";
						$actual_trial_end = "";
						$status_query = mlavu_query("select * from `poslavu_MAIN_db`.`payment_status` where `restaurantid`='[1]'",$rest_read['id']);
						if(mysqli_num_rows($status_query))
						{
							
							$status_read = mysqli_fetch_assoc($status_query);
							//$resturant_ID= $status_read['id'];
							$license_applied = $status_read['license_applied'];
							$license_type = $status_read['license_type'];
							$license_resellerid = $status_read['license_resellerid'];
							$trial_end = $status_read['trial_end'];
							$actual_trial_end = $trial_end;
							
							if($license_applied!="")
								$trial_end = "";
						}
						
						if($trial_end=="")
							$good_until = "n/a";
						else
							$good_until = display_datetime((string)$trial_end,"date");
						
						echo "<tr>";
						echo "<td>" . $rest_read['company_name'] . "</td><td width='6'>&nbsp;</td>";
						echo "<td class='datetime'>" . display_datetime((string)$rest_read['created']) . "</td><td width='6'>&nbsp;</td>";
						echo "<td class='datetime'><b>" . $good_until . "</b></td><td width='6'>&nbsp;</td>";
						echo "<td><a href='index.php?mode=autologin&loginto=".$rest_read['data_name']."' target='_blank'>(login)</a></td><td width='6'>&nbsp;</td>";
						
						echo "<td>";
						if($actual_trial_end=="" && $license_applied=="") // not created by a distro
						{
							echo "<table cellspacing=0 cellpadding=0>
								<tr>
								<td style='cursor:pointer' 
									onclick='
										if(document.getElementById(\"alt_signup_".$rest_read['id']."\").style.display==\"block\") {
											document.getElementById(\"alt_signup_".$rest_read['id']."\").style.display = \"none\"; 
											document.getElementById(\"alt_signup_".$rest_read['id']."_upgrade\").style.display = \"none\"; 
											
										}else{ 
											document.getElementById(\"alt_signup_".$rest_read['id']."\").style.display = \"block\"
											document.getElementById(\"alt_signup_".$rest_read['id']."_upgrade\").style.display = \"block\"
										}'>
										
										Alternate Signup
								</td>
								<td>
									<div id='alt_signup_".$rest_read['id']."' style='display:none'>
										&nbsp;
										<a href='index.php?mode=apply_license&applyto=".$rest_read['data_name']."'>(apply license)</a>
									</div>
								</td>
								<td  >
									<div id='alt_signup_".$rest_read['id']."_upgrade' style='display:none'>";
									$licenseID=  mlavu_query("select * from `poslavu_MAIN_db`.`reseller_licenses` where restaurantid='".$rest_read['id']."'");
									$id = mysqli_fetch_assoc($licenseID);
									
									echo"<a href='index.php?mode=upgrade_license&applyto=".$rest_read['data_name']."&license_type=".$licenseType."&resturant_id=".$rest_read['id']."&customer_name=".$cust_info['company_name']." '>
									(upgrade license)</a>
									</div>
								</td>
							</tr></table>";
							
						}
						else if($license_applied!=""){
							if($license_type!='Platinum'){
								//echo "select * from `poslavu_MAIN_db`.`reseller_licenses` where restaurantid='".$rest_read['id']."'";
								$licenseID=  mlavu_query("select * from `poslavu_MAIN_db`.`reseller_licenses` where restaurantid='".$rest_read['id']."'");
								$id = mysqli_fetch_assoc($licenseID);
								echo "<table cellspacing=0 cellpadding=0><tr><td style='cursor:pointer' onclick='
								
								if(document.getElementById(\"upgrade_".$rest_read['id']."\").style.display==\"block\") 
									document.getElementById(\"upgrade_".$rest_read['id']."\").style.display = \"none\"; 
								else 
									document.getElementById(\"upgrade_".$rest_read['id']."\").style.display = \"block\"'>
									".$license_type."
								</td><td><div id='upgrade_".$rest_read['id']."' style='display:none'>&nbsp;
								
								<a href='index.php?mode=upgrade_license&applyto=".$rest_read['data_name']."&license_type=".$license_type."&resturant_id=".$id['id']."'>(upgrade license)</a></div></td></tr></table>";
							}
							else 
								echo $license_type;
						}
						else{
							echo "<input type='button' value='Apply License' style='font-size:10px' onclick='window.location = \"index.php?mode=apply_license&applyto=".$rest_read['data_name']."\"' />";
						}
						echo "</td>";
						
						echo "</tr>";
					}
					
					echo "<tr><td colspan='4' align='left'><input type='button' value='Create New Account' onclick='window.location = \"index.php?mode=create_account\"' /></td></tr>";
					
					echo "</table>";
					
					echo "</td> <td><table> <tr><td><td width='20'>&nbsp;</td><td valign='top'>";
					
					echo "<table cellspacing=0 cellpadding=4 style='border:solid 1px #888888'>";
					echo "<tr bgcolor='#eeeeee'><td align='center'><b>Licenses not Applied</b></td><td width='6'>&nbsp;</td><td align='center'>Purchased</td></tr>";
					$lic_query = mlavu_query("select * from `poslavu_MAIN_db`.`reseller_licenses` where `resellerid`='[1]' and `restaurantid`=''",$distro_read['id']);
					//echo "select * from `poslavu_MAIN_db`.`reseller_licenses` where `resellerid`='[1]' and `restaurantid`=''",$distro_read['id'];
					while($lic_read = mysqli_fetch_assoc($lic_query))
					{
						$license_type=$lic_read['type'];
						if( $lic_read['type']!='Platinum'){
							echo "<tr><td align='right'>";
								echo "<table cellspacing=0 cellpadding=0><tr><td align='right' style='cursor:pointer' onclick='
								if(document.getElementById(\"upgrade_".$lic_read['id']."\").style.display==\"block\") 
									document.getElementById(\"upgrade_".$lic_read['id']."\").style.display = \"none\"; 
								else 
									document.getElementById(\"upgrade_".$lic_read['id']."\").style.display = \"block\"'>
									".$license_type."
								</td><td><div id='upgrade_".$lic_read['id']."' style='display:none'>&nbsp;
								<a href='index.php?mode=upgrade_license&applyto=".$lic_read['data_name']."&license_type=".$lic_read['type']."&resturant_id=".$lic_read['id']."'>
								(upgrade license)</a></div></td></tr></table>";
								
							echo "</td><td width='6'>&nbsp;</td><td class='datetime'>".display_datetime($lic_read['purchased'])."</td></tr>";
					
						}else 
							echo "<tr><td align='right'>" . $lic_read['type'] . "</td><td width='6'>&nbsp;</td><td class='datetime'>".display_datetime($lic_read['purchased'])."</td></tr>";
					
					}
					echo "<tr><td colspan='3' align='left'></td></tr>";
					
					
					
					$resellerQuery= mlavu_query("select * from `poslavu_MAIN_db`.`resellers` where id='".$distro_read['id']."'");
					$resellerRead = mysqli_fetch_assoc($resellerQuery);
					$firstName=$resellerRead['f_name']; $lastName=$resellerRead['l_name']; 
					$company=$resellerRead['company'];$address=$resellerRead['address']; $city=$resellerRead['city']; $state=$resellerRead['state']; 
					$country=$resellerRead['country'];$postCode=$resellerRead['postal_code']; $phone=$resellerRead['phone'];
					$email=$resellerRead['email']; $info=$resellerRead['notes']; $website=$resellerRead['website']; $username=$resellerRead['username'];
					$licenseCommission=$resellerRead['distro_license_com'];$isCertified= $resellerRead['isCertified'];
					$isCertified ? $isCertified='Certified' : $isCertified='Not Certified';
					echo "	
					<table cellspacing=0 cellpadding=4 style='border:solid 1px #888888; layout:fixed; width:290px;'>
						<form action = 'index.php?mode=updateInfo' method='post'>
						<tr bgcolor='#eeeeee'>
							
							<td align='center' colspan=2  style='cursor:pointer' onclick='";
							$information= array('f_name', 'l_name','company','address','city','state','country','postal_code','phone','email','notes','website','updateButton','distro_username');
							foreach($information as $item){
								
								echo "if( document.getElementById(\"".$item."\").style.display==\"block\"){
										document.getElementById(\"".$item."\").style.display=\"none\";
									    document.getElementById(\"".$item."_text\").style.display=\"block\";
									  }else{ 
									  	document.getElementById(\"".$item."\").style.display=\"block\";
									  	document.getElementById(\"".$item."_text\").style.display=\"none\";
									  }";
							}
							echo " '/><b>Edit Info</b>
							</td>
						</tr>
						<tr>
							<td align='right'><b>First Name:</b></td>
							<td valign='top'><input type='text' id='f_name' name='f_name' value='".$firstName."' style='display:none'/><div id='f_name_text' style= 'display:block'> ".$firstName."</div> </td>
						</tr>
						<tr>
							<td align='right'><b>Last Name:</b></td>
							<td valign='top'><input type='text' id='l_name' name='l_name' value='".$lastName."' style='display:none'/> <div id='l_name_text' style= 'display:block'> ".$lastName."</div> </td>
						</tr>
						<tr>
							<td align='right'><b>Company:</b></td>
							<td valign='top'><input type='text'id='company' name='company' value='".$company."' style='display:none'/>  <div id='company_text' style= 'display:block'> ".$company."</div></td>
						</tr>
						<tr>
							<td align='right'><b>Address:</b></td>
							<td valign='top'><input type='text'id='address' name='address' value='".$address."' style='display:none'/> <div id='address_text' style= 'display:block'> ".$address."</div></td>
						</tr>
						<tr>
							<td align='right'><b>City:</b></td>
							<td valign='top'><input type='text'id='city' name='city' value='".$city."' style='display:none'/> <div id='city_text' style= 'display:block'> ".$city."</div></td>
						</tr>
						<tr>
							<td align='right'><b>State:</b></td>
							<td valign='top'><input type='text'id='state' name='state' value='".$state."' style='display:none'/> <div id='state_text' style= 'display:block'> ".$state."</div></td>
						</tr>
						<tr>
							<td align='right'><b>Country:</b></td>
							<td valign='top'><input type='text' id='country' name='country' value='".$country."'style='display:none' /> <div id='country_text' style= 'display:block'> ".$country."</div></td>
						</tr>
						<tr>
							<td align='right'><b>Postal Code:</b></td>
							<td valign='top'><input type='text' id='postal_code' name='postal_code' value='".$postCode."'style='display:none' /> <div id='postal_code_text' style= 'display:block'> ".$postCode."</div></td>
						</tr>
						<tr>
							<td align='right'><b>Phone:</b></td>
							<td valign='top'><input type='text' id='phone' name='phone' value='".$phone."' style='display:none'/>  <div id='phone_text' style= 'display:block'> ".$phone."</div></td>
						</tr>
						<tr>
							<td align='right'><b>Email:</b></td>
							<td valign='top'><input type='text' id='email' name='email' value='".$email."'style='display:none' />  <div id='email_text' style= 'display:block'> ".$email."</div></td>
						</tr>
						<tr>
							<td align='right' valign='top'><b>Additional Info:</b></td>
							<td valign='top'><textarea  name='notes' id='notes' rows='5' cols='40' style='display:none'>".$info."</textarea> <div id='notes_text' style= 'display:block'> ".$info."</div></td>
						</tr>

						<tr>
							<td align='right'><b>Website:</b></td>
							<td valign='top'><input type='text' id='website' name='website' value='".$website."' style='display:none'/><div id='website_text' style= 'display:block'> ".$website."</div></td>
						</tr>

						<tr>
							<td align='right'><b>Username:</b></td>
							<td valign='top'>$username</td>
						</tr>
						<tr>
							<td align='right'><b>Password:</b></td>
							<td valign='top'><a href='index.php?mode=updatePassword&username=$username'>Update Password</a></td>
						</tr>
						<tr>
							<td align='right'><b>License Commision<br> (percent):</b></td>
							<td valign='top' id='inputID' >".$licenseCommission."</td>
						</tr>
						<tr>
						<tr>
							<td align='right'><b>Certification:</b></td>
							<td valign='top' >".$isCertified."</td>
						</tr>
							<td align='right'><input type='hidden' value ='".$distro_read['id']."' name= 'distroID'> </td>
							<td><input type='submit' value='Update Reseller' id='updateButton' style= 'display:none'/></td>
						</tr>
					</form>
					</tr></td></table>
					";
					
					
					echo "</tr></td>";
					echo "</table>";
					echo "</tr><tr><td>";
					echo "<table cellspacing=0 cellpadding=4 style='border:solid 1px #888888; '>";
						echo "<tr bgcolor='#eeeeee'><td align='left'><b>Resources</b></td><td width='6'>&nbsp;</td><td align='center'>&nbsp;</td></tr>";
						echo "<tr><td colspan='2' align='left'>";
					echo "&nbsp;";
					    
					    $linkToPricing = 'http://www.poslavu.com/en/ipad-pos-signup';
					    $fullLLSInstallerLink = 'http://www.poslavu.com/assets/products/Lavu-Localserver-1.3beta-10.8-x86_64.dmg';
					    $LLSUpdaterLink = 'http://www.poslavu.com/assets/products/LLS-Uninstall.dmg';
					    
					    //echo "<li><a href='$linkToPricing'>IMPORTANT LLS PRICING INFO--Please have your clients understand \"Lavu Local Server: Pricing\" prior to install.</a> </li>";
						//echo "<li><a href='$fullLLSInstallerLink'>DOWNLOAD--Full Lavu Local Server Installer.</a> </li>";
						//echo "<li><a href='$LLSUpdaterLink'>DOWNLOAD--Lavu Local Server Updater.</a> </li>";
						echo "<li>Please contact support for access to LLS Files</li>";
						echo"</td>";
					echo "</table>";
					echo "<table cellspacing=0 cellpadding=4 style='border:solid 1px #888888; '>
						
							<tr bgcolor='#eeeeee'><td align='left'><b>Upload a logo</b></td><td width='6'>&nbsp;</td><td align='center'>&nbsp;</td></tr>
							<td valign='top'>
							<form action='index.php?mode=uploadImage' method='post' enctype='multipart/form-data'>
							<label for='file'>Filename:</label>
							<input type='file' name='file' id='file' /> 
							<input type='hidden' name='username' value= ".$username.">
							<br/>
							<input type='submit' name='submit' value='Submit' />
							</form>
						</tr></td>
						<tr><td>
						Current logo:";
						if( file_exists('../images/logos/fullsize/'.$username.'.png'))
							echo "<img src='../images/logos/".$username.".png'> ";
						else
							echo " No logo uploaded";
						
						echo "</tr></td>
						</table></td>
					";
					
					if( $username == 'martin'){	
						echo "<td rowspan=2> 
						<table cellspacing=0 cellpadding=4 style='border:solid 1px #888888; '>
						<form action='index.php?mode=updateDistroMessage' method='POST'>
						<tr bgcolor='#eeeeee'><td> Change Distro Message </tr></td>
						<tr><td> New Message: </td>
						<td> <textarea name='message' cols=30 rows=5>$contents</textarea></td>
						</tr><tr><td></td>
						<td><input type= submit></td></tr>
						</table> 
						";		
					}
					echo "</table>";
				}
			}
		}		
		echo "</body>";
	}
	else{
		$in_lavu = true;
		require_once("login.php");
	}
	function updatePassword($username){
		echo "<table><tr><td><b>Update Password</b></td></tr>";
		echo "<form action ='index.php?mode=doPassUpdate' method=post>";
		echo "<tr><td> Current Password </td><td> <input type= 'password' name='current'> </td></tr>";
		echo "<tr><td> New Password </td> <td> <input type='password' name='new1'></td></tr>";
		echo "<tr><td> Password Again </td> <td> <input type='password' name= 'new2'> <input type= hidden value='".$username."' name=username></td></tr>";
		echo "<tr><td> <input type= submit></tr></td>";
		echo "</form>";
		echo "</table> ";
	
		echo "<br><br><a href='index.php'>Back to Main Menu</a><br><br>";
		
	}
	function doPassUpdate(){
		//echo "username: ".$_POST['username'];
		if( $_POST['new2'] != $_POST['new1'] ){
		
			echo "<div style='color:red'>Your new passwords do not match please try again.  </div>";
			updatePassword($_POST['username']);
		
		}else{
		
			mlavu_query("update `poslavu_MAIN_db`.`resellers` set `password`=PASSWORD('[1]') where `username`='[2]' and `password`= PASSWORD('[3]')", $_POST['new1'], $_POST['username'], $_POST['current']);
			if(ConnectionHub::getConn('poslavu')->affectedRows()){
				echo "password successfully updated.";
			}else{ 
				echo "<div style='color:red'>current password is incorrect. please try again.</div>";
				updatePassword($_POST['username']);
			}		


		}
	}
	
	function updateInfo(){
		echo "<b><center>Info Updated</b>";
		echo "<br><br><a href='index.php'>Back to Main Menu</a><br><br>";
		mlavu_query("update `poslavu_MAIN_db`.`resellers` set `f_name`='".$_POST['f_name']."', l_name='".$_POST['l_name']."', company='".$_POST['company']."', address='".$_POST['address']."', 
		city='".$_POST['city']."',state= '".$_POST['state']."', country= '".$_POST['country']."', postal_code = '".$_POST['postal_code']."',phone= '".$_POST['phone']."', email='".$_POST['email']."',
		website= '".$_POST['website']."',notes='".$_POST['notes']."' where id='".$_POST['distroID']."'");
		
	}
	function updateMessage(){
		
		$myVals= $_POST['message'];
		echo "New Message:".$myVals;

		$writeHandle=fopen("./distroMessages/messages.txt", 'r+');
		
	
		fputs($writeHandle,"|".$myVals ) or die( "couldnt write file") ;

		fclose($writeHandle);
	}
	
	function uploadImage(){
		$username= $_POST['username'].'.png';
		$allowedExts = array("jpg", "jpeg", "gif", "png");
		$extension = end(explode(".", basename($_FILES["file"]["name"])));
		
		if ((($_FILES["file"]["type"] == "image/gif") || ($_FILES["file"]["type"] == "image/png") || 
		($_FILES["file"]["type"] == "image/jpeg") || ($_FILES["file"]["type"] == "image/jpg")) && 
		($_FILES["file"]["size"] < 2000000) && in_array($extension, $allowedExts)){
			
			if ($_FILES["file"]["error"] > 0){
		    	echo "Return Code: " . $_FILES["file"]["error"] . "<br />";
		    
		    }else{
		    	
		    	if (file_exists("../images/logos/fullsize/" . $username)){
		    		echo $username . "Logo already exists. Overwriting. ";
		    		move_uploaded_file($_FILES["file"]["tmp_name"],"../images/logos/fullsize/".$username);
		    		resizeAndSave($username);
		    		
		    	}else{
		    		move_uploaded_file($_FILES["file"]["tmp_name"],"../images/logos/fullsize/".$username);
		    		resizeAndSave($username);
		    		echo "Successfully uploaded your logo. Thank you!";
		    		
		    	}
		    }
		}
		else{
			echo "Invalid file Please try another logo.";
		}
 }
 
 function resizeAndSave($filename){
 	copy("../images/logos/fullsize/".$filename."" , "../images/logos/".$filename);
 	$inv_main_size= getimagesize("../images/logos/fullsize/".$filename);
 	
 	//$convertcmd = "convert '../images/logos/$filename' -thumbnail '120x120 >' '../images/logos/$filename'";
 	$convertcmd = "convert ".lavuShellEscapeArg("../images/logos/$filename")." -thumbnail '120x120 >'"." ".lavuShellEscapeArg("../images/logos/$filename");
    //exec($convertcmd);
    lavuExec($convertcmd);
 }
?>
