<?php

	$product_code = (isset($_REQUEST['p']))?$_REQUEST['p']:false;
	$encoded_dataname = (isset($_REQUEST['d']))?$_REQUEST['d']:false;
	$token = (isset($_REQUEST['t']))?$_REQUEST['t']:false;
	
	if ($product_code && $encoded_dataname && $token) {

		require_once(dirname(__FILE__)."/cp/resources/core_functions.php");
		
		mlavu_select_db("poslavu_MAIN_db");
		
		$dataname = lc_decode($encoded_dataname);
		lavu_connect_dn($dataname, "poslavu_".$dataname."_db");
		
		$check_token = lavu_query("SELECT * FROM `config` WHERE `type` = 'password_reset_token' AND `value2` = '[1]'", $token);
		if (mysqli_num_rows($check_token) > 0) {

			$token_info = mysqli_fetch_assoc($check_token);

			$user_id = $token_info['value'];
			$user_table = "users";
			$username_field = "username";
			$reset_type = "";
			if ($product_code == "lg") { // Lavu Give
				if ($token_info['setting'] == "customer") {
					$user_table = "med_customers";
					$username_field = "user_name";
					$reset_type = "customer";
				}
			}
			
			$check_user = lavu_query("SELECT `id`, `".$username_field."` FROM `".$user_table."` WHERE `id` = '[1]'", $user_id);
			if (mysqli_num_rows($check_user) > 0 ) {
				
				$user_info = mysqli_fetch_assoc($check_user);
				$username = $user_info[$username_field];
			
				?>
				
				<script type='text/javascript'>
					function handleResetRequest(result) {
						var pwra = document.getElementById("pwresetarea");
			  		if (result) {
			 				if (result.status == "success") pwra.innerHTML = "Reset Successful";
			 				else pwra.innerHTML= "Password Reset Failed: " + result.error;
			 			} else pwra.innerHTML = "No response... Please try again.";		
			 		}
				</script>
			 
				<?php
			
				session_start();
				$_SESSION['verified'] = 1;
				$reset_script_path = "lib/password/pwreset.php";
				require_once(dirname(__FILE__)."/lib/password/password_reset_form.php");	 	
			
			} else echo "Account Not Found";
			
		} else echo "Invalid Token";		

	} else header("Location: http://www.poslavu.com");
	
?>