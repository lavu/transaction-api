<?php
require_once __DIR__ . "/../cp/resources/core_functions.php";
/**
 * Created by PhpStorm.
 * User: Leif
 * Date: 3/15/17
 * Time: 3:58 PM
 */
class SessionAuthenticator
{
	/**
	 * Authenticate a user via session information stored in a cookie. Cookies must be enabled.
	 */
	public static function authenticateSession(){
		if(!empty($_COOKIE['PHPSESSID'])){
			session_start($_COOKIE['PHPSESSID']);
			if(sessvar_isset('admin_dataname') || sessvar_isset('admin_companyid')){
			    session_write_close();
				return; //Successful authentication.
			}
		}
		self::authFailed();//Unsuccessful authentication.
	}

	/**
	 * Return error message and exit the site upon failure to authenticate.
	 */
	private static function authFailed(){ //TODO: Ensure this complies with error code standards
        header('HTTP/1.0 401 Unauthorized');
		echo "error:1001 - Authentication Failed.<br>Please verify that you are logged in with valid credentials.";
		exit();
	}
}
