<?php
/**
 * Created by PhpStorm.
 * User: Leif
 * Date: 4/5/17
 * Time: 2:42 PM
 */

/*
* Do not LazyLoad the session authenticator. Authentication needs to happen
* first to prevent unauthorized access.
*/
require_once __DIR__ . "/../../error_config.php";
require_once __DIR__ . "/../SessionAuthenticator.php";
SessionAuthenticator::authenticateSession();

require_once __DIR__ . "/../../cp/resources/core_functions.php";
require_once __DIR__ . "/../../../third_party_frameworks/vendor/vendor/autoload.php"; //Needed for Silex, etc.

/**
 * Include all Inventory Controllers.
 * This includes everything in the InventoryObjectControllers directory
 */
foreach(glob(__DIR__."/../../components/Inventory/InventoryObjectControllers/*") as $index=>$path){
	require_once($path);
}

/**
 * Include all Inventory Request Formatters.
 * This includes everything in the RequestFormatters directory
 */
foreach(glob(__DIR__."/../../components/Inventory/RequestFormatters/*") as $index=>$path){
	require_once($path);
}


/**
 * Include all Inventory Request Compilers.
 * This includes everything in the RequestFormatters directory
 */
foreach(glob(__DIR__."/../../components/Inventory/RequestCompilations/*") as $index=>$path){
	require_once($path);
}

/**
 * Include all Inventory Router Classes.
 * This includes everything in the RouterClasses directory
 */
foreach(glob(__DIR__."/RouterClasses/*") as $index=>$path){
	require_once($path);
}

use Symfony\Component\HTTPFoundation\Request;
/*******************/
/*utility functions*/
/*******************/
//These define fields are here just to have easy-to-use defaults that can't be
//mistaken for values.
define("INVENTORY_API_LATEST_VERSION",  "1.0");
define("INVENTORY_API_ROUTE",           "/inventory/v".INVENTORY_API_LATEST_VERSION);
define("API_REQUIRED_FIELD",            'API_REQUIRED_FIELD');
define("API_OPTIONAL_FIELD",            "API_OPTIONAL_FIELD");
define("API_MISSING_OPTIONAL_FIELD",    "API_MISSING_OPTIONAL_FIELD");
define("PARAMETER_DELIMITER",           ";");
define("BAD_REQUEST",                   400);
define("FIFO",                          "fifo");
define("AVERAGE_COST_PERIODIC",         "average_cost_periodic");
define("DAWN_OF_TIME",                  "2011-01-01");
/**
 * @param Request $request
 * @return array|mixed
 */
function decodeRequest(Request $request)
{
	if(isset($request))
	{
		$request = json_decode(urldecode($request->getContent()), true);
	}
	return $request;
}

/**
 * Checks to see if the response is a success or failure and returns a value of
 * 400 if it's a failure and 200 if it's a success.
 */
function getReturnStatusValue($response){
	if($response === BAD_REQUEST){
		return BAD_REQUEST;
	}else if(json_decode(json_encode($response),true)['Status'] == "Success"){
		return 200;
	}else{
		return 400;
	}
}

/**
*	Splits the string request into an array, using the PARAMETER_DELIMITER as the
* string to split by.
*/
function createRequestFromURLParams($params, $fieldKey="id", $delimiter=PARAMETER_DELIMITER){
	$params = explode($delimiter, $params);


	if($params !== FALSE && count($params) > 0){
		$formattedParams = array();
		foreach($params as $key=>$val){
			$formattedParams[] = array($fieldKey=>$val);
		}
		return $formattedParams;
	}else{
		return null;
	}
}
