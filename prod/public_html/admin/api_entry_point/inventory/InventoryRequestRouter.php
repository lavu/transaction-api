<?php
/**
 * Created by PhpStorm.
 * User: Leif
 * Date: 3/13/17
 * Time: 3:26 PM
 */
require_once __DIR__ . "/RequestRouterFunctions.php";
$silexApp = new Silex\Application();
$silexApp->register(new Silex\Provider\ValidatorServiceProvider());

use Symfony\Component\HTTPFoundation\Request;
use Silex\Application;
$silexApp['debug'] = lavuDeveloperStatus(); //If dev status is true, turn on debugging.
new InventoryRequestRouter($silexApp);
$silexApp->run();
class InventoryRequestRouter{
	function __construct(Application $silexApp){
		$this->inventoryItemRoutes($silexApp);
		$this->costRoutes($silexApp);
		$this->purchaseOrderRoutes($silexApp);
		$this->vendorItemRoutes($silexApp);
		$this->categoryRoutes($silexApp);
		$this->vendorRoutes($silexApp);
		$this->storageLocationRoutes($silexApp);
		$this->unitRoutes($silexApp);
		$this->transferRoutes($silexApp);
		$this->reconciliationRoutes($silexApp);
		$this->enterpriseRoutes($silexApp);
		$this->dashboardRoutes($silexApp);
		$this->auditRoutes($silexApp);
		$this->count86Routes($silexApp);
		$this->wasteRoutes($silexApp);
		$this->menuItemRoutes($silexApp);
		$this->modifierRoutes($silexApp);
		$this->reportRoutes($silexApp);
		$this->locationRoutes($silexApp);
		$this->cacheRoutes($silexApp);
	}

	private function inventoryItemRoutes(Application $silexApp)
	{
		/**
		 * Create an Inventory Item, or multiple inventory items.
		 */
		$silexApp->POST(INVENTORY_API_ROUTE . "/InventoryItems",
			function (Request $request) use ($silexApp)
			{
				return (new InventoryItemRouter())->createInventoryItems($request, $silexApp);
			}
		);

		/**
		 * Waste a quantity of an Inventory Item, or quantities of multiple Inventory Items.
		 */
		$silexApp->PUT(INVENTORY_API_ROUTE . "/InventoryItems/waste",
			function (Request $request) use ($silexApp)
			{
				return (new InventoryItemRouter())->wasteInventoryItems($request, $silexApp);
			}
		);

		/**
		 * Copy an Inventory Item, or multiple Inventory Items. This command may also modify the copied Inventory Item if
		 * modifications are specified.
		 */
		$silexApp->PUT(INVENTORY_API_ROUTE . "/InventoryItems/copy",
			function (Request $request) use ($silexApp)
			{
				return (new InventoryItemRouter())->copyInventoryItems($request, $silexApp);
			}
		);

		/**Update an inventory item or multiple inventory items.*/
		$silexApp->PUT(INVENTORY_API_ROUTE . "/InventoryItems",
			function (Request $request) use ($silexApp)
			{
				return (new InventoryItemRouter())->updateInventoryItems($request, $silexApp);
			}
		);

		/**
		 * Archive an Inventory Item or multiple Inventory Items by id.
		 */
		$silexApp->DELETE(INVENTORY_API_ROUTE . "/InventoryItems/{ids}",
			function ($ids) use ($silexApp)
			{
				return (new InventoryItemRouter())->archiveInventoryItems($ids, $silexApp);
			}
		)->assert('ids', '(\d+' . PARAMETER_DELIMITER . '?)+');


		/**Receive an inventory item or multiple inventory items.*/
		$silexApp->PUT(INVENTORY_API_ROUTE . "/InventoryItems/receive",
			function (Request $request) use ($silexApp)
			{
				return (new InventoryItemRouter())->receiveWithoutPurchaseOrders($request, $silexApp);
			}
		);

		/**
		 * Insert a new Manage View Modal item
		 */
		$silexApp->POST(INVENTORY_API_ROUTE . "/ManageView",
			function (Request $request) use ($silexApp)
			{
				return (new InventoryItemRouter())->createManageViewItem($request, $silexApp);
			}
		);

		/**
		 * Update Manage view inventory items
		 */
		$silexApp->PUT(INVENTORY_API_ROUTE . "/ManageView",
			function (Request $request) use ($silexApp)
			{
				return (new InventoryItemRouter())->updateManageViewItem($request, $silexApp);
			}
		);

		/**
		 * Add to quantity on hand of inventory items
		 */
		$silexApp->PUT(INVENTORY_API_ROUTE . "/InventoryItems/add",
			function (Request $request) use ($silexApp)
			{
				return (new InventoryItemRouter())->updateInventoryItemQuantityOnHand($request, $silexApp);
			}
		);

		/**
		 * Get all data related to an inventory item or multiple inventory items relating to the id of the inventory items.
		 */
		$silexApp->GET(INVENTORY_API_ROUTE . "/InventoryItems/{ids}",
			function ($ids) use ($silexApp)
			{
				return (new InventoryItemRouter())->getInventoryItemsByID($ids, $silexApp);
			}
		)->assert('ids', '(\d+' . PARAMETER_DELIMITER . '?)+');

        /**
         * Get data related to legacy inventory items.
         */
        $silexApp->GET(INVENTORY_API_ROUTE . "/InventoryItems/LegacyItems",
            function() use ($silexApp){
                return (new InventoryItemRouter())->getLegacyItems($silexApp);
            }
        )->assert('ids', '(\d+' . PARAMETER_DELIMITER . '?)+');


        /*
         * Get all vendor items for the inventory item ids
         */
		$silexApp->GET(INVENTORY_API_ROUTE . "/InventoryItems/linked/{ids}",
            function($ids) use ($silexApp){
		        return (new InventoryItemRouter())->getLinkedItemsByID($ids, $silexApp);
        })->assert('ids', '(\d+' . PARAMETER_DELIMITER . '?)+');

		/**
		 * Get all data related to an inventory item or multiple inventory items relating to the external location with restaurantID {id}
		 */
		$silexApp->GET(INVENTORY_API_ROUTE . "/InventoryItems/external/{id}",
			function ($id) use ($silexApp)
			{
				return (new InventoryItemRouter())->getExternalInventoryItems($id, $silexApp);
			}
		)->assert('id', '\d+');

		/**
		 * Get all Manage view rows. This collects data from multiple tables.
		 * The tables are:
		 * Last Updated by Dhruvpal on 31-01-2019
		 */
		
		$silexApp->GET(INVENTORY_API_ROUTE . "/ManageViewRows",
			function () use ($silexApp)
			{
				return (new InventoryItemRouter())->getAllManageViewRows($silexApp);
			}
		);

		/**
		 * Get all filtered inventory items. This collects data from multiple tables.
		 * Last Updated by Ravi T on 27-05-2019
		 */

		$silexApp->POST(INVENTORY_API_ROUTE . "/ManageInventorySearch",
			function () use ($silexApp)
			{
				return (new InventoryItemRouter())->getAllInventoryBySearch($silexApp);
			}
		);
	
		/**
		 * Paging for Manage Page
		 * Last Updated by Dhruvpal on 31-01-2019
		 */
		$silexApp->GET(INVENTORY_API_ROUTE . "/ManageViewRowsList/{id}",
			function ($id) use ($silexApp)
			{
				return (new InventoryItemRouter())->getAllManageViewRows($silexApp,$id);
			}
		)->assert('id', '(\d+'.PARAMETER_DELIMITER.'?)+');

		/**
		 * Get all Inventory Item Counts from the inventory_items table.
		 */
		$silexApp->GET(INVENTORY_API_ROUTE . "/InventoryItems/quantityOnHand",
			function () use ($silexApp)
			{
				return (new InventoryItemRouter())->getAllInventoryItemCounts($silexApp);
			}
		);

		/**
		 * Get all inventory items.
		 */
		$silexApp->GET(INVENTORY_API_ROUTE . "/InventoryItems",
			function () use ($silexApp)
			{
				return (new InventoryItemRouter())->getAllInventoryItems($silexApp);
			}
		);

		/**
		 * Get all archived inventory items.
		 */
		$silexApp->GET(INVENTORY_API_ROUTE . "/InventoryItems/archived",
			function () use ($silexApp)
			{
				return (new InventoryItemRouter())->getAllArchivedInventoryItems($silexApp);
			}
		);

		/**
		 * Delete all inventory records.
		 */
		$silexApp->GET(INVENTORY_API_ROUTE . "/Inventory/delete",
			function () use ($silexApp)
			{
				return (new InventoryItemRouter())->deleteAllInventory($silexApp);
			}
		);

		/*
		 * Used to Start or Skip Migration
		 */
		$silexApp->POST(INVENTORY_API_ROUTE."/Migration/Start",
            function(Request $request) use ($silexApp){
		        return( new InventoryItemRouter())->startMigration($request, $silexApp);
            });
		/*
		 * Pause Migration
		 */
		$silexApp->POST(INVENTORY_API_ROUTE."/Migration/pause",
            function(Request $request) use ($silexApp){
		        return( new InventoryItemRouter())->pauseMigration($request, $silexApp);
            });

        /*
         * Resume Migration
         */
        $silexApp->GET(INVENTORY_API_ROUTE."/Migration/resume",
            function() use ($silexApp){
                return( new InventoryItemRouter())->getAllInventoryItems($silexApp);
            });

        /*
         * Resume Migration
         */
        $silexApp->POST(INVENTORY_API_ROUTE."/Migration/finalize",
            function(Request $request) use ($silexApp){
                return( new InventoryItemRouter())->finalizeMigration($request, $silexApp);
            });

        /*
       * Get the value of a set of inventory items
       */
        $silexApp->POST(INVENTORY_API_ROUTE."/InventoryItems/value",
            function(Request $request) use ($silexApp){
                return( new InventoryItemRouter())->calculateValueForItems($request, $silexApp);
            });

        /*
       * Resume Migration
       */
        $silexApp->GET(INVENTORY_API_ROUTE."/InventoryItems/totalValue",
            function(Request $request) use ($silexApp){
                return( new InventoryItemRouter())->calculateTotalValue($request, $silexApp);
			});

		/**
		 * Export all inventory items into csv file.
		 */
		$silexApp->GET(INVENTORY_API_ROUTE . "/InventoryItems/export",
			function () use ($silexApp)
			{
				return (new InventoryItemRouter())->exportInventoryItems($silexApp);
			}
		);



	}

	private function costRoutes(Application $silexApp){
		/**
		 * Get FIFO Cost Data for inventory on-hand.
		 */
		$silexApp->GET(INVENTORY_API_ROUTE."/Cost/onHand",
			function(Request $request) use ($silexApp){return (new InventoryItemRouter())->calculateTotalValue($request, $silexApp);}
		);

		/**
		 * Get FIFO Cost Data for wasted inventory.
		 */
		$silexApp->GET(INVENTORY_API_ROUTE."/Cost/waste",
			function() use ($silexApp){return (new InventoryItemRouter())->getWasteCostData($silexApp);}
		);
	}

	private function purchaseOrderRoutes(Application $silexApp){
		/**
		 * Create a new Purchase Order with Purchase Order Items or multiple Purchase Orders with Purchase Order Items.
		 */
		$silexApp->POST(INVENTORY_API_ROUTE."/PurchaseOrders",
			function(Request $request) use ($silexApp){return (new InventoryPurchaseOrderRouter())->createPurchaseOrders($request, $silexApp);}
		);

		/**
		 * Copy a Purchase Order and it's items, or multiple Purchase Orders and their items.
		 */
		$silexApp->POST(INVENTORY_API_ROUTE."/PurchaseOrders/copy",
			function(Request $request) use ($silexApp){return (new InventoryPurchaseOrderRouter())->copyPurchaseOrders($request, $silexApp);}
		);

		/**
		 *  Change fields in purchaseorders relating to the purchase order's status.
		 */
		$silexApp->PUT(INVENTORY_API_ROUTE."/PurchaseOrders/status",
			function(Request $request) use ($silexApp){return (new InventoryPurchaseOrderRouter())->updatePurchaseOrderStatus($request, $silexApp);}
		);
		/**
		 * Receive a Purchase Order or multiple purchase orders. This means marking the Purchase Order as received and updating
		 * the quantity received.
		 * @param vendorID [int] - required
		 * @param invoiceNumber [int] -
		 * @param
		 */
		$silexApp->PUT(INVENTORY_API_ROUTE."/PurchaseOrders/receive",
			function(Request $request) use ($silexApp){return (new InventoryPurchaseOrderRouter())->receivePurchaseOrders($request, $silexApp);}
		);

		/**
		 * Get row data for the purchase orders main view.
		 * Modified On 28-01-2019 By Dhruvpal to provided paging - LP-8807
		 */
		$silexApp->GET(INVENTORY_API_ROUTE."/PurchaseOrdersView",
			function() use ($silexApp){return (new InventoryPurchaseOrderRouter())->getAllPurchaseOrders($silexApp);}
		);

		/**
		 * Paging for Orders Page
		 * Modified On 31-01-2019 By Dhruvpal to provided paging - LP-8807
		 */
		$silexApp->GET(INVENTORY_API_ROUTE."/PurchaseOrdersViewList/{id}",
			function($id) use ($silexApp){return (new InventoryPurchaseOrderRouter())->getAllPurchaseOrders($silexApp,$id);}
		)->assert('id', '(\d+' . PARAMETER_DELIMITER . '?)+');

		/**
		 * Get data for the create purchase order view.
		 */
		$silexApp->GET(INVENTORY_API_ROUTE."/CreatePurchaseOrdersView",//TODO Does this work correctly? It seems to...
			function() use ($silexApp){return (new InventoryPurchaseOrderRouter())->getCreatePurchaseOrderModalData($silexApp);}
		);

		/**
		 * Get a list of purchase order statuses.
		 */
		$silexApp->GET(INVENTORY_API_ROUTE."/PurchaseOrders/status",
			function() use ($silexApp){return (new InventoryPurchaseOrderRouter())->getAllPurchaseOrderStatuses($silexApp);}
		);

		$silexApp->GET(INVENTORY_API_ROUTE."/PurchaseOrderDrilldown/{ids}",
			function($ids) use ($silexApp){return (new InventoryPurchaseOrderRouter())->getPurchaseOrdersByID($ids, $silexApp);}
		)->assert('ids','(\d+'.PARAMETER_DELIMITER.'?)+');

		/**
		 * Export all Purchase orders into csv file.
		 */
		$silexApp->GET(INVENTORY_API_ROUTE . "/PurchaseOrders/export",
			function () use ($silexApp)
			{
				return (new InventoryPurchaseOrderRouter())->exportPurchaseOrders($silexApp);
			}
		);

	}

	private function vendorItemRoutes(Application $silexApp){
		/**
		 * Create a vendor item, or multiple vendor items.
		 */
		$silexApp->POST(INVENTORY_API_ROUTE."/VendorItems",
			function(Request $request) use ($silexApp){return (new InventoryVendorItemRouter())->createVendorItems($request, $silexApp);}
		);

		/**
		 * Link a vendor item to an inventory item, or create multiple of these 1 to 1 links.
		 */
		$silexApp->PUT(INVENTORY_API_ROUTE."/VendorItems/InventoryItems/link",
			function(Request $request) use ($silexApp){return (new InventoryVendorItemRouter())->linkVendorItemToInventoryItem($request, $silexApp);}
		);

		/**
		 * Link a vendor item to an inventory item, or create multiple of these 1 to 1 links.
		 */
		$silexApp->PUT(INVENTORY_API_ROUTE."/VendorItems",
			function(Request $request) use ($silexApp){return (new InventoryVendorItemRouter())->updateVendorItems($request, $silexApp);}
		);
		/**
		 * Get all vendor items which are linked to inventory items.
		 */
		$silexApp->GET(INVENTORY_API_ROUTE . "/VendorItems/linked/{ids}",
			function ($ids) use ($silexApp)
			{
				return (new InventoryVendorItemRouter())->getLinkedVendorItemsForInventoryItems($ids, $silexApp);
			}
		)->assert('ids', '(\d+' . PARAMETER_DELIMITER . '?)+');

		/**
		 * Get all vendor items for given specific vendor IDs.
		 */
		$silexApp->GET(INVENTORY_API_ROUTE . "/VendorItems/byVendorID/{ids}",
			function ($ids) use ($silexApp)
			{
				return (new InventoryVendorItemRouter())->getVendorItemsByVendorID($ids, $silexApp);
			}
		)->assert('ids', '(\d+' . PARAMETER_DELIMITER . '?)+');

        /**
         * Get all vendor items for given specific vendor IDs.
         */
        $silexApp->GET(INVENTORY_API_ROUTE . "/VendorItems/",
            function () use ($silexApp) {
                return (new InventoryVendorItemRouter())->getVendorItems($silexApp);
            });

	}

	private function categoryRoutes(Application $silexApp){
		/**
		 * Create an inventory category, or create multiple inventory categories.
		 */
		$silexApp->POST(INVENTORY_API_ROUTE."/Categories",
			function(Request $request) use ($silexApp){return (new InventoryCategoryRouter())->createCategories($request, $silexApp);}
		);

		/**
		 * Get all Categories from the categories table
		 */
		$silexApp->GET(INVENTORY_API_ROUTE."/Categories",
			function() use ($silexApp){return (new InventoryCategoryRouter())->getAllCategories($silexApp);}
		);
	}

	private function vendorRoutes(Application $silexApp){
		/**
		 * Create an inventory vendor, or create multiple inventory vendors.
		 */
		$silexApp->POST(INVENTORY_API_ROUTE."/Vendors",
			function(Request $request) use ($silexApp){return (new InventoryVendorRouter())->createVendors($request,$silexApp);}
		);

		/**
		 * Update one or more vendors.
		 */
		$silexApp->PUT(INVENTORY_API_ROUTE."/Vendors",
			function(Request $request) use ($silexApp){return (new InventoryVendorRouter())->updateVendors($request,$silexApp);}
		);

		/**
		 * Get all vendors from the vendors table.
		 */
		$silexApp->GET(INVENTORY_API_ROUTE."/Vendors",
			function() use ($silexApp){return (new InventoryVendorRouter())->getAllVendors($silexApp);}
		);

		/**
		 * Get all vendors from the vendors table.
		 * Last Updated By Dhruvpal On 28-01-2019,to provide paging
		 */
		$silexApp->GET(INVENTORY_API_ROUTE."/VendorList/{id}",
			function($id) use ($silexApp){return (new InventoryVendorRouter())->getAllVendors($silexApp,$id);}
		)->assert('id','(\d+'.PARAMETER_DELIMITER.'?)+');

		/**
		 * Get one or more Vendor by ID.
		 */
		$silexApp->GET(INVENTORY_API_ROUTE."/Vendors/{ids}",
			function($ids) use ($silexApp){ return (new InventoryVendorRouter())->getVendors($ids, $silexApp);}
		)->assert('ids','(\d+'.PARAMETER_DELIMITER.'?)+');

		/**
		 * Get all Ordered Items for each Vendor ID.
		 */
		$silexApp->GET(INVENTORY_API_ROUTE."/Vendors/{ids}/copy",
			function($ids) use ($silexApp){return (new InventoryVendorRouter())->copyVendors($ids, $silexApp);}
		)->assert('ids','(\d+'.PARAMETER_DELIMITER.'?)+');

		/**
		 * Get all Ordered Items for each Vendor ID.
		 */
		$silexApp->GET(INVENTORY_API_ROUTE."/Vendors/{ids}/OrderedItems",
			function($ids) use ($silexApp){return (new InventoryVendorRouter())->getOrderedItems($ids, $silexApp);}
		)->assert('ids','(\d+'.PARAMETER_DELIMITER.'?)+');

		/**
		 * Get last four Past Ordres for each Vendor ID.
		 */
		$silexApp->GET(INVENTORY_API_ROUTE."/Vendors/{ids}/PastOrders",
			function($ids) use ($silexApp){return (new InventoryVendorRouter())->getPastOrders($ids, $silexApp);}
		)->assert('ids','(\d+'.PARAMETER_DELIMITER.'?)+');

		/**
		 * Archive one or more Vendors by id.
		 */
		$silexApp->DELETE(INVENTORY_API_ROUTE."/Vendors/{ids}",
			function($ids) use ($silexApp){return (new InventoryVendorRouter())->archiveVendors($ids, $silexApp);}
		)->assert('ids','(\d+'.PARAMETER_DELIMITER.'?)+');

		/**
		 * Export all vendors records into csv file.
		 */
		$silexApp->GET(INVENTORY_API_ROUTE . "/Vendors/export",
			function () use ($silexApp)
			{
				return (new InventoryVendorRouter())->exportVendors($silexApp);
			}
		);
	}

	private function storageLocationRoutes(Application $silexApp){
		/**
		 * Create an inventory storage location, or create multiple inventory storage locations.
		 */
		$silexApp->POST(INVENTORY_API_ROUTE."/StorageLocations",
			function(Request $request) use ($silexApp){return (new InventoryStorageLocationRouter())->createStorageLocations($request,$silexApp);}
		);

		/**
		 * Get all locations
		 */
		$silexApp->GET(INVENTORY_API_ROUTE."/StorageLocations",
			function() use ($silexApp){return (new InventoryStorageLocationRouter())->getAllStorageLocations($silexApp);}
		);
	}

	private function unitRoutes(Application $silexApp){

		/**
		 * Create an inventory unit, or create multiple inventory units.
		 */
		$silexApp->POST(INVENTORY_API_ROUTE."/Units",
			function(Request $request) use ($silexApp){return (new InventoryUnitRouter())->createUnits($request,$silexApp);}
		);

		/**
		 * Create an inventory unit category, or create multiple inventory unit categories.
		 */
		$silexApp->POST(INVENTORY_API_ROUTE."/UnitCategories",
			function(Request $request) use ($silexApp){return (new InventoryUnitRouter())->createUnitCategories($request,$silexApp);}
		);

		/**
		 * Get all Sales Units of Measure from the inventory_unit table.
		 */
		$silexApp->GET(INVENTORY_API_ROUTE."/Units",
			function() use ($silexApp){return (new InventoryUnitRouter())->getAllUnits($silexApp);}
		);
		/**
		 * Get all Purchase Units of Measure from the purchaseorder_items table
		 */
		$silexApp->GET(INVENTORY_API_ROUTE."/Units/purchase",
			function() use ($silexApp){return (new InventoryUnitRouter())->getAllPurchaseUnitsOfMeasure($silexApp);}
		);
		/**
		 * Get all Sales Units of Measure from the inventory_items table
		 */
		$silexApp->GET(INVENTORY_API_ROUTE."/Units/sales",
			function() use ($silexApp){return (new InventoryUnitRouter())->getAllSalesUnitsOfMeasure($silexApp);}
		);

		/**
		 * Get all Sales Units of Measure from the inventory_items table
		 */
		$silexApp->GET(INVENTORY_API_ROUTE."/Units/custom",
			function() use ($silexApp){return (new InventoryUnitRouter())->getCustomUnitsOfMeasure($silexApp);}
		);

        /**
         * Get Units of Measure from the inventory_unit table based on category id
         */
        $silexApp->GET(INVENTORY_API_ROUTE."/Units/categorByID/{id}",
            function($id) use ($silexApp){return (new InventoryUnitRouter())->getAllUnitsByCategory($id, $silexApp);}
        )->assert('id','(\d+)');
	}

	private function transferRoutes(Application $silexApp){
		/**
		 *  Get transfers by id
		 */
		$silexApp->GET(INVENTORY_API_ROUTE."/Transfers/{ids}",
			function($ids) use ($silexApp){return (new InventoryTransferRouter())->getTransfersByID($ids, $silexApp);}
		)->assert('ids','(\d+'.PARAMETER_DELIMITER.'?)+');

		/**
		 *  update transfers
		 */
		$silexApp->PUT(INVENTORY_API_ROUTE."/Transfers",
			function(Request $request) use ($silexApp){return (new InventoryTransferRouter())->updateTransfers($request, $silexApp);}
		);
		/**
		 * Create a transfer, or create multiple transfers
		 */
		$silexApp->POST(INVENTORY_API_ROUTE."/Transfers",
			function(Request $request) use ($silexApp){return (new InventoryTransferRouter())->createTransfers($request, $silexApp);}
		);

		/**
		 * Copy a transfer, or multiple transfers
		 */
		$silexApp->POST(INVENTORY_API_ROUTE."/Transfers/copy",
			function(Request $request) use ($silexApp){return (new InventoryTransferRouter())->copyTransfers($request, $silexApp);}
		);

		/**
		 * Receive a transfer, or multiple transfers
		 */
		$silexApp->PUT(INVENTORY_API_ROUTE."/Transfers/receive",
			function(Request $request) use ($silexApp){return (new InventoryTransferRouter())->receiveTransfers($request, $silexApp);}
		);

//		/**
//		 * Import a transfer, or multiple transfers
//		 */
//		$silexApp->POST(INVENTORY_API_ROUTE."/Transfers/import",//TODO - Implement
//			function(Request $request) use ($silexApp){return (new InventoryTransferRouter())->importTransfers($request, $silexApp);}
//		);

		/**
		 * Get a list of transfer statuses.
		 */
		$silexApp->GET(INVENTORY_API_ROUTE."/Transfers/status",
			function() use ($silexApp){return (new InventoryTransferRouter())->getAllTransferStatuses($silexApp);}
		);

		/**
		 * Get all Transfer view rows.
		 * Last updated by Dhruvpal on 28-01-2019,provided standalone paging and paging with page record limit
		 */
		$silexApp->POST(INVENTORY_API_ROUTE."/TransfersView",
			function() use ($silexApp){return (new InventoryTransferRouter())->getAllTransferViewRows($silexApp);}
		);

		/**
		* Paging for Transfer Page
		* Updated By Dhruvpal on 31-01-2019
		*/
		$silexApp->GET(INVENTORY_API_ROUTE."/TransfersViewList/{id}",
			function($id) use ($silexApp){return (new InventoryTransferRouter())->getAllTransferViewRows($silexApp,$id);}
		)->assert('id', '(\d+'.PARAMETER_DELIMITER.'?)+');

		/**
		 * Get all received Transfers.
		 */
		$silexApp->GET(INVENTORY_API_ROUTE."/Transfers/received",
			function() use ($silexApp){return (new InventoryTransferRouter())->getAllReceivedTransfers($silexApp);}
		);

		/**
		 * Get all pending Transfers.
		 */
		$silexApp->GET(INVENTORY_API_ROUTE."/Transfers/pending",
			function() use ($silexApp){return (new InventoryTransferRouter())->getAllPendingTransfers($silexApp);}
		);

		/**
		 * Get all chained Locations.
		 */
		$silexApp->GET(INVENTORY_API_ROUTE."/Transfers/locations",
			function() use ($silexApp){return (new InventoryTransferRouter())->getAllTransferLocations($silexApp);}
		);

		/**
		 * Export all transfers records into csv file.
		 */
		$silexApp->GET(INVENTORY_API_ROUTE . "/Transfers/export",
			function () use ($silexApp)
			{
				return (new InventoryTransferRouter())->exportCSVTransfers($silexApp);
			}
		);
	}

	private function reconciliationRoutes(Application $silexApp){
		$silexApp->POST(INVENTORY_API_ROUTE."/Reconcile",
			function(Request $request) use ($silexApp){return (new InventoryReconciliationRouter())->performReconciliation($request,$silexApp);}
		);

		/**
		 * Get the reconciliation view rows.
		 */
		$silexApp->GET(INVENTORY_API_ROUTE."/ReconciliationView",
			function() use ($silexApp){return (new InventoryReconciliationRouter())->getAllReconciliationRows($silexApp);}
		);

		/**
		* Get The Reconciliation Rows According Pagination
		* Modified By Dhruvpalsinh On 29-01-2019
		*/
		$silexApp->GET(INVENTORY_API_ROUTE."/ReconciliationViewList/{id}",
			function($id) use ($silexApp){return (new InventoryReconciliationRouter())->getAllReconciliationRows($silexApp,$id);}
		)->assert('id', '(\d+'.PARAMETER_DELIMITER.'?)+');

		/**
		 * Export all reconciliation records into csv file.
		 */
		$silexApp->GET(INVENTORY_API_ROUTE . "/Reconciliation/export",
			function () use ($silexApp)
			{
				return (new InventoryReconciliationRouter())->exportReconciliation($silexApp);
			}
		);
	}

	private function enterpriseRoutes(Application $silexApp){

        $silexApp->GET(INVENTORY_API_ROUTE."/Location/language",
            function() use ($silexApp){ return (new InventoryEnterpriseRouter())->getLanguageInfo($silexApp); }
        );
		$silexApp->GET(INVENTORY_API_ROUTE."/Regions/getCountries/fullName",
			function() use ($silexApp){ return (new InventoryEnterpriseRouter())->getCountryNamesFull($silexApp); }
		);

		$silexApp->GET(INVENTORY_API_ROUTE."/Regions/getCountries/abbreviated",
			function() use ($silexApp){ return (new InventoryEnterpriseRouter())->getCountryNamesAbbreviated($silexApp); }
		);

		$silexApp->GET(INVENTORY_API_ROUTE."/Regions/getCountries",
			function() use ($silexApp){ return (new InventoryEnterpriseRouter())->getCountries($silexApp); }
		);

		$silexApp->GET(INVENTORY_API_ROUTE."/Chain/restaurantIDAndTitle",
			function() use ($silexApp){ return (new InventoryEnterpriseRouter())->getRestaurantIDAndTitle($silexApp); }
		);

		$silexApp->GET(INVENTORY_API_ROUTE."/Monetary/information",
			function() use ($silexApp){ return (new InventoryEnterpriseRouter())->getMonetaryInformation($silexApp); }
		);

		$silexApp->GET(INVENTORY_API_ROUTE."/Monetary/symbol",
			function() use ($silexApp){return (new InventoryEnterpriseRouter())->getSymbol($silexApp);}
		);

		$silexApp->GET(INVENTORY_API_ROUTE."/Monetary/symbolAlignment",
			function() use ($silexApp){return (new InventoryEnterpriseRouter())->getSymbolAlignment($silexApp);}
		);

		$silexApp->GET(INVENTORY_API_ROUTE."/Monetary/decimalSeparator",
			function() use ($silexApp){return (new InventoryEnterpriseRouter())->getDecimalSeparator($silexApp);}
		);

		$silexApp->GET(INVENTORY_API_ROUTE."/Monetary/thousandsSeparator",
			function() use ($silexApp){return (new InventoryEnterpriseRouter())->getThousandsSeparator($silexApp);}
		);

		$silexApp->GET(INVENTORY_API_ROUTE."/Chain/listChainedLocations",
			function() use ($silexApp){return (new InventoryEnterpriseRouter())->listChainedLocations($silexApp);}
		);

		$silexApp->POST(INVENTORY_API_ROUTE."/Config/create",
            function(Request $request) use ($silexApp){
                return (new InventoryEnterpriseRouter())->createOrUpdateConfigSetting($request, $silexApp);
            }
        );
        $silexApp->POST(INVENTORY_API_ROUTE."/Config/update",
            function(Request $request) use ($silexApp){
                return (new InventoryEnterpriseRouter())->createOrUpdateConfigSetting($request, $silexApp);
            }
        );
        $silexApp->POST(INVENTORY_API_ROUTE."/Config/get",
            function(Request $request) use ($silexApp){
                return (new InventoryEnterpriseRouter())->getConfigSetting($request, $silexApp);
            }
        );

		$silexApp->GET(INVENTORY_API_ROUTE."/Auth/isLavuAdmin",
			function() use ($silexApp){return (new InventoryEnterpriseRouter())->isLavuAdmin($silexApp);}
		);

	}

	private function dashboardRoutes(Application $silexApp){
		$silexApp->GET(INVENTORY_API_ROUTE."/DashboardView",
			function() use ($silexApp){return (new InventoryDashboardRouter())->getDashboardView($silexApp);}
		);

		$silexApp->GET(INVENTORY_API_ROUTE."/LowStockLevel",
			function() use ($silexApp){return (new InventoryDashboardRouter())->getLowStockLevel($silexApp);}
		);

		$silexApp->GET(INVENTORY_API_ROUTE."/InventoryValue",
			function() use ($silexApp){return (new InventoryDashboardRouter())->getInventoryValue($silexApp);}
		);

		$silexApp->GET(INVENTORY_API_ROUTE."/PendingTransfers",
			function() use ($silexApp){return (new InventoryDashboardRouter())->getPendingTransfers($silexApp);}
		);

		$silexApp->GET(INVENTORY_API_ROUTE."/WasteValue",
			function() use ($silexApp){return (new InventoryDashboardRouter())->getWasteValue($silexApp);}
		);

		$silexApp->GET(INVENTORY_API_ROUTE."/purchaseOrders",
			function() use ($silexApp){return (new InventoryDashboardRouter())->getpurchaseOrders($silexApp);}
		);

		$silexApp->GET(INVENTORY_API_ROUTE."/displayItems",
			function() use ($silexApp){return (new InventoryDashboardRouter())->getdisplayItems($silexApp);}
		);
	}

	private function auditRoutes(Application $silexApp){
		$silexApp->POST(INVENTORY_API_ROUTE."/Audit/waste",
			function(Request $request) use ($silexApp){return (new InventoryAuditRouter())->createWasteAudit($request, $silexApp);}
		);

		$silexApp->POST(INVENTORY_API_ROUTE."/Audit/reconciliation",
			function(Request $request) use ($silexApp){return (new InventoryAuditRouter())->createReconciliationAudit($request, $silexApp);}
		);

		$silexApp->POST(INVENTORY_API_ROUTE."/Audit/receiveWithoutPurchaseOrder",
			function(Request $request) use ($silexApp){return (new InventoryAuditRouter())->createReceiveWithoutPOAudit($request, $silexApp);}
		);

        $silexApp->POST(INVENTORY_API_ROUTE."/Audit/add",
            function(Request $request) use ($silexApp){return (new InventoryAuditRouter())->createAudits($request, $silexApp);}
        );

        $silexApp->GET(INVENTORY_API_ROUTE."/Audit/all",
            function(Request $request) use ($silexApp){return (new InventoryAuditRouter())->getAudits($silexApp);}
        );

        $silexApp->POST(INVENTORY_API_ROUTE."/Audit/criteria",
            function(Request $request) use ($silexApp){return (new InventoryAuditRouter())->getCriteriaAudits($request, $silexApp);}
        );
	}

    private function count86Routes(Application $silexApp){
        /**
         * Create an inventory add menu item 86 count.
         */
        $silexApp->POST(INVENTORY_API_ROUTE."/86Count/menuItems",
            function(Request $request) use ($silexApp){return (new Inventory86CountRouter())->create86CountMenuItems($request, $silexApp);}
        );

        /**
         * Create an inventory add modifier 86 count.
         */
        $silexApp->POST(INVENTORY_API_ROUTE."/86Count/modifiers",
            function(Request $request) use ($silexApp){return (new Inventory86CountRouter())->create86CountModifiers($request, $silexApp);}
        );

        /**
         * Create an inventory forced modifier 86 count.
         */
        $silexApp->POST(INVENTORY_API_ROUTE."/86Count/forcedModifiers",
            function(Request $request) use ($silexApp){return (new Inventory86CountRouter())->create86CountForcedModifiers($request, $silexApp);}
        );

        /**
         * Check menu items status an inventory menu item 86 count get by id(s).
         * IN_STOCK = we are able to sell the item and no critical ingredients are below their low quantity
         * LOW_STOCK = we are able to make the item, but at least one ingredient is below the low quantity
         * OUT_STOCK = we can no longer sell this item
         */
        $silexApp->GET(INVENTORY_API_ROUTE."/86Count/Status/menuItem/{ids}",
            function($ids) use ($silexApp){return (new Inventory86CountRouter())->getMenuItemStatus($ids, $silexApp);}
        )->assert('ids','(\d+'.PARAMETER_DELIMITER.'?)+');

        /**
         * Check all menu items status an inventory menu item 86 count.
         * IN_STOCK = we are able to sell the item and no critical ingredients are below their low quantity
         * LOW_STOCK = we are able to make the item, but at least one ingredient is below the low quantity
         * OUT_STOCK = we can no longer sell this item
         */
        $silexApp->GET(INVENTORY_API_ROUTE."/86Count/Status/menuItems",
            function() use ($silexApp){return (new Inventory86CountRouter())->getMenuItemsStatus($silexApp);}
        );

        /**
         * Check modifiers status an inventory modifier 86 count get by id(s).
         * IN_STOCK = we are able to sell the item and no critical ingredients are below their low quantity
         * LOW_STOCK = we are able to make the item, but at least one ingredient is below the low quantity
         * OUT_STOCK = we can no longer sell this item
         */
        $silexApp->GET(INVENTORY_API_ROUTE."/86Count/Status/modifier/{ids}",
            function($ids) use ($silexApp){return (new Inventory86CountRouter())->getModifierStatus($ids, $silexApp);}
        )->assert('ids','(\d+'.PARAMETER_DELIMITER.'?)+');

        /**
         * Check all modifiers status an inventory modifier 86 count.
         * IN_STOCK = we are able to sell the item and no critical ingredients are below their low quantity
         * LOW_STOCK = we are able to make the item, but at least one ingredient is below the low quantity
         * OUT_STOCK = we can no longer sell this item
         */
        $silexApp->GET(INVENTORY_API_ROUTE."/86Count/Status/modifiers",
            function() use ($silexApp){return (new Inventory86CountRouter())->getModifiersStatus($silexApp);}
        );

        /**
         * Check forced modifiers status an inventory forced modifier 86 count get by id(s).
         * IN_STOCK = we are able to sell the item and no critical ingredients are below their low quantity
         * LOW_STOCK = we are able to make the item, but at least one ingredient is below the low quantity
         * OUT_STOCK = we can no longer sell this item
         */
        $silexApp->GET(INVENTORY_API_ROUTE."/86Count/Status/forcedModifier/{ids}",
            function($ids) use ($silexApp){return (new Inventory86CountRouter())->getForcedModifierStatus($ids, $silexApp);}
        )->assert('ids','(\d+'.PARAMETER_DELIMITER.'?)+');

        /**
         * Check all forced modifiers status an inventory forced modifier 86 count get by id(s).
         * IN_STOCK = we are able to sell the item and no critical ingredients are below their low quantity
         * LOW_STOCK = we are able to make the item, but at least one ingredient is below the low quantity
         * OUT_STOCK = we can no longer sell this item
         */
        $silexApp->GET(INVENTORY_API_ROUTE."/86Count/Status/forcedModifiers",
            function() use ($silexApp){return (new Inventory86CountRouter())->getForcedModifiersStatus($silexApp);}
        );

        /**
         * Check all inventory item status which is used in item in 86 count table while server add item.
         * 3 soup means check 3 * quantityUsed available or not available
         * IN_STOCK = we are able to sell the item and no critical ingredients are below their low quantity
         * LOW_STOCK = we are able to make the item, but at least one ingredient is below the low quantity
         * OUT_STOCK = we can no longer sell this item
         */
        $silexApp->POST(INVENTORY_API_ROUTE."/86Count/Status/menu",
            function(Request $request) use ($silexApp){return (new Inventory86CountRouter())->getMenuStatus($request, $silexApp);}
        );
        /**
         * Deduct menu 86 quantity and quantityOnHand when order sent to kitchen.
         * @param id (menuItemID) [int], inventoryItemID [int], quantity [int] - required
         * @param
         */
        $silexApp->PUT(INVENTORY_API_ROUTE."/86Count/Deliver/menuQuantity",
            function(Request $request) use ($silexApp){return (new Inventory86CountRouter())->deductMenuQuantity($request, $silexApp);}
        );

        /**
         * Deduct modifier 86 quantity and quantityOnHand when order sent to kitchen.
         * @param id (menuItemID) [int], inventoryItemID [int], quantity [int] - required
         * @param
         */
        $silexApp->PUT(INVENTORY_API_ROUTE."/86Count/Deliver/modifierQuantity",
            function(Request $request) use ($silexApp){return (new Inventory86CountRouter())->deductModifierQuantity($request, $silexApp);}
        );

        /**
         * Deduct forced modifier quantity and quantityOnHand when order sent to kitchen.
         * @param id (menuItemID) [int], inventoryItemID [int], quantity [int] - required
         * @param
         */
        $silexApp->PUT(INVENTORY_API_ROUTE."/86Count/Deliver/forcedModifierQuantity",
            function(Request $request) use ($silexApp){return (new Inventory86CountRouter())->deductForcedModifierQuantity($request, $silexApp);}
        );

        /**
         * Get records from menu item 86 count.
         */
        $silexApp->GET(INVENTORY_API_ROUTE."/86Count/menuItems/{ids}",
            function($ids) use ($silexApp){return (new Inventory86CountRouter())->get86CountMenuItem($ids, $silexApp);}
        );

        /**
         * Get records from modifier 86 count.
         */
        $silexApp->GET(INVENTORY_API_ROUTE."/86Count/modifiers/{ids}",
            function($ids) use ($silexApp){return (new Inventory86CountRouter())->get86CountModifier($ids, $silexApp);}
        );

        /**
         * Get records from forced modifier 86 count.
         */
        $silexApp->GET(INVENTORY_API_ROUTE."/86Count/forcedModifiers/{ids}",
            function($ids) use ($silexApp){return (new Inventory86CountRouter())->get86CountForcedModifier($ids, $silexApp);}
        );

        /**
         * Delete soft delete records from menu item 86 count based on menuItemID.
         */
        $silexApp->DELETE(INVENTORY_API_ROUTE."/86Count/menuItems",
            function(Request $request) use ($silexApp){return (new Inventory86CountRouter())->delete86CountMenuItems($request, $silexApp);}
        );

        /**
         * Delete soft delete records from modifier 86 count based on modifierID.
         */
        $silexApp->DELETE(INVENTORY_API_ROUTE."/86Count/modifiers",
            function(Request $request) use ($silexApp){return (new Inventory86CountRouter())->delete86CountModifiers($request, $silexApp);}
        );

        /**
         * Delete soft delete records from forced modifier 86 count based on forced_modifierID.
         */
        $silexApp->DELETE(INVENTORY_API_ROUTE."/86Count/forcedModifiers",
            function(Request $request) use ($silexApp){return (new Inventory86CountRouter())->delete86CountForcedModifiers($request, $silexApp);}
        );

        /**
         * Delete soft delete records from menu item 86 count based on menuItemID.
         */
        $silexApp->PUT(INVENTORY_API_ROUTE."/86Count/menuItems",
            function(Request $request) use ($silexApp){return (new Inventory86CountRouter())->update86CountMenuItems($request, $silexApp);}
        );

        /**
         * Delete soft delete records from modifier 86 count based on modifierID.
         */
        $silexApp->PUT(INVENTORY_API_ROUTE."/86Count/modifiers",
            function(Request $request) use ($silexApp){return (new Inventory86CountRouter())->update86CountModifiers($request, $silexApp);}
        );

        /**
         * Delete soft delete records from forced modifier 86 count based on forced_modifierID.
         */
        $silexApp->PUT(INVENTORY_API_ROUTE."/86Count/forcedModifiers",
            function(Request $request) use ($silexApp){return (new Inventory86CountRouter())->update86CountForcedModifiers($request, $silexApp);}
        );

        /**
         * Add and deduct menu 86, modifier 86 and forced modifier 86 quantityUsed from reservation when user add menu item,
         *  modifier and forced modifier from order.
         * @param payload :- [{"added":{"item_id":[{"id":1,"quantity":2},{"id":2,"quantity":3}],"forced_modifier_id":[{"id":1},{"id":2,"quantity":3}],"optional_modifier_id":[{"id":1,"quantity":2},{"id":2,"quantity":3}]},"removed":{"item_id":[{"id":1,"quantity":2},{"id":2,"quantity":3}],"forced_modifier_id":[{"id":1},{"id":2,"quantity":3}],"optional_modifier_id":[{"id":1,"quantity":2},{"id":2,"quantity":3}]}}]
         * @param
         */
        $silexApp->PUT(INVENTORY_API_ROUTE."/86Count/Item/processOrder",
            function(Request $request) use ($silexApp){return (new Inventory86CountRouter())->updateReserveQuantity($request, $silexApp);}
        );

        /**
         * Deduct menu 86, modifier 86 and forced modifier 86 quantityUsed from reservation and from quantityOnHand when order sent to kitchen
         * @param payload :- [{"item_id":[{"id":1,"quantity":2},{"id":2,"quantity":3}],"forced_modifier_id":[{"id":1},{"id":2,"quantity":3}],"optional_modifier_id":[{"id":1,"quantity":2},{"id":2,"quantity":3}]}]
         * @param
         */
        $silexApp->PUT(INVENTORY_API_ROUTE."/86Count/Deliver/Item/order",
            function(Request $request) use ($silexApp){return (new Inventory86CountRouter())->updateDeliverQuantity($request, $silexApp);}
        );

	    /**
	     * Deduct menu 86, modifier 86 and forced modifier 86 quantityUsed from quantityOnHand
	     * @param payload :- [{"added":{"item_id":[{"id":1,"quantity":2},{"id":2,"quantity":3}],"forced_modifier_id":[{"id":1},{"id":2,"quantity":3}],"optional_modifier_id":[{"id":1,"quantity":2},{"id":2,"quantity":3}]},"removed":{"item_id":[{"id":1,"quantity":2},{"id":2,"quantity":3}],"forced_modifier_id":[{"id":1},{"id":2,"quantity":3}],"optional_modifier_id":[{"id":1,"quantity":2},{"id":2,"quantity":3}]}}]
	     * @param
	     */
	    $silexApp->PUT(INVENTORY_API_ROUTE."/86Count/quantityOnHand",
		    function(Request $request) use ($silexApp){return (new Inventory86CountRouter())->updateQuantityOnHand($request, $silexApp);}
	    );

        /**
         * Check menu items status an inventory menu item 86 count get by id(s) based on 86count on SALES unit of measure.
         * if 86 != 0 && inventory stock sales units > menu item ingredients in sales units && user entered 86 count
         * (IN STOCK){ALLOW ORDERING IN APP}
         * if 86 != 0 && inventory stock sales units == menu item ingredients in sales unit && user entered 86 count
         * (LOW STOCK){GIVE 86 WARNING IN APP}
         * if 86 != 0 && inventory stock sales units < menu item ingredients in sales units && user entered 86 count
         * (STOCK STOCK){86 ITEM IN APP}
         * if 86 == 0 && inventory stock sales units - menu item ingredients in sales units > 0 (IN STOCK){ ALLOW ORDERING IN APP}
         * if 86 == 0 && inventory stock sales units - menu item ingredients in sales units == 0 (LOW STOCK) { GIVE 86 WARNING IN APP}
         * if 86 == 0 && inventory stock sales units - menu item ingredients in sales units < 0  (OUT STOCK){86 ITEM IN APP}
         */
        $silexApp->GET(INVENTORY_API_ROUTE."/86Count/Status/lowQuantity/menuItem/{ids}",
            function($ids) use ($silexApp){return (new Inventory86CountRouter())->getMenuItemLowQuantityStatus($ids, $silexApp);}
        )->assert('ids','(\d+'.PARAMETER_DELIMITER.'?)+');

        /**
         * Check all menu items status an inventory menu item 86 count based on 86count on SALES unit of measure.
         * if 86 != 0 && inventory stock sales units > menu item ingredients in sales units && user entered 86 count
         * (IN STOCK){ALLOW ORDERING IN APP}
         * if 86 != 0 && inventory stock sales units == menu item ingredients in sales unit && user entered 86 count
         * (LOW STOCK){GIVE 86 WARNING IN APP}
         * if 86 != 0 && inventory stock sales units < menu item ingredients in sales units && user entered 86 count
         * (STOCK STOCK){86 ITEM IN APP}
         * if 86 == 0 && inventory stock sales units - menu item ingredients in sales units > 0 (IN STOCK){ ALLOW ORDERING IN APP}
         * if 86 == 0 && inventory stock sales units - menu item ingredients in sales units == 0 (LOW STOCK) { GIVE 86 WARNING IN APP}
         * if 86 == 0 && inventory stock sales units - menu item ingredients in sales units < 0  (OUT STOCK){86 ITEM IN APP}
         */
        $silexApp->GET(INVENTORY_API_ROUTE."/86Count/Status/lowQuantity/menuItems",
            function() use ($silexApp){return (new Inventory86CountRouter())->getMenuItemsLowQuantityStatus($silexApp);}
        );

        /**
         * Check modifiers status an inventory menu item 86 count get by id(s) based on 86count on SALES unit of measure.
         * if 86 != 0 && inventory stock sales units > menu item ingredients in sales units && user entered 86 count
         * (IN STOCK){ALLOW ORDERING IN APP}
         * if 86 != 0 && inventory stock sales units == menu item ingredients in sales unit && user entered 86 count
         * (LOW STOCK){GIVE 86 WARNING IN APP}
         * if 86 != 0 && inventory stock sales units < menu item ingredients in sales units && user entered 86 count
         * (STOCK STOCK){86 ITEM IN APP}
         * if 86 == 0 && inventory stock sales units - menu item ingredients in sales units > 0 (IN STOCK){ ALLOW ORDERING IN APP}
         * if 86 == 0 && inventory stock sales units - menu item ingredients in sales units == 0 (LOW STOCK) { GIVE 86 WARNING IN APP}
         * if 86 == 0 && inventory stock sales units - menu item ingredients in sales units < 0  (OUT STOCK){86 ITEM IN APP}
         */
        $silexApp->GET(INVENTORY_API_ROUTE."/86Count/Status/lowQuantity/modifier/{ids}",
            function($ids) use ($silexApp){return (new Inventory86CountRouter())->getModifierLowQuantityStatus($ids, $silexApp);}
        )->assert('ids','(\d+'.PARAMETER_DELIMITER.'?)+');

        /**
         * Check all modifiers status an inventory menu item 86 count get by id(s) based on 86count on SALES unit of measure.
         * if 86 != 0 && inventory stock sales units > menu item ingredients in sales units && user entered 86 count
         * (IN STOCK){ALLOW ORDERING IN APP}
         * if 86 != 0 && inventory stock sales units == menu item ingredients in sales unit && user entered 86 count
         * (LOW STOCK){GIVE 86 WARNING IN APP}
         * if 86 != 0 && inventory stock sales units < menu item ingredients in sales units && user entered 86 count
         * (STOCK STOCK){86 ITEM IN APP}
         * if 86 == 0 && inventory stock sales units - menu item ingredients in sales units > 0 (IN STOCK){ ALLOW ORDERING IN APP}
         * if 86 == 0 && inventory stock sales units - menu item ingredients in sales units == 0 (LOW STOCK) { GIVE 86 WARNING IN APP}
         * if 86 == 0 && inventory stock sales units - menu item ingredients in sales units < 0  (OUT STOCK){86 ITEM IN APP}
         */
        $silexApp->GET(INVENTORY_API_ROUTE."/86Count/Status/lowQuantity/modifiers",
            function() use ($silexApp){return (new Inventory86CountRouter())->getModifiersLowQuantityStatus($silexApp);}
        );

        /**
         * Check forced modifiers status an inventory menu item 86 count get by id(s) based on 86count on SALES unit of measure.
         * if 86 != 0 && inventory stock sales units > menu item ingredients in sales units && user entered 86 count
         * (IN STOCK){ALLOW ORDERING IN APP}
         * if 86 != 0 && inventory stock sales units == menu item ingredients in sales unit && user entered 86 count
         * (LOW STOCK){GIVE 86 WARNING IN APP}
         * if 86 != 0 && inventory stock sales units < menu item ingredients in sales units && user entered 86 count
         * (STOCK STOCK){86 ITEM IN APP}
         * if 86 == 0 && inventory stock sales units - menu item ingredients in sales units > 0 (IN STOCK){ ALLOW ORDERING IN APP}
         * if 86 == 0 && inventory stock sales units - menu item ingredients in sales units == 0 (LOW STOCK) { GIVE 86 WARNING IN APP}
         * if 86 == 0 && inventory stock sales units - menu item ingredients in sales units < 0  (OUT STOCK){86 ITEM IN APP}
         */
        $silexApp->GET(INVENTORY_API_ROUTE."/86Count/Status/lowQuantity/forcedModifier/{ids}",
            function($ids) use ($silexApp){return (new Inventory86CountRouter())->getForcedModifierLowQuantityStatus($ids, $silexApp);}
        )->assert('ids','(\d+'.PARAMETER_DELIMITER.'?)+');

        /**
         * Check all forced modifiers status an inventory menu item 86 count get by id(s) based on 86count on SALES unit of measure.
         * if 86 != 0 && inventory stock sales units > menu item ingredients in sales units && user entered 86 count
         * (IN STOCK){ALLOW ORDERING IN APP}
         * if 86 != 0 && inventory stock sales units == menu item ingredients in sales unit && user entered 86 count
         * (LOW STOCK){GIVE 86 WARNING IN APP}
         * if 86 != 0 && inventory stock sales units < menu item ingredients in sales units && user entered 86 count
         * (STOCK STOCK){86 ITEM IN APP}
         * if 86 == 0 && inventory stock sales units - menu item ingredients in sales units > 0 (IN STOCK){ ALLOW ORDERING IN APP}
         * if 86 == 0 && inventory stock sales units - menu item ingredients in sales units == 0 (LOW STOCK) { GIVE 86 WARNING IN APP}
         * if 86 == 0 && inventory stock sales units - menu item ingredients in sales units < 0  (OUT STOCK){86 ITEM IN APP}
         */
        $silexApp->GET(INVENTORY_API_ROUTE."/86Count/Status/lowQuantity/forcedModifiers",
            function() use ($silexApp){return (new Inventory86CountRouter())->getForcedModifiersLowQuantityStatus($silexApp);}
        );

        /**
         * Check all inventory item status which is used in item in 86 count table while server add item.
         * 3 soup means check 3 * quantityUsed available or not available
         * if 86 != 0 && inventory stock sales units > menu item ingredients in sales units && user entered 86 count
         * (IN STOCK){ALLOW ORDERING IN APP}
         * if 86 != 0 && inventory stock sales units == menu item ingredients in sales unit && user entered 86 count
         * (LOW STOCK){GIVE 86 WARNING IN APP}
         * if 86 != 0 && inventory stock sales units < menu item ingredients in sales units && user entered 86 count
         * (STOCK STOCK){86 ITEM IN APP}
         * if 86 == 0 && inventory stock sales units - menu item ingredients in sales units > 0 (IN STOCK){ ALLOW ORDERING IN APP}
         * if 86 == 0 && inventory stock sales units - menu item ingredients in sales units == 0 (LOW STOCK) { GIVE 86 WARNING IN APP}
         * if 86 == 0 && inventory stock sales units - menu item ingredients in sales units < 0  (OUT STOCK){86 ITEM IN APP}
         */
        $silexApp->POST(INVENTORY_API_ROUTE."/86Count/Status/lowQuantity/menu",
            function(Request $request) use ($silexApp){return (new Inventory86CountRouter())->getMenulowQuantityStatus($request, $silexApp);}
        );

        /**
         * Check inventory item status based on reqorder quantity PURCHASE unit of measure and make reorder alert
         * in 86 count table.
         * IN_STOCK = ingredients are above the reorder quantity
         * LOW_STOCK = ingredients are below the reorder quantity
         */
        $silexApp->GET(INVENTORY_API_ROUTE."/86Count/Status/reorder/inventory/{ids}",
            function($ids) use ($silexApp){return (new Inventory86CountRouter())->getInventoryReorderStatus($ids, $silexApp);}
        )->assert('ids','(\d+'.PARAMETER_DELIMITER.'?)+');

        /**
         * Check all inventory item status based on reqorder quantity PURCHASE unit of measure and make reorder alert
         * in 86 count table.
         * IN_STOCK = ingredients are above the reorder quantity
         * LOW_STOCK = ingredients are below the reorder quantity
         */
        $silexApp->GET(INVENTORY_API_ROUTE."/86Count/Status/reorder/inventorys",
            function() use ($silexApp){return (new Inventory86CountRouter())->getInventorysReorderStatus($silexApp);}
        );

        /**
         * Get status of all inventory items which is related to menu items, forced modifier and
         * modifier based on quantityOnHand, quantityUsed and reservation as well as get status of all inventory items
         * which is related to menu items, forced modifie and modifier based on quantityOnHand, quantityUsed and 86count
         * IN_STOCK = we are able to sell the item and no critical ingredients are below their low quantity
         * LOW_STOCK = we are able to make the item, but at least one ingredient is below the low quantity
         * OUT_STOCK = we can no longer sell this item
         *
         * if 86 != 0 && inventory stock sales units > menu item ingredients in sales units && user entered 86 count
         * (IN STOCK){ALLOW ORDERING IN APP}
         * if 86 != 0 && inventory stock sales units == menu item ingredients in sales unit && user entered 86 count
         * (LOW STOCK){GIVE 86 WARNING IN APP}
         * if 86 != 0 && inventory stock sales units < menu item ingredients in sales units && user entered 86 count
         * (STOCK STOCK){86 ITEM IN APP}
         * if 86 == 0 && inventory stock sales units - menu item ingredients in sales units > 0 (IN STOCK){ ALLOW ORDERING IN APP}
         * if 86 == 0 && inventory stock sales units - menu item ingredients in sales units == 0 (LOW STOCK) { GIVE 86 WARNING IN APP}
         * if 86 == 0 && inventory stock sales units - menu item ingredients in sales units < 0  (OUT STOCK){86 ITEM IN APP}
         */
        $silexApp->GET(INVENTORY_API_ROUTE."/86Count/Status/inventory",
            function() use ($silexApp){return (new Inventory86CountRouter())->getInventoryStatus($silexApp);}
        );

	    /**
	     * Get status of all inventory items which is related to specific category menu items, forced modifier and
	     * modifier based on quantityOnHand, quantityUsed and reservation as well as get status of all inventory items
	     * which is related to menu items, forced modifie and modifier based on quantityOnHand, quantityUsed and 86count
	     * IN_STOCK = we are able to sell the item and no critical ingredients are below their low quantity
	     * LOW_STOCK = we are able to make the item, but at least one ingredient is below the low quantity
	     * OUT_STOCK = we can no longer sell this item
	     *
	     * if 86 != 0 && inventory stock sales units > menu item ingredients in sales units && user entered 86 count
	     * (IN STOCK){ALLOW ORDERING IN APP}
	     * if 86 != 0 && inventory stock sales units == menu item ingredients in sales unit && user entered 86 count
	     * (LOW STOCK){GIVE 86 WARNING IN APP}
	     * if 86 != 0 && inventory stock sales units < menu item ingredients in sales units && user entered 86 count
	     * (STOCK STOCK){86 ITEM IN APP}
	     * if 86 == 0 && inventory stock sales units - menu item ingredients in sales units > 0 (IN STOCK){ ALLOW ORDERING IN APP}
	     * if 86 == 0 && inventory stock sales units - menu item ingredients in sales units == 0 (LOW STOCK) { GIVE 86 WARNING IN APP}
	     * if 86 == 0 && inventory stock sales units - menu item ingredients in sales units < 0  (OUT STOCK){86 ITEM IN APP}
	     */
	    $silexApp->GET(INVENTORY_API_ROUTE."/86Count/Status/category/inventory/{ids}",
		    function($ids) use ($silexApp){return (new Inventory86CountRouter())->getInventoryStatusByCategory($ids, $silexApp);}
	    )->assert('ids','(\d+'.PARAMETER_DELIMITER.'?)+');
    }

    private function wasteRoutes(Application $silexApp){
        /**
         * Get all active waste reasons.
         */
        $silexApp->GET(INVENTORY_API_ROUTE."/Waste/reasons",
            function() use ($silexApp){return (new InventoryWasteRouter())->getWasteReasons($silexApp);}
        );

        //Store waste menu level object into inventory_audit table
        $silexApp->POST(INVENTORY_API_ROUTE."/Waste/Item/audit",
            function(Request $request) use ($silexApp){return (new InventoryWasteRouter())->createItemWasteAudits($request, $silexApp);}
        );
    }

    private function menuItemRoutes(Application $silexApp){
	    $silexApp->GET(INVENTORY_API_ROUTE."/MenuItems/byInventoryItemID/{ids}",
		    function($ids) use ($silexApp){return (new InventoryMenuItemRouter())->getMenuItemsByInventoryItemID($ids, $silexApp);}
	    )->assert('ids','(\d+'.PARAMETER_DELIMITER.'?)+');

	    $silexApp->GET(INVENTORY_API_ROUTE."/MenuItems",
		    function() use ($silexApp){return (new InventoryMenuItemRouter())->getAllMenuItems($silexApp);}
	    );
    }

    private function modifierRoutes(Application $silexApp){
	    $silexApp->GET(INVENTORY_API_ROUTE."/Modifiers/byInventoryItemID/{ids}",
		    function($ids) use ($silexApp){return (new InventoryModifierRouter())->getModifiersByInventoryItemID($ids, $silexApp);}
	    )->assert('ids','(\d+'.PARAMETER_DELIMITER.'?)+');

	    $silexApp->GET(INVENTORY_API_ROUTE."/Modifiers",
		    function() use ($silexApp){return (new InventoryModifierRouter())->getAllModifiers($silexApp);}
	    );
	    $silexApp->GET(INVENTORY_API_ROUTE."/ForcedModifiers/byInventoryItemID/{ids}",
		    function($ids) use ($silexApp){return (new InventoryModifierRouter())->getForcedModifiersByInventoryItemID($ids, $silexApp);}
	    )->assert('ids','(\d+'.PARAMETER_DELIMITER.'?)+');

	    $silexApp->GET(INVENTORY_API_ROUTE."/ForcedModifiers",
		    function() use ($silexApp){return (new InventoryModifierRouter())->getAllForcedModifiers($silexApp);}
	    );
    }

    private function reportRoutes(Application $silexApp){
        /**
         * Inventory Report -- Food and Beverage
         */
        $silexApp->POST(INVENTORY_API_ROUTE."/Reports/FoodAndBeverage",
            function(Request $request) use ($silexApp){return (new InventoryReportRouter())->getFoodAndBeverageReport($request, $silexApp);}
        );

        /**
         * Inventory Report -- Usage
         */
        $silexApp->POST(INVENTORY_API_ROUTE."/Reports/Usage",
            function(Request $request) use ($silexApp){return (new InventoryReportRouter())->getUsageReport($request, $silexApp);}
        );

        /**
         * Inventory Report -- Low Inventory
         */
        $silexApp->POST(INVENTORY_API_ROUTE."/Reports/LowInventory",
            function(Request $request) use ($silexApp){return (new InventoryReportRouter())->getLowInventoryReport($request, $silexApp);}
        );

        /**
         * Inventory Report -- Waste
         */
        $silexApp->POST(INVENTORY_API_ROUTE."/Reports/Waste",
            function(Request $request) use ($silexApp){return (new InventoryReportRouter())->getWasteReport($request, $silexApp);}
        );

        /**
         * Inventory Report -- Transfer
         */
        $silexApp->POST(INVENTORY_API_ROUTE."/Reports/Transfer",
            function(Request $request) use ($silexApp){return (new InventoryReportRouter())->getTransferReport($request, $silexApp);}
        );

        /**
         * Inventory Report -- Variance
         */
        $silexApp->POST(INVENTORY_API_ROUTE."/Reports/Variance",
            function(Request $request) use ($silexApp){return (new InventoryReportRouter())->getVarianceReport($request, $silexApp);}
        );
    }

    private function locationRoutes(Application $silexApp){
        /**
         * Get all chained Locations.
         */
        $silexApp->GET(INVENTORY_API_ROUTE."/Location/Chain/locations",
            function() use ($silexApp){return (new InventoryLocationRouter())->getAllChainedLocations($silexApp);}
        );

        $silexApp->POST(INVENTORY_API_ROUTE."/Location/Change/location",
            function(Request $request) use ($silexApp){return (new InventoryLocationRouter())->changeLocation($request, $silexApp);}
        );
    }

	private function cacheRoutes(Application $silexApp){
		/**
		 * Get all cache keys Locations.
		 */
		$silexApp->GET(INVENTORY_API_ROUTE."/Cache/getAll",
			function() use ($silexApp){return (new InventoryRedisRouter())->getAllRedisKeys($silexApp);}
		);

		/**
		 * Check cache keys exist.
		 * Input ["demoleif_86count_menuitem_86", "demoleif_inventory_stock_status", "demoleif_inventory_stock_status123"]
		 */
		$silexApp->POST(INVENTORY_API_ROUTE."/Cache/key/check",
			function(Request $request) use ($silexApp){return (new InventoryRedisRouter())->checkRedisKeys($request, $silexApp);}
		);

		/**
		 * Delete cache keys
		 * Input ["demoleif_1_86count_menuitem_86", "demoleif_1_inventory_stock_status", "demoleif_1_inventory_stock_status123"]
		 */
		$silexApp->DELETE(INVENTORY_API_ROUTE."/Cache/delete",
			function(Request $request) use ($silexApp){return (new InventoryRedisRouter())->deleteRedisKeys($request, $silexApp);}
		);

		/**
		 * Delete menu category cache key
		 * Input
		 * return deleted key
		 */
		$silexApp->DELETE(INVENTORY_API_ROUTE."/Cache/MenuCategory/delete",
			function() use ($silexApp){return (new InventoryRedisRouter())->deleteMenuCategoryKey($silexApp);}
		);

		/**
		 * Set menu items detials into cache.
		 * Input {"1119":{"id":1119,"inv_count":0,"track_86_count":"86countInventory","combo":0},"957":{"id":957,"inv_count":10,"track_86_count":"86countInventory","combo":0}}
		 */
		$silexApp->POST(INVENTORY_API_ROUTE."/Cache/setMenuItems",
			function(Request $request) use ($silexApp){return (new InventoryRedisRouter())->setCacheMenuItems($request, $silexApp);}
		);

		/**
		 * Set forced modifiers detials into cache.
		 * Input {"101":{"id":90,"title":'Garlic Fries'},"57":{"id":57,"title":'Salad'}}
		 */
		$silexApp->POST(INVENTORY_API_ROUTE."/Cache/setForcedModifiers",
			function(Request $request) use ($silexApp){return (new InventoryRedisRouter())->setCacheForcedModifiers($request, $silexApp);}
		);

		/**
		 * Set modifiers detials into cache.
		 * Input {"1119":{"id":90,"title":'Gluten Free'},"957":{"id":57,"title":'Bread'}}
		 */
		$silexApp->POST(INVENTORY_API_ROUTE."/Cache/setModifiers",
			function(Request $request) use ($silexApp){return (new InventoryRedisRouter())->setCacheModifiers($request, $silexApp);}
		);

		/**
		 * Get language dictionary data from redis cache with given key.
		 * Input :- json string {"key": "active_language_pack"};
		 * Output :- Array
		 */
		$silexApp->POST(INVENTORY_API_ROUTE."/Cache/getLanguageDictionary",
			function(Request $request) use ($silexApp){return (new InventoryRedisRouter())->getLanguageDictionary($request, $silexApp);}
		);

		/**
		 * Set language dictionary data into redis cache with given key.
		 * Input {"key": "active_language_pack","data":{"name":"name","title":'Language Dictionary'}}
		 */
		$silexApp->POST(INVENTORY_API_ROUTE."/Cache/setLanguageDictionary",
			function(Request $request) use ($silexApp){return (new InventoryRedisRouter())->setLanguageDictionary($request, $silexApp);}
		);

		/**
		 * Get data from redis cache with given key.
		 * Input :- json string {"key": "active_language_pack_id"} ;
		 * Output :- Array
		 */
		$silexApp->POST(INVENTORY_API_ROUTE."/Cache/getCacheData",
			function(Request $request) use ($silexApp){return (new InventoryRedisRouter())->getCacheData($request, $silexApp);}
		);

		/**
		 * Set data into redis cache with given key.
		 * Input {"key": "active_language_pack_id","data":{"active_language_pack_id":"1","title":'Language Dictionary'}}
		 */
		$silexApp->POST(INVENTORY_API_ROUTE."/Cache/setCacheData",
			function(Request $request) use ($silexApp){return (new InventoryRedisRouter())->setCacheData($request, $silexApp);}
		);
	}
}
