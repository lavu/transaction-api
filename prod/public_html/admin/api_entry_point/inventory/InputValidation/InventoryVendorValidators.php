<?php
require_once __DIR__."/ValidationFunctions.php";
/**
 * Created by PhpStorm.
 * User: Leif
 * Date: 6/6/17
 * Time: 4:54 PM
 */
class InventoryVendorValidator extends ValidationFunctions
{
	protected $name;
	protected $contactName;
	protected $contactPhone;
	protected $contactEmail;
	protected $contactFax;
	protected $address;
	protected $city;
	protected $stateProvinceRegion;
	protected $zipCode;
	protected $country;

	public function validate(){
		$this->validateRequiredFieldsNotBlank(array('name'));
		$this->validateVendorNameNotDuplicate('name');
		return $this->result();
	}
}