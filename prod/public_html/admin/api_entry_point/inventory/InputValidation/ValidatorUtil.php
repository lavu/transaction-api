<?php

require_once __DIR__."/../RequestRouterFunctions.php"; //Included in order to ensure use of the Silex functions.
use Silex\Application;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\ConstraintViolationList as ConstraintViolationList;

/**
 * Created by PhpStorm.
 * User: Leif
 * Date: 6/7/17
 * Time: 9:21 AM
 */

/**
 * Supported validation constraints may be found at http://symfony.com/doc/current/validation.html
 *
 * Constraint violations are saved as Symfony\Component\Validator\ConstraintViolationList objects.
 *  The ConstraintViolationList is a list of ConstraintViolantions objects.
 *
 */
class ValidatorUtil
{
	protected $validator;
	protected $violationResponse;

	/** Upon construction of sub-classes, this constructor should be called, and this will initialize the member
	 *  variables for those sub-classes.
	 */
	public function __construct( Application $silexApp, $request){
		$this->validator = $silexApp['validator'];
		$this->violationResponse = array();
		$this->setMemberVariables($request);
	}

	/**
	 * Determine the return value based off of the number of elements in the violationResponse field. If there are no
	 * violations, the violation response field count will be 0, and so null shall be returned, otherwise the violations
	 * will be returned.
	 */
	protected function result(){
		if(count($this->violationResponse) > 0){
			return $this->violationResponse;
		}else{
			return null;
		}
	}

	/**
	 * Populate member variables for sub-classes with the parameters from the request formatter. Sub-class members names
	 * must be the same as the fields in the formatter that are heading to the database. These fields names may differ
	 * from what is sent in from external requests.
	 */
	protected function setMemberVariables($request){
		foreach($request as $i => $obj){
			foreach($obj as $key=>$value){
				if($key != 'validator' && $key != 'violations' && $key != 'violationReasons')
				{
					$this->{$key} = $value;
				}
			}
		}
	}

	/**
	 * Appending validations will allow us to create an error message showing all failed validations.
	 */
	protected function appendViolationResponse(ConstraintViolationList $violations, $validatedParam){
		if($violations->count() > 0){
			for($i = 0; $i < $violations->count(); $i++){
				$violation = $violations->get($i);
				$this->violationResponse[] = array(
					'FailedParameter'=>$validatedParam,
					'InvalidValue'=>$violation->getInvalidValue(),
					'Message'=> $violation->getMessage(),
//					'Code'=>$violation->getCode(),
//					'Path'=>$violation->getPropertyPath()
				);
			}
		}
	}

}