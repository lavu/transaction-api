<?php
require_once __DIR__."/ValidatorUtil.php";
/**
 * Created by PhpStorm.
 * User: Leif
 * Date: 6/8/17
 * Time: 10:35 AM
 */
use Symfony\Component\Validator\Constraints as Assert;
class ValidationFunctions extends ValidatorUtil
{
	protected function validatePositiveIntegers($paramArray){
		if(!is_array($paramArray)){
			error_log(__FILE__ . " " . __LINE__ . " " . "The paramArray must be an array. Value given: ". print_r($paramArray,1));
		}
		foreach($paramArray as $i=>$paramName){
			$this->validatePositiveInteger($paramName);
		}
	}

	protected function validatePositiveDoubles($paramArray){
		if(!is_array($paramArray)){
			error_log(__FILE__ . " " . __LINE__ . " " . "The paramArray must be an array. Value given: ". print_r($paramArray,1));
		}
		foreach($paramArray as $i=>$paramName){
			$this->validatePositiveDouble($paramName);
		}
	}

	protected function validatePositiveInteger($paramName){
		if(isset($this->{$paramName}))
		{
			$violations = $this->validator->validate($this->{$paramName}, new Assert\GreaterThan(
				array(
					'value' => 0
				)
			));
			$this->appendViolationResponse($violations, $paramName);
			$violations = $this->validator->validate($this->{$paramName},
				new Assert\Type(
					array(
						'type'=>'integer',
						'message'=>'The value {{ value }} is not a valid {{ type }}'
					)
				)
			);
			$this->appendViolationResponse($violations, $paramName);
		}
	}

	protected function validatePositiveDouble($paramName){
		if(isset($this->{$paramName}))
		{
			$violations = $this->validator->validate($this->{$paramName}, new Assert\GreaterThan(
				array(
					'value' => 0
				)
			));
			$this->appendViolationResponse($violations, $paramName);
			$violations = $this->validator->validate($this->{$paramName},
				new Assert\Type(
					array(
						'type'=>'double',
						'message'=>'The value {{ value }} is not a valid {{ type }}'
					)
				)
			);
			$this->appendViolationResponse($violations, $paramName);
		}
	}

	protected function validateRequiredFieldsNotBlank($paramArray){
		if(!is_array($paramArray)){
			error_log(__FILE__ . " " . __LINE__ . " " . "The paramArray must be an array. Value given: ". print_r($paramArray,1));
		}
		foreach($paramArray as $i=>$paramName){
			$violations = $this->validator->validate($this->{$paramName} ,new Assert\NotBlank());
			$this->appendViolationResponse($violations, $paramName);
		}
	}

    /**
     * This function is used to check if value is like "12,34,56"
     */
    protected function validateCommaSeparatedIntegers($paramArray){
        if(!is_array($paramArray)){
            error_log(__FILE__ . " " . __LINE__ . " " . "The paramArray must be an array. Value given: ". print_r($paramArray,1));
        }
        foreach($paramArray as $i=>$paramName){
            if($this->{$paramName} != '') {
                $integerIDs = json_decode('[' . $this->{$paramName} . ']', true);
                if (is_array($integerIDs)) {
                    foreach ($integerIDs as $id) {
                        $this->validateCommaSeparatedPositiveInteger($paramName, $id);
                    }
                } else {
                    $this->validateCommaSeparatedPositiveInteger($paramName, $this->{$paramName});
                }
            }
        }
    }

    protected function validateCommaSeparatedPositiveInteger($paramName, $value){
        if(isset($this->{$paramName}))
        {
            $violations = $this->validator->validate($value, new Assert\GreaterThan(
                array(
                    'value' => 0
                )
            ));
            $this->appendViolationResponse($violations, $paramName);
            $violations = $this->validator->validate($value,
                new Assert\Type(
                    array(
                        'type'=>'integer',
                        'message'=>'The value {{ value }} is not a valid {{ type }}'
                    )
                )
            );
            $this->appendViolationResponse($violations, $paramName);
        }
    }

    protected function validateVendorNameNotDuplicate($name){
		if(isset($this->{$name})){
			require_once(__DIR__."/../../../components/Inventory/InventoryObjectControllers/InventoryVendorController.php");
			require_once(__DIR__."/../../../components/Inventory/RequestFormatters/RequestFormatterUtil.php");
			$vendorController = new InventoryVendorController();
			$vendorsWithName = (new RequestFormatterUtil())->decodeLavuAPIResponse($vendorController->getVendorsByName(array($this->{$name})));
			$count = count($vendorsWithName);
			$violations = $this->validator->validate($count,
				new Assert\LessThanOrEqual(
					array(
						'value'=>0,
						'message'=>'Vendor with name "'.$this->{$name}.'" already exists'
					)
				)
			);
			$this->appendViolationResponse($violations, $name);
		}
    }
}