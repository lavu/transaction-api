<?php
require_once __DIR__."/ValidationFunctions.php";
/**
 * Created by PhpStorm.
 * User: Hayat
 * Date: 9/6/17
 * Time: 10:52 PM
 */
class inventoryReportValidator extends ValidationFunctions
{
    protected $menuItemID;
    protected $InventoryItemID;
    protected $quantityUsed;
    protected $criticalItem;
    protected $unitID;
    protected $reservation;
    protected $_deleted;

    public function validateFoodAndBeverageReport(){
        $this->validateRequiredFieldsNotBlank(array('startDate', 'endDate'));
        return $this->result();
    }
}
