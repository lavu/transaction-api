<?php
require_once __DIR__."/ValidationFunctions.php";
/**
 * Created by PhpStorm.
 * User: Hayat
 * Date: 9/6/17
 * Time: 10:52 PM
 */
class inventory86CountMenuItemValidator extends ValidationFunctions
{
    protected $menuItemID;
    protected $inventoryItemID;
    protected $quantityUsed;
    protected $criticalItem;
    protected $unitID;
    protected $reservation;
    protected $_deleted;

    public function validate(){
        $this->validateRequiredFieldsNotBlank(array('menuItemID', 'inventoryItemID', 'quantityUsed', 'unitID'));
        $this->validatePositiveIntegers(array('menuItemID', 'inventoryItemID', 'unitID'));
        $this->validatePositiveDouble(array('quantityUsed', 'reservation'));
        return $this->result();
    }
}

class inventory86CountModifierValidator extends ValidationFunctions
{
    protected $modifierID;
    protected $inventoryItemID;
    protected $quantityUsed;
    protected $criticalItem;
    protected $unitID;
    protected $reservation;
    protected $_deleted;

    public function validate(){
        $this->validateRequiredFieldsNotBlank(array('modifierID', 'inventoryItemID', 'quantityUsed', 'unitID'));
        $this->validatePositiveIntegers(array('modifierID', 'inventoryItemID', 'unitID'));
        $this->validatePositiveDouble(array('quantityUsed', 'reservation'));
        return $this->result();
    }
}

class inventory86CountForcedModifierValidator extends ValidationFunctions
{
    protected $forced_modifierID;
    protected $inventoryItemID;
    protected $quantityUsed;
    protected $criticalItem;
    protected $unitID;
    protected $reservation;
    protected $_deleted;

    public function validate(){
        $this->validateRequiredFieldsNotBlank(array('forced_modifierID', 'inventoryItemID', 'quantityUsed', 'unitID'));
        $this->validatePositiveIntegers(array('forced_modifierID', 'inventoryItemID', 'unitID'));
        $this->validatePositiveDouble(array('quantityUsed', 'reservation'));
        return $this->result();
    }
}

class inventoryMenuItemsStatusValidator extends ValidationFunctions
{
    protected $id;

    public function validate(){
        $this->validatePositiveIntegers(array('id'));
        return $this->result();
    }
}

class inventoryModifiersStatusValidator extends ValidationFunctions
{
    protected $id;

    public function validate(){
        $this->validatePositiveIntegers(array('id'));
        return $this->result();
    }
}

class inventoryForcedModifiersStatusValidator extends ValidationFunctions
{
    protected $id;

    public function validate(){
        $this->validatePositiveIntegers(array('id'));
        return $this->result();
    }
}

class inventoryDeduct86CountQuantityValidator extends ValidationFunctions
{
    protected $id;
    protected $inventoryItemID;
    protected $quantity;

    public function validate(){
        $this->validateRequiredFieldsNotBlank(array('id', 'inventoryItemID', 'quantity'));
        $this->validatePositiveIntegers(array('id', 'inventoryItemID', 'quantity'));
        return $this->result();
    }
}

class inventory86CountDeleteValidator extends ValidationFunctions
{
    protected $id;

    public function validate(){
        $this->validateRequiredFieldsNotBlank(array('id'));
        $this->validatePositiveIntegers(array('id'));
        return $this->result();
    }
}