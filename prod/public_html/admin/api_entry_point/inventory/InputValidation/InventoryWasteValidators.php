<?php
/**
 * Created by PhpStorm.
 * User: hayat
 * Date: 7/15/17
 * Time: 3:44 PM
 */

class CreateItemWasteAuditValidator extends ValidationFunctions{
    protected $id;
    protected $wasteQuantity;
    protected $userID;
    protected $wasteReason;

    public function validate(){
        $this->validateRequiredFieldsNotBlank(array('id', 'wasteQuantity', 'userID', 'wasteReason'));
        $this->validatePositiveIntegers(array('id', 'wasteQuantity', 'userID'));
        return $this->result();
    }
}