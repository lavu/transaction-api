<?php
require_once __DIR__."/ValidationFunctions.php";
/**
 * Created by PhpStorm.
 * User: Leif
 * Date: 6/6/17
 * Time: 4:51 PM
 */

use Symfony\Component\Validator\Constraints as Assert;

class WasteAuditValidator extends ValidationFunctions{
	protected $inventoryItemID;
	protected $action;
	protected $quantity;
	protected $unitID;
	protected $cost;
	protected $userID;
	protected $userNotes;
	protected $datetime;
	protected $_deleted;

	public function validate(){
		$this->validateRequiredFieldsNotBlank(array('action', 'datetime', 'userID'));
		$this->validatePositiveIntegers(array('inventoryItemID', 'quantity', 'unitID', 'userID'));
		return $this->result();
	}


}

class ReconciliationAuditValidator extends ValidationFunctions{
	protected $inventoryItemID;
	protected $action;
	protected $quantity;
	protected $unitID;
	protected $cost;
	protected $userID;
	protected $userNotes;
	protected $datetime;
	protected $_deleted;

	public function validate(){
		$this->validateRequiredFieldsNotBlank(array('action', 'datetime', 'userID'));
		$this->validatePositiveIntegers(array('inventoryItemID', 'quantity', 'unitID', 'userID'));
		return $this->result();
	}
}

class ReceiveWithoutPOAuditValidator extends ValidationFunctions{
	protected $inventoryItemID;
	protected $action;
	protected $quantity;
	protected $unitID;
	protected $cost;
	protected $userID;
	protected $userNotes;
	protected $datetime;
	protected $_deleted;

	public function validate(){
		$this->validateRequiredFieldsNotBlank(array('action', 'datetime', 'userID'));
		$this->validatePositiveIntegers(array('inventoryItemID', 'quantity', 'unitID', 'userID'));
		return $this->result();
	}
}

class CreateAuditValidator extends ValidationFunctions{
    protected $inventoryItemID;
    protected $unitID;
    protected $userID;
    protected $datetime;

    public function validate(){
        $this->validatePositiveIntegers(array('inventoryItemID', 'userID'));
        return $this->result();
    }
}