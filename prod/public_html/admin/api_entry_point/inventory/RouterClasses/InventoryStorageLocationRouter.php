<?php
use Symfony\Component\HTTPFoundation\Request;
use Silex\Application;
require_once(__DIR__ . "/../InputValidation/InventoryStorageLocationValidators.php");

/**
 * Created by PhpStorm.
 * User: Leif
 * Date: 5/23/17
 * Time: 10:44 AM
 */
class InventoryStorageLocationRouter
{
	public function createStorageLocations(Request $request, Application $silexApp){
		$storageLocationFormatter = new InventoryStorageLocationRequestFormatter();
		$storageLocationController = new InventoryStorageLocationController();

		$postBodyArray = $storageLocationFormatter->formatCreateStorageLocations(decodeRequest($request));
		$response = $storageLocationController->createStorageLocations($postBodyArray);
		return $silexApp->json($response,getReturnStatusValue($response));
	}

	public function getAllStorageLocations(Application $silexApp){
		$storageLocationController = new InventoryStorageLocationController();
		$response = $storageLocationController->getAllStorageLocations();
		return $silexApp->json($response,getReturnStatusValue($response));
	}
}