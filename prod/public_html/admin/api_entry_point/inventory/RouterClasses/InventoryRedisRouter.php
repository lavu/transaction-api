<?php
use Symfony\Component\HTTPFoundation\Request;
use Silex\Application;

/**
 * Created by PhpStorm.
 * User: Leif
 * Date: 5/23/17
 * Time: 10:34 AM
 */
class InventoryRedisRouter
{
	public function getAllRedisKeys(Application $silexApp){
		$redisController = new InventoryRedisController();
		$response = $redisController->getAllRedisKeys();
		return $silexApp->json($response, getReturnStatusValue($response));
	}

	public function checkRedisKeys(Request $request, Application $silexApp){
		$redisController = new InventoryRedisController();
		$postBodyArray = decodeRequest($request);
		$response = $redisController->checkRedisKeys($postBodyArray);
		return $silexApp->json($response, getReturnStatusValue($response));
	}

	public function deleteRedisKeys(Request $request, Application $silexApp){
		$redisController = new InventoryRedisController();
		$postBodyArray = decodeRequest($request);
		$response = $redisController->deleteRedisKeys($postBodyArray);
		return $silexApp->json($response, getReturnStatusValue($response));
	}

	public function deleteMenuCategoryKey(Application $silexApp){
		$redisController = new InventoryRedisController();
		$response = $redisController->deleteMenuCategoryKey();
		return $silexApp->json($response, getReturnStatusValue($response));
	}

	public function setCacheMenuItems($request, Application $silexApp){
		$postBodyArray = decodeRequest($request);
		$controller = new InventoryRedisController();
		$response = $controller->setMenuItems($postBodyArray);
		return $silexApp->json($response, getReturnStatusValue($response));
	}

	public function setCacheForcedModifiers($request, Application $silexApp){
		$request = createRequestFromURLParams($request);
		$controller = new InventoryRedisController();
		$response = $controller->setForcedModifiers($request);
		return $silexApp->json($response, getReturnStatusValue($response));
	}

	public function setCacheModifiers($request, Application $silexApp){
		$request = createRequestFromURLParams($request);
		$controller = new InventoryRedisController();
		$response = $controller->setModifiers($request);
		return $silexApp->json($response, getReturnStatusValue($response));
	}

	public function getLanguageDictionary($request, Application $silexApp){
		$postBodyArray = decodeRequest($request);
		$controller = new InventoryRedisController();
		$response = $controller->getLanguageDictionary($postBodyArray);
		return $silexApp->json($response, getReturnStatusValue($response));
	}

	public function setLanguageDictionary($request, Application $silexApp){
		$postBodyArray = json_encode($request->getContent());
		$controller = new InventoryRedisController();
		$response = $controller->setLanguageDictionary($postBodyArray);
		return $silexApp->json($response, getReturnStatusValue($response));
	}

	public function getCacheData($request, Application $silexApp){
		$postBodyArray = decodeRequest($request);
		$controller = new InventoryRedisController();
		$response = $controller->getCacheData($postBodyArray);
		return $silexApp->json($response, getReturnStatusValue($response));
	}

	public function setCacheData($request, Application $silexApp){
		$postBodyArray = decodeRequest($request);
		$controller = new InventoryRedisController();
		$response = $controller->setCacheData($postBodyArray);
		return $silexApp->json($response, getReturnStatusValue($response));
	}
}