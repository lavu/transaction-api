<?php
use Symfony\Component\HTTPFoundation\Request;
use Silex\Application;
require_once(__DIR__ . "/../InputValidation/InventoryEnterpriseValidators.php");

/**
 * Created by PhpStorm.
 * User: Leif
 * Date: 5/23/17
 * Time: 11:35 AM
 */
class InventoryEnterpriseRouter
{
	public function getCountryNamesFull(Application $silexApp){
		$controller = new InventoryEnterpriseController();
		$response = $controller->getCountryNamesFull();
		return $silexApp->json($response,getReturnStatusValue($response));
	}

	public function getCountryNamesAbbreviated(Application $silexApp){
		$controller = new InventoryEnterpriseController();
		$response = $controller->getCountryNamesAbbreviated();
		return $silexApp->json($response,getReturnStatusValue($response));
	}

	public function getCountries(Application $silexApp){
		$controller = new InventoryEnterpriseController();
		$response = $controller->getCountries();
		return $silexApp->json($response,getReturnStatusValue($response));
	}

	public function getRestaurantIDAndTitle(Application $silexApp){
		$controller = new InventoryEnterpriseController();
		$response = $controller->getRestaurantIDAndTitle();
		return $silexApp->json($response,getReturnStatusValue($response));
	}

	public function listChainedLocations(Application $silexApp){
		$controller = new InventoryEnterpriseController();
		$response = $controller->listChainedLocations();
		return $silexApp->json($response,getReturnStatusValue($response));
	}

	public function getMonetaryInformation(Application $silexApp){
		$controller = new InventoryMiscController();
		$response = $controller->getMonetaryInformation();
		return $silexApp->json($response,getReturnStatusValue($response));
	}

	public function getSymbol(Application $silexApp){
		$controller = new InventoryMiscController();
		$response = $controller->getMonetarySymbol();
		return $silexApp->json($response,getReturnStatusValue($response));
	}

	public function getSymbolAlignment(Application $silexApp){
		$controller = new InventoryMiscController();
		$response = $controller->getMonetarySymbolAlignment();
		return $silexApp->json($response,getReturnStatusValue($response));
	}

	public function getDecimalSeparator(Application $silexApp){
		$controller = new InventoryMiscController();
		$response = $controller->getMonetaryDecimalSeparator();
		return $silexApp->json($response,getReturnStatusValue($response));
	}

	public function getThousandsSeparator(Application $silexApp){
		$controller = new InventoryMiscController();
		$response = $controller->getMonetaryThousandsSeparator();
		return $silexApp->json($response,getReturnStatusValue($response));
	}

	public function createOrUpdateConfigSetting(Request $request, Application $silexApp){
	    $controller = new InventoryMiscController();
	    $response = $controller->createOrUpdateConfigSetting(decodeRequest($request));
	    return $silexApp->json($response, getReturnStatusValue($response));
    }
    public function getConfigSetting(Request $request, Application $silexApp){
        $controller = new InventoryMiscController();
        $response = $controller->getConfigSetting(decodeRequest($request));
        return $silexApp->json($response, getReturnStatusValue($response));
    }
    public function getLanguageInfo(Application $silexApp){
        $controller = new InventoryEnterpriseController();
        $response = $controller->getLanguageInfo();
        return $silexApp->json($response, getReturnStatusValue($response));
    }

	public function isLavuAdmin(Application $silexApp){
		$controller = new InventoryMiscController();
		$response = $controller->isLavuAdmin();
		return $silexApp->json($response,getReturnStatusValue($response));
	}
}