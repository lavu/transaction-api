<?php
use Symfony\Component\HTTPFoundation\Request;
use Silex\Application;
require_once(__DIR__ . "/../InputValidation/InventoryPurchaseOrderValidators.php");

/**
 * Created by PhpStorm.
 * User: Leif
 * Date: 5/22/17
 * Time: 10:20 PM
 */
class InventoryPurchaseOrderRouter
{
	public function getPurchaseOrdersByID($request, Application $silexApp){
		$request = createRequestFromURLParams($request);
        $purchaseOrderController = new InventoryPurchaseOrderController();
        $response = $purchaseOrderController->getPurchaseOrderDetails($request);
		return $silexApp->json($response,getReturnStatusValue($response));
	}

	public function createPurchaseOrders(Request $request, Application $silexApp){
		$purchaseOrderRequestFormatter = new InventoryPurchaseOrderRequestFormatter();
		$purchaseOrderController = new InventoryPurchaseOrderController();

		$postBodyArray = $purchaseOrderRequestFormatter->formatCreatePurchaseOrder(decodeRequest($request));
		$response = $purchaseOrderController->createPurchaseOrders($postBodyArray);
		return $silexApp->json($response,getReturnStatusValue($response));
	}

	public function copyPurchaseOrders(Request $request, Application $silexApp){
		$purchaseOrderRequestFormatter = new InventoryPurchaseOrderRequestFormatter();
		$purchaseOrderController = new InventoryPurchaseOrderController();

		$postBodyArray = $purchaseOrderRequestFormatter->formatCopyPurchaseOrder(decodeRequest($request));
		$response = $purchaseOrderController->copyPurchaseOrders($postBodyArray);
		return $silexApp->json($response,getReturnStatusValue($response));
	}

	public function updatePurchaseOrderStatus(Request $request, Application $silexApp){
		$formatter = new InventoryPurchaseOrderRequestFormatter();
		$controller = new InventoryPurchaseOrderController();
		$formattedRequest = $formatter->formatUpdatePurchaseOrderStatuses(decodeRequest($request));
		$response = $controller->updatePurchaseOrderStatuses($formattedRequest);
		return $silexApp->json($response,getReturnStatusValue($response));
	}

	public function receivePurchaseOrders(Request $request, Application $silexApp){
		$purchaseOrderRequestFormatter = new InventoryPurchaseOrderRequestFormatter();
		$purchaseOrderController = new InventoryPurchaseOrderController();
		$postBodyArray = $purchaseOrderRequestFormatter->formatReceivePurchaseOrder(decodeRequest($request));
		$response = $purchaseOrderController->receivePurchaseOrders($postBodyArray);
		return $silexApp->json($response,getReturnStatusValue($response));
	}

	public function getAllPurchaseOrders(Application $silexApp, $request = ''){
		$purchaseOrderViewCompiler = new InventoryPurchaseOrderViewCompiler();
		$request = createRequestFromURLParams($request);
		$response = $purchaseOrderViewCompiler->getPurchaseOrderMainView($request);	
		return $silexApp->json($response,getReturnStatusValue($response));
	}

	public function getAllPurchaseOrderStatuses(Application $silexApp){
		$controller = new InventoryMiscController();
		$response = $controller->getAllPurchaseOrderStatuses();
		return $silexApp->json($response,getReturnStatusValue($response));
	}

	public function getCreatePurchaseOrderModalData(Application $silexApp){
		$purchaseOrderViewCompiler = new InventoryPurchaseOrderViewCompiler();
		$response = $purchaseOrderViewCompiler->getPurchaseOrderCreationWindowData();
		return $silexApp->json($response,getReturnStatusValue($response));
	}

	public function exportPurchaseOrders(Application $silexApp){
		$purchaseOrderViewCompiler = new InventoryPurchaseOrderViewCompiler();
		$response = $purchaseOrderViewCompiler->exportPurchaseOrders();
		return $silexApp->json($response,getReturnStatusValue($response));
	}
}