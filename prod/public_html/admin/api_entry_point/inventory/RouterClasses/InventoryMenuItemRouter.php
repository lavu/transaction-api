<?php
use Symfony\Component\HTTPFoundation\Request;
use Silex\Application;
require_once(__DIR__ . "/../InputValidation/InventoryMenuItemValidators.php");
/**
 * Created by PhpStorm.
 * User: Leif
 * Date: 7/20/17
 * Time: 9:41 AM
 */
class InventoryMenuItemRouter
{
	public function getMenuItemsByInventoryItemID($request, Application $silexApp){
		$request = createRequestFromURLParams($request);
		$menuItemFormatter = new InventoryMenuItemRequestFormatter();
		$menuItemController = new InventoryMenutItemController();
		$formattedRequest = $menuItemFormatter->formatGetMenuItemsByInventoryItemIDs($request);
		$response = $menuItemController->getMenuItemsByInventoryItemID($formattedRequest);
		return $silexApp->json($response,getReturnStatusValue($response));
	}

	public function getAllMenuItems(Application $silexApp){
		$menuItemController = new InventoryMenutItemController();
		$response = $menuItemController->getAllMenuItems();
		return $silexApp->json($response,getReturnStatusValue($response));
	}

}