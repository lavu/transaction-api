<?php
require_once(__DIR__ . "/../InputValidation/InventoryAuditValidators.php");
use Symfony\Component\HTTPFoundation\Request;
use Silex\Application;
/**
 * Created by PhpStorm.
 * User: Leif
 * Date: 5/23/17
 * Time: 11:36 AM
 */
class InventoryAuditRouter
{
	public function createWasteAudit(Request $request, Application $silexApp){
		$formatter = new InventoryAuditRequestFormatter();
		$controller = new InventoryAuditController();
		$formattedRequest = $formatter->formatCreateAuditRequest(decodeRequest($request), 'waste');

		$validator = new WasteAuditValidator($silexApp, $formattedRequest);
		$violations = $validator->validate();
		if($violations != null){
			return $silexApp->json($violations, getReturnStatusValue(BAD_REQUEST));
		}

		$response = $controller->createWasteAudit($formattedRequest);
		return $silexApp->json($response,getReturnStatusValue($response));
	}
	public function createReconciliationAudit(Request $request, Application $silexApp){
		$formatter = new InventoryAuditRequestFormatter();
		$controller = new InventoryAuditController();
		$formattedRequest = $formatter->formatCreateAuditRequest(decodeRequest($request), 'reconciliation');

		$validator = new ReconciliationAuditValidator($silexApp, $formattedRequest);
		$violations = $validator->validate();
		if($violations != null){
			return $silexApp->json($violations, getReturnStatusValue(BAD_REQUEST));
		}

		$response = $controller->createReconciliationAudit($formattedRequest);
		return $silexApp->json($response,getReturnStatusValue($response));
	}

	public function createReceiveWithoutPOAudit(Request $request, Application $silexApp){
		$formatter = new InventoryAuditRequestFormatter();
		$controller = new InventoryAuditController();
		$formattedRequest = $formatter->formatCreateAuditRequest(decodeRequest($request), 'receiveWithoutPO');

		$validator = new ReceiveWithoutPOAuditValidator($silexApp, $formattedRequest);
		$violations = $validator->validate();
		if($violations != null){
			return $silexApp->json($violations, getReturnStatusValue(BAD_REQUEST));
		}

		$response = $controller->createReceiveWithoutPOAudit($formattedRequest);
		return $silexApp->json($response,getReturnStatusValue($response));
	}

    public function createAudits(Request $request, Application $silexApp){
        $formatter = new InventoryAuditRequestFormatter();
        $controller = new InventoryAuditController();
        $formattedRequest = $formatter->formatCreateAuditServiceRequest(decodeRequest($request));

        $validator = new CreateAuditValidator($silexApp, $formattedRequest);
        $violations = $validator->validate();
        if($violations != null){
            return $silexApp->json($violations, getReturnStatusValue(BAD_REQUEST));
        }

        $response = $controller->createAudits($formattedRequest);
        return $silexApp->json($response,getReturnStatusValue($response));
    }

    public function getAudits(Application $silexApp){
        $controller = new InventoryAuditController();
        $response = $controller->getAudits();
        return $silexApp->json($response,getReturnStatusValue($response));
    }

    public function getCriteriaAudits(Request $request, Application $silexApp){
        $formatter = new InventoryAuditRequestFormatter();
        $controller = new InventoryAuditController();
        $formattedRequest = $formatter->formatGetAuditsServiceRequest(decodeRequest($request));

        $validator = new CreateAuditValidator($silexApp, $formattedRequest);
        $violations = $validator->validate();
        if($violations != null){
            return $silexApp->json($violations, getReturnStatusValue(BAD_REQUEST));
        }

        $response = $controller->getCriteriaAudits($formattedRequest);
        return $silexApp->json($response,getReturnStatusValue($response));
    }

}