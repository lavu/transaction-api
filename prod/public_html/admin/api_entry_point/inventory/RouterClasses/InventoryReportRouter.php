<?php
use Symfony\Component\HTTPFoundation\Request;
use Silex\Application;
require_once(__DIR__ . "/../InputValidation/InventoryReportValidators.php");

class InventoryReportRouter
{
	public function getFoodAndBeverageReport(Request $request, Application $silexApp) {
		$compiler = new InventoryReportCompiler();
		$postBodyArray = decodeRequest($request);
		$response = $compiler->getFoodAndBeverageReport($postBodyArray);
		return $silexApp->json($response, getReturnStatusValue($response));
	}

	public function getUsageReport(Request $request, Application $silexApp) {
		$compiler = new InventoryReportCompiler();
		$postBodyArray = decodeRequest($request);
		$response = $compiler->getUsageReport($postBodyArray);
		return $silexApp->json($response, getReturnStatusValue($response));
	}

	public function getLowInventoryReport(Request $request, Application $silexApp) {
		$compiler = new InventoryReportCompiler();
		$response = $compiler->getLowInventoryReport();
		return $silexApp->json($response, getReturnStatusValue());
	}

	public function getWasteReport(Request $request, Application $silexApp) {
		$compiler = new InventoryReportCompiler();
		$postBodyArray = decodeRequest($request);
		$response = $compiler->getWasteReport($postBodyArray);
		return $silexApp->json($response, getReturnStatusValue($response));
	}

	public function getTransferReport(Request $request, Application $silexApp) {
		$compiler = new InventoryReportCompiler();
		$postBodyArray = decodeRequest($request);
		$response = $compiler->getTransferReport($postBodyArray);
		return $silexApp->json($response, getReturnStatusValue($response));
	}

	public function getVarianceReport(Request $request, Application $silexApp) {
		$compiler = new InventoryReportCompiler();
		$postBodyArray = decodeRequest($request);
		$response = $compiler->getVarianceReport($postBodyArray);
		return $silexApp->json($response, getReturnStatusValue($response));
	}
}