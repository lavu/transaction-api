<?php
use Symfony\Component\HTTPFoundation\Request;
use Silex\Application;
require_once(__DIR__ . "/../InputValidation/InventoryTransferValidators.php");

/**
 * Created by PhpStorm.
 * User: Leif
 * Date: 5/22/17
 * Time: 5:05 PM
 */
class InventoryTransferRouter
{

	public function getTransfersByID($request, Application $silexApp){
		$request = createRequestFromURLParams($request);
		$compiler = new InventoryTransferViewCompiler();
		$response = $compiler->getTransfersByID($request);
		return $silexApp->json($response,getReturnStatusValue($response));
	}

	public function updateTransfers(Request $request, Application $silexApp){
		$formatter = new InventoryTransferRequestFormatter();
		$controller = new InventoryTransferController();
		$formattedRequest = $formatter->formatUpdateTransfer(decodeRequest($request));
		$response = $controller->updateTransfers($formattedRequest);
		return $silexApp->json($response,getReturnStatusValue($response));
	}

	public function createTransfers(Request $request, Application $silexApp){
		$transferFormatter = new InventoryTransferRequestFormatter();
		$transferController = new InventoryTransferController();
		$postBodyArray = $transferFormatter->formatCreateTransfers(decodeRequest($request));
		$response = $transferController->createTransfers($postBodyArray);
		return $silexApp->json($response,getReturnStatusValue($response));
	}

	public function copyTransfers(Request $request, Application $silexApp){
		$transferFormatter = new InventoryTransferRequestFormatter();
		$transferController = new InventoryTransferController();
		$postBodyArray = $transferFormatter->formatCopyTransfers(decodeRequest($request));
		$response = $transferController->copyTransfers($postBodyArray);
		return $silexApp->json($response,getReturnStatusValue($response));
	}

	public function receiveTransfers(Request $request, Application $silexApp){
		$compiler = new InventoryTransferViewCompiler();
		$response = $compiler->receiveTransfers(decodeRequest($request));
		return $silexApp->json($response,getReturnStatusValue($response));
	}

	public function importTransfers(Request $request, Application $silexApp){
		$transferFormatter = new InventoryTransferRequestFormatter();
		$transferController = new InventoryTransferController();

		$postBodyArray = $transferFormatter->formatImportTransfers(decodeRequest($request));
		$response = $transferController->importTransfers($postBodyArray);
		return $silexApp->json($response,getReturnStatusValue($response));
	}

	public function exportTransfers(Request $request, Application $silexApp){
		$transferFormatter = new InventoryTransferRequestFormatter();
		$transferController = new InventoryTransferController();

		$postBodyArray = $transferFormatter->formatExportTransfers(decodeRequest($request));
		$response = $transferController->exportTransfers($postBodyArray);
		return $silexApp->json($response,getReturnStatusValue($response));
	}

	public function getAllTransferViewRows(Application $silexApp, $request = ''){
		$transferViewCompiler = new InventoryTransferViewCompiler();
		//$request = createRequestFromURLParams($request);
		$request = $_POST;
		$response = $transferViewCompiler->getAllTransferViewRows($request);	
		return $silexApp->json($response,getReturnStatusValue($response));
	}

	public function getAllTransferStatuses(Application $silexApp){
		$controller = new InventoryMiscController();
		$response = $controller->getAllTransferStatuses();
		return $silexApp->json($response,getReturnStatusValue($response));
	}

	public function getAllReceivedTransfers(Application $silexApp){
		$controller = new InventoryTransferController();
		$response = $controller->getAllReceivedTransfers();
		return $silexApp->json($response,getReturnStatusValue($response));
	}

	public function getAllPendingTransfers(Application $silexApp){
		$controller = new InventoryTransferController();
		$response = $controller->getAllPendingTransfers();
		return $silexApp->json($response,getReturnStatusValue($response));
	}

	public function getAllTransferLocations(Application $silexApp){
		$controller = new InventoryTransferController();
		$response = $controller->getAllTransferLocations();
		return $silexApp->json($response,getReturnStatusValue($response));
	}

	public function exportCSVTransfers(Application $silexApp){
		$purchaseOrderViewCompiler = new InventoryTransferViewCompiler();
		$response = $purchaseOrderViewCompiler->exportCSVTransfers();
		return $silexApp->json($response,getReturnStatusValue($response));
	}
}