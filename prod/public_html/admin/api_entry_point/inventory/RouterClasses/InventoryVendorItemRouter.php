<?php
use Symfony\Component\HTTPFoundation\Request;
use Silex\Application;
require_once(__DIR__ . "/../InputValidation/InventoryVendorItemValidators.php");
/**
 * Created by PhpStorm.
 * User: Leif
 * Date: 5/23/17
 * Time: 10:03 AM
 */
class InventoryVendorItemRouter
{
	public function createVendorItems(Request $request, Application $silexApp){
		$vendorItemRequestFormatter = new InventoryVendorItemRequestFormatter();
		$vendorItemController = new InventoryVendorItemController();
		$postBodyArray = $vendorItemRequestFormatter->formatCreateVendorItems(decodeRequest($request));
		$response = $vendorItemController->createVendorItems($postBodyArray);
		return $silexApp->json($response,getReturnStatusValue($response));
	}

	public function linkVendorItemToInventoryItem(Request $request, Application $silexApp){
		$vendorItemRequestFormatter = new InventoryVendorItemRequestFormatter();
		$vendorItemController = new InventoryVendorItemController();
		$postBodyArray = $vendorItemRequestFormatter->formatLinkVendorItemsToInventoryItems(decodeRequest($request));
		$response = $vendorItemController->linkVendorItemsToInventoryItems($postBodyArray);
		return $silexApp->json($response,getReturnStatusValue($response));
	}

	public function getLinkedVendorItemsForInventoryItems($request, Application $silexApp){
		$request = createRequestFromURLParams($request);
		$inventoryVendorItemController = new InventoryVendorItemController();
		$response = $inventoryVendorItemController->getLinkedVendorItemsForInventoryItems($request);
		return $silexApp->json($response,getReturnStatusValue($response));
	}

	public function getVendorItemsByVendorID($request, Application $silexApp){
		$request = createRequestFromURLParams($request);
		$inventoryVendorItemController = new InventoryVendorItemController();
		$response = $inventoryVendorItemController->getVendorItemsByVendorID($request);
		return $silexApp->json($response,getReturnStatusValue($response));
	}

	public function updateVendorItems(Request $request, Application $silexApp){
		$vendorItemRequestFormatter = new InventoryVendorItemRequestFormatter();
		$vendorItemController = new InventoryVendorItemController();
		$postBodyArray = $vendorItemRequestFormatter->formatUpdateVendorItems(decodeRequest($request));
		$response = $vendorItemController->updateVendorItems($postBodyArray);
		return $silexApp->json($response,getReturnStatusValue($response));
	}

	public function getVendorItems($silexApp){
	    $vendorItemController = new InventoryVendorItemController();
	    $response = $vendorItemController->getAllVendorItems();
	    return $silexApp->json($response,getReturnStatusValue($response));
    }
}