<?php
use Symfony\Component\HTTPFoundation\Request;
use Silex\Application;
require_once(__DIR__ . "/../InputValidation/InventoryCategoryValidators.php");

/**
 * Created by PhpStorm.
 * User: Leif
 * Date: 5/23/17
 * Time: 10:34 AM
 */
class InventoryCategoryRouter
{
	public function createCategories(Request $request, Application $silexApp){
		$categoryRequestFormatter = new InventoryCategoryRequestFormatter();
		$categoryController = new InventoryCategoryController();
		$postBodyArray = $categoryRequestFormatter->formatCreateInventoryCategories(decodeRequest($request));
		$response = $categoryController->createInventoryCategories($postBodyArray);
		return $silexApp->json($response,getReturnStatusValue($response));
	}

	public function getAllCategories(Application $silexApp){
		$categoryController = new InventoryCategoryController();
		$response = $categoryController->getAllCategories();
		return $silexApp->json($response,getReturnStatusValue($response));
	}
}