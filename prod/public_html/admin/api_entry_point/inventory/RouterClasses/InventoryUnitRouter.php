<?php
use Symfony\Component\HTTPFoundation\Request;
use Silex\Application;
require_once(__DIR__ . "/../InputValidation/InventoryUnitValidators.php");

/**
 * Created by PhpStorm.
 * User: Leif
 * Date: 5/23/17
 * Time: 10:47 AM
 */
class InventoryUnitRouter
{
	public function createUnits(Request $request, Application $silexApp){
		$unitFormatter = new InventoryUnitRequestFormatter();
		$unitController = new InventoryUnitController();

		$postBodyArray = $unitFormatter->formatCreateUnits(decodeRequest($request));
		$response = $unitController->createUnits($postBodyArray);
		return $silexApp->json($response,getReturnStatusValue($response));
	}

	public function createUnitCategories(Request $request, Application $silexApp){
		$unitCategoryFormatter = new InventoryUnitCategoryRequestFormatter();
		$unitCategoryController = new InventoryUnitCategoryController();

		$postBodyArray = $unitCategoryFormatter->formatCreateUnitCategories(decodeRequest($request));
		$response = $unitCategoryController->createUnitCategories($postBodyArray);
		return $silexApp->json($response,getReturnStatusValue($response));
	}

	public function getAllUnits(Application $silexApp){
		$unitController = new InventoryUnitController();
		$response = $unitController->getAllUnits();
		return $silexApp->json($response,getReturnStatusValue($response));
	}

	public function getAllPurchaseUnitsOfMeasure(Application $silexApp){
		$purchaseOrderItemController = new InventoryPurchaseOrderItemController();
		$response = $purchaseOrderItemController->getAllPurchaseUnitsOfMeasure();
		return $silexApp->json($response,getReturnStatusValue($response));
	}

	public function getAllSalesUnitsOfMeasure(Application $silexApp){
		$itemController = new InventoryItemController();
		$response = $itemController->getAllSalesUnitsOfMeasure();
		return $silexApp->json($response,getReturnStatusValue($response));
	}

    public function getAllUnitsByCategory($request, Application $silexApp){
        $request = createRequestFromURLParams($request);
        $unitController = new InventoryUnitController();

        $response = $unitController->getAllUnitsByCategory($request);
        return $silexApp->json($response, getReturnStatusValue($response));
    }

    public function getCustomUnitsOfMeasure(Application $silexApp){
	    $unitController = new InventoryunitController();
	    $response = $unitController->getCustomUnitsOfMeasure();
	    return $silexApp->json($response,getReturnStatusValue($response));
    }
}