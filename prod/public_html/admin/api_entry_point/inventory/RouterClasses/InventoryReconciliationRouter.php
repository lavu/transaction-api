<?php
use Symfony\Component\HTTPFoundation\Request;
use Silex\Application;
require_once(__DIR__ . "/../InputValidation/InventoryReconciliationValidators.php");

/**
 * Created by PhpStorm.
 * User: Leif
 * Date: 5/23/17
 * Time: 11:36 AM
 */
class InventoryReconciliationRouter
{
	public function performReconciliation(Request $request, Application $silexApp){
		$reconciliationCompiler = new InventoryReconciliationViewCompiler();
		$response = $reconciliationCompiler->performReconciliation(decodeRequest($request));
		return $silexApp->json($response,getReturnStatusValue($response));
	}

	public function getAllReconciliationRows(Application $silexApp, $request = ''){
		$reconciliationViewCompiler = new InventoryReconciliationViewCompiler();
		$request = createRequestFromURLParams($request);
		$response = $reconciliationViewCompiler->getReconciliationRows($request);
		return $silexApp->json($response,getReturnStatusValue($response));
	}

	public function exportReconciliation(Application $silexApp){
		$reconciliationViewCompiler = new InventoryReconciliationViewCompiler();
		$response = $reconciliationViewCompiler->exportReconciliation();
		return $silexApp->json($response,getReturnStatusValue($response));
	}
}