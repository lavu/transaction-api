<?php
use Symfony\Component\HTTPFoundation\Request;
use Silex\Application;
require_once(__DIR__ . "/../InputValidation/InventoryItemValidators.php");

/**
 * Created by PhpStorm.
 * User: Leif
 * Date: 5/22/17
 * Time: 5:00 PM
 */
class InventoryItemRouter
{
	public function wasteInventoryItems(Request $request, Application $silexApp){
		$wasteCompiler = new InventoryWasteViewCompiler();
		$response = $wasteCompiler->performInventoryWaste(decodeRequest($request));
		return $silexApp->json($response,getReturnStatusValue($response));
	}

	public function createInventoryItems(Request $request, Application $silexApp){
		$inventoryItemRequestFormatter = new InventoryItemRequestFormatter();
		$inventoryItemController = new InventoryItemController();
		$postBodyArray = $inventoryItemRequestFormatter->formatCreateInventoryItems(decodeRequest($request));
		$response = $inventoryItemController->createInventoryItems($postBodyArray);
		return $silexApp->json($response,getReturnStatusValue($response));
	}

	public function copyInventoryItems(Request $request, Application $silexApp){
		$inventoryItemRequestFormatter = new InventoryItemRequestFormatter();
		$manageViewCompiler = new InventoryManageViewCompiler();
		$postBodyArray = $inventoryItemRequestFormatter->formatCopyInventoryItems(decodeRequest($request));
		$response = $manageViewCompiler->copyInventoryItems($postBodyArray);
		return $silexApp->json($response,getReturnStatusValue($response));
	}

	public function updateInventoryItems(Request $request, Application $silexApp){
		$inventoryItemRequestFormatter = new InventoryItemRequestFormatter();
		$inventoryItemController = new InventoryItemController();
		$postBodyArray = $inventoryItemRequestFormatter->formatUpdateInventoryItems(decodeRequest($request));
		$response = $inventoryItemController->updateInventoryItems($postBodyArray);
		return $silexApp->json($response,getReturnStatusValue($response));
	}

	public function archiveInventoryItems($request, Application $silexApp){
		$request = createRequestFromURLParams($request);
		$inventoryItemRequestFormatter = new InventoryItemRequestFormatter();
		$inventoryItemController = new InventoryItemController();
		$postBodyArray = $inventoryItemRequestFormatter->formatArchiveInventoryItems($request);
		$response = $inventoryItemController->archiveInventoryItems($postBodyArray);
		return $silexApp->json($response,getReturnStatusValue($response));
	}

	public function receiveWithoutPurchaseOrders(Request $request, Application $silexApp){
		$manageViewCompiler = new InventoryManageViewCompiler();
		$response = $manageViewCompiler->receiveWithoutPurchaseOrder(decodeRequest($request));
		return $silexApp->json($response,getReturnStatusValue($response));
	}

	public function createManageViewItem(Request $request, Application $silexApp){
		$request = decodeRequest($request);
		$vendorItemRequestFormatter = new InventoryVendorItemRequestFormatter();
		$formattedRequest = $vendorItemRequestFormatter->isLinkingItem($request);
		if( $formattedRequest !== false ){ //TODO allow multiple items to link/create
			$vic = new InventoryVendorItemController();
			$response = $vic->createVendorItems($formattedRequest);
		}else
		{
			$manageViewCompiler = new InventoryManageViewCompiler();
			$response = $manageViewCompiler->createManageViewAddItem($request);
		}
		return $silexApp->json($response,getReturnStatusValue($response));
	}

	public function updateManageViewItem(Request $request, Application $silexApp){
		$itemFormatter = new InventoryItemRequestFormatter();
		$itemController = new InventoryItemController();
		$formattedInventoryItemRequest = $itemFormatter->formatUpdateInventoryItems(decodeRequest($request));
		$response = $itemController->updateInventoryItems($formattedInventoryItemRequest);
		return $silexApp->json($response,getReturnStatusValue($response));
	}

	public function updateInventoryItemQuantityOnHand(Request $request, Application $silexApp)
	{
		$inventoryItemRequestFormatter = new InventoryItemRequestFormatter();
		$inventoryItemController = new InventoryItemController();
		$request = $inventoryItemRequestFormatter->formatAddQuantityToInventoryItems(decodeRequest($request));
		$response = $inventoryItemController->addQuantitiesToInventoryItems($request);
		return $silexApp->json($response,getReturnStatusValue($response));
	}

	/**GETTERS*/
	public function getInventoryItemsByID($request, Application $silexApp){
		$request = createRequestFromURLParams($request);
		$manageViewCompiler = new InventoryManageViewCompiler();
		$response = $manageViewCompiler->getInventoryItemDrillDownsByID($request);
		return $silexApp->json($response,getReturnStatusValue($response));
	}

    public function getLinkedItemsByID($request, Application $silexApp){
	    $request = createRequestFromURLParams($request);
        $manageViewCompiler = new InventoryManageViewCompiler();
	    $response = $manageViewCompiler->getInventoryItemLinkedItems($request);
	    return $silexApp->json($response, getReturnStatusValue($response));
    }


    public function getLegacyItems(Application $silexApp){
        $itemController = new InventoryItemController();
        $response = $itemController->getLegacyItems();
        return $silexApp->json($response, getReturnStatusValue($response));
    }

	public function getExternalInventoryItems($request, Application $silexApp){
		$request = createRequestFromURLParams($request);
		$itemController = new InventoryItemController();
		$response = $itemController->getExternalInventoryItems($request);
		return $silexApp->json($response,getReturnStatusValue($response));
	}

	public function getAllManageViewRows(Application $silexApp, $request = ''){
		$manageViewCompiler = new InventoryManageViewCompiler();
		$request = createRequestFromURLParams($request);
		$response = $manageViewCompiler->getAllManageViewRows(false,$request);
		return $silexApp->json($response,getReturnStatusValue($response));
	}

	public function getAllInventoryBySearch(Application $silexApp){
		$manageViewCompiler = new InventoryManageViewCompiler();
		$request = $_POST;
		$response = $manageViewCompiler->getAllManageViewRows(false,$request);
		return $silexApp->json($response,getReturnStatusValue($response));
	}

	public function getAllInventoryItemCounts(Application $silexApp){
		$itemController = new InventoryItemController();
		$response = $itemController->getAllInventoryItemCounts();
		return $silexApp->json($response,getReturnStatusValue($response));
	}

	public function getAllInventoryItems(Application $silexApp){
		$inventoryItemController = new InventoryItemController();
		$response = $inventoryItemController->getAllInventoryItems();
		return $silexApp->json($response,getReturnStatusValue($response));
	}

	public function getAllArchivedInventoryItems(Application $silexApp){
		$manageViewCompiler = new InventoryManageViewCompiler();
		$response = $manageViewCompiler->getAllManageViewRows(true);
		return $silexApp->json($response,getReturnStatusValue($response));
	}

	public function getCostData(Application $silexApp){
		$inventoryItemController = new InventoryItemController();
		$response = $inventoryItemController->getCostData();
		return $silexApp->json($response,getReturnStatusValue($response));
	}

	public function getWasteCostData(Application $silexApp){
		$inventoryItemController = new InventoryItemController();
		$response = $inventoryItemController->getWasteCostData();
		return $silexApp->json($response,getReturnStatusValue($response));
	}

	public function pauseMigration($request, Application $silexApp){
	    $inventoryItemController = new InventoryItemController();
	    $response = $inventoryItemController->pauseMigration(decodeRequest($request));
	    return $silexApp->json($response, getReturnStatusValue($response));
    }

    public function getResumeMigration(Application $silexApp){
        $inventoryItemController = new InventoryItemController();
        $response = $inventoryItemController->resumeMigration();
        return $silexApp->json($response, getReturnStatusValue($response));
    }

    public function finalizeMigration($request, Application $silexApp){
        $inventoryItemController = new InventoryItemController();
        $response = $inventoryItemController->finalizeMigration(decodeRequest($request));
        return $silexApp->json($response, getReturnStatusValue($response));
    }

    public function startMigration($request, Application $silexApp){
        $inventoryItemController = new InventoryItemController();
        $response = $inventoryItemController->startMigration(decodeRequest($request));
        return $silexApp->json($response, getReturnStatusValue($response));
    }

    public function calculateValueForItems($request, Application $silexApp){
        $inventoryItemController = new InventoryItemController();
        $response = $inventoryItemController->calculateValueForItems(decodeRequest($request));
        return $silexApp->json($response, getReturnStatusValue($response));
    }

    public function calculateTotalValue($request, Application $silexApp){
        $inventoryItemController = new InventoryItemController();
        $response = $inventoryItemController->calculateTotalValue(decodeRequest($request));
        return $silexApp->json($response, getReturnStatusValue($response));
    }

	public function deleteAllInventory(Application $silexApp){
		$inventoryItemController = new InventoryItemController();
		$response = $inventoryItemController->deleteAllInventory();
		return $silexApp->json($response, getReturnStatusValue($response));
	}

	public function exportInventoryItems(Application $silexApp){
		$manageViewCompiler = new InventoryManageViewCompiler();
		$response = $manageViewCompiler->exportInventoryItems();
		return $silexApp->json($response,getReturnStatusValue($response));
	}
}