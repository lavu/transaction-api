<?php
use Symfony\Component\HTTPFoundation\Request;
use Silex\Application;
require_once(__DIR__ . "/../InputValidation/Inventory86CountValidators.php");
/**
 * Created by PhpStorm.
 * User: Hayat
 * Date: 06/09/17
 * Time: 11:37 PM
 */
class Inventory86CountRouter
{
	public function create86CountMenuItems(Request $request, Application $silexApp){
        $formatter = new Inventory86CountRequestFormatter();
        $controller = new Inventory86CountController();
        $formattedRequest = $formatter->formatCreate86CountMenuItems(decodeRequest($request));

        $validator = new inventory86CountMenuItemValidator($silexApp, $formattedRequest);
        $violations = $validator->validate();
        if($violations != null){
            return $silexApp->json($violations, getReturnStatusValue(BAD_REQUEST));
        }
		$response = $controller->createInventory86CountMenuItems($formattedRequest);
		return $silexApp->json($response,getReturnStatusValue($response));
	}

    public function create86CountModifiers(Request $request, Application $silexApp){
        $formatter = new Inventory86CountRequestFormatter();
        $controller = new Inventory86CountController();
        $formattedRequest = $formatter->formatCreate86CountModifiers(decodeRequest($request));

        $validator = new inventory86CountModifierValidator($silexApp, $formattedRequest);
        $violations = $validator->validate();
        if($violations != null){
            return $silexApp->json($violations, getReturnStatusValue(BAD_REQUEST));
        }
        $response = $controller->createInventory86CountModifiers($formattedRequest);
        return $silexApp->json($response,getReturnStatusValue($response));
    }

    public function create86CountForcedModifiers(Request $request, Application $silexApp){
        $formatter = new Inventory86CountRequestFormatter();
        $controller = new Inventory86CountController();
        $formattedRequest = $formatter->formatCreate86CountForcedModifiers(decodeRequest($request));

        $validator = new inventory86CountForcedModifierValidator($silexApp, $formattedRequest);
        $violations = $validator->validate();
        if($violations != null){
            return $silexApp->json($violations, getReturnStatusValue(BAD_REQUEST));
        }
        $response = $controller->createInventory86CountForcedModifiers($formattedRequest);
        return $silexApp->json($response,getReturnStatusValue($response));
    }

    public function getMenuItemStatus($request, Application $silexApp){
	    $response = array();
	    if ($request != '') {
		    $count86ViewCompiler = new Inventory86CountViewCompiler();
		    $response = $count86ViewCompiler->getMenuItemStatus($request);
        }

        return $silexApp->json($response,getReturnStatusValue($response));
    }

    public function getMenuItemsStatus(Application $silexApp){
        $controller = new Inventory86CountController();
        $response = $controller->getMenuItemsStatus(array());
        return $silexApp->json($response,getReturnStatusValue($response));
    }

    public function getModifierStatus($request, Application $silexApp){
	    $response = array();
	    if ($request != '') {
		    $count86ViewCompiler = new Inventory86CountViewCompiler();
		    $response = $count86ViewCompiler->getModifierStatus($request);
	    }

	    return $silexApp->json($response,getReturnStatusValue($response));
    }

    public function getModifiersStatus(Application $silexApp){
        $controller = new Inventory86CountController();
        $response = $controller->getModifiersStatus(array());
        return $silexApp->json($response,getReturnStatusValue($response));
    }

    public function getForcedModifierStatus($request, Application $silexApp){
	    $response = array();
	    if ($request != '') {
		    $count86ViewCompiler = new Inventory86CountViewCompiler();
		    $response = $count86ViewCompiler->getForcedModifierStatus($request);
	    }

	    return $silexApp->json($response,getReturnStatusValue($response));
    }

    public function getForcedModifiersStatus(Application $silexApp){
        $controller = new Inventory86CountController();
        $response = $controller->getForcedModifiersStatus(array());
        return $silexApp->json($response,getReturnStatusValue($response));
    }

    public function deductMenuQuantity(Request $request, Application $silexApp){
        $formatter = new Inventory86CountRequestFormatter();
        $controller = new Inventory86CountController();
        $formattedRequest = $formatter->formatDeduct86CountQuantity(decodeRequest($request));

        $validator = new inventoryDeduct86CountQuantityValidator($silexApp, $formattedRequest);
        $violations = $validator->validate();
        if($violations != null){
            return $silexApp->json($violations, getReturnStatusValue(BAD_REQUEST));
        }
        $response = $controller->deductMenuItemQuantity($formattedRequest);
        return $silexApp->json($response,getReturnStatusValue($response));
    }

    public function deductModifierQuantity(Request $request, Application $silexApp){
        $formatter = new Inventory86CountRequestFormatter();
        $controller = new Inventory86CountController();
        $formattedRequest = $formatter->formatDeduct86CountQuantity(decodeRequest($request));

        $validator = new inventoryDeduct86CountQuantityValidator($silexApp, $formattedRequest);
        $violations = $validator->validate();
        if($violations != null){
            return $silexApp->json($violations, getReturnStatusValue(BAD_REQUEST));
        }
        $response = $controller->deductModifierItemQuantity($formattedRequest);
        return $silexApp->json($response,getReturnStatusValue($response));
    }

    public function deductForcedModifierQuantity(Request $request, Application $silexApp){
        $formatter = new Inventory86CountRequestFormatter();
        $controller = new Inventory86CountController();
        $formattedRequest = $formatter->formatDeduct86CountQuantity(decodeRequest($request));

        $validator = new inventoryDeduct86CountQuantityValidator($silexApp, $formattedRequest);
        $violations = $validator->validate();
        if($violations != null){
            return $silexApp->json($violations, getReturnStatusValue(BAD_REQUEST));
        }
        $response = $controller->deductForcedModifierItemQuantity($formattedRequest);
        return $silexApp->json($response,getReturnStatusValue($response));
    }

    public function get86CountMenuItem($request, Application $silexApp){
        $request = createRequestFromURLParams($request);
        $formatter = new Inventory86CountRequestFormatter();
        $controller = new Inventory86CountController();
        $formattedRequest = $formatter->formatIds($request);
        $response = $controller->get86CountMenuItem($formattedRequest);
        return $silexApp->json($response,getReturnStatusValue($response));
    }

    public function get86CountModifier($request, Application $silexApp){
        $request = createRequestFromURLParams($request);
        $formatter = new Inventory86CountRequestFormatter();
        $controller = new Inventory86CountController();
        $formattedRequest = $formatter->formatIds($request);
        $response = $controller->get86CountModifier($formattedRequest);
        return $silexApp->json($response,getReturnStatusValue($response));
    }

    public function get86CountForcedModifier($request, Application $silexApp){
        $request = createRequestFromURLParams($request);
        $formatter = new Inventory86CountRequestFormatter();
        $controller = new Inventory86CountController();
        $formattedRequest = $formatter->formatIds($request);
        $response = $controller->get86CountForcedModifier($formattedRequest);
        return $silexApp->json($response,getReturnStatusValue($response));
    }

    public function delete86CountRowsMenuItems(Request $request, Application $silexApp){
        $formatter = new Inventory86CountRequestFormatter();
        $controller = new Inventory86CountController();
        $formattedRequest = $formatter->formatMenuItemsStatus(decodeRequest($request));

        $validator = new inventory86CountDeleteValidator($silexApp, $formattedRequest);
        $violations = $validator->validate();
        if($violations != null){
            return $silexApp->json($violations, getReturnStatusValue(BAD_REQUEST));
        }
        $response = $controller->delete86CountRowsMenuItems($formattedRequest);
        return $silexApp->json($response,getReturnStatusValue($response));
    }

    public function delete86CountRowsModifiers(Request $request, Application $silexApp){
        $formatter = new Inventory86CountRequestFormatter();
        $controller = new Inventory86CountController();
        $formattedRequest = $formatter->formatModifiersStatus(decodeRequest($request));

        $validator = new inventory86CountDeleteValidator($silexApp, $formattedRequest);
        $violations = $validator->validate();
        if($violations != null){
            return $silexApp->json($violations, getReturnStatusValue(BAD_REQUEST));
        }
        $response = $controller->delete86CountRowsModifiers($formattedRequest);
        return $silexApp->json($response,getReturnStatusValue($response));
    }

    public function delete86CountRowsForcedModifiers(Request $request, Application $silexApp){
        $formatter = new Inventory86CountRequestFormatter();
        $controller = new Inventory86CountController();
        $formattedRequest = $formatter->formatForcedModifiersStatus(decodeRequest($request));

        $validator = new inventory86CountDeleteValidator($silexApp, $formattedRequest);
        $violations = $validator->validate();
        if($violations != null){
            return $silexApp->json($violations, getReturnStatusValue(BAD_REQUEST));
        }
        $response = $controller->delete86CountRowsForcedModifiers($formattedRequest);
        return $silexApp->json($response,getReturnStatusValue($response));
    }

    public function delete86CountMenuItems(Request $request, Application $silexApp){
        $formatter = new Inventory86CountRequestFormatter();
        $controller = new Inventory86CountController();
        $formattedRequest = $formatter->formatMenuItemsStatus(decodeRequest($request));

        $validator = new inventory86CountDeleteValidator($silexApp, $formattedRequest);
        $violations = $validator->validate();
        if($violations != null){
            return $silexApp->json($violations, getReturnStatusValue(BAD_REQUEST));
        }
        $response = $controller->delete86CountMenuItems($formattedRequest);
        return $silexApp->json($response,getReturnStatusValue($response));
    }

    public function delete86CountModifiers(Request $request, Application $silexApp){
        $formatter = new Inventory86CountRequestFormatter();
        $controller = new Inventory86CountController();
        $formattedRequest = $formatter->formatModifiersStatus(decodeRequest($request));

        $validator = new inventory86CountDeleteValidator($silexApp, $formattedRequest);
        $violations = $validator->validate();
        if($violations != null){
            return $silexApp->json($violations, getReturnStatusValue(BAD_REQUEST));
        }
        $response = $controller->delete86CountModifiers($formattedRequest);
        return $silexApp->json($response,getReturnStatusValue($response));
    }

    public function delete86CountForcedModifiers(Request $request, Application $silexApp){
        $formatter = new Inventory86CountRequestFormatter();
        $controller = new Inventory86CountController();
        $formattedRequest = $formatter->formatForcedModifiersStatus(decodeRequest($request));

        $validator = new inventory86CountDeleteValidator($silexApp, $formattedRequest);
        $violations = $validator->validate();
        if($violations != null){
            return $silexApp->json($violations, getReturnStatusValue(BAD_REQUEST));
        }
        $response = $controller->delete86CountForcedModifiers($formattedRequest);
        return $silexApp->json($response,getReturnStatusValue($response));
    }

    public function update86CountMenuItems(Request $request, Application $silexApp){
        $formatter = new Inventory86CountRequestFormatter();
        $controller = new Inventory86CountController();
        $formattedRequest = $formatter->formatUpdate86CountMenuItems(decodeRequest($request));
        $response = $controller->update86CountMenuItems($formattedRequest);
        return $silexApp->json($response,getReturnStatusValue($response));
    }

    public function update86CountModifiers(Request $request, Application $silexApp){
        $formatter = new Inventory86CountRequestFormatter();
        $controller = new Inventory86CountController();
        $formattedRequest = $formatter->formatUpdate86CountModifiers(decodeRequest($request));
        $response = $controller->update86CountModifiers($formattedRequest);
        return $silexApp->json($response,getReturnStatusValue($response));
    }

    public function update86CountForcedModifiers(Request $request, Application $silexApp){
        $formatter = new Inventory86CountRequestFormatter();
        $controller = new Inventory86CountController();
        $formattedRequest = $formatter->formatUpdate86CountForcedModifiers(decodeRequest($request));
        $response = $controller->update86CountForcedModifiers($formattedRequest);
        return $silexApp->json($response,getReturnStatusValue($response));
    }

    public function updateReserveQuantity(Request $request, Application $silexApp){
        $count86ViewCompiler = new Inventory86CountViewCompiler();
        $response = $count86ViewCompiler->updateReserveQuantity(decodeRequest($request));
        return $silexApp->json($response,getReturnStatusValue($response));
    }

    public function updateDeliverQuantity(Request $request, Application $silexApp){
        $count86ViewCompiler = new Inventory86CountViewCompiler();
        $response = $count86ViewCompiler->updateDeliverQuantity(decodeRequest($request));
        return $silexApp->json($response,getReturnStatusValue($response));
    }

	public function updateQuantityOnHand(Request $request, Application $silexApp){
		$count86ViewCompiler = new Inventory86CountViewCompiler();
		$response = $count86ViewCompiler->updateQuantityOnHand(decodeRequest($request));
		return $silexApp->json($response,getReturnStatusValue($response));
	}

    public function getMenuStatus(Request $request, Application $silexApp){
        $count86ViewCompiler = new Inventory86CountViewCompiler();
        $response = $count86ViewCompiler->getMenuStatus(decodeRequest($request));
        return $silexApp->json($response,getReturnStatusValue($response));
    }

    public function getMenuItemLowQuantityStatus($request, Application $silexApp){
        $request = createRequestFromURLParams($request);
        $formatter = new Inventory86CountRequestFormatter();
        $count86ViewCompiler = new Inventory86CountViewCompiler();
        $formattedRequest = $formatter->formatMenuItemsStatus($request);
        $response = $count86ViewCompiler->getMenuItemLowStockStatus($formattedRequest);
        return $silexApp->json($response,getReturnStatusValue($response));
    }
    public function getMenuItemsLowQuantityStatus(Application $silexApp){
        $controller = new Inventory86CountController();
        $response = $controller->getMenuItemsLowQuantityStatus();
        return $silexApp->json($response,getReturnStatusValue($response));
    }

    public function getModifierLowQuantityStatus($request, Application $silexApp){
        $request = createRequestFromURLParams($request);
        $formatter = new Inventory86CountRequestFormatter();
        $count86ViewCompiler = new Inventory86CountViewCompiler();
        $formattedRequest = $formatter->formatModifiersStatus($request);
        $response = $count86ViewCompiler->getModifierLowStockStatus($formattedRequest);
        return $silexApp->json($response,getReturnStatusValue($response));
    }

    public function getModifiersLowQuantityStatus(Application $silexApp){
        $controller = new Inventory86CountController();
        $response = $controller->getModifiersLowQuantityStatus();
        return $silexApp->json($response,getReturnStatusValue($response));
    }

    public function getForcedModifierLowQuantityStatus($request, Application $silexApp){
        $request = createRequestFromURLParams($request);
        $formatter = new Inventory86CountRequestFormatter();
        $count86ViewCompiler = new Inventory86CountViewCompiler();
        $formattedRequest = $formatter->formatForcedModifiersStatus($request);
        $response = $count86ViewCompiler->getForcedModifierLowStockStatus($formattedRequest);
        return $silexApp->json($response,getReturnStatusValue($response));
    }

    public function getForcedModifiersLowQuantityStatus(Application $silexApp){
        $controller = new Inventory86CountController();
        $response = $controller->getForcedModifiersLowQuantityStatus();
        return $silexApp->json($response,getReturnStatusValue($response));
    }

    public function getMenuLowQuantityStatus(Request $request, Application $silexApp){
        $count86ViewCompiler = new Inventory86CountViewCompiler();
        $response = $count86ViewCompiler->getMenuLowQuantityStatus(decodeRequest($request));
        return $silexApp->json($response,getReturnStatusValue($response));
    }

    public function getInventoryReorderStatus($request, Application $silexApp){
        $request = createRequestFromURLParams($request);
        $formatter = new Inventory86CountRequestFormatter();
        $controller = new Inventory86CountController();
        $formattedRequest = $formatter->formatMenuItemsStatus($request);
        $response = $controller->getInventorysReorderStatus($formattedRequest);
        return $silexApp->json($response,getReturnStatusValue($response));
    }

    public function getInventorysReorderStatus(Application $silexApp){
        $controller = new Inventory86CountController();
        $response = $controller->getInventorysReorderStatus();
        return $silexApp->json($response,getReturnStatusValue($response));
    }

    public function getInventoryStatus(Application $silexApp){
        $count86ViewCompiler = new Inventory86CountViewCompiler();
        $response = $count86ViewCompiler->getInventoryStatus();
        return $silexApp->json($response,getReturnStatusValue($response));
    }

	public function getInventoryStatusByCategory($request, Application $silexApp){
		$count86ViewCompiler = new Inventory86CountViewCompiler();
		$request = createRequestFromURLParams($request);
		$response = $count86ViewCompiler->getInventoryStatusByCategory($request);
		return $silexApp->json($response,getReturnStatusValue($response));
	}
}