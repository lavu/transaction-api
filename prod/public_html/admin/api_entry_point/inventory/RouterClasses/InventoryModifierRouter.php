<?php
use Symfony\Component\HTTPFoundation\Request;
use Silex\Application;
require_once(__DIR__ . "/../InputValidation/InventoryModifierValidators.php");
/**
 * Created by PhpStorm.
 * User: Leif
 * Date: 7/20/17
 * Time: 2:51 PM
 */
class InventoryModifierRouter
{
	public function getModifiersByInventoryItemID($request, Application $silexApp){
		$request = createRequestFromURLParams($request);
		$modifierFormatter = new InventoryModifierRequestFormatter();
		$modifierController = new InventoryModifierController();
		$formattedRequest = $modifierFormatter->formatGetAllModifiersByInventoryItemID($request);
		$response = $modifierController->getAllModifiersByInventoryItemID($formattedRequest);
		return $silexApp->json($response,getReturnStatusValue($response));
	}

	public function getAllModifiers(Application $silexApp){
		$modifierController = new InventoryModifierController();
		$response = $modifierController->getAllModifiers();
		return $silexApp->json($response,getReturnStatusValue($response));
	}

	public function getForcedModifiersByInventoryItemID($request, Application $silexApp){
		$request = createRequestFromURLParams($request);
		$forcedModifierFormatter = new InventoryModifierRequestFormatter();
		$forcedModifierController = new InventoryModifierController();
		$formattedRequest = $forcedModifierFormatter->formatGetAllForcedModifiersByInventoryItemID($request);
		$response = $forcedModifierController->getAllForcedModifiersByInventoryItemID($formattedRequest);
		return $silexApp->json($response,getReturnStatusValue($response));
	}

	public function getAllForcedModifiers(Application $silexApp){
		$forcedModifierController = new InventoryModifierController();
		$response = $forcedModifierController->getAllForcedModifiers();
		return $silexApp->json($response,getReturnStatusValue($response));
	}
}