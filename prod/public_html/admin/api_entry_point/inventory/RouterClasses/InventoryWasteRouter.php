<?php
require_once(__DIR__ . "/../InputValidation/InventoryWasteValidators.php");
use Symfony\Component\HTTPFoundation\Request;
use Silex\Application;
/**
 * Created by PhpStorm.
 * User: Hayat
 * Date: 07/23/17
 * Time: 11:36 AM
 */
class InventoryWasteRouter
{
    public function getWasteReasons(Application $silexApp){
        $wasteController = new InventoryWasteController();
        $response = $wasteController->getAllWasteReasons();
        return $silexApp->json($response,getReturnStatusValue($response));
    }

    public function createItemWasteAudits(Request $request, Application $silexApp){
        $formatter = new InventoryWasteRequestFormatter();
        $formattedRequest = $formatter->formatCreateWasteAuditRequest(decodeRequest($request));
        $validator = new CreateItemWasteAuditValidator($silexApp, $formattedRequest);
        $violations = $validator->validate();
        if($violations != null){
            return $silexApp->json($violations, getReturnStatusValue(BAD_REQUEST));
        }

        $wastViewCompiler = new InventoryWasteViewCompiler();
        $response = $wastViewCompiler->createItemWasteAudits($formattedRequest);
        return $silexApp->json($response,getReturnStatusValue($response));
    }
}