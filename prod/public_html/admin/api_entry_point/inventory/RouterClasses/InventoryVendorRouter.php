<?php
use Symfony\Component\HTTPFoundation\Request;
use Silex\Application;
require_once(__DIR__ . "/../InputValidation/InventoryVendorValidators.php");
/**
 * Created by PhpStorm.
 * User: Leif
 * Date: 5/23/17
 * Time: 10:37 AM
 */
class InventoryVendorRouter
{
	public function createVendors(Request $request, Application $silexApp){
		$vendorRequestFormatter = new InventoryVendorRequestFormatter();
		$vendorController = new InventoryVendorController();

		$postBodyArray = $vendorRequestFormatter->formatCreateVendors(decodeRequest($request));

		$validator = new InventoryVendorValidator($silexApp, $postBodyArray);
		$violations = $validator->validate();
		if($violations != null){
			return $silexApp->json($violations, getReturnStatusValue(BAD_REQUEST));
		}

		$response = $vendorController->createInventoryVendors($postBodyArray);
		return $silexApp->json($response,getReturnStatusValue($response));
	}

	public function getAllVendors(Application $silexApp, $request = ''){
		$vendorController = new InventoryVendorController();
		$request = createRequestFromURLParams($request);
		$response = $vendorController->getAllVendors($request);	
		return $silexApp->json($response,getReturnStatusValue($response));
	}

	public function getVendors($request, Application $silexApp){
		$request = createRequestFromURLParams($request);
		$vendorRequestFormatter = new InventoryVendorRequestFormatter();
		$vendorController = new InventoryVendorController();

		$postBodyArray = $vendorRequestFormatter->formatIDsFromRequest($request);
		$response = $vendorController->getVendors($postBodyArray);
		return $silexApp->json($response, getReturnStatusValue($response));
	}

	public function archiveVendors($request, Application $silexApp){
		$request = createRequestFromURLParams($request);
		$vendorRequestFormatter = new InventoryVendorRequestFormatter();
		$vendorController = new InventoryVendorController();

		$postBodyArray = $vendorRequestFormatter->formatIDsFromRequest($request);
		$response = $vendorController->archiveVendors($postBodyArray);
		return $silexApp->json($response, getReturnStatusValue($response));
	}

	public function copyVendors($request, Application $silexApp){
		$request = createRequestFromURLParams($request);
		$vendorRequestFormatter = new InventoryVendorRequestFormatter();
		$vendorController = new InventoryVendorController();

		$postBodyArray = $vendorRequestFormatter->formatIDsFromRequest($request);
		$response = $vendorController->copyVendors($postBodyArray);
		return $silexApp->json($response, getReturnStatusValue($response));
	}

	public function getOrderedItems($request, Application $silexApp){
		$request = createRequestFromURLParams($request);
		$vendorRequestFormatter = new InventoryVendorRequestFormatter();
		$vendorController = new InventoryVendorController();

		$postBodyArray = $vendorRequestFormatter->formatIDsFromRequest($request);
		$response = $vendorController->getVendorsOrderedItems($postBodyArray);
		return $silexApp->json($response, getReturnStatusValue($response));
	}

	public function getPastOrders($request, Application $silexApp){
		$request = createRequestFromURLParams($request);
		$vendorViewCompiler = new InventoryVendorViewCompiler();

		$response = $vendorViewCompiler->getVendorSpecificPreviousOrders($request);
		return $silexApp->json($response, getReturnStatusValue($response));
	}

	public function updateVendors($request, Application $silexApp){
		$vendorRequestFormatter = new InventoryVendorRequestFormatter();
		$vendorController = new InventoryVendorController();

		$postBodyArray = $vendorRequestFormatter->formatUpdateVendors(decodeRequest($request));
		$response = $vendorController->updateVendors($postBodyArray);
		return $silexApp->json($response, getReturnStatusValue($response));
	}

	public function exportVendors(Application $silexApp){
		$vendorViewCompiler = new InventoryVendorViewCompiler();
		$response = $vendorViewCompiler->exportVendors();
		return $silexApp->json($response,getReturnStatusValue($response));
	}

}