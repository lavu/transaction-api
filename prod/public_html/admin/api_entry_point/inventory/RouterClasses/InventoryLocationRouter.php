<?php
use Symfony\Component\HTTPFoundation\Request;
use Silex\Application;
/**
 * Created by PhpStorm.
 * User: hayat
 * Date: 12/5/17
 * Time: 2:33 PM
 */

class InventoryLocationRouter
{
    public function getAllChainedLocations(Application $silexApp){
        $controller = new InventoryLocationController();
        $response = $controller->getAllChainedLocations();
        return $silexApp->json($response,getReturnStatusValue($response));
    }

    public function changeLocation(Request $request, Application $silexApp){
        $postBodyArray = decodeRequest($request);
        $controller = new InventoryLocationController();
        $response = $controller->changeLocation($postBodyArray);
        return $silexApp->json($response,getReturnStatusValue($response));
    }
}