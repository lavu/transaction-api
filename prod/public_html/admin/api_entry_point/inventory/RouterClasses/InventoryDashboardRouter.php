<?php
use Symfony\Component\HTTPFoundation\Request;
use Silex\Application;
require_once(__DIR__ . "/../InputValidation/InventoryDashboardValidators.php");

/**
 * Created by PhpStorm.
 * User: Leif
 * Date: 5/23/17
 * Time: 11:36 AM
 */
class InventoryDashboardRouter
{
	public function getDashboardView(Application $silexApp){
		$controller = new InventoryDashboardViewCompiler();
		$response = $controller->getInventoryDashboardView();
		return $silexApp->json($response,getReturnStatusValue($response));
	}

	public function getLowStockLevel(Application $silexApp){
		$controller = new InventoryDashboardViewCompiler();
		$response = $controller->getLowStockLevel();
		return $silexApp->json($response,getReturnStatusValue($response));
	}

	public function getInventoryValue(Application $silexApp){
		$controller = new InventoryDashboardViewCompiler();
		$response = $controller->getInventoryValue();
		return $silexApp->json($response,getReturnStatusValue($response));
	}

	public function getPendingTransfers(Application $silexApp){
		$controller = new InventoryDashboardViewCompiler();
		$response = $controller->getPendingTransfers();
		return $silexApp->json($response,getReturnStatusValue($response));
	}

	public function getWasteValue(Application $silexApp){
		$controller = new InventoryDashboardViewCompiler();
		$response = $controller->getWasteValue();
		return $silexApp->json($response,getReturnStatusValue($response));
	}

	public function getpurchaseOrders(Application $silexApp){
		$controller = new InventoryDashboardViewCompiler();
		$response = $controller->getpurchaseOrders();
		return $silexApp->json($response,getReturnStatusValue($response));
	}

	public function getdisplayItems(Application $silexApp){
		$controller = new InventoryDashboardViewCompiler();
		$response = $controller->getdisplayItems();
		return $silexApp->json($response,getReturnStatusValue($response));
	}
}