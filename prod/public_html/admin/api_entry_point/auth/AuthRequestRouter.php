<?php
/**
 * Created by PhpStorm.
 * User: Chris
 * Date: 1/5/18
 * Time: 1:21 PM
 */

require_once __DIR__ . "/AuthRouterFunctions.php";
/**
 * Include all related Router Classes.
 * This includes everything in the RouterClasses directory
 */
foreach(glob(__DIR__."/RouterClasses/*") as $index=>$path){
    require_once($path);
}
$silexApp = new Silex\Application();
$silexApp->register(new Silex\Provider\ValidatorServiceProvider());

use Symfony\Component\HTTPFoundation\Request;
use Silex\Application;

$silexApp['debug'] = lavuDeveloperStatus(); //If dev status is true, turn on debugging.
new AuthRequestRouter($silexApp);
$silexApp->run();
class AuthRequestRouter{
    function __construct(Application $silexApp){
        $this->authRoutes($silexApp);
    }

    private function authRoutes($silexApp){
        $silexApp->POST( "/auth/Session",
            function (Request $request) use ($silexApp)
            {
                return (new SessionRouter())->getOrCreateSession($request, $silexApp);
            }
        );

    }
}