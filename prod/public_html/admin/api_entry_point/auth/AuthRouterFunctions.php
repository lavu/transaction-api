<?php
/**
 * Created by PhpStorm.
 * User: Chris
 * Date: 1/8/18
 * Time: 9:19 AM
 */


require_once __DIR__ . "/../../cp/resources/core_functions.php";
require_once __DIR__ . "/../../../third_party_frameworks/vendor/vendor/autoload.php"; //Needed for Silex, etc.

/**
 * Include all Auth Request Formatters.
 * This includes everything in the RequestFormatters directory
 */
foreach(glob(__DIR__."/../../components/Auth/RequestFormatters/*") as $index=>$path){
    require_once($path);
}


/**
 * Include all Inventory Request Compilers.
 * This includes everything in the RequestFormatters directory
 */
foreach(glob(__DIR__."/../../components/Auth/RequestCompilations/*") as $index=>$path){
    require_once($path);
}

/**
 * Include all Inventory Router Classes.
 * This includes everything in the RouterClasses directory
 */
foreach(glob(__DIR__."/RouterClasses/*") as $index=>$path){
    require_once($path);
}

use Symfony\Component\HTTPFoundation\Request;
/*******************/
/*utility functions*/
/*******************/
//These define fields are here just to have easy-to-use defaults that can't be
//mistaken for values.
define("AUTH_API_LATEST_VERSION",  "1.0");
define("AUTH_API_ROUTE",           "/auth/v".AUTH_API_LATEST_VERSION);
define("API_REQUIRED_FIELD",            'API_REQUIRED_FIELD');
define("API_OPTIONAL_FIELD",            "API_OPTIONAL_FIELD");
define("API_MISSING_OPTIONAL_FIELD",    "API_MISSING_OPTIONAL_FIELD");
define("PARAMETER_DELIMITER",           ";");
define("BAD_REQUEST",                   400);
/**
 * @param Request $request
 * @return array|mixed
 */
function decodeRequest(Request $request)
{
    if(isset($request))
    {
        $request = json_decode(urldecode($request->getContent()), true);
    }
    return $request;
}

/**
 * Checks to see if the response is a success or failure and returns a value of
 * 400 if it's a failure and 200 if it's a success.
 */
function getReturnStatusValue($response){
    if($response === BAD_REQUEST){
        return BAD_REQUEST;
    }else if($response === "SESSION_EXPIRED"){
        return 401;
    }
    else if($response === "SUCCESS"){
        return 200;
    }
    else if(json_decode(json_encode($response),true)['Status'] == "Success"){
        return 200;
    }else{
        return 400;
    }
}

/**
 *	Splits the string request into an array, using the PARAMETER_DELIMITER as the
 * string to split by.
 */
function createRequestFromURLParams($params, $fieldKey="id", $delimiter=PARAMETER_DELIMITER){
    $params = explode($delimiter, $params);
    
    if($params !== FALSE && count($params) > 0){
        $formattedParams = array();
        foreach($params as $val){
            $formattedParams[] = array($fieldKey=>$val);
        }
        return $formattedParams;
    }else{
        return null;
    }
}
