<?php
/**
 * Created by PhpStorm.
 * User: Chris
 * Date: 1/5/18
 * Time: 2:13 PM
 */
use Symfony\Component\HTTPFoundation\Request;
use Silex\Application;
define("GQL_TOKEN", "I4cG9Un/uZD0CEni8lwIPkn6SfuYipkRuderLmkg34=");
class SessionRouter{
    // From sessname() in /cp/resources/core_functions.php
    const LAVU_SESSION_KEY_PREFIX = 'poslavu_234347273_';

    public function getOrCreateSession(Request $request, Application $silexApp){
        $return = array();
        $sessionStatus = "";
        $decodedRequest = decodeRequest($request);
        $dataname = isset($decodedRequest[0]['dataname'])?$decodedRequest[0]['dataname']:"";
        $token = isset($decodedRequest[0]['token'])?$decodedRequest[0]['token']:"";
        $PHPSESSID = isset($decodedRequest[0]['PHPSESSID'])?$decodedRequest[0]['PHPSESSID']:"";
        $session_json = isset($decodedRequest[0]['SESSION_JSON'])?$decodedRequest[0]['SESSION_JSON']:"";

        if($dataname === "" || $token != GQL_TOKEN){
            $sessionStatus = "SESSION_EXPIRED";
        }else{
            if($PHPSESSID != ""){
                session_start($PHPSESSID);
                $return['SESSION_JSON'] = $_SESSION['SESSION_JSON'];
            }
            else{
                if(session_id() != ''){
                    session_write_close(); //close current session
                }
                session_start();
                $return['PHPSESSID'] = session_id();
                if($session_json != ""){
                    $_SESSION['SESSION_JSON'] = $session_json;
                }
            }
            $sessionStatus = "SUCCESS";
            $_SESSION[self::LAVU_SESSION_KEY_PREFIX . 'admin_dataname'] = $dataname;
        }
        return $silexApp->json($return,getReturnStatusValue($sessionStatus));
    }
}