<?php
$filePath = __DIR__;
require_once($filePath.'/routing.php');
require_once($filePath.'/../../api_private/api.php');
require_once($filePath.'/constantMessages/constants.php');

class route extends routing {
	public function __construct() {
		parent::__construct();
	}
	
	public function token($args) {
		$apiToken = $this->getJwt($args);
		return $apiToken;
	}
	public function reToken($args) {
		$apiToken = $this->getJwt($args);
		echo $apiToken;
		exit;
	}
	public function authToken($args) {
		$jwt = $args['jwt'];
		$tokenId = $args['tokenId'];
		$result = $this->getData($args);
		return $result;
	}
	public function authSession($args) {
		$parametersArr = $args;
		$this->apiRequest['Action'] = "getAuthSession";
		$this->apiRequest['Auth']['dataname'] = "lavutogo";
		$this->apiRequest['Params']['authSession'] = $parametersArr;
		$this->apiRequest['Component'] ="LavuToGo";
		$this->apiRequest['Src'] = 'Olo';
		$apiRequestJSON = json_encode($this->apiRequest);
		$sessionResponse = Internal_API::apiQuery($apiRequestJSON);
		return $sessionResponse;
	}

	public function itemsList($args) {
		$parametersArr['category_id'] = $this->request['category_id'];
		$paginationArr['current_page'] = $this->request['current_page'];
		$paginationArr['next_page'] = isset($this->request['next_page']) ? $this->request['next_page'] : '';
		$paginationArr['previous_page'] = isset($this->request['previous_page']) ? $this->request['previous_page'] : '';
		$paginationArr['per_page'] = $this->request['per_page'];
		$this->apiRequest['Action'] = "getItemsList";
		$this->apiRequest['Params']['categoryID'] = $parametersArr;
		$this->apiRequest['Params']['dining_option'] = $this->request['dining_option'];
		$this->apiRequest['Params']['pagination'] = $paginationArr;
		$this->apiRequest['Component'] ="Items";
		$this->apiRequest['Src'] = 'Olo';
		$apiRequestJSON = json_encode($this->apiRequest);
		return Internal_API::apiQuery($apiRequestJSON);
	}

	public function itemDetails($args) {
		$parametersArr['category_id'] = $this->request['category_id'];
		$this->apiRequest['Action'] = "getItemDetail";
		$this->apiRequest['Params']['categoryID'] = $parametersArr;
		$this->apiRequest['Component'] ="Items";
		$this->apiRequest['Src'] = 'Olo';
		$apiRequestJSON = json_encode($this->apiRequest);
		return Internal_API::apiQuery($apiRequestJSON);
	}


	 //Customers Component methods:

	public function userRegistration($args) {
        $dtime = date("Y-m-d H:i:s");
        $regDetails = array(
            "email" => $this->request['email'],
            "password" => $this->request['password'],
            "fname" => $this->request['fname'],
            "lname" => $this->request['lname'],
            "mobile" => $this->request['mobile'],
            "login_attempt" => 0,
            "login_status" => 0,
            "token" => "QWE@*$@*@#",
            "created_date" => $dtime,
            "last_modified_date" => $dtime,
            "_deleted" => 0,
            "languageid" => $this->request['languageid'],
        );

        $cpwd = array("cpwd" => $this->request['cpwd']);

        $this->apiRequest['Action'] = "userRegistration";
        $this->apiRequest['Auth']['dataname'] = "lavutogo";
        $this->apiRequest['Params']['regDetails'] = $regDetails;
        $this->apiRequest['Params']['cpwd'] = $cpwd;
        $this->apiRequest['Params']['addressInformation'] = [];
        foreach ($this->request as $key => $value) {
            if (in_array($key, ['name', 'address_1', 'address_2', 'country', 'state', 'city', 'zipcode', 'is_primary']) && !empty($value)) {
                $this->apiRequest['Params']['addressInformation'][$key] = $value;
            }
        }
        if(sizeof($this->apiRequest['Params']['addressInformation']) <= 1) {
        	$this->apiRequest['Params']['addressInformation'] = [];
        }

        $this->apiRequest['Component'] = "LavuToGo";
        $this->apiRequest['Src'] = 'Olo';
        $apiRequestJSON = json_encode($this->apiRequest);
        return Internal_API::apiQuery($apiRequestJSON);
    }

	public function userAccount($args) {
		$user_info=array( "user_id" =>$this->request['userid'],
			"first_name"=>$this->request['fname'],
			"last_name"=>$this->request['lname'],
			"email"=>$this->request['email'],
			"address"=>$this->request['address'],
			"city"=>$this->request['city'],
			"state"=>$this->request['state'],
			"zipcode"=>$this->request['zipcode'],
			"phone"=>$this->request['phone'],
			"mobile"=>$this->request['mobile'],
			"preferred_language"=>$this->request['preferred_language'],
			"user_email"=>$this->request['user_email'],
			"curpwd"=>$this->request['curpwd'],
			"newpwd"=>$this->request['newpwd'],
			"conpwd"=>$this->request['conpwd'],
		    "languageid"=>$this->request['languageid'],
		);

		$this->apiRequest['Action'] = "myAccount";
		$this->apiRequest['Auth']['dataname'] = "lavutogo";
		$this->apiRequest['Params']['users_info'] = $user_info;
		$this->apiRequest['Component'] ="LavuToGo";
		$this->apiRequest['Src'] = 'Olo';
		$apiRequestJSON = json_encode($this->apiRequest);
		return Internal_API::apiQuery($apiRequestJSON);
	}

	public function userSignin($args) {
		$signInDetails = array(
			"dataname"=> $this->request['dataname'],
			"email"=> $this->request['email'],
			"password" => $this->request['password'],
		    "languageid" => $this->request['languageid'],
		);

		$this->apiRequest['Action'] = "userSignin";
		$this->apiRequest['Auth']['dataname'] = "lavutogo";
		$this->apiRequest['Params']['signInDetails'] = $signInDetails;
		$this->apiRequest['Component'] ="LavuToGo";
		$this->apiRequest['Src'] = 'Olo';
		$apiRequestJSON = json_encode($this->apiRequest);
		return Internal_API::apiQuery($apiRequestJSON);
	}

	public function user_signin($args) {
		$parametersArr['signInDetails'] = $this->request['signInDetails'];
		$this->apiRequest['Action'] = "userSignin";
		$this->apiRequest['Params']['signInDetails'] = $parametersArr;
		$this->apiRequest['Component'] ="LavuToGo";
		$this->apiRequest['Src'] = 'Olo';
		$apiRequestJSON = json_encode($this->apiRequest);
		return Internal_API::apiQuery($apiRequestJSON);
	}

	public function user_myAccount($args) {
		$parametersArr['users_info'] = $this->request['users_info'];
		$this->apiRequest['Action'] = "myAccount";
		$this->apiRequest['Params']['users_info'] = $parametersArr;
		$this->apiRequest['Component'] ="LavuToGo";
		$this->apiRequest['Src'] = 'Olo';
		$apiRequestJSON = json_encode($this->apiRequest);
		return Internal_API::apiQuery($apiRequestJSON);
	}

	// START LP-4557
	// for forgotpassword
	public function forgotPassword() {
	    $password_info = array(
	        "email"=> $this->request['email'],
	        "dataname"=> $this->request['dataname'],
	        "languageid"=> $this->request['languageid']
	    );
	    $this->apiRequest['Action'] = "forgotPassword";
	    $this->apiRequest['Auth']['dataname'] = "lavutogo";
	    $this->apiRequest['Params']['forgotPasswordDetails'] = $password_info;
	    $this->apiRequest['Component'] ="LavuToGo";
	    $this->apiRequest['Src'] = 'Olo';
	    $apiRequestJSON = json_encode($this->apiRequest);
	    return Internal_API::apiQuery($apiRequestJSON);
	}

	// for updatepassword
	public function updatePassword() {
	    $updatepassword_info=array(
	        "token" =>$this->request['token'],
	        "newpwd"=>$this->request['newpwd'],
	        "conpwd"=>$this->request['conpwd'],
	        "languageid"=>$this->request['languageid'],
	    );
	    $this->apiRequest['Action'] = "updatePassword";
	    $this->apiRequest['Auth']['dataname'] = "lavutogo";
	    $this->apiRequest['Params']['updatePasswordDetails'] = $updatepassword_info;
	    $this->apiRequest['Component'] ="LavuToGo";
	    $this->apiRequest['Src'] = 'Olo';
	    $apiRequestJSON = json_encode($this->apiRequest);
	    return Internal_API::apiQuery($apiRequestJSON);
	}

	// END LP-4557
	//Location Component Methods:
	public function locationAddress($args) {
		$parametersArr['lavu_togo_name'] = $this->request['lavutogoname'];
		$this->apiRequest['Action'] = "getLocationAddress";
		$this->apiRequest['Params']['lavu_togo_name'] = $parametersArr;
		$this->apiRequest['Component'] ="Location";
		$this->apiRequest['Src'] = 'Olo';

		// Work-around for not having dataname when working with Internal API
		if (empty($this->apiRequest['Auth']['dataname'])) {
			$this->apiRequest['Auth']['dataname'] = 'lavutogo';
			$this->apiRequest['Params']['dataname'] = '';
		}
		
		$apiRequestJSON = json_encode($this->apiRequest);
		$internalApiResponseJson = Internal_API::apiQuery($apiRequestJSON);
		return $internalApiResponseJson;
	}

    public function MealPlanInfo(){
        $this->apiRequest['Action'] = "getMealPlanInfo";
        $this->apiRequest['Params']['userId'] = $this->request['userId'];
        $this->apiRequest['dataname'] = $dataname;
        $this->apiRequest['Component'] ="Location";
        $this->apiRequest['Src'] = 'Olo';

        $apiRequestJSON = json_encode($this->apiRequest);
        return Internal_API::apiQuery($apiRequestJSON);
    }

	public function datanameDetails($args) {
		$parametersArr['lavu_togo_name'] = $this->request['lavutogoname'];
		$this->apiRequest['Action'] = "getDataName";
		$this->apiRequest['Params']['lavu_togo_name'] = $parametersArr;
		$this->apiRequest['Component'] = "LavuToGo";
		$this->apiRequest['Src'] = 'Olo';

		// Work-around for not having dataname when working with Internal API
		if (empty($this->apiRequest['Auth']['dataname'])) {
			$this->apiRequest['Auth']['dataname'] = 'lavutogo';
			$this->apiRequest['Params']['dataname'] = '';
		}

		$apiRequestJSON = json_encode($this->apiRequest);
		$internalApiResponseJson = Internal_API::apiQuery($apiRequestJSON);
		return $internalApiResponseJson;
	}

	public function lavutogolocationAddress($args) {
		$parametersArr['lavu_togo_name'] = $this->request['lavutogoname'];
		$this->apiRequest['Action'] = "getlavutogoLocationAddress";
		$this->apiRequest['Params']['lavu_togo_name'] = $parametersArr;
		$this->apiRequest['Component'] = "Location";
		$this->apiRequest['Src'] = 'Olo';
	
		// Work-around for not having dataname when working with Internal API
		if (empty($this->apiRequest['Auth']['dataname'])) {
			$this->apiRequest['Auth']['dataname'] = 'lavutogo';
			$this->apiRequest['Params']['dataname'] = '';
		}
	
		$apiRequestJSON = json_encode($this->apiRequest);
		$internalApiResponseJson = Internal_API::apiQuery($apiRequestJSON);
		return $internalApiResponseJson;
	}

	public function deliveryAddress($args) {
		$parametersArr['streetAddress'] = $this->request['streetAddress'];
		$parametersArr['lavu_togo_name'] = $this->request['lavu_togo_name'];
		$parametersArr['dataname'] = $args['dataname'];
		// print_r($parametersArr);
		$this->apiRequest['Action'] = "getDeliveryAddress";
		$this->apiRequest['Params']['streetAddress'] = $parametersArr;
		$this->apiRequest['Component'] ="Location";
		$this->apiRequest['Src'] = 'Olo';
		$this->apiRequest['Auth']['dataname'] = empty($args['dataname']) ? 'lavutogo' : $args['dataname'];

		// Work-around for not having dataname when working with Internal API
		if (empty($this->apiRequest['Auth']['dataname'])) {
			$this->apiRequest['Auth']['dataname'] = 'lavutogo';
		}
		$apiRequestJSON = json_encode($this->apiRequest);
		return Internal_API::apiQuery($apiRequestJSON);
	}

	public function pickUpDineInLocationAddress($args) {
	    $parametersArr['streetAddress'] = str_replace('||','/',$this->request['streetAddress']);
		$parametersArr['lavu_togo_name'] = $this->request['lavu_togo_name'];
		$parametersArr['dataname'] = $args['dataname'];
		//print_r($parametersArr);
		$this->apiRequest['Action'] = "getPickUpDineInLocationAddress";
		$this->apiRequest['Params']['streetAddress'] = $parametersArr;
		$this->apiRequest['Component'] ="Location";
		$this->apiRequest['Src'] = 'Olo';
		$this->apiRequest['Auth']['dataname'] = $args['dataname'];

		// Work-around for not having dataname when working with Internal API
		if (empty($this->apiRequest['Auth']['dataname'])) {
			$this->apiRequest['Auth']['dataname'] = 'lavutogo';
			$this->apiRequest['Params']['dataname'] = '';
		}

		$apiRequestJSON = json_encode($this->apiRequest);
		return Internal_API::apiQuery($apiRequestJSON);
	}

	public function locationConfig($args) {
		$this->apiRequest['Action'] = "getLocationConfig";
		$this->apiRequest['Component'] ="Location";
		$this->apiRequest['Params'] = $this->request;
		$this->apiRequest['Src'] = 'Olo';

		// Work-around for not having dataname when working with Internal API
		if (empty($this->apiRequest['Auth']['dataname'])) {
			$this->apiRequest['Auth']['dataname'] = 'lavutogo';
			$this->apiRequest['Params']['dataname'] = '';
		}

		$apiRequestJSON = json_encode($this->apiRequest);
		return Internal_API::apiQuery($apiRequestJSON);
	}


	public function deliveryLocationFees($args) {
		$parametersArr['streetAddress'] = $args['streetAddress'];
		$parametersArr['locationAddress'] = $args['locationAddress'];
		$this->apiRequest['Action'] = "getDeliveryLocationFees";
		$this->apiRequest['Component'] ="Location";
		$this->apiRequest['Params'] = $parametersArr;
		$this->apiRequest['Src'] = 'Olo';
		$apiRequestJSON = json_encode($this->apiRequest);
		return Internal_API::apiQuery($apiRequestJSON);
	}

	// Order Component Methods
	public function orderHistory($args) {
	    $userDetails = array('user_id' => $this->request['user_id'], 'loc_id' => 1, 'languageid' => $this->request['languageid']);
		$current_page=$this->request['current_page'];
		$next_page=$this->request['next_page'];
		$per_page=$this->request['per_page'];
		$pagination = array("current_page"=>$current_page, "next_page"=>$next_page,"per_page"=>$per_page);
		$this->apiRequest['Params']['userDetails'] = $userDetails;
		$this->apiRequest['Params']['pagination'] = $pagination;
		$this->apiRequest['Action'] = "getOrderHistory";
		$this->apiRequest['Component'] ="Orders";
		$this->apiRequest['Src'] = 'Olo';
		$apiRequestJSON = json_encode($this->apiRequest);
		return Internal_API::apiQuery($apiRequestJSON);
	}

	public function reOrder($args) {
		$this->apiRequest['Params'] = $args;
		$this->apiRequest['Action'] = "reOrder";
		$this->apiRequest['Component'] ="Orders";
		$this->apiRequest['Src'] = 'Olo';
		$apiRequestJSON = json_encode($this->apiRequest);
		return Internal_API::apiQuery($apiRequestJSON);
	}

	public function ioId($args) {
		$ioIdDetails = array('order_id' => $args['order_id'], 'data_name' => $args['data_name']);
		$this->apiRequest['Params']['ioIdDetails'] = $ioIdDetails;
		$this->apiRequest['Action'] = "generateIoid";
		$this->apiRequest['Component'] ="Orders";
		$this->apiRequest['Src'] = 'Olo';
		$apiRequestJSON = json_encode($this->apiRequest);
		return Internal_API::apiQuery($apiRequestJSON);
	}

	public function itemsTaxDetails($args) {
		$orderitems = json_decode($this->request['cart_contents']);
		$orderDetails = array('loc_id' => 1, 'languageid' => $args['languageid']); // here form order's id as order_id
		$ItemsDetails = array();

		foreach($orderitems as $orderitem) {
			$ItemsDetails[] = array(
				'itemid'=> $orderitem->itemid,
				'qty' => $orderitem->qty,
				'fmods_checked' => $orderitem->fmods_checked,
				'omods_checked' => $orderitem->omods_checked
			);
		}

		$this->apiRequest['Action'] = "getItemsTaxDetails";
		$this->apiRequest['Params']['orderDetails'] = $orderDetails;
		$this->apiRequest['Params']['orderItemsDetails'] = $ItemsDetails;
		$this->apiRequest['Component'] ="Orders";
		$this->apiRequest['Src'] = 'Olo';
		$apiRequestJSON = json_encode($this->apiRequest);
		return Internal_API::apiQuery($apiRequestJSON);
	}

	public function orderDetails($args) {
		$orderitems = json_decode($this->request['cart_contents']);
		$orderDetails = array(
			'order_id' => $this->request['order_id'],
			'order_type' =>$this->request['order_type'],
			'delivery_fee' => $this->request['delivery_fee'],
			'user_id' => $this->request['user_id'],
		    'open_date' =>$this->request['open_date'],
			'loc_id' => $this->request['locationid'],
		    'languageid' => $this->request['languageid'],
		);
		$ItemsDetails = array();

		foreach ($orderitems as $orderitem) {
			$ItemsDetails[] = array(
				'itemid'=> $orderitem->itemid,
			    'qty' => $orderitem->qty,
			    'fmods' => $orderitem->fmods,
			    'fmods_checked' => $orderitem->fmods_checked,
				'omods' => $orderitem->omods,
				'omods_checked' => $orderitem->omods_checked,
				'special' => $orderitem->special
			);
		}

		$this->apiRequest['Action'] = "myOrderitems";
		$this->apiRequest['Params']['orderDetails'] = $orderDetails;
		$this->apiRequest['Params']['orderItemsDetails'] = $ItemsDetails;
		$this->apiRequest['Component'] ="Orders";
		$this->apiRequest['Src'] = 'Olo';
		return Internal_API::apiQuery(json_encode($this->apiRequest));
	}

	public function deleteOrder($args) {
		$this->apiRequest['Action'] = "deleteOrder";
		$this->apiRequest['Component'] ="Menu";
		$this->apiRequest['Src'] = 'Olo';
		return Internal_API::apiQuery(json_encode($this->apiRequest));
	}

	// Card Payment Method
	public function cardPayment($args) {
		$this->apiRequest['Action'] = "initiateCardPayment";
		$this->apiRequest['Params'] = $this->getOrderParams();
		$this->apiRequest['Component'] ="Payment";
		$this->apiRequest['Src'] = 'Olo';
		return Internal_API::apiQuery(json_encode($this->apiRequest));
	}

	// Wallet Payment Method
	public function walletPayment($args) {
		$this->apiRequest['Action'] = "initiateWalletPayment";
		$this->apiRequest['Params'] = $this->getOrderParams();
		$this->apiRequest['Component'] = "Payment";
		$this->apiRequest['Src'] = 'Olo';
		return Internal_API::apiQuery(json_encode($this->apiRequest));
	}

	// Cash On Pickup/Delivery/Dine-in Payment Method
	public function cod($args) {
		$this->apiRequest['Action'] = "initiateCOD";
		$this->apiRequest['Params'] = $this->getOrderParams();
		$this->apiRequest['Component'] = "Payment";
		$this->apiRequest['Src'] = 'Olo';
		return Internal_API::apiQuery(json_encode($this->apiRequest));
	}

	// NYU Meal Plan Payment Method
	public function mealPlanPayment($args) {
		$this->apiRequest['Component'] = 'Payment';
		$this->apiRequest['Action'] = 'initiateMealPlanPayment';
		$this->apiRequest['Src'] = 'Olo';
		$this->apiRequest['Params'] = $this->getOrderParams();
		return Internal_API::apiQuery(json_encode($this->apiRequest));
	}

	public function mealPlanAddFunds($args) {
		$request_params = array(
			'dataname'              => $this->request['dataname'],
			'user_id'               => $this->request['user_id'],
			'payable_amount'        => $this->request['payable_amount'],
			'payment_method_nonce'  => $this->request['payment_method_nonce'],
		    'languageid'              => $this->request['languageid'],
		);

		$this->apiRequest['Component'] = 'Payment';
		$this->apiRequest['Action']    = 'initiateMealPlanAddFunds';
		$this->apiRequest['Src']       = 'Olo';
		$this->apiRequest['Params']    = $request_params;

		return Internal_API::apiQuery(json_encode($this->apiRequest));
	}

	// Lavu Gift Card Component Methods
	public function lavuGiftCardDetails($args) {
		$lavu_card_no = $this->request['lavuCard'];
		$orderId = $this->request['orderId'];
		$dataname = $this->request['dataname'];
		$languageId = $this->request['languageid'];

		$this->apiRequest['Action'] = "getLavuGiftCardDetails";
		$this->apiRequest['Params'] = array("lavu_card" => $lavu_card_no, "dataname" => $dataname, "orderId" => $orderId, "languageid" => $languageId);
		$this->apiRequest['Component'] ="LavuGiftCard";
		$this->apiRequest['Src'] = 'Olo';
		$apiRequestJSON = json_encode($this->apiRequest);
		return Internal_API::apiQuery($apiRequestJSON);
	}

	public function lavuLoyaltyPointsDetails($args) {
		$orderitems = json_decode($this->request['cart_contents']);
		$lavu_card_no = $this->request['lavuCard'];
		$orderId = $this->request['orderId'];
		$dataname = $this->request['dataname'];
		$discount_id = $this->request['discount_id'];
		$languageId = $this->request['languageid'];

		foreach($orderitems as $orderitem) {
			$ItemsDetails[] = array(
					'itemid'=> $orderitem->itemid,
					'qty' => $orderitem->qty,
					'fmods_checked' => $orderitem->fmods_checked,
					'omods_checked' => $orderitem->omods_checked
			);
		}

		$this->apiRequest['Action'] = "getLoyaltyPointDetails";
		$this->apiRequest['Params'] = array("lavu_card" => $lavu_card_no, "dataname" => $dataname, "orderId" => $orderId, "cart_contents" => $orderitems,"discount_id"=>$discount_id, "languageid" => $languageId);
		$this->apiRequest['Component'] ="LavuGiftCard";
		$this->apiRequest['Src'] = 'Olo';
		$apiRequestJSON = json_encode($this->apiRequest);
		return Internal_API::apiQuery($apiRequestJSON);
	}

	public function checkLavuAwardPoints($args) {
		$lavu_card_no = $this->request['lavuCard'];
		$orderId = $this->request['orderId'];
		$dataname = $this->request['dataname'];

		$this->apiRequest['Action'] = "checkAwardPoints";
		$this->apiRequest['Params'] = array("lavu_card" => $lavu_card_no, "dataname" => $dataname, "orderId" => $orderId);
		$this->apiRequest['Component'] ="LavuGiftCard";
		$this->apiRequest['Src'] = 'Olo';
		$apiRequestJSON = json_encode($this->apiRequest);
		return Internal_API::apiQuery($apiRequestJSON);
	}

	public function checkLoyaltyDiscountPoints($args) {
		$lavu_card_no = $this->request['lavuCard'];
		$orderId = $this->request['orderId'];
		$discountId = $this->request['discountId'];
		$dataname = $this->request['dataname'];

		$this->apiRequest['Action'] = "checkLoyaltyDiscountPoints";
		$this->apiRequest['Params'] = array("lavu_card" => $lavu_card_no, "dataname" => $dataname, "orderId" => $orderId, "discountId" => $discountId);
		$this->apiRequest['Component'] ="LavuGiftCard";
		$this->apiRequest['Src'] = 'Olo';
		$apiRequestJSON = json_encode($this->apiRequest);
		return Internal_API::apiQuery($apiRequestJSON);
	}

	//Menu Component Methods:
	public function Menus($args) {
		$this->apiRequest['Action'] = "getMenus";
		$this->apiRequest['Component'] ="Menu";
		$this->apiRequest['Src'] = 'Olo';
		$apiRequestJSON = json_encode($this->apiRequest);
		return Internal_API::apiQuery($apiRequestJSON);
	}
	
	public function itemExist() {
	    $languageid = $this->request['languageid'];
		$item_id = $this->request['item_id'];
		$dining_type = $this->request['dining_type'];
		$dataname = $this->request['dataname'];
		$this->apiRequest['Action'] = "checkItemExist";
		$this->apiRequest['Params'] = array("item_id" => $item_id, "dataname" => $dataname, "dining_type" => $dining_type, "languageid" => $languageid);
		$this->apiRequest['Component'] ="Menu";
		$this->apiRequest['Src'] = 'Olo';
		$apiRequestJSON = json_encode($this->apiRequest);
		return Internal_API::apiQuery($apiRequestJSON);
	}
	
	public function getDetourDetails($args) {
		$parametersArr = $this->request['detour_id'];
		$this->apiRequest['Params']['detour_id'] = $parametersArr;
		$this->apiRequest['Action'] = "getDetourDetails";
		$this->apiRequest['Component'] ="Items";
		$this->apiRequest['Src'] = 'Olo';
		$apiRequestJSON = json_encode($this->apiRequest);
		return Internal_API::apiQuery($apiRequestJSON);
	}
	
	public function mealPlanDetails($args) {
	    $user_id = $this->request['user_id'];
	    $dataname = $this->request['dataname'];
	    
	    $this->apiRequest['Action'] = "mealPlanDetails";
	    $this->apiRequest['Params'] = array("user_id" => $user_id, "dataname" => $dataname);
	    $this->apiRequest['Component'] ="LavuToGo";
	    $this->apiRequest['Src'] = 'Olo';
	    $apiRequestJSON = json_encode($this->apiRequest);
	    return Internal_API::apiQuery($apiRequestJSON);
	}
	
	public function locationAddressByLavutogoName() {
		$lavu_togo_name = $this->request['lavu_togo_name'];
		$dataname = $this->request['dataname'];
		$this->apiRequest['Action'] = "getLocationAddressByLavutogoName";
		$this->apiRequest['Params'] = array("lavu_togo_name" => $lavu_togo_name, "dataname" => $dataname);
		$this->apiRequest['Component'] ="Location";
		$this->apiRequest['Src'] = 'Olo';
		$apiRequestJSON = json_encode($this->apiRequest);
		return Internal_API::apiQuery($apiRequestJSON);
	}

	/*
	* List of addresses for user API
	*
	* The function provides a list of addresses storred specifically to an user. Authentication are done on
	* user token id and user id ensuring no data is leaked for anonymous user id's
	*
	* @since Method is made available in 2.0.1
	* @return JSON
	*/
	public function listUserAddress() {
		$this->apiRequest['Action'] = "getUserAddressList";
		$this->apiRequest['Auth']['dataname'] = "lavutogo";
		$this->apiRequest['Component'] = "LavuToGo";
		$this->apiRequest['Src'] = 'Olo';
		$this->apiRequest['Params'] = $this->request;
		$this->apiRequest['Params']['tokenId'] = $this->tokenId;
		return Internal_API::apiQuery( json_encode( $this->apiRequest ) );
	}

	/*
	* Save or update user address API
	*
	* The function allows users to update record base on id & users id or save data on user id
	*
	* @since Method is made available in OLO 2.0.1
	* @return array
	*/
	public function saveAddress() {
		$this->apiRequest['Action'] = "saveAddress";
		$this->apiRequest['Auth']['dataname'] = "lavutogo";
		$this->apiRequest['Component'] = "LavuToGo";
		$this->apiRequest['Src'] = 'Olo';
		$this->apiRequest['Params'] = $this->request;
		$this->apiRequest['Params']['tokenId'] = $this->tokenId;
		return Internal_API::apiQuery( json_encode( $this->apiRequest ) );
	}

	public function deleteAddress() {
		$this->apiRequest['Action'] = "deleteAddress";
		$this->apiRequest['Auth']['dataname'] = "lavutogo";
		$this->apiRequest['Component'] = "LavuToGo";
		$this->apiRequest['Src'] = 'Olo';
		$this->apiRequest['Params'] = $this->request;
		$this->apiRequest['Params']['tokenId'] = $this->tokenId;
		return Internal_API::apiQuery( json_encode( $this->apiRequest ) );
	}

	public function itemInventoryStatus() {
		$this->apiRequest['Action'] = "getItemInventoryStatus";
		$this->apiRequest['Auth']['dataname'] = $this->request['dataname'];
		$this->apiRequest['Component'] = "Items";
		$this->apiRequest['Src'] = 'Olo';
		$this->apiRequest['Params'] = $this->request;
		$this->apiRequest['Params']['tokenId'] = $this->tokenId;
		return Internal_API::apiQuery( json_encode( $this->apiRequest ) );
	}
	/*
	* Set user address infromation in requestParams
	*
	* The function add user address infromation into the given array
	*
	* @since Method is made available in OLO 2.0.1
	* @param array
	* @return array
	*/
	private function getOrderParams() {
		return [
			'dataname' => $this->request['dataname'],
			'order_id' => $this->request['order_id'],
			'user_id' => $this->request['user_id'],
			'payable_amount' => $this->request['payable_amount'],
			'rounding_amount' => $this->request['rounding_amount'],
			'tip_type' => $this->request['tip_type'],
			'tip_amount' => $this->request['tip_amount'],
			'lavu_pay_type' => $this->request['lavu_pay_type'],
			'lavu_card_number' => $this->request['lavu_card_number'],
			'lavu_add_points' => $this->request['lavu_add_points'],
			'lavu_pay_amount' => $this->request['lavu_pay_amount'],
			'discount_id' => $this->request['discount_id'],
			'phone_number' => $this->request['phone_number'],
			'email_id' => $this->request['email_id'],
			'first_name' => $this->request['first_name'],
			'last_name' => $this->request['last_name'],
			'delivery_instructions' => $this->request['delivery_instructions'],
			'customer_address' => $this->request['customer_address'],
			'delivery_fee' => $this->request['delivery_fee'],
			'order_type' => $this->request['order_type'],
			'location_id' => $this->request['location_id'],
			'dining_date' => $this->request['dining_date'],
			'dining_time' => $this->request['dining_time'],
			'closed_date' => $this->request['closed_date'],
			'orderItemsDetails' => json_decode($this->request['cart_contents'], true),
			'city' => $this->request['city'],
			'state' => 	$this->request['state'],
			'postalCode' => $this->request['postalCode'],
			'country' => $this->request['country'],
			'streetAddress' => 	$this->request['streetAddress'],
			'payment_method_nonce'  => $this->request['payment_method_nonce'],
		    'languageid' => $this->request['languageid'],
		];
	}
	
	/*This API is used to get language pack details
	 +	 * Return langulage pack details.
	 +	 */
	public function lavutogoLanguagePack() {
    	$parametersArr = $this->args[1];
    	$this->apiRequest['Action'] = "getlavutogoLanguagePack";
    	$this->apiRequest['Params']['lavu_togo_name'] = $parametersArr;
    	$this->apiRequest['Auth']['dataname'] = "lavutogo";
    	$this->apiRequest['Component'] = "Location";
    	$this->apiRequest['Src'] = 'Olo';
    	$apiRequestJSON = json_encode($this->apiRequest);
    	$internalApiResponseJson = Internal_API::apiQuery($apiRequestJSON);
    	return $internalApiResponseJson;
    }
}

$route = new route();
