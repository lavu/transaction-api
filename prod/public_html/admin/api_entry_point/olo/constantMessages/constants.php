<?php 
	$messageArray = [
		"LTG_ON_ORDER_CHECKOUT_MESSAGE" => "Thank you. Your order has been successfully placed."
	];

	foreach ($messageArray as $contantName => $message) {
		define($contantName, $message);
	}
?>
