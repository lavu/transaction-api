<?php
require_once('/home/poslavu/public_html/inc/olo/jwt/jwt.php');

class routing extends jwtOlo
{
	public $apiRequest = array();
	public $apiParameterArr = array();
	public $request = '';
	private $_error = 'N';
	private $_get = '';
	private $_poststr = '';
	
	public function __construct() {
		parent::__construct();
		header("Access-Control-Allow-Origin: *");
		header("Access-Control-Allow-Methods: *");
		header("Access-Control-Allow-Headers: Authorization, Access-Control-Allow-Headers, Origin, Accept, X-Requested-With, Content-Type, Access-Control-Request-Method, Access-Control-Request-Headers");
		header("Content-Type: application/json");
		$this->apiParameterArr['Menus'] = array(
			'method' => 'GET',
			'compareDataType' => array(
				'tokenId' => 'open',
				'dataname' => 'string'
			)
		);
		
		$this->apiParameterArr['itemsList'] = array(
			'method' => 'GET',
			'compareDataType' => array(
				'tokenId' => 'open',
				'dataname' => 'string',
				'current_page' => 'integer',
				'next_page' => 'integer',
				'previous_page' => 'integer',
				'per_page' => 'integer',
				'category_id' => 'integer',
				'dining_option' => 'string'
			)
		);
		$this->apiParameterArr['locationAddress'] = array(
			'method' => 'GET',
			'compareDataType' => array(
				'tokenId' => 'open',
				'lavutogoname' => 'string'
			)
		);
		$this->apiParameterArr['MealPlanInfo'] = array(
			'method' => 'POST',
			'compareDataType' => array(
				'tokenId' => 'open',
				'userId' => 'integer',
				'dataname' => 'string'
			)
		);
		$this->apiParameterArr['locationConfig'] = array(
			'method' => 'GET',
			'compareDataType' => array(
				'tokenId' => 'open',
				'lavutogoname' => 'open',
				'dataname' => 'string',
			)
		);
		$this->apiParameterArr['datanameDetails'] = array(
			'method' => 'GET',
			'compareDataType' => array(
				'tokenId' => 'open',
				'lavutogoname' => 'string',
			)
		);
		$this->apiParameterArr['lavutogoLocationAddress'] = array(
				'method' => 'GET',
				'compareDataType' => array(
						'tokenId' => 'open',
						'lavutogoname' => 'string'
				)
		);
		$this->apiParameterArr['deliveryAddress'] = array(
			'method' => 'POST',
			'compareDataType' => array(
				'tokenId' => 'open',
				'lavu_togo_name' => 'string',
				'streetAddress' => 'open',
				'diningOption' => 'open',
				'dataname' => 'string'
			)
		);
		$this->apiParameterArr['pickUpDineInLocationAddress'] = array(
			'method' => 'POST',
			'compareDataType' => array(
				'tokenId' => 'open',
				'lavu_togo_name' => 'string',
				 'dataname' => 'string',
				'streetAddress' => 'open',
				'diningOption' => 'open'
			)
		);
	   $this->apiParameterArr['ioId'] = array(
		   	'method' => 'POST',
			'compareDataType' => array(
				'tokenId' => 'open',
				'order_id' => 'string',
				'data_name' => 'string'
			)
		);
	   $this->apiParameterArr['deliveryLocationFees'] = array(
		   	'method' => 'POST',
			'compareDataType' => array(
				'tokenId' => 'open',
				'streetAddress' => 'string',
				'locationAddress' => 'string'
			)
		);
		$this->apiParameterArr['itemsTaxDetails'] = array(
			'method' => 'POST',
			'compareDataType' => array(
				'tokenId' => 'open',
				'dataname' => 'string',
				'cart_contents' => 'string',
			    	'languageid' => 'integer',
			)
		);
		$this->apiParameterArr['orderDetails'] = array(
			'method' => 'POST',
			'compareDataType' => array(
				'tokenId' => 'open',
				'dataname' => 'string',
				'cart_contents' => 'string',
				'order_id' => 'string',
				'user_id' => 'integer',
				'delivery_fee' => 'integer',
			    	'open_date'=>'string',
				'order_type' => 'string',
				'locationid' => 'integer',
			    	'languageid' => 'integer',
			)
		);

		$orderParams = [
			'method' => 'POST',
			"compareDataType" => [
				'tokenId' => 'open',
				'dataname' => 'string',
				'order_id' => 'string',
				'cart_contents' => 'string',
				'user_id' => 'integer',
				'payment_method_nonce' => 'string',
				'payable_amount' => 'integer',
				'tip_type' => 'open',
				'tip_amount' => 'open',
				'lavu_pay_type' => 'open',
				'lavu_card_number' => 'open',
				'lavu_add_points' => 'open',
				'lavu_pay_amount' => 'open',
				'discount_id' => 'open',
				'phone_number' => 'open',
				'email_id' => 'string',
				'first_name' => 'string',
				'last_name' => 'string',
				'delivery_instructions' => 'string',
				'customer_address' => 'string',
				'delivery_fee' => 'integer',
				'order_type' => 'string',
				'location_id' => 'integer',
				'dining_date' => 'open',
				'dining_time' => 'open',
				'closed_date' => 'open',
				'city' => 'open',
				'state' => 	'open',
				'postalCode' => 'open',
				'country' => 'open',
				'streetAddress' => 'open',
				'rounding_amount' => 'open',
			    	'languageid' => 'integer',
			]
		];

		$this->apiParameterArr['cardPayment'] = $orderParams;
		$this->apiParameterArr['walletPayment'] = $orderParams;
		$this->apiParameterArr['cod'] = $orderParams;
		$this->apiParameterArr['mealPlanPayment'] =  $orderParams;

		$this->apiParameterArr['mealPlanAddFunds'] = array(
			'method' => 'POST',
			'compareDataType' => array(
				'tokenId' => 'open',
				'dataname' => 'string',
				'user_id' => 'integer',
				'payment_method_nonce' => 'string',
				'payable_amount' => 'integer',
			    	'languageid' => 'integer',
			)
		);

		$this->apiParameterArr['lavuGiftCardDetails'] = array(
			'method' => 'POST',
			'compareDataType' => array(
				'tokenId' => 'open',
				'dataname' => 'string',
				'lavuCard' => 'integer',
				'orderId' => 'string',
			    	'languageid' => 'integer',
			)
		);
		$this->apiParameterArr['lavuLoyaltyPointsDetails'] = array(
			'method' => 'POST',
			'compareDataType' => array(
				'tokenId' => 'open',
				'dataname' => 'string',
				'lavuCard' => 'integer',
				'orderId' => 'string',
				'cart_contents' => 'string',
				'discount_id' => 'integer',
			    	'languageid' => 'integer',
			)
		);
		$this->apiParameterArr['checkLavuAwardPoints'] = array(
			'method' => 'POST',
			'compareDataType' => array(
				'tokenId' => 'open',
				'dataname' => 'string',
				'lavuCard' => 'integer',
				'orderId' => 'string'
			)
		);
		$this->apiParameterArr['checkLoyaltyDiscountPoints'] = array(
			'method' => 'POST',
			'compareDataType' => array(
				'tokenId' => 'open',
				'dataname' => 'string',
				'lavuCard' => 'integer',
				'orderId' => 'string',
				'discountId' => 'integer'
			)
		);
		$this->apiParameterArr['orderHistory'] = array(
			'method' => 'POST',
			'compareDataType' => array(
				'tokenId' => 'open',
				'dataname' => 'string',
				'user_id' => 'integer',
				'current_page' => 'open',
				'next_page' => 'open',
				'per_page' => 'open',
			    	'languageid' => 'integer',
			)
		);
		$this->apiParameterArr['reOrder'] = array(
			'method' => 'POST',
			'compareDataType' => array(
				'tokenId' => 'open',
				'dataname' => 'string',
				'user_id' => 'integer',
				'order_id' => 'string',
				'dinetype' => 'string',
			    	'languageid' => 'integer',
			)
		);
		$this->apiParameterArr['userRegistration'] = array(
			'method' => 'POST',
			'compareDataType' => array(
				'tokenId' => 'open',
				'email' => 'string',
				'password' => 'open',
				'fname' => 'string',
				'lname' => 'string',
				'mobile' => 'open',
				'cpwd' => 'open',
				'name' => 'open',
                		'address_1' => 'open',
                		'address_2' => 'open',
                		'country' => 'open',
                		'state' => 'open',
                		'city' => 'open',
                		'zipcode' => 'open',
                		'is_primary' => 'open',
			    	'languageid' => 'integer',
			)
		);
		$this->apiParameterArr['userSignin'] = array(
			'method' => 'POST',
			'compareDataType' => array(
					'dataname' => 'string',
					'tokenId' => 'open',
					'email' => 'string',
					'password' => 'open',
			        	'languageid' => 'integer',
			)
		);
		// LP-4557 START
		$this->apiParameterArr['forgotPassword'] = array('method' => 'POST',
		    'compareDataType' => array(
		        'tokenId' => 'open',
		        'email' => 'string',
		        'dataname' => 'string',
		        'languageid' => 'integer',
		    )
		);
		$this->apiParameterArr['updatePassword'] = array('method' => 'POST',
		    'compareDataType' => array(
		        'tokenId' => 'open',
		        'token'=> 'string',
		        'newpwd' => "open",
		        'conpwd' => "open",
		        'languageid' => 'integer',
		    )
		);
		// LP-4557 END
		$this->apiParameterArr['userAccount'] = array('method' => 'POST',
			'compareDataType' => array(
				'tokenId' => 'open',
				'userid'=> 'open',
				'fname' => 'string',
				'lname' => 'string',
				'email' => 'string',
				'address' => 'open',
				'city' => 'string',
				'state' => 'string',
				'zipcode' => 'open',
				'phone' => 'open',
				'mobile' => 'open',
				'preferred_language' => 'open',
				'curpwd' => "open",
				'newpwd' => "open",
				'conpwd' => "open",
			    	'languageid' => 'integer',
			)
		);
		$this->apiParameterArr['token'] = array(
			'method' => 'POST',
			'compareDataType' => array(
				'name' => 'string',
				'prod' => 'string',
				'user_id' => 'integer'
			)
		);
		$this->apiParameterArr['reToken'] = array(
			'method' => 'POST',
			'compareDataType' => array(
				'name' => 'string',
				'prod' => 'string',
				'user_id'=> 'integer'
			)
		);
		
		$this->apiParameterArr['itemExist'] = array(
				'method' => 'POST',
				'compareDataType' => array(
						'tokenId' => 'open',
						'dataname' => 'string',
						'item_id' => 'integer',
						'dining_type' => 'string',
				        	'languageid' => 'integer',
				)
		);
		
		$this->apiParameterArr['getDetourDetails'] = array(
				'method' => 'GET',
				'compareDataType' => array(
						'tokenId' => 'open',
						'dataname' => 'string',
						'detour_id' => 'open'
				)
		);
		$this->apiParameterArr['mealPlanDetails'] = array(
    		    'method' => 'POST',
		    'compareDataType' => array(
		        'tokenId' => 'open',
		        'dataname' => 'string',
		        'user_id' => 'integer'
		    )
		);
		$this->apiParameterArr['locationAddressByLavutogoName'] = array(
				'method' => 'POST',
				'compareDataType' => array(
						'tokenId' => 'open',
						'lavu_togo_name' => 'string',
						'dataname' => 'string',
		
				)
		);
		$this->apiParameterArr['listUserAddress'] = [
            'method' => 'POST',
            'compareDataType' => [
                'tokenId' => 'open',
                'userId' => 'integer',
                'languageid' => 'integer',
            ],
        ];
        $this->apiParameterArr['saveAddress'] = [
            'method' => 'POST',
            'compareDataType' => [
                'tokenId' => 'open',
                'user_id' => 'integer',
                'name' => 'string',
                'address_1' => 'open',
                'address_2' => 'open',
                'country' => 'open',
                'state' => 'open',
                'city' => 'open',
                'zipcode' => 'open',
                'is_primary' => 'open',
                'id' => 'integer',
                'languageid' => 'integer',
            ],
        ];
        $this->apiParameterArr['deleteAddress'] = [
            'method' => 'POST',
            'compareDataType' => [
                'tokenId' => 'open',
                'user_id' => 'integer',
                'id' => 'integer',
                'languageid' => 'integer',
            ],
        ];
        $this->apiParameterArr['itemInventoryStatus'] = array(
        		'method' => 'POST',
        		'compareDataType' => array(
        				'tokenId' => 'open',
        				'dataname' => 'string',
        				'item_contents' => 'string'
        		)
        );
        
		$this->apiParameterArr['lavutogoLanguagePack'] = array(
			'method' => 'GET',
			'compareDataType' => array(
				'lavutogoname' => 'string'
			 )
		);

		$this->apiParameterArr['lavutogoLanguagePack'] = array(
			'method' => 'GET',
			'compareDataType' => array(
				'lavutogoname' => 'string'
			)
		);

		if($_SERVER['REQUEST_METHOD'] == 'OPTIONS'){
            header("HTTP/1.1 200 OK");
            header("Access-Control-Allow-Origin: *");
            header("Access-Control-Allow-Methods: GET, POST");
            header("Access-Control-Allow-Headers: Authorization, Access-Control-Allow-Headers, Origin, Accept, X-Requested-With, Content-Type, Access-Control-Request-Method, Access-Control-Request-Headers");
            header("Access-Control-Request-Headers: x-requested-with");
            exit;
		}else{
			$this->method = $_SERVER['REQUEST_METHOD'];
		}
		$this->request = $_SERVER['REDIRECT_URL'];
		if($this->method == "POST"){
			$this->_jsonpost = file_get_contents('php://input');
			$this->_postarr = json_decode($this->_jsonpost,TRUE);
			//for LP-6132 (escaping '/' for street address)
			if(array_key_exists('streetAddress',$this->_postarr))
			{
			    $this->_postarr['streetAddress'] = str_replace('/','||',$this->_postarr['streetAddress']);
			}
			foreach($this->_postarr as $key=>$val){
				if (is_array($val)){
					$val = json_encode($val);
				}
				$this->_poststr .= sprintf("%s/%s/",$key,$val);
			}
			$this->request .= "/".$this->_poststr;
		}
		$this->args = explode('/', rtrim($this->request, '/'));

		array_shift($this->args);
		array_shift($this->args);
		array_shift($this->args);  // From moving from /olo_api => /api_entry_point/olo
		if (array_key_exists(0, $this->args) && !is_numeric($this->args[0])) {
			$this->verb = array_shift($this->args);
			$this->endpoint = array_shift($this->args);
			$i=0;
			while(count($this->args)>$i) {
				if(is_numeric($this->args[$i+1]) == 1 && ($this->apiParameterArr[$this->endpoint]['compareDataType'][$this->args[$i]] == "integer" || $this->apiParameterArr[$this->endpoint]['compareDataType'][$this->args[$i]] == "open")){
					$this->_get[$this->args[$i]] = $this->args[$i+1];
					$i+=2;
				}elseif(is_numeric($this->args[$i+1]) == NULL && ($this->apiParameterArr[$this->endpoint]['compareDataType'][$this->args[$i]] == "string" || $this->apiParameterArr[$this->endpoint]['compareDataType'][$this->args[$i]] == "open")){
					$this->_get[$this->args[$i]] = $this->args[$i+1];
					$i+=2;
				}else{
					$this->_error = 'Y';
					return $this->_response('{"status":"Fail","msg:{"Property '. $this->args[$i]  .' datatype should be":'.'"'.$this->apiParameterArr[$this->endpoint]['compareDataType'][$this->args[$i]].'"}}', 406);
					$i+=2;
				}
			}
			if(strtoupper($this->verb) == 'GET' || strtoupper($this->verb) == 'POST') {
				$this->apiRequest['dataname'] = isset($this->_get['dataname'])?$this->_get['dataname']:'';
				$this->apiRequest['Auth']['dataname'] = isset($this->_get['dataname'])?$this->_get['dataname']:'';
				$this->apiRequest['Auth']['user_id'] = 1;
				$this->apiRequest['Params'] = array();
				$this->apiRequest['Params']['dataname'] = isset($this->_get['dataname'])?$this->_get['dataname']:'';
			}
		}
		if ($this->method == 'POST' && array_key_exists('HTTP_X_HTTP_METHOD', $_SERVER)) {
			if ($_SERVER['HTTP_X_HTTP_METHOD'] == 'DELETE') {
				$this->method = 'DELETE';
			} else if ($_SERVER['HTTP_X_HTTP_METHOD'] == 'PUT') {
				$this->method = 'PUT';
			} else {
				throw new Exception("Unexpected Header");
			}
		}
		switch ($this->method) {
			case 'DELETE':
			case 'POST':
				$this->request = $this->_cleanInputs($this->_get);
				break;
			case 'GET':
				$this->request = $this->_cleanInputs($this->_get);
				break;
			case 'PUT':
				$this->request = $this->_cleanInputs($_GET);
				$this->file = file_get_contents("php://input");
				break;
			default:
				$this->_response('Invalid Method', 405);
				break;
		}
		$this->processAPI();
	}

	public function authToken($args){
	}
	public function token($args){
	}
	public function reToken($args){
	}
	private function _cleanInputs($data) {
		$clean_input = Array();
		if (is_array($data)) {
			foreach ($data as $k => $v) {
				$clean_input[$k] = $this->_cleanInputs($v);
			}
		} else {
			$clean_input = trim(strip_tags($data));
		}
		return $clean_input;
	}
	public function processAPI() {
		if (method_exists($this, $this->endpoint)) {
			if($this->endpoint != 'token' && $this->endpoint != 'reToken'){
				$this->tokenId = $this->request['tokenId'];
				array_shift($this->request);
				$response = "success";
				if($response == "success"){
					if($this->method != $this->apiParameterArr[$this->endpoint]['method']){
						return $this->_response('{"status":"Fail","msg":{"Method Not Allowed":'. '"'.$this->method.'"}}', 405);
					}elseif($this->_error == 'N'){
						return $this->_response($this->{$this->endpoint}($this->request));
					}
				}else{
					return $response;
				}
			}else{
				if($this->method != $this->apiParameterArr[$this->endpoint]['method']){
					return $this->_response('{"status":"Fail","msg":{"Method Not Allowed":'. '"'.$this->method.'"}}', 405);
				}elseif($this->_error == 'N'){
					return $this->_response($this->{$this->endpoint}($this->request));
				}
			}
		}elseif($this->_error == 'Y'){
			return $this->_response('{"status":"Fail","msg":{"No Endpoint":'. '"'.$this->endpoint.'"}}', 404);
		}
	}
	private function _response($data, $status = 200) {
		header("HTTP/1.1 " . $status . " " . $this->_requestStatus($status));
		if($status != 200 || $this->endpoint == "token"){
			echo $data;
		}else{
			return $data;
		}

		return;
	}
	private function _requestStatus($code) {
		$status = array(
				200 => 'OK',
				404 => 'Not Found',
				405 => 'Method Not Allowed',
				406 => 'Property datatype is incorrect',
				500 => 'Internal Server Error',
				401 => 'Access Denied',
				407 => "Invalid Token"
		);
		return ($status[$code])?$status[$code]:$status[500];
	}
	public function getAuthorizationHeader(){
		$headers = null;
		if (isset($_SERVER['Authorization'])) {
			$headers = trim($_SERVER["Authorization"]);
		}
		else if (isset($_SERVER['HTTP_AUTHORIZATION'])) { //Nginx or fast CGI
			$headers = trim($_SERVER["HTTP_AUTHORIZATION"]);
		} elseif (function_exists('apache_request_headers')) {
			$requestHeaders = apache_request_headers();
			$requestHeaders = array_combine(array_map('ucwords', array_keys($requestHeaders)), array_values($requestHeaders));
			if (isset($requestHeaders['Authorization'])) {
				$headers = trim($requestHeaders['Authorization']);
			}
		}
		return $headers;
	}
	public function getBearerToken() {
		$headers = $this->getAuthorizationHeader();
		$parameters = array("jwt"=>$headers, "tokenId"=>$this->tokenId);
		$reponse = $this->authToken($parameters);
		$responseArr = json_decode($reponse,'true');
		if($responseArr['status'] == "fail"){
			return $this->_response('{"status":"Fail","msg":{"Authorization":"Invalid Token"}', 407);
			exit;
		}else{
			if($responseArr['data']['userId'] != 0){
				$args['tokenId'] = $responseArr['tokenId'];
				$args['userId'] = $responseArr['data']['userId'];
				$return =  json_decode($this->authSession($args), true);
				if($return['DataResponse'] == 'authenticated'){
					$response = "Success";
				}else{
					$response = "Fail";
				}
			}
			$response = "success";
		}

		return $response;
	}

	public function itemsList($args) {
	}
	public function itemDetails($args) {
	}
	public function Menus($args){
	}
	public function locationConfig($args) {
	}
	public function datanameDetails($args) {
	}
	public function lavutogolocationAddress($args) {
	}
	public function locationAddress($args) {
	}
	public function deliveryAddress($args) {
	}
	public function deliveryLocationFees($args) {
	}
	public function itemsTaxDetails($args) {
	}
	public function orderDetails($args) {
	}
	public function lavuGiftCardDetails($args) {
	}
	public function lavuLoyaltyPointsDetails($args) {
	}
	public function checkLavuAwardPoints($args) {
	}
	public function checkLoyaltyDiscountPoints($args) {
	}
	public function orderHistory($args) {
	}
	public function reOrder($args) {
	}
	public function userRegistration($args) {
	}
	public function userSignin($args) {
	}
	public function forgotPassword() {
	}
	public function updatePassword() {
	}
	public function userAccount($args) {
	}
	public function cardPayment($args) {
	}
	public function walletPayment($args) {
	}
	public function cod($args) {
	}
	public function ioId($args) {
	}
	public function pickUpDineInLocationAddress($args) {
	}
	public function getDetourDetails($args) {
	}
	public function mealPlanDetails($args) {
	}
	public function getLocationAddressByLavutogoName($args) {
	}
	public function listUserAddress() {
    }
    public function saveAddress() {
    }
    public function MealPlanInfo($args) {
	}
	public function deleteAddress($args) {
		
	}
	public function itemInventoryStatus($args) {
	}
	public function lavutogoLanguagePack() {
	}

}