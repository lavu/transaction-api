<?php
	$reactStyle = $isNewControlPanel ? "style='display:none'" : "";
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN""http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

	<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">

			<meta http-equiv="X-UA-Compatible" content="IE=9">
			 <meta http-equiv="X-UA-Compatible" content="IE=edge" />
		<title>POSLavu Admin Control Panel</title>
		<!-- Google Tag Manager -->
		<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push(
		{'gtm.start': new Date().getTime(),event:'gtm.js'}
		);var f=d.getElementsByTagName(s)[0],
		j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
		'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
		})(window,document,'script','dataLayer','GTM-WH5DR2D');</script>
		<!-- End Google Tag Manager -->
		<link rel="stylesheet" href="styles/ios-checkbox.css" type="text/css" media="screen" charset="utf-8" />
		<link rel="stylesheet" type="text/css" href="<?php echo Lavu\Helpers\getAssetPath("styles/styles.css"); ?>">
		<link rel="stylesheet" type="text/css" href="<?php echo Lavu\Helpers\getAssetPath("styles/info_window.css"); ?>">
		<link rel="stylesheet" type="text/css" href="<?php echo Lavu\Helpers\getAssetPath("styles/ieStyles.css"); ?>">
		<link rel="stylesheet" type="text/css" href="<?php echo Lavu\Helpers\getAssetPath("styles/multiaccordion/jquery-ui-1.8.16.greenModifiers.min.css"); ?>">
		<link rel="stylesheet" type="text/css" href="<?php echo Lavu\Helpers\getAssetPath("styles/multiaccordion/jquery-ui-1.8.16.grayMessages.min.css"); ?>">
		<script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"> </script>
		<script type="text/javascript" src="<?php echo Lavu\Helpers\getAssetPath("scripts/multiAccordion.js"); ?>"> </script>
		<link rel="stylesheet" type="text/css" href="<?php echo Lavu\Helpers\getAssetPath("styles/fontcustom/fontcustom.css"); ?>">
		<script type="text/javascrpt" src="<?php echo Lavu\Helpers\getAssetPath("scripts/pages/ministries.js"); ?>" ></script>
		<script type="text/javascript" src="<?php echo Lavu\Helpers\getAssetPath("scripts/customelements.js"); ?>"></script>
		<![CDATA[EXTRA_BEFORE_SCRIPTS]]>
	</head>
<body style="margin:0px; background-color:#ECEEEF;">
	<!-- Google Tag Manager (noscript) -->
	<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-WH5DR2D"
	height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
	<!-- End Google Tag Manager (noscript) -->
	 <!-- sticky notification -->
	<div id='help_popup' style='position:absolute; left:0px; top:0px; z-index:200; display:none;'>
		<table cellspacing='0' cellpadding='0' style='border: 1px solid black;'>
			<tr>
				<td style='background:URL(images/popup_bg.png); swidth:724px; height:471px' align='left' valign='top'>
					<table cellspacing='0' cellpadding='0'>
						<tr><td colspan='3' height='22px'>&nbsp;</td></tr>
						<tr>
							<td width='21px'>&nbsp;</td>
							<td height='20px' align='right' style='background:URL(images/popup_bar.png);'><a onclick='hideHelp();' style='cursor:pointer; color:#EEEEEE;'><b>X</b></a> &nbsp; </td>
							<td width='25px'>&nbsp;</td>
						</tr>
						<tr>
							<td width='21px'>&nbsp;</td>
							<td align='left'><div id='iframe_scroller' style='width:679px; height:405px; overflow:hidden; margin:0px 0px 0px 0px; padding:0px 0px 0px 0px;'>
							<iframe id='help_iframe' width='675px' height='401px' frameborder='1' scrolling='yes' src='' style='width:675px; height:401px;'></iframe></div></td>
							<td width='25px'>&nbsp;</td>
						</tr>
					</table>
				</td>
			</tr>
		</table>
	</div>
	<div id='aboveHeader' <?php echo $reactStyle; ?>>
	[cp4BetaToggleBanner]
	</div>
	<div id="header" <?php echo $reactStyle; ?>>
		<div id="logInfo">
			<div class="[cp4BetaToggleClass]">
				<span id='lavuLogo'></span>
				[cp4BetaToggleLink]
			</div>
			<table id='alignRightDiv'>

				<tr>

					<td width='380px'>
						[billingNotification]
					</td>
					<td>

						<div id="userName">
							[username]

						</div>
						<div id="separator">
							<img src="images/arrowIcon.jpg"/>
						</div>

						<div id="logStatus">
							<a style='text-decoration:none; color: #aecd37' href="index.php?logout=1" id="loggedIn">
								LOGOUT
								<img src="images/loggedIn.jpg" style='border:0;'class="logStatusIcon"/>
							</a>
						</div>
						<div id= "menuMyAccount">
							<a style='text-decoration:none; color: #aecd37' href='?mode=billing'>MY ACCOUNT</a>
						</div>

						<div id= "navigationDropDown">
							[locations]
							<!-- new feature database name created By:Kelly Campbell -->

							<div id= "databasename">
								[databaseview]
							</div>

						</div>
					</td>
				</tr>
			</table>
		</div>
	</div>
		<div id='underHeader' <?php echo $reactStyle; ?>></div>
	</div>

	<div id='center' <?php if($isNewControlPanel) echo "style='margin-top:-50px'"; ?>>
		<div id='lockout_billing'></div>
		<div id='lockout'></div>
		<div id='mainTable'>
			<table width='100%' cellspacing="0">

				<tr class='menu' <?php echo $reactStyle; ?>>
					<td style='border-bottom:1px solid black;'>
					</td>
					<td style=' background-color:#313235; width:100%;'>
						[menuContent]
					</td>
					<td style='border-bottom:1px solid black;width:5%'>
					</td>
				</tr>
				<tr >
					<td class='content' colspan=3 >
						<div style="position:relative" id="main_content_area" >
							<center>

							[internalMenu]
							[content]

							</center>
						</div>
					</td>
				</tr>
			</table>
		</div>
	</div>
	<lavu-blackout hidden>
		<lavu-initial-setup-area></lavu-initial-setup-area>
	</lavu-blackout>
</body>

<!-- Begin Lavu GA Code -->
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-30186945-2', 'auto');
  ga('send', 'pageview');

</script>
<!-- End Lavu GA Code -->

<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.14/jquery-ui.min.js"></script>
<script type="text/javascript" src="<?php echo Lavu\Helpers\getAssetPath("scripts/pages/reports.js"); ?>"></script>
<script type="text/javascript" src="<?php echo Lavu\Helpers\getAssetPath("scripts/pages/extensions.js"); ?>"></script>
<script src="scripts/jquery.cookie.js" type="text/javascript" charset="utf-8"></script>
<script src="scripts/jquery.howdydo-bar.js" type="text/javascript" charset="utf-8"></script>
<script src="scripts/iphone-style-checkboxes.js" type="text/javascript" charset="utf-8"></script>
<script type="text/javascript" src="<?php echo Lavu\Helpers\getAssetPath("scripts/index.js"); ?>"> </script>
<!--[if lte IE 8]><script language="javascript" type="text/javascript" src="scripts/jquery/flot/excanvas.min.js"></script><![endif]-->

<script language="javascript" type="text/javascript" src="scripts/jquery/flot/jquery.flot.js"></script>
<script language="javascript" type="text/javascript" src="scripts/jquery/flot/jquery.flot.pie.js"></script>
<script language="javascript" type="text/javascript" src="scripts/jquery/iframe_ipad.js"></script>

<script type="text/javascript" src="//assets.zendesk.com/external/zenbox/v2.6/zenbox.js"></script>
<style type="text/css" media="screen, projection">
  @import url(//assets.zendesk.com/external/zenbox/v2.6/zenbox.css);
</style>
<!-- Start of lavu Zendesk Widget script -->
<script>/*<![CDATA[*/window.zEmbed||function(e,t){var n,o,d,i,s,a=[],r=document.createElement("iframe");window.zEmbed=function(){a.push(arguments)},window.zE=window.zE||window.zEmbed,r.src="javascript:false",r.title="",r.role="presentation",(r.frameElement||r).style.cssText="display: none",d=document.getElementsByTagName("script"),d=d[d.length-1],d.parentNode.insertBefore(r,d),i=r.contentWindow,s=i.document;try{o=s}catch(c){n=document.domain,r.src='javascript:var d=document.open();d.domain="'+n+'";void(0);',o=s}o.open()._l=function(){var o=this.createElement("script");n&&(this.domain=n),o.id="js-iframe-async",o.src=e,this.t=+new Date,this.zendeskHost=t,this.zEQueue=a,this.body.appendChild(o)},o.write('<body onload="document._l();">'),o.close()}("//assets.zendesk.com/embeddable_framework/main.js","lavu.zendesk.com");/*]]>*/</script>
<!-- End of lavu Zendesk Widget script -->
<script type="text/javascript">
 $("<div class='footer' <?php echo $reactStyle; ?>><span class=\"copyright-footer\">© Lavu 2010-<span class=\"currYear\">2017</span> | All Rights Reserved | <a style='color:gray' href=\"https://www.lavu.com/terms-of-service.pdf\" target=\"_blank\">Terms of Service | Privacy Policy</a></span></div>").insertAfter("#mainTable");
// footer date dynamicly display 
	$(".currYear").text(new Date().getFullYear());
</script>
<!-- <p>Lavu &copy; <script type="text/javascript">var d = new Date();document.write(d.getFullYear());</script></p> -->

<!-- javascript that will auto upate year for copy right footer
<script type="text/javascript">
  document.write(new Date().getFullYear());
</script> -->
<!--[if IE 8]>
    <script type="text/javascript">
	    $(document).ready( function(){
	    	$("nav ul").css("width", "96%");
	    });
    </script>
<![endif]-->
<!--[if IE 9]>
    <script type="text/javascript">
	    $(document).ready( function(){
	    	$("nav ul").css("width", "96%");
	    });
    </script>
<![endif]-->
</html>
