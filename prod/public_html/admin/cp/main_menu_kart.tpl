
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN""http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

	<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">

			<meta http-equiv="X-UA-Compatible" content="IE=9">
			 <meta http-equiv="X-UA-Compatible" content="IE=edge" />
		<title>POSLavu Admin Control Panel</title>
		<link rel="stylesheet" href="styles/ios-checkbox.css" type="text/css" media="screen" charset="utf-8" />
		<link rel="stylesheet" type="text/css" href="<?php echo Lavu\Helpers\getAssetPath("styles/styles.css"); ?>">
		<link rel="stylesheet" type="text/css" href="<?php echo Lavu\Helpers\getAssetPath("styles/info_window.css"); ?>">
		<link rel="stylesheet" type="text/css" href="<?php echo Lavu\Helpers\getAssetPath("styles/ieStyles.css"); ?>">
		<link rel="stylesheet" type="text/css" href="<?php echo Lavu\Helpers\getAssetPath("styles/multiaccordion/jquery-ui-1.8.16.greenModifiers.min.css"); ?>">
		<link rel="stylesheet" type="text/css" href="<?php echo Lavu\Helpers\getAssetPath("styles/multiaccordion/jquery-ui-1.8.16.grayMessages.min.css"); ?>">
		<script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"> </script>
		<script type="text/javascript" src="<?php echo Lavu\Helpers\getAssetPath("scripts/multiAccordion.js"); ?>"> </script>
	</head>

<body style="margin:0px; background-color:#ECEEEF">

 <!-- sticky notification -->
<!--
  <div id="notify">POS Lavu Client 2.0.4 available in the app store.&nbsp;&nbsp;<a href="http://itunes.apple.com/us/app/poslavu-client/id391800399?mt=8">Upgrade Now</a></div>
-->

  <!--<div id="notify">Attention Lavu Localserver users : Please DO NOT upgrade your Localservers to the latest Mac OS X Mountain Lion at this time.</div>-->

	<div id='help_popup' style='position:absolute; left:0px; top:0px; z-index:200; display:none;'>
		<table cellspacing='0' cellpadding='0' style='border: 1px solid black;'>
			<tr>
				<td style='background:URL(images/popup_bg.png); width:724px; height:471px' align='left' valign='top'>
					<table cellspacing='0' cellpadding='0'>
						<tr><td colspan='3' height='22px'>&nbsp;</td></tr>
						<tr>
							<td width='21px'>&nbsp;</td>
							<td height='20px' align='right' style='background:URL(images/popup_bar.png);'><a onclick='hideHelp();' style='cursor:pointer; color:#EEEEEE;'><b>X</b></a> &nbsp; </td>
							<td width='25px'>&nbsp;</td>
						</tr>
						<tr>
							<td width='21px'>&nbsp;</td>
							<td align='left'><div id='iframe_scroller' style='width:679px; height:405px; overflow:hidden; margin:0px 0px 0px 0px; padding:0px 0px 0px 0px;'>
							<iframe id='help_iframe' width='675px' height='401px' frameborder='1' scrolling='yes' src="" style='width:675px; height:401px;'></iframe></div></td>
							<td width='25px'>&nbsp;</td>
						</tr>
					</table>
				</td>
			</tr>
		</table>
	</div>

	<div id="header">

		<div id="logInfo">
		<span id='lavuLogo'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
			<table id='alignRightDiv'>

				<tr>

					<td width='430px'>
						[billingNotification]
					</td>
					<td>

						<div id="userName">
							[username]

						</div>
						<div id="separator">
							<img src="images/arrowIcon.jpg"/>
						</div>

						<div id="logStatus">
							<a style='text-decoration:none; color: #aecd37' href="index.php?logout=1" id="loggedIn">
								LOGOUT
								<img src="images/loggedIn.jpg" style='border:0;'class="logStatusIcon"/>
							</a>
						</div>
						<div id= "menuMyAccount">
							<a style='text-decoration:none; color: #aecd37' href='?mode=billing'>MY Money </a>
						</div> 
						<div id="navigationDropDown">
							[locations]

						</div>
					</td>
				</tr>
			</table>
		</div>
	</div>
		<div id= "underHeader">
		</div>

	<div id= "center">
		<div id= "mainTable">



			<table width='100%' cellspacing="0">

				<tr class='menu'>
					<td style='border-bottom:1px solid black; width:5% '>
					</td>
					<td width='75%' style='background-color:#313235;'>
						[menuContent]
					</td>
					<td style='border-bottom:1px solid black;width:5%'>
					</td>
				</tr>
				<tr >
					<td class='content' colspan=3 style='background-image:url(images/flag_background.png); background-repeat:no-repeat; background-size:100%;'>
						<div style="position:relative" id="main_content_area" >
							<center>

							[internalMenu]
							[content]

							</center>
						</div>
					</td>
				</tr>
			</table>
		</div>
	</div>

<!-- Begin Lavu GA Code -->
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-30186945-2', 'auto');
  ga('send', 'pageview');

</script>
<!-- End Lavu GA Code -->

	<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.6.2/jquery.min.js"></script>
	<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.14/jquery-ui.min.js"></script>
	<script type="text/javascript" src="<?php echo Lavu\Helpers\getAssetPath("scripts/pages/reports.js"); ?>"></script>
	<script type="text/javascript" src="<?php echo Lavu\Helpers\getAssetPath("scripts/pages/extensions.js"); ?>"></script>
	<script src="scripts/jquery.cookie.js" type="text/javascript" charset="utf-8"></script>
	<script src="scripts/jquery.howdydo-bar.js" type="text/javascript" charset="utf-8"></script>
	<script src="scripts/iphone-style-checkboxes.js" type="text/javascript" charset="utf-8"></script>
	<script type="text/javascript" src="<?php echo Lavu\Helpers\getAssetPath("scripts/index.js"); ?>"> </script>
	<!--[if lte IE 8]><script language="javascript" type="text/javascript" src="/newcp/backend/js/jquery/flot/excanvas.min.js"></script><![endif]-->

	<script language="javascript" type="text/javascript" src="scripts/jquery/flot/jquery.flot.js"></script>
	<script language="javascript" type="text/javascript" src="scripts/jquery/flot/jquery.flot.pie.js"></script>
	<script language="javascript" type="text/javascript" src="scripts/jquery/iframe_ipad.js"></script>

	<!--[if IE 8]>
	    <script type="text/javascript">
		    $(document).ready( function(){
		    	$("nav ul").css("width", "96%");
		    });
	    </script>
	<![endif]-->
	<!--[if IE 9]>
	    <script type="text/javascript">
		    $(document).ready( function(){
		    	$("nav ul").css("width", "96%");
		    });
	    </script>
	<![endif]-->
</body>
</html>
