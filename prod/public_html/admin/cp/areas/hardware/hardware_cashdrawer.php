<style type="text/css">
<!--
.style3 {color: #FF0000}
-->
body {
	background-color: #FFF;
}
</style>
<div style="position:relative" id="main_content_area">
              <script language='JavaScript' type="text/javascript">function form1_validate() { document.form1.submit();
} </script>
              <form action='' method='post' name='form1' id="form2">
                <input type='hidden' name='posted' value='1' />
<table width="530" align="center" id="maintext" style='border:solid 0px #aaaaaa'>
  <tr>
    <td width="500" colspan='2' align="left"><table width="520" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td width="40"><img src="images/i_hardware_sm.png" width="30" height="35" /></td>
        <td class="sectiontitle">Cash Drawers</td>
        </tr>
    </table></td>
    </tr>
  <tr>
    <td height="50" colspan="2" align='left'  valign='top'><p>Almost any cash drawer with a connection to the receipt printer will work with our software. </p>
      <p>The choice is up to you whether you prefer a smaller or larger drawer, with locking parts or not. </p>
      <p>Our software will trigger the drawer to open when a cash transaction is required, or the &ldquo;open register&rdquo; button is pressed.</p>
      <p><br />
      </p></td>
    </tr>
  <tr>
    <td height="50" colspan="2" align='left'  valign='bottom'><table width="522" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td width="130"><img src="images/pag.png" alt="apg" width="118" height="45" /></td>
        <td><strong>APG Cash drawer VBS320-BL1616</strong><br />
          Starting from<span class="style3"> $100</span> <br />
          <a href="http://zephyrhardware.com/Shop-Cash-Drawer/" target="_blank">click here to order: APG Cash drawer</a></td>
      </tr>
    </table>
      <p>* IF CABLE IS NOT INCLUDED.. Be sure to order the correct Cash Drawer cable for the Cash Drawer that you select and the make of printer.</p>
      <p>&nbsp;</p>
      <table border="0" cellspacing="0" cellpadding="1">
        <tr>
          <td align="left"><strong>CASH DRAWER CABLES</strong></td>
        </tr>
        <tr>
          <td align="left">- For Star Micronics Printers: <a href="http://www.posmicro.com/partNumbers/CD-014A.htm">320 MultiPro Cable Kit - CD-014A </a><br />
            - For Samsung (BIXOLON) Printers: <a href="http://www.pcrush.com/product/Data-Transfer-Cables/622228/APG-Cash-Drawers-CD-005A-Data-Transfer-Cable?refid=1643">APG Cash Drawers CD-005A Data Transfer Cable</a><br />
            - For Epson Printers: <a href="http://www.pcrush.com/product/Data-Transfer-Cables/622228/APG-Cash-Drawers-CD-005A-Data-Transfer-Cable?refid=1643">APG Cash Drawers CD-005A Data Transfer Cable</a></td>
        </tr>
      </table>
      <p>&nbsp;</p>
      <table border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td width="130"><a href="http://www.zephyrhardware.com/" target="_blank"><img src="images/zephyr_small.png" title="Zephyr Hardware for POSLavu Point of Sale " width="121" height="32" border="0" /></a></td>
          <td align="left" valign="middle">Our prefered Hardware provider is Zephyr Hardware. <br />
            They will get you connected with the appropriate hardware solutions for your needs.<br />
            <a href="http://www.zephyrhardware.com/" target="_blank">Zephyr Hardware</a></td>
        </tr>
      </table>
      <p>&nbsp;</p></td>
  </tr>
  </table>
  </form>
              <br />
            </div>