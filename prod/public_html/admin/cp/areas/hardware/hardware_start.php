<style type="text/css">
<!--
-->
body {
	background-color: #FFF;
}
</style>
<div style="position:relative" id="main_content_area">
              <script language='JavaScript' type="text/javascript">function form1_validate() { document.form1.submit();
} </script>
              <form action='' method='post' name='form1' id="form2">
                <input type='hidden' name='posted' value='1' />
<table width="530" align="center" id="maintext" style='border:solid 0px #aaaaaa'>
  <tr>
    <td width="500" colspan='2' align="left"><table width="520" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td width="40"><img src="images/i_hardware_sm.png" width="30" height="35" /></td>
        <td class="sectiontitle">Hardware Guide</td>
        </tr>
    </table></td>
    </tr>
  <tr>
    <td height="50" colspan="2" align='left'  valign='top'><p>Your POSLavu software is designed to work with preferred hardware that we<br />
      have tested and integrated with our application. </p>
      <p><br />
      </p></td>
    </tr>
  <tr>
    <td height="50" colspan="2" align='center'  valign='middle'><img src="images/hardware_devices.jpg" width="291" height="112" /></td>
  </tr>
  <tr>
    <td colspan="2" align='left'  valign='top'><p>The following categories of hardware components show which manufacturers and models currently work with our system, as well as where to order them. </p>
      <p>All of the following hardware can be packaged, configured, and installed by one of our Certified POSLavu Distributers. The same components can also be ordered for Self-Install.</p></td>
  </tr>
  <tr>
    <td colspan="2" align='left'  valign='top'><p>&nbsp;</p>
      <table border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td width="180"><a href="http://www.zephyrhardware.com/" target="_blank"><img src="images/zephyr_logo.jpg" alt="Zephyr Hardware for POSLavu Point of Sale " width="163" height="45" border="0" /></a></td>
          <td align="left" valign="middle">Our prefered Hardware provider is Zephyr Hardware. <br />
            They will get you connected with the appropriate hardware solutions for your needs.<br />
            <a href="http://www.zephyrhardware.com/" target="_blank">Zephyr Hardware</a></td>
        </tr>
      </table></td>
  </tr>
  </table>
  </form>
              <br />
            </div>