<style type="text/css">
<!--
-->
body {
	background-color: #FFF;
}
</style>
<div style="position:relative" id="main_content_area">
              <script language='JavaScript' type="text/javascript">function form1_validate() { document.form1.submit();
} </script>
              <form action='' method='post' name='form1' id="form2">
                <input type='hidden' name='posted' value='1' />
<table width="530" align="center" id="maintext" style='border:solid 0px #aaaaaa'>
  <tr>
    <td width="500" colspan='2' align="left"><table width="520" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td width="40"><img src="images/i_hardware_sm.png" width="30" height="35" /></td>
        <td class="sectiontitle">Routers, Bridges, Switches</td>
        </tr>
    </table></td>
    </tr>
  <tr>
    <td height="50" colspan="2" align='left'  valign='top'><p>Hard-wired printers can be transformed into wireless printers, capable  of receiving data transmitted from Apple&reg; multi-touch devices used in your  front-end operation.&nbsp; This allows you to  use our Direct Printing feature with non-wireless printers.&nbsp;</p>
<p><br />
      </p></td>
    </tr>
  <tr>
    <td height="50" colspan="2" align='center'  valign='bottom'><img src="images/how_it_works_sm.png" width="320" height="221" /></td>
  </tr>
  <tr>
    <td colspan="2" align='left'  valign='bottom'><p>&nbsp;</p>
      <p><strong>For USB  printers:</strong> </p>
      <table width="522" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td><img src="images/airport.jpg" width="100" height="85" /></td>
          <td><u>Apple</u><u> Airport</u><u> Express&reg;</u> - USB  out.&nbsp; Assigns IP Address to a USB wired  printer.&nbsp; 802.11n WiFi signal.&nbsp; Comes with an easy utility to install the  device on a computer and configure.</td>
        </tr>
        <tr>
          <td>&nbsp;</td>
          <td>&nbsp;</td>
          </tr>
        <tr>
          <td><img src="images/airportex_sm.png" width="100" height="49" /></td>
          <td><u>Apple Airport Extreme&reg;</u> - Same  function as the Airport Express&reg;, but with extended range of signal, and  multiple ports.</td>
        </tr>
      </table></td>
  </tr>
  </table>
  </form>
              <br />
            </div>