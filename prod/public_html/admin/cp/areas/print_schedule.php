<?php
	if($in_lavu)
	{
		function display_schedule_datetime($dt,$frmt="D m/d h:i a")
		{
			// based on date format setting
			$date_setting = "date_format";
			$location_date_format_query = lavu_query( "SELECT `location`, `setting`, `value` FROM `config` WHERE `setting` = '[1]'",  $date_setting);
			if( !$location_date_format_query || !mysqli_num_rows($location_date_format_query)){
				$date_format = array("value" => 2);
			}else
				$date_format = mysqli_fetch_assoc( $location_date_format_query );

			switch ($date_format['value']){
				case 1: $frmt = "D d/m h:i a";
				break;
				default: $frmt = "D m/d h:i a";
			}

			$dt_parts = explode(" ",$dt);
			if(count($dt_parts) > 1)
			{
				$tm_parts = explode(":",$dt_parts[1]);
				$dt_parts = explode("-",$dt_parts[0]);
				
				if(count($tm_parts) > 2 && count($dt_parts) > 2)
				{
					$year = $dt_parts[0];
					$month = $dt_parts[1];
					$day = $dt_parts[2];
					
					$hour = $tm_parts[0];
					$minute = $tm_parts[1];
					$seconds = $tm_parts[2];
					
					return date($frmt,mktime($hour,$minute,$seconds,$month,$day,$year));
				}
			}
			return $dt;
		}
		
		function display_schedule_day($dt)
		{
			return display_schedule_datetime($dt . " 00:00:00","D m/d");
		}
		
		function simple_date_add($dt,$n=0)
		{
			$dt = explode(" ",$dt);
			$dt = explode("-",$dt[0]);
			if(count($dt) > 2)
			{
				$new_ts = mktime(0,0,0,$dt[1],$dt[2] + $n,$dt[0]);
				return date("Y-m-d",$new_ts);
			}
			else return $dt;
		}
		
		function simple_time_get_hours_and_mins($tm)
		{
			$tm_parts = explode(" ",$tm);
			$tm = $tm_parts[count($tm_parts) - 1];
			$tm_parts = explode(":",$tm);
			if(count($tm_parts)>1)
			{
				$hours = $tm_parts[0];
				$mins = $tm_parts[1];
			}
			else if(is_numeric($tm))
			{
				$mins = $tm % 100;
				$hours = ($tm - $mins) / 100;
			}
			else
			{
				$mins = 0;
				$hours = 0;
			}
			return array("hours"=>$hours,"mins"=>$mins);
		}
		
		function simple_time_add($tm,$n=0)
		{
			$tm1 = simple_time_get_hours_and_mins($tm);
			$tm2 = simple_time_get_hours_and_mins($n);
			
			$tm_hours = $tm1['hours'] + $tm2['hours'];
			$tm_mins = $tm1['mins'] + $tm2['mins'];
			
			if($tm_mins >= 60)
			{
				$tm_hours++;
				$tm_mins -= 60;
			}
			
			if($tm_hours < 10) $tm_hours = "0" . ($tm_hours * 1);
			if($tm_mins < 10) $tm_mins = "0" . ($tm_mins * 1);
			
			return $tm_hours . ":" . $tm_mins;
		}
		
		function simple_time_output($tm,$frmt="h:i a")
		{
			$tm = simple_time_get_hours_and_mins($tm);
			
			$tm_hours = $tm['hours'];
			$tm_mins = $tm['mins'];
			$ts = mktime($tm_hours,$tm_mins,0,1,1,2014);
			
			return date($frmt,$ts);
			//if($tm_hours >= 24) $tm_hours -= 24;
			//$ap = "am";
			//if($tm_hours >= 12) $ap = "pm";
			//if($tm_hours * 1 == 0) $tm_hours = "12";
			
			//return ($tm_hours * 1) . ":" . $tm_mins . " " . $ap;
		}
		// based on date format setting
		$date_setting = "date_format";
		$location_date_format_query = lavu_query( "SELECT `location`, `setting`, `value` FROM `config` WHERE `setting` = '[1]'",  $date_setting);
		if( !$location_date_format_query || !mysqli_num_rows($location_date_format_query)){
			$date_format = array("value" => 2);
		}else
			$date_format = mysqli_fetch_assoc( $location_date_format_query );

		$start_date = urlvar("start_date");
		$end_date = urlvar("end_date");
		$start_date_format = explode("-", $start_date);
		$end_date_format = explode("-", $end_date);
		switch ($date_format['value']){
			case 1:$startdate = trim($start_date_format[2])."-".trim($start_date_format[1])."-".trim($start_date_format[0]);
			$enddate = trim($end_date_format[2])."-".trim($end_date_format[1])."-".trim($end_date_format[0]);
			break;
			case 2:$startdate = trim($start_date_format[1])."-".trim($start_date_format[2])."-".trim($start_date_format[0]);
			$enddate = trim($end_date_format[1])."-".trim($end_date_format[2])."-".trim($end_date_format[0]);
			break;
			case 3:$startdate = trim($start_date_format[0])."-".trim($start_date_format[1])."-".trim($start_date_format[2]);
			$enddate = trim($end_date_format[0])."-".trim($end_date_format[1])."-".trim($end_date_format[2]);
			break;
			default:$startdate = trim($start_date_format[1])."-".trim($start_date_format[2])."-".trim($start_date_format[0]);
			$enddate = trim($end_date_format[1])."-".trim($end_date_format[2])."-".trim($end_date_format[0]);
		}

		$class_list = array();
		$class_count = array();
		$class_list['E'] = array('title'=>"General Employee",'type'=>"GEN",'_deleted'=>0,'id'=>0,'tipout_rules'=>'');
		$class_count['E'] = 0;
		$class_query = lavu_query("select * from `emp_classes`");
		while($class_read = mysqli_fetch_assoc($class_query))
		{
			$class_list["E".$class_read['id']] = $class_read;
			$class_count["E".$class_read['id']] = 0;
		}
		
		$user_list = array();
		$user_query = lavu_query("select * from `users` where `_deleted`!='1'");
		while($user_read = mysqli_fetch_assoc($user_query))
		{
			$user_list[$user_read['id']] = $user_read;
			$class_count["E".$user_read['employee_class']]++;
		}
		
		$date_list = array();
		$cdate = simple_date_add($start_date,0);
		$rcount = 0;
		while($cdate <= $end_date && $rcount < 100)
		{
			$date_list[] = $cdate;
			$cdate = simple_date_add($cdate,1);
			$rcount++;
		}
		
		$no_coverage_complete = "AND `data_long` NOT LIKE '%coverage_requested=accepted%' ";
		
		$shift_query_str = "";
		$shift_query_str .= "SELECT * FROM `user_schedules` WHERE `loc_id`='[1]' AND ";
		$shift_query_str .= "`start_datetime` >= '[2]' AND `start_datetime` <= '[3]' ";
		$shift_query_str .= $no_coverage_complete;
		$shift_query_str .= " ORDER BY `start_datetime` ASC, `end_datetime` ASC";
		$shift_query = lavu_query($shift_query_str, $locationid, $start_date . " 00:00:00", $end_date . " 24:00:00");
		
		$display_type = (isset($_GET['display_type']))?$_GET['display_type']:"graph";
		if($display_type=="chart") $cellpadding = 8; else $cellpadding = 2;
		
		echo "<style>";
		echo "	body, table { font-family:Verdana,Arial; font-size:10px; } ";
		echo "  .time_header { font-family:Verdana,Arial; font-size:8px; } ";
		echo "</style>";
		echo "<table cellspacing=0 cellpadding=$cellpadding>";
		echo "<tr><td bgcolor='#777777' style='color:#ffffff; font-weight:bold' align='center' colspan='188'>";
		echo "Schedule for $startdate to $enddate";
		echo "</td></tr>";

		/*echo "<tr>";
		echo "<td>&nbsp;</td>";
		for($i=0; $i<count($date_list); $i++) 
		{
			echo "<td>" . display_schedule_day($date_list[$i]) . "</td>";
		}
		echo "</tr>";
		
		while($shift_read = mysqli_fetch_assoc($shift_query))
		{
			$start_time = display_schedule_datetime($shift_read['start_datetime']);
			$end_time = display_schedule_datetime($shift_read['end_datetime']);
			$user_info = $user_list[$shift_read['user_id']];
			$user_fullname = $user_info['f_name'] . " " . $user_info['l_name'];
			//foreach($shift_read as $key => $val) echo $key . " = " . $val . "<br>";
			
			echo "<tr><td align='right'>";
			echo $user_fullname;// . " " . $start_time . " - " . $end_time;
			echo "</td></tr>";
		}*/
		
		$ordered_shifts = array();
		$users_have_shifts = array();
		$min_time = "24:00";
		$max_time = "00:00";
		while($shift_read = mysqli_fetch_assoc($shift_query))
		{
			$shift_date = substr($shift_read['start_datetime'],0,10);
			//$start_time = display_schedule_datetime($shift_read['start_datetime']);
			//$end_time = display_schedule_datetime($shift_read['end_datetime']);
			//$user_info = $user_list[$shift_read['user_id']];
			//$user_fullname = $user_info['f_name'] . " " . $user_info['l_name'];
			$start_time = simple_time_output($shift_read['start_datetime'],"H:i");
			$end_time = simple_time_output($shift_read['end_datetime'],"H:i");
			
			$start_time_num = str_replace(":","",$start_time) * 1;
			$end_time_num = str_replace(":","",$end_time) * 1;
			if(is_numeric($end_time_num) && is_numeric($start_time_num) && $end_time_num < $start_time_num)
			{
				$end_time = simple_time_add($end_time,"24:00");
				$end_time_parts = explode("-",substr($shift_read['end_datetime'],0,10));
				$end_time_year = $end_time_parts[0];
				$end_time_month = $end_time_parts[1];
				$end_time_day = $end_time_parts[2];
				$set_end_day = date("Y-m-d",mktime(0,0,0,$end_time_month,$end_time_day - 1,$end_time_year));
				$shift_read['end_datetime'] = $set_end_day . " " . $end_time . ":00";
				
				if(isset($_GET['show_info']))
				{
					echo substr($shift_read['end_datetime'],0,10) . " " . $end_time . ":00" . "<br>";
				}
			}

			if($start_time < $min_time) $min_time = $start_time;
			if($end_time > $max_time) $max_time = $end_time;
			
			if(!isset($ordered_shifts[$shift_date])) $ordered_shifts[$shift_date] = array();
			if(!isset($ordered_shifts[$shift_date][$shift_read['user_id']])) $ordered_shifts[$shift_date][$shift_read['user_id']] = array();
			$ordered_shifts[$shift_date][$shift_read['user_id']][] = $shift_read;
			$users_have_shifts[$shift_read['user_id']] = true;
		}
		
		$start_time = $min_time;//"01:00";
		$end_time = $max_time;//"26:00";
		
		$start_time_parts = explode(":",$start_time);
		if(count($start_time_parts) > 1)
		{
			$start_time = $start_time_parts[0] . ":00";
		}
		
		$time_span = "00:15";
		$time_span_header = "01:00";
		$time_span_header_colspan = 4;
		
		if($display_type=="chart")
		{
			echo "<tr>";
			echo "<td bgcolor='#777777' style='border-top:solid 1px #cccccc'>&nbsp;</td>";
			for($i=0; $i<count($date_list); $i++) 
			{
				echo "<td bgcolor='#777777' style='border-top:solid 1px #cccccc; color:#ffffff'>";
				echo display_schedule_day($date_list[$i]) . "&nbsp;";
				echo "</td>";
			}
			echo "</tr>";
			
			foreach($class_list as $class_id => $class_info)
			{
				foreach($user_list as $user_id => $user_info)
				{
					$user_classes = array();
					$user_class_arr = explode(",",$user_info['role_id']);
					for($n=0; $n<count($user_class_arr); $n++)
					{
						$user_class_inner_arr = explode("|",$user_class_arr[$n]);
						$user_class_str = trim($user_class_inner_arr[0]);
						if($user_class_str!="") $user_classes[$user_class_str] = 1;
					}
					
					if($user_info['active']=="1" && $class_id=="E" && isset($users_have_shifts[$user_id]))// && count($date_shifts[$user_id]) > 0)
					{
						$user_fullname = $user_info['f_name'] . " " . $user_info['l_name'];
						$user_class = "&nbsp;";
						if($class_count[$class_id] > 0 && count($class_list) > 1)
						{
							//$user_class = $class_info['title'];
						}
						
						echo "<tr>";
						echo "<td style='border-left:solid 1px black; border-right:solid 1px black; border-bottom:solid 1px black' valign='top' align='right'>" . $user_fullname . "</td>";
						for($i=0; $i<count($date_list); $i++) 
						{
							$date_shifts = array();
							if(isset($ordered_shifts[$date_list[$i]])) $date_shifts = $ordered_shifts[$date_list[$i]];
							
							$user_schedules = array();
							if(isset($date_shifts[$user_id])) $user_schedules = $date_shifts[$user_id];
							
							echo "<td style='border-right:solid 1px black; border-bottom:solid 1px black' valign='top'>";
							if(count($user_schedules) > 0)
							{
								for($s=0; $s<count($user_schedules); $s++)
								{
									$shift_read = $user_schedules[$s];
									$shift_id = $shift_read['id'];
									
									$set_style = "";
									if($s<count($user_schedules) - 1) $set_style = "border-bottom:solid 1px #dddddd";
									echo "<table cellspacing=0 cellpadding=4 width='100%'><tr><td align='center' width='100%' style='$set_style'>";
									echo simple_time_output($shift_read['start_datetime'],"g:ia") . " - " . simple_time_output($shift_read['end_datetime'],"g:ia");
									
									if(isset($shift_read['data1']) && $shift_read['data1']!="")
									{
										$shift_class_id = "E" . $shift_read['data1'];
										if(isset($class_list[$shift_class_id]))
										{
											$class_title = $class_list[$shift_class_id]['title'];
											echo "<br><font style='color:#778855; font-size:10px; font-weight:bold'>$class_title</font>";
										}
									}
									echo "</td></tr></table>";
								}
							}
							else
							{
								echo "&nbsp;";
							}
							echo "</td>";
						}
						echo "</tr>";
					}
				}
			}
		}
		else
		{
			for($i=0; $i<count($date_list); $i++) 
			{
				$date_shifts = array();
				if(isset($ordered_shifts[$date_list[$i]])) $date_shifts = $ordered_shifts[$date_list[$i]];
				
				$t = $start_time;
				$rcount = 0;
				echo "<tr bgcolor='#dddddd'>";
				echo "<td align='right' colspan='2'>";
				echo display_schedule_day($date_list[$i]) . "&nbsp;";
				echo "</td>";
				while($t >= 0 && $t < $end_time && $rcount < 2000)
				{
					$show_time = simple_time_output($t,"g:i");
					//if(strpos($show_time,":00")===false) $show_time = "&nbsp;";
					echo "<td class='time_header' colspan='$time_span_header_colspan' style='border-left:solid 1px white'><nobr>" . $show_time . "</nobr></td>";
					$t = simple_time_add($t,$time_span_header);
					$rcount++;
				}
				echo "</tr>";
				
				foreach($class_list as $class_id => $class_info)
				{
					foreach($user_list as $user_id => $user_info)
					{
						$user_classes = array();
						$user_class_arr = explode(",",$user_info['role_id']);
						for($n=0; $n<count($user_class_arr); $n++)
						{
							$user_class_inner_arr = explode("|",$user_class_arr[$n]);
							$user_class_str = trim($user_class_inner_arr[0]);
							if($user_class_str!="") $user_classes[$user_class_str] = 1;
						}
						
						if($user_info['active']=="1" && $class_id=="E" && isset($users_have_shifts[$user_id]))// && count($date_shifts[$user_id]) > 0)
						{
							//$start_time = display_schedule_datetime($shift_read['start_datetime']);
							//$end_time = display_schedule_datetime($shift_read['end_datetime']);
							//$user_info = $user_list[$shift_read['user_id']];
							$user_fullname = $user_info['f_name'] . " " . $user_info['l_name'];
							$user_class = "&nbsp;";
							if($class_count[$class_id] > 0 && count($class_list) > 1)
							{
								//$user_class = $class_info['title'];
							}
							$user_schedules = array();
							if(isset($date_shifts[$user_id])) $user_schedules = $date_shifts[$user_id];
							
							$shifts_shown = array();
							$t = $start_time;
							$rcount = 0;
							echo "<tr>";
							echo "<td align='right' style='color:#cccccc'><nobr>" . $user_class . "&nbsp;</nobr></td>";
							echo "<td align='right' style='color:#777777'><nobr>" . $user_fullname . "&nbsp;</nobr></td>";
							while($t >= 0 && $t < $end_time && $rcount < 2000)
							{
								if(isset($_GET['ttt'])) echo $t . "<br>";
								$min_shift_time = $date_list[$i] . " " . $t . ":00";//. simple_time_output($t,"H:i:s");
								$bgcolor = "#eeeeee";
								$alt_tag = "";
								$shift_id = "";
								for($s=0; $s<count($user_schedules); $s++)
								{
									$shift_read = $user_schedules[$s];
									if(isset($_GET['test']))
									{
										foreach($shift_read as $sk => $sv) echo $sk . " = " . $sv . "<br>";
									}
									if($shift_read['start_datetime'] <= $min_shift_time && $shift_read['end_datetime'] > $min_shift_time)
									{
										$shift_id = $shift_read['id'];
										$bgcolor = "#aabbcc";
										$alt_tag = simple_time_output($shift_read['start_datetime'],"g:ia") . " - " . simple_time_output($shift_read['end_datetime'],"g:ia");
										$alt_tag = str_replace("m","",$alt_tag);
									}
								}
								$cell_style = "";
								if(strpos($t,":00")!==false) $cell_style = "border-left:solid 1px white";
								
								echo "<td class='time_header' bgcolor='$bgcolor' title='$alt_tag' alt='$alt_tag' style='border-bottom:solid 1px #ffffff; ".$cell_style."'>";
								
								if(!isset($shifts_shown[$shift_id]) && trim($alt_tag)!="")
								{
									echo "<div style='position:relative'>";
									echo "<div style='position:absolute; left:0px; top:-7px; z-index:200'><table style='background-color:#aabbcc'><tr><td style='font-size:7px; color:#000077'><nobr>&nbsp;$alt_tag&nbsp;</nobr></td></tr></table></div>";
									echo "</div>";
								}
								else
								{
									echo "<nobr>&nbsp;</nobr>";
								}
								$shifts_shown[$shift_id] = 1;
								echo "</td>";
								$t = simple_time_add($t,$time_span);
								$rcount++;
							}
							echo "</tr>";
						}
					}
				}
			}
		}
		
		echo "</table>";
		
		
		$note_query = lavu_query("select * from `user_schedules` where `date`='[1]'","notes_".display_schedule_datetime($start_date . " 00:00:00","m/d/y"));
		if(mysqli_num_rows($note_query))
		{
			$note_read = mysqli_fetch_assoc($note_query);
			$note_str = $note_read['data_long'];
		}
		else $note_str = "";
		
		if(trim($note_str)!="")
		{
			echo "<br><br><font style='font-size:14px; font-family:Arial'>" . str_replace("\n","<br>",str_replace("<","&lt;",strip_tags($note_str))) . "</font>";
		}
	}
?>
