<?php

if (!$in_lavu)
{
	exit();
}

echo "<br><br>";

$form_posted = (isset($_POST['posted']));

$display	= "";
$field		= "";

$qtitle_category	= FALSE;
$qtitle_item		= "Type";

$category_tablename			= "";
$inventory_tablename		= "order_tags";
$category_fieldname			= "";
$inventory_fieldname		= "id";
$inventory_select_condition	= " AND `id` not in (1)";
$inventory_primefield		= "name";
$category_primefield		= "";

$inventory_orderby = "id";

$qfields = array(
	array(
		"name",
		"name",
		speak("Type Name"),
		"input"
	),
	array(
		"tax_exempt",
		"tax_exempt",
		"Tax Exempt",
		"select",
		array(
			array( speak("Taxable"), "0" ),
			array( speak("Tax exempt"), "1" )
		)
	),
	array(
		"active",
		"active",
		"Active",
		"select",
		array(
			array( speak("Active"), "1" ),
			array( speak("Inactive"), "0" )
		)
	),
	array( "id", "id" ),
	array( "delete" )
);

$display .= "<style>
		.ui-dialog .ui-dialog-titlebar {
			background-image: none;
			background-color: #98b624;
		}
		.ui-button-text {
			background-image: none;
			background-color: #98b624;
		}
		</style>";
$display .= "<script language='javascript'>
				function getOrderTypeInfo(hidval,type) {
                   var hid_prompt = document.getElementById(hidval);

                   if(type === true)
					{  
                        if(hidval == 'config:order_tag_table_prompt'){ 
                            document.getElementById('config:order_tag_default_table').disabled=true;
                        }
                        else if(hidval == 'config:order_tag_tab_prompt'){
                            document.getElementById('config:order_tag_default_tab').disabled = true;
                        }
						hid_prompt.value = 1;
					}
					else
					{ 
                        if(hidval == 'config:order_tag_table_prompt'){ 
                            document.getElementById('config:order_tag_default_table').disabled=false;
                         }
                        else if(hidval == 'config:order_tag_tab_prompt'){
                            document.getElementById('config:order_tag_default_tab').disabled = false;
                        }
					    hid_prompt.value = '0';
					}
				}
				function showConfirmBox(message, hidval, type) {

					var hid_prompt = document.getElementById(hidval);

					if(type === false)
					{
					    $('<div></div>').appendTo('body')
					    .html('<div><h4>'+message+'?</h4></div>')
					    .dialog({
					        modal: true, title: 'WARNING', zIndex: 10000, autoOpen: true,
					        width: '500', resizable: false,
					        buttons: {
					            Yes: function () {
							hid_prompt.value = 0;
					                $(this).dialog(\"close\");
					            },
					            No: function () {
							hid_prompt.checked = 1;
							document.getElementById('orderTypeCheck').checked = true;
					                $(this).dialog(\"close\");
					            }
					        },
					        close: function (event, ui) {
							if(event.which == 1) {
								document.getElementById('orderTypeCheck').checked = true;
							}
					            $(this).remove();
					        }
					    });
					}
					else
					{
						hid_prompt.value = 1;
					}
				}
			</script>";

$display .= "<script type=\"text/javascript\" src=\"/manage/js/jquery/js/jquery-confirm.min.js\"></script>";
require_once(resource_path()."/dimensional_form.php");

$qdbfields = get_qdbfields($qfields);

$settings = array(
	array(
		speak("Include order type labels on print jobs").":",
		"order_tag_print_option",
		"select"
	),
	array(
		speak("Position of order type label on kitchen tickets").":",
		"order_tag_kitchen_ticket_position",
		"select"
	),
	array(
		speak("Default order type for quick serve orders").":",
		"order_tag_default_quick_serve",
		"select"
	),
	array(
		speak("Default order type for table orders").":",
		"order_tag_default_table",
		"select"
	),
    array(
        speak("Order type prompt for table orders").":",
        "order_tag_table_prompt",
        "bool"
    ),
	array(
		speak("Default order type for tab orders").":",
		"order_tag_default_tab",
		"select"
	),
    array(
        speak("Order type prompt for tab orders").":",
        "order_tag_tab_prompt",
        "bool"
    ),
	array(
		speak("Allow order types to be used as a forced modifier").":",
		"order_type_as_forced_modifier",
		"bool"
	),
);

$cat_count = 1;

if ($form_posted)
{
	if ($_POST['config:order_type_as_forced_modifier'] == 0) {
		lavu_query("UPDATE `forced_modifiers` SET `_deleted` = '1' WHERE ordertype_id != ''");
	}
	foreach ($settings as $setting)
	{
		$sttng = $setting[1];
		if (isset($_POST['config:'.$sttng]) && $_POST['config:'.$sttng]!=$location_info[$sttng])
		{
			update_config($sttng, $_POST['config:'.$sttng]);
		}
	}

	$item_count = 1;
	while (isset($_POST["ic_".$cat_count."_".$inventory_primefield."_".$item_count]))
	{
		$item_value = array(); // get posted values
		for($n=0; $n<count($qfields); $n++)
		{
			$qname = $qfields[$n][0];
			$qtitle = (isset($qfields[$n][2]))?$qfields[$n][2]:false;
			$set_item_value = $_POST['ic_'.$cat_count.'_'.$qname.'_'.$item_count];
			if ($qtitle && $set_item_value==$qtitle)
			{
				$set_item_value = "";
			}

			if ($qname=="cost")
			{
				$set_item_value = str_replace("$", "", $set_item_value);
				$set_item_value = str_replace(",", "", $set_item_value);
			}

			$item_value[$qname] = $set_item_value;
		}

		$item_id		= $item_value['id'];
		$item_delete	= $item_value['delete'];
		$item_name		= $item_value[$inventory_primefield];

			if ($item_delete == "1")
			{
				delete_poslavu_dbrow($inventory_tablename, $item_id);
				lavu_query("UPDATE `forced_modifiers` SET `_deleted` = '1' WHERE `ordertype_id` = '[1]'", $item_id);
			}
			else if ($item_name != "")
			{
				$tagsFlag = 0;
				if($item_id == '')
				{
					$tagsQuery = lavu_query("SELECT COUNT(*) as tagcnt, `_deleted` from `order_tags` WHERE LOWER(`name`)='".strtolower($item_name)."'");
					$tagsVal = mysqli_fetch_assoc($tagsQuery);
					$tagsCount = $tagsVal['tagcnt'];
					$tagsFlag = ($tagsCount > 0) ? 1 : $tagsFlag;
				}
				if($tagsFlag == 0) {
					$dbfields = array();
					for ($n = 0; $n < count($qdbfields); $n++)
					{
						$qname = $qdbfields[$n][0];
						$qfield = (isset($qdbfields[$n][1]))?$qdbfields[$n][1]:$qname;
						$dbfields[] = array($qfield, $item_value[$qname]);
					}
				}
				else {
					if ($tagsVal['_deleted'] == 1) {
						lavu_query("UPDATE `order_tags` SET `_deleted` = 0 WHERE LOWER(`name`)='".strtolower($item_name)."'");
					} else {
						$dbfields = '';
					}
				}
				update_poslavu_dbrow($inventory_tablename,$dbfields, $item_id);
			}

		$item_count++;
	}

	schedule_remote_to_local_sync($locationid, "table updated", "order_tags");
}

$order_tags = array();
$get_order_tags = lavu_query("SELECT `id`, `name` FROM `order_tags` WHERE `active` = '1' AND `_deleted` = '0' AND `id` not in('1')");
while ($info = mysqli_fetch_assoc($get_order_tags))
{
	$order_tags[$info['id']] = $info['name'];
}

$settings[0][] = array(
	''	=> speak("Kitchen Tickets"),
	'1'	=> speak("Receipts"),
	'2'	=> speak("Both")
);
$settings[1][] = array(
	'0' => speak("Top"),
	'1' => speak("Middle"),
	'2' => speak("Bottom")
);
$settings[2][] = $order_tags;
$settings[3][] = $order_tags;
$settings[5][] = $order_tags;

$details_column		= "";
$send_details_field	= "";

$qinfo = array(
	'category_fieldname'			=> $category_fieldname,
	'category_filter_by'			=> $category_filter_by,
	'category_filter_value'			=> $category_filter_value,
	'category_primefield'			=> $category_primefield,
	'category_tablename'			=> $category_tablename,
	'details_column'				=> $details_column,
	'field'							=> $field,
	'inventory_fieldname'			=> $inventory_fieldname,
	'inventory_filter_by'			=> "loc_id",
	'inventory_filter_value'		=> $locationid,
	'inventory_orderby'				=> $inventory_orderby,
	'inventory_primefield'			=> $inventory_primefield,
	'inventory_select_condition'	=> $inventory_select_condition,
	'inventory_tablename'			=> $inventory_tablename,
	'qtitle_category'				=> $qtitle_category,
	'qtitle_item'					=> $qtitle_item,
	'send_details'					=> $send_details_field
);

$form_name = "ing";
$field_code = create_dimensional_form($qfields, $qinfo, $cfields);

$display .= "<table cellspacing='0' cellpadding='3' class='tableSpacing'><tr><td align='center' style='border:2px solid #DDDDDD; padding: 3px 6px;'>";
$display .= "<form name='ing' method='post' action=''>";
$display .= "<input type='hidden' name='posted' value='1'>";
$display .= "<br><b>".speak("Order Types")."</b><br><br>";
$display .= settingsCode($settings);
$display .= $field_code;
$display .= "&nbsp;<br><input type='submit' id='ordTagSubmit' value='".speak("Save")."' style='width:120px'>";
$display .= "</form>";
$display .= "</td></tr></table>";
$display .="<script type='text/javascript'>";
$display .="$( document ).ready(function() {
			$ ( '#del_dinein' ).remove();
			$ ( '#td_dinein' ).html('<img src=\"images/blank_19x19.png\">');
		});
		$ ('#ordTagSubmit').click(function() {
			var itemCount = $(\"[id*='ic_1_name_']\");
			var fieldArr = [];
			var tagsName = [];
			var chkemptyFlag = 0;
			for (var otg=0; otg<itemCount.length; otg++) {
				var fieldId = $(itemCount[otg]).attr('id');
				var fieldVal = $('#'+fieldId).val();
				var setFlag = 0;
				var otgn = otg + 1;
				var chkdelid = 'icrow_delinfo_1_'+otgn;
				var chkdelval = $('#'+chkdelid).text();
				if (chkdelval == 'deleted') {
					chkemptyFlag = 0;
				} else {
					if(fieldVal == 'Type Name') {
					$ ( '#'+fieldId ).focus();
					chkemptyFlag = 1;
				        } else if(fieldVal.trim() == '') {
				  	$ ( '#'+fieldId ).focus();
					chkemptyFlag = 1;	
				  }
				}
				if (fieldArr.length>0 && fieldArr.includes(fieldVal.toLowerCase())) {
					setFlag = 1;
					tagsName.push(fieldVal);
				} else {
					fieldArr.push(fieldVal.toLowerCase());
				}
			}
			if(chkemptyFlag === 1) {
				alert('Please Enter The Appropriate Order Type');
				return false;
			}
			if (setFlag === 1) {
				alert(tagsName.toString()+' already exists');
				window.location.href='index.php?mode=settings_order_types';
				return false;
			}
		});";
$display .="</script>";

echo $display;

function update_config($setting, $value)
{
	global $location_info;

	if (isset($location_info[$setting]))
	{
		lavu_query("UPDATE `config` SET `value` = '[1]' WHERE `location` = '[2]' AND `type` = 'location_config_setting' AND `setting` = '[3]'", $value, $location_info['id'], $setting);
	}
	else
	{
		lavu_query("INSERT INTO `config` (`location`, `type`, `setting`, `value`) VALUES ('[1]', 'location_config_setting', '[2]', '[3]')", $location_info['id'], $setting, $value);
	}

	$location_info[$setting] = $value;
}

function settingsCode($settings)
{
	global $location_info;

	$str = "<table cellspacing='2' cellpadding='2'>";
	foreach ($settings as $setting)
	{
		$str .= "<tr><td align='right'>".$setting[0]."</td><td align='left'>";
		if ($setting[2] == "select")
		{ 
		    $disabled = "";
		    if($setting[1] == 'order_tag_default_table'){
		        $disabled=($location_info['order_tag_table_prompt']==1)? 'disabled':'';
		    }
		    else if($setting[1] == 'order_tag_default_tab'){
		        $disabled=($location_info['order_tag_tab_prompt']==1)? 'disabled':'';
		    }
		    
			$str .= "<select name='config:".$setting[1]."' id='config:".$setting[1]."' $disabled>";
			foreach ($setting[3] as $key => $val)
			{
				$selected = "";
				if ($location_info[$setting[1]] == $key)
				{
					$selected = " SELECTED";
				}

				$str .= "<option value='".$key."'".$selected.">".$val."</option>";
			}

			$str .= "</select>";
		}
		else if ($setting[2] == "bool") {
		    $query_statement = "SELECT `id` FROM `forced_modifiers` WHERE `ordertype_id` != '' 	AND `_deleted` != '1'";
		    $qResult = lavu_query($query_statement);
		    $fieldName = "config:".$setting[1];
		    $str .= "<input type='hidden' name='$fieldName' id='$fieldName' value=\"".$location_info[$setting[1]]."\">";
		    $checked="";
		    if ($location_info[$setting[1]] == 1) {
		        $checked= "checked";
		    }
		    if ($fieldName == "config:order_type_as_forced_modifier" && mysqli_num_rows($qResult) > 0) {
				$mesg = 'Disabling \"Allow order types to be used as a forced modifier\" will remove order types from all modifier lists. Are you sure you want to proceed';
				$str .= "<input type='checkbox' id='orderTypeCheck' onclick='showConfirmBox(\"$mesg\", \"".$fieldName."\", this.checked)' $checked>";
		    } else {
				$str .= "<input type='checkbox' onclick='getOrderTypeInfo(\"".$fieldName."\",this.checked)' $checked>";
		    }
		 }
         $str .= "</td></tr>";
	}

	$str .= "<tr><td colspan='2'><hr style='border:0px; color:#DDDDDD; background-color:#DDDDDD; height:2px;'></td></tr></table><br>";

	return $str;
}
