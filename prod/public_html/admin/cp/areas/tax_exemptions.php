<?php	
	if($in_lavu)
	{
		$form_posted = (isset($_POST['posted']));
		
		$display = "";
		$field = "";
		
		$qtitle_category = false;
		$qtitle_item = "Exemption"; // do not translate here
		
		$category_tablename = "";
		$inventory_tablename = "tax_exemptions";
		$category_fieldname = "";
		$inventory_fieldname = "id";
		$inventory_select_condition = " AND `loc_id` = '$locationid'";
		$inventory_primefield = "title";
		$category_primefield = "";
							
		$qfields = array();
		$qfields[] = array("title","title",speak("Button Title"),"input");
		$qfields[] = array("label","label",speak("Receipt Label"),"input");
		$qfields[] = array("loc_id","loc_id","Location ID","hidden", $locationid);
	
		$qfields[] = array("id","id");
		$qfields[] = array("delete");
		require_once(resource_path() . "/dimensional_form.php");
		$qdbfields = get_qdbfields($qfields);
					
		$cat_count = 1;
						
		if($form_posted)
		{

			$item_count = 1;
			while(isset($_POST["ic_".$cat_count."_".$inventory_primefield."_".$item_count]))
			{						
				$item_value = array(); // get posted values
				for($n=0; $n<count($qfields); $n++)
				{
					$qname = $qfields[$n][0];
					$qtitle = (isset($qfields[$n][2]))?$qfields[$n][2]:false;
					$set_item_value = $_POST['ic_'.$cat_count.'_'.$qname.'_'.$item_count];
					if($qtitle && $set_item_value==$qtitle) $set_item_value = "";
					if($qname=="cost") 
					{
						$set_item_value = str_replace("$","",$set_item_value);
						$set_item_value = str_replace(",","",$set_item_value);
					}
					$item_value[$qname] = $set_item_value;
				}
				$item_id = $item_value['id'];
				$item_delete = $item_value['delete'];
				$item_name = $item_value[$inventory_primefield];
				
				if($item_delete=="1")
				{
					delete_poslavu_dbrow($inventory_tablename, $item_id);
				}
				else if($item_name!="")
				{
					$dbfields = array();
					for($n=0; $n<count($qdbfields); $n++)
					{
						$qname = $qdbfields[$n][0];
						$qfield = (isset($qdbfields[$n][1]))?$qdbfields[$n][1]:$qname;
						$dbfields[] = array($qfield,$item_value[$qname]);
					}
					update_poslavu_dbrow($inventory_tablename,$dbfields, $item_id);
				}								
				$item_count++;
			}
		}
		
		$details_column = "";//"Zip Codes";
		$send_details_field = "";//"zip_codes";
	
		$qinfo = array();
		$qinfo["qtitle_category"] = $qtitle_category;
		$qinfo["qtitle_item"] = $qtitle_item;
		$qinfo["category_tablename"] = $category_tablename;
		$qinfo["inventory_tablename"] = $inventory_tablename;
		$qinfo["category_fieldname"] = $category_fieldname;
		$qinfo["inventory_fieldname"] = $inventory_fieldname;
		$qinfo["inventory_select_condition"] = $inventory_select_condition;
		$qinfo["inventory_primefield"] = $inventory_primefield;
		$qinfo["category_primefield"] = $category_primefield;
		$qinfo["details_column"] = $details_column;
		$qinfo["send_details"] = $send_details_field;
		$qinfo["field"] = $field;
		
		$qinfo["category_filter_by"] = $category_filter_by;
		$qinfo["category_filter_value"] = $category_filter_value;
		$qinfo["inventory_filter_by"] = "loc_id";
		$qinfo["inventory_filter_value"] = $locationid;
		
		$field_code = create_dimensional_form($qfields,$qinfo,$cfields);
		$display .= "<table cellspacing='0' cellpadding='3'><tr><td align='center' style='border:2px solid #DDDDDD'>";
		$display .= "<form name='ing' method='post' action=''>";
		$display .= "<input type='submit' value='".speak("Save")."' style='width:120px'><br>&nbsp;";
		$display .= "<input type='hidden' name='posted' value='1'>";
		$display .= $field_code;
		$display .= "&nbsp;<br><input type='submit' value='".speak("Save")."' style='width:120px'>";
		$display .= "</form>";
		$display .= "</td></tr></table>";
		
		echo $display;		
	}
?>
