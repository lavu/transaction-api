<?php
	ini_set("display_errors",1);
	//echo print_r($_GET,1);
?>
<style>
	.guest_metrics_container{
		width:800px;
		margin:0 auto;
		font-family:arial, sans-serif;
	}
	.guest_metrics_header{
		font-weight:600;
		font-size:20pt;
	}
</style>

<div class='guest_metrics_container'>
	<div class ='guest_metrics_header'>
		Guest Metrics Header goes here.
	</div>
</div>
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
<script type='text/javascript'>
	//var dn = '"'++'"';
	var default_url = "/cp/resources/guest_metrics_data.php";
	<?php echo "var data_name ='" .$_GET['data_name'] ."';"; ?>
	function get_guest_metrics_info(){
		performAJAX(default_url, "function=get_account_info&data_name="+data_name);
	}


	function performAJAX(URL, urlString,asyncSetting ){
		var returnVal='';
		$.ajax({
			url: URL,
			type: "POST",
			data:urlString,
			async:asyncSetting,
			success: function (string){
				returnVal= string;
			}
		});
		return returnVal;
	}
	get_guest_metrics_info();

</script>
