<?php	
	if($in_lavu)
	{
		$form_posted = (isset($_POST['posted']));
	
		$display = "";
		$field = "force_modifiers";
		$qtitle_category = "Tier Profile";
		//$qtitle_category = speak("List");
		$qtitle_category_plural = "Tier Profiles";
		$qtitle_item = "Price Tier";
		
		$category_tablename = "config";
		$inventory_tablename = "config";
		$category_fieldname = "id";
		$inventory_fieldname = "value2";
		$inventory_select_condition = "";
		$inventory_primefield = "value";
		$category_primefield = "value";
		
		$page_categories_by = 50;
	
		$inventory_orderby = "(`value` * 1)";
		
		$cfields = array(); // name, units
		$cfields[] = array("value2", "value2", "Units", "input");
		$cfields[] = array("type","type","","hidden","price_tier_profile");
		
		$category_filter_by = "location";
		$category_filter_value = $locationid;
		$category_filter_extra = "`type` = 'price_tier_profile'";
	
		$inventory_filter_by = "type";
		$inventory_filter_value = "price_tier";
						
		$qfields = array();
		$qfields[] = array("value","value","Floor quantity","input"); // Lower? limit?
		$qfields[] = array("value3","value3","Type","select",array(array("Price per unit", "per_unit"), array("Percentage of base", "percentage_of_base")));
		$qfields[] = array("value4","value4","Price/percentage","input"); // Operative value
		$qfields[] = array("type","type","","hidden","price_tier");
		
		$qfields[] = array("id");
		$qfields[] = array("delete");
		require_once(resource_path() . "/dimensional_form.php");
		$qdbfields = get_qdbfields($qfields);
						
		if($form_posted)
		{
			$invalid_value = false;		
		
			$cat_count = 1;
			while(isset($_POST["ic_".$cat_count."_category"]))
			{
				$category_name = $_POST['ic_'.$cat_count.'_category'];
				$category_id = $_POST['ic_'.$cat_count.'_categoryid'];
				$category_delete = $_POST['ic_'.$cat_count.'_delete_0'];
				if($category_name==$qtitle_category . " Name") $category_name = "";
				
				if($category_delete=="1")
				{
					delete_poslavu_dbrow($category_tablename, $category_id);
				}
				
				if($category_name!="")
				{
					$dbfields = array();
					$dbfields[] = array("value",$category_name);
					$dbfields[] = array("location",$locationid);
					for($n=0; $n<count($cfields); $n++)
					{
						$cfieldname = $cfields[$n][0];
						$dbfields[] = array($cfieldname,$_POST['ic_'.$cat_count.'_col_'.$cfieldname]);
					}
					$categoryid_indb = update_poslavu_dbrow($category_tablename,$dbfields, $category_id);
					if($category_fieldname=="title")
						$categoryid_indb = $category_name;
					$item_count = 1;
					
					while(isset($_POST["ic_".$cat_count."_".$inventory_primefield."_".$item_count]))
					{						
						$item_value = array(); // get posted values
						for($n=0; $n<count($qfields); $n++)
						{
							$qname = $qfields[$n][0];
							$qtitle = (isset($qfields[$n][2]))?$qfields[$n][2]:false;
							$set_item_value = $_POST['ic_'.$cat_count.'_'.$qname.'_'.$item_count];
							if($qtitle && $set_item_value==$qtitle) $set_item_value = "";
							if($qname=="value" || $qname=="value4") 
							{
								$set_item_value = str_replace("$","",$set_item_value);
								$set_item_value = str_replace(",","",$set_item_value);
								if (!is_numeric($set_item_value)) {
									$set_item_value = 0;
									$invalid_value = $qtitle;
								}
							}
							$item_value[$qname] = $set_item_value;
						}
						
						error_log("item_value: ".print_r($item_value, true));
						
						$item_id = $item_value['id'];
						$item_delete = $item_value['delete'];
						$item_name = $item_value[$inventory_primefield];
						
						if($item_delete=="1")
						{
							delete_poslavu_dbrow($inventory_tablename, $item_id);
						}
						else if($item_name!="")
						{
							$dbfields = array();
							$dbfields[] = array($inventory_fieldname,$categoryid_indb);
							$dbfields[] = array("location",$locationid);
							for($n=0; $n<count($qdbfields); $n++)
							{
								$qname = $qdbfields[$n][0];
								$qfield = (isset($qdbfields[$n][1]))?$qdbfields[$n][1]:$qname;
								$dbfields[] = array($qfield,$item_value[$qname]);
							}
							$orderby_tag = 'icrow_'.$cat_count.'_'.$item_count.'_order';
							if(isset($_POST[$orderby_tag]))
							{
								$dbfields[] = array("_order",$_POST[$orderby_tag]);
							}
							update_poslavu_dbrow($inventory_tablename,$dbfields, $item_id);
						}								
						$item_count++;
					}
				}
				$cat_count++;
			}
			
			if ($invalid_value) echo "<script language='javascript'>alert('Invalid value entered for ".$invalid_value.". Please use numeric values only.');</script>";
	
			$check_for_last_update = lavu_query("SELECT `id` FROM `config` WHERE `setting` = 'menu_last_updated' AND `location` = '[1]'", $locationid);
			if (@mysqli_num_rows($check_for_last_update)) {
				$uinfo = mysqli_fetch_assoc($check_for_last_update);
				lavu_query("UPDATE `config` SET `value` = now() WHERE `id` = '[1]'", $uinfo['id']);
			} else lavu_query("INSERT INTO `config` (`location`, `type`, `setting`, `value`) VALUES ('[1]', 'admin_activity', 'menu_last_updated', now())", $locationid);
	
			lavu_query("UPDATE `devices` SET `needs_reload` = '1' WHERE `loc_id` = '[1]'", $locationid);
				
			$form_error = "Warning: This section under Development";
		}
		
		$details_column = "";
		$send_details_field = "";
	
		$qinfo = array();
		$qinfo["qtitle_category"] = $qtitle_category;
		$qinfo["qtitle_category_plural"] = $qtitle_category_plural;
		$qinfo["qtitle_item"] = $qtitle_item;
		$qinfo["category_tablename"] = $category_tablename;
		$qinfo["inventory_tablename"] = $inventory_tablename;
		$qinfo["category_fieldname"] = $category_fieldname;
		$qinfo["inventory_fieldname"] = $inventory_fieldname;
		$qinfo["inventory_select_condition"] = $inventory_select_condition;
		$qinfo["inventory_primefield"] = $inventory_primefield;
		$qinfo["category_primefield"] = $category_primefield;
		$qinfo["details_column"] = $details_column;
		$qinfo["send_details"] = $send_details_field;
		$qinfo["field"] = $field;
		
		$qinfo["category_filter_by"] = $category_filter_by;
		$qinfo["category_filter_value"] = $category_filter_value;
		$qinfo["category_filter_extra"] = $category_filter_extra;
		$qinfo["page_categories_by"] = $page_categories_by;
		
		$qinfo["inventory_orderby"] = $inventory_orderby;
		
		$qinfo['forward_to'] = "index.php?mode=$mode";

		$form_name = "ing";
		$field_code = create_dimensional_form($qfields,$qinfo,$cfields);
		$display .= "<form name='ing' method='post' action=''>";
		$display .= "&nbsp;<input type='submit' value='".speak("Save")."' class='saveBtn'><br>&nbsp;";
		$display .= "<input type='hidden' name='posted' value='1'>";
		$display .= $field_code;
		$display .= "&nbsp;<input type='submit' value='".speak("Save")."' class='saveBtn'><br>&nbsp;";
		$display .= "</form>";
		
		echo $display;		
	}
?>
