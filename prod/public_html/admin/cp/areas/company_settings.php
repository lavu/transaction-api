<?php
	if($in_lavu) {

		echo "<br><br>";

		$forward_to = "index.php?mode={$section}_{$mode}";
		$use_db = "`poslavu_MAIN_db`.";
		$tablename = "restaurants";
		$rowid = admin_info("companyid");

		require_once(resource_path() . "/chain_functions.php");
		$countryCodeList = getCountryCodeList();
		$countryList = array();
		if (!empty($countryCodeList)) {
			foreach ($countryCodeList as $countryCode) {
				$countryList[$countryCode['code']] = $countryCode['name'];
			}
		}

		/**
		 * To get country code list for select box in lavu selectbox array format.
		 * @param array $countryList
		 * @return array $countryCodes[countryname][countryCode]
		 */
		function getCountryCodes($countryList) {
			$countryCodes = array(""=>"Select Country");
			if (!empty($countryList)) {
				foreach ($countryList as $code => $name) {
					$displayedValue = formatEncodeCountryCodeWithName($code, $name);
					$countryCodes[$name] = $displayedValue;
				}
			}
			return $countryCodes;
		}
		$configValue2 = array();
		// Needed because the location for the config setting is being recorded as the "rowid" which might be different
		$config_query = lavu_query("select * from `config` where `type`='location_config_setting' and `location`='[1]' and `setting` LIKE 'business_%'",$rowid);
		while($config_read = mysqli_fetch_assoc($config_query))
		{
			$location_info[$config_read['setting']] = $config_read['value'];
			$configValue2 [$config_read['setting']] = $config_read['value2'];
		}

		$settings="<script type='text/javascript' src='/cp/scripts/select2-4.0.3.min.js'></script>";
		$settings.="<link href='/cp/css/select2-4.0.3.min.css' rel='stylesheet'/>";

		$fields = array();
		$fields[] = array(speak("Company Settings"),"Company Settings","seperator");
		$fields[] = array(speak("Company Name").":","company_name","text","size:30");
		
		$fields[] = array(speak("Business Primary Contact Name").":","config:business_primary_contact","text","size:30");
		$fields[] = array(speak("Business Address").":","config:business_address","text", "size:30");
		$fields[] = array(speak("Business City").":","config:business_city","text","size:30");
		$fields[] = array(speak("Business State/Province").":","config:business_state","text","size:30");
		$fields[] = array(speak("Business Postal Code").":","config:business_zip","text","size:30");

		if ($configValue2['business_country'] != '') {
			$fields[] = array(speak("Business Country").":", "config:business_country", "select", getCountryCodes($countryList));
		} else {
			$fields[] = array(speak("Business Country").":", "config:business_country", "text_editCountry", "size:25,disabled,edit_businesscountry", $countryList);
		}
		$fields[] = array(speak("Business Phone").":","config:business_phone","text","size:30");
        $fields[] = array(speak("Business Reseller Name").":","config:business_reseller_name","text","size:30");
        $fields[] = array(speak("Business Reseller ID").":","config:business_reseller_id","text","size:30");
        
		$fields[] = array(speak("Alert Text Msg #").": (xxx-xxx-xxxx)","phone_alert","text","size:30");
		$fields[] = array(speak("Email").":","config:business_emailid","text","size:30");
		$fields[] = array("","","seperator");
		$fields[] = array(speak("Company Logo").":","logo_img","file");
		$fields[] = array("","","seperator");
		
		$fields[] = array(speak("Company Logo")." (".speak("on receipt")."):","receipt_logo_img","file");
		$fields[] = array("","","seperator");
		$fields[] = array(speak("Save"),"submit","submit");
		
		require_once(resource_path() . "/form.php");
		
		echo "<br><br>";

		$settings .= "<script type='text/javascript'>
		$('select[name^=\"config:business_country\"]').select2();
		$('.select2-container').attr('style','width:21em;');
		$('.select2-container .select2-selection--single').attr('style','max-width: 23em !important;padding-top:1px;');
		//To show Business country dropdown list.
		function editBusinessCountry() {
			$('select[name^=\"hidden_config:business_country\"]').select2();
			$('.select2-container').attr('style','width:21em;');
			$('.select2-container .select2-selection--single').attr('style','max-width: 23em !important;padding-top:1px;');
			$('select[id^=\"config:business_country\"]').attr('name','config:business_country');
		}
		//After selection of new country showing selected country and hide edit button.
		function selectNewCountry(){
			var countryVal = $('select[id^=\"config:business_country\"] option:selected').attr('view');
			$('input[name^=\"old_config:business_country\"]').val(countryVal);
			$('input[name^=\"old_config:business_country\"]').attr('size','30');
			$('input[id^=\"edit_business_country\"]').attr('style','visibility:hidden');
		}
		</script>";
		echo $settings;
	}
?>
