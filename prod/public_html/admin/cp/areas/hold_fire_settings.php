<?php
//Two entry points, first for general entry, second if posting to itself to save.
if(!empty($_POST['btn_1_select'])){

	//$usingDeliverManagementPage = !empty($_POST['using_holdfire_option']) ? "true" : "false";
	$holeSettingsToSadfirveArr =
	array( $_POST['btn_1_select'],
			$_POST['btn_2_select'],
			$_POST['btn_3_select'],
			$_POST['btn_4_select'] );
	//print_r($holeSettingsToSadfirveArr); exit;
	$holdfireSettingsToSaveStr = implode("|o|", $holeSettingsToSadfirveArr);
	$error='';
	if (count(array_unique($holeSettingsToSadfirveArr))>3){
	updateholdfireOptions($holdfireSettingsToSaveStr);
	mainDisplay();
	}else {
		$error="display duplicate values ";
		mainDisplay($error);
	}
}
else{
	mainDisplay();
}

function mainDisplay($error){
	$holdfireOptionsString = getConfigholdfireOptions();
	$holdfireOptionsArr = explode("|o|", $holdfireOptionsString);
	$dispHTML = '';
	$dispHTML .= getEditorHTML($holdfireOptionsArr,$error);
	echo $dispHTML;
}

function getconfigsettingdata(){

	$result = lavu_query("SELECT value FROM `config` WHERE `setting`='show_hold_and_fire'");
	
	if(mysqli_num_rows($result)){
		$row = mysqli_fetch_assoc($result);
		$holdfireOptions= $row['value'];
	}

	return $holdfireOptions;
}

function getEditorHTML($holdfireOptions,$error){

	//The selects that will be used
	$quickButton1Select = generateSelectForQuickButton('btn_1_select', $holdfireOptions[0]);
	$quickButton2Select = generateSelectForQuickButton('btn_2_select', $holdfireOptions[1]);
	$quickButton3Select = generateSelectForQuickButton('btn_3_select', $holdfireOptions[2]);
	$quickButton4Select = generateSelectForQuickButton('btn_4_select',$holdfireOptions[3]);

	$speak = 'speak';
	//$checked = $holdfireOptions[4] == 'true' ? "CHECKED" : "";
	$config_setting=getconfigsettingdata();
	$returnHTML = <<<HTML
           <div id="main_content_area">
HTML;
	if($error!=''){
		$returnHTML = $returnHTML.<<<HTML
		<div style="color:#cc0000;">Please select unique time intervals</div>
HTML;
	}
	if($config_setting!=1){
        $returnHTML = $returnHTML.<<<HTML
           <div><b style="color:#333;">{$speak('Hold & Fire Intervals')}</b></div></div><br />
            <div style="margin:20px 0px;">{$speak(' Please enable Hold & Fire from Advanced Location Settings to set the Hold Time Intervals!')}&nbsp;</div>
HTML;
    }else{
        $returnHTML = $returnHTML.<<<HTML
        <div><b style="color:#333;">{$speak('Hold & Fire Intervals')}</b></div>
            </div><br />
             <form action='' method='POST'>
              <table border='1'align="center" >
                    <tr>
                        <td>&nbsp;Hold & Fire Button 1&nbsp;</td>
                        <td>$quickButton1Select</td>
                    </tr>
                    <tr>
                        <td>&nbsp;Hold & Fire Button 2&nbsp;</td>
                        <td>$quickButton2Select</td>
                    </tr>
                    <tr>
                        <td>&nbsp;Hold & Fire Button 3&nbsp;</td>
                        <td>$quickButton3Select</td>
                    </tr>
                    <tr>
                        <td>&nbsp;Hold & Fire Button 4&nbsp;</td>
                        <td>$quickButton4Select</td>
                    </tr>
                </table>
                <br><br>
                <input type='submit' value='{$speak("Save")}'/>
              </form>
HTML;
            
	}
	return $returnHTML;
}

//HTML generator functions
function generateSelectForQuickButton($selectsID, $selectOnAmount){
	$selectHTML = "<select id='$selectsID' name='$selectsID'>";
	for($i = 5; $i <= 60 * 1; $i+=5){
		$d=$i;
		$disp = "$i Minutes";
		/*
		$p = $i % 60;
		$q = floor($i / 60);
		$m = $p < 10 ? '0' . $p : $p;
		$h = $q ? "$q:" : "";
		$d = "$h$m";
		$disp = '';
		if($h == ""){
			$disp = "$p Minutes";
		}else if($d == "1:00"){
			$disp = "$q Hour";
		}else if($m == "00"){
			$disp = "$q Hours";
		}else{
			$disp = "$d Hours";
		}
		*/
		$isSelectedStr = $selectOnAmount == $d || $selectOnAmount == $i ? "selected" : "";

		$selectHTML .= "<option id='{$selectsID}_option_$d' value='$d' $isSelectedStr>&nbsp;$disp</option>";

	}
	$selectHTML .= "</select>";
	return $selectHTML;
}

function generateSelectForIntervalChoice($selectsID, $selectOnAmount){

	$selectedOption10 = $selectOnAmount == '10' ? 'selected' : '';
	$selectedOption15 = $selectOnAmount == '15' ? 'selected' : '';
	$selectedOption30 = $selectOnAmount == '30' ? 'selected' : '';
	$selectedOption1h = $selectOnAmount == '1:00' ? 'selected' : '';
	$selectedOption2h = $selectOnAmount == '2:00' ? 'selected' : '';

	$selectHTML  = "<select id='$selectsID' name='$selectsID'>";
	$selectHTML .=   "<option id='{$selectsID}_option_1' value='10' $selectedOption10>10 Minutes</option>";
	$selectHTML .=   "<option id='{$selectsID}_option_2' value='15' $selectedOption15>15 Minutes</option>";
	$selectHTML .=   "<option id='{$selectsID}_option_3' value='30' $selectedOption30>30 Minutes</option>";
	$selectHTML .=   "<option id='{$selectsID}_option_4' value='1:00' $selectedOption1h>1 Hour</option>";
	$selectHTML .=   "<option id='{$selectsID}_option_5' value='2:00' $selectedOption2h>2 Hours</option>";
	$selectHTML .= "</select>";
	return $selectHTML;
}

//Database functions below
function getConfigholdfireOptions(){
	$result = lavu_query("SELECT * FROM `config` WHERE `setting`='hold_fire_intervals'");
	if(!$result){
		error_log("MySQL Error in holdfire options -ksjdf3- error: " . lavu_dberror());
		return false;
	}
	$holdfireOptions;
	if(mysqli_num_rows($result)){
		$row = mysqli_fetch_assoc($result);
		$holdfireOptions= $row['value'];
	}else{
		$holdfireOptions = insertAndReturnDefaultholdfireOptions();
	}
	return $holdfireOptions;
}

function insertAndReturnDefaultholdfireOptions(){
	$optionsArr = array("5", "10", "15", "20");
	$encodedString = implode("|o|", $optionsArr);

	$result = lavu_query("INSERT INTO `config` (`setting`,`value`,`location`,`type`) VALUES ('hold_fire_intervals', '[1]',".$_SESSION[sessname('locationid')].",'location_config_setting')", $encodedString);
	if(!$result){
		error_log("MySQL Error in holdfire options -aesefe- error: " . lavu_dberror());
	}
	return $encodedString;
}

function updateholdfireOptions($newholdfireOptions){
	$result = lavu_query("UPDATE `config` SET `value`='[1]' WHERE `setting`='hold_fire_intervals'", $newholdfireOptions);
	if(!$result){
		error_log("MySQL Error in holdfire options -aesefe- error: " . lavu_dberror());
	}
}

?>