<?php
        session_start();
        require_once(__DIR__.'/../resources/core_functions.php');
        include_once(dirname(__FILE__)."/api/pages/functions.php");
?>
<!doctype HTML>
<html>
<head>
	<link href="api/css/style.css" rel="stylesheet" type="text/css" />
	<script type="text/javascript" src="api/scripts/api_doc.js"></script>
</head>
<body>
	<div style="width: 80%; margin: 0 auto;">
		<h3>Lavu API Documentation</h3>	
		<?php 
			foreach($sections as $label => $id)
			{
				echo "<div class='sxs button' onclick=\"swapPage('$id');\">$label</div>";
			}
		?>
	</div>
	<?php
		foreach($sections as $label => $id)
		{
			echo "<div class=\"page\" id='$id' style=\"display: none;\">";
			require_once(dirname(__FILE__)."/api/pages/$id.php");
			echo "</div>";
		}
	?>
	<div style="width: 80%; margin: 0 auto;">
		<?php 
				foreach($sections as $label => $id)
				{
					echo "<div class='sxs button' onclick=\"swapPage('$id');\">$label</div>";
				}
		?>
	</div>
	<script type="text/javascript">
		swapPage('start');
	</script>
</body>
</html>
