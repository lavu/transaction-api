<?php
//	ini_set("display_errors","1");

	$extra_display_string = "";
	$dontprint = "DONTPRINTME";
	$dev_dir = "";
	require_once(dirname(__FILE__).'/../../lib/transaction_summary.php');

	if (!isset($mode))
		if (isset($_GET['mode']))
			$mode = $_GET['mode'];
		else
			$mode = "";

    if (isset($_GET['drawer'])) {
        $drawer = ($_GET['drawer'] != '') ? $_GET['drawer'] : 0;
    }
	if ($mode == "edit_transaction_summary_items") {

		// load the settings
		$apply_transaction_methods = get_transaction_method_preferences($drawer);
		// update the settings
		if (isset($_POST['posted'])) {
			foreach($apply_transaction_methods as $k=>$v) {
				if (isset($_POST[$k."_summary"]))
					$apply_transaction_methods[$k][0] = urldecode($_POST[$k."_summary"]);
				if (isset($_POST[$k."_tips"]))
					$apply_transaction_methods[$k][1] = urldecode($_POST[$k."_tips"]);
			}
			// clears the browser's history of the form data
			echo "<script type='text/javascript'>window.location = window.location;</script>";
		}
	/*
	error_log("Transaction Methods Debugging---------------------------------------------");
	error_log("apply_transaction_methods: " . print_r($apply_transaction_methods,1));
	error_log("original post arr: " . print_r($_POST,1));
	*/
		// save the settings
		save_transaction_method_preferences($apply_transaction_methods, $drawer);
		$apply_transaction_methods = get_transaction_method_preferences($drawer);

		echo speak("Add, subtract, or ignore specific values to the till in and till out summaries.") . "<br />";
		echo "<br /><br />";
	}
?>

<style type='text/css'>
.till_td_body {
	padding:2px 5px 2px 5px;
	background-color:#f8f8f8;
}
.till_td_body_name {
	padding:2px 5px 2px 5px;
	background-color:#f8f8f8;
	text-align:right;
}
.till_td_head {
	padding:2px 5px 2px 5px;
	background-color:#eee;
	text-align:center;
}
.till_table {
	border:2px solid gray;
}
</style>

<form method='post'>
	<input type='hidden' name='posted' value='1' />
	<table style='text-align:center;'>
		<tr>
			<td>
				<table class='till_table'>
					<tr>
						<?php echo "<td class='till_td_head'>".speak("Transaction Method")."</td><td class='till_td_head'>".speak("Summary")."</td><td class='till_td_head'>".speak("Tips")."</td>"; ?>
					</tr>
<?php
						foreach ($apply_transaction_methods as $name=>$values) {
							$print_options = array();
							$options = array( array("Add",urlencode("+")),array("Subtract",urlencode("-")),array("Ignore",urlencode("ignore")) );
							for ($j = 0; $j < count($values); $j++) {
								$print_options[] = array();
								for ($i = 0; $i < count($options); $i++) {
									if ($values[$j] == $options[$i][1])
										$print_options[$j][$i] = "<option value='".$options[$i][1]."' selected>".$options[$i][0]."</option>";
									else
										$print_options[$j][$i] = "<option value='".$options[$i][1]."'>".$options[$i][0]."</option>";
								}
							}

							$disabled_summary = array();
							$disabled_tips = array("paid_in", "paid_out", "gift_certificate", "cash", "cash_deposits", "cash_deposit_refunds", "card_deposits", "card_deposit_refunds");
							if (in_array($name, $disabled_summary)) $disable_summary = " disabled"; else $disable_summary = "";
							if (in_array($name, $disabled_tips)) $disable_tips = " disabled"; else $disable_tips = "";

							$print_name = explode("_", $name);
							foreach($print_name as $key=>$value)
								$print_name[$key] = ucfirst($value);
							$print_name = speak(implode(" ", $print_name));

							echo "					<tr><td class='till_td_body_name'>".$print_name."</td><td class='till_td_body'><select name='".$name."_summary'".$disable_summary.">".implode("",$print_options[0])."</select></td>";
							echo "<td class='till_td_body'><select name='".$name."_tips'".$disable_tips.">".implode("",$print_options[1])."</td></tr>\n";
						}
					?>
				</table>
			</td>
		</tr>
		<?php
			if (isset($_POST['posted'])) echo "<tr><td style='padding:15px 0 0 0;'><font style='font-style:italic;color:#aaa'>".speak("Preferences Saved")."</font></td></tr>";
		?>
		<tr>
			<td style='padding:15px 0 0 0;'>
				<input type='submit' value='Save'>
			</td>
		</tr>
	</table>
</form>