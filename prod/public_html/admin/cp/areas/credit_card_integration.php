<?php

require_once("/home/poslavu/public_html/admin/lib/gateway_lib/supported_gateways.php");
require_once(__DIR__."/../../lib/helpers/url_helpers.php");

if (!$in_lavu)
{
	exit();
}

/**
 * LavuPay's configuration is done in CP4, therefore redirect
 * to next _section_ within payments (Payment Methods) if the
 * location has the LavuPay extension enabled.
 */
if ($modules->hasModule('extensions.payment.lavupay')) {
	header("Location: /cp/index.php?mode=settings_payment_methods");
	exit();
}

$currGW = $location_info['gateway'];
$is_free_lavu = in_array(sessvar('package'), array( "17", "18", "19", "20")); // SOMEDAY : change this check to their package_level_object display name so it will be good forever

// new
$available_gateways = array_merge(
						array("" => "---".speak("Select a gateway")."---"),
						supported_gateways($currGW, is_lavu_admin()));

$dp = $location_info['disable_decimal'];
$dc = $location_info['decimal_char'];

$signature_minimums = array(
	'0'		=> speak("No minimum"),
	'10'	=> number_format(10, $dp, $dc, ""),
	'25'	=> number_format(25, $dp, $dc, ""),
	'50'	=> number_format(50, $dp, $dc, "")
);

$default_preauth_amounts = array();
$default_preauth_amounts['1']		= number_format(1, $dp, $dc, "");
for ($amt = 5; $amt <= 100; $amt += 5)
{
	$default_preauth_amounts[$amt]	= number_format($amt, $dp, $dc, "");
}
$default_preauth_amounts['500']		= number_format(500, $dp, $dc, "");
$default_preauth_amounts['1000']	= number_format(1000, $dp, $dc, "");
$default_preauth_amounts['5000']	= number_format(5000, $dp, $dc, "");
$default_preauth_amounts['10000']	= number_format(10000, $dp, $dc, "");

$server_levels = array(
	'1' => "1",
	'2' => "2",
	'3' => "3",
	'4' => "4"
);

$lavu_only	= "<font color='#0000CC'><b>*</b></font> ";
$forward_to = "index.php?mode=".$section."_".$mode;
$tablename	= "locations";
$rowid		= $locationid;

// - - - - - - - - Card Integration - - - - - - - - //

$password_field_type = ($currGW == "MIT")?"text_encrypted":"integration_encrypted";

$fields = array(
	array(
		speak("Card Integration"),
		"Card Integration",
		"seperator",
		"",
		FALSE,
		"GatewayNotes"
	),
	array(
		speak("Integrate credit card processing").":",
		"integrateCC",
		"bool"
	),
	array(
		speak("Payment Gateway").":",
		"gateway",
		"select",
		$available_gateways
	),
	array(
		speak("Integration data 1").":",
		"integration1",
		"integration_encrypted",
		"key:".integration_keystr()
	),
	array(
		speak("Integration data 2").":",
		"integration2",
		$password_field_type,
		"key:".integration_keystr()
	),
	array(
		speak("Integration data 3").":",
		"integration3",
		"integration_encrypted",
		"key:".integration_keystr()
	),
	array(
		speak("Integration data 4").":",
		"integration4",
		"integration_encrypted",
		"key:".integration_keystr()
	),
	array(
		speak("Integration data 5").":",
		"integration5",
		"integration_encrypted",
		"key:".integration_keystr()
	),
	array(
		speak("Integration data 6").":",
		"integration6",
		"integration_encrypted",
		"key:".integration_keystr()
	),
	array(
		speak("Integration data 7").":",
		"integration7",
		"integration_encrypted",
		"key:".integration_keystr()
	),
	array(
		speak("Integration data 8").":",
		"integration8",
		"integration_encrypted",
		"key:".integration_keystr()
	)
);

if (is_lavu_admin())
{
	$fields = array_merge( $fields, array(
		array(
			$lavu_only.speak("Integration data 9").":",
			"integration9",
			"integration_encrypted",
			"key:".integration_keystr()
		),
		array(
			$lavu_only.speak("Integration data 10").":",
			"integration10",
			"integration_encrypted",
			"key:".integration_keystr()
		)
	));
}

$fields[] = array(
	speak("Default transaction type").":",
	"cc_transtype",
	"select",
	array(
		''		=> "",
		'Auth'	=> "Auth",
		'Sale'	=> "Sale"
	)
);

$fields = array_merge( $fields, array(
	array(
		speak("Allow offline credit card transactions").":",
		"config:offline_cc_transaction",
		"bool",
		"",
		FALSE,
		"offlineCCTransaction"
		)
	)
);

if ($currGW == "Authorize.net")
{
	$fields[] = array(
		speak("Market Type").":",
		"market_type",
		"select",
		array(
			''			=> "",
			'Mobile'	=> "Mobile",
			'Moto'		=> "Moto",
			'Retail'	=> "Retail",
			'Web'		=> "Web"
		)
	);
}

if ($currGW == "MIT")
{
	$fields = array_merge( $fields, array(
		array(
			speak("Enable support for AMEX").":",
			"config:mit_prompt_for_amex",
			"bool"
		),
		array(
			speak("MIT Transaction URL").":",
			"config:mit_transaction_url",
			"text",
            "size:30"
		)
	));
}

if (is_lavu_admin())
{
	$fields = array_merge( $fields, array(
		array(
			$lavu_only."Allow merchant initiated batch settlement from Management Screen:",
			"config:allow_front_end_cc_batch",
			"bool"
		),
		array(
			$lavu_only.speak("Access level required to perform offline credit card transactions").":",
			"config:offline_cc_access_level",
			"select",
			array(
				'0' => "Disabled",
				'1' => "1",
				'2' => "2",
				'3' => "3",
				'4' => "4"
			)
		),
		array(
			$lavu_only.speak("Skip confirmation steps before performing offline credit card transactions").":",
			"config:offline_cc_skip_confirmation",
			"bool"
		)
	));
}

// - - - - - - - - Gift and Loyalty - - - - - - - - //

$fields = array_merge( $fields, array(
	array(
		speak("Gift and Loyalty"),
		"Gift Cards",
		"seperator"
	),

//array(speak("Gift Card Extension").":","config:giftcard_extension_gateway","select",$availableGiftCardExtensions);
//array(speak("Gift card data 1").":","giftcard1","text_hex_encrypted","key:".integration_keystr());
//array(speak("Gift card data 2").":","giftcard2","text_hex_encrypted","key:".integration_keystr());
//array(speak("Gift card data 3").":","giftcard3","text_hex_encrypted","key:".integration_keystr());
//array(speak("Gift card data 4").":","giftcard4","text_hex_encrypted","key:".integration_keystr());
//array(speak("Gift card data 5").":","giftcard5","text_hex_encrypted","key:".integration_keystr());

	array(
		speak("Access level required to deactivate gift and loyalty cards").":",
		"config:level_to_deactivate_gift_cards",
		"select",
		$server_levels
	)
));

$has_lavu_gift = $modules->hasModule("extensions.payment.lavu.gift");
$has_lavu_loyalty = $modules->hasModule("extensions.payment.lavu.loyalty");

if (is_lavu_admin() && $has_lavu_gift)
{
	$fields[] = array(
		$lavu_only."Use integrated gift card workflows for Lavu Gift:",
		"config:lavu_gift_integrated_workflow",
		"bool"
	);
}

if (is_lavu_admin() && $has_lavu_loyalty)
{
	$fields[] = array(
		$lavu_only."Use integrated loyalty card workflows for Lavu Loyalty:",
		"config:lavu_loyalty_integrated_workflow",
		"bool"
	);
}

if (is_lavu_admin() && ($has_lavu_gift || $has_lavu_loyalty))
{
	$fields[] = array(
		$lavu_only."Extension to use for obtaining a Lavu Gift or Loyalty card swipe (Integrated workflow only):",
		"config:lavu_gift_get_swipe_extension",
		"select",
		availableGetSwipeExtensions()
	);
}

$fields = array_merge( $fields, array(
	array(
		speak("Print balance chit when checking gift or loyalty card balance").":",
		"config:print_gift_balance_chit",
		"bool"
	),


// - - - - - - - - Readers and Input Methods - - - - - - - - //

	array(
		speak("Readers and Input Methods"),
		"Readers and Input Methods",
		"seperator"
	)
));

if (in_array($currGW, array( "Heartland", "Mercury", "TGate" )))
{
	$ipc_encryption_choices = array( '0' => speak("No") );
	if ($currGW != "Mercury")
	{
		$ipc_encryption_choices['1'] = speak("Yes");
	}

	$fields[] = array(
		speak("Allow unencrypted Infinea Tab and Linea Pro readers").":",
		"config:disable_infinea_linea_encryption",
		"select",
		$ipc_encryption_choices
	);
}

if ($currGW=="Mercury" && is_lavu_admin())
{
	$fields[] = array(
		$lavu_only."Infinea Tab and Linea Pro readers use credit card encryption for gift cards:",
		"config:linea_gift_cards_use_same_encryption",
		"bool"
	);
}


if (is_lavu_admin())
{
	$fields = array_merge( $fields, array(
		array(
			$lavu_only."Disallow Infinea Tab and Linea Pro in-app firmware updates:",
			"config:disallow_ipc_firmware_update",
			"bool"
		),
		array(
			$lavu_only."(PayPal users - this setting does not apply) Do not attempt to connect with audio jack card reader:",
			"config:disable_audio_jack_reader_connection",
			"bool"
		)
	));
}

$fields = array_merge( $fields, array(
	array(
		speak("Audio jack reader type").":",
		"config:audio_jack_reader_type",
		"select",
		"PayPal,uDynamo,UniMag,Walker"
	),
	array(
		speak("Verify charge amount when swiping a credit card").":",
		"verify_swipe_amount",
		"bool"
	)
));

if ($currGW == "Mercury")
{
	$fields[] = array(
		speak("Include zip code field in Mercury Hosted Checkout form").":",
		"config:mercury_hosted_checkout_avs_fields",
		"bool"
	);
}
else
{
	$fields[] = array(
		speak("Require CVN when keying in credit card information").":",
		"require_cvn",
		"bool"
	);
}

// - - - - - - - - Signatures and Tips - - - - - - - - //

$fields = array_merge( $fields, array(
	array(
		speak("Signatures and Tips"),
		"Signatures and Tips",
		"seperator"
	),
	array(
		speak("Capture customer signatures on iPad").":",
		"cc_signatures",
		"bool"
	),
	array(
		speak("Capture customer signatures on iPhone/iPod Touch").":",
		"cc_signatures_ipod",
		"bool"
	),
	array(
		speak("Minimum amount for signature requirement").":",
		"config:minimum_amount_for_signature",
		"select",
		$signature_minimums
	),
	array(
		speak("Show tip input on signature screen").":",
		"allow_signature_tip",
		"select",
		array(
			'0' => speak("No"),
			'1' => speak("Above signature input (single step)"),
			'2' => speak("Prior to presenting signature input (two step)")
		)
	),
	array(
		speak("Tip Text").":",
		"config:tip_text",
		"text",
        "size:30"
	),
	array(
		speak("Skip tip entry prompt for EMV sale transactions").":",
		"config:emv_skip_tip_question",
		"bool"
	),
	array(
		speak("Base tip calculation on pre-discount amount for checks with only one payment").":",
		"config:pre_discount_determines_cc_tip",
		"bool"
	),
	array(
		speak("Bypass forced tip entry on signature screen").":",
		"config:bypass_forced_signature_tip",
		"bool"
	),
	array(
		speak("Allow transactions to be cancelled from signature screen").":",
		"config:allow_cancel_from_signature",
		"bool"
	),
	array(
		speak("Allow servers to manage tips at front end").":",
		"server_manage_tips",
		"bool"
	)
));

if (is_lavu_admin())
{
	$fields[] = array(
		$lavu_only.speak("Server Tips paging control").":",
		"config:manage_tips_orders_per_page",
		"select",
		array(
			'0'		=> speak("None"),
			'10'	=> speak("10 orders per page"),
			'25'	=> speak("25 orders per page")
		)
	);
}

if (is_lavu_admin() && $currGW=="TGate")
{
	$fields[] = array(
		$lavu_only."Fill amount field for Adjustment transactions (TGate Sale/Adjustment scheme):",
		"config:tip_adjust_send_total",
		"bool"
	);
}

$fields = array_merge( $fields, array(
	array(
		speak("Include gratuity line on printed checks")." (".speak("graphics mode only")."):",
		"config:printed_check_gratuity_lines",
		"bool"
	),
	array(
		speak("Include gratuity suggestions on printed checks and credit card receipts")." (".speak("graphics mode only")."):",
		"config:cc_receipt_gratuity_suggestions",
		"bool"
	),
	array(
		speak("Exclude gratuity line from credit card receipts").":",
		"config:exclude_cc_receipt_tip_line",
		"bool"
	),

// - - - - - - - - Tabs - - - - - - - - //

	array(
		speak("Tabs"),
		"Tabs",
		"seperator"
	),
	array(
		speak("Name to keep when generating new tab names from card swipes").":",
		"config:swiped_tab_name_format",
		"select",
		array(
			'0' => speak("First Name"),
			'1' => speak("Last Name"),
			'2' => speak("Both")
		)
	),
	array(
		speak("Do not ask to perform preauths when creating new tabs from card swipes").":",
		"config:no_preauth_upon_tab_creation",
		"bool"
	),
	array(
		speak("Default preauth amount for new tabs").":",
		"default_preauth_amount",
		"select",
		$default_preauth_amounts
	),

// - - - - - - - - Printing - - - - - - - - //

	array(
		speak("Printing"),
		"Printing",
		"seperator"
	),
	array(
		speak("Print credit card receipts for Tab PreAuths")." (".speak("merchant copy")."):",
		"config:tab_preauth_receipts_merchant",
		"bool"
	),
	array(
		speak("Print credit card receipts for Tab PreAuths")." (".speak("customer copy")."):",
		"config:tab_preauth_receipts_customer",
		"bool"
	),
	array(
		speak("Print individual credit card receipts")." (".speak("merchant copy")."):",
		"individual_cc_receipts",
		"bool"
	),
	array(
		speak("Print individual credit card receipts")." (".speak("customer copy")."):",
		"customer_cc_receipt",
		"bool"
	),
	array(
		speak("Print credit card transaction details on primary receipt").":",
		"append_cct_details",
		"bool"
	),
	array(
		speak("Print credit card Void receipts")." (".speak("merchant copy")."):",
		"config:cc_void_receipts_merchant",
		"bool"
	),
	array(
		speak("Print credit card Void receipts")." (".speak("customer copy")."):",
		"config:cc_void_receipts_customer",
		"bool"
	),
	array(
		speak("Print credit card Refund receipts")." (".speak("merchant copy")."):",
		"config:cc_refund_receipts_merchant",
		"bool"
	),
	array(
		speak("Print credit card Refund receipts")." (".speak("customer copy")."):",
		"config:cc_refund_receipts_customer",
		"bool"
	),
	array(
		speak("Include location address on credit card receipts").":",
		"config:cc_receipts_show_address",
		"bool"
	),
	array(
		speak("Use BlueBamboo printer for credit card receipts").":",
		"config:bluebamboo_cc_receipts",
		"bool"
	),
	array(
		speak("Use BlueBamboo printer for order receipts").":",
		"config:bluebamboo_order_receipts",
		"bool"
	),
));

// - - - - - - - - Lavu Integrated Payments - - - - - - - - //

if ($modules->hasModule("extensions.payment.econduit"))
{
	$processors = array(
		'EVO'			=> "EVO",
		'First Data'	=> "First Data",
		'Heartland'		=> "Heartland",
		'TSYS'			=> "TSYS",
		'Vantiv'		=> "Vantiv",
	);
	$data_name = sessvar('admin_dataname');
	$obj = new ConfigLocal($data_name);
	$result=$obj->getInfo('value','setting = "business_country" AND _deleted=0');
	$country = $result[0]['value'];
	if (strtolower($country) === 'canada') {
		$processors['Moneris'] = "Moneris";
	}
	ksort($processors);
	$fields = array_merge( $fields, array(
		array(
			speak("Lavu Integrated Payments"),
			"Lavu Integrated Payments",
			"seperator"
		),
		array(
			speak("Processor").":",
			"config:econduit_processor",
			"select",
			$processors
		),
		array(
			speak("Capture customer signatures").":",
			"config:econduit_signatures",
			"select",
			array(
				'0' => speak("Respect standard setting (above)"),
				'1' => speak("No"),
				'2' => speak("Yes"),
				'3' => speak("For sale transactions only"),
				'4' => speak("For capture transactions only")
			)
		),
		array(
			speak("Show tip input on signature screen").":",
			"config:econduit_allow_signature_tip",
			"select",
			array(
				'0' => speak("Respect standard setting (above)"),
				'1' => speak("No"),
				'2' => speak("Above signature input (single step)"),
				'3' => speak("Prior to presenting signature input (two step)")
			)
		),
		array(
			speak("Prompt for tip in app when not taking signature").":",
			"config:econduit_app_tip",
			"select",
			array(
				'0' => speak("No"),
				'1' => speak("Yes"),
				'2' => speak("For sale transactions only"),
				'3' => speak("For capture transactions only")
			)
		),
		array(
			speak("Tab PreAuth Capture front end workflow").":",
			"config:econduit_preauth_capture_workflow",
			"select",
			array(
				'0' => speak("Always capture"),
				'1' => speak("Capture only when tip is included"),
				//'2' => speak("Perform secondary auth"),
				'3' => speak("Rely solely on backend capture")
			)
		)
	));

	if (is_lavu_admin())
	{
		$fields = array_merge( $fields, array(
			array(
				$lavu_only."Use iFrame for tip adjustments and preauth captures:",
				"config:iFrameTipAdjust",
				"bool"
			)
		));
	}
}

$fields[] = array(speak("Save"), "submit", "submit");

require_once(resource_path()."/form.php");

if (is_lavu_admin())
{
	echo "<br>".$lavu_only."Indicates items visible only to Lavu Admin users (i.e., developers and support staff)";
}

if (isset($_GET['show_connector']))
{
	echo "<br><br>Location Id: ".$locationid;
	echo "<br>Connector Name: ".admin_info("dataname");
	echo "<br>Connector Key: ".lsecurity_key(admin_info("companyid"));
}
echo "<br><br>";

$promptboxes_to_display[] = array();
include(__DIR__."/../resources/promptbox.php");
echo <<<JS
{$promptbox_html}
<script type="text/javascript">
var currentGateway = "{$currGW}";
var isFreeLavu = "{$is_free_lavu}";
$("#gateway").change( function () {
if ( $("#gateway").val() == "Authorize.net" )
$("#gateway").parent().parent().after("<tr class=authorizeNotice><td colspan=2>Authorize.net does not support Tip Adjustments and will not allow an Authorization to be captured for an amount<br> greater than the amount for which is was originally authorized.</td></tr> ");
else if ( isFreeLavu && currentGateway == "Mercury" && $("#gateway").val() != "Mercury" )
showPromptbox('freelavu_processor_changeaway');
else if ( isFreeLavu && currentGateway == "EVOSnap" && $("#gateway").val() != "EVOSnap" )
showPromptbox('freelavu_processor_changeaway');
else
$(".authorizeNotice").remove();
} );
</script>
JS;

//error_log("CP DEBUG: currGW={$currGW} is_free_lavu={$is_free_lavu}");  //debug

if ($is_free_lavu && isset($_POST['posted']) && $currGW!="Mercury" && $currGW!="EVOSnap")
{
	echo "<script language='javascript'>window.location.replace('/cp/?mode=billing&enter_cc=1');</script>";
	exit();
}

function availableGetSwipeExtensions()
{
	global $modules;

	$get_components = mlavu_query("SELECT `id`, `title`, `modules_required` FROM `poslavu_MAIN_db`.`component_layouts` WHERE `special_type` = 'payment_extension' AND `get_swipe` = '1' AND `_deleted` = '0'");
	if (mysqli_num_rows($get_components) == 0)
	{
		return;
	}

	$available_extensions = array( '' => speak("None") );
	$current_setting = locationSetting("lavu_gift_get_swipe_extension");

	while ($info = mysqli_fetch_assoc($get_components))
	{
		$id = $info['id'];
		$title = $info['title'];
		if (empty($title))
		{
			continue;
		}

		if (!empty($info['modules_required']) && !$modules->hasModule($info['modules_required']) && ($id != $current_setting))
		{
			continue;
		}

		$available_extensions[$id] = $title;
	}

	ksort($available_extensions);

	return $available_extensions;
}
