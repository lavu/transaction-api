<?php

	if(!empty($_POST['table_sync_request'])){	
		session_start();
		require_once (__DIR__."/../resources/core_functions.php");
		require_once (__DIR__."/../resources/chain_functions.php");
		if(sessvar('admin_access_level') < 4){
			echo "Permission Denied";
			return;
		}
		lavu_connect_dn(sessvar("admin_dataname"));
		if($_POST['table_sync_request'] == 'sync_table'){
			syncTableRequest();
		}
	}else{ //Render page, is not an ajax handler.
		if(sessvar('admin_access_level') < 4){
			echo "Permission Denied";
			return;
		}
		//Generate the JS and css for the area.	
		$tableSyncJS = file_get_contents(__DIR__.'/table_sync.js');
		$tableSyncCSS = file_get_contents(__DIR__.'/table_sync.css');


		//Marshal required initial data.
		/* Example packet
		{
			"primary_id": 42161,
			"primary_dataname": "acts_dev",
			"datanames_in_chain": [{
				"id": "42161",
				"data_name": "acts_dev",
				"title": "Acts - Dev",
				"is_primary": true
			}, {
				"id": "42337",
				"data_name": "acts_bistro_de",
				"title": "Acts Bistro Dev ",
				"is_primary": false
			}],
			"sync_allowed_tables": ["med_customers"],
			"account_table_row_counts": {
				"acts_dev": {
					"med_customers": "1"
				},
				"acts_bistro_de": {
					"med_customers": "0"
				}
			}
		}
		*/
		$syncPermissions = getTableSyncPermissionsAndInfo($data_name);
		$lastSyncTimes = getLastTableSyncTimesRowFromConfig();
		//echo "Sync Permission:".json_encode($syncPermissions,1);

		$staticData = array();
		$staticData['sync_permissions'] = $syncPermissions;
		$staticData['last_sync_times'] = json_decode($lastSyncTimes['value'],1);
		$staticData['allow_verification_of_sync'] = false;
		$staticDataJSON = json_encode($staticData);
		
		echo "<div id='table_sync_content_div' class='table_sync_content_div'></div>";
		echo "<style>".$tableSyncCSS."</style>";
		echo "<script>".
				"var staticData = ".$staticDataJSON.";\n".
				"var background_div = document.getElementById('table_sync_content_div');\n".
				$tableSyncJS.
				"buildArea();".
			 "</script>";

	}


/*  Moving to chain functions.
	function getLastTableSyncTimesFromConfig(){
		$tableToLastSyncTimeMap = array();
		$key = 'sync_primary_of_chain_table_last_times';
		$result = lavu_query("SELECT * FROM `config` WHERE `setting`='sync_primary_of_chain_table_settings'");
		if(!$result){
			error_log("SQL error in: ".__FILE__.' -shrsre8-');
		}else{
			if(mysqli_num_rows($result)){
				$row = mysqli_fetch_assoc($result);
				$tableToLastSyncTimeMap = json_decode($row['value']);
			}
		}
		return $tableToLastSyncTimeMap;
	}
*/


	function syncTableRequest(){
		$tableToSync = $_POST['table_2_sync'];
		$childDataname = $_POST['child_dataname'];
		$parentDataname = sessvar("admin_dataname");

		//echo "tableToSync:".$tableToSync."\n"."childDataname:".$childDataname."\n"."parentDataname:".$parentDataname."\n";
		$jsonReturn = array();
		$syncSuccessInfo = syncTableToChildAccount($parentDataname, $childDataname, $tableToSync);
		if(empty($syncSuccessInfo)){
			$jsonReturn['status'] = 'Fail';
		}else{
			$syncSuccessInfo['status'] = "Success";
			$jsonReturn = $syncSuccessInfo;
		}
		echo json_encode($jsonReturn);
	}










