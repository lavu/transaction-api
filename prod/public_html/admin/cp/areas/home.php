<?php
	//ini_set("display_errors",1);
	require_once dirname( dirname(__FILE__) ) . '/reports_v2/report_functions.php';
	require_once dirname( dirname(__FILE__) ) . '/reports_v2/area_reports.php';
	require_once(dirname(__FILE__)."/../resources/messagesDB.php");
	require_once(dirname(__FILE__) . "/survey.php");
	// SHOP.LAVU.COM pop up
	// require_once(dirname(__FILE__)."/shopPopup.php");
	// Inventory 2.0 pop up
		require_once(dirname(__FILE__)."/inventoryPopup.php");


	$ms = array();
	$ms['comma_char'] = $location_info['decimal_char'] !== '.'?'.':',';
	$ms['decimal_char'] = $location_info['decimal_char'];
	$ms['decimal_places'] = $location_info['disable_decimal'];
	$ms['monitary_symbol'] = $location_info['monitary_symbol'];
	$ms['left_or_right'] = $location_info['left_or_right'];

	prepare_floating_window();

	function prepare_floating_window()
	{
		echo "
		<div style='position:absolute; left:0px; top:0px; width:100%; height:100%; z-index:1000; visibility:hidden' id='floating_window'>
			<table width='100%'>
				<tr>
					<td align='center'>
						<table style='border:solid 2px #777777' cellpadding=6 cellspacing=0 bgcolor='#ffffff'>
							<tr>
								<td align='right' bgcolor='#eeeeee'>
									<table cellspacing=0 cellpadding=0 width='100%'>
										<tr>
											<td width='10%' align='left'>&nbsp;</td>
											<td width='80%' align='center' style='font-weight:bold; color:#aaaaaa; font-size:14px' id='floating_window_title'>&nbsp;</td>
											<td width='10%' align='right'>
												<b>
													<font style='color:#aaaaaa; font-size:20px; cursor:pointer' onclick='close_floating_window()'>X</font>
												</b>
											</td>
										</tr>
									</table>
								</td>
							</tr>
							<tr>
								<td width='480' height='320' id='floating_window_content' align='center' valign='top'>
									&nbsp;
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</div>

		<script language='javascript'>
		function open_floating_area(load_area,load_vars,set_title) {
			if(!set_title) set_title = '&nbsp;';
			document.getElementById('floating_window_title').innerHTML = set_title;
			document.getElementById('floating_window_content').innerHTML = '<table width=\"100%\" height=\"100%\"><tr><td align=\"center\" valign=\"center\"><img src=\"http://hotel.lavuhotel.comimages/animated-progress.gif\" /></td></tr></table>';
			document.getElementById('floating_window').style.visibility = 'visible';
			runGenericAjaxRequestDennedyStyle(load_area,load_vars,'floating_window_content');
		}
		function open_floating_window(set_content,set_title) {
			document.getElementById('floating_window_title').innerHTML = set_title;
			if(set_content.substring(0,8)=='from_id:') { set_content = document.getElementById(set_content.substring(8)).innerHTML; set_content = set_content.replace(/iidd/ig,'id');}
			document.getElementById('floating_window_content').innerHTML = set_content;
			document.getElementById('floating_window').style.visibility = 'visible';
		}
		function close_floating_window() {
			document.getElementById('floating_window_content').innerHTML = '';
			document.getElementById('floating_window').style.visibility = 'hidden';
		}
		function find_and_eval_script_tags(estr,script_start,script_end) {
			var xhtml = estr.split(script_start);
			for(var n=1; n<xhtml.length; n++)
			{
				var xhtml2 = xhtml[n].split(script_end);
				if(xhtml2.length > 1)
				{
					run_xhtml = xhtml2[0];
					eval(run_xhtml);
				}
			}
		}
		</script>
";
	}

	function getSalesOnDay($date){
		global $location_info;
		$locid= $location_info['id'];
		$mins= ($location_info['day_start_end_time']*1)%100;
		$hours= (($location_info['day_start_end_time']*1)-$mins)/100;
		$dayStart = date("H:i:s",mktime($hours, $mins, 0,1,1,2000));
		$end  = date("Y-m-d",strtotime("+1 day")). " ". $dayStart;
		$start= $date. " ". $dayStart;
		$query= lavu_query("select `cc_transactions`.`loc_id` AS `field2`,`cc_transactions`.`card_type` AS `field3`,`orders`.`void` AS `field4`,`cc_transactions`.`amount` AS `field5`,`cc_transactions`.`voided` AS `field6`,`orders`.`location_id` AS `field7`,`cc_transactions`.`got_response` AS `field8`,`cc_transactions`.`transtype` AS `field9`,`cc_transactions`.`order_id` AS `field10`,`orders`.`order_id` AS `field11`,IF(`voided`='1',0,IF(`action`='Refund', (0 - sum(`cc_transactions`.`amount`)), sum(`cc_transactions`.`total_collected`))) AS `field12`,`cc_transactions`.`pay_type` AS `field13`,IF(`cc_transactions`.`order_id`='Paid Out','Paid Out',IF(`cc_transactions`.`order_id`='Paid In','Paid In',`cc_transactions`.`action`)) AS `field14` from `cc_transactions` LEFT JOIN `orders` ON `cc_transactions`.`order_id` = `orders`.`order_id`where `cc_transactions`.`datetime` >= '[1]' and `cc_transactions`.`datetime` <= '[2]' and `cc_transactions`.`loc_id` = '[3]' and `cc_transactions`.`voided` = 0 and `orders`.`location_id` = `cc_transactions`.`loc_id` and `cc_transactions`.`transtype` != 'AddTip' and (`cc_transactions`.`action` = 'Sale' OR `cc_transactions`.`action` = 'Refund') group by concat(`cc_transactions`.`pay_type`,' ',IF(`action`='Refund', 'Refund', ''),IF(`cc_transactions`.`order_id` LIKE 'Paid%','PaidInOut',''))", $start, $end, $locid);
		$total=0;
		while($read = mysqli_fetch_assoc($query)){
			if( $read['field13']=='Cash' && $read['field14']=='Sale'){
				$total+= $read['field12'];
			}else if($read['field13']=='Card' && $read['field14']=='Sale'){
				$total+= $read['field12'];
			}
		}
		return $total;
	}

	function output_todays_sales()
	{
		global $remember_total_sales_for_day;

		if(isset($remember_total_sales_for_day))
		{
			return home_print_money($remember_total_sales_for_day);
		}
		else
		{
			$today = date('Y-m-d');
			$sales = getSalesOnDay($today);
			return home_print_money($sales);
		}
	}

	function home_print_money($amount) {
		global $location_info;
		if ($location_info['left_or_right'] == 'left')
		{
			return $location_info['monitary_symbol'].number_format($amount, $location_info['disable_decimal'], $location_info['decimal_char'], ',');
		}else{
			return number_format($amount, $location_info['disable_decimal'], $location_info['decimal_char'], ',').$location_info['monitary_symbol'];
		}
	}

	function output_shortcuts($style_type="vertical")
	{
		$mouseover_color = "#ddeeff";
		//$mouseover_color = "#d7e79b";
		$shortcuts = array("Schedule","End of Day","Timecards","Sales","Payments", "Web gift");
		$links = array("?mode=settings_scheduling","?mode=reports_reports_v2&report=33","?mode=reports_time_cards","?mode=reports_reports_v2&report=31","?mode=reports_reports_v2&report=30","?mode=webgift");
		$imgs = array("images/schedule_icon.png","images/endofday_icon.png","images/timecard_icon.png","images/salesreports_icon.png","images/payments_icon.png","images/webgift_icon.png");

		if($style_type!="vertical") $vertical = false;
		if($vertical)
		{
			$cellwidth = "100%";
			$cellheight = "120";
		}
		else
		{
			$cellwidth = "140";
			$cellheight = "120";
		}

		$str = "";
		$str .= "<table width='100%'>";
		for($i=0; $i<6; $i++)
		{
			$shortcut_title = (isset($shortcuts[$i]))?$shortcuts[$i]:"";
			$shortcut_link = (isset($links[$i]))?$links[$i]:"";
			$shortcut_img = (isset($imgs[$i]))?$imgs[$i]:"";

			if($shortcut_title!="")
			{
				$extra_style = "cursor:pointer";
				$output_img = "<img src='$shortcut_img' border='0' />";
				if ($shortcut_title == 'Web gift') { // Refer LP-8841
					$control_str = " onmouseover='this.bgColor = \"$mouseover_color\"' onmouseout='this.bgColor = \"#ffffff\"' onclick='window.open(\"$shortcut_link\", \"_Blank\")'";
				} else {
					$control_str = " onmouseover='this.bgColor = \"$mouseover_color\"' onmouseout='this.bgColor = \"#ffffff\"' onclick='window.location = \"$shortcut_link\"'";
				}
			}
			else
			{
				$extra_style = "";
				$output_img = "";
				$control_str = "";
			}

			if($vertical) $str .= "<tr>";
			$str .= "<td width='$cellwidth' height='$cellheight' style='$extra_style' align='center' valign='middle'".$control_str.">";
			$str .= "$output_img<br><font style='color:#aaaaaa'>$shortcut_title</font>";
			$str .= "</td>";
			if($vertical) $str .= "</tr>";
		}
		$str .= "</table>";
		return $str;
	}

	function output_home_page_messages($maxMessages=10)
	{
		$str = "";
		$fromCP=true;

		$messagesDB = new messagesDB();
		$messages = $messagesDB->getMessages();
		//$maxMessages = 6;
		if(count($messages) < $maxMessages){
			$maxMessages = count($messages);
		}
		if(admin_info('username')!= 'poslavu_admin'){
			$result=mysqli_fetch_assoc(lavu_query("SELECT `viewed_messages` FROM `users` WHERE `username`='[1]' ",admin_info('username')));
			$viewed_messages_arr= explode(",",$result['viewed_messages']);
		}

		$str .= "<table cellpadding=6 cellspacing=0>";
		for ($x = 0; $x < $maxMessages; $x++) {
			$message = $messages[$x];
			$type 	 = $message['type'];
			$title 	 = $message['title'];
			$content = $message['content'];
			$id		 = $message['id'];
			$new_m   = false;
			if(admin_info('username')!= 'poslavu_admin' && is_array($viewed_messages_arr) && !in_array($id, $viewed_messages_arr))
			{
				$unread_message_counter++;
				$new_m=true;
			}
			$contentTrimmed = strip_tags(substr($content, 0, 50).'…');
			$imgSrc = '';
			switch($type){
				case 1:
					$imgSrc = 'smallMailIconColor';
					break;
				case 2:
					$imgSrc = 'smallGearIconDarkGray';
					break;
				case 3:
					$imgSrc = 'smallAmbulanceIcon';
					break;
				case 4:
					$imgSrc = 'smallBugIconColor';
					break;
				default:
					$imgSrc = 'smallMailIconColor';
			}
			$extrastyle = "border-bottom:solid 1px #cccccc";
			if($x >= $maxMessages - 1) $extrastyle = "";

			$inner_container_id = "message_content_$x";
			//$onclick = "onclick='document.getElementById(\"$inner_container_id\").style.display = \"block\"'";
			$onclick = "onclick='open_floating_window(\"from_id:$inner_container_id\",\"$title\")'";
			$control_str = " onmouseover='this.bgColor = \"#d7e79b\"' onmouseout='this.bgColor = \"#ffffff\"' " . $onclick;

			$show_date = "&nbsp;";
			$cd_parts = explode(" ",$message['created_date']);
			$cd_parts = explode("-",$cd_parts[0]);
			if(count($cd_parts) > 2)
			{
				$cd_year = $cd_parts[0];
				$cd_month = $cd_parts[1];
				$cd_day = $cd_parts[2];
				$cd_ts = mktime(0,0,0,$cd_month,$cd_day,$cd_year);
				$days_ago = floor((time() - $cd_ts) / 60 / 60 / 24);

				if($days_ago==0) $show_date = "Today";
				else if($days_ago > 0 && $days_ago < 6) $show_date = date("l",$cd_ts);
				else if(date("Y",$cd_ts)!=date("Y")) $show_date = date("m/d/Y",$cd_ts);
				else $show_date = date("M d",$cd_ts);
			}

			$str .= "<tr style='cursor:pointer'".$control_str.">";
			//$str .= "<td valign='top' align='right' style='font-family:Verdana; color:#3382a9; font-size:10px; $extrastyle'><nobr>$show_date</nobr></td>";
			$str .= "<td style='$extrastyle' valign='top'>";
			$str .= "<img border=0 class=\"messagesIcon\" src=\"images/{$imgSrc}.png\"/>";
			$str .= "</td>";

			$str .= "<td valign='top' style='font-family:Verdana; color:#888888; $extrastyle'>";
			$str .= $title;
			$str .= "<div id='$inner_container_id' style='display:none'>";
			$str .= "<table><tr><td valign='top' align='left'>" . urldecode($content) . "</td></tr></table>";
			$str .= "</div>";
			$str .=  "</td></tr>";
		}
		$str .= "</table>";
		return $str;
	}

	function getSalesForYear(){
		global $location_info;
		$locid= $location_info['id'];
		$endDate = date("Y-m-d", strtotime("+1 day", strtotime(date('Y-m-d'))));
		$startDate= date("Y-m-01",strtotime("-366 days", strtotime(date( 'Y-m-d'))));
		$mins= ($location_info['day_start_end_time']*1)%100;
		$hours= (($location_info['day_start_end_time']*1)-$mins)/100;
		$dayStart = date("H:i:s",mktime($hours, $mins, 0,1,1,2000));
		$end  = $endDate. " ". $dayStart;
		$start= $startDate. " ". $dayStart;


		$payments_query = str_replace("\n", "", "SELECT IF( date_format( now(), \"%y\") = date_format(`cc_transactions`.`datetime`,'%y'), date_format(`cc_transactions`.`datetime`, '%b'), date_format(`cc_transactions`.`datetime`, '%b \'%y') ) as `interval`,
		`payment_types`.`type` as `payment_type`,
		COUNT(cc_transactions.id ) as `count`,
		sum( IF(`cc_transactions`.`action`='Refund', -1*`cc_transactions`.`total_collected`, `cc_transactions`.`total_collected`) ) as `total_payment`
	FROM `cc_transactions`
	LEFT JOIN `payment_types` ON `cc_transactions`.`pay_type_id` = `payment_types`.`id` AND
		(`cc_transactions`.`loc_id` = `payment_types`.`loc_id` OR '0' = `payment_types`.`loc_id` )
	WHERE
		`cc_transactions`.`datetime` >= '[1]' AND
		`cc_transactions`.`datetime` <= '[2]' AND
		`cc_transactions`.`voided` = '0' AND
		`cc_transactions`.`action` IN ('Sale', 'Refund') AND
		`cc_transactions`.`action` != 'Void' AND
		`cc_transactions`.`loc_id` ='[3]'
	GROUP BY
		date_format(`cc_transactions`.`datetime`,'%Y-%m'),
		`payment_types`.`type`
	ORDER BY
	 date_format(`cc_transactions`.`datetime`,'%Y-%m') ASC");
		if( ($payment_result = lavu_query( $payments_query, $start, $end, $locid ) ) !== FALSE ) {
			if (mysqli_num_rows($payment_result) < 1) return "empty";
			$payment_arr = array();
			$intervals = array();
			$pay_types = array();
			while( $row = mysqli_fetch_assoc($payment_result) ){
				$payment_arr[] = $row;
				$intervals[] = $row['interval'];
				$pay_types[] = $row['payment_type'];
			}
			mysqli_free_result($payment_result);
			$tmp_intervals = array_values(array_unique($intervals));
			$pay_types = array_values(array_unique($pay_types));
			$year_num = " '" . substr(  ((int)date('Y') - 1), -2);
			$all_months = array('Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec');
			foreach($all_months as $k => $v) $all_months[$k] = $v . $year_num;
			$all_months = array_merge($all_months, array('Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'));
			$max_month = array_search($tmp_intervals[count($tmp_intervals)-1], $all_months) + 1;
			$min_month = array_search($tmp_intervals[0], $all_months);
			// echo '<pre style="text-align: left;">' . print_r( $all_months, true ) . '</pre>';
			$intervals = array();

			if( is_numeric($min_month) AND is_numeric($max_month) AND ($max_month > $min_month) AND ($max_month - $min_month < 50) ){
				for($i = $min_month; $i <= $max_month; $i++) {
					$intervals[] = $all_months[$i];
				}
			}
			// $intervals = $tmp_intervals; //if reverting to old behavior is desired.
			//dates
			//pay_types

			$result = array();
			$result['labels'] = $intervals;
			$result['datasets'] = array();

			for( $j = 0; $j < count($pay_types); $j++ ){
				$data = array();
				$data['label'] = $pay_types[$j];
				$data['data'] = array();
				for( $i = 0; $i < count($intervals); $i++ ){
					$success = false;
					foreach( $payment_arr as $k => $v ) {
						if( $v['payment_type'] == $pay_types[$j] ) {
							if( $v['interval'] == $intervals[$i] ){
								$success = true;
								$data['data'][] = $v['total_payment'];
								break;
							}
						}
					}
					if(!$success){
						$data['data'][] = 0;
					}
				}
				$result['datasets'][] = $data;
			}

			if(count($result['datasets']) < 1)
			{
				return "empty";
			}

			return json_encode($result);
		} else {
			return '{ error: "' . lavu_dberror() . '" }';
		}
	}

	function report_format_money($m)
	{
		global $ms;
		$mstr = number_format(str_replace(",","",$m),$ms['decimal_places'],$ms['decimal_char'],$ms['comma_char']);
		if($ms['left_or_right']=="right") return $mstr . $ms['monitary_symbol'];
		else return $ms['monitary_symbol'] . $mstr;
	}

	function parse_report_lavu_query_string($query)
	{
		global $location_info;

		$rep_loc_id= $location_info['id'];
		$se_mins= ($location_info['day_start_end_time']*1)%100;
		$se_hours= (($location_info['day_start_end_time']*1)-$se_mins)/100;
		//$rep_loc_id = "1";
		$rep_start = date("Y-m-d H:i:s",mktime($se_hours,$se_mins,0,date("m"),date("d"),date("Y")));
		$rep_end = date("Y-m-d H:i:s",mktime($se_hours,$se_mins,0,date("m"),date("d")+1,date("Y")));
		$conn = ConnectionHub::getConn('poslavu');
		$query = str_replace("[start_datetime]",$conn->escapeString($rep_start),$query);
		$query = str_replace("[end_datetime]",$conn->escapeString($rep_end),$query);
		$query = str_replace("[loc_id]",$conn->escapeString($rep_loc_id),$query);

		return $query;
	}

	function run_report_quick_sum($col,$query,$repeatn=1)
	{
		$total_value = 0;
		$query = parse_report_lavu_query_string($query);

		for($n=0; $n<$repeatn; $n++)
		{
			$run_query = $query;
			$run_query = str_replace("[n]",($n + 1),$run_query);

			$set_value = 0;
			$rep_query = lavu_query($run_query);
			if(mysqli_num_rows($rep_query))
			{
				$rep_read = mysqli_fetch_assoc($rep_query);
				$set_value = $rep_read[$col];
				$set_value = str_replace(",","",str_replace("[money]","",$set_value));
			}
			$total_value += $set_value * 1;
		}

		return report_format_money($total_value);//"$" . number_format(str_replace(",","",$total_value),2);
	}

	function report_add_sums($n1,$n2,$n3=0,$n4=0)
	{
		$n1 = report_money_to_number($n1);//str_replace(",","",$n1) * 1;
		$n2 = report_money_to_number($n2);//str_replace(",","",$n2) * 1;
		$n3 = report_money_to_number($n3);//str_replace(",","",$n3) * 1;
		$n4 = report_money_to_number($n4);//str_replace(",","",$n4) * 1;
		return report_format_money($n1 + $n2 + $n3 + $n4);//"$" . number_format($n1 + $n2 + $n3 + $n4,2);
	}

	$remember_total_sales_for_day = 0;
	function getSalesForDay()
	{
		global $remember_total_sales_for_day;

		$query = "select menu_categories.name as category, sum(order_contents.quantity) as quantity, if(count(distinct orders.ioid )>count( distinct orders.order_id ),count( distinct orders.ioid ),count( distinct orders.order_id )) as order_count, if(count(*) > 0, concat('[money]',format(sum((order_contents.price + order_contents.modify_price + order_contents.forced_modifiers_price)*order_contents.quantity - if( order_contents.quantity > '0', order_contents.itax, 0) ),2)), '[money]0.00') as gross, if(count(*) > 0, concat('[money]',format(sum(if(order_contents.quantity > 0, order_contents.discount_amount + order_contents.idiscount_amount, 0)),2)), '[money]0.00') as discount, if(count(*) > 0, concat('[money]',format(sum((order_contents.price + order_contents.modify_price + order_contents.forced_modifiers_price)*order_contents.quantity - if( order_contents.quantity > '0',order_contents.discount_amount + order_contents.idiscount_amount + order_contents.itax, 0)),2)), '[money]0.00') as net, if(count(*) > 0, concat('[money]',format(sum( if( order_contents.quantity != 0 AND order_contents.tax_exempt != '1', `order_contents`.`tax1` + `order_contents`.`tax2` + `order_contents`.`tax3` + `order_contents`.`tax4` + `order_contents`.`tax5` - `order_contents`.`itax`, 0 ) ),2)), '[money]0.00') as order_tax, if(count(*) > 0, concat('[money]',format(sum( if( order_contents.quantity != 0 AND order_contents.tax_exempt != '1', `order_contents`.`itax`, 0) ),2)), '[money]0.00') as included_tax, `menu_categories`.`name` as `group_col_menu_categories_name` from `orders`LEFT JOIN `order_contents` ON `orders`.`order_id` = `order_contents`.`order_id` AND `orders`.`location_id` = `order_contents`.`loc_id` LEFT JOIN `menu_items` ON `order_contents`.`item_id` = `menu_items`.`id` LEFT JOIN `menu_categories` ON `menu_items`.`category_id` = `menu_categories`.`id` where orders.closed >= '[start_datetime]' and orders.closed < '[end_datetime]' and orders.void = '0' and orders.location_id='[loc_id]' and order_contents.item != 'SENDPOINT' group by `category`";
		$query = parse_report_lavu_query_string($query);
		$sales_query = lavu_query($query);

		$method = "datasets";
		$salesarr = array();
		$calc_total = 0;
		if($method=="datasets")
		{
			$salesarr['datasets'] = array();
			while($sales_read = mysqli_fetch_assoc($sales_query))
			{
				$dataobj = array();
				$dataobj['label'] = $sales_read['category'];
				$dataobj['data'] = array();
				$dataobj['data'][] = str_replace("[money]","",str_replace(",","",$sales_read['gross']));
				$salesarr['datasets'][] = $dataobj;
				$calc_total = $calc_total + str_replace("[money]","",str_replace(",","",$sales_read['gross'])) * 1;
			}
			if(count($salesarr['datasets']) < 1)
			{
				$dataobj = array();
				$dataobj['label'] = "No Sales";
				$dataobj['data'] = array();
				if(isset($_GET['showmenu']))
					$dataobj['data'][] = "10.01";
				else $dataobj['data'][] = "0.00000000001";
				$salesarr['datasets'][] = $dataobj;
			}
		}
		else
		{
			while($sales_read = mysqli_fetch_assoc($sales_query))
			{
				$salesarr[] = str_replace("[money]","",str_replace(",","",$sales_read['gross']));
				$calc_total = $calc_total + str_replace("[money]","",str_replace(",","",$sales_read['gross'])) * 1;
			}
			if(count($salesarr) < 1)
			{
				$salesarr[] = 1;
			}
		}
		if($calc_total > 0)
		{
			$remember_total_sales_for_day = $calc_total;
		}

		if(isset($_GET['jsl'])) echo json_encode($salesarr) . "<br>";
		return json_encode($salesarr);
	}

	function create_cpgraph_code($graph_name,$graph_type,$graph_data, $width, $height, $extra_code="", $extra_canvas_code="")
	{
		$htmlstr = "";
		$htmlstr .= "<div class=\"cpdash-$graph_name\"><div style='position:relative;'>";
		$htmlstr .= "	&nbsp;";
		$htmlstr .= "	<div style='positon:absolute; top:0px; left:0px; z-index:400'>";
		$htmlstr .= "		<canvas id=\"c_summary-$graph_name\" data=\"$graph_data\" width=\"$width\" height=\"$height\" style=\"width: ".$width."px; height: ".$height."px;\"".$extra_canvas_code."></canvas>";
		$htmlstr .= "	</div>";
		$htmlstr .= $extra_code;
		$htmlstr .= "</div></div>";

		$jsstr = "";
		$jsstr .= "var canvas_$graph_name = document.getElementById('c_summary-$graph_name'); ";
		$jsstr .= "var ctx_$graph_name = canvas_$graph_name.getContext('2d'); ";
		$jsstr .= "var chart_$graph_name = new Chart( ctx_$graph_name ); ";
		$jsstr .= "canvas_$graph_name.chart = chart_$graph_name; ";
		$jsstr .= "var data_$graph_name = JSON.parse( canvas_$graph_name.getAttribute('data') ); ";
		$jsstr .= "var c_$graph_name = chart_$graph_name.$graph_type( data_$graph_name ); ";

		return array("html"=>$htmlstr,"js"=>$jsstr);
	}

	function output_things_you_should_know()
	{
		$res = mysqli_fetch_assoc(mlavu_query("SELECT * FROM `poslavu_MAIN_db`.`messages` WHERE `type`='6' LIMIT 1"));
		return ($res === false || empty($res['content'])) ? '' : '<iframe style="margin:auto" width="180" height="135" src="'.$res['content'].'?wmode=transparent" frameborder="0" allowfullscreen></iframe>';
	}

	$hide_sales_for_year = (locationSetting("cp_home_hide_sales_for_year") == "1");

	$salesdata = htmlspecialchars(getSalesForDay());
	$rawyearsalesdata = $hide_sales_for_year?"":getSalesForYear();
	$yearsalesdata = htmlspecialchars($rawyearsalesdata);

	if($rawyearsalesdata=="empty")
	{
		$sales_output_str = "&nbsp;";
	}
	else
	{
		$sales_output_str = output_todays_sales();
	}

	$header_color_messages = "#97b74b";//"#77aa66";
	$header_color_shortcuts = "#8899cc";
	$header_color_messages_dark = "#77875b";//"#77aa66";
	$header_color_shortcuts_dark = "#7889ac";

	$extra_gcode = "<div id=\"c_legend_container\" style=\"position:absolute; visibility:visible; top:10px; left:110px;\"><table width='240'><tr><td width='100%' align='right'><div id=\"c_legend\" style=\"font-size: 8px; opacity:.9\"></div></td></tr></table></div>";
	$extra_gcode2 = "	<div style='position:absolute; top:33px; left:-30px; z-index:120'><table cellspacing=0 cellpadding=6 bgcolor='#ffffff' style='border:solid 1px $header_color_messages; '><tr><td width='120' align='center' style='font-size:16px; color:$header_color_messages_dark;' bgcolor='#ffffff'>Today's Sales</td></tr></table></div>";
	$extra_gcode2 .= "  <div id='todays_sale_container' style='position:absolute; top:134px; left:50px; z-index:20'><table cellspacing=0 cellpadding=0><tr><td width='200' align='center' style='font-size:18px; color:#aaaaaa'>" . $sales_output_str . "</td></tr></table></div>";
	$extra_gcode2 .= "  <div id='todays_sale_control_container' style='position:absolute; top:20px; left:0px; z-index:10; width:300px; height:270px; border-radius:100%' onmouseover='document.getElementById(\"todays_sale_container\").style.visibility = \"hidden\"; this.style.zIndex = -60'></div>";
	$more_gcode2 = " onmouseout='document.getElementById(\"todays_sale_control_container\").style.zIndex = 10; document.getElementById(\"todays_sale_container\").style.visibility = \"visible\"; c_sales.mouseMovedTo( { x: 0, y : 0 } );'";

	$extra_gcode .= "	<div style='position:absolute; top:-32px; left:60px; z-index:120'><table cellspacing=0 cellpadding=6 bgcolor='#ffffff' style='border:solid 1px $header_color_shortcuts; '><tr><td width='120' align='center' style='font-size:16px; color:$header_color_shortcuts_dark;' bgcolor='#ffffff'>At a Glance</td></tr></table></div>";

	$gcode = $hide_sales_for_year?"":create_cpgraph_code("sales2", "Line", $yearsalesdata, 400, 240, $extra_gcode);
	$gcode2 = create_cpgraph_code("sales","Doughnut",$salesdata, 300, 270,$extra_gcode2,$more_gcode2);

	$header_color_general = "#f4f4f4";
	$border_color_general = "#c2c2c2";
	$text_color_general = "#aaaaaa";

	$set_color = "#f6f6f6";
	$ds_str = draw_satisfaction_query(admin_info('dataname'));
	$ds_str = str_replace("#888888","$set_color",$ds_str);
	$ds_str = str_replace("#f5fed0","$set_color",$ds_str);
	$ds_str = str_replace("Survey","",$ds_str);
	$ds_str = str_replace("cols=\"60\"","cols=\"32\"",$ds_str);
	// $ds_str = '';
	//SURVEY IS DISABLED HERE. Remove above line and uncomment the lines above it to re-enable!

	$survey_str = "";
	$survey_str .= "<div id='main_poslavu_survey' style='display:none'>";
	$survey_str .= $ds_str;
	$survey_str .= "</div>";
	$survey_str .= "<div id='main_poslavu_survey_entry' style='display:block'>";
	$survey_str .= "<font style='color:#777777'>Please take a moment to fill out this brief survey.  We are always looking for ways to improve our processes and your feedback is important to us.</font><table width='100%' cellspacing=0 cellpadding=0><tr><td width='100%' align='right'><input type='button' value='Take Survey >>' style='cursor:pointer; color:#ffffff; font-size:12px; background-color:#99bb55; border-radius:6px; border:solid 1px #779944; width:120px; height:20px' onclick='document.getElementById(\"main_poslavu_survey_entry\").style.display = \"none\"; document.getElementById(\"main_poslavu_survey\").style.display = \"block\"' /></td></tr></table>";
	$survey_str .= "</div>";

	echo "<table>";
	echo "	<tr>";
	echo "		<td valign='top'>";
	//echo "			<table width='100%' cellpadding=6 cellspacing=0 style='border:solid 1px $border_color_general'>";
	//echo "				<tr><td width='100%' bgcolor='$header_color_general' align='center' style='color:$text_color_general; font-weight:bold; font-size:16px'>Sales</td></tr>";
	//echo "				<tr><td align='center'>";
	if($rawyearsalesdata=="empty")
	{
		$extra_gcode = str_replace("Today's Sales","At a Glance",$extra_gcode2);
		$gcode = create_cpgraph_code("sales2","Pie",$salesdata, 300, 270,$extra_gcode);
		echo $gcode['html'];
	}
	else
	{
		echo "&nbsp;<br><br><table cellpadding=0><tr><td><table cellspacing=0 cellpadding=4><tr><td width='80'>&nbsp;</td><td width='20' bgcolor='#ffffff'>&nbsp;</td><td align='left'><font style='color:#aaaaaa; font-size:18px'>&nbsp;</font></td></tr><tr><td colspan='3'>" . $gcode['html'] . "</td></tr></table></td></tr></table>";
	}
	//echo "				</td></tr>";
	//echo "			</table>";
	echo "		</td>";

	$enable_special_message = false;
	$one_week_ago = date("Y-m-d",mktime(0,0,0,date("m"),date("d")-7,date("Y")));
	$early_three_query = lavu_query("select * from devices where last_checkin > '[1]' and (poslavu_version LIKE '3.0.1%' or  poslavu_version LIKE '3.0.0%')",$one_week_ago);
	if(mysqli_num_rows($early_three_query))
	{
		$enable_special_message = true;
	}

	echo "		<td valign='top'";
	if($enable_special_message) echo " height='300'";
	echo ">" . $gcode2['html'] . "</td>";
	echo "		<td width='25'>&nbsp;</td>";
	echo "		<td valign='top' rowspan='3'>";



	if($enable_special_message)
	{
		$outmsg = "<style>.gridster li.msgli {font-size: 12px;margin-left: 10px;list-style-type:disc;}</style><font style='font-family:Din,Arial; font-size:14px; color:#888888'>We are pleased to announce that Lavu v3.0.3 is compatible with iOS 9. This version offers many advantages, including:<br><br><ul><li class='msgli'>Updated in version 2 reports to include discount reason.<li class='msgli'>Print refund receipts.<li class='msgli'>Improved check splitting and merging capabilities.<li class='msgli'>Edit seat and course numbers.</ul><br>In addition, v3.0.3 features many performance enhancements.<br>If you are currently using v3.0.0 or v3.0.1, we recommend upgrading to our latest v3.0.3 which can be found in the Apple App Store.</font>";
		echo "			<table width='400' cellpadding=6 cellspacing=0 style='border:solid 1px $header_color_messages'>";
		echo "				<tr><td width='100%' bgcolor='$header_color_messages' align='center' style='color:#ffffff; font-weight:bold; font-size:16px'>Upgrade to Lavu 3.0.3</td></tr>";
		echo "				<tr><td align='center'>";
		echo "<table cellpadding=8><tr><td>$outmsg</td></tr></table>";
		echo "				</td></tr>";
		echo "			</table>";
		//echo "<br><br>";
	}



	echo "			<table width='100%' cellpadding=6 cellspacing=0 style='border:solid 1px $header_color_messages'>";
	//echo "				<tr><td width='100%' bgcolor='$header_color_messages' align='center' style='color:#ffffff; font-weight:bold; font-size:16px'>Survey</td></tr>";
	//echo "				<tr><td>";
	//echo "					&nbsp;<br><br>";
	//echo "				</td></tr>";
	echo "				<tr><td width='100%' bgcolor='$header_color_messages' align='center' style='color:#ffffff; font-weight:bold; font-size:16px'>Messages<span id='displayContainer'></span></td></tr>";
	echo "				<tr><td align='center'>";
	//echo "					<table width='100%' bgcolor='#eeeeee'><tr><td width='100%' align='center'><br><br><br><br></td></tr></table>";
	echo "					<div style='width:350px; height:480px; overflow:hidden'>";
	if(1)//isset($_GET['showoption']))
	{
		if(trim($ds_str)!="")//isset($_GET['testhome']))
		{
			echo "						<table cellpadding=12 width='100%'><tr><td width='100%'><fieldset style='border-color:#aacc77; background-color:#f6f6f6; padding:16px;'><legend style='color:#547700; font-size:14px'>Survey</legend>$survey_str</fieldset></td></tr></table><br>";
		}
		/*echo "<img src='/cp/images/try-new-cp.png' border='0' onclick='window.location = \"/cp/?mode=settings_inventory_settings\"' style='cursor:pointer' /><a href='/cp/?mode=settings_inventory_settings'>
			<div class='messagesBtn saveBtn' href='/cp/?mode=settings_inventory_settings'>Try it Now</div>
		</a>";*/
	}
	else if(trim($ds_str)!="")
	{
		echo "						<table cellpadding=12 width='100%'><tr><td width='100%'><fieldset style='border-color:#aacc77; background-color:#f6f6f6; padding:16px;'><legend style='color:#547700; font-size:14px'>Survey</legend>$survey_str</fieldset></td></tr></table><br>";
	}

	echo output_home_page_messages(20);
	echo "					</div>";
	//echo "					<br><br><table width='100%' style='border-top:solid 1px $header_color_messages'><tr><td width='100%' align='center'>" . output_things_you_should_know() . "</td></tr></table>";
	echo "				</td></tr>";
	echo "			</table>";
	echo "		</td>";
	echo "	</tr>";
	echo "	<tr><td>&nbsp;</td></tr>";
	echo "	<tr>";
	echo "		<td colspan='2' valign='top'>";
	if ( !$isNewControlPanel ) {
		echo "			<table width='100%' cellpadding=6 cellspacing=0 style='border:solid 1px $header_color_shortcuts'>";
		echo "				<tr><td width='100%' bgcolor='$header_color_shortcuts' align='center' style='color:#ffffff; font-weight:bold; font-size:16px'>Shortcuts</td></tr>";

		echo "				<tr><td>" . output_shortcuts("horizontal") . "</td></tr>";
		echo "			</table>";
	}
	echo "		</td>";
	echo "	</tr>";
	echo "</table>";
	echo "<script type=\"text/javascript\" src=\"/cp/reports_v2/chart.js\"></script> ";
	echo "<script type=\"text/javascript\">";
	echo "	NumberFormatter.sharedFormatter( \"".$ms['comma_char']."\", \"".$ms['decimal_char']."\", ".$ms['decimal_places'].", \"".$ms['monitary_symbol']."\", \"".$ms['left_or_right']."\" );";
	echo $gcode['js'];
	echo $gcode2['js'];
	echo "</script>";
?>

<script type="text/javascript">
//Added 6/8 by EV Per LAVU-88
// counts the unread messages and outputs the result in the Messages header
function countUnread() {
	var message = "No New Messages";
	var messageCount = 0;
	if(messageCount>=1){
		message = "New Messages: " + messageCount;
	}
	document.getElementById('displayContainer').innerHTML = message;
}
countUnread();
// highlights any unread messages with the correct style found in styles.css
function highlightUnread() {
	var newClassArr = document.getElementsByClassName('change');
	while(newClassArr.length){
		newClassArr[0].className = "unreadMessage";
	}
	console.log("testing highlight unread");
}
// Finds unread messages and runs highlightUnread from above to apply the style to those unread messages
function showUnread() {
	var newMessage;
	if (newMessage == true) {
		highlightUnread();
	}
}
showUnread();



	var legend_element = document.getElementById('c_legend');
	var dataSets = chart_sales2._data.datasets;
	var dcounter = 0;
	for( var d in dataSets ){
		dcounter++;
		if(dcounter <= 4)
		{
			var entry = document.createElement('div');
			entry.style.display = 'inline-block';
			entry.style.margin = '0 0.4em';
			var dot = document.createElement('div');
			dot.style.borderRadius = '100%';
			dot.style.backgroundColor = dataSets[d].color.toString();
			dot.style.height = dot.style.width = '0.75em';
			dot.style.display = 'inline-block';
			dot.style.margin = '0.2em';
			dot.style.verticalAlign = 'text-bottom';

			entry.appendChild( dot );
			entry.appendChild( document.createTextNode(dataSets[d].label) );
			legend_element.appendChild( entry );
		}
	}

</script>
