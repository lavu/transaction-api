/*************************GLOBALS**************************************************/

var _start_x = 0; // mouse starting positions
var _start_y = 0;

var _hover_x = 0;
var _hover_y = 0;

var _debug = null; // makes life easier

var _tables = new Array();

var _selection_box_size = 6;

var _is_hovering = false;

var _is_resizing = false;

var _loc_x = 0; //these are mouse locations
var _loc_y = 0;

var _redraw_interval = 60;

var t = setInterval(draw_helper, _redraw_interval);

var selectionHandles = [];

var buffer_canvas = document.createElement('canvas');

/*************************END GLOBALS**********************************************/
window.onload = function() {
  // initialize all the events for the tables

  var tables = ['square', 'circle', 'diamond', 'left_slant', 'right_slant'];
  for (index in tables) {
    var elem = document.getElementById(tables[index]);
    elem.addEventListener('dragstart', drag_start, false);
    elem.addEventListener('dragend', drag_end, false);
    elem.addEventListener('dragleave', drag_leave, false);
  }

  document
    .getElementById('table_layout_room')
    .addEventListener('dragover', drag_over, false);
  _debug = document.getElementById('debug');
};
function allowDrop(event) {
  event.preventDefault();
}
function drag_start(event) {
  event.dataTransfer.setData('text/html', null); //cannot be empty string
  elem = document.getElementById(event.target.id);

  if (event.offsetX == undefined) {
    _start_x = event.layerX - $(event.target).position().left;
    _start_y = event.layerY - $(event.target).position().top;
  } else {
    _start_x = event.offsetX;
    _start_y = event.offsetY;
  }

  _debug.innerHTML = 'X: ' + _start_x + ' Y:' + _start_y;
}
function drag_over(event) {
  var cords = getOffset(event);
  var elem = document.querySelector('canvas[selected]');

  if (elem && elem.style.cursor != 'auto') _is_resizing = true;

  if (!_is_resizing) {
    if (cords.x - _start_x != _hover_x || cords.y - _start_y != _hover_y) {
      _hover_x = cords.x - _start_x;
      _hover_y = cords.y - _start_y;
      _debug.innerHTML = 'X: ' + cords.x + ' Y:' + cords.y;
    }
  } else {
    //this means we are moving in a direction;
    var mouse_loc = getOffset(event);
    perform_resize(elem, mouse_loc, event);
  }
}
function drag_leave(event) {}
function drag_end(event) {
  _debug.innerHTML = 'END';
  event.preventDefault();
  data = event.target.id;
  if (_is_resizing) {
  } else if (data) {
    // make sure to see if we have an id to start with

    var matches = data.match(/\d+/g);
    console.log(matches);
    if (matches != null) {
      // if we are moving a created table
      var table = document.getElementById(data);
      table.style.left = _hover_x;
      table.style.top = _hover_y;
      select(table);
      invalidate(table);
    } else {
      // if we are creating a table it wont have a number in  the id

      var copy = create_table(data);
      _tables.push(copy);
      invalidate(copy);
    }
  } else console.log('Failed to get Data');
}
function update_mouse_loc(e) {
  var l = getOffset(e);

  _loc_x = l.x;
  _loc_y = l.y;
  _debug.innerHTML = _loc_x + ' ' + _loc_y;
  check_mouse_loc(_loc_x, _loc_y);
}
function draw(can) {
  if (!buffer_canvas) buffer_canvas = document.getElementById('buffer_canvas');

  if (!can.hasAttribute('invalidated')) {
    return;
  }
  buffer_canvas.width = can.width;
  buffer_canvas.height = can.height;

  var ctx = buffer_canvas.getContext('2d');
  ctx.clearRect(0, 0, buffer_canvas.width, buffer_canvas.height);
  //console.log(document.getElementById(can.id.replace(/[0-9]/g, '') +"_img"));
  ctx.drawImage(
    document.getElementById(can.id.replace(/[0-9]/g, '') + '_img'),
    0,
    0,
    buffer_canvas.width,
    buffer_canvas.height
  );
  if (can.hasAttribute('selected')) {
    add_resize_handles(can);
  }
  var can_ctx = can.getContext('2d');
  can_ctx.putImageData(
    ctx.getImageData(0, 0, buffer_canvas.width, buffer_canvas.height),
    0,
    0
  );

  can.removeAttribute('invalidated');
}
/***********************HELPER FUNCTIONS ********************/
function draw_helper() {
  for (index in _tables) {
    draw(_tables[index]);
  }
}
function create_table(data) {
  var copy = document.getElementById(data).cloneNode(true);
  //_is_resizing	  = false;
  copy.className = data.toString() + ' added_table';
  copy.id = data + _tables.length.toString();
  copy.style.left = _hover_x;
  copy.style.top = _hover_y;
  copy.style.width = '70px';
  copy.style.height = '70px';
  copy.onclick = function() {
    select(copy);
  };
  copy.classList.remove(copy.id.replace(/[0-9]/g, ''));
  document.getElementById('table_layout_room').appendChild(copy);
  return copy;
}
function perform_resize(elem, mouse_loc, event) {
  var dragImgEl = null;
  /*
	if(!document.getElementById('invisible_drag')){
		dragImgEl = document.createElement('span');
	
		dragImgEl.setAttribute('id',"invisible_drag");
		dragImgEl.setAttribute('style','position: absolute; display: block; top: 0; left: 0; width: 0; height: 0;' );
		document.body.appendChild(dragImgEl);
		
		
	}else{
		dragImgEl= document.getElementById('invisible_drag');
	}
	event.dataTransfer.setDragImage(dragImgEl,0,0);
	*/
  _is_resizing = true;
  var resize_dir = get_cursor(elem.style.cursor);

  var oldx = parseInt(elem.style.left);
  var oldy = parseInt(elem.style.top);
  var mx = mouse_loc.x;
  var my = mouse_loc.y;

  // 0  1  2
  // 3     4
  // 5  6  7

  switch (resize_dir) {
    case 0:
      elem.style.left = mx;
      elem.style.top = my;
      elem.style.width += oldx - mx;
      elem.style.height += oldy - my;

      //console.log("0");
      console.log(mx + ' ' + my);
      break;
    case 1:
      elem.style.top = my;
      elem.style.height += oldy - my;
      //console.log("1");
      break;
    case 2:
      elem.style.top = my;
      elem.style.width = mx - oldx;
      elem.style.height += oldy - my;
      //console.log("2");
      break;
    case 3:
      elem.style.left = mx;
      elem.style.width += oldx - mx;
      //console.log("3");
      break;
    case 4:
      elem.style.width = mx - oldx;
      // elem.width= mx-oldx;
      //draw(elem);
      invalidate(elem);
      // elem.getContext('2d').drawImage(document.getElementById(elem.id.replace(/[0-9]/g, '') +"_img"),0,0,(mx-oldx),elem.height);
      //console.log("4");
      break;
    case 5:
      elem.style.left = mx;
      elem.style.width += oldx - mx;
      elem.style.height = my - oldy;
      //console.log("5");
      break;
    case 6:
      elem.style.height = my - oldy;
      elem.height = my - oldy;
      //invalidate(elem);
      //console.log("6");
      break;
    case 7:
      elem.style.width = mx - oldx;
      elem.style.height = my - oldy;
      //console.log("7");
      break;
  }
  //_is_resizing=false;
}
function check_mouse_loc(mx, my) {
  var elem = document.querySelector('canvas[selected]');
  if (elem) {
    var pixel_loc_x = parseInt(elem.style.left);
    var pixel_loc_y = parseInt(elem.style.top);

    for (var i = 0; i < 8; i++) {
      var cur = selectionHandles[i];
      var x = pixel_loc_x + cur.x;
      var y = pixel_loc_y + cur.y;

      if (
        mx >= x &&
        mx <= x + _selection_box_size &&
        my >= y &&
        my <= y + _selection_box_size
      ) {
        elem.style.cursor = get_cursor(i);
        return;
      }
    }
    elem.style.cursor = 'auto';
  }
}
function get_cursor(index) {
  var cursor_dirs = [
    'nw-resize',
    'n-resize',
    'ne-resize',
    'w-resize',
    'e-resize',
    'sw-resize',
    's-resize',
    'se-resize',
  ];
  if (isNumber(index)) {
    if (index == 4) return 'e-resize';
    else if (index == 6) return 's-resize';
    else if (index == 7) return 'se-resize';
    else return 'auto';
    return cursor_dirs[index];
  } else {
    return cursor_dirs.indexOf(index);
  }
}
function add_resize_handles(canvas) {
  var half = _selection_box_size / 2;

  // 0  1  2
  // 3     4
  // 5  6  7

  var width = parseInt(canvas.width);
  var height = parseInt(canvas.height);

  selectionHandles[0] = { x: 0, y: 0 }; //top left
  selectionHandles[1] = { x: width / 2 - half, y: 0 }; //top middle
  selectionHandles[2] = { x: width - _selection_box_size, y: 0 }; //top right
  selectionHandles[3] = { x: 0, y: height / 2 - half }; //middle left
  selectionHandles[4] = {
    x: width - _selection_box_size,
    y: height / 2 - half,
  }; //middle right
  selectionHandles[5] = { x: 0, y: height - _selection_box_size }; //bottom left
  selectionHandles[6] = {
    x: width / 2 - half,
    y: height - _selection_box_size,
  }; //bottom middle
  selectionHandles[7] = {
    x: width - _selection_box_size,
    y: height - _selection_box_size,
  }; //bottom right

  ctx = buffer_canvas.getContext('2d');
  ctx.fillStyle = '#ffffff';

  for (var i = 0; i < 8; i++) {
    if (i == 4 || i == 6 || i == 7) {
      var cur = selectionHandles[i];
      ctx.fillRect(cur.x, cur.y, _selection_box_size, _selection_box_size);
    }
  }
  canvas.addEventListener('mouseup', end_drag, false);
  canvas.addEventListener('mousemove', update_mouse_loc, false);
}
function end_drag() {
  console.log('up');
  var can = document.querySelector('canvas[selected]');
  if (can) invalidate(can);
}
function invalidate(elem) {
  elem.setAttribute('invalidated', '');
}
function select(elem) {
  unselect(null);
  elem.setAttribute('selected', '');
  invalidate(elem);
}
function unselect(elem) {
  if (elem) {
    elem.removeAttribute('selected');
    invalidate(elem);
  } else {
    var selected = document.querySelectorAll('canvas[selected]');
    for (var i = 0; i < selected.length; i++) {
      unselect(selected[i]);
    }
  }
}
function getOffset(evt) {
  var el = document.getElementById('table_layout_room').getBoundingClientRect();
  var x = evt.clientX - el.left;
  var y = evt.clientY - el.top;

  return { x: x, y: y };
}
function isNumber(n) {
  return !isNaN(parseFloat(n)) && isFinite(n);
}

/*
function canvas_hover(event){
	console.log('hover in');
	_is_hovering=true;
	start_hover_watching();
	
}
function canvas_mouseout(event){
	console.log('hover out');
	_is_hovering= false;
}
function start_hover_watching(){
	setTimeout(function(){
		if(_is_hovering){
			
		}
	}, 100);
}*/

/*
function unselect(){
	if(document.getElementsByClassName("selected_table").length){
		var can= document.getElementsByClassName("selected_table")[0];
		can.getContext("2d").clearRect(0, 0, can.width, can.height);
		can.getContext('2d').drawImage(document.getElementById(can.id.replace(/[0-9]/g, '') +"_img"),0,0,can.width,can.height);
		//can.removeEventListener('mousemove',arguments.callee,false);
		removeClass(can,'selected_table');
	}
}
function hasClass(ele,cls) {
    return ele.className.match(new RegExp('(\\s|^)'+cls+'(\\s|$)'));
}
function removeClass(ele,cls) {
	if (hasClass(ele,cls)) {
	    var reg = new RegExp('(\\s|^)'+cls+'(\\s|$)');
	    ele.className=ele.className.replace(reg,' ');
	}
}
function select_table(elem){

		if(document.querySelector('[selected]')){
		unselect();
	}
	
	if(elem.toElement != undefined){
		
		elem.toElement.className+=' selected_table';
		add_resize_handles(elem.toElement);
	}else{
		//console.log(elem);	
		elem.className+=' selected_table';
		add_resize_handles(elem);
	}

}
*/
