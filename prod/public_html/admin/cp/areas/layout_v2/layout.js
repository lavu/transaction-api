/*************************GLOBALS**************************************************/

var _start_x = 0; // mouse starting positions
var _start_y = 0;

var _hover_x = 0;
var _hover_y = 0;

var _debug = true; // makes life easier

var _tables = new Array();

var _room_ct = 0;

var _selection_box_size = 6;

var _is_dragging = false;

var _is_resizing = false;

var _expect_resize = -1;

var _loc_x = 0; //these are mouse locations
var _loc_y = 0;

var _redraw_interval = 20;

var _invalid = false;

var ROOM_WIDTH = 969;
var ROOM_HEIGHT = 1000;

var ongoingTouches = new Array();

var t = setInterval(draw_helper, _redraw_interval);
var isiPad = false;

if (
  /Android|webOS|iPhone|iPad|iPod|iemobile|BlackBerry/i.test(
    navigator.userAgent
  )
) {
  isiPad = true;
}

var use_snap = true;

//var isiPad = navigator.userAgent.match(/iPad/i) != null;

var mylatesttap;

var _is_double_tap = false;

if (isiPad) _selection_box_size = 15;

var selectionHandles = [];
var buffer_canvas = document.createElement('canvas');
var bctx = buffer_canvas.getContext('2d');
var main_can = null;
var table_obj = {
  //This is the json representation of a table
  id: '',
  table_type: '',
  table_img: '',
  x: '0',
  y: '0',
  width: '70',
  height: '70',
  valid: false,
  selected: false,
  name: 'table',
  rev_center: '',
};

/*************************END GLOBALS*********************************/
window.onload = function() {
  // initialize all the events for the tables
  var temp_square = new Image();
  temp_square.src = 'images/table_square.png';

  var temp_circle = new Image();
  temp_circle.src = 'images/table_circle.png';

  var temp_diamond = new Image();
  temp_diamond.src = 'images/table_diamond.png';

  var temp_slant_left = new Image();
  temp_slant_left.src = 'images/table_slant_left.png';

  var temp_slant_right = new Image();
  temp_slant_right.src = 'images/table_slant_right.png';

  /*this section adds the drag ability for the tables*/

  //var tables=['square','circle','diamond'];
  var tables = ['square', 'circle', 'diamond', 'left_slant', 'right_slant'];
  for (index in tables) {
    var elem = document.getElementById(tables[index]);
    elem.addEventListener('dragstart', drag_start, false);
    elem.addEventListener('dragend', drag_end, false);
    elem.addEventListener('dragleave', drag_leave, false);
    elem.addEventListener('touchstart', drag_start, false);
    elem.addEventListener('touchend', drag_end, false);
  }

  /*This adds all the listeners for the main canvas*/
  main_can = document.getElementById('table_layout_room');
  main_can.addEventListener('dragover', drag_over, false);
  main_can.addEventListener('dragenter', drag_enter, false);
  if (navigator.userAgent.toLowerCase().indexOf('firefox') > -1)
    main_can.addEventListener('drop', drop, false);

  main_can.onselectstart = function() {
    return false;
  }; // dont let people select the canvas
  main_can.onmousedown = mouse_down;
  main_can.onmouseup = mouse_up;
  main_can.ondblclick = double_click;
  main_can.onmousemove = mouse_move;
  main_can.oncontextmenu = right_click;

  document.addEventListener('keydown', button_listener, true);

  /* FOR IPAD */
  main_can.addEventListener('touchstart', touch_start, false);
  main_can.addEventListener('touchend', touch_end, false);
  main_can.addEventListener('touchmove', touch_move, false);

  /*This is our buffer canvas where we can see where shapes are and such*/
  buffer_canvas.width = ROOM_WIDTH;
  buffer_canvas.height = ROOM_HEIGHT;
  buffer_canvas.style.position = 'absolute';
  buffer_canvas.style.zIndex = -100;
  buffer_canvas.style.left = $(main_can).offset().left;
  buffer_canvas.style.top = $(main_can).offset().top;
  _debug = document.getElementById('debug'); // for debugging, its the x loc and y loc
  draw_background();
  var room_data = get_rooms();
  var rev_center_data = get_rev_centers();
  setTimeout(function() {
    invalidate();
  }, 250);
};
function button_listener(e) {
  if (e.target.localName == 'body') {
    //console.log('in here');2
    var key = e.keyCode;
    if (key == 8 || key == 46) {
      delete_table();
      e.preventDefault();
      return false;
    }
  }
}

function get_rooms() {
  var room_data = performAJAX(
    './areas/layout_v2/update_db.php',
    'function=get_rooms',
    false,
    'POST'
  );
  //var decoded		  = decodeURIComponent(room_data);
  var decoded = room_data;

  if (!check_json(room_data)) {
    new_room();
    return;
  }

  var obj = JSON.parse(room_data);
  if (obj[0].id == '-1') {
    new_room();
    return;
  }

  var room_selector = document.getElementById('room_select');

  for (var i = 0; i < obj.length; i++) {
    var option = document.createElement('option');
    if (i == 0) {
      option.selected = true;
      _tables = JSON.parse(obj[i].tables);
      for (var c = 0; c < _tables.length; c++) {
        if (_tables[c].table_type == 'slant_left')
          document.getElementById('left_slant').style.display = 'block';
        if (_tables[c].table_type == 'slant_right')
          document.getElementById('right_slant').style.display = 'block';

        var imageObj = new Image();
        if (!_tables[c].table_img)
          _tables[c].table_img = get_img_from_name(_tables[c].table_type);
        if (_tables[c].table_img.indexOf('http')) {
          var filename = _tables[c].table_img.replace(/^.*[\\\/]/, '');
          _tables[c].table_img = 'images/' + filename;
        }

        imageObj.src = _tables[c].table_img;
        _tables[c].table_img = imageObj;
        var id_num = _tables[c].id.replace(/\D/g, '');
        if (
          !isNaN(parseFloat(id_num)) &&
          isFinite(id_num) &&
          id_num * 1 > _room_ct * 1
        )
          _room_ct = id_num;
      }
      document.getElementById('room_tit').value = obj[i].title;
    } else {
      var temp = JSON.parse(obj[i].tables);
      for (var c = 0; c < temp.length; c++) {
        if (temp[c]) {
          var id_num = temp[c].id.replace(/\D/g, '');

          if (id_num && id_num * 1 > _room_ct * 1) _room_ct = id_num;
        } else {
          //console.log(temp[c]);
        }
      }
    }
    option.value = obj[i].id;
    option.text = obj[i].title;
    try {
      // for IE earlier than version 8
      room_selector.add(option, room_selector.options[null]);
    } catch (e) {
      room_selector.add(option, null);
    }
  }

  invalidate();
}
function get_rev_centers() {
  var res = performAJAX(
    './areas/layout_v2/update_db.php',
    'function=get_revenue_centers',
    false,
    'POST'
  );
  var result = JSON.parse(res);
  //console.log("res is"+res);
  if (result.length) {
    var rev_center_selector = document.getElementById('rev_center');
    for (var i = 0; i < result.length; i++) {
      var option = document.createElement('option');
      option.text = result[i].name;
      option.value = result[i].id;
      try {
        rev_center_selector.add(option, rev_center_selector.options[null]); // for IE earlier than version 8
      } catch (e) {
        rev_center_selector.add(option, null);
      }
    }
  } else {
    document.getElementById('rev_center_container').style.display = 'none';
  }
}
/***********************TABLE CREATION SECTION************************/
//This section is initialization. This only happens when we are first creating a table.
function allowDrop(event) {
  event.preventDefault();
}
function drop(e) {
  e.preventDefault();
}
function drag_start(event) {
  //console.log("The id is:" + event.target.id);
  //console.log(event.dataTransfer);
  if (event.dataTransfer) event.dataTransfer.setData('text', event.target.id); //cannot be empty string

  elem = document.getElementById(event.target.id);

  if (event.offsetX == undefined) {
    _start_x = event.layerX - $(event.target).position().left;
    _start_y = event.layerY - $(event.target).position().top;
  } else {
    _start_x = event.offsetX;
    _start_y = event.offsetY;
  }

  _debug.innerHTML = 'X: ' + _start_x + ' Y:' + _start_y;
}
function drag_enter(event) {
  event.preventDefault();
}
function drag_over(event) {
  event.preventDefault();
  var cords = getOffset(event);
  var elem = document.querySelector('canvas[selected]');

  if (elem && elem.style.cursor != 'auto') _is_resizing = true;

  if (!_is_resizing) {
    if (cords.x - _start_x != _hover_x || cords.y - _start_y != _hover_y) {
      if (navigator.userAgent.toLowerCase().indexOf('firefox') > -1) {
        _hover_x = cords.x - 35;
        _hover_y = cords.y - 35;
      } else {
        _hover_x = cords.x - _start_x;
        _hover_y = cords.y - _start_y;
      }
      _debug.innerHTML = 'X: ' + cords.x + ' Y:' + cords.y;
    }
  } else {
    //this means we are moving in a direction;
    var mouse_loc = getOffset(event);
    perform_resize(elem, mouse_loc, event);
  }
}
function drag_leave(event) {}
function drag_end(event) {
  //console.log("drag_end finished");
  _debug.innerHTML = '';
  event.preventDefault();
  data = event.target.id;
  if (data) {
    // make sure to see if we have an id to start with
    var copy = create_table(data);
    _tables.push(copy);
    invalidate();
  } else {
    //console.log("Failed to get Data");
    lavuLog('Failed to get Data');
  }
}

/***********************END TABLE CREATION SECTION********************/

/***********************MOUSE HANDLERS********************************/
function mouse_move(e) {
  e.preventDefault();
  move_handler(e);
}
function mouse_down(e) {
  e.preventDefault();

  $('#room_tit').blur();
  $('#table_name').blur();
  press_handler(e);
}
function mouse_up() {
  _is_dragging = false;
  _is_resizing = false;
  expectResize = -1;
}
function double_click(e) {
  // renames a table

  var rename_box = document.getElementById('rename_table');
  var input_box = document.getElementById('table_name');
  var elem = get_selected_elem();
  if (elem) {
    rename_box.style.left = elem.x + (parseInt(elem.width) / 2 - 60) + 'px';

    rename_box.style.top = elem.y + 'px';
    rename_box.style.display = 'block';

    input_box.value = elem.name;
    input_box.focus();
  }
}
function right_click(e) {
  //Rename a table
  /*
	var elem= get_selected_elem();

	document.getElementById("popup_box").style.display='block';
	document.getElementById("popup_box").style.top    =elem.y;
	document.getElementById("popup_box").style.left   =elem.x;
	document.getElementById("table_name").value 	  =elem.name;
	document.getElementById("x_loc").value			  =elem.x;
	document.getElementById("y_loc").value			  =elem.y;
	document.getElementById("width").value			  =elem.width;
	document.getElementById("height").value			  =elem.height;
	document.getElementById("table_type").value		  =elem.table_type;
	document.getElementById('table_name').focus();
	mouse_up();
	return false;*/
}
/**********************END MOUSE HANDLERS****************************/

/***********************TOUCH HANDLERS*******************************/
function touch_start(e) {
  e.preventDefault();
  doubletap();
  if (_is_double_tap) {
    double_click(null);
    return;
  }
  press_handler(e.changedTouches[0]); // this function is used for mouse down also
}
function touch_move(e) {
  e.preventDefault();
  move_handler(e.changedTouches[0]);
}
function touch_end(e) {
  //_debug.innerText='Touch End';
  _is_dragging = false;
  _is_resizing = false;
  expectResize = -1;
}
function doubletap() {
  var now = new Date().getTime();
  var timesince = now - mylatesttap;
  if (timesince < 600 && timesince > 0) _is_double_tap = true;
  else _is_double_tap = false;

  mylatesttap = new Date().getTime();
}
/**********************END TOUCH HANDLERS***************************/

/**********************BUTTON HANDLERS******************************/

function clone_table() {
  var elem = get_selected_elem();
  var def = create_table(elem.table_type);

  def.x = elem.x + 20;
  def.y = elem.y;
  def.width = elem.width;
  def.height = elem.height;
  def.rev_center = elem.rev_center;
  _tables.push(def);
  select(def);
  invalidate();
}
function change_rev_center(rev) {
  var selected_elem = get_selected_elem();
  if (selected_elem) {
    //console.log(rev);
    if (rev == '-1') open_rev_center_creator();
    else if (rev != 'Rev Center') selected_elem.rev_center = rev;
    else selected_elem.rev_center = '';

    invalidate();
  } else alert('Please select a table to apply a revenue center to.');
}
function new_room() {
  var val = performAJAX(
    './areas/layout_v2/update_db.php',
    'function=new_room',
    false,
    'POST'
  );
  var option = document.createElement('option');
  var room_selector = document.getElementById('room_select');
  var json = JSON.parse(decodeURIComponent(val));

  _tables = new Array();
  //console.log(json);
  option.selected = true;
  option.value = json.id;
  option.text = 'New Room';
  document.getElementById('room_tit').value = 'New Room';
  try {
    // for IE earlier than version 8
    room_selector.add(option, room_selector.options[null]);
  } catch (e) {
    room_selector.add(option, null);
  }
  invalidate();
}
function change_room(room_id) {
  var room_id = document.getElementById('room_select').value;
  if (room_id) load_room(room_id);
}
function delete_room() {
  if (confirm('Are you sure you want to delete this room')) {
    var room_id = document.getElementById('room_select').value;
    performAJAX(
      './areas/layout_v2/update_db.php',
      'function=delete_room&room_id=' + room_id,
      false,
      'POST'
    );
    window.location.reload();
  }
}
function save_room() {
  var room_id = document.getElementById('room_select').value;
  var tables_temp = _tables;
  var title = document.getElementById('room_tit').value;
  var e = document.getElementById('room_select');

  e.options[e.selectedIndex].text = title;
  for (var i = 0; i < tables_temp.length; i++) {
    if (tables_temp[i].table_img.src.indexOf('http') != -1) {
      var filename = tables_temp[i].table_img.src.replace(/^.*[\\\/]/, '');
      tables_temp[i].table_img = 'images/' + filename;
    } else {
      tables_temp[i].table_img = tables_temp[i].table_img.src;
    }

    if (tables_temp[i].width < 0) {
      tables_temp[i].x += tables_temp[i].width;
      tables_temp[i].width *= -1;
    }
    if (tables_temp[i].height < 0) {
      tables_temp[i].y += tables_temp[i].height;
      tables_temp[i].height *= -1;
    }

    tables_temp[i].x = tables_temp[i].x.toString();
    tables_temp[i].y = tables_temp[i].y.toString();
    tables_temp[i].width = tables_temp[i].width.toString();
    tables_temp[i].height = tables_temp[i].height.toString();
  }
  performAJAX(
    './areas/layout_v2/update_db.php',
    'function=save_room&room_id=' +
      room_id +
      '&tables=' +
      encodeURIComponent(JSON.stringify(tables_temp)) +
      '&title=' +
      title,
    false,
    'POST'
  );
  for (var c = 0; c < _tables.length; c++) {
    var imageObj = new Image();

    if (_tables[c].table_img.indexOf('http') != -1) {
      var filename = _tables[c].table_img.replace(/^.*[\\\/]/, '');
      _tables[c].table_img = 'images/' + filename;
    }
    imageObj.src = _tables[c].table_img;
    _tables[c].table_img = imageObj;
  }
}
function load_room() {
  var room_id = document.getElementById('room_select').value;
  var obj = decodeURIComponent(
    performAJAX(
      './areas/layout_v2/update_db.php',
      'function=load_room&room_id=' + room_id,
      false,
      'POST'
    )
  );
  var parsed = JSON.parse(obj);
  var t = parsed.tables;

  for (var c = 0; c < t.length; c++) {
    var imageObj = new Image();
    if (t[c].table_img.indexOf('http') != -1) {
      var filename = t[c].table_img.replace(/^.*[\\\/]/, '');
      t[c].table_img = 'images/' + filename;
    }
    imageObj.src = t[c].table_img;
    t[c].table_img = imageObj;
    if (
      t[c].x < 0 ||
      t[c].y < 0 ||
      t[c].x > ROOM_WIDTH ||
      t[c].y > ROOM_HEIGHT
    ) {
      t[c].x = 0;
      t[c].y = 0;
    }
    if (t[c].table_type == 'slant_left')
      document.getElementById('left_slant').style.display = 'block';
    if (t[c].table_type == 'slant_right')
      document.getElementById('right_slant').style.display = 'block';
  }
  document.getElementById('room_tit').value = parsed.title;
  _tables = t;
  invalidate();
}
function delete_table() {
  var elem = get_selected_elem();
  for (var i = 0; i < _tables.length; i++) {
    if (_tables[i].id == elem.id) _tables.splice(i, 1);
  }
  invalidate();
}
/**********************END BUTTON HANDLERS**************************/

/***********************HELPER FUNCTIONS ********************/

function move_handler(e) {
  if (_is_dragging) {
    document.getElementById('rename_table').style.display = 'none';
    var loc = getOffset(e);

    var elem = get_selected_elem();

    mx = loc.x - elem.width / 2;
    my = loc.y - elem.height / 2;

    elem.x = mx;
    elem.y = my;
    _debug.innerHTML = 'X: ' + mx + ' Y:' + my;
    // something is changing position so we better invalidate the canvas!
    invalidate();
  } else if (_is_resizing) {
    document.getElementById('rename_table').style.display = 'none';
    var loc = getOffset(e);
    var elem = get_selected_elem();
    var resize_dir = _expect_resize;
    var oldx = parseInt(elem.x);
    var oldy = parseInt(elem.y);
    var mx = parseInt(loc.x);
    var my = parseInt(loc.y);
    elem.width = parseInt(elem.width);
    elem.height = parseInt(elem.height);
    // 0  1  2
    // 3     4
    // 5  6  7

    var no_resize = false;
    if (resize_dir == 0) {
      elem.width += oldx - mx;
      elem.height += oldy - my;
      elem.x = mx;
      elem.y = my;
    } else if (resize_dir == 1) {
      elem.height += oldy - my;
      elem.y = my;
    } else if (resize_dir == 2) {
      elem.y = my;
      elem.width = mx - oldx;
      elem.height += oldy - my;
    } else if (resize_dir == 3) {
      elem.x = mx;
      elem.width += oldx - mx;
    } else if (resize_dir == 4) {
      elem.width = mx - oldx;
    } else if (resize_dir == 5) {
      elem.x = mx;
      elem.width += oldx - mx;
      elem.height = my - oldy;
    } else if (resize_dir == 6) {
      elem.height = my - oldy;
    } else if (resize_dir == 7) {
      elem.width = mx - oldx;
      elem.height = my - oldy;
      if (elem.width < 59) elem.width = 60;
      if (elem.height < 59) elem.height = 60;
    }
    invalidate();
  }

  var loc = getOffset(e);
  var elem = get_selected_elem();
  if (elem !== null && !_is_resizing) {
    check_mouse_loc(loc.x, loc.y);
  }
}
function press_handler(evt) {
  var loc = getOffset(evt);
  var mx = loc.x;
  var my = loc.y;
  //we are over a selection box
  check_mouse_loc(loc.x, loc.y);

  if (_expect_resize !== -1) {
    invalidate();
    _is_resizing = true;
    return;
  }
  if (_tables.length) {
    for (var i = _tables.length - 1; i >= 0; i--) {
      draw(_tables[i], true);
      // get image data at the mouse x,y pixel
      var imageData = bctx.getImageData(mx, my, 1, 1);
      //console.log("mx: "+ mx+ " my: "+my);
      var index = (mx + my * imageData.width) * 4;
      // if the mouse pixel exists, select and break
      if (imageData.data[3] > 0) {
        select(_tables[i]);
        invalidate();
        clear(buffer_canvas);
        _is_dragging = true;
        return;
      }
    }
  }
  clear(buffer_canvas); // clear the ghost canvas for next time

  if (document.getElementById('rename_table').style.display == 'block')
    save_name(document.getElementById('table_name').value);

  unselect(); // because we havent selected anything;
  invalidate(); // invalidate because we might need the selection border to disappear
  return;
}
function get_selected_elem() {
  if (_tables.length) {
    for (var i = 0; i < _tables.length; i++) {
      if (_tables[i].selected) return _tables[i];
    }
    return null;
  } else return null;
}
function draw(can, use_ghost, color) {
  if (use_ghost) {
    //can here is the ghost canvas and elem is the canvas and color is whatever color to use.
    if (!color) color = 'black';
    bctx.fillStyle = color;
    can.x = parseInt(can.x);
    can.y = parseInt(can.y);
    can.width = parseInt(can.width);
    can.height = parseInt(can.height);

    bctx.fillRect(can.x, can.y, can.width, can.height);
  } else {
    //can here is an individual table
    can.x = parseInt(can.x);
    can.y = parseInt(can.y);
    //if( can.x < 0 || can.y < 0 || can.x > ROOM_WIDTH ||can.y > ROOM_HEIGHT){
    //	can.x=0;
    //	can.y=0;

    //}
    can.width = parseInt(can.width);
    can.height = parseInt(can.height);

    var context = main_can.getContext('2d');
    if (can.rev_center) {
      context.fillStyle = revenue_center_color(can.rev_center);
    } else context.fillStyle = '#000000';

    context.font = '15px times';

    var text_width = get_width_of_text(can.name);
    var x = can.x;
    var y = can.y;
    var w = can.width;
    var h = can.height;
    if (can.width < 0) {
      x += can.width;
      w *= -1;
    }
    if (can.height < 0) {
      y += can.height;
      h *= -1;
    }
    try {
      if (!can.table_img) can.table_img = get_img_from_name(can.table_type);

      context.drawImage(
        can.table_img,
        parseInt(x),
        parseInt(y),
        parseInt(w),
        parseInt(h)
      );
      context.fillText(
        can.name,
        can.x + can.width / 2 - text_width / 2,
        can.y + 15
      );
    } catch (err) {
      //console.log(can.table_img +" "+ x +" "+ y +" "+ w +" "+ h);
      //console.log(err);
    }
    var rev_centers = document.getElementById('rev_center').children;
    for (var i = 0; i < rev_centers.length; i++) {
      if (rev_centers[i].value == can.rev_center) {
        var width = get_width_of_text(rev_centers[i].innerHTML);
        context.fillText(
          rev_centers[i].innerHTML,
          can.x + can.width / 2 - width / 2,
          can.y + parseInt(can.height) - 20
        );
      }
    }

    if (can.selected && can.selected != 'false') draw_handles(can);
  }
}
function clear(canvas) {
  if (canvas) {
    var context = canvas.getContext('2d');
    context.clearRect(0, 0, canvas.width, canvas.height);
  } else {
    var context = main_can.getContext('2d');
    context.clearRect(0, 0, main_can.width, main_can.height);
    draw_background();
  }
}
function draw_background() {
  var ctx = main_can.getContext('2d');
  var img = document.getElementById('canvas_background_img');
  var pat = ctx.createPattern(img, 'repeat');
  ctx.width = ROOM_WIDTH;
  ctx.height = ROOM_HEIGHT;
  ctx.rect(0, 0, ctx.width, ctx.height);
  ctx.fillStyle = pat;
  ctx.fill();
}
function draw_helper() {
  if (_invalid) {
    clear();
    for (var i = 0; i < _tables.length; i++) {
      draw(_tables[i]);
    }
    _invalid = false;
  }
}
function draw_handles(table) {
  if (table) {
    var width = table.width;
    var height = table.height;
    var half = _selection_box_size / 2;

    selectionHandles[0] = { x: 0, y: 0 }; //top left
    selectionHandles[1] = { x: width / 2 - half, y: 0 }; //top middle
    selectionHandles[2] = { x: width - _selection_box_size, y: 0 }; //top right
    selectionHandles[3] = { x: 0, y: height / 2 - half }; //middle left
    selectionHandles[4] = {
      x: width - _selection_box_size,
      y: height / 2 - half,
    }; //middle right
    selectionHandles[5] = { x: 0, y: height - _selection_box_size }; //bottom left
    selectionHandles[6] = {
      x: width / 2 - half,
      y: height - _selection_box_size,
    }; //bottom middle
    selectionHandles[7] = {
      x: width - _selection_box_size,
      y: height - _selection_box_size,
    }; //bottom right
    ctx = main_can.getContext('2d');
    ctx.fillStyle = '#000000';

    for (var i = 0; i < 8; i++) {
      var cur = selectionHandles[i];
      ctx.fillRect(
        cur.x + table.x,
        cur.y + table.y,
        _selection_box_size,
        _selection_box_size
      );
    }
  }
}
function create_table(data) {
  unselect();
  var new_table = cloneObject(table_obj);

  var name_with_paren = document.defaultView
    .getComputedStyle(document.getElementById(data), null)
    .backgroundImage.match(/\(([^\)]+)\)/)[0];
  var table_src = name_with_paren.substring(1, name_with_paren.length - 1);
  table_src = table_src.replace(/"/g, '');

  new_table.id = data + ++_room_ct;

  new_table.x = _hover_x;
  new_table.y = _hover_y;
  new_table.table_type = data;
  new_table.selected = true;
  new_table.name = data + _room_ct;

  var imageObj = new Image();
  if (table_src.indexOf('http') != -1) {
    var filename = table_src.replace(/^.*[\\\/]/, '');
    table_src = 'images/' + filename;
  }
  imageObj.src = table_src;
  imageObj.onload = function() {
    new_table.table_img = imageObj;
  };
  return new_table;
}

function check_mouse_loc(mx, my) {
  var elem = get_selected_elem();
  if (!selectionHandles.length) {
    draw_handles(elem);
  }
  if (elem) {
    var pixel_loc_x = elem.x;
    var pixel_loc_y = elem.y;

    for (var i = 0; i < 8; i++) {
      var cur = selectionHandles[i];
      var x = pixel_loc_x + cur.x;
      var y = pixel_loc_y + cur.y;
      //console.log("x: "+x+" y: "+y+" mx: "+mx+" my: "+my);
      if (
        mx >= x &&
        mx <= x + _selection_box_size &&
        my >= y &&
        my <= y + _selection_box_size
      ) {
        document.getElementById('table_layout_room').style.cursor = get_cursor(
          i
        );

        _expect_resize = i;
        return true;
      } else {
        //console.log('moving');
      }
    }
    _expect_resize = -1;
    document.getElementById('table_layout_room').style.cursor = 'auto';
    return false;
  } else {
    _expect_resize = -1;
    return false;
  }
}
function get_cursor(index) {
  var cursor_dirs = [
    'nw-resize',
    'n-resize',
    'ne-resize',
    'w-resize',
    'e-resize',
    'sw-resize',
    's-resize',
    'se-resize',
  ];
  if (isNumber(index)) {
    return cursor_dirs[index];
  } else {
    return cursor_dirs.indexOf(index);
  }
}
function invalidate() {
  _invalid = true;
}
function select(elem) {
  unselect();
  for (var i = 0; i < _tables.length; i++) {
    if (_tables[i].id == elem.id) _tables.splice(i, 1);
  }
  document.getElementById('rev_center').value = elem.rev_center;
  _tables.push(elem);
  elem.selected = true;
  invalidate();
}
function unselect() {
  for (var i = 0; i < _tables.length; i++) _tables[i].selected = false;

  document.getElementById('rename_table').style.display = 'none';
}
function getOffset(evt) {
  var el = document.getElementById('table_layout_room').getBoundingClientRect();
  var x = evt.clientX - el.left;
  var y = evt.clientY - el.top;

  return { x: x, y: y };
}
function isNumber(n) {
  return !isNaN(parseFloat(n)) && isFinite(n);
}
function cloneObject(obj) {
  if (obj === null || typeof obj !== 'object') {
    return obj;
  }

  var temp = obj.constructor(); // give temp the original obj's constructor
  for (var key in obj) {
    temp[key] = cloneObject(obj[key]);
  }

  return temp;
}
function save_name(tableName) {
  if (tableName == '') {
    alert('You may not save a table with a blank name.');
  } else if (!check_duplicate_names(tableName) && tableName != get_selected_elem().name) {
    alert('Duplicate name detected.');
  } else if (!checkDuplicateTables(tableName)) {
    alert('Table name already used in another room.');
  } else {
    get_selected_elem().name = tableName;
    document.getElementById('rename_table').style.display = 'none';
    invalidate();
  }
}
/**
 * To check duplicate table name
 *
 * @param {string} tableName - name of table
 *
 * @return {boolean}
 */
function checkDuplicateTables(tableName) {
  var tableData = performAJAX(
    "./areas/layout_v2/update_db.php",
    "function=getTables",
    false,
    "POST"
  );
  var objTable = JSON.parse(decodeURIComponent(tableData));
  if (objTable.status == 'success') {
     isTableExist = objTable.tables.includes(tableName);
     if (isTableExist == true) {
        return false;
     }
  }
  return true;
}
function check_duplicate_names(tableName) {
  var room_data = performAJAX(
    './areas/layout_v2/update_db.php',
    'function=get_rooms',
    false,
    'POST'
  );
  var obj = JSON.parse(decodeURIComponent(room_data));
  for (var i = 0; i < obj.length; i++) {
    for (var c = 0; c < _tables.length; c++) {
      if (_tables[c].name == tableName) return false;
    }
  }
  return true;
}
function ongoingTouchIndexById(idToFind) {
  for (var i = 0; i < ongoingTouches.length; i++) {
    var id = ongoingTouches[i].identifier;

    if (id == idToFind) {
      return i;
    }
  }
  return -1; // not found
}
function check_json(text) {
  if (
    /^[\],:{}\s]*$/.test(
      text
        .replace(/\\["\\\/bfnrtu]/g, '@')
        .replace(
          /"[^"\\\n\r]*"|true|false|null|-?\d+(?:\.\d*)?(?:[eE][+\-]?\d+)?/g,
          ']'
        )
        .replace(/(?:^|:|,)(?:\s*\[)+/g, '')
    )
  ) {
    return true;
  } else {
    return false;
  }
}
function revenue_center_color(id) {
  var r = (id * 73 + 0) % 256;
  var g = (id * 100 + 0) % 256;
  var b = (id * 53 + 200) % 256;

  return 'rgb(' + r + ',' + g + ',' + b + ')';
}
function get_width_of_text(text) {
  var text_div = document.createElement('span');
  text_div.style.fontFamily = 'times';
  text_div.style.fontSize = 'normal';
  text_div.innerHTML = text;
  text_div.style.color = 'white';
  document.body.appendChild(text_div);

  //var text_width= text_div.offsetWidth;
  var text_width = $(text_div).innerWidth();
  document.body.removeChild(text_div);

  return text_width + 5;
}
function get_img_from_name(name) {
  return 'images/table_' + name + '.png';
}
function open_rev_center_creator() {
  document.getElementById('rev_center_overlay_container').style.display =
    'block';
}
function close_container() {
  document
    .getElementById('rev_center')
    .children[0].setAttribute('selected', '1');
  document.getElementById('rev_center_overlay_container').style.display =
    'none';
}
function performAJAX(URL, urlString, asyncSetting, type) {
  var returnVal = '';
  document.getElementById('loading_container').style.display = 'block';
  //console.log('displaying');
  $.ajax({
    url: URL,
    type: type,
    data: urlString,
    async: asyncSetting,
    success: function(string) {
      returnVal = decodeURIComponent(string);
      if (returnVal.indexOf('Error: ') != -1) {
        alert(returnVal);
        window.location = 'https://admin.poslavu.com/cp';
      }
      document.getElementById('loading_container').style.display = 'none';
      //console.log('hiding');
    },
  });
  return returnVal;
}
