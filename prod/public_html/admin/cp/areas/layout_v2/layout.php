<?php
	ini_set("display_errors",0);

	
?>
<!DOCTYPE html>
<script type='text/javascript' src='areas/layout_v2/layout.js'></script>
<script type='text/javascript' src='//code.jquery.com/jquery.min.js'></script>
<div class='style_container'>
	
	<style scoped>
		*{
			font-family: sans-serif, verdana;
			font-weight:200;
		}
		.table_layout_container{
			width:100%;
			position:relative;
			height:100%;
			/*left: -12px;*/
		}
		.table_layout_positioner{
			/*width:1055px;*/
			height:1064px;
			margin:0 auto;
			background-color:#d0d0d0;
		}
		.table_layout_side_bar{
			width: 86px;
			height: 1000px;
			left: 1px;
			position: absolute;
			display: inline-block;
			border-right: 1px solid black;
		}
		.table_layout_top_bar{
			width:1055px;
			height:60px;
			border-bottom:1px solid black;
			padding-right: 50px;
			
		}
		#table_layout_room{
			border: 1px solid black;
			width: 969px;
			left: -3px;
			height: 1000px;
			position: relative;
			display: inline-block;
			
		}
		#canvas_background_img{
			background-image:url(images/table_layout_stripes.gif);
			background-size: contain;
			width: 969px;
			height:1000px;
		}
	
		.btn{
			font-size:small;
		}
		.btn:hover{

			cursor:pointer;
		}
		.save_btn{
			font-size:larger;
		}
		.table_layout_options_container{
			margin-top:15px;
			float:right;
		}
		.new_table{
			width:70px;
			height:70px;
			padding-bottom:10px;
			position:relative;
			margin:0 auto;
		}
		.added_table{
			position:absolute;
			width:70px;
			height:70px;
		}
		.square{
			background:url("images/table_square.png") no-repeat;
			background-size: 70px,70px;
			margin-top:10px;
		}
		.circle{
			background:url("images/table_circle.png") no-repeat;
			background-size: 70px,70px;
			
		}
		.diamond{
			background:url(images/table_diamond.png) no-repeat;
			background-size: 70px,70px;
		}
		.left_slant{
			background:url(images/table_slant_left.png) no-repeat;
			background-size: 70px,70px;
		}
		.right_slant{
			background:url(images/table_slant_right.png) no-repeat;
			background-size: 70px,70px;
		}
		#square_img, #circle_img, #diamond_img, #left_slant_img, #right_slant_img{
			display:none;
						
		}
		.delete_table{
			width:35px;
			height:35px;
			padding-bottom:10px;
			margin:0 auto;
		}
		#trash_can{
			background:url(images/deleteTableIcon.png) no-repeat;
			background-size: 35px,35px;
		}
		#trash_can:hover{
			background:url(images/deleteTableIconHover.png) no-repeat;
			cursor:pointer;
		}
		#selection_canvas{
			background-color:rgba(0,0,0,.1);
			border:1px solid black;
			display:none;
		}#rename_table{
			position:absolute; 
			display:none;
			z-index:100;
		}
		#canvas_container{
			width: 969px;
			left: 90px;
			position: absolute;
		}
		#rev_center_overlay_container{
			position:fixed;
			top:0px; 
			left:0px;
			background-color:rgba(0,0,0,.6);
			width:100%;
			height:100%;
		}
		.rev_center_overlay{
			position:relative;
			top:30px;
			background-color:white;
			margin:0 auto;
			width:900px;
			height:600px;
		}
	/*
		#popup_box{
			position:absolute; 
			margin:0 auto;
			width:300px;
			height:200px;
			background-color:white;
			border:1px solid black;
			border-radius: 12px;
			box-shadow: 10px 10px 5px #888888;
			display:none;
		}
		.close_btn{
			position:relative;
			width:100%;
			float:right;
			background:url("closeBTN.png") no-repeat;
			background-size:30px 30px;
			cursor:pointer;
			width:30px;
			height:30px;
		}
	*/
	#loading_container{
		display :none;
		z-index:300;
		position:absolute;
		left   :0px;
		top	   :0px;
		width  :100%;
		height :100%;
	}
	.loading_box{
		position: relative;
		display: table;
		margin: 0 auto;
		top: 200px;
		width: 200px;
		height: 100px;
		border: 1px solid black;
		border-radius: 5px;
		background-color: #a8a7a7;
		color: white;
	}.close_btn{
		float:right;
		border:1px solid black;
		padding:3px;
		margin-top:5px;
		margin-right:5px;
	}.close_btn:hover{
		background-color:black;
		color:white;
		cursor:pointer;
	}
	</style>
	<div id='loading_container'>
		<div class='loading_box'>Loading...</div>
	</div>
	
	<div class='table_layout_container'>
		<div class='table_layout_positioner'>
			<div class='table_layout_top_bar'>
				<span class='table_layout_options_container'>
					<?= help_mark_tostring(657); ?>
					<input type='button' class='btn save_btn' value='<?php echo speak("Save");?>' onclick='save_room()'/>
					<input type='button' class='btn' value='<?php echo speak("Delete Room")?>' onclick='delete_room()'/>
					<input type='button' class='btn' value='<?php echo speak("New Room")?>'  onclick=' if(confirm("Are you sure you want to create a new room?")){ new_room(); } '/>
					<?php echo speak("Room");?>: 
					<select id='room_select' onchange="change_room(document.getElementById('room_select').value)">
						<option value='' name=''>Select A Room</option>
					</select>
					Room Title: <input type='text'  id='room_tit' class='room_title' placeholder="Room Title" style='width:70px;'/>
					<span id='rev_center_container'>
						Rev Center: 
						<select id='rev_center' onchange="change_rev_center(document.getElementById('rev_center').value)" >
							<option>Rev Center</option>
						</select>
					</span>
					<input type='button' class='btn' value='Clone Table'  onclick='clone_table()'/>
				</span>
			</div>
			<span class='table_layout_side_bar'>
				
				<div class='new_table square' 	  id='square' 	   draggable="true"  width='70px' height='70px'></div>
				<div class='new_table circle' 	  id='circle' 	   draggable="true"  width='70px' height='70px'></div>
				<div class='new_table diamond'    id='diamond'     draggable="true"  width='70px' height='70px'></div>
				
				
				<div class='new_table left_slant'  id='left_slant'  draggable="true"  width='70px' height='70px'></div>
				<div class='new_table right_slant' id='right_slant' draggable="true"  width='70px' height='70px' style='display:none'></div>
				
				<div class='delete_table' id='trash_can' onclick="delete_table()" ></div>
			</span>
			<div id='canvas_container'>
				<div id='rename_table'>
					<input type='text' id='table_name' value='' onkeyup="if(event.keyCode==13){save_name(document.getElementById('table_name').value)}"/>
				</div>

				<canvas id='table_layout_room' ondragover="allowDrop(event)" ondragend="drag_end(event)"  width='969' height='1000' tabindex='1' >
					Your browser does not support Canvases'. Please update your browser to use table layout.  
				</canvas> 
			</div>
		</div>
			<div id='debug' ></div>
		</div>		
	</div>
</div>
<div id='rev_center_overlay_container' style='display:none'>
	<div class='rev_center_overlay'>
		<div class='close_btn' onclick='close_container()'>X</div>
		<center>
			<iframe src= '?mode=settings_revenue_centers' width='600px' height='600px' style ='border:0px'> </iframe>
		</center>
	</div>
</div>
<!--<img id='canvas_background_img' src= 'http://admin.poslavu.com/images/table_layout_stripes.gif'/>-->
<img id='canvas_background_img' src= './areas/layout_v2/cube.png' alt='background' style='display:none'/>

<!--
<canvas id= 'buffer_canvas' style='display:none'> </canvas>
<img id="circle_img"  	  src="http://admin.poslavu.com/images/table_circle.png" 	 alt="circle"	  width="70" height="70" />
<img id="square_img"  	  src="http://admin.poslavu.com/images/table_square.png" 	 alt="square" 	  width="70" height="70" />
<img id="diamond_img" 	  src="http://admin.poslavu.com/images/table_diamond.png" 	 alt="diamond" 	  width="70" height="70" />
<img id="left_slant_img"  src="http://admin.poslavu.com/images/table_slant_left.png"  alt="left slant" width="70" height="70" />
<img id="right_slant_img" src="http://admin.poslavu.com/images/table_slant_right.png" alt="right_slant" width="70" height="70" />
-->