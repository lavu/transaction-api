<?php
	ini_set("display_errors",0);
	require_once($_SERVER['DOCUMENT_ROOT']."/cp/resources/lavuquery.php");
	
?>
<script type='text/javascript' src='layout.js'></script>
<script type='text/javascript' src='http://code.jquery.com/jquery.min.js'></script>
<div class='style_container'>
	
	<style scoped>
		*{
			font-family: sans-serif, verdana;
			font-weight:200;
		}
		.table_layout_container{
			width:100%;
			position:relative;
			height:100%;
		}
		.table_layout_positioner{
			width:990px;
			height:600px;
			margin:0 auto;
			background-color:#d0d0d0;
		}
		.table_layout_side_bar{
			width: 86px;
			height: 537px;
			position: absolute;
			display: inline-block;
			border: 1px solid black;
		}
		.table_layout_top_bar{
			width:989px;
			height:60px;
			border:1px solid black;
			
		}
		#table_layout_room{
		border: 1px solid black;
		width: 902px;
		left: 87px;
		height: 537px;
		position: relative;
		display: inline-block;
			
		}
		.table_layout_background_img{
			background-image:url(images/table_layout_stripes.gif);
			background-size: contain;
			width: 902px;
			height: 537px;
		}
	
		.btn{
			//background-color:#aecd37;
			//border:1px solid white;
			//color:white;	
			//padding:5px;
			//border-radius:0px !important;
		}
		.btn:hover{
			//background-color:white;
			//color:#aecd37;
			cursor:pointer;
		}
		.save_btn{
			font-size:larger;
		}
		.table_layout_options_container{
			margin-top:15px;
			float:right;
		}
		.new_table{
			width:70px;
			height:70px;
			padding-bottom:10px;
			position:relative;
			margin:0 auto;
		}
		.added_table{
			position:absolute;
			width:70px;
			height:70px;
		}
		.square{
			background:url(images/table_square.png) no-repeat;
			background-size: 70px,70px;
		
		}
		.circle{
			background:url(images/table_circle.png) no-repeat;
			background-size: 70px,70px;
		}
		.diamond{
			background:url(images/table_diamond.png) no-repeat;
			background-size: 70px,70px;
		}
		.left_slant{
			background:url(images/table_slant_left.png) no-repeat;
			background-size: 70px,70px;
		}
		.right_slant{
			background:url(images/table_slant_right.png) no-repeat;
			background-size: 70px,70px;
		}
		#square_img, #circle_img, #diamond_img, #left_slant_img, #right_slant_img{
			display:none;
						
		}
		.delete_table{
			width:35px;
			height:35px;
			padding-bottom:10px;
			margin:0 auto;
		}
		#trash_can{
			background:url(images/deleteTableIcon.png) no-repeat;
			background-size: 35px,35px;
		}
		.trash_can:hover{
			background:url(images/deleteTableIconHover.png) no-repeat;
			cursor:pointer;
		}
		#selection_canvas{
			background-color:rgba(0,0,0,.1);
			border:1px solid black;
			display:none;
		}
	</style>
	
	<div class='table_layout_container'>
		<div class='table_layout_positioner'>
			<div class='table_layout_top_bar'>
				<span class='table_layout_options_container'>
					<input type='button' class='btn save_btn' value='Save Room' onclick='save_room()'/>
					<input type='button' class='btn' value='Delete Room' onclick='delete_room()'/>
					<input type='button' class='btn' value='New Room'  onclick='new_room()'/>
					Room: 
					<select >
						<option value=''>Select A Room</option>
					</select>
					Room Title: <input type='text'   class='room_title' placeholder="Room Title"/>
					<input type='button' class='btn' value='Clone Table' onclick='clone_selected_table()' />
					<select>
						<option value=''>Revenue Center</option>
					</select>
				</span>
			</div>
			<span class='table_layout_side_bar'>
				
				<canvas class='new_table square' 	  id='square' 	   draggable="true"  width='70px' height='70px'></canvas>
				<canvas class='new_table circle' 	  id='circle' 	   draggable="true"  width='70px' height='70px'></canvas>
				<canvas class='new_table diamond'     id='diamond' 	   draggable="true"  width='70px' height='70px'></canvas>
				<canvas class='new_table left_slant'  id='left_slant'  draggable="true"  width='70px' height='70px'></canvas>
				<canvas class='new_table right_slant' id='right_slant' draggable="true"  width='70px' height='70px'></canvas>
				
				<div class='delete_table' id='trash_can' ondragover="allowDrop(event)" ></div>
			</span>
			<div id='table_layout_room' ondragover="allowDrop(event)" ondragend="drag_end(event)">
				<div class='table_layout_background_img' onclick="unselect(null)"></div>
			</div>
			
			<div id='debug'>
			</div>
		
		</div>		
	</div>
</div>
<canvas id= 'buffer_canvas' style='display:none'> </canvas>
<img id="circle_img"  	  src="images/table_circle.png" 	 alt="circle"	  width="70" height="70" />
<img id="square_img"  	  src="images/table_square.png" 	 alt="square" 	  width="70" height="70" />
<img id="diamond_img" 	  src="images/table_diamond.png" 	 alt="diamond" 	  width="70" height="70" />
<img id="left_slant_img"  src="images/table_slant_left.png"  alt="left slant" width="70" height="70" />
<img id="right_slant_img" src="images/table_slant_right.png" alt="right_slant"width="70" height="70" />



