<?php	
	if($in_lavu)
	{
		echo "<br><br>";
		$form_posted = (isset($_POST['posted']));
		
		
		
		//--------------------------------------------- Handle Passbook Coupon Sending ----------------------------------------//
		$pb_query = lavu_query("select * from `discount_types` where `special`='passbook' and `loc_id`='[1]' and `_deleted`!='1'",$locationid);
		if(mysqli_num_rows($pb_query))
		{
		  
			$passbook_discount = "";
			$passbook_email = "";
			$show_passbook_form = true;
			

			// MEANS THAT SEND BUTTON WAS PRESSED, 'passbook_discount' is which discount is in the select.
			// Which pb coupon was held in the dom, thus we have to post to ourself so the server can
			// handle the event.
	  		if(isset($_POST['passbook_discount']))
			{
			
			    // we are now handling the user having pressed the send button.
				$show_passbook_form = false;    
				$passbook_discount = $_POST['passbook_discount'];
				$passbook_email = $_POST['passbook_email'];
				
				$send_to_emails = array();
				$passbook_email_list = explode(",",$passbook_email);
				for($i=0; $i<count($passbook_email_list); $i++)
				{
					$inner_emails = explode(";",$passbook_email_list[$i]);
					for($n=0; $n<count($inner_emails); $n++)
					{
					    //Here is the simple email verification
					    //TODO insert the php email filter that I use in the Lavutogo part.
						$iemail = trim($inner_emails[$n]);
						if($iemail!="" && strpos($iemail,"@") && strpos($iemail,".")) $send_to_emails[] = $iemail;
					}
				}
				
				$emailstr = "";
				for($i=0; $i<count($send_to_emails); $i++)
				{
					if($emailstr!="") $emailstr .= "; ";
					$emailstr .= $send_to_emails[$i];
				}
				
				if($emailstr=="")
				{
					echo "<b><font color='#880000'>Error: There are no valid email addresses.</font></b><br><br>";
					$show_passbook_form = true;
				}
				else
				{
				    //Here we make the call to the lavutogo part of the system.
					$d_query = lavu_query("select * from `discount_types` where `special`='passbook' and `loc_id`='[1]' and `_deleted`!='1' and `id`='[2]'",$locationid,$passbook_discount);
					if(mysqli_num_rows($d_query))
					{

						$d_read = mysqli_fetch_assoc($d_query);
						
						//A few more I need...
						//$companyName = admin_info("company_name");//This is strangely showing up blank, so we just perform a query to get it.
						$compNameQueryStr = "SELECT * FROM `poslavu_MAIN_db`.`restaurants` WHERE `data_name`='[1]'";
						$compNameQuery = mlavu_query($compNameQueryStr, $data_name);
						
						$r_read;
						if(mysqli_num_rows($compNameQuery)){
    						$r_read = mysqli_fetch_assoc($compNameQuery);
						}else{
    						$r_read = array();
						}
						//We append the needed information from the restaurants row.
						$d_read['company_name'] = $r_read['company_name'];
						$d_read['image_name']   = $r_read['logo_img'];
						$d_read['email_list'] = $emailstr; 
						$d_read['data_name']  = $data_name;
						$d_read['security_hash'] = sha1($emailstr . $data_name . 'Mr_JellyBean');
						//$d_read['expiration_date'] = $_POST['expiration_date'];
						
						//The URL and post variables (going to lavu togo server).
						$url = 'https://www.lavutogo.com/v1/Passbook/CouponBatchRequest.php';
						
						$d_read['discount_id'] = $_POST['discount_id'];
						$vars = http_build_query($d_read);
						
						$ch = curl_init();
						curl_setopt($ch, CURLOPT_URL, $url);
						curl_setopt($ch, CURLOPT_POST, 1);
						curl_setopt($ch, CURLOPT_POSTFIELDS, $vars);
						curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
						$response = curl_exec($ch);//   <---Here we call to the lavutogo server.   THIS IF FOR THE DIRECT COUPON CREATION, NOT SYNC COUPON DATA.
						curl_close ($ch); 
						$response_parts = explode("|", $response);
						
						//If we have success returned from the lavutogo server...
						if($response_parts[0]=="success" && count($response_parts) > 1)
						{
							echo "<b><font color='#08800'>" . trim($response_parts[1]) . "</font></b><br><br>";
						}
						else if($response_parts[0]=="error" && count($response_parts) > 1)//Error
						{
							echo "<b><font color='#880000'>Error: ".trim($response_parts[1])."</font></b><br><br>";
							$show_passbook_form = true;
						}
						else //Can't communicate
						{
							echo "<b><font color='#880000'>Error: Unable to communicate with Lavu Passbook Server.</font></b><br><br>";
							$show_passbook_form = true;
						}
					}
					else
					{
						echo "<b><font color='#880000'>Error: Unable to load coupon.</font></b><br><br>";
						$show_passbook_form = true;
					}
				}
			}
			
			if($form_posted) $show_passbook_form = false;
			
			//SHOWING THE TEXTAREA FOR ENTERING THE EMAIL LIST.
			if($show_passbook_form)
			{
			    // T H E    H T M L    F O R M    --that posts to ourself recursively 
			    //The html form which performs recursive posting. (pb-is prefixed for post-back (recursive post to yourself)).
    			echo "<form name='pbsend' method='post' action='index.php?mode=$mode'>";
    			echo "<table cellspacing=0 cellpadding=12 bgcolor='#dddde2' style='border:solid 1px #9999aa'>";
    			echo "<tr><td align='left'>";
    			
    			echo "<table><tr><td>";
    			echo "Send Passbook Coupon ";
    			echo "<select name='passbook_discount' id='pb_discount_selector'>";
    			$discount_id_arr = array();
    			while($pb_read = mysqli_fetch_assoc($pb_query))
    			{ 
    				$pb_id = $pb_read['id'];
    				$discount_id_arr[] = $pb_id;
    				$pb_name = $pb_read['label'];
    				echo "<option value='$pb_id'";
    				if($passbook_discount==$pb_id) echo " selected";
    				echo ">$pb_name</option>";
    			}
    			
    			//Begin javascript
                echo "<script type='text/javascript'>";
    			//We create a javascript array that mirrors the php discount_id_arr
    			echo "var pb_discount_ids = new Array();";
    			foreach($discount_id_arr as $currDiscountID){
        			echo "pb_discount_ids.push($currDiscountID);";
    			}
    			//Now we create the final submit function, which will carry our discount_id
    			echo "function submit_dts_form(){";
    			echo     "var discount_id_holder = document.getElementById('discount_id_hidden');";
                echo     "var discount_id_selector = document.getElementById('pb_discount_selector');";
    			echo     "var pb_ids_selectedIndex = discount_id_holder.selectedIndex;";
    			echo     "discount_id_holder.value = pb_discount_ids[discount_id_selector.selectedIndex];";
    			echo     "document.pbsend.submit();";
    			echo "}";
    			echo "</script>";
    			//End Javascript
    			
    			echo "</select>";
    			echo " to:";
    			echo "<br><textarea rows='4' cols='60' name='passbook_email'>$passbook_email</textarea>";
    			echo "</td></tr>";
    			echo "<tr>";
    			//echo "    <td align='left'> Experation Date (i.e. YYYY-mm-dd):";
    			echo "    <td align='left'>";
    			echo "       <input type='hidden' id='discount_id_hidden' value='MARKERVALUE' name='discount_id'>";////
    			    			
    			//echo "       <input type='text'   value='2014-12-31' name='expiration_date'/>";
    			//echo "       <input type='button' value='Send >>' onclick='document.pbsend.submit()' />";
    			echo "       <input type='button' value='Send >>' onclick='submit_dts_form()' />";
    			echo "    </td>";
    			echo "</tr>";
    			echo "</table>";
    			
    			echo "</td></tr>";
    			echo "</table>";
    			echo "</form>";
			}
			echo "<br><br>";
		}
		//---------------------------------------------------------------------------------------------------------------------//
		
		
		//Sends a request to sync coupon information to the couponing server.
		function syncCouponInformation($dataNameParam){
    		

		    $databaseName = 'poslavu_' . $dataNameParam . '_db';
		
    		//Here we perform a query on the discount type information.
    		$getPassbooks = "SELECT * FROM `[1]`.`discount_types` WHERE `special`='passbook' && (`_deleted`='0' OR `_deleted`='')";
    		$passbookResult = mlavu_query($getPassbooks, $databaseName);
    		if(!$passbookResult){
    		    return;
    		}
    		$rows = array();
    		while($currRow = mysqli_fetch_assoc($passbookResult)){
        		$rows[] = $currRow;
    		}
    		//We need to find the image name
    		$getLogoFilenameSelect  = "SELECT `logo_img`,`company_name` FROM `poslavu_MAIN_db`.`restaurants` WHERE `data_name`='[1]'";
    		$logoImgResult = mlavu_query($getLogoFilenameSelect, $dataNameParam);
    		$imageName = ''; $companyName = '';
    		if($logoImgResult && mysqli_num_rows($logoImgResult)){
        		$row = mysqli_fetch_assoc($logoImgResult);
        		$imageName = $row['logo_img'];
        		$companyName = $row['company_name'];
    		}		
    		
    		
    		
    		//We now generate the coupon list
    		$couponListArr = array();
    		foreach($rows as $currRow){
    		    //Gotta find the discount type.
    		    $codeMap = array( 'd' => 'amount', 'p' => 'percent' );
        		$currCouponDict = array();
        		$currCouponDict['discount_id'] = $currRow['id'];
        		$currCouponDict['discount_type'] = $codeMap[$currRow['code']]; //Either 'amount' or 'percent'
        		$currCouponDict['discount_value'] = $currRow['calc'];
        		$currCouponDict['discount_object'] = $currRow['label'];
        		$currCouponDict['start_date'] = $currRow['valid_start_date_inc'];
        		$currCouponDict['end_date'] = $currRow['valid_end_date_inc'];
        		$currCouponDict['expiration'] = $currRow['expiration_date'];
        		
        		$couponListArr[] = $currCouponDict;
    		}
    		
    		
    		
    		//Now we want to build the expected json.
    		$globalJSONAsArr = array();
    		$globalJSONAsArr['data_name']  = $dataNameParam;
    		$globalJSONAsArr['image_name'] = $imageName;
    		$globalJSONAsArr['company_name'] = $companyName;
    		$globalJSONAsArr['coupon_list'] = $couponListArr;
    		
    		//error_log('BEFORE');
    		$merchantSyncDataJSON = json_encode($globalJSONAsArr);
    		//$debugArrayStr = print_r($merchantSyncDataJSON, 1);
    		//error_log('AFTER:');
    		//error_log($debugArrayStr);
    		$postString  = "lv_coupon_sync_data=" . urlencode($merchantSyncDataJSON);
    		$postString .= "&jibberish=jJ29Sk3lSvioelDh310ZAQle059263jHfkeFk3927dDk";
    		$postString .= "&security_hash=" . sha1($dataNameParam . "MR_BEAN" . $dataNameParam);
    		$postString .= "&data_name=".$dataNameParam;
    		//Finally we encode this and curl it.
    		$url = "https://www.lavutogo.com/v1/Passbook/_CrossServerScripts/sync_couponing_merchant.php";

    		$ch = curl_init($url);
    		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    		curl_setopt($ch, CURLOPT_POST, true);
    		curl_setopt($ch, CURLOPT_POSTFIELDS, $postString);
    		$returnString = curl_exec($ch);
    		curl_close($ch);

    		return $returnString;
		}
		
		
				
		$display = "";
		$field = "";
		
		$qtitle_category = false;
		$qtitle_item = "Discount"; // do not translate here, will be translated later
		
		$category_tablename = "";
		$inventory_tablename = "discount_types";
		$category_fieldname = "";
		$inventory_fieldname = "id";
		$inventory_select_condition = " AND `loc_id` = '$locationid'";
		$inventory_primefield = "title";
		$category_primefield = "";
							
		$inventory_orderby = "_order";

		$qfields = array();
		
		
		if($data_name == 'demo_brian_d' || $data_name == '17kart' || $data_name == '17edison' || $data_name == 'ersdemo' || 
		         $data_name == 'mean_bao_llc' || $data_name == '16edison' || '84_salval'){   
		    $qfields[] = array("special","special",speak("Special Type"),"select",array(array(speak("Standard"),""),array("LoyalTree","loyaltree"),array("Mercury Loyalty","mercury_loyalty"),array("PassBook","passbook")));
        }
		else{
    		$qfields[] = array("special","special",speak("Special Type"),"select",array(array(speak("Standard"),""),array("LoyalTree","loyaltree"),array("Mercury Loyalty","mercury_loyalty")));
        }
		$qfields[] = array("title","title",speak("Button Title"),"input",array("' size='16"));
		$qfields[] = array("label","label",speak("Receipt Label"),"input",array("' size='16"));
		$qfields[] = array("calc","calc",speak("Calculate Value"),"input",array("' size='4"));
		$qfields[] = array("code","code",speak("Calculate Type"),"select",array(array(speak("Percentage"),"p"),array(speak("Amount"),"d")));
		$qfields[] = array("level","level","","select",array(array(speak("Applicable to checks"),"checks"),array(speak("Applicable to items"),"items"),array(speak("Applicable to both"),"")));
		
		$qfields[] = array("valid_start_date_inc","valid_start_date_inc","Valid Start Date Inc","hidden");
		$qfields[] = array("valid_end_date_inc","valid_end_date_inc","Valid End Date Inc","hidden");
		$qfields[] = array("expiration_date","expiration_date","Expiration Date","hidden");
		
		if($modules->hasModule("financial.discount.groups"))
		{
			$discount_groups = array();
			$discount_groups[] = array("----- Group -----","");
			$dgroup_query = lavu_query("select * from discount_groups where _deleted!='1' order by `name` asc");
			while($dgroup_read = mysqli_fetch_assoc($dgroup_query))
			{
				$discount_groups[] = array($dgroup_read['name'],$dgroup_read['id']);
			}
			$qfields[] = array("groupid","groupid",speak("Discount Group"),"select",$discount_groups);
		}
		$qfields[] = array("min_access","min_access",speak("Minimum access required"),"select",array(array("Access Level 1","1"),array("Access Level 2 (2.1+)","2"),array("Access Level 3 (2.1+)","3"),array("Access Level 4 (2.1+)","4")));
		$qfields[] = array("loc_id","loc_id","Location ID","hidden", $locationid);
	
		$qfields[] = array("id","id");
		$qfields[] = array("delete");
		require_once(resource_path() . "/dimensional_form.php");
		$qdbfields = get_qdbfields($qfields);
					
		$cat_count = 1;
						
		if($form_posted)
		{

			$item_count = 1;
			while(isset($_POST["ic_".$cat_count."_".$inventory_primefield."_".$item_count]))
			{						
				$item_value = array(); // get posted values
				for($n=0; $n<count($qfields); $n++)
				{
					$qname = $qfields[$n][0];
					$qtitle = (isset($qfields[$n][2]))?$qfields[$n][2]:false;
					$set_item_value = $_POST['ic_'.$cat_count.'_'.$qname.'_'.$item_count];
					if($qtitle && $set_item_value==$qtitle) $set_item_value = "";
					if($qname=="cost") 
					{
						$set_item_value = str_replace("$","",$set_item_value);
						$set_item_value = str_replace(",","",$set_item_value);
					}
					$item_value[$qname] = $set_item_value;
				}
				$item_id = $item_value['id'];
				$item_delete = $item_value['delete'];
				$item_name = $item_value[$inventory_primefield];
				
				if($item_delete=="1")
				{
					delete_poslavu_dbrow($inventory_tablename, $item_id);
				}
				else if($item_name!="")
				{
					$dbfields = array();
					for($n=0; $n<count($qdbfields); $n++)
					{
						$qname = $qdbfields[$n][0];
						$qfield = (isset($qdbfields[$n][1]))?$qdbfields[$n][1]:$qname;
						$dbfields[] = array($qfield,$item_value[$qname]);						
					}
					$orderby_tag = 'icrow_'.$cat_count.'_'.$item_count.'_order';
					if(isset($_POST[$orderby_tag]))
					{
						$dbfields[] = array("_order",$_POST[$orderby_tag]);
					}
					update_poslavu_dbrow($inventory_tablename,$dbfields, $item_id);
				}								
				$item_count++;
			}
			
			
			
			//Inserted by Brian D.  When the user hits save we sync the couponing information.
			if($data_name=="ersdemo" || $data_name =="demo_brian_d" || $data_name=="17edison" || $data_name=="16edison" || $data_name=="84_salval" || $data_name=="cozy_pines_res" || $data_name="mean_bao_llc")
			{
			
    			$couponingServerResponseJSON = syncCouponInformation($data_name);
    		    $returnJSONDict = json_decode($couponingServerResponseJSON);
    		    
    		    //Due to an error in the json_decode implementation
    		    $returnJSONDict = $returnJSONDict[0];
    		    if( !is_array($returnJSONDict) ){
        		    $javaScriptus  =  "<script type='text/javascript'>";
        		    $javaScriptus .=  " alert('Network communication problem -- Error_Code:ds11000'); </script>";
        		    echo $javaScriptus;
    		    }
    		    else if($returnJSONDict['status'] == 'failed'){
    		        //handleSyncFail($returnJSONDict);
    		        $errorCode = $returnJSONDict['e'];
    		        $errorDisplayMSG = preg_replace("/'/", "\\'", $returnJSONDict['fail_display']);//Keep from breaking the javascriptus alert('').
    		        $javaScriptus = "<script type='text/javascript'> alert('$errorDisplayMSG\\n\\n -- Error_Code:$errorCode --'); </script>";
    		        echo $javaScriptus;
    		    }
    		    else if($returnJSONDict['status'] == 'success'){
        		    if(isset($returnJSONDict['warning_display'])){
        		        $warningDisplay = preg_replace('/\'/', '\\\'', $returnJSONDict['warning_display']);
            		    $javaScriptus  = "<script type='text/javascript'> alert('$warningDisplay');";
            		    $javaScriptus .= "</script>";
            		    echo $javaScriptus;
        		    }
    		    }
			}
			echo "<script language='javascript'>window.location.replace('index.php?mode=$mode')</script>";
			exit();
        }
		
		//We back out of areas to v2, then resources 
		//echo "<script language='javascript'>";
		//require_once(__DIR__ . "/../resources/info_window.js");
		//echo "</"."script>";
		
		$details_column = "Discount Details";//"Zip Codes";
		$send_details_field = false;//"zip_codes";
	
		$qinfo = array();
		$qinfo["qtitle_category"] = $qtitle_category;
		$qinfo["qtitle_item"] = $qtitle_item;
		$qinfo["category_tablename"] = $category_tablename;
		$qinfo["inventory_tablename"] = $inventory_tablename;
		$qinfo["category_fieldname"] = $category_fieldname;
		$qinfo["inventory_fieldname"] = $inventory_fieldname;
		$qinfo["inventory_select_condition"] = $inventory_select_condition;
		$qinfo["inventory_primefield"] = $inventory_primefield;
		$qinfo["category_primefield"] = $category_primefield;
		$qinfo["details_column"] = $details_column;
		$qinfo["send_details"] = $send_details_field;
		$qinfo["field"] = $field;
		
		$qinfo["category_filter_by"] = $category_filter_by;
		$qinfo["category_filter_value"] = $category_filter_value;
		$qinfo["inventory_filter_by"] = "loc_id";
		$qinfo["inventory_filter_value"] = $locationid;

		$qinfo["inventory_orderby"] = $inventory_orderby;

		$form_name = "ing";		

		$field_code = create_dimensional_form($qfields,$qinfo,$cfields);

		$display .= "<table cellspacing='0' cellpadding='3' class='tableSpacing'><tr><td align='center' style='border:2px solid #DDDDDD'>";
		$display .= "<form name='ing' id='discount_types_html_form' method='post' action=''>";
		//$display .= "<input type='submit' value='".speak("Save")."' style='width:120px'><br>&nbsp;";
		$display .= "<input type='hidden' name='posted' value='1'>";
		$display .= "<br><b>Discount Types</b><br><br>";
		$display .= $field_code;
		$display .= "&nbsp;<br><input type='submit' value='".speak("Save")."' style='width:120px'>";
		$display .= "</form>";
		$display .= "</td></tr></table>";

		echo $display;		
		echo create_info_layer(600,480);
	}
?>
