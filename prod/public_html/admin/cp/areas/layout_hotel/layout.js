var _tables       = new Array();
var canvas        = new fabric.Canvas('c', { selection: false });
var grid          = 30;
var canvas_width  = 900;
var canvas_height = 700;

function add_event_listeners(){
  document.addEventListener("keyup", delete_table);
}
function build_grid(){
  for (var i = (canvas_width / grid); i > 0; i--) {
    canvas.add(new fabric.Line([ i * grid, 0, i * grid, canvas_width], { stroke: '#ccc', selectable: false }));

  }
  for (var i = (canvas_width / grid); i > 0; i--) {
      canvas.add(new fabric.Line([ 0, i * grid, canvas_width, i * grid], { stroke: '#ccc', selectable: false }))
    }
}
function save_room(){
  var objs  = canvas._objects;
  var rooms = [];
  var title   = document.getElementById('room_tit').value;
  for(var i=0; i< objs.length; i++){
    if( objs[i].type=='group'){ //this means its actually a roomthingy 
       var room_obj = {};
       if( objs[i].type == 'circle' || ( objs[i]._objects &&  objs[i]._objects[0].type== 'circle')){
          if(objs[i]._objects)
            var circ = objs[i]._objects[0];
          else 
            var circ = objs[i];

          room_obj= {   
            radius:           circ.radius, 
            scaleX:           objs[i].scaleX,
            scaleY:           objs[i].scaleY,
            stroke:           'gray', 
            strokeWidth:      1,
            type:             'circle',
            fill:             '#9f9', 
            originX:          'left', 
            originY:          'top',
            centeredRotation: true,
            x:                objs[i].left, 
            y:                objs[i].top,
            angle:            objs[i].angle,
            id:               objs[i]._objects[0].id,
            name:             objs[i]._objects[1].text,
            transparentCorners: false,
          };
        }else{
          
          if(objs[i]._objects)
            var rect =  objs[i]._objects[0];
          else 
            var rect =  objs[i];
          
          room_obj = { 
            width:              rect.width, 
            height:             rect.height, 
            scaleX:             objs[i].scaleX,
            scaleY:             objs[i].scaleY,
            transparentCorners: false,
            type:               'rectangle',
            stroke:             'gray', 
            strokeWidth:        1,
            fill:               '#faa', 
            originX:            'left', 
            originY:            'top',
            id:                 objs[i]._objects[0].id,
            name:               objs[i]._objects[1].text,
            x:                  objs[i].left, 
            y:                  objs[i].top,
            angle:              objs[i].angle,
            centeredRotation:   true,
          };
        }
        rooms.push(room_obj);
      }
    }
    var string = JSON.stringify(rooms);
    var room_id =document.getElementById("room_select").value;
    performAJAX("./areas/layout_hotel/update_db.php","function=save_room&title="+title+"&room_id="+room_id+"&tables="+string,false,"POST");
}
function save_name(val){
  canvas.getActiveObject()._objects[1].text= val;
  document.getElementsByClassName("name_overlay")[0].style.display='none';
  canvas.renderAll();
}
function center_view(){
  $('canvas').click( function() {
    $('html,body').animate({ scrollTop: $("canvas").offset().top - ( $(window).height() - $("canvas").outerHeight(true) ) / 2  }, 200);
  });
}
function get_rooms(){
  var room_data   = performAJAX("./areas/layout_hotel/update_db.php","function=get_rooms",false,"POST");
  var decoded     = decodeURIComponent(room_data);
  
  if(!check_json(decodeURIComponent(room_data))){
    new_room();
    return;
  }   

  var obj = JSON.parse(decodeURIComponent(room_data));
  if(obj[0].id=='-1'){
    new_room();
    return;
  }
  var room_selector = document.getElementById("room_select");
  for(var i=0; i < obj.length; i++){
    var option   = document.createElement("option");
    if(i==0){
      room_id = obj[i].id;
      document.getElementById("room_tit").value= obj[i].title;
      option.selected = true;
      _tables= JSON.parse(obj[i].tables);
      for(var c = 0; c < _tables.length; c++){
        
        if(!_tables[c].scaleX){
          _tables[c].scaleX = 1;
        }
        if(!_tables[c].scaleY){
          _tables[c].scaleY = 1;
        }
        if(!_tables[c].angle){
          _tables[c].angle = 0;
        }
        var text = new fabric.Text(_tables[c].name, {
            fontSize: 15,
            textAlign:'center',
            scaleX:1, 
            scaleY:1
        });
        var room = null;
        if( _tables[c].type == 'circle' ){
         
          if(!_tables[c].radius){
            _tables[c].radius = _tables[c].width/2;
          }
          
          room = new fabric.Circle({ 
           
            radius:             _tables[c].radius, 
            id:                 _tables[c].id,
            type:               'circle',
            stroke:             'gray', 
            strokeWidth:        1,
            fill:               '#9f9', 
            originX:            'left', 
            originY:            'top',
            centeredRotation:   true,
            transparentCorners: false,
            rx: -1,
            ry: -1,
          });
        }else{
          room = new fabric.Rect({ 
            width:   _tables[c].width, 
            height:  _tables[c].height, 
            
            transparentCorners: false,
            type:'rectangle',
            stroke: 'gray', 
            strokeWidth: 1,
            fill:  '#faa', 
            originX: 'left', 
            originY: 'top',
            id:_tables[c].id,
            centeredRotation: true,
            rx: -1,
            ry: -1,
          });
        }
        var group = new fabric.Group([room,text ], {
            left:  _tables[c].x, 
            top:   _tables[c].y,
            scaleX:_tables[c].scaleX,
            scaleY:_tables[c].scaleY,
            angle: _tables[c].angle,
        });
        canvas.add(group);
      }
    }
    option.value = obj[i].id;
    option.text  = obj[i].title;
    try{
      // for IE earlier than version 8
      room_selector.add(option,room_selector.options[null]);
    }catch (e){
      room_selector.add(option,null);
    }
  }
}
function load_room(id){
  
  var room_id = document.getElementById("room_select").value;
  var obj     = decodeURIComponent(performAJAX("./areas/layout_hotel/update_db.php","function=load_room&room_id="+room_id,false,'POST'));
  var parsed  = JSON.parse(obj);
  var _tables = parsed.tables;
  document.getElementById("room_tit").value= parsed.title;
  clear_room();
  for(var c = 0; c < _tables.length; c++){
    if(!_tables[c].scaleX){
      _tables[c].scaleX = 1;
    }
    if(!_tables[c].scaleY){
      _tables[c].scaleY = 1;
    }
    if(!_tables[c].angle){
      _tables[c].angle = 0;
    }
    var text = new fabric.Text(_tables[c].name, {
        fontSize: 15,
        textAlign:'center',
        scaleX:1, 
        scaleY:1
    });
    var room = null;
    if( _tables[c].type == 'circle' ){
     
      if(!_tables[c].radius){
        _tables[c].radius = _tables[c].width/2;
      }
      
      room = new fabric.Circle({ 
       
        radius:             _tables[c].radius, 
        id:                 _tables[c].id,
        type:               'circle',
        stroke:             'gray', 
        strokeWidth:        1,
        fill:               '#9f9', 
        originX:            'left', 
        originY:            'top',
        centeredRotation:   true,
        transparentCorners: false,
        rx: -1,
        ry: -1,
      });
    }else{
      room = new fabric.Rect({ 
        width:   _tables[c].width, 
        height:  _tables[c].height, 
        
        transparentCorners: false,
        type:'rectangle',
        stroke: 'gray', 
        strokeWidth: 1,
        fill:  '#faa', 
        originX: 'left', 
        originY: 'top',
        id:_tables[c].id,
        centeredRotation: true,
        rx: -1,
        ry: -1,
      });
    }
    var group = new fabric.Group([room,text ], {
        left:  _tables[c].x, 
        top:   _tables[c].y,
        scaleX:_tables[c].scaleX,
        scaleY:_tables[c].scaleY,
        angle: _tables[c].angle,
    });
    canvas.add(group);
  }
}
function clear_room(){
  var objs  = canvas._objects;
  var arr = [];
  for(var i=0; i< objs.length; i++){
    if( objs[i].type=='group'){ //this means its actually a roomthingy 
      arr.push(objs[i]);
    }
  }
  for (var i =0; i< arr.length; i++){
    canvas.remove(arr[i]); 
  }
}
function new_room(){
  var val           = performAJAX("./areas/layout_hotel/update_db.php","function=new_room",false,'POST');
  var option        = document.createElement("option");
  var room_selector = document.getElementById("room_select")
  var json          = JSON.parse(decodeURIComponent(val));
  _tables=new Array();
  clear_room();
  option.selected = true;
  option.value    = json.id;
  option.text     = "New Room";
  document.getElementById("room_tit").value="New Room";
  try{
    // for IE earlier than version 8
    room_selector.add(option,room_selector.options[null]);
  }catch (e){
    room_selector.add(option,null);
  }
}
function change_room(room_id){
  var room_id= document.getElementById("room_select").value;
  if(room_id)
    load_room(room_id);
}
function build_defaults(){

  var circle = new fabric.Circle({ 
    radius:30,   
    left: 910, 
    top: 5, 
    stroke:      'gray', 
    strokeWidth: 1,
    fill:        '#9f9', 
    originX: 'left', 
    originY: 'top',
    type:'circle',
    centeredRotation: true,
    transparentCorners: false,
    id:'default_circ',
  });

  var rectangle = new fabric.Rect({ 
    width:   60, 
    height:  60,
    left: 910, 
    top: 80,  
    type:'rectangle',
    transparentCorners: false,
    stroke: 'gray', 
    strokeWidth: 1,
    fill:  '#faa', 
    originX: 'left', 
    originY: 'top',
    centeredRotation: true,
    id:'default_rect'
  });
  
  canvas.add(circle);
  canvas.add(rectangle);
}
function onSelected(options){

  if(options.target.id != 'default_rect' && options.target.id != 'default_circ' ){
    var a     = canvas.getActiveObject();
    var index = canvas._objects.indexOf(a)
    if (index > -1) {
      canvas._objects.splice(index, 1);
    }
    canvas._objects.push(a);
    var overlay = document.getElementsByClassName('name_overlay')[0];
    overlay.style.display = 'none';
    //options.target.setControlsVisibility({"bl":false, "tl":false,"mt":false,"tr":false,"ml":false});
  }else{
    options.target.setControlsVisibility({"tl":false, "tr": false,"br": false,"bl": false,"ml": false,"mt": false,"mr": false,"mb": false,"mtr": false});
    var cloned = my_clone(options.target);
    canvas.add(cloned);
    canvas.setActiveObject(cloned);
  }
}
function delete_table(e){
  e.preventDefault();
  if( (e.keyCode == 8 || e.keyCode == 46 || e.keyCode==0) && canvas.getActiveObject() && document.activeElement.className != 'name_overlay_input'){
    canvas.remove(canvas.getActiveObject());
  }
}
function onRotate(options){
  options.target.setControlsVisibility({"bl":false, "tl":false,"mt":false,"tr":false,"ml":false});
  var a = Math.round(options.target.angle);
  var rem= a % 30;
  
  if(rem <= 15)
    a -=rem;
  else 
    a+= 30-rem;
  options.target.set({
      angle: a
  });
}
function onChange(options){
    if(options.target.id != 'default_rect' && options.target.id != 'default_circ' ){
      options.target.set({ 
        left: Math.round(options.target.left / grid) * grid,
        top: Math.round(options.target.top / grid) * grid
      });

      if(options.target.top < 0)
         options.target.top = 0;
      if(options.target.left < 0)
         options.target.left = 0;
      if( (options.target.left + options.target.currentWidth ) > 900){
        options.target.left = 900 - options.target.currentWidth ;

      }
      if(options.target.top+options.target.currentHeight > 700){
         options.target.top = 700 - options.target.currentHeight;
      }
      var obj     = canvas.getActiveObject();
      var overlay = document.getElementsByClassName('name_overlay')[0];
      overlay.style.top     = parseInt(obj.top)+"px";
      overlay.style.left    = parseInt(obj.left)+"px";
      overlay.style.width   = parseInt(obj.currentWidth)+"px";
    }else{
      if(options.target.type=='circle')
        options.target.set({left:910, top:5});
      else if( options.target.type=='rectangle')
        options.target.set({left:910, top:80});
    }
}
function onDoubleClick(options){
  var obj     = canvas.getActiveObject();
  var overlay = document.getElementsByClassName('name_overlay')[0];
      overlay.style.top     = parseInt(obj.top)+"px";
      overlay.style.left    = parseInt(obj.left)+"px";
      overlay.style.width   = parseInt(obj.currentWidth)+"px";
      overlay.style.display = 'inline-block';

  document.getElementsByClassName("name_overlay_input")[0].value = obj._objects[1].text;
}
function clone_table(){
  var cloned = my_clone(canvas.getActiveObject());
  canvas.add(cloned);
  canvas.setActiveObject(cloned);
}
function clone_room(){
  var objs  = canvas._objects;
  var rooms = [];
  var title   = document.getElementById('room_tit').value + " (clone)";
  for(var i=0; i< objs.length; i++){
    if( objs[i].type=='group'){ //this means its actually a roomthingy 
       var room_obj = {};
       if( objs[i].type == 'circle' || ( objs[i]._objects &&  objs[i]._objects[0].type== 'circle')){
          if(objs[i]._objects)
            var circ = objs[i]._objects[0];
          else 
            var circ = objs[i];

          room_obj= {   
            radius:           circ.radius, 
            scaleX:           objs[i].scaleX,
            scaleY:           objs[i].scaleY,
            stroke:           'gray', 
            strokeWidth:      1,
            type:             'circle',
            fill:             '#9f9', 
            originX:          'left', 
            originY:          'top',
            centeredRotation: true,
            x:                objs[i].left, 
            y:                objs[i].top,
            angle:            objs[i].angle,
            id:               objs[i]._objects[0].id,
            name:             objs[i]._objects[1].text,
            transparentCorners: false,
          };
        }else{
          
          if(objs[i]._objects)
            var rect =  objs[i]._objects[0];
          else 
            var rect =  objs[i];
          
          room_obj = { 
            width:              rect.width, 
            height:             rect.height, 
            scaleX:             objs[i].scaleX,
            scaleY:             objs[i].scaleY,
            transparentCorners: false,
            type:               'rectangle',
            stroke:             'gray', 
            strokeWidth:        1,
            fill:               '#faa', 
            originX:            'left', 
            originY:            'top',
            id:                 objs[i]._objects[0].id,
            name:               objs[i]._objects[1].text,
            x:                  objs[i].left, 
            y:                  objs[i].top,
            angle:              objs[i].angle,
            centeredRotation:   true,
          };
        }
        rooms.push(room_obj);
      }
    }
    var string = JSON.stringify(rooms);
    var room_id =document.getElementById("room_select").value;
    performAJAX("./areas/layout_hotel/update_db.php","function=clone_room&title="+title+"&tables="+string,false,"POST");
}
function my_clone(to_clone){
  var text = new fabric.Text("Room "+canvas._objects.length, {
      fontSize: 15,
      textAlign:'center',

  });
  if(to_clone.type == 'circle' || (to_clone._objects && to_clone._objects[0].type== 'circle')){
    if(to_clone._objects)
      var circ =  to_clone._objects[0];
    else 
      var circ =  to_clone;
    room = new fabric.Circle({   
      radius: circ.radius, 
      id: 'obj_'+canvas._objects.length,
      stroke: 'gray', strokeWidth: 2,
      type:'circle',
      fill: '#9f9', 
      originX: 'left', 
      originY: 'top',
      centeredRotation: true,
      transparentCorners: false
    });
  }else{
    if(to_clone._objects)
      var rect =  to_clone._objects[0];
    else 
      var rect =  to_clone;
    room = new fabric.Rect({ 
      
      width:   rect.width, 
      height:  rect.height, 
      transparentCorners: false,
      type:'rectangle',
      stroke: 'gray', strokeWidth: 2,
      fill:  '#faa', 
      originX: 'left', 
      originY: 'top',
      id: 'obj_'+canvas._objects.length,
      centeredRotation: true
    });
  }
  var group = new fabric.Group([room,text ], {
    left:   to_clone.left, 
    top:    to_clone.top,
    scaleX: to_clone.scaleX,
    scaleY: to_clone.scaleY,
    angle:  to_clone.angle
  });
  return group;
}
function check_json(text){
  
  if (/^[\],:{}\s]*$/.test(text.replace(/\\["\\\/bfnrtu]/g, '@').
  replace(/"[^"\\\n\r]*"|true|false|null|-?\d+(?:\.\d*)?(?:[eE][+\-]?\d+)?/g, ']').
  replace(/(?:^|:|,)(?:\s*\[)+/g, ''))) {
    return true;
  }else{
      return false;
    }
}
function performAJAX(URL, urlString,asyncSetting,type ){
    var returnVal='';
    $.ajax({
      url: URL,
      type: type,
      data:urlString,
      async:asyncSetting,
      success: 
        function (string){
          returnVal= decodeURIComponent(string);
          if(returnVal.indexOf("Error: ") !=-1){
            alert(returnVal);
            window.location="https://admin.poslavu.com/cp";
          }
        }
    }); 
    return returnVal; 
}
function initialize(){
  build_grid();
  build_defaults();
  get_rooms();
  add_event_listeners();
  canvas.on({
    'object:moving': onChange,
    'object:rotating': onRotate,
    'object:selected': onSelected
  });
}
fabric.util.addListener(fabric.document, 'dblclick', onDoubleClick);
window.onload = function(){
  initialize();
};