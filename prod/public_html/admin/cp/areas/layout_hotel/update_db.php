<?php
	session_start();
	ini_set('display_errors',0);
	$v2_dir = (stristr(dirname(__FILE__), "v2") === FALSE) ? "cp" : "v2";
	require_once($_SERVER['DOCUMENT_ROOT']."/{$v2_dir}/resources/core_functions.php");
	$rdb = admin_info("database");
	$data_name_bd = admin_info('dataname');

	$loc_id= check_location_id($rdb, sessvar("locationid"));
	
	lavu_connect_dn($data_name_bd, $rdb);
	
	
	if($loc_id===null || $rdb ===null){
		echo "Error: You are not logged in. Cannot Save. loc_id is:". admin_info('loc_id');
		return;
	}
		
	if(isset($_POST['function'])){
		$fun_name= $_POST['function'];
		unset($_POST['function']);
		$fun_name($_POST);			
	}
	
	function save_room($args){
		global $loc_id;
		if(isset($args['room_id'])){
			lavu_connect_dn(admin_info('dataname'),'poslavu_'.admin_info('dataname')."_db");
			if(mysqli_num_rows(lavu_query("SELECT * FROM `poslavu_[1]_db`.`tables` where `id`='[2]'", admin_info('dataname'),$args['room_id']))){
				$query=lavu_query("UPDATE `poslavu_[1]_db`.`tables` set `tables_json`='[2]', `title`='[3]' WHERE `id`='[4]' AND `loc_id`='[5]' ",admin_info('dataname'),rawurldecode($args['tables']), $args['title'], $args['room_id'], $loc_id);
			}
			lavu_query("UPDATE `poslavu_[1]_db`.`devices` SET `needs_reload` = '1' WHERE `loc_id` = '[2]'", admin_info('dataname'), $loc_id);
			convert_to_old(rawurldecode($args['tables']), $args['room_id']);
			$location_info= mysqli_fetch_assoc(lavu_query("SELECT * FROM `locations` WHERE `id`='[1]'", $loc_id));
			schedule_remote_to_local_sync($loc_id,  "table updated", "tables");
		}else
			echo "Error: Could not save room. No ID sent";
	}
	function delete_room($args){
		if(isset($args['room_id'])){
			lavu_connect_dn(admin_info('dataname'),'poslavu_'.admin_info('dataname')."_db");
			$query=lavu_query("UPDATE `poslavu_[1]_db`.`tables` set `_deleted`='1' WHERE `id`='[2]' ",admin_info('dataname'), $args['room_id']);
			if(lavu_dberror())
				echo lavu_dberror();
			else
				echo "success";
				//load_room();
		}else{
			echo "Error: No Room ID sent.";
		}
	}
	function load_room($args){
		global $loc_id;
		if(isset($args['room_id'])){
			lavu_connect_dn(admin_info('dataname'),'poslavu_'.admin_info('dataname')."_db");
			$query=lavu_query("SELECT `tables_json`,`title` FROM `poslavu_[1]_db`.`tables` WHERE `loc_id`='[2]' AND `_deleted` !='1' AND `id`='[3]' ",admin_info('dataname'),$loc_id, $args['room_id']);
			if(mysqli_num_rows($query)){
				$result= mysqli_fetch_assoc($query);
				echo rawurlencode("{\"id\":\"{$args['room_id']}\",\"title\":\"{$result['title']}\", \"tables\":{$result['tables_json']} }");
			}else
				echo "Error: Room with id: ".$args['room_id']." could not be found.";
		}else{	//load first
			$query=lavu_query("SELECT `tables_json` FROM `tables` WHERE `loc_id`='[1]', AND `_deleted` !='1' ",$loc_id);
			if(mysqli_num_rows($query)){
				$result= mysqli_fetch_assoc($query);
				echo "{'id':'$id', 'tables': {$result['tables_json']} }";
				return;
			}else{
				new_room();
			}
		}
	}
	function new_room(){
		$dataname= admin_info("dataname");
		global $loc_id;
		lavu_connect_dn(admin_info('dataname'),'poslavu_'.admin_info('dataname')."_db");
		$query= lavu_query("INSERT INTO `poslavu_[1]_db`.`tables` (`loc_id`, `_deleted`, `title`,`tables_json`) VALUES ('[2]', '0', 'New Room', '[3]') ",$dataname,$loc_id, json_encode(array()));
		$id= lavu_insert_id();
		echo json_encode(array("id"=>$id,"title"=>"New Room","tables"=>array()));
	}
	function clone_room($args){
		global $loc_id;
		lavu_connect_dn(admin_info('dataname'),'poslavu_'.admin_info('dataname')."_db");
		$query=lavu_query("INSERT INTO `poslavu_[1]_db`.`tables` (`tables_json`,`title`,`loc_id`) VALUES ('[2]', '[3]', '[4]') ",admin_info('dataname'),rawurldecode($args['tables']), $args['title'],$loc_id);
	}
	function get_rooms(){
		
		global $loc_id;
		lavu_connect_dn(admin_info('dataname'),'poslavu_'.admin_info('dataname')."_db");
		$query = lavu_query("SELECT * FROM `poslavu_[1]_db`.`tables` where `loc_id`='[2]' AND `_deleted`!='1' ",admin_info('dataname'), $loc_id);
		$return_arr= array();
		while($result = mysqli_fetch_assoc($query)){
			if($result['tables_json']==''){
				$result['tables_json']=convert_tables($result);
			}
			$return_arr[]= array("id"=>$result['id'],"title"=>$result['title'],"tables"=>$result['tables_json']);
		}
		if( sizeof($return_arr))
			echo rawurlencode(json_encode($return_arr));
		else{
			echo rawurlencode(json_encode(array(array("id"=>"-1","title"=>"New Room","tables"=>"[]"))));
		}
	}
	function get_revenue_centers() {
		lavu_connect_dn(admin_info('dataname'),'poslavu_'.admin_info('dataname')."_db");
	    $query = lavu_query("SELECT `name`, `id` FROM `poslavu_[1]_db`.`revenue_centers` WHERE `_deleted` = '0' ORDER BY `name`",admin_info('dataname'));
	    $return_arr=array();
		if(mysqli_num_rows($query)){
		    while($result= mysqli_fetch_assoc($query)){
		   		$return_arr[]= $result;
			}
			$return_arr[]= array("name"=>"New Rev Center","id"=>-1);
			echo rawurlencode(json_encode($return_arr));
	    }else{
	   		echo json_encode(array());
	  	}
    }
    function convert_tables($result){
	    $x_cords	 = explode("|",$result['coord_x']);
	    $y_cords	 = explode("|",$result['coord_y']);
	    $shapes 	 = explode("|",$result['shapes']);
	    $widths 	 = explode("|",$result['widths']);
	    $heights	 = explode("|",$result['heights']);
	    $names  	 = explode("|",$result['names']);
	    $rev_centers = explode("|",$result['rev_centers']);
	    $length		 = count($x_cords);
	    $json_string ='';
	    $table_arr   = array();
	   
	    for($i=0; $i < $length; $i++){
	    	$img= get_img_name($shapes[$i]); 
		    $table_arr[]="{\"id\":\"{$names[$i]}\", \"table_type\":\"{$shapes[$i]}\", \"table_img\":\"$img\",\"x\":\"{$x_cords[$i]}\",\"y\":\"{$y_cords[$i]}\",\"width\":\"{$widths[$i]}\",\"height\":\"{$heights[$i]}\",\"valid\":\"false\",\"selected\":\"false\",\"name\":\"{$names[$i]}\",\"rev_center\":\"{$rev_centers[$i]}\"}";
	    }
	    $tables_json= "[". implode(",", $table_arr)."]";
	    lavu_connect_dn(admin_info('dataname'),'poslavu_'.admin_info('dataname')."_db");
	    $query=lavu_query("UPDATE `poslavu_[1]_db`.`tables` SET `tables_json`='[2]' where `id`='[3]' LIMIT 1",admin_info('dataname'), $tables_json, $result['id']);
		if( lavu_dberror())
			echo "Error: ".lavu_dberror();
		else
			return $tables_json;
    }
    function get_img_name($table){
		return "images/table_$table.png";
    }
    function convert_to_old($json_string, $room_id){
		//echo "Error: ";
		require_once($_SERVER['DOCUMENT_ROOT']."/cp/objects/json_decode.php");	   
	    $json= new Json();
	    $decoded= $json->decode($json_string);
	    $x_cords	='';
	    $y_cords	='';
	    $shapes 	='';
	    $widths 	='';
		$heights	='';
		$names  	='';
		$rev_centers='';	
		$arr= array();
		$arr['dn']=admin_info('dataname');    
	    foreach($decoded as $json_obj){
		
			if($json_obj){
				$arr['x']		.=$json_obj->x		   ."|";
				$arr['y']		.=$json_obj->y		   ."|";
				$arr['shapes'] 	.=$json_obj->table_type."|";
				$arr['widths'] 	.=$json_obj->width	   ."|";			
				$arr['heights']	.=$json_obj->height    ."|";
				$arr['names']	.=$json_obj->name      ."|";
				if(!$json_obj->rev_center)
					$json_obj->rev_center='-1';
				$arr['rev_cent'].=$json_obj->rev_center."|";
			}
	    }
		$arr['room_id']=$room_id;
		if($arr){
			lavu_connect_dn(admin_info('dataname'),'poslavu_'.admin_info('dataname')."_db");
			lavu_query("UPDATE `poslavu_[dn]_db`.`tables` SET `coord_x`='[x]', `coord_y`='[y]',`shapes`='[shapes]', `widths`='[widths]',`heights`='[heights]',`names`='[names]',`revenue_centers`='[rev_cent]' 
			WHERE `id`='[room_id]' LIMIT 1",$arr);
		}
		if(lavu_dberror())
			echo "Error: ".lavu_dberror();
		else 
			echo "success";
    }
    function move_race($args){
	    $event_id= $args['event_id'];
    }
    function check_location_id($rdb, $locationid) {
		if ((int)$locationid == 0 && trim($rdb) !== '') {
			preg_match("/poslavu_(.*)_db/", $rdb, $data_name);
			lavu_connect_dn($data_name[1],$rdb);
			$query = lavu_query("SELECT `id` FROM `[1]`.`locations` WHERE `_disabled` = '0' LIMIT 1", $rdb);
			if ($query !== FALSE && mysqli_num_rows($query) > 0) {
				$query_read = mysqli_fetch_assoc($query);
				$locationid = $query_read['id'];
				set_sessvar('locationid', $locationid);
			}
		}
		
		return $locationid;
	}
?>
