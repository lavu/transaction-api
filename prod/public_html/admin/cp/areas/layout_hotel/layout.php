<?php
	ini_set("display_errors",0);
?>
<!DOCTYPE html>
<div class='style_container'>
	
<style scoped>
	*{
		font-family: sans-serif, verdana;
		font-weight:200;
	}
	canvas {
    	border: 1px solid #ccc;
	}
	.table_layout_container{
		width:100%;
		position:relative;
		height:100%;
		left: -12px;
	}
	.table_layout_positioner{
		width: 988px;
		height:763px;
		margin:0 auto;
		
	}
	.table_layout_side_bar{
		width: 86px;
		height: 700px;
		left: 6px;
		position: absolute;
		display: inline-block;
		border: 1px solid black;
	}
	.table_layout_top_bar{
		width:985px;
		height:60px;
		border:1px solid black;
		background-color:#d0d0d0;
		
	}
	#table_layout_room{
		border: 1px solid black;
		width: 969px;
		left: -3px;
		height: 1000px;
		position: relative;
		display: inline-block;
		
	}
	#canvas_background_img{
		background-image:url(images/table_layout_stripes.gif);
		background-size: contain;
		width: 969px;
		height:1000px;
	}

	.btn{
		font-size:small;
	}
	.btn:hover{

		cursor:pointer;
	}
	.save_btn{
		font-size:larger;
	}
	.table_layout_options_container{
		margin-top:15px;
		float:right;
	}
	.new_table{
		width:70px;
		height:70px;
		padding-bottom:10px;
		position:relative;
		margin:0 auto;
	}
	.added_table{
		position:absolute;
		width:70px;
		height:70px;
	}
	.square{
		background:url("images/table_square.png") no-repeat;
		background-size: 70px,70px;
		margin-top:10px;
	}
	.circle{
		background:url("images/table_circle.png") no-repeat;
		background-size: 70px,70px;
		
	}
	.diamond{
		background:url(images/table_diamond.png) no-repeat;
		background-size: 70px,70px;
	}
	.left_slant{
		background:url(images/table_slant_left.png) no-repeat;
		background-size: 70px,70px;
	}
	.right_slant{
		background:url(images/table_slant_right.png) no-repeat;
		background-size: 70px,70px;
	}
	#square_img, #circle_img, #diamond_img, #left_slant_img, #right_slant_img{
		display:none;
					
	}
	.delete_table{
		width:35px;
		height:35px;
		padding-bottom:10px;
		margin:0 auto;
	}
	#trash_can{
		background:url(images/deleteTableIcon.png) no-repeat;
		background-size: 35px,35px;
	}
	#trash_can:hover{
		background:url(images/deleteTableIconHover.png) no-repeat;
		cursor:pointer;
	}
	#selection_canvas{
		background-color:rgba(0,0,0,.1);
		border:1px solid black;
		display:none;
	}#rename_table{
		position:absolute; 
		display:none;
		z-index:100;
	}
	#canvas_container{
		width: 902px;
		/*left: 90px;*/
		position: absolute;
		background-color: white;
	}
	#rev_center_overlay_container{
		position:fixed;
		top:0px; 
		left:0px;
		background-color:rgba(0,0,0,.6);
		width:100%;
		height:100%;
	}
	.rev_center_overlay{
		position:relative;
		top:30px;
		background-color:white;
		margin:0 auto;
		width:900px;
		height:600px;
	}
	#loading_container{
		display :none;
		z-index:300;
		position:absolute;
		left   :0px;
		top	   :0px;
		width  :100%;
		height :100%;
	}
	.loading_box{
		position: relative;
		display: table;
		margin: 0 auto;
		top: 200px;
		width: 200px;
		height: 100px;
		border: 1px solid black;
		border-radius: 5px;
		background-color: #a8a7a7;
		color: white;
	}.close_btn{
		float:right;
		border:1px solid black;
		padding:3px;
		margin-top:5px;
		margin-right:5px;
	}.close_btn:hover{
		background-color:black;
		color:white;
		cursor:pointer;
	}
	.name_overlay{
		display:none;
		position:absolute;
		z-index:100;
		min-width:60px;
		background-color:white;
	}.name_overlay_input{
		width:100%;
		height:100%;
	}
</style>
	<div id='loading_container'>
		<div class='loading_box'>Loading...</div>
	</div>
	<div class='table_layout_container'>
		<div class='table_layout_positioner'>
			<div class='table_layout_top_bar'>
				<span class='table_layout_options_container'>
					<?= help_mark_tostring(657); ?>
					<input type='button' class='btn save_btn' value='<?php echo speak("Save");?>' onclick='save_room()'/>
					<input type='button' class='btn' value='<?php echo speak("Delete Room");?>' onclick='delete_room()'/>
					<input type='button' class='btn' value='<?php echo speak("Delete Selected Table");?>' onclick='delete_table(event)'/>
					<input type='button' class='btn' value='<?php echo speak("New Room");?>'  onclick=' if(confirm("Are you sure you want to create a new room?")){ new_room(); } '/>
					Room: 
					<select id='room_select' onChange="change_room(document.getElementById('room_select').value)">
						<option value='' name=''>Select A Room</option>
					</select>
					Room Title: <input type='text'  id='room_tit' class='room_title' placeholder="Room Title" style='width:70px;'/>
					
					<input type='button' class='btn' value='Clone Table'  onclick='clone_table()'/>
					<input type='button' class='btn' value='Clone Room'  onclick='clone_room()'/>
				</span>
			</div>

			<div id='canvas_container'>
				<div class= 'name_overlay'>
					<input type='text' class='name_overlay_input' placeholder = 'Add A Title' value = '' onkeyup="if(event.keyCode==13){save_name(document.getElementsByClassName('name_overlay_input')[0].value)}" />
				</div>
				<canvas id="c" width="986" height="700" >Your browser does not support Canvases'. Please update your browser to use table layout.  </canvas>
				
			</div>
		</div>
			<div id='debug' ></div>			
		</div>		
	</div>
</div>

<script type='text/javascript' src='http://code.jquery.com/jquery.min.js'></script>
<script type='text/javascript' src = 'areas/layout_hotel/fabric/dist/fabric.js' ></script>
<script type='text/javascript' src = 'areas/layout_hotel/layout.js' ></script>