<?php
	if($in_lavu)
	{
		$lavu_only = "<font color='#588604' style='font-size:14px'><b>*</b></font> ";

		$forward_to = "index.php?mode={$section}_{$mode}";
		$tablename = "locations";
		$rowid = $locationid;
		$fields = array();

		$notice_times = array();
		$notice_times['5'] = "5 minutes";
		$notice_times['10'] = "10 minutes";
		$notice_times['15'] = "15 minutes";
		$notice_times['20'] = "20 minutes";
		$notice_times['25'] = "25 minutes";
		$notice_times['30'] = "30 minutes";
		$notice_times['35'] = "35 minutes";
		$notice_times['40'] = "40 minutes";
		$notice_times['45'] = "45 minutes";
		$notice_times['50'] = "50 minutes";
		$notice_times['55'] = "55 minutes";
		$notice_times['60'] = "1 hour";
		$notice_times['120'] = "2 hours";
		$notice_times['180'] = "3 hours";
		$notice_times['240'] = "4 hours";
		$notice_times['300'] = "5 hours";
		$notice_times['360'] = "6 hours";
		$notice_times['420'] = "7 hours";
		$notice_times['480'] = "8 hours";
		$notice_times['540'] = "9 hours";
		$notice_times['600'] = "10 hours";
		$notice_times['660'] = "11 hours";
		$notice_times['720'] = "12 hours";
		$notice_times['780'] = "13 hours";
		$notice_times['840'] = "14 hours";
		$notice_times['900'] = "15 hours";
		$notice_times['960'] = "16 hours";
		$notice_times['1020'] = "17 hours";
		$notice_times['1080'] = "18 hours";
		$notice_times['1140'] = "19 hours";
		$notice_times['1200'] = "20 hours";
		$notice_times['1260'] = "21 hours";
		$notice_times['1320'] = "22 hours";
		$notice_times['1380'] = "23 hours";
		$notice_times['1440'] = "1 day";
		$notice_times['2880'] = "2 days";
		$notice_times['4320'] = "3 days";

		// supported phone carriers for LTG
		$phone_carriers = array();
		$phone_carriers['att'] = "AT&T";
		$phone_carriers['sprint'] = "Sprint";
		$phone_carriers['tmobile'] = "T-Mobile";
		$phone_carriers['verizon'] = "Verizon";
		
		$lavu_only = "<font color='#588604' style='font-size:14px'><b>*</b></font> ";

		$fields[] = array("Silvo","Silvo","seperator");
		$fields[] = array(speak("Enable")." Silvo:","config:use_lavu_togo","bool");
		$fields[] = array("ToGo Type","config:use_togo_type","hidden","defaultvalue:Silvo");		
		$fields[] = array("Earliest Pickup Time:","config:lavu_togo_start_time","time_no_late_night");
		$fields[] = array("Latest Pickup Time:","config:lavu_togo_end_time","time_no_late_night");
		$fields[] = array("Days Open", 'config:lavu_togo_days_open', 'day_of_week');
		$fields[] = array("Number of days to allow orders in advance:","config:max_advance_days","select","0,1,2,3,4,5,6,7");
		$fields[] = array(speak("Minimum preparation time for order fulfillment").":","config:minimum_notice_time","select",$notice_times);
		$fields[] = array("Enable in-app order polling:","config:enable_togo_polling","bool");
		$fields[] = array(speak("Minutes prior to pickup time to have to go orders print automatically").":","config:auto_print_minutes","select","5,10,15,20,25,30,35,40,45,50,55,60");			
		if (is_lavu_admin()) $fields[] = array($lavu_only."Enable order fetching for local server:","config:lls_fetch_api_orders","bool");
		$fields[] = array(speak("Submit"),"submit","submit");

		require_once(resource_path() . "/form.php");
	}
?>