<?php

// check for ssl connection
if ($_SERVER["HTTPS"] == "") {
	$user = $_REQUEST["user"];
	echo "<script type='text/javascript'>window.location.replace('?widget=force_change_password&user={$user}');</script>";
}

// find the user to change their password
$a_users = ConnectionHub::getConn('rest')->getAllInTable("users", array("id"=>$_REQUEST["user"]), FALSE);
if (count($a_users) == 0) {
	echo "<style>body { background-color:#eee; }</style><div style='width:300px; display:table; margin:0 auto; background-color:white; border:2px solid #666; margin-top:200px; border-radius:5px; padding:15px;'>Your user can't be found on our servers. Please <a href='index.php'>click here</a> to continue.</div>";
	return;
}
if ($a_users[0]["username"] != admin_info("username")) {
	echo "<style>body { background-color:#eee; }</style><div style='width:300px; display:table; margin:0 auto; background-color:white; border:2px solid #666; margin-top:200px; border-radius:5px; padding:15px;'>Invalid user account. Please <a href='index.php'>click here</a> to continue.</div>";
	return;
}

if (isset($_POST["password"])) {
	if ($_POST["password"] == "") {
		echo "<span style='color:red;'>Error: a new password must be entered to continue.</span>";
	}
	else if ($_POST["password"] != $_POST["confirm_password"])
	{
		echo "<span style='color:red;'>Error: passwords don't match.</span>";
	}
	else
	{
		echo "<span style='color:green;'>Success!</span> You are being redirected back to the control panel. If your page doesn't refresh in a few seconds <a href='index.php'>click here</a>.";
		lavu_query("UPDATE `users` SET `password`=PASSWORD('[password]') WHERE `id`='[id]'", array("password"=>$_POST["password"], "id"=>$a_users[0]["id"]));
		require_once(dirname(__FILE__)."/../auto_login.php");
		$o_autoLogin->setUserCanAutoLogin($data_name, $a_users[0]["id"], FALSE);
		echo "<script type='text/javascript'>window.location.replace('index.php');</script>";
		return;
	}
}

?>

<style type="text/css">
body {
	background-color:#eee;
}
</style>

<div style='width:300px; display:table; margin:0 auto; background-color:white; border:2px solid #666; margin-top:200px; border-radius:5px; padding:15px;'>
	Welcome, <span style="font-weight:bold;"><?php echo $a_users[0]["username"]; ?></span>!<br />Please change your password to something more easily remembered:<br /><br />
	<form action="?widget=force_change_password" method="POST" style="display:table; text-align:right;">
		<input type="hidden" name="user" value="<?php echo $a_users[0]["id"]; ?>"></input>
		New Password: <input type="password" name="password"></input><br />
		Confirm: <input type="password" name="confirm_password"></input><br />
		<input type="submit" value="Continue" />
	</form>
</div>
