<?php
session_start();
require_once (dirname(dirname(dirname(__FILE__))) ."/cp/resources/core_functions.php");

$data_name = sessvar('admin_dataname');
$code = $_REQUEST['code'];
$symbol = $_REQUEST['symbol'];
$secondvalue_query=lavu_query("select * from config where setting='primary_currency_code'");
$currency_code_data = mysqli_fetch_assoc($secondvalue_query);
$currency_code = $currency_code_data['value2'];
if ($currency_code==$code) {
		echo "failure"; exit;
} else {
	$query = " SELECT monetary_symbol,currency_separator,alphabetic_code  FROM `poslavu_MAIN_db`.`country_region`  WHERE id=[1] AND active=[2]  LIMIT 1 ";
	$result = lavu_query($query, $code, 1);
	$currency_data= mysqli_fetch_assoc($result);
	$select_query=lavu_query("select * from config where setting='enable_secondary_currency'");
	if (mysqli_num_rows($select_query)) {
		lavu_query("UPDATE `config` set `value` = '1' where `setting` = 'enable_secondary_currency'");
	} else {
		lavu_query("INSERT INTO `config` (`location`,`setting`,`value`,`type`,`_deleted`) VALUES ('1','enable_secondary_currency','1','location_config_setting','0')");
	}
	$secondary_query=lavu_query("select * from config where setting='secondary_currency_code'");
	if (mysqli_num_rows($secondary_query)) {
		lavu_query("UPDATE `config` set `value` = '$currency_data[alphabetic_code]',`value2` = '$code' where `setting` = 'secondary_currency_code'");
	} else {
		lavu_query("INSERT INTO `config` (`location`,`setting`,`value`,`value2`,`type`,`_deleted`) VALUES ('1','secondary_currency_code','$currency_data[alphabetic_code]','$code','location_config_setting','0')");
	}
	lavu_query("UPDATE `locations` set `secondary_monitary_symbol` = '$symbol' where `id` = '1'");
	echo "success";
}
?>