<?php
 require_once(dirname(__FILE__)."/../resources/core_functions.php");
 require_once(resource_path()."/lavuquery.php");
 require_once(resource_path() . "/dimensional_form.php");
 session_start($_GET['session_id']);
 $data_name = sessvar('admin_dataname');
 if(isset($_POST) && !empty($_POST)){
  $radiusArray=$_POST['radius'];
  $feesArray=$_POST['fees'];
    $arr=array_combine($radiusArray,$feesArray);
    $json=json_encode($arr);
    $query = "update config set value='$json' where setting='ltg_delivery_radius'";
  $result = lavu_query($query);
  header("Location:".$_SERVER['REQUEST_URI']. "&refresh_for_save=1");
 }
 $display .= "<html>";
 $display .= "<head>";
 $display .= "<meta http-equiv=\"Content-Type\" content=\"text/html;charset=ISO-8859-1\">";
 $display .= "<link rel='stylesheet' type='text/css' href='".$styleSheetPath."'>";
 $display .= "<link rel=\"stylesheet\" href=\"../styles/ios-checkbox.css\" type=\"text/css\" media=\"screen\" charset=\"utf-8\" />
		<link rel=\"stylesheet\" type=\"text/css\" href=\"../styles/styles.css\">
		<link rel=\"stylesheet\" type=\"text/css\" href=\"../styles/info_window.css\">
		<link rel=\"stylesheet\" type=\"text/css\" href=\"../styles/ieStyles.css\">
		<link rel=\"stylesheet\" type=\"text/css\" href=\"../styles/multiaccordion/jquery-ui-1.8.16.greenModifiers.min.css\">
		<link rel=\"stylesheet\" type=\"text/css\" href=\"../styles/multiaccordion/jquery-ui-1.8.16.grayMessages.min.css\">
		<script type=\"text/javascript\" src=\"//ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js\"> </script>
		<script type=\"text/javascript\" src=\"../scripts/multiAccordion.js\"> </script>
		<link rel=\"stylesheet\" type=\"text/css\" href=\"../styles/fontcustom/fontcustom.css\">
		<script type=\"text/javascrpt\" src = \"../scripts/pages/ministries.js\" ></script>
		<script type=\"text/javascript\" src=\"../scripts/customelements.js\"></script>";
 if (isset($_GET['refresh_for_save']) && $_GET['refresh_for_save'] == '1') {
 	$_GET['refresh_for_save'] = null;
 	$display .= "<script type='text/javascript'>window.parent.document.location.reload();</script>";
 }
 $display.='<form name = "form1" method="post" action="">';
// $display.="<h2>Delivery Fees by Distance</h2>";
 $display.="<div><table border='0' id='maintab' style='margin: auto;'>";

 
   $rowCount=1;
   $display_values=array();
   $query = "select value from config where setting='ltg_delivery_radius'";
   $data = lavu_query($query);
   while($row = mysqli_fetch_assoc($data)){

    $display_values = $row;
   }
   $decode=json_decode($display_values['value']);
   $arr=(array)json_decode($display_values['value']);
   foreach($arr as $key=>$value){
    $display.='<tr id="rowCount'.$rowCount.'">';
    $display.="<td> Miles / Delivery fee</td>";
    $display.="<td><select class='1-100' name='radius[]'><option value=".$key." selected>".$key."</option></select></td>";
    $display.="<td>/<input type='text' size='5' value=".$value." id='fees' name='fees[]' oninput='validateNumber(this);' class= 'fees'></td>";
    $display.='<td><a href="javascript:void(0);" class="closeBtn"  onclick="removeRow('.$rowCount.');">X</a></td>';
    $display.="</tr>";
    $rowCount++;
   }
   $display.="</table>";
   $display.="<br>";
   $display.="<input type='hidden' name='addcustom'>
<div id='list_addcustom'></div>
      <div style='padding-top:5px;text-align:center;'>
      <input type='button' value='Add More >>' id='add'>
      </div>";
   $display.="<br>";
   $disp = '';
   if(empty($arr)){
      $disp = 'style="display:none"';
   }
   $display .= '<div style="text-align:center;"><input type="button" '.$disp.' value="'.speak("Save").'" name="sub" class="saveBtn"><br></div>';
   $display.="</form>";
  
   echo $display;
?> 

<script type="text/javascript">
if(document.getElementById("fees") !=null){
  var validNumber = new RegExp(/^\d*\.?\d*$/);
var lastValid = document.getElementById("fees").value;
function validateNumber(elem) {
  if (!validNumber.test(elem.value)) {
   alert("please enter valid fees number");
    elem.value = '';
  }
}
}
</script>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
 <script>
 $(function(){
  var rowCount = $('#maintab tbody tr').length+1;

   var j=0;
     var $select = $(".1-100");
     for (i=1;i<=100;i++){
         $select.append($('<option></option>').val(i).html(i))
     }
     $("#add").click(function(){
 $("#maintab").append('<tr  id="rowCount'+rowCount+'">'+
   "<td>Miles / Delivery fee</td>"+
   "<td>"+
   "<select class='1-100' name='radius[]'>"+
   "<option value='5' selected>5</otpion>"+
   "</select>"+
   "</td>" +
   "<td>/<input type='text' size='5' value='0', name='fees[]' class= 'fees'>"+
   '</td><td><a href="javascript:void(0);" class="closeBtn" onclick="removeRow('+rowCount+');">X</a></td></tr>');
 //$(this).parent().parent().remove()
   j++;
   rowCount ++;

   var $select = $(".1-100");
     for (i=0;i<=100;i++){
         $select.append($('<option></option>').val(i).html(i))
     } 
     $(".saveBtn").show();
 });
     
 });

 function removeRow(removeNum) {
 $('#rowCount'+removeNum).remove();

 }

 $(document).ready(function() {
 $(".saveBtn").click(function( event ) {
   var radius = [];
   var fees = [];
  $('tbody tr').each(function(){ 
    feesValue = $(this).find('.fees').val();
    radiusValue = $(this).find("select[name*='radius']").val();
    if(feesValue != undefined){
    fees.push(feesValue);
  }
  if(radiusValue != undefined){
    radius.push(radiusValue);
  }
  });
  var uniqueRadius = radius.filter(function(itm, i, radius) {
    return i == radius.indexOf(itm);
});
  var uniquefees = fees.filter(function(itm, i, radius) {
    return i == fees.indexOf(itm);
});
  if(radius.length > 0 && radius.length  != uniqueRadius.length && fees.length  != uniquefees.length && fees.length > 0) {
    alert("Record already exists");
 }
 else{
  $("form[name='form1']").submit();
  event.preventDefault();
 }
 });
});

 </script>