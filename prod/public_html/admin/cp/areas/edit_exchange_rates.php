<?php
	if (!isset($mode))
		if (isset($_GET['mode']))
			$mode = $_GET['mode'];
		else
			$mode = "";
	
	if (isset($_POST['edit']) && $_POST['mode'] == "settings_edit_exchange_rates") {

		$SUBMISSION_PAGE = "resources/submit_exchange_rates.php";
		$RETURN_URL = "http://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];

//		echo speak("Edit Exchange Rates")."<br />"."<br />";
		
		$display = "<div id='exchangeRateFields'><div>Effective Date</div><div>From</div><div>To</div><div>Rate</div></div>";

		$field = "";
		
		$qtitle_category = false;
		$qtitle_item = "Exchange";
		
		$category_tablename = "";
		$inventory_tablename = "`exchange_rates`";
		$category_fieldname = "";
		$inventory_fieldname = "setting";
		$inventory_select_condition = "";
		$inventory_primefield = "setting";
		$category_primefield = "";
		$inventory_orderby = "id";

		$currencyCodes = getCurrencyCodes();

		$vals = array();
		foreach($currencyCodes as $key=>$val){
			$vals[] = $val;
		}

		$location_info = sessvar("location_info",null); //TODO after setting this check if it is isset and handle failure to retrieve (means session isn't active)

		$currencyToQuery = "select * from config where type='location_config_setting' and setting like '%primary_currency_code%' limit 1";
		$currencyToQueryResult = lavu_query($currencyToQuery);
		$currencyToAssocArray = mysqli_fetch_assoc($currencyToQueryResult);
		$currencyTo = array($currencyToAssocArray['value2']);

		if(!isset($currencyTo)){
			$currencyTo = "USD";
		}

		lavu_select_db(sessvar('poslavu_MAIN_db'));
		$qfields = array();
		$qfields[] = array("effective_date", "effective_date", speak("MM/DD/YY"), "input");
		$qfields[] = array("from_currency_code", "from_currency_code", speak("Type"), "select", $vals);
		$qfields[] = array("to_currency_code", "to_currency_code", speak("Type"), "select", $currencyTo); //Set to primary currency code
		$qfields[] = array("rate", "rate", speak("Rate"), "input");
		$qfields[] = array("delete");

		//TODO: it's only grabbing CurrencyTo Fields

		require_once(resource_path() . "/dimensional_form.php");
		$qdbfields = get_qdbfields($qfields);

		$details_column = "";//"Zip Codes";
		$send_details_field = "";//"zip_codes";
		$category_filter_by = "";
		$category_filter_value = "";
		$cfields = "";

		$qinfo = array();
		$qinfo["qtitle_category"] = $qtitle_category;
		$qinfo["qtitle_item"] = $qtitle_item;
		$qinfo["category_tablename"] = $category_tablename;
		$qinfo["inventory_tablename"] = $inventory_tablename;
		$qinfo["category_fieldname"] = $category_fieldname;
		$qinfo["inventory_fieldname"] = $inventory_fieldname;
		$qinfo["inventory_select_condition"] = $inventory_select_condition;
		$qinfo["inventory_primefield"] = $inventory_primefield;
		$qinfo["category_primefield"] = $category_primefield;
		$qinfo["details_column"] = $details_column;
		$qinfo["send_details"] = $send_details_field;
		$qinfo["field"] = $field;
		$qinfo["qsortby_preference"] = "";
		$qinfo['qtitle_category_plural'] = "";
		$qinfo["category_filter_by"] = $category_filter_by;
		$qinfo["category_filter_value"] = $category_filter_value;
		$qinfo["inventory_filter_value"] = "exchange_rates";
		$qinfo['inventory_filter_by'] = "";
		$qinfo["inventory_orderby"] = $inventory_orderby;
		
		$form_name = "exchange_rate_form";
		$RETURN_URL = "http://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];

		$field_code = create_dimensional_form($qfields,$qinfo,$cfields);

//error_log(__FILE__ . " "  . __LINE__ . "SESSSSIOOOOONNNN ".print_r($_SESSION,1)); //TODO remove this error log
		$display .= "<table cellspacing='0' cellpadding='3'><tr><td align='center' style='border:2px solid #DDDDDD;'>";
		$display .= "<form id='$form_name' name='$form_name' method='post' action='$SUBMISSION_PAGE'>";
		$display .= "<input type='hidden' name='posted' value='1'>";
		$display .= "<input type='hidden' name='returnURI' value='$RETURN_URL'>";
		$display .= $field_code;

		$display .= "&nbsp;<br><input type='submit' id='btn_exchange_rate_form' value='".speak("Save")."' style='width:120px'>";

		$display .= "</form>";
		$display .= "</td></tr></table>";
		
		echo $display;
	}

	// returns a dictionary (bills, coins) of denominations, from largest to smallest
	//      eg array("bills"=>array("100","50"...), "coins"=>array("1.00",".50",...))
	// if no denominations exist, it creates the default english denominations
	function get_db_exchange_rates() {
		$den_query = lavu_query("SELECT `value`,`value2` FROM `config` WHERE `setting` = 'exchange_rates' AND `type` = 'location_config_settings'");
		$has_den = FALSE;
		
		if ($den_query)
			if (mysqli_num_rows($den_query) > 0)
				$has_den = TRUE;
		
		$bills = NULL;
		$coins = NULL;
		if ($has_den) {
			$bills = array();
			$coins = array();
			while ($den = mysqli_fetch_assoc($den_query)) {
				if ($den['value'] == "bill") {
					$bills[] = $den['value2'];
				} else if ($den['value'] == "coin") {
					$coins[] = $den['value2'];
				}
			}
		} else {
			$bills = array("100", "50", "20", "10", "5", "2", "1");
			$coins = array("1.00", ".50", ".25", ".10", ".05", ".01");
			foreach($bills as $bill)
				lavu_query("INSERT INTO `config` (`setting`,`type`,`value`,`value2`) VALUES ('exchange_rates', 'location_config_settings', 'bill', '[1]')", $bill);
			foreach($coins as $coin)
				lavu_query("INSERT INTO `config` (`setting`,`type`,`value`,`value2`) VALUES ('exchange_rates', 'location_config_settings', 'coin', '[1]')", $coin);
		}
		
		$retval = array();
		$retval['bills'] = $bills;
		$retval['coins'] = $coins;
		return $retval;
	}

	function getCurrencyCodes(){
		$currency_country=array();

		$default_country_array = ["10","20","32","41","45","47","50","81","117","127","133","161","176","193","229","257","261","259"];


		$currency_country[]=speak("Select Currency Code");
		$query = "SELECT id, alphabetic_code FROM `poslavu_MAIN_db`.`country_region` WHERE id IN ([1])";
		$result = lavu_query($query, implode(",",$default_country_array));
		if(!$result){
			error_log("mysql error in ".__FILE__." mysql error:" . lavu_dberror());
		}
		else{
			while ($region_info = mysqli_fetch_assoc($result)) {
				$currency_country[$region_info['id']] = $region_info['alphabetic_code'];
			}
		}
		return $currency_country;
	}

	echo '<script type="text/javascript">
				(function($) {
					$(document).ready(function() {
						$( "input[name^=\'ic_1_effective_date\']" ).datepicker({ 
							dateFormat: \'mm/dd/y\', changeYear:"true",changeMonth:"true"  
						});
						//var tdElement = $( ".plus" ).parent();
						$( "#add_button1").on( "click", "span", function( event ) {
							$( "input[name^=\'ic_1_effective_date\']" ).datepicker({ 
								dateFormat: \'mm/dd/y\' , changeYear:"true",changeMonth:"true"
							});
						});
						$( "#add_button2").on( "click", "span", function( event ) {
							$( "input[name^=\'ic_1_effective_date\']" ).datepicker({ 
								dateFormat: \'mm/dd/y\' , changeYear:"true",changeMonth:"true"
							});
						});
						$( "#add_button_with_text").on( "click", "span", function( event ) {
							$( "input[name^=\'ic_1_effective_date\']" ).datepicker({ 
								dateFormat: \'mm/dd/y\' , changeYear:"true",changeMonth:"true"
							});
						});
					});
				})(jQuery);
				
			 </script>';
?>
