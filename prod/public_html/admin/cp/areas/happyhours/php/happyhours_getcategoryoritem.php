<?php
/******************************************************************************
 * About this file:
 * This file should be the primary means for the app to get happy hours from
 * the happy hour database. The three primary functions to use are:
 * get_category_happyhours($category_id)
 * get_item_happyhours($category_id, $item_id)
 * get_all_items_from_category_happyhours($category_id)
 *
 * Format needed by the app (aka for RICHARD):
 * The format of happy hours returned are documented in these three functions.
 * The available happy hour properties are price p/(+|-|=)/[0-9]+{(\.|,)[0-9]*}/($|%) and availability a/(a/n).
 *
 * Format in the database:
 * The happyhours table should be self explanatory.
 * In the `happyhour` field of menu_items and menu_categories, the format is:
 * [happyhourID:(e)nabledOR(d)isabledORde(f)ault:property{|}]* (eg 1:e:p/-/20.00/%|7:d:a/n )
 * If an id can't be found in the `happyhour` field for menu_categories, the default is disabled.
 * If an id can't be found in the `happyhour` field for menu_items, the default is category default.
 *
 * @author Benjamin Bean
 * @emal benjamin@poslavu.com
 *****************************************************************************/


	class HappyHoursGetCategoryOrItem {

		function __construct() {
			$this->reset_cache();
		}

		public function reset_cache() {
			$this->cached = (object)array(
				'get_category_happyhour_row'=>array(),
				'get_item_happyhour_row'=>array(),
				'get_category_items_happyhour_rows'=>array(),
				'happyhour_get_the_darn_global_happyhours'=>array()
			);
		}

		/**
		 * get the happy hours that apply at the category level and all of it's items
		 * @param  int     $category_id    the id of the category to load from the database
		 * @param  array   $a_category_row the category row from the database, can be NULL (loads it from the DB)
		 * @param  boolean $b_cached       if true, loads all non-deleted categories from the db and caches the result ($a_category_row must be NULL)
		 * @return string                  the happy hours in the form
		 *     starttime:endtime:weekdays:property (eg 0000:0000:UMTWRFS:p/-/20.00/%)
		 *     where each happyhour is bar-delimeted (eg 0000:0000:UMTWRFS:p/-/20.00/%|0000:0000:UMTWRFS:p/-/20.00/%)
		 */
		public function get_category_happyhours($category_id, $a_category_row = NULL, $b_cached = FALSE) {

			$a_category_row = $this->get_category_happyhour_row($category_id, $a_category_row, $b_cached);

			// get the result
			return $this->get_itemandcategory_happyhours($a_category_row, NULL);
		}

		/**
		 * retrieve the happy hours for the given item
		 * @param  int     $category_id    the id of the category this item belongs to
		 * @param  int     $item_id        the id of the item to retrieve
		 * @param  array   $a_category_hhs the category happy hours, in the form
		 *                                 array(category id=>array("id"=>$hh[0], "mode"=>$hh[1], "property"=>$hh[2]), ...)
		 * @param  array   $a_category_row the category row from the database, can be NULL (loads it from the DB)
		 * @param  array   $a_item_row     the item row from the database, can be NULL (loads it from the DB)
		 * @param  boolean $b_cached       if true, loads all non-deleted categories from the db and caches the result ($a_category_row must be NULL)
		 * @return string                  the happy hours in the form
		 *     starttime:endtime:weekdays:property (eg 0000:0000:UMTWRFS:p/-/20.00/%)
		 *     where each happyhour is bar-delimeted (eg 0000:0000:UMTWRFS:p/-/20.00/%|0000:0000:UMTWRFS:p/-/20.00/%)
		 */
		public function get_item_happyhours($category_id, $item_id, $a_category_hhs = NULL, $a_category_row = NULL, $a_item_row = NULL, $b_cached = FALSE) {

			// get the category happy hours from the passed in row value
			if ($a_category_hhs === NULL) {
				$a_query_results = $this->get_category_happyhour_row($category_id, $a_category_row, $b_cached);
				$a_category_hhs_in = explode("|", $a_query_results['happyhour']);
				if (strlen($a_category_hhs_in[0]) > 0) {
					foreach($a_category_hhs_in as $hh) {
						$a_hh = explode(":", $hh);
						$a_category_hhs[$a_hh[0]] = array("id"=>$a_hh[0], "mode"=>$a_hh[1], "property"=>$a_hh[2]);
					}
				}
			}

			$a_item_query_results = $this->get_item_happyhour_row($item_id, $a_item_row, $b_cached);

			return $this->get_itemandcategory_happyhours($a_item_query_results, $a_category_hhs, $b_cached);
		}

		/**
		 * retrieve the menu item rows from the DB associated with the category
		 * @param  int     $category_id    the id of the category to be loaded
		 * @param  array   $a_category_row the category row from the database, can be NULL (loads it from the DB)
		 * @param  array   $a_item_rows    the item rows from the database for the given category, can be NULL (loads it from the DB)
		 * @param  boolean $b_cached       if true, loads all non-deleted categories and items from the db and caches the result ($a_category_row must be NULL)
		 * @return array                   the happy hours for all menu items in a category in the form
		 *     array(menuitemid=>starttime:endtime:weekdays:property, ...)
		 *     eg array(1=>"0000:0000:UMTWRFS:p/-/20.00/%", ...)
		 */
		public function get_all_items_from_category_happyhours($category_id, $a_category_row = NULL, $a_items_rows = NULL, $b_cached = FALSE) {

			$b_passed_in = ($a_category_row === NULL) ? FALSE : TRUE;

			// get the menu category row
			if ($a_category_row === NULL)
				$a_category_row = $this->get_category_happyhour_row($category_id, NULL, $b_cached);
			if ($a_category_row === NULL)
				return array();

			// get the menu item row
			if ($a_items_rows === NULL)
				$a_items_rows = $this->get_category_items_happyhour_rows($category_id, NULL, $b_cached);
			if (count($a_items_rows) == 0)
				return array();

			$a_category_hhs = array();

			// get the category
			$category_hhs_in = explode("|", $a_category_row['happyhour']);
			if (strlen($category_hhs_in[0]) > 0) {
				foreach($category_hhs_in as $hh) {
					$a_hh = explode(":", $hh);
					$a_category_hhs[$a_hh[0]] = array("id"=>$a_hh[0], "mode"=>$a_hh[1], "property"=>$a_hh[2]);
				}
			}

			// get the menu items
			// $menu_items_query = lavu_query("SELECT `id` FROM `menu_items` WHERE `category_id` = '[1]' AND `_deleted` != '1'", $category_id);
			$menu_items = array();
			foreach($a_items_rows as $menu_item_row) {
				$result = $this->get_item_happyhours($category_id, $menu_item_row['id'], $a_category_hhs, $a_category_row, $menu_item_row, $b_cached);
				if ($result != "") {
					$menu_items[$menu_item_row['id']] = $result;
				}
			}
			if (count($menu_items) > 0)
				return $menu_items;
		}

		/**
		 * retrieve all happy hours from the happy hours table
		 * @param  boolean $b_cached if TRUE stores the returned result for future use
		 * @return array             an array of happy hours, where each happy hour is an array including
		 *     id, name, start_time, end_time, weekdays, property, and _deleted
		 */
		public function happyhour_get_the_darn_global_happyhours($b_cached = FALSE) {

			// check for cached values
			if ($b_cached && count($this->cached->happyhour_get_the_darn_global_happyhours) > 0)
				return $this->cached->happyhour_get_the_darn_global_happyhours;

			// load the happy hours from the database
			$global_hhs_query = lavu_query("SELECT * FROM `happyhours` WHERE `_deleted` != '1'");
			$global_hhs = array();
			if ($global_hhs_query) {
				while ($row = mysqli_fetch_assoc($global_hhs_query)) {
					if (!isset($row['extended_properties']))
						$row['extended_properties'] = "none";
					$global_hhs[] = $row;
				}
			}

			// cache the results
			if ($b_cached)
				$this->cached->happyhour_get_the_darn_global_happyhours = $global_hhs;

			return $global_hhs;
		}

		/**
		 * retrieves the happy hours that apply to the category and all of it's items,
		 * or just the items if $parent_hhs is NULL
		 * @param  array   $a_query_results the category row/item row from the DB that is being requested
		 * @param  array   $parent_hhs      if loading the items only, in the form of
		 *                                  array(category id=>array("id"=>$hh[0], "mode"=>$hh[1], "property"=>$hh[2]), ...)
		 * @param  boolean $b_cached        if TRUE stores the returned result for future use
		 * @return string                   the happy hours in the form
		 *      starttime:endtime:weekdays:property (eg 0000:0000:UMTWRFS:p/-/20.00/%)
		 *      where each happyhour is bar-delimeted (eg 0000:0000:UMTWRFS:p/-/20.00/%|0000:0000:UMTWRFS:p/-/20.00/%)
		 */
		public function get_itemandcategory_happyhours($a_query_results, $a_parent_hhs = NULL, $b_cached = FALSE) {

			// get the global happy hours (to fall back on, default if category not set)
			$global_hhs = $this->happyhour_get_the_darn_global_happyhours($b_cached);

			// get the item or category happy hours
			$a_child_hhs_query = $a_query_results;
			if ($a_child_hhs_query !== NULL)
				$a_child_hhs_in = explode("|", $a_child_hhs_query['happyhour']);
			$a_child_hhs = array();
			if (strlen($a_child_hhs_in[0]) > 0) {
				foreach($a_child_hhs_in as $hh) {
					$a_hh = explode(":", $hh);
					$a_child_hhs[$a_hh[0]] = array("id"=>$a_hh[0], "mode"=>$a_hh[1], "property"=>$a_hh[2]);
				}
			}

			$iscategory = NULL;
			if (isset($a_parent_hhs)) $iscategory = FALSE; else $iscategory = TRUE;

			$happyhours_toreturn = array();
			foreach($global_hhs as $hh) {
				$st = $hh["start_time"];
				$et = $hh["end_time"];
				$days = $hh["weekdays"];
				$prop = $hh["property"];
				$extended = $hh["extended_properties"];

				// set the default for the child (menu item) here
				if (!isset( $a_child_hhs[$hh["id"]] )) {
					$childmode = "";
					if ($iscategory) $childmode = "d"; else $childmode = "f";
					$a_child_hhs[$hh["id"]] = array("id"=>$hh["id"], "mode"=>$childmode, "property"=>"");
				}

				// set the default for the parent (menu category) here
				if (!isset( $a_parent_hhs[$hh["id"]] ) && !$iscategory) {
					$parentmode = "d";
					$a_parent_hhs[$hh["id"]] = array("id"=>$hh["id"], "mode"=>$parentmode, "property"=>"");
				}

				$continue = FALSE;
				switch ($a_child_hhs[$hh["id"]]["mode"]) {
				case "d":
					$continue = TRUE;
					break;
				case "e":
					$prop = $a_child_hhs[$hh["id"]]["property"];
					break;
				case "f":
					if (isset( $a_parent_hhs[$hh["id"]] )) {
						switch ($a_parent_hhs[$hh["id"]]["mode"]) {
						case "d":
							$continue = TRUE;
							break;
						case "e":
							$prop = $a_parent_hhs[$hh["id"]]["property"];
							break;
						case "f":
							break;
						}
					}
					break;
				}
				if ($continue) continue;
				$prop = $this->evaluate_and_modify_happy_hour_property($prop);
				$happyhours_toreturn = array_merge($happyhours_toreturn, $this->modify_for_after_midnight($st, $et, $days, $prop, $extended));
			}
			$retval = implode("|", $happyhours_toreturn);

			return $retval;
		}

		// breaks the happy hour property appart
		// if the property is price modification by percentage and the percentage is <= 1, then the percentage is multiplied by 100 (so that the percentage is actually sane)
		private function evaluate_and_modify_happy_hour_property($prop) {
			$prop_parts = explode("/", $prop);
			if ($prop_parts[0] == "p") {
				if ($prop_parts[3] == "%") {
					if ($prop_parts[2]*1.0 <= 1.0) {
						$prop_parts[2] *= 100;
					}
				}
			}
			$new_prop = implode("/", $prop_parts);
			return $new_prop;
		}

		/**
		 * Modify a happy hour if it continues on past midnight.
		 * @param  string $st       start time
		 * @param  string $et       end time
		 * @param  string $days     week days
		 * @param  string $prop     property
		 * @param  string $extended how it applies to the forced/optional modifiers
		 * @return array            an array of happy hours in the for array("{$st}:{$et}:{$days}:{$prop}:{$extended}", ...)
		 */
		private function modify_for_after_midnight($st, $et, $days, $prop, $extended) {
			if ((int)$et <= 2400) {
				return array("{$st}:{$et}:{$days}:{$prop}:{$extended}");
			}

			$new_et = str_pad((string)((int)$et-2400),4,"0",STR_PAD_LEFT);
			$new_days = "";
			$a_days = array("U", "M", "T", "W", "R", "F", "S");
			for($i = 0; $i < count($a_days); $i++) {
				if (strpos($days, $a_days[$i]) !== FALSE) {
					$new_days .= $a_days[($i+1) % 7];
				} else {
					$new_days .= "_";
				}
			}
			$new_days = substr($new_days, 6, 1).substr($new_days, 0, 6);

			return array(
				"{$st}:2400:{$days}:{$prop}:{$extended}",
				"0000:{$new_et}:{$new_days}:{$prop}:{$extended}"
			);
		}

		/**
		 * treat this as a private function, for use in this file only
		 * retrieves the happy hours row from the DB for the given category
		 * @param  int     $category_id    the id of the category to load from the database
		 * @param  array   $a_category_row the category row from the database, can be NULL (loads it from the DB)
		 * @param  boolean $b_cached       if true, loads all non-deleted categories from the db and caches the result ($a_category_row must be NULL)
		 * @return array                   NULL if the row can't be loaded/found, an array from the DB on success
		 */
		private function get_category_happyhour_row($category_id, $a_category_row = NULL, $b_cached = FALSE) {

			// set some values
			$category_id = (int)$category_id;

			// the row is passed in (no need for mysql calls)
			if ($a_category_row !== NULL && is_array($a_category_row))
				return $a_category_row;

			// caching is requested
			$a_query_result = NULL;
			if ($b_cached) {
				if (count($this->cached->get_category_happyhour_row) == 0) {
					$a_rows = ConnectionHub::getConn('rest')->getDBAPI()->getAllInTable('menu_categories', array('_deleted'=>0), FALSE, array('selectclause'=>'`happyhour`,`id`'));
					for($i = 0; $i < count($a_rows); $i++) {
						$this->cached->get_category_happyhour_row[(int)$a_rows[$i]['id']] = $a_rows[$i];
					}
				}
				if (isset($this->cached->get_category_happyhour_row[$category_id])) {
					$a_query_result = $this->cached->get_category_happyhour_row[$category_id];
				}
			}

			// the cached result is unavailable
			if ($a_query_result === NULL) {
				$query = lavu_query("SELECT `happyhour` FROM `menu_categories` WHERE `id` = '[1]'", $category_id);
				if ($query !== FALSE && mysqli_num_rows($query) > 0)
					$a_query_result = mysqli_fetch_assoc($query);
			}

			return $a_query_result;
		}

		/**
		 * treat this as a private function, for use in this file only
		 * retrieves the happy hours row from the DB for the given item
		 * @param  int     $item_id    the id of the item to load from the database
		 * @param  array   $a_item_row the item row from the database, can be NULL (loads it from the DB)
		 * @param  boolean $b_cached   if true, loads all non-deleted items from the db and caches the result ($a_item_row must be NULL)
		 * @return array               NULL if the row can't be loaded/found, an array from the DB on success
		 */
		private function get_item_happyhour_row($item_id, $a_item_row, $b_cached) {

			// set some values
			$item_id = (int)$item_id;

			// the row is passed in (no need for mysql calls)
			if ($a_item_row !== NULL && is_array($a_item_row))
				return $a_item_row;

			// caching is requested
			$a_query_result = NULL;
			if ($b_cached) {
				if (count($this->cached->get_item_happyhour_row) == 0) {
					$a_rows = ConnectionHub::getConn('rest')->getDBAPI()->getAllInTable('menu_items', array('_deleted'=>0), FALSE, array('selectclause'=>'`happyhour`,`id`'));
					for($i = 0; $i < count($a_rows); $i++) {
						$this->cached->get_item_happyhour_row[(int)$a_rows[$i]['id']] = $a_rows[$i];
					}
				}
				if (isset($this->cached->get_item_happyhour_row[$item_id])) {
					$a_query_result = $this->cached->get_item_happyhour_row[$item_id];
				}
			}

			// the cached result is unavailable
			if ($a_query_result === NULL) {
				$query = lavu_query("SELECT `happyhour` FROM `menu_items` WHERE `id` = '[1]'", $item_id);
				if ($query !== FALSE && mysqli_num_rows($query) > 0)
					$a_query_result = mysqli_fetch_assoc($query);
			}

			return $a_query_result;
		}

		/**
		 * treat this as a private function, for use in this file only
		 * retrieves the happy hours rows from the DB for the given category
		 * @param  int     $category_id    the id of the category to load from the database
		 * @param  array   $a_item_rows    the item rows from the database, can be NULL (loads it from the DB)
		 * @param  boolean $b_cached       if true, loads all non-deleted categories from the db and caches the result ($a_category_row must be NULL)
		 * @return array                   an empty array if the rows can't be loaded/found, an array of arrays from the DB on success
		 */
		private function get_category_items_happyhour_rows($category_id, $a_item_rows = NULL, $b_cached = FALSE) {

			// set some values
			$category_id = (int)$category_id;

			// the row is passed in (no need for mysql calls)
			if ($a_item_rows !== NULL && is_array($a_item_rows))
				return $a_item_rows;

			// caching is requested
			$a_query_results = NULL;
			if ($b_cached) {
				if (!isset($this->cached->get_category_items_happyhour_rows[(int)$category_id])) {
					$this->cached->get_category_items_happyhour_rows[(int)$category_id] = array();
					$a_rows = ConnectionHub::getConn('rest')->getDBAPI()->getAllInTable('menu_items', array('category_id'=>$category_id, '_deleted'=>0), FALSE, array('selectclause'=>'`id`,`happyhour`'));
					for($i = 0; $i < count($a_rows); $i++) {
						$this->cached->get_category_items_happyhour_rows[(int)$category_id][] = $a_rows[$i];
					}
				}
				if (isset($this->cached->get_category_items_happyhour_rows[(int)$category_id])) {
					$a_query_results = $this->cached->get_category_items_happyhour_rows[(int)$category_id];
				}
			}

			// the cached result is unavailable
			if ($a_query_results === NULL) {
				$query = lavu_query("SELECT `id`,`happyhour` FROM `menu_items` WHERE `category_id` = '[1]' AND `_deleted` != '1'", $category_id);
				if ($query !== FALSE) {
					$a_query_results = array();
					while ($row = mysqli_fetch_assoc($query))
						$a_query_results[] = $row;
				}
			}

			return $a_query_results;
		}

		private function my_lavu_query($s_query_string, $wt_query_var = NULL) {
			if (!isset($this->num_queries_old))
				$this->num_queries_old = 0;
			$this->num_queries_old++;
			return lavu_query($s_query_string, $wt_query_var);
		}

		public function num_queries() {
			$num_queries_old = $this->num_queries_old;
			$this->num_queries_old = 0;
			return $num_queries_old;
		}
		public function get_all_details_id_happyhours($happyhours_id, $type = '')
		{
			$where = "";
			if ($type != '') {
				$where = " AND `extended_properties` in ('both', '".$type."')";
			}
			$query = lavu_query("SELECT *  FROM `happyhours` WHERE `id` = '[1]' ".$where." AND `_deleted` != '1'", $happyhours_id);
			$row = mysqli_fetch_assoc($query);
			if(count($row)>0){
				$data=$row[start_time].":".$row[end_time].":".$row[weekdays].":".$row[property];
			}else{ $data="";}
			return $data;
		}
	}

	global $o_happyHoursGetCategoryOrItem;
	$o_happyHoursGetCategoryOrItem = new HappyHoursGetCategoryOrItem();
?>