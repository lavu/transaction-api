<?php

	require_once(dirname(__FILE__)."/../../../resources/lavuquery.php");
	require_once(dirname(__FILE__).'/happyhours_getcategoryoritem.php');
	require_once(dirname(__FILE__).'/happyhours_getcategoryoritem_bk.php');
	lavu_connect_dn('test_pizza_sho', 'poslavu_test_pizza_sho_db');

	?>
	<style type='text/css'>
		table:first-child td {
			border-top: 1px solid black;
		}
		tr:first-child {
			border-left: 1px solid black;
		}
		td {
			border-bottom: 1px solid black;
			border-right: 1px solid black;
		}
		td.success {
			color: green;
		}
		td.failure {
			color: red;
		}
		pre {
			text-align: left;
			overflow: auto;
		}
	</style>
	<?php

	global $o_happyHoursGetCategoryOrItem;
	global $a_category_row;
	global $a_item_rows;
	$a_category_row = ConnectionHub::getConn('rest')->getDBAPI()->getAllInTable('menu_categories', array('_deleted'=>0, 'id'=>18), FALSE);
	$a_item_rows = ConnectionHub::getConn('rest')->getDBAPI()->getAllInTable('menu_items', array('_deleted'=>0, 'category_id'=>18), FALSE);

	global $num_queries_old;
	get_all_items_from_category_happyhours(18);
	echo "original number of queries: {$num_queries_old}"."<br />";
	$o_happyHoursGetCategoryOrItem->reset_cache();
	$o_happyHoursGetCategoryOrItem->get_all_items_from_category_happyhours(18, $a_category_row[0], $a_item_rows, FALSE);
	echo "number of queries w/ passed values: ".$o_happyHoursGetCategoryOrItem->num_queries()."<br />";
	$o_happyHoursGetCategoryOrItem->reset_cache();
	$o_happyHoursGetCategoryOrItem->get_all_items_from_category_happyhours(18, NULL, NULL, TRUE);
	echo "number of queries w/ caching: ".$o_happyHoursGetCategoryOrItem->num_queries()."<br />";
	global $num_runs;

	$num_queries_old = 0;
	$num_runs = 0;
	$o_happyHoursGetCategoryOrItem->reset_cache();
	ob_start();
	run_unit_test(18, 1);
	run_unit_test(18, 1);
	run_unit_test(12, 1);
	$s_trash = ob_get_contents();
	ob_end_clean();
	echo "number of queries after {$num_runs} runs, old way: ".$num_queries_old."<br />";
	echo "number of queries after {$num_runs} runs, passing values: ".$o_happyHoursGetCategoryOrItem->num_queries()."<br />";
	$num_queries_old = 0;
	$num_runs = 0;
	$o_happyHoursGetCategoryOrItem->reset_cache();
	ob_start();
	run_unit_test(18, 2);
	run_unit_test(18, 2);
	run_unit_test(12, 2);
	$s_trash = ob_get_contents();
	ob_end_clean();
	echo "number of queries after {$num_runs} runs, with caching: ".$o_happyHoursGetCategoryOrItem->num_queries()."<br />";
	$num_queries_old = 0;
	$num_runs = 0;
	$o_happyHoursGetCategoryOrItem->reset_cache();
	ob_start();
	run_unit_test(18, 3);
	run_unit_test(18, 3);
	run_unit_test(12, 3);
	$s_trash = ob_get_contents();
	ob_end_clean();
	echo "number of queries after {$num_runs} runs, with caching and passing values: ".$o_happyHoursGetCategoryOrItem->num_queries()."<br />";

	ob_start();
	echo "<h1>First unit test</h1>";
	run_unit_test(18);
	echo "<h1>Second unit test</h1>";
	run_unit_test(18);
	echo "<h1>Third unit test</h1>";
	run_unit_test(12);
	$s_unit_test = ob_get_contents();
	ob_end_clean();

	echo $s_unit_test;

	function run_unit_test($category_id, $i_testing = 0) {
		global $num_runs;
		global $o_happyHoursGetCategoryOrItem;
		$a_category_row = ConnectionHub::getConn('rest')->getDBAPI()->getAllInTable('menu_categories', array('_deleted'=>0, 'id'=>$category_id), FALSE);
		$a_item_rows = ConnectionHub::getConn('rest')->getDBAPI()->getAllInTable('menu_items', array('_deleted'=>0, 'category_id'=>$category_id), FALSE);

		echo "<table>";

		for ($i = 0; $i < 2; $i++) {
			for ($j = 0; $j < 2; $j++) {
				for ($k = 0; $k < 2; $k++) {
					$a_category_row_test = (($i && ($i_testing == 0 || $i_testing == 1)) || $i_testing == 3) ? $a_category_row[0] : NULL;
					$a_item_rows_test = (($j && ($i_testing == 0 || $i_testing == 1)) || $i_testing == 3) ? $a_item_rows : NULL;
					$b_cache = (($k && ($i_testing == 0 || $i_testing == 2)) || $i_testing == 3) ? TRUE : FALSE;
					run_test(($i ? "yes" : "no")." pass category, ".($j ? "yes" : "no")." pass item, ".($k ? "yes" : "no")." cache", print_r(get_all_items_from_category_happyhours($category_id),TRUE),
						print_r($o_happyHoursGetCategoryOrItem->get_all_items_from_category_happyhours($category_id, $a_category_row_test, $a_item_rows_test, $b_cache),TRUE));
					$num_runs++;
				}
			}
		}

		echo "</table>";
	}

	function run_test($s_name, $s_expected, $s_result) {
		if ($s_expected !== $s_result) {
			echo "<tr><td class='failure'>FAIL: {$s_name}</td><td><pre>{$s_expected}</pre></td><td><pre>{$s_result}</pre></td></tr>";
		} else {
			echo "<tr><td class='success'>SUCCESS: {$s_name}</td><td><pre>{$s_expected}</pre></td><td><pre>{$s_result}</pre></td></tr>";
		}
	}

?>