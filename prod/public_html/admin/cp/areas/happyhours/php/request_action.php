<?php

//ini_set("display_errors", "1");

/*~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~
 * about happy hours
 *~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~*/
/* Overview
 * Happy hours are used to apply specific properties to menu items/categories based on the
 * the day of the week and time of day.
 *
 * Available properties:
 * At the time of writing this description, available properties to apply are "price modifiers" and "availability".
 * 
 * Inheritance:
 * Menu items inherit the happy hour properties of menu categories, and
 * menu categories inherit the happy hour properties of the global happy hours.
 * Categories and items can override the happy hour properties of their parent, and even
 * enable or disable specific happy hours.
 * 
 * Format:
 * The format of the `happyhour` column in menu_items and menu_categories is:
 * happyhourid:(e)nabledOR(d)isabled:property, eg 7:e:p/-/20.00/%
 * The column is also bar-delimeted, eg 1:d:a/n|7:e:p/-/20.00/%
 * If an id for a happy hour can't be found in the `happyhour` column, then the happy hour is
 * assumed to default to the parent value.
 * 
 * To get the happy hours of a menu category:
 * make a post to this file with:
 * action="get_category_happyhours"
 * category=id of the category
 * OR include the file "happyhours_getcategoryoritem.php"
 * 
 * To get the happy hours of a menu item:
 * make a post to this file with:
 * action="get_item_happyhours"
 * item=id of the item
 * cateegory=id of the category
 * OR include the file "happyhours_getcategoryoritem.php"
 */

// returns enabled or disabled and the happy hour property, in color-delimeted form (eg e:p/-/20.00/% or d:a/n)
function get_global_property($happyhourid) {
    global $location_info;// add for decimal character format,LP-4194
    $decimal=$location_info['decimal_char'];
	$hh_query = lavu_query("SELECT `property` FROM `happyhours` WHERE `id` = '[1]' LIMIT 1", $happyhourid);
	if ($hh_query) {
		while ($row = mysqli_fetch_assoc($hh_query)){
		    if($decimal == ','){// START LP-4194
		        $row['property']=str_replace('.',',',$row['property']);
		    }//END LP-4194
			return array( TRUE, "success?e:".$row['property'] );
		}
	}
	
	return array( FALSE, "database_error?Happy hour ".$happyhourid." could not be found in the database" );
}

// returns enabled or disabled and the happy hour property, in color-delimeted form (eg e:p/-/20.00/% or d:a/n)
// if the category doesn't have any defaults for that happyhour, then it defaults back to the global value
function get_category_property($categoryid, $happyhourid) {
	$retval = "";

	$category_happyhours = "";
	$category_happyhours_query = lavu_query("SELECT `happyhour` FROM `menu_categories` WHERE `id` = '[1]' LIMIT 1", $categoryid);
	if ($category_happyhours_query) {
		while ($row = mysqli_fetch_assoc($category_happyhours_query)) {
			$category_happyhours = $row['happyhour'];
		}
	}
	
	if ($category_happyhours != "") {
		$hhs = explode("|", $category_happyhours);
		foreach($hhs as $hh) {
			if (strlen($hh) > 0) {
				$hh = explode(":", $hh);
				if ($hh[0] != $happyhourid || $hh[1] == 'f')
					continue;
				$retvals = array();
				for ($i = 1; $i < count($hh); $i++)
					$retvals[] = $hh[$i];
				return array( TRUE, "success?".implode(":",$retvals) );
			}
		}
	}
	
	return get_global_property($happyhourid);
}

function main() {
	function check_required($arrayofvals) {
		for ($i = 0; $i < count($arrayofvals); $i++) {
			if ($arrayofvals[$i][1] === "") {
				echo "missingargument?Please provide a value for ".$arrayofvals[$i][0];
				return FALSE;
			}
		}
		return TRUE;
	}

	function get_post($var_name) {
		if (isset($_POST[$var_name]))
			return array($var_name, $_POST[$var_name]);
		return array($var_name, "");
	}

	$action = get_post("action");
	$categoryid = get_post("category");
	$itemid = get_post("item");
	$happyhourid = get_post("happyhour_id");
	$result = array( FALSE, "missingLocation?Location of site not known" );
	
	switch ($action[1])	{
	case "getcategory_hh_property":
		if (!check_required(array($categoryid, $happyhourid))) return;
		$result = get_category_property($categoryid[1], $happyhourid[1]);
		break;
	case "getglobal_hh_property":
		if (!check_required(array($happyhourid))) return;
		$result = get_global_property($happyhourid);
		break;
	case "get_category_happyhours":
		if (!check_required(array($categoryid))) return;
		require_once("happyhours_getcategoryoritem.php");
		$result = array( TRUE, get_category_happyhours($categoryid[1]) );
		break;
	case "get_item_happyhours":
		if (!check_required(array($categoryid, $itemid))) return;
		require_once("happyhours_getcategoryoritem.php");
		$result = array( TRUE, get_item_happyhours($categoryid[1], $itemid[1], NULL) );
		break;
	case "get_all_menu_items_happyhours":
		if (!check_required(array($categoryid))) return;
		require_once("happyhours_getcategoryoritem.php");
		$hhs = get_all_items_from_category_happyhours($categoryid[1]);
		$result = array( TRUE, implode("|", $hhs) );
		break;
	default:
		$result = array( FALSE, "badRequest?Unrecognized request from client" );
	}
	
	if (gettype($result) == "array" && count($result) > 1) {
		if ($result[0] == TRUE && $result[1] == "success") {
			echo "success";
		} else {
			echo $result[1];
		}
	} else {
		echo "failure";
	}
}

main();

?>
