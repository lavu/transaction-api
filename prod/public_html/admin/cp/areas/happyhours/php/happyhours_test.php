<script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>

<script type="text/javascript">
	send_get_happy_hours_retval = null;
	function send_get_happy_hours(category, item, get_all_menu_items) {
	  	var rand = Math.ceil(Math.random()*2);
	  	send_get_happy_hours_retval = false;
	  	var action = null;
	  	if (item) action = "get_item_happyhours"; else action = "get_category_happyhours";
	  	if (get_all_menu_items) action = "get_all_menu_items_happyhours";
	  	
	  	$.ajax({
	  		url: "index.php?widget=happyhours/php/request_action",
	  		type: "POST",
	  		async: false,
	  		cache: false,
	  		data: { action: action, category: category, item: item, random_val: rand },
	  		
	  		success: function (string){
	  			$("#results_font").html(string.replace("success",""));
	  			$("#results_font_broken_bar").html(string.replace("success","").replace(/\|/g,"<br />"));
	  		    if( string.indexOf('success') !== -1 ){
	  		    	send_get_happy_hours_retval = true;
	  		    } else {
		  		    send_get_happy_hours_retval = false;
	  		    }
	  		},
	  		
	  		error: function(j ,t, e){
				send_get_happy_hours_retval = false;
			}
	  	
	  	});
	  	
	  	return send_get_happy_hours_retval;
	}
	
	function get_happy_hours_item() {
		categoryid = $("#categoryidfield1").val();
		itemid = $("#itemidfield1").val();
		send_get_happy_hours(categoryid, itemid, false);
		console.log(".....");
	}
	
	function get_happy_hours_category() {
		categoryid = $("#categoryidfield2").val();
		send_get_happy_hours(categoryid, null, false);
		console.log(".....");
	}
	
	function get_all_menu_item_happy_hours_category() {
		categoryid = $("#categoryidfield3").val();
		send_get_happy_hours(categoryid, null, true);
		console.log(".....");
	}
</script>

happy hour settings for menu item --> categoryid: <input type='text' id='categoryidfield1' value='115'> itemid: <input type='textfield' id='itemidfield1' value='826'><input type='button' onclick='get_happy_hours_item();' value='submit'><br />
happy hour settings for category --> categoryid: <input type='text' id='categoryidfield2' value='115'> <input type='button' onclick='get_happy_hours_category();' value='submit'><br />
happy hour settings for all menu items in category --> categoryid: <input type='text' id='categoryidfield3' value='115'> <input type='button' onclick='get_all_menu_item_happy_hours_category();' value='submit'><br />
<br />
<font style='font-weight:bold'>results: </font><br /><div style='overflow-x:scroll;width:800px;'><font id='results_font'></font></div><br />
<br />
<font style='font-weight:bold'>results, broken by bars "|": </font><br /><font id='results_font_broken_bar'></font>