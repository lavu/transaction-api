<?php

	require_once(dirname(__FILE__)."/../../../resources/lavuquery.php");
	require_once(dirname(__FILE__).'/happyhours_getcategoryoritem.php');
	lavu_connect_dn('test_pizza_sho', 'poslavu_test_pizza_sho_db');

	?>
	<style type='text/css'>
		td.first-row {
			border-top: 1px solid black;
		}
		td.first-column {
			border-left: 1px solid black;
		}
		td {
			border-bottom: 1px solid black;
			border-right: 1px solid black;
		}
		td.success {
			color: green;
		}
		td.failure {
			color: red;
		}
		pre {
			text-align: left;
			overflow: auto;
		}
	</style>
	<?php

	// set some values
	$s_expected = "Array\n(\n    [709] => 1600:2000:U__WRFS:p/-/40.00/%:optional|0600:0601:UM_WRFS:a/a:both\n    [710] => 1600:2000:U__WRFS:p/-/20.00/%:optional\n    [711] => 0600:0601:UM_WRFS:a/n:both\n)\n";
	$o_original = new OriginalValues();

	// set the testing values
	lavu_query("UPDATE `menu_categories` SET `_deleted`='0',`happyhour`='1:f:|2:f:' WHERE `id`='18'");
	lavu_query("UPDATE `menu_items` SET `_deleted`='0',`happyhour`='1:e:p/-/40.00/%|2:e:a/a' WHERE `id`='709'");
	lavu_query("UPDATE `menu_items` SET `_deleted`='0',`happyhour`='2:d:' WHERE `id`='710'");
	lavu_query("UPDATE `menu_items` SET `_deleted`='0',`happyhour`='1:d:' WHERE `id`='711'");
	lavu_query("UPDATE `happyhours` SET `start_time`='1600',`end_time`='2000',`weekdays`='U__WRFS',`property`='p/-/20.00/%',`_deleted`='0',`chain_reporting_group`='0',`extended_properties`='optional' WHERE `id`='1'");
	lavu_query("UPDATE `happyhours` SET `start_time`='0600',`end_time`='0601',`weekdays`='UM_WRFS',`property`='a/n',`_deleted`='0',`chain_reporting_group`='0',`extended_properties`='both' WHERE `id`='2'");

	run_unit_test(18,$s_expected);
	$o_original->reset();

	function run_unit_test($category_id, $s_expected) {
		global $num_runs;
		global $o_happyHoursGetCategoryOrItem;
		$a_category_row = ConnectionHub::getConn('rest')->getDBAPI()->getAllInTable('menu_categories', array('_deleted'=>0, 'id'=>$category_id), FALSE);
		$a_item_rows = ConnectionHub::getConn('rest')->getDBAPI()->getAllInTable('menu_items', array('_deleted'=>0, 'category_id'=>$category_id), FALSE);

		echo "<table>";

		for ($i = 0; $i < 2; $i++) {
			for ($j = 0; $j < 2; $j++) {
				for ($k = 0; $k < 2; $k++) {
					$a_category_row_test = ($i) ? $a_category_row[0] : NULL;
					$a_item_rows_test = ($j) ? $a_item_rows : NULL;
					$b_cache = ($k) ? TRUE : FALSE;
					run_test(($i ? "yes" : "no")." pass category, ".($j ? "yes" : "no")." pass item, ".($k ? "yes" : "no")." cache", ($i+$j+$k==0 ? "first-row" : ""), "first-column", $s_expected,
						print_r($o_happyHoursGetCategoryOrItem->get_all_items_from_category_happyhours($category_id, $a_category_row_test, $a_item_rows_test, $b_cache),TRUE));
					$num_runs++;
				}
			}
		}

		echo "</table>";
	}

	function run_test($s_name, $s_first_row, $s_first_column, $s_expected, $s_result) {
		if ($s_expected !== $s_result) {
			echo "<tr><td class='failure {$s_first_row} {$s_first_column}'>FAIL: {$s_name}</td><td class='{$s_first_row}'><pre>{$s_expected}</pre></td><td class='{$s_first_row}'><pre>{$s_result}</pre></td></tr>";
		} else {
			echo "<tr><td class='success {$s_first_row} {$s_first_column}'>SUCCESS: {$s_name}</td><td class='{$s_first_row}'><pre>{$s_expected}</pre></td><td class='{$s_first_row}'><pre>{$s_result}</pre></td></tr>";
		}
	}

	class OriginalValues {

		function __construct() {
			$this->original = array(
				'menu_items'=>ConnectionHub::getConn('rest')->getDBAPI()->getAllInTable('menu_items', array('category_id'=>18), FALSE),
				'menu_categories'=>ConnectionHub::getConn('rest')->getDBAPI()->getAllInTable('menu_categories', array('id'=>18), FALSE),
				'happyhours'=>ConnectionHub::getConn('rest')->getDBAPI()->getAllInTable('happyhours', NULL, FALSE, array('whereclause'=>"WHERE `id`='1' OR `id`='2'"))
			);
		}

		public function reset() {
			foreach($this->original as $s_tablename=>$a_values) {
				foreach($a_values as $a_row) {
					$s_update_clause = ConnectionHub::getConn('rest')->getDBAPI()->arrayToUpdateClause($a_row);
					lavu_query("UPDATE `[tablename]` {$s_update_clause} WHERE `id`='[id]'", $a_row);
				}
			}
		}
	}

?>