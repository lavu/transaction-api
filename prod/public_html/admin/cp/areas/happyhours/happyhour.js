/******************************************************************************
 * Used to generate a "Happy Hour" form for creating happy hours
 * Implemented in resources/form_dialog.php and areas/edit_global_happy_hours
 * @author: Benjamin G. Bean (benjamin@poslavu.com)
 *
 * copyright Lavu
 ******************************************************************************/

/*~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~
 * about happy hours
 *~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~*/
/* Overview
 * Happy hours are used to apply specific properties to menu items/catagories based on the
 * the day of the week and time of day.
 *
 * Available properties:
 * At the time of writing this description, available properties to apply are "price modifiers" and "availability".
 *
 * Inheritance:
 * Menu items inherit the happy hour properties of menu categories, and
 * menu categories inherit the happy hour properties of the global happy hours.
 * Categories and items can override the happy hour properties of their parent, and even
 * enable or disable specific happy hours.
 *
 * Format:
 * The format of the `happyhour` column in menu_items and menu_categories is:
 * happyhourid:(e)nabledOR(d)isabled:property, eg 7:e:p/-/20.00/%
 * The column is also bar-delimeted, eg 1:d:a/n|7:e:p/-/20.00/%
 * If an id for a happy hour can't be found in the `happyhour` column, then the happy hour is
 * assumed to default to the parent value.
 */

/*~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~
 * for the catagories/items dialogs
 *~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~*/

// for the catagories and menu items forms
// @parentid the div to draw the happy hours inside of
// @hiddensaveid the div that contains the local happy hour settings of the catagory/item
// @globalhappyhours_hiddenid the id of the div with the global happy hours and their properties
// the global category_id js variable should be set by form_dialog.php
function hh_loadhappyhour(parentid, hiddensaveid, globalhappyhours_hiddenid) {
	// old
	//hh_echohappyhour($("#"+hiddensaveid).val(), parentid, hiddensaveid);
	// new
	hh_savehappyhours_catagoriesoritems = true;
	var ghhs = $("#"+globalhappyhours_hiddenid).val();
	hh_drawdefaultslayout(parentid, ghhs, hiddensaveid);
}

function hh_drawdefaultslayout(parentid, hhs, hiddensaveid) {
	var lhhs = $("#"+hiddensaveid).val();
	var local_hhs = [];
	var local_indexed = [];
	var iscategory = false;

	// get the local (category or item level) happy hours
	if (lhhs && lhhs.length > 0 && lhhs !== "") {
		local_hhs = lhhs.split("|");
		for (var i = 0; i < local_hhs.length; i++) {
			local_hhs[i] = local_hhs[i].split(":");
			//console.log(local_hhs[i]);
			local_hhs[i] = { id: local_hhs[i][0], mode: local_hhs[i][1], prop: local_hhs[i][2] };
			local_indexed[local_hhs[i]['id']] = { mode: local_hhs[i]['mode'], prop: local_hhs[i]['prop'] }
		}
	}

	// get the global happy hours
	var global_hhs = [];
	if (hhs && hhs.length > 0 && hhs !== "") {
		global_hhs = hhs.split("|");
		for (var i = 0; i < global_hhs.length; i++) {
			global_hhs[i] = global_hhs[i].split(":");
			global_hhs[i] = { id: global_hhs[i][4], name: global_hhs[i][5] };
		}
	}

	// is this a category or menu item?
	if (category_id) iscategory = false; else iscategory = true;

	var displaytext = "<table id='happyhour_hhstable'>";
	for (var i = 0; i < global_hhs.length; i++) {
		var id = global_hhs[i]['id'];
		var proptext = "";
		// set the default, here ^^^ \/ \/ \/ ^^^
		var usedefault = "";
		var disabled = "";
		if (iscategory) {
			disabled = " checked";
		} else {
			usedefault = " checked";
		}
		// ^^^ /\ /\ /\ ^^^
		var propsvisible = "display:none";
		if (local_indexed[id]) {
			usedefault = "";
			propsvisible = "";
			prop = local_indexed[id]['prop'];
			if (prop.length > 0)
				proptext += hh_drawproperty(i, prop.split("/"), hiddensaveid);
			else
				proptext = "";
			switch (local_indexed[id]['mode']) {
			case 'd':
				usedefault = "";
				disabled = " checked";
				break;
			case 'e':
				usedefault = "";
				disabled = "";
				break;
			case 'f':
				usedefault = " checked";
				disabled = "";
				break;
			}
		}
		var parentkindname = "";
		if (iscategory) parentkindname = "Universal"; else parentkindname = "Category";
		if (usedefault == " checked") usedefault = " selected";
		if (disabled == " checked") { disabled = " selected"; proptext = ""; }
		var override = "";
		if (usedefault == "" && disabled == "") override = " selected";
		var enabled_text = "<option value='default'"+usedefault+">"+(!iscategory ? parentkindname+" Default" : "Enabled")+"</option>";
		var disabled_text = "<option value='disabled'"+disabled+">Disabled</option>";
		var override_text = "<option value='override'"+override+">"+(iscategory ? "Override Properties" : "Enabled")+"</option>";
		if (override == "" && iscategory) override_text = "";
		var options_text = (iscategory ? enabled_text+disabled_text+override_text : enabled_text+override_text+disabled_text);
		displaytext += "<tr><td>"+global_hhs[i]['name']+"<input type='hidden' id='happyhour_hidden_globalid_"+i+"' value='"+id+"'></td><td> <select id='happyhour_select_mode_"+i+"' onchange='hh_changemode(this,\""+hhs+"\",\""+hiddensaveid+"\");hh_savehappyhours(\""+hiddensaveid+"\");'>"+options_text+"</select></td></tr>";
		displaytext += "<tr id='happyhour_hhstable_1_"+i+"'><td colspan='2' id='happyhour_property_"+i+"'>"+proptext+"</td></tr>";
		if (i < global_hhs.length-1) {
			displaytext += "<tr style='height:2px;background-color:black;opacity:0.1;'><td style='height:2px' colspan='2'></td></tr>";
		}
	}
	displaytext += "</table>";

	if ($("#happyhour_hhstable").length > 0)
		$("#happyhour_hhstable").remove();
	$("#"+parentid).append(displaytext);

	/* v1
	hh_enable_or_disable_all(); */
}

function hh_changemode(element, hhs, hiddensaveid) {
	var je = $(element); // get the jquery-element
	var id = je.prop("id");
	var happyhour_index = hh_myParseInt( id.match(/[0-9]+/) );
	var i = happyhour_index;
	var mode = je.val();

	switch(mode) {
	case "default":
	case "disabled":
		$("#happyhour_property_"+i).html("");
		break;
	case "override":
		var global_hhs = [];
		if (hhs && hhs.length > 0 && hhs !== "") {
			global_hhs = hhs.split("|");
			for (var j = 0; j < global_hhs.length; j++) {
				global_hhs[j] = global_hhs[j].split(":");
				global_hhs[j] = { id: global_hhs[j][4], name: global_hhs[j][5] };
			}
		}

		var propertycontainer = $("#happyhour_property_"+i);
		propertycontainer.append($("<font style='color:#999;font-decoration:italic;'>loading...</font>"));

		setTimeout(function() {
			var proptext = "";
			//console.log(global_hhs[i]);
			var result = hh_changedefault_send_getdefault(global_hhs[i]);
			//console.log(result);
			if (result.indexOf("success") !== -1) {
				var prop = result.split("?")[1]; // should be in the form success?e:p/-/20.00/%
				var propvals = prop.split(":")[1].split("/");
				proptext = hh_drawproperty(i, propvals, hiddensaveid);
			} else {
				proptext = "<table><tr><td>"+hh_drawproperty(i, "p/-/20.00/%".split("/"), hiddensaveid)+"</td></tr><tr><td><font style='color:#999;font-decoration:italic;'>failed to load default</font></td></tr></table>";
			}
			propertycontainer.html(proptext);
			hh_savehappyhours(hiddensaveid);
			//console.log(propertycontainer.html());
		}, 30);
		break;
	default:
	}
}

hh_changedefault_send_getdefault_retval = null;
function hh_changedefault_send_getdefault(global_happyhour_id) {
	var rand = Math.ceil(Math.random()*2);
	var action = "getcategory_hh_property";
	if (!category_id) {
		action = "getglobal_hh_property";
	}
	$.ajax({
		url: "../index.php?widget=happyhours/php/request_action",
		type: "POST",
		async: false,
		cache: false,
		timout: 1000,
		data: { action: action, category: category_id, happyhour_id: global_happyhour_id['id'], random_num: rand },

		success: function (string) {
			$('#duplicate_week_duplicate_button').removeAttr('disabled');
			if( string.indexOf('success') !== -1 ){
				hh_changedefault_send_getdefault_retval = string;
			}else{
				var error_type = string.split("?")[0];
				var error_message = string.split("?")[1];
				hh_changedefault_send_getdefault_retval = "Error (" + error_type + "): " + error_message;
			}
		},

		error: function(j ,t, e){
			$('#duplicate_week_duplicate_button').removeAttr('disabled');
			if (t === "timeout"){
				hh_changedefault_send_getdefault_retval = "Error (timeout): no response from server";
			}else{
				hh_changedefault_send_getdefault_retval = "Error (" + t + ")";
			}
		}
	});

	return hh_changedefault_send_getdefault_retval;
}

function hh_getcategoryinterface_savevals() {
	var savevals = [];
	var iscategory = null;
	if (category_id) iscategory = false; else iscategory = true;

	for (var i = 0; i < 100000 && $("#happyhour_select_mode_"+i).length > 0; i++) {
		var isdisabled = ($("#happyhour_select_mode_"+i).val() == "disabled");
		var isdefault = ($("#happyhour_select_mode_"+i).val() == "default");
		// break early for the default settings
		if (isdisabled && iscategory)
			continue;
		if (isdefault && !iscategory)
			continue;

		var saveindex = savevals.length;
		savevals[saveindex] = $("#happyhour_hidden_globalid_"+i).val();
		var prop = "";
		if (isdisabled) {
			savevals[saveindex] += ":d";
		} else if (isdefault) {
			savevals[saveindex] += ":f";
		} else { // override properties
			savevals[saveindex] += ":e";
			prop = hh_getproperty(i).join("/");
		}
		savevals[saveindex] += ":" + prop;
	}
	return savevals;
}

/*~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~
 * for both
 *~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~*/

hh_savehappyhours_catagoriesoritems = null;
// save the happy hours in the hidden input field
// @hiddensaveid the id of the hidden input field used to save the happy hours in
function hh_savehappyhours(hiddensaveid) {
	var savevals = [];

	// for the catagories/items dialogs
	if (hh_savehappyhours_catagoriesoritems) {
		savevals = hh_getcategoryinterface_savevals();
	} else
	//for the global happy hours dialog
	{
		for(var i = 0; i < 100000; i++) {
			if($("#happyhour_st_"+i) && $("#happyhour_st_"+i).length > 0) {
				var hh = hh_gethappyhour(i);
				hh["prop"] = hh["prop"].join("/");
				savevals.push( hh["times"]["st"] + ":" + hh["times"]["et"] + ":" + hh["days"] + ":" + hh["prop"] + ":" + hh["id"] + ":" + hh["name"] + ":" + hh["deleted"] + ":" + hh["chain_reporting_group"] + ":" + hh["affect_mods"] );
			} else {
				break;
			}
		}
	}

	//console.log("save id: " + hiddensaveid);
	$("#"+hiddensaveid).val(savevals.join("|"));
	//console.log("save value: " + $("#"+hiddensaveid).val());
}

/*~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~
 * for the global happy hours dialog
 *~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~*/

// @hhs happy hours, in the form happyhour|happyhour|happyhour...
//     each happy hour has the form stim:etim:WEEKD_Y:prop
//     each prop has the form p/(+|-|=)/123.45/($|%) or a/(a|n) where the first form is for "price" and the second form is for "availability"
// @parentid the id of the element to stick this into
// @hiddensaveid the id (and name) of the element to hold the savedata in
function hh_echohappyhour(hhs_in, parentid, hiddensaveid) {
	var hhs = [];
	if (hhs_in && hhs_in.length > 0)
		hhs = hhs_in.split("|");
	var tabletext = "<table id=\"happyhour_table_contents\"><tr><td>";

	if (hhs && hhs.length > 0) {
		var firsthh = 0;
		var lasthh = hhs.length-1;
		while (hhs[firsthh] && hhs[firsthh].split(":")[6] == "1")
			firsthh++;
		while (hhs[lasthh] && hhs[lasthh].split(":")[6] == "1")
			lasthh--;

		for(var i = 0; i < hhs.length; i++) {
			//
			var hh = hhs[i].split(":");
			hh[3] = hh[3].split("/");
			if (hh.length < 8) hh[7] = 0;
			if (hh.length < 9) hh[8] = "none";
			hh = {times: {st: hh[0], et: hh[1]}, days: hh[2], prop: hh[3], id: hh[4], name: hh[5], deleted: hh[6], chain_reporting_group: hh[7], affect_mods: hh[8]};
			var display = "";
			if (hh['deleted'] == '1' || hh['deleted'] == 1)
				display = "display:none;";

			var borderbottom = "border-bottom:none;";
			var bordertop = "border-top:none;";
			var border = "border:1px solid #999;";
			if (hh['deleted'] == "1") border = "";
			if (i == firsthh) bordertop = "";
			if (i == lasthh) borderbottom = "";

			tabletext += "<table class=\"single_hh_table\" style=\""+border+bordertop+borderbottom+" padding:15px;\">";
			tabletext += "<tr style='display:none;'>";
			tabletext += "	<td style=\"vertical-align:middle\">ID:</td>";
			tabletext += "	<td style=\"vertical-align:middle\" id=\"happyhour_id_"+i+"\">"+hh['id']+"</td>";
			tabletext += "</tr>";
			tabletext += "<tr style='display:none;'>";
			tabletext += "	<td style=\"vertical-align:middle\">Deleted:</td>";
			tabletext += "	<td style=\"vertical-align:middle\" id=\"happyhour_deleted_"+i+"\">"+hh['deleted']+"</td>";
			tabletext += "</tr>";
			tabletext += "<tr style='"+display+"'>";
			tabletext += "	<td style=\"vertical-align:middle\">Name:</td>";
			tabletext += "	<td style=\"vertical-align:middle\"><input type='text' size='20' id=\"happyhour_name_"+i+"\" value='"+hh['name']+"' onkeyup=\"hh_checkname_text(this);hh_savehappyhours('"+hiddensaveid+"');\" /></td>";
			tabletext += "</tr>";
			tabletext += "<tr style='display:none;'>";
			tabletext += "	<td style=\"vertical-align:middle\">Start time:</td>";
			tabletext += "	<td style=\"vertical-align:middle\" id=\"happyhour_st_"+i+"\">"+hh_drawtime(i, "st", hh["times"], hiddensaveid)+"</td>";
			tabletext += "</tr>";
			tabletext += "<tr style='display:none;'>";
			tabletext += "	<td style=\"vertical-align:middle\">End time:</td>";
			tabletext += "	<td style=\"vertical-align:middle\" id=\"happyhour_et_"+i+"\">"+hh_drawtime(i, "et", hh["times"], hiddensaveid)+"</td>";
			tabletext += "</tr>";
			tabletext += "<tr style='"+display+"'>";
			tabletext += "	<td style=\"vertical-align:middle\">Time:</td>";
			tabletext += "	<td style=\"vertical-align:middle\">"+tc_display_time(hh['times']['st'])+" - "+tc_display_time(hh['times']['et'])+" <button onclick=\"hh_build_time_chart("+i+", {st:"+(hh['times']['st']*1)+",et:"+(hh['times']['et']*1)+"},'"+hiddensaveid+"','"+parentid+"');\">Change</button></td>";
			tabletext += "<tr style='"+display+"'>";
			tabletext += "	<td style=\"vertical-align:middle\">Weekdays:</td>";
			tabletext += "	<td style=\"vertical-align:middle\">" + hh_drawdays(i, hh["days"], hiddensaveid); "</td>";
			tabletext += "</tr>";
			tabletext += "<tr style='"+display+"'>";
			tabletext += "	<td style=\"vertical-align:middle\">Property:</td>";
			tabletext += "	<td style=\"vertical-align:middle\" id=\"happyhour_property_"+i+"\">" + hh_drawproperty_text(i, hh["prop"], hiddensaveid) + "</td>";
			tabletext += "</tr>";
			tabletext += "<tr style='"+display+"'>";
			tabletext += "	<td style=\"vertical-align:middle\">Affect On Modifiers:</td>";
			tabletext += "	<td style=\"vertical-align:middle\" id=\"happyhour_mods\">"
			tabletext += "		<select id=\"happyhour_affect_mods_"+i+"\" onchange=\"hh_savehappyhours('"+hiddensaveid+"');\">";
			tabletext += "			<option value=\"none\""+(hh.affect_mods == "none" ? " SELECTED" : "")+">No Affect</option>";
			tabletext += "			<option value=\"forced\""+(hh.affect_mods == "forced" ? " SELECTED" : "")+">Forced Modifiers Only</option>";
			tabletext += "			<option value=\"optional\""+(hh.affect_mods == "optional" ? " SELECTED" : "")+">Optional Modifiers Only</option>";
			tabletext += "			<option value=\"both\""+(hh.affect_mods == "both" ? " SELECTED" : "")+">Both Forced And Optional Modifiers</option>";
			tabletext += "		</select>";
			tabletext += "	</td>";
			tabletext += "</tr>";
			if (window.b_part_of_chain) {
				var select_options = hh_draw_crgs(hh["chain_reporting_group"], i, hiddensaveid);
				if (select_options.match(/<input.*type.*=.*hidden.*/) === null) {
					tabletext += "<tr style='"+display+"'>";
					tabletext += "	<td style=\"vertical-align:middle\">Chain Reporting Group:</td>";
					tabletext += "	<td style=\"vertical-align:middle\">";
					tabletext += hh_draw_crgs(hh["chain_reporting_group"], i);
					tabletext += "</td>";
					tabletext += "</tr>";
				}
			}
			tabletext += "<tr style='"+display+"'>";
			tabletext += "	<td style=\"vertical-align:middle\"></td>";
			tabletext += "	<td style=\"vertical-align:middle\"><button onclick=\"hh_savehappyhours('"+hiddensaveid+"');hh_removehappyhour("+i+",'"+parentid+"','"+hiddensaveid+"');hh_savehappyhours('"+hiddensaveid+"');\">Remove</button> Happy Hour</td>";
			tabletext += "</tr>";
			tabletext += "</table>";
			if (i < hhs.length-1 && hhs[i+1].split(":")[6] != "1") {
				tabletext += "<table class=\"single_hh_table_break\" style=\""+border+" border-top-style:none; border-bottom-style:none; margin:0; padding:0; \">";
				tabletext += "	<tr style='margin:0;padding:0;"+display+"'>";
				tabletext += "		<td colspan=\"2\" style=\"margin:0;padding:0;\"><hr style=\"margin:0;padding:0;\"></td>";
				tabletext += "	</tr>";
				tabletext += "</table>";
			}
		}
	} else {
		tabletext += "There are currently no happy hours to display.";
	}

	tabletext += "</td></tr>";
	tabletext += "<tr>";
	tabletext += "	<td style=\"vertical-align:middle;\" colspan=\"2\"><div id=\"add_new_button_container\"><button style=\"margin-top:10px;\" onclick=\"hh_savehappyhours('"+hiddensaveid+"');hh_createnewhappyhour('"+parentid+"','"+hiddensaveid+"');hh_savehappyhours('"+hiddensaveid+"');\">Add More >></button></div></td>";
	tabletext += "</tr>";
	tabletext += "</table>";

	while ($("#happyhour_table_contents").length > 0)
		$("#happyhour_table_contents").remove();
	$("#" + parentid).append($(tabletext));
	$("#add_new_button_container").hide();
	$("#add_new_button_container").stop(false,true);
	hh_checkdays(-1);
	for(var i = 0; i < 100000; i++)
		if($("#happyhour_st_"+i) && $("#happyhour_st_"+i).length > 0)
			hh_forcemonetarytype(i);
		else
			break;

	var hh_tables = $(".single_hh_table");
	var hh_breaks = $(".single_hh_table_break");
	var both = $.merge(hh_tables, hh_breaks);
	if (hh_tables.length > 0) {
		var largest_table_width = 0;
		$.each(hh_tables, function(k,v) {
			largest_table_width = Math.max(parseInt($(v).width()), largest_table_width);
		});
		largest_table_width += 50;
		$.each(both, function(k,v) {
			$(v).css({ width:(largest_table_width+'px') });
		});
	}
}

// chooses the chain reporting group options for the happy hour
// and returns them as a list of <option>s
function hh_draw_crgs(hh_crg, i, hiddensaveid) {
	var s_retval = '';
	var b_found = false;

	s_retval = "<select id=\"happyhour_chain_reporting_group_"+i+"\" onchange=\"hh_savehappyhours('"+hiddensaveid+"');\">";
	$.each(a_chain_groups, function(k,v){
		var name = v[0];
		var id = v[1];
		var selected = "";
		if (hh_crg == id) {
			selected = " SELECTED";
			b_found = true;
		}
		s_retval += "<option value=\""+id+"\""+selected+">"+name+"</option>";
	});
	s_retval += '</select>';

	if (!b_found) {
		$.each(a_all_chain_groups, function(k,v) {
			var name = v['name'];
			var id = v['id'];
			if (hh_crg == id) {
				s_retval = "<input type=\"hidden\" id=\"happyhour_chain_reporting_group_"+i+"\" value=\""+id+"\" />";
			}
		});
	}

	return s_retval;
}

// removes the selected happy hour
// @happyhour_index there is no array of happy hours in the javascript
// the happy hours are stored as hidden input fields within the iframe
// @parentid id of the element within which to insert the happyhour html
// @hiddensaveid the id of the hidden input field used to save the happy hours in
function hh_removehappyhour(happhour_index, parentid, hiddensaveid) {
	var hhs = $("#"+hiddensaveid).val();
	if (hhs.indexOf("|") > -1)
		hhs = hhs.split("|");
	else
		hhs = [hhs];
	newhhs = [];
	for (var i = 0; i < hhs.length; i++) {
		// keep the happy hour, so that on a save the happy hour can be removed
		// change its properties so that it doesn't conflict with any of the other happy hours
		if (i == happhour_index) {
			var hh = hhs[i].split(":");
			//hh[0]; // start time
			//hh[1]; // end time
			hh[2] = "_______"; // weekdays
			//hh[3]; // property
			//hh[4]; // happy hour id
			//hh[5]; // happy hour name
			hh[6] = "1"; // deleted
			hhs[i] = hh.join(":");
		}
		newhhs.push(hhs[i]);
	}
	newhhs = newhhs.join("|");
	hh_echohappyhour(newhhs, parentid, hiddensaveid);
}

// creates a new default happy hour
// @parentid id of the element within which to insert the happyhour html
// @hiddensaveid the id of the hidden input field used to save the happy hours in
function hh_createnewhappyhour(parentid, hiddensaveid) {
	var newval = $("#"+hiddensaveid).val();
	if (newval && newval.length > 0) newval += "|";
	newval += "1600:2000:UMTWRFS:p/-/20.00/%:-1:New Happy Hour:0:0";
	hh_echohappyhour(newval, parentid, hiddensaveid);
}

// draws the time as an hour text field, min text field, and AM/PM select field
// @happyhour_index there is no array of happy hours in the javascript
// the happy hours are stored as hidden input fields within the iframe
// @st_or_et whether this is drawing the start or end time (eg "st" or "et")
// @happyhour_times the times of the happy hour (eg { st: "0000", et: "0000" })
// @hiddensaveid the id of the hidden input field used to save the happy hours in
function hh_drawtime(happyhour_index, st_or_et, happyhour_times, hiddensaveid) {
	if (!happyhour_times) {
		happyhour_times = hh_gethappyhour(happyhour_index)["times"];
	}

	var retval = "Hour: <select id=\"happyhour_"+st_or_et+"_hour_"+happyhour_index+"\" onchange=\"hh_savehappyhours('"+hiddensaveid+"');\">";
	var earliesthour = 0;
	var latesthour = 30; if (st_or_et == "st") latesthour = 23;
	var hour = happyhour_times[st_or_et].slice(0,2);
	var min = happyhour_times[st_or_et].slice(2,4);
	for (var i = earliesthour; i <= latesthour; i++) {
		var selected = ""; if (hh_myParseInt(hour) == i) selected = " selected";
		var ampm = "am"; if (i >= 12 && i < 24) ampm = "pm";
		var displayhour = i % 12; if (displayhour == 0) displayhour = 12; displayhour = displayhour+ampm;
		retval += "<option value=\""+i+"\" "+selected+">"+displayhour+"</option>"
	}
	retval += "</select>";

	var id = "happyhour_"+st_or_et+"_min_"+happyhour_index;
	retval += " Min: <input id=\""+id+"\" type=\"text\" size=\"2\" onkeyup=\"hh_checkmin_text(this, 59);hh_savehappyhours('"+hiddensaveid+"');\" value=\""+min+"\">";

	return retval;
}

// only allows one happy hour to have any day checked
// for example, only one happy hour can have "sunday" checked
// if all the days are checked, it disabled the "Add More >>" button
// @happyhour_index index of the happy hour that takes presidence
function hh_checkdays(happyhour_index) {
	var daysavailable = 7;
	for (var day = 0; day < 7; day++) {
		if ($("[id^=happyhour_day_"+day+"]:checked").length > 0 <?php echo (@$b_multiple_hhs ? '&& false' : ''); ?>) {
			var checked = false;
			if ($("#happyhour_day_"+day+"_"+happyhour_index).length > 0)
				if ($("#happyhour_day_"+day+"_"+happyhour_index).attr("checked"))
					checked = true;
			for (var i = 0; i < 100000 && $("#happyhour_day_"+day+"_"+i).length > 0; i++) {
				if ( (checked && i != happyhour_index) || !($("#happyhour_day_"+day+"_"+i).attr("checked"))) {
					$("#happyhour_day_"+day+"_"+i).removeAttr("checked");
					$("#happyhour_day_"+day+"_"+i).attr("disabled","disabled");
					$("#happyhour_day_text_"+day+"_"+i).css({ color: "#999" });
				} else {
					if ($("#happyhour_day_"+day+"_"+i).attr("checked")) {
						checked = true;
						daysavailable--;
					}
				}
			}
		} else {
			for (var i = 0; i < 100000 && $("#happyhour_day_"+day+"_"+i).length > 0; i++) {
				$("#happyhour_day_"+day+"_"+i).removeAttr("disabled");
				$("#happyhour_day_text_"+day+"_"+i).css({ color: "#000" });
			}
		}
	}

	if (daysavailable == 0 <?php echo (@$b_multiple_hhs ? '&& false' : ''); ?>) {
		$("#add_new_button_container").stop(false, true);
		$("#add_new_button_container").hide(200);
	} else {
		$("#add_new_button_container").stop(false, true);
		$("#add_new_button_container").show(200);
	}
}

// enforces the correct formatting in the name input text field
// @object the text field
function hh_checkname_text(object) {
	var text = $(object).val();

	if (text.match(/(:|'|"|\/|\||\?)/)) {
		text = text.replace(/(:|'|"|\/|\||\?)/g, "");
		$(object).val(text);
	}
}

// enforces the correct formatting in the time input text field
// @object the text field
function hh_checkmin_text(object, maxmin) {
	var text = $(object).val();
	var intRegex = /^\d+$/;
	for(var i = 0; i < text.length; i++) {
		if (!intRegex.test(text.slice(i, i+1))) {
			$(object).val(text.slice(0, i) + text.slice(i+1, text.length));
			return hh_checkmin_text(object, maxmin);
		}
	}

	if (hh_myParseInt(text) > maxmin) {
		$(object).val(maxmin);
	}

	if ($(object).val().length > 2) {
		$(object).val( $(object).val().slice(0,2) );
	}
}

// converts a 12hr time (eg 12:15pm) to a 24hr time (eg 1215)
function convert_to_numeric_time(t) {
	var ampm = "";
	if(t.indexOf('a') > 0 || t.indexOf('A') > 0)
		ampm = "am";
	else if(t.indexOf('p') > 0 || t.indexOf('P') > 0)
		ampm = "pm";
	if(ampm=="")
	{
		return -1;
	}
	val_parts = t.split(':');
	var hours = "";
	var mins = "";
	hours = val_parts[0];
	if(val_parts.length > 1)
	{
		mins = val_parts[1];
	}
	else
	{
		return -1;
	}
	hours = hours.replace(/[^0-9]/g, '');
	mins = mins.replace(/[^0-9]/g, '');

	if(hours >= 0 && hours <= 24)
	{
		if(ampm=="pm" && hours < 12) hours = hours * 1 + 12;
		else if(ampm=="am" && hours==12) hours = 0;
		hours = hours * 100;
	}
	else return -1;
	if(mins >= 0 && mins <= 60)
	{
	}
	else return -1;
	return (hours * 1 + mins * 1);
}

// enforces the correct formatting in the price input text field
// @object the text field
function hh_checkprice_text(object)  {
	var text = $(object).val();
	var intRegex = /^\d+$/;
	var decimalRegex = /\.|,/;
	var numperiods = 0;
	for(var i = 0; i < text.length; i++) {
		if (decimalRegex.test(text.slice(i, i+1))) {
			numperiods++;
			if (numperiods > 1){
				$(object).val(text.slice(0, i) + text.slice(i+1, text.length));
				return hh_checkprice_text(object);
			}
		}
		if (!intRegex.test(text.slice(i, i+1)) && !decimalRegex.test(text.slice(i, i+1))) {
			$(object).val(text.slice(0, i) + text.slice(i+1, text.length));
			return hh_checkprice_text(object);
		}
	}

	if (decimalRegex.test($(object).val())) {
		var centsRegex = /(\.|,).*$/;
		var dolarsRegex = /^.*(\.|,)/;
		var cents = $(object).val().match(centsRegex)[0];
		var dolars = $(object).val().match(dolarsRegex)[0];
		var period = "";
		if (cents.length > 0) { period = cents.slice(0, 1); cents = cents.slice(1, cents.length); }
		if (dolars.length > 0) { period = dolars.slice(dolars.length-1, dolars.length); dolars = dolars.slice(0, dolars.length-1); }
		if (cents.length > 2) {
			cents = cents.slice(0,2);
			$(object).val(dolars + period + cents);
		}
	}
}

// retuns a string representing the days available for the happy hour
// @happyhour_index there is no array of happy hours in the javascript
// the happy hours are stored as hidden input fields within the iframe
// @happyhour_weekdays the days of the week this happy hour applies to (eg U_T_R_S for sun, tue, thr, and sat)
// @hiddensaveid the id of the hidden input field used to save the happy hours in
function hh_drawdays(happyhour_index, happyhour_weekdays, hiddensaveid) {
	var i = happyhour_index;
	if (!happyhour_weekdays) {
		happyhour_weekdays = hh_gethappyhour(happyhour_index)["days"];
	}

	var daychecked = [];
	daychecked.push(""); if (happyhour_weekdays.indexOf("U") > -1) daychecked[daychecked.length-1] = " checked";
	daychecked.push(""); if (happyhour_weekdays.indexOf("M") > -1) daychecked[daychecked.length-1] = " checked";
	daychecked.push(""); if (happyhour_weekdays.indexOf("T") > -1) daychecked[daychecked.length-1] = " checked";
	daychecked.push(""); if (happyhour_weekdays.indexOf("W") > -1) daychecked[daychecked.length-1] = " checked";
	daychecked.push(""); if (happyhour_weekdays.indexOf("R") > -1) daychecked[daychecked.length-1] = " checked";
	daychecked.push(""); if (happyhour_weekdays.indexOf("F") > -1) daychecked[daychecked.length-1] = " checked";
	daychecked.push(""); if (happyhour_weekdays.indexOf("S") > -1) daychecked[daychecked.length-1] = " checked";
	var retval = "";
	for (var day = 0; day < 7; day++) {
		var dayname = "";
		switch(day) {
			case 0: dayname="Sun"; break;
			case 1: dayname="Mon"; break;
			case 2: dayname="Tue"; break;
			case 3: dayname="Wed"; break;
			case 4: dayname="Thu"; break;
			case 5: dayname="Fri"; break;
			case 6: dayname="Sat"; break;
		}
		retval += "<input type=\"checkbox\" id=\"happyhour_day_"+day+"_"+i+"\""+daychecked[day]+" onclick=\"hh_checkdays('"+happyhour_index+"');hh_savehappyhours('"+hiddensaveid+"');\"><font id=\"happyhour_day_text_"+day+"_"+i+"\">"+dayname+"</font> ";
	}

	return retval;
}

// actually write the property of the happy hour to the html
// @happyhour_index there is no array of happy hours in the javascript
// the happy hours are stored as hidden input fields within the iframe
// @hiddensaveid the id of the hidden input field used to save the happy hours in
function hh_echoproperty(happyhour_index, hiddensaveid, property) {
	var i = happyhour_index;
	var currval = $("#happyhour_property_selecttype_"+i).val();
	var prop = [];
	if (!property || property == null || property == "") {
		if (currval == "a") {
			prop = ["a", "a"];
		} else {
			prop = ["p", "+", "0.00", "%"];
		}
	} else {
		prop = property.split(":");
	}

	$("#happyhour_property_"+i).html(hh_drawproperty(i, prop, hiddensaveid));
	$("#happyhour_property_"+i).stop(false, true);
	$("#happyhour_property_"+i).hide();
	$("#happyhour_property_"+i).show(200);
}

// enforces the monetary type of the happy hour (whether percent is enabled or not)
// @happyhour_index there is no array of happy hours in the javascript
// the happy hours are stored as hidden input fields within the iframe
function hh_forcemonetarytype(happyhour_index) {
	var i = happyhour_index;
	$("#happyhour_property_radiopercent_"+i).removeAttr("disabled");
	$("#happyhour_property_percentsign_"+i).css("color", "#000");
	if ($("#happyhour_property_selectmodifier_"+i).val() == "=") {
		$("#happyhour_property_radiodolar_"+i).prop("checked", "checked");
		$("#happyhour_property_radiopercent_"+i).attr("disabled", "disabled");
		$("#happyhour_property_percentsign_"+i).css("color", "#aaa");
	}
}

// returns a string representing the property of the happy hour in text form
// @happyhour_index there is no array of happy hours in the javascript
// the happy hours are stored as hidden input fields within the iframe
// @happyhour_property the property field of the happy hour (eg ['p', '-', '20.00', '%'])
// @hiddensaveid the id of the hidden input field used to save the happy hours in
function hh_drawproperty_text(happyhour_index, happyhour_property, hiddensaveid) {
	var i = happyhour_index;
	var prop = happyhour_property;
	var retval = "<input type=\"hidden\" id=\"happyhour_property_hidden_"+i+"\" value=\""+prop.join(":")+"\">";

	if (prop[0] == "p") {
		retval += "Price: "+prop[1]+" "+prop[2]+prop[3]+" <input type=\"button\" onclick=\"hh_echoproperty("+i+",'"+hiddensaveid+"','"+prop.join(":")+"');\" value=\"Edit\">";
	} else {
		var availabilitytext = "available (<font style='font-style:italic'>only during this time</font>)";
		if (prop[1] == "n") availabilitytext = "not available (<font style='font-style:italic'>only during this time</font>)";
		retval += "Availability: "+availabilitytext+" <input type=\"button\" onclick=\"hh_echoproperty("+i+",'"+hiddensaveid+"','"+prop.join(":")+"');\" value=\"Edit\">";
	}
	return retval;
}

// returns a string of the property fields of the happy hour
// @happyhour_index there is no array of happy hours in the javascript
// the happy hours are stored as hidden input fields within the iframe
// @happyhour_property the property field of the happy hour (eg ['p', '-', '20.00', '%'])
// @hiddensaveid the id of the hidden input field used to save the happy hours in
function hh_drawproperty(happyhour_index, happyhour_property, hiddensaveid) {
	var i = happyhour_index;
	var prop = happyhour_property;

	var priceselected = ""; var availabilityselected = ""; if (prop[0] == "p") { priceselected = " selected"; } else { availabilityselected = " selected"; }
	var retval = "<select id=\"happyhour_property_selecttype_"+i+"\" onchange=\"hh_echoproperty("+i+",'"+hiddensaveid+"');hh_savehappyhours('"+hiddensaveid+"');\"><option value=\"p\""+priceselected+">Price</option><option value=\"a\""+availabilityselected+">Availability</option></select>";
	if (prop[0] == "p") {
		var pselected = ""; if (prop[1] == "+") pselected = " selected";
		var mselected = ""; if (prop[1] == "-") mselected = " selected";
		var eselected = ""; if (prop[1] == "=") eselected = " selected";
		var dchecked = ""; if (prop[3] == "$") dchecked = " checked";
		var pchecked = ""; if (prop[3] == "%") pchecked = " checked";
		retval += "<select id=\"happyhour_property_selectmodifier_"+i+"\" onchange=\"hh_forcemonetarytype("+i+");hh_savehappyhours('"+hiddensaveid+"');\"><option"+pselected+">+</option><option"+mselected+">-</option><option"+eselected+">=</option></select><input type=\"text\" size=\"6\" id=\"happyhour_property_textamount_"+i+"\" value=\""+prop[2]+"\" onkeyup=\"hh_checkprice_text(this);hh_savehappyhours('"+hiddensaveid+"');\"><input type=\"radio\" id=\"happyhour_property_radiopercent_"+i+"\" name=\"happyhour_radio_group_"+i+"\" value=\"%\""+pchecked+" onclick=\"hh_savehappyhours('"+hiddensaveid+"');\"><font id=\"happyhour_property_percentsign_"+i+"\">%</font><input type=\"radio\" id=\"happyhour_property_radiodolar_"+i+"\" name=\"happyhour_radio_group_"+i+"\" value=\"$\""+dchecked+" onclick=\"hh_savehappyhours('"+hiddensaveid+"');\">$";
	} else {
		aselected = ""; if (prop[1] == "a") aselected = " selected";
		nselected = ""; if (prop[1] == "n") nselected = " selected";
		retval += "<select id=\"happyhour_property_selectavailability_"+i+"\" onchange=\"hh_savehappyhours('"+hiddensaveid+"');\"><option value=\"a\""+aselected+">Available</option><option value=\"n\""+nselected+">Not Available</option></select> (<font style='font-style:italic'>only during this time</font>)";
	}
	return retval;
}

// finds the indicated happy hour and returns its property (eg "p/-/20.00/%")
// @happyhour_index there is no array of happy hours in the javascript
// the happy hours are stored as hidden input fields within the iframe
function hh_getproperty(happyhour_index) {
	var i = happyhour_index;
	var prop = [];
	if ($("#happyhour_property_hidden_"+i).length > 0) {
		prop = $("#happyhour_property_hidden_"+i).val().split(":");
	} else {
		prop = $("#happyhour_property_selecttype_"+i).val();
		if (prop == "p") {
			var dolar_or_percent = "$"; if ($("#happyhour_property_radiopercent_"+i).prop("checked")) { dolar_or_percent = "%"; }
			prop = ["p", $("#happyhour_property_selectmodifier_"+i).val(), $("#happyhour_property_textamount_"+i).val(), dolar_or_percent];
		} else {
			prop = ["a", $("#happyhour_property_selectavailability_"+i).val()];
		}
	}
	return prop;
}

function hh_padleft(instring,length,paddingchar) {
	while (instring.length < length && paddingchar.length > 0) instring = paddingchar + instring;
	return instring;
};

// finds the indicated happy hour and returns it as a dictionary (eg { times: { st: "0000", et: "0000" }, days: "UMTWRFS", prop: "p/-/20.00/%", id: "-1", name: "New Happy Hour", deleted: "0", chain_reporting_group: "0" })
// @happyhour_index there is no array of happy hours in the javascript
// the happy hours are stored as hidden input fields within the iframe
function hh_gethappyhour(happyhour_index) {
	var i = happyhour_index;
	var st = hh_padleft($("#happyhour_st_hour_" + i).val().toString(),2,"0") + hh_padleft($("#happyhour_st_min_" + i).val().toString(),2,"0");
	var et = hh_padleft($("#happyhour_et_hour_" + i).val().toString(),2,"0") + hh_padleft($("#happyhour_et_min_" + i).val().toString(),2,"0");
	var days = "";
	for (var day = 0; day < 7; day++) {
		var daychar = "";
		switch(day){
			case 0: daychar = "U"; break;
			case 1: daychar = "M"; break;
			case 2: daychar = "T"; break;
			case 3: daychar = "W"; break;
			case 4: daychar = "R"; break;
			case 5: daychar = "F"; break;
			case 6: daychar = "S"; break;
		}
		if ($("#happyhour_day_"+day+"_"+i).prop("checked")) { days += daychar; } else { days += "_"; }
	}
	var prop = hh_getproperty(i);
	var id = $("#happyhour_id_" + i).html();
	var name = $("#happyhour_name_" + i).val();
	var deleted = $("#happyhour_deleted_" + i).html();
	var chain_reporting_group = $("#happyhour_chain_reporting_group_"+i).val();
	var affect_mods = $("#happyhour_affect_mods_"+i).val();
	var retval = {times: {st: st, et: et}, days: days, prop: prop, id: id, name: name, deleted: deleted, chain_reporting_group: chain_reporting_group, affect_mods: affect_mods };
	return retval;
}

function hh_build_time_chart(happyhour_index, happyhour_times, hiddensaveid, parentid) {
	//console.log(happyhour_times);
	tc_open_time_chart(parentid+"."+happyhour_index+"."+hiddensaveid, happyhour_times['st'], happyhour_times['et']);
}

// for msie compatability
// msie interprets "0800" as 0, but all other browswer interpret it as 800
// this function checks for leading zeroes in numbers
function hh_myParseInt(instring) {
	if (instring == null) {
		//console.log("Null passed in as integer");
		return 0;
	}

	instring = ""+instring;
	if (instring.match(/[1-9][0-9]*/)) {
		return parseInt(instring.match(/[1-9][0-9]*/));
	} else {
		if (instring.match(/[0-9]*/)) {
			return parseInt(instring.match(/[0-9]*/));
		} else {
			//console.log("NaN ("+instring+")");
			return 0;
		}
	}
}

function hh_submit_time() {
	var property = $("#tc_time_chart_item_property").val();
	var parentid = property.split(".")[0];
	var i = hh_myParseInt(property.split(".")[1]);
	var hiddensaveid = property.split(".")[2];
	//console.log(parentid + ":" + i + ":" + hiddensaveid);
	var st = $("#tc_time_chart_start_time").val();
	var et = $("#tc_time_chart_end_time").val();
	var stval = hh_myParseInt(st);
	var etval = hh_myParseInt(et);

	var longer_than_day_shifts_enabled = tc_longer_than_day_shifts_enabled;
	var night_shift_start_time = tc_night_shift_start_time;
	var night_shift_end_time = tc_night_shift_end_time;

	if (etval < stval) {
		if (longer_than_day_shifts_enabled && stval >= night_shift_start_time && etval <= night_shift_end_time) {
			etval += 2400;
		} else {
			//if (stval >= 1200)
				alert("Graveyard happy hours must start after 12:00pm");
			//else
			//	alert("The start time must come before the end time.");
			setTimeout("tc_show_time_chart();", 100);
			return;
		}
	}

	while (st.length < 4) st = "0" + st;
	while (et.length < 4) et = "0" + et;
	$("#happyhour_st_hour_" + i).val( hh_myParseInt(st.slice(0,2)) );
	$("#happyhour_st_min_" + i).val( st.slice(2,4) );
	$("#happyhour_et_hour_" + i).val( hh_myParseInt(et.slice(0,2)) );
	$("#happyhour_et_min_" + i).val(et.slice(2,4));

	tc_hide_time_chart(200);
	hh_savehappyhours(hiddensaveid);
	hh_echohappyhour($("#"+hiddensaveid).val(), parentid, hiddensaveid);
}