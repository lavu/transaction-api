<?php

if (!$in_lavu)
{
	exit();
}

echo "<br><br>";

$forward_to	= "print_wrap.php?pw_mode=".$pw_mode;
$tablename	= "config";
$filter_by	= "`location` = '".$locationid."' AND `type` = 'printer_group'";

$kitchen_printers = array( '' => "" );
$pr_query = lavu_query("SELECT * FROM `config` WHERE `location` = '".$locationid."' AND `type` = 'printer' AND `setting` LIKE 'kitchen%' AND `_deleted` != '1' ORDER BY `value2` ASC");
while ($pr_read = mysqli_fetch_assoc($pr_query))
{
	$kitchen_printers[$pr_read['setting']] = $pr_read['value2'];
}

//Inserted by Brian D.
//Checking for post-back.  If is lls, we now need to sync down the config table.
if (!empty($_POST['posted']) && !empty($_POST['setting']) && !empty($_POST['posted_dataname']))
{
	$locinfo = sessvar('location_info');
	schedule_remote_to_local_sync_if_lls($locinfo, $locationid,  "table updated", "config", $sync_value_long="");
}

$fields = array(
		array(
				speak("Name").":",
				"setting",
				"text",
				"",
				"list:yes"
		),
		array(
				speak("Printer 1").":",
				"value",
				"select",
				$kitchen_printers,
				"list:yes"
		),
		array(
				speak("Printer 2").":",
				"value2",
				"select",
				$kitchen_printers,
				"list:yes"
		),
		array(
				speak("Printer 3").":",
				"value3",
				"select",
				$kitchen_printers,
				"list:yes"
		),
		array(
				speak("Printer 4").":",
				"value4",
				"select",
				$kitchen_printers,
				"list:yes"
		),
		array(
				speak("Printer 5").":",
				"value5",
				"select",
				$kitchen_printers,
				"list:yes"
		),
		array(
				speak("Printer 6").":",
				"value6",
				"select",
				$kitchen_printers,
				"list:yes"
		),
		array(
				"Location",
				"location",
				"hidden",
				"",
				"setvalue:".$locationid
		),
		array(
				"Type",
				"type",
				"hidden",
				"",
				"setvalue:printer_group"
		),
		array(
				speak("Save"),
				"submit",
				"submit"
		)
);

$form_max_width = 696;
require_once(resource_path()."/browse.php");

?>

<script type= 'text/javascript'>

	window.onload = function()
	{
		document.getElementById('zenbox_tab').style.display = 'none';
	}

	var a = document.getElementsByClassName('content')[0];
	document.getElementsByTagName('body')[0].innerHTML = a.innerHTML;
	document.getElementById('main_content_area').style.width = '600px';
	document.getElementsByTagName("body")[0].style.width = '600px';
	document.getElementsByTagName("body")[0].style.backgroundColor = 'white';
	document.getElementsByTagName("body")[0].style.overflowX = 'hidden';

</script>