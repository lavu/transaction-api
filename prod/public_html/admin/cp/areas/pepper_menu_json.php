<?php

require_once('/home/poslavu/public_html/inc/loyalty/pepper/PepperDataExtractor.php');

session_start();

// Do not load the page if they don't have a Lavu session
if (!sessvar_isset('admin_dataname'))
{
	return;
}

$dataExtractor = new PepperDataExtractor();

$result = $dataExtractor->getMenu($_GET);

$menu_json = json_encode($result);

$copy_button_str = speak("Copy JSON to Clipboard");

?>
<html lang="en">
<head>
<link rel="stylesheet" href="/cp/styles/styles.css" type="text/css" media="screen" charset="utf-8" />
<style>
body {
	background-color: transparent;
	color:white;
}
.main textarea {
	display: block;
	margin-left: auto;
	margin-right: auto;
	width: 60%;
	height: 60%;
	color: #aaaaaa;
}
.main button {
	display: block;
	margin-left: auto;
	margin-right: auto;
	margin-bottom: 5px;
	line-height: 2.0em;
	width: 60%;
}
#copy_result {
	display: block;
}
#instructions {
	margin-left: 12em;
}
a,
a:link,
a:visited {
  color: #aecd37;
  font-family: Arial, Helvetica, sans-serif;
  font-size: 12px;
  font-weight: 400;
}

a:hover {
  color: #aecd37;
  font-family: Arial, Helvetica, sans-serif;
  font-size: 12px;
  font-weight: 400;
}
</style>
</head>
<body>

<div class="main">
	<button class="contBtn" id="copy_button"><?= htmlspecialchars($copy_button_str) ?></button>
	<textarea id="menu_json"><?= htmlspecialchars($menu_json); ?></textarea>
</div>

<ol id="instructions">
	<li>Save the copied JSON to a text file</li>
	<li>Log into your <a href="https://console.pepperhq.com" target="_blank">Pepper Console</a></li>
	<li>Navigate to Content Management &gt; Menus</li>
	<li>Click the SELECT FILE button next to Menu JSON</li>
</ol>

<script>

var copyButtonStr = "<?= htmlspecialchars($copy_button_str) ?>";

var copyButton = document.getElementById("copy_button");
var copyResult = document.getElementById("copy_result");
var jsonTextarea = document.getElementById("menu_json");

function showResultMsg(msg) {
	copyButton.innerHTML = copyButtonStr +" "+ msg;
}

copyButton.addEventListener('click', function (event) {
	try {
		jsonTextarea.select();
		var copiedOk = document.execCommand('copy');
		(copiedOk) ? showResultMsg("✔️") : showResultMsg("✖️");
	}
	catch (e) {
		showResultMsg("✖️");
	}
});

</script>

</body>
</html>
