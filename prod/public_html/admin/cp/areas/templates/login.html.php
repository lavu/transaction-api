<?php

	require_once('/home/poslavu/public_html/admin/cp/resources/core_functions.php');
	if ( isset($_SERVER['IS_CP3_ENV']) && strtolower($_SERVER['IS_CP3_ENV']) === 'true') {
		if ( $_SERVER['REQUEST_METHOD'] == "GET" ) {
				$newLocation = $_SERVER['CP3_URL'].'/login?newLogin=true';

				if ($_SERVER['QUERY_STRING']) {
					$newLocation = $newLocation."&".$_SERVER['QUERY_STRING'];
				}

				header('Location: '.$newLocation);
				exit();
		}
	}
?>
<!doctype HTML>
<!-- <html> -->
<!--[if lt IE 7]><html class="ie ie6 oldie" lang="en"><![endif]-->
<!--[if IE 7]><html class="ie ie7 oldie" lang="en"><![endif]-->
<!--[if IE 8]><html class="ie ie8 oldie" lang="en"><![endif]-->
<!--[if IE 9]><html class="ie ie9 modern" lang="en"><![endif]-->
<!--[if gt IE 9]><!--><html class="no-ie modern" lang="en"><!--<![endif]-->
	<title>Admin Login</title>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta http-equiv="X-UA-Compatible" content="IE=8;IE=Edge"/>
	<link rel="stylesheet" type="text/css" href="styles/fontcustom/fontcustom.css">
	<link rel="stylesheet" type="text/css" href="styles/lavu-custom-input-elements.css">
	<link rel="stylesheet" type="text/css" href="styles/loginStyles.css">
	<script type="text/javascript" src="scripts/customelements.js"></script>
	<script type="text/javascript" src="scripts/lavu-custom-input-elements.js"></script>
	<script type="text/javascript" src="scripts/login.js"></script>
	<script type="text/javascript">
		var f = function( event ) {
			window.setTimeout(function(){
				var e = document.querySelector('lavu-container,[isa=lavu-container]');
				e.setAttribute('show','');
			}, 100);
		}
		document.addEventListener('DOMContentLoaded', f, false );

		function swapToSection( section ) {
			if( document.documentElement.classList.contains('ie8') || document.documentElement.classList.contains('ie9') ) {
				var visibleSections = document.querySelectorAll('lavu-section[show],[isa=lavu-section][show]');
				var ele = document.getElementById( section );
				var sectionsToHide = [];
				for( var i = 0; i < visibleSections.length; i++ ){
					sectionsToHide.push( visibleSections[i] );
					// visibleSections[i].removeAttribute( 'show' );
				}

				sectionsToHide.forEach(
					function( ele, i, arr ) {
						ele.removeAttribute('show');
						ele.setAttribute('hidden','');
					}
				);

				if( ele ){
					ele.removeAttribute('hidden');
					ele.setAttribute('show','');
				}
			} else {
				var visibleSections = document.querySelectorAll('lavu-section[show],[isa=lavu-section][show]');
				var ele = document.getElementById( section );
				var sectionsToHide = [];
				for( var i = 0; i < visibleSections.length; i++ ){
					sectionsToHide.push( visibleSections[i] );
					// visibleSections[i].removeAttribute( 'show' );
				}

				sectionsToHide.forEach(
					function( ele, i, arr ) {
						ele.removeAttribute('show');
						setTimeout(function(){
							ele.setAttribute('hidden','');
						}, 1000 );
					}
				);

				if( ele ){
					setTimeout(function(){
						ele.removeAttribute('hidden');
						setTimeout(function(){
							ele.setAttribute('show','');
						}, 10);
					},1000);
				}
			}
		}

		function setErrorMessage( message ){
			console.log( message );
			var ele = document.querySelector( '[show] lavu-error,[show] [isa=lavu-error]' );
			if( !ele ){
				return;
			}
			while( ele.childNodes.length ){
				ele.removeChild( ele.childNodes[0] );
			}
			var text = document.querySelector('[show] lavu-text-input,[show] [isa=lavu-text-input]' );
			var pw = document.querySelector('[show] lavu-password-input,[show] [isa=lavu-password-input]' );

			if( message != '' ){
				ele.appendChild( document.createTextNode(message) );
				ele.setAttribute('error','');

				if( text !== null ){
					text.setAttribute('error','');
				}
				if( pw !== null ){
					pw.setAttribute('error','');
				}
			} else {
				ele.removeAttribute('error');
				if( text !== null ){
					text.removeAttribute('error','');
				}
				if( pw !== null ){
					pw.removeAttribute('error','');
				}
				ele.appendChild( document.createTextNode(' ') );
			}
		}
	</script>
</head>
<body>
	<iframe name="proxy_submit" hidden style="display:hidden;">
	</iframe>
	<lavu-container>
		<lavu-top-container>
			<a href="https://www.lavu.com"><lavu-logo></lavu-logo></a>
		</lavu-top-container>
		<!-- <lavu-mid-container>
			<lavu-color-collection>
				<lavu-color-first></lavu-color-first>
				<lavu-color-second></lavu-color-second>
				<lavu-color-third></lavu-color-third>
				<lavu-color-fourth></lavu-color-fourth>
				<lavu-color-fifth></lavu-color-fifth>
				<lavu-color-sixth></lavu-color-sixth>
			</lavu-color-collection>
		</lavu-mid-container> -->
		<lavu-bottom-container>
			<lavu-relative>
				<lavu-section id="login_section" class="section" show>
					<span class="section_title">Log in</span>
					<form name="login" method="POST" target="proxy_submit">
						<lavu-text-input name="username" placeholder="Username"></lavu-text-input>
						<lavu-password-input name="password" placeholder="Password"></lavu-password-input>
						<lavu-button class="submit-login" ><lavu-glyph class="icon-forward"></lavu-glyph></lavu-button>
					</form>
					<lavu-error>&nbsp;</lavu-error>
					<div class="bottom">
						<a onclick="swapToSection('forgot_section');">Forgot Password?</a> | <a href="https://www.lavu.com/terms-of-service" target="_blank">Terms of Service</a><br />
						By continuing, you are agree to our <a href="https://www.lavu.com/terms-of-service" target="_blank">Terms of Service</a><br />
						<!-- <a onclick="swapToSection('reset_password_section');">Temporary Reset Section Access</a> -->
					</div>
				</lavu-section>
				<lavu-section id="forgot_section" class="section" hidden>
					<span class="section_title">Forgot Password</span>
					<lavu-text-input name="username" placeholder="Username"></lavu-text-input>
					<lavu-button class="submit-forgot" ><lavu-glyph class="icon-forward"></lavu-glyph></lavu-button>
					<lavu-error>&nbsp;</lavu-error>
					<div class="bottom">
						<a onclick="swapToSection('login_section');">Back to Login</a> | <a href="https://www.lavu.com/terms-of-service" target="_blank">Terms of Service</a><br />
						By continuing, you are agree to our <a href="https://www.lavu.com/terms-of-service" target="_blank">Terms of Service</a>
					</div>
				</lavu-section>
				<lavu-section id="reset_password_section" class="section" hidden>
					<span class="section_title">Reset Password</span>
					<lavu-text-input name="username_action" id="username_action" placeholder="Username"></lavu-text-input>
					<lavu-password-input name="password1" placeholder="New Password"></lavu-password-input>
					<lavu-password-input name="password2" placeholder="Confirm Password"></lavu-password-input>
					<input type="hidden" id="token_field" name="token_field" />
					<input type="hidden" id="mode" name="mode" value="perform_reset_pass" />
					<lavu-button class="submit-reset" ><lavu-glyph class="icon-forward"></lavu-glyph></lavu-button>
					<lavu-error>&nbsp;</lavu-error>
					<div class="bottom">
						<a onclick="swapToSection('login_section');">Back to Login</a> | <a href="https://www.lavu.com/terms-of-service" target="_blank">Terms of Service</a><br />
						By continuing, you are agree to our <a href="https://www.lavu.com/terms-of-service" target="_blank">Terms of Service</a><br />
						<!-- <a onclick="swapToSection('reset_password_section');">Temporary Reset Section Access</a> -->
					</div>
				</lavu-section>
			</lavu-relative>
		<lavu-bottom-container>
	</lavu-container>
</body>
<!--[if IE 8]><script type="text/javascript">
	new window.IE8RestructureMisinterprettedDOM();
</script><![endif]-->
<script type="text/javascript">
	(function(){
		/**
		 * Login Section
		 */
		var loginUserName = document.querySelector( '#login_section lavu-text-input, #login_section [isa=lavu-text-input]' );
		var loginPassword = document.querySelector( '#login_section lavu-password-input, #login_section [isa=lavu-password-input]' );
		var loginButton = document.querySelector( '#login_section lavu-button, #login_section [isa=lavu-button]' );
		var lavuError = document.querySelector( '#login_section lavu-error, #login_section [isa=lavu-error]' );

		var loginLoginSubmissionValidator = Lavu.login.LoginSubmissionValidator.sharedValidator();
		loginLoginSubmissionValidator.addField( 'username', loginUserName );
		loginLoginSubmissionValidator.addField( 'password', loginPassword );
		loginLoginSubmissionValidator.setSubmissionButton( loginButton );
		loginLoginSubmissionValidator.setMessageArea( lavuError );
	})();

	(function(){
		/**
		 * Forgot Section
		 */
		var loginUserName = document.querySelector( '#forgot_section lavu-text-input, #forgot_section [isa=lavu-text-input]' );
		var loginButton = document.querySelector( '#forgot_section lavu-button, #forgot_section [isa=lavu-button]' );
		var lavuError = document.querySelector( '#forgot_section lavu-error, #login_section [isa=lavu-error]' );

		var forgotPasswordSubmissionValidator = Lavu.login.ForgotPasswordSubmissionValidator.sharedValidator();
		forgotPasswordSubmissionValidator.addField( 'username_reset', loginUserName );
		forgotPasswordSubmissionValidator.setSubmissionButton( loginButton );
		forgotPasswordSubmissionValidator.setMessageArea( lavuError );
	})();

	(function(){
		/**
		 * Reset Password Section
		 */
		var loginUserName = document.querySelector( '#reset_password_section lavu-text-input, #reset_password_section [isa=lavu-text-input]' );
		var passwordFields = document.querySelectorAll( '#reset_password_section lavu-password-input, #reset_password_section [isa=lavu-password-input]' );
		var loginPassword1 = passwordFields[0];
		var loginPassword2 = passwordFields[1];
		var modeField = document.querySelector( 'input#mode' );
		var tokenField = document.querySelector( 'input#token_field' );
		var loginButton = document.querySelector( '#reset_password_section lavu-button, #reset_password_section [isa=lavu-button]' );
		var lavuError = document.querySelector( '#reset_password_section lavu-error, #login_section [isa=lavu-error]' );

		var resetPasswordSubmissionValidator = Lavu.login.ResetPasswordSubmissionValidator.sharedValidator();
		resetPasswordSubmissionValidator.addField( 'mode', modeField );
		resetPasswordSubmissionValidator.addField( 'token', tokenField );
		resetPasswordSubmissionValidator.addField( 'username_action', loginUserName );
		resetPasswordSubmissionValidator.addField( 'password1', loginPassword1 );
		resetPasswordSubmissionValidator.addField( 'password2', loginPassword2 );
		resetPasswordSubmissionValidator.setSubmissionButton( loginButton );
		resetPasswordSubmissionValidator.setMessageArea( lavuError );
	})();
</script>
<![CDATA[EXTRA_SCRIPTS_TO_RUN]]>
</html>
