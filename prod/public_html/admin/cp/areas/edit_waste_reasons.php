<?php
if (!isset($mode))
	if (isset($_GET['mode']))
		$mode = $_GET['mode'];
		else
			$mode = "";

			if (isset($_GET['edit']) && $mode == "edit_waste_reasons") {
				
				echo speak("Add or Remove Inventory Waste Reasons").".";

				echo "<br /><br />";

				$display = "";
				$field = "";

				$qtitle_category = false;
				$qtitle_item = "Reason";
				$inventory_tablename = "inventory_waste_reasons";
				$inventory_primefield = "reason";					
				$inventory_orderby = "reason";

				$qfields = array();
				$qfields[] = array("reason", "reason", speak("Reason"), "input");
				$qfields[] = array("id","id");
				$qfields[] = array("delete");

				require_once(resource_path() . "/dimensional_form.php");
				$qdbfields = get_qdbfields($qfields);

				$details_column = "";//"Zip Codes";
				$send_details_field = "";//"zip_codes";

				$cat_count = 1;
				
				if (isset($_POST['posted']))
				{
				
					$item_count = 1;
					while(isset($_POST["ic_".$cat_count."_".$inventory_primefield."_".$item_count]))
					{
						$item_value = array(); // get posted values
						for($n=0; $n<count($qfields); $n++)
						{
							$qname = $qfields[$n][0];
							$qtitle = (isset($qfields[$n][2]))?$qfields[$n][2]:false;
							$set_item_value = $_POST['ic_'.$cat_count.'_'.$qname.'_'.$item_count];
							if($qtitle && $set_item_value==$qtitle) $set_item_value = "";
								
							$item_value[$qname] = $set_item_value;
						}
						$item_id = $item_value['id'];
						$item_delete = $item_value['delete'];
						$item_name = $item_value[$inventory_primefield];
				
						if ($item_id == "0") {
								
							$dbfields = array();
							for($n=0; $n<count($qdbfields); $n++)
							{
								$qname = $qdbfields[$n][0];
								$qfield = (isset($qdbfields[$n][1]))?$qdbfields[$n][1]:$qname;
								$dbfields[] = array($qfield,$item_value[$qname]);
							}
							update_poslavu_dbrow($inventory_tablename,$dbfields, "");
								
						} else {
				
							if($item_delete=="1")
							{
								delete_poslavu_dbrow($inventory_tablename, $item_id);
							}
							else if($item_name!="")
							{
								$dbfields = array();
								for($n=0; $n<count($qdbfields); $n++)
								{
									$qname = $qdbfields[$n][0];
									$qfield = (isset($qdbfields[$n][1]))?$qdbfields[$n][1]:$qname;
									$dbfields[] = array($qfield,$item_value[$qname]);
								}
								update_poslavu_dbrow($inventory_tablename,$dbfields, $item_id);
							}
						}
						$item_count++;
					}
				
					// LOCAL SYNC RECORD
					schedule_remote_to_local_sync($locationid,"table updated",$inventory_tablename);
					// END LOCAL SYNC RECORD
				}
				
				$qinfo = array();
				$qinfo["qtitle_category"] = $qtitle_category;
				$qinfo["qtitle_item"] = $qtitle_item;
				$qinfo["inventory_tablename"] = $inventory_tablename;
				$qinfo["inventory_orderby"] = $inventory_orderby;

				$form_name = "tilling";
				$field_code = create_dimensional_form($qfields,$qinfo,'');
				$display .= "<table cellspacing='0' cellpadding='3'><tr><td align='center' style='border:2px solid #DDDDDD'>";
				$display .= "<form name='tilling' method='post' action=''>";
				$display .= "<input type='hidden' name='posted' value='1'>";
				$display .= $field_code;
				$display .= "&nbsp;<br><input type='submit' value='".speak("Save")."' style='width:120px'>";
				$display .= "</form>";
				$display .= "</td></tr></table>";

				echo $display;
			}
			?>