<?php
session_start();
require_once (dirname(dirname(dirname(__FILE__))) ."/cp/resources/core_functions.php");

$data_name = sessvar('admin_dataname');
$code = $_REQUEST['code'];
$symbol = $_REQUEST['symbol'];
$symbol_placement= $_REQUEST['placement'];
$decimal_separator= $_REQUEST['separator'];

$secondvalue_query=lavu_query("select * from config where setting='secondary_currency_code'");
$currency_code_data = mysqli_fetch_assoc($secondvalue_query);
$currency_code = $currency_code_data['value2'];
if ($currency_code==$code) {
	echo "failure"; exit;
} else {
	$query = " SELECT monetary_symbol,currency_separator,alphabetic_code  FROM `poslavu_MAIN_db`.`country_region`  WHERE id=[1] AND active=[2]  LIMIT 1 ";
	$result = lavu_query($query, $code, 1);
	$currency_data= mysqli_fetch_assoc($result);
	$secondary_query=lavu_query("select * from config where setting='primary_currency_code'");
	if (mysqli_num_rows($secondary_query)) {
		lavu_query("UPDATE `config` set `value` = '$currency_data[alphabetic_code]',`value2` = '$code' where `setting` = 'primary_currency_code'");
	} else {
		lavu_query("INSERT INTO `config` (`location`,`setting`,`value`,`value2`,`type`,`_deleted`) VALUES ('1','primary_currency_code','$currency_data[alphabetic_code]','$code','location_config_setting','0')");
	}
	lavu_query("UPDATE `locations` set `monitary_symbol` = '$symbol' where `id` = '1'");
	lavu_query("UPDATE `locations` set `left_or_right` = '$symbol_placement' where `id` = '1'");
	lavu_query("UPDATE `locations` set `decimal_char` = '$decimal_separator' where `id` = '1'");
	echo "success";
}
?> 