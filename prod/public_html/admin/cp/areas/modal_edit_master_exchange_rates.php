<?php
/**
 * Created by PhpStorm.
 * User: Leif
 * Date: 1/19/17
 * Time: 9:15 AM
 */

	require_once(dirname(__FILE__)."/../resources/core_functions.php");
	require_once(resource_path()."/lavuquery.php");



	session_start($_GET['session_id']);
	$req_schm = "https";
	if ($_SERVER['HTTP_HOST'] == 'admin.localhost' || $_SERVER['HTTP_HOST'] == 'localhost:8888') {
		$req_schm = "http";
	}
	$mode = "settings_edit_master_exchange_rates";
	$edit = "1";
	$display = "<head>";

	$display .= "<link rel='stylesheet' type='text/css' href='/cp/styles/styles.css'>";
	$display .= "<script type=\"text/javascript\" src=\"/manage/js/jquery/js/jquery-1.9.0.js\"></script>";
	$display .= "<script type=\"text/javascript\" src=\"/manage/js/jquery/js/jquery-ui-1.10.0.custom.min.js\"></script>
			<link rel=\"stylesheet\" href=\"/manage/js/jquery/css/smoothness/jquery-ui-1.10.0.custom.min.css\" />
			<link href=\"/cp/css/select2-4.0.3.min.css\" rel=\"stylesheet\" />
			<script src=\"/cp/scripts/select2-4.0.3.min.js\"></script>";
	if(isset($_GET['refresh_for_save']) && $_GET['refresh_for_save'] == '1') {
		$_GET['refresh_for_save'] = null;
		$display .= "<script type='text/javascript'>
							window.parent.document.location.reload();
						</script>
					";
	}
	$display .= "</head>";
	$display .= "<body>";

	$display .= "<div id='exchange_rate_page_content'>";

	$field = "";

	$qtitle_category = false;
	$qtitle_item = "Currency";

	$category_tablename = "";
	$inventory_tablename = "`master_currencies`";
	$category_fieldname = "";
	$inventory_fieldname = "setting";
	$inventory_select_condition = "";
	$inventory_primefield = "setting";
	$category_primefield = "";
	$inventory_orderby = "'id'";


	$currencies = array();
	$currencyCodes = getCurrencyCodes();
	$currencies[0][0] = $currencyCodes[0]['name'];
	$currencies[0][1] = $currencyCodes[0]['id'];

	$encoding = "UTF-8";
	$html_encoding = "HTML-ENTITIES";
	for ($i = 1; $i < count($currencyCodes); $i++) {
		$code = mb_convert_encoding($currencyCodes[$i]['alphabetic_code'], $encoding);
		$code = mb_convert_encoding($code, $html_encoding, $encoding);

		$val = mb_convert_encoding($currencyCodes[$i]['name'], $encoding);
		$val = mb_convert_encoding($val, $html_encoding, $encoding);
		$displayVal = "[" . $code . "] " . $val;

		$currencies[$i][0] = $displayVal;
		$currencies[$i][1] = $currencyCodes[$i]['id'];
	}

	$location_info = sessvar("location_info", null);

	$qfields = array();
	$qfields[] = array("enabled", "enabled", "", "bool", "1");
	$qfields[] = array("master_currency_code", "master_currency_code", speak("Type"), "select", $currencies);

	require_once(resource_path() . "/dimensional_form.php");
	$qdbfields = get_qdbfields($qfields);

	$details_column = "";//"Zip Codes";
	$send_details_field = "";//"zip_codes";
	$category_filter_by = "";
	$category_filter_value = "";
	$cfields = "";

	$qinfo = array();
	$qinfo["qtitle_category"] = $qtitle_category;
	$qinfo["qtitle_item"] = $qtitle_item;
	$qinfo["category_tablename"] = $category_tablename;
	$qinfo["inventory_tablename"] = $inventory_tablename;
	$qinfo["category_fieldname"] = $category_fieldname;
	$qinfo["inventory_fieldname"] = $inventory_fieldname;
	$qinfo["inventory_select_condition"] = $inventory_select_condition;
	$qinfo["inventory_primefield"] = $inventory_primefield;
	$qinfo["category_primefield"] = $category_primefield;
	$qinfo["details_column"] = $details_column;
	$qinfo["send_details"] = $send_details_field;
	$qinfo["field"] = $field;
	$qinfo["qsortby_preference"] = "";
	$qinfo['qtitle_category_plural'] = "";
	$qinfo["category_filter_by"] = $category_filter_by;
	$qinfo["category_filter_value"] = $category_filter_value;
	$qinfo["inventory_filter_value"] = "exchange_rates";
	$qinfo['inventory_filter_by'] = "";
	$qinfo["inventory_orderby"] = $inventory_orderby;

	$form_name = "master_exchange_rates";
	$form_name = "master_exchange_rates";

	$RETURN_URL = $req_schm . "://" . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
	$submission_url = $req_schm . "://" . $_SERVER['HTTP_HOST'] . "/cp/resources/submit_master_exchange_rates.php";
	$field_code = create_dimensional_form($qfields, $qinfo, $cfields);
	$display .= "<div id='masterCurrencyFields'><span>Active Currency Code</span></div>";
	$display .= "<table id='exchange_rate_form_table' cellspacing='0' cellpadding='3'><tr><td align='center'>";
	$display .= "<form id='$form_name' name='$form_name' method='post' action='$submission_url'>";
	$display .= "<input type='hidden' name='posted' value='1'>";
	$display .= "<input type='hidden' name='returnURL' value='$RETURN_URL'>";
	$display .= $field_code;

	$display .= "&nbsp;<br><input type='submit' id='btn_exchange_rate_form' value='" . speak("Save") . "' style='width:120px'>";

	$display .= "</form>";
	$display .= "</td></tr></table>";
	$display .= "</div>"; //end of exchange rate display content - div
	$display .= getFullDropdownList($currencies);
	$display .= "</body>";

	/**********************************************************************************************************************/
//									SCRIPTS
	/**********************************************************************************************************************/
	$display .= '<script type="text/javascript">
			var usedCurrencyOptions = [];
			var availableCurrencyList = [];
			var arrDefaultChkBoxSelected = [];
			var arrDefaultUnChkBoxSelected = [];
			var arrCurrentChkBoxSelected = [];
			var arrCurrentUnChkBoxSelected = [];
			var intDefaultLastIndexID ;

			(function($) {
				$(document).ready(function() {
					$("input[name^=\'ic_1_effective_date\']").datepicker({ 
						dateFormat: \'mm/dd/y\', minDate: 0  
					});
			
					$("#tdButton").on("click", "td", function() {
     					$( "input[name^=\'ic_1_effective_date\']" ).datepicker({ 
							dateFormat: \'mm/dd/y\',minDate: 0 
						});
   					});
					
					$("#add_button1").on( "click", "span", function( event ) {
						$( "input[name^=\'ic_1_effective_date\']" ).datepicker({ 
							dateFormat: \'mm/dd/y\',minDate: 0 
						});
					});
					
					$("#add_button2").on( "click", "span", function( event ) {
						$( "input[name^=\'ic_1_effective_date\']" ).datepicker({ 
							dateFormat: \'mm/dd/y\',minDate: 0 
						});
					});
					
					$("#add_button_with_text").on( "click", "span", function( event ) {
						$( "input[name^=\'ic_1_effective_date\']" ).datepicker({ 
							dateFormat: \'mm/dd/y\',minDate: 0 
						});
					});
					
					$(\'form[name="master_exchange_rates"]\').find(\'select\').each(function() {
							usedCurrencyOptions.push($(this).val());
					});
					
					updateAvailableCurrencies();
				});
			})(jQuery);
		
			function updateAvailableCurrencies(){
				availableCurrencyList = [];
				fullCurrencyList.map(function(item){
					if(usedCurrencyOptions.indexOf(item["key"]) < 0){
						availableCurrencyList.push(item);	
					}
				});
			}
			
			function updateUsedCurrencies(){
				usedCurrencyOptions = [];
				$(\'form[name="master_exchange_rates"]\').find(\'select\').each(function() {
					var thisval = $(this).val();
					usedCurrencyOptions.push(thisval);
				});

			}
			
			function checkForDuplicateSelections(){
				var offendingValues = [];
				for(var i = 0; i < usedCurrencyOptions.length-1; i++){
					if(usedCurrencyOptions.slice(i+1).indexOf(usedCurrencyOptions[i]) >= 0 && usedCurrencyOptions[i] != "0"){
						offendingValues.push(usedCurrencyOptions[i]);
					}
				}

				return offendingValues;
			}
			(function($) {
				$(document).ready(function() {
					var preventSelect2ChangeForPreviouslySavedItems = false;

					$(".addBtn").click(function(){
						if(preventSelect2ChangeForPreviouslySavedItems == true){
							$("select[name^=\'ic_1_master_currency_code\']").last().select2({
							    //dropdownAutoWidth : true,
								placeholder: "Select Currency Code",
								data: fullCurrencyList,
								allowClear: true
								
							});
						}else{
							preventSelect2ChangeForPreviouslySavedItems = true;
						}
						// Add an on-change listener to re-create the used currencies list.
						$("select[name^=\'ic_1_master_currency_code\']").on("change", function(){
							updateUsedCurrencies();
						});
					});

				});
		
				$("#ic_area_cat1").find(\'select \').each(function() {
					if($(this).val()!=""){
						$(this).prop(\'disabled\', true);
					}
				});
	
				$(document).ready(function() {
					$("select[name^=\'config:currency_region\']").on("change", function(){
						var selectedRegionID = $(this).val();
						$("select[name^=\'config:primary_currency_code\']").empty();
						$("input[name=monitary_symbol]").val("");
						if(selectedRegionID > 0){
							$("input[name=monitary_symbol]").val("");
							$.ajax({
								url: "index.php?widget=advanced_location_settings&als_ajax&regionid="+selectedRegionID,
								data: { regionid: selectedRegionID },
								type: "POST",
								async: true,
								success: function(data) {
									$("select[name^=\'config:primary_currency_code\']").empty();
									$("select[name^=\'config:primary_currency_code\']")
											.append("<option value=\'\'>Select Currency Code</option>");
									var i=0;
									for(i=0;i<data.length;i++){
										$("select[name^=\'config:primary_currency_code\']")
											.append("<option value=\'"+data[i].alphabetic_code+"\'>"+data[i].alphabetic_code+"</option>");
											//.append("<option value=\'"+data[i].alphabetic_code+"\'>"+data[i].name+" ("+data[i].alphabetic_code+")</option>");
									}
								},
								error: function() {
									alert("Seems problem with server loading the primary currency");
								}
							});
						}else{
							alert("Please select Currency Region!");
						}
					});
					
					$("select[name^=\'config:primary_currency_code\']").on("change", function(){
						var currencyCode = $(this).val();
						$("input[name=monitary_symbol]").val("");
						if(currencyCode!=""){
							var selectedRegionId = $("select[name^=\'config:currency_region\']").val();
							$.ajax({
								url: "index.php?widget=advanced_location_settings&get_monetary_symbol=1",
								data: { code : currencyCode, regionId:selectedRegionId},
								type: "POST",
								async: true,
								success: function(data) {
									console.log(data[0]);
									if(data!=null){
										if(data[0].monetary_symbol!=null){
											$("input[name=monitary_symbol]").val(data[0].monetary_symbol);
										}
									}
								},
								error: function() {
									alert("Seems problem with server loading the primary currency");
								}
							});
						}else{
							alert("Please select Primary Currency Code!");
						}
					});
				});
			
				$(document).on(\'click\', \'form[name="master_exchange_rates"] #btn_exchange_rate_form\', function(e) {


							var submitFlag = 0;
							var dupList = checkForDuplicateSelections();
							$(\'form[name="master_exchange_rates"]\').find(\'select\').each(function() {
								$(this).css(\'border\', \'1px solid #ccc\');
							})
							$(\'form[name="master_exchange_rates"]\').find(\'span[id*=master_currency_code]\').each(function() {
								$(this).css(\'border\', \'1px solid #ccc\');
							});
							
							for(var i = 0; i < dupList.length; i++){
								$(\'form[name="master_exchange_rates"]\').find(\'select\').each(function() {
									if($(this).val() == dupList[i] || $(this).text() == "Select Currency Code"){
										submitFlag++;
										$(this).css(\'border\', \'1px solid red\');
									}
									
								});

								$(\'form[name="master_exchange_rates"]\').find(\'span[id*=master_currency_code]\').each(function() {
									var fullId = $(this).attr("id");
									//console.log("ID:" + fullId);
		
									var trimmedId = fullId.replace("select2-", "");
									trimmedId = trimmedId.replace("-container","");
									var selVal = $("#"+trimmedId).val();
								
									if(selVal == dupList[i]){
										submitFlag++;
										$(this).css(\'border\', \'1px solid red\');
									}
								});
							}

							if(submitFlag == 0){

								var msgConfirm = "Are you sure you want to save? Changes are permanent!";
								var msgEditConfirm = "Are you sure you want to continue with the new changes? ";

								strDefaultChkBoxSelected = arrDefaultChkBoxSelected.join("#");
								strDefaultUnChkBoxSelected = arrDefaultUnChkBoxSelected.join("#");

								strCurrentChkBoxSelected = arrCurrentChkBoxSelected.join("#");
								strCurrentUnChkBoxSelected = arrCurrentUnChkBoxSelected.join("#");

								var div = document.getElementById(\'dialog-confirm\');
								div.innerHTML = "";

								if((strDefaultChkBoxSelected !== strCurrentChkBoxSelected) && (strDefaultUnChkBoxSelected !== strCurrentUnChkBoxSelected)) {
									div.innerHTML = div.innerHTML + msgEditConfirm;
								}else{
									div.innerHTML = div.innerHTML + msgConfirm;
								}
								var isConfirmed = false;
								if(!isConfirmed){
									var dialog = $("#dialog-confirm");
									dialog.load(
											$("#dialog-confirm").dialog({
											resizable: false,
											modal: true,
											height: "auto",
											width: 400,
											position: [\'middle\',28],
											buttons: {
												"Ok": function() {
														isConfirmed = true;
														$(\'#modal_close_0\', parent.document).hide();
														$(\'form[name="master_exchange_rates"]\').submit();
														$(this).dialog("close");
												},
												Cancel: function() {
														$(this).dialog("close");
												}
											}
										})
									);
				                   e.preventDefault();
				                   return false;
				               }
				               else
				                  return true;
							}else{
						        e.preventDefault();
						    }
		    		});


					/**************** Start code for checking alteration of the existing record  **************************/

					$(document).on("click", "form[name=\"master_exchange_rates\"] ", function (e) {

					    var isCheckbox = $(e.target).is(":checkbox");

					    if(isCheckbox) {

					        var intCurrentIndexID = $(e.target).prop("id");

					        intCurrentIndexID = intCurrentIndexID.replace("ic_1_enabled_", "");
					        intCurrentIndexID = intCurrentIndexID.replace("_cb", "");


					        if(parseInt(intCurrentIndexID) <= parseInt(intDefaultLastIndexID)) {
					       
								arrCurrentChkBoxSelected = [];
								arrCurrentUnChkBoxSelected = [];

								for(var i = 1; i <= parseInt(intDefaultLastIndexID); i++) {
								    var isChk = $("#ic_1_enabled_" + i + "_cb").is(":checked ");
								    var isChkID = $("#ic_1_enabled_" + i + "_cb").prop("id");
								    							    
								    if(isChk == true) {
								        arrCurrentChkBoxSelected.push(isChkID);
								    }
								    else {
								        arrCurrentUnChkBoxSelected.push(isChkID);
								    }
								}
					        }
					    }

					});

					$(document).one("mousemove", "form[name=\"master_exchange_rates\"] ", function (e) {
					    
						arrDefaultChkBoxSelected = [];
						arrDefaultUnChkBoxSelected = [];

						for(var i = 0; i < this.elements.length; i++) {
							 if(this.elements[i].type == "checkbox") {
						        if(this.elements[i].checked == true) {
						            arrDefaultChkBoxSelected.push(this.elements[i].getAttribute("id"));
						        }
						        else {
						            arrDefaultUnChkBoxSelected.push(this.elements[i].getAttribute("id"));
						        }
						    }
						}

					    arrCurrentChkBoxSelected = arrDefaultChkBoxSelected;
					    arrCurrentUnChkBoxSelected = arrDefaultUnChkBoxSelected;

					   
					    if (arrCurrentUnChkBoxSelected.length > 0 )
					   		intDefaultLastIndexID =  arrCurrentUnChkBoxSelected[arrCurrentUnChkBoxSelected.length - 1];					
						else
							intDefaultLastIndexID =  arrCurrentChkBoxSelected[arrCurrentChkBoxSelected.length - 1];	

					
					    intDefaultLastIndexID = intDefaultLastIndexID.replace("ic_1_enabled_", "");
					    intDefaultLastIndexID = intDefaultLastIndexID.replace("_cb", "");


					});

					/**************** End code for checking alteration of the existing record **************************/
		


			})(jQuery);	
		            
		    // Validates that the input string is a valid date formatted as "mm/dd/yyyy"
		    function isValidDate(dateString){
		        // First check for the pattern
		        if(!/^\d{1,2}\/\d{1,2}\/\d{2}$/.test(dateString)){
		            return false;
		        }
		        // Parse the date parts to integers
		        var parts = dateString.split("/");
		        var day = parseInt(parts[1], 10);
		        var month = parseInt(parts[0], 10);
		        var year = parseInt(parts[2], 10);
		
		        // Check the ranges of month and year
		        if(year < 00 || year > 99 || month == 0 || month > 12)
		            return false;
		
		        var monthLength = [ 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 ];
		
		        // Adjust for leap years
		        if(year % 400 == 0 || (year % 100 != 0 && year % 4 == 0))
		            monthLength[1] = 29;
		
		        // Check the range of the day
		        return day > 0 && day <= monthLength[month - 1];
		    };
	
			//validate rate field value for positive float
			function checkNumeric(elementVal){
				if((!/^(?=.+)(?:[1-9]\d*|0)?(?:\.\d+)?$/.test(elementVal))||(!/^[0-9].*$/.test(elementVal))){
		            return false;
		        }
		        if(parseFloat(elementVal)>parseFloat("99999.999999")){
					alert("Exchange rate must be less than 100,000");
		            return false;
		        }
		        return true;
			}
						
			</script>';


			echo $display;
			/**********************************************************************************************************************/
	//									FUNCTIONS
			/**********************************************************************************************************************/
			function getCurrencyCodes()
			{
				$currency_country = array();
				$currency_country[0]['name'] = speak("Select Currency Code");
				$currency_country[0]['id'] = "0";
				$query = "SELECT `id`, `name`, `alphabetic_code` FROM `poslavu_MAIN_db`.`country_region` Order By `name`";
				$result = mlavu_query($query);
				if (!$result) {
					error_log("mysql error in " . __FILE__ . " mysql error:" . lavu_dberror());
				} else {
					$index = 1;
					while ($region_info = mysqli_fetch_assoc($result)) {
						$currency_country[$index]['id'] = $region_info['id'];
						$currency_country[$index]['alphabetic_code'] = $region_info['alphabetic_code'];
						$currency_country[$index]['name'] = $region_info['name'];
						$index++;
					}
				}
				return $currency_country;
			}

			function getFullDropdownList($currencyCodes)
			{
				$listString = "<script type='text/javascript'>";
				$listArray = array();
				for ($i = 0; $i < count($currencyCodes); $i++) {
					$listArray[$i] = array("id" => $currencyCodes[$i][1], "text" => $currencyCodes[$i][0]);
				}
				$listString .= " var fullCurrencyList=" . json_encode($listArray) . ";";
				$listString .= "</script>";
				return $listString;
			}

?>
<div id="dialog-confirm" title="Confirmation" style="display:none;">
</div>
