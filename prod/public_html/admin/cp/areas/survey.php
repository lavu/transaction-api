<?php

/**********************************************************************************************************************************
* draws a query, once every 3 months, asking the customer how satified they are with their distributor and with lavu
* only draws the query:
*    if the ipaddress is not a distributor address or the ipaddress is the most commonly used address on the account
*    if the distro's most active ip isn't this ip or the distro hasn't logged in today
*    if the account is more than 25 days old (to give the account time to get set up)
*    if the previous results are more than 3 months old
*
* author: Benjamin Bean (benjamin@poslavu.com)
**********************************************************************************************************************************/

require_once(dirname(__FILE__)."/../resources/core_functions.php");
require_once(dirname(__FILE__)."/../resources/lavuquery.php");
global $loggedin_id;
if (!isset($loggedin_id))
	$loggedin_id = TRUE;
require_once(dirname(__FILE__).'/../resources/contact_zendesk.php');
require_once(dirname(__FILE__)."/../../manage/globals/email_addresses.php");
require_once(dirname(__FILE__)."/../../sa_cp/billing/payment_profile_functions.php");
if (!function_exists("is_dev")) {
	function is_dev(){
		if(strpos($_SERVER['REQUEST_URI'],"v2/")!==false)
			return true;
		else
			return false;
	}
}
if (!isset($maindb))
	$maindb = "poslavu_MAIN_db";

get_current_survey();

$survey_submit_button = postvar('survey_submit_button', '');
$command = postvar('command', '');
$value2 = postvar('value2', postvar('distributor_interaction', ''));
$value5 = postvar('value5', postvar('distributor_satisfaction', ''));
$value4 = postvar('value4', postvar('poslavu_satisfaction', ''));
$value_long = postvar('value_long', postvar('poslavu_feedback', 'No Comment'));
$dataname = postvar('dataname', '');

if ($survey_submit_button == 'Submit' && $command !== '' && $dataname !== '') {
	switch ($command) {
	case  'submit_survey_results':
		echo submit_survey_results($dataname, $value2, $value4, $value5, $value_long);
		break;
	default:
		return 'failed|bad command';
	}
}

function get_current_survey($value2 = '', $value4 = '', $value5 = '', $value_long = '') {
	$a_surveys = array(
	'2014-04-21'=> array(
		'table_settings'=>array(
			'setting'=>'satisfaction survey',
			'type'=>'log',
			'value'=>date("Y-m-d H:i:s"),
			'value2'=>$value2,
			'value3'=>'/cp/areas/survey.php, value:date, value2:distributor_interaction, value4:lavu_satisfaction, value_long:poslavu_feedback, value5:distro_satisfaction',
			'value4'=>$value4,
			'value_long'=>$value_long,
			'value5'=>$value5
		),
		'distro_questions'=>array(
			array("type"=>"rating", "name"=>"distributor_interaction", "min"=>1, "max"=>5, "maxtext"=>"Often", "cluealign"=>"side", "question"=>"How often do you interact with your distributor?"),
			array("type"=>"rating", "name"=>"distributor_satisfaction", "min"=>1, "max"=>5, "maxtext"=>"Best", "cluealign"=>"side", "question"=>"How satisfied are you with your distributor?")
		),
		'restaurant_questions'=>array(
			array("type"=>"rating", "name"=>"poslavu_satisfaction", "min"=>1, "max"=>5, "maxtext"=>"Best", "cluealign"=>"side", "question"=>"How satisfied are you with POS Lavu?"),
			array("type"=>"textarea", "name"=>"poslavu_feedback", "question"=>"Additional feedback (optional):", "placeholder"=>"No Comment")
		)
	),
	'2013-08-01'=> array(
		'table_settings'=>array(
			'setting'=>'lavu satisfaction survey',
			'type'=>'log',
			'value'=>date('Y-m-d H:i:s'),
			'value3'=>'/cp/areas/survey.php, value:date, value4:lavu_satisfaction, value_long:poslavu_feedback',
			'value4'=>$value4,
			'value_long'=>$value_long
		),
		'distro_questions'=>array(),
		'restaurant_questions'=>array(
			array("type"=>"rating", "name"=>"poslavu_satisfaction", "min"=>1, "max"=>5, "maxtext"=>"Best", "cluealign"=>"side", "question"=>"How satisfied are you with POS Lavu?"),
			array("type"=>"textarea", "name"=>"poslavu_feedback", "question"=>"<span class='sadText'>We see you are dissatisfied. How can we make you happy?</span><span style='display:none;' class='happyText'><span style='display:none;' class='extravegantText'>Great! </span>Do you have any feedback for us?</span>", "placeholder"=>"No Comment", "style"=>"display:none;")
		),
		"javascript"=>"
			var satisfactions = $('input[name=poslavu_satisfaction]');
			$.each(satisfactions, function(k,v) {
				$(v).click(function(){
					var container = $('textarea[name=poslavu_feedback]').parent();
					var sadText = container.find('.sadText');
					var happyText = container.find('.happyText');
					var extravegantText = container.find('.extravegantText');
					container.stop(true,true);
					sadText.hide();
					happyText.hide();
					extravegantText.hide();
					if (parseInt($(this).val()) <= 2) {
						sadText.show();
					} else {
						happyText.show();
						if (parseInt($(this).val()) > 3) {
							extravegantText.show();
						}
					}
					container.show(200);
				});
			});",
		'triggers'=>array(
			new ZendeskTicketGenerator(
				array('action'=>'result', 'name'=>'lavu_satisfaction', 'match'=>'=', 'values'=>array('1','2','3','4')),
				array(
				'email'=>'phone@rustedjunk.net',
				'group_id'=>ZENDESK::get_zendesk_user_id('Support', 'group'),
				'description'=>"Hello, \n \nPlease follow up with this customer (if appropriate) about the following survey feedback. Advise them that we received their feedback from the survey and that we would like to help them resolve any issues that they are having. \n \ndataname: --account-dataname-- \nsurvey: lavu satisfaction survey \ndate: --special-datetime-- \nlavu_satisfaction: --value-lavu_satisfaction-- \nposlavu_feedback: --value-poslavu_feedback--.",
				'subject'=>'Lavu Satisfaction Survey Follow Up',
				'tags'=>array("created_through_cp_survey", "multiple_issues")
			))
		)
	));
	foreach($a_surveys as $k=>$v)
		if (!isset($a_surveys[$k]['triggers']))
			$a_surveys[$k]['triggers'] = array();
	global $use_survey_array;
	$use_survey_array = $a_surveys['2013-08-01'];
}

// draws a query, once every 3 months, asking the customer how satified they are with their distributor and with lavu
// only draws the query:
//    if the ipaddress is not a distributor address or the ipaddress is the most commonly used address on the account
//    if the distro's most active ip isn't this ip or the distro hasn't logged in today
//    if the account is more than 25 days old (to give the account time to get set up)
//    if the previous results are more than 3 months old
function draw_satisfaction_query($s_dataname) {
	global $use_survey_array;
	$previous_query_older_than = "3 months ago";
	$i_prev_query_time = strtotime($previous_query_older_than);
	$s_current_ip = $_SERVER['REMOTE_ADDR'];

	// check that this message hasn't been displayed in the last three months
	$query_string = "SELECT * FROM `poslavu_MAIN_db`.`survey` WHERE `dataname`='[dataname]' AND `setting`='[setting]' AND `type`='[type]' AND `value`>'[mindate]' ORDER BY `value` DESC LIMIT 1";
	$query_vars = array("dataname"=>$s_dataname, "setting"=>$use_survey_array['table_settings']['setting'], "type"=>$use_survey_array['table_settings']['type'], "mindate"=>date("Y-m-d H:i:s",$i_prev_query_time));
	$query = mlavu_query($query_string,$query_vars);// poslavu_MAIN_db
	if ($query !== FALSE && $s_dataname !== 'test_pizza_sho')
		if (mysqli_num_rows($query) > 0)
			return '';

	// get the distro code and id of the restaurant
	$restaurant_query = mlavu_query("SELECT `id`,`distro_code` FROM `poslavu_MAIN_db`.`restaurants` WHERE `data_name`='[dataname]' AND `data_name`!=''",
		array("dataname"=>$s_dataname));
	if ($restaurant_query === FALSE)
		return '';
	if (mysqli_num_rows($restaurant_query) == 0) {
		error_log("the restaurant couldn't be found (/areas/survey.php ... dataname: ".$s_dataname.")");
		return '';
	}
	while ($restaurant_read = mysqli_fetch_assoc($restaurant_query)) {
		$i_rest_id = (int)$restaurant_read['id'];
		$s_distro_code = $restaurant_read['distro_code'];
	}

	// check that the account is more than 25 days old
	$a_signup = get_signup_by_id_dataname($i_rest_id, $s_dataname);
	if (count($a_signup) == 0)
		return '';
	$i_signup_time = strtotime($a_signup['signup_timestamp']);
	if ($i_signup_time >= strtotime("25 days ago"))
		return '';

	// get the distro ips and set the distro questions
	$a_distro_questions = array();
	$a_older_distro_ips = array();
	$a_recent_distro_ips = array();
	if ($s_distro_code !== '' && count($use_survey_array['distro_questions']) > 0) {
		$a_distro_questions = $use_survey_array['distro_questions'];
		$distro_ips_query = mlavu_query("SELECT `ipaddress`,`most_recent_ts` FROM `poslavu_MAIN_db`.`reseller_connects` WHERE `resellername`='[resellername]'",
			array("resellername"=>$s_distro_code));
		if ($distro_ips_query !== FALSE) {
			if (mysqli_num_rows($distro_ips_query) > 0)
				while ($distro_ips_read = mysqli_fetch_assoc($distro_ips_query))
					if (strtotime($distro_ips_read['most_recent_ts']) > strtotime("2 days ago"))
						$a_recent_distro_ips[] = trim($distro_ips_read['ipaddress']);
					else
						$a_older_distro_ips[] = trim($distro_ips_read['ipaddress']);
		} else {
			error_log("FAILED DISTRO QUERY (/areas/survey.php)");
		}
	}
	$a_distro_ips = array_merge($a_older_distro_ips, $a_recent_distro_ips);

	// check the this isn't the distro's ip address or that it's the restaurants primary address (for the past 30 days)
	// and that the distro didn't log in to this ip in the last two days
	if (in_array($s_current_ip, $a_recent_distro_ips) && $s_dataname != 'lavu_inc' && $s_dataname !== 'tigay_restaura')
		return '';
	if (in_array($s_current_ip, $a_distro_ips)) {
		$user_ip_query = mlavu_query("SELECT SUM(`count`) AS `total_count` FROM `poslavu_MAIN_db`.`ap_connects` WHERE `dataname`='[dataname]' AND `ipaddress`='[ipaddress]' AND `last_connect` > '[mintime]' GROUP BY `ipaddress`",
			array("dataname"=>$s_dataname, "ipaddress"=>$s_current_ip, "mintime"=>date("Y-m-d H:i:s",strtotime("25 days ago"))));
		$b_is_primary_user_ip = FALSE;
		if ($user_ip_query !== FALSE) {
			if (mysqli_num_rows($user_ip_query) > 0) {
				while ($user_ip_read = mysqli_fetch_assoc($user_ip_query)) {
					$count = (int)$user_ip_read['total_count'];
					if (($count) >= 8)
						$b_is_primary_user_ip = TRUE;
				}
			}
		}
		if (!$b_is_primary_user_ip && $s_dataname !== 'tigay_restaura')
			return '';
	}

	// draw the non-distro related questions
	$a_restaurant_questions = $use_survey_array['restaurant_questions'];
	$a_questions = array_merge($a_distro_questions, $a_restaurant_questions);
	$a_inputs = array();
	foreach($a_questions as $a_question) {
		$a_inputs[] = draw_input_question($a_question);
	}

	// draw some extra javascript
	$s_script = isset($use_survey_array['javascript']) ? '<script type="text/javascript">'.$use_survey_array['javascript'].'</script>' : '';

	$retval = '
		<form action="#" method="POST" id="satisfaction_survey_form">
			<table cellpadding=6 style="border:solid 1px #888888" bgcolor="#f5fed0">
				<tr>
					<td style="width:465px;text-align:left;">
						<p style="font-weight:bold;">Survey</p>
						<p style="color:gray;">Please take a moment to fill out this brief survey.  We are always looking for ways to improve our processes and your feedback is important to us.</p>
						'.implode('
						', $a_inputs).'
						<input type="hidden" name="command" value="submit_survey_results" />
						<input type="hidden" name="dataname" value="'.$s_dataname.'" />
						<input type="button" name="survey_submit_button" value="Submit" onclick="submit_form_by_ajax($(\'#satisfaction_survey_form\'), \'/cp/index.php?widget=survey\');" style="display:none;" class="saveBtn"><br />
						<label name="ajax_retval" style="color:gray;font-weight:bold;">&nbsp;</label>
					</td>
				</tr>
			</table>
		</form>
		'.$s_script;
	$retval .= draw_survey_javascript();

	return $retval;
}

/**
 * returns an array of the most recent survey result, or an empty array
 * the returned array is broken apart by the description value in value3 of the `poslavu_MAIN_db`.`survey` survey results row
 * syntax for value3:
 *     pathToCreationFile.php, value:description, value2:description, ... for all values used
 * @param  weakly typed  $wt_setting         The `setting` value to look for, can either be a single string or an array of strings
 * @param  string        $s_dataname         The dataname of the account, or load_all_poslavu_restaurants
 * @param  boolean       $b_ignore_first     If true, ignores the first survey result (chronologically)
 * @param  boolean       $b_load_multiple    If TRUE, returns all survey results
 * @param  string        $s_extra_query_code Inserts query code into the whereclause
 * @return array                             Either an array representing a single survey result, or an array of these results if $b_load_multiple is TRUE
 */
function get_most_recent_survey_results($wt_setting, $s_dataname = '', $b_ignore_first = FALSE, $b_load_multiple = FALSE, $s_extra_query_code = '') {

	// get the survey strings to search for
	$a_surveys = NULL;
	$a_setting = array();
	$a_wherevars = ($s_dataname == 'load_all_poslavu_restaurants') ? array() : array("dataname"=>$s_dataname);
	if (is_array($wt_setting)) {
		foreach($wt_setting as $k=>$v) {
			$a_wherevars["setting{$k}"] = $v;
			$a_setting[] = "`setting`='[setting{$k}]'";
		}
	} else {
		$a_setting[] = "`setting`='[setting]'";
		$a_wherevars['setting'] = $wt_setting;
	}
	$s_dataname_setting = ($s_dataname == 'load_all_poslavu_restaurants') ? "" : "`dataname`='[dataname]' AND ";
	$s_setting = $s_dataname_setting."(".implode(" OR ", $a_setting).")";

	// search the database
	$a_surveys = ConnectionHub::getConn('poslavu')->getDBAPI()->getAllInTable("survey", $a_wherevars, TRUE, array("whereclause"=>"WHERE {$s_setting}{$s_extra_query_code}", "orderbyclause"=>"ORDER BY `value` ASC", "print_query"=>FALSE));

	// load the distro codes for the restaurant(s)
	$a_dataname_to_distro_code = array();
	$a_dataname = ($s_dataname == "load_all_poslavu_restaurants") ? NULL : array("data_name"=>$s_dataname);
	$a_restaurants = ConnectionHub::getConn('poslavu')->getDBAPI()->getAllInTable("restaurants", $a_dataname, TRUE, array("selectclause"=>"`data_name`,`distro_code`"));
	foreach($a_restaurants as $a_restaurant) {
		$a_dataname_to_distro_code[$a_restaurant['data_name']] = $a_restaurant['distro_code'];
	}
	unset($a_restaurants);

	// process by dataname
	$a_surveys_by_dataname = array();
	foreach($a_surveys as $a_survey) {
		$s_dn = $a_survey['dataname'];
		if (!isset($a_surveys_by_dataname[$s_dn]))
			$a_surveys_by_dataname[$s_dn] = array();
		if (isset($a_dataname_to_distro_code[$s_dn])) {
			$a_survey['distro_code'] = $a_dataname_to_distro_code[$s_dn];
		} else {
			$a_survey['distro_code'] = '';
		}
		$a_surveys_by_dataname[$s_dn][] = $a_survey;
	}

	// ignore first
	if ($b_ignore_first) {
		foreach($a_surveys_by_dataname as $k1=>$a_by_dataname) {
			unset($a_surveys_by_dataname[$k1][0]);
		}
	}

	// load multiple
	if (!$b_load_multiple) {
		foreach($a_surveys_by_dataname as $k1=>$a_by_dataname) {
			$survey_submitted = 0;
			foreach($a_by_dataname as $k2=>$v) {
				$survey_submitted++;
				if ($survey_submitted == count($a_by_dataname)) {
					break;
				}
				unset($a_surveys_by_dataname[$k1][$k2]);
			}
		}
	}

	// put datanames back into a single array
	$a_surveys = array();
	foreach($a_surveys_by_dataname as $s_dataname=>$a_by_dataname) {
		foreach($a_by_dataname as $a_survey) {
			$a_retval = NULL;
			assign_names_to_values($a_survey, $a_retval);
			if (!isset($a_retval['dataname']))
				$a_retval['dataname'] = $s_dataname;
			if (!isset($a_retval['distro_code']))
				$a_retval['distro_code'] = $a_survey['distro_code'];
			$a_surveys[] = $a_retval;
		}
	}
	unset($a_surveys_by_dataname);
	unset($a_by_dataname);

	return $a_surveys;
}

// break to the survey into its constituent parts
// based off of the values in `value3`
function assign_names_to_values(&$a_survey, &$a_retval) {

	// get the keys
	$a_description_parts = explode(', ',$a_survey['value3']);
	foreach($a_description_parts as $k=>$s_description_part) {
		if (!strstr($s_description_part, ':')) {
			unset($a_description_parts[$k]);
		} else {
			$a_value_description = explode(':',$s_description_part);
			$a_description_parts[$k] = $a_value_description;
		}
	}

	// assign values to the keys
	foreach($a_description_parts as $a_value_description) {
		$a_retval[$a_value_description[1]] = $a_survey[$a_value_description[0]];
	}
	$a_retval['distro_code'] = $a_survey['distro_code'];
}

// draws an input for the survey
// supported types include:
//     type="rating": values min, max, name, question, [mintext, maxtext]
function draw_input_question($a_question) {
	switch($a_question['type']) {
	case 'rating':
		$s_mintext = (isset($a_question['mintext'])) ? speak($a_question['mintext']) : '';
		$s_maxtext = (isset($a_question['maxtext'])) ? speak($a_question['maxtext']) : '';
		$s_cluealign = (isset($a_question['cluealign'])) ? $a_question['cluealign'] : 'bottom';
		$s_input = '
			'.speak($a_question['question']).'<br />
			<table style="border:0px none;padding:0 0 4px 0;">
				<tr>
					<td colspan="2">
						<table>
							<tr>';
		if ($s_mintext != '' && $s_cluealign == 'side')
			$s_input .= '
								<td style="padding:0px 10px 10px 10px;color:#008800;">'.$s_mintext.'</td>';
		for($i = $a_question['min']; $i <= $a_question['max']; $i++)
			$s_input .= '
								<td style="text-align:center;padding:0px 10px 2px 10px;"><input type="radio" name="'.speak($a_question['name']).'" value="'.$i.'"><br />'.$i.'</td>';
		if ($s_maxtext != '' && $s_cluealign == 'side')
			$s_input .= '
								<td style="padding:0px 10px 10px 10px;color:#008800;">'.$s_maxtext.'</td>';
		$s_input .= '
							</tr>
						</table>
					</td>
				</tr>';
		if (($s_mintext != '' || $s_maxtext != '') && $s_cluealign == 'bottom')
			$s_input .= '
				<tr>
					<td style="text-align:left;padding-bottom:4px;color:gray;">
						'.$s_mintext.'
					</td>
					<td style="text-align:right;padding-bottom:4px;color:#008800;">
						'.$s_maxtext.'
					</td>
				</tr>';
		$s_input .= '
			</table>';
		return $s_input;
	case 'textarea':
		$i_cols = (isset($a_question['cols'])) ? (int)$a_question['cols'] : 60;
		$i_rows = (isset($a_question['rows'])) ? (int)$a_question['rows'] : 3;
		$s_placeholder = (isset($a_question['placeholder'])) ? speak($a_question['placeholder']) : '';
		$s_style = (isset($a_question['style']) ? $a_question['style'] : '');
		$s_input = '
			<div style="'.$s_style.'">
			'.$a_question['question'].'<br />
			<textarea cols="'.$i_cols.'" rows="'.$i_rows.'" name="'.speak($a_question['name']).'" placeholder="'.$s_placeholder.'"></textarea>
			</div>';
		if(1)
		{
			$keyup_cmd = speak($a_question['name']) . "_keyup";
			$msg_name = speak($a_question['name']) . "_message";
			$email_name = speak($a_question['name'])."_email";
			$phone_name = speak($a_question['name'])."_phone";
			$main_name = speak($a_question['name']);
			$main_question = $a_question['question'];
			
			$s_input = "
				<div style=\"$s_style\">
				$main_question<br />
				<textarea cols=\"$i_cols\" rows=\"$i_rows\" name='$msg_name' id='$msg_name' placeholder='$s_placeholder' onkeyup='$keyup_cmd()'></textarea>
				<table>
					<tr><td colspan='2' style='font-family:Verdana,Arial; color:#777777'>If you would like us to contact you about your feedback, please enter your contact info here:</td></tr>
					<tr><td style='padding-bottom:8px'><input type='text' placeholder='Your Email (Optional)' style='font-size:10px' value='' name='$email_name' id='$email_name' onkeyup='$keyup_cmd()'></td>
					<td style='padding-bottom:8px'><input type='text' placeholder='Your Phone (Optional)' style='font-size:10px' value='' name='$phone_name' id='$phone_name' onkeyup='$keyup_cmd()'></tr></table>
					<textarea name='$main_name' id='$main_name' style='display:none'></textarea>
				</div>
				<script type='text/javascript'>
					function $keyup_cmd() {
						var vstr = document.getElementById('$msg_name').value;
						var estr = document.getElementById('$email_name').value;
						var pstr = document.getElementById('$phone_name').value;
						if(estr!='') { if(vstr!='') vstr += '\\n\\n'; vstr += 'Email: ' + estr; }
						if(pstr!='') { if(vstr!='') { if(estr=='') vstr += '\\n'; vstr += '\\n'; } vstr += 'Phone: ' + pstr; }
						document.getElementById('$main_name').value = vstr;
					}
				</script>
				";
		}
		return $s_input;
	}
	return '';
}

// the javascript that goes along with the survey
function draw_survey_javascript() {
	return '
		<script type="text/javascript">
			function get_all_form_inputs(jform) {
				var a_inputs = jform.find("input");
				a_inputs = jQuery.merge(a_inputs, jform.find("select"));
				a_inputs = jQuery.merge(a_inputs, jform.find("textarea"));
				return a_inputs;
			}

			function get_all_form_inputs_names(jform) {
				var a_inputs = get_all_form_inputs(jform);

				// get all the names
				var a_names = [];
				for (var i = 0; i < a_inputs.length; i++) {
					var input_element = a_inputs[i];
					jelement = $(input_element);
					var s_name = jelement.prop("name");
					if ($.inArray(s_name, a_names) == -1)
						a_names.push(s_name);
				}

				return a_names;
			}

			function submit_form_by_ajax(jform, url) {
				var a_names = get_all_form_inputs_names(jform);

				var a_data = {};
				$.each(a_names, function(i_index) {
					var s_name = a_names[i_index];
					var jinput = jform.find("[name="+s_name+"]");
					var type = jinput.prop("type");
					if (type == "radio") {
						a_data[s_name] = jform.find("input[name="+s_name+"]:checked").val();
					} else {
						a_data[s_name] = jinput.val();
					}
				});

				$.ajax({
					cache:false,
					async:false,
					url:url,
					data:a_data,
					type:"POST",
					success:function(message){
						message = $.trim(message);
						var success = message.split("|")[0];
						var note = message.split("|")[1];
						var jlabel = jform.find("label[name=ajax_retval]");
						if (success == "success") {
							if (note == "reload page")
								window.location.reload();
							if (note.indexOf("replace form with html") == 0) {
								var new_html = note.split("replace form with html")[1];
								jform.html(new_html);
							}
							jlabel.css({ color: "gray" });
						} else {
							note = "Failed to submit results. Please try again.";
							jlabel.css({ color: "red" });
						}
						jlabel.html(note);
						jlabel.stop(true, true);
						jlabel.css({ opacity: 0 });
						jlabel.animate({ opacity: 1 }, 500);
					}
				});
			}

			function check_draw_submit_button(jform) {
				var a_names = get_all_form_inputs_names(jform);

				// check if any of the inputs arent set
				var all_inputs_are_filled = true;
				for (var i = 0; i < a_names.length; i++) {
					var s_name = a_names[i];
					var jinput = jform.find("[name="+s_name+"]");
					var type = jinput.prop("type");
					if (type == "radio") {
						if (jform.find("input[name="+s_name+"]:checked").length == 0)
							all_inputs_are_filled = false;
					} else if (type == "textarea") {
					} else {
						//if (jinput.val() == "")
							//all_inputs_are_filled = false;
					}
				}

				var jsubmit = jform.find("input[name=survey_submit_button]");
				if (all_inputs_are_filled) {
					jsubmit.stop(true, true);
					jsubmit.show(200);
				} else {
					jsubmit.hide();
				}
			}

			$(function(){
				a_inputs = get_all_form_inputs($("#satisfaction_survey_form"));
				$.each(a_inputs, function(k,v) {
					$(v).click(function() {
						check_draw_submit_button($("#satisfaction_survey_form"));
					});
				});
			});
		</script>';
}

// inputs should be posted from the above form
// inserts the inputs into the survey table and returns 'success|message' or 'failure|message'
// @$value2: can be (distributor_interaction, )
// @$value4: can be (lavu_satisfaction, )
// @$value5: can be (distro_satisfaction, )
// @$value_long: can be (poslavu_feedback, )
function submit_survey_results($s_dataname, $value2 = '', $value4 = '', $value5 = '', $value_long = '') {
	if (strlen(draw_satisfaction_query($s_dataname)) < 2)
		return 'failure|new survey is too recent';

	// set the values to insert into the database
	get_current_survey($value2, $value4, $value5, $value_long);
	global $use_survey_array;

	// insert the values into the database
	$a_insert_vars = array_merge(array("dataname"=>$s_dataname), $use_survey_array['table_settings']);
	$s_insert_clause = ConnectionHub::getConn('poslavu')->getDBAPI()->arrayToInsertClause($a_insert_vars);
	$insert_query_string = "INSERT INTO `poslavu_MAIN_db`.`survey` {$s_insert_clause}";
	mlavu_query($insert_query_string, $a_insert_vars);//poslavu_MAIN_db

	// email the results
	$a_names_to_values = array();
	assign_names_to_values($use_survey_array['table_settings'], $a_names_to_values);
	$s_body = "Survey Results:\n\n";
	$s_body .= "dataname: {$s_dataname}";
	$s_body .= "\nsurvey: ".$use_survey_array['table_settings']['setting'];
	foreach($a_names_to_values as $k=>$v)
		$s_body .= "\n{$k}: {$v}";
		
		mail('andy@lavu.com', 'Survey Results', $s_body, 'From: noreply@poslavu.com');
		mail('ag@lavu.com', 'Survey Results', $s_body, 'From: noreply@poslavu.com');
		mail('lucas@lavu.com', 'Survey Results', $s_body, 'From: noreply@poslavu.com');
		mail('brad@lavu.com', 'Survey results', $s_body, "From: noreply@poslavu.com");
		mail('grace.parazzoli@lavu.com', 'Survey results', $s_body, "From: noreply@poslavu.com");
		// mail('b.orourke@lavu.com', 'Survey Results', $s_body, 'From: noreply@poslavu.com');
	
	// check the triggers
	$a_names_to_values = array();
	assign_names_to_values($use_survey_array['table_settings'], $a_names_to_values);
	$a_account = array('dataname'=>$s_dataname);
	foreach($use_survey_array['triggers'] as $o_trigger) {
		if ($o_trigger->checkTrigger('result')) {
			$wt_result = $o_trigger->trigger($a_account, $a_names_to_values);
		}
	}

	// check for success
	if (mlavu_insert_id() == 0)
		return 'failure|failed to insert new value';
	$message = '<div style="color:gray;font-weight:bold;width:465px;text-align:left;">Thank you for your input.</div>';
	return "success|replace form with html".$message;
}

class SurveyPageTrigger {

	function __construct($a_trigger, $a_fields) {
		$this->generateTrigger($a_trigger);
		$this->$fields = $a_fields;
	}

	// check if this is the correct action for this object's trigger
	// @return: TRUE or FALSE
	public function checkTrigger($s_action) {

		// check if it's even the correct action
		if ($this->trigger->action != $s_action)
			return FALSE;
		return TRUE;
	}

	// should be called as a response to checkTrigger()
	// @$a_account: the values associated with the account as key=>value pairs
	// @$a_values: the necessary values for the trigger, eg the results from the survey
	// @return: TRUE if the trigger succeeded, FALSE if it didn't succeed, or NULL if the trigger wasn't supposed to occur
	public function trigger(&$a_account, &$a_values) {

		// check that the trigger should be triggered
		$b_match = FALSE;
		switch($this->trigger->action) {
		case 'result':
			foreach($this->trigger->match_values as $k=>$v) {
				switch($this->trigger->match_type) {
				case '=':
					if ($v == (string)$a_values[$this->trigger->field_name])
						$b_match = TRUE;
					break;
				}
			}
			break;
		}
		if (!$b_match)
			return NULL;

		return $this->finishTrigger($a_account, $a_values);
	}

	// creates a trigger, to be checked later
	protected function generateTrigger(&$a_trigger) {

		// determine what kind of trigger it is
		$this->trigger = new stdClass();
		$this->trigger->action = $a_trigger['action'];

		// parse the rest of the trigger
		switch($this->trigger->action) {
		case 'result':
			$this->trigger->field_name = $a_trigger['name'];
			$this->trigger->match_type = $a_trigger['match'];
			$this->trigger->match_values = $a_trigger['values'];
			break;
		}
	}

	protected function getSpecials() {
		return array(
			'datetime'=>date('Y-m-d H:i:s')
		);
	}

	// replaces values in the fields with values from the account, from the return values, and from special values
	protected function fillResults(&$a_account, &$a_values) {
		$this->filled_fields = $this->fields;
		foreach($this->filled_fields as $k=>$v) {
			foreach($a_account as $s_k=>$s_v)
				$v = str_replace("--account-{$s_k}--", "{$s_v}", $v);
			foreach($a_values as $s_k=>$s_v)
				$v = str_replace("--value-{$s_k}--", "{$s_v}", $v);
			foreach($this->getSpecials() as $s_k=>$s_v)
				$v = str_replace("--special-{$s_k}--", "{$s_v}", $v);
			$this->filled_fields[$k] = $v;
		}
	}

	// to be flushed out in the child
	// supposed to be called as a function by trigger()
	protected function finishTrigger(&$a_account, &$a_values) {}
}

class ZendeskTicketGenerator extends SurveyPageTrigger {

	function __construct($a_trigger, $a_fields) {

		$this->generateTrigger($a_trigger);
		$this->fields = $a_fields;
	}

	// flushes out the trigger funcitonality
	protected function finishTrigger(&$a_account, &$a_values) {

		// update the fields with new values
		$this->fillResults($a_account, $a_values);

		// the ids for these come from the zendesk CP backend
		$custom = array(
			ZENDESK::get_customer_field_to_submit('Data Name', $a_account['dataname'])
		);

		// there are triggers in the zendesk CP that assign the ticket to a group
		$tags = isset($this->filled_fields['tags']) ? $this->filled_fields['tags'] : '';
		$tags_string = json_encode($tags, JSON_FORCE_OBJECT);
		$tags_obj_string = '["'.implode('","',$tags).'"]';

		$a_optional = array('assignee_id', 'group_id');
		$arr = array(
			"z_subject"=>isset($this->filled_fields['subject']) ? $this->filled_fields['subject'] : 'No Subject',
			"z_description"=>isset($this->filled_fields['description']) ? $this->filled_fields['description'] : 'No Description',
			"z_recipient"=>isset($this->filled_fields['recipient']) ? $this->filled_fields['recipient'] : "support@poslavu.com",
			"z_name"=>isset($this->filled_fields['name']) ? $this->filled_fields['name'] : "N/A",
			"z_requester"=>isset($this->filled_fields['email']) ? $this->filled_fields['email'] : "test@poslavu.com",
		);
		foreach($a_optional as $s_key) {
			if (isset($this->filled_fields[$s_key])) {
				$arr["z_{$s_key}"] = $this->filled_fields[$s_key];
			}
		}

		$create_arr = array(
			'ticket' => array(
				'subject' => $arr['z_subject'],
				'comment' => array(
					'body' => $arr['z_description'],
				),
				'requester' => array(
					'name' => $arr['z_name'],
					'email' => $arr['z_requester']
				),
				'status' => 'new',
				'custom_fields' => $custom,
				'tags' => $tags,
			)
		);
		foreach($a_optional as $s_key) {
			if (isset($this->arr["z_{$s_key}"])) {
				$create_arr['ticket'][$s_key] = $arr["z_{$s_key}"];
			}
		}

		$create = json_encode($create_arr, JSON_FORCE_OBJECT);
		$create = str_replace($tags_string, $tags_obj_string, $create);
		$error = array();
		$curl_info = array();
		$decoded = array();
		$b_draw_errors = FALSE;
		$success = curlWrap("/tickets.json", $create, "POST", $error, $curl_info, $decoded, TRUE, $b_draw_errors);

		if (is_array($error) && count($error) > 0) {
			$s_subject = __FILE__."\n".__LINE__."\n";
			$s_subject .= print_r(array("/tickets.json", $create, "POST", $error, $curl_info, $decoded, TRUE, $b_draw_errors),TRUE);
			$o_emailAddressesMap->sendEmailToAddress("zendesk_ticket_errors", "Zendesk Ticket Error", $s_subject);
		}

		return $success;
	}
}

?>
