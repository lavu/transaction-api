<?php
/**
 * Created by PhpStorm.
 * User: Hayat
 * Author :Irshad Ahmad
 * Date: 17/04/18
 * Time: 10:15 AM
 */
	require_once(dirname(__FILE__)."/../resources/core_functions.php");
	require_once(resource_path()."/lavuquery.php");
	require_once(resource_path()."/chain_functions.php");

	session_start($_GET['session_id']);
	$req_schm = "https";
	if($_SERVER['HTTP_HOST'] == 'admin.localhost' || $_SERVER['HTTP_HOST'] == 'localhost:8888' ) {
		$req_schm = "http";
	}

	$activeUser = admin_loggedin();
	$data_name = sessvar("admin_dataname");
	$templateId = $_REQUEST['tpl_id'];
	$mode = "settings_edit_fiscaltemplate";
	$edit = "1";
	$styleSheetPath = "/cp/styles/styles.css";
	$saveTempResult = "";
	if (isset($_POST['posted'])) {
		$saveTempResult = createUpdateFiscalTemplate($_POST);
	}
	$display = "<!DOCTYPE html> ";
	$display .= "<html>";
	$display .= "<head>";
	$display .= "<meta http-equiv=\"Content-Type\" content=\"text/html;charset=ISO-8859-1\">";
	
	$display .= "<link rel='stylesheet' type='text/css' href='".$styleSheetPath."'>";
	$display .= "<link rel='stylesheet' type='text/css' href='/cp/styles/info_window.css'>";

	$display .= "<script type=\"text/javascript\" src=\"/manage/js/jquery/js/jquery-1.9.0.js\"></script>";
	$display .= "<script type=\"text/javascript\" src=\"/manage/js/jquery/js/jquery-ui-1.10.0.custom.min.js\"></script>
				<link rel=\"stylesheet\" href=\"/manage/js/jquery/css/smoothness/jquery-ui-1.10.0.custom.min.css\" />
				<link href=\"/cp/css/select2-4.0.3.min.css\" rel=\"stylesheet\" />
				<script src=\"/cp/scripts/select2-4.0.3.min.js\"></script>";
	
	$display .= "</head>";
	$display .= "<body>";

	$display .= "<div id='fiscal_template_page_content'>";

	$form_name = "fiscal_template_form";

	$field_code = buildFiscalTplForm($templateId, $data_name, $saveTempResult);

	$display .= "<table id='fiscal_template_form_table' cellspacing='0' cellpadding='3'><tr><td align='center'>";
	$display .= "<form id='$form_name' name='$form_name' method='post'>";
	$display .= "<input type='hidden' name='posted' value='1'>";
  $display .= "<input type='hidden' id='tpl_id' name='tpl_id' value='".$templateId."'>";
	$display .= "<input type='hidden' id='dataname' name='dataname' value='".$data_name."'>";
	$display .= "<input type='hidden' id='active_user' name='active_user' value='$activeUser'>";
	$display .= $field_code;

	$display .= "&nbsp;<br><input type='submit' id='btn_fiscal_temp_form' value='".speak("Apply")."' style='width:120px;height:3em;'>
<input type='button' id='preview_fiscal_temp' value='".speak("Preview")."' style='width:120px;height:3em;'>";

	$display .= "<div id='fiscal_temp_validation'></div>";
	$display .= "</form>";
	$display .= "</td></tr></table>";
	$display .= "</div>";
	$display .= "<div id='info_window_bg' align='center' name='info_window_bg' style='visibility: hidden;'><table id='info_area_bgtable' style='opacity: 0.9; width: 500px; height: 500px;'><tbody><tr><td align='center' valign='top'>&nbsp;</td></tr></tbody></table></div>";

    $display .= "<div id='info_window' align='center' name='info_window' style='visibility: hidden;width:100%'><div style='width: 500px; height:100%;background:#fff;border:2px solid gray;margin-top:-4em'><div style='float:right'><a onclick='fiscaltemp_visibility()' class='closeBtn' style='border:1px solid gray'>X</a></div><br><br>
    <div id='info_area_table' style='width: 500px; height:100%;background:#fff;margin-top:-4em;overflow-y: scroll;'>
    <table class='flex' cellpadding='4'>
    <tbody><tr>
    <td width='450px' bgcolor='#777777' class='flex' style='justify-content: center;'>
    <span class='headerTxt'>Preview</span>
    </td>
    </tr>
    </tbody></table>
    
    <div id='fiscal_temp_preview'>
    
    </div>
    </div>
    </div>";
    $display .= "</body>";

/**********************************************************************************************************************/
//									SCRIPTS
/**********************************************************************************************************************/
	$display .= '<script type="text/javascript">
                function fiscaltemp_visibility(setvis)
                {
                	infowin = document.getElementById("info_window");
                	infowinbg = document.getElementById("info_window_bg");
                	if(infowin.style.visibility=="hidden" || setvis=="on")
                	{
                		infowin.style.visibility = "visible";
                		infowinbg.style.visibility = "visible";
                	}
                	else
                	{
                		infowin.style.visibility = "hidden";
                		infowinbg.style.visibility = "hidden";
                	}
                }

                      
                function sendFormDetails(data, type, dataname, tplId){
                   var sessionId = $("#session_id").val();
                    $.ajax({ url: "save_fiscaltemplate.php",
                             data: {sessionId:sessionId, type: type, data:data, dataname:dataname, tpl_id:tplId},
                             type: "post",
                             success: function(output) {
                              //alert(output);
                             if(type == "preview"){
                              var previewData = output.split("@#$");
                              var popupHeight = (previewData[0] * 30)+400;
                               $("#info_area_bgtable").height(popupHeight);
                              fiscaltemp_visibility("on");
                              $("#fiscal_temp_preview").html(previewData[1]);
                              }
                             }
                    });
                
                }
				$(document).on(\'click\', \'form[name="fiscal_template_form"] #btn_fiscal_temp_form\', function(e) {
                  var previewInfo = fiscalTemplateFormData();
                  var dataname = $("#dataname").val();
                  if(previewInfo == false){
                    return false;
                   }
                    else{
                    sendFormDetails(previewInfo, "save", dataname,"");
                    }
				});

            //preview of fiscal template:::::::
            $(document).on(\'click\', \'form[name="fiscal_template_form"] #preview_fiscal_temp\', function(e) {
             var previewInfo = fiscalTemplateFormData();
            var dataname = $("#dataname").val();
            var tplId = $("#tpl_id").val();            
            //alert(previewInfo);
            var previewDtls = "";
              if(previewInfo == false){
                return false;
               }
               else{
                 sendFormDetails(previewInfo, "preview", dataname,tplId);
               }
               
            });
          
            function fiscalTemplateFormData(){
            var validation = 0;
            var RequiredValidation = [];
            var infoEachField =[];
            var rowStr = "";
            var active_user = $("#active_user").val();
            var tpl_id = $("#tpl_id").val();
            $(\'form[name="fiscal_template_form"]\').find("input:text, input:hidden, input:checkbox, textarea").each(function() {
            		var thisName = $(this).attr("name");
					      var title = $(this).attr("fieldTitle");
            		var fieldId = $(this).attr("fieldid");
            		var elID = $(this).prop("id");
            		var val = $(this).val();
			          var curElementValue = val;
                var elRequired = $(this).attr("req");
                var type = $(this).attr("format");
                var fieldLabel = $(this).attr("fieldLabel");
                if(elID == "display-"+fieldId && $("#"+elID).attr("type")=="checkbox"){
                  if ($("#"+elID).is(":checked")){
                   curElementValue = 1;
                  }
                  else{
                   curElementValue = 0;
                  }
                }
                
                if(elRequired==1 && elID != "display-"+fieldId){
                        if(curElementValue == ""){
                            validation = 1;
                            var validationMsg = $(this).attr("label");
                            var prefix = "Please Enter ";
        
                            RequiredValidation.push(prefix+validationMsg);
                        }
                 }
						if(fieldId != undefined){
            		    rowStr += fieldId+"^^^!"+elID+"^^^!"+curElementValue+"^^^!"+tpl_id+"^^^!"+active_user+"^^^!"+type+"^^^!"+fieldLabel+"^^^!"+title;
            		    infoEachField.push(rowStr);
						}
            		    rowStr = "";
            		
            });
                if(validation != 0){
                var message="";
            
                    $.each(RequiredValidation, function (index, value) {
                        message +="<div>"+value+"</div>";
                    });
                
                $("#fiscal_template_form #fiscal_temp_validation").html(message);
                return false;
                }
                else{
                $("#fiscal_template_form #fiscal_temp_validation").html("");
                return infoEachField;
                }
            }
            
            //end of fiscal preview...........
    </script>';

	$display .="</html>";
	echo $display;

/**********************************************************************************************************************/
//									FUNCTIONS
/**********************************************************************************************************************/

	  function buildFiscalTplForm($tplId, $dataname, $saveTempResult) {
      include_once($_SERVER['DOCUMENT_ROOT'].'/lib/jcvs/FiscalReceiptTplApi.php');
       $obj = new FiscalReceiptTplApi($dataname,array('tpl_id'=>$tplId,'action'=>'gettplfieldsbysection'));
      $templateInfo=$obj->doProcess();
      $templateFram = "<table  cellspacing=0 style='' width='100%'><tbody>";
      if($saveTempResult > 0) {
        $templateFram .= '<tr><td colspan=4 style="color:#9BBE42;text-align:center">'.speak("Fiscal Receipt Template has been saved successfully").'.</td></tr>';
      }
	else if($saveTempResult !="" && $saveTempResult == 0){
	$templateFram .= '<tr><td colspan=4 style="color:#ff0000;text-align:center">'.speak("Warning: There was a problem updating the database").'.</td></tr>';
	}
/*echo '<pre>';
print_r($templateInfo);
exit;     
*/ 
      foreach($templateInfo as $key => $recordData) {
	  $hiddens='';
	  $htmls='';
          if($recordData['allow_edit']==1){
            $templateFram .= '<tr><td colspan=4 bgcolor="#9BBE42" style="font-size: 14px;padding: 4px;text-align: center;color: #fff;"><b>'.speak($key).'</b></td></tr>';
          $templateFram .= '<tr><td class ="tpl_field_top" style="border-left: 0;border-right: 0">'.speak("Title").'</td><td class ="tpl_field_top">'.speak("Label").'</td><td class ="tpl_field_top">'.speak("Print Text").'</td><td class ="tpl_field_top">'.speak("Include on Receipt").'</td></tr>';            
          }
          foreach($recordData['fields'] as $rec){
                $fields[] = $rec['title'];
		$html=getFormfield($rec,$recordData['allow_edit']);
		if($rec['allow_edit']==0 || $recordData['allow_edit']==0){
			$hiddens .= $html;
		}else{
			$htmls .= $html;
		}
          }
	$templateFram .= $hiddens;
	$templateFram .= $htmls;
      }
      $templateFram .= "<input type = 'hidden' name = 'fields' value = '" . json_encode($fields) . "'>";
      $templateFram .= "<input type = 'hidden' name = 'tpl_id' value = '" .$tplId. "'>";

      $templateFram .= "</tbody></table><br>";
	      //$templateFram .= $hiddens;

      return $templateFram;
    }

    function getFormfield($recordData, $editSection=0) {
      $tplFieldId = $recordData['fiscal_receipt_tpl_field_id'];
      $label = $recordData['tpl_field_label'];
      $title = $recordData['title'];
      $final_label = $recordData['final_label'];
      $final_display = $recordData['final_display'];
      $dom = $recordData['final_dom'];
      $sizeInfo = $recordData['final_size'];
      $value = $recordData['loc_value'];
      $sizeDtls = explode("-", $sizeInfo);
      $required = $recordData['required'];
      $fieldType = $recordData['final_type'];
      $format = $fieldType."_".$recordData['pos'];
      $allow_hide = $recordData['allow_hide'];
      $show_label = $recordData['show_label'];
	 
	   
	    $templateFram = "";

      if($editSection==0 || $recordData['allow_edit']==0){
                if ($fieldType == "div") {
                  $templateFram .= "<input type='hidden' name='div_".$tplFieldId."' id='div-".$tplFieldId."' value='' fieldid='".$tplFieldId."' fieldTitle='".$title."' format='div'><input type='hidden' name='display_".$tplFieldId."' id='display-".$tplFieldId."' value='1'  fieldid='".$tplFieldId."' format='".$format."' fieldLabel='".$label."' fieldTitle='".$title."'>";
                }else{
		  if($show_label){
		$templateFrm .="<input title='".$final_label."' type='hidden' name='label_".$tplFieldId."' id='label-".$tplFieldId."' value='".$final_label."' label='".$label." label' fieldid='".$tplFieldId."' format='".$format."' fieldLabel='".$label."' fieldTitle='".$title."'>";	
		  }
                  $templateFram .= "<input type='hidden' name='value_".$tplFieldId."' id='value-".$tplFieldId."' value=''  fieldid='".$tplFieldId."' format='".$format."' fieldLabel='".$label."' fieldTitle='".$title."'>";
                  $templateFram .= "<input type='hidden' name='display_".$tplFieldId."' id='display-".$tplFieldId."' value='".$final_display."'  fieldid='".$tplFieldId."' format='".$format."' fieldLabel='".$label."' fieldTitle='".$title."'>";
                }
		$templateFram .= "<input type = 'hidden' name='fiscal_receipt_tpl_field_id_".$tplFieldId."' id='fiscal_receipt_tpl_field_id_".$tplFieldId."' value = '" . $tplFieldId . "' fieldTitle='".$title."'>";
            return $templateFram;
      	}

	    $defaultSize = 25;
	    $templateFram .= "<tr>";
	    if ($fieldType == "div") {
	    	$templateFram .= "<td colspan='4' class='tpl_column' style='display:none'><input type='hidden' name='div_".$tplFieldId."' id='div-".$tplFieldId."' value='' fieldid='".$tplFieldId."' fieldTitle='".$title."' format='div'><input type='hidden' name='display_".$tplFieldId."' value=1></td>";
	    }
	    else if ($title != "") {
	    
	    	$templateFram .= "<td class='tpl_field_header'>".$label."</td>";
	    
	    
	    	$text_disabled="disabled";
	    	if($show_label) {
	    		$size = ($sizeDtls[0]) ? $sizeDtls[0] : $defaultSize;
	    		$templateFram .= "<td class='tpl_column'><input alt='".$final_label."' title='".$final_label."' type='text' name='label_".$tplFieldId."' id='label-".$tplFieldId."' value='".$final_label."' style='color: #1c591c;padding-left:5px;' size='".$size."' req='".$required."' label='".$label." label' fieldid='".$tplFieldId."' format='".$format."' fieldLabel='".$label."' fieldTitle='".$title."'></td>";
	    	}
	    	else {
	    		$templateFram .= "<td class='tpl_column'><input type='text' size='".$defaultSize."' $text_disabled></td>";
	    	}
	    		
	    	 
	    	if ($dom == 'text' && $required && $fieldType == 'field') {
	    		$size = ($sizeDtls[0]) ? $sizeDtls[0] : $defaultSize;
	    		$templateFram .= "<td class='tpl_column'><input alt='".$final_label."' title='".$final_label."' type='text' name='value_".$tplFieldId."' id='value-".$tplFieldId."' value='".$value."' style='color: #1c591c;padding-left:5px;' size='".$size."' req='".$required."' label='".$label." value' fieldid='".$tplFieldId."' format='".$format."' fieldLabel='".$label."' fieldTitle='".$title."'></td>";
	    	}
	    	else if ($dom == 'textarea' && $required && $fieldType == 'field') {
	    		$rows = ($sizeDtls[0]) ? $sizeDtls[0] : 1;
	    		$cols = ($sizeDtls[1]) ? $sizeDtls[1] : 14;
	    		$templateFram .= "<td class='tpl_column'><textarea rows='".$rows."' cols='".$cols."' alt='".$final_label."' title='".$final_label."' type='text' name='value_".$tplFieldId."' id='value-".$tplFieldId."' style='color: #1c591c;padding-left:5px;' size='".$size."' req='".$required."' label='".$label." value' fieldid='".$tplFieldId."' format='".$format."' fieldLabel='".$label."' fieldTitle='".$title."'>".$value."</textarea></td>";
	    	}
	    	else {
	    		$templateFram .= "<td class='tpl_column'><input type='text' size='".$defaultSize."' disabled></td>";
	    	}
	    	 
	    	$checked = '';
	    	if ($final_display) {
	    		$checked = 'checked';
	    	}
	    	$disabled = "";
	    	if (!$allow_hide) {
	    		$disabled = "disabled";
	    	}
	    	$templateFram .= "<td class='tpl_column' style='text-align:center;'><input type='checkbox' name='display_".$tplFieldId."' req='".$required."' id='display-".$tplFieldId."' fieldid='".$tplFieldId."' fieldTitle='".$title."' fieldLabel='".$label."' format='".$format."' $disabled   $checked></td>";
	    }
	    $templateFram .= "<input type = 'hidden' name='fiscal_receipt_tpl_field_id_".$tplFieldId."' id='fiscal_receipt_tpl_field_id_".$tplFieldId."' value = '" . $tplFieldId . "' fieldTitle='".$title."'>";
	    $templateFram .= "</tr>";
	    
	   
	    return $templateFram;
	}

function createUpdateFiscalTemplate($data) {
    global $activeUser, $data_name, $templateId;
    include_once(__DIR__.'/../../lib/jcvs/ApiHelper.php');
    ApiHelper::includeOrm('FiscalReceiptTplFields');
    ApiHelper::includeOrm('FiscalReceiptTplLocal');
    $obj = new FiscalReceiptTplField($data_name);
    $tplFields = $obj->getInfo('fiscal_receipt_tpl_field_id AS tpl_field_id, allow_hide, display as default_display',' fiscal_receipt_tpl_id="' . $templateId . '"');
    $obj = new FiscalReceiptTplLocal($data_name);
    $whereCond = ($templateId > 0) ? 'fiscal_receipt_tpl_id = "' . $templateId . '"' : '';
    $obj->deleteDb($whereCond);
    $insertCount = "";
    foreach ($tplFields as $rec) {
      $templateData = array();
      $templateData['value'] = $data['value_'.$rec['tpl_field_id']];
      $templateData['label'] = $data['label_'.$rec['tpl_field_id']];
      $templateData['display'] = ($rec['allow_hide'])?(isset($data['display_'.$rec['tpl_field_id']]) ? '1' : '0'):$rec['default_display'];
      $templateData['created_by'] = $activeUser;
      $templateData['updated_by'] = $activeUser;
      $templateData['fiscal_receipt_tpl_id'] = $templateId;
      $templateData['fiscal_receipt_tpl_field_id'] = $data['fiscal_receipt_tpl_field_id_'.$rec['tpl_field_id']];
      $templateData['_deleted'] = 0;
      $obj = new FiscalReceiptTplLocal($data_name, $templateData);
      $insertResult = $obj->insertDb();
      if($insertResult){
	 $insertCount++ ;
	}
    }
	return $insertCount;	
  }
?>

