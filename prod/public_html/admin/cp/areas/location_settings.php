<?php
	if($in_lavu)
	{
		global $data_name;
		$tzs = array();
		$tzs[""] = "";
		$tzs["Pacific/Midway"] = "(GMT-11:00) Midway Island, Samoa";
		$tzs["America/Adak"] = "(GMT-10:00) Hawaii-Aleutian";
		$tzs["Etc/GMT+10"] = "(GMT-10:00) Hawaii";
		$tzs["Pacific/Marquesas"] = "(GMT-09:30) Marquesas Islands";
		$tzs["Pacific/Gambier"] = "(GMT-09:00) Gambier Islands";
		$tzs["America/Anchorage"] = "(GMT-09:00) Alaska";
		$tzs["America/Ensenada"] = "(GMT-08:00) Tijuana, Baja California";
		$tzs["Etc/GMT+8"] = "(GMT-08:00) Pitcairn Islands";
		$tzs["America/Los_Angeles"] = "(GMT-08:00) Pacific Time (US & Canada)";
		$tzs["America/Denver"] = "(GMT-07:00) Mountain Time (US & Canada)";
		$tzs["America/Chihuahua"] = "(GMT-07:00) Chihuahua, La Paz, Mazatlan";
		$tzs["America/Dawson_Creek"] = "(GMT-07:00) Arizona";
		$tzs["America/Belize"] = "(GMT-06:00) Saskatchewan, Central America";
		$tzs["America/Cancun"] = "(GMT-06:00) Guadalajara, Mexico City, Monterrey";
		$tzs["Chile/EasterIsland"] = "(GMT-06:00) Easter Island";
		$tzs["America/Chicago"] = "(GMT-06:00) Central Time (US & Canada)";
		$tzs["America/New_York"] = "(GMT-05:00) Eastern Time (US & Canada)";
		$tzs["America/Havana"] = "(GMT-05:00) Cuba";
		$tzs["America/Bogota"] = "(GMT-05:00) Bogota, Lima, Quito, Rio Branco";
		$tzs["America/Caracas"] = "(GMT-04:30) Caracas";
		$tzs["America/Santiago"] = "(GMT-04:00) Santiago";
		$tzs["America/La_Paz"] = "(GMT-04:00) La Paz";
		$tzs["Atlantic/Stanley"] = "(GMT-04:00) Faukland Islands";
		$tzs["America/Campo_Grande"] = "(GMT-04:00) Brazil";
		$tzs["America/Goose_Bay"] = "(GMT-04:00) Atlantic Time (Goose Bay)";
		$tzs["America/Glace_Bay"] = "(GMT-04:00) Atlantic Time (Canada)";
		$tzs["America/St_Johns"] = "(GMT-03:30) Newfoundland";
		$tzs["America/Araguaina"] = "(GMT-03:00) UTC-3";
		$tzs["America/Montevideo"] = "(GMT-03:00) Montevideo";
		$tzs["America/Miquelon"] = "(GMT-03:00) Miquelon, St. Pierre";
		$tzs["America/Godthab"] = "(GMT-03:00) Greenland";
		$tzs["America/Argentina/Buenos_Aires"] = "(GMT-03:00) Buenos Aires";
		$tzs["America/Sao_Paulo"] = "(GMT-03:00) Brasilia";
		$tzs["America/Noronha"] = "(GMT-02:00) Mid-Atlantic";
		$tzs["Atlantic/Cape_Verde"] = "(GMT-01:00) Cape Verde Is.";
		$tzs["Atlantic/Azores"] = "(GMT-01:00) Azores";
		$tzs["Europe/Belfast"] = "(GMT) Greenwich Mean Time : Belfast";
		$tzs["Europe/Dublin"] = "(GMT) Greenwich Mean Time : Dublin";
		$tzs["Europe/Lisbon"] = "(GMT) Greenwich Mean Time : Lisbon";
		$tzs["Europe/London"] = "(GMT) Greenwich Mean Time : London";
		$tzs["Africa/Abidjan"] = "(GMT) Monrovia, Reykjavik";
		$tzs["Europe/Amsterdam"] = "(GMT+01:00) Amsterdam, Berlin, Bern";
		$tzs["Europe/Amsterdam"] = "(GMT+01:00) Rome, Stockholm, Vienna";
		$tzs["Europe/Belgrade"] = "(GMT+01:00) Belgrade, Bratislava";
		$tzs["Europe/Belgrade"] = "(GMT+01:00) Budapest, Ljubljana, Prague";
		$tzs["Europe/Brussels"] = "(GMT+01:00) Brussels, Copenhagen";
		$tzs["Europe/Brussels"] = "(GMT+01:00) Madrid, Paris";
		$tzs["Africa/Algiers"] = "(GMT+01:00) West Central Africa";
		$tzs["Africa/Windhoek"] = "(GMT+01:00) Windhoek";
		$tzs["Asia/Beirut"] = "(GMT+02:00) Beirut";
		$tzs["Africa/Cairo"] = "(GMT+02:00) Cairo";
		$tzs["Asia/Gaza"] = "(GMT+02:00) Gaza";
		$tzs["Africa/Blantyre"] = "(GMT+02:00) Harare, Pretoria";
		$tzs["Asia/Jerusalem"] = "(GMT+02:00) Jerusalem";
		$tzs["Europe/Minsk"] = "(GMT+02:00) Minsk";
		$tzs["Asia/Damascus"] = "(GMT+02:00) Syria";
		$tzs["Europe/Moscow"] = "(GMT+03:00) Moscow, St. Petersburg, Volgograd";
		$tzs["Africa/Addis_Ababa"] = "(GMT+03:00) Nairobi";
		$tzs["Asia/Tehran"] = "(GMT+03:30) Tehran";
		$tzs["Asia/Dubai"] = "(GMT+04:00) Abu Dhabi, Muscat";
		$tzs["Asia/Yerevan"] = "(GMT+04:00) Yerevan";
		$tzs["Asia/Kabul"] = "(GMT+04:30) Kabul";
		$tzs["Asia/Yekaterinburg"] = "(GMT+05:00) Ekaterinburg";
		$tzs["Asia/Tashkent"] = "(GMT+05:00) Tashkent";
		$tzs["Asia/Kolkata"] = "(GMT+05:30) Chennai, Kolkata, Mumbai, New Delhi";
		$tzs["Asia/Katmandu"] = "(GMT+05:45) Kathmandu";
		$tzs["Asia/Dhaka"] = "(GMT+06:00) Astana, Dhaka";
		$tzs["Asia/Novosibirsk"] = "(GMT+06:00) Novosibirsk";
		$tzs["Asia/Rangoon"] = "(GMT+06:30) Yangon (Rangoon)";
		$tzs["Asia/Bangkok"] = "(GMT+07:00) Bangkok, Hanoi, Jakarta";
		$tzs["Asia/Krasnoyarsk"] = "(GMT+07:00) Krasnoyarsk";
		$tzs["Asia/Hong_Kong"] = "(GMT+08:00) Beijing, Chongqing";
		$tzs["Asia/Hong_Kong"] = "(GMT+08:00) Hong Kong, Urumqi";
		$tzs["Asia/Irkutsk"] = "(GMT+08:00) Irkutsk, Ulaan Bataar";
		$tzs["Australia/Perth"] = "(GMT+08:00) Perth";
		$tzs["Australia/Eucla"] = "(GMT+08:45) Eucla";
		$tzs["Asia/Tokyo"] = "(GMT+09:00) Osaka, Sapporo, Tokyo";
		$tzs["Asia/Seoul"] = "(GMT+09:00) Seoul";
		$tzs["Asia/Yakutsk"] = "(GMT+09:00) Yakutsk";
		$tzs["Australia/Adelaide"] = "(GMT+09:30) Adelaide";
		$tzs["Australia/Darwin"] = "(GMT+09:30) Darwin";
		$tzs["Australia/Brisbane"] = "(GMT+10:00) Brisbane";
		$tzs["Australia/Hobart"] = "(GMT+10:00) Hobart";
		$tzs["Asia/Vladivostok"] = "(GMT+10:00) Vladivostok";
		$tzs["Australia/Lord_Howe"] = "(GMT+10:30) Lord Howe Island";
		$tzs["Etc/GMT-11"] = "(GMT+11:00) Solomon Is., New Caledonia";
		$tzs["Asia/Magadan"] = "(GMT+11:00) Magadan";
		$tzs["Pacific/Norfolk"] = "(GMT+11:30) Norfolk Island";
		$tzs["Asia/Anadyr"] = "(GMT+12:00) Anadyr, Kamchatka";
		$tzs["Pacific/Auckland"] = "(GMT+12:00) Auckland, Wellington";
		$tzs["Etc/GMT-12"] = "(GMT+12:00) Fiji, Kamchatka, Marshall Is.";
		$tzs["Pacific/Chatham"] = "(GMT+12:45) Chatham Islands";
		$tzs["Pacific/Tongatapu"] = "(GMT+13:00) Nuku'alofa";
		$tzs["Pacific/Kiritimati"] = "(GMT+14:00) Kiritimati";
		
		$available_languages = array();
		$available_languages[0] = "Custom";
		$get_language_packs = mlavu_query("SELECT `id`, `language`, `_deleted` FROM `poslavu_MAIN_db`.`language_packs` ORDER BY `language` ASC");
		while ($lpack = mysqli_fetch_assoc($get_language_packs)) {
			if (($lpack['_deleted'] == '0') || ($location_info['use_language_pack'] == $lpack['id'])) {
				$available_languages[$lpack['id']] = $lpack['language'];
			}
		}
		
		$forward_to = "index.php?mode={$section}_{$mode}";
		$tablename = "locations";
		$rowid = $locationid;
		$fields = array();
		$fields[] = array(speak("Receipt Information Settings"),"Receipt Information Settings","seperator");
		$fields[] = array(speak("Location Name").":","title","text","size:40");
		$fields[] = array(speak("Address").":","address","text","size:40");
		$fields[] = array(speak("City").":","city","text", "size:40");
		$fields[] = array(speak("State").":","state","text", "size:40");
		$fields[] = array(speak("Postal Code").":","zip","text","size:15");
		$fields[] = array(speak("Country").":","country","text","size:30");
		$fields[] = array(speak("Phone").":","phone","text","size:30");
		$fields[] = array(speak("Email Address").":","email","text","size:40");
		$fields[] = array(speak("Website").":","website","text","size:40");
		$fields[] = array(speak("General Manager").":","manager","text", "size:40");
		// LP-4650 START
		if ($modules->hasModule("reports.fiscal.uruguay"))
		{
			$fields[] = array(speak("IRS NO").":","irs_no","text", "size:40");
		}
		// LP-4650 END
		//$fields[] = array("Menu","menu_id","select",$available_menus);
		// LP-4449 START
		global $data_name;
		$rest_query = mlavu_query("select `chain_name` from `poslavu_MAIN_db`.`restaurants` where `data_name`='[1]'",$data_name);
		if(mysqli_num_rows($rest_query))
		{
			$rest_read = mysqli_fetch_assoc($rest_query);
			$chain_name = $rest_read['chain_name'];
			if(substr($chain_name,0,3)=="ABI")
			{
				// condition argentina
				$fields[] = array(speak("CUIT No").":","cuit_no","text", "size:40");// for LP-4062
			}
		}// LP-4449 START
		$fields[] = array(speak("Other Location Settings"),"Other Location Settings","seperator");
		$fields[] = array(speak("Time Zone").":","timezone","select",$tzs);
		$fields[] = array(speak("Hours").":","config:hours_of_operation","hours_of_operation"); // if ($modules->hasModule("extensions.payment.mercury.paypal")) ???
		$fields[] = array(speak("Language pack").":","use_language_pack","select",$available_languages);
		
		/*$fields[] = array("Location Settings","Location Settings","seperator");
		$fields[] = array(speak("Street").":","street_number","text","size:40",array("db"=>"MAIN","table"=>"customer_location_actual","attr"=>"enabled","where_clause"=>"`data_name`='".$data_name."'"));
		$fields[] = array(speak("Address").":","route","text","size:40", array("db"=>"MAIN","table"=>"customer_location_actual","attr"=>"enabled","where_clause"=>"`data_name`='".$data_name."'"));
		$fields[] = array(speak("Neighborhood").":","neighborhood","text","size:40", array("db"=>"MAIN","table"=>"customer_location_actual","attr"=>"enabled","where_clause"=>"`data_name`='".$data_name."'"));
		$fields[] = array(speak("City").":","locality","text","size:40",array("db"=>"MAIN","table"=>"customer_location_actual","attr"=>"enabled","where_clause"=>"`data_name`='".$data_name."'"));
		$fields[] = array(speak("County").":","administrative_area_level_2","text","size:40", array("db"=>"MAIN","table"=>"customer_location_actual","attr"=>"enabled","where_clause"=>"`data_name`='".$data_name."'"));
		$fields[] = array(speak("State").":","administrative_area_level_1","text","size:40", array("db"=>"MAIN","table"=>"customer_location_actual","attr"=>"enabled","where_clause"=>"`data_name`='".$data_name."'"));
		$fields[] = array(speak("Country").":","country","text","size:40", array("db"=>"MAIN","table"=>"customer_location_actual","attr"=>"enabled","where_clause"=>"`data_name`='".$data_name."'"));
		$fields[] = array(speak("Postal Code").":","postal_code","text","size:40", array("db"=>"MAIN","table"=>"customer_location_actual","attr"=>"enabled","where_clause"=>"`data_name`='".$data_name."'"));*/
	//	$fields[] = array(speak("Update Location Information").":","loc_info","button","size:40", array("attr"=>"disabled","onclick"=>"show_update_loc_screen()"));

		$fields[] = array(speak("Save"),"submit","submit");
		
		require_once(resource_path() . "/form.php");
	}	
?>