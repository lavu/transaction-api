
<?php
/**
 * Created by PhpStorm.
 * User: Chris
 * Date: 10/18/17
 * Time: 2:30 PM
 */

if ($in_lavu) {
    echo "<br><br>";
    global $data_name;
    $form_posted = (isset($_POST['posted']));
    $mealPlanGroups = mysqli_fetch_assoc(clavuQuery("SELECT * FROM `MealPlanGroup`"));
    //build up the meal plan group array for the dimensional form - cfields
    $mealPlanGroupArray = array();
    $mealPlanGroupArray[] = array("name", "name", speak("Group Name"), "input");
    $mealPlanGroupArray[] = array("id", "id", "Group", "hidden");
    $mealPlanGroupArray[] = array("stipend", "stipend", speak("Stipend Amount"), "input");
    $mealPlanGroupArray[] = array("discount", "discount", speak("Discount Amount"), "input");
    $mealPlanGroupArray[] = array("access_level", "access_level", speak("Access Level"), "input");
    //build up the meal plan user array for Dimensional Form - qfields
    $mealPlanUserArray = array();
    $mealPlanUserArray[] = array("id", "id", "User ID", "hidden");
    $mealPlanUserArray[] = array("f_name", "f_name", speak("First Name"), "input");
    $mealPlanUserArray[] = array("l_name", "l_name", speak("Last Name"), "input");
    $mealPlanUserArray[] = array("rf_id", "rf_id", speak("RF ID"), "input");
    $mealPlanUserArray[] = array("email", "email", speak("E-Mail Address"), "input");
    $mealPlanUserArray[] = array("phone", "phone", speak("Phone Number"), "input");
    $mealPlanUserArray[] = array("badge_id", "badge_id", speak("Badge Number"), "input");
    $mealPlanUserArray[] = array("MealPlanGroupID", "MealPlanGroupID", "Meal Plan Group ID", "hidden");

    //qinfo fields
    $qinfo = array();
    $qinfo["qtitle_category"] = "Group";
    $qinfo["qtitle_item"] = "User";
    $qinfo["category_tablename"] = "MealPlanGroup";
    $qinfo["inventory_tablename"] = "MealPlanUser";
    $qinfo["category_fieldname"] = "id";
    $qinfo["category_primefield"] = "name";
    $qinfo["inventory_fieldname"] = "MealPlanGroupID";
    $qinfo["inventory_select_condition"] = "";
    $qinfo["inventory_primefield"] = "l_name";
    $qinfo["details_column"] = "";
    $qinfo["send_details"] = false;
    $qinfo["category_details_column"] = false;
    $qinfo["category_send_details"] = false;
    $qinfo["field"] = "MealPlanUsers";
    $qinfo["category_filter_by"] = false;
    $qinfo["category_filter_value"] = false;
    $qinfo["category_filter_extra"] = false;
    $qinfo["inventory_filter_by"] = false;
    $qinfo["inventory_filter_value"] = false;

    $qinfo["category_orderby"] = "id";
    $qinfo["inventory_orderby"] = "l_name";
    $include_session = "?session_id=" . session_id();
    $include_data_name = "&dn=" . sessvar("admin_dataname");

    require_once(resource_path() . "/dimensional_form.php");

    $userFields = array(0 => "f_name",
        1 => "l_name",
        2 => "rf_id",
        3 => "badge_id",
        4 => "email",
        5 => "phone",
        6 => "id");

    $groupFields = array(
        0 => "id",
        1 => "name",
        2 => "stipend",
        3 => "discount",
        4 => "access"
    );

    if ($form_posted) {
		
    	$validate_duplicate = check_duplicate($userFields,$groupFields);
    	if (is_array($validate_duplicate))
    	{
    		if (!empty($validate_duplicate))
    		{
    			$msg = '<div class="error_head"><h2>We found some error(s) while saving the data. They are listed below:</h2>';
    			$msg .='<table>';
    			$i=1;
    			foreach ($validate_duplicate as $val)
    			{
    				 
    				$msg .='<tr><td>'.$i.') '.$val.'</td></tr>';
    				$i++;
    			}
    			$msg .='</table>';
    			$msg .= '<p>Please rectify the above errors and try again.</p></div>';
    		}
    		echo $msg;
    	}
    	else {
            $category_tablename = "MealPlanGroup";
            //error_log("FORM HAS BEEN POSTED: " . print_r($_REQUEST, 1));
            $cat_count = 1;
            while (isset($_POST["ic_" . $cat_count . "_category"])) {
                $category_name = $_POST['ic_' . $cat_count . '_category'];
                $category_id = $_POST['ic_' . $cat_count . '_categoryid'];
    
                if ($category_name != "") { //Update menu_categories table
                    $dbfields = array();
                    $dbfields['_deleted'] =
                    $category_delete = $_POST['ic_' . $cat_count . '_delete_0'];
    
                    $cntGroupFields = count($groupFields);
                    for ($n = 0; $n < $cntGroupFields; $n++) {
                        $dbfields[$groupFields[$n]] = $_POST['ic_' . $cat_count ."_col_". $groupFields[$n]];
                    }
                    //error_log("DB FIELDS TO UPDATE: ".print_r($dbfields,1));
                    $category_id = updateMealPlanGroup($dbfields);
                }
                $item_count = 1;
                while (isset($_POST["ic_".$cat_count."_f_name_".$item_count]))
                {
                    $currentUser = array(); // get posted values
                    $cntUserFields = count($userFields);
                    for ($n=0; $n<$cntUserFields; $n++)
                    {
                        $qname = $userFields[$n];
                      //  error_log("Current key: ".$qname);
                        $currentUser[$qname] = $_POST['ic_' . $cat_count . '_' . $qname . '_' . $item_count];
    
                    }
                    $currentUser['MealPlanGroupID'] = $category_id;
    
                    $currentUser['delete'] =$_POST['ic_' . $cat_count . '_delete_' . $item_count];
                    $userID = updateMealPlanUser($currentUser);
    
    
                    $item_count++;
                }
                $cat_count++;
            }
    
            if (!empty($_FILES) && $_FILES['user_upload_list']['error']===0) {
            	$msg = '';
            	$csv_mimetypes = array(
            	    ‘text/csv’,
            	    ‘text/plain’,
            	    ‘application/csv’,
            	    ‘text/comma-separated-values’,
            	    ‘application/excel’,
            	    ‘application/vnd.ms-excel’,
            	    ‘application/vnd.msexcel’,
            	    ‘text/anytext’,
            	    ‘application/octet-stream’,
            	    ‘application/txt’,
            	);
            	if (in_array(strtolower($_FILES['user_upload_list']['type']), $csv_mimetypes)) {
                    require_once(resource_path() . "/csv_input_output.php");
                    $userCSV = array_map("str_getcsv", file($_FILES['user_upload_list']['tmp_name']));
                    $arrResults = importValidationUsers($userCSV);
                    
                    if (isset($arrResults) && array_key_exists('error', $arrResults)) {
                        $failedInserts = $arrResults['error'];
                            
                        if (is_array($failedInserts))
                        {
                        	if (!empty($failedInserts))
                        	{
                        		$msg = '<div class="error_head"><h2>We found some error(s) while uploading the CSV. They are listed below:</h2>';
                        		$msg .='<table>';
                        		$i=1;
                        		foreach ($failedInserts as $val)
                        		{
                        			
                        			$msg .='<tr><td>'.$i.') '.$val.'</td></tr>';
                        			 $i++;
                        		}
                        		$msg .='</table>';
                        		$msg .= '<p>Please rectify the above errors and try again.<p></div>';
                        	}
                        }
                        else {
                            $msg = '<div class="error_head"><h2>'.$arrResults['error'].'</h2></div>';
                        }
                    }
                    else if (isset($arrResults) && array_key_exists('success', $arrResults)) {
                        $msg = '<div class="succcess">'.$arrResults['success'].'</div>';
                    }
            	}
            	else {
            	    $msg = '<div class="error_head"><h2>Uploaded file should be of type CSV</h2></div>';
            	}
                    
				echo $msg;
            }
        }
    }
    else {
        //code for initial load goes here
    }
    $url = 'https://'.$_SERVER['SERVER_NAME'].'/csvtemplate/importMealPlanUser.csv';
    $field_code = create_dimensional_form($mealPlanUserArray,$qinfo,$mealPlanGroupArray);
	$display = "<br><br><form name='sform' enctype=\"multipart/form-data\" method='post' action=''>";
	$display .= "<input type='hidden' name='posted_dataname' value='$data_name'>";
	$display .= "&nbsp;<input type='submit' value='".speak("Save")."' class='saveBtn'><br>&nbsp;";
	$display .= "<input type='hidden' name='posted' value='1'>";
	$display .= $field_code;
	$display .= "&nbsp;<input type='button' value='".speak("Import Users")."' onclick='document.getElementById(\"csv_upload_file_chooser_id\").click();' name='buttonForward' id='buttonForward' class='saveBtn'/>&nbsp;<input type='button' value='".speak("Download Sample CSV")."' onclick='window.open(\"$url\");' style='width:14em;' class='saveBtn'/><br>&nbsp;";
	$display .= "<input type='file' name='user_upload_list' id='csv_upload_file_chooser_id' style='display:none;'><br>";
	$display .= "&nbsp;<input type='submit' value='".speak("Save")."' class='saveBtn'><br>&nbsp;";
	$display .= "</form>";

	echo create_info_layer(600,480);
	echo $display;
	//echo draw_copy_move_item_category_script();;
    echo "<br><br>";
}

function updateMealPlanUser($userArray) {
    //error_log("USER ARRAY: ".print_r($userArray, 1));
    $f_name = $userArray['f_name'];
    $l_name = $userArray['l_name'];
    $rf_id = $userArray['rf_id'];
    $badge_id = $userArray['badge_id'];
    $id = $userArray['id'];
    $email = $userArray['email'];
    $phone = $userArray['phone'];
    $restaurant_id = admin_info('companyid');
    $mealPlanGroupID = $userArray['MealPlanGroupID'];
    $delete = '';
    if($userArray['delete']==1)
    {
    	$delete = ',`_deleted` = 1';
    }
    if (!empty($id)) {
        $query = "UPDATE `MealPlanUser` SET `f_name` = '".clavu_query_encode($f_name)."',`l_name` = '".clavu_query_encode($l_name)."',`rf_id` = '".clavu_query_encode($rf_id)."',`badge_id` = '".clavu_query_encode($badge_id)."',`email` = '".clavu_query_encode($email)."',`phone` = '".clavu_query_encode($phone)."',`MealPlanGroupID` = '".clavu_query_encode($mealPlanGroupID)."',`restaurant_id` = '".clavu_query_encode($restaurant_id)."' $delete WHERE `id` = '$id'";
        clavuQuery($query);
        return $id;
    }
    else {
        $query = "INSERT INTO `MealPlanUser` (`f_name`, `l_name`, `email`, `phone`, `rf_id`, `badge_id`, `MealPlanGroupID`,`restaurant_id`) VALUES ('".clavu_query_encode($f_name)."', '".clavu_query_encode($l_name)."', '".clavu_query_encode($email)."', '".clavu_query_encode($phone)."', '".clavu_query_encode($rf_id)."', '".clavu_query_encode($badge_id)."', '".clavu_query_encode($mealPlanGroupID)."','".clavu_query_encode($restaurant_id)."')";
        clavuQuery($query);
        return clavuInsertID();
    }

    return 0;
}

function importMealPlanUsers($userCSV) {
    global $mealPlanGroups;
    $mealPlanGroups = array();
    $mealPlanGroupQuery = clavuQuery("SELECT * FROM `MealPlanGroup` WHERE `_deleted` = 0");
    while ($row = mysqli_fetch_assoc($mealPlanGroupQuery)) {
        $mealPlanGroups[$row['id']]['id'] = $row['id'];
        $mealPlanGroups[$row['id']]['name'] = $row['name'];
    }
    $fields = 0;
    if ($userCSV[0][0] === 'First Name' && $userCSV[0][1] === 'Last Name' && $userCSV[0][2] === 'RF ID' && $userCSV[0][3] === 'Badge_ID' && $userCSV[0][4] === 'Email' && $userCSV[0][5] === 'Phone' && $userCSV[0][6] === 'MealPlanGroup Name') {
        $fields = 1;
    }
    if ($fields == 0){
        return array('error' => "Uploaded file does not contain header row or header row does not match the sample csv header");
    }
    $failedInserts = array();
    $cntUserCSV = count($userCSV);
    for ($i=1;$i < $cntUserCSV;$i++) {
        $userArray = array();
        $userArray["id"] = "";
        $userArray['f_name'] = $userCSV[$i][0];
        $userArray['l_name'] = $userCSV[$i][1];
        $userArray['rf_id'] = $userCSV[$i][2];
        $userArray['badge_id'] = $userCSV[$i][3];
        $userArray['email'] = $userCSV[$i][4];
        $userArray['phone'] = $userCSV[$i][5];
        $userArray['MealPlanGroupID'] = getMealPlanGroupIDByName($userCSV[$i][6]);
       
        updateMealPlanUser($userArray);
            
        }
    return array('success' => 'File records are imported successfully');
}

function importValidationUsers($userCSV) {
	global $mealPlanGroups;
	$mealPlanGroups = array();
	$mealPlanGroupQuery = clavuQuery("SELECT * FROM `MealPlanGroup` WHERE `_deleted` = 0");
	while ($row = mysqli_fetch_assoc($mealPlanGroupQuery)){
		$mealPlanGroups[$row['id']]['id'] = $row['id'];
		$mealPlanGroups[$row['id']]['name'] = $row['name'];
	}
        
	$cntUserCSV = count($userCSV);
	if ($cntUserCSV == 0) {
	    return array('error' => "Uploaded file is empty");
	}
	
	$fields = 0;
	if ($userCSV[0][0] === 'First Name' && $userCSV[0][1] === 'Last Name' && $userCSV[0][2] === 'RF ID' && $userCSV[0][3] === 'Badge_ID' && $userCSV[0][4] === 'Email' && $userCSV[0][5] === 'Phone' && $userCSV[0][6] === 'MealPlanGroup Name') {
		$fields = 1;
	}
	if ($fields == 0) {
	    return array('error' => "Uploaded file does not contain header row or header row does not match the sample csv header");
	}
	
	if ($cntUserCSV == 1) {
	    return array('error' => "Uploaded file only contains header row");
	}
	
	$failedInserts = array();
	$cnt = 0;
	$user_email = '';
	
	for ($i=1;$i < $cntUserCSV;$i++) {
		$userArray = array();
		$error = '';
		$sep = '';
		$userArray['f_name'] = $userCSV[$i][0];
		$userArray['l_name'] = $userCSV[$i][1];
		$userArray['rf_id'] = $userCSV[$i][2];
		$userArray['badge_id'] = $userCSV[$i][3];
		$userArray['email'] = $userCSV[$i][4];
		$userArray['phone'] = $userCSV[$i][5];
		$userArray['MealPlanGroupID'] = getMealPlanGroupIDByName($userCSV[$i][6]);
		
		if ($userArray['f_name'] == '')
		{
			$error = 'First Name is missing';
			$sep = ', ';
		}
		if ($userArray['l_name'] == '')
		{
			$error .= $sep.'Last Name is missing';
			$sep = ', ';
		}
		if ($userArray['rf_id'] == '')
		{
			$error .= $sep.'Rf Id is missing';
			$sep = ', ';
		}
		if ($userArray['badge_id'] == '')
		{
			$error .= $sep.'Badge Id is missing';
			$sep = ', ';
		}
		if ($userArray['email'] == '')
		{
			$error .= $sep.'Email Address is missing';
			$sep = ', ';
		}
		else if (!filter_var($userArray['email'], FILTER_VALIDATE_EMAIL))
		{
			$error .= $sep.'Invalid Email Address';
			$sep = ', ';
		}
		else if ($user_email == $userArray['email'])
		{
			$error .= $sep.'Duplicate entry detected - The entered Email Address is used multiple times in the uploaded CSV';
			$sep = ', ';
		}
		if ($userArray['phone'] == '')
		{
			$error .= $sep.'Phone Number is missing';
			$sep = ', ';
		}
		if ($userArray['MealPlanGroupID'] === 0 || $userArray['MealPlanGroupID']== '')
		{
			$error .= $sep.'Missing or Invalid MealPlan Group Name';
			$sep = ', ';
		}
		if ($userArray['MealPlanGroupID'] > 0 && $userArray['MealPlanGroupID']!= '')
		{
			$chkUserExist = checkUserExistbyGroup($userArray['MealPlanGroupID'],$userArray['email']);
			if ($chkUserExist == 1)
			{
				$error .= $sep.'Duplicate entry detected - The entered Email Address already exists in that MealPlan Group Name';
				$sep = ', ';
			}
		}
		
		if (!empty($error))
		{
			$failedInserts[$cnt] = 'Row '.$i.': '.$error;
		}
		$user_email = $userArray['email'];
		$cnt++;
	}
	if (!empty($failedInserts))
	{
		return array('error' => $failedInserts);
	}
	else 
	{
		return importMealPlanUsers($userCSV);
	}
}

function getMealPlanGroupIDByName($name) {
    global $mealPlanGroups;
    foreach ($mealPlanGroups as $group) {
        if (strtolower($group['name']) === strtolower($name)) {
            return $group['id'];
        }
    }
    return 0; //No group with matching name.
}

function updateMealPlanGroup($dbFields) {
    $stipend = $dbFields['stipend'];
    $discount = $dbFields['discount'];
    $deleted = $dbFields['_deleted'];
    $id = $dbFields['id'];
    $name = $dbFields['name'];
    $restaurant_id = admin_info("companyid");
    if (!empty($id)) {
        $query = "UPDATE `MealPlanGroup` SET `name` = '".clavu_query_encode($name)."', `stipend` = '".clavu_query_encode($stipend)."', `discount` = '".clavu_query_encode($discount)."', `restaurant_id`='".clavu_query_encode($restaurant_id)."', `_deleted` = '".clavu_query_encode($deleted)."' WHERE `id` = '".clavu_query_encode($id)."'";
        clavuQuery($query);
        if($deleted=='1') {
            $query = "UPDATE `MealPlanUser` SET `_deleted` = '".clavu_query_encode($deleted)."'  WHERE `MealPlanGroupID` = '".clavu_query_encode($id)."'";
            clavuQuery($query);
        }
        return $id;
    }
    else {
        $query = "INSERT INTO `MealPlanGroup` (`name`, `stipend`, `discount`, `access_level`, `_deleted`, `restaurant_id`) VALUES ('".clavu_query_encode($name)."', '".clavu_query_encode($stipend)."', '".clavu_query_encode($discount)."', '4', '".clavu_query_encode($deleted)."', '".clavu_query_encode($restaurant_id)."')";
        clavuQuery($query);
        return clavuInsertID();
    }
}

function checkUserExistbyGroup($groupId,$emailId)
{
    $mealPlanUsers = clavuQuery("SELECT * FROM `MealPlanUser` where email='".clavu_query_encode($emailId)."' and MealPlanGroupID='".clavu_query_encode($groupId)."'");
	if (mysqli_num_rows($mealPlanUsers)>0)
	{
		return 1;
	}
	else {
		return 0;
	}
}

function check_duplicate($userFields, $groupFields)
{
	$cat_count = 1;
	$error_arr = [];
	while (isset($_POST["ic_" . $cat_count . "_category"])) {
		$category_name = $_POST['ic_' . $cat_count . '_category'];
		$category_id = $_POST['ic_' . $cat_count . '_categoryid'];
		$error = '';
		$sep = '';
		if ($category_name != "") {
			$dbfields = array();
			$dbfields['_deleted'] = $_POST['ic_' . $cat_count . '_delete_0'];
			
			$cntGroupFields = count($groupFields);
			for ($n = 0; $n < $cntGroupFields; $n++) {
				$dbfields[$groupFields[$n]] = $_POST['ic_' . $cat_count ."_col_". $groupFields[$n]];
			}
			
			if (empty($category_id)) {
				$category_id = checkGroupExist($dbfields['name']);
				if ($category_id === 1)
				{
					$error .= 'Group '.$dbfields['name']. ' already exists';
					$sep = ', ';
				}
			}
		}
		$item_count = 1;
		$user_email = '';
		while (isset($_POST["ic_".$cat_count."_f_name_".$item_count]))
		{
			$currentUser = array(); // get posted values
			$cntUserFields = count($userFields);
			for ($n=0; $n<$cntUserFields; $n++)
			{
				$qname = $userFields[$n];
				//  error_log("Current key: ".$qname);
				$currentUser[$qname] = $_POST['ic_' . $cat_count . '_' . $qname . '_' . $item_count];

			}
			$currentUser['MealPlanGroupID'] = $category_id;

			$currentUser['delete'] =$_POST['ic_' . $cat_count . '_delete_' . $item_count];
			if (!filter_var($currentUser['email'], FILTER_VALIDATE_EMAIL))
			{
				$error .= $sep.'Invalid Email Address '.$currentUser['email'];
				$sep = ', ';
			}
			if ($user_email != $currentUser['email'])
			{
				$user_email = $currentUser['email'];
			}
			else
			{
				$error .= $sep.'Duplicate entry detected - The entered Email Address '.$currentUser['email'].' already exists';
				$sep = ', ';
			}
			if (empty($currentUser['id']))
			{
				$userID = checkUserExist($currentUser);
				if ($userID === 1)
				{
					$error .= $sep.'User already exists with Email Address '.$currentUser['email'];
				}
			}

			$item_count++;
		}
		if (!empty($error))
		{
			$error_arr[] = $error;
		}
		
		$cat_count++;
	}
	
	if (!empty($error_arr))
	{
		return $error_arr;
	}
	else {
		return 'success';
	}
}


function checkGroupExist($name)
{
	$restaurant_id = admin_info("companyid");
	$mealPlanGroupQuery = clavuQuery("SELECT * FROM `MealPlanGroup` WHERE name = '".clavu_query_encode($name)."' and `restaurant_id`='".clavu_query_encode($restaurant_id)."' and `_deleted` = 0");
	if (mysqli_num_rows($mealPlanGroupQuery)>0)
	{
		return 1;
	}
	else {
		return 0;
	}
}

function checkUserExist($userArray)
{
	$email = $userArray['email'];
	$restaurant_id = admin_info('companyid');
	$mealPlanGroupID = $userArray['MealPlanGroupID'];
	$mealPlanUserQuery = clavuQuery("SELECT * FROM `MealPlanUser` WHERE `email` = '".clavu_query_encode($email)."' and `restaurant_id`='".clavu_query_encode($restaurant_id)."' and `_deleted` = 0");
	if(mysqli_num_rows($mealPlanUserQuery)>0)
	{
		return 1;
	}
	else {
		return 0;
	}
}
?>
