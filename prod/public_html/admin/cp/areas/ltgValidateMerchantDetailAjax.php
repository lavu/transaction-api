<?php	
require_once( __DIR__.'/../../../inc/olo/braintree/BraintreeConnect.php');
	$params = $_POST;
	if(isset($params)) {
		$braintreeConnect = new BraintreeConnect($params);
		$result = $braintreeConnect->hasError ? [
			'status' => 'failure',
			'message' => $braintreeConnect->errorMessage
		] :  [
			'status' => 'success'
		];
		$data = json_encode($result);
		echo $data;	
	}
?>