<?php
/**
 * Created by PhpStorm.
 * User: Leif
 * Date: 1/19/17
 * Time: 9:15 AM
 */

	require_once(dirname(__FILE__)."/../resources/core_functions.php");
	require_once(resource_path()."/lavuquery.php");
	require_once(resource_path()."/chain_functions.php");
	session_start($_GET['session_id']);
	$req_schm = "https";
	if($_SERVER['HTTP_HOST'] == 'admin.localhost' || $_SERVER['HTTP_HOST'] == 'localhost:8888' ) {
		$req_schm = "http";
	}

	$mode = "settings_edit_exchange_rates";
	$edit = "1";
//		echo speak("Edit Exchange Rates")."<br />"."<br />";
	$styleSheetPath = $req_schm. "://" . $_SERVER['HTTP_HOST'] . "/cp/styles/styles.css";
	$display = "<!DOCTYPE html> ";
	$display .= "<html>";
	$display .= "<head>";
	$display .= "<meta http-equiv=\"Content-Type\" content=\"text/html;charset=ISO-8859-1\">";
	$display .= "<link rel='stylesheet' type='text/css' href='".$styleSheetPath."'>";
	$display .= "<script type=\"text/javascript\" src=\"/manage/js/jquery/js/jquery-1.9.0.js\"></script>";
	$display .= "<script type=\"text/javascript\" src=\"/manage/js/jquery/js/jquery-ui-1.10.0.custom.min.js\"></script>
				<link rel=\"stylesheet\" href=\"/manage/js/jquery/css/smoothness/jquery-ui-1.10.0.custom.min.css\" />
				<link href=\"/cp/css/select2-4.0.3.min.css\" rel=\"stylesheet\" />
				<script src=\"/cp/scripts/select2-4.0.3.min.js\"></script>";
	if(isset($_GET['refresh_for_save']) && $_GET['refresh_for_save'] == '1') {
		$_GET['refresh_for_save'] = null;
		$display .= "<script type='text/javascript'>window.parent.document.location.reload();</script>";
	}
	$display .= "</head>";
	$display .= "<body>";

	$display .= "<div id='exchange_rate_page_content'>";

	$field = "";

	$qtitle_category = false;
	$qtitle_item = "Rate";

	$category_tablename = "";
	$inventory_tablename = "`exchange_rates`";
	$category_fieldname = "";
	$inventory_fieldname = "setting";
	$inventory_select_condition = "";
	$inventory_primefield = "setting";
	$category_primefield = "";
	$inventory_orderby = "id";

	$companyId = sessvar("admin_companyid");
	$primaryResIdData = getCompanyInfo($companyId, 'id');
	$primaryResChainId = (!empty($primaryResIdData['chain_id']))?$primaryResIdData['chain_id']:null;
	$primaryResId = null;
	if($primaryResChainId!=null){
		$primaryResId = getPrimaryChainRestaurant($primaryResChainId);
	}
	$currencyCodes = getCurrencyCodes($primaryResId);
	$currencyCodesWithArchive = getCurrencyCodes($primaryResId, true);

	$currencyCodesWithoutArchive = $currencyCodes;

	$location_info = sessvar("location_info",null); //TODO after setting this check if it is isset and handle failure to retrieve (means session isn't active)

	$qfields = array();
	$qfields[] = array("effective_date", "effective_date", speak("MM/DD/YY"), "input");
	$qfields[] = array("from_currency_id", "from_currency_id", speak("Type"), "select", $currencyCodesWithArchive);
	$qfields[] = array("to_currency_id", "to_currency_id", speak("Type"), "select", $currencyCodesWithArchive); //Set to primary currency code
	$qfields[] = array("rate", "rate", speak("Rate"), "input");
	$qfields[] = array("delete");

	//TODO: it's only grabbing CurrencyTo Fields

	require_once(resource_path() . "/dimensional_form.php");
	$qdbfields = get_qdbfields($qfields);

	$details_column = "";//"Zip Codes";
	$send_details_field = "";//"zip_codes";
	$category_filter_by = "";
	$category_filter_value = "";
	$cfields = "";

	$qinfo = array();
	$qinfo["qtitle_category"] = $qtitle_category;
	$qinfo["qtitle_item"] = $qtitle_item;
	$qinfo["category_tablename"] = $category_tablename;
	$qinfo["inventory_tablename"] = $inventory_tablename;
	$qinfo["category_fieldname"] = $category_fieldname;
	$qinfo["inventory_fieldname"] = $inventory_fieldname;
	$qinfo["inventory_select_condition"] = $inventory_select_condition;
	$qinfo["inventory_primefield"] = $inventory_primefield;
	$qinfo["category_primefield"] = $category_primefield;
	$qinfo["details_column"] = $details_column;
	$qinfo["send_details"] = $send_details_field;
	$qinfo["field"] = $field;
	$qinfo["qsortby_preference"] = "";
	$qinfo['qtitle_category_plural'] = "";
	$qinfo["category_filter_by"] = $category_filter_by;
	$qinfo["category_filter_value"] = $category_filter_value;
	$qinfo["inventory_filter_value"] = "exchange_rates";
	$qinfo['inventory_filter_by'] = "";
	$qinfo["inventory_orderby"] = $inventory_orderby;

	$form_name = "exchange_rate_form";

	$RETURN_URL = $req_schm."://" . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
	$submission_url = $req_schm . "://" . $_SERVER['HTTP_HOST'] . "/cp/resources/submit_exchange_rates.php";
	$field_code = create_dimensional_form($qfields,$qinfo,$cfields);

	$activeCurrencyCodes = createArchivedListForJQuery($currencyCodes);

	$display .= "<div id='exchangeRateFields'><span>Effective Date</span><span>From</span><span>To</span><span>Rate</span></div>";

	$display .= "<table id='exchange_rate_form_table' cellspacing='0' cellpadding='3'><tr><td align='center'>";
	$display .= "<form id='$form_name' name='$form_name' method='post' action='$submission_url'>";
	$display .= "<input type='hidden' name='posted' value='1'>";
	$display .= "<input type='hidden' name='returnURL' value='$RETURN_URL'>";
	$display .= $field_code;

	$display .= "&nbsp;<br><input type='submit' id='btn_exchange_rate_form' value='".speak("Save")."' style='width:120px'>";

	$display .= "<div id='rate-error-tip'>Rate is limited to 5 digits before the decimal and 16 digits after <br />for example: xxxxx.xxxxxxxxxxxxxxxx</div>";
	$display .= "</form>";

	$display .= "</td></tr></table>";
	$display .= "</div>"; //end of exchange rate display content - div
//	$display .= "<div id='active_currency_container' style='display:block'>".$activeCurrencyCodes."</div>";
	$display .= $activeCurrencyCodes;
	$display .= getFullDropdownList($activeCurrencyCodes);
	$display .= "</body>";

/**********************************************************************************************************************/
//									SCRIPTS
/**********************************************************************************************************************/
	$display .= '<script type="text/javascript">
				var oldRowIdNums;

				(function($) {

					$(document).ready(function() {
						$( "input[name^=\'ic_1_effective_date\']" ).datepicker({
							dateFormat: \'mm/dd/y\', minDate: 0
						});
						$( "#add_button1").on( "click", "span", function( event ) {
							$( "input[name^=\'ic_1_effective_date\']" ).datepicker({
								dateFormat: \'mm/dd/y\',minDate: 0
							});
						});
						$( "#add_button2").on( "click", "span", function( event ) {
							$("input[name^=\'ic_1_effective_date\']" ).css({\'color\':\'#1c591c\'});
							$("input[name^=\'ic_1_effective_date\']" ).datepicker({
								dateFormat: \'mm/dd/y\',minDate: 0,
								onSelect: function () {$(this ).css({\'color\':\'#1c591c\'})},
							});
						});
						$( "#add_button_with_text").on( "click", "span", function( event ) {
							$( "input[name^=\'ic_1_effective_date\']" ).datepicker({
								dateFormat: \'mm/dd/y\',minDate: 0
							});
						});
					});
					var oldRowIds = $("input[id^=\'ic_1_effective_date\']");
					oldRowIdNums = new Array();
					for(var i = 0; i < oldRowIds.length; i++){
						var curId = oldRowIds[i].id;
						var row_id = curId.substr(curId.lastIndexOf("_") + 1);;
						oldRowIdNums.push(row_id);
					}

					$("#add_button_with_text").unbind("click").bind("click", ".addBtn",  function(){

						if( $("input[name^=\'ic_1_effective_date_\']").length > 0 )
						{
							$("input[name^=\'ic_1_effective_date_\']").datepicker({
								dateFormat: \'mm/dd/y\',minDate: 0
							});


							$("#ui-datepicker-div").css("background","#aecd37");
						}

						var lastSelect = $("select:last");

						if ( lastSelect ) {
							var lastToSelectID = lastSelect.attr("id");
							var tempId = lastToSelectID.replace("to","from");
							var lastFromSelectID = $("#"+tempId);
							lastFromSelectID.empty();
							lastSelect.empty();
						}
						$.each(active_currencies, function(key,value){
							var textValue = $(\'<textarea />\').html(this.value).text();
							lastSelect.append($("<option>").attr(\'value\',this.key).text(textValue));
							lastFromSelectID.append($("<option>").attr(\'value\',this.key).text(textValue));

						});
					});
				})(jQuery);

				function isNotArchivedRow(name){
					// debugger;
					var rowIndex = name.substr(name.lastIndexOf("_") + 1);
					if(oldRowIdNums.indexOf(rowIndex) >= 0){
						return false;
					}
					return true;
				}

				/**************** Start code for duplicate currency selection in same row **************************/

				var arCurrencyBoxDuplicacy = [];


				$(document).on("change", "form[name=\"exchange_rate_form\"]  ._cls_row_ select",  function (e) {

				        var currentSelectedID = $(this).prop("id");

				        $(this).css("border-color", "#ECEEEF");

				        if(currentSelectedID.indexOf("_from_currency_id_") != -1) {
				            var fromCurrency = $(this).val();
				            var toCurrencyID = currentSelectedID.replace("from", "to");
				            var toCurrency = $("#" + toCurrencyID).val();

				            if(fromCurrency == toCurrency) {
				                $("#" + currentSelectedID).css("border-color", "red");
				                $("#" + currentSelectedID).css("border-width", "3px");

				                $("#" + toCurrencyID).css("border-color", "red");
				                $("#" + toCurrencyID).css("border-width", "3px");

				                var index = arCurrencyBoxDuplicacy.indexOf(currentSelectedID);
				                if(index > -1) arCurrencyBoxDuplicacy.splice(index, 1);

				                var index = arCurrencyBoxDuplicacy.indexOf(toCurrencyID);
				                if(index > -1) arCurrencyBoxDuplicacy.splice(index, 1);

				                arCurrencyBoxDuplicacy.push(currentSelectedID);
				                arCurrencyBoxDuplicacy.push(toCurrencyID);
				            }
				            else {
				                var index = arCurrencyBoxDuplicacy.indexOf(currentSelectedID);
				                if(index > -1) arCurrencyBoxDuplicacy.splice(index, 1);

				                var index = arCurrencyBoxDuplicacy.indexOf(toCurrencyID);
				                if(index > -1) arCurrencyBoxDuplicacy.splice(index, 1);
				            }

				        }
				        if(currentSelectedID.indexOf("_to_currency_id_") != -1) {
				            var toCurrency = $(this).val();
				            var fromCurrencyID = currentSelectedID.replace("to", "from");
				            var fromCurrency = $("#" + fromCurrencyID).val();;

				            if(fromCurrency == toCurrency) {
				                $("#" + currentSelectedID).css("border-color", "red");
				                $("#" + currentSelectedID).css("border-width", "3px");

				                $("#" + fromCurrencyID).css("border-color", "red");
				                $("#" + fromCurrencyID).css("border-width", "3px");

				                var index = arCurrencyBoxDuplicacy.indexOf(currentSelectedID);
				                if(index > -1) arCurrencyBoxDuplicacy.splice(index, 1);

				                var index = arCurrencyBoxDuplicacy.indexOf(fromCurrencyID);
				                if(index > -1) arCurrencyBoxDuplicacy.splice(index, 1);

				                arCurrencyBoxDuplicacy.push(currentSelectedID);
				                arCurrencyBoxDuplicacy.push(fromCurrencyID);

				            }
				            else {
				                var index = arCurrencyBoxDuplicacy.indexOf(currentSelectedID);
				                if(index > -1) arCurrencyBoxDuplicacy.splice(index, 1);

				                var index = arCurrencyBoxDuplicacy.indexOf(fromCurrencyID);
				                if(index > -1) arCurrencyBoxDuplicacy.splice(index, 1);
				            }
				        }

				    });

					$(document).on("click", "form[name=\"exchange_rate_form\"] ._cls_row_ .closeBtn",  function (e) {

						var fromCurrencyID = $(this).parent().parent().parent().find("select").prop("id");
						var toCurrencyID = fromCurrencyID.replace("from", "to");


						var index = arCurrencyBoxDuplicacy.indexOf(fromCurrencyID);
						if(index > -1) arCurrencyBoxDuplicacy.splice(index, 1);

						var index = arCurrencyBoxDuplicacy.indexOf(toCurrencyID);
						if(index > -1) arCurrencyBoxDuplicacy.splice(index, 1);

						$("#" + fromCurrencyID).val("");
						$("#" + toCurrencyID).val("");

					});


				/**************** End code for duplicate currency selection in same row **************************/

				var preventSelect2ChangeForPreviouslySavedItems = true;
				$(document).on(\'click\', \'form[name="exchange_rate_form"] table.addBtn\', function(e) {
					if(preventSelect2ChangeForPreviouslySavedItems == true){
						$("select[name^=\'ic_1_from_currency_id\']").last().select2({
							placeholder: "Select Currency Code",
							data: fullCurrencyList,
							allowClear: true
						});

						$("select[name^=\'ic_1_to_currency_id\']").last().select2({
							placeholder: "Select Currency Code",
							data: fullCurrencyList,
							allowClear: true
						});
					}else{
						preventSelect2ChangeForPreviouslySavedItems = true;
					}
				});

				$(document).on(\'click\', \'form[name="exchange_rate_form"] #btn_exchange_rate_form\', function(e) {
					var submitFlag = 0;
					var counter = 0;
					var arrElemId = [];
					var strElemId = "";
				    var eachRow =[];
					var rowStr = "";

					$(\'form[name="exchange_rate_form"]\').find("input:text, select").filter(\':visible\').each(function() {
						if($(this).attr("name").indexOf("ic_1_rate_")!=0){
							var thisName = $(this).attr("name");
							var thisDeleteVal = 0;
							$(this).css(\'border-color\', \'#ECEEEF\');

							var elID = $(this).prop("id");
							var curElementValue = $(this).val();

							counter++;

							if((counter % 3) > 0) {
							    rowStr += curElementValue + "#";
							    strElemId += elID + "#";
							}
							else {
							    rowStr += curElementValue + "#";
							    strElemId += elID + "#";
							    eachRow.push(rowStr);
							    arrElemId.push(strElemId);
							    rowStr = "";
							    strElemId = "";
							}
						}
					});

					$(\'form[name="exchange_rate_form"]\').find(\'input:text, select\').filter(\':visible\').each(function() {

						var thisName = $(this).attr("name");
						var thisDeleteVal = 0;
						$(this).css(\'border-color\', \'#ECEEEF\');

						if(typeof(thisName) != "undefined"){
							var numIndex = thisName.lastIndexOf("_") + 1;
							var rowNum = thisName.substring(numIndex);
							var thisDeleteButtonId = "#ic_1_delete_" + rowNum;
							thisDeleteVal = $(thisDeleteButtonId).val();
						}

						if(typeof($(this).attr("name")) != "undefined"  && $(this).attr("name").indexOf("ic_1_id_")!=0  && isNotArchivedRow($(this).attr("name"))  && thisDeleteVal != "1"   )
		                {

							$(this).css(\'border-color\', \'#ccc\');
							document.getElementById("rate-error-tip").style.visibility = "hidden";

							var elVal = $(this).val();
							var isDateField = $(this).attr("name").indexOf("ic_1_effective_date");
							var isRateField = $(this).attr("name").indexOf("ic_1_rate");
							if(isDateField == 0 && !isValidDate(elVal))
							{
								$(this).css(\'border-color\', \'red\');
								$(this).css(\'border-width\', \'3px\');
								submitFlag++;
							}
							// debugger;
							if(isRateField == 0 && !checkNumeric(elVal))
							{
								$(this).css(\'border-color\', \'red\');
								$(this).css(\'border-width\', \'3px\');
								document.getElementById("rate-error-tip").style.visibility = "visible";
								submitFlag++;
							}

							if(elVal.length <= 0
								|| elVal == \'MM/DD/YY\'
								|| elVal == \'Rate\'
								|| elVal == \'Select Currency Code\')
							{
								$(this).css(\'border-color\', \'red\');
								$(this).css(\'border-width\', \'3px\');
								submitFlag++;
							}

							if (elVal.length > 0 && elVal.indexOf("Currency") != -1) {
								$(this).css(\'color\', \'red\');
								$(this).css(\'border-width\', \'1px\');
								submitFlag++;
							}else {
								$(this).css(\'color\', \'#5B5E23\');
							}
						}
					});

						var arrDuplicates = [];
						for(var i = 0; i < eachRow.length; i++) {
						    for(var j = i + 1; j <= eachRow.length; j++) {
						        if(eachRow[i] == eachRow[j]) {
						            arrDuplicates.push(i);
						            arrDuplicates.push(j);
						        }
						    }
						}

						if( arrDuplicates.length > 0 ) {
							for(var a = 0; a < arrDuplicates.length; a++) {
							    var key = arrDuplicates[a];
							    var arrIDs = [];
							    var strIDs = arrElemId[key];
							    arrIDs = strIDs.split("#");
							    for(var t = 0; t < arrIDs.length; t++) {
									submitFlag = 1;
									$("#" + arrIDs[t]).css(\'border-color\', \'red\');
									$("#" + arrIDs[t]).css(\'border-width\', \'3px\');
							  	}
							        arrIDs = [];
							}
							alert("Please check duplicate row selection!");
						}

						if( arCurrencyBoxDuplicacy.length > 0 ) {
							submitFlag = 1;

							for(var i = 0; i < arCurrencyBoxDuplicacy.length; i++) {
								$("#" + arCurrencyBoxDuplicacy[i]).css(\'border-color\', \'red\');
								$("#" + arCurrencyBoxDuplicacy[i]).css(\'border-width\', \'3px\');
							}

							alert("Please check duplicate currency selection!");
						}

						if (submitFlag == 0){
									var isConfirmed = false;
									if(!isConfirmed){
										var dialog = $("#dialog-confirm");
										dialog.load(
										$("#dialog-confirm").dialog({
											resizable: false,
											modal: true,
											height: "auto",
											width: 400,
											position: [\'middle\',28],
											buttons: {
												"Ok": function() {
												isConfirmed = true;
												$(\'#modal_close_0\', parent.document).hide();
												$(\'form[name="exchange_rate_form"]\').submit();
												$(this).dialog("close");
											},
											Cancel: function() {
												$(this).dialog("close");
											}
										}
									})
									);
									e.preventDefault();
							return false;
						}
						else
							return true;
					}else{
				        e.preventDefault();
				    }
				});

				(function($) {
					$(document).ready(function() {
						//Start Disable Input
						$("#ic_area_cat1").find(\'input:text, select \').each(function() {
							if($(this).val()!="" && $(this).val()!="MM/DD/YY" && $(this).val()!="Rate"){
								$(this).prop(\'disabled\', true);
							}
						});
						$("#ic_area_cat1").find(\'input:text, select \').each(function() {
							if($(this).val()!="" && $(this).val()!="MM/DD/YY" && $(this).val()!="Rate"){
								$(this).prop(\'disabled\', true);
							}
						});
						$(\'#ic_area_cat1 a.closeBtn\') .remove();
						//End Disable Input
						$("select[name^=\'config:currency_region\']").on("change", function(){
							var selectedRegionID = $(this).val();
							$("select[name^=\'config:primary_currency_code\']").empty();
							$("input[name=monitary_symbol]").val("");
							if(selectedRegionID > 0){
								$("input[name=monitary_symbol]").val("");
								$.ajax({
									url: "index.php?widget=advanced_location_settings&als_ajax&regionid="+selectedRegionID,
									data: { regionid: selectedRegionID },
									type: "POST",
									async: true,
									success: function(data) {
										$("select[name^=\'config:primary_currency_code\']").empty();
										$("select[name^=\'config:primary_currency_code\']")
												.append("<option value=\'\'>Select Currency Code</option>");
										var i=0;
										for(i=0;i<data.length;i++){
											$("select[name^=\'config:primary_currency_code\']")
												.append("<option value=\'"+data[i].alphabetic_code+"\'>"+data[i].alphabetic_code+"</option>");
												//.append("<option value=\'"+data[i].alphabetic_code+"\'>"+data[i].name+" ("+data[i].alphabetic_code+")</option>");
										}
									},
									error: function() {
										alert("Seems problem with server loading the primary currency");
									}
								});
							}else{
								alert("Please select Currency Region!");
							}
						});

						$("select[name^=\'config:primary_currency_code\']").on("change", function(){
							var currencyCode = $(this).val();

							$("input[name=monitary_symbol]").val("");
							if(currencyCode!=""){
								var selectedRegionId = $("select[name^=\'config:currency_region\']").val();
								$.ajax({
									url: "index.php?widget=advanced_location_settings&get_monetary_symbol=1",
									data: { code : currencyCode, regionId:selectedRegionId},
									type: "POST",
									async: true,
									success: function(data) {

										if(data!=null){
											if(data[0].monetary_symbol!=null){
												$("input[name=monitary_symbol]").val(data[0].monetary_symbol);
											}
										}
									},
									error: function() {
										alert("Seems problem with server loading the primary currency");
									}
								});
							}else{
								alert("Please select Primary Currency Code!");
							}
						});
					});
				})(jQuery);

			    // Validates that the input string is a valid date formatted as "mm/dd/yyyy"
			    function isValidDate(dateString){
			        // First check for the pattern
			        if(!/^\d{1,2}\/\d{1,2}\/\d{2}$/.test(dateString)){
			            return false;
			        }
			        // Parse the date parts to integers
			        var parts = dateString.split("/");
			        var day = parseInt(parts[1], 10);
			        var month = parseInt(parts[0], 10);
			        var year = parseInt(parts[2], 10);

			        // Check the ranges of month and year
			        if(year < 00 || year > 99 || month == 0 || month > 12)
			            return false;

			        var monthLength = [ 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 ];

			        // Adjust for leap years
			        if(year % 400 == 0 || (year % 100 != 0 && year % 4 == 0))
			            monthLength[1] = 29;


			//debugger;
					var today = new Date();
					today.setHours(0,0,0,0);
					var enteredDate = new Date(dateString);
					if(enteredDate < today-1){
						return false;
					}

			        // Check the range of the day
			        return day > 0 && day <= monthLength[month - 1];
			    };

				//validate rate field value for positive float
				function checkNumeric(elementVal){
					var fullElementVal = elementVal;
					if((!/^\d*?\.*?\d*?$/.test(elementVal))){
			            return false;
			        }
			        var withoutDecimal = elementVal.replace(".","");
			        withoutDecimal = withoutDecimal.replace(/^0+|0+$/g, ""); //Remove trailing and preceding zeros
			        if(withoutDecimal.length > 21){
			            return false;
			        }
			        if(parseFloat(elementVal) > 100000){ //Database currently rounds down values greater than 100000 to 100000, so validate for that.
			            return false;
			        }
			        var decimalIndex = fullElementVal.indexOf(".");

			        if(decimalIndex != -1 && !(decimalIndex == fullElementVal.length-1)){

			            var afterDecimal = fullElementVal.substr(decimalIndex+1);
						console.log("afterDecimal: " + afterDecimal);
						console.log("afterDecimal length: " + afterDecimal.length);
			            if(afterDecimal.length > 16){
			                return false;
			            }
			        }

			        return true;
				}

				$(document).on("keypress", "input[type=\'text\']", function(e) {
			        var thisName = $(this).attr(\'name\');
			        $(this).attr(\'maxlength\', 8);


			        if (thisName.indexOf("_date_") != -1) {

			            var specialKeys = new Array();
			            specialKeys.push(8); //Backspace
			            specialKeys.push(9); //Tab
			            specialKeys.push(46); //Delete
			            specialKeys.push(36); //Home
			            specialKeys.push(35); //End
			            specialKeys.push(37); //Left
			            specialKeys.push(39); //Right

			            var keyCode = e.keyCode == 0 ? e.charCode : e.keyCode;

			            var ret = ((keyCode >= 48 && keyCode <= 57) || (keyCode == 43) || (keyCode == 47) || (specialKeys.indexOf(e.keyCode) != -1 && e.charCode != e.keyCode));
			            return ret;
			        }

			        if (thisName.indexOf("_rate_") != -1) {
			            $(this).attr(\'maxlength\', 22);
			            var charCode = (e.which) ? e.which : e.keyCode;
			            if(charCode >= 48 && charCode <=57 || charCode == 46){
			                return true;
			            }else{
			                return false;
			            }
			        }
			    });

	</script>';

	$display .="</html>";
	echo $display;

/**********************************************************************************************************************/
//									FUNCTIONS
/**********************************************************************************************************************/
	function getCurrencyCodes($primaryResId, $archived=false){
		$implodedList = getMasterCurrencyCodeList($primaryResId,$archived);
		$currency_country=array();
		$currency_country[0][0]=speak("Select Currency Code");
		$currency_country[0][1]="";
		if ($implodedList != "")
		{
			$query = "SELECT `id`, `name`,`alphabetic_code` FROM `poslavu_MAIN_db`.`country_region` WHERE id IN ([1]) ORDER BY `name`";
			$result = mlavu_query($query, $implodedList);
			if(!$result){
				error_log("mysql error in ".__FILE__." mysql error:" . lavu_dberror());
			}else{
				$index = 1;
				while ($region_info = mysqli_fetch_assoc($result)) {
					$currency_country[$index][0] = formatEncodeCountryCodeWithName($region_info['alphabetic_code'], $region_info['name']);
					$currency_country[$index][1] = $region_info['id'];
					$index++;
				}
			}
		}
		return $currency_country;
	}

	function getMasterCurrencyCodeList($primaryResId, $archived=false){
		if(!empty($primaryResId)) {
		  if($archived){
			$master_query_string = "SELECT DISTINCT `country_region_id` FROM `poslavu_MAIN_db`.`master_currencies` WHERE `master_id`=$primaryResId";
		  } else {
			$master_query_string = "SELECT DISTINCT `country_region_id` FROM `poslavu_MAIN_db`.`master_currencies` WHERE `_archived` != 1 AND `_enabled` = 1 AND `master_id`=$primaryResId";
		  }
		  $master_query = mlavu_query($master_query_string);
		  $master_id_list = array();
		  while($item = mysqli_fetch_array($master_query)){
			$master_id_list[] = $item[0]."";
		  }
		  $implodedList = implode(",",$master_id_list);
		  return $implodedList;
	    }
    }

	function createArchivedListForJQuery($currencyCodes){
		$listString = "<script type='text/javascript'>";
		$listArray = array();
		for($i = 0; $i < count($currencyCodes); $i++){
			$val = $currencyCodes[$i][0];
			$listArray[$i] = array("key"=>$currencyCodes[$i][1],"value"=>$val);
		}
		$listString .= " var active_currencies=".json_encode($listArray);
		$listString .= "</script>";
		return $listString;
	}

	function getFullDropdownList($currencyCodes){
		$listString = "<script type='text/javascript'>";
		$listArray = array();
		for ($i = 0; $i < count($currencyCodes); $i++) {
			$listArray[$i] = array("id" => $currencyCodes[$i][1], "text" => $currencyCodes[$i][0]);
		}
		$listString .= " var fullCurrencyList=" . json_encode($listArray) . ";";
		$listString .= "</script>";
		return $listString;
	}
?>
<div id="dialog-confirm" title="Confirmation" style="display:none;">
  <p>Are you sure you want to save? Changes are permanent!</p>
</div>
