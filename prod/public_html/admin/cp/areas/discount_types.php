<?php

if(!$in_lavu){exit();}

if(is_lavu_admin()){
	//--------------------------------------------- Handle Passbook Coupon Sending ----------------------------------------//
	$passbook_discount = "";
	$passbook_email = "";
	// MEANS THAT SEND BUTTON WAS PRESSED, 'passbook_discount' is which discount is in the select.
	// Which pb coupon was held in the dom, thus we have to post to ourself so the server can
	// handle the event.
	if(isset($_POST['passbook_discount'])) { handleCommandToSendForcedCouponBatchRequest(); }
	//SHOWING THE TEXTAREA FOR ENTERING THE EMAIL LIST.
	if(!isset($_POST['passbook_discount']) && isset($_GET['batch_request']) ) { echo getHTMLForForceSendingCoupons(); }
	//---------------------------------------------------------------------------------------------------------------------//
}

$call_post_action_function = true;

//Need to calculate a week from now for default to date chooser.
//function localize_datetime($timestamp, $to_tz)
$hasPassbookEnabled = hasPassbookEnabled($data_name);
$hasPepper = $modules->hasModule("extensions.loyalty.pepper");
$todayDate = localize_date(date("Y-m-d H:i:s"), $locinfo['timezone']);
$weekFromNow = date("Y-m-d", strtotime($todayDate . ' + 7 day'));

$tablename = 'discount_types';
$forward_to = "index.php?mode={$section}_{$mode}";

$specialFieldArr = array("" => "Standard", "loyaltree" => "LoyalTree", "heartland_loyalty" => "Heartland Loyalty", "mercury_loyalty" => "Mercury Loyalty", "lavu_loyalty" => "Lavu Loyalty");
if($hasPassbookEnabled) $specialFieldArr['passbook'] = "Passbook";

$applicableTypeArr = array("items" => "Items Only", "checks" => "Checks","" => "Both" );
if ($hasPepper) {
	$applicableTypeArr = array("lt_item" => "Items Only (Pepper Perk)", "items" => "Items Only", "checks" => "Checks","" => "Both" );
}
$accessLevelArr = array("1" => "Access Level 1", "2" => "Access Level 2", "3" => "Access Level 3", "4" => "Access Level 4");
$fields = array();
$fields[] = array(speak("Discount Type"),    'special', "select", $specialFieldArr, "list:yes");
$fields[] = array(speak("Button Title"),            'title',   'text',  'size:30', 'list:yes');
$fields[] = array(speak("Receipt Label"),            "label",   'text',  'size:30', 'list:yes');
$fields[] = array(speak("Calculation Type"), "code",    'select', array("p" => "Percentage", 'd' => "Amount"),'list:yes');
$fields[] = array(speak("Discount"),         "calc",    'text',  'size:30', 'list:yes');
$fields[] = array(speak("Apply To"),    "level",   'select', $applicableTypeArr, 'list:yes');
$fields[] = array(speak("Access Level"),     "min_access", 'select', $accessLevelArr, 'list:yes');
$fields[] = array(speak("Prompt for Reason")." (2.3.12+)", "get_info", "bool", "", "list:no");

if ($hasPepper) {
	$fields[] = array(speak("Token"), "token", 'text', 'size:40', 'list:yes');
	$fields[] = array(speak("Limit To"), "limit_to", 'text', 'size:30', 'list:yes');
}
else {
	$fields[] = array(speak("Loyalty Points"), "loyalty_cost", 'text', 'size:30', 'list:yes');
	$fields[] = array(speak("Loyalty Style"), "loyalty_use_or_unlock", 'select', array("0" => "None", "1" => "Costs Points", '2' => "Unlocks at points"),'list:yes');
}

$fields[] = array("Location","loc_id","hidden","","setvalue:$locationid");

if($hasPassbookEnabled){
	$fields[] = array(speak("(For Passbook) Expiration"), "expiration_date", 'date_chooser', "default_date:$weekFromNow", 'list:yes');
}
$fields[] = array(speak("Submit"),"submit","submit");

$descriptions = array();
$descriptions['special'] = " ";
$descriptions['title'] = "<br/>Appears in the POS Lavu app at the Check Out screen.";
$descriptions['label'] = "<br/>Appears on customer receipts and Passbook coupons.";
$descriptions['calc'] = "<br/>Enter Percentage discounts as a decimal. <br>e.g. Enter <b>0.50</b> for a 50% discount. <br><br> Enter Amount discounts as an amount to be discounted. <br>e.g. Enter <b>5</b> for a $5.00 discount";
$descriptions['code'] = "<br/>Discount a percentage or amount.";
$descriptions['min_access'] = "<br>Minimum access level required to apply this discount.";
$descriptions['get_info'] = "<br>Ask user to provide details about the reason for the discount before applying it.";
$descriptions['expiration_date'] = "Only applies to Passbook discounts.  This is when the passbook coupon expires.";
$descriptions['level'] = " ";
$descriptions['loyalty_cost'] = "<br>The number of loyalty points connected to a discount";
$descriptions['loyalty_use_or_unlock'] = "<br>Whether a discount spends loyalty points or just requires a customer to have them";
$descriptions['token'] = "<br>Used when creating a new Perks in the Pepper Console<br><br>";
$descriptions['limit_to'] = "<br>Limits \"Items Only (Pepper Perk)\" discounts to certain Menu Items.<br><br>You will need to enter each eligible Menu Item ID (shown in the Details popup on the Menu page) in the format below:<br><br><code>id=17 || id=885 || category_id=302</code>";

$filter_by = "`loc_id` = '$locationid'";

require_once(resource_path() . "/browse.php");

//We have to handle the case if something was deleted.
if($_GET['mode'] == 'discount_types' && !empty($_GET['removeid']) ){
	run_post_action();
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// ONLY FUNCTIONS BELOW
function run_post_action(){
	saveButtonPressed();
}

function getHTMLForForceSendingCoupons(){
	global $mode, $locationid;
	  $pb_query = lavu_query("select * from `discount_types` where `special`='passbook' and `_deleted`!='1'",$locationid);


	$htmlString =	"<form name='pbsend' method='post' action='index.php?mode=$mode'>" .
						"<table cellspacing=0 cellpadding=12 bgcolor='#dddde2' style='border:solid 1px #9999aa'>" .
						"<tr><td align='left'>" .
						"<table><tr><td>" .
						"Send Passbook Coupon " .
						"<select name='passbook_discount' id='pb_discount_selector'>";

	$discount_id_arr = array();

	while($pb_read = mysqli_fetch_assoc($pb_query)){
		$pb_id = $pb_read['id'];
		$discount_id_arr[] = $pb_id;
		$pb_name = $pb_read['label'];
		$htmlString .= "<option value='$pb_id'";
		if($passbook_discount==$pb_id){
			$htmlString .= " selected";
		}
		$htmlString .= ">$pb_name</option>";
	}

	//Begin javascript
	$htmlString .= "<script type='text/javascript'>" .
						"var pb_discount_ids = new Array();";
	foreach($discount_id_arr as $currDiscountID){
		$htmlString .= "pb_discount_ids.push($currDiscountID);";
	}

	//Now we create the final submit function, which will carry our discount_id
	$htmlString .= "function submit_dts_form(){" .
							"var discount_id_holder = document.getElementById('discount_id_hidden');" .
							"var discount_id_selector = document.getElementById('pb_discount_selector');" .
							"var pb_ids_selectedIndex = discount_id_holder.selectedIndex;" .
							"discount_id_holder.value = pb_discount_ids[discount_id_selector.selectedIndex];" .
							"document.pbsend.submit();" .
						 "}" .
						 "</script>";
	//End Javascript
	$htmlString .= "</select>".
						" to:".
						"<br><textarea rows='4' cols='60' name='passbook_email'>$passbook_email</textarea>".
						"</td></tr>".
						"<tr>".
						"    <td align='left'>".
						"       <input type='hidden' id='discount_id_hidden' value='MARKERVALUE' name='discount_id'>".
						"       <input type='button' value='Send >>' onclick='submit_dts_form()' />".
						"    </td>".
						"</tr>".
						"</table>".
						"</td></tr>".
						"</table>".
						"</form>".
						"<br><br>";

	return $htmlString;
}

function handleCommandToSendForcedCouponBatchRequest(){
	global $data_name, $locationid;
	// we are now handling the user having pressed the send button.
	$show_passbook_form = false;
	$passbook_discount = $_POST['passbook_discount'];
	$passbook_email = $_POST['passbook_email'];
	$send_to_emails = array();
	$passbook_email_list = explode(",",$passbook_email);
	for($i=0; $i<count($passbook_email_list); $i++){
		$inner_emails = explode(";",$passbook_email_list[$i]);
		for($n=0; $n<count($inner_emails); $n++){
			 //Here is the simple email verification
			 //TODO insert the php email filter that I use in the Lavutogo part.
			$iemail = trim($inner_emails[$n]);
			if($iemail!="" && strpos($iemail,"@") && strpos($iemail,".")) $send_to_emails[] = $iemail;
		}
	}

	$emailstr = "";
	for($i=0; $i<count($send_to_emails); $i++){
		if($emailstr!=""){
			$emailstr .= "; ";
		}
		$emailstr .= $send_to_emails[$i];
	}

	if($emailstr==""){
		echo "<b><font color='#880000'>Error: There are no valid email addresses.</font></b><br><br>";
		$show_passbook_form = true;
	}else{
		//Here we make the call to the lavutogo part of the system.
		$d_query = lavu_query("select * from `discount_types` where `special`='passbook' and `loc_id`='[1]' and `_deleted`!='1' and `id`='[2]'",$locationid,$passbook_discount);
		if(mysqli_num_rows($d_query))
		{
			$d_read = mysqli_fetch_assoc($d_query);
			//A few more I need...
			//$companyName = admin_info("company_name");//This is strangely showing up blank, so we just perform a query to get it.
			$compNameQueryStr = "SELECT * FROM `poslavu_MAIN_db`.`restaurants` WHERE `data_name`='[1]'";
			$compNameQuery = mlavu_query($compNameQueryStr, $data_name);//poslavu_MAIN_db
			$r_read;
			if(mysqli_num_rows($compNameQuery)){
				$r_read = mysqli_fetch_assoc($compNameQuery);
			}else{
				$r_read = array();
			}
			//We append the needed information from the restaurants row.
			$d_read['company_name'] = $r_read['company_name'];
			$d_read['image_name']   = $r_read['logo_img'];
			$d_read['email_list'] = $emailstr;
			$d_read['data_name']  = $data_name;
			$d_read['security_hash'] = sha1($emailstr . $data_name . 'Mr_JellyBean');
			//$d_read['expiration_date'] = $_POST['expiration_date'];
			//The URL and post variables (going to lavu togo server).
			$url = 'https://www.lavutogo.com/v1/Passbook/CouponBatchRequest.php';
			$d_read['discount_id'] = $_POST['discount_id'];
			$vars = http_build_query($d_read);
			$ch = curl_init();
			curl_setopt($ch, CURLOPT_URL, $url);
			curl_setopt($ch, CURLOPT_POST, 1);
			curl_setopt($ch, CURLOPT_POSTFIELDS, $vars);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			$response = curl_exec($ch);//   <---Here we call to the lavutogo server.   THIS IF FOR THE DIRECT COUPON CREATION, NOT SYNC COUPON DATA.
			curl_close ($ch);
			$response_parts = explode("|", $response);
			//If we have success returned from the lavutogo server...
			if($response_parts[0]=="success" && count($response_parts) > 1){
				echo "<b><font color='#08800'>" . trim($response_parts[1]) . "</font></b><br><br>";
			}else if($response_parts[0]=="error" && count($response_parts) > 1){//Error
				echo "<b><font color='#880000'>Error: ".trim($response_parts[1])."</font></b><br><br>";
				$show_passbook_form = true;
			}else{ //Can't communicate
				echo "<b><font color='#880000'>Error: Unable to communicate with Lavu Passbook Server.</font></b><br><br>";
				$show_passbook_form = true;
			}
		}else{
			echo "<b><font color='#880000'>Error: Unable to load coupon.</font></b><br><br>";
			$show_passbook_form = true;
		}
	}
}

//Sends a request to sync coupon information to the couponing server.
function syncCouponInformation($dataNameParam){
	$databaseName = 'poslavu_' . $dataNameParam . '_db';
	//Here we perform a query on the discount type information.
	lavu_connect_dn($dataNameParam,$databaseName );
	$getPassbooks = "SELECT * FROM `[1]`.`discount_types` WHERE `special`='passbook' && (`_deleted`='0' OR `_deleted`='')";
	$passbookResult = lavu_query($getPassbooks, $databaseName);
	if(!$passbookResult){
		return;
	}
	$rows = array();
	while($currRow = mysqli_fetch_assoc($passbookResult)){
		$rows[] = $currRow;
	}
	//We need to find the image name
	$getLogoFilenameSelect  = "SELECT `logo_img`,`company_name` FROM `poslavu_MAIN_db`.`restaurants` WHERE `data_name`='[1]'";
	$logoImgResult = mlavu_query($getLogoFilenameSelect, $dataNameParam);//poslavu_MAIN_db
	$imageName = ''; $companyName = '';
	if($logoImgResult && mysqli_num_rows($logoImgResult)){
		$row = mysqli_fetch_assoc($logoImgResult);
		$imageName = $row['logo_img'];
		$companyName = $row['company_name'];
	}
	//We now generate the coupon list
	$couponListArr = array();
	foreach($rows as $currRow){
		//Gotta find the discount type.
		$codeMap = array( 'd' => 'amount', 'p' => 'percent' );
		$currCouponDict = array();
		$currCouponDict['discount_id'] = $currRow['id'];
		$currCouponDict['discount_type'] = $codeMap[$currRow['code']]; //Either 'amount' or 'percent'
		$currCouponDict['discount_value'] = $currRow['calc'];
		$currCouponDict['discount_object'] = $currRow['label'];
		$currCouponDict['start_date'] = $currRow['valid_start_date_inc'];
		$currCouponDict['end_date'] = $currRow['valid_end_date_inc'];
		$currCouponDict['expiration'] = $currRow['expiration_date'];
		$couponListArr[] = $currCouponDict;
	}
	//Now we want to build the expected json.
	$globalJSONAsArr = array();
	$globalJSONAsArr['data_name']  = $dataNameParam;
	$globalJSONAsArr['image_name'] = $imageName;
	$globalJSONAsArr['company_name'] = $companyName;
	$globalJSONAsArr['coupon_list'] = $couponListArr;
	$merchantSyncDataJSON = json_encode($globalJSONAsArr);
	$postString  = "lv_coupon_sync_data=" . urlencode($merchantSyncDataJSON);
	$postString .= "&jibberish=jJ29Sk3lSvioelDh310ZAQle059263jHfkeFk3927dDk";
	$postString .= "&security_hash=" . sha1($dataNameParam . "MR_BEAN" . $dataNameParam);
	$postString .= "&data_name=".$dataNameParam;
	//Finally we encode this and curl it.
	$url = "https://www.lavutogo.com/v1/Passbook/_CrossServerScripts/sync_couponing_merchant.php";
	$ch = curl_init($url);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($ch, CURLOPT_POST, true);
	curl_setopt($ch, CURLOPT_POSTFIELDS, $postString);
	$returnString = curl_exec($ch);
	curl_close($ch);
	return $returnString;
}

function saveButtonPressed(){
	global $data_name;
	if(hasPassbookEnabled($data_name)){
		$couponingServerResponseJSON = syncCouponInformation($data_name);
		$returnJSONDict = json_decode($couponingServerResponseJSON);
		error_log('$couponingServerResponseJSON: ' . $couponingServerResponseJSON);
		//Due to an error in the json_decode implementation
		$returnJSONDict = $returnJSONDict[0];
		if( !is_array($returnJSONDict) ){
			$javaScriptus  =  "<script type='text/javascript'>";
			$javaScriptus .=  " alert('Network communication problem -- Error_Code:ds11000'); </script>";
			echo $javaScriptus;
		}else if($returnJSONDict['status'] == 'failed'){
			$errorCode = $returnJSONDict['e'];
			$errorDisplayMSG = preg_replace("/'/", "\\'", $returnJSONDict['fail_display']);//Keep from breaking the javascriptus alert('').
			$javaScriptus = "<script type='text/javascript'> alert('$errorDisplayMSG\\n\\n -- Error_Code:$errorCode --'); </script>";
			echo $javaScriptus;
		}else if($returnJSONDict['status'] == 'success'){
			if(isset($returnJSONDict['warning_display'])){
				$warningDisplay = preg_replace('/\'/', '\\\'', $returnJSONDict['warning_display']);
				$javaScriptus  = "<script type='text/javascript'> alert('$warningDisplay');";
				$javaScriptus .= "</script>";
				echo $javaScriptus;
			}
		}
	}
}

function hasPassbookEnabled($dataname){
	$hasEnabled = (
		//Demo accounts:
		$dataname == 'demo_brian_d'		||
		$dataname == '16edison'			||
		$dataname == '17edison'			||
		$dataname == 'ersdemo'			||
		//Live Demo's
		$dataname == 'cozy_pines_res' 	||
		$dataname == 'mean_bao_llc' 	||
		//real accounts
		$dataname == 'asian_noodle_b'	||
		$dataname == 'vinaigrette'		||
		$dataname == 'bosque2_brewer'	||
		$dataname == 'blackbird_buve'	||
		$dataname == 'sister'			||
		$dataname == 'prickly_pear1'	||
		$dataname == 'cake_fetish'		||
		$dataname == 'brickyard_pizz1'	||
		$dataname == 'tractor_brewin'	||
		$dataname == 'Farm_and_Table'	||
		$dataname == 'el_patio'			||
		$dataname == 'flamez_burgers'	||
		$dataname == 'gold_st_cafe'		||
		$dataname == 'lumpys_burgers1'	||
		$dataname == 'nicky_vs_neigh'	||
		$dataname == 'sushiking'		||
		$dataname == 'the_daily_grin1'	||
		$dataname == 'trinity_nails_'	||
		$dataname == 'urban_hot_dog'	||
		$dataname == 'wasabi_japanes'	||
		$dataname == 'burito_expres'	||
		$dataname == 'fat_squirrel_p'	||
		$dataname == 'cafe_trang1'		||
		$dataname == 'pizza_chain_de2'	||
		$dataname == 'green2go'			||
		$dataname == 'food_power_co_'	||
		$dataname == 'aston31'			||
		$dataname == 'five_wine_bar'	||
		$dataname == 'dress'			||
		$dataname == 'itdatacom1'		||
		$dataname == 'itdatacom'
	);

  return $hasEnabled;
}
?>
