<?php
	if($in_lavu)
	{
		$form_posted = (isset($_POST['posted']));

		$menu_id = $location_info['menu_id'];

		$display = "";
		$field = "force_modifiers";
		$qtitle_category = "Mod List";
		$qtitle_category_plural = "Lists";
		$qtitle_item = "Choice";

		$category_tablename = "forced_modifier_lists";
		$inventory_tablename = "forced_modifiers";
		$category_fieldname = "id";
		$inventory_fieldname = "list_id";
		$inventory_select_condition = "";
		$inventory_primefield = "title";
		$category_primefield = "title";

		$page_categories_by = 30;

		$inventory_orderby = "_order";

		if (!isset($data_name))
			$data_name = admin_info("dataname");

		$detour_list = array(array("Detour",""));
		$detour_query = lavu_query("select * from `forced_modifier_lists` where `_deleted`!='1' AND (`menu_id` = '[1]' OR `menu_id` = '0') order by `title` asc", $menu_id);
		while($detour_read = mysqli_fetch_assoc($detour_query))
		{
			$detour_list[] = array($detour_read['title'],"l".$detour_read['id']);
		}
		$detour_query = lavu_query("select * from `forced_modifier_groups` where `_deleted`!='1' AND (`menu_id` = '[1]' OR `menu_id` = '0') order by `title` asc", $menu_id);
		while($detour_read = mysqli_fetch_assoc($detour_query))
		{
			$detour_list[] = array("Group: ".$detour_read['title'],"g".$detour_read['id']);
		}
		$happy_query = lavu_query("select * from `happyhours` where `_deleted`!='1'");
		$happy_list[] = array('Select Happy Hours','0');
		while($happy_read = mysqli_fetch_assoc($happy_query))
		{
			$happy_list[] = array($happy_read['name'],$happy_read['id']);
		}
		require_once(dirname(dirname(dirname(dirname(__FILE__)))).'/sa_cp/manage_chain_functions.php');
		$a_chain_groups = array();
		$a_chain_items = array();
		$b_part_of_chain = getReportingGroupOptionsForDimensionalForm($data_name, $a_chain_groups, $a_chain_items);
		$menu_updated = FALSE;

		$cfields = array();
		$cfields[] = array("happyhours_id","happyhours_id",speak("happy hours"),"select",$happy_list);
		$cfields[] = array("type", "type", speak("Type"), "select", array("checklist", "choice"));
		$checklist_limit = lavu_query("SELECT `id` FROM `config` WHERE `setting` = 'enable_limited_checklist_modifiers' AND `value` = 1 AND `location` = '[1]'", $locationid);
		if (@mysqli_num_rows($checklist_limit)) {
			$type_option = array(array('No selection limit', 0), array('Choose 1', 1), array('Choose 2', 2), array('Choose 3', 3), array('Choose 4', 4), array('Choose 5', 5), array('Choose 6', 6));
			$cfields[] = array("type_option", "type_option", speak("type option"), "select", $type_option);
		}
		if ($b_part_of_chain && DEV)
			$cfields[] = array("chain_reporting_group", "chain_reporting_group", speak("Chain Reporting Group"), "select", $a_chain_groups);

		$category_filter_by = "type";
		$category_filter_value = array("","choice","checklist");
		$category_filter_extra = "(`menu_id` = '$menu_id' OR `menu_id` = '0')";

		$productTaxFlag = false;
		if ($location_info['display_product_code_and_tax_code_in_menu_screen']) {
			$productTaxFlag = true;
			$nameWidth = array("width:220px");
			$pricewidth = array("width:80px");
			$productCodeWidth = array("width:100px");
			$taxCodeWidth = array("width:80px");
		}

		$qfields = array();
		$qfields[] = array("title","title",speak("Title"),"input", $nameWidth);
		$qfields[] = array("cost","cost",speak("Price"),"input", $pricewidth);
		if ($b_part_of_chain && DEV)
			$qfields[] = array("chain_reporting_group", "chain_reporting_group", speak("Chain Reporting Group"), "select", $a_chain_items);

		if ($productTaxFlag) {
			$qfields[] = array("product_code","product_code",speak("Product Code"),"input", $productCodeWidth);
			$qfields[] = array("tax_code","tax_code",speak("Tax Code"),"input", $taxCodeWidth);
		}

		$qfields[] = array("ingredients","ingredients","Ingredients","special");
		$qfields[] = array("nutrition","nutrition",speak("Nutrition"),"special");//LP-1411
		$qfields[] = array("happyhours_id","happyhours_id",speak("Happy Hours"),"select",$happy_list);
		$qfields[] = array("detour","detour",speak("Detour"),"select",$detour_list);

		if(isset($comPackage) && $comPackage=="pizza")
		{
			$qfields[] = array("image","extra5","Picture","picture_grid_4_x","/images/test_pizza_sho/fmods/");
		}

		$qfields[] = array("id");
		$qfields[] = array("delete");
		$qfields[] = array("isOrderTag","isOrderTag","isOrderTag","hidden","");
		require_once(resource_path() . "/dimensional_form.php");
		$qdbfields = get_qdbfields($qfields);

		if($form_posted)
		{
		    $menu_updated = TRUE;
			$cat_count = 1;
			$cacheForcedModifiers = array();
			while(isset($_POST["ic_".$cat_count."_category"]))
			{
				$category_name = $_POST['ic_'.$cat_count.'_category'];
				$category_id = $_POST['ic_'.$cat_count.'_categoryid'];
				$category_delete = $_POST['ic_'.$cat_count.'_delete_0'];
				if ($b_part_of_chain && DEV)
					$category_chain_reporting_group = $_POST['ic_'.$cat_count.'_col_chain_reporting_group'];
				if($category_name==$qtitle_category . " Name") $category_name = "";

				if($category_delete=="1")
				{
					delete_poslavu_dbrow($category_tablename, $category_id);
				}

				if($category_name!="")
				{
					$dbfields = array();
					$dbfields[] = array("title",$category_name);
					$dbfields[] = array("menu_id",$menu_id);
					for($n=0; $n<count($cfields); $n++)
					{
						$cfieldname = $cfields[$n][0];
						if ($cfieldname == 'type_option' && $_POST['ic_'.$cat_count.'_col_'.$cfieldname] != 1 && $_POST['ic_'.$cat_count.'_col_'.$cfieldname] != 0) {
							$mod_count = 0;
							for ($gen_count = 1; $gen_count < count($_POST); $gen_count++)  {
								$str =  "ic_".$cat_count."_delete_".$gen_count;
								if (isset($_POST[$str])){
									if ($_POST[$str] == 0) {
										$mod_count++;
									}
								} else {
									break;
								}
							}
							if( $_POST['ic_'.$cat_count.'_col_'.$cfieldname] > $mod_count){
								$_POST['ic_'.$cat_count.'_col_'.$cfieldname] = 0;
							}
						}
						$dbfields[] = array($cfieldname,$_POST['ic_'.$cat_count.'_col_'.$cfieldname]);
					}

					$categoryid_indb = update_poslavu_dbrow($category_tablename,$dbfields, $category_id);
					if($category_fieldname=="title") {
						$categoryid_indb = $category_name;
					}

					$item_count = 1;
					while(isset($_POST["ic_".$cat_count."_".$inventory_primefield."_".$item_count]))
					{
						$item_value = array(); // get posted values
						for($n=0; $n<count($qfields); $n++)
						{
							$qname = $qfields[$n][0];
							$qtitle = (isset($qfields[$n][2]))?$qfields[$n][2]:false;
							$set_item_value = isset($_POST['ic_'.$cat_count.'_'.$qname.'_'.$item_count]) ? $_POST['ic_'.$cat_count.'_'.$qname.'_'.$item_count] : "" ;
							if($qtitle && $set_item_value==$qtitle) $set_item_value = "";
							if($qname=="cost") {
							$set_item_value = formatValueForMonetaryDisplay( $set_item_value );
							if ($set_item_value == '') {
							$set_item_value = 0;	
							}
							}
							if($qname == 'isOrderTag') {
							    $isOrderTag = $set_item_value;
							}
							else {
							    $item_value[$qname] = $set_item_value;
							}
						}
						$item_id = $item_value['id'];
						$item_delete = $item_value['delete'];
						$item_name = $item_value[$inventory_primefield];

						if($item_delete=="1")
						{
							delete_poslavu_dbrow($inventory_tablename, $item_id);
							//If the item is set to be deleted, delete it from the 86count table.
							delete_poslavu_86count($inventory_tablename, $item_id);
						}
						else if($item_name!="")
						{
							$dbfields = array();
							$dbfields[] = array($inventory_fieldname,$categoryid_indb);
							//Make array to set forced modifiers into cache
							$cacheForcedModifiers[$item_id]['id'] = $item_id;
							$cacheForcedModifiers[$item_id]['title'] = $item_value['title'];
							for($n=0; $n<count($qdbfields); $n++)
							{
							    $qname = $qdbfields[$n][0];
							    if ($qname != "isOrderTag") {
							        $qfield = (isset($qdbfields[$n][1]))?$qdbfields[$n][1]:$qname;

							        if ($qname == "title" && preg_match( "/^\d+$/", $item_name) && $isOrderTag==1) {
							            $checkOrderTypeQuery = lavu_query("SELECT `id`, `name` FROM `order_tags` WHERE `_deleted` != '1' AND `active` != '0' AND `id` = [1]", $item_name);
							            if (mysqli_num_rows($checkOrderTypeQuery)>0) {
							                $orderTypeRead = mysqli_fetch_assoc($checkOrderTypeQuery);

							                $dbfields[] = array($qfield, $orderTypeRead['name']);
							                $dbfields[] = array('ordertype_id', $orderTypeRead['id']);
							            }
							        } else {

							            $dbfields[] = array($qfield,$item_value[$qname]);
							        }
							    }
							}
							$orderby_tag = 'icrow_'.$cat_count.'_'.$item_count.'_order';
							if(isset($_POST[$orderby_tag]))
							{
								$dbfields[] = array("_order",$_POST[$orderby_tag]);
							} else {		//this is in testing, the idea is to keep theordering the way that the user created it without having to have them manually set anything (basically overwrite the alphabetical stuff)
								$dbfields[] = array("_order",$item_count);
							}
							update_poslavu_dbrow($inventory_tablename,$dbfields, $item_id);
						}
						$item_count++;
					}
				}
				$cat_count++;
			}

			//Storing forced modifiers id and title value into cache
			setCacheForcedModifiers($cacheForcedModifiers);
			// LOCAL SYNC RECORD
			schedule_remote_to_local_sync_if_lls($location_info, $locationid, "table updated", "forced_modifiers");
      /** 2017-07-24, LP-727 modified by david.einerson@lavu.com regarding
        * pizza creator bug where fmod lists were not sync'd from cloud to LLS, nor POS, when a list was changed in the control panel.
        * Added two tables to this array: forced_modifier_lists and forced_modifier_groups
        * Intended to sync the forced_modifier_lists table from cloud to LLS when changes are made to the pizza fmod lists.
        * The forced_modifier_groups table was added for good measure.
        * See also: ~/Repos/lavu/prod/public_html/admin/cp/resources/form.php
        */
        schedule_remote_to_local_sync_if_lls($location_info, $locationid, "table updated", "forced_modifier_lists");
        schedule_remote_to_local_sync_if_lls($location_info, $locationid, "table updated", "forced_modifier_groups");
			// END LOCAL SYNC RECORD

			$check_for_last_update = lavu_query("SELECT `id` FROM `config` WHERE `setting` = 'menu_last_updated' AND `location` = '[1]'", $locationid);
			if (@mysqli_num_rows($check_for_last_update)) {
				$uinfo = mysqli_fetch_assoc($check_for_last_update);
				lavu_query("UPDATE `config` SET `value` = now() WHERE `id` = '[1]'", $uinfo['id']);
			} else lavu_query("INSERT INTO `config` (`location`, `type`, `setting`, `value`) VALUES ('[1]', 'admin_activity', 'menu_last_updated', now())", $locationid);

			lavu_query("UPDATE `devices` SET `needs_reload` = '1' WHERE `loc_id` = '[1]'", $locationid);

			$form_error = "Warning: This section under Development";
		}

		$details_column = "";//"Zip Codes";
		$send_details_field = "";//"zip_codes";

		$qinfo = array();
		$qinfo["qtitle_category"] = $qtitle_category;
		$qinfo["qtitle_category_plural"] = $qtitle_category_plural;
		$qinfo["qtitle_item"] = $qtitle_item;
		$qinfo["category_tablename"] = $category_tablename;
		$qinfo["inventory_tablename"] = $inventory_tablename;
		$qinfo["category_fieldname"] = $category_fieldname;
		$qinfo["inventory_fieldname"] = $inventory_fieldname;
		$qinfo["inventory_select_condition"] = $inventory_select_condition;
		$qinfo["inventory_primefield"] = $inventory_primefield;
		$qinfo["category_primefield"] = $category_primefield;
		$qinfo["details_column"] = $details_column;
		$qinfo["send_details"] = $send_details_field;
		$qinfo["field"] = $field;

		$qinfo["category_filter_by"] = $category_filter_by;
		$qinfo["category_filter_value"] = $category_filter_value;
		$qinfo["category_filter_extra"] = $category_filter_extra;
		$qinfo["page_categories_by"] = $page_categories_by;

		$qinfo["inventory_orderby"] = $inventory_orderby;

		$qinfo['forward_to'] = "index.php?mode={$section}_{$mode}";

		$form_name = "ing";
		$field_code = create_dimensional_form($qfields,$qinfo,$cfields);
		$display .= "<form name='ing' method='post' action='' onsubmit='return checkPriceInformation(this, \"title\")'>";
		$display .= "<div class='menu_price_validation' style='color:#ff0000;'></div><br>&nbsp;";
		$display .= "&nbsp;<input type='submit' value='".speak("Save")."' class='saveBtn'><br>&nbsp;";
		$display .= "<input type='hidden' name='posted' value='1'>";
		$display .= $field_code;
		$display .= "&nbsp;<input type='submit' value='".speak("Save")."' class='saveBtn'><br>&nbsp;";
		$display .= "</form>";
		$display .= "<script>";
		$display .="
				filterOptions();
				function filterOptions() {
					$('select:has(option:contains(Select Order Type))').on('click, change, focus',function(){
						var selectParent = $(this).closest('tbody').parent().closest('tbody');
						var selectElements = selectParent.find('select:has(option:contains(Select Order Type))');
						var numberOfElements = selectElements.length;
						var i;
						selectParent.find('select:has(option:contains(Select Order Type)) option').attr('disabled', false);
						for (i=0;i < numberOfElements;i++) {
							if($(selectElements[i]).val() !== '') {
								selectParent.find('select:has(option:contains(Select Order Type)) option[value=\"'+$(selectElements[i]).val()+'\"]').attr('disabled', true);
							}
						}
						selectParent.find('select:has(option:contains(Select Order Type)) option:selected').attr('disabled', false);
					});
				}
				";
		$display .= "</script>";

		require_once(resource_path()."/dimensional_form_js.php");
		echo create_info_layer(600,480);
		echo $display;

		if ($menu_updated) {
			require_once('/home/poslavu/public_html/admin/sa_cp/manage_chain_functions.php');
			syncChainDatabases($data_name);
		}
	}
