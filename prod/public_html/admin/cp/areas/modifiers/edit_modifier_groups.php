<?php
	if($in_lavu)
	{
		$form_posted = (isset($_POST['posted']));

		$menu_id = $location_info['menu_id'];

		$display = "";
		$field = "force_modifiers";

		$qtitle_category = "Group";
		$qtitle_item = "List";

		$category_tablename = "forced_modifier_groups";
		$inventory_tablename = "forced_modifier_lists";
		$category_fieldname = "include_lists";
		$inventory_fieldname = "id";
		$inventory_select_condition = "";
		$inventory_primefield = "title";
		$category_primefield = "title";

		$category_filter_by = "menu_id";
		$category_filter_value = $menu_id;

		$select_options = array("");
		$get_lists = lavu_query("SELECT * FROM `forced_modifier_lists` where `_deleted`!='1' AND (`menu_id` = '[1]' OR `menu_id` = '0') ORDER BY `title` ASC", $menu_id);
		if (mysqli_num_rows($get_lists) > 0) {
			while ($extract = mysqli_fetch_array($get_lists)) {
				$select_options[] = array($extract['title'], $extract['id']);
			}
		}

		$qfields = array();
		$qfields[] = array("title","title","Title","select",$select_options);
		$qfields[] = array("id");
		$qfields[] = array("delete");
		require_once(resource_path() . "/dimensional_form.php");
		$qdbfields = get_qdbfields($qfields);

		if($form_posted)
		{
			$cat_count = 1;
			while(isset($_POST["ic_".$cat_count."_category"]))
			{
				$category_name = $_POST['ic_'.$cat_count.'_category'];
				$category_id = $_POST['ic_'.$cat_count.'_categoryid'];
				$category_delete = $_POST['ic_'.$cat_count.'_delete_0'];
				if($category_name==$qtitle_category . " Name") $category_name = "";

				if($category_delete=="1")
				{
					delete_poslavu_dbrow($category_tablename, $category_id);
				}

				if($category_name!="")
				{
					$dbfields = array();
					$dbfields[] = array("title",$category_name);
					$dbfields[] = array("menu_id",$menu_id);
					//$categoryid_indb = update_poslavu_dbrow($category_tablename,$dbfields, $category_id);
					if($category_fieldname=="title")
						$categoryid_indb = $category_name;
					$item_count = 1;
					$save_include_lists = "";
					while(isset($_POST["ic_".$cat_count."_".$inventory_primefield."_".$item_count]))
					{
						$del_item = $_POST["ic_".$cat_count."_delete_".$item_count];
						if($del_item!="1")
						{
							$saveval = $_POST["ic_".$cat_count."_".$inventory_primefield."_".$item_count];
							if($saveval!="")
							{
								if ($save_include_lists=="") {
									$save_include_lists = $saveval;
								} else {
									$save_include_lists .= "|".$saveval;
								}
							}
						}
						$item_count++;
					}
					$dbfields[] = array("include_lists", $save_include_lists);
					update_poslavu_dbrow($category_tablename,$dbfields, $category_id);
					//echo "<br><Br>" . $category_tablename . "<br>";
					//for($x=0; $x<count($dbfields); $x++) echo $dbfields[$x][0] . " - " . $dbfields[$x][1] . ", ";
					//echo "<br>" . $category_id;
				}
				$cat_count++;
			}
		}

		$details_column = "";
		$send_details_field = "";

		$qinfo = array();
		$qinfo["qtitle_category"] = $qtitle_category;
		$qinfo["qtitle_item"] = $qtitle_item;
		$qinfo["category_tablename"] = $category_tablename;
		$qinfo["inventory_tablename"] = $inventory_tablename;
		$qinfo["category_fieldname"] = $category_fieldname;
		$qinfo["inventory_fieldname"] = $inventory_fieldname;
		$qinfo["inventory_select_condition"] = $inventory_select_condition;
		$qinfo["inventory_primefield"] = $inventory_primefield;
		$qinfo["category_primefield"] = $category_primefield;
		$qinfo["details_column"] = $details_column;
		$qinfo["send_details"] = $send_details_field;
		$qinfo["field"] = $field;

		$qinfo["datasource"] = "delimited by |";

		$qinfo["category_filter_by"] = $category_filter_by;
		$qinfo["category_filter_value"] = $category_filter_value;

		$qinfo["qformname"] = "ing";
		$field_code = create_dimensional_form($qfields,$qinfo);
		$display .= "<form name='ing' method='post' action=''>";
		$display .= "&nbsp;<input type='submit' value='".speak("Save")."' class='saveBtn'><br>&nbsp;";
		$display .= "<input type='hidden' name='posted' value='1'>";
		$display .= $field_code;
		$display .= "&nbsp;<input type='submit' value='".speak("Save")."' class='saveBtn'><br>&nbsp;";
		$display .= "</form>";
		echo $display;
	}
?>
