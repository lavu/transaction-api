<?php
	if($in_lavu)
	{
		$form_posted = (isset($_POST['posted']));

		$menu_id = $location_info['menu_id'];

		$display = "";
		$field = "modifiers";

		$qtitle_category = "Mod List";
		$qtitle_item = "Item";

		$category_tablename = "modifier_categories";
		$inventory_tablename = "modifiers";
		$category_fieldname = "id";
		$inventory_fieldname = "category";
		$inventory_select_condition = "";
		$inventory_primefield = "title";
		$category_primefield = "title";

		$category_filter_by = "menu_id";
		$category_filter_value = $menu_id;

		$inventory_orderby = "_order, title";

		if (!isset($data_name))
			$data_name = admin_info("dataname");
		require_once(dirname(dirname(dirname(dirname(__FILE__)))).'/sa_cp/manage_chain_functions.php');
		$a_chain_groups = array();
		$a_chain_items = array();
		$b_part_of_chain = getReportingGroupOptionsForDimensionalForm($data_name, $a_chain_groups, $a_chain_items);
		$menu_updated = FALSE;
		
		$productTaxFlag = false;
		if ($location_info['display_product_code_and_tax_code_in_menu_screen']) {
			$productTaxFlag = true;
			$nameWidth = array("width:220px");
			$priceWidth = array("width:80px");
			$productCodeWidth = array("width:100px");
			$taxCodeWidth = array("width:100px");
		}
		$happy_query = lavu_query("select * from `happyhours` where `_deleted`!='1'");
		$happy_list[] = array('Select Happy Hours','0');
		while($happy_read = mysqli_fetch_assoc($happy_query))
		{
			$happy_list[] = array($happy_read['name'],$happy_read['id']);
		}
		$cfields = array();
		// if ($b_part_of_chain && DEV)
		// 	$cfields[] = array("chain_reporting_group", "chain_reporting_group", speak("Chain Reporting Group"), "select", $a_chain_groups);
		$cfields[] = array("happyhours_id","happyhours_id",speak("happy hours"),"select",$happy_list);
		$qfields = array();
		$qfields[] = array("title","title",speak("Modifier"),"input", $nameWidth);
		$qfields[] = array("cost","cost",speak("Price"),"input", $priceWidth);
		
		// if ($b_part_of_chain && DEV)
			// $qfields[] = array("chain_reporting_group", "chain_reporting_group", speak("Chain Reporting Group"), "select", $a_chain_items);
		
		if ($productTaxFlag) {
			$qfields[] = array("product_code","product_code",speak("Product Code"),"input", $productCodeWidth);
			$qfields[] = array("tax_code","tax_code",speak("Tax Code"),"input", $taxCodeWidth);
		}
		
		$qfields[] = array("ingredients","ingredients","Ingredients","special");
		$qfields[] = array("nutrition","nutrition",speak("Nutrition"),"special");//LP-1411
		$qfields[] = array("happyhours_id","happyhours_id",speak("Happy Hours"),"select", $happy_list);
		$qfields[] = array("id");
		$qfields[] = array("delete");
		require_once(resource_path() . "/dimensional_form.php");
		$qdbfields = get_qdbfields($qfields);

		if($form_posted)
		{
			$menu_updated = TRUE;
			$cat_count = 1;
			$cacheModifiers = array();
			while(isset($_POST["ic_".$cat_count."_category"]))
			{
				$category_name = isset($_POST['ic_'.$cat_count.'_category']) ? trim($_POST['ic_'.$cat_count.'_category']) : "";
				$category_id = isset($_POST['ic_'.$cat_count.'_categoryid']) ? trim($_POST['ic_'.$cat_count.'_categoryid']) : "";
				$category_delete = isset($_POST['ic_'.$cat_count.'_delete_0']) ? trim($_POST['ic_'.$cat_count.'_delete_0']) : "" ;
				
				$category_chain_reporting_group = isset($_POST['ic_'.$cat_count.'_col_chain_reporting_group']) ? trim($_POST['ic_'.$cat_count.'_col_chain_reporting_group']): "";
				
				if($category_name==$qtitle_category . " Name") $category_name = "";

				if($category_delete=="1")
				{
					delete_poslavu_dbrow($category_tablename, $category_id);
				}

				if($category_name!="")
				{
					$dbfields = array();
					$dbfields[] = array("title",$category_name);
					$dbfields[] = array("menu_id",$menu_id);
					for($n=0; $n<count($cfields); $n++)
					{
						$cfieldname = $cfields[$n][0];
						$dbfields[] = array($cfieldname,$_POST['ic_'.$cat_count.'_col_'.$cfieldname]);
					}
					$categoryid_indb = update_poslavu_dbrow($category_tablename,$dbfields, $category_id);
					if($category_fieldname=="title")
						$categoryid_indb = $category_name;
					$item_count = 1;
					while(isset($_POST["ic_".$cat_count."_".$inventory_primefield."_".$item_count]))
					{
						$item_value = array(); // get posted values
						for($n=0; $n<count($qfields); $n++)
						{
							$qname = $qfields[$n][0];
							$qtitle = (isset($qfields[$n][2]))?$qfields[$n][2]:false;
							$set_item_value = $_POST['ic_'.$cat_count.'_'.$qname.'_'.$item_count];
							if($qtitle && $set_item_value==$qtitle) $set_item_value = "";
							if($qname=="cost")
							{
								$set_item_value = formatValueForMonetaryDisplay( $set_item_value );
								if ($set_item_value == '') {
								$set_item_value = 0;	
								}
							}
							$item_value[$qname] = $set_item_value;
						}
						$item_id = $item_value['id'];
						$item_delete = $item_value['delete'];
						$item_name = $item_value[$inventory_primefield];

						if($item_delete=="1")
						{
							delete_poslavu_dbrow($inventory_tablename, $item_id);
							//If the item is set to be deleted, delete it from the 86count table.
							delete_poslavu_86count($inventory_tablename, $item_id);
						}
						else if($item_name!="")
						{
							$dbfields = array();
							$dbfields[] = array($inventory_fieldname,$categoryid_indb);
							//Make array to set modifiers into cache
							$cacheModifiers[$item_id]['id'] = $item_id;
							$cacheModifiers[$item_id]['title'] = $item_value['title'];
							for($n=0; $n<count($qdbfields); $n++)
							{
								$qname = $qdbfields[$n][0];
								$qfield = (isset($qdbfields[$n][1]))?$qdbfields[$n][1]:$qname;
								$dbfields[] = array($qfield,$item_value[$qname]);
							}
							$orderby_tag = 'icrow_'.$cat_count.'_'.$item_count.'_order';
							if(isset($_POST[$orderby_tag]))
							{
								$dbfields[] = array("_order",$_POST[$orderby_tag]);
							}
							update_poslavu_dbrow($inventory_tablename,$dbfields, $item_id);
						}
						$item_count++;
					}
				}
				$cat_count++;
			}

			//Storing modifiers id and title value into cache
			setCacheModifiers($cacheModifiers);
			// LOCAL SYNC RECORD
			schedule_remote_to_local_sync_if_lls($location_info, $locationid, "table updated", "modifiers"); // modifier_categories not needed in lls db
			// END LOCAL SYNC RECORD

			$check_for_last_update = lavu_query("SELECT `id` FROM `config` WHERE `setting` = 'menu_last_updated' AND `location` = '[1]'", $locationid);
			if (@mysqli_num_rows($check_for_last_update)) {
				$uinfo = mysqli_fetch_assoc($check_for_last_update);
				lavu_query("UPDATE `config` SET `value` = now() WHERE `id` = '[1]'", $uinfo['id']);
			} else lavu_query("INSERT INTO `config` (`location`, `type`, `setting`, `value`) VALUES ('[1]', 'admin_activity', 'menu_last_updated', now())", $locationid);

			lavu_query("UPDATE `devices` SET `needs_reload` = '1' WHERE `loc_id` = '[1]'", $locationid);
		}

		$details_column = "";//"Zip Codes";
		$send_details_field = "";//"zip_codes";

		$qinfo = array();
		$qinfo["qtitle_category"] = $qtitle_category;
		$qinfo["qtitle_item"] = $qtitle_item;
		$qinfo["category_tablename"] = $category_tablename;
		$qinfo["inventory_tablename"] = $inventory_tablename;
		$qinfo["category_fieldname"] = $category_fieldname;
		$qinfo["inventory_fieldname"] = $inventory_fieldname;
		$qinfo["inventory_select_condition"] = $inventory_select_condition;
		$qinfo["inventory_primefield"] = $inventory_primefield;
		$qinfo["category_primefield"] = $category_primefield;
		$qinfo["details_column"] = $details_column;
		$qinfo["send_details"] = $send_details_field;
		$qinfo["field"] = $field;

		$qinfo["category_filter_by"] = $category_filter_by;
		$qinfo["category_filter_value"] = $category_filter_value;

		$qinfo["inventory_orderby"] = $inventory_orderby;

		$form_name = "ing";
		$field_code = create_dimensional_form($qfields,$qinfo,$cfields);
		$display .= "<form name='ing' method='post' action='' onsubmit='return checkPriceInformation(this, \"title\")'>";
		$display .= "<div class='menu_price_validation' style='color:#ff0000;'></div><br>&nbsp;";
		$display .= "&nbsp;<input type='submit' value='".speak("Save")."' class='saveBtn'><br>&nbsp;";
		$display .= "<input type='hidden' name='posted' value='1'>";
		$display .= $field_code;
		$display .= "&nbsp;<input type='submit' value='".speak("Save")."' class='saveBtn'><br>&nbsp;";
		$display .= "</form>";

		require_once(resource_path()."/dimensional_form_js.php");
		echo create_info_layer(600,480);
		echo $display;

		if ($menu_updated) {
			require_once('/home/poslavu/public_html/admin/sa_cp/manage_chain_functions.php');
			syncChainDatabases($data_name);
		}
	}
?>
