<?php

ini_set("display_errors","1");
if(!$in_lavu){return;}
$tablename = "locations";
$rowid = $locationid;
$lavu_only = "<font color='#588604' style='font-size:14px'><b>*</b></font> ";
$fields = array();

$fields[] = array(speak("Shift Notification Settings"),"Shift Notification Settings","seperator");
$fields[] = array(speak("Send Mail Notification For Shift Trades").":","config:shift_change_mail_notifications","bool");

$managerApprovalChoices = array();
$managerApprovalChoices['0-0'] = "No";
$managerApprovalChoices['2-8'] = "Requires a Manager";
$managerApprovalChoices['3-8'] = "Requires a Level 3 Manager";
//$fields[] = array(speak("Require manager's approval to trade shifts").":","config:shift_change_manager_approval","select",$managerApprovalChoices);

$emailListChoices = array();
$emailListChoices['0-0'] = "Nobody";
$emailListChoices['1-8'] = "Everyone";
$emailListChoices['2-8'] = "Managers";
$emailListChoices['1-1'] = "Level 1 Employees Only";
$emailListChoices['2-2'] = "Level 2 Managers Only";
$emailListChoices['3-3'] = "Level 3 Managers Only";
$fields[] = array(speak("When requesting a shift trade, send email to").":","config:shift_trading_email_list","select",$emailListChoices);
$fields[] = array(speak("When accepting a shift trade, send email to").":","config:shift_accept_email_list","select",$emailListChoices);
$fields[] = array(speak("When revoking a shift trade, send email to").":","config:shift_revoke_email_list","select",$emailListChoices);
//$fields[] = array(speak("When approving a shift trade, send email to").":","config:shift_approve_email_list","select",$emailListChoices);

$fields[] = array(speak("Save"),"submit","submit");

require_once(resource_path() . "/form.php");

if (is_lavu_admin()){
	echo "<br>".$lavu_only."Indicates items visible only to Lavu Admin users (i.e., developers and support staff)";
}

if(isset($_GET['show_connector'])){
	echo "<br><br>Location Id: " . $locationid;
	echo "<br>Connector Name: " . admin_info("dataname");
	echo "<br>Connector Key: " . lsecurity_key(admin_info("companyid"));
}
echo "<br><br>";