<?php
	function get_cfg_setting($setting,$def="")
	{
		global $locationid;
		$setting_query = lavu_query("SELECT `value` FROM `config` WHERE `location`='[1]' AND `type`='[2]'",$locationid, $setting);
		if(mysqli_num_rows($setting_query))
		{
			$setting_read = mysqli_fetch_assoc($setting_query);
			return $setting_read['value'];
		}
		else return $def;
	}
	function set_cfg_setting($setting,$value="")
	{
		global $locationid;
		$setting_query = lavu_query("SELECT `id` FROM `config` WHERE `location`='[1]' AND `type`='[2]'",$locationid, $setting);
		if(mysqli_num_rows($setting_query))
		{
			$setting_read = mysqli_fetch_assoc($setting_query);
			lavu_query("update `config` set `value`='[1]' where `id`='[2]'",$value,$setting_read['id']);
		}
		else
		{
			lavu_query("insert into `config` (`location`,`type`,`value`) values ('[1]','[2]','[3]')",$locationid,$setting,$value);
		}
	}
	
	function get_fr_time_info($n,$type)
	{
		if($type=="display")
		{
			if($n < 12) $show_time = $n .":00am";
			else if($n==12) $show_time = $n .":00pm";
			else if($n > 12 && $n < 24) $show_time = ($n - 12) . ":00pm";
			else if($n==24) $show_time = ($n - 12) . ":00am";
			else if($n > 24) $show_time = ($n - 24) . ":00am";
			else $show_time = "";
			
			return $show_time;
		}
		else if($type=="base")
		{
			if($n >= 24) return ($n - 24);
			else return $n;
		}
		else if($type=="import")
		{
			if($n <= 4) return ($n + 24);
			else return $n;
		}
	}
	
	if(isset($_POST['posted']))
	{
		$flash_report_emails = $_POST['flash_report_emails'];
		set_cfg_setting("flash report emails",$flash_report_emails);
		
		$timelist = "";
		for($n=6; $n<=26; $n++)
		{
			if(isset($_POST['flash_report_time_'.$n]))
				$time_checked = true;
			else
				$time_checked = false;
			if($time_checked) $timelist .= "(" . get_fr_time_info($n,"base") . ")";
		}
		set_cfg_setting("flash report times",$timelist);
		
		$set_send_flash_reports = $_POST['send_flash_reports'];
		set_cfg_setting("send flash reports",$set_send_flash_reports);
	}
	
	$send_flash_reports = get_cfg_setting("send flash reports");
	$email_list = get_cfg_setting("flash report emails");
	$flash_times = get_cfg_setting("flash report times");
	
	echo "<form name='nform' method='post' action='index.php?mode=notifications'>";
	echo "<br><br>";
	echo "Flash report Email Addresses:<br><textarea name='flash_report_emails' rows='3' cols='48'>$email_list</textarea>";
	echo "<br>";
	echo "<br>Send Flash report Emails At:";
	echo "<table style='border:solid 1px #555555'><tr><td valign='top'>";
	echo "<table>";
	for($n=6; $n<=26; $n++)
	{
		$show_time = get_fr_time_info($n,"display");
		echo "<tr><td align='right'>";
		echo "<input type='checkbox' name='flash_report_time_$n'";
		if(strpos($flash_times,"(".get_fr_time_info($n,"base").")")!==false) echo " checked";
		echo ">";
		echo "</td><td>";
		echo $show_time;
		echo "</td></tr>";
		
		if($n==12 || $n==19)
		{
			echo "</table></td><td>&nbsp;&nbsp;</td><td valign='top'><table>";
		}
	}
	echo "</table>";
	echo "</table>";
	
	echo "<br>";
	echo "Flash email reports: ";
	echo "<select name='send_flash_reports'>";
	echo "<option value='Off'>Off</option>";
	echo "<option value='On'";
	if(strtolower($send_flash_reports)=="on")
		echo " selected";
	echo ">On</option>";
	echo "</select>";
	
	echo "<br><br>";
	echo "<input type='hidden' name='posted' value='1'>";
	echo "<input type='button' value='Submit' onclick='document.nform.submit()'>";
	echo "</form>";
	//echo "<br><br>Notifications: coming soon...<br><br>This is where you will be able to configure settings for a regularly emailed \"flash\" report<br>";
?>
