<?php
if (isset($_GET['dcd_status'])) {
    $brands = array();
    $dcdStatus = $_GET['dcd_status'];
    if ($dcdStatus) {
        $brands = getPrinterKdsBrands($dcdStatus);
    }
    else {
        $brands = getPrinterKdsBrands($dcdStatus);
    }
    $printView = preparePrintTypeView($brands, $_GET['rowid'], $_GET['dataname']);
    echo $printView;
}

if (!$in_lavu)
{
	exit();
}

echo "<br><br>";

$forward_to	= "print_wrap.php?pw_mode=".$pw_mode;
$tablename	= "config";
$filter_by	= "`location` = '".$locationid."' AND `type` = 'printer'";
$order_by	= "setting";
$lavu_only	= "<font color='#588604'><b>*</b></font> ";

$printer_settings = array(
	'receipt' => "receipt",
	'kitchen' => "kitchen"
);
for ($i = 2; $i <= 50; $i++)
{
	$printer_settings['receipt'.$i] = "receipt".$i;
}
for ($i = 2; $i <= 50; $i++)
{
	$printer_settings['kitchen'.$i] = "kitchen".$i;
}

$brands = array();
//$command_sets = array(""=>"");
$dcdStatus = 0;
$rowid = $_REQUEST['rowid'];
$data_name = sessvar('admin_dataname');
if (isset($rowid) && $rowid != ''){
    $dcd_query = lavu_query("select value17 from config where id ='".$rowid."'");
    $dcdInfo = mysqli_fetch_assoc($dcd_query);
    $dcdStatus = $dcdInfo['value17'];
}
$brands = getPrinterKdsBrands($dcdStatus);

function getPrinterKdsBrands($dcdStatus) {
    $brands = array();
    $supported_brands = array();
    require_once(dirname(__DIR__)."/resources/core_functions.php");
    $where_condition = "drawer_code_2 ='' and";
    if ($dcdStatus) {
        $where_condition = "drawer_code_2 !='' and";
    }
    $pr_query = mlavu_query("SELECT * FROM `poslavu_MAIN_db`.`supported_printers` WHERE ".$where_condition." `_deleted` != '1' ORDER BY `_order` asc");
    while ($pr_read = mysqli_fetch_assoc($pr_query))
    {
        $brand	= $pr_read['brand'];
    	
    	$prid	= $pr_read['id'];
    	$prname = $pr_read['name'];
    
    	$brands[$brand]['Command Set']['options'][$prid]       = $prname ;
    	$brands[$brand]['Command Set']['save_type']            = "single_val";
    	$brands[$brand]['Command Set']['column']               = "value6";
    	$supported_brands[] = $brand;
    	//$command_sets[$prid] = $prname;
    }
   
    $printerSeries = array();
    $models_query = mlavu_query("SELECT * FROM `poslavu_MAIN_db`.`printer_models`");
    while ($models_res = mysqli_fetch_assoc($models_query))
    {
        $brand = $models_res['brand'];

        if (in_array($brand, $supported_brands) || !$dcdStatus) {
        	$model = $models_res['model'];
        	
        	$brands[$brand]['Model']['options'][$model]        = $model;
        	$brands[$brand]['Model']['save_type']              = "json";
        	$brands[$brand]['Model']['column']                 = "value2_long";
        	$brands[$brand]['Model']['hidden_vals']            = array( 'language' => "ANK", 'brand' => $brand );
        	
        	$printerSeries[$model] = $models_res['printer_series'];
        }
    }
    
    $brands['Epson']['Lavu Controller pair mode']['options']['1']   = "Auto";
    $brands['Epson']['Lavu Controller pair mode']['options']['0']   = "Manual";
    $brands['Epson']['Lavu Controller pair mode']['save_type']      = "combined_json";
    $brands['Epson']['Lavu Controller pair mode']['json_key']       = "pair_mode";
    $brands['Epson']['Lavu Controller pair mode']['column']         = "value2_long";
    
    
    $brands['Epson']['Epson KDS'] = array(
        'column'	=> "value2_long",
        'json_key'	=> "epson_kds",
        'save_type'	=> "combined_json"
    );
    
    if ($dcdStatus == 0) {
        $brands['Star']['Post-print pause'] = array(
            'column'	=> "value2_long",
            'json_key'	=> "post_print_pause",
            'options'	=> array(
                '0'		=> "0",
                '0.05'	=> "0.05",
                '0.10'	=> "0.10",
                '0.15'	=> "0.15",
                '0.20'	=> "0.20",
                '0.25'	=> "0.25",
            ),
            'save_type'	=> "combined_json"
        );
    }
	
    return $brands;
}

$image_capabilities = array(
	'0' => speak("None"),
	'1' => speak("Bit Graphics")." (".speak("High Density").") (SP700)",
	//'2' => speak("Bit Graphics")." (".speak("Fine Density").")." (TSP650)",
	'3' => speak("Raster Graphics")." (TSP650)",
	'4' => "Epson ".speak("Graphics"),
);

$fields = array(
	array(
		speak("Setting").":",
		"setting",
		"select",
		$printer_settings,
		"list:yes"),
	array(
		speak("Name").":",
		"value2",
		"text",
		"",
		"list:yes"
	),
	array(
		speak("IP Address").":",
		"value3",
		"text",
		"",
		"list:yes"
	),
	array(
		speak("Port").":",
		"value5",
		"text",
		"",
		"list:yes"
	),
	array(
		speak("Enable Dual Cash Drawer").":",
		"value17",
		"bool"
	),
	array(
		speak("Printer Type").":",
		"value4",
		"printers_dynamic_select",
		$brands,
		"list:yes",
		"label_cell_id:printer_type_label"
	),
	array(
		speak("Characters Per Line").":",
		"value7",
		"text",
		"size:20",
		""
	),
	array(
		speak("Characters Per Line")." (".speak("large font")."):",
		"value8",
		"text",
		"size:20",
		""
	),
	array(
		speak("Line Spacing").":",
		"value9",
		"text",
		"size:20",
		""
	),
	array(
		speak("Image Capability").":",
		"value10",
		"select",
		$image_capabilities
	)
);

if (is_lavu_admin())
{
	$fields[] = array(
		$lavu_only.speak("Skip IP Address Ping").":",
		"value13",
		"bool"
	);
}

$fields[] = array(
	speak("Perform status check")." (".speak("Epson or Star only")."):",
	"value11",
	"bool",
	"1"
);

$access_interwrite = (isset($_SESSION['access_interwrite']))?$_SESSION['access_interwrite']:"no";

if (is_lavu_admin() || $access_interwrite=="yes")
{
	$fields = array_merge( $fields, array(
		array(
			$lavu_only."Use ETB flag:",
			"value12",
			"bool"
		),
		array(
			$lavu_only."Inter-write pause:",
			"value14",
			"select",
			"0.25,0.5,0.75,1,1.25,1.5,1.75,2,2.25,2.5,2.75,3"
		)
	));
}

$fields = array_merge( $fields, array(
	array(
		speak("Print each item on it's own individual ticket").":",
		"value_long",
		"bool"
	),
	array(
		speak("Provide a Grouping Label")." (".speak("for reports")."):",
		"value",
		"text",
		"size:20"
	),
	array(
		speak("Number of times to sound beeper when sending kitchen tickets").":",
		"value15",
		"select",
		array(
			'0' => speak("Disabled"),
			'1' => "1",
			'2' => "2",
			'3' => "3"
		)
	),
	array(
		speak("If an order contains an item(s) directed to this printer and a separate printer, then print all items to this printer").":",
		"value16",
		"select",
		array(
			'0' => speak("No"),
			'1' => speak("Yes")
		)
	),
	array(
		"Location",
		"location",
		"hidden",
		"",
		"setvalue:".$locationid
	),
	array(
		"Type",
		"type",
		"hidden",
		"",
		"setvalue:printer"
	),
	array(
		speak("Save"),
		"submit",
		"printer_submit"
	)
));

//Inserted by Brian D.
//Checking for post-back.  If is lls, we now need to sync down the config table.
if (!empty($_POST['posted']) && !empty($_POST['setting']) && !empty($_POST['posted_dataname']))
{
	$locinfo = sessvar('location_info');
	schedule_remote_to_local_sync_if_lls($locinfo, $locationid,  "table updated", "config", $sync_value_long="");
}

function run_post_action()
{
	global $locationid;

	lavu_query("DELETE FROM `config` WHERE `location` = '".$locationid."' AND `type`=''");
	$pr_query = lavu_query("SELECT * FROM `config` WHERE `location` ='".$locationid."' AND `type` = 'printer' AND `_deleted` != '1'");
	while ($pr_read = mysqli_fetch_assoc($pr_query))
	{
		$vars = array(
			'location'	=> $locationid,
			'type'		=> "",
			'setting'	=> $pr_read['setting'],
			'value'		=> $pr_read['value2']
		);

		lavu_query("INSERT INTO `config` (`location`, `type`, `setting`, `value`) VALUES ('[location]', '[type]', '[setting]', '[value]')", $vars);
	}

	update_api_access_log(admin_info("dataname"), $locationid);
}

//LP-6094
function preparePrintTypeView($props, $rowid, $data_name){
    $dataname = "poslavu_".$data_name."_db";
    $selections = mysqli_fetch_assoc(mlavu_query("SELECT * FROM $dataname.`config` WHERE `id` = '[1]'", $rowid));
    $long = (!empty($selections['value_long2']))?lavu_json_decode($selections['value_long2']):array();
    if (!isset($long['pair_mode']))
    {
        $long['pair_mode'] = "1";
    }
    
    if (!isset($long['post_print_pause']))
    {
        $long['post_print_pause'] = "0";
    }
    
    $outer_select = "<select class='outer_select' id='printertype' name='printer_type' onchange='show_inner_select(this);change_character_image_setting();' ><option value=''> </option>";
    
    $ds_adjust_tables = array();
    $ds_adjust_cells = array();
    
    $tableCnt=0;
    foreach ($props as $opt => $inner)
    {
        $ds_adjust_tables[] = $opt."_table";
        $outer_select .= "<option value='".$opt."'".(($opt == $long['brand'])?" selected='true'":"").">".$opt."</option>";
        $inner_select = "<table id='".$opt."_table_".$tableCnt."' style='border:0;margin-left: -290px;'>";
        $tableCnt++;
        
        foreach ($inner as $inner_key => $inner_select_opts)
        {
            if ($inner_select_opts['hidden_vals'])
            {
                $encoded = lavu_json_decode($inner_select_opts['hidden_vals']);
                $hidden_vals = "<input type='hidden' name='hidden_vals' value='".$encoded."'>";
                $inner_select .= $hidden_vals;
            }
            
            $submit_name = str_replace(" ", "_", strtolower($inner_key));
            $ds_adjust_cells[] = $opt."_".$submit_name."_cell";
            
            $lcpm = ($inner_key == "Lavu Controller pair mode");
            $pp_p = ($inner_key == "Post-print pause");
            $ekds = ($inner_key == "Epson KDS");
            
            $insel_row_display = ($lcpm && (!strstr($track_values['setting'], "receipt") || $track_values[Epson_model]!="TM-T88V-i"))?"none":"block";
            $insel_onchange = ($opt=="Epson" && $inner_key=="Model")?" onchange = 'epsonModelDidChange(this);'":"";
            $insel_onchange_model = ($opt!="Epson" && $inner_key=="Model")?" onchange = 'set_model_select(this)'":"";
            
            if($submit_name == 'epson_kds') {
                $checked = ($long['epson_kds'] == 1) ? " checked" : "";
                if($long['epson_kds'] == 1) {
                    $is_epson_kds_checked = 1;
                }
                $inner_select .=  "<tr style='display:block;'><td id='".$opt."_".$submit_name."_cell' style='width:283px; border-top:2px solid white; text-align:right;'>".$inner_key."</td><td><input type='checkbox' id='".$submit_name."' name='".$submit_name."' value='1' ".$checked." onchange='change_character_image_setting()'></td></tr>";
            }
            else {
                if ($submit_name == 'command_set') {
                	$inner_select.= "<tr".($lcpm?" id='lcpm_row'":"")." style='display:".$insel_row_display.";'><td id='".$opt."_".$submit_name."_cell' style='width:283px; border-top:2px solid white; text-align:right;'>".$inner_key.":</div></td><td><select ".$insel_onchange." name='".$submit_name."' class='inner_select' id='printer_command_$opt' onchange='set_command_select(this)' save_type='".$inner_select_opts['save_type']."' col='".$inner_select_opts['column']."'>";
                }
                else {
                	$inner_select.= "<tr".($lcpm?" id='lcpm_row'":"")." style='display:".$insel_row_display.";'><td id='".$opt."_".$submit_name."_cell' style='width:283px; border-top:2px solid white; text-align:right;'>".$inner_key.":</div></td><td><select ".$insel_onchange." ".$insel_onchange_model." id='".$submit_name."_".$opt."' name='".$submit_name."' class='inner_select' save_type='".$inner_select_opts['save_type']."' col='".$inner_select_opts['column']."'>";
                }
            }
          
            $found = 0;
            if (!$lcpm && !$pp_p && !$ekds) $inner_select .= "<option></option>";
            foreach ($inner_select_opts['options'] as $inner_key_opts => $inner_opts_val)
            {
                $selected = "";
                if ($selections['value6']==$inner_key_opts || $inner_opts_val==$long['model'] || ($lcpm && $inner_key_opts==$long['pair_mode']) || ($pp_p && $inner_key_opts==$long['post_print_pause']))
                {
                    $selected = " selected='true'";
                    $track_values[$opt."_".$submit_name] = $inner_opts_val;
                    $found = 1;
                }
                $inner_select .= "<option ".(($submit_name == 'command_set')?" name='printer'":"")." value ='$inner_key_opts'".$selected.">".$inner_opts_val."</option>";
            }
            $inner_select .= "</select></td></tr>";
        }
        $inner_select .= "</table>";
        
        $div_container .= "<div class='".$opt." inner_select_container' style='display:".(($opt == $long['brand'])?"block":"none").";'>".$inner_select."</div>";
    }
    $outer_select .= "</select>";
    $multi_container_div = "<div class ='multi_select'>".$outer_select.$div_container."</div>";
    return $multi_container_div;
}

$form_max_width = 696;
require(resource_path()."/browse.php");

?>

<script type= 'text/javascript'>
	document.getElementById('setting').addEventListener('change',function(){ showOnKitchenOnly()},false);
	document.getElementById('value17').parentNode.children[2].setAttribute('id','dual-cash-register');
	document.getElementById('dual-cash-register').parentNode.children[2].addEventListener('change',function(){ showOnRecieptOnly()});
	showOnRecieptOnly();
	showOnKitchenOnly();

	function showOnKitchenOnly()
	{
		var settingname = document.getElementById('setting').value;
		if( settingname && settingname.includes('kitchen'))
		{
			document.querySelector('#_setting_tbl_id select[name="value16"]').parentNode.parentNode.style.display = 'table-row';
		}else
		{
			document.querySelector('#_setting_tbl_id select[name="value16"]').value = '0';
			document.querySelector('#_setting_tbl_id select[name="value16"]').parentNode.parentNode.style.display = 'none';
		}
		showOnRecieptOnly();
	}
	function showOnRecieptOnly()
	{
		var settingname = document.getElementById('setting').value;
		if( settingname && settingname.includes('receipt'))
		{
			document.querySelector('#_setting_tbl_id [name="value17"]').parentNode.parentNode.style.display = 'table-row';
		}else
		{
			document.querySelector('#_setting_tbl_id [name="value17"]').value = '0';
			document.querySelector('#_setting_tbl_id [name="value17"]').parentNode.parentNode.style.display = 'none';
		}
	}
	window.onload = function()
	{
		document.getElementById('zenbox_tab').style.display='none';
	}

	var a = document.getElementsByClassName('content')[0];
	document.getElementsByTagName('body')[0].innerHTML = a.innerHTML;
	document.getElementById('main_content_area').style.width = '600px';
	document.getElementsByTagName("body")[0].style.width = '600px';
	document.getElementsByTagName("body")[0].style.backgroundColor = 'white';
	document.getElementsByTagName("body")[0].style.overflowX = 'hidden';

	function adjustDynamicSelectElements(guide_element, tables, cells)
	{
		var guide_width = document.getElementById(guide_element).offsetWidth;
		var table_target_margin = (0 - guide_width - 7);
		var cell_target_width = (guide_width - 2);

		var table_ids = tables.split(",");
		for (var ti = 0; ti < table_ids.length; ti++)
		{
			var table_id = table_ids[ti];
			document.getElementById(table_id).style.marginLeft = table_target_margin + "px";
		}

		var cell_ids = cells.split(",");
		for (var ci = 0; ci < cell_ids.length; ci++)
		{
			var cell_id = cell_ids[ci];
			document.getElementById(cell_id).style.width = cell_target_width + "px";
		}
	}

	function isSettingReceipt()
	{
		var setting = document.getElementById('setting').value;

		return (setting.indexOf('receipt') >= 0);
	}

	// KIOSK-273
	function epsonModelSetSeries(model)
	{
		var printerSeries = <?= json_encode($printerSeries, JSON_HEX_APOS) ?>;
		//var printerSeries = JSON.parse('<?= json_encode($printerSeries, JSON_HEX_APOS) ?>');

		var hiddenValsInput = document.getElementsByName("hidden_vals");
		if (hiddenValsInput && hiddenValsInput[0]) hiddenValsInput = hiddenValsInput[0];

		if (hiddenValsInput && printerSeries && printerSeries[model])
		{
			// Get hidden_vals JSON
			var hiddenVals = JSON.parse(hiddenValsInput.value);

			// Re-write hidden_vals JSON with series
			hiddenVals.series = printerSeries[model];
			hiddenValsInput.value = JSON.stringify(hiddenVals);
		}
	}

	function epsonModelDidChange(sel)
	{
		var set_lcpm_row_display = "none";
		if (isSettingReceipt())
		{
			if (sel.value == "TM-T88V-i")
			{
				set_lcpm_row_display = "block";
			}
		}

		var lcpm_row = document.getElementById('lcpm_row');
		if (lcpm_row)
		{
			lcpm_row.style.display = set_lcpm_row_display;
		}

		epsonModelSetSeries(sel.value);
		set_model_select(sel);
	}
	
	function set_model_select(elem) {
		var option;
		var Id= elem.id;
		var sel_value= elem.value;
		var select = document.getElementById(Id);
		for (var j=0; j<select.options.length;j++) {
			option = select.options[j];
		  if (option.value != '' && option.value == sel_value) {
			  
			  option.setAttribute('selected', true);
           }
		  else {
			  option.removeAttribute('selected', true);
		  }
		}
	}
	
	//LP-6094
	function getSelectedPrinterTypes(dcdStatus) {
        var v7 = document.form1.querySelector("input[name^='value7']");
        var v8 = document.form1.querySelector("input[name^='value8']");
        var v10 = document.form1.querySelector("select[name='value10']");
        v7.value = '';
		v8.value = '';
		v7.removeAttribute("readonly");
		v8.removeAttribute("readonly");
		v10.removeAttribute("disabled");
        
		var dcdval = (dcdStatus == true)? 1 : 0 ;
		var before_dcdval = document.querySelector('#_setting_tbl_id [name="value_before_value17"]').value;
        if (before_dcdval != dcdval) {
            document.querySelector('#_setting_tbl_id [name="value_before_value17"]').value = dcdval;
        }
        
		var url = 'areas/printers_kds.php?dcd_status='+dcdval+'&rowid='+<?php echo $rowid; ?> +'&dataname='+'<?php echo $data_name; ?>';
		var xhr = window.XMLHttpRequest ? new XMLHttpRequest() : new ActiveXObject('Microsoft.XMLHTTP');
	    xhr.open('GET', url);
	    xhr.onreadystatechange = function() {
	    if (xhr.readyState>3 && xhr.status==200) 
	    {
	        if (xhr.responseText != '') {
				document.getElementsByClassName('multi_select')[0].innerHTML = xhr.responseText;
			}
		 }
		    
	    };
	    xhr.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
	    xhr.send();
	}
</script>
