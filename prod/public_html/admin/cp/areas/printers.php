<center>
	<div id='printerKDScontainer'>
		<div class= 'divider' style='display:inline-block; width:696px; background-color:#98b624; margin:2px 0px 2px 0px; padding:10px 0px 10px 0px; color:white; font-size:12px; font-weight:bold;'> Purchase Printer & KDS Hardware </div>
		<div id='printerKDScontent'>
			<div class='cols2'>
				<img src='./images/printer.png' />
				<a href ='https://shop.lavu.com/collections/printers' target='_blank'><input type='button' value='Buy Printers & Accessories' /></a>
			</div>
			<div class='cols2'>
				<img src='./images/KDS.png' />
				<a href='https://shop.lavu.com/collections/kitchen-display-system' target='_blank'><input type='button' value='Buy KDS Hardware & Accessories' /></a>
			</div>
		</div>
	</div>
</center>

<?php

	require_once(__DIR__."/direct_printing.php");

	if (isset($_GET['access_interwrite']))
	{
		$_SESSION['access_interwrite'] = strtolower($_GET['access_interwrite']);
	}

	$dn_loc = $data_name.":".$locationid;
?>

	<div class='container_action' style='border-right:2px solid #aaaaaa; border-left:2px solid #aaaaaa; border-bottom:2px solid #aaaaaa; width:700px; padding-bottom:10px;'>

		<center>

			<div class= 'divider' style='display:inline-block; width:696px; background-color:#98b624; margin:2px 0px 2px 0px; padding:10px 0px 10px 0px; color:white; font-size:12px; font-weight:bold;'> Printers / KDS </div>
			<div style='width:700px; height:400px; overflow:hidden;' >
				<iframe style='border:0px; position:relative; width:700px; height:400px' src='print_wrap.php?pw_mode=printers_kds&dn_loc=<?php echo $dn_loc; ?>' scrolling='no' onload='iframeLoaded(this);'></iframe>
			</div>
<?php
			if ($modules->hasModule("extensions.ordering.kiosk"))
			{
?>
			<div class= 'divider' style='display:inline-block; width:696px; background-color:#98b624; margin:2px 0px 2px 0px; padding:10px 0px 10px 0px; color:white; font-size:12px; font-weight:bold;'> Kiosk Receipt Printers </div>
			<div style='width:700px; height:400px; overflow:hidden;' >
				<iframe style='border:0px; position:relative; width:700px; height:400px' src='print_wrap.php?pw_mode=kiosk_printers&dn_loc=<?php echo $dn_loc; ?>' scrolling='no' onload='iframeLoaded(this);'></iframe>
			</div>
<?php
			}
?>
			<div class= 'divider' style='display:inline-block; width:696px; background-color:#98b624; padding:10px 0px 10px 0px; color:white; margin:10px 0px 2px 0px; font-size:12px; font-weight:bold;'> Printer Groups </div>
			<div style='width:700px; height:300px; overflow:hidden;'>
				<iframe style='border:0px; position:relative; width:700px; height:300px' src='print_wrap.php?pw_mode=printer_groups&dn_loc=<?php echo $dn_loc; ?>' scrolling='no' onload='iframeLoaded(this);'></iframe>
			</div>

			<br>

			<div class= 'divider' style='display:inline-block; width:696px; background-color:#98b624; padding:10px 0px 10px 0px; color:white; margin:10px 0px 2px 0px;; font-size:12px; font-weight:bold;'> Kitchen Printer Redirects </div>
			<div id='kiprre' style='width:700px; height:150px; overflow:hidden'></div>

		</center>

	</div>

	<script src='areas/kitchen_printer_redirects.js'></script>
	<script type='text/javascript'>

		function iframeLoaded(iframe) {

			window.setTimeout(function(){adjustFrameSize(iframe)}, 100);
		}

		function adjustFrameSize(iframe) {

			var content_height = iframe.contentWindow.document.getElementById('pw_content_area').scrollHeight;
			var to_height = (content_height + 120);
			iframe.style.height = to_height + 'px';
			iframe.parentNode.style.height = to_height + 'px';
		}

		var kpr_c_id = "<?php echo admin_info("companyid"); ?>";
		var kpr_dn = "<?php echo $data_name; ?>";
		var kpr_loc_id = "<?php echo $locationid; ?>";

		initializeKIPRRE();

	</script>

<?php if (is_lavu_admin()) echo "<br>".$lavu_only."Indicates items visible only to Lavu Admin users (i.e., developers and support staff)"; ?>

	<script type='text/javascript'>

		window.onload= function() { }

	</script>