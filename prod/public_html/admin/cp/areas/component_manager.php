<?php

require_once( dirname(__FILE__) . "/../resources/LavuComponentManager.php" );

/**
 * Init
 */

global $locationid;

$namespace = 'default';
if ( admin_info('dataname') != '') $namespace = admin_info('dataname');  // Accounts granted access will use their datanames as their namespaces (so they can have multiple)
if ( admin_info("lavu_admin") && isset($_REQUEST['component_package_title']) ) $namespace = $_REQUEST['component_package_title'];  // Let Lavu Admins specify any `component_packages`.`title` so they can view the XML for anyone. 

$componentMgr = new LavuComponentManager($namespace);
$componentXmlHtml = '';
$configurationEnabledHtml = ($componentMgr->isEnabled($locationid)) ? 'checked' : '';


/**
 * Routing Logic
 */

// Check for when page posts back to itself.
if ( !empty($_REQUEST['component_xml']) ) {
	// Process the XML, save it to the database, and parse it back out to display it.
	$componentMgr->processXml( $_REQUEST['component_xml'] );
	$componentMgr->processEnabledStatus( $locationid, $_REQUEST['enabled'] );
	$configurationEnabledHtml = ($_REQUEST['enabled']) ? 'checked' : '';
}

// Turn the component settings from the database into XML and ready it for displaying.
$componentXml = $componentMgr->getXmlFromDatabase();
$componentXmlHtml = htmlspecialchars( $componentXml );


/**
 * Lavu Component Manager stuff - "inpsired" by /cp/areas/company_settings.php
 */

$speak = 'speak';
echo <<<HTML

<script>
var selectedTabId = "compMgrTab";
function selectTab(id) {

	if (selectedTabId != null) {
		var oldTab = document.getElementById(selectedTabId);
		if (oldTab != null) oldTab.className = "unselected";
		var oldDiv = document.getElementById(selectedTabId + "Div");
		if (oldDiv != null) oldDiv.className = "hidden";
	}

	var tab = document.getElementById(id);
	tab.className = 'selected';
	selectedTabId = id;

	var divElement = document.getElementById(id + "Div");
	if (divElement != null) divElement.className = "visible";
}

var xmlTagHelpImgs = ["images/xml_help_blank.png", "images/xml_help_product.png", "images/xml_help_tab.png", "images/xml_help_area.png"];
var xmlTagHelpImgIndex = 0;
function changeXmlTagHelpImg() {
	document.getElementById("xmlTagHelpImg").src = xmlTagHelpImgs[++xmlTagHelpImgIndex % xmlTagHelpImgs.length];
}
function changeXmlTagHelpImg(xmlTagHelpImgIndex) {
	document.getElementById("xmlTagHelpImg").src = xmlTagHelpImgs[xmlTagHelpImgIndex % xmlTagHelpImgs.length];
}
//setInterval( changeXmlTagHelpImg, 2000 );
</script>
<style>
#tabs ul {
	padding: 0;
	margin: 0 auto;
	list-style: none;
}
#tabs ul li {
	float: left;
	padding: 7px;
	width: 150px;
	margin-left: 5px;
	-webkit-border-top-left-radius: 1em;
	-webkit-border-top-right-radius: 1em;
	cursor: pointer;
	cursor: hand;
}
#tabs ul li:nth-child(1) {
	margin-left: 30%;
}
#tabs ul li a {
	display: block;
    margin: 0 6px 0 0;
    padding: 0 20px;
}
#tabContainer {
	margin-top: 0;
	border-top: 1px solid #a8c82b;
}
#xmlTagHelpImg {
	margin: 15px auto;
	margin-bottom: 0px;
}
#floatWrapper {
	width: 100%;
	margin: 0 auto;
}
#leftColumn {
	float: left;
	width: 40%;
}
#rightColumn {
	float: left;
	text-align: left;
	margin-left: 175px;
	margin-top: 0px;
	padding-top: 0px;
}
#screenshot {
	width: 240px;
	height: 240px;
}
#action {
	height: 24px;
	width: 100px;
	outline: none;
	text-transform: uppercase;
	color: white;
	background: #a8c82b;
	border: 1px solid #949590;
	-webkit-border-radius: .5em;
	background: -webkit-gradient(linear, left top, left bottom, form(#a8c82b), to(#c9e94c));
}
label {
	margin-right: 20px;
}
#compMgrTabDiv ul,
#doCmdsTabDiv ul {
	margin: 1em 30%;
	text-align: left;
}

#doCmdsTabDiv p {
	width: 70%;
}

#doCmdsTabDiv dl {
	text-align: left;
}

#doCmdsTabDiv dt {
	float: left;
	clear: left
}

#doCmdsTabDiv dd {
	float: left;
}

#doCmdsTabDiv table td code {
	/*font-size: 1.1em;*/
	font-family: sans-serif;
}

#DOcmdRef { 
    border-spacing: 2px;
    border-collapse: separate;
}

#DOcmdRef td { 
	background-color: #f0f0f0;
    border: 1px solid #f0f0f0;
    padding: 3px;
	vertical-align: top;
}
#addendum {
	margin-top: 15px;
}
#addendum ul li{
	padding: 10px;
}
h4 {
	margin-top: 20px;
}
textarea {
	border: 1px solid gray;
	border-top-left-radius: 10px;
	border-bottom-left-radius: 10px;
	box-shadow: 2px 3px 10px rgba(0,0,0,.3) inset;
	padding: 10px;
	color: green;
}
dl#xmlTagDescriptions {
	width: 350px;
}
dt {
	font-family: monospace;
	color: #a8c82b;
	font-size: 1.1em;
}
dd {
	margin-left: 0;
	/*word-wrap: break-word;*/
	margin-bottom: 5px;
}
dd strong {
}
li.selected {
	border-bottom: solid 1px #a8c82b;
	background-color: #a8c82b;
	color: white;
}
li.unselected {
	border-bottom: solid 1px white;
	background-color: #cccccc;
	color: gray;
}
figcaption {
	margin-top: 20px;
	font-weight: bold;
}
.hidden {
	display: none;
}
.visible {
	display: block;
}
.indent1 {
	margin-left: 1em;
}
.indent2 {
	margin-left: 2em;
}
.indent3 {
	margin-left: 3em;
}

.headers {
	font-size: .75em;
	text-transform: uppercase;
	color: darkgray;
}

.headerRow {
	font-weight: bold;
}

table#xmlTags,
table#webviewPostVars
{
	margin-top: 10px;
    border-spacing: 2px;
    border-collapse: separate;
}

table#xmlTags td,
table#webviewPostVars td {
    border: 1px solid #f0f0f0;
	background-color: #f0f0f0;
	padding: 3px;
	vertical-align: top;
}

table#DOcmdRef table.args,
table#DOcmdRef table.example {
	width: 100%;
	margin-top: 10px;
    border-spacing: 2px;
    border-collapse: separate;
}

table#DOcmdRef table.args td { 
    border: 1px solid #fcfcfc;
	background-color: #fcfcfc;
	white-space: normal;
	max-width: 300px;
}

table#DOcmdRef table.example td { 
    border: 1px solid #F5F5F0;
	background-color: #F5F5F0;
	white-space: normal;
	max-width: 300px;
}

</style>

</head>
<body>

<div id="tabs">
	<ul>
		<li id="compMgrTab" class="selected"   onclick="selectTab('compMgrTab')">{$speak('Component Manager')}</li>
		<li id="webviewTab" class="unselected" onclick="selectTab('webviewTab')">{$speak('Webviews')}</li>
		<li id="doCmdsTab"  class="unselected" onclick="selectTab('doCmdsTab')">{$speak('_DO Commands')}</li>
	<ul>
</div>

<div style="clear:both" />

<div id="tabContainer">

	<div id="compMgrTabDiv">

		<h3>{$speak('Component Manager')}</h3>

		<p>Manage the tabs shown in POS Lavu and their layouts by modifying the XML Component Configuration section below.</p>

		<div id="floatWrapper">
			<div id="leftColumn">
				<form method="post">
				<h4>XML Component Configuration</h4>
				<textarea name="component_xml" id="component_xml" cols="75" rows="30" wrap="off">{$componentXmlHtml}</textarea><br>
				<label for="enabled"><input type="checkbox" name="enabled" id="enabled" value="1" {$configurationEnabledHtml} /> Enable Configuration</label>
				<input type="submit" name="action" id="action" value="{$speak('Save')}" />
				</form>
			</div>
			<div id="rightColumn">
				<figure>
				<strong>To Get Started:</strong>
				<p>Add this XML below the POS tab's &lt;/tab&gt;<br> element to create a new tab.</p>
				<pre>
&lt;tab&gt;
	&lt;title&gt;Test&lt;/title&gt;
	&lt;type&gt;webview&lt;/type&gt;
&lt;/tab&gt;
				</pre>
				<p>Then check <strong>Enable Configuration</strong> and click <strong>Save</strong>.</p>
				<p><strong>Note:</strong> You must <strong>Reload Settings</strong> in the app every<br>
				time you change your configuration.</p>
				<figcaption>Example Screenshot</figcaption>
				<p>Hovering over the XML Tag Descriptions will show<br> what area of the app is affected by that element.</p>
				<img src="images/xml_help_blank.png" id="xmlTagHelpImg" border="0" width="273" height="241"><br>
				</figure>
			</div>
		</div>

		<div style="clear:both" />

		<h4>XML Tag Descriptions</h4>
		<table id="xmlTags">
		<tr class="headerRow">
			<td>XPath</td>
			<td>Type</td>
			<td>Description</td>
			<td>Value</td>
		</tr>
		<tr>
			<td onmouseover="changeXmlTagHelpImg(1)" onmouseout="changeXmlTagHelpImg(0)">/product</td>
			<td onmouseover="changeXmlTagHelpImg(1)" onmouseout="changeXmlTagHelpImg(0)">Required</td>
			<td onmouseover="changeXmlTagHelpImg(1)" onmouseout="changeXmlTagHelpImg(0)">Contains the tab element(s).</td>
			<td onmouseover="changeXmlTagHelpImg(1)" onmouseout="changeXmlTagHelpImg(0)"></td>
		</tr>
		<tr>
			<td onmouseover="changeXmlTagHelpImg(2)" onmouseout="changeXmlTagHelpImg(0)" class="indent1">/product/tabs/tab</td>
			<td onmouseover="changeXmlTagHelpImg(2)" onmouseout="changeXmlTagHelpImg(0)" class="indent1"></td>
			<td onmouseover="changeXmlTagHelpImg(2)" onmouseout="changeXmlTagHelpImg(0)" class="indent1">Creates a tab and its content area; the order of the tab elements determine the tab row order.</td>
			<td onmouseover="changeXmlTagHelpImg(2)" onmouseout="changeXmlTagHelpImg(0)" class="indent1"></td>
		</tr>
		<tr>
			<td onmouseover="changeXmlTagHelpImg(2)" onmouseout="changeXmlTagHelpImg(0)" class="indent2">/product/tabs/tab/name</td>
			<td onmouseover="changeXmlTagHelpImg(2)" onmouseout="changeXmlTagHelpImg(0)" class="indent2">Required</td>
			<td onmouseover="changeXmlTagHelpImg(2)" onmouseout="changeXmlTagHelpImg(0)" class="indent2">All lowercase version of /product/tabs/tab/title (usually with underscores instead of spaces) used when programmatically referencing the tab.</td>
			<td onmouseover="changeXmlTagHelpImg(2)" onmouseout="changeXmlTagHelpImg(0)" class="indent2"></td>
		</tr>
		<tr>
			<td onmouseover="changeXmlTagHelpImg(2)" onmouseout="changeXmlTagHelpImg(0)" class="indent2">/product/tabs/tab/title</td>
			<td onmouseover="changeXmlTagHelpImg(2)" onmouseout="changeXmlTagHelpImg(0)" class="indent2">Required</td>
			<td onmouseover="changeXmlTagHelpImg(2)" onmouseout="changeXmlTagHelpImg(0)" class="indent2">User-visible text shown on tab.</td>
			<td onmouseover="changeXmlTagHelpImg(2)" onmouseout="changeXmlTagHelpImg(0)" class="indent2"></td>
		</tr>
		<tr>
			<td onmouseover="changeXmlTagHelpImg(2)" onmouseout="changeXmlTagHelpImg(0)" class="indent2">/product/tabs/tab/type</td>
			<td onmouseover="changeXmlTagHelpImg(2)" onmouseout="changeXmlTagHelpImg(0)" class="indent2">Required</td>
			<td onmouseover="changeXmlTagHelpImg(2)" onmouseout="changeXmlTagHelpImg(0)" class="indent2">Tab type; usually "webview" since app defaults to having POS tab.</td>
			<td onmouseover="changeXmlTagHelpImg(2)" onmouseout="changeXmlTagHelpImg(0)" class="indent2">pos|webview</td>
		</tr>
		<tr>
			<td onmouseover="changeXmlTagHelpImg(2)" onmouseout="changeXmlTagHelpImg(0)" class="indent2">/product/tabs/tab/background</td>
			<td onmouseover="changeXmlTagHelpImg(2)" onmouseout="changeXmlTagHelpImg(0)" class="indent2"></td>
			<td onmouseover="changeXmlTagHelpImg(2)" onmouseout="changeXmlTagHelpImg(0)" class="indent2">URL for background image of tab content area.</td>
			<td onmouseover="changeXmlTagHelpImg(2)" onmouseout="changeXmlTagHelpImg(0)" class="indent2"></td>
		</tr>
		<tr>
			<td onmouseover="changeXmlTagHelpImg(2)" onmouseout="changeXmlTagHelpImg(0)" class="indent2">/product/tabs/tab/special_type</td>
			<td onmouseover="changeXmlTagHelpImg(2)" onmouseout="changeXmlTagHelpImg(0)" class="indent2">Internal</td>
			<td onmouseover="changeXmlTagHelpImg(2)" onmouseout="changeXmlTagHelpImg(0)" class="indent2">Internal Use Only</td>
			<td onmouseover="changeXmlTagHelpImg(2)" onmouseout="changeXmlTagHelpImg(0)" class="indent2">(blank)</td>
		</tr>
		<tr>
			<td onmouseover="changeXmlTagHelpImg(2)" onmouseout="changeXmlTagHelpImg(0)" class="indent2">/product/tabs/tab/modules_required</td>
			<td onmouseover="changeXmlTagHelpImg(2)" onmouseout="changeXmlTagHelpImg(0)" class="indent2">Internal</td>
			<td onmouseover="changeXmlTagHelpImg(2)" onmouseout="changeXmlTagHelpImg(0)" class="indent2">Internal Use Only</td>
			<td onmouseover="changeXmlTagHelpImg(2)" onmouseout="changeXmlTagHelpImg(0)" class="indent2">(blank)</td>
		</tr>
		<tr>
			<td onmouseover="changeXmlTagHelpImg(2)" onmouseout="changeXmlTagHelpImg(0)" class="indent2">/product/tabs/tab/id</td>
			<td onmouseover="changeXmlTagHelpImg(2)" onmouseout="changeXmlTagHelpImg(0)" class="indent2">Internal</td>
			<td onmouseover="changeXmlTagHelpImg(2)" onmouseout="changeXmlTagHelpImg(0)" class="indent2">Internal Use Only <strong>**Do not modify ID values**</strong></td>
			<td onmouseover="changeXmlTagHelpImg(2)" onmouseout="changeXmlTagHelpImg(0)" class="indent2">&lt;int&gt;</td>
		</tr>
		<tr>
			<td onmouseover="changeXmlTagHelpImg(2)" onmouseout="changeXmlTagHelpImg(0)" class="indent2">/product/tabs/tab/notes</td>
			<td onmouseover="changeXmlTagHelpImg(2)" onmouseout="changeXmlTagHelpImg(0)" class="indent2"></td>
			<td onmouseover="changeXmlTagHelpImg(2)" onmouseout="changeXmlTagHelpImg(0)" class="indent2">Internal notes about the tab; not displayed to users.</td>
			<td onmouseover="changeXmlTagHelpImg(2)" onmouseout="changeXmlTagHelpImg(0)" class="indent2"></td>
		</tr>
		<tr>
			<td onmouseover="changeXmlTagHelpImg(2)" onmouseout="changeXmlTagHelpImg(0)" class="indent2">/product/tabs/tab/area</td>
			<td onmouseover="changeXmlTagHelpImg(2)" onmouseout="changeXmlTagHelpImg(0)" class="indent2"></td>
			<td onmouseover="changeXmlTagHelpImg(2)" onmouseout="changeXmlTagHelpImg(0)" class="indent2">Contains configuration information for webview shown in tab content area.</td>
			<td onmouseover="changeXmlTagHelpImg(2)" onmouseout="changeXmlTagHelpImg(0)" class="indent2"></td>
		</tr>
		<tr>
			<td onmouseover="changeXmlTagHelpImg(3)" onmouseout="changeXmlTagHelpImg(0)" class="indent3">/product/tabs/tab/areas/area/name</td>
			<td onmouseover="changeXmlTagHelpImg(3)" onmouseout="changeXmlTagHelpImg(0)" class="indent3"></td>
			<td onmouseover="changeXmlTagHelpImg(3)" onmouseout="changeXmlTagHelpImg(0)" class="indent3">All lowercase name used when programmatically referencing the webview.</td>
			<td onmouseover="changeXmlTagHelpImg(3)" onmouseout="changeXmlTagHelpImg(0)" class="indent3"></td>
		</tr>
		<tr>
			<td onmouseover="changeXmlTagHelpImg(3)" onmouseout="changeXmlTagHelpImg(0)" class="indent3">/product/tabs/tab/areas/area/title</td>
			<td onmouseover="changeXmlTagHelpImg(3)" onmouseout="changeXmlTagHelpImg(0)" class="indent3"></td>
			<td onmouseover="changeXmlTagHelpImg(3)" onmouseout="changeXmlTagHelpImg(0)" class="indent3">Display name of webview; not shown to users.</td>
			<td onmouseover="changeXmlTagHelpImg(3)" onmouseout="changeXmlTagHelpImg(0)" class="indent3"></td>
		</tr>
		<tr>
			<td onmouseover="changeXmlTagHelpImg(3)" onmouseout="changeXmlTagHelpImg(0)" class="indent3">/product/tabs/tab/areas/area/id</td>
			<td onmouseover="changeXmlTagHelpImg(3)" onmouseout="changeXmlTagHelpImg(0)" class="indent3">Internal</td>
			<td onmouseover="changeXmlTagHelpImg(3)" onmouseout="changeXmlTagHelpImg(0)" class="indent3">Internal Use Only <strong>**Do not modify ID values**</td>
			<td onmouseover="changeXmlTagHelpImg(3)" onmouseout="changeXmlTagHelpImg(0)" class="indent3">&lt;int&gt;</td>
		</tr>
		<tr>
			<td onmouseover="changeXmlTagHelpImg(3)" onmouseout="changeXmlTagHelpImg(0)" class="indent3">/product/tabs/tab/areas/area/src</td>
			<td onmouseover="changeXmlTagHelpImg(3)" onmouseout="changeXmlTagHelpImg(0)" class="indent3"></td>
			<td onmouseover="changeXmlTagHelpImg(3)" onmouseout="changeXmlTagHelpImg(0)" class="indent3">URL of web page displayed in webview.</td>
			<td onmouseover="changeXmlTagHelpImg(3)" onmouseout="changeXmlTagHelpImg(0)" class="indent3"></td>
		</tr>
		<tr>
			<td onmouseover="changeXmlTagHelpImg(3)" onmouseout="changeXmlTagHelpImg(0)" class="indent3">/product/tabs/tab/areas/area/scrollable</td>
			<td onmouseover="changeXmlTagHelpImg(3)" onmouseout="changeXmlTagHelpImg(0)" class="indent3"></td>
			<td onmouseover="changeXmlTagHelpImg(3)" onmouseout="changeXmlTagHelpImg(0)" class="indent3">Whether scrolling is enabled for webview.</td>
			<td onmouseover="changeXmlTagHelpImg(3)" onmouseout="changeXmlTagHelpImg(0)" class="indent3">0|1</td>
		</tr>
		<tr>
			<td onmouseover="changeXmlTagHelpImg(3)" onmouseout="changeXmlTagHelpImg(0)" class="indent3">/product/tabs/tab/areas/area/coord_x</td>
			<td onmouseover="changeXmlTagHelpImg(3)" onmouseout="changeXmlTagHelpImg(0)" class="indent3"></td>
			<td onmouseover="changeXmlTagHelpImg(3)" onmouseout="changeXmlTagHelpImg(0)" class="indent3">X coordinate for upper-lefthand corner of webview.</td>
			<td onmouseover="changeXmlTagHelpImg(3)" onmouseout="changeXmlTagHelpImg(0)" class="indent3">0-1023</td>
		</tr>
		<tr>
			<td onmouseover="changeXmlTagHelpImg(3)" onmouseout="changeXmlTagHelpImg(0)" class="indent3">/product/tabs/tab/areas/area/coord_y</td>
			<td onmouseover="changeXmlTagHelpImg(3)" onmouseout="changeXmlTagHelpImg(0)" class="indent3"></td>
			<td onmouseover="changeXmlTagHelpImg(3)" onmouseout="changeXmlTagHelpImg(0)" class="indent3">Y coordinate for upper-lefthand corner of webview.</td>
			<td onmouseover="changeXmlTagHelpImg(3)" onmouseout="changeXmlTagHelpImg(0)" class="indent3">0-619</td>
		</tr>
		<tr>
			<td onmouseover="changeXmlTagHelpImg(3)" onmouseout="changeXmlTagHelpImg(0)" class="indent3">/product/tabs/tab/areas/area/width</td>
			<td onmouseover="changeXmlTagHelpImg(3)" onmouseout="changeXmlTagHelpImg(0)" class="indent3"></td>
			<td onmouseover="changeXmlTagHelpImg(3)" onmouseout="changeXmlTagHelpImg(0)" class="indent3">Width of webview</td>
			<td onmouseover="changeXmlTagHelpImg(3)" onmouseout="changeXmlTagHelpImg(0)" class="indent3">1-1024</td>
		</tr>
		<tr>
			<td onmouseover="changeXmlTagHelpImg(3)" onmouseout="changeXmlTagHelpImg(0)" class="indent3">/product/tabs/tab/areas/area/height</td>
			<td onmouseover="changeXmlTagHelpImg(3)" onmouseout="changeXmlTagHelpImg(0)" class="indent3"></td>
			<td onmouseover="changeXmlTagHelpImg(3)" onmouseout="changeXmlTagHelpImg(0)" class="indent3">Height of webview</td>
			<td onmouseover="changeXmlTagHelpImg(3)" onmouseout="changeXmlTagHelpImg(0)" class="indent3">1-620</td>
		</tr>
		<tr>
			<td onmouseover="changeXmlTagHelpImg(3)" onmouseout="changeXmlTagHelpImg(0)" class="indent3">/product/tabs/tab/areas/area/z-index</td>
			<td onmouseover="changeXmlTagHelpImg(3)" onmouseout="changeXmlTagHelpImg(0)" class="indent3"></td>
			<td onmouseover="changeXmlTagHelpImg(3)" onmouseout="changeXmlTagHelpImg(0)" class="indent3">Unsigned integer for rendering order of webview - determines how webviews overlap.</td>
			<td onmouseover="changeXmlTagHelpImg(3)" onmouseout="changeXmlTagHelpImg(0)" class="indent3"></td>
		</tr>
		<tr>
			<td onmouseover="changeXmlTagHelpImg(3)" onmouseout="changeXmlTagHelpImg(0)" class="indent3">/product/tabs/tab/areas/area/type</td>
			<td onmouseover="changeXmlTagHelpImg(3)" onmouseout="changeXmlTagHelpImg(0)" class="indent3"></td>
			<td onmouseover="changeXmlTagHelpImg(3)" onmouseout="changeXmlTagHelpImg(0)" class="indent3">Visibility type (v = visible, h = hidden)</td>
			<td onmouseover="changeXmlTagHelpImg(3)" onmouseout="changeXmlTagHelpImg(0)" class="indent3">v|h</td>
		</table>

	</div>

</div>

<div id="webviewTabDiv" class="hidden">

	<h3>Webviews</h3>

	<p>The app can display hosted web pages in a tab's content area in webviews.</p>

	<p>When a webview is loaded, the app sends it a HTTP POST request containing the following variables:</p>

	<table id="webviewPostVars">
	<tr class="headerRow">
		<td>POST Variable</td>
		<td>Description</td>
	</tr>
	<tr>
		<td>comp_name</td>
		<td>Component name which can be used to reference the component programmatically. <em>(XPath /product/tabs/tab/areas/area/name)</em></td>
	</tr>
	<tr>
		<td>XIG</td>
		<td>Four digit random integer that is generated when component loads, usually used to create unique URLs to prevent caching.</td>
	</tr>
	<tr>
		<td>server_id</td>
		<td>Server ID of user signed into app.</td>
	</tr>
	<tr>
		<td>server_name</td>
		<td>First name of user signed into app.</td>
	</tr>
	<tr>
		<td>loc_id</td>
		<td>Location ID.</td>
	</tr>
	<tr>
		<td>tab_name</td>
		<td>Tab name display name. <em>(XPath /product/tabs/tab/title)</em></td>
	</tr>
	<tr>
		<td>cc</td>
		<td>Company code, which contains dataname and additional internal identifiers.</td>
	</tr>
	<tr>
		<td>dn</td>
		<td>Dataname.</td>
	</tr>
	<tr>
		<td>available_DOcmds</td>
		<td>Comma-delimited list of the DOcmd javascript functions.</td>
	</tr>
	</table>

	<h4>refresh()</h4>
	<p>The following JavaScript function can be defined to control how the webview behalves when refreshed:</p>
	<code><pre>
		function refresh() {
		...

		return 1;
}
	</pre></code>
	<p>Webviews are refreshed whenever tabs become active or when a Reload Settings is performed from the LAVU button.</p>

</div>

<div id="doCmdsTabDiv" class="hidden">

	<h3>_DO Commands</h3>

	<p>_DO Commands allow webviews to communicate with the app.</p>

	<p>A _DO Command is performed by loading a URL (using JavaScript, usually) that starts with <code>_DO:</code> follwed by a <code>cmd</code> parameter set to the command name:</p>

	<code><pre>window.location = "_DO:cmd=takePicture";</pre></code>

	<p>Parameters are specified by appending querystring-like <code>key=value</code> pairs separated by &.  Values containing HTML special characters can be escaped using the JavaScript function <code>encodeURIComponent()</code>.</p>

	<p>Triggering a _DO Command does not load a new web pages; the app simply executes the predefined behaviors described below when it detects a valid _DO Command URL.</p>

	<h4>Supported _DO Commands</h4>

	<table id="DOcmdRef">
	<tr class="headerRow">
		<td>cmd</td>
		<td>Description</td>
	</tr>
	<tr>
		<td valign="top"><code>create_overlay</code></td>
		<td valign="top" colspan="4">Creates a webview layered above the current webview.
			<p>A querystring can be appended to the end of the _DO Command key=value pairs to pass parameters through to the overlay page.</p>
			<table class="args">
			<tr class="headers"><td>Args</td><td>Type</td><td>Desc</td><td>Values</td></tr>
			<tr><td><code>c_name</code></td><td>Required</td><td>Component name of the newly created component</td><td>&lt;New Component Name(s)&gt;</td></tr>
			<tr><td><code>f_name</code></td><td>Required</td><td>File name</td><td>&lt;File Path(s)&gt;</td></tr>
			<tr><td><code>dims</code></td><td>Required</td><td>Dimensions</td><td>&lt;Dimensions String(s)&gt;<br><em>Format: x1,y1,x2,y2</td></tr>
			<tr><td><code>scroll</code></td><td>Optional</td><td>Allow scrollbars</td><td>0|1</td></tr>
			</table><br>
			<table class="example">
			<tr class="headers"><td>Example</td></tr>
			<tr><td><code><pre>window.location = "_DO:cmd=create_overlay&c_name=foo&f_name=views/bar.php&dims=0,0,300,250&scroll=1?pg=a1&step=1";</pre></code></td>
			</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td valign="top"><code>load_url</code></td>
		<td valign="top" colspan="4">Load the specified URL in a component.
			<p>A querystring can be appended to the end of the _DO Command key=value pairs to pass parameters through to the overlay page.</p>
			<table class="args">
			<tr class="headers"><td>Args</td><td>Type</td><td>Desc</td><td>Values</td></tr>
			<tr><td><code>c_name</code></td><td>Required</td><td>Component name</td><td>&lt;Component Name(s)&gt;</td></tr>
			<tr><td><code>f_name</code></td><td>Required</td><td>File name to load in component</td><td>&lt;File Path(s)&gt;</td></tr>
			</table><br>
			<table class="example">
			<tr class="headers"><td>Example</td></tr>
			<tr><td><code><pre>window.location = "_DO:cmd=load_url&c_name=foo&f_name=views/bar.php?sessionid="+ encodeURIComponent(sessionId);</pre></code></td>
			</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td valign="top"><code>set_scrollable</code>
		<td valign="top" colspan="4">Set whether the webview is scrollable.
			<table class="args">
			<tr class="headers"><td>Args</td><td>Type</td><td>Desc</td><td>Values</td></tr>
			<tr><td><code>c_name</code></td><td>Required</td><td>Component name</td><td>&lt;Component Name&gt;</td></tr>
			<tr><td><code>set_to</code></td><td>Required</td><td>Scrollable flag value</td><td>0|1</td></tr>
			</table><br>
			<table class="example">
			<tr class="headers"><td>Example</td></tr>
			<tr><td><code><pre>window.location = "_DO:cmd=set_scrollable&c_name=foo&set_to=0";</pre></code></td>
			</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td valign="top"><code>send_js</code></td>
		<td valign="top" colspan="4">Run javascript commands in the specified component.
			<table class="args">
			<tr class="headers"><td>Args</td><td>Type</td><td>Desc</td><td>Values</td></tr>
			<tr><td><code>c_name</code></td><td>Required</td><td>Component name</td><td>&lt;Component Name(s)&gt;</td></tr>
			<tr><td><code>js</code></td><td>Required</td><td>Javascript to run</td><td>&lt;Javascript statement(s)&gt;</td></tr>
			</table><br>
			<table class="example">
			<tr class="headers"><td>Example</td></tr>
			<tr><td><code><pre>window.location = "_DO:cmd=send_js&c_name=foo&js=logHistoryEvent('6')";</pre></code></td>
			</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td valign="top"><code>close_overlay</code></td>
		<td valign="top" colspan="4">Close the specified overlay.
			<table class="args">
			<tr class="headers"><td>Args</td><td>Type</td><td>Desc</td><td>Values</td></tr>
			<tr><td><code>c_name</code></td><td>Required</td><td>Component name in which to load <code>f_name</code> or execute <code>js</code> after closing overlay</td><td>parent_overlay|&lt;Component Name(s)&gt;</td></tr>
			<tr><td><code>f_name</code></td><td>Required - unless <code>js</code> is specified</td><td>Component name</td><td>&lt;File Path(s)&gt;</td></tr>
			<tr><td><code>js</code></td><td>Required - unless <code>f_name</code> is specified</td><td>Javascript to run</td><td>&lt;Javascript statement(s)&gt;</td></tr>
			</table><br>
			<table class="example">
			<tr class="headers"><td>Example</td></tr>
			<tr><td><code><pre>window.location = "_DO:cmd=close_overlay&c_name=foo&f_name=views/bar.php";</pre></code></td>
			</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td valign="top"><code>alert</code></td>
		<td valign="top" colspan="4">Native iOS version of js alert().
			<table class="args">
			<tr class="headers"><td>Args</td><td>Type</td><td>Desc</td><td>Values</td></tr>
			<tr><td><code>title</code></td><td>Required - unless <code>message</code> is specified</td><td>Text to display in bold along upper-portion of alert window</td><td>&lt;string&gt;</td></tr>
			<tr><td><code>message</code></td><td>Required - unless <code>title</code> is specified</td><td>Text to display along lower-portion of alert window</td><td>&lt;string&gt;</td></tr>
			<tr><td><code>c_name</code></td><td>Optional - but must be specified with <code>f_name</code></td><td>Component name in which to load <code>f_name</code> after displaying alert window</td><td>&lt;Component Name&gt;</td></tr>
			<tr><td><code>f_name</code></td><td>Optional - but must be specified with <code>c_name</code></td><td>File name to load in component</td><td>&lt;File Path&gt;</td></tr>
			</table><br>
			<table class="example">
			<tr class="headers"><td>Example</td></tr>
			<tr><td><code><pre>window.location = "_DO:cmd=alert&title=WARNING&message=No tip amount specified.";</pre></code></td>
			</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td valign="top"><code>confirm</code></td>
		<td valign="top" colspan="4">Native iOS version of js confirm().
			<table class="args">
			<tr class="headers"><td>Args</td><td>Type</td><td>Desc</td><td>Values</td></tr>
			<tr><td><code>title</code></td><td>Required</td><td>Text to display in bold along upper-portion of confirm() window</td><td>&lt;string&gt;</td></tr>
			<tr><td><code>message</code></td><td>Required</td><td>Text to display along lower-portion of confirm() window</td><td>&lt;string&gt;</td></tr>
			<tr><td><code>c_name</code></td><td>Required</td><td>Component name in which to run <code>js_cmd</code> after displaying confirm() window</td><td>&lt;Component Name&gt;</td></tr>
			<tr><td><code>js_cmd</code></td><td>Required</td><td>Javascript to run</td><td>&lt;Javascript statement&gt;</td></tr>
			</table><br>
			<table class="example">
			<tr class="headers"><td>Example</td></tr>
			<tr><td><code><pre>window.location = "_DO:cmd=confirm&title=Continue&message=&c_name=foo&js_cmd=recordCustomerChoice('1-123', '59')";</pre></code></td>
			</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td valign="top"><code>number_pad</code></td>
		<td valign="top" colspan="4">Display the native iOS numeric keypad for input.
			<table class="args">
			<tr class="headers"><td>Args</td><td>Type</td><td>Desc</td><td>Values</td></tr>
			<tr><td><code>title</code></td><td>Required</td><td>Text to display in bold along upper-portion of confirm() window</td><td>&lt;string&gt;</td></tr>
			<tr><td><code>type</code></td><td>Required</td><td>Type of numeric keypad to display</td><td>single_digit|multi_digit|decimal|dashed|money|PIN</td></tr>
			<tr><td><code>c_name</code></td><td>Required</td><td>Component name in which to run <code>send_cmd</code> before displaying numeric keypad</td><td>&lt;Component Name&gt;</td></tr>
			<tr><td><code>send_cmd</code></td><td>Required</td><td>Javascript to run</td><td>&lt;Javascript statement&gt;</td></tr>
			<tr><td><code>max_digits</code></td><td>Optional</td><td>Maximum number of digits to display</td><td>&lt;integer&gt;<br><em>Default: 2</em></td></tr>
			</table><br>
			<table class="example">
			<tr class="headers"><td>Example</td></tr>
			<tr><td><code><pre>window.location = "_DO:cmd=number_pad&title=PIN&type=PIN&c_name=foo&send_cmd=acquiredPin()&max_digits=4";</pre></code></td>
			</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td valign="top"><code>update_signature</code></td>
		<td valign="top" colspan="4">Display the Lavu signature acquisition window to update a customer's signature on file.
			<table class="args">
			<tr class="headers"><td>Args</td><td>Type</td><td>Desc</td><td>Values</td></tr>
			<tr><td><code>refresh_comp</code></td><td>Required</td><td>Flag for whether the specified component should be refreshed after the signature window is dismissed</td><td>0|1</td></tr>
			<tr><td><code>customer_id</code></td><td>Required</td><td>Customer ID to associate with signature</td><td>&lt;integer&gt;</td></tr>
			<tr><td><code>minor_or_adult</code></td><td>Required</td><td>Whether to display the minor or adult waiver text.</td><td>adult|minor</td></tr>
			<tr><td><code>type</code></td><td>Optional</td><td>Type of waiver (currently should just contain the same value as <code>minor_or_adult</code>)</td><td>adult|minor<br><em>Default: adult</em></td></tr>
			<tr><td><code>js</code></td><td>Optional</td><td>Javascript to run</td><td>&lt;Javascript statement&gt;</td></tr>
			<tr><td><code>bgimage</code></td><td>Optional</td><td>URL/Path to background image file</td><td>&lt;File Path or URL&gt;</td></tr>
			<tr><td><code>message</code></td><td>Optional</td><td>Message to display on signature window</td><td>&lt;string&gt;</td></tr>
			</table><br>
			<table class="example">
			<tr class="headers"><td>Example</td></tr>
			<tr><td><code><pre>window.location = "_DO:cmd=update_signature&refresh_comp=1&customer_id=59&minor_or_adult=adult&type=adult&message=Sign";</pre></code></td>
			</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td valign="top"><code>make_hidden</code></td>
		<td valign="top" colspan="4">Hides visible component.
			<table class="args">
			<tr class="headers"><td>Args</td><td>Type</td><td>Desc</td><td>Values</td></tr>
			<tr><td><code>c_name</code></td><td>Required</td><td>Component name</td><td>&lt;Component Name(s)&gt;</td></tr>
			</table><br>
			<table class="example">
			<tr class="headers"><td>Example</td></tr>
			<tr><td><code><pre>window.location = "_DO:cmd=make_hidden&c_name=foo";</pre></code></td>
			</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td valign="top"><code>make_visible</code></td>
		<td valign="top" colspan="4">Displays hidden component.
			<table class="args">
			<tr class="headers"><td>Args</td><td>Type</td><td>Desc</td><td>Values</td></tr>
			<tr><td><code>c_name</code></td><td>Required</td><td>Component name</td><td>&lt;Component Name(s)&gt;</td></tr>
			</table><br>
			<table class="example">
			<tr class="headers"><td>Example</td></tr>
			<tr><td><code><pre>window.location = "_DO:cmd=make_visible&c_name=foo";</pre></code></td>
			</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td valign="top"><code>jump_to_order</code></td>
		<td valign="top" colspan="4">Jumps to the POS tab and displays the specified order.
			<table class="args">
			<tr class="headers"><td>Args</td><td>Type</td><td>Desc</td><td>Values</td></tr>
			<tr><td><code>order_id</code></td><td>Required</td><td>Order ID</td><td>new|&lt;Existing Order ID&gt;</td></tr>
			<tr><td><code>auto_fill</code></td><td>Optional</td><td>Automatically fills in additional order info using the specified data in querystring format</td><td>&lt;Auto-fill Querystring&gt;<br><em>Format: key1=val1&val2=val2<br>Escape: & with [AMEPERSAND], = with [EQUAL]<br>Key names: order table columns</em></td></tr>
			</table><br>
			<table class="example">
			<tr class="headers"><td>Example</td></tr>
			<tr><td><code><pre>window.location = "_DO:cmd=jump_to_order&order_id=1-123&auto_fill=togo_name[EQUALS]Chad";</pre></code></td>
			</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td valign="top"><code>clear_active_order</code></td>
		<td valign="top" colspan="4">Clears the active order of any menu items.<br>
			<table class="example">
			<tr class="headers"><td>Example</td></tr>
			<tr><td><code><pre>window.location = "_DO:cmd=clear_active_order";</pre></code></td>
			</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td valign="top"><code>get_active_order_info</code></td>
		<td valign="top" colspan="4">Runs the specified javascript in which all occurrences of the string "[ORDER_INFO]" have been replaced by a JSON encoded-string like:<br>
			<pre>
{
	"active_screen" : &lt;Active Screen Name&gt;,
	"order_id" : &lt;Order ID&gt;,
	"total" : &lt;Order Total&gt;,
	"should_save" : &lt;Flag Indicating Unsaved Modified Order&gt;
}
			</pre>
			<table class="args">
			<tr class="headers"><td>Args</td><td>Type</td><td>Desc</td><td>Values</td></tr>
			<tr><td><code>js</code></td><td>Required</td><td>Javascript to run</td><td>&lt;Javascript statement(s)&gt;</td></tr>
			</table><br>
			<table class="example">
			<tr class="headers"><td>Example</td></tr>
			<tr><td><code><pre>window.location = "_DO:cmd=get_active_order_info";</pre></code></td>
			</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td valign="top"><code>save_active_order</code></td>
		<td valign="top" colspan="4">Saves any modifications to the order since the last save.<br>
			<table class="example">
			<tr class="headers"><td>Example</td></tr>
			<tr><td><code><pre>window.location = "_DO:cmd=save_active_order";</pre></code></td>
			</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td valign="top"><code>startListeningForSwipe</code></td>
		<td valign="top" colspan="4">Enables the card swiper listener for the calling webview component.
		<p>The following JavaScript function will need to be defined to act as a call back for swipe events:</p>
		<code><pre>
function handleCardSwipe(swipeString, dataString, reader, encrypted) {
	...

	return 1;
}
		</pre></code><br>
			<table class="example">
			<tr class="headers"><td>Example</td></tr>
			<tr><td><code><pre>window.location = "_DO:cmd=startListeningForSwipe";</pre></code></td>
			</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td valign="top"><code>stopListeningForSwipe</code></td>
		<td valign="top" colspan="4">Disables the card swiper listener for the calling webview component.<br>
			<table class="example">
			<tr class="headers"><td>Example</td></tr>
			<tr><td><code><pre>window.location = "_DO:cmd=stopListeningForSwipe";</pre></code></td>
			</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td valign="top"><code>startListeningForKEI</code></td>
		<td valign="top" colspan="4">Enables the Keyboard Emulator Input (KEI) listener for the calling webview component.
			<p>The following JavaScript function will need to be defined to act as a call back for swipe events:</p>
			<code><pre>
function handleKEI(keiData) {
	...

	return 1;
}
		</pre></code>
		<p>where <code>keiData</code> is a string of the KEI input read.</p><br>
			<table class="example">
			<tr class="headers"><td>Example</td></tr>
			<tr><td><code><pre>window.location = "_DO:cmd=startListeningForKEI";</pre></code></td>
			</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td valign="top"><code>stopListeningForKEI</code></td>
		<td valign="top" colspan="4">Disables the Keyboard Emulator Input (KEI) listener for the calling webview component.<br>
			<table class="example">
			<tr class="headers"><td>Example</td></tr>
			<tr><td><code><pre>window.location = "_DO:cmd=stopListeningForKEI";</pre></code></td></td>
			</tr>
			</table>
	</tr>
	<tr>
		<td valign="top"><code>takePicture</code></td>
		<td valign="top" colspan="4">Activates native iOS camera app in full screen mode to take a picture.<br>
			<table class="example">
			<tr class="headers"><td>Example</td></tr>
			<tr><td><code><pre>window.location = "_DO:cmd=takePicture";</pre></code></td>
			</tr>
			</table>
		<p>The following JavaScript function will need to be defined to act as a call back for swipe events</p>
		<code><pre>
function processPicture(pictureData) {
	...

	return 1;
}
		</pre></code>
		<p>where <code>pictureData</code> is a base64 encoded string of the JPEG.</p>
		</td>
	</tr>
	<tr>
		<td valign="top"><code>go_to_tab</code></td>
		<td valign="top" colspan="4">Go to the specified tab.
			<table class="args">
			<tr class="headers"><td>Args</td><td>Type</td><td>Desc</td><td>Values</td></tr>
			<tr><td><code>tab_id</code></td><td>Required</td><td>Tab ID</td><td>&lt;Tab ID&gt;</td></tr>
			<tr><td><code>c_name</code></td><td>Optional - but must be specified with <code>js</code></td><td>Component name</td><td>&lt;Component Name(s)&gt;<br><em>Multiple value delimiter: ;;</em></td></tr>
			<tr><td><code>js</code></td><td>Optional - but must be specified with <code>c_name</code></td><td>Javascript to run after going to tab</td><td>&lt;Javascript statement(s)&gt;<br><em>Multiple value delimiter: ;;</em></td></tr>
			</table><br>
			<table class="example">
			<tr class="headers"><td>Example</td></tr>
			<tr><td><code><pre>window.location = "_DO:cmd=go_to_tab&tab_id=49&c_name=foo&js=alert('Complete')";</pre></code></td>
			</tr>
			</table>
		</td>
	</tr>
	</table>

	<div id="addendum">
		<strong>Additional Notes</strong>
		<ul>
			<li>&lt;Component Name&gt; corresponds to the XML Component Configuration element given by the XPath /product/tabs/tab/areas/area/name</li>
			<li>&lt;Tab ID&gt; corresponds to the XML Component Configuration element given by the XPath /product/tabs/tab/id</li>
			<li>VALUES with optional plurals (i.e. &lt;Component Name(s)&gt;) can contain multiple values delimited by ; characters.  But all optional plural arguments in that _DO Command must contain the same level of additional values.</li>
			<li>It is a best-practice for all &lt;Javascript statement&gt; code to return 1</li>
		</ul>
	</div>

</div>

</div>

HTML;

?>

