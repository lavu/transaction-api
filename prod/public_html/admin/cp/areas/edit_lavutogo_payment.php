<?php
require_once(dirname(__FILE__)."/../resources/core_functions.php");
session_start($_GET['session_id']);
$data_name = sessvar('admin_dataname');
$data = lavu_query("select ltg_payment_info from poslavu_".$data_name."_db.locations ");
$display_values = mysqli_fetch_assoc($data);
$ltg_payment_info = $display_values['ltg_payment_info'];

if(isset($_POST['sub'])) {
	$gateway = $_POST['gateway'];
	$integration1 = $_POST['integration1'];
	$integration2 = $_POST['integration2'];
	$integration3 = $_POST['integration3'];
	$integration4 = $_POST['integration4'];
	$integration5 = $_POST['integration5'];
	$status = $_POST['status'];
	if($gateway == "paypal" || $gateway == "bridgepay") {
		$payment_info[$gateway] = array('integration1'=>$integration1,"integration2"=>$integration2,"integration3"=>$integration3,"integration4"=>$integration4,"integration5"=>$integration5,"status"=>$status);
		$paymentinfo = json_encode($payment_info);
		$query = "update locations set ltg_payment_info='$paymentinfo'";
		$result = lavu_query($query);
	}	
	$displayvalue .= "<script type='text/javascript'>window.parent.document.location.reload();</script>";
	echo  $displayvalue;
	exit;
}

$display .= "<link rel=\"stylesheet\" type=\"text/css\" href=\"../styles/styles.css\">
<link rel=\"stylesheet\" type=\"text/css\" href=\"../styles/info_window.css\">
<link rel=\"stylesheet\" type=\"text/css\" href=\"../styles/ieStyles.css\">";

$display .= "<form  id='myForm' method='post' action='edit_lavutogo_payment.php'>";
$display .= "<table cellspacing=0 align='center' border='5' style='width:95%;border:2px solid #DDDDDD;border-collapse: collapse;'>";
$display .= "<tr>";
$display .= "<td style='padding: 5px;'>Payment Gateway:</td>";
$display .= "<td style='padding: 5px;'><Select name='gateway' id='gateway' onchange='changecat(this.value)' style='width:85%;'>";
$display.= "<option value=''>Select Paymentgateway</option>";
//$display.= "<option value='bridgepay' selected>bridgepay</option>";
$display.= "<option value='paypal'>Paypal(Braintree)</option>";
$display.="</select>";
$display .= "</tr>";
$display .= "<tr>";
$display .= "<td style='padding: 5px;'>Integration data 1:</td>";
$display .= "<td style='padding: 5px;'><input type='text' placeholder='value' name='integration1' id='integration1' style='width:65%;'>";
$display .="</tr>";
$display .= "<tr>";
$display .= "<td style='padding: 5px;'>Integration data 2:</td>";
$display .= "<td style='padding: 5px;'><input type='text' placeholder='value' name='integration2'  id='integration2' style='width:65%;'>";
$display .= "</tr>";
$display .= "<tr>";
$display .="<td style='padding: 5px;'>Integration data 3:</td>";
$display .= "<td style='padding: 5px;'><input type='text' placeholder='value' name='integration3'  id='integration3' style='width:65%;'>";
$display .= "</tr>";
$display .="<tr>";
$display .= "<td style='padding: 5px;'>Integration data 4:</td>";
$display .= "<td style='padding: 5px;'><input type='text' placeholder='value'  name='integration4'  id='integration4'>";
$display .= "</tr>";
$display .= "<tr>";
$display .= "<td style='padding: 5px;'>Integration data 5:</td>";
$display .= "<td style='padding: 5px;'><input type='text' placeholder='value' name='integration5'  id='integration5'>";
$display .= "</tr>";

$display .= "<tr>";
$display .= "<td style='padding: 5px;'>Status:</td>";
$display .= "<td style='padding: 5px;'><input type='radio' placeholder='value'  value='1' name='status'  id='r1'>Enable
                     <input type='radio' placeholder='value' value='0' name='status'  id='r2'>Disable </td>";
$display .= "</tr>";
$display .= "<input type='hidden' name='sub' value = 1>";
$display.="<br>";
$display.="<br>";
$display .= "<tr align='center'>";
$display .= "</tr>";
$display .= "</table>";
$display .= "<br>";
$display .= "<div style='padding-top:5px;text-align:center;' ><input type='button' value='".speak("Save")."' class='saveBtn' onclick=\"submitMerchantForm()\" ></div>";
$display .= "</form>";
// Based on LP-8622 we have commented the line of code "echo create_info_layer(600,480);"
echo $display;

$displayvalue = "<table border='0' style='width:95%;border:2px solid #DDDDDD;margin:auto;'>";
if ($ltg_payment_info == '' || $ltg_payment_info == 'null') {
	$displayvalue.= "<tr style='text-align:center;'><td>No records found</td></tr>";
} else {
	$payment_info=json_decode($display_values['ltg_payment_info'],1);
	$displayvalue.= "<th>&nbsp; Payment Method &nbsp; </th>";
	$displayvalue.= "<th>&nbsp; Merchant ID   &nbsp; </th>";
	$displayvalue.= "<th>&nbsp; API Key - Public Key   &nbsp; </th>";
	$displayvalue.= "<th>&nbsp; API Key - Private Key   &nbsp; </th>";
	$displayvalue.= "<th>&nbsp; Status  &nbsp; </th>";
	if(isset($payment_info['paypal'])){
		if($payment_info['paypal']['status']==1){
			$status='Enable';
		}
		else {
			$status='Disable';
		}
		$displayvalue.="<tr>";
		$displayvalue.="<td align='center'>";
		$displayvalue.="Paypal(Braintree)";
		$displayvalue.="</td>";
		$displayvalue.="<td align='center'>";
		$displayvalue.=$payment_info['paypal']['integration1'];
		$displayvalue.="</td>";
		$displayvalue.="<td align='center'>";
		$displayvalue.=$payment_info['paypal']['integration2'];
		$displayvalue.="</td>";
		$displayvalue.="<td align='center'>";
		$displayvalue.=$payment_info['paypal']['integration3'];
		$displayvalue.="</td>";
		$displayvalue.="<td align='center'>";
		$displayvalue.=$status;
		$displayvalue.="</td>";
		$displayvalue.="</tr>";
	}
	if(isset($payment_info['bridgepay'])){
		if($payment_info['bridgepay']['status']==1){
			$status='Enable';
		}
		else {
			$status='Disable';
		}
		$displayvalue.="<tr>";
		$displayvalue.="<td align='center'>";
		$displayvalue.="Bridgepay";
		$displayvalue.="</td>";
		$displayvalue.="<td align='center'>";
		$displayvalue.=$payment_info['bridgepay']['integration1'];
		$displayvalue.="</td>";
		$displayvalue.="<td align='center'>";
		$displayvalue.=$payment_info['bridgepay']['integration2'];
		$displayvalue.="</td>";
		$displayvalue.="<td align='center'>";
		$displayvalue.=$payment_info['bridgepay']['integration3'];
		$displayvalue.="</td>";
		$displayvalue.="<td align='center'>";
		$displayvalue.=$status;
		$displayvalue.="</td>";
		$displayvalue.="</tr>";
	}
}
$displayvalue.= "</table>";
echo $displayvalue;

if ($ltg_payment_info == '' || $ltg_payment_info == 'null' ) {
	$ltg_payment_info = '{"paypal":{"integration1":"","integration2":"","integration3":"","integration4":"","integration5":"","status":"1"}}';
}

?>
<script type='text/javascript' src='//ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js'> </script>
<script>
document.getElementById('integration1').parentElement.parentElement.style.display='none';
document.getElementById('integration2').parentElement.parentElement.style.display='none';
document.getElementById('integration3').parentElement.parentElement.style.display='none';
document.getElementById('integration4').parentElement.parentElement.style.display='none';
document.getElementById('integration5').parentElement.parentElement.style.display='none';
document.getElementById('r1').parentElement.parentElement.style.display='none';
    function changecat(selvalue){
        var json = '<?php echo $ltg_payment_info; ?>';
        var newjson = JSON.parse(json);
        for(key in newjson){
            if(selvalue !='') {
                if(key.indexOf(selvalue) != -1){
                    var displayObj = newjson[key];
                    document.getElementById('integration1').parentElement.parentElement.removeAttribute("style");
                    document.getElementById('integration2').parentElement.parentElement.removeAttribute("style");
                    document.getElementById('integration3').parentElement.parentElement.removeAttribute("style");
                    document.getElementById('integration4').parentElement.parentElement.removeAttribute("style");
                    document.getElementById('integration5').parentElement.parentElement.removeAttribute("style");
                    document.getElementById('r1').parentElement.parentElement.removeAttribute("style");
                    document.getElementById('integration1').value=displayObj.integration1;
                    document.getElementById('integration2').value=displayObj.integration2;
                    document.getElementById('integration3').value=displayObj.integration3;
                    document.getElementById('integration4').value=displayObj.integration4;
                    document.getElementById('integration5').value=displayObj.integration5;
                    var cell = document.getElementsByTagName("td");
                    var i = 0;
                    while (cell[i] != undefined) {
                            switch (cell[i].innerHTML) {
                                    case 'Integration data 1:':
                                            cell[i].innerHTML = 'Merchant ID:';
                                            break;
                                    case 'Integration data 2:':
                                            cell[i].innerHTML = 'API Key - Public Key:';
                                            break;
                                    case 'Integration data 3:':
                                            cell[i].innerHTML = 'API Key - Private Key:';
                                            break;
                                    case 'Integration data 4:':
                                            cell[i].parentElement.style.display='none';
                                            break;
                                    case 'Integration data 5:':
                                            cell[i].parentElement.style.display='none';
                                            break;
                            }
                        i++;
                    }

					var rates=displayObj.status;
					if (rates == '1') {
						rate_value = document.getElementById('r1').checked='true';
					} else if (rates == '0') {
						rate_value = document.getElementById('r2').checked='false';
					} else {
                        rate_value = document.getElementById('r2').checked='';
                        rate_value = document.getElementById('r1').checked='';
                    }
                    break;
                }
			} else {
				document.getElementById('integration1').parentElement.parentElement.style.display='none';
				document.getElementById('integration2').parentElement.parentElement.style.display='none';
				document.getElementById('integration3').parentElement.parentElement.style.display='none';
				document.getElementById('integration4').parentElement.parentElement.style.display='none';
				document.getElementById('integration5').parentElement.parentElement.style.display='none';
				document.getElementById('r1').parentElement.parentElement.style.display='none';
                document.getElementById('integration1').value="";
                document.getElementById('integration2').value="";
                document.getElementById('integration3').value="";
                document.getElementById('integration4').value="";
                document.getElementById('integration5').value="";
                document.getElementById('r1').checked='';
                document.getElementById('r2').checked='';
            }
        }
    }

    function submitMerchantForm() {
    	var gateway = document.getElementById('gateway').value;
    	var integration1 = document.getElementById('integration1').value;
        var integration2 = document.getElementById('integration2').value;
        var integration3 = document.getElementById('integration3').value;
        var div = document.getElementsByClassName("modal_content")[2];
        var parentjQuery = function (selector) { return parent.jQuery(selector, div ); };
        if(gateway === 'paypal') {
	    	$.ajax({
	    		'url':'./ltgValidateMerchantDetailAjax.php',
	    		'type': 'POST',
	    		'data': {
	    			'integration1':integration1,
	    			'integration2':integration2,
	    			'integration3':integration3
	    		},
	    		'success' : function(data) {
	    			data = JSON.parse(data);
	    			if(data.status === 'failure') {
	    				parentjQuery("#modal_body").append('<div id ="varify"><div><h4>'+data.message+'</h4></div></div>');
	    				parentjQuery('#varify').dialog({
	    					resizable: false,
	    					height: 'auto',
	    					width:'30em',
	    					modal: true,
	    					title: 'Error',
	    					draggable: false,
	    					buttons : {
	    					    'OK': function() {
	    							parentjQuery('#varify').remove();
	    						}
	    					}
	    				});
	    			} else {
	    				$( "#myForm" ).submit();
	    				parentjQuery("#advanced_settings_modal_1")[0].style.display = 'none';
	    			}
	    		}
	    	});
        } else {
        	$( "#myForm" ).submit();
        	parentjQuery("#advanced_settings_modal_1")[0].style.display = 'none';
        }
    }
</script>