<script type="text/javascript" src="/manage/js/jquery/js/jquery-ui.min_1.9.2.js"></script>
<?php
if($in_lavu) {
    echo "<br><br>";
    global $data_name;
    $form_posted = (isset($_POST['posted']));
    if(!empty($_POST['supergrp_name'])){
    	lavu_query("INSERT INTO `super_groups` (`title`) VALUES ('[1]')",$_POST['supergrp_name']);
    	unset($_POST);
    	header('location:'.$_SERVER['REQUEST_URI']);
    }
    function getConfigForGLSetting($locationid){
    	$check_for_gl_update = lavu_query("SELECT `id`,`value` FROM `config` WHERE `setting` = 'general_ledger_property_number' AND `location` = '[1]'", $locationid);
	    $gl_info = array();
	    if (@mysqli_num_rows($check_for_gl_update)) {
	    	$gl_info = mysqli_fetch_assoc($check_for_gl_update);
	    }
	    return $gl_info;
    }
    
    if($form_posted) {
    	$gl_info = getConfigForGLSetting($locationid);
    	if(count($gl_info)>0){
    		lavu_query("UPDATE `config` SET `value` = '[2]' WHERE `id` = '[1]'", $gl_info['id'],$_POST['gl_property_number']);
    	} else lavu_query("INSERT INTO `config` (`location`, `type`, `setting`, `value`) VALUES ('[1]', 'setting', 'general_ledger_property_number', '[2]')", $locationid,$_POST['gl_property_number']);
    	 
    	$pos = 1;
    	foreach ($_POST['gl'] as $key=>$val){
    		if(strpos($key,'rowId')!==false && $val['type'] == 'super_group' && $val['name']!='')
    		{
    			lavu_query("INSERT INTO `general_ledger_settings` (`ref_id`,`account_number`,`description`,`type`,`created_date`,`position`,`transaction_type`) VALUES ('[1]','[2]','[3]','super_group','[4]','[5]','[6]')",$val['name'],$val['account'],$val['description'],date("Y-m-d H:i:s"),$pos,$val['transaction_type']);
    		}
    		elseif($val['type']=='super_group' && strpos($key,'rowId')=== false && $val['name']!='')
    		{
    			lavu_query("UPDATE `general_ledger_settings` SET `ref_id`=[2],`account_number` = '[3]',`description` = '[4]',`position` = '[5]',`transaction_type`='[6]' WHERE `id` = '[1]'", $key,$val['name'],$val['account'],$val['description'],$pos,$val['transaction_type']);
    		}
    		else
    		{
    			lavu_query("UPDATE `general_ledger_settings` SET `account_number` = '[2]',`description` = '[3]', `position` = '[4]',`transaction_type`='[5]' WHERE `id` = '[1]'", $key,$val['account'],$val['description'],$pos,$val['transaction_type']);
    		}
    
    		$pos++;
    	}
    	
    	if(!empty($_POST['post_deleted'])){
    		$arr_del = explode(',',$_POST['post_deleted']);
    		$ids = implode("', '",$arr_del);
    		lavu_query("UPDATE general_ledger_settings SET `_deleted` = '1' WHERE `id` in ('$ids')");
    	}
    	unset($_POST);
    	header('location:'.$_SERVER['REQUEST_URI']);
    }
    $sg_query = lavu_query("SELECT id,ref_id FROM general_ledger_settings where type='super_group' AND `_deleted`=0");
    while ($sg_read = mysqli_fetch_assoc($sg_query))
    {
        $gl_sg_id[] = $sg_read[ref_id];
    }
    $display = "";
    /**Super groups***/
    $supergrpdata_opt = lavu_query("SELECT id,title FROM super_groups where `_deleted`=0 and id not in(select ref_id from general_ledger_settings where `_deleted` = 0 AND `type` = 'super_group')");
    $joptions .="<option value=\"\">Select Super Group</option>";
    if(mysqli_num_rows($supergrpdata_opt)>0){
    	while ($jinfo = mysqli_fetch_assoc($supergrpdata_opt))
    	{
    		$joptions .= "<option value=".$jinfo['id'].">".$jinfo['title']."</otpion>";
    	}
    }
    $joptions .="<option value=\"0\">+ Add Super Group</option>";
    
    
    
    $supergrpdata = lavu_query("SELECT id,title FROM super_groups where `_deleted`=0");
    $options = "";
    $superGrp_arr = array();

    while ($sginfo = mysqli_fetch_assoc($supergrpdata))
    {
       $options .= "<option value=".$sginfo['id'].">".$sginfo['title']."</otpion>";
       $superGrp_arr[] = array("id"=>$sginfo['id'],"title"=>$sginfo['title']);
    }
    $options .="<option value=\"0\">+ Add Super Group</option>";
    
    $get_ref_id_query = lavu_query("SELECT `ref_id` FROM `general_ledger_settings` WHERE `_deleted` != '1' and type='super_group' order by position");
    while ($info_ref_id = mysqli_fetch_assoc($get_ref_id_query))
    {
    	$get_all_ref_id[] = $info_ref_id['ref_id'];
    }
    
    $get_gl_data = lavu_query("SELECT `id`, `lineitem_name`,`ref_id`,`account_number`,`description`,`type`,`transaction_type` FROM `general_ledger_settings` WHERE `_deleted` != '1' order by position");
    $tabledata = '';
    $gl_supergroup = array();
    while ($info = mysqli_fetch_assoc($get_gl_data))
    {
    	$rowId = $info['id'];
    	$transaction_option='';
    	$transaction_array=array( "debit" => "Debit","credit" => "Credit");
    	foreach($transaction_array as $key =>$t_array){
    		$transaction_selected = ($info['transaction_type'] == $key) ? "selected": "";
    		$transaction_option.="<option value=".$key." ".$transaction_selected.">$t_array</otpion>";
    	}
    	if($info['type']=='super_group'){
    		$gl_supergroup[] = $info['id'];
    		$newoptions = '<option value="">Select Super Group</option>';
               foreach ($superGrp_arr as $value)
    		{
    		    $selected = ($info['ref_id'] == $value['id']) ? "selected": "";
    		    if (in_array($value['id'], $gl_sg_id)==false){
                        $newoptions .= "<option value=".$value['id']." ".$selected.">".$value['title']."</otpion>";
           	     } else { 
    		           if ($info['ref_id']==$value['id']){
    		            $newoptions .= "<option value=".$value['id']." ".$selected.">".$value['title']."</otpion>";
    		        }
    		    }
    		}
                          
    		$newoptions .="<option value='0'>+ Add Super Group</option>";
    		
    		$tabledata .= "<tr>
    						<td class='move-td'> <img src='images/movebox.png' height='90%' width='17px'>&nbsp;<input type='hidden' name='icrow_1_1_order' id='icrow_1_1_order' value='0'></td>
    						<td><input type='hidden' name='gl[$rowId][type]' value='super_group'><input placeholder='Account Number' type='text' style='color: #1c591c;' name='gl[$rowId][account]' size='20' value='".htmlspecialchars($info['account_number'],ENT_QUOTES)."'></td>
    						<td><select class='super-group' name='gl[$rowId][name]'  style='width:170px;color: #1c591c;' onchange='superGroupCheck(this);'>$newoptions</select></td>
    						<td><input placeholder='Description' type='text' style='color: #1c591c;' name='gl[$rowId][description]' size='20' value='".htmlspecialchars($info['description'],ENT_QUOTES)."'></td>
    						<td> <select name='gl[$rowId][transaction_type]'> $transaction_option </select><td>
    						<td ><a class='closeBtn' onclick='click_delete_gl(this,$rowId)'>X</a></td>
    					</tr>";
    	}
    	else{
    		$tabledata .= "<tr>
    						<td width='24' height='18' class='move-td'> <img src='images/movebox.png' height='90%' width='17px'>&nbsp;<input type='hidden' name='icrow_1_1_order' id='icrow_1_1_order' value='0'></td>
    						<td><input type='hidden' name='gl[$rowId][type]' value='default'><input placeholder='Account Number' type='text' style='color: #1c591c;' name='gl[$rowId][account]' size='20' value='".htmlspecialchars($info['account_number'],ENT_QUOTES)."'></td>
    						<td><input type='text' name='gl[$rowId][lineItem]' readonly size='20' value=".$info['lineitem_name']." style='cursor:auto; color: #1c591c;'></td>
    						<td><input placeholder='Description' type='text' style='color: #1c591c;' name='gl[$rowId][description]' size='20' value='".htmlspecialchars($info['description'],ENT_QUOTES)."'></td>";
    						$not_eligible  = array("Deposits", "Reconciliation");
    						if(!in_array($info['lineitem_name'], $not_eligible)){
    							$tabledata .="<td colspan='2'> <select name='gl[$rowId][transaction_type]'> $transaction_option </select></select><td>";
    						}
    						$tabledata .= "</tr>";
    	}
    }
    $gl_info = getConfigForGLSetting($locationid);
    $gl_property_number = @$gl_info['value'];
    $display .= "<style>
    				.modal {
					    display: none; /* Hidden by default */
					    position: fixed; /* Stay in place */
					    z-index: 1; /* Sit on top */
					    left: 0;
					    top: 0;
					    width: 100%; /* Full width */
					    height: 100%; /* Full height */
					    overflow: auto; /* Enable scroll if needed */
					    background-color: rgb(0,0,0); /* Fallback color */
					    background-color: rgba(0,0,0,0.4); /* Black w/ opacity */
					}
					
					/* Modal Content/Box */
					.modal-content {
    					position: relative;
					    background-color: #fefefe;
					    margin: 15% auto; /* 15% from the top and centered */
					    padding: 20px;
					    border: 1px solid #888;
					    width: 30%; /* Could be more or less, depending on screen size */
					}
    				.close {
					    color: #aaa;
					    float: right;
					    font-size: 28px;
					    font-weight: bold;
					}
					
					.close:hover,
					.close:focus {
					    color: black;
					    text-decoration: none;
					    cursor: pointer;
					}
					.modal-header {
					    padding: 2px 16px;
    					float: right;
    					margin-top: -20px;
    					margin-right: -30px;
					}
					.close:hover,
					.close:focus {
					    color: black;
					    text-decoration: none;
					    cursor: pointer;
					}
    				</style>
    			<div id='supergroup_content_area' class='modal'>
    				<form name='sform' enctype=\"multipart/form-data\" method='post' action=''>
	    				<div class='modal-content'>
    						<div class='modal-header'>
							    <span class='close' id='close_gl_sg'>&times;</span> 
						  	</div>
		    				<table cellspacing='1' cellpadding='3'  style='border:solid 2px #aaaaaa;'>
		    					<tr>
			    					<td align='right' bgcolor='#eeeeee' style='padding-left:32px;width:250px;' >
			    						<div class='help_text_dimensional_form_setting_help_text_seperator_Lavu_ToGo_General_Settings' style='position:absolute;left:5px;'></div>
			    							Title:
			    					</td>
			    					<td align='left' valign='top' style='padding-right:32px'><input type='text' name='supergrp_name' size='30'>
			    					</td>
		    					</tr>
		    					<tr>
									<td colspan='2' align='center'>&nbsp;<input type='submit' value='".speak("Save")."' class='saveBtn'><br>&nbsp;</td>
								</tr>
		    				</table>
	    				</div>
    				</form>
    			</div>
    			<form name='gl_sform' id='gl_sform' enctype=\"multipart/form-data\" method='post' action=''><table cellspacing='1' cellpadding='3' width='40%' style='border:solid 2px #aaaaaa;'>
    				<tr>
    					<td colspan='2' bgcolor='#98b624' style='color:#ffffff; font-weight:bold; height:27px; padding:5px 0px 2px 0px;' align='center'>
    						<div class='divider' style='display:inline-block; width:400px; background-color:#98b624; margin:2px 0px 2px 0px; padding:10px 0px 10px 0px; color:white; font-size:12px; font-weight:bold;'> General Ledger Settings </div>
    					</td>
    				</tr>
    				
    					<input type='hidden' name='posted_dataname' value='$data_name'>
    					<input type='hidden' name='posted' value='1'>
    					<input type='hidden' id='post_deleted' name='post_deleted'>
    				<tr>
    					<td align='right' bgcolor='#eeeeee' style='padding-left:32px;width:250px;' >
    						<div class='help_text_dimensional_form_setting_help_text_seperator_Lavu_ToGo_General_Settings' style='position:absolute;left:5px;'></div>
    							Property Number:
    					</td>
    					<td align='left' valign='top' style='padding-right:32px'><input type='text' name='gl_property_number' size='30' value='".htmlspecialchars($gl_property_number,ENT_QUOTES)."'>
    					</td>
    				</tr>
    				<tr>
    					<td colspan='2' align='center'>
    						<table id='gl_tbl_browse_id' style='border:solid 1px black;width:90%' cellspacing='0' cellpadding='4'>
    							<thead>
    							<tr>
    								<td align='center' colspan='2' style='padding-left:4px; padding-right:4px; border-bottom:solid 1px black;' bgcolor='#cdcdcd'>Account Number<div style='position:relative;left:5px;display:inline-block;'></div></td>
    								<td align='center' style='padding-left:4px; padding-right:4px; border-bottom:solid 1px black;' bgcolor='#cdcdcd'>Line Item<div style='position:relative;left:5px;display:inline-block;'></div></td>
									<td align='center' style='padding-left:4px; padding-right:4px; border-bottom:solid 1px black;text-align:left' bgcolor='#cdcdcd'>Description<div style='position:relative;left:5px;display:inline-block;'></div></td>
								<td align='center'  colspan='3' style='padding-left:4px; padding-right:4px; border-bottom:solid 1px black;text-align:left' bgcolor='#cdcdcd'>Debit/Credit<div style='position:relative;left:5px;display:inline-block;'></div></td>
    							</tr>
    							</thead>
							<tbody>
								".$tabledata."
							</tbody>
						</table>
    					</td>
    				</tr>
					<tr>
						<td colspan='2' align='center'>
							<a style='cursor:pointer;' class='addNewBtn' onClick='addNewLineItem();'><span class='plus'> + </span> ".speak('Line Item')."</a>
						</td>
					</tr>
					<tr>
						<td colspan='2' align='center'>&nbsp;<input type='button' value='".speak("Save")."' class='saveBtn' onclick='validateForm();'><br>&nbsp;</td>
					</tr>
				</table>
				</form>
			<script>
				var rowId = 1;
				$(document).ready( function(){ 
				
				$( '#gl_tbl_browse_id tbody' ).sortable({
	      				revert: true,
						handle: '.move-td',
	    			});
	    			
	    			var modal = document.getElementById('supergroup_content_area');
					$('#close_gl_sg').click(function() {
					    modal.style.display = \"none\";
					});
				});
				
				function addNewLineItem(){
					var lineItemRow = 'rowId_'+( rowId++ );
					 var tabledata = '<tr><td class=\'move-td\'> <img src=\'images/movebox.png\' height=\'90%\' width=\'17px\'>&nbsp;<input type=\'hidden\' name=\'icrow_1_1_order\' id=\'icrow_1_1_order\' value=\'0\'></td><td><input type=\'hidden\' name=\'gl['+lineItemRow+'][type]\' value=\'super_group\'><input placeholder=\'Account Number\' type=\'text\' style=\'color: #1c591c;\' name=\'gl['+lineItemRow+'][account]\' size=\'20\'></td><td><select class=\'super-group\' name=\'gl['+lineItemRow+'][name]\'  style=\'width:170px;color: #1c591c;\' onchange=\'superGroupCheck(this)\'>$joptions</select></td><td><input placeholder=\'Description\' type=\'text\' style=\'color: #1c591c;\' name=\'gl['+lineItemRow+'][description]\'  size=\'20\'></td> <td> <select name=\'gl['+lineItemRow+'][transaction_type]\'> <option value=\'debit\'> Debit </option> <option value=\'credit\' > Credit  </option> </select><td><td ><a class=\'closeBtn\' onclick=\'click_delete_gl(this)\'>X</a></td></tr>';
					var lastrow = $('#gl_tbl_browse_id').find('tr').last();
					$(tabledata).insertAfter(lastrow);
				}
				function click_delete_gl(obj,rowid){
					obj.parentNode.parentNode.remove();
					if(document.getElementById('post_deleted').value == '')
					{
						document.getElementById('post_deleted').value = rowid;
					}
					else
					{
						document.getElementById('post_deleted').value += ','+rowid;
					}
				}
				function validateForm(){
				var flag = 0;
					$('#gl_sform .super-group').each(function(i){
							if($(this).val() == '' || $(this).val()<=0){
								
								flag = 1;
							}
						});
					if(flag==1){
						alert('Please select super group');
						return false;
					}
					else{
						$('#gl_sform')[0].submit();
					}
				}
				function superGroupCheck(ddl){
					if(ddl.value==0 || ddl.value==''){
						$('#supergroup_content_area').show();
						$(ddl).prop('selectedIndex', 0);
					}
					else{
						var selectedArr = new Array();
						$('#gl_sform .super-group').each(function(i){
							if(Number( selectedArr.indexOf($(this).val()) )>-1){
                                                        if($(this).val() == '') {
                                                            alert('Please select super group');
                                                        } else {
                                                            alert('Please check for duplicate super group'+$(this).val());
                                                        }
								$(ddl).prop('selectedIndex', 0);
								return false;
							}
                                                        else {
                                                           if($(this).val() != '') {
                                                                selectedArr.push($(this).val());
                                                           } 
                                                        } 
						});
					}
				}
			</script>";
	echo $display;
    echo "<br><br>";
}

?>
