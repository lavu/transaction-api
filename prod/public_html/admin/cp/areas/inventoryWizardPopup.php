
 <?php

// START POP UP
// Inventory 2.0 wizard pop up
echo "
<div id='inventoryModal2' class='hidden'>
    <div class='inventoryModalcontent'>
    <div class='closeBtnContainer'>
        <a id='inventoryModalBtn' class='closeBtn'>X</a>
    </div>
        <h1>Add some Inventory specific settings</h1>
        <p>Default values for all Inventory Settings have been provided.</p>
        <p>After adding any Units of Measure, Storage Locations, and Inventory Categories unique to your business, select Inventory from the navigation bar above to proceed.</p>
    </div>
</div>

<script type='text/javascript'>
(function () {
    migrationStatus();
    document.getElementById('inventoryModal2').onclick = function() {
        closeModalCheckAndUpdateStatus();
    }
})();

  function closeModalCheckAndUpdateStatus() {
    inventoryModal2.classList.add('hidden');
    // Update status to Ready
    checkAndUpdateStatus();
  };

  function migrationStatus() {
    $.ajax({
        url: '../api_entry_point/inventory/InventoryRequestRouter.php/inventory/v1.0/Config/get',
        data: JSON.stringify([{'setting' : 'INVENTORY_MIGRATION_STATUS'}]),
        type: 'post'
    }).done(function(response) {
        if (response.DataResponse.DataResponse[0]) {
            if (response.DataResponse.DataResponse[0].value === 'Pending' ||
                response.DataResponse.DataResponse[0].value === 'Skipped') {
                inventoryModal2.classList.remove('hidden');
            }
        }
    });
  };

  function checkAndUpdateStatus() {
    $.ajax({
        url: '../api_entry_point/inventory/InventoryRequestRouter.php/inventory/v1.0/Config/get',
        data: JSON.stringify([{'setting' : 'INVENTORY_MIGRATION_STATUS'}]),
        type: 'post'
    }).done(function(response) {
      if (response.DataResponse.DataResponse[0]) {
        if (response.DataResponse.DataResponse[0].value === 'Skipped') {
          updateSkippedStatus();
        }
        if (response.DataResponse.DataResponse[0].value === 'Pending') {
          updatePendingStatus();
        }
      }
    })
  };

  function updateSkippedStatus() {
      return $.ajax({
          url: '../api_entry_point/inventory/InventoryRequestRouter.php/inventory/v1.0/Config/update',
          data: JSON.stringify([{
              'setting': 'INVENTORY_MIGRATION_STATUS',
              'value': 'Skipped_Ready',
              'type': 'inventory_setting'
          }]),
          type: 'POST'
      });
  };

  function updatePendingStatus() {
      return $.ajax({
          url: '../api_entry_point/inventory/InventoryRequestRouter.php/inventory/v1.0/Config/update',
          data: JSON.stringify([{
              'setting': 'INVENTORY_MIGRATION_STATUS',
              'value': 'Pending_Ready',
              'type': 'inventory_setting'
          }]),
          type: 'POST'
      });
  };
</script>
"

// END POP UP
?>
