<?php
	if($in_lavu)
	{
		echo "<br><br>";

		$user_schedule = urlvar("user_schedule","");
		if($user_schedule!="")
		{
			require_once(dirname(__FILE__) . "/scheduling.php");
		}
		else
		{
			$tablename = "users";
			$forward_to = "index.php?mode={$section}_{$mode}";

			$allow_order_by = true;
			$order_by = urlvar("ob");
			if (!$order_by && sessvar_isset("users_order_by")) $order_by = sessvar("users_order_by");
			else if (!$order_by) $order_by = "l_name";
			set_sessvar("users_order_by", $order_by);

			$order_dir = urlvar("od");
			if (!$order_dir && sessvar_isset("users_order_dir")) $order_dir = sessvar("users_order_dir");
			else if (!$order_dir) $order_dir = "asc";
			set_sessvar("users_order_dir", $order_dir);

			$access_array = array();
			$access_array['1'] = "1";
			$access_array['2'] = "2";
			$access_array['3'] = "3";
			if (admin_info("access_level") == 4) {
				$access_array['4'] = "4";
			}

			$locations_array = array();
			$locations_array['0'] = speak("All Locations");
			$get_locations = lavu_query("SELECT `id`, `title` FROM `locations` WHERE `_disabled` != '1' ORDER BY `title` ASC");
			while($loc_info = mysqli_fetch_assoc($get_locations)) {
				$locations_array[$loc_info['id']] = $loc_info['title'];
			}

			$fields = array();
			$fields[] = array(speak("ID"),"id","readonly","","list:yes");
			$fields[] = array(speak("Active"),"active","bool","","list:yes,defaultvalue:1");
			$fields[] = array(speak("First Name"),"f_name","text","size:30","list:yes");
			$fields[] = array(speak("Last Name"),"l_name","text","size:30","list:yes");
			$fields[] = array(speak("Username"),"username","text","","list:yes");
			$fields[] = array(speak("Password"),"password","create_password","size:8");
			$fields[] = array(speak("Access Level"),"access_level","select",$access_array,"list:yes");

			if($cpcode=="lavukart")
			{
				$job_type_array = array();
				$job_type_array[""] = "";
				$get_job_lists_res = lavu_query("SELECT * FROM roles");
				//$job_array_index = 0;
				while ($get_job_lists_row = mysqli_fetch_assoc($get_job_lists_res)){
					$job_id = $get_job_lists_row['id'];
					$job_title = $get_job_lists_row['title'];
					$job_type_array[$job_id] = $job_title;
				}//while

				$fields[] = array(speak("Job Type"),"role_id","select",$job_type_array,"list:yes");
			}

			$fields[] = array(speak("PIN"),"PIN","text","","size:20");
			if ($location_info['allow_RFID_clock_punch'] == "1") $fields[] = array(speak("RF ID"),"rf_id","text","","size:20");
			$fields[] = array(speak("Service Type"),"service_type","select",array("0"=>speak("Location Default"),"2"=>speak("Quick Serve"),"3"=>speak("Tables"),"1"=>speak("Tabs")),"list:yes");
			if (admin_info("access_level") == 4) $fields[] = array(speak("Location"),"loc_id","select",$locations_array);

			if($modules->hasModule("settings.employee_classes")){
				$class_array = array();
				$class_type_array[""] = "";
				$class_query = lavu_query("SELECT * FROM emp_classes");
				//$job_array_index = 0;
				while ($class_read = mysqli_fetch_assoc($class_query)){
					$class_id = $class_read['id'];
					$class_title = $class_read['title'];
					$class_type_array[$class_id] = $class_title;
				}//while

				$fields[] = array(speak("Class"),"employee_class","select",$class_type_array,"list:yes");
			}

			if($modules->hasModule("employees.payrates")){
				$fields[] = array(speak("Pay Rate"),"payrate","number","","list:yes");
			}

			$fields[] = array("","","seperator");
			$fields[] = array(speak("Address"),"address","text","size:40");
			$fields[] = array(speak("Home Phone"),"phone","text");
			$fields[] = array(speak("Mobile Phone"),"mobile","text");
			$fields[] = array(speak("Email Address"),"email","text","size:30");
			$fields[] = array("","","seperator");
			$fields[] = array(speak("Schedule"),"user_schedule","browse_button");

			$fields[] = array(speak("Submit"),"submit","submit");

			function verify_user_main($action,$cc,$username,$email)
			{
				$set_cc = str_replace("'","''",$cc);
				$set_username = str_replace("'","''",$username);
				$rquery = mlavu_query("select * from `poslavu_MAIN_db`.`customer_accounts` where `dataname`='[1]'",$set_cc);
				if(mysqli_num_rows($rquery))
				{
					$rread = mysqli_fetch_assoc($rquery);
					$cid = $rread['id'];
					$verify_query = mlavu_query("select * from `poslavu_MAIN_db`.`customer_accounts` where `username`='[1]'",$set_username);
					if(mysqli_num_rows($verify_query))
					{
						if($action=="remove")
						{
							mlavu_query("delete from `poslavu_MAIN_db`.`customer_accounts` where `dataname`='[1]' and `username`='[2]'",$set_cc,$set_username);
						}
						if ($action=="update") {
							mlavu_query("UPDATE `poslavu_MAIN_db`.`customer_accounts` SET `email` = '[1]' WHERE `username` = '[2]'", $email, $set_username);
						}
					}
					else
					{
						if($action=="insert" || $action=="update")
						{
							$cinfo = array();
							$cinfo['dataname'] = $set_cc;
							$cinfo['restaurant'] = $cid;
							$cinfo['username'] = $set_username;
							$cinfo['email'] = $email;
							mlavu_query("insert into `poslavu_MAIN_db`.`customer_accounts` (`dataname`,`restaurant`,`username`,`email`) values ('[dataname]','[restaurant]','[username]','[email]')",$cinfo);
						}
					}
				}
			}

			$filter_by = "(`loc_id` = '0' OR `loc_id` = '$locationid' OR `access_level` = '4')";
			if (!is_lavu_admin()) $filter_by .= " AND `lavu_admin` != '1'";

			require_once(resource_path() . "/browse.php");
		}
	}
?>
