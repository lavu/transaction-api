<?php
echo $styles = <<<STYLES
<style>
  .tbl-qa {
   	width: 90%;
   	font-size: 1em;
   	border: solid 2px #aaaaaa;
   }

  .tbl-qa th.table-header {
   	padding: 5px;
   	text-align: left;
   	padding: 10px;
   	color: white;
   	background : #98B624 ;
   }

   .tbl-qa td.table-row  {
   	padding: 10px;
   	background-color: #FDFDFD;
   	display: table-cell;
     border: #aaaaaa 1px solid;
   }

   .ajax-action-button  img{
   	color: #09F;
   	margin: 10px 0px;
   	cursor: pointer;
   	display: inline-block;
   	padding: 10px 20px;
   }

   .button {
	background: #98B624;
	border-radius: 0px;
	color: white;
   }
</style>
STYLES;

function heredoc($param) {
    // return after language translation whatever has been passed to us
    return speak($param);
}
$heredoc = 'heredoc';

echo $htmlHead = <<<HTMLHEAD
<div class="ajax-action-button" id="add-more" onclick="createNew();">
<a href="javascript:void(0);" class="addNewBtn"><span class="plus"> + </span> Add New</a>
</div>
	<table class="tbl-qa">
		<thead>
			<tr>
				<th class="table-header">{$heredoc("Terminal Name")}</th>
				<th class="table-header">{$heredoc("Terminal ID")}</th>
				<th class="table-header">{$heredoc("Actions")}</th>
			</tr>
		</thead>
		<tbody id="table-body">
HTMLHEAD;


	$admin_database = sessvar('admin_database');
	$data_name = sessvar('admin_dataname');
	$admin_companyid = sessvar('admin_companyid');
	$admin_company_name = sessvar('admin_company_name');
	$location_info[] = sessvar('location_info');
	$location_id = '';
	if (!empty($location_info)) {
		$location_id = $location_info['id'];
	}

	if (empty($location_id)){
		header('Location: /cp/index.php');
		exit;
	}

	if (isset($_SERVER['HTTP_X_REQUESTED_WITH']) && !empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {

		$_request_mode = $_REQUEST["mode"];
		$_request_action = $_REQUEST["action"];
		$_request_method = $_SERVER["REQUEST_METHOD"];

		if( !empty($_request_mode)  &&  $_request_mode =='settings_terminal_configuration_settings') {

			if( !empty($_request_action)  &&  $_request_action=='add' &&  $_request_method === 'POST') {

				$terminal_name= $_REQUEST["terminal_name"];
				$terminal_id = $_REQUEST["terminal_id"];

				$insert_query = "INSERT INTO `location_terminals` ( `location_id`,`terminal_name`, `terminal_id`) VALUES ( '[1]', '[2]', '[3]')";
				$success = lavu_query($insert_query,  $location_id, $terminal_name, $terminal_id );

				if ($success) {
					echo $row_updated = lavu_insert_id();
				}
			}

		   else if( !empty($_request_action)  &&  $_request_action == 'delete' && $_request_method === 'POST' ) {

		        $id = $_REQUEST["id"];

		        $delete_query = "DELETE FROM  location_terminals  WHERE id = '[1]' ";
		        lavu_query($delete_query, $id);
		        echo  mysqli_affected_rows();
		    }

		    else if( !empty($_request_action)  &&  $_request_action == 'update' && $_request_method === 'POST' ) {

		    	$id = $_REQUEST["id"];
		    	$terminal_name= $_REQUEST["terminal_name"];

		    	$update_query = "UPDATE location_terminals set terminal_name='[1]' WHERE id = '[2]' ";
		    	lavu_query($update_query, $terminal_name, $id);
		    	echo  mysqli_affected_rows();
		    }


		}
	}


	$integrationKeyStr = integration_keystr();
	$queryStr = " SELECT AES_DECRYPT(`integration9`,'[1]') as data9, AES_DECRYPT(`integration10`,'[1]') as data10 FROM `locations` WHERE `id` = '[2]' and _disabled=0 ";
	$result = lavu_query($queryStr, $integrationKeyStr, $location_id);
	if(!$result){ error_log('Mysql error in '.__FILE__.' -sirur- error:'.mysql_error()); };
	$integration_data = mysqli_fetch_assoc($result);

    $terminal_query = lavu_query("SELECT * FROM location_terminals  WHERE location_id = '[1]' ", $location_id);
    while($terminal_reads =  mysqli_fetch_assoc($terminal_query)){
        $arrTerminals[] = $terminal_reads;
    }

	$default_setdate = date("Y-m-d");
	$batch_date = (isset($_REQUEST['date'])) ? $_REQUEST['date'] : $default_setdate;
	$start_datetime = date('Y-m-d', strtotime ( '-7 days', strtotime($batch_date)  ) ) . " 00:00:00";
	$end_datetime = $batch_date. " 23:59:59";
	$terminalTransactions = array();
	$totalTransactionCount = 0;
	$order_query = lavu_query("SELECT `cc`.`ref_data`, count(*) as transactions FROM `orders` as `or` JOIN `cc_transactions` as `cc` on `or`.`order_id` = `cc`.`order_id` WHERE `or`.`order_id` NOT LIKE 'Paid%' AND `or`.`order_id` NOT LIKE '777%' AND `or`.`closed` >= '$start_datetime' AND `or`.`closed` <= '$end_datetime' AND `cc`.`gateway` = 'eConduit' AND `cc`.pay_type_id = 2 AND `cc`.auth = 1 AND `or`.`void` = '0'  AND `or`.`location_id` = '" . $location_id . "' GROUP BY `cc`.`ref_data`");
	while($transactionsDetails =  mysqli_fetch_assoc($order_query)){
		$terminalTransactions[$transactionsDetails['ref_data']] = $transactionsDetails['transactions'];
		$totalTransactionCount += $transactionsDetails['transactions'];
    }

	if(!empty($arrTerminals)) {
	foreach($arrTerminals as $k=>$v) { ?>
	  <tr class="table-row" id="table-row-<?php echo $arrTerminals[$k]["id"]; ?>">
		<td style="width:50%"><input type="text" id="table-data-<?php echo $arrTerminals[$k]["id"]; ?>" value="<?php echo $arrTerminals[$k]["terminal_name"]; ?>" style="width:80%;height:20px">&nbsp;<span id="table-span-<?php echo $arrTerminals[$k]["id"]; ?>" style="width:20%"></span></td>
		<td><?php echo $arrTerminals[$k]["terminal_id"]; ?></td>
		<td><a style="cursor: pointer" onclick="deleteRecord(<?php echo $arrTerminals[$k]["id"]; ?>);"><?php echo speak("Delete")?></a>&nbsp;|&nbsp;<a style="cursor: pointer" onclick="unpairTerminal('<?php echo $arrTerminals[$k]["terminal_id"]; ?>',<?php echo $arrTerminals[$k]["id"]; ?>);" ><?php echo speak("Unpair") ?></a>&nbsp;|&nbsp;<a style="cursor: pointer" onclick="updateTerminal(<?php echo $arrTerminals[$k]["id"]; ?>);" ><?php echo speak("Update") ?></a>&nbsp;|
		<?php
		if (isset($terminalTransactions[$arrTerminals[$k]["terminal_id"]]) && $terminalTransactions[$arrTerminals[$k]["terminal_id"]] > 0) {?>
			<a style="cursor: pointer" onclick="window.open('/lib/batchSettlement.php?source=web&terminal_id=<?php echo $arrTerminals[$k]["terminal_id"];?>', '_parent');" ><?php echo speak("Perform-Batch") ?></a></td>
		<?php } else { ?>
			<a style="cursor: pointer" onclick="performBatch('<?php echo $arrTerminals[$k]["terminal_id"]; ?>');" ><?php echo speak("Perform-Batch") ?></a></td>
		<?php } ?>
	  </tr>

<?php
      }
	}
    else 
    {
?>
        <tr class="table-row">
                <td colspan=3 ><?php echo speak('There are no Terminals.') ?> </td>
        </tr>
<?php }
echo $htmlFoot = <<<HTMLFOOT
</tbody>
</table>
HTMLFOOT;

if (!empty($arrTerminals)) {
	if ($totalTransactionCount > 0) {
?>
		<div class="ajax-action-button" id="add-more" onclick="window.open('/lib/batchSettlement.php?source=web', '_parent');" >
		<a href="javascript:void(0);" class="addNewBtn"><?php echo speak("Group Batch") ?></a>
		</div>
<?php
	} else {
?>
		<div class="ajax-action-button" id="add-more" onclick="performGroupBatch();"><a href="javascript:void(0);" class="addNewBtn"><?php echo speak("Group Batch") ?></a></div>
<?php
	}
}
?>



<script language="javascript">

function performGroupBatch(){
    var key = '<?php echo $integration_data["data9"]; ?>';
    var password = '<?php echo $integration_data["data10"]; ?>';

    var all_terminals=<?php echo json_encode($arrTerminals, JSON_PRETTY_PRINT); ?>;
    var message='';
    for (i = 0; i < all_terminals.length; i++)
    {
           var targetURL = 'https://econduitapp.com/services/api.asmx/closeBatch?key='+key+'&password='+password+'&terminalId='+all_terminals[i]["terminal_id"]+'&refId=111&merchantId=123456';
            $.ajax({
                    url:targetURL, 
                    type: "GET",
                    success: function( response ) {
                    if(response.MessageCredit){
                            message=message.concat("Terminal ID: "+response.TerminalID+" - "+response.MessageCredit+"\n");
                     }},
                    async: false
            });
    }
    alert(message);
}       


function performBatch(terminalId){
        var key = '<?php echo $integration_data["data9"]; ?>';
        var password = '<?php echo $integration_data["data10"]; ?>';
        var input_data_url = 'key='+key+'&password='+password+'&terminalId='+terminalId+'&refId=111&merchantId=123456';
		var targetURL = "https://econduitapp.com/services/api.asmx/closeBatch?" + input_data_url;

		//var targetURL = 'https://beta.econduitapps.com/services/api.asmx/closeBatch?'+'key='+key+'&password='+password+'&terminalId='+terminalId+'&refId=111&merchantId=123456';

		$.get( targetURL, function( response ) {
			  if(response.MessageCredit){
				  alert("Terminal ID: "+response.TerminalID+" - "+response.MessageCredit);
			  }
		});
}



function updateTerminal(id) {
    var terminal_name = $("#table-data-"+id).val();

    var terminal_id = $("#terminal_id").val();
    if (terminal_name.length > 0) {
             $.ajax({
                  url: document.URL,
                  type: "POST",
                  data:'terminal_name='+terminal_name +'&id='+id +'&action=update',
                  success: function(data){
                      $("#table-span-"+id).text("Updated");
                      setTimeout(function() {  $("#table-span-"+id).fadeOut('fast');}, 2000);
                  }
             });
     }
     else
     {
             alert("Terminal Name can not be blank!");
                $("#table-data-"+id).focus();
     }
   }

	function unpairTerminal(terminalId,id)
	{
		var key = '<?php echo $integration_data["data9"]; ?>';
        var password = '<?php echo $integration_data["data10"]; ?>';
	    if(confirm("Are you sure you want to unpair this terminal?")) {
	    	var http = new XMLHttpRequest();
	    	var params = 'key='+key+'&password='+password+'&terminalId='+terminalId;
	    	http.open("POST", 'https://econduitapp.com/services/api.asmx/unPairTerminal', true);

	    	http.setRequestHeader("Content-type", "application/x-www-form-urlencoded");

	    	http.onreadystatechange = function() {

	        	if(http.readyState == 4 && http.status == 200) {
	            	//-------------delete on UnPair-----------
	            	$.ajax({
	            		url: document.URL,
	            		type: "POST",
	            		data:'id='+id +'&action=delete',
	            		success: function(data){
	            			location.reload(true);
	            		}
	            	//-------------delete on UnPair-----------
	        	    });
			    }
	    	}
	       	http.send(params);
		}
	}



	function createNew() {
         $("#add-more").hide();
         var data = '<tr class="table-row" id="new_row_ajax">' +
         '<td ><input class="cls_terminal_name" id="terminal_name" type="text"  placeholder="Terminal Name" /><\/td>' +
         '<td ><input class="cls_terminal_id" id="terminal_id" type="text"  placeholder="Terminal ID"/><\/td>' +
         '<td><span id="confirmAdd"><a onClick="addToDatabase()" class="ajax-action-links"><button class ="button">Save</button><\/a> / <a onclick="cancelAdd();" class="ajax-action-links"><button class ="button">Cancel</button><\/a><\/span><\/td>' +
         '<\/tr>';
       $("#table-body").append(data);
      }
    function cancelAdd() {
         $("#add-more").show();
         $("#new_row_ajax").remove();
      }
    function addToDatabase() {
       var terminal_name = $("#terminal_name").val();
       var terminal_id = $("#terminal_id").val();
       if (terminal_name.length > 0 && terminal_id.length > 0 ) {
	        $.ajax({
	             url: document.URL,
	             type: "POST",
	             data:'terminal_name='+terminal_name +'&terminal_id='+terminal_id +'&action=add',
	             success: function(data){
	               //alert(data);
	               location.reload(true);
	             }
	           });
    	}
    	else
    	{
    		alert("Terminal Name/Terminal ID can not be blank!");
    		if (terminal_name.length  < 1)
    			$("#terminal_name").focus();
    		else
    			$("#terminal_id").focus();

    	}
      }
    function deleteRecord(id) {
         if(confirm("Are you sure you want to delete this row?")) {
             $.ajax({
                 url: document.URL,
                 type: "POST",
                 data:'id='+id +'&action=delete',
                 success: function(data){
                 //alert(data);
                  location.reload(true);
                 }
             });
         }
      }
	$(document).on("keypress", "#table-body input[type='text']", function(e) {
		var keyCode = e.keyCode == 0 ? e.charCode : e.keyCode;
		var thisName = $(this).attr('id');
		if (thisName.indexOf("terminal_name") != -1) {
			$(this).attr('maxlength', 50);
			// allow backspace, tab, enter, spacebar, numbers, end, home, and alphabets ONLY
			return (keyCode == 8 || keyCode == 9 || keyCode == 13 || keyCode == 32 || keyCode == 35 || keyCode == 36 || (keyCode > 64 && keyCode < 91) || (keyCode > 96 && keyCode < 123));
		}
		if (thisName.indexOf("terminal_id") != -1) {
			$(this).attr('maxlength', 10);
			// allow backspace, tab, enter, dash, numbers, end, home, and keypad numbers ONLY
			return (keyCode == 8 || keyCode == 9 || keyCode == 13 || keyCode == 45 || keyCode == 35 || keyCode == 36 || (keyCode >= 48 && keyCode <= 57));
		}
	});

</script>