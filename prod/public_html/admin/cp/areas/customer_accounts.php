<?php
	if($in_lavu)
	{
		echo "<br><br>";
		
		$forward_to = "index.php?mode={$section}_{$mode}";
		$tablename = "customer_accounts";
	
		if (isset($_REQUEST['mass_import']) && ($_REQUEST['mass_import'] == "customer_accounts")) {

			display_header("Customers - Mass Import");
			echo "<br><br><a href='$forward_to'><< ".speak("Back to List")."</a><br><br>";

			require_once(resource_path()."/import.php");
	
			$import_area = import_area("customer_accounts");

		} else {
	
			$filter_by = "`locationid`='$locationid'";
			$search_mode = "combined";
			$search_terms = array("f_name","l_name","[f_name] [l_name]","email","employee_id","adp_file_no","rf_id");
			$per_page = 25;

			$allow_order_by = true;
			$order_by = urlvar("ob");
			if (!$order_by && sessvar_isset("cust_order_by")) $order_by = sessvar("cust_order_by");
			else if (!$order_by) $order_by = "l_name";
			set_sessvar("cust_order_by", $order_by);

			$order_dir = urlvar("od");
			if (!$order_dir && sessvar_isset("cust_order_dir")) $order_dir = sessvar("cust_order_dir");
			else if (!$order_dir) $order_dir = "asc";
			set_sessvar("cust_order_dir", $order_dir);
								
			$fields = array();
			$fields[] = array(speak("ID"),"id","readonly","","list:yes");
			$fields[] = array("loc_id","loc_id","hidden","","setvalue:$locationid");
			$fields[] = array(speak("First Name"),"f_name","text","size:30","list:yes");
			$fields[] = array(speak("Last Name"),"l_name","text","size:30","list:yes");
			$fields[] = array(speak("Email Address"),"email","text","size:30");
			if (strstr($location_info['show_restricted_report_ids'], "|71|")) {
				$fields[] = array(speak("Employee ID"),"employee_id","text","","list:yes");
				$fields[] = array(speak("ADP File #"),"adp_file_no","text","","list:yes");
				$fields[] = array(speak("RF ID"),"rf_id","text");
			}
			$fields[] = array("","","seperator");
			$fields[] = array(speak("Save"),"submit","submit");
			
			$filter_by = "(`loc_id` = '0' OR `loc_id` = '$locationid')";
	
			$option_links = "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href='$forward_to&mass_import=customer_accounts'><img src='images/mass_import.png' border='0'/></a>";
			
			require_once(resource_path() . "/browse.php");
		}
	}
?>
