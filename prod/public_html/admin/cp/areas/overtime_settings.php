<?php

	require_once(dirname(__FILE__)."/../resources/ck_functions.php");

	$holidays = array();
	$holidays[] = array("New Year's Day","new_years_day");
	$holidays[] = array("Labor Day","labor_day");
	$holidays[] = array("Martin Luther King Day","martin_luther_king_day");
	$holidays[] = array("Columbus Day","columbus_day");
	$holidays[] = array("President's Day","presidents_day");
	$holidays[] = array("Veterans Day","veterans_day");
	$holidays[] = array("Easter","easter");
	$holidays[] = array("Thanksgiving","thanksgiving");
	$holidays[] = array("Memorial Day","memorial_day");
	$holidays[] = array("Christmas","christmas");
	$holidays[] = array("Independence Day","independence_day");
	
	if(isset($_POST['mySubmit']))
	{
		$selectTimeParam = $_POST['selectTimeParam'];
		
		$OTHours = $_POST['OTHours'];
		$DTHours = $_POST['DTHours'];
		
		$daySelected = $_POST['StartDayOfWorkWeek'];
		
		$overtimeCriticalValueDaysPerWeek = $_POST['OverTimeCriticalValue'];
		$hoursThisWeek = $_POST['hoursThisWeek'];
		$ot_california_setting = $_POST['ot_california_setting'];
		
		$locationid = sessvar("locationid");
		
		/*
		echo "OTCritVal: $overtimeCriticalValue<br />
					hoursthisweek: $hoursThisWeek<br /><br />
					daySelected: $daySelected<br />
					";
	*/
		//echo "id: $locationid<br>";
		
		//echo "clicked submit";
	
		$searchForType = "overtime_setting_day2";
		$result = lavu_query("SELECT * FROM `config` WHERE `location`=\"[1]\" AND type=\"[2]\"",$locationid, $searchForType);
	
		$row = mysqli_fetch_assoc($result);
		$type = $row['type'];
		//echo "type $type<br />"; 
		////echo "OT: ";  echo $row['value'];
	
	
	/*
		value = day (Over this many hours per day is considered overtime)
		value2 = day work week starts (Monday, Tuesday, etc...)
		value3 = overtime critical value, days per week (over how many days per week is considered overtime)
		value4 = hours this week (over how many hours per week is considered overtime?)
	*/
	
	/* If row does not exist */
		if (!$type){
			//die("The overtime setting does not exist.");
			$result = lavu_query("INSERT INTO config (location,type, value) VALUES(\"[1]\", \"[2]\", \"null\")", $locationid, $searchForType);
			$type = "overtime_setting_day2";
		}//if
	
	/* If row does exist */
		if ($type) {
		
			$ot_holidays = array();
			foreach ($holidays as $holiday) {
				if (isset($_POST['ot_'.$holiday[1]])) $ot_holidays[] = $holiday[1];
			}

			$dt_holidays = array();
			foreach ($holidays as $holiday) {
				if (isset($_POST['dt_'.$holiday[1]])) $dt_holidays[] = $holiday[1];
			}

			$obey_day_start_end = isset( $_POST['obeyDayStartEnd'] ) ? $_POST['obeyDayStartEnd']  : 'midnight';
		
			$vars = array();
			$q_update = "";
			$vars['value'] = $OTHours;
			$vars['value2'] = $daySelected;
			$vars['value3'] = $overtimeCriticalValueDaysPerWeek;
			$vars['value4'] = $hoursThisWeek;
			$vars['value5'] = $DTHours;
			$vars['value6'] = $obey_day_start_end;
			$vars['value_long'] = join("|", $ot_holidays);
			$vars['value_long2'] = join("|", $dt_holidays);
			$vars['value15'] = $ot_california_setting;	// LP-2481 update config table for ot_california_setting by using field 'value15' to store the value
			$keys = array_keys($vars);
			foreach ($keys as $key) {
				if ($q_update != "") $q_update .= ", ";
				$q_update .= "`$key` = '[$key]'";
			}
			$vars['location'] = $locationid;
			$vars['type'] = $searchForType;
			lavu_query("UPDATE `config` SET $q_update WHERE `location` = '[location]' AND `type` = '[type]'", $vars);
			//print_r($vars);exit;
		}//if
	
		while ($row = mysqli_fetch_assoc($result)) {
				 echo $row["setting"];
		}//while
		
		header('Location: ?mode=settings_overtime_settings');
	}
	else
	{

		$searchForType = "overtime_setting_day2";
		$getOTSettingsResults = query_db_ck('poslavu_DEV_db', "SELECT * FROM `config` WHERE `location`='$locationid' AND type='$searchForType'");
		$getOTSettingsRow = mysqli_fetch_assoc($getOTSettingsResults);
		
		$ot_holidays = explode("|", $getOTSettingsRow['value_long']);
		$dt_holidays = explode("|", $getOTSettingsRow['value_long2']); 
?>

<style type="text/css">

.leftForm{
		float:left;
		width:250px;
		text-align:right;
		border:0px dotted #000;
		padding-top:4px;
}

.rightForm{
		float:left;
		border:0px dotted #ccc;
		text-align:right;
}

.breakLine{
	clear:both;
	height:3px;
	margin-bottom:3px;
}

fieldset{
	padding-top:25px;
	margin-top:15px;
}

</style>


<script type="text/javascript">
	
	/* These functions pertain to the "Overtime settings" tab under "Location Settings" */
	
	function submitForm(){
		/*This form submits the user inputted conditions for the overtime settings. */
		
		var canSubmitForm = validateOTSettings();
		
		alert("submit form");
		
		document.forms["OverTimeSettingsForm"].submit();
		alert("Still Workds");
	}
	
	
	
</script>


<center>
<div style="width:600px">
<form name="OverTimeSettingsForm" id="OverTimeSettingsForm" method="POST" action="?<?php echo "mode={$section}_{$mode}"; ?>">
	<fieldset>
		<legend style="font-weight:bold; padding:0 5px 0 5px; color:#333;"><?php echo speak('Overtime Settings');?></legend>
		
		<div class="leftForm">
			<label>Work week starts on&nbsp;</label>
		</div>
		<div class="rightForm">
			<select name="StartDayOfWorkWeek">
				<?php
					$workWeekStartVal = $getOTSettingsRow['value2'];
					$weekDaysForListMenu = array("Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday");
					for ($i=0; $i<count($weekDaysForListMenu);$i++) {
						$insertWeekday = $weekDaysForListMenu[$i];
						if ($workWeekStartVal == $i) echo "<option value=\"$i\" selected=\"selected\">$insertWeekday</option>";
						if ($workWeekStartVal != $i) echo "<option value=\"$i\">$insertWeekday</option>";
					}
				?>
			</select>
		</div><!--rightForm-->
		<div class="breakLine"></div>
		
		<div class="leftForm">
			<label>Over&nbsp;</label>
		</div>
		<div class="rightForm">
			<select name="OTHours">
				<option value="DNA">Does Not Apply</option>
				<?php
					$setOTHours = $getOTSettingsRow['value'];
					for ($i=1; $i<=12;$i++) {
						if ($setOTHours != $i) echo "<option value=\"$i\"> $i </option>";
						if ($setOTHours == $i) echo "<option value=\"$i\" selected=\"selected\"> $i </option>";
					}
				?>	
			</select>
			<label>hours/day</label>						
		</div><!--rightForm-->
		<div class="breakLine"></div>
		
		
		<div class="leftForm">			
			<label>More than&nbsp;</label>
		</div>
		<div class="rightForm">
			<select name="OverTimeCriticalValue">
				<option value="DNA">Does Not Apply</option>
				<?php		
					$setDaysPerWeekVal = $getOTSettingsRow['value3'];
					for ($i=2;$i<=7;$i++) {
						echo "<option value=\""; echo $i; echo "\"";
						if ($i == $setDaysPerWeekVal) echo "selected=\"selected\"";
						echo ">"; echo $i; echo "</option>";
					}
				?>
			</select>
			<label>days/week &nbsp;</label>
		</div>
		<div class="breakLine"></div>
		
		<div class="leftForm">
			<label>More than&nbsp;</label>
		</div>	
		<div class="rightForm">       
			<select name="hoursThisWeek">
				<option value="DNA">Does Not Apply</option>
				<?php
					$setHoursPerWeekVal = $getOTSettingsRow['value4'];
					for ($i=10; $i<=60;$i++) {
						if ($setHoursPerWeekVal != $i) echo "<option value=\"$i\"> $i </option>";
						if ($setHoursPerWeekVal == $i) echo "<option value=\"$i\" selected=\"selected\"> $i </option>";
					}
				?>
			</select>
			<label>hours/week</label>
		</div><!--rightForm-->
		<div class="breakLine"></div>
		<div class="leftForm">
			<label>Calculate Day Rollover&nbsp;</label>
		</div>
		<div class="rightForm">
			<select name="obeyDayStartEnd">
				<?php
					$useDayStartEnd = $getOTSettingsRow['value6'];
				if( $useDayStartEnd == 'midnight' || empty( $useDayStartEnd ) ){
					echo '<option selected value="midnight">Use Midnight</option><option value="daystartend">Use Day Start/End</option>';
				} else if( $useDayStartEnd == 'daystartend' ){
					echo '<option value="midnight">Use Midnight</option><option selected value="daystartend">Use Day Start/End</option>';
				}
				?>
			</select>
			</div>
			<!-- start LP-2481 -->
				<div class="breakLine"></div>
				<div class="breakLine"></div><?php if($getOTSettingsRow['value15']==1){$otstatus="checked";}else{$otstatus="";} ?>
					<input type='checkbox' style ='vertical-align: middle' name='ot_california_setting' value='1' <?php echo $otstatus; ?> > <?php echo speak('Exclude OT hours from regular hours worked');?>
				<div class="breakLine"></div>
				<div class="breakLine"></div>
			<!-- end LP-2481 -->
		<div class="breakLine"></div>
		<div class="breakLine"></div>
		
		<table width='100%'>
			<tr>
				<td align='center'>
					<table width='95%' cellpadding='5px'>
						<tr><td align='center'>Automatically apply overtime for these holidays:</td></tr>
						<tr>
							<td align='center'>
								<table cellspacing='0' cellpadding='3'>
									<?php
										$count = 0;
										foreach ($holidays as $holiday) {
											if (($count % 2) == 0) echo "<tr>";
											$checked = "";
											if (in_array($holiday[1], $ot_holidays)) $checked = " CHECKED";
											echo "<td align='right'><input type='checkbox' name='ot_".$holiday[1]."' value='1'".$checked."></td><td align='left'>".$holiday[0]."</td>";
											$count++;
											if (($count % 2) == 0) echo "</tr>";
											else echo "<td>&nbsp;&nbsp;&nbsp;</td>";
										}
									?>
								</table>
							</td>
						</tr>
					</table>
				</td>
			</tr>
		</table>
		<div class="breakLine"></div>
		<div class="breakLine"></div>
		
	</fieldset>
	<fieldset>
		<legend style="font-weight:bold; padding:0 5px 0 5px; color:#333;"><?php echo speak('Double-time Settings');?></legend>

		<div class="leftForm">
			<label>Over&nbsp;</label>
		</div>
		<div class="rightForm">
			<select name="DTHours">
				<option value="DNA">Does Not Apply</option>
				<?php
					$setDTHours = $getOTSettingsRow['value5'];
					for ($i=1; $i<=23;$i++) {			
						if ($setDTHours != $i) echo "<option value=\"$i\"> $i </option>";
						if ($setDTHours == $i) echo "<option value=\"$i\" selected=\"selected\"> $i </option>";
					}
				?>
			</select>			
			<label>hours/day</label>						
		</div><!--rightForm-->
		<div class="breakLine"></div>
		<div class="breakLine"></div>
		
		<table width='100%'>
			<tr>
				<td align='center'>
					<table width='95%' cellpadding='5px'>
						<tr><td align='center'>Automatically apply double-time for these holidays:</td></tr>
						<tr>
							<td align='center'>
								<table cellspacing='0' cellpadding='3'>
									<?php
										$count = 0;
										foreach ($holidays as $holiday) {
											if (($count % 2) == 0) echo "<tr>";
											$checked = "";
											if (in_array($holiday[1], $dt_holidays)) $checked = " CHECKED";
											echo "<td align='right'><input type='checkbox' name='dt_".$holiday[1]."' value='1'".$checked."></td><td align='left'>".$holiday[0]."</td>";
											$count++;
											if (($count % 2) == 0) echo "</tr>";
											else echo "<td>&nbsp;&nbsp;&nbsp;</td>";
										}
									?>
								</table>
							</td>
						</tr>
					</table>
				</td>
			</tr>
		</table>
		<div class="breakLine"></div>
		<div class="breakLine"></div>
		
	</fieldset>
	<br>
	<input type="submit" name="mySubmit" value="<?php echo speak('Save'); ?>" id="mySubmit" />
</form>
</div>
</center>
<?php
	}
?>