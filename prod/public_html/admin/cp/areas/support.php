<?php
	require_once(dirname(__FILE__)."/../../manage/globals/email_addresses.php");
	global $o_emailAddressesMap;

    $not_so_secret_salt = "zenlavu";
	$from_poslavu_com = !empty( $_POST["message"] ) && !empty( $_POST['submission_from_poslavu_com'] );
	$secret = "";
	if ( $from_poslavu_com ){
		$secret = md5( $_POST['message'] . $not_so_secret_salt );
	}
	//error_log("-------------------v2/areas/support.php checking error_log");
	if (isset($_POST['submit_ticket_submission']) && ( $loggedin_id || ( $from_poslavu_com && $secret == $_POST['submission_from_poslavu_com'] ) ) ) {
		// TODO : add logic to enable tickets coming from lavu.com or poslavu.com
		require_once(dirname(__FILE__).'/../resources/contact_zendesk.php');
		//error_log("mmmmmmmmmm  ".ZDURL);
		require_once(dirname(__FILE__).'/../resources/json.php');
		//$json_object = new LavuJson();

		// check the email address
		if (trim($_POST['email']) == "") {
			echo "error|*note*|Please provide an email address.";
			exit();
		}
		if (trim($_POST['name']) == "") {
			echo "error|*note*|Please provide a name.";
			exit();
		}
		if (trim($_POST['company']) == "") {
			echo "error|*note*|Please provide a company name.";
			exit();
		}
		$a_email_parts = explode("@", $_POST['email']);
		if (count($a_email_parts) != 2 || strpos($a_email_parts[1],".") === FALSE) {
			echo "error|*note*|Improper email address provided.";
			exit();
		}

		// the ids for these come from the zendesk CP backend
		$phone_ext = ($_POST['phone_ext']!='') ? $_POST['phone_ext'] : '';
		$custom = array(
			ZENDESK::get_customer_field_to_submit('Phone Number', preg_replace('/[^\d]*/i', '', $_POST['phone'].$phone_ext)),
			ZENDESK::get_customer_field_to_submit('Customer Name', $_POST['name']),
			ZENDESK::get_customer_field_to_submit('Location Name', $_POST['company']),
			ZENDESK::get_customer_field_to_submit('Data Name', $_POST['dataname'])
		);
		if($custom['dataname']) {
            $tags = array(
                "website_contact_form_".$_POST['group'],
            );
        }
		// there are triggers in the zendesk CP that assign the ticket to a group
		error_log('=============poslavu.com contact GROUP=='.$_POST['group']);
        $group_id = 21130553; //Specialist group
	if($_POST['group'] == 'sales'){
		$email_lavu_dept = "sales@lavuinc.com";
		error_log('=============poslavu.com contact:: SALES');
        $group_id = 20755877;
	}else if($_POST['group'] == 'support'){
		$email_lavu_dept = "pos.support@lavuinc.com";
		error_log('=============poslavu.com contact:: SUPPORT');
        $group_id =  20739892;
	}else{
		$email_lavu_dept = "billing@lavuinc.com";
		error_log('=============poslavu.com contact:: BILLING');
        $group_id = 20753913;
	}

		$tags_string = LavuJson::json_encode($tags);
		$delimited_tags = is_array($tags)?implode('","',$tags):"";
		$tags_obj_string = '["'.$delimited_tags.'"]';

		$arr = array(
			"z_subject"=>$_POST['subject'],
			"z_description"=>trim( preg_replace('/\s+/', ' ', $_POST['message'] ) ),
			"z_recipient"=>$email_lavu_dept,
			"z_name"=>$_POST['name'],
			"z_requester"=>$_POST['email'],
		);

		$create_arr = array(
			'ticket' => array(
				'subject' => $arr['z_subject'],
				'comment' => array(
					'body' => $arr['z_description'],
				),
				'requester' => array(
					'name' => $arr['z_name'],
					'email' => $arr['z_requester']
				),
				'status' => 'new',
				'custom_fields' => $custom,
				'tags' => $tags,
                "group_id" => $group_id
			),
		);
		$create = LavuJson::json_encode($create_arr);//json_encode($create_arr, JSON_FORCE_OBJECT);
		$create = str_replace($tags_string, $tags_obj_string, $create);
		//error_log("support.php create==".$create);
		$error = array();
		$curl_info = array();
		$decoded = array();
		$success = curlWrap("/tickets.json", $create, "POST", $error, $curl_info, $decoded);

		// check if jquery was loaded by the time the page was ready (the form would finish submitting, otherwise, and the page should be loaded)
		if (!isset($_POST['jquery_failed']) || $_POST['jquery_failed'] != 'failed') {
			if ($success) {
				echo "success|*note*|"."Your ticket has been submitted. Please check your email for a response.";
			} else {
				if ((int)$curl_info['http_code'] == 429) {
					echo "error|*note*|".speak("Too many requests have been made in the last minute. Please retry sending your ticket again in")." <div style='display:inline;' id='counter'>60</div> ".speak("seconds").".|*command*|script|*note*|function count() { document.getElementById('counter').innerHTML = Math.max(parseInt(document.getElementById('counter').innerHTML)-1,0); setTimeout('count();', 1000); } count();";
				} else {
					//error_log("error in zendesk submission from ".__FILE__, print_r($error,TRUE)."\n".print_r($curl_info,TRUE)."\n".print_r($decoded,TRUE)."\n".$_SERVER['REMOTE_ADDR']);
					//$o_emailAddressesMap->sendEmailToAddress("zendesk_ticket_errors@poslavu.com", "error in zendesk submission from ".__FILE__, print_r($error,TRUE)."\n".print_r($curl_info,TRUE)."\n".print_r($decoded,TRUE)."\n".$_SERVER['REMOTE_ADDR']);
					mail("test@lavu.com", "error in zendesk submission from ".__FILE__,"Error: ".print_r($error,TRUE)."\n\nCurl Info:\n" .print_r($curl_info,TRUE)."\n\n Decoded:\n".print_r($decoded,TRUE)."\n\nServer Remote Address:\n".$_SERVER['REMOTE_ADDR']);
					echo "error|*note*|Failed to submit.";
				}
			}
			exit();
		}
	}

	$company_name = '';
	$comp_query = mlavu_query("SELECT `company_name` FROM `poslavu_MAIN_db`.`restaurants` WHERE `data_name`='[dataname]'",
		array('dataname'=>$data_name));
	if ($comp_query !== FALSE && mysqli_num_rows($comp_query) > 0) {
		$comp_read = mysqli_fetch_assoc($comp_query);
		$company_name = $comp_read['company_name'];
	}
?>

<script type="text/javascript" src="//asset0.zendesk.com/external/zenbox/v2.1/zenbox.js"></script>
<script type="text/javascript" src="/manage/js/jquery/js/jquery-1.9.0.js"></script>
<script type="text/javascript" src="/manage/js/jquery/js/jquery.validate.min.js"></script>
<style type="text/css" media="screen, projection">
  @import url(//asset0.zendesk.com/external/zenbox/v2.1/zenbox.css);
  @import url('/subdomains/support/styles/main.css');
</style>





<table width="530" id="maintext" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td class="sectiontitle">Lavu Support</td>
  </tr>
  <tr>
    <td ><p>How can we help you?</p>
      <p>Most questions you may have can be answered by reviewing our Tutorial videos and resources. If you have not used the <a href="http://admin.poslavu.com/cp/index.php?mode=guide_start">Setup Guide</a>, you should do so now. It is available at the top right on the main menu.</p>

    <p>&nbsp;</p></td>
  </tr>
</table>
<table width="530" border="0" cellpadding="10" cellspacing="0" id="maintext">
  <tr>
    <td width="135" align="right" bgcolor="#999999" class="whitetitle_small">EMAIL:</td>
    <td width="355" bgcolor="#EFEFEF"><a href="mailto:support@poslavu.com?Subject=POSLavu Support">support@poslavu.com</a></td>
  </tr>
  <tr>
    <td align="right" bgcolor="#999999" class="whitetitle_small">PHONE:</td>
    <td align="center" valign="middle" bgcolor="#EFEFEF"><img src="images/phone_lavu_new.png" alt="Lavu phone number 855 528 8457" width="210" height="44" /><br /><br />
    All International calls: 480-788-5288</td>
  </tr>
  <tr>
    <td align="right" bgcolor="#999999" class="whitetitle_small">SUPPORT TICKETS:</td>
    <td bgcolor="#EFEFEF"><a href="http://support.lavu.com">support.lavu.com</a></td>
  </tr>

  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
</table>



<!-- Support Ticket Form -->
<?php

$a_departments = array(
	'support'=>array('display'=>'Support', 'selected'=>(isset($_GET['department']) && $_GET['department']=='support')),
	'billing'=>array('display'=>'Billing', 'selected'=>(isset($_GET['department']) && $_GET['department']=='billing')),
);
global $comPackage;
if ($comPackage == 'lavukart')
	$a_departments['kart_support'] = array('display'=>'Lavu Kart', 'selected'=>(isset($_GET['department'])) && $_GET['department']='lavu_kart');
echo draw_tickets($data_name, $info, $company_name, $a_departments);

function draw_tickets($data_name, &$info, $company_name, &$a_departments) {
	$name = admin_info('fullname');
	$s_display_name = ($name != '') ?
		'<input type="hidden" name="name" value="'.$name.'">'.$name :
		'<input type="text" name="name" size="25" value="'.$info['name'].'" class="clear">';
	$s_display_email = '<input type="text" name="email" size="25" value="" class="clear">';
	$s_display_company_name = ($company_name !== '' && $data_name !== '') ?
		'<input type="hidden" name="company" size="25" value="'.$company_name.'">'.$company_name :
		'<input type="text" name="company" size="25" value="'.$info['company_name'].'" class="clear">';

	$s_display_options = '';
	foreach($a_departments as $k=>$v) {
		$display = $v['display'];
		$selected = ($v['selected']) ? 'SELECTED' : '';
		$s_display_options .= '
					<option value="'.$k.'" '.$selected.'>'.$display.'</option>';
	}

	$s_retval = '';
	$s_retval .= '
	<form action="#" method="POST" enctype="multipart/form-data" id="form_ticket_submit">
	<table cellpadding="2" cellspacing="1" width="530">
		<tr>
		    <td class="sectiontitle" colspan="2">Ticket System</td>
		 </tr>
		 <tr>
		    <td colspan="2"><p>Please fill in the form below to open a new ticket.</p></td>
		  </tr>
	    <tr>
	        <td class="header">Full Name:</td>
	        <td>
	        	'.$s_display_name.'
		        <br /><label for="name" class="error"></label>
	        </td>
	    </tr>
	    <tr>
	        <td class="header">Email Address:</td>
	        <td>
	        	'.$s_display_email.'
				<br /><label for="email" class="error textarea"></label>
	        </td>
	    </tr>
	    <tr>
	        <td class="header">Company (Account):</td>
	        <td>
	        	'.$s_display_company_name.'
		        <br /><label for="company" class="error"></label>
	        </td>
	    </tr>
	    <tr>
	        <td class="header">Telephone:</td>
	        <td>
	        	<input type="text" name="phone" size="25" value="'.$info['phone'].'" onblur="removeNonMatching(this, /[0-9]/g);" class="clear">
	             &nbsp;Ext&nbsp;
	             <input type="text" name="phone_ext" size="6" value="'.$info['phone_ext'].'" onblur="removeNonMatching(this, /[0-9]/g);" class="clear">
		        <br /><label for="phone" class="error"></label>
		        <label for="phone_ext" class="error"></label>
	       </td>
	    </tr>
	    <tr>
	    	<td class="header">Department:</td>
	    	<td>
	    		<select name="group">
					<option value="">Choose One</option>
					'.$s_display_options.'
	    		</select>
				<font>&nbsp;*</font>
		        <br /><label for="group" class="error"></label>
	    	</td>
	    </tr>
		<tr>
			<td class="header">Subject:</td>
			<td>
				<input type="text" name="subject" size="35" value="'.$info['subject'].'" class="clear">
				<font>&nbsp;*</font>
		        <br /><label for="subject" class="error"></label>
			</td>
		</tr>
		<tr>
			<td class="header">Message:</td>
			<td>
				<span>
					<textarea name="message" cols="35" rows="8" wrap="soft" style="width:85%; float:left;" class="clear">'.$info['message'].'</textarea>
					<font>&nbsp;*</font>
			        <br /><label for="message" class="error textarea"></label>
				</span>
			</td>
		</tr>
	    <tr>
	    	<td></td>
	    	<td>
	    		<font style="font-weight:bold; display:none;" id="submit_ticket_message">&nbsp;</font>
	    	</td>
	    </tr>
	    <tr height="2px"><td align="left" colspan="2">&nbsp;</td></tr>
	    <tr>
	        <td></td>
	        <td>
	            <input class="button" type="submit" name="submit_ticket_submission" value="Submit Ticket">
	            <input class="button" type="reset" value="Reset">
	            <input class="button" type="button" name="cancel" value="Cancel" onClick=\'window.location.href="index.php"\'>
	            <input type="hidden" name="dataname" value="'.$data_name.'" />
	            <input type="hidden" name="jquery_failed" value="failed" />
	        </td>
	    </tr>
	</table>
	</form>';

	return $s_retval;
}

?>

<style type="text/css">
	td.header {
		font-weight: bold;
		text-align: left;
		padding-left: 3.5px;
		vertical-align: top;
	}
	[type=text].error, [type=textarea].error, textarea.error, select.error {
		border: 1px solid red;
		border-radius: 3px;
		font-family: Verdana, Arial, Helvetica, sans-serif;
		font-size: 12px;
		font-weight: normal;
	}
	label.error {
		color: red;
	}
	label.error.textarea {
		float: left;
		margin-top: 3px;
	}
	a {
		color: #95AE31;
		text-decoration: underline;
	}
	a:hover {
		color: gray;
		cursor: pointer;
	}
</style>

<script type="text/javascript">
	function removeNonMatching(element, match) {
		var jelement = $(element);
		var value = jelement.val();
		var matches = value.match(match);
		value = "";
		if (matches !== null && matches.length > 0) {
			$.each(matches, function(k,v) {
				value += v;
			});
		}
		jelement.val(value);
	}

	function edit(element, new_input_type) {
		var jelement = $(element);
		var jinput = jelement.siblings("input");
		var jparent = jelement.parent();
		jparent.html(jinput.clone().attr("type", new_input_type));
	}

	$(document).ready(function() {
		$("input[name=jquery_failed]").val("hello");
		$("input[name=jquery_failed]").remove();
	});

	$("#form_ticket_submit").validate({
		rules:{
			name: {
				required: true,
			},
			company: {
				required: true,
			},
			email: {
				required: false,
				email: true,
			},
			phone: {
				required: false,
			},
			phone_ext: {
				required: false,
			},
			subject: {
				required: true,
				minlength: 5,
			},
			message: {
				required: true,
				minlength: 5,
			},
			group: {
				required: true,
			}
		},
		submitHandler: function(form){
        	$("#submit_ticket_message").stop(true,true);
        	$("#submit_ticket_message").css({color:"gray",opacity:0});
        	$("#submit_ticket_message").html("Processing...");
        	$("#submit_ticket_message").show();
        	$("#submit_ticket_message").animate({opacity:1}, 300);
			$.ajax({
            	type: "POST",
            	url: "index.php?widget=support",
            	data: $(form).serialize(),
            	cache: false,
            	async: true,
            	success: function(msgs){
            		msgs = msgs.trim().split("|*command*|");
            		$.each(msgs, function(k,msg) {
	            		msg = msg.trim();
	                	var result = msg.split("|*note*|");
	                	var clear = false;
	                	$("#submit_ticket_message").stop(true,true);
	                	if(result[0] == "success"){
		                	$("#submit_ticket_message").css({color:"black",opacity:0});
		                	clear = true;
		                	$("#submit_ticket_message").html(result[1]);
		                	$("#submit_ticket_message").show();
				        	$("#submit_ticket_message").animate({opacity:1}, 300);
	                	}
	                	else if (result[0] == "reload") {
	                		setTimeout(function() {
		                		window.location = "?mode=support";
		                	}, 1000);
		                	clear = true;
	                	}
	                	else if (result[0] == "script") {
	                		var append_str = "<script type='text/javascript'>"+result[1]+"<\/script>";
	                		$("#center").append(append_str);
	                	}
	                	else{
		                	$("#submit_ticket_message").css({color:"red",opacity:0});
		                	$("#submit_ticket_message").html(result[1]);
		                	$("#submit_ticket_message").show();
				        	$("#submit_ticket_message").animate({opacity:1}, 300);
	                	}
			        	if (clear)
			        		$(".clear").val("");
			        });
		        },
	            error: function(msg){
                	$("#submit_ticket_message").stop(true,true);
                	$("#submit_ticket_message").css({color:"red",opacity:0});
                	$("#submit_ticket_message").html("You ticket could not be submitted. Please try again in a few minutes or use the links above.");
                	$("#submit_ticket_message").show();
		        	$("#submit_ticket_message").animate({opacity:1}, 300);
	            },
            });
		},
	});
</script>
