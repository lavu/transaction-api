<b>Beta Version Agreement</b>

<br /><br />The new POSLavu Backend is currently in pre-release, <br />and subject to the terms of the beta testing agreement outlined below:
<br /><br />

<table cellspacing=0 cellpadding=8 style='width:400px; border:solid 1px #777777;'><tr><td align='center'>By using this beta version of a Lavu Inc. product or service, you acknowledge the potential
of malfunction and incomplete development. By using a beta version, you accept full
responsibility for any losses or damages incurred during the use of the beta version. Lavu
Inc. is not responsible for any negative event associated with the use of this beta version, and
makes no claims to its reliability, function, or capabilities.
<br /><Br />Lavu Incorporated
<br />2012
<br /><br />
<input type='button' value='I Agree' onclick='if(confirm("By proceeding you are agreeing to the Beta Version Agreement")) window.location = "https://admin.poslavu.com/newcp/backend_demo/"' />
</td></tr></table>

