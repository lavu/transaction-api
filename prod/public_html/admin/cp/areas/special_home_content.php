<?php
	function choose_datacenter($shc_msg="")
	{
		global $location_info;
		
		$net_path = $location_info['net_path'];
		$use_net_path = $location_info['use_net_path'];
		
		$cloud1_url = "http://cloud1.poslavu.com"; // dc main
		$backup1_url = "http://dcloud1.poslavu.com"; // dc backup
		$cloud3_url = "http://cloud3.poslavu.com"; // dc cloud3
		// secondary is admin.poslavu.com
				
		$current_dc = "";
		if($use_net_path=="1" && $net_path==$cloud1_url)
			$current_dc = "main";
		else if($use_net_path=="1" && $net_path==$backup1_url)
			$current_dc = "backup1";
		else if($use_net_path=="1" && $net_path==$cloud3_url)
			$current_dc = "cloud3";
		else if($use_net_path=="0" || $net_path=="https://admin.poslavu.com")
			$current_dc = "secondary";
		
		if(isset($_GET['update_data_center']))
		{
			$vars = array();
			$vars['action'] = "Update Data Center";
			$vars['user'] = admin_info("user");
			$vars['user_id'] = admin_info("user_id");
			$vars['ipaddress'] = $_SERVER['REMOTE_ADDR'];
			$vars['data'] = "previous: " . $location_info['net_path'] . " - " . $location_info['use_net_path'];
			$vars['detail1'] = "to: " . $_GET['update_data_center'];
			$vars['server_time'] = date("Y-m-d H:i:s");
			lavu_query("insert into `admin_action_log` (`action`,`user`,`user_id`,`ipaddress`,`data`,`detail1`,`server_time`) values ('[action]','[user]','[user_id]','[ipaddress]','[data]','[detail1]','[server_time]')",$vars);
			$udc = $_GET['update_data_center'];
			if($udc=="main" || $udc=="backup1" || $udc=="cloud3")
			{
				$set_url = "";
				if($udc=="main") $set_url = $cloud1_url;
				else if($udc=="backup1") $set_url = $backup1_url;
				else if($udc=="cloud3") $set_url = $cloud3_url;
				
				if($set_url!="")
				{
					lavu_query("update `locations` set `use_net_path`='1',`net_path`='[2]' where `id`='[1]'",$location_info['id'],$set_url);
				}
			}
			else if($udc=="secondary")
			{
				lavu_query("update `locations` set `use_net_path`='0' where `id`='[1]'",$location_info['id']);
			}
			echo "<script language='javascript'>window.location = \"?dc=1&updated_data_center=1\"</script>";
			exit();
		}
		else if(isset($_GET['updated_data_center']))
		{
			echo "<br>";
			echo "<table width='360'><tr><td align='center'>";
			echo "Your data center has been updated<br><b><font style='color:#880000'>Please reload settings on all of your iPads now</font></b>";
			echo "</td></tr></table>";
			echo "<br>";
		}
		
		$str = "";
		if($current_dc=="main" || $current_dc=="secondary" || $current_dc=="backup1" || $current_dc=="cloud3")
		{
			$str .= $shc_msg;
			
			$selected_props = "style='background-color:#aecd37; width:100px; color:white; border:2px solid #cad4d7; '";
			$other_props = " alt='dc[value]' title='dc [value]' onclick='if(confirm(\"Are you sure you want to change your data center? \\nAfter making this change you will need to reload settings\\non all iPads and iPod Touches\")) window.location = \"?dc=1&update_data_center=[value]\"'";

			$main_props = str_replace("[value]","main",$other_props);
			$secondary_props = str_replace("[value]","secondary",$other_props);
			$backup1_props = str_replace("[value]","backup1",$other_props);
			$cloud3_props = str_replace("[value]","cloud3",$other_props);

			if($current_dc=="main") $main_props = $selected_props;
			else if($current_dc=="secondary") $secondary_props = $selected_props;
			else if($current_dc=="backup1") $backup1_props = $selected_props;
			else if($current_dc=="cloud3") $cloud3_props = $selected_props;

			$str .= "<div style='margin:8px'>";
			$str .= "<table cellspacing=2 cellpadding=4  >";
			$str .= "<tr><td colspan=4 style='text-align:center; '>";
			//$str .= "<div id='fadeLine' style='width:360px'></div>";
			$str .= "<br><img src='/images/i_cloud_datacenter_sm.png' style=''/> <span style='position: relative;
font-family: sans-serif, verdana;
font-size: 14;
color: #c6c8bf;
top: -5px;'>SELECT DATA CENTER</span> <img src='/images/btn_question_cp_sm.png' onclick='$(\"#homePopupArea\").fadeIn(\"slow\")' class='aboutDataCenter'/></td></tr>";
			
			$str .= "<tr>";
			//$str .= "<td align='right' valign='middle'>Your Data Center:<br><font style='font-size:10px; color:#888888'>(click to change)</font></td>";
			$str .= "<td class='dataCenter' align='center' $main_props>";
			
			$str .= "Cumulus";
			
			if($current_dc=="secondary")
			{
				$str .= "</td><td class='dataCenter' align='center' $secondary_props >";
				
				$str .= "Stratus";
			}
			
			$str .= "</td><td class='dataCenter' align='center' $cloud3_props >";
			
			$str .= "Bubba";
			
			$str .= "</td><td class='dataCenter' align='center' $backup1_props >";
			
			$str .= "Nimbus";

			$str .= "</td></tr>";
			$str .= "</table>";
			$str .= "</div>";
		}
		
		return $str;
	}
	if(!isset($shc_msg)) $shc_msg = "";
	echo choose_datacenter($shc_msg);
?>
