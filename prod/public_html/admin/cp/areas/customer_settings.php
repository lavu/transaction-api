<?php


    if(empty($locinfo) && !empty($location_info)){
        $locinfo = $location_info;
    }

    //Problem existed where $locinfo was renamed.  On each save an insert occured.  There should only be 1 row per certain settings.
    makeSureOnlyOneOfEachConfigFieldsExist();

    $customerSettingsComponentPackages = array(  // TO DO : Keep this up-to-date! (key=component_package, val=ID)
        'customer' => '15',
        'pizza'    => '13',
        'fitness'  => '20',
        'tithe'    => '22',
        'hotel'    => '3'
    );

    /*
    if(!empty($_POST)){
        //getDisableOrderAddOn_19_row_fromConfig();
        //setDisableOrderAddOn_19_row_inConfig('1');
        //echo "STUFFS:" . print_r(getDisableOrderAddOn_19_row_fromConfig(), 1);
        echo "POST: " . print_r($_POST,1);
        return;
    }
    */
    require_once(resource_path().'/form_piggyback_functions.php');

    //echo 'Location_info has:' . count($location_info);
    //echo '$locinfo has:' . count($locinfo);

    //If they don't have a comtype, then they may upgrade to customer with a conversion.
    //So we just give them a page that gives them that option.
    if ( $comPackage == '' || !empty($_POST['convert_to_customer']) ){
        require_once( dirname(__FILE__) . '/customer_settings_enable.php' );
        return;
    }
    if ( $comPackage == 'customer' && isset($_REQUEST['convert_back_from_customer']) ){
        require_once( dirname(__FILE__) . '/customer_settings_enable.php' );
        return;
    }

    if ( $comPackage != 'ers' && !isset($customerSettingsComponentPackages[$comPackage]) ){
        return;
    }

    /*  Written by Brian D.
     *  The purpose of this file is for regular merchants with com-type == '', meaning not lavukart or ers, etc.
     *  to be able to change the configuration of their `Lavu Registrar` without them being able to screw it up.
     *
     *  Clients will be given a choice from static registration fields, so they are not capable of changing
     *  their labels etc. which could destroy the link within the config->med_customers indirection.
     *
     *  For each field the options they are given are: hidden/visible, optional/required, and order (of appearance).
     *
     *  The `Given Fields` below may each have a display 'type' of the following:
     *       input (general text), list_menu (select), date (date select), and list_menu of states (state is stored elsewhere).
     *       These are stored in column `type`, for states use list_menu and put 'useTool' in value4, and 'state' in value5
     *
     *  Given Fields:
     *     Can only use/show 14 at a time. (That's all that's visible in the registrar app).
     * column:     value                       value2    type       notes
     *         1.) First Name                  f_name    input
     *         2.) Last Name                   l_name    input
     *         3.) Email                       field1    input
     *         4.) Phone Number                field2    input      value4 = number THIS IS FOR FUTURE, TO USE/INCLUDE NUMPAD
     *         5.) Street Address              field3    input
     *         6.) City                        field4    input
     *         7.) State                       field5    list_menu  value4 = useTool, value5 = State
     *         8.) Zipcode                     field6    input      value4 = number THIS IS FOR FUTURE, TO USE/INCLUDE NUMPAD
     *         9.) Birthday                    field7    date
     *        10.) Company                     field8    input
     *        11.) How did you hear about us?  field9    input
     *        12 - 21.) Custom (unless set special per some extension etc. backwards compatible).
     *        22.) Dataname special (used for if can sync customers)
     *       Added fields for fitness...
     *       --waiver expiration
     *       --rfid
     *       --profile_picture
     *
     *      Currently adding for tithe
     *      --User login_name (needs to be unique)
     *      --User password_hash
     */

    $thisPageURL = "?mode={$section}_{$mode}";

     $dataName = admin_info('dataname');
     $locID = getTopLocID();

	 $sttngs = array();

     if ($comPackage != "tithe") {
        $sttngs['require_customer_for_checkout'] = "<span id='require_customer__label'>" .
                                   speak("Require customer selection before entering Check Out").":" . "</span>";
        $sttngs['auto_launch_customer_selector'] = "<span id='auto_launch__label'>" .
                                   speak("Automatically start customer selector when starting a new order").":" . "</span>";
     }


    $sttngs['auto_focus_customer_search'] = "<span id='auto_focus_customer_search'>".
                                   speak("Auto focus on customer search within app").":"."</span>";

    $sttngs['customers_shows_most_recent'] = "<span id='shows_most_recent__label'>" .
        speak("Start customer selector with recently active customers").":" . "</span>";

    $sttngs['bold_search_findings'] = "<span id=''>".
                                   speak("Bold search findings in results").":"."</span>";

    if(isSyncCustomersEnabled()){
        $sttngs['group_by_dataname_field22'] = "<span id='group_by_dataname_field22'>".
                                   speak("Group Results by Dataname (field22)").":"."</span>";
    }
     if ( !isset($comPackage) || !isset($customerSettingsComponentPackages[$comPackage]) ) {
         echo 'Customer Settings is not available for component type: ' . $comPackage;
         exit();
     }

     //Checks the configuration table for `has_registrar_fields_been_initialized`.
     $needsInitialization = !getHasRegistrarBeenInitializedInConfig();
     if ($needsInitialization) {
         //echo 'initializingRegistrarSettings';
         deleteAllRegistrarRowsFromConfig();
         insertInitialRegistrarRowsIntoConfig();
     }

     if ( isset($_POST['is_saving_registrar_settings']) && $_POST['is_saving_registrar_settings'] ) {
         updateConfigGetCustomerInfosIfPosted();
     }

     //If we just posted we want to get rid of the post so we don't get stupid resend msg on refresh.
    if(isset($_POST['is_saving_registrar_settings'])){ echo "<script type='text/javascript'>window.location.replace('?mode={$section}_{$mode}');</script>";exit(); }


    ensureDatanameFieldExistsIfAppropriate();


     //Now we load the GUI.
     echo getUserInterfaceHTML();
     echo javascriptAppendCustomFieldsPartOfSettingsTable();

     //We are going to do this from a config now.  We leave in case...
     //echo getDisableHTMLLink();//For customer to disable their 'Customer' stuffs.

     //    ONLY FUNCTIONS BELOW    //
     function makeSureOrderValidInPostArraySortIfNecessary(){
         global $_POST;
         $filterArr = array();
         $i = 0.0001;
         foreach($_POST as $key => $val){
             $i += 0.0001;
             if(preg_match('/^.*_order$/', $key)){
                 $filterArr[$key] = floatval($val . '.' . $i);
             }
         }
         asort($filterArr);
         $returnArr = array();
         foreach($filterArr as $key => $val){
             $returnArr[$key] = intval(floor($val));
         }
         //print_r($returnArr);
         return $returnArr;
     }

     function updateConfigGetCustomerInfosIfPosted(){
         global $_POST, $data_name, $locinfo, $locID, $sttngs;
         $labelOrder = makeSureOrderValidInPostArraySortIfNecessary();
         //Look at insertInitialRegistrarRowsIntoConfig(), we follow the same indirection.
         //value is the label, and value2 is the indirection field.

         //$(LABEL)_included
         //$(LABEL)_required
         //$(LABEL)_order

        //First_Name
         if(isset($_POST["First_Name_print_option"])){
            lavu_query("UPDATE `config` SET `value10`='1', `value12`='[1]' WHERE `setting`='GetCustomerInfo' AND `value`='First_Name'", $_POST["First_Name_print_option"]);
         }
         //Last_Name
         if(isset($_POST['Last_Name_print_option'])){
            lavu_query("UPDATE `config` SET `value10`='2', `value12`='[1]' WHERE `setting`='GetCustomerInfo' AND `value`='Last_Name'", $_POST["Last_Name_print_option"]);
         }
         
         


         $extra_array=array( array('extra1', 'extra1_included'),
         		array('Extra1', 'Extra1_included'),
         		array('Country', 'Country_included'),
         		array('CUIT', 'CUIT_included'),
         		array('Street_Address_1', 'Street_Address_1_included'));
         for($i=0;$i<count($extra_array);$i++){
	         if(isset($_POST[$extra_array[$i][1]])){
	         	$extra=$extra_array[$i][0];
	         	$extra_deleted = $_POST["{$extra}_included"] == 'include'  ? '0' : '1';
	         	$extra_value6 = $_POST["{$extra}_required"] == 'required' ? 'requiredField' : '';
	         	lavu_query("UPDATE `config` SET `value10`='[1]', `value12`='[2]', `_deleted`='[3]', `value11`='[4]', `value6`='$extra_value6' WHERE `setting`='GetCustomerInfo' AND `value`='$extra'" , $labelOrder["{$extra}_order"], $_POST["{$extra}_print_option"],$extra_deleted,$_POST["{$extra}_report_option"]);
	         }
         }
         schedule_remote_to_local_sync_if_lls($locinfo, $locID, "table updated", "config");
/*
         $otherColumns = array( array('Email', 'input'),
                                array('Phone_Number', 'input'),
                                array('Street_Address', 'input'),
                                array('City', 'input'),
                                array('State', 'list_menu'),
                                array('Zip_Code', 'input'),
                                array('Birthday', 'date'),
                                array('Company', 'input'),
                                array('How_did_you_hear_about_us?', 'input'),
                                array('Work_Phone', 'input'),
                                array('Cell_Phone', 'input'),
                                array('Street_Address_2', 'input'),
         						array('CUIT', 'input'),
                                array('EMC_First_Name', 'input'),
                                array('EMC_Last_Name', 'input'),
                                array('EMC_Phone', 'input') );
*/
        $otherColumns = array( array('Email', 'input'),
                                array('Phone_Number', 'input'),
                                array('Street_Address', 'input'),
                                array('City', 'input'),
                                array('State', 'list_menu'),
                                array('Zip_Code', 'input'),
                                array('Birthday', 'date'),
                                array('Company', 'input'),
                                array('How_did_you_hear_about_us?', 'input'),
                                array('Work_Phone', 'input'),
                                array('Cell_Phone', 'input'),
                                array('Street_Address_2', 'input'),
                                array('CUIT', 'input'),
                                array('EMC_First_Name', 'input'),
                                array('EMC_Last_Name', 'input'),
                                array('EMC_Phone', 'input') );
         $columnsCount = count($otherColumns);

         for($i=0;$i<$columnsCount;$i++) {

    			$currCol = $otherColumns[$i][0];

    			$vars = array();
    			//$vars['type'] = $currCol == 'State' ? 'list_menu' : '';

    			//Based on the title of the column/label
    			$vars['type'] = $otherColumns[$i][1];
    			$vars['value'] = $currCol;
    			$vars['value4'] = $currCol == 'State' ? 'useTool' : '';
    			$vars['value6'] = isset($_POST["{$currCol}_required"]) && $_POST["{$currCol}_required"] == 'required' ? 'requiredField' : '';
    			$vars['value10'] = !empty($labelOrder["{$currCol}_order"]) ? $labelOrder["{$currCol}_order"] : '';
    			$vars['value11'] = !empty($_POST["{$currCol}_report_option"]) ? $_POST["{$currCol}_report_option"] : '';
    			$vars['value12'] = !empty($_POST["{$currCol}_print_option"]) ? $_POST["{$currCol}_print_option"] : '';
    			$vars['_deleted'] = !empty($_POST["{$currCol}_included"]) && $_POST["{$currCol}_included"] == 'include'  ? '0' : '1';

    			$updQStr  = "UPDATE `config` SET `type`='[type]', `value4`='[value4]', `value6`='[value6]', `value10`='[value10]', `value11`='[value11]', `value12`='[value12]', `_deleted`='[_deleted]'";
    			$updQStr .= " WHERE `setting`='GetCustomerInfo' AND `value`='[value]'";
    			$updateResult = lavu_query($updQStr, $vars);
    			if(!$updateResult){
    					echo "MYSQL ERROR: " . lavu_dberror() . "<br>";
    					exit();
    			}
         }

		foreach ($sttngs as $key => $val) {

			$set_val = "0";
			if ($_POST['config:'.$key]) $set_val = "1";

			$query = "INSERT INTO `config` (`value`, `location`, `type`, `setting`) VALUES ('[1]', '[2]', 'location_config_setting', '[3]')";
			if (isset($locinfo[$key])) $query = "UPDATE `config` SET `value` = '[1]' WHERE `location` = '[2]' AND `type` = 'location_config_setting' AND `setting` = '[3]'";
			lavu_query($query, $set_val, $locID, $key);

			$locinfo[$key] = $set_val;
		}

		//Now update the Enable Waivers checkbox, which has 0 in db if checked and 1 in the db if not checked.
		$isUsingWaversChecked = $_POST['user_use_waiver_checkbox'] ? true : false;
		if($isUsingWaversChecked){
    		lavu_query("UPDATE `config` SET `value2`='0' WHERE `setting`='reg_app_bypass_sign_and_waivers'");
		}else{
    		lavu_query("UPDATE `config` SET `value2`='1' WHERE `setting`='reg_app_bypass_sign_and_waivers'");
		}


		//Now we set the disable_order_add_on_19 to 1 if it is checked, else we set it to 0.
		$disable_order_add_on_19_checked = $_POST['disable_add_on_19_checkbox'] == "on" ? "1" : "0";
        setDisableOrderAddOn_19_row_inConfig($disable_order_add_on_19_checked);

        //Default settings
/*
  "<select name='default_state_post_val'>".
                        getStatesAsSelectOptionsStr().
                    "</select>".
                "</td></tr>";
        $str .= "<tr><td style='text-align:right; border:1px solid black;'>".speak("Default City").":</td>".
                "<td style='border:1px solid black;'>".
                "<input type='text' name='default_city_post_value'></input>".  
*/

        insertOrUpdateConfigSetting('customer_settings_default_state', $_POST['default_state_post_val']);
        insertOrUpdateConfigSetting('customer_settings_default_city',  $_POST['default_city_post_value']);

        //Finally save the custom settings
        saveCustomFieldsOnFormSubmit();
     }

     function deleteAllRegistrarRowsFromConfig(){
         $deleteResult = lavu_query("DELETE FROM `config` WHERE `setting`='GetCustomerInfo'");
         if(!$deleteResult){
             error_log('Failed query in registrar_settings, search for fj9s4g4s');
             return false;
         }
     }

     function insertInitialRegistrarRowsIntoConfig(){
         global $location_info, $locID, $comPackage;
         //Keeping the columns in same internal order as the `config` table.
         $initQ  = "INSERT INTO `config` ";
         $initQ .= "(`location`,`setting`,`value`,`value2`,`value4`,`type`,`value5`,`value6`,`_deleted`,`value10`,`value11`,`value12`) ";
         $initQ .= "VALUES ";
         if($comPackage != 'tithe'){
         //           loc       setting           value          value2   val4        type   val5        value6        _del val10(order of appearance) val11(include in report ids : 150 = Active Customers)
         $initQ .= "('[1]', 'GetCustomerInfo', 'First_Name',     'f_name', '',        'input', '',          'requiredField', '0', '1', '150', ''),";//First Name
         $initQ .= "('[1]', 'GetCustomerInfo', 'Last_Name',      'l_name', '',        'input', '',          'requiredField', '0', '2', '150', ''),";//Last Name
         $initQ .= "('[1]', 'GetCustomerInfo', 'Email',          'field1', '',        'input', '',          '',              '0', '3', '150', ''),";//Email
         $initQ .= "('[1]', 'GetCustomerInfo', 'Phone_Number',   'field2', 'number',  'input', '',          '',              '0', '4', '150', ''),";//Phone Number
         $initQ .= "('[1]', 'GetCustomerInfo', 'Street_Address', 'field3', '',        'input', '',          '',              '0', '5', '', ''),";//Street Address
         $initQ .= "('[1]', 'GetCustomerInfo', 'City',           'field4', '',        'input', '',          '',              '0', '6', '', ''),";//City
         $initQ .= "('[1]', 'GetCustomerInfo', 'State',          'field5', 'useTool', 'list_menu', 'State', '',              '0', '7', '', ''),";//State
         $initQ .= "('[1]', 'GetCustomerInfo', 'Zip_Code',       'field6', 'number',  'input', '',          '',              '0', '8', '', ''),";//ZipCode
         $initQ .= "('[1]', 'GetCustomerInfo', 'Birthday',       'field7', '',        'date',  '',          '',              '0', '9', '', ''),";//Birthday
         $initQ .= "('[1]', 'GetCustomerInfo', 'Company',        'field8', '',        'input',  '',         '',              '0','10', '', ''),";//Company
         $initQ .= "('[1]', 'GetCustomerInfo', 'How_did_you_hear_about_us?', 'field9', '', 'input', '',     '',              '0', '11', '', '')";
         }
         if($comPackage == 'tithe'){
         $initQ .= "('[1]', 'GetCustomerInfo', 'First_Name',     'f_name', '',             'input', '',          'requiredField', '0', '1', '150', ''),";//First Name
         $initQ .= "('[1]', 'GetCustomerInfo', 'Last_Name',      'l_name', '',             'input', '',          'requiredField', '0', '2', '150', ''),";//Last Name
         $initQ .= "('[1]', 'GetCustomerInfo', 'User_Name',      'user_name',          '', 'input', '',          'requiredField', '0', '3', '', ''),";//User name.
         $initQ .= "('[1]', 'GetCustomerInfo', 'User_Password',  'user_password_hash', '', 'input', '',          'requiredField', '0', '4',  '', '')";//User pass hash.
         /*
         $initQ .= "('[1]', 'GetCustomerInfo', 'Email',          'field1', '',             'input', '',          '',              '0', '5', '150', ''),";//Email
         $initQ .= "('[1]', 'GetCustomerInfo', 'Phone_Number',   'field2', 'number',       'input', '',          '',              '0', '6', '150', ''),";//Phone Number
         $initQ .= "('[1]', 'GetCustomerInfo', 'Street_Address', 'field3', '',             'input', '',          '',              '0', '7', '', ''),";//Street Address
         $initQ .= "('[1]', 'GetCustomerInfo', 'City',           'field4', '',             'input', '',          '',              '0', '8', '', ''),";//City
         $initQ .= "('[1]', 'GetCustomerInfo', 'State',          'field5', 'useTool',      'list_menu', 'State', '',              '0', '9', '', ''),";//State
         $initQ .= "('[1]', 'GetCustomerInfo', 'Zip_Code',       'field6', 'number',       'input', '',          '',              '0', '10', '', ''),";//ZipCode
         $initQ .= "('[1]', 'GetCustomerInfo', 'Birthday',       'field7', '',             'date',  '',          '',              '0', '11', '', ''),";//Birthday
         $initQ .= "('[1]', 'GetCustomerInfo', 'Company',        'field8', '',             'input', '',          '',              '0', '12', '', ''),";//Company
         $initQ .= "('[1]', 'GetCustomerInfo', 'How_did_you_hear_about_us?', 'field9', '', 'input', '',          '',              '0', '13', '', '')";
         */
         }
         $insertResult = lavu_query($initQ, $locID);
         if(!$insertResult){
             echo "SQL error when trying to perform initialization insert, mysql error: " . lavu_dberror();
             exit();
         }

         $existsR = lavu_query("SELECT `value2` FROM `config` WHERE `setting`='has_registrar_fields_been_initialized'");
         if(!$existsR){
             echo 'error in query looking for `has_registrar_fields_been_initialized`';
             exit();
         }
         if(mysqli_num_rows($existsR)){
             lavu_query("UPDATE `config` SET `value2`='1' WHERE `setting`='has_registrar_fields_been_initialized'");
         }else{
             lavu_query("INSERT INTO `config` (`location`,`setting`,`value2`) VALUES ('[1]', 'has_registrar_fields_been_initialized', '1')", $locID);
         }

         $byPassResult = lavu_query("SELECT `value2` FROM `config` WHERE `setting`='reg_app_bypass_sign_and_waivers'");
         if(!$byPassResult){

         }
         if(mysqli_num_rows($byPassResult)){
             lavu_query("UPDATE `config` SET `value2`='1' WHERE `setting`='reg_app_bypass_sign_and_waivers'");
         }
         else{
             $qStr  = "INSERT INTO `config` (`location`,`setting`,`value2`) VALUES ";
             $qStr .= "('[1]','reg_app_bypass_sign_and_waivers', '1')";
             lavu_query($qStr, $locID);
         }
         schedule_remote_to_local_sync_if_lls($location_info, $locID, "table updated", "config");
     }

     //WHATEVER ELSE LITTLE UTILITY FUNCTIONS I NEED.
     function getTopLocID(){
         $selectResult = lavu_query("SELECT `id` FROM `locations` ORDER BY `id` ASC LIMIT 1");
         $resultArr = mysqli_fetch_assoc($selectResult);
         return $resultArr['id'];
     }
     function getHasRegistrarBeenInitializedInConfig(){
         $selectResult = lavu_query("SELECT `value2` FROM `config` WHERE `setting`='has_registrar_fields_been_initialized'");
         if(!$selectResult){
             error_log('Failed query in registrar, search for 5sjg4jsg4');
             return null;
         }
         if(mysqli_num_rows($selectResult) == 0){
             return false;
         }
         $resultArr = mysqli_fetch_assoc($selectResult);
         return $resultArr['value2'];
     }
     function getSettingEqualCustomerInfoTable(){
         $result = lavu_query("SELECT * FROM `config` WHERE `setting`='GetCustomerInfo' AND `value3` <> 'CustomField' ORDER BY `id` ASC");
         if(!$result){
            echo 'error trying to get the GetCustomerInfos from the config table';
            exit();
         }
         $returnArr = array();
         while($currRow = mysqli_fetch_array($result)){
            $returnArr[] = $currRow;
         }
         return $returnArr;
     }



     //                  <>~-~    G   U   I   ~-~<>                            //

     function getUserInterfaceHTML(){

         global $thisPageURL;

         //First we need the entire table
         $subTableOfCustInfo = getSettingEqualCustomerInfoTable();

         //Begin the form
         $outputHTML  = "<form action='$thisPageURL' id='save_form' method='post'>";
         //Some settings
         $outputHTML .= getSettingsFields();
         //Nice outlining frame
         $outputHTML  .= "<div style='border:1px solid black; display: inline-block; padding:2px 2px 7px 2px'>";
         //Now we create the table
         $outputHTML  .= "<table id='field_table' cellpadding='5'>";
         //The title row
         $outputHTML  .= getHTMLTitleRow();
         //$outputHTML .= "<tr><td>Label</td><"
         foreach($subTableOfCustInfo as $currRow){
            $outputHTML .= getHTMLTableRowForGetCustomerInfo($currRow);
         }
         $outputHTML .= "<input type='hidden' name='is_saving_registrar_settings' value='1' ></input>";
         $outputHTML .= "</table>";
         $outputHTML .= "<input id='save_submit_button' type='submit' value='".speak("Save")."'></input>";
         $outputHTML .= "</div>";
         $outputHTML .= "</form>";
         return $outputHTML;
     }

	function getOrInsertOneForConfigRowFor_reg_app_bypass_sign_and_waivers(){
    	$queryStr = "SELECT * FROM `config` WHERE `setting`='reg_app_bypass_sign_and_waivers'";
    	$result = lavu_query($queryStr);
    	if(!$result){
        	error_log("MYSQL Error in registrar_settings.php, query: " . $queryStr . "   Error: " . lavu_dberror());
    	}else if(mysqli_num_rows($result)){
        	$row = mysqli_fetch_assoc($result);
        	return $row['value2'];
    	}else{
        	$queryStr = "INSERT INTO `config` (`setting`,`value2`) VALUES ('reg_app_bypass_sign_and_waivers','1')";
        	$result = lavu_query($queryStr);
        	if(!$result){
        	   error_log("MYSQL Error in registrar_settings.php, query: " . $queryStr . "   Error: " . lavu_dberror());
        	}
        	return '1';
    	}
	}
	function getConfigBySetting($setting){
        $queryStr = "SELECT * FROM `config` WHERE `setting`='[1]'";
        $result = lavu_query($queryStr, $setting);
        if(!$result){
            error_log("MYSQL Error in registrar_settings.php, query: " . $queryStr . "   Error: " . lavu_dberror());
            return null;
        }else if(mysqli_num_rows($result)){
            $row = mysqli_fetch_assoc($result);
            return $row['value'];
        }else{
            return false;
        }
	}




	function getDisableOrderAddOn_19_row_fromConfig(){
	    global $locationid;
    	$queryStr = "SELECT * FROM `config` WHERE `setting`='disable_order_add_on_19' AND `location`='[1]'";
    	$result = lavu_query($queryStr, $locationid);
    	if(mysqli_num_rows($result)){
    	   return mysqli_fetch_assoc($result);
    	}else{
           return false;
    	}
	}
	function setDisableOrderAddOn_19_row_inConfig($value){
	    global $locationid;
    	$exists = getDisableOrderAddOn_19_row_fromConfig() ? true : false;
    	$result;
    	if($exists){
    	   $result = lavu_query("UPDATE `config` SET `value`='[1]', `type`='location_config_setting' WHERE `setting`='disable_order_add_on_19' AND `location`='[2]'", $value, $locationid);
    	}else{
           $result = lavu_query("INSERT INTO `config` (`setting`, `value`, `location`, `type`) VALUES ('disable_order_add_on_19', '[1]', '[2]', 'location_config_setting')", $value, $locationid);
    	}
	}

	function getSettingsFields() {

		global $locinfo;
		global $sttngs;
		global $comPackage;


		$str = "<table cellspacing='2' cellpadding='2'>";
		foreach ($sttngs as $key => $val) {
			$checked = "";
			if (isset($locinfo[$key]) && $locinfo[$key]=="1") $checked = " CHECKED";
			$id_4_checkbox = 'checkbox_'.$key.'_id';
			$str .= "<tr><td align='right'>$val</td><td><input type='checkbox' id='$id_4_checkbox' name='config:$key'$checked></td></tr>";
		}

		///
	    //the field is reg_app_bypass_sign_and_waivers, but we want them to check to use waivers.
	    //Essentially we handle this one inversely, if checked db value is 0, if not checked db value is 1;
	    $isRegAppBypassSignAndWaivers = getOrInsertOneForConfigRowFor_reg_app_bypass_sign_and_waivers();
	    $checked = $isRegAppBypassSignAndWaivers ? '' : ' CHECKED';

        if($comPackage != 'tithe'){
            $str .= "<tr><td align='right'>".speak("Enable Waivers With User Signature").":</td>".
                   "<td><input type='checkbox' id='enable_waivers_checkbox_id' name='user_use_waiver_checkbox'$checked></td></tr>";
        ///
        }



        if($comPackage != 'tithe'){
            //For disabling Customer Management inside the app.  Will be so that the customer button no longer appears, and customer
            //management is effectively turned off.
            //-+
            $disabledOrderAddON_19Row = getDisableOrderAddOn_19_row_fromConfig();
            $disabledOrderAddOn_19 = $disabledOrderAddON_19Row['value'];
            $dis_cm_CHECKED = !empty($disabledOrderAddOn_19) ? ' CHECKED' : '';
            $str .= "<tr><td align='right'>".speak("Disable Customer Management from within the app").":</td>";
            $str .=      "<td><input type='checkbox' name='disable_add_on_19_checkbox'$dis_cm_CHECKED ".
                                     "id='disable_add_on_19_checkbox_id' ".
                                     "onchange='disable_add_on_19_checkbox_changed(this);'></td></tr>";
            }

		ob_start();
		?>
        <script type='text/javascript'>
            var requireCustomerPreviousValue = null;
            var autoLaunchCustomerPreviousValue = null;
            function disable_add_on_19_checkbox_changed(disable19Checkbox){
                //The js id's: checkbox_require_customer_for_checkout_id
                //             checkbox_auto_launch_customer_selector_id
                var requireCustomerForCheckout_checkbox    = document.getElementById('checkbox_require_customer_for_checkout_id');
                var auto_launch_customer_selector_checkbox = document.getElementById('checkbox_auto_launch_customer_selector_id');
                var requireCustomerLabelSpan    = document.getElementById('require_customer__label');
                var autoLaunchCustomerLabelSpan = document.getElementById('auto_launch__label');

                if( disable19Checkbox.checked ){
                    requireCustomerPreviousValue = requireCustomerForCheckout_checkbox.checked;
                    autoLaunchCustomerPreviousValue = auto_launch_customer_selector_checkbox.checked;
                    requireCustomerForCheckout_checkbox.checked = false;
                    requireCustomerForCheckout_checkbox.disabled = true;
                    auto_launch_customer_selector_checkbox.checked = false;
                    auto_launch_customer_selector_checkbox.disabled = true;
                    requireCustomerLabelSpan.style.textDecoration    = 'line-through';
                    autoLaunchCustomerLabelSpan.style.textDecoration = 'line-through';
                }else{
                    if(requireCustomerPreviousValue !== null){
                        requireCustomerForCheckout_checkbox.checked = requireCustomerPreviousValue;
                    }
                    if(autoLaunchCustomerPreviousValue !== null){
                        auto_launch_customer_selector_checkbox.checked = autoLaunchCustomerPreviousValue;
                    }
                    requireCustomerForCheckout_checkbox.disabled = false;
                    auto_launch_customer_selector_checkbox.disabled = false;
                    requireCustomerLabelSpan.style.textDecoration    = '';
                    autoLaunchCustomerLabelSpan.style.textDecoration = '';
                }
            }
        </script>
        <?php
        $str .= ob_get_clean();
		//-+


        $defaultState = getConfigBySetting('customer_settings_default_state');
        $defaultCity = getConfigBySetting('customer_settings_default_city');

        //Building gui components for defaults
        $str .= "<tr><td style='text-align:right;'>".speak("Default State/Province").":</td>".
                "<td style=''>".
                    "<select name='default_state_post_val'>".
                        getStatesAsSelectOptionsStr($defaultState).
                    "</select>".
                "</td></tr>";
        $str .= "<tr><td style='text-align:right;'>".speak("Default City").":</td>".
                "<td style=''>".
                "<input type='text' name='default_city_post_value' value='$defaultCity'/>".
                "</td></tr>";

		$str .= "</table>";
		$str .= "<script type='text/javascript'>".
		          "disable_add_on_19_checkbox=document.getElementById('disable_add_on_19_checkbox_id');".
		          "disable_add_on_19_checkbox_changed(disable_add_on_19_checkbox);</script>";
		return $str;
	}

     function getHTMLTitleRow(){
         return "<tr style='text-align:center; background-color:#aaa;'>
                    <td style='text-align:left'>".speak("Label")."</td>
                    <td>".speak("Include")."</td>
                    <td>".speak("Required")."</td>
                    <td>".speak("Order")."</td>
					<td>".speak("Print")."</td>
					<td>".speak("Report")."</td>
                </tr>";
     }
     function getHTMLTableRowForGetCustomerInfo($rowArr){
         $label = $rowArr['value'];

         //For the selects, we need to default the option to what is currently in
         //the database.
         $hideSelected  = $rowArr['_deleted'] ? 'selected' : '';
         $requiredSelected = $rowArr['value6'] == 'requiredField' ? 'selected' : '';
         $disabled = $rowArr['value2'] == 'f_name' || $rowArr['value2'] == 'l_name' ? 'disabled' : '';
         $totalCountFields = 99;


         //TODO MAKE SURE First_Name and Last_Name are fixed, they have to be included and are required.
         //The label is unique, so will also be used inside the html id identifier.
         $displayLabel = str_replace('_', ' ', $label);
         $retStr = "<tr>
                        <td>
                            ".speak($displayLabel)."
                        </td>
                        <td>
                            <select id='select_include_do_not_include_toggle_$label' name='{$label}_included' $disabled>
                                <option value='include'>".speak('Include')."</option>
                                <option value='hide' $hideSelected>--".speak("Hide")."--</option>
                            </select>
                        </td>
                        <td>
                            <select id='select_required_optional_toggle_$label' name='{$label}_required' $disabled>
                                <option value='optional'>".speak("Optional")."</option>
                                <option value='required' $requiredSelected>".speak("Required")."</option>
                            </select>
                        </td>
                        <td>
                            <select id='select_order_of_appearance_$label' name='{$label}_order' $disabled>";

         //First Name, and Last name have mandatory placements of 1 and 2.
         if($rowArr['value2'] == 'f_name'){
             $retStr .=        "<option value='1'>1</option>";
         }
         else if($rowArr['value2'] == 'l_name'){
             $retStr .=        "<option value='2'>2</option>";
         }
         else {
            for( $i=3; $i <= $totalCountFields; $i++){
                 $isSelected = intval($rowArr['value10']) == intval($i) ? 'selected' : '';
                 $retStr .=        "<option value='$i' $isSelected>".speak($i)."</option>";
            }
         }

		$retStr .= "</select>
							</td>
							<td align='center'>
								<select id='select_print_option_$label' name='{$label}_print_option'>";
		$prntOptArray = array("No"=>"", "Kitchen"=>"k", "Receipt"=>"r", "Both"=>"b");
		foreach ($prntOptArray as $key => $val) {
			$selected = "";
			if ($rowArr['value12'] == $val) $selected = " selected";
			$retStr .= "<option value='$val'$selected>".speak($key)."</option>";
		}
		$retStr .= "</select>
							</td>
							<td align='center'>
								<select id='select_report_option_$label' name='{$label}_report_option' $disabled>
									<option value=''>".speak("No")."</option>
									<option value='150'".(($rowArr['value11']=="150" || $disabled)?" selected":"").">".speak("Yes")."</option>";
		// 150 = Active Customers report id... future forms may present a list of Registrar related reports for inclusion in a comma delimited list... simple Yes/No for now... 7/3/2013 RJG
		$retStr .= "</select>
							</td>
         </tr>";

         return $retStr;
     }

     function getDisableHTMLLink(){
         $htmlStr = "<br>";
         $htmlStr .= "<a href='?mode={$section}_{$mode}&convert_back_from_customer=question'> Click here to disable 'Customer Management'</a>";
         return $htmlStr;
     }

     function getStatesListFromDB(){
         $result = lavu_query("SELECT * FROM `states`");
         $statesArr = array();
         while($curr = mysqli_fetch_assoc($result)){
            if(empty($curr['_deleted']))
                $statesArr[$curr['state_name']] = $curr;
         }
         ksort($statesArr);
         return array_values($statesArr);
     }
     function getStatesAsSelectOptionsStr($currDefault){
         $statesList = getStatesListFromDB();
         $str = "<option value=''></option>";
         foreach($statesList as $currRow){
            $selectedStr = trim(strtolower($currRow['state_name'])) == trim(strtolower($currDefault)) ? "selected" : "";
            $str .= "<option value='{$currRow['state_name']}' $selectedStr>{$currRow['state_name']}</option>";
         }
         //error_log("options: ". $str);
         return $str;
     }
     function insertOrUpdateConfigSetting($setting, $value){
        $result = lavu_query("SELECT * FROM `config` where `setting`='[1]'", $setting);
        if(mysqli_num_rows($result)){
            lavu_query("UPDATE `config` SET `value`='[1]' WHERE `setting`='[2]'", $value, $setting);
        }else{
            lavu_query("INSERT INTO `config` (`setting`, `value`) VALUES ('[1]','[2]')", $setting, $value);
        }
     }
     function makeSureOnlyOneOfEachConfigFieldsExist(){
        $configFieldsToCheck = array("require_customer_for_checkout","auto_launch_customer_selector","customers_shows_most_recent");

        foreach($configFieldsToCheck as $currFieldToCheck){
            $result = lavu_query("SELECT `id` FROM `config` WHERE `setting`='[1]'",$currFieldToCheck);
            $idList = array();
            while($currRow = mysqli_fetch_assoc($result)){
                $idList[] = $currRow['id'];
            }
            $deleteIDList = $idList;
            unset($deleteIDList[0]);
            $deleteIDList = array_values($deleteIDList);
            if(count($deleteIDList)){
                $deleteIDListStr = implode(',', $deleteIDList);
                $deleteResult = lavu_query("DELETE FROM `config` WHERE `id` IN (".$deleteIDListStr.")");
            }
        }
     }


/*
{
    "primary_id": 43229,
    "primary_dataname": "acts_test_1",
    "datanames_in_chain": [{
        "id": "43229",
        "data_name": "acts_test_1",
        "title": "ACTS Test 1",
        "is_primary": true
    }, {
        "id": "43230",
        "data_name": "acts_test_2",
        "title": "ACTS Test 2",
        "is_primary": false
    }, {
        "id": "43231",
        "data_name": "acts_test_3",
        "title": "ACTS Test 3",
        "is_primary": false
    }],
    "sync_allowed_tables": ["med_customers"],
    "account_table_row_counts": {
        "acts_test_1": {
            "med_customers": "3"
        },
        "acts_test_2": {
            "med_customers": "3"
        },
        "acts_test_3": {
            "med_customers": "3"
        }
    }
}
*/
     function isSyncCustomersEnabled(){
        global $data_name;
        require_once(__DIR__.'/../resources/chain_functions.php');
        $syncPermissions = getTableSyncPermissionsAndInfoOfLocationsPrimary($data_name);
        $syncsCustomers = $syncPermissions && in_array('med_customers', $syncPermissions['sync_allowed_tables']);
        return $syncsCustomers;
     }


     //  LP - 8864   ------------  Custom Field Functions ---------------//
     // The purpose of this method is to return a data structure that is 2 part:
     // Part 1.) Static fields
     //     Static fields are those that are created when enabling customer management and those fields that have
     //     been migrated as per the specifics of the account (e.g. such as extra1/2 CUIT for fiscal, rfid, etc.)
     // Part 2.) Custom Fields
     //     Custom fields are different from the static fields by the following attributes:
     //         2.a) They are dynamically added and/or deleted by the user of CP.
     //         2.b) Their titles are editable (and may switch purpose if user is not careful)
     //         2.c) They will have a flag set on the config row value3='custom' this is so only they may be added/deleted.
     //     The custom fields will be under two keys: available, used.
     // This separation is in conscientious consideration of backward compatability and ensuring the integrity of other
     // previous uses of 1-off customer managment fields.
     //
     // This method will return a map with 2 top layer keys:
     // static_fields will be a numerical array of the pre-set / non-custom customer management fields.
     // used_custom_fields will be an array of the fields that can be added / deleted by the user.
     //     custom_fields will return all available fields even if they do not exist yet.  This is for the gui
     //     to properly manage creating new fields and knowing there is a 'slot' open for it on med_customers, etc.
     function getCustomerFieldsAndFieldAvailability(){
        // 1.) Grab the static fields.
        $staticFields = array();
        $staticFieldsResult = lavu_query("SELECT * FROM `config` WHERE `setting`='GetCustomerInfo' AND `value3` <> 'CustomField'");
        while($curr = mysqli_fetch_assoc($staticFieldsResult)){
            $staticFields[] = $curr;
        }
        // 2.) Grab the used custom fields.
        $usedCustomFields = array();
        $usedCustomFieldsResult = lavu_query("SELECT * FROM `config` WHERE `setting`='GetCustomerInfo' AND `value3` = 'CustomField'");
        while($curr = mysqli_fetch_assoc($usedCustomFieldsResult)){
            $usedCustomFields[] = $curr;
        }
        // 3.) Calculate the available fields.
        //Checking the available field range for use.
        $usedFieldsIndex = array();
        for($i = 12; $i < 22; $i++){
            $usedFieldsIndex['field'.$i] = false;
        }
        //Disable anything used elsewhere.
        foreach($staticFields as $currStaticField){
            $usedFieldsIndex[$currStaticField['value2']] = true;
        }
        foreach($usedCustomFields as $currDynamicField){
            $usedFieldsIndex[$currDynamicField['value2']] = true;
        }
        $availableFields = array();
        foreach($usedFieldsIndex as $columnName => $columnIsUsed){
            if(!$columnIsUsed){
                $availableFields[] = $columnName;
            }
        }
        return array('static_fields' => $staticFields, 'used_custom_fields' => $usedCustomFields, 'available_custom_columns' => $availableFields);
     }


     function javascriptAppendCustomFieldsPartOfSettingsTable(){
        
        $customerFieldInformation = getCustomerFieldsAndFieldAvailability();
        $customerFieldInformationJSONStr = json_encode($customerFieldInformation);
        $translationMap = array();
        $translationMap['add_custom_row_title'] = speak("Add Custom Field");
        $translationMap['label_place_holder'] = speak("Custom Field Label");
        $translationMap['Include'] = speak('Include');
        $translationMap['--Hide--'] = speak('--Hide--');
        $translationMap['optional_option_title'] = speak("Optional");
        $translationMap['required_option_title'] = speak("Required");
        $translationMap['print_option_no'] = speak("No");
        $translationMap['print_option_kitchen'] = speak("Kitchen");
        $translationMap['print_option_reciept'] = speak("Reciept");
        $translationMap['print_option_both'] = speak("Both");
        $translationMap['report_option_no'] = speak("No");
        $translationMap['report_option_yes'] = speak("Yes");
        $translationMapJSONStr = json_encode($translationMap);
        $customFieldsJSScript = file_get_contents(__DIR__.'/customer_management_custom_fields.js');
        echo '<script>'.$customFieldsJSScript.'loadCustomFields('.$translationMapJSONStr.', '.$customerFieldInformationJSONStr.');</script>';
     }

     function saveCustomFieldsOnFormSubmit(){
        $customFieldsInformationJSON = $_POST['custom_customer_field_information'];
        $customFieldsInformation = json_decode($customFieldsInformationJSON,1);
        $usedCustomFields = $customFieldsInformation['used_custom_fields'];

        foreach($usedCustomFields as $currToSaveCustomField){

            if(empty($currToSaveCustomField['value2'])){
                continue;
            }
            if($currToSaveCustomField['value3'] != 'CustomField'){
                continue;
            }
            $currToSaveCustomField['value3'] = !empty($currToSaveCustomField['value3']) ? $currToSaveCustomField['value3'] : 'CustomField';
            $existsAlreadyQuery = "SELECT * FROM `config` WHERE `setting`='GetCustomerInfo' AND `value2`='[1]'";
            $existsAlreadyResult = lavu_query($existsAlreadyQuery, $currToSaveCustomField['value2']);

            //1.) If it is accidentally not a custom already, we do not update, else update.
            /*
                'type':'input', 
                'value3':'1',
                'value2':availableColumn, //Column of med customers it maps to.
                '_deleted':'0', //whether to display or hide field
                'value':'', //Label name
                'setting':'GetCustomerInfo',
                'value11':'', //Report value (either '150' or '')
                'value6':'', //required or optional ('requiredField' or '')
                'value12':'', //Print 'k' Kitchen, 'r' Receipt, 'b' Both, '' NO
                'value10':fieldTable.rows.length //Display order
            */
            if(mysqli_num_rows($existsAlreadyResult)){ // Update (or user deleted)
                $existingRow = mysqli_fetch_assoc($existsAlreadyResult);

                //customFieldsDataStore.customerFieldInformation['used_custom_fields'][i]['customer_removed']
                if(!empty($currToSaveCustomField['customer_removed']) && $currToSaveCustomField['customer_removed'] === true){
                    $deleteQuery = "DELETE FROM `config` WHERE `id`='".intval($existingRow['id'])."'";
                    $removeResult = lavu_query($deleteQuery);
                }else{
                    $updateQuery  = "UPDATE `config` SET ";
                    $updateQuery .= "`type`='[type]', ";
                    $updateQuery .= "`value3`='[value3]', ";
                    $updateQuery .= "`value2`='[value2]', ";
                    $updateQuery .= "`_deleted`='[_deleted]', ";
                    $updateQuery .= "`value`='[value]', ";
                    $updateQuery .= "`value11`='[value11]', ";
                    $updateQuery .= "`value6`='[value6]', ";
                    $updateQuery .= "`value12`='[value12]', ";
                    $updateQuery .= "`value10`='[value10]' ";
                    $updateQuery .= "WHERE `id`='".intval($existingRow['id'])."'";
                    $updateResult = lavu_query($updateQuery, $currToSaveCustomField);
                    //If there are any other rows, we need to delete them... this shouldn't happen, but just in case.
                    if(mysqli_num_rows($existsAlreadyResult) > 1){
                        $toDeleteArray = array();
                        while($curr = mysqli_fetch_assoc($existsAlreadyResult)){
                            $toDeleteArray[] = $curr['id'];
                        }                    
                        lavu_query("DELETE FROM `config` WHERE `id` IN ('".implode("','", $toDeleteArray)."')");
                    } 
                }
            }else{ // Insert
                if(!empty($currToSaveCustomField['customer_removed']) && $currToSaveCustomField['customer_removed'] === true){
                    //Do Nothing, don't save.
                }else{
                    $insertQuery  = "INSERT INTO `config` ";
                    $insertQuery .= "(`setting`, `type`, `value3`,  `value2`,  `_deleted`,  `value`,  `value11`,  `value6`,  `value12`,  `value10`) VALUES ";
                    $insertQuery .= "('GetCustomerInfo','[type]','[value3]','[value2]','[_deleted]','[value]','[value11]','[value6]','[value12]','[value10]')";
                    //error_log("Query:".$insertQuery);
                    $insertResult = lavu_query($insertQuery, $currToSaveCustomField);
                }
            }
        }
     }

    function ensureDatanameFieldExistsIfAppropriate(){

        

        $datanameFieldExistsQuery = "SELECT * FROM `config` WHERE `value2`='field22'";
        $datanameFieldExistsResult = lavu_query($datanameFieldExistsQuery);
        
        $alreadyExists = false;
        if(!mysqli_num_rows($datanameFieldExistsResult)){
            $alreadyExists = true;
        }
        $isSyncCustomersEnabled = isSyncCustomersEnabled();


        //Add it if should exist and doesn't.
        if(!$alreadyExists && $isSyncCustomersEnabled){
            $insertQuery  = "INSERT INTO `config` (`value`,`value2`) VALUES ";
            $insertQuery .= "";
        }
        if($alreadyExists && !$isSyncCustomersEnabled){

        }
        //Remove if does exist but should not.
    }
