<?php	
	
	if ($in_lavu) {
	
		$forward_to = "index.php?mode=$mode";

		if (isset($_POST['start_new'])) {
		
				$current_start_date = localize_datetime(date("Y-m-d H:i:s"), $location_info['timezone']);
				$get_info = lavu_query("SELECT `id`, `value`, `value_long` FROM `config` WHERE `type` = 'location_config_setting' AND `setting` = 'apple_pay_periods' AND `location` = '[1]'", $locationid);
				$info = mysqli_fetch_assoc($get_info);
				if ($info['value_long'] == "") $past_start_dates = $info['value'];
				else $past_start_dates = $info['value'].",".$info['value_long'];
				$update_info = lavu_query("UPDATE `config` SET `value` = '[1]', `value_long` = '[2]' WHERE `id` = '[3]'", $current_start_date, $past_start_dates, $info['id']);
		
		} else {
	
			// `value` holds current period start date
			// `value_long` holds comma delimited list of past start dates
	
			$get_info = lavu_query("SELECT `value`, `value_long` FROM `config` WHERE `type` = 'location_config_setting' AND `setting` = 'apple_pay_periods' AND `location` = '[1]'", $locationid);
			if (@mysqli_num_rows($get_info) > 0) {
				$info = mysqli_fetch_assoc($get_info);
				$current_start_date = $info['value'];
				$past_start_dates = $info['value_long'];
			} else {
				$now_time = localize_datetime(date("Y-m-d H:i:s"), $location_info['timezone']);
				$current_start_date = $now_time;
				$past_start_dates = "";
				$create_record = lavu_query("INSERT INTO `config` (`location`, `type`, `setting`, `value`) VALUES ('[1]', 'location_config_setting', 'apple_pay_periods', '[2]')", $locationid, $now_time);
			}
		}
		
		
		
		echo "<br><br>Current Pay Period Started: $current_start_date";

		echo "<form name='the_form' action='' method='POST'><input type='hidden' name='start_new' value='1'><form>";
		
		echo "<br><br><input type='button' value='Start New Pay Period' onclick='if (confirm(\"Are you sure you want to start a new pay period?\")) the_form.submit();'>";
				
		if ($past_start_dates != "") {
			$dates_list = explode(",", $past_start_dates);
			echo "<br><br><br>Last 5 pay periods started:<br>";
			for ($d = 0; $d < count($dates_list); $d++) {
				echo "<br>".$dates_list[$d];
				if ($d >= 4) break;
			}
		}
		

	}
?>
