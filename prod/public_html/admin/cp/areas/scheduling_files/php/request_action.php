<?php

//ini_set("display_errors", "1");


function translateRowsForDataname($ids) {
	$s_in_clause = ConnectionHub::getConn('rest')->getDBAPI()->arrayToInClause($ids, TRUE);
	$a_shiftrows = ConnectionHub::getConn('rest')->getDBAPI()->getAllInTable('user_schedules', NULL, FALSE,
		array('whereclause'=>'WHERE `id` '.$s_in_clause));
	if (count($a_shiftrows) == 0 || !isset($a_shiftrows[0]['start_datetime']))
		return;
	foreach($a_shiftrows as $a_shiftrow) {
		modifyShiftrow($a_shiftrow);
		$a_vals = $a_shiftrow;
		lavu_query("UPDATE `user_schedules` SET `start_datetime`='[start_datetime]',`end_datetime`='[end_datetime]' WHERE `id`='[id]'", $a_vals);
	}
	return count($a_shiftrows);
}

function modifyShiftrow(&$a_shiftrow) {
	$s_date = translateShiftRowToRegularDateTime($a_shiftrow);
	$a_shiftrow['start_datetime'] = $s_date . ' ' . $a_shiftrow['start_time']. ':00';
	$a_shiftrow['end_datetime'] = $s_date . ' ' . $a_shiftrow['end_time']. ':00';
	$a_end_minutes = explode(':', $a_shiftrow['end_time']);
	$i_end_minutes = $a_end_minutes[0]*60+$a_end_minutes[1];
	if (date('Y-m-d', $i_end_minutes*60+strtotime($s_date)) != $s_date) {
		$hours = str_pad( floor(($i_end_minutes-24*60)/60), 2, '0', STR_PAD_LEFT);
		$minutes = str_pad( floor(($i_end_minutes-24*60)-$hours*60), 2, '0', STR_PAD_LEFT);
		$a_shiftrow['end_datetime'] = date('Y-m-d', strtotime($s_date)+60*60*26) . ' ' . $hours . ':' . $minutes . ':00';
	}
}

function translateShiftRowToRegularDateTime($shiftRow){
	//Needed Vars
	$sundayBaseValue = $shiftRow['date'];
	$dayOfWeekOffset  = $shiftRow['dayofweek'];
	
	//Split dateTime into parts.
	$sundayBaseValueParts = explode( '/', $shiftRow['date'] );
	$sundayBase_DayVal   = $sundayBaseValueParts[1];
	$sundayBase_MonthVal = $sundayBaseValueParts[0];
	$sundayBase_yearVal  = $sundayBaseValueParts[2];
	
	//Calculate the exact date.
	$daysForwardOffset = $shiftRow['dayofweek'];
	$sundaysDate = '20' . $sundayBase_yearVal . "-" . $sundayBase_MonthVal . "-" . $sundayBase_DayVal;
	
	$finalTime = strtotime( date("Y-m-d", strtotime($sundaysDate)) . " +$daysForwardOffset day");
	$finalDate = date('Y-m-d', $finalTime);
	return $finalDate;
}

// PLEASE NOTE!!!!!!
// imports one week's schedule on top of the current week's schedule, but
// DOES NOT IMPORT CONFLICTING SCHEDULES
function import_shifts($locationid, $from_date, $to_date, $user_id) {
	if ($from_date == "" or $to_date == "")
		return array( FALSE, "badData?Could not import shifts, bad data was received" );
	$inserted_ids = array();
	
	$from_query = NULL;
	$to_query = NULL;
	$from_date = trim($from_date);
	$to_date = trim($to_date);
	if ($user_id != "") {
		$from_query = lavu_query("SELECT * FROM `user_schedules` WHERE `date` = '[1]' AND `user_id` = '[2]'", $from_date, $user_id);
		$to_query = lavu_query("SELECT * FROM `user_schedules` WHERE `date` = '[1]' AND `user_id` = '[2]'", $to_date, $user_id);
	} else {
		$from_query = lavu_query("SELECT * FROM `user_schedules` WHERE `date` = '[1]'", $from_date);
		$to_query = lavu_query("SELECT * FROM `user_schedules` WHERE `date` = '[1]'", $to_date);
	}
	
	error_log($locationid.':'.$from_date.':'.$to_date.':'.$user_id);
	
	if (mysqli_num_rows($from_query) > 0) {
		$froms = array();
		$tos = array();
		while($row = mysqli_fetch_assoc($to_query)) {
			$row['id'] = NULL;
			$row['loc_id'] = $locationid;
			$tos[] = $row;
		}
		while($row = mysqli_fetch_assoc($from_query)) {
			$row['date'] = $to_date;
			$row['id'] = NULL;
			$row['loc_id'] = $locationid;
			$from_shift = $row;
			$no_collision = TRUE;
			// check for conflicts (aka collisions)
			foreach($tos as $to_index=>$to_shift) {
				$similarities = 0;
				foreach($to_shift as $key=>$value) {
					if ($key == "start_time" || $key == "end_time")
						continue;
					if ($value == $from_shift[$key])
						$similarities++;
				}
				if ($similarities == count($to_shift)-2) {
					$st1 = strtotime($from_shift['start_time']);
					$st2 = strtotime($to_shift['start_time']);
					$et1 = strtotime($from_shift['end_time']);
					$et2 = strtotime($to_shift['end_time']);
					if ( ($st1 >= $st2 && $st1 < $et2) or ($st2 >= $st1 && $st2 < $et1) ) {
						$no_collision = FALSE;
						break;
					}
				}
			}
			if ($no_collision == FALSE)
				continue;
			$froms[] = $row;
		}
		
		$values_string = "";
		$example_shift = $froms[0];
		foreach($example_shift as $key => $value) {
			if ($values_string != "")
				$values_string .= ", ";
			$values_string .= "'[$key]'";
		}
		foreach($froms as $shift) {
			lavu_query("INSERT INTO `user_schedules` VALUES (" . $values_string . ")", $shift);
			$inserted_ids[] = lavu_insert_id();
		}
		translateRowsForDataname($inserted_ids);
	} else {
		return array( FALSE, "badDates?No shifts could be found on the week of " . $from_date);
	}
	
	return array( TRUE, "success", $inserted_ids );
}

function save_preference($locationid, $type_of_preference, $preference_name, $preference) {
	$query = FALSE;
	switch($preference_name) {
		case "shift_editor_delete_destination_week":
		case "shift_editor_advanced_time_enabled":
			lavu_query("DELETE FROM `config` WHERE `location`='[1]' AND `setting`='[2]'", $locationid, $preference_name);
			$query = lavu_query("INSERT INTO `config` (`location`, `type`, `setting`, `value`) VALUES ('[1]', '[2]', '[3]', '[4]')", $locationid, $type_of_preference, $preference_name, $preference);
			break;
	}
	if ($query === FALSE) {
		return array( FALSE, "badPreferenceName?Bad preference " . $preference_name );
	}
	if (ConnectionHub::getConn('rest')->affectedRows() == 0) {
		return array( FALSE, "badResult?Failed to save preference " .$preference_name. " (" .$preference.")" );
	}
	return array( TRUE, "success" );
}

function main() {
	function get_post($var_name) {
		if (isset($_POST[$var_name]))
			return $_POST[$var_name];
		return "";
	}

	$action = get_post("action");
	$from_date = get_post("from_date");
	$to_date = get_post("to_date");
	$user_id = get_post("user_id");
	$location = get_post("location");
	$preference_name = get_post("preference_name");
	$preference = get_post("preference");
	$result = array( FALSE, "missingLocation?Location of site not known" );

	// based on date format setting
	$date_setting = "date_format";
	$location_date_format_query = lavu_query( "SELECT `location`, `setting`, `value` FROM `config` WHERE `setting` = '[1]'",  $date_setting);
	if( !$location_date_format_query || !mysqli_num_rows($location_date_format_query)){
		$date_format = array("value" => 2);
	}else{
		$date_format = mysqli_fetch_assoc( $location_date_format_query );
	}
	$change_format = explode("/", $from_date);
	//1:d/m/y
	//2:m/d/y
	//3:y/m/d
	switch ($date_format['value']){
		case 1:$from_date = trim($change_format[1]) . "/" . trim($change_format[0]) . "/" . trim($change_format[2]) ; // m/d/y
		break;
		case 2:$from_date = trim($change_format[0]) . "/" . trim($change_format[1]) . "/" . trim($change_format[2]) ; // m/d/y
		break;
		case 3:$from_date = trim($change_format[1]) . "/" . trim($change_format[2]) . "/" . trim($change_format[0]) ; // m/d/y
		break;
		default:$from_date = trim($change_format[0]) . "/" . trim($change_format[1]) . "/" . trim($change_format[2]) ; // m/d/y
	}

	if ($location != "") {
		switch ($action)	{
		case "save_preference":
			$result = save_preference($location, "preference", $preference_name, $preference);
			break;
		case "duplicate":
			lavu_query("DELETE FROM `user_schedules` WHERE `loc_id`='[1]' AND `date`='[2]'", $location, $to_date);
			$result = import_shifts($location, $from_date, $to_date, $user_id);
			break;
		case "import":
			$result = import_shifts($location, $from_date, $to_date, $user_id);
			break;
		default:
			$result = array( FALSE, "badRequest?Unrecognized request from client" );
		}
	}
	
	if (gettype($result) == "array" && count($result) > 1) {
		if ($result[0] == TRUE) {
			echo "success";
		} else {
			echo $result[1];
		}
	} else {
		echo "failure";
	}
}

main();

?>
