<?php
/******************************************************************************
 * Notes on how to use this file:
 * Call the function tc_open_time_chart(itemproperty) when you want to open the table
 * 	Saved data will be in the hidden input values of id="tc_time_chart_item_property", id="tc_time_chart_start_time", and id="tc_time_chart_end_time"
 * 		The data in tc_time_chart_item_property will be the same as was passed into tc_open_time_chart
 * 		The start and end times will be in 24 hr hhmm form (eg 1415)
 * Change the time range that the table displays with tc_change_default_times()
 * When the table is closed, the "tc_submit_time_button" button will be clicked. Add a div to your file that looks something like:
 * 	<div style="display:none;"><button id="tc_submit_time_button" onclick="hh_submit_time();">Submit Time</button></div>
 * If you want to add extra data to the top, left, bottom, or right of the table, add a line like the following to your html
 * 	(add "insertidtag" or "replaceid" to create unique ids ("insertidtag" will be replaced with "tc_", while "replaceid" will be replaced with ""))
 * 	<div style="display:none;" id="tc_extra_content_top_content"><font id="insertidtagtop_stuff">...top stuff...</font></div>
 * 	<div style="display:none;" id="tc_extra_content_left_content"><font id="replaceidtagleft_stuff">...left stuff...</font></div>
 * 	<div style="display:none;" id="tc_extra_content_bottom_content">...bottom stuff...</div>
 * 	<div style="display:none;" id="tc_extra_content_right_content">...right stuff...</div>
 *
 * @author Benjamin Bean
 * @email benjamin@poslavu.com
 *****************************************************************************/
	$locallocationid = 0;
	if (!isset($locationid)) {
		$locallocationid = admin_info("loc_id");
	} else {
		$locallocationid = $locationid;
	}
	$locationid = $locallocationid;
?>

<script type="text/javascript">
	// @tc_earliest_shift_time the time that the shifts start at
	// @tc_night_shift_start_time the time that night shifts start at (if tc_longer_than_day_shifts_enabled)
	// @tc_night_shift_end_time the time that night shifts end at (if tc_longer_than_day_shifts_enabled)
	// 			if not tc_longer_than_day_shifts_enabled, shifts end at this time or midnight, whichever is earlier
	// @tc_longer_than_day_shifts_enabled enable or disable night shifts
	tc_earliest_shift_time = 0; // 12 am
	tc_night_shift_start_time = 1200; // 12 pm
	tc_night_shift_end_time = 600; // 6 am
	tc_longer_than_day_shifts_enabled = true;
	function tc_change_default_times(est, nsst, nset, ltdse) {
		tc_earliest_shift_time = est;
		tc_night_shift_start_time = nsst;
		tc_night_shift_end_time = nset;
		tc_longer_than_day_shifts_enabled = ltdse;
	}
	
	// opens the time chart, including the overlay, in the center of the document with the overlay covering the entire window
	// @itemproperty the property of the item to be preserved
	// @st the currently selected start time in 24 hr hhmm form (eg 1415) (can be null)
	// @st the currently selected end time in 24 hr hhmm form (eg 1415) (must be null if st is null)
	function tc_open_time_chart(itemproperty, st, et) {
		var fullwidth = parseInt($(window).width());
		var fullheight = parseInt($(window).height());
		var destheight = Math.max(fullheight-180, Math.min(fullheight-180, 400));
		$("#tc_time_chart_container").css({ height: destheight });
		if ($("#tc_time_chart").length > 0)
			$("#tc_time_chart").remove();
		var timecharttext = tc_build_time_chart(itemproperty,st,et,null);
		if (timecharttext == "")
			return;
		$("#tc_time_chart_container").html("");
		$("#tc_time_chart_container").append(timecharttext);
		
		if ($("#tc_extra_content_top_content").length > 0) {
			if ($("#tc_top_content_container_container").length > 0) $("#tc_top_content_container_container").remove();
			$("#tc_top_content_container").append( "<div id=\"tc_top_content_container_container\">" + $("#tc_extra_content_top_content").html().replace(/insertidtag/g,"tctag_").replace(/replaceid/g,"") + "</div>" );
			$("#tc_top_content_container_container").height(tc_get_max_height_of_children("tc_top_content_container_container"));
		}
		if ($("#tc_extra_content_left_content").length > 0) {
			if ($("#tc_left_content_container_container").length > 0) $("#tc_left_content_container_container").remove();
			$("#tc_left_content_container").append( "<div id=\"tc_left_content_container_container\">" + $("#tc_extra_content_left_content").html().replace(/insertidtag/g,"tctag_").replace(/replaceid/g,"") + "</div>" );
		}
		if ($("#tc_extra_content_right_content").length > 0) {
			if ($("#tc_right_content_container_container").length > 0) $("#tc_right_content_container_container").remove();
			$("#tc_right_content_container").append( "<div id=\"tc_right_content_container_container\">" + $("#tc_extra_content_right_content").html().replace(/insertidtag/g,"tctag_").replace(/replaceid/g,"") + "</div>" );
		}
		if ($("#tc_extra_content_bottom_content").length > 0) {
			if ($("#tc_bottom_content_container_container").length > 0) $("#tc_bottom_content_container_container").remove();
			$("#tc_bottom_content_container").append( "<div id=\"tc_bottom_content_container_container\">" + $("#tc_extra_content_bottom_content").html().replace(/insertidtag/g,"tctag_").replace(/replaceid/g,"") + "</div>" );
			$("#tc_bottom_content_container_container").height(tc_get_max_height_of_children("tc_bottom_content_container_container"));
		}
		
		var width = parseInt($("#tc_time_selector_table").width());
		var height = parseInt($("#tc_time_selector_table").height());
		var destx = fullwidth/2-width/2;
		var desty = fullheight/2-height/2;
		var padding = parseInt($("#tc_time_selector_table").css("padding-left"));
		var border = parseInt($("#tc_time_selector_table").css("border-left-width"));
		destx = destx-padding-border;
		desty = desty-padding-border;
		if($.browser.msie) {
			var parentx = parseInt($("#tc_time_selector_table").parent().position().left);
			var parenty = parseInt($("#tc_time_selector_table").parent().position().top);
			var scrollx = parseInt($(document).scrollLeft());
			var scrolly = parseInt($(document).scrollTop());
			destx += -parentx+scrollx;
			desty += -parenty+scrolly;
		}
		$("#tc_time_selector_table").css({ left: destx, top: desty, display:"none" });
		tc_show_time_chart(200);
		
		$("#tc_timetextboxes_st").val(tc_display_time(st));
		$("#tc_timetextboxes_et").val(tc_display_time(et));
		$("#tc_time_chart_start_time").val(st);
		$("#tc_time_chart_end_time").val(et);
		
		if ((st || st == 0) && et) {
			var displaytime = tc_display_time(st);
			var tds = $("td:contains('"+displaytime+"')");
			var td = null;
			for (var i = 0; i < tds.length; i++) { if ($(tds[i]).html() == displaytime) { td = $(tds[i]); break; }}
			var div = td;
			while (div.prop("tagName") != "DIV" && div.prop("tagName") != "div") div = div.parent();
			var divid = "tc_time_chart_container";
			
			$("#"+divid).scrollTop(0);
			setTimeout('tc_scroll("'+divid+'",0,'+(td.position().top-div.position().top)+',true);', 200);
		}
	}
	
	// returns true of false, as to the current state of the time chart
	tc_time_chart_open_retval = false;
	function tc_time_chart_open() {
		if ($("#tc_time_chart").length > 0)
			return tc_time_chart_open_retval;
		return false;
	}
</script>

<div id="tc_scroll_tag" style="display:none;opacity:1"></div>
<div id='tc_time_chart_overlay' style='position:fixed;_position:absolute;opacity:0.6;background-color:black;left:0;right:0;width:0;height:0;display:none;z-index:42;' onclick='$("#tc_timetextboxes_submitbutton").click();'></div>
<table style="position:fixed;_position:absolute;left:0;top:0;width:auto;height:auto;background-color:#eee;padding:15px;border:5px;border-radius:5px;display:none;z-index:43;" id="tc_time_selector_table">
	<tr>
		<td id="tc_top_content_container" colspan="3"></td>
	</tr>
	<tr>
		<td id="tc_left_content_container"></td>
		<td>
			<table>
				<tr>
					<td>
					</td>
				</tr>
				<tr>
					<td><div id="tc_time_chart_container" style="width:250px;height:300px;overflow:auto;">&nbsp;</div></td>
				</tr>
				<tr>
					<td>
						<table style='width:250px'>
							<tr>
								<td style='text-align:left'>
									<?php
										$checked_query = lavu_query("SELECT `value` FROM `config` WHERE `location`='[1]' and `setting`='[2]'", $locationid, "shift_editor_advanced_time_enabled");
										$checked = "";
										if ($checked_query) {
											while ($checked_read = mysqli_fetch_assoc($checked_query)) {
												if ($checked_read["value"] == "1" || $checked_read["value"] == 1)
													$checked = " checked='checked'";
												break;
											}
										}
										echo "<input type='checkbox' id='tc_advanced_time_checkbox' onclick='tc_save_preference_checkbox(\"tc_advanced_time_checkbox\",\"shift_editor_advanced_time_enabled\");tc_show_if_checked(\"tc_advanced_time_checkbox\",\"tc_timetextboxes\");'".$checked.">Specify time exactly";
									?>
									<script type="text/javascript">setTimeout('tc_show_if_checked("tc_advanced_time_checkbox","tc_timetextboxes");',500);</script>
								</td>
								<td style='text-align:right'><font style='color:#555;font-weight:bold;cursor:pointer;visibility:hidden;' onclick='$("#tc_timetextboxes_submitbutton").click();'>FINISH</font></td>
							</tr>
							<tr>
								<td>
									<font style='font-style:italic;color:#aaa;' id='tc_advanced_time_checkbox_success'></font>
								</td>
								<td></td>
							</tr>
						</table>
						<br />
						<div id='tc_timetextboxes' style='display:none'>
							Specific time: <input type='text' size='7' id='tc_timetextboxes_st' onkeyup='tc_checktime_text(this,event);tc_fill_time_containers_from_specific_time_inputs("st");' value='4:00pm'> -
							<input type='text' size='7' id='tc_timetextboxes_et' onkeyup='tc_checktime_text(this,event);tc_fill_time_containers_from_specific_time_inputs("et");' value='8:00pm'>
							<input type='button' value='&#10003;' id='tc_timetextboxes_submitbutton' onclick='tc_submit_time();' />
						</div>
					</td>
				</tr>
			</table>
		</td>
		<td id="tc_right_content_container"></td>
	</tr>
	<tr>
		<td id="tc_bottom_content_container" colspan="3"></td>
	</tr>
</table>

<script type="text/javascript">
	function tc_get_max_height_of_children(parentid) {
		var jparent = $("#"+parentid);
		var children = jparent.children();
		var retval = 0;
		
		for (var i = 0; i < children.length; i++) {
			retval = Math.max(retval, parseInt($(children[i]).height()));
		}
		
		return retval+"px";
	}

	function tc_submit_time() {
		$("#tc_submit_time_button").click();
		setTimeout('tc_hide_time_chart();',30);
	}
	globaldiv = null;
	function tc_scroll(divid, scrollstart, scrolldest, dostart) {
		var div = $("#"+divid);
		
		if (dostart) {
			$("#tc_scroll_tag").stop(false, true);
			$("#tc_scroll_tag").css({ opacity: 0 });
			$("#tc_scroll_tag").animate({ opacity: 1 }, 200);
		}
	
		var percent = parseFloat($("#tc_scroll_tag").css("opacity"));
		
		if (percent > 0.999) {
			div.scrollTop(scrolldest);
		} else {
			div.scrollTop((scrolldest-scrollstart)*percent);
			setTimeout('tc_scroll("'+divid+'",'+scrollstart+','+scrolldest+');',15);
		}
	}
	
	// converts a 12hr time to 24hr time (eg 1:15pm -> 1315)
	function tc_convert_to_numeric_time(t) {
		var ampm = "am";
		if(t.indexOf('p') > 0 || t.indexOf('P') > 0)
			ampm = "pm";
		if(ampm=="")
		{
			return -1;
		}
		val_parts = t.split(':');
		var hours = "";
		var mins = "";
		if(val_parts.length > 1)
		{
			hours = val_parts[0];
			mins = val_parts[1];
		}
		else
		{
			if (t.match(/(a|A|p|P)/)) {
				hours = val_parts[0];
				mins = "";
			} else {
				hours = "";
				mins = val_parts[0];
			}
		}
		hours = hours.replace(/[^0-9]/g, '');
		mins = mins.replace(/[^0-9]/g, '');
		if (hours == "") hours = "0";
		if (mins == "") mins = "0";
		
		hours = Math.min(tc_night_shift_end_time+24, Math.max(0, parseInt(hours)));
		mins = Math.min(60, Math.max(0, parseInt(mins)));
		if (hours == 12 && ampm == "am") hours = 0;
		if (hours < 12 && ampm == "pm") hours += 12;
		hours *= 100;
		return (hours + mins);
	}
	
	// forces the contents of the object to fit within the
	// hh:mm(am|pm) format
	function tc_checktime_text(object,event) {
		var jobject = $(object);
		var text = jobject.val();
		var newtext = "";
		var lefttext = "";
		var righttext = "";
		
		// get the text before and after the ":"
		if (text.indexOf(":") > -1) {
			lefttext = text.split(":")[0];
			righttext = text.split(":")[1];
		} else {
			if (text.match(/(a|A|p|P)/)) {
				lefttext = text;
			} else {
				righttext = text;
			}
		}
		
		// enforce the parameters of the text field
		if (lefttext != "") {
			lefttext = lefttext.match(/[0-9]*/);
			if (!lefttext) lefttext = "";
			lefttext += "";
			lefttext = Math.min(parseInt(lefttext),12);
			lefttext = lefttext+text.replace(/[0-9]/g,'').match(/[ap]?m?/);
		}
		if (righttext != "") {
			righttext = righttext.match(/[0-9]*[ap]?m?/);
			if (!righttext) righttext = "";
			righttext += "";
			var rightinttext = righttext.match(/[0-9]*/);
			if (rightinttext != "") {
				if (rightinttext.indexOf("00") == -1) {
					if (rightinttext != "0") {
						var firstchar = righttext.match(/^0/)?"0":"";
						rightinttext = firstchar + Math.min(parseInt(rightinttext),59) + "";
					}
				}
			}
			righttext = rightinttext+righttext.replace(/[0-9]*/,"").match(/[ap]?m?/);
		}
		
		// replace if different
		if (text.indexOf(":") > -1) {
			newtext = lefttext+":"+righttext;
		} else {
			if (lefttext) {
				newtext = lefttext;
			} else {
				newtext = righttext;
			}
		}
		if (text != newtext) {
			jobject.val(newtext);
		}
		
		// handle "enter key" input
		if (event && event.which && event.which == 13) {
			var st_or_et = ""+jobject.prop("id").match(/_[es]t/);
			if (st_or_et == "_st") {
				$("#tc_timetextboxes_et").focus();
			} else {
				setTimeout('$("#tc_timetextboxes_submitbutton").click();',30);
			}
		}
	}
	
	// fill in the start time and end time objects
	// there are two objects that hold the returned time, which are created in tc_build_time_chart
	// this function fills in the numbers of those boxes
	function tc_fill_time_containers_from_specific_time_inputs(st_or_et) {
		switch(st_or_et) {
		case "st":
			var timetext = $("#tc_timetextboxes_st").val();
			$("#tc_time_chart_start_time").val(tc_convert_to_numeric_time(timetext));
			break;
		case "et":
			var timetext = $("#tc_timetextboxes_et").val();
			$("#tc_time_chart_end_time").val(tc_convert_to_numeric_time(timetext));
			break;
		}
	}
	
	tc_send_save_preference_retval = null;
	function tc_send_save_preference(in_preference_name, in_preference) {
		var rand = Math.ceil(Math.random()*2);
		tc_send_save_preference_retval = false;
		$.ajax({
			url: "index.php?widget=scheduling_files/php/request_action",
			type: "POST",
			async: false,
			cache: false,
			timout: 10000,
			data: { action: "save_preference", location: <?php echo $locationid; ?>, preference_name: in_preference_name, preference: in_preference, random_num: rand },
			
			success: function (string) {
				if( string.indexOf('success') !== -1 ){
					tc_send_save_preference_retval = true;
				}else{
					var error_type = string.split("?")[0];
					var error_message = string.split("?")[1];
					console.log("Error (" + error_type + "): " + error_message);
					tc_send_save_preference_retval = false;
				}
			},
			
			error: function(j ,t, e){
				tc_send_save_preference_retval = false;
			}
		});
		return tc_send_save_preference_retval;
	}
	
	tc_save_preference_checkbox_checkboxid = null;
	tc_save_preference_checkbox_preference_name = null;
	tc_save_preference_checkbox_value = null;
	function tc_save_preference_checkbox(checkboxid, preference_name) {
		var jcheckbox = $("#"+checkboxid);
		$("#"+jcheckbox.prop("id")+"_success").html("saving...");
		tc_save_preference_checkbox_value = null;
		tc_save_preference_checkbox_checkboxid = checkboxid;
		tc_save_preference_checkbox_preference_name = preference_name;
		setTimeout(function() {
			var jcheckbox = $("#"+checkboxid);
			tc_save_preference_checkbox_waiting_for_response(tc_save_preference_checkbox_checkboxid, tc_save_preference_checkbox_preference_name);
			tc_save_preference_checkbox_value = tc_send_save_preference(tc_save_preference_checkbox_preference_name, jcheckbox.attr("checked")?"1":"0");
		}, 200);
	}
	
	function tc_save_preference_checkbox_waiting_for_response(checkboxid, preference_name) {
		if (!tc_save_preference_checkbox_value) {
			setTimeout('tc_save_preference_checkbox_waiting_for_response("'+checkboxid+'","'+preference_name+'");', 100);
			return;
		}
		
		var jcheckbox = $("#"+checkboxid);
		if (tc_save_preference_checkbox_value) {
			$("#"+jcheckbox.prop("id")+"_success").html("<font id='success_saving_font'>preference saved</font>");
		} else {
			$("#"+jcheckbox.prop("id")+"_success").html("<font id='success_saving_font'>preference not saved</font>");
		}
	}
	
	function tc_show_if_checked(checkboxid, itemid) {
		var jcheckbox = $("#"+checkboxid);
		var jitem = $("#"+itemid);
		if (jcheckbox.prop("checked")) {
			jitem.stop(false, true);
			jitem.show(200);
		} else {
			jitem.stop(false, true);
			jitem.hide(200);
		}
	}
	
	function tc_show_time_chart(time_in_millisecs) {
		$("#tc_time_selector_table").stop(false, true);
		$("#tc_time_selector_table").show(time_in_millisecs);
		$("#tc_time_chart_overlay").stop(false, true);
		$("#tc_time_chart_overlay").css({ left: 0, top: 0, width: $(window).width(), height: $(window).height(), opacity: 0 });
		if ($.browser.msie) {
			var parentx = $("#tc_time_chart_overlay").parent().position().left;
			var parenty = $("#tc_time_chart_overlay").parent().position().top;
			var scrollx = $(document).scrollLeft();
			var scrolly = $(document).scrollTop();
			destx = -parentx+scrollx<?php
				if (isset($_GET['mode']))
					if ($_GET['mode'] == "meal_periods")
						echo "-50"; ?>;
			desty = -parenty+scrolly;
			$("#tc_time_chart_overlay").css({ left: destx, top: desty });
		}
		$("#tc_time_chart_overlay").show();
		$("#tc_time_chart_overlay").animate({ opacity: 0.6 }, time_in_millisecs);
		setTimeout('tc_update_overlay_position();', time_in_millisecs+100);
		setTimeout('tc_time_chart_open_retval = true;', time_in_millisecs);
	}
	
	function tc_update_overlay_position() {
		if ($("#tc_time_chart_overlay").length > 0 && parseFloat($("#tc_time_chart_overlay").css("opacity")) >= 0.1) {
			var width = $("#tc_time_chart_overlay").width();
			var height = $("#tc_time_chart_overlay").height();
			var currx = $("#tc_time_chart_overlay").position().left;
			var curry = $("#tc_time_chart_overlay").position().top;
			var destwidth = $(window).width();
			var destheight = $(window).height();
			var destx = 0;
			var desty = 0;
			if ($.browser.msie) {
				var parentx = $("#tc_time_chart_overlay").parent().position().left;
				var parenty = $("#tc_time_chart_overlay").parent().position().top;
				var scrollx = $(document).scrollLeft();
				var scrolly = $(document).scrollTop();
				destx += -parentx+scrollx<?php
				if (isset($_GET['mode']))
					if ($_GET['mode'] == "meal_periods")
						echo "-50"; ?>;
				desty += -parenty+scrolly;
				console.log(parentx+":"+scrollx);
			}
			if (width != destwidth || height != destheight || currx != destx || curry != desty)
				$("#tc_time_chart_overlay").css({ left: destx, top: desty, width: destwidth, height: destheight });
			console.log($("#tc_time_chart_overlay").position().left);
			var delaytime = 300;
			if ($.browser.msie) delaytime = 100;
			setTimeout('tc_update_overlay_position();', delaytime);
		}
	}
	
	function tc_hide_time_chart(time_in_millisecs) {
		$("#tc_time_selector_table").stop(false, true);
		$("#tc_time_selector_table").hide(200);
		$("#tc_time_chart_overlay").stop(false, true);
		$("#tc_time_chart_overlay").animate({ opacity: 0 }, { duration: time_in_millisecs, complete: function(){$("#tc_time_chart_overlay").hide()} });
		tc_time_chart_open_retval = false;
	}
	
	// builds the time chart (not including the specific time selector)
	// @itemproperty a single property of the item the time chart is affecting
	// @start_time the start time (eg 800 for 8:00am)
	// @end_time the end time (eg 1715 for 5:15pm)
	// @set_stime used within the function, always pass in null
	function tc_build_time_chart(itemproperty,start_time,end_time,set_stime) {
		var str = "";
		var stime = tc_earliest_shift_time;
		var etime = tc_night_shift_end_time;
		var slotsize = 30;
		
		if (tc_longer_than_day_shifts_enabled)
			etime += 2400;
		// for msie compatability
		// msie interprets "0800" as 0, whereas other browsers interpret it as 800
		console.log(start_time+"..."+end_time);
		if (start_time == null) { start_time = ""; }
		if (end_time == null) { end_time = ""; }
		start_time += "";
		end_time += "";
		if (start_time.match(/[1-9][0-9]*/))
			start_time = start_time.match(/[1-9][0-9]*/);
		else
			start_time = start_time.match(/[0-9]*/);
		if (end_time.match(/[1-9][0-9]*/))
			end_time = end_time.match(/[1-9][0-9]*/);
		else
			end_time = end_time.match(/[0-9]*/);
		
		var t = stime;
		var slotcount = 0;
		
		var h1 = -1;
		var h2 = -1;
		
		// parse the input start_time and end_time
		if (start_time != null && end_time != null)
		{
			h1 = parseInt(start_time);
			h2 = parseInt(end_time);
		}
		
		// enable or disable the specific times dialog
		if ($("#tc_timetextboxes_submitbutton").length > 0) {
			if (set_stime || set_stime === 0) {
				$("#tc_timetextboxes_submitbutton").attr("disabled","disabled");
			} else {
				$("#tc_timetextboxes_submitbutton").removeAttr("disabled");
			}
		}
		
		str += "<table cellspacing=0 cellpadding=2 id=\"tc_time_chart\">";
		str += "<tr><td><input type=\"hidden\" value=\""+itemproperty+"\" id=\"tc_time_chart_item_property\">";
		str += "<input type=\"hidden\" value=\""+((set_stime||set_stime===0)?set_stime:tc_convert_to_numeric_time($("#tc_timetextboxes_st").val()))+"\" id=\"tc_time_chart_start_time\">";
		str += "<input type=\"hidden\" value=\""+tc_convert_to_numeric_time($("#tc_timetextboxes_et").val())+"\" id=\"tc_time_chart_end_time\">";
		str += "</td></tr>";
		
		while(t <= etime && slotcount < 1000)
		{
			slotcount++;
			
			use_bgcolor = "#ddddee";
			use_bgcolor_over = "#bbbbcc";
			use_border_bottom = "#aabbee";
			use_text_color = "#555577";
			right_cell_text = "&nbsp;";
			click_code = "tc_choose_start_time(\"" +itemproperty + "\"," + h1 + "," + h2 + "," + t + ")";
			
			if (t == 2400) {
				use_bgcolor = "#eeeeee";
				use_bgcolor_over = "#eeeeee";
				use_border_bottom = "#dddddd";
				use_text_color = "#aaaaaa";
				right_cell_text = "(choose a start time)";
				click_code = "alert(\"First choose a start time.\")";
			}
			if (t > 2400) {
				use_bgcolor = "#eeeeee";
				use_bgcolor_over = "#eeeeee";
				use_border_bottom = "#dddddd";
				use_text_color = "#aaaaaa";
				right_cell_text = "(start after " + tc_display_time(tc_night_shift_start_time) + ")";
				click_code = "alert(\"Graveyard happy hours must start after " + tc_display_time(tc_night_shift_start_time) + ".\")";
			}
			
			if((set_stime || set_stime === 0) && set_stime !== "")
			{
				if(set_stime > t)
				{
					use_bgcolor = "#eeeeee";
					use_bgcolor_over = "#eeeeee";
					use_border_bottom = "#dddddd";
					use_text_color = "#aaaaaa";
					right_cell_text = "&nbsp;";
					click_code = "tc_choose_start_time(\"" +itemproperty + "\"," + h1 + "," + h2 + ",null)";
				}
				else if(set_stime==t)
				{
					use_bgcolor = "#555588";
					use_bgcolor_over = "#555588";
					use_border_bottom = "#555588";
					use_text_color = "#ffffff";
					right_cell_text = "(click to change start time)";
					click_code = "tc_choose_start_time(\"" +itemproperty + "\"," + h1 + "," + h2 + ",null)";
				}
				else if ( set_stime < t && (set_stime >= tc_night_shift_start_time || t <= 2400) )
				{
					use_bgcolor = "#ddddee";
					use_bgcolor_over = "#bbbbcc";
					use_border_bottom = "#aabbee";
					use_text_color = "#555577";
					right_cell_text = "&nbsp;";
					click_code = "$(\"#tc_time_chart_end_time\").val("+t+");tc_submit_time();";
				}
			}
			
			if(h1==t || h2==t)
				extra_style = "font-weight:bold; background-color:#bbbbdd;";
			//else if(h1 < t && h2 > t)
			//	extra_style = "font-weight:bold; background-color:#ddf;";
			else
				extra_style = "";
			str += "<tr bgcolor='" + use_bgcolor + "' onmouseover='this.bgColor = \"" + use_bgcolor_over + "\"' onmouseout='this.bgColor = \"" + use_bgcolor + "\"' onclick='" + click_code + "'><td align='right' style='color:" + use_text_color + "; font-family:Arial; font-size:11px; border-bottom:solid 1px " + use_border_bottom + "; cursor:pointer; " + extra_style + "'>";
			str += tc_display_time(t);
			str += "</td><td width='200' style='border-bottom:solid 1px " + use_border_bottom + "; cursor:pointer; color:" + use_text_color + "; font-family:Arial; font-size:11px' align='center'>";
			str += right_cell_text;
			str += "</td></tr>";
			
			t = tc_add_time(t,slotsize);
		}
		str += "</table>";
		
		return str;
	}
	
	// used by build_time_chart to increment the time slot display time
	// @bt the before time
	// @at the add time (slotsize time to add)
	function tc_add_time(bt,at) {
		var mins_bt = bt % 100;
		var hours_bt = (bt - mins_bt) / 100;
		var mins_at = at % 100;
		var hours_at = (at - mins_at) / 100;
		
		hours_bt += hours_at;
		mins_bt += mins_at;
		while(mins_bt > 59)
		{
			mins_bt -= 60;
			hours_bt ++;
		}
		
		return (hours_bt * 100) + mins_bt;
	}
	
	// used by build_time_chart to display a start time and query for an end time
	// @set_stime the start time to display
	function tc_choose_start_time(itemproperty, start_time, end_time, set_stime) {
		if ($("#tc_time_chart").length > 0)
			$("#tc_time_chart").remove();
		$("#tc_time_chart_container").append(tc_build_time_chart(itemproperty,start_time,end_time,set_stime));
	}
	
	// converts a 24hr time (eg 1215) to a 12hr time (eg 12:15pm)
	// @t the 24hr time to convert
	function tc_display_time(t) {
		var mins = t % 100;
		var hours = (t - mins) / 100;
		
		var set_mins = "";
		if(mins < 10) set_mins = "0" + mins; else set_mins = mins;
		if(hours % 24 ==0) return "12:" + set_mins + "am";
		else if(hours % 24 < 12) return (hours % 24) + ":" + set_mins + "am";
		else if(hours % 24 ==12) return "12:" + set_mins + "pm";
		else return ((hours % 24) - 12) + ":" + set_mins + "pm";
	}
</script>