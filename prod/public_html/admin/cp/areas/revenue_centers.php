<?php
	if($in_lavu)
	{
		echo "<br><br>";

		$tablename = "revenue_centers";
		$forward_to = "index.php?mode={$section}_{$mode}";
		//$filter_by = "`location`='$locationid'";

		$fields = array();
		$fields[] = array(speak("Name"),"name","text","","list:yes");
		$fields[] = array(speak("Submit"),"submit","submit");

		$exists_query = lavu_query("SELECT `id` FROM `".$tablename."` LIMIT 1");
		if ($exists_query !== FALSE) {
			require_once(resource_path() . "/browse.php");
		} else {
			echo "You don't have the necessary database to view this page.";
		}
	}

	function echoRevenueCentersAsDropDown($id) {
	    $rcs = get_revenue_centers();
	    if (!$rcs) { $rcs = array(); }
	    $blank = array(); $blank['name'] = ' '; $blank['id'] = -1;
	    array_unshift($rcs, $blank);
	    $selected = FALSE;
	    echo '<select id="'.$id.'">';
    	foreach($rcs as $rc){
    		if ($rc['name'] == "")
    			continue;
    		echo '<option value="'.$rc['id'].'"';
    		if (!$selected) {
	    		echo " selected";
	    		$selected = TRUE;
    		}
    		echo '>'.$rc['name'].'</option>';
    	}
    	echo '</select>';
    	echo ' <font style="color:#aaa;font-style:italic" id="'.$id.'_preference"></font>';
    }

    // gets the default values for the revenue centers from the `config` table
    // returns an array in the form [['name'=namedefault,'id'=iddefault], ['name'=namequickserve,'id'=idquickserve], ['name'=nametabs,'id'=idtabs]
    function get_defaults($location) {
	    $defaults = array();
    	$defaults_strings = array();
    	$defaults_strings[] = get_preference($location, "revenue_center_default_Default");
    	$defaults_strings[] = get_preference($location, "revenue_center_default_QuickServe");
    	$defaults_strings[] = get_preference($location, "revenue_center_default_Tabs");

    	$badval = array();
    	$badval[] = '';
    	$badval[] = '-1';

    	foreach ($defaults_strings as $rcid) {
			$default = $rcid;
			if ($default === FALSE || strlen($default) < 1) { $default = NULL; }
			if ($default) {
				$default = lavu_query("SELECT `name`,`id` FROM `revenue_centers` WHERE `id` = '[1]' and `_deleted` = '0'", $default);
				if ($default) {
					$defaults[] = mysqli_fetch_assoc($default);
				} else {
					$defaults[] = $badval;
				}
			} else {
				$defaults[] = $badval;
			}
		}
		return $defaults;
    }

    // gets a list of unique revenue centers as an array [['name':name1, 'id':id1], ['name':name2, 'id':id2], ...]
    // returns FALSE if there are no revenue_centers
    function get_revenue_centers($ignore_deleted = FALSE) {
    	global $locationid;
    	$defaults = get_defaults($locationid);

	    $result = lavu_query("SELECT `name`, `id` FROM `revenue_centers`  WHERE `_deleted` = '0' ORDER BY `name`");
	    if ($result) {
		    $oldrows = array();
		    if (mysqli_num_rows($result))
		    	while($oldrow = mysqli_fetch_assoc($result))
		    		$oldrows[] = $oldrow;
		    foreach ($defaults as $default)
		    	$oldrows[] = $default;
		    $newrows = array();
		    foreach ($oldrows as $oldrow)
		    	if (!in_array($oldrow, $newrows))
		    		$newrows[] = $oldrow;
		    return $newrows;
	    }
	    return FALSE;
    }

    function get_preference($locationid, $preference_name) {
		$result = lavu_query("SELECT `value` FROM `config` WHERE `location` = '[1]' AND `setting` = '[2]' LIMIT 1", $locationid, $preference_name);
		if ($result)
			if ($row = mysqli_fetch_assoc($result))
				return $row['value'];
		return FALSE;
	}
?>

<script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>

<script type="text/javascript">
	sendProcessPreviousOrders_retval = null;
	function sendProcessPreviousOrders() {
	  	var rand = Math.ceil(Math.random()*2);
	  	sendProcessPreviousOrders_retval = false;

	  	$.ajax({
	  		url: "index.php?widget=revenue_center_files/request_action_process_orders",
	  		type: "POST",
	  		async: false,
	  		cache: false,
	  		data: { action: "process_previous_orders", location: "<?php global $locationid; echo $locationid; ?>", random_val: rand },

	  		success: function (string){
	  		    if( string.indexOf('success') !== -1 ){
	  		    	sendProcessPreviousOrders_retval = true;
	  		    } else {
		  		    sendProcessPreviousOrders_retval = false;
	  		    }
	  		},

	  		error: function(j ,t, e){
				sendProcessPreviousOrders_retval = false;
			}

	  	});

	  	return sendProcessPreviousOrders_retval;
	}

	sendCanShowPreviousOrders_retval = null;
	function sendCanShowPreviousOrders() {
	  	var rand = Math.ceil(Math.random()*2);
	  	sendCanShowPreviousOrders_retval = false;

	  	$.ajax({
	  		url: "index.php?widget=revenue_center_files/request_action_process_orders",
	  		type: "POST",
	  		async: false,
	  		cache: false,
	  		data: { action: "previous_orders_open", location: "<?php global $locationid; echo $locationid; ?>", random_val: rand },

	  		success: function (string){
	  		    if (string.indexOf("true") !== -1) {
		  		    sendCanShowPreviousOrders_retval = true;
	  		    } else {
		  		    sendCanShowPreviousOrders_retval = false;
	  		    }
	  		},

	  		error: function(j ,t, e){
				sendCanShowPreviousOrders_retval = false;
			}

	  	});

	  	return sendCanShowPreviousOrders_retval;
	}

	function processPreviousOrders() {
		if (sendProcessPreviousOrders()) {
			$("#apply_to_previous_orders_tr1").hide();
			$("#apply_to_previous_orders_tr2").hide();
		}
	}

	function showPreviousOrdersMenu() {
		var val = sendCanShowPreviousOrders();
		if (val) {
			$("#apply_to_previous_orders_tr1").show();
			$("#apply_to_previous_orders_tr2").show();
		}
	}

	sendChangeRC_retval = null;
	function sendChangeRC(which, in_value) {
	  	var rand = Math.ceil(Math.random()*2);
	  	sendChangeRC_retval = "preference not saved";

	  	$.ajax({
	  		url: "index.php?widget=revenue_center_files/request_action_process_orders",
	  		type: "POST",
	  		async: false,
	  		cache: false,
	  		data: { action: "set_default", whichdefault: which, value: in_value, location: "<?php global $locationid; echo $locationid; ?>", random_val: rand },

	  		success: function (string){
	  		    if (string.indexOf("success") !== -1) {
		  		    sendChangeRC_retval = "preference saved";
	  		    } else {
		  		    sendChangeRC_retval = "error (" + string + ")";
	  		    }
	  		},

	  		error: function(j ,t, e){
				sendChangeRC_retval = "request failed (" + e + ")";
			}

	  	});

	  	return sendChangeRC_retval;
	}

	function changeRC(which) {
		var select_id = "#select" + which + "RC";
		var preference_id = "#select" + which + "RC_preference";
		var rcid = $(select_id).val();
		var val = sendChangeRC(which, rcid);
		$(preference_id).html(val);
	}
	stuffnstuff = null;
	function onreadyFunction() {
		$(document).ready(function() {
			showPreviousOrdersMenu();
			<?php
				global $locationid;
				$defaults = get_defaults($locationid);
			?>
			$("#selectDefaultRC").unbind();
			$("#selectDefaultRC").val(<?php echo $defaults[0]['id']; ?>);
			$("#selectDefaultRC").change(function() { changeRC("Default"); });
			$("#selectQuickServeRC").unbind();
			$("#selectQuickServeRC").val(<?php echo $defaults[1]['id']; ?>);
			$("#selectQuickServeRC").change(function() { changeRC("QuickServe"); });
			$("#selectTabsRC").unbind();
			$("#selectTabsRC").val(<?php echo $defaults[2]['id']; ?>);
			$("#selectTabsRC").change(function() { changeRC("Tabs"); });
		});
	}

	setTimeout('onreadyFunction();', 1000);
</script>

<link rel="stylesheet" type="text/css" href="areas/meal_periods/cp_areas.css">

<br /><br />
<table class="cp_areas">
	<tr>
		<td class="section_break" colspan="2">
			Default Revenue Centers
		</td>
	</tr>
	<tr>
		<td class="prompt">
			Default revenue center for tables:
		</td>
		<td class="query">
			<?php echoRevenueCentersAsDropDown("selectDefaultRC"); ?>
		</td>
	</tr>
	<tr>
		<td class="prompt">
			Revenue center for quick serves:
		</td>
		<td class="query">
			<?php echoRevenueCentersAsDropDown("selectQuickServeRC"); ?>
		</td>
	</tr>
	<tr>
		<td class="prompt">
			Revenue center for tabs:
		</td>
		<td class="query">
			<?php echoRevenueCentersAsDropDown("selectTabsRC"); ?>
		</td>
	</tr>

	<tr id="apply_to_previous_orders_tr1" style="display:none;">
		<td class="section_break" colspan="2">
			Previous Orders
		</td>
	</tr>
	<tr id="apply_to_previous_orders_tr2" style="display:none;">
		<td  class="prompt">
			Apply to previous orders:
		</td>
		<td class="query">
			<input type="button" value='<?php echo speak("Apply");?>' onclick="processPreviousOrders();"><br />
			Applies the current revenue centers to orders<br />opened before revenue centers were being processed.<br /><br />
			Once the orders have been processed,<br />they cannot be processed again.
		</td>
	</tr>
</table>
<script type= 'text/javascript'>
	// window.onload = function(){
	// 	document.getElementById('zenbox_tab').style.display='none';
	// }
	// 	var a = document.getElementsByClassName('content')[0];
	// 	document.getElementsByTagName('body')[0].innerHTML = a.innerHTML;
	// 	window.document.getElementsByClassName("internalMenu")[0].style.display='none';
	// 	document.getElementById('main_content_area').style.width='600px';
	// 	document.getElementsByTagName("body")[0].style.width ='600px';
	// 	document.getElementsByTagName("body")[0].style.backgroundColor='white';
	// 	document.getElementsByTagName("body")[0].style.overflowX = 'hidden';

</script>