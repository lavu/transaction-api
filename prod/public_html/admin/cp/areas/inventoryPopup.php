
 <?php

// START POP UP
// Inventory 2.0 pop up
echo "
<div id='inventoryModal' class='hidden'>
    <div class='inventoryModalcontent'>
        <div class='closeBtnContainer'>
            <a id='inventoryModalCloseBtn' class='closeBtn'>X</a>
        </div>
        <h1>LAVU Inventory 2.0 is here</h1>
        <p>
            Select “Start New” to begin building your inventory. <span id='migrateEnabled' class='hidden'>Choose “Migrate” to import your existing ingredients from the old Inventory system.</span> 
        </p>
        <p>
             If you have a large list of ingredients, we strongly advise you use the “Export/Import” feature to avoid potential time out issues.
        </p>
        <p>
            Close this window if you are not ready to begin yet. Migration will be available until 12/04/2017. You may choose to start fresh <span id='migrateEnabled2' class='hidden'>or migrate ingredients</span> at any time until this deadline.
        </p>
        <button id='startOverBtn' class='saveBtn'>
            Start New
        </button>
        <button id='migrateBtn' class='saveBtn hidden' disabled>
            Migrate
        </button>
        <button id='exportImportBtn' class='saveBtn'>export/import</button>
        <button id='delayMigrationBtn' class='saveBtn'>
            Not Ready
        </button>
        <p>Be advised that migration will take 30 - 60 minutes to complete. After migration is complete, new inventory items must be assigned to your menu as ingredients before the system can deduct from item stock with each sale. Please plan accordingly.</p>
    </div>
</div>

<script type='text/javascript'>
    'use strict';

    var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

    var migrationTools = (function () {
        var inventoryModal = document.getElementById('inventoryModal');
        function initializeModal() {
            var delayMigrationCookie = getCookie('delay_migration');

            if (delayMigrationCookie === 'true') {
                return;
            }

            const loadMigration = loadMigrationStatus()
            const loadLegacy = loadMigration.pipe(function (response) {
                var migrationStatus = response.DataResponse.DataResponse[0].value || '';
                if (migrationStatus.toLowerCase() === 'legacy') {
                    return loadLegacyItems();
                }
                return $.Deferred().reject();
            });

            return loadLegacy.done(function (response) {
                var hasLegacyItems = response.DataResponse.DataResponse.length > 0;
                displayMigrationOptions(hasLegacyItems);
                bindModalButtons();
                showMigrationModal();
            });
        }

        function getCookie(cname) {
            var name = cname + '=';
            var decodedCookie = decodeURIComponent(document.cookie);
            var ca = decodedCookie.split(';');
            for(var i = 0; i <ca.length; i++) {
                var c = ca[i];
                while (c.charAt(0) == ' ') {
                    c = c.substring(1);
                }
                if (c.indexOf(name) == 0) {
                    return c.substring(name.length, c.length);
                }
            }
            return '';
        }

        function showMigrationModal() {
            inventoryModal.classList.remove('hidden');
        }

        function hideMigrationModal() {
            inventoryModal.classList.add('hidden');
        }

        function bindModalButtons() {
            var inventoryModalCloseBtn =
                document.getElementById('inventoryModalCloseBtn');
            inventoryModalCloseBtn.onclick = delayMigration;

            var delayMigrationBtn =
                document.getElementById('delayMigrationBtn');
            delayMigrationBtn.onclick = delayMigration;

            var startOverBtn = document.getElementById('startOverBtn');
            startOverBtn.onclick = startOver;

            var migrateBtn = document.getElementById('migrateBtn');
            migrateBtn.onclick = migrate;
            
            var exportImportBtn =
                document.getElementById('exportImportBtn');
            exportImportBtn.onclick = exportImport;
        }

        function delayMigration() {
            document.cookie = 'delay_migration=true;max-age=86400;';
            hideMigrationModal();
        };

        function startOver() {
            return $.ajax({
                url: '../api_entry_point/inventory/InventoryRequestRouter.php/inventory/v1.0/Config/update',
                data: JSON.stringify([{
                    'setting': 'INVENTORY_MIGRATION_STATUS',
                    'value': 'Skipped',
                    'type': 'inventory_setting'
                }]),
                type: 'POST'
            }).done(function (response) {
                if (response.Status === 'Success') {
                    window.location = '/cp/?mode=settings_inventory_settings';
                }
            });
        };

        function migrate() {
            return $.ajax({
                url: '../api_entry_point/inventory/InventoryRequestRouter.php/inventory/v1.0/Config/update',
                data: JSON.stringify([{
                    'setting': 'INVENTORY_MIGRATION_STATUS',
                    'value': 'Pending',
                    'type': 'inventory_setting'
                }]),
                type: 'POST'
            }).done(function (response) {
                if (response.Status === 'Success') {
                    window.location = '/cp/?mode=settings_inventory_settings';
                }
            });
        };
        
        function exportImport() {
            return $.ajax({
                url: '../api_entry_point/inventory/InventoryRequestRouter.php/inventory/v1.0/Config/update',
                data: JSON.stringify([{
                    'setting': 'INVENTORY_MIGRATION_STATUS',
                    'value': 'Completed_Ready',
                    'type': 'inventory_setting'
                }]),
                type: 'POST'
            }).done(function (response) {
                if (response.Status === 'Success') {
                    window.location = 'index.php?mode=inventory_edit_ingredients&reportid=inventory_legacy_ingredients&export_type=csv&widget=reports/export';
                    setTimeout(function () {
                        window.location = 'areas/inventory/importItems';
                    }, 1000);
                }
            });
        };
        

        function displayMigrationOptions(hasLegacyItems) {
            if (hasLegacyItems) {
                var migrateBtn = document.getElementById('migrateBtn');
                var migrateInfoText = document.getElementById('migrateEnabled');
                var migrateInfoText2 = document.getElementById('migrateEnabled2');

                migrateBtn.disabled = false;
                migrateBtn.classList.remove('hidden');
                migrateInfoText.classList.remove('hidden');
                migrateInfoText2.classList.remove('hidden');
            }
        }

        function loadMigrationStatus() {
            return $.ajax({
                url: '../api_entry_point/inventory/InventoryRequestRouter.php/inventory/v1.0/Config/get',
                data: JSON.stringify([{
                    'setting': 'INVENTORY_MIGRATION_STATUS'
                }]),
                type: 'post'
            });
        };

        function loadLegacyItems() {
            return $.ajax({
                url: '../api_entry_point/inventory/InventoryRequestRouter.php/inventory/v1.0/InventoryItems/LegacyItems',
                type: 'get'
            });
        };

        return {
            initializeModal: initializeModal,
        };
    })();

    //window.onload = migrationTools.initializeModal;
</script>
"
// END POP UP
?>
