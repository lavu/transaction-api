<?php

$report_mode = substr($mode,0,8)=='reports_'?substr($mode,8):$mode;
set_time_limit(60);

if($report_mode=="menu")
{
	return;
}

if(substr($report_mode,0,3)=="id_")
{
	$date_mode = "unified";
	$reportid = ConnectionHub::getConn('poslavu')->escapeString(substr($report_mode,3));

	$has_required_module = TRUE;
	$required_modules_query = mlavu_query("SELECT `required_modules` FROM `$maindb`.`reports` WHERE `id` = '$reportid'");
	if ($required_modules_query !== FALSE) {
		if (mysqli_num_rows($required_modules_query) > 0) {

			$required_modules_read = mysqli_fetch_assoc($required_modules_query);
			$required_modules_read = $required_modules_read['required_modules'];
			$mods = explode("&", $required_modules_read);
			for ($i = 0; $i < count($mods); $i++) {
				$mod = $mods[$i];
				if ($mod == NULL || $mod == "")
					continue;
				$has_required_module = FALSE;	
				$or_mods = explode("|", $mod);
				foreach ($or_mods as $omod) {					
					if ($modules->hasModule($omod)) {
						$has_required_module = TRUE;
						break;
					}
				}
				if (!$has_required_module) break;
			}
		}
	}

	if ($has_required_module)
		require_once("resources/custom_report.php");
	else
		echo "You do not have the required settings to view this page.";
}
else
{
	$has_required_module = TRUE;

	if (isset($rootLink)) {
		$childname = "reports_".$report_mode;
		$child = $rootLink->findNestedChild($childname);
		if ($child !== FALSE){
			if (!$child->hasRequiredModules())
				$has_required_module = FALSE;
		}
		if ($has_required_module)
			require_once(dirname(__FILE__) . "/reports/".$report_mode.".php");
		else
			echo "You don't have the required settings to view this page.";
	}
}
