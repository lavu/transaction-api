<?php


if(!is_lavu_admin() || $primaryResId != $companyId || intval(sessvar("admin_access_level")) < 4){
	echo "Not Permitted.";
	exit;
}


if( !empty($_POST['save_sync_primary_of_chain_table_settings'])){
	saveSyncPrimarySecondaryTableSettings();
}

//Gather the settings if available.
$tableSyncSettingResult = lavu_query("SELECT `setting`,`value` FROM `config` WHERE `setting`='sync_primary_of_chain_table_settings'");
$tableSyncSettingRow = array();
$tableSyncSettingValueObject = array();
if(mysqli_num_rows($tableSyncSettingResult)){
	$tableSyncSettingRow = mysqli_fetch_assoc($tableSyncSettingResult);
	$tableSyncSettingValueJSON = $tableSyncSettingRow['value'];
	$tableSyncSettingValueObject = json_decode($tableSyncSettingValueJSON,1);
}else{//{"sync_allowed_tables":["med_customers","menu_items"], "allow_verification":false}
	$tableSyncSettingValueObject['sync_allowed_tables'] = array();
	$tableSyncSettingValueObject['allow_verification'] = false;
}



$syncAllowedTablesHTML = implode(',', $tableSyncSettingValueObject['sync_allowed_tables']);

$html = <<<HTML_FORM_TABLE
<br><br><br><br>
<form action='' method='post'>
	<input type='hidden' name='save_sync_primary_of_chain_table_settings' value='1' />
	<table>
		<tr>
			<td>Tables to allow syncing from primary(comma separated):</td>
			<td><input name='sync_allowed_tables' type='text' style='width:400px;cursor:Auto;' value='{$syncAllowedTablesHTML}'/></td>
		</tr>
		<tr>
			<td colspan='2' style='text-align:center;'><input type='submit' value='Save'/></td>
		<tr>
	</table>
</form>
HTML_FORM_TABLE;
echo $html;


function getSyncPrimarySecondaryTableSettings(){
	$tableSyncSettingResult = lavu_query("SELECT `id`,`setting`,`value` FROM `config` WHERE `setting`='sync_primary_of_chain_table_settings'");
	if(mysqli_num_rows($tableSyncSettingResult)){
		return mysqli_fetch_assoc($tableSyncSettingResult);
	}else{
		return false;
	}
}


function saveSyncPrimarySecondaryTableSettings(){
	$tableList = $_POST['sync_allowed_tables'];
	$tableListArr = explode(',', $tableList);
	$tableListArrFiltered = array();
	foreach($tableListArr as $currTable){
		$tableListArrFiltered[] = trim($currTable);
	}
	$previousSettingRow = getSyncPrimarySecondaryTableSettings();
	$fullValueObj = array();
	$fullValueObj['sync_allowed_tables'] = $tableListArrFiltered;
	$fullValueObj['allow_verification'] = false;
	$fullValueObjJSON = json_encode($fullValueObj,1);
	if(empty($fullValueObjJSON)){
		echo "Error building setting value for primary tables.";
		return;
	}

	if(!empty($previousSettingRow)){
		//Update
		$result = lavu_query("UPDATE `config` SET `value`='[1]' WHERE `setting`='sync_primary_of_chain_table_settings'",$fullValueObjJSON);
		if($result){
			echo "Updated Successfully.";
		}else{
			echo "Update Failed.";
		}
		
	}else{
		//Insert
		$result = lavu_query("INSERT INTO `config` (`setting`,`value`) VALUES ('sync_primary_of_chain_table_settings','[1]')",$fullValueObjJSON);
		if($result){
			echo "Saved Successfully.";
		}else{
			echo "Save Failed.";
		}
	}
}

