<?php

//	ini_set('display_errors',1);
	require_once($_SERVER['DOCUMENT_ROOT']."/cp/resources/core_functions.php");

	if(isset($_POST['new_layout']) && isset($_POST['data_name'])){

		mlavu_query("DELETE FROM `poslavu_[1]_db`.`config` WHERE `setting`='lower_checkout_panel_layout' AND `type`='location_config_setting' and `location`='[2]'", $_POST['data_name'], $_POST['location_id']);
		mlavu_query("INSERT INTO `poslavu_[1]_db`.`config` (`setting`, `value`,`location`,`type`) VALUES('lower_checkout_panel_layout', '[2]','[3]','location_config_setting')",
					 $_POST['data_name'], $_POST['new_layout'], $_POST['location_id']);

		if( mlavu_dberror())
			echo mlavu_dberror();
		else
			echo "success". print_r($_POST,1);
		exit;
	}else{
		get_lower_layout();
		get_alt_payment_methods();
	}
	function get_lower_layout(){	// if this is being called it means we arent coming from an ajax call. therefore we have lavuquery
		global $location_info;
		//echo "the location id is: ".print_r($location_info['id'],1);
		$query =lavu_query("SELECT * FROM `config` WHERE `setting`= 'lower_checkout_panel_layout' and `type`='location_config_setting' and `location`='[1]'",$location_info['id']);
		$has_alt_method = mysqli_num_rows(lavu_query("SELECT * FROM `payment_types` where `id` > '3' AND `_deleted`!='1'"));
		if($has_alt_method)
			$default= "var current_layout='[cash]|*|[card]|*|[discount]|*|[void]|**|[other]|*|[print_check]|*|[gratuity]';";
		else
			$default= "var current_layout='[cash]|*|[card]|*|[discount]|*|[void]|**|[gift_certificate]|*|[print_check]|*|[gratuity]';";
		if(mysqli_num_rows($query)){
			$res= mysqli_fetch_assoc($query);
			if($res['value'])
				echo "<script type='text/javascript'> var current_layout='".$res['value']."'; </script>";
			else
				echo "<script type='text/javascript'> $default </script>";
		}else{	//default
			echo "<script type='text/javascript'> $default </script>";
		}
	}function get_alt_payment_methods(){
		$val_arr=array();
		$query= lavu_query("SELECT * FROM `payment_types` where `id` > '3' AND `_deleted`!='1'");

		while($res=mysqli_fetch_assoc($query)){
			$val_arr[]= "'alt_pay:".$res['id']."':'".addslashes($res['type'])."'";
		}

		echo "<script type='text/javascript'>var alt_pay_methods={".implode(",",$val_arr)."}; </script>";
	}
	//Allow Tax Exempt Enable
	$isEnable = $location_info['allow_tax_exempt'];
?>
<style>
	#checkout_layout_editor{

		width: 750px;

		/*height: 215px;
		/*background-color:black;*/
		color:white;
	}.header_container{
		background-color:white;
		font-weight:100;
		font-size:larger;
		color:gray;
		height: 32px;
	}
	.start_img_bg{
		background: url("./images/checkout_panel_cp_s1g.png");
		background-size: 700px 150px;
		background-repeat: no-repeat;
		width: 700px;
		height: 145px;
		position: relative;
		top:50px;
		/*left: -160px;*/
	}
	.start_d{
		z-index:100;
	}
	.rows_selector{
		display:inline-block;
		position: relative;
		top: -2px;

	}.current_layout_container{
		background-color:black;
		height:300px;
	}.dnd_body_container{
		background-color:black;
		/*height:200px;*/
	}
	.alt_pay_row{

	}
	.no_drag{
		background-color:#757575;
		cursor:pointer;
	}
	.no_drag_selected{
		background-color:white;
		color:black;
	}
	.drag_td {

	    border: 1px solid black;
		border-radius: 4px;
		border-top: 1px solid gray;
		border-left: 1px solid gray;
		border-right: 1px solid gray;
		border-bottom: 1px solid #303030;
	    padding-top:5px;
	    padding-bottom:5px;
	    font-weight:bold;
	    text-align: center;
	    height: 25px;
	    background-color:#252525;
	    color:white;
		overflow: hidden;
		text-overflow: ellipsis;
		text-transform: uppercase;

	}


	/*Added 5/19/16 by EV per BT 6994
	Sets a hard pixel size to dragable objects to stop resize in FireFox*/
	.ui-draggable {
		width:135px;
	}
	.drag_td:hover{
		cursor:all-scroll;
	}.dnd_inner_container{
		width:550px;
		top: 15px;
		position: relative;
	}
	.dnd_table_drop_loc{
		width:550px;
		margin-top: 20px;
	}
	.dnd_table {
	    /*border: 1px solid black;*/
	    table-layout: fixed;
		width: 100%;
	}
	.col_selector2{
		height: 45px;
		width: 321px;
		position: relative;
		display: inline-block;
		/*top: 29px;*/
		/*margin-left: 60px;*/
	}
	.col_selector1{
		height: 37px;
		width: 445px;
		position: relative;
		top: 5px;
	}
	.num_cols_selector_inner_container{
		background-color:black;
		height: 220px;
	}
	.num_cols_selector_inner_inner_container{
		width:600px;
		padding-top: 19px;
	}
	.hover {
	    background-color: Red;
	}
	.selected_td{
		background-color:#aecd37;
	}
	.table_buttons{
		text-align:center;
		margin:0 auto;
	}
	.num_rows_selector_container{
		display:none;
	}
	.num_cols_selector_container{
		display:none;
	}
	.dnd_table_container{
		width:750px;
		display:none;
	}

	.rows_selector_options2{
		display:none;
	}
	.selections{
		color:white;
		background-color:#aecd37;
		cursor:pointer;
	}
	#current_lower_layout{
		width:445px;
		position: relative;
		top: -50px;
		left: 117px;
	}

	.checkout_buttons{
		background-color: gray;
		border: 1px solid black;
		border-radius: 3px;
		color: black;
		width: 20px;
		height: 20px;
		padding-top: 5px;
		padding-bottom: 10px;
		font-size: 20px;
		font-weight: 100;
		padding-left: 7px;
		padding-right: 7px;
		text-transform: uppercase;
	}
	.checkout_buttons:hover{
		color:black;
		background-color:white;
		cursor:pointer;

	}.checkout_button_selected{
		color:white;
	}
	.col_num{
		float: left;
		margin-right: 12px;
	}
	.row_num{
		margin-bottom:5px;
	}
	.preview_container{
		width: 500px;
		background-color: black;
		position: relative;
		top: 9px;
		margin: 0 auto;
		display: inline-block;
	}
	
	/*Relocated to styles.css on 9/14/16 by EV per LP-32*/
	/*.back_btn{
		width:70px;
		background-color:gray;
		color:black;
		font-size:large;
		float:left;
		border-radius:4px;
		border:1px solid gray;
		padding-top: 3px;
		padding-bottom: 3px;
		padding-left: 5px;
		padding-right: 5px;
	}
	.back_btn:hover{
		background-color:white;
		color:black;
		cursor:pointer;
	}
	.next_btn{
		width:70px;
		background-color:#aecd37;
		color:white;
		font-size:large;
		float:right;
		border:1px solid #aecd37;
		border-radius:4px;
		padding-top: 3px;
		padding-bottom: 3px;
		padding-left: 5px;
		padding-right: 5px;
	}
	.next_btn:hover{
		background-color:white;
		color:#aecd37;
		cursor:pointer;
	}
	.reset_btns{
		border: 1px solid black;
		border-radius: 3px;
		width: 71px;
		height: 29px;
		font-size: larger;
		color: black;
		left: 283px;
		position: relative;
		top:-10px;
	}.reset_btns:hover{
		background-color:gray;
		color:white;
		cursor:pointer;
	}
	*/
	
	.preview_cell{
		border: 1px solid black;
		border-radius: 4px;
		border-top: 1px solid gray;
		border-left: 1px solid gray;
		border-right: 1px solid gray;
		border-bottom: 1px solid #303030;
	    padding-top:5px;
	    padding-bottom:5px;
	    font-weight:bold;
	    text-align: center;
	    height: 25px;
	    background-color:#252525;
	    color:white;
		overflow: hidden;
		text-overflow: ellipsis;
		text-align:center;
		font-weight:bold;
		text-transform: uppercase;
	}.preview_table_r1, .preview_table_r2{
		width: 100%;
		/*width:508px;*/
		table-layout: fixed;
		position: relative;
		height: 40px;
	}
	.circle{
	/*
		color: white;
		border-radius: 100px;
		font-size: 35px;
		width: 42px;
	*/
	}.a_circle{
		background-color: orange;
		margin-left:-10px;
	}
	.b_circle{
		background: url("./images/a_b_drag.png");
		background-size: 90px 130px;
		height: 130px;
		width: 90px;
		background-repeat: no-repeat;
	}.direction_stuff{
		position: relative;
		float: left;
		top: -53px;
		/*position: relative;
		float: left;
		/*
		height: 42px;
		top:17px;
		width: 41px;*/
	}
	.border-top{
		margin-top:30px;
		background: url("./images/shadowline_down_10px.png");
		background-size: 700px 10px;
		height: 10px;
		width: 700px;
	}

	
	
	.arrow_left{
		width: 0;
		height: 0;
		border-top: 7px solid transparent;
		border-bottom: 7px solid transparent;
		border-right:7px solid #e6902b;
		display: inline-block;

	}.arrow_right{
		width: 0;
		height: 0;
		border-top: 7px solid transparent;
		border-bottom: 7px solid transparent;
		border-left: 7px solid #e6902b;
		display: inline-block;
	}

		/*Added 5/19/16 by EV per BT6920
		centers text in header of checkout layout editor in left nav CP only*/
		.leftNav .header_container {
			text-align: center;
		}

		/*Added 5/19/16 by EV per BT6920
		poistions first screen on checkout layout editor to the left in left nav CP only*/
		.leftNav #current_lower_layout {
			left: 250px;
		}

		/*Added 5/19/16 by EV per BT6920
		poistions second screen on checkout layout editor to the left in left nav CP only*/
		.leftNav .num_cols_selector_inner_inner_container {
			margin-left:110px;
		}

		/*Added 5/19/16 by EV per BT6920
		poistions third screen on checkout layout editor to the left in left nav CP only*/
		/*dragable top half*/
		.leftNav .dnd_inner_container {
			margin-left:100px;
		}
		/*directions*/
		.leftNav .direction_stuff {
			margin-left:10px;
			margin-top: 20px;
		}
		/*drop target area bottom half*/
		.leftNav .dnd_table_drop_loc {
			margin-left:100px;
		}
		/*reset button*/
		.leftNav .save_reset_container {
			margin-left: 375px;
		}

	}
</style>

<script type='text/javascript'>
	var num_rows_selected = 2;
	var num_cols_selected = 4;
	function initialize() {

		obj = $('.dnd_table td');
		dragObj = $('.dnd_table td');
		//var patt= new RegExp("/firefox/i");
		var firefox= false;

		if(navigator.userAgent.toLowerCase().indexOf("firefox")!=-1)
			firefox=true;

		for( var i=0; i< obj.length; i++){
			if(obj[i].className.indexOf("no_drag")==-1){
				$(obj[i]).draggable({
					cursor:'pointer',
					snap:$('td span'),
					zIndex: 100,
					revert: 'invalid'
				});
				if(firefox)
					$(obj[i]).draggable( "option", "helper", "clone" );
			}
		}


		$(dragObj).droppable({
			accept : obj,
			drop: function(event,ui){
				var target = $(event.target), //dragged cell
				drag = $(ui.draggable),       //old cell
				targetText = "",
				dragText ="",
				duration =0;

				if(target.text().trim() === ""){
					targetText = target.text().trim();
					dragText = drag.text().trim();
					target.html(dragText);
					drag.html('&nbsp;');  //uncommon this will remove the  dragged cell texts from table 1.
					if( drag[0].getAttribute('save_val')){
						target[0].setAttribute("alt_val", drag[0].getAttribute('save_val'));
					}
				}else{
					duration = 300;
				}
				ui.draggable.draggable('option', 'revertDuration', duration);
				ui.draggable.draggable('option','revert', true);
			}
		});
	}

	function select_row_num(elem){
		if( elem.className.indexOf("r1")!=-1){
			var b1= document.getElementsByClassName("checkout_buttons_r1");
			for( var i = 0; i < b1.length; i++){
				b1[i].className = 'checkout_buttons_r1';
				b1[i].style.color='#aecd37';
				b1[i].style.backgroundColor='white';
			}
		}else{
			var b2= document.getElementsByClassName("checkout_buttons_r2");
			for( var i = 0; i < b2.length; i++){
				b2[i].className= 'checkout_buttons_r2';
				b2[i].style.color='#aecd37';
				b2[i].style.backgroundColor='white';
			}
		}
		elem.className+=" selections ";
		elem.style.backgroundColor="#aecd37";
		elem.style.color="white";
	}

	function reset(table){
		//resets the bottom
		var d = document.getElementsByClassName('dnd_tr');
		for(var i=0; i< d.length; i++){
			var childs= d[i].childNodes;
			for (j = 0; j< childs.length; j++){
				childs[j].innerHTML ='&nbsp;';
			}
		}
		//resets the top
		var istaxexemptenable = '';
		var len = '';
		istaxexemptenable = '<?php echo $isEnable; ?>';
		if (istaxexemptenable == 1) {
			var top_text= ["cash","card","Discount","Void","Other","print check","gratuity","tax exemption"];
		} else {
			var top_text= ["cash","card","Discount","Void","Other","print check","gratuity"];
		}
		len = top_text.length;
		for( var i=0; i< len; i++){
			document.getElementsByClassName("start_s")[i ].innerHTML= top_text[i];
		}
		//resets alt payments

		var alts= document.querySelectorAll("[save_val]");
		for( var i=0; i< alts.length; i++){
			var save_val= alts[i].getAttribute("save_val");
			alts[i].innerHTML= alt_pay_methods[save_val];
		}

	}
	function save(){

		var save_str= "";
		var row_1= document.getElementsByClassName("dnd_tr")[2].children;
		if( num_rows_selected==2)
			var row_2= document.getElementsByClassName("dnd_tr")[3].children;
		var string_arr = [];

		for (var i=0; i < row_1.length; i++){
			var inner_val= row_1[i].innerHTML.replace(" ","_").toLowerCase();
			if(inner_val=='&nbsp;'){
				alert("All cells must be populated to save successfully. ");
				return;
			}else{
				if(row_1[i].getAttribute('alt_val')){
					string_arr.push("["+row_1[i].getAttribute('alt_val')+"]");
				}
				else{
					if(inner_val=='other')
						inner_val= "alt_pay";
					string_arr.push("["+inner_val+"]");
				}
			}
		}
		save_str+=string_arr.join("|*|");
		string_arr=[];
		if( row_2){
			save_str+="|**|";
			for (var i=0; i < row_2.length; i++){
				var inner_val= row_2[i].innerHTML.replace(" ","_").toLowerCase();
				if(inner_val=='&nbsp;'){
					alert("All cells must be populated to save successfully. ");
					return;
				}else{
					if(row_2[i].getAttribute('alt_val'))
						string_arr.push("["+row_2[i].getAttribute('alt_val')+"]");
					else{
						if(inner_val=='other')
							inner_val= "alt_pay";
						string_arr.push("["+inner_val+"]");

					}
				}
			}
			save_str+=string_arr.join("|*|");
		}
		var data_name= "<?php echo admin_info('dataname');?>";
		var loc_id= "<?php echo $location_info['id'];?>";
		$.ajax({
			url: "/cp/areas/checkout_layout_editor.php",
			type: "POST",
			data:"new_layout="+save_str+"&data_name="+data_name+"&location_id="+loc_id,
			success: function (string){
				location.reload();
			}
		});
	}
	function get_current_layout(){
		var container = document.getElementById('current_lower_layout');
		var rows= current_layout.split("|**|");
		for( var i=0; i< rows.length; i++){
			var table    = document.createElement('table');
				table.className= 'dnd_table';
			var cols     = rows[i].split("|*|");
			var html_row = document.createElement("tr");
			for (var j=0; j<cols.length; j++){
				var cell= document.createElement("td");
					cell.className='drag_td start_d';
					if(rows.length ==1)
						cell.style.height='80px';
					if(cols[j].search(/\balt_pay:\d+/)!==-1){
						cell.innerHTML=  alt_pay_methods[cols[j].replace("[", "").replace("]","")];
					}else{
						if( cols[j].indexOf('alt_pay')!=-1)
							cols[j]='other';
						cell.innerHTML= cols[j].replace("[", "").replace("]","").replace("_"," ");
					}
					cell.style.cursor='pointer';
				html_row.appendChild(cell);
			}
			table.appendChild(html_row);
			container.appendChild(table);
		}
	}
	function reset_to_defaults(){

		var data_name= "<?php echo admin_info('dataname');?>";
		var loc_id= "<?php echo $location_info['id'];?>";
		var save_str='[cash]|*|[card]|*|[discount]|*|[void]|**|[alt_pay]|*|[print_check]|*|[gratuity]';
		var r= confirm("are you sure you want to reset your layout? ");
		if(r){
			$.ajax({
				url: "/cp/areas/checkout_layout_editor.php",
				type: "POST",
				data:"new_layout="+save_str+"&data_name="+data_name+"&location_id="+loc_id,
				success: function (string){
					location.reload();
				}
			});
		}
	}
	function change_preview(elem_changed){
		change_selected(elem_changed);

		var selected_nums= document.getElementsByClassName("checkout_button_selected");
		num_rows_selected= selected_nums[1].innerHTML;
		num_cols1_selected= selected_nums[0].innerHTML;
		num_cols2_selected= selected_nums[2].innerHTML;

		if( num_rows_selected == 1){
			document.getElementsByClassName("preview_table_r2")[0].style.display='none';
			document.getElementsByClassName("preview_table_r1")[0].style.height='80px';
			document.getElementsByClassName("col_selector2")[0].style.display='none';
		}else if( num_rows_selected == 2){
			document.getElementsByClassName("preview_table_r2")[0].style.display='';
			document.getElementsByClassName("preview_table_r1")[0].style.height='40px';
			document.getElementsByClassName("col_selector2")[0].style.display='inline-block';
			var row = document.getElementsByClassName("preview_row")[1];
			row.innerHTML='';
			for( var i=0; i< num_cols2_selected; i++){
				var temp_col = document.createElement("td");
					temp_col.className = 'preview_cell';
				row.appendChild(temp_col);
			}
		}
		var row = document.getElementsByClassName("preview_row")[0];
		row.innerHTML='';

		for( var i=0; i< num_cols1_selected; i++){
			var temp_col = document.createElement("td");
				temp_col.className = 'preview_cell';
			row.appendChild(temp_col);
		}
	}
	function change_selected(ele){
		if(ele.className.indexOf('checkout_button_selected') == -1){
			var childs= ele.parentNode.children;
			for( var i=0; i < childs.length; i++ ){
				if(childs[i].className!='')
					childs[i].className = ele.className;
			}
			ele.className+=' checkout_button_selected';
		}
	}
	function navigate(start, end, fade_in_func){
		$("."+start).fadeOut("slow",function(){
				if(fade_in_func)
					window[fade_in_func]();
				$("."+end).fadeIn('slow');
			});
	}
	function create_editor(){
		var layout= document.getElementsByClassName("preview_container")[0];

			layout.children[0].className+=' dnd_table';
			//layout.children[0].style.height='40px';
			layout.children[1].className+=' dnd_table';
			layout.children[1].style.height='40px';
		document.getElementsByClassName("dnd_table_drop_loc")[0].innerHTML=layout.innerHTML;
		initialize();
	}
	function get_payment_methods(){
		var counter=0;
		var tr=document.createElement("tr");
			tr.className='alt_pay_row';
			tr.style.display='none';
		var table= document.getElementById("dnd_table_start");
		for(var i in alt_pay_methods){
			if(counter%4 == 0){
				table.appendChild(tr);
				tr=document.createElement("tr");
				tr.className='alt_pay_row';
				tr.style.display='none';
			}
			var td= document.createElement("td");
				td.innerHTML=alt_pay_methods[i];
				td.className='drag_td start_d';
				td.setAttribute("save_val", i);
			tr.appendChild(td);
			counter+=1;
		}
		table.appendChild(tr);
	}
	function show_alt_pay(elem){
		if(alt_pay_methods === undefined){
			alert("No Payment Methods found. You must create a Custom Payment Method. See Settings > Payment Settings > Payment Methods.  ");
			return;
		}

		if(document.getElementsByClassName("no_drag_selected").length){
			document.getElementsByClassName("no_drag")[0].className= 'start_d drag_td no_drag';
			document.getElementsByClassName("no_drag")[0].style.backgroundColor='#757575';
			document.getElementsByClassName("no_drag")[0].style.color='white';

			var alt_rows= document.getElementsByClassName("alt_pay_row");
			for( var i=0; i<alt_rows.length; i++){
				alt_rows[i].style.display='none';
			}
		}else{
			document.getElementsByClassName("no_drag")[0].className+=' no_drag_selected';
			document.getElementsByClassName("no_drag")[0].style.backgroundColor='white';
			document.getElementsByClassName("no_drag")[0].style.color='black';
			var alt_rows= document.getElementsByClassName("alt_pay_row");
			for( var i=0; i<alt_rows.length; i++){
				alt_rows[i].style.display='';
			}
		}

	}
	window.onload= function(){
		get_current_layout();
		get_payment_methods();

	}
</script>

<div class='border-top'></div>
<div id= 'checkout_layout_editor'>
	<div class='current_layout_container'>
		<div class='header_container'>
			<span class='row_col_back'>
				<div class='back_btn' onclick='reset_to_defaults()'><?php echo speak('RESET');?></div>
			</span>
			<span class='row_col_selector_title'><?php echo speak('YOUR CURRENT LOWER CHECKOUT PANEL');?></span>
			<span class='next_btn' onclick= 'navigate("current_layout_container","num_cols_selector_container")'><?php echo speak('EDIT');?></span>
		</div>
	 	<div class='start_img_bg'> </div>
	 	<div id= 'current_lower_layout'></div>

	 	<!--
	 	<div class='button_holder'>
	 		<input type='button' value='Click here to edit your lower checkout panel' onclick='show_next(this)'>
	 	</div>
	 	<div class='to_defaults'>
	 		<input type='button'  value = 'reset to default' onclick='reset_to_defaults()'>
	 	</div>-->
	</div>
	<!--
	<div class='num_rows_selector_container'>
		<div class='rows_selector_title'>Please select the number of rows you would like on your checkout screen:</div>
		<div class='rows_selector_options'><br>

		</div>
	</div>-->

	<div class='num_cols_selector_container'>
		<div class='header_container'>
			<span class='row_col_back'>
				<div class='back_btn' onclick='navigate("num_cols_selector_container", "current_layout_container")'>BACK</div>
			</span>
			<span class='row_col_selector_title'>
				SELECT YOUR BUTTON CONFIGURATION
			</span>
			<span class='next_btn' onclick= 'navigate("num_cols_selector_container","dnd_table_container","create_editor")'>NEXT</span>
		</div>
		<div class='num_cols_selector_inner_container'>
			<div class= 'num_cols_selector_inner_inner_container'>
				<div class ='col_selector1'>
					<div style='float:left; color: #3092c3;margin-top: 10px;'> HOW MANY BUTTONS &nbsp;&nbsp;<div class='arrow_right'> </div>&nbsp;&nbsp;</div>
					<div value='1' onclick='change_preview(this)' class='checkout_buttons col_num'>1</div>
					<div value='2' onclick='change_preview(this)' class='checkout_buttons col_num'>2</div>
					<div value='3' onclick='change_preview(this)' class='checkout_buttons col_num'>3</div>
					<div value='4' onclick='change_preview(this)' class='checkout_buttons col_num checkout_button_selected'>4</div>
				</div>
				<div class='rows_selector'>

					<div value='1' onclick='change_preview(this)' class='checkout_buttons row_num '>1</div>
					<div value='2' onclick='change_preview(this)' class='checkout_buttons row_num checkout_button_selected'>2</div>
				</div>
				<div class= 'preview_container'>
					<table class='preview_table_r1'>
						<tbody class='dnd_tbody'>
							<tr class='preview_row dnd_tr' >
								<td class='preview_cell'>&nbsp;</td>
								<td class='preview_cell'>&nbsp;</td>
								<td class='preview_cell'>&nbsp;</td>
								<td class='preview_cell'>&nbsp;</td>
							</tr>
						</tbody>
					</table>
					<table class='preview_table_r2'>
						<tbody class='dnd_tbody'>
							<tr class='preview_row dnd_tr'>
								<td class='preview_cell'>&nbsp;</td>
								<td class='preview_cell'>&nbsp;</td>
								<td class='preview_cell'>&nbsp;</td>
								<td class='preview_cell'>&nbsp;</td>
							</tr>
						</tbody>
					</table>
				</div>
				<div class ='col_selector2'>
					<div value='1' onclick='change_preview(this)' class='checkout_buttons col_num'>1</div>
					<div value='2' onclick='change_preview(this)' class='checkout_buttons col_num'>2</div>
					<div value='3' onclick='change_preview(this)' class='checkout_buttons col_num'>3</div>
					<div value='4' onclick='change_preview(this)' class='checkout_buttons col_num checkout_button_selected'>4</div>
					<div style='float:right; color: #3092c3;margin-top:10px;margin-right: -25px;'> <div class='arrow_left'></div> &nbsp; HOW MANY BUTTONS</div>
				</div>
			</div>
		</div>
		<!--
		<div class='continue_layout_editor'>
			<input type='button' value='continue' onclick='show_next(this)' class='num_cols_submit'/>
		</div>-->
	</div>
	<div class='dnd_table_container'>
		<div class='header_container'>
			<span class='row_col_back'>
				<div class='back_btn' onclick='navigate("dnd_table_container", "num_cols_selector_container"); reset($(".dnd_tbody"));'>BACK</div>
			</span>
			<span class='row_col_selector_title'>DRAG FROM <span style='color:orange'>A</span> TO <span style='color:blue'>B</span> TO SET CHECKOUT BUTTON LAYOUT</span>
			<span class='next_btn' onclick= 'save($("#dnd_tbody"))'>SAVE</span>
		</div>
		<div class='dnd_body_container'>
			<!--<div class= 'direction_stuff'>
				<div class='circle a_circle'>A</div>
				<div class='circle b_circle'>B</div>
			</div>-->

			<div class='dnd_inner_container'>

				<table class='dnd_table'>
				   <tbody id='dnd_table_start'>
					    <tr>
					        <td class='drag_td start_d start_s'><?php echo speak("Cash"); ?></td>
					        <td class='drag_td start_d start_s'><?php echo speak("Card"); ?></td>
					        <td class='drag_td start_d start_s'><?php echo speak("Discount"); ?></td>
					        <td class='drag_td start_d start_s'><?php echo speak("Void"); ?></td>
					    </tr>
					     <tr>
					        <td class='drag_td start_d start_s'><?php echo speak("Other"); ?></td>
					        <td class='drag_td start_d start_s'><?php echo speak("Print Check"); ?></td>
					        <td class='drag_td start_d start_s'><?php echo speak("Gratuity"); ?></td>
					        <?php if ($isEnable == 1) { ?>
					        <td class='drag_td start_d start_s'><?php echo speak("Tax Exemption"); ?></td>
							<?php } else { ?>
							<td class='no_drag drag_td start_d start_s' onclick = 'show_alt_pay(this)' style='background-color:#757575;cursor:pointer;'><?php echo speak("PAYMENT METHOD"); ?></td>
							<?php } ?>
						</tr>
						 <?php if ($isEnable == 1) { ?>
						<tr>
							<td class='no_drag drag_td start_d start_s' onclick = 'show_alt_pay(this)' style='background-color:#757575;cursor:pointer;'><?php echo speak("PAYMENT METHOD"); ?></td>
				  		</tr>
						<?php } ?>
				   </tbody>
				</table>

			</div>

			<!--<br/>-->
			<div class= 'direction_stuff'>
				<div class='circle b_circle'></div>
			</div>
			<div class='dnd_table_drop_loc'></div>
			<div class='save_reset_container'>
				<!--<input type='button' value='Save'  onclick='save($("#dnd_tbody"))'> -->
				<input type='button' class='reset_btns' value='Reset' onclick='reset($(".dnd_tbody"))'>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.6.2/jquery.min.js"></script>

