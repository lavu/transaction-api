<?php
	if($in_lavu)
	{
		$form_posted = (isset($_POST['posted']));
		$field = "ingredients";

		//$show_highlow_stuff = false;
		//if($_SERVER['REMOTE_ADDR']=="74.95.18.90")
		$show_highlow_stuff = true;
			
		$qtitle_category = "Inv. List";
		$qtitle_item = "Item";

		if (!isset($data_name))
			$data_name = admin_info("dataname");

		require_once('/home/poslavu/public_html/admin/sa_cp/manage_chain_functions.php');
		$a_chain_groups = array();
		$a_chain_items = array();
		$b_part_of_chain = getReportingGroupOptionsForDimensionalForm($data_name, $a_chain_groups, $a_chain_items);
		$menu_updated = FALSE;

		$category_tablename = "ingredient_categories";
		$inventory_tablename = "ingredients";
		$category_fieldname = "id";
		$inventory_fieldname = "category";
		$inventory_select_condition = "";
		$inventory_primefield = "title";
		$category_primefield = "title";

		$category_filter_by = "loc_id";
		$category_filter_value = $locationid;

		$cfields = array();
		if ($b_part_of_chain)
			$cfields[] = array("chain_reporting_group", "chain_reporting_group", speak("Chain Reporting Group"), "select", $a_chain_groups);

		$qfields = array();
		$qfields[] = array("title","title",speak("Ingredient"),"input");
		$qfields[] = array("qty","qty",speak("Quantity"),"input");
		$qfields[] = array("unit","unit",speak("Unit"),"input");
		$qfields[] = array("cost","cost",speak("Unit Cost"),"input");
		if($show_highlow_stuff)
		{
			$qfields[] = array("high","high",speak("Par"),"input");
			$qfields[] = array("low","low",speak("Low Alert Level"),"input");
		}
		if ($b_part_of_chain)
			$qfields[] = array("chain_reporting_group", "chain_reporting_group", speak("Chain Reporting Group"), "select", $a_chain_items);

		$qfields[] = array("id");
		$qfields[] = array("delete");
		require_once(resource_path() . "/dimensional_form.php");
		$qdbfields = get_qdbfields($qfields);

		if($form_posted)
		{
			$menu_updated = TRUE;
			$cat_count = 1;
			while(isset($_POST["ic_".$cat_count."_category"]))
			{
				$category_name = $_POST['ic_'.$cat_count.'_category'];
				$category_id = $_POST['ic_'.$cat_count.'_categoryid'];
				$category_delete = $_POST['ic_'.$cat_count.'_delete_0'];
				$category_chain_reporting_group = $_POST['ic_'.$cat_count.'_col_chain_reporting_group'];
				if($category_name==$qtitle_category . " Name") $category_name = "";

				if($category_delete=="1")
				{
					delete_poslavu_dbrow($category_tablename, $category_id);
				}

				if($category_name!="")
				{
					$dbfields = array();
					$dbfields[] = array("title",$category_name);
					$dbfields[] = array("loc_id",$locationid);
					$dbfields[] = array("chain_reporting_group",$category_chain_reporting_group);
					$categoryid_indb = update_poslavu_dbrow($category_tablename,$dbfields, $category_id);
					if($category_fieldname=="title")
						$categoryid_indb = $category_name;
					$item_count = 1;
					while(isset($_POST["ic_".$cat_count."_".$inventory_primefield."_".$item_count]))
					{
						$item_value = array(); // get posted values
						for($n=0; $n<count($qfields); $n++)
						{
							$qname = $qfields[$n][0];
							$qtitle = (isset($qfields[$n][2]))?$qfields[$n][2]:false;
							$set_item_value = $_POST['ic_'.$cat_count.'_'.$qname.'_'.$item_count];
							if($qtitle && $set_item_value==$qtitle) $set_item_value = "";
							if($qname=="cost")
							{
								// LP-1616 -- Switched to same function edit_menu.php uses to prep monetary string for database to fix thousand separator commas not being handled correctly when decimal_separator = none and disable_decimal = 0
								$set_item_value = formatValueForMonetaryDisplay( $set_item_value );
							}
							$item_value[$qname] = $set_item_value;
						}
						$item_id = $item_value['id'];
						$item_delete = $item_value['delete'];
						$item_name = $item_value[$inventory_primefield];

						if($item_delete=="1")
						{
							delete_poslavu_dbrow($inventory_tablename, $item_id);
						}
						else if($item_name!="")
						{
							$dbfields = array();
							$dbfields[] = array("loc_id",$locationid);
							$dbfields[] = array($inventory_fieldname,$categoryid_indb);
							for($n=0; $n<count($qdbfields); $n++)
							{
								$qname = $qdbfields[$n][0];
								$qfield = (isset($qdbfields[$n][1]))?$qdbfields[$n][1]:$qname;
								$dbfields[] = array($qfield,$item_value[$qname]);
							}
							update_poslavu_dbrow($inventory_tablename,$dbfields, $item_id);
						}
						$item_count++;
					}
				}
				$cat_count++;
			}


			//Inserted By Brian D.  We also need to sync the structure of their ingredient_categories and also ingredient usage.
            schedule_remote_to_local_sync_if_lls($location_info, $locationid, "table updated", "ingredient_categories");
			//end... inserting update.

			// LOCAL SYNC RECORD
			schedule_remote_to_local_sync_if_lls($location_info, $locationid, "table updated", "ingredients");

			// END LOCAL SYNC RECORD

			$check_for_last_update = lavu_query("SELECT `id` FROM `config` WHERE `setting` = 'menu_last_updated' AND `location` = '[1]'", $locationid);
			if (@mysqli_num_rows($check_for_last_update)) {
				$uinfo = mysqli_fetch_assoc($check_for_last_update);
				lavu_query("UPDATE `config` SET `value` = now() WHERE `id` = '[1]'", $uinfo['id']);
			} else lavu_query("INSERT INTO `config` (`location`, `type`, `setting`, `value`) VALUES ('[1]', 'admin_activity', 'menu_last_updated', now())", $locationid);

			lavu_query("UPDATE `devices` SET `needs_reload` = '1' WHERE `loc_id` = '[1]'", $locationid);
		}

		$details_column = "";//"Zip Codes";
		$send_details_field = "";//"zip_codes";

		$qinfo = array();
		$qinfo["qtitle_category"] = $qtitle_category;
		$qinfo["qtitle_item"] = $qtitle_item;
		$qinfo["category_tablename"] = $category_tablename;
		$qinfo["inventory_tablename"] = $inventory_tablename;
		$qinfo["category_fieldname"] = $category_fieldname;
		$qinfo["inventory_fieldname"] = $inventory_fieldname;
		$qinfo["inventory_select_condition"] = $inventory_select_condition;
		$qinfo["inventory_primefield"] = $inventory_primefield;
		$qinfo["category_primefield"] = $category_primefield;
		$qinfo["details_column"] = $details_column;
		$qinfo["send_details"] = $send_details_field;
		$qinfo["field"] = $field;

		$qinfo["category_filter_by"] = $category_filter_by;
		$qinfo["category_filter_value"] = $category_filter_value;

		$field_code = create_dimensional_form($qfields,$qinfo,$cfields);
		$display .= "<form name='ing' method='post' action=''>";
		$display .= "<input type='submit' value='".speak('Save')."' style='width:120px'>&nbsp;";
		$display .= "<a href='index.php?mode=inventory_edit_ingredients&reportid=inventory_edit_ingredients&export_type=csv&widget=reports/export' class='exportBtn' role='button'>Export to CSV</a><br>&nbsp;"; // LP-2892
		$display .= "<input type='hidden' name='posted' value='1'>";
		$display .= $field_code;
		$display .= "&nbsp;<br><input type='submit' value='".speak('Save')."' style='width:120px'>";
		$display .= "</form>";

		echo $display;

		if ($menu_updated) {
			require_once('/home/poslavu/public_html/admin/sa_cp/manage_chain_functions.php');
			syncChainDatabases($data_name);
		}		
	}
?>
