<?php
	if(!$in_lavu)
		return;

	echo "<br><br>";

	$fields = array();
	$fields[] = array(speak("State"),"state_name","text","","list:yes");
	$fields[] = array(speak("Abbr"),"state","text","","list:yes");
	$fields[] = array(speak("Submit"),"submit","submit");

	$tablename = "states";
	$forward_to = "index.php?mode={$section}_{$mode}";
	
	function should_column_be_added($tablename,$colname)
	{
		$exist_query = lavu_query("select `[1]` from `[2]` limit 1",$colname,$tablename);
		if(!$exist_query)
		{
			$err = lavu_dberror();
			return ($err=="Unknown column '$colname' in 'field list'");
		}
		return false;
	}
	
	if(should_column_be_added("states","_deleted"))
	{
		$row_query = lavu_query("ALTER TABLE `states` ADD `_deleted` int(11) default 0 not null");//Ensure the _deleted field exists.
	}
	require_once(resource_path() . "/browse.php");
