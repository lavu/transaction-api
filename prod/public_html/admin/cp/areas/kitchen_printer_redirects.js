
	var kiprre = document.getElementById("kiprre");
	var kiprre_com_mode = "";
	var modify_kpr_id = "";
	var modify_kpr_name = "";
	var modify_froms_and_tos = "";
	var loaded_redirects = [];
	var should_reload_list = false;
	
	var kpr_pair_count = 0;
	var kpr_used_froms = [];
	var kpr_pairs_marked_for_delete = [];

	function initializeKIPRRE() {
	
		kiprre_com_mode = "load_redirects";
	
		startKIPRRErequest();
	}
	
	function indicateKIPRREactivity() {
	
		var kW = kiprre.style.width;
		var kH = kiprre.style.height;
		kiprre.innerHTML = "<table width='" + kW + "' height='" + kH + "'><tr><td align='center' valign='middle'><img src='images/kpr_activity.gif' width='32px' height='32px'></td></tr></table>";
	}
	
	function displayError(error_code) {
	
		var message = "Unknown error...";

		switch (error_code) {
			case "bad_response": message = "An unrecognized response was returned by the server..."; break;
			case "expired_session": message = "Your log in session has expired..."; break;
			case "no_db": message = "An error occurred while trying to connect to the database..."; break;
			default: break;			
		}

		var kW = kiprre.style.width;
		var kH = kiprre.style.height;
		kiprre.innerHTML = "<table width='" + kW + "' height='" + kH + "'><tr><td align='center' valign='middle'><table width='400px'><tr><td align='center' style='color:#222222'>" + message + "</td></tr><tr><td align='center'><br><button onclick='startKIPRRErequest();'>Retry</button></td></tr></table></td></tr></table>";
	}
	
	function reloadOrDrawRedirectsList() {
		
		if (should_reload_list) {
			should_reload_list = false;
			initializeKIPRRE();
		} else drawRedirectsList();
	}
	
	function sortedRedirects() {
		
		var keyed = {};
		var names = []
		for (var i = 0; i < loaded_redirects.length; i++) {
			var name = loaded_redirects[i].setting;
			var append = 0;
			while (name in keyed) {
				append++;
				name += "" + append;
			}
			keyed[name] = loaded_redirects[i];
			names.push(name);
		}

		names.sort();
		
		var sorted = [];
		for (var i = 0; i < names.length; i++) {
			var name = names[i];
			sorted.push(keyed[name]);
		}
	
		return sorted;		
	}
	
	function drawRedirectsList() {
		
		var redirects = sortedRedirects();
		var len = redirects.length;
		
		var str = "<table id='redirects_list'>";
		str += "<tr><td height='20px'>&nbsp;</td></tr>";
		str += "<tr>";
		str += "<td align='center'>";
		str += "<table width='550px'>";
		str += "<tr><td align='left'>Kitchen Printer Redirects can be defined here and chosen from the front end to allow for selective re-routing of order items.<br><br><font color='#333333'>For example, a large restaurant may have a menu setup to have food items sent to a printer in the kitchen and drinks sent to a printer at one of two drink stations. A simple set of redirects can have drink items sent to the printer at the second drink station instead. A server would enable the redirect based on his or her proximity to either drink station.</font></td></tr>";
		str += "</table>";
		str += "</td>";
		str += "</tr>";
		str += "<tr><td height='20px'>&nbsp;</td></tr>";
		str += "<tr>";
		str += "<td align='center'>";
		str += "<div class='addNewBtn' onclick='editRedirects(\"0\", \"\", \"\");'><span class='plus'> + </span>  <span>Add New</span></div>";
		str += "<br><br>" + len + " " + ((len == 1)?"record":"records") + " found<br><br>";
		str += "</td>";
		str += "</tr>";
		if (len > 0) {

			str += "<tr><td align='center'><table style='border:solid 1px black; max-width:696px;' cellspacing=0 cellpadding=4>";
	
			for (var i = 0; i < len; i++) {
					
				var this_kpr = redirects[i];
				
				var escaped_setting = this_kpr.setting.replace(/"/g, '%22');
				escaped_setting = escaped_setting.replace(/'/g, "%27");
	
				str += "<tr style='cursor:pointer' onmouseover='this.bgColor = \"#aabbee\";' onmouseout='this.bgColor = \"#ffffff\";'>";
				str += "<td valign=\"top\" onclick=\"editRedirects('" + this_kpr.id + "', '" + escaped_setting + "', '" + this_kpr.value + "');\">" + this_kpr.setting + "</td><td>&nbsp;</td>";
				str += "<td><a style='cursor:pointer' onclick='if(confirm(\"Are you sure you want to delete this set of redirects?\")) removeRedirects(\"" + this_kpr.id + "\");'><img src='images/little_trash.gif' border='0'/></a>";
				str += "</tr>";
			}

			str += "</table></td></tr>";
		}
		
		str += "<tr><td height='50px'>&nbsp;</td></tr>";
		str += "</table>";
		
		kiprre.innerHTML = str;
		
		window.setTimeout(function(){adjustContainerHeightFor("redirects_list")}, 100);
	}
	
	function adjustContainerHeightFor(element_id) {
		
		var element = document.getElementById(element_id);
		var height = element.offsetHeight;
				
		kiprre.style.height = height + 'px';
	}
	
	function editRedirects(kpr_id, kpr_name, kpr_data) {
		
		kpr_pair_count = 0;
		kpr_used_froms = [];
		kpr_pairs_marked_for_delete = []
		
		var keyed_data = {};
		var split_data = kpr_data.split(",");
		var len = split_data.length;
		for (var i = 0; i < len; i++) {
			var sd = split_data[i].split(":");
			var key = "";
			var value = "";
			if (sd.length > 0) {
				key = sd[0];
				if (sd.length > 1) value = sd[1];
			}
			if (key.length > 0) keyed_data[key] = value;
		}

		var keys = [];
		for (var k in keyed_data) keys.push(k);
		keys.sort(function(a, b){return a-b});

		var str = "<table id='edit_redirects_form'>";
		str += "<tr><td height='20px'>&nbsp;</td></tr>";
		str += "<tr>";
		str += "<td align='center'>";
		str += "<a style='cursor:pointer; color:#333;' onclick='reloadOrDrawRedirectsList();'><u>&#10094; View List</u></a><br><br>";
		str += "<form id='kpr_dynamic_form'>";
		str += "<input name='kpr_id' type='hidden' value='" + kpr_id + "'>";
		str += "<table id='inner_edit_redirects_form' style='border:solid 2px #aaaaaa; max-width:696px;' class='tableSpacing'>";
		str += "<tr>";
		str += "<td align='right' bgcolor='#eeeeee' style='padding-left:32px;' valign='top'>Name:</td>";
		str += "<td align='left' valign='top' style='padding-right:12px;' colspan='2'><input name='kpr_name' type='text' value='" + decodeURIComponent(kpr_name).replace(/'/g, "&#39;") + "'></td>";
		str += "</tr>";
		
		len = keys.length;
		for (var i = 0; i < len; i++) {
			
			kpr_pair_count++;
			var from_printer = keys[i];
			kpr_used_froms.push(from_printer);
			var to_printer = keyed_data[from_printer];
			
			str += generatePairRow(from_printer, to_printer, i);
		}
		
		if (len < 25) {
			str += "<tr><td style='height:4px'></td></tr>";
			str += "<tr id='kpr_new_pair'>";
			str += "<td align='center' colspan='3' style='border-top:1px solid #bbbbbb; border-bottom:1px solid #aaaaaa; padding:2px 0px 1px 0px;'>";
			str += "<table cellspacing=0 cellpadding=0>";
			str += "<tr>";
			str += "<td align='right' style=' padding-right:5px;'>Add redirect for " + printerSelector("new", len) + "</td>";
			str += "<td align='left' style='padding:3px 0px 0px 0px;'><div class='plusBtn' style='cursor:pointer;' onclick='addRedirectPair();'>+</div></td>";
			str += "</tr>";
			str += "</table>";
			str += "<tr><td style='height:3px'></td></tr>";
		}
		str += "</td>";
		str += "</tr>";	
		str += "</form>";
		str += "<tr><td align='center' colspan='3'><button onclick='submitRedirects(); return false;'>Save</button></td></tr>";
		str += "</table>";
		str += "</tr>";
		str += "<tr><td height='50px'>&nbsp;</td></tr>";
		str += "</table>";
		
		kiprre.innerHTML = str;

		window.setTimeout(function(){adjustContainerHeightFor("edit_redirects_form")}, 100);
	}
	
	function numToDes(num) {
		
			var des = "kitchen";
			if (parseInt(num) == 0) des = "Do not print";
			if (parseInt(num) > 1) des += "" + num;
			
			return des;
	}
	
	function generatePairRow(from_printer, to_printer, form_row) {
		
		var str = "<tr id='kpr_pair_" + form_row + "'>";
		str += "<td align='right' bgcolor='#eeeeee' style='padding-left:32px;' valign='top'>" + numToDes(from_printer) + " <b>&#x2794;</b><input name='kpr_from_" + form_row + "' type='hidden' value='" + from_printer + "'></td>";
		str += "<td align='left' valign='top' style='width:100px; padding-right:7px;'>" + printerSelector(to_printer, form_row) + "</td>";
		str += "<td align='left' style='padding:3px 0px 0px 0px;'><div class='closeBtn' onclick='toggleRedirectForDelete(" + form_row + ", this);'>X</div></td>";
		str += "</tr>";

		return str;		
	}
	
	function printerSelector(selected, form_row) {
		
		var new_from = (selected == "new");
		var set_id = new_from?"kpr_new_from":"kpr_to_" + form_row;
		
		var str = "<select id='" + set_id + "' name='" + set_id + "'>";
		var kmax = new_from?25:26;
		for (var k = 1; k <= kmax; k++) {
			var kk = (k == 26)?0:k;
			var sel = (kk == selected)?" selected":"";
			var is_used = (kpr_used_froms.indexOf("" + kk) > -1);
			if (!new_from || !is_used) str += "<option value='" + kk + "'" + sel + ">" + numToDes(kk) + "</option>";
		}
		str += "</select>";
		
		return str;
	}
	
	function toggleRedirectForDelete(form_row, element) {
		
		var this_row = document.getElementById("kpr_pair_" + form_row);
		
		var set_opacity = 0.3;
		var set_src = "images/btn_omed_open_item.png";
		if (this_row.style.opacity == 0.3) {
			set_opacity = 1;
			// set_src = "images/btn_xmed_close_item.png";
			
			var new_kpmfd = [];
			for (var i = 0; i < kpr_pairs_marked_for_delete.length; i++) {
				var kprp = kpr_pairs_marked_for_delete[i];
				if (kprp != form_row) new_kpmfd.push(kprp);
			}
			kpr_pairs_marked_for_delete = new_kpmfd;
			
		} else	kpr_pairs_marked_for_delete.push(form_row);
			
		this_row.style.opacity = set_opacity;
		element.src = set_src;
	}
	
	function addRedirectPair() {
		
		var form_row = kpr_pair_count;
		kpr_pair_count++;
		
		var knf = document.getElementById("kpr_new_from");
		
		var new_row = document.getElementById("inner_edit_redirects_form").insertRow(kpr_pair_count);
		new_row.id = "kpr_pair_" + form_row;
		new_row.innerHTML = generatePairRow(knf.value, "1", form_row);
		
		kpr_used_froms.push(knf.value);
		knf.remove(knf.selectedIndex);
		
		if (kpr_pair_count == 25) document.getElementById("kpr_new_pair").style.display = "none";
		
		window.setTimeout(function(){adjustContainerHeightFor("edit_redirects_form")}, 100);
	}
	
	function submitRedirects() {
		
		var the_form = document.getElementById("kpr_dynamic_form");
		modify_kpr_id = the_form.elements.namedItem("kpr_id").value;
		modify_kpr_name = the_form.elements.namedItem("kpr_name").value;
		
		if (modify_kpr_name.length == 0) alert("Please enter a valid name for this set of redirects.");
		else {

			modify_froms_and_tos = "";
			for (var i = 0; i < kpr_pair_count; i++) {		
				var should_delete = (kpr_pairs_marked_for_delete.indexOf(i) > -1);
				if (!should_delete) {	
					if (modify_froms_and_tos != "") modify_froms_and_tos += ",";
					modify_froms_and_tos += the_form.elements.namedItem("kpr_from_" + i).value + ":" + the_form.elements.namedItem("kpr_to_" + i).value;
				}
			}
			
			should_reload_list = true;
			kiprre_com_mode = "submit_redirects";
			
			startKIPRRErequest();
		}
	}
	
	function removeRedirects(kpr_id) {
	
		kiprre_com_mode = "remove_redirects";
		modify_kpr_id = kpr_id;
	
		startKIPRRErequest();
	}

	function startKIPRRErequest() {
	
		indicateKIPRREactivity();

		var postVars = {};
		postVars.mode = kiprre_com_mode;
		postVars.c_id = kpr_c_id;
		postVars.dn = kpr_dn;
		postVars.loc_id = kpr_loc_id;
		if (kiprre_com_mode == "submit_redirects") {
			postVars.row_id = modify_kpr_id;
			postVars.kpr_name = modify_kpr_name;
			postVars.froms_and_tos = modify_froms_and_tos;
		} else if (kiprre_com_mode == "remove_redirects") {
			if (modify_kpr_id.length == 0) return;
			postVars.row_id = modify_kpr_id;
		}
			
		new KIPRRErequest(postVars);
	}

	function KIPRREresponse(response) {
	
		var respJSON = tryParseJSON(response);	
	
		if (respJSON) {	
			if (typeof respJSON.status != 'undefined') {
				var status = respJSON.status;
				if (status == "success") {
					if (typeof respJSON.info == 'object') {				
						if (kiprre_com_mode == "load_redirects") {
							loaded_redirects = respJSON.info;
							drawRedirectsList();
						} else if (kiprre_com_mode == "submit_redirects") {
							var saved_id = modify_kpr_id;
							if (typeof respJSON.info[0].row_id != 'undefined') saved_id = respJSON.info[0].row_id;
							editRedirects(saved_id, modify_kpr_name, modify_froms_and_tos);							
						} else if (kiprre_com_mode == "remove_redirects") initializeKIPRRE();
					} else displayError("bad_response");
				} else displayError(status);
			} else displayError("bad_response");
		} else displayError("bad_response");
	}

	function tryParseJSON(jsonString) {
		try {
				var o = JSON.parse(jsonString);
				if (o && typeof o === "object" && o !== null) {
						return o;
				}
		}
		catch (e) { }

		return false;
	};
	
	(function(){
		function AJAXRequest( url, requestType, params, async ){
			if( arguments.length === 0 ){
				return;
			}
			if( !requestType ){
				requestType = '';
			}

			if( !async ){
				async = false;
			} else {
				async = true;
			}

			switch( requestType.toUpperCase().trim() ){
				case 'POST' :
				case 'PATCH' :
				case 'PUT' :
					break;
				case 'GET' :
				default :
					requestType = 'GET';
					break;
			}

			if( ! params ){
				params = '';
			} else if( params instanceof Object ){
				obj = params;
				arr = [];
				for( var k in obj ){
					arr.push( encodeURIComponent( k ) + '=' + encodeURIComponent( obj[k] ) );
				}

				params = arr.join('&');
			} else if('string' != typeof params ){
				params = '';
			}

			this._request = new XMLHttpRequest();
			this._request.open( requestType, url, async );
			this._request.addEventListener('readystatechange', this, false );
			if( requestType == 'POST' ){
				this._request.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
			}

			this._request.send( params );
		}

		function handleEvent( event ){
			if( event.type != 'readystatechange' ){
				return;
			}

			if( this._request.readyState === 4  ){
				this._request.removeEventListener( 'readystatechange', this, false );
				if( this._request.status === 200 ){
					this.success( this._request.responseText );
				} else {
					this.failure( this._request.status, this._request.responseText );
				}
			}
		}

		function success( responseText ){
			alert( responseText );
		}

		function failure( status, responseText ){
			alert( status );
			alert( responseText );
		}

		window.AJAXRequest = AJAXRequest;
		AJAXRequest.prototype = {};
		AJAXRequest.prototype.constructor = AJAXRequest;
		AJAXRequest.prototype.handleEvent = handleEvent;
		AJAXRequest.prototype.success = success;
		AJAXRequest.prototype.failure = failure;
	})();
	
	(function(){
		function KIPRRErequest( params ){
			AJAXRequest.call( this, 'areas/kitchen_printer_redirects.php', 'POST', params, true );
		}

		function success( response ){
			KIPRREresponse(response);
		}
		
		function failure( status, response ){
			KIPRREresponse("");
		}

		window.KIPRRErequest = KIPRRErequest;
		KIPRRErequest.prototype.constructor = KIPRRErequest;
		KIPRRErequest.prototype = Object.create(AJAXRequest.prototype);
		KIPRRErequest.prototype.success = success;
		KIPRRErequest.prototype.failure = failure;
	})();