<?php
	create_tabs(array("iPads and iPods","hardware_devices"),
		array("Wireless","hardware_routers"),
		array("Printers","hardware_printers"),
		array("Cash Drawer","hardware_cashdrawer"),
		array("Card Swipers","hardware_payment"));
	if(is_file("areas/hardware/".$mode.".php")){			
		require_once("areas/hardware/".$mode.".php");
		
	}
	function create_tabs()
	{
		global $mode;
		global $submode;
		global $parent_mode;
		global $parent_submode;
		
		if($parent_mode && $parent_submode)
		{
			$tmode = $parent_mode;
			$tsubmode = $parent_submode;
		}
		else
		{
			$tmode = $mode;
			$tsubmode = $submode;
		}
				
		$top = "";
		$mid = "";
		$btm = "";
		
		$subtabs = 0;
		$exercise_subtab = false;
		$selected_column = 0;
		$show_progress_bar = false;
		$tabs = func_get_args();
		if ((count($tabs) == 1) && strstr($tabs[0], "|*|")) {
			$tabs = explode("|*|", $tabs[0]);
		} else if(count($tabs) > 1 && ($tabs[0]=="array" || $tabs[0]=="wizard"))
		{
			if($tabs[0]=="wizard")
				$show_progress_bar = true;
			$tabs = $tabs[1];
		}
		
		$mid .= "<tr>";
		$tabcount = 0;
		$maxtabcols = 6;
		if(count($tabs)==7) $maxtabcols = 4;
		for($i=0; $i<count($tabs); $i++)
		{
			$tabcount++;
			$tab_subtabs = 0;
			
			if($tabcount > $maxtabcols)
			{
				$tabcount = 1;
				$mid .= "</tr><tr>";
			}
			if(is_array($tabs[$i]))
			{
				$tab_title = $tabs[$i][0];
				$tab_mode = $tabs[$i][1];
				if(isset($tabs[$i][2]))
				{
					$tab_subtabs = $tabs[$i][2];
				}
			}
			else
			{
				$tab_title = $tabs[$i];
				$tab_mode = strtolower(str_replace(array(" - "," "),array("_","_"),$tab_title));
			}
			$border_color = "#999999";
			$bgcolor = "#cdcdcd";
			$extra_code = "onclick='window.location = \"index.php?mode=$tab_mode\"'";
			if($tmode==$tab_mode) 
			{
				$bgcolor = "#eeeeee";
				$subtabs = $tab_subtabs;
				$selected_column = $tabcount;
				if($parent_mode==$tab_mode && $subtabs<1)
				{
					$exercise_subtab = true;
				}
			}
			$mid .= "<td style='cursor:pointer; border:solid 1px $border_color' align='center' bgcolor='$bgcolor' $extra_code>".speak($tab_title)."</td>";
		}
		$mid .= "</tr>";
		
		if($subtabs > 0)
		{
			$btm .= "<tr>";
			for($i=1; $i<$selected_column; $i++)
			{
				$btm .= "<td>&nbsp;</td>";
			}
			$btm .= "<td>";
			$btm .= "<table>";
			for($i=1; $i<=$subtabs; $i++)
			{
				$subtab_title = $i;
				$border_color = "#999999";
				$bgcolor = "#cdcdcd";
				$extra_code = "onclick='window.location = \"index.php?mode=$tmode&submode=$i\"'";
				$exercise_code = "";
				if($tsubmode==$i) 
				{
					$bgcolor = "#eeeeee";
					if($parent_submode==$i)
					{
						$exercise_code = "<td style='cursor:pointer; padding-left:6px; padding-right:6px; border:solid 1px $border_color' bgcolor='$bgcolor'>".speak("Exercise")."</td>";
					}
				}
				$btm .= "<td style='cursor:pointer; padding-left:6px; padding-right:6px; border:solid 1px $border_color' bgcolor='$bgcolor' $extra_code>".speak($subtab_title)."</td>" . $exercise_code;
			}
			$btm .= "</table>";
			$btm .= "</td>";
			$btm .= "</tr>";
		}
		if($exercise_subtab)
		{
			$btm .= "<tr>";
			for($i=1; $i<$selected_column; $i++)
			{
				$btm .= "<td>&nbsp;</td>";
			}
			$btm .= "<td>";
			$btm .= "<table>";
			
			$btm .= "<td style='cursor:pointer; padding-left:6px; padding-right:6px; border:solid 1px #999999' bgcolor='#eeeeee'>".speak("Exercise")."</td>";
			
			$btm .= "</table>";
			$btm .= "</td>";
			$btm .= "</tr>";
		}
		
		echo "<table width='100%'><tr><td align='center'><table cellspacing=2 cellpadding=4>";
		echo $top;
		echo $mid;
		echo $btm;
		echo "</table></td></tr></table>";
	}
	
?>