<?php
if (!$in_lavu)
{
	exit();
}

if (isset($_GET['get_monetary_symbol']))
{
	if (isset($_POST['code']) && $_POST['code'] != "")
	{
		lavu_query("SET NAMES 'utf8'");
		$query = " SELECT monetary_symbol,currency_separator as decimal_separator
					FROM `poslavu_MAIN_db`.`country_region`
					WHERE id=[1] AND active=[2]
					 LIMIT 1 ";
		$result = lavu_query($query,$_POST['code'],1);
		if(!$result){
			error_log("mysql error in ".__FILE__." mysql error:" . lavu_dberror());
			header('Content-type: application/json');
			echo '{}';
			exit;
		}
		else{
			$selectedRegionCurrencyInfo = array();
			while ($currencyCode = mysqli_fetch_assoc($result)) {
				$selectedRegionCurrencyInfo[] = $currencyCode;
			}
			header('Content-type: application/json');
			echo json_encode($selectedRegionCurrencyInfo);
			exit;
		}
	} else {
		error_log("problem with ajax request, unable to get regionid parameter.");
		header('Content-type: application/json');
		echo '{}';
		exit;
	}
}

$times_array = array();
for ($t = 1; $t < 24; $t++)
{
	$times_array[] = "$t:00";
}

$dining_room_backgrounds = array(
	'0' => speak("Solid Black"),
	'1' => speak("Gradient 1"),
	'2' => speak("Gradient 2"),
	'3' => speak("Grey Floorboards"),
	'4' => speak("Brown Floorboards"),
	'5' => speak("Red Tiles"),
	'6' => speak("Brick Wall"),
	'7' => speak("Stone Patio")
);

$item_icon_colors = array(
	'grey'		=> speak("Light Gray"),
	'dark_grey'	=> speak("Dark Gray"),
	'green'		=> speak("Lavu Green"),
	'blue'		=> speak("Blue")
);

$available_fonts = array(
	'Chalkduster'      => "Chalkduster (iPad only)",
	'Helvetica'        => "Helvetica",
	'Helvetica-Bold'   => "Helvetica Bold",
	'MarkerFelt-Thin'  => "Marker Felt",
	'Papyrus'          => "Papyrus (iPad only)",
	'TrebuchetMS'      => "Trebuchet",
	'TrebuchetMS-Bold' => "Trebuchet Bold"
);

$available_fonts_ipod = array(
	'Helvetica'        => "Helvetica",
	'Helvetica-Bold'   => "Helvetica Bold",
	'MarkerFelt-Thin'  => "Marker Felt",
	'TrebuchetMS'      => "Trebuchet",
	'TrebuchetMS-Bold' => "Trebuchet Bold"
);

if ((intval(sessvar("admin_access_level"))==3) || (intval(sessvar("admin_access_level"))==4)) {
    $force_close_server_levels = array(
        '1' => "1",
        '2' => "2",
        '3' => "3",
        '4' => "4",
    );

    if (intval(sessvar("admin_access_level"))==3) {
        array_pop($force_close_server_levels);
    }
}
$server_levels = array(
	'1' => "1",
	'2' => "2",
	'3' => "3",
	'4' => "4"
);

$server_levels_with_disable = array(
	'1' => "1",
	'2' => "2",
	'3' => "3",
	'4' => "4",
	'0' =>speak("Disable")
);

$rounding_factors = array(
	''		=> speak("No rounding"),
	'100'	=> "100",
	'50'	=> "50",
	'25'	=> "25",
	'10'	=> "10",
	'5'		=> "5",
	'1'		=> "1",
	'0.5'	=> "0.5",
	'0.25'	=> "0.25",
	'0.2'	=> "0.2",
	'0.1'	=> "0.1",
	'0.05'	=> "0.05",
	'0.01'	=> "0.01",
	'0.005'	=> "0.005"
);

$dining_room_refresh_intervals = array(
	'0'		=> speak("Disable"),
	'10'	=> "10",
	'15'	=> "15",
	'20'	=> "20",
	'30'	=> "30",
	'45'	=> "45",
	'60'	=> "60"
);

$phone_carriers = array(
	'att'     => "AT&T",
	'sprint'  => "Sprint",
	'tmobile' => "T-Mobile",
	'verizon' => "Verizon"
);

$dp = $location_info['disable_decimal'];
$dc = $location_info['decimal_char'];
$default_preauth_amounts = array();
$default_preauth_amounts['1'] = number_format(1, $dp, $dc, "");
for ($amt = 5; $amt <= 100; $amt += 5)
{
	$default_preauth_amounts[$amt] = number_format($amt, $dp, $dc, "");
}

$register_settings_array = array("receipt"=>"receipt");
for ($r = 2; $r <= 20; $r++)
{
	$R = "receipt".$r;
	$register_settings_array[$R] = $R;
}

// Get list of template which will be used for this country
include_once($_SERVER['DOCUMENT_ROOT'].'/lib/jcvs/FiscalReceiptTplApi.php');
include_once($_SERVER['DOCUMENT_ROOT'].'/cp/areas/extensions/extensions_functions.php');
$obj=new FiscalReceiptTplApi(sessvar('admin_dataname'),array('action'=>'gettplbyregion'));
$fiscalTemplatesRawArr=$obj->doProcess();
$fiscalTemplatesArr[0] = 'None';
if($fiscalTemplatesRawArr){
	foreach($fiscalTemplatesRawArr as $templateDetails) {
		$fiscalTemplatesArr[$templateDetails['fiscal_receipt_tpl_id']] = $templateDetails['title'];
	}
}

$lavu_only	= "<font color='#588604' style='font-size:14px'><b>*</b></font> ";
$forward_to	= "index.php?mode=".$section."_".$mode;
$tablename	= "locations";
$rowid		= $locationid;

function getCurrencySettings()
{
	$locationCurrencyInfo=array();
	$query = "SELECT setting, value  FROM `config` where type='location_config_setting' and setting in ('monitary_symbol','primary_currency_code', 'currency_region') ";
	$result = lavu_query($query);
	if (!$result)
	{
		error_log("mysql error in ".__FILE__." mysql error:" . lavu_dberror());
	}
	else
	{
		while ($region_info = mysqli_fetch_assoc($result))
		{
			$locationCurrencyInfo[$region_info['setting']] = $region_info['value'];
		}
	}

	return $locationCurrencyInfo;
}

$companyId = sessvar("admin_companyid");
$primaryResIdData = getCompanyInfo($companyId, 'id');
$primaryResChainId = (!empty($primaryResIdData['chain_id']))?$primaryResIdData['chain_id']:null;
$primaryResId = null;
if ($primaryResChainId!=null)
{
	$primaryResId = getPrimaryChainRestaurant($primaryResChainId);
	$_SESSION['primary_restaurantid'] = $primaryResId;
}

function getMasterCurrencyCodes($primaryResId, $enabled=1)
{
	$currencyCodes=array(""=>"Select Currency");
	if (!is_null($primaryResId))
	{
		$query = "select A.id, A.name, A.alphabetic_code from poslavu_MAIN_db.country_region as A JOIN poslavu_MAIN_db.master_currencies as B
			ON (A.id=B.country_region_id) WHERE B.master_id=[1] AND _enabled=[2]  Group BY B.country_region_id ORDER BY A.name ";

		$result = mlavu_query($query, $primaryResId, $enabled);
		if (!$result)
		{
			error_log("mysql error in ".__FILE__." mysql error:" . lavu_dberror());
		}
		else
		{
			if (mysqli_num_rows($result)>0)
			{
				while ($region_info = mysqli_fetch_assoc($result))
				{
					$displayedValue = formatEncodeCountryCodeWithName($region_info['alphabetic_code'], $region_info['name']);
					$currencyCodes[$region_info['id']] = $displayedValue;
				}
			}
		}
	}

	if (count($currencyCodes)==1)
	{
		$query = "select A.id, A.name, A.alphabetic_code from poslavu_MAIN_db.country_region as A ORDER BY A.name ";
		$result = mlavu_query($query);
		if (!$result)
		{
			error_log("mysql error in ".__FILE__." mysql error:" . lavu_dberror());
		}
		else
		{
			if (mysqli_num_rows($result)>0)
			{
				while ($region_info = mysqli_fetch_assoc($result))
				{
					$displayedValue = formatEncodeCountryCodeWithName($region_info['alphabetic_code'], $region_info['name']);
					$currencyCodes[$region_info['id']] = $displayedValue;
				}
			}
		}
	}

	return $currencyCodes;
}

$include_session = "?session_id=".session_id();
$include_data_name = "&dn=".sessvar("admin_dataname");

$currency_master_exchange_rates_iframe_src = "/cp/areas/modal_edit_master_exchange_rates.php".$include_session.$include_data_name;
$currency_exchange_rates_iframe_src = "/cp/areas/modal_edit_exchange_rates.php".$include_session.$include_data_name;
$fiscaltemplate_iframe_src = "/cp/areas/modal_edit_fiscaltemplate.php".$include_session.$include_data_name;
$modalNumber = 0;

//---------------------------------LP-6409---------------------------------//
$currency_master_sec_exchange_rates_iframe_src = "/cp/areas/modal_edit_secondary_exchange_rates.php".$include_session.$include_data_name;
$currency_sec_exchange_rates_iframe_src = "/cp/areas/modal_edit_secondary_exchange_rates.php".$include_session.$include_data_name;
//---------------------------------LP-6409---------------------------------//

$fields = array();

	// - - - - - - - - Currency Settings - - - - - - - - //

	$fields = array_merge( $fields, array(
		array(
			speak("Currency Settings"),
			"Currency Settings",
			"seperator"
			)
		));

	if ($primaryResId == $companyId && intval(sessvar("admin_access_level")) >= 4)
	{
		$fields = array_merge( $fields, array(
			array(
				speak("Set master currencies").":",
				"master_currency_rates",
				"open_modal_button",
				array(
					'prefix'		=> "",
					'value'			=> speak("View / Edit"),
					'link'			=> "index.php?mode=settings_edit_master_exchange_rates&edit=1",
					'header_text'	=> "Set Master Currencies",
					'iframe_src'	=> $currency_master_exchange_rates_iframe_src,
					'modal_number'	=> $modalNumber++
				)
			),
			array(
				speak("Currency exchange rates").":",
				"exchange_rates",
				"open_modal_button",
				array(
					'prefix'		=> "",
					'value'			=> speak("View / Edit"),
					'link'			=> "index.php?mode=settings_edit_exchange_rates&edit=1",
					'header_text'	=> "Edit Currency Exchange Rates",
					'iframe_src'	=> $currency_exchange_rates_iframe_src,
					'modal_number'	=> $modalNumber++
				)
			)
		));
	}

	$fields = array_merge( $fields, array(
		array(
			speak("Primary currency code").":",
			"config:primary_currency_code",
			"select",
			getMasterCurrencyCodes($primaryResId)
		),
		array(
			speak("Monetary symbol").":",
			"monitary_symbol",
			"text",
			"size:8"
		),
		array(
			speak("Secondary currency code (for receipts)").":",
			"config:secondary_currency_code",
			"select",
			getMasterCurrencyCodes($primaryResId)
		),
		array(
			speak("Secondary monetary symbol").":",
			"secondary_monitary_symbol",
			"text",
			"size:8"
		),
		array(
			speak("Secondary currency receipt label").":",
			"secondary_currency_receipt_label",
			"text",
			"size:8"
		),
		array(
			speak("Secondary Currency exchange rates").":",
			"secondary_exchange_rates",
			"open_modal_button",
			array(
				'prefix'		=> "",
				'value'			=> speak("View / Edit"),
				'link'			=> "index.php?mode=settings_edit_exchange_rates&edit=1",
				'header_text'	=> "Edit Secondary Currency Exchange Rates",
				'iframe_src'	=> $currency_sec_exchange_rates_iframe_src,
				'modal_number'	=> $modalNumber++
			)
		),
		array(
				speak("Access level required to edit exchange rate in the POS application").":",
				"config:accessLevelToEditSecondaryExchangeRates",
				"select",
				$server_levels
		),
		array(
			speak("Monetary symbol position").":",
			"left_or_right",
			"select",
			array(
				'left'	=> speak("Before number"),
				'right'	=> speak("After number")
			)
		),
		array(
			speak("Display monetary symbol on printed and emailed receipts").":",
			"config:monetary_symbol_on_receipts",
			"bool"
		),
		array(
			speak("Decimal separator character").":",
			"decimal_char",
			"select",
			array(
				'' => "None",
				'.'	=> ".",
				',' => ","
			)
		),
			array(
					speak("Enable secondary currency display on receipts").":",
					"config:enable_secondary_currency",
					"bool",
			),
			array(
					speak("Enable Multiple secondary currency").":",
					"config:enable_multiple_secondary_currency",
					"bool",
			),
		array(
			speak("Number of decimal places").":",
			"disable_decimal",
			"select",
			"0,1,2,3,4"
		),

	// - - - - - - - - Accounting Settings - - - - - - - - //

		array(
			speak("Accounting Settings"),
			"Accounting Settings",
			"seperator"
		),
		array(
			speak("Round totals to nearest").":",
			"rounding_factor",
			"select",
			$rounding_factors
		),
		array(
			speak("Rounding direction").":",
			"round_up_or_down",
			"select",
			array(
				''		=> speak("Up or Down"),
				'Up'	=> speak("Up"),
				'Down'	=> speak("Down")
			)
		),
		array(
			speak("Allow rounding to be manually toggled at checkout").":",
			"config:allow_rounding_toggle",
			"bool"
		),
		array(
			speak("Automatically disable rounding when paying remaining amount of check with credit card").":",
			"config:credit_card_disables_rounding",
			"bool"
		),
		array(
			speak("Monetary denominations").":",
			"Monetary denominations",
			"redirect_button",
			array(
				'prefix'	=> "",
				'value'		=> speak("View / Edit"),
				'link'		=> "index.php?mode=settings_edit_monetary_denominations&edit=1"
			)
		),
		array(
			speak("Cash reconciliation options").":",
			"Cash reconciliation options",
			"redirect_button",
			array(
				'prefix'	=> "",
				'value'		=> speak("View / Edit"),
				'link'		=> "index.php?mode=settings_edit_transaction_summary_items&edit=1"
			)
		),
		array(
			speak("Server reconciliation options").":",
			"Server reconciliation options",
			"redirect_button",
			array(
				'prefix'	=> "",
				'value'		=> speak("View / Edit"),
				'link'		=> "index.php?mode=settings_edit_transaction_summary_items&edit=1&server_reconciliation"
			)
		),
		array(
			speak("High Level Till Alert").":",
			"config:high_level_till_alert_interval",
			"select",
			array(
				'0' => speak("Disable Alerts"),
				'1' => speak("Display Once"),
				'5' => speak("Every 5 minutes"),
				'10' => speak("Every 10 minutes"),
				'15' => speak("Every 15 minutes"),
				'20' => speak("Every 20 minutes"),
				'25' => speak("Every 25 minutes"),
				'30' => speak("Every 30 minutes"),
				'35' => speak("Every 35 minutes"),
				'40' => speak("Every 40 minutes"),
				'45' => speak("Every 45 minutes"),
				'50' => speak("Every 50 minutes"),
				'55' => speak("Every 55 minutes"),
				'60' => speak("Every 60 minutes")
			)
		),
		array(
			speak("Fiscal Integration Options").":",
			"config:fiscal_integration_countries",
			"select",
			array(
				'Norway'	=> speak("Norway"),
				'Uruguay'	=> speak("Uruguay"),
				'Argentina'	=> speak("Argentina"),
				'Italy'		=> speak("Italy"),
				'Brazil'	=> speak("Brazil"),
				'Spain'		=> speak("Spain"),
				'Chile'		=> speak("Chile")
			)
		),
		array(
			speak("High Level Till Alert Threshold Amount").":",
			"config:high_level_till_alert_threshold_amount",
			"text",
			"size:10"
		),
		array(
			speak("Hide Available Amount to be deposited for Bank Deposit").":",
			"config:availability_bank_deposit",
			"bool"
		),
		array(
			speak("Restrict user to making one Bank Deposit per day").":",
			"config:restrict_bank_deposit",
			"bool"
		),
		array(
			speak("Required Change Bank Amount").":",
			"config:required_change_bank_amount",
			"text",
			"size:10"
		),
		array(
			speak("Bank Deposit Options").":",
			"Bank Deposit Options",
			"redirect_button",
			array(
				'prefix'	=> "",
				'value'		=> speak("View / Edit"),
				'link'		=> "index.php?mode=settings_edit_transaction_summary_items&edit=1&bank_deposit"
			)
		),
		array(
			speak("Fiscal Tax Code").":",
			"config:fiscal_tax_code",
			"text",
			"size:10"
		),
		array(
			speak("Day start/end time").":",
			"day_start_end_time",
			"time_no_late_night"
		)
	));

	if ($min_POSLavu_version < 20200)
	{
		$fields[] = array(
			speak("Default Tax Rate")." (2.1.6-):",
			"taxrate",
			"text",
			"size:10"
		);
	}

	$fields = array_merge( $fields, array(
		array(
			speak("Tax included in all item prices").":",
			"tax_inclusion",
			"bool"
		),
		array(
			speak("Display base subtotal for orders with tax included items").":",
			"config:tax_included_orders_display_base_subtotal",
			"bool"
		),
		array(
			speak("Display included tax breakdown on order").":",
			"config:display_included_taxes_on_order",
			"select",
			array(
				'0' => speak("Never"),
				'1' => speak("For tax exempt orders only"),
				'2' => speak("Always")
			)
		),
		array(
			speak("Display included tax breakdown on receipts").":",
			"config:display_included_taxes_on_receipt",
			"select",
			array(
				'0' => speak("Never"),
				'1' => speak("For tax exempt orders only"),
				'2' => speak("Always")
			)
		),
		array(
			speak("Include zero tax subtotals in included tax breakdown").":",
			"config:included_tax_breakdown_show_zero_tax",
			"bool"
		),
		array(
			speak("Allow tax exempt orders").":",
			"allow_tax_exempt",
			"bool"
		),
		array(
			speak("Access level required to grant tax exemption").":",
			"level_to_grant_tax_exempt",
			"select",
			$server_levels
		),
		array(
			speak("Display tax breakdown on order for tax exempt orders").":",
			"config:display_exempt_taxes_on_order",
			"bool"
		),
		array(
			speak("Display tax breakdown on receipts for tax exempt orders").":",
			"config:display_exempt_taxes_on_receipt",
			"bool"
		),
		array(
			speak("Hide 'No Tax' line when no tax applies to order").":",
			"config:hide_no_tax_line",
			"bool"
		),
		array(
			"Disable Multiple Happy Hours Per Day:",
			"config:multiple_happy_hours_per_day",
			"negative bool"
		)
	));

	if (is_lavu_admin())
	{
		$fields = array_merge( $fields, array(
			array(
				$lavu_only.speak("Enable Fiscal Templates (France)").":",
				"config:enable_fiscal_template_france",
				"bool"
			)
		));
	}

	$fields = array_merge( $fields, array(
		array(
			speak("Enable Fiscal Receipt Templates").":",
			"config:enable_fiscal_receipt_templates",
			"bool"
		),
		array(
			speak("Fiscal Receipt Template").":",
			"config:fiscal_receipt_template",
			"open_modal_button",
			array(
				'prefix'		=> "",
				'value'			=> speak("View / Edit"),
				'link'			=> "index.php?mode=settings_edit_fiscaltemplate&edit=1",
				'header_text'	=> "Edit Fiscal Template",
				'iframe_src'	=> $fiscaltemplate_iframe_src,
				'modal_number'	=> 'template'
			)
		),
		array(
			speak("Enable Fiscal Invoice").":",
			"config:enable_fiscal_invoice",
			"bool"
		),
		array(
			speak("Invoice Number Series").":",
			"config:invoice_no_series",
			"invoice_no_series_textbox",
		),
		array(
			speak("Invoice Series Valid until").":",
			"invoice_series_valid_till",
			"date_picker",
		),
		array(
			speak("Invoice Number Prefix").":",
			"invoice_no_prefix",
			"invoice_no_prefix_textbox",
		),
		array(
			speak("Invoice Number Suffix").":",
			"invoice_no_postfix",
			"invoice_no_postfix_textbox",
		),
	));

	// - - - - - - - - General Interface Settings - - - - - - - - //

	$service_types = array(
		'2' => speak("Quick Serve"),
		'3' => speak("Tables"),
		'1' => speak("Tabs"),
		'4' => speak("Order List")
	);

	$fields = array_merge( $fields, array(
		array(
			speak("General Interface Settings"),
			"General Interface Settings",
			"seperator"
		),
		array(
			speak("Default service type").":",
			"tab_view",
			"select",
			$service_types
		),
		array(
			speak("Include Quick Serve button on Tab Screen").":",
			"config:display_quick_serve_tab",
			"bool"
		),
		array(speak("Default dining room background").":","default_dining_room_background","select",$dining_room_backgrounds),
		array(speak("Seconds between Dining Room view auto refresh").":","config:dining_room_auto_refresh","select",$dining_room_refresh_intervals),
		array(speak("Allow non-admin users to toggle between service types").":","allow_tab_view_toggle","bool"),
		array(speak("Show table orders in tabs view").":","tabs_and_tables","bool"),
		array(speak("Hide tabs that don't belong to active server").":","hide_tabs","bool"),
		array(speak("Restrict order access by server").":","lock_orders","bool"),
		array(speak("Ask for guest count when selecting a table").":","ask_for_guest_count","bool"),
		array(speak("Ask for guest count when creating a tab").":","config:tab_ask_for_guest_count","bool"),
		array(speak("PIN time out seconds")." (".speak("iPad only")." - ".speak("0 disables")."):","PINwaitM","text","size:8"),
		array(speak("PIN time out seconds while in order")." (".speak("iPad only")." - ".speak("0 disables")."):","PINwait","text","size:8"),
		array(speak("Require admin PIN to set terminal").":","admin_PIN_terminal_set","bool"),
		array(speak("Require admin PIN to print till report").":","admin_PIN_till_report","bool"),
		array(speak("Access level required to transfer a table").":","config:level_to_access_transfer_table","select",array("0"=>"Select Access Level", "1"=>"1", "2"=>"2", "3"=>"3", "4"=>"4")),
		array(speak("Access level required for Manager Functions").":","config:level_to_access_manager_functions","select",$server_levels),
		array(speak("Access level required for Management Tools").":","config:level_to_access_management_area","select",$server_levels),
			array(speak("Access level required for Deposit and Refund Functions").":","config:level_to_access_deposit_refund_functions","select",$server_levels),
		array(speak("Edit states list").":","Edit states list", "redirect_button", array("prefix"=>"", "value"=>speak("View / Edit"), "link"=>"index.php?mode=settings_edit_states")),
		array(speak("Enable Hold and Fire functionality (disables auto-send when printing check or checking out)").":", "config:show_hold_and_fire", "bool" ),
		array(speak("Show product code and tax code in Menu Items screen").":","config:display_product_code_and_tax_code_in_menu_screen","bool"),
		array(speak("Change date format").":","config:date_format","select",array("2"=>"MM/DD/YY","1"=>"DD/MM/YY","3"=>"YY/MM/DD"))
	));

	// - - - - - - - - Skin Pack Settings - - - - - - - - //

	$skinpack_list = array(
		''				=> speak("Classic UI"),
		'dark_ui'			=> speak("Dark UI"),
		'light_ui'			=> speak("Light UI")
	);

	if (is_lavu_admin())
	{
		$skinpack_list['inbev'] = "AB InBev";
		$skinpack_list['dutch_brothers'] = "Dutch Bros";
	}

	if (is_lavu_admin())
	{
		$fields[] = array(
		$lavu_only.speak("Skin pack")." (3.3.0+):",
		"config:skinpack",
		"select",
		$skinpack_list
		);
	}

	$fields[] = array(
		speak("Show alert on POS when API orders are received").":",
		"config:show_alert_on_pos",
		"bool"
		);

	// - - - - - - - - Menu Interface Settings - - - - - - - - //

	$fields = array_merge( $fields, array(
		array(speak("Menu Interface Settings"),"Menu Interface Settings","seperator"),
		array(speak("Default color scheme for item and category icons").":","item_icon_color","select",$item_icon_colors),
		array(speak("Menu icon style").":","config:menu_icons_square_corners","select",array("0"=>speak("Rounded corners"),"1"=>speak("Square corners"))),
		array(speak("Hide category titles on iPad").":","config:hide_category_titles","bool"),
		array(speak("Hide item titles on iPad").":", "hide_item_titles", "bool"),
		array(speak("Maximum number of menu category icons to show without scrolling").":", "config:visible_menu_categories", "select", "7,8,9")));

	// - - - - - - - - Order Options - - - - - - - - //
	$fields = array_merge($fields, array(
		array(speak("Menu items per row").":","config:menu_items_per_row","select","4,5"),
		array(speak("Display of the optional modifiers on iPad Vertically").":","config:optional_modifiers_display","bool")
	));

	// - - - - - - - - Combo Builder - - - - - - - - //
	$fields = array_merge( $fields, array(
			array(speak("Enable Combo Builder functionality").":","config:disable_combo_bulider","bool")
	));

	if (is_lavu_admin())
	{
		$fields = array_merge( $fields, array(
			array(
				$lavu_only.speak("Show Norway Mapping").":",
				"config:norway_mapping",
				"bool"
			),
			array(
				$lavu_only.speak("Enter Norway VAT ID").":",
				"config:norway_vat_id",
				"text",
				array( "size" => 16, "id" => "norway_vat_id" )
			)
		));
	}

	// - - - - - - - -Manage Item Availability - - - - - - - - //
	$fields = array_merge( $fields, array(
			array(
					speak("Manage Item Availability").":",
					"config:manage_item_availability",
					"bool"
			),
			array(
					speak("Access level required to manage item availability").":",
					"config:access_level_manage_item_availability",
					"select",
					$server_levels
			)
	));

	// - - - - - - - - Order Options - - - - - - - - //
	$fields = array_merge( $fields, array(
		array(speak("Order Options"),"Order Options","seperator"),
		array(speak("Order pad font")." (".speak("iPad")."):","order_pad_font","select",$available_fonts),
		array(speak("Order pad font")." (".speak("iPhone/iPod")."):","order_pad_font_ipod","select",$available_fonts_ipod)
	));

	if (is_lavu_admin())
	{
		$fields[] = array(
			$lavu_only."Order Pad line height:",
			"config:order_pad_line_height",
			"select",
			array(
				'30' => "30",
				'40' => "40"
			)
		);
	}

	$fields = array_merge( $fields, array(
		array(
			speak("Display active seat icon").":",
			"display_seat_course_icons",
			"bool"
		),
		array(
			speak("Display active course icon").":",
			"config:display_course_icon",
			"bool"
		),
		array(
			speak("Display forced modifier prices").":",
			"display_forced_modifier_prices",
			"bool"
		),
		array(
			speak("Return to order pad after selecting an item")." (".speak("iPhone/iPod")."):",
			"return_after_add_item_on_ipod",
			"bool"
		),
		array(
			speak("Automatically combine equivalent items on ticket").":",
			"group_equivalent_items",
			"bool"
		),
		array(
			speak("Access level required to manually override item prices").":",
			"config:level_to_override_price",
			"select",
			$server_levels_with_disable
		),
		array(
			speak("Access level required to edit and remove saved items").":",
			"config:level_to_edit_saved_items",
			"select",
			$server_levels
		),
		array(
			speak("Access level required to edit and remove sent items").":",
			"level_to_edit_sent_items",
			"select",
			$server_levels
		),
		array(
			speak("Allow order resend").":",
			"allow_resend",
			"bool"
		),
	));

	if (is_lavu_admin())
	{
		$fields = array_merge( $fields, array(
			array(
				$lavu_only.speak("Incorporate unique device prefixes into order numbers")." (".speak("Lavu Retro only")."):",
				"config:use_device_prefix",
				"bool"
			),
			array(
				$lavu_only.speak("Disable device level auto save").":",
				"config:disable_auto_save",
				"bool"
			)
		));
	}

	if ($min_POSLavu_version < 30300)
	{
		$fields[] = array(
			"Keep device order backups for offline mode (2.3.15-):",
			"config:offline_order_mode",
			"select",
			array(
				'1' => speak("Open orders only"),
				'2' => speak("All orders"),
				'0' => speak("Disable")
			)
		);
	}

	if (is_lavu_admin())
	{
		$fields[] = array(
			$lavu_only."Disable backup order loading in online mode:",
			"config:disable_backup_loading_in_online_mode",
			"bool"
		);
	}

	$fields = array_merge( $fields, array(
		array(
			speak("Prevent servers from clocking out when they have open orders").":",
			"config:open_orders_prevent_clock_out",
			"bool"
		),
		array(
			speak("Prevent servers from printing Server Summary when they have open orders").":",
			"config:open_orders_prevent_server_summary",
			"bool"
		),
		array(
			speak("Access level required to open an order that is already open on another device").":",
			"config:level_to_override_held_by_order_warning",
			"select",
			$server_levels_with_disable
		),
		array(
			speak("Bluetooth scale type").":",
			"config:enable_uartx_bluetooth_scale",
			"select",
			array(
				'0' => speak("None"),
				//'1' => "Uart-X",
				'2' => "Tor Rey L-EQ via BlueSnap Adapter"
			)
		),
		array(
			speak("Minimum access level required to manually enter weight").":",
			"config:level_to_manually_enter_weight",
			"select",
			$server_levels
		)
	));

	if (is_lavu_admin())
	{
		$fields = array_merge( $fields, array(
			array(
				$lavu_only."Print serial number on order receipts when available:",
				"config:order_receipts_serial_no",
				"bool"
			)
		));
	}

	$fields = array_merge( $fields, array(
		array(
			   "Disable inter-order item transfers:",
			   "config:disable_item_transfers",
			   "bool"
			)
		));

	if (is_lavu_admin())
	{
		$fields = array_merge( $fields, array(
			// - - - - - - - - Self Service Mode - - - - - - - - //
			array(
				$lavu_only.speak("Self Service Mode")." (".speak("Kiosk Mode").")",
				"Self Service Mode",
				"seperator"
			),
			array(
				$lavu_only."Access level required to toggle Self Service Mode:",
				"config:level_to_toggle_self_service_mode",
				"select",
				$server_levels_with_disable
			),
			array(
				$lavu_only."Ask for guest count when starting new order in Self Service Mode:",
				"config:self_serve_ask_for_guest_count",
				"bool"
			)
		));
	}

	// - - - - - - - - Order List Options - - - - - - - - //

	$fields = array_merge( $fields, array(
		array(
			speak("Order List Options"),
			"Order List Options",
			"seperator"
		),
		array(
			speak("Access level required to perform voids and refunds from the Open/Closed Orders screen").":",
			"config:order_list_void_access_level",
			"select",
			$server_levels_with_disable
		),
		array(
			speak("Allow order cash tip editing from Open/Closed Orders screen")." (".speak("Cash Tips Per Order Mode only")."):",
			"config:edit_cash_tips_from_order_list",
			"bool"
		),
		array(
			speak("Allow payments to be converted to other payment types from the Open/Closed Orders screen").":",
			"config:order_list_cash_conversion",
			"bool"
		),
		array(
			speak("Disable Reopen Order functionality").":",
			"config:disable_reopen_order",
			"bool"
		)
	));

	if (is_lavu_admin())
	{
		$fields = array_merge( $fields, array(
			array(
				$lavu_only."Skip animated order list updates on Open/Closed Orders Screen:",
				"config:order_list_update_force_reload",
				"bool"
			)
		));
	}

	// - - - - - - - - Checkout Options - - - - - - - - //

	$fields = array_merge( $fields, array(
		array(
			speak("Checkout Options"),
			"Checkout Options",
			"seperator"
		),
		array(
			speak("Use floating decimal for main number pad").":",
			"config:use_checkout_floating_decimal",
			"bool"
		),
		array(
			speak("Minimum guests to add gratuity automatically").":",
			"ag1",
			"text",
			"size:8"
		),
		array(
			speak("Gratuity percent").":",
			"ag2",
			"text",
			"size:8"
		),
		array(
			speak("Gratuity label").":",
			"gratuity_label",
			"text",
			"size:25"
		),
		array(
			speak("Gratuity Text").":",
			"config:gratuity_text",
			"text",
			"size:25"
		),
		array(
			speak("Access level required to manually toggle gratuity").":",
			"config:level_to_toggle_gratuity",
			"select",
			$server_levels
		),
		array(
			speak("Allow toggle gratuity at the check level").":",
			"config:toggle_gratuity_at_the_check_level",
			"bool"
		),
		array(
			speak("Allow lock check changes after payment").":",
			"config:lock_check_changes_after_payment",
			"bool"
		),
		array(
			speak("Tax and gratuity calculation scheme").":",
			"tax_auto_gratuity",
			"select",
			array(
				'2' => speak("Both based on subtotal"),
				'1' => speak("Gratuity before Tax"),
				'0' => speak("Tax before Gratuity")
			)
		),
		array(
			speak("Subtotal on which to base tax").":",
			"config:use_subtotal_for_tax",
			"select",
			array(
				'0' => speak("Subtotal after discounts"),
				'1' => speak("Subtotal after item discounts"),
				'2' => speak("Subtotal before discounts")
			)
		),
		array(
			speak("Subtotal on which to base auto gratuity").":",
			"config:auto_gratuity_subtotal",
			"select",
			array(
				'0' => speak("Subtotal after discounts"),
				'1' => speak("Subtotal after item discounts"),
				'2' => speak("Subtotal before discounts")
			)
		),
		array(
			speak("Credit card info to ask for when integration is turned off").":",
			"get_card_info",
			"select",
			array(
					'0' => speak("None"),
					'1' => speak("Card Type, Last Four and Card Tip"),
					'2' => speak("Card Type and Card Tip"),
					'3' => speak("Card Type and Last Four")
			)
		),
		array(
			speak("Disable default Gift Certificate payment type").":",
			"config:disable_default_gift_certificates",
			"bool"
		),
		array(
			speak("Verify payment amount when amount is keyed in").":",
			"verify_entered_payment",
			"bool"
		),
		array(
			speak("Verify payment amount when remaining amount is automatically used").":",
			"verify_remaining_payment",
			"bool"
		),
		array(
			speak("Verify payment amount for quick pay buttons").":",
			"verify_quickpay",
			"bool"
		),
		array(
			speak("Access level required for generic discount types").":",
			"admin_PIN_discount",
			"select",
			$server_levels_with_disable
		), // used offset values to maintain backward compatibility, i.e. 0 = false for versions prior to 2.1
		array(
			speak("Prompt user for reason when applying generic discount types").":",
			"config:generic_discount_reason",
			"bool"
		),
		array(
			speak("Allow preset amount discounts when the discount exceeds check remaining amount").":",
			"config:allow_check_overdiscounts",
			"bool"
		),
		array(
			speak("Access level required to void items").":",
			"config:level_to_void_items",
			"select",
			$server_levels
		),
		array(
			speak("Require admin PIN to void orders or checks").":",
			"admin_PIN_void",
			"bool"
		),
		array(
			speak("Require admin PIN to void payments or refunds").":",
			"admin_PIN_void_payments",
			"bool"
		),
		array(
			speak("Require admin PIN to perform refunds").":",
			"admin_PIN_refund",
			"bool"
		),
		array(
			speak("Skip reason prompt when performing refunds").":",
			"config:skip_refund_reason",
			"bool"
		),
		array(
			speak("Display receipt print options at checkout").":",
			"ask4email_at_checkout",
			"bool"
		),
		array(
			speak("Seconds before automatically dismissing receipt print options on iPad").":",
			"config:auto_dismiss_receipt_print_options",
			"select",
			array(
				'0'		=> speak("Disable"),
				'10'	=> "10",
				'20'	=> "20",
				'30'	=> "30",
				'40'	=> "40",
				'50'	=> "50",
				'60'	=> "60"
			)
		),
		array(
			speak("Allow system to attempt HTML receipt emails").":",
			"config:multipart_emails",
			"bool"
		),
		array(
			speak("Print receipt by default").":",
			"default_receipt_print",
			"bool"
		),
		array(
			speak("Print Lavu Loyalty and Gift receipts").":",
			"config:loyalty_gift_auto_print",
			"bool"
		),
		array(
			speak("Lavu Loyalty and Gift expiration").":","config:loyalty_gift_expiration","select",array(""=>"Never Expires","d.30"=>"Expires after 30 days","d.90"=>"Expires after 90 days","d.180"=>"Expires after 180 days","y.1"=>"Expires after 1 year","y.2"=>"Expires after 2 years","y.4"=>"Expires after 4 years")
		),
		array(
			speak("Bypass 'Check Out Complete' message at end of checkout in Quick Serve").":",
			"bypass_checkout_message",
			"select",
			array(
				'0' => speak("No"),
				'1' => speak("Yes"),
				'2' => speak("If no change due"),
				'3' => speak("If receipt was printed")
			)
		),
		array(
			speak("Bypass 'Check Out Complete' message at end of checkout in Table mode").":",
			"config:bypass_checkout_message_table",
			"select",
			array(
				'0' => speak("No"),
				'1' => speak("Yes"),
				'2' => speak("If no change due"),
				'3' => speak("If receipt was printed")
			)
		),
		array(
			speak("Bypass 'Check Out Complete' message at end of checkout in Tabs mode").":",
			"config:bypass_checkout_message_tabs",
			"select",
			array(
				'0' => speak("No"),
				'1' => speak("Yes"),
				'2' => speak("If no change due"),
				'3' => speak("If receipt was printed")
			)
		),
		array(
			speak("Include forced modifiers on customer receipt").":",
			"print_forced_mods_on_receipt",
			"bool"
		),
		array(
			speak("Include optional modifiers on customer receipt").":",
			"print_optional_mods_on_receipt",
			"bool"
		),
		array(
			speak("Include item notes on customer receipt").":",
			"print_item_notes_on_receipt",
			"bool"
		),
		array(
			speak("Auto send when printing check or checking out").":",
			"auto_send_at_checkout",
			"bool"
		),
		array(
			speak("Ask for email address to which to send receipt at bottom of printed check").":",
			"config:printed_check_email_question",
			"bool"
		),
		array(
			speak("Mute register ring").":",
			"mute_register_bell",
			"bool"
		),
		array(
			speak("Access level required to open cash drawer").":",
			"level_to_open_register",
			"select",
			$server_levels
		),
		array(
			speak("Access level required to open cash drawer when entering payment").":",
			"level_to_open_register_at_checkout",
			"select",
			$server_levels
		),
		array(
			speak("Open cash drawer when applying cash transactions").":",
			"cash_transactions_open_drawer",
			"bool"
		),
		array(
			speak("Open cash drawer when applying credit transactions").":",
			"credit_transactions_open_drawer",
			"bool"
		),
		array(
			speak("Open cash drawer when applying other transaction types").":",
			"other_transactions_open_drawer",
			"bool"
		),
		array(
			speak("Enable Dual Cash Drawer").":",
			"config:enable_dual_cash_register",
			"bool"
		),
		array(
			speak('PIN prompt required for below access level to open cash drawer').':',
			'config:level_to_access_dual_cash_drawer_pin',
			'select',
			$server_levels
		)
	));
	$fields = array_merge( $fields, array(
		array(
			speak("Allow entered gift certificate amount to be more than remaining amount on check").":",
			"config:allow_gift_certificate_over_pay",
			"bool"
		),
		array(
			speak("Prompt for a cash tip at checkout")." (".speak("Cash Tips Per Order Mode only")."):",
			"config:cash_tip_at_checkout",
			"select",
			array(
				'0' => speak("Never"),
				'1'	=> speak("When an order is paid only with cash"),
				'2' => speak("When an order has at least one cash payment"),
				'3' => speak("Always")
			)
		),
		array(
			speak("Autofill checkout cash tip prompt with last cash payment's change value").":",
			"config:checkout_cash_tip_use_change",
			"bool"
		)
	));

	if (is_lavu_admin())
	{
		$fields[] = array(
			$lavu_only.speak("Allow use of RFID scanner for payments").":",
			"allow_RFID",
			"bool"
		);
	}

	$fields = array_merge( $fields, array(
		array(speak("Allow use of RFID scanner for PIN entry").":","config:allow_RFID_clock_punch","bool"),
		array(speak("Identifier to use when selecting order for Merge Order/Add to Tab").":","config:merge_order_list_identifier","select",array("tablename"=>speak("Table/Tab Name"),"order_id"=>speak("Order ID"))),
		array(speak("Access level required for merging orders").":","config:level_to_merge_orders","select",$server_levels_with_disable),
		array(speak("How to handle guest count when merging orders").":","config:merge_order_guest_count_handling","select",array("0"=>speak("Use target order's guest count"),"1"=>speak("Use source order's guest count"),"2"=>speak("Combine guest counts"),"3"=>speak("Prompt user")))
	));

	if (is_lavu_admin())
	{
		$fields = array_merge( $fields, array(
			array(
				$lavu_only."Allow non-credit card payments through tableside devices:",
				"config:allow_tableside_non_credit_card_payments",
				"bool"
			),
			array(
				$lavu_only."Lower checkout panel button layout:",
				"config:lower_checkout_panel_layout",
				"text",
				"size:25"
			)
		));
	}
	// LP-5326 changes START
	if ( sessvar('admin_access_level')>=3 and sessvar('admin_access_level') <=4 ) {
			$fields[] = array(
					speak("Access level required to force close an order").":",
					"config:level_to_close_order",
					"select",
					$force_close_server_levels
			);
	}
	// LP-5326 changes END

	// - - - - - - - - Receipt Options - - - - - - - - //

	$fields = array_merge( $fields, array(
		array(
			speak("Receipt Options"),
			"Receipt Options",
			"seperator"),
		array(
			speak("Display product code on receipts").":",
			"config:product_code_receipt",
			"bool"
		),
		array(
			speak("Display tax code on receipts").":",
			"config:tax_code_receipt",
			"bool"
		),
		array(
			speak("Display item quantity sum on receipts").":",
			"config:item_quantity_sum_on_receipts",
			"bool"
		),
		array(
						speak("Receipt survey text").":",
						"config:receipt_postscript",
						"textarea",
						"rows:20"
		),
		array(
				speak("Pickup Label type").":",
				"config:order_number_for_customer_pick_up",
				"select",
				array(
						'disable'	=> "Disable",
						'enable'	=> "Auto-assign pickup numbers",
						'prompt_for_number'	=> "Prompt for unique pickup name/number"
				)
		),
		array(
				speak("Print placement for Pickup Label").":",
				"config:place_order_number_on",
				"select",
				array(
						'top'	=> "Top",
						'bottom'=> "Bottom"
				)
		),array(
				speak("Pickup number format").":",
				"config:pickup_number_format",
				"select",
				array(
						'3'	=> "001",
						'4'	=> "0001",
						'5'	=> "00001",
				)
		),
			array(
					speak("Include Pickup Label on the following print jobs").":",
					"config:print_pickup_preference",
					"select",
					array(
							'0' => speak("Both"),
							'1' => speak("Kitchen Only"),
							'2' => speak("Receipt Only")
					)
			),
			array(
					speak("Use Pickup Label for the following Service Types").":",
					"config:print_pickup_ordertype",
					"bool_pickup_ordertypes"
			)
	));

	if (is_lavu_admin())
	{
		$fields = array_merge( $fields, array(
			array(
				$lavu_only."Receipt format string:",
				"config:receipt_format_string",
				"text",
				"size:25"
			),
			array(
				$lavu_only."Append special results mode:",
				"config:receipt_append_results_mode",
				"select",
				array(
					""			=> "",
					"meal_plan"	=> "Meal Plan Usage"
				)
			)/*,
				array(
						speak("Receipt survey text").":",
						"config:receipt_postscript",
						"textarea",
						"rows:20"
				)*/
		));
	}
	/* Lp-3150 For uruguay Display Internal Invoice number on Receipt Start */
	global $location_info;
	$country_name = strtolower($location_info['country']);
	global $modules;
	if ($modules->hasModule("reports.fiscal.uruguay")){
		$fields = array_merge( $fields, array(
				array(
						speak("Display Internal Invoice number on Receipt").":",
						"config:internal_invoice_number_receipt",
						"bool"
				)
		));

	}
	/* Lp-3150 For uruguay Display Internal Invoice number on Receipt END */
	$fields = array_merge( $fields, array(
			array(
				speak("Hide “Powered by Lavu” text on printed receipts").":",
				"config:powered_by_lavu_receipt",
				"bool")
			));
	$auto_exit_field_type = ($max_POSLavu_version >= 20309)?"select":"bool";
	$ipads_or_ipods = array(
		'0' => speak("No"),
		'1' => speak("iPads Only"),
		'2' => speak("iPods Only"),
		'3' => speak("iPads and iPods")
	);

	// - - - - - - - - User Settings - - - - - - - - //

	$fields = array_merge( $fields, array(
		array(speak("User Settings"),"User Settings","seperator"),
		array(speak("Auto exit order after save").":","exit_order_after_save",$auto_exit_field_type,$ipads_or_ipods),
		array(speak("Auto exit order after send").":","exit_order_after_send",$auto_exit_field_type,$ipads_or_ipods),
		array(speak("Auto exit order after print check").":","exit_order_after_print_check",$auto_exit_field_type,$ipads_or_ipods),
		array(speak("Auto exit server after save").":","exit_after_save",$auto_exit_field_type,$ipads_or_ipods),
		array(speak("Auto exit server after send").":","exit_after_send",$auto_exit_field_type,$ipads_or_ipods),
		array(speak("Auto exit server after print check").":","exit_after_print_check",$auto_exit_field_type,$ipads_or_ipods),
		array(speak("Auto exit server after checkout").":","exit_after_checkout",$auto_exit_field_type,$ipads_or_ipods),
		array(speak("Auto exit server after void").":","exit_after_void",$auto_exit_field_type,$ipads_or_ipods),
		array(speak("Employee clocking in becomes active server").":","config:set_active_user_at_clock_in","bool"),
		array(speak("Auto exit server upon employee clock out").":","config:clock_out_exits_server","select",array("0"=>speak("No"),"1"=>speak("When employee is active server"),"2"=>speak("Always"))),
		array(speak("Access level for which to block PIN usage when user is clocked out").":","config:block_clocked_out_server_level","select",array_merge(array("0"=>speak("Don't block")), $server_levels)),
		array(speak("Disallow user from clocking in prior to break period ending and choose break period").":","config:user_break_out","select",array("0"=>speak("off"),"10"=>"10 ".speak("minutes"),"15"=>"15 ".speak("minutes"),"30"=>"30 ".speak("minutes"),"60"=>"1 ".speak("hour")))
	));

	if (is_lavu_admin()) {
		$fields = array_merge( $fields, array(
			array(
				speak("Enable New Server Tips Screen").":",
				"config:enable_new_server_tip_screen",
				"bool"
			)
		));
	}

	if (is_lavu_admin())
	{
		$fields = array_merge( $fields, array(
			//LP-10058
			// array($lavu_only.speak("Disallow user from clocking minutes before/after shift").":","config:minutes_before_after_shift_block_clockin","select",array("off"=>speak("off"),"5"=>"5 ".speak("minutes"),"10"=>"10 ".speak("minutes"),"15"=>"15 ".speak("minutes"),"30"=>"30 ".speak("minutes"),"60"=>"1 ".speak("hour"))),

	// - - - - - - - - LLS Print Queue - - - - - - - - //

			array($lavu_only.speak("LLS Print Queue"),"LLS Print Queue","seperator"),
			array($lavu_only."Use LLS to queue print jobs:","config:lls_queue_print_jobs","bool"),
			array($lavu_only."LLS print queue delay in seconds:","config:lls_print_queue_delay","select",array("1"=>"1","2"=>"2","3"=>"3","4"=>"4","5"=>"5")),
			array($lavu_only."LLS print queue alert message address:","config:lls_print_queue_alert_email","text","size:40"),
			array($lavu_only."Minutes to allow print job to remain unsent before sending alert message:","config:lls_print_queue_alert_minutes","select",array("5"=>"5","6"=>"6","7"=>"7","8"=>"8","9"=>"9","10"=>"10"))
		));
	}

	// - - - - - - - - Reports - - - - - - - - //

	$fields = array_merge( $fields, array(
		array(speak("Reports"),"Reports","seperator"),
		array(speak("Restrict servers from accessing their own server summary").":","config:restrict_single_server_summary","bool"),
		array(speak("Server Summary / Till report type").":","config:till_report_type","select",array("sales"=>speak("Sales"),"payments"=>speak("Payments"))),
		array(speak("Include Amount owed as Cash Sales minus Card Tip in Server Summary").":","config:server_summary_include_owed","bool")
	));

	if (is_lavu_admin())
	{
		$fields[] = array(
			$lavu_only."Track shifts for:",
			"config:track_shifts_by",
			"select",
			array(
				'register'	=> "Individual Registers",
				'house'		=> "Entire House",
				'both'		=> "Either/Or"
			)
		);
	}

	$fields = array_merge( $fields, array(
		array(
			speak("Server cash tips record mode").":",
			"config:cash_tips_as_daily_total",
			"select",
			array(
				'0' => speak("Per order"),
				'1' => speak("Daily total"),
				'2' => speak("No user input")
			)
		),
		array(speak("Prompt servers to deposit cash before printing Server Summary").":","config:server_summary_cash_deposit","bool"),
		array(speak("Prompt servers to enter cash tips before printing Server Summary (Forces Cash Tips Daily Total Mode)").":","config:server_summary_enter_tips","bool"),
		array(speak("Prompt servers to enter tip out amounts before printing Server Summary").":","config:server_summary_tipout_overlay","bool"),
		array(speak("Ask server to clock out after printing Server Summary").":","config:server_summary_asks_to_clock_out","bool"),
		array(speak("Gift Revenue Tracked").":","config:gift_revenue_tracked","select",array("when_sold"=>speak("When Sold"), "when_redeemed"=>speak("When Redeemed"))),
		array(speak("Primary register summary").":","config:register_summary_function","select",array("all_day"=>speak("Prints totals for the day"), "current_shift"=>speak("Prints totals for current shift")))
	));

	if (is_lavu_admin())
	{
		$fields[] = array(
			$lavu_only."Z report triggers credit card batch settlement:",
			"config:z_report_triggers_cc_batch_settlement",
			"bool"
		);
	}

	$fields[] = array(
		speak("Use plain text printing for X and Z reports").":",
		"config:x_and_z_report_plain_text",
		"bool"
	);

	if (is_lavu_admin())
	{
		$fields = array_merge( $fields, array(
			array($lavu_only.speak("Hide these default reports").":","config:hide_report_ids","text","size:25"),
			array($lavu_only.speak("Allow access to these restricted back end reports").":","config:show_restricted_report_ids","text","size:25")
		));
	}

	$fields = array_merge( $fields, array(
		array(speak("Time interval to include other employee in server tipout").":","config:server_tips_time_interval_for_tipout","select",array("0"=>"Disable","30"=>"30 ".speak("minutes"),"45" => "45 ".speak("minutes"), "60" => "1 ".speak("hour"), "120" => "2 ".speak("hours"), "180" => "3 ".speak("hours"), "240" => "4 ".speak("hours")))
	));

	if (is_lavu_admin())
	{
		$fields = array_merge( $fields, array(
			array(
				$lavu_only."Hide Sales for Year chart on control panel home page:",
				"config:cp_home_hide_sales_for_year",
				"bool"
			),

	// - - - - - - - - Apple Only - - - - - - - - //

			array(
				$lavu_only."Apple Only",
				"Apple Only",
				"seperator"
			),
			array(
				$lavu_only."Enable Apple Meal Plan:",
				"config:enable_apple_meal_plan",
				"bool"
			),
			array(
				$lavu_only."Server path to use for checking RFID for meal plan transactions:",
				"config:RFID_meal_plan_path",
				"select",
				"Local,Remote"
			),
			array(
				$lavu_only."Special clock punch message:",
				"config:clock_out_special_message",
				"text",
				"size:45"
			),

	// - - - - - - - - Lavu Display Settings - - - - - - - - //

			array(
				$lavu_only.speak("Lavu Display Settings"),
				"Lavu Display Settings",
				"seperator"
			),
			array(
				$lavu_only."Theme to use for Lavu Display App",
				"config:display_theme",
				"select",
				"Default,Blue,Green,Red,Orange,Yellow"
			),

	// - - - - - - - - LoyalTree - - - - - - - - //

			array($lavu_only."LoyalTree","LoyalTree","seperator"),
			array($lavu_only.speak("Enable")." LoyalTree:","config:enable_loyaltree","bool"),
			array($lavu_only."LoyalTree App:","config:loyaltree_app","select",array("" => "","bluelabel" => "bluelabel","whitelabel"=>"whitelabel")),
			array($lavu_only."LoyalTree ID:","config:loyaltree_id","text",array("size"=>"45")),
			array($lavu_only."LoyalTree Username:","config:loyaltree_username","text",array("size"=>"45")),
			array($lavu_only."LoyalTree Password:","config:loyaltree_password","text",array("size"=>"45")),
			array($lavu_only."LoyalTree Text:","config:loyaltree_text","text",array("size"=>"45"))
		));
	}

	// - - - - - - - - Sacoa - - - - - - - - //

	$has_sacoa_cards = $modules->hasModule("extensions.payment.sacoa");

	if ($has_sacoa_cards)
	{
		$fields[] = array(
				speak("Sacoa"),
				"Sacoa",
				"seperator"
		);

		for ($i=1;$i<=20;$i++)
		{
			$conversionRate[$i]=$i;
		}

		$fields = array_merge( $fields, array(
			array(
				speak("Sacoa Conversion Rate"),
				"config:sacoa_conversion_rate",
				"select",
				$conversionRate
			),
		));

		$access_array = array();
		$access_array['1'] = "1";
		$access_array['2'] = "2";
		$access_array['3'] = "3";
		$access_array['4'] = "4";

		$fields = array_merge( $fields, array(
			array(
				speak("Sacoa Balance Transfer Access Level"),
				"config:transfer_access_level",
				"select",
				$access_array
			),
		));

		$access_array = array();
		$access_array[''] = 'None';
		$access_array['82'] = 'Lavu Integrated Payments';

		$fields = array_merge( $fields, array(
			array(
				speak("Extension to use for obtaining a Sacoa Playcard Swipe (Integrated workflow only)"),
				"config:sacoa_get_swipe_extension",
				"select",
				$access_array
			),
		));

	}

	// - - - - - - - - Loyalty - - - - - - - - //

	$fields[] = array(
		speak("Loyalty"),
		"Loyalty",
		"seperator"
	);

	if (is_lavu_admin())
	{
		$fields = array_merge( $fields, array(
			array(
				$lavu_only."Loyalty Type:",
				"config:enable_mercury_loyalty",
				"select",
				array(
					''					=> "Disabled",
					'heartland_loyalty'	=> "Heartland Loyalty",
					'mercury_loyalty'	=> "Mercury Loyalty"
				)
			),
			array(
				$lavu_only."API Key / User Name:",
				"config:mercury_loyalty_api_key",
				"text",
				"size:25"
			),
			array(
				$lavu_only."API Secret / Password:",
				"config:mercury_loyalty_api_secret",
				"text",
				"size:25"
			)
		));
	}
	else
	{
		$fields[] = array(
			speak("Loyalty Type").":",
			"config:enable_mercury_loyalty",
			"select",
			array(
				''					=> speak("Disabled"),
				'heartland_loyalty'	=> "Heartland ".speak("Loyalty"))
		);
	}

	$fields[] = array(
		speak("Automatically prompt cashier to enter customer loyalty information").":",
		"config:mercury_loyalty_nag_mode",
		"select",
		array(
			'0' => speak("At checkout start and finish"),
			'1' => speak("At checkout start"),
			'2' => speak("At checkout finish"),
			'3' => speak("Disable")
		)
	);

if (is_lavu_admin())
{
	$host_monitor_intervals = array(
		'10'	=> "10 seconds",
		'20'	=> "20 seconds",
		'30'	=> "30 seconds",
		'60'	=> "1 minute",
		'180'	=> "2 minutes",
	);

// - - - - - - - - Components - - - - - - - - //

	$fields = array_merge( $fields, array(
		array(
			$lavu_only."Components",
			"Components",
			"seperator"
		),
		array(
			"Component Package",
			"component_package_code",
			"text",
			"size:25"
		),
		array(
			"Primary Package ID:",
			"component_package",
			"text",
			"size:8"
		),
		array(
			"Secondary Package ID:",
			"component_package2",
			"text",
			"size:8"
		),
		array(
			"Tertiary Package ID:",
			"component_package3",
			"text",
			"size:8"
		),
		array(
			"ERS Net Path:",
			"config:ers_netpath",
			"text",
			"size:25"
		)));
		/*array(
			"Lavu Task Path:",
			"config:lavu_task_netpath",
			"text"
		),
		array(
			"Lavu Task API Path:",
			"config:lavu_task_api",
			"text"
		);*/
		if ( is_lavu_admin() &&
			 $primaryResId == $companyId &&
			 intval(sessvar("admin_access_level")) >= 4){

			$fields = array_merge( $fields, array(
				array(
					speak("Set Primary to Secondary Sync Tables").":",
					"set_primary_to_secondary_sync_tables",
					"redirect_button",
					array(
						'prefix'	=> "",
						'value'		=> speak("View / Edit"),
						'link'		=> "index.php?mode=settings_edit_primary_to_secondary_sync_tables&edit=1"
					)
				)
			));
		}
// - - - - - - - - Technical - - - - - - - - //
	$fields = array_merge( $fields, array(
		array(
			$lavu_only.speak("Technical"),
			"Technical",
			"seperator"
		),
		array(
			speak("Use Net Path"),
			"use_net_path",
			"select",
			array("0"=>"No","1"=>"Yes","2"=>"Yes with Tunnel")
		),
		array(
			speak("Net path URL or IP address").":",
			"net_path",
			"text",
			"size:35"
		),
		array(
			speak("Net path is LLS").":",
			"config:net_path_is_lls",
			"bool"
		),
		array(
			speak("Net path Print URL or IP address").":",
			"net_path_print",
			"text",
			"size:35"
		),
		array(
			speak("Gateway path URL or IP address").":",
			"gateway_path",
			"text",
			"size:35"
		),
		array(
			speak("Local Server Records Check Range"),
			"config:records_check_days_back",
			"text",
			"size:35"
		),
		array(
			"Beta project keys (comma separated list of alpha/beta projects to turn on):",
			"config:beta_switches",
			"text",
			"size:35"
		),
	));

	if ( is_lavu_admin() ) {
	    $fields = array_merge( $fields, array(
	        array(
	            $lavu_only.speak("Enable Lavu 4.0 CP Limited Beta Only").":",
	            "enable_cp3_beta",
	            "bool"
	        ),
	    ));
	}

// - - - - - - - - Debug Settings - - - - - - - - //
	$fields = array_merge( $fields, array(
		array($lavu_only.speak("Debug Settings"),"Debug Settings","seperator"),
		array("Enable API debugging:","config:api_debug","bool"),
		array("Enable Gateway debugging:","gateway_debug","bool"),
		array("Enable JSON Connect debugging:","config:jc_debug","bool"),
		array("Enable Lavu To Go debugging:","config:ltg_debug","bool"),
		array("Enable Sync debugging:","config:lds_debug","bool"),
		array("Enable LoyalTree debugging:","config:loyaltree_debug","bool"),
		array(speak("Save device console snapshot upon app launch").":","save_device_console_log","select",array("0"=>"No","1"=>"Console Only","2"=>"Console and Disk")),
        ($location_info['gateway'] == 'Square') ? array("Enable Square QA Mode:", "config:enable_square_qa_mode", "bool") : '',

// - - - - - - - - Connection Settings - - - - - - - - //

		array(
			$lavu_only.speak("Connection Settings"),
			"Connection Settings",
			"seperator"
		),
		array(
			"Time interval to wait between host monitor connection checks when the last attempt was successful:",
			"config:host_monitor_succeeded_interval",
			"select",
			$host_monitor_intervals,
			"setvalue:".locationSetting("host_monitor_succeeded_interval", "180")
		),
		array(
			"Time interval to wait between host monitor connection checks when the last attempt failed:",
			"config:host_monitor_failed_interval",
			"select",
			$host_monitor_intervals,
			"setvalue:".locationSetting("host_monitor_failed_interval", "60")
		),
		array(
			"Prevent error alerts related to PayPal Here SDK startup from being displayed:",
			"config:ignore_paypal_sdk_errors",
			"select",
			array(
				'0' => "Never",
				'1' => "When offline",
				'2' => "Always"
			)
		),
		array(
			"Prevent error alerts related to PayPal token requests from being displayed:",
			"config:ignore_paypal_token_errors",
			"select",
			array(
				'0' => "Never",
				'1' => "When offline",
				'2' => "Always"
			)
		),

// - - - - - - - - Sync Settings - - - - - - - - //

		array(
			$lavu_only.speak("Sync Settings"),
			"Sync Settings",
			"seperator"
		),
		array(
			"Time interval before removing open, synced quick serve orders from a device's internal storage:",
			"config:save_clear_open_quick_serve_duration",
			"select",
			array(
				''		=> "Never",
				'60'	=> "1 minute",
				'300'	=> "5 minutes",
				'600'	=> "10 minutes"
			)
		),
		array(
			"Time interval before removing closed, synced orders from a device's internal storage:",
			"config:save_clear_closed_duration",
			"select",
			array(
				'1800' => "30 minutes",
				'3600' => "1 hour",
				'7200' => "2 hours"
			)
		),
		array(
			"Bypass server-side sync conflict detection if both the app and server versions of an order were last updated by the syncing device:",
			"config:lds_conflict_detection_check_same_device",
			"bool"
		),
		array(
			"Disable LDS Data Syncer sync_tag acknowledgment routine:",
			"config:disable_sync_tag_ack",
			"bool"
		),
		array(
			"Order selection cloud load policy:",
			"config:order_selector_cloud_policy",
			"select",
			array(
				'3' => "DeviceCount",	// Only attempt sync when more than one location device is detected
				'4' => "PushQueue",		// Only attempt sync when the subject order is not in the outbound push queue
				'5' => "Online",			// Only attempt sync when the device is online
				'6' => "Always",			// Always attempt to sync an order with the cloud version
				'1' => "Never",			// Never attempt to sync an order with the cloud version when taking an order into edit mode
				//'2' => "Thresholds",
				//'0' => "None",
			),
		),
		array(
			"Disable Pre-CheckOut sync:",
			"config:disable_checkout_safety_sync",
			"bool"
		),
		array(
			"Use DataPlexSyncer for Device Check-in:",
			"config:data_plex_syncer_device_check_in",
			"bool"
		),
		array(
			"Use DataPlexSyncer for Order Syncing:",
			"config:data_plex_syncer_order_sync",
			"bool"
		),
		array(
			"Use DataPlexSyncer for Lavu To Go polling",
			"config:data_plex_syncer_lavu_togo_polling",
			"bool"
		),
		array(
			"Use DataPlexSyncer for 86 Count Reconciliation:",
			"config:data_plex_syncer_86_counts",
			"bool"
		),
		array(
			"Enable Interdevice Syncing:",
			"config:offline_comms_enabled",
			"bool"
		),

	//	array("Enable DataPlexSyncer:","config:enable_data_plex_syncer","bool"),
	//	array("Disable all socket based Inter-Device Communications (IDC):","config:disable_idc","bool"),
	//	array("Disable IDC for order ownership requests:","config:disable_idc_order_ownership","bool"),
	//	array("Disable IDC for order update broadcasts:","config:disable_idc_order_update","bool"),

// - - - - - - - - Update Reminders - - - - - - - - //

		array(
			$lavu_only.speak("Update Reminders"),
			"Update Reminders",
			"seperator"
		),
		array("Disable app update reminders for the dining room:","config:disable_update_reminders_dining_room","bool"),
		array("Disable app update reminders for the PIN screen:","config:disable_update_reminders_pin_screen","bool"),
		array("Disable app update reminders for quick serve mode:","config:disable_update_reminders_quick_serve","bool"),
		array("Disable app update reminders during breakfast hours (6am to 10am):","config:disable_update_reminders_0600_to_1000","bool"),
		array("Disable app update reminders during lunch hours (10am to 2pm):","config:disable_update_reminders_1000_to_1400","bool"),
		array("Disable app update reminders during afternoon hours (2pm to 6pm):","config:disable_update_reminders_1400_to_1800","bool"),
		array("Disable app update reminders during dinner hours (6pm to 10pm):","config:disable_update_reminders_1800_to_2200","bool"),
		array("Disable app update reminders during late night hours (10pm to 2am):","config:disable_update_reminders_2200_to_0200","bool")
	));
}

$fields = array_merge( $fields, array(
	array(
		"",
		"",
		"seperator"
	),
	array(
		speak("Save"),
		"submit",
		"submit"
	)
));

require_once(resource_path() . "/form.php");

if (is_lavu_admin())
{
	echo "<br>".$lavu_only."Indicates items visible only to Lavu Admin users (i.e., developers and support staff)";
}

if (isset($_GET['show_connector']))
{
	echo "<br><br>Location Id: ".$locationid;
	echo "<br>Connector Name: ".admin_info("dataname");
	echo "<br>Connector Key: ".lsecurity_key(admin_info("companyid"));
}
echo "<br><br>";

?>
<link href="/cp/css/select2-4.0.3.min.css" rel="stylesheet" />
<script src="/cp/scripts/select2-4.0.3.min.js"></script>
<script language="javascript">
function showPickUpNumber(){
 	var pickup = document.getElementById('config:order_number_for_customer_pick_up').value;
	 var pickupValue = function(value) {
		 return document.getElementById(value);
	 }
	 switch(pickup) {
		 case "enable" :
		 pickupValue('config:pickup_number_formatid').style.display = '';
    	 pickupValue('config:place_order_number_onid').style.display = '';
		 pickupValue('config:print_pickup_preferenceid').style.display = '';
		 pickupValue('config:print_pickup_ordertypeid').style.display = '';
		 break;
		 case "disable" :
		 pickupValue('config:pickup_number_formatid').style.display = 'none';
		 pickupValue('config:place_order_number_onid').style.display = 'none';
	     pickupValue('config:print_pickup_preferenceid').style.display = 'none';
	     pickupValue('config:print_pickup_ordertypeid').style.display = 'none';
		 break;
		 case "prompt_for_number" :
		pickupValue('config:pickup_number_formatid').style.display = 'none';
	    pickupValue('config:place_order_number_onid').style.display = '';
	    pickupValue('config:print_pickup_preferenceid').style.display = '';
	    pickupValue('config:print_pickup_ordertypeid').style.display = '';
		break;
	  }
}

document.getElementById('config:enable_secondary_currency').addEventListener('click',function(){ showOnSecondaryCurrency()},false);
function showOnSecondaryCurrency(val){
    var secondary = document.getElementById('config:enable_secondary_currency').value;
    var multiplecurr = document.getElementById('config:enable_multiple_secondary_currency').value;
    if(val == true)
    {
        document.getElementById('config:secondary_currency_codeid').style.display = '';
        document.getElementById('secondary_monitary_symbolid').style.display = '';
        document.getElementById('secondary_currency_receipt_labelid').style.display = '';
        document.getElementById('secondary_exchange_ratesid').style.display = '';
        document.getElementById('config:accessLevelToEditSecondaryExchangeRatesid').style.display = '';
    }else
    {
        document.getElementById('config:secondary_currency_codeid').style.display = 'none';
        document.getElementById('secondary_monitary_symbolid').style.display = 'none';
        document.getElementById('secondary_currency_receipt_labelid').style.display = 'none';
        if(multiplecurr == 0){
        document.getElementById('secondary_exchange_ratesid').style.display = 'none';
        }
        document.getElementById('config:accessLevelToEditSecondaryExchangeRatesid').style.display = 'none';
    }
}
document.getElementById('config:enable_multiple_secondary_currency').addEventListener('click',function(){ showOnMultipleCurrency()},false);
function showOnMultipleCurrency(val){
	var secondary = document.getElementById('config:enable_secondary_currency').value;
	if(val == true){
		document.getElementById('secondary_exchange_ratesid').style.display = '';
	} else
	{
		if(secondary == false){
		 document.getElementById('secondary_exchange_ratesid').style.display = 'none';
		}
	}
}

(function($) {
	$(document).ready(function() {
		var multiplecurr = document.getElementById('config:enable_multiple_secondary_currency').value;
		if(multiplecurr == 1){
		document.getElementById('secondary_exchange_ratesid').style.display = '';
		}
		$("select[name^=\'config:primary_currency_code\']").select2();
		$("select[name^=\'config:secondary_currency_code\']").select2();
		$("select[name^=\'config:primary_currency_code\']").on("change", function(){
			var currencyCode = $(this).val();
			$("input[name=monitary_symbol]").val("");
			$("select[name=left_or_right]").val("");
			$("select[name=decimal_char]").val("");
			if(currencyCode!=""){
				$.ajax({
					url: "index.php?widget=advanced_location_settings&get_monetary_symbol=1",
					data: { code : currencyCode},
					type: "POST",
					async: true,
					success: function(data) {
						if(data!=null){
							if(data[0].monetary_symbol!=null){
								$("input[name=monitary_symbol]").val($.trim(data[0].monetary_symbol));
								$("select[name=left_or_right]").val($.trim(data[0].symbol_placement));
								$("select[name=decimal_char]").val($.trim(data[0].decimal_separator));
								savePrimaryCurrency(currencyCode, data[0].monetary_symbol,data[0].symbol_placement,data[0].decimal_separator);
							}
						}
					},
					error: function() {
						alert("Seems problem with server loading the primary currency");
					}
				});
			}else{
				alert("Please select Primary Currency Code!");
			}
		});
		$("select[name^=\'config:secondary_currency_code\']").on("change", function(){
			var currencyCode = $(this).val();
			$("input[name=secondary_monitary_symbol]").val("");
			if(currencyCode!=""){
				$.ajax({
					url: "index.php?widget=advanced_location_settings&get_monetary_symbol=1",
					data: { code : currencyCode},
					type: "POST",
					async: true,
					success: function(data) {
						if(data!=null){
							if(data[0].monetary_symbol!=null){
								document.getElementsByName('secondary_monitary_symbol')[0].value=data[0].monetary_symbol;
								 saveSecondaryCurrency(currencyCode, data[0].monetary_symbol);
							}
						}
					},
					error: function() {
						alert("Seems problem with server loading the secondary currency");
					}
				});
			}else{
				alert("Please select Secondary Currency Code!");
			}
		});
		function saveSecondaryCurrency(currencyCode,monetary_symbol) {

			$.ajax({
				url: "areas/ajax_secondary_currency.php",
				data: { code : currencyCode,symbol :monetary_symbol },
				type: "POST",
				async: true,
				success: function(data) {
					if (data=='failure') {
 						alert("Primary currency code and Secondary currency code can't be same. Please select another currency code.");
					}
					window.location.href="index.php?mode=settings_advanced_location_settings";
				},
				error: function() {
					alert("Seems problem with server loading the secondary currency");
				}
			});
		}
		function savePrimaryCurrency(currencyCode,monetary_symbol,symbol_placement,decimal_separator) {

			$.ajax({
				url: "areas/ajax_primary_currency.php",
				data: { code : currencyCode,symbol :monetary_symbol,placement :symbol_placement,separator :decimal_separator},
				type: "POST",
				async: true,
				success: function(data) {
					if (data=='failure') {
 						alert("Primary currency code and Secondary currency code can't be same. Please select another currency code.");
					}
					window.location.href="index.php?mode=settings_advanced_location_settings";
				},
				error: function() {
					alert("Seems problem with server loading the primary currency");
				}
			});
		}
		/*
		var toggle_list0 = $("input[name^=\'get_card_info_0\']:checked")[0].nextSibling.nodeValue;
		if(toggle_list0==="None")
		{
			 $("input[name^=\'get_card_info_1\']").prop("disabled", true);
			 $("input[name^=\'get_card_info_2\']").prop("disabled", true);
			 $("input[name^=\'get_card_info_3\']").prop("disabled", true);
		}
		$("input[name^=\'get_card_info_0\']").click(enable_check);
		function enable_check() {
		  if (this.checked) {
			  $("input[name^=\'get_card_info_1\']").prop("disabled", true);
				 $("input[name^=\'get_card_info_2\']").prop("disabled", true);
				 $("input[name^=\'get_card_info_3\']").prop("disabled", true);
		  }
		  else {
			  $("input[name^=\'get_card_info_1\']").prop("disabled", false);
				 $("input[name^=\'get_card_info_2\']").prop("disabled", false);
				 $("input[name^=\'get_card_info_3\']").prop("disabled", false);

		  }
		}*/

		//alert(toggle_list);
		var decimal_char = $("select[name^=\'decimal_char\']").val();
		if(decimal_char == ""){
			$("select[name^=\'disable_decimal\']").children('option').hide();
		}
		$("select[name^=\'decimal_char\']").on("change", function(){
			var decimal_char = $(this).val();
			var value = this.value;
			if("" === decimal_char){
				$("select[name^=\'disable_decimal\']").val('0');
				$("select[name^=\'disable_decimal\']").children('option').hide();
			}else{
				$("select[name^=\'disable_decimal\']").children('option').show();
			}
		});
		$("select[name^=\'disable_decimal\']").on("change", function(){
			var disable_decimal = $(this).val();
			if("0" === disable_decimal){
				$("select[name^=\'decimal_char\']").val("None");
				$("select[name^=\'decimal_char\']").children('option').hide();
			}else{
				$("select[name^=\'decimal_char\']").children('option').show();
			}
		});

		$("input[name^=\'sacoa_conversion_rate\']").on('blur', function() {
			var sacoa_val = Number($(this).val());
			if(Number.isNaN( sacoa_val ) || sacoa_val < 1){
				$(this).val('');
				alert("Please enter value greater than 1");
			}
		});

		if($("select[name^=\'config:level_to_access_transfer_table\'] :selected").val() == 0)
		{
			$("select[name^=\'config:level_to_access_transfer_table\'] option[value='3']").prop('selected', true);
		}

		$("select[name^=\'config:fiscal_receipt_template\']").on("change", function(){
			var templateId = $(this).val();
			if (templateId > 0) {
				document.getElementById('outer_modal_span_template').style.display = 'inline';
			} else {
				document.getElementById('outer_modal_span_template').style.display = 'none';
            		}
		});
	});
})(jQuery);


</script>
<style type="text/css">
	#_setting_btn_submit_id {
      background: #98B624;
      display:table;
      height: 40px;
      width: 120px;
	  z-index: 100;
	  position: fixed;
    }
</style>
