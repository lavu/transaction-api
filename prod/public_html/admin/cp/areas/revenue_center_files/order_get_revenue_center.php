<?php

/*******************************************************************************
 * The main function that should be worried about is the order_get_revenue_center($order_id) found bellow
 *******************************************************************************
 * @author: Benjamin Bean (benjamin@poslavu.com)
 *******************************************************************************/

// returns the revenue_center name and sets the 'revenue_center_id'
// the value of 'revenue_center_id' is set to the revenue_center_id of the first returned revenue_center name with the first matching table name
// @$order_id the id of the order that was placed (eg '1-1')
// @$location_id the id of the location to look for orders from
// @$default_id the default to use if $store_nulls is TRUE
// @$quickserve_id the revenue_center_id to use for `tablename`='Quick Serve' orders
// @$tab_id the revenue_center_id to use for `tab`='1' || strlen(`tabname`)>0 orders
// @$tables should be the result from order_get_revenue_center_get_tables() or NULL
// @$override if TRUE, then the stored 'revenue_center_id' of the order will be ignored
// @$store_nulls if TRUE, then the value '$default_id' is set when a matching revenue_center can't be found ([ FALSE, "no matching revenue center when last processed" ] is returned)
// @return possible return values include:
//   [ TRUE, revenue_center_name ]
//   [ FALSE, "order id not provided" ]
//   [ FALSE, "order id not found" ]
//   [ FALSE, "stored revenue center id not found" ]
//   [ FALSE, "no matching revenue centers" ]
//   [ FALSE, "could not edit order 'revenue_center_id'" ]
//   [ FALSE, "order id not found" ]
//   [ FALSE, "databases aren't synced" ]
//   [ FALSE, "no matching revenue center when last processed" ]
function order_get_revenue_center($order_id, $location_id, $default_id, $quickserve_id, $tab_id, $tables = NULL, $override = NULL, $store_nulls = NULL) {
	$table_name = "revenue_centers";
	
	if (!$order_id)
		return array( FALSE, "order id not provided" );
	if (strlen($order_id) == 0)
		return array( FALSE, "order id not provided" );
	
	$rows = array();
	{
		$ids = lavu_query("SELECT `revenue_center_id`,`tablename`,`tab`,`tabname` FROM `[1]` WHERE `order_id` = '[2]' AND `location_id` = '[3]' LIMIT 1", "orders", $order_id, $location_id);
		if (($ids) !== FALSE)
			if (mysqli_num_rows($ids) > 0)
				while($row = mysqli_fetch_assoc($ids))
					$rows[] = $row;
	}
	if (count($rows) > 0) {
		$row = $rows[0]; // there should only be one
		
		// check that the meal_period_typeid field exists
		if (!isset($row["revenue_center_id"])) {
			return array( FALSE, "databases aren't synced" );
		}
		
		// check that the order doesn't already have a meal_period_typeid
		if ($row["revenue_center_id"] && !$override) {
			$id = $row["revenue_center_id"];
			if ($id === "0" || $id === 0) {
				return array( FALSE, "no matching revenue center when last processed" );
			}
			if (strlen($id) > 0) {
				$types = array();
				{
					$ids = lavu_query("SELECT `id`,`name` FROM `[1]` WHERE `id` = '[2]' AND `_deleted` = '[3]'", $table_name, $id, "0");
					if (($ids) !== FALSE)
						if (mysqli_num_rows($ids) > 0)
							while($row = mysqli_fetch_assoc($ids))
								$types[] = $row;
				}
				if (count($types) > 0) {
					$type = $types[0];
					if ($type["id"] == $id) {
						return array( TRUE, array($type["name"]) );
					}
				}
				return array( FALSE, "stored meal period type id not found" );
			}
		}
		
		// find the corresponding revenue center
		if ($tables === NULL)
			$tables = order_get_revenue_center_get_tables($location_id);
		$id = NULL;
		$tablename = $row['tablename'];
		$tab = $row['tab'];
		$tabname = $row['tabname'];
		$table = NULL;
		$tableid = NULL;
		foreach($tables as $t) {
			if ($t[0] == $tablename) {
				$table = $t;
				$table[1] = strval($table[1]);
				$tableid = $table[1];
			}
		}
		if (strlen($tabname) > 0 || $tab === "1") {
			$id = $tab_id;
		}
		if ($tablename == "Quick Serve" && $id === NULL) {
			$id = $quickserve_id;
		}
		if (($table === NULL || $tableid === "-1") && $id === NULL) {
			if ($store_nulls) {
				lavu_query("UPDATE `orders` SET `revenue_center_id` = '[1]' WHERE `order_id` = '[2]' AND `location_id` = '[3]' LIMIT 1", $default_id, $order_id, $location_id);
				return array( FALSE, "no matching revenue center when last processed" );
			}
			return array( FALSE, "no matching revenue centers" );
		}
		if ($id === NULL)
			$id = $table[1];
		lavu_query("UPDATE `orders` SET `revenue_center_id` = '[1]' WHERE `order_id` = '[2]' AND `location_id` = '[3]' LIMIT 1", $id, $order_id, $location_id);
		if (ConnectionHub::getConn('rest')->affectedRows() < 1) {
			return array( FALSE, "could not edit order 'revenue_center_id'" );
		}
		return array( TRUE, $id );
	}
	
	return array( FALSE, "order id not found" );
}

// returns the tables as an array [[tablename1, revenue_center_id1], [tablename2, revenue_center_id2], ...]
// returns FALSE for no tables
function order_get_revenue_center_get_tables($location_id) {
	$retval = array();
	$badval = FALSE;
	
	$tables = array();
	{
		$matcher_query = lavu_query("SELECT `names`,`revenue_centers` FROM `[1]` WHERE `loc_id` = '[2]'", "tables", $location_id);
		if (($matcher_query) !== FALSE)
			if (mysqli_num_rows($matcher_query) > 0)
				while($row = mysqli_fetch_assoc($matcher_query))
					$tables[] = $row;
	}
	
	if (count($tables) == 0) {
		return $badval;
	}
	
	foreach($tables as $table) {
		$tablenames = $table['names'];
		$rcs = $table['revenue_centers'];
		$tablenames = explode("|", $tablenames);
		$rcs = explode("|", $rcs);
		for ($i = 0; $i < count($tablenames); $i++) {
			$tablename = $tablenames[$i];
			$rc = -1;
			if (count($rcs) >= $i) {
				$rc = $rcs[$i];
			}
			$retval[] = array($tablename, $rc);
		}
	}
	
	return $retval;
}

?>
