<?php

//ini_set("display_errors", "1");

$field_name = "revenue_centers_previous_orders_processed";
$default_name = ""; // is set in set_default_revenue_center
$orders_field = "revenue_center_id";

{
	$self = $_SERVER["PHP_SELF"];
	$self = explode("/", $self);
	$self = $self[1];
	require "/home/poslavu/public_html/admin/" . $self . "/areas/revenue_center_files/order_get_revenue_center.php";
}

function save_preference($locationid, $type_of_preference, $preference_name, $preference) {
	global $field_name; global $orders_field; global $default_name;

	$query = FALSE;
	switch($preference_name) {
		case $field_name:
		case $default_name:
			lavu_query("DELETE FROM `config` WHERE `location`='[1]' AND `setting`='[2]'", $locationid, $preference_name);
			$query = lavu_query("INSERT INTO `config` (`location`, `type`, `setting`, `value`) VALUES ('[1]', '[2]', '[3]', '[4]')", $locationid, $type_of_preference, $preference_name, $preference);
			break;
	}
	if ($query === FALSE) {
		return array( FALSE, "badPreferenceName?Bad preference " . $preference_name );
	}
	if (ConnectionHub::getConn('rest')->affectedRows() == 0) {
		return array( FALSE, "badResult?Failed to save preference " .$preference_name. " (" .$preference.")" );
	}
	return array( TRUE, "success" );
}

function get_preference($locationid, $preference_name) {
	global $field_name; global $orders_field;

	$result = lavu_query("SELECT `value` FROM `config` WHERE `location` = '[1]' AND `setting` = '[2]' LIMIT 1", $locationid, $preference_name);
	if ($result)
		if ($row = mysqli_fetch_assoc($result))
			return $row['value'];
	return FALSE;
}

// gets the default values for the revenue centers from the `config` table
// returns an array in the form [['name'=namedefault,'id'=iddefault], ['name'=namequickserve,'id'=idquickserve], ['name'=nametabs,'id'=idtabs]
function get_defaults($location) {
    $defaults = array();
	$defaults_strings = array();
	$defaults_strings[] = get_preference($location, "revenue_center_default_Default");
	$defaults_strings[] = get_preference($location, "revenue_center_default_QuickServe");
	$defaults_strings[] = get_preference($location, "revenue_center_default_Tabs");
	
	$badval = array();
	$badval[] = '';
	$badval[] = '-1';
	
	foreach ($defaults_strings as $rcid) {
		$default = $rcid;
		if ($default === FALSE || strlen($default) < 1) { $default = NULL; }
		if ($default) {
			$default = lavu_query("SELECT `name`,`id` FROM `revenue_centers` WHERE `id` = '[1]'", $default);
			if ($default) {
				$defaults[] = mysqli_fetch_array($default);
			} else {
				$defaults[] = $badval;
			}
		} else {
			$defaults[] = $badval;
		}
	}
	return $defaults;
}

// @$order_id if NULL, processes all previous orders
//    This is meant primarily for testing
function process_previous_orders($location, $order_id = NULL) {
	global $field_name; global $orders_field;

	$orders_open = previous_orders_open($location);
	if ($orders_open[1] == "true") { $orders_open = TRUE; } else { $orders_open = FALSE; }
	
	if (!$orders_open) {
		return array( FALSE, "previous orders have already been processed" );
	}
		
	$open_orders_query = NULL;
	if ($order_id == NULL)
		$open_orders_query = lavu_query("SELECT `order_id` FROM `orders` WHERE `location_id` = '[1]' AND `[2]` = ''", $location, $orders_field);
	else
		$open_orders_query = lavu_query("SELECT `order_id` FROM `orders` WHERE `location_id` = '[1]' AND `order_id` = '[3]' LIMIT 1", $location, $orders_field, $order_id);
	
	if ($open_orders_query !== FALSE) {
		save_preference($location, "admin_activity", $field_name, "2");
		$tables = order_get_revenue_center_get_tables($location);
		if ($tables === FALSE)
			return array( FALSE, "there are no revenue centers to apply" );
		$defaults = get_defaults($location);
		//echo("process_previous_orders: ".$defaults[0][1].":".$defaults[1][1].":".$defaults[2][1]."\n");
		while ($row = mysqli_fetch_assoc($open_orders_query)) {
			//echo "order_get_revenue_center\n";
			$result = order_get_revenue_center($row["order_id"], $location, $defaults[0][1], $default[1][1], $defaults[2][1], $tables, TRUE, TRUE);
		}
		save_preference($location, "admin_activity", $field_name, "1");
	} else {
		return array( FALSE, "No available orders" );
	}
	
	return array( TRUE, "success" );
}

// checks if previous orders have been processed already
// checks (for) the "meal_periods_previous_orders_processed" field in `config`
// @return [ TRUE, "true" ] or [ TRUE, "false" ]
function previous_orders_open($location) {
	global $field_name; global $orders_field;
	
	$test_query = lavu_query("SHOW COLUMNS FROM `revenue_centers`");
	if ($test_query)
		$test_query = lavu_query("SELECT `revenue_center_id` FROM `orders` LIMIT 1");
	if (!$test_query)
		return array( TRUE, "false" );

	$field = get_preference($location, $field_name);
		
	if ($field) {
		$processed = $field;
		if ($processed == "0") { $processed = FALSE; } else { $processed = TRUE; }
		
		if ($processed) {
			return array( TRUE, "false" );
		} else {
			return array ( TRUE, "true" );
		}
	}
	
	$missing_typeids_query = lavu_query("SELECT `order_id` FROM `orders` WHERE `[1]` = '' AND `location_id` = '[2]' LIMIT 1", $orders_field, $location);
	$missing_typeids = array();
	if ($missing_typeids_query !== FALSE)
		while ($row = mysqli_fetch_assoc($missing_typeids_query)) {
			$missing_typeids = $row; break; }
	if (count($missing_typeids) > 0) {
		save_preference($location, "admin_activity", $field_name, "0");
	}
	
	return array ( TRUE, "true" );
}

function set_default_revenue_center($whichdefault, $value, $location) {
	global $default_name;

	switch($whichdefault) {
		case "Default":
		case "QuickServe":
		case "Tabs":
			break;
		default:
			return array ( FALSE, "bad default requested" );
	}
	
	$default_name = "revenue_center_default_".$whichdefault;
	echo ("set_default: ".$whichdefault.":".$value.":".$location."\n");
	return save_preference($location, "location_config_setting", $default_name, $value);
}

function main() {
	function get_post($var_name) {
		if (isset($_POST[$var_name]))
			return $_POST[$var_name];
		return "";
	}

	$action = get_post("action");
	$location = get_post("location");
	$orderid = get_post("order_id");
	$whichdefault = get_post("whichdefault");
	$value = get_post("value");
	$result = array( FALSE, "missingLocation?Location of site not known" );
	
	if ($location != "") {
		switch ($action)	{
		case "process_previous_orders":
			$result = process_previous_orders($location);
			break;
		case "process_previous_order":
			$result = process_previous_orders($location, $orderid);
			break;
		case "previous_orders_open":
			$result = previous_orders_open($location);
			break;
		case "set_default":
			$result = set_default_revenue_center($whichdefault, $value, $location);
			break;
		default:
			$result = array( FALSE, "badRequest?Unrecognized request from client" );
		}
	}
	
	if (gettype($result) == "array" && count($result) > 1) {
		if ($result[0] == TRUE && (strlen($result[1]) == 0)) {
			echo "success";
		} else {
			echo $result[1];
		}
	} else {
		echo "failure";
	}
}

main();

?>
