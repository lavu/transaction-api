<?php

if (!$in_lavu)
{
	exit();
}

echo "<br><br>";

echo speak("Default payment methods include Cash, Credit Cards, and Gift Certificates.")." ".speak("Additional payment methods may be defined here.")."<br><br>";

$form_posted = (isset($_POST['posted']));
$display = "";
$field = "";

$qtitle_category	= FALSE;
$qtitle_item = "Method";

$category_tablename = "";
$inventory_tablename = "payment_types";
$category_fieldname = "";
$inventory_fieldname = "id";
$inventory_select_condition	= " AND `loc_id` = '".$locationid."'";
$inventory_primefield = "type";
$category_primefield = "";
$inventory_orderby = "_order";

$special_types = array( array( speak("Standard"), "" ) );

$presorted_types = array(
	speak("Gift Card")		=> array( speak("Gift Card"), "gift_card" ),
	speak("Loyalty Card")	=> array( speak("Loyalty Card"), "loyalty_card" ),
	'RF ID'					=> array( "RF ID", "rf_id" )
);

$has_sacoa_cards = $modules->hasModule("extensions.payment.sacoa");
if($has_sacoa_cards){
	$presorted_types = array_merge($presorted_types, array(
			speak("*Sacoa Playcard")		=> array( speak("*Sacoa Playcard"), "sacoa_card" )
	));
}

addWebExtensionPaymentTypes($presorted_types, $modules);
addNativeExtensionPaymentTypes($presorted_types, $modules);

$pt_keys = array_keys($presorted_types);
natsort($pt_keys);

foreach ($pt_keys as $key)
{
	$special_types[] = $presorted_types[$key];
}

$qfields = array(
	array(
		"type",
		"type",
		speak("Payment Method"),
		"input"
	),
	array(
		"special",
		"special",
		speak("Special Type"),
		"select",
		$special_types
	),
	array(
		"no_auto_close",
		"no_auto_close",
		speak("Auto Close Order"),
		"select",
		array(
			array( speak("Auto close"), "0" ),
			array( speak("No auto close"), "1" )
		)
	),
	array(
		"get_info",
		"get_info",
		speak("Get Info"),
		"select",
		array(
			array( speak("No additional info"), "0" ),
			array( speak("Prompt for additional info"), "1" )
		)
	),
	array(
		"info_label",
		"info_label",
		speak("Info Label"),
		"input"
	),
	array(
		"info_type",
		"info_type",
		speak("Info Type"),
		"select",
		array(
			array( speak("Number"), "number" ),
			array( "Text", "text" )
		)
	),
	array(
		"min_access",
		"min_access",
		speak("Minimum access required"),
		"select",
		array(
			array( "Access Level 1", "1" ),
			array( "Access Level 2", "2" ),
			array( "Access Level 3", "3" ),
			array( "Access Level 4", "4" )
		)
	),
	array(
		"disables_rounding",
		"disables_rounding",
		"",
		"select",
		array(
			array( speak(" - "), "0" ),
			array( speak("Disables Rounding"), "1" )
		)
	),
	array(
		"hide",
		"hide",
		"",
		"select",
		array(
			array( "Show", "0" ),
			array( "Hide", "1" )
		)
	)
);

if ($location_info['order_list_cash_conversion'] == "1")
{
	$convert_option = array(
		array(
			speak("Cannot convert"),
			"0"
		),
		array( // TODO: define a way to remove this option for non-standard payment types (`special` != '')
			speak("Can convert"),
			"1"
		)
	);

	$convert_to_option = array(
		array(
			speak("Cannot convert to"),
			"0"
		),
		array( // TODO: define a way to remove this option for non-standard payment types (`special` != '')
			speak("Can convert to"),
			"1"
		)
	);

	$qfields[] = array("can_convert", "can_convert", "", "select", $convert_option);
	$qfields[] = array("can_convert_to", "can_convert_to", "", "select", $convert_to_option);
}

$qfields = array_merge( $qfields, array(
	array(
		"loc_id",
		"loc_id",
		"Location ID",
		"hidden",
		$locationid
	),
	array(
		"id",
		"id"
	),
	array( "delete" )
));

require_once(resource_path() . "/dimensional_form.php");

$qdbfields = get_qdbfields($qfields);

$cat_count = 1;

if($form_posted)
{
	$item_count = 1;
	while(isset($_POST["ic_".$cat_count."_".$inventory_primefield."_".$item_count]))
	{
		$item_value = array(); // get posted values
		for($n=0; $n<count($qfields); $n++)
		{
			$qname			= $qfields[$n][0];
			$qtitle			= (isset($qfields[$n][2]))?$qfields[$n][2]:FALSE;
			$set_item_value	= $_POST["ic_".$cat_count."_".$qname."_".$item_count];

			if ($qtitle && $set_item_value==$qtitle)
			{
				$set_item_value = "";
			}

			if ($qname=="cost") 
			{
				$set_item_value = str_replace("$","",$set_item_value);
				$set_item_value = str_replace(",","",$set_item_value);
			}

			$item_value[$qname] = $set_item_value;
		}

		$item_id = $item_value['id'];
		$item_delete = $item_value['delete'];
		$item_name = $item_value[$inventory_primefield];

		if($item_delete=="1")
		{
			delete_poslavu_dbrow($inventory_tablename, $item_id);
		}
		else if($item_name!="")
		{
			$dbfields = array();
			for($n=0; $n<count($qdbfields); $n++)
			{
				$qname = $qdbfields[$n][0];
				$qfield = (isset($qdbfields[$n][1]))?$qdbfields[$n][1]:$qname;
				$dbfields[] = array($qfield,$item_value[$qname]);
			}
			$orderby_tag = "icrow_".$cat_count."_".$item_count."_order";
			if(isset($_POST[$orderby_tag]))
			{
				$dbfields[] = array("_order",$_POST[$orderby_tag]);
			}
			update_poslavu_dbrow($inventory_tablename,$dbfields, $item_id);
		}

		$item_count++;
	}

	schedule_remote_to_local_sync($locationid,"table updated","payment_types");
}

$details_column = "";//"Zip Codes";
$send_details_field = "";//"zip_codes";

$qinfo = array(
	'category_fieldname'			=> $category_fieldname,
	'category_filter_by'			=> isset($category_filter_by)?$category_filter_by:"",
	'category_filter_value'			=> isset($category_filter_value)?$category_filter_value:"",
	'category_primefield'			=> $category_primefield,
	'category_tablename'			=> $category_tablename,
	'details_column'				=> $details_column,
	'field'							=> $field,
	'inventory_fieldname'			=> $inventory_fieldname,
	'inventory_filter_by'			=> "loc_id",
	'inventory_filter_value'		=> $locationid,
	'inventory_orderby'				=> $inventory_orderby,
	'inventory_primefield'			=> $inventory_primefield,
	'inventory_select_condition'	=> $inventory_select_condition,
	'inventory_tablename'			=> $inventory_tablename,
	'qtitle_category'				=> $qtitle_category,
	'qtitle_item'					=> $qtitle_item,
	'send_details'					=> $send_details_field
);

$form_name = "ing";
if (!isset($cfields))
{
	$cfields = NULL;
}

$field_code = create_dimensional_form($qfields,$qinfo,$cfields);

$display .= "<table cellspacing='0' cellpadding='3' class='tableSpacing'><tr><td align='center' style='border:2px solid #DDDDDD'>";
$display .= "<form name='ing' method='post' action=''>";
//$display .= "<input type='submit' value='".speak("Save")."' style='width:120px'><br>&nbsp;";
$display .= "<input type='hidden' name='posted' value='1'>";
$display .= "<br><b>".speak("Payment Methods")."</b><br><br>";
$display .= $field_code;
$display .= "<br>Additional credit / debit cards may be added here";
$buttontitle = speak("Add Card");
$buttonlink = "index.php?mode=settings_payment_methods_card";
$display .= "&nbsp;<input type='button' value='" . $buttontitle . "' onclick='window.open(\"" . $buttonlink . "\",\"_blank\")'><br />";
$display .= "&nbsp;<br><input type='submit' value='".speak("Save")."' style='width:120px'>";
$display .= "</form>";
$display .= "</td></tr></table>";

echo $display;

function addWebExtensionPaymentTypes(&$payment_types, $modules)
{
	$get_components = mlavu_query("SELECT `id`, `title`, `modules_required`, `gift`, `loyalty` FROM `poslavu_MAIN_db`.`component_layouts` WHERE `special_type` = 'payment_extension' AND `_deleted` = '0'");
	if (mysqli_num_rows($get_components) == 0)
	{
		return;
	}

	$lavu_gift_integrated_workflow = (locationSetting("lavu_gift_integrated_workflow") == "1");
	$lavu_loyalty_integrated_workflow = (locationSetting("lavu_loyalty_integrated_workflow") == "1");

	while ($info = mysqli_fetch_assoc($get_components))
	{
		if (!empty($info['modules_required']) && !$modules->hasModule($info['modules_required']))
		{
			continue;
		}

		if($info['title']=="eConduit")
			$info['title']="Lavu Integrated Payments";		
		
		$ext_title			= $info['title'];
		$layout_id			= $info['id'];
		$lavu_gift_v1		= ($layout_id == "75");
		$lavu_gift_v2		= ($layout_id == "91");
		$lavu_loyalty_v1	= ($layout_id == "77");
		$lavu_loyalty_v2	= ($layout_id == "92");
		$special_value		= "payment_extension:".$layout_id;

		if ($lavu_gift_integrated_workflow)
		{
			if ($lavu_gift_v1)
			{
				$ext_title = "*".$ext_title;
				$special_value = "lavu_gift";
			}
			else if ($lavu_gift_v2)
			{
				// Exclude alphanumeric version when using integrated workflow for Lavu Gift
				continue;
			}
		}

		if ($lavu_loyalty_integrated_workflow)
		{
			if ($lavu_loyalty_v1)
			{
				$ext_title = "*".$ext_title;
				$special_value = "lavu_loyalty";
			}
			else if ($lavu_loyalty_v2)
			{
				// Exclude alphanumeric version when using integrated workflow for Lavu Gift
				continue;
			}
		}

		$payment_types[$ext_title.$layout_id] = array( $ext_title, $special_value );

		if ($lavu_gift_v1 || $lavu_gift_v2 || $lavu_loyalty_v1 || $lavu_loyalty_v2)
		{
			// Gift and Loyalty variations should never apply to Lavu Gift and Loyalty
			continue;
		}

		if ($info['gift'] == "1")
		{
			$gift_title = $ext_title." Gift";
			$payment_types[$gift_title] = array( $gift_title, "gift_extension:".$layout_id );
		}

		if ($info['loyalty'] == "1")
		{
			$loyalty_title = $ext_title." Loyalty";
			$payment_types[$loyalty_title] = array( $loyalty_title, "loyalty_extension:".$layout_id );
		}
	}
}

function addNativeExtensionPaymentTypes(&$payment_types, $modules)
{
	$get_extensions = mlavu_query("SELECT `id`, `title`, `module`, `code_word` FROM `poslavu_MAIN_db`.`extensions` WHERE `native` = '1' AND FIND_IN_SET('1', `group_ids`) AND `_deleted` = '0'");
	if (mysqli_num_rows($get_extensions) == 0)
	{
		return;
	}

	while ($info = mysqli_fetch_assoc($get_extensions))
	{
		if (empty($info['module']) || !$modules->hasModule($info['module']))
		{
			continue;
		}
		
		if($info['title']=="Lavu Integrated Payments")
			continue;
		
		$title = $info['title'];
		$payment_types[$title.$info['id']."N"] = array( $title, "native_extension:".$info['code_word'] );
	}
}
