<?php
	function output_custom_survey($survey_name,$step="start")
	{
		global $data_name;
		$current_step_link = custom_survey_step_link($step);
		
		$title = "";
		$message = "";
		$content = "";
		$str = "";
		
		if($survey_name=="primary_email")
		{
			if($step=="start")
			{
				$survey_field_list = "survey_email";//"survey_phone,survey_email";
				
				if(isset($_POST['survey_posted']))
				{
					$write_success = process_posted_survey_form("custom_survey",$survey_name,$survey_field_list);
					
					if($write_success)
					{
						$title .= "Thank you";
						$message .= "This will help us keep you up to date ";
						$message .= "as important features are released or if there are any urgent messages.";
					}
					else
					{
						$title .= "Thank you";
						$message .= "Error: Your entry was not able to be recorded. ";
						$message .= "<br>We apologize for any inconvenience";
					}
					$message .= "<br><br><input type='button' value='Continue >>' onclick='window.location = \"index.php\"' />";
				}
				else
				{
					$title .= "Primary Contact Email";
					$message .= "<form name='custom_survey' action='$current_step_link' method='post'>";
					$message .= "<b>Please provide your primary contact email(s)</b>.  This will help to keep you  ";
					$message .= "informed of important updates and events.";
					//$message .= "<br>For multiple emails use commas.";
					$message .= "<br><br><font style='color:#999999'>Example: elvis@example.com, sting@example.com</font>";
					//$message .= "<br><br><input type='text' name='survey_phone' placeholder='Your Phone'>";
					//$message .= "<br><br><input type='text' name='survey_email' placeholder='Your Email' size='36' style='font-size:18px'>";
					$message .= "<br><br><textarea name='survey_email' placeholder='Your Primary Email(s)' style='font-size:14px' rows='3' cols='44'></textarea>";
					$message .= "<br><br><input type='button' value='Submit' onclick='validate_survey_form()' style='font-size:22px; color:#444444'>";
					$message .= "<input type='hidden' name='survey_posted' value='1'>";
					$message .= "</form>";
					$message .= create_custom_survey_js("custom_survey",$survey_field_list);
				}
			}
		}
		else if($survey_name=="vendors")
		{
			$title = "";
			$message = "";
			$content = "";
			
			if($step=="start")
			{
				$survey_field_list = "survey_details";//,survey_phone,survey_email";
				if(isset($_POST['survey_posted']))
				{
					$write_success = process_posted_survey_form("custom_survey",$survey_name,$survey_field_list);
					
					if($write_success)
					{
						$title .= "Thank you for your feedback";
						$message .= "This information will help us determine how to best improve";
						$message .= " our system.";
					}
					else
					{
						$title .= "Thank you for your feedback";
						$message .= "Error: Your entry was not able to be recorded.  Please send";
						$message .= " your feedback to support@poslavu.com<br>We apologize for any inconvenience";
					}
					$message .= "<br><br><input type='button' value='Continue >>' onclick='window.location = \"index.php\"' />";
					
					$company_name = survey_get_company_name();
					$mailstr = "";
					$mailstr .= "Source: Vendor Survey\n";
					$mailstr .= "Dataname: $data_name\n";
					$mailstr .= "Date/Time: " . date("m/d/Y h:i a") . "\n";
					$mailstr .= "Username: " . admin_info("username") . "\n";
					if(isset($_POST['survey_email'])) $mailstr .= "Contact Email: " . $_POST['survey_email'] . "\n";
					if(isset($_POST['survey_phone'])) $mailstr .= "Contact Phone: " . $_POST['survey_phone'] . "\n";
					$mailstr .= "Company Name: " . $company_name . "\n\n";
					$mailstr .= "This user indicated what food vendors they use:\n";
					$mailstr .= "Details: " . $_POST['survey_details'] . "\n";
					
					mail("corey@lavu.com,ag@lavu.com,andy@lavu.com","Vendor Info for $company_name",$mailstr,"From: survey@poslavu.com");
				}
				else
				{
					$title .= "Your Vendors";
					$message .= "<form name='custom_survey' action='$current_step_link' method='post'>";
					$message .= "<font style='font-size:14px;'>Lavu is looking into Vendor Integration, and would like to know which food vendors you are currently using.  We appreciate your feedback.</font>";
					$message .= "<br><br><textarea name='survey_details' placeholder='Your Vendors' rows='6' cols='48'></textarea>";
					//$message .= "<br><input type='text' name='survey_phone' placeholder='Your Phone'>";
					$message .= "<br><input type='text' name='survey_email' placeholder='Your Email'>";
					$message .= "<br><input type='button' value='Submit' onclick='validate_survey_form()'>";
					$message .= "<input type='hidden' name='survey_posted' value='1'>";
					$message .= "</form>";
					$message .= create_custom_survey_js("custom_survey",$survey_field_list);
				}
			}
		}
		else if($survey_name=="inventory")
		{
			$title = "";
			$message = "";
			$content = "";
			
			if($step=="start")
			{
				$survey_field_list = "survey_details";//,survey_phone,survey_email";
				if(isset($_POST['survey_posted']))
				{
					$write_success = process_posted_survey_form("custom_survey",$survey_name,$survey_field_list);
					
					if($write_success)
					{
						$title .= "Thank you for your feedback";
						$message .= "This information will help us determine how to best improve";
						$message .= " our inventory system.";
					}
					else
					{
						$title .= "Thank you for your feedback";
						$message .= "Error: Your entry was not able to be recorded.  Please send";
						$message .= " your feedback to support@poslavu.com<br>We apologize for any inconvenience";
					}
					$message .= "<br><br><input type='button' value='Continue >>' onclick='window.location = \"index.php\"' />";
					
					$company_name = survey_get_company_name();
					$mailstr = "";
					$mailstr .= "Source: Inventory Survey\n";
					$mailstr .= "Dataname: $data_name\n";
					$mailstr .= "Date/Time: " . date("m/d/Y h:i a") . "\n";
					$mailstr .= "Username: " . admin_info("username") . "\n";
					if(isset($_POST['survey_email'])) $mailstr .= "Contact Email: " . $_POST['survey_email'] . "\n";
					if(isset($_POST['survey_phone'])) $mailstr .= "Contact Phone: " . $_POST['survey_phone'] . "\n";
					$mailstr .= "Company Name: " . $company_name . "\n\n";
					$mailstr .= "This user indicated ways to improve the inventory system:\n";
					$mailstr .= "Details: " . $_POST['survey_details'] . "\n";
					
					mail("corey@lavu.com,ag@lavu.com,andy@lavu.com","Inventory System for $company_name",$mailstr,"From: survey@poslavu.com");
				}
				else
				{
					$title .= "Improving Inventory";
					$message .= "<form name='custom_survey' action='$current_step_link' method='post'>";
					$message .= "<font style='font-size:14px;'>The par level for each item, as well as amount below par are now included in Lavu inventory reports.</font><br><br><font style='font-size:12px'>We are looking for additional feedback, so if you have any other ideas on how we can improve our inventory system please let us know using the form below.</font>";
					$message .= "<br><br><textarea name='survey_details' placeholder='Your Inventory Suggestions' rows='6' cols='48'></textarea>";
					//$message .= "<br><input type='text' name='survey_phone' placeholder='Your Phone'>";
					//$message .= "<br><input type='text' name='survey_email' placeholder='Your Email'>";
					$message .= "<br><input type='button' value='Submit' onclick='validate_survey_form()'>";
					$message .= "<input type='hidden' name='survey_posted' value='1'>";
					$message .= "</form>";
					$message .= create_custom_survey_js("custom_survey",$survey_field_list);
				}
			}
		}
		else if($survey_name=="printing")
		{
			$title = "";
			$message = "";
			$content = "";
			
			if($step=="start")
			{
				$title .= "Are your Kitchen Tickets printing<br>Reliably as Intended?";
				$content .= "<td align='center'>" . custom_survey_button("Yes","#00aa00","#008800","80px","answered_yes") . "</td>";
				$content .= "<td align='center'>" . custom_survey_button("No","#aa5555","#aa2222","80px","answered_no") . "</td>";
			}
			else if($step=="answered_yes")
			{
				$vars = array();
				$vars['dataname'] = $data_name;
				$vars['datetime'] = date("Y-m-d H:i:s");
				$vars['userid'] = admin_info("admin_loggedin");
				$vars['username'] = admin_info("username");
				$vars['ipaddress'] = $_SERVER['REMOTE_ADDR'];
				$vars['type'] = $survey_name;
				$vars['company'] = survey_get_company_name();
				$vars['answer'] = "Yes";
				
				mlavu_query("insert into `poslavu_MAIN_db`.`custom_survey_results` (`dataname`,`type`,`answer`,`datetime`,`ipaddress`,`userid`,`username`) values ('[dataname]','[type]','[answer]','[datetime]','[ipaddress]','[userid]','[username]')",$vars);
				$title .= "Thank you for your feedback";
				$message .= "This information helps us to improve the product.  Thanks!";
				$message .= "<br><br><input type='button' value='Continue >>' onclick='window.location = \"index.php\"' />";
			}
			else if($step=="answered_no")
			{
				$survey_field_list = "survey_details,survey_phone,survey_email";
				if(isset($_POST['survey_posted']))
				{
					$write_success = process_posted_survey_form("custom_survey",$survey_name,$survey_field_list);
					
					if($write_success)
					{
						$title .= "Thank you for your feedback";
						$message .= "This information will help us determine how to best improve";
						$message .= " the usage and reliablity of your installation.";
					}
					else
					{
						$title .= "Thank you for your feedback";
						$message .= "Error: Your entry was not able to be recorded.  Please send";
						$message .= " your feedback to support@poslavu.com<br>We apologize for any inconvenience";
					}
					$message .= "<br><br><input type='button' value='Continue >>' onclick='window.location = \"index.php\"' />";
					
					$company_name = survey_get_company_name();
					$mailstr = "";
					$mailstr .= "Source: Kitchen Print Survey\n";
					$mailstr .= "Dataname: $data_name\n";
					$mailstr .= "Date/Time: " . date("m/d/Y h:i a") . "\n";
					$mailstr .= "Username: " . admin_info("username") . "\n";
					$mailstr .= "Contact Email: " . $_POST['survey_email'] . "\n";
					$mailstr .= "Contact Phone: " . $_POST['survey_phone'] . "\n";
					$mailstr .= "Company Name: " . $company_name . "\n\n";
					$mailstr .= "This user indicated problems with Kitchen Printing:\n";
					$mailstr .= "Details: " . $_POST['survey_details'] . "\n";
					
					mail("pos.support@lavuinc.com","Kitchen Printing for $company_name",$mailstr,"From: survey@poslavu.com");
				}
				else
				{
					$title .= "Improving Kitchen Reliability";
					$message .= "<form name='custom_survey' action='$current_step_link' method='post'>";
					$message .= "Lavu Kitchen Printing is designed to be reliable.<br>If you are experiencing problems with kitchen printing,<br>chances are that your issues can be resolved by our support staff.<br><br>Please write a brief description of your experiences with Kitchen Printing in Lavu below, as well as the best phone number and/or email to reach you so that we may assist.";
					$message .= "<br><br><textarea name='survey_details' placeholder='Issue Details' rows='8' cols='80'></textarea>";
					$message .= "<br><input type='text' name='survey_phone' placeholder='Your Phone'>";
					$message .= "<br><input type='text' name='survey_email' placeholder='Your Email'>";
					$message .= "<br><input type='button' value='Submit' onclick='validate_survey_form()'>";
					$message .= "<input type='hidden' name='survey_posted' value='1'>";
					$message .= "</form>";
					$message .= create_custom_survey_js("custom_survey",$survey_field_list);
				}
			}
		}
		if($title!="")
		{
			$str .= "<table cellpadding=8>";
			$str .= "	<tr>";
			$str .= "		<td width='420' align='center' colspan='4'>";
			if($title!="")
			{
				$str .= "			<font style='font-size:18px; color:#aaaaaa'>$title</font>";
			}
			if($message!="")
			{
				$str .= "<br><br><div style='text-align:left; color:#444455; font-size:14px'>$message</div>";
			}
			$str .= "		</td>";
			$str .= "	</tr>";
			$str .= "	<tr>";
			$str .= "		<td>&nbsp;</td>";
			$str .= "		$content";
			$str .= "		<td>&nbsp;</td>";
			$str .= "	</tr>";
			$str .= "</table>";
		}
		return $str;
	}
	
	function create_custom_survey_js($formname,$fieldlist)
	{
		$jscode = "";
		$sfields = explode(",",$fieldlist);
		for($i=0; $i<count($sfields); $i++)
		{
			$fieldname = $sfields[$i];
			$fieldtitle = ucwords(str_replace("_"," ",str_replace("survey_","",strtolower($fieldname))));
			if($jscode!="") $jscode .= "else ";
			$jscode .= "if(document.$formname.$fieldname.value=='') alert('Please provide a value for ' + (document.$formname.$fieldname.placeholder?document.$formname.$fieldname.placeholder:\"$fieldtitle\")); ";
		}
		if($jscode!="") $jscode .= "else ";
		$jscode .= "document.$formname.submit(); ";
		
		return "<script type='text/javascript'>function validate_survey_form() { $jscode } </script>\n";
	}
	
	function survey_get_company_name()
	{
		$company_name = "";
		$rest_query = mlavu_query("select * from `poslavu_MAIN_db`.`restaurants` where `id`='[1]'",admin_info("companyid"));
		if(mysqli_num_rows($rest_query))
		{
			$rest_read = mysqli_fetch_assoc($rest_query);
			$company_name = $rest_read['company_name'];
		}
		return $company_name;
	}
	
	function process_posted_survey_form($formname,$survey_type,$fieldlist)
	{
		global $data_name;

		
		$query_fields = "";
		$query_values = "";
		$sfields = explode(",",$fieldlist);
		
		$vars = array();
		$vars['dataname'] = $data_name;
		$vars['datetime'] = date("Y-m-d H:i:s");
		$vars['userid'] = admin_info("admin_loggedin");
		$vars['username'] = admin_info("username");
		$vars['ipaddress'] = $_SERVER['REMOTE_ADDR'];
		$vars['type'] = $survey_type;
		$vars['answer'] = "No";
		$vars['company'] = survey_get_company_name();
		
		foreach($vars as $vkey => $vval) $sfields[] = $vkey;
		
		$conn = ConnectionHub::getCon('poslavu');
		for($i=0; $i<count($sfields); $i++)
		{
			$fieldname = $sfields[$i];
			if($query_fields!="") $query_fields .= ",";
			if($query_values!="") $query_values .= ",";
			$query_fields .= "`" . $conn->escapeString($fieldname)."`";
			$query_values .= "'[" . $conn->escapeString($fieldname)."]'";
			if(!isset($vars[$fieldname])) $vars[$fieldname] = $_POST[$fieldname];
		}
		if($query_fields!="" && $query_values!="")
		{
			$query = "insert into `poslavu_MAIN_db`.`custom_survey_results` ($query_fields) values ($query_values)";
			return mlavu_query("insert into `poslavu_MAIN_db`.`custom_survey_results` ($query_fields) values ($query_values)",$vars);
		}
		else return false;
	}
	
	function custom_survey_step_link($step)
	{
		$page_mode = (isset($_GET['mode']))?$_GET['mode']:"";
		return "?mode=home_custom_survey&step=$step";
	}
	
	function custom_survey_button($title,$bgcolor,$mouseover_bgcolor,$size,$set_step,$textcolor="#ffffff")
	{
		$set_step_link = custom_survey_step_link($set_step);
		
		$str = "";
		$str .= "<table cellpadding=8><tr><td bgcolor='$bgcolor' onmouseover='this.bgColor = \"$mouseover_bgcolor\"' onmouseout='this.bgColor = \"$bgcolor\"' style='width:$size; height:$size; font-size:18px; color:$textcolor; border:solid 2px $mouseover_bgcolor; font-weight:bold; cursor:pointer' onclick='window.location = \"$set_step_link\"' align='center' valign='middle'>";
		$str .= $title;
		$str .= "</td></tr></table>";
		return $str;
	}
	
	//$survey_type = "inventory";//printing";//"primary_email"
	$survey_type = "vendors";
	
	$survey_step = (isset($_GET['step']))?$_GET['step']:"start";
	$survey_filled_query = mlavu_query("select * from `poslavu_MAIN_db`.`custom_survey_results` where `dataname`='[1]' and `type`='[2]'",$data_name,$survey_type);
	if(mysqli_num_rows($survey_filled_query))
	{
		echo "&nbsp;";
	}
	else
	{
		echo output_custom_survey($survey_type,$survey_step);
	}
?>
