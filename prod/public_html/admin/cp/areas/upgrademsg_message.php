<table cellpadding=6 style='border:solid 1px #888888' bgcolor='#f5fed0'><tr><td width='465' align='left'>
  <font style='color:#5e8d02'><strong>Alert: Lavu Pricing Policy is Changing for new Customers</strong>
  <br>As of 11:59 PM MDT on 13 March 2013, the Monthly Fee for all software Levels of POS Lavu will increase.</font>
  <br><br><span style='font-size:10px'>Thank you for being an early Lavu adopter.
  As a current Lavu client, <b>you are Grandfathered in at the current pricing model.</b>
  Your monthly rates will only be affected if you choose to upgrade your Lavu License Level after Wednesday, March 13
  <br><br>
  If you wish to upgrade your license to a higher level (Silver to Gold, Silver to Platinum, or Gold to Platinum):
  <table cellpadding=6><tr><td>
  <table cellpadding=0 cellspacing=0>
    <tr><td width='16'>&nbsp;</td><td style='font-size:10px' valign='top'><li></td><td style='font-size:10px' valign='top'>Upgrades after this date will be billed at the new rates.</li></td></tr>
    <tr><td width='16'>&nbsp;</td><td style='font-size:10px' valign='top'><li></td><td style='font-size:10px' valign='top'>Upgrades <strong>before</strong> this date will enjoy the benefits of the current pricing model. </li></td></tr>
  </table>
  </td></tr></table>
      <table width='100%'><tr><td width='100%' align='center'><input type='button' value='Click to Upgrade' style='font-size:12px' onclick='window.location = "https://admin.poslavu.com/cp/index.php?mode=billing"' /></td></tr></table>
    <br>Need a better understanding?<BR>
      For more information about the details of this pricing update, please download and review this document.</span>
    <br><br><a href="http://poslavu.com/assets/documents/Lavu_Pricing_Policy.pdf" target="_blank"><font style='color:#000088'>Lavu Pricing Policy</font></a>
</td></tr></table>
