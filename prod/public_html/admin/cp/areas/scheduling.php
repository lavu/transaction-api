<?php
	ini_set("display_errors","0");

	if (isset($user_schedule) == FALSE){
		if (isset($_GET["user_schedule"])){
			$user_schedule = $_GET["user_schedule"];
		}else{
			$user_schedule = "all";
		}
	}

	$week_date = date("m/d/y", strtotime("last Sunday"));
	if (isset($_GET["date"])){
		$newdate = trim($_GET["date"]);
		if (strlen($newdate) == 0) $newdate = $week_date;
		$newdate = date("m/d/y", strtotime($newdate));
		if ($newdate !== FALSE)
			$week_date = $newdate;
	}

	$shift_indicator_shapes = array();
	$shift_indicator_shapes[] = "areas/scheduling_files/png/shift_blue.png";
	$shift_indicator_shapes[] = "areas/scheduling_files/png/shift_green.png";
	$shift_indicator_shapes[] = "areas/scheduling_files/png/shift_orange.png";
	$shift_indicator_shapes[] = "areas/scheduling_files/png/shift_purple.png";
	$shift_indicator_shapes[] = "areas/scheduling_files/png/shift_red.png";
	$shift_indicator_shapes[] = "areas/scheduling_files/png/shift_yellow.png";
	$del_icon = "areas/scheduling_files/png/i_trash_dark_20x.png";
	$back_icon = "areas/scheduling_files/png/arro_back.png";
	$next_icon = "areas/scheduling_files/png/arrow_next.png";
	$button_addshift_image = "areas/scheduling_files/png/btn_addshift.png";

	//$shift_button_code = "<input type='button' value='Add Shift' onclick='add_shift_click(\"[day]\")'>";
	$shift_button_code = "<div class='addShiftBtn' onclick='add_shift_click(\"[day]\");'><span class='plus'> + </span> ".speak("Add Shift")."</div>";
	$shift_button_code_escaped_quotes = str_replace("\"","\\\"",$shift_button_code);

	$earliest_shift_time = "00:00am";
	$night_shift_start = "01:00pm";
	$night_shift_end = "11:30am";
	$longer_than_day_shifts_enabled = TRUE;
	$max_shifts = 100;
?>

<script language="javascript">
	shift_button_code = "<?php echo $shift_button_code_escaped_quotes; ?>";
	shifts = new Array();
	shifts_by_user = new Array();
	for(var n=0; n<7; n++)
		shifts[n] = new Array();
	changes_exist = false;
	shift_click_stime = 0;
	shift_click_etime = 0;
	earliest_shift_time = "<?php echo $earliest_shift_time; ?>";
	night_shift_start_time = "<?php echo $night_shift_start; ?>";
	night_shift_end_time = "<?php echo $night_shift_end; ?>";
	longer_than_day_shifts_enabled = <?php if ($longer_than_day_shifts_enabled){echo "true";}else{echo"false";} ?>;
	max_shifts = <?php echo $max_shifts; ?>;

	// from https://developer.mozilla.org/en-US/docs/JavaScript/Reference/Global_Objects/Array/indexOf
	if (!Array.prototype.indexOf) {
	    Array.prototype.indexOf = function (searchElement /*, fromIndex */ ) {
	        "use strict";
	        if (this == null) {
	            throw new TypeError();
	        }
	        var t = Object(this);
	        var len = t.length >>> 0;
	        if (len === 0) {
	            return -1;
	        }
	        var n = 0;
	        if (arguments.length > 1) {
	            n = Number(arguments[1]);
	            if (n != n) { // shortcut for verifying if it's NaN
	                n = 0;
	            } else if (n != 0 && n != Infinity && n != -Infinity) {
	                n = (n > 0 || -1) * Math.floor(Math.abs(n));
	            }
	        }
	        if (n >= len) {
	            return -1;
	        }
	        var k = n >= 0 ? n : Math.max(len - Math.abs(n), 0);
	        for (; k < len; k++) {
	            if (k in t && t[k] === searchElement) {
	                return k;
	            }
	        }
	        return -1;
	    }
	}

	// checks that all users exist for all shifts
	// start_day and start_shift should be set within the recursive calls of this function
	function sanitize_input(start_day, start_shift) {
		if (!shifts || shifts.length < 1)
			return;

		if (!start_day) start_day = 0;
		if (!start_shift) start_shift = 0;

		var userids = new Array();
		for (var daynum = start_day; daynum < 7; daynum++) {
			if (shifts[daynum] && shifts[daynum].length > 0) {
				for (var i = start_shift; i < shifts[daynum].length; i ++) {
					start_shift = 0; // sanitize for the next day
					if (shifts[daynum][i] && shifts[daynum][i].length > 0 && shifts[daynum][i][2] && shifts[daynum][i][2].length > 0) {
						// the scan across userids must be restarted if a userid is removed
						var restart_scan = true;
						while (restart_scan == true) {
							restart_scan = false;
							for (var j = 0; j < shifts[daynum][i][2].length; j++) {
								var userid = shifts[daynum][i][2][j];
								// if the user's id doesn't exist in "userids_to_usernames", then remove it completely
								if (!userids_to_usernames[userid] || userids_to_usernames[userid].length < 1 || userids_to_usernames[userid] == "") {
									shifts[daynum][i][2].splice(j, 1);
									if (shifts[daynum][i][2].length < 1) {
										shifts[daynum].splice(i, 1);
										sanitize_input(daynum, i);
										return;
									}
									restart_scan = true;
									break;
								}
							}
						}
					}
				}
			}
		}
	}
	function display_time(t) {
		var mins = t % 100;
		var hours = (t - mins) / 100;

		var set_mins = "";
		if(mins < 10) set_mins = "0" + mins; else set_mins = mins;
		if(hours % 24 ==0) return "12:" + set_mins + "am";
		else if(hours % 24 < 12) return (hours % 24) + ":" + set_mins + "am";
		else if(hours % 24 ==12) return "12:" + set_mins + "pm";
		else return ((hours % 24) - 12) + ":" + set_mins + "pm";
	}
	function convert_to_numeric_time(t) {
		var ampm = "";
		if(t.indexOf('a') > 0 || t.indexOf('A') > 0)
			ampm = "am";
		else if(t.indexOf('p') > 0 || t.indexOf('P') > 0)
			ampm = "pm";
		if(ampm=="")
		{
			return -1;
		}
		val_parts = t.split(':');
		var hours = "";
		var mins = "";
		hours = val_parts[0];
		if(val_parts.length > 1)
		{
			mins = val_parts[1];
		}
		else
		{
			return -1;
		}
		hours = hours.replace(/[^0-9]/g, '');
		mins = mins.replace(/[^0-9]/g, '');

		if(hours >= 0 && hours <= 24)
		{
			if(ampm=="pm" && hours < 12) hours = hours * 1 + 12;
			else if(ampm=="am" && hours==12) hours = 0;
			hours = hours * 100;
		}
		else return -1;
		if(mins >= 0 && mins <= 60)
		{
		}
		else return -1;
		return (hours * 1 + mins * 1);
	}
	// called to modify/add shifts from internal functions
	// @daynum 0 for sunday, 6 for saturday
	// @starttime/endtime format hh:mm[am|pm]
	// @userids a javascript array of userids
	// @shiftnum the shift number for this day
	function add_shift(daynum,start_time,end_time,userids,shiftnum) {
		var actual_start_time = convert_to_numeric_time(start_time);
		var actual_end_time = convert_to_numeric_time(end_time);
		var userids2 = userids;
		userids = new Array();
		for (var i = 0; i < userids2.length; i++)
			userids.push(String(userids2[i]));
		var earliest_shift_start_time = convert_to_numeric_time(earliest_shift_time);
		var night_shift_start = convert_to_numeric_time(night_shift_start_time);
		var night_shift_end = convert_to_numeric_time(night_shift_end_time);

		// check for midnight times
		if (actual_end_time == 0) { actual_end_time = 2400; }

		// check for longer-than-day shifts
		if (actual_start_time >= night_shift_start && actual_end_time <= night_shift_end && longer_than_day_shifts_enabled)
			actual_end_time += 24*100;

		// check that the start time comes after the allowed start time
		if (actual_start_time < earliest_shift_start_time) {
			var extra_text = "";
			if (longer_than_day_shifts_enabled && actual_start_time < night_shift_end+100)
				extra_text = " (graveyard shifts must start between "+night_shift_start_time+" and midnight)";
			alert("Shifts can't start earlier than " + earliest_shift_time + "." + extra_text);
			return false;
		}

		// check for any users (if none, delete the shift)
		if (!userids || userids.length == 0 || userids[0] == "-1" || userids[0] == -1) {
			if (shifts[daynum][shiftnum-max_shifts] && shifts[daynum][shiftnum-max_shifts].length > 0) {
				delete_shift(daynum,shiftnum);
				return true;
			} else {
				return false;
			}
		}

		// check for conflicts
		conflicts = check_for_shift_conflicts(daynum, shiftnum, actual_start_time, actual_end_time, userids);
		if (conflicts.length > 0) {
			alert("The new shift conflicts with a shift from " + display_time(conflicts[0][0]) + " to " + display_time(conflicts[0][1]) + ".");
			return false;
		}

		// save the shift
		if (actual_start_time!=-1 && actual_end_time!=-1 && actual_start_time < actual_end_time) {
			nxt = shifts[daynum].length;
			if(shiftnum && (shiftnum-max_shifts) >= 0)
			{
				track_user_shifts("remove",shifts[daynum][shiftnum-max_shifts][2],daynum,shifts[daynum][shiftnum-max_shifts][0],shifts[daynum][shiftnum-max_shifts][1]);
				shifts[daynum][shiftnum-max_shifts] = new Array(actual_start_time,actual_end_time,userids,userids_to_usernames[userids[0]],"","",userids_to_userclasses_selected(userids));
				track_user_shifts("add",userids,daynum,actual_start_time,actual_end_time);
			}
			else
			{
				shifts[daynum][nxt] = new Array(actual_start_time,actual_end_time,userids,userids_to_usernames[userids[0]],"","",userids_to_userclasses_selected(userids));
				track_user_shifts("add",userids,daynum,actual_start_time,actual_end_time);
			}
			
			sort_shift_list(daynum);
		} else {
			if (longer_than_day_shifts_enabled && actual_end_time <= night_shift_end+100) {
				alert("Graveyard shifts must start after " + night_shift_start_time + " and end before " + night_shift_end_time + ".");
			} else {
				alert("Start time must come before end time.");
			}
			return false;
		}

		return true;
	}
	function userids_to_userclasses_selected(userids) {
		userid_class_list = new Array();
		for(var u=0; u<userids.length; u++)
		{
			var check_userid = userids[u];
			var userid_class = "";
			if(element_is_defined("selected_emp_classes_" + check_userid))
			{
				userid_class = document.getElementById("selected_emp_classes_" + check_userid).value;
			}
			userid_class_list[userid_class_list.length] = userid_class;
		}
		return userid_class_list;
	}
	function track_user_shifts(type,userids,daynum,start_time,end_time) {
		var track_ushifts = true;
		
		if(track_ushifts) {
			start_time = start_time + "";
			end_time = end_time + "";
			start_time = start_time.replace(/:/ig,'');
			end_time = end_time.replace(/:/ig,'');
			
			userid_list = userids;
			for(var ui=0; ui<userid_list.length; ui++)
			{
				var userid = userid_list[ui];
				if(userid!="")
				{
					if(typeof shifts_by_user[userid] == 'undefined')
					{
						shifts_by_user[userid] = 0;
					}
					shift_start_mins = start_time % 100;
					shift_start_hours = (start_time - shift_start_mins) / 100;
					shift_end_mins = end_time % 100;
					shift_end_hours = (end_time - shift_end_mins) / 100;
					shift_diff_hours = shift_end_hours - shift_start_hours;
					shift_diff_mins = shift_end_mins - shift_start_mins;
					if(shift_diff_mins < 0)
					{
						shift_diff_hours -= 1;
						shift_diff_mins += 60;
					}
					shift_diff_number = ((shift_diff_hours * 60) + (shift_diff_mins * 1));
					if(type=="remove")
						shifts_by_user[userid] -= shift_diff_number;
					else
						shifts_by_user[userid] += shift_diff_number;

					showstr = "";
					for(userid in shifts_by_user)
					{
						if(shifts_by_user[userid] > (60 * 40))
						{
							user_total_mins = shifts_by_user[userid] % 60;
							user_total_hours = (shifts_by_user[userid] - user_total_mins) / 60;
							
							showstr += "<font style='color:#aa0000'>";
							showstr += "<li>" + userids_to_usernames[userid] + " is Scheduled for <b>" + user_total_hours + "</b> hours";
							if(user_total_mins > 0) showstr + " and " + user_total_mins + " minutes";
							showstr += "</font>";
						}
					}
					if(showstr!="") showstr ="<br><b><font style='color:#880000; font-size:16px'>Warning!</font></b><br><br>" + showstr + "<br><br>";
					document.getElementById('message_alerts').innerHTML = showstr;
					//alert(userids_to_usernames[userid] + ") " + start_time + " - " + end_time + " diff: " + shift_diff_hours + ":" + shift_diff_mins + " (total is " + shifts_by_user[userid] + ")");
					//shifts_by_user[userid] += shift_length;
				}
			}
		}
	}
	// ONLY for shifts loaded by the php
	function init_shift(daynum, start_time, end_time, userids, username, classids, coverageInfo ) {
		nxt = shifts[daynum].length;
		shifts[daynum][nxt] = new Array(start_time.replace(/\:/ig,''), end_time.replace(/\:/ig,''), userids, username, "saved", "", classids, coverageInfo);
		track_user_shifts("add",userids,daynum,start_time,end_time);
	}
	function delete_shift(daynum,shiftnum) {
		var new_shifts = new Array();
		for(var n=0; n<shifts[daynum].length; n++)
		{
			if(n != shiftnum - max_shifts)
			{
				nxt = new_shifts.length;
				new_shifts[nxt] = shifts[daynum][n];
			}
			else
			{
				if(typeof shifts[daynum][n] == 'undefined')
				{
				}
				else if(shifts[daynum][n].length > 2)
				{
					var remove_userids = shifts[daynum][n][2];
					var remove_daynum = daynum;
					var remove_start_time = shifts[daynum][n][0];
					var remove_end_time = shifts[daynum][n][1];
					track_user_shifts("remove",remove_userids,remove_daynum,remove_start_time,remove_end_time);
				}
			}
		}
		shifts[daynum] = new_shifts;
		sort_shift_list(daynum);

		changes_exist = true;
		draw_table();
	}
	function auto_format_time(type,fin) {
		var valstr = document.getElementById("new_shift_" + type + "_textbox").value;
		var ampm = "";
		if(valstr.indexOf('a') > 0 || valstr.indexOf('A') > 0)
			ampm = "am";
		else if(valstr.indexOf('p') > 0 || valstr.indexOf('P') > 0)
			ampm = "pm";
		valstr = valstr.replace(/[^0-9]/g, '');
		var newstr = valstr;
		if(valstr.length==2)
		{
			newstr = valstr.substring(0,1) + ":" + valstr.substring(1,2);
		}
		else if(valstr.length==3)
		{
			newstr = valstr.substring(0,1) + ":" + valstr.substring(1,2) + valstr.substring(2,3);
		}
		else if(valstr.length==4)
		{
			newstr = valstr.substring(0,1) + valstr.substring(1,2) + ":" + valstr.substring(2,3) + valstr.substring(3,4);
		}
		if(fin && ampm=="")
		{
			val_parts = newstr.split(':');
			hours = val_parts[0] * 1;
			if(val_parts.length > 1)
			{
				mins = val_parts[1] * 1;
			}
			else
			{
				mins = 0;
			}
			if(type=="start")
			{
				if(hours==12) ampm = "pm";
				else if(hours > 7) ampm = "am";
				else ampm = "pm";
			}
			else if(type=="end")
			{
				ampm = "pm";
			}
		}
		newstr += ampm;
		document.getElementById("new_shift_" + type + "_textbox").value = newstr;
	}
	function input_key_pressed(evt,daynum,type,shiftnum,starttime,endtime) {
		if (evt.keyCode == 13)
		{
			if(type=="end") {
				submit_new_shift_from_time_chart(daynum,shiftnum,starttime,endtime);
			} else {
				$("#new_shift_end_textbox").focus();
			}
			return false;
		}
		else
		{
			//setTimeout("auto_format_time('" + type + "',false)",100);
			return true;
		}
	}
	// returns a table for the day, where each shift is a table row
	// @usershifts an array of user shifts
	//   each user shift has the form [shift_index, shift]
	//   each shift has the form [start_time, end_time, [user_ids], username, {saved}]
	// @shared array corresponding to usershifts, where the format is [a number if the shift has more than one userid attached, or null or false otherwise]
	shift_indicator_shapes = null;
	function shift_list(daynum,usershifts,shared) {
		if (usershifts.length == 0)
			return "";

		shift_indicator_shapes = new Array();
		for (var i = 0; i < <?php echo count($shift_indicator_shapes); ?>; i++) {
			switch(i) {
			<?php
				for ($i = 0; $i < count($shift_indicator_shapes); $i++) {
					echo "case " . $i . ":\n";
					echo "shift_indicator_shapes.push('" . $shift_indicator_shapes[$i] . "'); break; \n";
				}
			?>
			}
		}

		var str = "";
		var last_had_class = false;
		for(var n=0; n<usershifts.length; n++)
		{
			var usershift = usershifts[n];
			var color = "#F00";
			var star = "*";
			if (usershift[1][4] && usershift[1][4] == "saved"){
				color = "#000";
				star = "";
			}
			if (n > 0 && !last_had_class)
				str += "<br />";

			//LP-7558 : To show coverage message.
			var style = '';
			var title = '';
	
			if(typeof usershift[1][7] != "undefined") {
			coverageData = usershift[1][7].split('_&');

				if(coverageData[0] == usershift[1][2][0] && (coverageData[1] == 'yes' || coverageData[1] == 'accepted')) {
					 style = 'text-decoration:line-through;color:#aa0000;';
					 title = "title=\'Coverage Message : "+coverageData[2];
				     if(coverageData[1] == 'accepted') {
					     title += "&#010;Coverage Accepted By : "+userids_to_usernames[coverageData[3]];
				     }
				     title += "\'";
				}
			}

			str += "<font style=\'cursor:pointer;"+style+"\' "+title+" onclick=\'shift_click(" + daynum + "," + usershift[0] + ",[" + usershift[1][2][0] + "])\'>";
			str += display_time(usershift[1][0]) + " - " + display_time(usershift[1][1]) + star;
			if (shared[n] && shared[n] != "") {
				str += "<img src='" + shift_indicator_shapes[shared[n] % shift_indicator_shapes.length] + "' alt='";
				str += "[" + shared[n] + "]";
				str += "'>";
			}
			str += "</font>";
			
			//------------------- start classid handling --------------------------//
			last_had_class = false;
			if(typeof usershift[1][6] != "undefined")
			{
				if(usershift[1][6]!="")
				{
					if(typeof emp_class_info_by_id[usershift[1][6]] != "undefined")
					{
						last_had_class = true;
						set_style = "";
						if(n<usershifts.length - 1) set_style = "border-bottom:solid 1px #dddddd";
						str += "<br><table cellspacing=0 cellpadding=0 width='100%'><tr><td align='center' width='100%' style='" + set_style + "'><font style='color:#778855; font-size:10px; font-weight:bold'>" + emp_class_info_by_id[usershift[1][6]]['title'] + "</font></td></tr></table>";
					}
				}
			}
			//------------------------- end classid handling ---------------------//
		}
		return str;
	}
	// @daynum the index of the day
	// @user_ids array of all user ids to be drawn (should probably be a list of all user ids that have shifts)
	function shift_list_by_user_ids(daynum, user_ids) {
		var str = "";
		for (var i = 0; i < user_ids.length; i++) {
			var userid = user_ids[i];
			var username = userids_to_usernames[userid];
			// check if the shift is shared
			var shared = new Array();
			// create a list of shifts to be passed to shift_list
			var usershifts = new Array();
			// go through each shift and see if it contains the userid
			var dayshifts = shifts[daynum];
			if (dayshifts != null && dayshifts.length > 0) {
				for (var k = 0; k < dayshifts.length; k++) {
					var shift = dayshifts[k];
					if (shift != null && shift.length > 0) {
						var shift_userids = shift[2];
						if (shift_userids.indexOf(userid) > -1) {
							shiftclassid = "";
							if(typeof shift[6] != "undefined") shiftclassid = shift[6][shift_userids.indexOf(userid)];
							var shiftCoverageInfo = shift[7];
							var newshift = new Array(shift[0], shift[1], new Array(userid), username, shift[4],"", shiftclassid, shiftCoverageInfo);
							var usershift = new Array(k, newshift);
							usershifts.push(usershift);
							if (!shift[5]) { shift[5] = null; }
							shared.push(shift[5]);
						}
					}
				}
			}

			// pass it on to shift_list
			str += shift_list(daynum, usershifts, shared);
		}
		return str;
	}
	function draw_table() {
		// be sure the table exists
		if (!document.getElementById("user_schedule_table_mark2"))
			return false;

		// get a list of users that have shifts
		var user_ids = new Array();
		for (var i = 0; i < shifts.length; i++) {
				var dayshifts = shifts[i];
				if (dayshifts != null && dayshifts.length > 0) {
					for (var j = 0; j < dayshifts.length; j++) {
						shift = dayshifts[j];
						if (shift != null && shift[2].length > 0) {
							for (var k = 0; k < shift[2].length; k++) {
								userid = shift[2][k];
								if (user_ids.indexOf(userid) < 0) {
									user_ids.push(userid);
								}
							}
						}
					}
				}
			}
		user_ids = sort_users(user_ids);
		// number all shifts that belong to more than one user
		var shared = 1;
		for (var i = 0; i < 7; i++) {
			for (var j = 0; j < shifts[i].length; j++) {
				shift = shifts[i][j];
				if (shift[2] && shift[2].length > 1) {
					shift[5] = shared;
					shared++;
				} else {
					shift[5] = null;
				}
			}
		}

		// if only a single user is selected, draw thay user even if they don't have any shifts
		if (<?php if ($user_schedule !== "all") { echo "true"; } else { echo "false"; } ?>) {
			if (user_ids.length == 0) {
				user_ids.push(<?php echo $user_schedule; ?>);
			}
		}

		// T A B L E
		var str = "";
		var background_style = "background-color:#f8f8f8";
		// draw the headers
		str += "<tr>";
		str += $("#weekdays_table_header").html().replace(/row[sS]pan=['"]?2['"]?/g, "rowspan='" + (user_ids.length+2) + "'");
		str += "</tr>";
		// draw the user schedules
		for (var h = 0; h < user_ids.length; h++) {
			userid = new Array();
			userid.push(user_ids[h]);
			var bg = background_style;
			if (h % 2 == 1) {
				bg = "background-color:#eee";
			}
			str += "<tr>";
			str += "<td style='text-align:center;" + bg + ";vertical-align:top;padding:3px;vertical-align:middle;'>";
			str += "<font style='visibility:visible;cursor:pointer;' onclick='change_user(\"" + <?php
				if ($user_schedule == "all")
					echo 'userids_to_usernames[userid[0]]';
				else
					echo '"All Users"';
				?> + "\");')>";
			str += userids_to_usernames[userid[0]];
			str += "</font>";
			str += "</td>";
			for (var i = 0; i < 7; i++) {
				str += "<td style='text-align:right;" + bg + ";vertical-align:top;'>";
				var str_per_userid = shift_list_by_user_ids(i, userid);
				str += str_per_userid;
				str += "</td>";
			}
			str += "</tr>";
		}
		// draw the add_shift buttons
		str += "<tr>";
		str += "<td style='" + background_style + ";'></td>";
		for (var i = 0; i < 7; i++) {
			str += "<td style='text-align:center;" + background_style + ";'>";
			str += shift_button(i);
			str += "</td>";
		}
		str += "</tr>";

		$("#user_schedule_table_mark2").html(str);
	}
	function shift_button(daynum) {
		return shift_button_code.replace(/\[day\]/ig,daynum);
	}
	var save_users_clicky = function(){};
	function shift_editor_duplicate_save_button(dontrecurse) {
		<?php if ($user_schedule != "all") echo "return;\n\n"; ?>

		$("#shift_editor_save_button2").stop(false, true);
		var html = $("#shift_editor_save_button_container").html().replace("shift_editor_save_button", "shift_editor_save_button_i");
		$("#shift_editor_save_button2").html(html);
		$("#shift_editor_save_button_i").prop("disabled", $("#shift_editor_save_button").prop("disabled"));
		$("#shift_editor_save_button_i").css("visibility", $("#shift_editor_save_button").css("visibility"));
		$("#shift_editor_save_button_i").click(save_users_clicky);
		if (!dontrecurse) {
			setTimeout('shift_editor_duplicate_save_button(true);', 250);
			setTimeout('shift_editor_duplicate_save_button(true);', 500);
		}
	}
	function shift_editor_show_save_button() {
		if ($("#shift_editor_save_button").prop("disabled") == false) {
			var already_hidden = false;
			if ($("#shift_editor_save_button:hidden").length > 0) {
				already_hidden = true;
			}
			$("#shift_editor_save_button").stop(false, true);
			$("#shift_editor_save_button").hide();
			$("#shift_editor_save_button").width("0px");
			$("#shift_editor_save_button").css("visibility", "visible");
			$("#shift_editor_save_button").width("auto");
			if (already_hidden)
				$("#shift_editor_save_button").show(250);
			else
				$("#shift_editor_save_button").show();
			shift_editor_duplicate_save_button();
			$("#shift_editor_save_button2").show(250);
		}
	}
	function shift_editor_enable_save_button() {
		$("#shift_editor_save_button").prop("disabled", false);
		shift_editor_duplicate_save_button();
	}
	function shift_editor_disable_save_button() {
		$("#shift_editor_save_button").prop("disabled", true);
		shift_editor_duplicate_save_button();
	}
	function shift_editor_hide_time() {
		$("#shift_editor_time_container").stop(false, true);
		$("#shift_editor_time_container").hide();
		$("#shift_editor_time_container").css("display", "none");
	}
	function shift_editor_show_time(instant) {
		var already_hidden = false;
		if ($("#shift_editor_time_container").css("display") == "auto" || $("#shift_editor_time_container:hidden").length > 0) {
			already_hidden = true;
		}
		$("#shift_editor_time_container").stop(false, true);
		$("#shift_editor_time_container").css("display", "auto");
		if (already_hidden && !instant)
			$("#shift_editor_time_container").show(250);
		else
			$("#shift_editor_time_container").show();
		/*var already_hidden = false;
		if ($("#shift_editor_time_container:hidden").length > 0) {
			already_hidden = true;
		}
		$("#shift_editor_time_container").stop(false, true);
		$("#shift_editor_time_container").hide();
		$("#shift_editor_time_container").width("0px");
		$("#shift_editor_time_container").css("visibility", "visible");
		$("#shift_editor_time_container").width("auto");
		if (already_hidden)
			$("#shift_editor_time_container").show(250);
		else
			$("#shift_editor_time_container").show();*/
	}
	function shift_editor_hide_checkboxes() {
		$("#shift_editor_user_checkboxes").stop(false, true);
		$("#shift_editor_user_checkboxes").hide(250);
	}
	function element_is_defined(elem) {
		if(typeof(document.getElementById(elem))=="undefined" || document.getElementById(elem)==null) return false; else return true;
	}
	function shift_editor_update_checkboxes() {
		var length = userids_to_usernames.length;
		for (var i = 0; i < length; i++)
			if (userids_to_usernames[i] && userids_to_usernames[i] != "")
			{
				$("#shift_editor_user_checkbox_" + i).prop("checked", false);
			}
		var userid = get_selected_user_ids("chart")[0];
		$("#shift_editor_user_checkbox_" + userid).prop("checked", true);
		shift_editor_show_checkboxes();
	}
	function shift_editor_show_checkboxes() {
		var already_hidden = false;
		if ($("#shift_editor_user_checkboxes").css("display") == "none" || $("#shift_editor_user_checkboxes:hidden").length > 0) {
			already_hidden = true;
		}
		$("#shift_editor_user_checkboxes").stop(false, true);
		$("#shift_editor_user_checkboxes").hide();
		$("#shift_editor_user_checkboxes").css("dsiplay", "auto");
		if (already_hidden)
			$("#shift_editor_user_checkboxes").show(250);
		else
			$("#shift_editor_user_checkboxes").show();
	}
	function shift_textboxes_show() {
		$("#shift_textboxes").stop(false, true);
		$("#shift_textboxes").show(250);
	}
	function shift_textboxes_hide() {
		$("#shift_textboxes").stop(false, true);
		$("#shift_textboxes").hide(250);
	}
	// @stime/etime hhmm in 24-hour format
	shift_textboxes_update_daynum = null;
	shift_textboxes_update_shiftnum = null;
	function shift_textboxes_update(daynum, shiftnum, stime, etime) {
		if (stime) { $("#new_shift_start_textbox").val(display_time(stime)); }
		if (etime) { $("#new_shift_end_textbox").val(display_time(etime)); }
		if (daynum && 0 <= daynum && 6 >= daynum) {
			shift_textboxes_update_daynum = daynum;
			shift_textboxes_update_shiftnum = shiftnum;
			$("#new_shift_end_textbox").unbind();
			$("#new_shift_end_textbox").keypress(function (event){
					return input_key_pressed(event, shift_textboxes_update_daynum, 'end', shift_textboxes_update_shiftnum, $("#new_shift_start_textbox").val(), $("#new_shift_end_textbox").val());
				});
			$("#new_shift_start_textbox").unbind();
			$("#new_shift_start_textbox").keypress(function (event){
					return input_key_pressed(event, shift_textboxes_update_daynum, 'start', shift_textboxes_update_shiftnum, $("#new_shift_start_textbox").val(), $("#new_shift_end_textbox").val());
				});
		}
		if ($("#advanced_time_checkbox").prop("checked")) {
			shift_textboxes_show();
		} else {
			shift_textboxes_hide();
		}
	}
	function sort_shift_list(daynum, sort_by) {
		// expects two shifts as input
		function compare_by_start_time(a, b)
		{
			var astime = a[0];
			var bstime = b[0];
			return ((astime < bstime) ? -1 : ((astime > bstime) ? 1 : 0));
		}
		function compare_by_end_time(a, b)
		{
			var aetime = a[0];
			var betime = b[0];
			return ((aetime < betime) ? -1 : ((aetime > betime) ? 1 : 0));
		}
		function compare_by_username(a, b)
		{
			var auname = a[2][0];
			var buname = b[2][0];
			return ((auname < buname) ? -1 : ((auname > buname) ? 1 : 0));
		}
		for (var day in shifts)
		{
			if (shifts[day].length > 1)
			{
				shifts[day].sort(compare_by_username);
				shifts[day].sort(compare_by_end_time);
				shifts[day].sort(compare_by_start_time);
			}
		}
	}
	// sorts an array of users according to username
	// expects format user[id]
	blarg = null;
	function sort_users(array_of_userids) {
		if (!array_of_userids || array_of_userids.length < 2)
			return array_of_userids;
		blarg = array_of_userids;
		var users = array_of_userids;
		// expects two shifts as input
		function compare_by_username(a, b)
		{
			var auname = userids_to_usernames[a].toLowerCase();
			var buname = userids_to_usernames[b].toLowerCase();
			return ((auname < buname) ? -1 : ((auname > buname) ? 1 : 0));
		}
		users.sort(compare_by_username);
		return users;
	}
	// @stime/etime expects format
	// return a list of conflicts if there are any, in the format shift[stime, etime, [userids], {saved}]
	function check_for_shift_conflicts(daynum, new_shiftnum, stime, etime, userids){
		var dayshifts = shifts[daynum];
		var st1 = stime;
		var et1 = etime;
		var conflicts = new Array();

		if (dayshifts.length > 0){
			for (var shiftnum = 0; shiftnum < dayshifts.length; shiftnum++){
				if (new_shiftnum && new_shiftnum-max_shifts >= 0 && shiftnum == new_shiftnum-max_shifts)
					continue;
				shift=dayshifts[shiftnum];
				for (var uidindex = 0; uidindex < userids.length; uidindex++) {
					var userid = userids[uidindex];
					var shift_userids = shift[2];
					if (shift_userids.indexOf(userid) >= 0){
						var st2 = shift[0];
						var et2 = shift[1];
						if (st1 == st2 && et1 == et2){
							//alert("There is a duplicate shift, already, for "+shift[3]);
							conflicts.push(shift);
						}else if (st1 >= st2 && st1 < et2){
							//alert("The shift conflicts with another shift from "+display_time(st2)+" to "+display_time(et2)+" for "+shift[3]);
							conflicts.push(shift);
						}else if (st2 >= st1 && st2 < et1){
							//alert("The shift conflicts with another shift from "+display_time(st2)+" to "+display_time(et2)+" for "+shift[3]);
							conflicts.push(shift);
						}
						break;
					}
				}
			}
		}
		return conflicts;
	}
	function save_shifts() {
		var data = "";
		for(var n=0; n<shifts.length; n++)
		{
			if(shifts[n].length > 0)
			{
				data += "<day><day_number>" + n + "</day_number>";
				for(var x=0; x<shifts[n].length; x++)
				{
					for(var uidindex=0; uidindex < shifts[n][x][2].length; uidindex++)
					{
						if(shifts[n][x].length > 1)
						{
							var timetext = "<time>" + shifts[n][x][0] + "-" + shifts[n][x][1] + "</time>";
							var uidtext = "<userid>" + shifts[n][x][2][uidindex] + "</userid>";
							var extratext = "";
							if(shifts[n][x].length > 6 && shifts[n][x][6].length > uidindex)
							{
								if(shifts[n][x][6][uidindex] != "")
									extratext += "<classid>" + shifts[n][x][6][uidindex] + "</classid>";
							}
							
							data += "<shift>" + timetext + uidtext + extratext + "</shift>";
						}
					}
				}
				data += "</day>";
			}
		}
		document.schedule_form.data.value = data;
		document.schedule_form.schedule_special_notes.value = document.getElementById('schedule_special_notes_textarea').value;
		document.schedule_form.submit();
	}
	// redirects the page to user_schedule=usernames_to_userids[username]
	// @username the username to change to, or empty to select the name in id='user_schedule'
	function change_user(username) {
		if (!username || username == "") {
			var username = $("#user_selectbox").val();
		}

		if (username && username != "" && (username == "All Users" || usernames_to_userids[username])) {
			// check if it's ok to change
			var do_save_and_change_user = false;
			if (changes_exist) {
				if (confirm("Changes exist. Save changes and continue?")) {
					do_save_and_change_user = true;
				}
			} else {
				do_save_and_change_user = true;
			}
			// get the userid
			var userid = "";
			if (username == "All Users") {
				userid = "all";
			} else {
				userid = usernames_to_userids[username];
			}
			// change
			if (do_save_and_change_user) {
				$("#user_schedule").val(userid);
				$("#reload_form").submit();
			}
		}
	}
	function update_advanced_time_checkbox_setpreference() {
		set_preferences("shift_editor_advanced_time_enabled");
	}
	update_advanced_time_checkbox_daynum = null;
	update_advanced_time_checkbox_shiftnum = null;
	function update_advanced_time_checkbox(daynum, shiftnum) {
		update_advanced_time_checkbox_daynum = daynum;
		update_advanced_time_checkbox_shiftnum = shiftnum;
		$("#advanced_time_checkbox").unbind();
		$("#advanced_time_checkbox").click(function() {
			daynum = update_advanced_time_checkbox_daynum;
			shiftnum = update_advanced_time_checkbox_shiftnum;
			shift_textboxes_update(daynum, shiftnum, null, null, true);
			setTimeout('update_advanced_time_checkbox_setpreference()', 400);
		});
	}
	// when the user wants to add a new shift
	function add_shift_click(daynum) {
		var str = "";
		close_shift_editor();
		shift_editor_disable_save_button();

		var one_user = <?php if ($user_schedule != "all"){echo "true";}else{echo "false";} ?>;
		if (!one_user) {
			shift_editor_hide_time();
		}

		update_advanced_time_checkbox(daynum);

		open_shift_editor(daynum);
		//$.scrollTo($("#shift_editor_container"), 500);
	}
	// when the user wants to modify an existing shift
	function shift_click(daynum,shift_number,userids) {
		var str = "";
		var sn = shift_number;
		close_shift_editor();
		shift_editor_enable_save_button();

		shift_click_stime = shifts[daynum][sn][0];
		shift_click_etime = shifts[daynum][sn][1];
		shift_editor_show_time(true);
		update_advanced_time_checkbox(daynum, shift_number);

		open_shift_editor(daynum,sn + max_shifts,userids_to_usernames[userids[0]]);
	}
	//----------------------------- For Time Overlay -------------------------------//
	function getObjPosition(obj)
	{
		var curwidth = obj.offsetWidth;
		var curheight = obj.offsetHeight;
		var curleft = obj.offsetLeft;
		var curtop = obj.offsetTop;
		while(obj.offsetParent)
		{
		  obj = obj.offsetParent;
		  curleft += obj.offsetLeft;
		  curtop += obj.offsetTop;
		}
		var retval = new Array();
		retval[0] = curleft;
		retval[1] = curtop;
		retval[2] = curwidth;
		retval[3] = curheight;
		retval['left'] = curleft;
		retval['top'] = curtop;
		retval['width'] = curwidth;
		retval['height'] = curheight;
		return retval;
	}

	function getElementPosition(element_id)
	{
		var obj = document.getElementById(element_id);
		return getObjPosition(obj);
	}

	function add_time(bt,at) {
		var mins_bt = bt % 100;
		var hours_bt = (bt - mins_bt) / 100;
		var mins_at = at % 100;
		var hours_at = (at - mins_at) / 100;

		hours_bt += hours_at;
		mins_bt += mins_at;
		while(mins_bt > 59)
		{
			mins_bt -= 60;
			hours_bt ++;
		}

		return (hours_bt * 100) + mins_bt;
	}
	function tc_choose_start_time(daynum,shiftnum,set_stime) {
		if(shiftnum && shiftnum=="") shiftnum = false;
		document.getElementById("shift_editor_chart_container").innerHTML = build_time_chart(daynum,shiftnum,set_stime);
	}
	// returns the selected user id as a single-element array depending on whether "chart" or "type" is selected as which_form
	// if which_form = "checkboxes" then it checks all of checkboxes for id='shift_editor_user_checkbox_' and returns an array
	// if the user_selector can't be found or is hidden, then returns [-1]
	function get_selected_user_ids(which_form)
	{
		<?php
		if ($user_schedule != "all") {
			echo "var single_user_array = new Array();\n";
			echo "single_user_array.push(". $user_schedule .");\n";
			echo "return single_user_array;\n";
		}
		?>
		var badval = new Array(); badval.push(-1);
		if (document.getElementById("shift_editor_user_select")){
			var username = "";
			if (which_form == "chart"){
				if ($("#shift_editor_user_select:hidden").length > 0)
					return badval;
				username = $("#shift_editor_user_select").val();
			} else if (which_form == "checkboxes") {
				if ($("#shift_editor_user_select:hidden").length > 0 || $("#shift_editor_user_checkboxes:hidden").length > 0)
					return badval;
				var length = userids_to_usernames.length;
				var retval = new Array();
				for (var i = 0; i < length; i++)
					if (userids_to_usernames[i] && userids_to_usernames[i] != "")
						if ($("#shift_editor_user_checkbox_" + i).prop("checked"))
							retval.push(i);
				var selected_id = parseInt(get_selected_user_ids("chart")[0]);
				if (retval.length == 0)
					return badval;
				return retval;
			}
			if (username == " ")
				return badval;
			var userid = usernames_to_userids[username];
			var retval = new Array();
			retval.push(userid);
			return retval;
		} else {
			return badval;
		}
	}
	// @start_time/end_time in display_time() format
	function submit_new_shift_from_time_chart(daynum,shiftnum,start_time,end_time) {
		var str = "";
		//var change_on_an_individual_basis = true;
		//var change_individual_shift = false;
		if(shiftnum && shiftnum=="") shiftnum = false;
		var new_userids = get_selected_user_ids("checkboxes");
		if (!new_userids || new_userids.length == 0 || !new_userids[0] || new_userids[0] == "-1" || new_userids[0] == -1) {
			new_userids = get_selected_user_ids("chart");
		}
		if (!new_userids || new_userids.length == 0 || !new_userids[0] || new_userids[0] == "-1" || new_userids[0] == -1) {
			alert("Please choose a user for this shift");
			return;
		}
		var userids = new Array();
		if (new_userids.length > 0) {
			userids = new Array();
			var length = new_userids.length;
			for (var i = 0; i < new_userids.length; i++) {
				userids.push	(new_userids[i]);
			}
		}
		var success = false;
		if (userids)	{
			success = add_shift(daynum,start_time,end_time,userids,shiftnum);
		}

		if(success) {
			changes_exist = true;
			draw_table();
			close_shift_editor();
		}
	}
	function build_time_chart(daynum,shiftnum,set_stime) {
		var str = "";
		var stime = convert_to_numeric_time(earliest_shift_time);
		var etime = convert_to_numeric_time(night_shift_end_time);
		var slotsize = 30;

		if (longer_than_day_shifts_enabled)
			etime += 2400;

		var t = stime;
		var slotcount = 0;

		var h1 = -1;
		var h2 = -1;
		if(shiftnum && shiftnum >= max_shifts)
		{
			h1 = parseInt(shifts[daynum][shiftnum - max_shifts][0]);
			h2 = parseInt(shifts[daynum][shiftnum - max_shifts][1]);

			if (longer_than_day_shifts_enabled && h2 <= convert_to_numeric_time(night_shift_end_time) && h1 >= convert_to_numeric_time(night_shift_start_time))
				h2 += 2400;

			var stimemin = (((stime - (stime % 100)) / 100) * 60) + (stime % 100);
			var etimemin = (((etime - (etime % 100)) / 100) * 60) + (etime % 100);
			var slotstart = stimemin % slotsize;
			var h1min = (((h1 - (h1 % 100)) / 100) * 60) + (h1 % 100);
			var h2min = (((h2 - (h2 % 100)) / 100) * 60) + (h2 % 100);
			h1min = h1min - ((h1min - slotstart) % slotsize);
			if (((h2min - slotstart) % slotsize) != 0)
				h2min = h2min + (slotsize - ((h2min - slotstart) % slotsize));
			h1min = Math.max(h1min, stimemin);
			h2min = Math.min(h2min, etimemin);

			h1 = Math.floor(h1min / 60) * 100 + (h1min % 60);
			h2 = Math.floor(h2min / 60) * 100 + (h2min % 60);
		}
		str += "<table cellspacing=0 cellpadding=2>";
		while(t <= etime && slotcount < 1000)
		{
			slotcount++;

			use_bgcolor = "#ddddee";
			use_bgcolor_over = "#bbbbcc";
			use_border_bottom = "#aabbee";
			use_text_color = "#555577";
			right_cell_text = "&nbsp;";
			click_code = "tc_choose_start_time(\"" + daynum + "\",\"" + shiftnum + "\",\"" + t + "\")";

			if (t == 2400) {
				use_bgcolor = "#eeeeee";
				use_bgcolor_over = "#eeeeee";
				use_border_bottom = "#dddddd";
				use_text_color = "#aaaaaa";
				right_cell_text = "(choose a start time)";
				click_code = "alert(\"First choose a start time.\")";
			}
			if (t > 2400) {
				use_bgcolor = "#eeeeee";
				use_bgcolor_over = "#eeeeee";
				use_border_bottom = "#dddddd";
				use_text_color = "#aaaaaa";
				right_cell_text = "(start after " + night_shift_start_time + ")";
				click_code = "alert(\"Graveyard shifts must start after " + night_shift_start_time + ".\")";
			}

			if(set_stime && set_stime!="")
			{
				if(set_stime > t)
				{
					use_bgcolor = "#eeeeee";
					use_bgcolor_over = "#eeeeee";
					use_border_bottom = "#dddddd";
					use_text_color = "#aaaaaa";
					right_cell_text = "&nbsp;";
					click_code = "tc_choose_start_time(\"" + daynum + "\",\"" + shiftnum + "\",\"\")";
				}
				else if(set_stime==t)
				{
					use_bgcolor = "#555588";
					use_bgcolor_over = "#555588";
					use_border_bottom = "#555588";
					use_text_color = "#ffffff";
					right_cell_text = "(click to change start time)";
					click_code = "tc_choose_start_time(\"" + daynum + "\",\"" + shiftnum + "\",\"\")";
				}
				else if(set_stime < t && (set_stime >= convert_to_numeric_time(night_shift_start_time) || t <= 2400))
				{
					use_bgcolor = "#ddddee";
					use_bgcolor_over = "#bbbbcc";
					use_border_bottom = "#aabbee";
					use_text_color = "#555577";
					right_cell_text = "&nbsp;";
					if (shifts[daynum] && shifts[daynum][shiftnum-max_shifts] && shifts[daynum][shiftnum-max_shifts][2]) {
						click_code = "submit_new_shift_from_time_chart(\"" + daynum + "\",\"" + shiftnum + "\",\"" + display_time(set_stime) + "\",\"" + display_time(t) + "\",["+shifts[daynum][shiftnum-max_shifts][2]+"])";
					} else {
						click_code = "submit_new_shift_from_time_chart(\"" + daynum + "\",\"" + shiftnum + "\",\"" + display_time(set_stime) + "\",\"" + display_time(t) + "\",[])";
					}
				}
			}

			if(h1==t || h2==t)
				extra_style = "font-weight:bold; background-color:#bbbbdd;";
			//else if(h1 < t && h2 > t)
			//	extra_style = "font-weight:bold; background-color:#ddf;";
			else
				extra_style = "";
			str += "<tr bgcolor='" + use_bgcolor + "' onmouseover='this.bgColor = \"" + use_bgcolor_over + "\"' onmouseout='this.bgColor = \"" + use_bgcolor + "\"' onclick='" + click_code + "'><td align='right' style='color:" + use_text_color + "; font-family:Arial; font-size:11px; border-bottom:solid 1px " + use_border_bottom + "; cursor:pointer; " + extra_style + "'>";
			str += display_time(t);
			str += "</td><td width='200' style='border-bottom:solid 1px " + use_border_bottom + "; cursor:pointer; color:" + use_text_color + "; font-family:Arial; font-size:11px' align='center'>";
			str += right_cell_text;
			str += "</td></tr>";

			t = add_time(t,slotsize);
		}
		str += "</table>";
		return str;
	}
	function open_shift_editor(daynum,shiftnum,username) {
		var epos = getElementPosition("day_" + daynum);
		var se = document.getElementById("shift_editor_container");
		var set_left = epos['left'] - (epos['width'] / 2);
		var set_top = epos['top'] - (epos['height'] / 2);

		set_left -= 80;
		if ($.browser.msie)
			set_left -= 500;
		set_top -= 240;

		if(set_left < 10) set_left = 10;
		if(set_top < 10) set_top = 10;
		if(set_left > 630) set_left = 630;

		document.getElementById("shift_editor_dayname").innerHTML = get_dayname(daynum);
		$("#shift_editor_user_select").val(username);
		document.getElementById("shift_editor_chart_container").innerHTML = build_time_chart(daynum,shiftnum);
		// shift_editor_del_button
		if (shiftnum && shiftnum >= 0 && shiftnum){
			$("#shift_editor_del_button").show();
			$("#shift_editor_del_button").unbind();
			$("#shift_editor_del_button").click(function(){delete_shift(''+daynum+'',''+shiftnum+'');close_shift_editor();});
		}else{
			$("#shift_editor_del_button").hide();
		}
		// shift_editor_save_button
		$("#shift_editor_save_button").unbind();
		save_users_clicky = function(){
			var userids = get_selected_user_ids("checkboxes");
			if (userids[0] == -1 && $("#shift_editor_save_button:hidden").length > 0)
				userids = get_selected_user_ids("chart");
			var stime = display_time(shift_click_stime);
			var etime = display_time(shift_click_etime);
			if (add_shift(daynum, stime, etime, userids, shiftnum))
				close_shift_editor();
		};
		$("#shift_editor_save_button").click(save_users_clicky);
		shift_editor_duplicate_save_button();
		// shift_editor_user_checkboxes
		var length = userids_to_usernames.length;
		for (var i = 0; i < length; i++) {
			if (userids_to_usernames[i] && userids_to_usernames[i] != "") {
				$("#shift_editor_user_checkbox_" + i).prop("checked", false);
					if(element_is_defined("emp_classes_" + i))
						document.getElementById("emp_classes_" + i).style.display = "none";
			}
		}
		if (daynum >= 0 && daynum <= 6 && shiftnum && (shiftnum-max_shifts) >= 0 && shifts[daynum] && shifts[daynum][shiftnum-max_shifts] && shifts[daynum][shiftnum-max_shifts][2] && shifts[daynum][shiftnum-max_shifts][2].length > 0) {
			for (var i = 0; i < shifts[daynum][shiftnum-max_shifts][2].length; i++) {
				$("#shift_editor_user_checkbox_" + shifts[daynum][shiftnum-max_shifts][2][i]).prop("checked", true);
				
				emp_check_id = shifts[daynum][shiftnum-max_shifts][2][i];
				if(element_is_defined("emp_classes_" + emp_check_id))
				{
					emp_check_classid = shifts[daynum][shiftnum-max_shifts][6][i];
					document.getElementById("emp_classes_" + emp_check_id).style.display = "block";
					select_role_id_for_employee(emp_check_id,emp_check_classid,class_choices_by_userid[emp_check_id]);
					document.getElementById("selected_emp_classes_" + emp_check_id).value = emp_check_classid;
				}
			}
		}
		if (shifts[daynum] && shifts[daynum][shiftnum-max_shifts] && shifts[daynum][shiftnum-max_shifts][2] && shifts[daynum][shiftnum-max_shifts][2].length > 1) {
			shift_editor_show_checkboxes();
		}
		// new_shift_start_textbox
		if ($("#advanced_time_checkbox").prop("checked") == true) {
			sn = shiftnum-max_shifts;
			if (shifts[daynum] && shifts[daynum][sn] && shifts[daynum][sn][0] && shifts[daynum][sn][1])
				shift_textboxes_update(daynum, shiftnum, shifts[daynum][sn][0], shifts[daynum][sn][1], true);
			else
				shift_textboxes_update(daynum, shiftnum, null, null, true);
		}

		se.style.left = set_left;
		se.style.top = set_top;
		se.style.visibility = "visible";
		$("#shift_editor_container").stop(false, true);
		$("#shift_editor_container").show(300);
	}
	function close_shift_editor() {
		$("#shift_editor_container").stop(false, true);
		$("#shift_editor_container").hide(300);
		$("#shift_editor_save_button").stop(false, true);
		$("#shift_editor_save_button").hide(300, function() {
			shift_editor_show_checkboxes();
		});
		shift_editor_duplicate_save_button();
		$("#shift_editor_save_button2").hide();
		draw_table();
	}
	function get_dayname(dn) {
		if(dn==0) return "Sunday";
		else if(dn==1) return "Monday";
		else if(dn==2) return "Tuesday";
		else if(dn==3) return "Wednesday";
		else if(dn==4) return "Thursday";
		else if(dn==5) return "Friday";
		else if(dn==6) return "Saturday";
		else return "";
	}
	function send_import_shifts(date) {
		var rand = Math.ceil(Math.random()*2);
		$.ajax({
			url: "index.php?widget=scheduling_files/php/request_action",
			type: "POST",
			async: false,
			cache: false,
			timout: 10000,
			data: { action: "import", location: <?php echo $locationid; ?>, from_date: date, to_date: <?php echo '"'.$week_date.'"'; ?>, random_num: rand },

			success: function (string) {
				$('#import_shifts_import_button').removeAttr('disabled');
				if( string.indexOf('success') !== -1 ){
					location.reload();
					return true;
				}else{
					var error_type = string.split("?")[0];
					var error_message = string.split("?")[1];
					alert("Error (" + error_type + "): " + error_message);
				}
			},

			error: function(j ,t, e){
				$('#import_shifts_import_button').removeAttr('disabled');
				if (t === "timeout"){
					alert("Error (timeout): no response from server");
				}else{
					alert("Error (" + t + ")");
				}
			}
		});
		return false;
	}
	function import_shifts_pressed() {
		if ($("#delete_current_week_shifts_checkbox").prop("checked") == true) {
			duplicate_week();
		} else {
			import_shifts();
		}
	}
	function import_shifts() {
		var date = $("#import_shifts_select_week").val();
		date = date.split("-")[0];
		date = date.trim();
		$('#import_shifts_import_button').attr('disabled', 'disabled');
		setTimeout("$('#import_shifts_import_button').removeAttr('disabled');", 10000);
		send_import_shifts(date);
	}
	function send_duplicate_week(date) {
		var rand = Math.ceil(Math.random()*2);
		$.ajax({
			url: "index.php?widget=scheduling_files/php/request_action",
			type: "POST",
			async: false,
			cache: false,
			timout: 10000,
			data: { action: "duplicate", location: <?php echo $locationid; ?>, from_date: date, to_date: <?php echo '"'.$week_date.'"'; ?>, random_num: rand },

			success: function (string) {
				$('#duplicate_week_duplicate_button').removeAttr('disabled');
				if( string.indexOf('success') !== -1 ){
					location.reload();
					return true;
				}else{
					var error_type = string.split("?")[0];
					var error_message = string.split("?")[1];
					alert("Error (" + error_type + "): " + error_message);
				}
			},

			error: function(j ,t, e){
				$('#duplicate_week_duplicate_button').removeAttr('disabled');
				if (t === "timeout"){
					alert("Error (timeout): no response from server");
				}else{
					alert("Error (" + t + ")");
				}
			}
		});
		return false;
	}
	function duplicate_week() {
		var date = $("#import_shifts_select_week").val();
		date = date.split("-")[0];
		date = date.trim();
		$('#duplicate_week_duplicate_button').attr('disabled', 'disabled');
		setTimeout("$('#duplicate_week_duplicate_button').removeAttr('disabled');", 10000);
		send_duplicate_week(date);
	}
	send_save_preference_retval = null;
	function send_save_preference(in_preference_name, in_preference) {
		var rand = Math.ceil(Math.random()*2);
		send_save_preference_retval = false;
		$.ajax({
			url: "index.php?widget=scheduling_files/php/request_action",
			type: "POST",
			async: false,
			cache: false,
			timout: 10000,
			data: { action: "save_preference", location: <?php echo $locationid; ?>, preference_name: in_preference_name, preference: in_preference, random_num: rand },

			success: function (string) {
				if( string.indexOf('success') !== -1 ){
					send_save_preference_retval = true;
				}else{
					var error_type = string.split("?")[0];
					var error_message = string.split("?")[1];
					alert("Error (" + error_type + "): " + error_message);
					send_save_preference_retval = false;
				}
			},

			error: function(j ,t, e){
				send_save_preference_retval = false;
			}
		});
		return send_save_preference_retval;
	}
	function set_preferences(in_preference) {
		var checkboxid = "";
		var successboxid = "";
		var preference_name = "";
		var val = "";

		switch(in_preference) {
		case "delete_destination_week":
			checkboxid = "delete_current_week_shifts_checkbox";
			successboxid = "delete_current_week_shifts_success";
			preference_name = "shift_editor_delete_destination_week";
			break;
		case "shift_editor_advanced_time_enabled":
			checkboxid = "advanced_time_checkbox";
			successboxid = "advanced_time_checkbox_success";
			preference_name = "shift_editor_advanced_time_enabled";
			break;
		}

		if ($("#" + checkboxid)) {
			val = $("#" + checkboxid).prop("checked");
			if (val) {
				val = "1";
			} else {
				val = "0";
			}
		}

		if (val) {
			if (send_save_preference(preference_name, val) == true)
				$("#" + successboxid).html("preference saved");
			else
				$("#" + successboxid).html("preference not saved");
		}
	}
</script>

<?php
	// ~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>
	// define globaly used variables here (for reference)
	// ~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>
	$users = NULL; // array of users for this database
	$loaded_shifts = NULL; // array of all shifts, in the form $shifts[day_of_week, [shift_index, [start_time, end_time, "usern"=>user_name]]]
	$init_js = NULL;
	$save_errors = NULL; // array of errors encounted when trying to save new shifts, in the form [error_index, [start_time, end_time, "dow"=>day_of_week, "usern"=>user_name, "error_name"=>error_name, "error_description"=>description]]

	// ~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>
	// get an array of all the users
	// ~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>
	$users = array();
	$deleted_users = array();
	$user_name = "All Users";
	{
		// deleted users and bad userids
		$user_query_del = lavu_query("SELECT * FROM `users` WHERE `_deleted` = '1' ORDER BY `f_name` ASC");
		if (mysqli_num_rows($user_query_del) > 0)
		{
			while($user_read = mysqli_fetch_assoc($user_query_del))
			{
				$deleted_users[] = array();
				end($deleted_users);
				$key = key($deleted_users);
				$usern = trim($user_read['f_name'] . " " . $user_read['l_name']);
				if($usern=="") $usern = $user_read['username'];
				$user_id = $user_read["id"];
				$deleted_users[$key]["name"] = $usern;
				$deleted_users[$key]["id"] = $user_id;
			}
		}
		// existing users
		$user_query = lavu_query("SELECT * FROM `users` WHERE `_deleted` != '1' ORDER BY `f_name` ASC");
		if (mysqli_num_rows($user_query) > 0)
		{
			while($user_read = mysqli_fetch_assoc($user_query))
			{
				$users[] = array();
				end($users);
				$key = key($users);
				$usern = trim($user_read['f_name'] . " " . $user_read['l_name']);
				if($usern=="") $usern = $user_read['username'];
				$user_id = $user_read["id"];
				
				$role_id_list = array();
				$role_id_parts = explode(",",$user_read["role_id"]);
				for($r=0; $r<count($role_id_parts); $r++)
				{
					$inner_role_parts = explode("|",$role_id_parts[$r]);
					if(count($inner_role_parts) > 1)
					{
						$inner_role_id = trim($inner_role_parts[0]);
						$inner_role_rate = trim($inner_role_parts[1]);
						
						if($inner_role_id!="")
						{
							$role_id_list[] = array("id"=>$inner_role_id,"rate"=>$inner_role_rate);
						}
					}
				}
				
				if (!$usern || $usern == "" || strlen($usern) == 0) {
					$deleted_users[] = array();
					end($deleted_users);
					$key = key($deleted_users);
					$deleted_users[$key]["name"] = $usern;
					$deleted_users[$key]["id"] = $user_id;
					$deleted_users[$key]["role_id_list"] = $role_id_list;
				} else {
					$users[$key]["name"] = $usern;
					$users[$key]["id"] = $user_id;
					$users[$key]["role_id_list"] = $role_id_list;
				}
			}
		}
	}
	if ($user_schedule == "all") {
		$user_name = "All Users";
	} else {
		foreach($users as $user) {
			if ($user['id'] = $user_schedule) {
				$user_name = get_safe_username($user['name'], 'html');
			}
		}
	}

	// ~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>
	// sanitize the user_schedules table
	// ~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>
	// set the date to the current sunday for all user schedules that don't have a date
	lavu_query("UPDATE `user_schedules` SET `date` = '[1]' WHERE `date` = ''", date("m/d/y", strtotime("last Sunday")));




	//Inserted by Brian D.
	// AD-HOC function for transcribing the week format (Sunday date + day of week offset)---------------------------------
	// to exact date so that we may index the shifts.
	function calculateExactDateFromWeekFormat($sundayDate, $dayOfWeekOffset){

    	//Split the dateTime into parts.
    	$sundayBaseValueParts = explode('/', $sundayDate);
    	$sundayBase_dayVal   = $sundayBaseValueParts[1];
    	$sundayBase_monthVal = $sundayBaseValueParts[0];
    	$sundayBase_yearVal  = $sundayBaseValueParts[2];

    	//Calculate the exact date.
    	$sundaysSQLDateFormat = '20' . $sundayBase_yearVal . "-" . $sundayBase_monthVal . "-" . $sundayBase_dayVal;
    	$finalEpochTime = strtotime( date("Y-m-d", strtotime($sundaysSQLDateFormat)) . " +$dayOfWeekOffset day");
    	$finalDate = date('Y-m-d', $finalEpochTime);

    	return $finalDate;
	}
	//Converts
	//  Written to account for day light savings as well.
	function calculateExactDateTimeFromDateAndHour($Ymd_Date, $endTime_MilitaryFormatOverflowing){
	    $endTimeParts = explode(':', $endTime_MilitaryFormatOverflowing);
	    $endTimeHr = $endTimeParts[0];
	    $endTimeMins = $endTimeParts[1];

	    $returning_Ymd_Date  = $endTimeHr < 24 ? $Ymd_Date : date('Y-m-d', strtotime($Ymd_Date . ' +1 day'));
	    $newHour = $endTimeHr % 24;
	    $newHour = strlen($newHour) == 1 ? '0' . $newHour : $newHour;
	    $returning_Ymd_Date_Time = "$returning_Ymd_Date $newHour:$endTimeMins";
	    return $returning_Ymd_Date_Time;
	}
	function insertShiftRowIntoDB($locationid, $uid, $day_number, $start_time, $end_time, $week_date, $extra_shift_values=false){

    	//Exact start time
    	$shiftExactDate = calculateExactDateFromWeekFormat($week_date, $day_number);
    	$shiftExactStart = $shiftExactDate . ' ' . $start_time . ':00';//We add seconds to keep in SQL date format.

        //Exact end time
        $shiftExactEndTime = calculateExactDateTimeFromDateAndHour($shiftExactDate, $end_time);
        $shiftExactEndTime .= ':00'; //We add seconds to keep in SQL date format.
    	//Gotta build the query array.
    	$shiftQueryArray = array();
    	$shiftQueryArray['loc_id'] = $locationid;
    	$shiftQueryArray['user_id'] = $uid;
    	$shiftQueryArray['dayofweek'] = $day_number;
    	$shiftQueryArray['start_time'] = $start_time;
    	$shiftQueryArray['end_time'] = $end_time;
    	$shiftQueryArray['date'] = $week_date;
    	$shiftQueryArray['start_datetime'] = $shiftExactStart;
    	$shiftQueryArray['end_datetime'] = $shiftExactEndTime;
		
		$set_data1 = "";
		$set_data2 = "";
		$set_data_long = "";
		if($extra_shift_values && is_array($extra_shift_values))
		{
			foreach($extra_shift_values as $esk => $esv)
			{
				if($esk=="classid") $set_data1 = $esv;
				else
				{
					if($set_data_long!="") $set_data_long .= "&";
					$set_data_long .= urlencode($esk) . "=" . urlencode($esv);
				}
			}
		}
		$shiftQueryArray['data1'] = $set_data1;
		$shiftQueryArray['data2'] = $set_data2;
		$shiftQueryArray['data_long'] = $set_data_long;

    	$shiftins_queryStr  = "INSERT INTO `user_schedules` ";
    	$shiftins_queryStr .= "(`loc_id`,`user_id`,`dayofweek`,`start_time`,`end_time`,`date`,`start_datetime`,`end_datetime`,`data1`,`data2`,`data_long`) ";
    	$shiftins_queryStr .= "VALUES('[loc_id]','[user_id]','[dayofweek]','[start_time]','[end_time]','[date]','[start_datetime]','[end_datetime]','[data1]','[data2]','[data_long]') ";

    	return lavu_query($shiftins_queryStr, $shiftQueryArray);
	}
	//End_Added_By_Brian D.----------------------------------------------------------------------------------------------------------------------------





	// ~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>
	// get an array of all the shifts
	// ~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>
	// sorts an array of lists by an index name
	function sort_by_field($objs, $index) {
		$func = "return (\$o1[" . $index . "] < \$o2[" . $index . "]) ? -1 : 1;";
		usort($objs,
			  create_function("\$o1,\$o2", $func));

		return $objs;
	}
	function print_array_as_javascript_array($array,$type="numeric") {
		$str = "";
		if($type=="assoc") $str .= "{";
		else $str .= "[";
		$first = TRUE;
		foreach($array as $key => $value) {
			if ($first)
				$first = FALSE;
			else
				$str .= ", ";
			if($type=="assoc")
				$str .= "\"$key\" : \"$value\"";
			else
				$str .= "\"" . $value . "\"";
		}
		if($type=="assoc") $str .= "}";
		else $str .= "]";
		return $str;
	}

	//Included by Brian D. as a helper function
	function takeSundayDateAndReturnLowerAndUpperBoundForIndexedQuery($mdyFormattedDate){
    	$mdyFormattedDateParts = explode('/', $mdyFormattedDate);
    	$day   = $mdyFormattedDateParts[1];
    	$month = $mdyFormattedDateParts[0];
    	$year  = '20' . $mdyFormattedDateParts[2];
    	$fullSundayLeftBorder = $year . '-' . $month . '-';

    	$aWeekLaterRightBorder = date('Y-m-d 99:99:99', strtotime($year.'-'.$month.'-'.$day.' +1 week'));
    	return array($fullSundayLeftBorder, $aWeekLaterRightBorder);
	}
	//End Brian D. Insertion of function.-----------------

	function load_shifts($locationid, $user_schedule, $week_date, $users, $deleted_users, $init_js) {
		$loaded_shifts = array();
		$display_shifts = array();
		$init_shifts = array();
		$init_js = "";
		for($i=0; $i<7; $i++) {
			$loaded_shifts[$i] = array();
			$init_shifts[$i] = array();
		}
		if($user_schedule=="all") {
		//Edited by Brian D. ------------------------------------------------------------
		//We have added an index'd field of regular SQL-Date format
		//Basically, we are going to use the original query and now include an 'IN' statement
		// which will use the index.
		///Original Query:
			//$shift_query = lavu_query("select * from `user_schedules` where `loc_id`='[1]' and `date`='[2]' and `dayofweek`!='' order by `dayofweek` asc",$locationid,$week_date);
			$db_bounds = takeSundayDateAndReturnLowerAndUpperBoundForIndexedQuery($week_date);
			$shift_query_str  = "SELECT * FROM `user_schedules` WHERE `loc_id`='[1]' AND `date`='[2]' AND `dayofweek` != '' " . $no_coverage_complete;
			$shift_query_str .= "AND `id` IN ";
			$shift_query_str .= "( SELECT `id` FROM `user_schedules` WHERE `start_datetime` >= '[3]' AND `end_datetime` <= '[4]' ) ORDER BY `dayofweek` ASC";
			$shift_query = lavu_query($shift_query_str, $locationid, $week_date, $db_bounds[0], $db_bounds[1]);
        //End Editing by Brian D. -------------------------------------------------------------------------------
		} else {
		//Edited by Brian D. ------------------------------------------------------------
        //We have added an index'd field of regular SQL-Date format
		//Basically, we are going to use the original query and now include an 'IN' statement
		// which will use the index.
		///Original Query:
			//$shift_query = lavu_query("select * from `user_schedules` where `loc_id`='[1]' and `date`='[2]' and `user_id`='[3]' and `dayofweek`!='' order by `dayofweek` asc",$locationid,$week_date,$user_schedule);
			$db_bounds = takeSundayDateAndReturnLowerAndUpperBoundForIndexedQuery($week_date);
			$shift_query_str  = "SELECT * FROM `user_schedules` WHERE `loc_id`='[1]' AND `date`='[2]' AND `user_id`='[3]' AND `dayofweek`!='' " . $no_coverage_complete;
			$shift_query_str .= "AND `id` IN ";
			$shift_query_str .= "( SELECT `id` FROM `user_schedules` WHERE `start_datetime` >= '[4]' AND `end_datetime` <= '[5]' ) ORDER BY `dayofweek` ASC";
			$shift_query = lavu_query($shift_query_str, $locationid, $week_date, $user_schedule, $db_bounds[0], $db_bounds[1]);
        //End Editing by Brian D. -------------------------------------------------------------------------------
		}

		while($shift_read = mysqli_fetch_assoc($shift_query))
		{
			$daynum = $shift_read['dayofweek'];
			$start_time = $shift_read['start_time'];
			$end_time = $shift_read['end_time'];
			$user_id = $shift_read['user_id'];
			$classid = $shift_read['data1'];
			//LP-7558: The schedule should not hide all other employees from the All User options of the schedule.
            $dataLongArr = explode('&',$shift_read['data_long']);
            $coverage = array();
            foreach($dataLongArr as $valueStr) {
                $valueArr = explode('=', $valueStr);
                $coverage[$valueArr[0]] = $valueArr[1];
            }
            $coverageInfo = ($coverage['coverage_requested'] != '') ? $user_id."_&".$coverage['coverage_requested'] : '_&';
            $coverageInfo.= ($coverage['coverage_message'] != '') ? "_&".$coverage['coverage_message'] : '_&';
            $coverageInfo.= ($coverage['coverage_accepted_by'] != '') ? "_&".$coverage['coverage_accepted_by'] : '';

			$usern = "";
			foreach($users as $user)
				if ($user["id"] == $user_id){ $usern = get_safe_username($user["name"], 'javascript' );}
			// check that the username exists
			if (strlen($usern) == 0) continue;
			// check for deleted users
			if (count($deleted_users) > 0) {
				$bad_user = FALSE;
				foreach($deleted_users as $deleted_user)
					if ($deleted_user["id"] == $user_id) {
						$bad_user = TRUE; break;
					}
				if ($bad_user == TRUE)
					continue;
			}
			
			$user_id_info = array("userid"=>$user_id,"classid"=>$classid);
			
			if (isset($init_shifts[$daynum][$start_time][$end_time]))
			{
				$init_shifts[$daynum][$start_time][$end_time][0][] = $user_id;
				$init_shifts[$daynum][$start_time][$end_time][2][] = $classid;
				$init_shifts[$daynum][$start_time][$end_time][3][] = $coverageInfo;
			}
			else
				$init_shifts[$daynum][$start_time][$end_time] = array(array($user_id), $usern, array($classid), array($coverageInfo));
			$loaded_shifts[$daynum][] = array($start_time,$end_time,"usern"=>$usern,"userid"=>$user_id,"classid"=>$classid,"saved"=>1);
		}
		$init_shift_array_index = 0;
		foreach($init_shifts as $daynum=>$dayshift) {
			foreach($dayshift as $start_time=>$startshift) {
				foreach($startshift as $end_time=>$endshift) {
					$coverageInfo = $endshift[3][0];
					// $endshift[0] is an array of user_ids, $endshift[1] is the default user_name
					$init_js .= "\nvar init_array_" . $init_shift_array_index . " = " . print_array_as_javascript_array($endshift[0]) . ";";
					$init_js .= "\nvar init_array_class_" . $init_shift_array_index . " = " . print_array_as_javascript_array($endshift[2]) . ";";
					$init_js .= "\ninit_shift($daynum,'$start_time','$end_time',init_array_" . $init_shift_array_index . ",'".$endshift[1]."',init_array_class_" . $init_shift_array_index . ", '$coverageInfo'); ";
					$init_shift_array_index++;
					$display_shifts[$daynum][] = array($start_time,$end_time,"usern"=>$endshift[1],"userids"=>$endshift[0],"saved"=>1);
				}
			}
		}
		foreach($loaded_shifts as $day => $dayshifts)
		{
			$loaded_shifts[$day] = sort_by_field($dayshifts, "usern");
			$loaded_shifts[$day] = sort_by_field($dayshifts, "1");
			$loaded_shifts[$day] = sort_by_field($dayshifts, "0");
		}

		return array("init_js" => $init_js, "loaded_shifts" => $loaded_shifts, "display_shifts" => $display_shifts);
	}

	$results = load_shifts($locationid, $user_schedule, $week_date, $users, $deleted_users, NULL);
	$init_js = $results["init_js"];
	$loaded_shifts = $results["loaded_shifts"];

	// ~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>
	// save all shifts (if shifts are passed in)
	// ~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>
	$save_errors = array();
	if(isset($_POST['data']))
	{

		// checks for conflicts between a potential shift and all other shifts for that user on that day
		// @dow day of week (as an integer, where Sunday is 0)
		// @st start time
		// @et end time
		// @return array with conflicting shift [st, et, conflict_warning], or FALSE
		function new_shift_conflicts_for_users($locationid, $user_schedule, $loaded_shifts, $user_name, $dow, $st_in, $et_in)
		{
				if (count($loaded_shifts[$dow]) == 0)
				{
					return FALSE;
				}

				$retval = FALSE;
				$st = strtotime($st_in);
				$et = strtotime($et_in);
				$day_shifts = $loaded_shifts[$dow];
				foreach($day_shifts as $daynum => $dayshift)
				{
					if (count($dayshift) > 0)
					{
						if ($dayshift["usern"] == $user_name)
						{
							$st2 = strtotime($dayshift[0]);
							$et2 = strtotime($dayshift[1]);
							$st2_std = date("g:ia", $st2);
							$et2_std = date("g:ia", $et2);

							if ($st == $st2 && $et == $et2){
								$retval = array($st2_std, $et2_std, "same_shift", $dow, $daynum); break; }
							if ($st >= $st2 && $st < $et2){
								$retval = array($st2_std, $et2_std, "conflict"); break; }
							if ($st2 >= $st && $st2 < $et){
								$retval = array($st2_std, $et2_std, "conflict"); break; }
						}
					}
				}

				return $retval;
		}

		// get the value wrapped in an xml tag
		function xml_get_value($tag, $str)
		{
				$right = explode("<$tag>", $str);
				if (count($right) > 1)
				{
					$center = explode("</$tag>", $right[1]);
					if (count($center) > 1)
					{
						return $center[0];
					}
				}

				return "";
		}

		// counts the number of duplicates
		// only records the first
		$duplicates = array();
		// if a shift is changed and the change conflicts, then the shift isn't saved
		// this records conflicting shifts and the conflict message in the form [shift[$locationid,$uid,$day_number,$start_time,$end_time,$week_date], conflict[$locationid,$uid,$day_number,$start_time,$end_time,$week_date]]
		$conflicting_shifts = array();
		$get_by_post_shifts = array();

		for ($i = 0; $i < 7; $i++)
			$duplicates[] = array();

		// get the ids of user shifts from the start of this month to the end of this week
		$db_bounds = takeSundayDateAndReturnLowerAndUpperBoundForIndexedQuery($week_date);
		$ids_query = ConnectionHub::getConn('rest')->getDBAPI()->getAllInTable('user_schedules', array('start_datetime'=>$db_bounds[0], 'end_datetime'=>$db_bounds[1]), FALSE,
			array('whereclause'=>"WHERE `start_datetime` >= '[start_datetime]' AND `end_datetime` <= '[end_datetime]'", 'selectclause'=>'`id`'));
		$ids = array();
		foreach($ids_query as $a_id)
			$ids[] = $a_id['id'];
		$s_in_clause = ConnectionHub::getConn('rest')->getDBAPI()->arrayToInClause($ids, TRUE);

		// delete user schedules for (this/all) users from the current week
		// use $db_bounds to take advantage of indexed columns in user_schedules
		$shift_query_vars = array('locationid'=>$locationid, 'date'=>$week_date, 'user_id'=>$user_schedule, 'start_datetime'=>$db_bounds[0], 'end_datetime'=>$db_bounds[1]);
		$shift_query_str  = " delete from `user_schedules` where `loc_id`='[locationid]' and `date`='[date]'";
		if ($user_schedule != "all") {
			$shift_query_str .= " and `user_id`='[user_id]'";
		}
		if (!empty($ids)) {
			$shift_query_str .= " AND `id` ".$s_in_clause;
		}
		$del_query = lavu_query($shift_query_str,$shift_query_vars);
		
		if(isset($_REQUEST['schedule_special_notes']))
		{
			lavu_query("delete from `user_schedules` where `date`='[1]'","notes_".$week_date);
			lavu_query("insert into `user_schedules` (`date`,`data_long`) values ('[1]','[2]')","notes_".$week_date,$_REQUEST['schedule_special_notes']);
		}

		$day_data = explode("<day>",$_POST['data']);
		for($d=1; $d<count($day_data); $d++) {
			$parts = explode("</day_number>",$day_data[$d]);
			if(count($parts) > 1) {
				$num_parts = explode("<day_number>",$parts[0]);
				if(count($num_parts) > 1) {
					$day_number = trim($num_parts[1]);

					$shift_parts = explode("<shift>",$parts[1]);
					foreach($shift_parts as $shift_left) {
						$shift_center = explode("</shift>", $shift_left);
						if (count($shift_center) > 1) {
							$shift = $shift_center[0];
							$time = xml_get_value("time",$shift);
							$time = explode("-", $time);
							$uid = xml_get_value("userid",$shift);
							
							$classid = xml_get_value("classid",$shift);
							
							$extra_shift_values = array();
							if($classid!="") $extra_shift_values['classid'] = $classid;

							if ($uid != "" && count($time) == 2) {
								$start_time = $time[0];
								$end_time = $time[1];

								// get the user id (if $user_schedule != "all")
								if ($user_schedule != "all" && $uid == -1)
									$uid = $user_schedule;

								$start_mins = $start_time % 100;
								$start_hours = ($start_time - $start_mins) / 100;
								if($start_hours < 10) $start_hours = "0" . ($start_hours * 1);
								if($start_mins < 10) $start_mins = "0" . ($start_mins * 1);
								$end_mins = $end_time % 100;
								$end_hours = ($end_time - $end_mins) / 100;
								if($end_hours < 10) $end_hours = "0" . ($end_hours * 1);
								if($end_mins < 10) $end_mins = "0" . ($end_mins * 1);

								$start_time = $start_hours . ":" . $start_mins;
								$end_time = $end_hours . ":" . $end_mins;

								// get the username and day name
								// used in checking for conflicts
								$usern = "";
								$day_name = date('l', strtotime("Sunday + $day_number Days"));
								foreach($users as $user)
									if ($user["id"] == $uid){$usern = $user["name"];}

								// save the shift
								if ($usern != "") {
									$get_by_post_shifts[] = array($locationid, $uid, $day_number, "st"=>$start_time, "et"=>$end_time, "date"=>$week_date);
									if (($conflict = new_shift_conflicts_for_users($locationid, $user_schedule, $loaded_shifts, $usern, $day_number, $start_time, $end_time)) === FALSE || $conflict[2] !== "conflict") {
										if ($conflict[2] !== "same_shift") {

										//Edited by Brian D. ----------------------------------------------------------------------------------------------------------------------
										//Commenting out current insert and making new insert that includes the exact dateTime of the shift.
											////$shiftins_query = lavu_query("insert into `user_schedules` (`loc_id`,`user_id`,`dayofweek`,`start_time`,`end_time`,`date`) values ('[1]','[2]','[3]','[4]','[5]','[6]')",$locationid,$uid,$day_number,$start_time,$end_time,$week_date);
										$shiftins_query = insertShiftRowIntoDB($locationid, $uid, $day_number, $start_time, $end_time, $week_date, $extra_shift_values);
								        //End editing by Brian D. ------------------------------------------------------------------------------------------------------------------




											$loaded_shifts[$day_number][] = array($start_time,$end_time,$usern);
										} else {
											if (isset($duplicates[$day_number][$uid][$start_time][$end_time])) {
												$duplicates[$day_number][$uid][$start_time][$end_time]++;
												$save_errors[] = array($start_time, $end_time, "usern"=>$usern, "dow"=>$day_number, "error_name"=>"duplicate_shift", "error_description"=>"User $usern already has a shift on $day_name from $conflict[0] to $conflict[1].");
											} else {
												$duplicates[$day_number][$uid][$start_time][$end_time]=1;
											}
											if ($duplicates[$day_number][$uid][$start_time][$end_time]<=1) {

											//Edited by Brian D.------------------------------------------------------------------------------------------------------------------
											//Commenting out current insert and making new insert that includes the exact dateTime of the shift.
												//$shiftins_query = lavu_query("insert into `user_schedules` (`loc_id`,`user_id`,`dayofweek`,`start_time`,`end_time`,`date`) values ('[1]','[2]','[3]','[4]','[5]','[6]')",$locationid,$uid,$day_number,$start_time,$end_time,$week_date);
												$shiftins_query = insertShiftRowIntoDB($locationid, $uid, $day_number, $start_time, $end_time, $week_date, $extra_shift_values);
								            //End editing by Brian D.------------------------------------------------------------------------------------------------------------------
											}
										}
									} else {
										$conflicting_shifts[] = array( array($locationid,$uid,$day_number,"st"=>$start_time,"et"=>$end_time,"date"=>$week_date,"classid"=>$classid), array($locationid,$uid,$day_number,"st"=>$conflict[0],"et"=>$conflict[1],"date"=>$week_date));
									}
								} else {
									$save_errors[] = array($start_time, $end_time, "usern"=>"", "dow"=>$day_number, "error_name"=>"unknown_userid", "error_description"=>"Unknown user id $uid. Shift not saved on $day_name from $conflict[0] to $conflict[1].");
								}
							} //if ($uid != "" && count($time) == 2) {
						} //if (count($shift_center) > 1) {
					} //foreach($shift_parts as $shift_left) {
				} //if(count($num_parts) > 1) {
			} //if(count($parts) > 1) {
		} //for($d=1; $d<count($day_data); $d++) {

		// check that the conflicting shifts weren't just changed shifts
		//   they wouldn't conflict if the supposed conflicting shift doesn't exist
		// create them if they were just changes (conflicting_shifts[1] doesn't exist in new_shifts)
		// create an error message if there was actually a conflict (conflicting_shifts[1] does exist in new_shifts)
		foreach($conflicting_shifts as $conshift_whole) {
			$start_time = NULL;
			$end_time = NULL;
			$uid = NULL;
			$usern = NULL;
			$day_number = NULL;
			$locationid = NULL;
			$day_name = NULL;

			$conshift = $conshift_whole[1];
			$conshift["st"] = date("H:i", strtotime($conshift["st"]));
			$conshift["et"] = date("H:i", strtotime($conshift["et"]));
			$exists = FALSE;
			// check that the conshift !exist in the newshifts
			foreach($get_by_post_shifts as $newshift) {
				$difference = FALSE;
				// checks that the conshift isn't similar to the newshift
				foreach($newshift as $key => $value) {
					if ($value != $conshift[$key]) {
						$difference = TRUE;
						break;
					}
				}
				if ($difference == FALSE) {
					$exists = TRUE;
					$start_time = $newshift["st"];
					$end_time = $newshift["et"];
					$uid = $newshift[1];
					foreach($users as $user)
						if ($user["id"] == $uid){$usern = $user["name"];}
					$day_number = $newshift[2];
					$locationid = $newshift[0];
					$day_name = date('l', strtotime("Sunday + $day_number Days"));
					break;
				}
			}
			if ($exists == TRUE) {
				$save_errors[] = array($start_time, $end_time, "usern"=>$usern, "dow"=>$day_number, "error_name"=>"time_conflict", "error_description"=>"User $usern already has a shift on $day_name from ".$conshift['st']." to ".$conshift['et'].".");
			} else {
				$conshift = $conshift_whole[0];
				$start_time = $conshift["st"];
				$end_time = $conshift["et"];
				$uid = $conshift[1];
				$classid = (isset($conshift['classid']))?$conshift["classid"]:"";
				foreach($users as $user)
					if ($user["id"] == $uid){$usern = $user["name"];}
				$day_number = $conshift[2];
				$locationid = $conshift[0];
				$day_name = date('l', strtotime("Sunday + $day_number Days"));

				$extra_shift_values = array("classid"=>$classid);

				//Edited by Brian D.------------------------------------------------------------------------------------------------------------------
				//Commenting out current insert and making new insert that includes the exact dateTime of the shift.
				//$shiftins_query = lavu_query("insert into `user_schedules` (`loc_id`,`user_id`,`dayofweek`,`start_time`,`end_time`,`date`) values ('[1]','[2]','[3]','[4]','[5]','[6]')",$locationid,$uid,$day_number,$start_time,$end_time,$week_date);
				$shiftins_query = insertShiftRowIntoDB($locationid, $uid, $day_number, $start_time, $end_time, $week_date, $extra_shift_values);
				//End editing by Brian D.------------------------------------------------------------------------------------------------------------------




			}
		}
	} // end of save shifts

	// ~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>
	// get an array of all the shifts
	// ~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>
	$results = load_shifts($locationid, $user_schedule, $week_date, $users, $deleted_users, NULL);
	$init_js = $results["init_js"];
	$loaded_shifts = $results["loaded_shifts"];
	$display_shifts = $results["display_shifts"];

	// ~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>
	// create an array of userids -> usernames that the javascript will use
	// ~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>
	function get_safe_username($username, $special_case) {
		// $bad_symbols = array("'", '"', '[', ']', '(', ')', '-', "\\", '/', '<', '>', ':', ';', '*', '^', '%', '.', '$', '#', '@', '!', '`', '?', '|', '{', '}', '_');
		// foreach($bad_symbols as $bs)
		// 	$username = str_replace($bs, '', $username);
		switch( $special_case ){
			case 'javascript' : {
				return str_ireplace("'", "&#39;", htmlspecialchars( $username ));
			} case 'url' : {
				return urlencode( $username );
			} case 'html' : {
			} default : {
				return htmlspecialchars( $username );
			} 
		}
	}

	echo "<script language='javascript'>" . "\n";

	echo "  var userids_to_usernames = new Array();" . "\n";
	foreach($users as $user)
		echo "  userids_to_usernames[" . $user['id'] . "] = '" . get_safe_username($user['name'], 'javascript') . "';" . "\n";
	echo "  userids_to_usernames[-1] = '" . $user_schedule . "';" . "\n";

	echo "  var usernames_to_userids = new Array();" . "\n";
	foreach($users as $user)
		echo "  usernames_to_userids['" . get_safe_username($user['name'], 'javascript') . "'] = '" . $user['id'] . "';" . "\n";

	if ($user_schedule != "all"){
		foreach($users as $user) {
			if ($user['id'] = $user_schedule) {
				echo "  userids_to_usernames[-1] = '" . get_safe_username($user['name'], 'javascript') . "';" . "\n";
			}
		}
	}

	echo "var first_user_name = '" . get_safe_username($users[0]['name'], 'javascript') . "';";

	echo "</script>" . "\n";

	// ~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>
	// draw the original form
	// ~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>
	$user_name = "";
	foreach ($users as $user)
		if ($user["id"] == $user_schedule) {$user_name = $user["name"]; break;}
	if ($user_name == "")
		foreach ($deleted_users as $deleted_user)
			if ($deleted_user["id"] == $user_schedule) {$user_name = $deleted_user["name"]; break;}
	if($user_name!="" || $user_schedule == "all")
	{
		echo '<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"><'.'/script>' . "\n";
		//echo '<script type="text/javascript" src="http://demos.flesler.com/jquery/scrollTo/js/jquery.scrollTo-min.js"><'.'/script>' . "\n";

    	// S H I F T   E D I T O R   C H A R T
    	// container
		echo "<div style='position:absolute; top:0px; left:0px; visibility:hidden' id='shift_editor_container'>" . "\n";
		echo "  <table cellpadding=2 style='border:solid 1px #777777' bgcolor='#eeeeee'>" . "\n";
		// dayname and close button
		echo "    <tr><td align='center' colspan='2'>" . "\n";
		echo "      <table width='100%'><tr><td width='10%'><a style='cursor:pointer;' onclick='' id='shift_editor_del_button'><b><img src='".$del_icon."' alt='DEL'></b></a></td><td align='center' width='80%' id='shift_editor_dayname'>&nbsp;</td><td width='10%' align='right'><a style='cursor:pointer' onclick='close_shift_editor()'><b><font style='color:#777777'>X</font></b></a></td></tr></table>" . "\n";
		echo "    </td></tr>" . "\n";
		// user selectbox
		if ($user_schedule == "all") {
			echo "    <tr><td align='center' colspan='2'>" . "\n";
			echo "      <select id='shift_editor_user_select' onchange='shift_editor_show_save_button();shift_editor_update_checkboxes();shift_editor_show_time();' style='visibility:hidden;width:1px; height:1px;'>" . "\n";
			echo "<option>&nbsp;</option>";
			foreach($users as $user)
			{
				$name2 = preg_replace('/\s+/', '_', $user["name"]);
				$name1 = $user["name"];
				echo "<option>" . $name1 . "</option>";
			}
			echo "</select>" . "\n";
			echo "<font id='shift_editor_save_button_container'><input type='button' id='shift_editor_save_button' onclick='' style='visibility:hidden;width:0px;' value='Update Shift' disabled></font>";
			echo "<b><font  style='cursor:pointer;color:#777777;display:none;' onclick='shift_editor_show_checkboxes()'>More+</font></b>";
			echo "    </td></tr>" . "\n";
		}
		// time slots
		echo "    <tr>";
		echo "<td style='vertical-align:top;text-align:center;'>";
		echo "<div id='shift_editor_time_container'>";
		echo "<div id='shift_textboxes' style='display:none'>";
		echo "Specific time: ";
		echo "<input type='text' style='font-size:10px' size='7' id='new_shift_start_textbox' onkeypress='' value='' />";
		echo " - ";
		echo "<input type='text' style='font-size:10px' size='7' id='new_shift_end_textbox' onkeypress='' value='' />";
		echo "<input type='button' value='&#10003;' onclick='$(\"#new_shift_end_textbox\").trigger({type:\"keypress\",which:13,keyCode:13});' />";
		echo "</div><br />";
		echo "<div id='shift_editor_chart_container' style='width:250px;height:420px;overflow:auto;'>&nbsp;</div>";
		// advanced time option checkbox
		$checked_query = lavu_query("SELECT * FROM `config` WHERE `location`='[1]' and `setting`='[2]'", $locationid, "shift_editor_advanced_time_enabled");
		$checked = "";
		if (mysqli_num_rows($checked_query) > 0) {
			$checked_read = mysqli_fetch_assoc($checked_query);
			if ($checked_read["value"] == "1" || $checked_read["value"] == 1)
				$checked = "checked";
		}

		$class_list = array();
		$class_count = array();
		$class_list['E'] = array('title'=>"General Employee",'type'=>"GEN",'_deleted'=>0,'id'=>0,'tipout_rules'=>'');
		$class_count['E'] = 0;
		$class_query = lavu_query("select * from `emp_classes`");
		echo "<script type='text/javascript'>";
		echo "	emp_class_info_by_id = new Array(); ";
		while($class_read = mysqli_fetch_assoc($class_query))
		{
			$class_list["E".$class_read['id']] = $class_read;
			$class_count["E".$class_read['id']] = 0;
			echo "	emp_class_info_by_id['".$class_read['id']."'] = ".print_array_as_javascript_array($class_read,"assoc")."; ";
		}
		echo "</script>";
		
		$js_class_list_arr = "class_choices_by_userid = new Array(); ";
		echo "<input type='checkbox' id='advanced_time_checkbox' onclick='' ".$checked.">Specify time exactly";
		echo "&nbsp;<font style='font-style:italic;color:#aaa' id='advanced_time_checkbox_success'></font><br />";
		echo "</div>";
		echo "</td>" . "\n";
		// users checkbox
		if ($user_schedule == "all") {
			echo "<td style='vertical-align:top;display:none;' id='shift_editor_user_checkboxes'><div style='width:300px;height:420px;overflow:auto;'>";
			echo "<table cellspacing=0 cellpadding=0>";
			foreach($users as $user)
			{
				echo "<tr>";
				$class_str = "";
				$class_init_selected_value = "";
				$allow_class_selection = true;
				//if(isset($_GET['testt'])) $allow_class_selection = true;
				if($allow_class_selection) // Class Selection Code
				{
					$role_id_list_str = "";
					for($r=0; $r<count($user["role_id_list"]); $r++)
					{
						$class_id = $user["role_id_list"][$r]['id'];
						if(isset($class_list["E".$class_id]))
						{
							$class_read = $class_list["E".$class_id];
							$select_class_id = "select_class_" . $user['id'] . "_" . $class_id;
							if($role_id_list_str!="") $role_id_list_str .= ",";
							$role_id_list_str .= "\"$class_id\"";
							if($class_str=="") 
							{
								$class_init_selected_value = $class_id;
								$style_str = "color:#444444; border:solid 1px #444444; background-color:#eeeeee";
							}
							else $style_str = "color:#aaaaaa; border:solid 1px #aaaaaa; background-color:#dddddd";
							$class_str .= "<td id='$select_class_id' style='font-size:10px; cursor:pointer; $style_str' onclick='select_role_id_for_employee(\"".$user["id"]."\",\"$class_id\",new Array([role_id_list_str]))'><nobr>" . $class_read['title'] . "</nobr></font>";
							$js_class_list_arr .= "class_choices_by_userid['".$user['id']."'] = new Array([role_id_list_str]); ";
						}
					}
					$class_str = str_replace("[role_id_list_str]",$role_id_list_str,$class_str);
					$js_class_list_arr = str_replace("[role_id_list_str]",$role_id_list_str,$js_class_list_arr);
				}
				$empcid = "emp_classes_".$user["id"];
				echo "<td>";
				echo "<input type='checkbox' id='shift_editor_user_checkbox_" . $user['id'] . "' style='padding:0 0 0 5px;' onclick='";
				if($class_str!="")
				{
					echo "if(this.checked) setdis = \"block\"; else setdis = \"none\"; document.getElementById(\"$empcid\").style.display = setdis; ";
				}
				echo "shift_editor_show_save_button();shift_editor_show_time();'>" . get_safe_username($user['name'], 'html');
				echo "</input>";
				echo "</td>";
				if($class_str!="")
				{
					echo "<td><table cellspacing=2 cellpadding=2 style='display:none' id='$empcid'><tr>" . $class_str . "</tr></table><input type='hidden' value='$class_init_selected_value' id='selected_$empcid'></td>";
				}
				echo "</tr>";
			}
			echo "</table>";
			echo "<script type='text/javascript'>";
			echo "	function select_role_id_for_employee(ruser_id,rclass_id,rclass_list) { ";
			echo "		document.getElementById('selected_emp_classes_' + ruser_id).value = rclass_id; ";
			echo "		for(var r=0; r<rclass_list.length; r++) { ";
			echo "			if(rclass_list[r]==rclass_id) {";
			echo "				set_bgcolor = '#eeeeee'; ";
			echo "				set_color = '#444444'; ";
			echo "			} ";
			echo "			else { ";
			echo "				set_bgcolor = '#dddddd'; ";
			echo "				set_color = '#aaaaaa'; ";
			echo "			} ";
			echo "			set_border = 'solid 1px ' + set_color; ";
			echo "			var rsid = document.getElementById('select_class_' + ruser_id + '_' + rclass_list[r]); ";
			echo "			rsid.style.backgroundColor = set_bgcolor; ";
			echo "			rsid.style.color = set_color; ";
			echo "			rsid.style.border = set_border; ";
			echo "		} ";
			echo "	} ";
			echo "	$js_class_list_arr ";
			echo "</script>";
			echo "<font id='shift_editor_save_button2'>&nbsp;</font>";
			echo "</div></td>" . "\n";
		}
		echo "</tr>";
		echo "  </table>" . "\n";
		echo "</div>" . "\n";

		// H E A D E R   A B O V E   T A B L E
		// print any save errors
		for ($i = 0; $i < count($save_errors); $i++)
		{
			$error = $save_errors[$i];
			echo "<table style='border:solid 1px #888888; font-size:10px; color:#555555' bgcolor='#eeeeee' cellpadding=8><tr><td align='center'>";
			echo "<b>Error (" . $error["error_name"] . "):</b> " . $error["error_description"];
			echo "</td></tr></table>";
			if ($i < count($save_errors)-1)
				echo "<br />";
		}
		// edit user schedule
		echo speak("Edit User Schedule").": ";
		echo "<select onchange='change_user()' id='user_selectbox'><option>All Users</option>";
		foreach($users as $user)
			if ($user['id'] == $user_schedule)
				echo "<option selected='selected'>" . get_safe_username($user['name'], 'html') . "</option>";
			else
				echo "<option>" . get_safe_username($user['name'], 'html') . "</option>";
		echo "</select>";
		echo "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
		echo "<input type='button' value='".speak('Save Shifts')."' onclick='save_shifts()' />";
		echo "<br />";
		// hr and schedule
		echo "<hr style='color:#a5a5a5'>";
        echo "<div id='message_alerts'></div>";
		echo "<font style='font-family:\"Verdana\",sans-serif,serif;font-size:30px;color:#a5a5a5;'>Schedule</font>";
		// week
		echo "<form method='get' action='' style='font-size:12px;font-weight:bold;font-family:\"Verdana\",sans-serif,serif;color:#454545;vertical-align:bottom;' class='scheduleTable'>";
		{
			$week_time = strtotime($week_date);
			$sunday = date("M d", $week_time);
			$saturday = date("M d", strtotime("Next Saturday", $week_time));
			$year = date("Y", $week_time);
			echo "Week of $sunday - $saturday, $year";
			if ($week_date == date("m/d/y", strtotime('last Sunday'))){
				echo " (Current)";
			} else {
				echo "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
				foreach($_GET as $key => $value)
					if ($key != "date") echo "<input type='hidden' name='" .$key. "' value='" . $value . "'>";
				echo "<input type='hidden' name='date' value='" . date("m/d/y", strtotime("last Sunday")) . "'>";
				echo "<input type='submit' value='Today'>";
			}
            echo "&nbsp;&nbsp;Printable Schedule: <a href='/cp/?widget=print_schedule&start_date=".date("Y-m-d",$week_time)."&end_date=".date("Y-m-d",strtotime("Next Saturday",$week_time))."' target='_blank'><input type='button' class='categoryDetails' value='Graph Style' /></a>";
			echo "&nbsp;&nbsp;";
            echo "<a href='/cp/?widget=print_schedule&start_date=".date("Y-m-d",$week_time)."&end_date=".date("Y-m-d",strtotime("Next Saturday",$week_time))."&display_type=chart' target='_blank'><input type='button' class='categoryDetails' value='Chart Style' /></a>";
		}

		// T A B L E
		// tell the user if this employee has been deleted
		if ($user_schedule != "all") {
			$bad_user = FALSE;
			foreach ($deleted_users as $deleted_user) {
				if ($deleted_user["id"] == $user_schedule) {
					echo ("User [" . $deleted_user["name"] . ", " . $user_schedule . "] has been deleted and their schedule cannot be shown.");
					$bad_user = TRUE;
					break;
				}
			}
			if ($bad_user == FALSE) {
				echo "<table style='border:solid 1px #aaaaaa;width:100%;' id='user_schedule_table_mark2'></table>";
			}
		} else {
			echo "<table style='border:solid 1px #aaaaaa;width:100%;' id='user_schedule_table_mark2'></table>";
		}
		echo "<br /><br />";
		
		$note_query = lavu_query("select * from `user_schedules` where `date`='[1]'","notes_".$week_date);
		if(mysqli_num_rows($note_query))
		{
			$note_read = mysqli_fetch_assoc($note_query);
			$note_str = $note_read['data_long'];
		}
		else $note_str = "";
		echo "Special Notes:<br>";
		echo "<textarea rows='6' cols='80' name='schedule_special_notes_textarea' id='schedule_special_notes_textarea'>".str_replace("<","&lt;",$note_str)."</textarea>";
		echo "<br /><br />";

		echo "</form>" . "\n";

		// based on date format setting
		$date_setting = "date_format";
		$location_date_format_query = lavu_query( "SELECT `location`, `setting`, `value` FROM `config` WHERE `setting` = '[1]'",  $date_setting);
		if( !$location_date_format_query || !mysqli_num_rows($location_date_format_query)){
			$date_format = array("value" => 2);
		}else
			$date_format = mysqli_fetch_assoc( $location_date_format_query );

		// F O O T E R
		// print the week importer form
		// allows importing and duplication from the last $maxweeks weeks, that have shifts in them
		$result = lavu_query("SELECT `date` FROM `user_schedules` WHERE `date` != '[1]' GROUP BY `date`", $week_date);
		if (mysqli_num_rows($result) > 0){
			echo "<table style='width:400px;text-align:left;border:solid 1px #aaa;background:#eee;'><tr><td>" . "\n";
			$dates = array();
			if (mysqli_num_rows($result) > 0){
				while($row = mysqli_fetch_assoc($result)){
					if (date("D", strtotime(trim($row['date']))) == "Sun"){
						$dates[] = strtotime(trim($row['date']));
					}
				}
			}
			arsort($dates, SORT_NUMERIC);
			if ($user_schedule == "all") {
				echo "Import shifts for all users:<br />";
			} else {
				echo "Import shifts for user $user_name:<br />";
			}
			echo "<table><tr><td>";
			echo "Source week: ";
			echo "</td><td>";
			echo "<select id='import_shifts_select_week'>";
			$maxweeks = 52;
			$numweeks = 0;
			foreach($dates as $date){
				if ($numweeks > $maxweeks)
					break;
				$numweeks++;
				switch ($date_format['value']){
					case 1:$startdate = date("d/m/y", $date);
					$enddate = date("d/m/y", strtotime("Next Saturday", $date));
					break;
					case 2:$startdate = date("m/d/y", $date);
					$enddate = date("m/d/y", strtotime("Next Saturday", $date));
					break;
					case 3:$startdate = date("y/m/d", $date);
					$enddate = date("y/m/d", strtotime("Next Saturday", $date));
					break;
					default:$startdate = date("m/d/y", $date);
					$enddate = date("m/d/y", strtotime("Next Saturday", $date));
				}
				echo "<option>$startdate - $enddate</option>";
			}
			echo "</select>" . "\n";
			echo "</td></tr><tr><td>";
			echo "Destination week: ";
			echo "</td><td>";
			echo "<select><option>this week</option></select>" . "\n";
			echo "</td></tr></table>";
			$checked_query = lavu_query("SELECT * FROM `config` WHERE `location`='[1]' and `setting`='[2]'", $locationid, "shift_editor_delete_destination_week");
			$checked = "checked";
			if (mysqli_num_rows($checked_query) > 0) {
				$checked_read = mysqli_fetch_assoc($checked_query);
				if ($checked_read["value"] == "0" || $checked_read["value"] == 0)
					$checked = "";
			}
			echo "<input type='checkbox' id='delete_current_week_shifts_checkbox' onclick='set_preferences(\"delete_destination_week\")' ".$checked." /> Delete all shifts in destination week.";
			echo "&nbsp;<font style='color:#aaa;font-style:italic' id='delete_current_week_shifts_success'></font><br />" . "\n";
			echo "<input type='button' id='import_shifts_import_button' onclick='import_shifts_pressed()' value='Import Week' /><br />" . "\n";
			echo "</td></tr></table>" . "\n";
			echo "<br /><br />" . "\n";
		}

		// print the directions
		echo "<br>" . "\n";
		echo "<table style='border:solid 1px #888888; font-size:10px; color:#555555' bgcolor='#eeeeee' cellpadding=8><tr><td align='center'>" . "\n";
		echo "<b>How to Edit a User Schedule:</b><br>" . "\n";
		echo "<div style='text-align:left;'>";
		echo "<br>1. Choose a user's schedule to view from the drop down list at the top. Select \"All Users\" to view all users.";
		echo "<br>2. To Add a new shift:";
		echo "<br>&nbsp;&nbsp;&nbsp;&nbsp;2.1. To Add a new shift, click \"Add Shift\" on the day of the week you would like to create a shift on.";
		echo "<br>&nbsp;&nbsp;&nbsp;&nbsp;2.2. Choose one or \"More+\" users for the shift, then click on the time span for the shift.";
		echo "<br>3. To modify an existing shift";
		echo "<br>&nbsp;&nbsp;&nbsp;&nbsp;3.1. Click on the shift you would like to change.  To delete it, click on the \"DEL\" icon in the upper left corner.";
		echo "<br>4. Don't forget to hit the \"Save\" button to save your changes!";
		echo "</div>";
		echo "</td></tr></table><br>" . "\n";

		// print some whitespace
		echo "<div style='height:300px;'>&nbsp;</div>" . "\n";

		echo "<form name='schedule_form' id='schedule_form' method='post' action=''>" . "\n";
		echo "<input type='hidden' name='data' id='data' value=''>" . "\n";
		echo "<input type='hidden' name='schedule_special_notes' id='schedule_special_notes'>" . "\n";
		echo "</form>" . "\n";

		echo "<form name='reload_form' id='reload_form' method='get' action=''>" . "\n";
		foreach($_GET as $key => $value)
			if ($key != "user_schedule")
				echo "<input type='hidden' name='" . $key . "' value='" . $value . "'>";
		echo "<input type='hidden' name='user_schedule' id='user_schedule' value='" . $user_schedule . "'>" . "\n";
		echo "</form>";

		// T A B L E   W E E K D A Y S
		// print the table of shifts
		echo "<table style='border:solid 1px #aaaaaa;visibility:collapse;'>";
		echo "<tr id='weekdays_table_header'>";
		// left arrow (previous week)
		$previous_week_submit = "$(\"#previous_week_form\").submit()";
		$week_time = strtotime($week_date);
		$previous_week_date = date("m/d/y", strtotime("-1 week", $week_time));
		$previous_week_form = "<form id='previous_week_form' visibility='hidden' method='get' action=''><input type='hidden' name='date' value='".$previous_week_date."'>";
		foreach($_GET as $key => $value)
			if ($key != "date")
				$previous_week_form .= "<input type='hidden' name='" . $key . "' value='" . $value . "'>";
		$previous_week_form .= "</form>";
		$icon = "<image src='" . $back_icon . "' alt='<'>";
		echo "<td rowspan='2' style='background-color:#d2e297;text-align:center;vertical-align:middle;cursor:pointer;' onclick='$previous_week_submit'>$previous_week_form $icon</td>";

		$week_time = strtotime($week_date);
		$spacer = "<br /><font style='visibility:hidden'>12:00am - 12:00pm[1]</font>";
		echo "<td style='background-color:#eee;text-align:center;vertical-align:middle;font-family:\"Verdana\",sans-serif,serif;font-size:18px;color:afafaf;'>User</td>";
		$daynames = array("Sun","Mon","Tue","Wed","Thu","Fri","Sat");
		for($n=0; $n<7; $n++)
		{
			switch ($date_format['value']){
				case 1:$daydate = date("d/m/y", strtotime("+{$n} days", $week_time));
				break;
				case 2:$daydate = date("m/d/y", strtotime("+{$n} days", $week_time));
				break;
				case 3:$daydate = date("y/m/d", strtotime("+{$n} days", $week_time));
				break;
			}
			//$daydate = date("m/d/y", strtotime("+{$n} days", $week_time));
			echo "<td style='background-color:#eee;text-align:center;' id='day_" . $n . "'>";
			echo "<font style='font-family:\"Verdana\",sans-serif,serif;font-size:18px;color:afafaf;'>".$daynames[$n]."</font><br />";
			echo "<font>".$daydate.$spacer."</font></td>";
		}

		// right arrow (next week)
		$next_week_submit = "$(\"#next_week_form\").submit();";
		$next_week_date = date("m/d/y", strtotime("+1 week", $week_time));
		$next_week_form = "<form id='next_week_form' visibility='hidden' method='get' action=''><input type='hidden' name='date' value='".$next_week_date."'>";
		foreach($_GET as $key => $value)
			if ($key != "date")
				$next_week_form .= "<input type='hidden' name='" . $key . "' value='" . $value . "'>";
		$next_week_form .= "</form>";
		$icon = "<image src='" . $next_icon . "' alt='>'>";
		echo "<td rowspan='2' style='background-color:#d2e297;text-align:center;vertical-align:middle;cursor:pointer;' onclick='$next_week_submit'>$next_week_form $icon</td>";
		echo "</tr>";
		echo "</table>";
	} else {
		echo ("<form method='get' action=''>User " . $user_schedule . " has a bad username. Click <input type='submit' value='here'> to view all user schedules.");
		foreach($_GET as $key => $value)
			if ($key != "user_schedule") echo "<input type='hidden' name='" .$key. "' value='" . $value . "'>";
		echo "<input type='hidden' name='user_schedule' value='all'></form>";
		$user_name = "";
	}

	if($init_js!="") {
		echo "<script language='javascript'> ";
		echo $init_js;
		echo "</script> ";
	}
	echo "<script language='javascript'> ";
	echo "sanitize_input();";
	echo "setTimeout('draw_table();', 300);"; // just in case the next call fails
	echo "draw_table();";
	echo "</script> ";
?>
