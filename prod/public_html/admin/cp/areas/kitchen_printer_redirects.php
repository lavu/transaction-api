<?php

	session_start();
	
	require_once(dirname(__FILE__) . "/../resources/core_functions.php");

	$r_mode = (isset($_REQUEST['mode']))?$_REQUEST['mode']:"";
	$r_c_id = (isset($_REQUEST['c_id']))?$_REQUEST['c_id']:"";
	$r_dn = (isset($_REQUEST['dn']))?$_REQUEST['dn']:"";
	$r_loc_id = (isset($_REQUEST['loc_id']))?$_REQUEST['loc_id']:"";

	$rdb = admin_info("database");
	$dn = sessvar("admin_dataname");
	$loc_id = sessvar("locationid");
	
	$kpr_response = array();
	
	if ($rdb && $dn && $loc_id && $dn==$r_dn && $loc_id==$r_loc_id) {
	
		lavu_connect_byid($r_c_id, $rdb);
	
		switch ($r_mode) {
			case "load_redirects": loadRedirects($r_loc_id, $kpr_response); break;
			case "submit_redirects": submitRedirects($r_loc_id, $_REQUEST['row_id'], $_REQUEST['kpr_name'], $_REQUEST['froms_and_tos'], $kpr_response); break;
			case "remove_redirects": removeRedirects($r_loc_id, $_REQUEST['row_id'], $kpr_response); break;
			default: $kpr_response['status'] = "unknown_mode"; break;
		}
		
	} else $kpr_response['status'] = "expired_session";

	echo lavu_json_encode($kpr_response);
	
	function loadRedirects($loc_id, &$resp) { 
	
		$get_redirects = lavu_query("SELECT `id`, `setting`, `value` FROM `config` WHERE `location` = '[1]' AND `type` = 'printer_redirects' AND `_deleted` = '0'", $loc_id);
		if ($get_redirects) {
			$resp['status'] = "success";
			$info = array();
			if (mysqli_num_rows($get_redirects) > 0) {
				while ($read = mysqli_fetch_assoc($get_redirects)) {
					$info[] = $read;
				}
			}		
			$resp['info'] = $info;		

		} else db_error($resp);
	}
	
	function submitRedirects($loc_id, $row_id, $kpr_name, $froms_and_tos, &$resp) {
	
		$check_config = lavu_query("SELECT `id` FROM `config` WHERE `id` = '[1]' AND `location` = '[2]' AND `type` = 'printer_redirects'", $row_id, $loc_id);
		if ($check_config) {
			if (mysqli_num_rows($check_config) > 0) {
				$update_redirects = lavu_query("UPDATE `config` SET `_deleted` = '0', `setting` = '[1]', `value` = '[2]' WHERE `id` = '[3]'", $kpr_name, $froms_and_tos, $row_id);
				if ($update_redirects) {
					$resp['status'] = "success";
					$resp['info'] = array(array("row_id"=>$row_id));
					doOnConfigurationChange($loc_id);
				} else db_error($resp);
			} else {
				$insert_redirects = lavu_query("INSERT INTO `config` (`location`, `type`, `setting`, `value`) VALUES ('[1]', 'printer_redirects', '[2]', '[3]')", $loc_id, $kpr_name, $froms_and_tos);
				if ($insert_redirects) {
					$resp['status'] = "success";
					$resp['info'] = array(array("row_id"=>lavu_insert_id()));
					doOnConfigurationChange($loc_id);
				} else db_error($resp);
			}
		} else db_error($resp);
	}
	
	function removeRedirects($loc_id, $row_id, &$resp) {
	
		$remove_redirects = lavu_query("UPDATE `config` SET `_deleted` = '1' WHERE `id` = '[1]'", $row_id);
		if ($remove_redirects) {
			$resp['status'] = "success";
			$resp['info'] = array(array("row_id"=>$row_id));
			doOnConfigurationChange($loc_id);
		} else db_error($resp);
	}
	
	
	function doOnConfigurationChange($loc_id) {
	
		lavu_query("UPDATE `devices` SET `needs_reload` = '1' WHERE `loc_id` = '[1]'", $loc_id);
	}
	
?>
