<?php
if($in_lavu) {
	// Inventory 2.0 wizard pop up
    if ($_REQUEST['posted'] != 1) {
        require_once(dirname(__FILE__) . "/inventoryWizardPopup.php");

    }
	echo "<br><br>";
	global $data_name;
	
	$forward_to = "index.php?mode={$section}_{$mode}";
	$tablename = "restaurants";
	$rowid = admin_info("companyid");

	// Needed because the location for the config setting is being recorded as the "rowid" which might be different
	$config_query = lavu_query("select * from `inventory_settings`");
	while($config_read = mysqli_fetch_assoc($config_query))
	{
		$location_info[$config_read['setting']] = $config_read['value'];
	}
    $include_session = "?session_id=".session_id();
    $include_data_name = "&dn=".sessvar("admin_dataname");

    $edit_inventory_units_measure_iframe_src = "/cp/modal_edit_inventory_units_measure.php".$include_session.$include_data_name;
    $edit_storage_location_iframe_src = "/cp/modal_edit_storage_location.php".$include_session.$include_data_name;
    $edit_waste_reason_iframe_src = "/cp/modal_edit_waste_reasons.php".$include_session.$include_data_name;
    $edit_inventory_categories_iframe_src = "/cp/modal_edit_inventory_categories.php".$include_session.$include_data_name;
    $modalNumber = 0;
	$fields = array();
	$fields[] = array(speak("Inventory Settings"),"Inventory Settings","seperator");
	$fields[] = array(
			speak("Units of Measure").":",
			"Units of Measure",
			"open_modal_button",
			array(
					'prefix'	=> "",
					'value'		=> speak("View / Edit"),
					'link'     => "index.php?mode=settings_edit_inventory_units_measure&edit=1",
                    'header_text'	=> "Units Of Measure",
                    'iframe_src'	=> $edit_inventory_units_measure_iframe_src,
                    'modal_number'	=> $modalNumber++
			)
	);


	$costing_methods = array(
			'average_cost_periodic' => speak('Average costing, periodic'),
			'fifo' => speak('FIFO (First In, First Out)')
	);
	$fields[] = array(
			speak("Costing Method").":",
			"inventory_config:inventory_costing_method",
			"select",
			$costing_methods
	);
	$fields[] = array(
			speak("Storage Location").":",
			"Storage Location",
			"open_modal_button",
			array(
                'prefix'	=> "",
                'value'		=> speak("View / Edit"),
                'link'		=> "index.php?mode=settings_edit_storage_location&edit=1",
                'header_text'	=> "Storage Location",
                'iframe_src'	=> $edit_storage_location_iframe_src,
                'modal_number'	=> $modalNumber++
			)
	);
	$fields[] = array(
			speak("Waste Reasons").":",
			"Waste Reasons",
			"open_modal_button",
			array(
                'prefix'	=> "",
                'value'		=> speak("View / Edit"),
                'link'		=> "index.php?mode=settings_edit_waste_reasons&edit=1",
                'header_text'	=> "Waste Reasons",
                'iframe_src'	=> $edit_waste_reason_iframe_src,
                'modal_number'	=> $modalNumber++
			)
	);
	$fields[] = array(
			speak("Inventory Categories").":",
			"Inventory Categories",
			"open_modal_button",
			array(
                'prefix'	=> "",
                'value'		=> speak("View / Edit"),
                'link'		=> "index.php?mode=settings_edit_inventory_categories&edit=1",
                'header_text'	=> "Inventory Categories",
                'iframe_src'	=> $edit_inventory_categories_iframe_src,
                'modal_number'	=> $modalNumber++
			)
	);
	$fields[] = array(
			speak("Use First Inventory Dashboard").":",
			"inventory_config:use_first_inventory",
			"select",
			getInventoryCategoriesList()
	);
	$display_items = array(
			'1' => speak('1 day old'),
			'5' => speak('5 days old'),
			'7' => speak('7 days old'),
			'14' => speak('14 days old'),
			'30' => speak('30 days old'),
			'60' => speak('60 days old')
	);
	$fields[] = array(
			speak("Display items that are").":",
			"inventory_config:display_items_that_are_x_days_old",
			"select",
			$display_items
	);
	$fields[] = array(speak("Save"),"submit","submit");
	require_once(resource_path() . "/form.php");

	echo "<br><br>";
}

function getInventoryCategoriesList(){
	$inv_query = lavu_query("SELECT `id`,`name` from `inventory_categories` where `_deleted` != 1");
	$dname = array();
	$did = array();
	while ($den = mysqli_fetch_assoc($inv_query)) {
		$dname[] = $den['name'];
		$did[] = $den['id'];
	}
	return array_combine($did,$dname);
}
//Will return true if any of the default settings have been added onto.
function checkInventorySettings(){
    $inventorySettingsQuery = lavu_query("SELECT * from 'inventory_settings'");
    $settingsArray = mysqli_fetch_assoc($inventorySettingsQuery);
    if(count($settingsArray > 1)){
        return true;
    }
    $storageQuery = lavu_query("SELECT * from `inventory_storage_locations` WHERE `_deleted` != 1");
    $storageArray = mysqli_fetch_assoc($storageQuery);
    if(count($storageArray) > 6){
        return true;
    }
    $categoryQuery = lavu_query("SELECT * from `inventory_category` WHERE `_deleted` != 1");
    $categoryArray = mysqli_fetch_assoc($categoryQuery);
    if(count($categoryArray) > 4){
        return true;
    }
    $wasteQuery = lavu_query("SELECT * from `inventory_waste_reasons` WHERE `_deleted` != 1");
    $wasteArray = mysqli_fetch_assoc($wasteQuery);
    if(count($wasteArray) > 4){
        return true;
    }

    return false;
}
?>
<script type='text/javascript'>
    function CloseModalPopUp(modal) {
        document.getElementById('advanced_settings_modal_'+ modal).style.display = "none";
    }
</script>
