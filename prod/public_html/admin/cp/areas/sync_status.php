<?php
	echo "<br><br>";
	// locationid
	// data_name
	// orders synced up
	// records check status
	
	$last_sync_query = lavu_query("select * from config where `setting`='orders synced up' order by `value` desc limit 1");
	if(mysqli_num_rows($last_sync_query))
	{
		$last_sync_read = mysqli_fetch_assoc($last_sync_query);
		
		$last_sync_time = $last_sync_read['value5'];
		$last_sync_contents = explode("|",$last_sync_read['value3']);
		
		if(count($last_sync_contents) > 1)
		{
			$elapsed_sync = time() - $last_sync_read['value'] * 1;
			if($elapsed_sync >= 90)
			{
				if($elapsed_sync >= 60 * 60 * 24)
				{
					$days_ago = floor($elapsed_sync / (60 * 60 * 24));
					$time_ago = $days_ago . " day";
					if($days_ago > 1) $time_ago .= "s";
					$time_ago .= " ago";
				}
				else if($elapsed_sync >= 60 * 60)
				{
					$hours_ago = floor($elapsed_sync / (60 * 60));
					$time_ago = $hours_ago . " hour";
					if($hours_ago > 1) $time_ago .= "s";
					$time_ago .= " ago";
				}
				else
				{
					$minutes_ago = floor($elapsed_sync / 60);
					$time_ago = $minutes_ago . " minute";
					if($minutes_ago > 1) $time_ago .= "s";
					$time_ago .= " ago";
				}
				echo "<br>Last Synced: " . date("Y-m-d h:i:s a",$last_sync_read['value']) . " Central Time (".$time_ago.")";
			}
			else
			{
				$time_ago = "<table cellspacing=0 cellpadding=0><tr><td><div id='elapsed_seconds'>" . $elapsed_sync . "</div></td><td>&nbsp;second";
				if($elapsed_sync!=1) $time_ago .= "s";
				$time_ago .= " ago";
				$time_ago .= "</table>";
				echo "<br><table cellspacing=0 cellpadding=0><tr><td>Last Synced: " . date("Y-m-d h:i:s a",$last_sync_read['value']) . " Central Time (</td><td>".$time_ago."</td><td>)</td></tr></table>";
				
				$sync_left_var = "seconds_elapsed";
				$until_next_sync = $elapsed_sync;
				$sync_left_function = "advance_seconds_elapsed";
				$sync_left_display = "elapsed_seconds";
				
				echo "<script language='javascript'>";
				echo " $sync_left_var = ".$until_next_sync."; ";
				echo " function ".$sync_left_function."() { ";
				echo "  $sync_left_var = $sync_left_var + 1; ";
				echo "  document.getElementById('$sync_left_display').innerHTML = ".$sync_left_var."; ";
				echo "  if($sync_left_var > 0) { ";
				echo "    setTimeout('".$sync_left_function."()', 1000); ";
				echo "  } ";
				echo " } ";
				echo " setTimeout('".$sync_left_function."()', 1000); ";
				echo "</script>";
			}
			
			//echo "Last Sync At: " . $last_sync_time . " (Central Time)";
			echo "<br><br>";
			
			$sync_parts = explode(")(",$last_sync_contents[1]);
			for($n=0; $n<count($sync_parts); $n++)
			{
				echo trim($sync_parts[$n],"()") . "<br>";
			}
		}
	}
	else
	{
		echo speak("No Local Server Sync information found");
	}
?>

