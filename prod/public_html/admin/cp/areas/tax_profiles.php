<?php	
	if($in_lavu)
	{
	    $nor_way = lavu_query("SELECT `value` FROM `config` WHERE `setting` = 'norway_mapping'");
	    $norway_values = mysqli_fetch_assoc($nor_way);
	    $norway_status=$norway_values[value];
	    if($norway_status=='1'){
	    //norway related code
	    
    	    function readCSVFile($csvFile)
    	    {
    	        
    	        $file_handle = fopen($csvFile, 'r');
    	        while (!feof($file_handle) ) {
    	            $line_of_text[] = fgetcsv($file_handle, 12048);
    	        }
    	        fclose($file_handle);
    	        return $line_of_text;
    	    }
    	    $csvFile = __DIR__.'/../../norway_reports/codes/Standard_Tax_Codes.csv';
    	    $csv = readCSVFile($csvFile);
    	    foreach($csv as $oneArray) {
    	        $newArray[] = implode("",$oneArray);
    	    }
    	    array_shift($newArray);
    	    array_pop($newArray);
    	    
    	    foreach ($newArray as $vat_code)
    	    {
    	        $vat_ex=explode(";",$vat_code);
    	        $vat_data[0]=$vat_ex[2].($vat_ex[3]?' ('.$vat_ex[3].')': '');
    	        $vat_data[1]=$vat_ex[0];
    	        $vat_data1[]=$vat_data;
    	    }
    	    $vat_data=array_filter($vat_data1);
	    }
	    //end
	    
		$form_posted = (isset($_POST['posted']));
	
		$display = "";
		$field = "force_modifiers";
		$qtitle_category = "Profile";
		//$qtitle_category = speak("List");
		$qtitle_category_plural = "Tax Profiles";
		$qtitle_item = "Tax";
		
		$category_tablename = "tax_profiles";
		$inventory_tablename = "tax_rates";
		$category_fieldname = "id";
		$inventory_fieldname = "profile_id";
		$inventory_select_condition = "";
		$inventory_primefield = "title";
		$category_primefield = "title";
		
		$page_categories_by = 0;
	
		$inventory_orderby = "id";
		
		$cfields = array();
		
		$category_filter_by = "";
		$category_filter_extra = "(`loc_id` = '$locationid')";

		$category_filter_by = "loc_id";
		$category_filter_value = $locationid;
		$category_filter_extra = "";
				
		$qfields = array();
		$qfields[] = array("title","title",speak("Tax Name"),"input");
		$qfields[] = array("calc","calc",speak("Base Rate"),"input");
		//$qfields[] = array("force_apply","force_apply",speak("Force Apply"),"select",array(array("-", "0"), array("Force Apply", "1")));
		$qfields[] = array("stack","stack",speak("Stack"),"select",array(array("Don't Stack", "0"), array("Stack", "1")));
		$qfields[] = array("tier_type1","tier_type1",speak("Tier Type"),"select",array(array("No Tier", ""), array("Tier by Subtotal", "subtotal"), array("Tier by Quantity (2.3.12+)", "quantity"))); //, array("Tier by Quantity", "quantity")
		$qfields[] = array("tier_value1","tier_value1",speak("Tier Value"),"input");
		$qfields[] = array("calc2","calc2",speak("Tier Rate"),"input");
		if($norway_status=='1'){
		  $qfields[] = array("norway_vat_code","norway_vat_code",speak("Vat Code"),"select",$vat_data);
		}
		//$qfields[] = array("force_tier1","force_tier1",speak("Force Tier"),"select",array(array("-", "0"), array("Force Tier", "1")));
		
		$qfields[] = array("id");
		$qfields[] = array("delete");
		require_once(resource_path() . "/dimensional_form.php");
		$qdbfields = get_qdbfields($qfields);
						
		if($form_posted)
		{
			$cat_count = 1;
			while(isset($_POST["ic_".$cat_count."_category"]))
			{
				$category_name = $_POST['ic_'.$cat_count.'_category'];
				$category_id = $_POST['ic_'.$cat_count.'_categoryid'];
				$category_delete = $_POST['ic_'.$cat_count.'_delete_0'];
				if($category_name==$qtitle_category . " Name") $category_name = "";
				
				if($category_delete=="1")
				{
					delete_poslavu_dbrow($category_tablename, $category_id);
					lavu_query("UPDATE `menu_categories` SET `tax_profile_id`='' WHERE `tax_profile_id` = '[1]'", $category_id);
					lavu_query("UPDATE `menu_items` SET `tax_profile_id`='' WHERE `tax_profile_id` = '[1]'", $category_id);
				}
				
				if($category_name!="")
				{
					$dbfields = array();
					$dbfields[] = array("title",$category_name);
					$dbfields[] = array("loc_id",$locationid);
					for($n=0; $n<count($cfields); $n++)
					{
						$cfieldname = $cfields[$n][0];
						$dbfields[] = array($cfieldname,$_POST['ic_'.$cat_count.'_col_'.$cfieldname]);
					}
					$categoryid_indb = update_poslavu_dbrow($category_tablename,$dbfields, $category_id);
					if($category_fieldname=="title")
						$categoryid_indb = $category_name;
					$item_count = 1;
					while(isset($_POST["ic_".$cat_count."_".$inventory_primefield."_".$item_count]))
					{						
						$item_value = array(); // get posted values
						for($n=0; $n<count($qfields); $n++)
						{
							$qname = $qfields[$n][0];
							$qtitle = (isset($qfields[$n][2]))?$qfields[$n][2]:false;
							$set_item_value = $_POST['ic_'.$cat_count.'_'.$qname.'_'.$item_count];
							if($qtitle && $set_item_value==$qtitle) $set_item_value = "";
							if($qname=="cost") 
							{
								$set_item_value = str_replace("$","",$set_item_value);
								$set_item_value = str_replace(",","",$set_item_value);
							}
							$item_value[$qname] = $set_item_value;
						}
						$item_id = $item_value['id'];
						$item_delete = $item_value['delete'];
						$item_name = $item_value[$inventory_primefield];
						
						if($item_delete=="1")
						{
							delete_poslavu_dbrow($inventory_tablename, $item_id);
						}
						else if($item_name!="")
						{
							$dbfields = array();
							$dbfields[] = array($inventory_fieldname,$categoryid_indb);
							$dbfields[] = array("loc_id",$locationid);
							for($n=0; $n<count($qdbfields); $n++)
							{
								$qname = $qdbfields[$n][0];
								$qfield = (isset($qdbfields[$n][1]))?$qdbfields[$n][1]:$qname;
								$dbfields[] = array($qfield,$item_value[$qname]);
							}
							$orderby_tag = 'icrow_'.$cat_count.'_'.$item_count.'_order';
							if(isset($_POST[$orderby_tag]))
							{
								$dbfields[] = array("_order",$_POST[$orderby_tag]);
							}
							update_poslavu_dbrow($inventory_tablename,$dbfields, $item_id);
						}								
						$item_count++;
					}
				}
				$cat_count++;
			}
	
			$check_for_last_update = lavu_query("SELECT `id` FROM `config` WHERE `setting` = 'menu_last_updated' AND `location` = '[1]'", $locationid);
			if (@mysqli_num_rows($check_for_last_update)) {
				$uinfo = mysqli_fetch_assoc($check_for_last_update);
				lavu_query("UPDATE `config` SET `value` = now() WHERE `id` = '[1]'", $uinfo['id']);
			} else lavu_query("INSERT INTO `config` (`location`, `type`, `setting`, `value`) VALUES ('[1]', 'admin_activity', 'menu_last_updated', now())", $locationid);
	
			lavu_query("UPDATE `devices` SET `needs_reload` = '1' WHERE `loc_id` = '[1]'", $locationid);
				
			$form_error = "Warning: This section under Development";
		}
		
		$details_column = "";
		$send_details_field = "";
	
		$qinfo = array();
		$qinfo["qtitle_category"] = $qtitle_category;
		$qinfo["qtitle_category_plural"] = $qtitle_category_plural;
		$qinfo["qtitle_item"] = $qtitle_item;
		$qinfo["category_tablename"] = $category_tablename;
		$qinfo["inventory_tablename"] = $inventory_tablename;
		$qinfo["category_fieldname"] = $category_fieldname;
		$qinfo["inventory_fieldname"] = $inventory_fieldname;
		$qinfo["inventory_select_condition"] = $inventory_select_condition;
		$qinfo["inventory_primefield"] = $inventory_primefield;
		$qinfo["category_primefield"] = $category_primefield;
		$qinfo["details_column"] = $details_column;
		$qinfo["send_details"] = $send_details_field;
		$qinfo["field"] = $field;
		
		$qinfo["category_filter_by"] = $category_filter_by;
		$qinfo["category_filter_value"] = $category_filter_value;
		$qinfo["category_filter_extra"] = $category_filter_extra;
		$qinfo["page_categories_by"] = $page_categories_by;
		
		$qinfo["inventory_orderby"] = $inventory_orderby;
		
		$qinfo['forward_to'] = "index.php?mode=$mode";

		if ($min_POSLavu_version < 20202) {
			$display .= "<table width='700px'><tr><td align='left'>";
			$display .= "Tax Profiles will take effect with POSLavu version 2.2, and will replace the settings in the Tax Rates section as well as the default location tax rate setting in Advanced Location Settings. Tax Profiles can be assigned at both the Category and Item level.<br><br>The Custom Tax Rate setting for Categories and Items will still be effective, but it is recommended that all tax settings be set to rely on Tax Profiles as the Custom Tax Rate setting will be phased out in future versions.<br>";
			$display .= "</td></tr></table><br><br>";
		}

		$form_name = "ing";
		$field_code = create_dimensional_form($qfields,$qinfo,$cfields);
		$display .= "<table width='700px'><tr><td align='left'><span style='font-size:11px'>Note: When deleting a tax profile and creating a new tax profile, you must assign the desired profile to the menu categories/items through the Category/Item Details. Once you have selected the Tax Profile, be sure to hit Apply and then Save. Once you have reloaded settings on your device(s), the new profile will be applied to the selected menu items.<br><br>If your tax rates have recently changed, you can simply update the current tax profile and save changes. Once you have reloaded settings on your device(s), all items with the assigned tax profile will update to the new tax rate.</font></td></tr></table><br><br>";
		$display .= "<form name='ing' method='post' action=''>";
		$display .= "&nbsp;<input type='submit' value='".speak("Save")."' class='saveBtn'><br>&nbsp;";
		$display .= "<input type='hidden' name='posted' value='1'>";
		$display .= $field_code;
		$display .= "&nbsp;<input type='submit' value='".speak("Save")."' class='saveBtn'><br>&nbsp;";
		$display .= "</form>";
		
		echo $display;		
	}
?>
