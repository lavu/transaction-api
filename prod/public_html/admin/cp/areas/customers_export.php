<?php
/*

<input type='text' placeholder="search" id='search_input_id'/>
<input type='button' onclick='search_button_clicked'/>
<br>
*/
//LP-3611 
$cnt_validData=0;
$data_name=$_SESSION['poslavu_234347273_admin_dataname'];
$chain_id=$_SESSION['poslavu_234347273_admin_chain_id'];
//"select count(*) as cnt from poslavu_MAIN_db.restaurant_chains where id='$chain_id' and name='$data_name'"
$quary=rpt_query("select count(*) as cnt from `poslavu_MAIN_db`.`restaurants` where `data_name`='$data_name' and chain_id='$chain_id' and chain_name like('ABI%')");
$valid_info = mysqli_fetch_assoc($quary);
$cnt_validData=$valid_info['cnt'];

$titleFieldsMap = getColumnTitleToFieldMap($cnt_validData);
//echo "<pre>";print_r($titleFieldsMap);
$sortColumn = empty($_REQUEST['sort_by']) || empty($titleFieldsMap[$_REQUEST['sort_by']]) ? "l_name" : $titleFieldsMap[$_REQUEST['sort_by']];
$ascDesc = empty($_REQUEST['order'])  || $_REQUEST['order'] != 'desc' ? "asc" : 'desc';

//LP-3611 , to get order details...functionality...

if($cnt_validData>0){
    $customerRows = getAllMedCustomerRows_ABI($titleFieldsMap, $sortColumn, $ascDesc);
}
else{
    $customerRows = getAllMedCustomerRows($titleFieldsMap, $sortColumn, $ascDesc);
}

//$customerRows = sortCustomerArrByTitle($customerRows, $_REQUEST['sort_by'], $_REQUEST['order']);


if(isset($_GET['widget'])){
    
   
	header('Content-disposition: attachment; filename='.'customers_'.date('YmdHis').'.txt');
	header ("Content-Type:text/txt");
	echo generateCommaSeperatedListFromData($customerRows, $titleFieldsMap);
	//echo $resultData;
    exit;
}

$exportButton = "<input type='button' value='Export Tab Delimited' style='cursor:pointer; font-size:9px' onclick='window.location=\"?widget=customers_export&sort_by=".$sortColumn."&order=".$ascDesc."\"'/><br>&nbsp;";
echo $exportButton;
$displayTable = buildTableFromListOfMedCustomerRows($customerRows, $titleFieldsMap);
echo $displayTable;
echo "&nbsp;<br>" . $exportButton;

function getAllMedCustomerRows($titleFieldsMap, $sortColumn, $ascDesc){
    
    $result = rpt_query("SELECT * FROM `med_customers` ORDER BY `[1]` [2]", $sortColumn, $ascDesc);
    if(!$result){ error_log("MYSQL error in ".__FILE__." -diut- error:" . lavu_dberror() ); }
    echo lavu_dberror();
    $allCustomerRows = array();
    while($currRow = mysqli_fetch_assoc($result)){
      
        if($currRow['field6']!=""){
            $currRow['field6']="'".$currRow['field6'];
        }
        
        $allCustomerRows[] = $currRow;
    }
    return $allCustomerRows;
}

function getAllMedCustomerRows_ABI($titleFieldsMap, $sortColumn, $ascDesc){
    
    $result = rpt_query("SELECT * FROM `med_customers` ORDER BY `[1]` [2]", $sortColumn, $ascDesc);
    if(!$result){ error_log("MYSQL error in ".__FILE__." -diut- error:" . lavu_dberror() ); }
    echo lavu_dberror();
    
    //following new code added for LP-3611
    $med_id="";
    $medCustomerRows = array();
     $allCustomerRows = array();
    while($currRow = mysqli_fetch_assoc($result)){
        
        if($currRow['field6']!=""){
            $currRow['field6']="'".$currRow['field6'];
        }
       $medCustomerRows = $currRow;
      // echo "<pre>";print_r($medCustomerRows);exit;
        $med_id=$currRow['id'];
      
        if(!empty($med_id)){
         //echo "SELECT orders.order_id,orders.closed,(orders.subtotal-orders.itax) as order_sale,orders.discount,orders.gratuity as gratuity,orders.tax as tax,orders.itax as itax,orders.total as net,orders.order_status,cc_transactions.tip_amount + orders.cash_tip as tip_amount FROM `orders` left join cc_transactions on orders.order_id=cc_transactions.order_id  where order_status='closed' and original_id like('%|".$med_id."|%')";
        $order_result = rpt_query("SELECT orders.order_id,orders.closed,(orders.subtotal-orders.itax) as order_sale,orders.discount,orders.gratuity as gratuity,orders.tax as tax,orders.itax as itax,orders.total as net,orders.order_status,cc_transactions.tip_amount + orders.cash_tip as tip_amount FROM `orders` left join cc_transactions on orders.order_id=cc_transactions.order_id  where order_status='closed' and original_id like('%|".$med_id."|%')");
        if(!$order_result){ error_log("MYSQL error in ".__FILE__." -diut- error:" . lavu_dberror() ); }
        echo lavu_dberror();
       
        $rowcount=mysqli_num_rows($order_result);
        if($rowcount>0){
           
            while($currRow_order = mysqli_fetch_assoc($order_result)){
              $currRow_order['closed']=date('m/d/Y',strtotime($currRow_order['closed']));
              if($currRow_order['tip_amount']==''){
                $currRow_order['tip_amount']=0;
              }
              $allCustomerRows[]=array_merge($medCustomerRows, $currRow_order);
               
            }
         }
         else{
             $allCustomerRows[]=$medCustomerRows;
         }
         
         
         
        }
        
         
    }
    //end for LP-3611.
    
    return $allCustomerRows;
}

function getColumnTitleToFieldMap($cnt_validData=0){
    $result = rpt_query("SELECT * FROM `config` WHERE `setting`='GetCustomerInfo' AND `_deleted`='0' order by `id` ASC");
    $custInfoRows = array();
    while($currRow = mysqli_fetch_assoc($result)){
        $custInfoRows[] = $currRow;
    }
    
    $titleToFieldMap = array();
    foreach($custInfoRows as $currRow){
        $currTitle = str_replace("_", " ", $currRow['value']);
        $titleToFieldMap[$currTitle] = $currRow['value2'];
    }
   
    if($cnt_validData>0){
        //for LP-3611, new titles are added.
        $new_titles=array('Invoice ID'=>'order_id','Invoice Date'=>'closed','Order Sale'=>'order_sale','Tip'=>'tip_amount','Discount'=>'discount','Gratuity'=>'gratuity','Tax'=>'tax','iTax'=>'itax','Net'=>'net');
        $titleToFieldMap=array_merge($titleToFieldMap,$new_titles);
        //End of LP-3611.
    }
  
    
    return $titleToFieldMap;
}

function sortCustomerArrByTitle(&$customerArr, $title, $sortOrder){
    $titleToFieldMap = getColumnTitleToFieldMap();
    if(empty($title) || empty($titleToFieldMap[$title])) return $customerArr;
    $titleToFieldMap = getColumnTitleToFieldMap();
    $field = $titleToFieldMap[$title];
    $sortedArr = array();
    $i = 0;
    foreach($customerArr as $currRow){
        $prePend = empty($currRow[$field]) ? ' ' : '';
        $key = $prePend . strtolower($currRow[$field] . $i++);
        
        $sortedArr[$key] = $currRow;
    }
    if($sortOrder == 'desc')
        krsort($sortedArr);
    else
        ksort($sortedArr);
    return array_values($sortedArr);
}

function buildTableFromListOfMedCustomerRows($medCustomerRows, $columnTitleToFieldMap){
    $columnTitleToFieldMap = getColumnTitleToFieldMap();
    
    $htmlTable = "".
    "<table cellspacing=0 cellpadding=2>".
      "<tr>";
        foreach($columnTitleToFieldMap as $titleAsKey => $fieldAsValue){
            $sortOrder = $_REQUEST['sort_by'] == $titleAsKey && $_REQUEST['order'] == 'asc' ? 'desc' : 'asc';
            $onclick = "window.location = \"?mode=customers_export&sort_by=$titleAsKey&order=$sortOrder\"";
            $highlight = $_REQUEST['sort_by'] == $titleAsKey ? 'background-color:gray;' : '';
            $htmlTable .= "<td onclick='$onclick' align='center' valign='bottom' bgcolor='#888888' style='color:#ffffff; cursor:pointer;$highlight'>$titleAsKey</td>";
        }
      $htmlTable .= "</tr>";
    foreach($medCustomerRows as $currMedCustomerRow){
        $htmlTable .= "<tr>";
        foreach($columnTitleToFieldMap as $titleAsKey => $fieldAsValue){
            $htmlTable .= "<td valign='top' style='border-left:solid 1px #cccccc; border-right:solid 1px #cccccc; border-bottom:solid 1px #cccccc'>".$currMedCustomerRow[$fieldAsValue]."</td>";
        }
        $htmlTable .= "</tr>";
    }
    //$htmlTable .= "<tr><td colspan='60' style='border-top:solid 1px #cccccc'>&nbsp;</td></tr>";
    $htmlTable .= "</table>";
    return $htmlTable;
}

function generateCommaSeperatedListFromData($medCustomerRows, $columnTitleToFieldMap){
    $strBuilder = '';
    $init = false;
    foreach($columnTitleToFieldMap as $key => $val){
        if($init){$strBuilder .= "\t";} $init=true;
        $key = str_replace("\t", "\\t", $key);
        $key = str_replace("\"", "\\\"", $key);
        $strBuilder .= $key;
    }
    $strBuilder .= "\n";
    
    foreach($medCustomerRows as $currRow){
        $init = false;
        foreach($columnTitleToFieldMap as $key => $val){
            if($init) {$strBuilder .= "\t";} $init = true;
            $currValue = $currRow[$val];
            $currValue = str_replace("\\", "\\\\", $currValue);
            $currValue = str_replace("\t", "\\t",  $currValue);
            $currValue = str_replace("\n", "\\n",  $currValue);
            $strBuilder .= $currValue;
        }
        $strBuilder .= "\n";
    }
    return substr($strBuilder, 0, -1);//Return everything except last \n.
}

