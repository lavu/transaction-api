 <?php

require "global_values_and_functions.php";

// creates a meal_period_type
// @table_name the database table to pull from (should probably end with ".meal_period_types")
function create_meal_period_type($table_name, $name, $locationID)
{
	if ($name === "")
	{
			echo "nameMustNotBeEmpty";
			return array( False, "Error: the new name must not be empty" );
	}

	$result = lavu_query("SELECT * FROM `[1]` WHERE `name` = '[2]' AND `_deleted` = '[3]'", $table_name, $name, "0");

	if (mysqli_num_rows($result) === 1)
	{
		echo "MealPeriodTypeAlreadyExists";
		return array( False, "Error: Meal period type already exists" );
	}
	else
	{
		$date_created = date("m/d/y", strtotime("today"));
		lavu_query("INSERT INTO `[1]` (`name`, `date_created`) VALUES ('[2]', '[3]')", $table_name, $name, $date_created);
		if (ConnectionHub::getConn('rest')->affectedRows() > 0)
		{
			echo "success";
			sync_table($table_name, $locationID);
			return array( True, "start" );
		}
		else
		{
			echo "couldNotInsert";
			return array( False, "failed to insert" );
		}
	}
}

// removes a meal_period_type
// force will force deletion of meal periods if any exist that belong to this meal_period_type
function delete_meal_period_type($name, $force, $locationID)
{
	global $table_name_mp;
	global $table_name_mpt;
	$table_name = $table_name_mpt;

	// check that there aren't going to be any meal_periods destroyed
	$rows = ConnectionHub::getConn('rest')->getDBAPI()->getAllInTable($table_name_mp, array('type_name'=>'1', '_deleted'=>'0'));
	if (count($rows) > 0)
	{
			if (strtoupper($force) === "TRUE")
			{
					for ($i = 0; $i < count($rows); $i++)
					{
							$row = $rows[$i];
							$mp = get_meal_period_from_row($row);
							delete_meal_period($table_name_mp, $mp->id);
					}
			}
			else
			{
					echo "parentOfMealPeriods?" . count($rows);
					return array( False, "Error: there are " . count($rows) . " meal periods that belong to this type" );
			}
	}

	$result = lavu_query("UPDATE `[1]` SET `_deleted` = '[2]', `date_deleted` = '[3]' WHERE `name` = '[4]' AND `_deleted` = '[5]'", $table_name, "1", date("m/d/y", strtotime("today")), $name, "0");
	
	if (ConnectionHub::getConn('rest')->affectedRows() > 0)
	{
			echo "success";
			sync_table($table_name, $locationID);
			return array( True, "start" );
	}
	else
	{
			echo "noSuchMealPeriodType";
			return array( False, "noSuchMealPeriodType" );
	}
}

// modifies a meal period type to change it's name
function modify_meal_period_type($oldname, $newname, $locationID)
{
	global $table_name_mp;
	global $table_name_mpt;
	$table_name = $table_name_mpt;

	if ($newname === "")
	{
			echo "nameMustNotBeEmpty";
			return array( False, "Error: the new name must not be empty" );
	}
	
	$mpts_with_same_name = lavu_query("SELECT * FROM `[1]` WHERE `name` = '[2]' AND `_deleted` = '[3]'", $table_name, $newname, "0");

	if (mysqli_num_rows($mpts_with_same_name) === 1)
	{
			echo "MealPeriodTypeAlreadyExists";
			return array( False, "Error: Meal period type already exists" );
	}
	else
	{
			lavu_query("UPDATE `[1]` SET `name` = '[2]' WHERE `name` = '[3]' AND `_deleted` = '[4]'", $table_name, $newname, $oldname, "0");
			if (ConnectionHub::getConn('rest')->affectedRows() > 0)
			{
					// don't forget to update the meal_periods
					$rows = ConnectionHub::getConn('rest')->getDBAPI()->getAllInTable($table_name_mp, array('type_name'=>$oldname, '_deleted'=>'0'));
					for ($i = 0; $i < count($rows); $i++)
					{
							$row = $rows[$i];
							$mp = get_meal_period_from_row($row);
							$mp->change("type_name", $newname, $locationID);
					}
					echo "success";
					sync_table($table_name, $locationID);
					return array( True, "start" );
			}
			else
			{
					echo "noSuchMealPeriodType";
					return array( False, "noSuchMealPeriodType" );
			}
	}
}

function main()
{
	$action = $_GET["action"];
	switch ($action)
	{
	case "create":
			create_meal_period_type("meal_period_types", $_GET["new_name"], $_GET["location"]);
			break;
	case "delete":
			delete_meal_period_type($_GET["old_name"], $_GET["force"], $_GET["location"]);
			break;
	case "modify":
			modify_meal_period_type($_GET["old_name"], $_GET["new_name"], $_GET["location"]);
			break;
	case "get_as_javascript_array":
			echo mpts_as_javascriptarray(meal_period_types()) . ";";
			break;
	default:
			echo "badRequest";
	}
}

main();

?>
