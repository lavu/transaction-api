<?php

//ini_set("display_errors", "1");

{
	$self = $_SERVER["PHP_SELF"];
	$self = explode("/", $self);
	$self = $self[1];
	require "/home/poslavu/public_html/admin/" . $self . "/areas/meal_periods/order_get_meal_period_type_names.php";
}

function save_preference($locationid, $type_of_preference, $preference_name, $preference) {
	$query = FALSE;
	switch($preference_name) {
		case "meal_periods_previous_orders_processed":
			lavu_query("DELETE FROM `config` WHERE `location`='[1]' AND `setting`='[2]'", $locationid, $preference_name);
			$query = lavu_query("INSERT INTO `config` (`location`, `type`, `setting`, `value`) VALUES ('[1]', '[2]', '[3]', '[4]')", $locationid, $type_of_preference, $preference_name, $preference);
			break;
	}
	if ($query === FALSE) {
		return array( FALSE, "badPreferenceName?Bad preference " . $preference_name );
	}
	if (ConnectionHub::getConn('rest')->affectedRows() == 0) {
		return array( FALSE, "badResult?Failed to save preference " .$preference_name. " (" .$preference.")" );
	}
	return array( TRUE, "success" );
}

function process_previous_orders($location) {
	$orders_open = previous_orders_open($location);
	if ($orders_open[1] == "true") { $orders_open = TRUE; } else { $orders_open = FALSE; }
	
	if (!$orders_open) {
		return array( FALSE, "previous orders have already been processed" );
	}
	
	$open_orders_query = lavu_query("SELECT * FROM `orders` WHERE `meal_period_typeid` = ''");
	if ($open_orders_query !== FALSE) {
		save_preference($location, "admin_activity", "meal_periods_previous_orders_processed", "2");
		while ($row = mysqli_fetch_assoc($open_orders_query)) {
			order_get_meal_period_type_names($row["order_id"], $location, TRUE, TRUE);
		}
		save_preference($location, "admin_activity", "meal_periods_previous_orders_processed", "1");
	}
	
	return array( TRUE, "success" );
}

// checks if previous orders have been processed already
// checks (for) the "meal_periods_previous_orders_processed" field in `config`
// @return [ TRUE, "true" ] or [ TRUE, "false" ]
function previous_orders_open($location) {
	$field_name = "meal_periods_previous_orders_processed";
	$field_query = lavu_query("SELECT * FROM `config` WHERE `location`='[1]' AND `setting`='[2]'", $location, $field_name);
	$field = array();
	if ($field_query !== FALSE)
		while ($row = mysqli_fetch_assoc($field_query)) 
			$field = $row;
		
	if (isset($field["value"])) {
		$processed = $field["value"];
		if ($processed == "0") { $processed = FALSE; } else { $processed = TRUE; }
		
		if ($processed) {
			return array( TRUE, "false" );
		} else {
			return array ( TRUE, "true" );
		}
	}
	
	$missing_typeids_query = lavu_query("SELECT * FROM `orders` WHERE `meal_period_typeid` = '' LIMIT 1");
	$missing_typeids = array();
	if ($missing_typeids_query !== FALSE)
		while ($row = mysqli_fetch_assoc($missing_typeids_query)) {
			$missing_typeids = $row; break; }
	if (count($missing_typeids) > 0) {
		save_preference($location, "admin_activity", "meal_periods_previous_orders_processed", "0");
	}
	
	return array ( TRUE, "true" );
}

function main() {
	function get_post($var_name) {
		if (isset($_POST[$var_name]))
			return $_POST[$var_name];
		return "";
	}

	$action = get_post("action");
	$location = get_post("location");
	$result = array( FALSE, "missingLocation?Location of site not known" );
	
	if ($location != "") {
		switch ($action)	{
		case "process_previous_orders":
			$result = process_previous_orders($location);
			break;
		case "previous_orders_open":
			$result = previous_orders_open($location);
			break;
		default:
			$result = array( FALSE, "badRequest?Unrecognized request from client" );
		}
	}
	
	if (gettype($result) == "array" && count($result) > 1) {
		if ($result[0] == TRUE && (strlen($result[1]) == 0)) {
			echo "success";
		} else {
			echo $result[1];
		}
	} else {
		echo "failure";
	}
}

main();

?>
