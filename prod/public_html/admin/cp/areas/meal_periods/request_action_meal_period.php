<?php

require "global_values_and_functions.php";

// does a mysql request to find the id of a given meal_period_type
// returns False if no such meal_period_type is found
// @table_name the database table to pull from (should probably end with ".meal_periods")
function get_meal_period_type_id($table_name, $meal_period_type)
{
	$mpts = meal_period_types($table_name);
	for ($i = 0; $i < count($mpts); $i++)
	{
			if ($mpts[$i]->name === $meal_period_type)
			{
					return $mpts[$i]->id;
			}
	}

	return False;
}

// creates a meal period with the given specs
// dow: days of week, st: start time, et: end time
// @st and et should be in the form "hh:mm" or "hh:mm:['AM','PM']"
// if there's a time conflict between start and end time, reports that and doesn't create
// if there's a time overlap between this and another meal period with the same meal_period_type, reports that and doesn't creat unless force is true
function create_meal_period($meal_period_type, $dow, $days, $st, $et, $do_force, $request_long_time, $locationID)
{
	echo $meal_period_type."…".$dow."…".$days."…".$st."…".$et."…".$do_force."…".$request_long_time;
	// because it is called from multiple parts of the function
	function do_create($table_name, $mpt, $mptid, $dow, $st, $et, $locationID)
	{
		$values = array();
		$values["table_name"] = $table_name;
		$values["mptid"] = $mptid;
		$values["mpt"] = $mpt;
		$values["dow"] = $dow;
		$values["st"] = $st;
		$values["et"] = $et;
		$values["dc"] = date("m/d/y", strtotime("today"));
		lavu_query("INSERT INTO `[table_name]` (`type_id`, `type_name`, `day_of_week`, `start_time`, `end_time`, `date_created`) VALUES ('[mptid]', '[mpt]', '[dow]', '[st]', '[et]', '[dc]')", $values);
		echo "success";
		sync_table($table_name, $locationID);
		return array( TRUE, "start" );
	}

	global $table_name_mp;
	global $table_name_mpt;
	$table_name = $table_name_mp;
	$typeid = get_meal_period_type_id($database . "." . $table_name_mpt, $meal_period_type);
	date_default_timezone_set("America/Denver");
	$st_int = strtotime($st);
	$et_int = strtotime($et);

	// clean up the input
	if (strtolower($do_force) === "true"){$do_force = TRUE;}else{$do_force = FALSE;}
	if (strtolower($request_long_time) === "true"){$do_force = TRUE;}else{$request_long_time = FALSE;}
	if ($typeid === False)
	{
			echo "noSuchMealPeriodType";
			return array( False, "Error: no such meal period type exists" );
	}
	$st = date("H:i", $st_int);
	$et = date("H:i", $et_int);
	if ($dow === "Weekdays"){$days = "MTWRF";}
	if ($dow === "Weekends"){$days = "US";}
	if (str_replace("+", " ", $dow) === "All Days"){$days = "UMTWRFS";}

	// make sure the input is good
	// (check that the times are in the correct order
	//   or that the start time comes after the end time and the end time is <= 6:00AM)
	echo "\n".$st_int.":".$et_int."\n";
	if ($st_int >= $et_int)
	{
			if ( $et_int <= (strtotime("06:00")) )
			{
				$hour = date("H", $et_int);
				$min = date("i", $et_int);
				$hour = $hour + 24;
				$et = $hour . ":" . $min;
				$et_int += 60*60*24;
			}
			else
			{
				echo "startAndEndTimesConflict";
				return array( FALSE, "Error, the end time must come after the start time" );
			}
	}
	echo "\n".$st.":".$et."\n";
	
	// check that the times aren't more than 12 hours appart
	// if they are, echo "timeIsTooLong?12"
	//   unless $request_long_time is true, in which case ignore the extra long times
	{
		$secdif = $et_int - $st_int;
		if (($secdif/3600) > 12 && $request_long_time === FALSE)
		{
			echo "timeIsTooLong?12";
			return array( FALSE, "The difference between the start and end times should not be more than 12 hours" );
		}
	}
	
	// create a test string to search for days of week
	$request_days = "";
	if (strpos((strtoupper($days)), "U") !== FALSE){$request_days=$request_days . "U|";}
	if (strpos((strtoupper($days)), "M") !== FALSE){$request_days=$request_days . "M|";}
	if (strpos((strtoupper($days)), "T") !== FALSE){$request_days=$request_days . "T|";}
	if (strpos((strtoupper($days)), "W") !== FALSE){$request_days=$request_days . "W|";}
	if (strpos((strtoupper($days)), "R") !== FALSE){$request_days=$request_days . "R|";}
	if (strpos((strtoupper($days)), "F") !== FALSE){$request_days=$request_days . "F|";}
	if (strpos((strtoupper($days)), "S") !== FALSE){$request_days=$request_days . "S|";}
	$request_days = rtrim($request_days, "|");

	// check that at least one day is selected
	if (strlen($request_days) < 1)
	{
			echo "numberOfDaysLessThanOne";
			return array( False, "Error: select at least one day in the week" );
	}

	// query the mysql server
	// we are checking that there aren't time conflicts
	// we don't want (start_time,end_time) and (st,et) to overlap
	//   st < start_time < et < end_time
	$where_request = "WHERE `day_of_week` REGEXP '[2]' AND `start_time` >= '[3]' AND `start_time` < '[4]' AND `_deleted` = '[5]'";
	$result = lavu_query("SELECT * FROM `[1]` " . $where_request, $table_name, $request_days, $st, $et, "0");
	//   start_time < st < end_time < et
	if (!$result || mysqli_num_rows($result) == 0)
	{
			$where_request = "WHERE `day_of_week` REGEXP '[2]' AND `end_time` > '[3]' AND `end_time` <= '[4]' AND `_deleted` = '[5]'";
			$result = lavu_query("SELECT * FROM `[1]` " . $where_request, $table_name, $request_days, $st, $et, "0");
	}

	if ($result)
	{
			if ($do_force === FALSE && mysqli_num_rows($result) > 0)
			{
					$mp = get_meal_period_from_row(mysqli_fetch_assoc($result));
					$meal_period_overlapped = "";
					$meal_period_overlapped = $meal_period_overlapped . "Type: " . $mp->type_name . "\n";
					$meal_period_overlapped = $meal_period_overlapped . "Day(s): " . $mp->day_of_week . "\n";
					$meal_period_overlapped = $meal_period_overlapped . "Time: " . $mp->start_time . "-" . $mp->end_time;
					echo "MealPeriodTimeOverlap?" . $meal_period_overlapped;
					return array( False, "Error: Meal period has a time conflict" );
			}
			else
			{
					do_create($table_name, $meal_period_type, $typeid, $days, $st, $et, $locationID);
			}
	}
	else
	{
			do_create($table_name, $meal_period_type, $typeid, $days, $st, $et, $locationID);
	}
}

// sets the sort state
// sort should be one of those found in the "available_sorts" array
function set_sort($sort_by)
{
	$available_sorts = array("Type", "Day", "Starts At", "Ends At");
	
	// check that the input is good
	if (in_array($sort_by, $available_sorts) === FALSE)
	{
		echo "invalidSortType";
		return array( FALSE, "Choose a different sort type" );
	}

	$location = admin_info("loc_id");
	$where_request = "WHERE `location` = '".ConnectionHub::getConn('rest')->escapeString($location)."' AND `setting` = 'meal_periods_sort_by'";
	$a_wherevars = array('location'=>$location, 'setting'=>'meal_periods_sort_by');
	//$rows = query_the_thing_for_rows_yay("config", $where_request, $location);
	$rows = ConnectionHub::getConn('rest')->getDBAPI()->getAllInTable('config', $a_wherevars, FALSE);
	
	$result = FALSE;
	if (count($rows) > 0)
	{
		$result = lavu_query("UPDATE `config` SET `value` = '[1]' " . $where_request, $sort_by, $location);
	}
	else
	{
		$result = lavu_query("INSERT INTO `config` (location, setting, value) VALUES ('[1]', 'meal_periods_sort_by', '[2]')", $location, $sort_by);
	}
	
	// check if the insertion was successful
	if ($result)
	{
		echo "success";
		return array( TRUE, "success" );
	}
	else
	{
		echo "couldNotInsert";
		return array( FALSE, "unable to insert sort_by into config array" );
	}
}

function main()
{
	$action = $_GET["action"];
	switch ($action)
	{
	case "create":
			create_meal_period($_GET["meal_period_type"], $_GET["day_of_week"], $_GET["days"], $_GET["start_time"], $_GET["end_time"], $_GET["do_force"], $_GET["do_request_long_time"], $_GET["location"]);
			break;
	case "delete":
			delete_meal_period("meal_periods", $_GET["meal_period_id"], $_GET["location"]);
			break;
	case "setsort":
			$result = set_sort($_GET["sort_by"]);
			echo $result[1];
			break;
	case "getsort":
			$result = get_sort_meal_periods();
			echo $result[1];
			break;
	case "get_as_table":
			echo meal_period_table();
			break;
	default:
			echo "badRequest";
	}
}

main();

?>
