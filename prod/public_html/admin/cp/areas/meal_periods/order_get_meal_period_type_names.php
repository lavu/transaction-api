<?php

/*******************************************************************************
 * The main function that should be worried about is the order_get_meal_period_type_names($order_id) found bellow
 *******************************************************************************
 * @author: Benjamin Bean (benjamin@poslavu.com)
 *******************************************************************************/

// returns the meal_period_type name and sets the 'meal_period_typeid'
// the value of 'meal_period_typeid' is set to the meal_period_type_id of the first returned meal_period_type name
// @$order_id the id of the order that was placed (eg '1-1')
// @$location_id the id of the location to look for orders from
// @$override if TRUE, then the stored 'meal_period_typeid' of the order will be ignored
// @$store_zeroes if TRUE, then the value '0' is set when a matching meal period can't be found ([ FALSE, "no matching meal period when last processed" ] is returned)
// @return possible return values include:
//   [ TRUE, [meal_period_type_name1, meal_period_type_name2...] ]
//   [ FALSE, "order id not provided" ]
//   [ FALSE, "order id not found" ]
//   [ FALSE, "stored meal period type id not found" ]
//   [ FALSE, "no matching meal periods" ]
//   [ FALSE, "could not edit order 'meal_period_typeid'" ]
//   [ FALSE, "order id not found" ]
//   [ FALSE, "databases aren't synced" ]
//   [ FALSE, "no matching meal period when last processed" ]
function order_get_meal_period_type_names($order_id, $location_id, $override = NULL, $store_zeroes = NULL) {

	$table_name_mp = "meal_periods";
	$table_name_mpt = "meal_period_types";

	if (!$order_id)
		return array( FALSE, "order id not provided" );
	if (strlen($order_id) == 0)
		return array( FALSE, "order id not provided" );

	$rows = array();
	{
		$ids = lavu_query("SELECT * FROM `[1]` WHERE `order_id` = '[2]' AND `location_id` = '[3]'", "orders", $order_id, $location_id);
		if (($ids) !== FALSE)
			if (mysqli_num_rows($ids) > 0)
				while($row = mysqli_fetch_assoc($ids))
					$rows[] = $row;
	}
	if (count($rows) > 0) {
		$row = $rows[0]; // there should only be one

		// check that the meal_period_typeid field exists
		if (!isset($row["meal_period_typeid"])) {
			return array( FALSE, "databases aren't synced" );
		}

		// check that the order doesn't already have a meal_period_typeid
		if ($row["meal_period_typeid"] && !$override) {
			$mptid = $row["meal_period_typeid"];
			if ($mptid === "0" || $mptid === 0) {
				return array( FALSE, "no matching meal period when last processed" );
			}
			if (strlen($mptid) > 0) {
				$mpts = array();
				{
					$ids = lavu_query("SELECT * FROM `[1]` WHERE `id` = '[2]'", $table_name_mpt, $mptid);
					if (($ids) !== FALSE)
						if (mysqli_num_rows($ids) > 0)
							while($row = mysqli_fetch_assoc($ids))
								$mpts[] = $row;
				}
				if (count($mpts) > 0) {
					$mpt = $mpts[0];
					if ($mpt["id"] == $mptid) {
						return array( TRUE, array($mpt["name"]) );
					}
				}
				return array( FALSE, "stored meal period type id not found" );
			}
		}

		// get the stats on the order
		$open_at = $row["opened"];
		$open_time = date("H:i", strtotime($open_at));
		// day
		$day = strtolower(date("D", strtotime($open_at)));
		switch($day) {
		case "sun":
			$day = "U";
			break;
		case "mon":
			$day = "M";
			break;
		case "tue":
			$day = "T";
			break;
		case "wed":
			$day = "W";
			break;
		case "thu":
			$day = "R";
			break;
		case "fri":
			$day = "F";
			break;
		case "sat":
			$day = "S";
			break;
		}

		// find the corresponding meal_period_type
		$mps = array();
		{
			$ids = lavu_query("SELECT * FROM `[table_name_mp]` WHERE LOCATE('[day]', `day_of_week`) AND `_deleted` = '0' AND `start_time` <= '[open_time]' AND `end_time` > '[open_time]'", array('table_name_mp'=>$table_name_mp, 'day'=>$day, 'open_time'=>$open_time));
			if (($ids) !== FALSE)
				if (mysqli_num_rows($ids) > 0)
					while($row = mysqli_fetch_assoc($ids))
						$mps[] = $row;
		}
		$retval = array();
		$mptid = NULL;
		foreach($mps as $mp) {
			if (!in_array($mp["type_name"], $retval)) {
				$retval[] = $mp["type_name"];
			}
			if ($mptid == NULL) {
				$mptid = $mp["type_id"];
			}
		}
		if (count($retval) == 0) {
			$store_zeroes = TRUE;
			if ($store_zeroes) {
				lavu_query("UPDATE `orders` SET `meal_period_typeid` = '[1]' WHERE `order_id` = '[2]' AND `location_id` = '[3]' LIMIT 1", "0", $order_id, $location_id);
				return array( FALSE, "no matching meal period when last processed" );
			}
			return array( FALSE, "no matching meal periods" );
		}
		lavu_query("UPDATE `orders` SET `meal_period_typeid` = '[1]' WHERE `order_id` = '[2]' AND `location_id` = '[3]' LIMIT 1", $mptid, $order_id, $location_id);
		if (ConnectionHub::getConn('rest')->affectedRows() < 1) {
			return array( FALSE, "could not edit order 'meal_period_typeid'" );
		}
		return array( TRUE, $retval );
	}

	return array( FALSE, "order id not found" );
}

?>
