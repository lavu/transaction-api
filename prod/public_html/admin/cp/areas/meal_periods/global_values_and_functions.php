<?php

/*******************************************************************************
 * These are the core functions of the meal periods
 *******************************************************************************
 * @author: Benjamin Bean (benjamin@poslavu.com)
 *******************************************************************************/

//ini_set("display_errors", "1");

$table_name_mp = "meal_periods";
$table_name_mpt = "meal_period_types";

// These pages are the possible selections for useful pages to load
// Should be the same as the name for input submit buttons
// There is an order on these pages. The later pages can't be displayed
//   if certain requirements aren't met (and it falls back to an earlier
//   page).
$meal_period_valid_pages = array(
    "start_no_meal_period_types",
    "create_new_meal_period_type",
    "modify_meal_period_type",
    "start_no_meal_periods",
    "create_new_meal_period",
    "start",
    "start2",
//    "list_meal_periods",
);

// These screens are always able to be displayed
// override screen have no prerequisites to being drawn
$meal_periods_override_screens = array(
	"back_button",
	"start",
	"start2",
);

class t_meal_period_type
{
	public $id;
	public $name;

	function print_as_option($set_as_selected)
	{
		$selected = "";
		if ($set_as_selected){$selected = " selected=\"selected\"";}
		return "<option" . $selected . ">" . $this->name . "</option>";
	}
}

class t_meal_period
{
	public $id;
	public $type_id;
	public $type_name;
	public $day_of_week;
	public $start_time;
	public $end_time;

	function change($field, $newvalue, $locationID)
	{
		global $table_name_mp;
		$table_name = $table_name_mp;

		if (isset($this->$field))
		{
				$this->$field = $newvalue;
				lavu_query("UPDATE `[1]` SET `[2]` = '[3]' WHERE `id` = '[4]'", $table_name, $field, $newvalue, $this->id);
				sync_table($table_name, $locationID);
		}
	}

	function format_pretty($what)
	{
		switch ($what)
		{
			case "day_of_week":
				$retval = "";
				if (strpos($this->$what, "U") !== FALSE){$retval = $retval . "S";}else{$retval = $retval . "_";}
				if (strpos($this->$what, "M") !== FALSE){$retval = $retval . "M";}else{$retval = $retval . "_";}
				if (strpos($this->$what, "T") !== FALSE){$retval = $retval . "T";}else{$retval = $retval . "_";}
				if (strpos($this->$what, "W") !== FALSE){$retval = $retval . "W";}else{$retval = $retval . "_";}
				if (strpos($this->$what, "R") !== FALSE){$retval = $retval . "T";}else{$retval = $retval . "_";}
				if (strpos($this->$what, "F") !== FALSE){$retval = $retval . "F";}else{$retval = $retval . "_";}
				if (strpos($this->$what, "S") !== FALSE){$retval = $retval . "S";}else{$retval = $retval . "_";}
				return $retval;
		}
	}
}

// sorts an array by the given field name
// from http://php.net/manual/en/language.types.object.php#50815
function sort_array_by_field($objs, $field)
{
	$func = "return (\$o1->" . $field . " < \$o2->" . $field . ") ? -1 : 1;";
	usort($objs,
		  create_function("\$o1,\$o2", $func));

	return $objs;
}

function sync_table($table_name, $locationID) {
	require_once(dirname(dirname(dirname(__FILE__))).'/resources/core_functions.php');
	$get_location_info = lavu_query("SELECT `use_net_path`, `net_path` FROM `locations` WHERE `id`= '[1]'", $locationID);
	if (mysqli_num_rows($get_location_info) > 0) {
		$location_info = mysqli_fetch_assoc($get_location_info);
		schedule_remote_to_local_sync_if_lls($location_info, $locationID, "table updated", $table_name);
	}
}

// Loads the meal period types for the session and then returns
// them as an array
function meal_period_types()
{
//	static $mtps = NULL;
	global $table_name_mpt;
	$mtps = NULL;
	if ($mtps == NULL)
	{
		$rows = ConnectionHub::getConn('rest')->getDBAPI()->getAllInTable($table_name_mpt, array('_deleted'=>'0'));
		for ($i = 0; $i < count($rows); $i++)
		{
				$row = $rows[$i];
				$mtp = new t_meal_period_type();
				$mtp->id = $row['id'];
				$mtp->name = $row['name'];
				$mtps[$i] = $mtp;
		}
	}
	return $mtps;
}

// given a row from a mysql request, it will return a t_meal_period
function get_meal_period_from_row($row)
{
	$mp = new t_meal_period();
	$mp->id = $row['id'];
	$mp->type_id = $row['type_id'];
	$mp->type_name = $row['type_name'];
	$mp->day_of_week = $row['day_of_week'];
	$mp->start_time = $row['start_time'];
	$mp->end_time = $row['end_time'];
	return $mp;
}

// @$mpts should be an array of meal_period_objects
function mpts_as_javascriptarray($mpts) {
	if (!$mpts)
		return "[]";
	if (count($mpts) <= 0)
		return "[]";

	$retval = "[";
	$add_comma = FALSE;
	foreach($mpts as $mp) {
		if ($add_comma)
			$retval .= ",";
		else
			$add_comma = TRUE;
		$retval .= "[\"".$mp->name."\",".$mp->id."]";
	}
	$retval .= "]";

	return $retval;
}

// Loads the meal periods for the session and then returns
// them as an array
function meal_periods()
{
//	static $mps = NULL;
	global $table_name_mp;
	$mps = NULL;
	if ($mps == NULL)
	{
		$rows = ConnectionHub::getConn('rest')->getDBAPI()->getAllInTable($table_name_mp, array('_deleted'=>'0'));
		for ($i = 0; $i < count($rows); $i++)
		{
				$row = $rows[$i];
				$mp = get_meal_period_from_row($row);
				$mps[$i] = $mp;
		}
	}
	return $mps;
}

// turns a meal period into a table row
// format:
//   .*"%MEALPERIODSASTABLEROWS%"[arg*]
//   arg:
//     field_name: the name of the field of the meal period that is wished to be included
//     ['literal text']arg['literal text']
// @line a line that should be in the given format
// @table the database table to pull from (should probably be "meal_periods")
// @sort_by the field name to sort by
// @return a line with all of the meal periods in [<tr>meal_period_data</tr>]* format
function translate_meal_period_as_table_rows($line, $table, $sort_by = "")
{
	$beggining = explode("%MEALPERIODSASTABLEROWS%", $line);
	$beggining = $beggining[0];
	$options = explode("%MEALPERIODSASTABLEROWS%", $line);
	$options = $options[1];
	$options = explode("&", $options);
	$line = $beggining;
	$mps = meal_periods($table);

	// make sure that we have meal periods
	if (count($mps) <= 0)
	{
		return "";
	}

	date_default_timezone_set("America/Denver");

	// for things that need to be sorted the same way they are displayed
	foreach($mps as $mp)
	{
		// because we are displaying the day_of_week as pretty,
		// they also need to be sorted in the way they are being displayed
		$mp->day_of_week = $mp->format_pretty("day_of_week");
		// because the end time can be anwhere between 00:15 and 30:00
		{
			$parts = explode(":", $mp->end_time);
			$et = "";
	    	if ($parts[0] >= 24)
	    	{
	    		$hour = $parts[0] - 24;
	    		while ($hour >= 24)
	    		{
		    		$hour = $hour - 24;
	    		}
	    		$min = date("i",strtotime($mp->end_time));
	    		$et = date("H:i", strtotime($hour . ":" . $min));
	    	}
	    	else
	    	{
	    		$et = $mp->end_time;
	    	}
	    	$mp->end_time = date("H:i", strtotime($et));
		}
	}

	// sanitize the input
	if ($sort_by === "")
	{
			$result = get_sort_meal_periods();
			if ($result[0] === TRUE)
			{
				$sort_by = $result[1];
			}
			else
			{
				$sort_by = "Type";
			}
	}
	switch ($sort_by)
	{
	case "Type":
			$sort_by = "type_name";
			break;
	case "Day":
			$sort_by = "day_of_week";
			break;
	case "Starting Time":
	case "Starts At":
			$sort_by = "start_time";
			break;
	case "Ending Time":
	case "Ends At":
			$sort_by = "end_time";
			break;
	default:
			$sort_by = "type_name";
	}
	$mps = sort_array_by_field($mps, $sort_by);

	// translates the commands in the html into properties of the meal period objects
	function translate_meal_period_option($opt, $mp, $sort_by)
	{
		$line = "";
		switch($opt)
		{
		case "id":
				$line = $line . $mp->id;
				break;
		case "type_id":
				$line = $line . $mp->type_id;
				break;
		case "type_name":
				$line = $line . $mp->type_name;
				break;
		case "day_of_week":
				$line = $line . $mp->day_of_week;
				break;
		case "start_time":
				$line = $line . date("h:i A", strtotime($mp->start_time));
				break;
		case "end_time":
				$parts = explode(":", $mp->end_time);
				$et = "";
				if ($parts[0] > 24)
				{
					$hour = $parts[0] - 24;
					$min = date("i",strtotime($mp->end_time));
					$et = date("H:i", strtotime($hour . ":" . $min));
				}
				else
				{
					$et = $mp->end_time;
				}
				$line = $line . date("h:i A", strtotime($et));
				break;
		default:
				$splits = explode("'", $opt);
				for ($i = 1; $i < count($splits); $i++)
				{
						if ($i % 2 == 1)
						{
								$line = $line . $splits[$i];
						}
						else
						{
								$line = $line . translate_meal_period_option($splits[$i], $mp, $sort_by);
						}
				}
		}
		return $line;
	}

	$odd = TRUE;
	for ($mpi = 0; $mpi < count($mps); $mpi++)
	{
			$bgcolor = "";
			if ($odd){$bgcolor = "background-color:#FFF;";}
			$mp = $mps[$mpi];
			$line = $line . "<tr>";
			for ($i = 0; $i < count($options); $i++)
			{
					$line = $line . "<td";
					$opt = $options[$i];
					$default_style = $bgcolor . "padding:0px 5px 0px 5px;";
					if (strpos($opt, "time") !== False)
					{
							$line = $line . " style=\"text-align:right;" . $default_style . "\">";
					}
					else if (strpos($opt, "day_of_week") !== FALSE)
					{
							$line = $line . " style=\"font-family:'courier',monospace;" . $default_style . "\">";
					}
					else
					{
							$line = $line . " style=\"" . $default_style . "\">";
					}
					$line = $line . translate_meal_period_option($opt, $mp, $sort_by);
					$line = $line . "</td>";
			}
			$line = $line . "</tr>";
			$odd = !$odd;
	}

	return $line;
}

function meal_period_table() {
	$retval = "";

	$retval .= '<table style="border-style:none; border-width:0px; width:500px; margin:10px; padding:0px; text-align:center;">';
	$retval .= '<tr>';
	$retval .= '<td style="border-style:solid;border-width:0px 0px 0px 0px;"><a href="#meal_period_listings_td" id="sort1" onclick="sortMPs(\'Type\');" class="unchanging_link">Type</a></td>';
	$retval .= '<td style="border-style:solid;border-width:0px 0px 0px 0px;"><a href="#meal_period_listings_td" id="sort2" onclick="sortMPs(\'Day\');" class="unchanging_link">Day</a></td>';
	$retval .= '<td style="border-style:solid;border-width:0px 0px 0px 0px;"><a href="#meal_period_listings_td" id="sort3" onclick="sortMPs(\'Starts At\');" class="unchanging_link">Starts At</a></td>';
	$retval .= '<td style="border-style:solid;border-width:0px 0px 0px 0px;"><a href="#meal_period_listings_td" id="sort4" onclick="sortMPs(\'Ends At\');" class="unchanging_link">Ends At</a></td>';
	$retval .= '<td style="border-style:solid;border-width:0px 0px 0px 0px;">Delete Meal Period</td>';
	$retval .= '</tr>';
	$retval .= translate_meal_period_as_table_rows("%MEALPERIODSASTABLEROWS%type_name&day_of_week&start_time&end_time&'<input type=\"button\" onclick=\"deleteMP('id');\" value=\"Delete\">'", "meal_periods");
	$retval .= '</table>';

	return $retval;
}

// @table_name the database table to pull from (should probably end with ".meal_periods")
function delete_meal_period($table_name, $id, $locationID)
{
	$id = ConnectionHub::getConn('rest')->escapeString($id);

	$result = lavu_query("UPDATE `[1]` SET `_deleted` = '[2]', `date_deleted` = '[3]' WHERE `id` = '[4]'", $table_name, "1", date("m/d/y", strtotime("today")), $id);

	if (ConnectionHub::getConn('rest')->affectedRows() > 0)
	{
			echo "success";
			sync_table($table_name, $locationID);
			return array( TRUE, "success" );
	}
	else
	{
			echo "noSuchMealPeriod";
			return array( FALSE, "Error: no such meal periods exists" );
	}
}

// get the sort setting for the current location
// possible values are listed in request_action_meal_period > set_sort > available_sorts
// @return array( TRUE | FALSE, VALUE | "error message" )
function get_sort_meal_periods()
{
	$location = admin_info("loc_id");
	$rows = ConnectionHub::getConn('rest')->getDBAPI()->getAllInTable('config', array('location'=>$location, 'setting'=>'meal_periods_sort_by', '_deleted'=>'0'));

	if (count($rows) > 0)
	{
		return array( TRUE, $rows[0]['value'] );
	}
	else
	{
		return array( FALSE, "failure: unable to find property 'meal_period_sort_by' in 'config' for location $location" );
	}
}

?>
