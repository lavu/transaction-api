<?php

	if ($in_lavu)
	{
		$first_letter = (isset($_GET['fl']))?$_GET['fl']:"A";
		$letter_list = array("A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z", "Other");

		$word_filter = "";
		if ($first_letter == "Other")
		{
			foreach ($letter_list as $letter)
			{
				if ($letter != "Other")
				{
					$word_filter .= " AND `word_or_phrase` NOT LIKE '".$letter."%'";
				}
			}
		}
		else
		{
			$word_filter = " AND `word_or_phrase` LIKE '".$first_letter."%'";
		}

		//LP-10020, updated query to add `tag` value to custom_dictionary
		$check_main_dictionary = mlavu_query("SELECT `id`, `word_or_phrase`, `tag` FROM `poslavu_MAIN_db`.`language_dictionary` WHERE `pack_id` = '1' AND `_deleted` != '1'".$word_filter);
		while ($main_entry = mysqli_fetch_assoc($check_main_dictionary))
		{
			$m_id	= $main_entry['id'];
			$m_word	= $main_entry['word_or_phrase'];
			$m_tag = $main_query['tag'];

			$check_custom = lavu_query("SELECT `id`, `word_or_phrase` FROM `custom_dictionary` WHERE `english_id` = '[1]' AND `loc_id` = '[2]'", $m_id, $locationid);
			if (mysqli_num_rows($check_custom) > 0)
			{
				$custom_entry = mysqli_fetch_assoc($check_custom);
				if ($custom_entry['word_or_phrase'] != $m_word)
				{
					lavu_query("UPDATE `custom_dictionary` SET `word_or_phrase` = '[1]', `tag` = '[2]' WHERE `id` = '[3]'", $m_word, $m_tag, $custom_entry['id']);
				}
			}
			else
			{
				lavu_query("INSERT INTO `custom_dictionary` (`word_or_phrase`, `loc_id`, `english_id`, `tag`) VALUES ('[1]', '[2]', '[3]', '[4]')", $m_word, $locationid, $m_id, $m_tag);
			}
		}

		$should_reload = false;
		if (isset($_GET['fill_with']))
		{
			//Commneted below line because we need tag value for each rows. Refernce Ticket LP-10020
			/*
			if ($_GET['fill_with'] == "1")
			{
				$update_custom_pack = lavu_query("UPDATE `custom_dictionary` SET `translation` = '' WHERE `loc_id` = '[1]'", $locationid);
			}
			else
			{
			*/
				$get_main_pack = mlavu_query("SELECT `translation`, `english_id`, `tag` FROM `poslavu_MAIN_db`.`language_dictionary` WHERE `pack_id` = '[1]'", $_GET['fill_with']);
				while ($pack_info = mysqli_fetch_assoc($get_main_pack))
				{
					$update_custom_pack = lavu_query("UPDATE `custom_dictionary` SET `translation` = '[1]', `tag` = '[2]' WHERE `loc_id` = '[3]' AND `english_id` = '[4]'", $pack_info['translation'], $pack_info['tag'], $locationid, $pack_info['english_id']);
				}
			//}
			$should_reload = true;
		}

		$form_posted = (isset($_POST['posted']));
		if ($form_posted)
		{
			$count = 1;
			while (isset($_POST['translation_'.$count]))
			{
				if (isset($_POST['rowid_'.$count]))
				{
					$setval = "";
					if ($_POST['translation_'.$count] != "Translation")
					{
						$setval = $_POST['translation_'.$count];
					}
					$update_custom = lavu_query("UPDATE `custom_dictionary` SET `translation` = '[1]' WHERE `id` = '[2]'", $setval, $_POST['rowid_'.$count]);
				}
				$count++;
			}
			$should_reload = true;
		}

		if ($should_reload)
		{
			global $active_language_pack_id;
			deleteAllLanguagePack(0, $locationid);
			unset_sessvar("active_language_pack");
			$active_language_pack = load_language_pack("0", $locationid);
			$active_language_pack_id = 0;
			setLanguageDictionary($active_language_pack, $locationid);
			//set_sessvar("active_language_pack", $active_language_pack);
		}

		echo "<br><br>
		<table cellspacing='0' cellpadding='0' width='800px'>
			<tr><td align='left'>".speak("You may define your own custom language pack for Lavu POS.")." ".speak("When your location is set to use your custom language pack, any words or phrases found throughout the app from the left column will be replaced with the translations you define on the right.")."</td></tr>
		</table><br>";

		echo "<script language='javascript'>var languagePacks = new Array();</script>";

		echo speak("Fill in custom language pack with primary language pack").": <select id='select_language' onchange='if (document.getElementById(\"select_language\").value != \"\") { if (confirm(\"".speak("Are you sure you want to fill your custom language pack in with")." \" + languagePacks[document.getElementById(\"select_language\").value] + \"?\")) { window.location = \"index.php?mode=settings_custom_language&fill_with=\" + document.getElementById(\"select_language\").value; } else { document.getElementById(\"select_language\").value = \"\"; } }'>";
		echo "<option value=''>".speak("Select Language")."</option>";

		$get_language_packs = mlavu_query("SELECT `id`, `language` FROM `poslavu_MAIN_db`.`language_packs` WHERE `_deleted` != '1' ORDER BY `language` ASC");
		while ($pack_info = mysqli_fetch_assoc($get_language_packs))
		{
			echo "<option value='".$pack_info['id']."'>".$pack_info['language']."</option>";
			echo "<script language='javascript'>languagePacks[".$pack_info['id']."] = '".$pack_info['language']."';</script>";
		}

		echo "</select><br><br>";

		echo "Words or phrases starting with: &nbsp;";
		foreach ($letter_list as $letter)
		{
			if ($letter != $first_letter)
			{
				echo "<a href='".$_SERVER['REQUEST_URI']."&fl=".$letter."'>".$letter."</a>&nbsp;&nbsp;";
			}
			else
			{
				echo $letter."&nbsp;&nbsp";
			}
		}

		echo "<br><br>";

		$display = "<table cellspacing='0' cellpadding='0'>
			<tr>
				<td align='center'>
					<form method='post' action=''>
						<input type='submit' value='Save' style='width:120px'>
						<input type='hidden' name='posted' value='1'>
						<table cellspacing=0 cellpadding=0>";

		$get_custom = lavu_query("SELECT `id`, `word_or_phrase`, `translation` FROM `custom_dictionary` WHERE `loc_id` = '[1]'".$word_filter." ORDER BY `word_or_phrase` ASC", $locationid);
		$count = 0;
		while ($custom_entry = mysqli_fetch_assoc($get_custom))
		{
			$count++;
			if ($custom_entry['translation'] != "")
			{
				$setval = $custom_entry['translation'];
				$color = "color:#1c591c;";
			}
			else
			{
				$setval = "Translation";
				$color = "color:#aaaaaa;";
			}

			$display .= "<tr>
				<td><textarea alt='Word or Phrase' title='Word or Phrase' type='text' name='word_or_phrase_".$count."' style='color:#1c591c; font-size: 15px;' rows='3' cols='60' onfocus='this.style.color = \"#1c591c\"; if(this.value==\"Word or Phrase\") this.value = \"\"; ' onblur='if(this.value==\"\") {this.value = \"Word or Phrase\"; this.style.color = \"#aaaaaa\";} ' readonly='readonly'>".$custom_entry['word_or_phrase']."</textarea></td>
				<td><textarea alt='Translation' title='Translation' type='text' name='translation_".$count."' style='".$color." font-size: 15px;' rows='3' cols='60' onfocus='this.style.color = \"#1c591c\"; if(this.value==\"Translation\") this.value = \"\"; ' onblur='if(this.value==\"\") {this.value = \"Translation\"; this.style.color = \"#aaaaaa\";} '>".$setval."</textarea></td>
				<input type='hidden' name='rowid_".$count."' value='".$custom_entry['id']."'>
			</tr>";
		}
		$display .= "</table>
						<input type='submit' value='Save' style='width:120px'><br>&nbsp;
					</form>
				</td>
			</tr>
		</table>";

		echo $display;
	}
?>
