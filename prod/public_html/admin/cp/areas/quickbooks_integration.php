<?php


	require_once(dirname(__FILE__)."/../resources/json.php");


	$pathToQBAPI = getQBsAPIPathFromDB(admin_info('dev')); //Currently https://admin/(dev)/lib/quickbooks/qbapi/
	$versionPath = "qbv1/";
	$fullPathToAPIURI = $pathToQBAPI.$versionPath."qb_api_service/QBWCService.php";


	if(!empty($_REQUEST['ajax_create_or_get_qb_account_row_from_qb_server'])){
		$api_response_str = createOrGetQBAccountRowFromQBServer();
		$api_response_arr = LavuJson::json_decode($api_response_str,1);

		$userHelpPage = $pathToQBAPI.$versionPath.'user_help.php';
		$QBWXCML_file = '<?xml version="1.0"?>'.
						'<QBWCXML>'.
							//'<AppName>'.$locinfo['title'].' ('.$data_name.') User: '.$api_response_arr['account_row']['user_name'].' - Lavu Quickbooks App '.date('Y-m-d H:i:s').'</AppName>'.
							'<AppName>Lavu account '.$data_name.' quickbooks integration for '.$api_response_arr['account_row']['user_name'].'</AppName>'.
							'<AppID>'.$data_name.'</AppID>'.
							//'<AppURL>https://qbook.eventrentalsystems.com/qbv1/quickbooks/qb_api_service/QBWCService.php?dn='.$data_name.'&amp;user='.$api_response_arr['account_row']['user_name'].'</AppURL>'.
							'<AppURL>'.$fullPathToAPIURI.'?dn='.$data_name.'&amp;user='.$api_response_arr['account_row']['user_name'].'</AppURL>'.
							'<AppDescription>Sync POS orders down as Quickbooks invoices.</AppDescription>'.
							'<AppSupport>'.$userHelpPage.'</AppSupport>'.
							'<UserName>'.$api_response_arr['account_row']['user_name'].'</UserName>'.
							'<OwnerID>{DED8219E-5D47-A3F2-ADDE-36AD18A56DE7}</OwnerID>'.
							'<FileID>'.$api_response_arr['account_row']['file_id'].'</FileID>'.
							'<QBType>QBFS</QBType>'.
							'<Scheduler>'.
								'<RunEveryNMinutes>1</RunEveryNMinutes>'.
							'</Scheduler>'.
							'<IsReadOnly>false</IsReadOnly>'.
						'</QBWCXML>';

		header('Content-disposition: attachment; filename='.$data_name.'.qwc');
		header ("Content-Type:text/qwc");
		echo $QBWXCML_file;
		exit;
	}





	$forward_to = "index.php?mode=$mode";
	$tablename = "locations";
	$rowid = $locationid;

	$fields = array();
	$fields[] = array(speak("Quicbooks Integration"),"Quickbooks Integration","seperator");
	$fields[] = array(speak("Income Account Full Name"), "config:quickbooks_default_income_account_ref_full_name", "text");
	$fields[] = array(speak("COGS Account Full Name"), "config:quickbooks_default_cogs_account_ref_full_name", "text");
	$fields[] = array(speak("Asset Account Full Name"), "config:quickbooks_default_asset_account_ref_full_name", "text");
	$fields[] = array(speak("Accounts Receivable Account Full Name"), "config:quickbooks_default_accounts_receivable_full_name", "text");
	$fields[] = array(speak("Customer Full Name"), "config:quickbooks_default_customer_ref_full_name", "text");
	$fields[] = array(speak("Zero Tax Full Name"), "config:quickbooks_default_zero_tax_ref_full_name", "text");
	$fields[] = array(speak("Default Item Sales Tax Full Name"), "config:quickbooks_default_item_sales_tax_ref_full_name", "text");
	$fields[] = array(speak("Subtotal Item Full Name"), "config:quickbooks_default_subtotal_ref_full_name", "text");
	$fields[] = array(speak("Discount Item Full Name"), "config:quickbooks_default_discount_ref_full_name", "text");
	$fields[] = array(speak("Menu Items Parent Item Full Name"), "config:quickbooks_default_menu_items_parent", "text");
	$fields[] = array(speak("Auto Gratuity Item Full Name"), "config:quickbooks_default_autogratuity_ref_full_name", "text");
	$fields[] = array(speak("Submit"),"submit","submit");

	require_once(resource_path() . "/form.php");

	$sectionAndMode = $section.'_'.$mode;

	$quickBooksQWCFileDownloadHTML = <<<EQBsQWCFDH
	<script type='text/javascript'>
		function generate_download_qwc_file(){
			var username_input = document.getElementById('un_input_id').value;
			var password_input = document.getElementById('pw_input_id').value;
			var xmlhttp;
			if(window.XMLHttpRequest){
				xmlhttp=new XMLHttpRequest;
			}
			else{
				xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
			}
			xmlhttp.onreadystatechange=function(){
				if(xmlhttp.readyState==4 && xmlhttp.status==200){
					alert('got response:' + xmlhttp.responseText);
				}
			}
			window.location = "?mode={$sectionAndMode}&ajax_create_or_get_qb_account_row_from_qb_server=1&username_input=" + username_input + "&password_input=" + password_input;
		}
	</script>
	<div style='border:solid 1px black; display:inline-block'>
		<input type='button' onclick='generate_download_qwc_file()' value='Generate .qwc File'/>
		<input type='text' id='un_input_id' placeholder='username'/>
		<input type='text' id='pw_input_id' placeholder='password'/>
	</div>
EQBsQWCFDH;

	echo "<br>Download .qwc file with Quickbook's user's username and password<br>";
	echo $quickBooksQWCFileDownloadHTML;

	if (is_lavu_admin()) echo "<br>".$lavu_only."Indicates items visible only to Lavu Admin users (i.e., developers and support staff)";





	function createOrGetQBAccountRowFromQBServer(){
		global $data_name;
		global $pathToQBAPI;
		global $versionPath;

		$postArray['data_name'] = $data_name;
		$postArray['server_name'] = 'lavu';
		$postArray['function'] = 'add_or_get_account';
		$postArray['func_args'] = '{"user_name":"'.$_REQUEST['username_input'].'","password":"'.$_REQUEST['password_input'].'","qbw_file_path":""}'; //TODO MAKE PW VARIABLE, PUT TEXT INPUT INTO PAGE FOR IT!
		$postArray['security_key'] = '915204710443208127187736920';
		$postArray['security_token'] = '271602091522081443877390471';
		$postArray['security_hash'] = sha1( $postArray['server_name'] . $postArray['function'] . $postArray['data_name'] . $postArray['security_token'] );
		$postArray['response_url'] = '';

		$postData = http_build_query($postArray);

		///$url = 'https://qbook.eventrentalsystems.com/qbv1/quickbooks/qb_api_service/qbapi.php';
		$url = $pathToQBAPI.$versionPath."qb_api_service/qbapi.php";

		$ch = curl_init($url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_POST, true);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $postData);
		$api_call_response = curl_exec($ch);
		//error_log("API RESPONSE:" . $api_call_response);
		curl_close($ch);
		return $api_call_response;
	}


	function getQBsAPIPathFromDB($isDev){
	    if($isDev){
    	   $result = mlavu_query("SELECT * FROM `poslavu_LAVU_QUICKBOOKS_db`.`config` WHERE `SETTING`='qb_api_server_url_dev'");
        }else{
           $result = mlavu_query("SELECT * FROM `poslavu_LAVU_QUICKBOOKS_db`.`config` WHERE `SETTING`='qb_api_server_url'");
        }
        if(!$result){ error_log("MySQL error in ".__FILE__." -98w45h- error: ".mlavu_dberror()); } //TODO javascript alert error or something.
        if(!mysqli_num_rows($result)){ error_log("Could not find quickbooks api uri from db. In file ".__FILE__.""); }
        $row = mysqli_fetch_assoc($result);
        return $row['value'];
	}



?>
