<?php

	/********************************************************************************
	 * This file is used by the user of a restaurant to joing chains, remove restaurants
	 * from a chain, set the sync status of menu items and menu-related item, and
	 * anything else user-related.
	 *
	 * For ajax calls, use the chainAjax class.
	 *
	 * For syncing menu items in the database, use the syncFunc class.
	 *
	 * Author: Benjamin G. Bean
	 * benjamin@poslavu.com
	 ********************************************************************************/

	if(session_id()==''){
		session_start();
	}
	require_once(dirname(dirname(dirname(__FILE__))).'/sa_cp/manage_chain_functions.php');
	require_once(dirname(dirname(dirname(__FILE__))).'/sa_cp/billing/payment_profile_functions.php');
	require_once(dirname(__FILE__).'/login_functions.php');
	require_once(dirname(__FILE__).'/../resources/core_functions.php');
	require_once(dirname(__FILE__).'/../objects/json_decode.php');

	// return the html for either:
	//     a form to create a new chain or join an existing chain
	// or
	//     the name of the restaurant's current chain
	function drawCreateJoinChain($s_dataname) {
		$a_chain = get_restaurant_chain($s_dataname);
		$s_retval = '
				<style type="text/css">
					.grouping {
						border: 1px solid #888;
						border-radius: 3px;
						background-color: #eceeef;
						padding: 10px;
					}
					.smallSaveBtn {
						padding-left: 20px;
						padding-right: 20px;
					}
					.saveBtn {
						margin: 10px;
					}
					.dividingRule {
						width: 300px;
						color: #888;
					}
				</style>';

		if (count($a_chain) == 0) {
			$s_retval = drawCreateChain($s_dataname, $a_chain, $s_retval);
		} else {
			$s_retval = drawJoinChain($s_dataname, $a_chain, $s_retval);
		}
		return $s_retval;
	}

	// helper function of drawCreateJoinChain()
	// returns the html to join a chain
	function drawJoinChain($s_dataname, &$a_chain, $s_retval) {
		$a_restaurants = get_restaurants_by_chain($a_chain['id']);
		$s_retval .= '
			<font style="font-weight:bold;">Chain information:</font>
			<div class="grouping">
				This restaurant is part of the chain<br />
				<hr class="dividingRule" size="1px">
				'.$a_chain['name'].'
			</div>
			<br />';
		if (count($a_restaurants) > 0) {
			$s_retval .= '
			<div class="grouping">
				Restaurants in this chain include: <br />
				<hr class="dividingRule" size="1px">';
			foreach($a_restaurants as $a_restaurant)
				if ($a_restaurant['id'] == $a_chain['primary_restaurantid'])
					$s_retval .= '
				'.$a_restaurant['company_name'].' (primary)<br />';
			foreach($a_restaurants as $a_restaurant)
				if ($a_restaurant['id'] != $a_chain['primary_restaurantid'])
					$s_retval .= '
				'.$a_restaurant['company_name'].'<br />';
			$s_retval .= '
			</div>
			<br />';
		}
		$s_retval .= '
			<font style="font-weight:bold;">Chain configuration:</font>
			<div class="grouping">
				Reports can be configured to show only specified restaurants in your chain.
				<form method="get">
					<input type="hidden" name="mode" value="settings_configure_reports" />
					<input type="hidden" name="return_mode" value="reports_menu" />
					<input type="submit" class="saveBtn" value="Configure" />
				</form>
			</div>
			<br />
			<font style="font-weight:bold;">Remove this restaurant:</font>
			<div class="grouping">
				<form id="removeChainForm">
					<input type="hidden" name="command" value="remove_chain" />
					<input type="hidden" name="chain_management_ajax" value="1" />
					Remove this restaurant from the chain.<br />
					<input type="button" name="posted" value="Remove" class="saveBtn" onclick="if (confirm(\'Are you sure you want to remove this restaurant from the chain '.$a_chain['name'].'?\')) { chain.send_ajax_call_from_form(\'areas/chain_management.php\', \'removeChainForm\'); } " /><br />
					<label class="errors"></label>
				</form>
			</div>';
		return $s_retval;
	}

	// helper function of drawCreateJoinChain()
	// returns the html to create a chain
	function drawCreateChain($s_dataname, &$a_chain, $s_retval) {
		$a_restaurant = get_restaurant_from_dataname($s_dataname);
		if (count($a_restaurant) == 0)
			return 'Error: this restaurant couldn\'t be found.';
		$s_retval .= '
			<div class="grouping">
				This restaurant is not currently part of a chain.<br />
				You can...
			</div>
			<br />
			<font style="font-weight:bold;">Create a new chain:</font>
			<div class="grouping">
				<form id="createChainForm">
					<input type="hidden" name="command" value="create_chain" />
					<input type="hidden" name="chain_management_ajax" value="1" />
					Chain name: <input type="textarea" name="new_chain_name" value="'.find_unused_chain_name($a_restaurant['company_name']).'" /><br />
					<input type="button" name="posted" value="'.speak('Create Chain').'" onclick="chain.send_ajax_call_from_form(\'areas/chain_management.php\', \'createChainForm\');" /><br />
					<label class="errors"></label>
				</form>
			</div>
			<br />
			<font style="font-weight:bold;">Find an existing chain:</font>
			<div class="grouping">
				<form id="findChainForm" onkeypress="if (event.keyCode == 13) { $(this).find(\'input[type=button]\').click(); }">
					<input type="hidden" name="command" value="get_chain_name" />
					<input type="hidden" name="chain_management_ajax" value="1" />
					Lavu login: <input type="textarea" name="username" placeholder="username" /> <br />
					Password: <input type="password" name="password" placeholder="password" /> <br />
					<input type="button" name="posted" value="'.speak('Find Chain').'"  onclick="chain.send_ajax_call_from_form(\'areas/chain_management.php\', \'findChainForm\');" /> <br />
					<label class="errors"></label>
				</form>
			</div>
			<br />
			<div id="joinChainDialog" style="display:none;">
				<font style="font-weight:bold;">Join the chain <label class="global" name="chain_name"></label>:</font>
				<div class="grouping">
					<form id="joinChainForm">
						<input type="hidden" class="global" name="command" value="join_chain" />
						<input type="hidden" name="chain_management_ajax" value="1" />
						<input type="hidden" class="global" name="username" />
						<input type="hidden" class="global" name="password" />
						<input type="button" name="posted" value="Join Chain" class="saveBtn smallSaveBtn" onclick="chain.send_ajax_call_from_form(\'areas/chain_management.php\', \'joinChainForm\');" /> <br />
						<label class="errors"></label>
					</form>
				</div>
			</div>';
		return $s_retval;
	}

	function drawChainJavascript() {
		return '
		<script type="text/javascript">
			function checkSSL() {
				current = window.location.href;
				// if (current.indexOf("https") > -1)
				// 	return;
				// window.location.replace(current.replace("http://", "https://"));
			}

			function bensAjaxFunctions() {
				this.send_ajax_call_from_form = function(php_file_name, form_id) {
					var jform = $("#"+form_id);
					var a_inputs = jform.find("input");
					var a_selects = jform.find("select");
					var a_textareas = jform.find("textarea");
					var inputs = $.merge($.merge(a_inputs, a_selects), a_textareas);

					var posts = {};
					var full_posts = [];
					for(var i = 0; i < inputs.length; i++) {
						var name = $(inputs[i]).prop("name");
						var value = $(inputs[i]).val();
						full_posts.push([name, value]);
						posts[name] = value;
					}

					jerrors_label = $(jform.find("label.errors"));
					set_html_and_fade_in(jerrors_label, "", "<font style=\'color:gray;\'>Please wait...</font>");
					setTimeout(function() {
						var commands_array = retval_to_commands(send_ajax_call(php_file_name, posts));
						if (commands_array.length == 1 && commands_array[0].length == 1 && commands_array[0][0] == "")
							commands_array = [["print error","The website gave no response"]];
						set_html_and_fade_in(jerrors_label, "", "");
						interpret_common_ajax_commands(commands_array);
						for (var i = 0; i < commands_array.length; i++) {
							var command = commands_array[i][0];
							var note = commands_array[i][1];
							if (command == "print error") {
								set_html_and_fade_in(jerrors_label, "", "<font style=\'color:red;\'>"+note+"</font>");
							} else if (command == "print success") {
								set_html_and_fade_in(jerrors_label, "", "<font style=\'color:gray;font-weight:normal;\'>"+note+"</font>");
							} else if (command == "load page with post") {
								var posts_string = "";
								for (var i = 0; i < full_posts.length; i++)
									posts_string += \'<input type="hidden" name="\'+full_posts[i][0]+\'" value="\'+full_posts[i][1]+\'" />\';
								var id_string = get_unique_id();
								var create_string = \'<form method="POST" action="\'+note+\'" id="\'+id_string+\'">\'+posts_string+\'</form>\';
								$(create_string).appendTo("body");
								$("#"+id_string).submit();
							} else if (command == "clear field") {
								jform.find("input[name="+note+"]").val("");
							} else if (command == "focus field") {
								jform.find("input[name="+note+"]").focus();
							} else if (command == "show field") {
								setTimeout(function() {
									$("#"+note).show();
									$("#"+note).css({ opacity: 0 });
									$("#"+note).animate({ opacity: 1 }, 300);
								}, 10);
							} else if (command == "change value") {
								var a_parts = note.split("[*value*]");
								var findBy = a_parts[0];
								var newValue = a_parts[1];
								var global = false;
								if (a_parts[2] && a_parts[2] == "global")
									global = true;
								var element = $(findBy);
								if (!global)
									element = jform.find(findBy);
								element.val(newValue);
								setTimeout(function() {
									element.stop(true, true);
									element.css({ opacity: 0 });
									element.animate({ opacity: 1 }, 300);
								}, 10);
							} else if (command == "change html") {
								var a_parts = note.split("[*value*]");
								var findBy = a_parts[0];
								var newValue = a_parts[1];
								var global = false;
								if (a_parts[2] && a_parts[2] == "global")
									global = true;
								var element = $(findBy);
								if (!global)
									element = jform.find(findBy);
								element.html(newValue);
								setTimeout(function() {
									element.stop(true, true);
									element.css({ opacity: 0 });
									element.animate({ opacity: 1 }, 300);
								}, 10);
							}
						}
					}, 100);
				}

				this.set_html_and_fade_in = function(jparent_object, parent_id, html) {
					setTimeout(function() {
						set_html_and_fade_in(jparent_object, parent_id, html);
					}, 10);
				}
				function set_html_and_fade_in(jparent_object, parent_id, html) {
					if (jparent_object === null)
						jparent_object = $("#"+parent_id);
					kill_children(jparent_object);
					jparent_object.html(\'\');
					jparent_object.append(html);
					jparent_object.stop(true,true);
					jparent_object.children().css({opacity:0});
					jparent_object.children().animate({opacity:1},300);
				}

				function kill_children(jobject) {
					var children = jobject.children();
					for(var i = 0; i < children.length; i++)
						$(children[i]).remove();
				}

				function retval_to_commands(retval) {
					var commands_list = retval.split("[*command*]");
					var commands_array = [];
					for (var i = 0; i < commands_list.length; i++) {
						var command = commands_list[i].split("[*note*]");
						commands_array.push(command);
					}
					return commands_array;
				}

				function interpret_common_ajax_commands(commands_array) {
					for (var i = 0; i < commands_array.length; i++) {
						var command = commands_array[i][0];
						var note = commands_array[i][1];
						if (command == "load page") {
							window.location = note;
						} else if (command == "reload page") {
							location.reload();
						} else if (command == "alert") {
							alert(note);
						}
					}
				}

				function send_ajax_call(php_file_name, posts) {
					a_message = send_async_ajax_call(php_file_name, posts, false);
					return a_message;
				}

				function send_async_ajax_call(php_file_name, posts, async) {
					$.ajax({
						url: php_file_name,
						async: async,
						cache: false,
						data: posts,
						type: "POST",
						timeout: 10000,
						success: function(data) {
							send_ajax_call_retval = data;
						},
						error: function(xhr, ajaxOptions, thrownError) {
							if (parseInt(xhr.status) == 0 && thrownError) {
								if ((thrownError+"").indexOf("NETWORK_ERR") > -1) {
									send_ajax_call_retval = "network error encountered";
									return;
								}
							}
							alert("Error sending request: ("+xhr.status+") "+thrownError);
							send_ajax_call_retval = "error";
						}
					});
					return send_ajax_call_retval;
				}

				function get_unique_id() {
					var retval = "id";
					for (var i = 0; i < 1000000; i++) {
						if ($("#"+retval+i).length == 0)
							return retval+i;
					}
				}
			}

			checkSSL();

			chain = new bensAjaxFunctions();
		</script>';
	}

	// checks that the given user has the correct password and is level 4 access
	// return TRUE or FALSE
	// updates the login_logs table
	function check_user_access($rdb, $s_username, $s_password) {
		preg_match("/poslavu_(.*)_db/", $rdb, $output_array);
		lavu_connect_dn($output_array[1],$rdb);
		$user_query = lavu_query("select * from `$rdb`.`users` where `access_level`>'3' and `_deleted`!='1' and `username`='[1]' and (`password`=PASSWORD('[2]') or `password`=OLD_PASSWORD('[2]'))",$s_username,$s_password);
		if ($user_query === FALSE) {
			error_log('bad user query in '.__FILE__);
			return FALSE;
		}
		if (mysqli_num_rows($user_query) == 0) {
			update_cp_login_status(FALSE,$s_username);
			return FALSE;
		}
		update_cp_login_status(TRUE,$s_username);
		return TRUE;
	}

	function get_table_columns($s_tablename) {
		$column_names_query = mlavu_query("SHOW COLUMNS FROM `poslavu_DEV_db`.`[tablename]`",
			array('tablename'=>$s_tablename));
		$a_colnames = array();
		while ($row = mysqli_fetch_assoc($column_names_query))
			$a_colnames[] = $row['Field'];

		$a_retval = array();
		foreach($a_colnames as $s_colname) {
			if (stristr($s_colname, 'id'))
				$a_retval[] = "'$s_colname'=>array('has_parent'=>TRUE, 'parent_table'=>'$s_colname', 'parent_ref'=>'$s_colname', 'parent_index'=>'id')";
			else
				$a_retval[] = "'$s_colname'=>array('has_parent'=>FALSE)";
		}
		return implode(',
', $a_retval);
	}

	/********************************************************************************
	 * All sync functions related to copying menu items and menu-related items
	 * are encapsulated inside this class.
	 *
	 * Functions of this class include deciding what items should be synced, setting
	 * the sync status of items, and copying items to their appropriate databases.
	 *
	 * important functions are:
	 * setDefaultSyncSettings()
	 * syncFromRestaurant()
	 ********************************************************************************/
	class syncFunc {
		// returns an array of columns, where each value is an array like one of the following:
		// 'columnname'=>array('has_parent'=>true/false,'parent_table'=>stuff,'parent_ref'=>usuallyname,'parent_index'=>usuallyid,
		//     ['default'=>array('value'=>value, 'table'=>will pull the first value from column if value is null, 'column'=>columnName)])
		// 'columnname'=>array('image'=>TRUE, 'directory'=>directoryname, 'directory_extensions'=>array('','main', 'full')) // directory name fits in as $s_dir_name in '/mnt/poslavu-files/images/'.$s_dataname.'/'.$s_dir_name.'/'.$s_dir_extension
		// 'columnname'=>array('happyhour'=>TRUE, 'group_delimeter'=>string, 'value_delimeter'=>string, 'parent_table'=>happyhourstable)
		// 'columnname'=>array('ingredients'=>TRUE, 'group_delimeter'=>string, 'value_delimeter'=>string, 'parent_table'=>ingredientstable)
		// 'columnname'=>array('has_prefixes'=>TRUE, 'prefixes'=>array(
		//     'prefix1'=>array('has_parent'=>true/false,'parent_table'=>stuff,'parent_ref'=>usuallyname,'parent_index'=>usuallyid),
		//     ...)
		// 'columnname'=>array('forcedmodifiergrouplist'=>TRUE, 'group_delimeter'=>string, 'parent_table'=>forcedmodifergrouplisttable)
		public static function getColumnsToSyncByTablename($s_tablename) {
			$a_columns = array();

			// commented out rows are parent categories and shouldn't be synced
			// or, I'm confused on how to handle them and need help from Corey
			switch($s_tablename) {
			case 'menu_categories':
				$a_columns = array(
					'menu_id'=>array('has_parent'=>TRUE, 'parent_table'=>'menus', 'parent_ref'=>'title', 'parent_index'=>'id', 'default'=>array('value'=>NULL, 'table'=>'menus', 'column'=>'id')),
					'group_id'=>array('has_parent'=>TRUE, 'parent_table'=>'menu_groups', 'parent_ref'=>'group_name', 'parent_index'=>'id'),
					'name'=>array('has_parent'=>FALSE),
					'image'=>array('image'=>TRUE, 'directory'=>'categories', 'directory_extensions'=>array('','main', 'full')),
					'description'=>array('has_parent'=>FALSE),
					'active'=>array('has_parent'=>FALSE),
					'_order'=>array('has_parent'=>FALSE),
					'print'=>array('has_parent'=>FALSE),
					'last_modified_date'=>array('has_parent'=>FALSE),
					'printer'=>array('has_parent'=>FALSE),
					'modifier_list_id'=>array('has_parent'=>TRUE, 'parent_table'=>'modifiers', 'parent_ref'=>'title', 'parent_index'=>'id'),
					'_deleted'=>array('has_parent'=>FALSE),
					'apply_taxrate'=>array('has_parent'=>FALSE),
					'custom_taxrate'=>array('has_parent'=>FALSE),
					'forced_modifier_group_id'=>array('has_prefixes'=>TRUE, 'prefixes'=>array(
						''=>array('has_parent'=>TRUE, 'parent_table'=>'forced_modifier_groups', 'parent_ref'=>'title', 'parent_index'=>'id'),
						'f'=>array('has_parent'=>TRUE, 'parent_table'=>'forced_modifier_lists', 'parent_ref'=>'title', 'parent_index'=>'id'))),
					'print_order'=>array('has_parent'=>FALSE),
					'super_group_id'=>array('has_parent'=>TRUE, 'parent_table'=>'super_groups', 'parent_ref'=>'title', 'parent_index'=>'id'),
					'tax_inclusion'=>array('has_parent'=>FALSE),
					'ltg_display'=>array('has_parent'=>FALSE),
					'happyhour'=>array('happyhour'=>TRUE, 'group_delimeter'=>'|', 'value_delimeter'=>':', 'parent_table'=>'happyhours'),
				);
				break;
			case 'menu_items':
				$a_columns = array(
					'category_id'=>array('has_parent'=>TRUE, 'parent_table'=>'menu_categories', 'parent_ref'=>'name', 'parent_index'=>'id'),
					'menu_id'=>array('has_parent'=>TRUE, 'parent_table'=>'menus', 'parent_ref'=>'title', 'parent_index'=>'id', 'default'=>array('value'=>NULL, 'table'=>'menus', 'column'=>'id')),
					'name'=>array('has_parent'=>FALSE),
					'price'=>array('has_parent'=>FALSE),
					'description'=>array('has_parent'=>FALSE),
					'image'=>array('image'=>TRUE, 'directory'=>'items', 'directory_extensions'=>array('','main', 'full')),
					'image2'=>array('image'=>TRUE, 'directory'=>'items', 'directory_extensions'=>array('','main', 'full')),
					'image3'=>array('image'=>TRUE, 'directory'=>'items', 'directory_extensions'=>array('','main', 'full')),
					'misc_content'=>array('has_parent'=>FALSE),
					//'options1'=>array('has_parent'=>FALSE),
					//'options2'=>array('has_parent'=>FALSE),
					//'options3'=>array('has_parent'=>FALSE),
					'active'=>array('has_parent'=>FALSE),
					'_order'=>array('has_parent'=>FALSE),
					'print'=>array('has_parent'=>FALSE),
					'quick_item'=>array('has_parent'=>FALSE),
					'last_modified_date'=>array('has_parent'=>FALSE),
					'printer'=>array('has_parent'=>FALSE),
					'apply_taxrate'=>array('has_parent'=>FALSE),
					'custom_taxrate'=>array('has_parent'=>FALSE),
					'modifier_list_id'=>array('has_parent'=>TRUE, 'parent_table'=>'forced_modifier_lists', 'parent_ref'=>'title', 'parent_index'=>'id'),
					'_deleted'=>array('has_parent'=>FALSE),
					'forced_modifier_group_id'=>array('has_prefixes'=>TRUE, 'prefixes'=>array(
						''=>array('has_parent'=>TRUE, 'parent_table'=>'forced_modifier_groups', 'parent_ref'=>'title', 'parent_index'=>'id'),
						'f'=>array('has_parent'=>TRUE, 'parent_table'=>'forced_modifier_lists', 'parent_ref'=>'title', 'parent_index'=>'id'),
						'C'=>array('has_parent'=>FALSE))),
					'ingredients'=>array('ingredients'=>TRUE, 'group_delimeter'=>',', 'value_delimeter'=>' x ', 'parent_table'=>'ingredients'),
					'open_item'=>array('has_parent'=>FALSE),
					'hidden_value'=>array('has_parent'=>FALSE),
					'hidden_value2'=>array('has_parent'=>FALSE),
					'hidden_value3'=>array('has_parent'=>FALSE),
					'allow_deposit'=>array('has_parent'=>FALSE),
					'UPC'=>array('has_parent'=>FALSE),
					//'inv_count'=>array('has_parent'=>FALSE),
					'show_in_app'=>array('has_parent'=>FALSE),
					//'super_group_id'=>array('has_parent'=>TRUE, 'parent_table'=>'super_group_id', 'parent_ref'=>'super_group_id', 'parent_index'=>'id'),
					'tax_inclusion'=>array('has_parent'=>FALSE),
					'ltg_display'=>array('has_parent'=>FALSE),
					'happyhour'=>array('happyhour'=>TRUE, 'group_delimeter'=>'|', 'value_delimeter'=>':', 'parent_table'=>'happyhours'),
				);
				break;
			case 'happyhours':
				$a_columns = array(
					'name'=>array('has_parent'=>FALSE),
					'start_time'=>array('has_parent'=>FALSE),
					'end_time'=>array('has_parent'=>FALSE),
					'weekdays'=>array('has_parent'=>FALSE),
					'property'=>array('has_parent'=>FALSE),
					'_deleted'=>array('has_parent'=>FALSE)
				);
				break;
			case 'menus':
				$a_columns = array(
					'title'=>array('has_parent'=>FALSE),
					'created_by'=>array('has_parent'=>FALSE),
					//'modifications'=>array('has_parent'=>FALSE), // obsolete, ignoring
				);
				break;
			case 'menu_groups':
				$a_columns = array(
					'menu_id'=>array('has_parent'=>TRUE, 'parent_table'=>'menus', 'parent_ref'=>'title', 'parent_index'=>'id', 'default'=>array('value'=>NULL, 'table'=>'menus', 'column'=>'id')),
					'group_name'=>array('has_parent'=>FALSE),
					'orderby'=>array('has_parent'=>FALSE),
					'_order'=>array('has_parent'=>FALSE),
					'_deleted'=>array('has_parent'=>FALSE)
				);
				break;
			case 'super_groups':
				$a_columns = array(
					'title'=>array('has_parent'=>FALSE),
					'_order'=>array('has_parent'=>FALSE),
					'_deleted'=>array('has_parent'=>FALSE)
				);
				break;
			case 'modifiers':
				$a_columns = array(
					'title'=>array('has_parent'=>FALSE),
					'cost'=>array('has_parent'=>FALSE),
					//'category'=>array('has_parent'=>FALSE),
					'_deleted'=>array('has_parent'=>FALSE),
					'_order'=>array('has_parent'=>FALSE),
					'ingredients'=>array('ingredients'=>TRUE, 'group_delimeter'=>',', 'value_delimeter'=>' x ', 'parent_table'=>'ingredients')
				);
				break;
			case 'modifier_categories':
				$a_columns = array(
					'title'=>array('has_parent'=>FALSE),
					'description'=>array('has_parent'=>FALSE),
					'_deleted'=>array('has_parent'=>FALSE),
					'menu_id'=>array('has_parent'=>TRUE, 'parent_table'=>'menus', 'parent_ref'=>'title', 'parent_index'=>'id', 'default'=>array('value'=>NULL, 'table'=>'menus', 'column'=>'id')),
				);
				break;
			case 'forced_modifier_groups':
				$a_columns = array(
					'title'=>array('has_parent'=>FALSE),
					'include_lists'=>array('forcedmodifiergrouplist'=>TRUE, 'group_delimeter'=>'|', 'parent_table'=>'forced_modifier_lists'),
					'_deleted'=>array('has_parent'=>FALSE),
					'menu_id'=>array('has_parent'=>TRUE, 'parent_table'=>'menus', 'parent_ref'=>'title', 'parent_index'=>'id', 'default'=>array('value'=>NULL, 'table'=>'menus', 'column'=>'id')),
				);
				break;
			case 'forced_modifier_lists':
				$a_columns = array(
					'title'=>array('has_parent'=>FALSE),
					'description'=>array('has_parent'=>FALSE),
					'type'=>array('has_parent'=>FALSE),
					'_deleted'=>array('has_parent'=>FALSE),
					'menu_id'=>array('has_parent'=>TRUE, 'parent_table'=>'menus', 'parent_ref'=>'title', 'parent_index'=>'id', 'default'=>array('value'=>NULL, 'table'=>'menus', 'column'=>'id'))
				);
				break;
			case 'forced_modifiers':
				$a_columns = array(
					'title'=>array('has_parent'=>FALSE),
					'cost'=>array('has_parent'=>FALSE),
					//'list_id'=>array('has_parent'=>TRUE, 'parent_table'=>'list_id', 'parent_ref'=>'list_id', 'parent_index'=>'id'),
					'_order'=>array('has_parent'=>FALSE),
					'_deleted'=>array('has_parent'=>FALSE),
					'detour'=>array('has_prefixes'=>TRUE, 'prefixes'=>array(
						'l'=>array('has_parent'=>TRUE, 'parent_table'=>'forced_modifier_lists', 'parent_ref'=>'title', 'parent_index'=>'id'),
						'g'=>array('has_parent'=>TRUE, 'parent_table'=>'forced_modifier_groups', 'parent_ref'=>'title', 'parent_index'=>'id'))),
					'ingredients'=>array('ingredients'=>TRUE, 'group_delimeter'=>',', 'value_delimeter'=>' x ', 'parent_table'=>'ingredients', 'parent_ref'=>'title', 'parent_index'=>'id'),
					'extra'=>array('has_parent'=>FALSE),
					'extra2'=>array('has_parent'=>FALSE),
					'extra3'=>array('has_parent'=>FALSE),
					'extra4'=>array('has_parent'=>FALSE),
					'extra5'=>array('image'=>TRUE, 'directory'=>'fmods', 'directory_extensions'=>array('','main', 'full'))
				);
				break;
			case 'ingredients':
				$a_columns = array(
					'title'=>array('has_parent'=>FALSE),
					//'category'=>array('has_parent'=>TRUE, 'parent_table'=>'category', 'parent_ref'=>'category', 'parent_index'=>'id'),
					//'qty'=>array('has_parent'=>FALSE), don't change real-world values, ignoring
					'unit'=>array('has_parent'=>FALSE),
					'low'=>array('has_parent'=>FALSE),
					'high'=>array('has_parent'=>FALSE),
					'cost'=>array('has_parent'=>FALSE),
					'_deleted'=>array('has_parent'=>FALSE),
				);
				break;
			case 'ingredient_categories':
				$a_columns = array(
					'title'=>array('has_parent'=>FALSE),
					'description'=>array('has_parent'=>FALSE),
					'_deleted'=>array('has_parent'=>FALSE),
				);
				break;
			}

			$a_retval = array(
				'withparent'=>array(),
				'noparent'=>array(),
				'happyhour'=>array(),
				'forcedmodifiergrouplist'=>array(),
				'image'=>array(),
				'ingredients'=>array(),
				'has_prefixes'=>array()
			);
			foreach($a_columns as $k=>$a_column) {
				if (isset($a_column['happyhour']))
					$a_retval['happyhour'][$k] = $a_column;
				else if (isset($a_column['forcedmodifiergrouplist']))
					$a_retval['forcedmodifiergrouplist'][$k] = $a_column;
				else if (isset($a_column['image']))
					$a_retval['image'][$k] = $a_column;
				else if (isset($a_column['ingredients']))
					$a_retval['ingredients'][$k] = $a_column;
				else if (isset($a_column['modifications']))
					$a_retval['modifications'][$k] = $a_column;
				else if (isset($a_column['has_prefixes']))
					$a_retval['has_prefixes'][$k] = $a_column;
				else if (isset($a_column['has_parent']) && $a_column['has_parent'])
					$a_retval['withparent'][$k] = $a_column;
				else if (isset($a_column['has_parent']) && !$a_column['has_parent'])
					$a_retval['noparent'][$k] = $a_column;
			}

			return $a_retval;
		}

		// syncs columns that are independent (have no parents)
		// return the array(tablename, id) of rows updated
		public static function syncColumnsWithoutParents($sync_array) {
			$a_sync_types = $sync_array['a_sync_types'];
			$a_sync_rows = $sync_array['a_sync_rows'];
			$s_dataname = $sync_array['s_dataname'];
			$s_tablename = $sync_array['s_tablename'];
			$a_to_sync_by_tablename = $sync_array['a_to_sync_by_tablename'];
			$s_match_parents_by = $sync_array['s_match_parents_by'];
			$a_retval = array();

			// get the names of columns to by synced
			$a_colnames = array();
			foreach($a_sync_types['noparent'] as $s_colname=>$a_sync_column)
				$a_colnames[$s_colname] = $s_colname;
			$s_select_clause = ConnectionHub::getConn('poslavu')->getDBAPI()->arrayToSelectClause($a_colnames);

			// get data on the rows to be synced
			// inserts 'sync__rowdata' into all of the $a_sync_rows
			$a_ids_to_be_synced = array();
			foreach($a_sync_rows as $a_sync_row)
				if (trim($a_sync_row['syncfrom_id']) != '')
					$a_ids_to_be_synced[$a_sync_row['syncfrom_id']] = $a_sync_row['syncfrom_id'];
			if (!self::insertSyncDataIntoSyncRows($a_sync_rows, $a_ids_to_be_synced, $s_select_clause, $s_tablename, $s_dataname))
				return array();

			foreach($a_sync_rows as $a_sync_row) {

				// get the data to by synced
				if (!isset($a_sync_row['sync__rowdata'])) continue;
				$a_row = $a_sync_row['sync__rowdata'];
				$s_dest_dataname = $a_sync_row['restaurant_syncto_dataname'];

				// sync this row in the database
				// $a_retval is updated with the rows that were updated as array($s_tablename, $a_sync_row['syncfrom_id'])
				if (isset($a_row['id']))
					unset($a_row['id']);
				self::syncColumnsDoUpdate($a_row, $s_dest_dataname, $s_tablename, $a_sync_row['syncto_id'], $a_sync_row['syncfrom_id'], $a_retval);
			}

			return $a_retval;
		}

		// a helper funciton for the 'syncColumnsWith...' functions
		// finds the rows to be synced and inserts them into the valuse of the
		//     $a_sync_rows array with a new index ['sync__rowdata']
		// return TRUE on success or FALSE on error
		private static function insertSyncDataIntoSyncRows(&$a_sync_rows, &$a_ids_to_be_synced, &$s_select_clause, &$s_tablename, &$s_dataname) {
			// sanitize the data
			if (strpos($s_select_clause, '`id`') === FALSE) {
				if ($s_select_clause == '') {
					$s_select_clause .= '`id`';
				} else {
					$s_select_clause .= ',`id`';
				}
			}

			// get the rows to be synced
			if (count($a_ids_to_be_synced) == 0) {
				error_log('no ids to be synced');
				return FALSE;
			}
			$a_rows = ConnectionHub::getConn('poslavu')->getDBAPI()->getAllInTable($s_tablename, NULL, TRUE,
				array('whereclause'=>'WHERE `id` '.ConnectionHub::getConn('poslavu')->getDBAPI()->arrayToInClause($a_ids_to_be_synced), 'selectclause'=>$s_select_clause, 'databasename'=>'poslavu_'.$s_dataname.'_db'));
			if (count($a_rows) == 0) {
				error_log('no rows found');
				return FALSE;
			}

			// make a mapping for more efficient mapping in the next step
			$a_id_to_data = array();
			foreach($a_rows as $a_row)
				$a_id_to_data[$a_row['id']] = $a_row;

			// insert the values into the $a_sync_rows
			foreach($a_sync_rows as $k=>$a_sync_row)
				if (isset($a_id_to_data[$a_sync_row['syncfrom_id']]) && !isset($a_sync_row['sync__rowdata']))
					$a_sync_rows[$k]['sync__rowdata'] = $a_id_to_data[$a_sync_row['syncfrom_id']];
			return TRUE;
		}

		// a helper funciton for the 'syncColumnsWith...' functions
		// syncs the values for a row in the $a_update_vars in the $s_dest_dataname.$s_tablename table
		// syncs to the given row id
		// stores the return value in $a_retval for each row updated
		private static function syncColumnsDoUpdate(&$a_update_vars, $s_dest_dataname, $s_tablename, $s_syncto_id, $s_syncfrom_id, &$a_retval, $b_print = FALSE) {
			if (count($a_update_vars) == 0) return;
			$s_update_clause = ConnectionHub::getConn('poslavu')->getDBAPI()->arrayToUpdateClause($a_update_vars);
			$query_string = "UPDATE `[sync__database]`.`[sync__table]` $s_update_clause WHERE `id`='[sync__id]'";
			$query_vars = array_merge(array('sync__database'=>'poslavu_'.$s_dest_dataname.'_db', 'sync__table'=>$s_tablename, 'sync__id'=>$s_syncto_id), $a_update_vars);
			if ($b_print) {
				echo "$query_string\n";
				print_r($query_vars);
			}
			mlavu_query($query_string, $query_vars);
			if (ConnectionHub::getConn('poslavu')->affectedRows() > 0)
				$a_retval[] = array($s_tablename, $s_syncfrom_id);
		}

		// syncs columns that have parents and must have a parent reference to update to
		// if $s_prefix is defined, it will be prepended to the parent id
		// return the array(tablename, id) of rows updated
		public static function syncColumnsWithParents($sync_array, $s_prefix = '') {
			$a_sync_types = $sync_array['a_sync_types'];
			$a_sync_rows = $sync_array['a_sync_rows'];
			$s_dataname = $sync_array['s_dataname'];
			$s_tablename = $sync_array['s_tablename'];
			$a_to_sync_by_tablename = $sync_array['a_to_sync_by_tablename'];
			$s_match_parents_by = $sync_array['s_match_parents_by'];
			$a_retval = array();

			// get the select clause to select only the necessary columns from the table
			// (for memory efficiency)
			$a_colnames = array();
			foreach($a_sync_types['withparent'] as $s_colname=>$a_sync_column)
				$a_colnames[$s_colname] = $s_colname;
			$s_select_clause = ConnectionHub::getConn('poslavu')->getDBAPI()->arrayToSelectClause($a_colnames);

			// get data on the rows to be synced
			// inserts 'sync__rowdata' into all of the $a_sync_rows
			$a_ids_to_be_synced = array();
			foreach($a_sync_rows as $a_sync_row)
				if (trim($a_sync_row['syncfrom_id']) != '')
					$a_ids_to_be_synced[$a_sync_row['syncfrom_id']] = $a_sync_row['syncfrom_id'];
			if (!syncFunc::insertSyncDataIntoSyncRows($a_sync_rows, $a_ids_to_be_synced, $s_select_clause, $s_tablename, $s_dataname))
				return array();

			// update each row
			foreach($a_sync_rows as $a_sync_row) {
				// get the data to be synced
				if (!isset($a_sync_row['sync__rowdata'])) {
					error_log('with parents: no rows found');
					continue;
				}
				$a_row = $a_sync_row['sync__rowdata'];
				$s_dest_dataname = $a_sync_row['restaurant_syncto_dataname'];
				$a_update_vars = array();

				// check for columns that are ready to be updated and add them to the update list
				foreach($a_sync_types['withparent'] as $s_colname=>$a_sync_column) {

					// get the $s_parent_ref of the parent row,
					// to be matched with the $s_parent_ref of the destination parent row
					$s_parent_tablename = $a_sync_column['parent_table'];
					$s_parent_index = $a_sync_column['parent_index'];
					$s_parent_indexvalue = $a_row[$s_colname];
					$s_parent_ref = $a_sync_column['parent_ref'];
					$s_full_query = '';
					print_r($a_sync_column);
					$a_source_match = ConnectionHub::getConn('poslavu')->getDBAPI()->getAllInTable($s_parent_tablename, array($s_parent_index=>$s_parent_indexvalue), TRUE,
						array('selectclause'=>'`'.$s_parent_ref.'`,`id`', 'databasename'=>'poslavu_'.$s_dataname.'_db', 'check_deleted'=>TRUE),
						$s_full_query);
					echo $s_full_query."<br />";
					$s_default_value = NULL;

					if (count($a_source_match) == 0) {
						//if ($s_parent_indexvalue !== '0') { echo("\n".'no parent match found for source'."\n"); echo($s_full_query."\n"); }
						continue;
					}

					// find updates and add them to the update list
					if ($s_match_parents_by == 'name') {

						// match columns based on name/value
						$s_source_match = $a_source_match[0][$s_parent_ref];
						$a_dest_match = ConnectionHub::getConn('poslavu')->getDBAPI()->getAllInTable($s_parent_tablename, array($s_parent_ref=>$s_parent_ref, $s_source_match=>$s_source_match), TRUE,
							array('selectclause'=>'`'.$s_parent_index.'`', 'databasename'=>'poslavu_'.$s_dest_dataname.'_db', 'whereclause'=>"WHERE `[$s_parent_ref]` LIKE '[$s_source_match]'", 'check_deleted'=>TRUE),
							$s_full_query);
						$s_value = '';
						if (count($a_dest_match) == 0) {
							// 'default'=>array('value'=>NULL, 'table'=>'menus', 'column'=>'id')
							// check if there's a default value before dismissing the sync
							if (isset($a_sync_column['default']) && is_array($a_sync_column['default'])) {
								$a_default = $a_sync_column['default'];
								if ($a_default['value'] === NULL) {
									$a_default_values = ConnectionHub::getConn('poslavu')->getDBAPI()->getAllInTable($a_default['table'], NULL, TRUE,
										array('selectclause'=>'`'.$a_default['column'].'`', 'limitclause'=>'LIMIT 1', 'databasename'=>'poslavu_'.$s_dest_dataname.'_db'));
									$s_value = $a_default_values[0][$a_default['column']];
								} else {
									$s_value = $a_default['value'];
								}
							} else {
								continue;
							}
						} else {
							$s_value = $a_dest_match[0][$s_parent_index];
						}
						$a_update_vars[$s_colname] = $s_value;
					} else if ($s_match_parents_by == 'chain_replications' && count($a_to_sync_by_tablename[$s_parent_tablename]) > 0) {

						// match columns based on what is being synced in the chain_replications table
						$s_source_match = $a_source_match[0]['id'];
						foreach($a_to_sync_by_tablename[$s_parent_tablename] as $a_syncmatch_row) {
							if ($a_syncmatch_row['syncfrom_id'] == $s_source_match) {
								$a_update_vars[$s_colname] = $a_syncmatch_row['syncto_id'];
								break;
							}
						}
					}

					// prepend the prefix to the $s_colname
					if (isset($a_update_vars[$s_colname])) {
						if ($s_default_value === NULL)
							$a_update_vars[$s_colname] = $s_prefix.$a_update_vars[$s_colname];
						else
							$a_update_vars[$s_colname] = $s_default_value;
					}
				}

				// sync this row in the database
				// $a_retval is updated with the rows that were updated as array($s_tablename, $a_sync_row['syncfrom_id'])
				self::syncColumnsDoUpdate($a_update_vars, $s_dest_dataname, $s_tablename, $a_sync_row['syncto_id'], $a_sync_row['syncfrom_id'], $a_retval);
			}

			return $a_retval;
		}

		// helper function for syncColumnsWithDelimeters
		private static function explodeOnDelimeters($s_group_delimeter, $s_source_column, $s_value_delimeter, &$a_retval) {

			// get the parent groups
			$a_groups = explode($s_group_delimeter, $s_source_column);
			if ($s_value_delimeter != '') {

				// get all of the arrays(id, val_delim.val) of the parent elements
				foreach($a_groups as $s_group) {
					if ($s_group == '')
						continue;
					$a_parts = explode($s_value_delimeter, $s_group);
					$a_value = $a_parts;
					unset($a_value[0]);
					$s_value = implode($s_value_delimeter,$a_value);
					$a_retval[$a_parts[0]] = array($a_parts[0], $s_value_delimeter.$s_value);
				}
			} else {

				// get all of the ids of the parent elements
				$a_retval = $a_groups;
			}
		}

		// helper function for syncColumnsWithDelimeters
		private static function findParentIdsForColumnsWithDelimeters(&$a_to_sync_table, &$a_source_parent_ids, &$a_dest_parent_ids) {

			// get a map of ids to sync from=>to
			$a_ids_map = array();
			foreach($a_to_sync_table as $a_to_sync)
				$a_ids_map[$a_to_sync['syncfrom_id']] = $a_to_sync['syncto_id'];

			// find the matching parent rows and build a list of those ids
			$a_dest_parent_ids = array();
			foreach($a_source_parent_ids as $s_parent_id) {
				if (!is_array($s_parent_id)) {
					if (isset($a_ids_map[$s_parent_id]))
						$a_dest_parent_ids[] = $a_ids_map[$s_parent_id];
				} else {
					if (isset($a_ids_map[$s_parent_id[0]]))
						$a_dest_parent_ids[] = $a_ids_map[$s_parent_id[0]] . $s_parent_id[1];
				}
			}
		}

		// syncs columns that are delimeted and must have a parent reference to update to
		// return the array(tablename, id) of rows updated
		public static function syncColumnsWithDelimeters($sync_array, $s_sync_type) {
			$a_sync_types = $sync_array['a_sync_types'];
			$a_sync_rows = $sync_array['a_sync_rows'];
			$s_dataname = $sync_array['s_dataname'];
			$s_tablename = $sync_array['s_tablename'];
			$a_to_sync_by_tablename = $sync_array['a_to_sync_by_tablename'];
			$s_match_parents_by = $sync_array['s_match_parents_by'];
			$a_retval = array();

			// get the select clause
			$a_colnames = array();
			foreach($a_sync_types[$s_sync_type] as $s_colname=>$a_sync_column) {
				$a_colnames[$s_colname] = $s_colname;
				$s_group_delimeter = $a_sync_column['group_delimeter'];
				$s_value_delimeter = isset($a_sync_column['value_delimeter']) ? $a_sync_column['value_delimeter'] : '';
				$s_parent_table = $a_sync_column['parent_table'];
				break; // there should only be one delimeted $s_sync_type per row
			}
			$s_select_clause = ConnectionHub::getConn('poslavu')->getDBAPI()->arrayToSelectClause($a_colnames);

			// check that there is data to match up
			if (count($a_to_sync_by_tablename[$s_parent_table]) == 0)
				return array();

			// get data on the rows to be synced
			// inserts 'sync__rowdata' as the source row data into all of the $a_sync_rows
			$a_ids_to_be_synced = array();
			foreach($a_sync_rows as $a_sync_row)
				if (trim($a_sync_row['syncfrom_id']) != '')
					$a_ids_to_be_synced[$a_sync_row['syncfrom_id']] = $a_sync_row['syncfrom_id'];
			if (!syncFunc::insertSyncDataIntoSyncRows($a_sync_rows, $a_ids_to_be_synced, $s_select_clause, $s_tablename, $s_dataname))
				return array();

			// update each row
			foreach($a_sync_rows as $a_sync_row) {
				// get the data to be synced
				if (!isset($a_sync_row['sync__rowdata'])) {
					error_log('delimeters: no data to be synced');
					break;
				}
				$a_source_row = $a_sync_row['sync__rowdata'];
				$s_dest_dataname = $a_sync_row['restaurant_syncto_dataname'];
				$a_update_vars = array();

				// get the values to be updated
				{ // organization, yo!

					// explode the parent row te get the groups and
					// get the ids of the parent elements
					// @$a_source_parent_ids: will either be ids, or arrays(id, delim.value)
					$a_source_parent_ids = array();
					self::explodeOnDelimeters($s_group_delimeter, $a_source_row[$s_colname], $s_value_delimeter, $a_source_parent_ids);

					if (count($a_source_parent_ids) == 0) {
						$a_update_vars[$s_colname] = '';
					} else {

						// find the matching parent rows and build a list of those ids
						$a_dest_parent_ids = array();
						self::findParentIdsForColumnsWithDelimeters($a_to_sync_by_tablename[$s_parent_table], $a_source_parent_ids, $a_dest_parent_ids);

						// build the list of ids to be updated
						if (count($a_dest_parent_ids) == 0)
							$a_update_vars[$s_colname] = '';
						else
							$a_update_vars[$s_colname] = implode($s_group_delimeter, $a_dest_parent_ids);
					}
				}

				// sync this row in the database
				// $a_retval is updated with the rows that were updated as array($s_tablename, $a_sync_row['syncfrom_id'])
				self::syncColumnsDoUpdate($a_update_vars, $s_dest_dataname, $s_tablename, $a_sync_row['syncto_id'], $a_sync_row['syncfrom_id'], $a_retval);
			}

			return $a_retval;
		}

		// syncs columns that have images and the images must be copied
		// return the array(tablename, id) of rows updated
		public static function syncColumnsWithImages($sync_array) {
			$a_sync_types = $sync_array['a_sync_types'];
			$a_sync_rows = $sync_array['a_sync_rows'];
			$s_dataname = $sync_array['s_dataname'];
			$s_tablename = $sync_array['s_tablename'];
			$a_to_sync_by_tablename = $sync_array['a_to_sync_by_tablename'];
			$s_match_parents_by = $sync_array['s_match_parents_by'];

			$a_retval = array();

			// get the select clause
			$a_colnames = array();
			foreach($a_sync_types['image'] as $s_colname=>$a_sync_column) {
				$a_colnames[$s_colname] = $s_colname;
				$s_dir_name = $a_sync_column['directory'];
			}
			$s_select_clause = ConnectionHub::getConn('poslavu')->getDBAPI()->arrayToSelectClause($a_colnames);

			// get data on the rows to be synced
			// inserts 'sync__rowdata' into all of the $a_sync_rows
			$a_ids_to_be_synced = array();
			foreach($a_sync_rows as $a_sync_row)
				if (trim($a_sync_row['syncfrom_id']) != '')
					$a_ids_to_be_synced[$a_sync_row['syncfrom_id']] = $a_sync_row['syncfrom_id'];
			if (!syncFunc::insertSyncDataIntoSyncRows($a_sync_rows, $a_ids_to_be_synced, $s_select_clause, $s_tablename, $s_dataname))
				return array();

			// update each row
			foreach($a_sync_rows as $a_sync_row) {

				// get the data to by synced
				if (!isset($a_sync_row['sync__rowdata'])) continue;
				$a_source_row = $a_sync_row['sync__rowdata'];
				$s_dest_dataname = $a_sync_row['restaurant_syncto_dataname'];
				$a_dest_rows = ConnectionHub::getConn('poslavu')->getDBAPI()->getAllInTable($s_tablename, array('id'=>$a_sync_row['syncto_id']), TRUE,
					array('selectclause'=>$s_select_clause, 'databasename'=>'poslavu_'.$s_dest_dataname.'_db', 'check_deleted'=>TRUE));
				if (count($a_dest_rows) == 0) continue;
				$s_dest_row = $a_dest_rows[0];

				// get directory extensions to comply with forced modifiers
				$a_directory_extensions = (isset($a_sync_row['directory_extensions'])) ? $a_sync_row['directory_extensions'] : array('');

				// for each image
				$a_update_vars = array();
				foreach($a_sync_types['image'] as $s_colname=>$a_sync_column) {

					// for each directory containing that image
					foreach($a_directory_extensions as $s_directory_extension) {

						// get the image name and the directory name
						$s_imagename = $a_source_row[$s_colname];
						if ($s_imagename == '') continue;
						$s_directory_extension .= ($s_directory_extension != '') ? '/' : '';

						// the images are out of sync,
						// so update [and copy?] them
						{
							// copy the images?
							$s_filesafe_imagename = str_replace(' ', '\ ', $s_imagename);
							$s_source_filename = realpath('/mnt/poslavu-files/images/'.$s_dataname.'/'.$s_dir_name.$s_directory_extension).'/'.$s_imagename;
							$s_dest_filename = realpath('/mnt/poslavu-files/images/'.$s_dest_dataname.'/'.$s_dir_name.$s_directory_extension).'/'.$s_imagename;

							// check that the source image exists
							// still replace the destination field, because the image data could be stored in an "extra" field
							//      (as is the case for extra5 in forced modifiers, which can be used as either an image description or
							//      the dimensions to the pop-up html forms, in the app)
							if (file_exists($s_source_filename)) {
							$b_copy_image = FALSE;

								// if the destination image doesn't exist or is older than the source, replace it
								if (!file_exists($s_dest_filename) || (filemtime($s_dest_filename) < filemtime($s_source_filename)))
									$b_copy_image = TRUE;
								if ($b_copy_image)
									copy($s_source_filename, $s_dest_filename);
							}
						}

						// mark the column as needing to be updated
						if ($s_imagename == $a_dest_row[$s_colname]) continue;
						$a_update_vars[$s_colname] = $s_imagename;
					}
				}

				// sync this row in the database
				// $a_retval is updated with the rows that were updated as array($s_tablename, $a_sync_row['syncfrom_id'])
				self::syncColumnsDoUpdate($a_update_vars, $s_dest_dataname, $s_tablename, $a_sync_row['syncto_id'], $a_sync_row['syncfrom_id'], $a_retval);
			}

			return $a_retval;
		}

		// syncs columns that have parents and must have a parent reference to update to (and have a prefix before the parent denoting the parent table)
		// return the array(tablename, id) of rows updated
		public static function syncColumnsWithPrefixes($sync_array) {
			$a_sync_types = $sync_array['a_sync_types'];
			$a_sync_rows = $sync_array['a_sync_rows'];
			$s_dataname = $sync_array['s_dataname'];
			$s_tablename = $sync_array['s_tablename'];
			$a_to_sync_by_tablename = $sync_array['a_to_sync_by_tablename'];
			$s_match_parents_by = $sync_array['s_match_parents_by'];

			$a_retval = array();

			// get the select clause
			$a_colnames = array();
			$a_prefixes = array();
			foreach($a_sync_types['has_prefixes'] as $s_colname=>$a_sync_column) {
				$a_colnames[$s_colname] = $s_colname;
				$a_prefixes[$s_colname] = $a_sync_column['has_prefixes'];
			}
			$s_select_clause = ConnectionHub::getConn('poslavu')->getDBAPI()->arrayToSelectClause($a_colnames);

			// get data on the rows to be synced
			// inserts 'sync__rowdata' into all of the $a_sync_rows
			$a_ids_to_be_synced = array();
			foreach($a_sync_rows as $a_sync_row)
				if (trim($a_sync_row['syncfrom_id']) != '')
					$a_ids_to_be_synced[$a_sync_row['syncfrom_id']] = $a_sync_row['syncfrom_id'];
			if (!syncFunc::insertSyncDataIntoSyncRows($a_sync_rows, $a_ids_to_be_synced, $s_select_clause, $s_tablename, $s_dataname))
				return array();

			// update each row
			foreach($a_sync_rows as $a_sync_row) {

				// get the data to be synced
				if (!isset($a_sync_row['sync__rowdata'])) {
					error_log('delimeters: no data to be synced');
					break;
				}
				$a_source_row = $a_sync_row['sync__rowdata'];
				$s_dest_dataname = $a_source_row['restaurant_syncto_dataname'];

				// for each column that is marked as a "has_prefixes" column
				foreach($a_colnames as $s_colname) {

					// find the integer part of the reference
					$a_matches = array();
					$a_source_row[$s_colname] = trim($a_source_row[$s_colname]);
					preg_match("/[0-9]*$/", $a_source_row[$s_colname], $a_matches); // (assume only one integer per reference)
					if (count($a_matches) == 0 || $a_matches[0] == '') {

						// assume that, without an integer part, the object has no reference
						// update rows without a prefix as if they had no parent
						$temp_sync_array = array();
						$temp_sync_array['a_sync_types'] = array();
						$temp_sync_array['a_sync_types']['noparent'] = array($s_colname=>array('has_parent'=>FALSE));
						$temp_sync_array['a_sync_rows'] = array($a_sync_row);
						$temp_sync_array['tablename'] = $sync_array['a_sync_rows']['table_name'];
						foreach($sync_array as $k=>$v)
							if (!isset($temp_sync_array[$k]))
								$temp_sync_array[$k] = $sync_array[$k];
						$a_retval = array_merge(syncFunc::syncColumnsWithoutParents($temp_sync_array), $a_retval);
					} else {

						// update rows with a prefix as if they had a parent, plus prepend a prefix
						// get the prefix
						$i_parent_id = $a_matches[0];
						$s_prefix = str_replace($i_parent_id, '', $a_source_row[$s_colname]);
						$a_sync_col_type = $a_sync_types['has_prefixes'][$s_colname]['prefixes'][$s_prefix];

						// create the values to pass to syncColumnsWithParents()
						$temp_sync_row = $a_sync_row;
						$temp_sync_row['sync__rowdata'][$s_colname] = $i_parent_id;
						$temp_sync_array = array();
						$temp_sync_array['a_sync_types'] = array();
						$temp_sync_array['a_sync_types']['withparent'] = array($s_colname=>$a_sync_col_type);
						$temp_sync_array['a_sync_rows'] = array($temp_sync_row);
						$temp_sync_array['s_tablename'] = $temp_sync_row['table_name'];
						$temp_sync_array['s_dataname'] = $sync_array['s_dataname'];
						$temp_sync_array['s_match_parents_by'] = $sync_array['s_match_parents_by'];

						// send the values to syncColumnsWithParents()
						$a_retval = array_merge($a_retval, syncFunc::syncColumnsWithParents($temp_sync_array, $s_prefix));
					}
				}
			}

			return $a_retval;
		}

		// helper function for createNonexistantEntries()
		// looks for all columns that are important for the database structure in $s_tablename
		// returns: an array of all important column names (ignores 'id')
		public static function getImportantTableColumns($s_current_table, &$a_tables_to_sync) {
			$a_retval = array();
			foreach($a_tables_to_sync as $s_tablename=>$a_table) {
				$s_addval = '';
				if ($a_table['parent'] == $s_current_table) {
					$s_addval = $a_table['parent_ref'];
				} else if ($s_tablename == $s_current_table) {
					$s_addval = $a_table['index'];
				}
				if ($s_addval != '' && $s_addval != 'id')
					$a_retval[$s_addval] = $s_addval;
			}
			return $a_retval;
		}

		// helper function for syncFromRestaurant()
		// looks for entries from `chain_replications` with a `syncto_id`='-2', meaning that they have to be created in the destination database
		// creates the entries and copies any columns used as an index or child_ref index
		// it recurses up to 10 times to support nested table structures
		public static function createNonexistantEntries($s_source_dataname, $s_reporting_group, $i_recurse_count = 0) {
			// check that the function hasn't recursed too many times
			$i_recurse_count++;
			if ($i_recurse_count == 10) {
				error_log('too much recursion');
				return;
			}

			$s_source_database = 'poslavu_'.$s_source_dataname.'_db';

			// get the values to sync and
			// get the current restaurant
			$a_entries_to_create = ConnectionHub::getConn('poslavu')->getDBAPI()->getAllInTable('chain_replications', array('reporting_group'=>$s_reporting_group), TRUE,
				array('databasename'=>$s_source_database, 'whereclause'=>"WHERE `reporting_group`='[reporting_group]' AND `syncto_id`='-2' AND `match_type`='automatic'"));
			$a_rest_sources = ConnectionHub::getConn('poslavu')->getDBAPI()->getAllInTable('restaurants', array('data_name'=>$s_source_dataname), TRUE);
			$a_rest_source = $a_rest_sources[0];
			if (count($a_entries_to_create) == 0 || count($a_rest_source) == 0)
				return;

			// get important columns for each table
			$a_tables_to_sync = array();
			self::getTablesToSync($a_tables_to_sync);
			$a_important_columns = array();
			foreach($a_entries_to_create as $a_entry) {
				$s_sync_tablename = $a_entry['table_name'];
				if (!isset($a_important_columns[$s_sync_tablename]))
					$a_important_columns[$s_sync_tablename] = self::getImportantTableColumns($s_sync_tablename, $a_tables_to_sync);
			}

			foreach($a_entries_to_create as $k=>$a_entry) {

				// get data about the entry and about the restaurant
				$s_sync_tablename = $a_entry['table_name'];
				$s_syncfrom_id = $a_entry['syncfrom_id'];
				$s_dest_dataname = $a_entry['restaurant_syncto_dataname'];
				$s_dest_database = 'poslavu_'.$s_dest_dataname.'_db';
				$a_rest_dest = ConnectionHub::getConn('poslavu')->getDBAPI()->getAllInTable('restaurants', array('data_name'=>$s_dest_dataname), TRUE);
				$a_create_entries = ConnectionHub::getConn('poslavu')->getDBAPI()->getAllInTable('config', array('type'=>'special', 'setting'=>'create chain item', 'value'=>$a_rest_source['id'], 'value2'=>$a_entry['syncfrom_id'], 'value4'=>$a_entry['table_name']), TRUE,
					array('databasename'=>$s_dest_database));
				$a_create_entry = $a_create_entries[0];

				// decode the entry and get information from the source element
				$a_create_entry['value_long'] = Json::unencode($a_create_entry['value_long']);
				$o_vlong = $a_create_entry['value_long'][0];
				$a_syncfrom_elements = ConnectionHub::getConn('poslavu')->getDBAPI()->getAllInTable($s_sync_tablename, array('id'=>$a_create_entry['value2']), TRUE,
					array('databasename'=>$s_source_database));
				$a_syncfrom_element = $a_syncfrom_elements[0];

				// check that the parent element exists
				if ($o_vlong->parent_table !== 'null' && $o_vlong->parent_table !== '') {
					$query_string = "SELECT `id` FROM `[database]`.`[parent_table]` WHERE `[parent_index]`='[parent_value]' LIMIT 1";
					$query_vars = array('database'=>$s_source_database, 'parent_table'=>$o_vlong->parent_table, 'parent_index'=>$o_vlong->parent_index, 'parent_value'=>$o_vlong->parent_value);
					$parent_exists_query = mlavu_query($query_string, $query_vars);
					if ($parent_exists_query === FALSE || mysqli_num_rows($parent_exists_query) == 0)
						continue;
				}

				// get the values to insert
				$a_insert_vars = array();
				foreach($a_important_columns[$s_sync_tablename] as $s_important_column)
					if (isset($a_syncfrom_element[$s_important_column]))
						$a_insert_vars[$s_important_column] = $a_syncfrom_element[$s_important_column];
				$s_insert_clause = ConnectionHub::getConn('poslavu')->getDBAPI()->arrayToInsertClause($a_insert_vars);

				// check that the entry doesn't exist
				$a_exists = ConnectionHub::getConn('poslavu')->getDBAPI()->getAllInTable($s_sync_tablename, $a_insert_vars, TRUE,
					array('databasename'=>$s_dest_database));
				$b_exists = (count($a_exists) > 0) ? TRUE : FALSE;

				// insert the entry into the destination database
				// and get the id
				if ($b_exist) {
					$s_new_entry_id = $a_exists[0]['id'];
				} else {
					$query_string = "INSERT INTO `[database]`.`[tablename]` $s_insert_clause";
					$query_vars = array_merge($a_insert_vars, array('database'=>$s_dest_database, 'tablename'=>$s_sync_tablename));
					$result = mlavu_query($query_string, $query_vars);
					if ($result === FALSE) // their database structure is out of sync
						continue;
					$s_new_entry_id = mlavu_insert_id();
					loyaltree_update($s_new_entry_id, $s_dest_dataname);
					

				}

				// remove the create_entry from `config`
				$query_string = "DELETE FROM `[database]`.`config` WHERE `id`='[id]'";
				$query_vars = array('database'=>$s_dest_database, 'id'=>$a_create_entry['id']);
				mlavu_query($query_string, $query_vars);

				// update the `chain_replications` table with the new id
				$query_string = "UPDATE `[database]`.`chain_replications` SET `syncto_id`='[new_id]' WHERE `id`='[id]'";
				$query_vars = array('database'=>$s_source_database, 'new_id'=>$s_new_entry_id, 'id'=>$a_entry['id']);
				mlavu_query($query_string, $query_vars);
				echo ConnectionHub::getConn('poslavu')->getDBAPI()->drawQuery($query_string, $query_vars)."<br />";
			}
		}

		// syncs all data across from one restaurant to another according to `chain_replications`
		// @$s_dataname: the dataname to sync from
		// @$s_reporting_group: the reporting group to sync to
		// @$b_all_reporting_groups: if true, syncs to all reporting groups of the chain
		// @return: an array( array(tablename,id) ) of values synced
		public static function syncFromRestaurant($s_dataname, $s_reporting_group, $b_all_reporting_groups = FALSE) {
			global $maindb;
			$s_match_parents_by = 'name';
			$a_delimeter_list = array('happyhour', 'forcedmodifiergrouplist', 'ingredients');

			// check that this dataname is part of a chain and
			// get the reporting groups of this dataname
			$a_chain = get_restaurant_chain($s_dataname);
			$a_restaurants = get_restaurants_by_chain($a_chain['id']);
			$a_crgs = get_chain_reporting_groups($a_chain['id'], $a_restaurants);
			if (count($a_crgs) == 0)
				return array();

			// create entries that don't yet exist
			foreach($a_crgs as $a_crg) {
				$s_reporting_group = $a_crg['name'];
				self::createNonexistantEntries($s_dataname, $s_reporting_group);
			}

			// get the values to sync from the `chain_replications` table
			if ($b_all_reporting_groups)
				$s_whereclause = "WHERE `syncto_id`>'0'";
			else
				$s_whereclause = "WHERE `reporting_group`='[reporting_group]' AND `syncto_id`>'0'";
			$a_to_sync = ConnectionHub::getConn('poslavu')->getDBAPI()->getAllInTable('chain_replications', array('reporting_group'=>$s_reporting_group), TRUE,
				array('databasename'=>'poslavu_'.$s_dataname.'_db', 'whereclause'=>$s_whereclause));

			// process the values to sync
			// arrange the syncs by the table that they should sync to
			echo "<pre>".print_r($a_to_sync,TRUE)."</pre>";
			if (count($a_to_sync) == 0)
				return array();
			$a_to_sync_by_tablename = array();
			foreach($a_to_sync as $a_sync_row) {
				$s_tablename = $a_sync_row['table_name'];
				if (!isset($a_to_sync_by_tablename[$s_tablename]))
					$a_to_sync_by_tablename[$s_tablename] = array();
				$a_to_sync_by_tablename[$s_tablename][] = $a_sync_row;
			}

			// $a_sync_rows are all of the rows in the given $s_tablename that are to be synced from the source to the destination
			$a_values_updated = array(); // each entry is an array(tablaname, id)
			foreach($a_to_sync_by_tablename as $s_tablename=>$a_sync_rows) {
				// each entry in $a_sync_types is a column to be synced in this table
				// the columns describe how to do the syncing
				$a_sync_types = syncFunc::getColumnsToSyncByTablename($s_tablename);
				if (count($a_sync_types) == 0)
					continue;

				$sync_array = array(
					'a_sync_types'=>&$a_sync_types,
					'a_sync_rows'=>&$a_sync_rows,
					's_dataname'=>&$s_dataname,
					's_tablename'=>&$s_tablename,
					'a_to_sync_by_tablename'=>&$a_to_sync_by_tablename,
					's_match_parents_by'=>&$s_match_parents_by,
				);

				/******************************************************************************************
				 * process those columns without parents (straight copy)
				 ******************************************************************************************/
				if (count($a_sync_types['noparent']) > 0) {
					$a_values_updated = array_merge($a_values_updated, syncFunc::syncColumnsWithoutParents($sync_array));
				}

				/******************************************************************************************
				 * process those columns with parents (matched copy)
				 ******************************************************************************************/
				if (count($a_sync_types['withparent']) > 0) {
					$a_values_updated = array_merge($a_values_updated, syncFunc::syncColumnsWithParents($sync_array));
				}

				/******************************************************************************************
				 * process special columns (happy hours, forced modifier group lists, and the like)
				 ******************************************************************************************/
				foreach($a_delimeter_list as $s_delimeter_type)
					if (count($a_sync_types[$s_delimeter_type]) > 0)
						$a_values_updated = array_merge($a_values_updated, syncFunc::syncColumnsWithDelimeters($sync_array, $s_delimeter_type));
				if (count($a_sync_types['image']) > 0)
					$a_values_updated = array_merge($a_values_updated, syncFunc::syncColumnsWithImages($sync_array));
				if (count($a_sync_types['has_prefixes']) > 0)
					$a_values_updated = array_merge($a_values_updated, syncFunc::syncColumnsWithPrefixes($sync_array));
			}

			$a_retval = array();
			$a_distinct_values_updated = array();
			foreach($a_values_updated as $a_value_updated) {
				$s_table = $a_value_updated[0];
				$s_id = $a_value_updated[1];
				if (!isset($a_distinct_values_updated[$s_table]))
					$a_distinct_values_updated[$s_table] = array();
				if (!isset($a_distinct_values_updated[$s_table][$s_id])) {
					$a_distinct_values_updated[$s_table][$s_id] = TRUE;
					$a_retval[] = $a_value_updated;
				}
			}
			return $a_retval;
		}

		// returns an array of all ids in $a_dest that have a matching strcasecmp(trim(value)) for the given index
		// the arrays are assumed to have an 'id' index
		// $a_source: the syncfrom elements
		// $a_dest: the syncto elements
		// returns one id for each value in $a_source with a match
		// returns an array('type'=>'create', 'parent_table'=>'null') for each value in $a_source that should be created that doesn't have a parent table (assumes that all elements in $a_source should be copied)
		// returns an array('type'=>'create', 'parent_table'=>'tablename', 'parent_index'=>'columnname', 'parent_value'=>'name') for each value in $a_source that should be created that does have a parent table (assumes that all elements in $a_source should be copied)
		public static function find_ids_with_same_name($s_index, &$a_source, &$a_dest, &$a_parent_comparison) {
			$a_retval = array();
			foreach($a_source as $a_source_val) {
				$b_found_match = FALSE;
				foreach($a_dest as $a_dest_val) {
					if ( strtolower(trim($a_source_val[$s_index])) == strtolower(trim($a_dest_val[$s_index])) ) {
						$b_found_match = TRUE;

						// try to find a parent comparison to match
						// if there is a parent for the source, $parent_source_comp = name of the parent element (eg, $parent_source_comp = menu_category.title for menu_items)
						// the same is true for the destination
						if (count($a_parent_comparison) !== 0) {
							$parent_source_comp = FALSE;
							$parent_dest_comp = FALSE;
							foreach($a_parent_comparison['parent_source'] as $a_parent_source)
								if ($a_parent_source[$a_parent_comparison['parent_ref']] == $a_source_val[$a_parent_comparison['child_ref']])
									$parent_source_comp = $a_parent_source[$a_parent_comparison['parent_index']];
							foreach($a_parent_comparison['parent_dest'] as $a_parent_dest)
								if ($a_parent_dest[$a_parent_comparison['parent_ref']] == $a_dest_val[$a_parent_comparison['child_ref']])
									$parent_dest_comp = $a_parent_dest[$a_parent_comparison['parent_index']];
						}

						// there's a match! Insert the match into $a_retval
						if (count($a_parent_comparison) !== 0 && $parent_source_comp !== FALSE && $parent_dest_comp !== FALSE) {
							if ( strtolower(trim($parent_source_comp)) == strtolower(trim($parent_dest_comp)) ) {
								echo($a_source_val[$s_index].' '.$a_dest_val[$s_index].' <>~<>~<>~<>~<> '.$parent_source_comp.' '.$parent_dest_comp.'<br />');
								$a_retval[$a_source_val['id']] = (int)$a_dest_val['id'];
								break;
							}
						} else {
							echo($a_source_val[$s_index].' '.$a_dest_val[$s_index].'<br />');
							$a_retval[$a_source_val['id']] = (int)$a_dest_val['id'];
							break;
						}
					}
				}

				// create the element
				if ($b_found_match === FALSE) {
					if (count($a_parent_comparison) == 0) {
						$a_retval[$a_source_val['id']] = array('type'=>'create', 'parent_table'=>'null');
					} else {
						$parent_source_comp = FALSE;
						foreach($a_parent_comparison['parent_source'] as $a_parent_source)
							if ($a_parent_source[$a_parent_comparison['parent_ref']] == $a_source_val[$a_parent_comparison['child_ref']])
								$parent_source_comp = $a_parent_source[$a_parent_comparison['parent_index']];
						if ($parent_source_comp !== FALSE)
							$a_retval[$a_source_val['id']] = array('type'=>'create', 'parent_table'=>$a_parent_comparison['parent'], 'parent_index'=>$a_parent_comparison['parent_index'], 'parent_value'=>$parent_source_comp);
					}
				}
			}
			return $a_retval;
		}

		// helper function of setDefaultSyncSettings()
		// update the reporting groups so that they are consisten across all elements
		// @$s_tablename: the name of the table from $a_tables_to_sync for this group of elements
		// @$a_elements:
		//     if the id of the reporting group is >0, it fills 'chain_reporting_group' with the id
		//     if the id is <=0 and there's a definition for that id, it fills 'chain_reporting_group' with that definition
		//     if the id is <=0 and there's no definition for that id, it fills 'chain_reporting_group' with 'none'
		// @returns:
		//     if elements are removed and are found to already exist, it returns the syncfrom_ids of those elements as an array
		private static function setReportingGroupMatch($s_tablename, &$a_tables_to_sync, &$a_elements, &$a_parent_source, &$a_in_matches) {

			// get the chain_reporting_group defaults of this group of elements
			$a_chain_reporting_group_defaults = &$a_tables_to_sync[$s_tablename]['reporting_group'];
			$CHAIN_REPORTING_GROUP = 'crg';

			// for matching child to parent
			// a match occurs when $a_parent_source[$i][$s_parent_ref] == $a_element[$s_child_ref]
			$s_parent_ref = $a_parent_source['parent_ref']; // the index of the parent that should match the value in child_ref
			$s_child_ref = $a_parent_source['child_ref']; // the index of element that points to the parent

			// for each element, check for chain_reporting_group defaults
			foreach($a_elements as $k=>$a_element) {

				// get the child's reporting group
				$i_child_chain_reporting_group = (int)$a_element[$CHAIN_REPORTING_GROUP];

				// ignore if >0
				if ($i_child_chain_reporting_group > 0) {
					$a_elements[$k][$CHAIN_REPORTING_GROUP] = $i_child_chain_reporting_group;
					continue;
				}

				// if a default is not set, assume it is 'none'
				if (!isset($a_chain_reporting_group_defaults[$i_child_chain_reporting_group])) {
					$a_elements[$k][$CHAIN_REPORTING_GROUP] = 'none';
					continue;
				}

				// does it default to the parent?
				if ($a_chain_reporting_group_defaults[$i_child_chain_reporting_group] == 'parent_default') {

					// get the parent reporting group id
					$i_parent_chain_reporting_group = 0;
					$a_parent_chain_reporting_group_defaults = array();
					if (count($a_parent_source) > 0 && isset($a_parent_source['parent_source'])) {

						// get the default reporting groups of the parent
						$s_parent_tablename = $a_parent_source['parent'];
						if (isset($a_tables_to_sync[$s_parent_tablename]) && isset($a_tables_to_sync[$s_parent_tablename]['reporting_group']))
							$a_parent_chain_reporting_group_defaults = $a_tables_to_sync[$s_parent_tablename]['reporting_group'];

						// find the parent
						$a_parent = array();
						foreach($a_parent_source['parent_source'] as $a_possible_parent) {
							if ($a_possible_parent[$s_parent_ref] == $a_element[$s_child_ref]) {
								$a_parent = $a_possible_parent;
								break;
							}
						}

						// get the parent reporting group id
						if (isset($a_parent['crg']))
							$i_parent_chain_reporting_group = (int)$a_parent['crg'];
					}

					if ($i_parent_chain_reporting_group <= 0) {
						if (isset($a_parent_chain_reporting_group_defaults[$i_parent_chain_reporting_group]) && $a_parent_chain_reporting_group_defaults[$i_parent_chain_reporting_group] != 'parent_default')
							$a_elements[$k][$CHAIN_REPORTING_GROUP] = $a_parent_chain_reporting_group_defaults[$i_parent_chain_reporting_group];
						else
							$a_elements[$k][$CHAIN_REPORTING_GROUP] = 'none';
					} else {
						$a_elements[$k][$CHAIN_REPORTING_GROUP] = $i_parent_chain_reporting_group;
					}
					continue;
				}

				// change to the default
				$a_elements[$k][$CHAIN_REPORTING_GROUP] = $a_chain_reporting_group_defaults[$i_child_chain_reporting_group];
			}

			// checks for and removes any elements that have a chain_reporting_group of 'none'
			$a_removed_elements = array();
			$a_to_remove = array();
			foreach($a_elements as $k=>$a_element) {
				if ($a_element[$CHAIN_REPORTING_GROUP] == 'none')
					$a_to_remove[] = $k;
			}
			foreach($a_to_remove as $k) {
				if (self::getRuleExists($a_in_matches, $a_elements[$k]['id'])) {
					$a_removed_elements[] = $a_elements[$k]['id'];
				}
				unset($a_elements[$k]);
			}

			return $a_removed_elements;
		}

		// helper function of setDefaultSyncSettings()
		//
		// index: what will be compared
		// extra_select: extra columns to look for in the select clause, typically to be used as the child_ref
		// parent: the parent table of this table
		// parent_ref: the column of the parent to match with the child_ref column of the child
		// child_ref: the column of the child to match with the parent_ref column of the parent
		// reporting_group: what special values in the reporting group id mean
		//     possibilities are 'none', 'all', or 'parent_default'
		//     note on 'parent_default': This only looks up one level, so
		//         if the menu item is parent default and the menu category is parent default,
		//         the reporting group gets set to 'none' rather than to the reporting group of menu group
		private static function getTablesToSync(&$a_retval) {
			// definitions
			$a_retval = array(
				'ingredient_categories'=>array('index'=>'title',
					'reporting_group'=>array(0=>'none', -1=>'all')),
				'ingredients'=>array('index'=>'title', 'extra_select'=>',`category`', 'parent'=>'ingredient_categories', 'parent_ref'=>'id', 'child_ref'=>'category',
					'reporting_group'=>array(0=>'parent_default', -1=>'all', -2=>'none')),
				'menus'=>array('index'=>'title', 'wherevars'=>array(),
					'reporting_group'=>array(0=>'none', -1=>'all')),
				'menu_groups'=>array('index'=>'group_name',
					'reporting_group'=>array(0=>'none', -1=>'all')),//, 'extra_select'=>',`menu_id`', 'parent'=>'menus', 'parent_ref'=>'id', 'child_ref'=>'menu_id'),
				'menu_categories'=>array('index'=>'name', 'extra_select'=>',`group_id`', 'parent'=>'menu_groups', 'parent_ref'=>'id', 'child_ref'=>'group_id',
					'reporting_group'=>array(0=>'none', -1=>'all')),
				'menu_items'=>array('index'=>'name', 'extra_select'=>',`category_id`', 'parent'=>'menu_categories', 'parent_ref'=>'id', 'child_ref'=>'category_id',
					'reporting_group'=>array(0=>'parent_default', -1=>'all', -2=>'none')),
				'happyhours'=>array('index'=>'name',
					'reporting_group'=>array(0=>'none', -1=>'all')),
				'super_groups'=>array('index'=>'title',
					'reporting_group'=>array(0=>'none', -1=>'all')),
				'modifier_categories'=>array('index'=>'title',
					'reporting_group'=>array(0=>'none', -1=>'all')),
				'modifiers'=>array('index'=>'title', 'extra_select'=>',`category`', 'parent'=>'modifier_categories', 'parent_ref'=>'id', 'child_ref'=>'category',
					'reporting_group'=>array(0=>'parent_default', -1=>'all', -2=>'none')),
				'forced_modifier_groups'=>array('index'=>'title',
					'reporting_group'=>array(0=>'none', -1=>'all')),//, 'extra_select'=>',`menu_id`', 'parent'=>'menus', 'parent_ref'=>'id', 'child_ref'=>'menu_id'),
				'forced_modifier_lists'=>array('index'=>'title',
					'reporting_group'=>array(0=>'none', -1=>'all')),//, 'extra_select'=>',`menu_id`', 'parent'=>'menus', 'parent_ref'=>'id', 'child_ref'=>'menu_id'),
				'forced_modifiers'=>array('index'=>'title', 'extra_select'=>',`list_id`', 'parent'=>'forced_modifier_lists', 'parent_ref'=>'id', 'child_ref'=>'list_id',
					'reporting_group'=>array(0=>'parent_default', -1=>'all', -2=>'none')),
			);
		}

		// helper function of setDefaultSyncSettings()
		// checks for matches in the syncfrom_id and reporting_group that already exist in this database
		//     so that they aren't rematched
		private static function getExistingMatches($s_tablename, $b_use_mlavu, &$a_restaurant, &$a_in_matches) {
			// check that there's not a manual match for them
			// if a manual match exists, don't auto-match
			// manual matches are a forwards-compatibility feature
			$a_manual_matches = ConnectionHub::getConn('poslavu')->getDBAPI()->getAllInTable('chain_replications', array('table_name'=>$s_tablename, 'match_type'=>'manual'), $b_use_mlavu,
				array('databasename'=>'poslavu_'.$a_restaurant['data_name'].'_db', 'selectclause'=>"`syncfrom_id`,`reporting_group`"));
			foreach($a_manual_matches as $a_manual_match)
				if (isset($a_in_matches[$a_manual_match['syncfrom_id']]))
					$a_in_matches[$a_manual_match['syncfrom_id']]['reporting_groups'][] = $a_manual_match['reporting_group'];
				else
					$a_in_matches[$a_manual_match['syncfrom_id']] = array('reporting_groups'=>array($a_manual_match['reporting_group']));

			// check that there's not an automatic match that already exists for them
			// if an automatic match exists, don't auto-match
			$a_automatic_matches = ConnectionHub::getConn('poslavu')->getDBAPI()->getAllInTable('chain_replications', array('table_name'=>$s_tablename, 'match_type'=>'automatic'), $b_use_mlavu,
				array('databasename'=>'poslavu_'.$a_restaurant['data_name'].'_db', 'selectclause'=>"`syncfrom_id`,`reporting_group`"));
			foreach($a_automatic_matches as $a_automatic_match)
				if (isset($a_in_matches[$a_automatic_match['syncfrom_id']]))
					$a_in_matches[$a_automatic_match['syncfrom_id']]['reporting_groups'][] = $a_automatic_match['reporting_group'];
				else
					$a_in_matches[$a_automatic_match['syncfrom_id']] = array('reporting_groups'=>array($a_automatic_match['reporting_group']));
		}

		// helper function of setDefaultSyncSettings()
		// computes a parent comparison for setReportingGroupMatch()
		// pass in all arrays by reference for efficiency
		// return the computed value in $a_parent_comparison
		private static function getParentComparison(&$a_table_vals, &$a_tables_to_sync, &$a_parent_table_vals, $b_use_mlavu, &$a_restaurant, &$a_other_restaurant, &$a_parent_comparison) {
			$s_parent = $a_table_vals['parent'];
			$s_parent_ref = $a_table_vals['parent_ref'];
			$s_child_ref = $a_table_vals['child_ref'];
			$a_parent_table_vals = &$a_tables_to_sync[$s_parent];
			$s_parent_index = $a_parent_table_vals['index'];
			$a_wherevars = isset($a_parent_table_vals['wherevars']) ? $a_parent_table_vals['wherevars'] : array();
			$a_parent_source = ConnectionHub::getConn('poslavu')->getDBAPI()->getAllInTable($s_parent, $a_wherevars, $b_use_mlavu,
				array('databasename'=>'poslavu_'.$a_restaurant['data_name'].'_db', 'selectclause'=>'`id`,`chain_reporting_group` AS `crg`,`'.$s_parent_index.'`'));
			$a_parent_dest = ConnectionHub::getConn('poslavu')->getDBAPI()->getAllInTable($s_parent, $a_wherevars, TRUE,
				array('databasename'=>'poslavu_'.$a_other_restaurant['data_name'].'_db', 'selectclause'=>'`id`,`chain_reporting_group` AS `crg`,`'.$s_parent_index.'`'));
			$a_parent_comparison = array('parent_index'=>$s_parent_index, 'parent'=>$s_parent, 'parent_ref'=>$s_parent_ref, 'child_ref'=>$s_child_ref, 'parent_source'=>$a_parent_source, 'parent_dest'=>$a_parent_dest);
		}

		// helper function of setDefaultSyncSettings()
		// checks if a rule already exists for the given element ID by looking in $a_in_matches for that id
		// if $s_reporting_group is set, it also checks that the element's reporting group is also in $a_in_matches
		// returns TRUE or FALSE
		private static function getRuleExists(&$a_in_matches, $i_element_id, $s_reporting_group = '', $s_syncfrom_id = '') {
			if (isset($a_in_matches[$i_element_id])) {
				if ($s_reporting_group == '') {
					return TRUE;
				} else if (in_array($s_reporting_group, $a_in_matches[$s_syncfrom_id]['reporting_groups'])) {
						return TRUE;
				}
			}
			return FALSE;
		}

		// helper function of setDefaultSyncSettings()
		// if the element is in the reporting group:
		//     if the other restaurant is in the reporting group:
		//         if the rule doesn't yet exist
		//             insert the rule into chain_replications
		// if the rule exists:
		//     remove the rule from chain_replications
		private static function insertOrDeleteRule(&$a_in_matches, &$a_element, &$a_reporting_group, $s_syncfrom_id, &$a_insert_vars, $s_element_reporting_group, &$a_other_restaurant, $s_syncfrom_database) {

			// check if the rule exists, already
			$b_rule_exists = self::getRuleExists($a_in_matches, $a_element['id'], $a_reporting_group['name'], $s_syncfrom_id);
			$a_insert_vars['reporting_group'] = $a_reporting_group['name'];
			$query_vars = array_merge(array('database'=>$s_syncfrom_database), $a_insert_vars);

			// check the reporting group
			if ($s_element_reporting_group == 'all' || (int)$s_element_reporting_group == (int)$a_reporting_group['id']) {
				$b_element_matches_reporting_group = TRUE;

				if (in_array($a_other_restaurant['id'], $a_reporting_group['contents'])) {

					// check if this rule exists already
					if ($b_rule_exists) {
						return;
					}

					// insert into the `chain_replications`
					$s_insert_clause = ConnectionHub::getConn('poslavu')->getDBAPI()->arrayToInsertClause($a_insert_vars);
					$s_query_string = "INSERT INTO `[database]`.`chain_replications` $s_insert_clause";
					$a_query_vars = array_merge(array('database'=>$s_syncfrom_database), $a_insert_vars);
					mlavu_query($s_query_string, $a_query_vars);
				}
			} else {
				$b_element_matches_reporting_group = FALSE;

				// remove the rule if it exists, since it shouldn't be applied to this reporting group any more
				if ($b_rule_exists) {
					$query_string = "DELETE FROM `[database]`.`chain_replications` WHERE `restaurant_syncto_id`='[restaurant_syncto_id]' AND `reporting_group`='[reporting_group]' AND `syncfrom_id`='[syncfrom_id]' AND `syncto_id`='[syncto_id]'";
					mlavu_query($query_string, $query_vars);
				}
			}
		}

		// helper function of setDefaultSyncSettings()
		// if $wt_similar_id is an array, it saves the array as an entry in the config table of the destination restaurant
		// returns -2 if $wt_similar_id is an array, 0 if it's invalid, or (int)$wt_similar_id otherwise
		public static function get_similar_id(&$wt_similar_id, $s_dest_dataname, $i_restaurant_id, $s_syncfrom_id, $s_tablename) {
			if ($wt_similar_id === NULL)
				return 0;
			if (is_array($wt_similar_id)) {
				$s_json = json_encode(array($wt_similar_id));
				$a_insert_vars = array('location'=>1, 'type'=>'special', 'setting'=>'create chain item', 'value'=>$i_restaurant_id, 'value2'=>$s_syncfrom_id, 'value_long'=>$s_json, 'value4'=>$s_tablename,
					'value3'=>json_encode(array('value'=>'rest_syncfrom_id', 'value2'=>'item_syncfrom_id', 'value_long'=>'instructions')) );
				$s_insert_clause = ConnectionHub::getConn('poslavu')->getDBAPI()->arrayToInsertClause($a_insert_vars);
				mlavu_query("INSERT INTO `[database]`.`config` $s_insert_clause",
					array_merge(array('database'=>'poslavu_'.$s_dest_dataname.'_db'), $a_insert_vars));
				return -2;
			} else {
				return (int)$wt_similar_id;
			}
		}

		// set sync settings for all categories and menu items for all chain locations based on name
		// primary restaurant id is set with the given restaurant id
		// @$a_reporting_groups: the reporting groups to create syncs to, if NULL assigns all reporting groups for the chain
		public static function setDefaultSyncSettings($i_restaurant_id, $a_reporting_groups = NULL) {

			// get all info necessary:
			//     $a_restaurant: the primary restaurant
			//     $a_chain: the chain
			//     $a_other_restaurants: other restaurant datanames in the chain
			$a_restaurant = get_restaurant_from_id($i_restaurant_id);
			if (count($a_restaurant) == 0)
				return 'no restaurant';
			$a_chain = get_restaurant_chain('', FALSE, $a_restaurant);
			if (count($a_chain) == 0)
				return 'no chain';
			$a_all_chain_restaurants = get_restaurants_by_chain($a_chain['id']);
			if (count($a_all_chain_restaurants) < 2)
				return 'no other restaurants';
			$a_other_restaurants = $a_all_chain_restaurants;
			foreach($a_other_restaurants as $k=>$a_other_restaurant)
				if ($a_other_restaurant['data_name'] == $a_restaurant['data_name'])
					unset($a_other_restaurants[$k]);
			$s_syncfrom_database = 'poslavu_'.$a_restaurant['data_name'].'_db';

			// find the reporting groups of this restaurant
			if ($a_reporting_groups === NULL)
				$a_reporting_groups = get_chain_reporting_groups($a_chain['id'], $a_all_chain_restaurants);

			// get the tables to sync and their properties
			//     (index, extra_select, parent, parent_ref, child_ref, reporting_group)
			$a_tables_to_sync = array();
			self::getTablesToSync($a_tables_to_sync);

			foreach($a_tables_to_sync as $s_tablename=>$a_table_vals) {
				$s_index = $a_table_vals['index'];
				$s_extra_select = isset($a_table_vals['extra_select']) ? $a_table_vals['extra_select'] : '';
				$a_wherevars = isset($a_table_vals['wherevars']) ? $a_table_vals['wherevars'] : array('_deleted'=>'0');
				$s_element_select_clause = "`id`,`chain_reporting_group` AS `crg`,`$s_index`$s_extra_select";

				// get the source rest's elements from the $s_tablename table
				// to match against the destination rest's elements and
				// check that there are any elements to match
				$a_elements = ConnectionHub::getConn('poslavu')->getDBAPI()->getAllInTable($s_tablename, $a_wherevars, TRUE,
					array('databasename'=>$s_syncfrom_database, 'selectclause'=>$s_element_select_clause));
				if (count($a_elements) == 0)
					return 'no elements';

				// check that there's not a match that already exists for them in chain_replications
				// insert the id of all existing matches into $a_in_matches as
				// 'id'=>array( 'reporting_groups'=>array('0'=>group1,'1'=>group2,...) )
				$a_in_matches = array();
				self::getExistingMatches($s_tablename, TRUE, $a_restaurant, $a_in_matches);

				// find menu items with similar names between this restaurant and the other restaurants in the chain
				$a_similar_ids = array();
				$a_to_delete_elements = array();
				foreach($a_other_restaurants as $a_other_restaurant) {
					$s_syncto_dataname = $a_other_restaurant['data_name'];

					// get the parent comparisons
					if (isset($a_table_vals['parent'])) {
						$a_parent_comparison = array();
						// compute the parent comparison
						// returns a value like the following:
						//     array('parent_index'=>'$s_parent_index', 'parent'=>'$s_parent', 'parent_ref'=>'$s_parent_ref', 'child_ref'=>'$s_child_ref',
						//         	'parent_source'=>array('databasename'=>'poslavu_parent_db', 'selectclause'=>'`id`,`s_parent_index`'),
						//        	'parent_dest'=>array('databasename'=>'poslavu_parent_db', 'selectclause'=>'`id`,`s_parent_index`'));
						self::getParentComparison($a_table_vals, $a_tables_to_sync, $a_parent_table_vals, TRUE, $a_restaurant, $a_other_restaurant, $a_parent_comparison);
					} else {
						$a_parent_comparison = array();
					}

					// check that the matches aren't marked as "none"
					// updates $a_elements with their reporting group match id and
					// removes any that are marked as "none", also removes them from the chain_replications table
					$a_to_delete_elements = array_merge(
						$a_to_delete_elements,
						self::setReportingGroupMatch($s_tablename, $a_tables_to_sync, $a_elements, $a_parent_comparison, $a_in_matches)
					);

					// compare with the child
					$a_other_elements = ConnectionHub::getConn('poslavu')->getDBAPI()->getAllInTable($s_tablename, $a_wherevars, TRUE,
						array('databasename'=>'poslavu_'.$s_syncto_dataname.'_db', 'selectclause'=>$s_element_select_clause));
					$a_similar_ids[$s_syncto_dataname] = self::find_ids_with_same_name($s_index, $a_elements, $a_other_elements,  $a_parent_comparison);
				}

				// delete elements that already exist and now have a new reporting group of 'none'
				if (count($a_to_delete_elements) > 0) {
					$s_in_clause = ConnectionHub::getConn('poslavu')->getDBAPI()->arrayToInClause($a_to_delete_elements, TRUE);
					$s_query_string = "DELETE FROM `[database]`.`chain_replications` WHERE `syncfrom_id` $s_in_clause AND `table_name`='[table]' AND `match_type`='automatic'";
					$a_query_vars = array('database'=>$s_syncfrom_database, 'table'=>$s_tablename);
					//echo ConnectionHub::getConn('poslavu')->getDBAPI()->drawQuery($s_query_string, $a_query_vars)."<br />";
					mlavu_query($s_query_string, $a_query_vars);
				}

				// add similar id's to the `[restaurant]`.chain_replications table,
				// once for each chain reporting group ('crg') that the element is a part of
				foreach($a_elements as $a_element) {
					$s_element_reporting_group = $a_element['crg'];
					$s_syncfrom_id = $a_element['id'];
					foreach($a_other_restaurants as $a_other_restaurant) {
						$s_other_dataname = $a_other_restaurant['data_name'];

						// get the id of the row to copy to and check that it exists (!=0)
						$wt_similar_id = isset($a_similar_ids[$s_other_dataname][$s_syncfrom_id]) ? $a_similar_ids[$s_other_dataname][$s_syncfrom_id] : NULL;
						echo "<pre>".print_r($wt_similar_id,TRUE)."</pre>";
						$i_similar_id = self::get_similar_id($wt_similar_id, $s_other_dataname, $i_restaurant_id, $s_syncfrom_id, $s_tablename);
						if ($i_similar_id === 0) continue;

						// create the values to be inserted and go through each reporting group,
						// checking that the restaurant is part of that group
						$a_insert_vars = array('table_name'=>$s_tablename, 'restaurant_syncto_id'=>$a_other_restaurant['id'], 'restaurant_syncto_dataname'=>$s_other_dataname, 'reporting_group'=>'replace_me', 'syncfrom_id'=>$s_syncfrom_id, 'syncto_id'=>$i_similar_id, 'match_type'=>'automatic', 'time_created'=>date('Y-m-d H:i:s'));
						foreach($a_reporting_groups as $a_reporting_group) {
							self::insertOrDeleteRule($a_in_matches, $a_element, $a_reporting_group, $s_syncfrom_id, $a_insert_vars, $s_element_reporting_group, $a_other_restaurant, $s_syncfrom_database);
						}
					}
				}

			}
		}
	}

	/********************************************************************************
	 * Used as an encapsulation for ajax calls to this file.
	 * Only methods encapsulated by this class callable be callable via ajax.
	 ********************************************************************************/
	class chainAjax {
		function get_chain_name() {
			global $maindb;
			$badval = 'print error[*note*]This restaurant has no chain';
			$s_username = postvars('username');
			$s_password = postvars('password');

			$cust_query = mlavu_query("select `dataname` from `[maindb]`.`customer_accounts` where `username`='[username]'",
				array('maindb'=>$maindb, 'username'=>$_POST['username']));
			$cust_read = mysqli_fetch_assoc($cust_query);
			$s_user_dataname = $cust_read['dataname'];
			$rdb = "poslavu_".$s_user_dataname."_db";

			// check that the logged in user has access
			$s_user_has_access = user_can_access_chains("join_chain");
			if ($s_user_has_access !== 'success')
				return $s_user_has_access;

			if (!check_user_access($rdb, $s_username, $s_password))
				return 'print error[*note*]Incorrect username or password[*command*]clear field[*note*]password[*command*]focus field[*note*]password';

			$a_chain = get_chain_by_username($s_username, $s_user_dataname);
			if (count($a_chain) == 0)
				return $badval;
			if ($a_chain['name'] == '')
				return $badval;

			$a_commands = array();
			$a_commands[] = 'change html[*note*]label.global[name=chain_name][*value*]"'.$a_chain['name'].'"[*value*]global';
			$a_commands[] = 'change value[*note*]input.global[name=username][*value*]'.$s_username.'[*value*]global';
			$a_commands[] = 'change value[*note*]input.global[name=password][*value*]'.$s_password.'[*value*]global';
			$a_commands[] = 'show field[*note*]joinChainDialog';
			return implode('[*command*]', $a_commands);
		}

		function create_chain() {
			global $maindb;

			session_start();
			$s_dataname = admin_info('dataname');
			$s_proposed_name = postvars('new_chain_name');
			$s_chain_name = find_unused_chain_name($s_proposed_name);
			$a_restaurant = get_restaurant_from_dataname($s_dataname);
			$s_username = admin_info('username');

			// check that the logged in user has access
			$s_user_has_access = user_can_access_chains("create_chain");
			if ($s_user_has_access !== 'success')
				return $s_user_has_access;

			if ($s_username === FALSE)
				return 'print error[*note*]Cannot find your username. Refresh the page and try again.';
			if (count($a_restaurant) == 0)
				return 'print error[*note*]Cannot find this restaurant. Refresh the page and try again.';
			if ($s_chain_name == '')
				return 'print error[*note*]Cannot find a similar unused chain name. Please choose a different name.';

			$a_insert_vars = array('name'=>$s_chain_name, 'primary_restaurantid'=>$a_restaurant['id'], 'created'=>date('Y-m-d H:i:s'), 'created_by'=>'client:'.$s_username);
			$s_insertClause = ConnectionHub::getConn('poslavu')->getDBAPI()->arrayToInsertClause($a_insert_vars);
			mlavu_query("INSERT INTO `[maindb]`.`restaurant_chains` ".$s_insertClause,
				array_merge(array('maindb'=>$maindb), $a_insert_vars));
			$i_chain_id = (int)mlavu_insert_id();

			$a_update_vars = array('chain_id'=>$i_chain_id, 'chain_name'=>$s_chain_name);
			$s_updateClause = ConnectionHub::getConn('poslavu')->getDBAPI()->arrayToUpdateClause($a_update_vars);
			$a_where_vars = array('id'=>$a_restaurant['id']);
			$s_whereClause = ConnectionHub::getConn('poslavu')->getDBAPI()->arrayToWhereClause($a_where_vars);
			mlavu_query("UPDATE `[maindb]`.`restaurants` ".$s_updateClause.' '.$s_whereClause,
				array_merge(array('maindb'=>$maindb), $a_update_vars, $a_where_vars));

			insert_into_chain_history($i_chain_id,array( 'created'=>array(date('Y-m-d H:i:s')=>$s_dataname)));
			error_log("The chain id created is: ". $i_chain_id." sdataname is: ". $s_dataname);
			loyaltree_update($i_chain_id, $s_dataname);
			return 'print success[*note*]Reloading page...[*command*]reload page';
		}

		function join_chain() {
			session_start();
			global $maindb;
			$s_dataname = admin_info('dataname');
			$s_username = postvars('username');
			$s_password = postvars('password');

			// check that the logged in user has access
			$s_user_has_access = user_can_access_chains("join_chain");
			if ($s_user_has_access !== 'success')
				return $s_user_has_access;

			$cust_query = mlavu_query("select `dataname` from `[maindb]`.`customer_accounts` where `username`='[username]'",
				array('maindb'=>$maindb, 'username'=>$_POST['username']));
			$cust_read = mysqli_fetch_assoc($cust_query);
			$s_user_dataname = $cust_read['dataname'];
			$rdb = "poslavu_".$s_user_dataname."_db";

			if (!check_user_access($rdb, $s_username, $s_password))
				return 'print error[*note*]Incorrect username or password[*command*]clear field[*note*]password[*command*]focus field[*note*]password';

			$a_chain = get_chain_by_username($s_username, $s_user_dataname);
			if (count($a_chain) == 0)
				return 'print error[*note*]The given user doesn\'t have an associated restaurant chain[*command*]clear field[*note*]username[*command*]clear field[*note*]password[*command*]focus field[*note*]username';

			$a_update_vars = array('chain_id'=>$a_chain['id'], 'chain_name'=>$a_chain['name']);
			$s_updateClause = ConnectionHub::getConn('poslavu')->getDBAPI()->arrayToUpdateClause($a_update_vars);
			mlavu_query("UPDATE `[maindb]`.`restaurants` ".$s_updateClause." WHERE `data_name`='[data_name]'",
				array_merge(array('maindb'=>$maindb, 'data_name'=>$s_dataname), $a_update_vars));
				
			loyaltree_update($a_chain, $s_dataname);	

			addRemoveChainedGiftCard($a_chain['id'], $s_dataname ); #merge gift card while making chain

			insert_into_chain_history(
				$a_chain['id'],
				array( 'joined'=>array(date('Y-m-d H:i:s')=>$s_dataname) )
			);

			return 'print success[*note*]Reloading...[*command*]reload page';
		}

		function remove_chain($s_dataname = '') {
			session_start();
			global $maindb;
			$s_dataname = ($s_dataname != '') ? $s_dataname : admin_info('dataname');

			// check that the logged in user has access
			$s_user_has_access = user_can_access_chains("remove_chain");
			if ($s_user_has_access !== 'success')
				return $s_user_has_access;

			// remove the restaurant
			require_once('/home/poslavu/public_html/admin/sa_cp/manage_chain_functions.php');
			$b_success = remove_restaurant_from_chain($s_dataname, $s_error);
			if (!$b_success && $s_error == '')
				$s_error = 'Unknown error';

			// check that the removal was successful
			if ($s_error != '')
				return 'print error[*note*]'.$s_error;
			return 'print success[*note*]Reloading...[*command*]reload page';

			error_log('removing location dataname is: '. $s_dataname);
			loyaltree_update("0", $s_dataname);
		}

		// deletes a chain if there are no more restaurants in it
		function updateRestaurantChain($s_chain_id) {
			require_once('/home/poslavu/public_html/admin/sa_cp/manage_chain_functions.php');
			update_restaurant_chain_check_if_deleted($s_chainid);
		}

		function get_new_chain_name() {
			$s_dataname = postvars('dataname');
			return 'print error[*note*]'.find_unused_chain_name($s_dataname);
		}

		function draw_query($s_query, $a_vars) {
			foreach($a_vars as $k=>$v)
				$s_query = str_replace("[$k]", $v, $s_query);
			return $s_query;
		}
	}

	if (!function_exists('is_dev')) {
		function is_dev() {
			return (strpos($_SERVER['REQUEST_URI'],"v2/") !== FALSE) ? TRUE : FALSE;
		}
	}

	if (!isset($_POST['posted']) && isset($_GET['mode']) && $_GET['mode']=='settings_chain_management') {
		echo drawChainJavascript();
		echo '<table><tr><td style="text-align:center;">
			<div style="text-align:left;width:350px;">
				'.drawCreateJoinChain(admin_info('dataname')).'
			</div>
		</td></tr></table>';
	} else if (isset($_GET['bentest'])) {
		// check that I'm logged in (so nobody non-lavu can use this file)
		if (admin_info("loggedin") != '') {
			if (isset($_GET['dosync'])) {
				echo '<pre>';
				print_r(syncFunc::syncFromRestaurant('DEV', "it's a group!", TRUE));
				echo '</pre>';
			} else if (isset($_GET['get_table_columns'])) {
				echo get_table_columns($_GET['get_table_columns']);
			} else if (isset($_GET['get_chain_history'])) {
				print_r(get_chain_history($_GET['get_chain_history']));
			} else if (isset($_GET['update_chain_histories'])) {
				if ($_GET['update_chain_histories'] != '')
					update_chain_histories($_GET['update_chain_histories']);
				else
					for ($i = 0; $i < 1000; $i++)
						update_chain_histories($i);
			} else {
				echo syncFunc::setDefaultSyncSettings(5);
			}
		}
	} else if (isset($_POST['chain_management_ajax'])) {
		function postvars($s_name, $s_default = '') {
			return isset($_POST[$s_name]) ? $_POST[$s_name] : $s_default;
		}

		$s_command = postvars('command');
		$o_funcs = new chainAjax();

		if ($s_command == '') {
			echo 'asdf';
		} else if (method_exists($o_funcs, $s_command)) {
			echo $o_funcs->$s_command();
		} else {
			echo 'bad command';
		}
	}
	function loyaltree_update($chain_id, $data_name){
		lavu_connect_dn($data_name,'poslavu_'.$data_name.'_db');
		if(mysqli_num_rows(lavu_query("SELECT * FROM `poslavu_[1]_db`.`config` where `setting`='enable_loyaltree'", $data_name))){
			require_once(dirname(__FILE__).'/../../../inc/loyalty/loyaltree/bluelabel/LoyalTreeBlueLabel.php');
			$res = mysqli_fetch_assoc(mlavu_query("SELECT * FROM `poslavu_MAIN_db`.`restaurants` WHERE `data_name`='[1]' ", $data_name));
			LoyalTreeBlueLabel::update_chain(array("businessid"=>$res['id'], "chainid"=>$chain_id));
		}
	}

?>
