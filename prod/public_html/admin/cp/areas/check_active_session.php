<?php

require_once(__DIR__."/../resources/session_functions.php");
standarizeSessionDomain();

session_start();
require_once(__DIR__."/../resources/core_functions.php");


// Enables CORS for CP3 Domain
header("Access-Control-Allow-Origin: ".$_SERVER['CP3_URL']);
header("Access-Control-Allow-Credentials: true");

if (!admin_loggedin()) {
  http_response_code(404);
  echo "Not logged in";
} else {
  echo "OK";
}

exit();
