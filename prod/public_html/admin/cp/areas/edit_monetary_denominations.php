<?php
	if (!isset($mode))
		if (isset($_GET['mode']))
			$mode = $_GET['mode'];
		else
			$mode = "";
	
	if (isset($_GET['edit']) && $mode == "edit_monetary_denominations") {
	//	ini_set("display_errors","1");
		// save
		if (isset($_POST['posted'])) {
			lavu_query("DELETE FROM `config` WHERE `setting` = 'monetary_denomination'");
			for ($denomination_index = 1; isset($_POST['ic_1_value_'.$denomination_index]); $denomination_index++) {
				$type = $_POST['ic_1_value_'.$denomination_index];
				$value = $_POST['ic_1_value2_'.$denomination_index];
				// force them to conform to our standards
				preg_match('/[0-9]*[\.,]?[0-9]*/', $value, $matches, PREG_OFFSET_CAPTURE);
				if (count($matches) > 0)
					$value = $matches[0][0];
				else
					$value = "";
				// check if it's deleted
				if (isset($_POST['ic_1_delete_'.$denomination_index]))
					if ($_POST['ic_1_delete_'.$denomination_index] == '1')
						$value = "";
				if ($value != "")
					lavu_query("INSERT INTO `config` (`setting`,`type`,`value`,`value2`) VALUES ('monetary_denomination', 'location_config_settings', '[1]', '[2]')", $type, $value);
			}
		}
		get_db_denominations();
	
		echo speak("Add or remove denominations for the values of bills and coins in your monetary system.") . "<br />" . speak("Default denominations are for United States currency.");
		
		echo "<br /><br />";
		
		$display = "";
		$field = "";
		
		$qtitle_category = false;
		$qtitle_item = "Bill/Coin";
		
		$category_tablename = "";
		$inventory_tablename = "config";
		$category_fieldname = "";
		$inventory_fieldname = "setting";
		$inventory_select_condition = " AND `setting` = 'monetary_denomination'";
		$inventory_primefield = "setting";
		$category_primefield = "";
							
		$inventory_orderby = "value";

		$qfields = array();
		$qfields[] = array("monetary_denomination", "monetary_denomination", "Monetary Denomination", "hidden", "monetary_denomination");
		$qfields[] = array("value", "value", speak("Type"), "select", array(array(speak("Bill"),"bill"), array(speak("Coin"),"coin")));
		$qfields[] = array("value2", "value2", speak("Value"), "input");
		$qfields[] = array("delete");
		
		require_once(resource_path() . "/dimensional_form.php");
		$qdbfields = get_qdbfields($qfields);
		
		$details_column = "";//"Zip Codes";
		$send_details_field = "";//"zip_codes";
	
		$qinfo = array();
		$qinfo["qtitle_category"] = $qtitle_category;
		$qinfo["qtitle_item"] = $qtitle_item;
		$qinfo["category_tablename"] = $category_tablename;
		$qinfo["inventory_tablename"] = $inventory_tablename;
		$qinfo["category_fieldname"] = $category_fieldname;
		$qinfo["inventory_fieldname"] = $inventory_fieldname;
		$qinfo["inventory_select_condition"] = $inventory_select_condition;
		$qinfo["inventory_primefield"] = $inventory_primefield;
		$qinfo["category_primefield"] = $category_primefield;
		$qinfo["details_column"] = $details_column;
		$qinfo["send_details"] = $send_details_field;
		$qinfo["field"] = $field;
		$qinfo["qsortby_preference"] = " asc, `value2`*1 desc";
		
		$qinfo["category_filter_by"] = $category_filter_by;
		$qinfo["category_filter_value"] = $category_filter_value;
		$qinfo["inventory_filter_by"] = "setting";
		$qinfo["inventory_filter_value"] = "monetary_denomination";
		
		$qinfo["inventory_orderby"] = $inventory_orderby;
		
		$form_name = "tilling";
		$field_code = create_dimensional_form($qfields,$qinfo,$cfields);
		$display .= "<table cellspacing='0' cellpadding='3'><tr><td align='center' style='border:2px solid #DDDDDD'>";
		$display .= "<form name='tilling' method='post' action=''>";
		$display .= "<input type='hidden' name='posted' value='1'>";
		$display .= $field_code;
		$display .= "&nbsp;<br><input type='submit' value='".speak("Save")."' style='width:120px'>";
		$display .= "</form>";
		$display .= "</td></tr></table>";
		
		echo $display;
	}
	
	// returns a dictionary (bills, coins) of denominations, from largest to smallest
	//      eg array("bills"=>array("100","50"...), "coins"=>array("1.00",".50",...))
	// if no denominations exist, it creates the default english denominations
	function get_db_denominations() {
		$den_query = lavu_query("SELECT `value`,`value2` FROM `config` WHERE `setting` = 'monetary_denomination' AND `type` = 'location_config_settings'");
		$has_den = FALSE;
		
		if ($den_query)
			if (mysqli_num_rows($den_query) > 0)
				$has_den = TRUE;
		
		$bills = NULL;
		$coins = NULL;
		if ($has_den) {
			$bills = array();
			$coins = array();
			while ($den = mysqli_fetch_assoc($den_query)) {
				if ($den['value'] == "bill") {
					$bills[] = $den['value2'];
				} else if ($den['value'] == "coin") {
					$coins[] = $den['value2'];
				}
			}
		} else {
			$bills = array("100", "50", "20", "10", "5", "2", "1");
			$coins = array("1.00", ".50", ".25", ".10", ".05", ".01");
			foreach($bills as $bill)
				lavu_query("INSERT INTO `config` (`setting`,`type`,`value`,`value2`) VALUES ('monetary_denomination', 'location_config_settings', 'bill', '[1]')", $bill);
			foreach($coins as $coin)
				lavu_query("INSERT INTO `config` (`setting`,`type`,`value`,`value2`) VALUES ('monetary_denomination', 'location_config_settings', 'coin', '[1]')", $coin);
		}
		
		$retval = array();
		$retval['bills'] = $bills;
		$retval['coins'] = $coins;
		return $retval;
	}
	
	// fetches the denominations from get_db_denominations and returns them as a string "Bills: (bill1, bill2, ...)<br />Coins: (coin1, coin2, ...)<br />"
	function draw_db_denominations() {
		$den = get_db_denominations();
		$bills = $den['bills'];
		$coins = $den['coins'];
		
		if (count($bills) == 0 && count($coins) == 0) {
			return "There are no denominations to display";
		}
		
		$retval = "";
		
		$firstbill = TRUE;
		foreach($bills as $bill) {
			if ($firstbill) {
				$retval .= speak("Bills").": (";
				$firstbill = FALSE;
			} else {
				$retval .= ", ";
			}
			$retval .= $bill;
		}
		if (count($bills) > 0)
			$retval .= ")<br />";
			
		$firstcoin = TRUE;
		foreach($coins as $coin) {
			if ($firstcoin) {
				$retval .= speak("Coins").": (";
				$firstcoin = FALSE;
			} else {
				$retval .= ", ";
			}
			$retval .= $coin;
		}
		if (count($coins) > 0)
			$retval .= ")<br />";
		
		return $retval;
	}
?>
