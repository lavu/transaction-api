<?php

//Top Multiplexing
if( ($comPackage == '' || $comPackage == 'customer') && !empty($_POST['convert_to_pizza']) && $_POST['convert_to_pizza'] == 'true'){
    updateLocationToPizzaComponentPackage();
}else if($comPackage == '' || $comPackage == 'customer'){
    displayOptionToConvert();
}


function displayOptionToConvert(){
?>

    <form action='' method='POST'>
      <input type='hidden' name='convert_to_pizza' value='true' />
      <table border='0'>
          <!-- <tr><td><img src ='/images/customer_management_ipads_2_zoom.jpg'/></td></tr> -->

        <tr>
        	<td>
        		<div style='border:1px solid black; width:600px; height:130px; margin:0 auto;padding-top:10px;padding-left:20px;padding-right:20px; background-color:#eeeeee;'>
        		<!-- <span style='font-weight:bold'> This feature is not available for Local Server clients at this time.  Coming soon. </span><br><br> -->
        		<span>
				The Lavu Pizza Creator is a customizable graphic representation of a pizza on the order screen. 
                Users can place toppings on the whole pizza or either half, and an image of a pizza on the screen displays the toppings as they're added. 
                The Pizza Creator also accommodates modifiers beyond toppings, such as size, crust type, and sauce type. 
                These options are on one full size screen to allow employees to quickly and accurately create a pizza order. </span>
         <br><br><br><div style='text-align:center'> <input type='submit' value='Enable'/ style='font-size: 12px;padding: 10px;width: 100px;'></div>
        </td></tr>

      </table>
    </form>
    

<?php
}


function updateLocationToPizzaComponentPackage(){

    global $data_name;
    global $locationid;
	global $location_info;
		
    //require_once("/home/poslavu/public_html/admin/manage/misc/LLSFunctions.php");//Will need this to download all needed files to lls.
    $restaurantID = sessvar('admin_companyid');
    if(!empty($restaurantID)){
		if(isset($location_info) && isset($location_info['component_package'])) $comPackageNum = $location_info['component_package'];
		else $comPackageNum = "";
		
        ob_start();
        require_once(dirname(__FILE__) . "/../../sa_cp/tools/convert_product.php");
        update_account_product($restaurantID, 'Pizza');
        $garbage = ob_get_clean();
		
		if($comPackageNum!="")
		{
			$success = lavu_query("update `locations` set `component_package`='[1]' where `id`='[2]'",$comPackageNum,$locationid);
		}

        if(isClientOnLLS($data_name)){

            //schedule_msync($data_name, $locationid, "create_directory",array("/poslavu-local/...", "/poslavu-local/...", ... ));
            /*
              /////    quick breakdown of all files, using 'get_required_files' from pizza_mod_wrapper.
             /admin/dev/components/pizza/pizza_mod_wrapper.php
             /admin/resources/lavuquery.php
             /admin/dev/components/comconnect.php
             /admin/resources/core_functions.php
             /admin/dev/components/pizza/pizza_mod_shared_funcs.php
             /admin/objects/dbFunctions/DB_interface.php
             /admin/areas/pizza_settings.php
            */
        }
        ?>
            <br><br><br>
            <div style='width:400px'>
                Congratulations! Pizza is now enabled for your account. <a href=''> Click here to enter your pizza settings. </a>
            </div>
        <?php
    }
    return;
}

function isClientOnLLS($dataname){
    return false;
}

?>
