<?php
$basedir = "/home/poslavu/public_html/admin/cp/areas/meal_periods/";
?>
    <link href="areas/meal_periods/cp_areas.css" rel="stylesheet" type="text/css">
 <?php

// This page is responsible for drawing all of the various screens displayed for creation of meal_periods and meal_period_types
// For ajax requests (creation, modifcation, deletion), see request_action_meal_period['','_type'].php

include $basedir . "global_values_and_functions.php";

// creates a display box and fills it with an error
function print_error($errormsg)
{
	echo "<table class=\"center_with_width_400\"><tr><td class=\"full_td_red\">";
	echo $errormsg;
	echo "</td></tr></table>";
}

// create a mysql connection
// @username the username to connect with
// @password the password to connect with
function connection($username = "", $password = "")
{
	static $connection = NULL;
	if ($connection == NULL)
    {
			$connection = mysqli_connect("localhost", $username, $password);
			if (!$connection)
			{
					$connection = "";
					return FALSE;
			}
    }
	return $connection;
}

// Get the html to draw to the screen
// @screen The screen to draw. Choices are:
//     start_no_meal_period_types 
//       create_new_meal_period_type // all following screens drop to start_no_meal_period_types if there are no meal period types in "database"
//       modify_meal_period_type
//       confirm_meal_period_type_deletion
//     start_no_meal_periods 
//       create_new_meal_period // all following screens drop to start_no_meal_periods if there are no meal periods in "database"
//     start
//       view_meal_periods
//       confirm_meal_period_deletion
// @return the text to be printed to the screen
function draw($screen)
{
	global $meal_period_valid_pages;
	global $meal_periods_override_screens;

	// replaces command words with the necessary html text
	// @data an array of strings to look for command words in
	//   possible words are: %PAGEURL%, %MEAL_PERIOD_TYPES_AS_DROPDOWN%, %MEAL_PERIODS_AS_TABLE%, %MEALPERIODTYPESASOPTIONS%, %SHORTPAGEURL%, %NAME%
	// @return the array given in data, with commandwords replaced
	function interpret_commandwords($data)
	{
		global $basedir;
		global $locationid;

		$retdata = array();
		foreach($data as $line)
		{
				if (stripos($line, "%NAME%") !== FALSE)
				{
						$line = str_ireplace("%NAME%", $_GET["name"], $line);
				}
				if (stripos($line, "%DATABASE%") !== FALSE)
				{
						$line = str_ireplace("%DATABASE%", "ben_dev", $line);
				}
				// get the base web url translation of "$basedir"
				// eg /areas/meal_periods/
				if (stripos($line, "%BASEDIR%"))
				{
						$first_subdirectory = $_SERVER['PHP_SELF'];
						$first_subdirectory = explode("/", $first_subdirectory);
						$first_subdirectory = $first_subdirectory[1];
						$prefix = explode($first_subdirectory, $basedir);
						$prefix = $prefix[0];
						$bd = explode($prefix, $basedir);
						$bd = "/" . $bd[1];
						$line = str_ireplace("%BASEDIR%", $bd, $line);
				}
				// the exact current page url
				// eg: "google.com/balls?what=yourmamma"
				if (stripos($line, "%PAGEURL%") !== FALSE)
				{
						$line = str_ireplace("%PAGEURL%", curPageURL(), $line);
				}
				// the current page url, minus everything after a "?"
				// eg: "google.com/balls?what=yourmamma" -> "google.com/balls"
				if (stripos($line, "%PAGEURLMINUSGET%") !== FALSE)
				{
						$pageurl = curPageURL();
						$pu = explode("?", $pageurl);
						$newurl = $pu[0];
						$newurl = curPageURL(); //comment out this line to make the function work like it's supposed to, again
						$line = str_ireplace("%PAGEURLMINUSGET%", $newurl, $line);
				}
				// the current page url, minus the last page
				// eg "google.com/balls" -> "google.com/"
				if (stripos($line, "%SHORTPAGEURL%") !== FALSE)
				{
						$pageurl = curPageURL();
						$pu = explode("/", $pageurl);
						$newurl = $pu[0];
						for ($i = 1; $i < count($pu)-1; $i++)
								$newurl = $newurl . "/" . $pu[$i];
						$newurl = $newurl . "/" . $basedir;
						$newurl = curPageURL(); //comment out this line to make the function work like it's supposed to, again
						$line = str_ireplace("%SHORTPAGEURL%", $newurl, $line);
				}
				if (stripos($line, "%MEALPERIODTABLE%") !== FALSE)
				{
						$line = str_ireplace("%MEALPERIODTABLE%", meal_period_table(), $line);
				}
				if (stripos($line, "%MEALPERIODSORTOPTIONS%") !== FALSE)
				{
						$sort_options = array("Type", "Day", "Starting Time", "Ending Time");
						$options = "";
						$default = "Type";
						for ($i = 0; $i < count($sort_options); $i++)
						{
								$option = $sort_options[$i];
								$options = $options . "<option";
								if ($option == $default)
								{
										$options = $options . " selected='selected'";
								}
								$options = $options . ">" . $option . "</option>";
						}
						$line = str_ireplace("%MEALPERIODSORTOPTIONS%", $options, $line);
				}
				if (stripos($line, "%MEALPERIODTYPESASOPTIONS%") !== FALSE)
				{
						if (count(meal_period_types("meal_period_types")) == 0)
						{
								$line = str_ireplace("%MEALPERIODTYPESASOPTIONS%", "<option>N/A</option>", $line);
//								print_error("Error: attempt to access Meal Period Types when there are no Meal Period Types");
						}
						else
						{
							$mealPeriodTypesAsOptions = "";
							$mtps = meal_period_types("meal_period_types");
							$mtps = sort_array_by_field($mtps, "id");
							$highest_id = $mtps[count($mtps)-1]->id;
							$mtps = sort_array_by_field($mtps, "name");
							foreach ($mtps as $mpt)
							{
									$selected = FALSE;
									if ($mpt->id == $highest_id){$selected = TRUE;}
									$mealPeriodTypesAsOptions = $mealPeriodTypesAsOptions . $mpt->print_as_option($selected);
							}
							$line = str_ireplace("%MEALPERIODTYPESASOPTIONS%", $mealPeriodTypesAsOptions, $line);
						}
				}
				if (stripos($line, "%MEALPERIODTYPESASARRAY%") !== FALSE)
				{
						$line = str_ireplace("%MEALPERIODTYPESASARRAY%", mpts_as_javascriptarray(meal_period_types("meal_period_types")), $line);
				}
				if (stripos($line, "%MEALPERIODSASTABLEROWS%") !== FALSE)
				{
						$line = translate_meal_period_as_table_rows($line, "meal_periods");
				}
				if (stripos($line, "%LOCATIONID%") !== FALSE) {
						$line = str_ireplace("%LOCATIONID%", $locationid, $line);
				}
				$retdata[count($retdata)] = $line;
		}
		return $retdata;
	}

	// loads the html text from file "filename" and passes it to "interpret_commandwords" to be interpretted
	// returns the lines to be printed as plain text
	function draw_load_file($filename)
	{
		global $basedir;
		$lines = file($basedir . $filename);
		if ($lines === FALSE)
		{
				print_error("Error: File not found: " . $filename);
				return "";
		}
		
		$lines = interpret_commandwords($lines);
		$retval = "";
		foreach($lines as $line)
		{
				$retval = $retval . $line;
		}
		return $retval;
	}

	// check if this is an override screen
	// (override screen have no prerequisites to being drawn)
	for ($i = 0; $i < count($meal_periods_override_screens); $i++)
	{
			$os = $meal_periods_override_screens[$i];
			if ($os === $screen)
					return draw_load_file($screen . ".html");
	}

	// find the screen number corresponding to the screen provided
	// also validates the screen request
	//   if the screen request is not available, the function returns with a value of ""
	$screen_num = -1;
	for ($j = 0; $j < count($meal_period_valid_pages); $j++)
	{
			if ($meal_period_valid_pages[$j] === $screen)
			{
					$screen_num = $j;
					break;
			}
	}
	if ($screen_num === -1)
	{
			print_error("Error: Unknown screen " . $screen);
			return "";
	}
	
	// returns the screen to be drawn
	//   returns a lower level screen if there aren't the necessary resources in
	//   the mysql to validate the screen (eg, trying to access a list of meal_period_types when
	//   there are no meal_period_types, it drops back to create_new_meal_period_type)
	if ($screen_num <= 1 || count(meal_period_types("meal_period_types")) == 0)
    {
			if ($screen_num > 1)
			{
					return draw_load_file("start_no_meal_period_types.html");
			}
			else
			{
					return draw_load_file($screen . ".html");
			}
    }
	else if ($screen_num <= 4 || count(meal_periods("meal_periods")) == 0)
    {
			if ($screen_num > 4)
			{
					return draw_load_file("start_no_meal_periods.html");
			}
			else
			{
					return draw_load_file($screen . ".html");
			}
    }
	else
    {
			return draw_load_file($screen . ".html");
    }
}

function meal_periods_main()
{
	global $meal_period_valid_pages;
	global $meal_periods_override_screens;

	// interpret the page that the user wants to go to
	// O(n^2) operation, I know, but I don't (think?) that it should really matter
	$page = "start2";
	// the following was useful when this code was on it's own server
	/*foreach ($_GET as $key => $value)
	{
			// ~~~<>~~~<>~~~<>~~~<>~~~<>~~~<>~~~ standard actions ~~~<>~~~<>~~~<>~~~<>~~~<>~~~<>~~~
			// These are actions take by the user
			// ~~~<>~~~<>~~~<>~~~<>~~~<>~~~<>~~~<>~~~<>~~~<>~~~<>~~~<>~~~<>~~~<>~~~<>~~~<>~~~<>~~~
			for ($j = 0; $j < count($meal_period_valid_pages); $j++)
			{
					if ($key === $meal_period_valid_pages[$j])
					{
							$page = $key;
							break;
					}
			}
	}*/

	// draw the requested page and close the mysql connection once it is no longer needed
	//connection("bbean", "ben_lavu");//not needed on the live server
	$text = draw($page);
	echo $text;
	require_once("areas/scheduling_files/php/time_chart.php");
	//mysqli_close(connection());//not needed on the live server
}

meal_periods_main()
?>
