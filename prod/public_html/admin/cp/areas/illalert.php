<style type="text/css">
.main{
	width:80%;
	padding:10px;
}
.table {
	border:2px solid #ccc;
	width:100%
}
.table tr{
	border:1px solid #ccc;
	}
.table tr td{
	border:1px solid #ccc;
}

</style>
<?php
//require_once("areas/chain_management.php");
ini_set('display_errors',1);
$recepit_query=lavu_query("select value2 from config where type='printer' and setting like 'receipt%'");
while($rec = mysqli_fetch_assoc($recepit_query))
{
	$recc[]=$rec;
}
//print_r($recc);
$i=1;
foreach($recc as $reg){
	
$receipt=$reg[value2];
$register=$receipt;
$date1=date('Y-m-d');
$transitionOptions = array (
		array (
				0 => "Paid In",
				1 => "till_customization_settings_paid_in"
		),
		array (
				0 => "Paid Out",
				1 => "till_customization_settings_paid_out"
		),
		array (
				0 => "Cash",
				1 => "till_customization_settings_cash"
		),
		array (
				0 => "Card",
				1 => "till_customization_settings_credit_card"
		),
		array (
				0 => "Gift Certifcate",
				1 => "till_customization_settings_gift_certificate"
		),
		array (
				0 => "Till In",
				1 => "till_customization_settings_till_in"
		),
		array (
				0 => "Till Out",
				1 => "till_customization_settings_till_out"
		)
);

foreach ( $transitionOptions as $oneOption ) {

	$filed1 = $oneOption[0];
	$filed2 = $oneOption[1];

	$get_cash_data_info = lavu_query ( "SELECT setting,value,value2 FROM cash_data WHERE `type`='location_config_setting' AND
												`setting`='[1]' AND `value3` = 'summary_item'",$filed2 );
	$get_cash_data = mysqli_fetch_object ( $get_cash_data_info );
	$data [$filed1] [summary] = $get_cash_data->value;
	$data [$filed1] [tip] = $get_cash_data->value2;

	if(is_null($data [$filed1] [summary])) {
		$data [$filed1] [summary]='';
	}

	if(is_null($data [$filed1] [tip])){
		$data [$filed1] [tip] = '';
	}


	switch($filed1) {
		case 'Paid In':
		case 'Paid Out':
			$get_amount_tip = lavu_query ( "SELECT
													SUM(total_collected) AS amount,
													SUM(tip_amount) AS tip
												FROM cc_transactions
													WHERE order_id='[1]' AND
													datetime LIKE '[2] %' AND
													register_name ='[3]'",
													$filed1,
													$date1,
													$register);
			break;
		case 'Till In':
			$get_amount_tip = lavu_query ( "SELECT
													SUM(SUBSTRING_INDEX(value, ':',1)) AS amount,
													SUM(value_med) AS tip
												FROM cash_data
													WHERE setting like '%:[1]' AND
													date='[2] ' AND
													setting Like 'till_in_out_%' ",
													$register,
													$date1);
			break;
		case 'Till Out':
			$get_amount_tip = lavu_query ( "SELECT
												SUM(SUBSTRING_INDEX(value, ':',-1)) AS amount,
													SUM(value_med) AS tip
												FROM cash_data
													WHERE setting like '%:[1]' AND
													date='[2] ' AND
													setting Like 'till_in_out_%' ",
													$register,
													$date1);
			break;
		default:
			$get_amount_tip = lavu_query ( "SELECT
													SUM(total_collected) AS amount,
                                                    SUM(tip_amount) AS tip
                                                 FROM cc_transactions
                                                    WHERE pay_type='[1]' AND
                                                    datetime LIKE '[2]%' AND
                                                    register_name ='[3]'AND order_id NOT IN ('Paid In','Paid Out')",
                                                    $filed1,
                                                    $date1,
                                                    $register );
				
	}

	$get_amounts_tip = mysqli_fetch_object ( $get_amount_tip );

	if ($get_amounts_tip->amount != "") {
		$total_mount = $get_amounts_tip->amount;
	} else {
		$total_mount = '0';
	}

	if ($get_amounts_tip->tip != "") {
		$total_tip = $get_amounts_tip->tip;
	} else {
		$total_tip = '0';
	}

	$data [$filed1] [summary_amount] = $total_mount;
	$data [$filed1] [tip_amount] = $total_tip;
}
//echo "<pre>";
//print_r($data);
$total="0";
if($data['Paid In']['summary']=='+'){
	$total=$total+$data['Paid In']['summary_amount'];
}elseif($data['Paid In']['summary']=='-'){
	$total=$total-$data['Paid In']['summary_amount'];
}
if($data['Paid Out']['summary']=='+'){
	$total=$total+$data['Paid Out']['summary_amount'];
}elseif($data['Paid Out']['summary']=='-'){
	$total=$total-$data['Paid Out']['summary_amount'];
}
if($data['Cash']['summary']=='+'){
	$total=$total+$data['Cash']['summary_amount'];
}elseif($data['Cash']['summary']=='-'){
	$total=$total-$data['Cash']['summary_amount'];
}
if($data['Cash']['tip']=='+'){
	$total=$total+$data['Cash']['tip_amount'];
}elseif($data['Cash']['tip']=='-'){
	$total=$total-$data['Cash']['tip_amount'];
}
if($data['Card']['tip']=='+'){
	$total=$total+$data['Card']['tip_amount'];
}elseif($data['Card']['tip']=='-'){
	$total=$total-$data['Card']['tip_amount'];
}
$total=$total+$data['Till In']['summary_amount'];
$total=$total-$data['Till Out']['summary_amount'];

?>
<div class="main">
	<div> <b>Register<?php echo $i; ?> : <?php echo $receipt;?></b></div>
	<div>
		<table class="table"> 
			<tr>
				<td>Paid In</td>
				<td><?php echo $data['Paid In']['summary']; ?></td>
				<td><?php echo $data['Paid In']['summary_amount']; ?></td>
			</tr>
			<tr>
				<td>Paid Out</td>
				<td><?php echo $data['Paid Out']['summary']; ?></td>
				<td><?php echo $data['Paid Out']['summary_amount']; ?></td>
			</tr>
			<tr>
				<td>Cash </td>
				<td><?php echo $data['Cash']['summary']; ?></td>
				<td><?php echo $data['Cash']['summary_amount']; ?></td>
			</tr>
			<tr>
				<td>Cash Tip </td>
				<td><?php echo $data['Cash']['tip']; ?></td>
				<td><?php echo $data['Cash']['tip_amount']; ?></td>
			</tr>
			<tr>
				<td>Card Tip</td>
				<td><?php echo $data['Card']['tip']; ?></td>
				<td><?php echo $data['Card']['tip_amount']; ?></td>
			</tr>
			<tr>
				<td>Till In </td>
				<td><?php echo "+"; ?></td>
				<td><?php echo $data['Till In']['summary_amount']; ?></td>
			</tr>
			<tr>
				<td>Till Out </td>
				<td><?php echo "-"; ?></td>
				<td><?php echo $data['Till Out']['summary_amount']; ?></td>
			</tr>
			<tr>
				<td colspan="2" align="right"><b>Total Amount</b>  </td>
				<td ><?php echo $total; ?></td>
			</tr>
		</table>
	</div>	
</div>

<?php $i++; } ?>
