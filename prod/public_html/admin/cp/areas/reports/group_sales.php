<?php
ob_implicit_flush(true);
$_GET['user'] = 'all';
// prepare to load the file
ob_start();
// load the file
require_once(dirname(__FILE__).'/time_cards_obj.php');
// cleanup
$s_contents = ob_get_contents();
ob_end_clean();

class GROUP_SALES {

	public static $o_total_labor = NULL;
	public static $o_date_chooser = NULL;
	public static $o_total_payments = NULL;
	public static $a_user_labor_stats = NULL;

	// set the statistics on all users for the time period
	// depends on the global variable $info_array set from within 'time_cards.php'
	// @return: stores values in GROUP_SALES::$a_user_labor_stats
	public static function set_user_stats() {
		global $info_array;

		$a_to_remove = array();
		foreach($info_array as $k=>$v) {
			unset($info_array[$k]['html']);
			unset($info_array[$k]['details']);
			if ((int)$info_array[$k]['total_paid'] == 0)
				$a_to_remove[] = $k;
		}
		foreach($a_to_remove as $k)
			unset($info_array[$k]);

			echo "<div style='text-align:left;'><pre>";
			print_r($info_array);
			echo "</pre></div>";

			self::$a_user_labor_stats = $info_array;
	}

	// get the statistics on all users for the time period
	// @a_retval: if not null, then this will be used to return the array
	// @return: if a_retval is NULL, then the array will be returned
	public static function get_user_stats(&$a_retval = NULL) {
		if (!isset(self::$a_user_labor_stats))
			set_user_stats();

			if ($a_retval !== NULL)
				$a_retval = self::$a_user_labor_stats;
				else
					return self::$a_user_labor_stats;
	}



	// simple function to reduce complexity of code and increase performance
	public static function get_date_chooser_obj() {
		/*if (!isset(self::$o_date_chooser) || self::$o_date_chooser !== NULL) {
		 self::$o_date_chooser = get_date_chooser_obj();
		 self::$o_date_chooser->use_hour = isset($_GET['hour']) ? $_GET['hour'] : (int)date('H', strtotime(localize_datetime(date('Y-m-d H:i:s'), NULL)));
		 if (self::$o_date_chooser->use_hour == 'now') {
		 self::$o_date_chooser->hour_is_now = TRUE;
		 self::$o_date_chooser->use_hour = (int)date("H");
		 } else {
		 self::$o_date_chooser->hour_is_now = FALSE;
		 self::$o_date_chooser->use_hour = (int)self::$o_date_chooser->u0se_hour;
		 }
		 self::$o_date_chooser->hour_is_set = isset($_GET['hour']) ? TRUE : FALSE;
			}
			return self::$o_date_chooser;*/
		//return TimeCardUserFunctions::get_date_chooser_obj( $_GET['mode'] );
		if (!isset(self::$o_date_chooser) || self::$o_date_chooser !== NULL) {
			self::$o_date_chooser = TimeCardUserFunctions::get_date_chooser_obj( $_GET['mode'] );
			self::$o_date_chooser->use_hour = isset($_GET['hour']) ? $_GET['hour'] : (int)date('H', strtotime(localize_datetime(date('Y-m-d H:i:s'), NULL)));
			if (self::$o_date_chooser->use_hour == 'now') {
				self::$o_date_chooser->hour_is_now = TRUE;
				self::$o_date_chooser->use_hour = (int)date("H");
			} else {
				self::$o_date_chooser->hour_is_now = FALSE;
				self::$o_date_chooser->use_hour = (int)self::$o_date_chooser->use_hour;
			}
			self::$o_date_chooser->hour_is_set = isset($_GET['hour']) ? TRUE : FALSE;
		}

		return self::$o_date_chooser;
	}

	// draws the header for this report
	// @return: html for the header
	public static function draw_header() {

		// initialize variables
		$s_retval = '';
		$s_retval .= 'Report: <b>Group Sales </b><br /><br />';

		// get the necessary contents
		$o_date_chooser = self::get_date_chooser_obj();
		$s_select_clause = trim($o_date_chooser->select_clause);

		// update the select clause to choose either the end date or end hour
		$s_date = $o_date_chooser->use_date;
		$s_hour = $o_date_chooser->use_hour;
		$s_hour_get = $o_date_chooser->hour_is_set ? "&hour=$s_hour" : "";
		$s_hour_code = $o_date_chooser->hour_is_set ? "
				<script type='text/javascript'>
					setTimeout(function() {
						$('#selectByDayOrHour').val('hour');
						$('#date2day').hide();
						$('#date2hour').show();
					}, 100);
				</script>" : "";
		// refresh every five minutes if by hour and hour is now
		if ($o_date_chooser->hour_is_now && $o_date_chooser->hour_is_set)
			$s_hour_code .= "
				<script type='text/javascript'>
					setTimeout(function() {
						window.location='index.php?".$o_date_chooser->forward_vars."&hour=now';
					}, 300000);
				</script>";
			echo '<pre style="text-align: left;">' . htmlspecialchars( $a_select_clause ) . '</pre>';
			$a_selects = explode('<select', $s_select_clause);
			$a_date1_selects = explode("&date=", $a_selects[1]);
			$s_select_clause;// = $a_selects[0]."
			//<select".$a_date1_selects[0]."$s_hour_get&date=".$a_date1_selects[1];
			if (DEV)
				$s_select_clause1 .= "
				<select id='selectByDayOrHour' onchange='
					a= null;
					b = null;
					if (this.value == \"day\") {
						a = $(\"#date2day\");
						b = $(\"#date2hour\");
					} else {
						a = $(\"#date2hour\");
						b = $(\"#date2day\");
					}
					a.show();
					b.hide();
					setTimeout(function() {
						a.change();
					}, 100);
					'".(DEV ? '' : "style='display:none;'").">
					<option value='day'>By Day</option>
					<option value='hour'>By Hour</option>
				</select>";
				$s_select_clause .= "
				<select id='date2day' style='display: none;'></select>
				<select id='date2hour' onchange='window.location=\"index.php?".$o_date_chooser->forward_vars."&hour=\"+this.value;' style='".(($o_date_chooser->hour_is_set)?"":"display:none;")."'>
					<option value='now'>This Hour</option>";
				for($i = 0; $i < 24; $i++) {
					$i_hour = $i % 24;
					$s_selected = ($i == (int)$s_hour && !$o_date_chooser->hour_is_now) ? " SELECTED" : "";
					$s_select_clause .= "
					<option value='$i_hour'$s_selected>".date("ga",strtotime("2013-01-01 $i_hour:00:00"))."</option>";
				}
				$s_select_clause .= "
				</select>
				$s_hour_code";

				$s_retval .= $s_select_clause;
				return $s_retval;
	}

	// returns an object with the total payments for the given date
	public static function get_total_payments() {
		global $data_name;
		global $location_info;
		$o_date_chooser = self::get_date_chooser_obj();
		$report_settings = TimeCardAccountSettings::settings();
		$s_hour_start = '00:00:00';
		$s_hour_end = '99:99:99';
		if ($o_date_chooser->hour_is_set) {
			$s_hour_start = str_pad($o_date_chooser->use_hour, 2, '0', STR_PAD_LEFT).':00:00';
			$s_hour_end = str_pad($o_date_chooser->use_hour+1, 2, '0', STR_PAD_LEFT).':00:00';
		}
		$current_tz = date_default_timezone_get();
		if( !empty( $location_info['timezone'] )){
			date_default_timezone_set( $location_info['timezone'] );
		}
		$s_date_start = date('Y-m-d H:i:s', $report_settings->start_time);//$o_date_chooser->use_date.' '.$s_hour_start;
		$s_date_end = date('Y-m-d H:i:s', $report_settings->end_time);//$o_date_chooser->use_date2.' '.$s_hour_end;
		date_default_timezone_set( $current_tz );
		$s_date_start = $o_date_chooser->use_date1." 00:01:00";
		$s_date_end	= $o_date_chooser->use_date2." 00:01:00";
		// do this rediculous query
		$group_string= "select SQL_NO_CACHE  `menu_groups`.`group_name` AS `title`,  round(sum((order_contents.price + order_contents.modify_price + order_contents.forced_modifiers_price)*order_contents.quantity - if( order_contents.quantity > '0', if(order_contents.discount_amount<(order_contents.price + order_contents.modify_price +
  		order_contents.forced_modifiers_price)*order_contents.quantity,order_contents.discount_amount,(order_contents.price + order_contents.modify_price +  order_contents.forced_modifiers_price)*order_contents.quantity) + order_contents.idiscount_amount + order_contents.itax, 0)),2) as net  from `[database]`.`orders` LEFT JOIN `[database]`.`order_contents` ON `orders`.`order_id` = `order_contents`.`order_id` LEFT JOIN `[database]`.`menu_items` ON `order_contents`.`item_id` = `menu_items`.`id` LEFT JOIN `[database]`.`menu_categories` ON `menu_items`.`category_id` = `menu_categories`.`id` LEFT JOIN `[database]`.`menu_groups` ON `menu_categories`.`group_id` = `menu_groups`.`id`where `orders`.`void` = 0 and `order_contents`.`loc_id` = `orders`.`location_id` and `order_contents`.`item_id` > 0 and (`order_contents`.`quantity` * 1) >0 and `orders`.`closed` >= '[date_start]' and `orders`.`closed` <= '[date_end]' group by `menu_groups`.`group_name`";
  		$a_query_vars = array('database'=>'poslavu_'.$data_name.'_db', 'location_id'=>$location_info['id'], 'date_start'=>$s_date_start, 'date_end'=>$s_date_end);
		$total_net="0";
		$group_query = rpt_query($group_string,$a_query_vars);
		while ($group_data_fetch = mysqli_fetch_assoc($group_query)) {
			$group_data[]=$group_data_fetch;
			$total_net=$total_net+$group_data_fetch[net];
		}
		//echo"<pre>".$total_net; print_r($group_data);
		$o_retval[net]=$total_net;
		$o_retval[data]=$group_data;
		return $o_retval;
	}

	// draws the table representing the report
	public static function draw_report() {

		global $location_info;
		global $s_cant_display_error;

		// get the necessary data and check that it loaded correctly
		$o_total_payments = self::get_total_payments();
		//echo "</pre>"; print_r($o_total_payments);
		$netsales=$o_total_payments[net];

		if ($netsales==0) {
			$s_cant_display_error = "There were no Group Sales  made during this time.";
			return $s_cant_display_error;
		}

		// the table class
		$s_class = '
				<style scoped>
					table.summary_table {
						border-collapse: collapse;
					}
					table.summary_table th {
						border-bottom: 1px solid black;
						padding: 0 5px 0 5px;
					}
					table.summary_table td.money {
						text-align: right;
					}
					table.summary_table td {
						padding: 10px;
					}
					table.summary_table line {
						border-top: 1px solid black;
						padding: 0 5px 0 5px;
					}
					.total {
						border-top: 1px solid black;
					}
				</style>';
		// the total payments table
		$s_payments_table = '';
		$s_payments_table .= '<table class="summary_table"><thead><tr>
				<th>Group</th>
				<th>Net Sales</th>
				<th>Percentage</th>
				</tr></thead>';

		foreach($o_total_payments[data] as 	$o_data){
			$precentage=
			$s_payments_table .= '<tbody><tr>
				<td>'.$o_data[title].'</td>
				<td class="money">'.display_money($o_data[net],$location_info).'</td>
				<td class="money">'.number_format(($o_data[net]/$netsales)*100 ,2,".","").'%</td>
				</tr></tbody>';
		}
		$s_payments_table .= '<tbody  class="line total"><tr>
				<td>Totals</td>
				<td class="money">'.display_money($netsales,$location_info).'</td>
				<td class="money">100%</td>
				</tr></tbody>';
		$s_payments_table .='</table>';

		$s_retval = '';
		$s_retval .= $s_class;
		//$s_retval .= '<b>Labor Report</b><br />';
		$s_retval .= $s_payments_table;

		return $s_retval;
	}


}

if ($in_lavu) {

	$draw = '';
	$s_report = GROUP_SALES::draw_report();
	$s_header = GROUP_SALES::draw_header();
	//$s_graph .= 	LABOR::draw_graph();

	$s_start = '<div id="payments_in_vs_labor_start" style="display:none;"></div>';
	$s_end = '<div id="payments_in_vs_labor_end" style="display:none;"></div>';

	$draw .= $s_start.$s_graph.'<br /><br />'.$s_header.'<br /><br /><br />'.$s_report.$s_end;

	echo $draw;
}
?>
