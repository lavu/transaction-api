<?php
session_start ();
$filePath = dirname ( __FILE__ );
$dirReportPath = dirname(dirname(dirname(__FILE__)));
require_once ($dirReportPath . "/resources/core_functions.php");
require_once ($dirReportPath . "/reports_v2/report_functions.php");
require_once ($dirReportPath . "/reports_v2/area_reports.php");
require_once ($dirReportPath . "/reports_v2/constants/reportConstants.php");

$output_mode = (isset ( $_GET ['output_mode'] )) ? $_GET ['output_mode'] : "";
$selfServe = reqvar('selfserve', '');
$isSelfServe = $selfServe !== '';
$selfServePath = $filePath . '/'.$selfServe.'.php';

if (isset ( $_GET ['report'] ) && substr ( $_GET ['report'], 0, 11 ) == "autodirect:") {
	require_once (dirname ( __FILE__ ) . "/../" . substr ( $_GET ['report'], 11 ) . ".php");
	return;
}

global $data_name;
if (isset ( $_GET ['dataname'] )) {
	$data_name = $_GET ['dataname'];
	set_sessvar ( "admin_dataname", $data_name );
} else {
	$data_name = sessvar ( 'admin_dataname' );
}

$report = ((isset ( $_GET ['report'] ) && ! empty ( $_GET ['report'] )) ? $_GET ['report'] : "test_report");

if ($output_mode === OUTPUT_MODE_CSV_TO_EMAIL) {
	require_once ($dirReportPath .'/areas/reports/register_email_report.php');
	exit;
}

if ($isSelfServe && file_exists($selfServePath)) {
	$ar = new area_reports ([
		"report" => $report,
		'display_query' => 1 
	]);
	require_once $selfServePath;

}	else if ($output_mode == "") {
	$hstr = (isset($hstr)&&!empty($hstr))?$hstr:"";
	$ar = new area_reports ( array (
			"report" => $report,
			'display_query' => 1 
	) );
	echo <<<HTML
			{$hstr}
			<style>
				.ajax_overlay_greyed_out {
					opacity:.40;
					filter: alpha(opacity=40);
					-moz-opacity: 0.4;
					width:100%;
				}
				.ajax_overlay {
				}
			</style>
			<table width='100%'>
				<tbody>
					<tr>
						<td width='100%' align='center' valign='top'>
							<div id='report_container'>
								{$ar->show_query_table()}
							</div>
						</td>
					</tr>
				</tbody>
			</table>
HTML;
} else {
	$ar = new area_reports([
		"report" => $report,
		"output_mode" => $output_mode 
	]);
	switch ($output_mode) {
		case OUTPUT_MODE_JSON :
			require_once (dirname ( dirname ( dirname ( __FILE__ ) ) ) . "/reports_v2/exporter_report_drawer_json.php");
			$ar->report_drawer = new exporter_report_drawer ( $ar );
		case OUTPUT_MODE_CSV :
		default :
			require_once (dirname ( dirname ( dirname ( __FILE__ ) ) ) . "/reports_v2/exporter_report_drawer.php");
			$ar->report_drawer = new exporter_report_drawer ( $ar );
			break;
	}
	$export_str = $ar->show_query_table ();
}
