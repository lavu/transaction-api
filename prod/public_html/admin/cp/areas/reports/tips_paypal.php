<?php
ob_implicit_flush(true);
$_GET['user'] = 'all';
// prepare to load the file
ob_start();
// load the file
require_once(dirname(__FILE__).'/time_cards_obj.php');
// cleanup
$s_contents = ob_get_contents();
ob_end_clean();

class TIPSPAYPAL {

	public static $o_date_chooser = NULL;
	
	// simple function to reduce complexity of code and increase performance
	public static function get_date_chooser_obj() {
		
		if (!isset(self::$o_date_chooser) || self::$o_date_chooser !== NULL) {
			self::$o_date_chooser = TimeCardUserFunctions::get_date_chooser_obj( $_GET['mode'] );
			self::$o_date_chooser->use_hour = isset($_GET['hour']) ? $_GET['hour'] : (int)date('H', strtotime(localize_datetime(date('Y-m-d H:i:s'), NULL)));
			if (self::$o_date_chooser->use_hour == 'now') {
				self::$o_date_chooser->hour_is_now = TRUE;
				self::$o_date_chooser->use_hour = (int)date("H");
			} else {
				self::$o_date_chooser->hour_is_now = FALSE;
				self::$o_date_chooser->use_hour = (int)self::$o_date_chooser->use_hour;
			}
			self::$o_date_chooser->hour_is_set = isset($_GET['hour']) ? TRUE : FALSE;
		}

		return self::$o_date_chooser;
	}

	// draws the header for this report
	// @return: html for the header
	public static function draw_header() {

		// initialize variables
		$s_retval = '';
		$s_retval .= 'Report: <b>Card Tips Transaction Report </b><br /><br />';

		// get the necessary contents
		$o_date_chooser = self::get_date_chooser_obj();
		$s_select_clause = trim($o_date_chooser->select_clause);

		// update the select clause to choose either the end date or end hour
		$s_date = $o_date_chooser->use_date;
		$s_hour = $o_date_chooser->use_hour;
		$s_hour_get = $o_date_chooser->hour_is_set ? "&hour=$s_hour" : "";
		$s_hour_code = $o_date_chooser->hour_is_set ? "
				<script type='text/javascript'>
					setTimeout(function() {
						$('#selectByDayOrHour').val('hour');
						$('#date2day').hide();
						$('#date2hour').show();
					}, 100);
				</script>" : "";
		// refresh every five minutes if by hour and hour is now
		if ($o_date_chooser->hour_is_now && $o_date_chooser->hour_is_set)
			$s_hour_code .= "
				<script type='text/javascript'>
					setTimeout(function() {
						window.location='index.php?".$o_date_chooser->forward_vars."&hour=now';
					}, 300000);
				</script>";
			echo '<pre style="text-align: left;">' . htmlspecialchars( $a_select_clause ) . '</pre>';
			$a_selects = explode('<select', $s_select_clause);
			$a_date1_selects = explode("&date=", $a_selects[1]);
			$s_select_clause;// = $a_selects[0]."
			//<select".$a_date1_selects[0]."$s_hour_get&date=".$a_date1_selects[1];
			if (DEV)
				$s_select_clause1 .= "
				<select id='selectByDayOrHour' onchange='
					a= null;
					b = null;
					if (this.value == \"day\") {
						a = $(\"#date2day\");
						b = $(\"#date2hour\");
					} else {
						a = $(\"#date2hour\");
						b = $(\"#date2day\");
					}
					a.show();
					b.hide();
					setTimeout(function() {
						a.change();
					}, 100);
					'".(DEV ? '' : "style='display:none;'").">
					<option value='day'>By Day</option>
					<option value='hour'>By Hour</option>
				</select>";
				$s_select_clause .= "
				<select id='date2day' style='display: none;'></select>
				<select id='date2hour' onchange='window.location=\"index.php?".$o_date_chooser->forward_vars."&hour=\"+this.value;' style='".(($o_date_chooser->hour_is_set)?"":"display:none;")."'>
					<option value='now'>This Hour</option>";
				for($i = 0; $i < 24; $i++) {
					$i_hour = $i % 24;
					$s_selected = ($i == (int)$s_hour && !$o_date_chooser->hour_is_now) ? " SELECTED" : "";
					$s_select_clause .= "
					<option value='$i_hour'$s_selected>".date("ga",strtotime("2013-01-01 $i_hour:00:00"))."</option>";
				}
				$s_select_clause .= "
				</select>
				$s_hour_code";

				$s_retval .= $s_select_clause;
				return $s_retval;
	}

	// returns an object with the tips for the given date
	public static function get_timings() {
		
		$o_date_chooser = self::get_date_chooser_obj();
		$report_settings = TimeCardAccountSettings::settings();
		//echo "<pre>";print_r($report_settings);
		$s_hour_start = '00:00:00';
		$s_hour_end = '99:99:99';
		if ($o_date_chooser->hour_is_set) {
			$s_hour_start = str_pad($o_date_chooser->use_hour, 2, '0', STR_PAD_LEFT).':00:00';
			$s_hour_end = str_pad($o_date_chooser->use_hour+1, 2, '0', STR_PAD_LEFT).':00:00';
		}
		$current_tz = date_default_timezone_get();
		if( !empty( $location_info['timezone'] )){
			date_default_timezone_set( $location_info['timezone'] );
		}
		$current_date=date('Y-m-d H:i:s');
		$s_date_start = date('Y-m-d H:i:s', $report_settings->start_time);//$o_date_chooser->use_date.' '.$s_hour_start;
		$s_date_end = date('Y-m-d H:i:s', $report_settings->end_time);//$o_date_chooser->use_date2.' '.$s_hour_end;
		date_default_timezone_set( $current_tz );
		$s_date_start = $o_date_chooser->use_date1." 00:01:00";
		$s_date_end	= $o_date_chooser->use_date2." 00:01:00";
		
		return array("start_date" => $s_date_start, "date_end" => $s_date_end);
	}

	// draws the table representing the report
	public static function draw_report() {
		global $data_name;
		global $location_info;
		global $s_cant_display_error;
		
		$timings = self::get_timings();		
			
		$s_query_string = "select SQL_NO_CACHE cc_main.order_id as `order_id`, cc.check as `check`, cc.gateway as `gateway`, cc_main.response as `response`, cc_main.status as `status`, cc_main.updated_date as `updated_date`, cc.server_name as `server_name`, cc.tip_amount as `tip_amount`,cc_main.transaction_info as trans_info from `poslavu_MAIN_db`.`cc_tip_transactions` as cc_main join `poslavu_[dataname]_db`.`cc_transactions` as cc on (cc.order_id = cc_main.order_id and cc.transaction_id = cc_main.transaction_id) where cc_main.updated_date >='[date_start]'  and cc_main.updated_date <'[date_end]' and cc.pay_type_id = '2' and cc.action = 'Sale' AND cc.voided != '1' and  cc_main.dataname = '[dataname]' group by order_id, cc_main.transaction_id order by cc_main.updated_date desc, cc_main.order_id, cc.check asc ";
		$a_query_vars = array('dataname'=>$data_name, 'date_start'=>$timings['start_date'], 'date_end'=>$timings['date_end'] );
		
		$payments_query = lavu_query($s_query_string, $a_query_vars);
		$rows = mysqli_num_rows($payments_query);
		
		if (empty($rows)){
			$s_cant_display_error = "There were no Batch Tips Tansactions during this time.";
			return $s_cant_display_error;
		} 

		// the table class
		$s_class = '
				<style scoped>
					table.summary_table {
						border-collapse: collapse;
					}
					table.summary_table th {
						border-bottom: 1px solid black;
						padding: 0 5px 0 5px;
					}
					table.summary_table td.money {
						text-align: right;
					}
					table.summary_table td {
						padding: 10px;
					}
					table.summary_table line {
						border-top: 1px solid black;
						padding: 0 5px 0 5px;
					}
					.total {
						border-top: 1px solid black;
					}
				</style>';
		
		// the total tips table
		$s_payments_table = '';
		$s_payments_table .= '<table class="summary_table"><thead><tr>
				<th>Date Time</th>
				<th>Order Id</th>
				<th>Server Name</th>
				<th>Check</th>
				<th>Tip Amount</th>
				<th>Gateway</th>
				<th>Gateway Response</th>
				<th>Status</th>
				</tr></thead><tbody>';
		
		while($row = mysqli_fetch_assoc($payments_query)){
			$response = json_decode($row['response']);

			#if success response from gateway
			if(!is_null($response)){
				if(isset($response->status) && strtolower($response->status) == 'c'){
					$row['response'] = "Success";
				}
			}else{
				$responseData = explode("-",$row['response']);
				if(count($responseData) > 1){
					$responseData = substr($responseData[1],0,-2);
					$responseData = json_decode($responseData);
					$row['response'] = $responseData->message ? "Failed - ".$responseData->message : "Failed - ".$responseData->error_description;
				}
			}
			$s_payments_table .= '<tr>
				<td>'.$row['updated_date'].'</td>
				<td>'.$row['order_id'].'</td>
				<td>'.$row['server_name'].'</td>
				<td>'.$row['check'].'</td>
				<td class="money">'.display_money($row['tip_amount'], $location_info).'</td>
				<td>'.$row['gateway'].'</td>
				<td>'.$row['response'].'</td>
				<td>'.$row['status'].'</td>
				</tr>';
		}
		
		$s_payments_table .='</tbody></table>';

		$s_retval = '';
		$s_retval .= $s_class;
		$s_retval .= $s_payments_table;

		return $s_retval;
	}


}

if ($in_lavu) {

	$draw = '';
	$s_report = TIPSPAYPAL::draw_report();
	$s_header = TIPSPAYPAL::draw_header();
	//$s_graph .= 	TIPSPAYPAL::draw_graph();

	$s_start = '<div id="payments_in_vs_labor_start" style="display:none;"></div>';
	$s_end = '<div id="payments_in_vs_labor_end" style="display:none;"></div>';

	$draw .= $s_start.$s_graph.'<br /><br />'.$s_header.'<br /><br /><br />'.$s_report.$s_end;

	echo $draw;
}
?>