<?php
if( !$in_lavu ){
	return;
}
global $location_info, $locationid;
session_start();
require_once dirname(dirname(dirname(__FILE__))) . '/resources/dimensional_form.php';

function filterRepeatedTimes( $time_iso, $time ){
	static $last_time = '';

	if( $last_time != $time_iso ){
		$last_time = $time_iso;
		//$display_time = date('Y-m-d h:i:s A ', strtotime($time));
		// based on date format setting
		$date_setting = "date_format";
		$location_date_format_query = lavu_query( "SELECT `location`, `setting`, `value` FROM `config` WHERE `setting` = '[1]'",  $date_setting);
		if( !$location_date_format_query || !mysqli_num_rows($location_date_format_query)){
			$date_format = array("value" => 2);
		}else{
			$date_format = mysqli_fetch_assoc( $location_date_format_query );
		}

		switch ($date_format['value']){
			case 1:$display_time = date('d-m-Y h:i:s A ', strtotime($time));
			break;
			case 2:$display_time = date('m-d-Y h:i:s A ', strtotime($time));
			break;
			case 3:$display_time = date('Y-m-d h:i:s A ', strtotime($time));
			break;
		}
		return "<time datetime=\"{$time_iso}\" title=\"{$display_time}\"> $display_time </time>";
	}
	return '';
}

function timeOrderableItemSort( $a, $b ){
	$timea = strtotime(timestampToISO( $a->time() ));
	$timeb = strtotime(timestampToISO( $b->time() ));

	if( $timea < $timeb ){
		return -1;
	}
	if( $timea > $timeb ){
		return 1;
	}

	if( $a instanceof OrderItem && $b instanceof OrderItem ){
		if( $a->showQuantity() && !$b->showQuantity() ){
			return -1;
		}

		if( !$a->showQuantity() && $b->showQuantity() ){
			return 1;
		}

		if( $a->showAmount() && !$b->showAmount() ){
			return -1;
		}

		if( !$a->showAmount() && $b->showAmount() ){
			return 1;
		}
	}

	if( $a instanceof ActionLog && !($b instanceof ActionLog ) ){
		return -1;
	}

	if( !($a instanceof ActionLog) && $b instanceof ActionLog ){
		return 1;
	}

	if( $a instanceof OrderSendLog && !($b instanceof OrderSendLog ) ){
		return -1;
	}

	if( !($a instanceof OrderSendLog) && $b instanceof OrderSendLog ){
		return 1;
	}

	if( $a instanceof EditAfterSendLog && !($b instanceof EditAfterSendLog ) ){
		return -1;
	}

	if( !($a instanceof EditAfterSendLog) && $b instanceof EditAfterSendLog ){
		return 1;
	}

	return 0;
}

function timestampToISO( $time ){
	global $location_info;
	$tz = date_default_timezone_get();
	if( !empty( $location_info['timezone'] ) ){
		date_default_timezone_set( $location_info['timezone'] );
	}

	$ts = strtotime( $time );
	$result =  date('c', $ts);
	date_default_timezone_set( $tz );
	return $result;
}

abstract class OrderBase {
	public function __get( $name ){
		if( isset( $this->$name ) ){
			return $this->$name;
		}
		return null;
	}

	public abstract function drawOrderDetailInteractive();
	public abstract function drawOrderTimelineLogs();
	public abstract function drawOrderDetailPrintable();
	public abstract function time();

	protected function outputRow( $title, $value, $class=''){
		if( !empty($class) ){
			$class = " class=\"{$class}\"";
		}
		if($title == "Exchange Rate"){
		    echo
		    "<tr{$class}>
				   <td class=\"alignLeft\" colspan=\"2\">{$title}:{$value}</td>
			    </tr>";
		} else {
		echo
			"<tr{$class}>
				<td></td>
				<td class=\"alignRight\">{$title}:</td>
				<td class=\"alignRight\">{$value}</td>
			</tr>";
		}
	}
}

class OrderPayment extends OrderBase {
	protected
		$payTypeID,
		$datetime,
		$paymentType,
		$description,
		$voided,
		$action,
		$amount,
		$processed,
		$offline_order,
		$temp_data,
		$tip,
		$total,
		$multiSecondarySymbol,
		$multiSecondaryRateList,
		$multiSecondaryAmount,
		$multiExchangeRate,
		$check;

	private function __construct(){
		$this->payTypeID = 0;
		$this->datetime = 0;
		$this->paymentType = '';
		$this->description = '';
		$this->voided = false;
		$this->action = '';
		$this->processed = false;
		$this->offline_order = false;
		$this->temp_data = '';
		$this->amount = 0;
		$this->tip = 0;
		$this->total = 0;
		$this->multiSecondarySymbol = '';
		$this->multiSecondaryAmount = 0;
		$this->multiSecondaryRateList = array();
		$this->multiExchangeRate = 0;
		$this->check = 1;
	}

	public static function createOrderPayment( $ccTransaction, $multiSecondaryRateList) {
		$result = new OrderPayment();
		$result->payTypeID = $ccTransaction['pay_type_id'];
		$result->datetime = $ccTransaction['datetime'];
		$result->paymentType = $ccTransaction['pay_type'];
		$result->description = $ccTransaction['card_desc'];
		$result->voided = $ccTransaction['voided'] == '1';
		$result->action = $ccTransaction['action'];
		$result->processed = $ccTransaction['processed'];
		$result->offline_order = substr($ccTransaction['process_data'], 0, 4) == 'LOCT';
		$result->temp_data = $ccTransaction['temp_data'];
		$action = $ccTransaction['action'];
		$amount = $ccTransaction['total_collected'];
		$tip = $ccTransaction['tip_amount'];
		$total = $ccTransaction['total_collected']+$ccTransaction['tip_amount'];
		$multiSecondarySymbol = '';
		if (isset($ccTransaction['secondary_currency_code']) && $ccTransaction['secondary_currency_code'] != '' && $ccTransaction['transaction_id'] != '' && $ccTransaction['pay_type'] = 'Cash') {
			$multiSecondarySymbol = $result->getMonetarySymbol($ccTransaction['secondary_currency_code']);
		}

		$result->multiSecondarySymbol = $multiSecondarySymbol;
		$result->multiSecondaryAmount = $ccTransaction['secondary_paid_amount'];
		$result->multiSecondaryRateList = $multiSecondaryRateList;
		$result->multiExchangeRate = $ccTransaction['secondary_paid_amount'] / $ccTransaction['exchange_rate'];
		$result->check =  $ccTransaction['check'];

		if( $action == 'Refund' ) {
			$result->amount = -$amount;
			$result->tip = -$tip;
			$result->total = -$total;
		} else if( $action == 'Sale' ) {
			$result->amount = $amount;
			$result->tip = $tip;
			$result->total = $total;
		}

		return $result;
	}

	private function outputDetail( $title, $amount ){
		$this->outputRow( $title, format_currency($amount), $this->voided ? 'voided' : '');
	}

	public function drawOrderDetailInteractive(){
		if( $this->action == 'Void' || 
			( $this->offline_order &&
				$this->processed &&
				is_numeric($this->temp_data) ) ){
			return;
		}
		$title_str = '';

		if( $this->payTypeID <= '2' ){
			if( $this->action == 'Refund' ){
				$title_str .= 'Refund with ';
			} else if ( $this->action == 'Sale' ){
				if( $this->offline_order ){
					$title_str .= 'Offline ';
				} else {
					$title_str .= 'Paid with ';
				}
			}
		}

		$title_str .= $this->paymentType;
		if( $this->description ){
			$title_str .= " ($this->description)";
		}

		// based on date format setting
		$date_setting = "date_format";
		$location_date_format_query = lavu_query( "SELECT `location`, `setting`, `value` FROM `config` WHERE `setting` = '[1]'",  $date_setting);
		if( !$location_date_format_query || !mysqli_num_rows($location_date_format_query)){
			$date_format = array("value" => 2);
		}else
			$date_format = mysqli_fetch_assoc( $location_date_format_query );

		$split_date = explode(" ", $this->datetime);
		$change_format = explode("-", $split_date[0]);
		switch ($date_format['value']){
			case 1:$show_date = $change_format[2]."/".$change_format[1]."/".$change_format[0]." ".$split_date[1];
			break;
			case 2:$show_date = $change_format[1]."/".$change_format[2]."/".$change_format[0]." ".$split_date[1];
			break;
			case 3:$show_date = $change_format[0]."/".$change_format[1]."/".$change_format[2]." ".$split_date[1];
			break;
		}
		$datetime_iso = timestampToISO( $this->datetime );
		//$title_str .=  ":<br />At <time datetime=\"{$datetime_iso}\" title=\"{$this->datetime}\"></time>";
		$title_str .=  ":<br />At <time datetime=\"{$datetime_iso}\" title=\"{$this->datetime}\">$show_date</time>";

		$class = '';
		if( $this->voided ){
			$class .= 'voided';
		}

		if( $this->offline_order ){
			$class .= ' offline';
		}

		if( !$this->processed ) {
			$class .= ' unprocessed';
		} else {
			$class .= ' processed';
			if( $this->offline_order && !is_numeric( $this->temp_data ) ){
				$class .= ' declined';
			}
		}

        if($this->multiSecondaryAmount != 0) {
			$this->outputRow( $title_str, format_currency($this->multiSecondaryAmount, false, $this->multiSecondarySymbol), $class);
			$this->outputRow( 'Exchange Amount', format_currency($this->multiExchangeRate), $class);
			echo "<tr><td><br/></td></tr>";
        } else {
			$this->outputRow( $title_str, format_currency($this->amount), $class);
        }

		if( !empty($this->tip) && $this->tip != 0 ){
			echo "
		<tr>
			<td></td>
			<td class=\"alignRight\">
				<div style=\"position: relative;\">
					<table class=\"payment_detail\">
						<tbody>";
			$this->outputDetail( "Tip", $this->tip );
			$this->outputDetail( "Total", $this->total );
			echo "</tbody>
					</table>
				</div>
			</td>
			<td>
			</td>
		</tr>";
		}
	}

	public function drawOrderDetailPrintable(){

	}

	public function drawOrderTimelineLogs(){

	}

	public function time(){
		return $this->datetime;
	}

	/**
	* This function is used to get monetory symbol with monetory symbol
	*
	* @param $monetoryCode string
	* @return $moneorySymbol string.
	*/
	public function getMonetarySymbol($monetoryCode) {
	$monetarySymbol = '';
	if ($monetoryCode != '') {
	    lavu_query("SET NAMES 'utf8'");
	    $country_query = lavu_query(" SELECT `monetary_symbol` FROM `poslavu_MAIN_db`.`country_region` WHERE `alphabetic_code` = '".$monetoryCode."' AND `active` = '1' LIMIT 1");
	    $countryRegionInfo = mysqli_fetch_assoc($country_query);
	    $monetarySymbol = $countryRegionInfo['monetary_symbol'];
	}

    return $monetarySymbol;
	}
}

class OrderDiscount extends OrderBase {
	protected
		$title,
		$value,
		$type,
		$amount;

	private function __construct(){
		$this->title = '';
		$this->value = 0;
		$this->type = 'd';
		$this->amount = 0;
	}

	public static function createDiscount( $title, $value, $type, $amount ){
		$result = new OrderDiscount();
		$result->title = $title;
		$result->value = $value;
		$result->type = $type;
		$result->amount = $amount;
		return $result;
	}

	public function drawOrderDetailInteractive(){

	}

	public function drawOrderDetailPrintable(){

	}

	public function drawOrderTimelineLogs(){}
	public function time(){}
}

class OrderTaxProfile extends OrderBase {
	protected
		$name,
		$percentage,
		$consideredSubtotal,
		$amount;

	private function __construct(){
		$this->name = '';
		$this->percentage = 0;
		$this->consideredSubtotal = 0;
		$this->amount = 0;
	}

	public static function createOrderTaxProfile( $name, $percentage, $consideredSubtotal, $amount ){
		$result = new OrderTaxProfile();
		$result->name = $name;
		$result->percentage = $percentage;
		$result->consideredSubtotal = $consideredSubtotal;
		$result->amount = $amount;

		return $result;
	}
	/**
	 * Description:- Fix the issue of rounding value of tax amount
	 * Input:- $totalValue, $numberOfChecks, $decimalPlaces,$type
	 * Output:- return array of equal distributed value of tax amount
	 * Ticket: LP-10552
	 */
	public static function getDistributedValue($totalValue, $numberOfChecks, $decimalPlaces,$type){
		$smallestMoney      = 1/(pow(10,$decimalPlaces)); //smalest number of $decimalPlaces
		$splitPrice          = sprintf("%0.".$decimalPlaces."f",($totalValue / $numberOfChecks)); // till $decimalPlaces
		$splitPriceRemainder = sprintf("%0.".$decimalPlaces."f",($totalValue - ($splitPrice * $numberOfChecks))); // till decimalPlaces
		$isReminderInNegative  = false;
		
		if ( $splitPriceRemainder < 0 ){
			$isReminderInNegative = true;
			$splitPriceRemainder = -($splitPriceRemainder);
		}
		
		$distributedDictionary = array();
		for ( $i = 0 ; $i < $numberOfChecks; $i ++ ){
			if ( $splitPriceRemainder >= (($i+1)*$smallestMoney)){
				if ( $isReminderInNegative ){
					$distributedDictionary[$i] = ($splitPrice - $smallestMoney);
				}else{
					$distributedDictionary[$i] = ($splitPrice + $smallestMoney);
				}
			}else{
				$distributedDictionary[$i] = $splitPrice;
			}
		}
		
		#if reminder is negative, we will distribute it from bottom to top else top to bottom
		if (($isReminderInNegative && $type != "tax") || (!$isReminderInNegative && $type == "tax")){

			$distributedDictionary= array_reverse($distributedDictionary);
			
		}
		
		return $distributedDictionary;
	}

	/**
	 * Description:- To get total tax amount of each tax profile
	 *
	 * @SuppressWarnings(PHPMD.StaticAccess)
	 *
	 * @param string $taxProfilePieces taxinfo
	 *
	 * @return array $taxInfo calculated tax info.
	 */
	public static function getTaxProfileValue($taxProfilePieces) {
		$taxInfo = array();
		foreach ($taxProfilePieces as $taxProfilePiece) {
			$taxProfileInfo = explode('|-|', str_replace(',','.',$taxProfilePiece)); #to get details of each tax profile
			$taxProfileIdenfier = $taxProfileInfo[0] . ':' . $taxProfileInfo[1] . ':' . $taxProfileInfo[2];
			$taxInfo[$taxProfileIdenfier] = OrderTaxProfile::createOrderTaxProfile( $taxProfileInfo[0], $taxProfileInfo[1], $taxProfileInfo[2], $taxProfileInfo[3]);
		}
		return $taxInfo;
	}

	public function addToConsideredSubtotal( $subtotal ){
		$this->consideredSubtotal += $subtotal;
	}

	public function addToTotal( $amount ){
		$this->amount += $amount;
	}

	public function drawOrderDetailInteractive() {
		$percentage_str = $this->percentage * 100;
		if ($percentage_str > 0 && $this->consideredSubtotal > 0 ) {
			$this->outputRow( $this->name . " ({$percentage_str}%)", format_currency($this->amount) );
		}
	}

	public function drawOrderDetailPrintable(){

	}

	public function drawOrderTimelineLogs(){}
	public function time(){}
}

class OrderItem extends OrderBase {
	protected
		$deviceTime,
		$quantity,
		$name,
		$subtotal,
		$showQuantity,
		$showAmount,
		$isVoided;

	private function __construct(){
		$this->deviceTime = '';
		$this->quantity = 0;
		$this->name = '';
		$this->subtotal = 0;
		$this->showQuantity = false;
		$this->showAmount = false;
		$this->isVoided = false;
	}

	public static function createOrderItem( $deviceTime, $quantity, $name, $subtotal, $showQuantity=false, $showAmount=false, $isVoided=false ){
		$result = new OrderItem();
		$result->deviceTime = $deviceTime;
		$result->quantity = $quantity;
		$result->name = $name;
		$result->subtotal = $subtotal;
		$result->showQuantity = $showQuantity;
		$result->showAmount = $showAmount;
		$result->isVoided = $isVoided;

		return $result;
	}

	public function showQuantity(){
		return $this->showQuantity;
	}

	public function showAmount(){
		return $this->showAmount;
	}

	public function isVoided(){
		return $this->isVoided;
	}

	public function drawOrderDetailInteractive(){
		$class = '';
		if( $this->isVoided ){
			if( $this->subtotal < 0 ){
				$class = ' class="discount voided"';
			} else {
				$class = ' class="voided"';
			}
		} else {
			if( $this->subtotal < 0 ){
				$class = ' class="discount"';
			}
		}
		$quantity_str = ($this->showQuantity) ? htmlspecialchars($this->quantity) : '';
		$amount_str = ($this->showAmount && $this->subtotal*1 != 0) ? htmlspecialchars(format_currency($this->subtotal)) : '';
		$name = $this->name;
		echo <<<HTML
			<tr{$class}>
				<td>{$quantity_str}</td>
				<td>{$name}</td>
				<td class="alignRight">{$amount_str}</td>
			</tr>
HTML;
	}

	public function drawOrderDetailPrintable(){
		
	}

	public function drawOrderTimelineLogs(){
		$time_iso = timestampToISO( $this->time() );

		$class = '';
		if( $this->isVoided ){
			if( $this->subtotal < 0 ){
				$class = ' class="discount voided"';
			} else {
				$class = ' class="voided"';
			}
		} else {
			if( $this->subtotal < 0 ){
				$class = ' class="discount"';
			}
		}

		$quantity_str = ($this->showQuantity) ? htmlspecialchars($this->quantity) : '';
		$amount_str = ($this->showAmount && $this->subtotal*1 != 0) ? htmlspecialchars(format_currency($this->subtotal)) : '';
		$time_str = ($this->showQuantity && $this->subtotal*1 != 0) ? filterRepeatedTimes($time_iso, $this->time()) : '';
		$name = $this->name;
		echo <<<HTML
		<tr>
			<td>{$time_str}
			</td>
			<td></td>
			<td>
				<table style="width: 100%">
					<tbody>
						<tr{$class}>
							<td>{$quantity_str}</td>
							<td>{$name}</td>
							<td class="alignRight">{$amount_str}</td>
						</tr>
					</tbody>
				</table>
			</td>
		</tr>
HTML;
	}

	public function time(){
		return $this->deviceTime;
	}
}

class OrderCheck extends OrderBase {
	protected
		$check,
		$order_id,
		$items,
		$subtotal,
		$discount,
		$autoGratuityPercentage,
		$autoGratuityAmount,
		$taxExempt,
		$taxExemptReason,
		$taxes,
		$total,
		$payments;

	private function __construct( ){
		$this->check = null;
		$this->order_id = null;
		$this->items = array();
		$this->subtotal = null;
		$this->discount = null;
		$this->autoGratuityPercentage = 0;
		$this->autoGratuityAmount = 0;
		$this->taxExempt = false;
		$this->taxExemptReason = '';
		$this->taxes = array();
		$this->taxesIncluded = array();
		$this->rounding_amount = null;
		$this->total = null;
		$this->DualCurrencyEnable = null;
		$this->secondaryCurrencyCode = null;
		$this->secondaryExchangeRate = null;
		$this->payments = array();
		$this->changeAmount = 0;
	}

	private static function createOrderCheck( $split_check_detail ){
		$result = new OrderCheck();
		$result->order_id = $split_check_detail['order_id'];
		$result->check = $split_check_detail['check'];
		$result->items = $split_check_detail['items'];
		$result->subtotal = $split_check_detail['subtotal'];
		$result->discount = $split_check_detail['discount'];
		$result->autoGratuityPercentage = $split_check_detail['gratuity_percent'];
		$result->autoGratuityAmount = $split_check_detail['gratuity'];
		$result->taxExempt = $split_check_detail['tax_exempt'];
		$result->taxExemptions = $split_check_detail['taxExemptions'];
		$result->taxes = $split_check_detail['taxes'];
		$result->taxesIncluded = $split_check_detail['taxesIncluded'];
		$result->total = $split_check_detail['check_total'];
		$result->rounding_amount = $split_check_detail['rounding_amount'];
		$result->payments = $split_check_detail['payments'];
		$result->DualCurrencyEnable = $split_check_detail['value'];
		$result->secondaryCurrencyCode = $split_check_detail['secondary_currency_code'];
		$result->secondaryExchangeRate = $split_check_detail['exchange_rate_value'];
		$result->changeAmount = $split_check_detail['change'];

		return $result;
	}

	public static function createOrderChecks( $order_stub, $split_check_details, $order_contents, $cc_transactions ){
		global $location_info;
		$result = array();
		$multiSecondaryRateList = array();
		foreach( $split_check_details as $split_check_details_row => $split_check_detail ){

			$items = array();
			$taxes = array();
			$taxExemptions = array();
			$taxesIncluded = array();
			$taxProfileValue = array();
			foreach( $order_contents as $order_contents_row => $order_content ){
				if( $order_content['check'] != $split_check_detail['check'] &&
					$order_content['check'] != '0' &&
					!strstr($order_content['check'], '|'.$split_check_detail['check'].'|') ){
					continue;
				}
				
				if( $order_content['quantity']*1 > 0 ){
					$quantity = $order_content['quantity']*1;
					$subtotal = $order_content['subtotal']*1;
					$subtotal_with_mods = $order_content['subtotal_with_mods']*1;
					$no_of_checks = 1;

					if( $order_content['check'] == '0' ){
						$no_of_checks = $order_stub['no_of_checks'];
					} else if( $order_content['check'][0] == '|' ) {
						$no_of_checks = count( explode('||', $order_content['check'] ) );
					}
					if( $quantity / $no_of_checks < 1 ){
						$quantity  .= '/'.$no_of_checks;
					} else {
						$quantity /= $no_of_checks;
					}

					$subtotal /= $no_of_checks;
					$subtotal_with_mods /= $no_of_checks;

					$includedModifierPrice =  ($location_info['display_forced_modifier_prices'] == '1');
					$items[] = OrderItem::createOrderItem($order_content['device_time'], $quantity, $order_content['item'], ($includedModifierPrice ? $subtotal : $subtotal_with_mods),true, true);
					
					if( !empty( $order_content['options'] ) ){
						$subtotalModpriceByQuantity = ($order_content['forced_modifiers_price']*$order_content['quantity'])/$no_of_checks;
						$items[] = OrderItem::createOrderItem($order_content['device_time'], 1, $order_content['options'], $subtotalModpriceByQuantity, false, $includedModifierPrice);
					}

					if( !empty( $order_content['special'] ) ){
					    $subtotalspecialModpriceByQuantity = $order_content['modify_price']*$order_content['quantity'];//LP-1412
					    $items[] = OrderItem::createOrderItem($order_content['device_time'], 1, $order_content['special'], $subtotalspecialModpriceByQuantity, false, $includedModifierPrice);
					}

					if ( !empty( $order_content['idiscount_id'] ) ) {
						$discountAmount = $order_content['idiscount_amount'];
						if ($order_content['check'] == 0) {
							$discountAmount = $order_content['idiscount_amount'] / $no_of_checks;
						}

						$items[] = OrderItem::createOrderItem($order_content['device_time'], 1, $order_content['idiscount_sh'], -$discountAmount, false, true);
					}

					{
						$all_tax_profiles_pieces = explode(';;', $order_content['apply_taxrate']);	#to get all tax profiles
						
						$i = 1;
						$taxValues = array();
						
						foreach( $all_tax_profiles_pieces as $all_tax_profiles_pieces_row => $all_tax_profiles_piece ){
							if ($order_content['order_id'][0] === '5') {
								if ($i == 1) {
									$alteredString = ltrim(strstr($all_tax_profiles_piece, '::'), ':');
								} else { 
									$alteredString = $all_tax_profiles_piece; 
								}
								$tax_profile_pieces = explode('::', str_replace(',','.',$alteredString));
							} else {
								$modifiedString = ltrim(strstr($all_tax_profiles_piece, '::'), ':');
								$tax_profile_pieces = explode('::', str_replace(',','.',$modifiedString)); #to get details of each tax profile
							}

							$taxValues[] = $order_content['tax'.$i];
							$taxProfileValue[$tax_profile_pieces[0]] += $order_content['tax'.$i];
							$i++;
						}

						if ($order_content['tax_exempt'] == 1) {
							$taxExemptions[][$order_content['exemption_name']] = array_sum($taxValues) .":". $no_of_checks;
						}
					}
				} 
				
				if( $order_content['void']*1 > 0  ){
					$includedModifierPrice =  ($location_info['display_forced_modifier_prices'] == '1');
					$items[] = OrderItem::createOrderItem($order_content['device_time'], $order_content['void'], $order_content['item'], 0, true, true, true);

					if( !empty( $order_content['options'] ) ){
						$items[] = OrderItem::createOrderItem($order_content['device_time'], 1, $order_content['options'], 0, false, $includedModifierPrice, true);
					}

					if( !empty( $order_content['special'] ) ){
						$items[] = OrderItem::createOrderItem($order_content['device_time'], 1, $order_content['special'], 0, false, $includedModifierPrice, true);
					}

					if( !empty( $order_content['idiscount_id'] ) ){
						$items[] = OrderItem::createOrderItem($order_content['device_time'], 1, $order_content['idiscount_sh'], 0, false, true, true);
					}
				}
			}

			/**
			 * Description : order stub tax details are getting from split_check_detail table tax_info column.
			 *
			 * In `tax_info` we get 'Tax40|-|0.4|-|3.25|-|1.30|o|Tax50|-|0.5|-|4.55|-|2.27|*|1 Percent|-|0.01|-|11.00|-|0.11|o|2 Percent|-|0.02|-|11.00|-|0.22' formatted data.
			 *
			 * By exploding '|*|'
			 * we get
			 * [0] => included tax. Eg: Tax40|-|0.4|-|3.25|-|1.30|o|Tax50|-|0.5|-|4.55|-|2.27
			 * [1] => order tax. Eg: 1 Percent|-|0.01|-|11.00|-|0.11|o|2 Percent|-|0.02|-|11.00|-|0.22
			 *
			 * By exploding '|o|'
			 * we get
			 * [0] => tax title,
			 * [1] => tax percentage,
			 * [2] => tax to be applied on this amount,
			 * [3] => tax percentage applied amount.
			 *
			 * @SuppressWarnings(PHPMD.StaticAccess)
			 */
			$taxInfo = explode('|*|', $split_check_detail['tax_info']);	#to get all tax profiles
			//included tax.
			if ($taxInfo[0] != '') {
				$IncludedTaxProfilePieces = explode('|o|', $taxInfo[0]);
				$taxesIncluded = OrderTaxProfile::getTaxProfileValue($IncludedTaxProfilePieces);
			}
			//order tax.
			if ($taxInfo[1] != '') {
				$orderTaxProfilePieces = explode('|o|', $taxInfo[1]);
				$taxes = OrderTaxProfile::getTaxProfileValue($orderTaxProfilePieces);
			}

			if (!empty($taxExemptions)) {
				$sumArray = array();
				foreach ($taxExemptions as $tax_row => $tax) {
					foreach ($tax as $tax_key => $tax_val) {
						$checksCount = explode(":", $tax_val);
						$taxVal = $checksCount[0];
						$no_checks = $checksCount[1];
						if (!empty($no_checks) && $no_checks > 1) {
							$sumArray[$tax_key] += $taxVal / $no_checks;
							unset($taxExemptions[$tax_row]);
						} else {
							$sumArray[$tax_key] += $taxVal;
							unset($taxExemptions[$tax_row]);
						}
					}
				}
				$taxExemptions[] = $sumArray;
			}

			$discount = null;
			if( !empty( $split_check_detail['discount_id'] ) ){
				$discount = OrderDiscount::createDiscount( $split_check_detail['discount_sh'], $split_check_detail['discount_value'], $split_check_detail['discount_type'], $split_check_detail['discount']);
			}

			$payments = array();
			foreach( $cc_transactions as $cc_transactions_row => $cc_transaction ) {
				if( $cc_transaction['check'] != $split_check_detail['check'] ) {
					continue;
				}
		      if (isset($cc_transaction['secondary_currency_code']) && $cc_transaction['secondary_currency_code'] != '' && $cc_transaction['transaction_id'] != '' && $cc_transaction['pay_type'] = 'Cash') {
					//split_check_detail['value'] is used to show/hide secondary exchange rates.
					$split_check_detail['value'] = 0;
					$multiSecondaryRateList[$cc_transaction['check']][$cc_transaction['secondary_currency_code']] = array(
						'primary_currency_code' => $cc_transaction['primary_currency_code'],
						'exchange_rate' => $cc_transaction['exchange_rate']
					);
		        }

				$payments[] = OrderPayment::createOrderPayment($cc_transaction, $multiSecondaryRateList);
			}

			$split_check_detail['secondary_currency_code'] = $order_stub['secondary_currency_code'];
			$split_check_detail['exchange_rate_value'] = $order_stub['exchange_rate_value'];
			$split_check_detail['gratuity_percent'] = $order_stub['gratuity_percent'];
			$split_check_detail['items'] = $items;
			$split_check_detail['discount'] = $discount;
			$split_check_detail['taxExemptions'] = $taxExemptions;
			$split_check_detail['taxes'] = $taxes;
			$split_check_detail['taxesIncluded'] = $taxesIncluded;
			$split_check_detail['payments'] = $payments;
			$split_check_detail['tax_exempt'] = $order_stub['tax_exempt'] == '1';

			$check = OrderCheck::createOrderCheck($split_check_detail);

			$result[] = $check;
		}
		
		return $result;
	}

	private function outputTaxes(){
		if (!empty($this->taxes)) {
			foreach ($this->taxes as $tax_row => $tax) {
				$tax->drawOrderDetailInteractive();
			}
		}
		if (!empty($this->taxExemptions)) {
			foreach ($this->taxExemptions as $tax_row) {
				foreach ($tax_row as $tax_key => $tax_val) {
					$this->outputRow( "Tax Exemption ($tax_key)", "-".format_currency( $tax_val ) );
				}
			}
		}
	}

	private function outputTaxesIncluded(){
		if( !count( $this->taxesIncluded ) ){
			return;
		}
		echo '<tr><td colspan="100" style="text-align: center;">Included Taxes</td></tr>';
		foreach( $this->taxesIncluded as $tax_row => $tax ){
			$tax->drawOrderDetailInteractive();
		}

		$this->outputSepatator();
	}

	private function outputPayments(){
		$total_payments = 0;

		foreach( $this->payments as $payment_row => $payment ){
			$payment->drawOrderDetailInteractive();
			if( !$payment->voided && !$payment->offline_order )
				$total_payments += $payment->amount;
		}

		//To show change amount.
		if($this->changeAmount > 0) {
			$this->outputRow( 'Change', format_currency( $this->changeAmount ) );
			$this->outputSepatator();
		}

		$this->outputRow('Total Paid', format_currency( $total_payments ) );

		$this->outputSepatator();
		if( $total_payments > $this->total ){
			$this->outputRow( 'Overpaid', format_currency($total_payments - $this->total), 'overpaid' );
		} else if ( $total_payments < $this->total ){
			$this->outputRow( 'Amount Due', format_currency($this->total - $total_payments), 'amount_due' );
		}

		//To show multi secondary currency exchange rates.
		$multiSecondaryRates = end($this->payments)->multiSecondaryRateList[$this->check];
		if (!empty($multiSecondaryRates)) {
			foreach ($multiSecondaryRates as $secondarycurrencyCode => $currencyInfo) {
				$primaryCurrencyCode = $currencyInfo['primary_currency_code'];
				$exchangeValue = " 1 [".$primaryCurrencyCode."] = ".ltrim(format_currency( $currencyInfo['exchange_rate'], true ))." [".$secondarycurrencyCode."]";
				$this->outputRow('Exchange Rate', $exchangeValue, 'alignLeft' );
			}
		}
	}

	private function outputSepatator(){
		echo '<tr><td><br/></td></tr>';
	}

	public function drawOrderDetailInteractive(){
		global $location_info;
		foreach( $this->items as $item_row => $item ){
			$item->drawOrderDetailInteractive();
		}
		$this->outputSepatator();

		$this->outputRow( 'Subtotal', format_currency( $this->subtotal ) );
		if( $this->discount ){
			$this->outputRow( $this->discount->title, format_currency( -$this->discount->amount ), 'discount' );
			$this->outputRow( "After Discount", format_currency( $this->subtotal - $this->discount->amount ) );
		}

		if( !empty($this->autoGratuityAmount) ){
			$gratuity_percent_str = $this->autoGratuityPercentage * 100;
			if($gratuity_percent_str > 0 && $this->autoGratuityAmount > 0) {
			$this->outputRow( $location_info['gratuity_label'] . " ({$gratuity_percent_str}%)", format_currency( $this->autoGratuityAmount ) );
		        }
		}
		$this->outputTaxes();
		if( $this->rounding_amount !== null && 
			abs($this->rounding_amount) != 0.0 ) {
			$this->outputRow( 'Rounding Amount', format_currency( $this->rounding_amount ) );
		}
		$this->outputRow( 'Total', format_currency( $this->total ) );
		if($this->DualCurrencyEnable == 1){
		    $ExhangeValue = ltrim(format_currency( $this->secondaryExchangeRate ), "$")*ltrim(format_currency( $this->total ), "$");
		    $this->outputRow( $this->secondaryCurrencyCode , format_currency( $ExhangeValue ) );
		}
		$this->outputSepatator();
		$this->outputTaxesIncluded();
		$this->outputPayments();

		if($this->DualCurrencyEnable == 1){
		    $this->outputRow('Exchange Rate', ltrim(format_currency( $this->secondaryExchangeRate ), "$"), 'alignLeft' );
		}
	}

	public function items(){
		return $this->items;
	}

	public function drawOrderDetailPrintable(){
		
	}

	public function drawOrderTimelineLogs(){

	}

	public function time(){
		return null;
	}
}

class OrderSendLog extends OrderBase {
	protected
		$order_id,
		$location,
		$location_id,
		$tablename,
		$total,
		$server,
		$server_id,
		$send_server,
		$sender_server_id,
		$time_sent,
		$server_time,
		$device_udid,
		$items_sent,
		$islid,
		$course,
		$resend,
		$redirection;

	private function __construct(){
		$this->order_id = null;
		$this->location = null;
		$this->location_id = null;
		$this->tablename = null;
		$this->total = null;
		$this->server = null;
		$this->server_id = null;
		$this->send_server = null;
		$this->sender_server_id = null;
		$this->time_sent = null;
		$this->server_time = null;
		$this->device_udid = null;
		$this->items_sent = array();
		$this->islid = null;
		$this->course = null;
		$this->resend = null;
		$this->redirection = null;
	}

	public static function createOrderSendLogs( $order_send_logs ) {
		$result = array();
		for( $i = 0; $i < count( $order_send_logs ); $i++ ){
			$result[] = OrderSendLog::createOrderSendLog( $order_send_logs[$i] );
		}
		return $result;
	}

	public static function createOrderSendLog( $order_send_log_row ){
		$result = new OrderSendLog();
		$result->order_id = $order_send_log_row['order_id'];
		$result->location = $order_send_log_row['location'];
		$result->location_id = $order_send_log_row['location_id'];
		$result->tablename = $order_send_log_row['tablename'];
		$result->total = $order_send_log_row['total'];
		$result->server = $order_send_log_row['server'];
		$result->server_id = $order_send_log_row['server_id'];
		$result->send_server = $order_send_log_row['full_name'];
		$result->sender_server_id = $order_send_log_row['sender_server_id'];
		$result->time_sent = $order_send_log_row['time_sent'];
		$result->server_time = $order_send_log_row['server_time'];
		$result->device_udid = $order_send_log_row['device_udid'];
		$result->items_sent = explode('|*|', $order_send_log_row['items_sent']);
		$result->islid = $order_send_log_row['islid'];
		$result->course = $order_send_log_row['course'];
		$result->resend = $order_send_log_row['resend'];
		$result->redirection = $order_send_log_row['redirection'];
		return $result;
	}

	public function drawOrderDetailInteractive(){
		return '';
	}
	public function drawOrderTimelineLogs() {
		$time_iso = timestampToISO( $this->time() );
		$time = filterRepeatedTimes( $time_iso, $this->time() );
		echo <<<HTML
		<tr>
			<td>{$time}</td>
			<td>{$this->send_server}</td>
			<td>Send Log:<br />
				&nbsp;&nbsp;&nbsp;{$items_sent}
			</td>
		</tr>
HTML;
	}
	public function drawOrderDetailPrintable(){}
	public function time() {
		return $this->time_sent;
	}
}

class EditAfterSendLog extends OrderBase {
	protected
		$note,
		$order_id,
		$location,
		$location_id,
		$item,
		$options,
		$special,
		$modify_price,
		$quantity,
		$time,
		$server,
		$server_id,
		$auth_by,
		$server_time;

	private function __construct(){
		$this->note = null;
		$this->order_id = null;
		$this->location = null;
		$this->location_id = null;
		$this->item = null;
		$this->options = null;
		$this->special = null;
		$this->modify_price = null;
		$this->quantity = null;
		$this->time = null;
		$this->server = null;
		$this->server_id = null;
		$this->auth_by = null;
		$this->server_time = null;
	}

	public static function createEditAfterSendLog( $edit_after_send_log ) {
		$result = new EditAfterSendLog();
		$result->note = $edit_after_send_log['note'];
		$result->order_id = $edit_after_send_log['order_id'];
		$result->location = $edit_after_send_log['location'];
		$result->location_id = $edit_after_send_log['location_id'];
		$result->item = $edit_after_send_log['item'];
		$result->options = $edit_after_send_log['options'];
		$result->special = $edit_after_send_log['special'];
		$result->modify_price = $edit_after_send_log['modify_price'];
		$result->quantity = $edit_after_send_log['quantity'];
		$result->time = $edit_after_send_log['time'];
		$result->server = $edit_after_send_log['full_name'];
		$result->server_id = $edit_after_send_log['server_id'];
		$result->auth_by = $edit_after_send_log['auth_by'];
		$result->server_time = $edit_after_send_log['server_time'];
		return $result;
	}

	public static function createEditAfterSendLogs( $edit_after_send_logs  ) {
		$result = array();
		for( $i = 0; $i < count( $edit_after_send_logs ); $i++ ){
			$result[] = EditAfterSendLog::createEditAfterSendLog( $edit_after_send_logs[$i] );
		}
		return $result;
	}

	public function drawOrderDetailInteractive(){
		return '';
	}
	public function drawOrderTimelineLogs() {
		$time_iso = timestampToISO( $this->time() );
		$time = filterRepeatedTimes( $time_iso, $this->time() );
		echo <<<HTML
			<tr>
				<td>{$time}</td>
				<td>{$this->server}</td>
				<td>{$this->note}<br />
					Authorized by {$this->auth_by}
				</td>
			</tr>
HTML;
	}
	public function drawOrderDetailPrintable(){}
	public function time() {
		return $this->time;
	}
}

class ActionLog extends OrderBase {
	protected
		$action,
		$loc_id,
		$order_id,
		$time,
		$user,
		$user_id,
		$server_time,
		$item_id,
		$details,
		$check,
		$device_udid,
		$details_short;

	private function __construct(){
		$this->action = null;
		$this->loc_id = null;
		$this->order_id = null;
		$this->time = null;
		$this->user = null;
		$this->user_id = null;
		$this->server_time = null;
		$this->item_id = null;
		$this->details = null;
		$this->check = null;
		$this->device_udid = null;
		$this->details_short = null;
	}

	public static function createActionLog( $action_log ){
		$result = new ActionLog();
		$result->action = $action_log['action'];
		$result->loc_id = $action_log['loc_id'];
		$result->order_id = $action_log['order_id'];
		$result->time = $action_log['time'];
		$result->user = $action_log['full_name'];
		$result->user_id = $action_log['user_id'];
		$result->server_time = $action_log['server_time'];
		$result->item_id = $action_log['item_id'];
		$result->details = $action_log['details'];
		$result->check = $action_log['check'];
		$result->device_udid = $action_log['device_udid'];
		$result->details_short = $action_log['details_short'];
		return $result;
	}

	public static function createActionLogs( $action_logs ){
		$result = array();
		for( $i = 0; $i < count( $action_logs ); $i++ ){
			$result[] = ActionLog::createActionLog( $action_logs[$i] );
		}

		return $result;
	}

	public function drawOrderDetailInteractive(){
		return '';
	}
	public function drawOrderTimelineLogs() {
		$time_iso = timestampToISO( $this->time() );
		$time = filterRepeatedTimes( $time_iso, $this->time() );
		echo <<<HTML
			<tr>
				<td>{$time}</td>
				<td>{$this->user}</td>
				<td>
					{$this->action}<br />
					{$this->details}
				</td>
			</tr>
HTML;
	}
	public function drawOrderDetailPrintable(){}
	public function time() {
		return $this->time;
	}
}

class OrderStub extends OrderBase {
	protected
		$tag,
		$order_id,
		$tableName,
		$opened,
		$closed,
		$void,
		$guests,
		$server,
		$cashier,
		$checks,
		$send_logs,
		$action_logs,
		$edit_after_send_logs;

	private function __construct(){
		$this->tag = '';
		$this->order_id = '';
		$this->tableName = 'Quick Serve';
		$this->opened = time();
		$this->closed = time();
		$this->void = false;
		$this->guests = 0;
		$this->server = '';
		$this->cashier = '';
		$this->checks = array();
		$this->send_logs = array();
		$this->action_logs = array();
		$this->edit_after_send_logs = array();
	}

	public static function createOrderStub( $tag, $order_id, $tableName, $opened, $closed, $void, $guests, $server, $cashier, $checks, $send_logs, $action_logs, $edit_after_send_logs ){
		$result = new OrderStub();
		$result->tag = $tag;
		$result->order_id = $order_id;
		$result->tableName = $tableName;
		$result->opened = $opened;
		$result->closed = $closed;
		$result->void = $void;
		$result->guests = $guests;
		$result->server = $server;
		$result->cashier = $cashier;
		$result->checks = $checks;
		$result->send_logs = $send_logs;
		$result->action_logs = $action_logs;
		$result->edit_after_send_logs = $edit_after_send_logs;

		return $result;
	}

	public static function createOrderSubFromSources( $order_stub, $order_tags, $split_check_details, $order_contents, $cc_transactions, $send_logs, $action_logs, $edit_after_send_logs ){
		$checks = OrderCheck::createOrderChecks( $order_stub, $split_check_details, $order_contents, $cc_transactions );

		global $location_info;
		if( $order_stub['togo_status'] != '0' ){
			foreach( $order_tags as $key => $order_tag_element ){
				if( $order_tag_element['id'] == $order_stub['togo_status'] ){
					$order_tag = $order_tag_element['name'];
					break;
				}
			}
		}

		$send_logs = OrderSendLog::createOrderSendLogs( $send_logs );
		$action_logs = ActionLog::createActionLogs( $action_logs );
		$edit_after_send_logs = EditAfterSendLog::createEditAfterSendLogs( $edit_after_send_logs );

		return OrderStub::createOrderStub($order_tag, $order_stub['order_id'], $order_stub['tablename'], $order_stub['opened'], $order_stub['closed'], $order_stub['void'], $order_stub['guests'], $order_stub['server'], $order_stub['cashier'], $checks, $send_logs, $action_logs, $edit_after_send_logs );
	}

	public function drawOrderDetailInteractive(){
		$opened_iso = timestampToISO( $this->opened );
		$closed_iso = timestampToISO( $this->closed );

		$voided = ($this->void) ? '<div class="void_alert">Void</div>' : '';

		// based on date format setting
		$date_setting = "date_format";
		$location_date_format_query = lavu_query( "SELECT `location`, `setting`, `value` FROM `config` WHERE `setting` = '[1]'",  $date_setting);
		if( !$location_date_format_query || !mysqli_num_rows($location_date_format_query)){
			$date_format = array("value" => 2);
		}else
			$date_format = mysqli_fetch_assoc( $location_date_format_query );

		switch ($date_format['value']){
			case 1:
			$opened_time = date('d-m-Y h:i:s A ', strtotime($this->opened));
			$closed_time = date('d-m-Y h:i:s A ', strtotime($this->closed));
			break;
			case 2:
			$opened_time = date('m-d-Y h:i:s A ', strtotime($this->opened));
			$closed_time = date('m-d-Y h:i:s A ', strtotime($this->closed));
			break;
			case 3:
			$opened_time = date('Y-m-d h:i:s A ', strtotime($this->opened));
			$closed_time = date('Y-m-d h:i:s A ', strtotime($this->closed));
			break;
		}

		//$opened_time = date('Y-m-d h:i:s A ', strtotime($this->opened));
		//$closed_time = date('Y-m-d h:i:s A ', strtotime($this->closed));

		$opened_display = ($this->opened == '0000-00-00 00:00:00' || empty($this->opened)) ? "<span title=\"{$this->opened}\">No Opened Time (ISSUE)</span>" : "<time datetime=\"{$opened_iso}\" title=\"{$opened_time}\">{$opened_time}</time>";

		$closed_display = ($this->closed == '0000-00-00 00:00:00' || empty($this->closed)) ? "<span title=\"{$this->closed}\">Still Open</span>" : "<time datetime=\"{$closed_iso}\" title=\"{$closed_time}\">{$closed_time}</time>";
		
		echo <<<HTML
	<style type="text/css">
		.alignLeft {
			text-align: left;
		}

		.alignRight {
			text-align: right;
		}

		.alignCenter {
			text-align: center;
		}

		table > td {
			vertical-align: text-top;
			padding: 3px 5px;
		}

		tr.discount td {
			color: green;
		}

		tr.voided td {
			text-decoration: line-through;
		}

		tr.offline td {
			text-decoration: underline;
			color: rgb(255, 140, 30);
		}

		tr.offline.processed.declined td {
			color: red;
		}

		tr.offline.unprocessed td:nth-child(2):before {
			content: "⚠ ";
			font-weight: bold;
		}

		tr.offline.processed.declined td:nth-child(2):before {
			content: "⃠ ";
			font-weight: bold;
		}

		tr.overpaid td, tr.amount_due td {
			color: red;
		}

		table.payment_detail {
			float: right;
			font-size: 0.8em;
		}
	</style>
	<div style="display: table">
		<table style='width: 100%;'>
			<tbody>
				<tr>
					<td colspan="100" class="alignCenter">{$this->tag}</td>
				</tr>
				<tr>
					<td class="alignRight">Order #:</td>
					<td class="alignLeft">{$this->order_id}</td>
				</tr>
				<tr>
					<td class="alignRight">Table:</td>
					<td class="alignLeft">{$this->tableName}</td>
				</tr>

				<tr>
					<td class="alignRight">Opened:</td>
					<td class="alignLeft">{$opened_display}</td>
				</tr>
				<tr>
					<td class="alignRight">Closed:</td>
					<td class="alignLeft">{$closed_display}</td>
				<tr>
					<td class="alignRight">Guests:</td>
					<td class="alignLeft">{$this->guests}</td>
				</tr>
				<tr>
					<td class="alignRight">Server:</td>
					<td class="alignLeft">{$this->server}</td>
				</tr>
				<tr>
					<td class="alignRight">Cashier:</td>
					<td class="alignLeft">{$this->cashier}</td>
				</tr>
			</tbody>
		</table>
		<br />
		{$voided}
		<br />
		<table style="width: 100%;">
			<tbody>
HTML;

		foreach( $this->checks as $check_row => $check ){
			if( count( $this->checks ) > 1 ){
				echo <<<HTML
					<tr>
						<td colspan="100">
						<br />
							<div style="height: 1px; background-color: black; text-align: center">
								<span style="background-color: white; position: relative; top: -0.5em; padding: 5px">
									Check {$check->check}
								</span>
							</div>
							<br />
						</td>
					</tr>
HTML;
			}

			$check->drawOrderDetailInteractive();
		}

			echo <<<HTML
			</tbody>
		</table>
	</div>
HTML;
	}

	public function drawOrderDetailPrintable(){
		
	}

	private function outputSection( $section ){
		if( count( $section ) === 0 ){
			return;
		}

		$it = 0;
		foreach( $section as $k => $v ){
			if( $it++ === 0 ){
				$time_iso = timestampToISO( $v->time() );
				$time = filterRepeatedTimes( $time_iso, $v->time() );
				echo <<<HTML
			<tr>
				<td>{$time}</td>
				<td></td>
				<td>
					<table style="width: 100%">
						<tbody>
HTML;
			}
			$v->drawOrderDetailInteractive();
		}
		echo <<<HTML
						</tbody>
					</table>
				</td>
			</tr>
HTML;
	}

	public function drawOrderTimelineLogs(){
		$allTimeStampedItems = array();

		$checks = $this->checks;
		for( $i = 0; $i < count( $checks ); $i++ ){
			$check = $checks[$i];
			$items = $check->items();
			for( $j = 0; $j < count( $items ); $j++ ){
				$allTimeStampedItems[] = $items[$j];
			}
		}

		for( $i = 0; $i < count($this->send_logs); $i++ ){
			$allTimeStampedItems[] = $this->send_logs[$i];
		}

		for( $i = 0; $i < count($this->action_logs); $i++ ){
			$allTimeStampedItems[] = $this->action_logs[$i];	
		}

		for( $i = 0; $i < count($this->edit_after_send_logs); $i++ ){
			$allTimeStampedItems[] = $this->edit_after_send_logs[$i];
		}

		uasort($allTimeStampedItems, 'timeOrderableItemSort' );
		uasort($allTimeStampedItems, 'timeOrderableItemSort' );

		echo <<<HTML
		<table style="width: 100%">
			<thead>
				<th>Timestamp</th>
				<th>User</th>
				<th>Action</th>
			</thead>
			<tbody>
HTML;
		$time = null;
		$section = array();
		foreach( $allTimeStampedItems as $k => $v ){
			if( $v instanceof OrderItem ){
				if( (count($section) > 0 && $time == $v->time()) || count($section) === 0 ) {
				} else {
					$this->outputSection( $section );
					$section = array();
				}
				$section[] = $v;
				$time = $v->time();
				continue;
			}
			$this->outputSection( $section );
			$v->drawOrderTimelineLogs();
		}
		echo <<<HTML
			</tbody>
		</table>
HTML;
	}

	public function time(){
		return $this->opened;
	}
}

//Lp-3020...
$dataname=$_SESSION['poslavu_234347273_admin_database'];


/**
 *Description : To get location database list on the basis of restaurant id list.
 *@param Int $restIds(with comma separate)
 *@return datanames list.
 */
function getDatabaseForRestId( $restIds ) {
    if($restIds==''){
        return false;
    }
    $restQueryResult = mlavu_query( "SELECT `data_name` FROM `restaurants` WHERE `id` in ($restIds)");
    if( !$restQueryResult || !mysqli_num_rows($restQueryResult)){
        return false;
    }
    
    while( $row = mysqli_fetch_assoc( $restQueryResult ) ){
        $restDBInfo[] = $row;
    } 
    return $restDBInfo;
}

$order_stub = null;
{
	$arguments = array(
		'order_id' => trim($_GET['order_id']),
		'loc_id' => $locationid
	);
	$orderStubQuery = lavu_query("SELECT * FROM $dataname.`orders` WHERE `order_id` = '[order_id]' AND `location_id` = '[loc_id]'", $arguments );
	$orderStubNum=mysqli_num_rows($orderStubQuery);
	if($orderStubNum==0 || $orderStubNum==''){
		$chain_id = $_SESSION['poslavu_234347273_admin_chain_id'];
		$group_query = mlavu_query("select * from `poslavu_MAIN_db`.`chain_reporting_groups` where `chainid`='[1]' and `chainid`!='' and `_deleted`!='1'", $chain_id);
		if(mysqli_num_rows($group_query))
		{
			while($group_read = mysqli_fetch_assoc($group_query))
			{
				$group_ids[] = explode(",",$group_read['contents']);
			}
		}
		$gids = array();
		foreach($group_ids as $key => $val){
			foreach($val as $k => $v){
				$gids[] = $v;
			}
		}
		$gids = array_unique($gids);
		$chainRestIds=$gids;
		if(!empty($chainRestIds)){ //LP-3020
			$restIds = "'" .implode("', '", $chainRestIds) . "'";
			$databaseList=getDatabaseForRestId($restIds);
	
			foreach($databaseList as $database){
				$databaseName='poslavu_' .$database['data_name']. '_db';
	
				$orderStubQuery = lavu_query("SELECT * FROM $databaseName.`orders` WHERE `order_id` = '[order_id]' AND `location_id` = '[loc_id]'", $arguments );
				$recNum=mysqli_num_rows($orderStubQuery);
				$dataname=$databaseName;
				if($recNum>0){
					$dataname=$databaseName;
					break;
				}
			}
		}
	}
	if( $orderStubQuery === false || !mysqli_num_rows($orderStubQuery)){
		echo 'No Order Found';
		return;
	} 
	if( $order_stub = mysqli_fetch_assoc( $orderStubQuery ) ){
	} else {
		echo 'error?';
		return;
	}
}

$order_tags = array();
{
	$order_tags_query = lavu_query("SELECT * FROM $dataname.`order_tags`" );
	if( $order_tags_query === false || !mysqli_num_rows($order_tags_query)){
	} else {
		while( $row = mysqli_fetch_assoc( $order_tags_query ) ){
			$order_tags[] = $row;
		}
	}
}

$order_checks = array();
{
	$arguments = array(
		'order_id' => trim($_GET['order_id']),
		'loc_id' => $locationid
	);
	$order_checks_query = lavu_query("SELECT * FROM $dataname.`split_check_details` WHERE `order_id` = '[order_id]' AND `loc_id` = '[loc_id]' ORDER BY `check`", $arguments );
	if( $order_checks_query === false || !mysqli_num_rows($order_checks_query)){
		echo 'No Order Checks Found';
		return;
	}

	while( $row = mysqli_fetch_assoc( $order_checks_query ) ){
		$order_checks[] = $row;
	}
	$secondary_enable = lavu_query("SELECT `value` FROM $dataname.`config` WHERE `setting` = 'enable_secondary_currency' ");
	$secondary_data = mysqli_fetch_assoc( $secondary_enable );
	$order_checks[0]['value'] = $secondary_data['value'];
}

$order_contents = array();
{
	$arguments = array(
		'order_id' => trim($_GET['order_id']),
		'loc_id' => $locationid
	);
	$order_contents_query = lavu_query("SELECT * FROM $dataname.`order_contents` WHERE `order_id` = '[order_id]' AND `loc_id` = '[loc_id]' AND `item` != 'SENDPOINT'", $arguments );
	if( $order_contents_query === false || !mysqli_num_rows($order_contents_query)){
		echo 'No Order Contents Found';
		return;
	}

	while( $row = mysqli_fetch_assoc( $order_contents_query ) ){
		$order_contents[] = $row;
	}
}

$order_payments = array();
{
	$arguments = array(
		'order_id' => trim($_GET['order_id']),
		'loc_id' => $locationid
	);

	$order_payments_query = lavu_query("SELECT cc.*,cc_ext.`primary_currency_code`, cc_ext.`secondary_currency_code`, cc_ext.`secondary_paid_amount`, cc_ext.`exchange_rate` FROM $dataname.`cc_transactions` AS cc LEFT JOIN $dataname.`cc_transactions_ext` AS cc_ext ON cc_ext.`transaction_id` = cc.`transaction_id` WHERE cc.`order_id` = '[order_id]' AND cc.`loc_id` = '[loc_id]'", $arguments );

	if ( $order_payments_query === false || !mysqli_num_rows($order_payments_query)) {

	} else {
		while ( $row = mysqli_fetch_assoc( $order_payments_query ) ) {
			$order_payments[] = $row;
		}
	}
}


$order_send_logs = array();
{
	$arguments = array(
		'order_id' => trim($_GET['order_id']),
		'loc_id' => $locationid
	);
	$order_send_logs_query = lavu_query("SELECT `send_log`.*, CONCAT(`users`.`f_name`, ' ', `users`.`l_name`) as `full_name` FROM $dataname.`send_log` JOIN $dataname.`users` ON `send_log`.`send_server_id` = `users`.`id` WHERE `order_id`='[order_id]' AND `location_id`='[loc_id]'", $arguments);
	if( $order_send_logs_query !== false && mysqli_num_rows( $order_send_logs_query ) ){
		while( $row = mysqli_fetch_assoc( $order_send_logs_query ) ){
			$order_send_logs[] = $row;
		}
	}
}

$action_logs = array();
{
	$arguments = array(
		'order_id' => trim($_GET['order_id']),
		'loc_id' => $locationid
	);
	$action_log_query = lavu_query("SELECT `action_log`.*, CONCAT(`users`.`f_name`, ' ', `users`.`l_name`) as `full_name` FROM $dataname.`action_log` JOIN $dataname.`users` ON `action_log`.`user_id` = `users`.`id` WHERE `order_id`='[order_id]' AND `action_log`.`loc_id`='[loc_id]' AND `action` != 'Offline Sync Order Snapshot'", $arguments);
	if( $action_log_query !== false && mysqli_num_rows( $action_log_query ) ){
		while( $row = mysqli_fetch_assoc( $action_log_query ) ){
			$action_logs[] = $row;
		}
	}
}

$edit_after_send_logs = array();
{
	$arguments = array(
		'order_id' => trim($_GET['order_id']),
		'loc_id' => $locationid
	);
	$edit_after_send_log_query = lavu_query("SELECT `edit_after_send_log`.*, CONCAT(`users`.`f_name`, ' ', `users`.`l_name`) as `full_name` FROM $dataname.`edit_after_send_log` JOIN $dataname.`users` ON `edit_after_send_log`.`server_id` = `users`.`id` WHERE `order_id`='[order_id]' AND `location_id`='[loc_id]'", $arguments);
	if( $edit_after_send_log_query !== false && mysqli_num_rows( $edit_after_send_log_query ) ){
		while( $row = mysqli_fetch_assoc( $edit_after_send_log_query ) ){
			$edit_after_send_logs[] = $row;
		}
	}
}

$order = OrderStub::createOrderSubFromSources( $order_stub, $order_tags, $order_checks, $order_contents, $order_payments, $order_send_logs, $action_logs, $edit_after_send_logs );
echo <<<HTML
<div>
	<style type="text/css" scoped>
		tab-area {
			display: block;
		}
		[hide] {
			display: none;
		}
		nav.order_detail_navigation {
			display: table;
			margin: 0 auto;
		}
		nav.order_detail_navigation > div {
			display: inline-block;
			padding: 2px 10px;
			border: 1px solid rgba(175,175,175,1.0);
			color: rgba(175,175,175,1.0);
			border-left: 0;
			cursor: pointer;
		}

		nav.order_detail_navigation > div:nth-child(1) {
			border-left: 1px solid rgba(175,175,175,1.0) !important;
		}

		nav.order_detail_navigation > div:hover, nav.order_detail_navigation > div[selected] {
			color: white;
			background: rgba(175,175,175,1.0);
		}
		table tr td {
			vertical-align: text-top;
		}
	</style>
	<nav class="order_detail_navigation">
		<div onclick="show_report_tab('stub');" selected stub>Stub</div>
		<div onclick="show_report_tab('details');" details>Details</div>
	</nav>
	<tab-area>
		<div stub>
HTML;
$order->drawOrderDetailInteractive();

echo <<<HTML
		</div>
		<div details hide>
HTML;
$order->drawOrderTimelineLogs();
echo <<<HTML
		</div>
	</tab-area>
</div>
HTML;

echo <<<HTML
	<script type="text/javascript">
		var timeElements = document.querySelectorAll('time');
		for( var i = 0; i < timeElements.length; i++ ){
			var d = new Date(timeElements[i].getAttribute('datetime'));
			timeElements[i].appendChild( document.createTextNode( d.toLocaleString() ));
		}
	</script>
HTML;
return;