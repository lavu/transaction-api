<?php 
	/***
	this horrible function is kinda nifty actually since flot 
	is in JS, what i do is i build a string ($script) that gets
	passed back to the show_report file and gets echoed to the browser 
	at that point the JS gets interpreted and ran as valid JS 
	
	
	***/

	function chart ($categories, $tableInfo, $showGraphOptions = true){
		
		$labelCategoryArray= getLabelsArray();
		$labels=array();	
		$labelLocation=0;
		$s_default_category_text = speak('What would you like to graph?');
		
		$s_category_options = '';
		$c=0;
		foreach($categories as $category){
			if(!in_array($category, $labelCategoryArray))
				$s_category_options.='<option value="'.$c.'">'.$category.'</option>';
			else
				$labelLocation= $c;
			$c++; 				
		}
		$s_xaxis_label = $categories[$labelLocation];

		if ($showGraphOptions) {
		
			$script= '
				 <style type="text/css">
				 	
	    			#graph .button {
	        			position: absolute;
	        			cursor: pointer;
	    			}
	    			#graph div.button {
	        			font-size: smaller;
	        			color: #999;
	        			background-color: #eee;
	        			padding: 2px;
	    			}
	
	    		</style>
	
				<table id="pickerTable" style="position:absolute; left:40%; " > 
			       <tr>
	    		<td>
	
	    		<select class="graphPicker" >
	    			<option value="0">'.$s_default_category_text.'</option>
	    				'.$s_category_options.'
	    		</select>	 
	    		
	    		</td>
	    		</tr>
	    		<tr><td><center><div class="chartControls" >
	    			<input type="button" value="Pie Chart" class="chart">
	    			<input type="button" value="Bar Chart" class="chart">
	    		</div>
	    		</center>
	    		</td>
	    		</tr>
	
	    	</table>
	    	<br><br><br><br><br>
	    	';	
		} else {
			$script = '';
		}
    	
    	//This little piece of juju gets all the categories into their own 
    	//nifty little arrays 
    	$countInfo=count($tableInfo);
    	$colData=array();
    	for($j=0;$j<=$c; $j++ ){
    		$tempArray=array();
    		
    		for($i=0; $i< $countInfo; $i++){
    		
    			if(!(($j-$i)%$c)){
    				array_push($tempArray, $tableInfo[$i]);
    			}
    		}
    		array_push($colData,$tempArray);
    	}
		
		$labels= $colData[$labelLocation];
		$data=$colData[0];
		$counter=count($data);
		
		$s_ticks = '[[]]';
		if( $counter< 11){
			$s_ticks = 'getTicks()';
		}
		
		$script.='
		<script type="text/javascript">
  			
		    function plotPie(labels, dataIn){
		    	console.log(labels);
		    	console.log(dataIn);
		    	var plot;
		   		$(function () {
		   	
					var myData = [];
    				for (var i = 0; i < labels.length-1; i ++){
						myData[i] = { label: labels[i], data: dataIn[i] }
					}
					console.log(myData);';
        			$script.='
					$.plot($("#graph, #new_graph"), myData,
					{
		    		    series: {
		    		        pie: { 
		    		            show: true,
		    		            radius: 1,
		    		            tilt: 0.5,
		    		            label: {
		    		                show: true,
		    		                radius: 1,
		    		                formatter: function(label, series){';
		    		                $script.= "    return '<div style=\"font-size:8pt;text-align:center;padding:2px;color:white;\">'+label+'<br/>'+Math.round(series.percent)+'%</div>';";
		    		                $script.= '},
		    		                background: { opacity: 0.8 }
		    		            },
		    		            combine: {
		    		                color: "#999",
		    		                threshold: 0.1
		    		            }
		    		        }
		    		    },
		    		    legend: {
		    		        show: false
		    		    }
					});
		    	});
		    }
			function plotBar(labels, dataIn, y_label, x_label){
				var plot;
				$(function () {';
				    $script.='
				    var myData=[];
				    var numItems=labels.length;
				    var skipped = 0;
				    for (var i = 0; i < labels.length; i ++){
				    	if (typeof(dataIn[i]) != "undefined")
							myData[i-skipped] = { label: labels[i], data:[[i, dataIn[i]]] }
						else
							skipped++;
					}
					function reverse(val) {
						return val*-1;
					}
					myData = $(myData).sort(function(a, b) {
						if (a.data[0][1] < b.data[0][1]) {
							return reverse(-1);
						} else {
							if (a.data[0][1] > b.data[0][1]) {
								return reverse(1);
							} else {
								return (a.label < b.label) ? reverse(-1) : reverse(1);
							}
						}
					});
					
					$.each(myData, function(k,v) {
						myData[k].data[0][0] = k;
						i++;
					});
					
					function getTicks(axis) {
						var retval = [];
						$.each(myData, function(k,v) {
							retval.push([v.data[0][0], v.label]);
						});
						return retval;
					}

    				plot=$.plot($("#graph, #new_graph"), myData , {
        		    	series: {
    						bars: { show: true, combine: {color: "#999", threshold: 0.1 }, barWidth: 0.7, align:"center" },
  						},
  						grid: { 
  							hoverable: true,
  							clickable: false,
  						},
		    		    legend: {
		    		        show: true,
		    		        labelFormatter: function(str, obj) {
		    		        	return "<span onmouseover=\'showHoverTextGlobal("+JSON.stringify(obj)+");\' onmouseout=\'removeTooltipGlobal();\'>"+str+"</span>";
		    		        },
		    		    },
  						xaxis:{
        					ticks : '.$s_ticks.',
        					zoomRange: [0.1, numItems+10],
        					panRange: [-20, numItems+10],
        					tickLength : 0,
        				},
        				yaxis: {
        					show: true,
        					zoomRange: [0.1, 100],
        					panRange: [0, 1000],
        				},
        				zoom: {
            				interactive: true
        				},
        				pan: {
            				interactive: true
        				},
  						multiplebars:true,
					});
					
					var rotate = "-webkit-transform: rotate(270deg); -moz-transform: rotate(270deg); -ms-transform: rotate(270deg); -o-transform: rotate(270deg); transform: rotate(270deg);";
					$(".xAxisLabel").remove();
					$(".tickLabels").children(".xAxis").append("<div class=\'xAxisLabel\' style=\'color:black; font-weight:bold;\'>"+x_label+"</div>");
					$(".yAxisLabel").remove();
					$(".tickLabels").children(".yAxis").append("<div class=\'yAxisLabel\' style=\'"+$(".tickLabels").children(".yAxis").children(".tickLabel").attr("style")+"; top:200px; width: 200px; margin-left:-20px; color:black; font-weight:bold;"+rotate+"\'>"+y_label+"</div>");
					$(".yAxisLabel").css({ right: parseInt($(".yAxisLabel").css("right"))-66 });
					$(".tickLabels").parent().css({ "margin-bottom": "20px" });
					
					var legendHeight = parseInt($(".legend").children().children().css("height"))+15;
					var legendWidth = parseInt($(".legend").children().css("width"))+15;
					$(".legend").children().css({ height: Math.min(legendHeight, 272)+"px", right: "-130px", "z-index": 2 });
					if (legendHeight > 272)
						$(".legend").children().css({ width: Math.min(legendWidth, 150) });
					
					$("#graph, #new_graph").bind("plotzoom", function (event, plot) {
        				var axes = plot.getAxes();
        				if(isNaN(axes.xaxis.min.toFixed(2))){
           		 			//lavuLog( "ISNAN");
           		 			$(".chartControls input").click();
           		 		}
           		 	});
           		 	
           		 	// from http://www.jqueryflottutorial.com/how-to-make-jquery-flot-time-series-chart.html
					function showTooltip(x, y, color, contents) {
						$("<div id=\'tooltip\'>" + contents + "</div>").css({
							position: "absolute",
							display: "none",
							top: y - 40,
							left: x - 20,
							border: "2px solid " + color,
							padding: "3px",
							"font-size": "9px",
							"border-radius": "5px",
							"background-color": "#fff",
							"font-family": "Verdana, Arial, Helvetica, Tahoma, sans-serif",
							opacity: 1,
							"z-index": 1,
						}).appendTo("body").fadeIn(200);
					}
					previousPoint = null, previousLabel = null;
					$("#graph, #new_graph").bind("plothover", function (event, pos, item) {
						if (item) {
							showHoverText(event, pos, item);
						} else {
							removeTooltip();
						}
					});
					function showHoverText(event, pos, item) {
						function addCommas(nStr)
						{
							nStr += "";
							x = nStr.split(".");
							x1 = x[0];
							x2 = x.length > 1 ? "." + x[1] : "";
							var rgx = /(\d+)(\d{3})/;
							while (rgx.test(x1)) {
								x1 = x1.replace(rgx, "$1" + "," + "$2");
							}
							return x1 + x2;
						}
						if ((previousLabel != item.series.label) || (previousPoint != item.dataIndex)) {
							previousPoint = item.dataIndex;
							previousLabel = item.series.label;
							$("#tooltip").remove();
							
							var x = item.datapoint[0];
							var y = item.datapoint[1];
							var color = item.series.color;
							
							showTooltip(item.pageX, item.pageY, color,
							"<strong>" + item.series.label + "</strong><br>"  +
							addCommas(y));
						}
					}
					function removeTooltip() {
						$("#tooltip").remove();
						previousPoint = null;
					}
					
					window.showHoverTextGlobal = function(obj) {
						obj.series = {
							label: obj.label,
							color: obj.color,
						}
						obj.dataIndex = 0;
						obj.datapoint = obj.datapoints.points;
						obj.pageX = $(plot.getCanvas()).offset().left + plot.pointOffset({ x: obj.data[0][0], y: obj.data[0][1] }).left;
						obj.pageY = $(plot.getCanvas()).offset().top + plot.pointOffset({ x: obj.data[0][0], y: obj.data[0][1] }).top;
						obj.seriesIndex = obj.data[0][0];
						return showHoverText(null, null, obj);
					}
					window.removeTooltipGlobal = function() {
						return removeTooltip();
					}
           		 	
           		 	$("#graph, #new_graph").bind("plotpan", function (event, plot) {
        				var axes=plot.getAxes();
					});
					
           		 	/*$(\'<div class="button" style="position:absolute; background-color: transparent;width:80px; height:14px; font-size:8pt">zoom out</div>\').appendTo($("#graph, #new_graph")).click(function (e) {
        				e.preventDefault();
        				plot.zoomOut();
    				});*/
    				
    				
				});

			} 
			var labels=[';
                	$counter= count($labels);
                	for( $i=0; $i < $counter; $i++){
                		                	
						$script.='"'.$labels[$i].'",';	
                	}
                	$script.='""];
                	
                	var data=[';
                	for( $i=0; $i< $c; $i++){
                		$script.='[';
                		for( $j=0; $j<$counter; $j++){
                			if( $i!= $labelLocation) {
                				$s_display = $colData[$i][$j];
                				if (!is_float($s_display) && !is_numeric($s_display))
                					$s_display = "'$s_display'";
                				$script.=$s_display.',';
                			}
                		}
                		$script.='],';
                	}
                	$script.='[]];
			
			isBar = true;
        	
			$(".chartControls input").click(function (e) {
        		e.preventDefault();
				
        		if( $(this).val() == "Bar Chart" ){
        			isBar=true;
        			plotBar(labels, data[$(".graphPicker option:selected").val()], getYAxisLabel(), "'.$s_xaxis_label.'")
        		
        		}else{
        			isBar=false;
        			plotPie(labels, data[$(".graphPicker option:selected").val()]);    		
        		}
        	});  
        	
			$(".graphPicker").change(function () {
				$(".chart").removeAttr("disabled");
				
         		$(".graphPicker option:selected").each(function () {
         		
         		if(isBar)
         			plotBar(labels, data[$(this).val()], getYAxisLabel(), "'.$s_xaxis_label.'");
         		else
               		plotPie(labels, data[$(this).val()]);	
             	});
        	});			

			function showTooltip(x, y, contents) {
			        $(\'<div id="tooltip">\' + contents + \'</div>\').css( {
			            position: \'absolute\',
			            display: \'none\',
			            top: y + 5,
			            left: x + 5,
			            border: \'1px solid #fdd\',
			            padding: \'2px\',
			            \'background-color\': \'#fee\',
			            opacity: 0.80
			        }).appendTo("body").fadeIn(200);
			    }
			
			    var previousPoint = null;
			    $("#graph").bind("plothover", function (event, pos, item) {
			        $("#x").text(pos.x.toFixed(2));
			        $("#y").text(pos.y.toFixed(2));
			
			        if (true) {
			            if (item) {
			                if (previousPoint != item.dataIndex) {
			                    previousPoint = item.dataIndex;
			                    
			                    $("#tooltip").remove();
			                    var x = item.datapoint[0].toFixed(2),
			                        y = item.datapoint[1].toFixed(2);
			                    
			                    showTooltip(item.pageX, item.pageY,
			                                item.series.label + ": " + y);
			                }
			            }
			            else {
			                $("#tooltip").remove();
			                previousPoint = null;            
			            }
			        }
			    });
			
			function getYAxisLabel() {
				if (parseInt($(".graphPicker").val()) == 0 && $(".graphPicker option:selected").text() == "'.$s_default_category_text.'")
					return $($(".graphPicker option:selected").siblings()[0]).text();
				else
					return $(".graphPicker option:selected").text();
			}
			
			//And so it just automagically loads up a graph: 
 			plotBar(labels, data[0], getYAxisLabel(), "'.$s_xaxis_label.'");
        	';
		
		$script.= '</script>';
		
		return $script;
	
	}	
	//This is just for testing now, later on we will have a section for labels
	
	function getLabelsArray(){
		return array("FirstName","LastName","Payment Type","Category", "Groups","Payment Type","Item", "Inventory Item", "Discount");
	}
	
?>