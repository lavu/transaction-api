<?php
set_time_limit(400);
checkIndexes();
?>

<style type="text/css">
.row{
	margin-left:150px;
}
.header{
	font-weight: 700;
}
.rowElement{
	float:left;
	border-bottom: 1px solid #666;
	padding: 5px 0 5px 0;
}
.re1{
	width:150px;
}
.re2{
	width:100px;
	
}
.re3{
	width:200px;
	
}
.re4{
	width:100px;
	
}
.re5{
	width:100px;
	
}
.re6{
	width:100px;
	
}

.hidden_info{
	background-color: #999;
}
.clearRow{
	clear:both;
}
.hold_date{
	float:left;
	width:200px;
	border-right:1px solid #ccc;
}
.date{
	margin-left:260px;
}
</style>

<script type='text/javascript'>
var showing = "";//which row is visible right now?
function showReport(class_id){
		if (showing == class_id){
				document.getElementById(class_id).style.display = 'none';
				showing = "";
				return;
		}//if
		if (showing != ""){
				document.getElementById(showing).style.display = 'none';
		}//if
		document.getElementById(class_id).style.display = 'inline';
		showing = class_id;
		return;
}//showPartInfo()
function submitForm(){
		//alert("hi");
		start_month = document.getElementById("start_month").value;
		start_day = document.getElementById("start_day").value;
		start_year = document.getElementById("start_year").value;
		
		end_month = document.getElementById("end_month").value;
		end_day = document.getElementById("end_day").value;
		end_year = document.getElementById("end_year").value;
		
		link = "index.php?mode=reports_p2r_karts_usage&start_month="+start_month+"&start_day="+start_day+"&start_year="+start_year;
		link += "&end_month="+end_month+"&end_day="+end_day+"&end_year="+end_year;
		window.location = link;
		return;
}//submitForm()
</script>
<h2>Kart Usage</h2>


<?php
$cur_date = date("Y-m-d");
$cur_date_arr = explode("-",$cur_date);
$cur_month = $cur_date_arr[1];
$cur_year = $cur_date_arr[0];
$cur_day = $cur_date_arr[2];

if (!reqvar("start_month") && !reqvar("start_day") && !reqvar("start_year") && !reqvar("end_month") && !reqvar("end_month") && !reqvar("end_month")){
		$start_date 	= date("Y-m-d",mktime(0,0,0,$cur_month*1-1,$cur_day*1,$cur_year*1));
		$end_date 	= date("Y-m-d",mktime(0,0,0,$cur_month*1,$cur_day*1,$cur_year*1));
}//if
else{
		$start_date = reqvar("start_year")	.	"-"	.	reqvar("start_month")	.	"-"	.	reqvar("start_day");
		$end_date  = reqvar("end_year")	.	"-"	.	reqvar("end_month")	.	"-"	.	reqvar("end_day"); 
}//else
//echo "start_date:$start_date<br>";
//echo "end_date:$end_date<br>";
?>

<div class="date">
	<div class='hold_date'>
	
		<?php selectDate("Start Day","start",$start_date);?>
	</div>
	<div class='hold_date'>
		<?php selectDate("End Day","end",$end_date);?>
	</div>
	<div class='clearRow'></div>
</div><!--date-->
<br/>
<input type="button" name="submit" value="Go" onclick="submitForm();"/>



<?php

echo "<div class='row'>";
echo "<div class='rowElement re1'>Kart Number</div>";
echo "<div class='rowElement re2'>Number of Laps</div>";
echo "<div class='clearRow'></div>";
echo "</div>";
echo "<div class='clearRow'></div>";


$sql 							= "SELECT DISTINCT(number) AS number FROM lk_karts WHERE number!='' ORDER BY number*1";
$kart_number_info  = lavu_query($sql);
while ($kart_row =	mysqli_fetch_assoc($kart_number_info)){
		
		$kart_id 			= $kart_row['id'];
		$kart_number  = $kart_row['number'];
		//echo "kartNum: $kart_number<br>";
		
		$sql 	 = "SELECT COUNT(lk_laps.scheduleid) AS num_laps, lk_event_schedules.kartid AS kart_id, lk_karts.number AS kart_number FROM lk_laps";
		$sql .= " LEFT JOIN lk_event_schedules ON lk_event_schedules.id=lk_laps.scheduleid LEFT JOIN lk_karts ON lk_event_schedules.kartid=lk_karts.id";
		$sql .= " LEFT JOIN lk_events ON lk_events.id=lk_event_schedules.eventid WHERE lk_karts.number='$kart_number' AND LEFT(lk_events.date,10)>='$start_date' AND LEFT(lk_events.date,10)<='$end_date'";
		
		//echo "sql: $sql<br>";
		$info = lavu_query($sql);
		$row = mysqli_fetch_assoc($info);
		$num_laps = $row['num_laps'];
		echo "<div class='row'>";
		echo "<div class='rowElement re1'>$kart_number</div>";
		echo "<div class='rowElement re2'>$num_laps</div>";
		echo "<div class='clearRow'></div>";
		echo "</div>";
		echo "<div class='clearRow'></div>";
		
}//while


//echo "sql: $sql<br>";
?>
<script type='text/javascript'>
document.getElementById("select_part").value = '<?php echo $selected_part;?>';
</script>

<?php
function formatDate($datetime){
		
		$datetime_arr 		= explode(" ",$datetime);
		$date_str 			= $datetime_arr[0];
		$time_str 	 		= $datetime_arr[1];
		
		$date_arr				= explode("-",$date_str);
		$year					= $date_arr[0];
		
		$month				= $date_arr[1];
		$day						= $date_arr[2];
		$formated_date 	= $month."-".$day."-".$year." ".$time_str;
		
		return $formated_date;
}//formatDate()

function selectDate($label,$id,$set_to_this_date){
				
				$set_to_date_arr = explode("-",$set_to_this_date);
				$set_year = $set_to_date_arr[0];
				$set_month = $set_to_date_arr[1];
				$set_day = $set_to_date_arr[2];
				
				//$startVal = reqvar("start_date_text_field");
				//$start_painted = reqvar("paint_start_date_id");
				$month_id = $id."_month";
				$day_id = $id."_day";
				$year_id = $id."_year";
				
				echo "<div class='leftForm'><label>$label</label></div><!--leftForm-->";
				echo "<div class='rightForm'><select name='$month_id' id='$month_id'><option value=''> </option>";
			        	
				for ($i=1; $i<=12; $i++){
					echo "<option value='".pad($i,2)."'> $i </option>";
				}//for
		
				echo "</select>";
				echo "<select name='$day_id' id='$day_id'>";
	        	echo "<option value=''> </option>";
        	
				for ($i=1; $i<=31; $i++){
					echo "<option value='".pad($i,2)."'> $i </option>";
				}//for
				
				echo "</select>";
				echo "<select name='$year_id' id='$year_id'>";
				echo "<option value=''> </option>";
		
				for ($i=2011; $i<=2013; $i++){
					if (reqvar("startYear") == $i){
						echo "<option value=\"$i\" selected=\"selected\"> $i </option>";
					}//if
					
					if (reqvar("startYear") != $i){
						echo "<option value=\"$i\"> $i </option>";
					}//if
				}//for
				
				echo "</select>";
				echo "</div><!--rightForm-->";
				echo "<div class='breakLine'></div>";
				
				echo "<script type='text/javascript'>";
				//echo "alert('$set_month');";
				
				echo "document.getElementById('$month_id').value = '$set_month';";
				echo "document.getElementById('$day_id').value = '$set_day';";
				echo "document.getElementById('$year_id').value = '$set_year';";
				echo "</script>";

}//selectDate

function pad($str,$length){
		while (strlen($str)<$length){
				$str = "0".$str;
		}//while
		return $str;
}//pad

function checkIndexes(){
		/* Make sure all the appropriate indexes have been set*/
		//$inform = false;
		$does_index_exist = lavu_query("SHOW INDEX FROM lk_laps WHERE KEY_NAME = 'scheduleid_index'");
		if (mysqli_num_rows($does_index_exist) < 1){
				echo "Creating index".mysqli_num_rows($does_index_exist)." for faster processing time.<br/>";
				$script  = "<script type='text/javascript'>";
				$script .= "alert(\"This script must perform an indexing of your database. This is a one time operation but will greatly increase the run time of this page. Please reload the page again in a few minutes.\")";
				$script .= "</script>";
				echo $script;

				$create_index = lavu_query("CREATE INDEX scheduleid_index ON lk_laps (scheduleid);");
				echo "make index<br>";
				//$inform = true;
		}//if
		//echo "inform1: $inform<br>";
		
		/* Adjust fields to proper data types. Some queries take too long to execute if the data type is not int 11 */
		$sql_query = "SHOW FIELDS FROM lk_laps";
		$result = lavu_query($sql_query);
		$type_arr = array();
		
		while ($row = mysqli_fetch_assoc($result)){
				/* we need this to see what data type the lk_event_schedules.eventid field is, we need it to be int(11) */
				$type_arr[$row['Field']]["type"] = $row['Type'];
		}//while
		//foreach ($type_arr as $key => $value){
		//	echo "$key: ".$value["type"]."<br>";
		//}
		$event_id_data_type = $type_arr["scheduleid"]["type"];
		$correct_var_type 	  = "int(11)";
		//echo "type: $correct_var_type<br>";
		//echo "type from db: $event_id_data_type <br>";

		//echo "datatype: $event_id_data_type<br>";
		//echo "strcmp :".strcmp($event_id_data_type,"int(11)")."<br>";
		//echo "int(11)";
		//echo "event: ".strlen($event_id_data_type)."<br>";
		//for($i = 0; $i < strlen($event_id_data_type); $i++)
		//{
	//		echo $event_id_data_type[$i] . " ";
		//}
		//echo "<br />";
		//echo "event: ".strlen($correct_var_type)."<br>";
		if (trim($correct_var_type) != trim($event_id_data_type)){
				$sql_query = "ALTER TABLE lk_laps MODIFY column scheduleid int(11)";
				echo "Altering datatype to int(11) for faster processing time<br>";
				
				$script  = "<script type='text/javascript'>";
				$script .= "alert(\"This script is performing optimization operations to enable faster load time. Please run this script again in a few minutes after it's changes to the database are finished propogating.\")";
				$script .= "</script>";
				echo $script;

				
				$res = lavu_query($sql_query);
				if (mysqli_num_rows($res)*1 > 0){
						echo "Altered data type of lk_event_schedules.eventid to int(11) for faster processing<br>";
				}//if
				//echo "change int(11)<br>";
				//$inform = true;
		}//if
		//echo "inform2: $inform<br>";
		/*
		if ($inform == true){
				$script  = "<script type='text/javascript'>";
				$script .= "alert(\"This script is performing optimization operations to enable faster load time. Please run this script again in a few minutes after it's changes to the database are finished propogating.\")";
				$script .= "</script>";
				echo $script;
				//exit();
		}
			*/
}//checkIndexes()


?>