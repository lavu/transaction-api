<?php
set_time_limit(140);
function calOvertimeHours($start_date, $end_date, $locationid, $server_id, $getOTSettingsRow){
	$locationid    = ($locationid) ? $locationid : sessvar("locationid"); //9; // ADJUST THIS, SHOULD BE SAME FOR ALL ENTRIES!!
	$searchForType = $getOTSettingsRow['type']; // this represents from the config table associated with overtime settings */
	$summary = "no";//assume it's only showing one person
	/* Get these values from user input */
	$start_date_str = explode(" ",$start_date);
	$start_date_arr = explode("-",$start_date_str[0]);
	$rangeStartDateMonth = $start_date_arr[1];
	//echo $rangeStartDateMonth."<br>kdjf";
	$rangeStartDateDay = $start_date_arr[2];
	$rangeStartDateYear = $start_date_arr[0];

	$startDateTimeStamp = mktime(0, 0, 0, $rangeStartDateMonth, $rangeStartDateDay, $rangeStartDateYear);

	/* Get these values from user input */
	$end_date_str = explode(" ",$end_date);
	$end_date_arr = explode("-",$end_date_str[0]);
	$rangeEndDateMonth = $end_date_arr[1];
	$rangeEndDateDay = $end_date_arr[2];
	$rangeEndDateYear = $end_date_arr[0];

	$endDateTimeStamp = mktime(0, 0, 0, $rangeEndDateMonth, $rangeEndDateDay, $rangeEndDateYear);

	/* Given server id and location display hours and wages to be paid according to overtime settings. */
	$ot_holidays = explode("|", $getOTSettingsRow['value_long']);
	$dt_holidays = explode("|", $getOTSettingsRow['value_long2']);

	/* Get overtime settings */
	/*
		value = day (Over this many hours per day is considered overtime)
		value2 = day work week starts (Monday, Tuesday, etc...)
		value3 = overtime critical value, days per week (over how many days per week is considered overtime)
		value4 = hours this week (over how many hours per week is considered overtime?)
	*/

	/* Default values - If user never specifies anything use values so large they are effectively not set */
	$maxDailyHoursForOvertime = 24;
	$maxDailyHoursForDoubletime = 10000;//set these values to way more than what is necessary, so they can't interfere;
	$startDayOfWorkWeek = 0;//sunday
	$maxWorkDaysForOvertime = 7;
	$maxWeeklyHoursForOvertime = 10000;
	//echo "startDatedTimeStamps: $startDateTimeStamp<br>";
	//echo "endDatetimes: $endDateTimeStamp<br>";
	//echo "heres<br>";
	//exit();
	/* User specified values */
	if ($getOTSettingsRow['value'] != "DNA" ){
		$maxDailyHoursForOvertime = $getOTSettingsRow['value'];
	}//if

	if ($getOTSettingsRow['value2'] != "DNA" ) $startDayOfWorkWeek = $getOTSettingsRow['value2'];
	//if

	if ($getOTSettingsRow['value3'] != "DNA" ){
		$maxWorkDaysForOvertime = $getOTSettingsRow['value3'];
	}//if

	if ($getOTSettingsRow['value4'] != "DNA" ){
		$maxWeeklyHoursForOvertime = $getOTSettingsRow['value4'];
	}//

	if ($getOTSettingsRow['value5'] != "DNA" ){
		$maxDailyHoursForDoubletime = $getOTSettingsRow['value5'];
	}


	if ($getOTSettingsRow['value5'] == "" ){
		/* This 'Double Time' feature was added after the app was finished, so this value doesn't get set by the "Set Overtime Settings" page. We will set it here since if it is not set by the user, the value in the config table
			should be "DNA" instead of blank */
		//echo "AINT SET<BR>";
		//echo "locationid: $locationid<br>search: $searchForType<br>";
		lavu_query("UPDATE `config` SET `value5`='DNA' WHERE `location`='[1]' AND type='[2]'",$locationid, $searchForType);
	}

	/* Keep decrementing the days from $startDateTimeStamp until we find the work week start day */
	$foundWorkWeekStartDate = false;
	$daysBeforeRangeStartDay = 0;

	while ($foundWorkWeekStartDate == false){
		/* make sure these are integers */
		$workWeekStartDateTimeStamp = mktime(0, 0, 0, $rangeStartDateMonth*1, ($rangeStartDateDay*1 - $daysBeforeRangeStartDay*1), (int)$rangeStartDateYear);
		$daysBeforeRangeStartDay += 1;
		$dayOfWeekArray = getdate($workWeekStartDateTimeStamp);
		$dayOfWeek = $dayOfWeekArray[wday];

		if ($dayOfWeek == $startDayOfWorkWeek){
			$workWeekStartDate = $dayOfWeekArray[mon] . "-" . $dayOfWeekArray[mday] . "-" . $dayOfWeekArray[year]; //mm-dd-yyyy format
			$workWeekStartMonth = $dayOfWeekArray[mon];
			$workWeekStartDay = $dayOfWeekArray[mday];
			$workWeekStartYear = $dayOfWeekArray[year];
			$useWorkWeekStartDate = $workWeekStartYear."-".pad($workWeekStartMonth,2)."-".pad($workWeekStartDay,2);//different format for mysql queries
			$foundWorkWeekStartDate = true;
		}//if

	}//while

	$loopTimes = 1;/*run through once by default (we assume only one employee is selected*/
	$employee_array = array();
	$employee_array[0] = $server_id;//assume this by default, we'll overwrite it later if all employees were selected

	/*
	if ($_POST['selectEmployee'] == "all"){
			$getAllEmpRes = lavu_query("SELECT * FROM users WHERE _deleted!='1' ORDER BY f_name");
			$index = 0;
			$summary = "yes";//set this to yes so a javascript closes all the tables to make a better presentation to the user
			while ($getAllEmpRow = mysqli_fetch_assoc($getAllEmpRes)){
						$employee_array[$index] = $getAllEmpRow['id'];
						$index++;
			}//while
	}//if
	*/

	$start_date_without_time_str = explode(" ",$start_date);
	$start_date_without_time = $start_date_without_time_str[0];
	$end_date_without_time_str = explode(" ",$end_date);
	$end_date_without_time = $end_date_without_time_str[0];

	for ($i=0;$i<count($employee_array);$i++){
		$server_id = $employee_array[$i];
		$class_name = "class_".$server_id;//use this to display or hide table entries
		//echo "<input type='hidden' value='none' id='displayOrHide_".$server_id."'/>";
		$sql = "SELECT * FROM `clock_punches` WHERE `location_id`=\"$locationid\" AND server_id=\"$server_id\" AND time_out!=\"\" AND _deleted!='1' AND time>='$useWorkWeekStartDate' AND LEFT(time_out,10)<=DATE_ADD('$end_date', INTERVAL 27 HOUR)";
		//echo "sql: $sql<br>";
		$result = lavu_query($sql);

		$useTheseClockPunches = 0;// zero this out
		$useTheseClockPunches = array();// zero this out

		while ($row = mysqli_fetch_assoc($result)){
			$clockPunch          = $row['time'];

			$piece1              = explode(" ", $clockPunch);
			$clockPunchDate      = $piece1[0];
			$clockPunchHours     = $row['hours'];
			$clockPunchDateArray = explode("-", $clockPunchDate);

			$clockPunchYear      = $clockPunchDateArray[0];
			$clockPunchMonth     = $clockPunchDateArray[1];
			$clockPunchDay       = $clockPunchDateArray[2];

			$clockPunchTimeStamp = mktime(0, 0, 0, $clockPunchMonth, $clockPunchDay, $clockPunchYear);

			//store date if it is in the specified range
			if ( ($clockPunchTimeStamp >= $workWeekStartDateTimeStamp) && ($clockPunchTimeStamp <= $endDateTimeStamp) ){
				$holdHours = 0;
				/* Make sure we're not overwriting values already in the array for that date */
				if (isset($useTheseClockPunches[$clockPunchTimeStamp])){
					$holdHours = $useTheseClockPunches[$clockPunchTimeStamp];
				}
				$useTheseClockPunches[$clockPunchTimeStamp] = $clockPunchHours + $holdHours;

			}//if


			//mktime(hour,minute,second,month,day,year,is_dst)

		}//while

		$getEmpNameRes                = lavu_query("SELECT * FROM users WHERE id='[1]'",$server_id);
		$getEmpNameRow                = mysqli_fetch_assoc($getEmpNameRes);
		$employeeName                 = $getEmpNameRow['f_name'] . " " . $getEmpNameRow['l_name'];

		$offset                       = 6; //days for pay period
		$newWeekOffset                = 6;
		$index                        = 0;
		$weeklyTotalHours             = 0;
		$numDaysWorkedThisWeek        = 0;
		$maxDaysInWeekCritValReached  = "";
		$totalRegHours                = 0;
		$totalOTHours                 = 0;
		$totalDTHours                 = 0;
		$ot_steps                     = array();
		$runDates                     = false;
		$hoursLeftTillWeeklyOverTime  = $maxDailyHoursForOvertime;

		/* Display */
		while($runDates == false){
			$key                         = mktime(0, 0, 0, $workWeekStartMonth, $workWeekStartDay + $index, $workWeekStartYear);
			$hours                       = 0;
			$maxDailyHoursCritValReached = "";
			$keyDateArray                = getdate($key);
			$keyMonth                    = $keyDateArray[mon];
			$keyDay                      = $keyDateArray[mday];
			$keyYear                     = $keyDateArray[year];

			if ( mktime(0, 0, 0, (int)$keyMonth, (int)$keyDay, (int)$keyYear) > mktime(0, 0, 0, $rangeStartDateMonth * 1, $rangeStartDateDay * 1 + $newWeekOffset * 1, $rangeStartDateYear * 1) ){
				$newWeekOffset += 7;
			}//if

			/* New week */
			if ( mktime(0, 0, 0, $keyMonth, $keyDay, $keyYear) > mktime(0, 0, 0, $workWeekStartMonth, $workWeekStartDay + $offset, $workWeekStartYear) ){
				$hoursLeftTillWeeklyOverTime = $maxDailyHoursForOvertime;
				$weeklyTotalHours = 0;
				$weeklyTotalCritValueReached = "";
				$numDaysWorkedThisWeek = 0;
				$maxDaysInWeekCritValReached = "";
				$offset += 7; //new week

			}//if

			if (isset($useTheseClockPunches[$key])){
				$skipTheRest = false;
				$hours = $useTheseClockPunches[$key];
				$numDaysWorkedThisWeek += 1;

				if ($numDaysWorkedThisWeek > $maxWorkDaysForOvertime) {
					$regHours = 0;
					$otHours = $hours;
					$maxDaysInWeekCritValReached = "<span style=\"font-weight:700;\">&nbsp;&dagger;</span>";
					$skipTheRest = true;
				}//if

				$difference = $maxWeeklyHoursForOvertime - $weeklyTotalHours;
				if ($difference < 0){
					$difference = 0;
				}//if
				if ( ($difference - $maxDailyHoursForOvertime) >= 0 ){
					$priorityGoesTo = "CheckMaxDailyHoursFirst";
				}//if
				if ( ($difference - $maxDailyHoursForOvertime) < 0 ){
					$priorityGoesTo = "CheckDifferenceFirst";
				}//if

				if (isHoliday($key, $ot_holidays)) {
					$regHours = 0;
					$otHours = $hours;
				} else {
					if ($priorityGoesTo == "CheckDifferenceFirst"){
						if ( ($hours >= $difference) && ($skipTheRest == false) ){
							$regHours = $difference;
							$otHours = $hours - $regHours;
							$weeklyTotalCritValueReached = "<span style=\"font-weight:700;\">&nbsp; &dagger;</span>";
							$skipTheRest = true;
						}//if

						if ( ($hours >= $maxDailyHoursForOvertime) && ($skipTheRest == false) ){
							$regHours = $maxDailyHoursForOvertime;
							$otHours = $hours - $regHours;
							$maxDailyHoursCritValReached = "<span style=\"font-weight:700;\">&nbsp; &dagger;</span>";
							$skipTheRest = true;
						}//if

						if ( ($hours < $difference) && ($skipTheRest == false)  ){
							$regHours = $hours;
							$otHours = 0;
							$skipTheRest = true;
						}//if

					}//if
					if ($priorityGoesTo == "CheckMaxDailyHoursFirst"){
						if ( ($hours >= $maxDailyHoursForOvertime) && ($skipTheRest == false) ){
							$regHours = $maxDailyHoursForOvertime;
							$otHours = $hours - $regHours;
							$maxDailyHoursCritValReached = "<span style=\"font-weight:700;\">&nbsp; &dagger;</span>";
							$skipTheRest = true;
						}//if

						if ( ($hours >= $difference) && ($skipTheRest == false) ){
							$regHours = $difference;
							$otHours = $hours - $regHours;
							$weeklyTotalCritValueReached = "<span style=\"font-weight:700;\">&nbsp; &dagger;</span>";
							$skipTheRest = true;
						}//if

						if ( ($hours < $difference) && ($skipTheRest == false)  ){
							$regHours = $hours;
							$otHours = 0;
							$skipTheRest = true;
						}//if


					}//if
				}
				//echo "regHours: $regHours<br>";
				$hoursLeftTillWeeklyOverTime -= $hours;
				$weeklyTotalHours += $hours;

				$isDTHoliday = isHoliday($key, $dt_holidays);
				if ($hours > $maxDailyHoursForDoubletime || $isHoliday) {
					$dtHours = ($hours - $maxDailyHoursForDoubletime);
					if ($isHoliday) $dtHours = $hours;
					if ($otHours > $dtHours) $otHours -= $dtHours;
					else {
						$otHours = 0;
						$regHours -= $dtHours;
						if ($regHours < 0) $regHours = 0;
					}
				}
			}//if

			$dateArray = getdate($key);
			$date = $dateArray[mon] . "-" . $dateArray[mday] . "-" . $dateArray[year]; //mm-dd-yyyy format

			if ($key >= $startDateTimeStamp){
				$totalRegHours += $regHours;
				$totalOTHours += $otHours;
				$totalDTHours += $dtHours;
				$ot_steps[date('Y-m-d',$key)] = array(
					'reg_hours'    => $regHours,
					'ot_hours'     => $otHours,
					'dt_hours'     => $dtHours
				);
			}//if

			if ( $key ==  $endDateTimeStamp){
				/* Break the while loop */
				$runDates = true;
			}//if
			/* Reset values */
			$regHours    = 0;
			$otHours     = 0;
			$dtHours     = 0;

			$index++;
		}//while
		$output                   = array();
		$output["regular_hours"]  = $totalRegHours;
		$output["ot_hours"]       = $totalOTHours;
		$output["dt_hours"]       = $totalDTHours;
		$output["ot_steps"]       = $ot_steps;
		return $output;

	}//for
}//function end

function workWeekStartDay($weekDayNumber) {

	switch ($weekDayNumber) {
	case 0 : return "Sunday"; break;
	case 1 : return "Monday"; break;
	case 2 : return "Tuesday"; break;
	case 3 : return "Wednesday"; break;
	case 4 : return "Thursday"; break;
	case 5 : return "Friday"; break;
	case 6 : return "Saturday"; break;
	default: return "Sunday"; break;
	}
}//workWeekStartDay


/*							 if ($weekDayNumber == 0){
							 			$val = "Sunday";
										return $val;
								}//if

								if ($weekDayNumber == 1){
							 			$val = "Monday";
										return $val;
								}//if

								if ($weekDayNumber == 2){
							 			$val = "Tuesday";
										return $val;
								}//if

								if ($weekDayNumber == 3){
							 			$val = "Wednesday";
										return $val;
								}//if

								if ($weekDayNumber == 4){
							 			$val = "Thursday";
										return $val;
								}//if

								if ($weekDayNumber == 5){
							 			$val = "Friday";
										return $val;
								}//if

								if ($weekDayNumber == 6){
							 			$val = "Saturday";
										return $val;
								}//if

}*///workWeekStartDay

function isHoliday($ts, $allow_holidays) {

	$holiday = false;
	$year = date("Y", $ts);

	$holidays = array();
	$holidays_map = array(
		'new_years_day' => format_date($year, 1, 1),
		'martin_luther_king_day' => get_holiday($year, 1, 1, 3),
		'presidents_day' => get_holiday($year, 2, 1, 3),
		'easter' => calculate_easter($year),
		'memorial_day' => get_holiday($year, 5, 1, count_dows_in_month($year, 5, 1)),
		'independence_day' => format_date($year, 7, 4),
		'labor_day' => get_holiday($year, 9, 1, 1),
		'columbus_day' => get_holiday($year, 10, 1, 2),
		'veterans_day' => format_date($year, 11, 11),
		'thanksgiving' => get_holiday($year, 11, 4, 4),
		'christmas' => format_date($year, 12, 25),
	);

	foreach( $holidays_map as $holiday_name => $_holiday ){
		if( in_array( $holiday_name, $allow_holidays ) ) {
			$holidays[] = $_holiday;
		}
	}
	//echo "<br>".date("Y-m-d", $ts);
	//echo "<br>".print_r($allow_holidays, true);
	//echo "<br>".print_r($holidays_map, true);
	// echo 'date: ' . date("Y-m-d", $ts) . '<pre style="text-align: left;">' . print_r( $holidays, true ) . print_r( $allow_holidays, true ) . '</pre>';
	return in_array(date("Y-m-d", $ts), $holidays);
}

function format_date($year, $month, $day) {

	// pad single digit months/days with a leading zero for consistency (aesthetics)
	// and format the date as desired: YYYY-MM-DD by default

	if (strlen($month) == 1) $month = "0". $month;
	if (strlen($day) == 1) $day = "0". $day;
	$date = $year ."-". $month ."-". $day;

	return $date;
}

function calculate_easter($y) {

	// In the text below, 'intval($var1/$var2)' represents an integer division neglecting
	// the remainder, while % is division keeping only the remainder. So 30/7=4, and 30%7=2
	//
	// This algorithm is from Practical Astronomy With Your Calculator, 2nd Edition by Peter
	// Duffett-Smith. It was originally from Butcher's Ecclesiastical Calendar, published in
	// 1876. This algorithm has also been published in the 1922 book General Astronomy by
	// Spencer Jones; in The Journal of the British Astronomical Association (Vol.88, page
	// 91, December 1977); and in Astronomical Algorithms (1991) by Jean Meeus.

	$a = $y%19;
	$b = intval($y/100);
	$c = $y%100;
	$d = intval($b/4);
	$e = $b%4;
	$f = intval(($b+8)/25);
	$g = intval(($b-$f+1)/3);
	$h = (19*$a+$b-$d-$g+15)%30;
	$i = intval($c/4);
	$k = $c%4;
	$l = (32+2*$e+2*$i-$h-$k)%7;
	$m = intval(($a+11*$h+22*$l)/451);
	$p = ($h+$l-7*$m+114)%31;
	$EasterMonth = intval(($h+$l-7*$m+114)/31);    // [3 = March, 4 = April]
	$EasterDay = $p+1;    // (day in Easter Month)

	return format_date($y, $EasterMonth, $EasterDay);
}

function observed_day($year, $month, $day) { // may need for Veterans Day?
	// sat -> fri & sun -> mon, any exceptions?
	//
	// should check $lastday for bumping forward and $firstday for bumping back,
	// although New Year's & Easter look to be the only holidays that potentially
	// move to a different month, and both are accounted for.

	$dow = date("w", mktime(0, 0, 0, $month, $day, $year));

	if ($dow == 0) $dow = $day + 1;
	elseif ($dow == 6) {
		if (($month == 1) && ($day == 1)) {    // New Year's on a Saturday
			$year--;
			$month = 12;
			$dow = 31;
		} else $dow = $day - 1;
	} else $dow = $day;

	return format_date($year, $month, $dow);
}

function get_holiday($year, $month, $day_of_week, $week_of_month) {
	// i.e. find the 3rd Monday in January, 2000

	if (($week_of_month > 5) || ($week_of_month < 1) || ($day_of_week > 6) || ($day_of_week < 0)) {
		// should check any month/week combo first against $lastday...
		return FALSE;
	} else {
		$firstday = date("w", mktime(0, 0, 0, $month, 1, $year));
		$lastday = date("t", mktime(0, 0, 0, $month, 1, $year));

		// start by finding the first occurence in the month of that $day_of_week #
		// then add appropriate number of weeks

		if ($firstday > $day_of_week) {
			// means we need to jump to the second week to find the first $day_of_week
			$diff = ($firstday - $day_of_week);
			$first_dow = (7 - $diff) + 1;
			$day = $first_dow + (($week_of_month * 7) - 7);
		} elseif ($firstday < $day_of_week) {
			// correct week, now move forward to specified day
			$day = ($day_of_week - $firstday + 1) + (($week_of_month * 7) - 7);
		} else {
			// correct day in first week ($firstday = $day_of_week)
			$day = 1 + (($week_of_month * 7) - 7);
		}
		$date = format_date($year, $month, $day);
		return $date;
	}
}

function count_dows_in_month($year, $month, $day_of_week) {
	// count how many weeks in the month have a specified day, such as Monday.
	// we know there will be 4 or 5, so no need to check for $weeks<4 or $weeks>5

	$firstday = date("w", mktime(0, 0, 0, $month, 1, $year));
	$lastday = date("t", mktime(0, 0, 0, $month, 1, $year));

	if ($firstday > $day_of_week) {
		// means we need to jump to the second week to find the first $day_of_week
		$d = (7 - ($firstday - $day_of_week)) + 1;
	} elseif ($firstday < $day_of_week) {
		// correct week, now move forward to specified day
		$d = ($day_of_week - $firstday + 1);
	} else {    // $firstday = $day_of_week
		// correct day in first week
		$d = ($firstday - 1);
	}

	$d += 28;    // jump to the 5th week and see if the day exists
	if ($d > $lastday) $weeks = 4;
	else $weeks = 5;

	return $weeks;
}


function pad($str,$length){
	while (strlen($str)*1 < $length*1){
		$str = "0".$str;
	}
	return $str;
}//pad

if (isset($summary) && $summary == "no"){
	//this means only one employee's records has been selected
	echo "<script type='text/javascript'>showSummary('$class_name');</script>";
}//if

?>