<?php
$onFailure = json_encode([
    'status' => 'failure',
    'message' => speak('Something went wrong, please try again. If the issue still persists please contact our support team, thank you!')
]);
$onSuccess = json_encode([
    'status' => 'success',
    'message' => speak('Thank you. Your CSV export request is in progress and will be delivered shortly.')
]);
if ($report !== false) {
    $areaReport = new area_reports([
        "report" => $report,
        "output_mode" => $output_mode
    ]);
    echo $areaReport->processEmailReport() ? $onSuccess : $onFailure;
} else {
    echo $onFailure;
}
exit;
?>