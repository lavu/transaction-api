<?php
	/********************************************************************************
	 * The time cards reports was super long and super difficult to read, so
	 * I made this report to ease readability and management, and to reduce code
	 * size and complexity.
	 *
	 * The basic idea is that computing overtime is too much for my mind to handle,
	 * so I pass each part of the calculation off to an object to compute for me.
	 *
	 * Object structure:
	 * .Account Settings
	 * .User ->
	 *     .Work Week ->
	 *         .Work Day ->
	 *             .Work Hour
	 *
	 * Notes:
	 * obeys location time zone
	 * obeys leap days
	 * obeys daylight savings time
	 * does not obey day_start_end_time
	 * does not check for overlapping punches when loading the report
	 * checks for overlapping punches when saving punches
	 * obeys employee pay rates (once pay rates are set up, not currently developed)
	 *
	 * @author: Benjamin G. Bean (benjamin@poslavu.com)
	 ********************************************************************************/
	global $location_info;

	define('ALL', 0);
	define('REGULAR', 1);
	define('OVERTIME', 2);
	define('DOUBLE_TIME', 3);
	define('START', 0);
	define('END', 1);
	define('BOTH', 2);
	require_once(dirname(__FILE__).'/../../resources/dimensional_form.php');
	require_once dirname(dirname(dirname(dirname(__FILE__)))).'/sa_cp/show_report.php';

	$current_tz = date_default_timezone_get();
	if( !empty( $location_info['timezone'])){
		date_default_timezone_set( $location_info['timezone'] );
	}
	if (!isset($_GET['ordermode'])) {
		$_GET['ordermode']='asc';
		$_GET['order_by_col']='id';
	}

	ConnectionHub::$useReplica = TRUE;

	class SortOrder {
		private $order;
		private function __construct(){
			if( isset($_GET['order']) && '1' == $_GET['order']){
				$this->order = array( 2, 1, 4 );
			} else {
				$this->order = array( 1, 2, 4 );
			}
		}

		public function order(){
			return $this->order;
		}

		public static function sortOrder(){
			static $sortOrder = null;
			if( !$sortOrder ){
				$sortOrder = new SortOrder();
			}
			return $sortOrder;
		}
	}

	class UserRole {
		public
			$id,
			$title,
			$type,
			$_deleted,
			$tipoutRules;
		private static $_all_roles;

		private static function initializeAllRoles() {
			UserRole::$_all_roles = array();

			$a_all_roles = ConnectionHub::getConn('rest')->getDBAPI()->getAllInTable('emp_classes');
			foreach( $a_all_roles as $_ => $a_row ) {
				$role = new UserRole( $a_row );
				UserRole::$_all_roles[ $role->id ] = $role;
			}
		}
		private function __construct( $a_role_data ){
			$this->id = $a_role_data['id'];
			$this->title = $a_role_data['title'];

			$this->type = $a_role_data['type'];
			$this->_deleted = $a_role_data['_deleted'];
			$this->tipoutRules = $a_role_data['tipout_rules'];
		}

		public static function getUserRole( $role_id ){
			if( UserRole::$_all_roles === null ) {
				UserRole::initializeAllRoles();
			}
			return UserRole::$_all_roles[$role_id];
		}
	}

	class TimeCardUserRoles {
		public function __construct( $roles ){
			$this->roles = array();
			$roles_break = explode(",", $roles);

			foreach( $roles_break as $k => $role ){
				$role = explode("|", $role );
				$role_id = $role[0];

				$this->roles[$role_id] = UserRole::getUserRole( $role_id );
			}
		}

		public function getRole( $i_role_id ){
			return (isset($this->roles[$i_role_id])) ? $this->roles[$i_role_id] : UserRole::getUserRole( $i_role_id );
		}
	}

	class TimeCardRates {

		public function __construct($f_rate, $roles) {
			// load the rates
			$this->rates = array();
			$this->rates[0] = $f_rate;

			$role_ids_payrates = explode(",", $roles);
			foreach( $role_ids_payrates as $k => $role_id_payrate ){
				$role_rate = explode("|", $role_id_payrate);
				if(count($role_rate > 1) && !empty($role_rate[1]) ){
					$this->rates[$role_rate[0]] = (float)$role_rate[1];
				} else {
					$this->rates[$role_rate[0]] = $f_rate;
				}
			}
		}

		public function get_rate_id($f_rate) {
			foreach($this->rates as $i_id=>$a_rate) {
				if ($a_rate['rate'] == $f_rate) {
					return $i_id;
				}
			}
		}

		public function get_rate_rate($i_rate_id) {
			if( !isset( $this->rates[$i_rate_id] ) ){
				return $this->rates[0];
			}
			return $this->rates[$i_rate_id];
		}
	}

	// all of the functions most commonly used by users of this file
	class TimeCardUserFunctions {

		// returns the rows from the db of all users that qualify for the time cards report
		// uses lavu_query, not mlavu query
		public static function get_eligable_users($s_user = '') {
			$a_wherevars = array('_deleted'=>0);
			if ($s_user !== '') $a_wherevars['username'] = $s_user;
			return ConnectionHub::getConn('rest')->getDBAPI()->getAllInTable('users', $a_wherevars, FALSE, array('orderbyclause'=>"ORDER BY `l_name` ASC, `f_name` ASC, `username` ASC"));
		}

		public static function get_choosen_users() {
			if (!isset($_GET['user']) || $_GET['user'] == 'all')
				return self::get_eligable_users();
			else
				return self::get_eligable_users($_GET['user']);
		}

		// creates all TimeCardUsers and returns them in an array
		// convenience function
		// @$i_start_time: start to load punches from
		// @$i_end_time: end time to load punches from
		// @return: an array('users'=>array(userid=>TimeCardUser, ...), 'account_settings'=>TimeCardAccountSettings, 'time'=>('provided_starttime'=>datetime, 'provided_endtime'=>datetime, 'week_starttime'=>datetime, 'week_endtime'=>datetime))
		public static function load_all_users($i_start_time, $i_end_time) {
			global $location_info;

			$a_retval = array();
			$a_punches = array();

			// load the account settings
			$o_account_settings = TimeCardAccountSettings::settings($i_start_time, $i_end_time);
			$a_retval['account_settings'] = $o_account_settings;

			// load all relevant time punches (within this time period)
			$a_punches['counted'] = TimeCardTimePunch::get_time_punches();

			// get all user ids that have time punches
			// only searches for uses that have time punches in the given time period
			$a_retval['users'] = array();
			$a_user_ids = array();
			for ($i = 0; $i < count($a_punches['counted']); $i++)
				$a_user_ids[] = $a_punches['counted'][$i]->userid;

			// get punches for the previous week, for computing overtime and doubletime
			$a_punches['other'] = TimeCardTimePunch::get_time_punches( $o_account_settings->start_week, $o_account_settings->end_week);
			TimeCardTimePunch::filter_out_punches($a_punches['other'], $a_punches['counted']);
			unset($a_punches['counted']);

			// load the users with ids in the punches from the given time period
			$a_db_users = array();
			if (count($a_user_ids) > 0){
				if(isset($_GET['user']) && $_GET['user'] == "all" ){
					$a_db_users = ConnectionHub::getConn('rest')->getDBAPI()->getAllInTable('users', NULL, FALSE, array('whereclause'=>"WHERE `id` ".ConnectionHub::getConn('rest')->getDBAPI()->arrayToInClause($a_user_ids, TRUE)));
				} else {
					$a_db_users = ConnectionHub::getConn('rest')->getDBAPI()->getAllInTable('users', array( "username" => $_GET['user'] ), FALSE, array('whereclause'=>"WHERE `username` = '[username]'"));
				}
			}
			if (count($a_db_users) > 0) {

				// get the TimeCardUser objects for each user loaded
				$a_id_users = array();
				$a_to_unset = array();
				for($i = 0; $i < count($a_db_users); $i++) {
					$i_id = (int)$a_db_users[$i]['id'];
					$a_id_users[$i_id] = $a_db_users[$i];
					$a_retval['users'][$i_id] = TimeCardUser::user($i_id, $a_id_users);
					if (!$a_retval['users'][$i_id]->userfound) {
						$a_to_unset[] = array('users_ids_index'=>$i, 'users_index'=>$i_id);
					}
				}

				// release memory for users that can't be found
				foreach($a_to_unset as $a_unset_vars) {
					unset($a_user_ids[$a_unset_vars['users_ids_index']]);
					unset($a_retval['users'][$a_unset_vars['users_index']]);
				}
				unset($a_db_users);
				unset($a_id_users);
				unset($a_to_unset);
			}

			$locationid          = $location_info['id'];
			$searchForType       = "overtime_setting_day2"; // this represents from the config table associated with overtime settings */
			$settingsResults     = lavu_query("SELECT * FROM `config` WHERE `location`='[1]' AND type='[2]'",$locationid, $searchForType);
			$overtimeSettingDay2 = mysqli_fetch_assoc($settingsResults);
			// process time punches for each user
			foreach($a_retval['users'] as $i_id=>$o_user) {
				foreach( $a_punches['other'] as $punch_id => $o_punch ){
					if( $o_punch->userid == $o_user->id ){
						$o_user->addPunch( $o_punch );
					}
				}

				$a_punches_used = $o_user->process_time_punches( $o_user->punches(), '', $overtimeSettingDay2);

				// remove punches used so that they aren't processed again
				foreach($a_punches['other'] as $k=>$o_punch) {
					if (in_array($o_punch->id, $a_punches_used)) {
						unset($a_punches['other'][$k]);
					}
				}
			}

			return $a_retval;
		}

		// used to create a date chooser object, which pulls data from the get vars and provides optiosn to change the date
		// @$mode: the getvar mode to change to when the date gets changed
		// @return: an object with the following fields:
		//     use_date: the "from" date
		//     use_date2: the "to" date
		//     select_clause: the html to produce a date picker
		//     mode: the mode to forward to
		public static 	function get_date_chooser_obj($mode = NULL) {

			$current_tz = date_default_timezone_get();
			if( !empty($location_info['timezone']) ){
				date_default_timezone_set( $location_info['timezone'] );
			}
			global $o_time_cards_chooser_obj;
			if (isset($o_time_cards_chooser_obj))
				return $o_time_cards_chooser_obj;

			// sanitize the input
			if ($mode === NULL)
				$mode = (isset($_GET['mode'])) ? $_GET['mode'] : '';
			global $section;

			$extra = "";
			if( isset( $_GET['format'] )){
				$extra .= "&format={$_GET['format']}";
			}

			if( isset( $_GET['totalsonly'])){
				$extra .= "&totalsonly={$_GET['totalsonly']}";
			}

			$s_userstr = (isset($_GET['user']) ? '&user='.$_GET['user'] : '');
			global $data_name;
			ob_start();
			$show_report_results = show_report("poslavu_{$data_name}_db",'poslavu_MAIN_db','unified', "index.php?mode={$mode}{$s_userstr}{$extra}",'select_date' );
			$time_picker = ob_get_contents();
			ob_end_clean();

			// create the object to return
			$use_date1 = $show_report_results['date_start'] . ' ' . $show_report_results['time_start'];
			$use_date2 = $show_report_results['date_end'] . ' ' . $show_report_results['time_end'];

			//$duration = isset($_REQUEST['day_duration']) ? $_REQUEST['day_duration']*1 : (int)((strtotime( $use_date2 ) - strtotime( $use_date1 ))/(3600*24));
			//Above Line does not work properly with daylight savings time, the following is to fix this problem - CF - 3/28/16
			$duration = isset($_REQUEST['day_duration']) ? $_REQUEST['day_duration']*1 : (int)round((strtotime( $show_report_results['date_end'] ) - strtotime( $show_report_results['date_start'] ))/(3600*24));

			$o_retval = new stdClass();
			$o_retval->use_date1 = $show_report_results['date_start'];
			$o_retval->use_date2 = $show_report_results['date_end'];
			$o_retval->duration = $duration;
			$o_retval->select_clause_with_extra_vars = $time_picker;//$select;
			$o_retval->select_clause = str_replace('--extra_vars--', '', $time_picker);
			$o_retval->mode = $mode;


			$o_retval->forward_vars = "mode={$mode}&setdate={$o_retval->use_date1}&day_duration={$duration}{$extra}";

			$o_time_cards_chooser_obj = $o_retval;


			date_default_timezone_set( $current_tz );

			return $o_retval;
		}
	}

	class TimeCardTimePunch {

		// turn an sql row into a time punch
		// @$a_sql_row: the array from sql (will not be edited)
		// @$i_rate_id: the id of the rate for this time punch, defaults to 0
		public function __construct(&$a_sql_row, $i_rate_id = 0) {
			global $location_info;
			$current_tz = date_default_timezone_get();
			if( !empty($location_info['timezone']) ){
				date_default_timezone_set( $location_info['timezone'] );
			}
			// parse the sql data
			$this->id = (int)$a_sql_row['id'];
			$this->locationid = (int)$a_sql_row['location_id'];
			$this->userid = (int)$a_sql_row['server_id'];
			$this->time_in = strtotime($a_sql_row['time']);
			$this->time_out = ($a_sql_row['time_out'] == '' ? time() : strtotime($a_sql_row['time_out']));
			$this->clocked_out = ($a_sql_row['time_out'] == '' ? FALSE : TRUE);
			$this->clocked_out_via_app = ($a_sql_row['punched_out'] == '1' ? TRUE : FALSE);
			$this->deleted = ($a_sql_row['_deleted'] == '0' ? FALSE : TRUE);
			$this->rate_id = $i_rate_id;
			$this->in_requested_time_period = TRUE;
			$this->role_id = (int)$a_sql_row['role_id']; // the employee class the punch is attributed to
			$this->contribution = array( REGULAR => array(), OVERTIME => array(), DOUBLE_TIME => array() );
			$this->payrate = (double)$a_sql_row['payrate'];
			$this->time_in -= $this->time_in % 60;
			$this->time_out -= $this->time_out % 60;
			$this->is_break_out = ($a_sql_row['break'])?true:false;

			date_default_timezone_set( $current_tz );
		}

		// markes punches as either in_requested_time_period or not in_requested_time_period
		// @$a_punches: a set of TimeCardTimePunches
		public static function filter_out_punches(&$a_punches, &$a_punches_in_time_period) {

			// get a list of all ids not to include
			$a_dont_include_ids = array();
			$a_in_period_ids = array();
			foreach($a_punches_in_time_period as $o_punch)
				$a_in_period_ids[] = $o_punch->id;

			// remove punches from $a_punches that match
			foreach($a_punches as $k=>$o_punch) {
				if (in_array($o_punch->id, $a_in_period_ids))
					$a_punches[$k]->in_requested_time_period = TRUE;
				else
					$a_punches[$k]->in_requested_time_period = FALSE;
			}
		}

		public function addContribution( $contribution ){
			if(!is_array($contribution)){
				return;
			}
			foreach( $contribution as $time_type => $a_payrate_times ){
				foreach( $a_payrate_times as $pay_rate_id => $contribution_seconds ){
					if( !isset( $this->contribution[$time_type][$pay_rate_id] ) ){
						$this->contribution[$time_type][$pay_rate_id] = 0;
					}
					$this->contribution[$time_type][$pay_rate_id] += $contribution_seconds;
				}
			}
		}

		// determine if this punch falls on the given day
		// @$i_date: the UTC datetime to test
		// @return: START, END, BOTH, OVERLAP, or FALSE
		//     OVERLAP means that the time punch is longer than the day
		public function in_day($i_date) {

			// get the start time of this day and the next
			$i_day = self::utc_to_date($i_date);
			$i_next_day = strtotime(date('Y-m-d',$i_day).' +1 day');

			// determine if the start/end time is in this day's time interval
			$b_start = FALSE;
			$b_end = FALSE;
			if ($this->time_in >= $i_day && $this->time_in < $i_next_day)
				$b_start = TRUE;
			if ($this->time_out > $i_day && $this->time_out <= $i_next_day)
				$b_end = TRUE;

			// return values
			if ($b_start && $b_end)
				return BOTH;
			if ($b_start)
				return START;
			if ($b_end)
				return END;

			// determine if the punch is longer than the day
			if ($this->time_in <= $i_day && $this->time_out >= $i_next_day)
				return OVERLAP;

			return FALSE;
		}

		// determine if this punch falls on the given hour
		// @$i_date: the UTC datetime to test
		// @return: START, END, BOTH, OVERLAP, or FALSE
		//     OVERLAP means that the time punch is longer than the hour
		public function in_hour($i_date) {

			// get the start time of this day and the next
			$i_day = self::utc_to_hour($i_date);
			$i_next_hour = strtotime(date('Y-m-d H',$i_day).' +1 hour');

			// determine if the start/end time is in this hour's time interval
			$b_start = FALSE;
			$b_end = FALSE;
			if ($this->time_in >= $i_day && $this->time_in < $i_next_hour)
				$b_start = TRUE;
			if ($this->time_out > $i_day && $this->time_out <= $i_next_hour)
				$b_end = TRUE;

			// return values
			if ($b_start && $b_end)
				return BOTH;
			if ($b_start)
				return START;
			if ($b_end)
				return END;

			// determine if the punch is longer than the hour
			if ($this->time_in <= $i_day && $this->time_out >= $i_next_hour)
				return OVERLAP;

			return FALSE;
		}

		// get time punches for every entry in `clock_punches`
		// @$o_account_settings: an TimeCardAccountSettings object
		// @$i_start_datetime: the start datetime (will load punches that start on or after this time, or $o_account_settings->start_time if not set)
		// @$i_end_datetime: the end datetime (will load punches that start on or before this time, or $o_account_settings->end_time if not set)
		// @$i_user_id: the user id to load punches for (-1 for all)
		// @return: an array of TimeCardTimePunch objects
		public static function get_time_punches( $i_start_datetime = -1, $i_end_datetime = -1, $i_user_id = -1) {

			// decide to load all user or just one user
			$s_user = ($i_user_id == -1 ? '' : "AND `id`='[id]'");

			$o_account_settings = TimeCardAccountSettings::settings();
			// set time start/end
			if ($i_start_datetime == -1) $i_start_datetime = $o_account_settings->start_time;
			if ($i_end_datetime == -1) $i_end_datetime = $o_account_settings->end_time;

			// load rows from the database
			$a_wherevars = array(
				'id'			=> $i_user_id,
				'location_id'	=> $o_account_settings->locationid,
				'time'			=> date("Y-m-d H:i:s", $i_start_datetime),
				'time_out'		=> date("Y-m-d H:i:s", $i_end_datetime)
			);

			$a_extras = array(
				'orderbyclause'	=> "ORDER BY `server_id`, `time` ASC",
				'whereclause'	=> "WHERE `location_id` = '[location_id]' AND ( ( `time` <= '[time_out]' AND `time_out` >= '[time_out]' ) || ( `time` <= '[time]' AND `time_out` >= '[time]' ) || ( `time` >= '[time]' AND `time_out` <= '[time_out]' ) ) AND `_deleted` = '0' ".$s_user
			);

			$a_time_punches = ConnectionHub::getConn('rest')->getDBAPI()->getAllInTable('clock_punches', $a_wherevars, FALSE, $a_extras);

			// $arr = array();
			// echo '<pre style="text-align: left">' . print_r( $a_time_punches, true ) . '</pre>';

			if (count($a_time_punches) == 0)
				return array();

			// parse the rows into time punches
			$a_retval = array();
			for($i = 0; $i < count($a_time_punches); $i++){
				$a_retval[] = new TimeCardTimePunch($a_time_punches[$i]);
			}

			$last_punch = null;
			for( $i = 0; $i < count( $a_retval ); $i++ ){
				$current_punch = $a_retval[$i];
				if( $last_punch->userid == $current_punch->userid ){
					$last_punch->time_to_next_punch = $current_punch->time_in - $last_punch->time_out;
				}
				$last_punch = $a_retval[$i];
			}
			// echo '<pre style="text-align: left">' . print_r( $a_retval, true ) . '</pre>';

			return $a_retval;
		}

		// convert a UTC time to a day, exactly
		// @$i_date: a UTC datetime
		// @return: a UTC datetime (eg strtotime('Y-m-d'))
		public static function utc_to_date($i_datetime) {
			return mktime(0, 0, 0, (int)date('m',$i_datetime), (int)date('d',$i_datetime), (int)date('Y',$i_datetime));
		}

		// convert a UTC time to an hour, exactly
		// @$i_date: a UTC datetime
		// @return: a UTC datetime (eg strtotime('Y-m-d H'))
		public static function utc_to_hour($i_datetime) {
			return mktime((int)date('H',$i_datetime), 0, 0, (int)date('m',$i_datetime), (int)date('d',$i_datetime), (int)date('Y',$i_datetime));
		}

		// adds the hours together for the two arrays and store the value in $a_hours1
		// assumes that $a_hours1 is the primary destination and $a_hours2 is the primary source
		public static function sum_hours(&$a_hours1, &$a_hours2) {
			if (!is_array($a_hours2) || count($a_hours2) == 0)
				return $a_hours1;
			foreach($a_hours2 as $type=>$a_hours_by_rate) {
				if (!isset($a_hours1[$type]))
					$a_hours1[$type] = array();
				foreach($a_hours_by_rate as $rate=>$i_hours) {
					if (!isset($a_hours1[$type][$rate]))
						$a_hours1[$type][$rate] = 0;
					$a_hours1[$type][$rate] += $i_hours;
				}
			}
		}
	}

	// contains all of the account settings relevent to time cards
	// should be created to be passed on to TimeCardUser upon creation
	class TimeCardAccountSettings {

		// load all of the settings
		// @$i_start_time: the time to start loading time cards from (can be 0 if time cards aren't actually going to be processed)
		// @$i_end_time: the time to finish loading time cards from (can be 0 if time cards aren't actually going to be processed)
		private function __construct($i_start_time, $i_end_time) {

			global $location_info;
			$current_tz = date_default_timezone_get();
			if( !empty($location_info['timezone']) ){
				date_default_timezone_set( $location_info['timezone'] );
			}
			// basic settings

			//if(isset($_GET['nialls_testing']))
			//	echo "the location id is:".sessvar("locationid");

			$this->locationid = sessvar("locationid");
			$this->type = "overtime_setting_day2";
			$this->weekdays = array('SUNDAY', 'MONDAY', 'TUESDAY', 'WEDNESDAY', 'THURSDAY', 'FRIDAY', 'SATURDAY');
			$this->weekday_keys = array_flip($this->weekdays);
			$this->week_start = $this->weekday_keys['SUNDAY'];
			$this->obey_day_startend = 'midnight';
			$this->day_start_end_time = 0;
			$this->day_start_hour_offset = 0;
			$this->day_start_minute_offset = 0;

			// Default values - If user never specifies anything use values so large they are effectively not set
			$this->ot_settings = new stdClass();
			$this->ot_settings->daily_seconds = 24 * 3600;
			$this->ot_settings->weekly_days = 7;
			$this->ot_settings->weekly_seconds = 10000 * 3600;
			$this->ot_settings->holidays = array();
			$this->ot_settings->holidays_dates = array();
			$this->dt_settings->daily_seconds = 10000 * 3600;
			$this->dt_settings->holidays = array();
			$this->dt_settings->holidays_dates = array();

			// load settings
			$this->load_settings();
			$this->set_dates($i_start_time, $i_end_time, true);
			date_default_timezone_set( $current_tz );
		}

		public static function settings( $i_start_time = null, $i_end_time = null ){
			static $settings = null;
			if(!$settings && $i_start_time !== null && $i_end_time !== null ){
				$settings = new TimeCardAccountSettings( $i_start_time, $i_end_time );
			}
			return $settings;
		}

		// determine if the given date is a holiday
		// @$i_date: the date to be processed, in UTC time
		// @return: DOUBLE_TIME, OVERTIME, or REGULAR
		public function is_holiday($i_date) {

			require_once(dirname(__FILE__).'/calOvertime.php');

			// sanitize the input
			$i_date = TimeCardTimePunch::utc_to_date($i_date);

			// check double time holidays
			if (!isset($this->dt_settings->holidays_dates[$i_date])) {
				$this->dt_settings->holidays_dates[$i_date] = isHoliday($i_date, $this->dt_settings->holidays);
			}
			if ($this->dt_settings->holidays_dates[$i_date]) {
				return DOUBLE_TIME;
			}

			// check overtime holidays
			if (!isset($this->ot_settings->holidays_dates[$i_date])) {
				$this->ot_settings->holidays_dates[$i_date] = isHoliday($i_date, $this->ot_settings->holidays);
			}
			if ($this->ot_settings->holidays_dates[$i_date]) {
				return OVERTIME;
			}

			return REGULAR;
		}

		// loads the overtime/double time settings from the config table
		private function load_settings() {
			global $location_info;
			// load the settings
			$a_settings = ConnectionHub::getConn('rest')->getDBAPI()->getAllInTable('config', array('location'=>$this->locationid, 'type'=>$this->type), FALSE,
				array('selectclause'=>'`value` AS `ot_daily_hours`, `value2` AS `week_start`, `value3` AS `ot_weekly_days`, `value4` AS `ot_weekly_hours`, `value5` AS `dt_daily_hours`, `value6` AS `obey_day_startend`, `value_long` AS `ot_holidays`, `value_long2` AS `dt_holidays`'));
			if (count($a_settings) == 0)
				return;
			$a_settings = $a_settings[0];

			// parse the holidays
			$this->ot_settings->holidays = ($a_settings['ot_holidays'] == '' ? array() : explode("|", $a_settings['ot_holidays']));
			$this->dt_settings->holidays = ($a_settings['dt_holidays'] == '' ? array() : explode("|", $a_settings['dt_holidays']));

			// parse the hourly/daily settings
			if ($a_settings['week_start'] != 'DNA')
				$this->week_start = max(min((int)$a_settings['week_start'], $this->weekday_keys['SATURDAY']), $this->weekday_keys['SUNDAY']);
			if ($a_settings['ot_daily_hours']  != 'DNA')
				$this->ot_settings->daily_seconds = max((int)$a_settings['ot_daily_hours'], 0) * 3600;
			if ($a_settings['ot_weekly_days']  != 'DNA')
				$this->ot_settings->weekly_days = max((int)$a_settings['ot_weekly_days'], 0);
			if ($a_settings['ot_weekly_hours']  != 'DNA')
				$this->ot_settings->weekly_seconds = max((int)$a_settings['ot_weekly_hours'], 0) * 3600;
			if ($a_settings['dt_daily_hours']  != 'DNA' && $a_settings['dt_daily_hours'] !== '')
				$this->dt_settings->daily_seconds = max((int)$a_settings['dt_daily_hours'], 0) * 3600;

			$this->obey_day_startend = (!empty($a_settings['obey_day_startend'])) ? $a_settings['obey_day_startend'] : 'midnight';
			if( ! empty($location_info['day_start_end_time']) && $this->obey_day_startend == 'daystartend' ){
				$day_start_end_time = $location_info['day_start_end_time']*1;
				$hour_offset = (($day_start_end_time/100)%100);
				$minute_offset = ($day_start_end_time%100);

				$this->day_start_end_time = $day_start_end_time;
				$this->day_start_hour_offset = $hour_offset;
				$this->day_start_minute_offset = $minute_offset;
			}

			$this->weekday_start = $this->weekdays[$this->week_start];
		}

		// computes the overlapping weeks of the start and end times
		// @$i_start_time: the starting time of the query
		// @$i_end_time: the ending time of the query
		public function set_dates($i_start_time, $i_end_time, $b_set_start_and_end=false) {
			global $location_info;
			$current_tz = date_default_timezone_get();
			if( !empty($location_info['timezone']) ){
				date_default_timezone_set( $location_info['timezone'] );
			}

			$hour_offset = 0;
			$minute_offset = 0;
			if( ! empty($location_info['day_start_end_time'])){
				$day_start_end_time = $location_info['day_start_end_time']*1;
				$hour_offset = (($day_start_end_time/100)%100) * 3600;
				$minute_offset = ($day_start_end_time%100) * 60;
			}
			// log the provided times
			$i_start_time += $hour_offset + $minute_offset;
			$i_end_time += $hour_offset + $minute_offset;
			if( $b_set_start_and_end ){
				$this->start_time = $i_start_time;
				$this->end_time = $i_end_time;
			}
			$i_start_date = TimeCardTimePunch::utc_to_date($i_start_time);
			$i_end_date = TimeCardTimePunch::utc_to_date($i_end_time);

			// compute the start of the week for $i_start_date, and the end of the week for $i_end_date
			$week_starttime = TimeCardWorkWeek::compute_starttime($i_start_time, $this->weekday_start, $this->day_start_hour_offset, $this->day_start_minute_offset );
			$week_endtime = TimeCardWorkWeek::compute_endtime($i_end_time, $this->weekday_start, $this->day_start_hour_offset, $this->day_start_minute_offset );

			// store the week duration
			$this->start_week_human_readable = date('Y-m-d H:i:s', $week_starttime);
			$this->end_week_human_readable = date('Y-m-d H:i:s', $week_endtime);
			$this->start_week = strtotime($this->start_week_human_readable);
			$this->end_week = strtotime($this->end_week_human_readable);
			date_default_timezone_set( $current_tz );
		}
	}

	// a user contains all information for time punches processed
	// start by creating the user, then processing their time punches or the desired dates, then recalling the desired information
	// a time card user contains a number of time card work weeks
	class TimeCardUser {

		// set some defaults
		// @$user_id: id of the user
		// @$o_account_settings: a TimeCardAccountSettings object
		// @$a_preloaded_users: array of users (so that each user doesn't have to be loaded individually)
		private function __construct($user_id, &$a_preloaded_users = NULL) {

			// some basic settings
			$this->id = (int)$user_id;
			$this->work_weeks = array(); // indexed by their week start time
			// $this->processed = FALSE;
			$o_account_settings = TimeCardAccountSettings::settings();
			$this->timePunches = array();
			// load the user
			if ($a_preloaded_users === NULL)
				$a_users = ConnectionHub::getConn('rest')->getDBAPI()->getAllInTable('users', array('id'=>$this->id, 'loc_id'=>$o_account_settings->locationid), FALSE);
			$this->userfound = FALSE;
			if (count($a_users) > 0 || $a_preloaded_users !== NULL) {
				$a_user = ($a_preloaded_users === NULL ? $a_users[0] : $a_preloaded_users[$user_id]);
				$this->userfound = TRUE;
				$this->username = $a_user['username'];
				$this->last_name = $a_user['l_name'];
				$this->first_name = $a_user['f_name'];
				$this->full_name = ( !empty( $a_user['l_name'] ) ? ucfirst($a_user['l_name']).', '.ucfirst($a_user['f_name']) : ucfirst($a_user['f_name']));
				$this->access = (int)$a_user['access_level'];
				$this->locationid = ((int)$a_user['loc_id'] == 0 ? $o_account_settings->locationid : (int)$a_user['loc_id']);
				$this->lavu_admin = ($a_user['lavu_admin'] == '1' ? TRUE : FALSE);
				$this->deleted = ($a_user['_deleted'] == '0' ? FALSE : ($a_user['active'] == '1' ? TRUE : FALSE));
				$this->classes = new TimeCardUserRoles( $a_user['role_id'] );
				$this->payrates = new TimeCardRates($a_user['payrate'], $a_user['role_id']);
				$this->clocked_in = $a_user['clocked_in'] == '1' ? TRUE : FALSE;
			}
		}
		private static $users = array();
		public static function user( $user_id, &$a_preloaded_users = NULL ){
			if(!isset(TimeCardUser::$users[$user_id])){
				TimeCardUser::$users[$user_id] = new TimeCardUser( $user_id, $a_preloaded_users );
			}
			return TimeCardUser::$users[$user_id];
		}

		public static function userByUsername( $username ){
			if( count( TimeCardUser::$users ) ){
				foreach( TimeCardUser::$users as $user_id => $user ){
					if( $user->username == $username ){
						return $user;
					}
				}
			}
			//Not Found....
			$o_account_settings = TimeCardAccountSettings::settings();
			if( $user_result = rpt_query("SELECT * FROM `users` WHERE `username`='[1]'", $username, $o_account_settings->locationid ) ){
				$a_preloaded_users = array();
				while( $user_info = mysqli_fetch_assoc( $user_result ) ){
					$a_preloaded_users[ $user_info['id'] ] = $user_info;
				}

				foreach( $a_preloaded_users as $user_id => $value ){
					TimeCardUser::user( $user_id, $a_preloaded_users );
				}

				// echo '<pre style="text-align: left;">' . print_r( $a_preloaded_users, true ) . '</pre>';
				// echo '<pre style="text-align: left;">' . print_r( TimeCardUser::$users, true ) . '</pre>';

				if( count($a_preloaded_users) ){
					return TimeCardUser::userByUsername( $username );
				}
			} else {
				echo lavu_dberror() . '<br />';
			}

			return null;
		}

		public function fullname(){
			$result = '';
			if( !empty($this->first_name) ){
				$result .= $this->first_name;
			}
			if( !empty($this->last_name) ){
				if($result != '' ){
					$result .= ' ';
				}
				$result .= $this->last_name;
			}
			return $result;
		}

		public function addPunch( $o_punch ){
			$this->timePunches[] = $o_punch;
		}

		public function punches(){
			return $this->timePunches;
		}

		// determine if the user actually exists
		// @return: TRUE if the user exists in the `users` table is `active` and is not `_deleted`, FALSE otherwise
		public function user_exists() {

			return $this->userfound && $this->deleted;
		}

		// loads and processes all time punches between the given dates
		// a convenience function for process_time_punches($a_time_punches)
		// @i_date_1: the start date in UTC time (will load everything from the start of this week)
		// @i_date_2: the end date in UTC time (will load everything to the end of this week)
		// @return: the number of time punches processed, or FALSE if week($i_date_2) < week($i_date_1)
		public function process_dates($i_date_1, $i_date_2) {

		}

		// processes the given time punches to prepare weekly reports
		// @$a_time_punches: the array of time cards to be processed (as TimeCardTimePunch objects)
		// @$b_process_again: if TRUE, will process the user again, ignoring the $this->processed flag
		// @return: the punches used for this user as an array(punch_id1, ...)
		public function process_time_punches( &$a_time_punches, $b_process_again = FALSE, $overtimeSettingDay2) {

			// build of list of punches that apply to this user
			// and check if the user has already been processed
			$a_used_punch_ids = array();
			if ($this->processed && !$b_process_again)
				return array();
			$o_account_settings = TimeCardAccountSettings::settings();

			// echo '<pre style="text-align: left;">' . print_r( $o_account_settings, true ) . '</pre>';
			//We need to push out the start and end pulling times now that we have the punches
			foreach($a_time_punches as $o_punch) {
				if( $o_punch->time_in < $o_account_settings->start_week ) {
					$o_account_settings->set_dates( $o_punch->time_in, $o_account_settings->end_week );
				}

				if($o_punch->time_out < $o_account_settings->start_week ) {
					$o_account_settings->set_dates( $o_punch->time_in, $o_account_settings->end_week );
				}

				if( $o_punch->time_in > $o_account_settings->end_week ) {
					$o_account_settings->set_dates( $o_account_settings->start_week,  $o_punch->time_in );
				}

				if($o_punch->time_out > $o_account_settings->end_week ) {
					$o_account_settings->set_dates( $o_account_settings->start_week, $o_punch->time_out );
				}
			}

			// echo '<pre style="text-align: left;">' . print_r( $o_account_settings, true ) . '</pre>';
			// loop through each punch, finding ones that are relative to this user and processing those
			// add all processed punch ids to an array to return
			foreach($a_time_punches as $o_punch) {

				// check that the punch applies to the user
				if ((int)$o_punch->userid != (int)$this->id)
					continue;

				// get the weeks that this punch applies to
				/*$o_punch->start_week = TimeCardWorkWeek::compute_starttime($o_punch->time_in, $o_account_settings->weekday_start, $o_account_settings->day_start_hour_offset, $o_account_settings->day_start_minute_offset );
				$o_punch->end_week = TimeCardWorkWeek::compute_starttime($o_punch->time_out, $o_account_settings->weekday_start, $o_account_settings->day_start_hour_offset, $o_account_settings->day_start_minute_offset );

				// create the start week, and all subsequent weeks, and apply this time card to those weeks
				for ($i_week = $o_punch->start_week; $i_week <= $o_punch->end_week; $i_week = strtotime(date('Y-m-d H:i:s',$i_week).' +1 week')) {
					if (!isset($this->work_weeks[$i_week])){
						$this->work_weeks[$i_week] = TimeCardWorkWeek::week($i_week);
					}
					// $this->work_weeks[$i_week]->remember_time_punch($o_punch);
				}*/

				for ($i_week = $o_account_settings->start_week; $i_week <= $o_account_settings->end_week; $i_week = strtotime(date('Y-m-d H:i:s',$i_week).' +1 week')) {
					if (!isset($this->work_weeks[$i_week])){
						$this->work_weeks[$i_week] = TimeCardWorkWeek::week($i_week);
						// echo $i_week . ': <pre style="text-align:">' . print_r( $this->work_weeks[$i_week], true ) . '</pre>';
					}
					// $this->work_weeks[$i_week]->remember_time_punch($o_punch);
				}

				// add the punch to the list of punches used
				$a_used_punch_ids[] = $o_punch->id;
			}
			// echo 'TimeCardAccountSettings: <pre style="text-align: left;">' . print_r( $o_account_settings, true ) . '</pre>';
			// echo '<pre style="text-align: left;">' . print_r( $this->work_weeks, true ) . '</pre>';
			// process all weeks
			foreach($this->work_weeks as $i_week=>$o_work_week) {
				$o_work_week->process_time_punches($a_time_punches, $this->id, $overtimeSettingDay2);
			}

			// record that this user has been processed and
			// return the list of punches uses
			// $this->processed = TRUE;
			return $a_used_punch_ids;
		}

		// get the total hours worked by this user, of the weeks processed, of the counted hours
		// @$i_date_1: the start date in UTC time
		// @$i_date_2: the end date in UTC time
		// @$s_time_period: 'total' or 'in_time_period'
		// @return: array('start_week'=>UTC date, 'end_week'=>UTC date, 'hours'=>array('total'=>array(REGULAR=>array(rate1, ...), ...), 'in_time_period'=>array(REGULAR=>array(rate1, ...), ...)), or FALSE if the dates are outside of the processed range
		public function get_seconds( $i_date_1, $i_date_2, $s_time_period) {

			// check the value of $s_time_period
			if (!in_array($s_time_period, array('total', 'in_time_period')))
				return FALSE;
			$o_account_settings = TimeCardAccountSettings::settings();
			// initialize some variables
			$i_date_1 = strtotime(date('Y-m-d',$i_date_1));
			$i_date_2 = strtotime(date('Y-m-d',$i_date_2));
			$i_load_start_time = strtotime(date('Y-m-d H:i:s',$o_account_settings->start_time));
			$i_load_end_time = strtotime(date('Y-m-d H:i:s',$o_account_settings->end_time));
			$a_hours = array();

			// check that all of the applicable dates are processed
			if ($i_date_1 < $i_load_start_time || $i_date_2 > $i_load_end_time)
				return FALSE;

			// compute which weeks these dates apply to
			$i_start_week = TimeCardWorkWeek::compute_starttime($i_date_1, $o_account_settings->weekday_start, $o_account_settings->day_start_hour_offset, $o_account_settings->day_start_minute_offset );
			$i_end_week = TimeCardWorkWeek::compute_starttime($i_date_2, $o_account_settings->weekday_start, $o_account_settings->day_start_hour_offset, $o_account_settings->day_start_minute_offset );
			$i_end_week = strtotime(date('Y-m-d H:i:s', $i_end_week).' +1 week');
			$a_weeks = array($i_start_week, $i_end_week);
			$i_week = strtotime(date('Y-m-d H:i:s',$i_start_week).' +1 week');
			while($i_week < $i_end_week) {
				$a_weeks[] = $i_week;
				$i_week = strtotime(date('Y-m-d H:i:s',$i_week).' +1 week');
			}

			// get all hours for all weeks
			foreach($a_weeks as $i_week) {
				if (!isset($this->work_weeks[$i_week]))
					continue;
				TimeCardTimePunch::sum_hours($a_hours, $this->work_weeks[$i_week]->seconds[$s_time_period]);
			}

			return array('start_week'=>$i_start_week, 'end_week'=>$i_end_week, 'seconds'=>array($s_time_period=>$a_hours));
		}

		// get the hours worked in a week when the entire week shouldn't be accounted for
		// @$i_week: index of the week to be processed (for efficiency)
		// @$i_start_date: first day to be processed in UTC time
		// @$i_start_date: last day to be processed in UTC time
		// @return: the hours from the matching days
		private function get_hours_partial_week($i_week, $i_start_date, $i_end_date) {

		}

		// get the hours worked on the given date
		// @$i_date: the day to get the hours of in UTC
		// @return: array($s_time_period=>array(REGULAR=>array(rate1, ...), ...)), or FALSE if the date is outside of the processed range
		public function get_day_hours( $i_date, $s_time_period) {
			$o_account_settings = TimeCardAccountSettings::settings();
			// initialize some variables
			$i_date = strtotime(date('Y-m-d',$i_date));
			$i_load_start_time = strtotime(date('Y-m-d',$o_account_settings->start_time));
			$i_load_end_time = strtotime(date('Y-m-d',$o_account_settings->end_time));

			// check that all of the applicable dates are processed
			if ($i_date < $i_load_start_time || $i_date > $i_load_end_time)
				return FALSE;

			// get the hours
			$i_week = TimeCardWorkWeek::compute_starttime($i_date, $o_account_settings->weekday_start, $o_account_settings->day_start_hour_offset, $o_account_settings->day_start_minute_offset );
			return $this->work_weeks[$i_week]->get_day_hours($i_date_1, $s_time_period);
		}

		// get the hours worked on the given date at the given hour
		// @$i_datetime: the date and time to get the hours of in UTC
		// @return: array($s_time_period=>array(REGULAR=>array(rate1, ...), ...)), or FALSE if the date is outside of the processed range
		public function get_hour_seconds($i_datetime, $s_time_period) {

			// check that all of the applicable dates are processed
			if ($i_datetime < $o_account_settings->start_time || $i_datetime > $o_account_settings->end_time)
				return FALSE;

			// get the hours
			$i_week = TimeCardWorkWeek::compute_starttime($i_datetime, $o_account_settings->weekday_start, $o_account_settings->day_start_hour_offset, $o_account_settings->day_start_minute_offset );
			return $this->work_weeks[$i_week]->get_hour_seconds($i_datetime, $s_time_period);
		}
	}

	// contains the days worked by week
	class TimeCardWorkWeek {

		// set some defaults
		// @$i_date: should be the day that this work week starts
		// @$o_account_settings: a TimeCardAccountSettings object
		private function __construct($i_date) {
			$o_account_settings = TimeCardAccountSettings::settings();
			// $i_date = TimeCardTimePunch::utc_to_date($i_date);
			$this->week = date('Y-m-d H:i:s', $i_date);
			$this->next_week = strtotime($this->week.' +1 week'); // 60 * 60 * 24 * 7
			$this->week = $i_date;//
			$this->work_days = array(); // array of TimeCardWorkDay objects (indexed by the UTC date)

			for($i_day = $i_date; $i_day < $this->next_week; $i_day = strtotime(date('Y-m-d H:i:s',$i_day).' +1 day')){
				$this->work_days[$i_day] = new TimeCardWorkDay($i_day);
			}
		}

		public static function week( $i_date ){
			static $weeks = array();
			if(!isset($weeks[$i_date])){
				$weeks[$i_date] = new TimeCardWorkWeek( $i_date );
			}
			return $weeks[$i_date];
		}

		// adds a time punch to the list of punches to be processed
		// NOTE: there is no checking to verify that the punch falls within this week
		// @$o_punch: the time card punch object
		// public function remember_time_punch($o_punch) {
		// 	$this->time_punches[$o_punch->id] = $o_punch;
		// }

		// processes time punches to find the total hours of each type worked, at each pay rate
		public function process_time_punches( $a_time_punches, $user_id = -1, $overtimeSettingDay2) {
			// no hours or days have been accounted for, so far
			$i_seconds_processed = 0.0;
			$i_days_processed = 0;

			// process the time punches and increment $i_seconds_processed and $this->seconds
			foreach($this->work_days as $o_work_day) {
				// process the time punches
				$b_day_is_used = FALSE;
				$o_work_day->process_time_punches( $i_seconds_processed, $i_days_processed, $a_time_punches, $user_id, $overtimeSettingDay2);
			}
		}

		// returns the number of seconds worked this week for all time periods, all types, and all pay rates
		// @return: the number of seconds, or FALSE if process_time_punches() has not yet been called
		public function get_total_seconds() {

			// check that time punches have already been processed
			if ($this->processed === FALSE)
				return FALSE;

			// get the total seconds
			$i_seconds = 0;
			foreach($this->seconds as $s_time_period=>$a_hours_by_time_period) {
				if (count($a_hours_by_time_period) == 0)
					continue;
				foreach($a_hours_by_time_period as $i_type=>$a_hours_by_type) {
					if (count($a_hours_by_type) == 0)
						continue;
					foreach($a_hours_by_type as $s_rate=>$i_seconds_by_rate)
						$i_seconds += $i_seconds_by_rate;
				}
			}

			return $i_seconds;
		}

		// determine if this week contains the given date
		// @$i_date: a UTC date
		// @return: TRUE or FALSE
		private function has_date($i_date) {
			if (isset($this->work_days[$i_date]))
				return TRUE;
			return FALSE;
		}

		// get the hours from the given date
		// @$i_date: a UTC date
		// @$s_time_period: 'total' or 'in_time_period'
		// @return: the hours as an array(REGULAR=>array(rate_1=>hours, ...), ...), or NULL if this week does not contain that date, or FALSE if process_time_punches() has not yet been called
		public function get_day_hours($i_date, $s_time_period) {

			// initialize some variables
			$i_date = TimeCardTimePunch::utc_to_date($i_date);

			// check that time punches have already been processed and the date is valid
			if ($this->processed === FALSE)
				return FALSE;
			if (!$this->has_date($i_date))
				return NULL;

			return $this->work_days[$i_date]->seconds[$s_time_period];
		}

		// get the hours from the given date
		// @$i_datetime: a UTC date and time
		// @$s_time_period: 'total' or 'in_time_period'
		// @return: the hours as an array(REGULAR=>array(rate_1=>hours, ...), ...), or NULL if this week does not contain that date, or FALSE if process_time_punches() has not yet been called
		public function get_hour_seconds($i_datetime, $s_time_period) {

			// initialize some variables
			$i_date = TimeCardTimePunch::utc_to_date($i_datetime);

			// check that time punches have already been processed and the date is valid
			if ($this->processed === FALSE)
				return FALSE;
			if (!$this->has_date($i_date))
				return NULL;

			return $this->work_days[$i_date]->get_hour_seconds($i_datetime, $s_time_period);
		}

		// get the number of days worked this week
		// @return: the number of days worked this week, or FALSE if process_time_punches() has not yet been called
		public function get_days() {

			// check that time punches have already been processed
			if ($this->processed === FALSE)
				return FALSE;

			return $this->days;
		}

		// computes the start time of the week containing $i_start_time
		// @$i_start_time: some UTC time within a week
		// @$s_weekday_start: one of 'SUNDAY', 'MONDAY', 'TUESDAY', 'WEDNESDAY', 'THURSDAY', 'FRIDAY', 'SATURDAY'
		// @return: the start time of the week, as a UTC integer
		public static function compute_starttime($i_start_time, $s_weekday_start, $offset_hour = 0, $offset_minute = 0) {
			// $o_account_settings = TimeCardAccountSettings::settings();
			$i_start_date = TimeCardTimePunch::utc_to_date($i_start_time);
			$week_starttime = strtotime(date('Y-m-d',$i_start_date).' previous '.$s_weekday_start);

			$s_hms = str_pad($offset_hour, 2, '0', STR_PAD_LEFT) . ':' . str_pad($offset_minute, 2, '0', STR_PAD_LEFT) . ':' . '00';
			$week_starttime = strtotime(date('Y-m-d',$week_starttime) . ' ' . $s_hms);

			// echo date('Y-m-d H:i:s', $week_starttime) . '<br />';
			// $week_starttime = ($week_starttime == strtotime(date('Y-m-d ',$i_start_date).str_pad($offset_hour, 2, '0', STR_PAD_LEFT) . ':' . str_pad($offset_minute, 2, '0', STR_PAD_LEFT) . ':' . '00'.' -7 days') ? $i_start_date : $week_starttime);
			// $week_starttime += ($offset_hour * 3600) + ($offset_minute * 60);
			return $week_starttime;
		}

		// computes the end time of the week containing $i_end_time
		// @$i_end_time: some UTC time within a week
		// @$s_weekday_start: one of 'SUNDAY', 'MONDAY', 'TUESDAY', 'WEDNESDAY', 'THURSDAY', 'FRIDAY', 'SATURDAY'
		// @return: the end time of the week, as a UTC integer
		public static function compute_endtime($i_end_time, $s_weekday_start, $offset_hours, $offset_minutes) {
			$i_end_date = TimeCardTimePunch::utc_to_date($i_end_time);
			$week_endtime = strtotime(date('Y-m-d',$i_end_date).' next '.$s_weekday_start);

			$week_endtime += ($offset_hours*3600) + ($offset_minutes*60);
			return $week_endtime;
		}
	}

	// contains the hours worked by day
	class TimeCardWorkDay {
		public
			$day,
			$tomorrow,
			$is_ot_holiday,
			$is_dt_holiday,
			$is_leap_day,
			$worked_hours,
			$processed,
			$seconds,
			$a_hours,
			$work_hours;
		// set some defaults
		public function __construct($i_date) {
			$o_account_settings = TimeCardAccountSettings::settings();
			$this->day = $i_date;
			$this->tomorrow = strtotime(date('Y-m-d H:i:s',$this->day).' +1 day');
			$holiday_check = $o_account_settings->is_holiday($i_date);
			$this->is_ot_holiday = $holiday_check === OVERTIME;
			$this->is_dt_holiday = $holiday_check === DOUBLE_TIME;
			$this->is_leap_day = (date('L m-d',$i_date) == '1 02-29');
			$this->work_hours = array();
			// $this->processed = FALSE;
			// $this->seconds = array();
			// $a_hours = array(REGULAR=>array(), OVERTIME=>array(), DOUBLE_TIME=>array()); // each array will be a quantity according to payrate ids, eg worked 4 hours at rate 1 and 5 hours at rate 2 > REGULAR=>array(1=>4, 2=>5)

			// echo 'TimeCardWorkDay: <pre style="text-align: left;">' . print_r( $this, true ) . '</pre>';
			//

			// foreach(array('total', 'in_time_period') as $s_time_period)
			// 	$this->seconds[$s_time_period] = $a_hours;
			for($i_hour = $this->day; $i_hour < $this->tomorrow; $i_hour = strtotime(date('Y-m-d H:i',$i_hour).':00 +1 hour')){
				$this->work_hours[$i_hour] = new TimeCardWorkHour($i_hour);
			}
		}

		// return this day, as a UTC time
		// @return: a UTC timestamp
		public function get_day() {
			return $this->day;
		}

		// processes time punches to produce $this->work_hours and $this->seconds
		// @$i_seconds_week: sum of hours calculated before this hour (for this week)
		// @$i_days_week: number of days worked before this day (for this week)
		// @$a_time_punches: array of TimeCardTimePunch objects (should only be for one user, doesn't necessarilly only need to be this day)
		public function process_time_punches( &$i_seconds_week, &$i_days_week, $a_time_punches, $user_id = -1, $overtimeSettingDay2) {
			$i_seconds_day = 0;
			// process each punch
			foreach($a_time_punches as $o_punch) {
				// count the seconds that this punch occurs for during this day
				$i_seconds = 0;
				if ($o_punch->time_out < $this->day || $o_punch->time_in >= $this->tomorrow) {

					// the time punch doesn't occur on this day
					$i_seconds = 0;
				} else if ($o_punch->time_in >= $this->day && $o_punch->time_in < $this->tomorrow) {

					// the time punch starts on this day
					if ($o_punch->time_out < $this->tomorrow)
						$i_seconds = $o_punch->time_out - $o_punch->time_in;
					else
						$i_seconds = $this->tomorrow - $o_punch->time_in;
				} else if ($o_punch->time_out >= $this->day && $o_punch->time_out < $this->tomorrow) {

					// the time punch ends on this day
					if ($o_punch->time_in >= $this->day)
						$i_seconds = $o_punch->time_out - $o_punch->time_in;
					else
						$i_seconds = $o_punch->time_out - $this->day;
				} else {

					// the time punch envelopes this day
					$i_seconds = $this->tomorrow - $this->day;
				}

				if(!$i_seconds){
					continue;
				}

				// determine the type of punch
				$s_time_period = ($o_punch->in_requested_time_period ? 'in_time_period' : 'total');

				// convert to hours, and fill in the hours objects until there are zero hours left to fill
				//$f_hours = $i_seconds / 3600.0;
				$i_seconds_it = $i_seconds;
				$i_seconds_worked_this_shift = 0;
				foreach($this->work_hours as $o_work_hour) {

					// get the number of seconds/hours worked during this hour
					$i_seconds_worked_this_hour = $o_work_hour->seconds_worked_now($o_punch->time_in, $o_punch->time_out);
					$i_seconds_worked_this_hour = min($i_seconds_worked_this_hour, $i_seconds_it);
					//$f_hours_worked_this_hour = $i_seconds_worked_this_hour / 3600.0;
					// calculate regular/ot/dt
					if ($i_seconds_worked_this_hour > 0){
						$punch_contribution = $o_work_hour->calculate_seconds( $s_time_period, $i_seconds_day + $i_seconds_worked_this_shift, $i_seconds_week + $i_seconds_worked_this_shift, $i_days_week, $i_seconds_worked_this_hour, $o_punch->role_id, $this->is_ot_holiday, $this->is_dt_holiday, $user_id, $overtimeSettingDay2);
						$o_punch->addContribution( $punch_contribution );
					}
					$i_seconds_worked_this_shift += $i_seconds_worked_this_hour;
					$i_seconds_it -= $i_seconds_worked_this_hour;

					// check if the seconds have all been used up
					if ($i_seconds_it == 0){
						break;
					}
				}
				$i_seconds_day += $i_seconds;
				$i_seconds_week += $i_seconds;
			}
			if($i_seconds_day){
				$i_days_week++;
			}
		}

		// get the hours worked on this day
		// includes REGULAR, OVERTIME, and DOUBLE_TIME hours
		// @$s_time_period: 'total' or 'in_time_period'
		// @return: $this->seconds, or FALSE if process_time_punches() has not yet been called
		public function get_seconds($s_time_period) {

			// check that time punches have already been processed
			if ($this->processed === FALSE)
				return FALSE;

			return $this->seconds[$s_time_period];
		}

		// get the hours worked during the given hour
		// @$i_hour: the hour during the day (as a UTC time)
		// @$s_time_period: 'total' or 'in_time_period'
		// @return: the hours array(REGULAR=>array(rate1=>hours, ...), ...), or NULL if that hour doesn't exist, or FALSE if process_time_punches() has not yet been called
		public function get_hour_seconds($i_hour, $s_time_period) {

			// check that time punches have already been processed
			if ($this->processed === FALSE)
				return FALSE;

			// check for that hour
			if (!isset($this->work_hours[$i_hour]))
				return NULL;

			return $this->work_hours[$i_hour]->seconds[$s_time_period];
		}
	}

	// contains the hours worked by hour
	class TimeCardWorkHour {
		public
			$hour,
			$next_hour;
			// $processed,
			// $seconds;
		// set some defaults
		// @$i_hour: the hour that is being constructed (0-23)
		public function __construct($i_hour) {
			$this->hour = $i_hour;
			$this->next_hour = strtotime(date('Y-m-d H:i',$i_hour).':00 +1 hour');
			// $this->processed = FALSE;
			// $this->seconds = array();
			// $a_hours = array(REGULAR=>array(), OVERTIME=>array(), DOUBLE_TIME=>array());

			// foreach(array('total', 'in_time_period') as $s_time_period)
			// 	$this->seconds[$s_time_period] = $a_hours;
		}

		// calculates the number of regular/overtime/doubletime hours worked this hour (0.0 - 1.0)
		// @$s_time_period: 'total' or 'in_time_period'
		// @$i_seconds_day: sum of hours calculated before this hour (for this day)
		// @$i_seconds_week: sum of hours calculated before this hour (for this week)
		// @$i_days_week: number of days worked before this day (for this week)
		// @$i_seconds_now: number of hours worked during this hour
		// @$i_rate_id: rate id of the current time punch
		// @$is_ot_holiday: TRUE or FALSE (if it is a overtime holiday for the day this hour occurs on)
		// @$is_dt_holiday: TRUE or FALSE (if it is a double time holiday for the day this hour occurs on)
		public function calculate_seconds( $s_time_period, $i_seconds_day, $i_seconds_week, $i_days_week, $i_seconds_now, $i_rate_id, $is_ot_holiday, $is_dt_holiday, $user_id = -1, $overtimeSettingDay2 = array()) {
			$contribution = array( REGULAR => array( $i_rate_id => 0 ), OVERTIME => array( $i_rate_id => 0 ), DOUBLE_TIME => array( $i_rate_id => 0 ) );
			$o_account_settings = TimeCardAccountSettings::settings();
			$i_seconds_left = $i_seconds_now;

			// check for double time
			// check for a holiday
			if ($is_dt_holiday) {
				$contribution[DOUBLE_TIME][$i_rate_id] += $i_seconds_left;
				$i_seconds_left = 0;
			}
			// check for daily hours
			if ($i_seconds_day + $i_seconds_left > $o_account_settings->dt_settings->daily_seconds && $i_seconds_left > 0){
				$dt_seconds = min($i_seconds_left, $i_seconds_day + $i_seconds_left - $o_account_settings->dt_settings->daily_seconds);
				$contribution[DOUBLE_TIME][$i_rate_id] += $dt_seconds;
				$i_seconds_left -= $dt_seconds;
			}

			// check for overtime
			// check for a holiday
			if ($is_ot_holiday) {
				$contribution[OVERTIME][$i_rate_id] += $i_seconds_left;
				$i_seconds_left = 0;
			}
			// check for weekly days
			if ($i_days_week >= $o_account_settings->ot_settings->weekly_days) {
				$contribution[OVERTIME][$i_rate_id] += $i_seconds_left;
				$i_seconds_left = 0;
			}
			// check for daily hours
			if ($i_seconds_day + $i_seconds_left > $o_account_settings->ot_settings->daily_seconds && $i_seconds_left > 0){
				$ot_seconds = min($i_seconds_left, $i_seconds_day + $i_seconds_left - $o_account_settings->ot_settings->daily_seconds);
				$contribution[OVERTIME][$i_rate_id] += $ot_seconds;
				$i_seconds_left -= $ot_seconds;
			}

			if (empty($overtimeSettingDay2)) {
				//LP-2481 there is no need to keep this condition as the data filter is done by day later its removed  Start
				global $location_info;
				$locationid          = $location_info['id'];
				$searchForType       = "overtime_setting_day2";
				$otresult            = rpt_query("SELECT * FROM `config` WHERE `location`=\"[1]\" AND type=\"[2]\"",$locationid, $searchForType);
				$overtimeSettingDay2 = mysqli_fetch_assoc($otresult);
			}
			$ot_california_setting = $overtimeSettingDay2['value15'];

			if(!$ot_california_setting ==1){
				// check for weekly hours
				if ($i_seconds_week + $i_seconds_left > $o_account_settings->ot_settings->weekly_seconds && $i_seconds_left > 0) {
					$ot_seconds = min($i_seconds_left, $i_seconds_week + $i_seconds_left - $o_account_settings->ot_settings->weekly_seconds);
					$contribution[OVERTIME][$i_rate_id] += $ot_seconds;
					$i_seconds_left -= $ot_seconds;
				}

			}//LP-2481 there is no need to keep this condition as the data filter is done by day later its removed   End


			// add the rest to regular hours
			if ($i_seconds_left > 0){
				$contribution[REGULAR][$i_rate_id] += $i_seconds_left;
			}

			return $contribution;
		}

		// calculates the number of seconds worked during this hour, based on the start and end times
		// @$i_start_time: the start time of the shift
		// @$i_end_time: the end time of the shift
		public function seconds_worked_now($i_start_time, $i_end_time) {
			// figure out how the shift overlaps with this hours
			if ($i_start_time >= $this->next_hour || $i_end_time < $this->hour) {

				// the shift does not occur during this hour
				return 0;
			} else if ($i_start_time >= $this->hour && $i_start_time < $this->next_hour) {

				// the shift starts during this hour
				if ($i_end_time < $this->next_hour)
					return $i_end_time - $i_start_time;
				else
					return $this->next_hour - $i_start_time;
			} else if ($i_end_time >= $this->hour && $i_end_time < $this->next_hour) {

				// the shift ends during this hour
				if ($i_start_time >= $this->hour)
					return $i_end_time - $i_start_time;
				else
					return $i_end_time - $this->start_time;
			} else {

				// the shift envelopes this hour
				return $this->next_hour - $this->hour;
			}
		}

		// get this hour's hour (0-23)
		public function get_hour() {
			return $this->hour;
		}
	}

	// all of the views used in the time cards report
	class TimeCardViews {

		/********************************************************************************
		 * R E P O R T   V I E W S
		 ********************************************************************************/

		public static function draw_choose_users() {
			echo self::header_tostr();
			echo "<br /><br /><table><tr><td>Detailed Timecards</td></tr><tr><td style='background-color:gray; height:2px;'></td></tr>";
			echo str_replace(array('--start_table--', '--end_table--'), array('', '</table>'), self::userlist_tostr());
		}

		public static function draw_report() {
			global $user_id;
			global $location_info;
			global $time_cards_results;
			$current_tz = date_default_timezone_get();
			if( !empty( $location_info['timezone'])){
				date_default_timezone_set( $location_info['timezone'] );
			}
			$o_time_cards_chooser_obj = TimeCardUserFunctions::get_date_chooser_obj();
			//$i_date1 = strtotime(isset($_GET['setdate'])?$_GET['setdate']:date('Y-m-d'));
			if(isset($_GET['setdate'])){ $i_date1=$_GET['setdate']; }
			elseif(isset($_SESSION['rselector_setdate'])){ $i_date1=$_SESSION['rselector_setdate']; }
			else{ $i_date1=date('Y-m-d'); }
			$i_date1 = strtotime($i_date1);
			$i_date2 = $i_date1;

			//$day_duration = isset($_GET['day_duration'])?$_GET['day_duration']:1;
			if(isset($_GET['day_duration'])){ $day_duration=$_GET['day_duration']; }
			elseif(isset($_SESSION['rselector_day_duration'])){ $day_duration=$_SESSION['rselector_day_duration']; }
			else{ $day_duration=1; }

			$i_end_date = strtotime('+'.max( 1, $day_duration ).' days',$i_date2);
			$time_cards_results = TimeCardUserFunctions::load_all_users($i_date1, $i_end_date);

			date_default_timezone_set( $current_tz );
			$table_headers = array(
				'ID',
				'Server',
				'Class',
				'Pay Rate',
				'Time In',
				'Time Out',
				'Total Hrs',
				'Reg Hrs',
				'OT Hrs',
				'DT Hrs',
				'Total Paid',
				'Reg Paid',
				'OT Paid',
				'DT Paid'
			);
			$data = array();
			foreach( $time_cards_results['users'] as $user_id => $user ){
				//id
				//work_weeks
				//processed
				//userfound
				//username
				//full_name
				//access
				//locationid
				//lavu_admin
				//deleted
				//classes
				//payrates
				//clocked_in
				//timePunches

				// echo '<pre style="text-align: left;">' . print_r( $user, true ) . '</pre>';
				$payrates = $user->payrates;
				$classes = $user->classes;
				$full_name = $user->full_name;
				$user_id = $user->id;
				$punches = array();
				//foreach( $user->work_weeks as $work_week_start_ts => $work_week ){
					//week
					//next_week
					//work_days
					//seconds
					//days
					//processed
					//time_punches
					foreach($user->timePunches as $punch_id => $punch ){
						//id
						//locationid
						//userid
						//time_in
						//time_out
						//clocked_out
						//clocked_out_via_app
						//delete
						//rate_id
						//in_requested_time_period
						//role_id
						//start_week
						//end_week
						//contribution
						$punches[$punch->id] = $punch;
					}
				//}

				foreach( $punches as $punch_id => $punch ){

					$report_start_time = TimeCardAccountSettings::settings()->start_time;
					$report_end_time = TimeCardAccountSettings::settings()->end_time;

					if( !(($punch->time_out >= $report_start_time && $punch->time_out <= $report_end_time) ||
						($punch->time_in >= $report_start_time && $punch->time_in <= $report_end_time) ||
						($punch->time_in <= $report_start_time && $punch->time_out >= $report_end_time)) ||
						!$punch->in_requested_time_period ){
						continue;
					}
					$view_payrates= $punch->payrate;
					$role =  UserRole::getUserRole( $punch->role_id );
					$row_result = array(
						$user_id,
						$full_name,
						$role,
						$view_payrates,
						$punch->time_in,//'m/d h:i A'
						($punch->clocked_out?'':'e') . $punch->time_out
					);

					$regular_seconds = 0;
					$regular_rate = 0;
					$overtime_rate = 0;
					$doubletime_rate = 0;

					foreach( $punch->contribution[REGULAR] as $role_id => $seconds ){
						$regular_seconds += $seconds;
						if( $punch->payrate ){
							$regular_rate = $punch->payrate / 3600.0;
						} else if( $role_id || $role_id == 0 ){
							$regular_rate = $payrates->get_rate_rate( $role_id ) / 3600.0;
						}
					}

					$overtime_seconds = 0;
					foreach( $punch->contribution[OVERTIME] as $role_id => $seconds ){
						$overtime_seconds += $seconds;
						if( $punch->payrate ){
							$overtime_rate = $punch->payrate / 3600.0;
						} else if( $role_id || $role_id == 0  ){
							$overtime_rate = $payrates->get_rate_rate( $role_id ) / 3600.0;
						}
					}

					$double_time_seconds = 0;
					foreach( $punch->contribution[DOUBLE_TIME] as $role_id => $seconds ){
						$double_time_seconds += $seconds;
						if( $punch->payrate ){
							$doubletime_rate = $punch->payrate / 3600.0;
						} else if( $role_id || $role_id == 0  ){
							$doubletime_rate = $payrates->get_rate_rate( $role_id ) / 3600.0;
						}
					}

					$total_seconds = $regular_seconds + $overtime_seconds + $double_time_seconds;

					$regular_paid = $regular_seconds * $regular_rate * 1.0;
					$overtime_paid = $overtime_seconds * $overtime_rate * 1.5;
					$double_time_paid = $double_time_seconds * $doubletime_rate * 2.0;
					$total_paid = $regular_paid + $overtime_paid + $double_time_paid;

					$row_result[] = $total_seconds;
					$row_result[] = $regular_seconds;
					$row_result[] = $overtime_seconds;
					$row_result[] = $double_time_seconds;

					$row_result[] = $total_paid;
					$row_result[] = $regular_paid;
					$row_result[] = $overtime_paid;
					$row_result[] = $double_time_paid;
					$row_result[] = $punch;
					$data[] = $row_result;
				}
			}

			usort( $data , array( 'TimeCardViews', 'reportOutputSortFunction' ) );

			if($_GET['ordermode'] == "desc"){
				$data = array_reverse($data);
			}
			$f = null;
			if(!isset($_GET['export'])){
				$f = new HTMLFormatter();
			} else {
				switch( $_GET['export'] ){
					case 'txt' : {
						$f = new TXTFormatter();
						break;
					} case 'csv' : {
						$f = new CSVFormatter();
						break;
					} default : {
						$f = new HTMLFormatter();
					}
				}
			}
			echo $f->toOutput( $data, $table_headers );
		}

		public static function reportOutputSortFunction( $entry1, $entry2 ){
			$sort_order = SortOrder::sortOrder()->order();
			$index0 = $sort_order[0];
			$index1 = $sort_order[1];
			$index2 = $sort_order[2];
			if( TimeCardViews::getEntryValue( $entry1[$index0] ) < TimeCardViews::getEntryValue( $entry2[$index0] ) ){
				return -1;
			}
			if( TimeCardViews::getEntryValue( $entry1[$index0] ) > TimeCardViews::getEntryValue( $entry2[$index0] ) ){
				return 1;
			}

			if( TimeCardViews::getEntryValue( $entry1[$index1] ) < TimeCardViews::getEntryValue( $entry2[$index1] ) ){
				return -1;
			}
			if( TimeCardViews::getEntryValue( $entry1[$index1] ) > TimeCardViews::getEntryValue( $entry2[$index1] ) ){
				return 1;
			}

			if( TimeCardViews::getEntryValue( $entry1[$index2] ) < TimeCardViews::getEntryValue( $entry2[$index2] ) ){
				return -1;
			}
			if( TimeCardViews::getEntryValue( $entry1[$index2] ) > TimeCardViews::getEntryValue( $entry2[$index2] ) ){
				return 1;
			}
			return 0;
		}

		public static function getEntryValue( $entry ){
			if( $entry instanceof UserRole ){
				return $entry->title;
			}
			return $entry;
		}

		/********************************************************************************
		 * V I E W   P A R T S
		 ********************************************************************************/

		public static function header_tostr() {
			global $location_info;

			$date_setting = "date_format";
			$location_date_format_query = rpt_query( "SELECT `location`, `setting`, `value` FROM `config` WHERE `setting` = '[1]'",  $date_setting);
			if( !$location_date_format_query || !mysqli_num_rows($location_date_format_query)){
				$date_format = array("value" => 2);
			}else
				$date_format = mysqli_fetch_assoc( $location_date_format_query );
			$location_date_format = "";
			switch ($date_format['value']){
				case 1: $location_date_format = "d-m-Y H:i:s";
				$location_date_format_without_time = "d/m/Y";
				$location_date_letter_format = "j-n-Y";
				break;
				case 2: $location_date_format = "m-d-Y H:i:s";
				$location_date_format_without_time = "m/d/Y";
				$location_date_letter_format = "n-j-Y";
				break;
				case 3: $location_date_format = "Y-m-d H:i:s";
				$location_date_format_without_time = "Y/m/d";
				$location_date_letter_format = "Y-n-j";
				break;
			}

			// get the mode and stuff
			$o_time_cards_chooser_obj = TimeCardUserFunctions::get_date_chooser_obj();
			$s_userstr = (isset($_GET['user']) ? '&user='.$_GET['user'] : '');
			$s_date_selector = str_replace('--extra_vars--', $s_userstr, $o_time_cards_chooser_obj->select_clause_with_extra_vars);

			$a_location = self::get_location_info();

			$location_display = $a_location['title'];
			$date_range_str = '';

			$current_tz = date_default_timezone_get();
			if( !empty($location_info['timezone']) ){
				date_default_timezone_set( $location_info['timezone'] );
			}

			if($s_userstr){
				$o_account_settings = TimeCardAccountSettings::settings();
				//$date_range_str = '<b>'.date( 'm/d/Y', $o_account_settings->start_time ) . ' - ' . date( 'm/d/Y', strtotime(date('Y-m-d H:i:s', $o_account_settings->end_time ).' -1 day')) .'</b>';
				$date_range_str = '<b>'.date( $location_date_format_without_time, $o_account_settings->start_time ) . ' - ' .date( $location_date_format_without_time,strtotime('-1 day',$o_account_settings->end_time) ) .'</b>';
			}
			date_default_timezone_set( $current_tz );

			// $note = lavu_note::build_note("Attention","If users are experiencing issues with the clock-in functionality in the app (for example, the app is requiring the user to clock-out, sometimes repeatedly) then the user will need to continue to clock-out until the allowed to clock-in. This is closing any and all open clock-in punches in the system for the user.");
			return "<a href='index.php?".$o_time_cards_chooser_obj->forward_vars."' style='color:black;'>Time Cards:</a> <font style='font-weight:bold;'>".$a_location['title']."</font><br />{$date_range_str}<br /><br />{$s_date_selector}<br />";
		}

		public static function userlist_tostr() {
			// set some initial variables
			$a_users = array_merge(array(array('l_name'=>'All Users', 'f_name'=>'', 'username'=>'all')), TimeCardUserFunctions::get_eligable_users());
			$o_time_cards_chooser_obj = TimeCardUserFunctions::get_date_chooser_obj();
			$s_link_start = "<tr class='userlist'><td><a href='index.php?".$o_time_cards_chooser_obj->forward_vars."&user=";
			$s_link_mid = "' class='location_link_lg'>";
			$s_link_end = "</a></td></tr>";

			// put the users into the retval
			$s_retval = "--start_table--";
			foreach($a_users as $a_user)

				//
				$s_retval .= $s_link_start.str_ireplace("'", '%27', $a_user['username']).$s_link_mid.$a_user['f_name'].' '.$a_user['l_name'].$s_link_end;
			$s_retval .= "--end_table--";

			// return
			$s_retval = str_replace(array("\n","\r","\n\r"), "\n", $s_retval);
			return $s_retval;
		}

		public static function get_location_info($locationid = 1) {
			global $location_info;
			if (!isset($location_info)) {
				$a_locations = ConnectionHub::getConn('rest')->getDBAPI()->getAllInTable('locations', array('id'=>$locationid), FALSE);
				if (count($a_locations) > 0)
					$location_info = mysqli_fetch_assoc($location_query);
			}
			return $location_info;
		}
	}

	interface ReportFormatter {
		public function toOutput( $data, $header );
	}

	class HTMLFormatter implements ReportFormatter {

		private function wrap_item_with_table( $item ){
			return $this->wrap_item( $item, 'table', 'cellspacing="0" cellpadding="2" style="text-align: center; width: 100%;"' );
		}

		private function wrap_item_with_thead( $item ){
			return $this->wrap_item( $item, 'thead', '' );
		}

		private function wrap_item_with_tbody( $item ){
			return $this->wrap_item( $item, 'tbody', '' );
		}

		private function wrap_item_with_tfoot( $item ){
			return $this->wrap_item( $item, 'tfoot', '' );
		}

		private function wrap_item_with_td_right( $item ){
			return $this->wrap_item( $item, 'td', 'class="alignRight"' );
		}

		private function wrap_item_with_td_left( $item ){
			return $this->wrap_item( $item, 'td', 'class="alignLeft"' );
		}

		private function wrap_item_with_th_bottom( $item ){
			return $this->wrap_item( $item, 'th', 'class="tableHeader"' );
		}

		private function wrap_item_with_td( $item ){
			return $this->wrap_item( $item, 'td', '' );
		}

		private function wrap_item_with_tr( $item ){
			return $this->wrap_item( $item, 'tr', '' );
		}

		private function wrap_item_with_tr_user( $item ){
			return $this->wrap_item( $item, 'tr', 'class="usertotal"' );
		}

		private function wrap_item_with_tr_selectable( $item ){
			$attributes = <<<HTML
class="selectable" onclick="show_info('<iframe src=&quot;resources/form_dialog.php?mode=timecard&editpunch=[id]&quot; width=&quot;500&quot; height=&quot;425&quot; frameborder=&quot;0&quot; allowtransparency=&quot;true&quot;></iframe>')"
HTML;
			return $this->wrap_item( $item, 'tr', $attributes );
		}

		private function wrap_item_with_tr_foot( $item ){
			return $this->wrap_item( $item, 'tr', '' );
		}

		private function wrap_item_with_td_foot_right( $item ){
			return $this->wrap_item( $item, 'td', 'class="tableFooter alignRight"' );
		}

		private function wrap_item_with_td_foot_left( $item ){
			return $this->wrap_item( $item, 'td', 'class="tableFooter alignLeft"' );
		}

		private function wrap_item_with_td_foot( $item ){
			return $this->wrap_item( $item, 'td', 'class="tableFooter"' );
		}

		private function wrap_item_with_b( $item ){
			return $this->wrap_item( $item, 'b', '' );
		}

		private function wrap_item( $item, $tagType, $attributes ){
			return "<{$tagType} {$attributes}>$item</{$tagType}>\n";
		}

		private function outputHeaderRow( $row ){

			global $modules;

			//echo "the modules are: ".print_r($modules,1);

			$o_time_cards_chooser_obj = TimeCardUserFunctions::get_date_chooser_obj();
			$user = $_GET['user'];
			$result = '';
			$result .= $this->wrap_item_with_th_bottom( $row[0] );
			if (isset($_GET['ordermode']) && $_GET['ordermode'] == 'asc' && $_GET['order'] == 0) {
				$result .= $this->wrap_item_with_th_bottom( "<span style=\" cursor: s-resize;\" onclick=\"window.location = 'index.php?{$o_time_cards_chooser_obj->forward_vars}&user={$user}&order=0&ordermode=desc&order_by_col=$row[1]'\">".$row[1].'</span>' );
			} else {
				$result .= $this->wrap_item_with_th_bottom( "<span style=\" cursor: s-resize;\" onclick=\"window.location = 'index.php?{$o_time_cards_chooser_obj->forward_vars}&user={$user}&order=0&ordermode=asc&order_by_col=$row[1]'\">".$row[1].'</span>' );
			}
			if (isset($_GET['ordermode']) && $_GET['ordermode'] == 'asc' && $_GET['order'] == 1) {
				$result .= $this->wrap_item_with_th_bottom( "<span style=\" cursor: s-resize;\" onclick=\"window.location = 'index.php?{$o_time_cards_chooser_obj->forward_vars}&user={$user}&order=1&ordermode=desc&order_by_col=$row[2]'\">".$row[2].'</span>' );
			} else {
				$result .= $this->wrap_item_with_th_bottom( "<span style=\" cursor: s-resize;\" onclick=\"window.location = 'index.php?{$o_time_cards_chooser_obj->forward_vars}&user={$user}&order=1&ordermode=asc&order_by_col=$row[2]'\">".$row[2].'</span>' );
			}
			$result .= $this->wrap_item_with_th_bottom( $row[3] );
			$result .= $this->wrap_item_with_th_bottom( $row[4] );
			$result .= $this->wrap_item_with_th_bottom( $row[5] );
			$result .= $this->wrap_item_with_th_bottom( $row[6] );
			$result .= $this->wrap_item_with_th_bottom( $row[7] );
			$result .= $this->wrap_item_with_th_bottom( $row[8] );
			$result .= $this->wrap_item_with_th_bottom( $row[9] );



			if( $modules->hasModule( 'employees.payrates' ) ){
				$result .= $this->wrap_item_with_th_bottom( $row[10] );
				$result .= $this->wrap_item_with_th_bottom( $row[11] );
				$result .= $this->wrap_item_with_th_bottom( $row[12] );
				$result .= $this->wrap_item_with_th_bottom( $row[13] );
			}
			// for( $i = 0; $i < count( $row ); $i++ ){
			// 	$result .= $this->wrap_item_with_th_bottom( $row[$i] );
			// }
			$result = $this->wrap_item_with_tr( $result );
			$result = $this->wrap_item_with_thead( $result );
			return $result;
		}

		private function formatTime( $value ){
			$result = '';
			if( isset($_GET['format']) && 'decimalized' == $_GET['format'] ){
				$result = format_currency( $value/3600, true );
			} else {
				$hours = floor( $value/3600 );
				$mins = ($value / 60) - ($hours * 60);
				$result = $hours . ':' .  str_pad($mins, 2, '0', STR_PAD_LEFT);
			}
			return $result;
		}

		private function outputDataRow( $row, $punch = null ){
			$result = '';
			$result .= $this->outputMainReportRow( $row, '' );
			if($punch !== null ){
				$result = $this->wrap_item_with_tr_selectable( $result );
				$result = str_ireplace("[id]", $punch->id, $result);
			} else {
				$result = $this->wrap_item_with_tr( $result );
			}
			return $result;
		}

		private function outputMainReportRow( $row, $stylesStr ){
			global $modules;

			$result = '';

			$result .= $this->wrap_item( $row[0], 'td', "class=\"{$stylesStr}\"");
			$result .= $this->wrap_item( $row[1], 'td', "class=\"{$stylesStr} alignLeft\"");
			$result .= $this->wrap_item( $row[2], 'td', "class=\"{$stylesStr} alignLeft\"");
			$result .= $this->wrap_item( $row[3], 'td', "class=\"{$stylesStr}\"");
		//	$result .= $this->wrap_item( $this->formatTime( $row[3] ), 'td', "class=\"{$stylesStr} alignRight\"");
			$result .= $this->wrap_item( $row[4], 'td', "class=\"{$stylesStr}\"");
			$result .= $this->wrap_item( $row[5], 'td', "class=\"{$stylesStr}\"");
			//$result .= $this->wrap_item( $this->formatTime( $row[5] ), 'td', "class=\"{$stylesStr} alignRight\"");
			if($row[1] != 'Total'){
				$result .= $this->wrap_item( $this->formatTime( $row[6] ), 'td', "class=\"{$stylesStr} alignRight\"");
			}else{
				if(isset($_GET['format']) && 'decimalized' == $_GET['format']){
					$result .= $this->wrap_item( $row[6],  'td', "class=\"{$stylesStr} alignRight\"");
				}else{
					$result .= $this->wrap_item( $this->formatTime( $row[6] ), 'td', "class=\"{$stylesStr} alignRight\"");
				}
			}
			//$result .= $this->wrap_item( $this->formatTime( $row[6] ), 'td', "class=\"{$stylesStr} alignRight\"");
			$result .= $this->wrap_item( $this->formatTime( $row[7] ), 'td', "class=\"{$stylesStr} alignRight\"");
			$result .= $this->wrap_item( $this->formatTime( $row[8] ), 'td', "class=\"{$stylesStr} alignRight\"");
			$result .= $this->wrap_item( $this->formatTime( $row[9] ), 'td', "class=\"{$stylesStr} alignRight\"");
			if( $modules->hasModule( 'employees.payrates' ) ){
				$result .= $this->wrap_item( $row[10], 'td', "class=\"{$stylesStr} alignRight\"");
				$result .= $this->wrap_item( $row[11], 'td', "class=\"{$stylesStr} alignRight\"");
				$result .= $this->wrap_item( $row[12], 'td', "class=\"{$stylesStr} alignRight\"");
				$result .= $this->wrap_item( $row[13], 'td', "class=\"{$stylesStr} alignRight\"");
			}

			return $result;
		}

		private function outputDataRowRoleTotal( $row ){
			$result = '';
			$result .= $this->outputMainReportRow( $row, 'tableFooter' );
			$result = $this->wrap_item_with_tr( $result );
			return $result;
		}

		private function outputDataRowTotal( $row ){
			$result = '';
			$result .= $this->outputMainReportRow( $row, 'tableFooter' );
			$result = $this->wrap_item_with_tr_user( $result );
			return $result;
		}

		private function outputFooterRow( $row ){
			$result = '';
			$result .= $this->outputMainReportRow( $row, 'tableFooter' );
			$result = $this->wrap_item_with_tr_foot( $result );
			$result = $this->wrap_item_with_tfoot( $result );
			return $result;
		}

		private function outputRowSpacer(){
			return '<tr><td>&nbsp</td></tr>';
		}


		public function toOutput( $data, $header ){
			$window = create_info_layer( 600, 480 );
			$styles = <<<HTML
<style type="text/css">

	td {
		position: relative;
	}
	.tableFooter {
		border-top: 2px solid gray;
	}

	.tableHeader {
		border-bottom: 2px solid gray
	}

	.alignLeft {
		text-align: left;
	}

	.alignRight {
		text-align: right;
	}

	tfoot tr {
		background: #777;
		color: white;
	}

	tr.usertotal {
		background: #ccc;
	}

	tr.selectable {
		cursor: pointer;
	}

	tr.selectable:hover {
		background: #aabbee;
	}

	.hoverPopUp {
		position: absolute;
		top: 100%;
		left: 80%;
		background: white;
		display: none;
		border: 1px solid #ccc;
		width: 150%;
		padding: 5px;
		text-align: center;
		z-index: 1;
		color: black;
	}

	td:hover > .hoverPopUp {
		display: table;
	}

	td.break.alignRight, td.break.alignLeft  {
		color: white;
	}

	tr:not(.selectable) > td:nth-child(4), tr:not(.selectable) > td:nth-child(5) {
		color: green;
	}

	tfoot tr:not(.selectable) > td.tableFooter:nth-child(4), tfoot tr:not(.selectable) > td.tableFooter:nth-child(5) {
		color: white;
	}

	tr.tableFooter:not(.selectable) > td:nth-child(4) {
		text-align: right;
	}

	tr.tableFooter:not(.selectable) > td:nth-child(5) {
		text-align: left;
	}
</style>
HTML;
			$script = '<script type="text/javascript" src="resources/info_window.js"></script>';
			$table_head = '';
			$table_body = '';
			$table_foot = '';

			$table_head .= $this->outputHeaderRow( $header );

			$role_totals = array( 'id', 'name', 'role', '', '', 0, 0, 0, 0, 0, 0, 0, 0 ); //5 - 12
			$user_totals = array( 'id', 'name', 'role', '', '', 0, 0, 0, 0, 0, 0, 0, 0 );
			$all_users_totals = array( 'id', 'name', 'role', 'start', 'stop', 0, 0, 0, 0, 0, 0, 0, 0 );
			$all_role_totals = array();
			global $user_id;
			global $location_info;
			$current_tz = date_default_timezone_get();
			if( !empty($location_info['timezone']) ){
				date_default_timezone_set( $location_info['timezone'] );
			}

			$sort_order = SortOrder::sortOrder()->order();
			$index0 = $sort_order[0];
			$index1 = $sort_order[1];
			$index2 = $sort_order[2];

			$date_setting = "date_format";
			$location_date_format_query = rpt_query( "SELECT `location`, `setting`, `value` FROM `config` WHERE `setting` = '[1]'",  $date_setting);
			if( !$location_date_format_query || !mysqli_num_rows($location_date_format_query)){
				$date_format = array("value" => 2);
			}else
				$date_format = mysqli_fetch_assoc( $location_date_format_query );
			for( $i = 0; $i < count( $data ); $i++ ) {
				$output_data = array();
				$output_data[0] = $data[$i][0];
				$output_data[1] = $data[$i][1];
				$output_data[2] = $data[$i][2]->title;
				$output_data[3] = format_currency( $data[$i][3] );
				//$output_data[4] = date('m/d h:iA',$data[$i][4]);
				 switch ($date_format['value']){
					case 1: $output_data[4] = date('d/m h:iA',$data[$i][4]);
					$display_date = date('d/m h:iA',$data[$i][5]);
					break;
					case 2: $output_data[4] = date('m/d h:iA',$data[$i][4]);
					$display_date = date('m/d h:iA',$data[$i][5]);
					break;
					case 3: $output_data[4] = date('m/d h:iA',$data[$i][4]);
					$display_date = date('m/d h:iA',$data[$i][5]);
					break;
				}
				if( is_string( $data[$i][5] ) && $data[$i][5][0] === 'e' ){
					$data[$i][5] = substr($data[$i][5], 1 ) * 1;
					//$output_data[4] = '<i style="color: #aaa">now: '.date('m/d h:iA',$data[$i][4]).'</i>';
					$output_data[5] = '<i style="color: #aaa">now: '.$display_date.'</i>';
				} else {
					$output_data[5] = $display_date;
					//$output_data[4] = date('m/d h:iA',$data[$i][4]);
				}
				//$output_data[5] = $data[$i][5];
				$output_data[6] = $data[$i][6];
				$output_data[7] = $data[$i][7];
				$output_data[8] = $data[$i][8];
				$output_data[9] = $data[$i][9];
				$output_data[10] = format_currency( $data[$i][10] );
				$output_data[11] = format_currency( $data[$i][11] );
				$output_data[12] = format_currency( $data[$i][12] );
				$output_data[13] = format_currency( $data[$i][13] );
				if(!isset( $all_role_totals[$output_data[2]]) ){
					$all_role_totals[$output_data[2]] = array( $output_data[2], 0, 0, 0, 0, 0, 0, 0, 0, 0, '' );
				}
				$punch = $data[$i][14];
				if( $punch->is_break_out && $punch->time_to_next_punch ){
					$role_totals[5] += $punch->time_to_next_punch;
					$user_totals[5] += $punch->time_to_next_punch;
					$all_users_totals[5] += $punch->time_to_next_punch;
					$all_role_totals[5] += $punch->time_to_next_punch;
				}

				for( $j = 6; $j <= 13; $j++ ){
					$role_totals[$j] += $data[$i][$j];
					$user_totals[$j] += $data[$i][$j];
					$all_users_totals[$j] += $data[$i][$j];
					$all_role_totals[$output_data[2]][$j-5] += $data[$i][$j];
				}
				if( !(isset( $_GET['totalsonly'] ) && $_GET['totalsonly'] == '1') ){
					$table_body .= $this->outputDataRow( $output_data, $punch );
					if( $punch->is_break_out && $punch->time_to_next_punch ){
						$break_details = array_fill(0, 13, '');
						$break_details[4] = 'Break: ';
						$break_details[5] = $this->formatTime( $punch->time_to_next_punch );
						$table_body .= $this->outputMainReportRow( $break_details, 'break' );
					}
				}

				if( $i == count( $data ) - 1 || TimeCardViews::getEntryValue($data[$i][$index1]) != TimeCardViews::getEntryValue($data[$i+1][$index1])  ||TimeCardViews::getEntryValue( $data[$i][$index0]) != TimeCardViews::getEntryValue($data[$i+1][$index0]) ){
					//Output Role Totals
					$role_totals[0] = '';//$data[$i][0];
					$role_totals[1] = '';//$data[$i][1];
					$role_totals[2] = '';//$data[$i][2]->title;
					$role_totals[3] = '';
					if( !empty($role_totals[5]) ){
						$role_totals[4] = 'Break: ';
						$role_totals[5] = $this->formatTime( $role_totals[5] );
					} else {
						$role_totals[4] = '';
						$role_totals[5] = '';
					}
					//$role_totals[5] = $role_totals[5];
					$role_totals[6] = $role_totals[6];
					$role_totals[7] = $role_totals[7];
					$role_totals[8] = $role_totals[8];
					$role_totals[9] = $role_totals[9];
					$role_totals[10] = format_currency( $role_totals[10] );
					$role_totals[11] = format_currency( $role_totals[11] );
					$role_totals[12] = format_currency( $role_totals[12] );
					$role_totals[13] = format_currency( $role_totals[13] );
					$role_totals[$index1] = TimeCardViews::getEntryValue( $data[$i][$index1] );

					if( !(isset( $_GET['totalsonly'] ) && $_GET['totalsonly'] == '1') ){
						$table_body .= $this->outputDataRowRoleTotal( $role_totals );
						$table_body .= $this->outputRowSpacer();
					}
					$role_totals = array( 'id', 'name', 'role', '', '', 0, 0, 0, 0, 0, 0, 0, 0 ,0);
				}

				if( $i == count( $data ) - 1 || TimeCardViews::getEntryValue($data[$i][$index0]) != TimeCardViews::getEntryValue($data[$i+1][$index0]) ){
					//Output UserTotals
					$user_totals[0] = '';//$data[$i][0];
					$user_totals[1] = '';//$data[$i][1];
					$user_totals[2] = '';
					$user_totals[3] = '';
					if( !empty($user_totals[5]) ){
						$user_totals[4] = 'Break: ';
						$user_totals[5] = $this->formatTime( $user_totals[5] );
					} else {
						$user_totals[4] = '';
						$user_totals[5] = '';
					}
					//$user_totals[5] = $user_totals[5];
					$user_totals[6] = $user_totals[6];
					$user_totals[7] = $user_totals[7];
					$user_totals[8] = $user_totals[8];
					$user_totals[9] = $user_totals[9];
					$user_totals[10] = format_currency( $user_totals[10] );
					$user_totals[11] = format_currency( $user_totals[11] );
					$user_totals[12] = format_currency( $user_totals[12] );
					$user_totals[13] = format_currency( $user_totals[13] );
					$user_totals[$index0] = TimeCardViews::getEntryValue( $data[$i][$index0] );
					$table_body .= $this->outputDataRowTotal( $user_totals );
					$table_body .= $this->outputRowSpacer();
					$user_totals = array( 'id', 'name', 'role', '', '', 0, 0, 0, 0, 0, 0, 0, 0 ,0);
				}

				if( $i == count( $data ) - 1 ){
					//Output All User Totals
					$all_users_totals[0] = '';
					$all_users_totals[1] = 'Total';
					$all_users_totals[2] = '';
					$all_users_totals[3] = '';
					if( !empty($all_users_totals[5]) ){
						$all_users_totals[4] = 'Break: ';
						$all_users_totals[5] = $this->formatTime( $all_users_totals[5] );
					} else {
						$all_users_totals[4] = '';
						$all_users_totals[5] = '';
					}
					//$all_users_totals[5] = $all_users_totals[5];
					if(isset($_GET['format']) && 'decimalized' == $_GET['format']){
						$all_users_totals[6] = round($all_users_totals[6]/3600,2);
					}else{
						$all_users_totals[6] = $all_users_totals[6];
					}
					//$all_users_totals[6] = $all_users_totals[6];
					$all_users_totals[7] = $all_users_totals[7];
					$all_users_totals[8] = $all_users_totals[8];
					$all_users_totals[9] = $all_users_totals[9];
					$all_users_totals[10] = format_currency( $all_users_totals[10] );
					$all_users_totals[11] = format_currency( $all_users_totals[11] );
					$all_users_totals[12] = format_currency( $all_users_totals[12] );
					$all_users_totals[13] = format_currency( $all_users_totals[13] );
					$table_foot = $this->outputFooterRow( $all_users_totals );
					$all_users_totals = array( 'id', 'name', 'role', '', '', 0, 0, 0, 0, 0, 0, 0, 0 ,0);
				}
			}

			date_default_timezone_set( $current_tz );

			$role_breakdown_rows = '';
			$role_breakdown_head = '';
			$role_breakdown_head .= $this->outputHeaderRow( array( '', $header[2], '','', '', '', $header[6], $header[7], $header[8], $header[9], $header[10], $header[11] ,$header[12], $header[13]));
			$all_role_total_row_report = array();
			foreach( $all_role_totals as $key => $all_role_total_row ){
				$all_role_total_row_report[0] = '';
				$all_role_total_row_report[1] = $all_role_total_row[0];
				$all_role_total_row_report[2] = '';
				$all_role_total_row_report[3] = '';
				if( !empty($all_role_total_row_report[5]) ){
						$all_role_total_row_report[4] = 'Break: ';
						$all_role_total_row_report[5] = $this->formatTime( $all_role_total_row_report[5] );
					} else {
						$all_role_total_row_report[4] = '';
						$all_role_total_row_report[5] = '';
					}
				$all_role_total_row_report[6] = $all_role_total_row[1];
				$all_role_total_row_report[7] = $all_role_total_row[2];
				$all_role_total_row_report[8] = $all_role_total_row[3];
				$all_role_total_row_report[9] = $all_role_total_row[4];
				$all_role_total_row_report[10] = format_currency( $all_role_total_row[5] );
				$all_role_total_row_report[11] = format_currency( $all_role_total_row[6] );
				$all_role_total_row_report[12] = format_currency( $all_role_total_row[7] );
				$all_role_total_row_report[13] = format_currency( $all_role_total_row[8] );
				$role_breakdown_rows .= $this->outputDataRow( $all_role_total_row_report );
			}

			$role_breakdown_rows = $this->wrap_item_with_tbody( $role_breakdown_rows );

			$time_picker = TimeCardViews::header_tostr();
			$o_time_cards_chooser_obj = TimeCardUserFunctions::get_date_chooser_obj();

			$user = $_GET['user'];
			$alternate_link = "<a href=\"index.php?{$o_time_cards_chooser_obj->forward_vars}&user={$user}&format=decimalized\">Hours as a Decimal</a>. Please note that you may encounter a loss of precision.";
			if( isset($_GET['format']) ){
				$setdate = $_GET['setdate'];
				$mode = $_GET['mode'];
				$day_duration = $_GET['day_duration'];
				global $section;
				$alternate_link = "Hours represented as <a href=\"index.php?mode={$mode}&setdate={$setdate}&day_duration={$day_duration}&user={$user}\">Hours:Minutes</a>.";
			}

			$notice = "<p>Click for the report with the {$alternate_link}</p><p style=\"text-align: left\"><b>NOTICE:</b> If you do not see time for the punches, please navigate to <a href='index.php?mode=settings_overtime_settings'>Overtime Settings</a> and click 'submit', even if you do not modify anything.</p>";

			if( !(isset( $_GET['totalsonly'] ) && $_GET['totalsonly'] == '1') ){
				$notice = "<button onclick=\"window.location='index.php?{$o_time_cards_chooser_obj->forward_vars}&user={$user}&totalsonly=1';\">List Totals Only</button><br />" . $notice;
			} else {
				$notice = "<button onclick=\"window.location='index.php?{$o_time_cards_chooser_obj->forward_vars}&user={$user}&totalsonly=0';\">List All</button><br />" . $notice;
			}

			$o_time_cards_chooser_obj->forward_vars;
			$user = $_GET['user'];

			$create_punch_option = '';
			if('all' != $user){
				$o_user = null;
				if( !$user_id ){
					echo $user . '<br />';
					$o_user = TimeCardUser::userByUsername( $user );
				} else {
					$o_user = TimeCardUser::user( $user_id );
				}

				$user_id = $o_user->id;
				$user_first_last = '';
				if( $o_user ){
					$user_first_last = $o_user->fullname();
				}

				//$user = TimeCardUser::user( $user_id );
				//echo '<pre style="text-align: left;">'.print_r($user, true).'</pre>';

				$create_punch_option = "
				<button onclick=\"show_info('<iframe src=&quot;resources/form_dialog.php?mode=timecard&editpunch=new&userid={$user_id}&username=". urlencode($user_first_last)."&quot; width=&quot;500&quot; height=&quot;425&quot; frameborder=&quot;0&quot; allowtransparency=&quot;true&quot;></iframe>')\">Create New Punch for {$user_first_last}</button><br /><br />
				";
			}
			if ($_GET['ordermode']) {
				$orderMode = $_GET['ordermode'];
			} else {
				$orderMode = "asc";
			}
			$links = "<br /><br /><div style=\"text-align: left;\">
			{$create_punch_option}
			<a href=\"index.php?{$o_time_cards_chooser_obj->forward_vars}&user={$user}&ordermode={$orderMode}&export=csv\">Export To Comma Delimited &quot;.csv&quot; file for import into MS Excel</a><br /><br />
			<a href=\"index.php?{$o_time_cards_chooser_obj->forward_vars}&user={$user}&ordermode={$orderMode}&export=txt\">Export To Tab Delimited &quot;.txt&quot; file</a><br />
			</div>
			";

			//forward_vars
			return $window . "\n" . $styles . "\n" . $script . "\n" . $time_picker . "\n" . $notice . "\n<br />\n<br />\n" . $this->wrap_item_with_table( $table_head . $table_body . $table_foot ) . '<br /><br /><br />' . $this->wrap_item_with_table($role_breakdown_head . $role_breakdown_rows) . $links;
		}
	}

	class CSVFormatter implements ReportFormatter {
		private function formatEntry( $item ){
			if( stripos($item, ',') !== FALSE || stripos($item, '"') !== FALSE ){
				return '"' . str_ireplace( '"', '\"', str_ireplace("\\", '\\', $item) ) . '"';
			}
			return $item;
		}


		private function outputRow( $row ){
			$result = '';
			for( $i = 0; $i < count($row); $i++ ){
				if($i){
					$result .= ',';
				}
				$result .= $this->formatEntry( $row[$i] );
			}
			return $result;
		}

		public function toOutput( $data, $header ){
			global $location_info;
			//print_r($data);
			$current_tz = date_default_timezone_get();
			if( !empty($location_info['timezone']) ){
				date_default_timezone_set( $location_info['timezone'] );
			}
			$result = '';
			$result .= $this->outputRow( $header ) . "\n";

			$sort_order = SortOrder::sortOrder()->order();
			$index0 = $sort_order[0];
			$index1 = $sort_order[1];
			$index2 = $sort_order[2];

			$all_users_totals = array( 'id', 'name', 'role', 'start', 'stop', 0, 0, 0, 0, 0, 0, 0, 0 );
			$role_totals = array( 'id', 'name', 'role', 'start', 'stop', 0, 0, 0, 0, 0, 0, 0, 0 ); //5 - 12
			$user_totals = array( 'id', 'name', 'role', 'start', 'stop', 0, 0, 0, 0, 0, 0, 0, 0 );
			for( $i = 0; $i < count($data); $i++ ){
				$output_data = array();
				$output_data[0] = $data[$i][0];
				$output_data[1] = $data[$i][1];
				$output_data[2] = $data[$i][2]->title;
				$output_data[3] = $data[$i][3];
				$output_data[4] = date('m/d h:iA',$data[$i][4]);
				if( is_string( $data[$i][5] ) && $data[$i][5][0] === 'e' ){
					$data[$i][5] = substr($data[$i][5], 1 ) * 1;
					$output_data[5] = 'now: '.date('m/d h:iA',$data[$i][5]);
				} else {
					$output_data[5] = date('m/d h:iA',$data[$i][5]);
				}
				//$output_data[5] = $data[$i][5] / 3600;
				$output_data[6] = $data[$i][6] / 3600;
				$output_data[7] = $data[$i][7] / 3600;
				$output_data[8] = $data[$i][8] / 3600;
				$output_data[9] = $data[$i][9] / 3600;
				global $modules;
				if( $modules->hasModule( 'employees.payrates' ) ){
					$output_data[10] = format_currency( $data[$i][10] );
					$output_data[11] = format_currency( $data[$i][11] );
					$output_data[12] = format_currency( $data[$i][12] );
					$output_data[13] = format_currency( $data[$i][13] );
				}
				for( $j = 6; $j <= 13; $j++ ){
					$role_totals[$j] += $data[$i][$j];
					$user_totals[$j] += $data[$i][$j];
					$all_users_totals[$j] += $data[$i][$j];
					$all_role_totals[$output_data[2]][$j-5] += $data[$i][$j];
				}
				if( !(isset( $_GET['totalsonly'] ) && $_GET['totalsonly'] == '1') ){
					$result .= $this->outputRow($output_data) . "\n";
				}

				if( $i == count( $data ) - 1 || TimeCardViews::getEntryValue($data[$i][$index1]) != TimeCardViews::getEntryValue($data[$i+1][$index1])  ||TimeCardViews::getEntryValue( $data[$i][$index0]) != TimeCardViews::getEntryValue($data[$i+1][$index0]) ){
					//Output Role Totals
					$role_totals[0] = '';//$data[$i][0];
					$role_totals[1] = '';//$data[$i][1];
					$role_totals[2] = '';//$data[$i][2]->title;
					$role_totals[3] = '';
					$role_totals[4] = '';
					$role_totals[5] = '';
					$role_totals[6] = $role_totals[6]/3600;
					$role_totals[7] = $role_totals[7]/3600;
					$role_totals[8] = $role_totals[8]/3600;
					$role_totals[9] = $role_totals[9]/3600;
					$role_totals[10] = format_currency( $role_totals[10] );
					$role_totals[11] = format_currency( $role_totals[11] );
					$role_totals[12] = format_currency( $role_totals[12] );
					$role_totals[13] = format_currency( $role_totals[13] );
					$role_totals[$index1] = TimeCardViews::getEntryValue( $data[$i][$index1] );

					if( !(isset( $_GET['totalsonly'] ) && $_GET['totalsonly'] == '1') ){
						// $result .= $this->outputRow( $role_totals ) . "\n;"
					}
					$role_totals = array( 'id', 'name', 'role', 'start', 'stop', 0, 0, 0, 0, 0, 0, 0, 0 );
				}

				if( $i == count( $data ) - 1 || TimeCardViews::getEntryValue($data[$i][$index0]) != TimeCardViews::getEntryValue($data[$i+1][$index0]) ){
					//Output UserTotals
					$user_totals[0] = '';//$data[$i][0];
					$user_totals[1] = '';//$data[$i][1];
					$user_totals[2] = '';
					$user_totals[3] = '';
					$user_totals[4] = '';
					$user_totals[5] = '';
					$user_totals[6] = $user_totals[6]/3600;
					$user_totals[7] = $user_totals[7]/3600;
					$user_totals[8] = $user_totals[8]/3600;
					$user_totals[9] = $user_totals[9]/3600;
					$user_totals[10] = format_currency( $user_totals[10] );
					$user_totals[11] = format_currency( $user_totals[11] );
					$user_totals[12] = format_currency( $user_totals[12] );
					$user_totals[13] = format_currency( $user_totals[13] );
					$user_totals[$index0] = TimeCardViews::getEntryValue( $data[$i][$index0] );
					if( (isset( $_GET['totalsonly'] ) && $_GET['totalsonly'] == '1') ){
						$result .= $this->outputRow( $user_totals ) . "\n";
					}
					$user_totals = array( 'id', 'name', 'role', 'start', 'stop', 0, 0, 0, 0, 0, 0, 0, 0 );
				}

				if( $i == count( $data ) - 1 ){
					//Output All User Totals
					$all_users_totals[0] = '';
					$all_users_totals[1] = 'Total';
					$all_users_totals[2] = '';
					$all_users_totals[3] = '';
					$all_users_totals[4] = '';
					$all_users_totals[5] = '';
					$all_users_totals[6] = $all_users_totals[6]/3600;
					$all_users_totals[7] = $all_users_totals[7]/3600;
					$all_users_totals[8] = $all_users_totals[8]/3600;
					$all_users_totals[9] = $all_users_totals[5]/3600;
					$all_users_totals[10] = format_currency( $all_users_totals[10] );
					$all_users_totals[11] = format_currency( $all_users_totals[11] );
					$all_users_totals[12] = format_currency( $all_users_totals[12] );
					$all_users_totals[13] = format_currency( $all_users_totals[13] );
					// $table_foot = $this->outputRow( $all_users_totals );
					$all_users_totals = array( 'id', 'name', 'role', 'start', 'stop', 0, 0, 0, 0, 0, 0, 0, 0 );
				}
			}

			$o_time_cards_chooser_obj = TimeCardUserFunctions::get_date_chooser_obj();
			export_report( "time_cards_{$_GET['user']}_{$o_time_cards_chooser_obj->use_date1}_{$o_time_cards_chooser_obj->use_date2}.csv", $result);
			date_default_timezone_set( $current_tz );
			exit();
			return '';
		}
	}

	class TXTFormatter implements ReportFormatter {
		private function formatEntry( $item ){
			if( stripos($item, "\t") !== FALSE || stripos($item, '"') !== FALSE ){
				return '"' . str_ireplace( '"', '\"', str_ireplace("\\", '\\', $item) ) . '"';
			}
			return $item;
		}


		private function outputRow( $row ){
			$result = '';
			for( $i = 0; $i < count($row); $i++ ){
				if($i){
					$result .= "\t";
				}
				$result .= $this->formatEntry( $row[$i] );
			}

			return $result;
		}

		public function toOutput( $data, $header ){
			global $location_info;
			$current_tz = date_default_timezone_get();
			if( !empty($location_info['timezone']) ){
				date_default_timezone_set( $location_info['timezone'] );
			}
			$result = '';
			$result .= $this->outputRow( $header ) . "\n";

			$sort_order = SortOrder::sortOrder()->order();
			$index0 = $sort_order[0];
			$index1 = $sort_order[1];
			$index2 = $sort_order[2];

			$all_users_totals = array( 'id', 'name', 'role', 'start', 'stop', 0, 0, 0, 0, 0, 0, 0, 0 );
			$role_totals = array( 'id', 'name', 'role', 'start', 'stop', 0, 0, 0, 0, 0, 0, 0, 0 ); //5 - 12
			$user_totals = array( 'id', 'name', 'role', 'start', 'stop', 0, 0, 0, 0, 0, 0, 0, 0 );
			for( $i = 0; $i < count($data); $i++ ){
				$output_data = array();
				$output_data[0] = $data[$i][0];
				$output_data[1] = $data[$i][1];
				$output_data[2] = $data[$i][2]->title;
				$output_data[3] = $data[$i][3];
				$output_data[4] = date('m/d h:iA',$data[$i][4]);
				if( is_string( $data[$i][5] ) && $data[$i][5][0] === 'e' ){
					$data[$i][5] = substr($data[$i][5], 1 ) * 1;
					$output_data[5] = 'now: '.date('m/d h:iA',$data[$i][5]);
				} else {
					$output_data[5] = date('m/d h:iA',$data[$i][5]);
				}
				//$output_data[5] = $data[$i][5] / 3600;
				$output_data[6] = $data[$i][6] / 3600;
				$output_data[7] = $data[$i][7] / 3600;
				$output_data[8] = $data[$i][8] / 3600;
				$output_data[9] = $data[$i][9] / 3600;
				global $modules;
				if( $modules->hasModule( 'employees.payrates' ) ){
					$output_data[9] = format_currency( $data[$i][9] );
					$output_data[10] = format_currency( $data[$i][10] );
					$output_data[11] = format_currency( $data[$i][11] );
					$output_data[12] = format_currency( $data[$i][12] );
					$output_data[13] = format_currency( $data[$i][13] );
				}

				for( $j = 5; $j <= 12; $j++ ){
					$role_totals[$j] += $data[$i][$j];
					$user_totals[$j] += $data[$i][$j];
					$all_users_totals[$j] += $data[$i][$j];
					$all_role_totals[$output_data[2]][$j-4] += $data[$i][$j];
				}

				if( !(isset( $_GET['totalsonly'] ) && $_GET['totalsonly'] == '1') ){
					$result .= $this->outputRow($output_data) . "\n";
				}

				if( $i == count( $data ) - 1 || TimeCardViews::getEntryValue($data[$i][$index1]) != TimeCardViews::getEntryValue($data[$i+1][$index1])  ||TimeCardViews::getEntryValue( $data[$i][$index0]) != TimeCardViews::getEntryValue($data[$i+1][$index0]) ){
					//Output Role Totals
					$role_totals[0] = '';//$data[$i][0];
					$role_totals[1] = '';//$data[$i][1];
					$role_totals[2] = '';//$data[$i][2]->title;
					$role_totals[3] = '';
					$role_totals[4] = '';
					$role_totals[5] = '';
					$role_totals[6] = $role_totals[6]/3600;
					$role_totals[7] = $role_totals[7]/3600;
					$role_totals[8] = $role_totals[8]/3600;
					$role_totals[9] = $role_totals[9]/3600;
					$role_totals[10] = format_currency( $role_totals[10] );
					$role_totals[11] = format_currency( $role_totals[11] );
					$role_totals[12] = format_currency( $role_totals[12] );
					$role_totals[13] = format_currency( $role_totals[13] );
					$role_totals[$index1] = TimeCardViews::getEntryValue( $data[$i][$index1] );

					if( !(isset( $_GET['totalsonly'] ) && $_GET['totalsonly'] == '1') ){
						// $result .= $this->outputRow( $role_totals ) ."\n";
					}
					$role_totals = array( 'id', 'name', 'role', 'start', 'stop', 0, 0, 0, 0, 0, 0, 0, 0 );
				}

				if( $i == count( $data ) - 1 || TimeCardViews::getEntryValue($data[$i][$index0]) != TimeCardViews::getEntryValue($data[$i+1][$index0]) ){
					//Output UserTotals
					$user_totals[0] = '';//$data[$i][0];
					$user_totals[1] = '';//$data[$i][1];
					$user_totals[2] = '';
					$user_totals[3] = '';
					$user_totals[4] = '';
					$user_totals[5] = '';
					$user_totals[6] = $user_totals[6]/3600;
					$user_totals[7] = $user_totals[7]/3600;
					$user_totals[8] = $user_totals[8]/3600;
					$user_totals[9] = $user_totals[9]/3600;
					$user_totals[10] = format_currency( $user_totals[10] );
					$user_totals[11] = format_currency( $user_totals[11] );
					$user_totals[12] = format_currency( $user_totals[12] );
					$user_totals[13] = format_currency( $user_totals[13] );
					$user_totals[$index0] = TimeCardViews::getEntryValue( $data[$i][$index0] );
					if( (isset( $_GET['totalsonly'] ) && $_GET['totalsonly'] == '1') ){
						$result .= $this->outputRow( $user_totals );
					}
					$user_totals = array( 'id', 'name', 'role', 'start', 'stop', 0, 0, 0, 0, 0, 0, 0, 0 );
				}

				if( $i == count( $data ) - 1 ){
					//Output All User Totals
					$all_users_totals[0] = '';
					$all_users_totals[1] = 'Total';
					$all_users_totals[2] = '';
					$all_users_totals[3] = '';
					$all_users_totals[4] = '';
					$all_users_totals[5] = '';
					$all_users_totals[6] = $all_users_totals[6]/3600;
					$all_users_totals[7] = $all_users_totals[7]/3600;
					$all_users_totals[8] = $all_users_totals[8]/3600;
					$all_users_totals[9] = $all_users_totals[5]/3600;
					$all_users_totals[10] = format_currency( $all_users_totals[10] );
					$all_users_totals[11] = format_currency( $all_users_totals[11] );
					$all_users_totals[12] = format_currency( $all_users_totals[12] );
					$all_users_totals[13] = format_currency( $all_users_totals[13] );
					// $table_foot = $this->outputRow( $all_users_totals );
					$all_users_totals = array( 'id', 'name', 'role', 'start', 'stop', 0, 0, 0, 0, 0, 0, 0, 0 );
				}
			}

			if( !(isset( $_GET['totalsonly'] ) && $_GET['totalsonly'] == '1') ){

			}

			$o_time_cards_chooser_obj = TimeCardUserFunctions::get_date_chooser_obj();
			export_report( "time_cards_{$_GET['user']}_{$o_time_cards_chooser_obj->use_date1}_{$o_time_cards_chooser_obj->use_date2}.txt", $result);
			date_default_timezone_set( $current_tz );
			exit();
			return '';
		}
	}

	set_time_limit(10);
	ini_set('max_execution_time', 100);
	if ($in_lavu) {

		$start_time = microtime( true );
		if (!isset($_GET['user'])) {
			TimeCardViews::draw_choose_users();
		} else {
			TimeCardViews::draw_report();
		}

		$end_time = microtime( true );

		$total_time_ms = ROUND(($end_time - $start_time)*1000,2);
		// echo "Total Time Taken to Compute: {$total_time_ms}ms";
	}
?>
