<?php
	function display_datetime($d)
	{
		$d_parts = explode(" ",$d);
		if(count($d_parts) > 1)
		{
			$t_parts = explode(":",$d_parts[1]);
			$d_parts = explode("-",$d_parts[0]);
			
			if(count($d_parts) > 2 && count($t_parts) > 2)
			{
				return date("m/d/Y h:i:s a",mktime($t_parts[0],$t_parts[1],$t_parts[2],$d_parts[1],$d_parts[2],$d_parts[0]));
			}
			else return $d;
		}
		else return $d;
	}
	
	function display_date1($d)
	{
		$d_parts = explode("-",$d);
		
		if(count($d_parts) > 2)
		{
			return date("m/d",mktime(0,0,0,$d_parts[1],$d_parts[2],$d_parts[0]));
		}
		else return $d;
	}
	
	function display_price($p)
	{
		return "$" . number_format($p,2);
	}
	
	echo "<br><br>";
	require_once("/home/poslavu/public_html/admin/sa_cp/show_report.php");
	$date_info = show_report(admin_info("database"),"poslavu_MAIN_db","unified","index.php?mode=$mode","select_date");
	$date_start = $date_info['date_start'];
	$date_end = $date_info['date_end'];
	$time_start = $date_info['time_start'];
	$time_end = $date_info['time_end'];
	$date_reports_by = $date_info['date_reports_by'];
	$locationid = $date_info['locationid'];
	
	$start_datetime = $date_start . " " . $time_start;
	$end_datetime = $date_end . " " . $time_end;

	$start_date_parts = explode("-",$date_start);
	$start_time_parts = explode(":",$time_start);
	$start_ts = mktime($start_time_parts[0],$start_time_parts[1],$start_time_parts[2],$start_date_parts[1],$start_date_parts[2],$start_date_parts[0]);
	$end_ts = mktime($start_time_parts[0],$start_time_parts[1],$start_time_parts[2],$start_date_parts[1],$start_date_parts[2] + 1,$start_date_parts[0]);
	
	$start_datetime = date("Y-m-d H:i:s",$start_ts);
	$end_datetime = date("Y-m-d H:i:s",$end_ts);
		
	echo "<br><br>";
	echo "Showing Tipouts for <b><font color='#000044'>" . date("m/d/Y",$start_ts) . "</font></b><br>";
	
	echo "<style>";
	echo ".coltitle { font-family:Verdana,Arial; color:#888888; text-align:right; background-color:#f0f0f0; } ";
	echo ".iheader { color:#ffffff; text-align:center; background-color:#777777; padding-right:6px; padding-left:6px;} ";
	echo ".itotal { text-align:center; border-top:solid 1px #777777;} ";
	echo "</style>";
	
	//echo "Locationid: $locationid<br>Date Reports By: $date_reports_by<br>$start_datetime - $end_datetime<br>";
	$class_sales = array();
	$sales_query = lavu_query("select `emp_classes`.`title` as `employee_class_name`, `emp_classes`.`id` as `employee_class_id`, `orders`.`opened` AS `field0`,`orders`.`void` AS `field1`,`orders`.`location_id` AS `field2`,`orders`.`change_amount` AS `field3`,`orders`.`gift_certificate` AS `field4`,IF (`orders`.`cash_paid` > `orders`.`gratuity`, `orders`.`gratuity`,`orders`.`cash_paid`) AS `field5`,`orders`.`togo_status` AS `field6`,`orders`.`register` AS `field7`,`orders`.`register_name` AS `field8`,`users`.`id` AS `field9`,concat(`orders`.`server`,' (#',`orders`.`server_id`,')') AS `field10`,`orders`.`server_id` AS `field11`,`users`.`l_name` AS `field12`,sum(`orders`.`guests`) AS `field13`,sum(`orders`.`idiscount_amount`) AS `field14`,sum(`orders`.`subtotal` - `orders`.`itax` + `orders`.`idiscount_amount`) AS `field15`,sum(`orders`.`discount` + `orders`.`idiscount_amount`) AS `field16`,sum(`orders`.`subtotal` - `orders`.`itax` - `orders`.`discount`) AS `field17`,sum(`orders`.`tax` + `orders`.`itax`) AS `field18`,sum(`orders`.`total` - `orders`.`gratuity`) AS `field19`,sum(`orders`.`cash_paid` - `orders`.`change_amount`) AS `field20`,sum(`orders`.`card_paid`) AS `field21`,sum(`orders`.`alt_paid` + `orders`.`gift_certificate`) AS `field22`,sum(`orders`.`gratuity`) AS `field23`,sum(`orders`.`ag_cash`) AS `field24`,sum(`orders`.`ag_card`) AS `field25`,sum(`orders`.`cash_tip`) AS `field26`,sum(`orders`.`card_gratuity`) AS `field27`,sum(`orders`.`card_gratuity` + `orders`.`ag_card`) AS `field28` from `orders` LEFT JOIN `users` ON `orders`.`server_id` = `users`.`id` LEFT JOIN `emp_classes` ON `users`.`employee_class`=`emp_classes`.`id` where `orders`.`[1]` >= '[2]' and `orders`.`[1]` <= '[3]' and `orders`.`void` = 0 and `orders`.`location_id` = '[4]' and `orders`.`server_id` > 0 group by `field11` order by `field10`",$date_reports_by,$start_datetime,$end_datetime,$locationid);
	while($sales_read = mysqli_fetch_assoc($sales_query))
	{
		$employee_class_id = $sales_read['employee_class_id'];
		$employee_class_name = $sales_read['employee_class_name'];
		$first_name = $sales_read['field10'];
		$server_id = $sales_read['field11'];
		$last_name = $sales_read['field12'];
		$gross = $sales_read['field15'];
		$discounts = $sales_read['field16'];
		$net = $sales_read['field17'];
		$autograt = $sales_read['field23'];
		$ag_cash = $sales_read['field24'];
		$ag_card = $sales_read['field25'];
		$cashtip = $sales_read['field26'];
		$cardtip = $sales_read['field27'];
		$cash_paid = $sales_read['field20'];
		
		$totalcard_tips = ($ag_card + $cardtip);
		$totaltip_estimate = ($gross * .18);
		
		$cashtip_estimate1 = $cash_paid * .18;
		$cashtip_estimate2 = $totaltip_estimate - $totalcard_tips;
		
		$estimate1_total_percentage = number_format((($cashtip_estimate1 + $totalcard_tips) / $gross) * 100,2);
				
		if(trim($employee_class_id)!="")
		{
			if(!isset($class_sales[$employee_class_id]))
				$class_sales[$employee_class_id] = array();
			$class_sales[$employee_class_id][] = array("server_id"=>$server_id,
														"employee_class_name"=>$employee_class_name,
														"first_name"=>$first_name,
														"last_name"=>$last_name,
														"gross"=>$gross,
														"discounts"=>$discounts,
														"net"=>$net,
														"autograt"=>$autograt,
														"ag_cash"=>$ag_cash,
														"ag_card"=>$ag_card,
														"cashtip"=>$cashtip,
														"cardtip"=>$cardtip,
														"cash_paid"=>$cash_paid,
														"totalcard_tips"=>$totalcard_tips,
														"totaltip_estimate"=>$totaltip_estimate,
														"cashtip_estimate1"=>$cashtip_estimate1,
														"cashtip_estimate2"=>$cashtip_estimate2,
														"estimate1_total_percentage"=>$estimate1_total_percentage);
		}
	}
	
	$classlist = array();
	$classname_query = lavu_query("select * from `emp_classes`");
	while($classname_read = mysqli_fetch_assoc($classname_query))
	{
		$classname_read['tipin'] = 0;
		$classname_read['users_present'] = array();
		$classlist[$classname_read['id']] = $classname_read;
	}
	
	$class_query = lavu_query("select * from `emp_classes` where `tipout_rules`!=''");
	while($class_read = mysqli_fetch_assoc($class_query))
	{
		$class_title = $class_read['title'];
		$class_id = $class_read['id'];
		$tipout_rules = explode(",",$class_read['tipout_rules']);
		$total_percentage = 0;
		
		echo "<br>" . $class_title . ":</b>";
		$tipout_groups = array();
		for($i=0; $i<count($tipout_rules); $i++)
		{
			$trule = explode("|",$tipout_rules[$i]);
			if(count($trule) > 2)
			{
				$secondary = $trule[0];
				$percentage = $trule[1];
				$primary = $trule[2];
				
				$total_percentage += $percentage;
				$tipout_groups[] = array("primary"=>$primary,"secondary"=>$secondary,"percentage"=>$percentage);
			}
		}

		$tipout_card_total = 0;
		$tipout_cash_total = 0;
		$tipout_total_total = 0;
		$sales_total = 0;
		$tip_card_total = 0;
		$tip_cash_total = 0;
		$tip_all_total = 0;
		if(isset($class_sales[$class_id]) && is_array($class_sales[$class_id]))
		{
			echo "<table cellspacing=0 cellpadding=4>";
			echo "<tr>";
			echo "<td class='iheader'>&nbsp;</td>";
			echo "<td class='iheader'>Gross</td>";
			echo "<td class='iheader'>Card Tips</td>";
			echo "<td class='iheader'>Cash Sales</td>";
			echo "<td class='iheader'>Estimated Cash Tips</td>";
			echo "<td class='iheader'>Percentage</td>";
			echo "<td class='iheader'>Tipout for Card</td>";
			echo "<td class='iheader'>Tipout for Cash</td>";
			echo "<td class='iheader'>Total Tipout</td>";
			echo "</tr>";
			
			for($i=0; $i<count($class_sales[$class_id]); $i++)
			{
				$csale = $class_sales[$class_id][$i];
				$tipout_card = $csale['totalcard_tips'] * $total_percentage / 100;
				$tipout_cash = $csale['cashtip_estimate1'] * $total_percentage / 100;
				$tipout_total = $tipout_card + $tipout_cash;
				
				echo "<tr>";
				echo "<td align='center'>" . $csale['first_name'] . "</td>";
				echo "<td align='center'>$" . number_format($csale['gross'],2) . "</td>";
				echo "<td align='center'>$" . number_format($csale['totalcard_tips'],2) . "</td>";
				echo "<td align='center'>$" . number_format($csale['cash_paid'],2) . "</td>";
				echo "<td align='center'>$" . number_format($csale['cashtip_estimate1'],2) . "</td>";
				//echo "<td align='center'>" . number_format($csale['estimate1_total_percentage'],0) . "</td>";
				echo "<td align='center'>" . number_format($total_percentage,0) . "%</td>";
				echo "<td align='center'>$" . number_format($tipout_card,2) . "</td>";
				echo "<td align='center'>$" . number_format($tipout_cash,2) . "</td>";
				echo "<td align='center'>$" . number_format($tipout_total,2) . "</td>";
				echo "</tr>";
				
				$tipout_card_total += $tipout_card;
				$tipout_cash_total += $tipout_cash;
				$tipout_total_total += $tipout_total;
				$tip_card_total += $csale['totalcard_tips'];
				$tip_cash_total += $csale['cashtip_estimate1'];
				$tip_all_total += $csale['totalcard_tips'] + $csale['cashtip_estimate1'];
				$sales_total += $csale['gross'];
			}
			echo "<tr>";
			echo "<td colspan=6' class='itotal'>&nbsp;</td>";
			echo "<td class='itotal'>$" . number_format($tipout_card_total,2) . "</td>";
			echo "<td class='itotal'>$" . number_format($tipout_cash_total,2) . "</td>";
			echo "<td class='itotal'>$" . number_format($tipout_total_total,2) . "</td>";
			echo "</tr>";
			echo "</table>";
		}
		
		echo "<table style='border:solid 1px #bbbbbb' bgcolor='#f2f2f2'>";
		for($n=0; $n<count($tipout_groups); $n++)
		{
			$tg = $tipout_groups[$n];
			$show_group_name = trim($classlist[$tg['primary']]['title']);
			if(trim($tg['secondary'])!="") 
			{
				$show_group_name .= " / " . $classlist[$tg['secondary']]['title'];
			}
			
			$class_tipout_amount = $tip_all_total * $tg['percentage'] / 100;
			$class_tipout_amount_card = $tip_card_total * $tg['percentage'] / 100;
			$class_tipout_amount_cash = $tip_cash_total * $tg['percentage'] / 100;
			$classlist[$tg['primary']]['tipin'] += $class_tipout_amount;
			$classlist[$tg['primary']]['tipin_card'] += $class_tipout_amount_card;
			$classlist[$tg['primary']]['tipin_cash'] += $class_tipout_amount_cash;
			echo "<tr><td align='right'>&nbsp;&nbsp;Tipout " . $tg['percentage'] . "% ($".number_format($class_tipout_amount,2).")</td><td> to " . $show_group_name . "&nbsp;&nbsp;</td></tr>";
		}
		echo "</table>";
		echo "<br>";
		//echo "Primary: $primary Secondary: $secondary Percentage: $percentage<br>";		
	}

	$clock_query = lavu_query("select distinct(`server_id`) as `server_id`,`employee_class`,`f_name`,`l_name` from `clock_punches` left join `users` on `clock_punches`.`server_id`=`users`.`id` where `clock_punches`.`time`>='[1]' and `clock_punches`.`time`<='[2]' order by `f_name` asc, `l_name` asc",$start_datetime,$end_datetime);
	while($clock_read = mysqli_fetch_assoc($clock_query))
	{
		$server_id = $clock_read['server_id'];
		$employee_class = $clock_read['employee_class'];
		$classlist[$employee_class]['users_present'][] = $clock_read;
	}
	
	echo "<table cellspacing=0 cellpadding=4>";
	foreach($classlist as $key => $cl)
	{
		if($cl['tipin'] > 0)
		{
			echo "<tr bgcolor='#666699'>";
			echo "<td colspan='5' style='color:#ffffff;' align='right'><b>" . $cl['title'] . "</b> - Total Tips to receive: <b>$" . number_format($cl['tipin'],2) . "</b></td>";
			echo "<td style='color:#ffffff;' align='right'>Card: <b>$".number_format($cl['tipin_card'],2)."</b></td>";
			echo "<td style='color:#ffffff;' align='right'>Cash: <b>$".number_format($cl['tipin_cash'],2)."</b></td>";
			echo "</tr>";
			
			$user_share = $cl['tipin'] / count($cl['users_present']);
			$user_share_card = $cl['tipin_card'] / count($cl['users_present']);
			$user_share_cash = $cl['tipin_cash'] / count($cl['users_present']);
			
			for($n=0; $n<count($cl['users_present']); $n++)
			{
				$user_read = $cl['users_present'][$n];
				
				echo "<tr>";
				echo "<td>&nbsp;</td>";
				echo "<td>&nbsp;</td>";
				echo "<td>" . $user_read['f_name'] . "</td>";
				echo "<td>" . $user_read['l_name'] . "</td>";
				echo "<td align='right'>$" . number_format($user_share,2) . "</td>";
				echo "<td align='right'>$" . number_format($user_share_card,2) . "</td>";
				echo "<td align='right'>$" . number_format($user_share_cash,2) . "</td>";
				echo "</tr>";
			}
			
			echo "<tr><td>&nbsp;</td></tr>";
		}
	}
	echo "</table>";
?>
