<?php

ini_set("display_errors","1");
require_once(dirname(__FILE__)."/kbi_functions4.php");
set_time_limit (500);
checkIndexes();//make sure all indexes and fields are formatted properly
?>
<?php
/* set variables here from database, update if necessary */

//event_dept
if ($_POST['event_department'] != ""){
		$get_event_dept_res = lavu_query("SELECT id FROM config WHERE type='kbi_setting' AND setting='event_department' AND location='$locationid'");
		$set_event_dept = $_POST['event_department'];//CSR, Mechanic, Trackside, etc....	
		if (mysqli_num_rows($get_event_dept_res)*1 < 1){
					lavu_query("INSERT INTO config (location,type,setting,value) VALUES ('$locationid','kbi_setting','event_department','$set_event_dept')");
		}//if
		lavu_query("UPDATE config SET value='$set_event_dept' WHERE location='$locationid' AND type='kbi_setting' AND setting='event_department'");
}//if
$get_event_dept_id_res = lavu_query("SELECT value FROM config WHERE location='$locationid' AND type='kbi_setting' AND setting='event_department'");
$get_event_dept_id_row = mysqli_fetch_assoc($get_event_dept_id_res);
$event_department = $get_event_dept_id_row['value'];
		//echo "event dept: $event_dept_index<br/>";


//role_efficiency
if ($_POST['role_efficiency'] != ""){
		$get_role = lavu_query("SELECT id FROM config WHERE type='kbi_setting' AND setting='role_efficiency' AND location='$locationid'");
		$set_role_efficiency = $_POST['role_efficiency'];//CSR, Mechanic, Trackside, etc....	
		if (mysqli_num_rows($get_role)*1 < 1){
					lavu_query("INSERT INTO config (location,type,setting,value) VALUES ('$locationid','kbi_setting','role_efficiency','$set_role_efficiency')");
		}//if
		lavu_query("UPDATE config SET value='$set_role_efficiency' WHERE location='$locationid' AND type='kbi_setting' AND setting='role_efficiency'");
}//if
$role_effic = lavu_query("SELECT value FROM config WHERE location='$locationid' AND type='kbi_setting' AND setting='role_efficiency'");
$role_effic_row = mysqli_fetch_assoc($role_effic);
$role_efficiency = $role_effic_row['value'];
		//echo "role efficiency: $role_efficiency<br/>";
		
//track_efficiency
if ($_POST['track_efficiency'] != ""){
		$get_track = lavu_query("SELECT id FROM config WHERE type='kbi_setting' AND setting='track_efficiency' AND location='$locationid'");
		$set_track_efficiency = $_POST['track_efficiency'];//CSR, Mechanic, Trackside, etc....	
		if (mysqli_num_rows($get_track)*1 < 1){
					lavu_query("INSERT INTO config (location,type,setting,value) VALUES ('$locationid','kbi_setting','track_efficiency','$set_track_efficiency')");
		}//if
		lavu_query("UPDATE config SET value='$set_track_efficiency' WHERE location='$locationid' AND type='kbi_setting' AND setting='track_efficiency'");
}//if
$track_effic = lavu_query("SELECT value FROM config WHERE location='$locationid' AND type='kbi_setting' AND setting='track_efficiency'");
$track_effic_row = mysqli_fetch_assoc($track_effic);
$track_efficiency = $track_effic_row['value'];
		//echo "track efficiency: $track_efficiency<br/>";


//races_sold - what gets considered in the 'Track Data' section
if ($_POST['track_data'] != ""){
		$get_track_data = lavu_query("SELECT id FROM config WHERE type='kbi_setting' AND setting='track_data' AND location='$locationid'");
		$set_track_data = $_POST['track_data'];//CSR, Mechanic, Trackside, etc....	
		if (mysqli_num_rows($get_track_data)*1 < 1){
					lavu_query("INSERT INTO config (location,type,setting,value) VALUES ('$locationid','kbi_setting','track_data','$set_track_data')");
		}//if
		lavu_query("UPDATE config SET value='$set_track_data' WHERE location='$locationid' AND type='kbi_setting' AND setting='track_data'");
}//if
$track_data = lavu_query("SELECT value FROM config WHERE location='$locationid' AND type='kbi_setting' AND setting='track_data'");
$track_data_row = mysqli_fetch_assoc($track_data);
$track_data_id = $track_data_row['value'];
		//echo "track data: $track_data<br/>";


/* End set variables section */			
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Untitled Document</title>

<style type="text/css">

.leftForm{
	float:left;
	width:250px;
	text-align:right;
	margin-left:150px;
}
.rightForm{
	float:left;
	width:250px;
	text-align:left;
	margin-left:5px;
}
.clearForm{
	clear:both;
}
.rowElement{
	float:left;
	text-align: right;
	margin-left:5px;
	padding-left: 10px;
}
.innerLeftForm{
	float:left;
	width:100px;
	text-align:right;
}
.innerRightForm{
	float:left;
	width:150px;
	text-align:left;
}
.clearInnerForm{
	clear:both;
}
.divider{
	border-bottom: 1px solid #000;
}
.bottomDivider{
	border-bottom: 10px solid #ccddff;
}
.row{
	border-bottom: 1px solid grey;
	
}

.header{
	font-weight:700;

}

.clearRow{
	clear:both;
}
.re0Header{
	width:125px;
	margin-right:10px;
	text-align: left;
	padding:10px;
}

.re0{
	width:125px;
	margin-right:10px;
	border-right:1px solid #000;
	text-align: left;
	
}

.re1{
	width:70px;
	padding-left:10px;
	
}

.re2{
	width:100px;
	padding-left:10px;
}

.re3{
	width:100px;
	padding-left:10px;
}

.re4{
	width:75px;
	padding-left:10px;
}

.re5{
	width:100px;
	padding-left:10px;
}
 
.re6{
	width:100px;
	
}

.re7{
	width:100px;
	
}

.re8{
	width:100px;
	
}
.re9{
	width:60px;
	padding-right:5px;
	
}

label{
	font-size:85%;
}
</style>

</head>

<body>




<form name="select_date_form" method="post" action="">



<div class="leftForm"><label>Start Date</label></div>
<div class="rightForm">
<select name="start_month" id="start_month">
	<option value=""></option>
	<?php 
		for ($month=1; $month<=12; $month++){
				echo "<option value='".pad($month,2)."'>$month</option>";
		}//for
	?>
</select>

<select name="start_day" id="start_day">
	<option value=""></option>
	<?php 
		
		for ($day=1; $day<=date('t')-1; $day++){
				echo "<option value='".pad($day,2)."'>$day</option>";
		}//for
	?>
</select>

<select name="start_year" id="start_year">
	<option value=""></option>
	<?php 
		for ($year=2011; $year<=2014; $year++){
				echo "<option value='$year'>$year</option>";
		}//for
	?>
</select>
</div><!--rightForm-->
<div class="clearForm"></div>



<div class="leftForm">
<label>What menu category should be calculated in the 'Events Department' section?</label>
</div>
<div class="rightForm">
<select name="event_department" id="event_department">
	<option value=""></option>
	<?php
		/* Get which menu they are using */
		$sql_query = "SELECT menu_id FROM locations";
		$use_menu_id_res = lavu_query($sql_query);
		$use_menu_id_row = mysqli_fetch_assoc($use_menu_id_res);
		$use_menu_id = $use_menu_id_row['menu_id'];
		$get_event_dep_res = lavu_query("SELECT * FROM menu_categories WHERE menu_id='$use_menu_id' AND active='1' AND _deleted!='1' ORDER BY name");
		$get_menu_name_from_id = array();
		$build_check_box_string = "";/* we'll build this in this loop and use it later so we don't have to run two queries*/
		while($get_event_dep_row = mysqli_fetch_assoc($get_event_dep_res)){
				echo "<option value='".$get_event_dep_row['id']."'>".$get_event_dep_row['name']."</option>";
				$build_check_box_string .= "<div class='innerLeftForm'>".$get_event_dep_row['name']."</div><div class='innerRightForm'><input type='checkbox' id='revenue_".$get_event_dep_row['id']."' name='revenue_".$get_event_dep_row['id']."' /></div><div class='clearInnerForm'></div>";
				$get_menu_name_from_id[$get_event_dep_row['id']] = $get_event_dep_row['name'];
		}//while
	?>
</select>
</div><!--rightForm-->
<div class="clearForm"></div>


<div class="leftForm"><label>Which employee category would you like to calculate an efficiency report for?</label></div>
<div class="rightForm">
	<select name="role_efficiency" id="role_efficiency">
			<option value=""></option>
			<?php
				$get_roles_info = lavu_query("SELECT id,title FROM roles");
				while ($get_roles_row = mysqli_fetch_assoc($get_roles_info)){
						echo "<option value='".$get_roles_row['id']."'>".$get_roles_row['title']."</option>";
				}//while
			?>
	</select>
</div>
<div class="clearForm"></div>

<div class="leftForm"><label>Calculate track hrs for this employee category</label></div>
<div class="rightForm">
	<select name="track_efficiency" id="track_efficiency">
			<option value=""></option>
			<?php
				$get_roles_info = lavu_query("SELECT id,title FROM roles");
				while ($get_roles_row = mysqli_fetch_assoc($get_roles_info)){
						echo "<option value='".$get_roles_row['id']."'>".$get_roles_row['title']."</option>";
				}//while
			?>
	</select>
</div>
<div class="clearForm"></div>


<div class="leftForm"><label>Set track data option</label></div>
<div class="rightForm">
		<select name="track_data" id="track_data">
				<option value=""></option>
				<?php
					$get_event_dep_res = lavu_query("SELECT * FROM menu_categories WHERE menu_id='$use_menu_id' AND active='1' AND _deleted!='1' ORDER BY name");
					$get_menu_name_from_id = array();
					$build_check_box_string = "";/* we'll build this in this loop and use it later so we don't have to run two queries*/
					while($get_event_dep_row = mysqli_fetch_assoc($get_event_dep_res)){
							echo "<option value='".$get_event_dep_row['id']."'>".$get_event_dep_row['name']."</option>";
							$build_check_box_string .= "<div class='innerLeftForm'>".$get_event_dep_row['name']."</div><div class='innerRightForm'><input type='checkbox' id='revenue_".$get_event_dep_row['id']."' name='revenue_".$get_event_dep_row['id']."' /></div><div class='clearInnerForm'></div>";
							$get_menu_name_from_id[$get_event_dep_row['id']] = $get_event_dep_row['name'];
					}//while
				?>
	</select></div>
<div class="clearForm"></div>


<div class="leftForm">&nbsp;</div>
<div class="rightForm">
<input type="submit" value="Go"/>
</div><!--rightForm-->
<div class="clearForm"></div>
</form>




<div class="row">
	<h2>Performance Indicator</h2>
</div>

<div class="row header">
		
		<div class="rowElement re0Header">
			Category
		</div>
		
		<div class="rowElement re1">
			Current Week
		</div>
		
		<div class="rowElement re2">
			Prior Week
		</div>
		
		<div class="rowElement re3">
			Prior Year
		</div>
		
		<div class="rowElement re4">
			Plan
		</div>
		
		<div class="rowElement re6">
			Trend
		</div>
		
		<div class="rowElement re7">
			Prior Year <span style="font-size:75%;">&#43;&#47;&#45;</span>
		</div>
		
		<div class="rowElement re8">
			&#37; to Goal
		</div>
		
</div><!--row-->



<div class="clearRow"></div>
<div class='divider'></div>
<?php
$the_current_date = date("Y-m-d");
$tcd_arr = explode("-",$the_current_date);

$date_year = $tcd_arr[0];
$date_month = pad($tcd_arr[1],2);
$date_day = pad($tcd_arr[2],2);

/* If they haven't selected a date, select last week */
$use_date = date("Y-m-d",mktime(0,0,0,$date_month*1,$date_day*1-7,$date_year*1));

$tcd_arr = explode("-",$use_date);
$date_year = $tcd_arr[0];
$date_month = pad($tcd_arr[1],2);
$date_day = pad($tcd_arr[2],2);

$db_name = "poslavu_p2r_corona_lavu";
if ( $_POST['start_day']!="" && $_POST['start_month']!="" && $_POST['start_year']!="" ){
		$date_day = pad($_POST['start_day'],2);
		$date_month = pad($_POST['start_month'],2);
		$date_year = $_POST['start_year'];
}//if



$cur_week_date = date("Y-m-d",mktime(0,0,0,$date_month*1,$date_day*1,$date_year*1)); 
$prior_week_date = date("Y-m-d",mktime(0,0,0,$date_month*1,$date_day*1-7,$date_year*1)); 
$prior_year_date = date("Y-m-d",mktime(0,0,0,$date_month*1,$date_day*1,$date_year*1-1)); 


$get_menu_categories = lavu_query("SELECT menu_items.category_id AS category_id, menu_categories.name AS menu_category_name, menu_categories.menu_id AS menu_category_id FROM orders LEFT JOIN order_contents ON orders.order_id=order_contents.order_id LEFT JOIN menu_items ON menu_items.id=order_contents.item_id LEFT JOIN menu_categories ON menu_items.category_id=menu_categories.id WHERE orders.closed>='$date' AND orders.closed<DATE_ADD('$date',INTERVAL 172 HOUR) AND void='0'");

//$category_haystack = array();//store menu categories in this array


$menu_ids_used = array(); //what menu ids from the menu_categories.menu_id field are used
$index = 0;


$current_week_revenue = salesByCategory($cur_week_date,$track_data_id);//this return total sales by category for the entered date plus one week
$prior_week_revenue = salesByCategory($prior_week_date,$track_data_id);//this return total sales by category for the entered date plus one week
$prior_year_revenue = salesByCategory($prior_year_date,$track_data_id);//this return total sales by category for the entered date plus one week

/* Now build the map we can loop through to get a menu category name from the menu category id */
$menu_name_map = array();

//echo $current_week_revenue["sql"];
//exit();


//print_r($current_week_revenue["menu_category_map"]);
if( isset( $current_week_revenue["menu_category_map"])){
	foreach($current_week_revenue["menu_category_map"] as $menu_category_id => $menu_category_name){
			$menu_name_map[$menu_category_id] = $menu_category_name;
			
			/* And we can also display some data here while we're at it for efficiency's sake*/
			//echo $menu_category_name."- ".$current_week_revenue[$menu_category_id]."<br>";
			$sales_info = displaySalesByCategoryInfo($current_week_revenue,$prior_week_revenue,$prior_year_revenue,$menu_category_id,$menu_category_name);
	}//foreach
}
/*use $rev_per_labor_hr to calculate revenue per labor hour later on. I will update this field with javascript. This value depends on the labor hours calculated later */
$rev_per_labor_hr = array();

$cur_week_total_customers = $current_week_revenue["num_customers"];//how many people ordered something?
$cur_week_total_revenue = $current_week_revenue["total_revenue"];
$rev_per_labor_hr["current_week"]["revenue"] = $cur_week_total_revenue;
$cur_week_revenue_per_cust = "&#36;".number_format((($cur_week_total_revenue*1)/($cur_week_total_customers*1)),2);

$prior_week_total_customers = $prior_week_revenue["num_customers"];//how many people ordered something?
$prior_week_total_revenue = $prior_week_revenue["total_revenue"]; 
$rev_per_labor_hr["prior_week"]["revenue"] = $prior_week_total_revenue;
$prior_week_revenue_per_cust = "&#36;".number_format((($prior_week_total_revenue*1)/($prior_week_total_customers*1)),2);


$prior_year_total_customers = $prior_year_revenue["num_customers"];//how many people ordered something?
$prior_year_total_revenue = $prior_year_revenue["total_revenue"]; 
$rev_per_labor_hr["prior_year"]["revenue"] = $prior_year_total_revenue;
if( $prior_year_total_customers!= 0)
	$prior_year_revenue_per_cust = "&#36;".number_format((($prior_year_total_revenue*1)/($prior_year_total_customers*1)),2);
else
	$prior_year_revenue_per_cust=0;
/*Get number of race credits sold*/
$cur_week_races_sold = getNumRaceCredits($cur_week_date);
$prior_week_races_sold = getNumRaceCredits($prior_week_date );
$prior_year_races_sold = getNumRaceCredits($prior_year_date);

echo "
		<div class='row'>		
				<div class='rowElement re0'>
					Race Credits
				</div>
				<div class='rowElement re1'>
					$cur_week_races_sold
				</div>
				<div class='rowElement re2'>
					$prior_week_races_sold 
				</div>
				<div class='rowElement re3'>
					".number_format($prior_year_races_sold, 2, '.', '')."
				</div>
				<div class='rowElement re4'>
					&nbsp;
				</div>
				<div class='rowElement re6'>
					&nbsp;
				</div>
				<div class='rowElement re7'>
					&nbsp;
				</div>
				<div class='rowElement re8'>
					&nbsp;
				</div>
		</div><!--row-->
		<div class='clearRow'></div>
";

/*Get number of race credits sold*/
$cur_week_membership_sold = getNumMembershipCredits($cur_week_date);
$prior_week_membership_sold = getNumMembershipCredits($prior_week_date);
$prior_year_membership_sold = getNumMembershipCredits($prior_year_date);

echo "
		<div class='row'>		
				<div class='rowElement re0'>
					Membership Units
				</div>
				<div class='rowElement re1'>
					$cur_week_membership_sold
				</div>
				<div class='rowElement re2'>
					$prior_week_membership_sold
				</div>
				<div class='rowElement re3'>
					$prior_year_membership_sold
				</div>
				<div class='rowElement re4'>
					&nbsp;
				</div>
				<div class='rowElement re6'>
					&nbsp;
				</div>
				<div class='rowElement re7'>
					&nbsp;
				</div>
				<div class='rowElement re8'>
					&nbsp;
				</div>
		</div><!--row-->
		<div class='clearRow'></div>
";


echo "
		<div class='row'>		
				<div class='rowElement re0'>
					Revenue per Customer
				</div>
				<div class='rowElement re1'>
					$cur_week_revenue_per_cust
				</div>
				<div class='rowElement re2'>
					$prior_week_revenue_per_cust 
				</div>
				<div class='rowElement re3'>
					$prior_year_revenue_per_cust
				</div>
				<div class='rowElement re4'>
					&nbsp;
				</div>
				<div class='rowElement re6'>
					&nbsp;
				</div>
				<div class='rowElement re7'>
					&nbsp;
				</div>
				<div class='rowElement re8'>
					&nbsp;
				</div>
		</div><!--row-->
		<div class='clearRow'></div>
";

echo "
		<div class='row'>		
				<div class='rowElement re0'>
					Revenue per Labor Hour
				</div>
				<div class='rowElement re1'>
					<span id='rev_per_hour_cur_week'></span>
				</div>
				<div class='rowElement re2'>
					&nbsp;
				</div>
				<div class='rowElement re3'>
					&nbsp;
				</div>
				<div class='rowElement re4'>
					&nbsp;
				</div>
				<div class='rowElement re6'>
					&nbsp;
				</div>
				<div class='rowElement re7'>
					&nbsp;
				</div>
				<div class='rowElement re8'>
					&nbsp;
				</div>
		</div><!--row-->
		<div class='clearRow'></div>
";
echo "
		<div class='row'>		
				<div class='rowElement re0'>
					Membership Conversion &#37;
				</div>
				<div class='rowElement re1'>
					<span id='mem_conv_cur_wk'></span>
				</div>
				<div class='rowElement re2'>
					<span id='mem_conv_prior_wk'></span>
				</div>
				<div class='rowElement re3'>
					<span id='mem_conv_prior_yr'></span>
				</div>
				<div class='rowElement re4'>
					&nbsp;
				</div>
				<div class='rowElement re6'>
					&nbsp;
				</div>
				<div class='rowElement re7'>
					&nbsp;
				</div>
				<div class='rowElement re8'>
					&nbsp;
				</div>
		</div><!--row-->
		<div class='clearRow'></div>
		<br><br>
		<div class='bottomDivider'></div>
";






//print_r($current_week_revenue);
//print_r($prior_week_revenue);
//print_r($prior_year_revenue);
//print_r($menu_name_map);
?>



<div class="row">
<div class="row header">
		<div class="rowElement re0Header">
			Track Data
		</div>
		<div class="rowElement re1">
			Current Week
		</div>
		<div class="rowElement re2">
			Prior Week
		</div>
		<div class="rowElement re3">
			Prior Year
		</div>
		<div class="rowElement re4">
			Plan
		</div>
		<div class="rowElement re5">
			Trend
		</div>
		<div class="rowElement re6">
			Prior Year Trend
		</div>
		<div class="rowElement re7">
			&#37; to Goal
		</div>
		<div class="rowElement re9">
			&nbsp;
		</div>
</div><!--row-->
<div class="clearRow"></div>
<div class="divider"></div>
<?php
//$yay = calcTrackData($current_week_revenue,$prior_week_revenue,$prior_year_revenue,$cur_week_date,$track_data_id);
///echo "Track data id: $track_data_id<br>";
//exit();


$yay = calcTrackData($cur_week_date,$track_data_id);
$num_cust_array = displayTrackData($yay);
//displayTrackData($cur_week_date,$prior_week_date,$prior_year_date);
?>
<br>
<br>
 <div class="bottomDivider"></div>



<div class="row">
	<div class="row header">
			<div class="rowElement re0Header">
				Event Department
			</div>
			<div class="rowElement re1">
				Current Week
			</div>
			<div class="rowElement re2">
				Prior Week
			</div>
			<div class="rowElement re3">
				Prior Year
			</div>
			<div class="rowElement re4">
				Plan
			</div>
			<div class="rowElement re5">
				&nbsp
			</div>
	</div><!--row-->
<div class="clearRow"></div>
<div class="divider"></div>
<?php
displayEventDept($current_week_revenue,$prior_week_revenue,$prior_year_revenue,$event_department,$cur_week_date,$prior_week_date,$prior_year_date);
?>
<br>
<br>
 <div class="bottomDivider"></div>



<div class="row">
<div class="row header">
		<div class="rowElement re0Header">
			Track Efficiency
		</div>
		<div class="rowElement re1">
			Real Time
		</div>
		<div class="rowElement re2">
			Open Time
		</div>
		<div class="rowElement re3">
			Efficiency &#37;
		</div>
		<div class="rowElement re4">
			Races Sold
		</div>
		<div class="rowElement re5">
			Races
		</div>
		<div class="rowElement re6">
			<span style="font-size:75%;">Real Time per race</span>
		</div>
		<div class="rowElement re7">
			Plan
		</div>
		<div class="rowElement re9">
			Track Hrs
		</div>
</div><!--row-->
<div class="clearRow"></div>
<div class="divider"></div>

<?php
$track_data = calcTrackEfficiency($cur_week_date,$track_data_id);
displayTrackEfficiencyData($track_data);
//echo $track_data[$cur_week_date]["day_name"]."<br>";
?>

<br>
<br>
 <div class="bottomDivider"></div>

<?php
$labor_usage_data = calLaborUsage($cur_week_date);
echo '<div class= "divider"></div>';
$trk_hours = displayLaborUsage($labor_usage_data,$cur_week_date,$track_efficiency); //we have to use javascript to update this value to the track efficiency section of the report
?>

<br><br>
<div class="bottomDivider"></div>

<div class="row">
<div class="row header">
		<div class="rowElement re0Header">
			CSR Efficiency
		</div>
		<div class="rowElement re1">
			Tickets
		</div>
		<div class="rowElement re2">
			CSR Hour
		</div>
		<div class="rowElement re3">
			Ticket per Hr
		</div>
		<div class="rowElement re4">
			&nbsp;
		</div>
		<div class="rowElement re5">
			&nbsp;
		</div>
		<div class="rowElement re6">
			&nbsp;
		</div>
		<div class="rowElement re7">
			&nbsp;
		</div>
		<div class="rowElement re9">
			Actual
		</div>
</div><!--row-->

<div class="clearRow"></div>
<div class= "divider"></div>
<?php
$actual = displayCSREfficiency($labor_usage_data,$cur_week_date,$role_efficiency);
?>
<script type="text/javascript">
<?php

/* calculate membership conversion percent for performance indicator section*/
	$mem_conver_cur_wk = number_format(calcPercentage($cur_week_membership_sold,$num_cust_array["num_customers"]["current_week"]),2)."&#37;";
	$mem_conver_prior_wk = number_format(calcPercentage($prior_week_membership_sold,$num_cust_array["num_customers"]["prior_week"]),2)."&#37;";
	$mem_conver_prior_yr = number_format(calcPercentage($prior_year_membership_sold,$num_cust_array["num_customers"]["prior_week"]),2)."&#37;";
	
				
	
			
				
	


/* Calculate revenue per labor hour, he only wants the current year, Thank God */
$cur_week = "&#36;".number_format(($rev_per_labor_hr["current_week"]["revenue"]*1)/($labor_usage_data["total_labor_hours"]*1),2);


echo "
		document.getElementById('mem_conv_cur_wk').innerHTML = '$mem_conver_cur_wk';
		document.getElementById('mem_conv_prior_wk').innerHTML = '$mem_conver_prior_wk';
		document.getElementById('mem_conv_prior_yr').innerHTML = '$mem_conver_prior_yr';
		
		document.getElementById('role_efficiency').value = '$role_efficiency';
		document.getElementById('track_efficiency').value = '$track_efficiency';	
		document.getElementById('event_department').value = '$event_department';
		document.getElementById('track_data').value = '$track_data_id';	
		
		document.getElementById('start_year').value = '$date_year';
		document.getElementById('start_month').value = '$date_month';	
		document.getElementById('start_day').value = '$date_day';		
		
		document.getElementById('csr_effic_1').innerHTML = '$actual';
		document.getElementById('trk_hours_0').innerHTML = '$trk_hours';
		
		document.getElementById('rev_per_hour_cur_week').innerHTML = '$cur_week';
";
?>
</script>

<?php
//echo $current_week_revenue["sql"];
?>
