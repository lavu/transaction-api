<style type="text/css">
.row{
	margin-left:150px;
}
.header{
	font-weight: 700;
}
.rowElement{
	float:left;
	border-bottom: 1px solid #666;
	padding: 5px 0 5px 0;
}
.re1{
	width:150px;
}
.re2{
	width:100px;
	
}
.re3{
	width:200px;
	
}
.re4{
	width:100px;
	
}

.clearRow{
	clear:both;
}
</style>

<script type='text/javascript'>
function showPartInfo(part_id){
		link = "index.php?mode=reports_p2r_parts_cost_by_time&part="+part_id;
		window.location = link;
}//showPartInfo()
</script>
<h2>Part Costs by Time</h2>
<?php

$sql         			 = "SELECT DISTINCT(lk_parts_received.part_id) AS part_id, lk_parts.name AS name, lk_parts.custom_part_id AS custom_part_id FROM lk_parts_received LEFT JOIN lk_parts ON lk_parts.id=lk_parts_received.part_id";
$sql					.= " WHERE lk_parts_received.part_id!='0' AND lk_parts.name!='' AND lk_parts.custom_part_id!=''";

$part_ids_res   = lavu_query($sql);

echo "<select name='select_part' id='select_part' onchange='showPartInfo(this.value);'>";
echo "<option value=''>Choose a part</option>";
while ($part_row = mysqli_fetch_assoc($part_ids_res)){
		$part_id 				= 	$part_row['part_id'];
		$part_desc 			= 	$part_row['name'];
		$custom_part_id = 	$part_row['custom_part_id'];
		echo "<option value='$part_id'>$custom_part_id - $part_desc</option>";
} //while
echo "</select>";
echo "<br/><br/>";


$selected_part = reqvar("part");
if ($selected_part){
		//$vars 		= array();
		//$vars["selected_part"] 	= $selected_part;
		//echo "vars[0]. ".$vars[0]."<br>";
		$sql         			 = "SELECT lk_parts_received.datetime AS datetime, lk_parts_received.cost_per_one AS cost_per_one, lk_parts.name AS description, lk_parts.custom_part_id AS custom_part_id FROM lk_parts_received";
		$sql					.= " LEFT JOIN lk_parts ON lk_parts.id=lk_parts_received.part_id";
		$sql					.= " WHERE lk_parts.id='$selected_part' AND lk_parts_received.cost_per_one!='' ORDER BY lk_parts_received.datetime DESC";
		
		
		echo 		   "<div class='row header'>";
		echo 		   "<div class='rowElement re1'>Date (mm-dd-yyyy)</div>";
		//echo 		   "<div class='rowElement re2'>Custom ID</div>";
		//echo 		   "<div class='rowElement re3'>Description</div>";
		echo 		   "<div class='rowElement re4'>Cost per unit</div>";
		echo 		   "<div class='clearRow'></div>";
		echo 		   "</div>";
		echo 		   "<div class='clearRow'></div>";
		$result 	= lavu_query($sql,$vars);
		//echo "numrow: ".mysqli_num_rows($result)."<br>";
		while ($row = mysqli_fetch_assoc($result)){
				$datetime 			= $row['datetime'];
				$cost_per_one 	= $row['cost_per_one'];
				$description		= $row['description'];
				$custom_part_id = $row['custom_part_id'];
				
				echo 		   "<div class='row'>";
				echo 		   "<div class='rowElement re1'>".formatDate($datetime)."</div>";
				//echo 		   "<div class='rowElement re2'>$custom_part_id</div>";
				//echo 		   "<div class='rowElement re3'>$description</div>";
				echo 		   "<div class='rowElement re4'>&#36;".number_format($cost_per_one,2)."</div>";
				echo 		   "<div class='clearRow'></div>";
				echo 		   "</div>";
				echo 		   "<div class='clearRow'></div>";
 
		} //while
}//if


?>
<script type='text/javascript'>
document.getElementById("select_part").value = '<?php echo $selected_part;?>';
</script>

<?php
function formatDate($datetime){
		
		$datetime_arr 		= explode(" ",$datetime);
		$date_str 			= $datetime_arr[0];
		$time_str 	 		= $datetime_arr[1];
		
		$date_arr				= explode("-",$date_str);
		$year					= $date_arr[0];
		
		$month				= $date_arr[1];
		$day						= $date_arr[2];
		$formated_date 	= $month."-".$day."-".$year." ".$time_str;
		
		return $formated_date;
}//formatDate()
?>