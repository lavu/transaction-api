<?php
	ob_implicit_flush(true);
	$_GET['user'] = 'all';

	// prepare to load the file
	ob_start();
	// load the file
	require_once(dirname(__FILE__).'/time_cards_obj.php');
	// cleanup
	$s_contents = ob_get_contents();
	ob_end_clean();

	class PAYMENTS_IN_VS_LABOR {

		public static $o_total_labor = NULL;
		public static $o_date_chooser = NULL;
		public static $o_total_payments = NULL;
		public static $a_user_labor_stats = NULL;

		// set the statistics on all users for the time period
		// depends on the global variable $info_array set from within 'time_cards.php'
		// @return: stores values in PAYMENTS_IN_VS_LABOR::$a_user_labor_stats
		public static function set_user_stats() {
			global $info_array;

			$a_to_remove = array();
			foreach($info_array as $k=>$v) {
				unset($info_array[$k]['html']);
				unset($info_array[$k]['details']);
				if ((int)$info_array[$k]['total_paid'] == 0)
					$a_to_remove[] = $k;
			}
			foreach($a_to_remove as $k)
				unset($info_array[$k]);

			echo "<div style='text-align:left;'><pre>";
			print_r($info_array);
			echo "</pre></div>";

			self::$a_user_labor_stats = $info_array;
		}

		// get the statistics on all users for the time period
		// @a_retval: if not null, then this will be used to return the array
		// @return: if a_retval is NULL, then the array will be returned
		public static function get_user_stats(&$a_retval = NULL) {
			if (!isset(self::$a_user_labor_stats))
				set_user_stats();

			if ($a_retval !== NULL)
				$a_retval = self::$a_user_labor_stats;
			else
				return self::$a_user_labor_stats;
		}

		// get the total labor costs
		// @return the total labor cost, as calculated by the time cards report, as an object with the following fields:
		//     total: the totals for all users
		//     total->total_hours: number of hours clocked
		//     total->regular_hours: number of regular hours clocked, subset of total_hours
		//     total->overtime_hours: number of overtime hours clocked, subset of total_hours
		//     total->doubletime_hours: number of doubletime hours clocked, subset of total_hours
		//     if ($modules->hasModule("employees.payrates") && isset($total_paid))
		//         total->total_paid, total->regular_paid, total->overtime_paid, total->doubletime_paid
		public static function get_total_labor_cost() {
			$o_retval = new stdClass();
			global $time_cards_results;
			$data = array();
			foreach( $time_cards_results['users'] as $user_id => $user ){
				//id
				//work_weeks
				//processed
				//userfound
				//username
				//full_name
				//access
				//locationid
				//lavu_admin
				//deleted
				//classes
				//payrates
				//clocked_in
				//timePunches

				$payrates = $user->payrates;
				$classes = $user->classes;
				$full_name = $user->full_name;
				$user_id = $user->id;
				$punches = array();
				//foreach( $user->work_weeks as $work_week_start_ts => $work_week ){
					//week
					//next_week
					//work_days
					//seconds
					//days
					//processed
					//time_punches
					foreach($user->timePunches as $punch_id => $punch ){
						//id
						//locationid
						//userid
						//time_in
						//time_out
						//clocked_out
						//clocked_out_via_app
						//delete
						//rate_id
						//in_requested_time_period
						//role_id
						//start_week
						//end_week
						//contribution
						$punches[$punch->id] = $punch;
					}
				//}

				foreach( $punches as $punch_id => $punch ){

					$report_start_time = TimeCardAccountSettings::settings()->start_time;
					$report_end_time = TimeCardAccountSettings::settings()->end_time;

					if( !(($punch->time_out >= $report_start_time && $punch->time_out <= $report_end_time) ||
						($punch->time_in >= $report_start_time && $punch->time_in <= $report_end_time) ||
						($punch->time_in <= $report_start_time && $punch->time_out >= $report_end_time)) ||
						!$punch->in_requested_time_period ){
						continue;
					}

					$role = $classes->getRole( $punch->role_id );
					$row_result = array(
						$user_id,
						$full_name,
						$punch->time_in,//'m/d h:i A'
						($punch->clocked_out?'':'e') . $punch->time_out
					);
					$regular_seconds = 0;
					$regular_rate = 0;
					$overtime_rate = 0;
					$doubletime_rate = 0;

					foreach( $punch->contribution[REGULAR] as $role_id => $seconds ){
						$regular_seconds += $seconds;
						static $it = 0;

						if( $punch->payrate ){
							$regular_rate = $punch->payrate / 3600.0;
						} else if( $role_id || $role_id == 0 ){
							$regular_rate = $payrates->get_rate_rate( $role_id ) / 3600.0;
						}
					}

					$overtime_seconds = 0;
					foreach( $punch->contribution[OVERTIME] as $role_id => $seconds ){
						$overtime_seconds += $seconds;
						if( $punch->payrate ){
							$overtime_rate = $punch->payrate / 3600.0;
						} else if( $role_id || $role_id == 0  ){
							$overtime_rate = $payrates->get_rate_rate( $role_id ) / 3600.0;
						}
					}

					$double_time_seconds = 0;
					foreach( $punch->contribution[DOUBLE_TIME] as $role_id => $seconds ){
						$double_time_seconds += $seconds;
						if( $punch->payrate ){
							$doubletime_rate = $punch->payrate / 3600.0;
						} else if( $role_id || $role_id == 0  ){
							$doubletime_rate = $payrates->get_rate_rate( $role_id ) / 3600.0;
						}
					}

					$total_seconds = $regular_seconds + $overtime_seconds + $double_time_seconds;

					$regular_paid = $regular_seconds * $regular_rate * 1.0;
					$overtime_paid = $overtime_seconds * $overtime_rate * 1.5;
					$double_time_paid = $double_time_seconds * $doubletime_rate * 2.0;
					$total_paid = $regular_paid + $overtime_paid + $double_time_paid;

					$row_result[] = $total_seconds;
					$row_result[] = $regular_seconds;
					$row_result[] = $overtime_seconds;
					$row_result[] = $double_time_seconds;

					$row_result[] = $total_paid;
					$row_result[] = $regular_paid;
					$row_result[] = $overtime_paid;
					$row_result[] = $double_time_paid;
					$row_result[] = $punch_id;
					$data[] = $row_result;
				}
			}
			$o_retval->total = new stdClass();
			$o_retval->total->total_hours = 0;
			$o_retval->total->regular_hours = 0;
			$o_retval->total->overtime_hours = 0;
			$o_retval->total->doubletime_hours = 0;
			$o_retval->total->total_paid = 0;
			$o_retval->total->regular_paid = 0;
			$o_retval->total->overtime_paid = 0;
			$o_retval->total->doubletime_paid = 0;
			foreach( $data as $emp_entry ){
				$o_retval->total->total_hours += $emp_entry[4];
				$o_retval->total->regular_hours += $emp_entry[5];
				$o_retval->total->overtime_hours += $emp_entry[6];
				$o_retval->total->doubletime_hours += $emp_entry[7];
				$o_retval->total->total_paid += $emp_entry[8];
				$o_retval->total->regular_paid += $emp_entry[9];
				$o_retval->total->overtime_paid += $emp_entry[10];
				$o_retval->total->doubletime_paid += $emp_entry[11];
			}

			// these are from the time_cards report
			/*global $modules;
			global $t_hours;
			global $t_reg_hours;
			global $t_ot_hours;
			global $t_dt_hours;
			global $total_paid;
			global $t_reg_hours_paid;
			global $t_ot_hours_paid;
			global $t_dt_hours_paid;

			if (isset(self::$o_total_labor) && self::$o_total_labor !== NULL) {
				return self::$o_total_labor;
			}

			$b_has_payrates = FALSE;

			// get the necessary contents
			$o_retval = new stdClass();
			$o_retval->total = new stdClass();
			$o_retval->total->total_hours = $t_hours;
			$o_retval->total->regular_hours = $t_reg_hours;
			$o_retval->total->overtime_hours = $t_ot_hours;
			$o_retval->total->doubletime_hours = $t_dt_hours;
			if ($modules->hasModule("employees.payrates") && isset($total_paid)) {
				$b_has_payrates = TRUE;
				$o_retval->total->total_paid = $total_paid;
				$o_retval->total->regular_paid = $t_reg_hours_paid;
				$o_retval->total->overtime_paid = $t_ot_hours_paid;
				$o_retval->total->doubletime_paid = $t_dt_hours_paid;
			}*/

			self::$o_total_labor = $o_retval;
			return $o_retval;
		}

		// simple function to reduce complexity of code and increase performance
		public static function get_date_chooser_obj() {
			/*if (!isset(self::$o_date_chooser) || self::$o_date_chooser !== NULL) {
				self::$o_date_chooser = get_date_chooser_obj();
				self::$o_date_chooser->use_hour = isset($_GET['hour']) ? $_GET['hour'] : (int)date('H', strtotime(localize_datetime(date('Y-m-d H:i:s'), NULL)));
				if (self::$o_date_chooser->use_hour == 'now') {
					self::$o_date_chooser->hour_is_now = TRUE;
					self::$o_date_chooser->use_hour = (int)date("H");
				} else {
					self::$o_date_chooser->hour_is_now = FALSE;
					self::$o_date_chooser->use_hour = (int)self::$o_date_chooser->use_hour;
				}
				self::$o_date_chooser->hour_is_set = isset($_GET['hour']) ? TRUE : FALSE;
			}
			return self::$o_date_chooser;*/
			//return TimeCardUserFunctions::get_date_chooser_obj( $_GET['mode'] );
			if (!isset(self::$o_date_chooser) || self::$o_date_chooser !== NULL) {
				self::$o_date_chooser = TimeCardUserFunctions::get_date_chooser_obj( $_GET['mode'] );
				self::$o_date_chooser->use_hour = isset($_GET['hour']) ? $_GET['hour'] : (int)date('H', strtotime(localize_datetime(date('Y-m-d H:i:s'), NULL)));
				if (self::$o_date_chooser->use_hour == 'now') {
					self::$o_date_chooser->hour_is_now = TRUE;
					self::$o_date_chooser->use_hour = (int)date("H");
				} else {
					self::$o_date_chooser->hour_is_now = FALSE;
					self::$o_date_chooser->use_hour = (int)self::$o_date_chooser->use_hour;
				}
				self::$o_date_chooser->hour_is_set = isset($_GET['hour']) ? TRUE : FALSE;
			}

			return self::$o_date_chooser;
		}

		// draws the header for this report
		// @return: html for the header
		public static function draw_header() {

			// initialize variables
			$s_retval = '';
			$s_retval .= speak('Report') . ': <b>' . speak('Payments In vs. Labor') . ' </b><br /><br />';

			// get the necessary contents
			$o_date_chooser = self::get_date_chooser_obj();
			$s_select_clause = trim($o_date_chooser->select_clause);

			// update the select clause to choose either the end date or end hour
			$s_date = $o_date_chooser->use_date;
			$s_hour = $o_date_chooser->use_hour;
			$s_hour_get = $o_date_chooser->hour_is_set ? "&hour=$s_hour" : "";
			$s_hour_code = $o_date_chooser->hour_is_set ? "
				<script type='text/javascript'>
					setTimeout(function() {
						$('#selectByDayOrHour').val('hour');
						$('#date2day').hide();
						$('#date2hour').show();
					}, 100);
				</script>" : "";
			// refresh every five minutes if by hour and hour is now
			if ($o_date_chooser->hour_is_now && $o_date_chooser->hour_is_set)
				$s_hour_code .= "
				<script type='text/javascript'>
					setTimeout(function() {
						window.location='index.php?".$o_date_chooser->forward_vars."&hour=now';
					}, 300000);
				</script>";
			echo '<pre style="text-align: left;">' . htmlspecialchars( $a_select_clause ) . '</pre>';
			$a_selects = explode('<select', $s_select_clause);
			$a_date1_selects = explode("&date=", $a_selects[1]);
			$s_select_clause;// = $a_selects[0]."
				//<select".$a_date1_selects[0]."$s_hour_get&date=".$a_date1_selects[1];
			if (DEV)
				$s_select_clause .= "
				<select id='selectByDayOrHour' onchange='
					a= null;
					b = null;
					if (this.value == \"day\") {
						a = $(\"#date2day\");
						b = $(\"#date2hour\");
					} else {
						a = $(\"#date2hour\");
						b = $(\"#date2day\");
					}
					a.show();
					b.hide();
					setTimeout(function() {
						a.change();
					}, 100);
					'".(DEV ? '' : "style='display:none;'").">
					<option value='day'>By Day</option>
					<option value='hour'>By Hour</option>
				</select>";
			$s_select_clause .= "
				<select id='date2day' style='display: none;'></select>
				<select id='date2hour' onchange='window.location=\"index.php?".$o_date_chooser->forward_vars."&hour=\"+this.value;' style='".(($o_date_chooser->hour_is_set)?"":"display:none;")."'>
					<option value='now'>This Hour</option>";
			for($i = 0; $i < 24; $i++) {
				$i_hour = $i % 24;
				$s_selected = ($i == (int)$s_hour && !$o_date_chooser->hour_is_now) ? " SELECTED" : "";
				$s_select_clause .= "
					<option value='$i_hour'$s_selected>".date("ga",strtotime("2013-01-01 $i_hour:00:00"))."</option>";
			}
			$s_select_clause .= "
				</select>
				$s_hour_code";

			$s_retval .= $s_select_clause;
			return $s_retval;
		}

		// returns an object with the total payments for the given date
		public static function get_total_payments() {
			global $data_name;
			global $location_info;

			if (isset(self::$o_total_payments) && self::$o_total_payments !== NULL)
				return self::$o_total_payments;

			// get the dates
			$o_date_chooser = self::get_date_chooser_obj();
			$report_settings = TimeCardAccountSettings::settings();
			$s_hour_start = '00:00:00';
			$s_hour_end = '99:99:99';
			if ($o_date_chooser->hour_is_set) {
				$s_hour_start = str_pad($o_date_chooser->use_hour, 2, '0', STR_PAD_LEFT).':00:00';
				$s_hour_end = str_pad($o_date_chooser->use_hour+1, 2, '0', STR_PAD_LEFT).':00:00';
			}
			$current_tz = date_default_timezone_get();
			if( !empty( $location_info['timezone'] )){
				date_default_timezone_set( $location_info['timezone'] );
			}
			$s_date_start = date('Y-m-d H:i:s', $report_settings->start_time);//$o_date_chooser->use_date.' '.$s_hour_start;
			$s_date_end = date('Y-m-d H:i:s', $report_settings->end_time);//$o_date_chooser->use_date2.' '.$s_hour_end;

			date_default_timezone_set( $current_tz );
			// do this rediculous query
			$s_query_string = "select SQL_NO_CACHE IF(`cc_transactions`.`order_id` LIKE \"Paid %\",0,IF(`cc_transactions`.`voided`='1',0,IF(`cc_transactions`.`action`='Refund', (0 - sum(`cc_transactions`.`total_collected`)), sum(`cc_transactions`.`total_collected`)))) AS `field0`,`cc_transactions`.`datetime` AS `field1`,`cc_transactions`.`loc_id` AS `field2`,`orders`.`void` AS `field3`,IF(`cc_transactions`.`for_deposit`>0,'deposit','') AS `field4`,`orders`.`location_id` AS `field5`,`orders`.`ag_cash` AS `field6`,`orders`.`ag_card` AS `field7`,`orders`.`gratuity` AS `field8`,`cc_transactions`.`transtype` AS `field9`,`cc_transactions`.`order_id` AS `field10`,`orders`.`order_id` AS `field11`,IF(`cc_transactions`.`voided`='1',0,IF(`cc_transactions`.`action`='Refund', (0 - sum(`cc_transactions`.`total_collected`)), sum(`cc_transactions`.`total_collected`))) AS `field12`,IF(`cc_transactions`.`voided`='1',0,IF(`cc_transactions`.`action`='Refund', (0 - sum(`cc_transactions`.`tip_amount`)), sum(`cc_transactions`.`tip_amount`))) AS `field13`,IF(`cc_transactions`.`voided`='1',0,IF(`cc_transactions`.`action`='Refund', (0 - sum(`cc_transactions`.`total_collected` + `cc_transactions`.`tip_amount`)), sum(`cc_transactions`.`total_collected` + `cc_transactions`.`tip_amount`))) AS `field14`,IF(`cc_transactions`.`pay_type`='Card',IF(`cc_transactions`.`voided`='1',0,IF(`cc_transactions`.`action`='Refund',(0 - sum(`cc_transactions`.`amount` + `cc_transactions`.`tip_amount`)), sum(`cc_transactions`.`amount` + `cc_transactions`.`tip_amount`))),0) AS `field15`,`cc_transactions`.`pay_type` AS `field16`,`cc_transactions`.`card_type` AS `field17`,IF(`cc_transactions`.`order_id`='Paid Out','Paid Out',IF(`cc_transactions`.`order_id`='Paid In','Paid In',`cc_transactions`.`action`)) AS `field18`,IF(`cc_transactions`.`voided`='1',sum(`cc_transactions`.`total_collected`),0) AS `field19` from `[database]`.`cc_transactions` LEFT JOIN `[database]`.`orders` ON `cc_transactions`.`order_id` = `orders`.`order_id` where `cc_transactions`.`datetime` >= '[date_start]' and `cc_transactions`.`datetime` <= '[date_end]' and `cc_transactions`.`loc_id` = '[location_id]' and `orders`.`void` = 0 and (`orders`.`location_id` = `cc_transactions`.`loc_id` OR `cc_transactions`.`order_id` = \"Paid In\" OR `cc_transactions`.`order_id` = \"Paid Out\") and `cc_transactions`.`transtype` != \"AddTip\" and (`cc_transactions`.`action` = \"Sale\" OR `cc_transactions`.`action` = \"Refund\") and `cc_transactions`.`voided` != 1 group by concat(`cc_transactions`.`pay_type`,' ',`cc_transactions`.`card_type`,`cc_transactions`.`action`,`cc_transactions`.`voided`,IF(`cc_transactions`.`order_id` LIKE 'Paid%','PaidInOut',''))";
			$a_query_vars = array('database'=>'poslavu_'.$data_name.'_db', 'location_id'=>$location_info['id'], 'date_start'=>$s_date_start, 'date_end'=>$s_date_end);
			$payments_query = rpt_query($s_query_string, $a_query_vars);
			if ($payments_query === FALSE || mysqli_num_rows($payments_query) === 0)
				return NULL;

			$payments = NULL;
			while ($row = mysqli_fetch_assoc($payments_query)) {
				if ($payments === NULL) {
					foreach($row as $k=>$v)
						$payments[$k] = $v;
				} else {
					foreach($row as $k=>$v)
						$payments[$k] += $v;
				}
			}

			$o_retval = new stdClass();
			$o_retval->sales_collected = $payments['field0']*1;
			$o_retval->collected = $payments['field12']*1;
			$o_retval->tips = $payments['field13']*1;
			$o_retval->total = $payments['field14']*1;
			$o_retval->total_card = $payments['field15']*1;

			self::$o_total_payments = $o_retval;
			return $o_retval;
		}

		// draws the table representing the report
		public static function draw_report() {

			global $location_info;
			global $s_cant_display_error;

			// get the necessary data and check that it loaded correctly
			$o_total_payments = self::get_total_payments();
			$o_total_labor = self::get_total_labor_cost();
			$o_total_labor = $o_total_labor->total;
			if (false && !isset($o_total_labor->total_paid)) {
				$s_cant_display_error = "This report is unavailable.";
				return $s_cant_display_error;
			}
			if ($o_total_labor->total_paid == 0) {
				$o_date_chooser = self::get_date_chooser_obj();
				$s_cant_display_error = "There are either no <a href='index.php?mode=reports_time_cards&date=".$o_date_chooser->use_date."&date2=".$o_date_chooser->use_date2."&user=all'>clock punches</a> for this time or you don't have any <a href='index.php?mode=edit_users'>payrates</a> set.";
				return $s_cant_display_error;
			}
			if ($o_total_payments === NULL) {
				$s_cant_display_error = speak("There were no payments made during this time.");
				return $s_cant_display_error;
			}

			// the table class
			$s_class = '
				<style scoped>
					table.summary_table {
						border-collapse: collapse;
					}
					table.summary_table th {
						border-bottom: 1px solid black;
						padding: 0 5px 0 5px;
					}
					table.summary_table td.money {
						text-align: right;
					}
					table.summary_table td {
						padding: 0 5px 0 5px;
					}
				</style>';
			// the total payments table
			$s_payments_table = '';
			$s_payments_table .= '<table class="summary_table"><thead><tr>
				<th>Sales Collected</th>
				<th>Collected</th>
				<th>Tips</th>
				<th>Total</th>
				<th>Total Card</th>
				</tr></thead><tbody><tr>
				<td class="money">'.display_money($o_total_payments->sales_collected,$location_info).'</td>
				<td class="money">'.display_money($o_total_payments->collected,$location_info).'</td>
				<td class="money">'.display_money($o_total_payments->tips,$location_info).'</td>
				<td class="money">'.display_money($o_total_payments->total,$location_info).'</td>
				<td class="money">'.display_money($o_total_payments->total_card,$location_info).'</td>
				</tr></tbody></table>';

			// the total labor table
			$s_labor_table = '';
			$s_labor_table .= '<table class="summary_table"><thead><tr>
				<th>Total Hrs</th>
				<th>Reg Hrs</th>
				<th>OT Hrs</th>
				<th>DT Hrs</th>
				<th>Total Paid</th>
				<th>Reg Paid</th>
				<th>OT Paid</th>
				<th>DT Paid</th>
				</tr></thead><tbody><tr>
				<td>'.number_format($o_total_labor->total_hours/3600,2,".","").'</td>
				<td>'.number_format($o_total_labor->regular_hours/3600,2,".","").'</td>
				<td>'.number_format($o_total_labor->overtime_hours/3600,2,".","").'</td>
				<td>'.number_format($o_total_labor->doubletime_hours/3600,2,".","").'</td>
				<td class="money">'.display_money($o_total_labor->total_paid,$location_info).'</td>
				<td class="money">'.display_money($o_total_labor->regular_paid,$location_info).'</td>
				<td class="money">'.display_money($o_total_labor->overtime_paid,$location_info).'</td>
				<td class="money">'.display_money($o_total_labor->doubletime_paid,$location_info).'</td>
				</tr></tbody></table>';

			$s_retval = '';
			$s_retval .= $s_class;
			$s_retval .= '<b>Total Payments</b><br />';
			$s_retval .= $s_payments_table;
			$s_retval .= '<br /><br /><b>Total Labor</b><br />';
			$s_retval .= $s_labor_table;

			return $s_retval;
		}

		public static function draw_graph() {

			global $location_info;

			// get the necessary data and check that it loaded correctly
			$o_total_labor = self::get_total_labor_cost();
			$o_total_payments = self::get_total_payments();
			$o_total_labor = $o_total_labor->total;
			if (!isset($o_total_labor->total_paid))
				return "";
			if ($o_total_labor->total_paid == 0)
				return "";
			if ($o_total_payments === NULL)
				return "";


			$remaining = ($o_total_payments->sales_collected + $o_total_payments->tips) - ( $o_total_labor->regular_paid + $o_total_labor->overtime_paid + $o_total_labor->doubletime_paid);
			$a_categories = array(speak('Payments vs Labor'), speak('Total Payments and Labor'), speak('Category'));
			$a_table_info = array(0, (int)$o_total_payments->sales_collected, 'Total Payments', 0, (int)$o_total_payments->tips, 'Total Tips', (int)$remaining, 0, 'Remaining Sales + Tips', (int)$o_total_labor->regular_paid,(int)$o_total_labor->regular_paid, 'Regular Paid', (int)$o_total_labor->overtime_paid,(int)$o_total_labor->overtime_paid, 'Overtime Paid', (int)$o_total_labor->doubletime_paid,(int)$o_total_labor->doubletime_paid, 'Doubletime Paid');

			$s_graph = '';

			require_once dirname(__FILE__) . '/graphData.php';
			$s_graph .= "<div id='graph' style='width:600px;height:300px; margin:0 auto;'></div>";
			$s_chart_retval = chart($a_categories, $a_table_info);
			$s_graph .= $s_chart_retval;

			return $s_graph;
		}
	}

	if ($in_lavu) {

		$draw = '';
		$s_report = PAYMENTS_IN_VS_LABOR::draw_report();
		$s_header = PAYMENTS_IN_VS_LABOR::draw_header();
		$s_graph .= PAYMENTS_IN_VS_LABOR::draw_graph();

		$s_start = '<div id="payments_in_vs_labor_start" style="display:none;"></div>';
		$s_end = '<div id="payments_in_vs_labor_end" style="display:none;"></div>';

		$draw .= $s_start.$s_graph.'<br /><br />'.$s_header.'<br /><br /><br />'.$s_report.$s_end;

		echo $draw;
	}
?>
