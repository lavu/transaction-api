(function(){
	if( !Lavu.Reports ){
		Lavu.Reports = {};
	}

	(function(){
		function ICalculatable(){
			Object.call( this );
		}

		function calculate( obj ){
			return 0;
		}

		Lavu.Reports.ICalculatable = ICalculatable;
		Lavu.Reports.ICalculatable.prototype.constructor = ICalculatable;
		Lavu.Reports.ICalculatable.prototype = {};
		Lavu.Reports.ICalculatable.prototype.calculate = calculate;
	})();

	(function(){
		function Constant( value ){
			Lavu.Reports.ICalculatable.call( this );
			this._value = value;
		}

		function calculate( obj ){
			return this._value;
		}

		function toString(){
			return this._value.toString();
		}

		Lavu.Reports.Constant = Constant;
		Lavu.Reports.Constant.prototype.constructor = Constant;
		Lavu.Reports.Constant.prototype = new Lavu.Reports.ICalculatable();
		Lavu.Reports.Constant.prototype.calculate = calculate;
		Lavu.Reports.Constant.prototype.toString = toString;
	})();

	(function(){
		var absoluteMaxPrecision = 20;
		function IFormatable(){
			Object.call( this );
			this._numberOfDecimals = 2;
			this._decimalChar = ".";
			this._thousandsChar = ",";
			this._prefix = "";
			this._postfix = "";
			this._accountingNegative = false;
		}

		function setAccountingNegation( accountingNegation ){
			if( typeof accountingNegation != "boolean" ){
				return false;
			}

			this._accountingNegative = accountingNegation;
			return true;
		}

		function setNumberOfDecimals( numberOfDecimals ){
			if( typeof numberOfDecimals != "number" ){
				return false;
			}
			this._numberOfDecimals = Math.floor(Math.min( absoluteMaxPrecision, numberOfDecimals ));
			return true;
		}

		function setDecimalChar( decimalChar ){
			if( typeof decimalChar != "string" ){
				return false;
			}

			this._decimalChar = decimalChar;
			return true;
		}

		function setThousandsChar( thousandsChar ){
			if( typeof thousandsChar != "string" ){
				return false;
			}

			this._thousandsChar = thousandsChar;
			return true;
		}

		function setPrefix( prefix ){
			if( typeof prefix != "string" ){
				return false;
			}

			this._prefix = prefix;
			return true;
		}

		function setPostFix( postfix ){
			if( typeof postfix != "string" ){
				return false;
			}

			this._postfix = postfix;
			return true;
		}

		function toFormattedString( value ){
			var decimalPrecision = Math.pow(10,absoluteMaxPrecision);
			var preDecimalDigits = Math.floor( Math.abs(value) );
			var postDecimalDigits = Math.floor((Math.abs(value) - Math.abs(preDecimalDigits)) * decimalPrecision);
			/**
			 * Contains a String Representation of the the post decimal digits.  Should have a length
			 * of @variable	absoluteMaxPrecision at all times.
			 *
			 * Ensures we have a length of absoluteMaxPrecision
			 *
			 * @type {string}
			 */
			postDecimalDigits = postDecimalDigits.toString();
			if( postDecimalDigits.length < absoluteMaxPrecision ){
				while( postDecimalDigits.length < absoluteMaxPrecision ){
					postDecimalDigits = '0' + postDecimalDigits;
				}
			}

			var result = this._prefix;
			var num = "";

			var preDecimal = Math.abs(preDecimalDigits).toString();
			while( preDecimal.length ){
				var numDigits = preDecimal.length % 3;
				num += preDecimal.substr(0,numDigits);
				preDecimal = preDecimal.substr( numDigits );
				if(preDecimal.length){
					num += this._thousandsChar;
				}
			}

			if(this._numberOfDecimals){
				num += this._decimalChar;
				num += postDecimalDigits.substr(0, this._numberOfDecimals);
			}

			if( value < 0 ){
				if( this._accountingNegative ){
					num = "(" + num + ")";
				} else {
					result = "-" + result;
				}
			}
			result += num;

			result += this._postfix;

			return result;
		}

		Lavu.Reports.IFormatable = IFormatable;
		Lavu.Reports.IFormatable.prototype.constructor = IFormatable;
		Lavu.Reports.IFormatable.prototype = {};
		Lavu.Reports.IFormatable.prototype.toFormattedString = toFormattedString;
		Lavu.Reports.IFormatable.prototype.setAccountingNegation = setAccountingNegation;
		Lavu.Reports.IFormatable.prototype.setDecimalChar = setDecimalChar;
		Lavu.Reports.IFormatable.prototype.setThousandsChar = setThousandsChar;
		Lavu.Reports.IFormatable.prototype.setPrefix = setPrefix;
		Lavu.Reports.IFormatable.prototype.setPostFix = setPostFix;
	})();

	(function(){
		function FormatPercentage(){
			Lavu.Reports.IFormatable.call( this );
			this._postfix = "%";
		}

		/**
		 * [toFormattedString description]
		 * @param  {[type]} value [description]
		 * @return {[type]}       [description]
		 */
		function toFormattedString( value ){
			var percentageValue = value * 100;
			Lavu.Reports.IFormatable.prototype.toFormattedString( percentageValue );
		}

		Lavu.Reports.FormatPercentage = FormatPercentage;
		Lavu.Reports.FormatPercentage.prototype.constructor = FormatPercentage;
		Lavu.Reports.FormatPercentage.prototype = new Lavu.Reports.IFormatable();
		Lavu.Reports.FormatPercentage.prototype.toFormattedString = toFormattedString;
	})();

	(function(){
		function FormatUSD(){
			Lavu.Reports.IFormatable.call( this );
			this.setPrefix( "$" );
			this.setDecimalChar( "." );
			this.setThousandsChar( "," );
		}

		Lavu.Reports.FormatUSD = FormatUSD;
		Lavu.Reports.FormatUSD.prototype.constructor = FormatUSD;
		Lavu.Reports.FormatUSD.prototype = new Lavu.Reports.IFormatable();
	})();

	(function(){
		function FormatUSDAccounting(){
			Lavu.Reports.FormatUSD.call( this );
			this.setAccountingNegation( true );
		}

		Lavu.Reports.FormatUSDAccounting = FormatUSDAccounting;
		Lavu.Reports.FormatUSDAccounting.prototype.constructor = FormatUSDAccounting;
		Lavu.Reports.FormatUSDAccounting.prototype = new Lavu.Reports.FormatUSD();
	})();

	(function(){
		function FormatCount(){
			Lavu.Reports.IFormatable.call( this );
			this.setNumberOfDecimals( 0 );
		}

		Lavu.Reports.FormatCount = FormatCount;
		Lavu.Reports.FormatCount.prototype.constructor = FormatCount;
		Lavu.Reports.FormatCount.prototype = new Lavu.Reports.IFormatable();
	})();

	(function(){
		function IEquationOperator(){
			Object.call( this );
		}

		function operate( evaluate1, evaluate2 ){
			return 0;
		}

		Lavu.Reports.IEquationOperator = IEquationOperator;
		Lavu.Reports.IEquationOperator.prototype.constructor = IEquationOperator;
		Lavu.Reports.IEquationOperator.prototype = {};
		Lavu.Reports.IEquationOperator.prototype.operate = operate;
	})();

	(function(){
		function EquationOperatorAddition() {
			Lavu.Reports.IEquationOperator.call( this );
		}

		function operate( evaluate1, evaluate2, obj ){
			if( evaluate1 && evaluate2 && evaluate1 instanceof Lavu.Reports.ICalculatable && evaluate2 instanceof Lavu.Reports.ICalculatable ){
				return evaluate1.calculate( obj ) + evaluate2.calculate( obj );
			}

			return 0;
		}

		function toString(){
			return "+";
		}

		Lavu.Reports.EquationOperatorAddition = EquationOperatorAddition;
		Lavu.Reports.EquationOperatorAddition.prototype.constructor = EquationOperatorAddition;
		Lavu.Reports.EquationOperatorAddition.prototype = new Lavu.Reports.IEquationOperator();
		Lavu.Reports.EquationOperatorAddition.prototype.operate = operate;
		Lavu.Reports.EquationOperatorAddition.prototype.toString = toString;
	})();

	(function(){
		function EquationOperatorDifference() {
			Lavu.Reports.IEquationOperator.call( this );
		}

		function operate( evaluate1, evaluate2, obj ){
			if( evaluate1 && evaluate2 && evaluate1 instanceof Lavu.Reports.ICalculatable && evaluate2 instanceof Lavu.Reports.ICalculatable ){
				return evaluate1.calculate( obj ) - evaluate2.calculate( obj );
			} else if( evaluate2 && evaluate2 instanceof Lavu.Reports.ICalculatable ) {
				return - evaluate2.calculate( obj );
			}

			return 0;
		}

		function toString(){
			return "-";
		}

		Lavu.Reports.EquationOperatorDifference = EquationOperatorDifference;
		Lavu.Reports.EquationOperatorDifference.prototype.constructor = EquationOperatorDifference;
		Lavu.Reports.EquationOperatorDifference.prototype = new Lavu.Reports.IEquationOperator();
		Lavu.Reports.EquationOperatorDifference.prototype.operate = operate;
		Lavu.Reports.EquationOperatorDifference.prototype.toString = toString;
	})();

	(function(){
		function EquationOperatorProduct() {
			Lavu.Reports.IEquationOperator.call( this );
		}

		function operate( evaluate1, evaluate2, obj ){
			if( evaluate1 && evaluate2 && evaluate1 instanceof Lavu.Reports.ICalculatable && evaluate2 instanceof Lavu.Reports.ICalculatable ){
				return evaluate1.calculate( obj ) * evaluate2.calculate( obj );
			}

			return 0;
		}

		function toString(){
			return "*";
		}

		Lavu.Reports.EquationOperatorProduct = EquationOperatorProduct;
		Lavu.Reports.EquationOperatorProduct.prototype.constructor = EquationOperatorProduct;
		Lavu.Reports.EquationOperatorProduct.prototype = new Lavu.Reports.IEquationOperator();
		Lavu.Reports.EquationOperatorProduct.prototype.operate = operate;
		Lavu.Reports.EquationOperatorProduct.prototype.toString = toString;
	})();

	(function(){
		function EquationOperatorFactorial() {
			Lavu.Reports.IEquationOperator.call( this );
		}

		function fact( n ){
			if( n < 1 ){
				return 1;
			}

			return n * fact( n - 1 );
		}

		function operate( evaluate1, evaluate2, obj ){
			if( evaluate1 && evaluate1 instanceof Lavu.Reports.ICalculatable ){
				return fact( evaluate1.calculate( obj ) );
			}

			return 1;
		}

		function toString(){
			return "!";
		}

		Lavu.Reports.EquationOperatorFactorial = EquationOperatorFactorial;
		Lavu.Reports.EquationOperatorFactorial.prototype.constructor = EquationOperatorFactorial;
		Lavu.Reports.EquationOperatorFactorial.prototype = new Lavu.Reports.IEquationOperator();
		Lavu.Reports.EquationOperatorFactorial.prototype.operate = operate;
		Lavu.Reports.EquationOperatorFactorial.prototype.fact = fact;
		Lavu.Reports.EquationOperatorFactorial.prototype.toString = toString;
	})();

	(function(){
		function EquationOperatorDivision() {
			Lavu.Reports.IEquationOperator.call( this );
		}

		function operate( evaluate1, evaluate2, obj ){
			if( evaluate1 && evaluate2 && evaluate1 instanceof Lavu.Reports.ICalculatable && evaluate2 instanceof Lavu.Reports.ICalculatable ){
				return evaluate1.calculate( obj ) / evaluate2.calculate( obj );
			}

			return 0;
		}

		function toString(){
			return "/";
		}

		Lavu.Reports.EquationOperatorDivision = EquationOperatorDivision;
		Lavu.Reports.EquationOperatorDivision.prototype.constructor = EquationOperatorDivision;
		Lavu.Reports.EquationOperatorDivision.prototype = new Lavu.Reports.IEquationOperator();
		Lavu.Reports.EquationOperatorDivision.prototype.operate = operate;
		Lavu.Reports.EquationOperatorDivision.prototype.toString = toString;
	})();

	(function(){
		function EquationOperatorExponent() {
			Lavu.Reports.IEquationOperator.call( this );
		}

		function operate( evaluate1, evaluate2, obj ){
			if( evaluate1 && evaluate2 && evaluate1 instanceof Lavu.Reports.ICalculatable && evaluate2 instanceof Lavu.Reports.ICalculatable ){
				return Math.pow( evaluate1.calculate( obj ), evaluate2.calculate( obj ) );
			}

			return 0;
		}

		function toString(){
			return "^";
		}

		Lavu.Reports.EquationOperatorExponent = EquationOperatorExponent;
		Lavu.Reports.EquationOperatorExponent.prototype.constructor = EquationOperatorExponent;
		Lavu.Reports.EquationOperatorExponent.prototype = new Lavu.Reports.IEquationOperator();
		Lavu.Reports.EquationOperatorExponent.prototype.operate = operate;
		Lavu.Reports.EquationOperatorExponent.prototype.toString = toString;
	})();

	(function(){
		function getOperatorFromStr( str ){
			switch( str ){
				case '+' :
					return new Lavu.Reports.EquationOperatorAddition();
				case '-' :
					return new Lavu.Reports.EquationOperatorDifference();
				case '*' :
					return new Lavu.Reports.EquationOperatorProduct();
				case '!' :
					return new Lavu.Reports.EquationOperatorFactorial();
				case '/' :
					return new Lavu.Reports.EquationOperatorDivision();
				case '^' :
					return new Lavu.Reports.EquationOperatorExponent();
				default :
					return null;
			}
		}

		Lavu.Reports.IEquationOperator.getOperatorFromStr = getOperatorFromStr;
	})();

	(function(){
		function EquationLeaf(){
			Lavu.Reports.ICalculatable.call( this );
			this._value = null;
			this._token = null;
		}

		function setValue( value ){
			if( !(value instanceof Lavu.Reports.ICalculatable) ){
				return false;
			}

			this._value = value;
			return true;
		}

		function setToken( token ){
			this._token = token;
		}

		function calculate( obj ){
			if( this._value === undefined || this._value === null || ! this._value instanceof Lavu.Reports.ICalculatable ){
				return 0;
			}

			return this._value.calculate( obj );
		}

		function toString(){
			return this._token + "";
		}

		Lavu.Reports.EquationLeaf = EquationLeaf;
		Lavu.Reports.EquationLeaf.prototype.constructor = EquationLeaf;
		Lavu.Reports.EquationLeaf.prototype = new Lavu.Reports.ICalculatable();
		Lavu.Reports.EquationLeaf.prototype.setValue = setValue;
		Lavu.Reports.EquationLeaf.prototype.setToken = setToken;
		Lavu.Reports.EquationLeaf.prototype.calculate = calculate;
		Lavu.Reports.EquationLeaf.prototype.toString = toString;
	})();

	(function(){
		function EquationNode(){
			Lavu.Reports.ICalculatable.call( this );
			this._operator = null;
			this._node1 = null;
			this._node2 = null;
		}

		function setOperator( operator ){
			if(!operator){
				return false;
			}
			if( !(operator instanceof Lavu.Reports.IEquationOperator) ){
				return false;
			}

			this._operator = operator;
			return true;
		}

		function setNode1( node ){
			if(!node){
				return false;
			}
			if( !(node instanceof Lavu.Reports.EquationNode) && !(node instanceof Lavu.Reports.EquationLeaf) && !(node instanceof Lavu.Reports.EquationFunction) ){
				return false;
			}

			this._node1 = node;
			return false;
		}

		function setNode2( node ){
			if(!node){
				return false;
			}
			if( !(node instanceof Lavu.Reports.EquationNode) && !(node instanceof Lavu.Reports.EquationLeaf) && !(node instanceof Lavu.Reports.EquationFunction) ){
				return false;
			}

			this._node2 = node;
			return false;
		}

		function calculate( obj ){
			if( !this._operator ){
				return false;
			}

			return this._operator.operate( this._node1, this._node2, obj );
		}

		function toString(){
			var result = "";
			if( this._operator ){
				if( this._node1 ){
					result += "(" + this._node1 + ")";
				}
				result += this._operator;
				if( this._node2 ){
					result += "(" + this._node2 + ")";
				}
			}

		return result;
		}

		Lavu.Reports.EquationNode = EquationNode;
		Lavu.Reports.EquationNode.prototype.constructor = EquationNode;
		Lavu.Reports.EquationNode.prototype = new Lavu.Reports.ICalculatable();
		Lavu.Reports.EquationNode.prototype.setOperator = setOperator;
		Lavu.Reports.EquationNode.prototype.setNode1 = setNode1;
		Lavu.Reports.EquationNode.prototype.setNode2 = setNode2;
		Lavu.Reports.EquationNode.prototype.calculate = calculate;
		Lavu.Reports.EquationNode.prototype.toString = toString;
	})();

	(function(){
		function EquationFunction(){
			this._function = null;
			this._arguments = [];
		}

		function setFunction( func ){
			if( typeof func == "function" ){
				this._function = func;
				return true;
			}
			return false;
		}

		function addArgument( arg ){
			if( arg instanceof Lavu.Reports.ICalculatable ){
				this._arguments.push( arg );
				return true;
			}
			return false;
		}

		function calculate( obj ){
			if(this._function){
				var appliedArr = [];
				for( var i = 0; i < this._arguments.length; i++ ){
					appliedArr.push( this._arguments[i].calculate( obj ) );
				}
				return this._function.apply( this, appliedArr );
			}
			return 0;
		}

		function toString(){
			return this._function.name;
		}

		Lavu.Reports.EquationFunction = EquationFunction;
		Lavu.Reports.EquationFunction.prototype.constructor = EquationFunction;
		Lavu.Reports.EquationFunction.prototype = new Lavu.Reports.ICalculatable();
		Lavu.Reports.EquationFunction.prototype.setFunction = setFunction;
		Lavu.Reports.EquationFunction.prototype.addArgument = addArgument;
		Lavu.Reports.EquationFunction.prototype.calculate = calculate;
		Lavu.Reports.EquationFunction.prototype.toString = toString;
	})();

	(function(){
		function KPI(){
			Lavu.Reports.ICalculatable.call( this );
			this._format = null;
			this._name = "";
		}

		function setFormat( format ){
			if( !(format instanceof Lavu.Reports.IFormatable ) ){
				return false;
			}
			this._format = format;
			return true;
		}

		function getFormat(){
			return this._format;
		}

		function setName( name ){
			if( typeof name != "string" ){
				return false;
			}
			this._name = name;
		}

		function getName(){
			return this._name;
		}

		function calculate( reportDelimiters ){
			return 0;
		}

		Lavu.Reports.KPI = KPI;
		Lavu.Reports.KPI.prototype.constructor = KPI;
		Lavu.Reports.KPI.prototype = new Lavu.Reports.ICalculatable();
		Lavu.Reports.KPI.prototype.getName = getName;
		Lavu.Reports.KPI.prototype.setName = setName;
		Lavu.Reports.KPI.prototype.getFormat = getFormat;
		Lavu.Reports.KPI.prototype.getFormat = getFormat;
		Lavu.Reports.KPI.prototype.calculate = calculate;
	})();

	(function(){
		function KPIStorage(){
			this._storage = [];
		}

		function addKPI( kpi ){
			if( !(kpi instanceof Lavu.Reports.KPI) ){
				return false;
			}

			this._storage.push( kpi );
		}

		function getKPIByName( kpiName ){
			for( var i = 0; i < this._storage.length; i++ ){
				if(this._storage[i].getName() == kpiName ){
					return this._storage[i];
				}
			}
			return null;
		}

		Lavu.Reports.KPIStorage = KPIStorage;
		Lavu.Reports.KPIStorage.prototype.constructor = KPIStorage;
		Lavu.Reports.KPIStorage.prototype = {};
		Lavu.Reports.KPIStorage.prototype.addKPI = addKPI;
		Lavu.Reports.KPIStorage.prototype.getKPIByName = getKPIByName;
	})();

	(function(){
		/**
		 * PEMDAS
		 *
		 * +,-,*,/
		 *
		 * f(1,2,3,...,n) = 1+2-3*4/5
		 *
		 * 1+2 - (3*(4/5))
		 */

		var operatorToPrecidence = {
			'+' : 0,
			'-' : 0,
			'*' : 1,
			'/' : 1,
			'!' : 3,
			'^' : 2
		};

		var operators = [];
		var precidenceToOperator = [];
		for(var operator in operatorToPrecidence ){
			operators.push( operator );
			if( precidenceToOperator[ operatorToPrecidence[ operator ] ] === undefined) {
				precidenceToOperator[ operatorToPrecidence[ operator ] ] = [];
			}
			precidenceToOperator[ operatorToPrecidence[ operator ] ].push( operator );
		}

		function KPICalculation(){
			Lavu.Reports.KPI.call( this );
			this._equation = null;
			this._atoms = {};
		}

		function setEquation( strEquation ){
			this._equation = strEquation;
		}

		function calculate( reportDelimiters ){
			return this._equation.calculate( reportDelimiters );
		}

		function bindVariable( variable, value ){
			if(variable === undefined || variable === null ){
				return false;
			}

			if( value === undefined || value === null ){
				return false;
			}

			if( this._atoms[variable] === undefined ){
				return false;
			}

			if( !isNaN( Number( value ) ) ){
				this._atoms[variable].setValue( new Lavu.Reports.Constant( Number(value) ) );
				return true;
			}

			if( ! value instanceof Lavu.Reports.ICalculatable ){
				return false;
			}

			this._atoms[variable].setValue( value );
			return true;
		}

		function parseEquation( equationStr ){
			this._atoms = {};
			this._equation = this._parseEquation( equationStr );
		}

		function trim( str ){
			if( typeof str == "string" ){
				return str.trim();
			}
			return null;
		}

		function _parseEquation( equationStr ){
			var strLength = equationStr.length;
			if( equationStr.substr(0,1) == '(' && equationStr.substr(-1) == ')' ){
				return this._parseEquation( equationStr.substr(1, equationStr.length - 2) );
			}

			var operatorIndex = -1;
			var currentPrecidence = Number.MAX_VALUE;
			var parenthesisLevel = 0;
			for(var i = 0; i < equationStr.length; i++){
				var character = equationStr[i];
				if( character == '(' ){
					parenthesisLevel++;
				} else if( character == ')' ){
					parenthesisLevel--;
				} else if( operators.indexOf( character ) >= 0 && parenthesisLevel === 0 ){
					var operator = character; //Same thing, just really clarifying here.
					var operatorPrecidence = operatorToPrecidence[ operator ];
					currentPrecidence = Math.min( currentPrecidence, operatorPrecidence );
					if( operatorPrecidence == currentPrecidence ){
						operatorIndex = i;
					}
				}
			}

			if( operatorIndex !== -1 ){
				//We found an operator... HOORAY!
				var operatorObj = new Lavu.Reports.IEquationOperator.getOperatorFromStr( equationStr.charAt(operatorIndex) );

				var operatorNode = new Lavu.Reports.EquationNode();
				var firstHalf = "";
				var secondHalf = "";
				firstHalf = equationStr.substr(0, operatorIndex );
				if( ! (operatorObj instanceof Lavu.Reports.EquationOperatorFactorial) ) { //binary operator
					secondHalf = equationStr.substr(operatorIndex+1);
					operatorNode.setNode2( this._parseEquation( secondHalf ) );
				}
				operatorNode.setOperator( operatorObj );
				operatorNode.setNode1( this._parseEquation( firstHalf ) );

				return operatorNode;
			} else {
				//We found an 'atom', or function;
				if( equationStr.indexOf("(") !== -1 ){
					//function
					var openParen = equationStr.indexOf("(");
					var closeParen = equationStr.indexOf(")");

					var argString = equationStr.substr( openParen + 1, closeParen - openParen - 1 );
					var args = argString.split(",");
					args = args.map( trim );

					var funcName = equationStr.substr(0, openParen);
					var func = null;
					if(Math[funcName] && typeof Math[funcName] == "function" ){
						func = new Lavu.Reports.EquationFunction();
						func.setFunction( Math[funcName] );
						for( var idx in args ){
							var parsedArg = this._parseEquation( args[idx] );
							func.addArgument( parsedArg );
						}
						return func;
					} else {
						return null;
					}
				} else {
					//atom
					if( this._atoms[equationStr] === undefined ){
						// An Atom
						var atom = new Lavu.Reports.EquationLeaf();
						if( !isNaN(Number( equationStr ))){
							atom.setValue(new Lavu.Reports.Constant(Number( equationStr )));
						}

						if( Math[equationStr] ){
							atom.setValue( new Lavu.Reports.Constant(Number( Math[equationStr] )));
						}

						this._atoms[equationStr] = atom;
						atom.setToken( equationStr );
						return atom;
					} else {
						return this._atoms[ equationStr ];
					}
				}
			}
		}

		function toString(){
			return this._equation.toString();
		}

		Lavu.Reports.KPICalculation = KPICalculation;
		Lavu.Reports.KPICalculation.prototype.constructor = KPICalculation;
		Lavu.Reports.KPICalculation.prototype = new Lavu.Reports.KPI();
		Lavu.Reports.KPICalculation.prototype.setEquation = setEquation;
		Lavu.Reports.KPICalculation.prototype.calculate = calculate;
		Lavu.Reports.KPICalculation.prototype.bindVariable = bindVariable;
		Lavu.Reports.KPICalculation.prototype.parseEquation = parseEquation;
		Lavu.Reports.KPICalculation.prototype._parseEquation = _parseEquation;
		Lavu.Reports.KPICalculation.prototype.toString = toString;
	})();
})();