//mvc
(function(){
	if( ! window.Lavu ) {
		window.Lavu = {};
	}

	if( ! window.Lavu.mvc ) {
		window.Lavu.mvc = {};
	}

	(function(){
		/**
		 * An MVC Model, represents the base-line representation of any data based object.  Anything wishing to employ the MVC framework should extend Lavu.mvc.Model instead of Object.
		 * This class contains base representations of the Model, including defining functions for running a Lavu.mvc.Command that has been submitted to it, as well as the ability to Store Lavu.mvc.Views or remove them.
		 * If you extend Lavu.mvc.Model, any time your interal data changes, you are required to call updateViews(), so that your views will be updated appropriately.
		 */
		function Model() {
			Object.call( this );
			this._views = [];
		}

		/**
		 * This method will attempt to add a Lavu.mvc.View to the view list that is responsbil for representing/observicing this Lavu.mvc.Model
		 * @param {Lavu.mvc.View} view   the view to add to this Lavu.mvc.Model
		 * @param {bool} redraw true if the view was succesfully added, or false otherwise.  Will not add the view if the view is already contained within the Lavu.mvc.Model's view list.
		 */
		function addView( view, redraw ) {
			if(! view ) {
				return false;
			}

			if( ! view instanceof Lavu.mvc.View ) {
				return false;
			}

			if( this._views.indexOf( view ) >= 0 ) {
				return false;
			}

			this._views.push( view );
			if( !redraw && redraw !== false) {
				view.notify( this );
			}
			return true;
		}

		/**
		 * This method will attemp to remove the provided Lavu.mvc.View from this Lavu.mvc.Model.  In doing so successfully, the Lavu.mvc.View will no longer be notified of any updates to this Lavu.mvc.Model.
		 * @param  {Lavu.mvc.View} view the view to attempt to remove from this Lavu.mvc.Model
		 * @return {bool}      true if the removeal occurreced successfully, or false otherwise.
		 */
		function removeView( view ) {
			if( ! view ) {
				return false;
			}

			if( ! view instanceof Lavu.mvc.View ) {
				return false;
			}

			var index = this._views.indexOf( view );
			if( index <  0 ) {
				return false;
			}

			this._views.splice( index, 1 );
			return true;
		}

		/**
		 * This method iterates through all of the Lavu.mvc.Model's Lavu.mvc.Views and notifys them that this Lavu.mvc.Model has updated, it will also pass it a reference to itself.  utilizing that information, the Lavu.mvc.View will be able to update its representation accordingly.
		 * @return {void} doesn't return a value.
		 */
		function updateViews() {
			for( var i = 0; i < this._views.length; i++ ){
				this._views[i].notify( this );
			}
			return;
		}

		/**
		 * The run method will attempt to run the Lavu.mvc.Command that has been provided by providing the command a reference to itself.
		 * @param  {Lavu.mvc.Command} command the command to run
		 * @return {bool}         returns true if the command was run successfully, false otherwise.
		 */
		function run( command ) {
			if(! command ) {
				return false;
			}

			if(! command instanceof Lavu.mvc.Command ) {
				return false;
			}

			command.run( this );
			return true;
		}

		/* Object Setup and Inheritence  */
		Lavu.mvc.Model = Model;
		Lavu.mvc.Model.prototype.constructor = Model;
		Lavu.mvc.Model.prototype.addView = addView;
		Lavu.mvc.Model.prototype.removeView = removeView;
		Lavu.mvc.Model.prototype.updateViews = updateViews;
		Lavu.mvc.Model.prototype.run = run;
	})();

	(function(){

		/**
		 * This array will contain all of the Lavu.mvc.Views that are declared within the mvc structure.
		 * @type {Array}
		 */
		var __allViews = [];

		/**
		 * The main draw loop iterates through all of the various views and informs them to attemp to update.  The update funtion for the Lavu.mvc.View will empty the list of notifiers that they contain, and will tell themselves to draw with the contained information.
		 * @return {void} This function doesn't return anything.
		 */
		function mainDrawLoop(){
			for( var i = 0; i < __allViews.length; i++ ){
				__allViews.update();
			}
		}

		/**
		 * This reference holds the main draw loop reference.  It
		 * @type {interval handle}
		 */
		var __mainDrawLoopRef = window.setInterval( mainDrawLoop, 300 );

		/**
		 * An MVC View, represents a 'view' that is able to display information when provided a Lavu.mvc.Model. The Lavu.mvc.View is represented as an object and creates an HTML Element for which it is responsible for. In this case, the default HTML Element is a div.  A Lavu.mvc.View can also have a parent and children. It is only responsible for itself however, and is not requires to inform it's children of any updates.
		 */
		function View() {
			Object.call( this );
			__allViews.push(this);
			this._notifiers = [];
		}

		function notify( model ){
			if(!model || !(model instanceof Lavu.mvc.Model)){
				return false;
			}

			if( this._notifiers.indexOf(model) !== -1 ){
				return false;
			}

			this._notifiers.push( model );
			return true;
		}

		/**
		 * This method is requires by anything extending Lavu.mvc.View.  By default, in order to ensure it's implementation this function returns false to signify that it wasn't able to update. In general, it should update the information it's representing accordingly based on what is presented within the Lavu.mvc.Model that it has been handed.
		 * @return {bool} true if successfully updated for all notifiers, false otherwise.
		 */
		function update() {
			var result = true;
			while( this._notifiers.length ){
				result = result && this.draw( this._notifiers[0] );
				this._notifiers.splice(0,1);
			}
			return result;
		}

		/**
		 * The draw method of the View should contain everything that is needed to be able to draw any information it needs to for the user to see.  When a view has been notified, it will be handed a reference to the model that has notified it.  When the drawloop happens the notifiers will iterate to the model and the draw function will be called, passing in the notifying model so that it has the information it needs to draw itself accordingly.
		 * @param  {Lavu.mvc.Model} model The model whose information is to be reddrawn.
		 * @return {bool}       returns true upon succesfully drawing, false otherwise.
		 */
		function draw( model ){
			console.error('Error: Draw method not implemented.');
			//throw new Error('Draw method not implemented.');
			return false;
		}

		/* Object setup and Inheritence */
		Lavu.mvc.View = View;
		Lavu.mvc.View.prototype.constructor = View;
		Lavu.mvc.View.prototype.update = update;
		Lavu.mvc.View.prototype.draw = draw;
	})();

	(function(){
		/**
		 * TODO: COMMENTS NEED UPDATING
		 *
		 * An mvc Controller.  The Lavu.mvc.Controller is spawned with an initial Model.
		 * It primarily passes Commands to the provided Model, and is responsible for overall inital setup
		 * of most things.
		 **/

		/**
		 * An mvc Controller.  The Lavu.mvc.Controller is spawned with an initial Model. It primarily passes Commands to the provided Model, and is responsible for overall inital setup of most things.
		 * @param {Lavu.mvc.Model} model a model to add to this Controller.
		 */
		function Controller( model ) {
			if(! model) {
				return null;
			}

			if(! model instanceof Lavu.mvc.Model ){
				return null;
			}

			this._models = [ model ];
		}

		/**
		 * The runCommand method will pass the provided Lavu.mvc.Command to the Lavu.mvc.Model based on the Lavu.mvc.Command's target.
		 * @param  {Lavu.mvc.Command} command the command to run on the target model
		 * @return {bool}         true if the command was run successfully, false otherwise.
		 */
		function runCommand( command ) {
			var target = command.getTarget();
			if( !target ) {
				target = Lavu.mvc.Model;
			}

			var model = this.getModelByType( target );
			if( model ) {
				model.run( command );
			}

			return;
		}

		/**
		 * the getModelByType method returns the first Lavu.mvc.Model that matches the target critera.
		 * @param  {prototype} type the type of the Lavu.mvc.Model that should be returned.
		 * @return {Lavu.mvc.Model}      A Lavu.mvc.Model that matches the type specified.
		 */
		function getModelByType( type ) {
			if( ! type ) {
				return null;
			}

			for( var i = 0; i < this._models.length; i++ ){
				if( this._models[i] instanceof type ){
					return this._models[i];
				}
			}

			return null;
		}

		function addModel( model ) {
			if(! model ){
				return false;
			}

			if(! model instanceof Lavu.mvc.Model ){
				return false;
			}

			if( this._models.indexOf( model ) >= 0 ){
				return false;
			}

			this._models.push( model );
			return true;
		}

		function removeModel( model ) {
			if(! model ){
				return false;
			}

			if(! model instanceof Lavu.mvc.Model ){
				return false;
			}

			var index = this._models.indexOf( model );
			if( index < 0 ){
				return false;
			}

			this._models.splice( index, 1 );
			return true;
		}

		/* Object setup and Inheritence */
		Lavu.mvc.Controller = Controller;
		Lavu.mvc.Controller.prototype.constructor = Controller;
		Lavu.mvc.Controller.prototype.runCommand = runCommand;
		Lavu.mvc.Controller.prototype.getModelByType = getModelByType;
		Lavu.mvc.Controller.prototype.addModel = addModel;
		Lavu.mvc.Controller.prototype.removeModel = removeModel;
	})();

	(function(){
		/**
		 * An mvc Command is a Lavu.mvc.Command that promsises to be extended and implement functionality for run method.
		 **/
		function Command( ) {
			this._target = null;
		}

		/**
		 * The run method will perform the actions neccessary on the provided Lavu.mvc.Model.  Please ensure that the provided Object is Not null, and is a Lavu.mvc.Model before performing any tasks on the Lavu.mvc.Model to avoid errors.
		 * @param  {Lavu.mvc.Model} model the model to perform the Lavu.mvc.Command on.
		 * @return {bool}       true if the command was performed successfully, false otherwise.
		 */
		function run( model ) {
			return false;
		}

		/**
		 * Returns teh current target for this Lavu.mvc.command.  By default this is null, and it's up to the extender to modify this.  The point of the target is to specify a prototype type so that the Lavu.mvc.Model or Lavu.mvc.Controller can either choose to accept/discard this Lavu.mvc.Command, or so they can redirect the Lavu.mvc.Command to an appropriate recipient.
		 * @return {prototype} the currently set target of this Lavu.mvc.Command.
		 */
		function getTarget() {
			return this._target;
		}

		/**
		 * Will set the current target of this Lavu.mvc.Command to whatever is specified. This does not filter the
		 * input and will even accept null.
		 *
		 * @returns void
		 **/
		function setTarget( target ) {
			this._target = target;
		}

		/* Object setup and Inheritence */
		Lavu.mvc.Command = Command;
		Lavu.mvc.Command.prototype.constructor = Command;
		Lavu.mvc.Command.prototype.run = run;
		Lavu.mvc.Command.prototype.getTarget = getTarget;
		Lavu.mvc.Command.prototype.setTarget = setTarget;
	})();

})();