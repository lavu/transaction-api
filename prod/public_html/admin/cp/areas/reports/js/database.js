(function(){
	if( !Lavu.Database ){
		Lavu.Database = {};
	}

	(function(){
		function Table() {
			this._name = '';
			this._rows = [];
			this._columns = [];
		}

		function setName( name ){
			this._name = name;
		}

		function getName() {
			return this._name;
		}

		function addRow( row ){
			this._rows.push( row );
		}

		function getRows(){
			return this._rows;
		}

		Lavu.Database.Table = Table;
		Lavu.Database.Table.prototype.constructor = Table;
		Lavu.Database.Table.prototype = new Lavu.mvc.Model();
		Lavu.Database.Table.prototype.setName = setName;
		Lavu.Database.Table.prototype.getName = getName;
		Lavu.Database.Table.prototype.addRow = addRow;
		Lavu.Database.Table.prototype.getRows = getRows;
	})();

	(function(){
		function Database() {
			this._name = '';
			this._tables = {};
		}

		function getTable( tableName ){
			if( this._tables[ tableName ] ){
				return this._tables[ tableName ];
			}
			return null;
		}

		function addTable( tableName, table ){
			if( table instanceof Lavu.Database.Table ){
				this._tables[ tableName ] = table;
				return true;
			}
			return false;
		}

		Lavu.Database.Database = Database;
		Lavu.Database.Database.prototype.constructor = Database;
		Lavu.Database.Database.prototype = new Lavu.mvc.Model();
		Lavu.Database.Database.prototype.getTable = getTable;
		Lavu.Database.Database.prototype.addTable = addTable;
	})();

	(function(){
		function FieldRelationship(){
			this._tableName1 = '';
			this._fieldName1 = '';
			this._tableName2 = '';
			this._fieldName2 = '';
		}

		function getTableName1(){
			return this._tableName1;
		}

		function getTableName2(){
			return this._tableName2;
		}

		function getFieldName1(){
			return this._fieldName1;
		}

		function getFieldName2(){
			return this._fieldName2;
		}


		Lavu.Database.FieldRelationship = FieldRelationship;
		Lavu.Database.FieldRelationship.prototype.constructor = FieldRelationship;
		Lavu.Database.FieldRelationship.prototype = {};
		Lavu.Database.FieldRelationship.prototype.getTableName1 = getTableName1;
		Lavu.Database.FieldRelationship.prototype.getTableName2 = getTableName2;
		Lavu.Database.FieldRelationship.prototype.getFieldName1 = getFieldName1;
		Lavu.Database.FieldRelationship.prototype.getFieldName2 = getFieldName2;
	})();
})();