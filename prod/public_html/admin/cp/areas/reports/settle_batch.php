<?php

	$mid .= "<tr><td align='center'><b>Credit Card Batch Settlement</b><br><br></td></tr><tr><td></td></tr>";
			
	$error_code = 0;
	$error_info = 0;
						
	$integration_info = get_integration_from_location($locationid, "poslavu_".$process_info['data_name']."_db");

	if (!isset($dev_dir)) {
		$dev_dir = "";
	}
	
	$req_path = dirname(__FILE__)."/../../../".$dev_dir."lib";
	$req_file = "Transaction_Controller.php";

	require_once(dirname(__FILE__) . "/../../../../admin/vendor/autoload.php");
	require_once(dirname(__FILE__) . "/../../../app/Http/Controllers/LavuPayController.php");
	use App\Controller\LavuPayController;
	use Symfony\Component\HttpFoundation\JsonResponse;
	use Symfony\Component\HttpFoundation\Request;

	require_once($req_path."/gateway_settings.php");

	if (file_exists($req_path."/gateway_lib/".$req_file)) {
		require_once($req_path."/gateway_lib/".$req_file);						
	}

	$transactionController = new Transaction_Controller($integration_info['gateway'], $process_info);
	
	$process_info['username'] = $integration_info['integration1'];
	$process_info['password'] = $integration_info['integration2'];
	$process_info['integration3'] = $integration_info['integration3'];
	$process_info['integration4'] = $integration_info['integration4'];
	$process_info['integration5'] = $integration_info['integration5'];
	$process_info['integration6'] = $integration_info['integration6'];
	
	$batch_total = 0;

	$detail2 = "";
	$detail3 = "";
	$log_response = '';
	$error_founded = false;
	
	$till_report_info = array();

	if (in_array($integration_info['gateway'], array("Authorize.net", "BluePay", "CyberSource", "Elavon"))) {
	
		$forty_five_days_ago = date("Y-m-d H:i:s", time() - 3888000);

		$tip_was_declined = false;
		$get_transactions = lavu_query("SELECT * FROM `cc_transactions` WHERE `loc_id` = '[1]' AND `pay_type_id` = '2' AND `Action` = 'Sale' AND `voided` != '1' AND `order_id` NOT LIKE '777%' AND `processed` != '1' AND `gateway` = 'Authorize.net' AND `datetime` >= '[2]'".$order_by, $location_info['id'], $forty_five_days_ago);
		if (@mysqli_num_rows($get_transactions) > 0) {

			$log_response = "Transactions settled with no errors. ";
	
			while ($transaction_info = mysqli_fetch_assoc($get_transactions)) {
	
				usleep(1000);
				$iid = $transaction_info['internal_id'];
				$process_info['order_id'] = $transaction_info['order_id'];
				$process_info['ioid'] = $transaction_info['ioid'];
				$process_info['check'] = $transaction_info['check'];
				$process_info['pnref'] = $transaction_info['transaction_id'];

				if ($transaction_info['auth'] == "1") {

					$process_info['transtype'] = "PreAuthCapture";
					$process_info['card_amount'] = $transaction_info['amount'];
					$process_info['split_tender_id'] = $transaction_info['split_tender_id'];

					$transactionController->process_transaction($process_info,$location_info);
					$response = $transactionController->getResponse();

					$response_parts = explode("|", $response);
					if ($response_parts[0] == "1") {

						$txid = $response_parts[1];
						$update_transaction = lavu_query("UPDATE `cc_transactions` SET `auth` = '0', `transaction_id` = '[1]', `last_mod_ts` = '[2]' WHERE `id` = '[3]'", $txid, time(), $transaction_info['id']);
						if (!empty($iid)) {
							schedule_remote_to_local_sync_if_lls($location_info, $location_info['id'], "run local query", "cc_transactions", "UPDATE `cc_transactions` SET `auth` = '0', `transaction_id` = '".$txid."', `last_mod_ts` = '".time()."' WHERE `internal_id` = '".$iid."'");
						}
					}

				} else {

					$response = "1|Approved";
					$response_parts = explode("|", $response);
				}

				if (($transaction_info['tip_amount'] > 0) && ($response_parts[0] == "1")) {

					usleep(1000);

					$process_info['transtype'] = "AddTip";
					$process_info['card_amount'] = $transaction_info['tip_amount'];
				
					if ($integration_info['gateway'] == "CyberSource") {

						$card_data = explode("|*|", interpret($transaction_info['temp_data']));
						$process_info['mag_data'] = $card_data[4];
						$process_info['card_number'] = $card_data[0];
						$process_info['exp_month'] = $card_data[1];
						$process_info['exp_year'] = $card_data[2];
						$process_info['card_cvn'] = $card_data[3];
						$process_info['transaction_db_id'] = $transaction_info['id'];


					} else {
						$card_data = explode("|", interpret($transaction_info['temp_data']));
						$process_info['mag_data'] = "";
						$process_info['card_number'] = $card_data[0];
						$process_info['exp_month'] = substr($card_data[1], 0, 2);
						$process_info['exp_year'] = substr($card_data[1], -2);
						$process_info['card_cvn'] = $card_data[2];
					}

					$transactionController->process_transaction($process_info,$location_info);
					$response = $transactionController->getResponse();
				}

				$response_parts = explode("|", $response);			
				if ($response_parts[0] == "1") {
	
					$update_transaction = lavu_query("UPDATE `cc_transactions` SET `processed` = '1', `temp_data` = '', `last_mod_ts` = '[1]' WHERE `id` = '[2]'", time(), $transaction_info['id']);
					if (!empty($iid)) {
						schedule_remote_to_local_sync_if_lls($location_info, $location_info['id'], "run local query", "cc_transactions", "UPDATE `cc_transactions` SET `processed` = '1', `temp_data` = '', `last_mod_ts` = '".time()."' WHERE `internal_id` = '".$iid."'");
					}
					$mid .= "<tr><td align='center'><br>Order ID ".$location_info['id'].$transaction_info['order_id']." - Card Tip: ".number_format($transaction_info['tip_amount'], $decimal_places, ".", "")." - Approved</td></tr>";
				} else if (($response_parts[0] != "1") && ($transaction_info['tip_amount'] > 0)) {
					$mid .= "<tr><td align='center'><br>Order ID ".$location_info['id'].$transaction_info['order_id']." - Card Tip: ".number_format($transaction_info['tip_amount'], $decimal_places, ".", "")." - Declined</td></tr>";
					$tip_was_declined = true;
					file_put_contents("/home/poslavu/logs/company/".$process_info['data_name']."/tip_declines.txt", $transaction_info['order_id']." : ".$process_info['card_amount']."\n",FILE_APPEND);
					
				} else {

					$mid .= "<tr><td align='center'><br>Order ID ".$location_info['id'].$transaction_info['order_id']." - ".$response_parts[1]."</td></tr>";
					$log_response = "One or more errors encountered. ";
					$error_founded = true;
				}
			}

		} else {

			$mid .= "<tr><td align='center'>No card transactions to settle...</td></tr>";
			$log_response = "No card transactions to settle.";
		}
		
		if ($tip_was_declined) {

			$mid .= "<tr><td>&nbsp;</td></tr><tr><td align='center'>Some tips were declined. Transactions with declined tips will still be flagged as unprocessed so you may adjust tip amounts until all transactions can be processed successfully.<br><br><br></td></tr>";
			$log_response .= "One or more tip transactions were declined.";
			$error_founded = true;
		}

	} else if ($integration_info['gateway'] == "Magensa") {

		$tip_was_declined = false;
		$get_transactions = lavu_query("SELECT * FROM `cc_transactions` WHERE `loc_id` = '".$location_info['id']."' AND `pay_type` = 'Card' AND `transtype` = 'Auth' AND `auth` != '2' AND `voided` != '1' AND `got_response` = '1' AND `processed` != '1'"); // AND `datetime` <= '2013-09-16 03:59:00'
		if (@mysqli_num_rows($get_transactions) > 0) {

			$log_response = "Settled with no errors.";
			$base_total = 0;
			$tip_total = 0;
			$base_total_c = 0;
			$tip_total_c = 0;

			while ($transaction_info = mysqli_fetch_assoc($get_transactions)) {

				usleep(1000);

				$process_info['pnref'] = $transaction_info['preauth_id'];
				$process_info['order_id'] = $transaction_info['order_id'];
				$process_info['check'] = $transaction_info['check'];
				$capture_amount = number_format(($transaction_info['amount'] + $transaction_info['tip_amount']), $decimal_places, ".", "");

				$process_info['transtype'] = "PreAuthCapture";
				$iid = $transaction_info['internal_id'];
				$process_info['card_amount'] = $capture_amount;

				$transactionController->process_transaction($process_info, $location_info);
				$response = $transactionController->getResponse();
				
				$base_total += $transaction_info['amount'];
				$tip_total += $transaction_info['tip_amount'];

				$response_parts = explode("|", $response);
				if ($response_parts[0] == "1") {

					$base_total_c += $transaction_info['amount'];
					$tip_total_c += $transaction_info['tip_amount'];
					$txid = $response_parts[1];
					$update_transaction = lavu_query("UPDATE `cc_transactions` SET `transaction_id` = '[1]', `voided` = '0', `auth` = '0', `processed` = '1', `temp_data` = '', `last_mod_ts` = '[2]' WHERE `id` = '[3]'", $txid, time(), $transaction_info['id']);
					if (!empty($iid)) {
						schedule_remote_to_local_sync_if_lls($location_info, $location_info['id'], "run local query", "cc_transactions", "UPDATE `cc_transactions` SET `transaction_id` = '".txid."', `auth` = '0', `processed` = '1', `temp_data` = '', `last_mod_ts` = '".time()."' WHERE `internal_id` = '".$iid."'");
					}
					$mid .= "<tr><td align='center'><br>Order ID ".$location_info['id'].$transaction_info['order_id']." - Card Tip: ".number_format($transaction_info['tip_amount'], $decimal_places, ".", "")." - Approved</td></tr>";

				} else if (($response_parts[0] != "1") && ($transaction_info['tip_amount'] > 0)) {

					$mid .= "<tr><td align='center'><br>Order ID ".$location_info['id'].$transaction_info['order_id']." - Card Tip: ".number_format($transaction_info['tip_amount'], $decimal_places, ".", "")." - Declined</td></tr>";
					$tip_was_declined = true;
					$log_response = "One or more errors encountered. ";
					$error_founded = true;

				} else {

					$mid .= "<tr><td align='center'><br>Order ID ".$location_info['id'].$transaction_info['order_id']." - ".$response_parts[1]."</td></tr>";
					$log_response = "One or more errors encountered. ";
					$error_founded = true;
				}
			}
			
			$mid .= "<tr><td>&nbsp;</td></tr>";
			$mid .= "<tr><td align='center'>Total Base Amount: ".number_format($base_total_c, $decimal_places, ".", "")." / ".number_format($base_total, $decimal_places, ".", "")."</td></tr>";
			$mid .= "<tr><td align='center'>Total Tip Amount: ".number_format($tip_total_c, $decimal_places, ".", "")." / ".number_format($tip_total, $decimal_places, ".", "")."</td></tr>";
			$mid .= "<tr><td align='center'>Grand Total Amount: ".number_format(($base_total_c + $tip_total_c), $decimal_places, ".", "")." / ".number_format(($base_total + $tip_total), $decimal_places, ".", "")."</td></tr>";
			$mid .= "<tr><td>&nbsp;</td></tr>";
			$batch_total = number_format(($base_total_c + $tip_total_c), $decimal_places, ".", "")." / ".number_format(($base_total + $tip_total), $decimal_places, ".", "");
			
		} else {

			$mid .= "<tr><td align='center'>No card transactions to settle...</td></tr>";
			$log_response = "No card transactions to settle.";
		}
		
		if ($tip_was_declined) {
			$mid .= "<tr><td align='center'>Some tips were declined. Transactions with declined tips will still be flagged as unprocessed so you may adjust tip amounts until all transactions can be processed successfully.<br><br><br></td></tr>";
		} else {
			$mid .= "<tr><td></td></tr><tr><td align='center'><b>".$log_response."</b></td></tr>";
		}
		
	} else {
	
		$process_info['transtype'] = "CaptureAll";
		$process_info['order_id'] = "";
		$process_info['check'] = "";
		$process_info['pnref'] = "";
	
		switch ($integration_info['gateway']) {
			case "EVOSnap" :
			case "Heartland" :
			case "MerchantWARE" :
			case "Mercury" :
			case "RedFin" :
			case "TGate" :
			case "PayPal":
				$transactionController->process_transaction($process_info, $location_info);
				$response = $transactionController->getResponse();
				break;
			case "LavuPay":
				try {
					$response = settleBatchWithLavuPay(Request::createFromGlobals(), $process_info, $location_info);
				} catch (Exception $e) {
					$error_founded = true;
					$response = '0|Error: ' . $e->getMessage();
					error_log("Caught exception: " . $e->getMessage());
				}
				break;
			case "USAePay" : $response = "0|Merchant initiated batching is not available for Axia ePay."; break;
			default : $response = "0|Error: Unrecognized payment gateway (".$integration_info['gateway'].")..."; break;
		}
		
		$response_parts = explode("|", $response);
		$till_report_info['result'] = $response_parts[0];
		if ($response_parts[0] == "1") {

			$log_response = $response_parts[3];
			$show_response = $response_parts[3];
			if ($integration_info['gateway'] == "Heartland") $show_response = $response_parts[4];
			if ($integration_info['gateway'] == "Mercury") $log_response = $response_parts[1];
			if ($integration_info['gateway'] == 'LavuPay') {
				$show_response = $response_parts[1];
				$log_response =  $response_parts[1];
			}
			$update_transactions = lavu_query("UPDATE `cc_transactions` SET `processed` = '1', `last_mod_ts` = '[1]' WHERE `datetime` > DATE_SUB(NOW(), INTERVAL 2 WEEK) AND `processed` != '1' AND `loc_id` = '[2]' AND `mpshc_pid` = '' AND `auth` = '0' AND `voided` != '1'", time(), $location_info['id']);
			$mid .= "<tr><td align='center'>".$show_response."</td></tr>";
			
			switch ($integration_info['gateway']) {
				case "Heartland" :
					$batch_total = $response_parts[7];
					$detail3 = "Batch # ".$response_parts[1];	
					break;
				case "Mercury" :
					$batch_total = $response_parts[2];
					$detail3 = "Batch # ".$response_parts[4];
					$till_report_info['details'] = $response_parts[5];
					break;
				default:
					break;
			}
			
			$till_report_info['batch_total'] = $batch_total;
			$till_report_info['batch_no'] = $detail3;

		} else {
		
			$log_response = $response_parts[1];
			$show_response = $response_parts[1];
			$error_founded = true;
	
			if (($integration_info['gateway'] == "TGate") && ($log_response == "No Records To Process")) {
				$mid .= "<tr><td align='center'>No Records To Process - please note this message will always be returned if you are using a host based processor, and there is no need for you to manually settle batches from the POSLavu back end. Host-based processors include Concord (EFSNet), Global, Paymentech - Tampa, FastCheck/Cyclone, IPay/Intercept, NCN (Rocky Mountain), Echo, Valuetec, Element, Litle, Global East Canada, Givex, Telecheck, and Elavon/Nova.</td></tr>";
			} else {
				$mid .= "<tr><td align='center'>".$log_response."</td></tr>";
			}
		}
		
		$till_report_info['response'] = $show_response;
	}

	$l_vars = array();
	$q_fields = "";
	$q_values = "";
	$l_vars['action'] = "Credit card batch settled";
	$l_vars['user'] = $username;
	$l_vars['user_id'] = $serverid;
	$l_vars['ipaddress'] = $_SERVER['REMOTE_ADDR'];
	$l_vars['order_id'] = "0";
	$l_vars['data'] = $log_response;
	$l_vars['detail1'] = $integration_info['gateway'];
	$l_vars['detail2'] = $batch_total;
	$l_vars['detail3'] = $detail3;
	$l_vars['loc_id'] = $locationid;
	$l_vars['time'] = localize_datetime(date("Y-m-d H:i:s"), $location_info['timezone']);
	$keys = array_keys($l_vars);
	foreach ($keys as $key) {
		if ($q_fields != "") {
			$q_fields .= ", ";
			$q_values .= ", ";
		}
		$q_fields .= "`$key`";
		$q_values .= "'[$key]'";
	}
	$log_this = lavu_query("INSERT INTO `admin_action_log` ($q_fields, `server_time`) VALUES ($q_values, now())", $l_vars);

	$detail2 = "";
	$detail3 = "";
	if ($integration_info['gateway'] == "Mercury") {
		
		$check_mpshc = lavu_query("SELECT `id` FROM `cc_transactions` WHERE `processed` = '0' AND `loc_id` = '".$location_info['id']."' AND `mpshc_pid` != '' AND `auth` = '0' AND `voided` != '1'");
		if (@mysqli_num_rows($check_mpshc) > 0) {

			$response = close_hosted_checkout_batch($companyid, $location_info['id'], $integration_info['integration3'], $integration_info['integration4'], $dataname, $poslavu_version, $serverid, "Back End");
			$response_parts = explode("|", $response);
			$till_report_info['result_hc'] = $response_parts[0];
			$log_response = $response_parts[1];
			if ($response_parts[0] == "1") {

				$update_transactions = lavu_query("UPDATE `cc_transactions` SET `processed` = '1', `last_mod_ts` = '[1]' WHERE `datetime` > DATE_SUB(NOW(), INTERVAL 2 WEEK) AND `processed` != '1' AND `loc_id` = '[2]' AND `mpshc_pid` != '' AND `auth` = '0' AND `voided` != '1'", time(), $location_info['id']);
				$mid .= "<tr><td align='center'><br>".$response_parts[3]."<br></td></tr>";
				$detail2 = $response_parts[2];
				$detail3 = "Batch # ".$response_parts[4];
				$till_report_info['details_hc'] = $response_parts[5];
				$till_report_info['batch_total_hc'] = $detail2;
				$till_report_info['batch_no_hc'] = $detail3;

			} else {
				
				$mid .= "<tr><td align='center'>".$response_parts[1]."</td></tr>";
			}
			
		} else {

			$till_report_info['result_hc'] = "0";
			$log_response = "No Records To Process";
			$mid .= "<tr><td align='center'><br>No Hosted Checkout transactions to settle...</td></tr>";
		}

		$till_report_info['response_hc'] = $log_response;
		
		$l_vars = array();
		$q_fields = "";
		$q_values = "";
		$l_vars['action'] = "Hosted Checkout batch settled";
		$l_vars['user'] = $username;
		$l_vars['user_id'] = $serverid;
		$l_vars['ipaddress'] = $_SERVER['REMOTE_ADDR'];
		$l_vars['order_id'] = "0";
		$l_vars['data'] = $log_response;
		$l_vars['detail1'] = $integration_info['gateway'];
		$l_vars['detail2'] = $detail2;
		$l_vars['detail3'] = $detail3;
		$l_vars['loc_id'] = $locationid;
		$l_vars['time'] = localize_datetime(date("Y-m-d H:i:s"), $location_info['timezone']);

		$keys = array_keys($l_vars);
		foreach ($keys as $key) {
			if ($q_fields != "") {
				$q_fields .= ", ";
				$q_values .= ", ";
			}
			$q_fields .= "`$key`";
			$q_values .= "'[$key]'";
		}

		$log_this = lavu_query("INSERT INTO `admin_action_log` ($q_fields, `server_time`) VALUES ($q_values, now())", $l_vars);
	}
	
	if ($return_json) {
		http_response_code($error_founded ? 400 : 200);
		echo json_encode([
			'message' => $log_response,
		]);

		exit();
	}

	//If this is set then we know that the calling class was from the executeActions.php file. 
	//if( isset($fromExecuteActions)){
	//	if( stristr($log_response, 'error'))
	//		echo $log_response;
		//else
			//echo $mid;
	//}


	/**
	 * Captures all _unadjusted/uncaptured_ transactions
	 *
	 * @param Request       $request
	 * @param array         $process_info
	 * @param array    		$location_info
	 * @SuppressWarnings(PHPMD.StaticAccess)
	 */
	function settleBatchWithLavuPay(Request $request, $process_info, $location_info) {
		$request->query->has('loc_id') ? null : $request->query->add(array("loc_id" => $location_info['id']));
		$request->query->has('action') ? null : $request->query->add(array("action" => "settle"));
		$response = LavuPayController::route($request, $process_info, $location_info);
		return lavuPayBatchingHandler($response);
	}

	/**
	 * Handles settleBatchWithLavuPay
	 *
	 * @param Symfony\Component\HttpFoundation\JsonResponse  $batchingResponse
	 *
	 */
	function lavuPayBatchingHandler(JsonResponse $batchingResponse) {
		$response = '';
		$message = json_decode($batchingResponse->getContent());
		if ($message && $message->error) {
			throw new Exception($message->error);
		} elseif ($message && $message->batch) {
			$response = "1|Successfully settled " . $message->batch->transactions->count . " transactions";
		} else {
			$response = "1|Success";
		}
		return $response;
	}
?>
