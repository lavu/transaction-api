<?php
if( !$in_lavu ){
	return;
}
global $location_info, $locationid;
session_start();
require_once dirname(dirname(dirname(__FILE__))) . '/resources/dimensional_form.php';

abstract class OrderBase {
	public function __get( $name ){
		if( isset( $this->$name ) ){
			return $this->$name;
		}
		return null;
	}

	public abstract function drawOrderDetailInteractive();
	
	protected function outputRow( $title, $value, $class=''){
		if( !empty($class) ){
			$class = " class=\"{$class}\"";
		}
		echo
			"<tr{$class}>
				<td></td>
				<td class=\"alignRight\">{$title}:</td>
				<td class=\"alignRight\">{$value}</td>
			</tr>";
	}
}

class ComboStub extends OrderBase {
	protected
		$combo_id,
		$combo_name,
		$combo_tiems;

	private function __construct(){
		$this->combo_id = '';
	    $this->combo_name='';
		$this->combo_items = array();
		
	}

	public static function createComboStub($combo_id,$combo_name,$combo_items){
		$result = new ComboStub();
		$result->combo_items = $combo_items;
		$result->combo_id = $combo_id;
		$result->combo_name = $combo_name;
	
		return $result;
	}

	public static function createComboSubFromSources($combo_id,$combo_name,$combo_items){
		
	    return ComboStub::createComboStub($combo_id,$combo_name,$combo_items);
	}

	public function drawOrderDetailInteractive(){

		echo <<<HTML
	<style type="text/css">
		.alignLeft {
			text-align: left;
		}
       .separate{
       padding-right:20px;
        }

		.alignRight {
			text-align: right;
		}

		.alignCenter {
			text-align: center;
		}

		table > td {
			vertical-align: text-top;
			padding: 3px 10px;
		}

	</style>
	<div style="display: table">
		<table style='width: 100%;'>
			<tbody>
				<tr><td colspan="100" class="alignCenter">COMBO SALES</td></tr>
                <tr><td colspan="100" class="alignCenter">&nbsp;</td></tr>
                <tr><td class="alignLeft">Combo ID #:{$this->combo_id}</td></tr>
                <tr><td class="alignLeft">Combo Name :{$this->combo_name}</td></tr>
			</tbody>
		</table>
		<table style="width: 100%;">
			<tbody>
             <tr><td colspan="100"><br><div style="height: 1px; background-color: black; text-align: center">
								<span style="background-color: white; position: relative; top: -0.5em; padding: 5px">
									Combo Items
								</span>
				</div><br></td></tr>

HTML;
  
		foreach( $this->combo_items as $combo_row => $combo ){
		    echo <<<HTML
		    <tr><td colspan="10" class="alignLeft separate">{$combo['item']}</td><td class="alignRight separate">{$combo['sale_qty']}</td></tr>
HTML;

		}
			echo <<<HTML
			</tbody>
		</table>
	</div>
HTML;
	}


	private function outputSection( $section ){
		if( count( $section ) === 0 ){
			return;
		}

		$item = 0;
		foreach( $section as $sectionvalue ){
		    if( $item++ === 0 ){
		
				echo <<<HTML
			<tr>
				<td>
					<table style="width: 100%">
						<tbody>
HTML;
			}
			$sectionvalue->drawOrderDetailInteractive();
		}
		echo <<<HTML
						</tbody>
					</table>
				</td>
			</tr>
HTML;
	}

}

$combo_contents = array();
	$arguments = array(
		'combo_id' => trim($_GET['combo_id']),
	);
	
	$query="SELECT orderSummary.item_id, orderSummary.quantity as sale_qty,orderSummary.item
            FROM
            (
             select order_contents.order_id,order_contents.item_id,order_contents.item,sum(order_contents.quantity) as quantity  from order_contents where order_contents.combo_id='[combo_id]' 
              AND  order_contents.is_combo=0 group by order_contents.item_id
            ) AS orderSummary
            INNER JOIN
            combo_items
              ON orderSummary.item_id = combo_items.menu_item_id and combo_items._deleted=0 
            inner join
             orders 
              on orders.order_id=orderSummary.order_id and orders.void=0 
            GROUP BY orderSummary.item_id";
	$order_contents_query = lavu_query($query, $arguments);
	
	if( $order_contents_query === false || !mysqli_num_rows($order_contents_query)){
		echo 'Combo Items Not Available.';
		return;
	}

	while( $row = mysqli_fetch_assoc( $order_contents_query ) ){
		$order_contents[] = $row;
	}

 $combo_name = array();

	$comboNM_query = lavu_query("SELECT item FROM `order_contents` WHERE `combo_id` = '[combo_id]' and is_combo=1", $arguments );
	if( $comboNM_query === false || !mysqli_num_rows($comboNM_query)){

	} else {
	    while( $row = mysqli_fetch_assoc( $comboNM_query ) ){
	        $combo_name = $row['item'];
		}
	}

$combo_id=$_GET['combo_id'];

$order = ComboStub::createComboStub($combo_id,$combo_name,$order_contents);
echo <<<HTML
<div>
	<style type="text/css" scoped>
		tab-area {
			display: block;
		}
		[hide] {
			display: none;
		}
		nav.order_detail_navigation {
			display: table;
			margin: 0 auto;
		}
		nav.order_detail_navigation > div {
			display: inline-block;
			padding: 2px 10px;
			border: 1px solid rgba(175,175,175,1.0);
			color: rgba(175,175,175,1.0);
			border-left: 0;
			cursor: pointer;
		}

		nav.order_detail_navigation > div:nth-child(1) {
			border-left: 1px solid rgba(175,175,175,1.0) !important;
		}

		nav.order_detail_navigation > div:hover, nav.order_detail_navigation > div[selected] {
			color: white;
			background: rgba(175,175,175,1.0);
		}
		table tr td {
			vertical-align: text-top;
		}
	</style>
	
	<tab-area>
		<div stub>
HTML;
$order->drawOrderDetailInteractive();
echo <<<HTML
		</div> 
	</tab-area>
</div>
HTML;

