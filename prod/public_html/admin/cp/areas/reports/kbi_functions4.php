<?php
ini_set("display_errors","1");
function getNumMembershipCredits($date){
		
		$sql_query = "SELECT COUNT(menu_items.hidden_value2) AS memberships_sold FROM orders LEFT JOIN order_contents ON orders.order_id=order_contents.order_id LEFT JOIN menu_items ON menu_items.id=order_contents.item_id WHERE LEFT(orders.closed,10)>='$date' AND LEFT(orders.closed,10)<DATE_ADD('$date',INTERVAL 172 HOUR) AND menu_items.hidden_value2='membership' AND order_contents.void!='1'";
		$result = lavu_query($sql_query);
		$row = mysqli_fetch_assoc($result);
		return $row['memberships_sold'];
}//getNumMembershipCredits()


function getNumRaceCredits($date){
		
		$sql_query = "SELECT COUNT(menu_items.hidden_value2) AS races_sold FROM orders LEFT JOIN order_contents ON orders.order_id=order_contents.order_id LEFT JOIN menu_items ON menu_items.id=order_contents.item_id WHERE LEFT(orders.closed,10)>='$date' AND LEFT(orders.closed,10)<DATE_ADD('$date',INTERVAL 172 HOUR) AND menu_items.hidden_value2='race credits' AND order_contents.void!='1'";
		$result = lavu_query($sql_query);
		$row = mysqli_fetch_assoc($result);
		return $row['races_sold'];
}//getNumRaceCredits()

function displayTrackData($assoc_1){
		
		$output = array();//return all variables from this function through this array
		/*
		$output["num_new_cust"] = $row['num_new_race_customers'];
		$output["num_total_races"] = $row['num_races'];
		$output["num_total_cust"] = $row['num_total_customers'];
		$output["avg_race_per_cust"] = ($row['num_races']*1)/($row['num_total_customers']*1);
		*/
		
		/* arpc is for average race per customer */
		
		$arpc_cur_week = number_format($assoc_1["current_week"]["avg_race_per_cust"],2);
		$races_sold_cur_week = $assoc_1["current_week"]["num_total_races"];
		$num_cust_cur_week = $assoc_1["current_week"]["num_total_cust"];
		$num_new_cust_cur_week = $assoc_1["current_week"]["num_new_cust"];
		
		$arpc_prior_week = number_format($assoc_1["prior_week"]["avg_race_per_cust"],2);
		$races_sold_prior_week = $assoc_1["prior_week"]["num_total_races"];
		$num_cust_prior_week = $assoc_1["prior_week"]["num_total_cust"];
		$num_new_cust_prior_week = $assoc_1["prior_week"]["num_new_cust"];
		
		$arpc_prior_year = number_format($assoc_1["prior_year"]["avg_race_per_cust"],2);
		$races_sold_prior_year = $assoc_1["prior_year"]["num_total_races"];
		$num_cust_prior_year = $assoc_1["prior_year"]["num_total_cust"];
		$num_new_cust_prior_year = $assoc_1["prior_year"]["num_new_cust"];

/*
		$arpc_prior_year = number_format($assoc_array["prior_year"]["avg_race_per_cust"],2);
		$races_sold_prior_year = $assoc_array["prior_year"]["num_races"];
		$num_cust_prior_year = $assoc_array["prior_year"]["num_customers"];
		$num_new_cust_prior_year = $assoc_array["prior_year"]["num_new_customers"];
		$year_trend = "&#37;".number_format(trend($arpc_cur_week,$arpc_prior_year),2);
	*/	
		
		echo "
				<div class='row'>
							<div class='rowElement re0'>
								Avg Race per Customer
							</div>
							<div class='rowElement re1'>
								$arpc_cur_week
							</div>
							<div class='rowElement re2'>
								$arpc_prior_week
							</div>
							<div class='rowElement re3'>
								$arpc_prior_year 
							</div>
							<div class='rowElement re4'>
								&nbsp;
							</div>
							<div class='rowElement re5'>
								$trend
							</div>
							<div class='rowElement re6'>
								$year_trend
							</div>
					</div><!--row-->
				<div class='clearRow'></div>
		";
		
		$trend = "&#37;".number_format(trend($races_sold_cur_week,$races_sold_prior_week),2);
		$year_trend = "&#37;".number_format(trend($races_sold_cur_week,$races_sold_prior_year),2);
		echo "
				<div class='row'>
							<div class='rowElement re0'>
								Races Sold
							</div>
							<div class='rowElement re1'>
								$races_sold_cur_week
							</div>
							<div class='rowElement re2'>
								$races_sold_prior_week
							</div>
							<div class='rowElement re3'>
								$races_sold_prior_year 
							</div>
							<div class='rowElement re4'>
								&nbsp;
							</div>
							<div class='rowElement re5'>
								$trend
							</div>
							<div class='rowElement re6'>
								$year_trend
							</div>
					</div><!--row-->
				<div class='clearRow'></div>
		";
		
		$trend = "&#37;".number_format(trend($num_cust_cur_week,$num_cust_prior_week),2);
		$year_trend = "&#37;".number_format(trend($num_cust_cur_week,$num_cust_prior_year),2);
		$output["num_customers"]["current_week"] = $num_cust_cur_week;
		$output["num_customers"]["prior_week"] = $num_cust_prior_week;
		$output["num_customers"]["prior"] = $num_cust_prior_year;
		echo "
				<div class='row'>
							<div class='rowElement re0'>
								Customer Totals
							</div>
							<div class='rowElement re1'>
								$num_cust_cur_week
							</div>
							<div class='rowElement re2'>
								$num_cust_prior_week
							</div>
							<div class='rowElement re3'>
								$num_cust_prior_year
							</div>
							<div class='rowElement re4'>
								&nbsp;
							</div>
							<div class='rowElement re5'>
								$trend
							</div>
							<div class='rowElement re6'>
								$year_trend
							</div>
					</div><!--row-->
				<div class='clearRow'></div>
		";
		
		$trend = "&#37;".number_format(trend($num_new_cust_cur_week,$num_new_cust_prior_week),2);
		$year_trend = "&#37;".number_format(trend($num_new_cust_cur_week,$num_new_cust_prior_year),2);
		echo "
				<div class='row'>
							<div class='rowElement re0'>
								Customer New
							</div>
							<div class='rowElement re1'>
								$num_new_cust_cur_week
							</div>
							<div class='rowElement re2'>
								$num_new_cust_prior_week
							</div>
							<div class='rowElement re3'>
								$num_new_cust_prior_year
							</div>
							<div class='rowElement re4'>
								&nbsp;
							</div>
							<div class='rowElement re5'>
								$trend
							</div>
							<div class='rowElement re6'>
								$year_trend
							</div>
							
					</div><!--row-->
				<div class='clearRow'></div>
		";
		/* new customer percentage */
		$cur_week = number_format(calcPercentage($num_new_cust_cur_week,$num_cust_cur_week),2)."&#37;";	
		$prior_week = number_format(calcPercentage($num_new_cust_prior_week,$num_cust_prior_week),2)."&#37;";	
		$prior_year = number_format(calcPercentage($num_new_cust_prior_year,$num_cust_prior_year),2)."&#37;";
		$trend = "&#37;".number_format(trend($cur_week,$prior_week),2);
		$year_trend = "&#37;".number_format(trend($cur_week,$prior_year ),2);	
		echo "
				<div class='row'>
							<div class='rowElement re0'>
								Customer New &#37;
							</div>
							<div class='rowElement re1'>
								$cur_week
							</div>
							<div class='rowElement re2'>
								$prior_week
							</div>
							<div class='rowElement re3'>
								$prior_year
							</div>
							<div class='rowElement re4'>
								&nbsp;
							</div>
							<div class='rowElement re5'>
								$trend
							</div>
							<div class='rowElement re6'>
								$year_trend
							</div>
					</div><!--row-->
				<div class='clearRow'></div>
		";

		return $output;

}//displayTrackData()


function calcTrackData($date,$track_data_option){
	
		$date_arr = explode("-",$date);
		$year = $date_arr[0];
		$month = $date_arr[1];
		$day = $date_arr[2];
		$total_of_weekly_totals = 0;
		$totals = array();
		
		$output = array();
		
		$cur_week_date = date("Y-m-d",mktime(0,0,0,$month*1,$day*1+0,$year*1));
		$prior_week_date = date("Y-m-d",mktime(0,0,0,$month*1,$day*1-7,$year*1));
		$prior_year_date = date("Y-m-d",mktime(0,0,0,$month*1,$day*1+1,$year*1-1));
		
		$output["current_week"] = returnTrackDataInfo($cur_week_date,$track_data_option);
		$output["prior_week"] = returnTrackDataInfo($prior_week_date,$track_data_option);
		$output["prior_year"] = returnTrackDataInfo($prior_year_date,$track_data_option);
				
		return $output;
}//function calcTrackData()

function returnTrackDataInfo($date,$track_data_option){
		

		$output = array();
		$sql_query = "SELECT COUNT(DISTINCT(lk_customers.id)) AS num_race_customers, COUNT(lk_event_schedules.id) AS num_races FROM lk_events LEFT JOIN lk_event_schedules ON lk_event_schedules.eventid=lk_events.id LEFT JOIN lk_customers ON lk_event_schedules.customerid=lk_customers.id WHERE LEFT(lk_events.ms_start>='$date',10) AND LEFT(lk_events.ms_start,10)<DATE_ADD('$date',INTERVAL 172 HOUR) AND lk_events.ms_end!='' ";
		$result = lavu_query($sql_query);
		$row = mysqli_fetch_assoc($result);
		$output["num_total_cust"] = $row['num_race_customers'];
		
		$sql_query2 = "
SELECT COUNT(DISTINCT(lk_customers.id)) AS num_new_race_customers, COUNT(lk_event_schedules.id) AS num_races FROM lk_events LEFT JOIN lk_event_schedules ON lk_event_schedules.eventid=lk_events.id LEFT JOIN lk_customers ON lk_event_schedules.customerid=lk_customers.id WHERE LEFT(lk_events.ms_start>='$date',10) AND LEFT(lk_events.ms_start,10)<DATE_ADD('$date',INTERVAL 172 HOUR) AND lk_events.ms_end!='' AND LEFT(lk_customers.date_created,10)>='$date' AND LEFT(lk_customers.date_created,10)<DATE_ADD('$date',INTERVAL 172 HOUR)";
		
		
		$result2 = lavu_query($sql_query2);
		$row2 = mysqli_fetch_assoc($result2);
		$output["num_new_cust"] = $row2['num_new_race_customers'];
		//$output["num_total_races"] = $row['num_races'];//I'm not going to use this/
		
		
		/* calculate how many customers bought something from the category selected under set track data options (it should be Races) */
		//don't use this block
		$num_total_cust_query = "SELECT COUNT(DISTINCT(orders.order_id)) AS num_total_customers FROM orders LEFT JOIN order_contents ON orders.order_id=order_contents.order_id LEFT JOIN menu_items ON menu_items.id=order_contents.item_id WHERE LEFT(closed,10)>='$date' AND LEFT(closed,10)<DATE_ADD('$date',INTERVAL 172 HOUR) AND menu_items.category_id='$track_data_option'";
		$num_total_cust_res = lavu_query($num_total_cust_query);
		$num_total_cust_row = mysqli_fetch_assoc($num_total_cust_res);
		//$output["num_total_cust"] = $num_total_cust_row['num_total_customers'];
		
		//$output["num_total_cust"] = $row['num_race_customers'];
		
		
		$races_sold_query = "SELECT COUNT(order_contents.item_id) AS num_races, SUM(order_contents.quantity) AS qty FROM orders LEFT JOIN order_contents ON order_contents.order_id=orders.order_id LEFT JOIN menu_items ON menu_items.id=order_contents.item_id WHERE LEFT(orders.closed,10)>='$date' AND LEFT(orders.closed,10)<DATE_ADD('$date', INTERVAL 147 HOUR) AND menu_items.category_id='$track_data_option' AND order_contents.void!='1'";
		$races_sold_res = lavu_query($races_sold_query);
		$races_sold_row = mysqli_fetch_assoc($races_sold_res);
		$output["num_total_races"] = $races_sold_row['qty'];//races sold
		//$output["week_trend"] = trend($current_val,$prior_val
		if(isset( $output["num_total_cust"]) && $output["num_total_cust"]!=0 )
			$output["avg_race_per_cust"] = ($output['num_total_races']*1)/($output["num_total_cust"]*1);
		else
			$output["avg_race_per_cust"]=0;
			//$output["avg_race_per_cust"] = ($row['num_races']*1)/($row['num_race_customers']*1);
		return $output;
}//returnTrackDataInfo()

function displayEventDept($assoc_array_1,$assoc_array_2,$assoc_array_3,$event_department,$cur_week,$prior_week,$prior_year){
		
		$sql_query = "select SUM(IF(`cc_transactions`.`action`='Void',0,IF(`cc_transactions`.`voided`='1',0,IF(`cc_transactions`.`action`='Refund', (0 - (`cc_transactions`.`total_collected`)), `cc_transactions`.`total_collected`)))) AS prepayments_this_week from `cc_transactions` LEFT JOIN `orders` ON `cc_transactions`.`order_id` = `orders`.`order_id`where (LEFT(`cc_transactions`.`datetime`,10) < LEFT(`orders`.`closed`,10) or LEFT(`orders`.`closed`,10)='0000-00-00') and LEFT(`cc_transactions`.`datetime`,10) >= '$cur_week' and LEFT(`cc_transactions`.`datetime`,10) <= DATE_ADD('$cur_week',INTERVAL 172 HOUR) and `cc_transactions`.`loc_id` = `orders`.`location_id` and `cc_transactions`.`voided` != '1'and `orders`.`void` = 0";
		$prepayment_info = lavu_query($sql_query);
		$prepayment_row = mysqli_fetch_assoc($prepayment_info);
		$cur_week_prepayment = number_format($prepayment_row['prepayments_this_week'],2);

		$sql_query = "select SUM(IF(`cc_transactions`.`action`='Void',0,IF(`cc_transactions`.`voided`='1',0,IF(`cc_transactions`.`action`='Refund', (0 - (`cc_transactions`.`total_collected`)), `cc_transactions`.`total_collected`)))) AS prepayments_this_week from `cc_transactions` LEFT JOIN `orders` ON `cc_transactions`.`order_id` = `orders`.`order_id`where (LEFT(`cc_transactions`.`datetime`,10) < LEFT(`orders`.`closed`,10) or LEFT(`orders`.`closed`,10)='0000-00-00') and LEFT(`cc_transactions`.`datetime`,10) >= '$prior_week' and LEFT(`cc_transactions`.`datetime`,10) <= DATE_ADD('$prior_week',INTERVAL 172 HOUR) and `cc_transactions`.`loc_id` = `orders`.`location_id` and `cc_transactions`.`voided` != '1'and `orders`.`void` = 0";
		$prepayment_info = lavu_query($sql_query);
		$prepayment_row = mysqli_fetch_assoc($prepayment_info);
		$prior_week_prepayment = number_format($prepayment_row['prepayments_this_week'],2);
		
		$sql_query = "select SUM(IF(`cc_transactions`.`action`='Void',0,IF(`cc_transactions`.`voided`='1',0,IF(`cc_transactions`.`action`='Refund', (0 - (`cc_transactions`.`total_collected`)), `cc_transactions`.`total_collected`)))) AS prepayments_this_week from `cc_transactions` LEFT JOIN `orders` ON `cc_transactions`.`order_id` = `orders`.`order_id`where (LEFT(`cc_transactions`.`datetime`,10) < LEFT(`orders`.`closed`,10) or LEFT(`orders`.`closed`,10)='0000-00-00') and LEFT(`cc_transactions`.`datetime`,10) >= '$prior_year' and LEFT(`cc_transactions`.`datetime`,10) <= DATE_ADD('$prior_year',INTERVAL 172 HOUR) and `cc_transactions`.`loc_id` = `orders`.`location_id` and `cc_transactions`.`voided` != '1'and `orders`.`void` = 0";
		$prepayment_info = lavu_query($sql_query);
		$prepayment_row = mysqli_fetch_assoc($prepayment_info);
		$prior_year_prepayment = number_format($prepayment_row['prepayments_this_week'],2);

		
		echo "
		<div class='row'>
			<div class='row'>
					<div class='rowElement re0'>
						Prepayments Taken
					</div>
					<div class='rowElement re1'>
						&#36;$cur_week_prepayment
					</div>
					<div class='rowElement re2'>
						&#36;$prior_week_prepayment 
					</div>
					<div class='rowElement re3'>
						&#36;$prior_year_prepayment 
					</div>
					<div class='rowElement re4'>
						&nbsp;
					</div>
					<div class='rowElement re5'>
						&nbsp
					</div>
			</div><!--row-->
		<div class='clearRow'></div>
		";

		
		$cur_week_total = $assoc_array_1[$event_department];
		$prior_week_total = $assoc_array_2[$event_department];
		$prior_year_total = $assoc_array_3[$event_department];
		$menu_category_name = $assoc_array_1["menu_category_map"][$event_department];
		echo "
		<div class='row'>
			<div class='row'>
					<div class='rowElement re0'>
						$menu_category_name
					</div>
					<div class='rowElement re1'>
						&#36;$cur_week_total 
					</div>
					<div class='rowElement re2'>
						&#36;$prior_week_total
					</div>
					<div class='rowElement re3'>
						&#36;$prior_year_total
					</div>
					<div class='rowElement re4'>
						&nbsp; 
					</div>
					<div class='rowElement re5'>
						&nbsp
					</div>
			</div><!--row-->
		<div class='clearRow'></div>
		";
		
		
}//displayEventDept()


function displayCSREfficiency($hours_assoc,$date,$role_id){
		//echo "role_id: $role_id<br>";
		$date_arr = explode("-",$date);
		$year = $date_arr[0];
		$month = $date_arr[1];
		$day = $date_arr[2];
		$total_of_weekly_totals = 0;
		$totals = array();
		for ($i=0;$i<7;$i++){
					$key_date = date("Y-m-d",mktime(0,0,0,$month*1,$day*1+1*$i,$year*1));
					
					$hours_worked = $hours_assoc[$key_date][$role_id]["hours"];
					$day_name = $hours_assoc[$key_date]["day_name"];
					$sql_query  = "SELECT COUNT(id) FROM orders WHERE opened >= DATE_ADD('$key_date', INTERVAL 3 HOUR) AND opened<DATE_ADD('$key_date',INTERVAL 27 HOUR) AND void='0'";
					//echo "sql_query: $sql_query<br>";
					//$sql_query = "SELECT COUNT(id) FROM orders WHERE LEFT(opened,10)>='$key_date' AND LEFT(opened,10)<DATE_ADD('$key_date',INTERVAL 29 HOUR)";
					$num_tickets_res = lavu_query($sql_query);
					$num_tickets_row = mysqli_fetch_assoc($num_tickets_res);
					$num_tickets = $num_tickets_row['COUNT(id)'];
					$tickets_per_hr = (($num_tickets*1)/($hours_worked*1));
					$totals["tickets"] = $totals["tickets"]*1 + $num_tickets*1;
					$totals["total_hours"] = $totals["total_hours"]*1 + $hours_worked*1;
					$totals["tickets_per_hr"] = $totals["tickets_per_hr"]*1 + $tickets_per_hr*1;
					$tickets_per_hr = number_format($tickets_per_hr,2);
					$hours_worked = number_format($hours_worked,2);//now it's pretty
					echo "
							<div class='row'>
								<div class='row'>
										<div class='rowElement re0'>
											$day_name
										</div>
										<div class='rowElement re1'>
											$num_tickets
										</div>
										<div class='rowElement re2'>
											$hours_worked
										</div>
										<div class='rowElement re3'>
											$tickets_per_hr 
										</div>
										<div class='rowElement re4'>
											&nbsp;
										</div>
										<div class='rowElement re5'>
											&nbsp;
										</div>
										<div class='rowElement re6'>
											&nbsp;
										</div>
										<div class='rowElement re7'>
											&nbsp;
										</div>
										<div class='rowElement re9'>
											<span id='csr_effic_".$i."'></span>
										</div>
								</div><!--row-->
								<div class='clearRow'></div>
								
					";
			}//for
			$total_tickets = number_format($totals["tickets"],2);
			$total_hrs = number_format($totals["total_hours"],2);
			$total_tickets_per_hr = number_format($totals["tickets_per_hr"],2);
					echo "
								<div class='row header'>
										<div class='rowElement re0'>
											Total
										</div>
										<div class='rowElement re1'>
											$total_tickets
										</div>
										<div class='rowElement re2'>
											$total_hrs
										</div>
										<div class='rowElement re3'>
											$total_tickets_per_hr 
										</div>
										<div class='rowElement re4'>
											&nbsp;
										</div>
										<div class='rowElement re5'>
											&nbsp;
										</div>
										<div class='rowElement re6'>
											&nbsp;
										</div>
										<div class='rowElement re7'>
											&nbsp;
										</div>
										<div class='rowElement re9'>
											&nbsp;
										</div>
								</div><!--row-->
								<div class='clearRow'></div>
								
					";
					return $total_hrs;//you'll have to use a javascript to put this in the right spot
}//displayCSREfficiency()


function displayLaborUsage($assoc_array,$date,$track_efficiency){
		$css_index = 1;
		
		echo "
		<div class='row header'>
							<div class='rowElement re0'>
								Labor Usage
							</div>
		";

		foreach ($assoc_array AS $key => $value){
				$day_name = $assoc_array[$key]["day_name"];
												echo "<div class='rowElement re".$css_index."'>$day_name</div>";
				$css_index++;
		}//foreach
		echo "				
				<div class='rowElement re9'>
						Totals
				</div>

				</div><!--row-->
				<div class='clearRow'></div>	
		";
		
		/* We will loop throught the assoc array by creating the proper keys, which are the dates of the chosen week */
		$date_arr = explode("-",$date);
		$year = $date_arr[0];
		$month = $date_arr[1];
		$day = $date_arr[2];
		
				
		$num_rows = $assoc_array["num_rows"];
		
		for ($i=1;$i<=count($num_rows)+1;$i++){//$i is for the role id
				
				$key_date_0 = date("Y-m-d",mktime(0,0,0,$month*1,$day*1+0*1,$year*1));
				$key_date_1 = date("Y-m-d",mktime(0,0,0,$month*1,$day*1+1*1,$year*1));
				$key_date_2 = date("Y-m-d",mktime(0,0,0,$month*1,$day*1+2*1,$year*1));
				$key_date_3 = date("Y-m-d",mktime(0,0,0,$month*1,$day*1+3*1,$year*1));
				$key_date_4 = date("Y-m-d",mktime(0,0,0,$month*1,$day*1+4*1,$year*1));
				$key_date_5 = date("Y-m-d",mktime(0,0,0,$month*1,$day*1+5*1,$year*1));
				$key_date_6 = date("Y-m-d",mktime(0,0,0,$month*1,$day*1+6*1,$year*1));
				
				$hours_0 = $assoc_array[$key_date_0][$i]["hours"];
				$hours_1 = $assoc_array[$key_date_1][$i]["hours"];
				$hours_2 = $assoc_array[$key_date_2][$i]["hours"];
				$hours_3 = $assoc_array[$key_date_3][$i]["hours"];
				$hours_4 = $assoc_array[$key_date_4][$i]["hours"];
				$hours_5 = $assoc_array[$key_date_5][$i]["hours"];
				$hours_6 = $assoc_array[$key_date_6][$i]["hours"];
				 
				 $weekly_total = number_format($assoc_array["hi"][$i]["sum_hours"],2);
				 $total_of_weekly_totals += $weekly_total*1;
				
				$role_title = $assoc_array[$date]["role_title"][$i];
				if ($role_title == ""){
						
						for ($o=1;$o<6;$o++){//start at the second entry since the first obviously doesn't have the role_title
								$find_role_title_date = date("Y-m-d",mktime(0,0,0,$month*1,$day*1+$o*1,$year*1));
								if ($assoc_array[$find_role_title_date]["role_title"][$i] != ""){
										$role_title = $assoc_array[$find_role_title_date]["role_title"][$i];
								}//if
								
						}//for
						if ($role_title == ""){
								$role_title = "[Role not parsed]";
						}//if
				}//if
				 //echo "role_title: $role_title<br>";
				// echo "hours_0: $hours_0<br>";
				// exit();
				/* If you need these numbers for calculations, use them before they are formatted */
				$hours_0 = number_format($hours_0,2);
				$hours_1 = number_format($hours_1,2);
				$hours_2 = number_format($hours_2,2);
				$hours_3 = number_format($hours_3,2);
				$hours_4 = number_format($hours_4,2);
				$hours_5 = number_format($hours_5,2);
				$hours_6 = number_format($hours_6,2);
				
				if ($i*1 == $track_efficiency*1){
						$weekly_total_track_hours = $hours_0*1+$hours_1*1+$hours_2*1+$hours_3*1+$hours_4*1+$hours_5*1+$hours_6*1;
				}//if
				
				echo "
				<div class='row'>
					<div class='row'>
							<div class='rowElement re0'>
								$role_title
							</div>
							<div class='rowElement re1'>
								$hours_0
							</div>
							<div class='rowElement re2'>
								$hours_1
							</div>
							<div class='rowElement re3'>
								$hours_2
							</div>
							<div class='rowElement re4'>
								$hours_3
							</div>
							<div class='rowElement re5'>
								$hours_4
							</div>
							<div class='rowElement re6'>
								$hours_5
							</div>
							<div class='rowElement re7'>
								$hours_6
							</div>
							<div class='rowElement re9'>
								$weekly_total
							</div>
					</div><!--row-->
					<div class='clearRow'></div>
					
				";

		}//for
		
		$weekly_total_of_all_categories = number_format($assoc_array["total_labor_hours"],2);
		echo "
				
					<div class='row header'>
							<div class='rowElement re0'>
								&nbsp;
							</div>
							<div class='rowElement re1'>
								&nbsp;
							</div>
							<div class='rowElement re2'>
								&nbsp;
							</div>
							<div class='rowElement re3'>
								&nbsp;
							</div>
							<div class='rowElement re4'>
								&nbsp;
							</div>
							<div class='rowElement re5'>
								&nbsp;
							</div>
							<div class='rowElement re6'>
								&nbsp;
							</div>
							<div class='rowElement re7'>
								Total
							</div>
							<div class='rowElement re9'>
								$weekly_total_of_all_categories
							</div>
					</div><!--row-->
					<div class='clearRow'></div>
					
				";

				return $weekly_total_track_hours;
}//displayLaborUsage()

function calLaborUsage($date){
		$date_arr = explode("-",$date);
		$year = $date_arr[0];
		$month = $date_arr[1];
		$day = $date_arr[2];
		$output = array();//everything leaving this function goes in here
		$roles_ids_arr = array();//so you know which role_ids to call
		//$index = 0;
		$num_rows = 0;//how many employee categories are being calculated
		for ($index=0; $index<7;$index++){
				$cur_date = date("Y-m-d",mktime(0,0,0,$month*1,$day*1+$index*1,$year*1)); 
				$sql_query = "SELECT SUM(clock_punches.hours) AS total_hours, users.role_id AS role_id, roles.title AS title FROM clock_punches LEFT JOIN users ON users.id=clock_punches.server_id LEFT JOIN roles ON users.role_id=roles.id WHERE LEFT(clock_punches.time,10)='$cur_date' AND clock_punches.time_out!='' AND clock_punches._deleted!='1' GROUP BY users.role_id";
				$get_labor_info = lavu_query($sql_query);
				$role_title_set = false;
				while($get_labor_row = mysqli_fetch_assoc($get_labor_info)){
						$role_id = $get_labor_row['role_id'];
						if (mysqli_num_rows($get_labor_info)*1 > $num_rows*1){
								$num_rows = mysqli_num_rows($get_labor_info);
						}//if
						$output[$cur_date]["day_name"] = getDayNameFromDate($cur_date);
						$output[$cur_date][$role_id]["hours"] = $get_labor_row['total_hours'];
						$output["total_labor_hours"] = $output["total_labor_hours"]*1 + $get_labor_row['total_hours']*1;//the total for all categories of employees
						$output[$cur_date]["role_title"][$role_id]= $get_labor_row['title'];
						
						$output["hi"][$role_id]["sum_hours"] = $output["hi"][$role_id]["sum_hours"]*1 +  $get_labor_row['total_hours']*1;//the total for that category of employee
				}//while
		}//for
		$output["num_rows"] = $num_rows;
		return $output;
}//calLaborUsage()

function displayTrackEfficiencyData($assoc_array){
		/* This is not the same as displayTrackData() */
		$total = array();
		$trk_hours = "";
		$display_trk_hours = 0;//we add the value (how many hrs the track guys worked) to the <span id='trk_hours_0'></span> field */
		foreach($assoc_array as $key=> $value){
				$day_name = getDayNameFromDate($assoc_array[$key]["day_name"]);
				$efficiency = number_format($assoc_array[$key]["efficiency"],2);
				
				$real_time = number_format(($assoc_array[$key]["real_time_seconds"]*1/60),2);//convert to minutes
				$total["total_real_time"] = $total["total_real_time"]*1 + $assoc_array[$key]["real_time_seconds"]*1/60;
				
				$open_time = number_format(($assoc_array[$key]["open_time_seconds"]*1/60),2);//convert to minutes
				$total["total_open_time"] = $total["total_open_time"]*1 +$assoc_array[$key]["open_time_seconds"]*1/60;
				
				$races_sold = $assoc_array[$key]["num_races"];
				$total["races_sold"] = $total["races_sold"]*1 + $assoc_array[$key]["num_races"]*1;
				$num_races_sold = $assoc_array[$key]["num_races_sold"];
				$total["races"] = $total["races"]*1 +  $assoc_array[$key]["num_races_sold"]*1;
				$real_time_per_race = number_format($assoc_array[$key]["real_time_per_race"],2);///in minutes
				$total["real_time"] = $total["real_time"]*1 + ($assoc_array[$key]["real_time_seconds"]*1/60);//minutes
				$total["open_time"] = $total["open_time"]*1 + ($assoc_array[$key]["open_time_seconds"]*1/60);//minutes
				
				
				echo "
				<div class='row'>
					<div class='row'>
							<div class='rowElement re0'>
								$day_name
							</div>
							<div class='rowElement re1'>
								$real_time
							</div>
							<div class='rowElement re2'>
								$open_time
							</div>
							<div class='rowElement re3'>
								$efficiency &#37;
							</div>
							<div class='rowElement re4'>
								$num_races_sold
							</div>
							<div class='rowElement re5'>
								$races_sold
							</div>
							<div class='rowElement re6'>
								$real_time_per_race 
							</div>
							<div class='rowElement re7'>
								&nbsp;
							</div>
							<div class='rowElement re9'>
								<span id='trk_hours_".$display_trk_hours."'></span>
							</div>
					</div><!--row-->
					<div class='clearRow'></div>
					";
					$display_trk_hours++;
		}//foreach
		
		/* Evaluate totals */
		$real_time = number_format($total["real_time"],2);
		$open_time = number_format($total["open_time"],2);
		$efficiency = number_format((100*($total["real_time"]*1)/($total["open_time"]*1)),2);
		$races_sold = $total["races"];
		$races = $total["races_sold"];
		echo "
				<div class='row'>
					<div class='row header'>
							<div class='rowElement re0'>
								&nbsp;
							</div>
							<div class='rowElement re1'>
								$real_time 
							</div>
							<div class='rowElement re2'>
								$open_time
							</div>
							<div class='rowElement re3'>
								$efficiency &#37;
							</div>
							<div class='rowElement re4'>
								$races_sold
							</div>
							<div class='rowElement re5'>
								$races
							</div>
							<div class='rowElement re6'>
								$real_time_per_race 
							</div>
							<div class='rowElement re7'>
								&nbsp;
							</div>
							<div class='rowElement re9'>
								&nbsp;
							</div>
					</div><!--row-->
					<div class='clearRow'></div>
					";

		
}//displayTrackEfficiencyData()

function calcTrackEfficiency($date,$track_data_id){
		/* Not the same as calcTrackData() */
		$date_arr = explode("-",$date);
		$year = $date_arr[0];
		$month = $date_arr[1];
		$day = $date_arr[2];
		$output = array();//everything leaving this function goes in here, putting in values into this array can cause 'Wednesday' to show up too many times since this array is processed by a foreach statement later
		$index = 0;
		while ($index <= 6){
				$cur_date = date("Y-m-d",mktime(0,0,0,$month*1,$day*1+$index*1,$year*1)); 
				$sql_query = "SELECT lk_events.ms_start AS ms_start, lk_events.ms_end AS ms_end, SUM(TIMESTAMPDIFF(SECOND,lk_events.ms_start,lk_events.ms_end)) AS time_diff_seconds, lk_tracks.time_start AS open_time_start, lk_tracks.time_end AS open_time_end, COUNT(lk_events.id) AS num_races FROM lk_events LEFT JOIN lk_tracks ON lk_events.locationid=lk_tracks.locationid WHERE LEFT(lk_events.date,10)='$cur_date' AND lk_events._deleted!='1' AND lk_events.ms_start!='' AND lk_events.ms_end!='' AND HOUR(TIMEDIFF(lk_events.ms_end, lk_events.ms_start))<'2'";
				$get_track_res = lavu_query($sql_query);
				$get_track_row = mysqli_fetch_assoc($get_track_res);
				$output[$cur_date]["day_name"] = getDayNameFromDate($cur_date);
				$output[$cur_date]["real_time_seconds"] = $get_track_row['time_diff_seconds'];
				$output[$cur_date]["num_races"] = $get_track_row['num_races'];
				/* Now find out how much time they were open */
				$time_business_opened = $get_track_row['open_time_start'];
				$open_time_hrs = $time_business_opened{0}.$time_business_opened{1};
				$open_time_min = $time_business_opened{2}.$time_business_opened{3};
				$open_time_date = strtotime($cur_date." ".$open_time_hrs.":".$open_time_min.":00");
				$time_business_closed = $get_track_row['open_time_end'];
				$close_time_hrs = $time_business_closed{0}.$time_business_closed{1};
				$close_time_min = $time_business_closed{2}.$time_business_closed{3};
				$close_time_date = strtotime($cur_date." ".$close_time_hrs.":".$close_time_min.":00");
				$output[$cur_date]["open_time_seconds"] = $close_time_date*1 - $open_time_date*1;//how many seconds were they open that day
				$output[$cur_date]["efficiency"] = 100*(($output[$cur_date]["real_time_seconds"]*1)/($output[$cur_date]["open_time_seconds"]*1));
				
				$sql_query = "SELECT * FROM lk_events WHERE date='$cur_date'";//if race is longer than 2 hours it's obviously bogus
				$get_races_sold = lavu_query($sql_query);
				
				//THIS ONE
				//$races_sold_query = "SELECT COUNT(orders.order_id) AS num_races FROM orders LEFT JOIN order_contents ON orders.order_id=order_contents.order_id LEFT JOIN menu_items ON menu_items.id=order_contents.item_id WHERE LEFT(closed,10)='$date' AND menu_items.category_id='$track_data_id'";
				
				$str = "SELECT COUNT(id) FROM lk_event_schedules WHERE eventid='something that doesnt exisit'";//need the first eventid entry to start building the sql string
			    while($get_races_row = mysqli_fetch_assoc($get_races_sold)){
						$str .= " OR eventid='".$get_races_row['id']."' ";
				}//while
				$str .= " AND _deleted!='1' ";
				//echo $str;
				$num_races_sold = lavu_query($str);
				$num_races_row = mysqli_fetch_assoc($num_races_sold);
				$num_races = $num_races_row['COUNT(id)'];
				
				$races_sold_query = "SELECT COUNT(order_contents.item_id) AS num_races_this_day, SUM(order_contents.quantity) AS qty_this_day FROM orders LEFT JOIN order_contents ON order_contents.order_id=orders.order_id LEFT JOIN menu_items ON menu_items.id=order_contents.item_id WHERE LEFT(orders.closed,10)='$cur_date' AND menu_items.category_id='$track_data_id' AND order_contents.void!='1'";
				$races_sold_info = lavu_query($races_sold_query);
				$races_sold_row = mysqli_fetch_assoc($races_sold_info);
				$num_races = $races_sold_row['qty_this_day'];
				
				$output[$cur_date]["num_races_sold"] = $races_sold_row['qty_this_day'];//$num_races;
				
				$output[$cur_date]["num_races_sold"] = $num_races;
				$output[$cur_date]["real_time_per_race"] = ($output[$cur_date]["real_time_seconds"]/60)/($output[$cur_date]["num_races"]*1);
				$index++;
		}//while
		return $output;
}//calcTrackEfficiency(

function getDayNameFromDate($date){
		/* Take in date in YYYY-mm-dd format and return the day name of the week (saturday, sunday,etc...)*/
		$date_info = getdate(strtotime($date));
		$dayName = $date_info["weekday"];
		return $dayName;
}//getDayNameFromDate()


function displaySalesByCategoryInfo($current_week_revenue,$prior_week_revenue,$prior_year_revenue,$menu_category_id,$menu_category_name){
		/* Just put stuff on the screen and do a few other calculations */
		$cur_week_total = $current_week_revenue[$menu_category_id];
		$prior_week_total = $prior_week_revenue[$menu_category_id];
		$prior_year_total = $prior_year_revenue[$menu_category_id];
		$trend = number_format(trend($cur_week_total,$prior_week_total),2);
		$prior_year_trend = number_format(trend($cur_week_total,$prior_year_total),2);
		echo "
					<div class='row'>		
							<div class='rowElement re0'>
								$menu_category_name
							</div>
							<div class='rowElement re1'>
								&#36;$cur_week_total
							</div>
							<div class='rowElement re2'>
								&#36;$prior_week_total
							</div>
							<div class='rowElement re3'>
								&#36;$prior_year_total
							</div>
							<div class='rowElement re4'>
								&nbsp;
							</div>
							<div class='rowElement re6'>
								$trend&#37;
							</div>
							<div class='rowElement re7'>
								$prior_year_trend&#37;
							</div>
							<div class='rowElement re8'>
								&nbsp;
							</div>
					</div><!--row-->
					<div class='clearRow'></div>
		";
		
		
		
}//displaySalesByCategoryInfo();

function trend($current_val,$prior_val){
	if(isset( $prior_val) && $prior_val!=0 )
		$trend_val = 100*( ( ($current_val*1)/($prior_val*1) ) -1);
	else
		$trend_val=0;	
	return $trend_val;
}//trend()

function salesByCategory($date,$menu_category_id){
					
					$output = array();//an associative that stores all values calculated in this function
					
					/* Just getting the people who just signed up  within the specied date range, they are new customers. This value for new_customers is used to calculate the revenue per customer for ALL menu categories. It is not the same one use for the Track Data section. */
					$sql_query = "SELECT COUNT(id) AS num_new_customers FROM lk_customers WHERE LEFT(date_created,10)>='$date' AND LEFT(date_created,10)<DATE_ADD('$date',INTERVAL 172 HOUR) AND LEFT(last_activity,10)>='$date' AND LEFT(last_activity,10)>=DATE_ADD('$date',INTERVAL 172 HOUR)";
					$num_new_cust_results = lavu_query($sql_query);
					$num_new_cust_row = mysqli_fetch_assoc($num_new_cust_results);
					$output["num_new_customers"] = $num_new_cust_row['num_new_customers'];
					
								
					$sql_query = "SELECT order_id FROM orders WHERE orders.closed>='$date' AND orders.closed<DATE_ADD('$date',INTERVAL 172 HOUR)";//one week
					
					$order_ids = lavu_query($sql_query);
					$output["num_customers"] = mysqli_num_rows($order_ids);//considering everything they sell
					 
					$order_ids_array = array();
					$index = 0;
					$str = "";
					while ($order_ids_row = mysqli_fetch_assoc($order_ids)){
							//$order_ids_array[$index] = $order_ids_row['order_id'];
							$str .= "OR order_contents.order_id='".$order_ids_row['order_id']."' ";
							//$index++;
					}//while
					$sql_query = "SELECT order_contents.item_id AS item_id, menu_items.category_id AS category_id, order_contents.subtotal_with_mods AS subtotal_with_mods, menu_categories.name AS menu_category_name, order_contents.order_id AS order_id FROM order_contents LEFT JOIN menu_items ON order_contents.item_id=menu_items.id LEFT JOIN menu_categories ON menu_items.category_id=menu_categories.id WHERE order_contents.order_id='something_that_doesnt_exist' $str";
					/* How many of these customers are members? */
					
					
					$order_contents_info = lavu_query($sql_query);
					$menu_category_map = array();
					$oid_index = 0;
					$output["num_race_orders"][$order_id] = 0;//how many races were run, not events, but each person on the track counts as one race each time
					$output["list_of_order_ids"][$oid_index] = "";//we keep track of unique order ids, this is how many orders containing races were placed
					$output["menu_category_map"]= array();
					while ($row = mysqli_fetch_assoc($order_contents_info)){
							$cur_item_id = $row['item_id'];
							$cur_menu_category = $row['category_id'];
							$menu_category_name = $row['menu_category_name'];
							$order_id = $row['order_id'];
							$output[$cur_menu_category] = $output[$cur_menu_category]*1 + $row['subtotal_with_mods']*1;//should be the total revenue for this menu category
							$output["total_revenue"] = $output["total_revenue"]*1 + $row['subtotal_with_mods']*1;
							//$output["num_customers_by_menu_category"][$cur_menu_category] += 1;
							
							if (!in_array($menu_category_name,$output["menu_category_map"])){
									/* This if statement builds a key value map one can use to find the name of the menu category from the menu category id*/
									$output["menu_category_map"][$cur_menu_category ] = $menu_category_name;
							}//if 
							if ( ($cur_menu_category*1 == $menu_category_id*1) ){//how many of this category were ordered
									$output["num_race_orders"][$order_id] = $output["num_race_orders"][$order_id]*1 + 1;
									if (!in_array($order_id, $output["list_of_order_ids"])){
											$output["list_of_order_ids"][$oid_index] = $order_id;
											$oid_index++;
									}//if
							}//if
					}//while
					$output["sql"] = $sql_query;
					return $output;
}//salesByCategory()


function pad($str,$length){
		while (strlen($str)*1 < $length*1){
			$str = "0".$str;
		}
		return $str;
}//pad



function displayForm(){
		return;
}//displayForm()

function calcPercentage($num,$total){
	if( isset( $total ) && $total !=0)
		$percentage = 100*($num*1)/($total*1);
	else
		$percentage=0;
	
	return $percentage;
}//calcPercentage()

function checkIndexes(){
		/* Make sure all the appropriate indexes have been set*/
		
		$does_index_exist = lavu_query("SHOW INDEX FROM lk_event_schedules WHERE KEY_NAME = 'eventid_index'");
		if (mysqli_num_rows($does_index_exist) < 1){
				//echo "Creating index".mysqli_num_rows($does_index_exist)."<br/>";
				$create_index = lavu_query("CREATE INDEX eventid_index ON lk_event_schedules (eventid);");
		}//if

		$does_index_exist = lavu_query("SHOW INDEX FROM lk_event_schedules WHERE KEY_NAME = 'customerid'");
		if (mysqli_num_rows($does_index_exist) < 1){
				//echo "Creating index".mysqli_num_rows($does_index_exist)."<br/>";
				$create_index = lavu_query("CREATE INDEX customerid ON lk_event_schedules (customerid);");
		}//if
				
		$does_index_exist = lavu_query("SHOW INDEX FROM lk_events WHERE KEY_NAME = 'ms_end_index'");
		if (mysqli_num_rows($does_index_exist) < 1){
				//echo "Creating index".mysqli_num_rows($does_index_exist)."<br/>";
				$create_index = lavu_query("CREATE INDEX ms_end_index ON lk_events (ms_end);");
		}//if

		/* Adjust fields to proper data types. Some queries take too long to execute if the data type is not int 11 */
		$sql_query = "SHOW FIELDS FROM lk_event_schedules";
		$result = lavu_query($sql_query);
		$type_arr = array();
		
		while ($row = mysqli_fetch_assoc($result)){
				/* we need this to see what data type the lk_event_schedules.eventid field is, we need it to be int(11) */
				$type_arr[$row['Field']]["type"] = $row['Type'];
		}//while
		$event_id_data_type = $type_arr["eventid"]["type"]."<br>";
		//echo "datatype: $event_id_data_type<br>";
		if ($event_id_data_type != "int(11)"){
				$sql_query = "ALTER TABLE lk_event_schedules MODIFY column eventid int(11);";
				//echo "Altered data type of lk_event_schedules.eventid to int(11) for faster processing<br>";
				lavu_query($sql_query);
		}//if
			
}//checkIndexes()

?>


<?php
/*
$index = 0;
		$cur_date = date("Y-m-d",mktime(0,0,0,$date_month*1,$date_day*1+$index,$date_year*1));
		echo "
				<div class='row'>
					<div class='row'>
							<div class='rowElement re0'>
								$day_name
							</div>
							<div class='rowElement re1'>
								$real_time
							</div>
							<div class='rowElement re2'>
								$open_time
							</div>
							<div class='rowElement re3'>
								$efficiency &#37;
							</div>
							<div class='rowElement re4'>
								$num_races_sold
							</div>
							<div class='rowElement re5'>
								$races_sold
							</div>
							<div class='rowElement re6'>
								$real_time_per_race 
							</div>
							<div class='rowElement re7'>
								&nbsp;
							</div>
							<div class='rowElement re9'>
								
							</div>
					</div><!--row-->
					<div class='clearRow'></div>
					
		";
*/
?>