<?php
	if($in_lavu)
	{
		$request_string = "";
		$send_log_search_fields = array("send_log_server", "send_log_table", "send_log_date_from", "send_log_date_to");
		foreach($send_log_search_fields as $field) {
			$request_string .= $field."=\" + escape(document.send_log_search.".$field.".value) + \"&";
		}

		$send_log_location = $locationid;
		$send_log_server = (isset($_GET['send_log_server']))?$_GET['send_log_server']:0;
		$send_log_table = (isset($_GET['send_log_table']))?$_GET['send_log_table']:"";
		$send_log_date_from = (isset($_GET['send_log_date_from']))?$_GET['send_log_date_from']:date("Y-m-d", (time() - 86400));
		$send_log_date_to = (isset($_GET['send_log_date_to']))?$_GET['send_log_date_to']:date("Y-m-d", time());

		// based on date format setting
		$date_setting = "date_format";
		$location_date_format_query = rpt_query( "SELECT `location`, `setting`, `value` FROM `config` WHERE `setting` = '[1]'",  $date_setting);
		if( !$location_date_format_query || !mysqli_num_rows($location_date_format_query)){
			$date_format = array("value" => 2);
		}else{
			$date_format = mysqli_fetch_assoc( $location_date_format_query );
		}

		if ($date_format['value']=='1'){
			$send_log_date_from2 = (isset($_GET['send_log_date_from']))?$_GET['send_log_date_from']:date("d-m-Y", (time() - 86400));
			$send_log_date_to2 = (isset($_GET['send_log_date_to']))?$_GET['send_log_date_to']:date("d-m-Y", time());
		}

		if ($date_format['value']=='2'){
			$send_log_date_from2 = (isset($_GET['send_log_date_from']))?$_GET['send_log_date_from']:date("m-d-Y", (time() - 86400));
			$send_log_date_to2 = (isset($_GET['send_log_date_to']))?$_GET['send_log_date_to']:date("m-d-Y", time());
		}

		if ($date_format['value']=='3'){
			$send_log_date_from2 = (isset($_GET['send_log_date_from']))?$_GET['send_log_date_from']:date("Y-m-d", (time() - 86400));
			$send_log_date_to2 = (isset($_GET['send_log_date_to']))?$_GET['send_log_date_to']:date("Y-m-d", time());
		}

		$filter_code = "";
		if ($send_log_server != 0) {
			$filter_code .= " AND server_id = '".$send_log_server."'";
		}
		if ($send_log_table != "") {
			$filter_code .= " AND tablename = '".$send_log_table."'";
		}
		if ($send_log_date_from != "") {
			$change_format = explode("-", $send_log_date_from);
			switch ($date_format['value']){
				case 1:$send_log_date_from = $change_format[2]."-".$change_format[1]."-".$change_format[0];
				break;
				case 2:$send_log_date_from = $change_format[2]."-".$change_format[0]."-".$change_format[1];
				break;
				case 3:$send_log_date_from = $change_format[0]."-".$change_format[1]."-".$change_format[2];
				break;
			}
			$filter_code .= " AND time_sent >= '".$send_log_date_from." 00:00:00'";
		}
		if ($send_log_date_to != "") {
			$change_format = explode("-", $send_log_date_to);
			switch ($date_format['value']){
				case 1:$send_log_date_to = $change_format[2]."-".$change_format[1]."-".$change_format[0];
				break;
				case 2:$send_log_date_to = $change_format[2]."-".$change_format[0]."-".$change_format[1];
				break;
				case 3:$send_log_date_to = $change_format[0]."-".$change_format[1]."-".$change_format[2];
				break;
			}
			$filter_code .= " AND time_sent <= '".$send_log_date_to." 23:59:59'";
		}

		$display .= "
		<tr>
			<td align='center'>
				<table cellspacing='0' cellpadding='0' width='810px'>
					<tr><td class='inner_panel_top'></td></tr>
					<tr>
						<td class='inner_panel_mid' align='center' style='padding:10px 15px 15px 15px'>
							<form name='send_log_search'>
								<table cellspacing='0' cellpadding='5'>
									<tr><td align='center'><b>Search Criteria</b></td></tr>
									<tr>
										<td align='center'>
											<table cellspacing='0' cellpadding='5'>
												<tr>
													<td align='right' style='padding:5px 5px 5px 20px'>Server:</td>
													<td align='left'>
														<select name='send_log_server'>";
		$get_servers = rpt_query("SELECT * FROM users ORDER BY f_name, l_name ASC");
		if (@mysqli_num_rows($get_servers) == 1) {
			$extract = mysqli_fetch_array($get_servers);
			$display .= "<option value='".$extract['id']."'>".$extract['f_name']." ".$extract['l_name']."</option>";
		} else if (@mysqli_num_rows($get_servers) > 1) {
			if ($send_log_server == 0) {
				$selected = " selected";
			} else {
				$selected = "";
			}
			$display .= "<option value='0'".$selected.">All Servers</option>";
			while ($extract = mysqli_fetch_array($get_servers)) {
				if ($extract['id'] == $send_log_server) {
					$selected = " selected";
				} else {
					$selected = "";
				}
				$display .= "<option value='".$extract['id']."'".$selected.">".$extract['f_name']." ".$extract['l_name']."</option>";
			}
		} else {
			$display .= "<option value='0' selected>Error loading servers</option>";
		}
		$display .= "</select>
													</td>
													<td align='right' style='padding:5px 5px 5px 20px'>Table:</td>
													<td align='left'><input type='text' name='send_log_table' size='10' value='".$send_log_table."'></td>
												</tr>
											</table>
											<input type='hidden' name='mode' value='4'>
										</td>
									</tr>
									<tr>
										<td align='center'>
											<table cellspacing='0' cellpadding='5'>
												<tr>
													<td align='right'>From date:</td>
													<td align='left'><input type='text' name='send_log_date_from' size='11' value='".$send_log_date_from2."' onFocus='scwShow(this,event);' onClick='scwShow(this,event);'></td>
													<td align='right' style='padding:5px 5px 5px 20px'>To date:</td>
													<td align='left'><input type='text' name='send_log_date_to' size='11' value='".$send_log_date_to2."' onFocus='scwShow(this,event);' onClick='scwShow(this,event);'></td>
												</tr>
											</table>
										</td>
									</tr>
									<tr><td align='center'><input type='button' onclick='window.location = \"index.php?mode={$section}_{$mode}&".$request_string."\";' value='Search'></td></tr>
								</table>
							</form>
						</td>
					</tr>
					<tr><td class='inner_panel_bottom'></td></tr>
				</table>
			</td>
		</tr>";
		$get_send_log = rpt_query("SELECT * FROM send_log WHERE id > '0'".$filter_code." ORDER BY time_sent DESC LIMIT 1500");
		$decimal_char = $location_info['decimal_char'];
		if($location_info['disable_decimal'] == 0){
			$decimal_char = ".";
			$thousands_char = ",";
		}else{
			$thousands_char = ($decimal_char == ".")?",":".";
		}
		if (@mysqli_num_rows($get_send_log) > 0) {
			$rows = mysqli_num_rows($get_send_log);
			$row = 1;
			$bottom_border = " border-bottom:1px solid #999999;";
			$display .= "<tr>
				<td align='center'>
					<table cellspacing='0' cellpadding='0' width='900px'>
						<tr><td class='inner_panel_lg_top'></td></tr>
						<tr>
							<td class='inner_panel_lg_mid' align='center' style='padding:15px 15px 15px 15px'>
								<table cellspacing='1' cellpadding='5'>
									<tr class='order_list_header'><td align='center'><b>Order ID</b></td><td align='center'><b>Location</b></td><td align='center'><b>Table</b></td><td align='center'><b>Server</b></td><td align='center'><b>Server ID</b></td><td align='center'><b>Total</b></td><td align='center'><b>Time Sent</b></td></tr>";
			while ($extract = mysqli_fetch_array($get_send_log)) {
				if ($row == $rows) {
					$bottom_border = "";
				}
				$total_amount = number_format($extract['total'], $location_info['disable_decimal'], $decimal_char, $thousands_char);
				$split_date = explode(" ", $extract['time_sent']);
				$change_format = explode("-", $split_date[0]);
				switch ($date_format['value']){
					case 1:$extract['time_sent'] = $change_format[2]."-".$change_format[1]."-".$change_format[0]." ".$split_date[1];
					break;
					case 2:$extract['time_sent'] = $change_format[1]."-".$change_format[2]."-".$change_format[0]." ".$split_date[1];
					break;
					case 3:$extract['time_sent'] = $change_format[0]."-".$change_format[1]."-".$change_format[2]." ".$split_date[1];
					break;
				}
				$display .= "<tr class='order_list'><td align='right' style='padding:5px 10px 5px 15px;".$bottom_border."'>".str_pad($extract['order_id'], 8, "0", STR_PAD_LEFT)."</td><td align='left' style='padding:5px 20px 5px 5px;".$bottom_border."'>".$extract['location']."</a></td><td align='left' style='padding:5px 20px 5px 5px;".$bottom_border."'>".$extract['tablename']."</td><td align='left' style='padding:5px 20px 5px 5px;".$bottom_border."'>".$extract['send_server']."</td><td align='left' style='padding:5px 20px 5px 5px;".$bottom_border."'>".$extract['send_server_id']."</td><td align='right' style='padding:5px 15px 5px 15px;".$bottom_border."'>$".$total_amount."</td><td align='center' style='padding:5px 20px 5px 20px;".$bottom_border."'>".$extract['time_sent']."</td></tr>
				<tr>";
				$row++;
			}
			$display .= "</table>
							</td>
						</tr>
						<tr><td class='inner_panel_lg_bottom'></td></tr>
					</table>
				</td>
			</tr>
			<tr><td>&nbsp;</td></tr>";
		} else {
			$display .= "<tr><td align='center' style='padding:15px 0px 15px 0px'>There are no send log records in the system which meet your search criteria...</td></tr>";
		}
		echo "<input type='hidden' name='setting_format' id='setting_format' value='".$date_format['value']."'>";
		echo "<script language='javascript' src='resources/scw.js'></script>";
		echo "<table>";
		echo $display;
		echo "</table>";
	}
?>
