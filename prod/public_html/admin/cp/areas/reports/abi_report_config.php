<?php
/**
 * Created by PhpStorm.
 * User: hayat
 * Date: 11/7/17
 * Time: 4:10 PM
 */

/*
 * Sample array for data, make it json object
$sampleArray[2017][1] = 648000;
$sampleArray[2017][2] = 398000;
$sampleArray[2017][3] = 477000;
$sampleArray[2017][4] = 382000;
$sampleArray[2017][5] = 344000;
$sampleArray[2017][6] = 401000;
$sampleArray[2017][7] = 299000;
$sampleArray[2017][8] = 422000;
$sampleArray[2017][9] = 233000;
$sampleArray[2017][10] = 1240000;
$sampleArray[2017][11] = 898000;
$sampleArray[2017][12] = 1008000;
$sampleArray[2017]['Q1'] = 1523000;
$sampleArray[2017]['Q2'] = 1127000;
$sampleArray[2017]['Q3'] = 954000;
$sampleArray[2017]['Q4'] = 3146000;
$sampleArray[2017]['FY'] = 6750000;
$sampleArray[2018][1] = 1045000;
$sampleArray[2018][2] = 1045000;
$sampleArray[2018][3] = 1132000;
$sampleArray[2018][4] = 1220000;
$sampleArray[2018][5] = 1394000;
$sampleArray[2018][6] = 1568000;
$sampleArray[2018][7] = 1742000;
$sampleArray[2018][8] = 2091000;
$sampleArray[2018][9] = 2265000;
$sampleArray[2018][10] = 2439000;
$sampleArray[2018][11] = 2439000;
$sampleArray[2018][12] = 2526000;
$sampleArray[2018]['Q1'] = 3223000;
$sampleArray[2018]['Q2'] = 4181000;
$sampleArray[2018]['Q3'] = 6098000;
$sampleArray[2018]['Q4'] = 7404000;
$sampleArray[2018]['FY'] = 20907000;

echo json_encode($sampleArray, true);
*/

$emailSchedule = array("Daily", "Weekly");

$emailReportType = array("Sales", "Trends");

$host = 'admin.poslavu.com';
$emailImageBaseURL = 'http://admin.poslavu.com/emails/abi/';
$baseEmailImageDir = '/home/poslavu/public_html/admin/emails/abi/';
$abiDashBoardURL = 'https://'.$host.'/cp/areas/abi-dashboard/dashboard';
$defaultDataname = 'anheuser_busch';
$defaultChainId = '564';
$from_mail = "no-reply@lavu.com";
$from_name = "Lavu";
$replyto = "no-reply@lavu.com";
