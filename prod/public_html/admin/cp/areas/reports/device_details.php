<?php
if( !$in_lavu ){
	return;
}
 global $location_info, $locationid;
session_start();

function timestampToISO( $time ){
	global $location_info;
	$tz = date_default_timezone_get();
	if( !empty( $location_info['timezone'] ) ){
		date_default_timezone_set( $location_info['timezone'] );
	}

	$ts = strtotime( $time );
	$result =  date('c', $ts);
	date_default_timezone_set( $tz );
	return $result;
}

abstract class ItemBase {
	public function __get( $name ){
		if( isset( $this->$name ) ){
			return $this->$name;
		}
		return null;
	}

	protected function outputRow( $title, $value, $class=''){
		if( !empty($class) ){
			$class = " class=\"{$class}\"";
		}
		echo
			"<tr{$class}>
				<td></td>
				<td class=\"alignRight\">{$title}:</td>
				<td class=\"alignRight\">{$value}</td>
			</tr>";
	}
}

class ApplicationInformation extends ItemBase {
	protected
		$app_name,
		$poslavu_version,
		$usage_type;

	private function __construct(){
		$this->app_name = null;
		$this->poslavu_version = null;
		$this->usage_type = null;
	}

	public static function applicationInformation( $device_detail ){
		$result = new ApplicationInformation();
		$result->app_name = $device_detail['app_name'];
		$result->poslavu_version = $device_detail['poslavu_version'];
		$result->usage_type = $device_detail['usage_type'];
		return $result;
	}
}

class LocationInformation extends ItemBase{
	protected
		$company_id,
		$data_name,
		$location_id;

	private function __construct(){
		$this->company_id = null;
		$this->data_name = null;
		$this->location_id = null;
	}

	public static function locationInformation( $device_detail ){
		$result = new LocationInformation();
		$result->company_id = $device_detail['company_id'];
		$result->data_name = $device_detail['data_name'];
		$result->location_id = $device_detail['loc_id'];
		return $result;
	}
}

class iOSDeviceFactory extends ItemBase {
	public static function iOSDevice( $device_detail ){
		switch( $device_detail['model'] ){
			case 'iPod touch' : {
				$result = iPodTouch::iPodTouch( $device_detail );
				break;
			} case 'iPad' : {
				$result = iPad::iPad( $device_detail );
				break;
			} case 'iPhone' : {
				$result = iPhone::iPhone( $device_detail );
				break;
			} default : {
				return null;
			}
		}
		return $result;
	}
}

abstract class iOSDevice {
	protected
		$app_info,
		$device_time,
		$holding_order_id,
		$id,
		$inactive,
		$ip_address,
		$last_checkin,
		$lavu_admin,
		$locationInfo,
		$MAC,
		$model,
		$name,
		$needs_reload,
		$prefix,
		$remote_ip,
		$reseller_admin,
		$slot_claimed,
		$specific_model,
		$system_name,
		$system_version,
		$UDID,
		$UUID;

	protected function __construct(){
		$this->app_info = null;
		$this->device_time = null;
		$this->holding_order_id = null;
		$this->id = null;
		$this->inactive = null;
		$this->ip_address = null;
		$this->last_checkin = null;
		$this->lavu_admin = null;
		$this->locationInfo = null;
		$this->MAC = null;
		$this->model = null;
		$this->name = null;
		$this->needs_reload = null;
		$this->prefix = null;
		$this->remote_ip = null;
		$this->reseller_admin = null;
		$this->slot_claimed = null;
		$this->specific_model = null;
		$this->system_name = null;
		$this->system_version = null;
		$this->UDID = null;
		$this->UUID = null;
	}

	protected function iosDevice( $device_detail ) {
		$this->app_info = ApplicationInformation::applicationInformation( $device_detail );
		$this->device_time = $device_detail['device_time'];
		$this->holding_order_id = $device_detail['holding_order_id'];
		$this->id = $device_detail['id'];
		$this->inactive = $device_detail['inactive'];
		$this->ip_address = $device_detail['ip_address'];
		$this->last_checkin = $device_detail['last_checkin'];
		$this->lavu_admin = $device_detail['lavu_admin'];
		$this->locationInfo = LocationInformation::locationInformation( $device_detail );
		$this->MAC = $device_detail['MAC'];
		$this->model = $device_detail['model'];
		$this->name = $device_detail['name'];
		$this->needs_reload = $device_detail['needs_reload'];
		$this->prefix = $device_detail['prefix'];
		$this->remote_ip = $device_detail['remote_ip'];
		$this->reseller_admin = $device_detail['reseller_admin'];
		$this->slot_claimed = $device_detail['slot_claimed'];
		$this->specific_model = $device_detail['specific_model'];
		$this->system_name = $device_detail['system_name'];
		$this->system_version = $device_detail['system_version'];
		$this->UDID = $device_detail['UDID'];
		$this->UUID = $device_detail['UUID'];
	}

	public function outputDetails(){

		$last_check_in_time = $this->device_time;
		//$last_check_in_time_ISO = timestampToISO( $last_check_in_time );
		$date_setting = "date_format";
		$location_date_format_query = lavu_query( "SELECT `location`, `setting`, `value` FROM `config` WHERE `setting` = '[1]'",  $date_setting);
		if( !$location_date_format_query || !mysqli_num_rows($location_date_format_query)){
			$date_format = array("value" => 2);
		}else
			$date_format = mysqli_fetch_assoc( $location_date_format_query );
		$split_date = explode(" ", $last_check_in_time);
		$change_format = explode("-", $split_date[0]);
		switch ($date_format['value']){
			case 1:$show_date = $change_format[2]."/".$change_format[1]."/".$change_format[0].", ".$split_date[1];
			break;
			case 2:$show_date = $change_format[1]."/".$change_format[2]."/".$change_format[0].", ".$split_date[1];
			break;
			case 3:$show_date = $change_format[0]."/".$change_format[1]."/".$change_format[2].", ".$split_date[1];
			break;
		}

		echo $this->getDeviceImage();
		echo '<br />';
		echo '<table style="margin: 0 auto;"><tbody>';
		echo '<tr><td colspan="2" style="text-align: center; font-size: 0.9em;">' . $this->UUID . '</td>';
		echo '<tr><td class="alignRight"><b>Name</b>:</td><td>' . $this->name . '</td>';
		echo '<tr><td class="alignRight"><b>iOS Version</b>:</td><td>' . $this->system_version . '</td>';
		//echo '<tr><td class="alignRight"><b>Last Check In</b>:</td><td>' . "<time title='{$last_check_in_time }' datetime='{$last_check_in_time_ISO}'></time>"  . '</td>';
		echo '<tr><td class="alignRight"><b>Last Check In</b>:</td><td>' . $show_date  . '</td>';
		echo '<tr><td class="alignRight"><b>Status</b>:</td><td>' . ($this->inactive ? 'Inactive' : 'Active') . '</td>';
		echo '<tr><td class="alignRight"><b>Application</b>:</td><td>' . $this->app_info->app_name . '</td>';
		echo '<tr><td class="alignRight"><b>Version</b>:</td><td>' . $this->app_info->poslavu_version . '</td>';
		echo '<tr><td class="alignRight"><b>Type</b>:</td><td>' . $this->app_info->usage_type . '</td>';
		echo '</tbody></table>';
	}

	public abstract function getDeviceImage();
}

class iPhone extends iOSDevice {
	protected function __construct(){
		parent::__construct();
	}

	public static function iPhone( $device_detail ){
		$result = new iPhone();
		$result->iOSDevice( $device_detail );
		return $result;
	}

	public function getDeviceImage(){
		return '<img style="margin: 0 auto; display: table;" src="images/iPhone_5.png"></img>';
	}
}

class iPodTouch extends iOSDevice {
	protected function __construct(){
		parent::__construct();
	}

	public static function iPodtouch( $device_detail ){
		$result = new iPodTouch();
		$result->iOSDevice( $device_detail );
		return $result;
	}

	public function getDeviceImage(){
		return '<img style="margin: 0 auto; display: table;" src="images/ipod_touch.png"></img>';
	}
}

class iPad extends iOSDevice {
	protected function __construct(){
		parent::__construct();
	}

	public static function iPad( $device_detail ){
		$result = new iPad();
		$result->iOSDevice( $device_detail );
		return $result;
	}

	public function getDeviceImage(){
		return '<img style="margin: 0 auto; display: table;" src="images/iPad.png"></img>';
	}
}

$device_details = null;
{
	$arguments = array(
		'UUID' => $_GET['UUID'],
		'loc_id' => $locationid
	);
	$device_details_query = lavu_query("SELECT * FROM `devices` WHERE `UUID` = '[UUID]'", $arguments );
	if( $device_details_query === false || !mysqli_num_rows($device_details_query)){
		echo 'No Devices Found';
		return;
	}
	if( $device_details = mysqli_fetch_assoc( $device_details_query ) ){
	} else {
		echo 'error?';
		return;
	}
}

$device = iOSDeviceFactory::iOSDevice( $device_details );
$device->outputDetails();