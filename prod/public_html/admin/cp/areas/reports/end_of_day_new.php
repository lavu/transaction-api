<?php

/**
TODO:
lavu_query need to replace by mlavu_query. There are tables need to create in main db (refer jira ticket for new DDLs) and since for QA cannot update prod main db for new tables hence created those tables under location database poslavu_demo_shuja_ind_db. Post QA is done and prior to prod deployment, will replace below lavu_query with mlavu_query
*/if (!$in_lavu)
{
	exit();
}

$data_name = admin_info('dataname');

require_once(__DIR__."/../../../lib/gateway_functions.php");
require_once(__DIR__."/../../../lib/mpshc_functions.php");

set_time_limit(2700);

$poslavu_version	= admin_info('version');
$device_time		= localize_datetime(date("Y-m-d H:i:s"), $location_info['timezone']);
$server_name		= admin_info('fullname');
$date_setting = "date_format";
$location_date_format_query = rpt_query( "SELECT `location`, `setting`, `value` FROM `config` WHERE `setting` = '[1]'",  $date_setting);
if( !$location_date_format_query || !mysqli_num_rows($location_date_format_query)){
	$date_format = array("value" => 2);
}else{
	$date_format = mysqli_fetch_assoc( $location_date_format_query );
}

$num = rand(0, 99);

$allow_ULA = TRUE;
if (sessvar("LULA") && $recvd_sess_id!="" && (time() < ((int)sessvar("LULA") + 3600)))
{
	$allow_ULA = FALSE;
}

if ((($num % 15) == 0) && $allow_ULA)
{
	$update_main_record = mlavu_query("UPDATE `poslavu_MAIN_db`.`restaurants` SET `last_activity` = now() WHERE `id` = '[1]'", admin_info('companyid'));
	set_sessvar("LULA", time());
}

mlavu_close_db();

$title				= "End of Day Report";
$display			= "";
$mid				= "";
$resp				= "";
$layer_code			= "";
$location			= $locationid;
$user				= getvar("user");
$cc_settle			= getvar("cc_settle");
$confirm_batch_ts	= getvar("cbts");

$found_unprocessed_transactions = FALSE;

$trigger_lls_mgmt_update = getvar("llsmgmtccu");
$lls_mgmt_update_alert = "";
$lls = (in_array($location_info['use_net_path'], array("1", "2")) && !strstr($location_info['net_path'],".lavu") && !strstr($location_info['net_path'], "poslavu.com") && !strstr($location_info['net_path'], "lavulite.com"));
if ($lls)
{
	require_once("/home/poslavu/public_html/admin/cp/resources/msync_functions.php");
	if ($trigger_lls_mgmt_update)
	{
		if ((time() - $confirm_batch_ts) < 300)
		{
			$lls_mgmt_update_alert = "An update of your Management Area has been initiated. Please check back in a few minutes.";

			$repo_dir = (llsFileStructureContainsString("ZendGuardLoader") && !llsFileStructureContainsString("ioncube_loader"))?"lls-zendguard":"lls-ioncube";

			schedule_msync($data_name, $locationid, "sync database");
			schedule_msync($data_name, $locationid, "copy remote file", $repo_dir."/lib/inapp_management.php", "../lib/inapp_management.php");
			schedule_msync($data_name, $locationid, "copy remote file", $repo_dir."/lib/management/mgmt_menu.php", "../lib/management/mgmt_menu.php");
			schedule_msync($data_name, $locationid, "copy remote file", $repo_dir."/lib/management/cc_auths.php", "../lib/management/cc_auths.php");
			schedule_msync($data_name, $locationid, "read file structure");

		}
		else
		{
			$lls_mgmt_update_alert = "Your window for triggering a Management Area update has expired. Please try again if necessary.";
		}
	}
}

$decimal_places = $location_info['disable_decimal'];
$smallest_money = (1 / pow(10, $decimal_places));
$market_type = $location_info['market_type'];
$send_debug_messages = $location_info['gateway_debug'];
if ($location_info['decimal_char'] != "") $decimal_char = $location_info['decimal_char'];
$thousands_char	= ($decimal_char == ".")?",":".";

$cc_pay_type_ids = array("2");
$alt_payment_types = array();
$alt_paid_grand_totals = array();
$alt_column = speak("Gift");
$alt_m_title = speak("Gift Total");
$get_alt_payment_types = rpt_query("SELECT * FROM `payment_types` WHERE `loc_id` = '[1]' AND `id` > '3' ORDER BY `_order` ASC, `type` ASC", $location_info['id']);
if (@mysqli_num_rows($get_alt_payment_types) > 0)
{
	$alt_payment_types[] = array(3,speak("Gift Certificate"));
	$alt_paid_grand_totals[] = 0;
	while ($alt_payment_info = mysqli_fetch_assoc($get_alt_payment_types))
	{
		$alt_payment_types[] = array($alt_payment_info['id'], $alt_payment_info['type'], $alt_payment_info['special']);
		$alt_paid_grand_totals[] = 0;
		if ($alt_payment_info['special'] == "payment_extension:31") $cc_pay_type_ids[] = $alt_payment_info['id'];
	}
	$alt_column = speak("Other");
	$alt_m_title = speak("Other Total");
}

$se_hour = substr(str_pad($location_info['day_start_end_time'], 4, "0", STR_PAD_LEFT), 0, 2);
$se_min = substr(str_pad($location_info['day_start_end_time'], 4, "0", STR_PAD_LEFT), 2, 2);

/*if(isset($_GET['eod_filter']))
{
	if($_GET['eod_filter']=="opened")
		$eod_filter = "opened";
	else
		$eod_filter = "closed";
}
else if(isset($_SESSION['eod_filter']))
	$eod_filter = $_SESSION['eod_filter'];
else
	$eod_filter = "closed";
$_SESSION['eod_filter'] = $eod_filter;*/

$eod_filter = "opened";
if (isset($_GET['eod_filter']))
{
	$cfg_query = rpt_query("select * from `config` where `location`='[1]' and `type`='special' and `setting`='report config'",$locationid);
	if (mysqli_num_rows($cfg_query))
	{
		$cfg_read = mysqli_fetch_assoc($cfg_query);
		lavu_query("update `config` set `value`='[1]' where `id`='[2]'", $_GET['eod_filter'], $cfg_read['id']);
	}
	else
	{
		lavu_query("insert into `config` (`location`,`type`,`setting`,`value`) values ('[1]','special','report config','[2]')", $locationid, $_GET['eod_filter']);
	}
}

$cfg_query = rpt_query("select * from `config` where `location`='[1]' and `type`='special' and `setting`='report config'", $locationid);
if (mysqli_num_rows($cfg_query))
{
	$cfg_read = mysqli_fetch_assoc($cfg_query);
	$eod_filter = $cfg_read['value'];
}

$process_info = array(
	'card_amount'		=> "",
	'card_cvn'			=> "",
	'card_number'		=> "",
	'company_id'		=> admin_info('companyid'),
	'data_name'			=> admin_info('dataname'),
	'device_time'		=> $device_time,
	'device_udid'		=> "",
	'exp_month'			=> "",
	'exp_year'			=> "",
	'ext_data'			=> "",
	'for_deposit'		=> "",
	'loc_id'			=> $location_info['id'],
	'mag_data'			=> "",
	'name_on_card'		=> "",
	'reader'			=> "",
	'register'			=> "Back End",
	'register_name'		=> "Back End",
	'server_id'			=> admin_info('loggedin'),
	'server_name'		=> $server_name,
	'set_pay_type'		=> "Card",
	'set_pay_type_id'	=> "2"
);

$cash_tip_mode = (int)$location_info['cash_tips_as_daily_total'];
$date_setting = "date_format";
$location_date_format_query = rpt_query( "SELECT `location`, `setting`, `value` FROM `config` WHERE `setting` = '[1]'",  $date_setting);
if( !$location_date_format_query || !mysqli_num_rows($location_date_format_query)){
	$date_format = array("value" => 2);
}
else{
	$date_format = mysqli_fetch_assoc( $location_date_format_query );
}

if ($user)
{
	$default_setdate = date("Y-m-d");
	if (date("H") < 7)
	{
		$default_setdate = date("Y-m-d",mktime(0,0,0,date("m"),date("d") - 1,date("Y")));
	}

	$set_use_date = (isset($_REQUEST['date']))?$_REQUEST['date']:$default_setdate;
	$set_udate = explode("-", $set_use_date);
	$use_ts = mktime($se_hour, $se_min, 0, $set_udate[1], $set_udate[2], $set_udate[0]);

	$start_datetime = date("Y-m-d H:i:s", $use_ts);
	$end_datetime = date("Y-m-d H:i:s", ($use_ts + 86399));

	switch ($date_format['value']){
			case 1:$user_date_display = date("j/n/Y", $use_ts);
				$display_start_datetime = date("d-m-Y H:i:s", $use_ts);
				$display_end_datetime = date("d-m-Y H:i:s", ($use_ts + 86399));
			break;

			case 2:$user_date_display = date("n/j/Y", $use_ts);$display_start_datetime = date("m-d-Y H:i:s", $use_ts);
				$display_end_datetime = date("m-d-Y H:i:s", ($use_ts + 86399));
			break;

			case 3:$user_date_display = date("Y/n/j", $use_ts);
				$display_start_datetime = date("Y-m-d H:i:s", $use_ts);
				$display_end_datetime = date("Y-m-d H:i:s", ($use_ts + 86399));

			break;
	}

	if ($user=="all")
	{
		$user_found = TRUE;
		$show_user_name = speak("All Users");
		$user_query_code = "";
	}
	else if (substr($user,0,9)=="register:")
	{
		$user_found = TRUE;
		$find_register = substr($user,9);

		if ($find_register=="receipt" || $find_register=="")
		{
			$show_user_name = "Receipt";
			$user_query_code = "and (`register`='' or `register`='receipt' or `register` = 'Not set') ";
		}
		else
		{
			$show_user_name = ucfirst($find_register);
			$user_query_code = "and `register`='$find_register' ";
		}
	}
	else
	{
		$user_query = rpt_query("SELECT * FROM `users` where `id`=$user");
		$user_found = mysqli_num_rows($user_query);
		if ($user_found)
		{
			$user_read = mysqli_fetch_assoc($user_query);
			$show_user_name = trim($user_read['f_name'] . " " . $user_read['l_name']);
			$user_query_code = "and `server_id`=".$user_read['id']." ";
		}
	}

	if ($user_found)
	{
		$title = "<a style='cursor:pointer; color:#000000' href='index.php?mode={$section}_{$mode}'>".speak("End of Day Report").":</a> ";
		$title .= " - ";
		$title .= $show_user_name;
		$title .= "<br><br>".$user_date_display."<br></b><span style='font-family:Verdana, Arial, Helvetica, sans-serif; font-size:10px; color:#333333;'>($display_start_datetime to $display_end_datetime)</span><b>";

		$display_cash_tip_total = "";
		if ($cash_tip_mode == 1)
		{
			if (is_numeric($user) || $user=="all")
			{
				$todays_cash_tips = get_daily_cash_tip_total($location_info['id'], $user, $set_use_date);
				$display_cash_tip_total = "<br><table width='100%'><tr><td align='center'>Today's Cash Tips: ".display_price($todays_cash_tips, $decimal_places)."</td></tr></table><br>";
			}
		}
		else if ($cash_tip_mode == 0)
		{
			$othertip_list = getvar('othertip_list');
			if ($othertip_list)
			{
				$othertip_list = explode("|",$othertip_list);
				for ($c = 0; $c < count($othertip_list); $c++)
				{
					$othertip_identifier = $othertip_list[$c];
					$set_othertip = getvar('set_othertip_'.$othertip_identifier, "na");
					if ($set_othertip != "na")
					{
						$set_othertip = str_replace("$","",$set_othertip);
						$set_othertip = str_replace(",",".",$set_othertip);
						if (is_numeric($set_othertip))
						{
							$id_parts = explode("_", $othertip_identifier);

							$where_clause = " `order_id` = '[4]' AND `location_id` = '[5]' ";
							$where_identifier = $id_parts[0];
							if (!empty($id_parts[1]))
							{
								$where_clause = " `ioid` = '[4]'";
								$where_identifier = $id_parts[1];
							}

							lavu_query("UPDATE `orders` SET `other_tip` = '[1]', `last_modified` = '[2]', `last_mod_ts` = '[3]', `pushed_ts` = '[3]', `last_mod_device` = 'END OF DAY' WHERE ".$where_clause, str_replace("'","''", number_format($set_othertip, $decimal_places, ".", "")), $device_time, time(), $where_identifier, $locationid);
						}
					}
				}
			}

			$cashtip_list = getvar('cashtip_list');
			if ($cashtip_list)
			{
				$cashtip_list = explode("|",$cashtip_list);
				for ($c = 0; $c < count($cashtip_list); $c++)
				{
					$cashtip_identifier = $cashtip_list[$c];
					$set_cashtip = getvar('set_cashtip_'.$cashtip_identifier, "na");
					if ($set_cashtip != "na")
					{
						$set_cashtip = str_replace("$","",$set_cashtip);
						$set_cashtip = str_replace(",",".",$set_cashtip);
						if (is_numeric($set_cashtip))
						{
							$id_parts = explode("_", $cashtip_identifier);
							$where_clause = " `order_id` = '[4]' AND `location_id` = '[5]' ";
							$where_identifier = $id_parts[0];
							if (!empty($id_parts[1]))
							{
								$where_clause = " `ioid` = '[4]'";
								$where_identifier = $id_parts[1];
							}

							lavu_query("UPDATE `orders` SET `cash_tip` = '[1]', `last_modified` = '[2]', `last_mod_ts` = '[3]', `pushed_ts` = '[3]', `last_mod_device` = 'END OF DAY' WHERE ".$where_clause, str_replace("'","''", number_format($set_cashtip, $decimal_places, ".", "")), $device_time, time(), $where_identifier, $locationid);
						}
						else
						{
							$resp .= "<tr><td>Error: Invalid cash tip value \"".$set_cashtip."\" for Order ID ".$id_parts[0]."</td></tr>";
						}
					}
				}
				shell_exec("php /home/poslavu/private_html/scripts/tipSharing.php $_REQUEST[user] $locationid ".$process_info['data_name']." > /dev/null 2>/dev/null &");
			}
		}

		$cardtip_list = getvar("cardtip_list");
		$cctip_list = getvar("cctip_list");
		$overpaid_ccs = explode("|", getvar("overpaid_cc_orders"));
		$overpaid_CCs = array();
		foreach ($overpaid_ccs as $opc)
		{
			$opc_parts = explode(":", $opc);
			$overpaid_CCs[$opc_parts[0]] = (isset($opc_parts[1]))?$opc_parts[1]:"";
		}

		$submit_title = speak("Submit Tip Changes / Capture Authorizations");
		if ($location_info['integrateCC'] == "1")
		{
			$integration_info = get_integration_from_location($locationid, "poslavu_".admin_info('dataname')."_db");
			$func_path = __DIR__."/../../../lib/gateway_lib/".strtolower($integration_info['gateway'])."_func.php";
			if (file_exists($func_path))
			{
				require_once($func_path);
			}

			if (in_array($integration_info['gateway'], array("Authorize.net", "BluePay", "CyberSource", "Magensa", "MIT", "Moneris")))
			{
				$submit_title = speak("Submit Tip Changes");
			}
		}
		else
		{
			$submit_title = speak("Submit Tip Changes");
		}

		if ($cardtip_list)
		{
			$error_encountered = FALSE;
			$cardtip_list = explode("|", $cardtip_list);

			for ($c = 0; $c < count($cardtip_list); $c++)
			{
				$cardtip_identifier = $cardtip_list[$c];
				$a_tip_has_changed = FALSE;

				$set_cardtip = getvar('set_cardtip_'.$cardtip_identifier, "na");
				if ($set_cardtip != "na")
				{
					$set_cardtip = str_replace("$","",$set_cardtip);
					$set_cardtip = str_replace(",",".",$set_cardtip);
					if (is_numeric($set_cardtip) || ($set_cardtip == "") || ($set_cardtip == "multi"))
					{
						$error_code = 0;
						$error_info = 0;

						$check_got_response = ($location_info['integrateCC'] == "1")?" AND `got_response` = '1'":"";

						//echo "-----------------------".$location_info['gift_revenue_tracked'] . "<bR>";

						$id_parts = explode("_", $cardtip_identifier);
						$where_clause = " `order_id` = '[1]' AND `loc_id` = '[2]'";
						$where_identifier = $id_parts[0];
						if (!empty($id_parts[1]))
						{
							$where_clause = " `ioid` = '[1]'";
							$where_identifier = $id_parts[1];
						}

						$get_transactions = rpt_query("SELECT * FROM `cc_transactions` WHERE ".$where_clause."$check_got_response AND `pay_type_id` = '2' AND (`action` = 'Sale' OR `action` = 'Refund') AND `voided` != '1' ORDER BY `check`, `id` ASC", $where_identifier, $locationid);
						$transaction_count = mysqli_num_rows($get_transactions);
						if ($transaction_count == 1)
						{
							$found_unprocessed_transactions = TRUE;
							$transaction_info = mysqli_fetch_assoc($get_transactions);
							$apply_tip =  number_format((float)$set_cardtip, $decimal_places, ".", "");

							if (((float)$apply_tip != (float)$transaction_info['tip_amount']) || ($transaction_info['auth'] == 1) || isset($overpaid_CCs[$cardtip_identifier]))
							{
								$a_tip_has_changed = TRUE;
								if ($location_info['integrateCC'] == "1")
								{
									if ($transaction_info['gateway'] == "PayPal" && $transaction_info['transtype'] == "Auth")
									{
										$process_info['order_id'] = $transaction_info['order_id'];
										$get_main_tips = mlavu_query("SELECT `id`, `status` FROM `cc_tip_transactions` WHERE `dataname` = '[1]' AND `order_id` = '[2]' AND `transaction_id`='[3]'", $process_info['data_name'], $transaction_info['order_id'],$transaction_info['transaction_id']);
										$tip_rows = mysqli_num_rows($get_main_tips);

										$resp = "<tr><td align='center'>All card tips have been successfully submitted. Depending on the volume of tips submitted and the processing platform, it may take a few minutes for the transaction adjustments to be completed.<br>Please check the 'Batch Tips Processing' page to view the status of submitted tips</td></tr>";

										$vars = array();
										$vars['dataname'] = $process_info['data_name'];
										$vars['order_id'] = $transaction_info['order_id'];
										$vars['transaction_id'] = $transaction_info['transaction_id'];
										$idatetime = date("Y-m-d H:i:s");
										$process_info['authcode']=$transaction_info['auth_code'];
										$locationInfoArray = $location_info;
								        if (isset($locationInfoArray['ltg_pizza_info']) ) {
										    unset($locationInfoArray['ltg_pizza_info']);
										}
										$transaction_info_array = array("location_info" => $locationInfoArray, "integration_info" => $integration_info, "process_info" => $process_info, "transaction_info" => $transaction_info, "tip_amount" => $apply_tip,"last_tip"=>$transaction_info['tip_amount']);
										$vars['transaction_info'] = json_encode($transaction_info_array);
										$vars['status'] = 3; // default in pending -> 3 status
										$vars['created_date'] = $idatetime;
										$vars['updated_date'] = $idatetime;

										$inProcessQueue = FALSE;
										if ($tip_rows < 1)
										{
											mlavu_query("INSERT INTO `cc_tip_transactions` (`dataname`, `order_id`,`transaction_id`, `transaction_info`, `status`, `created_date`, `updated_date`) VALUES ('[dataname]', '[order_id]','[transaction_id]' , '[transaction_info]', '[status]', '[created_date]', '[updated_date]')", $vars);
											$inProcessQueue = TRUE;
										}
										else if ($tip_rows == 1)
										{
											$main_tips_data = mysqli_fetch_assoc($get_main_tips);
											if ($main_tips_data['status'] == "pending")
											{
												mlavu_query("UPDATE `cc_tip_transactions` SET `transaction_info` = '[1]', `updated_date` = '[2]' where transaction_id='[3]' ", $vars['transaction_info'], $vars['updated_date'], $transaction_info['transaction_id']);
												$inProcessQueue = TRUE;
											}
										}

										if ($inProcessQueue)
										{
											updateCCRecord($transaction_info['transaction_id'],$apply_tip);
										}
									}
									else
									{
										$set_cardtip = process_capture_or_tip($location_info, $integration_info, $process_info, $transaction_info, $apply_tip, $overpaid_CCs, TRUE);
									}
								}
								else
								{
									lavu_query("UPDATE `cc_transactions` SET `tip_amount` = '[1]', `last_mod_ts` = '[2]' WHERE `id` = '[3]'", $apply_tip, time(), $transaction_info['id']);
								}
							}
							else
							{
								$set_cardtip = $transaction_info['tip_amount'];
								if ($location_info['integrateCC'] == "1")
								{
									$resp .= "<tr><td align='center'>Order #".$transaction_info['order_id']." - ".$transaction_info['card_type']." ...".$transaction_info['card_desc']." - Tip: $apply_tip - Tip unchanged</td></tr>";
								}
							}
						}
						else if ($transaction_count > 1)
						{
							$found_unprocessed_transactions = TRUE;
							$tip_total = 0;
							$cctip_list_array = explode("|", $cctip_list);
							while ($transaction_info = mysqli_fetch_assoc($get_transactions))
							{
								if ($transaction_info['action'] == "Sale")
								{
									for ($i = 0; $i < count($cctip_list_array); $i++)
									{
										$cct_id = $cctip_list_array[$i];

										if ($transaction_info['id'] == $cct_id)
										{
											$apply_tip = getvar('set_cctip_'.$cct_id, "na");
											if ($apply_tip != "na")
											{
												$apply_tip = number_format((float)str_replace("$","",$apply_tip), $decimal_places, ".", "");

												if (((float)$apply_tip != (float)$transaction_info['tip_amount']) || ($transaction_info['auth'] == 1))
												{
													$a_tip_has_changed = TRUE;
													if ($location_info['integrateCC'] == "1")
													{
														if ($transaction_info['gateway'] == "PayPal" && $transaction_info['transtype'] == "Auth")
														{
															$process_info['order_id'] = $transaction_info['order_id'];
															$get_main_tips = mlavu_query("SELECT `id`, `status` FROM `cc_tip_transactions` WHERE `dataname` = '[1]' AND `order_id` = '[2]' AND `transaction_id`='[3]'", $process_info['data_name'], $process_info['order_id'], $transaction_info['transaction_id']);
															$tip_rows = mysqli_num_rows($get_main_tips);

															$resp = "<tr><td align='center'>All card tips have been successfully submitted. Depending on the volume of tips submitted and the processing platform, it may take a few minutes for the transaction adjustments to be completed.<br>Please check the 'Batch Tips Processing' page to view the status of submitted tips.</td></tr>";

															$vars = array();
															$vars['dataname'] = $process_info['data_name'];
															$vars['order_id'] = $process_info['order_id'];
															$vars['transaction_id'] = $transaction_info['transaction_id'];
															$idatetime = date("Y-m-d H:i:s");
															$process_info['authcode']=$transaction_info['auth_code'];
															$locationInfoArray = $location_info;
															if (isset($locationInfoArray['ltg_pizza_info']) ) {
															    unset($locationInfoArray['ltg_pizza_info']);
															}
															$transaction_info_array = array("location_info" => $locationInfoArray, "integration_info" => $integration_info, "process_info" => $process_info, "transaction_info" => $transaction_info, "tip_amount" => $apply_tip,"last_tip" => $transaction_info['tip_amount']);
															$vars['transaction_info'] = json_encode($transaction_info_array);
															$vars['status'] = 3; // default in pending -> 3 status
															$vars['created_date'] = $idatetime;
															$vars['updated_date'] = $idatetime;

															$inProcessQueue = FALSE;
															if ($tip_rows < 1)
															{
																mlavu_query("INSERT INTO `cc_tip_transactions` (`dataname`, `order_id`,`transaction_id`, `transaction_info`, `status`, `created_date`, `updated_date`) VALUES ('[dataname]', '[order_id]', '[transaction_id]', '[transaction_info]', '[status]', '[created_date]', '[updated_date]')", $vars);
																$inProcessQueue=TRUE;
															}
															else if ($tip_rows == 1)
															{
																$main_tips_data = mysqli_fetch_assoc($get_main_tips);
																if ($main_tips_data['status'] == "pending")
																{
																	mlavu_query("UPDATE `cc_tip_transactions` SET `transaction_info` = '[1]', `updated_date` = '[2]' where `transaction_id`='[3]' ", $vars['transaction_info'], $vars['updated_date'], $transaction_info['transaction_id'] );
																	$inProcessQueue=TRUE;
																}
															}

															if ($inProcessQueue)
															{
																updateCCRecord($transaction_info['transaction_id'],$apply_tip);
															}
														}
														else
														{
															$tip_total += process_capture_or_tip($location_info, $integration_info, $process_info, $transaction_info, $apply_tip, "", FALSE);
														}
													}
													else
													{
														$tip_total += $apply_tip;
														lavu_query("UPDATE `cc_transactions` SET `tip_amount` = '[1]', `last_mod_ts` = '[2]' WHERE `id` = '[3]'", $apply_tip, time(), $transaction_info['id']);
													}
												}
												else
												{
													$tip_total += $transaction_info['tip_amount'];
													if ($location_info['integrateCC'] == "1")
													{
														$resp .= "<tr><td align='center'>Order #".$transaction_info['order_id']." - ".$transaction_info['card_type']." ...".$transaction_info['card_desc']." - Tip: $apply_tip - Tip unchanged</td></tr>";
													}
												}
											}
										}
									}
								}
							}

							$set_cardtip = $tip_total;
						}

						if ($a_tip_has_changed)
						{
							$where_clause = " `order_id` = '[4]' AND `location_id` = '[5]'";
							$where_identifier = $id_parts[0];
							if (!empty($id_parts[1]))
							{
								$where_clause = " `ioid` = '[4]'";
								$where_identifier = $id_parts[1];
							}

							$local_time = localize_datetime(date("Y-m-d H:i:s"), $location_info['timezone']);
							lavu_query("UPDATE `orders` SET `card_gratuity` = '[1]', `last_modified` = '[2]', `last_mod_ts` = '[3]', `pushed_ts` = '[3]', `last_mod_device` = 'END OF DAY' WHERE ".$where_clause, str_replace("'","''", number_format($set_cardtip, $decimal_places, ".", "")), $local_time, time(), $where_identifier, $locationid);
						}

						switch ($error_code)
						{
							case 1:

								$resp .= "<tr><td align='center'>Error: Failed to load order ID $error_info...</td></tr>";
								$error_encountered = TRUE;
								break;

							case 2:

								$resp .= "<tr><td align='center'>Error: Failed to load transactions for order ID $error_info...</td></tr>";
								$error_encountered = TRUE;
								break;

							case 3:

								$resp .= "<tr><td align='center'>Error: Unrecognized payment gateway...</td></tr>";
								$error_encountered = TRUE;
								break;

							case 4:

								$resp .= "<tr><td align='center'>Gateway error: $error_info...</td></tr>";
								$error_encountered = TRUE;
								break;

							default:
								break;
						}
					}
					else
					{
						$resp .= "<tr><td align='center'>Error: Invalid card tip value \"".$set_cardtip."\" for Order ID ".$id_parts[0]."</td></tr>";
						$error_encountered = TRUE;
					}
				}
				else
				{
					$resp .= "<tr><td>Error: Invalid card tip value \"".$set_cardtip."\" for Order ID ".$id_parts[0]."</td></tr>";
					$error_encountered = TRUE;
				}
			}

			if (!$found_unprocessed_transactions && ($location_info['integrateCC'] == "1"))
			{
				$resp .= "<tr><td align='center'>All the transactions listed above have been marked as settled so no tip adjustments have been sent to the gateway.</td></tr>
					<tr><td align='center'>Tip adjustments may possibly still be performed from the gateway's virtual terminal.</td></tr>
					<tr><td align='center'>&nbsp;<script language='javascript'>alert('All the transactions listed above have been marked as settled so no tip adjustments have been sent to the gateway. Tip adjustments may possibly still be performed from the gateway's virtual terminal.');</script></td></tr>";
			}
			else if (!$error_encountered)
			{
				$resp .= "<tr><td align='center'>All card tips successfully updated with no errors.<script language='javascript'>alert('All card tips successfully updated with no errors.');</script></td></tr>";
			}
			else if ($error_encountered)
			{
				$resp .= "<tr><td align='center'>One or more errors occurred while trying to update tip amounts. Please take note.<script language='javascript'>alert('One or more errors occurred while trying to update tip amounts. Please view details at the bottom of this page.');</script></td></tr>";
			}

			if ($return_json) {
				http_response_code($error_encountered ? 400 : 200);
				echo json_encode([
					'message' => $resp,
				]);

				exit();
			}

			shell_exec("php /home/poslavu/private_html/scripts/tipSharing.php $_REQUEST[user] $locationid ".$process_info['data_name']." > /dev/null 2>/dev/null &");
		}

		if (isset($_REQUEST['orderby']))
		{
			$o_orderby = "`".str_replace("`","",$_REQUEST['orderby'])."` ".$_GET['ordermode']."";
			if( $_GET['ordermode']=='asc') {
				$_GET['ordermode']='desc';
			}else {
					$_GET['ordermode']= 'asc';
			}
		}
		else
		{
			$o_orderby = "`server` ASC, `opened` ASC";
			$_GET['ordermode']= 'asc';
		}
		$order_query = lavu_query("SELECT * FROM `orders` WHERE `order_id` NOT LIKE 'Paid%' AND `order_id` NOT LIKE '777%' AND `$eod_filter` >= '$start_datetime' and `$eod_filter` <= '$end_datetime' and `void`='0' and `location_id`='".$location_info['id']."' ".$user_query_code."ORDER BY $o_orderby");

		$mid .= "<tr><td align='left'>";
		$mid .= "<style>";
		$mid .= "  .ttop { border-bottom:solid 2px #777777; text-align:center; font-weight:bold; }";
		$mid .= "  .tbot { border-top:solid 2px #777777; text-align:right; font-weight:bold; }";
		$mid .= "</style>";

		// needed for eConduit iFrame tip adjust
		$mid .= "<script src='../../../cp/scripts/ajax_prototype.js'></script>";
		$mid .= "<script src='../../../components/payment_extensions/eConduit/eConduitTipAdjust.js'></script>";
		$mid .= "<script>var isEmbeddedWebView = false; function refreshPage() { window.location.reload(); }</script>";

		$crl = "index.php?mode={$section}_{$mode}&date=".$_REQUEST['date']."&user=$user";
		if (isset($_REQUEST['date2']))
		{
			$crl .= "&date2=" . $_REQUEST['date2'];
		}

		$mid .= $display_cash_tip_total;
		$mid .= "<br><form id='eod_form' name='eod' method='post' action=''>";
		$mid .= "<table cellspacing=0 cellpadding=2>";
		$mid .= "<tr>";
		$mid .= "<td class='ttop' onclick='window.location = \"$crl&orderby=order_id&ordermode=$_GET[ordermode]\"' style='cursor:pointer'>".speak("ID")."</td><td class='ttop'>&nbsp;</td>";
		$mid .= "<td class='ttop' onclick='window.location = \"$crl&orderby=server&ordermode=$_GET[ordermode]\"' style='cursor:pointer'>".speak("Server")."</td><td class='ttop'>&nbsp;</td>";
		$mid .= "<td class='ttop' onclick='window.location = \"$crl&orderby=opened&ordermode=$_GET[ordermode]\"' style='cursor:pointer'>".speak("Time")."</td><td class='ttop'>&nbsp;</td>";
		$mid .= "<td class='ttop' onclick='window.location = \"$crl&orderby=tablename&ordermode=$_GET[ordermode]\"' style='cursor:pointer'>".speak("Table")."</td><td class='ttop'>&nbsp;</td>";
		$mid .= "<td class='ttop'>".speak("Subtotal")."</td><td class='ttop'>&nbsp;</td>";
		$mid .= "<td class='ttop'>".speak("Check Discount")."</td><td class='ttop'>&nbsp;</td>";
		$mid .= "<td class='ttop'>".speak("Tax")."</td><td class='ttop'>&nbsp;</td>";
		$mid .= "<td class='ttop'>".speak("Total")."</td><td class='ttop'>&nbsp;</td>";
		$mid .= "<td class='ttop'>".speak("Due")."</td><td class='ttop'>&nbsp;</td>";
		$mid .= "<td class='ttop'>".speak("Cash Paid")."</td><td class='ttop'>&nbsp;</td>";
		if ($cash_tip_mode == 0)
		{
			$mid .= "<td class='ttop'>".speak("Cash Tip")."</td><td class='ttop'>&nbsp;</td>";
		}
		$mid .= "<td class='ttop'>".speak("Card Paid")."</td><td class='ttop'>&nbsp;</td>";
		$mid .= "<td class='ttop'>".speak("Card Tip")."</td><td class='ttop'>&nbsp;</td>";
		$mid .= "<td class='ttop'>".speak("Card Total")."</td><td class='ttop'>&nbsp;</td>";
		$mid .= "<td class='ttop'>".$alt_column."</td><td class='ttop'>&nbsp;</td>";
		$mid .= "<td class='ttop'>".trim($alt_column . " " . speak("Tip"))."</td><td class='ttop'>&nbsp;</td>";
		$mid .= "<td class='ttop'>".speak("Auto Gratuity")."</td><td class='ttop'>&nbsp;</td>";
		$mid .= "<td class='ttop'>".speak("AG - Cash")."</td><td class='ttop'>&nbsp;</td>";
		$mid .= "<td class='ttop'>".speak("AG - Card")."</td><td class='ttop'>&nbsp;</td>";
		$mid .= "<td class='ttop'>".speak("Total Tips")."</td><td class='ttop'>&nbsp;</td>";
		$mid .= "</tr>";

		$t_subtotal			= 0;
		$t_discount			= 0;
		$t_tax				= 0;
		$t_itax				= 0;
		$t_total			= 0;
		$t_cash				= 0;
		$t_card				= 0;
		$t_due				= 0;
		$t_card_tip			= 0;
		$t_card_total		= 0;
		$t_gift_certificate	= 0;
		$other_tip_total	= 0;

		$t_auto_grat	= 0;
		$t_ag_cash		= 0;
		$t_cash_tip		= 0;
		$t_ag_card		= 0;
		$t_total_tips	= 0;

		if (mysqli_num_rows($order_query) < 1)
		{
			$mid .= "<td colspan='16' align='center'>&nbsp;<br>No Orders Found<br>&nbsp;</td>";
		}

		$overpaid_cc_orders = array();

		$cashtipjs_list		= "";
		$othertipjs_list	= "";
		$midjs_list			= "";
		$midjs				= "";
		$cctjs_list			= "";
		$hidden_fields		= "";
		$row				= 0;

		while ($order_read = mysqli_fetch_assoc($order_query))
		{
			$row++;
			$show_multiple_ccts = FALSE;
			$show_single_cct = FALSE;
			$method_paid = "";
			$order_identifier = $order_read['order_id']."_".$order_read['ioid'];
			if ($order_read['cash_paid'] > 0)
			{
				$method_paid .= "Cash";
			}

			if ($order_read['card_paid'] > 0)
			{
				if ($method_paid!="")
				{
					$method_paid .= ", ";
				}
				$method_paid .= "Card";
			}

			if (isset($order_read['cash_applied']) && (string)$order_read['cash_applied'] != "")
			{
				$cash_applied = ($order_read['cash_applied'] * 1);
			}
			else if (isset($order_read['cash_paid']))
			{
				$cash_applied = ($order_read['cash_paid'] * 1);
			}

			$alt_paid_total = 0;
			$alt_paid_amounts = array();
			$alt_paid_amounts[] = array(speak("Gift Certificate"), display_price($order_read['gift_certificate'], $decimal_places));
			$alt_paid_grand_totals[0] += $order_read['gift_certificate'];
			for ($ap = 1; $ap < count($alt_payment_types); $ap++)
			{
				$ptype = $alt_payment_types[$ap];
				$this_total = get_alt_total($locationid, $order_read['order_id'], $ptype[0]);
				if (isset($_GET['test87']))
				{
					echo "N:" . $this_total . "<br>";
				}

				if ((float)$this_total != 0)
				{
					$alt_paid_total += $this_total;
					$alt_paid_amounts[] = array($ptype[1], display_price($this_total, $decimal_places));
					$alt_paid_grand_totals[$ap] += $this_total;
				}
			}

			if (isset($order_read['other_tip']))
			{
				$set_other_tip = $order_read['other_tip'];//"0.00";
			}

			$set_cash_tip = 0;
			if ($cash_tip_mode == 0)
			{
				$set_cash_tip = number_format((float)$order_read['cash_tip'], $decimal_places, $decimal_char, $thousands_char);
				if ($set_cash_tip == "")
				{
					$set_cash_tip = 0;
				}
			}
			else
			{
				$display_other_tip_code = "&nbsp;";
			}

			$set_card_tip = number_format((float)$order_read['card_gratuity'], $decimal_places, $decimal_char, $thousands_char);
			if ($set_card_tip == "")
			{
				$set_card_tip = 0;
			}

			$t_subtotal += $order_read['subtotal'];
			$t_discount += $order_read['discount'];
			$t_itax += $order_read['itax'];
			$t_tax += $order_read['tax'];
			$t_total += ($order_read['total'] - $order_read['gratuity']);
			$t_cash += $cash_applied;
			$t_cash_tip += $order_read['cash_tip'];
			$t_card += $order_read['card_paid'];
			$t_card_tip += $order_read['card_gratuity'];
			$t_card_total += $order_read['card_paid'] + $order_read['card_gratuity'];

			$total_tips = $order_read['gratuity'] * 1 + $order_read['cash_tip'] * 1 + $order_read['card_gratuity'] * 1;;
			$t_auto_grat += $order_read['gratuity'];
			$t_total_tips += $total_tips;

			$t_gift_certificate += ($order_read['gift_certificate'] + $alt_paid_total);

			$display_gift_certificate = display_price(($order_read['gift_certificate'] + $alt_paid_total), $decimal_places);
			$display_cash_paid = display_price(($cash_applied), $decimal_places);

			$alt_style = "";
			$alt_code = "";
			$layer_code = "";
			if (($order_read['gift_certificate'] + $alt_paid_total) > 0)
			{
				$gift_color = "#007841";
				if (count($alt_payment_types) > 0)
				{
					$alt_style = " cursor:pointer;";
					$alt_code = " onmouseover='showAltPayments(\"alt_paid_".$order_read['order_id']."\", this);'";
					$alt_code .= " onmouseout='hideAltPayments(\"alt_paid_".$order_read['order_id']."\");'";
					$layer_code .= "<div id='alt_paid_".$order_read['order_id']."' style='position:absolute; left:0px; top:0px; z-index:".($row + 500)."; display:none;'>";
					$layer_code .= "<table cellspacing='0' cellpadding='1'><tr><td align='center' style='background:URL(images/t_gray1.png); background-repeat:repeat;'>";
					$layer_code .= "<table cellspacing='0' cellpadding='1'><tr><td align='center' style='background:URL(images/t_gray1.png); background-repeat:repeat;'>";
					$layer_code .= "<table cellspacing='0' cellpadding='1'><tr><td align='center' style='background:URL(images/t_gray1.png); background-repeat:repeat;'>";
					$layer_code .= "<table cellspacing='0' cellpadding='4' bgcolor='#FFFFFF'>";
					foreach ($alt_paid_amounts as $apa)
					{
						$layer_code .= "<tr><td align='right'>".$apa[0].":</td><td align='right'>".$apa[1]."</td></tr>";
					}
                    $layer_code .= "</table>";
                    $layer_code .= "</td></tr></table>";
                    $layer_code .= "</td></tr></table>";
                    $layer_code .= "</td></tr></table>";
                    $layer_code .= "</div>";
				}
			}
			else
			{
				$gift_color = "#bbbbbb";
			}

			$display_gift_certificate = "<font color='$gift_color'>$display_gift_certificate</font>".$layer_code;

			$cash_color = ($order_read['cash_paid'] > 0)?"#00aa00":"#bbbbbb";
			$display_cash_paid = "<font color='$cash_color'>$display_cash_paid</font>";

			$row_color = (($row % 2) == 0)?"#EDEDED":"#FFFFFF";

			$mid .= "<tr bgcolor='".$row_color."'>";
			$mid .= "<td class='location_link_lg' style='text-align:center'>" . $order_read['order_id'] . "</td><td>&nbsp;</td>";
			$mid .= "<td class='location_link_lg' style='text-align:left'>" . $order_read['server'] . "</td><td>&nbsp;</td>";
			$mid .= "<td class='location_link_lg' style='text-align:right'>" . display_datetime($order_read['opened']) . "</td><td>&nbsp;</td>";
			$mid .= "<td class='location_link_lg' style='text-align:left'>" . $order_read['tablename'] . "</td><td>&nbsp;</td>";

			$mid .= "<td class='location_link_lg' style='text-align:right'><font color='#000055'>" . display_price(($order_read['subtotal'] - $order_read['itax']), $decimal_places) . "</font></td><td>&nbsp;</td>";
			$mid .= "<td class='location_link_lg' style='text-align:right'><font color='#007841'>" . display_price($order_read['discount'], $decimal_places) . "</font></td><td>&nbsp;</td>";
			$mid .= "<td class='location_link_lg' style='text-align:right'><font color='#005500'>" . display_price(($order_read['tax'] + $order_read['itax']), $decimal_places) . "</font></td><td>&nbsp;</td>";
			$mid .= "<td class='location_link_lg' style='text-align:right'><font color='#0000aa'>" . display_price(($order_read['total'] - $order_read['gratuity']), $decimal_places) . "</font></td><td>&nbsp;</td>";

			$row_after_due = "<td>&nbsp;</td>";
			$row_after_due .= "<td class='location_link_lg' style='text-align:right'>" . $display_cash_paid . "</td><td>&nbsp;</td>";
			if ($cash_tip_mode == 0)
			{
				$row_after_due .= "<td class='location_link_lg' style='text-align:right' colspan='2'>";
				if ($lls)
				{
					$row_after_due .= display_price($set_cash_tip, $decimal_places);
				}
				else
				{
					$row_after_due .= "<input type='text' size='6' style='text-align:right' id='input_cash_tip_$order_identifier' name='set_cashtip_$order_identifier' value='$set_cash_tip'>";
				}
				$row_after_due .= "</td>";

				if ($cashtipjs_list != "")
				{
					$cashtipjs_list .= "|";
				}
				$cashtipjs_list .= "$order_identifier";
			}

			if (isset($order_read['other_tip']))
			{
				$set_other_tip = number_format((float)$order_read['other_tip'], $decimal_places, $decimal_char, $thousands_char);//"0.00";
			}
			else
			{
				$set_other_tip = "0.00";
			}

			if ($set_other_tip * 1 != 0)
			{
				$other_tip_total = $other_tip_total * 1 + $order_read['other_tip'] * 1;
			}
			if ($lls)
			{
				$display_other_tip_code = display_price($order_read['other_tip']);
			}
			else if(isset($order_read['other_tip']))
			{
				if ($order_read['other_tip']=="" && $order_read['alt_paid'] * 1 > 0)
				{
					$trans_query = lavu_query("select sum(`tip_amount`) as `other_tips` from `cc_transactions` where `order_id`='[1]' and `pay_type_id` * 1 > 2 and `voided`='0'",$order_read['order_id']);
					if (mysqli_num_rows($trans_query))
					{
						$trans_read = mysqli_fetch_assoc($trans_query);
						$sum_other_tips = $trans_read['other_tips'] * 1;
						if ($sum_other_tips > 0)
						{
							$set_other_tip = $sum_other_tips;
						}
					}
				}

				$display_other_tip_code = "<input type='text' size='6' style='text-align:right' id='input_other_tip_$order_identifier' name='set_othertip_$order_identifier' value='$set_other_tip'>";
				if ($othertipjs_list != "")
				{
					$othertipjs_list .= "|";
				}
				$othertipjs_list .= "$order_identifier";
			}
			else
			{
				$display_other_tip_code = "&nbsp;";
			}

			if (isset($set_other_tip) && is_numeric($set_other_tip))
			{
				$total_tips = $total_tips * 1 + $set_other_tip * 1;
			}

			//echo "HERE: $correct_cc_amount<br>";
			$correct_cc_amount = $order_read['card_paid'];
			//echo "HERE 1: $correct_cc_amount<br>";
			//echo "correct_cc_amount: $correct_cc_amount : order_read: ".$order_read['card_paid']."<br>";
			$correct_tip_amount = $order_read['card_gratuity'];
			$check_got_response = ($location_info['integrateCC'] == "1")?" AND `got_response` = '1'":"";

			$where_clause = " `order_id` = '[1]' AND `loc_id` = '[2]'";
			$where_identifier = $order_read['order_id'];
			if (!empty($order_read['ioid']))
			{
				$where_clause = 	" `ioid` = '[1]'";
				$where_identifier = $order_read['ioid'];
			}

			$check_cct = lavu_query("SELECT * FROM `cc_transactions` WHERE ".$where_clause." AND `voided` != '1'$check_got_response AND `pay_type_id` IN ('".implode("','", $cc_pay_type_ids)."') AND (`action` = 'Sale' OR `action` = 'Refund') AND `tip_for_id` = '0' ORDER BY `check`, `id` ASC", $where_identifier, $locationid);
			// create an array of all records
			while ($check = mysqli_fetch_assoc($check_cct))
			{
				$all_check_ccts[] = $check;
			}
			// reset the pointer back so we can still access this
			mysqli_data_seek($check_cct, 0);

			if (@mysqli_num_rows($check_cct) > 1)
			{
				$show_multiple_ccts = TRUE;
				if (((float)$order_read['card_gratuity'] == 0) || ((float)$amount_due != 0 ))
				{
					$check_cc_total = lavu_query("SELECT SUM(`cc_transactions`.`amount`) AS `amount`, SUM(`tip_amount`) AS `tip_amount` FROM `cc_transactions` WHERE `order_id` = '[1]' AND `loc_id` = '[2]' AND `voided` != '1'$check_got_response AND `pay_type_id` = '2' AND `action` = 'Sale' AND `tip_for_id` = '0' ORDER BY `check`, `id` ASC", $order_read['order_id'], $order_read['location_id']);
					$cc_total_info = mysqli_fetch_assoc($check_cc_total);

					$check_cc_refund = lavu_query("SELECT SUM(`cc_transactions`.`amount`) AS `amount`, SUM(`tip_amount`) AS `tip_amount` FROM `cc_transactions` WHERE `order_id` = '[1]' AND `loc_id` = '[2]' AND `voided` != '1'$check_got_response AND `pay_type_id` = '2' AND `action` = 'Refund' AND `tip_for_id` = '0' ORDER BY `check`, `id` ASC", $order_read['order_id'], $order_read['location_id']);
					$cc_refund_info = mysqli_fetch_assoc($check_cc_refund);

					if (((float)$cc_total_info['amount'] > 0) || ((float)$cc_refund_info['amount'] > 0))
					{
						$correct_cc_amount = ($cc_total_info['amount'] - $cc_refund_info['amount']);
						//echo "HERE 2: $correct_cc_amount<br>";
					}
					// Start LP-1171 head
					/*
					$cashtip_list = getvar('cashtip_list');
					if ($cashtip_list) {
						$cashtip_list = explode("|",$cashtip_list);
						for ($c = 0; $c < count($cashtip_list); $c++) {
							$cashtip_identifier = $cashtip_list[$c];
							$set_cashtip = getvar('set_cashtip_'.$cashtip_identifier, "na");
							if ($set_cashtip != "na") {
								$set_cashtip = str_replace("$","",$set_cashtip);
								if (is_numeric($set_cashtip)) {
									$id_parts = explode("_", $cashtip_identifier);
									$where_clause = " `order_id` = '[4]' AND `location_id` = '[5]' ";
									$where_identifier = $id_parts[0];
									$order_id = $id_parts[0];
									if (!empty($id_parts[1])) {
										$where_clause = " `ioid` = '[4]'";
										$where_identifier = $id_parts[1];
									}
									$cash_tips = str_replace("'","''", number_format($set_cashtip, $decimal_places, ".", ""));
									lavu_query("UPDATE `orders` SET `cash_tip` = '[1]', `last_modified` = '[2]', `last_mod_ts` = '[3]', `pushed_ts` = '[3]', `last_mod_device` = 'END OF DAY' WHERE ".$where_clause, $cash_tips, $device_time, time(), $where_identifier, $locationid);
									$ccTransExistsResult = lavu_query("SELECT * FROM `cc_transactions` WHERE `ioid` = '[1]' AND `pay_type` = 'Cash' AND `action` = 'Sale' AND `check` = 1 ", $id_parts[1]);
									if(mysqli_num_rows($ccTransExistsResult)) {
										lavu_query("UPDATE `cc_transactions` SET `tip_amount` = '[1]', `last_mod_ts` = '[2]' WHERE `ioid` = '[3]' AND `pay_type` = 'Cash' AND `action` = 'Sale' AND `check` = 1 ", $cash_tips, time(), $id_parts[1]);
									} else {
										$server_time = date('Y-m-d H:i:s');
										$insertCCTransQuery = "INSERT INTO `cc_transactions` (`order_id`,`check`,`tip_amount`, `loc_id`,`datetime`,`pay_type`,`register`,`action`,`server_name`,`server_id`,`register_name`,`pay_type_id`,`server_time`,`ioid`,`last_mod_ts`) VALUES ('".$order_id."','1','".$cash_tips."','".$locationid."','".$device_time."','Cash','".$process_info['register']."','Sale','".$server_name."','".$process_info['server_id']."','".$process_info['register_name']."','1','".$server_time."','".$id_parts[1]."','".time()."')";
										$insertCashTipCCTransRowResult = lavu_query($insertCCTransQuery);
									}

								} else $resp .= "<tr><td>Error: Invalid cash tip value \"".$set_cashtip."\" for Order ID ".$id_parts[0]."</td></tr>";
							}
						}*/
						//End LP-1171 head

					if (((float)$cc_total_info['tip_amount'] > 0) || ((float)$cc_refund_info['tip_amount'] > 0))
					{
						$correct_tip_amount = ($cc_total_info['tip_amount'] - $cc_refund_info['tip_amount']);
						$set_card_tip = number_format($correct_tip_amount, $decimal_places, $decimal_char, $thousands_char);
					}
				}

				$card_gratuity_string = "<td class='info_text' align='right'>".display_price($set_card_tip, $decimal_places)."</td><td>&nbsp;</td>";
				$hidden_fields .= "<input type='hidden' name='set_cardtip_$order_identifier' value='multi'>";
				if ($midjs_list != "")
				{
					$midjs_list .= "|";
				}
				$midjs_list .= "$order_identifier";
			}
			else if ((@mysqli_num_rows($check_cct) == 1) || ($order_read['card_paid'] > 0))
			{
				$cct_info	= mysqli_fetch_assoc($check_cct);
				$gateway	= $cct_info['gateway'];
				$auth		= $cct_info['auth'];

				$show_single_cct = TRUE;
				if ($cct_info['pay_type_id'] == "2")
				{
					$correct_cc_amount = $cct_info['amount'];
					$correct_tip_amount = $cct_info['tip_amount'];
					if (((float)$order_read['card_gratuity'] > 0) && ($correct_tip_amount == 0))
					{
						$correct_tip_amount = $order_read['card_gratuity'];
						if ($gateway != "PayPal")
						{
							$update_record = lavu_query("UPDATE `cc_transactions` SET `tip_amount` = '[1]', `last_mod_ts` = '[2]' WHERE `id` = '[3]'", $correct_tip_amount, time(), $cct_info['id']);
						}
					}

					$set_card_tip = number_format((float)$correct_tip_amount, $decimal_places, $decimal_char, $thousands_char);
					$card_gratuity_string = "<td class='info_text' align='right' colspan='2'>";
					if ($cct_info['processed']=="1" || $auth=="2" || in_array($gateway, array("iZettle", "MIT", "Moneris")) || $lls)
					{
						$card_gratuity_string .= display_price($set_card_tip, $decimal_places);
					}
					else
					{
						// check for a matching refund for this transaction
						$foundMatchingRefund = findSaleWithRefund($all_check_ccts, $cct_info);
						if ($foundMatchingRefund)
						{
							// if the sale was refunded, show uneditable label
							$card_gratuity_string .= "<span class='cct_info'>$".number_format((float)$cct_info['tip_amount'], $decimal_places, ".", "")."</span>";
						}
						else
						{
							// show editable textfield

							// if the gateway is eConduit and the iFrame flag is set, we want to submit a tip adjustment
							if (useIFrameTipAdjust($gateway))
							{
								// loadEconduitTipAdjustOrCapture arguments
								$field_id			= "input_card_tip_".$order_identifier;
								$tipAmount			= "0.00";
								$terminalId			= $cct_info['ref_data'];
								$refId				= $cct_info['transaction_id'];
								$rowId				= $cct_info['id'];
								$transactionAmount	= $cct_info['amount'];
								$ioid				= $order_read['ioid'];
								$econduit_command	= ($auth == "0")?"tipadjust":"capture";

								// use eConduit iFrame for tip adjust
								$card_gratuity_string .= "<input onblur='loadEconduitTipAdjustOrCapture(this, \"$field_id\", \"$tipAmount\", \"$terminalId\", \"$refId\", \"$rowId\", \"$transactionAmount\", \"$ioid\", \"$local_time\",  \"$locationid\", \"$data_name\", \"$set_card_tip\", \"$econduit_command\")' type='text' size='6' style='text-align:right' id='input_card_tip_$order_identifier' name='set_cardtip_$order_identifier' value='$set_card_tip'>";
							}
							else
							{
								// use Web API for tip adjust
								$card_gratuity_string .= "<input type='text' size='6' style='text-align:right' id='input_card_tip_$order_identifier' name='set_cardtip_$order_identifier' value='$set_card_tip'>";
							}
						}

						if ($midjs_list!="")
						{
							$midjs_list .= "|";
						}
						$midjs_list .= "$order_identifier";
					}
					$card_gratuity_string .= "</td>";
				}
				else
				{
					$card_gratuity_string = "<td class='info_text' align='center'>-</td><td>&nbsp;</td>";
				}
			}
			else
			{
				$card_gratuity_string = "<td class='info_text' align='center'>-</td><td>&nbsp;</td>";
			}

			$local_time = localize_datetime(date("Y-m-d H:i:s"), $location_info['timezone']);
			if (number_format((float)$correct_cc_amount,2) != number_format((float)$order_read['card_paid'], 2))
			{
				lavu_query("UPDATE `orders` SET `card_paid` = '[1]', `last_modified` = '[2]', `last_mod_ts` = '[3]', `pushed_ts` = '[3]', `last_mod_device` = 'END OF DAY' WHERE `id` = '[4]'", number_format((float)$correct_cc_amount, $decimal_places, ".", ""), $local_time, time(), $order_read['id']);
			}

			if ($correct_tip_amount != $order_read['card_gratuity'])
			{
				lavu_query("UPDATE `orders` SET `card_gratuity` = '[1]', `last_modified` = '[2]', `last_mod_ts` = '[3]', `pushed_ts` = '[3]', `last_mod_device` = 'END OF DAY' WHERE `id` = '[4]'", number_format((float)$correct_tip_amount, $decimal_places, ".", ""), $local_time, time(), $order_read['id']);
			}

			$card_color = ($correct_cc_amount > 0)?"#00aa00":"#bbbbbb";

			$display_card_paid = "<font color='$card_color'>".display_price($correct_cc_amount, $decimal_places)."</font>";
			$display_card_total = "<font color='$card_color'>".display_price(($correct_cc_amount + $correct_tip_amount), $decimal_places)."</font>";

			$amount_due = ($order_read['total'] - $order_read['gift_certificate'] - $correct_cc_amount - $cash_applied - $alt_paid_total);

			if (abs($amount_due) < $smallest_money)
			{
				$amount_due = 0;
			}
			$t_due += $amount_due;

			if (($amount_due < 0) && ($order_read['card_paid'] > 0) && ($order_read['card_paid'] >= (0 - $amount_due)))
			{
				$overpaid_cc_orders[] = $order_identifier.":".(0 - $amount_due);
			}

			$mid .= "<td class='location_link_lg' style='text-align:right'>";
			if ($amount_due > 0)
			{
				$font_color = "#dd0000";
			}
			else if ($amount_due < 0)
			{
				$font_color = "#00dd00";
			}
			else
			{
				$font_color = "#aaaaaa";
			}
			$mid .= "<font color='$font_color'>" . display_price($amount_due, $decimal_places) . "</font>";
			$mid .= "</td>";

			$mid .= $row_after_due;

			$mid .= "<td class='location_link_lg' style='text-align:right'>" . $display_card_paid . "</td><td>&nbsp;</td>";
			$mid .= $card_gratuity_string;

			$mid .= "<td class='location_link_lg' id='show_card_total_$order_identifier' style='text-align:right'>" . $display_card_total . "</td><td width='16'>&nbsp;</td>";
			$mid .= "<td class='location_link_lg' style='text-align:right;$alt_style'$alt_code>" .  $display_gift_certificate . "</td><td>&nbsp;</td>";
			$mid .= "<td class='location_link_lg' style='text-align:right;$alt_style'$alt_code>" .  $display_other_tip_code . "</td><td>&nbsp;</td>";
			$mid .= "<td class='location_link_lg' style='text-align:right'><font color='#005500'>" . display_price($order_read['gratuity'], $decimal_places) . "</font></td><td>&nbsp;</td>";

			$calc_ag_by_order_stub = TRUE;
			$check_count = (int)$order_read['no_of_checks'];

			$st_ag_cash = 0;
			$st_ag_card = 0;

			if ((int)$check_count > 1)
			{
				$get_check_details = rpt_query("SELECT `gratuity`, `cash`, `card`, `gift_certificate`, `alt_paid` FROM `split_check_details` WHERE `loc_id` = '[1]' AND `order_id` = '[2]' AND  `check` <= '[3]' ", $order_read['location_id'], $order_read['order_id'], $check_count);
				if ($get_check_details!==FALSE && mysqli_num_rows($get_check_details)>0)
				{
					$calc_ag_by_order_stub = FALSE;
					while ($info = mysqli_fetch_assoc($get_check_details))
					{
						$ag_cash = 0;
						$ag_card = 0;
						$ag_other = 0;

						distributeAutoGratuity($info['gratuity'], $info['cash'], $info['card'], $info['gift_certificate'], $info['alt_paid'], $ag_cash, $ag_card, $ag_other);

						$st_ag_cash += $ag_cash;
						$st_ag_card += $ag_card;

						$t_ag_cash += $ag_cash;
						$t_ag_card += $ag_card;
						// what to do with $ag_other?
					}
				}
			}

			if ($calc_ag_by_order_stub)
			{
				$ag_cash = 0;
				$ag_card = 0;
				$ag_other = 0;

				distributeAutoGratuity($order_read['gratuity'], $cash_applied, $order_read['card_paid'], $order_read['gift_certificate'], $order_read['alt_paid'], $ag_cash, $ag_card, $ag_other);

				$st_ag_cash = $ag_cash;
				$st_ag_card = $ag_card;

				$t_ag_cash += $ag_cash;
				$t_ag_card += $ag_card;
				// what to do with $ag_other?
			}

			$mid .= "<td class='location_link_lg' style='text-align:right'><font color='#005500'>" . display_price($st_ag_cash, $decimal_places) . "</font></td><td>&nbsp;</td>";
			$mid .= "<td class='location_link_lg' style='text-align:right'><font color='#005500'>" . display_price($st_ag_card, $decimal_places) . "</font></td><td>&nbsp;</td>";

			$mid .= "<td class='location_link_lg' style='text-align:right'><font color='#0000aa'>" . display_price($total_tips, $decimal_places) . "</font></td><td>&nbsp;</td>";

			$mid .= "</tr>";

			if (mysqli_num_rows($check_cct))
			{
				mysqli_data_seek($check_cct, 0);
			}

			if ($show_multiple_ccts || $show_single_cct)
			{
				$mid .= "<tr bgcolor='".$row_color."'>
					<td colspan='3'>&nbsp</td>
					<td align='right' colspan='25'>
						<table cellspacing='0' cellpadding='2'>";
				while ($extract = mysqli_fetch_assoc($check_cct))
				{
					$gateway	= $extract['gateway'];
					$auth		= $extract['auth'];
					$action		= $extract['action'];

					$correct_tip_amount += $extract['tip_amount'];
					$mid .= "<tr>";
					$first_column_content = FALSE;
					if ($auth == "2")
					{
						$first_column_content = TRUE;
						$mid .= "<td class='cct_labels'><font color='#FF3300'>AuthForTab</font></td><td>&nbsp;&nbsp;&nbsp;&nbsp;</td>";
					}
					else
					{
						$signature_file = "/home/poslavu/public_html/admin/images/".admin_info('dataname')."/signatures/".$order_read['location_id']."-".$extract['order_id']."-".$extract['check']."-".$extract['preauth_id'].".jpg";
						if (file_exists($signature_file))
						{
							$first_column_content = TRUE;
							$mid .= "<td class='cct_labels'><a onclick='showSignature(\"".admin_info('dataname')."\", \"".$order_read['location_id']."\", \"".$extract['order_id']."\", \"".$extract['check']."\", \"".$extract['preauth_id']."\", \"".number_format((float)$extract['amount'], $decimal_places, ".", "")."\", \"".number_format((float)$extract['tip_amount'], $decimal_places, ".", "")."\", \"\");' style='cursor:pointer; color:#0000CC;'>".speak("Signature")."</a></td><td>&nbsp;&nbsp;&nbsp;&nbsp;</td>";
						}
						else
						{
							$signature_file = "/home/poslavu/public_html/admin/images/".admin_info('dataname')."/signatures/".$order_read['location_id']."-".$extract['order_id']."-".$extract['check']."-".$extract['preauth_id']."-".$extract['auth_code'].".jpg";
							if (file_exists($signature_file))
							{
								$first_column_content = TRUE;
								$mid .= "<td class='cct_labels'><a onclick='showSignature(\"".admin_info('dataname')."\", \"".$order_read['location_id']."\", \"".$extract['order_id']."\", \"".$extract['check']."\", \"".$extract['preauth_id']."\", \"".number_format((float)$extract['amount'], $decimal_places, ".", "")."\", \"".number_format((float)$extract['tip_amount'], $decimal_places, ".", "")."\", \"".$extract['auth_code']."\");' style='cursor:pointer; color:#0000CC;'>".speak("Signature")."</a></td><td>&nbsp;&nbsp;&nbsp;&nbsp;</td>";
							}
						}
					}

					if ($action == "Refund")
					{
						$mid .= "<td class='cct_labels'><font color='#C90000'>REFUND</font></td><td>&nbsp;&nbsp;&nbsp;&nbsp;</td>";
					}
					else if (!$first_column_content)
					{
						$mid .= "<td></td><td></td>";
					}

					$mid .= "<td class='cct_labels'>".(!empty($extract['card_type'])?$extract['card_type']:speak("Card Transaction"))." ...<span class='cct_info'>".(!empty($extract['card_desc'])?$extract['card_desc']:"")."</span></td><td width='10'></td>";

					$ref_label = speak("Ref #");
					$ref_value = $extract['transaction_id'];
					if ($gateway == "MIT")
					{
						$ref_label = "Folio";
						$ref_value = $extract['record_no'];
					}

					$mid .= (!empty($ref_value)?"<td class='cct_labels'>".$ref_label.": <span class='cct_info'>".$ref_value."</span></td><td width='10'></td>":"");
					$mid .= (!empty($extract['auth_code'])?"<td class='cct_labels'>".speak("Auth Code").": <span class='cct_info'>".$extract['auth_code']."</span></td><td width='10'></td>":"<td></td><td></td>");
					$mid .= "<td class='cct_labels' width='110px'>".speak("Amount").": <span class='cct_info'>".display_price($extract['amount'], $decimal_places)."</span></td><td width='10'></td>";

					if ($show_multiple_ccts || ($show_single_cct && $extract['pay_type_id']!="2"))
					{
						$label = ($action == "Refund")?speak("Refunded Tip"):speak("Tip");
						if ((($extract['processed']=="1" || $auth=="2" || in_array($gateway, array("iZettle", "MIT", "Moneris")) || $lls) && $action=="Sale") || $action=="Refund")
						{
							// show uneditable tip label
							$mid .= "<td class='cct_labels'>".$label.": <span class='cct_info'>".display_price($extract['tip_amount'], $decimal_places)."</span></td>";
						}
						else if ($extract['processed']=="0" && $action=="Sale")
						{
							// check for a matching refund for this transaction
							$foundMatchingRefund = findSaleWithRefund($all_check_ccts, $extract);
							if ($foundMatchingRefund)
							{
								// if the sale was refunded, show uneditable label
								$mid .= "<td class='cct_labels'>".$label.": <span class='cct_info'>".display_price($extract['tip_amount'], $decimal_places)."</span></td>";
							}
							else
							{
								// show editable textfield
								$field_id	= "input_cctip_".$extract['id'];
								$field_name	= "set_cctip_".$extract['id'];
								$fieldValue	= number_format((float)$extract['tip_amount'], $decimal_places, ".", "");

								// if the gateway is eConduit and the iFrame flag is set, we want to submit a tip adjustment
								if (useIFrameTipAdjust($gateway))
								{
									// loadEconduitTipAdjustOrCapture arguments
									$tipAmount			= "0.00";
									$terminalId			= $extract['ref_data'];
									$refId				= $extract['transaction_id'];
									$rowId				= $extract['id'];
									$transactionAmount	= $extract['amount'];
									$ioid				= $extract['ioid'];
									$econduit_command	= ($auth == "0")?"tipadjust":"capture";

									// use eConduit iFrame for tip adjust
									$mid .= "<td class='cct_labels'>".$label.": <input onblur='loadEconduitTipAdjustOrCapture(this, \"$field_id\", \"$tipAmount\", \"$terminalId\", \"$refId\", \"$rowId\", \"$transactionAmount\", \"$ioid\", \"$local_time\",  \"$locationid\", \"$data_name\", \"$set_card_tip\", \"$econduit_command\")' type='text' size='6' style='text-align:right' id='$field_id' name='$field_name' value='$fieldValue'></td>";
								}
								else
								{
									// use Web API for tip adjust
									$mid .= "<td class='cct_labels'>".$label.": <input type='text' size='6' style='text-align:right' id='$field_id' name='$field_name' value='$fieldValue'></td>";
								}
							}

							if ($cctjs_list != "")
							{
								$cctjs_list .= "|";
							}
							$cctjs_list .= $extract['id'];
						}
						else
						{
							$mid .= "<td class='cct_labels' width='85px'>&nbsp;</td>";
						}
					}
					else
					{
						$mid .= "<td class='cct_labels' width='85px'>&nbsp;</td>";
					}
					$mid .= "</tr>";
				}
				$mid .= "</table></td><td colspan='10'>&nbsp;</td></tr>";
			}
		}

		$alt_code = "";
		$layer_code = "";
		if ((count($alt_payment_types) > 0) && ($t_gift_certificate > 0))
		{
			$alt_code = " style='cursor:pointer'";
			$alt_code .= " onmouseover='showAltPayments(\"alt_paid_grand_total\", this);'";
			$alt_code .= " onmouseout='hideAltPayments(\"alt_paid_grand_total\");'";
			$layer_code .= "<div id='alt_paid_grand_total' style='position:absolute; left:0px; top:0px; z-index:".($row + 501)."; display:none;'>";
			$layer_code .= "<table cellspacing='0' cellpadding='1'><tr><td align='center' style='background:URL(images/t_gray1.png); background-repeat:repeat;'>";
			$layer_code .= "<table cellspacing='0' cellpadding='1'><tr><td align='center' style='background:URL(images/t_gray1.png); background-repeat:repeat;'>";
			$layer_code .= "<table cellspacing='0' cellpadding='1'><tr><td align='center' style='background:URL(images/t_gray1.png); background-repeat:repeat;'>";
			$layer_code .= "<table cellspacing='0' cellpadding='4' bgcolor='#FFFFFF'>";
			for ($ap = 0; $ap < count($alt_payment_types); $ap++)
			{
				$layer_code .= "<tr><td align='right'>".$alt_payment_types[$ap][1].":</td><td align='right'>".display_price($alt_paid_grand_totals[$ap], $decimal_places)."</td></tr>";
			}
			$layer_code .= "</table>";
			$layer_code .= "</td></tr></table>";
			$layer_code .= "</td></tr></table>";
			$layer_code .= "</td></tr></table>";
			$layer_code .= "</div>";
		}

		$mid .= "<tr>";
		$mid .= "<td class='tbot' colspan='7' align='right'>TOTALS:</td><td class='tbot'>&nbsp;</td>";
		$mid .= "<td class='tbot'>".display_price(($t_subtotal - $t_itax), $decimal_places)."</td><td class='tbot'>&nbsp;</td>";
		$mid .= "<td class='tbot'>".display_price($t_discount, $decimal_places)."</td><td class='tbot'>&nbsp;</td>";
		$mid .= "<td class='tbot'>".display_price(($t_tax + $t_itax), $decimal_places)."</td><td class='tbot'>&nbsp;</td>";
		$mid .= "<td class='tbot'>".display_price($t_total, $decimal_places)."</td><td class='tbot'>&nbsp;</td>";

		$font_color = ($t_due > 0)?"#dd0000":"#000000";

		$mid .= "<td class='tbot'><font color='$font_color'>".display_price($t_due, $decimal_places)."</font></td><td class='tbot'>&nbsp;</td>";
		$mid .= "<td class='tbot'>".display_price($t_cash, $decimal_places)."</td><td class='tbot'>&nbsp;</td>";
		if ($cash_tip_mode == 0)
		{
			$mid .= "<td class='tbot'>".display_price($t_cash_tip, $decimal_places)."</td><td class='tbot'>&nbsp;</td>";
		}
		$mid .= "<td class='tbot'>".display_price($t_card, $decimal_places)."</td><td class='tbot'>&nbsp;</td>";
		$mid .= "<td class='tbot'>".display_price($t_card_tip, $decimal_places)."</td><td class='tbot'>&nbsp;</td>";
		$mid .= "<td class='tbot' id='show_card_total'>".display_price($t_card_total, $decimal_places)."</td><td class='tbot'>&nbsp;</td>";
		$mid .= "<td class='tbot'$alt_code>".display_price($t_gift_certificate, $decimal_places)."$layer_code</td><td class='tbot'>&nbsp;</td>";
		$mid .= "<td class='tbot'$alt_code>".display_price($other_tip_total, $decimal_places)."</td><td class='tbot'>&nbsp;</td>";
		$mid .= "<td class='tbot'>".display_price($t_auto_grat, $decimal_places)."</td><td class='tbot'>&nbsp;</td>";

		$mid .= "<td class='tbot'>".display_price($t_ag_cash, $decimal_places)."</td><td class='tbot'>&nbsp;</td>";
		$mid .= "<td class='tbot'>".display_price($t_ag_card, $decimal_places)."</td><td class='tbot'>&nbsp;</td>";

		$mid .= "<td class='tbot'>".display_price($t_total_tips, $decimal_places)."</td><td class='tbot'>&nbsp;</td>";
		$mid .= "</tr>";
		$mid .= "</table>";

		$submit_click_code = "document.getElementById(\"eod_form\").submit();";
		$mid .= "<table width='100%'><tr><td align='center'><br><br>";
		if ($lls)
		{
			$mid .= speak("Clients using local server must submit all tips and authorization captures through the POS Lavu Client app").".";
		}
		else
		{
			$mid .= "<input type='button' value='$submit_title' onclick='$submit_click_code'>";
		}
		$mid .= "</td></tr></table>";
		$mid .= $hidden_fields."<input type='hidden' name='cashtip_list' value='$cashtipjs_list'>
			<input type='hidden' name='othertip_list' value='$othertipjs_list'>
			<input type='hidden' name='cardtip_list' value='$midjs_list'>
			<input type='hidden' name='cctip_list' value='$cctjs_list'>
			<input type='hidden' name='overpaid_cc_orders' value='".join("|", $overpaid_cc_orders)."'>
		</form>";
		$mid .= "</td></tr>";
		/****square reconciliation code****/
		$resp .= getReconciliationHtml($start_datetime, $end_datetime);
	}
	else
	{
		$mid .= "<tr><td align='center'>". speak("Error: User data not found in the database"). "</td></tr>";
	}
}
else if ($cc_settle)
{
	if ((time() - $confirm_batch_ts) < 30)
	{
		$dataname = admin_info('dataname');
		$companyid = admin_info('companyid');
		$serverid = admin_info('loggedin');
		$username = admin_info('username');

		//echo "The dataname is:". $dataname. " The company id is: ". $companyid . " the serverid is: ". $serverid . " the username is: ". $username . " <br> ";

		require_once(__DIR__."/settle_batch.php");

		$mid .= "<tr><td><table cellpadding='15' width='100%'><tr><td align='center'><input type='button' value='" . speak("Continue") . "' onclick='window.location = \"index.php?mode={$section}_{$mode}\"'></td></tr></table></td></tr>";
	}
	else
	{
		$mid .= "<tr><td align='center'>" . speak("To help prevent unintended batch settlements, the Settle Credit Card Batch button has a 30 second timeout") . ".<br><br>". speak("Please try again if you're certain you want to settle the current batch") . ".</td></tr>";
		$mid .= "<tr><td>&nbsp;</td></tr>";
		$mid .= "<tr><td>&nbsp;</td></tr>";
		$mid .= "<tr><td align='center'><input type='button' value='".speak("Settle Credit Card Batch")."' onclick='window.location = \"index.php?mode={$section}_{$mode}&cc_settle=1&cbts=".time()."\";'> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <input type='button' value='". speak("Continue Without Settling"). "' onclick='window.location = \"index.php?mode={$section}_{$mode}\"'></td></tr>";
	}
}
else if ($locationid)
{
	$location_date_format = "";
	switch ($date_format['value']){
		case 1: $location_date_format = "d-m-Y H:i:s";
				$location_date_letter_format = "j-n-Y";
		break;
		case 2: $location_date_format = "m-d-Y H:i:s";
				$location_date_letter_format = "n-j-Y";
		break;
		case 3: $location_date_format = "Y-m-d H:i:s";
				$location_date_letter_format = "Y-n-j";
		break;
	}
	$title = speak("End of Day Report").": ".$location_info['title']."<br>";

	$use_date = (isset($_REQUEST['date']))?$_REQUEST['date']:date("Y-m-d");
	$udate = explode("-", $use_date);
	$uyear = $udate[0];
	$umonth = $udate[1];
	$uday = $udate[2];
	$use_ts = mktime($se_hour, $se_min, 0, $udate[1], $udate[2], $udate[0]);
	$start_datetime = date($location_date_format, $use_ts);
	$udate_display = date("n/j/Y", $use_ts);

	$use_date2 = (isset($_REQUEST['date2']))?$_REQUEST['date2']:$use_date;
	$udate2 = explode("-", $use_date2);
	$uyear2 = $udate2[0];
	$umonth2 = $udate2[1];
	$uday2 = $udate2[2];
	$use_ts2 = mktime($se_hour, $se_min, 0, $udate2[1], $udate2[2], $udate2[0]);
	$end_datetime = date($location_date_format, ($use_ts2 + 86399));
	$udate_display = date("n/j/Y", $use_ts2);

	$date_diff = floor(((time() - $use_ts) / 60 / 60 / 24));

	$title .= "<br>";
	$title .= "<select onchange='window.location = \"index.php?mode={$section}_{$mode}&date=\" + this.value + \"&date2=\" + this.value;'>";
	for ($i = -1; $i <= (366 + $date_diff); $i++)
	{
		$idate = mktime(0,0,0,date("m"),date("d") - $i,date("Y"));
		$idate_value = date("Y-m-d",$idate);
		switch ($date_format['value']){
				case 1: $idate_display = date("d/m/Y",$idate);

				break;
				case 2:$idate_display = date("m/d/Y",$idate);break;
				case 3: $idate_display = date("Y/m/d",$idate);

				break;

		}
		$title .= "<option value='$idate_value'";
		if ($idate_value==$use_date)
		{
			$title .= " selected";
		}
		$title .= ">$idate_display</option>";
	}
	$title .= "</select>";

	$title .= "<select onchange='window.location = \"index.php?mode={$section}_{$mode}&date=$use_date&date2=\" + this.value;'>";
	$title .= "<option value='$use_date'";
	if ($use_date == $use_date2)
	{
		$title .= " selected";
	}
	$title .= ">---</option>";
	for ($i = 1; $i <= 62; $i++)
	{
		$idate = mktime(0, 0, 0, $umonth, ($uday + $i), $uyear);
		$idate_value = date("Y-m-d",$idate);
		if ($idate_value <= date("Y-m-d"))
		{
			$title .= "<option value='$idate_value'";
			if ($idate_value == $use_date2)

				$title .= " selected";
			switch ($date_format['value']){
						case 1: $idate_display = date("d/m/Y",$idate);

						break;
						case 2: $idate_display = date("m/d/Y",$idate);

						break;
						case 3: $idate_display = date("Y/m/d",$idate);

						break;}
			$title .= ">$idate_display</option>";
		}
	}
	$title .= "</select><br></b><span style='font-family:Verdana, Arial, Helvetica, sans-serif; font-size:10px; color:#333333;'>($start_datetime to $end_datetime)</span><b>";

	$eod_ts = $use_ts;

	function getCashTip777RowsIfExistsFalseIfNot()
	{
		global $eod_ts;
		global $eod_filter;

		$startd = date("Y-m-d H:i:s", $eod_ts);
		$endd = date("Y-m-d H:i:s", ($eod_ts + 86399));
		$totalCashTipsResult = lavu_query("SELECT * FROM `orders` WHERE `order_id` LIKE '777%' AND `[1]` > '[2]' AND `[1]` <= '[3]'", $eod_filter, $startd, $endd);
		if (!$totalCashTipsResult)
		{
			return FALSE;
		}

		if (mysqli_num_rows($totalCashTipsResult) == 0)
		{
			return FALSE;
		}

		$cashTipRowsArr = array();
		while ($currRow = mysqli_fetch_assoc($totalCashTipsResult))
		{
			$cashTipRowsArr[] = $currRow;
		}

		return $cashTipRowsArr;
	}

			//LP-2198: To get the Tax calculation get_eod_tax() is used
			//LP-6200: Removing [money] keyword from "tax" field because it gets added in php (and never used anywhere else)
			function get_eod_tax($locationid, $filter, $startd, $endd, $field) {
				$def = 0;
				$tax_query = rpt_query("select order_contents.tax_inclusion, order_contents.tax_rate1*1 as tax_rate, order_contents.tax_exempt, count(order_contents.tax_inclusion) as count, sum(order_contents.tax1 + order_contents.tax2 + order_contents.tax3 + order_contents.tax4 + order_contents.tax5) as tax from `orders` left join `order_contents` on `orders`.`order_id` = `order_contents`.`order_id` and `orders`.`location_id` = `order_contents`.`loc_id` where orders.$filter >= '$startd' and orders.$filter < '$endd' and orders.void = '0' and order_contents.loc_id = '$locationid' and order_contents.quantity > '0' group by order_contents.tax_inclusion, order_contents.tax_exempt order by tax_rate desc");
				if (mysqli_num_rows($tax_query)) {
					while ($get_total_taxes = mysqli_fetch_assoc($tax_query)) {
						if ($field == 'itax') {
							if ($get_total_taxes['tax_inclusion'] == 1 && $get_total_taxes['tax_exempt'] == 0) {
								$def = $def + $get_total_taxes['tax'];
							}
						} else {
							if ($get_total_taxes['tax_inclusion'] == 0 && $get_total_taxes['tax_exempt'] == 0) {
								$def = $def + $get_total_taxes['tax'];
							}
						}
					}
				}
				return $def;
			}

			function get_rounding_amount($locationid)
			{
		global $eod_ts;
		global $eod_filter;
if($filter=="") $filter = $eod_filter;
		$startd = date("Y-m-d H:i:s", $eod_ts);
				$endd = date("Y-m-d H:i:s", ($eod_ts + 86399));
				$check_rounding = rpt_query("select count(*) as count,if (count(*) > 0, concat('[money]',format(cast(sum(split_check_details.gratuity)as decimal(30,5)),3)), '0.00') as gratuity, if(count(*) > 0, concat(cast(sum(split_check_details.rounding_amount)as decimal(30,5))), '0.00') as rounding_amount from `orders`LEFT JOIN `split_check_details` ON `orders`.`order_id` = `split_check_details`.`order_id` AND `orders`.`location_id` = `split_check_details`.`loc_id` where orders.$filter >='$startd' and orders.$filter < '$endd' and orders.void = '0' and orders.location_id = '$locationid' and orders.order_id NOT LIKE 'Paid%' order by (sum(split_check_details.gratuity)*1) asc ");
		$get_total_round_amt = mysqli_fetch_assoc($check_rounding);
			return $get_total_round_amt['rounding_amount'];
			}

			function get_eod_taxexempt($locationid) {
				global $eod_ts;
				$filter = 'closed';
				$startd = date("Y-m-d H:i:s", $eod_ts);
				$endd = date("Y-m-d H:i:s", ($eod_ts + 86399));
				$where_clause = "WHERE `orders`.`order_id` NOT LIKE 'Paid%' AND `orders`.`order_id` NOT LIKE '777%' AND `orders`.`$filter` >= '$startd' AND `orders`.`$filter` <= '$endd' AND `orders`.`void` = '0' AND `orders`.`location_id` = '$locationid' AND `order_contents`.`tax_exempt` = 1";
				$taxexempt_totals = rpt_query("SELECT SUM(`order_contents`.`tax1` + `order_contents`.`tax2` + `order_contents`.`tax3` + `order_contents`.`tax4` + `order_contents`.`tax5`) AS `tax` FROM `order_contents` LEFT JOIN `orders` ON `orders`.`order_id` = `order_contents`.`order_id` ".$where_clause);
				$taxexempt_totals_read = mysqli_fetch_assoc($taxexempt_totals);
				return $taxexempt_totals_read['tax'];
			}

			function get_eod_includedtaxexemptions($locationid) {
				global $eod_ts;
				$filter = 'closed';
				$startd = date("Y-m-d H:i:s", $eod_ts);
				$endd = date("Y-m-d H:i:s", ($eod_ts + 86399));
				$where_clause = "WHERE `orders`.`order_id` NOT LIKE 'Paid%' AND `orders`.`order_id` NOT LIKE '777%' AND `orders`.`$filter` >= '$startd' AND `orders`.`$filter` <= '$endd' AND `orders`.`void` = '0' AND `orders`.`location_id` = '$locationid' AND `order_contents`.`tax_exempt` = 1 AND `order_contents`.`tax_inclusion` = 1";
				$taxexempt_totals = rpt_query("SELECT SUM(`order_contents`.`tax1` + `order_contents`.`tax2` + `order_contents`.`tax3` + `order_contents`.`tax4` + `order_contents`.`tax5`) AS `tax` FROM `order_contents` LEFT JOIN `orders` ON `orders`.`order_id` = `order_contents`.`order_id` ".$where_clause);
				$taxexempt_totals_read = mysqli_fetch_assoc($taxexempt_totals);
				return $taxexempt_totals_read['tax'];
			}

			function get_charge_total($locationid) {
				global $eod_ts;
				$filter = 'closed';
				$startd = date("Y-m-d H:i:s", $eod_ts);
				$endd = date("Y-m-d H:i:s", ($eod_ts + 86399));
				$where_clause = "WHERE `orders`.`order_id` NOT LIKE 'Paid%' AND `orders`.`order_id` NOT LIKE '777%' AND `orders`.`$filter` >= '$startd' AND `orders`.`$filter` <= '$endd' AND `orders`.`void` = '0' AND `orders`.`location_id` = '$locationid'";
				$taxexempt_totals = rpt_query("SELECT SUM(`gratuity`) AS `charge_amount` FROM `orders` ".$where_clause);
				$taxexempt_totals_read = mysqli_fetch_assoc($taxexempt_totals);
				return $taxexempt_totals_read['charge_amount'];
			}

			function get_eod_total($locationid, $field, $def="&nbsp;",$filter ="")
			{ //get_eod_total($locationid,'gift_certificate')
				global $eod_ts;
				global $eod_filter;
		if($filter=="") $filter = $eod_filter;

				$startd = date("Y-m-d H:i:s", $eod_ts);
				$endd = date("Y-m-d H:i:s", ($eod_ts + 86399));

				$where_clause = "WHERE `order_id` NOT LIKE 'Paid%' AND `order_id` NOT LIKE '777%' AND `$filter` >= '$startd' AND `$filter` <= '$endd' AND `void` = '0' AND `location_id` = '$locationid'";

		if ($field == "idiscount_amount")
		{ // pull total from split_check_details since population of orders.idiscount_amount is not reliable before version 3.0.3
			$total_query = rpt_query("SELECT SUM(`$field`) AS `sum_total` FROM `split_check_details` WHERE `order_id` IN (SELECT `order_id` FROM `orders` ".$where_clause.")");

		}
				/*LP-2198
				 * Fetching Subtotal of Super Group
				 * using the same query that used in V2-Report
				 *
				 */
				elseif($field == 'show_subtotal')
				{
					$show_total_sales = rpt_query("select if(count(*) > 0, concat(cast((sum((order_contents.price + order_contents.modify_price + order_contents.forced_modifiers_price)*order_contents.quantity - if( order_contents.quantity > '0', order_contents.itax, 0) ))as decimal(30,5))), '[money]0.00') as gross from `orders`LEFT JOIN `order_contents` ON `orders`.`order_id` = `order_contents`.`order_id` AND `orders`.`location_id` = `order_contents`.`loc_id` LEFT JOIN `menu_items` ON `order_contents`.`item_id` = `menu_items`.`id` LEFT JOIN `menu_categories` ON `menu_items`.`category_id` = `menu_categories`.`id` LEFT JOIN `super_groups` ON IF (menu_items.super_group_id = '', menu_categories.super_group_id, menu_items.super_group_id) = `super_groups`.`id` where orders.$filter >= '$startd' and orders.$filter < '$endd' and orders.void = '0' and order_contents.item != 'SENDPOINT' and order_contents.loc_id = '$locationid' group by super_groups.title order by (sum((order_contents.price + order_contents.modify_price + order_contents.forced_modifiers_price)*order_contents.quantity - if( order_contents.quantity > '0', order_contents.itax, 0) )*1) desc");
					if(mysqli_num_rows($show_total_sales))
					{
						while($get_total_sales = mysqli_fetch_assoc($show_total_sales))
						{
							$def = $def + $get_total_sales['gross'];
						}
					}
				}
				/*LP-2198
				 * Fetching Included Tax and Excluded Tax
				 * using the same query that used in V2-Report
				 *
				 */
				elseif($field == 'itax' || $field == 'tax')
				{
					$def = get_eod_tax($locationid,$filter,$startd,$endd,$field);
				} elseif ($field == 'gratuity') {
					$total_query = rpt_query("SELECT SUM(IF((refunded + refunded_cc + refunded_gc + alt_refunded) != 0, IF(cash_paid = 0, IF(card_paid = 0, IF(alt_paid + gift_certificate = 0, '0.00', `$field`), `$field`), `$field`), `$field`)) AS `sum_total` FROM `orders` ".$where_clause);
				} else
		{
			$total_query = rpt_query("SELECT SUM(`$field`) AS `sum_total` FROM `orders` ".$where_clause);
			//if($field=='deposit_amount')
			//echo "SELECT SUM(`$field`) AS `sum_total` FROM `orders` WHERE `order_id` NOT LIKE 'Paid%' AND `$filter` >= '$startd' AND `$filter` <= '$endd' AND `void` = '0' AND `location_id` = '$locationid';<br>";
		}

		if (mysqli_num_rows($total_query))
		{
			$total_read = mysqli_fetch_assoc($total_query);
			return $total_read['sum_total'];
		}

		else
		{
			return $def;
		}
	}

	function get_eod_server_list($locationid, $filter="")
	{
		global $eod_ts;
		global $eod_filter;

		if ($filter == "")
		{
			$filter = $eod_filter;
		}

		$startd = date("Y-m-d H:i:s", $eod_ts);
		$endd = date("Y-m-d H:i:s", ($eod_ts + 86399));

		$id_list = array();
		$get_server_ids = rpt_query("SELECT `server_id` FROM `orders` WHERE `$filter` >= '$startd' AND `$filter` <= '$endd' AND `void` = '0' AND `location_id` = '$locationid' GROUP BY `server_id`");
		if (mysqli_num_rows($get_server_ids))
		{
			while ($info = mysqli_fetch_assoc($get_server_ids))
			{
				$id_list[] = $info['server_id'];
			}
		}

		$server_list = array();
		if (count($id_list) > 0)
		{
			$get_server_info = rpt_query("SELECT `id`, `f_name`, `l_name` FROM `users` WHERE `id` IN (".implode(",", $id_list).") ORDER BY `f_name` ASC");
			while ($info = mysqli_fetch_assoc($get_server_info))
			{
				$server_list[] = array($info['id'], trim($info['f_name']), trim($info['l_name']));
			}
		}

		return $server_list;
	}

	function get_paidin_paidout($locationid)
	{
		global $eod_ts;
		global $eod_filter;

		if ($filter == "")
		{
			$filter = $eod_filter;
		}

		$startd = date("Y-m-d H:i:s", $eod_ts);
		$endd = date("Y-m-d H:i:s", ($eod_ts + 86399));
		$paidin = 0;
		$paidout = 0;

		$trans_query = rpt_query("select sum(`amount`) as `total` from `cc_transactions` where `loc_id`='[1]' and `datetime`>='[2]' and `datetime`<='[3]' and `order_id` LIKE 'Paid In'",$locationid,$startd,$endd);
		if (mysqli_num_rows($trans_query))
		{
			$trans_read = mysqli_fetch_assoc($trans_query);
			$paidin = $trans_read['total'];
		}

		$trans_query = rpt_query("select sum(`amount`) as `total` from `cc_transactions` where `loc_id`='[1]' and `datetime`>='[2]' and `datetime`<='[3]' and `order_id` LIKE 'Paid Out'",$locationid,$startd,$endd);
		if (mysqli_num_rows($trans_query))
		{
			$trans_read = mysqli_fetch_assoc($trans_query);
			$paidout = $trans_read['total'];
		}

		return ($paidin - $paidout);
	}

	function get_deposits_refunds($locationid)
	{
		global $eod_ts;
		global $eod_filter;
		if ($filter == "")
		{
			$filter = $eod_filter;
		}
		$startd = date("Y-m-d H:i:s", $eod_ts);
		$endd = date("Y-m-d H:i:s", ($eod_ts + 86399));
		$total = 0;
		$trans_query = rpt_query("select sum( IF(transtype='deposit_refund', -1*amount, amount)) as `total` from `cc_transactions` where `loc_id`='[1]' and `datetime`>='[2]' and `datetime`<='[3]' and  transtype in ('deposit_refund','deposit')",$locationid,$startd,$endd);
		if (mysqli_num_rows($trans_query))
		{
			$trans_read = mysqli_fetch_assoc($trans_query);
			$total = $trans_read['total'];
		}
		return $total;
	}

	function get_deposit_amount($location_id, $def=0, $tabletarget="orders", $filter="")
	{
		global $eod_ts;
		global $eod_filter;

		if ($filter == "")
		{
			$filter = $eod_filter;
		}

		$arguments = array (
			'tabletarget' => $tabletarget,
			'locationid' => $location_id,
			'filter' => $filter,
			'startd' => date("Y-m-d H:i:s", $eod_ts),
			'endd' => date("Y-m-d H:i:s", ($eod_ts + 86399))
		);
		$totals_query = rpt_query("SELECT SUM(if( `cc_transactions`.`action`='Refund', -`cc_transactions`.`total_collected`,`cc_transactions`.`total_collected`)) AS `sum_total` FROM `orders` JOIN `cc_transactions` ON `orders`.`order_id` = `cc_transactions`.`order_id` AND `orders`.`location_id` = `cc_transactions`.`loc_id` WHERE `[tabletarget]`.`[filter]` >= '[startd]' AND `[tabletarget]`.`[filter]` <= '[endd]' AND `orders`.`void` = '0' AND `cc_transactions`.`voided`='0' AND `cc_transactions`.`action` IN ('Sale', 'Refund') AND `cc_transactions`.`is_deposit`='1' AND `orders`.`location_id`='[locationid]'", $arguments );
		if ($totals_query !== FALSE )
		{
			if (mysqli_num_rows($totals_query))
			{
				$total_read = mysqli_fetch_assoc($totals_query);
				return $total_read['sum_total'];
			}
		}
		return $def;
	}

	function get_alt_eod_total($locationid, $type_id, $def="&nbsp;",$filter="",&$tip_total)
	{
		global $eod_ts;
		global $eod_filter;

		if ($filter == "")
		{
			$filter = $eod_filter;
		}

		$startd = date("Y-m-d H:i:s", $eod_ts);
		$endd = date("Y-m-d H:i:s", ($eod_ts + 86399));
		// $total_query = rpt_query("SELECT SUM(`alternate_payment_totals`.`amount`) AS `sum_total`, SUM(`alternate_payment_totals`.`tip`) AS `sum_tip` FROM `alternate_payment_totals` LEFT JOIN `orders` ON `orders`.`location_id` = `alternate_payment_totals`.`loc_id` AND `orders`.`order_id` = `alternate_payment_totals`.`order_id` WHERE `orders`.`$filter` >= '$startd' AND `orders`.`$filter` <= '$endd' AND `orders`.`void` = '0' AND `alternate_payment_totals`.`loc_id` = '$locationid' AND `alternate_payment_totals`.`pay_type_id` = '$type_id'");
		$total_query = rpt_query("SELECT SUM(IF(`action`='Refund', 0 - `cc_transactions`.`total_collected`, `cc_transactions`.`total_collected`)) AS `sum_total`, SUM(IF(`action`='Refund', 0 - `cc_transactions`.`tip_amount`, `cc_transactions`.`tip_amount`)) AS `sum_tip` FROM `cc_transactions` LEFT JOIN `orders` ON `orders`.`location_id` = `cc_transactions`.`loc_id` AND `orders`.`order_id` = `cc_transactions`.`order_id` WHERE `orders`.`$filter` >= '$startd' AND `orders`.`$filter` <= '$endd' AND `orders`.`void` = '0' AND `cc_transactions`.`loc_id` = '$locationid' AND `cc_transactions`.`pay_type_id` = '$type_id' AND `voided`='0' AND (`action`='Sale' OR `action`='Refund')");
		if (mysqli_num_rows($total_query))
		{
			$total_read = mysqli_fetch_assoc($total_query);
			$tip_total = $total_read['sum_tip'];
			return $total_read['sum_total'];
		}
		return $def;
	}

	function get_order_count($locationid, $def="&nbsp;", $filter="")
	{
		global $eod_ts;
		global $eod_filter;

		if ($filter == "")
		{
			$filter = $eod_filter;
		}

		$startd = date("Y-m-d H:i:s", $eod_ts);
		$endd = date("Y-m-d H:i:s", ($eod_ts + 86399));
		$thingy = "SELECT COUNT(`id`) FROM `orders` WHERE `order_id` NOT LIKE 'Paid%' AND `$filter` >= '$startd' AND `$filter` < '$endd' AND `void` = '0' AND `location_id` = '$locationid'";
		//echo "thingy: $thingy<br>";
		$get_count = rpt_query($thingy);
		$count = (int)mysqli_result($get_count, 0);

		return $count;
	}

	function get_ag_totals($locationid, $filter="")
	{
		global $eod_ts;
		global $eod_filter;
		if ($filter == "") $filter = $eod_filter;

		$t_details = array();
		$t_details['cash'] = 0;
		$t_details['card'] = 0;

		$startd = date("Y-m-d H:i:s", $eod_ts);
		$endd = date("Y-m-d H:i:s", ($eod_ts + 86399));
		$order_query = rpt_query("SELECT `order_id`, `no_of_checks`, `gratuity`, `cash_applied`, `card_paid`, `gift_certificate`, `alt_paid` FROM `orders` WHERE `order_id` NOT LIKE 'Paid%' AND `order_id` NOT LIKE '777%' AND `$filter` >= '$startd' AND `$filter` < '$endd' AND `void` = '0' AND `location_id` = '$locationid' AND `gratuity`!='' AND `gratuity`>0");
		while ($order_read = mysqli_fetch_assoc($order_query))
		{
			$calc_ag_by_order_stub = TRUE;
			$check_count = (int)$order_read['no_of_checks'];

			if ((int)$check_count > 1)
			{
				$get_check_details = rpt_query("SELECT `gratuity`, `cash`, `card`, `gift_certificate`, `alt_paid` FROM `split_check_details` WHERE `loc_id` = '[1]' AND `order_id` = '[2]' AND  `check` <= '[3]' ", $locationid, $order_read['order_id'], $check_count);
				if ($get_check_details!==FALSE && mysqli_num_rows($get_check_details)>0)
				{
					$calc_ag_by_order_stub = FALSE;
					while ($info = mysqli_fetch_assoc($get_check_details))
					{
						$ag_cash = 0;
						$ag_card = 0;
						$ag_other = 0;

						distributeAutoGratuity($info['gratuity'], $info['cash'], $info['card'], $info['gift_certificate'], $info['alt_paid'], $ag_cash, $ag_card, $ag_other);

						$t_details['cash'] += ($ag_cash * 1);
						$t_details['card'] += ($ag_card * 1);
						$t_details['other'] += ($ag_other * 1);
					}
				}
			}

			if ($calc_ag_by_order_stub)
			{
				$ag_cash = 0;
				$ag_card = 0;
				$ag_other = 0;

				distributeAutoGratuity($order_read['gratuity'], $order_read['cash_applied'], $order_read['card_paid'], $order_read['gift_certificate'], $order_read['alt_paid'], $ag_cash, $ag_card, $ag_other);

				$t_details['cash'] += ($ag_cash * 1);
				$t_details['card'] += ($ag_card * 1);
				$t_details['other'] += ($ag_other * 1);
			}
		}

		return $t_details;
	}

	function get_card_source_breakdown($location_info)
	{
		global $data_name;
		global $eod_ts;
		global $eod_filter;
		global $modules;

		$src_brkdwn = array();
		if ($location_info['gateway'] == "Mercury")
		{
			$int_data = get_integration_from_location($location_info['id'], "poslavu_".$data_name."_db");
			if (!empty($int_data['integration3']) && !empty($int_data['integration4']))
			{
				$src_brkdwn['hc_base'] = 0;
				$src_brkdwn['hc_tips'] = 0;
			}

			if ($modules->hasModule("extensions.payment.mercury.wellsfargo"))
			{
				$src_brkdwn['qr_base'] = 0;
				$src_brkdwn['qr_tips'] = 0;
			}

			$startd = date("Y-m-d H:i:s", $eod_ts);
			$endd = date("Y-m-d H:i:s", ($eod_ts + 86399));

			$get_transactions = rpt_query("SELECT `cc_transactions`.`action`, `cc_transactions`.`total_collected`, `cc_transactions`.`tip_amount`, `cc_transactions`.`mpshc_pid`, `cc_transactions`.`reader` FROM `cc_transactions` LEFT JOIN `orders` ON `cc_transactions`.`order_id` = `orders`.`order_id` WHERE `orders`.`order_id` NOT LIKE 'Paid%' AND `orders`.`$eod_filter` >= '$startd' AND `orders`.`$eod_filter` <= '$endd' AND `orders`.`void` = '0' AND `cc_transactions`.`pay_type_id` = '2' AND `cc_transactions`.`got_response` = '1' AND `cc_transactions`.`voided` = '0' AND `orders`.`location_id` = '".$location_info['id']."'");
			if (mysqli_num_rows($get_transactions))
			{
				while ($info = mysqli_fetch_assoc($get_transactions))
				{
					$base_key = "primary_base";
					$tips_key = "primary_tips";
					if ($info['reader'] == "zbar")
					{
						$base_key = "qr_base";
						$tips_key = "qr_tips";
					}
					else if (!empty($info['mpshc_pid']))
					{
						$base_key = "hc_base";
						$tips_key = "hc_tips";
					}

					if (!isset($src_brkdwn[$base_key]))
					{
						$src_brkdwn[$base_key] = 0;
					}

					if (!isset($src_brkdwn[$tips_key]))
					{
						$src_brkdwn[$tips_key] = 0;
					}

					if ($info['action'] == "Sale")
					{
						$src_brkdwn[$base_key] += $info['total_collected'];
						$src_brkdwn[$tips_key] += $info['tip_amount'];
					}
					else
					{
						$src_brkdwn[$base_key] -= $info['total_collected'];
						$src_brkdwn[$tips_key] -= $info['tip_amount'];
					}
				}
			}

			if (isset($src_brkdwn['hc_base']) || isset($src_brkdwn['qr_base']))
			{
				if (!isset($src_brkdwn['primary_base']))
				{
					$src_brkdwn['primary_base'] = 0;
				}

				if (!isset($src_brkdwn['primary_tips']))
				{
					$src_brkdwn['primary_tips'] = 0;
				}
			}
			else
			{
				$src_brkdwn = array();
			}
		}

		return $src_brkdwn;
	}

	if ($use_date != $use_date2)
	{
		function md_title($str)
		{
			global $mid;
			$mid .= "<td align='center' style='border-bottom:solid 1px black'>$str</td>";
		}

		function md_tot($str)
		{
			global $mid;
			global $decimal_places;
			$mid .= "<td style='border-top:solid 1px black'>".display_price($str, $decimal_places)."</td>";
		}

		$mt_subtotal = 0;
		$mt_item_discount = 0;
		$mt_discount = 0;
		$mt_itax = 0;
		$mt_tax = 0;
		$mt_taxexempt = 0;
		$mt_auto_tip = 0;
		$mt_total = 0;
		$mt_cash = 0;
		$mt_card_subtotal = 0;
		$mt_card_tips = 0;
		$mt_card_total = 0;
		$mt_gc_total = 0;
		$mt_due = 0;
		$mt_deposits_collected = 0;
		$mt_deposits_used = 0;
		$mt_rounding_amount = 0;

		$mid .= "<table cellpadding=5 cellspacing=0>";
		$mid .= "<tr>";
		md_title(speak("Date"));
		md_title(speak("Orders"));
		md_title(speak("Guests"));
		md_title(speak("Total Sales"));
		md_title(speak("Discount"));
		md_title(speak("Tax"));
		md_title(speak("Tax Exemptions"));
		md_title(speak("Auto Grat"));
		md_title(speak("Check Rounding"));
		md_title(speak("Total"));
		md_title(speak("Cash"));
		md_title(speak("Card Subt"));
		md_title(speak("Card Grat"));
		md_title(speak("Card Total"));
		md_title($alt_m_title);
		md_title(speak("Due"));
		md_title(speak("Deposits Collected"));
		md_title(speak("Deposits Used"));
		$mid .= "</tr>";
	}

	$ecount = 1;

	while($eod_ts <= $use_ts2 && $ecount < 365)
	{
		//echo $eod_ts . " (".date("m/d/Y H:i:s",$eod_ts).")" . "<br>";
		$ecount++;
		$t_total = get_eod_total($locationid,'total');
		$t_auto_gratuity = get_eod_total($locationid,'gratuity');

		$t_order_count = get_order_count($locationid);
		$t_guest_count = get_eod_total($locationid,'guests');

		$t_item_disc = get_eod_total($locationid, 'idiscount_amount');
		$t_disc = get_eod_total($locationid,'discount');
		$t_itax = get_eod_total($locationid,'itax');
		//LP-2198: Replacing the subtotal query by V2 Report Query
		$t_subt = get_eod_total($locationid,'show_subtotal');
		$t_deposits_used = get_deposit_amount($locationid);
		$t_deposits_collected = get_deposit_amount($locationid, 0, 'cc_transactions', 'datetime');

		$show_total_sales = display_price(($t_subt), $decimal_places);
		$show_item_discount = display_price($t_item_disc, $decimal_places);
		$show_subtotal = display_price(($t_subt - $t_item_disc), $decimal_places);
		$show_check_discount = display_price($t_disc, $decimal_places);
		$show_combined_discount = display_price(($t_item_disc + $t_disc), $decimal_places);
		$show_subtotal_after_discount = display_price((($t_subt * 1) - ($t_item_disc * 1) - ($t_disc * 1)), $decimal_places);
		$get_total_tax =  (get_eod_total($locationid,'tax')+$t_itax);
		$get_rounding_amount = get_rounding_amount($locationid);
		$show_rounding_amount = display_price($get_rounding_amount, $decimal_places);
		$show_tax = display_price((get_eod_total($locationid,'tax') + $t_itax), $decimal_places);
		$get_total_taxexempt = get_eod_taxexempt($locationid);
		$show_tax_exempt_amount = display_price($get_total_taxexempt, $decimal_places);
		$get_total_included_taxexemptions = (get_eod_includedtaxexemptions($locationid) != 0) ? '-'.get_eod_includedtaxexemptions($locationid) : get_eod_includedtaxexemptions($locationid);
		$show_included_tax_exempt_amount = display_price($get_total_included_taxexemptions, $decimal_places);
		$show_auto_gratuity = display_price($t_auto_gratuity, $decimal_places);
		$t_charge_amount = get_charge_total($locationid);
		$show_charge_amount = display_price($t_charge_amount, $decimal_places);
		// LP-2198 - Modified Show total by using the $item_total variable
		$item_total = (($t_subt + $t_charge_amount + $get_total_tax + $get_rounding_amount) - ($t_item_disc + $t_disc) - $t_deposits_used + $get_total_included_taxexemptions);
		$calc_total = display_price((($t_subt * 1) - ($t_disc * 1 + $t_item_disc * 1) + $get_total_tax + $get_rounding_amount + $t_charge_amount + $get_total_included_taxexemptions), $decimal_places);
		$show_total = display_price($item_total, $decimal_places);
		$cash_paid = get_eod_total($locationid,'cash_paid');
		$show_cash = display_price($cash_paid, $decimal_places);

		$paidin_paidout = get_paidin_paidout($locationid);
		$show_paidin_paidout = display_price($paidin_paidout,$decimal_places);

		$deposits_refunds= get_deposits_refunds($locationid);
		$show_deposits_refunds = display_price($deposits_refunds,$decimal_places);

		$t_card_subtotal = get_eod_total($locationid,'card_paid');
		$t_card_tips = get_eod_total($locationid,'card_gratuity');
		$t_card_total = $t_card_subtotal + $t_card_tips;

		$card_source_breakdown = get_card_source_breakdown($location_info);

		$cashTipEODTotal = get_eod_total($locationid,'cash_tip');
		$dailyCashTipTotal = get_daily_cash_tip_total($locationid, "all", $use_date);
		$cashTip777Rows = getCashTip777RowsIfExistsFalseIfNot();

		//Added by Leif G.
		$cash_tip_database_name = "poslavu_".$data_name."_db";
		$isDailyTotalQuery = "SELECT `value` from `".$cash_tip_database_name."`.`config` WHERE `setting`='cash_tips_as_daily_total'";
		$isDailyTotal = mlavu_query($isDailyTotalQuery);
		$isDailyTotal = mysqli_fetch_assoc($isDailyTotal);

		$cash_gratuity_query = "SELECT SUM(`cash_tip`) as 'cash_tip' FROM `".$cash_tip_database_name."`.`orders` ";
		if ($isDailyTotal['value'] == 1)
		{
			$cash_gratuity_query .= "WHERE `orders`.`order_id` LIKE '777%' AND ";
		}
		else
		{
			$cash_gratuity_query .= "WHERE `orders`.`order_id` NOT LIKE '777%' AND ";
		}

		$startd = date("Y-m-d H:i:s", $eod_ts);
		$endd = date("Y-m-d H:i:s", ($eod_ts + 86399));
		$cash_gratuity_query .= "`$eod_filter` > '$startd' AND `$eod_filter` <= '$endd'";
		$cash_gratuity_query_result = mlavu_query($cash_gratuity_query);
		$cash_gratuity_query_result_assoc_array = mysqli_fetch_assoc($cash_gratuity_query_result);
		$cash_gratuity = $cash_gratuity_query_result_assoc_array['cash_tip'];
		//End of adding stuff by Leif G.

		$show_cash_gratuity = display_price($cash_gratuity, $decimal_places);

		//----Inserted by Brian D.
		$numerical_cash_total = ($cash_paid + $paidin_paidout) * 1;
		$show_cash_total = display_price($numerical_cash_total, $decimal_places);
		//
		$show_cash_total_w_tips = display_price(($numerical_cash_total + $cash_gratuity), $decimal_places);

		$show_card_subtotal = display_price($t_card_subtotal, $decimal_places);
		$show_card_gratuity = display_price($t_card_tips, $decimal_places);
		$show_card_total = display_price($t_card_total, $decimal_places);

		$gc_total = get_eod_total($locationid,'gift_certificate');
		$alt_total = 0;
		$alt_rev_total = 0;
		$show_alt_totals = array();
		$alt_tips = 0;
		$show_alt_tips = array();
		$alt_paid_grand_totals[0] += $gc_total;

		//Show alt totals is built here alternate_payment_types
		//Alternate

		for ($ap = 1; $ap < count($alt_payment_types); $ap++)
		{
			$ptype = $alt_payment_types[$ap];
			if (isset($_GET['dbg']))
			{
				echo "alt payment: " . $ptype[0] . " - " . $ptype[1] . " - " . $ptype[2] . " - " . $this_total . "<br>";
			}

			$this_tip = 0;
			$this_total = get_alt_eod_total($locationid,$ptype[0],"&nbsp;","",$this_tip);
			if ((float)$this_total != 0)
			{
				$alt_total += $this_total;
				$alt_rev_total += ($ptype[2] == 'gift_card' || $ptype[2] == 'loyalty_card') ? $this_total :0.0;//Inserted by Brian D.
				$show_alt_totals[] = array($ptype[1], display_price($this_total, $decimal_places));
				$alt_paid_grand_totals[$ap] += $this_total;
			}

			if ((float)$this_tip != 0)
			{
				$alt_tips += $this_tip;
				$show_alt_tips[] = array($ptype[1], display_price($this_tip, $decimal_places));
			}
		}

		$show_gc_total = display_price($gc_total, $decimal_places);
		$show_alt_total = display_price(($gc_total + $alt_total), $decimal_places);

		$amount_due = $t_total - $t_card_subtotal - $cash_paid - $gc_total - $alt_total;
		if (($amount_due > (0 - ($smallest_money / 2))) && ($amount_due < ($smallest_money / 2)))
		{
			$amount_due = 0;
		}
		$show_amount_due = display_price($amount_due, $decimal_places);

		$show_deposits_used = display_price($t_deposits_used, $decimal_places);
		$show_deposits_collected = display_price($t_deposits_collected, $decimal_places);

		$show_amount_in = display_price($t_total - $t_deposits_used, $decimal_places);
		/*
			Nialls fixed as per alexis' request. dont know if it really needed to be "fixed" buttt... yeah uncomment line below to revert
		*/
		//$show_actual_sales=display_price(str_replace("$", "", $t_total)*1 - str_replace("$", "",$t_deposits_collected)+ $t_deposits_used , $decimal_places);

		//$show_actual_sales = display_price($t_total - $t_deposits_collected + $t_deposits_used  , $decimal_places);

		$ag_totals = get_ag_totals($locationid);
		$show_ag_card = display_price($ag_totals['card'], $decimal_places);
		$show_ag_cash = display_price($ag_totals['cash'], $decimal_places);
		$show_ag_other = display_price($ag_totals['other'], $decimal_places);

		$total_plus_tips = ($cash_paid + $t_card_subtotal + $t_card_tips + $gc_total + $alt_total + $alt_tips);

		// gift/loyalty card sales totals

		$startd = date("Y-m-d H:i:s", $eod_ts);
		$endd = date("Y-m-d H:i:s", ($eod_ts + 86399));

		$gc_orderred_total = 0;
		$lc_orderred_total = 0;
		$gce_orderred_total = 0;
		$alternate_orderred_total = 0;
		{
			$gc_orderred_query = lavu_query("SELECT `id` FROM `forced_modifier_lists` WHERE `title`='*Gift Card'"); // consider basing search on hidden1 field (i.e., gift_card_keyed_in)
			if (mysqli_num_rows($gc_orderred_query))
			{
				$gc_orderred_query = rpt_query("SELECT SUM(`order_contents`.`subtotal`) as `subtotal` FROM `order_contents` LEFT JOIN `orders` ON `order_contents`.`loc_id` = `orders`.`location_id` AND `order_contents`.`order_id` = `orders`.`order_id` LEFT JOIN `menu_items` ON `order_contents`.`item_id` = `menu_items`.`id` LEFT JOIN `menu_categories` ON `menu_items`.`category_id` = `menu_categories`.`id` WHERE `orders`.`$eod_filter` >= '$startd' AND `orders`.`$eod_filter` < '$endd' AND `orders`.`void` = '0' AND `order_contents`.`quantity` > '0' AND (`menu_items`.`forced_modifier_group_id` IN (SELECT CONCAT('f',`id`) as `id` FROM `forced_modifier_lists` WHERE `title`='*Gift Card') OR  (`menu_categories`.`forced_modifier_group_id` IN (SELECT CONCAT('f',`id`) as `id` FROM `forced_modifier_lists` WHERE `title`='*Gift Card') AND 	`menu_items`.`forced_modifier_group_id` = 'C'))");  //Gift Cards

				if (mysqli_num_rows($gc_orderred_query))
				{
					$temp = mysqli_fetch_assoc($gc_orderred_query);
					$gc_orderred_total = $temp['subtotal'];
				}
			}

			$lc_orderred_query = rpt_query("SELECT `id` FROM `forced_modifier_lists` WHERE `title`='*Loyalty Card'");
			if (mysqli_num_rows($lc_orderred_query))
			{
				$lc_orderred_query = rpt_query("SELECT SUM(`order_contents`.`subtotal`) as `subtotal` FROM `order_contents` LEFT JOIN `orders` ON `order_contents`.`loc_id` = `orders`.`location_id` AND `order_contents`.`order_id` = `orders`.`order_id` LEFT JOIN `menu_items` ON `order_contents`.`item_id` = `menu_items`.`id` LEFT JOIN `menu_categories` ON `menu_items`.`category_id` = `menu_categories`.`id` WHERE `orders`.`$eod_filter` >= '$startd' AND `orders`.`$eod_filter` < '$endd' AND `orders`.`void` = '0' AND `order_contents`.`quantity` > '0' AND (`menu_items`.`forced_modifier_group_id` IN (SELECT CONCAT('f',`id`) as `id` FROM `forced_modifier_lists` WHERE `title`='*Loyalty Card') OR (`menu_categories`.`forced_modifier_group_id` IN (SELECT CONCAT('f',`id`) as `id` FROM `forced_modifier_lists` WHERE `title`='*Loyalty Card') AND `menu_items`.`forced_modifier_group_id` = 'C'))");  //Loyalty Cards

				if (mysqli_num_rows($lc_orderred_query))
				{
					$temp = mysqli_fetch_assoc($lc_orderred_query);
					$lc_orderred_total = $temp['subtotal'];
				}
			}

			$gce_orderred_query = rpt_query("SELECT SUM(`order_contents`.`subtotal`) as `subtotal` FROM `order_contents` LEFT JOIN `menu_items` ON `order_contents`.`item_id` = `menu_items`.`id` LEFT JOIN `orders` ON `order_contents`.`loc_id` = `orders`.`location_id` AND `order_contents`.`order_id` = `orders`.`order_id` WHERE `menu_items`.`hidden_value2`='Gift Certificate' AND `orders`.`$eod_filter` >= '$startd' AND `orders`.`$eod_filter` < '$endd' AND `orders`.`void` = '0' AND `order_contents`.`quantity` > '0'");  // Gift Certificate

			$temp = mysqli_fetch_assoc($gce_orderred_query);
			$gce_orderred_total = $temp['subtotal'];
		}

		$not_zero = 0;
		if ($gc_orderred_total != 0) $not_zero++;
		if ($gce_orderred_total != 0) $not_zero++;
		if ($lc_orderred_total != 0) $not_zero++;

		$alternate_orderred_total = ($gc_orderred_total + $gce_orderred_total + $lc_orderred_total);

		$show_gc_orderred_total = display_price($gc_orderred_total, $decimal_places);
		$show_gce_orderred_total = display_price($gce_orderred_total, $decimal_places);
		$show_lc_orderred_total = display_price($lc_orderred_total, $decimal_places);
		$show_alternate_orderred_total = display_price($alternate_orderred_total, $decimal_places);

		//Inserted by Brian D. ---- New field to go into index.php?mode={$section}_{$mode}
		//Total paid = cashTotal + card total + foreach[1] <- these are alternate payment types. + gift certificates
		$show_total_paid = display_price($numerical_cash_total + $t_card_subtotal + $alt_total + $gc_total - $t_deposits_collected + $t_deposits_used, $decimal_places);
		$show_actual_sales = $show_total_paid; //Lp-2198 Replace the $show_actual_sales with $show_total_paid
		$show_total_plus_tips = display_price($total_plus_tips, $decimal_places);
		$show_alt_rev_total = display_price($alt_total + $gc_total, $decimal_places);
		$show_total_revenue = "";
		if ($location_info['gift_revenue_tracked'] == 'when_redeemed')
		{
			$show_total_revenue = display_price($numerical_cash_total + $t_card_subtotal + $alt_total + $gc_total - $alternate_orderred_total, $decimal_places);
		}
		else
		{
			$show_total_revenue = display_price($numerical_cash_total + $t_card_subtotal + $alt_total + $gc_total - ($alt_rev_total + $gc_total), $decimal_places);
		}
		//--------------------------

		$eod_parts = explode("-", date("Y-n-j", $eod_ts));

		if ($use_date != $use_date2)
		{
			$alt_code = "";
			$layer_code = "";
			if (($gc_total + $alt_total) > 0)
			{
				if (count($alt_payment_types) > 0)
				{
					$alt_code = " style='cursor:pointer'";
					$alt_code .= " onmouseover='showAltPayments(\"alt_paid_".$ecount."\", this);'";
					$alt_code .= " onmouseout='hideAltPayments(\"alt_paid_".$ecount."\");'";
					$layer_code .= "<div id='alt_paid_".$ecount."' style='position:absolute; left:0px; top:0px; z-index:".($ecount + 500)."; display:none;'>";
					$layer_code .= "<table cellspacing='0' cellpadding='1'><tr><td align='center' style='background:URL(images/t_gray1.png); background-repeat:repeat;'>";
					$layer_code .= "<table cellspacing='0' cellpadding='1'><tr><td align='center' style='background:URL(images/t_gray1.png); background-repeat:repeat;'>";
					$layer_code .= "<table cellspacing='0' cellpadding='1'><tr><td align='center' style='background:URL(images/t_gray1.png); background-repeat:repeat;'>";
					$layer_code .= "<table cellspacing='0' cellpadding='4' bgcolor='#FFFFFF'>";
					$layer_code .= "<tr><td align='right'>Gift Certificates Redeemed:</td><td align='right'>".$show_gc_total."</td></tr>";
					foreach ($show_alt_totals as $apt)
					{
						$layer_code .= "<tr><td align='right'>".$apt[0].":</td><td align='right'>".$apt[1]."</td></tr>";
					}
					$layer_code .= "</table>";
					$layer_code .= "</td></tr></table>";
					$layer_code .= "</td></tr></table>";
					$layer_code .= "</td></tr></table>";
					$layer_code .= "</div>";
				}
			}

			$mid .= "<tr>";
			switch($date_format['value']){
				case 1:
					$mid .= "<td>".$eod_parts[2] . "/" . $eod_parts[1] . "/" . $eod_parts[0]."</td>";
				break;
				case 2:
					$mid .= "<td>".$eod_parts[1] . "/" . $eod_parts[2] . "/" . $eod_parts[0]."</td>";
				break;
				case 3:
					$mid .= "<td>".$eod_parts[0] . "/" . $eod_parts[1] . "/" . $eod_parts[2]."</td>";
				break;
			}
			$mid .= "<td>".$t_order_count."</td>";
			$mid .= "<td>".(int)$t_guest_count."</td>";
			$mid .= "<td>".$show_subtotal."</td>";
			$mid .= "<td>".$show_combined_discount."</td>";
			$mid .= "<td>".$show_tax."</td>";
			$mid .= "<td>".$show_tax_exempt_amount."</td>";
			$mid .= "<td>".$show_auto_gratuity."</td>";
			$mid .= "<td>".$show_rounding_amount."</td>";
			$mid .= "<td>".$show_total."</td>";
			$mid .= "<td>".$show_cash."</td>";
			$mid .= "<td>".$show_card_subtotal."</td>";
			$mid .= "<td>".$show_card_gratuity."</td>";
			$mid .= "<td>".$show_card_total."</td>";
			$mid .= "<td".$alt_code.">".$show_alt_total.$layer_code."</td>";
			$mid .= "<td>".$show_amount_due."</td>";
			$mid .= "<td>".$show_deposits_collected."</td>";
			$mid .= "<td>".$show_deposits_used."</td>";
			$mid .= "</tr>";

			$mt_order_count		+= $t_order_count;
			$mt_guest_count		+= $t_guest_count;
			//$mt_subtotal modified
			$mt_subtotal += $t_subt;
			$mt_item_discount	+= $t_item_disc;
			$mt_discount		+= $t_disc;
			$mt_itax			+= $t_itax;
			$mt_tax				+= get_eod_total($locationid,'tax');
			$mt_taxexempt		+= $get_total_taxexempt;
			$mt_auto_tip		+= $t_auto_gratuity;
			$mt_rounding_amount += $get_rounding_amount;
			//$mt_total modified
			$mt_total += $item_total;
			$mt_cash			+= $cash_paid;
			$mt_card_subtotal	+= $t_card_subtotal;
			$mt_card_tips		+= $t_card_tips;
			$mt_card_total		+= $t_card_total;
			$mt_gc_total		+= ($gc_total + $alt_total);
			$mt_due				+= $amount_due;

			$mt_deposits_collected	+= $t_deposits_collected;
			$mt_deposits_used		+= $t_deposits_used;

			//$mid .= "<table cellspacing=0 cellpadding=0><td style='border-bottom:solid black 1px; width:400px'>".$eod_parts[1] . "/" . $eod_parts[2] . "/" . $eod_parts[0]."</td></table>";
		}
		else
		{
			$t_html .= "<table style='text-align:left'>";
			$t_html .= "<tr><td>&nbsp;</td></tr>";
			$t_html .= "<tr><td align='right'>".speak("Order Count").":</td><td>" . $t_order_count . "</td></tr>";
			$t_html .= "<tr><td align='right'>".speak("Guest Count").":</td><td>" . (int)$t_guest_count . "</td></tr>";
			$t_html .= "<tr><td>&nbsp;</td></tr>";
			$t_html .= "<tr><td align='right'>".speak("Total Sales").":</td><td>" . $show_total_sales . "</td></tr>";
			$t_html .= "<tr><td align='right'>".speak("Item Discounts").":</td><td>" . $show_item_discount . "</td></tr>";
			$t_html .= "<tr><td align='right'>".speak("Subtotal").":</td><td>" . $show_subtotal . "</td></tr>";
			$t_html .= "<tr><td align='right'>".speak("Check Discounts").":</td><td>" . $show_check_discount . "</td></tr>";
			$t_html .= "<tr><td align='right'>".speak("Subtotal after Check Discounts").":</td><td>" . $show_subtotal_after_discount . "</td></tr>";
			$t_html .= "<tr><td align='right'>".speak("Charge (Gratuity)").":</td><td>" . $show_charge_amount . "</td></tr>";
			$t_html .= "<tr><td align='right'>".speak("Tax").":</td><td>" . $show_tax . "</td></tr>";
			$t_html .= "<tr><td align='right'>".speak("Tax Exemptions (Included Tax)").":</td><td>" . $show_included_tax_exempt_amount . "</td></tr>";
			$t_html .= "<tr><td align='right'>".speak("Rounding Amount").":</td><td>" . $show_rounding_amount . "</td></tr>";
			$t_html .= "<tr><td align='right' style='border-top:solid 1px black'>".speak("Total").":</td><td style='border-top:solid 1px black'>" . $calc_total . "</td></tr>";
			$t_html .= "<tr><td>&nbsp;</td></tr>";
			$t_html .= "<tr><td align='right'>".speak("Cash").":</td><td>" . $show_cash . "</td></tr>";
			$t_html .= "<tr><td align='right'>".speak("Paid In / Paid Out").":</td><td>" . $show_paidin_paidout . "</td></tr>";
			$t_html .= "<tr><td align='right'>".speak("Cash Total").":</td><td>" . $show_cash_total . "</td></tr>";
			$t_html .= "<tr><td align='right'>".speak("Cash Tips").":</td><td>" . $show_cash_gratuity . "</td></tr>";
			$t_html .= "<tr><td align='right'>".speak("Cash Total w/ Tips").":</td><td>" . $show_cash_total_w_tips . "</td></tr>";
			$t_html .= "<tr><td>&nbsp;</td></tr>";
			$t_html .= "<tr><td align='right'>".speak("Card").":</td><td>" . $show_card_subtotal . "</td></tr>";
			$t_html .= "<tr><td align='right'>".speak("Card Tips").":</td><td>" . $show_card_gratuity . "</td></tr>";
			$t_html .= "<tr><td align='right'>".speak("Card Total").":</td><td>" . $show_card_total . "</td></tr>";

			if (count($card_source_breakdown) > 0)
			{
				$hc_base = isset($card_source_breakdown['hc_base'])?$card_source_breakdown['hc_base']:0;
				$hc_tips = isset($card_source_breakdown['hc_tips'])?$card_source_breakdown['hc_tips']:0;
				$qr_base = isset($card_source_breakdown['qr_base'])?$card_source_breakdown['qr_base']:0;
				$qr_tips = isset($card_source_breakdown['qr_tips'])?$card_source_breakdown['qr_tips']:0;
				$t_html .= "<tr><td colspan='2' align='center'>&nbsp;</td></tr>";
				$t_html .= "<tr><td align='right'>".speak("Primary Card Amount").":</td><td>".display_price(($t_card_subtotal - $hc_base - $qr_base), $decimal_places)."</td></tr>";
				$t_html .= "<tr><td align='right'>".speak("Primary Card Tips").":</td><td>".display_price(($t_card_tips - $hc_tips - $qr_tips), $decimal_places)."</td></tr>";

				if (isset($card_source_breakdown['hc_base']))
				{
					$t_html .= "<tr><td align='right'>Hosted Checkout ".speak("Amount").":</td><td>".display_price($hc_base, $decimal_places)."</td></tr>";
					$t_html .= "<tr><td align='right'>Hosted Checkout ".speak("Tips").":</td><td>".display_price($hc_tips, $decimal_places)."</td></tr>";
				}

				if (isset($card_source_breakdown['qr_base']))
				{
					$t_html .= "<tr><td align='right'>Wells Fargo QR ".speak("Amount").":</td><td>".display_price($qr_base, $decimal_places)."</td></tr>";
					$t_html .= "<tr><td align='right'>Wells Fargo QR ".speak("Tips").":</td><td>".display_price($qr_tips, $decimal_places)."</td></tr>";
				}
			}

			if ($gce_orderred_total>0 || $gc_orderred_total>0 || $lc_orderred_total>0)
			{
				$t_html .= "<tr><td>&nbsp;</td></tr>";
			}

			if ($gce_orderred_total > 0)
			{
				$t_html .= "<tr><tr><td align='right'>" . speak("Gift Certificates Ordered") . ":</td><td align='left'>" . $show_gce_orderred_total . "</td></tr>";
			}

			if ($gc_orderred_total > 0)
			{
				$t_html .= "<tr><td align='right'>" . speak("Gift Cards Ordered") . ":</td><td align='left'>" . $show_gc_orderred_total . "</td></tr>";
			}

			if ($lc_orderred_total > 0)
			{
				$t_html .= "<tr><td align='right'>" . speak("Loyalty Cards Ordered") . ":</td><td align='left'>" . $show_lc_orderred_total . "</td></tr>";
			}

			if ($alternate_orderred_total > 0 && $not_zero > 0)
			{
				$t_html .= "<tr><td align='right' style='border-top: 1px solid black;' >" . speak("Alternate Revenue Ordered") . ":</td><td align='left' style='border-top: 1px solid black;' >" . $show_alternate_orderred_total . "</td></tr>";
			}

			$t_html .= "<tr><td>&nbsp;</td></tr>";
			$t_html .= "<tr><td align='right'>".speak("Gift Certificates Redeemed").":</td><td>" . $show_gc_total . "</td></tr>";
			foreach ($show_alt_totals as $this_alt_total)
			{
				$t_html .= "<tr><td align='right'>".$this_alt_total[0]." ".speak("Redeemed").":</td><td>".$this_alt_total[1]."</td></tr>";
			}
			$t_html .= "<tr><td align='right' style='border-top: 1px solid black;'>".speak('Alternate Revenue Redeemed').":</td><td style='border-top: 1px solid black;' >" . $show_alt_rev_total . "</td></tr>";
			// Added by Brian D.------ step three $alt_rev_total
			//--------------------------

			if (count($show_alt_tips) > 0)
			{
				$t_html .= "<tr><td>&nbsp;</td></tr>";
				foreach ($show_alt_tips as $this_alt_tip)
				{
					$t_html .= "<tr><td align='right'>".$this_alt_tip[0]." ".speak("Tips").":</td><td>".$this_alt_tip[1]."</td></tr>";
				}
			}

			$t_html .= "<tr><td>&nbsp;</td></tr>";
			$t_html .= "<tr><td align='right'>".speak("Auto Gratuity").":</td><td>" . $show_auto_gratuity . "</td></tr>";
			$t_html .= "<tr><td align='right'>".speak("AG Cash").":</td><td>" . $show_ag_cash . "</td></tr>";
			$t_html .= "<tr><td align='right'>".speak("AG Card").":</td><td>" . $show_ag_card . "</td></tr>";
			$t_html .= "<tr><td align='right'>".speak("AG Other").":</td><td>" . $show_ag_other . "</td></tr>";
			$t_html .= "<tr><td>&nbsp;</td></tr>";

			// Added by Brian D.------
			//total paid minus gift certificates. Total paid =  cashTotal + card total + foreach[1] <- these are alternate payment types. + gift certificates
			//Total paid goes here step 1.
			$t_html .= "<tr><td align='right'>".speak("Total Paid").":</td><td>" . $show_total_paid . "</td></tr>";
			//step 2 is total revenue
			$t_html .= "<tr><td align='right'>".speak("Total Revenue").":</td><td>" . $show_total_revenue . "</td></tr>";
			//--------------------------

			$t_html .= "<tr><td align='right'>".speak("Total + Tips (Manual)").":</td><td>" . $show_total_plus_tips . "</td></tr>";
			$t_html .= "<tr><td>&nbsp;</td></tr>";
			$t_html .= "<tr><td align='right'>".speak("Unpaid").":</td><td>" . $show_amount_due . "</td></tr>";
			$t_html .= "<tr><td>&nbsp;</td></tr>";
			$t_html .= "<tr><td align='right'>".speak("Tax Exemptions").":</td><td>" . $show_tax_exempt_amount . "</td></tr>";
			$t_html .= "<tr><td>&nbsp;</td></tr>";
			$t_html .= "<tr><td align='right'>".speak("Customer Deposits And Refunds").":</td><td>" . $show_deposits_refunds . "</td></tr>";
			$t_html .= "<tr><td align='right'>".speak("Deposits Collected").":</td><td>" . $show_deposits_collected . "</td></tr>";
			$t_html .= "<tr><td align='right'>".speak("Deposits Used").":</td><td>" . $show_deposits_used . "</td></tr>";
			$t_html .= "<tr><td align='right'>".speak("Actual Sales")." <span style='font-size:10; color:#444444'>(".speak("total")." - ".speak("dep collected")." + ".speak("dep used").")</span>:</td><td>" . $show_actual_sales . "</td></tr>";
			$t_html .= "<tr><td align='right'>".speak("Amount To Take In")." <span style='font-size:10; color:#444444'>(".speak("total")." - ".speak("dep used").")</span>:</td><td>" . $show_total . "</td></tr>";
			$t_html .= "</table>";
			//$t_html .= "<div>*Deposits are calculated using orders that contain items with \"Allow Deposit\" enabled. </div>";

			$mid .= $t_html;

			$printable = "<div style='font-family:Verdana, Arial, Helvetica, sans-serif; font-size:12px;'>";
			$printable .= "<div align='center'><b>".speak("End of Day Report").": ".$location_info['title']."</b></div>";
			$printable .= "<div align='center'><b>".$udate_display."</b></div>";
			$printable .= "<div align='center'><span style='font-family:Verdana, Arial, Helvetica, sans-serif; font-size:10px; color:#333333;'>($start_datetime to $end_datetime)</span></div>";
			$printable .= $t_html;
			$printable .= "<div>&nbsp;</div>";

			$printable .= "<div align='center' style='font-size:10px'>".speak("Generated").": ".(isset($location_info['timezone'])?localize_datetime(date("Y-m-d H:i:s"), $location_info['timezone']):date("Y-m-d H:i:s")." CST")."</div>";
			$printable .= "</div>";

			$_SESSION['printable_EOD'] = $printable;

			if ($location_info['integrateCC']=="1" && !in_array($location_info['gateway'], array("SailPay", "USAePay", "WorldPay")))
			{
				$uncaptured_dates = array();
				if (!in_array($location_info['gateway'], array("Authorize.net", "BluePay", "CyberSource", "Elavon", "Magensa")))
				{
					$check_uncaptured_transactions =rpt_query("SELECT `orders`.`$eod_filter` AS `datetime` FROM `cc_transactions` LEFT JOIN `orders` ON (`orders`.`order_id` = `cc_transactions`.`order_id` AND `orders`.`location_id` = `cc_transactions`.`loc_id`) WHERE `cc_transactions`.`loc_id` = '[1]' AND `cc_transactions`.`pay_type_id` = '2' AND `cc_transactions`.`auth` != '0' AND `got_response` = '1' AND `cc_transactions`.`voided` = '0' AND `orders`.`$eod_filter` > '[2]' AND `orders`.`void` = '0' ORDER BY `orders`.`$eod_filter` DESC", $locationid, date("Y-m-d H:i:s", (time() - 2592000)));
					if (mysqli_num_rows($check_uncaptured_transactions) > 0)
					{
						while ($info = mysqli_fetch_assoc($check_uncaptured_transactions))
						{
							$date_info = get_use_date_end_date($location_info, $info['datetime']);
							if (!in_array($date_info[0], $uncaptured_dates))
							{
								$uncaptured_dates[] = $date_info[0];
							}
						}
					}
				}

				$mid .= "<tr><td>&nbsp;</td></tr>";
				if (count($uncaptured_dates) > 0)
				{
					$mid .= "<tr><td align='center'>";
					$mid .= "<table width='450px' cellpadding='2'>";
					$mid .= "<tr><td align='center'>";
					$mid .= "<table><tr><td><img src='images/warning.png' width='17px' height='17px'></td><td><b>Uncaptured Transactions</b></td></tr></table>";
					$mid .= "</td></tr>";
					$mid .= "<tr><td align='center'>";
					$mid .= "<table width='430px'><tr><td align='center'>";
					if ($lls)
					{
						$mid .= "Please go to the Credit Card Authorizations section of the Management Area and press 'Capture All' before settling the credit card batch.";
					}
					else
					{
						$mid .= "Please go to the detailed End of Day report (all users) for each of the dates listed below and press 'Capture Authorizations' before settling the credit card batch.";
					}
					$mid .= "</td></tr></table>";
					$mid .= "</td></tr>";
					$mid .= "<tr><td align='center'>Uncaptured transactions were found for the following business dates:<br>".implode(", ", $uncaptured_dates)."</td></tr>";
					if ($lls && !llsFileStructureContainsString("/poslavu-local/lib/management/cc_auths.php") && !$trigger_lls_mgmt_update)
					{
						$mid .= "<tr><td>&nbsp;</td></tr>";
						$mid .= "<tr><td align='center'>The system shows that the Credit Card Authorizations section may not be present on your local server. You may initiate an update here: <input type='button' value='Update' onclick='window.location = \"index.php?mode={$section}_{$mode}&llsmgmtccu=1&cbts=".time()."\";''></td><tr>";
						//schedule_msync($data_name, $locationid, "read file structure");
					}
					$mid .= "</table>";
					$mid .= "</td></tr>";
				}
				$mid .= "<tr><td>&nbsp;</td></tr>";
				$mid .= "<tr><td>&nbsp;</td></tr>";
				$mid .= "<tr><td align='center'><input type='button' value='".speak("Settle Credit Card Batch")."' onclick='if (confirm(\"Proceed with credit card transaction batch settlement only if all tip amounts have been entered and applied and open authorizations have been captured. If you are in the process of adjusting tips for more than one day, you must send all tip adjustments for orders created since the last batch settlement before settling the current credit card batch.\")) window.location = \"index.php?mode={$section}_{$mode}&cc_settle=1&cbts=".time()."\";'></td></tr>";
			}
		}

		if ($t_order_count>0 && $use_date==$use_date2)
		{
			$userlist = get_eod_server_list($locationid);
		}

		$eod_hour	= date("H",$eod_ts);
		$eod_mins	= date("i",$eod_ts);
		$eod_day	= date("d",$eod_ts);
		$eod_month	= date("m",$eod_ts);
		$eod_year	= date("Y",$eod_ts);
		$eod_ts		= mktime($eod_hour, $eod_mins, 0, $eod_month, ($eod_day + 1), $eod_year);
		//$eod_ts += 86400;
	}

	if ($use_date != $use_date2)
	{
		$alt_style = "";
		$alt_code = "";
		$layer_code = "";
		if ((count($alt_payment_types) > 0) && ($mt_gc_total > 0))
		{
			$alt_style = " cursor:pointer;";
			$alt_code = " onmouseover='showAltPayments(\"alt_paid_grand_total\", this);'";
			$alt_code .= " onmouseout='hideAltPayments(\"alt_paid_grand_total\");'";
			$layer_code .= "<div id='alt_paid_grand_total' style='position:absolute; left:0px; top:0px; z-index:".($ecount + 501)."; display:none;'>";
			$layer_code .= "<table cellspacing='0' cellpadding='1'><tr><td align='center' style='background:URL(images/t_gray1.png); background-repeat:repeat;'>";
			$layer_code .= "<table cellspacing='0' cellpadding='1'><tr><td align='center' style='background:URL(images/t_gray1.png); background-repeat:repeat;'>";
			$layer_code .= "<table cellspacing='0' cellpadding='1'><tr><td align='center' style='background:URL(images/t_gray1.png); background-repeat:repeat;'>";
			$layer_code .= "<table cellspacing='0' cellpadding='4' bgcolor='#FFFFFF'>";
			for ($ap = 0; $ap < count($alt_payment_types); $ap++)
			{
				$layer_code .= "<tr><td align='right'>".$alt_payment_types[$ap][1].":</td><td align='right'>".display_price($alt_paid_grand_totals[$ap], $decimal_places)."</td></tr>";
			}
			$layer_code .= "</table>";
			$layer_code .= "</td></tr></table>";
			$layer_code .= "</td></tr></table>";
			$layer_code .= "</td></tr></table>";
			$layer_code .= "</div>";
		}

		$mid .= "<tr>";
		$mid .= "<td style='border-top:solid 1px black'>&nbsp;</td>";
		$mid .= "<td style='border-top:solid 1px black'>".$mt_order_count."</td>";
		$mid .= "<td style='border-top:solid 1px black'>".$mt_guest_count."</td>";

		md_tot($mt_subtotal);
		md_tot($mt_item_discount + $mt_discount);
		md_tot($mt_tax + $mt_itax);
		md_tot($mt_taxexempt);
		md_tot($mt_auto_tip);
		md_tot($mt_rounding_amount);
		md_tot($mt_total);
		md_tot($mt_cash);
		md_tot($mt_card_subtotal);
		md_tot($mt_card_tips);
		md_tot($mt_card_total);
		$mid .= "<td style='border-top:solid 1px black;$alt_style'$alt_code>".display_price($mt_gc_total, $decimal_places).$layer_code."</td>";
		md_tot($mt_due);
		md_tot($mt_deposits_collected);
		md_tot($mt_deposits_used);
		$mid .= "</tr>";
		//$mid .= "</table>";
		/****square reconciliation code****/
		if ($eod_filter=="closed") {
			$sdate = $_REQUEST['date']. ' ' . date("H:i:s");
			$edate = $_REQUEST['date2']. ' ' . date("H:i:s");
			$mid .= getReconciliationHtml($sdate, $edate);
		}
	}

	if ($use_date == $use_date2)
	{
		//$mid .= "<input type='button' value='Print to Receipt Printer' onclick='showSection(\"reports\",\"mode=6&location=$location&date=$use_date&print=1\");'>";
		if (isset($_REQUEST['print']))
		{
			$loc = $company_info['company_name'] . " - " . $location_info['title'];
			$dir = "/home/poslavu/public_html/admin/print/queue/" . $loc;

			$str = "receipt:";
			$str .= "---------------------";
			$str .= ":End of Day Report";
			$str .= ":".$udate_display.": ";
			$str .= ":Subtotal[c " . $show_subtotal;
			$str .= ":Tax[c " . $show_tax;
			$str .= ":Auto Gratuity[c " . $show_auto_gratuity;
			$str .= ":Total[c " . $show_total;
			$str .= ":Cash[c " . $show_cash;
			$str .= ":Card Subtotal[c " . $show_card_subtotal;
			$str .= ":Card Gratuity[c " . $show_card_gratuity;
			$str .= ":Card Total[c " . $show_card_total;
			$str .= ":Gift Certificates[c " . $show_gc_total;
			foreach ($show_alt_totals as $this_alt_total)
			{
				$str .= ":".$this_alt_total[0]."[c ".$this_alt_total[1];
			}
			$str .= ":Unpaid[c" . $show_amount_due;
			$str .= ": : : :---------------------";

			if (!is_dir($dir))
			{
				mkdir($dir,0755);
			}
			$fname = $dir . "/".date("YmdHis").rand(10000,99999).".txt";
			if ($fp = fopen($fname,"w"))
			{
				fwrite($fp,$str);
				fclose($fp);
				$success = "1";
			}
		}
		$mid .= "</td></tr>";

		/*$userlist = array();
		$userlist[] = array(speak("All Users"),"all");
		$user_query = rpt_query("SELECT * FROM `users` order by `f_name` asc, `l_name` asc");
		while($user_read = mysqli_fetch_assoc($user_query))
		{
			$userlist[] = array(trim($user_read['f_name'] . " " . $user_read['l_name']), $user_read['id']);
		}

		$mid .= "<tr><td>&nbsp;</td></tr><tr><td style='border-bottom: solid 2px #777777'>".speak("Detailed End of Day Reports")."</td></tr>";
		for($i=0; $i<count($userlist); $i++)
		{
			$mid .= "<tr><td align='left'><a href='index.php?mode={$section}_{$mode}&date=$use_date&user=" . $userlist[$i][1] . "' class='location_link_lg'>" . $userlist[$i][0] . "</a></td></tr>";
		}

		$mid .= "<tr><td>&nbsp;</td></tr>";
		$pr_query = rpt_query("select * from `config` where `type`='printer' and `location`='$locationid' order by `setting` asc");
		while($pr_read = mysqli_fetch_assoc($pr_query))
		{
			$mid .= "<tr><td align='left'><a href='index.php?mode={$section}_{$mode}&date=$use_date&user=register:" . $pr_read['setting'] . "' class='location_link_lg'>Register: " . ucfirst($pr_read['setting']) . "</a></td></tr>";
		}*/

		if ($t_order_count > 0)
		{
			$mid .= "<tr><td align='center'><br><br>";
			$mid .= "<table cellspacing='0' cellpadding='3'>";
			$mid .= "<tr><td colspan='3' style='border-bottom: solid 2px #777777' align='center'>".speak("Detailed End of Day Reports")."</td></tr>";
			$mid .= "<tr>";

			$mid .= "<td align='left'>".speak("Users").": ";
			$mid .= "<select onchange='window.location = \"?mode={$section}_{$mode}&date=$use_date&date2=$use_date2&user=\" + this.value'>";
			$mid .= "<option value='' style='color:#666666'> - - - - - - ".speak("select")." - - - - - - </option>";
			$mid .= "<option value='all'>".speak("All Users")."</option>";
			foreach ($userlist as $this_user)
			{
				$mid .= "<option value='".$this_user[0]."'>".$this_user[1].(empty($this_user[1])?"":" ").$this_user[2]."</option>";
			}
			$mid .= "</select>";
			$mid .= "</td>";
			$mid .= "<td width='15px'>&nbsp;</td>";

			$mid .= "<td align='right'>".speak("Registers").": ";
			$mid .= "<select onchange='window.location = \"?mode={$section}_{$mode}&date=$use_date&date2=$use_date2&user=\" + this.value'>";
			$mid .= "<option value='' style='color:#666666'> - - - - - - ".speak("select")." - - - - - - </option>";

			$pr_query = rpt_query("SELECT `setting`, `value2` FROM `config` WHERE `type` = 'printer' AND `setting` LIKE 'receipt%' AND `location` = '$locationid' ORDER BY `setting` ASC");
			while ($pr_read = mysqli_fetch_assoc($pr_query))
			{
				$mid .= "<option value='register:".$pr_read['setting']."'>".ucfirst($pr_read['value2'])." (".$pr_read['setting'].")"."</option>";
			}
			$mid .= "</select>";
			$mid .= "</td>";

			$mid .= "</tr>";
			$mid .= "</table>";
			$mid .= "</td></tr>";
		}
	}
}

if (!isset($use_date) && isset($_GET['date']))
{
	$use_date = $_GET['date'];
	$use_date2 = $_GET['date'];
}

$display .= "<table><tr><td align='center'>";

/* iFrame for eConduit tip adjust */
$display .= "<div id='eConduitOverlay' class='iFrameOverlay'></div>";
/* iFrame for eConduit tip adjust */

$display .= "<b>" . $title . "</b><br>&nbsp;";
if (isset($use_date))
{
	$display .= "<select onchange='window.location = \"?mode={$section}_{$mode}&date=$use_date&date2=$use_date2&eod_filter=\" + this.value'>";
	$display .= "<option value='closed'".($eod_filter=="closed"?" selected":"").">Report using orders CLOSED on this date</option>";
	$display .= "<option value='opened'".($eod_filter=="opened"?" selected":"").">Report using orders OPENED on this date</option>";
	$display .= "</select>";
}
$display .= "<table cellspacing=0 cellpadding=0>";
//$display .= "<tr><td class='inner_panel_top'></td></tr>";
//$display .= "<tr><td class='inner_panel_mid' align='center'>";
$display .= "<tr><td align='center'>";
if (!$cc_settle)
{
	$display .= "<br><input type='button' onclick='window.open(\"http://admin.poslavu.com/cp/areas/reports/printable_eod.php\", \"EOD\", \"width=700\", \"height=300\")' value='Printable Version'><br>";
}

$display .= "<table>";

$display .= $mid;
$display .= $resp."<tr><td>&nbsp;</td></tr><tr><td>&nbsp;</td></tr>";

$display .= "</table>";
$display .= "</td></tr>";
//$display .= "<tr><td class='inner_panel_bottom'></td></tr>";
$display .= "</table>";

$display .= "
	<div id='customer_signature'style='height: 200px;color: white; position:fixed; left:0px; top:50%; display:none; width: 100%;'>
		<div style = 'position:relative; display: table; margin:0 auto; background-color:gray; height:185px; box-shadow: rgba(0,0,0,0.25) 2px 5px 10px 10px; border-radius: 2px;'>
			<div style='float: right; padding: 2px 5px;'><a onclick='hideSignature();' style='cursor:pointer;'>".speak("Close")."</a></div>
			<div style='overflow: hidden; padding: 2px 5px;'><input id='signature_transaction_info' size='81' READONLY style='border:none; background : URL(images/blank.png); color:white' /></div>
			<img id='signature_image' style='width:480px; height:150px; margin: 0 auto; border-radius: 5px;'></img>
			<!--<table cellspacing='0' cellpadding='0' style='color:white'>
				<tr>
					<td style='display: table; height:221;' align='center' valign='middle'>
						<table cellspacing='0' cellpadding='0'>
							<tr><td align='left'><td align='right'>&nbsp;&nbsp;</td></tr>
							<tr><td colspan='2'></td></tr>
						</table>
					</td>
				</tr>
			</table>-->

		</div>
	</div>";


$display .= "</td></tr>";

if (isset($_GET['sync_local_orders']))
{
	if (isset($start_datetime))
	{
		$first_sync = $start_datetime;
		$last_sync = $end_datetime;
	}
	else
	{
		$first_sync = $_GET['sync_local_orders'] . " 00:00:00";
		$last_sync = $_GET['sync_local_orders'] . " 24:00:00";
	}
	schedule_remote_to_local_sync_if_lls($location_info, $locationid, "run local query", "config", "update orders set lastsync='1000-00-00 00:00:00' where opened>='$first_sync' and opened<='$last_sync'");
	$display .= "<tr><td align='center'>Syncing local orders for " . $first_sync . " to ".$last_sync."</td></tr>";
}
//$display .= "<tr><td align='center'><input type='button' value='".speak("Sync Orders from Local Server")."' onclick='window.location = \"index.php?mode={$section}_{$mode}&sync_local_orders=$use_date\"'></td></tr>";

echo $display;

if ($lls_mgmt_update_alert != "")
{
	echo "<script language='javascript'>alert(\"".str_replace('"', '\"', $lls_mgmt_update_alert)."\");</script>";
}

function getvar($str, $def=FALSE)
{
	return (isset($_REQUEST[$str]))?$_REQUEST[$str]:$def;
}

function display_price($p, $dp)
{
	global $location_info;

	$decimal_char = ".";
	if ($location_info['decimal_char'] != "")
	{
		$decimal_char = $location_info['decimal_char'];
	}

	$thousands_char	= ($decimal_char == ".")?",":".";

	if ($location_info['left_or_right'] == "right")
	{
		return number_format(((float)$p), $dp, $decimal_char, $thousands_char)." ".$location_info['monitary_symbol'];
	}
	else
	{
		return $location_info['monitary_symbol']." ".number_format(((float)$p), $dp, $decimal_char, $thousands_char);
	}
}

function display_datetime($dt)
{
	$dt		= explode(" ", $dt);
	$date	= trim($dt[0]);
	$time	= ($dt[1]);
	$date	= explode("-", $date);
	$time	= explode(":", $time);
	$ts		= mktime((int)$time[0], (int)$time[1], (int)$time[2], (int)$date[1], (int)$date[2], (int)$date[0]);
	$date_setting = "date_format";
	$location_date_format_query = rpt_query( "SELECT `location`, `setting`, `value` FROM `config` WHERE `setting` = '[1]'",  $date_setting);
	if( !$location_date_format_query || !mysqli_num_rows($location_date_format_query)){
		$date_format = array("value" => 2);
	}else{
		$date_format = mysqli_fetch_assoc( $location_date_format_query );
	}
	switch ($date_format['value']){
		case 1:$display_date = date("j/n h:ia",$ts);

		break;
		case 2:$display_date = date("n/j h:ia",$ts);break;
		case 3:$display_date = date("n/j h:ia",$ts);

		break;
	}
	return $display_date;
}

function get_alt_total($loc_id, $order_id, $type_id)
{
	$total_query = rpt_query("SELECT SUM(`amount`) AS `sum_total` FROM `cc_transactions` WHERE `loc_id` = '[1]' AND `order_id` = '[2]' AND `pay_type_id` = '[3]' AND `voided` = '0' AND `action` = 'Sale'", $loc_id, $order_id, $type_id);
	if (mysqli_num_rows($total_query))
	{
		$total_read = mysqli_fetch_assoc($total_query);
		return $total_read['sum_total'];
	}

	return 0;
}

function get_daily_cash_tip_total($loc_id, $server_id, $use_date)
{
	$cash_tips = 0;
	$check_server_id = "";
	if (is_numeric($server_id))
	{
		$check_server_id = " AND `server_id` = '$server_id'";
	}

	$check_todays_cash_tips = rpt_query("SELECT SUM(`value`) AS `value` FROM `cash_data` WHERE `type` = 'server_cash_tips' AND `loc_id` = '[1]'$check_server_id AND `date` = '[2]'", $loc_id, $use_date);
	if (mysqli_num_rows($check_todays_cash_tips) > 0)
	{
		$info = mysqli_fetch_assoc($check_todays_cash_tips);
		$cash_tips = $info['value'];
	}

	return $cash_tips;
}

// check to see if this sale has a matching refund transaction
// took this code from ValidateOrderAndCCTransactionsTables.php
// using it here and in manage_tips.php
function findSaleWithRefund($all_check_ccts, $cc_transaction_rows)
{
	$foundMatch = FALSE;
	// Loop through all of the rows to see if the transaction_id exists.
	foreach ($all_check_ccts as $check_cct)
	{
		// If we find a match, make a note of it and break from the inner for-loop
		if (!empty($cc_transaction_rows['internal_id']) && !empty($check_cct['refund_pnref'])
			&& $cc_transaction_rows['internal_id'] === $check_cct['refund_pnref'])
		{
			$foundMatch = TRUE;
		}
	}

	return $foundMatch;
}
/**
*LP-3718:This is to check if square extension has been enabled for location
*@param
*@return string 0/1
*/
function checkSquareEnabledOrNot(){
	$data_name = admin_info('dataname');
	$query = mlavu_query("select id from restaurants where data_name='".$data_name."' and `modules` like '%extensions.payment.+square%'");
	if(mysqli_num_rows($query)>0){
  		return $enabled = '1';
 	}else{
		return $enabled = '0';
	}
}

function getReconciliationHtml($start_datetime, $end_datetime)
{
	$data_name = admin_info('dataname');
	$dataname = $data_name;
	$html = '';
	$datetime = date("Y-m-d H:i:s");
	/*if (!empty($timezone))
	{
		$datetime = DateTimeForTimeZone($timezone);
	}*/
	$userid = admin_info("loggedin");
	if(isset($_POST['submit_type']) && $_POST['submit_type'] == 'square_request_capture')
	{
		//$currentdate = gmdate('Y-m-d', strtotime($datetime));
		$currentdate = date('Y-m-d', strtotime($start_datetime));
		$existqry = mlavu_query("select `id` from `gateway_sync_request` where gateway = 'square' and data_name = '" .$dataname . "' and date(start_date) = '" . $currentdate . "' AND status = 'pending'");
		if(mysqli_num_rows($existqry) <= 0) {
			$startdate = $start_datetime;
			$enddate = $end_datetime;
			$reqinfo = array('data_name'=>$dataname, 'gateway'=>'square', 'start_date'=>$startdate, 'end_date'=>$enddate, 'user_id'=>$userid, 'status'=>'pending', 'created_date'=>$datetime);
			//	print_r($reqinfo);exit;
			mlavu_query("INSERT INTO `gateway_sync_request` (`data_name`, `gateway`, `start_date`, `end_date`, `user_id`, `status`, `created_date`) values ('[data_name]', '[gateway]', '[start_date]', '[end_date]', '[user_id]', '[status]', '[created_date]')", $reqinfo);
		}
		unset($_POST["reconciliationBtn"]);
		unset($_POST["type"]);
		unset($_POST);
		//header('location:'.$_SERVER['REQUEST_URI']);
	}

	$checksquareEnabled = checkSquareEnabledOrNot(); //returns 1 = enbaled, 0 = not enabled
	if($checksquareEnabled){
		//$mid .= "<table width='100%' align='center'><tr><td><b>Square Reconciliation Details</b></td></tr></table></br>";
		$disabled = '';
		$show = 'none';
		$checkstatusqry = mlavu_query("select `id` from gateway_sync_request WHERE gateway = 'square' AND data_name = '" . $dataname . "' AND status = 'pending'");
		$btnvalue = speak("Start Reconciliation");
		$btnstyle = "width:180px";
		if(mysqli_num_rows($checkstatusqry)>0)
		{
			$disabled = 'disabled';
			$show = 'block';
			$btnvalue = speak("Reconciliation is in progress");
			$btnstyle = "width:250px; color:#808080;";
		}
		$start_datetime = date("Y-m-d H:i:s", strtotime($start_datetime));
		$end_datetime = date("Y-m-d H:i:s", strtotime($end_datetime));
		if (admin_info("access_level") >= 3){
			$html .= "<table>
								<tr>
									<td align='center' valign='middle' height='35px'>
										<form method='POST' action='' id='reconciliateForm'>
											<input type='hidden' name='submit_type' value='square_request_capture'/>
											<input type='hidden' id='start_date' value='".$start_datetime."'/>
											<input type='hidden' id='end_date' value='".$end_datetime."'/>
											<input type='button' value='" . $btnvalue . "' name ='reconciliationBtn' id='reconciliationBtn' style='$btnstyle' onclick='disableBtn()' $disabled>
											</form>
											</td>
											</tr>
											</table>";
			$html.= "<br><br>";
		}

		$lasttwodays =  date('Y-m-d', strtotime("-2 days", strtotime($datetime)));
		$recordsqry = mlavu_query("select `data_name`, `start_date`, `end_date`, `user_id`, `status`, `created_date` FROM `gateway_sync_request` WHERE `gateway` = 'square' AND data_name = '" .$dataname . "' AND status = 'done' AND transaction_id = '' AND date(created_date) >= '".$lasttwodays."' order by id desc");
		//echo "total rows".mysqli_num_rows($recordsqry);
		if(mysqli_num_rows($recordsqry)>0)
		{

			$table = "<h3>Reconciliation Log </h3>
				<table cellspacing=1 cellpadding=2>
					<tbody>
						<tr height='25px' bgcolor='#EEEEEE'>

							<td align='center' width='180px'>".speak("Processed On")."</td>
							<td align='center' width='180px'>".speak("Start Date")."</td>
							<td align='center' width='180px'>".speak("End Date")."</td>
						</tr>";

			while ($rread = mysqli_fetch_assoc($recordsqry))
			{
				$created_date = date("Y-m-d H:i:s", strtotime($rread['created_date']));
				$startDate = date("Y-m-d H:i:s", strtotime($rread['start_date']));
				$endDate = date("Y-m-d H:i:s", strtotime($rread['end_date']));

				$table .= "<tr height='25px'>

							<td align='center' width='180px'>".$created_date."</td>
							<td align='center' width='180px'>".$startDate."</td>
							<td align='center' width='180px'>".$endDate."</td>
							</tr>";

			}

			$table .= "</tbody></table>";


		}

	}
	$script = '<script type="text/javascript">
			function disableBtn()
			{
				$("#reconciliationBtn").attr("disabled","disabled");
				$("#reconciliationBtn").val("Reconciliation is in progress");
				$("#reconciliationBtn").css({"width":"250px", "color":"#808080"});
				$.post( "", { submit_type:"square_request_capture" } ).done(function(  ) {
					//console.log( "Data Loaded: " + data );

				});
				return false;
			}

			</script>';
	$html .= "<tr><td align='center'>".$table.$script."</td></tr>";
	return $html;
}
