<?php
/**
 * Created by PhpStorm.
 * User: hayat
 * Date: 11/7/17
 * Time: 4:39 PM
 */

    function getLocationForReport($dataName, $reportedLocation, $groupLocations, $singleLocation = true) {
        global $reportLocations;
        if ($groupLocations == 'currentLocation' && $dataName == $reportedLocation) {
            $restaurantLocations[$reportedLocation] = $reportedLocation;
        } else if ($singleLocation && $groupLocations != 'currentLocation' && $dataName != $reportedLocation) {
            $restaurantLocations[$reportedLocation] = $reportedLocation;
        }else {
            if (!empty($reportLocations)) {
                if (isset($reportLocations[$groupLocations]) && !empty($reportLocations[$groupLocations])) {
                    $restaurantLocations = $reportLocations[$groupLocations];
                } else {
                    $restaurantLocations[$reportedLocation] = $reportedLocation;
                }
            } else {
                $restaurantLocations[$reportedLocation] = $reportedLocation;
            }
        }
        return $restaurantLocations;
    }

    function getReportCountry($rdb){
        $companyLocationDataQuery = "select * from `[1]`.`config` where `type`='location_config_setting' and `setting` like '%primary_currency_code%'";
        $companyLocationResults = lavu_query($companyLocationDataQuery, $rdb);
        if(mysqli_num_rows($companyLocationResults) >0 ){
            $companyLocationData = mysqli_fetch_assoc($companyLocationResults);
            if(isset($companyLocationData['value'])){
                return $companyLocationData['value2'];
            }
            else if(isset($companyLocationData[0]['value'])){
                return $companyLocationData[0]['value2'];
            }
        }
        return "USA"; //Default to the USA Currency code.
    }

    function convertLocationCurrency ($conversionRateTo, $conversionRateFrom, $amount) {
        if(empty($amount)){
            return 0;
        }

        $intermediateValue = $amount * $conversionRateTo; //get value in terms of common currency
        $finalValue = $intermediateValue / $conversionRateFrom; //convert from common denomination into target currency:
        return round($finalValue, 2);
    }

    /*
    * convertCurrency, takes 2 types of currency and a value.  Converts the value from arg[1] to arg[0]
    */
    function getConversionCurrency($convertFrom, $date){
        global $ConvertTo;
        if($ConvertTo === $convertFrom){
            return array(1, 1);
        }
        $conversionRateFrom = getLocationConversionRate($convertFrom, $date);
        $conversionRateTo = getLocationConversionRate($ConvertTo, $date);
        return array($conversionRateFrom, $conversionRateTo);
    }

    function getLocationConversionRate($currency_code = "", $date = ""){
        global $PrimaryRestaurantID;
        if(empty($currency_code)){
            return 1;
        }

        if($date === ""){
            $date = "00-00-0000";
        }
        $conversionQueryString = "SELECT * FROM `exchange_rates` where `effective_date` = (SELECT max(`effective_date`) FROM `exchange_rates` where `effective_date` <='".$date."' AND `from_currency_id` = '".$currency_code
            ."' AND `primary_restaurantid` = '".$PrimaryRestaurantID."') AND `from_currency_id` = '".$currency_code."' AND `primary_restaurantid` = '".$PrimaryRestaurantID."'";
        $conversionResults = mlavu_query($conversionQueryString);
        if($conversionResults === false) {
            error_log(lavu_dberror());
            return 1;
        }
        if(mysqli_num_rows($conversionResults) > 0) {
            $conversionArray = mysqli_fetch_assoc($conversionResults);
            if (isset($conversionArray['rate'])) {
                return $conversionArray['rate'];
            }
            else if (isset($conversionArray[0]['rate'])) {
                return $conversionArray[0]['rate'];
            }

        }
        else return 1;
    }

    /**
     * Return error message and exit the site upon failure to authenticate.
     */
    function authFailed() { //TODO: Ensure this complies with error code standards
        $response['message'] = "error:1001 - Authentication Failed.<br>Please verify that you are logged in with valid credentials.";
        $status = "failed";
        jsonResponse($response, $status, 401);
        exit();
    }

    /**
     * Return json reponse with status and header code.
     */
    function jsonResponse($response, $status, $code = '') {
        if ($status == "success") {
            setHeader(200);
        } else if ($status == 'failed' && $code != '') {
            setHeader($code);
        } else {
            setHeader(400);
        }
        $dataResponse['status'] = $status;
        $dataResponse['message']['data'] = $response;
        echo json_encode($dataResponse);
        exit();
    }

    function setHeader($code = 200) {
        header("Access-Control-Allow-Origin: *");
        header("Access-Control-Allow-Methods: POST, GET, OPTIONS");
        header("Access-Control-Allow-Headers: Content-Type");
        header("Access-Control-Max-Age: 86400");
        if ($code == 200) {
            header('HTTP/1.0 200 OK');
        } else if ($code == 400) {
            header('HTTP/1.0 400 Bad Request');
        } else if ($code == 401) {
            header('HTTP/1.0 401 Unauthorized');
        }
    }

    function createReportChart($title, $hAxisTitle, $vAxisTitle, $column) {
        $data = "<!DOCTYPE html> 
            <html>
            <head>
            <script type=\"text/javascript\" src=\"https://www.gstatic.com/charts/loader.js\"></script>
            <script type=\"text/javascript\">
            google.charts.load('current', {packages: ['corechart']});
            google.charts.setOnLoadCallback(drawChart);
            function drawChart() {
            var data = google.visualization.arrayToDataTable([
            [chartColumn],
            [chartData]
            ]);
            
            var view = new google.visualization.DataView(data);
            var col = ".$column.";
            if(col == 1) {
                  view.setColumns([0, 1,
                                   { calc: \"stringify\",
                                     sourceColumn: 1,
                                     type: \"string\",
                                     role: \"annotation\" }
                                   ]);
            } else if (col == 2) {
                view.setColumns([0, 1,
                                   { calc: \"stringify\",
                                     sourceColumn: 1,
                                     type: \"string\",
                                     role: \"annotation\" },
                                   2,
                                   { calc: \"stringify\",
                                     sourceColumn: 2,
                                     type: \"string\",
                                     role: \"annotation\" },
                                   ]);
            }
            var options = {
            title: '".$title."',
            seriesType: 'bars',
            vAxis: {title: '".$vAxisTitle."',  titleTextStyle: {color: 'red'}},
            hAxis: {title: '".$hAxisTitle."',  titleTextStyle: {color: 'blue'}},
            };
            
            var chart = new google.visualization.ComboChart(document.getElementById('[chartDiv]'));
            chart.draw(view, options);
            }
            </script>
            </head>
            <body>
            <div id=\"[chartDiv]\" style=\"width: 900px; height: 500px;\"></div>
            </body>
            </html>";

        return $data;
    }

    function createRevenueLineChart($width, $height) {
        $data = "<!DOCTYPE html> 
                <html>
                <head>
                <script type=\"text/javascript\" src=\"https://www.gstatic.com/charts/loader.js\"></script>
                <script type=\"text/javascript\">
                google.charts.load('current', {packages: ['corechart']});
                google.charts.setOnLoadCallback(drawChart);
                function drawChart() {
                var data = google.visualization.arrayToDataTable([
                [chartColumn],
                [chartData]
                ]);
               var width = ".$width.";
               var height = ".$height.";
                var options = {
                    legend: 'none',
                    'width':width,
                    'height':height,
                    axisFontSize : 0,
                    pointSize: 5,
                    hAxis: {
                      baselineColor: 'none',
                      ticks: [],
                      textColor: '#ffffff'
                    },
                    vAxis: {
                      baselineColor: 'none',
                      ticks: [],
                      textColor: '#ffffff'
                    }
                };

                
                var chart = new google.visualization.LineChart(document.getElementById('[chartDiv]'));
                chart.draw(data, options);
                }
                </script>
                </head>
                <body>
                <div id=\"[chartDiv]\" style=\"width: ".$width."px; height: ".$height."px;\"></div>
                </body>
                </html>";

        return $data;
    }

    function mail_attachment($filename, $path, $mailto, $from_mail, $from_name, $replyto, $subject, $message, $embededMessage) {
        global $emailImageBaseURL, $abiDashBoardURL;
        $file = $path.$filename;
        $file_size = filesize($file);
        $handle = fopen($file, "r");
        $content = fread($handle, $file_size);
        fclose($handle);
        $content = chunk_split(base64_encode($content));
        if ($embededMessage == '') {
            $embededMessage = '';
            $embededMessage .= '<html><body>';
            $embededMessage .= '<h1>' . $message . '</h1>';
            $embededMessage .= '<div><a href="' . $abiDashBoardURL . '"><img src="' . $emailImageBaseURL . $filename . '?t=' . time() . '" alt="Sales Chart" /></a></div>';
            $embededMessage .= "<p>Please find attachement</p>";
            $embededMessage .= '</body></html>';
        }

        /* Set the email header */
        // Generate a boundary
        $boundary = md5(uniqid(time()));

        // Email header
        $header = "From: ".$from_name." <".$from_mail.">".PHP_EOL;
        $header .= "Reply-To: ".$replyto.PHP_EOL;
        $header .= "MIME-Version: 1.0".PHP_EOL;

        // Multipart wraps the Email Content and Attachment
        $header .= "Content-Type: multipart/mixed; boundary=\"".$boundary."\"".PHP_EOL;
        $header .= "This is a multi-part message in MIME format.".PHP_EOL;
        $header .= "--".$boundary.PHP_EOL;

        // Email content
        // Content-type can be text/plain or text/html
        $header .= "Content-type:text/html; charset=iso-8859-1".PHP_EOL;
        $header .= "Content-Transfer-Encoding: 7bit".PHP_EOL.PHP_EOL;
        $header .= "$embededMessage".PHP_EOL;
        $header .= "--".$boundary.PHP_EOL;

        // Attachment
        // Edit content type for different file extensions
       $header .= "Content-Type: application/xml; name=\"".$filename."\"".PHP_EOL;
        $header .= "Content-Transfer-Encoding: base64".PHP_EOL;
        $header .= "Content-Disposition: attachment; filename=\"".$filename."\"".PHP_EOL.PHP_EOL;
        $header .= $content.PHP_EOL;
        $header .= "--".$boundary."--";
        $mailResponse = mail($mailto, $subject, "", $header);
        error_log("Mail Sent to " . $mailto. "Subject : " . $subject . " Status : ". $mailResponse);
    }

    function convertHtmlToImage($htmlFile, $imgFile, $width = '', $height = '') {
        $eWidth = ($width) ? "--width $width" : '';
        $eHeight = ($height) ? "--height $height" : '';
        $cmd = "xvfb-run -a /usr/bin/wkhtmltoimage $eWidth $eHeight $htmlFile $imgFile";
        $response = exec($cmd);
        return $response;
    }

    function convertHtmlToPdf($htmlFile, $imgFile) {
        $cmd = "xvfb-run -a /usr/bin/wkhtmltopdf $htmlFile $imgFile";
        $response = exec($cmd);
        return $response;
    }

    function checkDir($dir = '') {
        global $baseEmailImageDir;
        $newDir = $baseEmailImageDir.$dir;
        if (!is_dir($newDir)) {
            mkdir($newDir, 0755, true);
        }
        return $newDir;
    }

    function calculatePlanNumbers($planNumbers, $month, $year) {
        global $startDate, $endDate;
        $startDateStr = strtotime($startDate);
        $endDateStr = strtotime($endDate);
        $startMonth = date('n', $startDateStr);
        $startYear = date('Y', $startDateStr);
        $endMonth = date('n', $endDateStr);
        $endYear = date('Y', $endDateStr);
        $startMonthDay = date('j', $startDateStr);
        $startTotalDay = date('t', $startDateStr);
        $endMonthDay = date('j', $endDateStr);
        $endTotalDay = date('t', $endDateStr);
        if ($startMonth == $endMonth && $startYear == $endYear) {
            if ($startMonthDay > 1 && $endMonthDay < $startTotalDay) {
                $planNumbers = round(($planNumbers / $startTotalDay) * ($endMonthDay - $startMonthDay  + 1));
            } else if ($startMonthDay == 1 && $endMonthDay < $startTotalDay && $startMonth == $endMonth) {
                $planNumbers = round(($planNumbers / $startTotalDay) * $endMonthDay, 2);
            }
        } else if ($startMonth != $endMonth && $month == $startMonth && $year == $startYear) {
            if ($startMonthDay > 1 && $startMonthDay < $startTotalDay) {
                $planNumbers = round(($planNumbers / $startTotalDay) * ($startTotalDay - $startMonthDay  + 1));
            }
        } else if ($month == $endMonth && $year == $endYear) {
            $planNumbers =  ($endMonthDay < $endTotalDay) ? round(($planNumbers / $endTotalDay) * $endMonthDay, 2) : $planNumbers;
        }
        return $planNumbers;
    }

    function getPlannedData($chain_id) {
        $planDataQuery = "select * from `budget_plan_data` where `chain_id`='[1]' and `_deleted` = 0";
        $planData = mlavu_query($planDataQuery, $chain_id);
        $staticPlanRecord = array();
        $staticBeerVolume = array();
        if(mysqli_num_rows($planData) >0 ){
            while ($data_read = mysqli_fetch_assoc($planData)) {
                if ($data_read['category'] == 'sale') {
                    $staticPlanRecord[$data_read['region']] = json_decode($data_read['data'], true);
                    $reportRegions[$data_read['region']] = $data_read['region'];
                }
                if ($data_read['category'] == 'beer') {
                    $staticBeerVolume[$data_read['region']] = json_decode($data_read['data'], true);
                    $reportRegions[$data_read['region']] = $data_read['region'];
                }
            }
        }

        return array($reportRegions, $staticPlanRecord, $staticBeerVolume);
    }