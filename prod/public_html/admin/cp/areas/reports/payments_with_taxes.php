<?php
	if(!$in_lavu) {
		exit();
	}
		
	require_once(dirname(dirname(dirname(dirname(__FILE__))))."/sa_cp/show_report.php");
	$date_info = show_report(admin_info("database"),"poslavu_MAIN_db","unified","index.php?mode={$section}_{$mode}","select_date");
	$date_start = $date_info['date_start'];
	$date_end = $date_info['date_end'];
	$time_start = $date_info['time_start'];
	$time_end = $date_info['time_end'];
	$date_reports_by = $date_info['date_reports_by'];
	$start_datetime = $date_start . " " . $time_start;
	$end_datetime = $date_end . " " . $time_end;

	$location_read;
	if($locationid)
	{
		$location_query = lavu_query("select * from `locations` where `id`='[1]'",$locationid);
		if(mysqli_num_rows($location_query))
		{
			$location_read = mysqli_fetch_assoc($location_query);
		}
	}
	
	echo "<br /><br />";
	
	/**
	 * Fix empty split_check_details
	 */
	{
		$order_id_query = "
		SELECT 
			`split_check_details`.`order_id`
	
		FROM `split_check_details` 

		LEFT JOIN `orders` ON `split_check_details`.`order_id` = `orders`.`order_id`

		WHERE 
			`orders`.`" . $date_reports_by . "` >= '" . $start_datetime . "' AND
			`orders`.`" . $date_reports_by . "` <= '" . $end_datetime . "' AND
			`orders`.`location_id` = '" . $locationid . "' AND
			`split_check_details`.`tax` = '[tax]'
			
	
		GROUP BY `order_id`
		";
		
		$order_id_result = lavu_query($order_id_query);
		if(mysqli_num_rows($order_id_result))
		{
			$orders = "";
			while($order = mysqli_fetch_assoc($order_id_result))
			{
				if($orders != "")
					$orders .= ",";
					
				$orders .= "'" . $order['order_id'] . "'";
			}
			$empty_tax_information = "
			SELECT
				`split_check_details`.`order_id`,
				`split_check_details`.`check`,
				`split_check_details`.`tax`,
				`orders`.`total` as `order_total`,
				`orders`.`tax` as `order_tax`,
				SUM(`cc_transactions`.`total_collected`) as `paid`
				
			
			FROM `split_check_details`
			
			LEFT JOIN `orders` ON `split_check_details`.`order_id` = `orders`.`order_id`
			LEFT JOIN `cc_transactions` ON `cc_transactions`.`order_id` = `split_check_details`.`order_id` AND `cc_transactions`.`check`=`split_check_details`.`check`
			
			WHERE
				`orders`.`order_id` IN
					(" . $orders . ") AND
				`cc_transactions`.`voided` != '1' AND
				`cc_transactions`.`action` != 'Void'
			
	GROUP BY
		`order_id` asc, `check` asc
				";
				
			//echo $empty_tax_information . "<br /><br />";
			
			$empty_tax_result = lavu_query($empty_tax_information);
			if(mysqli_num_rows($empty_tax_result))
			{
				$records_updated = 0;
				while($detail = mysqli_fetch_assoc($empty_tax_result))
				{
					$update_query = "UPDATE 
						`split_check_details` 
					SET 
						`tax`='[tax]', 
						`check_total`='[check_total]'
						
					WHERE
						`order_id` = '[order_id]' AND
						`check` = '[check]' AND
						`loc_id` = '[loc_id]'
					";
					
					$update_vals = array();
					$update_vals['loc_id'] = $locationid;
					$update_vals['check'] = $detail['check'];
					$update_vals['order_id'] = $detail['order_id'];
					$update_vals['check_total'] = $detail['paid'];
					
					$tax = $detail['order_tax'] * (round($detail['paid'],2)/round($detail['order_total']));
					$update_vals['tax'] = round($tax,2);
					
					
					foreach($update_vals as $key => $value)
					{
						$update_query = str_ireplace('[' . $key . ']', $value, $update_query);
					}
					
					//echo $update_query;
					//break;
					
					lavu_query($update_query, $update_vals);
					$records_updated += ConnectionHub::getConn('rest')->affectedRows();
					
				}
				
				mysqli_free_result($empty_tax_result);
				if($records_updated)
					echo "Entries Update: " . $records_updated . "<br />";
			}
		}
	}
	
	
	/**
	 * Main Query
	 */
	{
		$payment_query = "
		SELECT 
			`cc_transactions`.`order_id` as `order_id`,
			`cc_transactions`.`check` as `Check Number`,
			`cc_transactions`.`datetime` as `Payment Time`,
			`cc_transactions`.`total_collected` as `Sales Collected`,
			`cc_transactions`.`total_collected` as `Collected`,
			`cc_transactions`.`tip_amount` as `Tips`,
			`orders`.`total` as `order_total`,
			`split_check_details`.`tax` as `check_tax`,
			`cc_transactions`.`total_collected` + `cc_transactions`.`tip_amount` as `Total`, 
			`orders`.`tax` as `order_tax`,
			IF(`cc_transactions`.`pay_type`='Card',`cc_transactions`.`amount` + `cc_transactions`.`tip_amount`,0) as `Total Card`,
			IF(`orders`.`no_of_checks` > '1',`split_check_details`.`tax`,`orders`.`tax`) as `Tax`,
			`cc_transactions`.`card_type` as `Card Type`, 
			IF(`cc_transactions`.`order_id`='Paid Out','Paid Out',
			IF(`cc_transactions`.`order_id`='Paid In','Paid In',`cc_transactions`.`action`)) as `Action`,
			`cc_transactions`.`pay_type` as `Payment Type`
	
			
		FROM `cc_transactions`
		
		LEFT JOIN `orders` ON `cc_transactions`.`order_id` = `orders`.`order_id`
		LEFT JOIN `split_check_details` ON `split_check_details`.`order_id` = `orders`.`order_id` AND `split_check_details`.`check` = `cc_transactions`.`check`
	
		WHERE 
			`cc_transactions`.`voided` != '1' AND
			`orders`.`void` = '0' AND
			`cc_transactions`.`action` != '' AND
			`cc_transactions`.`action` != 'Void' AND 
			`orders`.`" . $date_reports_by . "` >= '" . $start_datetime . "' AND
			`orders`.`" . $date_reports_by . "` <= '" . $end_datetime . "' AND
			`cc_transactions`.`loc_id` = '" . $locationid . "'
			
		ORDER BY 
			`order_id` asc, `Check Number` desc, `Payment Time` desc
		";
		//echo $payment_query . "<br /><br />";
		$payment_result = lavu_query($payment_query);
		if(mysqli_num_rows($payment_result))
		{
			$result = array();
			
			$last_orderid = "";
			$last_check_num = "";
			$remaining_tax = "";
			
			$tax = 0;
			$check_tax = 0;
			$order_tax_single = 0;
			$check_tax_single = 0;
			$order_id_list = Array();
			while($payment = mysqli_fetch_assoc($payment_result))
			{
			
				if($payment['order_id'] != $last_orderid)
				{
					if(number_format($order_tax_single) != number_format($check_tax_single) && $last_orderid != "")
					{
						$order_id_list[] = Array($last_orderid, $order_tax_single, $check_tax_single);
					}
					
				}
			
				if(!isset($payment['Action']))
					$result[$payment['Action']] = Array();
				
				if(!isset($result[$payment['Action']][$payment['Payment Type']]))
					$result[$payment['Action']][$payment['Payment Type']] = Array();
					
				$mult = 1;
				if($payment['Action'] == 'Refund' || $payment['Action'] == 'Paid Out')
					$mult = -1;
					
				$paid = $payment['Action'] == 'Paid Out' || $payment['Action'] == 'Paid In';
					
				$result[$payment['Action']][$payment['Payment Type']]['Sales Collected'] += $paid ? 0.0 : $mult * $payment['Sales Collected'];
				$result[$payment['Action']][$payment['Payment Type']]['Collected'] += $mult * $payment['Collected'];
				$result[$payment['Action']][$payment['Payment Type']]['Tips'] += $mult * $payment['Tips'];
				$result[$payment['Action']][$payment['Payment Type']]['Total'] += $mult * $payment['Total'];
				$result[$payment['Action']][$payment['Payment Type']]['Total Card'] += $mult * $payment['Total Card'];
				
				if(!($last_orderid == $payment['order_id'] && $last_check_num == $payment['Check Number']))
				{
					$remaining_tax = $payment['Tax'];
				}
				
				{
					$applied_tax = min($payment['Sales Collected'], $remaining_tax);
					if($payment['Action'] != 'Refund')
					{
						$remaining_tax -= $applied_tax;
						$result[$payment['Action']][$payment['Payment Type']]['Tax'] += $applied_tax;
					}
					
				}
				
				if($payment['order_id'] != $last_orderid)
				{
					$tax += $payment['order_tax'];
					$order_tax_single = $payment['order_tax'];
					$check_tax_single = 0;
				}
					
				if($payment['order_id'] != $last_orderid || $last_check_num != $payment['Check Number'])
				{
					$check_tax += $payment['check_tax'];
					$check_tax_single += $payment['check_tax'];
				}
				
				$last_orderid = $payment['order_id'];
				$last_check_num = $payment['Check Number'];
			}
			
			//echo "<pre style='text-align: left;'>" . print_r($result, true) . "</pre>";
			
			echo "
				<table style='width: 100%;'>
					<thead>
						<tr style='text-align: center; height: 1.5em;'>
							<td><b>Sales Collected</b></td>
							<td><b>Collected</b></td>
							<td><b>Tips</b></td>
							<td><b>Total</b></td>
							<td><b>Total Card</b></td>
							<td><b>Tax</b></td>
							<td><b>Payment Type</b></td>
							<td><b>Action</b></td>
						</tr>
					</thead>
					<tbody>";
					
			
			
			$tot_sales_collected;
			$tot_collected;
			$tot_tips;
			$tot_total;
			$tot_total_card;
			$tot_tax;
			
			foreach($result as $action => $a_val)
			{
				foreach($a_val as $pay_type => $p_val)
				{
						echo "
							<tr>
								<td style='border-bottom: 1px solid black; padding: 5px;'>" . display_money($p_val['Sales Collected'],$location_read) . "</td>
								<td style='border-bottom: 1px solid black; padding: 5px; text-align: right;'>" . display_money($p_val['Collected'],$location_read) . "</td>
								<td style='border-bottom: 1px solid black; padding: 5px; text-align: right;'>" . display_money($p_val['Tips'],$location_read) . "</td>
								<td style='border-bottom: 1px solid black; padding: 5px;'>" . display_money($p_val['Total'],$location_read) . "</td>
								<td style='border-bottom: 1px solid black; padding: 5px;'>" . display_money($p_val['Total Card'],$location_read) . "</td>
								<td style='border-bottom: 1px solid black; padding: 5px;'>" . display_money($p_val['Tax'],$location_read) . "</td>
								<td style='border-bottom: 1px solid black; padding: 5px;'>" . $pay_type . "</td>
								<td style='border-bottom: 1px solid black; padding: 5px;'>" . $action . "</td>
							</tr>
							";
							
						$tot_sales_collected += $p_val['Sales Collected'];
						$tot_collected += $p_val['Collected'];
						$tot_tips += $p_val['Tips'];
						$tot_total += $p_val['Total'];
						$tot_total_card += $p_val['Total Card'];
						$tot_tax += $p_val['Tax'];
				}
			}
			echo "
							<tr>
								<td style='padding: 5px;'><b>" . display_money($tot_sales_collected,$location_read) . "</b></td>
								<td style='padding: 5px; text-align: right;'><b>" . display_money($tot_collected,$location_read) . "</b></td>
								<td style='padding: 5px; text-align: right;'><b>" . display_money($tot_tips,$location_read) . "</b></td>
								<td style='padding: 5px; '><b>" . display_money($tot_total,$location_read) . "</b></td>
								<td style='padding: 5px; '><b>" . display_money($tot_total_card,$location_read) . "</b></td>
								<td style='padding: 5px;'><b>" . display_money($tot_tax,$location_read) . "</b></td>
								<td></td>
								<td></td>
							</tr>
							";
							
			echo "
					</tbody>
				</table>";
			//echo $tax . "<br />";
			//echo $check_tax . "<br />";
			//echo "<pre>" . print_r($order_id_list, true) . "</pre>";
		}
		
		//ACTION
		//PAYMENT TYPE
		//CARD TYPE
		
		//Sales Collected
		//Collected
		//Tips
		//Total
		//Total Card
		//Tax
	}
?>
