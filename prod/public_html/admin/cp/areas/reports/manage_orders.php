<?php
	if($in_lavu)
	{
		$goto_anchor = "";

		function field_title($str)
		{
			$str = explode(":",$str);
			return ucfirst(str_replace("_"," ",$str[0]));
		}

		function field_name($str)
		{
			$str = explode(":",$str);
			return $str[0];
		}

		function field_type($str)
		{
			$str = explode(":",$str);
			if(count($str) > 1)
				return $str[1];
			else
				return "text";
		}

		function display_price($p, $use_sym)
		{
			global $location_info;

			$dp = $location_info['disable_decimal'];

			$decimal_char = ".";
			if ($location_info['decimal_char'] != "") {
				$decimal_char = $location_info['decimal_char'];
			}
			$thousands_char	= ($decimal_char == ".")?",":".";

			/* if (!$use_sym) return number_format($p, $dp, $decimal_char, ",");
			else if ($location_info['left_or_right'] == "right") return number_format($p, $dp, $decimal_char, ",")." ".$location_info['monitary_symbol'];
			else return $location_info['monitary_symbol']." ".number_format($p, $dp, $decimal_char, ","); */
			if (!$use_sym) return number_format($p, $dp, $decimal_char, $thousands_char);
			else if ($location_info['left_or_right'] == "right") return number_format($p, $dp, $decimal_char, $thousands_char)." ".$location_info['monitary_symbol'];
			else return $location_info['monitary_symbol']." ".number_format($p, $dp, $decimal_char, $thousands_char);
		}

		$dp = $location_info['disable_decimal'];

		$alt_pay_types = false;
		$get_alt_payment_types = rpt_query("SELECT * FROM `payment_types` WHERE `loc_id` = '[1]' AND `id` > '3' ORDER BY `_order` ASC, `type` ASC", $location_info['id']);
		if (@mysqli_num_rows($get_alt_payment_types) > 0) $alt_pay_types = true;

		$fields = array("order_id:order_id","opened","total:price","subtotal:price","itax:price","discount:price","tax:price","gratuity:price","cash_paid:cash","card_paid:price","gift_certificate:price","due:due","server");
		$split_fields = array("check_total:price","subtotal:price","itax:price","discount:price","tax:price","gratuity:price","cash:cash_split","card:price","gift_certificate:price","due:due_split");
		$adjust_type_list = array("cash_paid","card_paid","gift_certificate");//,"discount","gift_certificate");

		echo "<br><br>";
		require_once dirname(dirname(dirname(dirname(__FILE__)))).'/sa_cp/show_report.php';
		$date_info = show_report(admin_info("database"),"poslavu_MAIN_db","unified","index.php?mode={$section}_{$mode}","select_date");
		$date_start = $date_info['date_start'];
		$date_end = $date_info['date_end'];
		$time_start = $date_info['time_start'];
		$time_end = $date_info['time_end'];

		$filter_orders = rs_getvar("filter_orders","open");
		echo "<select name='filter_orders' onchange='window.location = \"index.php?mode={$section}_{$mode}&reportid=select_date&filter_orders=\" + this.value'>";
		echo "<option value='open'".($filter_orders=="open"?" selected":"").">Open Orders</option>";
		echo "<option value='opened on'".($filter_orders=="opened on"?" selected":"").">Orders Opened On Date</option>";
		echo "<option value='closed'".($filter_orders=="closed"?" selected":"").">Closed Orders</option>";
		echo "</select>";

		echo "<br><br>";
		echo "<script language='javascript'>";
		echo "last_order_info_id = 0; ";
		echo "</script>";

		echo "<form name='manage_orders' method='post' action='index.php?mode={$section}_{$mode}'>";
		echo "<input type='hidden' name='manage_action' value=''>";
		echo "<input type='hidden' name='order_id' value=''>";
		echo "<input type='hidden' name='adjust_type' value=''>";
		echo "<input type='hidden' name='adjust_amount' value=''>";
		echo "<input type='hidden' name='adjust_check_number' value=''>";
		echo "<table cellspacing=0 cellpadding=2>";
		echo "<tr>";
		$colspan = count($fields) * 2 + 1;
		for($i=0; $i<count($fields); $i++)
		{
			$title = field_title($fields[$i]);
			if (substr($fields[$i], 0, 16)=="gift_certificate" && $alt_pay_types) $title = "Other";
			if (substr($fields[$i], 0, 4) == "itax") $title = "Included Tax";
			echo "<td style='border-bottom:solid 1px black'>$title</td><td style='border-bottom:solid 1px black'>&nbsp;</td>";
		}
		function ascii_code($str)
		{
			$str2 = "";
			for($i=0; $i<strlen($str); $i++)
			{
				if($str2!="") $str2 .= ",";
				$str2 .= ord(substr($str,$i,1));
			}
			return $str2;
		}
		function output_order_cols($order_read,$fields,$ommit="")
		{
			global $dp;

			$date_setting = "date_format";
			$location_date_format_query = rpt_query( "SELECT `location`, `setting`, `value` FROM `config` WHERE `setting` = '[1]'",  $date_setting);
			if( !$location_date_format_query || !mysqli_num_rows($location_date_format_query)){
				$date_format = array("value" => 2);
			}else
				$date_format = mysqli_fetch_assoc( $location_date_format_query );

			$ommit = explode(",",$ommit);

			$str = "";
			for($i=0; $i<count($fields); $i++)
			{
				$field_name = field_name($fields[$i]);
				$field_type = field_type($fields[$i]);

				$show_col = true;
				for($n=0; $n<count($ommit); $n++)
				{
					if(trim($ommit[$n])==$field_name)
						$show_col = false;
				}

				if($show_col)
				{
					if(isset($order_read['cash_applied']) && (string)$order_read['cash_applied']!="")
					{
						$cash_applied = $order_read['cash_applied'] * 1;
					}
					else if(isset($order_read['cash_paid']) && isset($order_read['change_amount']))
					{
						$cash_applied = $order_read['cash_paid'] - $order_read['change_amount'];
					}
					else if(isset($order_read['cash']) && isset($order_read['change']))
					{
						$cash_applied = $order_read['cash'] - $order_read['change'];
					}

					if($field_type=="cash")
					{
						$value = $cash_applied;
					}
					else if($field_type=="cash_split")
					{
						$value = $cash_applied;//($order_read['cash'] - $order_read['change']);
					}
					else if($field_type=="due")
					{
						$value = number_format($order_read['total'] - $order_read['gift_certificate'] - ($cash_applied) - $order_read['card_paid'] - $order_read['alt_paid'] /*+ $order_read['refunded'] + $order_read['refunded_cc'] + $order_read['refunded_gc'] + $order_read['alt_refunded']*/,$dp,".","");
					}
					else if($field_type=="due_split")
					{
						$value = number_format($order_read['check_total'] - $order_read['gift_certificate'] - ($cash_applied) - $order_read['card'] - $order_read['alt_paid'] /*+ $order_read['refunded'] + $order_read['refund_cc'] + $order_read['refund_gc'] + $order_read['alt_refunded']*/,$dp,".","");
					}
					else
					{
						$value = $order_read[$field_name];
					}

					if ($field_name=="subtotal") $value = ($order_read['subtotal'] - $order_read['itax']);

					//if($field_name=='tax'){
						//$value = $order_read['tax']; // + $order_read['itax'];
					//}

					if ($field_name=="gift_certificate") $value = ($order_read['gift_certificate'] + $order_read['alt_paid']);

					if($field_type=="price" || $field_type=="due" || $field_type=="due_split" || $field_type=="cash" || $field_type=="cash_split")
						$value = display_price(($value * 1), true);
					//else if($field_type=="order_id")
						//$value = str_pad($value, 8, "0", STR_PAD_LEFT);
					$str .= "<td";
					if($field_type=="order_id")
					{
						$str .= " style='color:#000088' onclick='showWhiteBox(\"".speak("Order ID").": ".$order_read['order_id']."<br><iframe src=\\\"index.php?show_page=reports/order_details&order_id=".$order_read['order_id']."\\\" width=\\\"100%\\\" height=\\\"100%\\\" frameborder=\\\"0\\\"></iframe>\")'";
					}
					if($field_name=="opened")
					{
						$split_date = explode(" ", $value);
						$change_format = explode("-", $split_date[0]);
						switch ($date_format['value']){
							case 1:$value = $change_format[2]."-".$change_format[1]."-".$change_format[0]." ".$split_date[1];
							break;
							case 2:$value = $change_format[1]."-".$change_format[2]."-".$change_format[0]." ".$split_date[1];
							break;
							case 3:$value = $change_format[0]."-".$change_format[1]."-".$change_format[2]." ".$split_date[1];
							break;
						}
					}
					$str .= ">" . $value . "</td><td width='12'>&nbsp;</td>";
				}
			}
			return $str;
		}
		echo "<td style='border-bottom:solid 1px black'>&nbsp;</td>";
		echo "</tr>";

		$filter_query_code = "`order_id` NOT LIKE 'Paid%' AND `order_id` NOT LIKE '777%' AND ";
		$dt_field = "opened";

		if($filter_orders=="open")
		{
			$filter_query_code .= "(`closed`='0000-00-00 00:00:00' OR (`reopened_datetime` != '' AND `reclosed_datetime` = '')) and ";
			$date_start = "1900-00-00";
			$time_start = "00:00:00";
			$date_end = "9000-00-00";
			$time_end = "24:00:00";
		}
		else if($filter_orders=="opened on")
		{

		}
		else if($filter_orders=="closed")
		{
			$dt_field = "closed";
			$filter_query_code .= "`closed`!='0000-00-00 00:00:00' AND ((`reopened_datetime` = '' AND `reclosed_datetime` = '') OR (`reopened_datetime` != '' AND `reclosed_datetime` != '')) and ";
		}

		if(isset($_POST['manage_action']))
		{
			$vars = array();
			$vars['action'] = "";
			$vars['data'] = "";
			$vars['order_id'] = 0;
			$vars['time'] = date("Y-m-d H:i:s");
			$vars['user'] = admin_info("username");
			$vars['user_id'] = admin_info("loggedin");
			$vars['ipaddress'] = $_SERVER['REMOTE_ADDR'];

			$manage_action = $_POST['manage_action'];
			if($manage_action=="void")
			{
				$close_time = localize_datetime($vars['time'], $location_info['timezone']);
				$void_list = "";
				$order_query = rpt_query("select * from `orders` where ".$filter_query_code."`".$dt_field."`>='$date_start $time_start' and `".$dt_field."`<='$date_end $time_end' and `void`='0' and `location_id`='[1]' order by `opened` asc",$locationid);
				while($order_read = mysqli_fetch_assoc($order_query))
				{
					$orderid = $order_read['order_id'];

					if(isset($_POST['check_'.str_replace("-", "_", $orderid)]))
					{
						$v_vars = array();
						$v_vars['closed'] = $close_time;
						$v_vars['auth_by'] = $vars['user'];
						$v_vars['auth_by_id'] = $vars['user_id'];
						$v_vars['last_modified'] = $close_time;
						$v_vars['last_mod_ts'] = time();
						$v_vars['last_mod_device'] = "Control Panel";
						$v_vars['order_id'] = $orderid;
						$v_vars['location_id'] = $locationid;

						lavu_query("update `orders` set `void`='1', `closed`='[closed]', `auth_by`='[auth_by]', `auth_by_id`='[auth_by_id]', `last_modified`='[last_modified]', `last_mod_ts`='[last_mod_ts]', `pushed_ts`='[last_mod_ts]', `last_mod_device`='[last_mod_device]', `cashier`='[auth_by]', `cashier_id`='[auth_by_id]', `closing_device`='[last_mod_device]' WHERE `order_id`='[order_id]' AND `location_id`='[location_id]'", $v_vars);
						echo "voiding order " . $orderid . "<br>";
						if($void_list!="") $void_list .= ",";
						$void_list .= $orderid;
					}
				}
				$vars['action'] = "void group";
				$vars['data'] = $void_list;
				schedule_remote_to_local_sync_if_lls($location_info, $locationid,"void orders",$close_time,$void_list);
			}
			else if($manage_action=="adjust" && FALSE)
			{
				$adjust_type = $_POST['adjust_type'];
				$adjust_amount = str_replace("$","",$_POST['adjust_amount']);
				$adjust_order_id = $_POST['order_id'];
				$adjust_check_id = $_POST['adjust_check_number'];

				for($i=0; $i<count($adjust_type_list); $i++)
				{
					if($adjust_type==$adjust_type_list[$i])
					{
						$order_query = rpt_query("select * from `orders` where `order_id`='[1]'",$adjust_order_id);
						if(mysqli_num_rows($order_query))
						{
							$order_read = mysqli_fetch_assoc($order_query);
							if(isset($order_read[$adjust_type]))
							{
								$start_amount = $order_read[$adjust_type];
								if($start_amount=="" || !is_numeric($start_amount)) $start_amount = 0;
								if(is_numeric($start_amount) && is_numeric($adjust_amount))
								{
									$end_amount = $start_amount * 1 + $adjust_amount * 1;

									if($adjust_type=="cash_paid")
									{
										$applied_amount = ($order_read['cash_paid'] * 1 - $order_read['change_amount']) + $adjust_amount * 1;
										lavu_query("update `orders`	set `$adjust_type`='[1]', `cash_applied`='[2]' where `order_id`='[3]'",$end_amount,$applied_amount,$adjust_order_id);
									}
									else
									{
										lavu_query("update `orders`	set `$adjust_type`='[1]' where `order_id`='[2]'",$end_amount,$adjust_order_id);
									}

									$check_number = 1;
									if($adjust_check_id!="")
									{
										$check_query = rpt_query("select * from `split_check_details` where `id`='[1]'",$adjust_check_id);
										if(mysqli_num_rows($check_query))
										{
											$adjust_split_type = $adjust_type;
											if($adjust_type=="card_paid") $adjust_split_type = "card";
											else if($adjust_type=="cash_paid") $adjust_split_type = "cash";

											$check_read = mysqli_fetch_assoc($check_query);
											$check_start_amount = $check_read[$adjust_split_type];
											$check_number = $check_read['check'];
											if($check_start_amount=="") $check_start_amount = 0;
											$check_end_amount = $check_start_amount * 1 + $adjust_amount * 1;

											//echo "adjust check id: $adjust_check_id start: $check_start_amount end: $check_end_amount $adjust_type<br>";
											if($adjust_type=="cash_paid")
											{
												$check_applied_amount = ($check_read['cash'] * 1 - $order_read['change']) + $adjust_amount * 1;
												lavu_query("update `split_check_details` set `cash`='[1]', `cash_applied`='[2]' where `id`='[3]'",$check_end_amount,$check_applied_amount,$adjust_check_id);
											}
											else
											{
												lavu_query("update `split_check_details` set `$adjust_split_type`='[1]' where `id`='[2]'",$check_end_amount,$adjust_check_id);
											}
										}
									}

									/*id 481
									order_id 1026
									check 1
									amount 20.00
									card_desc
									transaction_id
									refunded 0
									refund_notes
									refunded_by	0
									auth	0
									loc_id	8
									processed 1
									datetime	2011-04-07 15:33:33
									pay_type	Cash
									voided	0
									voided_by	0
									register	receipt
									got_response 0
									change	5.16
									total_collected	14.84
									server_name	Ancori
									action	Sale
									voice_auth	0
									server_id	15*/

									if($adjust_type=="cash_paid" || $adjust_type=="cash") {
										$set_pay_type = "Cash";
										$set_pay_type_id = "1";
									}	else if($adjust_type=="card_paid" || $adjust_type=="card") {
										$set_pay_type = "Card";
										$set_pay_type_id= "2";
									} else if($adjust_type=="gift_certificate") {
										$set_pay_type = "Gift Certificate";
										$set_pay_type_id = "3";
									} else
										$set_pay_type = "Misc";

									$vars2 = array();
									$vars2['order_id'] = $adjust_order_id;
									$vars2['check'] = $check_number;
									$vars2['amount'] = $adjust_amount;
									$vars2['loc_id'] = $locationid;
									$vars2['processed'] = 1;
									$vars2['datetime'] = date("Y-m-d H:i:s");
									$vars2['pay_type'] = $set_pay_type;
									$vars2['pay_type_id'] = $set_pay_type_id;
									$vars2['total_collected'] = $adjust_amount;
									$vars2['server_name'] = admin_info('fullname');
									$vars2['action'] = "Admin Adjustment";
									$vars2['server_id'] = admin_info('loggedin');
									$vars2['register'] = "Back End";

									lavu_query("insert into `cc_transactions` (`order_id`,`check`,`amount`,`loc_id`,`processed`,`datetime`,`pay_type`,`pay_type_id`,`total_collected`,`server_name`,`action`,`server_id`, `register`) values ('[order_id]','[check]','[amount]','[loc_id]','[processed]','[datetime]','[pay_type]','[pay_type_id]','[total_collected]','[server_name]','[action]','[server_id]','[register]')",$vars2);

									$vars['action'] = "adjust";
									$vars['order_id'] = $adjust_order_id;
									$vars['data'] = "<type>$adjust_type</type><amount>$adjust_amount</amount><start_amount>$start_amount</start_amount><end_amount>$end_amount</end_amount>";
								}
							}
						}
					}
				}
			}
			if($vars['action']!="")
				lavu_query("insert into `admin_action_log` (`action`,`time`,`user`,`user_id`,`order_id`,`ipaddress`,`data`) values ('[action]','[time]','[user]','[user_id]','[order_id]','[ipaddress]','[data]')",$vars);
		}

		$sum_cash_paid = 0;
		$sum_card_paid = 0;
		$sum_order_paid = 0;
		$sum_trans_paid = 0;
		$sum_tax = 0;

		//echo "select * from `orders` where ".$filter_query_code."`opened`>='$date_start $time_start' and `opened`<='$date_end $time_end' and `void`!='1' and `location_id`='[1]' order by `opened` asc<br>";
		$order_query = rpt_query("select * from `orders` where ".$filter_query_code."`".$dt_field."`>='$date_start $time_start' and `".$dt_field."`<='$date_end $time_end' and `void`='0' and `location_id`='[1]' order by `opened` asc",$locationid);

		$count_query = rpt_query("select sum(`gift_certificate`) as `gift_certificate`, sum(`cash_paid` * 1 - `change_amount` * 1) as `cash_paid`, sum(`card_paid` * 1) as `card_paid` from `orders` where ".$filter_query_code."`".$dt_field."`>='$date_start $time_start' and `".$dt_field."`<='$date_end $time_end' and `void`='0' and `location_id`='[1]' order by `opened` asc",$locationid);
		$count_read = mysqli_fetch_assoc($count_query);
		//echo "gift certificate: " . $count_read['gift_certificate'] . "<br>cash paid: " . $count_read['cash_paid'] . "<br>card paid: " . $count_read['card_paid'] . "<br>total paid: " . ($count_read['cash_paid'] + $count_read['card_paid']) . "<br>";

		while($order_read = mysqli_fetch_assoc($order_query))
		{
			$order_id = $order_read['order_id'];
			$check_name = "check_".str_replace("-", "_", $order_id);
			$order_info_id = "order_info_".str_replace("-", "_", $order_id);
			$order_split_id = "order_split_".str_replace("-", "_", $order_id);
			$adjust_name = "adjust_".str_replace("-", "_", $order_id);
			$adjust_by_name = "adjust_type_".str_replace("-", "_", $order_id);
			$adjust_check_number = "adjust_check_number_".str_replace("-", "_", $order_id);
			$submit_adjustment = "submit_adjustment_".str_replace("-", "_", $order_id);

			$sum_cash_paid += (float)$order_read['cash_paid'];
			$sum_card_paid += (float)$order_read['card_paid'];
			$sum_tax += (float)$order_read['tax'];

			echo "<tr onmouseover='this.bgColor = \"#aabbcc\"' onmouseout='this.bgColor = \"#ffffff\"' onclick='if(last_order_info_id) document.getElementById(last_order_info_id).style.display = \"none\"; if(last_order_info_id==\"$order_info_id\") last_order_info_id = 0; else {document.getElementById(\"$order_info_id\").style.display = \"table-row\"; last_order_info_id = \"$order_info_id\";}'>";

			echo output_order_cols($order_read,$fields);

			echo "<td>";
			echo "<input type='checkbox' name='$check_name' id='$check_name'>";
			echo "</td>";
			echo "</tr>";

			//$fields = array("order_id:order_id","opened","total:price","subtotal:price","tax:price","discount:price","gift_certificate:price","cash_paid:cash","card_paid:price","due:due");
			if(isset($_GET['check_total']))
			{
				$check_total = $order_read['subtotal'] - $order_read['discount'] + $order_read['tax'] + $order_read['gratuity'];
				if(number_format($check_total,$dp) != number_format($order_read['total'],$dp))
				{
					echo "<tr><td>MISMATCH ON TOTAL: <br>calculated:" . number_format($check_total,$dp) . "<br>in db:".number_format($order_read['total'],$dp)."</td></tr>";
				}
			}

			$no_of_checks = $order_read['no_of_checks'];
			$check_options = array();
			if($no_of_checks > 1)
			{
				$split_query = rpt_query("select * from `split_check_details` where `order_id`='[1]' AND `loc_id` = '[2]' order by `check` asc", $order_id, $locationid);
				while($split_read = mysqli_fetch_assoc($split_query))
				{
					$check_options[] = array($split_read['id'],$split_read['check']);
					echo "<tr>";
					echo "<td align='right'><font color='#000077' style='font-size:10px'>Check " . $split_read['check'] . "</font></td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td>";
					echo output_order_cols($split_read,$split_fields);
					echo "</tr>";
				}
			}

			$paid_total = 0;
			$paid_total_type = array();
			$paid_total_type['gift'] = 0;
			$paid_total_type['alt'] = 0;
			$paid_total_type['card'] = 0;
			$paid_total_type['cash'] = 0;

			echo "<tr style='display:none' id='$order_info_id'><td colspan='$colspan' align='center'>";
			$trans_query = rpt_query("select * from `cc_transactions` where `order_id`='[1]' and `loc_id`='[2]' AND `tip_for_id` = '0' order by `check` asc",$order_id,$locationid);
			if(mysqli_num_rows($trans_query))
			{
				echo "<table>";
				while($trans_read = mysqli_fetch_assoc($trans_query))
				{

					if($trans_read['action']=="")
					{
					}
					else
					{
						echo "<tr>";
						if($no_of_checks > 1)
						{
							echo "<td>Check ".$trans_read['check']."</td>";
						}
						echo "<td>";
						echo $trans_read['pay_type'];
						if($trans_read['card_type']!="") echo " - " . trim($trans_read['card_type'] . " " . $trans_read['card_desc']);
						else if ($trans_read['card_desc']!="") echo " - ".trim($trans_read['card_desc']);
						//if($trans_read['transtype']!="") echo " - " . $trans_read['transtype'];
						echo "</td>";
						//echo "<td>$".number_format($trans_read['total_collected'],$dp,".",",")."</td>";
						echo "<td>&nbsp;&nbsp;&nbsp;&nbsp;".$trans_read['action'];
						if($trans_read['voided']=="1")
							echo " - Voided";
						echo "</td>";
						echo "<td>&nbsp;&nbsp;&nbsp;&nbsp;".$trans_read['server_name']."</td>";
						echo "<td>&nbsp;&nbsp;&nbsp;&nbsp;".display_price($trans_read['amount'] - $trans_read['change'], true)."</td>";
						if($trans_read['processed']!="1" && $trans_read['got_response']=="1" && $trans_read['pay_type_id']=="2")
							echo "<td>Not Batched</td>";
						else
							echo "<td>&nbsp;</td>";
						if(($trans_read['voided']!="1") && (($trans_read['action'] == "Admin Adjustment") || ($trans_read['action'] == "Sale") || ($trans_read['action'] == "Refund")))
						{
							if($trans_read['pay_type']=="Cash") $ptype = "cash";
							else if($trans_read['pay_type']=="Card") $ptype = "card";
							else if($trans_read['pay_type']=="Gift Certificate") $ptype = "gift";
							else $ptype = "alt";

							if($trans_read['action']=="Refund")
							{
								$paid_total -= number_format($trans_read['amount'] - $trans_read['change'],$dp,".","");
								$paid_total_type[$ptype] -= number_format($trans_read['amount'] - $trans_read['change'],$dp,".","");
							}
							else
							{
								$paid_total += number_format($trans_read['amount'] - $trans_read['change'],$dp,".","");
								$paid_total_type[$ptype] += number_format($trans_read['amount'] - $trans_read['change'],$dp,".","");
							}
						}
						echo "</tr>";
					}
				}
				echo "<tr><td>Paid on Record: " . display_price($paid_total,true);
				echo "</table>";
			}

			// this was being abused as a method of voiding cc_transactions
			if (FALSE) {
				echo "<table style='border:solid 1px #888888' bgcolor='#dddddd' width='80%'><td align='center'>";

				echo "<b>Order #" . $order_id . ":</b> ";
				echo "Adjust ";

				if($no_of_checks > 1 && count($check_options) > 0)
				{
					echo "<select name='$adjust_check_number'>";
					echo "<option value=''></option>";
					for($n=0; $n<count($check_options); $n++)
					{
						echo "<option value='".$check_options[$n][0]."'>Check ".$check_options[$n][1]."</option>";
					}
					echo "</select>";
				}
				else
				{
					echo "<input type='hidden' name='$adjust_check_number' value=''>";
				}

				echo "<select name='$adjust_name'>";
				echo "<option value=''></option>";
				for($i=0; $i<count($adjust_type_list); $i++)
				{
					echo "<option value='".$adjust_type_list[$i]."'>";
					echo ucfirst(str_replace("_"," ",$adjust_type_list[$i]));
					echo "</option>";
				}
				echo "</select>";
				echo " by ";
				echo "<input type='text' name='$adjust_by_name'> ";
				echo "<script language='javascript'>";
				echo "\n function ".$submit_adjustment."() { ";
				echo "\n  if(document.manage_orders.$adjust_name.value==\"\") alert(\"Please choose an adjustment type\"); ";
				echo "\n  else if(document.manage_orders.$adjust_by_name.value==\"\") alert(\"Please type the amount to adjust by\"); ";
				echo "\n  else { ";
				echo "\n     document.manage_orders.manage_action.value = \"adjust\"; ";
				echo "\n     document.manage_orders.order_id.value = \"$order_id\"; ";
				echo "\n     document.manage_orders.adjust_type.value = document.manage_orders.$adjust_name.value; ";
				echo "\n     document.manage_orders.adjust_check_number.value = document.manage_orders.$adjust_check_number.value; ";
				echo "\n     document.manage_orders.adjust_amount.value = document.manage_orders.$adjust_by_name.value; ";
				echo "\n     document.manage_orders.submit(); ";
				echo "\n   }";
				echo "\n }";
				echo "</script>";
				echo "<input type='button' value='Submit' onclick='".$submit_adjustment."()'>";
				echo "</td></table>";
			}

			echo "</td></tr>";

			$order_paid_total = number_format($order_read['gift_certificate'] + $order_read['alt_paid'] + $order_read['cash_paid'] + $order_read['card_paid'],2,".","");
			if((float)number_format($paid_total,2,".","") * 1 != (float)$order_paid_total * 1)
			{
				if(isset($_GET['fix_pay_mismatch']) && $_GET['fix_pay_mismatch']==$order_read['order_id'])
				{
					$extra_msg = "<br><a name='fixmm_".$order_read['order_id']."'></a>";
					$extra_msg .= "<br><font color='#008800'>";
					$extra_msg .= "Setting Cash to " . display_price($paid_total_type['cash'],TRUE);
					$extra_msg .= "<br>Setting Card to " . display_price($paid_total_type['card'],TRUE);
					$extra_msg .= "<br>Setting Gift to " . display_price($paid_total_type['gift'],TRUE);
					$extra_msg .= "<br>Setting Alt to " . display_price($paid_total_type['alt'],TRUE);
					$extra_msg .= "<br><a href='index.php?mode={$section}_{$mode}&finalize_pay_mismatch=".$order_read['order_id']."'><input type='button' value='Submit Fix' onclick='window.location = \"index.php?mode={$section}_{$mode}&finalize_pay_mismatch=".$order_read['order_id']."\"'></a></font>";
					$goto_anchor = "fixmm_" . $order_read['order_id'];
				}
				else if(isset($_GET['finalize_pay_mismatch']) && $_GET['finalize_pay_mismatch']==$order_read['order_id'])
				{
					$extra_msg = "<br><a name='fixmm_".$order_read['order_id']."'></a>";
					$extra_msg .= "<br><font color='#008800'>";
					$extra_msg .= "FIXING</font>";

					$query = "update `orders` set `card_paid`='[card]', `cash_paid`='[cash]', `change_amount`='0', `cash_applied`='[cash]', `gift_certificate`='[gift]', `alt_paid`='[alt]' where `order_id`='[order_id]' limit 1";
					$vars = $paid_total_type;
					$vars['order_id'] = $order_read['order_id'];
					/*$extra_msg .= "<br>" . $query;
					foreach($vars as $key => $val)
						$extra_msg .= "<br>$key = $val";*/
					lavu_query($query,$vars);

					$lquery = $query;
					foreach($vars as $vkey => $vval)
					{
						$lquery = str_replace("[".$vkey."]",ConnectionHub::getConn('rest')->escapeString($vval),$lquery);
					}
					schedule_remote_to_local_sync_if_lls($location_info, $locationid,"run local query","orders",$lquery);

					$extra_msg .= "<script language='javascript'>window.location.replace('index.php?mode={$section}_{$mode}')</script>";
					$extra_msg .= "<br><a href='index.php?mode={$section}_{$mode}'>FIXED: (refresh to view)</a>";
					$goto_anchor = "fixmm_" . $order_read['order_id'];
				}
				else
				{
					$extra_msg = "<a name='fixmm_".$order_read['order_id']."' href='index.php?mode={$section}_{$mode}&fix_pay_mismatch=".$order_read['order_id']."'>(fix)</a>";
				}

				echo "<tr><td colspan='$colspan' align='center' style='color:#880000; font-weight:bold'>Pay Mismatch: Payments ".display_price((float)$paid_total,TRUE)." to Order ".display_price((float)$order_paid_total,TRUE)." $extra_msg</td></tr>";
			}
			$sum_order_paid += (float)$order_paid_total;
			$sum_trans_paid += (float)$paid_total;

			if(isset($_GET['contents_subtotal']))
			{
				$contents_subtotal = 0;
				$contents_tax = 0;
				$contents_total = 0;
				$contents_query = rpt_query("select * from `order_contents` where `loc_id`='[1]' and `order_id`='[2]'",$order_read['location_id'],$order_read['order_id']);
				while($contents_read = mysqli_fetch_assoc($contents_query))
				{
					$contents_subtotal += ($contents_read['subtotal_with_mods'] - $contents_read['idiscount_amount']);
					$contents_tax += $contents_read['tax_amount'];
					$contents_total += $contents_read['total_with_tax'];
				}
				if((string)($contents_subtotal * 1) != (string)($order_read['subtotal'] * 1))
					echo "<tr><td colspan='$colspan' align='center' style='color:#000088; font-weight:bold'>Contents Subtotal ".($contents_subtotal * 1)." to Order ".($order_read['subtotal'])."</td></tr>";
				if((string)($contents_tax * 1) != (string)($order_read['tax'] * 1))
					echo "<tr><td colspan='$colspan' align='center' style='color:#880000; font-weight:bold'>Contents Tax ".($contents_tax * 1)." to Order ".($order_read['tax'])."</td></tr>";
				if((string)($contents_total * 1) != (string)($order_read['total'] * 1))
					echo "<tr><td colspan='$colspan' align='center' style='color:#880000; font-weight:bold'>Contents Total ".($contents_total * 1)." to Order ".($order_read['total'])."</td></tr>";
			}
		}
		echo "<tr>";
		echo "<td style='border-top:solid 1px black' colspan='$colspan' align='right'>";
		echo "<table cellpadding=4><td><a style='cursor:pointer' onclick='if(confirm(\"Are you sure you want to Void all checked orders?\")) {document.manage_orders.manage_action.value = \"void\"; document.manage_orders.submit();}'><font color='#004444'>Void All Checked</font></a></td></table>";
		echo "</td>";
		echo "</tr>";

		echo "</table>";
		echo "</form>";
		echo "<br><br>";
		/*echo "<br>Sum Tax: " . $sum_tax;
		echo "<br>Sum Order Paid: " . $sum_order_paid;
		echo "<br>Sum Cash Paid: " . $sum_card_paid;
		echo "<br>Sum Card Paid: " . $sum_cash_paid;
		echo "<br>Sum Trans Paid: " . $sum_trans_paid;*/

		if($goto_anchor!="")
		{
			echo "Goto Anchor: $goto_anchor<br>";
			echo "<script language='javascript'>window.location = '#".$goto_anchor."'</script>";
		}
	}
?>
