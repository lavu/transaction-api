<?php
	echo "<br><br>";
	require_once("/home/poslavu/public_html/admin/sa_cp/show_report.php");
	$date_info = show_report(admin_info("database"),"poslavu_MAIN_db","unified","index.php?mode=$mode","select_date");
	$date_start = $date_info['date_start'];
	$date_end = $date_info['date_end'];
	$time_start = $date_info['time_start'];
	$time_end = $date_info['time_end'];
	$date_reports_by = $date_info['date_reports_by'];
	$start_datetime = $date_start . " " . $time_start;
	$end_datetime = $date_end . " " . $time_end;
	echo "<br><br>";
	echo "<style>";
	echo ".coltitle { font-family:Verdana,Arial; color:#888888; text-align:right; background-color:#f0f0f0; } ";
	echo "</style>";
	
	$check_discount_query = "select SQL_NO_CACHE `orders`.`location_id` AS `field0`,`orders`.`opened` AS `field1`,`orders`.`void` AS `field2`,`orders`.`discount_id` AS `field3`,`discount_types`.`title` AS `field4`,count(`discount_types`.`calc`) AS `field5`,sum(`orders`.`discount`) AS `field6`, `discount_types`.`groupid` as `groupid`,`discount_groups`.`name` as `type` from `orders` LEFT JOIN `discount_types` ON `orders`.`discount_id` = `discount_types`.`id` LEFT JOIN `discount_groups` ON `discount_types`.`groupid`=`discount_groups`.`id` where `orders`.`location_id` = '[locationid]' and `orders`.`[date_reports_by]` >= '[start_datetime]' and `orders`.`[date_reports_by]` <= '[end_datetime]' and `orders`.`void` != 1 and (`orders`.`discount` + 1 - 1) != 0 group by `field3` order by `field4` asc";
	// field0 > location_id
	// field1 > opened
	// field2 > void
	// field3 > discount_id
	// field4 > discount_types.title
	// field5 > count(discount_types.calc)
	// field6 > sum(discount)
	// groupid
	// type
	
	$item_discount_query = "select SQL_NO_CACHE `orders`.`location_id` AS `field0`,`orders`.`opened` AS `field1`,`orders`.`void` AS `field2`,`order_contents`.`void` AS `field3`,`order_contents`.`quantity` AS `field4`,`order_contents`.`loc_id` AS `field5`,`order_contents`.`idiscount_id` AS `field6`,`discount_types`.`title` AS `field7`,count(`discount_types`.`calc`) AS `field8`,sum(`order_contents`.`idiscount_amount`) AS `field9`, `discount_types`.`groupid` as `groupid`,`discount_groups`.`name` as `type` from `orders` LEFT JOIN `order_contents` ON `orders`.`order_id` = `order_contents`.`order_id` LEFT JOIN `discount_types` ON `order_contents`.`idiscount_id` = `discount_types`.`id` LEFT JOIN `discount_groups` ON `discount_types`.`groupid`=`discount_groups`.`id` where `orders`.`location_id` = '[locationid]' and `orders`.`[date_reports_by]` >= '[start_datetime]' and `orders`.`[date_reports_by]` <= '[end_datetime]' and `orders`.`void` != 1 and (`order_contents`.`quantity` + 1 - 1) > 0 and `order_contents`.`loc_id` = `orders`.`location_id` and (`order_contents`.`idiscount_amount` + 1 - 1) != 0 group by `field6` order by `field7` asc";
	
	// field0 > location_id
	// field1 > opened
	// field2 > void
	// field3 > order_contents.void
	// field4 > order_contents.quantity
	// field5 > order_contents.loc_id
	// field6 > order_contents.idiscount_id
	// field7 > discount_types.title
	// field8 > count(discount_types.calc)
	// field9 > sum(order_contents.idiscount_amount)
	// groupid
	// type
	
	$qvars = array();
	$qvars['locationid'] = $locationid;
	$qvars['date_reports_by'] = $date_reports_by;
	$qvars['start_datetime'] = $start_datetime;
	$qvars['end_datetime'] = $end_datetime;
		
	function add_merged_query($query_str,$query_vars,$results,$fields,$sum_fields)
	{
		$cquery = lavu_query($query_str,$query_vars);
		if(mysqli_num_rows($cquery))
		{
			while($cread = mysqli_fetch_assoc($cquery))
			{
				$cid = $cread[$fields['id']];
				if(isset($results[$cid]))
				{
					foreach($sum_fields as $key => $val)
						$results[$cid][$key] += $cread[$val];
				}
				else
				{
					foreach($fields as $key => $val)
						$results[$cid][$key] = $cread[$val];
					foreach($sum_fields as $key => $val)
						$results[$cid][$key] = $cread[$val];
				}
			}
		}
		else
		{
		}
		return $results;
	}
	
	function order_dimensional_array_by($arr,$col,$dec=true)
	{
		$newarr = array();
		foreach($arr as $key => $val)
		{
			if($dec) $newkey = str_pad(str_replace(",","",number_format($val[$col],4)), 12, "0", STR_PAD_LEFT) . "_" . str_pad($key,8, "0", STR_PAD_LEFT);
			else $newkey = $val[$col] . "_" . str_pad($key,8, "0", STR_PAD_LEFT);
			$newarr[$newkey] = $val;
		}
		ksort($newarr);
		return $newarr;
	}
	
	function draw_total_line($val,$ssums,$money_fields,$number_fields,$exclude_fields)
	{
		$str = "";
		$str .= "<tr bgcolor='#8899aa'>";
		foreach($val as $akey => $aval) 
		{
			$money = false; for($n=0; $n<count($money_fields); $n++) if(trim(strtolower($money_fields[$n]))==trim(strtolower($akey))) $money = true;
			$field_is_number = false; for($n=0; $n<count($number_fields); $n++) if(trim(strtolower($number_fields[$n]))==trim(strtolower($akey))) $field_is_number = true;
			$show = true; for($n=0; $n<count($exclude_fields); $n++) if(trim(strtolower($exclude_fields[$n]))==trim(strtolower($akey))) $show = false;
			if($show) 
			{
				$str .= "<td style='font-size:10px; color:#ffffff; font-weight:bold'>";
				if(isset($ssums[$akey]))
				{
					if($money) $str .= "$" . number_format($ssums[$akey],2);
					else if($field_is_number) $str .= $ssums[$akey];
					$ssums[$akey] = 0;
				}
				$str .= "&nbsp;";
				$str .= "</td>";
			}
		}
		$str .= "</tr>";
		return array("str"=>$str,"ssums"=>$ssums);
	}
	
	function output_merged_results($results,$orderby,$orderdir="asc",$number_str="",$money_str="",$exclude_str="id",$subsum_field="")
	{
		global $mode;
		
		$number_fields = explode(",",$number_str);
		$money_fields = explode(",",$money_str);
		$exclude_fields = explode(",",$exclude_str);
		
		$number_fields = array_merge($number_fields,$money_fields);
		$field_is_number = false; for($n=0; $n<count($number_fields); $n++) if(trim(strtolower($number_fields[$n]))==trim(strtolower($orderby))) $field_is_number = true;
		
		$results = order_dimensional_array_by($results,$orderby,$field_is_number);
		if($orderdir=="desc") $results = array_reverse($results);
		
		$str = "";
		$finalstr = "";
		$lastsub_value = "----xxx----";
		$bsums = array();
		$ssums = array();
		foreach($results as $key => $val) 
		{
			if($str=="")
			{
				$str .= "<tr>";
				foreach($val as $akey => $aval) 
				{
					$show = true; for($n=0; $n<count($exclude_fields); $n++) if(trim(strtolower($exclude_fields[$n]))==trim(strtolower($akey))) $show = false;
					if($show) 
					{
						if(trim(strtolower($orderby))==trim(strtolower($akey))) 
						{
							$extra_style = "font-weight:bold;";
							if($orderdir=="asc") $odir = "desc"; else $odir = "asc";
						}
						else 
						{
							$extra_style = "";
							$field_is_number = false; for($n=0; $n<count($number_fields); $n++) if(trim(strtolower($number_fields[$n]))==trim(strtolower($akey))) $field_is_number = true;
							if($field_is_number) $odir = "desc"; else $odir = "asc";
						}
						
						$str .= "<td style='border-bottom:solid 1px black; cursor:pointer; $extra_style' align='center' onclick='window.location = \"?mode=$mode&orderby=$akey&orderdir=$odir\"'>";
						$str .= ucfirst($akey);
						$str .= "</td>";
					}
				}
				$str .= "</tr>";
			}
			
			if($subsum_field!="" && $subsum_field==$orderby)
			{
				if($lastsub_value!="----xxx----" && $lastsub_value!=$val[$subsum_field])
				{
					$lresult = draw_total_line($val,$ssums,$money_fields,$number_fields,$exclude_fields);
					$str .= $lresult['str'];
					$ssums = $lresult['ssums'];					
				}
				$lastsub_value = $val[$subsum_field];
			}
			
			$finalstr = "";
			$str .= "<tr>";
			foreach($val as $akey => $aval) 
			{
				$money = false; for($n=0; $n<count($money_fields); $n++) if(trim(strtolower($money_fields[$n]))==trim(strtolower($akey))) $money = true;
				$field_is_number = false; for($n=0; $n<count($number_fields); $n++) if(trim(strtolower($number_fields[$n]))==trim(strtolower($akey))) $field_is_number = true;
				$show = true; for($n=0; $n<count($exclude_fields); $n++) if(trim(strtolower($exclude_fields[$n]))==trim(strtolower($akey))) $show = false;
				if($show) 
				{
					$str .= "<td>";
					if($money) $str .= "$" . number_format($aval,2);
					else $str .= $aval;
					$str .= "</td>";
					
					if($money || $field_is_number)
					{
						if(!isset($bsums[$akey])) 
						{
							$bsums[$akey] = $aval;
							$ssums[$akey] = $aval;
						}
						else 
						{
							$bsums[$akey] += $aval;
							$ssums[$akey] += $aval;
						}
						$finalstr .= "<td style='border-top:solid 1px black'>";
						if($money) $finalstr .= "$" . number_format($bsums[$akey],2);
						else $finalstr .= $bsums[$akey];
						$finalstr .= "</td>";
					}
					else
					{
						$finalstr .= "<td style='border-top:solid 1px black'>&nbsp;</td>";
					}
				}
			}
			$str .= "</tr>";
			
			if($subsum_field!="" && $subsum_field==$orderby)
			{
				$lresult = draw_total_line($val,$ssums,$money_fields,$number_fields,$exclude_fields);
				$lresult_str = $lresult['str'];
			}
			else $lresult_str = "";
			
			$finalstr = $lresult_str . "<tr>$finalstr</tr>";
		}		
		$str .= $finalstr;
		$str = "<table cellspacing=0 cellpadding=4>$str</table>";
		return $str;
	}

	if(isset($_GET['orderby'])) $orderby = $_GET['orderby']; else $orderby = "amount";
	if(isset($_GET['orderdir'])) $orderdir = $_GET['orderdir']; else $orderdir = "desc";

	$dresults = array();
	$dresults = add_merged_query($check_discount_query,$qvars,$dresults,array("id"=>"field3","title"=>"field4","type"=>"type"),array("count"=>"field5","amount"=>"field6"));
	$dresults = add_merged_query($item_discount_query,$qvars,$dresults,array("id"=>"field6","title"=>"field7","type"=>"type"),array("count"=>"field8","amount"=>"field9"));
	echo output_merged_results($dresults,$orderby,$orderdir,"count","amount","id","type");
?>
