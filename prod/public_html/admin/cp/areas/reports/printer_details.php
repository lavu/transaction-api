<?php
ini_set('display_errors','1');
if( !$in_lavu ){
	return;
}

abstract class PrinterModel {
	protected
		$language,
		$lavu_controller,
		$command_set,
		$model;

	public function __construct( $printer_details ){
		$this->language = null;
		$this->lavu_controller = null;
		$this->command_set = null;
		$this->model = null;
		$this->printerModel( $printer_details );
	}

	protected function printerModel( $printer_details ){
		$this->command_set = $printer_details['value6'];
		$obj = json_decode( $printer_details['value_long2'] );

		$this->language = $obj->language;
		$this->model = $obj->model;
		$this->lavu_controller = $obj->lavu_controller;

	}

	public function getPrinterImage() {
		switch( $this->model ){
			//STAR
			case 'TSP650':
				return '<img style="margin: 0 auto; display: table;" src="images/printers/star-tsp650.png"></img>';
			case 'SP700':
				return '<img style="margin: 0 auto; display: table;" src="images/printers/star-sp700.png"></img>';

			//EPSON
			case 'TM-P60':
			case 'TM-P60II':
			case 'TM-P80':
			case 'TM-T20':
			case 'TM-T20II':
			case 'TM-T70':
			case 'TM-T70II':
			case 'TM-T81II':
			case 'TM-T82':
			case 'TM-T82II':
				return '';
			case 'TM-T88V':
			case 'TM-T88V-i':
				return '<img style="margin: 0 auto; display: table;" src="images/printers/epson-t88v.png"></img>';
			case 'TM-T90II':
			case 'TM-TU220':
				return '<img style="margin: 0 auto; display: table;" src="images/printers/epson-tmu220.png"></img>';
			//BIXOLON
			case 'SRP-350':
				return '<img style="margin: 0 auto; display: table;" src="images/printers/bixolon-samsung-srp-350.png"></img>';
			case 'SRP-275':
				return '<img style="margin: 0 auto; display: table;" src="images/printers/bixolon-samsung-srp-275.png"></img>';
			default:
				return '';
		}
	}
	public abstract function brand();
	public abstract function commandSet();
	public function model() {
		return $this->model;
	}
}

class EpsonModel extends PrinterModel {
	public function brand(){
		return 'Epson';
	}
	public function commandSet(){
		switch( $this->command_set ){
			case '2':
				return 'Epson Thermal';
			case '4':
				return 'Epson Impact';
			default:
				return 'Unknown Command Set';
		}
	}
}

class StarModel extends PrinterModel {
	public function brand(){
		return 'Star';
	}

	public function commandSet(){
		switch( $this->command_set ){
			case '1':
				return 'Star Thermal';
			case '13':
				return 'Star Impact';
			case '15':
				return 'TSP650';
			case '16':
				return 'SP700';
			default:
				return 'Unknown Command Set';
		}
	}
}

class BixolonModel extends PrinterModel {
	public function brand(){
		return 'Bixolon';
	}

	public function commandSet(){
		switch( $this->command_set ){
			case '6':
				return 'Bixolon';
			case '9':
				return 'Bixolon (Basic)';
			case '11':
				return 'Bixolon (No Cash Drawer)';
			default:
				return 'Unknown Command Set';
		}
	}
}

class PartnerModel extends PrinterModel {
	public function brand(){
		return 'Partner';
	}
	public function commandSet(){
		switch( $this->command_set ){
			case '3':
				return 'Partner';
			default:
				return 'Unknown Command Set';
		}
	}
}

class SNBCModel extends PrinterModel {
	public function brand(){
		return 'SNBC';
	}
	public function commandSet(){
		switch( $this->command_set ){
			case '5':
				return 'BTP-M280';
			default:
				return 'Unknown Command Set';
		}
	}
}

class KDSModel extends PrinterModel {
	public function brand(){
		return 'KDS';
	}
	public function commandSet(){
		switch( $this->command_set ){
			case '12':
				return 'KDS';
			case '19':
				return 'KDS Pro';
			default:
				return 'Unknown Command Set';
		}
	}
}

class PrinterModelFactory {
	public static function printerModel( $printer_details ){
		$command_set = $printer_details['value6']*1;
		switch( $command_set ){
			case 2:
			case 4:
				return new EpsonModel( $printer_details );
			case 1:
			case 13:
			case 15:
			case 16:
				return new StarModel( $printer_details );
			case 6:
			case 9:
			case 11:
				return new BixolonModel( $printer_details );
			case 3:
				return new PartnerModel( $printer_details );
			case 5:
				return new SNBCModel( $printer_details );
			case 12:
			case 19:
				return new KDSModel( $printer_details );
			default:
				return null;
		}
	}
}

class Printer {
	protected
		$designator,
		$name,
		$ip_address,
		$port,
		$model,
		$characters_per_line,
		$characters_per_line_large_font,
		$line_spacing,
		$image_capability,
		$skip_ip_address_ping,
		$perform_status_check,
		$use_etb_flag,
		$inter_write_pause,
		$print_each_item_on_separate_ticket,
		$group_labeling;

	public function __construct(){
		$this->designator = null;
		$this->name = null;
		$this->ip_address = null;
		$this->port = null;
		$this->characters_per_line = null;
		$this->characters_per_line_large_font = null;
		$this->line_spacing = null;
		$this->image_capability = null;
		$this->skip_ip_address_ping = null;
		$this->perform_status_check = null;
		$this->use_etb_flag = null;
		$this->inter_write_pause = null;
		$this->print_each_item_on_separate_ticket = null;
		$this->group_labeling = null;
	}

	public static function printer( $printer_details ){
		$result = new Printer();
		$result->designator = $printer_details['setting'];
		$result->name = $printer_details['value2'];
		$result->ip_address = $printer_details['value3'];
		$result->port = $printer_details['value5'];
		// $result->command_set = some_function( $printer_details );
		// $result->model = some_function( $printer_details );
		$result->characters_per_line = $printer_details['value7'];
		$result->characters_per_line_large_font = $printer_details['value8'];
		$result->line_spacing = $printer_details['value9'];
		// $result->image_capability = some_function( $printer_details );
		$result->skip_ip_address_ping = $printer_details['value13'];
		$result->perform_status_check = $printer_details['value11'];
		$result->use_etb_flag = $printer_details['value12'];
		$result->inter_write_pause = $printer_details['value14'];
		$result->print_each_item_on_separate_ticket = $printer_details['value_long'];
		$result->group_labeling = $printer_details['value'];

		$result->model = PrinterModelFactory::printerModel( $printer_details );
		return $result;
	}

	public function outputDetails(){
		echo $this->model->getPrinterImage();
		echo '<br />';
		echo '<table style="margin: 0 auto;"><tbody>';
		echo '<tr><td class="alignRight"><b>Designator</b>:</td><td>' . $this->designator . '</td>';
		echo '<tr><td class="alignRight"><b>Name</b>:</td><td>' . $this->name . '</td>';
		echo '<tr><td class="alignRight"><b>Group Labeling</b>:</td><td>' . $this->group_labeling . '</td>';
		echo '<tr><td class="alignRight"><b>IP Address</b>:</td><td>' . $this->ip_address . '</td>';
		echo '<tr><td class="alignRight"><b>Port</b>:</td><td>' . $this->port . '</td>';
		echo '<tr><td class="alignRight"><b>Brand</b>:</td><td>' . $this->model->brand() . '</td>';
		echo '<tr><td class="alignRight"><b>Command Set</b>:</td><td>' . $this->model->commandSet() . '</td>';
		echo '<tr><td class="alignRight"><b>Model</b>:</td><td>' . $this->model->model() . '</td>';
		echo '</tbody></table>';
	}
}



$printer_details = null;
{
	$arguments = array(
		'designator' => $_GET['designator'],
		'loc_id' => $locationid
	);
	$printer_details_query = lavu_query("SELECT * FROM `config` WHERE `type`='printer' AND `setting` = '[designator]' AND `location`='[loc_id]' ORDER BY `_deleted` ASC, `id` DESC", $arguments );
	if( $printer_details_query === false || !mysqli_num_rows($printer_details_query)){
		echo 'No Printers Found';
		return;
	}
	if( $printer_details = mysqli_fetch_assoc( $printer_details_query ) ){
	} else {
		echo 'error?';
		return;
	}
}

$printer = Printer::printer( $printer_details );
$printer->outputDetails();