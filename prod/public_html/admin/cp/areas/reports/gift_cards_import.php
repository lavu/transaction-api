<?php
	//echo "Dataname:".$data_name;
	if($data_name != 'demo_brian_d'){
		return;
	}


	if(empty($rdb) || empty($data_name) || empty($locationid) || !$in_lavu){
		exit;
	}

	ini_set('display_errors',1);

	require_once(dirname(__FILE__).'/../../resources/core_functions.php');
	require_once(dirname(__FILE__).'/../../resources/csv_input_output.php');

	if(!empty($_POST['submit_upload'])){
		handleUploadSubmit();
	}else{
		echo getUploadCSVFileHTML();
	}


	function getUploadCSVFileHTML(){
		$html = <<<HTML
<div>
		The file import type is .csv (comma seperated values).<br>This is a common output format for databases and spreadsheet software (e.g. Excel).<br>
		The final output may contain the following fields:'Gift Card ID', 'Balance', 'Expiration Date', 'Points'<br>
		Please note the date should be 4 digit year, 2 digit month (use leading 0 if necessary),<br>and 2 digit day of month (also use leading 0 if necessary).
		An example file may look like this:<br>
<div style='text-align:left;margin-left:auto;margin-right:auto;display:inline-block;border:1px solid gray;'><pre>
Gift Card ID,Balance,Expiration
12345,13.42,2016-01-01
23456,9.33,2016-01-01
</pre></div><br>
Also note, gift cards with duplicate 'Gift Card ID' will not be entered.<br><br><br>
		<form action="?mode=reports_gift_cards_import" method="post" enctype="multipart/form-data">
			Select the Gift Card .csv file to import:
			<input type="file" name="csv_upload_list" id="csv_upload_file_chooser_id" style="background-color:gray;"><br>
			<input type="submit" value="Add Gift Cards in list." name="submit_upload" style="width:250px;">
		</form>
HTML;
		return $html;
	}

	function handleUploadSubmit(){
		global $locationid;
		$tempFilePath=$_FILES["csv_upload_list"]["tmp_name"];
		$giftCardCSVTitles2DBColumnNamesMap = _getGiftCardCSVTitleToDBFieldMap();
		$defaultDBValuesMap = _getDefaultDBValuesMap();
		$disallowedDuplicateColumns = _getDisallowDuplicatesAcrossColumnsMap();
		//Set the needed maps
		setCSVFieldToDBFieldMap( $giftCardCSVTitles2DBColumnNamesMap );
		setDefaultValuesForDBMap( $defaultDBValuesMap );
		setDisallowDuplicatesAcrossColumns( $disallowedDuplicateColumns );

		$totalRowsInserted = performInsertForCSVFile($tempFilePath, 'lavu_gift_cards', 'poslavu_MAIN_db');//We pass the optional 3rd Arg, MAIN db, to override database.

		//schedule_remote_to_local_sync($locationid,"table updated",'lavu_gift_cards');
		echo "Total number of customer gift card rows inserted:".$totalRowsInserted;
	}







	//-------------------------------------------- HARD CODED OPTIONS/DATA SETS --------------------------------------------
	function _getGiftCardCSVTitleToDBFieldMap(){
		return array("Gift Card ID"=>"name","Balance"=>"balance", "Expiration"=>"expires", "Points"=>"points");
	}
	function _getDefaultDBValuesMap(){
		global $data_name;
		global $locinfo;
		$currLocalizedTime = localize_datetime(date('Y-m-d H:i:s'), $locinfo['timezone']);
		$defaultHistoryXML4Import = <<<DEFAULTHISTORYXML
			<event><action>Created By Backend Import</action><datetime>$currLocalizedTime</datetime></event>
DEFAULTHISTORYXML;
		return array("dataname"=>$data_name,"created_datetime"=>$currLocalizedTime);
	}
	function _getDisallowDuplicatesAcrossColumnsMap(){
		return array('name', 'dataname');
	}
	//----------------------------------------------------------------------------------------------------------------------
?>