<?php
/**
 * Created by PhpStorm.
 * User: hayat
 * Date: 10/18/17
 * Time: 5:01 PM
 */

	session_start();

    require_once (dirname(__FILE__)."/abi_report_config.php");
	require_once(dirname(dirname(dirname(__FILE__))) ."/resources/core_functions.php");
    require_once(dirname(dirname(dirname(__FILE__))) ."/resources/chain_functions.php");
    require_once dirname(__FILE__) . '/../../areas/currency_util.php';
    require_once (dirname(__FILE__)."/abi_report_functions.php");

    if((isset($_REQUEST['auth']) && ($_REQUEST['auth'] != '4au4TH$H$TIh4hauigahisguhfv8fahiHT$-gani4inrginff')) || !isset($_REQUEST['auth']))
    {
        authFailed();
    }

    if (isset($_REQUEST['sessid']) && $_REQUEST['sessid'] != '') {
        session_id($_REQUEST['sessid']);
    }


    global $Conn;
    global $data_name;
    global $locationsDatanames;
    global $PrimaryRestaurantID;
    global $ConvertTo;

    if( isset($_REQUEST['dataname']) && $_REQUEST['dataname'] != ''){
        $data_name = $_REQUEST['dataname'];
    } else {
        $data_name = sessvar('admin_dataname');
    }
    if( isset( $_REQUEST['timezone'] ) ){
        $timezone = $_REQUEST['timezone'];
    } else {
        $timezone = sessvar('location_info')['timezone'];
        //$timezone = sessvar('timezone');
    }

    date_default_timezone_set("America/Denver");
    if($timezone != '') {
        date_default_timezone_set($timezone);
    }

    $reportedLocation = '';
    $reportedRegion = '';

    if (isset($_REQUEST['report_region']) && $_REQUEST['report_region'] != '') {
        $reportedRegion = $_REQUEST['report_region'];
        $reportedLocation = (isset($_REQUEST['report_location']) && $_REQUEST['report_location'] != '') ? $_REQUEST['report_location'] : '';
    } else {
        $reportedLocation = $data_name;
    }

    $chainID = sessvar('admin_chain_id');
    $PrimaryRestaurantID = getPrimaryChainRestaurant($chainID);
    $country = strtolower(sessvar('country'));
    list($reportRegions, $staticPlanRecord, $staticBeerVolume) = getPlannedData($chainID);
    $planRecord = $staticPlanRecord[$reportedRegion];
    $beerVolume = $staticBeerVolume[$reportedRegion];

    $Conn = mysqli_connect(my_poslavu_host(), my_poslavu_username(), my_poslavu_password(), TRUE);
    $reportDb = "poslavu_".$data_name."_db";
    mysqli_select_db($reportDb, $Conn);
    if(isset($_REQUEST['action']) && $_REQUEST['action'] != "login") {
        if ($data_name == '') {
            $userInfo['message'] = "Sorry, Session has been expired.";
            $status = "failed";
            jsonResponse($userInfo, $status, 401);
        }
        $filterClick = strtoupper($_REQUEST['filter_click']);
        switch ($filterClick) {
            case 'TODAY':
                $startDate = date('Y-m-d H:i:s', mktime(0, 0, 0, date('m'), date('d'), date('Y')));
                $endDate = date('Y-m-d H:i:s', mktime(23, 59, 59, date('m'), date('d'), date('Y')));
                $lastYearDate = strtotime("-1 year", time());
                $lastStartDate = date('Y-m-d H:i:s', mktime(0, 0, 0, date('m', $lastYearDate), date('d', $lastYearDate), date('Y', $lastYearDate)));
                $lastEndDate = date("Y-m-d H:i:s", mktime(23, 59, 59, date('m', $lastYearDate), date('d', $lastYearDate), date('Y', $lastYearDate)));
                break;
            case 'YESTERDAY':
                $yesterdayDate = strtotime("-1 day", time());
                $lastYearYesterdayDate = strtotime("-1 year", $yesterdayDate);
                $startDate = date('Y-m-d H:i:s', mktime(0, 0, 0, date('m', $yesterdayDate), date('d', $yesterdayDate), date('Y', $yesterdayDate)));
                $endDate = date('Y-m-d H:i:s', mktime(23, 59, 59, date('m', $yesterdayDate), date('d', $yesterdayDate), date('Y', $yesterdayDate)));
                $lastStartDate = date('Y-m-d H:i:s', mktime(0, 0, 0, date('m', $lastYearYesterdayDate), date('d', $lastYearYesterdayDate), date('Y', $lastYearYesterdayDate)));
                $lastEndDate = date('Y-m-d H:i:s', mktime(23, 59, 59, date('m', $lastYearYesterdayDate), date('d', $lastYearYesterdayDate), date('Y', $lastYearYesterdayDate)));
                break;
            case '7DAYS':
                $time = strtotime("-7 days", time());
                $timeStartLastYear = strtotime("-1 year", $time);
                $timeEndLastYear = strtotime("-1 year", time());
                $startDate = date('Y-m-d H:i:s', mktime(0, 0, 0, date('m', $time), date('d', $time), date('Y', $time)));
                $endDate = date('Y-m-d H:i:s', mktime(23, 59, 59, date('m'), date('d'), date('Y')));
                $lastStartDate = date('Y-m-d H:i:s', mktime(0, 0, 0, date('m', $timeStartLastYear), date('d', $timeStartLastYear), date('Y', $timeStartLastYear)));
                $lastEndDate = date('Y-m-d H:i:s', mktime(23, 59, 59, date('m', $timeEndLastYear), date('d', $timeEndLastYear), date('Y', $timeEndLastYear)));
                break;
            case 'MTD':
                $time = strtotime("-1 month", time());
                $timeStartLastYear = strtotime("-1 year", time());
                $timeEndLastYear = strtotime("-1 year", time());
                $startDate = date('Y-m-d H:i:s', mktime(0, 0, 0, date('m'), 1, date('Y', $time)));
                $endDate = date('Y-m-d H:i:s', mktime(23, 59, 59, date('m'), date('d'), date('Y')));
                $lastStartDate = date('Y-m-d H:i:s', mktime(0, 0, 0, date('m', $timeStartLastYear), 1, date('Y', $timeStartLastYear)));
                $lastEndDate = date('Y-m-d H:i:s', mktime(23, 59, 59, date('m', $timeEndLastYear), date('d', $timeEndLastYear), date('Y', $timeEndLastYear)));
                break;
            case 'YTD':
                $timeStartLastYear = strtotime("-1 year", time());
                $timeEndLastYear = strtotime("-1 year", time());
                $startDate = date('Y-m-d H:i:s', mktime(0, 0, 0, 1, 1, date('Y')));
                $endDate = date('Y-m-d H:i:s', mktime(23, 59, 59, 12, 31, date('Y')));
                $lastStartDate = date('Y-m-d H:i:s', mktime(0, 0, 0, 01, 01, date('Y', $timeStartLastYear)));
                $lastEndDate = date('Y-m-d H:i:s', mktime(23, 59, 59, 12, 31, date('Y', $timeEndLastYear)));
                break;
            case 'RANGE':
                $filter_start_date = strtotime($_REQUEST['filter_start_date']);
                $filter_end_date = strtotime($_REQUEST['filter_end_date']);
                $startDate = date("Y-m-d H:i:s", $filter_start_date);
                $endDate = date('Y-m-d H:i:s', $filter_end_date);
                $timePrevStartYear = strtotime("-1 year", $filter_start_date);
                $lastStartDate = date("Y-m-d H:i:s", $timePrevStartYear);
                $timePrevEndYear = strtotime("-1 year", $filter_end_date);
                $lastEndDate = date("Y-m-d H:i:s", $timePrevEndYear);
                break;
            default:
                $startDate = date('Y-m-d H:i:s', mktime(0, 0, 0, date('m'), date('d'), date('Y')));
                $endDate = date('Y-m-d H:i:s', mktime(23, 59, 59, date('m'), date('d'), date('Y')));
                $lastYearDate = strtotime("-1 year", time());
                $lastStartDate = date('Y-m-d H:i:s', mktime(0, 0, 0, date('m', $lastYearDate), date('d', $lastYearDate), date('Y', $lastYearDate)));
                $lastEndDate = date("Y-m-d H:i:s", mktime(23, 59, 59, date('m', $lastYearDate), date('d', $lastYearDate), date('Y', $lastYearDate)));
        }
        $singleLocation = ($reportedRegion == '' || ($reportedRegion != ''  && $reportedLocation != '') ) ? true : false;
        list($getAllLocation, $reportLocations) = getRegionWiseChainLocations($data_name, $chainID, $reportRegions);
        $locationsDatanames = getLocationForReport($data_name, $reportedLocation, $reportedRegion, $singleLocation);
        $ConvertTo = getReportCountry($reportDb);
        // Store dataname and Company name mapping into array
        foreach ($getAllLocation as $locDetails) {
            foreach ($locDetails as $details) {
                $datanameCompanyNameMap[$details['data_name']]['name'] = $details['company_name'];
                $datanameCompanyNameMap[$details['data_name']]['id'] = $details['company_id'];
            }
        }
    }


    if (isset($_REQUEST['action']) && $_REQUEST['action'] == "login")
    {
        $userInfo = array();
        $cust_query = mlavu_query("SELECT `dataname` FROM `poslavu_MAIN_db`.`customer_accounts` WHERE `username` = '[1]'", trim($_REQUEST['un']));
        if(mysqli_num_rows($cust_query) > 0) {
            $cust_read = mysqli_fetch_assoc($cust_query);
            $dataname = $cust_read['dataname'];

            $user_query = mlavu_query("SELECT `id`, `quick_serve`, `service_type`, `loc_id`, `f_name`, `l_name`, `access_level`, `PIN`, `lavu_admin`, `role_id`, `_use_tunnel`, `username` FROM `poslavu_[1]_db`.`users` WHERE `username` = '[2]' AND `password` = PASSWORD('[3]') AND `_deleted` != '1' AND `active` = '1'", $dataname, trim($_REQUEST['un']), trim($_REQUEST['pw']));
            if(mysqli_num_rows($user_query) > 0) {
                $user_read = mysqli_fetch_assoc($user_query);
                $user_level = $user_read['access_level'];
                $rest_query = mlavu_query("select * from `poslavu_MAIN_db`.`restaurants` where `data_name`='[1]' and (`disabled`='' or `disabled`='0')",$dataname);
                if(mysqli_num_rows($rest_query))
                {
                    $rest_read = mysqli_fetch_assoc($rest_query);
                    $chain_id = trim($rest_read['chain_id']);

                    $company_name = trim($rest_read['company_name']);
                    if($company_name=="") $company_name = $rest_read['data_name'];
                    $loclist = rawurlencode($company_name) . "|" . rawurlencode($rest_read['data_name']);
                    if($user_level * 1 == 4 && $chain_id!="" && is_numeric($chain_id) && $chain_id * 1 > 0)
                    {
                        $loc_query = mlavu_query("select * from `poslavu_MAIN_db`.`restaurants` where `chain_id`='[1]' and (`disabled`='' or `disabled`='0')",$chain_id);
                        while($loc_read = mysqli_fetch_assoc($loc_query))
                        {
                            if($loc_read['data_name']!="")
                            {
                                $loc_company_name = trim($loc_read['company_name']);
                                if(trim($loc_company_name)=="") $loc_company_name = $loc_read['data_name'];
                                $loclist .= ",";
                                $loclist .= rawurlencode($loc_company_name) . "|" . rawurlencode($loc_read['data_name']);
                            }
                        }
                    }
                    $loclist = str_replace("%20"," ",$loclist);
                    $status = "success";
                    $userInfo['dataname'] = $dataname;
                    $userInfo['customerid'] = $user_read['id'];
                    $userInfo['fullname'] = trim($user_read['f_name'] . " " . $user_read['l_name']);
                    $userInfo['username'] = $user_read['username'];
                    $userInfo['access_level'] = $user_read['access_level'];
                    $userInfo['role_id'] = $user_read['role_id'];
                    $userInfo['loclist'] = $user_read['loclist'];
                    set_sessvar("access_level",$user_read['access_level']);

                    global $location_info;

                    $mon_left_sym = "$ ";
                    $mon_right_sym = "";
                    $mon_comma = ",";
                    $mon_decimal = ".";
                    $mon_no_of_decimals = 2;
                    if(!isset($location_info) || !is_array($location_info) || count($location_info) < 1)
                    {
                        $loc_query = mlavu_query("select * from `poslavu_[1]_db`.`locations` limit 1",$dataname);
                        while($loc_read = mysqli_fetch_assoc($loc_query))
                        {
                            $location_info = $loc_read;
                        }
                    }
                    $formats = $location_info;
                    if($formats['left_or_right'] == 'left')
                    {
                        $mon_left_sym = $formats['monitary_symbol'] . " ";
                        $mon_right_sym = "";
                    }
                    else
                    {
                        $mon_right_sym = " " . $formats['monitary_symbol'];
                        $mon_left_sym = "";
                    }
                    if($formats['decimal_char'] == ',')
                    {
                        $mon_comma = ".";
                        $mon_decimal = ",";
                    }
                    $mon_no_of_decimals = $formats['disable_decimal'] * 1;
                    $timezone = $location_info['timezone'];
                    set_sessvar("admin_dataname",$dataname);
                    set_sessvar("mon_left_sym",$mon_left_sym);
                    set_sessvar("mon_right_sym",$mon_right_sym);
                    set_sessvar("mon_comma",$mon_comma);
                    set_sessvar("mon_decimal",$mon_decimal);
                    set_sessvar("mon_no_of_decimals",$mon_no_of_decimals);
                    set_sessvar("timezone",$timezone);
                    set_sessvar('locationid',$formats['id']);
                    set_sessvar("city",$formats['city']);
                    set_sessvar("state",$formats['state']);
                    set_sessvar("country",$formats['country']);
                    set_sessvar("chain_id", $chain_id);
                    $userInfo['mon_left_sym'] = $mon_left_sym;
                    $userInfo['mon_right_sym'] = $mon_right_sym;
                    $userInfo['mon_comma'] = $mon_comma;
                    $userInfo['mon_decimal'] = $mon_decimal;
                    $userInfo['mon_no_of_decimals'] = $mon_no_of_decimals;
                    $userInfo['sessid'] = session_id();
                }
            } else {
                $userInfo['error'] = "Username / Password not found";
                $status = "failed";
            }
        } else {
            $userInfo['message'] = "User Not Found";
            $status = "failed";
        }
        jsonResponse($userInfo, $status);
    } else if (isset($_REQUEST['action']) && strtolower($_REQUEST['action']) == "getalllocations")
    {
        jsonResponse($getAllLocation, 'success');
    } else if (isset($_REQUEST['action']) && strtolower($_REQUEST['action']) == "dashboard")
    {
        $dashBoardReport = getDashboardReport($startDate, $endDate, $lastStartDate, $lastEndDate);
        jsonResponse($dashBoardReport, 'success');
    } else if (isset($_REQUEST['action']) && strtolower($_REQUEST['action']) == "sales_payments")
    {
        $salesAndPayments = getSalesAndPaymentReport($startDate, $endDate);
        jsonResponse($salesAndPayments, 'success');
    } else if (isset($_REQUEST['action']) && strtolower($_REQUEST['action']) == "product_inventory")
    {
        $salesAndPayments = getProductInventoryReport($startDate, $endDate, 10);
        jsonResponse($salesAndPayments, 'success');
    } else if (isset($_REQUEST['action']) && strtolower($_REQUEST['action']) == "trends_view")
    {
        $salesAndPayments = getTrendsViewReport($startDate, $endDate, $lastStartDate, $lastEndDate, 10);
        jsonResponse($salesAndPayments, 'success');
    } else if (isset($_REQUEST['action']) && strtolower($_REQUEST['action']) == "current_region")
    {
        $country = sessvar('location_info')['country'];
        $regionResponse['region'] = $country;
        $regionResponse['location'] = ($country != '') ? $data_name : '';
        jsonResponse($regionResponse, 'success');
    } else {
        $info['message'] = "Invalid Action";
        $status = "failed";
        jsonResponse($info, $status);
    }

    function getSummaryReport($startDate, $endDate, $lastStartDate, $lastEndDate)
    {
        global $reportRegions, $data_name, $reportedLocation, $reportedRegion, $singleLocation;
        $summaryResponse = array();
        $selectedLocationReport = array();
        $summaryReportCurrent = array();
        $summaryReportPrev = array();
        $aheadOrBehind = array();
        $aheadOrBehindReport = array();
        $zoneBoardReport = array();
        $regionConversionRate = array();
        if ($singleLocation && $reportedRegion == '') {
            array_unshift($reportRegions , 'currentLocation');
            $reportedRegion = 'currentLocation';
        }
        foreach ($reportRegions as $region) {
            $regionLocations = getLocationForReport($data_name, $reportedLocation, $region, false);
            $conversionFlag = true;
            foreach ($regionLocations as $dataName) {
                $rdb = "poslavu_" . $dataName . "_db";
                // Get conversion rate only one time for reducing query call for each dataname
                if ($conversionFlag) {
                    $chainCountry = getReportCountry($rdb);
                    list($conversionRateFrom, $conversionRateTo) = getConversionCurrency($chainCountry, $endDate);
                    list($prevYearConversionRateFrom, $prevYearconversionRateTo) = getConversionCurrency($chainCountry, $lastEndDate);
                    $conversionFlag = false;
                    $regionConversionRate[$region]['from'] = $conversionRateFrom;
                    $regionConversionRate[$region]['to'] = $conversionRateTo;
                    $prevYearConversionRate[$region]['from'] = $prevYearConversionRateFrom;
                    $prevYearConversionRate[$region]['to'] = $prevYearconversionRateTo;
                }
                $sale_query = lavu_query("select YEAR(orders.closed) AS year, DATE_FORMAT(orders.closed, '%c') AS month, if(count(distinct orders.ioid )>count( distinct orders.order_id ),count( distinct orders.ioid ),count( distinct orders.order_id )) as order_count, if(count(*) > 0, sum((order_contents.price + order_contents.modify_price + order_contents.forced_modifiers_price)*order_contents.quantity - if( order_contents.quantity > '0', order_contents.itax, 0) ), '0.00') as gross, if(count(*) > 0, sum((order_contents.price + order_contents.modify_price + order_contents.forced_modifiers_price)*order_contents.quantity - if( order_contents.quantity > '0',if(order_contents.discount_amount<(order_contents.price + order_contents.modify_price + order_contents.forced_modifiers_price),order_contents.discount_amount,order_contents.price + order_contents.modify_price + order_contents.forced_modifiers_price)+ order_contents.idiscount_amount + order_contents.itax, 0)), '0.00') as net from `[3]`.`orders` LEFT JOIN `[3]`.`order_contents` ON `orders`.`order_id` = `order_contents`.`order_id` AND `orders`.`location_id` = `order_contents`.`loc_id`  LEFT JOIN `[3]`.`menu_items` ON `order_contents`.`item_id` = `menu_items`.`id` where orders.closed >= '[1]' and orders.closed < '[2]' and orders.void = '0' and orders.location_id='1' and  order_contents.item != 'SENDPOINT' GROUP BY year, month", $startDate, $endDate, $rdb);
                while ($sales_read = mysqli_fetch_assoc($sale_query)) {
                    $summaryReportCurrent[$region][$dataName][$sales_read['year']][$sales_read['month']]['net'] += $sales_read['net'];
                    $summaryReportCurrent[$region][$dataName][$sales_read['year']][$sales_read['month']]['gross'] += $sales_read['gross'];
                    $summaryReportCurrent[$region][$dataName][$sales_read['year']][$sales_read['month']]['transaction'] += $sales_read['order_count'];
                    $zoneBoardReport[$region][$dataName] += $sales_read['net'];
                }
                $sale_prev_query = lavu_query("select if(count(distinct orders.ioid )>count( distinct orders.order_id ),count( distinct orders.ioid ),count( distinct orders.order_id )) as order_count, if(count(*) > 0, sum((order_contents.price + order_contents.modify_price + order_contents.forced_modifiers_price)*order_contents.quantity - if( order_contents.quantity > '0', order_contents.itax, 0) ), '0.00') as gross, if(count(*) > 0, sum((order_contents.price + order_contents.modify_price + order_contents.forced_modifiers_price)*order_contents.quantity - if( order_contents.quantity > '0',if(order_contents.discount_amount<(order_contents.price + order_contents.modify_price + order_contents.forced_modifiers_price),order_contents.discount_amount,order_contents.price + order_contents.modify_price + order_contents.forced_modifiers_price)+ order_contents.idiscount_amount + order_contents.itax, 0)), '0.00') as net from `[3]`.`orders` LEFT JOIN `[3]`.`order_contents` ON `orders`.`order_id` = `order_contents`.`order_id` AND `orders`.`location_id` = `order_contents`.`loc_id`  LEFT JOIN `[3]`.`menu_items` ON `order_contents`.`item_id` = `menu_items`.`id` where orders.closed >= '[1]' and orders.closed < '[2]' and orders.void = '0' and orders.location_id='1' and  order_contents.item != 'SENDPOINT'", $lastStartDate, $lastEndDate, $rdb);
                while ($sales_prev_read = mysqli_fetch_assoc($sale_prev_query)) {
                    $summaryReportPrev[$region][$dataName]['net'] += $sales_prev_read['net'];
                    $summaryReportPrev[$region][$dataName]['gross'] += $sales_prev_read['gross'];
                    $summaryReportPrev[$region][$dataName]['transaction'] += $sales_prev_read['order_count'];
                }
            }
        }
        $totalNetSales = 0;
        $totalGrossSales = 0;
        $totalTransactions = 0;
        $totalNetLastSales = 0;
        $totalGrossLastSales = 0;
        $totalLastTransactions = 0;
        if ($singleLocation) {
            if (isset($summaryReportCurrent['currentLocation'])) {
                $selectedRegionReportCurrent = $summaryReportCurrent['currentLocation'][$reportedLocation];
                $selectedRegionReportPrev = $summaryReportPrev['currentLocation'][$reportedLocation];
            } else if (isset($summaryReportCurrent[$reportedRegion][$reportedLocation])) {
                $selectedRegionReportCurrent = $summaryReportCurrent[$reportedRegion][$reportedLocation];
                $selectedRegionReportPrev = $summaryReportPrev[$reportedRegion][$reportedLocation];
            }
            foreach ($selectedRegionReportCurrent as $currentYear => $monthlyCurrentReports) {
                foreach($monthlyCurrentReports as $monthly => $currentReports) {
                    $totalNetSales += $currentReports['net'];
                    $totalGrossSales += $currentReports['gross'];
                    $totalTransactions += $currentReports['transaction'];
                    $aheadOrBehind[$currentYear][$monthly] += $currentReports['net'];
                }

            }
            foreach ($selectedRegionReportPrev as $prevReports) {
                $totalNetLastSales += $prevReports['net'];
                $totalGrossLastSales += $prevReports['gross'];
                $totalLastTransactions += $prevReports['transaction'];
            }

        } else if (isset($summaryReportCurrent[$reportedRegion])) {
            $selectedRegionReportCurrent = $summaryReportCurrent[$reportedRegion];
            $selectedRegionReportPrev = $summaryReportPrev[$reportedRegion];
            foreach ($selectedRegionReportCurrent as $locationCurrentReports) {
                foreach ($locationCurrentReports as $currentYear => $monthlyCurrentReports) {
                    foreach ($monthlyCurrentReports as $monthly => $currentReports) {
                        $totalNetSales += $currentReports['net'];
                        $totalGrossSales += $currentReports['gross'];
                        $totalTransactions += $currentReports['transaction'];
                        $aheadOrBehind[$currentYear][$monthly] += $currentReports['net'];
                    }
                }
            }
            foreach ($selectedRegionReportPrev as $prevLocationReports) {
                foreach ($prevLocationReports as $prevReports) {
                    $totalNetLastSales += $prevReports['net'];
                    $totalGrossLastSales += $prevReports['gross'];
                    $totalLastTransactions += $prevReports['transaction'];
                }
            }
        }
        $selectedLocationReport['current']['sale'] = convertLocationCurrency($regionConversionRate[$reportedRegion]['from'], $regionConversionRate[$reportedRegion]['to'], $totalNetSales);
        $selectedLocationReport['current']['gross'] = convertLocationCurrency($regionConversionRate[$reportedRegion]['from'], $regionConversionRate[$reportedRegion]['to'], $totalGrossSales);
        $selectedLocationReport['current']['transactions'] = $totalTransactions;
        $selectedLocationReport['last']['sale'] = convertLocationCurrency($prevYearConversionRate[$reportedRegion]['from'], $prevYearConversionRate[$reportedRegion]['to'], $totalNetLastSales);
        $selectedLocationReport['last']['gross'] = convertLocationCurrency($prevYearConversionRate[$reportedRegion]['from'], $prevYearConversionRate[$reportedRegion]['to'], $totalGrossLastSales);
        $selectedLocationReport['last']['transactions'] = $totalLastTransactions;

        foreach ($aheadOrBehind as $aYear => $aMonthReport) {
            foreach($aMonthReport as $aMonth => $aReport) {
                $aheadOrBehindReport[$aYear][$aMonth] = convertLocationCurrency($regionConversionRate[$reportedRegion]['from'], $regionConversionRate[$reportedRegion]['to'], $aReport);
            }
        }

        $netSalesCurrent = $selectedLocationReport['current']['sale'];
        $netSalesLast = $selectedLocationReport['last']['sale'];
        $transactionCurrent = $selectedLocationReport['current']['transactions'];
        $transactionLast = $selectedLocationReport['last']['transactions'];
        $curVsLastSales = ($netSalesCurrent - $netSalesLast) * 100/ $netSalesCurrent;
        $summaryResponse['summary']['netSales']['current'] = $netSalesCurrent;
        $summaryResponse['summary']['netSales']['last'] = $netSalesLast;
        $summaryResponse['summary']['netSales']['currentVsLast'] = round($curVsLastSales, 2);
        $curVsLastTransaction = ($transactionCurrent - $transactionLast) * 100/ $transactionCurrent;
        $summaryResponse['summary']['transactions']['current'] = $transactionCurrent;
        $summaryResponse['summary']['transactions']['last'] = $transactionLast;
        $summaryResponse['summary']['transactions']['currentVsLast'] = round($curVsLastTransaction, 2);
        $averageOrderCurrent = round($netSalesCurrent / $transactionCurrent, 2);
        $averageOrderLast = round($netSalesLast / $transactionLast, 2);
        $curVsLastAverageOrder = ($averageOrderCurrent - $averageOrderLast) * 100/ $averageOrderCurrent;
        $summaryResponse['summary']['averageOrder']['current'] = $averageOrderCurrent;
        $summaryResponse['summary']['averageOrder']['last'] = $averageOrderLast;
        $summaryResponse['summary']['averageOrder']['currentVsLast'] = round($curVsLastAverageOrder, 2);
        $summaryResponse['aheadOrBehind'] = $aheadOrBehindReport;
        $summaryResponse['zoneBoard'] = $zoneBoardReport;
        $summaryResponse['conversion'] = $regionConversionRate;
        return $summaryResponse;
    }

    function getAheadOrBehindReport($aheadOrBehind, $startDate, $endDate) {
        global $planRecord;
        $response = array();
        $start = strtotime($startDate);
        $end = strtotime($endDate);
        $startYear = date('Y', $start);
        $startMonth = date('m', $start);
        $endYear = date('Y', $end);
        $endMonth = date('m', $end);
        $filterPlanRecord  = array();
        $loop = 0;
        for($y = $startYear; $y <= $endYear; $y++) {
            if (isset($planRecord[$startYear]) && $y == $startYear && $loop == 0) {
                $month = ($startYear == $endYear) ? $endMonth : 12;
                for ($i = $startMonth; $i <= $month; $i++) {
                    $i = (int)$i;
                    $filterPlanRecord[$startYear][$i] = $planRecord[$startYear][$i];
                }
            }
            if (isset($planRecord[$y]) && $y != $startYear && $y != $endYear && $loop > 0) {
                for ($j = 1; $j <= 12; $j++) {
                    $filterPlanRecord[$y][$j] = $planRecord[$y][$j];
                }
            }
            if (isset($planRecord[$endYear]) && ($y == $endYear) && $loop > 0) {
                for ($l = 1; $l <= $endMonth; $l++) {
                    $filterPlanRecord[$endYear][$l] = $planRecord[$endYear][$l];
                }
            }
            $loop++;
        }

        $aheadOrBehindData = array();
        ksort($aheadOrBehind);
        foreach ($aheadOrBehind as $year => $monthSales) {
            ksort($monthSales);
            foreach ($monthSales as $month => $sales) {
                $monthlySales = array();
                $monthlySales['year'] = $year;
                $monthlySales['month'] = $month;
                $monthlySales['sales'] = $sales;
                $monthlySales['plan'] = calculatePlanNumbers($filterPlanRecord[$year][$month], $month, $year);
                $aheadOrBehindData[] = $monthlySales;
            }

        }
        $response['aheadOrBehind']['mtdNetRevenue'] =  $aheadOrBehindData;
        return $response;
    }

    function getNetRevenueReport($startDate, $endDate) {
        global $planRecord;
        global $beerVolume;
        global $locationsDatanames;
        $tsp = strtotime($endDate);
        if ($tsp > time()) {
            $tsp = time();
        }

        $year = date('Y', $tsp);
        $month = date('m', $tsp);
        $lastDay = date('t', $tsp);

        $quarter = ceil(date('n', $tsp)/3); // get the starting month of the current quarter

        //Start date is week day from Monday end week day Sunday on given end date
        $startWeekDate = date('N', $tsp)==1 ? date('Y-m-d H:i:s', $tsp) : date('Y-m-d H:i:s', strtotime('last monday', $tsp));
        $endWeekDate = date('N', $tsp)==7 ? date('Y-m-d', $tsp) : date('Y-m-d', strtotime('next sunday', $tsp)) . ' 23:59:59';

        //Start date is ist day of month and last day is current day of given month
        $startMonthDate = date('Y-m-d H:i:s', mktime(0, 0, 0, $month, 1, $year));
        $endMonthDate = date('Y-m-d H:i:s', mktime(23, 59, 59, $month, $lastDay, $year));

        //Start date is first day of given date of quarter and last date is current date of given date
        $startQuarterDate = date('Y-m-d', strtotime($year . '-' . (($quarter * 3) - 2) . '-1')) . ' 00:00:00';
        $endQuarterDate = date('Y-m-t', strtotime($year . '-' . (($quarter * 3)) . '-1')) . ' 23:59:59';

        // Start date is Ist Jan and Last date is 31st december
        $startYearDate = date('Y-m-d H:i:s', mktime(0, 0, 0, 1, 1, $year));
        $endYearDate = date('Y-m-d H:i:s', mktime(23, 59, 59, 12, 31, $year));

        $response = array();

        $weekly_sales_amount = 0;
        $quarterly_sales_amount = 0;
        $monthly_sales_amount = 0;
        $yearly_sales_amount = 0;

        //TODO:- Currently we have no beer category, Will comment out after getting live data
        /*
        $weekly_beer_volume = 0;
        $quarterly_beer_volume = 0;
        $monthly_beer_volume = 0;
        $yearly_beer_volume = 0;
        */

        $conversionFlag = true;
        foreach ($locationsDatanames as $dataName) {
            $rdb = "poslavu_" . $dataName . "_db";
            // Get conversion rate only one time for reducing query call for each dataname
            if ($conversionFlag) {
                $chainCountry = getReportCountry($rdb);
                list($conversionRateFrom, $conversionRateTo) = getConversionCurrency($chainCountry, $endDate);
                $conversionFlag = false;
            }
            $net_revenue_query = lavu_query("SELECT (orders.closed >= '". $startYearDate."' AND orders.closed <= '". $endYearDate . "') as year,  (orders.closed >= '" . $startQuarterDate . "' AND orders.closed <= '" . $endQuarterDate . "') as quarter, (orders.closed >= '" . $startMonthDate . "' AND orders.closed <= '" . $endMonthDate . "') as month, (orders.closed >= '" . $startWeekDate . "' AND orders.closed <= '" . $endWeekDate . "') as week, if(count(*) > 0, sum((order_contents.price + order_contents.modify_price + order_contents.forced_modifiers_price)*order_contents.quantity - if( order_contents.quantity > '0',if(order_contents.discount_amount<(order_contents.price + order_contents.modify_price + order_contents.forced_modifiers_price),order_contents.discount_amount,order_contents.price + order_contents.modify_price + order_contents.forced_modifiers_price)+ order_contents.idiscount_amount + order_contents.itax, 0)), '0.00') AS net from `[3]`.`orders` LEFT JOIN `[3]`.`order_contents` ON `orders`.`order_id` = `order_contents`.`order_id` AND `orders`.`location_id` = `order_contents`.`loc_id`  LEFT JOIN `[3]`.`menu_items` ON `order_contents`.`item_id` = `menu_items`.`id` WHERE orders.closed >= '[1]' AND orders.closed <= '[2]' AND orders.void = '0' AND orders.location_id='1' AND  order_contents.item != 'SENDPOINT' GROUP BY year, quarter, month, week", $startDate, $endDate, $rdb);
            while ($net_revenue_read = mysqli_fetch_assoc($net_revenue_query)) {
                if ($net_revenue_read['year'] == 1) {
                    $yearly_sales_amount += $net_revenue_read['net'];
                }
                if ($net_revenue_read['quarter'] == 1) {
                    $quarterly_sales_amount += $net_revenue_read['net'];
                }
                if ($net_revenue_read['month'] == 1) {
                    $monthly_sales_amount += $net_revenue_read['net'];
                }
                if ($net_revenue_read['week'] == 1) {
                    $weekly_sales_amount += $net_revenue_read['net'];
                }
            }

            //TODO:- Currently we have no beer category, Will comment out after getting live data
            /*
            $net_beer_volume_query = lavu_query("SELECT (orders.closed >= '". $startYearDate."' AND orders.closed <= '". $endYearDate . "') as year,  (orders.closed >= '" . $startQuarterDate . "' AND orders.closed <= '" . $endQuarterDate . "') as quarter, (orders.closed >= '" . $startMonthDate . "' AND orders.closed <= '" . $endMonthDate . "') as month, (orders.closed >= '" . $startWeekDate . "' AND orders.closed <= '" . $endWeekDate . "') as week, sum(order_contents.quantity) as quantity from `[3]`.`orders` LEFT JOIN `[3]`.`order_contents` ON `orders`.`order_id` = `order_contents`.`order_id` AND `[3]`.`orders`.`location_id` = `order_contents`.`loc_id`  LEFT JOIN `[3]`.`menu_items` ON `order_contents`.`item_id` = `menu_items`.`id` LEFT JOIN `menu_categories` ON `menu_items`.`category_id` = `menu_categories`.`id` WHERE orders.closed >= '[1]' AND orders.closed <= '[2]' AND menu_categories.name like '%beer' AND orders.void = '0' AND orders.location_id='1' AND  order_contents.item != 'SENDPOINT' GROUP BY year, quarter, month, week", $startYearDate, $endYearDate, $rdb);
            while ($net_beer_volume_read = mysqli_fetch_assoc($net_beer_volume_query)) {
                if ($net_beer_volume_read['year'] == 1) {
                    $yearly_beer_volume += $net_beer_volume_read['quantity'];
                }
                if ($net_beer_volume_read['quarter'] == 1) {
                    $quarterly_beer_volume += $net_beer_volume_read['quantity'];
                }
                if ($net_beer_volume_read['month'] == 1) {
                    $monthly_beer_volume += $net_beer_volume_read['quantity'];
                }
                if ($net_beer_volume_read['week'] == 1) {
                    $weekly_beer_volume += $net_beer_volume_read['quantity'];
                }
            }*/
        }
        $weekly_conv_sales_amount = convertLocationCurrency($conversionRateFrom, $conversionRateTo, $weekly_sales_amount);
        $monthly_conv_sales_amount = convertLocationCurrency($conversionRateFrom, $conversionRateTo, $monthly_sales_amount);
        $quarterly_conv_sales_amount = convertLocationCurrency($conversionRateFrom, $conversionRateTo, $quarterly_sales_amount);
        $yearly_conv_sales_amount = convertLocationCurrency($conversionRateFrom, $conversionRateTo, $yearly_sales_amount);

        //TODO:- Currently we have no beer category, Will comment out after getting live data
        /*
        $weekly_conv_beer_volume = $weekly_beer_volume;
        $quarterly_conv_beer_volume = $quarterly_beer_volume;
        $monthly_conv_beer_volume = $monthly_beer_volume;
        $yearly_conv_beer_volume =  $yearly_beer_volume;
        */

        $weekly_expected_sales_amount = round(($planRecord[$year][(int)$month] / $lastDay) * 7, 2);
        $monthly_expected_sales_amount = $planRecord[$year][(int)$month];
        $quarterly_expected_sales_amount = $planRecord[$year]['Q'.$quarter];
        $yearly_expected_sales_amount = $planRecord[$year]['FY'];

        $weekly_expected_beer_volume =  round(($beerVolume[$year][(int)$month] / $lastDay) * 7, 2);
        $monthly_expected_beer_volume = $beerVolume[$year][(int)$month];
        $quarterly_expected_beer_volume = $beerVolume[$year]['Q'.$quarter];
        $yearly_expected_beer_volume = $beerVolume[$year]['FY'];

        $response['netRevenue'][0]= array('dateRange' => 'week', 'actual' => $weekly_conv_sales_amount, 'expected' => $weekly_expected_sales_amount);
        $response['netRevenue'][1]= array('dateRange' => 'month', 'actual' => $monthly_conv_sales_amount, 'expected' => $monthly_expected_sales_amount);
        $response['netRevenue'][2]= array('dateRange' => 'quarter', 'actual' => $quarterly_conv_sales_amount, 'expected' => $quarterly_expected_sales_amount);
        $response['netRevenue'][3]= array('dateRange' => 'year', 'actual' => $yearly_conv_sales_amount, 'expected' => $yearly_expected_sales_amount);

        //TODO:- Currently we have no beer category, Will comment out after getting live data
        /*
        $response['beerVolume'][0]= array('dateRange' => 'week', 'actual' => $weekly_beer_volume, 'expected' => $weekly_expected_beer_volume);
        $response['beerVolume'][1]= array('dateRange' => 'month', 'actual' => $monthly_conv_beer_volume, 'expected' => $monthly_expected_beer_volume);
        $response['beerVolume'][2]= array('dateRange' => 'quarter', 'actual' => $quarterly_conv_beer_volume, 'expected' => $quarterly_expected_beer_volume);
        $response['beerVolume'][3]= array('dateRange' => 'year', 'actual' => $yearly_conv_beer_volume, 'expected' => $yearly_expected_beer_volume);
        */
        //TODO:- Removed this dummy beer volume data after getting real data
        $response['beerVolume'][0]= array('dateRange' => 'week', 'actual' => 0, 'expected' => $weekly_expected_beer_volume);
        $response['beerVolume'][1]= array('dateRange' => 'month', 'actual' => 0, 'expected' => $monthly_expected_beer_volume);
        $response['beerVolume'][2]= array('dateRange' => 'quarter', 'actual' => 0, 'expected' => $quarterly_expected_beer_volume);
        $response['beerVolume'][3]= array('dateRange' => 'year', 'actual' => 0, 'expected' => $yearly_expected_beer_volume);

        return $response;
    }

    function getZoneScoreBoard ($zoneBoardReports, $regionConversion, $startDate, $endDate) {
        global $reportRegions, $staticPlanRecord, $datanameCompanyNameMap;

        $response = array();
        $start = strtotime($startDate);
        $end = strtotime($endDate);
        $startYear = date('Y', $start);
        $startMonth = date('m', $start);
        $endYear = date('Y', $end);
        $endMonth = date('m', $end);
        $filterPlanRecord  = array();
        foreach($reportRegions as $region) {
            $loop = 0;
            $planRecord = $staticPlanRecord[$region];
            for ($y = $startYear; $y <= $endYear; $y++) {
                if (isset($planRecord[$startYear]) && $y == $startYear && $loop == 0) {
                    $month = ($startYear == $endYear) ? $endMonth : 12;
                    for ($i = $startMonth; $i <= $month; $i++) {
                        $i = (int)$i;
                        $filterPlanRecord[$region] += calculatePlanNumbers($planRecord[$startYear][$i], $i, $y);
                    }
                }
                if (isset($planRecord[$y]) && $y != $startYear && $y != $endYear && $loop > 0) {
                    for ($j = 1; $j <= 12; $j++) {
                        $filterPlanRecord[$region] += calculatePlanNumbers($planRecord[$y][$j], $j, $y);
                    }
                }
                if (isset($planRecord[$endYear]) && ($y == $endYear) && $loop > 0) {
                    for ($l = 1; $l <= $endMonth; $l++) {
                        $filterPlanRecord[$region] += calculatePlanNumbers($planRecord[$endYear][$l], $l, $y);
                    }
                }
                $loop++;
            }
        }

        $zoneLocationData = array();
        $paceRegionSales = array();
        if (isset($zoneBoardReports['currentLocation'])) {
            unset($zoneBoardReports['currentLocation']);
        }
        foreach ($zoneBoardReports as $region => $locationsData) {
            foreach ($locationsData as $location => $data) {
                $locData = convertLocationCurrency($regionConversion[$region]['from'], $regionConversion[$region]['to'], $data);
                $zoneLocationData[$location] += $locData;
                $paceRegionSales[$region] += $locData;

            }
        }

        ksort($paceRegionSales);
        foreach ($paceRegionSales as $region => $regionData) {
            $zonesdata['name'] = $region;
            $zonesdata['sales'] = $regionData;
            if ($filterPlanRecord[$region] > 0) {
                $zPlan = round(($regionData * 100) / $filterPlanRecord[$region], 2);
                $zplan = ($zPlan == 0) ? round(($regionData * 100) / $filterPlanRecord[$region], 4) : $zPlan;
            } else {
                $zplan = 100;
            }
            $zonesdata['percentageOfPlan'] = $zplan;
            $response['pace']['zones'][] = $zonesdata;
        }

        //TODO:- Removed below line when will get actual location wise plan
        $locationWisePlan = 0;
        foreach ($zoneLocationData as $loc => $ldata) {
            $locationsdata = array();
            $companyName = $datanameCompanyNameMap[$loc]['name'] . " (#".($datanameCompanyNameMap[$loc]['id'] + 100000) . ")";
            $locationsdata['name'] = $companyName;
            $locationsdata['sales'] = $ldata;
            if ($locationWisePlan > 0) {
                $pPlan = round(($ldata * 100) / $locationWisePlan, 2);
                $pPlan = ($pPlan == 0) ? round(($ldata * 100) / $locationWisePlan, 4) : $pPlan;
            } else {
                $pPlan = 100;
            }
            $locationsdata['percentageOfPlan'] = $pPlan;
            $response['pace']['locations'][] = $locationsdata;
        }
        return $response;
    }

    function getDashboardReport($startDate, $endDate, $lastStartDate, $lastEndDate) {
        $summaryResponse = getSummaryReport($startDate, $endDate, $lastStartDate, $lastEndDate);
        $AheadOrBehindReportResponse = getAheadOrBehindReport($summaryResponse['aheadOrBehind'], $startDate, $endDate);
        $netRevenueReportResponse = getNetRevenueReport($startDate, $endDate);
        $zoneBoardReportResponse = getZoneScoreBoard($summaryResponse['zoneBoard'], $summaryResponse['conversion'], $startDate, $endDate);
        $summaryReportResponse['summary'] = $summaryResponse['summary'];
        $response = $summaryReportResponse + $AheadOrBehindReportResponse + $netRevenueReportResponse + $zoneBoardReportResponse;
        return $response;
    }

    function getSalesAndPaymentReport($startDate, $endDate) {
        $netSalesPlanReportResponse = getNetSalesPlanReport($startDate, $endDate);
        $getSalesByPaymentReport = getSalesByPaymentReport($startDate, $endDate);

        /*
        $AheadOrBehindReportResponse = getAheadOrBehindReport($startDate, $endDate);
        $netRevenueReportResponse = getNetRevenueReport($startDate, $endDate);
        $zoneBoardReportResponse = getZoneScoreBoard($startDate, $endDate);
        $response = $summaryReportResponse + $AheadOrBehindReportResponse + $netRevenueReportResponse + $zoneBoardReportResponse;
        */
        $response = $netSalesPlanReportResponse + $getSalesByPaymentReport;
        return $response;
    }

    function getNetSalesPlanReport($startDate, $endDate) {
        global $locationsDatanames;
        $response = array();
        $salesVsPlan = array();
        $conversionFlag = true;
        $timeDayReports = array();
        $superGroupReports = array();
        foreach ($locationsDatanames as $dataName) {
            $rdb = "poslavu_" . $dataName . "_db";
            // Get conversion rate only one time for reducing query call for each dataname
            if ($conversionFlag) {
                $chainCountry = getReportCountry($rdb);
                list($conversionRateFrom, $conversionRateTo) = getConversionCurrency($chainCountry, $endDate);
                $conversionFlag = false;
            }
            $sale_query = lavu_query("select super_groups.title as super_group, DATE_FORMAT(`orders`.`closed`, '%w') as day, FROM_UNIXTIME(FLOOR(UNIX_TIMESTAMP(`orders`.`closed`)/(30*60))*(30*60), '%H:%i') AS hrm,  if(count(*) > 0, sum((order_contents.price + order_contents.modify_price + order_contents.forced_modifiers_price)*order_contents.quantity - if( order_contents.quantity > '0',if(order_contents.discount_amount<(order_contents.price + order_contents.modify_price + order_contents.forced_modifiers_price),order_contents.discount_amount,order_contents.price + order_contents.modify_price + order_contents.forced_modifiers_price)+ order_contents.idiscount_amount + order_contents.itax, 0)), '0.00') as net from `[3]`.`orders` LEFT JOIN `[3]`.`order_contents` ON `orders`.`order_id` = `order_contents`.`order_id` AND `orders`.`location_id` = `order_contents`.`loc_id`  LEFT JOIN `[3]`.`menu_items` ON `order_contents`.`item_id` = `menu_items`.`id` LEFT JOIN `[3]`.`menu_categories` ON `menu_items`.`category_id` = `menu_categories`.`id` LEFT JOIN `[3]`.`super_groups` ON IF (menu_items.super_group_id = '', menu_categories.super_group_id, menu_items.super_group_id) = `super_groups`.`id` where orders.closed >= '[1]' and orders.closed < '[2]' and orders.void = '0' and orders.location_id='1' and  order_contents.item != 'SENDPOINT' GROUP BY super_group, day, hrm", $startDate, $endDate, $rdb);
            while ($sales_read = mysqli_fetch_assoc($sale_query)) {
                $salesVsPlan[$sales_read['day']] += $sales_read['net'];
                $timeDayReports[$sales_read['hrm']] += $sales_read['net'];
                $superGroup = ($sales_read['super_group']) ? $sales_read['super_group'] : 'Other';
                $superGroupReports[$superGroup] += $sales_read['net'];
            }
        }
        $days = array('1' => 'M', '2' => 'T', '3' => 'W', '4' => 'TH', '5' => 'F', '6' => 'S', '0' => 'SU');
        $salesReport = array();
        foreach($days as $key => $day) {
            $netSale['sales'] = isset($salesVsPlan[$key]) ? convertLocationCurrency($conversionRateFrom, $conversionRateTo, $salesVsPlan[$key]) : 0;
            $netSale['day'] = $day;
            $netSale['plan'] = 0;
            $salesReport[] = $netSale;
        }
        $response['netSales'] = $salesReport;

        ksort($timeDayReports, SORT_FLAG_CASE);
        foreach($timeDayReports as $time => $sale) {
            $timeDayReport['sales'] = convertLocationCurrency($conversionRateFrom, $conversionRateTo, $sale);
            $timeDayReport['time'] = $time;
            $timeDayReport['plan'] = '+ 100%';
            $response['timeOfDay'][] = $timeDayReport;
        }

        foreach ($superGroupReports as $superGroup => $sale) {
            $categoryData['data'] = convertLocationCurrency($conversionRateFrom, $conversionRateTo, $sale);
            $categoryData['category'] = $superGroup;
            $response['saleCategory']['sales'][] = $categoryData;

        }
        return $response;
    }

    function getSalesByPaymentReport($startDate, $endDate) {
        global $locationsDatanames;
        $response = array();
        $payTypeReports = array();
        $conversionFlag = true;
        $totalNetSale = 0;
        foreach ($locationsDatanames as $dataName) {
            $rdb = "poslavu_" . $dataName . "_db";
            // Get conversion rate only one time for reducing query call for each dataname
            if ($conversionFlag) {
                $chainCountry = getReportCountry($rdb);
                list($conversionRateFrom, $conversionRateTo) = getConversionCurrency($chainCountry, $endDate);
                $conversionFlag = false;
            }
            $sales_by_payment_query = lavu_query("select concat( cc_transactions.pay_type, concat( ' ', cc_transactions.card_type)) as pay_type,  if(count(*) > 0, sum(cc_transactions.amount), '0.00') as net, if(count(*) > 0, sum(cc_transactions.tip_amount), '0.00') as tip_amount, if(count(*) > 0, sum(cc_transactions.total_collected), '0.00') as total_collected from `[3]`.`cc_transactions`LEFT JOIN `[3]`.`orders` ON `cc_transactions`.`order_id` = `orders`.`order_id` AND `cc_transactions`.`loc_id` = `orders`.`location_id` where orders.closed < '[2]' and orders.closed != '0000-00-00 00:00:00' and orders.closed != '' and orders.closed != '0' and orders.void = '0' and cc_transactions.datetime >= '[1]' and cc_transactions.datetime <= '[2]' and cc_transactions.voided = '0' and cc_transactions.action != 'Void' and cc_transactions.action NOT LIKE '%issue%' and cc_transactions.action NOT LIKE '%reload%' and cc_transactions.process_data NOT LIKE 'LOCT%' and cc_transactions.action != '' and cc_transactions.order_id not like 'Paid%' and cc_transactions.action = 'Sale' and cc_transactions.order_id NOT LIKE '777%' and cc_transactions.loc_id = '1' group by concat( cc_transactions.pay_type, concat( ' ', cc_transactions.card_type))", $startDate, $endDate, $rdb);
            while ($sales_by_payment_read = mysqli_fetch_assoc($sales_by_payment_query)) {
                $payType = isset($sales_by_payment_read['pay_type']) ? $sales_by_payment_read['pay_type'] : 'Other';
                $netSale = $sales_by_payment_read['net'];
                $payTypeReports[$payType]['sale'] += $netSale;
                $payTypeReports[$payType]['tip_amount'] += $sales_by_payment_read['tip_amount'];
                $payTypeReports[$payType]['total_collected'] += $sales_by_payment_read['total_collected'];
                $totalNetSale += $netSale;
            }
        }
        $totalNetSale = convertLocationCurrency($conversionRateFrom, $conversionRateTo, $totalNetSale);
        foreach ($payTypeReports as $type => $salesData) {
            $reports['type'] = $type;
            $reports['amount'] = convertLocationCurrency($conversionRateFrom, $conversionRateTo, $salesData['sale']);
            $reports['tips'] = convertLocationCurrency($conversionRateFrom, $conversionRateTo, $salesData['tip_amount']);
            $reports['total'] = convertLocationCurrency($conversionRateFrom, $conversionRateTo, $salesData['total_collected']);
            $totalSales = round(($reports['amount'] * 100) / $totalNetSale, 2);
            $totalSales = ($totalSales == 0) ? round(($reports['amount'] * 100) / $totalNetSale, 4) : $totalSales;
            $reports['totalSales'] = $totalSales;
            $response['payType'][] = $reports;

        }
        return $response;
    }

    function getProductInventoryReport($startDate, $endDate, $limit) {
        global $locationsDatanames;
        $menuItemsSales = array();
        $limit = ($limit > 0) ? $limit : 10;
        $beers_volume = array();
        $conversionFlag = true;
        foreach ($locationsDatanames as $dataName) {
            $rdb = "poslavu_" . $dataName . "_db";
            // Get conversion rate only one time for reducing query call
            if ($conversionFlag) {
                $chainCountry = getReportCountry($rdb);
                list($conversionRateFrom, $conversionRateTo) = getConversionCurrency($chainCountry, $endDate);
                $conversionFlag = false;
            }

            $items_sales_query = lavu_query("select `menu_items`.`name` as item, `menu_items`.`id`, if(count(distinct orders.ioid )>count( distinct orders.order_id ),count( distinct orders.ioid ),count( distinct orders.order_id )) as order_count, if(count(*) > 0, sum((order_contents.price + order_contents.modify_price + order_contents.forced_modifiers_price)*order_contents.quantity - if( order_contents.quantity > '0',if(order_contents.discount_amount<(order_contents.price + order_contents.modify_price + order_contents.forced_modifiers_price),order_contents.discount_amount,order_contents.price + order_contents.modify_price + order_contents.forced_modifiers_price)+ order_contents.idiscount_amount + order_contents.itax, 0)), '0.00') as net from `[3]`.`orders` LEFT JOIN `[3]`.`order_contents` ON `orders`.`order_id` = `order_contents`.`order_id` AND `orders`.`location_id` = `order_contents`.`loc_id`  LEFT JOIN `[3]`.`menu_items` ON `order_contents`.`item_id` = `menu_items`.`id` WHERE orders.closed >= '[1]' AND orders.closed < '[2]' AND orders.void = '0' AND orders.location_id='1' AND  order_contents.item != 'SENDPOINT' GROUP BY item ORDER BY net DESC LIMIT ".$limit, $startDate, $endDate, $rdb);
            while ($items_sales_read = mysqli_fetch_assoc($items_sales_query)) {
                $menuItemsSales[$items_sales_read['item']] += $items_sales_read['net'];
            }
            //TODO:- Currently we have no beer category, Will comment out after getting live data
/*
            $beer_volume_query = lavu_query("SELECT `menu_items`.name as item, sum(order_contents.quantity) as quantity from `[3]`.`orders` LEFT JOIN `[3]`.`order_contents` ON `orders`.`order_id` = `order_contents`.`order_id` AND `[3]`.`orders`.`location_id` = `order_contents`.`loc_id`  LEFT JOIN `[3]`.`menu_items` ON `order_contents`.`item_id` = `menu_items`.`id` LEFT JOIN `[3]`.`menu_categories` ON `menu_items`.`category_id` = `menu_categories`.`id` WHERE orders.closed >= '[1]' AND orders.closed <= '[2]' AND menu_categories.name like '%beer' AND orders.void = '0' AND orders.location_id='1' AND  order_contents.item != 'SENDPOINT' GROUP BY `menu_items`.`name` ORDER BY quantity DESC LIMIT ".$limit, $startDate, $endDate, $rdb);
            while ($beer_volume_read = mysqli_fetch_assoc($beer_volume_query)) {
                $beers_volume[$beer_volume_read['name']] += $beer_volume_read['quantity'];
            }
*/
        }

        //TODO:- Currently we have no beer category, Will remove after getting live data
        $beers_volume = array("beer1"=>25, "beer2"=>30, "beer3"=>15);
        //Get top menu items
        arsort($menuItemsSales);
        $topSalesItems = array_slice($menuItemsSales, 0, $limit);
        $totalSales = convertLocationCurrency($conversionRateFrom, $conversionRateTo, array_sum($topSalesItems));

        //TODO:- uncomment it if we get live beer volume
        /*
        //Get top beers volume
        arsort($beers_volume);
        $topSalesBeers = array_slice($beers_volume, 0, $limit);
        */

        $response = array();
        $productMixSales = array();
        foreach ($topSalesItems as $item => $net) {
            $data['name'] = $item;
            $convNet = convertLocationCurrency($conversionRateFrom, $conversionRateTo, $net);
            $avgSales = round($convNet * 100 / $totalSales, 2);
            $avgSales = ($avgSales == 0) ? round($convNet * 100 / $totalSales, 4) : $avgSales;
            $data['value'] = $avgSales;
            $percentageSales[] = $data;
            $netData['name'] = $item;
            $netData['value'] = round($convNet, 2);
            $sales[] = $netData;
        }

        $beerVolume = array();
        //TODO:- Remove this if will get real beer and target volume
        $topSalesBeers = array("beer1"=>20, "beer2"=>30, "beer3"=>10);
        $topSalesBeersTarget = array("target"=>30,"target"=>25,"target"=>50);
        foreach ($topSalesBeers as $beer => $bVolume) {
            $bData['name'] = $beer;
            $bData['volume'] = $bVolume;
            $bData['target'] = $topSalesBeersTarget['target'];
            $beerVolume[] = $bData;
        }
        $productMixSales['percentageSales'] = $percentageSales;
        $productMixSales['sales'] = $sales;
        $response['productMix'] = $productMixSales;
        $response['beerVolumeVsTarget'] = $beerVolume;
        return $response;
    }

    function getTrendsViewReport($startDate, $endDate, $lastStartDate, $lastEndDate, $limit) {
        global $locationsDatanames;
        $response = array();
        $menuItemsCurrentSales = array();
        $menuItemsLastSales = array();
        $menuItemsMonthCurrentSales = array();
        $menuItemsMonthLastSales = array();
        $limit = ($limit > 0) ? $limit : 10;
        $conversionFlag = true;
        foreach ($locationsDatanames as $dataName) {
            $rdb = "poslavu_" . $dataName . "_db";
            // Get conversion rate only one time for reducing query call
            if ($conversionFlag) {
                $chainCountry = getReportCountry($rdb);
                list($conversionRateFrom, $conversionRateTo) = getConversionCurrency($chainCountry, $endDate);
                $conversionFlag = false;
            }

            $items_sales_query = lavu_query("select `menu_items`.`name` as item, `menu_items`.`id`, DATE_FORMAT(orders.closed, '%c') AS month, if(count(*) > 0, count( distinct orders.ioid ),count( distinct orders.order_id )) as order_count, if(count(*) > 0, sum((order_contents.price + order_contents.modify_price + order_contents.forced_modifiers_price)*order_contents.quantity - if( order_contents.quantity > '0',if(order_contents.discount_amount<(order_contents.price + order_contents.modify_price + order_contents.forced_modifiers_price),order_contents.discount_amount,order_contents.price + order_contents.modify_price + order_contents.forced_modifiers_price)+ order_contents.idiscount_amount + order_contents.itax, 0)), '0.00') as net from `[3]`.`orders` LEFT JOIN `[3]`.`order_contents` ON `orders`.`order_id` = `order_contents`.`order_id` AND `orders`.`location_id` = `order_contents`.`loc_id`  LEFT JOIN `[3]`.`menu_items` ON `order_contents`.`item_id` = `menu_items`.`id` WHERE orders.closed >= '[1]' AND orders.closed < '[2]' AND orders.void = '0' AND orders.location_id='1' AND `menu_items`.`_deleted` != 1 AND order_contents.item != 'SENDPOINT' GROUP BY item, month ORDER BY net DESC", $startDate, $endDate, $rdb);
            while ($items_sales_read = mysqli_fetch_assoc($items_sales_query)) {
                $menuItemsCurrentSales[$items_sales_read['item']] += $items_sales_read['net'];
                $menuItemsMonthCurrentSales[$items_sales_read['month']]['net'] += $items_sales_read['net'];
                $menuItemsMonthCurrentSales[$items_sales_read['month']]['transaction'] += $items_sales_read['order_count'];
            }

            $items_last_sales_query = lavu_query("select `menu_items`.`name` as item, `menu_items`.`id`, DATE_FORMAT(orders.closed, '%c') AS month, if(count(*) > 0, count( distinct orders.ioid ),count( distinct orders.order_id )) as order_count, if(count(*) > 0, sum((order_contents.price + order_contents.modify_price + order_contents.forced_modifiers_price)*order_contents.quantity - if( order_contents.quantity > '0',if(order_contents.discount_amount<(order_contents.price + order_contents.modify_price + order_contents.forced_modifiers_price),order_contents.discount_amount,order_contents.price + order_contents.modify_price + order_contents.forced_modifiers_price)+ order_contents.idiscount_amount + order_contents.itax, 0)), '0.00') as net from `[3]`.`orders` LEFT JOIN `[3]`.`order_contents` ON `orders`.`order_id` = `order_contents`.`order_id` AND `orders`.`location_id` = `order_contents`.`loc_id`  LEFT JOIN `[3]`.`menu_items` ON `order_contents`.`item_id` = `menu_items`.`id` WHERE orders.closed >= '[1]' AND orders.closed < '[2]' AND orders.void = '0' AND orders.location_id='1' AND `menu_items`.`_deleted` != 1 AND order_contents.item != 'SENDPOINT' GROUP BY item, month ORDER BY net", $lastStartDate, $lastEndDate, $rdb);
            while ($items_last_sales_read = mysqli_fetch_assoc($items_last_sales_query)) {
                $menuItemsLastSales[$items_last_sales_read['item']] += $items_last_sales_read['net'];
                $menuItemsMonthLastSales[$items_last_sales_read['month']]['net'] += $items_last_sales_read['net'];
                $menuItemsMonthLastSales[$items_last_sales_read['month']]['transaction'] += $items_last_sales_read['order_count'];
            }
        }

        //Get top current menu items
        arsort($menuItemsCurrentSales);
        $topCurrentSalesItems = array_slice($menuItemsCurrentSales, 0, $limit);
        //$totalCurrentSales = array_sum($topCurrentSalesItems);

        $progressSalesItem = array();
        $progressComparison = array();
        foreach ($topCurrentSalesItems as $item => $itemSales) {
            $progressSalesItem['name'] = $item;
            $progressSalesItem['current'] = convertLocationCurrency($conversionRateFrom, $conversionRateTo, $itemSales);
            $progressSalesItem['previous'] = isset($menuItemsLastSales[$item]) ? convertLocationCurrency($conversionRateFrom, $conversionRateTo, $menuItemsLastSales[$item]) : 0;
            $progressComparison[] = $progressSalesItem;
        }

        //Get top last menu items
        arsort($menuItemsLastSales);
        $topLastSalesItems = array_slice($menuItemsLastSales, 0, $limit);
        $totalLastSales = convertLocationCurrency($conversionRateFrom, $conversionRateTo, array_sum($topLastSalesItems));

        $percentageSales = array();
        foreach ($topLastSalesItems as $item => $netLastSales) {
            $data['name'] = $item;
            $convNetSales = convertLocationCurrency($conversionRateFrom, $conversionRateTo, $netLastSales);
            $data['value'] = round($convNetSales * 100 / $totalLastSales, 2);
            $percentageSales[] = $data;
        }
        $netSales = array();
        $averageSales = array();
        $transactions = array();
        for ($i = 1; $i <= 12; $i++) {
            $monthNetSales['month'] = $i;
            $monthCurrentNetSale = isset($menuItemsMonthCurrentSales[$i]) ? convertLocationCurrency($conversionRateFrom, $conversionRateTo, $menuItemsMonthCurrentSales[$i]['net']) : 0;
            $monthNetSales['current'] = $monthCurrentNetSale;
            $monthLastNetSale = isset($menuItemsMonthLastSales[$i]) ? convertLocationCurrency($conversionRateFrom, $conversionRateTo,  $menuItemsMonthLastSales[$i]['net']) : 0;
            $monthNetSales['last'] = $monthLastNetSale;
            $netSales[] = $monthNetSales;
            $monthAverageSales['month'] = $i;
            $currentTransaction = ($menuItemsMonthCurrentSales[$i]['transaction'] > 0 ) ? $menuItemsMonthCurrentSales[$i]['transaction'] : 0;
            $monthAverageSales['current'] = ($currentTransaction > 0) ? round($monthCurrentNetSale / $currentTransaction, 2) : 0;
            $lastTransaction = ($menuItemsMonthLastSales[$i]['transaction'] > 0 ) ? $menuItemsMonthLastSales[$i]['transaction'] : 0;
            $monthAverageSales['last'] = ($lastTransaction > 0) ? round($monthLastNetSale / $lastTransaction, 2) : 0;
            $averageSales[] = $monthAverageSales;
            $monthTransactions['month'] = $i;
            $monthTransactions['current'] = $currentTransaction;
            $monthTransactions['last'] = $lastTransaction;
            $transactions[] = $monthTransactions;
        }

        $salestrends['progressComparison'] = $progressComparison;
        $salestrends['topTen'] = $percentageSales;
        $salestrends['saleTrends']['netSales'] = $netSales;
        $salestrends['saleTrends']['averageSales'] = $averageSales;
        $salestrends['saleTrends']['transactions'] = $transactions;
        $response['trends'] = $salestrends;
        return $response;
    }
