<?php
	if($in_lavu)
	{
		$request_string = "";
		$closed_orders_search_fields = array("closed_orders_server", "closed_orders_table", "closed_orders_date_from", "closed_orders_date_to");
		foreach($closed_orders_search_fields as $field) {
			$request_string .= $field."=\" + escape(document.closed_orders_search.".$field.".value) + \"&";
		}

		$closed_orders_location = $locationid;
		$closed_orders_server = (isset($_GET['closed_orders_server']))?$_GET['closed_orders_server']:0;
		$closed_orders_table = (isset($_GET['closed_orders_table']))?$_GET['closed_orders_table']:"";
		$closed_orders_date_from = (isset($_GET['closed_orders_date_from']))?$_GET['closed_orders_date_from']:date("Y-m-d", (time() - 86400));
		$closed_orders_date_to = (isset($_GET['closed_orders_date_to']))?$_GET['closed_orders_date_to']:date("Y-m-d", time());
	
		$filter_code = "";
		if ($closed_orders_location != 0) {
			$filter_code .= " AND location_id = '".$closed_orders_location."'";
		}
		if ($closed_orders_server != 0) {
			$filter_code .= " AND server_id = '".$closed_orders_server."'";
		}
		if ($closed_orders_table != "") {
			$filter_code .= " AND tablename = '".$closed_orders_table."'";
		}
		if ($closed_orders_date_from != "") {
			$filter_code .= " AND opened >= '".$closed_orders_date_from." 00:00:00'";
			//$filter_code .= " AND closed >= '".$closed_orders_date_from." 00:00:00'";
		}
		if ($closed_orders_date_to != "") {
			$filter_code .= " AND opened >= '".$closed_orders_date_from." 00:00:00'";
			//$filter_code .= " AND closed <= '".$closed_orders_date_to." 23:59:59'";
		}
		
		$display .= "
		<tr>
			<td align='center'>
				<table cellspacing='0' cellpadding='0' width='810px'>
					<tr><td class='inner_panel_top'></td></tr>
					<tr>
						<td class='inner_panel_mid' align='center' style='padding:10px 15px 15px 15px'>
							<form name='closed_orders_search'>
								<table cellspacing='0' cellpadding='5'>
									<tr><td align='center'><b>Search Criteria</b></td></tr>
									<tr>
										<td align='center'>
											<table cellspacing='0' cellpadding='5'>
												<tr>
													<td align='right' style='padding:5px 5px 5px 20px'>Server:</td>
													<td align='left'>
														<select name='closed_orders_server'>";
		$get_servers = lavu_query("SELECT * FROM users ORDER BY f_name, l_name ASC");
		if (@mysqli_num_rows($get_servers) == 1) {
			$extract = mysqli_fetch_array($get_servers);
			$display .= "<option value='".$extract['id']."'>".$extract['f_name']." ".$extract['l_name']."</option>";
		} else if (@mysqli_num_rows($get_servers) > 1) {
			if ($closed_orders_server == 0) {
				$selected = " selected";
			} else {
				$selected = "";
			}
			$display .= "<option value='0'".$selected.">All Servers</option>";
			while ($extract = mysqli_fetch_array($get_servers)) {
				if ($extract['id'] == $closed_orders_server) {
					$selected = " selected";
				} else {
					$selected = "";
				}
				$display .= "<option value='".$extract['id']."'".$selected.">".$extract['f_name']." ".$extract['l_name']."</option>";
			}
		} else {
			$display .= "<option value='0' selected>Error loading servers</option>";
		}
		$display .= "</select>
													</td>
													<td align='right' style='padding:5px 5px 5px 20px'>Table:</td>
													<td align='left'><input type='text' name='closed_orders_table' size='10' value='".$closed_orders_table."'></td>
												</tr>
											</table>
											<input type='hidden' name='mode' value='3'>
										</td>
									</tr>
									<tr>
										<td align='center'>
											<table cellspacing='0' cellpadding='5'>
												<tr>
													<td align='right'>From date:</td>
													<td align='left'><input type='text' name='closed_orders_date_from' size='11' value='".$closed_orders_date_from."' onFocus='scwShow(this,event);' onClick='scwShow(this,event);'></td>
													<td align='right' style='padding:5px 5px 5px 20px'>To date:</td>
													<td align='left'><input type='text' name='closed_orders_date_to' size='11' value='".$closed_orders_date_to."' onFocus='scwShow(this,event);' onClick='scwShow(this,event);'></td>
												</tr>
											</table>
										</td>
									</tr>
									<tr><td align='center'><input type='button' onclick='window.location = \"index.php?mode=$mode&$request_string\";' value='Search'></td></tr>
								</table>
							</form>
						</td>
					</tr>
					<tr><td class='inner_panel_bottom'></td></tr>
				</table>
			</td>
		</tr>
		<tr><td style='padding:15px 15px 15px 15px'>&nbsp;</td></tr>";

		$get_orders = lavu_query("SELECT * FROM orders WHERE closed != '0000-00-00 00:00:00'".$filter_code." AND void != '1' ORDER BY closed DESC");
		if (@mysqli_num_rows($get_orders) > 0) {
			$rows = mysqli_num_rows($get_orders);
			$row = 1;
			$bottom_border = " border-bottom:1px solid #999999;";
			$display .= "<tr>
				<td align='center'>
					<table cellspacing='0' cellpadding='0' width='900px'>
						<tr><td class='inner_panel_lg_top'></td></tr>
						<tr>
							<td class='inner_panel_lg_mid' align='center' style='padding:15px 15px 15px 15px'>
								<table cellspacing='1' cellpadding='5'>
									<tr class='order_list_header'><td align='center'><b>ID</b></td><td align='center'><b>Location</b></td><td align='center'><b>Opened</b></td><td align='center'><b>Closed</b></td><td align='center'><b>Server</b></td><td align='center'><b>Cashier</b></td><td align='center'><b>Table</b></td><td align='center'><b>Subtotal</b></td><td align='center'><b>Tax</b></td><td align='center'><b>Total</b></td></tr>";
			while ($extract = mysqli_fetch_array($get_orders)) {
				if ($row == $rows) {
					$bottom_border = "";
				}
				$display .= "<tr class='order_list'><td align='right' style='padding:5px 10px 5px 15px;".$bottom_border."'><a onclick='document.getElementById(\"order_contents_".$extract['id']."\").style.display = \"inline\"' class='location_link' title='Click to view order contents...'>".str_pad($extract['order_id'], 8, "0", STR_PAD_LEFT)."</td><td align='left' style='padding:5px 20px 5px 5px;".$bottom_border."'>".$extract['location']."</a></td><td align='center' style='padding:5px 15px 5px 15px;".$bottom_border."'>".$extract['opened']."</td><td align='center' style='padding:5px 15px 5px 15px;".$bottom_border."'>".$extract['closed']."</td><td align='left' style='padding:5px 20px 5px 15px;".$bottom_border."'>".$extract['server']."</td><td align='left' style='padding:5px 20px 5px 15px;".$bottom_border."'>".$extract['cashier']."</td><td align='left' style='padding:5px 20px 5px 15px;".$bottom_border."'>".$extract['tablename']."</td><td align='right' style='padding:5px 15px 5px 15px;".$bottom_border."'>$".number_format($extract['subtotal'], 2)."</td><td align='right' style='padding:5px 15px 5px 15px;".$bottom_border."'>$".number_format($extract['tax'], 2)."</td><td align='right' style='padding:5px 15px 5px 15px;".$bottom_border."'>$".number_format($extract['total'], 2)."</td></tr>
				<tr>
					<td align='center' height='1px' colspan='9' style='padding:5px 35px 5px 35px;'>
						<table cellspacing='0' cellpadding='0'>
							<tr id='order_contents_".$extract['id']."' style='display:none'>
								<td align='left' valign='top' width='40px'><a onclick='document.getElementById(\"order_contents_".$extract['id']."\").style.display = \"none\"' title='Hide contents...'><img src='images/order_up.png' onmouseover='this.src = \"images/order_up_over.png\"' onmouseout='this.src = \"images/order_up.png\"'></a></td>
								<td align='center'>
									<table cellspacing='1' cellpadding='5' bgcolor='#FFFFFF'>";
				$get_items = lavu_query("SELECT * FROM `order_contents` WHERE `loc_id` = '".$extract['location_id']."' AND `order_id` = '".$extract['order_id']."' AND `item` != 'SENDPOINT' ORDER BY id ASC");
				if (mysqli_num_rows($get_items) > 0) {
					$rows_i = mysqli_num_rows($get_items);
					$row_i = 1;
					$bottom_border_i = " border-bottom:1px solid #999999;";
					while ($extract_i = mysqli_fetch_array($get_items)) {
						if ($row_i == $rows_i) {
							$bottom_border_i = "";
						}
						$display .= "<tr class='order_contents'><td align='left' valign='top' width='200px' style='padding:5px 10px 5px 5px;".$bottom_border_i."'>".$extract_i['item'];
						if ($extract_i['options'] != "") {
							$display .= "<br /><i>".$extract_i['options']."</i>";
						}
						if ($extract_i['special'] != "") {
							$display .= "<br /><span class='special_instructions'><i>".$extract_i['special']."</i></span>";
						}
						$display .= "</td><td align='right' valign='top' style='padding:5px 5px 5px 10px;".$bottom_border_i."'>$".number_format($extract_i['price'], 2)."</td><td align='right' valign='top' width='40px' style='padding:5px 2px 5px 2px;".$bottom_border_i."'>X ".$extract_i['quantity']."&nbsp;&nbsp; =</td><td align='right' valign='top' width='60px' style='padding:5px 12px 5px 7px;".$bottom_border_i."'>$".number_format(($extract_i['price'] * $extract_i['quantity']), 2)."</td></tr>";
						$row_i++;
					}
				} else {
					$display .= "<tr><td align='center'>An error occurred while trying to load order contents...</td></tr>";
				}
				$display .= "</table>
								</td>
								<td valign='top' width='40px'></td>
							</tr>
						</table>
					</td>
				</tr>";
				$row++;
			}
			$display .= "</table>
							</td>
						</tr>
						<tr><td class='inner_panel_lg_bottom'></td></tr>
					</table>
				</td>
			</tr>
			<tr><td>&nbsp;</td></tr>";
		} else {
			$display .= "<tr><td align='center' style='padding:15px 0px 15px 0px'>There are no orders in the system which meet your search criteria...</td></tr>";
		}
		echo "<script language='javascript' src='resources/scw.js'></script>";
		echo "<table>";
		echo $display;
		echo "</table>";
	}
?>
