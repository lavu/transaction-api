<?php
	function set_config_val($config_key,$set_value)
	{
		$exist_query = lavu_query("select * from `config` where `type`='general_setting' and `setting`='[1]'",$config_key);
		if(mysqli_num_rows($exist_query))
		{
			$exist_read = mysqli_fetch_assoc($exist_query);
			lavu_query("update `config` set `value`='[1]' where `id`='[2]'",$set_value,$exist_read['id']);
		}
		else
		{
			lavu_query("insert into `config` (`type`,`setting`,`value`) values ('general_setting','[1]','[2]')",$config_key,$set_value);
		}
	}
	
	function get_config_val($config_key,$default="")
	{
		$exist_query = lavu_query("select * from `config` where `type`='general_setting' and `setting`='[1]'",$config_key);
		if(mysqli_num_rows($exist_query))
		{
			$exist_read = mysqli_fetch_assoc($exist_query);
			return $exist_read['value'];
		}
		else 
		{
			return $default;
		}
	}
	
	function export_custom_string($filename,$str)
	{
		$known_mime_types=array(
			"pdf" => "application/pdf",
			"txt" => "text/plain",
			"csv" => "text/plain",
			"html" => "text/html",
			"htm" => "text/html",
			"exe" => "application/octet-stream",
			"zip" => "application/zip",
			"doc" => "application/msword",
			"xls" => "application/vnd.ms-excel",
			"ppt" => "application/vnd.ms-powerpoint",
			"gif" => "image/gif",
			"png" => "image/png",
			"jpeg"=> "image/jpg",
			"jpg" =>  "image/jpg",
			"php" => "text/plain"
		);
		$file_extension = strtolower(substr(strrchr($filename,"."),1));
		$mime_type = (isset($known_mime_types[$file_extension]))?$known_mime_types[$file_extension]:$file_extension;
		
		/*if(strpos($str,"&#8364;")!==false && $file_extension=="xls")
		{
			$mime_type = "text/plain";
			$str = "........" . $str;
		}*/
		
		header('Content-Type: ' . $mime_type);
		header('Content-Disposition: attachment; filename="'.$filename.'"');
		header("Content-Transfer-Encoding: binary");
		header('Accept-Ranges: bytes');
		
		header("Cache-control: private");
		header('Pragma: private');
		header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
		
		header("Content-Length: ".strlen($str));
		echo $str;
	}
	
	$tablename = (isset($_GET['tablename']))?$_GET['tablename']:"";
	if(isset($_POST['mode']) && $_POST['mode']=="export")
	{
		$filename = $_POST['export_filename'];
		$export_str = $_POST['export_contents'];
		export_custom_string($filename,$export_str);
		
		exit();
	}
	
	$aliases = array("cc_transactions"=>"order_payments","config"=>"x");
	
	if($tablename!="")
	{
		$show_tablename = ucfirst(str_replace("_"," ",$tablename));
		echo "<form name='query_form' method='post' action=''>";
		echo "<table><tr>";
		echo "<td width='120' align='right'><input type='button' value='<< Other Tables' onclick='window.location = \"?mode={$section}_{$mode}\"'></td>";
		echo "<td><b>$show_tablename</b></td>";
		echo "<td width='120'>&nbsp;</td>";
		echo "</tr></table>";
		
		$order_query_by = (isset($_POST['orderby']))?$_POST['orderby']:"id";
		$order_query_dir = (isset($_POST['orderdir']))?$_POST['orderdir']:"desc";
		
		$auto_set = array();
		if(!isset($_POST['posted']))
		{
			$rmq = get_config_val("remember_q".$tablename,"-na-");
			//if(isset($_SESSION['remember_q' . $tablename]))
			if($rmq!="-na-")
			{
				$remember_parts = explode("|",$rmq);//$_SESSION['remember_q' . $tablename]);
				for($i=0; $i<count($remember_parts); $i++)
				{
					$rpart = $remember_parts[$i];
					if(substr($rpart,0,8)=="orderby:")
					{
						$order_query_by = substr($rpart,8);
					}
					else if(substr($rpart,0,9)=="orderdir:")
					{
						$order_query_dir = substr($rpart,9);
					}
					else
					{
						$auto_set[$rpart] = 1;
					}
				}
			}
		}
		
		//echo "order_by: $order_query_by<br>";
		$actual_tablename = $tablename;
		foreach($aliases as $akey => $aval)
		{
			if($actual_tablename==$aval)
			{
				$actual_tablename = $akey;
			}
		}
		
		$table_cols = array();
		$checked_cols = array();
		$col_query = lavu_query("select * from `[1]` limit 1",$actual_tablename);
		if(mysqli_num_rows($col_query))
		{
			$col_read = mysqli_fetch_assoc($col_query);
			
			if(count($table_cols) < 1)
			{
				foreach($col_read as $col_key => $col_val)
				{
					$table_cols[] = $col_key;
				}
			}
		}
		sort($table_cols);
		
		$maxrows = 6;
		$maxcols = ceil(count($table_cols) / $maxrows);
		$current_row = 1;
		
		//echo "<br><br>tablename: $actual_tablename<br><br>";
		echo "<br>";
		echo "<table style='border:solid 1px #aaaaaa'>";
		echo "<tr>";
		echo "<td align='right' colspan='12' bgcolor='#dddddd'>";
		echo "<table cellpadding=4><tr><td>";
		echo "<input type='checkbox' name='select_all' id='select_all' onclick='select_checkboxes(this.checked)'> Select All";
		echo "</td></tr></table>";
		echo "</td>";
		echo "</tr>";
		echo "<tr>";
		echo "<td valign='top'>";
		$jscode = "";
		$jscode .= "cbnames = new Array(); ";
		for($i=0; $i<count($table_cols); $i++)
		{
			$table_column = $table_cols[$i];
			$show_table_column = $table_column;//ucfirst(str_replace("_"," ",$table_column));
			
			$cbname = "checkbox_" . $table_column;
			$input_props = "";
			if(isset($_POST[$cbname]) || isset($auto_set[$table_column]))
			{
				$checked_cols[] = $table_column;
				$input_props = " checked";
			}
			echo "<input type='checkbox' name='$cbname' id='$cbname'".$input_props.">";
			$jscode .= "  cbnames[cbnames.length] = \"$cbname\"; ";
			echo $table_column . "<br>";
			
			
			$current_row++;
			if($current_row > $maxcols && $i < count($table_cols))
			{
				echo "</td><td width='20'>&nbsp;</td><td valign='top'>";
				$current_row = 1;
			}
		}
		echo "</td>";
		echo "</tr>";
		echo "<tr>";
		echo "<td align='right' colspan='12' bgcolor='#dddddd'>";
		echo "<table cellpadding=4><tr><td>";
		echo "<input type='button' value='Run Query >>' onclick='document.query_form.submit()'>";
		echo "</td></tr></table>";
		echo "</td>";
		echo "</tr>";
		echo "</table>";
		
		if(isset($_POST['limit_size']))
		{
			$_SESSION['query_limit_size'] = $_POST['limit_size'];
		}
		$size = (isset($_SESSION['query_limit_size']))?$_SESSION['query_limit_size']:2000;
		$limit_start = (isset($_POST['limit_start']))?$_POST['limit_start']:0;
		$limit_size = $size;
		
		echo "<input type='hidden' name='posted' value='1'>";
		echo "<input type='hidden' name='orderby' id='orderby' value='$order_query_by'>";
		echo "<input type='hidden' name='orderdir' id='orderdir' value='$order_query_dir'>";
		echo "<input type='hidden' name='limit_start' id='limit_start' value='$limit_start'>";
		echo "<input type='hidden' name='limit_size' id='limit_size' value='$limit_size'>";
		echo "<input type='hidden' name='query_action' id='query_action' value=''>";
		
		echo "<script language='javascript'>";
		echo $jscode;
		echo "function select_checkboxes(set_check) { ";
		echo "  for(n=0; n<cbnames.length; n++) { ";
		echo "		document.getElementById(cbnames[n]).checked = set_check; ";
		echo "	} ";
		echo "} ";
		echo "function select_all_checkboxes() { ";
		echo "	select_checkboxes(true); ";
		echo "} ";
		echo "function deselect_all_checkboxes() { ";
		echo "	select_checkboxes(false); ";
		echo "} ";
		echo "function set_order_by(colname,coldir) { ";
		echo "	document.getElementById(\"orderby\").value = colname; ";
		echo "	document.getElementById(\"orderdir\").value = coldir; ";
		echo " document.query_form.submit(); ";
		echo "} ";
		echo "function set_query_page(set_page,set_limit) { ";
		echo "  document.getElementById(\"limit_start\").value = set_page; ";
		echo "  document.getElementById(\"limit_size\").value = set_limit; ";
		echo " document.query_form.submit(); ";
		echo "} ";
		echo "function export_page() { ";
		echo "	document.getElementById(\"query_action\").value = 'export_page'; ";
		echo " document.export_form.submit(); ";
		echo "} ";
		echo "</script>";
		echo "</form>";
		
		if(count($table_cols) < 1)
		{
			echo "<table><tr><td>----- No Data -----</td></tr></table>";
		}
		
		function get_export_row_sep()
		{
			return chr(13);
		}
		function get_export_col_sep()
		{
			return ",";
		}
		function create_export_col($str)
		{
			return "\"" . str_replace(chr(13),"\\n",str_replace("\"","&quot;",str_replace("&","&amp;",$str))) . "\"";
		}
		
		if(isset($_POST['posted']))
		{
			$remember_str = "";
			$other_cols = array("orderby:".$order_query_by,"orderdir:".$order_query_dir);
			$r_cols = array_merge($checked_cols,$other_cols);
			for($i=0; $i<count($r_cols); $i++)
			{
				$colname = $r_cols[$i];
				if($remember_str!="") $remember_str .= "|";
				$remember_str .= $colname;
			}
			
			set_config_val("remember_q".$tablename,$remember_str);
			//$_SESSION['remember_q' . $tablename] = $remember_str;
			
			$count_query = lavu_query("select count(*) as `count` from `[1]`",$actual_tablename);
			if(mysqli_num_rows($count_query))
			{
				$count_read = mysqli_fetch_assoc($count_query);
				$total_count = $count_read['count'];
			}
			else $total_count = 0;
			
			echo "<br><br>Results:";
			
			$start = 1;
			$p = 1;
			
			echo "<select onchange='set_query_page(this.value,$size)'>";
			$pmax = ceil($total_count / $size);
			for($n=$start; $n<$total_count; $n+=$size)
			{
				$n2 = $n + $size - 1;
				if($n2 > $total_count) $n2 = $total_count;
				echo "<option value='".($n-1)."'";
				if($limit_start==($n-1)) echo " selected";
				echo ">Page $p / $pmax ($n - $n2)</option>";
				$p++;
			}
			echo "</select>";
			
			echo "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
			$limit_size_choices = array(100,200,500,1000,2000,4000,6000,8000,10000);
			echo "Number of Records To Show for each Page: <select onchange='set_query_page(0,this.value)'>";
			for($n=0; $n<count($limit_size_choices); $n++)
			{
				$lsize = $limit_size_choices[$n];
				echo "<option value='$lsize'";
				if($lsize * 1==$limit_size * 1) echo " selected";
				echo ">$lsize</option>";
			}
			echo "</select>";
						
			$export_str = "";
			echo "<br><br>";
			echo "<div style='width:1000px; height:360px; overflow:auto'>";
			echo "<table style='border:solid 1px #888888' cellspacing=0 cellpadding=2>";
			echo "<tr>";
			echo "<td bgcolor='#eeeeee' style='border-right:solid 1px #dddddd'>&nbsp;</td>";
			for($i=0; $i<count($checked_cols); $i++)
			{
				$colname = $checked_cols[$i];
				$coldir = "asc";
				if($colname==$order_query_by && $order_query_dir=="asc") $coldir = "desc";
				if($colname==$order_query_by) $extra_style = " font-weight:bold"; else $extra_style = "";
				echo "<td bgcolor='#ccccdd' align='center' style='cursor:pointer; $extra_style' onclick='set_order_by(\"$colname\",\"$coldir\")'>$colname</td>";
				$export_str .= create_export_col($colname);
				if($i < count($checked_cols) - 1) $export_str .= get_export_col_sep();
			}
						
			echo "</tr>";
			$result_count = 0;
			$result_query = lavu_query("select * from `[1]` order by [2] [3] limit [4],[5]",$actual_tablename,$order_query_by,$order_query_dir,$limit_start,$limit_size);
			while($result_read = mysqli_fetch_assoc($result_query))
			{
				$result_count ++;
				$export_str .= get_export_row_sep();
				
				$show_result_count = $result_count + $limit_start;
				echo "<tr>";
				echo "<td bgcolor='#eeeeee' style='border-right:solid 1px #dddddd; color:#bbbbbb' valign='top' align='right'>#$show_result_count&nbsp;&nbsp;</td>";
				for($i=0; $i<count($checked_cols); $i++)
				{
					$colname = $checked_cols[$i];
					if($i < count($checked_cols) - 1)
					{
						$tdstyle = "style='border-right:solid 1px #bbbbbb'";
					}
					else $tdstyle = "";
					echo "<td valign='top' $tdstyle><div style='width:80px; overflow:hidden'><nobr>" . str_replace("<","&lt;",str_replace("&","&amp;",$result_read[$colname])) . "</nobr></div></td>";
					
					$export_str .= create_export_col($result_read[$colname]);
					if($i < count($checked_cols) - 1) $export_str .= get_export_col_sep();
				}
				echo "</tr>";
			}
			echo "</table>";
			echo "</div>";
			
			echo "<br><br>";
			echo "<table cellpadding=6 style='border:solid 2px #8888aa' bgcolor='#eeeeff'><tr><td width='600' align='center'>";
			echo "<input type='button' value='Export >>' onclick='export_page()'>";
			echo "</td></tr></table>";
			
			$query_action = (isset($_POST['query_action']))?$_POST['query_action']:"";
			/*if($query_action=="export_page")
			{
				echo "export: ";
				echo "<br><textarea rows='40' cols='120'>$export_str</textarea>";
			}*/
			
			$export_filename = $tablename . "_" . ($limit_start + 1) . "-" . ($limit_start + $limit_size) . ".csv";
			echo "<form name='export_form' method='post' action='index.php?widget=reports/customer_query' target='hframe'>";
			echo "<textarea rows='40' cols='120' style='display:none' name='export_contents'>".str_replace("<","&lt",str_replace(">","&gt;",$export_str))."</textarea>";
			echo "<input type='hidden' name='export_filename' value='$export_filename'>";
			echo "<input type='hidden' name='mode' value='export'>";
			echo "</form>";
			
			echo "<iframe width='400' height='120' name='hframe' id='hframe' style='display:none'></iframe>";
		}
	}
	else
	{
		$tlist = array();
		$table_query = lavu_query("show tables");
		while($table_read = mysqli_fetch_array($table_query))
		{
			$tablename = $table_read[0];
			if(isset($aliases[$tablename])) $tablename = $aliases[$tablename];
			
			if($tablename!="x" && $tablename!="")
			{
				$tlist[] = $tablename;
			}
		}
		
		sort($tlist);
		
		$maxrows = 4;
		$maxcols = ceil(count($tlist) / $maxrows);
		$current_row = 1;
		
		echo "<b>Custom Query: Choose a database table</b><br><br>";
		echo "<table style='border:solid 1px #cccccc'>";
		echo "<tr>";
		echo "<td valign='top'>";
		for($i=0; $i<count($tlist); $i++)
		{
			$tablename = $tlist[$i];
			$show_tablename = ucfirst(str_replace("_"," ",$tablename));
			
			echo "<table><tr><td style='cursor:pointer; width:160px' onclick='window.location = \"?mode={$section}_{$mode}&tablename=$tablename\"' onmouseover='this.bgColor = \"#bbccdd\"' onmouseout='this.bgColor = \"#ffffff\"'>";
			echo $show_tablename;
			echo "</td></tr></table>";
			
			$current_row++;
			if($current_row > $maxcols && $i < count($tlist))
			{
				echo "</td><td width='20'>&nbsp;</td><td valign='top'>";
				$current_row = 1;
			}
		}
		echo "</td>";
		echo "</tr>";
		echo "</table>";
	}
?>
