<!--
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Untitled Document</title>
-->
<script type="text/javascript">
newHeight = 0;
moveDistance = newHeight;
var thisDate = new Date();
var drawYear = thisDate.getFullYear();/*updated by drawThreeMonths, this is how the function drawThreeMonths knows what is the currently drawected month*/
var drawMonth = thisDate.getMonth();
var drawDay = 1;//thisDate.getDate();
var timerHasFired = 0;//I don't think I ever used this
var moveDirection;
var totalMoved = 0;
var start_or_end_date;
var STOID; //this is the setTimeOutID, stop the animation by calling clearTimeout(STOID)
var arrows_off = 0;
show_arrows_hist = arrows_off;
var start_date_black = "";//store id for the highlighted date so we can undraw it later
var end_date_black = "";
first_day_drawn = false;//this is used to make the first day of the month 'active' when a user scrolls through the months, this stops the javascript from freezing (it's a bug fix! :P)
//0var showDayIDs = "";
//drawCurrentMonth(drawYear,drawMonth,drawDay);
	// showAll();
function showAll(){
		alert("thisDate: " + thisDate + "\ndrawYear: " + drawYear + "\ndrawMonth: " + drawMonth + "\ndrawDay: " + drawDay + "\nmoveDirection: " + moveDirection + "\ntotalMoved: " + totalMoved + "\nstart_or_end_date: " + start_or_end_date + "\nstart_date_black: " + start_date_black + "\nend_date_black: " + end_date_black + "\narrows_off: " + arrows_off + "\nshow_arrows_hist: " + show_arrows_hist);
}//showAll()
	 
//alert(drawYear+"-"+drawMonth+"-"+drawDay);
function drawCurrentMonth(drawYear,drawMonth,drawDay){
		
		var date = new Date(drawYear,drawMonth,drawDay);
		curMonth = date.getMonth();
		curYearStr = date.getFullYear().toString();
		curYear = curYearStr;
		curYear = "'"+curYearStr.charAt(2) + curYearStr.charAt(3);
		//alert(curMonth);//0 is january, 1 is february, etc...
		//alert(convertDateNumberToText(curMonth));
		var displayString = "";
		displayString += "<div id=\'molo\' class=\'monthName\'>";
		displayString += "<div class=\"arrow left\"><a href=\"javascript:moveDirection='left'; start_or_end_date='"+start_or_end_date+"'; drawMonth='"+drawMonth+"'; drawYear='"+curYearStr+"'; if (arrows_off*1==0){doMove();}\"><<</a></div>";
		displayString += "<div class='monthText'>"+convertDateNumberToText(curMonth)+" "+curYear+"</div>";
		displayString += "<div class=\"arrow right\"><a href=\"javascript:moveDirection='right'; start_or_end_date='"+start_or_end_date+"'; drawMonth='"+drawMonth+"'; drawYear='"+curYearStr+"'; if (arrows_off*1==0){doMove();}\">>>&nbsp;</a></div>";
		displayString += "<div style='clear:both;'></div>";
		displayString += "</div>";
		displayString += "<div class=\"weekAbr\" style=\"font-weight:700;\">";
		displayString += "<div class=\"holdDay\" style=\"height:20px;\">Sun</div>";
		displayString += "<div class=\"holdDay\" style=\"height:20px;\">Mon</div>";
		displayString += "<div class=\"holdDay\" style=\"height:20px;\">Tues</div>";
		displayString += "<div class=\"holdDay\" style=\"height:20px;\">Wed</div>";
		displayString += "<div class=\"holdDay\" style=\"height:20px;\">Thu</div>";
		displayString += "<div class=\"holdDay\" style=\"height:20px;\">Fri</div>";
		displayString += "<div class=\"holdDay\" style=\"height:20px;\">Sat</div>";
		displayString += "<div style=\"clear:both;\"></div>";
		displayString += "</div>";
		displayString += "<div class=\"holdDates\" id=\"holdDates\">"+drawDates(date)+"</div>";
		
		//document.getElementById("calendar").innerHTML = displayString;
		return displayString;
		//curTop = document.getElementById("move").style.marginTop;
		//alert(curTop);
/*		
		for (i=0;i<200;i++){
			
				document.getElementById("move").style.top += 1;
		}//for
	&*/			
}//drawCurrentMonth()

function turnOffArrows(){
		/* We turn off the arrows to scroll to the next month to keep a user from clicking to the next month before processes from their first click are done running. IF this happens, certain memory variables don't get set and the script hangs up. */
		/* this function doesn't do anything */
		return;
}//turnOffArrows()

function drawNextMonth(){
		//document.getElementById("holdThreeMonths").style.right = '400px';
		/* Draws the current month in the middle, and the two adjacent months to the side*/
		//drawMonth++;
		var drawID = "nextMonth";
		if (start_or_end_date == "end"){
				drawID += "_end"; 
		}//if
		
		currentMonthStr = drawCurrentMonth(drawYear*1,drawMonth*1+1,drawDay*1);
		document.getElementById(drawID).innerHTML = currentMonthStr;
	
		//document.getElementById("holdThreeMonths").style.right = '-400px';
}//drawThreeMonths()

function drawPreviousMonth(){
		//document.getElementById("holdThreeMonths").style.right = '400px';
		/* Draws the current month in the middle, and the two adjacent months to the side*/
		//drawMonth++;
		var drawID = "previousMonth";
		if (start_or_end_date == "end"){
				drawID += "_end"; 
		}//if
		
		currentMonthStr = drawCurrentMonth(drawYear*1,drawMonth*1-1,drawDay*1);
		document.getElementById(drawID).innerHTML = currentMonthStr;
	
		//document.getElementById("holdThreeMonths").style.right = '-400px';
}//drawThreeMonths()


function drawThreeMonths(){
		
		/* Draws the current month in the middle, and the two adjacent months to the side*/
		/* This just draws the month in the middle, or the one with id="currentMonth"*/
		
		var drawID = "currentMonth";
		if (start_or_end_date == "end"){
				drawID += "_end"; 
		}//if
		
		currentMonthStr = drawCurrentMonth(drawYear*1,drawMonth*1,drawDay*1);
		document.getElementById(drawID).innerHTML = currentMonthStr;
		
	
		//document.getElementById("holdThreeMonths").style.right = '-400px';
}//drawThreeMonths()

function daysInMonth(iMonth, iYear){
		/* How many days are in this month? */
		return 32 - new Date(iYear, iMonth, 32).getDate();
}

function dayClicked(date_clicked, f_start_or_end_date){
		/* This function is called when the user clicks a date on either calendar */
		//alert(date_clicked);
		//start_date_black=date_clicked;
		//showAll();
		//	alert(document.getElementById("paint_end_date_id").value);
		
		/* This block of code unpaints any days that are already highlighted on page load */
		if (document.getElementById("paint_start_date_id").value != "" && f_start_or_end_date == "start"){
						document.getElementById(document.getElementById("paint_start_date_id").value).style.backgroundColor = "#fff";
		}//if
		if (document.getElementById("paint_end_date_id").value != "" && f_start_or_end_date == "end"){
						document.getElementById(document.getElementById("paint_end_date_id").value).style.backgroundColor = "#fff";
		}//if
	
		if ( (start_date_black != "") && (f_start_or_end_date == "start") ){
				//alert(start_date_black);
				document.getElementById(start_date_black).style.backgroundColor = "#fff";
				
			
		}//if
		
		if ( (end_date_black != "") && (f_start_or_end_date == "end") ){
				//alert(end_date_black);
				document.getElementById(end_date_black).style.backgroundColor = "#fff";		
		}//if
			
		if (f_start_or_end_date == "start"){start_date_black = date_clicked;}
		if (f_start_or_end_date == "end"){end_date_black = date_clicked;}
	
		if (first_day_drawn == true){
				drawColor = "#000";		
		}//if
		if (first_day_drawn != true){
				drawColor = "#ccc";		
		}//if
		first_day_drawn = true;
		document.getElementById(date_clicked).style.backgroundColor = drawColor;
		
		if (f_start_or_end_date == "start"){
				document.getElementById("paint_start_date_id").value = date_clicked;
		}//if
		if (f_start_or_end_date == "end"){
				document.getElementById("paint_end_date_id").value = date_clicked;
		}//if
		
		
		start_or_end_date = f_start_or_end_date;//f_start_or_end_date is passed into this function when the user clicks a day on the calendar. It tells us if the user clicked the start day calendar or the end date calendar
		//alert(day_clicked);
		//showAll();
		//alert(f_start_or_end_date);
		// Fill in the values for the list menu start and end dates 
		var monthID;
		var yearID;
		var dayID;
		if (f_start_or_end_date == "start"){
				monthID = "startMonth";
				yearID = "startYear";
				dayID = "startDay";
		}//if
		if (f_start_or_end_date == "end"){
				monthID = "endMonth";
				yearID = "endYear";
				dayID = "endDay";
		}//if
		
		date_clicked_str = date_clicked.split("-");
		day_clicked = date_clicked_str[3];
		month_clicked = date_clicked_str[2];
		year_clicked = date_clicked_str[1];
		
		drawDateObj = new Date(year_clicked, month_clicked, day_clicked);
		//alert(drawDateObj);
		calDay = drawDateObj.getDate();
		calMonth = drawDateObj.getMonth()*1 + 1;//javascript counts january as 0, php counts it as 1...
		calYear = drawDateObj.getFullYear();
		document.getElementById(monthID).value = calMonth;
		document.getElementById(yearID).value = calYear;
		document.getElementById(dayID).value = calDay;
		
		showThisDateOnLoad = calYear+"-"+calMonth+"-"+calDay;
		if (f_start_or_end_date == "start"){
				document.getElementById("start_date_text_field").value = showThisDateOnLoad;
		}//if
		if (f_start_or_end_date == "end"){
				document.getElementById("end_date_text_field").value = showThisDateOnLoad;
		}//if
		
}//dayClicked()


function drawDates(date){
		/* Display the calendar part from passed in date object */
		dayOfMonth = date.getDate();
		month = date.getMonth();
		year = date.getFullYear();
		numDaysInMonth = daysInMonth(month,year);
		//alert("dayOfMonth: " + dayOfMonth + "\nmonth: " + month + "\nyear: " + year + "\nnumDaysInMonth: " + numDaysInMonth);
		drawDatesStr = "";
		
		numWeeksInMonth = Math.ceil(numDaysInMonth*1/7);
		
		curDate = new Date(year,month,1);
		firstDayOfMonth = curDate.getDay();
		//alert(firstDayOfMonth);
		index = 1;
		drawActiveDate = false;
		for (week=0;week<=numWeeksInMonth;week++){
			drawDatesStr += "<div class=\"weekRow\">";
				for (day=0;day<=6;day++){
							day_id = start_or_end_date+"-"+year+"-"+month+"-"+index;
							
							if (drawActiveDate != true){
									drawDatesStr += "<div class=\"holdDay\">";
							}//if
							//drawDatesStr += drawActiveDate;
							if ( (day >= firstDayOfMonth) && (drawActiveDate == false) ){
									drawActiveDate = true;
							}//if
							
							if (index > numDaysInMonth){
									drawActiveDate = false;
							}//if
							
							if (drawActiveDate == true){
									drawDatesStr += "<div class=\"holdDay\" id=\""+day_id+"\">";
									drawDatesStr += "<a href=\"javascript:first_day_drawn = true; dayClicked('"+day_id+"','"+start_or_end_date+"');\">"+index+"</a>";
									//showDayIDs += "_"+day_id;
									
									index++;
							}//if
							
							if (drawActiveDate == false){
									drawDatesStr += "&nbsp;";
							}//if
							drawDatesStr += "</div>";
							//drawDatesStr += "blurp";
							
							drawDatesStr += "</div>";
				}//for
			drawDatesStr += "</div><div style='clear:both;'></div>";
		}//for
		
		return drawDatesStr;
}//drawDates()
		
function doMove() {
		//alert('hello');
		
		
		arrows_off += 1;//doMove() cannot be executed if arrows_off is anything but 0. This keeps people from running the script in conjuction with itself, which freezes the javascript
		
		show_arrows_hist += ","+arrows_off;
		if (totalMoved < 350){
				moveDistance = 40;
		}//if
		
		if ( (totalMoved >= 350) && (totalMoved < 375) ){
				moveDistance = 30;
		}//if
		
		
		if ( (totalMoved >= 375) ){
				moveDistance = 1;
		}//if
				
		
		if (moveDirection == "right"){
				newHeight+=moveDistance;
		}//if
		
		if (moveDirection == "left"){
				newHeight-=moveDistance;
		}//if
		
		
		var drawID = "holdThreeMonths";
		if (start_or_end_date == "end"){
				drawID += "_end";
		}//if
		
		document.getElementById(drawID).style.right = newHeight+'px';
		
  	totalMoved = totalMoved + moveDistance;
	//  timerHasFired++;
		if ( Math.abs(totalMoved) > 400){
				stopCount();
				return;
		}//if
	
		STOID = setTimeout(doMove,15); // call domolo() in 20000 msec
		
}
 
function stopCount(){
		clearTimeout(STOID);
		redraw();
		//document.getElementById("holdThreeMonths").style.right = '400px';
		//document.getElementById("showfinish").innerHTML = "done";
		first_day_clicked = start_or_end_date+"-"+drawYear+"-"+drawMonth+"-"+drawDay;
		first_day_drawn = false;
		dayClicked(first_day_clicked, start_or_end_date);
		arrows_off = 0;
		newHeight = 0;
		totalMoved = 0;
}//stopCount()
 


function redraw(){
		
		if (moveDirection == "right"){
				drawMonth++;			
		}//if
		
		if (moveDirection == "left"){
				drawMonth--;			
		}//if
		
		/* This block is necessary to keep the year accurate */
		updateDrawDateInfo = new Date(drawYear,drawMonth,drawDay);
		drawMonth = updateDrawDateInfo.getMonth();
		drawYear = updateDrawDateInfo.getFullYear();
		drawDay = updateDrawDateInfo.getDate();
		
		drawThreeMonths();
		drawNextMonth();
		drawPreviousMonth();	
		
		var drawID = "holdThreeMonths";
		if (start_or_end_date == "end"){
				drawID += "_end";
		}//if
		document.getElementById(drawID).style.right = '0px';
}//redraw()
 
function convertDateNumberToText(input){
		/* takes in 0, returns January, takes in 1, returns February, etc... */
		if (input == "0"){
				return "January";
		}//if
		
		if (input == "1"){
				return "February";
		}//if
		
		if (input == "2"){
				return "March";
		}//if
		
		if (input == "3"){
				return "April";
		}//if
		
		if (input == "4"){
				return "May";
		}//if
		
		if (input == "5"){
				return "June";
		}//if
		
		if (input == "6"){
				return "July";
		}//if
		
		if (input == "7"){
				return "August";
		}//if
		
		if (input == "8"){
				return "September";
		}//if
		
		if (input == "9"){
				return "October";
		}//if
		
		if (input == "10"){
				return "November";
		}//if
		
		if (input == "11"){
				return "December";
		}//if
	
}//convertDateNumberToText()

</script>




<style type="text/css">

.holdDates{
	position:relative;
}

.day{
	float:left;
}

.holdThreeMonths{
	height:auto;
	width:1200px;
	position:relative;
	
}

.holdDay{
	float:left;
	width:45px;
	border:0px #000 solid;
	text-align:center;
	font-weight:700;
	color:#333;
	height:25px;
	
}

.arrow{
	width:100px;
	background-color:#fff;
	float:left;
}

.inactiveArrow{
	width:100px;
	background-color:#fff;
	color:#000;
	display:none;
	float:left;
}

.monthElement{
	float:left;
	border:0px solid #333;
}

.left{
	
}

.right{
	
}

.monthName{
	width:400px;
	border-right:0px solid #000;
	text-align:center;
	
}

.monthText{
	font-family:Verdana, Geneva, sans-serif;
	font-size:17pt;
	float:left;
	width:180px;
	background-color:#fff;
}

.calendar{
	width:370px;
	height:200px;
	background-color:#fff;
	border:1px solid #333;
	/*margin:auto;*/
	overflow:hidden;
	/*position:absolute;*/
	z-index:0;
	float:left;
}

.hidePanel{
	width:400px;
	display:none;
	height:200px;
	position:absolute;
	top:0px; 
	padding:0; 
	
	margin:0;
	
	background-color:#fff;
	z-index:1;
}

</style>
<!--
</head>

<body>


<input type="button" value="Stop count!" onclick="stopCount()" />
<input type="text" id="stop" />
-->
<div class="calendar"  id="calendar_start">
		
		<div class="holdThreeMonths"  id="holdThreeMonths">
				<div id="previousMonth" style="" class="monthElement"></div>
				<div id="currentMonth" style="" class="monthElement"></div>
				<div id="nextMonth" style="" class="monthElement"></div>
		</div><!--holdThreeMonths-->
</div><!--calendar_start-->

<div class="calendar" style="width:370px;"  id="calendar_end">
		
		<div class="holdThreeMonths"  id="holdThreeMonths_end">
				<div id="previousMonth_end" style="" class="monthElement"></div>
				<div id="currentMonth_end" style="" class="monthElement"></div>
				<div id="nextMonth_end" style="" class="monthElement"></div>
		</div><!--holdThreeMonths-->
</div><!--calendar_start-->




<script type="text/javascript">
	//draw the calendar on the left
	if (document.getElementById("start_date_text_field").value != ""){
				var getStartDate = document.getElementById("start_date_text_field").value;
				//alert(getStartDate);
				
				var getStartArr = getStartDate.split("-");
				var getStartYear = getStartArr[0];
				var getStartMonth = getStartArr[1]*1 - 1;//change to javascript date from php date, javascript starts january as 0, in php, january is 1
				var getStartDay = getStartArr[2];
				var thisDate = new Date(getStartYear,getStartMonth,getStartDay);
				
				
				//paintThisID = "start-"+String(getStartYear)+"-"+String(getStartMonth)+"-"+String(getStartDay);
				//document.getElementById(paintThisID).innerHTML;
				//alert(paintThisID);
				var drawYear = thisDate.getFullYear();/*updated by drawThreeMonths, this is how the function drawThreeMonths knows what is the currently drawected month*/
				var drawMonth = thisDate.getMonth();
				var drawDay = 1;//thisDate.getDate();
	}//if
	
	start_or_end_date = "start";
	drawPreviousMonth();
	drawThreeMonths();
	drawNextMonth();
	document.getElementById("currentMonth").scrollIntoView();//line up the current month with the view
	
	//draw the calendar on the right
	if (document.getElementById("end_date_text_field").value != ""){
				var getEndDate = document.getElementById("end_date_text_field").value;
				//alert(getEndDate);
				var getEndArr = getEndDate.split("-");
				var getEndYear = getEndArr[0];
				var getEndMonth = getEndArr[1]*1 - 1;//change to javascript date from php date, javascript Ends january as 0, in php, january is 1
				var getEndDay = getEndArr[2];
				var thisDate = new Date(getEndYear,getEndMonth,getEndDay);
				//alert(thisDate);
				var drawYear = thisDate.getFullYear();/*updated by drawThreeMonths, this is how the function drawThreeMonths knows what is the currently drawected month*/
				var drawMonth = thisDate.getMonth();
				var drawDay = 1;//thisDate.getDate();
	}//if
	
	start_or_end_date = "end";
	drawPreviousMonth();
	drawThreeMonths();
	drawNextMonth();
	document.getElementById("currentMonth_end").scrollIntoView();//line up the current month with the view
	//alert(showDayIDs);
	
	//paint selected dates, if any
	if (document.getElementById("paint_start_date_id").value != ""){
			document.getElementById(document.getElementById("paint_start_date_id").value).style.backgroundColor = "#000";
	}//if
	
	if (document.getElementById("paint_end_date_id").value != ""){
			document.getElementById(document.getElementById("paint_end_date_id").value).style.backgroundColor = "#000";
	}//if
	
</script>



	