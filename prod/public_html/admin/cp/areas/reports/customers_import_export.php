<?php
	//Written by Brian Dennedy
	ini_set('display_errors', 1);


	$allowedColumnsAndTitleToDBMappingArr = array();
	$allowedColumnsAndTitleToDBMappingArr["Last Name"] = 'l_name';
	$allowedColumnsAndTitleToDBMappingArr["First Name"] = 'f_name';
	$allowedColumnsAndTitleToDBMappingArr["Email"] = 'field1';
	$allowedColumnsAndTitleToDBMappingArr["Phone Number"] = 'field2';
	$allowedColumnsAndTitleToDBMappingArr["Street Address"] = 'field3';
	$allowedColumnsAndTitleToDBMappingArr["City"] = 'field4';
	$allowedColumnsAndTitleToDBMappingArr["State"] = 'field5';
	$allowedColumnsAndTitleToDBMappingArr["Postal Code"] = 'field6';
	$allowedColumnsAndTitleToDBMappingArr["Birthday"] = 'field7';
	$allowedColumnsAndTitleToDBMappingArr["Company"] = 'field8';
	$allowedColumnsAndTitleToDBMappingArr["How did you hear about us?"] = 'field9';
	$allowedColumnsAndTitleToDBMappingArr["Work Phone"] = 'field10';
	$allowedColumnsAndTitleToDBMappingArr["Cell Phone"] = 'field11';
	$allowedColumnsAndTitleToDBMappingArr["Street Address 2"] = 'field12';
	$allowedColumnsAndTitleToDBMappingArr["Emergency Contact F Name"] = 'field13';
	$allowedColumnsAndTitleToDBMappingArr["Emergency Contact L Name"] = 'field14';
	$allowedColumnsAndTitleToDBMappingArr["Emergency Contact Phone"] = 'field15';




	$allowedColumnsAndTitleToDBMappingArr["Date Created"] = 'date_created';

	function getTableDataForExport($contextArr = null){
		global $allowedColumnsAndTitleToDBMappingArr;
		if(!empty($contextArr) && is_array($contextArr)){
			// Fields: context => (e.g. table_display, etc...), order_by_column, barrier_value (value of order_by_value for paging), asc_desc_order, limit_count
			$context=$contextArr['sdie_context'];
		}
		$columnList = implode(',', array_values($allowedColumnsAndTitleToDBMappingArr));
		$dataname = $_SESSION['poslavu_234347273_admin_dataname'];
		$database = $_SESSION['poslavu_234347273_admin_database'];
		lavu_connect_dn($dataname,$database);

		if(!empty($context) && $context == 'table_display'){
			$orderByClause = '';
			$limitClause = '';
			$whereClause = '';
			$reverseResults = false;
			$error_code = '';
			//We check if it needs to be a double reverse (for paging back up.)
			if( $contextArr['sdie_asc_desc_order'] == 'asc' ){ //Rows are displayed ASC based on order_field.
				if(!empty($contextArr['sdie_barrier_value'])){
					if($contextArr['sdie_paging_direction'] == 'forward'){
						$whereClause = "WHERE `[sdie_order_by_column]` > '[sdie_barrier_value]'";
						$orderByClause = "ORDER BY [sdie_order_by_column] ASC";
						$limitClause = "LIMIT [sdie_limit_count]";
						$reverseResults = false;
						$error_code = "-est48b-";
					}else{
						$whereClause = "WHERE `[sdie_order_by_column]` < '[sdie_barrier_value]'";
						$orderByClause = "ORDER BY [sdie_order_by_column] DESC";//Double reverse
						$limitClause = "LIMIT [sdie_limit_count]";
						$reverseResults = true;
						$error_code = "-et8922-";
					}
				}else{ // First load listing ascending, no paging.
					$whereClause = "";
					$orderByClause = "ORDER BY [sdie_order_by_column] ASC";
					$limitClause = "LIMIT [sdie_limit_count]";
					$reverseResults = false;
					$error_code = "-87ey5t-";
				}
			}else{ ////Rows are displayed DESC based on order_field.
				if(!empty($contextArr['sdie_barrier_value'])){
					if($contextArr['sdie_paging_direction'] == 'forward'){
						$whereClause = "WHERE `[sdie_order_by_column]` < '[sdie_barrier_value]'";
						$orderByClause = "ORDER BY [sdie_order_by_column] DESC";
						$limitClause = "LIMIT [sdie_limit_count]";
						$reverseResults = false;
						$error_code = "-qwertp-";
					}else{
						$whereClause = "WHERE `[sdie_order_by_column]` > '[sdie_barrier_value]'";
						$orderByClause = "ORDER BY [sdie_order_by_column] ASC";
						$limitClause = "LIMIT [sdie_limit_count]";
						$reverseResults = true;
						$error_code = "-209348-";
					}
				}else{
					$whereClause = "";
					$orderByClause = "ORDER BY [sdie_order_by_column] DESC";
					$limitClause = "LIMIT [sdie_limit_count]";
					$reverseResults = false;
					$error_code = "-jjhksfa-";
				}
			}
			$result = lavu_query("SELECT $columnList FROM `med_customers` $whereClause $orderByClause $limitClause ", $contextArr);
			///$result = lavu_query("SELECT $columnList FROM `med_customers` $whereClause $orderByClause $limitClause");
			if(!$result){echo "Error trying to receive data $error_code :".lavu_dberror(); return false;}
		}else if(!empty($context) && $context == 'search_table_display'){
			$searchTermString = trim($contextArr['sdie_search_value']);
			$queryString = '';
			$queryString .= "SELECT $columnList FROM `med_customers` WHERE ";
			$i = 0;
			foreach($allowedColumnsAndTitleToDBMappingArr as $columnDisplayTitleAsKey => $dbColumnNameAsVal){
				$queryString .= "`".$dbColumnNameAsVal."` LIKE '%[1]%' ";
				if(++$i < count($allowedColumnsAndTitleToDBMappingArr)){
					$queryString .= "OR ";
				}
			}
			$queryString .= " LIMIT 50";
			$result = lavu_query($queryString, $searchTermString);
		}
		else{
			//This is for the export.
			$result = lavu_query("SELECT $columnList FROM `med_customers` ORDER BY `id` ASC");
			if(!$result){echo "Error trying to receive data -tdooh- :".lavu_dberror(); return false;}
		}


		//Build the data array.
		$dataArr = array();
		$needKeys = true;
		$keysArrFirstRow;
		while($currRow = mysqli_fetch_assoc($result)){
			if($needKeys){
				$keysArrFirstRow = array_keys($currRow);
				$needKeys = false;
			}
			$dataArr[] = array_values($currRow);
		}
		if($reverseResults){
			$dataArr = array_reverse($dataArr);
		}
		$returnArr = array_merge(array($keysArrFirstRow), $dataArr);
		return $returnArr;
	}


	require_once(dirname(__FILE__).'/../../resources/simple_display_import_export.php');
	$datetime = date('Y-m-d H:i:s');
	setSimpleDisplayTableTitleName('Customers');
	setSimpleDisplayTable('med_customers');
	//For poslavu_MAIN_db such as for lavu_gift_cards
	//setSimpleDisplayDatabase('');//Used for special dbs like poslavu_MAIN_db, or poslavu_QUICKBOOKS_db etc.

	setSimpleDisplayTitles2DBColumnsMap( $allowedColumnsAndTitleToDBMappingArr );//array("Last Name"=>"l_name","First Name"=>"f_name","Email"=>'field1') );

	setSimpleDisplayDefaultValuesForDBMap( array('locationid'=>'1','date_created'=>$datetime) );   //TODO, SHOULD VALIDATE THE DEFAULT VALUE MAP, THAT COLUMNS ACTUALLY EXIST.
	//setSimpleDisplayOrderByColumn4TableDisplay('l_name');
	//TODO consider if this should be set or not...
	//setSimpleDisplayDisallowDuplicatesAcrossColumns( array('dataname','name') );
	setSimpleDisplayOrderByColumn4TableDisplay('l_name');

	setSimpleDisplayMandatoryTitleColumns( array("First Name","Last Name") );
	setSimpleDisplayOptionalTitlesColumns( array("Email","Phone Number","Street Address","City","State",
		"Postal Code","Birthday","Company","How did you hear about us?","Work Phone","Cell Phone","Street Address 2",
		"Emergency Contact F Name","Emergency Contact L Name","Emergency Contact Phone") );

	$exampleText  = "First Name,Last Name,Email,Phone Number,Cell Phone,How did you hear about us?\n";
	$exampleText .= "John,Smith,jsmith@examp.com,555-555-5555,555-555-5555,Billboard\n";
	$exampleText .= 'Jane,Smith,janeSmith@example.com,555-123-4567,555-123-4567,"From someone, I think"'."\n";
	$exampleText .= "...";
	setSimpleDisplayCSVExampleText($exampleText);

	//setSimpleDisplayUpdateWhenColumnsAreEqualArrays();
	setQueryFunctionForExportArrays('getTableDataForExport');
	setSimpleDisplayExportFilePathForPostbackURL(__FILE__);
	simpleDisplayLaunch();
