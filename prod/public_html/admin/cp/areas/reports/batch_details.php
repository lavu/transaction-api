<?php
	function display_datetime($d)
	{
		$d_parts = explode(" ",$d);
		if(count($d_parts) > 1)
		{
			$t_parts = explode(":",$d_parts[1]);
			$d_parts = explode("-",$d_parts[0]);
			
			if(count($d_parts) > 2 && count($t_parts) > 2)
			{
				return date("m/d/Y h:i:s a",mktime($t_parts[0],$t_parts[1],$t_parts[2],$d_parts[1],$d_parts[2],$d_parts[0]));
			}
			else return $d;
		}
		else return $d;
	}
	
	function display_date1($d)
	{
		$d_parts = explode("-",$d);
		
		if(count($d_parts) > 2)
		{
			return date("m/d",mktime(0,0,0,$d_parts[1],$d_parts[2],$d_parts[0]));
		}
		else return $d;
	}
	
	function display_price($p)
	{
		return "$" . number_format($p,2);
	}
	
	echo "<br><br>";
	require_once("/home/poslavu/public_html/admin/sa_cp/show_report.php");
	$date_info = show_report(admin_info("database"),"poslavu_MAIN_db","unified","index.php?mode={$section}_{$mode}","select_date");
	$date_start = $date_info['date_start'];
	$date_end = $date_info['date_end'];
	$time_start = $date_info['time_start'];
	$time_end = $date_info['time_end'];
	$start_datetime = $date_start . " " . $time_start;
	$end_datetime = $date_end . " " . $time_end;
	echo "<br><br>";
	echo "<style>";
	echo ".coltitle { font-family:Verdana,Arial; color:#888888; text-align:right; background-color:#f0f0f0; } ";
	echo "</style>";

	$batch_strs = array();
	$batch_strs['SW'] = array();
	$batch_strs['HC'] = array();
	
	$batch_query = lavu_query("select * from `cc_batches` where `datetime` >= '[1]' and `datetime` <= '[2]' and `batch_item_count` > 0 group by concat(`hc`,'-',`batch_no`) order by `datetime` asc",$start_datetime,$end_datetime);
	while($batch_read = mysqli_fetch_assoc($batch_query))
	{
		$last_batch_query = lavu_query("select * from `cc_batches` where `datetime` < '[1]' and `batch_no` * 1 < '[2]' * 1 and `hc`='[3]' order by `batch_no` * 1 desc, `datetime` desc limit 1",$batch_read['datetime'],$batch_read['batch_no'],$batch_read['hc']);
		if(mysqli_num_rows($last_batch_query))
		{
			$last_batch_read = mysqli_fetch_assoc($last_batch_query);
			$batch_start_date = $last_batch_read['datetime'];
		}
		else $batch_start_date = "0000-00-00 00:00:00";
		$batch_end_date = $batch_read['datetime'];
		$hc = $batch_read['hc'];

		if($hc=="1") $btype = "HC"; else $btype = "SW";
		
		$set_num_style = "color:#880000";
		$set_price_style = "color:#880000";

		$show_range_start = display_datetime($batch_start_date);		
		$str = "";
		//$str .= "<br><br>";
		//echo "Batch Start: $batch_start_date<br>";

		$str .= "<table><tr>";
		$str .= "<td valign='top'>";
		$str .= "<table style='border:solid 1px #aaaaaa' cellspacing=0 cellpadding=6>";
		$str .= "<tr><td class='coltitle'>Batch Date</td><td>" . display_datetime($batch_read['datetime']) . "</td></tr>";
		//$str .= "<tr><td class='coltitle'>Range Start</td><td>[show_range_start]</td></tr>";
		$str .= "<tr><td class='coltitle'>Gateway</td><td>" . $batch_read['gateway'] ."</td></tr>";
		$str .= "<tr><td class='coltitle'>Batch Item Count</td><td style='[numstyle]'>" . $batch_read['batch_item_count'] . "</td></tr>";
		$str .= "<tr><td class='coltitle'>Net Batch total</td><td style='[pricestyle]'>" . display_price($batch_read['net_batch_total']) . "</td></tr>";
		$str .= "<tr><td class='coltitle'>Type</td><td>";
		if($batch_read['hc']=="1") $str .= "Hosted Checkout (Manual)"; else $str .= "Regular (Swipe)";
		$str .= "</td></tr>";
		
		if($hc=="0")
		{
			$where_cond = "`mpshc_pid`=''";
		}
		else
		{
			$where_cond = "`mpshc_pid`!=''";
		}
		
		$trans_query = lavu_query("select count(*) as `count`, sum(IF(`action`='Refund',0 - (`total_collected` + `tip_amount`),`total_collected` + `tip_amount`)) as `total` from `cc_transactions` where `datetime`>'[1]' and `datetime`<'[2]' and $where_cond and `pay_type_id`='2' and (`action`='Refund' or `action`='Sale') and `voided` < 1 AND `auth` = '0'",$batch_start_date,$batch_end_date);
		if(mysqli_num_rows($trans_query))
		{
			/*$count = 0;
			$total = 0;
			while ($trans_read = mysqli_fetch_assoc($trans_query)) {
				$count++;
				$total += $trans_read['total'];
			}
			$trans_read = array();
			$trans_read['count'] = $count;
			$trans_read['total'] = $total;*/
		
			$use_start_batch_date = $batch_start_date;
			$unmatched_transactions = array("count"=>0);
			$trans_read = mysqli_fetch_assoc($trans_query);
			$trans_read_count = $trans_read['count'];
			$trans_read_total = $trans_read['total'];
			$specially_noted = "";
			if ($trans_read_count != $batch_read['batch_item_count']) {
				$trans_query_again = lavu_query("select IF(`action`='Refund',0 - (`total_collected` + `tip_amount`),`total_collected` + `tip_amount`) as `total`, `datetime`, `card_type`, `card_desc`, `order_id`, `action`, `auth_code`, `voided`, `void_notes` from `cc_transactions` where `datetime`>'[1]' and `datetime`<'[2]' and $where_cond and `pay_type_id`='2' and (`action`='Refund' or `action`='Sale') ORDER BY `datetime` DESC",$batch_start_date,$batch_end_date);
				if (mysqli_num_rows($trans_query_again)) {
					$count = 0;
					$void_count = 0;
					$sn_count = 0;
					$trans_read['total'] = 0;
					$um_card_types = array();
					while ($info = mysqli_fetch_assoc($trans_query_again)) {
						if ($info['voided'] < 1) {
							$count++;
							if ($count <= $batch_read['batch_item_count']) $trans_read['total'] += $info['total'];
							else {
								$unmatched_transactions['count']++;
								$unmatched_transactions['total'] += $info['total'];
								if (!isset($um_card_types[$info['card_type']])) {
									$ct_info = array();
									if ($info['action'] == "Refund") {
										$ct_info['s_amt'] = 0;
										$ct_info['r_amt'] = $info['total'];
									} else {
										$ct_info['s_amt'] = $info['total'];
										$ct_info['r_amt'] = 0;
									}
									$um_card_types[$info['card_type']] = $ct_info;
								} else {
									$ct_info = $um_card_types[$info['card_type']];
									if ($info['action'] == "Refund") $ct_info['r_amt'] += $info['total'];
									else $ct_info['s_amt'] += $info['total'];
									$um_card_types[$info['card_type']] = $ct_info;									
								}
								if ($count == ($batch_read['batch_item_count'] + 1)) $unmatched_transactions['end_date'] = $info['datetime'];
								if ($count == (mysqli_num_rows($trans_query_again) - $void_count)) $unmatched_transactions['start_date'] = $info['datetime'];
							}
						} else {
							$void_count++;
							if ($count == (mysqli_num_rows($trans_query_again) - $void_count)) $unmatched_transactions['start_date'] = $info['datetime'];
						}
						if (strstr($info['void_notes'], "LAVU_ADMIN:")) {
							//echo "<br>".print_r($info, true);
							$split_notes = explode("LAVU_ADMIN:", $info['void_notes']);
							$top_border = "";
							if ($specially_noted == "") {
								$specially_noted = "<table style='border:solid 1px #aaaaaa' cellspacing=0 cellpadding=6>";
								$specially_noted .= "<tr><td class='coltitle' style='color:#111111; text-align:center;'>Specially Noted Transaction(s)</td></tr>";
								$top_border = " border-top:solid 2px #aaaaaa';";
							}
							$sn_count++;
							$specially_noted .= "<tr><td style='font-size:11px;$top_border'>".$info['order_id']." &nbsp;".$info['card_type']." &nbsp;".$info['card_desc']." &nbsp;".$info['action']." &nbsp;".display_price($info['total'])." &nbsp;".$info['auth_code']."<br> ".$split_notes[1]."</td></tr>";
						}
					}
					if ($specially_noted != "") $specially_noted .= "</table>";
					if (($trans_read_count - $sn_count) > $batch_read['batch_item_count']) {
						$trans_read['count'] = $batch_read['batch_item_count'];
						$use_start_batch_date = $unmatched_transactions['end_date'];
						//echo "<br><br>";
						
						$astr = "";
						$astr .= "<table><tr>";
						$astr .= "<td valign='top'>";
						$astr .= "<table style='border:solid 1px #aaaaaa' cellspacing=0 cellpadding=6>";
						$astr .= "<tr><td class='coltitle' style='color:#111111; text-align:center;' colspan='2'>Possible Auto Batch(es)</td></tr>";
						$astr .= "<tr><td class='coltitle' style='border-top:solid 2px #aaaaaa;'>Range End</td><td style='border-top:solid 2px #aaaaaa'>" . display_datetime($unmatched_transactions['end_date']) . "</td></tr>";
						//$astr .= "<tr><td class='coltitle'>Range Start</td><td>" . display_datetime($unmatched_transactions['start_date']) . "</td></tr>";
						$astr .= "<tr><td class='coltitle'style='border-top:solid 2px #aaaaaa;'>Transaction Count</td><td style='border-top:solid 2px #aaaaaa; color:#660000'>" . $unmatched_transactions['count'] . "</td></tr>";
						$astr .= "<tr><td class='coltitle'>Transaction Total</td><td style='color:#660000'>" . display_price($unmatched_transactions['total']) . "</td></tr>";
						asort($um_card_types);
						$keys = array_keys($um_card_types);
						foreach ($keys as $type) {
							$s_amt = number_format((float)$um_card_types[$type]['s_amt'], $location_info['disable_decimal'], ".", "");
							if ($s_amt > 0) $astr .= "<tr><td class='coltitle'>".$type." - Sale</td><td>".display_price($s_amt)."</td></tr>";
							$r_amt = number_format((float)$um_card_types[$type]['r_amt'], $location_info['disable_decimal'], ".", "");
							if ($r_amt < 0) $astr .= "<tr><td class='coltitle'>".$type." - Refund</td><td>".display_price($r_amt)."</td></tr>";
						}
						$astr .= "</table>";
						$astr .= "</td>";
						if ($specially_noted != "") {
							$astr .= "<td valign='top'>".$specially_noted."</td>";
							$specially_noted = "";
						}
						$astr .= "</tr></table>";
						
						$show_range_start = display_datetime($unmatched_transactions['end_date']);
						
						$bdate = trim(substr($unmatched_transactions['start_date'],0,10));
						if(!isset($batch_strs[$btype][$bdate])) $batch_strs[$btype][$bdate] = array();
						$batch_strs[$btype][$bdate][$unmatched_transactions['start_date']] = $astr;
					} else $trans_read['total'] = $trans_read_total;
				}
			}


			/*if($trans_read['count'] != $batch_read['batch_item_count'])
				$style = "font-weight:bold";
			else
				$style = "";*/
				
			if($trans_read['count']==$batch_read['batch_item_count']) $set_num_style = "color:#008800; font-weight:bold";
			if(number_format($trans_read['total'],2)==number_format($batch_read['net_batch_total'],2)) $set_price_style = "color:#008800; font-weight:bold";
				
			//echo "<tr><td class='coltitle'>Date Range</td><td>" . display_datetime($batch_start_date) . " - " . display_datetime($batch_read['datetime']) . "</td></tr>";
			$str .= "<tr><td class='coltitle' style='border-top:solid 2px #aaaaaa'>Transaction Count</td><td style='border-top:solid 2px #aaaaaa; [numstyle]'>" . $trans_read['count'] . "</td></tr>";
			$str .= "<tr><td class='coltitle'>Transaction Total</td><td style='[pricestyle]'>" . display_price($trans_read['total']) . "</td></tr>";
			
			$t_query = lavu_query("select sum(IF(`action`='Refund',0 - (`total_collected` + `tip_amount`),`total_collected` + `tip_amount`)) as `total`, `action`, `card_type` from `cc_transactions` where `datetime`>'[1]' and `datetime`<'[2]' and $where_cond and `pay_type_id`='2' and (`action`='Refund' or `action`='Sale') and `voided` < 1 group by concat(`card_type`,`action`)",$use_start_batch_date,$batch_end_date);
			while($t_read = mysqli_fetch_assoc($t_query))
			{
				$str .= "<tr><td class='coltitle'>" . $t_read['card_type'] . " - " . $t_read['action'] . "</td><td>" . display_price($t_read['total']) . "</td></tr>";
			}
			
			//echo "<span style='$style'>Transaction Count: " . $trans_read['count'] . " - ".number_format($trans_read['total'],2)."</span><br>";
			/*if($trans_read['count'] < 20)
			{
				foreach($batch_read as $key => $val) echo $key . ": " . $val . "<br>";
				echo "<table>";
				$t_query = lavu_query("select * from `cc_transactions` where `datetime`>'[1]' and `datetime`<'[2]' and $where_cond and `pay_type_id`='2' and (`action`='Refund' or `action`='Sale') and `voided` < 1",$batch_start_date,$batch_end_date);
				while($t_read = mysqli_fetch_assoc($t_query))
				{
					echo "<tr>";
					foreach($t_read as $key => $val) echo "<td valign='top'>" . $val . "</td>";
					echo "</tr>";
				}
				echo "</table>";
			}*/
		}
		$str .= "</table>";
		$str .= "</td>";
		if ($specially_noted != "") {
			$str .= "<td valign='top'>".$specially_noted."</td>";
			$specially_noted = "";
		}
		$str .= "</tr></table>";
				
		$str = str_replace("[numstyle]",$set_num_style,$str);
		$str = str_replace("[pricestyle]",$set_price_style,$str);
		$str = str_replace("[show_range_start]",$show_range_start,$str);
		
		$bdate = trim(substr($batch_read['datetime'],0,10));
		if(!isset($batch_strs[$btype][$bdate])) $batch_strs[$btype][$bdate] = array();
		$batch_strs[$btype][$bdate][$batch_read['datetime']] = $str;
	}
	
	/*foreach($batch_strs as $bkey => $bval)
	{
		echo "<hr>";
		foreach($bval as $b2key => $b2val)
		{
			echo "<br><br>";
			echo $b2val;
		}
	}*/
	
	$date_start_parts = explode("-",$date_start);
	$start_ts = mktime(0,0,0,$date_start_parts[1],$date_start_parts[2],$date_start_parts[0]);
	
	$dts = $start_ts;
	$dd = date("Y-m-d",$dts);
	$rcount = 0;
	
	echo "<table cellspacing=0 cellpadding=4>";
	while($dd < $date_end && $rcount < 4000)
	{
		echo "<tr><td colspan='2' bgcolor='#777777' style='color:#ffffff; font-weight:bold' align='center'>".display_date1($dd)."</td></tr>";
		echo "<tr><td valign='top' style='border-top:solid 1px #999999; border-right:solid 4px #dddddd' width='360'>";
		
		if(isset($batch_strs['SW'][$dd]))
		{
			$bval = $batch_strs['SW'][$dd];
			foreach($bval as $b2key => $b2val)
			{
				echo $b2val;
				echo "<br><br>";
			}
		}
		else
		{
			echo "&nbsp;<br><br>";
		}
		
		echo "</td><td valign='top' style='border-top:solid 1px #999999' width='360'>";
		
		if(isset($batch_strs['HC'][$dd]))
		{
			$bval = $batch_strs['HC'][$dd];
			foreach($bval as $b2key => $b2val)
			{
				echo $b2val;
				echo "<br><br>";
			}
		}
		else
		{
			echo "&nbsp;<br><br>";
		}
		
		echo "</td></tr>";
		
		$dts += (60 * 60 * 24);
		$dd = date("Y-m-d",$dts);
		$rcount++;
	}
	echo "</table>";
?>
