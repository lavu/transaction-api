<?php
	$reportid = urlvar("reportid");
	if($in_lavu && $reportid)
	{
		require_once("/home/poslavu/public_html/admin/sa_cp/show_report.php");
		
		if($reportid=="export_fields_from_table")
		{
			$tablename = urlvar("tablename");
			$fields = urlvar("fields");
			
			$export_output = "";
			$export_ext = "txt";
			$export_row = "\r\n";
			$export_col = "\t";

			if($tablename!="" && $fields!="")
			{
				$fields = explode("|",$fields);
				$fieldlist = "";
				for($i=0; $i<count($fields); $i++)
				{
					if(trim($fields[$i])!="")
					{
						if($i > 0) 
						{
							$export_output .= $export_col;	
							$fieldlist .= ",";
						}
						$export_output .= $fields[$i];
						$fieldlist .= "`".ConnectionHub::getConn('rest')->escapeString($fields[$i])."`";
					}
				}
				
				//$export_output .= $tablename . $export_col . $fieldlist . $export_row;
				$t_query = lavu_query("select $fieldlist from `[1]` order by `id` asc",$tablename);
				while($t_read = mysqli_fetch_assoc($t_query))
				{
					$export_output .= $export_row;
					for($i=0; $i<count($fields); $i++)
					{
						if($i > 0) $export_output .= $export_col;
						$export_output .= $t_read[$fields[$i]];
					}
				}
			}
			
			export_report("report_".date("Y-m-d H:i").".".$export_ext,$export_output);
		}
	// lp-2892 Export to excel ********************************************************************************************start
	elseif ($reportid=="inventory_edit_ingredients"){
		$export_output = "";
		$export_ext = urlvar("export_type");

		// header row
		$export_output .="List Name,Ingredient,Quantity,Unit,Unit Cost,Par,Low Alert Level,Chain Reporting Group";
		$export_output .= "\r\n";

		// ingredient category details
		$category_query = lavu_query("select * from `ingredient_categories` where `_deleted`!='1'and `loc_id`='1' order by `title` asc");
		while($category_read = mysqli_fetch_assoc($category_query))
		{
			$qvars = array();
			$qvars['inventory_fieldname'] = "category";
			$qvars['inventory_primefield'] = "title";
			$qvars['inventory_orderby'] = "title";
			$qvars['matchfield'] = $category_read['id'];

			// ingredient details
			$inventory_query = lavu_query("select * from `ingredients` where _deleted!=1".$inventory_filter_code." and `[inventory_fieldname]`='[matchfield]'".$inventory_select_condition." order by [inventory_orderby] asc", $qvars);
			while($inventory_read = mysqli_fetch_assoc($inventory_query))
			{
				$export_output .= $category_read['title'].","; //ListName
				$export_output .= $inventory_read['title'].","; //Ingredient
				$export_output .= $inventory_read['qty'].","; //Quantity
				$export_output .= $inventory_read['unit'].","; //Unit
				$export_output .= $inventory_read['cost'].","; //Unit Cost
				$export_output .= $inventory_read['high'].","; //par
				$export_output .= $inventory_read['low'].",";  //Low Alert Level
				$chaingroup=($inventory_read['chain_reporting_group']==-2?"No Chain Group":"Default Chain Group");
				$export_output .= $chaingroup.","; //Chain Reporting Group
				$export_output .= "\r\n";
			}
		}
		export_report("inventory_".date("Y-m-d H:i").".".$export_ext,$export_output);
	}
		// lp-2892 Export to excel *****************************************************************************************************END
		 
		
		// lp-4045 Export legacy Inventory ********************************************************************************************start
		elseif ($reportid=="inventory_legacy_ingredients"){
			$export_output = "";
			$export_ext = urlvar("export_type");

			// header row
			$export_output .="ITEM,STORAGE LOCATION, CATEGORY, VENDOR, STOCK, PURCHASE UOM (symbol), SALES UOM (symbol), lAST COST";
			$export_output .= "\r\n";

			// ingredient category details
			$category_query = lavu_query("select * from `ingredient_categories` where `_deleted`!='1'and `loc_id`='1' order by `title` asc");
			while($category_read = mysqli_fetch_assoc($category_query))
			{
				$qvars = array();
				$qvars['inventory_fieldname'] = "category";
				$qvars['inventory_primefield'] = "title";
				$qvars['inventory_orderby'] = "title";
				$qvars['matchfield'] = $category_read['id'];

				// ingredient details
				$inventory_query = lavu_query("select * from `ingredients` where _deleted!=1".$inventory_filter_code." and `[inventory_fieldname]`='[matchfield]'".$inventory_select_condition." order by [inventory_orderby] asc", $qvars);
				while($inventory_read = mysqli_fetch_assoc($inventory_query))
				{
					$export_output .= $inventory_read['title'].","; //ITEM
					$export_output .= "Pantry,"; //STORAGE LOCATIONS
					$export_output .= "Condiments,"; //CATEGORY
					$export_output .= ","; //VENDOR
					$export_output .= $inventory_read['qty'].","; //STOCK
					$export_output .= "kg,"; //PURCHASE UOM (symbol)
					$export_output .= "oz,"; //SALES UOM (symbol)
					$export_output .= $inventory_read['cost'].","; //LAST COST
					$export_output .= "\r\n";
				}
			}
			export_report("inventory_".date("Y-m-d H:i").".".$export_ext,$export_output);
			// lp-4045 Export legacy Inventory *****************************************************************************************************END
	}
		else
		{
			$export_type = urlvar("export_type","txt");
			show_report(admin_info("database"),"poslavu_MAIN_db","unified","index.php?mode=$mode",$reportid,"export:".$export_type);
		}
	}
?>
