<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Untitled Document</title>

<style type="text/css">

.leftForm{
	float:left;
	width:250px;
	text-align:right;
	margin-left:150px;
}
.rightForm{
	float:left;
	width:250px;
	text-align:left;
	margin-left:5px;
}
.clearForm{
	clear:both;
}
.rowElement{
	float:left;
	text-align:right;
	margin-left:5px;
	border-right:0px solid #000;
}
.innerLeftForm{
	float:left;
	width:100px;
	text-align:right;
}
.innerRightForm{
	float:left;
	width:150px;
	text-align:left;
}
.clearInnerForm{
	clear:both;
}


.row{
	
}

.header{
	font-weight:700;
}

.clearRow{
	clear:both;
}

.re0{
	width:125px;
	margin-right:10px;
	border-right:1px solid #333;
}

.re1{
	width:70px;
	font-size:85%;
	
}

.re2{
	width:100px;
	
}

.re3{
	width:100px;
	
}

.re4{
	width:75px;
	
}

.re5{
	width:100px;
	
}
 
.re6{
	width:100px;
	
}

.re7{
	width:100px;
	
}

.re8{
	width:100px;
	
}
.re9{
	width:60px;
	padding-right:5px;
	
}

label{
	font-size:85%;
}
</style>

</head>

<body>
<form name="select_date_form" method="post" action="">
<div class="leftForm"><label>Start Date</label></div>
<div class="rightForm">
<select name="start_month" id="start_month">
	<option value=""></option>
	<?php 
		for ($month=1; $month<=12; $month++){
				echo "<option value='".pad($month,2)."'>$month</option>";
		}//for
	?>
</select>

<select name="start_day" id="start_day">
	<option value=""></option>
	<?php 
		for ($day=1; $day<=31; $day++){
				echo "<option value='".pad($day,2)."'>$day</option>";
		}//for
	?>
</select>

<select name="start_year" id="start_year">
	<option value=""></option>
	<?php 
		for ($year=2012; $year<=2014; $year++){
				echo "<option value='$year'>$year</option>";
		}//for
	?>
</select>
</div><!--rightForm-->
<div class="clearForm"></div>
<div style='display:none;'>
<div class="leftForm">
<label>End</label>
</div>
<div class="rightForm">
<select name="end_month" id="end_month">
	<option value=""></option>
	<?php 
		for ($month=1; $month<=12; $month++){
				echo "<option value='".pad($month,2)."'>$month</option>";
		}//for
	?>
</select>

<select name="end_day" id="end_day">
	<option value=""></option>
	<?php 
		for ($day=1; $day<=31; $day++){
				echo "<option value='".pad($day,2)."'>$day</option>";
		}//for
	?>
</select>

<select name="end_year" id="end_year">
	<option value=""></option>
	<?php 
		for ($year=2012; $year<=2014; $year++){
				echo "<option value='$year'>$year</option>";
		}//for
	?>
</select>
</div><!--rightForm-->
<div class="clearForm"></div>
</div>


<div class="leftForm">
<label>What menu category should be calculated in the 'Events Department' section?</label>
</div>
<div class="rightForm">
<select name="event_department" id="event_department">
	<option value=""></option>
	<?php
		$get_event_dep_res = lavu_query("SELECT * FROM menu_categories WHERE menu_id='12' AND active='1' AND _deleted!='1' ORDER BY name");
		$get_menu_name_from_id = array();
		$build_check_box_string = "";/* we'll build this in this loop and use it later so we don't have to run two queries*/
		while($get_event_dep_row = mysqli_fetch_assoc($get_event_dep_res)){
				echo "<option value='".$get_event_dep_row['id']."'>".$get_event_dep_row['name']."</option>";
				$build_check_box_string .= "<div class='innerLeftForm'>".$get_event_dep_row['name']."</div><div class='innerRightForm'><input type='checkbox' id='revenue_".$get_event_dep_row['id']."' name='revenue_".$get_event_dep_row['id']."' /></div><div class='clearInnerForm'></div>";
				$get_menu_name_from_id[$get_event_dep_row['id']] = $get_event_dep_row['name'];
		}//while
	?>
</select>
</div><!--rightForm-->
<div class="clearForm"></div>


<div class="leftForm"><label>Which employee category would you like to calculate an efficiency report for?</label></div>
<div class="rightForm">
	<select name="role_efficiency" id="role_efficiency">
			<option value=""></option>
			<?php
				$get_roles_info = lavu_query("SELECT id,title FROM roles");
				while ($get_roles_row = mysqli_fetch_assoc($get_roles_info)){
						echo "<option value='".$get_roles_row['id']."'>".$get_roles_row['title']."</option>";
				}//while
			?>
	</select>
</div>
<div class="clearForm"></div>

<div class="leftForm"><label>Calculate track hrs for this employee category</label></div>
<div class="rightForm">
	<select name="track_efficiency" id="track_efficiency">
			<option value=""></option>
			<?php
				$get_roles_info = lavu_query("SELECT id,title FROM roles");
				while ($get_roles_row = mysqli_fetch_assoc($get_roles_info)){
						echo "<option value='".$get_roles_row['id']."'>".$get_roles_row['title']."</option>";
				}//while
			?>
	</select>
</div>
<div class="clearForm"></div>


<div class="leftForm" style='display:none;'><label>Which of these menu categories should be factored into the total revenue?</label></div>
<div class="rightForm" style='display:none;'><?php echo $build_check_box_string;?></div>
<div class="clearForm"></div>
<div class="leftForm">&nbsp;</div>
<div class="rightForm">
<input type="submit" value="Go"/>
</div><!--rightForm-->
<div class="clearForm"></div>
</form>
<script type="text/javascript">
//function makeYear(){
		var start_date = new Date(); 
		start_day = start_date.getDate();
		start_month = start_date.getMonth();
		start_year = start_date.getFullYear();
		document.getElementById("start_day").value = start_day;
		document.getElementById("start_month").value = start_month*1;//javascript starts counting months at 0, not 1
		document.getElementById("start_year").value = start_year;
		
		
		var end_date = new Date(); 
		end_day = end_date.getDate();
		end_month = end_date.getMonth();
		end_year = end_date.getFullYear();
		document.getElementById("end_day").value = end_day*1 + 6;
		document.getElementById("end_month").value = end_month*1;//javascript ends counting months at 0, not 1
		document.getElementById("end_year").value = end_year;
		
		
		//alert(start_date);
	//	return false;
//}//makeYear()
</script>



<?php

	//$locationid = sessvar("loc_id");
	//echo "locatin: $locationid<br/>";
	$sql_query = lavu_query("SELECT * FROM users");
 	while ($rows = mysqli_fetch_assoc($sql_query)){
			//echo $rows['f_name']." ".$rows['l_name']."<br/>";
	}
	echo "event_dept_index: $event_dept_index<br/>";
?>
<div class="row">
	<h2>Performance Indicator</h2>
</div>

<div class="row header">
		
		<div class="rowElement re0">
			Category
		</div>
		
		<div class="rowElement re1">
			Current Week
		</div>
		
		<div class="rowElement re2">
			Prior Week
		</div>
		
		<div class="rowElement re3">
			Prior Year
		</div>
		
		<div class="rowElement re4">
			Plan
		</div>
		
		
		
		<div class="rowElement re6">
			Trend
		</div>
		
		<div class="rowElement re7">
			Prior Year <span style="font-size:75%;">&#43;&#47;&#45;</span>
		</div>
		
		<div class="rowElement re8">
			&#37; to Goal
		</div>
</div><!--row-->
<div class="clearRow"></div>





<?php

function trend($current_val,$prior_val){
		$trend_val = 100*( ( ($current_val*1)/($prior_val*1) ) -1);
		return $trend_val;
}//trend()
 


function calculatePrepayments($start_date,$end_date,$event_dept_index){
		//this function needs to be verified for accuracy
		$output = array();
		$res1 = lavu_query("SELECT order_id FROM orders WHERE (opened>='$start_date' AND opened<='$end_date' AND LEFT(closed,10)!='$start_date' AND cash_paid>'0') OR (opened>='$start_date' AND opened<='$end_date' AND LEFT(closed,10)!='$start_date' AND card_paid>'0')");//we'll have to insert variables here later
		
		//echo "event: $event_dept_index<br/>";
		//$res_num_customers = lavu_query("SELECT COUNT(id) FROM "
	 					
		$prepayment_amount = 0;
						
		while ($row1 = mysqli_fetch_assoc($res1)){
					$orderid = $row1['order_id'];//i need the order id so i can go to order_contents and figure out the actual items
									
					$res2 = lavu_query("SELECT item_id,price FROM order_contents WHERE order_id='$orderid'");//array
									
					while($row2 = mysqli_fetch_assoc($res2)){
							$item_id = $row2['item_id'];
														
							$resMenuItems = lavu_query("SELECT id,category_id,menu_id FROM menu_items WHERE id='$item_id' AND category_id='$event_dept_index'");
							if (mysqli_num_rows($resMenuItems)*1 > 0){
									$prepayment_amount += $row2['price']*1;
							}//if
					}//while
		}//while
						
		$output["prepay_amount"] = $prepayment_amount;
		return $output;
}//calculatePrepayments


function calculateTotal($start_date,$end_date,$menu_category_id,$event_dept_index){
		$output = array();
		$res1 = lavu_query("SELECT order_id FROM orders WHERE closed>='$start_date' AND closed<DATE_ADD('$end_date',INTERVAL 29 HOUR) AND void='0'");//we'll have to insert variables here later
		
		
		//$res_num_customers = lavu_query("SELECT COUNT(id) FROM "
	 	
						$total_category_price = 0;
						$event_dept_index_sales = 0;//any sales from the user selected Event department index are collected with this.
						$num_customers = 0;
						while ($row1 = mysqli_fetch_assoc($res1)){
									$orderid = $row1['order_id'];//i need the order id so i can go to order_contents and figure out the actual items
									
									$res2 = lavu_query("SELECT item_id,subtotal FROM order_contents WHERE order_id='$orderid' AND void='0'");
									
									while($row2 = mysqli_fetch_assoc($res2)){
												$item_id = $row2['item_id'];
															
												$resMenuItems = lavu_query("SELECT id,category_id,menu_id FROM menu_items WHERE id='$item_id'");
												$count_this_order = false;
												while($rowMenuItems = mysqli_fetch_assoc($resMenuItems)){
														if ($rowMenuItems['category_id'] == $menu_category_id){
																	$count_this_order = true;
																	$price = $row2['subtotal'];
																	$total_category_price += $price*1;
														}//if
														if ($rowMenuItems['category_id'] == $event_dept_index){
																	$price = $row2['subtotal'];
																	$event_dept_index_sales += $price*1;
														}//if
												}//while
												if ($count_this_order){
														$num_customers++;
														$count_this_order = false;
												}//if
												//echo "orderid: $item_id<br/>";
									}//while
						}//while
						
						/*this is the same everytime it is run. I put it here so I could avoid having to format the start_date and end_date for the current, previous week, and previous year. It gets run more times than it has to but it was easier to code*/
						$num_labor_hours_res = lavu_query("SELECT SUM(hours) FROM clock_punches WHERE time>='$start_date' AND time_out<DATE_ADD('$end_date',INTERVAL 29 HOUR)");
						$num_labor_hour_row = mysqli_fetch_assoc($num_labor_hours_res);
						$output["sum_labor_hours"] = $num_labor_hour_row['SUM(hours)'];
						
						$output["num_customers"] = $num_customers;
						
						$output["total_category_price"] = $total_category_price;
						$output["event_dept_index_sales"] = $event_dept_index_sales;
						return $output;
}//calculateTotal()


function thisPerThat($this,$that){
		/* Returns  $this divided $that */
		$val = ($this*1)/($that*1);
		return $val;
}//thisPerThat()

/* This should be set to use the current date if the user hasn't specified yet. If these values aren't set the script crashes */
		$start_date_day = "14";
		$start_date_month = "7";
		$start_date_year = "2012";
		$start_date = $start_date_year."-".$start_date_month."-".$start_date_day;
		
		$end_date_day = $start_date_day*1 + 6;
		$end_date_month = $start_date_month;
		$end_date_year = $start_date_year ;
		$end_date = $end_date_year."-".$end_date_month."-".$end_date_day;

if ( ($_POST['start_day'] != "") && ($_POST['start_month'] != "") && ($_POST['start_year'] != "") ){
		$start_date_day = $_POST['start_day'];
		$start_date_month = $_POST['start_month'];
		$start_date_year = $_POST['start_year'];
		
		//$lavu_query("INSERT INTO config (location,type,setting) VALUES ('$location_id','kbi_setting','$')");
		
		$end_date_day = $start_date_day*1 + 6;
		$end_date_month = $start_date_month;
		$end_date_year = $start_date_year ;
		/*
		$end_date_day = pad($_POST['end_day'],2);
		$end_date_month = pad($_POST['end_month'],2);
		$end_date_year = $_POST['end_year'];
		*/
		
}//if
		
		/* the padding of the number with a 0 in front is necessary for the date format */
		$start_date_day = pad($start_date_day,2);
		$start_date_month = pad($start_date_month,2);
		$end_date_day = pad($end_date_day,2);
		$end_date_month = pad($end_date_month,2);
		
		$start_date = $start_date_year."-".$start_date_month."-".$start_date_day;
		$end_date = $end_date_year."-".$end_date_month."-".$end_date_day;


		//$track_efficiency = $_POST['track_efficiency'];// for track efficiency
		$event_dept_index = $_POST['event_department'];

/* set variables here from database, update if necessary */

//event_dept
if ($_POST['event_department'] != ""){
		$get_event_dept_res = lavu_query("SELECT id FROM config WHERE type='kbi_setting' AND setting='event_department' AND location='$locationid'");
		$set_event_dept = $_POST['event_department'];//CSR, Mechanic, Trackside, etc....	
		if (mysqli_num_rows($get_event_dept_res)*1 < 1){
					lavu_query("INSERT INTO config (location,type,setting,value) VALUES ('$locationid','kbi_setting','event_department','$set_event_dept')");
		}//if
		lavu_query("UPDATE config SET value='$set_event_dept' WHERE location='$locationid' AND type='kbi_setting' AND setting='event_department'");
}//if
$get_event_dept_id_res = lavu_query("SELECT value FROM config WHERE location='$locationid' AND type='kbi_setting' AND setting='event_department'");
$get_event_dept_id_row = mysqli_fetch_assoc($get_event_dept_id_res);
$event_dept_index = $get_event_dept_id_row['value'];
		//echo "event dept: $event_dept_index<br/>";


//role_efficiency
if ($_POST['role_efficiency'] != ""){
		$get_role = lavu_query("SELECT id FROM config WHERE type='kbi_setting' AND setting='role_efficiency' AND location='$locationid'");
		$set_role_efficiency = $_POST['role_efficiency'];//CSR, Mechanic, Trackside, etc....	
		if (mysqli_num_rows($get_role)*1 < 1){
					lavu_query("INSERT INTO config (location,type,setting,value) VALUES ('$locationid','kbi_setting','role_efficiency','$set_role_efficiency')");
		}//if
		lavu_query("UPDATE config SET value='$set_role_efficiency' WHERE location='$locationid' AND type='kbi_setting' AND setting='role_efficiency'");
}//if
$role_effic = lavu_query("SELECT value FROM config WHERE location='$locationid' AND type='kbi_setting' AND setting='role_efficiency'");
$role_effic_row = mysqli_fetch_assoc($role_effic);
$role_efficiency = $role_effic_row['value'];
		//echo "role efficiency: $role_efficiency<br/>";
		
//track_efficiency
if ($_POST['track_efficiency'] != ""){
		$get_track = lavu_query("SELECT id FROM config WHERE type='kbi_setting' AND setting='track_efficiency' AND location='$locationid'");
		$set_track_efficiency = $_POST['track_efficiency'];//CSR, Mechanic, Trackside, etc....	
		if (mysqli_num_rows($get_track)*1 < 1){
					lavu_query("INSERT INTO config (location,type,setting,value) VALUES ('$locationid','kbi_setting','track_efficiency','$set_track_efficiency')");
		}//if
		lavu_query("UPDATE config SET value='$set_track_efficiency' WHERE location='$locationid' AND type='kbi_setting' AND setting='track_efficiency'");
}//if
$track_effic = lavu_query("SELECT value FROM config WHERE location='$locationid' AND type='kbi_setting' AND setting='track_efficiency'");
$track_effic_row = mysqli_fetch_assoc($track_effic);
$track_efficiency = $track_effic_row['value'];
		//echo "track efficiency: $track_efficiency<br/>";

/* End set variables section */				
				
				/* Calculate total sales by menu category, given a time range in YYYY-mm-dd format */
			
			
			/* Calculate revenue per customer using user selected values If a value is set to 'on', that means the user wants this menu category to be included in the calcuation of total revenue, and thus it will be added to it's respective array. Values are calculated at this part of the script but will appear in the "Revenue Per Customer" row of the report under the section 'Performance Indicator'. */
			
			
			$ids_to_include_in_revenue_calculation = array();
			if (isset($_POST)){
					$index = 0;
					foreach($_POST as $key => $value){
							
							//echo "$key: $value<br/>";
							$key_array = explode("_",$key);// get the menu_category_id

							if ($key_array[0] == "revenue"){
									
									/* The checked values (menu_category_ids) are stored in an array, so lter when we calculated revenue per week in the "Performance Indicator" section of the report, we can run in_array($menu_category_id,$ids_to_include_in_revenue_calculation) to find out if the revenue from that menu category should be included in the "revenue per customer" part of the "Performance Indicator section*/
									$ids_to_include_in_revenue_calculation[$index] = $key_array[1];
									//echo "$key: $value and ".$key_array[1];
									$index++;
							}//if
					}//foreach
			}//if
			
			//foreach ($ids_to_include_in_revenue_calculation as $key => $value){
			//	echo "$key: $value<br/>";
			//}
			//echo "IDS: ".print_r($ids_to_include_in_revenue_calculation);
			
			/* Check all the checkboxes that the user selected so it shows up on page reload */
			echo "<script type='text/javascript'>";
			echo "document.getElementById('event_department').value = $event_dept_index;";
				for ($i=0;$i<count($ids_to_include_in_revenue_calculation);$i++){
							echo "document.getElementById('revenue_".$ids_to_include_in_revenue_calculation[$i]."').checked = true;";	
				}//for
				//alert('$ids_to_include_in_revenue_calculation');
			echo "</script>";
			
			
			
			$does_index_exist = lavu_query("SHOW INDEX FROM orders WHERE KEY_NAME = 'closed_index'");
			if (mysqli_num_rows($does_index_exist) < 1){
					echo "Creating index".mysqli_num_rows($does_index_exist)."<br/>";
					$create_index = lavu_query("CREATE INDEX closed_index ON orders (closed);");
			}//if
						
						
						
			$does_index_exist = lavu_query("SHOW INDEX FROM order_contents WHERE KEY_NAME = 'orderid_index'");
			if (mysqli_num_rows($does_index_exist) < 1){
					echo "Creating index on order_contents".mysqli_num_rows($does_index_exist)."<br/>";
					$create_index = lavu_query("CREATE INDEX orderid_index ON order_contents (order_id);");
			}//if
			
			$cur_week_revenue_per_customer = array();
			$prior_week_revenue_per_customer = array();
			$prior_year_revenue_per_customer = array();
			
			$total_revenue_all_menu_categories = array();
			$total_revenue_all_menu_categories["current_week"] = 0;
			
			function get_cat_data_for_week($start_datetime,$end_datetime,$location_id,$event_dept_index=0)
			{
				// total_category_price
				// num_customers
				// total_revenue
				// sum_labor_hours
				$event_dept_index_sales = 0;
				$str_cat_query = "select sum(`order_contents`.`subtotal_with_mods` - `order_contents`.`discount_amount` + `order_contents`.`tax_amount`) AS `field0`,`orders`.`void` AS `field1`,`orders`.`opened` AS `field2`,`orders`.`guests` AS `order_guests`,`order_contents`.`forced_modifiers_price` AS `field3`,`orders`.`location_id` AS `field4`,`order_contents`.`loc_id` AS `field5`,`orders`.`cashier` AS `field6`,`orders`.`cashier_id` AS `field7`,`order_contents`.`item_id` AS `field8`,`order_contents`.`void` AS `field9`,`order_contents`.`itax` AS `field10`,sum(`order_contents`.`quantity`) AS `field11`,`menu_categories`.`name` AS `field12`,`menu_categories`.`id` AS `field13`,sum((`order_contents`.`price` * `order_contents`.`quantity`) - `order_contents`.`itax`) AS `field14`,sum((`order_contents`.`modify_price` + `order_contents`.`forced_modifiers_price`) * `order_contents`.`quantity`) AS `field15`,sum(`order_contents`.`idiscount_amount`) AS `field16`,sum(`order_contents`.`subtotal_with_mods` - `order_contents`.`idiscount_amount`- `order_contents`.`itax`) AS `field17`,sum(`order_contents`.`discount_amount`) AS `field18`,sum(`order_contents`.`subtotal_with_mods` - `order_contents`.`discount_amount` - `order_contents`.`idiscount_amount` - `order_contents`.`itax`) AS `field19`,sum(`order_contents`.`tax_amount` + `order_contents`.`itax`) AS `field20`,sum(`order_contents`.`subtotal_with_mods` - `order_contents`.`discount_amount` + `order_contents`.`tax_amount` - `order_contents`.`idiscount_amount`) AS `field21` from `order_contents` LEFT JOIN `menu_items` ON `order_contents`.`item_id` = `menu_items`.`id` LEFT JOIN `orders` ON `order_contents`.`order_id` = `orders`.`order_id` LEFT JOIN `menu_categories` ON `menu_items`.`category_id` = `menu_categories`.`id` where `orders`.`void` = 0 and `orders`.`closed` >= '[start_datetime]' and `orders`.`closed` <= '[end_datetime]' and `orders`.`location_id` = '[location_id]' and `order_contents`.`loc_id` = `orders`.`location_id` and `order_contents`.`item_id` > 0 and (`order_contents`.`quantity` * 1) > 0 group by `field13` order by sum(`order_contents`.`subtotal_with_mods`) desc";
				$cat_data = array();
				$num_labor_hours_res = lavu_query("SELECT SUM(hours) FROM clock_punches WHERE time>='[1]' AND time_out<DATE_ADD('[2]',INTERVAL 29 HOUR)",$start_datetime,$end_datetime,$location_id);
				$num_labor_hour_row = mysqli_fetch_assoc($num_labor_hours_res);
				$output["sum_labor_hours"] = $num_labor_hour_row['SUM(hours)'];
				$cat_query = lavu_query($str_cat_query,array("start_datetime"=>$start_datetime,"end_datetime"=>$end_datetime,"location_id"=>$location_id));
				while($cat_read = mysqli_fetch_assoc($cat_query)) 
				{
					$cat_read['sum_labor_hours'] = $sum_labor_hours;
					$cat_read['total_category_price'] = $cat_read['field17'];
					//$cat_read['total_revenue'] = "";
					$cat_read['num_customers'] = $cat_read['order_guests'];
					$cat_data[] = $cat_read;
					if ($cat_read['field13'] == $event_dept_index){
						$event_dept_index_sales += $cat_read['field17'] * 1;
					}//if
				}
				for($n=0; $n<count($cat_data); $n++)
					$cat_data[$n]['event_dept_index_sales'] = $event_dept_index_sales;
				return $cat_data;
			}
			
			$start_datetime = "2012-08-01 00:00:00";
			$end_datetime = "2012-08-01 00:00:00";
			$cat_data = array();
			$cat_data['current'] = get_cat_data_for_week($start_datetime,$end_datetime,$location_id,$event_dept_index);
			$cat_data['prior_week'] = get_cat_data_for_week($start_datetime,$end_datetime,$location_id,$event_dept_index);
			$cat_data['prior_year'] = get_cat_data_for_week($start_datetime,$end_datetime,$location_id,$event_dept_index);
			
			//$output["num_customers"] = $num_customers;
			//$output["total_category_price"] = $total_category_price;
			//$output["event_dept_index_sales"] = $event_dept_index_sales;
			//return $output;
			
			for($n=0; $n<count($cat_data['current']); $n++) {
						$row0 = $cat_data['current'][$n];
						$current_total = $cat_data['current'][$n];
						$prior_week_total = $cat_data['prior_week'][$n];
						$prior_year_current_week_total = $cat_data['prior_year'][$n];
						
			// No Longer Needed - CF
			//$res0 = lavu_query("SELECT * FROM menu_categories WHERE menu_id='12' AND active='1' AND _deleted!='1'");
			//while ($row0 = mysqli_fetch_assoc($res0)){
				
						$name = $row0['name'];
						$menu_category_id = $row0['id'];/*is it a drink? a membership? what is it?*/
						$menu_id = $row0['menu_id'];/*i guess they can make different menu versions*/
						
						//echo "menu_category_id: $menu_category_id<br/>menu_id: $menu_id<br/>";
						
						/* Have to get the orders within the specified time frame, and the only dates available are in the orders table, which unfortuneately doesn't have the order items */
						
						// No Longer Needed - CF
						/*$current_total = calculateTotal(date("Y-m-d", mktime(0, 0, 0, $start_date_month*1, $start_date_day*1, $start_date_year*1)),date("Y-m-d", mktime(0, 0, 0, $end_date_month*1, $end_date_day*1, $end_date_year*1)),$menu_category_id,$event_dept_index);
						$prior_week_total = calculateTotal(date("Y-m-d", mktime(0, 0, 0, $start_date_month*1, $start_date_day*1-7, $start_date_year*1)),date("Y-m-d", mktime(0, 0, 0, $end_date_month*1, $end_date_day*1-7, $end_date_year*1)),$menu_category_id,$event_dept_index);
						$prior_year_current_week_total = calculateTotal(date("Y-m-d", mktime(0, 0, 0, $start_date_month*1, $start_date_day*1, $start_date_year*1-1)),date("Y-m-d", mktime(0, 0, 0, $end_date_month*1, $end_date_day*1, $end_date_year*1-1)),$menu_category_id,$event_dept_index);*/
						
						/* calculate the total revenue so we can determine the revenue per customer at the bottom of this section of the report */
						if (in_array($menu_category_id,$ids_to_include_in_revenue_calculation)){
								$cur_week_revenue_per_customer["total_revenue"] += $current_total["total_category_price"]*1;
								$cur_week_revenue_per_customer["num_customers"] += $current_total["num_customers"];
							
						}//if
						if (in_array($menu_category_id,$ids_to_include_in_revenue_calculation)){
								$prior_week_revenue_per_customer["total_revenue"] += $prior_week_total["total_category_price"]*1;
								$prior_week_revenue_per_customer["num_customers"] += $prior_week_total["num_customers"];
						}//if
						if (in_array($menu_category_id,$ids_to_include_in_revenue_calculation)){
								$prior_year_revenue_per_customer["total_revenue"] += $prior_year_current_week_total["total_category_price"]*1;
								$prior_year_revenue_per_customer["num_customers"] += $prior_year_current_week_total["num_customers"];
						}//if
						
						
						
						echo "
									<div class='row'>
											"."
											<div class='rowElement re0'>
												$name 
											</div>
											
											<div class='rowElement re1'>
												&#36;".number_format($current_total["total_category_price"],2)."
											</div>
											
											<div class='rowElement re2'>&#36;".
												number_format($prior_week_total["total_category_price"],2)
											."</div>
											
											<div class='rowElement re3'>&#36;".
												number_format($prior_year_current_week_total["total_category_price"],2)
											."</div>
											
											<div class='rowElement re4'>
												&nbsp;
											</div>
											
											
											
											<div class='rowElement re6'>";
												/* calculate trend */
												//echo "$current_total and $prior_week_total";
												$trend = trend($current_total["total_category_price"],$prior_week_total["total_category_price"]);
												echo number_format($trend,0)."&#37;";
											echo "</div>
											
											<div class='rowElement re7'>";
												$prior_year_trend = trend($current_total["total_category_price"],$prior_year_current_week_total["total_category_price"]);
												echo number_format($prior_year_trend,0)."&#37;";
											echo "</div>
											
											<div class='rowElement re8'>
												&nbsp;
											</div>
									</div><!--row-->
									<div class='clearRow'></div>
						";
			}//while

			
		
		 
		//$revenue_per_cust_info = lavu_query("SELECT * FROM "

			echo "
									<div class='row'>
											
											<div class='rowElement re0'>
												Revenue per Customer
											</div>
											
											<div class='rowElement re1'>&#36;".
											number_format(thisPerThat($cur_week_revenue_per_customer["total_revenue"],$cur_week_revenue_per_customer["num_customers"]),2)."
												"."
											</div>
											
											<div class='rowElement re2'>&#36;".
												number_format(thisPerThat($prior_week_revenue_per_customer["total_revenue"],$prior_week_revenue_per_customer["num_customers"]),2).
											"</div>
											
											<div class='rowElement re3'>&#36;".
											number_format(thisPerThat($prior_year_revenue_per_customer["total_revenue"],$prior_year_revenue_per_customer["num_customers"]),2)."
											</div>
											
											<div class='rowElement re4'>
												&nbsp;
											</div>
											
											
											
											<div class='rowElement re6'>";
												
											echo "</div>
											
											<div class='rowElement re7'>";
												
											echo "</div>
											
											<div class='rowElement re8'>
												&nbsp;
											</div>
									</div><!--row-->
									<div class='clearRow'></div>
						";
						
						
						
						echo "
									<div class='row'>
											
											<div class='rowElement re0' style='font-size:65%;'>
												Revenue per Labor Hour
											</div>
											
											<div class='rowElement re1'>&#36;".
											
											number_format(thisPerThat($cur_week_revenue_per_customer["total_revenue"],$current_total["sum_labor_hours"]),2)."
											
											</div>
											
											<div class='rowElement re2'>&#36;".
												number_format(thisPerThat($prior_week_revenue_per_customer["total_revenue"],$prior_week_total["sum_labor_hours"]),2).
											"</div>
											
											
											<div class='rowElement re3'>&#36;".
											number_format(thisPerThat($prior_year_revenue_per_customer["total_revenue"],$prior_year_current_week_total["sum_labor_hours"]),2)."
											</div>
											
											<div class='rowElement re4'>
												&nbsp;
											</div>
											
											
											
											<div class='rowElement re6'>";
												
											echo "</div>
											
											<div class='rowElement re7'>";
												
											echo "</div>
											
											<div class='rowElement re8'>
												&nbsp;
											</div>
									</div><!--row-->
									<div class='clearRow'></div>
						";
					
					/*	
						echo "
									<div class='row'>
											
											<div class='rowElement re0'>
												Total
											</div>
											
											<div class='rowElement re1'>&#36;".
											number_format($cur_week_revenue_per_customer["total_revenue"],2)."
												"."
											</div>
											
											<div class='rowElement re2'>&#36;".
												number_format($prior_week_revenue_per_customer["total_revenue"],2).
											"</div>
											
											<div class='rowElement re3'>&#36;".
											number_format($prior_year_revenue_per_customer["total_revenue"],2)."
											</div>
											
											<div class='rowElement re4'>
												&nbsp;
											</div>
											
											
											
											<div class='rowElement re6'>";
												
											echo "</div>
											
											<div class='rowElement re7'>";
												
											echo "</div>
											
											<div class='rowElement re8'>
												&nbsp;
											</div>
									</div><!--row-->
									<div class='clearRow'></div>
						";
				*/
						
						echo "
									<div class='row'>
											
											<div class='rowElement re0' style='font-size:65%;'>
												Membership Conversion &#37;
											</div>
											
											<div class='rowElement re1'>&nbsp;";
												
												echo "<div id='cur_week_membership_conversion_percent'></div>";
											echo "</div>
											
											<div class='rowElement re2'>&nbsp;";
												echo "<div id='prior_week_membership_conversion_percent'></div>";
											echo "</div>
											
											<div class='rowElement re3'>&nbsp;";
											echo "<div id='prior_year_membership_conversion_percent'></div>";
											echo "</div>
											
											<div class='rowElement re4'>
												&nbsp;
											</div>
											
											
											
											<div class='rowElement re6'>";
												
											echo "</div>
											
											<div class='rowElement re7'>";
												
											echo "</div>
											
											<div class='rowElement re8'>
												&nbsp;
											</div>
									</div><!--row-->
									<div class='clearRow'></div>
						";
				
						echo "
									<div class='row'>
											
											<div class='rowElement re0' style='font-size:65%;'>
												Race Credits Issued &#37;
											</div>
											
											<div class='rowElement re1'>";
												echo  "&#37;".number_format(getMemConvInfo(date("Y-m-d", mktime(0, 0, 0, $start_date_month*1, $start_date_day*1, $start_date_year*1)),date("Y-m-d", mktime(0, 0, 0, $end_date_month*1, $end_date_day*1, $end_date_year*1))),2);
											//echo $cur_week_race_cred_issued;
											echo "
											</div>
											
											<div class='rowElement re2'>&nbsp;";
												//echo  number_format(getMemConvInfo(date("Y-m-d", mktime(0, 0, 0, $start_date_month*1, $start_date_day*1-7, $start_date_year*1)),date("Y-m-d", mktime(0, 0, 0, $end_date_month*1, $end_date_day*1-7, $end_date_year*1))),2);

											echo "
											</div>
											
											<div class='rowElement re3'>&nbsp;";
											//echo  number_format(getMemConvInfo(date("Y-m-d", mktime(0, 0, 0, $start_date_month*1, $start_date_day*1, $start_date_year*1-1)),date("Y-m-d", mktime(0, 0, 0, $end_date_month*1, $end_date_day*1, $end_date_year*1-1))),2);
											echo "
											</div>
											
											<div class='rowElement re4'>
												&nbsp;
											</div>
											
											
											
											<div class='rowElement re6'>";
												
											echo "</div>
											
											<div class='rowElement re7'>";
												
											echo "</div>
											
											<div class='rowElement re8'>
												&nbsp;
											</div>
									</div><!--row-->
									<div class='clearRow'></div>
						";


?>
<?php
function getMemConvInfo($start_date,$end_date){
		$get_credits_issued = lavu_query("select SUM(details_short) from action_log WHERE action='Customer Credit Change' AND LEFT(time,10)>='$start_date' AND LEFT(time,10)<DATE_ADD('$end_date',INTERVAL 29 HOUR) ORDER BY time DESC");
		$get_credits_issued_row = mysqli_fetch_assoc($get_credits_issued);
		$credits_issued = $get_credits_issued_row['SUM(details_short)'];
		
		$get_total_races_ran = lavu_query("SELECT id FROM lk_events WHERE LEFT(date,10)>='$start_date' AND LEFT(date,10)<DATE_ADD('$end_date',INTERVAL 29 HOUR)");
		$get_how_many_races_str = "SELECT COUNT(id) FROM lk_event_schedules WHERE";
		$index = 1;
		while ($row = mysqli_fetch_assoc($get_total_races_ran)){
				$racer_id = $row['id'];
				$get_how_many_races_str .= " id='$racer_id'";
				if ($index*1 != mysqli_num_rows($get_total_races_ran)*1){
						$get_how_many_races_str .= " OR";
				}
				$index++;
		}//while
		$num_races = lavu_query($get_how_many_races_str);
		$num_races_row = mysqli_fetch_assoc($num_races);
		$num_races = $num_races_row['COUNT(id)'];
		
		$conversion_percent = 100*($credits_issued*1)/($num_races*1);
		
		return $conversion_percent;
}//getMemConvInfo()
?>


<br /><br /><br />

<div class="row">
	<h2>Event Department</h2>
</div>

<div class="row header">
		
		<div class="rowElement re0">
			Category
		</div>
		
		<div class="rowElement re1">
			Current Week
		</div>
		
		<div class="rowElement re2">
			Prior Week
		</div>
		
		<div class="rowElement re3">
			Prior Year
		</div>
		
		<div class="rowElement re4">
			Plan
		</div>

		<div class="rowElement re5">
			Trend
		</div>
		
		
		
		
		
</div><!--row-->
<div class="clearRow"></div>

<?php
	
?>
<div class="row">
		
		<div class="rowElement re0">
			Prepayments Taken
		</div>
		
		<div class="rowElement re1">
			<?php 
				$cur_week_prepayment = calculatePrepayments(date("Y-m-d", mktime(0, 0, 0, $start_date_month*1, $start_date_day*1, $start_date_year*1)),date("Y-m-d", mktime(0, 0, 0, $end_date_month*1, $end_date_day*1, $end_date_year*1)),$event_dept_index);
				echo number_format($cur_week_prepayment["prepay_amount"],2);
			?>
		</div>
		
		<div class="rowElement re2">
			<?php 
				$prior_week_prepayment = calculatePrepayments(date("Y-m-d", mktime(0, 0, 0, $start_date_month*1, $start_date_day*1-7, $start_date_year*1)),date("Y-m-d", mktime(0, 0, 0, $end_date_month*1, $end_date_day*1-7, $end_date_year*1)),$event_dept_index);
				echo number_format($prior_week_prepayment["prepay_amount"],2);
			?>
		</div>
		
		<div class="rowElement re3">
			<?php 
				$prior_year_prepayment = calculatePrepayments(date("Y-m-d", mktime(0, 0, 0, $start_date_month*1, $start_date_day*1, $start_date_year*1-1)),date("Y-m-d", mktime(0, 0, 0, $end_date_month*1, $end_date_day*1, $end_date_year*1-1)),$event_dept_index);
				echo number_format($prior_year_prepayment["prepay_amount"],2);
			?>
			
		</div>
		
		<div class="rowElement re4">
			&nbsp;
		</div>
		
		
		
		<div class="rowElement re5">
		<?php
			$trend = ($cur_week_prepayment["prepay_amount"]*1) - ($prior_week_prepayment["prepay_amount"]*1);
			echo "&#36;".number_format($trend,2);
		?>
		</div>
		
	
</div><!--row-->
<div class="clearRow"></div>
<div class="rowElement" style="width:300px;border-left:1px solid #333; background-color:#fff;">
			&nbsp;
		</div>
</div><!--row-->
<div class="clearRow"></div>


<div class="row">
		<div class="rowElement re0">
			<?php echo $get_menu_name_from_id[$event_dept_index];?>
		</div>
		<div class="rowElement re1">
			<?php echo number_format($current_total["event_dept_index_sales"],2);?>
		</div>
		<div class="rowElement re2">
			<?php echo number_format($prior_week_total["event_dept_index_sales"],2);?>
		</div>
		<div class="rowElement re3">
			<?php echo number_format($prior_year_current_week_total["event_dept_index_sales"],2);?>
		</div>
		<div class="rowElement re4">
			&nbsp;
		</div>
		<div class="rowElement re5">
		<?php
			$trend = ($current_total["event_dept_index_sales"]*1) - ($prior_week_total["event_dept_index_sales"]*1);
			echo "&#36;".number_format($trend,2);
		?>
		</div>
		
	
</div><!--row-->
<div class="clearRow"></div>






<br /><br /><br />

<?php
	$is_date_index_set = lavu_query("SHOW INDEX FROM lk_events WHERE KEY_NAME='date_index'");
	if (mysqli_num_rows($is_date_index_set)*1 < 1){
	 		$create_date_index = lavu_query("CREATE INDEX date_index ON lk_events (date)");
			echo "index date_index created on lk_events(date)";
	}//if
	
	$is_date_index_set = lavu_query("SHOW INDEX FROM lk_event_schedules WHERE KEY_NAME='eventid_index'");
	if (1==2 &&mysqli_num_rows($is_date_index_set)*1 < 1){
	 		$create_date_index = lavu_query("CREATE INDEX eventid_index ON lk_event_schedules (event_id)");
			echo "index eventid_index created on lk_event_schedules(event_id)";
	}//if
?>
<div class="row">
	<h2>Track Data</h2>
</div>

<div class="row header">
		
		<div class="rowElement re0">
			Category
		</div>
		
		<div class="rowElement re1">
			Current Week
		</div>
		
		<div class="rowElement re2">
			Prior Week
		</div>
		
		<div class="rowElement re3">
			Prior Year
		</div>
		
		<div class="rowElement re4">
			Plan
		</div>
		
		
		
		<div class="rowElement re6">
			Trend
		</div>
		
		<div class="rowElement re7">
			Prior Year <span style="font-size:75%;">&#43;&#47;&#45;</span>
		</div>
		
		<div class="rowElement re8">
			&#37; to Goal
		</div>
</div><!--row-->
<div class="clearRow"></div>
<div class="row">
		<div class="rowElement re0">
			Avg Races per Customer
		</div>
		<div class="rowElement re1">
		<?php
			$cur_week_avg_race_per_cust = avgRacePerCustomer(date("Y-m-d", mktime(0, 0, 0, $start_date_month*1, $start_date_day*1, $start_date_year*1)),date("Y-m-d", mktime(0, 0, 0, $end_date_month*1, $end_date_day*1, $end_date_year*1)));
			echo number_format($cur_week_avg_race_per_cust["avg_races_per_racer"],2);		
			?>
			
		</div>
		<div class="rowElement re2">
			<?php
			$prior_week_avg_race_per_cust = avgRacePerCustomer(date("Y-m-d", mktime(0, 0, 0, $start_date_month*1, $start_date_day*1-7, $start_date_year*1)),date("Y-m-d", mktime(0, 0, 0, $end_date_month*1, $end_date_day*1-7, $end_date_year*1)));
			echo number_format($prior_week_avg_race_per_cust["avg_races_per_racer"],2);		
			?>
		</div>
		<div class="rowElement re3">
			<?php
			$prior_year_avg_race_per_cust = avgRacePerCustomer(date("Y-m-d", mktime(0, 0, 0, $start_date_month*1, $start_date_day*1, $start_date_year*1-1)),date("Y-m-d", mktime(0, 0, 0, $end_date_month*1, $end_date_day*1, $end_date_year*1-1)));
			echo number_format($prior_year_avg_race_per_cust["avg_races_per_racer"],2);		
			?>
		</div>
		<div class="rowElement re4">
			Plan
		</div>
		<div class="rowElement re6">
			<?php
				$trend = trend($cur_week_avg_race_per_cust["avg_races_per_racer"],$prior_week_avg_race_per_cust["avg_races_per_racer"]);
				echo number_format($trend,2)."&#37;";
			?>
		</div>
		<div class="rowElement re7">
		<?php
			$trend = trend($cur_week_avg_race_per_cust["avg_races_per_racer"],$prior_year_avg_race_per_cust["avg_races_per_racer"]);
				echo number_format($trend,2)."&#37;";
		?>
		</div>	
		<div class="rowElement re8">
			&#37; to Goal
		</div>
</div><!--row-->
<div class="clearRow"></div>
<div class="row">
		<div class="rowElement re0">
			Races Sold
		</div>
		<div class="rowElement re1">
		<?php
			echo $cur_week_avg_race_per_cust["races_sold"];
			?>
			
		</div>
		<div class="rowElement re2">
			<?php
			echo $prior_week_avg_race_per_cust["races_sold"];
			?>
		</div>
		<div class="rowElement re3">
			<?php
			echo $prior_year_avg_race_per_cust["races_sold"];
			?>
		</div>
		<div class="rowElement re4">
			&nbsp;
		</div>
		<div class="rowElement re6">
			<?php
				echo number_format(trend($cur_week_avg_race_per_cust["races_sold"],$prior_week_avg_race_per_cust["races_sold"]),0)."&#37;";
			?>
		</div>
		<div class="rowElement re7">
		<?php
				echo number_format(trend($cur_week_avg_race_per_cust["races_sold"],$prior_year_avg_race_per_cust["races_sold"]),0)."&#37;";
			?>
		</div>	
		<div class="rowElement re8">
			
		</div>
</div><!--row-->
<div class="clearRow"></div>

<div class="row">
		
		<div class="rowElement re0">
			Customer Totals
		</div>
		
		<div class="rowElement re1">
			<?php
				echo $cur_week_avg_race_per_cust["total_customers"]; 
			?>
		</div>
		
		<div class="rowElement re2">
			<?php
				echo $prior_week_avg_race_per_cust["total_customers"]; 
			?>
		</div>
		
		<div class="rowElement re3">
			<?php
				echo $prior_year_avg_race_per_cust["total_customers"]; 
			?>
		</div>
		
		<div class="rowElement re4">
			Plan
		</div>
		
		
		
		<div class="rowElement re6">
			<?php
				$trend = trend($cur_week_avg_race_per_cust["total_customers"],$prior_week_avg_race_per_cust["total_customers"]);
				echo number_format($trend,2)."&#37;";
			?>
		</div>
		
		<div class="rowElement re7">
			<?php
				$trend = trend($cur_week_avg_race_per_cust["total_customers"],$prior_year_avg_race_per_cust["total_customers"]);
				echo number_format($trend,2)."&#37;";
			?>
		</div>
		
		<div class="rowElement re8">
			&nbsp;
		</div>
</div><!--row-->
<div class="clearRow"></div>

<div class="row">
		
		<div class="rowElement re0">
			Customer New
		</div>
		
		<div class="rowElement re1">
			<?php
				echo $cur_week_avg_race_per_cust["num_new_customers"]; 
			?>
		</div>
		
		<div class="rowElement re2">
			<?php
				echo $prior_week_avg_race_per_cust["num_new_customers"]; 
			?>
		</div>
		
		<div class="rowElement re3">
			<?php
				echo $prior_year_avg_race_per_cust["num_new_customers"]; 
			?>
		</div>
		
		<div class="rowElement re4">
			&nbsp;
		</div>
		
		
		
		<div class="rowElement re6">
			<?php
			$trend = trend($cur_week_avg_race_per_cust["num_new_customers"],$prior_week_avg_race_per_cust["num_new_customers"]);
			echo number_format($trend,2)."&#37;";
			?>
		</div>
		
		<div class="rowElement re7">
			<?php
			$trend = trend($cur_week_avg_race_per_cust["num_new_customers"],$prior_year_avg_race_per_cust["num_new_customers"]);
			echo number_format($trend,2)."&#37;";
			?>
		</div>
		
		<div class="rowElement re8">
			&nbsp;
		</div>
</div><!--row-->
<div class="clearRow"></div>
<div class="clearRow"></div>

<div class="row">
		
		<div class="rowElement re0">
			Customer New &#37;
		</div>
		
		<div class="rowElement re1">
			<?php
				/* Calculate percentage of new customers from total customers */
				$percentage = calcPercentage($cur_week_avg_race_per_cust["num_new_customers"],$cur_week_avg_race_per_cust["total_customers"]) ;
				echo number_format($percentage,2)."&#37;";
			?>
		</div>
		
		<div class="rowElement re2">
			<?php
				$percentage = calcPercentage($prior_week_avg_race_per_cust["num_new_customers"],$prior_week_avg_race_per_cust["total_customers"]) ;
				echo number_format($percentage,2)."&#37;";
			?>
		</div>
		
		<div class="rowElement re3">
			<?php
				$percentage = calcPercentage($prior_year_avg_race_per_cust["num_new_customers"],$prior_year_avg_race_per_cust["total_customers"]) ;
				echo number_format($percentage,2)."&#37;";
			?>
		</div>
		
		<div class="rowElement re4">
			&nbsp;
		</div>
		
		
		
		<div class="rowElement re6">
			&nbsp;
		</div>
		
		<div class="rowElement re7">
			&nbsp;
		</div>
		
		<div class="rowElement re8">
			&nbsp;
		</div>
</div><!--row-->
<div class="clearRow"></div>


<div class="row">
	<h2>Track Efficiency</h2>
</div>

<div class="row header">
		
		<div class="rowElement re0">
			&nbsp;
		</div>
		
		<div class="rowElement re1">
			Real Time
		</div>
		
		<div class="rowElement re2">
			Open Time
		</div>
		
		<div class="rowElement re3">
			Efficieny &#37;
		</div>
		
		<div class="rowElement re4">
			Races Sold
		</div>
		
		
		
		<div class="rowElement re6">
			Races
		</div>
		
		<div class="rowElement re7">
			<span style="font-size:70%;">Real Time per Race</span>
		</div>
		
		<div class="rowElement re8">
			Plan
		</div>
		
		<div class="rowElement re9">
			Track Hrs
		</div>
		
</div><!--row-->
<div class="clearRow"></div>

<?php
//echo "locationId: $locationid<br/>";
$get_lk_track_info = lavu_query("SELECT time_interval,time_start,time_end FROM lk_tracks WHERE locationid='$locationid'");
$lk_track_row = mysqli_fetch_assoc($get_lk_track_info);
$start_time = $lk_track_row['time_start'];
$start_time_hour = $start_time{0}.$start_time{1};
$start_time_minutes = $start_time{2}.$start_time{3};
$start_time_ts = mktime($start_time_hour*1,$start_time_minutes*1,0);

$end_time = $lk_track_row['time_end'];
$end_time_hour = $end_time{0}.$end_time{1};
$end_time_minutes = $end_time{2}.$end_time{3};
$end_time_ts = mktime($end_time_hour*1,$end_time_minutes*1,0);

$open_time_minutes = ($end_time_ts*1 - $start_time_ts*1)/60;//convert from seconds to minutes
//echo "open_time: $open_time_minutes<br/>";

$track_efficiency_sums = array();

$time_interval = $lk_track_row['time_interval'];//the time interval of a race, around 10 minutes
//echo "time_interval: $time_interval<br/>";
$keep_going = true;
$index = 0;
while ($keep_going){
			$curDate = date('Y-m-d', mktime(0, 0, 0, $start_date_month*1, $start_date_day*1+$index, $start_date_year*1));
			$track_data = getTrackEfficiencyData($curDate,$time_interval);
			echo "
			<div class='row'>
					<div class='rowElement re0'>";
							echo $track_data["weekday"];
					echo "
					</div>
					<div class='rowElement re1'>";
						$track_efficiency_sums["real_time"] += $track_data["real_time"]*1;
						echo number_format($track_data["real_time"],2);
					echo "
					</div>
					<div class='rowElement re2'>";
							$track_efficiency_sums["open_time_minutes"] += $open_time_minutes*1;
							echo $open_time_minutes;
					echo "
					</div>
					<div class='rowElement re3'>";
						$efficiency = number_format((100*($track_data["real_time"]*1)/$open_time_minutes),0)."&#37;";// real_time/open_time
						//$track_efficiency_sums["efficiency_percent_total"] += $efficiency*1; //don't need this
					echo "$efficiency
					</div>
					<div class='rowElement re4'>";
						echo $track_data["num_people_raced"];
						$track_efficiency_sums["num_people_raced"] += $track_data["num_people_raced"]*1;
					echo "</div>
					<div class='rowElement re6'>";
						$track_efficiency_sums["num_races"] += $track_data["num_races"]*1; 
						echo $track_data["num_races"];
					echo "
					</div>
					<div class='rowElement re7'>";
						//real time per race
						echo number_format((($track_data["real_time"]*1)/($track_data["num_races"]*1)),2);
					echo "</div>
					<div class='rowElement re8'>
						&nbsp;
					</div>
					<div class='rowElement re9'>";
						if ($index*1 == 0){
							/* This field depends on values calculated later in the script, so we will add this value with a javascript after the other values are calculated */
							echo "<div id='track_hrs'></div>";
						}//if
					echo "				
					</div>
			</div><!--row-->
			<div class='clearRow'></div>
			";
			if($curDate == date("Y-m-d", mktime(0, 0, 0, $start_date_month*1, $start_date_day*1+6, $start_date_year*1)) ){
					$keep_going = false;
			}//if
			$index++;
}//while
?>

<div class="row header">
		
		<div class="rowElement re0">
			Total
		</div>
		
		<div class="rowElement re1">
		<?php
			echo number_format($track_efficiency_sums["real_time"],2);
		?>
		</div>
		
		<div class="rowElement re2">
		<?php
			echo number_format($track_efficiency_sums["open_time_minutes"],2);
		?>
		</div>
		
		<div class="rowElement re3">
		<?php
			$weekly_efficiency = 100*($track_efficiency_sums["real_time"]*1)/($track_efficiency_sums["open_time_minutes"]*1); 
			echo number_format($weekly_efficiency,2)."&#37;";
		?>
		</div>
		
		<div class="rowElement re4">
		<?php
			echo number_format($track_efficiency_sums["num_people_raced"],2);
		?>	
		</div>
		
		
		
		<div class="rowElement re6">
		<?php
			echo number_format($track_efficiency_sums["num_races"],2);
		?>		
		</div>
		
		<div class="rowElement re7">
		<?php
			$weekly_real_time_per_race = ($track_efficiency_sums["real_time"]*1)/($track_efficiency_sums["num_races"]*1);
			echo number_format($weekly_real_time_per_race,2);
		?>		
		</div>
		
		<div class="rowElement re8">
			<span style='font-size:65%'>Trackhour per race</span>
		</div>
		
		<div class="rowElement re9">
			<div id='trackhour_per_race'></div><!--this field will be filled in by a javascript later, because it depends on values calculated later in the php code-->
		</div>
		
</div><!--row-->
<div class="clearRow"></div>



<?php
 
function getTrackEfficiencyData($date,$time_interval){
		/* $time_interval in minutes */
		/* Returns the name of day (monday,tuesday,etc...) from a date formatted as YYY-mm-dd H:i:s*/
		
		$output["weekday"] = getDayNameFromDate($date);
		
		$get_num_races_res = lavu_query("SELECT COUNT(id) FROM lk_events WHERE date='$date'");//this is the total number of races that day
		$num_races_row = mysqli_fetch_assoc($get_num_races_res);
		$output["num_races"] = $num_races_row['COUNT(id)'];//the number of races on the schedule for that day.
		
		$get_races_res = lavu_query("SELECT id FROM lk_events WHERE date='$date'");
		$real_time_sum = 0;//seconds
		while ($get_races_row = mysqli_fetch_assoc($get_races_res)){
					$event_id = $get_races_row['id'];
					$get_realtime_res = lavu_query("SELECT ms_start,ms_end FROM lk_events WHERE id='$event_id'");
					$get_realtime_row = mysqli_fetch_assoc($get_realtime_res);
					$ms_start = $get_realtime_row['ms_start'];
					$ms_end = $get_realtime_row['ms_end'];
					$real_time_sum += strtotime($ms_end)*1 - strtotime($ms_start)*1;
		}//while
		
		
		$output["real_time"] = ($real_time_sum*1)/60;//convert from seconds to minutes
		$how_many_racers = 0;// FIX THIS PART
		$get_num_races_res = lavu_query("SELECT id FROM lk_events WHERE date='$date'");
		while ($num_races_row = mysqli_fetch_assoc($get_num_races_res)){
				$event_id = $num_races_row['id'];
				$count_num_racers = lavu_query("SELECT COUNT(id) FROM lk_event_schedules WHERE eventid='$event_id'");//race credits sold
				if (mysqli_num_rows($count_num_racers)*1 > 0){
						$num_racers_row = mysqli_fetch_assoc($count_num_racers);
						$how_many_racers += $num_racers_row['COUNT(id)']*1;
				}//if
				
		}//while
		$output["num_people_raced"] = $how_many_racers;
		return $output;
}//getDayNameFromDate()

function calcPercentage($num,$total){
		$percentage = 100*($num*1)/($total*1);
		return $percentage;
}//calcPercentage()

function avgRacePerCustomer($start_date,$end_date){
		/* of the racers who raced that week, what is the average per customer */
		
		/* Get the events within the date range, then we use the event_id to find out how many customers were in that event under the lk_event_schedules table */
		$get_events_info_res = lavu_query("SELECT id FROM lk_events WHERE date>='$start_date' AND date<='$end_date'");
		//$num_customers = mysqli_num_rows($get_num_customers_res);
		
		//build sql query
		$sql_query = "SELECT customerid,COUNT(*) FROM lk_event_schedules WHERE eventid='";
		$index = 1;
		while ($get_events_info_row = mysqli_fetch_assoc($get_events_info_res)){
				//echo $get_events_info_row['id'];
				$sql_query .= $get_events_info_row['id']."' ";
				if ($index*1 != mysqli_num_rows($get_events_info_res)*1){
						$sql_query .= " OR eventid='";
				}//if
				
				$index++;
				
		}//while
		
		$sql_query .= "GROUP BY customerid";
		$get_average_info_res = lavu_query($sql_query);
		
		$total_racers_num = mysqli_num_rows($get_average_info_res);///how many racers are there
		$string = "";
		$count = 0;//count is how many races were raced by everyone.
		$num_new_customers = 0;//how many people created an account within the select date range
		
		$num_new_customers_sql = "SELECT COUNT(*) FROM lk_customers WHERE id='";//build this sql query with a while loop
		$num_members = 0;
		while($total_racers_row = mysqli_fetch_assoc($get_average_info_res)){
				$count += $total_racers_row['COUNT(*)']*1;
				$cust_id = $total_racers_row['customerid'];
				$num_new_customers_sql .= $cust_id."'";
				$is_new_customer = lavu_query("SELECT * FROM lk_customers WHERE date_created>='$start_date' AND date_created<='$end_date' AND id='$cust_id'");
				//$num_new_customers .= mysqli_num_rows($is_new_customer)."||";
				if (mysqli_num_rows($is_new_customer)*1 > 0){
						$num_new_customers++;
				}//if
				$is_new_customer_row = mysqli_fetch_assoc($is_new_customer);
				if ($is_new_customer_row['membership'] != ""){
						$num_members++;
				}//if
				
		}//foreach
		
		$avg_races_per_racer = ($count*1)/($total_racers_num*1);
		$output["num_members"] = $num_members;
		$output["num_new_customers"] = $num_new_customers;
		$output["avg_races_per_racer"] = $avg_races_per_racer;
		$output["races_sold"] = $count;
		$output["total_customers"] = $total_racers_num;
		return $output;
}//avgRacePerCustomer()
?>



<div class="row">
	<?php
		$get_role_title = lavu_query("SELECT title FROM roles WHERE id='$role_efficiency'");
		$get_role_title_row = mysqli_fetch_assoc($get_role_title);
		$title = $get_role_title_row['title'];	
	?>
	<h2><?php echo $title." Efficiency";?></h2>
</div>

<div class="row header">
		
		<div class="rowElement re0">
			&nbsp;
		</div>
		
		<div class="rowElement re1">
			Tickets
		</div>
		
		<div class="rowElement re2">
			CSR Hours
		</div>
		
		<div class="rowElement re3">
			Ticket per hr
		</div>
		
		<div class="rowElement re4">
			&nbsp;
		</div>

				
		
		
		<div class="rowElement re6">
			&nbsp;
		</div>
		
		<div class="rowElement re7">
			&nbsp;
		</div>
		
		<div class="rowElement re8">
			&nbsp;
		</div>
		
		<div class="rowElement re9">
			Actual
		</div>
</div><!--row-->
<div class="clearRow"></div>
<?php
//echo "start_date: $start_date; end_date: $end_date<br/>";

/* We find out who the cashiers are by looking at who is actually checking out orders */
		$get_cashier_ids = lavu_query("SELECT DISTINCT(cashier_id) FROM orders WHERE closed>='$start_date' AND closed<DATE_ADD('$end_date',INTERVAL 29 HOUR)");
		$build_query_string = " id='";
		$index = 1;
		while($get_cashier_ids_row = mysqli_fetch_assoc($get_cashier_ids)){
				$build_query_string .= $get_cashier_ids_row['cashier_id']."'";
				//echo $get_cashier_ids_row['cashier_id'];
				if ($index*1 != mysqli_num_rows($get_cashier_ids)*1){
					$build_query_string .= " OR id='";
				}//if
				$index++;
		}//while
		//echo "QUERY_STRING: $build_query_string<br/>";
$weekly_efficiency_sum = array();
$keep_going = true;
$index = 0;
while ($keep_going){
			$curDate = date('Y-m-d', mktime(0, 0, 0, $start_date_month*1, $start_date_day*1+$index, $start_date_year*1));
			
			$csr_info = getCSREfficiencyInfo($curDate,$build_query_string,$role_efficiency);
			echo "
			<div class='row'>
					<div class='rowElement re0'>";
							echo $csr_info["weekday"];
					echo "
					</div>
					<div class='rowElement re1'>";
						$weekly_efficiency_sum["num_tickets"] += $csr_info["num_tickets"]*1;
						echo number_format($csr_info["num_tickets"],0);
					echo "</div>
					<div class='rowElement re2'>";
						$weekly_efficiency_sum["csr_hours"] += $csr_info["csr_hours"]*1;
						echo number_format($csr_info["csr_hours"],2);
					echo "</div>
					<div class='rowElement re3'>
							";
							$tickets_per_csr_hour = ($csr_info["num_tickets"]*1)/($csr_info["csr_hours"]*1);
							echo number_format($tickets_per_csr_hour,2);
					echo "						
					</div>
					<div class='rowElement re4'>
						&nbsp;
					</div>
					<div class='rowElement re6'>
						&nbsp;
					</div>
					<div class='rowElement re7'>
						&nbsp;
					</div>
					<div class='rowElement re8'>
						&nbsp;
					</div>
					<div class='rowElement re9'>";
						if ($index*1 == 0){
							echo "<div id='actual_efficiency'></div>";/* this value depends on values calculated later, so we update this field with a javascript at the end of the file */
						}//if
						if ($index*1 != 0){
							echo "&nbsp;";
						}//if
					echo "</div>
					<div class='clearRow'></div>
			</div><!--row-->
			<div class='clearRow'></div>
			";
			if($curDate == date("Y-m-d", mktime(0, 0, 0, $start_date_month*1, $start_date_day*1+6, $start_date_year*1)) ){
					$keep_going = false;
			}//if
			$index++;
}//while
?>
<div class="row header">
		
		<div class="rowElement re0">
			&nbsp;
		</div>
		
		<div class="rowElement re1">
		<?php
			echo number_format($weekly_efficiency_sum["num_tickets"] ,0);
		?>
		</div>
		
		<div class="rowElement re2">
		<?php
			echo number_format($weekly_efficiency_sum["csr_hours"],2);
		?>	
		</div>
		
		<div class="rowElement re3">
		<?php
			$ticket_per_hr = ($weekly_efficiency_sum["num_tickets"]*1)/($weekly_efficiency_sum["csr_hours"]*1);
			echo number_format($ticket_per_hr,2);
		?>
		</div>
		
		<div class="rowElement re4">
			&nbsp;
		</div>
		
		
		
		<div class="rowElement re6">
			&nbsp;
		</div>
		
		<div class="rowElement re7">
			Plan
		</div>
		
		<div class="rowElement re8">
			<span style='font-size:65%;'>CSRHour per race</span>
		</div>
		<div class="rowElement re9">
		<?php
			$csr_hr_per_race = ($weekly_efficiency_sum["csr_hours"]*1)/($track_efficiency_sums["num_races"]*1);
			echo number_format($csr_hr_per_race,2);
		?>
		</div>

</div><!--row-->
<div class="clearRow"></div>



<?php
function getCSREfficiencyInfo($curDate,$cashier_id_string,$role_id){
		/* $cashier_id_string looks like " AND id='2' OR id='4' OR...", it helps us select which clockpunches are from cashiers and not mechanics, sales reps, etc */
		$output["weekday"] = getDayNameFromDate($curDate);
		$get_ticket_info = lavu_query("SELECT COUNT(id) FROM orders WHERE LEFT(closed,10)='$curDate'");
		$ticket_info_row = mysqli_fetch_assoc($get_ticket_info);
		$output["num_tickets"] = $ticket_info_row['COUNT(id)'];
		
		$time_index_exists = lavu_query("SHOW INDEX FROM clock_punches WHERE KEY_NAME = 'time_index'");
		if (mysqli_num_rows($time_index_exists)*1 < 1){
				$create_index = lavu_query("CREATE INDEX time_index ON clock_punches (time)");
				echo "index time_index created on clock punches<br/>";
		}//if
		$time_out_index_exists = lavu_query("SHOW INDEX FROM clock_punches WHERE KEY_NAME = 'time_out_index'");
		if (mysqli_num_rows($time_out_index_exists)*1 < 1){
				$create_index = lavu_query("CREATE INDEX time_out_index ON clock_punches (time_out)");
				echo "index time_out_index created on clock punches<br/>";
		}//if
		
		
		/*For $get_csr_hours, I'm taking all the clock punches for that day and the next 5 hours, so that way if someone works through midnight their hours are still counted for the day their shift originated on */
		/*
		$cashier_clock_punch_str = "SELECT SUM(hours) FROM clock_punches WHERE time>='$curDate' AND time_out<(SELECT DATE_ADD('$curDate',INTERVAL 29 HOUR)) AND $cashier_id_string ORDER BY time_out";
		//echo $cashier_clock_punch_str;
		$get_csr_hours = lavu_query($cashier_clock_punch_str);
		$csr_hours_row = mysqli_fetch_assoc($get_csr_hours);
		*/
		$output["csr_hours"] = getLaborUsageInfo($curDate,$role_id);//$csr_hours_row['SUM(hours)'];
		
		
		return $output;
}//getCSREfficiencyInfo()

function getDayNameFromDate($date){
		/* Take in date in YYYY-mm-dd format and return the day name of the week (saturday, sunday,etc...)*/
		$date_info = getdate(strtotime($date));
		$dayName = $date_info["weekday"];
		return $dayName;
}//getDayNameFromDate()
?>

<div class="row">
	<h2>Labor Usage</h2>
</div>

<?php
$total_weekly_hrs_of_employee_type = array();//holds the total hours for each employee type using $total_weekly_hrs_of_employee_type["category_id"] = $total_hours
echo "<div class='row header'>";
echo "
<div class='rowElement re0'>
						&nbsp;
</div>
					";
$index = 0;
$keep_going = true;
$col_num = 1;
while ($keep_going){
				$curDate = date('Y-m-d', mktime(0, 0, 0, $start_date_month*1, $start_date_day*1+$index, $start_date_year*1));
				echo "
						
					<div class='rowElement re".$col_num."'>&nbsp;
						".getDayNameFromDate($curDate)."
					</div>
					
				";
				if($curDate == date("Y-m-d", mktime(0, 0, 0, $end_date_month*1, $end_date_day*1, $end_date_year*1)) ){
						$keep_going = false;
				}//if
				$col_num++;
				$index++;
}//while
echo "
			<div class='rowElement re9'>
					Total
			</div>
";
					
echo "</div><!--row-->";
echo "<div class='clearRow'></div>";

$sum_of_weekly_total_hours = 0;
$get_job_res = lavu_query("SELECT * FROM roles");
while ($get_job_row = mysqli_fetch_assoc($get_job_res)){
			echo "
						<div class='row'>
												
												<div class='rowElement re0'>
													".$get_job_row['title']."
												</div>";
										$weekly_total_hours = 0;
										$col_num = 1;
										for ($index=0;$index<7;$index++){		
												echo "<div class='rowElement re".$col_num."'>";
												$hours = getLaborUsageInfo(date('Y-m-d', mktime(0, 0, 0, $start_date_month*1, $start_date_day*1+$index, $start_date_year*1)),$get_job_row['id']);
												$weekly_total_hours += $hours*1;
												echo"
														".number_format($hours,2)."
														</div>";
												$col_num++;
										}//for
										echo "<div class='rowElement re9'>";
													$total_weekly_hrs_of_employee_type[$get_job_row['id']] = $weekly_total_hours;
													$sum_of_weekly_total_hours += $weekly_total_hours*1;
													echo number_format($weekly_total_hours,2);
													echo "</div>
										";		
			echo "								
					</div><!--row-->
					<div class='clearRow'></div>
			";
}//while
echo "
			<div class='row'>
					<div class='rowElement re0'>&nbsp;</div>
					<div class='rowElement re1'>&nbsp;</div>
					<div class='rowElement re2'>&nbsp;</div>
					<div class='rowElement re3'>&nbsp;</div>
					<div class='rowElement re4'>&nbsp;</div>
					<div class='rowElement re5'>&nbsp;</div>
					<div class='rowElement re6'>&nbsp;</div>
					<div class='rowElement re7 header'>Total</div>
					<div class='rowElement re9 header' style='border-top:2px solid #333;'>".number_format($sum_of_weekly_total_hours,2)."</div>
															
															
													
			</div><!--row-->
			<div class='clearRow'></div>
";

?>
<?php
function getLaborUsageInfo($date,$job_id){
		$get_hours_info_res = lavu_query("SELECT id FROM users WHERE role_id='$job_id'");
		$total_hours = 0;
		while ($get_hours_info_row = mysqli_fetch_assoc($get_hours_info_res)){
				$server_id = $get_hours_info_row['id'];
				$get_cur_total_hours_res = lavu_query("SELECT SUM(hours) FROM clock_punches WHERE server_id='$server_id' AND LEFT(time_out,10)>='$date' AND LEFT(time_out,10)<DATE_ADD('$date',INTERVAL 29 HOUR)");
				if (mysqli_num_rows($get_cur_total_hours_res)*1 > 0){
							$get_cur_total_hours_row = mysqli_fetch_assoc($get_cur_total_hours_res);
				 			$total_hours += $get_cur_total_hours_row['SUM(hours)']*1;
				}//if
		}//while
		return $total_hours;
}//getLaborUsageInfo()
?>
</body>
</html>

<script type="text/javascript">
<?php
$actual_efficiency = $weekly_efficiency_sum["csr_hours"];
echo "
		document.getElementById('start_day').value = '$start_date_day';
		document.getElementById('start_month').value = '$start_date_month';
		document.getElementById('start_year').value = '$start_date_year';
		//document.getElementById('end_day').value = '$end_date_day';
		//document.getElementById('end_month').value = '$end_date_month';
		//document.getElementById('end_year').value = '$end_date_year';
		document.getElementById('role_efficiency').value = '$role_efficiency';
		document.getElementById('track_efficiency').value = '$track_efficiency';	
		document.getElementById('actual_efficiency').innerHTML = '$actual_efficiency';	
";
	$track_efficiency_id = $total_weekly_hrs_of_employee_type[$track_efficiency];
	$weekly_total_races = $track_efficiency_sums["num_races"];
	
	$cur_week_num_cust = $cur_week_avg_race_per_cust["num_new_customers"];
	$prior_week_num_cust = $prior_week_avg_race_per_cust["num_new_customers"];
	$prior_year_num_cust = $prior_year_avg_race_per_cust["num_new_customers"];

	$cur_week_num_member = $cur_week_avg_race_per_cust["num_members"];
	$prior_week_num_member = $prior_week_avg_race_per_cust["num_members"];
	$prior_year_num_member = $prior_year_avg_race_per_cust["num_members"];
	
echo "
		calcTrackHrs('$track_efficiency_id','$weekly_total_races');
		calcMemConvPercent('$cur_week_num_cust ', '$cur_week_num_member','cur_week_membership_conversion_percent');
		calcMemConvPercent('$prior_week_num_cust ', '$prior_week_num_member','prior_week_membership_conversion_percent');
		calcMemConvPercent('$prior_year_num_cust ', '$prior_year_num_member','prior_year_membership_conversion_percent');
";

?>

function calcMemConvPercent(num_new_customers, num_members,field_id){
		/* Calculate member ship conversion percent. Had to update with this javascript because the values needed to calculate it aren't processed until the end of the php code, so we have to update the innerHTML with JS.*/
		mem_percent = (num_members*1)/(num_new_customers*1);
		document.getElementById(field_id).innerHTML = mem_percent.toFixed(2);
		return;
}//calcMemConvPercent()

function calcTrackHrs(total_weekly_hours, total_weekly_races){
	/* This function calculates values obtained from the php code, and writes the calculated value into the <div id='track_hrs'></div> under Track Efficiency. This was done because the value in  <div id='track_hrs'></div> depends on values that aren't 	 calculated until later in the php code.*/
	//alert(total_weekly_hours);
	document.getElementById("track_hrs").innerHTML = total_weekly_hours;
	
	
	track_hrs_per_race = (total_weekly_hours*1)/(total_weekly_races*1);
	document.getElementById("trackhour_per_race").innerHTML = track_hrs_per_race.toFixed(2);
	return;
}//calcTrackHrs()

</script>

<?php
function pad($str,$length){
		while (strlen($str)*1 < $length*1){
			$str = "0".$str;
		}
		return $str;
}//pad
?>

