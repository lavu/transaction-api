#!/usr/bin/php -q
<?php
/**
 * Created by PhpStorm.
 * User: hayat
 * Date: 11/7/17
 * Time: 3:43 PM
 */

    require_once (dirname(__FILE__)."/abi_report_config.php");
    require_once(dirname(dirname(dirname(__FILE__))) ."/resources/core_functions.php");
    require_once(dirname(dirname(dirname(__FILE__))) ."/resources/chain_functions.php");
    require_once dirname(__FILE__) . '/../../areas/currency_util.php';
    require_once (dirname(__FILE__)."/abi_report_functions.php");

    // /home/poslavu/public_html/admin/cp/areas/reports/flash_email_report.php mode chain_id
    $mode = ($argv[1] != '') ? $argv[1] : $_REQUEST['mode'];
    $mode = strtolower($mode);

    // Check mode
    if (!in_array($mode, array('daily', 'weekly'))) {
        echo "Invalid Mode";
        return false;
    }

    $chain_id = ($argv[2] != '') ? $argv[2] : $_REQUEST['chain_id'];
    $chain_id = ($chain_id != '') ? $chain_id : $defaultChainId;
    $data_name = '';
    if ($chain_id != '') {
        $PrimaryRestaurantID = getPrimaryChainRestaurant($chain_id);
        $companyInfo = getCompanyInfo($PrimaryRestaurantID, 'id');
        $data_name = $companyInfo['data_name'];
    }
    $data_name = ($data_name != '') ? $data_name : $defaultDataname;
    //We are allowing to get Chain location for access level 4 user in chain_function.php
    // This script will run as cron job, So setting hardcoded access leve 4 in session
    session_start();
    set_sessvar('admin_access_level', 4);

    list($reportRegions, $staticPlanRecord, $staticBeerVolume) = getPlannedData($chain_id);
    $emailSchedule = ucfirst($mode);
    $imageDir = checkDir();
    // Get email id and report type from manage email list
    $manageReportDetails = reportSendToEmailId($emailSchedule, $chain_id);

    if(empty($manageReportDetails)) {
        error_log('There is no any email detail configured. Please configure it from cp setting click "Manage Email Reports" link');
    }

    list($getAllLocation, $reportLocations) = getRegionWiseChainLocations($data_name, $chain_id, $reportRegions);

    if (empty($reportLocations)) {
        error_log("Chain Location not found for Data name " . $data_name. "Chain id : " . $chain_id . " Regions : ". implode(',', $reportRegions));
    }
    $dataLocationNameMapping = array();
    foreach ($getAllLocation as $region => $regionLocation) {
        foreach ($regionLocation as $locationDetails) {
            $dataLocationNameMapping[$locationDetails['data_name']]['name'] = $locationDetails['company_name'];
            $dataLocationNameMapping[$locationDetails['data_name']]['company_id'] = $locationDetails['company_id'];
        }
    }

    if ($mode == 'daily') {
        $date = strtotime("-1 day", time());
        $startDate = date('Y-m-d H:i:s', mktime(0, 0, 0, date('m', $date), date('d', $date), date('Y', $date)));
        $endDate = date('Y-m-d H:i:s', mktime(23, 59, 59, date('m', $date), date('d', $date), date('Y', $date)));
        $reportFlag = true;
        foreach ($manageReportDetails as $region => $manageEmailDetails) {
            foreach ($manageEmailDetails as $key => $manageDetails) {
                if ($reportFlag) {
                    // Daily sales vs target report
                    list($dailySalesReport, $regionConversionRate) = getSalesVsTargetReport($data_name, $startDate, $endDate);
                    //list($dailyBeerSalesReport, $regionConversionRate) = getBeerSales($data_name, $startDate, $endDate);
                    //list($dailyFoodSalesReport, $regionConversionRate) = getFoodSales($data_name, $startDate, $endDate);
                    $reportFlag = false;
                }
                emailDailySalesReport($startDate, $endDate, $dailySalesReport, $regionConversionRate, $region, $manageDetails, $key);
            /*
                // Daily beer sales report
                emailDailyBeerSalesReport($startDate, $endDate, $dailyBeerSalesReport, $regionConversionRate, $region, $manageDetails, $key);

                // Daily food sales report
                emailDailyFoodSalesReport($startDate, $endDate, $dailyFoodSalesReport, $regionConversionRate, $region, $manageDetails, $key);
            */
            }
        }
    } else if ($mode == 'weekly') {
        $ts = time();
        //Start date is week day from Monday end week day Sunday on given end date
        $startDate = date('Y-m-d', strtotime('last week monday')) . ' 00:00:00';
        $endDate = date('Y-m-d', strtotime('last week sunday')) . ' 23:59:59';
        $reportFlag = true;
        foreach ($manageReportDetails as $region => $manageEmailDetails) {
            foreach ($manageEmailDetails as $manageDetails) {
                if ($manageDetails['report_type'] == 'Sales') {
                    if ($reportFlag) {
                        // Weekly sales report
                        list($weeklySalesReport, $regionConversionRate) = getSalesReport($data_name, $startDate, $endDate);
                        $reportFlag = false;
                    }
                    emailWeeklySalesReport($startDate, $endDate, $weeklySalesReport, $regionConversionRate, $region, $manageDetails);
                } else if ($manageDetails['report_type'] == 'Trends') {
                    list($netRevenueTrackerReport, $regionConversionRate) = getNetRevenueTrackerReport($data_name, $startDate, $endDate, $region);
                    emailNetRevenueTrackerReport($startDate, $endDate, $netRevenueTrackerReport, $regionConversionRate, $region, $manageDetails);
                }
                /*
                // Weekly beer sales report
                list($weeklyBeerSalesReport, $regionConversionRate) = getBeerSales($data_name, $startDate, $endDate);
                emailWeeklyBeerSalesReport ($startDate, $endDate, $weeklyBeerSalesReport, $regionConversionRate, $manageReportDetails);

                // Weekly food sales report
                list($weeklyFoodSalesReport, $regionConversionRate) = getFoodSales($data_name, $startDate, $endDate);
                emailWeeklyFoodSalesReport ($startDate, $endDate, $weeklyFoodSalesReport, $regionConversionRate, $manageReportDetails);
                */
            }
        }
    }

    function getSalesReport($data_name, $startDate, $endDate) {
        global $reportRegions;
        $salesReport = array();
        foreach ($reportRegions as $region) {
            $regionLocations = getLocationForReport($data_name, '', $region, false);
            $conversionFlag = true;
            if (!empty($regionLocations)) {
                foreach ($regionLocations as $dataName) {
                    $rdb = "poslavu_" . $dataName . "_db";
                    // Get conversion rate only one time for reducing query call for each dataname
                    if ($conversionFlag) {
                        $chainCountry = getReportCountry($rdb);
                        list($conversionRateFrom, $conversionRateTo) = getConversionCurrency($chainCountry, $endDate);
                        $conversionFlag = false;
                        $regionConversionRate[$region]['from'] = $conversionRateFrom;
                        $regionConversionRate[$region]['to'] = $conversionRateTo;
                    }
                    $sale_query = lavu_query("select DATE_FORMAT(`orders`.`closed`, '%w') as day, if(count(*) > 0, sum((order_contents.price + order_contents.modify_price + order_contents.forced_modifiers_price)*order_contents.quantity - if( order_contents.quantity > '0',if(order_contents.discount_amount<(order_contents.price + order_contents.modify_price + order_contents.forced_modifiers_price),order_contents.discount_amount,order_contents.price + order_contents.modify_price + order_contents.forced_modifiers_price)+ order_contents.idiscount_amount + order_contents.itax, 0)), '0.00') as net from `[3]`.`orders` LEFT JOIN `[3]`.`order_contents` ON `orders`.`order_id` = `order_contents`.`order_id` AND `orders`.`location_id` = `order_contents`.`loc_id`  LEFT JOIN `[3]`.`menu_items` ON `order_contents`.`item_id` = `menu_items`.`id` where orders.closed >= '[1]' and orders.closed < '[2]' and orders.void = '0' and orders.location_id='1' and  order_contents.item != 'SENDPOINT' GROUP BY day", $startDate, $endDate, $rdb);
                    if (mysqli_num_rows($sale_query) > 0) {
                        while ($sales_read = mysqli_fetch_assoc($sale_query)) {
                            $salesReport[$region][$dataName][$sales_read['day']] += $sales_read['net'];
                        }
                    } else {
                        $salesReport[$region][$dataName][0] = 0;
                    }
                }
            }
        }

        return array($salesReport, $regionConversionRate);
    }

    function getSalesVsTargetReport($data_name, $startDate, $endDate) {
        global $reportRegions;
        $salesReport = array();
        foreach ($reportRegions as $region) {
            $regionLocations = getLocationForReport($data_name, '', $region, false);
            $conversionFlag = true;
            if (!empty($regionLocations)) {
                foreach ($regionLocations as $dataName) {
                    $rdb = "poslavu_" . $dataName . "_db";
                    // Get conversion rate only one time for reducing query call for each dataname
                    if ($conversionFlag) {
                        $chainCountry = getReportCountry($rdb);
                        list($conversionRateFrom, $conversionRateTo) = getConversionCurrency($chainCountry, $endDate);
                        $conversionFlag = false;
                        $regionConversionRate[$region]['from'] = $conversionRateFrom;
                        $regionConversionRate[$region]['to'] = $conversionRateTo;
                    }
                    $sale_query = lavu_query("select if(count(*) > 0, sum((order_contents.price + order_contents.modify_price + order_contents.forced_modifiers_price)*order_contents.quantity - if( order_contents.quantity > '0',if(order_contents.discount_amount<(order_contents.price + order_contents.modify_price + order_contents.forced_modifiers_price),order_contents.discount_amount,order_contents.price + order_contents.modify_price + order_contents.forced_modifiers_price)+ order_contents.idiscount_amount + order_contents.itax, 0)), '0.00') as net from `[3]`.`orders` LEFT JOIN `[3]`.`order_contents` ON `orders`.`order_id` = `order_contents`.`order_id` AND `orders`.`location_id` = `order_contents`.`loc_id`  LEFT JOIN `[3]`.`menu_items` ON `order_contents`.`item_id` = `menu_items`.`id` where orders.closed >= '[1]' and orders.closed < '[2]' and orders.void = '0' and orders.location_id='1' and  order_contents.item != 'SENDPOINT'", $startDate, $endDate, $rdb);
                    while ($sales_read = mysqli_fetch_assoc($sale_query)) {
                        $salesReport[$region][$dataName] += $sales_read['net'];
                    }
                }
            }
        }

        return array($salesReport, $regionConversionRate);
    }

    function getBeerSales($data_name, $startDate, $endDate) {
        global $reportRegions;
        $salesReport = array();
        foreach ($reportRegions as $region) {
            $regionLocations = getLocationForReport($data_name, '', $region, false);
            $conversionFlag = true;
            if (!empty($regionLocations)) {
                foreach ($regionLocations as $dataName) {
                    $rdb = "poslavu_" . $dataName . "_db";
                    // Get conversion rate only one time for reducing query call for each dataname
                    if ($conversionFlag) {
                        $chainCountry = getReportCountry($rdb);
                        list($conversionRateFrom, $conversionRateTo) = getConversionCurrency($chainCountry, $endDate);
                        $conversionFlag = false;
                        $regionConversionRate[$region]['from'] = $conversionRateFrom;
                        $regionConversionRate[$region]['to'] = $conversionRateTo;
                    }
                    $sale_query = lavu_query("select if(count(*) > 0, sum((order_contents.price + order_contents.modify_price + order_contents.forced_modifiers_price)*order_contents.quantity - if( order_contents.quantity > '0',if(order_contents.discount_amount<(order_contents.price + order_contents.modify_price + order_contents.forced_modifiers_price),order_contents.discount_amount,order_contents.price + order_contents.modify_price + order_contents.forced_modifiers_price)+ order_contents.idiscount_amount + order_contents.itax, 0)), '0.00') as net from `[3]`.`orders` LEFT JOIN `[3]`.`order_contents` ON `orders`.`order_id` = `order_contents`.`order_id` AND `orders`.`location_id` = `order_contents`.`loc_id`  LEFT JOIN `[3]`.`menu_items` ON `order_contents`.`item_id` = `menu_items`.`id` LEFT JOIN `[3]`.`menu_categories` ON `menu_items`.`category_id` = `menu_categories`.`id` LEFT JOIN `[3]`.`super_groups` ON IF (menu_items.super_group_id = '', menu_categories.super_group_id, menu_items.super_group_id) = `super_groups`.`id` where super_groups.title like '%beer%' and orders.closed >= '[1]' and orders.closed < '[2]' and orders.void = '0' and orders.location_id='1' and  order_contents.item != 'SENDPOINT'", $startDate, $endDate, $rdb);
                    while ($sales_read = mysqli_fetch_assoc($sale_query)) {
                        $salesReport[$region][$dataName] += $sales_read['net'];
                    }
                }
            }
        }

        return array($salesReport, $regionConversionRate);
    }

    function getFoodSales($data_name, $startDate, $endDate) {
        global $reportRegions;
        $salesReport = array();
        foreach ($reportRegions as $region) {
            $regionLocations = getLocationForReport($data_name, '', $region, false);
            $conversionFlag = true;
            if (!empty($regionLocations)) {
                foreach ($regionLocations as $dataName) {
                    $rdb = "poslavu_" . $dataName . "_db";
                    // Get conversion rate only one time for reducing query call for each dataname
                    if ($conversionFlag) {
                        $chainCountry = getReportCountry($rdb);
                        list($conversionRateFrom, $conversionRateTo) = getConversionCurrency($chainCountry, $endDate);
                        $conversionFlag = false;
                        $regionConversionRate[$region]['from'] = $conversionRateFrom;
                        $regionConversionRate[$region]['to'] = $conversionRateTo;
                    }
                    $sale_query = lavu_query("select if(count(*) > 0, sum((order_contents.price + order_contents.modify_price + order_contents.forced_modifiers_price)*order_contents.quantity - if( order_contents.quantity > '0',if(order_contents.discount_amount<(order_contents.price + order_contents.modify_price + order_contents.forced_modifiers_price),order_contents.discount_amount,order_contents.price + order_contents.modify_price + order_contents.forced_modifiers_price)+ order_contents.idiscount_amount + order_contents.itax, 0)), '0.00') as net from `[3]`.`orders` LEFT JOIN `[3]`.`order_contents` ON `orders`.`order_id` = `order_contents`.`order_id` AND `orders`.`location_id` = `order_contents`.`loc_id`  LEFT JOIN `[3]`.`menu_items` ON `order_contents`.`item_id` = `menu_items`.`id` LEFT JOIN `[3]`.`menu_categories` ON `menu_items`.`category_id` = `menu_categories`.`id` LEFT JOIN `[3]`.`super_groups` ON IF (menu_items.super_group_id = '', menu_categories.super_group_id, menu_items.super_group_id) = `super_groups`.`id` where super_groups.title like '%food%' and orders.closed >= '[1]' and orders.closed < '[2]' and orders.void = '0' and orders.location_id='1' and  order_contents.item != 'SENDPOINT'", $startDate, $endDate, $rdb);
                    while ($sales_read = mysqli_fetch_assoc($sale_query)) {
                        $salesReport[$region][$dataName] += $sales_read['net'];
                    }
                }
            }
        }

        return array($salesReport, $regionConversionRate);
    }


    function getNetRevenueTrackerReport($data_name, $startDate, $endDate, $region) {
        global $reportRegions;
        $weeklySalesReport = array();
        $monthlyActualSalesReport = array();
        $yearlyActualSalesReport = array();
        $salesReport = array();
        if ($region != 'All') {
            $reportRegions = array();
            $reportRegions[0] = $region;
        }

        foreach ($reportRegions as $region) {
            $regionLocations = getLocationForReport($data_name, '', $region, false);
            $conversionFlag = true;
            if (!empty($regionLocations)) {
                foreach ($regionLocations as $dataName) {
                    $rdb = "poslavu_" . $dataName . "_db";
                    // Get conversion rate only one time for reducing query call for each dataname
                    if ($conversionFlag) {
                        $chainCountry = getReportCountry($rdb);
                        list($conversionRateFrom, $conversionRateTo) = getConversionCurrency($chainCountry, $endDate);
                        $conversionFlag = false;
                        $regionConversionRate[$region]['from'] = $conversionRateFrom;
                        $regionConversionRate[$region]['to'] = $conversionRateTo;
                    }
                    $weekly_sales_query = lavu_query("select if(count(*) > 0, sum((order_contents.price + order_contents.modify_price + order_contents.forced_modifiers_price)*order_contents.quantity - if( order_contents.quantity > '0',if(order_contents.discount_amount<(order_contents.price + order_contents.modify_price + order_contents.forced_modifiers_price),order_contents.discount_amount,order_contents.price + order_contents.modify_price + order_contents.forced_modifiers_price)+ order_contents.idiscount_amount + order_contents.itax, 0)), '0.00') as net from `[3]`.`orders` LEFT JOIN `[3]`.`order_contents` ON `orders`.`order_id` = `order_contents`.`order_id` AND `orders`.`location_id` = `order_contents`.`loc_id`  LEFT JOIN `[3]`.`menu_items` ON `order_contents`.`item_id` = `menu_items`.`id` where orders.closed >= '[1]' and orders.closed < '[2]' and orders.void = '0' and orders.location_id='1' and  order_contents.item != 'SENDPOINT'", $startDate, $endDate, $rdb);
                    if (mysqli_num_rows($weekly_sales_query) > 0) {
                        while ($weekly_sales_read = mysqli_fetch_assoc($weekly_sales_query)) {
                            $weeklySalesReport[$region] += $weekly_sales_read['net'];
                        }
                    } else {
                        $weeklySalesReport[$region][$dataName][0] = 0;
                    }

                    $startTimeStamp = strtotime($startDate);
                    $monthStartDate = date('Y-m-d H:i:s', mktime(0, 0, 0, date('m', $startTimeStamp), 1, date('Y', $startTimeStamp)));
                    $yearStartDate = date('Y-m-d H:i:s', mktime(0, 0, 0, 1, 1, date('Y', $startTimeStamp)));
                    $monthly_sales_query = lavu_query("select if(count(*) > 0, sum((order_contents.price + order_contents.modify_price + order_contents.forced_modifiers_price)*order_contents.quantity - if( order_contents.quantity > '0',if(order_contents.discount_amount<(order_contents.price + order_contents.modify_price + order_contents.forced_modifiers_price),order_contents.discount_amount,order_contents.price + order_contents.modify_price + order_contents.forced_modifiers_price)+ order_contents.idiscount_amount + order_contents.itax, 0)), '0.00') as net from `[3]`.`orders` LEFT JOIN `[3]`.`order_contents` ON `orders`.`order_id` = `order_contents`.`order_id` AND `orders`.`location_id` = `order_contents`.`loc_id`  LEFT JOIN `[3]`.`menu_items` ON `order_contents`.`item_id` = `menu_items`.`id` where orders.closed >= '[1]' and orders.closed < '[2]' and orders.void = '0' and orders.location_id='1' and  order_contents.item != 'SENDPOINT'", $monthStartDate, $endDate, $rdb);
                    if (mysqli_num_rows($monthly_sales_query) > 0) {
                        while ($monthly_sales_read = mysqli_fetch_assoc($monthly_sales_query)) {
                            $monthlyActualSalesReport[$region] += $monthly_sales_read['net'];
                        }
                    } else {
                        $monthlyActualSalesReport[$region] = 0;
                    }

                    $yearly_sales_query = lavu_query("select if(count(*) > 0, sum((order_contents.price + order_contents.modify_price + order_contents.forced_modifiers_price)*order_contents.quantity - if( order_contents.quantity > '0',if(order_contents.discount_amount<(order_contents.price + order_contents.modify_price + order_contents.forced_modifiers_price),order_contents.discount_amount,order_contents.price + order_contents.modify_price + order_contents.forced_modifiers_price)+ order_contents.idiscount_amount + order_contents.itax, 0)), '0.00') as net from `[3]`.`orders` LEFT JOIN `[3]`.`order_contents` ON `orders`.`order_id` = `order_contents`.`order_id` AND `orders`.`location_id` = `order_contents`.`loc_id`  LEFT JOIN `[3]`.`menu_items` ON `order_contents`.`item_id` = `menu_items`.`id` where orders.closed >= '[1]' and orders.closed < '[2]' and orders.void = '0' and orders.location_id='1' and  order_contents.item != 'SENDPOINT'", $yearStartDate, $endDate, $rdb);
                    if (mysqli_num_rows($yearly_sales_query) > 0) {
                        while ($yearly_sales_read = mysqli_fetch_assoc($yearly_sales_query)) {
                            $yearlyActualSalesReport[$region] += $yearly_sales_read['net'];
                        }
                    } else {
                        $yearlyActualSalesReport[$region] = 0;
                    }
                }
            }
        }
        $maxDay = date('t');
        $curDay = date('d');
        $remainDay = $maxDay - $curDay;
        $fullMonthTrends = array();
        foreach($monthlyActualSalesReport as $region => $monthlyNet) {
            $fullMonthTrends[$region]  =  (($monthlyNet / $curDay) * $remainDay) + $monthlyNet;
        }
        $salesReport['actualWeekly'] = $weeklySalesReport;
        $salesReport['actualMonthly'] = $monthlyActualSalesReport;
        $salesReport['actualYearly'] = $monthlyActualSalesReport;
        $salesReport['fullMonthTrends'] = $fullMonthTrends;
        return array($salesReport, $regionConversionRate);
    }

    function emailDailySalesReport ($startDate, $endDate, $dailySalesReport, $regionConversionRate, $region, $manageDetails, $key) {
        if(!empty($dailySalesReport)) {

            $chartDetails['chartTitle'] = 'Daily Sales Vs Target Report';
            $chartDetails['rChartX'] = 'Region';
            $chartDetails['rChartY'] = 'Amounts';
            $chartDetails['lChartX'] = 'Location';
            $chartDetails['lChartY'] = 'Amounts';
            $chartDetails['fileName'] = '_daily_sales_chart';
            $message = ' Daily Sales Report for '. date('Y-m-d', strtotime($startDate)) . ' to '. date('Y-m-d', strtotime($endDate));
            createChartAndSendMail ($manageDetails, $dailySalesReport, $regionConversionRate, $chartDetails, $message, $region, $key);
        }
    }

    function emailDailyBeerSalesReport ($startDate, $endDate, $dailySalesReport, $regionConversionRate, $region, $manageDetails, $key) {
        if(!empty($dailySalesReport)) {
            $chartDetails['chartTitle'] = 'Daily Beer Sales Report';
            $chartDetails['rChartX'] = 'Region';
            $chartDetails['rChartY'] = 'Amounts';
            $chartDetails['lChartX'] = 'Location';
            $chartDetails['lChartY'] = 'Amounts';
            $chartDetails['fileName'] = '_daily_beer_chart';
            $message = ' Daily Beer Sales Report for '. date('Y-m-d', strtotime($startDate)) . ' to '. date('Y-m-d', strtotime($endDate));
            createChartAndSendMail ($manageDetails, $dailySalesReport, $regionConversionRate, $chartDetails, $message, $region, $key);
        }
    }

    function emailDailyFoodSalesReport ($startDate, $endDate, $dailySalesReport, $regionConversionRate, $region, $manageDetails, $key) {
        if(!empty($dailySalesReport)) {
            $chartDetails['chartTitle'] = 'Daily Food Sales Report';
            $chartDetails['rChartX'] = 'Region';
            $chartDetails['rChartY'] = 'Amounts';
            $chartDetails['lChartX'] = 'Location';
            $chartDetails['lChartY'] = 'Amounts';
            $chartDetails['fileName'] = '_daily_food_chart';
            $message = ' Daily Food Sales Report for '. date('Y-m-d', strtotime($startDate)) . ' to '. date('Y-m-d', strtotime($endDate));
            createChartAndSendMail ($manageDetails, $dailySalesReport, $regionConversionRate, $chartDetails, $message, $region, $key);
        }
    }

    function emailWeeklyRevenueReport ($startDate, $endDate, $weeklySalesReport, $regionConversionRate, $region, $manageDetails, $key) {
        if(!empty($weeklySalesReport)) {

            $chartDetails['chartTitle'] = 'Weekly Sales Report';
            $chartDetails['rChartX'] = 'Region';
            $chartDetails['rChartY'] = 'Amounts';
            $chartDetails['lChartX'] = 'Location';
            $chartDetails['lChartY'] = 'Amounts';
            $chartDetails['fileName'] = '_weekly_sales_chart';
            $message = ' Weekly Sales Report for '. date('Y-m-d', strtotime($startDate)) . ' to '. date('Y-m-d', strtotime($endDate));
            revenueHTMLFormatAndSendMail ($manageDetails, $weeklySalesReport, $regionConversionRate, $chartDetails, $message, $region, $key);
        }
    }

    function emailWeeklySalesReport ($startDate, $endDate, $weeklySalesReport, $regionConversionRate, $region, $manageDetails) {
        if(!empty($weeklySalesReport)) {

            $chartDetails['chartTitle'] = 'Weekly Sales Report';
            $chartDetails['rChartX'] = 'Region';
            $chartDetails['rChartY'] = 'Amounts';
            $chartDetails['lChartX'] = 'Location';
            $chartDetails['lChartY'] = 'Amounts';
            $chartDetails['fileName'] = '_weekly_sales_chart';
            $message = ' Weekly Sales Report for '. date('Y-m-d', strtotime($startDate)) . ' to '. date('Y-m-d', strtotime($endDate));
            //createChartAndSendMail ($manageDetails, $weeklySalesReport, $regionConversionRate, $chartDetails, $message, $region);
            createLineChartAndSendMail($manageDetails, $weeklySalesReport, $regionConversionRate, $chartDetails, $message, $region);
        }
    }

    function emailWeeklyFoodSalesReport ($startDate, $endDate, $weeklySalesReport, $regionConversionRate, $manageReportDetails) {

        if(!empty($weeklySalesReport)) {
            $chartDetails['chartTitle'] = 'Weekly Food Sales Report';
            $chartDetails['rChartX'] = 'Region';
            $chartDetails['rChartY'] = 'Amounts';
            $chartDetails['lChartX'] = 'Location';
            $chartDetails['lChartY'] = 'Amounts';
            $chartDetails['fileName'] = '_weekly_food_chart';
            $message = ' Weekly Food Sales Report for '. date('Y-m-d', strtotime($startDate)) . ' to '. date('Y-m-d', strtotime($endDate));
            createChartAndSendMail ($manageReportDetails, $weeklySalesReport, $regionConversionRate, $chartDetails, $message);
        }
    }

    function emailWeeklyBeerSalesReport ($startDate, $endDate, $weeklySalesReport, $regionConversionRate, $manageReportDetails) {

        if(!empty($weeklySalesReport)) {
            $chartDetails['chartTitle'] = 'Weekly Beer Sales Report';
            $chartDetails['rChartX'] = 'Region';
            $chartDetails['rChartY'] = 'Amounts';
            $chartDetails['lChartX'] = 'Location';
            $chartDetails['lChartY'] = 'Amounts';
            $chartDetails['fileName'] = '_weekly_beer_chart';
            $message = ' Weekly Beer Sales Report for '. date('Y-m-d', strtotime($startDate)) . ' to '. date('Y-m-d', strtotime($endDate));
            createChartAndSendMail ($manageReportDetails, $weeklySalesReport, $regionConversionRate, $chartDetails, $message);
        }
    }


    function createChartAndSendMail ($manageDetails, $salesReportData, $regionConversionRate, $chartDetails, $message, $mregion, $key) {
        global $imageDir, $from_mail, $from_name, $replyto;

        if ($mregion == 'All') {
            $regionWiseSalesReport = array();
            $convertRegionalSalesReport = array();
            foreach ($salesReportData as $region => $locationData) {
                foreach ($locationData as $location => $data) {
                    $regionWiseSalesReport[$region] += $data;
                }
            }
            foreach ($regionWiseSalesReport as $region => $rData) {
                $convertRegionalSalesReport[$region] = convertLocationCurrency($regionConversionRate[$region]['from'], $regionConversionRate[$region]['to'], $rData);
            }
            $myData = array();
            $chartTitle = 'All Region ' . $chartDetails['chartTitle'];
            $fileName = 'all_region' . $chartDetails['fileName'];
            if (!empty($targetSalesReport)) {
                $chartStr = createReportChart($chartTitle, $chartDetails['rChartX'], $chartDetails['rChartY'], 2);
                foreach ($convertRegionalSalesReport as $fRegion => $regionData) {
                    $myData[] = "['" . $fRegion . "', " . $regionData . ", " . $targetSalesReport['temp'] . "]";
                }
                $chartColumn = "['Region', 'Sale', 'Target']";
                $chartStr = str_replace('[chartColumn]', $chartColumn, $chartStr);
            } else {
                $chartStr = createReportChart($chartTitle, $chartDetails['rChartX'], $chartDetails['rChartY'], 1);
                foreach ($convertRegionalSalesReport as $fRegion => $regionData) {
                    $myData[] = "['" . $fRegion . "', " . $regionData . "]";
                }
                $chartColumn = "['Region', 'Sale']";
                $chartStr = str_replace('[chartColumn]', $chartColumn, $chartStr);
            }

            $chartData = implode(",", $myData);
            $chartStr = str_replace('[chartData]', $chartData, $chartStr);
            $chartStr = str_replace('[chartDiv]', $fileName, $chartStr);
            $htmlFile = $imageDir . $fileName . ".html";
            $attachFile = $fileName . ".jpg";
            $imgFile = $imageDir . $attachFile;
            unlink($htmlFile);
            unlink($imgFile);
            file_put_contents($htmlFile, $chartStr);
            convertHtmlToImage($htmlFile, $imgFile);
            if (file_exists($imgFile)) {
                $mailto = $manageDetails['send_to'];
                $message = 'ABI: All Region ' . $message;
                mail_attachment($attachFile, $imageDir, $mailto, $from_mail, $from_name, $replyto, $message, $message);
            }
        } else {
            if ($manageDetails['locations'] == 'All') {
                $myData = array();
                $allLocationSalesReport = array();
                $convertLocationsSalesReport = array();
                foreach ($salesReportData[$mregion] as $location => $locationData) {
                    $allLocationSalesReport[$mregion] += $locationData;
                }
                $convertLocationsSalesReport[$mregion] = convertLocationCurrency($regionConversionRate[$mregion]['from'], $regionConversionRate[$mregion]['to'], $allLocationSalesReport[$mregion]);
                $chartTitle = $mregion . ' (All Location) ' . $chartDetails['chartTitle'];
                $fileName = strtolower($mregion) . '_all_location' . $chartDetails['fileName'];
                $chartStr = createReportChart($chartTitle, $chartDetails['lChartX'], $chartDetails['lChartY'], 1);
                $myData[] = "['" . $mregion . "', " . $convertLocationsSalesReport[$mregion] . "]";
                $chartColumn = "['Region', 'Sale']";
                $chartStr = str_replace('[chartColumn]', $chartColumn, $chartStr);
                $mLocation = "All Locations";
            } else {
                $myData = array();
                $locations = explode(',', $manageDetails['locations']);
                $chartTitle = $mregion . ' (Locations) '. $chartDetails['chartTitle'];
                $fileName = strtolower($mregion) . '_' . $key . $chartDetails['fileName'];
                $chartStr = createReportChart($chartTitle, $chartDetails['lChartX'], $chartDetails['lChartY'], 1);
                foreach ($locations as $location) {
                    $locData = convertLocationCurrency($regionConversionRate[$mregion]['from'], $regionConversionRate[$mregion]['to'], $salesReportData[$mregion][$location]);
                    $myData[] = "['" . $location . "', " . $locData . "]";
                }
                $chartColumn = "['Region', 'Sale']";
                $chartStr = str_replace('[chartColumn]', $chartColumn, $chartStr);
                $mLocation = "Locations";
            }

            $chartData = implode(",", $myData);
            $chartStr = str_replace('[chartData]', $chartData, $chartStr);
            $chartStr = str_replace('[chartDiv]', $fileName, $chartStr);
            $htmlFile = $imageDir . $fileName . ".html";
            $attachFile = $fileName . ".jpg";
            $imgFile = $imageDir . $attachFile;
            unlink($htmlFile);
            unlink($imgFile);
            file_put_contents($htmlFile, $chartStr);
            convertHtmlToImage($htmlFile, $imgFile);
            if (file_exists($imgFile)) {
                $mailto = $manageDetails['send_to'];
                $eMessage = 'ABI: ' . $mregion .' ('.$mLocation.') ' . $message;
                mail_attachment($attachFile, $imageDir, $mailto, $from_mail, $from_name, $replyto, $eMessage, $eMessage);
            }
        }
    }

    function createLineChartAndSendMail($manageDetails, $salesReportData, $regionConversionRate, $chartDetails, $message, $mregion) {
        global $imageDir, $from_mail, $from_name, $replyto;
        $chartColumn = "['Day', 'Sale']";

        if ($mregion == 'All') {
            $regionWiseSalesReport = array();
            $regionDayWiseSalesReport = array();
            $convertRegionalSalesReport = array();
            $convertRegionalDaySalesReport = array();
            foreach ($salesReportData as $region => $locationData) {
                foreach ($locationData as $location => $dayWiseData) {
                    foreach ($dayWiseData as $day => $data) {
                        $regionWiseSalesReport[$region] += $data;
                        $regionDayWiseSalesReport[$region][$day] += $data;
                    }
                }
            }
            foreach ($regionWiseSalesReport as $region => $rData) {
                $convertRegionalSalesReport[$region] = convertLocationCurrency($regionConversionRate[$region]['from'], $regionConversionRate[$region]['to'], $rData);
            }
            foreach ($regionDayWiseSalesReport as $region => $rdayData) {
                foreach ($rdayData as $rday => $rdData) {
                    $convertRegionalDaySalesReport[$region][$rday] = convertLocationCurrency($regionConversionRate[$region]['from'], $regionConversionRate[$region]['to'], $rdData);
                }
            }
            $htmlString = '';
            foreach ($convertRegionalDaySalesReport as $fRegion => $regionDayData) {
                $attachFile = '';
                if ($convertRegionalSalesReport[$fRegion] > 0 ) {
                    $myData = array();
                    $fileName = 'all_region_' . $fRegion . $chartDetails['fileName'];
                    $chartStr = createRevenueLineChart(130, 70);
                    $chartStr = str_replace('[chartColumn]', $chartColumn, $chartStr);
                    foreach ($regionDayData as $day => $dData) {
                        $myData[] = "['" . $day . "', " . $dData . "]";
                    }
                    $chartData = implode(",", $myData);
                    $chartStr = str_replace('[chartData]', $chartData, $chartStr);
                    $chartStr = str_replace('[chartDiv]', $fileName, $chartStr);
                    $htmlFile = $imageDir . $fileName . ".html";
                    $attachFile = $fileName . ".jpg";
                    $imgFile = $imageDir . $attachFile;
                    unlink($htmlFile);
                    unlink($imgFile);
                    file_put_contents($htmlFile, $chartStr);
                    convertHtmlToImage($htmlFile, $imgFile, 130, 70);
                }
                $htmlString .= revenueHTMLString($fRegion, $convertRegionalSalesReport[$fRegion], $attachFile);
            }

            $hMessage = 'ABI: All Region ' . $message;
            $finalEmbedFile = 'final_all_region.html';
            $finalHTMLFile = $imageDir  . $finalEmbedFile;
            $finalAttachFile = 'final_all_region.jpg';
            $finalImgFile = $imageDir . $finalAttachFile;
            $revenueHTMLStr = revenueHTMLFormat($htmlString, $hMessage);
            unlink($finalHTMLFile);
            unlink($finalImgFile);
            file_put_contents($finalHTMLFile, $revenueHTMLStr);
            convertHtmlToImage($finalHTMLFile, $finalImgFile);
            if (file_exists($finalHTMLFile)) {
                $mailto = $manageDetails['send_to'];
                $emessage = 'ABI: All Region ' . $message;
                mail_attachment($finalAttachFile, $imageDir, $mailto, $from_mail, $from_name, $replyto, $emessage, $emessage, $revenueHTMLStr);
            }
        } else {
            if ($manageDetails['locations'] == 'All') {
                $htmlString = '';
                $myData = array();
                $allLocationSalesReport = array();
                $allLocationDayWiseSalesReport = array();
                $convertLocationsSalesReport = array();
                $convertLocationsDaySalesReport = array();
                foreach ($salesReportData[$mregion] as $location => $dayWiseData) {
                    foreach ($dayWiseData as $day => $data) {
                        $allLocationSalesReport[$mregion] += $data;
                        $allLocationDayWiseSalesReport[$mregion][$day] += $data;
                    }
                }

                foreach ($allLocationSalesReport as $region => $rData) {
                    $convertLocationsSalesReport[$region] = convertLocationCurrency($regionConversionRate[$region]['from'], $regionConversionRate[$region]['to'], $rData);
                }
                foreach ($allLocationDayWiseSalesReport as $region => $rdayData) {
                    foreach ($rdayData as $rday => $rdData) {
                        $convertLocationsDaySalesReport[$region][$rday] = convertLocationCurrency($regionConversionRate[$region]['from'], $regionConversionRate[$region]['to'], $rdData);
                    }
                }
                $attachFile = '';
                if ($convertLocationsSalesReport[$mregion] > 0 ) {
                    $fileName = strtolower($mregion) . '_all_location' . $chartDetails['fileName'];
                    $chartStr = createRevenueLineChart(130, 70);
                    $chartStr = str_replace('[chartColumn]', $chartColumn, $chartStr);
                    foreach ($convertLocationsDaySalesReport as $region => $regionDayData) {
                        foreach ($regionDayData as $day => $dData) {
                            $myData[] = "['" . $day . "', " . $dData . "]";
                        }
                    }
                    $chartData = implode(",", $myData);
                    $chartStr = str_replace('[chartData]', $chartData, $chartStr);
                    $chartStr = str_replace('[chartDiv]', $fileName, $chartStr);
                    $htmlFile = $imageDir . $fileName . ".html";
                    $attachFile = $fileName . ".jpg";
                    $imgFile = $imageDir . $attachFile;
                    unlink($htmlFile);
                    unlink($imgFile);
                    file_put_contents($htmlFile, $chartStr);
                    convertHtmlToImage($htmlFile, $imgFile, 130, 70);
                }
                $htmlString .= revenueHTMLString($mregion, $convertLocationsSalesReport[$mregion], $attachFile);
                $mLocation = $mregion . " (All Locations)";
            } else {
                $locations = explode(',', $manageDetails['locations']);
                $htmlString = '';
                $locationSalesReport = array();
                $locationDayWiseSalesReport = array();
                $convertLocationsSalesReport = array();
                $convertLocationsDaySalesReport = array();
                foreach ($locations as $location) {
                    foreach ($salesReportData[$mregion][$location] as $day => $dData) {
                        $locationSalesReport[$mregion][$location] += $dData;
                        $locationDayWiseSalesReport[$mregion][$location][$day] += $dData;
                    }
                }

                foreach ($locationSalesReport[$mregion] as $location => $lData) {
                    $convertLocationsSalesReport[$mregion][$location] = convertLocationCurrency($regionConversionRate[$mregion]['from'], $regionConversionRate[$mregion]['to'], $lData);
                }
                foreach ($locationDayWiseSalesReport[$mregion] as $location => $ldayData) {
                    foreach ($ldayData as $lday => $ldData) {
                        $convertLocationsDaySalesReport[$mregion][$location][$lday] = convertLocationCurrency($regionConversionRate[$mregion]['from'], $regionConversionRate[$mregion]['to'], $ldData);
                    }
                }
                foreach ($convertLocationsDaySalesReport[$mregion] as $fLocation => $locationDayData) {
                    $myData = array();
                    $attachFile = '';
                    if ($convertLocationsSalesReport[$mregion][$fLocation] > 0 ) {
                        $fileName = 'location_' . $fLocation . $chartDetails['fileName'];
                        $chartStr = createRevenueLineChart(130, 70);
                        $chartStr = str_replace('[chartColumn]', $chartColumn, $chartStr);
                        foreach ($locationDayData as $day => $dData) {
                            $myData[] = "['" . $day . "', " . $dData . "]";
                        }
                        $chartData = implode(",", $myData);
                        $chartStr = str_replace('[chartData]', $chartData, $chartStr);
                        $chartStr = str_replace('[chartDiv]', $fileName, $chartStr);
                        $htmlFile = $imageDir . $fileName . ".html";
                        $attachFile = $fileName . ".jpg";
                        $imgFile = $imageDir . $attachFile;
                        unlink($htmlFile);
                        unlink($imgFile);
                        file_put_contents($htmlFile, $chartStr);
                        convertHtmlToImage($htmlFile, $imgFile, 130, 70);
                    }
                    $htmlString .= revenueHTMLString($fLocation, $convertLocationsSalesReport[$mregion][$fLocation], $attachFile);
                }
                $mLocation = $mregion . " (Locations)";
            }
            $hMessage = 'ABI: ' . $mLocation . $message;
            $finalEmbedFile = 'final_' . $mregion . '_all_location.html';
            $finalHTMLFile = $imageDir  . $finalEmbedFile;
            $finalAttachFile = 'final_' . $mregion . '_all_location.pdf';
            $finalPdfFile = $imageDir . $finalAttachFile;
            $revenueHTMLStr = revenueHTMLFormat($htmlString, $hMessage);

            unlink($finalHTMLFile);
            unlink($finalPdfFile);
            file_put_contents($finalHTMLFile, $revenueHTMLStr);
            convertHtmlToPdf($finalHTMLFile, $finalPdfFile);
            if (file_exists($finalHTMLFile)) {
                $mailto = $manageDetails['send_to'];
                mail_attachment($finalAttachFile, $imageDir, $mailto, $from_mail, $from_name, $replyto, $hMessage, $hMessage, $revenueHTMLStr);
            }
        }
    }

    function emailNetRevenueTrackerReport ($startDate, $endDate, $salesReport, $regionConversionRate, $region, $manageReportDetails) {
        global $imageDir, $reportRegions, $staticPlanRecord, $from_mail, $from_name, $replyto;
        if(!empty($salesReport)) {
            if ($region != 'All') {
                $reportRegions = array();
                $reportRegions[0] = $region;
            }
            $convertWeeklySalesReport = array();
            $convertMonthlySalesReport = array();
            $convertYearlySalesReport = array();
            $convertfullMonthTrendsReport = array();
            foreach ($reportRegions as $region) {
                $convertWeeklySalesReport[$region] = convertLocationCurrency($regionConversionRate[$region]['from'], $regionConversionRate[$region]['to'], $salesReport['actualWeekly'][$region]);
                $convertMonthlySalesReport[$region] = convertLocationCurrency($regionConversionRate[$region]['from'], $regionConversionRate[$region]['to'], $salesReport['actualMonthly'][$region]);
                $convertYearlySalesReport[$region] = convertLocationCurrency($regionConversionRate[$region]['from'], $regionConversionRate[$region]['to'], $salesReport['actualYearly'][$region]);
                $convertfullMonthTrendsReport[$region] = convertLocationCurrency($regionConversionRate[$region]['from'], $regionConversionRate[$region]['to'], $salesReport['fullMonthTrends'][$region]);
            }

            $trString = '';
            $month = date('m');
            $year = date('Y');
            $row = 0;
            foreach ($reportRegions as $region) {
                $fullMonthPlan = $staticPlanRecord[$region][$year][$month];
                $fullMonthTrends = $convertfullMonthTrendsReport[$region];
                $trendsVsPlan = round((($fullMonthTrends - $fullMonthPlan) * 100) / $fullMonthPlan);
                $actualYearPlan = $staticPlanRecord[$region][$year]['FY'];
                $actualfullYear = $convertYearlySalesReport[$region];

                $actualVsPlan = round((($actualfullYear - $actualYearPlan) * 100) / $actualYearPlan);
                $revenueTrackerData['poc_name'] = $region;
                $revenueTrackerData['weekly_sales'] = $convertWeeklySalesReport[$region];
                $revenueTrackerData['mtd_actual'] =  $convertMonthlySalesReport[$region];
                $revenueTrackerData['full_month_trend'] = $fullMonthTrends;
                $revenueTrackerData['full_month_plan'] = $fullMonthPlan;
                $revenueTrackerData['trends_vs_plan'] = $trendsVsPlan;
                $revenueTrackerData['ytd_actuals'] = $actualfullYear;
                $revenueTrackerData['ytd_plan'] = $actualYearPlan;
                $revenueTrackerData['actual_vs_plan'] = $actualVsPlan;
                $revenueTrackerData['row'] = $row;

                $trString .= revenueTrackerHTMLString($revenueTrackerData);
                $row++;
            }

            $header = 'POC Net Revenue Tracker in \'000';
            $trackerHTMLStr = revenueTrackerHTMLFormat($startDate, $endDate, $trString, $header);
            $fileName = 'net_revenue_tracker';
            $trackerEmbedFile = $fileName.'.html';
            $trackerAttachFile = $fileName.'.pdf';
            $trackerHTMLFile = $imageDir  . $trackerEmbedFile;
            $trackerPDFFile = $imageDir  . $trackerAttachFile;
            unlink($trackerHTMLFile);
            unlink($trackerPDFFile);
            file_put_contents($trackerHTMLFile, $trackerHTMLStr);
            convertHtmlToPdf($trackerHTMLFile, $trackerPDFFile);

            $message = ' Net revenue tracker for '. date('Y-m-d', strtotime($startDate)) . ' to '. date('Y-m-d', strtotime($endDate));
            if (file_exists($trackerHTMLFile)) {
                $mailto = $manageReportDetails['send_to'];
                mail_attachment($trackerAttachFile, $imageDir, $mailto, $from_mail, $from_name, $replyto, $message, $message, $trackerHTMLStr);
            }
        }
    }

    function revenueHTMLFormat($trString, $header) {
        $tabularString = '';
        $tabularString .= '<table style="width:75%; font-size: 10px; font-family: Arial Black; border:1px solid #000000;"><tr><td style="width:20%; font-weight: bold; font-size: 12px; text-align: center; border-bottom: 1px solid #000000">POC Name</td><td style="width:15%; font-weight: bold; font-size: 12px; text-align: center; border-bottom: 1px solid #000000">Sales</td><td style="width:65%; font-weight: bold; font-size: 12px; border-bottom: 1px solid #000000">&nbsp;</td></tr>';
        $tabularString .= $trString;
        $tabularString .= '</table>';
        $htmlString = '';
        $htmlString .= '<!DOCTYPE html><html><head><title>ABI Report</title></head><body>';
        $htmlString .= '<h1>'.$header.'</h1>';
        $htmlString .= $tabularString;
        //$htmlString .= "<p>Please find attachement</p>";
        $htmlString .= '</body></html>';
        return $htmlString;
    }

    function revenueTrackerHTMLFormat($startDate, $endDate, $trString, $header) {
        $tabularString = '';
        $startTimeStamp = strtotime($startDate);
        $endTimeStamp = strtotime($endDate);
        $weekColumnName = 'Week of <br/>'.date('m/d', $startTimeStamp) . ' - '. date('m/d', $endTimeStamp);
        $tabularString .= '<table cellpadding="2" width="100%" style="font-size: 10px; font-family: Arial Black; border:1px solid #000000;">'.PHP_EOL;
        $tabularString .= '<tr style="border-bottom: 1px solid #000000; font-weight: bold; font-size: 10px;"><td width="10%" style="border-right: 1px solid #000000; border-bottom: 1px solid #000000; text-align:center">Region</td><td width="10%" align="center" style="border-right: 1px solid #000000; border-bottom: 1px solid #000000; text-align:center">' . $weekColumnName . '</td><td width="10%" style="border-right: 0px solid #000000; border-bottom: 1px solid #000000; text-align:center">MTD<br/>Actual</td><td width="10%" style="border-right: 0px solid #000000; border-bottom: 1px solid #000000; text-align:center">Full Month<br/>Trend</td><td width="10%" style="border-right: 0px solid #000000; border-bottom: 1px solid #000000; text-align:center">Full Month<br/>Plan</td><td align="center" width="10%" style="border-right: 1px solid #000000; border-bottom: 1px solid #000000">Trend vs<br/>Plan (%)</td><td width="10%" style="border-left: 0px solid #000000; border-bottom: 1px solid #000000; text-align:center">YTD Actuals</td><td width="10%" style="border-right: 0px solid #000000; border-bottom: 1px solid #000000; text-align:center">YTD Plan</td><td width="10%" style="border-bottom: 1px solid #000000; text-align:center">Actual vs<br/>Plan (%)</td></tr>'.PHP_EOL;
        $tabularString .= $trString;
        $tabularString .= '</table>';
        $htmlString = '';
        $htmlString .= '<!DOCTYPE html><html><head><title>ABI Report</title></head><body>'.PHP_EOL;
        $htmlString .= '<h1>'.$header.'</h1>';
        $htmlString .=  $tabularString;
        $htmlString .= '</body></html>';
        return $htmlString;
    }

    function revenueHTMLString($poc_name, $sales, $filename) {
        global $emailImageBaseURL, $dataLocationNameMapping;
        $pocName = (isset($dataLocationNameMapping[$poc_name]['name'])) ? $dataLocationNameMapping[$poc_name]['name'] : $poc_name;
        $lineGraph = ($sales > 0 ) ? '<img src="'.$emailImageBaseURL.$filename.'?t='.time().'" style="margin-top:0px;" alt="Line Graph" />' : '&nbsp;';
        $trString = '<tr><td style="border-bottom:1px solid #000000; vertical-align: middle">'.$pocName.'</td><td style="border-bottom:1px solid #000000; text-align: center; vertical-align: middle">$&nbsp;&nbsp;'.$sales.'</td><td style="border-bottom:1px solid #000000; text-align: center; vertical-align: middle">'.$lineGraph.'</td></tr>'.PHP_EOL;
        return $trString;
    }

    function revenueTrackerHTMLString($revenueTrackerData) {
        global $dataLocationNameMapping;
        $trString = '';
        $pocName = (isset($dataLocationNameMapping[$revenueTrackerData['poc_name']]['name'])) ? $dataLocationNameMapping[$revenueTrackerData['poc_name']]['name'] : $revenueTrackerData['poc_name'];
        $bgColor = ($revenueTrackerData['row'] % 2 == 0) ? 'background-color:#DBE6F0' : 'background-color:#F2F2F2';
        $trendPlanColor = ($revenueTrackerData['trends_vs_plan'] > 0 ) ? 'color:#00aa00' : 'color:#ff0000';
        $actualPlanColor = ($revenueTrackerData['actual_vs_plan'] > 0 ) ? 'color:#00aa00' : 'color:#ff0000';
        $trString .= '<tr style="border:1px solid #000000; '.$bgColor.';"><td valign="top" style="border-right:1px solid #000000;">'.$pocName.'</td><td valign="top" align="center" style="border-right:1px solid #000000;">$&nbsp;&nbsp;'.$revenueTrackerData['weekly_sales'].'</td><td valign="top" align="center">$&nbsp;&nbsp;' . $revenueTrackerData['mtd_actual'] . '</td><td valign="top" align="center">$&nbsp;&nbsp;' . $revenueTrackerData['full_month_trend'] . '</td><td valign="top" align="center">$&nbsp;&nbsp;' . $revenueTrackerData['full_month_plan'] . '</td><td valign="top" align="center" style="'.$trendPlanColor.' ;border-right:1px solid #000000; text-align:center">' . $revenueTrackerData['trends_vs_plan'] . '%</td><td valign="top" align="center">$&nbsp;&nbsp;' . $revenueTrackerData['ytd_actuals'] . '</td><td valign="top" align="center">$&nbsp;&nbsp;' . $revenueTrackerData['ytd_plan'] . '</td><td valign="top" align="center" style="'.$actualPlanColor.' ;border-right:0px solid #000000;">' . $revenueTrackerData['actual_vs_plan'] . '%</td></tr>'.PHP_EOL;
        return $trString;
    }

    // Get email_schedule, region, location and manage email list from reports_email_schedule table
    function reportSendToEmailId($email_schedule, $chain_id) {
        $schedularDetails = array();
        $query = mlavu_query("SELECT * FROM `poslavu_MAIN_db`.`reports_email_schedule` WHERE  `email_schedule` = '[1]' AND `chain_id` = '[2]' AND `_deleted` != '[3]'", $email_schedule, $chain_id, 1);
        if(mysqli_num_rows($query) > 0) {
            while ($read = mysqli_fetch_assoc($query)) {
                if ($read['region'] == 'All') {
                    $schedularDetails[$read['region']][$read['id']]['region'] = 'All';
                    $schedularDetails[$read['region']][$read['id']]['send_to'] = $read['send_to'];
                    $schedularDetails[$read['region']][$read['id']]['report_type'] = $read['report_type'];
                } else {
                    if ($read['location_name'] == '') {
                        $schedularDetails[$read['region']][$read['id']]['locations'] = 'All';
                        $schedularDetails[$read['region']][$read['id']]['send_to'] = $read['send_to'];
                    } else {
                        $schedularDetails[$read['region']][$read['id']]['locations'] = $read['location_name'];
                        $schedularDetails[$read['region']][$read['id']]['send_to'] = $read['send_to'];
                    }
                    $schedularDetails[$read['region']][$read['id']]['report_type'] = $read['report_type'];
                }
            }
        }

        return $schedularDetails;
    }
