<?php

	//Written by Brian Dennedy
	ini_set('display_errors', 0);
	//"Gift Card ID"=>"name","Expires"=>'expires',"Balance"=>"balance","Points"=>'points'
	/*
	 * Please maintain header sequence because we are using this sequence to fix column width.
	 */
	$allowedColumnsAndTitleToDBMappingArr = array();
	$allowedColumnsAndTitleToDBMappingArr["Gift Card ID"] = 'name';
	$allowedColumnsAndTitleToDBMappingArr["Expires"] = 'expires';
	$allowedColumnsAndTitleToDBMappingArr["Balance"] = 'balance';
	$allowedColumnsAndTitleToDBMappingArr["Points"] = 'points';
	$allowedColumnsAndTitleToDBMappingArr["Date Created"] = 'created_datetime';

	 if (isset($_POST['action']) && $_POST['action'] != '') {
		require_once(dirname(__DIR__).'/../resources/csv_input_output.php');
		require_once(dirname(__DIR__)."/../resources/core_functions.php");

		$backupData = array();
		$dataname = $_POST['data_name'];
		$action = $_POST['action'];
		$where = "";
		$displayName = str_replace("_", " ", $action);
		if ($action == 'delete_selected_entries') {
		$giftcardId = json_decode($_POST['gift_card_id'], true);
		
		$giftcardIds = "";
		$count = 0;
		foreach ($giftcardId as $recordId) {
			if ($count == 0) {
				$giftcardIds = "'".$recordId."'";
			}
			else {
				$giftcardIds .= ",'".$recordId."'";
			}
			
			$count++;
		}
		
		$where = " `dataname`='".$dataname."' and `name` in ($giftcardIds)";
		$backupData = backupSelectedEntries($where);
		}
		else if ($action == 'delete_all_entries') {
		$where = " `dataname`='".$dataname."' ";
		$backupData = backupSelectedEntries($where);
		}
		else {
			exit;
		}
		
		if (!empty($backupData)) {
			$csvDataString = getCSVStringFromArrays($backupData, ",", '"');
			header('Content-disposition: attachment; filename='.$displayName.'_'.date('YmdHis').'.csv');
			header ("Content-Type:text/txt");
			echo $csvDataString;
			exit;
		}
	}
	
	function backupSelectedEntries ($condition) {
		global $allowedColumnsAndTitleToDBMappingArr;
		$resultData = array();
		$backup_tableName = 'lavu_gift_cards_backup';
		$orginal_tableName = 'lavu_gift_cards';
		$usesMainDb = 'poslavu_MAIN_db';
		$checkTable = mlavu_query("SHOW TABLES LIKE '".$backup_tableName."'");
		$tableCount = mysqli_num_rows($checkTable);
		if (!$tableCount) {
			echo 'Warning :: In '.$usesMainDb.' , '.$backup_tableName.' table is not available for Backup. Please Create Table.';
			exit;
		}
		$selectedData = array();

		$result = mrpt_query("SELECT * FROM `".$orginal_tableName."` WHERE $condition ORDER BY `id` ASC");
		$count = 0;

		$totalRecords = mysqli_num_rows($result);
		if ($totalRecords > 0) {
			while ($resultRow = mysqli_fetch_assoc($result)) {
				$selectedData[$count]['name'] = $resultRow['name'];
				$selectedData[$count]['expires'] = $resultRow['expires'];
				$selectedData[$count]['balance'] = $resultRow['balance'];
				$selectedData[$count]['points'] = $resultRow['points'];
				$selectedData[$count]['created_datetime'] = $resultRow['created_datetime'];

				$insertValue = "('".$resultRow['chainid']."','".$resultRow['dataname']."','".$resultRow['name']."','".$resultRow['balance']."','".$resultRow['expires']."','".$resultRow['created_datetime']."','".$resultRow['history']."','".$resultRow['id']."','".$resultRow['points']."','".$resultRow['inactive']."','".$resultRow['created_ts']."','".$resultRow['expires_ts']."')";
				$insertQuery = "INSERT INTO `".$backup_tableName."` ( chainid, dataname, name, balance, expires, created_datetime, history, lavu_gift_id, points, inactive, created_ts, expires_ts) VALUES ".$insertValue.";";
				$insertResult = mlavu_query($insertQuery);
				if ($insertResult) {
					$count ++;
				}
			}
		}

		if ($totalRecords == $count) {
			$DeleteResult = mlavu_query("DELETE FROM `".$orginal_tableName."` WHERE $condition");
		}

		if ($DeleteResult) {
			$displayLabels[] = array_keys($allowedColumnsAndTitleToDBMappingArr);
			$resultData = array_merge($displayLabels, $selectedData);
		}
		
		return $resultData;
	}

	function getTableDataForExport($contextArr = null){

		global $allowedColumnsAndTitleToDBMappingArr;
		$columnList = implode(', ', array_values($allowedColumnsAndTitleToDBMappingArr));

		//echo "<pre>".print_r($_SESSION,1)."</pre>";
		//exit;
		$dataname = $_SESSION['poslavu_234347273_admin_dataname'];
		///$database = $_SESSION['poslavu_234347273_admin_database']; //Don't need this.
		$contextArr['sdie_dataname'] = $dataname;
		if(empty($dataname) ){
			echo "Could not find dataname.<br>";
			return false;
		}
		$context = $contextArr['sdie_context'];
		if(!empty($context) && $context == 'table_display'){
			$whereClause = "WHERE `deleted` = 0 AND dataname = '[sdie_dataname]' ";
			if( $contextArr['sdie_asc_desc_order'] == 'asc' ){ //Rows are displayed ASC based on order_field.
				if(!empty($contextArr['sdie_barrier_value'])){
					if($contextArr['sdie_paging_direction'] == 'forward'){
						$whereClause .= " AND `[sdie_order_by_column]` > '[sdie_barrier_value]'";
						$orderByClause = "ORDER BY [sdie_order_by_column] ASC";
						$limitClause = "LIMIT [sdie_limit_count]";
						$reverseResults = false;
						$error_code = "-z2wavrgd-";
					}else{
						$whereClause .= " AND `[sdie_order_by_column]` < '[sdie_barrier_value]'";
						$orderByClause = "ORDER BY [sdie_order_by_column] DESC";//Double reverse
						$limitClause = "LIMIT [sdie_limit_count]";
						$reverseResults = true;
						$error_code = "-huimuh-";
					}
				}else{ // First load listing ascending, no paging.
					$orderByClause = "ORDER BY [sdie_order_by_column] ASC";
					$limitClause = "LIMIT [sdie_limit_count]";
					$reverseResults = false;
					$error_code = "-d4tv5vdd-";
				}
			}else{ ////Rows are displayed DESC based on order_field.
				if(!empty($contextArr['sdie_barrier_value'])){
					if($contextArr['sdie_paging_direction'] == 'forward'){
						$whereClause .= " AND `[sdie_order_by_column]` < '[sdie_barrier_value]'";
						$orderByClause = "ORDER BY [sdie_order_by_column] DESC";
						$limitClause = "LIMIT [sdie_limit_count]";
						$reverseResults = false;
						$error_code = "-t78mt-";
					}else{
						$whereClause .= " AND `[sdie_order_by_column]` > '[sdie_barrier_value]'";
						$orderByClause = "ORDER BY [sdie_order_by_column] ASC";
						$limitClause = "LIMIT [sdie_limit_count]";
						$reverseResults = true;
						$error_code = "-srbe-";
					}
				}else{
					$orderByClause = "ORDER BY [sdie_order_by_column] DESC";
					$limitClause = "LIMIT [sdie_limit_count]";
					$reverseResults = false;
					$error_code = "-bdu64-";
				}
			}
			$queryStr = "SELECT $columnList FROM `poslavu_MAIN_db`.`lavu_gift_cards` $whereClause $orderByClause $limitClause";
			$result = mrpt_query($queryStr, $contextArr);
			if(!$result){echo "Error trying to receive data $error_code :".mlavu_dberror(); return false;}
		}else if(!empty($context) && $context == 'search_table_display'){
			$searchTermString = trim($contextArr['sdie_search_value']);
			$queryString = '';
			$queryString .= "SELECT $columnList FROM `poslavu_MAIN_db`.`lavu_gift_cards` WHERE `deleted`=0 AND `dataname`='[2]' AND (";
			$i = 0;
			foreach($allowedColumnsAndTitleToDBMappingArr as $columnDisplayTitleAsKey => $dbColumnNameAsVal){
				$queryString .= "`".$dbColumnNameAsVal."` LIKE '%[1]%' ";
				if(++$i < count($allowedColumnsAndTitleToDBMappingArr)){
					$queryString .= "OR ";
				}
			}
			$queryString .= ") LIMIT 50";
			$result = mrpt_query($queryString, $searchTermString, $dataname);
		}else if(!empty($context) && $context == 'search_table_display'){
			$searchTermString = trim($contextArr['sdie_search_value']);
			$queryString = '';
			$queryString .= "SELECT $columnList FROM `poslavu_MAIN_db`.`lavu_gift_cards` WHERE `deleted`=0 AND  ";
			$i = 0;
			foreach($allowedColumnsAndTitleToDBMappingArr as $columnDisplayTitleAsKey => $dbColumnNameAsVal){
				$queryString .= "`".$dbColumnNameAsVal."` LIKE '%[1]%' ";
				if(++$i < count($allowedColumnsAndTitleToDBMappingArr)){
					$queryString .= "OR ";
				}
			}
			$queryString .= " LIMIT 50";
			$result = rpt_query($queryString, $searchTermString);
		}else{ //We are pulling for .csv export
			$result = mrpt_query("SELECT $columnList FROM `poslavu_MAIN_db`.`lavu_gift_cards` WHERE `deleted`=0 AND  `dataname`='[1]' ORDER BY `id` ASC", $dataname);
			if(!$result){echo "Error trying to receive data -resiipu-:".mlavu_dberror(); return false;}
		}

		// based on date format setting
		$date_setting = "date_format";
		$location_date_format_query = rpt_query( "SELECT `location`, `setting`, `value` FROM `config` WHERE `setting` = '[1]'",  $date_setting);
		if( !$location_date_format_query || !mysqli_num_rows($location_date_format_query)){
			$date_format = array("value" => 2);
		}else
			$date_format = mysqli_fetch_assoc( $location_date_format_query );

		//Build the data array.
		$dataArr = array();
		$needKeys = true;
		$keysArrFirstRow;
		while($currRow = mysqli_fetch_assoc($result)){
			foreach($currRow as $key => $val){
				if($key == "created_datetime"){
					$split_date = explode(" ", $val);
					$change_format = explode("-", $split_date[0]);
					switch ($date_format['value']){
						case 1:$currRow[$key] = $change_format[2]."-".$change_format[1]."-".$change_format[0]." ".$split_date[1];
						break;
						case 2:$currRow[$key] = $change_format[1]."-".$change_format[2]."-".$change_format[0]." ".$split_date[1];
						break;
						case 3:$currRow[$key] = $change_format[0]."-".$change_format[1]."-".$change_format[2]." ".$split_date[1];
						break;
					}
				}
			}
			if($needKeys){
				$keysArrFirstRow = array_keys($currRow);
				$needKeys = false;
			}
			$dataArr[] = array_values($currRow);
		}
		if($reverseResults){
			$dataArr = array_reverse($dataArr);
		}
		$returnArr = array_merge(array($keysArrFirstRow), $dataArr);
		return $returnArr;
	}

	//Need to get the chain id.
	

	require_once(dirname(__FILE__).'/../../resources/simple_display_import_export.php');


	$datetime=localize_datetime(date('Y-m-d H:i:s'), $location_info['timezone']);

	setSimpleDisplayTableTitleName('Gift Cards');
	setSimpleDisplayTable('lavu_gift_cards');
	// For poslavu_MAIN_db
	setSimpleDisplayDatabase('poslavu_MAIN_db');//Used for special dbs like poslavu_MAIN_db, or poslavu_QUICKBOOKS_db etc.
	setSimpleDisplayTitles2DBColumnsMap($allowedColumnsAndTitleToDBMappingArr);

	//Default values...
	if(empty($data_name) && !empty($_POST['data_name'])){$data_name=$_POST['data_name'];}
	$defaultMappingArray = array('dataname'=>$data_name,'created_datetime'=>$datetime);
	$chain_id=getChainID();
	if ( !empty($chain_id) ) { $defaultMappingArray['chainid'] = $chain_id; }
	setSimpleDisplayDefaultValuesForDBMap($defaultMappingArray);   //TODO, SHOULD VALIDATE THE DEFAULT VALUE MAP, THAT COLUMNS ACTUALLY EXIST.

	//these are the 'titles' as in what the merchant knows the columns as, not the actual db column names.
	setSimpleDisplayMandatoryTitleColumns( array("Gift Card ID") );
	setSimpleDisplayOptionalTitlesColumns( array("Balance","Points","Expires","Date Created") );

	setSimpleDisplayCSVExampleText("Gift Card ID,Expires,Balance,Points\n293083892509508,2100,14.95,0\n293083892509508,2150,0.0,15\n...");

	setSimpleDisplayOrderByColumn4TableDisplay('name');//Uses the database column name.

	//setSimpleDisplayOrderByColumn4TableDisplay('created_datetime');
	//TODO consider if this should be set or not...
	setSimpleDisplayDisallowDuplicatesAcrossColumns( array('dataname','name') );//Function for not allowing duplicates across certain fields.
	//setSimpleDisplayUpdateWhenColumnsAreEqualArrays();
	setQueryFunctionForExportArrays('getTableDataForExport');
	setSimpleDisplayExportFilePathForPostbackURL(__FILE__);
	simpleDisplayLaunch();

	function getChainID(){
		global $data_name;
		$result = mrpt_query("SELECT `chain_id` FROM `poslavu_MAIN_db`.`restaurants` WHERE `data_name`='[1]'",$data_name);
		if(!$result){error_log("mysql error in ".__FILE__.' -dhsc- mysql_error:'.mlavu_dberror());}
		if(mysqli_num_rows($result) == 0){error_log("Error finding chain_id in file:".__FILE__);}
		$restaurantTableRow = mysqli_fetch_assoc($result);
		return $restaurantTableRow['chain_id'];
	}
