<style type="text/css">
.row{
	margin-left:150px;
}
.header{
	font-weight: 700;
}
.rowElement{
	float:left;
	border-bottom: 1px solid #666;
	padding: 5px 0 5px 0;
}
.re1{
	width:150px;
}
.re2{
	width:100px;
	
}
.re3{
	width:200px;
	
}
.re4{
	width:100px;
	
}
.re5{
	width:100px;
	
}
.re6{
	width:100px;
	
}

.hidden_info{
	background-color: #999;
}
.clearRow{
	clear:both;
}
.hold_date{
	float:left;
	width:200px;
	border-right:1px solid #ccc;
}
.date{
	margin-left:260px;
}
</style>

<script type='text/javascript'>
var showing = "";//which row is visible right now?
function showReport(class_id){
		if (showing == class_id){
				document.getElementById(class_id).style.display = 'none';
				showing = "";
				return;
		}//if
		if (showing != ""){
				document.getElementById(showing).style.display = 'none';
		}//if
		document.getElementById(class_id).style.display = 'inline';
		showing = class_id;
		return;
}//showPartInfo()
function submitForm(){
		//alert("hi");
		start_month = document.getElementById("start_month").value;
		start_day = document.getElementById("start_day").value;
		start_year = document.getElementById("start_year").value;
		
		end_month = document.getElementById("end_month").value;
		end_day = document.getElementById("end_day").value;
		end_year = document.getElementById("end_year").value;
		
		link = "index.php?mode=reports_p2r_parts_cost_by_kart&start_month="+start_month+"&start_day="+start_day+"&start_year="+start_year;
		link += "&end_month="+end_month+"&end_day="+end_day+"&end_year="+end_year;
		window.location = link;
		return;
}//submitForm()
</script>
<h2>Part Costs by Kart</h2>


<?php
$cur_date = date("Y-m-d");
$cur_date_arr = explode("-",$cur_date);
$cur_month = $cur_date_arr[1];
$cur_year = $cur_date_arr[0];
$cur_day = $cur_date_arr[2];

if (!reqvar("start_month") && !reqvar("start_day") && !reqvar("start_year") && !reqvar("end_month") && !reqvar("end_month") && !reqvar("end_month")){
		$start_date 	= date("Y-m-d",mktime(0,0,0,$cur_month*1-1,$cur_day*1,$cur_year*1));
		$end_date 	= date("Y-m-d",mktime(0,0,0,$cur_month*1,$cur_day*1,$cur_year*1));
}//if
else{
		$start_date = reqvar("start_year")	.	"-"	.	reqvar("start_month")	.	"-"	.	reqvar("start_day");
		$end_date  = reqvar("end_year")	.	"-"	.	reqvar("end_month")	.	"-"	.	reqvar("end_day"); 
}//else
//echo "start_date:$start_date<br>";
//echo "end_date:$end_date<br>";
?>

<div class="date">
	<div class='hold_date'>
	
		<?php selectDate("Start Day","start",$start_date);?>
	</div>
	<div class='hold_date'>
		<?php selectDate("End Day","end",$end_date);?>
	</div>
	<div class='clearRow'></div>
</div><!--date-->
<br/>
<input type="button" name="submit" value="Go" onclick="submitForm();"/>



<?php
$user_has_specified_date = false;
$start_date       = reqvar("start_year")."-".reqvar("start_month")."-".reqvar("start_day");
$end_date 		 = reqvar("end_year")."-".reqvar("end_month")."-".reqvar("end_day");

$sql         			 = "SELECT DISTINCT(lk_service.kart_id) AS kart_id, lk_karts.number AS number FROM lk_service LEFT JOIN lk_karts ON lk_karts.id=lk_service.kart_id";
$sql					.= " WHERE lk_karts.number!='' AND LEFT(datetime,10)>='$start_date' AND LEFT(datetime,10)<='$end_date' ORDER BY lk_karts.number*1";
//echo "sql: $sql<br>";
$kart_ids_res   = lavu_query($sql);


//echo "<select name='select_kart' id='select_kart' onchange='showPartInfo(this.value);'>";
//echo "<option value=''>Choose a kart</option>";
echo 					"<br/><br/><br/>";
echo 					"<div class='row header'>";
echo 					"<div class='rowElement re1'>Kart Number</div>";
echo 					"<div class='rowElement re2'>Cost to Date</div>";
echo 					"<div class='clearRow'></div>";
echo 					"</div>";
echo 					"<div class='clearRow'></div>";


while ($kart_row = mysqli_fetch_assoc($kart_ids_res)){

		$kart_id 				= 	$kart_row['kart_id'];
		
		$kart_number		=  $kart_row['number'];
		$sql						= 	"SELECT SUM(part_cost*quantity) AS part_costs FROM lk_service WHERE kart_id='$kart_id' AND LEFT(datetime,10)>='$start_date' AND LEFT(datetime,10)<='$end_date'";
		//echo "sql1: $sql<br>";
		$cost_res				=	lavu_query($sql);
		$cost_row			= mysqli_fetch_assoc($cost_res);
		$cost 					= $cost_row['part_costs'];
		$class_name		= "kart_".$kart_id;
		echo 					"<div class='row ' onclick='showReport(\"$class_name\");'>";
		echo 					"<div class='rowElement re1'>$kart_number</div>";
		echo 					"<div class='rowElement re2'>&#36;".number_format($cost,2)."</div>";
		echo 					"<div class='clearRow'></div>";
		echo 					"</div>";
		echo 					"<div class='clearRow'></div>";
		
		/* First row of hidden report */
		echo 					"<div id='$class_name' style='display:none;' onclick='showReport(\'$class_name\');'>";
		echo 					"<div class='row header hidden_info'>";
		echo 					"<div class='rowElement re1'>Date</div>";
		echo 					"<div class='rowElement re2'>Cost per unit</div>";
		echo 					"<div class='rowElement re3'>Part name</div>";
		echo 					"<div class='rowElement re4'>Part ID</div>";
		echo 					"<div class='rowElement re5'>Qty</div>";
		echo 					"<div class='rowElement re6'>Total</div>";
		echo 					"<div class='clearRow'></div>";
		echo 					"</div>";
		echo 					"<div class='clearRow'></div>";
		$sql 						= "SELECT lk_service.datetime AS datetime,lk_service.part_cost AS part_cost,lk_service.notes AS notes,lk_service.part_name AS part_name,lk_service.quantity AS quantity, lk_service.custom_part_id AS part_id";
		$sql					   .= " FROM lk_service WHERE lk_service.kart_id='$kart_id' AND LEFT(datetime,10)>='$start_date' AND LEFT(datetime,10)<='$end_date' ORDER BY datetime DESC";
		//echo "sql: $sql kart_number: $kart_number<br>";
		$report_info			= lavu_query($sql);
		while ($report_row = mysqli_fetch_assoc($report_info)){
				$datetime 			= formatDate($report_row['datetime']);
				$cost_per_unit		= $report_row['part_cost'];
				$part_name			= $report_row['part_name'];
				$quantity				= $report_row['quantity'];
				$total_cost			= $cost_per_unit*1 * $quantity*1;
				$part_id				= $report_row['part_id'];
				echo 					"<div class='row hidden_info'>";
				echo 					"<div class='rowElement re1'>$datetime &nbsp;</div>";
				echo 					"<div class='rowElement re2'>$cost_per_unit &nbsp;</div>";
				echo 					"<div class='rowElement re3'>$part_name &nbsp;</div>";
				echo 					"<div class='rowElement re4'>$part_id &nbsp;</div>";
				echo 					"<div class='rowElement re5'>$quantity &nbsp;</div>";
				echo 					"<div class='rowElement re6'>$total_cost &nbsp;</div>";
				echo 					"<div class='clearRow'></div>";
				echo 					"</div>";
				echo 					"<div class='clearRow'></div>";
		}//while
		echo  							"</div>";
} //while
//echo "</select>";
//echo "<br/><br/>";

?>
<script type='text/javascript'>
document.getElementById("select_part").value = '<?php echo $selected_part;?>';
</script>

<?php
function formatDate($datetime){
		
		$datetime_arr 		= explode(" ",$datetime);
		$date_str 			= $datetime_arr[0];
		$time_str 	 		= $datetime_arr[1];
		
		$date_arr				= explode("-",$date_str);
		$year					= $date_arr[0];
		
		$month				= $date_arr[1];
		$day						= $date_arr[2];
		$formated_date 	= $month."-".$day."-".$year." ".$time_str;
		
		return $formated_date;
}//formatDate()

function selectDate($label,$id,$set_to_this_date){
				
				$set_to_date_arr = explode("-",$set_to_this_date);
				$set_year = $set_to_date_arr[0];
				$set_month = $set_to_date_arr[1];
				$set_day = $set_to_date_arr[2];
				
				//$startVal = reqvar("start_date_text_field");
				//$start_painted = reqvar("paint_start_date_id");
				$month_id = $id."_month";
				$day_id = $id."_day";
				$year_id = $id."_year";
				
				echo "<div class='leftForm'><label>$label</label></div><!--leftForm-->";
				echo "<div class='rightForm'><select name='$month_id' id='$month_id'><option value=''> </option>";
			        	
				for ($i=1; $i<=12; $i++){
					echo "<option value='".pad($i,2)."'> $i </option>";
				}//for
		
				echo "</select>";
				echo "<select name='$day_id' id='$day_id'>";
	        	echo "<option value=''> </option>";
        	
				for ($i=1; $i<=31; $i++){
					echo "<option value='".pad($i,2)."'> $i </option>";
				}//for
				
				echo "</select>";
				echo "<select name='$year_id' id='$year_id'>";
				echo "<option value=''> </option>";
		
				for ($i=2011; $i<=2013; $i++){
					if (reqvar("startYear") == $i){
						echo "<option value=\"$i\" selected=\"selected\"> $i </option>";
					}//if
					
					if (reqvar("startYear") != $i){
						echo "<option value=\"$i\"> $i </option>";
					}//if
				}//for
				
				echo "</select>";
				echo "</div><!--rightForm-->";
				echo "<div class='breakLine'></div>";
				
				echo "<script type='text/javascript'>";
				//echo "alert('$set_month');";
				
				echo "document.getElementById('$month_id').value = '$set_month';";
				echo "document.getElementById('$day_id').value = '$set_day';";
				echo "document.getElementById('$year_id').value = '$set_year';";
				echo "</script>";

}//selectDate

function pad($str,$length){
		while (strlen($str)<$length){
				$str = "0".$str;
		}//while
		return $str;
}//pad

?>