<?php


function displayTrackData($assoc_array){
		//$avg_race_per_cust = "42";
		//$race_sold_cur_week = $assoc_1
		
		/* arpc is for average race per customer */
		$arpc_cur_week = number_format($assoc_array["current_week"]["avg_race_per_cust"],2);
		$races_sold_cur_week = $assoc_array["current_week"]["num_races"];
		$num_cust_cur_week = $assoc_array["current_week"]["num_customers"];
		$num_new_cust_cur_week = $assoc_array["current_week"]["num_new_customers"];
		
		$arpc_prior_week = number_format($assoc_array["prior_week"]["avg_race_per_cust"],2);
		$races_sold_prior_week = $assoc_array["prior_week"]["num_races"];
		$num_cust_prior_week = $assoc_array["prior_week"]["num_customers"];
		$num_new_cust_prior_week = $assoc_array["prior_week"]["num_new_customers"];
		
		$arpc_prior_year = number_format($assoc_array["prior_year"]["avg_race_per_cust"],2);
		$races_sold_prior_year = $assoc_array["prior_year"]["num_races"];
		$num_cust_prior_year = $assoc_array["prior_year"]["num_customers"];
		$num_new_cust_prior_year = $assoc_array["prior_year"]["num_new_customers"];
		echo "
				<div class='row'>
							<div class='rowElement re0'>
								Avg Race per Customer
							</div>
							<div class='rowElement re1'>
								$arpc_cur_week
							</div>
							<div class='rowElement re2'>
								$arpc_prior_week
							</div>
							<div class='rowElement re3'>
								$arpc_prior_year 
							</div>
							<div class='rowElement re4'>
								&nbsp;
							</div>
							<div class='rowElement re5'>
								&nbsp
							</div>
					</div><!--row-->
				<div class='clearRow'></div>
		";
		echo "
				<div class='row'>
							<div class='rowElement re0'>
								Races Sold
							</div>
							<div class='rowElement re1'>
								$races_sold_cur_week
							</div>
							<div class='rowElement re2'>
								$races_sold_prior_week
							</div>
							<div class='rowElement re3'>
								$races_sold_prior_year 
							</div>
							<div class='rowElement re4'>
								&nbsp;
							</div>
							<div class='rowElement re5'>
								&nbsp
							</div>
					</div><!--row-->
				<div class='clearRow'></div>
		";
		echo "
				<div class='row'>
							<div class='rowElement re0'>
								Customer Totals
							</div>
							<div class='rowElement re1'>
								$num_cust_cur_week
							</div>
							<div class='rowElement re2'>
								$num_cust_prior_week
							</div>
							<div class='rowElement re3'>
								$num_cust_prior_year
							</div>
							<div class='rowElement re4'>
								&nbsp;
							</div>
							<div class='rowElement re5'>
								&nbsp
							</div>
					</div><!--row-->
				<div class='clearRow'></div>
		";
		echo "
				<div class='row'>
							<div class='rowElement re0'>
								Customer New
							</div>
							<div class='rowElement re1'>
								$num_new_cust_cur_week
							</div>
							<div class='rowElement re2'>
								$num_new_cust_prior_week
							</div>
							<div class='rowElement re3'>
								$num_new_cust_prior_year
							</div>
							<div class='rowElement re4'>
								&nbsp;
							</div>
							<div class='rowElement re5'>
								&nbsp
							</div>
					</div><!--row-->
				<div class='clearRow'></div>
		";
		/* new customer percentage */
		$cur_week = number_format(calcPercentage($num_new_cust_cur_week,$num_cust_cur_week),2)."&#37;";	
		$prior_week = number_format(calcPercentage($num_new_cust_prior_week,$num_cust_prior_week),2)."&#37;";	
		$prior_year = number_format(calcPercentage($num_new_cust_prior_year,$num_cust_prior_year),2)."&#37;";	
		echo "
				<div class='row'>
							<div class='rowElement re0'>
								Customer New &#37;
							</div>
							<div class='rowElement re1'>
								$cur_week
							</div>
							<div class='rowElement re2'>
								$prior_week
							</div>
							<div class='rowElement re3'>
								$prior_year
							</div>
							<div class='rowElement re4'>
								&nbsp;
							</div>
							<div class='rowElement re5'>
								&nbsp
							</div>
					</div><!--row-->
				<div class='clearRow'></div>
		";



}//displayTrackData()


function calcTrackData($assoc_1,$assoc_2,$assoc_3,$date,$menu_category_id){
		$date_arr = explode("-",$date);
		$year = $date_arr[0];
		$month = $date_arr[1];
		$day = $date_arr[2];
		$total_of_weekly_totals = 0;
		$totals = array();
		
		$cur_week_date = date("Y-m-d",mktime(0,0,0,$month*1,$day*1+1,$year*1));
		$prior_week_date = date("Y-m-d",mktime(0,0,0,$month*1,$day*1+1,$year*1));
		$prior_year_date = date("Y-m-d",mktime(0,0,0,$month*1,$day*1+1,$year*1));
		
		$cur_week = 0;//total races ordered by everyone
		$prior_week = 0;//total races ordered by everyone
		$prior_year = 0;//total races ordered by everyone
		
		/* The length of these arrays represent number of orders that contain races */
		$assoc_1_length = count($assoc_1["list_of_order_ids"]);
		$assoc_2_length = count($assoc_2["list_of_order_ids"]);
		$assoc_3_length = count($assoc_3["list_of_order_ids"]);
		
		
		for ($i=1;$i<$assoc_1_length;$i++){
				/* How many orders containing races for the current order_id*/
				$cur_week = $cur_week*1 + $assoc_1["num_race_orders"][$assoc_1["list_of_order_ids"][$i]]*1;				
		}//for
		for ($i=1;$i<$assoc_2_length;$i++){
				/* How many orders containing races for the current order_id*/
				$prior_week = $prior_week*1 + $assoc_2["num_race_orders"][$assoc_2["list_of_order_ids"][$i]]*1;				
		}//for
		for ($i=1;$i<$assoc_3_length;$i++){
				/* How many orders containing races for the current order_id*/
				$prior_year = $prior_year*1 + $assoc_3["num_race_orders"][$assoc_3["list_of_order_ids"][$i]]*1;				
		}//for
		
		$output["current_week"]["num_new_customers"] = $assoc_1["num_new_customers"];
		$output["current_week"]["num_races"] = $cur_week;
		$output["current_week"]["num_customers"] = $assoc_1_length;
		$output["current_week"]["avg_race_per_cust"] = ($cur_week*1)/($assoc_1_length*1);
		
		$output["prior_week"]["num_new_customers"] = $assoc_2["num_new_customers"];
		$output["prior_week"]["num_races"] = $prior_week;
		$output["prior_week"]["num_customers"] = $assoc_2_length;
		$output["prior_week"]["avg_race_per_cust"] = ($prior_week*1)/($assoc_2_length*1);
		
		$output["prior_year"]["num_new_customers"] = $assoc_3["num_new_customers"];
		$output["prior_year"]["num_races"] = $prior_year;
		$output["prior_year"]["num_customers"] = $assoc_3_length;
		$output["prior_year"]["avg_race_per_cust"] = ($prior_year*1)/($assoc_3_length*1);
		
		return $output;
}//function calcTrackData()


function displayEventDept($assoc_array_1,$assoc_array_2,$assoc_array_3,$event_department,$cur_week,$prior_week,$prior_year){
		
		$sql_query = "select SUM(IF(`cc_transactions`.`action`='Void',0,IF(`cc_transactions`.`voided`='1',0,IF(`cc_transactions`.`action`='Refund', (0 - (`cc_transactions`.`total_collected`)), `cc_transactions`.`total_collected`)))) AS prepayments_this_week from `cc_transactions` LEFT JOIN `orders` ON `cc_transactions`.`order_id` = `orders`.`order_id`where (LEFT(`cc_transactions`.`datetime`,10) < LEFT(`orders`.`closed`,10) or LEFT(`orders`.`closed`,10)='0000-00-00') and LEFT(`cc_transactions`.`datetime`,10) >= '$cur_week' and LEFT(`cc_transactions`.`datetime`,10) <= DATE_ADD('$cur_week',INTERVAL 172 HOUR) and `cc_transactions`.`loc_id` = `orders`.`location_id` and `cc_transactions`.`voided` != '1'and `orders`.`void` = 0";
		$prepayment_info = lavu_query($sql_query);
		$prepayment_row = mysqli_fetch_assoc($prepayment_info);
		$cur_week_prepayment = $prepayment_row['prepayments_this_week'];

		$sql_query = "select SUM(IF(`cc_transactions`.`action`='Void',0,IF(`cc_transactions`.`voided`='1',0,IF(`cc_transactions`.`action`='Refund', (0 - (`cc_transactions`.`total_collected`)), `cc_transactions`.`total_collected`)))) AS prepayments_this_week from `cc_transactions` LEFT JOIN `orders` ON `cc_transactions`.`order_id` = `orders`.`order_id`where (LEFT(`cc_transactions`.`datetime`,10) < LEFT(`orders`.`closed`,10) or LEFT(`orders`.`closed`,10)='0000-00-00') and LEFT(`cc_transactions`.`datetime`,10) >= '$prior_week' and LEFT(`cc_transactions`.`datetime`,10) <= DATE_ADD('$prior_week',INTERVAL 172 HOUR) and `cc_transactions`.`loc_id` = `orders`.`location_id` and `cc_transactions`.`voided` != '1'and `orders`.`void` = 0";
		$prepayment_info = lavu_query($sql_query);
		$prepayment_row = mysqli_fetch_assoc($prepayment_info);
		$prior_week_prepayment = $prepayment_row['prepayments_this_week'];
		
		$sql_query = "select SUM(IF(`cc_transactions`.`action`='Void',0,IF(`cc_transactions`.`voided`='1',0,IF(`cc_transactions`.`action`='Refund', (0 - (`cc_transactions`.`total_collected`)), `cc_transactions`.`total_collected`)))) AS prepayments_this_week from `cc_transactions` LEFT JOIN `orders` ON `cc_transactions`.`order_id` = `orders`.`order_id`where (LEFT(`cc_transactions`.`datetime`,10) < LEFT(`orders`.`closed`,10) or LEFT(`orders`.`closed`,10)='0000-00-00') and LEFT(`cc_transactions`.`datetime`,10) >= '$prior_year' and LEFT(`cc_transactions`.`datetime`,10) <= DATE_ADD('$prior_year',INTERVAL 172 HOUR) and `cc_transactions`.`loc_id` = `orders`.`location_id` and `cc_transactions`.`voided` != '1'and `orders`.`void` = 0";
		$prepayment_info = lavu_query($sql_query);
		$prepayment_row = mysqli_fetch_assoc($prepayment_info);
		$prior_year_prepayment = $prepayment_row['prepayments_this_week'];

		
		echo "
		<div class='row'>
			<div class='row'>
					<div class='rowElement re0'>
						Prepayments Taken
					</div>
					<div class='rowElement re1'>
						$cur_week_prepayment
					</div>
					<div class='rowElement re2'>
						$prior_week_prepayment 
					</div>
					<div class='rowElement re3'>
						$prior_year_prepayment 
					</div>
					<div class='rowElement re4'>
						&nbsp;
					</div>
					<div class='rowElement re5'>
						&nbsp
					</div>
			</div><!--row-->
		<div class='clearRow'></div>
		";

		
		$cur_week_total = $assoc_array_1[$event_department];
		$prior_week_total = $assoc_array_2[$event_department];
		$prior_year_total = $assoc_array_3[$event_department];
		$menu_category_name = $assoc_array_1["menu_category_map"][$event_department];
		echo "
		<div class='row'>
			<div class='row'>
					<div class='rowElement re0'>
						$menu_category_name
					</div>
					<div class='rowElement re1'>
						$cur_week_total 
					</div>
					<div class='rowElement re2'>
						$prior_week_total
					</div>
					<div class='rowElement re3'>
						$prior_year_total
					</div>
					<div class='rowElement re4'>
						&nbsp; 
					</div>
					<div class='rowElement re5'>
						&nbsp
					</div>
			</div><!--row-->
		<div class='clearRow'></div>
		";
		
		
}//displayEventDept()


function displayCSREfficiency($hours_assoc,$date,$role_id){
		//echo "role_id: $role_id<br>";
		$date_arr = explode("-",$date);
		$year = $date_arr[0];
		$month = $date_arr[1];
		$day = $date_arr[2];
		$total_of_weekly_totals = 0;
		$totals = array();
		for ($i=0;$i<7;$i++){
					$key_date = date("Y-m-d",mktime(0,0,0,$month*1,$day*1+1*$i,$year*1));
					
					$hours_worked = $hours_assoc[$key_date][$role_id]["hours"];
					$day_name = $hours_assoc[$key_date]["day_name"];
					
					$sql_query = "SELECT COUNT(id) FROM orders WHERE LEFT(opened,10)>='$key_date' AND LEFT(opened,10)<DATE_ADD('$key_date',INTERVAL 29 HOUR)";
					$num_tickets_res = lavu_query($sql_query);
					$num_tickets_row = mysqli_fetch_assoc($num_tickets_res);
					$num_tickets = $num_tickets_row['COUNT(id)'];
					$tickets_per_hr = (($num_tickets*1)/($hours_worked*1));
					$totals["tickets"] = $totals["tickets"]*1 + $num_tickets*1;
					$totals["total_hours"] = $totals["total_hours"]*1 + $hours_worked*1;
					$totals["tickets_per_hr"] = $totals["tickets_per_hr"]*1 + $tickets_per_hr*1;
					$tickets_per_hr = number_format($tickets_per_hr,2);
					$hours_worked = number_format($hours_worked,2);//now it's pretty
					echo "
							<div class='row'>
								<div class='row'>
										<div class='rowElement re0'>
											$day_name
										</div>
										<div class='rowElement re1'>
											$num_tickets
										</div>
										<div class='rowElement re2'>
											$hours_worked
										</div>
										<div class='rowElement re3'>
											$tickets_per_hr 
										</div>
										<div class='rowElement re4'>
											&nbsp;
										</div>
										<div class='rowElement re5'>
											&nbsp;
										</div>
										<div class='rowElement re6'>
											&nbsp;
										</div>
										<div class='rowElement re7'>
											&nbsp;
										</div>
										<div class='rowElement re9'>
											<span id='csr_effic_".$i."'></span>
										</div>
								</div><!--row-->
								<div class='clearRow'></div>
								
					";
			}//for
			$total_tickets = number_format($totals["tickets"],2);
			$total_hrs = number_format($totals["total_hours"],2);
			$total_tickets_per_hr = number_format($totals["tickets_per_hr"],2);
					echo "
								<div class='row header'>
										<div class='rowElement re0'>
											Total
										</div>
										<div class='rowElement re1'>
											$total_tickets
										</div>
										<div class='rowElement re2'>
											$total_hrs
										</div>
										<div class='rowElement re3'>
											$total_tickets_per_hr 
										</div>
										<div class='rowElement re4'>
											&nbsp;
										</div>
										<div class='rowElement re5'>
											&nbsp;
										</div>
										<div class='rowElement re6'>
											&nbsp;
										</div>
										<div class='rowElement re7'>
											&nbsp;
										</div>
										<div class='rowElement re9'>
											&nbsp;
										</div>
								</div><!--row-->
								<div class='clearRow'></div>
								
					";
					return $total_hrs;//you'll have to use a javascript to put this in the right spot
}//displayCSREfficiency()


function displayLaborUsage($assoc_array,$date,$track_efficiency){
		$css_index = 1;
		
		echo "
		<div class='row header'>
							<div class='rowElement re0'>
								Labor Usage
							</div>
		";

		foreach ($assoc_array AS $key => $value){
				$day_name = $assoc_array[$key]["day_name"];
												echo "
							<div class='rowElement re".$css_index."'>
								$day_name
							</div>
				";
				$css_index++;
		}//foreach
		echo "				
				<div class='rowElement re9'>
						Totals
				</div>

				</div><!--row-->
				<div class='clearRow'></div>	
		";
		
		/* We will loop throught the assoc array by creating the proper keys, which are the dates of the chosen week */
		$date_arr = explode("-",$date);
		$year = $date_arr[0];
		$month = $date_arr[1];
		$day = $date_arr[2];
		
		for ($i=1;$i<=count($assoc_array[$date]["role_title"]);$i++){//$i is for the role id
				
				$key_date_0 = date("Y-m-d",mktime(0,0,0,$month*1,$day*1+0*1,$year*1));
				$key_date_1 = date("Y-m-d",mktime(0,0,0,$month*1,$day*1+1*1,$year*1));
				$key_date_2 = date("Y-m-d",mktime(0,0,0,$month*1,$day*1+2*1,$year*1));
				$key_date_3 = date("Y-m-d",mktime(0,0,0,$month*1,$day*1+3*1,$year*1));
				$key_date_4 = date("Y-m-d",mktime(0,0,0,$month*1,$day*1+4*1,$year*1));
				$key_date_5 = date("Y-m-d",mktime(0,0,0,$month*1,$day*1+5*1,$year*1));
				$key_date_6 = date("Y-m-d",mktime(0,0,0,$month*1,$day*1+6*1,$year*1));
				
				$hours_0 = $assoc_array[$key_date_0][$i]["hours"];
				$hours_1 = $assoc_array[$key_date_1][$i]["hours"];
				$hours_2 = $assoc_array[$key_date_2][$i]["hours"];
				$hours_3 = $assoc_array[$key_date_3][$i]["hours"];
				$hours_4 = $assoc_array[$key_date_4][$i]["hours"];
				$hours_5 = $assoc_array[$key_date_5][$i]["hours"];
				$hours_6 = $assoc_array[$key_date_6][$i]["hours"];
				 
				 $weekly_total = number_format($assoc_array["hi"][$i]["sum_hours"],2);
				 $total_of_weekly_totals += $weekly_total*1;
				 
				//$weekly_total = $output[$i]["sum_hours"]; 
				$role_title = $assoc_array[$key_date_0]["role_title"][$i];
				
				/* If you need these numbers for calculations, use them before they are formatted */
				$hours_0 = number_format($hours_0,2);
				$hours_1 = number_format($hours_1,2);
				$hours_2 = number_format($hours_2,2);
				$hours_3 = number_format($hours_3,2);
				$hours_4 = number_format($hours_4,2);
				$hours_5 = number_format($hours_5,2);
				$hours_6 = number_format($hours_6,2);
				
				if ($i*1 == $track_efficiency*1){
						$weekly_total_track_hours = $hours_0*1+$hours_1*1+$hours_2*1+$hours_3*1+$hours_4*1+$hours_5*1+$hours_6*1;
				}//if
				
				echo "
				<div class='row'>
					<div class='row'>
							<div class='rowElement re0'>
								$role_title
							</div>
							<div class='rowElement re1'>
								$hours_0
							</div>
							<div class='rowElement re2'>
								$hours_1
							</div>
							<div class='rowElement re3'>
								$hours_2
							</div>
							<div class='rowElement re4'>
								$hours_3
							</div>
							<div class='rowElement re5'>
								$hours_4
							</div>
							<div class='rowElement re6'>
								$hours_5
							</div>
							<div class='rowElement re7'>
								$hours_6
							</div>
							<div class='rowElement re9'>
								$weekly_total
							</div>
					</div><!--row-->
					<div class='clearRow'></div>
					
				";

		}//foreach
		
		$weekly_total_of_all_categories = number_format($assoc_array["total_labor_hours"],2);
		echo "
				
					<div class='row header'>
							<div class='rowElement re0'>
								&nbsp;
							</div>
							<div class='rowElement re1'>
								&nbsp;
							</div>
							<div class='rowElement re2'>
								&nbsp;
							</div>
							<div class='rowElement re3'>
								&nbsp;
							</div>
							<div class='rowElement re4'>
								&nbsp;
							</div>
							<div class='rowElement re5'>
								&nbsp;
							</div>
							<div class='rowElement re6'>
								&nbsp;
							</div>
							<div class='rowElement re7'>
								Total
							</div>
							<div class='rowElement re9'>
								$weekly_total_of_all_categories
							</div>
					</div><!--row-->
					<div class='clearRow'></div>
					
				";

				return $weekly_total_track_hours;
}//displayLaborUsage()

function calLaborUsage($date){
		$date_arr = explode("-",$date);
		$year = $date_arr[0];
		$month = $date_arr[1];
		$day = $date_arr[2];
		$output = array();//everything leaving this function goes in here
		$roles_ids_arr = array();//so you know which role_ids to call
		//$index = 0;
		
		for ($index=0; $index<7;$index++){
				$cur_date = date("Y-m-d",mktime(0,0,0,$month*1,$day*1+$index*1,$year*1)); 
				$sql_query = "SELECT SUM(clock_punches.hours) AS total_hours, users.role_id AS role_id, roles.title AS title FROM clock_punches LEFT JOIN users ON users.id=clock_punches.server_id LEFT JOIN roles ON users.role_id=roles.id WHERE LEFT(clock_punches.time,10)='$cur_date' AND clock_punches.time_out!='' AND clock_punches._deleted!='1' GROUP BY users.role_id";
				$get_labor_info = lavu_query($sql_query);
				 
				while($get_labor_row = mysqli_fetch_assoc($get_labor_info)){
						$role_id = $get_labor_row['role_id'];
						
						$output[$cur_date]["day_name"] = getDayNameFromDate($cur_date);
						$output[$cur_date][$role_id]["hours"] = $get_labor_row['total_hours'];
						$output["total_labor_hours"] = $output["total_labor_hours"]*1 + $get_labor_row['total_hours']*1;//the total for all categories of employees
						$output[$cur_date]["role_title"] [$role_id]= $get_labor_row['title'];
						$output["hi"][$role_id]["sum_hours"] = $output["hi"][$role_id]["sum_hours"]*1 +  $get_labor_row['total_hours']*1;//the total for that category of employee
				}//while
		}//for
		return $output;
}//calLaborUsage()

function displayTrackEfficiencyData($assoc_array){
		$total = array();
		$trk_hours = "";
		$display_trk_hours = 0;//we add the value (how many hrs the track guys worked) to the <span id='trk_hours_0'></span> field */
		foreach($assoc_array as $key=> $value){
				$day_name = getDayNameFromDate($assoc_array[$key]["day_name"]);
				$efficiency = number_format($assoc_array[$key]["efficiency"],2);
				
				$real_time = number_format(($assoc_array[$key]["real_time_seconds"]*1/60),2);//convert to minutes
				$total["total_real_time"] = $total["total_real_time"]*1 + $assoc_array[$key]["real_time_seconds"]*1/60;
				
				$open_time = number_format(($assoc_array[$key]["open_time_seconds"]*1/60),2);//convert to minutes
				$total["total_open_time"] = $total["total_open_time"]*1 +$assoc_array[$key]["open_time_seconds"]*1/60;
				
				$races_sold = $assoc_array[$key]["num_races"];
				$total["races_sold"] = $total["races_sold"]*1 + $assoc_array[$key]["num_races"]*1;
				$num_races_sold = $assoc_array[$key]["num_races_sold"];
				$total["races"] = $total["races"]*1 +  $assoc_array[$key]["num_races_sold"]*1;
				$real_time_per_race = number_format($assoc_array[$key]["real_time_per_race"],2);///in minutes
				$total["real_time"] = $total["real_time"]*1 + ($assoc_array[$key]["real_time_seconds"]*1/60);//minutes
				$total["open_time"] = $total["open_time"]*1 + ($assoc_array[$key]["open_time_seconds"]*1/60);//minutes
				
				
				echo "
				<div class='row'>
					<div class='row'>
							<div class='rowElement re0'>
								$day_name
							</div>
							<div class='rowElement re1'>
								$real_time
							</div>
							<div class='rowElement re2'>
								$open_time
							</div>
							<div class='rowElement re3'>
								$efficiency &#37;
							</div>
							<div class='rowElement re4'>
								$num_races_sold
							</div>
							<div class='rowElement re5'>
								$races_sold
							</div>
							<div class='rowElement re6'>
								$real_time_per_race 
							</div>
							<div class='rowElement re7'>
								&nbsp;
							</div>
							<div class='rowElement re9'>
								<span id='trk_hours_".$display_trk_hours."'></span>
							</div>
					</div><!--row-->
					<div class='clearRow'></div>
					";
					$display_trk_hours++;
		}//foreach
		
		/* Evaluate totals */
		$real_time = $total["real_time"];
		$open_time = $total["open_time"];
		$efficiency = number_format((100*($total["real_time"]*1)/($total["open_time"]*1)),2);
		$races_sold = $total["races"];
		$races = $total["races_sold"];
		echo "
				<div class='row'>
					<div class='row header'>
							<div class='rowElement re0'>
								&nbsp;
							</div>
							<div class='rowElement re1'>
								$real_time 
							</div>
							<div class='rowElement re2'>
								$open_time
							</div>
							<div class='rowElement re3'>
								$efficiency &#37;
							</div>
							<div class='rowElement re4'>
								$races_sold
							</div>
							<div class='rowElement re5'>
								$races
							</div>
							<div class='rowElement re6'>
								$real_time_per_race 
							</div>
							<div class='rowElement re7'>
								&nbsp;
							</div>
							<div class='rowElement re9'>
								&nbsp;
							</div>
					</div><!--row-->
					<div class='clearRow'></div>
					";

		
}//displayTrackData()

function calcTrackEfficiency($date){
		$date_arr = explode("-",$date);
		$year = $date_arr[0];
		$month = $date_arr[1];
		$day = $date_arr[2];
		$output = array();//everything leaving this function goes in here
		$index = 0;
		while ($index <= 6){
				$cur_date = date("Y-m-d",mktime(0,0,0,$month*1,$day*1+$index*1,$year*1)); 
				$sql_query = "SELECT lk_events.ms_start AS ms_start, lk_events.ms_end AS ms_end, SUM(TIMESTAMPDIFF(SECOND,lk_events.ms_start,lk_events.ms_end)) AS time_diff_seconds, lk_tracks.time_start AS open_time_start, lk_tracks.time_end AS open_time_end, COUNT(lk_events.id) AS num_races FROM lk_events LEFT JOIN lk_tracks ON lk_events.locationid=lk_tracks.locationid WHERE LEFT(lk_events.date,10)='$cur_date' AND lk_events._deleted!='1' AND lk_events.ms_start!='' AND lk_events.ms_end!='' ";
				$get_track_res = lavu_query($sql_query);
				$get_track_row = mysqli_fetch_assoc($get_track_res);
				$output[$cur_date]["day_name"] = getDayNameFromDate($cur_date);
				$output[$cur_date]["real_time_seconds"] = $get_track_row['time_diff_seconds'];
				$output[$cur_date]["num_races"] = $get_track_row['num_races'];
				/* Now find out how much time they were open */
				$time_business_opened = $get_track_row['open_time_start'];
				$open_time_hrs = $time_business_opened{0}.$time_business_opened{1};
				$open_time_min = $time_business_opened{2}.$time_business_opened{3};
				$open_time_date = strtotime($cur_date." ".$open_time_hrs.":".$open_time_min.":00");
				$time_business_closed = $get_track_row['open_time_end'];
				$close_time_hrs = $time_business_closed{0}.$time_business_closed{1};
				$close_time_min = $time_business_closed{2}.$time_business_closed{3};
				$close_time_date = strtotime($cur_date." ".$close_time_hrs.":".$close_time_min.":00");
				$output[$cur_date]["open_time_seconds"] = $close_time_date*1 - $open_time_date*1;//how many seconds were they open that day
				$output[$cur_date]["efficiency"] = 100*(($output[$cur_date]["real_time_seconds"]*1)/($output[$cur_date]["open_time_seconds"]*1));
				
				$sql_query = "SELECT id FROM lk_events WHERE date='$cur_date' ";
				$get_races_sold = lavu_query($sql_query);
				$str = "SELECT COUNT(id) FROM lk_event_schedules WHERE eventid='something that doesnt exisit'";
			    while($get_races_row = mysqli_fetch_assoc($get_races_sold)){
						$str .= " OR eventid='".$get_races_row['id']."' ";
				}//while
				$str .= " AND _deleted!='1' ";
				//echo $str;
				$num_races_sold = lavu_query($str);
				$output["sql_str"] = $str;
				$num_races_row = mysqli_fetch_assoc($num_races_sold);
				$num_races = $num_races_row['COUNT(id)'];
				$output[$cur_date]["num_races_sold"] = $num_races;
				$output[$cur_date]["real_time_per_race"] = ($output[$cur_date]["real_time_seconds"]/60)/($output[$cur_date]["num_races_sold"]*1);
				$index++;
		}//while
		return $output;
}//calcTrackEfficiency(

function getDayNameFromDate($date){
		/* Take in date in YYYY-mm-dd format and return the day name of the week (saturday, sunday,etc...)*/
		$date_info = getdate(strtotime($date));
		$dayName = $date_info["weekday"];
		return $dayName;
}//getDayNameFromDate()


function displaySalesByCategoryInfo($current_week_revenue,$prior_week_revenue,$prior_year_revenue,$menu_category_id,$menu_category_name){
		/* Just put stuff on the screen and do a few other calculations */
		$cur_week_total = $current_week_revenue[$menu_category_id];
		$prior_week_total = $prior_week_revenue[$menu_category_id];
		$prior_year_total = $prior_year_revenue[$menu_category_id];
		$trend = number_format(trend($cur_week_total,$prior_week_total),2);
		$prior_year_trend = number_format(trend($cur_week_total,$prior_year_total),2);
		echo "
					<div class='row'>		
							<div class='rowElement re0'>
								$menu_category_name
							</div>
							<div class='rowElement re1'>
								$cur_week_total
							</div>
							<div class='rowElement re2'>
								$prior_week_total
							</div>
							<div class='rowElement re3'>
								$prior_year_total
							</div>
							<div class='rowElement re4'>
								&nbsp;
							</div>
							<div class='rowElement re6'>
								$trend&#37;
							</div>
							<div class='rowElement re7'>
								$prior_year_trend&#37;
							</div>
							<div class='rowElement re8'>
								&nbsp;
							</div>
					</div><!--row-->
					<div class='clearRow'></div>
		";
		
		
		
}//displaySalesByCategoryInfo();

function trend($current_val,$prior_val){
		$trend_val = 100*( ( ($current_val*1)/($prior_val*1) ) -1);
		return $trend_val;
}//trend()

function salesByCategory($date,$menu_category_id){
					
					$output = array();//an associative that stores all values calculated in this function
					
					/* Just getting the people who just signed up AND had last activity within the specied date range */
					$sql_query = "SELECT COUNT(id) AS num_new_customers FROM lk_customers WHERE LEFT(date_created,10)>='$date' AND LEFT(date_created,10)<DATE_ADD('$date',INTERVAL 172 HOUR) AND LEFT(last_activity,10)>='$date' AND LEFT(last_activity,10)>=DATE_ADD('$date',INTERVAL 172 HOUR)";
					$num_new_cust_results = lavu_query($sql_query);
					$num_new_cust_row = mysqli_fetch_assoc($num_new_cust_results);
					$output["num_new_customers"] = $num_new_cust_row['num_new_customers'];
					
								
					$sql_query = "SELECT order_id FROM orders WHERE orders.closed>='$date' AND orders.closed<DATE_ADD('$date',INTERVAL 172 HOUR)";//one week
					
					$order_ids = lavu_query($sql_query);
					$output["num_customers"] = mysqli_num_rows($order_ids);//considering everything they sell
					 
					$order_ids_array = array();
					$index = 0;
					$str = "";
					while ($order_ids_row = mysqli_fetch_assoc($order_ids)){
							//$order_ids_array[$index] = $order_ids_row['order_id'];
							$str .= "OR order_contents.order_id='".$order_ids_row['order_id']."' ";
							//$index++;
					}//while
					$sql_query = "SELECT order_contents.item_id AS item_id, menu_items.category_id AS category_id, order_contents.subtotal_with_mods AS subtotal_with_mods, menu_categories.name AS menu_category_name, order_contents.order_id AS order_id FROM order_contents LEFT JOIN menu_items ON order_contents.item_id=menu_items.id LEFT JOIN menu_categories ON menu_items.category_id=menu_categories.id WHERE order_contents.order_id='something_that_doesnt_exist' $str";
					$output["sql"] = $sql_query;
					/* How many of these customers are members? */
					//$sql_query_2 = 
					
					//echo $sql_query;
					$order_contents_info = lavu_query($sql_query);
					$menu_category_map = array();
					$oid_index = 0;
					$output["num_race_orders"][$order_id] = 0;//how many races were run, not events, but each person on the track counts as one race each time
					$output["list_of_order_ids"][$oid_index] = "";//we keep track of unique order ids, this is how many orders containing races were placed

					while ($row = mysqli_fetch_assoc($order_contents_info)){
							$cur_item_id = $row['item_id'];
							$cur_menu_category = $row['category_id'];
							$menu_category_name = $row['menu_category_name'];
							$order_id = $row['order_id'];
							$output[$cur_menu_category] = $output[$cur_menu_category]*1 + $row['subtotal_with_mods']*1;//should be the total revenue for this menu category
							$output["total_revenue"] = $output["total_revenue"]*1 + $row['subtotal_with_mods']*1;
														if (!in_array($menu_category_name,$output["menu_category_map"])){
									/* This if statement builds a key value map one can use to find the name of the menu category from the menu category id*/
									$output["menu_category_map"][$cur_menu_category ] = $menu_category_name;
							}//if 
							if ( ($cur_menu_category*1 == $menu_category_id*1) ){
									$output["num_race_orders"][$order_id] = $output["num_race_orders"][$order_id]*1 + 1;
									if (!in_array($order_id, $output["list_of_order_ids"])){
											$output["list_of_order_ids"][$oid_index] = $order_id;
											$oid_index++;
									}//if
							}//if
					}//while
				
					return $output;
}//salesByCategory()


function pad($str,$length){
		while (strlen($str)*1 < $length*1){
			$str = "0".$str;
		}
		return $str;
}//pad



function displayForm(){
		return;
}//displayForm()

function calcPercentage($num,$total){
		$percentage = 100*($num*1)/($total*1);
		return $percentage;
}//calcPercentage()

?>


<?php
/*
$index = 0;
		$cur_date = date("Y-m-d",mktime(0,0,0,$date_month*1,$date_day*1+$index,$date_year*1));
		echo "
				<div class='row'>
					<div class='row'>
							<div class='rowElement re0'>
								$day_name
							</div>
							<div class='rowElement re1'>
								$real_time
							</div>
							<div class='rowElement re2'>
								$open_time
							</div>
							<div class='rowElement re3'>
								$efficiency &#37;
							</div>
							<div class='rowElement re4'>
								$num_races_sold
							</div>
							<div class='rowElement re5'>
								$races_sold
							</div>
							<div class='rowElement re6'>
								$real_time_per_race 
							</div>
							<div class='rowElement re7'>
								&nbsp;
							</div>
							<div class='rowElement re9'>
								
							</div>
					</div><!--row-->
					<div class='clearRow'></div>
					
		";
*/
?>