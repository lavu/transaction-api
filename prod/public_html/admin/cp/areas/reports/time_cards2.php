<?php
	
	$a_user_totals = array();
	
	if($in_lavu)
	{
	
		require_once("/home/poslavu/public_html/admin/sa_cp/show_report.php");
		require_once(dirname(__FILE__)."/calOvertime.php");
		
		$location_info = sessvar('location_info');
		
		function getvar($str,$def=false){
			return (isset($_GET[$str]))?$_GET[$str]:$def;
		}
		
		function display_price($p){
			return "$".number_format($p,2,".",",");
		}
		
		function display_datetime($dt){
			if(trim($dt)=="") return "";
			$dt      = explode(" ",$dt);
			$date    = trim($dt[0]);
			$time    = ($dt[1]);
			$date    = explode("-",$date);
			$time    = explode(":",$time);
			$ts      = mktime($time[0],$time[1],$time[2],$date[1],$date[2],$date[0]);
			return date("m/d h:ia",$ts);
		}
	
		function optlist($options,$value){
			$str = "";
			for($i=0; $i<count($options); $i++){
				$option = $options[$i];
				$str .= "<option value='$option'";
				if($value==$option) $str .= " selected";
				$str .= ">$option</option>";
			}
			return $str;
		}
	
		function edit_display_datetime($name,$dt,$seturl="",$default_time=false){
			if(!$default_time) $default_time = date("Y-m-d H:i:s");
			if(!$dt || $dt=="") $dt = $default_time;
			
			$str         = "";
			$dt          = explode(" ",$dt);
			$date        = trim($dt[0]);
			$time        = ($dt[1]);
			$date        = explode("-",$date);
			$time        = explode(":",$time);
			$ts          = mktime($time[0],$time[1],$time[2],$date[1],$date[2],$date[0]);
			
			$date_name   = "ed_date_$name";
			$time_name   = "ed_time_$name";
			$ap_name     = "ed_ap_$name";
			
			$str .= "<select name='$date_name' id='$date_name'>";
			for($i=-365; $i<=10; $i++){
				$opt_ts = mktime((int)$time[0],(int)$time[1],(int)$time[2],(int)$date[1],(int)($date[2] + $i),(int)$date[0]);
				$str .= "<option value='".date("Y-m-d",$opt_ts)."'";
				if(date("Y-m-d",$ts)==date("Y-m-d",$opt_ts)) $str .= " selected";
				$str .= ">".date("m/d",$opt_ts)."</option>";
			}
			$str .= "</select>";
			$str .= "\n<input type='text' name='$time_name' id='$time_name' value='".date("h:i",$ts)."' size='5' maxlength='5' onblur='force_format(this, [ [/^0[1-9]|1[0-2]/, \"\"], [/:/, \":\"], [/[0-5][0-9]$/, \"\"] ]);'>";
			$str .= "<select name='$ap_name' id='$ap_name'>";
			$str .= optlist(array("am","pm"),date("a",$ts));
			$str .= "</select>";
			
			$str .= "<script type='text/javascript'>
				function force_format(element, a_regex) {
					var value = element.value + '';
					var newval = '';
					for (i = 0; i < a_regex.length; i++) {
						var regex = a_regex[i][0];
						var not_matched = a_regex[i][1];
						var match = value.match(regex);
						if (match !== null && match.length > 0) {
							match = match[0];
							newval += match;
							value = value.substring(match.length, value.length);
						} else {
							newval += not_matched;
						}
					}
					element.value = newval;
				}
				
				function check_format_times() {
					var clockin = document.getElementsByName('ed_time_clockin')[0];
					var clockout = document.getElementsByName('ed_time_clockout')[0];
					
					if (clockin.value.match(/(^0[1-9]|1[0-2])(:)([0-5][0-9]$)/) == null) {
						alert('Please provide a clock in time in the format \"hh:mm\" (eg \"".date("h:i")."\").');
						return false;
					}
					if (clockout.value.match(/(^0[1-9]|1[0-2])(:)([0-5][0-9]$)/) == null) {
						alert('Please provide a clock out time in the format \"hh:mm\" (eg \"".date("h:i")."\").');
						return false;
					}
					
					return true;
				}
			</script>";
			
			return $str;
			//return date("m/d h:ia",$ts);
		}
		
		/*function export_report($filename,$str){
			$known_mime_types=array(
				"pdf"   => "application/pdf",
				"txt"   => "text/plain",
				"html"  => "text/html",
				"htm"   => "text/html",
				"exe"   => "application/octet-stream",
				"zip"   => "application/zip",
				"doc"   => "application/msword",
				"xls"   => "application/vnd.ms-excel",
				"ppt"   => "application/vnd.ms-powerpoint",
				"gif"   => "image/gif",
				"png"   => "image/png",
				"jpeg"  => "image/jpg",
				"jpg"   => "image/jpg",
				"php"   => "text/plain"
			);
			$file_extension = strtolower(substr(strrchr($filename,"."),1));
			if ($file_extension == "xls"){
				$str = excel_format($str);
			}//if
			$mime_type = (isset($known_mime_types[$file_extension]))?$known_mime_types[$file_extension]:$file_extension;
			
			
			header('Content-Type: ' . $mime_type);
			header('Content-Disposition: attachment; filename="'.$filename.'"');
			header("Content-Transfer-Encoding: binary");
			header('Accept-Ranges: bytes');
			
			header("Cache-control: private");
			header('Pragma: private');
			header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
			
			header("Content-Length: ".strlen($str));
			echo $str;
		}*/
		
		function excel_format($str){
				//Take data that is tab delimited and format it for excel making text delimited by "", and new lines by \n 
				$new_line_explode = explode("\n",$str);
				$new_string = "";
				foreach($new_line_explode as $line){
						$tab_explode = explode("\t",$line);
						/*$new_string .= "\"".$tab_explode[0]."\",";
						$new_string .= "\"".$tab_explode[1]."\",";
						$new_string .= "\"".$tab_explode[2]."\",";
						$new_string .= "\"".$tab_explode[3]."\"";
						$new_string .= "\\n";
						*/
						$new_string .= "\t".$tab_explode[0];
						$new_string .= "\t".$tab_explode[1];
						$new_string .= "\t".$tab_explode[2];
						$new_string .= "\t".$tab_explode[3];
						$new_string .= "\n";
				}
				//echo $new_string;
				//exit();
				return $new_string;
		}//excel_format()
		
		function get_hours_difference($t1, $t2){
			//echo $t1 . "<br>" . $t2 . "<br>------------";
			$clockInArray    = explode(" ", $t1);
			$clockInDate     = explode("-", $clockInArray[0]);
			$clockInTime     = explode(":", $clockInArray[1]);
			$inStamp         = mktime($clockInTime[0], $clockInTime[1], 0, $clockInDate[1], $clockInDate[2], $clockInDate[0]);
	
			$clockOutArray   = explode(" ", $t2);
			$clockOutDate    = explode("-", $clockOutArray[0]);
			$clockOutTime    = explode(":", $clockOutArray[1]);
			$outStamp        = mktime($clockOutTime[0], $clockOutTime[1], 0, $clockOutDate[1], $clockOutDate[2], $clockOutDate[0]);
	
			$hours           = (($outStamp - $inStamp) / 3600);
			return $hours;
		}
		
		function convert_to_military_time($t,$ap){
			$tparts  = explode(":",$t);
			$hours   = $tparts[0] * 1;
			$mins    = $tparts[1] * 1;
			if($ap=="pm" && $hours < 12) $hours += 12;
			else if($ap=="am" && $hours==12) $hours = 0;
			if($mins < 10) $mins = "0" . $mins;
			if($hours < 10) $hours = "0" . $hours;
			//echo "<br>" . $hours . ":" . $mins . "<br><br>";
			return $hours . ":" . $mins;
		}
		
		function considerPunchForDate(&$ot_detail, $hours_considered){
			$ot_detail_reg_hours = 0;
			$ot_detail_ot_hours = 0;
			$ot_detail_dt_hours = 0;
			
			/**
			 * Added Rounding.  $hours_considered is already taking place a a percision
			 * level of 2.  That's bad for accuracy, but in order to accomidate it, I've
			 * make the OT considerations a Percision Level of 2 as well.
			 **/
			$ot_detail['reg_hours'] = round($ot_detail['reg_hours'],2);
			$ot_detail['ot_hours'] = round($ot_detail['ot_hours'],2);
			$ot_detail['dt_hours'] = round($ot_detail['dt_hours'],2);
		
			if(!isset($ot_detail['reg_hours_acc'])){
				$ot_detail['reg_hours_acc'] = 0;
			}
			
			if(!isset($ot_detail['ot_hours_acc'])){
				$ot_detail['ot_hours_acc'] = 0;
			}
			
			if(!isset($ot_detail['dt_hours_acc'])){
				$ot_detail['dt_hours_acc'] = 0;
			}
			
			if(!isset($ot_detail['tot_hours_acc'])){
				$ot_detail['tot_hours_acc'] = 0;
			}
			$ot_detail['tot_hours_acc'] += $hours_considered;
			if($ot_detail['reg_hours_acc'] != $ot_detail['reg_hours']){//Count Toward Regular Hours
				$ot_detail_reg_hours_diff = $ot_detail['reg_hours'] - $ot_detail['reg_hours_acc'];
				if($ot_detail_reg_hours_diff > $hours_considered){
					$ot_detail_reg_hours_diff = $hours_considered;
				}
				$ot_detail['reg_hours_acc'] += $ot_detail_reg_hours_diff;
				$ot_detail_reg_hours = $ot_detail_reg_hours_diff;
				$hours_considered -= $ot_detail_reg_hours_diff;
			}
			
			if($ot_detail['ot_hours_acc'] != $ot_detail['ot_hours']){//Count Toward OT Hours
				$ot_detail_ot_hours_diff = $ot_detail['ot_hours'] - $ot_detail['ot_hours_acc'];
				if($ot_detail_ot_hours_diff > $hours_considered){
					$ot_detail_ot_hours_diff = $hours_considered;
				}
				$ot_detail['ot_hours_acc'] += $ot_detail_ot_hours_diff;
				$ot_detail_ot_hours = $ot_detail_ot_hours_diff;
				$hours_considered -= $ot_detail_ot_hours_diff;
			}
			
			if($ot_detail['dt_hours_acc'] != $ot_detail['dt_hours']){// Count Toward DT Hours
				$dt_detail_dt_hours_diff = $ot_detail['dt_hours'] - $ot_detail['dt_hours_acc'];
				if($dt_detail_dt_hours_diff > $hours_considered){
					$dt_detail_dt_hours_diff = $hours_considered;
				}
				$ot_detail['dt_hours_acc'] += $dt_detail_dt_hours_diff;
				$dt_detail_dt_hours = $dt_detail_dt_hours_diff;
				$hours_considered -= $dt_detail_dt_hours_diff;
			}
			
			/**
			 * This is to resolve the mismatched/inaccuracy between the OT calculated between days,
			 * and the time calculated by time punches.  This may cause issues in the future, look
			 * for it here.
			 **/
			if($hours_considered != 0){
				if($ot_detail['dt_hours'] != 0){
					$dt_detail_dt_hours += $hours_considered;
				}
				else if($ot_detail['ot_hours'] != 0){
					$ot_detail_dt_hours += $hours_considered;
				}
				else{
					$reg_detail_dt_hours += $hours_considered;
				}
				$hours_considered = 0; // All Hours Accounted For.
			}
			
			return array( $ot_detail_reg_hours, $ot_detail_ot_hours, $dt_detail_dt_hours);
		}
		
		function query_safe($value) {
	
			return str_replace("'", "''", $value);	
		}
		
		
		//echo "location_id: $locationid<br>";
		$location_query = lavu_query("SELECT * FROM `locations` where id='[1]'",$locationid);
		if(mysqli_num_rows($location_query)){
			$location_read   = mysqli_fetch_assoc($location_query);
			$locationname    = $location_read['title'];
		}		
		else $locationname = "";
		
		if($modules->hasModule("employees.classes")){
			$allow_order_by_class = true;
		}
		else $allow_order_by_class = false;
		
		$order_by_class       = false;
		$order_by_class_type  = 0;
		$server_classes       = array();
		function get_server_class($class_id,$var=""){
			global $server_classes;
			if($var!="")
				return $server_classes[$class_id][$var];
			else
				return $server_classes[$class_id];
		}
		
		if(isset($_GET['order_by_class'])){
			$set_order_by_class = $_GET['order_by_class'];
			if($set_order_by_class=="all"){
				$_SESSION['order_by_class']         = 1;
				$_SESSION['order_by_class_type']    = 0;
			}
			else if(is_numeric($set_order_by_class) && $set_order_by_class > 0){
				$_SESSION['order_by_class']         = 1;
				$_SESSION['order_by_class_type']    = $set_order_by_class;
			}
			else{
				$_SESSION['order_by_class']         = 0;
				$_SESSION['order_by_class_type']    = 0;
			}
		}
		if($allow_order_by_class){
			$class_query = lavu_query("select `id`,`title` from `emp_classes`");
			while($class_read = mysqli_fetch_assoc($class_query)){
				$server_classes[$class_read['id']] = $class_read;
			}
		}
		if(isset($_SESSION['order_by_class']) && $_SESSION['order_by_class']=="1"){	
			$order_by_class = true;
			$order_by_class_type = (isset($_SESSION['order_by_class_type']))?$_SESSION['order_by_class_type']:0;
		}
		
		if(isset($_GET['editpunch'])){
			
			$punchid     = $_GET['editpunch'];
			$userid      = (isset($_GET['userid']))?$_GET['userid']:false;
			
			$username    = (isset($_GET['username']))?$_GET['username']:false;
			$modify      = (isset($_GET['modify']))?$_GET['modify']:false;
			$setpunch    = (isset($_GET['set']))?$_GET['set']:false;
			$setcol      = (isset($_GET['setcol']))?$_GET['setcol']:"time";
			$edittype    = (isset($_GET['edittype']))?$_GET['edittype']:false;
			$otherval    = (isset($_GET['otherval']))?$_GET['otherval']:false;
			
			$delete_in   = (isset($_GET['delete_in']))?$_GET['delete_in']:false;
			$delete_out  = (isset($_GET['delete_out']))?$_GET['delete_out']:false;
			
			echo "<body style='background:transparent'>";
			echo "<style>";
			echo "body,table {font-family:Verdana,Arial; color:white;} ";
			echo ".rowtitle {padding:6px; color:white;} ";
			echo "a:link, a:visited, a:hover {color:#000088;} ";
			echo "</style>";
			
			if($delete_in || $delete_out){
				if($delete_in)
					//$punch_query = "delete from `clock_punches` where id='$delete_in'";
					$punch_query = "UPDATE `clock_punches` SET `_deleted` = '1', `notes` = concat(`notes`, IF(`notes` != '', ' - ', ''), 'Deleted by ".admin_info("username")." (".admin_info("loggedin").") at ".date("Y-m-d H:i:s")." from IP address ".$_SERVER['REMOTE_ADDR']."') WHERE `id` = '$delete_in'";
				if($delete_out)
					//$punch_query = "delete from `clock_punches` where id='$delete_out'";
					$punch_query = "UPDATE `clock_punches` SET `_deleted` = '1', `notes` = concat(`notes`, IF(`notes` != '', ' - ', ''), 'Deleted by ".admin_info("username")." (".admin_info("loggedin").") at ".date("Y-m-d H:i:s")." from IP address ".$_SERVER['REMOTE_ADDR']."') WHERE `id` = '$delete_out'";
				lavu_query($punch_query);
				schedule_remote_to_local_sync_if_lls($location_info, $locationid,"run local query","clock_punches",$punch_query);
				//echo "Entry deleted";
				echo "<script language='javascript'>window.parent.location = window.parent.location;</script>";
			}
			else if($punchid=="new"){
				if(isset($_POST['ed_date_clockin'])){
					$set_clockin_date  = $_POST['ed_date_clockin'];
					$set_clockin_time  = $_POST['ed_time_clockin'];
					$set_clockin_ap    = $_POST['ed_ap_clockin'];
					$set_clockout_date = $_POST['ed_date_clockout'];
					$set_clockout_time = $_POST['ed_time_clockout'];
					$set_clockout_ap   = $_POST['ed_ap_clockout'];
									
					$set_clockin_time  = convert_to_military_time($set_clockin_time,$set_clockin_ap);
					$set_clockout_time = convert_to_military_time($set_clockout_time,$set_clockout_ap);
					
					$set_clockin       = $set_clockin_date . " " . $set_clockin_time . ":00";
					$set_clockout      = $set_clockout_date . " " . $set_clockout_time . ":00";
					
					$set_punched_out   = 0;
					$set_hours         = "";
					if($set_clockout!=""){
						$set_punched_out  = 1;
						$set_hours        = get_hours_difference($set_clockin,$set_clockout);
					}
					
					// check for conflicts with existing punches
					$a_insert_vars = array('punch_type'=>'Shift', 'time'=>$set_clockin, 'time_out'=>$set_clockout, 'server'=>$username);
					$s_conflicts = get_punch_conflicts($a_insert_vars, $set_clockin_ap, $set_clockin_date, $set_clockin_time, $set_clockout_ap, $set_clockout_date, $set_clockout_time);
					if ($s_conflicts !== FALSE) {
						echo $s_conflicts;
					} else {
						
						// insert the new punch
						$punch_query = "insert into `clock_punches` (`punch_type`,`time`,`time_out`,`hours`,`punched_out`,`server_id`,`server`,`location_id`,`location`,`server_time`,`udid_in`,`udid_out`,`ip_in`,`ip_out`,`notes`) values ('Shift','$set_clockin','$set_clockout','$set_hours','$set_punched_out','$userid','$username','$locationid','".query_safe($locationname)."','".date("Y-m-d H:i:s")."','Back End','Back End','".$_SERVER['REMOTE_ADDR']."','".$_SERVER['REMOTE_ADDR']."','Created by ".admin_info("username")." (".admin_info("loggedin").")')";
						lavu_query($punch_query);
						schedule_remote_to_local_sync_if_lls($location_info, $locationid,"run local query","clock_punches",$punch_query);
						//echo "New Punch Inserted";
						echo "<script language='javascript'>window.parent.location = window.parent.location;</script>";
					}
				}
				else{
					$display_clockintime   = edit_display_datetime("clockin","");
					$display_clockouttime  = edit_display_datetime("clockout","");
					echo <<<FORM
						Create New Punch for $username:<br>
						<form name='new_punch' method='post' action='form_dialog.php?mode=timecard&editpunch=$punchid&userid=$userid&username=$username'>
							<table>
								<tr><td class='rowtitle'>Clocked In: </td><td>{$display_clockintime}</td></tr>
								<tr><td class='rowtitle'>Clocked Out: </td><td>{$display_clockouttime}</td></tr>
								<tr><td></td><td><input type='button' value='Submit' onclick='if (check_format_times()) document.new_punch.submit();'></td></tr>
							</table>
						</form>
FORM;
				}
			}
			else{
				if($setpunch && $modify){
					
					$a_punch = ConnectionHub::getConn('rest')->getDBAPI()->getAllInTable('clock_punches', array('id'=>$modify), FALSE);
					$a_punch = $a_punch[0];
					$a_punch[$setcol] = $setpunch;
					
					if($edittype=="in" && $otherval){
						$set_hours = get_hours_difference($setpunch,$otherval);
					}
					else if($edittype=="out" && $otherval){
						$set_hours = get_hours_difference($otherval,$setpunch);
					}
					else $set_hours = "";
					if($set_hours!="") {
						$query_addition = ", `hours`='$set_hours'";
						$a_punch['hours'] = $set_hours;
					} else {
						$query_addition = "";
					}
					
					$set_clockin_ap = date('a', strtotime($a_punch['time']));
					$set_clockin_date = date('m/d', strtotime($a_punch['time']));
					$set_clockin_time = date('h:i', strtotime($a_punch['time']));
					$set_clockout_ap = date('a', strtotime($a_punch['timeout']));
					$set_clockout_date = date('m/d', strtotime($a_punch['timeout']));
					$set_clockout_time = date('h:i', strtotime($a_punch['timeout']));
					$set_clockin = $a_punch['time'];
					$set_clockout = $a_punch['time_out'];
					$username = ($username != '') ? $username : $a_punch['server'];
					
					// check for conflicts with existing punches
					$a_insert_vars = array('punch_type'=>'Shift', 'time'=>$set_clockin, 'time_out'=>$set_clockout, 'server'=>$username);
					$s_conflicts = get_punch_conflicts($a_insert_vars, $set_clockin_ap, $set_clockin_date, $set_clockin_time, $set_clockout_ap, $set_clockout_date, $set_clockout_time, 'modified', $modify);
					if ($s_conflicts !== FALSE) {
						echo $s_conflicts;
					} else {
					
						$punch_query = "update `clock_punches` set `$setcol`='$setpunch'".$query_addition.", `notes` = concat(`notes`, IF(`notes` != '', ' - ', ''), 'Modified by ".admin_info("username")." (".admin_info("loggedin").") at ".date("Y-m-d H:i:s")." from IP address ".$_SERVER['REMOTE_ADDR']."') where `id`='$modify'";
						lavu_query($punch_query);
						schedule_remote_to_local_sync_if_lls($location_info, $locationid,"run local query","clock_punches",$punch_query);
						$modify = false;
					}
				}
				
				
				$clockin_set    = "time";
				$clockout_set   = "time";
				$clock_query    = lavu_query("select * from `clock_punches` where `_deleted` != '1' AND `id`='$punchid'");
				if(mysqli_num_rows($clock_query)){
					$clock_read    = mysqli_fetch_assoc($clock_query);
					$server_name   = $clock_read['server'];
					$server_id     = $clock_read['server_id'];
					$punch_type    = $clock_read['punch_type'];
					if($punch_type=="Shift"){
						$clockin_time     = $clock_read['time'];
						$clockin_id       = $clock_read['id'];
						$clockout_time    = $clock_read['time_out'];
						$clockout_id      = $clock_read['id'];
						$clockout_set     = "time_out";
						$duration         = $clock_read['hours'];
					}
					else if($punch_type=="Clocked In"){
						$clockin_time = $clock_read['time'];
						$clockin_id   = $clock_read['id'];
						$c2_query     = lavu_query("select * from `clock_punches` where `_deleted` != '1' AND `server_id`='$server_id' and `punch_type`='Clocked Out' and `time`>'$clockin_time' order by `time` asc limit 1");
						if(mysqli_num_rows($c2_query)){
							$c2_read         = mysqli_fetch_assoc($c2_query);
							$clockout_time   = $c2_read['time'];
							$clockout_id     = $c2_read['id'];
							$duration        = $c2_read['hours'];
						}
						else{
							$clockout_time   = false;
							$clockout_id     = false;
							$duration        = false;
						}
					}
					else if($punch_type == "Clocked Out"){
						$clockout_time    = $clock_read['time'];
						$clockout_id      = $clock_read['id'];
						$duration         = $clock_read['hours'];
						$c2_query         = lavu_query("select * from `clock_punches` where `_deleted` != '1' AND `server_id`='$server_id' and `punch_type`='Clocked In' and `time`<'$clockout_time' order by `time` desc limit 1");
						if(mysqli_num_rows($c2_query)){
							$c2_read         = mysqli_fetch_assoc($c2_query);
							$clockin_time    = $c2_read['time'];
							$clockin_id      = $c2_read['id'];
						}
						else{
							$clockin_time    = false;
							$clockin_id      = false;
						}
					}
					echo <<<SCRIPT
						<script language='javascript'>
							function get_modified_datetime(dname) { 
								ddate = document.getElementById('ed_date_' + dname).value;
								dtime = document.getElementById('ed_time_' + dname).value;;
								dap = document.getElementById('ed_ap_' + dname).value;
								dsplit = dtime.split(':');
								if(dsplit.length > 1) {
								dhours = dsplit[0] * 1;
								dmins = dsplit[1] * 1;
								if(dhours < 1 || dhours > 12) alert('Invalid hours, must be between 1 and 12');
								else if(dmins < 0 || dmins > 59) alert('Invalid minutes, must be between 0 and 59');
								else {
									if(dap=='am' && dhours==12) dhours = 0;
										else if(dap=='pm' && dhours<12) dhours += 12;
										if(dhours < 10) dhours = '0'+dhours;
										if(dmins < 10) dmins = '0'+dmins;
										set_datetime = ddate + ' ' + dhours + ':' + dmins + ':00';
										window.location = 'form_dialog.php?mode=timecard&editpunch={$punchid}&edittype={$edittype}&setcol={$setcol}&otherval=$otherval&modify=$modify&set=' + set_datetime;
									}
								} else {
								  alert('Invalid time, must be in format h:mm');
								}
							}
						</script>
						<table>
							<tr>
								<td class='rowtitle'>Server Name: </td><td>{$server_name}</td>
							</tr>
SCRIPT;
					if($clockin_time || $punch_type=="Shift") {
						if($clockin_id==$modify && $edittype=="in")
							echo "<tr><td class='rowtitle'>Clocked In: </td><td>" . edit_display_datetime("clockin",$clockin_time,"form_dialog.php?mode=timecard&editpunch=$punchid&edittype=in&setcol=$clockin_set&otherval=$clockout_time&modify=$modify&set=") . "</td><td><input type='button' value='Update' onclick='get_modified_datetime(\"clockin\")' /></td></tr>";
						else
							echo "<tr><td class='rowtitle'>Clocked In: </td><td>" . display_datetime($clockin_time) . "</td><td><a style='color:#aecd37' href='form_dialog.php?mode=timecard&editpunch=$punchid&edittype=in&setcol=$clockin_set&otherval=$clockout_time&modify=$clockin_id'>(modify)</a></td></tr>";
					}
					if($clockout_time || $punch_type=="Shift") {
						if($clockout_id==$modify && $edittype=="out")
							echo "<tr><td class='rowtitle'>Clocked Out: </td><td>" . edit_display_datetime("clockout",$clockout_time,"form_dialog.php?mode=timcard&editpunch=$punchid&edittype=out&setcol=$clockout_set&otherval=$clockin_time&modify=$modify&set=") . "</td><td><input type='button' value='Update' onclick='get_modified_datetime(\"clockout\")' /></td></tr>";
						else
							echo "<tr><td class='rowtitle'>Clocked Out: </td><td>" . display_datetime($clockout_time) . "</td><td><a style='color:#aecd37' href='form_dialog.php?mode=timecard&editpunch=$punchid&edittype=out&setcol=$clockout_set&otherval=$clockin_time&modify=$clockout_id'>(modify)</a></td></tr>";
					}
					if($duration) echo "<tr><td class='rowtitle'>Duration: </td><td>" . number_format($duration,2,".",",") . "</td></tr>";
					echo "</table>";
					echo "<br><br><br>";
					echo "<a style='cursor:pointer' onclick='if(confirm(\"Are you sure you want to delete this entry?\")) window.location = \"form_dialog.php?mode=timecard&editpunch=$punchid&delete_in=$clockin_id&delete_out=$clockout_id\"'><font color='red' style='font-size:10px'>Delete this entry</font></a>";
					echo "<br /><br />
						<font style='color:lightgray;'>The time should be in the format hh:mm (eg 02:15)</font>";
				}
			}
			
			echo "</body>";
			exit();
		}
		
		$title            = "Time Card Report:";
		$mid              = "";
		$location         = $locationid;
		$user             = getvar("user");

		$export_output    = "";
		$export_row       = "\r\n";
		$export_col       = "\t";
		$export_type      = getvar("export_type");
		if($export_type=="xls") $export_ext = "xls"; else $export_ext = "txt";
		
		if($user){
			$o_date_chooser = get_date_chooser_obj();
			$use_date        = $o_date_chooser->use_date;
			$use_date2       = $o_date_chooser->use_date2;
			$udate           = explode("-",$use_date);
			$uyear           = $udate[0];
			$umonth          = $udate[1];
			$uday            = $udate[2];
			$udate_display   = date("m/d/Y",mktime(0,0,0,$umonth,$uday,$uyear));
			$udate2          = explode("-",$use_date2);
			if(count($udate2) > 2){
				$uyear2         = $udate2[0];
				$umonth2        = $udate2[1];
				$uday2          = $udate2[2];
				$udate2_display = date("m/d/Y",mktime(0,0,0,$umonth2,$uday2,$uyear2));
			}
			else $udate2_display = "";
			
			if (!isset($location_info)) {
				$location_query = lavu_query("SELECT * FROM locations where id=$location");
				if (mysqli_num_rows($location_query)) $location_info = mysqli_fetch_assoc($location_query);
			}
				
			//$location_query = lavu_query("SELECT * FROM locations where id=$location");
			//if(mysqli_num_rows($location_query))
			//{
				//$location_read = mysqli_fetch_assoc($location_query);
				$location_read  = $location_info;
				
				$order_by       = "`server_id` ASC";
				$id_onclick     = "";
				$server_onlick  = "";
				$rh_onclick     = "";
				$ot_onclick     = "";
				
				$detail_mode    = (isset($_REQUEST['dm']))?$_REQUEST['dm']:"details";
											
				if($user=="all") {
					$user_found        = true;
					$show_user_name    = "All Users";
					$user_query_code   = "";
					
					$order_key         = (isset($_REQUEST['ob']))?$_REQUEST['ob']:"l_name";// WHAT IS THE ORDER_KEY FOR!!!!!????? <!----- Let me see, maybe its for ordering stuff?
					
					$ord_dir           = (isset($_REQUEST['od']))?$_REQUEST['od']:"asc";
					$forward_to        = "index.php?mode=$mode&date=$use_date&date2=$use_date2&user=all";//WHAT IS A USE DATE!!!??? <!----- You've gotta be kidding, you really can't figure that out?
					
					if ($order_key != "server_id") $id_onclick = " onclick='window.location = \"$forward_to&ob=server_id&od=asc&dm=$detail_mode\"' style='cursor:s-resize'";
					if ($order_key == "l_name") $server_onclick = " onclick='window.location = \"$forward_to&ob=f_name&od=asc&dm=$detail_mode\";' style='cursor:s-resize'";
					else $server_onclick = " onclick='window.location = \"$forward_to&ob=l_name&od=asc&dm=$detail_mode\";' style='cursor:s-resize'";

					$set_dir = "asc";//WHAT IS AN ASC!!?? THIS IS A VERY UNDESCRIPTIVE VARIABLE NAME <---- It means Ascending!
					$cursor_style = "s-resize";
					if ($order_key=="reg_hours" && $ord_dir=="asc") {
						$set_dir = "desc";
						$cursor_style = "n-resize";
					}
					$rh_onclick = " onclick='window.location = \"$forward_to&ob=reg_hours&od=".$set_dir."&dm=$detail_mode\"' style='cursor:".$cursor_style."'";
	
					$set_dir = "asc";
					$cursor_style = "s-resize";
					if ($order_key=="ot_hours" && $ord_dir=="asc") {
						$set_dir = "desc";
						$cursor_style = "n-resize";
					}
					$ot_onclick = " onclick='window.location = \"$forward_to&ob=ot_hours&od=".$set_dir."&dm=$detail_mode\"' style='cursor:".$cursor_style."'";
				}//if user = all
				else{
					$user_query = lavu_query("SELECT * FROM `users` where `id`='[1]'",$user);
					$user_found = mysqli_num_rows($user_query);
					if($user_found){
						$user_read = mysqli_fetch_assoc($user_query);
						$show_user_name = trim($user_read['f_name'] . " " . $user_read['l_name']);
						$user_query_code = "AND `clock_punches`.`server_id`='".str_replace("'","''",$user_read['id'])."' ";
					}
				}//else
							
				if($user_found){
					$server_name = $show_user_name;
					$title = "<a style='cursor:pointer; color:#000000' href='index.php?mode=$mode'>Time Card:</a> ";
					$title .= "<a style='cursor:pointer; color:#aecd37' href='index.php?mode=$mode&date=$use_date&date2=$use_date2'>".$location_read['title']."</a>";
					$title .= " - ";
					$title .= $show_user_name;
					$title .= "<br>".$udate_display;
					if($udate2_display!="") $title .= " - " . $udate2_display;
					
					if(!isset($use_date) || $use_date2=="") $use_date2 = $use_date;
					$startd = $use_date . " 00:00:00";
					$endd = $use_date2 . " 99:99:99";
					$cvars = array();
					$cvars['startd'] = $startd;
					$cvars['endd'] = $endd;
					$cvars['location_id'] = $location_read['id'];
					if($order_by_class){
						$extra_cond = "";
						if($order_by_class_type > 0){
							$cvars['employee_class'] = $order_by_class_type;
							$extra_cond = " AND `employee_class`='[employee_class]'";
						}
						$clock_query = lavu_query("SELECT `users`.`employee_class` as `employee_class`,`clock_punches`.`id` AS `id`, `clock_punches`.`server_id` AS `server_id`, `clock_punches`.`server` AS `server`, `users`.`f_name` AS `f_name`, `users`.`l_name` AS `l_name`, `clock_punches`.`punch_type` AS `punch_type`, `clock_punches`.`time` AS `time`, `clock_punches`.`time_out` AS `time_out`, `clock_punches`.`hours` AS `hours`, `clock_punches`.`break` AS `break`, `users`.`payrate` as `payrate` FROM `clock_punches` LEFT JOIN `users` ON `users`.`id` = `clock_punches`.`server_id` WHERE `clock_punches`.`_deleted`!='1'".$extra_cond." AND `clock_punches`.`time`>='$startd' AND `clock_punches`.`time`<='$endd' AND `clock_punches`.`location_id`='[location_id]' ".$user_query_code."ORDER BY `clock_punches`.`server_id` ASC, `clock_punches`.`server` ASC, `time` ASC",$cvars);
					}
					else
						$clock_query = lavu_query("SELECT `clock_punches`.`id` AS `id`, `clock_punches`.`server_id` AS `server_id`, `clock_punches`.`server` AS `server`, `users`.`f_name` AS `f_name`, `users`.`l_name` AS `l_name`, `clock_punches`.`punch_type` AS `punch_type`, `clock_punches`.`time` AS `time`, `clock_punches`.`time_out` AS `time_out`, `clock_punches`.`hours` AS `hours`, `clock_punches`.`break` AS `break`, `users`.`payrate` as `payrate` FROM `clock_punches` LEFT JOIN `users` ON `users`.`id` = `clock_punches`.`server_id` WHERE `clock_punches`.`_deleted`!='1' AND `clock_punches`.`time`>='$startd' AND `clock_punches`.`time`<='$endd' AND `clock_punches`.`location_id`='[location_id]' ".$user_query_code."ORDER BY `clock_punches`.`server_id` ASC, `clock_punches`.`server` ASC, `time` ASC",$cvars);
					$mid .= "<tr><td align='left'>";
					$mid .= "<style>";
					$mid .= "  .ttop { border-bottom:solid 2px #777777; text-align:center; font-weight:bold; }";
					$mid .= "  .tbot { border-top:solid 2px #777777; text-align:left; font-weight:bold; }";
					$mid .= "</style>";
					
					$dm_title = "List Totals Only";
					$dm_setto = "totals";
					if ($detail_mode == "totals") {
						$dm_title = "List Details";
						$dm_setto = "details";
					}
					if ($user=="all") {
						$mid .= "<table width='100%'><tr><td align='center'>";
						$mid .= "<input type='button' value='$dm_title' onclick='window.location = \"$forward_to&ob=$order_key&od=$ord_dir&dm=$dm_setto\"'>";
						if($allow_order_by_class){
							$mid .= "&nbsp;&nbsp;";
							$mid .= "<select onchange='window.location = \"$forward_to&order_by_class=\" + this.value'>";
							$mid .= "<option value='0'>Do not order by class</option>";
							$mid .= "<option value='all'";
							if($order_by_class) $mid .= " selected";
							$mid .= ">Order by Class (Show All)</option>";
							
							$sname_list = array();
							foreach($server_classes as $key => $val)
								$sname_list[$val['title']] = $key;
							ksort($sname_list);	
							//sort($sname_list);
							foreach($sname_list as $key => $val)
							{
								$mid .= "<option value='$val'";
								if($order_by_class_type==$val) $mid .= " selected";
								$mid .= ">".$key."</option>";
							}
							$mid .= "</select>";
						}
						
						$mid .= "</td></tr><tr><td>&nbsp;</td></tr></table>";
					}
					
					$mid .= "<form name='eod' method='post' action=''>";
					$mid .= "<table cellspacing=0 cellpadding=2>";
					$mid .= "<tr>";
					$mid .= "<td class='ttop'$id_onclick>ID</td><td class='ttop'>&nbsp;</td>";
					$mid .= "<td class='ttop'$server_onclick>Server</td><td class='ttop'>&nbsp;</td>";
					$mid .= "<td class='ttop'>Time In</td><td class='ttop'>&nbsp;</td>";
					$mid .= "<td class='ttop'>Time Out</td><td class='ttop'>&nbsp;</td>";
					$mid .= "<td class='ttop'>Total Hrs</td><td class='ttop'>&nbsp;</td>";
					$mid .= "<td class='ttop'$rh_onclick>Reg Hrs</td><td class='ttop'>&nbsp;</td>";
					$mid .= "<td class='ttop'$ot_onclick>OT Hrs</td><td class='ttop'>&nbsp;</td>";
					$mid .= "<td class='ttop'>DT Hrs</td><td class='ttop'>&nbsp;</td>";
					if($modules->hasModule("employees.payrates")){
						$mid .= "<td class='ttop'>Total Paid</td><td class='ttop'>&nbsp;</td>";
						$mid .= "<td class='ttop'>Reg Paid</td><td class='ttop'>&nbsp;</td>";
						$mid .= "<td class='ttop'>OT Paid</td><td class='ttop'>&nbsp;</td>";
						$mid .= "<td class='ttop'>DT Paid</td><td class='ttop'>&nbsp;</td>";
					}
					$mid .= "</tr>";
					$export_output .= "ID" . $export_col;
					$export_output .= "Server" . $export_col;
					$export_output .= "Time In" . $export_col;
					$export_output .= "Time Out" . $export_col;
					$export_output .= "Hours" . $export_col;
					$export_output .= "Reg Hrs" . $export_col;
					$export_output .= "OT Hrs" . $export_col;
					$export_output .= "DT Hrs" . $export_col;
					$export_output .= "Total Paid" . $export_col;
					$export_output .= "Reg Hrs Paid" . $export_col;
					$export_output .= "OT Hrs Paid" . $export_col;
					$export_output .= "DT Hrs Paid" . $export_col;
					
					$t_hours           = 0;
					$t_reg_hours       = 0;
					$t_ot_hours        = 0;
					$t_dt_hours        = 0;
					$u_hours           = 0;
					$t_reg_hours_paid  = 0;
					$t_ot_hours_paid   = 0;
					$t_dt_hours_paid   = 0;
					$last_user         = "";
					$last_userid       = "";
	
					if(mysqli_num_rows($clock_query) < 1){
						$mid .= "<td colspan='16' align='center'>&nbsp;<br>No Hours Found<br>&nbsp;</td>";
					}
					
					// calc overtime info
					
					//echo $ot_info["regular_hours"];
					//exit();
					
					$html          = "";
					$details_array = array();
					$info_array    = array();
					$sort_array    = array();
					
					function add_to_export_output($info) {
						global $modules;
						global $export_output;
						global $export_row;
						global $export_col;
						global $location_info;
						
						foreach ($info['details'] as $detail) {
							$export_output .= $export_row;
							$export_output .= $detail['server_id'] . $export_col;
							$export_output .= $detail['server_name'] . $export_col;
							$export_output .= $detail['time_in'] . $export_col;
							$export_output .= $detail['time_out'] . $export_col;
							$export_output .= $detail['hours'] . $export_col;
							$export_output .= $detail['reg_hours'] . $export_col;					
							$export_output .= $detail['ot_hours'] . $export_col;					
							$export_output .= $detail['dt_hours'] . $export_col;
							if($modules->hasModule("employees.payrates")){
								$export_output .= display_money($detail['total_paid'],$location_info) . $export_col;
								$export_output .= display_money($detail['reg_hours_paid'],$location_info) . $export_col;
								$export_output .= display_money($detail['ot_hours_paid'],$location_info) . $export_col;
								$export_output .= display_money($detail['dt_hours_paid'],$location_info) . $export_col;
							}				
						}
						$export_output .= $export_row;
						$export_output .= $info['server_id'] . $export_col;
						$export_output .= $info['server_name'] . $export_col;
						$export_output .= $info['break_total'] . $export_col;
						$export_output .= $export_col;
						$export_output .= $info['total_hours'] . $export_col;
						$export_output .= $info['reg_hours'] . $export_col;
						$export_output .= $info['ot_hours'] . $export_col;
						$export_output .= $info['dt_hours'] . $export_col;
						if($modules->hasModule("employees.payrates")){
							$export_output .= display_money($info['total_paid'],$location_info) . $export_col;
							$export_output .= display_money($info['reg_hours_paid'],$location_info) . $export_col;
							$export_output .= display_money($info['ot_hours_paid'],$location_info) . $export_col;
							$export_output .= display_money($info['dt_hours_paid'],$location_info) . $export_col;
						}
					}//add_to_export_output()
					$searchForType = "overtime_setting_day2"; // this represents from the config table associated with overtime settings */
					$settingsResults = lavu_query("SELECT * FROM `config` WHERE `location`='[1]' AND type='[2]'",$locationid, $searchForType);
					if (!mysqli_num_rows($settingsResults)) {	// I added this on 7/3/2013  because if we dont have an entry for config it was sticking eveything in double time.
						$value =  'DNA';
						$value2 =  0;
						$value3 =  'DNA';
						$value4 =  'DNA';
						$value5 =  'DNA';
						$deleted =  0;
						lavu_query("INSERT INTO `config` (`location`, `value`, `value2`, `value3`, `value4`, `type`, `value5`, `_deleted`) VALUES('[1]', $value, $value2, $value3, $value4, $searchForType, $value5, $deleted)", $locationid);
						$getOTSettingsRow['location'] = $locationid;
						$getOTSettingsRow['value'] = $value;
						$getOTSettingsRow['value2'] = $value2;
						$getOTSettingsRow['value3'] = $value3;
						$getOTSettingsRow['value4'] = $value4;
						$getOTSettingsRow['value5'] = $value5;
						$getOTSettingsRow['type'] = $searchForType;
						$getOTSettingsRow['_deleted'] = $deleted;
					} else {
						$getOTSettingsRow = mysqli_fetch_assoc($settingsResults);
					}
					
					$last_break_out                = false;
					$ot_detail_reg_hours_tot       = 0;
					$ot_detail_ot_hours_tot        = 0;
					$ot_detail_dt_hours_tot        = 0;
					$ot_detail_reg_hours_paid_tot  = 0;
					$ot_detail_ot_hours_paid_tot   = 0;
					$ot_detail_dt_hours_paid_tot   = 0;
					while($clock_read = mysqli_fetch_assoc($clock_query)){
						$server_name              = $clock_read['server'];
						$server_id                = $clock_read['server_id'];
						$f_name                   = $clock_read['f_name'];
						$l_name                   = $clock_read['l_name'];
						$pay_rate                 = $clock_read['payrate'];
						
						if(isset($clock_read['employee_class'])){
							$server_class_id = $clock_read['employee_class'];
						}
						else
							$server_class_id = "";
						
						if($last_userid!="" && $last_userid!=$server_id){
							/* This adds to the row for totals at the bottom of each entry*/

							//echo "startd: $startd<br>";
							//echo "use_date2: $use_date2<br>";
							//echo "locationid: $locationid<br>";
							//echo "server_id: $server_id<br>";
							$show_break_total = "";
							if ($u_bo_hours > 0.009) $show_break_total = "Break Total: ".number_format($u_bo_hours,2,".","")." hours";
							$reg_hours   = number_format((float)$ot_info["regular_hours"],2,".","");
							$ot_hours    = number_format((float)$ot_info["ot_hours"],2,".","");//when the user id = all
							$dt_hours    = number_format((float)$ot_info["dt_hours"],2,".","");
							
							$html .= "<tr>";
							$html .= "<td class='tbot'>".$last_userid."</td><td class='tbot'>&nbsp;</td>";
							$html .= "<td class='tbot'>".$show_server_name."</td><td class='tbot'>&nbsp;</td>";
							$html .= "<td class='tbot' colspan='4'><b>".$show_break_total."</b></td>";
							$html .= "<td class='tbot' style='text-align:right'>".number_format($u_hours,2)."</td><td class='tbot'>&nbsp;</td>";
							$html .= "<td class='tbot' style='text-align:right'>".number_format($ot_detail_reg_hours_tot,2)."</td><td class='tbot'>&nbsp;</td>";
							$html .= "<td class='tbot' style='text-align:right'>".number_format($ot_detail_ot_hours_tot,2)."</td><td class='tbot'>&nbsp;</td>";
							$html .= "<td class='tbot' style='text-align:right'>".number_format($ot_detail_dt_hours_tot,2)."</td><td class='tbot'>&nbsp;</td>";
							if($modules->hasModule("employees.payrates")){
								$total_paid = $ot_detail_reg_hours_paid_tot + $ot_detail_ot_hours_paid_tot + $ot_detail_dt_hours_paid_tot;
								$html .= "<td class='tbot' style='text-align:right'>".display_money($total_paid,$location_info)."</td><td class='tbot'>&nbsp;</td>";
								$html .= "<td class='tbot' style='text-align:right'>".display_money($ot_detail_reg_hours_paid_tot,$location_info)."</td><td class='tbot'>&nbsp;</td>";
								$html .= "<td class='tbot' style='text-align:right'>".display_money($ot_detail_ot_hours_paid_tot,$location_info)."</td><td class='tbot'>&nbsp;</td>";
								$html .= "<td class='tbot' style='text-align:right'>".display_money($ot_detail_dt_hours_paid_tot,$location_info)."</td><td class='tbot'>&nbsp;</td>";
							}
							$html .= "</tr>";
							$html .= "<tr><td>&nbsp;</td></tr>";
							
							$t_reg_hours                            += $ot_detail_reg_hours_tot;
							$t_ot_hours                             += $ot_detail_ot_hours_tot;
							$t_dt_hours                             += $ot_detail_dt_hours_tot;
							
							$t_reg_hours_paid                       += $ot_detail_reg_hours_paid_tot;
							$t_ot_hours_paid                        += $ot_detail_ot_hours_paid_tot;
							$t_dt_hours_paid                        += $ot_detail_dt_hours_paid_tot;
							
							$this_info                               = array();
							$this_info['html']                       = $html;
							$this_info['server_id']                  = $last_userid;
							$this_info['server_name']                = $show_server_name;
							$this_info['break_total']                = $show_break_total;
							$this_info['total_hours']                = $u_hours;
							$this_info['reg_hours']                  = $reg_hours;
							$this_info['ot_hours']                   = $ot_hours;
							$this_info['dt_hours']                   = $dt_hours;
							$this_info['total_paid']                 = $ot_detail_reg_hours_paid_tot + $ot_detail_ot_hours_paid_tot + $ot_detail_dt_hours_paid_tot;
							$this_info['reg_hours_paid']             = $ot_detail_reg_hours_paid_tot;
							$this_info['ot_hours_paid']              = $ot_detail_ot_hours_paid_tot;
							$this_info['dt_hours_paid']              = $ot_detail_dt_hours_paid_tot;
							$this_info['details']                    = $details_array;
							$info_array[$last_userid.$last_l_name]   = $this_info;
							
							$ot_detail_reg_hours_tot                 = 0;
							$ot_detail_ot_hours_tot                  = 0;
							$ot_detail_dt_hours_tot                  = 0;
							$ot_detail_reg_hours_paid_tot            = 0;
							$ot_detail_ot_hours_paid_tot             = 0;
							$ot_detail_dt_hours_paid_tot             = 0;

							$u_hours                                 = 0;
							$u_bo_hours                              = 0;
							//echo "u_bo_hours: $u_bo_hours<br>";
							unset($ot_info);
							$html            = "";
							$last_break_out  = false;
						}//if
						
						$extra_sort_array_var = "";
						if($order_by_class){
							$extra_sort_array_var = get_server_class($server_class_id,"title");
							if($extra_sort_array_var=="") $extra_sort_array_var = "ZZZZ";
						}
						
						if (!isset($ot_info)) {
							//echo
							$details_array = array();
							$ot_info = calOvertimeHours($startd, $use_date2, $locationid, $server_id, $getOTSettingsRow);
							$ot_details  = $ot_info['ot_steps'];

							if ($order_key == "reg_hours"){ 
								$sort_array[$server_id.$l_name] = $extra_sort_array_var . $ot_info['regular_hours'];
							}//if
							else if ($order_key == "ot_hours"){
								$sort_array[$server_id.$l_name] = $extra_sort_array_var . $ot_info['ot_hours'];
							}//else if
							else{
								$sort_array[$server_id.$l_name] = $extra_sort_array_var . strtoupper($clock_read[$order_key]);
							}//else 
						}//if
						$last_userid  = $server_id;
						$last_user    = $server_name;
						$last_f_name  = $f_name;
						$last_l_name  = $l_name;
						
						if ($order_key == "f_name") {
							$show_server_name = trim($f_name);
							$add_l_name = trim($l_name);
							if (!empty($show_server_name) && !empty($add_l_name)) $show_server_name .= " ";
							$show_server_name .= $add_l_name;
						} else {
							$show_server_name = trim($l_name);
							$add_f_name = trim($f_name);
							if (!empty($show_server_name) && !empty($add_f_name)) $show_server_name .= ", ";
							$show_server_name .= $add_f_name;
						}//else

						if($clock_read['punch_type']=="Clocked In"){
							$time_in     = $clock_read['time'];
							$time_out    = "";
						}//if
						else if($clock_read['punch_type']=="Clocked Out"){
							$time_in     = "";
							$time_out    = $clock_read['time'];
						}//if
						else{
							$time_in     = $clock_read['time'];
							$time_out    = $clock_read['time_out'];						
						}//if
						
						if ($last_break_out) {
						
							$bo_datetime = explode(" ", $last_break_out);// BO IS ACTUALLY SHORT FOR "BREAK OUT", MEANING HOW LONG DID AN EMPLOYEE TAKE FOR A BREAK. I HAD TO HUNT DOWN THREE PEOPLE TO FIGURE THIS OUT
							$bo_date     = explode("-", $bo_datetime[0]);
							$bo_time     = explode(":", $bo_datetime[1]);
							$bo_ts       = mktime($bo_time[0], $bo_time[1], $bo_time[2], $bo_date[1], $bo_date[2], $bo_date[0]);

							$ti_datetime = explode(" ", $time_in);
							$ti_date     = explode("-", $ti_datetime[0]);
							$ti_time     = explode(":", $ti_datetime[1]);
							$ti_ts       = mktime($ti_time[0], $ti_time[1], $ti_time[2], $ti_date[1], $ti_date[2], $ti_date[0]);
							
							$bo_hours = (($ti_ts - $bo_ts) / 3600);
							if ($bo_hours < 3) {
								$u_bo_hours += $bo_hours;
								$show_break = "Break: ".number_format($bo_hours,2,".","")." hours";
							
								if ($detail_mode == "details") {
								
									$html .= "<tr style='color:#336633'>";
									$html .= "<td>".$last_userid."</td><td>&nbsp;</td>";
									$html .= "<td>".$show_server_name."</td><td>&nbsp;</td>";
									$html .= "<td colspan='2'>".$show_break."</td>";
									$html .= "</tr>";
									
									$this_detail                   = array();
									$this_detail['server_id']      = $last_userid;
									$this_detail['server_name']    = $show_server_name;
									$this_detail['time_in']        = $show_break;
									$this_detail['time_out']       = "";
									$this_detail['hours']          = "";
									$details_array[]               = $this_detail;
								}
							}
							$last_break_out = false;
						}//if
												
						if($clock_read['hours'] > 0) $hours = number_format($clock_read['hours'],2,".",""); else $hours = "";
						
						$ot_start_date = substr($time_in, 0, 10);
						$ot_end_date = substr($time_out, 0, 10);
						$hours_considered = $hours;
						
						$ot_res  = considerPunchForDate($ot_details[$ot_start_date], $hours_considered);
						$ot2_res = array( 0, 0, 0);
						if($ot_end_date != $ot_start_date){
							$hours_acc = 0;
							foreach($ot_res as $val){
								$hours_acc += $val;
							}
							$ot2_res = considerPunchForDate($ot_details[$ot_end_date], $hours_considered - $hours_acc);
						}

						$ot_detail_reg_hours     = $ot_res[0] + $ot2_res[0];
						$ot_detail_ot_hours      = $ot_res[1] + $ot2_res[1];
						$ot_detail_dt_hours      = $ot_res[2] + $ot2_res[2];
						
						$ot_detail_reg_hours_tot+= $ot_detail_reg_hours;
						$ot_detail_ot_hours_tot += $ot_detail_ot_hours;
						$ot_detail_dt_hours_tot += $ot_detail_dt_hours;
						
						$ot_detail_reg_hours_paid_tot += $ot_detail_reg_hours * $pay_rate;
						$ot_detail_ot_hours_paid_tot += $ot_detail_ot_hours * $pay_rate * 1.5;
						$ot_detail_dt_hours_paid_tot += $ot_detail_dt_hours * $pay_rate * 2.0;
						if ($detail_mode == "details") {
							$html .= "<tr onmouseover='this.bgColor = \"#aabbee\"' onmouseout='this.bgColor = \"#ffffff\"' style='cursor:pointer' onclick='show_info(\"<iframe src=\\\"resources/form_dialog.php?mode=timecard&editpunch=".$clock_read['id']."\\\" width=500 height=425 frameborder=0 allowtransparency=true></iframe>\")'>";
							$html .= "<td>".$last_userid."</td><td>&nbsp;</td>";
							$html .= "<td>".$show_server_name."</td><td>&nbsp;</td>";					
							$html .= "<td>".display_datetime($time_in)."</td><td>&nbsp;</td>";
							$html .= "<td>".display_datetime($time_out)."</td><td>&nbsp;</td>";
							if ($clock_read['break'] == "1") $last_break_out = $time_out;
							$html .= "<td align='right'>".number_format($hours,2)."</td><td>&nbsp;</td>";
							$html .= "<td align='right'>".number_format($ot_detail_reg_hours,2)."</td><td>&nbsp;</td>";
							$html .= "<td align='right'>".number_format($ot_detail_ot_hours,2)."</td><td>&nbsp;</td>";
							$html .= "<td align='right'>".number_format($ot_detail_dt_hours,2)."</td><td>&nbsp;</td>";
							if($modules->hasModule("employees.payrates")){
								$total_paid = ($ot_detail_reg_hours * $pay_rate) + ($ot_detail_ot_hours * $pay_rate * 1.5) + ($ot_detail_dt_hours * $pay_rate * 2.0);
								$html .= "<td align='right'>".display_money($total_paid,$location_info)."</td><td>&nbsp;</td>";
								$html .= "<td align='right'>".display_money($ot_detail_reg_hours * $pay_rate,$location_info)."</td><td>&nbsp;</td>";
								$html .= "<td align='right'>".display_money($ot_detail_ot_hours * $pay_rate * 1.5,$location_info)."</td><td>&nbsp;</td>";
								$html .= "<td align='right'>".display_money($ot_detail_dt_hours * $pay_rate * 2.0,$location_info)."</td><td>&nbsp;</td>";
							}
							if($order_by_class) $html .= "<td>".get_server_class($server_class_id,"title")."</td>";
							
							//$startd = $use_date . " 00:00:00";
							//$endd = $use_date2 . " 99:99:99";
							/*
							echo "startd: $startd<br>";
							echo "time_in: $time_in<br>";
							echo "locationid: $locationid<br>";
							echo "server_id: $server_id<br>";
							exit();
							*/
							
							//$ot_hrs = $ot_info["ot_hrs"];
							//$reg_hrs = $ot_info["reg_hrs"];
							//$mid .= "<td>$reg_hrs</td>";
							//$mid .= "<td>$ot_hrs</td>";
							$html .= "</tr>";
							
							$this_detail                     = array();
							$this_detail['server_id']        = $last_userid;
							$this_detail['server_name']      = $show_server_name;
							$this_detail['time_in']          = display_datetime($time_in);
							$this_detail['time_out']         = display_datetime($time_out);
							$this_detail['hours']            = $hours;
							$this_detail['reg_hours']        = $ot_detail_reg_hours;
							$this_detail['ot_hours']         = $ot_detail_ot_hours;
							$this_detail['dt_hours']         = $ot_detail_dt_hours;
							$this_detail['total_paid']       = ($ot_detail_reg_hours * $pay_rate) + ($ot_detail_ot_hours * $pay_rate * 1.5) + ($ot_detail_dt_hours * $pay_rate * 2.0);
							$this_detail['reg_hours_paid']   = $ot_detail_reg_hours * $pay_rate;
							$this_detail['ot_hours_paid']    = $ot_detail_ot_hours * $pay_rate * 1.5;
							$this_detail['dt_hours_paid']    = $ot_detail_dt_hours * $pay_rate * 2.0;
							$details_array[]                 = $this_detail;
						}//if
							
						//mail("richard@greenkeyconcepts.com","this detail - ".$show_server_name,print_r($this_detail, true)."\n\n\n".print_r($details_array, true));
						
						/*$export_output .= $export_row;
						$export_output .= $clock_read['server_id'] . $export_col;
						$export_output .= $server_name . $export_col;
						$export_output .= display_datetime($time_in) . $export_col;
						$export_output .= display_datetime($time_out) . $export_col;
						$export_output .= $hours . $export_col;*/
						
						$t_hours += $hours;
						$u_hours += $hours;
					}//while - A VERY GIGANTIC WHILE STATEMENT THAT SPANS MANY LINES CAUSE NO FUNCTIONS;
					
					$show_break_total = "";
					if ($u_bo_hours > 0.009) $show_break_total .= "Break Total: ".number_format($u_bo_hours,2,".","")." hours";

					//echo "startd: $startd<br>";
					//echo "use_date2: $use_date2<br>";

					$reg_hours = number_format((float)$ot_info["regular_hours"],2,".","");
					//echo "reg_hours: $reg_hours<br>";
					//exit();
					$ot_hours = number_format((float)$ot_info["ot_hours"],2,".","");//when a single user is selected
					$dt_hours = number_format((float)$ot_info["dt_hours"],2,".","");
					$html .= "<tr>";
					$html .= "<td class='tbot'>".$last_userid."</td><td class='tbot'>&nbsp;</td>";
					$html .= "<td class='tbot'>".$show_server_name."</td><td class='tbot'>&nbsp;</td>";
					$html .= "<td class='tbot' colspan='4'><b>".$show_break_total."</b></td>";
					$html .= "<td class='tbot' style='text-align:right'>".number_format($u_hours,2)."</td><td class='tbot'>&nbsp;</td>";
					$html .= "<td class='tbot' style='text-align:right'>".number_format($ot_detail_reg_hours_tot,2)."</td><td class='tbot'>&nbsp;</td>";
					$html .= "<td class='tbot' style='text-align:right'>".number_format($ot_detail_ot_hours_tot,2)."</td><td class='tbot'>&nbsp;</td>";//when only one user is selected
					$html .= "<td class='tbot' style='text-align:right'>".number_format($ot_detail_dt_hours_tot,2)."</td><td class='tbot'>&nbsp;</td>";
					if($modules->hasModule("employees.payrates")){
						$total_paid = $ot_detail_reg_hours_paid_tot + $ot_detail_ot_hours_paid_tot + $ot_detail_dt_hours_paid_tot;
						$html .= "<td class='tbot' style='text-align:right;'>".display_money($total_paid,$location_info)."</td><td class='tbot'>&nbsp;</td>";
						$html .= "<td class='tbot' style='text-align:right;'>".display_money($ot_detail_reg_hours_paid_tot,$location_info)."</td><td class='tbot'>&nbsp;</td>";
						$html .= "<td class='tbot' style='text-align:right;'>".display_money($ot_detail_ot_hours_paid_tot,$location_info)."</td><td class='tbot'>&nbsp;</td>";
						$html .= "<td class='tbot' style='text-align:right;'>".display_money($ot_detail_dt_hours_paid_tot,$location_info)."</td><td class='tbot'>&nbsp;</td>";
					}
					$html .= "</tr><tr><td>&nbsp;</td></tr>";
					
					$t_reg_hours                          += $ot_detail_reg_hours_tot;
					$t_ot_hours                           += $ot_detail_ot_hours_tot;
					$t_dt_hours                           += $ot_detail_dt_hours_tot;
					$t_reg_hours_paid                     += $ot_detail_reg_hours_paid_tot;
					$t_ot_hours_paid                      += $ot_detail_ot_hours_paid_tot;
					$t_dt_hours_paid                      += $ot_detail_dt_hours_paid_tot;
					$this_info                             = array();
					$this_info['html']                     = $html;
					$this_info['server_id']                = $last_userid;
					$this_info['server_name']              = $last_userid;
					$this_info['break_total']              = $show_break_total;
					$this_info['total_hours']              = $u_hours;
					$this_info['reg_hours']                = $ot_detail_reg_hours_tot;
					$this_info['ot_hours']                 = $ot_detail_ot_hours_tot;
					$this_info['dt_hours']                 = $ot_detail_dt_hours_tot;
					$this_info['total_paid']               = $ot_detail_reg_hours_paid_tot + $ot_detail_ot_hours_paid_tot + $ot_detail_dt_hours_paid_tot;
					$this_info['reg_hours_paid']           = $ot_detail_reg_hours_paid_tot;
					$this_info['ot_hours_paid']            = $ot_detail_ot_hours_paid_tot;
					$this_info['dt_hours_paid']            = $ot_detail_dt_hours_paid_tot;
					$this_info['details']                  = $details_array;
					$info_array[$last_userid.$last_l_name] = $this_info;
	
					asort($sort_array);
					$keys = array_keys($sort_array);
					if ($ord_dir == "desc") {
						for ($i = (count($keys) - 1); $i >= 0; $i--) {
							$info = $info_array[$keys[$i]];
							$mid .= $info['html'];
							add_to_export_output($info);
						}//for
					} else {
						for ($i = 0; $i < count($keys); $i++) {
							$info = $info_array[$keys[$i]];
							$mid .= $info['html'];
							add_to_export_output($info);
						}//for
					}//else
					
					$mid .= "<tr><td>&nbsp;</td></tr>";
					$mid .= "<tr bgcolor='#777777' style='font-weight:bold; color:#ffffff'>";
					$mid .= "<td class='tbot'>&nbsp;</td><td class='tbot'>&nbsp;</td>";
					$mid .= "<td class='tbot'>Total</td><td class='tbot'>&nbsp;</td>";
					$mid .= "<td class='tbot'>&nbsp;</td><td class='tbot'>&nbsp;</td>";
					$mid .= "<td class='tbot'>&nbsp;</td><td class='tbot'>&nbsp;</td>";
					$mid .= "<td class='tbot' style='text-align:right'>".number_format($t_hours,2,".","")."</td><td class='tbot'>&nbsp;</td>";
					$mid .= "<td class='tbot' style='text-align:right'>".number_format($t_reg_hours,2,".","")."</td><td class='tbot'>&nbsp;</td>";
					$mid .= "<td class='tbot' style='text-align:right'>".number_format($t_ot_hours,2,".","")."</td><td class='tbot'>&nbsp;</td>";
					$mid .= "<td class='tbot' style='text-align:right'>".number_format($t_dt_hours,2,".","")."</td><td class='tbot'>&nbsp;</td>";
					if($modules->hasModule("employees.payrates")){
						$total_paid = $t_reg_hours_paid + $t_ot_hours_paid + $t_dt_hours_paid;
						$mid .= "<td class='tbot' style='text-align:right'>".display_money($total_paid,$location_info)."</td><td class='tbot'>&nbsp;</td>";
						$mid .= "<td class='tbot' style='text-align:right'>".display_money($t_reg_hours_paid,$location_info)."</td><td class='tbot'>&nbsp;</td>";
						$mid .= "<td class='tbot' style='text-align:right'>".display_money($t_ot_hours_paid,$location_info)."</td class='tbot'><td>&nbsp;</td>";
						$mid .= "<td class='tbot' style='text-align:right'>".display_money($t_dt_hours_paid,$location_info)."</td><td class='tbot'>&nbsp;</td>";
					}
					$mid .= "</tr>";
					$mid .= "</table>";
					
					//echo print_r($info_array, true);
					
					if($user!="all"){
						$mid .= "<br><input type='button' value='Create New Punch' onclick='show_info(\"<iframe src=\\\"resources/form_dialog.php?mode=timecard&editpunch=new&userid=$user&username=".str_replace("'","",$server_name)."&locationid=$location&locationname=".str_replace("\"","",str_replace("'","",$location_read['title']))."\\\" width=500 height=425 frameborder=0 allowtransparency=true></iframe>\")'>";
					}//if
																	
					$mid .= "</td></tr>";
					
					if($export_type && $export_type!=""){
						export_report("report_".date("Y-m-d H:i").".".$export_ext,$export_output);
						exit();
					}//if
					else{
						$mid .= "<tr><td>";
						$mid .= "<br><a href='index.php?mode=reports_time_cards&date=$use_date&date2=$use_date2&user=$user&ob=$order_key&od=$ord_dir&dm=$detail_mode&export_type=txt&widget=reports/time_cards'>Export To Tab Delimited  \".txt\" file for import into MS Excel</a>";
						// the xls download doesn't open in MS Excel correctly and people were complaining about it, so they can just import the .txt download into MS Excel and it works fine
						$mid .= "<br><br><a href='index.php?mode=reports_time_cards&date=$use_date&date2=$use_date2&user=$user&ob=$order_key&od=$ord_dir&dm=$detail_mode&export_type=xls&widget=reports/time_cards'>Export To Tab Delimited \".xls\" file</a>";
						$mid .= "</td></tr>";
					}//else
				}//if user found
				else $mid .= "Error: User data not found in the database";
			//}
			//else $mid .= "Error: Location data not found in the database";
		}//if YES THIS IF STATEMENT REALLY IS 470 LINES LONG!!  USE OF FUNCTIONS WOULD MAKE IT SMALLER!!
		else{
			$title = "We are aware of an issues related to the Time Cards Report and Clock In / Out functionality.<br>

The issue that some customers are experiencing is related to fixing another issue in the Time Card system.<br>
 
Original Bug: Users could clock-in through the app, then a create completed pair of time punches (clock-in and clock-out) using the Time Cards report in the Lavu backend. This would leave an unclosed clock-in (the one from the app) which would be ignored since a complete time punch pair was created in the backend. This problem caused issues for some locations when doing payroll.<br>
 
The Fix: To address the original bug, the development team changed the app behavior to recognize the last punch from the app. This has resulted in what you are seeing now. The app requires users with unclosed clock-ins to be closed, by clocking out, prior to being able to clock-in.<br>
 
Moving Forward: Any open clock punches prior to April 1st have been closed. This eliminates the need to clock out numerous times for most users. Complete time punch pairs (in and out) that resulted in 100's or 1000's of hours on the Time Cards report, can be deleted or edited to reflect the appropriate time.<br>
 
Thank you for your patience as the Lavu Support Department works to get back to every customer who has reported this issue and has questions.<br>";
			$title .= "<a style='cursor:pointer; color:#000000' href='index.php?mode=$mode'>Time Cards:</a> ".$location_read['title'];
			$title .= "<br>";
			
			$o_date_chooser = get_date_chooser_obj($mode);
			$use_date = $o_date_chooser->use_date;
			$use_date2 = $o_date_chooser->use_date2;
			$title .= $o_date_chooser->select_clause;
	
			/*$mid .= "<input type='button' value='Print to Receipt Printer' onclick='showSection(\"reports\",\"mode=7&location=$location&date=$use_date&print=1\");'>";
			if(isset($_GET['print']))
			{
				$loc = $company_info['company_name'] . " - " . $location_read['title'];
				$dir = "/home/poslavu/public_html/admin/print/queue/" . $loc;
				
				$str = "receipt:";
				$str .= "---------------------";
				$str .= ":End of Day Report";
				$str .= ":".$udate_display.": ";
				$str .= ":Subtotal[c " . $show_subtotal;
				$str .= ":Tax[c " . $show_tax;
				$str .= ":Auto Gratuity[c " . $show_auto_gratuity;
				$str .= ":Total[c " . $show_total;
				$str .= ":Cash[c " . $show_cash;
				$str .= ":Card Subtotal[c " . $show_card_subtotal;
				$str .= ":Card Gratuity[c " . $show_card_gratuity;
				$str .= ":Card Total[c " . $show_card_total;
				$str .= ":Unpaid[c" . $show_amount_due;
				$str .= ": : : :---------------------";

				if(!is_dir($dir))
					mkdir($dir,0755);
				$fname = $dir . "/".date("YmdHis").rand(10000,99999).".txt";
				if($fp = fopen($fname,"w"))
				{
					fwrite($fp,$str);
					fclose($fp);
					$success = "1";
				}
				mail("corey@meyerwind.com","EOD Report",$fname . "\n\n" . $str,"From: info@poslavu.com");
			}*/
			$mid .= "</td></tr>";
			
			$userlist = array();
			$userlist[] = array("All Users","all");
			$user_query = lavu_query("SELECT * FROM `users` where 1 order by `f_name` asc, `l_name` asc"); //_deleted!='1' 
			while($user_read = mysqli_fetch_assoc($user_query)){
				$userlist[] = array(trim($user_read['f_name'] . " " . $user_read['l_name']), $user_read['id']);
			}//while
			
			$mid .= "<tr><td>&nbsp;</td></tr><tr><td style='border-bottom: solid 2px #777777'>Detailed Timecards</td></tr>";
			for($i=0; $i<count($userlist); $i++){
				$mid .= "<tr><td align='left'><a href='index.php?mode=$mode&date=$use_date&date2=$use_date2&user=" . $userlist[$i][1] . "' class='location_link_lg'>" . $userlist[$i][0] . "</a></td></tr>";
			}	//for
		}//ELSE
		
		$display .= "<table><tr><td align='center'>";
		$display .= "<b>" . $title . "</b><br>&nbsp;";
		$display .= "<table cellspacing=0 cellpadding=0>";
		//$display .= "<tr><td class='inner_panel_top'></td></tr>";
		//$display .= "<tr><td class='inner_panel_mid' align='center'>";
		$display .= "<tr><td align='center'>";
		$display .= "<table>";
		
		$display .= $mid;
		
		$display .= "</table>";
		$display .= "</td></tr>";
		//$display .= "<tr><td class='inner_panel_bottom'></td></tr>";
		$display .= "</table>";
		$display .= "</td></tr>";
		
		echo create_info_layer(600,480);
		echo $display;
	}//if($in_lavu) OMG THIS IF STATEMENT IS  980 LINES LONG!!!!! 980!!!!!!
	
	// checks for conflicts between the shift that is trying to be created/modified and existing shifts
	// returns FALSE if there are not conflictions, or a string to print to the user if there are conflictions
	function get_punch_conflicts($a_insert_vars, $set_clockin_ap, $set_clockin_date, $set_clockin_time, $set_clockout_ap, $set_clockout_date, $set_clockout_time, $s_action = 'created', $punch_id = FALSE) {
		$s_retval = FALSE;
		
		// get the where checks
		$punch_id = ($punch_id !== FALSE) ? "AND `id`!='$punch_id'" : "";
		
		// query for conflicting time cards
		$a_overlapping_punches_timein = ConnectionHub::getConn('rest')->getDBAPI()->getAllInTable('clock_punches', $a_insert_vars, FALSE,
			array('whereclause'=>"WHERE `punch_type`='[punch_type]' AND `server`='[server]' AND `time`<= '[time]' AND `time_out` > '[time]' AND `_deleted`='0' $punch_id"));
		$a_overlapping_punches_timeout = ConnectionHub::getConn('rest')->getDBAPI()->getAllInTable('clock_punches', $a_insert_vars, FALSE,
			array('whereclause'=>"WHERE `punch_type`='[punch_type]' AND `server`='[server]' AND `time_out`>= '[time_out]' AND `time` < '[time_out]' AND `_deleted`='0' $punch_id"));
		
		// alert the user that they are trying to create a punch that conflicts
		if (count($a_overlapping_punches_timein) > 0 || count($a_overlapping_punches_timeout) > 0) {
			$s_retval = '';
			$s_retval .= '<font style="font-weight:bold;">Error:</font><br />
				A punch from '.$set_clockin_date.' '.$set_clockin_time.$set_clockin_ap.' to '.$set_clockout_date.' '.$set_clockout_time.$set_clockout_ap.' can\'t be '.$s_action.' because it conflicts with the following punches:<br /><br />
				<table style="text-align:center;">
					<thead><tr><th>Time In</th><th>Time Out</th></thead>';
			$a_overlapping_punches = array_merge($a_overlapping_punches_timein, $a_overlapping_punches_timeout);
			foreach($a_overlapping_punches as $a_overlapping_punch) {
				$s_retval .= '
					<tr><td>'.date('m/d h:ia', strtotime($a_overlapping_punch['time'])).'</td><td>'.date('m/d h:ia', strtotime($a_overlapping_punch['time_out'])).'</td></tr>';
			}
			$s_retval .= '
				</table><br />';
		}
		
		return $s_retval;
	}
	
	// returns an object with the following fields:
	// use_date: the "from" date
	// use_date2: the "to" date
	// select_clause: the html to produce a date picker
	$o_time_cards_chooser_obj = NULL;
	function get_date_chooser_obj($mode = NULL) {
		
		global $o_time_cards_chooser_obj;
		if (isset($o_time_cards_chooser_obj))
			return $o_time_cards_chooser_obj;
		
		// sanitize the input
		if ($mode === NULL)
			$mode = (isset($_GET['mode'])) ? $_GET['mode'] : '';
		
		// get the current date selection
		$use_date = (isset($_GET['date']))?$_GET['date']:date("Y-m-d");
		$use_date2 = (isset($_GET['date2']))?$_GET['date2']:$use_date;
		if(strtotime($use_date2) < strtotime($use_date)) $use_date2 = $use_date;
		$udate = explode("-",$use_date);
		$uyear = $udate[0];
		$umonth = $udate[1];
		$uday = $udate[2];
		$udate_display = date("m/d/Y",mktime(0,0,0,$umonth,$uday,$uyear));
		
		$select = '';
		
		// get the select "from" clause
		$select .= "<select onchange='window.location = \"index.php?mode=$mode&date2=$use_date2&date=\" + this.value'>";
		for($i=0; $i<=730; $i++){
			$idate = mktime(0,0,0,date("m"),date("d") - $i,date("Y"));
			$idate_value = date("Y-m-d",$idate);
			$idate_display = date("m/d/Y",$idate);
			$select .= "<option value='$idate_value'";
			if($idate_value==$use_date)
				$select .= " selected";
			$select .= ">$idate_display</option>";
		}//for
		$select .= "</select>";
		
		// get the select "to" clause
		$select .= "<select onchange='window.location = \"index.php?mode=$mode&date=$use_date&date2=\" + this.value'>";
		for($i=0; $i<=730; $i++){
			$idate = mktime(0,0,0,date("m"),date("d") - $i,date("Y"));
			$idate_value = date("Y-m-d",$idate);
			$idate_display = date("m/d/Y",$idate);
			if($idate_value >= $use_date){
				$select .= "<option value='$idate_value'";
				if($idate_value==$use_date2)
					$select .= " selected";
				$select .= ">$idate_display</option>";
			}//if
		}//for
		$select .= "</select>";
		
		// create the object to return
		$o_retval = new stdClass();
		$o_retval->use_date = $use_date;
		$o_retval->use_date2 = $use_date2;
		$o_retval->select_clause = $select;
		
		$o_time_cards_chooser_obj = $o_retval;
		
		return $o_retval;
	}
?>
