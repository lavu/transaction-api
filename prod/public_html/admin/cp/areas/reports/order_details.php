<?php
	session_start();
	require_once("resources/core_functions.php");
	$maindb = "poslavu_MAIN_db";
	$data_name = sessvar("admin_dataname");
	$in_lavu = true;
	
	$location_info = sessvar("location_info");
	
	function display_money($val, $ts=",", $show_sym=true) {
	
		global $location_info;
	
		$sym = $location_info['monitary_symbol'];
		$lor = $location_info['left_or_right'];
		$dp = $location_info['disable_decimal'];
		$dc = $location_info['decimal_char'];
		$ts	= ($dc == ".")?",":".";
	
			if (($sym == "") || !$show_sym)
				return number_format(round($val, $dp), $dp, $dc, $ts);
			else if ($lor == "right")
				return number_format(round($val, $dp), $dp, $dc, $ts)." ".$sym;
			else
				return $sym." ".number_format(round($val, $dp), $dp, $dc, $ts);
	}
	
	echo "<style> body, table { font-family:Verdana, Arial, Helvetica, sans-serif; font-size:14px; } </style>
		<body><center>";
		
    function getLoadSnapshotSelectorHTML(){
        global $_GET, $locationid;
        $current_snapshot = (isset($_GET['load_snapshot']))?$_GET['load_snapshot']:"";
        $current_orderID = $_GET['order_id'];
        //$getSnapshotsQuery = "SELECT `id`,`time` FROM `action_log` WHERE `action`='Order_Snapshot' AND `order_id`='[1]'";

        $getSnapshotsQuery = "SELECT `id`,`time`,`action` FROM `action_log` WHERE `order_id`='[1]' AND `loc_id`='[2]' ORDER BY `time` DESC";
        
        //$snapsForOrderIDResult = lavu_query($getSnapshotsQuery, $_GET['order_id']);
        $snapsForOrderIDResult = lavu_query($getSnapshotsQuery, $_GET['order_id'], $locationid);
        $snapShotsForOrderIDArr = array();
        while($currActionLogRow = mysqli_fetch_assoc($snapsForOrderIDResult)){
            $snapShotsForOrderIDArr[] = $currActionLogRow;
        }
        
        $javascriptus  = "<script type='text/javascript'>";
        $javascriptus .= "function navigateToSnapshot(action_log_id){";
        //$javascriptus .= "    alert('hi');";
        $javascriptus .= "    if(action_log_id == -1 ) { window.location = '?show_page=reports/order_details&order_id=$current_orderID'; }";
        $javascriptus .= "    else{";
        $javascriptus .= "      var __loc = '?show_page=reports/order_details&order_id=$current_orderID&load_snapshot=' + action_log_id;";
        $javascriptus .= "      window.location = __loc; }";
        $javascriptus .= "}";
        $javascriptus .= "</script>";
        
        $htmlReturn  = '';
        $htmlReturn .= $javascriptus;
        $htmlReturn .= "<select onchange='navigateToSnapshot(this.value)'>";
        $htmlReturn .= "<option id='-1' value='-1'>Current</option>";
        $i__ = 0;
        foreach($snapShotsForOrderIDArr as $curr){
            $time = $curr['time'];
            $action = $curr['action'];
            $id__ = $curr['id'];
            $displayVal = '';
            if(preg_match('/^Order_Snapshot.*$/', $action)){
                $displayVal = 'Order Snapshot ' . $time;
            }
            else{
                $displayVal = '-----' . substr($action, 0, 30);
            }
            $disabled = $curr['action'] == 'Order_Snapshot' ? '' : 'disabled';
            $htmlReturn .= "<option id='$id__' value='$id__' $disabled";
            if($id__==$current_snapshot) $htmlReturn .= " selected";
            $htmlReturn .= ">" . $displayVal . "</option>";
        }
        $htmlReturn .= "</select>";
        return $htmlReturn;
    }
    
	if(!admin_loggedin())
	{
		echo "You are no longer logged in";
	}
	else if(isset($_GET['order_id']))
	{
		$rdb = admin_info("database");
		$dbname = $_GET['datanameGrp'];
		lavu_connect_byid(admin_info("companyid"),$rdb);
		
		$order_id =  $_GET['order_id'];
		$locationid = sessvar("locationid");

		$item_list = array();
		$display = "";
		$order_read = array();
		$pnr_details_t_read;//Don't set here.
		$load_snapshot = (isset($_GET['load_snapshot']))?$_GET['load_snapshot']:"";
		if($load_snapshot!="")
		{
		    //Parse the details from the action=Order_Snapshot
		    $getSnapshotResult = lavu_query("SELECT `details` FROM `action_log` WHERE `id`='[1]'", $load_snapshot);
		    
		    //parse the details.
		    $currActionLogRow = mysqli_fetch_assoc($getSnapshotResult);
		    $snapshot_details_str = $currActionLogRow['details'];
		    $snapshot_details_arr = explode('[^.^]', $snapshot_details_str);
		    $details_arr = array();
		    foreach($snapshot_details_arr as $currDelimitedList){
		        $parts__ = explode('<^-^>', $currDelimitedList);
		        $currKey = $parts__[0];
		        $currVal = $parts__[1];
    		    $details_arr[$currKey] = $currVal;
		    }
		    
		    /*
		    //From jc_functions
		  $i_vars['subtotal_with_mods'] = (decimalize($order_item[2]) * (decimalize($order_item[1]) + decimalize($order_item[6]) + decimalize($order_item[15])));
		  
		   MAPPING BETWEEN ARRAYS
		   2 quantity -> 18
		   1 Price -> 39
		   6 Modify Price -> 42
		   15 Forced modifier Price -> 50
		   
		   //We need to look at the decimalize() function and see what it does.
		   */   
		    $check_info_arr   = explode('|*|', $details_arr['check_info']);//index meanings inside jc_functions.php
		    $item_details_embedded_arr = explode('|*|', $details_arr['item_details']); 
		    $pnr_details_arr = explode('|*|', $details_arr['pnr_details']);
		    
		                        
		    //echo "Quantity:      " . $item_details_arr[18] . "<br>";
		    //echo "Price:         " . $item_details_arr[39] . "<br>";
		    //echo "Modify Price:  " . $item_details_arr[42] . "<br>";
		    //echo "Forced Mod Pr: " . $item_details_arr[50] . "<br>";
		    //echo "itemDetails Size: " . count($item_details_arr);
		    //Details has been parsed, but we still have to fill the array from the delimited list.
		    foreach($item_details_embedded_arr as $item_details_arr_str){
		    
		        $item_details_arr = explode('|o|', $item_details_arr_str);
    		    $subTotalWithMods = 
    		      str_replace(",", ".", (string)$item_details_arr[18]) *
    		      str_replace(",", ".", (string)$item_details_arr[39]) + 			
    		      str_replace(",", ".", (string)$item_details_arr[42]) + 
    		      str_replace(",", ".", (string)$item_details_arr[50]);    
                $item_list_entr[] = array();
                $item_list_entr['idiscount_amount'] = $item_details_arr[0];
                $item_list_entr['item'] = $item_details_arr[38];
                $item_list_entr['options'] = '';
                $item_list_entr['special'] = $item_details_arr[40];
                $item_list_entr['notes'] = '';
                $item_list_entr['subtotal_with_mods'] = $subTotalWithMods;
                $item_list_entr['quantity'] = $item_details_arr[18];
                $item_list_entr['idiscount_sh'] = '';
                $item_list[] = $item_list_entr;
            }
		    /*
		    $get_items = lavu_query("SELECT * FROM `order_contents` WHERE `loc_id` = '[1]' AND `order_id` = '[2]' AND `item` != 'SENDPOINT' AND (`quantity` * 1) > 0 ORDER BY id ASC", $locationid, $order_id);
    		while($extract_i = mysqli_fetch_assoc($get_items))
    		{
        		$item_list[] = $extract_i;
    		}
    		*/
		    /*
		    $item_list[] = array("idiscount_amount"=>0,"item"=>"Bike","options"=>"yipee","special"=>"","notes"=>"","subtotal_with_mods"=>12,"quantity"=>1,"idiscount_amount"=>0,"idiscount_sh"=>"");
		    $item_list[] = array("idiscount_amount"=>0,"item"=>"Bike monster","options"=>"yipeedooda","special"=>"","notes"=>"","subtotal_with_mods"=>12,"quantity"=>1,"idiscount_amount"=>0,"idiscount_sh"=>"");
		    */
		    $order_read['subtotal'] = $check_info_arr[12];
		    $order_read['discount'] = $check_info_arr[1];
		    $order_read['tax'] = $check_info_arr[13];
		    $order_read['gratuity'] = $check_info_arr[3];
		    $order_read['gratuity_percent'] = $check_info_arr[19];
		    $order_read['total'] = $check_info_arr[14];
		    $order_read['tablename'] = "";
		    $order_read['server'] = $check_info_arr[24];
		    $order_read['location_id'] = $locationid;
		    
		    //
		    $pnr_details_t_read = array();
		    //Now we load all the payments in.
		    foreach($pnr_details_arr as $curr_pnr_detail){
    		    $curr_pnr_detail_arr = explode('|o|',$curr_pnr_detail);
    		    $passing_pnr = array();
    		    $passing_pnr['total_collected'] = $curr_pnr_detail_arr[3];
    		    $passing_pnr['action'] = $curr_pnr_detail_arr[6];
    		    $passing_pnr['pay_type'] = $curr_pnr_detail_arr[4];
    		    $passing_pnr['for_deposit'] = $curr_pnr_detail_arr[19];
    		    $passing_pnr['datetime'] = $curr_pnr_detail_arr[5];
    		    $passing_pnr['card_type'] = $curr_pnr_detail_arr[20];
    		    $passing_pnr['card_desc'] = $curr_pnr_detail_arr[16];
    		    $passing_pnr['tip_amount'] = $curr_pnr_detail_arr[21];
    		    $passing_pnr['signature'] = "";//$curr_pnr_detail_arr[];
    		    $passing_pnr['order_id'] = $curr_pnr_detail_arr[0];
    		    $passing_pnr['check'] = $curr_pnr_detail_arr[1];
    		    $passing_pnr['preauth_id'] = $curr_pnr_detail_arr[28];
    		    $passing_pnr['auth_code'] = $curr_pnr_detail_arr[29];
    		    $passing_pnr['id'] = "";//$curr_pnr_detail_arr[];
    		    $passing_pnr['transaction_id'] = $curr_pnr_detail_arr[27];
    		    $passing_pnr['amount'] = $curr_pnr_detail_arr[2];
    		    $passing_pnr['location_id'] = $locationid;// $curr_pnr_detail_arr[];

    		    $pnr_details_t_read[] = $passing_pnr;
		    }
		}
		else
		{
    		$get_items = lavu_query("SELECT * FROM ".$dbname.".`order_contents` WHERE `loc_id` = '[1]' AND `order_id` = '[2]' AND `item` != 'SENDPOINT' AND (`quantity` * 1) > 0 ORDER BY id ASC", $locationid, $order_id);
    		while($extract_i = mysqli_fetch_assoc($get_items))
    		{
        		$item_list[] = $extract_i;
    		}
			$order_query = lavu_query("select * from ".$dbname.".`orders` WHERE `location_id` = '[1]' AND `order_id` = '[2]'", $locationid, $order_id);
			if(mysqli_num_rows($order_query))
			{
    			$order_read = mysqli_fetch_assoc($order_query);
			}
        }
		
		
		//if (@mysqli_num_rows($get_items) > 0) {
		if(true) {
			//$rows_i = mysqli_num_rows($get_items);
			$rows_i = count($item_list);
			$row_i = 1;
			$bottom_border_i = " border-bottom:1px solid #999999;";
			//while ($extract_i = mysqli_fetch_assoc($get_items)) {
			for($n=0; $n<count($item_list); $n++)
			{
			    $extract_i = $item_list[$n];
				if ($row_i == $rows_i) {
					$bottom_border_i = "";
				}
				$use_bottom_border = $bottom_border_i;
				if($extract_i['idiscount_amount'] > 0) $use_bottom_border = "";
				$display .= "<tr class='order_contents'><td align='left' valign='top' width='200px' style='font-size:12px; padding:2px 4px 2px 2px;".$use_bottom_border."'>".$extract_i['item'];
				if ($extract_i['options'] != "") {
					$display .= "<br /><i>".$extract_i['options']."</i>";
				}
				if ($extract_i['special'] != "") {
					$display .= "<br /><span class='special_instructions'><i>".$extract_i['special']."</i></span>";
				}
				if ($extract_i['notes'] != "") {
					$display .= "<br /><span class='special_instructions'><i>".$extract_i['notes']."</i></span>";
				}
				$subtotal_with_mods=$extract_i['subtotal_with_mods'] - $extract_i['itax'];
				$display .= "</td><td align='right' valign='top' style='padding:2px 2px 2px 4px;".$use_bottom_border."'>".display_money($subtotal_with_mods / $extract_i['quantity'])."</td><td align='right' valign='top' width='40px' style='padding:2px 1px 2px 1px;".$use_bottom_border."'>x ".$extract_i['quantity'] . "&nbsp;";
				$display .= "=</td><td align='right' valign='top' width='60px' style='padding:2px 6px 2px 3px;".$use_bottom_border."'>".display_money($subtotal_with_mods)."</td></tr>";
				if ($extract_i['idiscount_amount'] > 0)
					$display .= "<tr><td colspan='3' align='right' valign='top' style='padding:2px 2px 2px 4px;".$bottom_border_i."'><span style='font-size:11px;'>Item Discount:</span> ".$extract_i['idiscount_sh']."</td><td align='right' valign='top' style='padding:2px 6px 2px 3px; font-size:13px;".$bottom_border_i."'>- ".display_money($extract_i['idiscount_amount'])."</td></tr>";
				$row_i++;
			}
			$display .= "<tr><td align='right' colspan='4'>";
			
			//$order_query = lavu_query("select * from `orders` WHERE `location_id` = '[1]' AND `order_id` = '[2]'", $locationid, $order_id);
			//if(mysqli_num_rows($order_query))
			//{
			//		$order_read = mysqli_fetch_assoc($order_query);
			if(isset($order_read))
			{	
			    $subtotal=$order_read['subtotal'] - $order_read['itax'];
			    $tax=$order_read['tax'] + $order_read['itax'];

				$display .= "<table cellspacing=0 cellpadding=3>";
				$display .= "<tr><td align='right'>Subtotal: </td><td align='right'><b>".display_money($subtotal)."</b></td></tr>";
				if($order_read['discount'] > 0)
				{
					$display .= "<tr><td align='right' style='border-bottom:solid 1px black'>Discount: </td><td align='right' style='border-bottom:solid 1px black'>".display_money($order_read['discount'])."</td></tr>";
					$display .= "<tr><td align='right'>After Discount: </td><td align='right'>".display_money($order_read['subtotal'] * 1 - $order_read['discount'] * 1)."</td></tr>";
				}
				$display .= "<tr><td align='right'>Tax: </td><td align='right'>".display_money($tax)."</td></tr>";
				if ($order_read['gratuity'] > 0) {
					$gratuity_label = (empty($location_info['gratuity_label']))?"Gratuity":$location_info['gratuity_label'];
					$display .= "<tr><td align='right'>".$gratuity_label." (".trim(number_format(($order_read['gratuity_percent'] * 100), 3), 0)."%): </td><td align='right'>".display_money($order_read['gratuity'])."</td></tr>";
				}
				$display .= "<tr><td align='right'>Total: </td><td align='right'><b>".display_money($order_read['total'])."</b></td></tr>";
				$display .= "</table>";
				$display .= "</td></tr>";
			}
		} else {
			$display .= "<tr><td align='center'>No order contents found...</td></tr>";
		}
		
		if(isset($_GET['card_receipt']))
		{
			$location_query = lavu_query("select * from locations where id='[1]'",$locationid);
			if(mysqli_num_rows($location_query))
			{
				$location_read = mysqli_fetch_assoc($location_query);
				
				$t_query = lavu_query("select * from ".$dbname.".cc_transactions where `order_id`='[1]' and `id`='[2]'",$_GET['order_id'],$_GET['card_receipt']);
				if(mysqli_num_rows($t_query))
				{
					$t_read = mysqli_fetch_assoc($t_query);
					
					echo "<style>";
					echo "table {font-family:Arial; font-size:12px;} ";
					echo "</style>";
					echo "<center>";
					echo "<table style='width:280px' cellpadding=4>";
					echo "<tr><td>";
					echo "<br>" . $location_read['title'];
					echo "<br>Order #: " . $t_read['order_id'] . " Check: " . $t_read['check'];
					echo "<br>" . $order_read['tablename'];
					echo "<br>Server: " . $order_read['server'];
					echo "<br>" . $t_read['datetime'];
					echo "</td></tr>";
					echo "<tr><td align='center' style='border-top:solid 2px black'>";
					echo "&nbsp;<br>CREDIT CARD SALE";
					echo "</td></tr>";
					echo "<tr><td align='left'>";
					echo "&nbsp;";
					echo "<br>" . $t_read['card_type'] . "..." . $t_read['card_desc'];
					echo "<br>Ref #: " . $t_read['transaction_id'];
					echo "<br>Auth Code: " . $t_read['auth_code'];
					echo "</td></tr>";
					echo "<tr><td align='right'>";
					echo "&nbsp;";
					echo "<table cellpadding=6>";
					echo "<tr><td align='right'>Amount:</td><td align='right'>" . display_money($t_read['amount'], "", false) . "</td></tr>";
					if ((float)$t_read['tip_amount'] != 0) {
						echo "<tr><td align='right'>Tip:</td><td align='right'>".display_money($t_read['tip_amount'], "", false)."</td></tr>";
						echo "<tr><td align='right'>Total:</td><td align='right'>".display_money(($t_read['amount'] + $t_read['tip_amount']), "", false)."</td></tr>";
					} else {
						echo "<tr><td align='right'>Tip:</td><td align='right'>________</td></tr>";
						echo "<tr><td align='right'>Total:</td><td align='right'>________</td></tr>";
					}
					echo "</table>";
					echo "</td></tr>";
					echo "<tr><td align='center'>";
					$signature_basename = $order_read['location_id']."-".$t_read['order_id']."-".$t_read['check']."-".$t_read['preauth_id']."-".$t_read['auth_code'].".jpg";
					$signature_file = "/home/poslavu/public_html/admin/images/".admin_info('dataname')."/signatures/".$signature_basename;
					$signature_url = "/images/".admin_info('dataname')."/signatures/".$signature_basename;
					echo "<img src='$signature_url' border='0' width='280' />";
					echo "<br>Merchant Copy";
					echo "</td></tr>";
					echo "</table>";
					echo "</center>";
				}
			}
		}
		else if(isset($order_read))
		{
			$date_setting = "date_format";
			$location_date_format_query = lavu_query( "SELECT `location`, `setting`, `value` FROM `config` WHERE `setting` = '[1]'",  $date_setting);
			if( !$location_date_format_query || !mysqli_num_rows($location_date_format_query)){
				$date_format = array("value" => 2);
			}else
				$date_format = mysqli_fetch_assoc( $location_date_format_query );

			echo "<table>$display</table><br>";
			
			echo "<table cellpadding=3>";
			
			if(!isset($pnr_details_t_read) || count($pnr_details_t_read) == 0){//IF we read from snapshot then we load this already and don't need the query.
    			$t_query = lavu_query("select * from ".$dbname.".cc_transactions where `loc_id`='[2]' AND `order_id`='[1]' AND (`action` = 'Sale' OR `action` = 'Refund') AND `tip_for_id` = '0' AND `voided` != '1' AND `transtype` != 'Void'",$_GET['order_id'], $locationid);
    			
    			$pnr_details_t_read = array();
    			while($t_read_row = mysqli_fetch_assoc($t_query)){
        			$pnr_details_t_read[] = $t_read_row;
    			}
			}
			
			//while($t_read = mysqli_fetch_assoc($t_query))
			foreach($pnr_details_t_read as $t_read)
			{
				$amount = $t_read['total_collected'];
				if($t_read['action']=="Refund")
					$amount = $amount * -1;
				$pay_type = $t_read['pay_type'];
				if($t_read['for_deposit']==1)
					$pay_type .= " (deposit)";
				echo "<tr>";

				$split_date = explode(" ", $t_read['datetime']);
				$change_format = explode("-", $split_date[0]);
				switch ($date_format['value']){
					case 1:$t_read['datetime'] = $change_format[2]."-".$change_format[1]."-".$change_format[0]." ".$split_date[1];
					break;
					case 2:$t_read['datetime'] = $change_format[1]."-".$change_format[2]."-".$change_format[0]." ".$split_date[1];
					break;
					case 3:$t_read['datetime'] = $change_format[0]."-".$change_format[1]."-".$change_format[2]." ".$split_date[1];
					break;
				}

				echo "<td>" . $t_read['datetime'] . "</td>";
				echo "<td bgcolor='#dddddd' width='120'>" . $pay_type . "</td>";
				echo "<td>" . $t_read['card_type'] . "</td>";
				echo "<td>" . $t_read['card_desc'] . "</td>";
				echo "<td bgcolor='#dddddd' width='60' align='right'>".display_money($amount)."</td>";
				
				echo "</tr>";
				
				if ((float)$t_read['tip_amount'] != 0) echo "<tr><td></td><td>Tip: ".display_money($t_read['tip_amount'])."</td><td colspan=3></td></tr>";
				
				if (substr($pay_type,0,4) == "Card" && $t_read['signature'] == "1") {
					echo "<tr><td colspan=5 align='right'>";
					$storageKey = $t_read['storage_key_signature'];
					$signatureUrl = "";

					if ($storageKey != "") {
						$imageInfo	= getImageInfo($storageKey);
						$imagePath	= $imageInfo['path']."/".$imageInfo['uuid'];
						$getImageUri = json_decode(file_get_contents($imagePath), true);
						$signatureUrl = $getImageUri['uri'];
					} else {
						$signatureBasename = $order_read['location_id']."-".$t_read['order_id']."-".$t_read['check']."-".$t_read['preauth_id']."-".$t_read['auth_code'].".jpg";
						$signatureFile = "/home/poslavu/public_html/admin/images/".admin_info('dataname')."/signatures/".$signatureBasename;
						if (is_file($signatureFile)) {
							$signatureUrl = "/images/".admin_info('dataname')."/signatures/".$signatureBasename;
						}
					}

					if ($signatureUrl != "") {
						echo "<img src='$signatureUrl' border='0' width='240' /> <a href='index.php?show_page=reports/order_details&order_id=$order_id&card_receipt=".$t_read['id']."' target='_blank'>(print card receipt)</a>";
					} else {
						$location_query = lavu_query("select * from locations where id='[1]'",$locationid);
						if (mysqli_num_rows($location_query)) {
							$location_read = mysqli_fetch_assoc($location_query);
							if ($location_read['use_net_path'] == "1" || $location_read['use_net_path'] == "2") {
								if (isset($_GET['load_signature']) && $_GET['load_signature'] == $t_read['id']) {
									//schedule_msync(admin_info('data_name'),$locationid,"upload signature",$signatureBasename);
									schedule_remote_to_local_sync($locationid,"upload signature",$signatureBasename);
									echo "Signature Retrieval Scheduled.  Please check in 5 minutes...";
								} else {
									$sched_query = lavu_query("select * from `config` where `setting`='[1]' and `location`='[2]' and `value`='[3]'","upload signature",$locationid,$signatureBasename);
									if (mysqli_num_rows($sched_query)) {
										echo "Signature Retrieval Scheduled - " . $signatureBasename;
									} else
										echo "<a href='index.php?show_page=reports/order_details&order_id=$order_id&load_signature=".$t_read['id']."' style='color:#000088; font-size:10px'>Retrieve signature from Local Server</a>";
								}
							}
						}
					}
					echo "</td></tr>";
				}
			}
			echo "</table>";
		}
	}
	if (!isset($_GET['card_receipt'])) {
		echo getLoadSnapshotSelectorHTML();
	}
	echo "</center></body>";
?>
