<?php
/*

<input type='text' placeholder="search" id='search_input_id'/>
<input type='button' onclick='search_button_clicked'/>
<br>
*/
$titleFieldsMap = getColumnTitleToFieldMap();
$sortColumn = empty($_REQUEST['sort_by']) || empty($titleFieldsMap[$_REQUEST['sort_by']]) ? "l_name" : $titleFieldsMap[$_REQUEST['sort_by']];
$ascDesc = empty($_REQUEST['order'])  || $_REQUEST['order'] != 'desc' ? "asc" : 'desc';
$customerRows = getAllMedCustomerRows($titleFieldsMap, $sortColumn, $ascDesc);
//$customerRows = sortCustomerArrByTitle($customerRows, $_REQUEST['sort_by'], $_REQUEST['order']);


if(isset($_GET['widget'])){
	header('Content-disposition: attachment; filename='.'customers_'.date('YmdHis').'.txt');
	header ("Content-Type:text/txt");
    echo generateCommaSeperatedListFromData($customerRows, $titleFieldsMap);
    exit;
}

$exportButton = "<br><input type='button' value='Export To Tab Delimited' style='cursor:pointer;' onclick='window.location=\"?widget=customers_export&sort_by=".$sortColumn."&order=".$ascDesc."\"'/><br>";
echo $exportButton;
echo "<br>";
$displayTable = buildTableFromListOfMedCustomerRows($customerRows, $titleFieldsMap);
echo $displayTable;
echo $exportButton;

function getAllMedCustomerRows($titleFieldsMap, $sortColumn, $ascDesc){

    $result = rpt_query("SELECT * FROM `med_customers` ORDER BY `[1]` [2]", $sortColumn, $ascDesc);
    if(!$result){ error_log("MYSQL error in ".__FILE__." -diut- error:" . lavu_dberror() ); }
    echo lavu_dberror();
    $allCustomerRows = array();
    while($currRow = mysqli_fetch_assoc($result)){
        $allCustomerRows[] = $currRow;
    }
    return $allCustomerRows;
}

function getColumnTitleToFieldMap(){
    $result = rpt_query("SELECT * FROM `config` WHERE `setting`='GetCustomerInfo' AND `_deleted`='0' order by `id` ASC");
    $custInfoRows = array();
    while($currRow = mysqli_fetch_assoc($result)){
        $custInfoRows[] = $currRow;
    }
    
    $titleToFieldMap = array();
    foreach($custInfoRows as $currRow){
        $currTitle = str_replace("_", " ", $currRow['value']);
        $titleToFieldMap[$currTitle] = $currRow['value2'];
    }
    return $titleToFieldMap;
}

function sortCustomerArrByTitle(&$customerArr, $title, $sortOrder){
    $titleToFieldMap = getColumnTitleToFieldMap();
    if(empty($title) || empty($titleToFieldMap[$title])) return $customerArr;
    $titleToFieldMap = getColumnTitleToFieldMap();
    $field = $titleToFieldMap[$title];
    $sortedArr = array();
    $i = 0;
    foreach($customerArr as $currRow){
        $prePend = empty($currRow[$field]) ? ' ' : '';
        $key = $prePend . strtolower($currRow[$field] . $i++);
        
        $sortedArr[$key] = $currRow;
    }
    if($sortOrder == 'desc')
        krsort($sortedArr);
    else
        ksort($sortedArr);
    return array_values($sortedArr);
}

function buildTableFromListOfMedCustomerRows($medCustomerRows, $columnTitleToFieldMap){
    $columnTitleToFieldMap = getColumnTitleToFieldMap();
    
    $htmlTable = "".
    "<table cellspacing=0 cellpadding=2>".
      "<tr>";
        foreach($columnTitleToFieldMap as $titleAsKey => $fieldAsValue){
            $sortOrder = $_REQUEST['sort_by'] == $titleAsKey && $_REQUEST['order'] == 'asc' ? 'desc' : 'asc';
            $onclick = "window.location = \"?mode=customers_export&sort_by=$titleAsKey&order=$sortOrder\"";
            $highlight = $_REQUEST['sort_by'] == $titleAsKey ? 'background-color:gray;' : '';
            $htmlTable .= "<td onclick='$onclick' align='center' valign='bottom' bgcolor='#888888' style='color:#ffffff; cursor:pointer;$highlight'>$titleAsKey</td>";
        }
      $htmlTable .= "</tr>";
    foreach($medCustomerRows as $currMedCustomerRow){
        $htmlTable .= "<tr>";
        foreach($columnTitleToFieldMap as $titleAsKey => $fieldAsValue){
            $htmlTable .= "<td valign='top' style='border-left:solid 1px #cccccc; border-right:solid 1px #cccccc; border-bottom:solid 1px #cccccc'>".$currMedCustomerRow[$fieldAsValue]."</td>";
        }
        $htmlTable .= "</tr>";
    }
    //$htmlTable .= "<tr><td colspan='60' style='border-top:solid 1px #cccccc'>&nbsp;</td></tr>";
    $htmlTable .= "</table>";
    return $htmlTable;
}

function generateCommaSeperatedListFromData($medCustomerRows, $columnTitleToFieldMap){
    $strBuilder = '';
    $init = false;
    foreach($columnTitleToFieldMap as $key => $val){
        if($init){$strBuilder .= "\t";} $init=true;
        $key = str_replace("\t", "\\t", $key);
        $key = str_replace("\"", "\\\"", $key);
        $strBuilder .= $key;
    }
    $strBuilder .= "\n";
    
    foreach($medCustomerRows as $currRow){
        $init = false;
        foreach($columnTitleToFieldMap as $key => $val){
            if($init) {$strBuilder .= "\t";} $init = true;
            $currValue = $currRow[$val];
            $currValue = str_replace("\\", "\\\\", $currValue);
            $currValue = str_replace("\t", "\\t",  $currValue);
            $currValue = str_replace("\n", "\\n",  $currValue);
            $strBuilder .= $currValue;
        }
        $strBuilder .= "\n";
    }
    return substr($strBuilder, 0, -1);//Return everything except last \n.
}
