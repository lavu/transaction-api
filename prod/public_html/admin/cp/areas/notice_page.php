
<table cellspacing=0 cellpadding=6 style='border:solid 1px #cccccc'><tr><td align='center'>

<b>* What bug?</b>
<table><tr><td align='left'>
&nbsp;&nbsp;&nbsp;&nbsp;Poslavu 2.0.7 contains a bug that causes Poslavu to close when 
<br>checking out immediately after sending to the kitchen, after which 
<br>the user must first touch the Poslavu icon, then reopen the order 
<br>through "open orders" to continue.  This bug is generally only
<br>noticeable to restaurants using "Quick Serve" mode, as table service
<br>restaurants do not usually checkout directly after sending items to 
<br>the kitchen.  Please note that 2.0.8 does contain a fix for this bug, 
<br>has already been submitted to the App Store for approval, and will be
<br />released as soon as possible.
</td></tr></table>

<br><br>

<b>* Why release 2.0.7 when it has a bug?</b>
<table><tr><td align='left'>
&nbsp;&nbsp;&nbsp;&nbsp;The new Apple IOS 6 is expected to be released on Sep 19, and
<br>after testing POSLavu version 2.06 with Apple IOS 6, it has been
<br>determined that they are not compatible with each-other.  Because updating to 
<br>IOS 6 would render POSLavu versions 2.0.6 and earlier inoperable, it is important 
<br>that POSLavu version 2.0.7 be released before the IOS 6 release date of Sep 19.
</td></tr></table>

<br><br>

<b>* What should I do?</b>
<table>
	<tr><td align='left'>
    	1) Please refrain from updating your iPads and iPod touches to IOS 6 until POSLavu 2.0.8 has been released
	</td></tr>
    <tr><td align='left'>
   		2) Please do not update to POSLavu 2.0.7 unless your iPads or iPod touches are already running IOS 6
    </td></tr>
</table>

<br><br>

<b>* What should I do if I already updated my App to 2.0.7?</b>
<table>
	<tr><td align='left'>
    	&nbsp;&nbsp;&nbsp;&nbsp;There are a couple ways to minimize the effect of the bug that occurs when checking out directly after
        <br />sending to the kitchen.  One method is to use the "Auto send at checkout" feature, which can be enabled through the
        <br />advanced location settings.  This way, the kitchen order will be automatically sent when you hit the checkout button, and
        <br />it will not be necesssary to hit the "send" button.  This will allow you to checkout and send to the kitchen at the same time
        <br />without encountering the bug.  Another less attractive method is to exit the order and go back in after sending to the kitchen,
        <br />which will also suppress the bug.
    </td></tr>
</table>

</td></tr></table>