<?php

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////
	// check if we're authenticated
	if(!$in_lavu)
		return;

	// check if we're supposed to load a user's schedule
	$user_schedule = urlvar("user_schedule","");
	if($user_schedule!="")
	{
		echo "<br><br><br>";
		require_once(dirname(__FILE__) . "/scheduling.php");
		return;
	}
	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////

	// echo "<br><br><br>";

	// initialize some variables
	$tablename = "users";
	$forward_to = "index.php?mode={$section}_{$mode}";
	if( isset($_GET['rowid'] ) ){
		CPActionTracker::track_action('settings/edit_users', 'edit_user');
		// $forward_to .= '&rowid=' . $_GET['rowid'];
	}
	else if( isset($_GET['addnew'])) {
		CPActionTracker::track_action('settings/edit_users', 'add_user');
	}
	else{
		CPActionTracker::track_action('settings/edit_users', 'list_users');
	}

	$allow_order_by = true;

	// choose how to order the users (eg, by last name)
	// I have no idea where this is used
	$order_by = urlvar("ob");
	if (!$order_by && sessvar_isset("users_order_by")) $order_by = sessvar("users_order_by");
	else if (!$order_by) $order_by = "l_name";
	set_sessvar("users_order_by", $order_by);

	// order ascending or decending order?
	// I have no idea where this is used
	$order_dir = urlvar("od");
	if (!$order_dir && sessvar_isset("users_order_dir")) $order_dir = sessvar("users_order_dir");
	else if (!$order_dir) $order_dir = "asc";
	set_sessvar("users_order_dir", $order_dir);

	// user levels and who has access to the reports
	$access_array = array();
	$access_array['0'] = "0";
	$access_array['1'] = "1";
	$access_array['2'] = "2";
	$access_array['3'] = "3";
	if (admin_info("access_level") == 4) {
		$access_array['4'] = "4";
	}

	//$report_access_array= array("None","1 week","2 week", "3 week", "All"); // future implemenetaition
	$report_access_array= array();
	$report_access_array['0'] = "";
	$report_access_array['1'] = "None";
	$report_access_array['2'] = "All";


	// set reporting group for level 3 users
	$a_reporting_groups = array();
	$a_reporting_groups['0'] = "Current Location";

	if (admin_info("access_level") == 4) {

		require_once('/home/poslavu/public_html/admin/sa_cp/manage_chain_functions.php');

		// get the chain for the dataname
		$s_dataname = admin_info('dataname');
		$a_restaurant_chain = get_restaurant_chain($s_dataname);
		$b_part_of_chain = (count($a_restaurant_chain) !== 0) ? TRUE : FALSE;
		if ($b_part_of_chain) {

			// get the chain reporting groups
			$i_chain_id = (int)$a_restaurant_chain['id'];
			$a_chain_reporting_groups = get_chain_reporting_groups($i_chain_id, get_restaurants_by_chain($i_chain_id));
			$a_reporting_groups[0] = 'Current Location';
			foreach($a_chain_reporting_groups as $a_chain_reporting_group) {
				$a_reporting_groups[$a_chain_reporting_group['id']] = $a_chain_reporting_group['name'];
			}
		}
	}

	// build table layout list
	$table_layout_list = array("0"=>speak("None"));
	$get_table_setup = lavu_query("SELECT `id`, `title` FROM `tables` WHERE `loc_id` = '[1]' AND `_deleted` = '0' ORDER BY `_order`, `title` ASC", $locationid);
	if (mysqli_num_rows($get_table_setup) > 0) {
		while ($info = mysqli_fetch_assoc($get_table_setup)) {
			$table_layout_list[$info['id']] = $info['title'];
		}
	}

	// get the locations associated with the account
	$locations_array = array();
	$locations_array['0'] = speak("All Locations");
	$get_locations = lavu_query("SELECT `id`, `title` FROM `locations` WHERE `_disabled` != '1' ORDER BY `title` ASC");
	while($loc_info = mysqli_fetch_assoc($get_locations)) {
		$locations_array[$loc_info['id']] = $loc_info['title'];
	}

	$carrier_list = array("","Verizon","AT&T","T-Mobile","Sprint");
	$carrier_arr = array();
	for($n=0; $n<count($carrier_list); $n++)
		$carrier_arr[$carrier_list[$n]] = $carrier_list[$n];

	$query_arguments = array( 'id' => $_GET['rowid'] );
	$restricted_user = false;
	$access_level = 1;
	$user_id = $_GET['rowid'];
	if( ($query_result = lavu_query("SELECT * FROM `users` WHERE `id`='[id]'", $query_arguments)) !== FALSE ){
		$user_result = mysqli_fetch_assoc($query_result);
		$restricted_user = $user_result['restricted_user'] == '1';
		$access_level = $user_result['access_level'] *1;
	}

	$available_languages = array();
	$available_languages['-1']	= speak("Location Default");
	$available_languages['0']	= speak("Custom");
	$get_language_packs = mlavu_query("SELECT `id`, `language` FROM `poslavu_MAIN_db`.`language_packs` ORDER BY `language` ASC");
	while ($lpack = mysqli_fetch_assoc($get_language_packs))
	{
		$available_languages[(string)$lpack['id']] = $lpack['language'];
	}

	// the user fields to display
	/***************************************************************************
	 *							THE FIRST SET OF FIELDS
	 **************************************************************************/
	$fields = array();
	$fields[] = array(speak("User"),"","seperator");
	$fields[] = array(speak("ID"),"id","readonly","","list:yes");
	$fields[] = array(speak("Active"),"active","bool","","list:yes,defaultvalue:1");
	$fields[] = array(speak("First Name"),"f_name","text","size:30","list:yes");
	$fields[] = array(speak("Last Name"),"l_name","text","size:30","list:yes");
	$fields[] = array(speak("Username"),"username","text","","list:yes");
	$fields[] = array(speak("Password"),"password","create_password","size:8");
	$fields[] = array(speak("Access Level"),"access_level","select",$access_array,"list:yes");

	$fields[] = array(speak("PIN"),"PIN","text","","size:20");
	$fields[] = array(speak("Language pack").":","use_language_pack","select",$available_languages,"list:yes,defaultvalue:".$location_info['use_language_pack']);
	if ($location_info['allow_RFID_clock_punch'] == "1"){
		$fields[] = array(speak("RF ID"),"rf_id","text","","size:20");
	}


	// more user fields
	/***************************************************************************
	 *							THE SECOND SET OF FIELDS
	 **************************************************************************/
	$fields[] = array(speak("Contact Details"),"","seperator");
	$fields[] = array(speak("Address"),"address","text","size:40");
	$fields[] = array(speak("Home Phone"),"phone","text","size:40");
	$fields[] = array(speak("Mobile Phone"),"mobile","text","size:40");
	$fields[] = array(speak("Mobile Carrier"),"mobile_carrier","select",$carrier_arr);
	$fields[] = array(speak("Email Address"),"email","text","size:30");

	// more user fields
	/***************************************************************************
	 *							THE THIRD SET OF FIELDS
	 **************************************************************************/

	$fields[] = array(
		speak("Administration"),
		"",
		"seperator"
	);

	// lavukart specific options, meaning "Job Type" (aka "role_id")
	if (admin_info('access_level') == 4)
	{
		$fields[] = array(speak("Report Access"),"report_access","select",$report_access_array,"list:yes,defaultvalue:2");
	}

	if ($cpcode=="lavukart" && !$modules->hasModule("employees.classes"))
	{
		$job_type_array = array();
		$job_type_array[""] = "";
		$get_job_lists_res = lavu_query("SELECT * FROM roles");
		while ($get_job_lists_row = mysqli_fetch_assoc($get_job_lists_res))
		{
			$job_id = $get_job_lists_row['id'];
			$job_title = $get_job_lists_row['title'];
			$job_type_array[$job_id] = $job_title;
		}//while

		$fields[] = array(
			speak("Job Type"),
			"role_id",
			"select",
			$job_type_array,
			"list:yes"
		);
	}

	// more user fields
	if (count($a_reporting_groups) > 0)
	{
		$fields[] = array(
			speak("Manager Group"),
			"reporting_groups",
			"select",
			$a_reporting_groups
		);
	}

	$fields[] = array(
		speak("Service Type"),
		"service_type",
		"select",
		array(
			'0' => speak("Location Default"),
			'2' => speak("Quick Serve"),
			'3' => speak("Tables"),
			'1' => speak("Tabs"),
			'4' => speak("Order List")
		),
		"list:yes"
	);

	if (count($table_layout_list) > 2)
	{
		$fields[] = array(
			speak("Default Table Layout"),
			"default_table_layout",
			"select",
			$table_layout_list
		);
	}

	if (admin_info("access_level") == 4)
	{
		$fields[] = array(
			speak("Location"),
			"loc_id",
			"select",
			$locations_array
		);
	}

	// show employee classes, if the account has access to the feature
	if ($modules->hasModule("employees.classes"))
	{
		$all_classes = array();
		$selectable_classes = array();
		$selectable_classes[''] = "";
		$class_query = lavu_query("SELECT * FROM `emp_classes` ORDER BY `title` ASC");
		while ($class_read = mysqli_fetch_assoc($class_query))
		{
			$all_classes[$class_read['id']] = $class_read['title'];
			if ($class_read['_deleted'] != "1") $selectable_classes[$class_read['id']] = $class_read['title'];
		}//while

		$fields[] = array(speak("Classes"),"role_id","employee_classes",$selectable_classes,"list:yes",$all_classes);
	}

	// show emplyee payrates, if the account has access to employee pay rates
	if($modules->hasModule("employees.payrates"))
		$fields[] = array(speak("Pay Rate"),"payrate","number","","list:yes");

	$area_titles = array( 'home' => speak('Home') );
	if($comPackage=='lavukart' || $comPackage=='tithe' ){
		$area_titles['menu'] = speak('Products');
	} else {
		$area_titles['menu'] = speak('Menu');
	}
	if( $comPackage == 'lavukart' ) {
		$area_titles['kart'] = speak('Kart');
	} else if ($comPackage=='tithe'){
		$area_titles['ministries'] = speak('Ministries');
	} else {
		$area_titles['inventory'] = speak('Inventory');
		$area_titles['layout'] = speak('Layout');
	}
	$area_titles['reports'] = speak('Reports');
	$area_titles['settings'] = speak('Settings');
	$area_titles['assistance'] = speak('Assistance');
	$area_titles['extensions'] = speak('Extensions');

	if( $access_level >= 3 && !BackendUser::currentBackendUser()->isRestrictedUser() && BackendUser::currentBackendUser()->ID() != $user_id ){
		$fields[] = array(speak("Restricted User"),"restricted_user","bool");
		// if( $restricted_user ){
		$fields[] = array(speak("Area Access Whitelist"),"access_white_list","toggle_list",$area_titles,'');
		// }
	}

	$fields[] = array("","","seperator");
	if (!$isNewControlPanel) {
		$fields[] = array(speak("Schedule"),"user_schedule","browse_button");
	}
	$fields[] = array(speak("Save"),"submit","submit");

	// creates, updates, or removes a user
	// @$action: "remove", "update", or "insert"
	// @$cc: the dataname
	// @$username: the user's username
	// @$email: the user's email address (used for inserts and updates)
	function verify_user_main($action,$cc,$username,$email)
	{
		CPActionTracker::track_action('settings/edit_users', $action.'_user');
		$set_cc = str_replace("'","''",$cc);
		$set_username = str_replace("'","''",$username);
		$rquery = mlavu_query("select * from `poslavu_MAIN_db`.`customer_accounts` where `dataname`='[1]'",$set_cc);
		if(mysqli_num_rows($rquery))
		{
			$rread = mysqli_fetch_assoc($rquery);
			$cid = $rread['id'];
			$verify_query = mlavu_query("select * from `poslavu_MAIN_db`.`customer_accounts` where `username`='[1]'",$set_username);
			if(mysqli_num_rows($verify_query))
			{
				if($action=="remove")
				{
					mlavu_query("delete from `poslavu_MAIN_db`.`customer_accounts` where `dataname`='[1]' and `username`='[2]'",$set_cc,$set_username);
				}
				if ($action=="update") {
					mlavu_query("UPDATE `poslavu_MAIN_db`.`customer_accounts` SET `email` = '[1]' WHERE `username` = '[2]'", $email, $set_username);
				}
			}
			else
			{
				if($action=="insert" || $action=="update")
				{
					$cinfo = array();
					$cinfo['dataname'] = $set_cc;
					$cinfo['restaurant'] = $cid;
					$cinfo['username'] = $set_username;
					$cinfo['email'] = $email;
					mlavu_query("insert into `poslavu_MAIN_db`.`customer_accounts` (`dataname`,`restaurant`,`username`,`email`) values ('[dataname]','[restaurant]','[username]','[email]')",$cinfo);
				}
			}
		}
	}

	// choose only users from this location, or
	// users that don't belong to a location, or
	// users with access level 4, and
	// don't show lavu admins to non-lavu-admin users
	$filter_by = "(`loc_id` = '0' OR `loc_id` = '{$locationid}' OR `access_level` = '4')";
	if (!is_lavu_admin()) $filter_by .= " AND `lavu_admin` != '1'";

	require_once(resource_path() . "/browse.php");

	// to only display reporting group options for level 3 users
	// hides the <tr> containing the reporting_groups if the access level isn't 3
$jsScript = <<<JSSCRIPT
	<script type="text/javascript">
		function setAccessOnChange() {
			var jaccess = $("[id=access_level]");
			if (jaccess.length == 0)
				return;
			jaccess.change(function() {
				showHideReportingGroups();
			});
		}

		function showHideReportingGroups() {
			var jaccess = $("[id=access_level]");
			var value = jaccess.val();
			var jreporting_groups = $("[id=reporting_groups]");
			if (jreporting_groups.length == 0)
				return;
			if (parseInt(value) != 3){
				jreporting_groups.parent().parent().hide();
				$("#reporting_groups").children().removeAttr("selected");
			}
			else {
				jreporting_groups.parent().parent().show();
				var preVal = $("input[name=value_before_reporting_groups]").val();
			    $("#reporting_groups  option[value='" + preVal + "'").attr("selected", true);
			}
		}

		setTimeout(function() {
			setAccessOnChange();
			showHideReportingGroups();
		}, 500);
		setAccessOnChange();
		showHideReportingGroups();
	</script>
JSSCRIPT;
	echo $jsScript;
?>
