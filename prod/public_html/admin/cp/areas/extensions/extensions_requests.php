<?php
if(!isset($_SESSION)) {
	session_start();
}

require_once(__DIR__."/../../resources/core_functions.php");
// Load Zuora lib if it's not the local development env
if(!isLocalhost()) {
	require_once(__DIR__."/../../../../inc/billing/zuora/ZuoraIntegration.php");
}
require_once("extensions_functions.php");

$r_mode		= reqvar("mode", "");
$r_dn		= reqvar("dn", "");
$r_loc_id	= reqvar("loc_id", "");
$ext_id		= reqvar("ext_id", "");
$ext_title	= reqvar("ext_title", "");
$ext_word	= reqvar("ext_codeword", "");
$module_id	= reqvar("module_id", "");

$rdb	= admin_info("database");
$dn		= sessvar("admin_dataname");

// Load session variables.
// When accessing this file from CP4 without going through index.php,
// there are some session variables that are not set.
// We need to explicitely load them.

// Check that the `locationid` session variable is set
$loc_id = sessvar("locationid");
if (!$loc_id && isNewControlPanelActive()) {
	$loc_id = check_location_id($rdb, sessvar("locationid"));
}

// Check that the `location_info` session variable is set
if (!sessvar('location_info') && isNewControlPanelActive()) {
	$get_location_config = mlavu_query("SELECT `setting`, `value` FROM `poslavu_" . $dn . "_db`.`config` WHERE `type` = 'location_config_setting' AND `location` = '[1]'", $loc_id);
	while ($config_info = mysqli_fetch_assoc($get_location_config)) {
		$location_info[$config_info['setting']] = $config_info['value'];
	}

	set_sessvar("location_info", $location_info);
}

$ext_response = array();
$ext_response['ext_title'] = $ext_title;
$ext_response['ext_codeword'] = $ext_word;

if ($rdb && $dn && $loc_id && $dn==$r_dn && $loc_id==$r_loc_id)
{
	switch ($r_mode)
	{
		case "load_details":

			loadExtensionDetails($ext_id, $ext_response, $_REQUEST['enabled']);
			break;

		case "check_prerequisites":

			checkPrerequisites($ext_id, $ext_response);
			break;

		case "enable_extension":

			enableDisableExtension($ext_id, $ext_response, TRUE);
			break;

		case "disable_extension":

			enableDisableExtension($ext_id, $ext_response, FALSE);
			break;
		
		case "enable_module":

			addRemoveModule($module_id, $ext_id, $ext_response, TRUE);

		case "disable_module":

			addRemoveModule($module_id, $ext_id, $ext_response, FALSE);

		default:

			$ext_response['status'] = "unknown_mode";
			break;
	}
}
else
{
	$ext_response['status'] = "expired_session";
}

$send_response = json_encode($ext_response);

//error_log($send_response);

echo $send_response;

function loadExtensionDetails($ext_id, &$resp, $enabled)
{
	$get_details = mlavu_query("SELECT `description`".(($enabled == "1")?", `enabled_description`":"")." FROM `poslavu_MAIN_db`.`extensions` WHERE `id` = '[1]'", $ext_id);
	if (!$get_details)
	{
		db_error($resp);
		return;
	}

	if (mysqli_num_rows($get_details) == 0)
	{
		$resp['status'] = "no_ext";
		return;
	}

	$info = mysqli_fetch_assoc($get_details);
	if ($enabled == "1")
	{
		if (!empty($info['enabled_description']))
		{
			$info['description'] = $info['enabled_description'];
		}

		unset($info['enabled_description']);

		$info['description'] = performEnabledStringReplacements($ext_id, $info['description']);
	}

	$resp['info']	= $info;
	$resp['status']	= "success";
}

function performEnabledStringReplacements($ext_id, $desc)
{
	switch ($ext_id)
	{
		case "3":

			$lt_settings = getSavedLoyalTreeSettings();
			$desc = str_replace(array("[LOYALTREE_ID]", "[LOYALTREE_CHAIN_ID]"), array($lt_settings['loyaltree_id'], $lt_settings['loyaltree_chain_id']), $desc);

			break;

		default:
			break;
	}

	return $desc;
}

function checkPrerequisites($ext_id, &$resp)
{
	$get_details = mlavu_query("SELECT `title`, `prerequisites` FROM `poslavu_MAIN_db`.`extensions` WHERE `id` = '[1]'", $ext_id);
	if (!$get_details)
	{
		db_error($resp);
		return;
	}

	if (mysqli_num_rows($get_details) == 0)
	{
		$resp['status'] = "no_ext";
		return;
	}

	$info = mysqli_fetch_assoc($get_details);

	$resp['info'] = array(
		'pr_message'	=> "",
		'title'			=> $info['title']
	);
	$resp['status'] = "success";

	if ($info['prerequisites'] == "")
	{
		return;
	}

	$prerequisites = json_decode(json_encode($info['prerequisites']));
	foreach ($prerequisites[0] as $key => $val)
	{
		if ($key=="gateway" && checkLocationGateway()!=$val)
		{
			$resp['info']['pr_message'] = "Sorry, but the ".$info['title']." extension is currently only available to locations integrated with ".$val.".";
			break;

		}
		else if ($key=="module" && !accountHasModule($val))
		{
			break;
		}
		else if ($key=="package_level" && accountPackageLevel()<($val * 1))
		{
			break;
		}
	}
}

function checkLocationGateway()
{
	global $rdb;
	global $loc_id;

	$gateway = "";

	$check_gw = mlavu_query("SELECT `gateway` FROM `".$rdb."`.`locations` WHERE `id` = '[1]'", $loc_id);
	if (!$check_gw)
	{
		return "";
	}

	if (mysqli_num_rows($check_gw) == 0)
	{
		return "";
	}

	$info = mysqli_fetch_assoc($check_gw);

	return $info['gateway'];
}

function accountHasModule($module)
{
	return TRUE;
}

function accountPackageLevel()
{
	return 3;
}

function enableDisableExtension($ext_id, &$resp, $enable)
{
	$resp['info'] = array(
		'description'	=> "",
		'error'			=> ""
	);
	$resp['status'] = "success";

	$ext_codeword = strtolower(trim($resp['ext_codeword']));

	switch ($ext_codeword) {
		case "mercury_paypal":

			enableDisableMercuryPayPal($ext_id, $resp, $enable);
			break;

		case "mercury_wells_fargo":

			enableDisableMercuryWellsFargo($ext_id, $resp, $enable);
			break;

		case "loyaltree":

			enableDisableLoyalTree($ext_id, $resp, $enable);
			break;

		case "customer_management":

			enableDisableCustomerManagement($ext_id, "Customer Management", $resp, $enable);
			break;

		case "moneris":

			enableDisableMoneris($ext_id, $resp, $enable);
			break;

		case "lavu_gift":

			enableDisableLavuGiftOrLoyalty($ext_id, "gift", $resp, $enable);
			break;

		case "paypal":

			enableDisablePayPal($ext_id, $resp, $enable);
			break;

		case "lavu_loyalty":

			enableDisableLavuGiftOrLoyalty($ext_id, "loyalty", $resp, $enable);
			break;

		case "loyaltree_white_label":

			enableDisableLoyalTreeWhiteLabel($ext_id, $resp, $enable);
			break;

		case "kds":

			enableDisableKds($ext_id, $resp, $enable);
			break;

		case "econduit":

			enableDisableeConduit($ext_id, $resp, $enable);
			break;

		case "pepper":

			enableDisablePepper($ext_id, $resp, $enable);
			break;

		case "square":
			
			enableDisableSquare($ext_id, $resp, $enable);
			break;

		case "sacoa_playcard":

			enableDisableSacoa($ext_id, $resp, $enable);
			break;

		case "lavu_kiosk":
			enableDisableKiosk($ext_id, $resp, $enable);
			break;

		case "lavutogo":
			enableDisableLavuToGo($ext_id, $resp, $enable);
			break;

		case "dtt":
			enableDisableDtt($ext_id, $resp, $enable);
			break;

		case "lavupay":
			enableDisableLavuPay($ext_id, $resp, $enable);
			break;

		default:

			$resp['info']['error'] = "Unable to enable extension: ".$resp['ext_title'] .".";
			break;
	}
}

function addRemovePaymentMethod($type, $special, &$resp, $enable)
{
	global $rdb;
	global $loc_id;

	$ptt = "`".$rdb."`.`payment_types`";
	$check_payment_types = mlavu_query("SELECT `id`, `special`, `_deleted` FROM ".$ptt." WHERE `loc_id` = '[1]' ORDER BY `_deleted` ASC", $loc_id);
	if (!$check_payment_types)
	{
		$resp['more_info'] = "payment_type_check_failed";
		return;
	}

	$has_payment_type = FALSE;
	if (mysqli_num_rows($check_payment_types) > 0)
	{
		while ($info = mysqli_fetch_assoc($check_payment_types))
		{
			if ($info['special'] == $special)
			{
				$update_it = mlavu_query("UPDATE ".$ptt." SET `_deleted` = '[1]' WHERE `id` = '[2]'", $enable?"0":"1", $info['id']);
				$has_payment_type = TRUE;

				if ($enable)
				{
					$resp['more_info'] = "payment_type_exists";
				}
				else
				{
					$resp['more_info'] = $update_it?"payment_type_deleted":"payment_type_not_deleted";
				}

				break;
			}
		}
	}

	if (!$has_payment_type && $enable)
	{
		$create_payment_type = mlavu_query("INSERT INTO ".$ptt." (`loc_id`, `type`, `special`) VALUES ('[1]', '[2]', '[3]')", $loc_id, $type, $special);
		$resp['more_info'] = $create_payment_type?"payment_type_created":"payment_type_not_created";
	}

	trigger_lls_sync("payment_types");
}

function addRemoveDiscountType($title, $special, &$resp, $enable)
{
	global $rdb;
	global $loc_id;

	$dtt = "`".$rdb."`.`discount_types`";
	$check_discount_types = mlavu_query("SELECT `id`, `title`, `special`, `_deleted` FROM ".$dtt." WHERE `loc_id` = '[1]' ORDER BY `_deleted` ASC", $loc_id);
	if (!$check_discount_types)
	{
		$resp['more_info'] = "payment_type_check_failed";
		return;
	}

	$has_discount_type = FALSE;
	if (mysqli_num_rows($check_discount_types) > 0)
	{
		while ($info = mysqli_fetch_assoc($check_discount_types))
		{
			if ($info['special'] == $special)
			{
				$update_it = mlavu_query("UPDATE ".$dtt." SET `_deleted` = '[1]' WHERE `id` = '[3]'", $enable?"0":"1", $special, $info['id']);
				$has_discount_type = TRUE;

				if ($enable)
				{
					$resp['more_info'] = "discount_type_exists";
				}
				else
				{
					$resp['more_info'] = $update_it?"discount_type_deleted":"discount_type_not_deleted";
				}

				break;
			}
		}
	}

	if (!$has_discount_type && $enable)
	{
		$create_pdiscount_type = mlavu_query("INSERT INTO ".$dtt." (`loc_id`, `title`, `label`, `code`, `special`) VALUES ('[1]', '[2]', '[2]', 'p', '[3]')", $loc_id, $title, $special);
		$resp['more_info'] = $create_payment_type?"discount_type_created":"discount_type_not_created";
	}
}

function modifyLocationSettings($config_set)
{
	global $rdb;
	global $loc_id;

	$sync_config = FALSE;
	$sync_locations = FALSE;

	foreach ($config_set as $cset)
	{
		$type = $cset[0];
		$setting = $cset[1];
		$value = $cset[2];
		$force = $cset[3];
		$delete = isset($cset[4]) ? $cset[4] : '';

		if ($type == "config")
		{
			$ct = "`".$rdb."`.`config`";
			$check_setting = mlavu_query("SELECT `id` FROM ".$ct." WHERE `location` = '[1]' AND `type` = 'location_config_setting' AND `setting`= '[2]'", $loc_id, $setting);
			if ($check_setting)
			{
				if ($delete)
				{
					if (mysqli_num_rows($check_setting) > 0)
					{
						$info = mysqli_fetch_assoc($check_setting);
						$update_setting = mlavu_query("UPDATE ".$ct." SET `_deleted` = '1' WHERE `id` = '[id]'", $info);
					}
				}
				else if (mysqli_num_rows($check_setting) > 0)
				{
					$info = mysqli_fetch_assoc($check_setting);
					if ($force)
					{
						$update_setting = mlavu_query("UPDATE ".$ct." SET `value` = '[1]', `_deleted` = '0'  WHERE `id` = '[2]'", $value, $info['id']);
					}
				}
				else
				{
					$create_setting = mlavu_query("INSERT INTO ".$ct." (`location`, `type`, `setting`, `value`) VALUES ('[1]', 'location_config_setting', '[2]', '[3]')", $loc_id, $setting, $value);
				}
			}

			$sync_config = TRUE;
		}
		else if ($type == "location")
		{
			// perform update to locations table if force is true ???

			$sync_locations = TRUE;
		}
	}

	$update_devices = mlavu_query("UPDATE `".$rdb."`.`devices` SET `needs_reload` = '1' WHERE `loc_id` = '[1]'", $loc_id);

	if ($sync_config)
	{
		trigger_lls_sync("config");
	}

	if ($sync_locations)
	{
		trigger_lls_sync("locations");
	}
}

function trigger_lls_sync($table)
{
	$loc_info = sessvar('location_info');
	$loc_id = $loc_info['id'];
	schedule_remote_to_local_sync_if_lls($loc_info, $loc_id, "table updated", $table);
}

// - - - - - - - - Mercury PayPal and Wells Fargo - - - - - - - - //

function enableDisableMercuryPayPal($ext_id, &$resp, $enable)
{
	// first tackle billing changes, then move on to module and payment_type changes

	if (!addRemoveModule("extensions.payment.mercury.paypal", $ext_id, $resp, $enable))
	{
		return;
	}

	addRemovePaymentMethod("PayPal", "payment_extension:31", $resp, $enable);

	$config_set = array(
		array(
			"config",
			"mercury_paypal_transaction_method",
			$enable?"1":"0",
			TRUE
		),
		array(
			"config",
			"mercury_paypal_tab_polling_interval",
			$enable?"0":"0", // keep disabled until further notice - RJG
			TRUE
		)
	);

	modifyLocationSettings($config_set);

	adminActionLogExtensionChange("Mercury PayPal", $enable);

	if ($enable)
	{
		loadExtensionDetails($ext_id, $resp, "1");
	}
}

function enableDisableMercuryWellsFargo($ext_id, &$resp, $enable)
{
	// first tackle billing changes, then move on to module and payment_type changes

	if (!addRemoveModule("extensions.payment.mercury.wellsfargo", $ext_id, $resp, $enable))
	{
		return;
	}

	addRemovePaymentMethod("QR", "payment_extension:32", $resp, $enable);

	adminActionLogExtensionChange("Mercury Wells Fargo", $enable);

	if ($enable)
	{
		loadExtensionDetails($ext_id, $resp, "1");
	}
}

// - - - - - - - - LoyalTree - - - - - - - - //

function getSavedLoyalTreeSettings()
{
	global $rdb;

	$loc_info = sessvar('location_info');

	$lt_settings = array(
		'loyaltree_chain_id'	=> "",
		'loyaltree_id'			=> ""
	);

	$check_config = mlavu_query("SELECT `setting`, `value` FROM `".$rdb."`.`config` WHERE `type` = 'location_config_setting' AND `setting` LIKE 'loyaltree%' AND `location` = '[1]'", $loc_info['id']);
	if ($check_config)
	{
		if (mysqli_num_rows($check_config) > 0)
		{
			while ($info = mysqli_fetch_assoc($check_config))
			{
				$lt_settings[$info['setting']] = $info['value'];
			}
		}
	}

	return $lt_settings;
}

function enableDisableLoyalTree($ext_id, &$resp, $enable)
{
	global $rdb;
	global $dn;

	$config_set = array();

	if ($enable)
	{
		$company_id	= admin_info("companyid");
		$chain_id	= admin_info("chain_id");
		$loc_info	= sessvar('location_info');

		lavu_connect_byid($company_id, $rdb);

		$loyaltree_id		= ($company_id * 72990);
		$loyaltree_chain_id	= ($chain_id * 72990);

		$lt_settings = getSavedLoyalTreeSettings();

		$old_loyaltree_id		= $lt_settings['loyaltree_id'];
		$old_loyaltree_chain_id	= $lt_settings['loyaltree_chain_id'];

		require_once(resource_path()."/../../../inc/loyalty/loyaltree/bluelabel/LoyalTreeBlueLabel.php");

		$categories	= LoyalTreeBlueLabel::getCats($loc_info['menu_id']);
		$items		= LoyalTreeBlueLabel::getItems($loc_info['menu_id']);

		if (empty($old_loyaltree_id))
		{
			$locations  = LoyalTreeBlueLabel::getLocations($company_id);
			$xml = LoyalTreeBlueLabel::signup($loyaltree_id, $loyaltree_chain_id, admin_info("company_name"), $locations, $categories, $items, FALSE);

			if (!isset($xml->success))
			{
				$resp['status'] = "msg:LoyalTree signup failed";
				if (isset($xml->error))
				{
					 $resp['status'] .= ": ".$xml->error;
				}
				return;
			}
		}
		else
		{
			if ($loyaltree_chain_id != $old_loyaltree_chain_id)
			{
				// tell LoyalTree about the change?
			}

			$data = array(
				'businessid'	=> $loyaltree_id,
				'chain_id'		=> $loyaltree_chain_id,
				'storeid'		=> $company_id.$loc_info['id']
			);

			$xml = LoyalTreeBlueLabel::preload($data, $categories, $items);
		}

		$config_set = array_merge( $config_set, array(
			array(
				"config",
				"loyaltree_chain_id",
				$loyaltree_chain_id,
				TRUE
			),
			array(
				"config",
				"loyaltree_id",
				$loyaltree_id,
				TRUE
			)
		));
	}

	if (!addRemoveModule("extensions.loyalty.loyaltree", $ext_id, $resp, $enable))
	{
		return;
	}

	addRemoveDiscountType("LoyalTree", "loyaltree", $resp, $enable);

	$config_set[] = array(
		"config",
		"enable_loyaltree",
		$enable?"1":"0",
		TRUE
	);

	modifyLocationSettings($config_set);

	adminActionLogExtensionChange("LoyalTree", $enable);

	if ($enable)
	{
		loadExtensionDetails($ext_id, $resp, "1");
	}
}

// - - - - - - - - LoyalTree White Label - - - - - - - - //

function getSavedLoyalTreeWhiteLabelSettings()
{
	global $rdb;

	$loc_info = sessvar('location_info');

	$lt_settings = array(
		'enable_loyaltree'	 => '',
		'loyaltree_id'       => '',
		'loyaltree_username' => '',
		'loyaltree_password' => '',
	);

	$check_config = mlavu_query("SELECT `setting`, `value` FROM `".$rdb."`.`config` WHERE `type` = 'location_config_setting' AND `setting` LIKE '%loyaltree%' AND `location` = '[1]' AND `_deleted`='0'", $loc_info['id']);
	if ($check_config)
	{
		if (mysqli_num_rows($check_config) > 0)
		{
			while ($info = mysqli_fetch_assoc($check_config))
			{
				$lt_settings[$info['setting']] = $info['value'];
			}
		}
	}

	return $lt_settings;
}

function getSavedPepperSettings()
{
	global $rdb;

	$loc_info = sessvar('location_info');

	$lt_settings = array(
		'pepper_enabled'	    => '',
		'pepper_applicationid'  => '',
		'pepper_apitoken'       => '',
		'pepper_beta'           => '',
		'pepper_locationid'     => '',
		'pepper_registerid'     => '',
		'pepper_loyalty_points' => '',
		'pepper_loyalty_stamps' => '',
	);

	$check_config = mlavu_query("SELECT `setting`, `value` FROM `".$rdb."`.`config` WHERE `type` = 'location_config_setting' AND `setting` LIKE 'pepper_%' AND `location` = '[1]' AND `_deleted`='0'", $loc_info['id']);
	if ($check_config)
	{
		if (mysqli_num_rows($check_config) > 0)
		{
			while ($info = mysqli_fetch_assoc($check_config))
			{
				$lt_settings[$info['setting']] = $info['value'];
			}
		}
	}

	return $lt_settings;
}

function enableDisableLoyalTreeWhiteLabel($ext_id, &$resp, $enable)
{
	global $rdb;
	global $dn;
	global $loc_id;

	if ($enable)
	{
		require_once(__DIR__.'/../../../../inc/loyalty/loyaltree/LoyalTreeIntegration.php');

		$loyaltree_id          = reqvar('loyaltree_id', '');
		$loyaltree_username    = reqvar('loyaltree_username', '');
		$loyaltree_password    = reqvar('loyaltree_password', '');
		$loyaltree_chain_id    = admin_info('chain_id');
		$loyaltree_app         = LoyalTreeConfig::WHITELABEL_APP;

		// Delete old LoyalTree config rows prior to enabling LoyalTree White Label to save values somewhere instead of just overwriting them
		$lt_settings = getSavedLoyalTreeSettings();
		if (isset($lt_settings['enable_loyaltree']) && $lt_settings['enable_loyaltree'] == '1')
		{
			modifyLocationSettings( array(
				array('config', 'enable_loyaltree', null, false, true),
				array('config', 'loyaltree_id', null, false, true),
				array('config', 'loyaltree_chain_id', null, false, true),
				array('config', 'loyaltree_username', null, false, true),
				array('config', 'loyaltree_password', null, false, true),
			) );  // TO DO : confirm no LoyalTree code pulls config rows without checking _deleted flag
		}

		// Add new config rows first because, if we're enabling, the White Label config rows have to be present before we can perform a signup API call
		modifyLocationSettings( array(
			array('config', 'enable_loyaltree', '1', true),
			array('config', 'loyaltree_id', $loyaltree_id, true),
			array('config', 'loyaltree_chain_id', $loyaltree_chain_id, true),
			array('config', 'loyaltree_username', $loyaltree_username, true),
			array('config', 'loyaltree_password', $loyaltree_password, true),
			array('config', 'loyaltree_app', $loyaltree_app, true),
		) );

		// Enable LoyalTree White Label
		$args = array(
			'action'      => 'signup',
			'dataname'    => $dn,
			'loc_id'      => $loc_id,
			'app'         => $loyaltree_app,
		);

		try
		{
			LoyalTreeIntegration::signup($args);
		}
		catch (Exception $e)
		{
			error_log(__FILE__ ." got exception with args=". json_encode($args) .": ". $e->getMessage());

			$resp['status'] = "msg:LoyalTreeIntegration error - Please contact Lavu Support";
			$resp['info']['error'] = $e->getMessage();
			$resp['info']['description'] = 'LoyalTreeIntegration error';
			return;
		}
	}
	else
	{
		modifyLocationSettings(array(array('config', 'enable_loyaltree', '0', true)));
	}

	if (!addRemoveModule("extensions.loyalty.loyaltree_whitelabel", $ext_id, $resp, $enable))
	{
		return;
	}

	addRemoveDiscountType("LoyalTree", "loyaltree", $resp, $enable);

	adminActionLogExtensionChange("LoyalTreeWhiteLabel", $enable);

	if ($enable)
	{
		loadExtensionDetails($ext_id, $resp, "1");
	}
}

// - - - - - - - - Pepper - - - - - - - - //

function enableDisablePepper($ext_id, &$resp, $enable)
{
	global $rdb;
	global $dn;
	global $loc_id;

	if ($enable)
	{
		$pepper_applicationid  = reqvar('pepper_applicationid', '');
		$pepper_apitoken       = reqvar('pepper_apitoken', '');
		$pepper_apiurl         = reqvar('pepper_apiurl', '');
		$pepper_locationid     = reqvar('pepper_locationid', '');
		$pepper_registerid     = reqvar('pepper_registerid', '');
		$pepper_loyalty_model  = reqvar('pepper_loyalty_model', '');

		// Synthetic values
		$pepper_beta = (strpos(strtolower($pepper_apiurl), 'beta') !== false) ? '1' : '';
		$pepper_loyalty_points = ($pepper_loyalty_model == 'points') ? '1' : '0';
		$pepper_loyalty_stamps = ($pepper_loyalty_model == 'stamps') ? '1' : '0';

		// Delete old LoyalTree config rows prior to enabling LoyalTree White Label to save values somewhere instead of just overwriting them
		$lt_settings = getSavedPepperSettings();
		if (isset($lt_settings['pepper_enabled']) && $lt_settings['pepper_enabled'] == '1')
		{
			modifyLocationSettings( array(
				array('config', 'pepper_enabled', null, false, true),
				array('config', 'pepper_applicationid', null, false, true),
				array('config', 'pepper_apitoken', null, false, true),
				array('config', 'pepper_beta', null, false, true),
				array('config', 'pepper_locationid', null, false, true),
				array('config', 'pepper_registerid', null, false, true),
				array('config', 'pepper_loyalty_points', null, false, true),
				array('config', 'pepper_loyalty_stamps', null, false, true),
				//array('config', 'api_order_auto_send', null, false, true),
				array('config', 'valid_set_order_value_keys', null, false, true),
			) );
		}

		// Add new config rows first because, if we're enabling, the White Label config rows have to be present before we can perform a signup API call
		$location_settings = array();
		$location_settings[] = array('config', 'pepper_enabled', '1', true);
		$location_settings[] = array('config', 'pepper_applicationid', $pepper_applicationid, true);
		$location_settings[] = array('config', 'pepper_apitoken', $pepper_apitoken, true);
		if ($pepper_beta)
		{
			$location_settings[] = array('config', 'pepper_beta', $pepper_beta, true);
		}
		$location_settings[] = array('config', 'pepper_locationid', $pepper_locationid, true);
		$location_settings[] = array('config', 'pepper_registerid', $pepper_registerid, true);
		if ($pepper_loyalty_points)
		{
			$location_settings[] = array('config', 'pepper_loyalty_points', $pepper_loyalty_points, true);
		}
		if ($pepper_loyalty_stamps)
		{
			$location_settings[] = array('config', 'pepper_loyalty_stamps', $pepper_loyalty_stamps, true);
		}
		// Auto-send API orders to the kitchen
		$location_settings[] = array('config', 'api_order_auto_send', '1', true);
		// This setting is needed for the Pay with Points/App webview to save the Pepper order_id in orders.original_id
		$location_settings[] = array('config', 'valid_set_order_value_keys', 'original_id', true);

		modifyLocationSettings($location_settings);
	}
	else
	{
		modifyLocationSettings(array(array('config', 'pepper_enabled', '0', true)));
	}

	if (!addRemoveModule("extensions.loyalty.pepper", $ext_id, $resp, $enable))
	{
		return;
	}

	// Add/remove custom Discount Types
	// We didn't end up needing a custom Discount Type by default

	// Add/remove custom Payment Methods
	addRemovePaymentMethod("App", "payment_extension:113", $resp, $enable);
	if ($pepper_loyalty_points || !$enable) addRemovePaymentMethod("Points", "payment_extension:113", $resp, $enable);

	// Add/remove component_package on locations table
	if ($enable)
	{
		$updated_component_package = mlavu_query("UPDATE `[1]`.`locations` SET `component_package`='62' WHERE `component_package`!='62'", $rdb);
	}
	else
	{
		$updated_component_package = mlavu_query("UPDATE `[1]`.`locations` SET `component_package`='' WHERE `component_package`='62'", $rdb);
	}

	adminActionLogExtensionChange("LavuPremiumLoyalty", $enable);

	if ($enable)
	{
		loadExtensionDetails($ext_id, $resp, "1");
		// TO DO : database changes (?)
	}
}

// - - - - - - - - Moneris - - - - - - - - //

function enableDisableMoneris($ext_id, &$resp, $enable)
{
	if (!addRemoveModule("extensions.payment.moneris", $ext_id, $resp, $enable))
	{
		return;
	}

	addRemovePaymentMethod("MONERIS", "payment_extension:49", $resp, $enable);

	adminActionLogExtensionChange("Moneris", $enable);

	if ($enable)
	{
		loadExtensionDetails($ext_id, $resp, "1");
	}
}

// - - - - - - - - PayPal - - - - - - - - //

// Enable or disable the PayPal extension
function enableDisablePayPal($ext_id, &$resp, $enable, $PPCore=NULL)
{
	global $dn;

	if (!addRemoveModule("extensions.payment.paypal", $ext_id, $resp, $enable))
	{
		return;
	}

	$config_set = array(
		array(
			"config",
			"modules",
			"extensions.payment.+paypal",
			TRUE
		)
	);

	modifyLocationSettings($config_set);

	addRemovePaymentMethod("PayPal", "native_extension:paypal", $resp, $enable);

	adminActionLogExtensionChange("PayPal", $enable);

	if ($enable)
	{
		loadExtensionDetails($ext_id, $resp, "1");
	}
	else
	{
		$result = mlavu_query("UPDATE `poslavu_MAIN_db`.`auth_tokens` SET `token` = '' WHERE `dataname` = '[1]' AND `extension_id` = '[2]'", $dn, $ext_id);
		if (!$result)
		{
			$errorstring = mlavu_dberror();
			error_log("mysql error for token erasure: ".$errorstring);
		}
		else
		{
			error_log("Query result: ".$result);
		}
	}
}

// - - - - - - - - Lavu Gift and Loyalty - - - - - - - - //

function enableDisableLavuGiftOrLoyalty($ext_id, $gift_or_loyalty, &$resp, $enable)
{
	global $rdb;

	if (!addRemoveModule("extensions.payment.lavu.".$gift_or_loyalty, $ext_id, $resp, $enable))
	{
		return;
	}

	$ext_layout_id	= "75";
	$ext_title		= "Lavu Gift";
	$extra2			= "lavugift";
	if ($gift_or_loyalty == "loyalty")
	{
		$ext_layout_id	= "77";
		$ext_title		= speak("Lavu Loyalty");
		$extra2			= "lavuloyalty";
	}

	addRemovePaymentMethod("LAVU ".strtoupper($ext_title), "payment_extension:".$ext_layout_id, $resp, $enable);

	$fml_exist_query = mlavu_query("SELECT * FROM `[1]`.`forced_modifier_lists` WHERE `title` = '[2]' and `type` = 'custom'", $rdb, $ext_title);
	if ((mysqli_num_rows($fml_exist_query) == 0) && $enable)
	{
		$fml_success = mlavu_query("INSERT INTO `[1]`.`forced_modifier_lists` (`title`, `type`, `_deleted`, `menu_id`) VALUES ('[2]', 'custom', '0', '0')", $rdb, $ext_title);
		if ($fml_success)
		{
			$fml_id = mlavu_insert_id();
			if ($fml_id)
			{
				$fm_success = mlavu_query("INSERT INTO `[1]`.`forced_modifiers` (`title`, `list_id`, `extra`, `extra2`, `extra3`, `extra5`) VALUES ('add_mod', '[2]', 'wrapper', '[3]', 'payment_extensions/lavugift', '0x0x0x0|120x154x800x460')", $rdb, $fml_id, $extra2);
			}
		}
	}
	else
	{
		$mod_list = mysqli_fetch_assoc($fml_exist_query);

		$list_id = $mod_list['id'];

		$update_list = mlavu_query("UPDATE `[1]`.`forced_modifier_lists` SET `_deleted` = '[2]' WHERE `id` = '[3]'", $rdb, $enable?"0":"1", $list_id);

		$update_mod = mlavu_query("UPDATE `[1]`.`forced_modifiers` SET `_deleted` = '[2]' WHERE `list_id` = '[3]'", $rdb, $enable?"0":"1", $list_id);
	}

	adminActionLogExtensionChange($ext_title, $enable);

	if ($enable)
	{
		loadExtensionDetails($ext_id, $resp, "1");
	}
}



// - - - - - - - - Customer Management - - - - - - - - //

function enableDisableCustomerManagement($ext_id, $customerManagement, &$resp, $enable) {
    
    if (!addRemoveModule("components.customer", $ext_id, $resp, $enable)) {
        return;
    }
    
    $ext_title  =   $customerManagement; 
    $extra2     = "customerManagement";
    
    adminActionLogExtensionChange($ext_title, $enable);
    
    if ($enable) {
        loadExtensionDetails($ext_id, $resp, "1");
    }
}

// - - - - - - - - KDS Advanced - - - - - - - - //

function enableDisableKds($ext_id, &$resp, $enable)
{
	if (!addRemoveModule("extensions.ordering.kds", $ext_id, $resp, $enable))
	{
		return;
	}

	addRemovePaymentMethod("LAVU KDS", "lavu_kds", $resp, $enable);

	$config_set[] = array(
		"config",
		"enable_kds",
		$enable?"1":"0",
		TRUE
	);

	modifyLocationSettings($config_set);

	adminActionLogExtensionChange("KDS", $enable);

	if ($enable)
	{
		loadExtensionDetails($ext_id, $resp, "1");
	}
	$headers  = 'MIME-Version: 1.0' . "\r\n";
    $headers .= "Content-Type: text/html; charset=UTF-8\r\n";
	$loc_info = sessvar('location_info');
	$action = ($enable) ?"Enable":"Disable";
	$message = "<font style='font-family:Verdana,Arial,Helvetica,sans-serif;
    font-size: 13px;'>Dear Billing Team,<br/><br/>Location <b>".$loc_info['title']."</b> has requested to <b>".$action."</b> KDS Advanced Feature.<br/><br/>Please proceed accordingly.<br/><br/>Regards,<br/>Lavu Customer Care</font>";
	mail("billing@lavuinc.com","KDS Advanced Features Info", $message, $headers);

}


function enableDisableeConduit($ext_id, &$resp, $enable)
{
	$paymentGateway = 'eConduit';

	// first tackle billing changes, then move on to module and payment_type changes
	if (!addRemoveModule("extensions.payment.econduit", $ext_id, $resp, $enable))
	{
		return;
	}

	addRemovePaymentMethod($paymentGateway, "payment_extension:82", $resp, $enable);

	adminActionLogExtensionChange($paymentGateway, $enable);

	enableDisableIntegrateCardProcessing($enable, $paymentGateway);

	if ($enable)
	{
		loadExtensionDetails($ext_id, $resp, "1");
	}
}

// - - - - - - - - Kiosk - - - - - - - -  //
function enableDisableKiosk($ext_id, &$resp, $enable)
{
	if (!addRemoveModule("extensions.ordering.kiosk", $ext_id, $resp, $enable))
	{
		return;
	}

	$config_set[] = array(
		"config",
		"kiosk_enabled",
		$enable?"1":"0",
		TRUE
	);
	$config_set[] = array(
		"config",
		"api_order_auto_send",
		$enable?"1":"0",
		TRUE
	);
	modifyLocationSettings($config_set);

	adminActionLogExtensionChange("Kiosk", $enable);

	if ($enable)
	{
		loadExtensionDetails($ext_id, $resp, "1");
	}
	
	$headers  = 'MIME-Version: 1.0' . "\r\n";
	$headers .= "Content-Type: text/html; charset=UTF-8\r\n";
	$loc_info = sessvar('location_info');
	$kiosk_devices  = reqvar('kiosk_devices');
	$action = ($enable) ?"Enable":"Disable";
	if ($action === "Enable")
	{
		$message = "<font style='font-family:Verdana,Arial,Helvetica,sans-serif;
		font-size: 13px;'>Dear Billing Team,<br/><br/>Location <b>".$loc_info['title']."</b> has requested to <b>".$action."</b> Kiosk for ".$kiosk_devices." devices.<br/><br/>Please proceed accordingly.<br/><br/>Regards,<br/>Lavu Customer Care</font>";
		mail("billing@lavuinc.com","Kiosk Info", $message, $headers);
	}
	elseif ($action === "Disable") {
		$message = "<font style='font-family:Verdana,Arial,Helvetica,sans-serif;
		font-size: 13px;'>Dear Billing Team,<br/><br/>Location <b>".$loc_info['title']."</b> has requested to <b>".$action."</b> Kiosk.<br/><br/>Please proceed accordingly.<br/><br/>Regards,<br/>Lavu Customer Care</font>";
		mail("billing@lavuinc.com","Kiosk Info", $message, $headers);
	}

}

/**
* This function is used to enable/disable dtt extension settings.
* @param  $ext_id
* @param  $resp
* @param  $enable
*/
function enableDisableDtt($ext_id, &$resp, $enable) {
	if (!addRemoveModule("extensions.reporting.dtt", $ext_id, $resp, $enable))
	{
		return;
	}

	$config_set[] = array(
			"config",
			"dtt_enabled",
			$enable?"1":"0",
			TRUE
	);

	modifyLocationSettings($config_set);

	adminActionLogExtensionChange("DTT", $enable);

	if ($enable)
	{
		loadExtensionDetails($ext_id, $resp, "1");
	}

	$headers  = 'MIME-Version: 1.0' . "\r\n";
	$headers .= "Content-Type: text/html; charset=UTF-8\r\n";
	$loc_info = sessvar('location_info');
	$dtt_devices  = reqvar('dtt_devices');
	$action = ($enable) ?"Enable":"Disable";
	if ($action === "Enable")
	{
		$message = "<font style='font-family:Verdana,Arial,Helvetica,sans-serif;
		font-size: 13px;'>Dear Billing Team,<br/><br/>Location <b>".$loc_info['title']."</b> has requested to <b>".$action."</b> DTT for ".$dtt_devices." devices.<br/><br/>Please proceed accordingly.<br/><br/>Regards,<br/>Lavu Customer Care</font>";
		mail("billing@lavuinc.com","DTT Info", $message, $headers);
	}
	elseif ($action === "Disable") {
		$message = "<font style='font-family:Verdana,Arial,Helvetica,sans-serif;
		font-size: 13px;'>Dear Billing Team,<br/><br/>Location <b>".$loc_info['title']."</b> has requested to <b>".$action."</b> DTT.<br/><br/>Please proceed accordingly.<br/><br/>Regards,<br/>Lavu Customer Care</font>";
		mail("billing@lavuinc.com","DTT Info", $message, $headers);
	}
}

//------- - - - - - Lavu ToGO  -----------//

function enableDisableLavuToGo($ext_id, &$resp, $enable)
{
	global $dn;
	if ($enable)
	{
		loadExtensionDetails($ext_id, $resp, "1");
	}else{
		require_once(__DIR__."/../../../../inc/olo/config.php");
		if (!addRemoveModule("extensions.ordering.lavutogo", $ext_id, $resp, $enable))
		{
			return;
		}
		$config_set[] = array(
				"config",
				"use_lavu_togo_2",
				$enable?"1":"0",
				TRUE
		);
		modifyLocationSettings($config_set);
		adminActionLogExtensionChange("Lavu To Go", $enable);
		mlavu_query("update `poslavu_MAIN_db`.`payment_status` set `lavutogo_payment`=0 where `dataname` ='[1]'",$dn);
		try {
			$zuoraConnect = new ZuoraIntegration();
			$zuoraResponse = $zuoraConnect->cancelLavuToGoSubscriptions([
				'dataname' => $dn,
				'cancel_reason' => 'Disabling Lavu To Go Subscription'
			]);
		} catch(Exception $e) {
			$zuoraResponse = false;
		}
		$headers  = 'MIME-Version: 1.0' . "\r\n";
		$headers .= "Content-Type: text/html; charset=UTF-8\r\n";
		$loc_info = sessvar('location_info');
		if ($zuoraResponse != false) {
			$message = "<font style='font-family:Verdana,Arial,Helvetica,sans-serif;
			font-size: 13px;'>Dear Billing Team,<br/><br/>Location <b>".$loc_info['title']."</b> has requested to <b>Disable</b> Lavu To Go.<br/><br/>Please proceed accordingly.<br/><br/>Regards,<br/>Lavu Customer Care</font>";
			$subject = "Lavu To Go V2 Disabled for ".$dn;
		} else {
			$message = "<font style='font-family:Verdana,Arial,Helvetica,sans-serif;
			font-size: 13px;'>Dear Billing Team,<br/><br/>Location: <b>".$dn."</b>(data name),<br/><br/>Lavu To Go has been enabled for this account and there is not an active Zuora account associated with this account.<br/><br/> Please ensure we stop billing payment from this customer.<br/><br/>Regards,<br/>Lavu Customer Care</font>";
			$subject = "Lavu To Go V2 Disabled for ".$dn;
		}
		mail(ZUORA_BILLING_EMAIL, $subject, $message, $headers);
	}
}

// - - - - - - - - Square - - - - - - - - //

// Enable or disable the Square extension
function enableDisableSquare($ext_id, &$resp, $enable, $PPCore=NULL)
{
	global $dn;
	$location_info = sessvar('location_info');
	
	if ($enable) {
		
		$primaryCurrency = $location_info['primary_currency_code'];
		if($primaryCurrency == '') {
			$resp['info']['error'] = "Please navigate to Advanced Location Settings and set a Primary Currency Code for your account before enabling the Square extension.";
			$resp['info']['description'] = 'Square error';
			return;
		}
		
		$get_square = mlavu_query("SELECT *  FROM `gateway_auth_tokens` WHERE `token` != '' AND `location_id` != '' AND `dataname` = '[1]' AND `extension_id` = [2] AND `permission_granted` = 1 AND `_deleted` = 0", $dn, $ext_id);
			if (mysqli_num_rows($get_square) == 0)
			{
				$resp['info']['error'] = "Please authorize square account.";
				$resp['info']['description'] = 'Square error';
				return;
			}
    }

    if (!addRemoveModule("extensions.payment.square", $ext_id, $resp, $enable))
    {
        return;
    }
		
    $config_set = array(
        array(
            "config",
            "modules",
            "extensions.payment.+square",
            TRUE
        )
    );

    modifyLocationSettings($config_set);

    addRemovePaymentMethod("Square", "native_extension:square", $resp, $enable);

    adminActionLogExtensionChange("Square", $enable);

    if ($enable)
    {
        loadExtensionDetails($ext_id, $resp, "1");
    }
}

function enableDisableSacoa(&$resp, $enable){

	global $dn;
	global $rdb;
	global $loc_id;

        //get extension_id
        $handler = mlavu_query("SELECT * from extensions WHERE title='Sacoa Playcard'");
        $extInfo=mysqli_fetch_assoc($handler);
        $ext_id=$extInfo['id'];

	if ($enable) {
		$sacoa_username    = reqvar('sacoa_username', '');
		$sacoa_password    = reqvar('sacoa_password', '');
		$sacoaUrl          = reqvar('sacoa_url', '');

		require_once("../../../lib/jcvs/SacoaApi.php");
		$apiObj = new SacoaApi();
		$apiObj->dataname=$dn;
		$apiObj->extensionId=$ext_id;
		$apiObj->apiHost=$sacoaUrl;
		$apiObj->init();	
		$sacoa_req = array("user" => $sacoa_username, "pass" => $sacoa_password, "computerId" => "101");
		$sacoa_req_string = json_encode($sacoa_req);
		$response = $apiObj->sacoaApi($sacoa_req_string,$apiObj->endpoints['login'], 1);
		$res_data = json_decode($response, 1);

		if ($res_data['body']['authorized'] != 1 && $res_data['body']['errorCode'] == 3)
		{
			$resp['info']['error'] = "Not able to access Sacoa Extension, Invalid Username or Password.";
			$resp['info']['description'] = 'Sacoa error';
			return;
		}
		else if (!$res_data['body']['authorized'])
		{
			$resp['info']['error'] = "Not able to access Sacoa API.";
			$resp['info']['description'] = 'Sacoa error';
			return;
		}
		
		if($res_data['body']['authorized'] == 1){
			$en_pwd = base64_encode($sacoa_password);
			$en_usern_pwd = array($sacoa_username, $en_pwd);
			$token = implode("|", $en_usern_pwd);
			$deleted = 0;
			
			$insert_token = mlavu_query("INSERT INTO `poslavu_MAIN_db`.`auth_tokens`(`id`, `dataname`, `extension_id`, `token`, `_deleted`, `token_type`)
 				VALUES (0, '".$dn."', '".$ext_id."', '".$token."', '".$deleted."', 'secret')");
			//Should delete existing entries in forced_modier_lists table before insert
			$handler=mlavu_query("DELETE FROM `poslavu_".$dn."_db`.`forced_modifier_lists` WHERE type='sacoa_card'");
			$insert_sacoa_into_fm = mlavu_query("INSERT INTO `poslavu_".$dn."_db`.`forced_modifier_lists` (`title`, `description`, `type`, `_deleted`, `id`, `menu_id`, `chain_reporting_group`, `happyhours_id`) VALUES ('*Sacoa Playcard', '', 'sacoa_card', 0, 0, 0, '', 0)");
			//Should delete existing entries in forced_modier_lists table
		}
	}

	$ext_title		= "Sacoa";

	if (!addRemoveModule("extensions.payment.sacoa", $ext_id, $resp, $enable))
	{
		return;
	}
	
/* 	$config_set = array(
			array(
					"config",
					//"modules",
					"extensions.cards.+sacoa",
					$enable?"1":"0",
					TRUE
			)
	);
	
	modifyLocationSettings($config_set); */
	$ct = "`".$rdb."`.`config`";
	$check_setting = mlavu_query("SELECT `id`, `value` FROM ".$ct." WHERE `location` = '[1]' AND `type` = 'location_config_setting' AND `setting`= '[2]'", $loc_id, 'modules');
	if (!$check_setting)
	{
		db_error($resp);
		return FALSE;
	}
	
	if ($check_setting)
	{
		if (mysqli_num_rows($check_setting) > 0)
		{
			$info = mysqli_fetch_assoc($check_setting);
			$modules	= explode(",", $info['value']);
			$module 	= "extensions.payment.lavu.sacoa";
			
			$rsig	= $enable?"+":"-";
			$nrsig	= $enable?"-":"+";
			
			$mod_parts	= explode(".", $module);
			$mp_cnt		= count($mod_parts);
			$mod_tail	= $mod_parts[($mp_cnt - 1)];
			$mod_base	= implode(".", array_slice($mod_parts, 0, -1));
			
			$new_modules	= array();
			$required		= $mod_base.".".$rsig.$mod_tail;
			$has_required	= FALSE;
			
			foreach ($modules as $mod)
			{
				if ($mod != str_replace($rsig, $nrsig, $required))
				{
					if (substr($mod, -2) == $nrsig."*")
					{
						$compare_len = (strlen($mod) - 3);
						if (substr($mod, 0, $compare_len) == substr($mod_base, 0, $compare_len))
						{
							continue;
						}
					}
					
					$new_modules[] = $mod;
					if ($mod == $required)
					{
						$has_required = TRUE;
					}
				}
			}
			
			if (!$has_required)
			{
				$new_modules[] = $required;
			}
			
			sort($new_modules);
			
			$update_modules = mlavu_query("UPDATE ".$ct." SET `value` = '[1]' WHERE `id`= '[2]'", implode(",", $new_modules), $info['id']);
			if (!$update_modules)
			{
				db_error($resp);
				return FALSE;
			}
		}else{
			//LP-5561: Not sure if location=1 rec should pre-exist as for other random locations found rec with location=1 exist along with location=0. Hence inserting. 
			$updateConfig = mlavu_query("INSERT INTO ".$ct." (location,setting,value,type,_deleted) VALUES (1,'modules','extensions.payment.lavu.+sacoa','location_config_setting',0)");
			if(!$updateConfig){
				db_error($resp);
				return FALSE;
			}
		}
	}
	addRemovePaymentMethod("Sacoa Playcard", "sacoa_card", $resp, $enable);
	
	adminActionLogExtensionChange($ext_title, $enable);
	
	if ($enable)
	{
		loadExtensionDetails($ext_id, $resp, "1");
	}	
	else
	{
		mlavu_query("DELETE FROM `poslavu_MAIN_db`.`auth_tokens` WHERE `extension_id` = '[1]' AND `dataname` = '[2]' AND `token_type` = 'secret'", $ext_id, $dn);
		mlavu_query("DELETE FROM `poslavu_MAIN_db`.`gateway_auth_tokens` WHERE `extension_id` = '[1]' AND `dataname` = '[2]'", $ext_id, $dn);
		mlavu_query("DELETE FROM `poslavu_".$dn."_db`.`forced_modifier_lists` WHERE `type` = 'sacoa_card'");
		mlavu_query("DELETE FROM `poslavu_".$dn."_db`.`payment_types` WHERE `special` = 'sacoa_card'");
	}
}

function enableDisableLavuPay($ext_id, &$resp, $enable) {
	if (!addRemoveModule("extensions.payment.lavupay", $ext_id, $resp, $enable))
	{
		return;
	}

	adminActionLogExtensionChange("LavuPay", $enable);

	if ($enable)
	{
		loadExtensionDetails($ext_id, $resp, "1");
	}
}

/**
 * This function is used to enable/disable Integrate credit card processing and select eConduit gateway
 * while user enable and disable extension and onboarding gateway
 * @param bollean $enable extension enable/disable.
 * @param string $paymentGateway Payment Gateway
 */
function enableDisableIntegrateCardProcessing($enable, $paymentGateway) {
	global $loc_id;
	$integrateCC = ($enable) ? 1 : 0;
	$paymentGateway     = ($enable) ? $paymentGateway : '';
	lavu_query("UPDATE `locations` SET `integrateCC`='[1]', `gateway`='[2]' WHERE `id` = '[3]'", $integrateCC, $paymentGateway, $loc_id);
}
