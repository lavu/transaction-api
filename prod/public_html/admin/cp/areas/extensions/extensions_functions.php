<?php
function addRemoveModule($module, $ext_id, &$resp, $enable)
{
	global $data_name;
	if ($data_name == '') {
		$data_name = sessvar("admin_dataname");
	}
	$rdb = "`poslavu_{$data_name}_db`";
	$rest = "`poslavu_MAIN_db`.`restaurants`";

	$get_modules = mlavu_query("SELECT `id`, `modules` FROM ".$rest." WHERE `data_name` = '[1]'", $data_name);
	if (!$get_modules) {
		db_error($resp);
		return false;
	}

	if (mysqli_num_rows($get_modules) == 0) {
		db_error($resp);
		return false;
	}

	$info		= mysqli_fetch_assoc($get_modules);
	$modules	= explode(",", $info['modules']);

	$rsig	= $enable?"+":"-";
	$nrsig	= $enable?"-":"+";

	$mod_parts	= explode(".", $module);
	$mp_cnt		= count($mod_parts);
	$mod_tail	= $mod_parts[($mp_cnt - 1)];
	$mod_base	= implode(".", array_slice($mod_parts, 0, -1));

	$new_modules	= array();
	$required		= $mod_base.".".$rsig.$mod_tail;
	$has_required	= false;

	foreach ($modules as $mod) {
		if ($mod != str_replace($rsig, $nrsig, $required)) {
			if (substr($mod, -2) == $nrsig."*") {
				$compare_len = (strlen($mod) - 3);
				if (substr($mod, 0, $compare_len) == substr($mod_base, 0, $compare_len)) {
					continue;
				}
			}

			$new_modules[] = $mod;
			if ($mod == $required) {
				$has_required = true;
			}
		}
	}

	if (!$has_required) {
		$new_modules[] = $required;
	}

	sort($new_modules);
	$imploded_modules = implode(",", $new_modules);

	$update_modules = mlavu_query("UPDATE ".$rest." SET `modules` = '[1]' WHERE `id`= '[2]'", $imploded_modules, $info['id']);
	if (!$update_modules) {
		db_error($resp);
		return false;
	}

	$company_id	= admin_info("companyid");
	lavu_connect_byid($company_id, $rdb);

	$check_config_modules = lavu_query("SELECT value FROM {$rdb}.`config` WHERE `setting` = 'modules' AND `type`='location_config_setting' AND `_deleted`=0 AND `location`!=0");
	if (!$check_config_modules) {
		rdb_error($resp);
		return false;
	}
	if (mysqli_num_rows($check_config_modules) == 0) {
		$insert_config_modules = lavu_query("INSERT INTO {$rdb}.`config` (`location`, `setting`, `value`, `value2`, `value3`, `value4`, `value_long`, `type`, `value5`, `value6`, `value7`, `value8`, `_deleted`, `value9`, `value10`, `value11`, `value12`, `value13`, `value14`, `value_long2`, `value15`, `value16`, `value17`) VALUES (1, 'modules', '[1]', '', '', '', '', 'location_config_setting', '', '', '', '', 0, '', '', '', '', '', '', '', '', '0', '0')", $imploded_modules);
		if (!$insert_config_modules) {
			rdb_error($resp);
			return false;
		}
	} else {
		$update_config_modules = lavu_query("UPDATE {$rdb}.`config` SET `value` = '[1]' WHERE `setting`= 'modules' AND `type`= 'location_config_setting' AND '_deleted' =0 AND `location` !=0", $imploded_modules);
		if (!$update_config_modules) {
			rdb_error($resp);
			return false;
		}
	}

	$resp['ext_id'] = $ext_id;

	set_sessvar("modules_dn", "");

	return true;
}

function adminActionLogExtensionChange($ext_title, $enable, $dataname='')
{
	global $rdb;
	global $loc_id;

	$vars = array(
			'action'		=> ($enable?"Enabled":"Disabled")." ".$ext_title." extension",
			'data'			=> admin_info("fullname"),
			'detail1'		=> "Access level: ".admin_info("access_level"),
			'ipaddress'		=> reliableRemoteIP(),
			'loc_id'		=> $loc_id,
			'order_id'		=> "0",
			'server_time'	=> UTCDateTime(true),
			'time'			=> DateTimeForTimeZone(reqvar("ext_tz", "")),
			'user'			=> admin_info("username"),
			'user_id'		=> admin_info("loggedin")
	);

	$fields = "";
	$values = "";

	buildInsertFieldsAndValues($vars, $fields, $values);

	$vars['dataname'] = admin_info("dataname");
	if ($rdb == '') {
		$rdb = 'poslavu_'.$dataname.'_db';
	}
	mlavu_query("INSERT INTO `".$rdb."`.`admin_action_log` (".$fields.") VALUES (".$values.")", $vars);
}
?>