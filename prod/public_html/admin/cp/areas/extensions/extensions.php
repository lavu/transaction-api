<?php

if (isset($_GET['mode']) && $_GET['mode']=="extensions_appstore")
{
	require_once(__DIR__."/../lavu_app_store.php");
	return;
}

if (!$in_lavu)
{
	return;
}

if (session_status() == PHP_SESSION_NONE)
{
    session_start();
}

echo "<style type='text/css'>".file_get_contents(__DIR__."/extensions.css")."</style>";

require_once(__DIR__."/../../../components/payment_extensions/PayPal/PayPalCore.php");

// Hide Square extension becuase we will use Square onboarding from cp4 extension page
//require_once(__DIR__."/../../../components/payment_extensions/square/SquareCore.php");

$group_id = $extension_group_mode_to_id[$mode];
$extension_details = getExtensionDetails($group_id);
// Define reactMode in case it was redefined along the way
$reactMode = isset($_GET['react']) || isset($_COOKIE['CP3_ENABLED']) ? true : false;

// This code is added to remove the Square and Paypal from the extension list
if($reactMode) {
	foreach($extension_details as $key=>$ext){
		if(($ext['code_word'] == "paypal" || $ext['code_word'] == "square"))
		{
			unset($extension_details[$ext['code_word']]);		
			$ext_count -= 1; 
		}
	}
}

$ext_count = count($extension_details);
if (!$ext_count)
{
	echo "<br><br><b>".speak("No extensions found for this group.")."</b><br><br><br>";
}
else
{
	$display = "<script src='areas/extensions/extensions.js?t=".time()."'></script>";
	$display .= "<script language='javascript'>var ext_dn = \"".$data_name."\"; var ext_loc_id = \"".$locationid."\"; var ext_tz = \"".$location_info['timezone']."\";</script>";
	$display .= "<br><br><table cellspacing='0' cellpadding='0' style='margin: 6px'>";

	rsort($extension_details);

	$count = 0;

	$has_dev_extension = false;

	$init_js = "";

	foreach ($extension_details as $ext)
	{
		$ext_cell_top		= "extension_cell_top";
		$ext_cell_bottom	= "extension_cell_bottom";

		if ($ext['dev'] == "1")
		{
			$has_dev_extension	= true;
			$ext_cell_top		= "dev_extension_cell_top";
			$ext_cell_bottom	= "dev_extension_cell_bottom";
		}

		if (($count % 3) == 0)
		{
			$top_code = "<tr>";
		}

		$top_code .= "<td class='".$ext_cell_top."' align='center' valign='top'>";
		$top_code .= "<table width='350px'>";

		$top_code .= "<tr><td class='ext_inner_cell1' align='center' valign='middle'>";
		if ($ext['thumbnail'] != "")
		{
			$top_code .= "<img src='areas/extensions/images/".$ext['thumbnail']."' height='50px'>";
		}
		else
		{
			$top_code .= "<span class='extension_title'>".$ext['title']."</span>";
		}
		$top_code .= "</td></tr>";

		$top_code .= "<tr><td class='ext_inner_cell2' align='left' valign='top'>";
		$top_code .= getDescription($ext);
		$top_code .= "</td></tr>";

		$top_code .= "</table></td>";

		if (($count%3)==2 || ($count+1)==$ext_count)
		{
			$top_code .= "</tr>";
		}
		else
		{
			$top_code .= "<td width='6px'></td>";
		}

		if (($count % 3) == 0)
		{
			$bottom_code = "<tr>";
		}
		$bottom_code .= "<td class='".$ext_cell_bottom."' align='center' valign='bottom'>";
		$bottom_code .= "<table width='350px'>";

		$ed_icon			= "icon_disabled2.png";
		$ed_class			= "ext_disabled";
		$ed_click			= getEnableOnClickEvent($ext);
		$ed_text			= "Not enabled";
		$ed_change_class	= "enabledBtn";
		$ed_change_text		= "Enable";

		$ext_enabled = $modules->hasModule($ext['module']);
		if ($ext_enabled)
		{
			$ed_icon			= "icon_enabled2.png";
			$ed_class			= "ext_enabled";
			$ed_click			= "disableExtension(\"".$ext['id']."\", \"".$ext['title']."\", \"".$ext['code_word']."\");";
			$ed_text			= "Enabled";
			if ($ext['code_word'] == 'lavutogo' && !is_lavu_admin()) {
				$ed_change_class	= "btn-disable";
			} else {
				$ed_change_class	= "disabledBtn";
			}
			$ed_change_text		= "Disable";
		}

		// If LoyalTree White Label is enabled, show the account-specific values from the config table
		if ($ext['title'] == 'LoyalTree White Label' && $ext_enabled)
		{
			$loyaltree_whitelabel_setting_names = array('loyaltree_id', 'loyaltree_username', 'loyaltree_password');
			$config_rows = lavu_query("SELECT * FROM `poslavu_[1]_db`.`config` WHERE `setting` LIKE '%loyaltree%' AND `_deleted`=0", $data_name);
			while ($config_rows && $config = mysqli_fetch_assoc($config_rows))
			{
				if (in_array($config['setting'], $loyaltree_whitelabel_setting_names))
				{
					$init_js .= ' document.getElementById("'. $config['setting'] .'").value = "'. str_replace('"', '\"', $config['value']) .'";';
				}
			}
			$init_js .= 'hideLoyalTreeWhiteLabelInputs("'.$ext['id'].'", "'.$ext['title'].'", "'.$ext['code_word'].'");';
		}

		// If Pepper is enabled, pre-popualte its config settings in the on-boarding fields
		if ($ext['title'] == 'Lavu Premium Loyalty' && $ext_enabled)
		{
			// Default settings (which could be overriden by config settings)
			$init_js .= ' document.getElementById("pepper_apiurl").value = "https://api.pepperhq.com/";';
			$init_js .= ' document.getElementById("pepper_loyalty_model").value = "points";';

			$pepper_config_setting_names = array('pepper_applicationid', 'pepper_apitoken', 'pepper_locationid', 'pepper_registerid');
			$config_rows = lavu_query("SELECT * FROM `poslavu_[1]_db`.`config` WHERE `setting` LIKE 'pepper_%' AND `_deleted`=0", $data_name);
			while ($config_rows && $config = mysqli_fetch_assoc($config_rows))
			{
				if (in_array($config['setting'], $pepper_config_setting_names))
				{
					$init_js .= ' document.getElementById("'. $config['setting'] .'").value = "'. str_replace('"', '\"', $config['value']) .'";';
				}
				else if ($config['setting'] == 'pepper_beta' && $config['value'] == '1')
				{
					$init_js .= ' document.getElementById("pepper_apiurl").value = "https://beta-api.pepperhq.com/";';
				}
				else if ($config['setting'] == 'pepper_loyalty_stamps' && $config['value'] == '1')
				{
					$init_js .= ' document.getElementById("pepper_loyalty_model").value = "stamps";';  // TO DO : make sure this selects a drop-down properly
				}
			}
			$init_js .= 'hidePepperInputs("'.$ext['code_word'].'", "'.$ext['title'].'", "'.$ext['code_word'].'");';
		}

		if ($ext['title'] == 'Lavu Kiosk' && $ext_enabled)
		{
			$dn	= sessvar("admin_dataname");
			$kiosk_limit_query = mlavu_query("SELECT custom_max_kiosk from `poslavu_MAIN_db`.`payment_status` where `dataname` = '[1]'", $dn);
			$data = mysqli_fetch_assoc($kiosk_limit_query);
			$kiosk_current_limit = $data['custom_max_kiosk'];
			$init_js .= 'setKioskDeviceLimit('.$kiosk_current_limit.');';
		}

		$bottom_code .= "<tr><td class='ext_inner_cell3' align='left' valign='middle'>";
		$bottom_code .= "<table cellspacing='0' cellpadding='0' width='100%'>
			<tr id='indicate_button_".$ext['id']."'>
				<td valign='middle' style='position:relative; width:115px'><img src='areas/extensions/images/".$ed_icon."' height='17' width='21' style='position:absolute; top:50%; margin-top:-8px'>
					<span class='".$ed_class."' style='position:absolute; top:50%; left:25px; height:15px; margin-top:-7px'>".$ed_text."</span></td>
				<td align='center'>
					<table cellspacing='0' cellpadding='0'>
						<tr>
							<td class='".$ed_change_class."_left'></td>
							<td class='".$ed_change_class."_mid'><button class='".$ed_change_class."' onclick='".$ed_click."'>".$ed_change_text."</button></td>
							<td class='".$ed_change_class."_right'></td>
						</tr>
					</table>
				</td>
				<td align='right'><a class='ext_learn_more' onclick='extensionLearnMore(\"".$ext['id']."\", \"".($ext_enabled?"1":"0")."\")'>Learn More</a></td>
			</tr>
		</table>";
		$bottom_code .= "</td></tr>";

		$bottom_code .= "</table>";
		$bottom_code .= "</td>";

		if (($count%3)==2 || ($count+1)==$ext_count)
		{
			$bottom_code .= "</tr><tr><td height='8px'></td></tr>";
			$display .= $top_code.$bottom_code;
		}
		else
		{
			$bottom_code .= "<td width='4px'></td>";
		}

		$count++;
	}

	$display .= "</table><br><br><br>";

	if ((admin_info("lavu_admin") == "1") && $has_dev_extension)
	{
		$display .= "<font color='#588604' style='font-size:14px'><b>*</b></font> ";
		$display .= "Bold, dark green borders indicate extensions visible only to Lavu Admin users (i.e., developers and support staff)";
	}

	if (!empty($init_js))
	{
		$display .= '<script>/*Init JS*/'.$init_js.'</script>';
	}
	// Hide Square extension becuase we will use Square onboarding from cp4 extension page
	/*
	if(isset($_GET['square']) && trim($_GET['square'])!='')
	{
		$resp_status = $_GET['square'] ;
		$locationsJson = '';
		if($resp_status == 'success')
		{
			$sqr_message = "Authorization successful! Please specify your location";
			$SquareCore = new SquareCore();
			$locations = $SquareCore->getLocationsForDataname($data_name);
			$locationsJson = json_encode($locations);
	
		} else if($resp_status == 'failed' || $resp_status == 'auth_failed') {
            $sqr_message = 'Authorization Failed';
        } else if($resp_status == 'invalid_request') {
            $sqr_message = 'Invalid Request';
        }
		if(!isset($_GET['locationstatus']) || $_GET['locationstatus']!='1') {
            if ($locationsJson == '') {
                $display .= '<script>alertpopup(\'' . $sqr_message . '\',\'error\');</script>';
            } else {
                $display .= '<script>var locations = JSON.stringify('.$locationsJson.');locationsPopupShow(\'' . $sqr_message . '\',locations);</script>';
            }
        }
	}
	*/

	echo $display;
}

function getEnableOnClickEvent($extension)
{
	global $data_name;
	if ($extension['title'] == 'PayPal')
	{
        $PayPalCore = new PayPalCore();
		if ($data_name === null || $data_name ==="")
		{
			if (isset($_SESSION['last_data_name']))
			{
				$data_name = $_SESSION['last_data_name'];
			}
		}

		$refresh_token = $PayPalCore->tokenHelper->getRefreshToken($data_name, "refresh");
		if ($refresh_token === null || $refresh_token === '' || is_array($refresh_token))
		{
			return 'initiatePayPalOnboarding()';
		}
		else
		{
			return 'enableExtension("'.$extension['id'].'", "'.$extension['title'].'", "'.$extension['code_word'].'");';
		}
	}
	else if ($extension['title'] == 'LoyalTree White Label')
	{
		return 'hideLoyalTreeWhiteLabelInputs("'.$extension['id'].'", "'.$extension['title'].'", "'.$extension['code_word'].'")';
	}
	else if ($extension['title'] == 'Lavu Premium Loyalty')
	{
		return 'hidePepperInputs("'.$extension['id'].'", "'.$extension['title'].'", "'.$extension['code_word'].'")';
	}
	// Hide Square extension becuase we will use Square onboarding from cp4 extension page
	/*
	else if ($extension['title'] == 'Square')
    {
	    if ($data_name === null || $data_name ==="")
	    {
	        if (isset($_SESSION['last_data_name']))
	        {
	            $data_name = $_SESSION['last_data_name'];
	        }
	    }
	    $SquareCore = new SquareCore('', $data_name);
	    //$square_credential = $SquareCore->tokenHelper->getAPICredentials($data_name);
	    $square_credential = $SquareCore->tokenHelper->getOAuthCredentials();
	    //print_r($square_credential);
	    if ($square_credential['applicationID'] == '' || $square_credential['tokenID'] == '' || $square_credential['secretID'] == '' || $square_credential['locationID'] == '')
	    {
	        return 'initiateSquareOnboarding()';
	    }
	    else
	    {
	    	//return 'initiateSquareOnboarding()';
	       return 'enableExtension("'.$extension['id'].'", "'.$extension['title'].'", "'.$extension['code_word'].'");';
	    }
	}
	*/

	return 'enableExtension("'.$extension['id'].'", "'.$extension['title'].'", "'.$extension['code_word'].'");';
}


function getDescription($extension)
{
	global $data_name;
	global $locationid;

	if ($extension['short_description'] !== "")
	{
		return $extension['short_description'];
	}
	else if ($extension['title'] === "PayPal")
	{
        $PayPalCore = new PayPalCore();
		if ($data_name===NULL || $data_name==="")
		{
			if (isset($_SESSION['last_data_name']))
			{
				$data_name = $_SESSION['last_data_name'];
			}
		}

		$refresh_token = $PayPalCore->tokenHelper->getRefreshToken($data_name, "refresh");
		//error_log("dataname: ".$data_name." refresh token: ".$refresh_token);
		if ($refresh_token===NULL || $refresh_token==="" || is_array($refresh_token))
		{
			return $PayPalCore->PayPalHereOnboardingForm($data_name, $locationid, FALSE);
		}

		$merchantId = $PayPalCore->tokenHelper->getRefreshToken($data_name, "merchantID");
		//error_log("dataname: ".$data_name." Merchant ID token: ".$merchantId);
		if ($merchantId===NULL || $merchantId==="" || is_array($merchantId))
		{
			return $PayPalCore->PayPalExpressCheckoutOnboarding($data_name, FALSE);
		}
		else
		{
			return file_get_contents(__DIR__."/PayPal/PayPalDescription.html");
		}
	}
	// Hide Square extension becuase we will use Square onboarding from cp4 extension page
	/*
	else if ($extension['title'] == "Square") {
        if ($data_name===NULL || $data_name==="")
        {
            if (isset($_SESSION['last_data_name']))
            {
                $data_name = $_SESSION['last_data_name'];
            }
        } 
        $SquareCore = new SquareCore('', $data_name);
        //$apiCredentials = $SquareCore->tokenHelper->getAPICredentials($data_name);
      	 $apiCredentials = $SquareCore->tokenHelper->getOAuthCredentials();
      	// print_r($apiCredentials);
        return $SquareCore->squareHereOnboardingForm($apiCredentials, $data_name);
	}
	*/
}


?>

<script>

function PopupCenter(url, title, w, h) {
    // Fixes dual-screen position                         Most browsers      Firefox
    var dualScreenLeft = window.screenLeft != undefined ? window.screenLeft : screen.left;
    var dualScreenTop = window.screenTop != undefined ? window.screenTop : screen.top;

   var width = window.innerWidth ? window.innerWidth : document.documentElement.clientWidth ? document.documentElement.clientWidth : screen.width;
    var height = window.innerHeight ? window.innerHeight : document.documentElement.clientHeight ? document.documentElement.clientHeight : screen.height;

   var left = ((width / 2) - (w / 2)) + dualScreenLeft;
    var top = ((height / 2) - (h / 2)) + dualScreenTop;
    var newWindow = window.open(url, title, 'scrollbars=yes, width=' + w + ', height=' + h + ', top=' + top + ', left=' + left);

   // Puts focus on the newWindow
    if (window.focus) {
        newWindow.focus();
    }
}


</script>
