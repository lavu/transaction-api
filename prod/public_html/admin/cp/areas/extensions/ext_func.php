<?php

function getExtensionGroups()
{
	$extension_groups['No Extension Groups'] = "0";

	$get_extension_groups = mlavu_query("SELECT * FROM `poslavu_MAIN_db`.`extension_groups` WHERE `_deleted` = '0'".((admin_info('lavu_admin') == "1")?"":" AND `dev` = '0'")." ORDER BY `_order` ASC");
	if (mysqli_num_rows($get_extension_groups) > 0)
	{
		$extension_groups = array();
		while ($info = mysqli_fetch_assoc($get_extension_groups))
		{
			$extension_groups[$info['title']] = $info['id'];
		}
	}

	return $extension_groups;
}

function getExtensionDetails($group_id)
{
	$extension_details = array();

	$filter = ($group_id == "0")?"":" AND (`group_ids` = '[1]' OR `group_ids` LIKE '[1],%' OR `group_ids` LIKE '%,[1]' OR `group_ids` LIKE '%,[1],%')";
	// Hide Square extension becuase we will use Square onboarding from cp4 extension page
	$get_extensions = mlavu_query("SELECT * FROM `poslavu_MAIN_db`.`extensions` WHERE `_deleted` = '0' AND code_word NOT IN ('square') ".((admin_info('lavu_admin') == "1")?"":" AND `dev` = '0'").$filter, $group_id);

	if (mysqli_num_rows($get_extensions) > 0)
	{
		while ($info = mysqli_fetch_assoc($get_extensions))
		{
			$extension_details[$info['code_word']] = $info;
		}
	}

	ksort($extension_details);

	return $extension_details;
}