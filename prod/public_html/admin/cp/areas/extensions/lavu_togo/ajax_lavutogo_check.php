<?php
ini_set('display_errors', '0');
session_start();
require_once("../extensions_requests.php");
require_once("../extensions_functions.php");
require_once(__DIR__."/../../../resources/core_functions.php");
require_once(__DIR__."/../../../../../inc/olo/config.php");
require_once(__DIR__."/../../../../../inc/billing/zuora/ZuoraIntegration.php"); 
if (isset($_SESSION['poslavu_234347273_location_info'])) { 
	$get_extension_query = mlavu_query("SELECT id FROM `poslavu_MAIN_db`.`extensions` WHERE `_deleted` = '0' and `title` = 'Lavu To Go'");
	$get_extension = mysqli_fetch_assoc($get_extension_query);
	$ext_id = $get_extension['id'];
	$enable =1;
	$resp['info'] = array('description'	=> "",'error'=> "");
	$resp['status'] = "success";
	addRemoveModule("extensions.ordering.lavutogo", $ext_id, $resp, $enable);
	$config_set[] = array("config","use_lavu_togo_2",$enable?"1":"0",true);
	modifyLocationSettings($config_set);
	$details -> fname = $_REQUEST['fname'];
	$details -> lname = $_REQUEST['lname'];
	$details -> dob = $_REQUEST['dob'];
	$details -> pack = $_REQUEST['pack'];
	$detailsJSON = json_encode($details);
	$data_name = $_SESSION['poslavu_234347273_admin_dataname'];
	$get_config_query = mlavu_query("update `poslavu_".$data_name."_db`.`config` set value2='$detailsJSON' WHERE `_deleted` = '0' and setting='use_lavu_togo_2'");
	adminActionLogExtensionChange("Lavu To Go", $enable);
	$vars['dataname'] = $_SESSION['last_data_name'];
	$vars['restaurantid'] = $_SESSION['primary_restaurantid']!=''?$_SESSION['primary_restaurantid']:$_SESSION['poslavu_234347273_admin_root_companyid'];
	$vars['username'] = $_SESSION['poslavu_234347273_admin_username'] == 'poslavu_admin' ? $_SESSION['posadmin_loggedin'] : $_SESSION['poslavu_234347273_admin_username'];
	$vars['fullname'] = $_SESSION['poslavu_234347273_admin_username'] == 'poslavu_admin' ? $_SESSION['posadmin_fullname'] : $_SESSION['poslavu_234347273_admin_fullname'];
	$vars['ipaddress'] = $_SERVER['REMOTE_ADDR'];
	$vars['action'] =  "Lavu To Go Enabled";
	$name = "Name: ".$_REQUEST['fname']." ".$_REQUEST['lname'].", DOB: ".$_REQUEST['dob'];

	if ($_REQUEST['pack']==PACK_MONTHLY) {
		$package = PACK_MONTHLY_TEXT;
		$package_id=LTG_MONTHLY;
		$zuoraPackage = ZUORA_LTG_MONTHLY;
	} else if ($_REQUEST['pack']==PACK_ANNUAL) {
		$package = PACK_ANNUAL_TEXT	;
		$package_id=LTG_ANNUAL;
		$zuoraPackage = ZUORA_LTG_ANNUAL;
	}

	$details = $name. ", Set Package: ".$package;

	// enable zuora billing
	try {
		$zuoraConnect = new ZuoraIntegration();
		$zuoraResponse = $zuoraConnect->subscribeToLavuToGo([
			'dataname' => $_SESSION['last_data_name'],
			'package' => $zuoraPackage
		]);
	} catch(Exception $e) {
		$zuoraResponse = false;
	} 
	
	$vars['details'] = $details;
	$keys = array_keys($vars);
	$qfields = "";
	$qvalues = "";
	foreach ($keys as $key) {
		if ($qfields != "") {
			$qfields .= ", ";
			$qvalues .= ", ";
		}
		$qfields .= "`$key`";
		$qvalues .= "'[$key]'";
	}

	$record_billing_log = mlavu_query("INSERT INTO `poslavu_MAIN_db`.`billing_log` ($qfields, `datetime`) VALUES ($qvalues, now())", $vars);
	mlavu_query("update `poslavu_MAIN_db`.`payment_status` set `lavutogo_payment`=$package_id where `dataname` ='".$_SESSION['last_data_name']."'");

	$loc_info = sessvar('location_info');
	if ($zuoraResponse !== false && isset($zuoraResponse->SubscriptionId) && !empty($zuoraResponse->SubscriptionId)) {
		$details .= "| Zuora subscription enabled | Subscription Id ".$zuoraResponse->SubscriptionId. " | Subscription Package $zuoraPackage";
		$message = "<font style='font-family:Verdana,Arial,Helvetica,sans-serif;
			font-size: 13px;'>Dear Billing Team,<br/><br/>Location <b>".$loc_info['title']."</b> have <b>Enabled</b> Lavu To Go.<br/><br/>
			<b>Zuora Subscription Id : </b> ".$zuoraResponse->SubscriptionId."<br/>
			<b>Zuora Subscription Number : </b> ".$zuoraResponse->SubscriptionNumber."<br/>
			<b>Zuora Subscription Package : </b> ".$zuoraPackage."<br/>
			<br/><br/>Regards,<br/>Lavu Customer Care</font>";
		$subject = "Lavu To Go V2 Enabled for ".$_SESSION['last_data_name'];
	} else {
		$message = "<font style='font-family:Verdana,Arial,Helvetica,sans-serif;
			font-size: 13px;'>Dear Billing Team,<br/><br/>Location: <b>".$_SESSION['last_data_name']."</b>(data name),<br/><br/>Lavu To Go has been enabled for this account and there is not an active Zuora account associated with this account.<br/><br/> Please ensure this is an internal account or collect payment from the customer.<br/><br/>Regards,<br/>Lavu Customer Care</font>";
		$subject = "Lavu To Go V2 Enabled for ".$_SESSION['last_data_name'];
	}


	$headers  = 'MIME-Version: 1.0' . "\r\n";
	$headers .= "Content-Type: text/html; charset=UTF-8\r\n";
	
	mail(ZUORA_BILLING_EMAIL, $subject, $message, $headers);
	echo "success";
} else {
	echo "failure";
}