
var hold_main_content_height	= 0;
var ext_comm_mode				= "";
var ext_active_id				= "";
var ext_active_title			= "";
var ext_enabled					= 0;
var main_content_div			= document.getElementById("main_content_area");
var extension_info_div			= null;
var ext_active_codeword			= "";

function extensionLearnMore(extension_id, enabled)
{
	addExtensionInfo(true);

	ext_comm_mode	= "load_details";
	ext_active_id	= extension_id;
	ext_enabled		= enabled;

	startEXTrequest();
}

function initiatePayPalOnboarding()
{
	document.getElementById("pphsignup_form").submit();
}

function addExtensionInfo(with_ei_main_table)
{
	hold_main_content_height = main_content_div.offsetHeight;

	var str = "<div class='extension_info_div'>";
	str += "<div class='extension_info_bg'></div>";
	if (with_ei_main_table)
	{
		str += "<div id='ei_main_table_div' class='extension_info_inner_div'><center>";
		str += "<table id='ei_main_table' cellspacing='0'><tr><td id='ei_main_top' class='ei_main_top' align='right'><div class='btn_ei_close' onclick='extensionInfoClose();'></div></td><tr><td id='ei_main_cell' class='ei_main_cell' style='display:none;'></td></tr></table><br><br>";
		str += "</center></div>";
	}
	str += "<div id='ext_activity_div' class='extension_info_bg'style='padding-top:200px; opacity:0.3;'><center><img src='images/kpr_activity.gif' width='32px' height='32px'></center></div>";
	str += "</div>";

	extension_info_div = document.createElement("div");
	extension_info_div.innerHTML = str;

	main_content_div.appendChild(extension_info_div);
}

function enableExtension(extension_id, extension_title, extension_codeword)
{
	ext_active_title = extension_title;
	ext_active_codeword = extension_codeword;

	if(ext_active_title == 'Sacoa Playcard')
	{
		var sacoa_un =  document.getElementById("sacoa_username").value;
		var sacoa_pwd =  document.getElementById("sacoa_password").value;
		var sacoa_url =  document.getElementById("sacoa_url").value;
		if(sacoa_un == "" || sacoa_pwd == "")
		{
			alert("Sacoa login details not specified.\nPlease enter username & password details.");
			return;
		}else if(sacoa_url == ""){
			alert("Please specify Sacoa Service URL.");
			return;
		}
	}
	if (hold_main_content_height == 0)
	{
		hold_main_content_height = main_content_div.offsetHeight;
	}

	if (extension_info_div == null)
	{
		addExtensionInfo(false);
	}

	ext_comm_mode = "check_prerequisites";
	ext_active_id = extension_id;

	startEXTrequest();
}

function confirmEnableExtension(ext_title)
{
	var ext_id = ext_active_id;

	stopEXTactivity();

	if (extension_info_div == null)
	{
		addExtensionInfo(true);
	}
	
	if(ext_title=="Blackline")
		ext_title="Lavu Integrated Payments";
	
		
	if (confirm("Please confirm that you would like to enable the " + ext_title + " extension."))
	{
		ext_comm_mode = "enable_extension";
		ext_active_id = ext_id;
		ext_active_title = ext_title;

		startEXTrequest();
	}
	else
	{
		extensionInfoClose();
	}
}

function initiatePayPalOnboarding()
{
    document.getElementById("pphsignup_form").submit();
}

function disableExtension(extension_id, ext_title, extension_codeword)
{
	if (extension_info_div == null)
	{
		addExtensionInfo(false);
	}

	if (confirm("Please confirm that you want to disable the " + ext_title + " extension."))
	{
		ext_comm_mode = "disable_extension";
		ext_active_id = extension_id;
		ext_active_title = ext_title;
		ext_active_codeword = extension_codeword;

		startEXTrequest();
		if (ext_title == "PayPal")
		{
			location.reload();
		}
		else if (ext_title == "LoyalTree White Label")
		{
			document.getElementById('loyaltree_id').disabled = false;
			document.getElementById('loyaltree_username').disabled = false;
			document.getElementById('loyaltree_password').disabled = false;
		}
		else if (ext_title == "Lavu Premium Loyalty")
		{
			document.getElementById('pepper_applicationid').disabled = false;
			document.getElementById('pepper_apitoken').disabled = false;
			document.getElementById('pepper_apiurl').disabled = false;
			document.getElementById('pepper_locationid').disabled = false;
			document.getElementById('pepper_registerid').disabled = false;
			document.getElementById('pepper_loyalty_model').disabled = false;
		}
		else if (ext_title == "Sacoa")
		{
			document.getElementById('sacoa_username').value = "";
			document.getElementById('sacoa_password').value = "";
			document.getElementById('sacoa_url').value = "";
		}
	}
	else
	{
		extensionInfoClose();
	}
}

function hideLoyalTreeWhiteLabelInputs(extensionId, ext_active_title, extension_codeword)
{
	document.getElementById('loyaltree_id').disabled = true;
	document.getElementById('loyaltree_username').disabled = true;
	document.getElementById('loyaltree_password').disabled = true;

	// Only enable extension if extensionId arg is passed in
	if (typeof(extensionId) != 'undefined')
	{
		enableExtension(extensionId, ext_active_title, extension_codeword);
	}
}

function hidePepperInputs(extensionId, ext_active_title, extension_codeword)
{
	 if(extensionId != 'pepper')
        {
                var pepper_applicationid =  document.getElementById("pepper_applicationid").value;
                var pepper_apitoken =  document.getElementById("pepper_apitoken").value;
                var pepper_apiurl =  document.getElementById("pepper_apiurl").value;
                var pepper_locationid =  document.getElementById("pepper_locationid").value;
                var pepper_registerid =  document.getElementById("pepper_registerid").value;
                if(pepper_applicationid == "" || pepper_apitoken == "" || pepper_apiurl == "" || pepper_locationid == "" || pepper_registerid == "")
                {
                        alert("Lavu Premium Loyalty login details not specified.\nPlease enter all required details.");
                        return;
                }
        }
	document.getElementById('pepper_applicationid').disabled = true;
	document.getElementById('pepper_apitoken').disabled = true;
	document.getElementById('pepper_apiurl').disabled = true;
	document.getElementById('pepper_locationid').disabled = true;
	document.getElementById('pepper_registerid').disabled = true;
	document.getElementById('pepper_loyalty_model').disabled = true;

	// Only enable extension if extensionId arg is passed in
	if (extensionId != 'pepper')
	{
		enableExtension(extensionId, ext_active_title, extension_codeword);
	}
}

function switchIndicateButton(ext_id, enabled)
{
	var indicate_button = document.getElementById("indicate_button_" + ext_id);
	if (!indicate_button)
	{
		return;
	}

	var ed_icon			= "icon_disabled2.png";
	var ed_class		= "ext_disabled";
	var ed_click		= "enableExtension(\"" + ext_id + "\", \"" + ext_active_title + "\", \"" + ext_active_codeword + "\");";
	var ed_text			= "Not enabled";
	var ed_change_class	= "enabledBtn";
	var ed_change_text	= "Enable";

	if (enabled)
	{
		ed_icon			= "icon_enabled2.png";
		ed_class		= "ext_enabled";
		ed_click		= "disableExtension(\"" + ext_id +"\", \"" + ext_active_title + "\", \"" + ext_active_codeword + "\");";
		ed_text			= "Enabled";
		ed_change_class	= "disabledBtn";
		ed_change_text	= "Disable";
	}

	var str = "<td valign='middle' style='position:relative; width:115px'><img src='areas/extensions/images/" + ed_icon +"' height='17' width='21' style='position:absolute; top:50%; margin-top:-8px'>";
	str += "<span class='" + ed_class + "' style='position:absolute; top:50%; left:25px; height:15px; margin-top:-7px'>" + ed_text + "</span></td>";
	str += "<td align='center'>";
	str += "<table cellspacing='0' cellpadding='0'><tr>";
	str += "<td class='" + ed_change_class + "_left'></td>";
	str += "<td class='" + ed_change_class + "_mid'><button class='" + ed_change_class + "' onclick='" + ed_click + "'>" + ed_change_text + "</button></td>";
	str += "<td class='" + ed_change_class + "_right'></td>";
	str += "</tr></table>";
	str += "</td>";
	str += "<td align='right'><a class='ext_learn_more' onclick='extensionLearnMore(\"" + ext_id + "\", \"" + (enabled?"1":"0") + "\")'>Learn More</a></td>";

	indicate_button.innerHTML = str;
}

function extensionInfoClose()
{
	main_content_div.style.height = hold_main_content_height + "px";
	if (extension_info_div)
	{
		main_content_div.removeChild(extension_info_div);
	}

	extension_info_div = null;

	ext_comm_mode = "";
	ext_active_id = "";
}

function adjustForExtensionInfo()
{
	document.getElementById("ei_main_top").style.display = "block";
	document.getElementById("ei_main_cell").style.display = "block";

	var ei_height = document.getElementById("ei_main_table").offsetHeight;

	main_content_div.style.height = ((hold_main_content_height < ei_height)?ei_height:hold_main_content_height) + "px";
	document.getElementById("ext_activity_div").style.display = "none";

	var ei_main_table_div	= document.getElementById("ei_main_table_div");
	var emtd_rect			= ei_main_table_div.getBoundingClientRect();
	var erT					= emtd_rect.top;
	var emtdH				= ei_main_table_div.offsetHeight;

	if (erT < 0)
	{
		var currentY = window.pageYOffset;
		var mcdH = main_content_div.offsetHeight;

		var diffY = 0;
		if (emtdH < mcdH)
		{
			diffY = (currentY - (currentY + erT) + 20);
			var TH = (diffY + emtdH);
			if (TH > mcdH)
			{
				diffY -= (TH - mcdH);
			}
			ei_main_table_div.style.top = diffY + "px";
		}

		window.scrollTo(0, (currentY + erT + diffY - 20));
	}
}

function indicateEXTactivity()
{
	var ead = document.getElementById("ext_activity_div");
	if (ead)
	{
		ead.style.display = "block";
	}

	var emt = document.getElementById("ei_main_top");
	if (emt)
	{
		emt.style.display = "none";
	}

	var emc = document.getElementById("ei_main_cell");
	if (emc)
	{
		emc.style.display = "none";
	}
}

function stopEXTactivity()
{
	var ead = document.getElementById("ext_activity_div");
	if (ead)
	{
		ead.style.display = "none";
	}

	var emt = document.getElementById("ei_main_top");
	if (!emt)
	{
		 extensionInfoClose();
	}
}

function startEXTrequest()
{
	indicateEXTactivity();

	var postVars = {};

	postVars.mode	= ext_comm_mode;
	postVars.dn		= ext_dn;
	postVars.loc_id	= ext_loc_id;
	postVars.ext_tz	= ext_tz;
	postVars.ext_id	= ext_active_id;
	postVars.ext_title = ext_active_title;
	postVars.ext_codeword = ext_active_codeword;

	if (ext_comm_mode == "load_details")
	{
		postVars.enabled = ext_enabled;
	}

	// LoyalTree White Label custom logic to get input fields
	if (ext_active_id == '19' && ext_comm_mode == 'enable_extension')
	{
		postVars.loyaltree_id = document.getElementById('loyaltree_id').value;
		postVars.loyaltree_username = document.getElementById('loyaltree_username').value;
		postVars.loyaltree_password = document.getElementById('loyaltree_password').value;
	}
	
	if (ext_active_title == 'Sacoa Playcard' && ext_comm_mode == 'enable_extension')
	{
		postVars.sacoa_username = document.getElementById('sacoa_username').value;
		postVars.sacoa_password = document.getElementById('sacoa_password').value;
		postVars.sacoa_url = document.getElementById('sacoa_url').value;
	}

	// Pepper custom logic to get input fields
	if (ext_active_id == '22' && ext_comm_mode == 'enable_extension')
	{
		postVars.pepper_enabled = '1';
		postVars.pepper_applicationid = document.getElementById('pepper_applicationid').value;
		postVars.pepper_apitoken = document.getElementById('pepper_apitoken').value;
		postVars.pepper_apiurl = document.getElementById('pepper_apiurl').value;
		postVars.pepper_locationid = document.getElementById('pepper_locationid').value;
		postVars.pepper_registerid = document.getElementById('pepper_registerid').value;
		postVars.pepper_loyalty_model = document.getElementById('pepper_loyalty_model').value;
	}

	if (ext_active_id == '25' && ext_comm_mode == 'enable_extension')
	{
		postVars.kiosk_devices = document.getElementById('kiosk_devices').value;
	}
	

	if (ext_comm_mode != "")
	{
		new EXTrequest(postVars);
	}
}

function EXTresponse(response)
{

	var respJSON = tryParseJSON(response);

	if(ext_active_title==''){
		ext_active_title=respJSON.info.title;
	}

	if (!respJSON)
	{
		displayError("bad_response");
		return;
	}

	if (typeof respJSON.status == "undefined")
	{
		displayError("bad_response");
		return;
	}

	var status = respJSON.status;
	if (status != "success")
	{
		displayError(status);
		return;
	}

	if (typeof respJSON.info != "object")
	{
		displayError("bad_response");
		return;
	}

	switch (ext_comm_mode)
	{
		case "load_details":

			document.getElementById("ei_main_cell").innerHTML = respJSON.info.description;
			window.setTimeout(function(){adjustForExtensionInfo()}, 200);

			break;

		case "check_prerequisites":

			var ext_title = respJSON.info.title;
			var pr_message = respJSON.info.pr_message;

			if (typeof ext_title!="undefined" && typeof pr_message!="undefined")
			{
				if (pr_message != "")
				{
					displayError("msg:" + pr_message);
				}
				else
				{
					confirmEnableExtension(ext_title);
				}
			}
			else
			{
				displayError("bad_response");
			}

			break;

		case "enable_extension":
		{
			var enabled_description = respJSON.info.description;
			var error = respJSON.info.error;

			if (typeof enabled_description!="undefined")
			{
				if (typeof error!="undefined" && error!="")
				{
					displayError("msg:" + error);
				}
				else
				{
					if (ext_active_id != 29) {
						switchIndicateButton(ext_active_id, true);
					}
					if (extension_info_div == null)
					{
						addExtensionInfo(true);
					}
					else
					{
						stopEXTactivity();
					}
					document.getElementById("ei_main_cell").innerHTML = respJSON.info.description;
					window.setTimeout(function(){adjustForExtensionInfo();}, 100);
					document.getElementById('sacoa_username').value = "";
					document.getElementById('sacoa_password').value = "";
					document.getElementById('sacoa_url').value = "";
					if (respJSON.ext_title === 'Lavu To Go') {
						lavutogoDataCheck();
					}
				}
			}
			else
			{
				displayError("bad_response");
			}

			break;
		}

		case "disable_extension":
		{
			var error = respJSON.info.error;

			if (typeof error!=="undefined")
			{
				if (error != "")
				{
					displayError("msg:" + error);
				}
				else
				{
					switchIndicateButton(ext_active_id, false);
					stopEXTactivity();
					alert("The " + ext_active_title  + " extension has been disabled.");
				}
			}
			else
			{
				displayError("bad_response");
			}

			break;
		}

		default:

			break;
	}
}

function tryParseJSON(jsonString)
{
	try
	{
		var o = JSON.parse(jsonString);
		if (o && typeof o==="object" && o!==null)
		{
			return o;
		}
	}
	catch (e) { }

	return false;
};

function displayError(error_code)
{
	var message = "Unknown error...";

	if (error_code.length>=4 && error_code.substring(0, 4)=="msg:")
	{
		message = error_code.substring(4);
	}
	else
	{
		switch (error_code)
		{
			case "bad_response":

				message = "An unrecognized response was returned by the server...";
				break;

			case "expired_session":

				message = "Your log in session has expired...";
				break;

			case "no_db":

				message = "An error occurred while trying to connect to the database...";
				break;

			case "no_ext":

				message = "An error occurred while trying to load extension data...";
				break;

			default:
				break;
		}
	}

	switch (ext_comm_mode)
	{
		case "load_details":

			extensionInfoClose();
			break;

		case "check_prerequisites":
		case "enable_extension":
		case "disable_extension":

			extensionInfoClose();
			stopEXTactivity();
			break;

		default:
			break;
	}

	alert(message);
}

function setKioskDeviceLimit(currentLimit)
{
	$("#kiosk_devices").val(currentLimit);
}

(function()
{
	function AJAXRequest(url, requestType, params, async)
	{
		if (arguments.length === 0)
		{
			return;
		}

		if (!requestType)
		{
			requestType = "";
		}

		if (!async)
		{
			async = false;
		}
		else
		{
			async = true;
		}

		switch (requestType.toUpperCase().trim())
		{
			case "POST":
			case "PATCH":
			case "PUT":

				break;

			case "GET":
			default :

				requestType = "GET";
				break;
		}

		if (!params)
		{
			params = "";
		}
		else if (params instanceof Object)
		{
			obj = params;
			arr = [];
			for (var k in obj)
			{
				arr.push(encodeURIComponent(k) + "=" + encodeURIComponent(obj[k]));
			}

			params = arr.join("&");
		}
		else if ("string" != typeof params)
		{
			params = '';
		}

		this._request = new XMLHttpRequest();
		this._request.open(requestType, url, async);
		this._request.addEventListener("readystatechange", this, false);
		if (requestType == "POST")
		{
			this._request.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
		}

		this._request.send(params);
	}

	function handleEvent(event)
	{
		if (event.type != "readystatechange")
		{
			return;
		}

		if (this._request.readyState === 4)
		{
			this._request.removeEventListener("readystatechange", this, false);
			if (this._request.status === 200)
			{
				this.success(this._request.responseText);
			}
			else
			{
				this.failure(this._request.status, this._request.responseText);
			}
		}
	}

	function success(responseText)
	{
		alert(responseText);
	}

	function failure(status, responseText)
	{
		alert(status);
		alert(responseText);
	}

	window.AJAXRequest = AJAXRequest;
	AJAXRequest.prototype = {};
	AJAXRequest.prototype.constructor = AJAXRequest;
	AJAXRequest.prototype.handleEvent = handleEvent;
	AJAXRequest.prototype.success = success;
	AJAXRequest.prototype.failure = failure;
})();

(function()
{
	function EXTrequest(params)
	{
		AJAXRequest.call(this, "areas/extensions/extensions_requests.php", "POST", params, true);
	}

	function success(response)
	{
		EXTresponse(response);
	}

	function failure(status, response)
	{
		EXTresponse("");
	}

	window.EXTrequest = EXTrequest;
	EXTrequest.prototype.constructor = EXTrequest;
	EXTrequest.prototype = Object.create(AJAXRequest.prototype);
	EXTrequest.prototype.success = success;
	EXTrequest.prototype.failure = failure;
})();


(function()
{
    function SquareLocationRequest(url, method)
    {
        AJAXRequest.call(this, url, method, "", true);
    }

    function success(response)
    {
		var respJSON = tryParseJSON(response);
		if(response.indexOf('disconnectlocation')>-1)
		{ 
			if(response.indexOf('fail')>-1)
			{
				alertpopup('Square accont disconnection failed, try again.','error');
			}
			else
			{
				alertpopup('Account Disconnected Successfully');
				//document.getElementById('squareLocName').innerHTML = '';
		  		location.reload();
			}         
		}
		if (response != undefined && respJSON.error == undefined) {
			if (respJSON.locations != undefined) {
				//document.getElementById('select_loc_id').innerHTML = respJSON.locations;
				if(respJSON.locations == 'no more locations')
				{
					//document.getElementById('divBackground').style.display = 'block';
					var msg = "<div style='padding:3px;'>No locations remaining  for your square account.</div><div>Please login <a href='https://squareup.com/dashboard/'>Square Dashboard</a> to create locations.</div>";
					alertpopup(msg,'error');
					//showMessageInfo('no more locations');
				}
				else {
					document.getElementById('info_div').innerHTML='';
					var loc = JSON.stringify(respJSON);
					squarelocationsShow(loc);
					//console.log(respJSON.locations);
				}
			
			} else if (respJSON.credential != undefined) {
				alert("Credential successfully added. Please enable square gateway.");
				closeSquareOnBoardingForm();
				location.reload();
			} else if(respJSON.updatelocation != undefined) {
				closeSquareOnBoardingForm();
				alertpopup('Square location saved successfully');
				if(location.href.indexOf('square=success')>-1){
					var urlsplit = location.href.split('&');
					location.href = urlsplit[0];
				}
				else
				{
					location.reload();
				}
			}
			/*else if(respJSON.disconnectlocation != undefined)
			{
				if(respJSON.disconnectlocation == 'fail')
				{
					alertpopup('Square accont disconnection failed, try again.','error');
				}
				else
				{
					alertpopup('Account Disconnected Successfully');
					document.getElementById('squareLocName').innerHTML = '';
					location.reload();
				}
			}*/
		} else {
			alert(respJSON.error);
			return false;
		}

    }

    function failure(status, response)
    {
        return tryParseJSON(response);
    }

    window.SquareLocationRequest = SquareLocationRequest;
    SquareLocationRequest.prototype.constructor = SquareLocationRequest;
    SquareLocationRequest.prototype = Object.create(AJAXRequest.prototype);
    SquareLocationRequest.prototype.success = success;
    SquareLocationRequest.prototype.failure = failure;
})();

	function openInNewTab(url) {
		var newUrl = url;
		var squareAuthInsert = new SquareLocationRequest("areas/extensions/square/square_ajax.php?action=oAuthInsert", 'POST');
		//document.getElementById('divBackground').style.display = 'block'; 
		window.location.href = newUrl;
	  	//var win =window.open(newUrl,'OAuth','width=660,height=540,toolbar=0,menubar=0,location=0');  
		//win.focus();
		//squarelocationsShow('','6GEEFXPH5H664',win);
	}
	function squarelocationsShow(reqstr)
	{
        var locationsjson = JSON.parse(reqstr).locations;
        if (locationsjson != '' && locationsjson != undefined) {
            document.getElementById('divBackground').style.display = 'block';

            var optstr = '<option value="">---Select Square Location---</option>';
            for (var i = 0; i < locationsjson.length; i++) {
                optstr += "<option value='" + locationsjson[i]['id'] + "'>" + locationsjson[i]['name'] + "</option>"
            }
            document.getElementById('location_id').innerHTML =optstr;
        } else {
            var message = "No locations found for your Square account.<br/>Please login <a href='https://squareup.com/dashboard/'>Square Dashboard</a> to create locations.</div>";
            alertpopup(message,'error')
		}

	}
	function updateSquareLocation() { 
        var locationId = document.getElementById('location_id').value; 
        if(locationId == "") {
            //alert('Please select square location');
			showMessageInfo('Please select square location');
        } else if ( location_id != "") {
			var locationName = document.getElementById('location_id').options[document.getElementById('location_id').selectedIndex].text;
        	// alert(locationName)
			var squareLocation = new SquareLocationRequest("areas/extensions/square/square_ajax.php?action=updateSquareLocation&loc_id=" + encodeURIComponent(locationId)+"&name="+encodeURIComponent(locationName), 'POST');
        }  
    }
    function reLinkSquareLocations()
	{
		var squareLocation = new SquareLocationRequest("areas/extensions/square/square_ajax.php?action=getSquareLocations", 'GET');
	}
	function disconnectSquareLocations(){
		var answer = confirm("This will revoke access token for all the locations mapped to your merchant\naccount. Henceforth, none of the locations will able to process any transaction.\n\nAre you sure you want to disconnect your Square account?")
		if (answer) {
		   var squareLocation = new SquareLocationRequest("areas/extensions/square/square_ajax.php?action=removeSquareLocations", 'POST');
		   
		}
	}
	function showMessageInfo(msg,type)
	{
		if(type=='success'){
			document.getElementById('info_div').style.color = '#aecd37';
		}
		else{
			document.getElementById('info_div').style.color = '#801C1C';
		}
		document.getElementById('info_div').innerHTML = msg;
		
		document.getElementById('info_div').style.display='block';
		
	}
	function closeSquareOnBoardingForm() {
        document.getElementById('divBackground').style.display = 'none';
        if(location.href.indexOf('square=success')>-1){
            var urlsplit = location.href.split('&');
            location.href = urlsplit[0];
        }
	}
	function alertpopup(message,type)
	{
		document.getElementById('divpopupmessage').style.display = 'block';
		document.getElementById('message_content').innerHTML = message;
		if(type=='error'){
			document.getElementById('message_content').style.color = '#000000';
		}
		else{
			document.getElementById('message_content').style.color = '#aecd37';
		}
			
	}
	function locationsPopupShow(msg,locationdata)
	{ 
		 squarelocationsShow(locationdata); 
		 showMessageInfo(msg,'success');
	}
	function closeMessagePopup() {
        document.getElementById('divpopupmessage').style.display = 'none'
	}

	function initiateSquareOnboarding()
	{
		alert("Square merchant access token details not specified.\n\nPlease click Get Started link to specify Access Token details.");
	}

	function blockScreen(message) {
		$('body').append('<div class="block-screen" style="display:none;background: #00000047;position: fixed;left: 0px;right: 0px;bottom: 0px;width: 100%;top: 0px;z-index: 9999;"> <div style="background: white;text-align: center;width: 300px;padding: 14px;font-size: 14px;margin: 10% auto;">'+message+'</div></div>');
		$('.block-screen').fadeIn('slow');
	}

	function unBlockScreen() {
		$('block-screen').fadeOut(function(){
			$(this).remove();
		});
	}

	function enableLavuToGoCheck() {
		var fname = document.getElementById('fname').value;
		var lname = document.getElementById('lname').value;
		var dob = document.getElementById('dob').value;
		var pay_type = document.getElementById('pay_type').checked;
  		var pay_id = pay_type ? 'pay_type' : 'pay_type2';
  		var pay_value =  document.getElementById(pay_id).value;
		var val_dob = validateDob(dob);
                if (val_dob) {
		        blockScreen("Enabling extension please wait...");
			$.ajax({
				url: "areas/extensions/lavu_togo/ajax_lavutogo_check.php",
				data: { fname : fname,lname :lname,dob :dob,pack :pay_value},
				type: "POST",
				async: true,
				success: function(data) {
					extensionInfoClose();
					if(data !== 'failure') {
						alert("Congrats! You can now use Lavu To Go 2.0 ");
						location.reload();
					}
					unBlockScreen();
				},
				error: function() {
					alert("Seems problem with server loading ");
					unBlockScreen();
				}
			});
		}
	}

	function butactive(){
		var fname = document.getElementById('fname').value;
		var lname = document.getElementById('lname').value;
		var dob = document.getElementById('dob').value;
		if(fname.length && lname.length && dob.length) {
			document.getElementById('submit').disabled = false;
			document.getElementById('submit').style.backgroundColor = '#3774d6';
			document.getElementById('submit').style.border = ' 1px solid #3774D2';
		} else {
			document.getElementById('submit').disabled = true;
			document.getElementById('submit').style.backgroundColor = '#ccc';
			document.getElementById('submit').style.border = ' 1px solid #ccc';
		}
	}
	function radioactive(){
		var pay_type = document.getElementById('pay_type').checked;
		var pay_id = pay_type ? 'pay_type' : 'pay_type2';
		if(pay_id=='pay_type'){
			document.getElementById('r1d1').style.border = '1px solid #3CA5D9';
			document.getElementById('r1d3').style.background = '#3CA5D9';
			document.getElementById('r1s1').style.color = '#FFFFFF';
			document.getElementById('r2d1').style.border = '1px solid #ccc';
			document.getElementById('r2d3').style.background = '#ccc';
			document.getElementById('r2s1').style.color = '#23163F';
		} else {
			document.getElementById('r2d1').style.border = '1px solid #3CA5D9';
			document.getElementById('r2d3').style.background = '#3CA5D9';
			document.getElementById('r2s1').style.color = '#FFFFFF';
			document.getElementById('r1d1').style.border = '1px solid #ccc';
			document.getElementById('r1d3').style.background = '#ccc';
			document.getElementById('r1s1').style.color = '#23163F';
		}
	}
	function lavutogoDataCheck(){
		$.ajax({
			url: "areas/extensions/lavu_togo/ajax_lavutogo_check_data.php",
			type: "POST",
			async: true,
			success: function(data) {
				if (data !='') {
					var dataArray = JSON.parse( data );
					document.getElementById("dob").value = dataArray.dob;
					document.getElementById("fname").value = dataArray.fname;
					document.getElementById("lname").value = dataArray.lname;
					document.getElementById('submit').disabled = false;
					document.getElementById('submit').style.backgroundColor = '#3774d6';
					document.getElementById('submit').style.border = ' 1px solid #3774D2';
					if (dataArray.pack==588) {
						document.getElementById('r2d1').style.border = '1px solid #3CA5D9';
						document.getElementById('r2d3').style.background = '#3CA5D9';
						document.getElementById('r2s1').style.color = '#FFFFFF';
						document.getElementById('r1d1').style.border = '1px solid #ccc';
						document.getElementById('r1d3').style.background = '#ccc';
						document.getElementById('r1s1').style.color = '#23163F';
						$("#pay_type2").prop("checked", true);
					}
				}
			},
			error: function() {
				alert("Seems problem with server loading ");
			}
		});
	}
	function validateDob(dob) {
		 var dobRGEX = /^(0[1-9]|1[012])\/\d{4}$/;
		 var dobResult = dobRGEX.test(dob);
		 if(dobResult == false) {
		   	 alert('Please enter a valid DOB (MM/YYYY)');
			 return false;
		 } else {
			var d = new Date();
			var today = [d.getMonth()+1, d.getDate(),  d.getFullYear()].join('/');
			dob = dob.replace("/", "/01/");
			dob = Date.parse(dob);
			today = Date.parse(today);
			if (today < dob) {
				alert("Future date is not allowed.");
				return false;
			}
		 }
		 return true;
	}
