<?php
/**
 * Created by PhpStorm.
 * User: hayat
 * Date: 8/26/17
 * Time: 8:59 PM
 */
session_start();
require_once(dirname(__FILE__) . "/../../../../components/payment_extensions/square/SquarePaymentHandler.php");
require_once(dirname(__FILE__) . "/../../../../components/payment_extensions/square/SquareTokenHelper.php");
require_once(dirname(__FILE__) . "/../../../../components/payment_extensions/square/SquareCore.php");
$data_name		= sessvar("admin_dataname");
$action         = reqvar("action", "");
$access_token   = reqvar("acc_token", "");
$isCP3          = isset($_GET['isCP3']);


if( $isCP3 && !$data_name ) {
    $headers = getallheaders();
    if( $headers["dataname"] !== NULL )  {
        $data_name = $headers["dataname"];
    }
}

if ($data_name == "") {
    $ajaxResponse['error'] = "Access Error: No Valid Dataname. May be your session expired.";
    echo json_encode($ajaxResponse);
    exit;
}


$access_token = ($access_token != '') ? $access_token : '';
$tokenHelper = new SquareTokenHelper($data_name);
$SquarePaymentHandler = new SquarePaymentHandler($data_name, $access_token);
$SquareCore = new SquareCore();

$ajaxResponse = array();
if($action == 'getLocation') {
    // if is cp3
    if($isCP3) {

        $locationdata =  $tokenHelper->getCredentialsByDataname($data_name);
        $ajaxResponse['selectedLocation'] = $locationdata;
    }else {
        $square_loc_id = reqvar("loc_id", "");
        $access_token  = reqvar("acc_token", "");

        // Get location from Square API
        $loc = $SquarePaymentHandler->getAllSquareLocation();
        // Get Credential from DB
        $credentialResponse = $tokenHelper->getAPICredentials($data_name);
        if(!isset($credentialResponse['error'])) {
            $square_loc_id = ($square_loc_id != '') ? $square_loc_id : $credentialResponse['locationID'];
        }
        if (!isset($loc['error'])) {
            if (!empty($loc['locations'])) {
                $locStr = '<select style="height:20px; font-size:14px; width:300px" id="location_id" name="location_id" onchange="selectSquareLocation()">';
                $locStr .= '<option value="">---Select Square Location---</option>';

                foreach ($loc['locations'] as $locIdName) {
                    $selected = ($locIdName['id'] == $square_loc_id) ? 'selected' : '';
                    $locStr .= '<option value="' . $locIdName['id'] . '" ' . $selected . '>' . $locIdName['name'] . '</option>';
                }

                $locStr .= '</select>';
            } else {
                $locStr = 'no more locations';
            }

            $ajaxResponse['locations'] = $locStr;
        } else {
            $ajaxResponse['error'] = $loc['error'];
        }
    }

}
else if($action == 'oAuthInsert') {
    $insertTokenTbl = $tokenHelper->insertGateWayToken($data_name);

    // if cp3 return $ajaxResponse
    if($isCP3) {
        $ajaxResponse["success"] = true;
        $ajaxResponse["message"] = "Successfully added gateway token";
    }
}
else if($action == 'checklocation'){
	$ajaxResponse = $tokenHelper->getTokenInfoByDataName($data_name);
}
else if($action == 'updateSquareLocation') {
    $loc_id     = reqvar("loc_id", "");
	$name       = reqvar("name", "");
	$squareCredentialArr['locationID']      = $loc_id;
    $squareCredentialArr['locationName']    = $name;

	$updateTokenTbl = $tokenHelper->updateSquareLocation($data_name,$squareCredentialArr);
	if ($updateTokenTbl) {
		$enableWebhook = $SquarePaymentHandler->enableWebhook($loc_id);
    }

    if($isCP3) {
        $ajaxResponse = array('message'=>'success', 'success'=>true);
    }else {
        $ajaxResponse = array('updatelocation'=>'success');
    }
}
elseif ($action == 'getSquareLocations'){
	$res = $SquareCore->getLocationsForDataname($data_name);
    if(count($res)<=0)
        if($isCP3) {
            $ajaxResponse = array();
        }else {
            $ajaxResponse = array('locations'=>'no more locations');
        }
	else
		$ajaxResponse = $res;
}
elseif ($action == 'removeSquareLocations') {
    $revokeInfo = $SquarePaymentHandler->revokeSquareToken();
    if (isset($revokeInfo['success']) && $revokeInfo['success'] == 1) {
        $insertTokenTbl = $tokenHelper->removeSquareLocation($data_name);
        $extid = $tokenHelper->getExtensionID();
        require_once(dirname(__FILE__) . "/../extensions_requests.php");
        $resp = array();
        enableDisableSquare($extid,$resp,FALSE);
        if($isCP3) {
            $ajaxResponse = array('message'=>'success', 'success'=>true);
        }else {
            $ajaxResponse = array('disconnectlocation'=>'success');
        }
	}
	else {
        if($isCP3) {
            $ajaxResponse = array('message'=>'fail', 'success'=>false, 'error'=>$revokeInfo['error']);
        }else {
            $ajaxResponse = array('disconnectlocation'=>'fail');
        }
	}
}

echo json_encode($ajaxResponse);
