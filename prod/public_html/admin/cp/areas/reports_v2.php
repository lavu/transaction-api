<?php
	if(isset($_GET['display_errors'])) ini_set('display_errors',1);
	require_once dirname( dirname(__FILE__) ) . '/reports_v2/report_functions.php';
	require_once dirname( dirname(__FILE__) ) . '/reports_v2/area_reports.php';

	if($in_lavu)
	{
		if(isset($_GET['printable']) && $_GET['printable']=="1")
		{
			echo "<script type=\"text/javascript\" src=\"//ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js\"> </script>";
			echo "<script language='javascript' src='reports_v2/ajax_lib.js'></script>
	<script language='javascript' src='reports_v2/floating_window.js'></script>
	<script type='text/javascript' src='resources/tigra/tcal.js'></script>
	<script type='text/javascript' src='reports_v2/chart.js'></script>
	<link rel='stylesheet' type='text/css' href='resources/tigra/tcal.css' />
	<link rel='stylesheet' type='text/css' href='styles/reports_v2.css' />
	<style>
		body, table { font-family:Verdana,Arial; font-size:10px; }
	</style>";

			$reports_query = ers_query("SELECT `title`, `id` FROM `reports` where `title` NOT LIKE 'test%' and `name` NOT LIKE 'alt%' and `_deleted`!='1'");
			$reports_result = array();
			while( $reports_result[] = mysqli_fetch_assoc( $reports_query ) ); array_pop( $reports_result );

			$mode = $_GET['mode'];
			if( isset($_GET['report']) ){
				$joutput = "";
				$joutput .= "<script type='text/javascript'>\n";
				$joutput .= "	function click_to_page(page_url) {\n";
				$joutput .= " 		Color.reset();\n";
				$joutput .= "		ajax_url = 'reports.display?report=".$_GET['report'];
				$joutput .= "&printable=1";
				$joutput .= "&' + page_url.replace(/\?/ig,'');\n";
				$joutput .= "		if(window.history.pushState) window.history.pushState(ajax_url, 'page', window.location.href);\n";
				$joutput .= "		ajax_link('report_container',ajax_url,'','','f_tcalInit');\n";
				$joutput .= "	}\n";
				$joutput .= "</script>";
				echo $joutput;
				$_GET['mode'] = (isset($_GET['time_mode']))?$_GET['time_mode']:$mode;
				require_once dirname(__FILE__) . '/reports/v2_entry.php';
				echo "<script type='text/javascript' src='reports_v2/reports_v2.js'></script>";
			}
			exit();
		}
$speak="speak";
// based on date format setting
$date_setting = "date_format";
$location_date_format_query = lavu_query( "SELECT `location`, `setting`, `value` FROM `config` WHERE `setting` = '[1]'",  $date_setting);

if( !$location_date_format_query || !mysqli_num_rows($location_date_format_query)){
	$date_format = array("value" => 2);
}else
	$date_format = mysqli_fetch_assoc( $location_date_format_query );

switch ($date_format['value']){
	case 1:$dateFormat = 'd/m/Y';
	break;
	case 2:$dateFormat = 'm/d/Y';
	break;
	case 3:$dateFormat = 'Y/m/d';
	break;
	default:$dateFormat = 'm/d/Y';
}
$dispay="";
$rpt_style = "";
$obj = new DBConnector();
$memc_conn = $obj->getLink('readreplica');
if ($memc_conn == '') {

		$dispay = "<div style='color:#FF0000;font-size: 12px;'> ".speak("The reports application is currently unavailable due to maintenance. <br> Please check back later or contact our support team for more information.")."</div>";
		$rpt_style = "display: none;";
}
		$reactStyle = $isNewControlPanel ? "style='display:none'" : "";
		echo <<<HTML
		{$dispay}
	<script>var cpDateFormatSetting='$dateFormat';</script>
	<script language='javascript' src='reports_v2/ajax_lib.js'></script>
	<script language='javascript' src='reports_v2/floating_window.js'></script>
	<script type='text/javascript' src='resources/tigra/tcal.js'></script>
	<script type='text/javascript' src='reports_v2/chart.js'></script>
	<link rel='stylesheet' type='text/css' href='resources/tigra/tcal.css' />
	<link rel='stylesheet' type='text/css' href='styles/reports_v2.css' />
	<style>
		body, table { font-family:Verdana,Arial; font-size:10px; }
	</style>
    <div {$reactStyle} style='display:inline-table; float:left;$rpt_style'>
        <table cellpadding=6><tr><td valign='middle'><img src='images/question_icon.png' /></td><td valign='middle' style='font-family:Verdana; font-weight:bold; font-size:12px; color:#3481a9'>{$speak("Looking for your old reports")}?</td><td valign='middle'><img src='images/arrow_icon.png' /></td><td width='120'>&nbsp;</td></tr></table>
    </div>
	<div {$reactStyle} style="text-align: right; padding: 5px 10px;$rpt_style">
		<a href="?mode=reports_end_of_day_new" style="color: transparent; display: inline-table; margin: 2px;">
			<div style="background: #3382A9; padding: 5px 10px; border-radius: 3px; color: white;">
				{$speak("End of Day / Batching (Version 1)")}
			</div>
		</a>
		<a href="?mode=reports_time_cards" style="color: transparent; display: inline-table; margin: 2px;">
			<div style="background: #3382A9; padding: 5px 10px; border-radius: 3px; color: white;">
				{$speak("Time Cards")}
			</div>
		</a>
		<a href="?mode=reports_menu_general" style="color: transparent; display: inline-table; margin: 2px;">
			<div style="background: #3382A9; padding: 5px 10px; border-radius: 3px; color: white;">
				{$speak("Other Version 1 Reports")}
			</div>
		</a>
	</div>
	<br />
HTML;
		$reports_query = ers_query("SELECT `title`, `id` FROM `reports` where `title` NOT LIKE 'test%' and `name` NOT LIKE 'alt%' and `_deleted`!='1'");
		$reports_result = array();
		$mode = $_GET['mode'];
		while( $reports_result[] = mysqli_fetch_assoc( $reports_query ) ); array_pop( $reports_result );

		function filterReports($reports_result, $validIds) {
			$filter_results = [];
			foreach ($reports_result as $rep) {
				if( in_array($rep['id'], $validIds) ) {
					array_push($filter_results, $rep);
				}
			}
			return $filter_results;
		}

		function moveElement($array, $arr, $back) {
			$out = array_splice($array, $arr, 1);
			array_splice($array, $back, 0, $out);
			return $array;
		}


		if( $isNewControlPanel ) {
			$rep_param = $_GET['report'];
			$salesAndPaymentsIds = ["30", "31", "36", "37", "42", "39", "71"];
			$laborIds = ["48", "35"];
			$inventoryIds = ["38", "49", "50", "51", "52", "53", "54"];
			$cashManagementIds = ["64", "65", "66", "68"];
			if( $rep_param === 'info' ) {
				$reports_result = [];
			}elseif ( in_array($rep_param, $salesAndPaymentsIds) ) {
				// Sales and Payments
				// a) Payments "30"
				// b) Sales "31"
				// c) Taxes "36"
				// d) Discounts "37"
				// e) Fiscal "42"
				// f) Lavu Gift "39"
				$reports_result = filterReports($reports_result, $salesAndPaymentsIds);
			}elseif ( in_array($rep_param, $laborIds) ) {
				// Sales and Payments
				// a) Labor 48
				// b) Action Log 35
				$reports_result = array_reverse(filterReports($reports_result, $laborIds));
			}elseif ( in_array($rep_param, $inventoryIds) ) {
				// Inventory
				// a) Food & Beverages 49
				// b) Ingredient Usage 38
				// c) Usage 50
				// d) Low Inventory 51
				// e) Waste 52
				// f) Transfer 53
				// g) Variance 54
				$inventory_result = filterReports($reports_result, $inventoryIds);
				$reports_result = moveElement($inventory_result, 0, 1);
			} else if (in_array($rep_param, $cashManagementIds)) {
				// Cash Management
				// a) Change Bank 72
				// b) Bank Deposit 73
				// c) General Ledger 74
				// d) Customer Deposits 76
				$reports_result = filterReports($reports_result, $cashManagementIds);
			}

		}

		echo "<div class='reporting_navigation'>";
		$attributes = "";
		if( !isset($_GET['report']) ){
			$attributes = ' selected';
		}
		if( !$isNewControlPanel ) {
			echo "<a href=\"index.php?mode={$mode}\"><div class='report_button'{$attributes}>".speak("Dashboard")."</div></a>";
		}
		$attributes = "";
		if( isset($_GET['report']) && $_GET['report']=="info" ){
			$attributes = ' selected';
		}
		if( ($isNewControlPanel && $rep_param === 'info') || !$isNewControlPanel ) {
			echo "<a href=\"index.php?mode={$mode}&report=info\"><div class='report_button'{$attributes}>".speak("Info")."</div></a>";
		}
		foreach( $reports_result as $key => $report_entry ){
			$attributes = "";
			if(isset($_GET['report']) && $_GET['report']==$report_entry['id']) $attributes = " selected";
			if ($report_entry['title'] =='Tip Sharing' && $countNumber == 0) {
			    continue;
			}
			echo "<a href=\"index.php?mode={$mode}&report={$report_entry['id']}\"><div class='report_button'{$attributes}>".speak($report_entry['title'])."</div></a>";
		}
		// echo "<div class='report_button'{$attributes} onclick='window.location = \"index.php?mode={$mode}&report=33\"'>End of Day</div>";
		echo "<div class='report_button' end></div>";
		echo "</div>";
		unset( $_GET['mode'] ); //Date Selectors used mode, this confuses them.

		$get_report = "";
        if(isset($_GET['report'])) $get_report = $_GET['report'];

		if( $get_report != "info" && $get_report != "" ){
			require_once dirname(__FILE__) . '/reports/v2_entry.php';
			echo <<<HTML
			<script type='text/javascript' src='reports_v2/reports_v2.js'></script>
			<script type='text/javascript' src='reports_v2/pagination.js'></script>

HTML;
		}
        else if($get_report=="")
        {
        	require_once(dirname(__FILE__) . "/../reports_v2/reports_home_page.php");
        }
		else if($get_report=="info")
		{
			echo <<<HTML
			<div id="report_container">
				<br />
				<table style="margin-top: 50px;">
					<tr>
						<td><iframe width="420" height="315" src="//www.youtube.com/embed/N4lUwJpRP8I" frameborder="0" allowfullscreen=""></iframe></td>
						<td><iframe width="560" height="315" src="//www.youtube.com/embed/Mundvvxfx5k" frameborder="0" allowfullscreen=""></iframe></td>
					</tr>
				</table>
				
HTML;

			require_once dirname(dirname(__FILE__)) . '/resources/change_notes.php';

			echo <<<HTML
			</div>
			<script language='javascript' src='reports_v2/reports_v2.js'></script>

HTML;
		}
	}
?>
