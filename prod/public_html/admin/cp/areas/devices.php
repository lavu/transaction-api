<?php

	if (!$in_lavu)
		return;

	global $o_package_container;
	require_once(dirname(__FILE__)."/../../sa_cp/billing/payment_profile_functions.php");
	require_once(dirname(__FILE__)."/../objects/deviceLimitsObject.php");
	require_once(dirname(__FILE__)."/../../sa_cp/billing/package_levels_object.php");
	require_once(dirname(__FILE__)."/../resources/json.php");
	$b_forced_limit_account = $o_device_limits->isForcediPadLimitAccount(admin_info('dataname'));
	if (!isset($o_package_container))
		$o_package_container = new package_container();
	
	$forward_to = "index.php?mode={$section}_{$mode}";
	$tablename = "devices";

	// get the device upgrades link string
	$a_restaurants = ConnectionHub::getConn('poslavu')->getDBAPI()->getAllInTable("restaurants", array('data_name'=>admin_info('dataname')), TRUE, array("selectclause"=>"`id`", "limitclause"=>"LIMIT 1"));
	$i_restaurant_id = (count($a_restaurants) > 0) ? $a_restaurants[0]['id'] : 0;
	$a_package = find_package_status($i_restaurant_id);
	$s_upgrade_device_limits = "";
	if (strtolower($o_package_container->get_printed_name_by_attribute('level', (int)$a_package['package'])) == "pro")
		$s_upgrade_device_limits = "<div class='device_link'><a href='index.php?mode=billing'>Click Here to upgrade your device limits</a></div>";

	// save the forced device limits settings
	if (isset($_POST['cmd'])) {
		if ($_POST['cmd'] == "set_force_device_limits") {
			if (!is_lavu_admin()) {
				echo "<!--message-->".LavuJson::json_encode(array("success"=>FALSE,"message"=>"You don't have permissions to use this feature."))."<!--endmessage-->";
				return;
			}
			$s_enabled = $_POST['forced'];
			$b_forced = (strtolower($s_enabled) == 'enable') ? TRUE : FALSE;
			$s_other = $b_forced ? "Disable" : "Enable";
			$b_success = $o_device_limits->saveForcedLimitsDatanames(admin_info('dataname'), $b_forced);
			if ($b_success) {
				echo "<!--message-->".LavuJson::json_encode(array("success"=>TRUE,"message"=>"Settings saved!","buttonVal"=>$s_other))."<!--endmessage-->";
			} else {
				echo "<!--message-->".LavuJson::json_encode(array("success"=>FALSE,"message"=>"Failed to save settings."))."<!--endmessage-->";
			}
			return;
		}
	}

	if (isset($_POST['posted'])) {

		$package_info = $o_device_limits->getDeviceLimitsByDataname(admin_info('dataname'));

		$pName = $package_info['pName'];
		$iPod_max = $package_info['iPod_max'];
		$iPad_max = $package_info['iPad_max'];
		$tablesideIPadMax = $package_info['tableside_max_ipads'];

		$activate_ids = array(0);
		$iPad_count = 0;
		$tableside_iPad_count = 0;
		$iPod_count = 0;
		$keys = array_keys($_POST);
		foreach ($keys as $key) {
			if (substr($key, 0, 7) == "device_") {
				$info = explode("_", $key);
				$activate_ids[] = $info[1];

				$ut = $_POST['usage_type_'.$info[1]];

				if ($_POST[$key]=="iPad" && $ut=="Terminal") $iPad_count++;
				else if ($_POST[$key]=="iPad") $tableside_iPad_count++;
				else $iPod_count++;
			}
		}

		if ($iPad_count > $iPad_max) echo "<script language='javascript'>alert(\"You've tried to activate too many iPad Terminals. The limit is ".$iPad_max.".\");</script>";
		else if ($iPod_count > $iPod_max) {
			$dvc = "iPhones/iPods";
			echo "<script language='javascript'>alert(\"You've tried to activate too many ".$dvc.". The limit is ".$iPod_max.".\");</script>";
		}elseif ($tableside_iPad_count > $tablesideIPadMax) {
			$dvc = "tableside device";
			if ($tableside_iPad_count > 0){
				$dvc = "tableside devices";
			}
			echo "<script language='javascript'>alert(\"You've tried to activate too many ".$dvc.". The limit is ".$tablesideIPadMax.".\");</script>";
		} else {
			$update_active = lavu_query("UPDATE `devices` SET `slot_claimed` = IF(`id` IN (".implode(",", $activate_ids)."), '[1]', '') WHERE `loc_id` = '[2]' AND `inactive` != '1'", date("Y-m-d"), $locationid);
			schedule_remote_to_local_sync_if_lls($location_info, $locationid, "table updated", "devices");
		}
	}

	$vars = array();
	$vars['loc_id'] = $locationid;
	$filter = "`loc_id` = '[loc_id]' AND `inactive` != '1' AND `usage_type` != 'kiosk'";
	if (!is_lavu_admin()) $filter .= " AND `lavu_admin` != '1'";

	$lavu_only = "<font color='#588604' style='font-size:14px'><b>*</b></font> ";
	$lavu_admin = "<font color='#0000CC'><b>*</b></font>";
	$reseller_admin = "<font color='#FF6600'><b>*</b></font>";
	
	$get_devices = lavu_query("SELECT `id`, `model`, `specific_model`, `usage_type`, `name`, `system_version`, `ip_address`, `UUID`, `UDID`, `slot_claimed`, `lavu_admin`, `app_name`, `poslavu_version`, `device_time`, `reseller_admin` FROM `devices` WHERE $filter ORDER BY `model`, `name`", $vars);
	if (@mysqli_num_rows($get_devices) > 0) {
		
		if ($package_info['level'] < 3) echo "<br><a class='device_link' style='text-decoration:none;' href='index.php?mode=billing'>Click Here to upgrade your device limits</a><br>";
		echo "<br><br>";
		echo "<table cellspacing='0' cellpadding='3'>
			<tr>
				<td style='border:2px solid #DDDDDD' align='center'>
					<form method='post' action='$forward_to'>
						<input type='hidden' name='posted' value='1'>
						<table>
							<tr>
								<td>
									<table cellspacing=1 cellpadding=2>
										<tbody>
											<tr height='25px' bgcolor='#EEEEEE'>
												<td></td>
												<td align='center' width='180px'>".speak("Model")."</td>
												<td align='center'>".speak("Name")."</td>
												<td align='center' width='60px'>iOS</td>
												<td align='center' width='105px'>".speak("App")."</td>
												<td align='center' width='105px'>".speak("IP Address")."</td>
												<td align='center' width='105px'>".speak("Device ID")."</td>
												<td align='center' width='105px'>".speak("Last Check In")."</td>
												<td align='center' width='100px'>".speak("Usage Type")."</td>";
		if (is_lavu_admin()) echo "<td align='center' width='80px'>".$lavu_only.speak("Active")."</td>";
		echo "</tr>
											<tr><td colspan='10' height='1px'><hr style='height: 1px; margin:1px 0px 5px 0px;'></td></tr>";

		$platform_list = iOSplatformList();
		$has_reseller_admin = false;

		// based on date format setting
		$date_setting = "date_format";
		$location_date_format_query = lavu_query( "SELECT `location`, `setting`, `value` FROM `config` WHERE `setting` = '[1]'",  $date_setting);
		if( !$location_date_format_query || !mysqli_num_rows($location_date_format_query)){
			$date_format = array("value" => 2);
		}else
			$date_format = mysqli_fetch_assoc( $location_date_format_query );

		while ($info = mysqli_fetch_assoc($get_devices)) {

			$model = (!empty($info['specific_model']))?$info['specific_model']:$info['model'];
			if (isset($platform_list[$model])) $model = $platform_list[$model];
	
			$usage_type = "";
			if (strstr($info['model'], "iPad")) {
				$usage_type = empty($info['usage_type'])?"Terminal":ucwords($info['usage_type']);
				$ut = $info['usage_type'];
			} else $usage_type = "Tableside";
			$usage_type .= "<input type='hidden' name='usage_type_".$info['id']."' value='".$usage_type."'>";

			$checked = ($info['slot_claimed'] == date('Y-m-d'))?" checked":"";
			$checkbox = "<input name='device_".$info['id']."' type='checkbox' value='".$info['model']."'$checked>";
	
			$admin_indicator = "";
			if ($info['lavu_admin'] == "1") $admin_indicator = $lavu_admin;
			else if ($info['reseller_admin'] == "1") {			
				$admin_indicator = $reseller_admin;
				$has_reseller_admin = true;
			}
			if ($admin_indicator != "") $checkbox = $admin_indicator;
			
			$app_version = "";
			if (isset($info['app_name'])) {
				$app_version = $info['app_name'];
				if (isset( $info['poslavu_version'])) {
					$version = explode(" ", $info['poslavu_version']);
					$app_version .= " ({$version[0]})";
				}
			}

			$app_version = str_ireplace("POSLavu Client", "POS Lavu", $app_version);

			$device_id = "";
			if (!empty($info['UUID']) && $info['UUID']!="-") {
				$uuid = explode("-", $info['UUID']);
				$device_id = $uuid[0];
				if (count($uuid) >= 2) $device_id .= "-".$uuid[1];
			}
			if (empty($device_id)) $device_id = $info['UDID'];

			$split_date = explode(" ", $info['device_time']);
			$change_format = explode("-", $split_date[0]);
			switch ($date_format['value']){
				case 1:$info['device_time'] = $change_format[2]."-".$change_format[1]."-".$change_format[0]." ".$split_date[1];
				break;
				case 2:$info['device_time'] = $change_format[1]."-".$change_format[2]."-".$change_format[0]." ".$split_date[1];
				break;
				case 3:$info['device_time'] = $change_format[0]."-".$change_format[1]."-".$change_format[2]." ".$split_date[1];
				break;
			}

			echo "<tr>
				<td>".$admin_indicator."</td>
				<td align='left'>".$model."</td>
				<td align='left' style='font-size:11px;'>".$info['name']."</td>
				<td align='center' style='font-size:10px;'>".$info['system_version']."</td>
				<td align='center' style='font-size:10px;'>".$app_version ."</td>
				<td align='center' style='font-size:10px;'>".$info['ip_address']."</td>
				<td align='center' style='font-size:10px;'>".$device_id."</td>
				<td align='center' style='font-size:10px;'>".$info['device_time']."</td>
				<td align='center' style='font-size:10px;'>".$usage_type."</td>";
			if (is_lavu_admin()) echo "<td align='center'>$checkbox</td>";
			echo "</tr>";
		}

		echo "<tr><td colspan='10'><hr></td></tr>
									</table>
								</td>
							</tr>
							<tr><td align='center' valign='middle' height='35px'><input type='submit' value='".speak("Save")."' style='width:120px'></td></tr>
						</table>
					</form>
				</td>
			</tr>
		</table>";

		if (is_lavu_admin()) {
			echo "<br>".$lavu_only."Indicates items visible only to Lavu Admin users (i.e., developers and support staff)<br>";
			echo "<br>".$lavu_admin." Indicates Lavu Admin devices<br>";
		}
		if ($has_reseller_admin) echo "<br>".$reseller_admin." Indicates Specialist devices";

	} else {

		echo "<br><br>No active devices found...";
	}

	echo "<br><br><br>";

?>
