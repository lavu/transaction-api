<?php

if (!$in_lavu)
{
	exit();
}

$server_levels = array(
	'1' => "1",
	'2' => "2",
	'3' => "3",
	'4' => "4"
);

$font_sizes_text = array(
	'Standard'		=> speak("Standard"),
	'Emphasize'		=> speak("Emphasize"),
	'Double Height'	=> speak("Double Height")
);

$font_sizes_graphical = array(
	'24' => "24",
	'28' => "28",
	'32' => "32",
	'36' => "36",
	'40' => "40",
	'44' => "44"
);

$lavu_only	= "<font color='#588604' style='font-size:14px'><b>*</b></font> ";
$forward_to = "index.php?mode={$section}_{$mode}";
$tablename	= "locations";
$rowid		= $locationid;

$fields = array(
	array(
		speak("Direct Printing"),
		"Direct Printing",
		"seperator"
	),
	array(
		speak("Enable legacy printing (no background printing)").":",
		"config:no_background_printing",
		"bool"
	),
	array(
		speak("Access level required to use front end Printer Setup").":",
		"config:printer_setup_min_access_level",
		"select",
		$server_levels
	),
	array(
		speak("Kitchen ticket font size").":",
		"kitchen_ticket_font_size",
		"select",
		$font_sizes_text
	),
	array(
		speak("Kitchen ticket font size in graphics mode").":",
		"raster_mode_font_size1",
		"select",
		$font_sizes_graphical
	),
	array(
		speak("Print modifiers in red on kitchen ticket").":",
		"modifiers_in_red",
		"bool"
	),
	array(
		speak("Print forced modifiers in separate lines on kitchen tickets").":",
		"config:print_forced_modifiers_kitchen_tickets",
		"bool"
	),
    array(
        speak("Print optional modifiers in separate lines on kitchen tickets").":",
        "config:print_optional_modifiers_kitchen_tickets",
        "bool"
    ),
    array(
        speak("User that sends items to kitchen will be printed as Server on kitchen tickets").":",
        "config:print_as_server_on_kitchen_tickets",
        "bool"
    ),
	array(
		speak("Receipt font size").":",
		"receipt_font_size",
		"select",
		$font_sizes_text
	),
	array(
		speak("Receipt font size in graphics mode").":",
		"raster_mode_font_size2",
		"select",
		$font_sizes_graphical
	),
	array(
		speak("Include company logo on receipts").":",
		"print_logo_on_receipts",
		"bool"
	),
	array(
		speak("Order receipts respect category print order").":",
		"config:receipts_respect_print_order",
		"bool"
	),
	array(
		speak("Automatically send new API orders to the kitchen").":",
		"config:api_order_auto_send",
		"bool"
	),
	array(
		speak("Send void tickets to the kitchen when an item or order is voided").":",
		"config:send_void_kitchen_ticket",
		"bool"
	),
	array(
		speak("Print void ticket heading in red").":",
		"config:void_ticket_heading_red",
		"bool"
	)
);

if (is_lavu_admin())
{
	$fields[] = array(
		$lavu_only.speak("Drawer pulse pause").":",
		"config:drawer_pulse_pause",
		"select",
		array(
			'0.05'	=> "0.05",
			'0.10'	=> "0.10",
			'0.15'	=> "0.15",
			'0.20'	=> "0.20",
			'0.25'	=> "0.25",
			'0.60'	=> "0.60",
			'0.90'	=> "0.90",
			'1.20'	=> "1.20",
			'1.60'	=> "1.60",
			'2.00'	=> "2.00"
		)
	);
}

$fields = array_merge( $fields, array(
	array(
		speak("Save"),
		"submit",
		"submit"
	)
));

$form_fixed_width = 704;
require_once(resource_path()."/form.php");

if (isset($_GET['show_connector']))
{
	echo "<br><br>Location Id: ".$locationid;
	echo "<br>Connector Name: ".admin_info("dataname");
	echo "<br>Connector Key: ".lsecurity_key(admin_info("companyid"));
}

echo "<script type='text/javascript'>
	document.getElementsByClassName('tableSpacing')[0].style.width = '700px';
</script>";
