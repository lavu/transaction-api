/*************
	
	GLOBALS

**************/

var selectedRoom = -1;
var isNewRoom = false;

var windowWidth = 1000;
var windowHeight = 600;

var toolBoxWidth = 100;
var toolBoxHeight = 500;

var tableStandardWidth = 50;
var tableStandardHeight = 50;

var increaseSizeWhenDragging = 20;

var paper;

var toolBox;
var masterCircle;
var masterBox;

var activeElement;

elementsArray = {};
elementsCenter={};
elementsProperties = {};
labelsArray = {};

var rotate;

var xOffset = 56;
var yOffset = 36;


var elementCounter = 0;

trashHeight = 0;
trashWidth = 0;
trashX = 0;
trashY = 0;

changed = 0;

obScale = 0.80;

tablesOutOfRange = 0;

tableProperties = [];
circleStart = 0;
boxStart = 0;
slantLeftStart=0;
slantRightStart=0;
diamondStart=0;

circleDropped = 0;
boxDropped = 0;
slantLeftDropped=0;
slantRightDropped=0;
diamondDropped=0;
var touchOptions = {};

var init= true; 
var apply=true;

eventCounter=0;
currentCount=0;

var greenFill = {
    	//fill: 'green',
    	fill:	'100-#c6c9b7',
    	//fill: '90-#e7eef9:50-#c7cbd1',
    	stroke: '#999',
    	'stroke-opacity':1.0,
    	'stroke-width':	1,
    	cursor: 'move',
    };
    
var blueFillSelected = {
	//fill: 'blue',
	fill:	'100-#8e96b9',
	//fill: '90-#e7eef9:50-#c7cbd1',
	//stroke: '#999',
	opacity: .4,
	stroke:	'#FFF',
	'stroke-opacity':1.0,
	//'stroke-width':3
	'stroke-width':2
};    

    
var iconFill = {
	fill: 'green',
    cursor: 'pointer'
};

var fontAttr = { 
	"font-size": 16, 
	"font-family": "Arial, Helvetica, sans-serif" 
};


var currentCenterX, currentCenterY, currentWidth, currentHeight, currentRotation, currentX, currentY;
var init=false;

/***********

	END GLOBALS

***********/
	
function lavuLog(log) {
	console.log(log);
}

// pulls the revenue center from the 'activeElement'
// if the active element doesn't have a revenue center, it looks for one in the 'nonActiveSubject'
// if the nonActiveSubject doesn't have a revenue center, the function returns -2
function getRevenueCenter(nonActiveSubject, ignoreActive) {
	var first = activeElement;
	var second = nonActiveSubject;
	var revenueCenter = -2;
	
	if (ignoreActive)
		first = null;
	
	// try first first
	if (first) {
		// try the nested subject first
		if (first.subject && first.subject.attrs && first.subject.attrs['revenueCenter']) {
			revenueCenter = first.subject.attrs['revenueCenter']
		} else if (first.attrs && first.attrs['revenueCenter']) {
			revenueCenter = first.attrs['revenueCenter'];
		}
	}
	// try second second
	if (revenueCenter == -2 && second) {
		// try the nested subject first
		if (second.subject && second.subject.attrs && second.subject.attrs['revenueCenter']) {
			revenueCenter = second.subject.attrs['revenueCenter']
		} else if (second.attrs && second.attrs['revenueCenter']) {
			revenueCenter = second.attrs['revenueCenter'];
		}
	}
	
	// if revenue center isn't set for the objects but one of the objects exist, set it to -1
	if (revenueCenter == -2) {
		if (first && (first.attrs || first.subject.attrs))
			revenueCenter = -1;
		if (second && (second.attrs || second.subject))
			revenueCenter = -1;
	}
	
	return revenueCenter;
}

function updateRevenueCenterSelect(subject) {
	var revenueCenter = getRevenueCenter(subject);
	if (revenueCenter != -2) {
		$("#selectRevenueCenter").val(revenueCenter);
		if (activeElement && activeElement.attrs && activeElement.attrs['revenueCenter'])
			activeElement.attrs['revenueCenter'] = revenueCenter;
	}
}

function elementClicked(subject, old){

	saveCurrentSelectionProperties();
	
	activeElement =  subject.subject;
	
	populateProperties();

	for(elementID in elementsArray) {
		elementsArray[elementID].subject.attr(greenFill);
		
		hideHandles(elementsArray[elementID]);

	}
	subject.subject.attr(blueFillSelected);
	
	updateRevenueCenterSelect(subject);
	
	showHandles(subject);
	lavuLog("revenue center: " + getRevenueCenter(subject));
}

function showHandles(subject){
    subject.subject.freeTransform.handles.x.disc.attr({opacity: 1.0});
    subject.subject.freeTransform.handles.y.disc.attr({opacity: 1.0});
    //subject.subject.freeTransform.handles.center.disc.attr({opacity: 1.0});
    	
    subject.subject.freeTransform.handles.x.line.attr({opacity: 0.5});
    subject.subject.freeTransform.handles.y.line.attr({opacity: 0.5});
   	
   	// set the display style to hidden so that users don't click on an invisible item
	xdisc = subject.subject.freeTransform.handles.x.disc[0];
	ydisc = subject.subject.freeTransform.handles.y.disc[0];
	if ($(xdisc)) $(xdisc).show();
	if ($(ydisc)) $(ydisc).show();
}

function hideHandles(element){
    element.handles.x.disc.attr({opacity: 0.0});
    element.handles.y.disc.attr({opacity: 0.0});
    //element.handles.center.disc.attr({opacity: 0.0});
    	
   	element.handles.x.line.attr({opacity: 0.0});
   	element.handles.y.line.attr({opacity: 0.0});
   	
   	// set the display style to hidden so that users don't click on an invisible item
	xdisc = element.handles.x.disc[0];
	ydisc = element.handles.y.disc[0];
	if ($(xdisc)) $(xdisc).hide();
	if ($(ydisc)) $(ydisc).hide();
}

function populateProperties(){
	elementName = ''
	if(elementsProperties[activeElement.node.id]!=undefined){
		elementName = elementsProperties[activeElement.node.id]['name'];
	}

	if(activeElement!=undefined && elementName != undefined){
		$('#elementName').val(elementName);
	}
	
}

function saveCurrentSelectionProperties(){
	elementName = $('#elementName').val();
	
	if(activeElement!=undefined){
		elementsProperties[activeElement.node.id]['name']
		//console.log(elementsProperties[activeElement.node.id]);

		elementsProperties[activeElement.node.id]['name'] = elementName;
		//for(label in labelsArray)
			//lavuLog(label+"=>"+labelsArray[label]);
		
		label = labelsArray[activeElement.node.id];
		
		label.attr({text:elementName});
	}	
}

/***
The purpose of this function is to update the boxes to the left that 
keep track of the x, y, width, height, and angle of the element selected. 
***/
function updateTableInfo(){
	elementName = $('#elementName').val();
	
	if(activeElement!=undefined){
		elementsProperties[activeElement.node.id]['name'] = elementName;
		label = labelsArray[activeElement.node.id];
		
		label.attr({text:elementName});
	}	
}


function deleteElement(){
	changed++;
	trashNormal();

	if(activeElement==undefined){
		return;
	}

	paper.freeTransform(elementsArray[activeElement.node.id].subject).unplug();
	
	elementsArray[activeElement.node.id].subject.animate({
    	width: 0, 
    	height: 0, 
    	opacity: 0.0
    	},
    	500, ">");
        
    labelsArray[activeElement.node.id].remove();
    //elementsProperties[activeElement.node.id].remove();
    //elementsArray[activeElement.node.id].remove();
    
    delete labelsArray[activeElement.node.id];
    delete elementsProperties[activeElement.node.id];
    delete elementsArray[activeElement.node.id];
    
    //console.log(elementsArray[activeElement.node.id]);
    	
    activeElement = undefined;
    
    $('#elementName').val('');
}

/*
	These will be used when we convert everything over and get vectors in the app
function createNewCircle(){
	changed++;
	createEllipse(300,300, tableStandardWidth, tableStandardHeight, 0, 'table',400,300);
}

function createNewBox(){
	changed++;
	createBox(600,300, tableStandardWidth*2, tableStandardHeight*2, 0, 'table',400, 300);
}*/

function createNewSquare(){
	changed++;
	var revenueCenter = $("#selectRevenueCenter").val();
	createOldBox('square', -42, 300, tableStandardWidth, tableStandardHeight, 0, 'table', revenueCenter, 400, 300);
}
function createNewCircle(){
	changed++;
	var revenueCenter = $("#selectRevenueCenter").val();
	createOldBox('circle', -42, 300, tableStandardWidth, tableStandardHeight, 0, 'table', revenueCenter, 400, 300);
}
function createNewSlantLeft(){
	changed++;
	var revenueCenter = $("#selectRevenueCenter").val();
	createOldBox('slant_left', -42, 300, tableStandardWidth, tableStandardHeight, 0, 'table', revenueCenter, 400, 300);
}
function createNewSlantRight(){
	changed++;
	var revenueCenter = $("#selectRevenueCenter").val();
	createOldBox('slant_right', -42, 300, tableStandardWidth, tableStandardHeight, 0, 'table', revenueCenter, 400, 300);
}
function createNewDiamond(){
	changed++;
	var revenueCenter = $("#selectRevenueCenter").val();
	createOldBox('diamond', -42, 300, tableStandardWidth, tableStandardHeight, 0, 'table', revenueCenter, 400, 300);
}
function createEllipse(typeIn,x,y, width, height, rotation, ellipseName, centerX, centerY){
	ellipse = paper.ellipse(x, y, width, height); 
	lavuLog("In Create ellipse Center x and Y "+centerX+" "+ centerY);
	if( centerX && centerY)
		ellipse.center( centerX, centerY);
	else{
		centerX= ellipse.getBBox()['x']+80;
		centerY=ellipse.getBBox()['y']+80;
		lavuLog("AssigningVals");

	}
	ellipse.attr(greenFill);
	id = 'element'+elementCounter++;
	
	ellipse.node.id = id;
	
	var label = paper.text(centerX,centerY-20,ellipseName);
	label.attr(fontAttr);
	
	freeTransformEllipse = paper.freeTransform(ellipse, {keepRatio:false}, transformCallback);
	
	if(rotation!=undefined){

		freeTransformEllipse.attrs.rotate = rotation;
		freeTransformEllipse.apply();
	}

	elementsArray[id] = freeTransformEllipse;
	
	if( centerX!=0 && centerY!=0)
		elementsCenter[ellipse.node.id]= { cx: centerX, cy: centerY};
	else
		elementsCenter[ellipse.node.id]={ cx:freeTransformEllipse.handles.center.disc.attrs.cx,
										  cy:freeTransformEllipse.handles.center.disc.attrs.cy };
	labelsArray[id] = label;

	elementClicked({subject: ellipse});
	if( centerX !=0 && centerY!=0)
		ellipse.center( centerX, centerY);
	$('#elementName').val(ellipseName);
	elementsProperties[id]={name: ellipseName, type: typeIn};
	

}

function createBox(typeIn,x,y, width, height, rotation, boxName, revenueCenter, centerX, centerY){

	box = paper.rect(x, y, width, height);
	box.center( centerX, centerY);
	box.attr(greenFill);
	box.attr(revenueCenter);
	//lavuLog( box);
	box.node.id ='element'+(elementCounter);
	if(!centerX && !centerY){
		centerX= box.getBBox()['x']+80;
		centerY=box.getBBox()['y']+80;
		lavuLog(box.getBBox());

	}
	var label = paper.text(centerX,centerY-20,boxName);
	label.attr(fontAttr);
	
	freeTransformBox = paper.freeTransform(box, {keepRatio:false}, transformCallback);
	if(rotation!=undefined){

		freeTransformBox.attrs.rotate = rotation;
		freeTransformBox.apply();
	}

	//freeTransformBox.subject.attrs["revenueCenter"] = revenueCenter;
	elementsArray[box.node.id] = freeTransformBox;
	elementsCenter[box.node.id]= { cx: centerX, cy: centerY};
	
	labelsArray[box.node.id] = label;
	
	elementClicked({subject: box});
	box.center( centerX, centerY);
	
	$('#elementName').val(boxName);
	elementsProperties[box.node.id]={name: boxName, type:typeIn};

}

function createOldBox(typeIn,x,y, width, height, rotation, name, revenueCenter, centerX, centerY){
	// verify the input
	if (typeof(revenueCenter) == "string") {
		if (revenueCenter.length == 0) {
			revenueCenter = -1;
		} else {
			revenueCenter = parseInt(revenueCenter);
		}
	}
	// wooo! holy hack!
	// (sincerely, Benjamin Bean)
	if (navigator.platform && navigator.platform.indexOf("iPad") !== -1 && x < 0)
	{
		x = 100;
		y = 100;
		elementCounter++;
	}
	// ok, continue with (good?) code
	if (x >= 0)
	{
		//making the object 
		var old= getOld(typeIn,x,y,width,height);
		lavuLog( "x: "+ x+" y: "+ y+" width: "+width+" height: "+ height );
		old.node.id='element'+(elementCounter);
		//Making the label
		var label= paper.text(x+width/2, y+(height/2)-20,name );
		label.attr(fontAttr);
		lavuLog(old);
		//Making the freetransform stuff.
		freeTransformOld = paper.freeTransform(old, {keepRatio:false, rotate:false }, transformCallback);
		freeTransformOld.apply();
		freeTransformOld.attrs["revenueCenter"] = revenueCenter;
		elementsArray[old.node.id] = freeTransformOld;
		
		labelsArray[old.node.id] = label;
			
		elementClicked({subject: old}, true);
		old.center( 0, 0);
		
		$('#elementName').val(name);
		elementsProperties[old.node.id]={name: name, type:typeIn};
		
		updateCircles();
		updateRevenueCenterSelect(freeTransformOld);
	}
}

/**
   Old will be where we store the old style of table these are so that we can 
   be backwards compatible so they are only able to be stretched and 
**/
function createOld(table){
	//making the object 
	//alert (name);
	var typeIn = table['type']; var x = table['x']; var y = table['y']; var width = table['width'];
	var height = table['height']; var name = table['name']; var revenueCenter = table['revenueCenter'];
	// returns a raphael image
	var old= getOld(typeIn,x,y,width,height);
	//alert( "x: "+ x+" y: "+ y+" width: "+width+" height: "+ height );
	old.node.id='element'+(++elementCounter);
	//Making the label
	var label= paper.text(x+width/2, y+(height/2)-20, name);
	label.attr(fontAttr);
	lavuLog(old);
	//Making the freetransform stuff.
	freeTransformOld = paper.freeTransform(old, {keepRatio:false, rotate:false }, transformCallback);
	freeTransformOld.apply();
	freeTransformOld.attrs["revenueCenter"] = revenueCenter;
	elementsArray[old.node.id] = freeTransformOld;
	
	labelsArray[old.node.id] = label;
		
	elementClicked({subject: old}, true);
	old.center( 0, 0);
	
	$('#elementName').val(name);
	elementsProperties[old.node.id]={name: name, type:typeIn};


		
}
//This function gets the image that will be used for compatibility with the ipad and with 
//the old backend
function getOld(type, x, y, width, height, revenueCenter){
	var image;
	if(type== 'circle')
	return paper.image('./images/table_circle.png',x,y, width, height);
	else if( type== 'slant_left' )
	return paper.image('./images/table_slant_left.png',x,y, width, height);
	else if( type== 'slant_right')
		return paper.image('./images/table_slant_right.png',x,y, width, height);
	else if( type=='square'      )
		return paper.image('./images/table_square.png',x,y, width, height);
		
	else 
		return paper.image('./images/table_diamond.png',x,y, width, height);
}



Raphael.el.center= function(centerX, centerY){
	this.attr({cx: centerX, cy: centerY});

}

//All that the callback should be used for is to update the boxes on the left
function transformCallback(subject, events ){
	if(subject){
		eventCounter++;

		//If we are about to start dragging. 
		if(events[0]=='drag start'){
			elementClicked(subject);
			currentCount=eventCounter;
			//This is also where we initialize the boxes on the left. 
			if( subject.subject.type!='image'){
				currentCenterX= activeElement.freeTransform.handles.center.disc.attrs.cx;
		    	currentCenterY= activeElement.freeTransform.handles.center.disc.attrs.cy;
		    	currentWidth= activeElement.getBBox()['width'];
		    	currentHeight= activeElement.getBBox()['height'];
		    	currentRotation= activeElement.freeTransform.attrs.rotate;
			}else{
				currentCenterX=subject.handles.center.disc.attrs.cx;
				currentCenterY=subject.handles.center.disc.attrs.cy;
				currentRotation=0;
				currentHeight= subject.subject.attrs.height;
				currentHeight= subject.subject.attrs.width;
			}
			lavuLog( subject);
			
			widthChanged(currentWidth);
			heightChanged(currentHeight);
			rotationChanged(currentRotation);	
		
			xLocChanged(currentCenterX);
			yLocChanged(currentCenterY);
			
		// if we are already dragging	
		}else if(events[0]=='drag'){

		    // set the variables we are going to apply 
		    if( subject.subject.type!='image'){
				currentCenterX= activeElement.freeTransform.handles.center.disc.attrs.cx;
		    	currentCenterY= activeElement.freeTransform.handles.center.disc.attrs.cy;
		    	currentX=activeElement.getBBox()['x'];
		    	currentY=activeElement.getBBox()['y'];
			}else{
				currentCenterX=subject.handles.center.disc.attrs.cx;
				currentCenterY=subject.handles.center.disc.attrs.cy;
				currentX= subject.subject.attrs.x;
				currentY= subject.subject.attrs.y;
			}
		   
		    //update the boxes on the left for current x and y locations
		    xLocChanged(currentCenterX);
			yLocChanged(currentCenterY);
			
			elementDragging(subject);
				
		// if we just finished dragging 	
		}else if(events[0]=='drag end'){
			// here i am making sure i am gettiing the last placed values 
			if( subject.subject.type!='image'){
				currentCenterX= activeElement.freeTransform.handles.center.disc.attrs.cx;
		    	currentCenterY= activeElement.freeTransform.handles.center.disc.attrs.cy;
		    	currentX=activeElement.getBBox()['x'];
		    	currentY=activeElement.getBBox()['y'];
			}else{
				currentCenterX=subject.handles.center.disc.attrs.cx;
				currentCenterY=subject.handles.center.disc.attrs.cy;
				currentX= subject.subject.attrs.x;
				currentY= subject.subject.attrs.y;
			}

		    xLocChanged(currentCenterX);
			yLocChanged(currentCenterY);
		
		// if we are rotating or changing width or height
		}else if( events[0] == 'rotate'){
			lavuLog( subject);
			currentWidth= currentCenterX=subject.handles.center.disc.attrs.cx;
			currentCenterY=subject.handles.center.disc.attrs.cy;;
			currentHeight= activeElement.getBBox()['height'];

			currentRotation= activeElement.freeTransform.attrs.rotate;
			
			widthChanged(currentWidth);
			heightChanged(currentHeight);
			rotationChanged(currentRotation);	
		}else if( events[0]=='scale'){
			currentWidth= activeElement.getBBox()['width'];
			currentHeight= activeElement.getBBox()['height'];
			widthChanged(currentWidth);
			heightChanged(currentHeight);
		//if we are just creating this object we need to initialize it.
		}else if (events[0]== 'init'){
			//todo set the initial variables 
			lavuLog("Initializing");
			newInfo= getInitialValues(subject.subject.node.id);
			currentWidth=newInfo[0];
			currentHeight=newInfo[1]; 
			currentX=newInfo[2];
			currentY=newInfo[3];
			currentRotation=newInfo[4]; 
			currentCenterX=newInfo[5]; 
			currentCenterY=newInfo[6];
			init=true;
		//if we are applying changes to this object
		}else if (events[0]=='apply'){
			if(init){
				if( subject.subject.type=='image'){
					currentCenterX=subject.handles.center.disc.attrs.cx;
					currentCenterY=subject.handles.center.disc.attrs.cy;
					
				}
				else{
					if(currentWidth)
						subject.subject.getBBox()['width']=currentWidth;
					if(currentHeight)
						subject.subject.getBBox()['height']=currentHeight;
					if(currentX)
						subject.subject.getBBox()['x']=currentX;
					if(currentY)	
						subject.subject.getBBox()['y']=currentY;
					if( subject.subject.type!='image' && elementsCenter[subject.subject.node.id].cx )
						subject.handles.center.disc.attrs.cx=elementsCenter[subject.subject.node.id].cx;
					if(subject.subject.type!='image' && elementsCenter[subject.subject.node.id].cy )
						subject.handles.center.disc.attrs.cy=elementsCenter[subject.subject.node.id].cy;
				}
				if(currentRotation){
					lavuLog( "currentRotation: "+currentRotation);
					subject.subject.freeTransform.attrs.rotate=currentRotation;
				}
				xLocChanged(currentCenterX);
				yLocChanged(currentCenterY);	
				widthChanged(currentWidth);
				heightChanged(currentHeight);
				rotationChanged(currentRotation);
	
				
				lavuLog("applying: ");
				lavuLog( "width: "+subject.subject.getBBox()['width']); 	
				lavuLog( "height: "+subject.subject.getBBox()['height']);
				lavuLog( "X: "+subject.subject.getBBox()['x']);
				lavuLog( "Y: "+subject.subject.getBBox()['y']);
				lavuLog( "centerX: "+subject.handles.center.disc.attrs.cx);
				lavuLog( "centerY: "+subject.handles.center.disc.attrs.cy);
				lavuLog( "rotation: "+subject.subject.freeTransform.attrs.rotate);
				
				//elementDragging(subject);
				
				lavuLog(subject);
				subject.attrs.center.x= subject.handles.center.disc.attrs.cx;
				subject.attrs.center.y= subject.handles.center.disc.attrs.cy;
				//lavuLog(subject.attr.x);
				//once we are done applying the settings we need to reset everything;
				currentCenterX=0; 
				currentCenterY=0; 
				currentWidth=0;
				currentHeight=0;
				currentRotation=0;
				currentX=0;
				currentY=0;
				init=false;
				
				subject.apply();
				if(subject.subject.type=="rect"){		
					cy=subject.attrs.center.y-(subject.subject.attrs.height/2);
					cx=subject.attrs.center.x-(subject.subject.attrs.width/2);

					subject.subject.attr({x:cx});
					subject.subject.attr({y:cy});
				}	
				subject.apply();
			}
			
		}
		
		//This next section is for if you want to delete the 
		//element by holding down the click on the object
		
		currentSubject = subject;
		
		var start = new Date().getTime(); 
		var i = 0, limit = 12, busy = false; 
		var processor = setInterval(function() { 
		
			 if(!busy) { 
			      busy = true; 
			      
			      $('#deleteTable').val((new Date().getTime() - start));
			      
			      if(currentCount!=eventCounter){
			      	clearInterval(processor);
			      }
			      	
			      if(++i == limit){ 
			      	clearInterval(processor);
			      	
			      	if(currentCount==eventCounter){
			      		$('#deleteTable').click();
			      	}
			      }
			       
			      busy = false; 
		    } 
		
		}, 100);
	
	}
}
//for when we are first creating the opbject
function getInitialValues(id){
	//lavuLog(elementsArray[id]);

	var infoArray= new Array();
	infoArray.push(elementsArray[id].attrs.scale.x*elementsArray[id].attrs.size.x);
	infoArray.push(elementsArray[id].attrs.scale.y*elementsArray[id].attrs.size.y);
	infoArray.push(elementsArray[id].subject.attrs.x);
	infoArray.push(elementsArray[id].subject.attrs.y);
	infoArray.push(elementsArray[id].subject.freeTransform.attrs.rotate);
	infoArray.push(elementsArray[id].handles.center.disc.attrs.cx);
	infoArray.push(elementsArray[id].handles.center.disc.attrs.cy);
	//lavuLog( infoArray);
	return infoArray;
}

//This function only takes care of the label positional stuff not rotational stuff. 
function elementDragging(subject){
	//lavuLog( "In elementDragging");
	if( subject.subject.type=="rect"){
		label = labelsArray[subject.subject.node.id];
		newY = subject.handles.center.disc.attrs.cy-20;
		newX = subject.handles.center.disc.attrs.cx;
		subject.subject.attr({cx: subject.handles.center.disc.attrs.cx, cy: subject.handles.center.disc.attrs.cy });
		label.attr({x: newX});
		label.attr({y: newY});
	}
	else{
	label = labelsArray[subject.subject.node.id];
		newY = subject.handles.center.disc.attrs.cy-20;
		newX = subject.handles.center.disc.attrs.cx;
		label.attr({x: newX});
		label.attr({y: newY});

	}

}

function nameChanged(){
	saveCurrentSelectionProperties();
}


/***
 This function gets called when the user changes the value of the x text box
 This gets called onkeyUp from layout.php
***/
function changeXLoc(xInput){
	
	label = labelsArray[activeElement.node.id];
	
	label.attr( {x: $(xInput).val()});
	lavuLog(elementsProperties[activeElement.node.id]['x']);
	
	elementsProperties[activeElement.node.id]['x']=$(xInput).val();
}

/***
 This function gets called when the user changes the value of the y text box
 This gets called onkeyUp from layout.php
***/
function changeYLoc(yInput){
	yLocChanged($(yInput).val());
}

/***
 This function gets called when the user changes the value of the rotation in text box
 This gets called onkeyUp from layout.php
***/
function changeRotation(x){
	rotationChanged(x.val());
}

/***
 This function gets called when the user changes the value of the rotation in text box
 This gets called onkeyUp from layout.php
***/
function changeWidth(x){
	widthChanged(x.val());
}

/***
 This function gets called when the user changes the value of the rotation in text box
 This gets called onkeyUp from layout.php
***/
function changeHeight(x){
	heightChanged(x.val());
}
/***
This function updates the value of the textbox on the left with the current x value of the box that is selected
*/
function xLocChanged(newX){
	$("#inputX").val(newX);
}

/***
This function updates the value of the textbox on the left with the current y value of the box that is selected
*/
function yLocChanged(newY){
	$("#inputY").val(newY);
}
/***
This function updates the value of the textbox on the left with the current width value of the box that is selected
*/
function widthChanged(newWidth){
	//lavuLog(newWidth);
	$("#inputWidth").val(newWidth);
}
/***
This function updates the value of the textbox on the left with the current height value of the box that is selected
*/
function heightChanged(newHeight){
	$("#inputHeight").val(newHeight);
}
/***
This function updates the value of the textbox on the left with the current height value of the box that is selected
*/
function rotationChanged(newRot){
	$("#inputRotation").val(newRot);
}
/***
This function updates the object's revenue center flag
*/
function applyRevenueCenter() {
	if (activeElement && activeElement.attrs)
		activeElement.attrs['revenueCenter'] = parseInt($("#selectRevenueCenter").val());
	colorCircles();
}

function newRoom(){
	$.post("index.php?widget=table_setup_files/php/newRoom", 
				{posted: "TRUE",
				locationID: globalLocationID,
				roomTitle: 'New Room'},
				function(data){
					location.reload();
					/*init_globals();
					selectedRoom = parseInt(data);
					isNewRoom = true;
					$('#loading').css('display','block');
					setTimeout(function(){
						$('#loading').css('display','none');
					});
					
					$.ajax(
					{
						type: "POST",
						url: "index.php?widget=layout",
						cache: false,
						data: {reload_table_layout: "TRUE"},
						success: function(data){
							$("#table_layout_container_master").html(data);
						}
					});*/
				});
}

function deleteRoom(){
	deleteIt = confirm("Do you really want to delete this room?");
	
	if(deleteIt){
		selectedRoom = $('#selectedRoom').val();
	
		$.post("index.php?widget=table_setup_files/php/deleteRoom", 
					{posted: "TRUE",
					roomID: selectedRoom},
					function(data){
						//alert(data);
						if(data!='OK'){
							alert('Room was not deleted.');
						}else{
							location.reload();
						}
					});
	}
}

function roomChanged(){

	value = $('#selectedRoom').val();
	
	//console.log(value);

	wantToContinue = true;
	if(changed!=0){
	
		wantToContinue = confirm("There are some unsaved changes. Do you want to continue without saving?");
		
	}

	if(wantToContinue){
		colorCircles_startval = elementCounter;
		initializeRaphael();
	}else{
		$('#selectedRoom').val('"'+value+'"');
	}
}

function disableLayoutButtons(){
	$('#selectedRoom').attr("disabled", true); 
	
	$('#newRoom').attr("disabled", true); 
	$('#deleteRoom').attr("disabled", true); 
	$('#saveRoom').attr("disabled", true); 
	
	$('#elementName').attr("disabled", true); 
	$('#roomTitle').attr("disabled", true); 
}

function enableLayoutButtons(){
	$('#selectedRoom').removeAttr("disabled"); 

	$('#newRoom').removeAttr("disabled"); 
	$('#deleteRoom').removeAttr("disabled"); 
	$('#saveRoom').removeAttr("disabled"); 
	
	$('#elementName').removeAttr("disabled"); 
	$('#roomTitle').removeAttr("disabled"); 
}

function dragToRotate(){
	if($('#dragToRotate').attr('checked')!=undefined){
		touchOptions = {
			keepRatio: true,
			dragRotate: true,
			dragScale: 	true,
			distance:	1.5
		};
	}else{
		touchOptions = {
			keepRatio:true,
			dragRotate: false,
			dragScale: 	false,
			distance:	1.5
		};
	}

	for(elementID in elementsArray){
		elementsArray[elementID].setOpts(touchOptions);
		elementsArray[elementID].updateHandles();
	}
}

function KeyCheck(event){
	var KeyID = event.keyCode;
	
	if( event.target.type !="undefined"){
		//console.log( event.target);
		switch(KeyID){
		case 8:
		case 46:
    		if( event.target.type != "text" &&
   				event.target.type != "textarea" &&
    			event.target.type != "password"){
				deleteElement();
    		}else{
    			return true;
    		}
    		
    	    event.stopPropagation();
    	    
			return false;
		default:
			break;
		}
	}
	//console.log(KeyID);
}

function initializeRaphael(){
	apply=true;
	init=true;
	if($('#selectedRoom').val()==undefined){
		$('#newRoom').click();
	}


	xOffset = $('#tableToolBoxTD').css('width');
	xOffset = parseInt(xOffset.substring(0, xOffset.length-2));
	
	trashHeight = $('#deleteTable').css('height');
	trashHeight = trashHeight.substring(0,trashHeight.length-2);
	
	trashWidth = $('#deleteTable').css('width');
	trashWidth = trashWidth.substring(0,trashWidth.length-2);
	
	trashX = $('#deleteTable').position().top;
	trashY = $('#deleteTable').position().left;

	//initialize table buttons
	$("#circleTableIcon").draggable({
		helper: "clone",
		opacity: 0.7,
		start: function(event, ui){
			circleStart = $(this).position();
		},
		stop: function(event, ui){
			changed++;
			
			circleDropped = ui.helper.position();
			var revenueCenter = $("#selectRevenueCenter").val();
			var tdOffset = $('#canvas_containerTD').position();
			createOldBox('circle',circleDropped.left-tdOffset.left,
			circleDropped.top-tdOffset.top+(tableStandardHeight/2), tableStandardWidth, tableStandardHeight, 0, 'table'+(++elementCounter), revenueCenter, 0, 0);
			//Uncomment the following line when we are ready to move to vectors 
			//createEllipse('ellipse',circleDropped.left-circleStart.left-(tableStandardWidth/2)+xOffset, 
			//circleDropped.top-circleStart.top+(tableStandardHeight/2), tableStandardWidth, tableStandardHeight, 0, 'table'+elementCounter,0,0);
		}
	});
	
	$("#squareTableIcon").draggable({
		helper: "clone",
		opacity: 0.7,
		start: function(event, ui){
			boxStart = $(this).position();
		},
		stop: function(event, ui){
			changed++;
			
			boxDropped = ui.helper.position();
			var revenueCenter = $("#selectRevenueCenter").val();
			var tdOffset = $('#canvas_containerTD').position();
			createOldBox('square',boxDropped.left-tdOffset.left,
			boxDropped.top-tdOffset.top+(tableStandardHeight*1.3), tableStandardWidth, tableStandardHeight, 0, 'table'+(++elementCounter), revenueCenter, 0, 0);
			//boxDropped.top-boxStart.top+(tableStandardHeight*2)-yOffset, tableStandardWidth, tableStandardHeight, 0, 'table'+(++elementCounter), revenueCenter, 0, 0);
			//Uncomment the following line when we are ready to move to vectors 
			//createBox('box',boxDropped.left-boxStart.left-(tableStandardWidth*2)+xOffset*1.25, 
			//boxDropped.top-boxStart.top+(tableStandardHeight*2)-yOffset, tableStandardWidth*2, tableStandardHeight*2, 0, 'table'+(elementCounter));
		}
	});
	
	$("#diamondTableIcon").draggable({
		helper: "clone",
		opacity: 0.7,
		start: function(event, ui){
			boxStart = $(this).position();
		},
		stop: function(event, ui){
			changed++;
			
			boxDropped = ui.helper.position();
			var revenueCenter = $("#selectRevenueCenter").val();
			var tdOffset = $('#canvas_containerTD').position();
			createOldBox('diamond',boxDropped.left-tdOffset.left,
			boxDropped.top-tdOffset.top+(tableStandardHeight*2.3), tableStandardWidth, tableStandardHeight, 0, 'table'+(++elementCounter), revenueCenter, 0, 0);
			//Uncomment the following line when we are ready to move to vectors 
			//createBox('box',boxDropped.left-boxStart.left-(tableStandardWidth*2)+xOffset*1.25, 
			//boxDropped.top-boxStart.top+(tableStandardHeight*2)-yOffset, tableStandardWidth*2, tableStandardHeight*2, 0, 'table'+(elementCounter));
		}

	});
	$("#slantLeftTableIcon").draggable({
		helper: "clone",
		opacity: 0.7,
		start: function(event, ui){
			boxStart = $(this).position();
		},
		stop: function(event, ui){
			changed++;
			
			boxDropped = ui.helper.position();
			var revenueCenter = $("#selectRevenueCenter").val();
			var tdOffset = $('#canvas_containerTD').position();
			createOldBox('slant_left',boxDropped.left-tdOffset.left,
			boxDropped.top-tdOffset.top+(tableStandardHeight*4.3), tableStandardWidth, tableStandardHeight, 0, 'table'+(++elementCounter), revenueCenter, 0, 0);
			//Uncomment the following line when we are ready to move to vectors 
			//createBox('box',boxDropped.left-boxStart.left-(tableStandardWidth*2)+xOffset*1.25, 
			//boxDropped.top-boxStart.top+(tableStandardHeight*2)-yOffset, tableStandardWidth*2, tableStandardHeight*2, 0, 'table'+(elementCounter));
		}

	});
		$("#slantRightTableIcon").draggable({
		helper: "clone",
		opacity: 0.7,
		start: function(event, ui){
			boxStart = $(this).position();
		},
		stop: function(event, ui){
			changed++;
			
			boxDropped = ui.helper.position();
			var revenueCenter = $("#selectRevenueCenter").val();
			var tdOffset = $('#canvas_containerTD').position();
			createOldBox('slant_right',boxDropped.left-tdOffset.left,
			boxDropped.top-tdOffset.top+(tableStandardHeight*3.3), tableStandardWidth, tableStandardHeight, 0, 'table'+(++elementCounter), revenueCenter, 0, 0);
			//Uncomment the following line when we are ready to move to vectors 
			//createBox('box',boxDropped.left-boxStart.left-(tableStandardWidth*2)+xOffset*1.25, 
			//boxDropped.top-boxStart.top+(tableStandardHeight*2)-yOffset, tableStandardWidth*2, tableStandardHeight*2, 0, 'table'+(elementCounter));
		}

	});
	$.ajaxSetup({async:false});
	
	document.onkeydown = KeyCheck;
	
	if(isNewRoom){
		$('#selectedRoom').val(selectedRoom);
		isNewRoom = false;
	}	
	
	selectedRoom = $('#selectedRoom').val();
	
	if(navigator.userAgent.match(/iPad/i) != null){
		touchOptions = {
			gridSnap:	7,
			size: 10, 
			distance: 1.5
		}
		
		document.getElementById('canvas_container').ontouchmove = function(event) {
			event.preventDefault();
		};
		
		document.getElementById('layoutTable').ontouchmove = function(event) {
			event.preventDefault();
		};
		
	}else{
		touchOptions = {
			gridSnap:	7,
			size: 5, 
			distance: 1.2
		}
	}
	
	$.post("index.php?widget=table_setup_files/php/getRoomData", 
				{posted: "TRUE",
				locationID: globalLocationID,
				roomID: selectedRoom},
				function(data){
				
					//console.log(data);
					
				
					$('#loadingTables').css('display', 'block');
					disableLayoutButtons();
				
					elementsArray = {};
					elementsProperties = {};
					labelsArray = {};
					
					activeElement = undefined;
					
					
					if(paper){
						paper.clear();
						paper.remove();
					}
				
					maxWidth = $('#centerCanvasSpace').css('width');
					
					tablesDescriptionArray =  eval('('+ data +')');
					//lavuLog("data coming In: "+data);
					size = tablesDescriptionArray['roomSize'];
					
					//console.log(size);
						
					//paper = new Raphael('canvas_container', size[0], size[1]); 
					paperWidth = 969;
					paperHeight = 1000;
					
					paper = new Raphael('canvas_container', paperWidth, paperHeight); 
					
					if(size[0]<maxWidth){
						$('#properties').css('width',size[0]);
					}else{
						$('#properties').css('width',maxWidth);
					}
					
					$('#roomWidth').val(size[0]);
					$('#roomHeight').val(size[1]);
					$('#roomTitle').val(tablesDescriptionArray['roomTitle']);
					
					
					for(i=0;i<tablesDescriptionArray['coord_x'].length;i++){
						lavuLog("Loading…");
						
						x = tablesDescriptionArray['coord_x'][i];
						y = tablesDescriptionArray['coord_y'][i];
						centerX = tablesDescriptionArray['centerX'][i];
						centerY = tablesDescriptionArray['centerY'][i];
						lavuLog("x and y: "+x+" y: "+y);
						width = tablesDescriptionArray['widths'][i];
						height = tablesDescriptionArray['heights'][i];
						lavuLog("width and height: "+ width+", "+height );
						
						shape = tablesDescriptionArray['shapes'][i];
						lavuLog("shape: "+ shape);
						
						name = tablesDescriptionArray['names'][i];
						lavuLog("name: "+name);
						
						revenueCenter = tablesDescriptionArray['revenueCenters'][i];
						lavuLog("revenueCenter: "+revenueCenter);
						
						rotation = tablesDescriptionArray['rotate'][i];
						
						lavuLog("Rotation"+ rotation);

						if(y<0){
							y = 0;
							
							tablesOutOfRange++;
						}
						if(x<0){
							tablesOutOfRange++;
							x=0;
						}
						
						if(x>paperWidth){
							lavuLog(" paperWidth is: "+paperWidth);
							if(shape=="square"){
								x = paperWidth-width;
							}else{
								x = paperWidth-width/2;
							}
							
							tablesOutOfRange++;
							//x = paperWidth-width;
						}
						
						tempArray = [];
						
						/*
						if(shape=='square'){
							tempArray = ['square', x,y, width, height, rotation, name, centerX, centerY];
						}else if( shape=='diamond' ){
							tempArray = ['diamond', x,y, width, height, 45, name, centerX, centerY];
						}else if(shape=='slant_right'){
							tempArray = ['slant_right', x,y, width*1.5, height, -45, name, centerX, centerY];
						}else if(shape=='slant_left'){
							tempArray = ['slant_left', x,y, width*1.5, height, 45, name, centerX, centerY];
						}else if (shape=='box'){
							tempArray = ['box', x,y, width, height, rotation, name, centerX, centerY];
						}else if (shape=='circle'){
							tempArray = ['circle', x+(width/2),y+(height/2), width/2, height/2, rotation, name, centerX, centerY];
						}
						else{
							tempArray = ['ellipse', x+(width/2),y+(height/2), width/2, height/2, rotation, name, centerX, centerY];
						}*/
						
						
						if(shape=='square'){
							tempArray = ['square', x,y, width, height, rotation, name, revenueCenter, centerX, centerY];
						}else if( shape=='diamond' ){
							tempArray = ['diamond', x,y, width, height, rotation, name, revenueCenter, centerX, centerY];
						}else if(shape=='slant_right'){
							tempArray = ['slant_right', x,y, width, height, rotation, name, revenueCenter, centerX, centerY];
						}else if(shape=='slant_left'){
							tempArray = ['slant_left', x,y, width, height, rotation, name, revenueCenter, centerX, centerY];
						}else if (shape=='box'){
							tempArray = ['box', x,y, width, height, rotation, name, revenueCenter, centerX, centerY];
						}else if (shape=='circle'){
							tempArray = ['circle', x,y, width, height, rotation, name, revenueCenter, centerX, centerY];
						}
						else{
							tempArray = ['ellipse', x+(width/2),y+(height/2), width/2, height/2, rotation, name, revenueCenter, centerX, centerY];
						}
						
						var ta = tempArray;
						tempArray = {'type':ta[0], 'x':ta[1], 'y':ta[2], 'width':ta[3], 'height':ta[4], 'rotation':ta[5], 'name':ta[6], 'revenueCenter':ta[7], 'centerX':ta[8], 'centerY':ta[9]}

						tableProperties.push(tempArray);
						
					}
					//lavuLog("The table Properties are: "+ tableProperties);
					printTables(0);
				
					setTimeout('finishedLoading();', 200);
				});		
	$.ajaxSetup({async:true});	
	
	changed=0;
}

function printTables(time){
	if(tableProperties.length > 0){
		table = tableProperties.shift();
		type= table['type'];
		
		//if( type == 'diamond' && (typeof rotation != 'undefined' && table[5] < 360) ) {
			//			type     x,			y, 	   width,  height,  rotation, boxName    centerx   centery 	
			//createBox(table[0],table[1], table[2], table[3],table[4], table[5], table[6], table[7], table[8], table[9]);
		//else if( type == 'circle' && (typeof rotation != 'undefined' &&  table[5] < 360) )
			//createEllipse(table[0],table[1], table[2], table[3],table[4], table[5], table[6], table[7], table[8], table[9]);
		//} else {
			createOld(table);
			$("#selectRevenueCenter").val(table['revenueCenter']);
		//}

		setTimeout(function(){
			printTables(time);
		}, time);
	}else{
		$('#loadingTables').css('display', 'none');
		enableLayoutButtons();
		
		if(tablesOutOfRange>0){
			alert("There were some tables that were out of the room range, they need to be moved so they can be displayed. Please notice that you room might look a little different.");
			tablesOutOfRange=0;
		}
	}
}

function saveRoom(){
	//console.log('Number of elements: '+Object.keys(elementsArray).length);
	
	$('#saving').css('display','block');
	
	//coordX and coordY should be the top left corner of the shape
	
	coordX = new Array();
	coordY = new Array();
	//centerX and centerY are the centers of the shapes. 
	centerX= new Array();
	centerY= new Array();
	
	//The shapes to be saved
	shapes = new Array();
	
	widths = new Array();
	heights = new Array();
	names = new Array();
	rotate = new Array();
	revenueCenters = new Array();
	
	for(elementID in elementsArray){
		//Pieces of data to get:  
		//	Top left X
		//	Top left Y 
		// 	width 
		//	height
		//	rotation
		// 	name
		// 	shape
		// revenueCenter
		
		//This is center X and center Y 
		//cx = elementsArray[elementID].handles.center.disc.attrs.cx;
		//cy = elementsArray[elementID].handles.center.disc.attrs.cy;
		lavuLog("Saving….");
		//lavuLog ("object");
		//lavuLog( elementsArray[elementID]);
		//lavuLog( " bbox:");
		//lavuLog( elementsArray[elementID].subject.getBBox());
		//Gettings the center
		cx = elementsArray[elementID].handles.center.disc.attrs.cx;
		cy = elementsArray[elementID].handles.center.disc.attrs.cy;
		lavuLog("Center: "+cx+", "+cy);
		
		centerX.push(cx);
		centerY.push(cy);
		
		
		//Getting the x and y (top left) 
		
		x=elementsArray[elementID].subject.getBBox()['x'];
		y= elementsArray[elementID].subject.getBBox()['y'];
		lavuLog("X and Y: "+x+", "+y);
		
		coordX.push(x);
		coordY.push(y);
		
		//Getting the width and height 
		//using this way of getting the x and y we get a situation where the center moves when we scale the 
		//objects. 
		//width= elementsArray[elementID].attrs.size.x* elementsArray[elementID].attrs.scale.x;
		//height= elementsArray[elementID].attrs.size.y* elementsArray[elementID].attrs.scale.y;
		
		//Using this method, we get a situation where the center stays the same but we are getting incorrect widths and heights. 
		//width=elementsArray[elementID].subject.getBBox()['width'];
		//height=elementsArray[elementID].subject.getBBox()['height'];
		
		//width=80+Math.abs(activeElement.freeTransform.handles.x.line.attrs.path[1][2]-activeElement.freeTransform.handles.x.line.attrs.path[0][2])*2;
		//height=80+Math.abs(activeElement.freeTransform.handles.y.line.attrs.path[1][1]-activeElement.freeTransform.handles.y.line.attrs.path[0][1])*2;

		width=elementsArray[elementID].attrs.scale.x*elementsArray[elementID].attrs.size.x;
		height=elementsArray[elementID].attrs.scale.y*elementsArray[elementID].attrs.size.y;

		
		lavuLog("Width and Height: "+ width+ ", "+ height);
		
		widths.push(width);
		heights.push(height);
		
		revenueCenter = getRevenueCenter(elementsArray[elementID], true);
		if (revenueCenter == -2) { revenueCenter = -1; }
		lavuLog("Revenue center: " + revenueCenter);
		revenueCenters.push(revenueCenter)
		
		if( elementsArray[elementID].subject.type=='image')
			rotation=360;
		else
			rotation= elementsArray[elementID].subject.freeTransform.attrs.rotate;
		//rotation=-rotation;
		//lavuLog("rotation: "+ rotation);
		rotate.push(rotation);
		
		type= elementsArray[elementID].subject.type;
		lavuLog( "type: "+type);

		
		names.push(elementsProperties[elementID]['name']);
		if(elementsProperties[elementID].type =='ellipse')
			shapes.push('circle');
		else if( elementsProperties[elementID].type =='box')
			shapes.push('diamond');
		else
			shapes.push(elementsProperties[elementID].type);
		
		
	}
	
	roomTitle = $('#roomTitle').val();
	roomWidth = $('#roomWidth').val();
	roomHeight = $('#roomHeight').val();
	roomID = $('#selectedRoom').val();
	
	
	$('#selectedRoom option[value="'+roomID+'"]').text(roomTitle);
	
	$.post("index.php?widget=table_setup_files/php/saveRoomData", 
				{posted: "TRUE",
				coordX: coordX,
				coordY: coordY,
				centerX: centerX,
				centerY: centerY,
				widths: widths,
				heights: heights,
				rotate: rotate,
				names: names,
				shapes: shapes,
				roomTitle: roomTitle,
				roomWidth: roomHeight,
				roomHeight: roomHeight,
				roomID: roomID,
				locationID: globalLocationID,
				revenueCenter: revenueCenters},
				function(data){
					
					$('#saving').css('display','none');
					
					if(data.indexOf('UNKNOWN COLUMNS') !== -1) {
						alert("Same data couldn't be saved. (" + data + ")");
					} else if(data.indexOf('OK') !== -1){
						changed = 0;
					} else {
						if (data.indexOf("FAIL") !== -1)
							alert("Table setup saving failed");
						else
							alert("Connection error, saving failed");
					}
				});
	
	
	setTimeout(function(){
		$('#saving').css('display','none');
	}, 10000);
	apply=true;
	init=true;
}

function trashHover(){
	$('#deleteTable').attr('src', 'images/deleteTableIconHover.png');
}

function trashNormal(){
	$('#deleteTable').attr('src', 'images/deleteTableIcon.png');
}

function finishedLoading() {
	if ($("#loadingTables").css('display') == "none") {
		updateCircles();
	} else {
		setTimeout('finishedLoading();', 200);
	}
}

// resize the handle circles and apply new properties
function updateCircles() {
	var ipad = false;
	if (navigator.platform && navigator.platform.indexOf("iPad") !== -1)
		ipad = true;
	var circles = $("circle");
	for (var i = 0; i < circles.length; i++) {
		if (ipad)
			circles[i].r.baseVal.value = 8;
		switch(i % 3) {
			case 0: // x-axis
				$(circles[i]).css('cursor', 'ew-resize');
				break;
			case 1: // y-axis
				$(circles[i]).css('cursor', 'ns-resize');
				break;
			case 2: // center
				$(circles[i]).css('cursor', 'move');
				break;
		}
	}
	colorCircles();
}

// applies color to the circles depending upon the revenue_center_id
colorCircles_startval = 0;
function colorCircles() {
	var ipad = false;
	var circles = $("circle");
	var accountForDeleted = 0;
	var maxAccountForDeleted = 100;
	for (var i = 0; i < circles.length; i++) {
		var elementID = "element" + (Math.floor(i/3)+(1+colorCircles_startval+accountForDeleted));
		var element = elementsArray[elementID];
		while (!element && accountForDeleted < maxAccountForDeleted) {
			accountForDeleted++;
			elementID = "element" + (Math.floor(i/3)+(1+colorCircles_startval+accountForDeleted));
			element = elementsArray[elementID];
		}
		if (element) {
			var rcid = getRevenueCenter(element, true);
			if (rcid != -2) {
				var color = revenue_center_color(rcid);
				$(circles[i]).css("fill", color);
			}
		}
	}
}
