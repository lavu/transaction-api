<?php
	//UNCOMENT WHEN DEBUGGING
	/*ini_set("display_errors",1);
	$_POST['posted'] = TRUE;
	$_POST['coordX'] = array(208, 588);
	$_POST['coordY'] = array(244, 244);
	$_POST['widths'] = array(281, 174);
	$_POST['heights'] = array(180, 93);
	$_POST['rotate'] = array(0, 0);
	$_POST['names'] = array('Table 7','Table 8');
	$_POST['shapes'] = array('square','circle');
	$_POST['roomTitle'] = 'My Room';
	$_POST['roomWidth'] = 1900;
	$_POST['roomHeight'] = 820;
	
	$_POST['roomID'] = 9;
	$_POST['locationID'] = 27;*/
	
	function saveRoomData($coordX, $coordY,$centerX, $centerY, $widths, $heights, $rotate, $names, $shapes, $roomTitle, $roomWidth, $roomHeight, $roomID, $locationID, $revenuCenter){
    	$coordXString 	= implode('|', $coordX);
    	$coordYString 	= implode('|', $coordY);
    	$centerXString 	= implode('|', $centerX);
        $centerYString 	= implode('|', $centerY);
    	$widthsString 	= implode('|', $widths);
    	$heightsString 	= implode('|', $heights);
    	$rotateString 	= implode('|', $rotate);
    	$namesString 	= implode('|', $names);
    	$shapesString 	= implode('|', $shapes);
    	$revenueCenterString = implode('|', $revenuCenter);
    	
    	$inputArray = array();
    	$inputArray["coordXString"] = $coordXString;
    	$inputArray["coordYString"] = $coordYString;
    	$inputArray["centerXString"] = $centerXString;
        $inputArray["centerYString"] = $centerYString;
    	$inputArray["widthsString"] = $widthsString;
    	$inputArray["heightsString"] = $heightsString;
    	$inputArray["rotateString"] = $rotateString;
    	$inputArray["namesString"] = $namesString;
    	$inputArray["shapesString"] = $shapesString;
    	$inputArray["roomIDString"] = $roomID;
    	$inputArray["roomTitleString"] = $roomTitle;
    	$inputArray["revenueCenterString"] = $revenueCenterString;
    	
    	$queryStrings = array();
    	$queryStrings[] = " `coord_x`='[coordXString]'";
    	$queryStrings[] = " `coord_y`='[coordYString]'";
    	$queryStrings[] = " `centerX`='[centerXString]'";
    	$queryStrings[] = " `centerY`='[centerYString]'";
    	$queryStrings[] = " `widths`='[widthsString]'";
    	$queryStrings[] = " `heights`='[heightsString]'";
    	$queryStrings[] = " `rotate`='[rotateString]'";
    	$queryStrings[] = " `names`='[namesString]'";
    	$queryStrings[] = " `shapes`='[shapesString]'";
    	$queryStrings[] = " `title`='[roomTitleString]'";
    	$queryStrings[] = " `revenue_centers`='[revenueCenterString]'";
    	
    	$inputColumns = array();
    	foreach ($queryStrings as $qs) {
       		$parts = explode("`", $qs);
	    	$inputColumns[] = $parts[1];
    	}
    	$columnsQuery = lavu_query("SHOW COLUMNS FROM `tables`");
    	$realColumns = array();
    	while ($row = mysqli_fetch_assoc($columnsQuery)) {
	    	$realColumns[] = $row['Field'];
    	}
    	$unknowns = array();
    	foreach($inputColumns as $ic) {
	    	if (array_search($ic, $realColumns) === FALSE) {
		    	$unknowns[] = $ic;
	    	}
    	}
    	
    	$query = "UPDATE `tables` SET";
    	$first = TRUE;
    	foreach ($queryStrings as $qs) {
	    	$columnName = explode("`", $qs);
	    	$columnName = $columnName[1];
	    	if (array_search($columnName, $unknowns) === FALSE) {
	    		if ($first) {
		    		$first = FALSE;
	    		} else {
		    		$query .= ",";
	    		}
		    	$query .= $qs;
	    	}
    	}
    	$query .= " WHERE `id`='[roomIDString]'";
    	
    	//echo $query . "\n";
    	//print_r($inputArray);
    	
    	$result = lavu_query($query, $inputArray);
    	//echo gettype($result);
    	if (count($unknowns) > 0) {
	    	echo "UNKNOWN COLUMNS ".implode(", ",$unknowns);
	    	return "yourmamma";
    	}
    	return $result;
    }
	
	$posted = isset($_POST['posted']);
	
	if($posted){
		
		//get the ids from the post
		$coordX 	= $_POST['coordX'];
		$coordY 	= $_POST['coordY'];
		$centerX	= $_POST['centerX'];
		$centerY	= $_POST['centerY'];
		$widths 	= $_POST['widths'];
		$heights	= $_POST['heights'];
		$rotate 	= $_POST['rotate'];
		$names 		= $_POST['names'];
		$shapes 	= $_POST['shapes'];
		$roomTitle 	= $_POST['roomTitle'];
		$revenueCenter = $_POST['revenueCenter'];
		
		if(isset($_POST['roomWidth'])){
			$roomWidth 	= $_POST['roomWidth'];
		}else{
			$roomWidth = 969;
		}
		
		if(isset($_POST['roomHeight'])){
			$roomHeight = $_POST['roomHeight'];
		}else{
			$roomHeight = 1000;
		}
		
		
		$roomID 	= $_POST['roomID'];
		$locationID = $_POST['locationID'];
        
		
		//execute the query
        $result=saveRoomData($coordX,$coordY,$centerX, $centerY, $widths,$heights,$rotate,$names,$shapes,$roomTitle,$roomWidth,$roomHeight,$roomID,$locationID,$revenueCenter);
		
		//indicate if the query executed succesfully
		if($result){
			if ($result !== "yourmamma")
				$get_location_info = lavu_query("SELECT `use_net_path`, `net_path` FROM `locations` WHERE `id`= '[1]'", $locationID);
				if (mysqli_num_rows($get_location_info) > 0) {
					$location_info = mysqli_fetch_assoc($get_location_info);
					schedule_remote_to_local_sync_if_lls($location_info, $locationID, "table updated", "tables");
				}		
				echo 'OK';
			return;
		}else{
			echo 'FAIL';
		}
	//if no data was sent
	}else{
		echo "NO_DATA";	
	}
?>

