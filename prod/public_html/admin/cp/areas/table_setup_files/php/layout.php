<?php
	ini_set("display_errors","1");
	
	function No_Cache()
	{
	   if(headers_sent()) return FALSE;
	
	   header('pragma: public'); # HTTP/1.0
	   header('expires: Mon, 26 Jul 1997 05:00:00 GMT');
	   @header('last-modified: ' . gmdate('D, d M Y H:i:s') . ' gmt'); # !
	   @header('cache-control: no-store, no-cache, must-revalidate'); # HTTP/1.1
	   header('cache-control: private, post-check=0, pre-check=0, max-age=0', FALSE); # HTTP/1.1
	   header('pragma: no-cache'); # HTTP/1.0
	   header('expires: 0');
	   return;
	}
	No_Cache();
    
	$rooms = getRooms($locationid);
	$iPad = count(explode("iPad", $_SERVER['HTTP_USER_AGENT']));
	if ($iPad == 2)
		$iPad = TRUE;
	else
		$iPad = FALSE;
		
	function echoclick() {
		global $iPad;
		//if ($iPad) { echo "Tap"; } else { echo "Click"; }
	}
	
	function getRooms($locID){
    	$result =  array();
    
    	$query = "select * from tables where loc_id=".$locID." and _deleted!=1 order by title";
    	
    	//echo $query;
    	
    	$lavu_query = lavu_query($query);
    	
    	if ($lavu_query) {
	    	while($result_read = mysqli_fetch_assoc($lavu_query)){
	    		array_push($result, $result_read);
	    	}
	    }
    
    	return $result;
    }
    
    function echoRoomsAsDropDown($rooms, $id){
    	echo '<select id="'.$id.'" onchange="roomChanged()">';
    	$selected_set = FALSE;
    	foreach($rooms as $room){
    		echo '<option value="'.$room['id'].'"';
    		if ($room['title'] == 'New Room' && !$selected_set) {
    			echo " selected";
    			$selected_set = TRUE;
    		}
    		echo '>'.$room['title'].'</option>';
    	}
    	echo '</select>';
    }
    
    function echoRevenueCentersAsDropDown($id) {
    	global $modules;
    
	    $rcs = get_revenue_centers();
	    if (!$rcs) { $rcs = array(); }
	    $blank = array(); $blank['name'] = ' '; $blank['id'] = -1;
	    array_unshift($rcs, $blank);
	    $selected = FALSE;
	    $display = "";
	    if (!$modules->hasModule("dining.revenuecenters"))
	    	$display = "display:none;";
	    //$display = "";
	    echo '<select id="'.$id.'" style="'.$display.'">';
    	foreach($rcs as $rc){
    		echo '<option value="'.$rc['id'].'"';
    		if (!$selected) {
	    		echo " selected";
	    		$selected = TRUE;
    		}
    		echo '>'.$rc['name'].'</option>';
    	}
    	echo '</select>';
    }
    
    // gets a list of unique revenue centers as an array [['name':name1, 'id':id1], ['name':name2, 'id':id2], ...]
    // returns FALSE if there are no revenue_centers
    function get_revenue_centers() {
	    $result = lavu_query("SELECT `name`, `id` FROM `revenue_centers` WHERE `_deleted` = '0' ORDER BY `name`");
	    if ($result) {
		    $oldrows = array();
		    if (mysqli_num_rows($result))
		    	while($oldrow = mysqli_fetch_assoc($result))
		    		$oldrows[] = $oldrow;
		    $newrows = array();
		    foreach ($oldrows as $oldrow)
		    	if (!in_array($oldrow, $newrows))
		    		$newrows[] = $oldrow;
		    return $newrows;
	    }
	    return FALSE;
    }
?>

<script type="text/javascript" src="scripts/jquery.min.1.8.3.js"></script>
<link rel="stylesheet" href="areas/table_setup_files/css/jquery.fancybox.css" type="text/css" media="screen" />
<script type="text/javascript" src="areas/table_setup_files/js/jquery.fancybox.pack.js"></script>
<script langauge="javascript" src="areas/table_setup_files/js/raphael-min.js"></script>
<script langauge="javascript" src="areas/table_setup_files/js/raphael.free_transform.js"></script>
<script langauge="javascript" src="areas/table_setup_files/js/jquery.ui.touch-punch.min.js"></script>
<script langauge="javascript" src="areas/table_setup_files/js/layout.js"></script>
<link rel="stylesheet" type="text/css" href="areas/table_setup_files/css/style.css"></script>

<!-- startup script -->
<script language="javascript">
	globalLocationID = <?php echo $locationid; ?>;
	setTimeout('initializeRaphael();', 500);

	/* @id the revenue center id */
	function revenue_center_color(id) {
		ids_to_colors = {'-1':'rgb(255,255,255)'<?php
			$rcs = get_revenue_centers();
			if ($rcs) {
				foreach($rcs as $rc) {
					$i = intval($rc['id']);
					$r = ($i*73 + 0) % 256;
					$g = ($i*100 + 0) % 256;
					$b = ($i*53 + 200) % 256;
					echo ", '".$rc['id']."':'rgb($r,$g,$b)'";
				}
			}
		?>};
		return ids_to_colors[id];
	}
	
	setTimeout(function() {
		//if ($.browser.msie)
		//	$("#visittablelayouteditor1button").click();
		$("#selectRevenueCenter").change(function() {
			applyRevenueCenter();
		});
	}, 500);
	$(document).keypress(function(e) {
    if(e.which == 13 && $("#elementName").css("display")=='block') {
     	$("#elementName").css("display","none");
    }
});
	
</script>

<div id="loadingTables" style="display:block">
	<div id="loadingTablesText">
		Loading
	</div>
	<img src="images/table_layout_loading.gif" id="loadingTablesBar"> 
</div>

<div id="saving">
	<div id="savingText">
		Saving
	</div>
	<img id="savingAllBar" src="images/table_layout_loading.gif"/> 
</div>

<div style="width:1000px;">
	
	<div style="width:930px;text-align:right;margin-top:-35px">
		<form method='get' id='viewOldForm' action=''>
			<?php
				foreach ($_GET as $key => $value) {
					if ($key != "old" && $key != "new")
						echo "<input type='hidden' name='".$key."' value='".$value."'>";
				}
			?>
			<input type='hidden' name='old' value='TRUE'>
			<input type='button' value='Visit Table Layout Editor V1' id='visittablelayouteditor1button' onclick='$("#viewOldForm").submit()'>
		</form>
	</div>
	<input style='position:absolute; display:none; 'id='elementName' size='10' type='text' val='' onkeyup='nameChanged()'/>
	<table style="padding:0px;margin-top: -3px;margin-left: -38px;text-align:left;background-color:#E2DEDC;border:1px solid #aaa;height:1000px; ">
		<tr>
			<td>
				<div id="roomProperties">
					<table id="roomPropertiesTable">
						<tr>
							
							<td class="tableProperties">
								<?= help_mark_tostring(656); ?>
							</td>
							<td id="elementNameLabel" class="tableProperties">
								Table Name:
							</td>
							<td id="elementNameTD" class="tableProperties">
								
							</td>
						
					
							
							<td class="roomProperties">
								<button id="saveRoom" class="layoutButton" type="button" onclick="saveRoom()"> Save Room </button>
							</td>
							<td class="roomProperties">
								<button id="deleteRoom" class="layoutButton" type="button" onclick="deleteRoom()"> Delete Room </button>
							</td>
							<td class="roomProperties">
								<button id="newRoom" class="layoutButton" type="button" onclick="newRoom()"> New Room </button>
							</td>
							<!--<td>
								<input id="roomID" type="text" value=""/>
							</td>-->
							
							
							<td id="roomLabel" class="roomProperties">
								Room:
							</td>
							<td class="roomProperties">
								<?php echoRoomsAsDropDown($rooms, "selectedRoom"); ?>
							</td>
							<td id="roomTitleLabel" class="roomProperties">
								Room Title: 
							</td>
							<td class="roomProperties">
								<input id="roomTitle" type="text"/>
							</td>
							
							<td id="revenueCenterLabel" class="roomProperties">
								<?php if ($modules->hasModule("dining.revenuecenters")) { echo "Revenue Center:"; } ?>&nbsp;
							</td>
							<td class="roomProperties">
								<?php echoRevenueCentersAsDropDown("selectRevenueCenter"); ?>
								<!--<select id="selectRevenueCenter" onchange="applyRevenueCenter();"><option value="-1"> </option><option value="1">Bar</option><option value="4">Restaurant</option></select>-->
							</td>
							<td> 
								<input type='button' onclick = 'cloneTable()' value="Clone Table" />
							</td>
						</tr>
					</table>
				</div>
			</td>
		</tr>
		
		<tr>
			<td>
				<table id="layoutTable">
					<tr>
						<td id="tableToolBoxTD"  class="tableProperties" style="width:50px">
							<table>
								<!-- This is for a later release. uncomment this line to enable vector tables. 
								<tr>
									<td>
										<img id="circleTableIcon" class="tableIcon" src="images/circleTableIcon.png" onclick="createNewCircle()"/>
									</td>
								</tr>
								<tr>
									<td>
										<img id="squareTableIcon" class="tableIcon" src="images/squareTableIcon.png" onclick="createNewBox()"/>
									</td>
								</tr>
								-->
								<tr>
									<td>
										<img id="circleTableIcon" class="tableIcon" src="images/table_circle.png" width='50' height='50' onclick="createNewCircle()"/>
									</td>
								</tr>
								<tr>
									<td>
										<img id="squareTableIcon" class="tableIcon" src="images/table_square.png" width='50' height='50' onclick="createNewSquare()"/>
									</td>
								</tr>
								
								<tr>
									<td>
										<img id="diamondTableIcon" class="tableIcon" src="images/table_diamond.png" width='50' height='50' onclick="createNewDiamond()"/>
									</td>
								</tr>
								<tr>
									<td>
										<img id="slantRightTableIcon" class="tableIcon" src="images/table_slant_right.png" width='50' height='50' onclick="createNewSlantRight()"/>
									</td>
								</tr>
								<tr>
									<td>
										<img id="slantLeftTableIcon" class="tableIcon" src="images/table_slant_left.png" width='50' height='50' onclick="createNewSlantLeft()"/>
									</td>
								</tr>
								<tr>
									<td>
										<img id="deleteTable" class="tableIcon" src="images/deleteTableIcon.png" onclick="deleteElement()" onmouseover="trashHover()" onmouseout="trashNormal()" />
									</td>
								</tr>
				
								<!--
								<tr> 
									<td>
				
										<div class= 'labelX'> x: </div>
										<input id= "inputX" type='text' size='1' value='0' onkeyup="changeXLoc(this)">  
										<script language= javascript>
										$("#inputX").prop('disabled', true);
										$("#inputX").prop('readOnly', true);
										</script>
										</script>
									</td>
									
								</tr>
								
								<tr> 
									<td>
										<div class= 'labelY'> y: </div> <input id= "inputY" type='text' size='1' value='0' onkeyup="changeY(this)"> 
										<script language= javascript>
										$("#inputY").prop('disabled', true);
										$("#inputY").prop('readOnly', true);
										</script>
									</td>
									
								</tr>
								
								<tr> 
									<td>
										<input id= "inputRotation" type='text' size='1' value='0' onkeyup="changeRotation(this)"> <div class= 'labelRotation'>  &#176; </div>
										<script language= javascript>
										$("#inputRotation").prop('disabled', true);
										$("#inputRotation").prop('readOnly', true);
										</script>
									</td>
									
								</tr>
								
								<tr> 
									<td>
										<div class= 'labelWidth'> w: </div> <input id= "inputWidth" type='text' size='1' value='0' onkeyup="changeWidth(this)"> 
										<script language= javascript>
										$("#inputWidth").prop('disabled', true);
										$("#inputWidth").prop('readOnly', true);
										</script>
									</td>
									
								</tr>
								
								<tr> 
									<td>
										<div class= 'labelHeight'> h: </div> <input id= "inputHeight" type='text' size='1' value='0' onkeyup="changeHeight(this)"> 
										<script language= javascript>
										$("#inputHeight").prop('disabled', true);
										$("#inputHeight").prop('readOnly', true);
										</script>					
									</td>
									
								</tr>-->
								
								
								
							</table>	
						</td>
						<td id="canvas_containerTD">
							<div id="canvas_container">
							</div>
						</td>
					</tr>
				</table>
			</td>
		</tr>
					
		<tr>
			<td>
				<div id="properties">
					<table id="propertiesTable">
						<tr>
							<td>
								
							</td>
							<td id="dragToRotateTD">
								<table>
									<tr>
										<td>
											<input type="checkbox" name="dragToRotate" value="true" id="dragToRotate" onclick="dragToRotate()"/>
										</td>
										<td>
											<p id="dragToRotateText">Drag to rotate </p>
										</td>
									</tr>
								</table>
								
							</td>
							<td>
								
							</td>
							<td>
								
							</td>
				
							
							
							<!--<td id="elementNameLabel">
								Name:
							</td>
							<td>
								<input id='elementName' type='text' val='' onkeyup='nameChanged()'/>
							</td>-->
						</tr>
					</table>
				</div>
			</td>
		</tr>
	</table>
	
	<br /><br />
	
	<table>
		<tr>
			<td id="instructionsTD" style="border:solid 1px #888; font-size:10px; color:#555; background-color:#eee; padding:8px; text-align:left">
				<?php function tab(){ echo "&nbsp;&nbsp;&nbsp;&nbsp;"; } ?>
				To Create a new table:<br />
				<?php tab(); ?>&bull; <?php if ($iPad) { echo "Tap a table type from the option bar on the left."; } else { echo "Click and drag a table type from the option bar on the left."; } ?><br />
				<?php tab(); ?>&rarr; <?php echoclick(); ?> Save Room to make changes.<br /><br />
				To Modify a table:<br />
				<?php tab(); ?>&bull; <?php echoclick(); ?> and hold then drag a table to reposition.<br />
				<?php tab(); ?>&bull; <?php echoclick(); ?> to select, then click and hold and drag on the resize circles to modify the shape of a table.<br />
				<?php tab(); ?>&bull; <?php echoclick(); ?> to select, then click the table name text to change the name.<br />
				<?php tab(); ?>&rarr; <?php echoclick(); ?> Save Room to make changes.<br /><br />
				To Delete a table:<br />
				<?php tab(); ?>&bull; <?php echoclick(); ?> to select a table then click the Trashcan icon to delete.<br />
				<?php tab(); ?>&rarr; <?php echoclick(); ?> Save Room to make changes.<br /><br />
				To Clone a table:<br />
				<?php tab(); ?>&bull; <?php echoclick(); ?> to select a table then click the Clone Table button in the top bar.<br />
				<?php tab(); ?>&bull; Size and shape of the original table are applied to a new, cloned table.<br />
				<?php tab(); ?>&rarr; <?php echoclick(); ?> Save Room to make changes.<br /><br />
				Rooms:<br />
				<?php tab(); ?>&bull; Choose the current room from the Room dropdown.<br />
				<?php tab(); ?>&bull; Change the room name in Room Title text field<br />
				<?php tab(); ?>&bull; <?php echoclick(); ?> New Room in the top bar to create a new room or Delete Room to delete the current room<br />
				<?php tab(); ?>&rarr; <?php echoclick(); ?> Save Room to make changes.<br /><br />
				Revenue Centers:<br />
				<?php tab(); ?>&bull; Select a table to see the revenue center of that table displayed on the Revenue Center dropdown.<br />
				<?php tab(); ?>&bull; The revenue center is also displayed as a table color.<br />
				<?php tab(); ?>&bull; Change the revenue center of the currently selected table by changing the selection in the dropdown.<br />
				<?php tab(); ?>&bull; A newly created table will have the same revenue center as the currently selected table.<br />
				<?php tab(); ?>&rarr; <?php echoclick(); ?> Save Room to make changes.<br /><br />
			</td>
		</tr>
	</table>
</div>