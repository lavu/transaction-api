<?php
	//UNCOMENT WHEN DEBUGGING
	/*ini_set("display_errors",1);
	$_POST['posted'] = TRUE;
	$_POST['locationID'] = 27;
	$_POST['roomID'] = 9;*/
	
	 function getRoom($locID, $roomID){
    	$result =  array();
    
    	$query = "select * from tables where loc_id='[1]' and _deleted!=1 and id='[2]'";
    	
    	$lavu_query = lavu_query($query, $locID, $roomID);
    
    	while($result_read = mysqli_fetch_assoc($lavu_query)){
    		array_push($result, $result_read);
    	}
    
    	return $result;
    }
    
    function getRoomsSize($locationID){
		$result =  array();
		
		$room_width = 0;
		$room_height = 0;
		
		$query = "select * from config where type='special' and setting='room size [1]'";
		
		$lavu_query = lavu_query($query, $locationID);
		
		while($result_read = mysqli_fetch_assoc($lavu_query)){
			$roomWidth = $result_read['value'];
			$roomHeight = $result_read['value2']; 
    	}
    	
    	if($roomWidth==0 || $roomHeight==0){
    		$room_width = 900;
			$room_height = 820;
    	}
		
		$result = array($roomWidth, $roomHeight);
		
		/*
		paperWidth = 969;
		paperHeight = 1000;
		*/
		
		//$result = array(969, 1000);
    	
    	return $result;
    }
	
	$posted = isset($_POST['posted']);
	
	if($posted){
		
		//getting the id from the post
		$locationID = $_POST['locationID'];
		$roomID = $_POST['roomID'];
		
		//echo $locationID;
		
		$rooms = getRoom($locationID, $roomID);
		
		//tell if the query executed succesfully
		if($rooms){
			$roomSize = getRoomsSize($locationID);

			$toEcho = '';

			foreach($rooms as $room){
				$coord_x = str_replace('|',",",$room['coord_x']);
				$coord_y = str_replace('|',',',$room['coord_y']);
				$centerX = str_replace('|',',',$room['centerX']);
				$centerY = str_replace('|',',',$room['centerY']);
				$widths =  str_replace('|',',',$room['widths']);
				$heights = str_replace('|',',',$room['heights']);
				$rotate =  str_replace('|',',',$room['rotate']);
				$revenueCenters = str_replace('|',',',$room['revenue_centers']);
				
				$shapesArray = explode('|', $room['shapes']);
				foreach($shapesArray as &$shape){
					$shape = '\''.$shape.'\'';
				}
				
				$shapes = implode(',',$shapesArray);
				
				
				$namesArray = explode('|', $room['names']);
				$numTables = count($namesArray);
				foreach($namesArray as &$name){
					$name = '\''.ConnectionHub::getConn('rest')->escapeString($name).'\'';
				}
				
				$names = implode(',',$namesArray);
				
				$roomTitle = $room['title'];
				
				if (strlen($revenueCenters) == 0 && $numTables > 0) {
					$revenueCenters = "0";
					for ($i = 1; $i < $numTables; $i++) {
						$revenueCenters .= ",0";
					}
				}

				$toEcho .= '{';
					$toEcho .= '"coord_x":['.$coord_x.'],';
					$toEcho .= '"coord_y":['.$coord_y.'],';
					$toEcho .= '"centerX":['.$centerX.'],';
					$toEcho .= '"centerY":['.$centerY.'],';
					$toEcho .= '"shapes":['.$shapes.'],';
					$toEcho .= '"widths":['.$widths.'],';
					$toEcho .= '"heights":['.$heights.'],';
					$toEcho .= '"names":['.$names.'],';
					$toEcho .= '"rotate":['.$rotate.'],';
					$toEcho .= '"revenueCenters":['.$revenueCenters.'],';
					$toEcho .= '"roomSize":['.$roomSize[0].','.$roomSize[1].'],';
					$toEcho .= '"roomTitle":"'.$roomTitle.'"';
				$toEcho .= '}';
			}
			
			echo $toEcho;
		}else{
			//echo 'FAIL';
			
			$toEcho = '{';
				$toEcho .= '"coord_x":[],';
				$toEcho .= '"coord_y":[],';
				$toEcho .= '"shapes":[],';
				$toEcho .= '"widths":[],';
				$toEcho .= '"heights":[],';
				$toEcho .= '"names":[],';
				$toEcho .= '"rotate":[],';
				$toEcho .= '"revenueCenters":[],';
				$toEcho .= '"roomSize":[969,1000],';
				$toEcho .= '"roomTitle":"New Room"';
			$toEcho .= '}';
			
			echo $toEcho;
		}
	//if no data sent, throw an error
	}else{
		echo "NO_DATA";	
	}
?>
