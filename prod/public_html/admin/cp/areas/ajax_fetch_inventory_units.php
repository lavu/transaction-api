<?php 
require_once (__DIR__ . "/../resources/inventory_functions.php");
$response = [];
$items = get_inventory_items();

function cmp($value1, $value2) {
 return strcmp($value1["name"], $value2["name"]);
}
usort($response, "cmp");
$a_replace_chars = array("'", '"', "\n");
foreach ($items as $invValues) {
    $response['options'] .= "<option value='{$invValues['id']}'>" . trim(str_replace($a_replace_chars, "", $invValues['name'])) . "</option>";
    $response['units'][$invValues['id']] = $invValues['salesUnitID'];
}
echo json_encode($response);
?>