<?php
	require_once(dirname(__DIR__)."/resources/core_functions.php");
	global $data_name;

	if (isset($_GET['dtt_validation'])) {
		$dttInfo = reqvar("dtt", "");
		$locationid = reqvar("locationid", "");
		$data_name = reqvar("dataname", "");
		$resultInfo = checkDTTFormValidation($dttInfo);

		if ($resultInfo == 'success') {
			$resultInfo = checkDuplicateDttSettings($dttInfo);
		}

		if ($resultInfo == 'success') {
			$resultInfo = saveDTTSettings($dttInfo, $locationid);
			unset($_POST);
		}

		echo $resultInfo;
		exit;
	}

	if (isset($_GET['dtt_delete'])) {
		$configId = reqvar("config_id", "");
		$data_name = reqvar("dataname", "");
		$resultInfo = deleteDttSetting($configId);
		echo $resultInfo;
		exit;
	}

	echo "<br><br>";
	/**
	 * This function is used to save/update DTT settings in config table.
	 * @param array $dttInfo
	 * @param int $locationid
	 */
	function saveDTTSettings ($dttInfo, $locationid) {
		$resultInfo = 'fail^#&Something went wrong, unable to save DTT setting.';

		foreach ($dttInfo as $key => $val) {
			if ( strpos($key, 'rowId') !== false && $val['camera_name'] != '' ) {
				$dttInsertArr[] =  "('".$locationid."', 'dtt_integration', '".$val['camera_name']."', '".$val['ip_address']."', '".$val['port_number']."', '', '', 'dtt_camera', '', '', '', '', 0, '', '', '', '', '', '', '', '', '0', '0')";
			}

		   if (strpos($key, 'rowId') === false && $val['camera_name'] != '' ) {
				$dttUpdatetArr[$key] = $val;
		   }
		}

		$resultInfo = getDttInsertUpdate($dttInsertArr, $dttUpdatetArr);
		return $resultInfo;
	}

	/**
	 * Thisfunction is used to insert, update dtt settings.
	 * @param array $dttInsertArr
	 * @param array $dttUpdatetArr
	 * @return string success/ fail message
	 */
	function getDttInsertUpdate($dttInsertArr, $dttUpdatetArr) {
		$resultInfo = 'fail^#&Something went wrong, unable to save DTT setting.';
		$dttUpdate = 1;
		$dttInsert = 1;

		if (!empty($dttInsertArr)) {
			$dttInsertValue = implode(',', $dttInsertArr);
			$dttInsert = lavu_query('INSERT INTO `config` (`location`, `setting`, `value`, `value2`, `value3`, `value4`, `value_long`, `type`, `value5`, `value6`, `value7`, `value8`, `_deleted`, `value9`, `value10`, `value11`, `value12`, `value13`, `value14`, `value_long2`, `value15`, `value16`, `value17`) VALUES ' . $dttInsertValue );
		}

		if (!empty($dttUpdatetArr)) {
			$dttUpdate = updateDttSettings($dttUpdatetArr);
		}

		if ($dttInsert && $dttUpdate) {
			$resultInfo = 'success^#&DTT Integration settings are saved successfully.';
		}

		return $resultInfo;
	}

	/**
	 * This function is used to update dtt settings.
	 * @param array $dttUpdatetArr
	 * @return number updatestatus
	 */
	function updateDttSettings($dttUpdatetArr) {
		$dttCount = 0;
		$dttUpdate = 1;

		foreach ($dttUpdatetArr as $dttKey => $dttVal) {
			$dttUpdate = lavu_query('UPDATE `config` SET `value` = "[2]", `value2` = "[3]", `value3` = "[4]" WHERE `id` = "[1]"', $dttKey, $dttVal['camera_name'], $dttVal['ip_address'], $dttVal['port_number']);
			($dttUpdate) ? $dttCount++ : $dttCount;
		}
		$dttUpdate = ($dttCount == count($dttUpdatetArr)) ? 1 : 0;

		return $dttUpdate;
	}

	/**
	 * This function is used to validate DTT integration settings form page.
	 * @param array $dttInfo
	 * @return string fail/ success message
	 */
	function checkDTTFormValidation($dttInfo) {
		$resultData = 'success';
		foreach ($dttInfo as $rowData) {
			$resultData = dttSettingValidation($rowData);
			if (substr($resultData, 0, 4) == 'fail') {
				break;
			}
		}

		return $resultData;
	}

	/**
	 * This function is used to check dtt setting validation.
	 * @param array $rowData
	 * @return string fail/ success message
	 */
	function dttSettingValidation($rowData) {
		$resultInfo = 'success';

		foreach ($rowData as $keyName => $value) {
			$value = trim($value);
			$columnName = ucwords(str_replace("_", " ", $keyName));
			if (empty($value)) {
				return 'fail^#&Please enter '.$columnName;
			} else {
				$validationMsg = checkValidKey($keyName, $value);
				if ($validationMsg == 'fail') {
					return $validationMsg.'^#&Please enter valid '.$columnName;
				}
			}

		}

		return $resultInfo;
	}

	/**
	 * This function is used to check key validation for DTT settings.
	 * @param string $keyName
	 * @param string $value
	 * @return string fail/success message
	 */
	function checkValidKey($keyName, $value) {
		$resultInfo = 'success';
		if ($keyName == 'ip_address') {
			$validation = filter_var($value, FILTER_VALIDATE_IP);
			if ($validation == null) {
				$resultInfo = 'fail';
			}
		}

		if ($keyName == 'port_number') {
			$validation = filter_var($value, FILTER_VALIDATE_INT);
			if ($validation == null) {
				$resultInfo = 'fail';
			}
		}

		return $resultInfo;
	}

	/**
	 * This function is used to check duplicate DTT settings.
	 * @param array $dttInfo
	 * @return success/ fail message
	 */
	function checkDuplicateDttSettings($dttInfo) {
		$resultInfo = 'success';
		$checkDuplicate = array();

		foreach ($dttInfo as $value) {
			$checkDuplicate[array_search ($value['camera_name'], $value)][$value['camera_name']][] = $value['camera_name'];
			$checkDuplicate[array_search ($value['ip_address'], $value)][$value['ip_address']][] = $value['ip_address'];
		}

		foreach ($checkDuplicate as $keyName => $values) {
			foreach ($values as $duplicateRec) {
				if (count($duplicateRec) > 1) {
					$column = ucwords(str_replace("_", " ", $keyName));
					return 'fail^#&Please enter unique '.$column;
				}
			}
		}

		return $resultInfo;
	}

	/**
	 * This function is used to delete DTT camera settings and related paired information.
	 * @param  $configId
	 * @return string delete status.
	 */
	function deleteDttSetting($configId) {
		$resultInfo = 'Something went wrong, selected DTT setting is not deleted.';
		$delDttPairedData = lavu_query('DELETE FROM `dtt_pairs` WHERE `config_id` = "[1]" ', $configId);
		$delConfigData = lavu_query('DELETE FROM `config` WHERE `id` = "[1]" AND `setting` = "dtt_integration" ', $configId);
		if ($delConfigData && $delDttPairedData) {
			$resultInfo = 'Selected DTT setting is deleted successfully.';
		}

		return $resultInfo;
	}

	$get_dtt_data = lavu_query('SELECT con.id as id, con.value as camera_name, con.value2 as ip_address, con.value3 as port_number, dtt.device_uuid FROM config con LEFT JOIN  dtt_pairs dtt ON con.id = dtt.config_id where con.setting = "dtt_integration" AND con.location = "[1]" AND con._deleted = 0', $locationid);
	$tabledata = '';
	$dttData = array();
	$pre_cameraName = '';
	$uuidName = array();
	while ($info = mysqli_fetch_assoc($get_dtt_data) ) {
	  $dttData[$info['camera_name']][] = $info;
	  $cameraName = $info['camera_name'];
	  $uuidName[$info['camera_name']][$info['device_uuid']] = $info['device_uuid'];

		if ($pre_cameraName == $cameraName) {
			$uuidName[$info['camera_name']][$info['device_uuid']] = $info['device_uuid'];
			$device_uuid = implode('", "', $uuidName[$info['camera_name']]);
			$info['device_uuid'] = $device_uuid;
			$dttData[$info['camera_name']] = $info;
		} else {
			$dttData[$info['camera_name']] = $info;
		}

	  $pre_cameraName = $cameraName;
	}

	foreach ($dttData as $dttInfo) {
		$rowId = $dttInfo['id'];
		$PosPairedDevices = '';
		if (trim($dttInfo['device_uuid']) != '') {
			$diviceDtls = array();
			$getPosPairDevice = lavu_query('SELECT name, UUID FROM `devices` where UUID in ("'.$dttInfo['device_uuid'].'")');
			while ($deviceInfo = mysqli_fetch_assoc($getPosPairDevice) ) {
				$diviceDtls[$deviceInfo['UUID']] = $deviceInfo['name'];
			}

			$PosPairedDevices = implode(', ', $diviceDtls);
		}

		$tabledata .= "<tr>
		<td><input placeholder='Enter Name' type='text' style='color: #1c591c;width:140px;' name='dtt[$rowId][camera_name]' size='20' value='".htmlspecialchars($dttInfo['camera_name'], ENT_QUOTES)."'></td>
		<td><input placeholder='Enter IP Address' type='text' style='color: #1c591c;width:140px;' name='dtt[$rowId][ip_address]' size='20' value='".htmlspecialchars($dttInfo['ip_address'], ENT_QUOTES)."'></td>
		<td><input placeholder='Enter Port' type='text' style='color: #1c591c;width:70px;' name='dtt[$rowId][port_number]' size='20' value='".htmlspecialchars($dttInfo['port_number'], ENT_QUOTES)."'></td><td><div style='width:200px;margin-top:5px;color:#808080'>".htmlspecialchars($PosPairedDevices, ENT_QUOTES)."</div></td><td width='20px'><a onclick='click_delete_dtt(this, $rowId)' style='cursor: pointer'><img src='images/little_trash.gif' border='0'></a></td>";
		$tabledata .= "</tr>";
	}

	$display .= "<form name='dtt_sform' id='dtt_sform' enctype=\"multipart/form-data\" method='post' action=''><table cellspacing='1' cellpadding='3' width='60%' style='border:solid 2px #aaaaaa;'>
	    				<tr>
	    				<td colspan='2' bgcolor='#98b624' style='color:#ffffff; font-weight:bold; height:27px; padding:5px 0px 2px 0px;' align='center'>
	    				<div class='divider' style='display:inline-block; width:400px; background-color:#98b624; margin:2px 0px 2px 0px; padding:10px 0px 10px 0px; color:white; font-size:12px; font-weight:bold;'> DTT Surveillance Devices </div>
	    				</td>
	    				</tr>
    				<tr>
    					<td colspan='2' align='center'>
    						<table id='dtt_tbl_browse_id' style='width:100%' cellspacing='0' cellpadding='4'>
    							<thead>
    							 <col width='140'><col width='140'><col width='70'><col width='200'><col width='20'>
    							<tr>
    								<td style='padding-left:4px; padding-right:4px;border-right: 5px solid #fff;' bgcolor='#cdcdcd'>Camera / DVR Name<div style='position:relative;left:5px;display:inline-block;'></div></td>
    								<td style='padding-left:4px; padding-right:4px;border-right: 5px solid #fff;' bgcolor='#cdcdcd'>IP Address<div style='position:relative;left:5px;display:inline-block;'></div></td>
									<td style='padding-left:4px; padding-right:4px;text-align:left;border-right: 5px solid #fff;' bgcolor='#cdcdcd'>Port<div style='position:relative;left:5px;display:inline-block;'></div></td>
								<td style='padding-left:4px; padding-right:4px;text-align:left;' bgcolor='#cdcdcd'>POS Pairing(s)<div style='position:relative;left:5px;display:inline-block;'></div></td>
								<td style='padding-left:4px; padding-right:4px;text-align:left;'></td>
    							</tr>
    							</thead>
							<tbody>
								".$tabledata."
							</tbody>
						</table>
    					</td>
    				</tr>
					<tr>
						<td colspan='2' align='center'>
							<a style='cursor:pointer;float:left;' class='addNewBtn' onClick='addNewLineItem();'><span class='plus'> + </span> ".speak('Add New')."</a>
						</td>
					</tr>
					<tr>
						<td colspan='2' align='center'>&nbsp;<input type='button' value='".speak("Save")."' class='saveBtn' onclick='validateForm();'><br>&nbsp;</td>
						</tr>
						</table>
						</form>
						<script>
						var rowId = 1;

						function addNewLineItem() {
						var lineItemRow = 'rowId_'+( rowId++ );
						var tabledata = '<tr><td><input placeholder=\'Enter Name\' type=\'text\' style=\'color: #1c591c;width:140px;\' name=\'dtt['+lineItemRow+'][camera_name]\' size=\'20\'></td><td><input placeholder=\'Enter IP Address\' type=\'text\' style=\'color: #1c591c;width:140px;\' name=\'dtt['+lineItemRow+'][ip_address]\'  size=\'20\'></td> <td><input placeholder=\'Enter Port\' type=\'text\' style=\'color: #1c591c;width:70px;\' name=\'dtt['+lineItemRow+'][port_number]\' size=\'20\'></td><td width=\'200px\'>&nbsp;</td><td width=\'20px\'><a onclick=\'click_delete_dtt(this)\' style=\'cursor: pointer\'><img src=\'images/little_trash.gif\' border=\'0\'></a></td></tr>';
						var lastrow = $('#dtt_tbl_browse_id').find('tr').last();
						$(tabledata).insertAfter(lastrow);
						}

						function click_delete_dtt(obj,configid) {
						   var deleteDtt = confirm('Are you sure you want to delete this DTT setting?');
						   if(deleteDtt == true) {
								obj.parentNode.parentNode.remove();
								var url = 'areas/dtt_integration.php?dtt_delete=1&dataname='+'$data_name'+'&config_id='+configid;
								$.ajax({
					            type: 'POST',
					            url: url,
					            success: function(result) {
								    alert(result);
					            }
					        });
							}
						}

						function validateForm() {
							var formData = $('#dtt_sform').serialize();
							if(formData == '') {
							 alert('Please add at least one DTT Surveillance Device');return false;
							}
							var url = 'areas/dtt_integration.php?dtt_validation=1&locationid='+'$locationid'+'&dataname='+'$data_name';
							$.ajax({
					            type: 'POST',
					            url: url,
								data: $('#dtt_sform').serialize(),
					            success: function(data) {
									var getResult = data.split('^#&');
									if(getResult[0] == 'success') {
										alert(getResult[1]);
										location.reload();
									} else {
										alert(getResult[1]);
									}
					            }
					        });
						}
						</script>";
	echo $display;
	echo "<br><br>";
?>
<script type="text/javascript" src="/manage/js/jquery/js/jquery-ui.min_1.9.2.js"></script>