<?php

	//LP-9797
	require_once ($_SERVER['DOCUMENT_ROOT'].'/cp/areas/header.php');

	echo "<br><br>";

	function api_create_new_password($length=12)
	{
		$str = "";
		for($i=0; $i<$length; $i++)
		{
			$n = rand(0,61);
			if($n < 10) // 0-9
			{
				$setn = 48 + $n;
			}
			else if($n<=35) // 10-35
			{
				$setn = 55 + $n;
			}
			else if($n<=61) // 36-61
			{
				$setn = 61 + $n;
			}
			$str .= chr($setn);
		}
		return $str;
	}

	function areas_api_get_api_credentials($locationid) {
		$api_query = lavu_query("select * from `locations` where `id`='[1]'",$locationid);
		if(mysqli_num_rows($api_query))
		{
			$api_read = mysqli_fetch_assoc($api_query);

			if($api_read['api_token']=="")
			{
				$create_token = api_create_new_password(20);
				lavu_query("update `locations` set `api_token`=AES_ENCRYPT('$create_token','lavutokenapikey') where `id`='[1]'",$locationid);
			}
			if($api_read['api_key']=="")
			{
				$create_key = api_create_new_password(20);
				lavu_query("update `locations` set `api_key`=AES_ENCRYPT('$create_key','lavuapikey') where `id`='[1]'",$locationid);
			}
		}

		$token_query = lavu_query("select AES_DECRYPT(`api_token`,'lavutokenapikey') as `api_token`, AES_DECRYPT(`api_key`,'lavuapikey') as `api_key` from `locations` where `id`='[1]'",$locationid);
		if(mysqli_num_rows($token_query))
		{
			$token_read = mysqli_fetch_assoc($token_query);
			return $token_read;
		}
		else
		{
			return array();
		}
	}

	$api_query = lavu_query("select * from `locations` where `id`='[1]'",$locationid);
	if(mysqli_num_rows($api_query))
	{
		$api_read = mysqli_fetch_assoc($api_query);

		if($api_read['api_token']=="")
		{
			$create_token = api_create_new_password(20);
			lavu_query("update `locations` set `api_token`=AES_ENCRYPT('$create_token','lavutokenapikey') where `id`='[1]'",$locationid);
		}
		if($api_read['api_key']=="")
		{
			$create_key = api_create_new_password(20);
			lavu_query("update `locations` set `api_key`=AES_ENCRYPT('$create_key','lavuapikey') where `id`='[1]'",$locationid);
		}
		$token_query = lavu_query("select AES_DECRYPT(`api_token`,'lavutokenapikey') as `api_token`, AES_DECRYPT(`api_key`,'lavuapikey') as `api_key` from `locations` where `id`='[1]'",$locationid);
		$token_read = mysqli_fetch_assoc($token_query);

		echo "<br>Location ID: " . $locationid;
		echo "<br>Restaurant ID: " . admin_info("companyid");
		echo "<br>Dataname: " . $data_name;
		echo "<br>Key: " . $token_read['api_key'];
		echo "<br>Token: " . $token_read['api_token'];
		echo "<br><br><a href='?mode=settings_old_api_doc' target='_blank'>Basic API Documentation (requires technical knowledge)</a>";
		echo "<form id=\"api2\" action=\"areas/api_doc.php\" method=\"post\" target=\"_blank\">";
		echo "<input type=\"hidden\" name=\"dataname\" value=\"$data_name\" />";
		echo "<input type=\"hidden\" name=\"api_key\" value=\"". $token_read['api_key'] . "\" />";
		echo "<input type=\"hidden\" name=\"api_token\" value=\"" .$token_read['api_token'] . "\" />";
		echo "<a href=\"#\" onclick=\"$('#api2').submit();\" >New API Documentation</a>";
		echo "</form>";
		echo "<br><br> When installing Lavu Local Server using the following <br>API information make sure that you have the latest installer package <br>for the LLS.  Previous installer versions are no longer supported by the API on install attempts.";
	}
?>
