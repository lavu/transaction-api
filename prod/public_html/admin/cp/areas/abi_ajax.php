<?php
/**
 * Created by PhpStorm.
 * User: hayat
 * Date: 11/9/17
 * Time: 1:04 PM
 */

session_start();
require_once (dirname(dirname(dirname(__FILE__))) ."/cp/resources/core_functions.php");
require_once(dirname(dirname(dirname(__FILE__))) ."/cp/resources/chain_functions.php");
require_once (dirname(__FILE__)."/reports/abi_report_config.php");

$data_name = sessvar('admin_dataname');

if($data_name == ''){return;}

$data_name = 'anheuser_busch';
$action = (isset($_REQUEST['action'])) ? $_REQUEST['action'] : '';


$region = $_REQUEST['region'];
if($action == 'getLocation') {
    list($getAllLocation, $reportLocations) = getRegionWiseChainLocations($data_name, '', $reportRegions);
    $selectedRegionLocations = $getAllLocation[$region];
    $str .= '<select name="selLocation[]" id="selLocation" multiple="multiple" size="3" style="height:5em">';
    $str .= '<option value = "">--Select Location--</option>';
    foreach($selectedRegionLocations as $locationDetail) {
        $str .= '<option value = "' . $locationDetail["data_name"] . '">' . $locationDetail["company_name"] .' (#' . $locationDetail["company_id"] . ')';
    }
    $str .= '</select>';
    echo $str;
    exit;
}