<?php

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

ini_set('memory_limit', -1);
ini_set('max_execution_time', 0);

date_default_timezone_set($location_info['timezone']);

spl_autoload_register(
    function ($className) {
        require_once (__DIR__ . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'norway_reports' . DIRECTORY_SEPARATOR . $className .
             '.php');
    });

/**
 * @param string $reportType
 * @param string $register
 * @param string $fileDir
 * @return string[]|NULL[]|unknown[]
 *  Here the $reportType will z or x, by default it is "z", based on the params this function will check for already existing z-reports/x-reports sequence number and returns the calculated "nextSequenceNumber", "lastReportDateTime", "lastReportOrderAutoIncrementId"
 */
function getFileSequenceNumber($reportType = 'z', $register = '', $fileDir = '')
{
    $nextSequenceNumber = sprintf("%'.06d\n", 1);
    $existingReportFiles = array();

    $pattern = $reportType . "report_" . $register;

    foreach (glob($fileDir . DIRECTORY_SEPARATOR . $pattern . "_*.xml") as $filename) {
    	$fileDetails = explode("_", basename($filename));
    	$existingReportFiles[] = intval(str_replace(".xml", "", $fileDetails[4]));
    }
    if (count($existingReportFiles)) {
        $nextSequenceNumber = sprintf("%'.06d\n", max($existingReportFiles) + 1);
        $lastReportDetails = sprintf("%'.06d\n", max($existingReportFiles));
    }
    // die($lastReportDetails);

    $lastFileDateTime = '';
    $lastReportOrderAutoIncrementId = '';
    if (isset($lastReportDetails) || $reportType == 'x') {
        // die($fileDir.DIRECTORY_SEPARATOR.$pattern."_*_".$lastReportDetails.".xml");
        if ($reportType == 'x') {
            // now scan and get the last z report info to generate the next x report
            $existingZReportFiles = array();
            $pattern = "zreport_" . $register;
            // echo $fileDir.DIRECTORY_SEPARATOR.$pattern;
            foreach (glob($fileDir . DIRECTORY_SEPARATOR . $pattern . "_*.xml") as $filename) {
                $fileDetails = explode("_", basename($filename));
                $existingZReportFiles[] = intval(str_replace(".xml", "", $fileDetails[4]));
            }           
            if (count($existingZReportFiles)) {
                $lastReportDetails = sprintf("%'.06d\n", max($existingZReportFiles));
            }
        }
        // echo ($fileDir.DIRECTORY_SEPARATOR.$pattern);
        foreach (glob($fileDir . DIRECTORY_SEPARATOR . $pattern . "_*.xml") as $filename) {
            $fileDetails = explode("_", basename($filename));
            if (trim($lastReportDetails) == trim(explode(".", $fileDetails[4])[0])) {
                $lastFileDateTime = $fileDetails[2];
                $lastReportOrderAutoIncrementId = $fileDetails[3];
            }
        }
    }

    $array = array(
        'nextSequenceNumber' => trim($nextSequenceNumber),
        'lastReportDateTime' => str_replace("/", ":", $lastFileDateTime),
        'lastReportOrderAutoIncrementId' => $lastReportOrderAutoIncrementId
    );
    
    return $array;
}
/*
 * sanitizeXML function will sanitize the given xml string
 */
function sanitizeXML($xmlStr = '')
{
    if ($xmlStr != '') {
        $xmlStr = preg_replace('/&(?!#?[a-z0-9]+;)/', '&amp;', $xmlStr);
        $xmlStr = preg_replace("/(^[\r\n]*|[\r\n]+)[\s\t]*[\r\n]+/", "\n", $xmlStr);
        $xmlStr = str_replace(' & ', ' &amp; ', html_entity_decode((htmlspecialchars_decode($xmlStr))));
    }
    return $xmlStr;
}

/**
 * @param unknown $xmlStr
 * @param unknown $fileDir
 * @param unknown $file
 * @param unknown $fileType
 * @param string $showDownload
 * @param string $printStr
 * based on the params, this function will create the new xml file with given param data in $xmlStr 
 * and stores it in the $fileDir with filename $file,
 * if param $showDownload is true, it will allow the user to download the file instantly, 
 * The param $printStr if not empty indicates POS App error message.
 */
function createFile($xmlStr, $fileDir, $file, $fileType, $showDownload = false, $printStr = '')
{
    if (!file_exists($fileDir)) {
        mkdir($fileDir, 0777, true);
    }

    try {
        $myfile = fopen($file, "w");
        chmod ($file, 0777);
        file_put_contents($file, $xmlStr);
        if ($printStr != '') {
            echo "1|".$printStr;
            lavu_exit();
        }
    } catch (Exception $e) {
        echo 'There is problem generating the <b>' . basename($file) . '</b> file, please check the below message.';
        echo $e->getMessage();
    }

    if ($showDownload) {
        if (file_exists($file)) {
            header('Content-Description: File Transfer');
            header('Content-Type: text/' . $fileType . '');
            header('Content-Disposition: attachment; filename=' . basename($file));
            header('Content-Transfer-Encoding: binary');
            header('Expires: 0');
            header('Cache-Control: no-cache, must-revalidate');
            header('Pragma: public');
            header('Content-Length: ' . filesize($file));
            ob_clean();
            flush();
            readfile($file);
            exit();
        } else {
            echo 'There is problem generating the <b>' . basename($file) . '</b> file, please check.';
        }
    }
}

/**
 * @param unknown $xmlStr
 * @return string
 *  This function adds an header string dynamically to append the already existing z report/ x report,
 *  so that users can download the file in the form of xml to view in the http://localhost/cp/index.php?m=mnorwayReport---UI page 
 *  Z reports and X Reports Section
 */
function addHeaderNameSpaceXMLTags($xmlStr) {
    $xmlStr = '<?xml version="1.0"?><n1:auditfile xmlns:n1="urn:StandardAuditFile-Taxation-CashRegister:NO" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="urn:StandardAuditFile-Taxation-CashRegister:NO Norwegian_SAF-T_Cash_Register_Schema_v_1.00.xsd">'.$xmlStr.'</n1:auditfile>';
    return $xmlStr;
}


/**
 * @param unknown $xmlStr
 * @return mixed
 * This function removes the header string dynamically 
 * so that the already existing z report/ x report data will be appended to the cash Register XML FIle
 */
function removeHeaderNameSpaceXMLTags($xmlStr) {
    $xmlStr = str_replace('<?xml version="1.0"?><n1:auditfile xmlns:n1="urn:StandardAuditFile-Taxation-CashRegister:NO" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="urn:StandardAuditFile-Taxation-CashRegister:NO Norwegian_SAF-T_Cash_Register_Schema_v_1.00.xsd">',"",$xmlStr);
    $xmlStr = str_replace('</n1:auditfile>',"",$xmlStr);
    return $xmlStr;
}

/**
 * @param unknown $name
 * @param unknown $setdate
 * @param unknown $onselect
 * This function is used for http://localhost/cp/index.php?m=mnorwayReport---UI page
 * to show the startDate and endDate for downloading the cash register xml file.
 */
function newDatePicker($name,$setdate,$onselect)
{
    echo "<script language='javascript'>";
    //echo "DatePickerControl.minDate='2017-05-01'";
    echo "DatePickerControl.onSelect = function(inputid) { ";
    
    echo " if(inputid == 'startDate')
            { 
                //alert(document.getElementById(inputid).value);
                var startDate = new Date(document.getElementById(inputid).value);
                var endDate = new Date(document.getElementById('endDate').value);
                
                if (startDate > endDate) {
                    alert('".speak('Start Date cannot be greater than End Date')."');
                    document.getElementById(inputid).value = '';
                    return false;
                }
            } 
            if(inputid=='endDate')
            {
                var startDate = new Date(document.getElementById('startDate').value);
                var endDate = new Date(document.getElementById(inputid).value);
                
                if (endDate < startDate) {
                    alert('".speak('End Date cannot be less than Start Date')."');
                    document.getElementById(inputid).value = '';
                    return false;
                } 
                
            }";
    echo "  " . str_replace("[value]","document.getElementById(inputid).value",$onselect);
    echo "} ";
    echo "</script>";
    
    $set_date_start = $setdate;
    $date_start_ts = mktime(0,0,0,substr($set_date_start,5,2),substr($set_date_start,8,2),substr($set_date_start,0,4));
    
    echo "<input type=\"hidden\" id=\"DPC_TODAY_TEXT\" value=\"today\">";
    echo "<input type=\"hidden\" id=\"DPC_BUTTON_TITLE\" value=\"Open calendar...\">";
    echo "<input type=\"hidden\" id=\"DPC_MONTH_NAMES\" value=\"['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December']\">";
    echo "<input type=\"hidden\" id=\"DPC_DAY_NAMES\" value=\"['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat']\">";
    echo "<input type=\"text\" name=\"$name\" id=\"$name\" datepicker=\"true\" datepicker_format=\"YYYY-MM-DD\" value=\"".date("Y-m-d",$date_start_ts)."\" datepicker_max=\"".date("Y-m-d")."\">";
}

if (isset($_GET['downloadFile']) && $_GET['downloadFile'] != '') {

    $relativePath = norwayCashRegister::getFilePathDir();
    $downloadFile = $relativePath . DIRECTORY_SEPARATOR . $data_name . DIRECTORY_SEPARATOR . basename($_GET['downloadFile']);

    header('Content-Description: File Transfer');
    header('Content-Type: text/xml');
    header('Content-Disposition: attachment; filename=' . $_GET['downloadFile']);
    header('Content-Transfer-Encoding: binary');
    header('Expires: 0');
    header('Cache-Control: no-cache, must-revalidate');
    header('Pragma: public');
    header('Content-Length: ' . filesize($downloadFile));
    ob_clean();
    flush();

    readfile($downloadFile);
    exit();
}

//show UI
if(!isset($_GET['fiscalYear']) && $mode != 'z_report' && $mode != 'x_report') {
    //echo '<input type="text" name="edit1" id="edit1" datepicker="true" datepicker_format="YYYY-MM-DD">';
    echo "<script type=\"text/javascript\" src=\"resources/datepickercontrol.js\"></script>";
    echo "<link type=\"text/css\" rel=\"stylesheet\" href=\"resources/datepickercontrol.css\">";
    echo "<script type=\"text/javascript\"> function validateDates() { if (document.getElementById('startDate').value == '' || document.getElementById('endDate').value == ''){ alert('Please enter Start/End Dates!'); return false;} else {return true;}} </script>";
    echo '<div style="float:left;text-align:left;"><h3>'.speak("Norway Cash Register XML - Fiscal Report").'</h3></div><br /><br />';
    echo '<form action="'.$_SERVER['REQUEST_URI'].'" method="GET" onsubmit="return validateDates();">';
    echo '<div style="float:left;">'.speak('Select Start and End Dates').':</div>'.newDatePicker('startDate',date('Y-m-d',strtotime("-1 days")),"");
    echo newDatePicker('endDate',date('Y-m-d'),"");
    echo '<br /><br />';
    $currentYear = date('Y');
    echo speak('Select Fiscal Year').':<select name="fiscalYear">';
    for($i=$currentYear-10; $i<=$currentYear; $i++){
        if($i==$currentYear){
            echo '<option value="'.$i.'" selected=selected>'.$i.'</option>';
        } else {
            echo '<option value="'.$i.'">'.$i.'</option>';
        }
    }
    echo '</select>';
    echo '<br /><br />';
    echo '<input type="hidden" name="mode" value="mnorwayReport"/>';
    echo '<input type="submit" value="'.speak('Download').'"/>';
    echo "</form>";
    
    echo "<br /><br /><h4>".speak('Z Reports')."</h4>";
    
    
    $pattern = str_replace(" ", "\ ", 'zreport');
    $fileDir = norwayCashRegister::getInstance()->filePathDir . DIRECTORY_SEPARATOR . $data_name;
    //echo $fileDir.DIRECTORY_SEPARATOR.$pattern;die;
    echo "<ol>";
    foreach (glob($fileDir . DIRECTORY_SEPARATOR . $pattern . "_*.xml") as $filename) {
        echo '<li><a href="'.$_SERVER['REQUEST_URI'].'&downloadFile='. basename($filename) .'">'.basename($filename).'</a></li>';
    }
    echo "</ol>";
    
    echo "<br /><br /><h4>".speak('X Reports')."</h4>";
    
    
    $pattern = str_replace(" ", "\ ", 'xreport');
    $fileDir = norwayCashRegister::getInstance()->filePathDir . DIRECTORY_SEPARATOR . $data_name;
    //echo $fileDir.DIRECTORY_SEPARATOR.$pattern;die;
    echo "<ol>";
    foreach (glob($fileDir . DIRECTORY_SEPARATOR . $pattern . "_*.xml") as $filename) {
        echo '<li><a href="'.$_SERVER['REQUEST_URI'].'&downloadFile='. basename($filename) .'">'.basename($filename).'</a></li>';
    }
    echo "</ol>";
    
} else {

    // create norwayCashregister object with basic initialization data
    $norwayCashRegisterObj = norwayCashRegister::getInstance();
    if (isset($_GET['fiscalYear'])) {
        if (isset($_GET['startDate']) && isset($_GET['endDate'])) {
            $norwayCashRegisterObj->generateXMLFile($_GET['fiscalYear'], '', $_GET['startDate'], $_GET['endDate']);
        }
        if (isset($_GET['noOfmonths'])) {
            $norwayCashRegisterObj->generateXMLFile($_GET['fiscalYear'], $_GET['noOfmonths']);
        }
    }
    
    $specificLocationId = sessvar('location_info')['id'];
    $fileDir = $norwayCashRegisterObj->filePathDir . DIRECTORY_SEPARATOR . $data_name;
    $createReportPDF = false;
    
    if (isset($mode) && isset($register_name)) {
        $createReportPDF = true;
        $mode = explode("_", $mode);
        $mode = $mode[0];
        $detailsForNewReport = getFileSequenceNumber($mode, $register_name, $fileDir);
        $specificLocationId = $_REQUEST['loc_id'];

        $norwayConfigQuery = lavu_query("SELECT `value` FROM `config` WHERE `setting` = 'norway_vat_id'");
        $norwayConfigValues = mysqli_fetch_assoc($norwayConfigQuery);

        if($norwayConfigValues['value'] != "")
        {
            $norwayCashRegisterObj->organizationNumber = (int) str_replace(" ", "", $norwayConfigValues['value']);
            $norwayCashRegisterObj->organizationVAT = strtoupper(str_replace($norwayCashRegisterObj->organizationNumber, "", str_replace(" ", "", $norwayConfigValues['value'])));
        } else {
            $orgDataArr = explode('Org. nr.', $company_info['company_name']);
            $norwayCashRegisterObj->organizationNumber = (int) str_replace(" ", "", $orgDataArr[1]);
            $norwayCashRegisterObj->organizationVAT = strtoupper(str_replace($norwayCashRegisterObj->organizationNumber, "", str_replace(" ", "", $orgDataArr[1])));
        }
    }
    
    // $norwayFinancialObj = norwayFinancial::getInstance();
    // $norwayFinancialObj->generateXML($_GET['fiscalYear'], $_GET['noOfmonths']);
    
    // ob_clean(); //for debugging we can use this to echo the content to browser
    
    // EVENTS
    $events = array(
        // 'Location Chosen' => array('code'=>'13999','name'=>'Other'),
        // 'Settings reloaded' => array('code'=>'13007','name'=>'Update of POS application'),
        'Order Opened' => array(
            'code' => '13999',
            'name' => 'Other'
        ),
        'Payment Applied' => array(
            'code' => '13999',
            'name' => 'Other'
        ),
        'Order Receipt Emailed' => array(
            'code' => '13016',
            'name' => 'Delivery receipt'
        ),
        'Order Closed' => array(
            'code' => '13999',
            'name' => 'Other'
        ),
        'Order Saved' => array(
            'code' => '13999',
            'name' => 'Other'
        ),
        'Drawer Opened' => array(
            'code' => '13005',
            'name' => 'Open cash drawer'
        ),
        'Order Sent to Kitchen' => array(
            'code' => '13999',
            'name' => 'Other'
        ),
        'Server Clocked In' => array(
            'code' => '13001',
            'name' => 'Employee log in'
        ),
        'Payment Voided' => array(
            'code' => '13010',
            'name' => 'Suspend transaction'
        ),
        'Refund Applied' => array(
            'code' => '13999',
            'name' => 'Other'
        ),
        'Refund Voided' => array(
            'code' => '13999',
            'name' => 'Other'
        ),
        'Item Partially Voided' => array(
            'code' => '13999',
            'name' => 'Other'
        ),
        'Order Reclosed' => array(
            'code' => '13999',
            'name' => 'Other'
        ),
        'Item Voided' => array(
            'code' => '13999',
            'name' => 'Other'
        ),
        'Order Reopened' => array(
            'code' => '13999',
            'name' => 'Other'
        ),
         'Item changed after item sent - Revised details'=>array('code'=>'13999','name'=>'Other'),
         'Item quantity decreased after check printed'=>array('code'=>'13999','name'=>'Other'),
         'Item removed after check printed'=>array('code'=>'13999','name'=>'Other'),
         'Item changed after item sent - Item removed'=>array('code'=>'13999','name'=>'Other'),
        'Item added after check printed' => array(
            'code' => '13999',
            'name' => 'Other'
        ),
         'Item changed after item saved - Revised details'=>array('code'=>'13999','name'=>'Other'),
        'Guest count changed' => array(
            'code' => '13999',
            'name' => 'Other'
        ),
        'Merged with Order' => array(
            'code' => '13999',
            'name' => 'Other'
        ),
        'Table name changed' => array(
            'code' => '13999',
            'name' => 'Other'
        ),
        'Z Report' => array(
            'code' => '13009',
            'name' => 'ZREP'
        ),
        'X Report' => array(
            'code' => '13008',
            'name' => 'XREP'
        )
    );
    // END OF EVENTS
    
    $dateCondition = " ( o.closed BETWEEN '" . $norwayCashRegisterObj->startDate . " " . $norwayCashRegisterObj->startDayTime . "' AND '" . $norwayCashRegisterObj->endDate . " " .
         $norwayCashRegisterObj->endDayTime . "'";
    $dateCondition .= " OR o.reclosed_datetime BETWEEN '" . $norwayCashRegisterObj->startDate . " " . $norwayCashRegisterObj->startDayTime . "' AND '" . $norwayCashRegisterObj->endDate . " " .
         $norwayCashRegisterObj->endDayTime . "'";
    $dateCondition .= " OR o.reopened_datetime BETWEEN '" . $norwayCashRegisterObj->startDate . " " . $norwayCashRegisterObj->startDayTime . "' AND '" . $norwayCashRegisterObj->endDate . " " .
         $norwayCashRegisterObj->endDayTime . "' )";

    $timeCondition = " time BETWEEN '" . $norwayCashRegisterObj->startDate . " " . $norwayCashRegisterObj->startDayTime . "' AND '" . $norwayCashRegisterObj->endDate . " " .
         $norwayCashRegisterObj->endDayTime . "'";
    
    if ($createReportPDF) {
        // die('here---');
        $dateCondition = " o.last_mod_register_name='" . $register_name . "' ";
        $timeCondition = " 1=1 ";
    	
        #changes for x-report generation changes
        if($mode == 'x')
        {
        	$lastReportDateTimeStr = strtotime($detailsForNewReport['lastReportDateTime']);
        	$lastReportDateTime = date('Y-m-d H:i:s',$lastReportDateTimeStr);
        	//$datelast = DATE_FORMAT($newformat,'%y-%m-%d') ;
        	$dateCondition .= " AND (o.closed > '" . $lastReportDateTime . "' ";
        	$dateCondition .= " OR o.reclosed_datetime > '" . $lastReportDateTime . "' ";
        	$dateCondition .= " OR o.reopened_datetime > '" . $lastReportDateTime . "') ";
        }
        else{
	        if ($detailsForNewReport['lastReportDateTime'] != '') {
	            $dateCondition .= " AND (o.closed > '" . $detailsForNewReport['lastReportDateTime'] . "' ";
	            $dateCondition .= " OR o.reclosed_datetime > '" . $detailsForNewReport['lastReportDateTime'] . "' ";
	            $dateCondition .= " OR o.reopened_datetime > '" . $detailsForNewReport['lastReportDateTime'] . "') ";
	        }
	    
	        if ($detailsForNewReport['lastReportDateTime'] != '') {
	            $timeCondition .= " AND time > '" . $detailsForNewReport['lastReportDateTime'] . "' ";
	        }
        }
    }
    
    // build arrays
    $cashRegistersClosedOrdersQuery = lavu_query(
                                                    "SELECT
                                                        o.id,
                                                    	o.order_id,
                                                        o.order_status,
                                                    	o.server_id,
                                                    	o.cashier_id,
                                                    	o.last_mod_register_name,
                                                        o.email,
                                                    	o.check_has_printed,
                                                    	o.rounding_amount,
                                                    	o.discount,
                                                        o.discount_value,
                                                    	o.total,
                                                    	o.subtotal,
                                                        o.tax,
                                                    	cct.action,
                                                    	IF(cct.action = 'Sale', 'C', 'D') as amounttype,
                                                    	o.closed,
                                                    	o.signature,
                                                    	o.key_version,
                                                    	cct.pay_type,
                                                    	cct.pay_type_id,
                                                    	cct.total_collected,
                                                        cct.tip_amount,
                                                        o.card_gratuity,
                                                        o.cash_tip,
                                                        o.other_tip,
                                                        o.location_id,
                                                        o.void_reason
                                                    FROM
                                                    	orders o 
                                                        LEFT JOIN
                                                    		cc_transactions cct 
                                                            ON cct.order_id = o.order_id AND cct.action != 'CashTips'
                                                    WHERE
                                                        o.order_status IN
                                                    	(
                                                    		'closed',
                                                            'reclosed',
                                                            'reopened',
                                                            'voided'
                                                        )
                                                        AND o.last_mod_device != 'MANAGE TIPS'
                                                        AND " . $dateCondition . "
                                                        AND o.signature != ''
                                                    ORDER BY
                                                        o.closed,
                                                        o.last_mod_register_name ASC
                                                    ");
    
    $allRegisters = array();
    $allOrders = array();
    
    // $cashRegistersReportDetails = array('totalCashSaleAmnt'=>'', 'reportPayments'=>array(), 'reportTip'=>'','reportCashSalesVAT'=>array());
    if ($createReportPDF) {
        $cashRegistersReportDetails = array();
    }
    $orderStr="";
    if (mysqli_num_rows($cashRegistersClosedOrdersQuery) > 0) {
        while ($info = mysqli_fetch_assoc($cashRegistersClosedOrdersQuery)) {
    
            $allOrders[$info['order_id']] = $info;
            $closedDate = explode(" ", $info['closed']);
            $allOrders[$info['order_id']]['closed_date'] = $closedDate[0];
            $allOrders[$info['order_id']]['closed_time'] = $closedDate[1];
    
            $allRegisters[] = $info['last_mod_register_name'];
            $cashRegisterOrderIds[$info['last_mod_register_name']]['order_ids'][] = $info['order_id'];
            $allOrderIds[] = $info['order_id'];
    
            $registerUsers[$info['last_mod_register_name']][$info['order_id']] = $info['server_id'];
            $registerUsers[$info['last_mod_register_name']][$info['order_id']] = $info['cashier_id'];
    
            $closedDayTime = explode(" ", $info['closed']);
            $lastOrderAutoIncrementIDValsArray[] = $info['id'];
    
            if ($createReportPDF) {
    
                /*
                 * X/Z-REPORT start
                 *
                 */
    
                // Get ReportCashSales
                if (!isset($cashRegistersReportDetails[$info['last_mod_register_name']]['totalCashSaleAmnt'])) {
                    $cashRegistersReportDetails[$info['last_mod_register_name']]['totalCashSaleAmnt'] = 0.00;
                }
    
                if ($info['pay_type'] == 'Cash' && $info['amounttype'] == 'C') {
                    $cashRegistersReportDetails[$info['last_mod_register_name']]['totalCashSaleAmnt'] += $info['total_collected'];
                }
                // End
    
                // Get payment type transactions on individual day
                if (!isset($cashRegistersReportDetails[$info['last_mod_register_name']]['reportPayments'][$info['pay_type']])) {
                    $cashRegistersReportDetails[$info['last_mod_register_name']]['reportPayments'][$info['pay_type']]['paymentAmnt'] = 0.00;
                    $cashRegistersReportDetails[$info['last_mod_register_name']]['reportPayments'][$info['pay_type']]['paymentNum'] = 1;
                } else {
                    $cashRegistersReportDetails[$info['last_mod_register_name']]['reportPayments'][$info['pay_type']]['paymentNum'] += 1;
                }
    
                $cashRegistersReportDetails[$info['last_mod_register_name']]['reportPayments'][$info['pay_type']]['paymentAmnt'] += $info['total_collected'];
                // End
    
                // reportTip
                if (!isset($cashRegistersReportDetails[$info['last_mod_register_name']]['reportTips']['tipAmnt'])) {
                    $cashRegistersReportDetails[$info['last_mod_register_name']]['reportTips']['tipAmnt'] = 0.00;
                    $cashRegistersReportDetails[$info['last_mod_register_name']]['reportTips']['tipNum'] = 0;
                }
    
                if ($info['card_gratuity'] != '' || $info['cash_tip'] != '' || $info['other_tip'] != '') {
                    $cashRegistersReportDetails[$info['last_mod_register_name']]['reportTips']['tipNum'] += 1;
                    $cashRegistersReportDetails[$info['last_mod_register_name']]['reportTips']['tipAmnt'] += $info['card_gratuity'] + $info['cash_tip'] + $info['other_tip'];
                }
    
                if (!isset($cashRegistersReportDetails[$info['last_mod_register_name']]['reportReceiptNum'])) {
                    $cashRegistersReportDetails[$info['last_mod_register_name']]['reportReceiptNum'] = 0;
    
                    $cashRegistersReportDetails[$info['last_mod_register_name']]['reportReceiptProformaAmnt'] = 0.00;
    
                    $cashRegistersReportDetails[$info['last_mod_register_name']]['reportReceiptCopyNum'] = 0;
                    $cashRegistersReportDetails[$info['last_mod_register_name']]['reportReceiptCopyAmnt'] = 0.00;
                }
    
                /*if ($info['pay_type'] == 'Cash' && $info['amounttype'] == 'C') {
                    if ($info['check_has_printed'] == 1 || $info['email'] != '') {
                        $cashRegistersReportDetails[$info['last_mod_register_name']]['reportReceiptNum'] += 1;
                        $cashRegistersReportDetails[$info['last_mod_register_name']]['reportReceiptProformaAmnt'] += $info['total_collected'];
                    }
                }*/
    
                if (!isset($cashRegistersReportDetails[$info['last_mod_register_name']]['reportReturnNum'])) {
                    $cashRegistersReportDetails[$info['last_mod_register_name']]['reportReturnNum'] = 0;
                    $cashRegistersReportDetails[$info['last_mod_register_name']]['reportReturnAmnt'] = 0.00;
                }
    
                if ($info['pay_type'] == 'Cash' && $info['action'] == 'Refund') {
                    $cashRegistersReportDetails[$info['last_mod_register_name']]['reportReturnNum'] += 1;
                    $cashRegistersReportDetails[$info['last_mod_register_name']]['reportReturnAmnt'] += $info['total_collected'];
                }
    
                if (!isset($cashRegistersReportDetails[$info['last_mod_register_name']]['reportDiscountNum'])) {
                    $cashRegistersReportDetails[$info['last_mod_register_name']]['reportDiscountNum'] = 0;
                    $cashRegistersReportDetails[$info['last_mod_register_name']]['reportDiscountAmnt'] = 0.00;
                }
    
                if ($info['discount'] > 0) {
                    $cashRegistersReportDetails[$info['last_mod_register_name']]['reportDiscountNum'] += 1;
                    $cashRegistersReportDetails[$info['last_mod_register_name']]['reportDiscountAmnt'] += $info['discount'];
                }
    
                // get reportGrandTotalSales
                if (!isset($cashRegistersReportDetails[$info['last_mod_register_name']]['reportGrandTotalSales'])) {
                    $cashRegistersReportDetails[$info['last_mod_register_name']]['reportGrandTotalSales'] = 0.00;
                }
    
                $cashRegistersReportDetails[$info['last_mod_register_name']]['reportGrandTotalSales'] += $info['total_collected'];
                // end
    
                // get reportGrandTotalReturn
                if (!isset($cashRegistersReportDetails[$info['last_mod_register_name']]['reportGrandTotalReturn'])) {
                    $cashRegistersReportDetails[$info['last_mod_register_name']]['reportGrandTotalReturn'] = 0.00;
                }
    
                if ($info['action'] == 'Refund') {
                    $cashRegistersReportDetails[$info['last_mod_register_name']]['reportGrandTotalReturn'] += $info['total_collected'];
                }
                // end

                if (!isset($cashRegistersReportDetails[$info['last_mod_register_name']]['reportVoidTransNum'])) {
                    $cashRegistersReportDetails[$info['last_mod_register_name']]['reportVoidTransNum'] = 0;
                    $cashRegistersReportDetails[$info['last_mod_register_name']]['reportVoidTransAmnt'] = 0;
                }

                if ($info['order_status'] == 'voided' || $info['order_status'] == 'reopened') {
                    $cashRegistersReportDetails[$info['last_mod_register_name']]['reportVoidTransNum'] += 1;
                    $cashRegistersReportDetails[$info['last_mod_register_name']]['reportVoidTransAmnt'] += $info['total'];
                }
    
                /*
                 * End of X/Z-REPORT
                 *
                 */
                
                $orderStr .="'".$info['order_id']."',";
            }
        }
    }
    
    if($orderStr!=""){
        $orderStr=rtrim($orderStr,',');
        if(ucfirst($mode)=='Z'){
            @$update_cash_tips = lavu_query("UPDATE `orders` SET `zreport_status` = 1 WHERE `order_id` in ( ".$orderStr.")  ");
        }
    }
    
    if (isset($allRegisters)) {
        $allRegisters = array_unique($allRegisters);
    }
    
    if ($createReportPDF) {
        $allRegisters = array(
            $register_name
        );
    }
    
    // for split check details add to the signature  for resolve duplicate signeture for transaction START
    $splitcheckdetails = lavu_query("SELECT  o.id,o.order_id,scd.check from orders o 
    		LEFT JOIN split_check_details scd ON o.order_id = scd.order_id 
    		WHERE o.order_status IN ('closed','reclosed','reopened','voided') 
    		AND o.last_mod_device != 'MANAGE TIPS' 
    		AND o.signature != '' ORDER BY o.closed, o.last_mod_register_name ASC ");
    // for split check START
    
    if (mysqli_num_rows($splitcheckdetails) > 0) {
    	while ($splitinfo = mysqli_fetch_assoc($splitcheckdetails)) {
    		$allOrders[$splitinfo['order_id']]['check'][]=$splitinfo['check'];
    	}
    }
    //for split check details add to the signature  for resolve duplicate signeture for transaction END
    
    // echo '<pre>'; print_r($cashRegistersReportDetails); die;
    // echo '<pre>'; print_r($cashRegisterOrderIds);die;
    // echo '<pre>'; print_r($allOrders);die;
    // echo '<pre>'; print_r($registerUsers); die;
    
    // get orders Copy Receipt  data for report
    if ($createReportPDF && isset($allOrderIds)) {
        $cashRegistersOrdersReceiptCopyQuery = lavu_query(
                                                    "SELECT
                                                        o.last_mod_register_name,
                                                        scd.check_total,
                                                        scd.email,
                                                        scd.check_has_printed
                                                    FROM
                                                        orders o
                                                        LEFT JOIN
                                                            cc_transactions cct
                                                            ON cct.order_id = o.order_id
                                                        LEFT JOIN
                                                            split_check_details scd
                                                            ON scd.order_id = o.order_id
                                                    WHERE
                                                        o.order_id IN ('" . implode("','", $allOrderIds) . "')
                                                        AND cct.action = 'Sale'
                                                        AND cct.pay_type = 'Cash'
                                                    GROUP BY
                                                        scd.id
                                                    ORDER BY
                                                        o.closed,
                                                        o.last_mod_register_name ASC");
    
        if (mysqli_num_rows($cashRegistersOrdersReceiptCopyQuery) > 0) {
            while ($info = mysqli_fetch_assoc($cashRegistersOrdersReceiptCopyQuery)) {
                if ($info['email'] != '' || $info['check_has_printed'] == 1 || $info['check_has_printed'] == '1') {
                    $cashRegistersReportDetails[$info['last_mod_register_name']]['reportReceiptNum'] += 1;
                }

                if ($info['email'] != '' && ($info['check_has_printed'] == 1 || $info['check_has_printed'] == '1')) {
                    $cashRegistersReportDetails[$info['last_mod_register_name']]['reportReceiptCopyNum'] += 1;
                    $cashRegistersReportDetails[$info['last_mod_register_name']]['reportReceiptCopyAmnt'] += $info['check_total'];
                }
            }
        }
    }
    // end
    
    if ($createReportPDF && !isset($allOrderIds)) {
        echo "1|ALERT|".speak('No orders found to generate the'). " ".strtoupper($mode)." ".speak('Report')."";
        lavu_exit();
    } else if(!isset($allOrderIds)) {
        echo '<script>alert("'.speak("There are no orders in the selected duration! Hence not generating the Cash Register XML File").'"); window.history.back(); </script>';
        lavu_exit();
    }
    
    // get all order line items
    $orderLineItems = array();
    $vatDetailsArray = array();
    $menuDetailsArray = array();
    
    $registerOrderLineItems = lavu_query(
                                          "SELECT
                                        	  o.id,
                                        	  order_id,
                                        	  item_id,
                                        	  item,
                                        	  price,
                                        	  quantity,
                                        	  discount_type,
                                              subtotal,
                                        	  o.category_id,
                                        	  subtotal_with_mods,
                                              tax_amount,
                                              tax1,
                                              tax_rate1,
                                        	  tax_name1,
                                              tax_subtotal1,
                                              discount_amount,
                                              total_with_tax,
                                              after_discount,
                                        	  title,
                                        	  tax_name1 
                                          FROM
                                        	  order_contents as o 
                                        	  LEFT JOIN
                                        		  discount_types as d 
                                        		  ON o.discount_id = d.id 
                                          WHERE
                                                o.order_id IN ('" . implode("','", $allOrderIds) . "')");
    
    if (mysqli_num_rows($registerOrderLineItems) > 0) {
        while ($data = mysqli_fetch_assoc($registerOrderLineItems)) {
            $orderLineItems[$data['order_id']][] = $data;
            if ($data['tax_name1'] != "") {
                $vatDetailsArray[$data['tax_name1']][] = explode(" ", $allOrders[$data['order_id']]['closed'])[0];
            }
            $menuDetailsArray[$data['item_id']][] = explode(" ", $allOrders[$data['order_id']]['closed'])[0];
    
            $closedDayTime = explode(" ", $allOrders[$data['order_id']]['closed']);
    
            if ($createReportPDF) {
    
                $cashRegistersReportDetails[$allOrders[$data['order_id']]['last_mod_register_name']]['reportCashSalesVat'][$data['tax_name1']]['vatPerc'] = $data['tax_rate1'];
    
                if (! isset($cashRegistersReportDetails[$allOrders[$data['order_id']]['last_mod_register_name']]['reportCashSalesVat'][$data['tax_name1']]['cashSaleAmnt'])) {
                    $cashRegistersReportDetails[$allOrders[$data['order_id']]['last_mod_register_name']]['reportCashSalesVat'][$data['tax_name1']]['cashSaleAmnt'] = 0;
                }
    
                $cashRegistersReportDetails[$allOrders[$data['order_id']]['last_mod_register_name']]['reportCashSalesVat'][$data['tax_name1']]['cashSaleAmnt'] += $data['tax_subtotal1'];
    
                if (! isset($cashRegistersReportDetails[$allOrders[$data['order_id']]['last_mod_register_name']]['reportCashSalesVat'][$data['tax_name1']]['vatAmnt'])) {
                    $cashRegistersReportDetails[$allOrders[$data['order_id']]['last_mod_register_name']]['reportCashSalesVat'][$data['tax_name1']]['vatAmnt'] = 0;
                }
    
                $cashRegistersReportDetails[$allOrders[$data['order_id']]['last_mod_register_name']]['reportCashSalesVat'][$data['tax_name1']]['vatAmnt'] += $data['tax1'];
    
                // reportArtGroups
                if (! isset($cashRegistersReportDetails[$allOrders[$data['order_id']]['last_mod_register_name']]['reportArtGroups'][$data['category_id']]['artGroupAmnt'])) {
                    $cashRegistersReportDetails[$allOrders[$data['order_id']]['last_mod_register_name']]['reportArtGroups'][$data['category_id']]['artGroupAmnt'] = 0;
                    $cashRegistersReportDetails[$allOrders[$data['order_id']]['last_mod_register_name']]['reportArtGroups'][$data['category_id']]['artGroupNum'] = 1;
                } else {
                    $cashRegistersReportDetails[$allOrders[$data['order_id']]['last_mod_register_name']]['reportArtGroups'][$data['category_id']]['artGroupNum'] += 1;
                }
                $cashRegistersReportDetails[$allOrders[$data['order_id']]['last_mod_register_name']]['reportArtGroups'][$data['category_id']]['artGroupAmnt'] += $data['tax_subtotal1'];
    
                // reportPriceInquires
                if (! isset(
                    $cashRegistersReportDetails[$allOrders[$data['order_id']]['last_mod_register_name']]['reportPriceInquiries'][$data['category_id']]['priceInquiryNum'])) {
                    $cashRegistersReportDetails[$allOrders[$data['order_id']]['last_mod_register_name']]['reportPriceInquiries'][$data['category_id']]['priceInquiryNum'] = 0;
                    $cashRegistersReportDetails[$allOrders[$data['order_id']]['last_mod_register_name']]['reportPriceInquiries'][$data['category_id']]['priceInquiryAmnt'] = 0;
                }
                $cashRegistersReportDetails[$allOrders[$data['order_id']]['last_mod_register_name']]['reportPriceInquiries'][$data['category_id']]['priceInquiryNum'] += 1;
                $cashRegistersReportDetails[$allOrders[$data['order_id']]['last_mod_register_name']]['reportPriceInquiries'][$data['category_id']]['priceInquiryAmnt'] += $data['subtotal'];
            }
        }
    }
    
    // echo '<pre>'; print_r($cashRegistersReportDetails); die;
    // echo '<pre>'; print_r($vatDetailsArray); die;
    // echo '<pre>'; print_r($orderLineItems); die;
    
    $orderTransactions = array();
    $registerOrderCCtransactions = lavu_query(
                                                "SELECT
                                                	order_id,
                                                	pay_type_id,
                                                	pay_type,
                                                	total_collected 
                                                FROM
                                                	cc_transactions WHERE order_id IN ('" . implode("','", $allOrderIds) . "')");
    if (mysqli_num_rows($registerOrderCCtransactions) > 0) {
        while ($data = mysqli_fetch_assoc($registerOrderCCtransactions)) {
            $orderTransactions[$data['order_id']][] = $data;
        }
    }
    
    $registerUserEvents = array();
    $registerUserEventsQuery = lavu_query(
                                            "SELECT id, action, time,order_id, signature, key_version
                                            FROM
                                            	action_log 
                                            WHERE
                                            	order_id IN ('" . implode("','", $allOrderIds) . "')
                                            	AND action IN ('" . implode("','", array_keys($events)) . "')
                                                AND " . $timeCondition . "
                                            ORDER BY
                                            	time ASC");
    
    if (mysqli_num_rows($registerUserEventsQuery) > 0) {
        while ($data = mysqli_fetch_assoc($registerUserEventsQuery)) {
            $dateTime = explode(" ", $data['time']);
            $data['date'] = $dateTime[0];
            $data['time'] = $dateTime[1];
            $registerUserEvents[$data['order_id']][] = $data;
    
            if ($data['action'] == 'Drawer Opened' && $createReportPDF) {
                // die('---');
                if (! isset($cashRegistersReportDetails[$allOrders[$data['order_id']]['last_mod_register_name']]['reportOpenCashBoxNum'])) {
                    // $allOrders[$info['order_id']]['last_mod_register_name']
                    $cashRegistersReportDetails[$allOrders[$data['order_id']]['last_mod_register_name']]['reportOpenCashBoxNum'] = 0;
                }
    
                $cashRegistersReportDetails[$allOrders[$data['order_id']]['last_mod_register_name']]['reportOpenCashBoxNum'] += 1;
            }
        }
    }
    // echo '<pre>'; print_r($cashRegistersReportDetails); die();
    // end building arrays
    
    /*
     *
     * Build basic Tags
     *
     */
    // $basics = array('menuItems' => array(),'vatCodeDetails'=>array(),'');
    
    // Building Basics Tags
    $basicsTagXmlStr = '<n1:basics>';
    
    $vatCodeDetails = array();
    $tempStr = '';
    $vatCodesArrayXmlStr = '';
    
    $taxProfilesQuery = lavu_query(
                                    "SELECT
                                    	title,
                                    	norway_vat_code 
                                    FROM
                                    	tax_rates 
                                    WHERE 
                                        title IN ('" . implode("','", array_keys($vatDetailsArray)) . "')");
    $taxProfilesArray = array();
    if (mysqli_num_rows($taxProfilesQuery) > 0) {
        while ($data = mysqli_fetch_assoc($taxProfilesQuery)) {
            $taxProfilesArray[$data['title']] = $data['norway_vat_code'];
        }
    }
    // echo '<pre>'; print_r($taxProfilesArray);die;
    foreach ($vatDetailsArray as $oneVat => $array) {
        if (isset($taxProfilesArray[$oneVat])) {
            // $vatCodeDetails[$oneVat] = $array[0];
            $tempStr .= '<n1:vatCodeDetail>';
            $tempStr .= '<n1:vatCode>' . $oneVat . '</n1:vatCode>';
            $tempStr .= '<n1:dateOfEntry>' . $vatDetailsArray[$oneVat][0] . '</n1:dateOfEntry>';
            $tempStr .= '<n1:standardVatCode>' . $taxProfilesArray[$oneVat] . '</n1:standardVatCode>';
            $tempStr .= '</n1:vatCodeDetail>';
        }
    }
    
    if ($tempStr != '') {
        $vatCodesArrayXmlStr .= '<n1:vatCodeDetails>';
        $vatCodesArrayXmlStr .= $tempStr;
        $vatCodesArrayXmlStr .= '</n1:vatCodeDetails>';
    }
    
    $menuDetails = array();
    $tempStr = '';
    $menusArrayXmlStr = '';
    
    $menuItemStandardCodesQuery = lavu_query(
                                              "SELECT
                                            	  mi.id,
                                            	  mi.name,
                                            	  norway_menu_mapping,
                                            	  mi.category_id 
                                              FROM
                                            	  menu_items mi 
                                            	  INNER JOIN
                                            		  menu_categories mc 
                                            		  ON mi.category_id = mc.id 
                                              WHERE
                                                mi.id IN ('" . implode("','", array_keys($menuDetailsArray)) . "')");
    
    $menuItemStandardCodesArray = array();
    if (mysqli_num_rows($menuItemStandardCodesQuery) > 0) {
        while ($data = mysqli_fetch_assoc($menuItemStandardCodesQuery)) {
            $menuItemStandardCodesArray[$data['id']] = array(
                "mapping" => $data['norway_menu_mapping'],
                "name" => $data['name'],
                "category_id" => $data['category_id']
            );
        }
    }
    
    // echo '<pre>'; print_r($menuDetailsArray);die;
    foreach ($menuItemStandardCodesArray as $key => $oneMenu) {
        // $vatCodeDetails[$oneVat] = $array[0];
        $tempStr .= '<n1:article>';
        $tempStr .= '<n1:artID>' . $key . '</n1:artID>';
        $tempStr .= '<n1:dateOfEntry>' . $menuDetailsArray[$key][0] . '</n1:dateOfEntry>';
        $tempStr .= '<n1:artGroupID>' . $oneMenu['category_id'] . '</n1:artGroupID>';
        $tempStr .= '<n1:artDesc>' . $oneMenu['name'] . '</n1:artDesc>';
        $tempStr .= '</n1:article>';
    
        $basicsTagXmlStr .= '<n1:basic>';
        $basicsTagXmlStr .= '<n1:basicType>04</n1:basicType>';
        $basicsTagXmlStr .= '<n1:basicID>' . $oneMenu['category_id'] . '</n1:basicID>';
        $basicsTagXmlStr .= '<n1:predefinedBasicID>' . $oneMenu['mapping'] . '</n1:predefinedBasicID>';
        $basicsTagXmlStr .= '<n1:basicDesc>' . $oneMenu['name'] . '</n1:basicDesc>';
        $basicsTagXmlStr .= '</n1:basic>';
    }
    
    if ($tempStr != '') {
        $menusArrayXmlStr .= '<n1:articles>';
        $menusArrayXmlStr .= $tempStr;
        $menusArrayXmlStr .= '</n1:articles>';
    }
    
    // discnount
    
    foreach ($events as $key => $array) {
        $basicsTagXmlStr .= '<n1:basic>';
        $basicsTagXmlStr .= '<n1:basicType>13</n1:basicType>';
        $basicsTagXmlStr .= '<n1:basicID>' . $array['name'] . '</n1:basicID>';
        $basicsTagXmlStr .= '<n1:predefinedBasicID>' . $array['code'] . '</n1:predefinedBasicID>';
        $basicsTagXmlStr .= '<n1:basicDesc>' . $key . '</n1:basicDesc>';
        $basicsTagXmlStr .= '</n1:basic>';
    }
    
    // raise codes for card gratuity/cash tip and other tip
    /*
     * $basicsTagXmlStr .= '<n1:basic>';
     * $basicsTagXmlStr .= '<n1:basicType>10</n1:basicType>';
     * $basicsTagXmlStr .= '<n1:basicID>Amount from gratuity/tip</n1:basicID>';
     * $basicsTagXmlStr .= '<n1:predefinedBasicID>10001</n1:predefinedBasicID>';
     * $basicsTagXmlStr .= '<n1:basicDesc>Card Gratuity/Cash Tip</n1:basicDesc>';
     * $basicsTagXmlStr .= '</n1:basic>';
     *
     * $basicsTagXmlStr .= '<n1:basic>';
     * $basicsTagXmlStr .= '<n1:basicType>10</n1:basicType>';
     * $basicsTagXmlStr .= '<n1:basicID>Other</n1:basicID>';
     * $basicsTagXmlStr .= '<n1:predefinedBasicID>10999</n1:predefinedBasicID>';
     * $basicsTagXmlStr .= '<n1:basicDesc>Other Tip</n1:basicDesc>';
     * $basicsTagXmlStr .= '</n1:basic>';
     */
    
    $basicsTagXmlStr .= '</n1:basics>';
    // END
    
    // get reportchangefloats of register for zreport/xreport
    if ($createReportPDF) {
        $openingChangeFloat = 0.00;
        if ($detailsForNewReport['lastReportDateTime'] != "") {
            $condition = " AND CONCAT(date,' ',value2) > '".$detailsForNewReport['lastReportDateTime']."' ";
        } else {
            $condition = "";
        }
        $reportChangeFloatQuery = lavu_query(
                                              "SELECT
                                                    `setting`,
                                                    `value`
                                              FROM
                                                cash_data
                                                WHERE setting LIKE '%_register:" . $register_name . "'
                                              " . $condition . " AND _deleted=0 ORDER BY id ASC");
    
        if (mysqli_num_rows($reportChangeFloatQuery) > 0) {
            while ($data = mysqli_fetch_assoc($reportChangeFloatQuery)) {
                $openingChangeFloat += explode(":", $data['value'])[0];
            }
        }
    }
    // end
    
    // echo '<pre>';
    // echo($vatCodesArrayXmlStr);
    // die($vatCodesArrayXmlStr);
    /*
     *
     * END
     */
    
    //Register ID details
    $printerStr = "";
    $registerInfo = array();
    foreach($allRegisters as $printer){
    	if($printerStr!=""){
    	    $printerStr .=", '".$printer."'"; 
    	}else{
    	    $printerStr .= "'".$printer."'";
    	}
    }
    
    $printerInfoQuery = lavu_query("SELECT `id`, `value2` FROM config WHERE type='printer' AND value2 in (".$printerStr.") AND _deleted=0 ORDER BY id ASC" );
    
    if (mysqli_num_rows($printerInfoQuery) > 0) {
        while ($data = mysqli_fetch_assoc($printerInfoQuery)) {
            $registerInfo[$data['value2']]=$data['id'];
        }
    }
    
    $xmlStr = '<?xml version="1.0" encoding="UTF-8"?>';
    $xmlStr .= '<n1:auditfile xmlns:n1="urn:StandardAuditFile-Taxation-CashRegister:NO" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="urn:StandardAuditFile-Taxation-CashRegister:NO Norwegian_SAF-T_Cash_Register_Schema_v_1.00.xsd">';
    // $xmlStr .= '<n1:auditfile>';
    
    $xmlStr .= '<n1:header>
                  <n1:fiscalYear>' . $norwayCashRegisterObj->fiscalYear . '</n1:fiscalYear>
                  <n1:startDate>' . $norwayCashRegisterObj->startDate . '</n1:startDate>
                  <n1:endDate>' . $norwayCashRegisterObj->endDate . '</n1:endDate>
                  <n1:curCode>' . sessvar('location_info')['primary_currency_code'] . '</n1:curCode>
                  <n1:dateCreated>' . $norwayCashRegisterObj->currentDate . '</n1:dateCreated>
                  <n1:timeCreated>' . $norwayCashRegisterObj->currentTime . '</n1:timeCreated>
                  <n1:softwareDesc>POSLavu Admin Control Panel</n1:softwareDesc>
                  <n1:softwareVersion>' . admin_info('version') . '</n1:softwareVersion>
                  <n1:softwareCompanyName>Lavu Inc.</n1:softwareCompanyName>
                  <n1:auditfileVersion>1.0</n1:auditfileVersion>
                 </n1:header>';
    
    $xmlStr .= '<n1:company>
                  <n1:companyIdent>' . $norwayCashRegisterObj->fiscalYear . '</n1:companyIdent>
    		      <n1:companyName>' . admin_info('company_name') . '</n1:companyName>';
    
    $xmlStr .= $vatCodesArrayXmlStr;
    
    $xmlStr .= $menusArrayXmlStr;
    
    $xmlStr .= $basicsTagXmlStr;
    
    $xmlStr .= '<n1:location>
                   <n1:name>' . sessvar('location_info')['title'] . '</n1:name>';
    
    foreach ($allRegisters as $register) {
        $xmlStr .= '<n1:cashregister>
                    <n1:registerID>' . $register . ' </n1:registerID>';
    
        $tempStr = '';
        if (isset($registerUsers[$register])) {
            foreach ($registerUsers[$register] as $key => $userId) {
                if (isset($registerUserEvents[$key])) {
                    foreach ($registerUserEvents[$key] as $one) {
                        $tempStr .= '<n1:event>';
                        $tempStr .= '<n1:eventID>' . $one['id'] . '</n1:eventID>';
                        $tempStr .= '<n1:eventType>' . $one['action'] . '</n1:eventType>';
                        $tempStr .= '<n1:eventDate>' . $one['date'] . '</n1:eventDate>';
                        $tempStr .= '<n1:eventTime>' . $one['time'] . '</n1:eventTime>';
                        if ($one['signature'] !=''){
                        	$tempStr .= '<n1:eventSignature>' . $one['signature'] . '</n1:eventSignature>';
                        	$tempStr .= '<n1:eventKeyVersion>' . $one['key_version'] . '</n1:eventKeyVersion>';
                        }
                        $tempStr .= '</n1:event>';
                    }
                }
            }
    
            $xmlStr .= $tempStr;
        }
    
        // start of zreport event
        // echo '<pre>'; print_r($cashRegistersReportDetails);die($register);
        if ($createReportPDF) {
            $tempStr = '';
            $printStr = reqvar('register', 'receipt').":";
            foreach ($cashRegistersReportDetails as $detailsArray) {
                $tempStr .= '<n1:event>';
                $tempStr .= '<n1:eventID>' . ucfirst($mode) .
                     ((isset($last_inserted_admin_action_log_id) && ! empty($last_inserted_admin_action_log_id)) ? $last_inserted_admin_action_log_id : $detailsForNewReport['nextSequenceNumber']) .
                     '</n1:eventID>';
                $tempStr .= '<n1:eventType>' . ucfirst($mode) . 'REP</n1:eventType>';
                $tempStr .= '<n1:eventDate>' . localize_date(date("Y-m-d"), $location_info['timezone']) . '</n1:eventDate>';
                $tempStr .= '<n1:eventTime>' . localize_time(date("Y-m-d H:i:s"), $location_info['timezone']) . '</n1:eventTime>';
    
                $tempStr .= '<n1:eventReport>';
                $tempStr .= '<n1:reportID>' . $detailsForNewReport['nextSequenceNumber'] . '</n1:reportID>';
                $tempStr .= '<n1:reportType>' . ucfirst($mode) . ' report</n1:reportType>';
                $tempStr .= '<n1:companyIdent>' . $norwayCashRegisterObj->organizationNumber . '</n1:companyIdent>';
                $tempStr .= '<n1:companyName>' . $company_info['company_name'] . '</n1:companyName>';
                $tempStr .= '<n1:reportDate>' . localize_date(date("Y-m-d H:i:s"), $location_info['timezone']) . '</n1:reportDate>';
                $tempStr .= '<n1:reportTime>' . localize_time(date("Y-m-d H:i:s"), $location_info['timezone']) . '</n1:reportTime>';
    
                $tempStr .= '<n1:registerID>' . $register . '</n1:registerID>';
    
                $printStr .= "---------------------------:";
               
                
                $printStr .= "     ".ucfirst($mode)." Report:";
                $printStr .= " :";
                
                if(ucfirst($mode)=='Z'){
                    $printStr .= "Report ID. ".$detailsForNewReport['nextSequenceNumber']." :";
                }
                
                $business_date2 = date("Y-n-j", $use_ts);
                $business_time = date("g:i a", $nowts);
               
                $printStr .= $business_date." :";
                //$printStr .= "Report Date ".$business_date2." :";
                //$printStr .= "Report Time ".$business_time." :";
                $printStr .= "Org Num - ".$norwayCashRegisterObj->organizationNumber." :";
                $printStr .= "Org Name - ".$company_info['company_name']." :";
                $printStr .= "Register Name - ".$register." :";
                $printStr .= "Register ID - ".$registerInfo[$register]." :";
                $printStr .= "(Printed - ".str_replace(":", "[c", $print_datetime).") :";
                $printStr .= "Register[c ".reqvar('register', 'receipt')." ::";
                $printStr .= "---------------------------:";
    
                $totalCashSaleAmnt = (isset($detailsArray['totalCashSaleAmnt']) ? str_replace(",", "", number_format(floatval($detailsArray['totalCashSaleAmnt']), 2)) : "0.00");
    
                $tempStr .= '<n1:reportTotalCashSales><n1:totalCashSaleAmnt>' 
                    .$totalCashSaleAmnt.
                     '</n1:totalCashSaleAmnt></n1:reportTotalCashSales>';
                $printStr .= "Total Cash Sales [c ".$totalCashSaleAmnt.":";
    
                if (isset($detailsArray['reportArtGroups'])) {
                    $tempStr .= '<n1:reportArtGroups>';
                    $printStr .= ":: - - - - Menu Categories - - - - :";
                    foreach ($detailsArray['reportArtGroups'] as $key => $val) {
                        $tempStr .= '<n1:reportArtGroup>';
                        $tempStr .= '<n1:artGroupID>' . $key . '</n1:artGroupID>';
                        $tempStr .= '<n1:artGroupNum>' . $val['artGroupNum'] . '</n1:artGroupNum>';
                        $tempStr .= '<n1:artGroupAmnt>' . str_replace(",", "", number_format(floatval($val['artGroupAmnt']), 2)) . '</n1:artGroupAmnt>';
                        $tempStr .= '</n1:reportArtGroup>';
                        $printStr .= "ID/Num/Amount [c ".$key."/".$val['artGroupNum']."/".str_replace(",", "", number_format(floatval($val['artGroupAmnt']), 2))." :";
                    }
                    $tempStr .= '</n1:reportArtGroups>';
                }
    
                if (isset($detailsArray['reportPayments'])) {
                    $tempStr .= '<n1:reportPayments>';
                    $printStr .= ":: - - - - Payments - - - - :";
                    foreach ($detailsArray['reportPayments'] as $key => $val) {
                        $tempStr .= '<n1:reportPayment>';
                        $tempStr .= '<n1:paymentType>' . $key . '</n1:paymentType>';
                        $tempStr .= '<n1:paymentNum>' . $val['paymentNum'] . '</n1:paymentNum>';
                        $tempStr .= '<n1:paymentAmnt>' . str_replace(",", "", number_format(floatval($val['paymentAmnt']), 2)) . '</n1:paymentAmnt>';
                        $tempStr .= '</n1:reportPayment>';
                        if($key!=""){
                        $printStr .= "Type/Num/Amount [c ".$key."/".$val['paymentNum']."/".str_replace(",", "", number_format(floatval($val['paymentAmnt']), 2))." :";
                        }
                    } 
                    $tempStr .= '</n1:reportPayments>';
                }
    
                if (isset($detailsArray['reportTips'])) {
                    $tempStr .= '<n1:reportTip>';
                    $tempStr .= '<n1:tipNum>' . $detailsArray['reportTips']['tipNum'] . '</n1:tipNum>';
                    $tempStr .= '<n1:tipAmnt>' . str_replace(",", "", number_format(floatval($detailsArray['reportTips']['tipAmnt']), 2)) . '</n1:tipAmnt>';
                    $tempStr .= '</n1:reportTip>';
    
                    $printStr .= ":: - - - - Tips - - - - :";
                    $printStr .= "Num [c".$detailsArray['reportTips']['tipNum']." :";
                    $printStr .= "Amount [c ".str_replace(",", "", number_format(floatval($detailsArray['reportTips']['tipAmnt']), 2))." :";
                }
    
                if (isset($detailsArray['reportCashSalesVat'])) {
                    $tempStr .= '<n1:reportCashSalesVat>';
                    $printStr .= ":: - - - - Cash Sales VAT - - - - :";
                    foreach ($detailsArray['reportCashSalesVat'] as $key => $val) {
                        $tempStr .= '<n1:reportCashSaleVat>';
                        $tempStr .= '<n1:vatCode>' . $key . '</n1:vatCode>';
                        $tempStr .= '<n1:vatPerc>' . $val['vatPerc'] . '</n1:vatPerc>';
                        $tempStr .= '<n1:cashSaleAmnt>' . str_replace(",", "", number_format(floatval($val['cashSaleAmnt']), 2)) . '</n1:cashSaleAmnt>';
                        $tempStr .= '<n1:vatAmnt>' . str_replace(",", "", number_format(floatval($val['vatAmnt']), 2)) . '</n1:vatAmnt>';
                        $tempStr .= '</n1:reportCashSaleVat>';
    
                        $printStr .= "VAT Code/VAT Perc/Cash Sale Amount/VAT Amount [c ".$key."/".$val['vatPerc']."/".str_replace(",", "", number_format(floatval($val['cashSaleAmnt']), 2))."/".str_replace(",", "", number_format(floatval($val['vatAmnt']), 2)).":";
    
                    }
                    $tempStr .= '</n1:reportCashSalesVat>';
                }
    
                $printStr .= ":: - - - - Others - - - - :";
    
                $printStr .= "Opening Change Float Amount [c ".str_replace(",", "", number_format(floatval($openingChangeFloat), 2))."::";
                $printStr .= "Number of Receipts [c ".(isset($detailsArray['reportReceiptNum']) ? $detailsArray['reportReceiptNum'] : "0").":";
    
                $printStr .= ":Number of Times cash Drawer Opened [c ".(isset($detailsArray['reportOpenCashBoxNum']) ? $detailsArray['reportOpenCashBoxNum'] : "0").":";
    
                $printStr .= ":Number of Copy Receipts [c ".(isset($detailsArray['reportReceiptCopyNum']) ? $detailsArray['reportReceiptCopyNum'] : "0").":";
                $printStr .= "Total Copy Receipts Amount [c ".(isset($detailsArray['reportReceiptCopyAmnt']) ? str_replace(",", "", number_format(floatval($detailsArray['reportReceiptCopyAmnt']), 2)) : "0.00") .":";
    
                $printStr .= ":Number of Proforma Receipts  [c 0:";
                $printStr .= "Total Proforma Receipts Amount[c ".(isset($detailsArray['reportReceiptProformaAmnt']) ? str_replace(",", "", number_format(floatval($detailsArray['reportReceiptProformaAmnt']), 2)) : "0.00").":";
    
                $printStr .= ":Number of Returns  [c ".(isset($detailsArray['reportReturnNum']) ? $detailsArray['reportReturnNum'] : "0").":";
                $printStr .= "Total Returns Amount [c ".(isset($detailsArray['reportReturnAmnt']) ? str_replace(",", "", number_format(floatval($detailsArray['reportReturnAmnt']), 2)) : "0.00").":";
    
                $printStr .= ":Number of Discounts [c ".(isset($detailsArray['reportDiscountNum']) ? $detailsArray['reportDiscountNum'] : "0").":";
                $printStr .= "Total Discounts Amount [c ".(isset($detailsArray['reportDiscountAmnt']) ? str_replace(",", "", number_format(floatval($detailsArray['reportDiscountAmnt']), 2)) : "0.00").":";
    
                $printStr .= ":Number of Void Orders [c ".(isset($detailsArray['reportVoidTransNum']) ? $detailsArray['reportVoidTransNum'] : "0").":";
                $printStr .= "Total Amount of Void Orders [c ".(isset($detailsArray['reportVoidTransAmnt']) ? str_replace(",", "", number_format(floatval($detailsArray['reportVoidTransAmnt']), 2)) : "0.00").":";
    
                // echo '<pre>';print_r($detailsArray);die;
                $tempStr .= '<n1:reportOpeningChangeFloat>' . str_replace(",", "", number_format(floatval($openingChangeFloat), 2)) . '</n1:reportOpeningChangeFloat>';
                $tempStr .= '<n1:reportReceiptNum>' . (isset($detailsArray['reportReceiptNum']) ? $detailsArray['reportReceiptNum'] : "0") . '</n1:reportReceiptNum>';
                $tempStr .= '<n1:reportOpenCashBoxNum>' . (isset($detailsArray['reportOpenCashBoxNum']) ? $detailsArray['reportOpenCashBoxNum'] : "0") .
                     '</n1:reportOpenCashBoxNum>';
                $tempStr .= '<n1:reportReceiptCopyNum>' . (isset($detailsArray['reportReceiptCopyNum']) ? $detailsArray['reportReceiptCopyNum'] : "0") .
                     '</n1:reportReceiptCopyNum>';
                $tempStr .= '<n1:reportReceiptCopyAmnt>' .
                     (isset($detailsArray['reportReceiptCopyAmnt']) ? str_replace(",", "", number_format(floatval($detailsArray['reportReceiptCopyAmnt']), 2)) : "0.00") .
                     '</n1:reportReceiptCopyAmnt>';
                $tempStr .= '<n1:reportReceiptProformaNum>0</n1:reportReceiptProformaNum>';
                $tempStr .= '<n1:reportReceiptProformaAmnt>' .
                     (isset($detailsArray['reportReceiptProformaAmnt']) ? str_replace(",", "", number_format(floatval($detailsArray['reportReceiptProformaAmnt']), 2)) : "0.00") .
                     '</n1:reportReceiptProformaAmnt>';
                $tempStr .= '<n1:reportReturnNum>' . (isset($detailsArray['reportReturnNum']) ? $detailsArray['reportReturnNum'] : "0") . '</n1:reportReturnNum>';
                $tempStr .= '<n1:reportReturnAmnt>' . (isset($detailsArray['reportReturnAmnt']) ? str_replace(",", "", number_format(floatval($detailsArray['reportReturnAmnt']), 2)) : "0.00") .
                     '</n1:reportReturnAmnt>';
                $tempStr .= '<n1:reportDiscountNum>' . (isset($detailsArray['reportDiscountNum']) ? $detailsArray['reportDiscountNum'] : "0") . '</n1:reportDiscountNum>';
                $tempStr .= '<n1:reportDiscountAmnt>' .
                     (isset($detailsArray['reportDiscountAmnt']) ? str_replace(",", "", number_format(floatval($detailsArray['reportDiscountAmnt']), 2)) : "0.00") .
                     '</n1:reportDiscountAmnt>';
    
                $tempStr .= '<n1:reportVoidTransNum>' . (isset($detailsArray['reportVoidTransNum']) ? $detailsArray['reportVoidTransNum'] : "0") . '</n1:reportVoidTransNum>';
                $tempStr .= '<n1:reportVoidTransAmnt>' .
                     (isset($detailsArray['reportVoidTransAmnt']) ? str_replace(",", "", number_format(floatval($detailsArray['reportVoidTransAmnt']), 2)) : "0.00") .
                     '</n1:reportVoidTransAmnt>';
    
                $tempStr .= '<n1:reportCorrLines>';
                $tempStr .= '<n1:reportCorrLine>';
                $tempStr .= '<n1:corrLineType>None</n1:corrLineType>';
                $tempStr .= '<n1:corrLineNum>0</n1:corrLineNum>';
                $tempStr .= '<n1:corrLineAmnt>0.00</n1:corrLineAmnt>';
                $tempStr .= '</n1:reportCorrLine>';
                $tempStr .= '</n1:reportCorrLines>';
                
                $printStr .= "::";
                
                //s) number of line corrections type and amount
                $printStr .= "Line correction type [c None :";
                $printStr .= "Line correction amount [c 0.00 :";
    
                if (isset($detailsArray['reportPriceInquiries'])) {
                    $tempStr .= '<n1:reportPriceInquiries>';
                    $printStr .= ":: - - - - Menu Categoriewise Items Prices Inquiry - - - - :";
                    foreach ($detailsArray['reportPriceInquiries'] as $key => $val) {
                        $tempStr .= '<n1:reportPriceInquiry>';
                        $tempStr .= '<n1:priceInquiryGroup>' . $key . '</n1:priceInquiryGroup>';
                        $tempStr .= '<n1:priceInquiryNum>' . $val['priceInquiryNum'] . '</n1:priceInquiryNum>';
                        $tempStr .= '<n1:priceInquiryAmnt>' . str_replace(",", "", number_format(floatval($val['priceInquiryAmnt']), 2)) . '</n1:priceInquiryAmnt>';
                        $tempStr .= '</n1:reportPriceInquiry>';
                        $printStr .= "Category/Num/Amount [c".$key."/".$val['priceInquiryNum']."/".str_replace(",", "", number_format(floatval($val['priceInquiryAmnt']), 2)).":";
                    }
                    $tempStr .= '</n1:reportPriceInquiries>';
                }
                
                $printStr .= "::";
                
                if (isset($detailsArray['reportPriceInquiries'])) {
                	foreach ($detailsArray['reportPriceInquiries'] as $key => $val) {
                		//t) the number of price look-ups specified by product group and amount
                		$printStr .= "Price Inquiry Group [c ".$key.":";
                		$printStr .= "Price Inquiry Amount [c ".str_replace(",", "", number_format(floatval($val['priceInquiryAmnt']), 2)).":";
                	}
                }
    
                // right now hardcoded, need to check this in future if we can do something with this
                $tempStr .= '<n1:reportOtherCorrs>';
                $tempStr .= '<n1:reportOtherCorr>';
                $tempStr .= '<n1:otherCorrType>None</n1:otherCorrType>';
                $tempStr .= '<n1:otherCorrNum>0</n1:otherCorrNum>';
                $tempStr .= '<n1:otherCorrAmnt>0.00</n1:otherCorrAmnt>';
                $tempStr .= '</n1:reportOtherCorr>';
                $tempStr .= '</n1:reportOtherCorrs>';
                
                $printStr .= "::";
                                    
                $tempStr .= '<n1:reportReceiptDeliveryNum>0</n1:reportReceiptDeliveryNum>';
                $tempStr .= '<n1:reportReceiptDeliveryAmnt>0.00</n1:reportReceiptDeliveryAmnt>';
               
                $tempStr .= '<n1:reportTrainingNum>0</n1:reportTrainingNum>';
                $tempStr .= '<n1:reportTrainingAmnt>0.00</n1:reportTrainingAmnt>';
               
                $printStr .= ":: - - - - Grand Totals - - - - :";
    
                $tempStr .= '<n1:reportGrandTotalSales>' .
                     (isset($detailsArray['reportGrandTotalSales']) ? str_replace(",", "", number_format(floatval($detailsArray['reportGrandTotalSales']), 2)) : "0.00") .
                     '</n1:reportGrandTotalSales>';
                $tempStr .= '<n1:reportGrandTotalReturn>' .
                     (isset($detailsArray['reportGrandTotalReturn']) ? str_replace(",", "", number_format(floatval($detailsArray['reportGrandTotalReturn']), 2)) : "0.00") .
                     '</n1:reportGrandTotalReturn>';
    
                $printStr .= "Grand Total Sales [c ".str_replace(",", "", number_format(floatval(($detailsArray['reportGrandTotalSales'] - $detailsArray['reportGrandTotalReturn'])), 2)).":";
                $printStr .= "Grand Total Return [c ".str_replace(",", "", number_format(floatval(($detailsArray['reportGrandTotalReturn'] - $detailsArray['reportGrandTotalReturn'])), 2)).":";
                if (isset($detailsArray['reportGrandTotalSales']) && isset($detailsArray['reportGrandTotalReturn'])) {
                    $tempStr .= '<n1:reportGrandTotalSalesNet>' .
                         str_replace(",", "", number_format(floatval(($detailsArray['reportGrandTotalSales'] - $detailsArray['reportGrandTotalReturn'])), 2)) .
                         '</n1:reportGrandTotalSalesNet>';
                    $printStr .= "Grand Total Sales Net [c ".str_replace(",", "", number_format(floatval(($detailsArray['reportGrandTotalSales'] - $detailsArray['reportGrandTotalReturn'])), 2)).":";
                } else {
                    $tempStr .= '<n1:reportGrandTotalSalesNet>0.00</n1:reportGrandTotalSalesNet>';
                    $printStr .= "Grand Total Sales Net [c 0.00:";
                }
    
                $printStr .= " :";
                $printStr .= " :";
                $printStr .= " :";
                $printStr .= " :";
                $printStr .= "---------------------------:";
    
                $tempStr .= '</n1:eventReport>';
                $tempStr .= '</n1:event>';
    
                // echo 'testing--';
            }
            // $tempStr = '';
        }
        // die;
        // create report PDF and store starts here
        if ($createReportPDF) {
            $tempStr = sanitizeXML($tempStr);
            $nextSequenceNumber = $detailsForNewReport['nextSequenceNumber'];
            $file = $fileDir . DIRECTORY_SEPARATOR . $mode . 'report_' . $register . '_' . date('Y-m-d H:i:s') . '_' . max($lastOrderAutoIncrementIDValsArray) . '_' .
                 $nextSequenceNumber . '.xml';
            // die($file);
            $printStr = str_replace(array("\n","\r"), "", $printStr);
            $printStr = str_replace("				", "", $printStr);
            $tempStr = addHeaderNameSpaceXMLTags($tempStr);
            createFile($tempStr, $fileDir, $file, 'xml', false, $printStr);
            lavu_exit();
        }
        // end of report PDF generation
    
        /*
         * start scanning the existing reports of cash register and pull the content to append here of both x and z reports
         */
        $pattern = str_replace(" ", "\ ", 'xreport_' . $register);
        foreach (glob($fileDir . DIRECTORY_SEPARATOR . $pattern . "_*.xml") as $filename) {
            $fileDetails = explode("_", basename($filename));
            // echo $filename.'--'.$norwayCashRegisterObj->startDate.' '.$norwayCashRegisterObj->endDate.' '.$fileDetails[2];die;
            if ($norwayCashRegisterObj->startDate . " " . $norwayCashRegisterObj->startDayTime <= $fileDetails[2] &&
                 $fileDetails[2] <= $norwayCashRegisterObj->endDate . " " . $norwayCashRegisterObj->endDayTime) {
                $xmlStr .= removeHeaderNameSpaceXMLTags(trim(file_get_contents($filename)));
            }
        }
    
        $pattern = str_replace(" ", "\ ", 'zreport_' . $register);
        // echo $fileDir.DIRECTORY_SEPARATOR.$pattern;die;
        foreach (glob($fileDir . DIRECTORY_SEPARATOR . $pattern . "_*.xml") as $filename) {
            // echo $filename.'--'.$norwayCashRegisterObj->startDate.' '.$norwayCashRegisterObj->endDate.' '.$fileDetails[2];
            $fileDetails = explode("_", basename($filename));
            if ($norwayCashRegisterObj->startDate . " " . $norwayCashRegisterObj->startDayTime <= $fileDetails[2] &&
                $fileDetails[2] <= $norwayCashRegisterObj->endDate . " " . $norwayCashRegisterObj->endDayTime) {
                // die('MEE');
                $xmlStr .= removeHeaderNameSpaceXMLTags(trim(file_get_contents($filename)));
            }
        }
        // die;
        /*
         * end of scanning and appending content
         */
    
        // $xmlStr .= $tempStr;
        $tempStr = '';
        $orderarr = array();
        $checkorderArr = array();
        $i = 0;
        // end of report data
    
        foreach ($cashRegisterOrderIds[$register]['order_ids'] as $oneOrder) {
            $xmlStr .= '<n1:cashtransaction>';
            $xmlStr .= '<n1:nr>' . $allOrders[$oneOrder]['order_id'] . '</n1:nr>';
            $xmlStr .= '<n1:transID>' . $allOrders[$oneOrder]['order_id'] . '</n1:transID>';
            $xmlStr .= '<n1:transAmntIn>' . str_replace(",", "",number_format(floatval($allOrders[$oneOrder]['total']), 2)) . '</n1:transAmntIn>';
            $xmlStr .= '<n1:transAmntEx>' . str_replace(",", "",number_format(floatval(($allOrders[$oneOrder]['subtotal'] - $allOrders[$oneOrder]['discount'])), 2)) . '</n1:transAmntEx>';
            $xmlStr .= '<n1:amntTp>' . $allOrders[$oneOrder]['amounttype'] . '</n1:amntTp>';
            $xmlStr .= '<n1:transDate>' . $allOrders[$oneOrder]['closed_date'] . '</n1:transDate>';
            $xmlStr .= '<n1:transTime>' . $allOrders[$oneOrder]['closed_time'] . '</n1:transTime>';
    
            if (isset($orderLineItems[$oneOrder])) {
                foreach ($orderLineItems[$oneOrder] as $oneLineItem) {
                    $xmlStr .= '<n1:ctLine>';
                    $xmlStr .= '<n1:nr>' . $oneLineItem['order_id'] . '</n1:nr>';
                    $xmlStr .= '<n1:lineID>' . $oneLineItem['id'] . '</n1:lineID>';
                    $xmlStr .= '<n1:lineType>' . $oneLineItem['order_id'] . '</n1:lineType>';
                    $xmlStr .= '<n1:artGroupID>' . $oneLineItem['category_id'] . '</n1:artGroupID>';
    
                    $xmlStr .= '<n1:artID>' . $oneLineItem['item_id'] . '</n1:artID>';
                    $xmlStr .= '<n1:qnt>' . $oneLineItem['quantity'] . '</n1:qnt>';
                    $xmlStr .= '<n1:lineAmntIn>' . str_replace(",", "",number_format(floatval($oneLineItem['total_with_tax']), 2)) . '</n1:lineAmntIn>';
                    $xmlStr .= '<n1:lineAmntEx>' . str_replace(",", "",number_format(floatval($oneLineItem['tax_subtotal1']), 2)) . '</n1:lineAmntEx>';
                    $xmlStr .= '<n1:amntTp>' . $allOrders[$oneOrder]['amounttype'] . '</n1:amntTp>';
    
                    // vat
                    $xmlStr .= '<n1:vat>';
                    $xmlStr .= '<n1:vatCode>' . $oneLineItem['tax_name1'] . '</n1:vatCode>';
                    $xmlStr .= '<n1:vatAmnt>' . str_replace(",", "",number_format(floatval($oneLineItem['tax1']), 2)) . '</n1:vatAmnt>';
                    $xmlStr .= '</n1:vat>';
                    // discount
                    if ($oneLineItem['discount_amount'] > 0) {
                        $xmlStr .= '<n1:discount>';
                        $xmlStr .= '<n1:dscTp>' . $oneLineItem['discount_type'] . '</n1:dscTp>';
                        $xmlStr .= '<n1:dscAmnt>' . str_replace(",", "",number_format(floatval($oneLineItem['discount_amount']), 2)) . '</n1:dscAmnt>';
                        $xmlStr .= '</n1:discount>';
                    }
                    // raise
    
                    $xmlStr .= '</n1:ctLine>';
                }
            }
    
            // rounding amount tag
            if ($allOrders[$oneOrder]['rounding_amount'] > 0) {
                $xmlStr .= '<n1:rounding>';
                $xmlStr .= '<n1:roundingAmnt>' . $allOrders[$oneOrder]['rounding_amount'] . '</n1:roundingAmnt>';
                $xmlStr .= '</n1:rounding>';
            }
    
            if (isset($orderTransactions[$oneOrder])) {
                foreach ($orderTransactions[$oneOrder] as $oneCCtransaction) {
                    $xmlStr .= '<n1:payment>';
                    $xmlStr .= '<n1:paymentType>' . $oneCCtransaction['pay_type'] . '</n1:paymentType>';
                    $xmlStr .= '<n1:paidAmnt>' . str_replace(",", "",number_format(floatval($oneCCtransaction['total_collected']), 2)) . '</n1:paidAmnt>';
                    $xmlStr .= '</n1:payment>';
                }
            }

            if ($allOrders[$oneOrder]['order_status'] == 'voided') {
                $xmlStr .= '<n1:desc>'.speak('Order Voided Reason - ').$allOrders[$oneOrder]['void_reason'].'</n1:desc>';
            }

            
            /* for split transaction we add check number to the signature at the end separated by # key start */
            $orderarr[]= $allOrders[$oneOrder]['order_id'];
            $checkorderArr=array_count_values($orderarr);
            if($checkorderArr[$allOrders[$oneOrder]['order_id']]==1)
            {
            	$i=0;
            }
            
            // calculate signature or get it from DB
            if($allOrders[$oneOrder]['check'][$i] !=''){
            	$xmlStr .= '<n1:signature>' . $allOrders[$oneOrder]['signature'] .'#'.$allOrders[$oneOrder]['check'][$i]. '</n1:signature>';
            	}else{
            	$xmlStr .= '<n1:signature>' . $allOrders[$oneOrder]['signature'] . '</n1:signature>';
            }
            $xmlStr .= '<n1:keyVersion>' . $allOrders[$oneOrder]['key_version'] . '</n1:keyVersion>';
            $i++;
            /* for split transaction we add check number to the signature at the end separated by # key end */
            
            if ($allOrders[$oneOrder]['order_status'] == 'voided') {
                $xmlStr .= '<n1:voidTransaction>true</n1:voidTransaction>';
            } else {
                $xmlStr .= '<n1:voidTransaction>false</n1:voidTransaction>';
            }

            $xmlStr .= '</n1:cashtransaction>';
        }
    
        $xmlStr .= '</n1:cashregister>';
    }
    
    $xmlStr .= '  </n1:location>
                 </n1:company>
                </n1:auditfile>';
    // die;
    
    // sanitize start
    $xmlStr = sanitizeXML($xmlStr);
    // sanitize end
    
    $file = $fileDir . DIRECTORY_SEPARATOR . $norwayCashRegisterObj->currentFileName;
    
    createFile($xmlStr, $fileDir, $file, 'xml', true);
    
    lavu_exit();
}