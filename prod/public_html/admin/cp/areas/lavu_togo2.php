<?php
	$result = lavu_query("SELECT value FROM `config` WHERE `setting`='use_lavu_togo_2'");
	$row = mysqli_fetch_assoc($result);
	$lavu_enable= $row['value'];
	if ($in_lavu && $lavu_enable == '1') {
		$lavu_only		= "<font color='#588604' style='font-size:14px'><b>*</b></font> ";
		$forward_to		= "index.php?mode=".$section."_".$mode;
		$tablename		= "locations";
		$rowid			= $locationid;
		$notice_times = array(
			'5'		=> "5 minutes",
			'10'		=> "10 minutes",
			'15'		=> "15 minutes",
			'20'		=> "20 minutes",
			'25'		=> "25 minutes",
			'30'		=> "30 minutes",
			'35'		=> "35 minutes",
			'40'		=> "40 minutes",
			'45'		=> "45 minutes",
			'50'		=> "50 minutes",
			'55'		=> "55 minutes",
			'60'		=> "1 hour",
			'120'	=> "2 hours",
			'180'	=> "3 hours",
			'240'	=> "4 hours",
			'300'	=> "5 hours",
			'360'	=> "6 hours",
			'420'	=> "7 hours",
			'480'	=> "8 hours",
			'540'	=> "9 hours",
			'600'	=> "10 hours",
			'660'	=> "11 hours",
			'720'	=> "12 hours",
			'780'	=> "13 hours",
			'840'	=> "14 hours",
			'900'	=> "15 hours",
			'960'	=> "16 hours",
			'1020'	=> "17 hours",
			'1080'	=> "18 hours",
			'1140'	=> "19 hours",
			'1200'	=> "20 hours",
			'1260'	=> "21 hours",
			'1320'	=> "22 hours",
			'1380'	=> "23 hours",
			'1440'	=> "1 day",
			'2880'	=> "2 days",
			'4320'	=> "3 days"
		);
		// supported phone carriers for LTG
		$phone_carriers = array(
			'att'		=> "AT&T",
			'sprint'		=> "Sprint",
			'tmobile'	=> "T-Mobile",
			'verizon'	=> "Verizon"
		);
		$delivery_options = array(
				'Pickup'=> speak("Pickup"),
				'Delivery'=> speak("Delivery"),
				'Dine-In'=> speak("Dine-In"),
				'Pickup,Delivery'=> speak("Pickup and Delivery"),
				'Pickup,Dine-In'=> speak("Pickup and Dine-In"),
				'Delivery,Dine-In'=> speak("Delivery and Dine-In"),
				'Pickup,Delivery,Dine-In'=> speak("Pickup andDelivery and Dine-In")
		);

		$ltgCardInfo = json_decode($location_info['ltg_payment_info']);
		$gatewayStatus = 0;
		foreach ($ltgCardInfo as $integration) {
			$gatewayStatus = $integration->status;
			if($gatewayStatus) {
				break;
			}
		}
		$payment_options = array(
			2 => speak("Payment Upon Receipt Only")
		);
		if ($gatewayStatus) {
			$payment_options[1] = speak("Prepay Only")." (".speak("credit card integration required").")";
			$payment_options[3] = speak("Prepay and Payment Upon Receipt");
		}

		$order_type_query = lavu_query("SELECT value FROM `config` WHERE `setting`='ltg_dine_in_option_2'");
		$order_type_data = mysqli_fetch_assoc($order_type_query);
		if (isset($order_type_data['value']) && strpos($order_type_data['value'], 'Delivery') != false ) {
			$radius_query = lavu_query("SELECT value FROM `config` WHERE `setting`='ltg_delivery_radius'");
			$radius_data = mysqli_fetch_assoc($radius_query);
			if (!isset($radius_data['value']) || $radius_data['value'] == 'null' || $radius_data['value'] =='') {
				echo "<div style='color: #ce2222;'>Enabling Delivery order type delivery fees on distance in miles is <strong>required</strong></div>";
			}
		}
		$include_session = "?session_id=".session_id();
		$include_data_name = "&dn=".sessvar("admin_dataname");
		$hours_of_availability_iframe_src = "/cp/areas/lavu_togo_times.php".$include_session.$include_data_name;
		$delivery_fees_by_distance = "/cp/areas/edit_lavutogo_radius.php".$include_session.$include_data_name;
		$lavu_togo_credit_card_info = "/cp/areas/edit_lavutogo_payment.php".$include_session.$include_data_name;
// - - - - - - - - Lavu ToGo Online Ordering - - - - - - - - //
		$advanced_max_days = implode(',',range(0,30));
		$fields = array(
			array(
				"General Settings for Customer Online Ordering",	// field label
				"Lavu ToGo Online Ordering",
				"seperator"								// field type
			),
			array(
				"To Go ".speak("Type").":",
				"config:use_togo_type_2",
				"hidden",
				"defaultvalue:Lavu ToGo2"
			),
			array(
				speak("Use Optional Modifiers from Menu").":",
				"config:ltg_use_opt_mods_2",
				"bool"
			),
			array(
				"Lavu To Go ".speak("Unique URL").": <strong style='color:#336699'>https://go.lavutogo.com/</strong>",
				"config:lavu_togo_name",
				"text",
                "size:30"
			),
			array(
				"Lavu To Go ".speak("Embed code")." (".speak("copy/paste it into your website")."):",
				"",
				""
			),
			array(
				"<div id='ltg_embed' style='width:600px; border:1px solid #ccc; padding:3px; margin:auto;'>&nbsp;</div>",
				"",
				"custom"
			),
			// - - - - - - - - Lavu ToGo General Settings - - - - - - - - //
			array(
					"Lavu To Go ".speak("General Settings"),
					"Lavu ToGo General Settings",
					"seperator"
			),
			array(
					speak("Hours of Availability").":",
					"ltg_start_end_time",
					"open_modal_button",
					array(
							'prefix'		=> "",
							'value'			=> speak("View / Edit"),
							'link'			=> "index.php?mode=settings_lavu_togo_times&edit=1",
							'header_text'	=> "Lavu To Go Hours Of Availability",
							'iframe_src'	=> $hours_of_availability_iframe_src,
							'modal_number'	=> $modalNumber++
					)
			),
			array(
					speak("Lavu To Go Credit Card Integration Settings").":",
					"payment_gateways",
					"open_modal_button",
					array(
							'prefix'     => "",
							'value'		 => speak("View / Edit"),
							'link'		 => "index.php?mode=settings_edit_lavutogo_payment",
							'header_text'	=> "Lavu To Go Credit Card Integration Settings",
							'iframe_src'	=> $lavu_togo_credit_card_info,
							'modal_number'	=> $modalNumber++
					)
			),
			array(
				speak("Only allow orders to be placed as 'ASAP' orders").":",
				"config:enable_allow_asap_orders_2",
				"bool"
			),
		);
		$fields = array_merge( $fields, array(
			array(
				speak("Numbers of Days in Advance Orders Can Be Placed").":",
				"config:max_advance_days_2",
				"select",
				$advanced_max_days						// comma delimited list for select options
			),
			array(
				speak("Minimum preparation time for order fulfillment").":",
				"config:minimum_notice_time_2",
				"select",
				$notice_times							// array for select options
			),
			array(
				speak("Enable In-app Order Polling").":",
				"config:enable_togo_polling_2",
				"bool"
			),
			array(
					speak("Minimum Order Total for Delivery Orders").":",
					"config:ltg_minimum_delivery_amount",
					"text",
					"size:15"
			),
			array(
					speak("Default Delivery fee").":",
					"config:ltg_delivery_fee",          //LP-1439
					"text",
					"size:15"
			),
			array(
					speak("Maximum Delivery Distance in Miles").":",
					"config:ltg_max_distance_miles",
					"text",
					"size:15"
			),
			array(
					speak("Theme Color Picker").":",
					"config:lavu_togo_theme",
					"text",
					"size:10",
					"value:#"
			)
		));
		if (is_lavu_admin())
		{
			$fields[] = array(
				"Lavu To Go and Auto Sending Polling Interval:",
				"config:togo_order_polling_interval_2",
				"select",
				"10,15,20,25,30,35,40,45,50,55,60"
			);
		}
		$fields = array_merge( $fields, array(
			// TODO - maybe in the future this features required
			/*array(
				speak("Cell Phone Carrier for Order Alert Text Messages").":",
				"config:ltg_phone_carrier_2",
				"select",
				$phone_carriers
			),

			array(
				speak("Phone number for Order Alert Notifications")." (".speak("format").": 5051234567):",
				"config:ltg_phone_number_2",
				"text",
                "size:30"
			),
			  
			array(
				speak("Email for Order Alert Notifications").":",
				"config:ltg_email_2",
				"text",
                "size:30"
			),*/
			array(
					speak("Treat site as single location").":",
					"config:ltg_treat_site_single_location",
					"bool"
			),
			// - - - - - - - - Lavu ToGo Customer Settings - - - - - - - - //
			array(
					"Lavu To Go ".speak("Customer Settings"),
					"Lavu ToGo Customer Settings",
					"seperator"
			),
			array(
					speak("Delivery fee based on distance in miles").":",
					"ltg_delivery_radius",
					"open_modal_button",
					array(
							'prefix'	=> "",
							'value'		=> speak("View / Edit"),
							'link'		=> "index.php?mode=settings_edit_lavutogo_radius&edit=1",
							'header_text'	=> "Delivery Fees by Distance",
							'iframe_src'	=> $delivery_fees_by_distance,
							'modal_number'	=> $modalNumber++
					)
			),
			array(
			    speak("Dining Types").":",
			    "config:ltg_dine_in_option_2",
			    "bool_multiple"
			),
			array(
					speak("Show Tip Options").":",
					"config:ltg_tip",
					"bool"
			),
			array(
					speak("Enable restaurant information on checkout").":",
					"config:ltg_on_checkout_display_restaurant_info",
					"bool"
			),
			array(
					speak("Success message on checkout").":",
					"config:ltg_on_checkout_success_message",
					"textarea",
					"rows:4,cols:28,custom:true"
			),
			array(
				speak("Customer Payment Options").":",
				"config:ltg_payment_option_2",
				"select",
				$payment_options
			)
		));
		if (is_lavu_admin())
		{
			$fields = array_merge( $fields, array(
				array(
					$lavu_only.speak("Enable Order Fetching for Local Server").":",
					"config:lls_fetch_api_orders_2",
					"bool"
				),
// - - - - - - - - Lavu ToGo Technical Settings - - - - - - - - //
					array(
							$lavu_only."Lavu To Go Technical Settings",
							"Lavu ToGo Technical Settings",
							"seperator"
					),
					array(
							"Lavu Central Server URL for Lavu To Go Transactions:",
							"config:ltg_use_central_server_url",
							"text",
							"size:30"),
					array(
							"Enable Lavu To Go debugging:",
							"config:ltg_debug",
							"bool"),
					array(
							speak("Embed Typeform feedback survey code").":",
							"config:ltg_on_embed_typform_feedback",
							"textarea",
							"rows:4,cols:28,custom:true"
					),
					array(
							speak("Enter Google Analytics Tracking Id").":",
							"config:ltg_google_analytics_tracking_id",
							"text",
							"size:30"),
					array(
							speak("Enter Google Tag Management Id").":",
							"config:ltg_google_tag_management_id",
							"text",
							"size:30"),
			));
		}
		$fields[] = array(
			speak("Save"),
			"lavutogo_submit",
			"lavutogo_submit"
		);
		require_once(resource_path()."/form.php");

		if (is_lavu_admin())
		{
			echo "<br>"."Indicates items visible only to Lavu Admin users (i.e., developers and support staff)";
		}

		if (isset($_GET['show_connector']))
		{
			echo "<br><br>Location Id: ".$locationid;
			echo "<br>Connector Name: ".admin_info("dataname");
			echo "<br>Connector Key: ".lsecurity_key(admin_info("companyid"));
		}
		echo "<br><br>";
	} else {
		echo "<div style='margin:20px 0px;'> Please enable Lavu To Go 2.0 &nbsp;</div>";
	}
?>
<link href="/cp/css/select2-4.0.3.min.css" rel="stylesheet" />
	<script src="/cp/scripts/select2-4.0.3.min.js"></script>
	<script src="/cp/scripts/jscolor.js"></script>
<script type="text/javascript">
$(document).ready(function() {
		 $('input[name^=\'config:lavu_togo_theme\']').addClass("jscolor");
		 var input = $('input[name^=\'config:lavu_togo_theme\']');
		 input.val("#"+input.val() )
		 var asapordv = $('[name="config:enable_allow_asap_orders_2"]').val();
		 if(asapordv== 1)
		 {
			var advanceordvalue = 0;
			$('[name="config:max_advance_days_2"]').val(advanceordvalue);
			document.getElementById('config:max_advance_days_2').parentNode.parentNode.style.display='none';
		 }

});

	if ((document.form1.elements['config:lavu_togo_name']) && document.form1.elements['config:lavu_togo_name'].value!="")
	{
		var ltgname = document.form1.elements['config:lavu_togo_name'].value;
		var iframecode = "<iframe src='https://go.lavutogo.com/" + ltgname + "' width='100%' height='100%' frameborder='0' scrolling='yes'></iframe>";
		// build iframe code for embeddable ltg
		document.getElementById('ltg_embed').innerHTML = "<pre style='white-space:normal;' id='ifc'>" + iframecode.replace(/&/g, "&amp;").replace(/</g, "&lt;") + "</pre>";
		document.getElementById('ifc').focus();
	}
	else
	{
		document.getElementById('ltg_embed').style.display = "none";
	}
	function remove(array, element) {
	    var index = array.indexOf(element);
	    array.splice(index, 1);
	}
	
	$('[type="checkbox"][value="Pickup"],[type="checkbox"][value="Delivery"],[type="checkbox"][value="Dine-In"]').on('change',function(){
		var value = $('[name="config:ltg_dine_in_option_2"]').val();
		if( value === '0' || value === '') {
			$('[type="checkbox"][value="Pickup"]').prop('checked', true);
			$('[name="config:ltg_dine_in_option_2"]').val($('[type="checkbox"][value="Pickup"]').val());
		}
		var checked=$('#ltg_Delivery').is(":checked");
		if (checked == 1) {
			toCheckDeliveryDistance();
		 }
	});
	$('[type="checkbox"][input[name="config:enable_allow_asap_orders_2"]]').change(function(){
		var asapord = $('[name="config:enable_allow_asap_orders_2"]').val();
		var defaultadvancedays = $('[name="value_before_config:max_advance_days_2"]').val();
		if(asapord == 1)
		{
			var advanceordvalue = 0;
			$('[name="config:max_advance_days_2"]').val(advanceordvalue);
			document.getElementById('config:max_advance_days_2').parentNode.parentNode.style.display='none';
		}
		else
		{
			$('[name="config:max_advance_days_2"]').val(0);
			document.getElementById('config:max_advance_days_2').parentNode.parentNode.style.display='';
		} 	
	});
	function toCheckDeliveryDistance() {
		$.ajax({
			url: "areas/ajax_lavu_togo.php",
			type: "POST",
			async: true,
			success: function(data) {
				if (data !='' && data =='null') {
						alert("Enabling Delivery order type delivery fees on distance in miles is required.");
						var d_value = $('[name="config:ltg_dine_in_option_2"]').val();
						var dt_value = d_value.split(",");
						var index = dt_value.indexOf('Delivery');
						if (index !== -1) dt_value.splice(index, 1);
						$('[name="config:ltg_dine_in_option_2"]').val(dt_value);
						$('input:checkbox[value="Delivery"]').prop('checked', false);
				}
			},
			error: function() {
				alert("Seems problem with server loading.. Please try again later");
			}
		});
	}
	$('#_setting_btn_submit_id').click(function(){
		var deliveryFees = $('[name="config:ltg_delivery_fee"]').val();
		var maxDeliveyDistance = $('[name="config:ltg_max_distance_miles"]').val();
		var checkoutMessage = $('[name="config:ltg_on_checkout_success_message"]').val();
		var minOrderAmount = $('[name="config:ltg_minimum_delivery_amount"]').val();
		var regexToCheckOnlyAlpha = /^[a-zA-Z\s.,0-9]*$/;
		var regexpatter = /^\s*(?=.*[0-9])\d*(?:\.\d{1,9})?\s*$/;
		var regexpatterDistance = /^\s*(?=.*[1-9])\d*(?:\.\d{1,9})?\s*$/;
		var validateField = (fieldName) => { 
			return isNaN(fieldName) || fieldName < '0' || fieldName === '-0';			
		}
		if (isNaN(deliveryFees) || deliveryFees == '' || deliveryFees < 0 || !regexpatter.test(deliveryFees)) {
			alert('Default Delivery fee cannot be character, symbol or numbers less then zero');
			return false;
		}
		else if (isNaN(maxDeliveyDistance) || maxDeliveyDistance == '' || maxDeliveyDistance < 0 || !regexpatterDistance.test(maxDeliveyDistance)) {
			alert('Maximum Delivery Distance in Miles cannot be character,null or zero');
			return false;
		}
		else if (isNaN(minOrderAmount) || minOrderAmount == '' || minOrderAmount < 0 || !regexpatter.test(minOrderAmount)) {
			alert('Minimum Order Amount cannot be character, symbol or numbers less then zero');
			return false;
		}
		else if (checkoutMessage.replace(/\s/g,'').length < 1 && regexToCheckOnlyAlpha.test(checkoutMessage)) {
			alert('Please enter a valid checkout success message.');
			return false;
		} else {
			var ltgname = document.form1.elements['config:lavu_togo_name'].value;
			$.ajax({
				url: "areas/validate_ltg_url.php",
				data: { ltg_restaurant_name : ltgname },
				type: "POST",
				async: true,
				success: function(data) {
					var json = JSON.parse(data);
					if (json.status == 'Failure') {
						alert(json.msg);
						return false;
					} else {
						document.form1.submit();
					}
				},
				error: function() {
					alert("Seems problem with server loading the Lavu To Go URL");
				}
			});
		}
	});
</script>