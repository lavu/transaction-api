<?php
	function render_mode2_content($str)
	{
		$str = str_replace("[h_logo]",render_mode2_logo(),$str);
		$str = str_replace("[h_restaurant_name]",render_mode2_restaurant_name(),$str);
		$str = str_replace("[h_nav]",render_mode2_nav(),$str);
		return $str;
	}
	
	$mode2_rest_read = array();
	function render_mode2_rest_read($colname="")
	{
		global $data_name;
		global $mode2_rest_read;
		
		if(count($mode2_rest_read) < 1)
		{
			$rest_query = mlavu_query("select * from `poslavu_MAIN_db`.`restaurants` where `data_name`='[1]' and `data_name`!=''",$data_name);
			if(mysqli_num_rows($rest_query))
			{
				$mode2_rest_read = mysqli_fetch_assoc($rest_query);
			}
		}
		if($colname=="") return $mode2_rest_read;
		else if(isset($mode2_rest_read[$colname])) return $mode2_rest_read[$colname];
		else return "";
	}
	
	function render_mode2_logo()
	{
		global $data_name;
		global $location_info;
		
		$logo_img = render_mode2_rest_read("logo_img");
		if($logo_img!="")
		{
			$img_path = "/images/$data_name/" . $logo_img;
			return "<img src='$img_path' style='max-width:90px; max-height:90px; border-radius:14px' />";
		}
		else return "";
	}
	
	function render_mode2_restaurant_name()
	{
		return render_mode2_rest_read("company_name");
	}
	
	function render_mode2_nav()
	{
		global $rootLink;
		
		$selected_bgcolor = "#555555";
		$mouseover_bgcolor = "#777777";
		
		$nav_start = "<table cellpadding=12 cellspacing=0 style='width:100%; cursor:pointer'>";
		$row_start = "<tr onmouseover='this.bgColor = \"$mouseover_bgcolor\"' onmouseout='this.bgColor = \"transparent\"'><td valign='middle' style='padding-left:30px'>";
		$row_start_selected = "<tr bgcolor='$selected_bgcolor' onmouseover='this.bgColor = \"$mouseover_bgcolor\"' onmouseout='this.bgColor = \"$selected_bgcolor\"'><td valign='middle' style='padding-left:30px'>";
		$row_middle = "</td><td valign='middle' style='color:#c0c0be; font-size:16px; font-family:Din,Arial'>";
		$row_end = "<td></tr>";
		$nav_end = "</table>";
		
		$str = $rootLink->__toString2();
				
		$str = str_replace("<img ","<img style='max-width:34px; max-height:34px' ",$str);
		$str = str_replace("<ul>","",$str);
		$str = str_replace("</li>","",$str);
		$str = str_replace("<li>",$row_start,$str);
		$str = str_replace("</a>","</a>" . $row_end,$str);
		$str = str_replace("<br/>","</a>" . $row_middle,$str);
		$str = str_replace("<a ","<a_d ",$str); // override links
		$str = str_replace("</a>","</a_d>",$str); // override links
		$str = str_replace("selected","selected_d",$str);
		
		$str = $nav_start . $str . $nav_end;
		
		$newstr = "";
		$str_parts = explode("<tr ",$str);
		for($i=0; $i<count($str_parts); $i++)
		{
			if($i==0) $newstr .= $str_parts[0];
			else
			{
				if(strpos($str_parts[$i],"selected")!==false)
				{
					$str_parts[$i] = str_replace(substr($row_start,4),substr($row_start_selected,4),$str_parts[$i]);
				}
				
				$newstr_part = "";
				$link_str = "";
				$inner_parts = explode("href='",$str_parts[$i]);
				if(count($inner_parts) > 1)
				{
					$link_parts = explode("'",$inner_parts[1],2);
					if(count($link_parts) > 1)
					{
						$link_str = trim($link_parts[0]);
					}
				}
				
				$newstr_part .= "<tr ";
				$link_str = str_replace("'","",$link_str);
				$link_str = str_replace("\"","",$link_str);
				if($link_str!="") $newstr_part .= "onclick='window.location = \"$link_str\"' ";
				$newstr_part .= $str_parts[$i];
				
				$newstr .= $newstr_part;
			}
		}
		$str = $newstr;
	
		$str = str_replace($row_middle . "</a_d>",$row_middle . "Dashboard</a_d>",$str);
		
		$showstr = $str;
		$showstr = str_replace("<tr ","\n\t<tr ",$showstr);
		$showstr = str_replace("<td ","\n\t\t<td ",$showstr);
		$showstr = str_replace("</td>","\n\t\t</td>",$showstr);
		$showstr = str_replace("</tr>","\n\t</tr>",$showstr);
		$showstr = str_replace("</table>","\n</table>",$showstr);
		
		return $str;
	}
?>
