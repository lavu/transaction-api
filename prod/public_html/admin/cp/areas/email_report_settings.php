<?php
/**
 * Created by PhpStorm.
 * User: hayat
 * Date: 11/8/17
 * Time: 9:06 PM
 */

    require_once (dirname(__FILE__)."/reports/abi_report_config.php");
    require_once (dirname(__FILE__)."/reports/abi_report_functions.php");

    if(!$in_lavu){return;}

    $action = (isset($_REQUEST['action'])) ? strtolower($_REQUEST['action']) : '';
    $id = (isset($_REQUEST['id'])) ? strtolower($_REQUEST['id']) : '';
    $chain_id = 564;//sessvar('admin_chain_id');
    list($reportRegions, $staticPlanRecord, $staticBeerVolume) = getPlannedData($chain_id);
    sort($reportRegions);
    $eRegion = '';
    $eLocation_name = '';
    $eEmail_schedule = '';
    $eReport_type = '';
    $eSend_to = '';
    $edit = false;
    if($action == 'edit' && $id != '') {
        $query = mlavu_query("SELECT * FROM `poslavu_MAIN_db`.`reports_email_schedule` WHERE `id` = '[1]' AND `chain_id` = '[2]' AND `_deleted` != '[3]'", $id, $chain_id, 1);
        $read = mysqli_fetch_assoc($query);
        $eRegion = $read['region'];
        $eLocation_name = $read['location_name'];
        $eEmail_schedule = $read['email_schedule'];
        $eReport_type = $read['report_type'];
        $eSend_to = implode("\n", explode(',', $read['send_to']));
        $edit = true;
    } else if($action == 'delete' && $id != '') {
        mlavu_query("UPDATE `poslavu_MAIN_db`.`reports_email_schedule` SET `_deleted` = '[1]' WHERE `id` = '[2]'", 1, $id);
        $vars = array();
        $vars['action'] = "Delete Manage Email Reports";
        $vars['action'] = "Delete Manage Email Reports";
        $vars['user'] = admin_info("username");
        $vars['user_id'] = admin_info("loggedin");
        $vars['ipaddress'] = reliableRemoteIP();
        $vars['data'] = admin_info("fullname");
        $vars['detail1'] = "Access level: ".admin_info("access_level");
        $vars['detail2'] = "Deleted Id: " . $id;
        $vars['server_time'] = UTCDateTime(TRUE);
        $vars['time'] = DateTimeForTimeZone(reqvar("ext_tz", ""));
        lavu_query("insert into `admin_action_log` (`action`,`user`,`user_id`,`ipaddress`,`data`,`detail1`,`detail2`, `time`, `server_time`) values ('[action]','[user]','[user_id]','[ipaddress]','[data]','[detail1]','[detail2]','[time]','[server_time]')",$vars);
        header("Location:?mode=settings_email_report_settings");
    }
    if (!empty($_POST)) {
        $email_schedule = $_POST['email_schedule'];
        $report_type = $_POST['report_type'];
        $region = $_POST['region'];
        $location = $_POST['location'];
        $sendToArr = explode("\r\n", $_POST['send_to']);
        $send_to = implode(",", $sendToArr);
        $query = mlavu_query("SELECT * FROM `poslavu_MAIN_db`.`reports_email_schedule` WHERE `region` = '[1]' AND  `location_name` = '[2]' AND `email_schedule` = '[3]' AND `report_type` = '[4]' AND `chain_id` = '[5]' AND `_deleted` != '[6]'", $region, $location, $email_schedule, $report_type, $chain_id, 1);
        if(mysqli_num_rows($query) > 0 || $edit) {
            $read = mysqli_fetch_assoc($query);
            $id = ($edit) ? $id : $read['id'];
            if (!$edit) {
                $existSendTo = explode(',', $read['send_to']);
                $newSendTo = array_diff($sendToArr, $existSendTo);
                $send_to = implode(',', array_merge($existSendTo, $newSendTo));
            }
            $action = "Updated Email to Manage Email Reports";
            mlavu_query("UPDATE `poslavu_MAIN_db`.`reports_email_schedule` SET `region` = '[1]', `location_name` = '[2]', `report_type` = '[3]', `send_to` = '[4]' WHERE `id` = '[5]'", $region, $location, $report_type, $send_to, $id);
        } else {
            $action = "Added Email to Manage Email Reports";
            $query = mlavu_query("INSERT INTO `reports_email_schedule` (`region`, `location_name`, `email_schedule`, `report_type`, `send_to`, `chain_id`) VALUES ('[1]','[2]','[3]','[4]', '[5]', '[6]')", $region, $location, $email_schedule, $report_type, $send_to, $chain_id);
        }
        $vars = array();
        $vars['action'] = $action;
        $vars['user'] = admin_info("username");
        $vars['user_id'] = admin_info("loggedin");
        $vars['ipaddress'] = reliableRemoteIP();
        $vars['data'] = admin_info("fullname");
        $vars['detail1'] = "Access level: ".admin_info("access_level");
        $vars['detail2'] = "Region: " . $region . '|Location: ' . $location . '|Email Schedule: ' . $email_schedule . '|Report Type: ' . $report_type . '|Send To: ' . $send_to . '|Chain id: ' . $chain_id;
        $vars['server_time'] = UTCDateTime(TRUE);
        $vars['time'] = localize_datetime(date("Y-m-d H:i:s"), $location_info['timezone']);
        lavu_query("insert into `admin_action_log` (`action`,`user`,`user_id`,`ipaddress`,`data`,`detail1`,`detail2`, `time`, `server_time`) values ('[action]','[user]','[user_id]','[ipaddress]','[data]','[detail1]','[detail2]','[time]','[server_time]')",$vars);
        header("Location:?mode=settings_email_report_settings");
    }
    $display = '<table width="40%" id="_setting_tbl_id" style="border:solid 2px #aaaaaa;" class="tableSpacing"><tbody><tr><td colspan="2" bgcolor="#98b624" style="color:#ffffff; font-weight:bold; height:27px; padding:5px 0px 2px 0px;" align="center">';
    $display .= '<b>Manage Email Reports</b></span></td></tr>';
    $display .= '<form  name="scheduler" id="scheduler" method="post" action="">
    <tr><td>'.speak("Email Schedule").': </td><td><select name="email_schedule" id="email_schedule">
    <option value="">--'.speak("Select Schedule").'--</option>';
    foreach($emailSchedule as $schedule) {
        $selected = ($schedule == $eEmail_schedule) ? ' selected = "selected"' : '';
        $display .= '<option value="'.$schedule.'"' . $selected .'>'.speak($schedule).'</option>';
    }

    $display .= '<tr><td>'.speak("Report Type").': </td><td><select name="report_type" id="report_type">
    <option value="">--'.speak("Select Report Type").'--</option>';
    foreach($emailReportType as $rType) {
        $selected = ($rType == $eReport_type) ? ' selected = "selected"' : '';
        $display .= '<option value="'.$rType.'"' . $selected .'>'.speak($rType).'</option>';
    }
    $display .= '</select></td></tr>';
    $display .= '<tr><td>'.speak("Region").': </td><td><select name="region" id="region" onChange="getLocations()">';

    $display .= '<option value="">--'.speak("Select Region").'--</option>';
    $allSelected = ($eRegion == 'All') ? ' selected = "selected"' : '';
    $display .= '<option value="All" '.$allSelected.'>'.speak("All").'</option>';
    foreach($reportRegions as $region) {
        $selected = ($region == $eRegion) ? ' selected = "selected"' : '';
        $display .= '<option value="'.$region.'"' . $selected . '>'.$region.'</option>';
    }
    $display .= '</select></td></tr>';
    $display .= '<tr id="tr_location" style="display:none"><td>'.speak("Location").': </td><td id="location_td">';
    $display .= '</td>';
    $display .= '<input type="hidden" name="location" id="location" value="'.$eLocation_name.'"></tr>';
    $display .= '<tr><td valign="top">'.speak("Send report to").': </td><td><span style="font-size:10px; color:#d3d3d3">Send the report to these emails (one email per line) <br \></span><textarea cols="40" rows="10" placeholder="Add email per line" name="send_to" id="send_to">'. $eSend_to . '</textarea></td></tr>';
    $display .= '<tr><td valign="top" align="center" colspan="2">&nbsp;<br><input type="button" id="saveBtn" value="'.speak("Save").'" style="width:120px" onclick="add_schedular()"></td></tr>';
    $display .= '</form></table>';

    $query = mlavu_query("SELECT * FROM `poslavu_MAIN_db`.`reports_email_schedule` WHERE `_deleted` = 0");
    if(mysqli_num_rows($query) > 0) {
        $display .= '<br/><table width="60%" id="_setting_tbl_id" style="border:solid 2px #aaaaaa;" class="tableSpacing"><tbody><tr><td colspan="6" bgcolor="#98b624" style="color:#ffffff; font-weight:bold; height:27px; padding:5px 0px 2px 0px;" align="center">';
        $display .= '<b>Manage Email Report List</b></span></td></tr>';
        $display .= '<tr><td valign="top" style="border-bottom:solid 1px #aaaaaa; font-weight: bold">'.speak("Region").'</td><td style="border-bottom:solid 1px #aaaaaa; font-weight: bold">'.speak("Location").'</td><td style="border-bottom:solid 1px #aaaaaa; font-weight: bold">'.speak("Report Type").'</td><td style="border-bottom:solid 1px #aaaaaa; font-weight: bold">'.speak("Schedule").'</td><td style="border-bottom:solid 1px #aaaaaa; font-weight: bold">'.speak("Send To").'</td><td style="border-bottom:solid 1px #aaaaaa; font-weight: bold">'.speak("Action").'</td></tr>';
        while ($read = mysqli_fetch_assoc($query)) {
            $showLocations = implode("<br/>", explode(',', $read["location_name"]));
            $display .= '<tr style="border-bottom:solid 1px #aaaaaa"><td valign="top">' . $read["region"] . '</td><td valign="top">' . $showLocations . '</td><td valign="top">' . $read["report_type"] . '</td><td valign="top">' . $read["email_schedule"] . '</td><td valign="top">' . str_replace(",", "<br/>", $read["send_to"]) . '</td><td valign="top"><a href="?mode=settings_email_report_settings&action=edit&id='.$read["id"].'">Edit</a>&nbsp;|&nbsp;<a href="?mode=settings_email_report_settings&action=delete&id='.$read["id"].'">'. speak("Delete").'</td></tr>';
        }
    }
    $display .= '</table>';
    echo $display;
?>
<script language="javascript">
    var eLocation = $('#location').val();
    if (eLocation != '' && eLocation != undefined) {
        getLocations(eLocation);
    }
    function getLocations(dLocation = '') {
        var region = $('#region').val();
        var report_type = $('#report_type').val();
        if (region != 'All' && region != '' && report_type != 'Trends') {
            $.ajax({
                url: "areas/abi_ajax.php?action=getLocation&region=" + region, success: function (result) {
                    $("#location_td").html(result);
                    if (dLocation != '') {
                        var dLocations = dLocation.split(",");
                        $("#selLocation").val(dLocations);
                    }
                }
            });
            $('#tr_location').show();
        } else {
            $('#tr_location').hide();
        }
    }

    function add_schedular() {
        var email_schedule = $('#email_schedule').val();
        var report_type = $('#report_type').val();
        var region = $('#region').val();
        var location = $('#selLocation').val();
        var sendTo = $('#send_to').val();
        if (email_schedule == '') {
            alert("Please select email schedule");
            return false;
        }
        if (report_type == '') {
            alert("Please select report type");
            return false;
        }
        if (region == '') {
            alert("Please select region");
            return false;
        }
        if(region != '' && region != 'All' && location == '') {
            alert("Please select location");
            return false;
        }
        if (email_schedule == 'Daily' && report_type == 'Trends') {
            alert('Please select "Weekly" schedule for Trends Report.');
            return false;
        }
        $('#location').val(location);
        var emails = sendTo.split('\n');
        if (sendTo == '') {
            alert("Please enter email per line");
            return false;
        }
        $.each(emails, function( index, value ) {
            if (!validateEmail(value)) {
                alert('"' + value + '" is not valid email');
                return false;
            }
        });

        $('#scheduler').submit();
    }

    function validateEmail($email) {
        var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
        return emailReg.test( $email );
    }
</script>
