<?php

if( !isset($in_lavu) || !$in_lavu) {
	return;
}

/*******************************************************************************
 *                          R U N T I M E   C O D E                            *
 ******************************************************************************/

require_once(dirname(__FILE__).'/../resources/lavuquery.php');
require_once(dirname(__FILE__).'/../objects/json_decode.php');

//Inserted by Brian D.
$videoEmbedHTML = '<iframe width="560" height="315" src="//www.youtube.com/embed/Ze7ax36n_E0" frameborder="0" allowfullscreen></iframe>';
echo $videoEmbedHTML;
//End insert -------------------------------------------------------------------

//Inserted by Brian D.
//Here we check if their component package is not pizza.  If it is "" or "customer",
//then we give them the option to convert their component package code.  This
//Is encapsulated in pizza_settings_enable.php which will handle all of that process,
//but we must first check here.
if( $in_backend && ($comPackage == "" || $comPackage == "customer") ){
	require_once(dirname(__FILE__).'/pizza_settings_enable.php');
	return;
}else if($in_backend && $comPackage != "pizza"){
	return;
}
//End insert  ------------------------------------------------------------------

if (isset($dont_draw_pizza_settings) && $dont_draw_pizza_settings) {
	// do nothing
} else if (isset($_GET['edit_creators'])) {
	PizzaCreatorSettings::sanitizeRows();
	$a_creators = PizzaCreatorSettings::get_pizza_creators();
	if (PizzaCreatorSettings::draw_edit_creators($a_creators))
		require_once(resource_path() . "/browse.php");
	PizzaCreatorSettings::sanitizeRows();
	PizzaCreatorSettings::update_fmods($a_creators);
	PizzaCreatorSettings::marshall_pizza_info_4_ltg_config();//Added by Brian D.
} else if (isset($_GET['creator_id'])) {
	if (PizzaCreatorSettings::prepare_draw_pizza_creator())
		require_once(resource_path() . "/browse.php");
	$a_creators = PizzaCreatorSettings::get_pizza_creators();
	PizzaCreatorSettings::update_fmods($a_creators);
	PizzaCreatorSettings::marshall_pizza_info_4_ltg_config();//Added by Brian D.
} else {
	PizzaCreatorSettings::draw_redirect();
	$a_creators = PizzaCreatorSettings::get_pizza_creators();
	PizzaCreatorSettings::update_fmods($a_creators);
}

/*******************************************************************************
 *             C L A S S   A N D   F U N C T I O N S   C O D E                 *
 ******************************************************************************/

class PizzaCreatorSettings {

	public static function sanitizeRows() {
		self::echo_function_name();
		global $locationid;

		$s_whereclause = "`setting`='pizza creator' AND `type`='setting' AND `location`='[location]'";
		$a_wherevars = array('location'=>$locationid);
		self::lavu_query("UPDATE `config` SET `value3` = 'value:id, value2:name, value4:created, value5:deleted, value6:fmod_list_id' WHERE $s_whereclause", $a_wherevars);
		self::lavu_query("UPDATE `config` SET `value4` = '[date]' WHERE $s_whereclause AND `value4`=''", array_merge($a_wherevars, array('date'=>date('Y-m-d H:i:s'))));
		self::lavu_query("UPDATE `config` SET `value5` = '0000-00-00 00:00:00' WHERE $s_whereclause AND `value5`=''", $a_wherevars);
		self::lavu_query("UPDATE `config` SET `_deleted` = '0' WHERE $s_whereclause AND `_deleted`=''", $a_wherevars);
		self::lavu_query("UPDATE `config` SET `value5` = '[date]' WHERE $s_whereclause AND `value5`='0000-00-00 00:00:00' AND `_deleted`='1'", array_merge($a_wherevars, array('date'=>date('Y-m-d H:i:s'))));
	}

	public static function draw_navigation() {
		self::echo_function_name();

		echo "<br /><br />";
		echo self::creator_selector_tostr();
		echo "<br /><br />";
	}

	// redirect the page to the first found pizza creator
	public static function draw_redirect() {
		self::echo_function_name();
		$a_creators = PizzaCreatorSettings::get_pizza_creators();
		foreach($a_creators as $k=>$v) {
			$s_location = "?mode=".$_GET['mode'].'&creator_id='.$v['id'];
			header("Location: {$s_location}");
			echo "<script type='text/javascript'>window.location = '{$s_location}'</script>";
			break;
		}
	}

	// draws the editor for the creators
	// @$a_creators: can be NULL if the creators should be loaded, can be an array of creators to prevent future loads (for efficiency)
	public static function draw_edit_creators(&$a_creators = NULL) {
		self::echo_function_name();

		global $s_help_text_prepend_settingname;
		$s_help_text_prepend_settingname = 'creators_';
		self::draw_navigation();

		// check for editing by the browse form
		if (isset($_GET['addnew'])) {
			if (!isset($a_creators) || $a_creators === NULL)
				$a_creators = self::get_pizza_creators(FALSE);
			$a_all_creators = self::get_pizza_creators(TRUE);
			$newid = 0;
			foreach($a_all_creators as $a_creator) {
				$id = $a_creator['id'];
				if ($id >= $newid)
					$newid = $id+1;
			}
		} else if (isset($_GET['rowid'])) {
			$a_rows = ConnectionHub::getConn('poslavu')->getDBAPI()->getAllInTable('config', array('id'=>$_GET['rowid']), FALSE);
			$newid = $a_rows[0]['value'];
		}

		global $locationid;
		global $tablename;
		global $forward_to;
		global $filter_by;
		global $order_by;
		$mode = $_GET['mode'];
		$tablename = "config";
		$forward_to = "index.php?mode={$mode}&edit_creators=1";
		$filter_by = "`location`='$locationid' and `setting`='pizza creator'";
		$order_by = "value2";

		// check for deletion
		if (isset($_GET['removeid'])) {
error_log("removeid set");
			$a_live_creators = self::get_pizza_creators();
			if (count($a_live_creators) < 2) {
				echo "There must be at least two Pizza Creators in order to remove a creator. <a href='{$forward_to}'>back</a>";
				return FALSE;
			} else {

				// redirect to reflect changes
				$s_location = str_replace('&removeid='.$_GET['removeid'], '', $_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']);
				if (strpos($s_location, 'http://') === FALSE && strpos($s_location, 'https://') === FALSE)
					$s_location = 'http://'.$s_location;
				echo "<script type='text/javascript'>window.location='{$s_location}';</script>";
			}
		}

		global $fields;
		$fields = array();

		$fields[] = array(speak("Name"),"value2","text","","list:yes");
		$fields[] = array("Setting","setting","hidden","","setvalue:pizza creator");
		$fields[] = array("Type","type","hidden","","setvalue:setting");
		$fields[] = array("ID", "value", "hidden", "", "setvalue:{$newid}");
		$fields[] = array("Location","location","hidden","","setvalue:$locationid");
		$fields[] = array(speak("Save"),"submit","submit");

		return TRUE;
	}

	// used to universally query for forced mods for the pizza creator settings
	public static function set_forced_mods_selectors($s_creator_id, &$s_tablename, &$s_filter_by, &$s_order_by) {
		self::echo_function_name();

		global $locationid;

		// set the values
		$s_tablename = "config";
		$creator_id_selector = ($s_creator_id == '0') ? "AND (`value7`='0' OR `value7`='')" : "AND `value7`='{$s_creator_id}'";
		$s_filter_by = "`location`='$locationid' AND `setting`='pizza_mods' {$creator_id_selector}";
		$s_order_by = "value8`, `value6";
	}

	/**
	 * Updates the deleted categories based on the deletion status of the related forced mods
	 * @return none N/A
	 */
	public static function update_deleted_categories() {
		$s_creator_id = $_GET['creator_id'];

		$a_creators = ConnectionHub::getConn('poslavu')->getDBAPI()->getAllInTable("config", array("setting"=>"pizza_mods", "value7"=>$s_creator_id), FALSE, array("selectclause"=>"`id`,`value2` AS `forced_modifier_lists_id`, `_deleted`"));

		foreach($a_creators as $a_creator) {
			$a_forced_modifiers = ConnectionHub::getConn('poslavu')->getDBAPI()->getAllInTable("forced_modifier_lists", array("id"=>$a_creator['forced_modifier_lists_id'], "_deleted"=>$a_creator['_deleted']), FALSE, array("whereclause"=>"WHERE `id`='[id]' AND `_deleted`!='[_deleted]'", "selectclause"=>"_deleted"));
			if (count($a_forced_modifiers) == 0) {
				continue;
			}
			if( !empty($a_forced_modifiers[0]['_deleted']) ){
				lavu_query("UPDATE `config` SET `_deleted`='[_deleted]' WHERE `id`='[id]'", array("id"=>$a_creator['id'], "_deleted"=>$a_forced_modifiers[0]['_deleted']));
			}
		}
	}

	public static function prepare_draw_pizza_creator() {
		self::echo_function_name();

		self::draw_navigation();

		self::update_deleted_categories();

		// check that some values are within a tollerable range
		self::lavu_query("UPDATE `config` SET `type`='setting' WHERE `setting`='pizza_mods'");
		self::lavu_query("UPDATE `config` SET `value8` = '1' WHERE `setting`='pizza_mods' AND `value8` < 1");
		self::lavu_query("UPDATE `config` SET `value6` = '1' WHERE `setting`='pizza_mods' AND `value6` < 1");
		self::lavu_query("UPDATE `config` SET `value10` = '0.5' WHERE `setting`='pizza_mods' AND `value10` = ''");
		self::lavu_query("UPDATE `config` SET `value10` = '2.0' WHERE `setting`='pizza_mods' AND `value10` > 2");

		global $locationid;
		global $tablename;
		global $forward_to;
		global $filter_by;
		global $order_by;
		$mode = $_GET['mode'];
		$s_creator_id = $_GET['creator_id'];
		$forward_to = "index.php?mode={$mode}&creator_id=$s_creator_id";
		self::set_forced_mods_selectors($s_creator_id, $tablename, $filter_by, $order_by);

		global $fields;
		$fields = array();

		$modlist = array();
		$modlist[''] = "";
		$mod_query = self::lavu_query("select * from `forced_modifier_lists` where (`type`='choice' or `type`='checklist') AND `title` NOT LIKE '*%' AND `_deleted` = '0' ORDER BY `title` ASC");
		while($mod_read = mysqli_fetch_assoc($mod_query))
		{
			$modid = $mod_read['id'];
			$modname = $mod_read['title'];
			$modlist[$modid] = $modname;
		}
		$multiColumnSettings = array("0"=>"Don't Allow", "1"=>"Allow");

		$fields[] = array(speak("Name"),"value","text","","list:yes");
		$fields[] = array(speak("Forced Mods"),"value2","select",$modlist,"list:yes");
		$fields[] = array(speak("Page"),"value8","text","","list:yes");
		$fields[] = array(speak("Display Order"),"value6","text","","list:yes");
		$fields[] = array(speak("Dual-Column"),"value9","select",$multiColumnSettings,"list:no");
		$fields[] = array(speak("Partial Serving Pricing"),"value10","decimal:0.5","","list:no");
		$fields[] = array("Setting","setting","hidden","","setvalue:pizza_mods");
		$fields[] = array("Location","location","hidden","","setvalue:$locationid");
		$fields[] = array("Creator ID","value7","hidden","","setvalue:$s_creator_id");
		$fields[] = array(speak("Save"),"submit","submit");

		return TRUE;
	}

	// break to the createor into its constituent parts
	// based off of the values in `config`.`value3`
	public static function assign_names_to_values(&$a_creator, &$a_retval) {
		self::echo_function_name();
		$a_description_parts = explode(', ',$a_creator['value3']);
		foreach($a_description_parts as $s_description_part) {
			if (!strstr($s_description_part, ':'))
				continue;
			$a_value_description = explode(':',$s_description_part);
			$s_value = $a_value_description[0];
			$s_description = $a_value_description[1];
			$a_retval[$s_description] = $a_creator[$s_value];
		}
	}

	// gets an array of all pizza creators
	// creates them if they don't exist, yet
	// @$b_include_deleted: whether to find the deleted ones, as well
	// @return: an array of pizza creators, in the form
	//     array('name'=>string, '_deleted'=>'0'/'1', 'id'=>int, 'created'=>datetime string, 'deleted'=>datetime string, 'fmod_list_id'=>int)
	public static function get_pizza_creators($b_include_deleted = FALSE) {
		self::echo_function_name();

		global $locationid;

		// try to fetch the creators from the database
		$a_fetch_deleted = ($b_include_deleted ? array() : array('_deleted'=>'0'));
		$a_searchvars = array_merge(array('location'=>$locationid, 'type'=>'setting', 'setting'=>'pizza creator'), $a_fetch_deleted);
		$a_extravars = array('orderbyclause'=>"order by `value2`");
		$a_creators = ConnectionHub::getConn('poslavu')->getDBAPI()->getAllInTable('config', $a_searchvars, FALSE, $a_extravars);

		// create them if they don't already exist
		if (count($a_creators) == 0) {

			// check if the mods exit
			$a_mods_exist = ConnectionHub::getConn('poslavu')->getDBAPI()->getAllInTable('config', array('location'=>$locationid, 'setting'=>'pizza_mods', 'type'=>'', 'value7'=>''), FALSE);
			if (count($a_mods_exist) > 0) {
				$b_mods_exist = TRUE;

				// get the current fmod list id
				$a_fmod_lists = ConnectionHub::getConn('poslavu')->getDBAPI()->getAllInTable('forced_modifier_lists', array('title'=>'Pizza Mod', 'type'=>'custom'), FALSE);
				$i_fmod_list_id = (count($a_fmod_lists) == 1) ? (int)$a_fmod_lists[0]['id'] : -1;

				// mods already exist, create a single "generic" creator for just those mods
				$a_defaults = array(
					array('location'=>$locationid, 'type'=>'setting', 'setting'=>'pizza creator', 'value'=>0, 'value2'=>'Generic Pizza Creator', 'value3'=>'value:id, value2:name, value4:created, value5:deleted, value6:fmod_list_id, value7:creator_id, value8:page_num, value9:dual-column', 'value4'=>date('Y-m-d H:i:s'), 'value5'=>'0000-00-00 00:00:00', '_deleted'=>0, 'value6'=>$i_fmod_list_id)
				);
			} else {
				$b_mods_exist = FALSE;

				// mods don't already exist, create "small," "medium," and "large" creators for new mods
				$a_defaults = array(
					array('location'=>$locationid, 'type'=>'setting', 'setting'=>'pizza creator', 'value'=>1, 'value2'=>'Small Pizza Creator', 'value3'=>'value:id, value2:name, value4:created, value5:deleted, value6:fmod_list_id', 'value4'=>date('Y-m-d H:i:s'), 'value5'=>'0000-00-00 00:00:00', '_deleted'=>0, 'value6'=>-1),
					array('location'=>$locationid, 'type'=>'setting', 'setting'=>'pizza creator', 'value'=>2, 'value2'=>'Medium Pizza Creator', 'value3'=>'value:id, value2:name, value4:created, value5:deleted, value6:fmod_list_id', 'value4'=>date('Y-m-d H:i:s'), 'value5'=>'0000-00-00 00:00:00', '_deleted'=>0, 'value6'=>-1),
					array('location'=>$locationid, 'type'=>'setting', 'setting'=>'pizza creator', 'value'=>3, 'value2'=>'Large Pizza Creator', 'value3'=>'value:id, value2:name, value4:created, value5:deleted, value6:fmod_list_id', 'value4'=>date('Y-m-d H:i:s'), 'value5'=>'0000-00-00 00:00:00', '_deleted'=>0, 'value6'=>-1)
				);
			}

			// insert the creators into the database
			foreach($a_defaults as $a_creator_default) {
				$s_insert_clause = ConnectionHub::getConn('poslavu')->getDBAPI()->arrayToInsertClause($a_creator_default);
				self::lavu_query("INSERT INTO `config` {$s_insert_clause}", $a_creator_default);
			}
			$a_creators = ConnectionHub::getConn('poslavu')->getDBAPI()->getAllInTable('config', $a_searchvars, FALSE, $a_extravars);
		}

		$a_retval = array();
		foreach($a_creators as $a_creator) {
			$a_retval_part = array();
			self::assign_names_to_values($a_creator, $a_retval_part);
			$a_retval[] = array_merge($a_creator,$a_retval_part);
		}
		return $a_retval;
	}

	// given a pizza creator name, it tries to return the id
	// @$s_name: name of the pizza creator
	// @$b_include_deleted: TRUE or FALSE
	// @return: the id, or 0 if the id can't be found
	public static function creator_name_to_id($s_name, $b_include_deleted = FALSE) {
		self::echo_function_name();

		// get the creators
		$a_creators = self::get_pizza_creators($b_include_deleted);
		if (count($a_creators) == 0)
			return 0;

		// find the creator, if the name matches
		foreach($a_creators as $a_creator) {
			if (trim($a_creator['name']) == trim($s_name))
				return (int)$a_creator['id'];
		}

		 return 0;
	}

	// update the old forced modifiers to reflect the new pizza creator settings
	// @$a_creators: the pizza creators in the`config` table of the database
	public static function update_fmods(&$a_creators) {
		self::echo_function_name();

		// update the names for each creator
		foreach($a_creators as $k=>$a_creator) {

			// get some vars
			global $data_name;
			$s_creator_id = $a_creator['id'];
			$s_title = $a_creator['name'];
			$s_list_id = $a_creator['fmod_list_id'];

			// modify the forced_modifer_lists
			$a_update_vars = array('title'=>$s_title, 'type'=>'custom');
			$a_wherevars = array('id'=>$s_list_id);
			$a_trash = NULL;
			$i_insert_update_id = 0;
			$s_query_performed = '';
			$s_result = ConnectionHub::getConn('poslavu')->getDBAPI()->insertOrUpdate('poslavu_'.$data_name.'_db', 'forced_modifier_lists', $a_update_vars, $a_wherevars, $a_trash, $i_insert_update_id, $s_query_performed);
			if ($s_result == 'b' || $i_insert_update_id < 1)
				continue;

			// modify the creators if the fmod list was just inserted
			if (substr($s_result, 0, 1) == 'i') {
				$a_creators[$k]['fmod_list_id'] = $i_insert_update_id;
				$s_query_string = "UPDATE `config` SET `value6`='[list_id]' WHERE `value`='[id]' AND `setting`='pizza creator'";
				$a_query_vars = array('id'=>$s_creator_id, 'list_id'=>$i_insert_update_id);
				self::lavu_query($s_query_string, $a_query_vars);
			}

			// modify the forced_modifiers
			$a_mod_update_vars = array('title'=>$s_title, 'extra'=>'pizza_mod_wrapper', 'extra2'=>'pizza_mod', 'extra3'=>'pizza', 'extra5'=>'0x0x1024x748|70x0x885x748', 'list_id'=>$i_insert_update_id);
			$a_mod_wherevars = array('list_id'=>$i_insert_update_id);
			$s_result = ConnectionHub::getConn('poslavu')->getDBAPI()->insertOrUpdate('poslavu_'.$data_name.'_db', 'forced_modifiers', $a_mod_update_vars, $a_mod_wherevars, $a_trash, $i_insert_update_id, $s_query_performed);
		}

		// mark deleted pizza creator mods as deleted, and live pizza creator mods as live
		$a_all_creators = self::get_pizza_creators(TRUE);
		foreach($a_all_creators as $a_creator) {
			if ($a_creator['_deleted'] != '0') {
				lavu_query("UPDATE `forced_modifier_lists` SET `_deleted`='1' WHERE `id`='[id]' AND `_deleted`='0'", array("id"=>$a_creator['fmod_list_id']));
				lavu_query("UPDATE `forced_modifiers` SET `_deleted`='1' WHERE `list_id`='[id]' AND `_deleted`='0'", array("id"=>$a_creator['fmod_list_id']));
			} else {
				lavu_query("UPDATE `forced_modifier_lists` SET `_deleted`='0' WHERE `id`='[id]' AND `_deleted`!='0'", array("id"=>$a_creator['fmod_list_id']));
				lavu_query("UPDATE `forced_modifiers` SET `_deleted`='0' WHERE `list_id`='[id]' AND `_deleted`!='0'", array("id"=>$a_creator['fmod_list_id']));
			}
		}
	}

	// returns the html to select a pizza creator
	// @return: the selector on success, or empty string "" on failure
	public static function creator_selector_tostr() {
		self::echo_function_name();

		global $locationid;

		// get the pizza creators
		$a_creators = self::get_pizza_creators();

		// for each creator, create a link object
		$a_tabs = array();
		foreach($a_creators as $a_creator) {
			$a_tabs[] = array('name'=>$a_creator['name'], 'submode_name'=>'creator_id', 'submode_value'=>$a_creator['id']);
		}
		$a_tabs[] = array('name'=>'Edit Creators', 'submode_name'=>'edit_creators', 'submode_value'=>'1');

		$a_tabstr = linkObj::tabsTostr($_GET['mode'], $a_tabs);
		if (!$a_tabstr['success'])
			return "";
		$s_menu = $a_tabstr['value'];

		return $s_menu;
	}

	// returns a string representation of the current pizza creator id
	public static function get_current_pizza_creator_id() {
		self::echo_function_name();
		if (!isset($_SESSION[sessname('current_pizza_creator')])) {
			set_sessvar('current_pizza_creator', '0');
		}
		return sessvar('current_pizza_creator');
	}

	// set the current pizza creator id
	// @$s_id: should be a number >= 1, or an empty string
	public static function set_current_pizza_creator_id($s_id) {
		self::echo_function_name();
		set_sessvar('current_pizza_creator', $s_id);
	}

	public static function echo_function_name($index = 0) {
		if ($index == 0) {
			return;
			$index = 1;
		}
		$a_stacktrace = debug_backtrace();
		error_log(".".$a_stacktrace[$index]['function']."\n");
	}

	public static function lavu_query($a, $b = NULL) {
		if ($b === NULL)
			$b = array();
		//error_log(ConnectionHub::getConn('poslavu')->getDBAPI()->drawQuery($a,$b));
		return lavu_query($a, $b);
	}

	public static function insertOrUpdate() {
		self::echo_function_name(2);
		$a_args = func_get_args();
		foreach($a_args as $v) {
			if (is_array($v))
				error_log(print_r($v,TRUE));
			else
				error_log($v);
		}
		$i = 0;
		$a1 = isset($a_args[$i]) ? $a_args[$i] : '';
		$i++;
		$a2 = isset($a_args[$i]) ? $a_args[$i] : '';
		$i++;
		$a3 = isset($a_args[$i]) ? $a_args[$i] : '';
		$i++;
		$a4 = isset($a_args[$i]) ? $a_args[$i] : '';
		$i++;
		$a5 = isset($a_args[$i]) ? $a_args[$i] : '';
		$i++;
		$a6 = isset($a_args[$i]) ? $a_args[$i] : '';
		$i++;
		$a7 = isset($a_args[$i]) ? $a_args[$i] : '';
		$i++;
		$wt_retval = ConnectionHub::getConn('poslavu')->getDBAPI()->insertOrUpdate($a1, $a2, $a3, $a4, $a5, $a6, $a7);
		error_log($a7);
		return $wt_retval;
	}

	//Added by Brian D.  We send pizza information to the ltg server.
	//So we marshall the information whenever there is a save.
	public static function marshall_pizza_info_4_ltg_config(){   // THIS SHOULD PROBABLY BE PUT IN THE MENU AREA.  WHEN MENU IS EVER SAVED.

		global $locationid;

		//Gather Pizza Settings from config.
		$pizzaSettingsResult = lavu_query("SELECT * FROM `config` WHERE `setting`='pizza_mods' || `setting`='pizza creator'");
		$pizza_mods = array(); $pizza_creators = array();
		while($currRow = mysqli_fetch_assoc($pizzaSettingsResult)){
			$currRow['setting'] == 'pizza creator' ? $pizza_creators[] = $currRow : $pizza_mods[] = $currRow;
		}
		require_once(dirname(__FILE__)."/../resources/json.php");
		$pizza_json_info = LavuJson::json_encode( array('pizza_mods' => $pizza_mods, 'pizza_creators' => $pizza_creators) );
		//We now do an insert or update on config field ltg
		$existsResult = lavu_query("SELECT `id` FROM `config` WHERE `setting`='ltg_pizza_info'");
		if(mysqli_num_rows($existsResult)){
			$row = mysqli_fetch_assoc($existsResult);
			$updateResult = lavu_query("UPDATE `config` SET `value`='[1]' WHERE `id`='[2]'",$pizza_json_info,$row['id']);
			return $updateResult ? true : false;
		}else{
			$insertResult = lavu_query("INSERT INTO `config` (`location`,`setting`,`value`,`type`) VALUES ('[1]','ltg_pizza_info','[2]','location_config_setting');", $locationid, $pizza_json_info);
			return $insertResult ? true : false;
		}
	}
}

?>
