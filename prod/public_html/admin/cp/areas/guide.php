<?php

				// Title of tab, initial page .php, number of pages - leave blank for 1 page
				$tablist = array(array("Guide Start","guide_start"),
								array("Business Info","guide_business_info",4),
								array("Inventory","guide_inventory"),
								array("Menu","guide_menu",3),
								array("Printing", "guide_print",2),
								array("Guide Finish", "guide_finish"));
				create_tabs("wizard",$tablist);
				
				if(is_numeric($submode) && $submode > 1) $mode_file = $mode . $submode;
				else {
					$mode_file = $mode;
					$submode = 1;
				}
				
				if($parent_mode && $parent_submode)
				{
					$tmode = $parent_mode;
					$tsubmode = $parent_submode;
				}
				else
				{
					$tmode = $mode;
					$tsubmode = $submode;
				}
				
				$max_submode = 1;
				$tab_index = 0;
				for($i=0; $i<count($tablist); $i++)
				{
					if($tmode==$tablist[$i][1])
					{
						$max_submode = (isset($tablist[$i][2]))?$tablist[$i][2]:1;
						$tab_index = $i;
					}
				}
				
				if($max_submode > $tsubmode)
					$next_link = "index.php?mode=assistance_{$tmode}&submode=".($tsubmode + 1);
				else if($tab_index < count($tablist) - 1)
					$next_link = "index.php?mode=assistance_".$tablist[$tab_index + 1][1];
				else
					$next_link = false;
				
				if($parent_mode && $parent_submode)
					$prev_link = "index.php?mode=assistance_$parent_mode&submode={$parent_submode}";
				else if($tsubmode > 1)
					$prev_link = "index.php?mode=assistance_{$tmode}&submode=".($tsubmode - 1);
				else if($tab_index > 0)
					$prev_link = "index.php?mode=assistance_".$tablist[$tab_index - 1][1];
				else
					$prev_link = false;
				
				if(is_file("areas/guide/".$mode_file.".php"))
					require_once("areas/guide/".$mode_file.".php");
				$guide_content = ob_get_contents();
				ob_end_clean();
				
				$guide_content = str_replace("index.php?mode=assistance_guide_exercise","index.php?parent_mode=".$mode."&parent_submode=".$submode."&mode=guide_exercise",$guide_content);
				$split_strings = array("lsetting","lcheckbox","lradio");
				$stag_found = false;
				for($m=0; $m<count($split_strings); $m++)
				{
					if(strpos($guide_content,"[".$split_strings[$m].":")!==false)
						$stag_found = true;
				}
				if($stag_found)
				{
					$set_guide_content = $guide_content;
					$lsetting_list = array();
					for($n=0; $n<count($split_strings); $n++)
					{
						$ltype = $split_strings[$n];
						$cparts = explode("[".$ltype.":",$set_guide_content);
						$set_guide_content = $cparts[0];
						for($i=1; $i<count($cparts); $i++)
						{
							if(strpos($cparts[$i],"]")===false)
							{
								$set_guide_content .= $cparts[$i];
							}
							else
							{
								$tag_end = strpos($cparts[$i],"]");
								$tag_parts = explode(":",substr($cparts[$i],0,$tag_end));
								$tag_name = $tag_parts[0];
								if(count($tag_parts) > 1)
									$tag_details = $tag_parts[1];
								else
									$tag_details = "";
								
								if($ltype=="lcheckbox")
								{
									if($location_info[$tag_name]=="1")
										$set_guide_content .= "checked";
								}
								else if($ltype=="lradio")
								{
									if($location_info[$tag_name]==$tag_details)
										$set_guide_content .= "checked";
								}
								else
								{
									$set_guide_content .= $location_info[$tag_name];
								}
								
								$lsetting_list[] = array($tag_name,$ltype);
								$set_guide_content .= substr($cparts[$i],$tag_end + 1);
							}
						}
					}
					if(isset($_POST['posted']))
					{
						
						$update_vars = "";
						$update_vals = array();
						$update_vals['id'] = $locationid;
						for($i=0; $i<count($lsetting_list); $i++)
						{
							$lname = $lsetting_list[$i][0];
							$ltype = $lsetting_list[$i][1];
							if($update_vars!="") $update_vars .= ",";
							$update_vars .= "`".$lsetting_list[$i][0]."`='[val_".$i."]'";
							if($ltype=="lcheckbox")
							{
								if(isset($_POST[$lsetting_list[$i][0]]))
									$setval = "1";
								else
									$setval = "0";
							}
							else
							{
								$setval = $_POST[$lsetting_list[$i][0]];
							}
							$update_vals['val_' . $i] = $setval;
							//echo "$lname:$ltype = " . $setval . "<br>";
						}
						lavu_query("update `locations` set $update_vars where `id`='[id]'",$update_vals);
						echo "<script language='javascript'>";
						echo "window.location.replace('$next_link'); ";
						echo "</script>";
						exit();
					}
					$guide_content = "<form name='guide_form' method='post' action='index.php?mode=assistance_{$mode}&submode={$submode}'>";
					$guide_content .= "<input type='hidden' name='posted' value='1'>";
					$guide_content .= $set_guide_content;
					$guide_content .= "</form>";
					$atag_props = "style='cursor:pointer' onclick='document.guide_form.submit()'";
				}
				else
				{
					$atag_props = "href='$next_link'";
				}
				
				ob_start();
				echo $guide_content;
				echo "<table>";
				echo "<tr>";
				if($prev_link)
				{
					echo "<td align='center'><a href='$prev_link'><div class='contBtn'>Back</div></a></td>";
				}
				if($next_link)
				{
					echo "<td colspan='2'><table width='460' height='46' border='0' cellspacing='0' cellpadding='3' background='images/arrow_bg_long.png'><tr>";
					echo "<td align='center'>Click here to continue with your set up</td>";
					echo "<td width='80' align='right'><a ".$atag_props."><div class='contBtn'>Continue</div></a></td>";
					echo "</tr></table></td>";
				}			
				echo "</tr>";
				echo "</table>";
				
				
				
function create_tabs()
	{
		global $mode;
		global $submode;
		global $parent_mode;
		global $parent_submode;
		
		if($parent_mode && $parent_submode)
		{
			$tmode = $parent_mode;
			$tsubmode = $parent_submode;
		}
		else
		{
			$tmode = $mode;
			$tsubmode = $submode;
		}
				
		$top = "";
		$mid = "";
		$btm = "";
		
		$subtabs = 0;
		$exercise_subtab = false;
		$selected_column = 0;
		$show_progress_bar = false;
		$tabs = func_get_args();
		if ((count($tabs) == 1) && strstr($tabs[0], "|*|")) {
			$tabs = explode("|*|", $tabs[0]);
		} else if(count($tabs) > 1 && ($tabs[0]=="array" || $tabs[0]=="wizard"))
		{
			if($tabs[0]=="wizard")
				$show_progress_bar = true;
			$tabs = $tabs[1];
		}
		
		$mid .= "<tr>";
		$tabcount = 0;
		$maxtabcols = 6;
		if(count($tabs)==7) $maxtabcols = 4;
		for($i=0; $i<count($tabs); $i++)
		{
			$tabcount++;
			$tab_subtabs = 0;
			
			if ($tabs[$i] === NULL)
			{
				continue;
			}
			else if($tabcount > $maxtabcols)
			{
				$tabcount = 1;
				$mid .= "</tr><tr>";
			}
			if(is_array($tabs[$i]))
			{
				$tab_title = $tabs[$i][0];
				$tab_mode = $tabs[$i][1];
				if(isset($tabs[$i][2]))
				{
					$tab_subtabs = $tabs[$i][2];
				}
			}
			else
			{
				$tab_title = $tabs[$i];
				$tab_mode = strtolower(str_replace(array(" - "," "),array("_","_"),$tab_title));
			}
			$border_color = "#999999";
			$bgcolor = "#cdcdcd";
			$extra_code = "onclick='window.location = \"index.php?mode=assistance_{$tab_mode}\"'";
			if($tmode==$tab_mode) 
			{
				$bgcolor = "#eeeeee";
				$subtabs = $tab_subtabs;
				$selected_column = $tabcount;
				if($parent_mode==$tab_mode && $subtabs<1)
				{
					$exercise_subtab = true;
				}
			}
			$mid .= "<td style='cursor:pointer; border:solid 1px $border_color' align='center' bgcolor='$bgcolor' $extra_code>".speak($tab_title)."</td>";
		}
		$mid .= "</tr>";
		
		if($subtabs > 0)
		{
			$btm .= "<tr>";
			for($i=1; $i<$selected_column; $i++)
			{
				$btm .= "<td>&nbsp;</td>";
			}
			$btm .= "<td>";
			$btm .= "<table>";
			for($i=1; $i<=$subtabs; $i++)
			{
				$subtab_title = $i;
				$border_color = "#999999";
				$bgcolor = "#cdcdcd";
				$extra_code = "onclick='window.location = \"index.php?mode=assistance_{$tmode}&submode={$i}\"'";
				$exercise_code = "";
				if($tsubmode==$i) 
				{
					$bgcolor = "#eeeeee";
					if($parent_submode==$i)
					{
						$exercise_code = "<td style='cursor:pointer; padding-left:6px; padding-right:6px; border:solid 1px $border_color' bgcolor='$bgcolor'>".speak("Exercise")."</td>";
					}
				}
				$btm .= "<td style='cursor:pointer; padding-left:6px; padding-right:6px; border:solid 1px $border_color' bgcolor='$bgcolor' $extra_code>".speak($subtab_title)."</td>" . $exercise_code;
			}
			$btm .= "</table>";
			$btm .= "</td>";
			$btm .= "</tr>";
		}
		if($exercise_subtab)
		{
			$btm .= "<tr>";
			for($i=1; $i<$selected_column; $i++)
			{
				$btm .= "<td>&nbsp;</td>";
			}
			$btm .= "<td>";
			$btm .= "<table>";
			
			$btm .= "<td style='cursor:pointer; padding-left:6px; padding-right:6px; border:solid 1px #999999' bgcolor='#eeeeee'>".speak("Exercise")."</td>";
			
			$btm .= "</table>";
			$btm .= "</td>";
			$btm .= "</tr>";
		}
		
		echo "<table width='100%'><tr><td align='center'><table cellspacing=2 cellpadding=4>";
		echo $top;
		echo $mid;
		echo $btm;
		echo "</table></td></tr></table>";
	}

?>