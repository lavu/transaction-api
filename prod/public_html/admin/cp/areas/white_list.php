<?php

//Add public IPs you want to be ignored when enforcing login policies

$whiteList = array(
  "135.84.56.43"=>"Miami CIC 02/13/2017",
  "67.134.3.50"=>"Albuquerque 02/13/2017",
  "127.0.0.1"=>"Local host ip 4 02/13/2017",
  "::1"=>"Local host ip 6 02/13/2017",
  "10.9.0.104"=>"VPN 02/13/2017"
);

function reserved_ip($ip)
{
    $reserved_ips = array( // not an exhaustive list
    '167772160'  => 184549375,  /*    10.0.0.0 -  10.255.255.255 */
    '3232235520' => 3232301055, /* 192.168.0.0 - 192.168.255.255 */
    '2130706432' => 2147483647, /*   127.0.0.0 - 127.255.255.255 */
    '2851995648' => 2852061183, /* 169.254.0.0 - 169.254.255.255 */
    '2886729728' => 2887778303, /*  172.16.0.0 -  172.31.255.255 */
    '3758096384' => 4026531839, /*   224.0.0.0 - 239.255.255.255 */
    );

    $ip_long = sprintf('%u', ip2long($ip));

    foreach ($reserved_ips as $ip_start => $ip_end)
    {
        if (($ip_long >= $ip_start) && ($ip_long <= $ip_end))
        {
            return TRUE;
        }
    }
    return FALSE;
}

$ipaddress = (isset($_SERVER['HTTP_X_FORWARDED_FOR']))?$_SERVER['HTTP_X_FORWARDED_FOR']:$_SERVER['REMOTE_ADDR'];
$isLocalIP = reserved_ip($_SERVER['REMOTE_ADDR']);
