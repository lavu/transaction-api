<?php
	//ini_set("display_errors",1);
	require_once dirname( dirname(__FILE__) ) . '/reports_v2/report_functions.php';
	require_once dirname( dirname(__FILE__) ) . '/reports_v2/area_reports.php';
	require_once(dirname(__FILE__)."/../resources/messagesDB.php");
	require_once(dirname(__FILE__) . "/survey.php");
	// SHOP.LAVU.COM pop up
	// require_once(dirname(__FILE__)."/shopPopup.php");
	// Inventory 2.0 pop up
		require_once(dirname(__FILE__)."/inventoryPopup.php");

	$ms = array();
	$ms['comma_char'] = $location_info['decimal_char'] !== '.'?'.':',';
	$ms['decimal_char'] = $location_info['decimal_char'];
	$ms['decimal_places'] = $location_info['disable_decimal'];
	$ms['monitary_symbol'] = $location_info['monitary_symbol'];
	$ms['left_or_right'] = $location_info['left_or_right'];
	$zone = $location_info['timezone'];

	$border_color_grey = "#b6b6b6";
	$header_color_white = "#ffffff";
	$header_color_green_dark = "#77875b";//"#77aa66";
	$header_color_blue_dark = "#7889ac";

	$header_color_green = "#99bc08";//"#97b74b";//"#77aa66";
	$header_color_light_green = "#aecd37";
	$header_color_mint = "#0eae95";
	$header_color_dark_mint = "#108972";
	$header_color_purple = "#5053a2";
	$header_color_dark_purple = "#3c4287";
	$header_color_light_orange = "#f68e20";
	$header_color_dark_orange = "#d36b0d";
	$header_color_gold = "#c9aa2b";

	$header_color_blue = "#3382a9";//#8899cc";

	prepare_floating_window();

	function prepare_floating_window()
	{
		echo "
		<div style='position:absolute; left:0px; top:0px; width:100%; height:100%; z-index:1000; visibility:hidden' id='floating_window'>
			<table width='100%'>
				<tr>
					<td align='center'>
						<table style='border:solid 2px #777777' cellpadding=6 cellspacing=0 bgcolor='#ffffff'>
							<tr>
								<td align='right' bgcolor='#eeeeee'>
									<table cellspacing=0 cellpadding=0 width='100%'>
										<tr>
											<td width='10%' align='left'>&nbsp;</td>
											<td width='80%' align='center' style='font-weight:bold; color:#aaaaaa; font-size:14px' id='floating_window_title'>&nbsp;</td>
											<td width='10%' align='right'>
												<b>
													<font style='color:#aaaaaa; font-size:20px; cursor:pointer' onclick='close_floating_window()'>X</font>
												</b>
											</td>
										</tr>
									</table>
								</td>
							</tr>
							<tr>
								<td width='480' height='320' id='floating_window_content' align='center' valign='top'>
									&nbsp;
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</div>

		<script language='javascript'>
		function open_floating_area(load_area,load_vars,set_title) {
			if(!set_title) set_title = '&nbsp;';
			document.getElementById('floating_window_title').innerHTML = set_title;
			document.getElementById('floating_window_content').innerHTML = '<table width=\"100%\" height=\"100%\"><tr><td align=\"center\" valign=\"center\"><img src=\"http://hotel.lavuhotel.comimages/animated-progress.gif\" /></td></tr></table>';
			document.getElementById('floating_window').style.visibility = 'visible';
			runGenericAjaxRequestDennedyStyle(load_area,load_vars,'floating_window_content');
		}
		function open_floating_window(set_content,set_title) {
			document.getElementById('floating_window_title').innerHTML = set_title;
			if(set_content.substring(0,8)=='from_id:') { set_content = document.getElementById(set_content.substring(8)).innerHTML; set_content = set_content.replace(/iidd/ig,'id');}
			document.getElementById('floating_window_content').innerHTML = set_content;
			document.getElementById('floating_window').style.visibility = 'visible';
		}
		function close_floating_window() {
			document.getElementById('floating_window_content').innerHTML = '';
			document.getElementById('floating_window').style.visibility = 'hidden';
		}
		function find_and_eval_script_tags(estr,script_start,script_end) {
			var xhtml = estr.split(script_start);
			for(var n=1; n<xhtml.length; n++)
			{
				var xhtml2 = xhtml[n].split(script_end);
				if(xhtml2.length > 1)
				{
					run_xhtml = xhtml2[0];
					eval(run_xhtml);
				}
			}
		}
		</script>
";
	}

	function getSalesOnDay($date,$date_range=1){
		global $location_info;
		$locid= $location_info['id'];
		$mins= ($location_info['day_start_end_time']*1)%100;
		$hours= (($location_info['day_start_end_time']*1)-$mins)/100;
		$dayStart = date("H:i:s",mktime($hours, $mins, 0,1,1,2000));
		$end  = date("Y-m-d",strtotime("+".$date_range." day")). " ". $dayStart;
		$start= $date. " ". $dayStart;
		$query= lavu_query("select `cc_transactions`.`loc_id` AS `field2`,`cc_transactions`.`card_type` AS `field3`,`orders`.`void` AS `field4`,`cc_transactions`.`amount` AS `field5`,`cc_transactions`.`voided` AS `field6`,`orders`.`location_id` AS `field7`,`cc_transactions`.`got_response` AS `field8`,`cc_transactions`.`transtype` AS `field9`,`cc_transactions`.`order_id` AS `field10`,`orders`.`order_id` AS `field11`,IF(`voided`='1',0,IF(`action`='Refund', (0 - sum(`cc_transactions`.`amount`)), sum(`cc_transactions`.`total_collected`))) AS `field12`,`cc_transactions`.`pay_type` AS `field13`,IF(`cc_transactions`.`order_id`='Paid Out','Paid Out',IF(`cc_transactions`.`order_id`='Paid In','Paid In',`cc_transactions`.`action`)) AS `field14` from `cc_transactions` LEFT JOIN `orders` ON `cc_transactions`.`order_id` = `orders`.`order_id`where `cc_transactions`.`datetime` >= '[1]' and `cc_transactions`.`datetime` <= '[2]' and `cc_transactions`.`loc_id` = '[3]' and `cc_transactions`.`voided` = 0 and `orders`.`location_id` = `cc_transactions`.`loc_id` and `cc_transactions`.`transtype` != 'AddTip' and (`cc_transactions`.`action` = 'Sale' OR `cc_transactions`.`action` = 'Refund') group by concat(`cc_transactions`.`pay_type`,' ',IF(`action`='Refund', 'Refund', ''),IF(`cc_transactions`.`order_id` LIKE 'Paid%','PaidInOut',''))", $start, $end, $locid);
		$total=0;
		while($read = mysqli_fetch_assoc($query)){
			if( $read['field13']=='Cash' && $read['field14']=='Sale'){
				$total+= $read['field12'];
			}else if($read['field13']=='Card' && $read['field14']=='Sale'){
				$total+= $read['field12'];
			}
		}
		return $total;
	}

	function output_todays_sales($skey="",$is_money=true)
	{
		global $remember_total_sales_for_day;
		global $remember_day_sales_info;

		if($skey!="")
		{
			if(isset($remember_day_sales_info[$skey]))
			{
				$val = ($remember_day_sales_info[$skey]);
			}
			else
			{
				$val = 0;
			}
			if($is_money)
				return home_print_money($val);
			else
				return $val;
		}

		if(isset($remember_total_sales_for_day))
		{
			return home_print_money($remember_total_sales_for_day);
		}
		else
		{
			$today = date('Y-m-d');
			$sales = getSalesOnDay($today);
			return home_print_money($sales);
		}
	}

	function home_print_money($amount,$medsize=0,$smallsize=0) {
		global $location_info;

		$dec_places = $location_info['disable_decimal'];
		$str = "";
		$extra_str = "";

		if($amount * 1 > 100000) { $amount = $amount / 1000; $extra_str = "k"; $amount = floor($amount); $dec_places = 0; }
		if($amount * 1 > 10000) { $amount = floor($amount); $dec_places = 0; }

		if ($location_info['left_or_right'] == 'left')
		{
			$str = $location_info['monitary_symbol'].number_format($amount, $dec_places, $location_info['decimal_char'], ',');
		}else{
			$str = number_format($amount, $dec_places, $location_info['decimal_char'], ',').$location_info['monitary_symbol'];
		}

		$outputstr = $str . $extra_str;

		return $outputstr;
	}

	function output_shortcuts($style_type="vertical")
	{
		$mouseover_color = "#ddeeff";
		//$mouseover_color = "#d7e79b";
		$shortcuts = array("Schedule","End of Day","Timecards","Sales","Payments");
		$links = array("?mode=settings_scheduling","?mode=reports_reports_v2&report=33","?mode=reports_time_cards","?mode=reports_reports_v2&report=31","?mode=reports_reports_v2&report=30");
		//$imgs = array("images/schedule_icon.png","images/endofday_icon.png","images/timecard_icon.png","images/salesreports_icon.png","images/payments_icon.png");
		$imgs = array("images/shortcuts/Schedule-Icon.png","images/shortcuts/EndOfDaySales.png","images/shortcuts/TImeSheetsIcon.png","images/shortcuts/SalesIcon.png","images/shortcuts/PaymentsIcon.png");

		if($style_type!="vertical") $vertical = false;
		if($vertical)
		{
			$cellwidth = "100%";
			$cellheight = "120";
		}
		else
		{
			$cellwidth = "148";
			$cellheight = "90";
		}
		$imgwidth = "50";

		$str = "";
		$str .= "<table>";
		for($i=0; $i<5; $i++)
		{
			$shortcut_title = (isset($shortcuts[$i]))?$shortcuts[$i]:"";
			$shortcut_link = (isset($links[$i]))?$links[$i]:"";
			$shortcut_img = (isset($imgs[$i]))?$imgs[$i]:"";

			if($shortcut_title!="")
			{
				$extra_style = "cursor:pointer";
				$output_img = "<img src='$shortcut_img' border='0' width='$imgwidth' />";
				$control_str = " onmouseover='this.bgColor = \"$mouseover_color\"' onmouseout='this.bgColor = \"#ffffff\"' onclick='window.location = \"$shortcut_link\"'";
			}
			else
			{
				$extra_style = "";
				$output_img = "";
				$control_str = "";
			}

			if($vertical) $str .= "<tr>";
			$str .= "<td width='$cellwidth' height='$cellheight' style='$extra_style' align='center' valign='middle'".$control_str.">";
			$str .= "$output_img<br><font style='color:#888888; font-size:16px'>$shortcut_title</font>";
			$str .= "</td>";
			if($vertical) $str .= "</tr>";
		}
		$str .= "</table>";
		return $str;
	}

	function output_home_page_messages($maxMessages=10)
	{
		$str = "";
		$fromCP=true;

		$messagesDB = new messagesDB();
		$messages = $messagesDB->getMessages();
		//$maxMessages = 6;
		if(count($messages) < $maxMessages){
			$maxMessages = count($messages);
		}
		if(admin_info('username')!= 'poslavu_admin'){
			$result=mysqli_fetch_assoc(lavu_query("SELECT `viewed_messages` FROM `users` WHERE `username`='[1]' ",admin_info('username')));
			$viewed_messages_arr= explode(",",$result['viewed_messages']);
		}

		$str .= "<table cellpadding=16 cellspacing=0>";
		for ($x = 0; $x < $maxMessages; $x++) {
			$message = $messages[$x];
			$type 	 = $message['type'];
			$title 	 = $message['title'];
			$content = $message['content'];
			$id		 = $message['id'];
			$new_m   = false;
			if(admin_info('username')!= 'poslavu_admin' && is_array($viewed_messages_arr) && !in_array($id, $viewed_messages_arr))
			{
				$unread_message_counter++;
				$new_m=true;
			}
			$contentTrimmed = strip_tags(substr($content, 0, 50).'…');
			$imgSrc = '';
			switch($type){
				case 1:
					$imgSrc = 'smallMailIconColor';
					break;
				case 2:
					$imgSrc = 'smallGearIconDarkGray';
					break;
				case 3:
					$imgSrc = 'smallAmbulanceIcon';
					break;
				case 4:
					$imgSrc = 'smallBugIconColor';
					break;
				default:
					$imgSrc = 'smallMailIconColor';
			}
			$extrastyle = "border-bottom:solid 1px #e7eaec";
			if($x >= $maxMessages - 1) $extrastyle = "";

			$inner_container_id = "message_content_$x";
			//$onclick = "onclick='document.getElementById(\"$inner_container_id\").style.display = \"block\"'";
			$onclick = "onclick='open_floating_window(\"from_id:$inner_container_id\",\"$title\")'";
			$control_str = " onmouseover='this.bgColor = \"#d7e79b\"' onmouseout='this.bgColor = \"#ffffff\"' " . $onclick;

			$show_date = "&nbsp;";
			$cd_parts = explode(" ",$message['created_date']);
			$cd_parts = explode("-",$cd_parts[0]);
			if(count($cd_parts) > 2)
			{
				$cd_year = $cd_parts[0];
				$cd_month = $cd_parts[1];
				$cd_day = $cd_parts[2];
				$cd_ts = mktime(0,0,0,$cd_month,$cd_day,$cd_year);
				$days_ago = floor((time() - $cd_ts) / 60 / 60 / 24);

				if($days_ago==0) $show_date = "Today";
				else if($days_ago > 0 && $days_ago < 6) $show_date = date("l",$cd_ts);
				else if(date("Y",$cd_ts)!=date("Y")) $show_date = date("m/d/Y",$cd_ts);
				else $show_date = date("M d",$cd_ts);
			}

			$str .= "<tr style='cursor:pointer'".$control_str.">";
			//$str .= "<td valign='top' align='right' style='font-family:Verdana; color:#3382a9; font-size:10px; $extrastyle'><nobr>$show_date</nobr></td>";
			$str .= "<td style='$extrastyle' valign='middle'>";//valign='top'>";
			$str .= "<img border=0 class=\"messagesIcon\" src=\"images/{$imgSrc}.png\" width='30'/>";
			$str .= "</td>";

			$str .= "<td valign='top' style='font-family:Verdana; color:#888888; $extrastyle'>";
			$str .= "<font style='font-size:14px'><nobr>$title</nobr></font><br><font style='color:#bbbbbb'>" . $show_date . "</font>";
			$str .= "<div id='$inner_container_id' style='display:none'>";
			$str .= "<table><tr><td valign='top' align='left'>" . urldecode($content) . "</td></tr></table>";
			$str .= "</div>";
			$str .=  "</td></tr>";
		}
		$str .= "</table>";
		return $str;
	}

	function getSalesForYear(){
		global $location_info;
		$locid= $location_info['id'];
		$endDate = date("Y-m-d", strtotime("+1 day", strtotime(date('Y-m-d'))));
		$startDate= date("Y-m-01",strtotime("-366 days", strtotime(date( 'Y-m-d'))));
		$mins= ($location_info['day_start_end_time']*1)%100;
		$hours= (($location_info['day_start_end_time']*1)-$mins)/100;
		$dayStart = date("H:i:s",mktime($hours, $mins, 0,1,1,2000));
		$end  = $endDate. " ". $dayStart;
		$start= $startDate. " ". $dayStart;


		$payments_query = str_replace("\n", "", "SELECT IF( date_format( now(), \"%y\") = date_format(`cc_transactions`.`datetime`,'%y'), date_format(`cc_transactions`.`datetime`, '%b'), date_format(`cc_transactions`.`datetime`, '%b \'%y') ) as `interval`,
		`payment_types`.`type` as `payment_type`,
		COUNT(cc_transactions.id ) as `count`,
		sum( IF(`cc_transactions`.`action`='Refund', -1*`cc_transactions`.`total_collected`, `cc_transactions`.`total_collected`) ) as `total_payment`
	FROM `cc_transactions`
	LEFT JOIN `payment_types` ON `cc_transactions`.`pay_type_id` = `payment_types`.`id` AND
		(`cc_transactions`.`loc_id` = `payment_types`.`loc_id` OR '0' = `payment_types`.`loc_id` )
	WHERE
		`cc_transactions`.`datetime` >= '[1]' AND
		`cc_transactions`.`datetime` <= '[2]' AND
		`cc_transactions`.`voided` = '0' AND
		`cc_transactions`.`action` IN ('Sale', 'Refund') AND
		`cc_transactions`.`action` != 'Void' AND
		`cc_transactions`.`loc_id` ='[3]'
	GROUP BY
		date_format(`cc_transactions`.`datetime`,'%Y-%m'),
		`payment_types`.`type`
	ORDER BY
	 date_format(`cc_transactions`.`datetime`,'%Y-%m') ASC");
		if( ($payment_result = lavu_query( $payments_query, $start, $end, $locid ) ) !== FALSE ) {
			if (mysqli_num_rows($payment_result) < 1) return "empty";
			$payment_arr = array();
			$intervals = array();
			$pay_types = array();
			while( $row = mysqli_fetch_assoc($payment_result) ){
				$payment_arr[] = $row;
				$intervals[] = $row['interval'];
				$pay_types[] = $row['payment_type'];
			}
			mysqli_free_result($payment_result);
			$tmp_intervals = array_values(array_unique($intervals));
			$pay_types = array_values(array_unique($pay_types));
			$year_num = " '" . substr(  ((int)date('Y') - 1), -2);
			$all_months = array('Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec');
			foreach($all_months as $k => $v) $all_months[$k] = $v . $year_num;
			$all_months = array_merge($all_months, array('Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'));
			$max_month = array_search($tmp_intervals[count($tmp_intervals)-1], $all_months) + 1;
			$min_month = array_search($tmp_intervals[0], $all_months);
			// echo '<pre style="text-align: left;">' . print_r( $all_months, true ) . '</pre>';

			$intervals = array();

			if( is_numeric($min_month) AND is_numeric($max_month) AND ($max_month > $min_month) AND ($max_month - $min_month < 50) ){
				for($i = $min_month; $i <= $max_month; $i++) {
					$intervals[] = $all_months[$i];
				}
			}
			// $intervals = $tmp_intervals; //if reverting to old behavior is desired.
			//dates
			//pay_types

			$result = array();
			$result['labels'] = $intervals;
			$result['datasets'] = array();

			for( $j = 0; $j < count($pay_types); $j++ ){
				$data = array();
				$data['label'] = $pay_types[$j];
				$data['data'] = array();
				for( $i = 0; $i < count($intervals); $i++ ){
					$success = false;
					foreach( $payment_arr as $k => $v ) {
						if( $v['payment_type'] == $pay_types[$j] ) {
							if( $v['interval'] == $intervals[$i] ){
								$success = true;
								$data['data'][] = $v['total_payment'];
								break;
							}
						}
					}
					if(!$success){
						$data['data'][] = 0;
					}
				}
				$result['datasets'][] = $data;
			}

			if(count($result['datasets']) < 1)
			{
				return "empty";
			}

			return json_encode($result);
		} else {
			return '{ error: "' . lavu_dberror() . '" }';
		}
	}

	function report_format_money($m)
	{
		global $ms;
		$mstr = number_format(str_replace(",","",$m),$ms['decimal_places'],$ms['decimal_char'],$ms['comma_char']);
		if($ms['left_or_right']=="right") return $mstr . $ms['monitary_symbol'];
		else return $ms['monitary_symbol'] . $mstr;
	}

	function parse_report_lavu_query_string($query)
	{
		global $location_info;
		$conn = ConnectionHub::getConn('rest');
		$rep_loc_id= $location_info['id'];
		$se_mins= ($location_info['day_start_end_time']*1)%100;
		$se_hours= (($location_info['day_start_end_time']*1)-$se_mins)/100;
		//$rep_loc_id = "1";
		$rep_start = date("Y-m-d H:i:s",mktime($se_hours,$se_mins,0,date("m"),date("d"),date("Y")));
		$rep_end = date("Y-m-d H:i:s",mktime($se_hours,$se_mins,0,date("m"),date("d")+1,date("Y")));

		$query = str_replace("[start_datetime]",$conn->escapeString($rep_start),$query);
		$query = str_replace("[end_datetime]",$conn->escapeString($rep_end),$query);
		$query = str_replace("[loc_id]",$conn->escapeString($rep_loc_id),$query);

		return $query;
	}

	function run_report_quick_sum($col,$query,$repeatn=1)
	{
		$total_value = 0;
		$query = parse_report_lavu_query_string($query);

		for($n=0; $n<$repeatn; $n++)
		{
			$run_query = $query;
			$run_query = str_replace("[n]",($n + 1),$run_query);

			$set_value = 0;
			$rep_query = lavu_query($run_query);
			if(mysqli_num_rows($rep_query))
			{
				$rep_read = mysqli_fetch_assoc($rep_query);
				$set_value = $rep_read[$col];
				$set_value = str_replace(",","",str_replace("[money]","",$set_value));
			}
			$total_value += $set_value * 1;
		}

		return report_format_money($total_value);//"$" . number_format(str_replace(",","",$total_value),2);
	}

	function report_add_sums($n1,$n2,$n3=0,$n4=0)
	{
		$n1 = report_money_to_number($n1);//str_replace(",","",$n1) * 1;
		$n2 = report_money_to_number($n2);//str_replace(",","",$n2) * 1;
		$n3 = report_money_to_number($n3);//str_replace(",","",$n3) * 1;
		$n4 = report_money_to_number($n4);//str_replace(",","",$n4) * 1;
		return report_format_money($n1 + $n2 + $n3 + $n4);//"$" . number_format($n1 + $n2 + $n3 + $n4,2);
	}

	$remember_total_sales_for_day = 0;
	$remember_day_sales_info = array();
	function getSalesForDay($return_mode="json")
	{
		global $remember_total_sales_for_day;
		global $remember_day_sales_info;

		$query = "select menu_categories.name as category, sum(order_contents.quantity) as quantity, if(count(distinct orders.ioid )>count( distinct orders.order_id ),count( distinct orders.ioid ),count( distinct orders.order_id )) as order_count, if(count(*) > 0, concat('[money]',format(sum((order_contents.price + order_contents.modify_price + order_contents.forced_modifiers_price)*order_contents.quantity - if( order_contents.quantity > '0', order_contents.itax, 0) ),2)), '[money]0.00') as gross, if(count(*) > 0, concat('[money]',format(sum(if(order_contents.quantity > 0, order_contents.discount_amount + order_contents.idiscount_amount, 0)),2)), '[money]0.00') as discount, if(count(*) > 0, concat('[money]',format(sum((order_contents.price + order_contents.modify_price + order_contents.forced_modifiers_price)*order_contents.quantity - if( order_contents.quantity > '0',order_contents.discount_amount + order_contents.idiscount_amount + order_contents.itax, 0)),2)), '[money]0.00') as net, if(count(*) > 0, concat('[money]',format(sum( if( order_contents.quantity != 0 AND order_contents.tax_exempt != '1', `order_contents`.`tax1` + `order_contents`.`tax2` + `order_contents`.`tax3` + `order_contents`.`tax4` + `order_contents`.`tax5` - `order_contents`.`itax`, 0 ) ),2)), '[money]0.00') as order_tax, if(count(*) > 0, concat('[money]',format(sum( if( order_contents.quantity != 0 AND order_contents.tax_exempt != '1', `order_contents`.`itax`, 0) ),2)), '[money]0.00') as included_tax, `menu_categories`.`name` as `group_col_menu_categories_name` from `orders`LEFT JOIN `order_contents` ON `orders`.`order_id` = `order_contents`.`order_id` AND `orders`.`location_id` = `order_contents`.`loc_id` LEFT JOIN `menu_items` ON `order_contents`.`item_id` = `menu_items`.`id` LEFT JOIN `menu_categories` ON `menu_items`.`category_id` = `menu_categories`.`id` where orders.closed >= '[start_datetime]' and orders.closed < '[end_datetime]' and orders.void = '0' and orders.location_id='[loc_id]' and order_contents.item != 'SENDPOINT' group by `category`";
		$query = parse_report_lavu_query_string($query);
		$sales_query = lavu_query($query);

		$method = "datasets";
		$salesarr = array();
		$calc_total = 0;
		if($method=="datasets")
		{
			$salesarr['datasets'] = array();
			while($sales_read = mysqli_fetch_assoc($sales_query))
			{
				$dataobj = array();
				$dataobj['label'] = $sales_read['category'];
				$dataobj['data'] = array();
				$dataobj['data'][] = str_replace("[money]","",str_replace(",","",$sales_read['gross']));
				$salesarr['datasets'][] = $dataobj;
				$calc_total = $calc_total + str_replace("[money]","",str_replace(",","",$sales_read['gross'])) * 1;
				foreach($sales_read as $skey => $sval)
				{
					$sval = str_replace(",","",$sval);
					if(substr($sval,0,7)=="[money]" || is_numeric($sval))
					{
						if(substr($sval,0,7)=="[money]")
							$sval = substr($sval,7);
						if(is_numeric($sval))
						{
							if(!isset($remember_day_sales_info[$skey])) $remember_day_sales_info[$skey] = 0;
							$remember_day_sales_info[$skey] += $sval * 1;
						}
					}
				}
			}
			if(count($salesarr['datasets']) < 1)
			{
				$dataobj = array();
				$dataobj['label'] = "No Sales";
				$dataobj['data'] = array();
				if(isset($_GET['showmenu']))
					$dataobj['data'][] = "10.01";
				else $dataobj['data'][] = "0.00000000001";
				$salesarr['datasets'][] = $dataobj;
			}
		}
		else
		{
			while($sales_read = mysqli_fetch_assoc($sales_query))
			{
				$salesarr[] = str_replace("[money]","",str_replace(",","",$sales_read['gross']));
				$calc_total = $calc_total + str_replace("[money]","",str_replace(",","",$sales_read['gross'])) * 1;
			}

			if(count($salesarr) < 1)
			{
				$salesarr[] = 1;
			}
		}
		if($calc_total > 0)
		{
			$remember_total_sales_for_day = $calc_total;
		}

		if($return_mode=="array") return $salesarr;
		if(isset($_GET['jsl'])) echo json_encode($salesarr) . "<br>";
		return json_encode($salesarr);
	}

	function create_cpgraph_code($graph_name,$graph_type,$graph_data, $width, $height, $extra_code="", $extra_canvas_code="")
	{
		$htmlstr = "";
		$htmlstr .= "<div class=\"cpdash-$graph_name\"><div style='position:relative;'>";
		$htmlstr .= "	&nbsp;";
		$htmlstr .= "	<div style='positon:absolute; top:0px; left:0px; z-index:400'>";
		$htmlstr .= "		<canvas id=\"c_summary-$graph_name\" data=\"$graph_data\" width=\"$width\" height=\"$height\" style=\"width: ".$width."px; height: ".$height."px;\"".$extra_canvas_code."></canvas>";
		$htmlstr .= "	</div>";
		$htmlstr .= $extra_code;
		$htmlstr .= "</div></div>";

		$jsstr = "";
		$jsstr .= "var canvas_$graph_name = document.getElementById('c_summary-$graph_name'); ";
		$jsstr .= "var ctx_$graph_name = canvas_$graph_name.getContext('2d'); ";
		$jsstr .= "var chart_$graph_name = new Chart( ctx_$graph_name ); ";
		$jsstr .= "canvas_$graph_name.chart = chart_$graph_name; ";
		$jsstr .= "var data_$graph_name = JSON.parse( canvas_$graph_name.getAttribute('data') ); ";
		$jsstr .= "var c_$graph_name = chart_$graph_name.$graph_type( data_$graph_name ); ";

		return array("html"=>$htmlstr,"js"=>$jsstr);
	}

	function output_things_you_should_know()
	{
		$res = mysqli_fetch_assoc(mlavu_query("SELECT * FROM `poslavu_MAIN_db`.`messages` WHERE `type`='6' LIMIT 1"));
		return ($res === false || empty($res['content'])) ? '' : '<iframe style="margin:auto" width="180" height="135" src="'.$res['content'].'?wmode=transparent" frameborder="0" allowfullscreen></iframe>';
	}

	$salesdata = htmlspecialchars(getSalesForDay());
	$rawyearsalesdata = getSalesForYear();
	$yearsalesdata = htmlspecialchars($rawyearsalesdata);

	if($rawyearsalesdata=="empty")
	{
		$sales_output_str = "&nbsp;";
	}
	else
	{
		$sales_output_str = output_todays_sales();
	}

	$extra_gcode = "<div id=\"c_legend_container\" style=\"position:absolute; visibility:visible; top:10px; left:110px;\"><table width='240'><tr><td width='100%' align='right'><div id=\"c_legend\" style=\"font-size: 8px; opacity:.9\"></div></td></tr></table></div>";
	/*
	$extra_gcode2 .= "  <div id='todays_sale_container' style='position:absolute; top:134px; left:50px; z-index:20'><table cellspacing=0 cellpadding=0><tr><td width='200' align='center' style='font-size:18px; color:#aaaaaa'>" . $sales_output_str . "</td></tr></table></div>";
	$extra_gcode2 .= "  <div id='todays_sale_control_container' style='position:absolute; top:20px; left:0px; z-index:10; width:300px; height:270px; border-radius:100%' onmouseover='document.getElementById(\"todays_sale_container\").style.visibility = \"hidden\"; this.style.zIndex = -60'></div>";
	$more_gcode2 = " onmouseout='document.getElementById(\"todays_sale_control_container\").style.zIndex = 10; document.getElementById(\"todays_sale_container\").style.visibility = \"visible\"; c_sales.mouseMovedTo( { x: 0, y : 0 } );'";
	*/
	$extra_gcode2 = "";
	$more_gcode2 = "";

	// Overlay Titles
	//$extra_gcode2 = "	<div style='position:absolute; top:33px; left:-30px; z-index:120'><table cellspacing=0 cellpadding=6 bgcolor='#ffffff' style='border:solid 1px $header_color_green; '><tr><td width='120' align='center' style='font-size:16px; color:$header_color_green_dark;' bgcolor='#ffffff'>Today's Sales</td></tr></table></div>";
	//$extra_gcode .= "	<div style='position:absolute; top:-32px; left:80px; z-index:120'><table cellspacing=0 cellpadding=6 bgcolor='#ffffff' style='border:solid 1px $header_color_blue; '><tr><td width='120' align='center' style='font-size:16px; color:$header_color_blue_dark;' bgcolor='#ffffff'>At a Glance</td></tr></table></div>";

	$gcode = create_cpgraph_code("sales2","Line",$yearsalesdata, 400, 240,$extra_gcode);
	$gcode2 = create_cpgraph_code("sales","Doughnut",$salesdata, 300, 270,$extra_gcode2,$more_gcode2);

	$header_color_general = "#f4f4f4";
	$border_color_general = "#c2c2c2";
	$text_color_general = "#aaaaaa";

	$set_color = "#f6f6f6";
	$ds_str = draw_satisfaction_query(admin_info('dataname'));
	$ds_str = str_replace("#888888","$set_color",$ds_str);
	$ds_str = str_replace("#f5fed0","$set_color",$ds_str);
	$ds_str = str_replace("Survey","",$ds_str);
	$ds_str = str_replace("cols=\"60\"","cols=\"32\"",$ds_str);

	$survey_str = "";
	$survey_str .= "<div id='main_poslavu_survey' style='display:none'><div style='position:absolute; top:-12px; left:-20px; width:380px; height:320px; overflow:hidden'>";
	$survey_str .= $ds_str;
	$survey_str .= "</div></div>";
	$survey_str .= "<div id='main_poslavu_survey_entry' style='display:block'>";
	$survey_str .= "	<div style='position:absolute; top:-6px; left:10px; width:300px'>";
	$survey_str .= "<font style='color:#777777'>Please take a moment to fill out this brief survey.  We are always looking for ways to improve our processes and your feedback is important to us.</font><table width='100%' cellspacing=0 cellpadding=0><tr><td width='100%' align='right'>&nbsp;<br><input type='button' value='Take Survey >>' style='cursor:pointer; color:#ffffff; font-size:12px; background-color:#99bb55; border-radius:6px; border:solid 1px #779944; width:120px; height:20px' onclick='document.getElementById(\"main_poslavu_survey_entry\").style.display = \"none\"; document.getElementById(\"main_poslavu_survey\").style.display = \"block\"; document.getElementById(\"general_card_table_survey\").style.height = \"320px\"; document.getElementById(\"general_card_html_div_survey\").style.height = \"300px\"; gridster.resize_widget(gwidges[5],2,2);' /></td></tr></table>";
	$survey_str .= "	</div>";
	$survey_str .= "</div>";

	$msg_str = "";
	$msg_str .= "			<table width='360' height='555' cellpadding=6 cellspacing=0 style='border:solid 1px $border_color_grey' bgcolor='#ffffff'>";
	//echo "				<tr><td width='100%' bgcolor='$header_color_green' align='center' style='color:#ffffff; font-weight:bold; font-size:16px'>Survey</td></tr>";
	//echo "				<tr><td>";
	//echo "					&nbsp;<br><br>";
	//echo "				</td></tr>";
	$msg_str .= "				<tr><td width='100%' height='20' bgcolor='$header_color_green' align='center' class='card_header'>Messages</td></tr>";
	$msg_str .= "				<tr><td align='center'>";
	//echo "					<table width='100%' bgcolor='#eeeeee'><tr><td width='100%' align='center'><br><br><br><br></td></tr></table>";
	$inner_msg_str = "";
	$inner_msg_str .= "					<div style='width:350px; height:[container_height]px; position:relative; top:-10px; overflow:hidden'>";
	/*if(trim($ds_str)!="")
	{
		$inner_msg_str .= "						<table cellpadding=12 width='100%'><tr><td width='100%'><fieldset style='border-color:#aacc77; background-color:#f6f6f6; padding:16px;'><legend style='color:#547700; font-size:14px'>Survey</legend>$survey_str</fieldset></td></tr></table><br>";
	}*/
	$inner_msg_str .= output_home_page_messages(20);
	$inner_msg_str .= "					</div>";
	$msg_str .= $inner_msg_str;
	//echo "					<br><br><table width='100%' style='border-top:solid 1px $header_color_green'><tr><td width='100%' align='center'>" . output_things_you_should_know() . "</td></tr></table>";
	$msg_str .= "				</td></tr>";
	$msg_str .= "			</table>";

	$shortcut_str = "";
	if ( !$isNewControlPanel ) {
		$shortcut_str .= "			<table cellpadding=6 cellspacing=0 style='width:750px; height:175px; border:solid 1px $border_color_grey' bgcolor='#ffffff'>";
		$shortcut_str .= "				<tr><td width='100%' bgcolor='$header_color_blue' align='center' class='card_header'>Shortcuts</td></tr>";
		$shortcut_str .= "				<tr><td valign='top'>" . output_shortcuts("horizontal") . "</td></tr>";
		$shortcut_str .= "			</table>";
	}


	$year_str = "";
	$year_str .= "			<table cellpadding=6 cellspacing=0 style='width:360px; height:360px; border:solid 1px $border_color_grey' bgcolor='#ffffff'>";
	$year_str .= "				<tr><td width='100%' bgcolor='$header_color_mint' align='center' class='card_header'>At a Glance</td></tr>";
	$year_str .= "				<tr><td>";
	$inner_year_str = "";
	if($rawyearsalesdata=="empty")
	{
		$inner_year_str .= "				<div style='position:relative; top:0px; left:0px; width:360px; height:320px; overflow:hidden'>";
		$extra_gcode = str_replace("Today's Sales","At a Glance",$extra_gcode2);
		$gcode = create_cpgraph_code("sales2","Pie",$salesdata, 300, 270,$extra_gcode);
		$inner_year_str .= $gcode['html'];
		$inner_year_str .= "</div>";
	}
	else
	{
		$inner_year_str .= "				<div style='position:relative; top:-40px; left:-60px; width:400px; height:320px; overflow:hidden'>";
		$inner_year_str .= "&nbsp;<br><br><table cellpadding=0><tr><td><table cellspacing=0 cellpadding=4><tr><td width='80'>&nbsp;</td><td width='20' bgcolor='#ffffff'>&nbsp;</td><td align='left'><font style='color:#aaaaaa; font-size:18px'>&nbsp;</font></td></tr><tr><td colspan='3'>" . $gcode['html'] . "</td></tr></table></td></tr></table>";
		$inner_year_str .= "</div>";
	}
	$year_str .= $inner_year_str;
	$year_str .= "</td></tr></table>";

	$today_str = "";
	$today_str .= "			<table cellpadding=6 cellspacing=0 style='width:360px; height:360px; border:solid 1px $border_color_grey' bgcolor='#ffffff'>";
	$today_str .= "				<tr><td width='100%' bgcolor='$header_color_blue' align='center' class='card_header'>Today's Sales</td></tr>";
	$today_str .= "				<tr><td>";
	$today_str .= "				<div style='position:relative; top:10px; left:20px; width:320px; height:320px; overflow:hidden'>";
	$today_str .= $gcode2['html'];
	$today_str .= "</div>";
	$today_str .= "</td></tr></table>";

	$extra_card_code = "";
	ini_set("display_errors",true);
	function clocked_in_str()
	{
		$min_datetime = date("Y-m-d H:i:s",time() - (60 * 60 * 24 * 2));
		$report_query_str = "select * from `clock_punches` where `time`>'".ConnectionHub::getConn('rest')->escapeString($min_datetime)."' and `server_time`!='' and `time_out`='' and `location_id`='[loc_id]' and `server`!='' and `punch_type`='Shift' order by `server_time` asc";
		$report_query_str = parse_report_lavu_query_string($report_query_str);
		$report_query = lavu_query($report_query_str);

		$report_list = array();
		while($report_read = mysqli_fetch_assoc($report_query))
		{
			$report_list[] = $report_read;
		}

		while(count($report_list) < 6)
		{
			$report_list[] = array("server"=>"------","hours"=>"----");
		}

		$max_count = 600;
		$count = 0;
		$output = "";
		$output .= "<table width='336' cellpadding=8 cellspacing=0>";
		for($i=0; $i<count($report_list); $i++)
		{
			$report_read = $report_list[$i];
			if($count < $max_count)
			{
				$show_name = $report_read['server'];
				if(strlen($show_name) > 20)
				{
					$show_name = substr($show_name,0,17) . "...";
				}
				$show_hours = $report_read['hours'];
				if($show_hours!="----")
				{
					date_default_timezone_set($GLOBALS['zone']);
					//$time_diff = time() - strtotime($report_read['server_time']);
					$time_diff = time() - strtotime($report_read['time']);
					$show_hours = number_format(($time_diff / 60 / 60),2) .  " Hrs";
					//$show_hours = "0";
				}

				$extra_css = "";
				if($count > 0)
				{
					$extra_css = "border-top:solid 1px #ffffff";
				}
				$css = "style='font-family:Din,Arial; color:#ffffff; font-size:16px; font-weight:bold; padding-bottom:16px; padding-top:16px; $extra_css'";

				$output .= "<tr>";
				$output .= "<td $css>" . ($count + 1) . ".</td>";
				$output .= "<td $css>" . ucwords($show_name) . "</td>";
				$output .= "<td $css>" . $show_hours . "</td>";
				$output .= "</tr>";
			}
			$count++;
		}
		$output .= "</table>";

		$output = "<div style='width:350px; height:310px; overflow:auto'>" . $output . "</div>";
		return $output;
	}

	function payment_type_totals()
	{
		$paytype_query_str = "select payment_types.type as payment_type, COUNT(cc_transactions.id ) as transactions, if(count(*) > 0, concat('[money]',format(sum( IF(cc_transactions.action='Refund', -1*cc_transactions.total_collected, cc_transactions.total_collected) ),2)), '[money]0.00') as total_payment, if(count(*) > 0, concat('[money]',format(sum( IF(cc_transactions.action='Refund', -1*cc_transactions.tip_amount, cc_transactions.tip_amount) ),2)), '[money]0.00') as total_tip, `payment_types`.`type` as `group_col_payment_types_type` from `cc_transactions`LEFT JOIN `payment_types` ON `cc_transactions`.`pay_type_id` = `payment_types`.`id` AND (`cc_transactions`.`loc_id` = `payment_types`.`loc_id` OR '0' = `payment_types`.`loc_id` ) where cc_transactions.datetime >= '[start_datetime]' and cc_transactions.datetime <= '[end_datetime]' and cc_transactions.voided = '0' and cc_transactions.action != 'Void' and cc_transactions.process_data NOT LIKE 'LOCT%' and cc_transactions.action != '' and cc_transactions.loc_id='[loc_id]' group by payment_types.type";
		$paytype_query_str = parse_report_lavu_query_string($paytype_query_str);
		$paytype_query_str = str_replace("[money]","",$paytype_query_str);
		$paytype_query = lavu_query($paytype_query_str);

		$cash_total = 0;
		$card_total = 0;
		$other_total = 0;

		$str = "";
		while($paytype_read = mysqli_fetch_assoc($paytype_query))
		{
			$ptype = strtolower(trim($paytype_read['payment_type']));
			//foreach($paytype_read as $k => $v) $str .= "----$k = $v<br>";
			//$str .= "type: $ptype<br>";
			$ptotal = ($paytype_read['total_payment'] * 1);

			if($ptype=="cash") $cash_total += $ptotal;
			else if($ptype=="card") $card_total += $ptotal;
			else $other_total += $ptotal;
		}

		return array("cash"=>$cash_total,"card"=>$card_total,"other"=>$other_total,"debug"=>$str);
	}

	function best_seller_str()
	{
		$sales_query_str = "select order_contents.item as order_item, sum(order_contents.quantity) as quantity, if(count(distinct orders.ioid )>count( distinct orders.order_id ),count( distinct orders.ioid ),count( distinct orders.order_id )) as order_count, if(count(*) > 0, concat('[money]',format(sum((order_contents.price + order_contents.modify_price + order_contents.forced_modifiers_price)*order_contents.quantity - if( order_contents.quantity > '0', order_contents.itax, 0) ),2)), '[money]0.00') as gross, if(count(*) > 0, concat('[money]',format(sum(if(order_contents.quantity > 0, order_contents.discount_amount + order_contents.idiscount_amount, 0)),2)), '[money]0.00') as discount, if(count(*) > 0, concat('[money]',format(sum((order_contents.price + order_contents.modify_price + order_contents.forced_modifiers_price)*order_contents.quantity - if( order_contents.quantity > '0',order_contents.discount_amount + order_contents.idiscount_amount + order_contents.itax, 0)),2)), '[money]0.00') as net, if(count(*) > 0, concat('[money]',format(sum( if( order_contents.quantity != 0 AND order_contents.tax_exempt != '1', `order_contents`.`tax1` + `order_contents`.`tax2` + `order_contents`.`tax3` + `order_contents`.`tax4` + `order_contents`.`tax5` - `order_contents`.`itax`, 0 ) ),2)), '[money]0.00') as order_tax, if(count(*) > 0, concat('[money]',format(sum( if( order_contents.quantity != 0 AND order_contents.tax_exempt != '1', `order_contents`.`itax`, 0) ),2)), '[money]0.00') as included_tax, `order_contents`.`item` as `group_col_order_contents_item` from `orders`LEFT JOIN `order_contents` ON `orders`.`order_id` = `order_contents`.`order_id` AND `orders`.`location_id` = `order_contents`.`loc_id` where orders.closed >= '[start_datetime]' and orders.closed < '[end_datetime]' and orders.void = '0' and orders.location_id='[loc_id]' and order_contents.item != 'SENDPOINT' group by order_contents.item order by (sum((order_contents.price + order_contents.modify_price + order_contents.forced_modifiers_price)*order_contents.quantity - if( order_contents.quantity > '0', order_contents.itax, 0) )*1) desc";
		$sales_query_str = parse_report_lavu_query_string($sales_query_str);
		$sales_query = lavu_query($sales_query_str);

		$sales_list = array();
		while($sales_read = mysqli_fetch_assoc($sales_query))
		{
			$sales_list[] = $sales_read;
		}

		while(count($sales_list) < 6)
		{
			$sales_list[] = array("order_item"=>"------","quantity"=>"0","gross"=>"0");
		}

		$max_count = 6;
		$count = 0;
		$best_str = "";
		$best_str .= "<table width='336' cellpadding=8 cellspacing=0>";
		for($i=0; $i<count($sales_list); $i++)
		{
			$sales_read = $sales_list[$i];
			if($count < $max_count)
			{
				$show_item_name = $sales_read['order_item'];
				if(strlen($show_item_name) > 20)
				{
					$show_item_name = substr($show_item_name,0,17) . "...";
				}

				$show_gross = str_replace("[money]","",$sales_read['gross']);
				$show_gross = str_replace("$","",str_replace(",","",$show_gross));
				//$show_gross = "$" . number_format($show_gross,2);
				$show_gross = home_print_money($show_gross);

				$extra_css = "";
				if($count > 0)
				{
					$extra_css = "border-top:solid 1px #ffffff";
				}
				$css = "style='font-family:Din,Arial; color:#ffffff; font-size:16px; font-weight:bold; padding-bottom:16px; padding-top:16px; $extra_css'";

				$best_str .= "<tr>";
				$best_str .= "<td $css>" . ($count + 1) . ".</td>";
				$best_str .= "<td $css>" . $show_item_name . "</td>";
				$best_str .= "<td align='right' $css>x " . $sales_read['quantity'] . "</td>";
				$best_str .= "<td $css>" . $show_gross . "</td>";
				$best_str .= "</tr>";
			}
			$count++;
		}
		$best_str .= "</table>";

		return $best_str;
	}

	function create_general_card($title,$color,$content,$width=2,$height=2,$top_offset=0,$replacement_fields=array())
	{
		global $extra_card_code;
		global $border_color_grey;
		global $location_info;

		$wconv = array();
		$wconv[1] = 170;//170;
		$wconv[2] = 380;//360; //+60
		$wconv[3] = 570;//550; //+100
		$wconv[4] = 800;//740; //+150

		if(isset($wconv[$width])) $html_width = $wconv[$width];
		else $html_width = 180 * $width;
		if(isset($wconv[$height])) $html_height = $wconv[$height];
		else $html_height = 180 * $height;
		$html_height = $html_height -60;//- 30;

		$titleid = strtolower(trim(str_replace(" ","_",$title)));
		$set_overflow = "overflow:hidden";
		if($title=="Survey") $set_overflow = "";

		$str = "";
		$str .= "<table cellpadding=6 cellspacing=0 style='width:".$html_width."px; height:".$html_height."px; border-top:solid 8px #bdbdbd' bgcolor='#ffffff' id='general_card_table_$titleid'>";
		$str .= "	<tr><td width='100%' align='left' style='border-bottom:solid 1px #e7eaec; font-family:Din,Arial,Verdana,Arial; font-size:18px; color:#888888; padding:16px; padding-left:20px'>$title</td></tr>";
		//$str .= "<table cellpadding=6 cellspacing=0 style='width:".$html_width."px; height:".$html_height."px; border:solid 1px $border_color_grey' bgcolor='#ffffff'>";
		//$str .= "	<tr><td width='100%' bgcolor='$color' align='center' class='card_header'>$title</td></tr>";
		$str .= "	<tr><td bgcolor='$color'>";
		$str .= "		<div style='position:relative; top:" . (10 + $top_offset) . "px; left:20px; width:".$html_width."px; height:".$html_height."px; $set_overflow' id='general_card_html_div_$titleid'>";
		$str .= $content;
		$str .= "		</div>";
		$str .= "	</td></tr>";
		$str .= "</table>";

		foreach($replacement_fields as $rkey => $rval)
		{
			$str = str_replace("[" . $rkey . "]",$rval,$str);
		}

		if ($extra_card_code!="")
		    $extra_card_code .= ",";

        /*
         * as per LP-1587, junk chars are getting displayed instead of currency symbols, just url encoded required part
         * of string to ensure correct currency symbols are displayed in the new dashboard page
         *
         */
		if (strpos($str, $location_info['monitary_symbol'])) {
		  $explodeContent = explode($location_info['monitary_symbol'], $str);
		  $currencyTempStr = '';
		  foreach($explodeContent as &$eachItem){
		      $eachItem = urlencode($eachItem);
		  }
		  $currencyTempStr= implode($location_info['monitary_symbol'], $explodeContent);
		  $extra_card_code .= "['<li>' + unescape('".str_replace("+"," ",$currencyTempStr) ."') + '</li>', $width, $height]";
		} else {
		  $extra_card_code .= "['<li>' + unescape('".str_replace("+"," ",urlencode($str)) ."') + '</li>', $width, $height]";
		}
	}

	function card_quickinfo_str($innerstr)
	{
		$str = "";
		$str .= "<table width='100%' height='100%'><tr><td align='center' valign='middle' style='font-family:Verdana,Arial; color:#ffffff; font-weight:bold; font-size:24px'>";

		if(strlen($innerstr) > 7)
		{
			if(strlen($innerstr) > 20) $fontsize = 12;
			else if(strlen($innerstr) > 16) $fontsize = 14;
			else if(strlen($innerstr) > 12) $fontsize = 16;
			else if(strlen($innerstr) > 10) $fontsize = 18;
			else $fontsize = 20;

			$str .= "<font style='font-size:".$fontsize."px'>$innerstr</font>";
		}
		else
		{
			$str .= $innerstr;
		}

		$str .= "</td><td width='40'>&nbsp;</td></tr><tr><td height='20' colspan='2'>&nbsp;</td></tr></table>";
		return $str;
	}

	if(trim($sales_output_str)=="&nbsp;") $sales_output_str = report_format_money(0);

	$outmsg = "<font style='font-family:Arial; font-size:16px; color:#888888'>Apple will release iOS 9 sometime in September 2015. We recommend NOT TO UPDATE devices using Lavu apps to iOS 9 until we determine that iOS 9 is stable and fully compatible with Lavu.</font>";
	$outmsg = "<style>.gridster li.msgli {font-size: 12px;margin-left: 10px;list-style-type:disc;}</style><font style='font-family:Din,Arial; font-size:14px; color:#888888'>We are pleased to announce that Lavu v3.0.3 is compatible with iOS 9. This version offers many advantages, including:<br><br><ul><li class='msgli'>Updated in version 2 reports to include discount reason.<li class='msgli'>Print refund receipts.<li class='msgli'>Improved check splitting and merging capabilities.<li class='msgli'>Edit seat and course numbers.</ul><br>In addition, v3.0.3 features many performance enhancements.<br>If you are currently using v3.0.0 or v3.0.1, we recommend upgrading to our latest v3.0.3 which can be found in the Apple App Store.</font>";
	$outimg = "<img src='/cp/images/alertcircle.png' border='0' style='width:80px' />";

	$outstr = "";
	$outstr .= "<table cellspacing=0 cellpadding=4>";
	$outstr .= "<tr>";//$outstr .= "<tr><td valign='top'>$outimg</td>";
	$outstr .= "<td valign='top'>$outmsg</td><td width='8'>&nbsp;</td></tr>";
	$outstr .= "</table>";

	$one_week_ago = date("Y-m-d",mktime(0,0,0,date("m"),date("d")-7,date("Y")));
	$early_three_query = lavu_query("select * from devices where last_checkin > '[1]' and (poslavu_version LIKE '3.0.1%' or  poslavu_version LIKE '3.0.0%')",$one_week_ago);
	if(mysqli_num_rows($early_three_query))
	{
		create_general_card("Upgrade to Lavu 3.0.3",$header_color_white,$outstr,2,2);
	}

	create_general_card("Gross Sales",$header_color_mint,card_quickinfo_str($sales_output_str),1,1);
	create_general_card("Discounts",$header_color_dark_mint,card_quickinfo_str(output_todays_sales("discount")),1,1);
	create_general_card("Net",$header_color_blue,card_quickinfo_str(output_todays_sales("net")),1,1);
	create_general_card("Tax",$header_color_dark_purple,card_quickinfo_str(output_todays_sales("order_tax")),1,1);

	create_general_card("Messages",$header_color_white,$inner_msg_str,2,1,-10,array("container_height"=>"150")); // was 2 x 3, offset 0, container_height 535

	if(isset($ds_str) && trim($ds_str)!="")//isset($_GET['testhome']))
	{
		$srvstr = "<table cellpadding=12 width='270'><tr><td width='100%'>$survey_str</td></tr></table><br>";
		// NOTE: SURVEY is disabled here! Just uncomment to re-enable!
		// create_general_card("Survey",$header_color_white,$srvstr,2,1);
	}

	create_general_card("At a Glance",$header_color_white,"<div style='position:relative; left:20px'>" . $inner_year_str . "</div>",2,2);
	create_general_card("Category Sales",$header_color_white,"<div style='position:relative; left:20px'>" . $gcode2['html'] . "</div>",2,2);
	if ( !$isNewControlPanel ) {
		create_general_card("Shortcuts",$header_color_white,output_shortcuts("horizontal"),4,1,-10);
	}

	create_general_card("Best Sellers",$header_color_green,best_seller_str(),2,2);

	$avg_txn = 0;
	$sales_count = output_todays_sales("order_count",false);
	$sales_gross = output_todays_sales("gross",false);
	if($sales_count > 0)
	{
		$avg_txn = $sales_gross / $sales_count;
	}
	output_todays_sales("order_tax");

	create_general_card("Order Count",$header_color_dark_orange,card_quickinfo_str($sales_count),1,1);
	create_general_card("Avg Trans",$header_color_gold,card_quickinfo_str(home_print_money($avg_txn)),1,1);
	create_general_card("Clocked In",$header_color_purple,clocked_in_str(),2,2);

	$ptotals = payment_type_totals();
	$last_7_days_gross = getSalesOnDay(date("Y-m-d",strtotime("-7 days")),7);

	create_general_card("Cash",$header_color_mint,card_quickinfo_str(home_print_money($ptotals['cash'])),1,1);
	create_general_card("Credit",$header_color_dark_orange,card_quickinfo_str(home_print_money($ptotals['card'])),1,1);
	create_general_card("Other",$header_color_dark_purple,card_quickinfo_str(home_print_money($ptotals['other'])),1,1);
	create_general_card("Last 7 Days",$header_color_light_green,card_quickinfo_str(home_print_money($last_7_days_gross)),1,1);
?>
	<style>
		.card_header {
			font-family:Din,Verdana,Arial;
			font-size:16px;
			color:#ffffff;
		}
	</style>

    <div class="gridster" id='gridster_container' style='width:900px;'>
        <ul></ul>
    </div>

    <br><br><table><tr><td align='center' width='600' id='switch_container'><a href='?viewcp=previous' style='color:#9999bb; text-decoration:none'>Switch to Previous Control Panel Version</a></td></tr>
				</table>



    <script type="text/javascript" id="code">
    var gridster;
	gwidges = new Array();

    $(function(){
		var winwidth = ($(window).width());
		var set_gridster_width = winwidth - 220;
		if(set_gridster_width < 1250) set_gridster_width = 900;
		if(set_gridster_width > 1360) set_gridster_width = 1320;
		document.getElementById('switch_container').style.width = set_gridster_width + "px";
		document.getElementById('gridster_container').style.width = set_gridster_width + "px";

      gridster = $(".gridster > ul").gridster({
          widget_margins: [15, 15],
          widget_base_dimensions: [180, 180]
      }).data('gridster');

	  gridster.disable();
      var widgets = [
          <?php echo $extra_card_code; ?>
      ];

      $.each(widgets, function(i, widget){
          gwidges[gwidges.length] = gridster.add_widget.apply(gridster, widget)
      });

   });

	setTimeout("check_id_code()",1000);
    </script>

<?php
	echo "<script type=\"text/javascript\" src=\"/cp/reports_v2/chart.js\"></script> ";
	echo "<script type=\"text/javascript\">";
	echo "	NumberFormatter.sharedFormatter( \"".$ms['comma_char']."\", \"".$ms['decimal_char']."\", ".$ms['decimal_places'].", \"".$ms['monitary_symbol']."\", \"".$ms['left_or_right']."\" );";
	echo "	function check_id_code() { ";
	echo $gcode['js'];
	echo $gcode2['js'];
	echo "	} ";
	echo "</script>";
?>

<script type="text/javascript">
	var legend_element = document.getElementById('c_legend');
	var dataSets = chart_sales2._data.datasets;
	var dcounter = 0;
	for( var d in dataSets ){
		dcounter++;
		if(dcounter <= 4)
		{
			var entry = document.createElement('div');
			entry.style.display = 'inline-block';
			entry.style.margin = '0 0.4em';
			var dot = document.createElement('div');
			dot.style.borderRadius = '100%';
			dot.style.backgroundColor = dataSets[d].color.toString();
			dot.style.height = dot.style.width = '0.75em';
			dot.style.display = 'inline-block';
			dot.style.margin = '0.2em';
			dot.style.verticalAlign = 'text-bottom';

			entry.appendChild( dot );
			entry.appendChild( document.createTextNode(dataSets[d].label) );
			legend_element.appendChild( entry );
		}
	}
</script>
