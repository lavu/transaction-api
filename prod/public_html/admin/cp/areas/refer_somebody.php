<?php

// checks if too many emails have been sent to the source or from the destination
// the source is allowed to send multiple emails to the same destination
// @$b_error_sending_email: set to TRUE if there's an error
// @$s_error_message: set to an error message if there's an error
// @return: TRUE if the check succeeds, FALSE otherwise
function check_referral_emails($to_email, $from_email, &$b_error_sending_email, &$s_error_message) {
	global $data_name;

	// how many emails the destination is allowed to receive in the set number of days
	$destination_days = 7;
	$destination_count = 1;

	// how many emails the source is allowed to send in the set number of days
	$source_days = 1;
	$source_count = 20;

	// how many emails the source is allowed to send to a single destination in the set number of days
	// overrides source_count and destination_count
	$source_override_days = 7;
	$source_override_count = 2;

	// check that the emails have good syntax
	if (!check_valid_email_syntax($to_email, $b_error_sending_email, $s_error_message))
		return FALSE;
	if (!check_valid_email_syntax($from_email, $b_error_sending_email, $s_error_message))
		return FALSE;

	// check that the emails haven't been spammed
	$a_destination_emails = ConnectionHub::getConn('poslavu')->getDBAPI()->getAllInTable('referrals_sent', array('to_email'=>$to_email, 'datetime'=>date('Y-m-d H:i:s',strtotime('-'.$destination_days.' days'))), TRUE,
		array('whereclause'=>"WHERE `to_email`='[to_email]' AND `datetime` >= '[datetime]' AND `allowed_to_send`='1'"));
	$a_source_emails = ConnectionHub::getConn('poslavu')->getDBAPI()->getAllInTable('referrals_sent', array('from_dataname'=>$data_name, 'datetime'=>date('Y-m-d H:i:s',strtotime('-'.$source_days.' days'))), TRUE,
		array('whereclause'=>"WHERE `from_dataname`='[from_dataname]' AND `datetime` >= '[datetime]' AND `allowed_to_send`='1'", 'selectclause'=>"DISTINCT(`to_email`)"));
	$a_source_override_emails = ConnectionHub::getConn('poslavu')->getDBAPI()->getAllInTable('referrals_sent', array('to_email'=>$to_email, 'from_dataname'=>$data_name, 'datetime'=>date('Y-m-d H:i:s',strtotime('-'.$source_override_days.' days'))), TRUE,
		array('whereclause'=>"WHERE `to_email`='[to_email]' AND `from_dataname`='[from_dataname]' AND `datetime` >= '[datetime]' AND `allowed_to_send`='1'"));

	// check the destination
	if (count($a_source_emails) >= $source_count) {
		if (count($a_source_override_emails) == 0) {
			$b_error_sending_email = TRUE;
			$s_day = ($source_override_days == 1) ? 'today' : 'over the past '.$source_override_days.' days';
			$s_error_message = 'You have already sent the maximum allowed referrals '.$s_day.'.';
			return FALSE;
		}
		if (count($a_source_override_emails) >= $source_override_count) {
			$b_error_sending_email = TRUE;
			$s_day = ($source_override_days == 1) ? 'today' : 'in the past '.$source_override_days.' days';
			$s_error_message = 'You have already sent the maximum allowed referrals to "'.$to_email.'" '.$s_day.'.';
			return FALSE;
		}
	}

	// check the source
	if (count($a_destination_emails) >= $destination_count) {
		if (count($a_source_override_emails) == 0) {
			$b_error_sending_email = TRUE;
			$s_error_message = 'The email "'.$to_email.'" has already received a referral offer.';
			return FALSE;
		}
		if (count($a_source_override_emails) >= $source_override_count) {
			$b_error_sending_email = TRUE;
			$s_error_message = 'The email "'.$to_email.'" has already received a referral offer from this account.';
			return FALSE;
		}
	}

	return TRUE;
}

if($in_lavu)
{
	require_once(dirname(__FILE__)."/../../sa_cp/billing/referal_functions.php");
	require_once(dirname(__FILE__)."/../resources/json.php");

	function referral_form_style_tostring() {
		ob_start();
		?>
		<style type="text/css" media="screen">
			body {
				background: gray;
				margin: 0;
				font-family: Verdana;
			}

			#main-container {
				padding-bottom: 0;
				padding-left: 50px;
				padding-right: 50px;
				color: #1f1f1f;
				position: relative;
			}
			.three-columns {
				margin: 20px auto;
			}
			.three-columns > * {
				float: left;
			}

			.three-columns > .left-column {
				width: 345px;
			}
			#referral-code {
				text-align: center;
				font-family:Verdana, Geneva, sans-serif;
				font-weight: 100;
				font-size: 56pt;
				margin-bottom: 0;
				margin-top: 10px;
				color: #AECD37;
			}
			.three-columns > .left-column h4 {
				margin-top: 0;
				text-align: center;
				font-family:Verdana, Geneva, sans-serif;
				font-size: 12pt;
			}
			.three-columns > .left-column p {
				text-align: center;
				font-size: 9pt;
				color: #b2b2b2;
			}
			.three-columns > .middle-column {
				width: 190px;
				margin-left: 10px;
			}
			.three-columns > .right-column {
				width: 240px;
				margin-left: 10px;
			}
			.three-columns > .middle-column {

			}
			.three-columns > .middle-column h2:first-child, .three-columns > .right-column h2:first-child {
				font-weight: 200;
				font-size: 16pt;
				color: #c6c6c6;
				margin-top: 25px;
			}
			.three-columns > .middle-column p, .three-columns > .right-column p {
				font-size: 9pt;
				color: #b2b2b2;
			}
			.three-columns > .middle-column ul, .three-columns > .right-column ul {
				list-style-type: none;
				padding-left: 0;
			}
			.three-columns > .middle-column li, .three-columns > .right-column li {
				font-size: 9pt;
				font-weight: 600;
				line-height:1.5em;
				background-image: url("images/check_sm.png");
				background-position: 0 2px;
				background-repeat: no-repeat;
				padding-left: 18px;
				text-align: left;
			}
			#main-container hr {
				width: 80%;
				margin: 5px auto;
				height: 5px;
				border: none;
			}

			#middle-part {
				text-align: center;
				color: #AECD37;
				font-weight: 400;
				font-size:17px;
			}
			#main-container form table {
				text-align: center;
				font-size: 16pt;
				letter-spacing: 1px;
				position: relative;
			}
			#main-container form table tr td {
				vertical-align: bottom;
			}
			#main-container form table tr td table tr td label {
				font-size: 8pt;
				text-align: center;
				letter-spacing: 0;
			}
			#main-container form table tr td input[type=text] {
				background: #F6FBE4; /* rgb 234, 239, 217 */
				border: 1px solid #999999;
				height: 26px;
				box-shadow: inset 0 7px 7px -7px rgba(0,0,0,0.5);
				border-radius: 3px;
				-webkit-border-radius: 3px;
				-moz-border-radius: 3px;
				margin-left:0;
				margin-right:0;
			}
			#main-container form table tr td table tr td input[type=submit] {
				border: none;
				background: #A9C730;
				color: white;
				height:35px;
				width:150px;
				padding: 5px 0;
				border-radius: 3px;
				-webkit-border-radius: 3px;
				-moz-border-radius: 3px;
			}
			.referral_hr {
				background-image: url("images/tabs_light_fadeup.png");
				height: 44px;
				width: 870px;
				background-repeat: no-repeat;
			}
			.submit_lavu_lead_button {
				background-image: url("images/btn_submit_lead.png");
				width: 160px;
				height: 44px;
				border-radius: 6px;
				border-width: 0px;
				background-repeat: no-repeat;
				cursor: pointer;
				position: relative;
				bottom: -4px;
			}

		</style>
		<?php
		$s_retval = ob_get_contents();
		ob_end_clean();
		return $s_retval;
	}

	function referral_form_form_tostring($mode) {
		ob_start();
		?>
		<script type="text/javascript">
			function submit_referral_form(submit_button) {
				var jsubmit = $(submit_button);
				var jsubmit_parent = jsubmit.parent().parent();
				var jemail = jsubmit_parent.find("input[name=refer_email]");
				var jname = jsubmit_parent.find("input[name=refer_name]");
				var email_val = jemail.val();
				var name_val = jname.val();

				if (!name_val.match(/.+/)) {
					alert("Please enter a contact name.");
					return;
				}
				if (!email_val.match(/.+\@.+\..+/)) {
					alert("Please enter a valid email address.");
					return;
				}
				findParent(jsubmit, "form").submit();
			}
			function findParent(jelement, match) {
				var jparent = jelement.parent();
				if (jparent.is(match))
					return jparent;
				if (jparent.is("document"))
					return null;
				return findParent(jparent, match);
			}
		</script>
		<form action="index.php?mode=<?= $mode ?>" method="post" name="rfsend">
			<table>
				<tr>
					<td>
						<font style="position:relative; bottom:7px;">Send Lavu Referral to:</font>
					</td>
					<td style="text-align:center;">
						<table>
							<tr><td>
								<label for="name">Enter a contact name</label>
							</td></tr>
							<tr><td>
								<input type="text" name="refer_name" value="" size="25" />
							</td></tr>
						</table>
					</td>
					<td style="text-align:center;">
						<table>
							<tr><td>
								<label for="email">Enter an email address</label>
							</td></tr>
							<tr><td>
								<input type="text" name="refer_email" value="" size="25" />
							</td></tr>
						</table>
					</td>
					<td>
						<input type="button" class="submit_lavu_lead_button" alt="SUBMIT LAVU LEAD" onclick="submit_referral_form(this);" />
					</td>
					<td style="vertical-align:middle;">
						<?= help_mark_tostring(19) ?>
					</td>
				</tr>
			</table>
		</form>
		<?php
		$s_retval = ob_get_contents();
		ob_end_clean();
		return $s_retval;
	}

	function referral_form_code_tostring($ref_code) {
		ob_start();
		?>
		<div id="top-content" class="three-columns">
			<div class="left-column">
				<h2 id="referral-code"><?php echo $ref_code; ?></h2>
				<h4>This is your Lavu Referral Code!</h4>
				<p>
					Contact business owner friends and Refer Lavu.
					<br />
					They signup using your Referral Code, and you BOTH win.
				</p>
			</div>
			<img alt="" class="arrow-separator" src="images/arrowshade.jpg" />
			<div class="middle-column">
				<h2>REFERRAL</h2>
				<p>
					Your friend will be offered:
				</p>
				<ul>
					<li>
						A Free Trial
					</li>
					<li>
						A Free Consultation
					</li>
					<li>
						A Free Month of Hosting
					</li>
				</ul>
			</div>
			<img alt="" class="arrow-separator" src="images/arrowshade.jpg" />
			<div class="right-column">
				<h2>REWARD</h2>
				<p>
					Upon signup, you will receive:
				</p>
				<ul>
					<li>
						1 Free Month of Lavu
					</li>
					<li>
						Quality Lavu T-shirt
					</li>
					<!--<li>
						5 Referrals - WE PAY YOU $500!
					</li>-->
				</ul>
			</div>
		</div>
		<?php
		$s_retval = ob_get_contents();
		ob_end_clean();
		return $s_retval;
	}

	function referral_form_hr_tostring($i_padding_top = 0, $i_padding_bottom = 0) {
		$i_padding_top -= 42;
		$i_padding_bottom -= 1;
		return '
		<div class="referral_hr" style="margin-top:<?= $i_padding_top ?>px;"></div>
		<div style="width:1px; height:1px; margin-top:<?= $i_padding_bottom ?>px;"></div>';
	}

	function referral_form_promo_tostring() {
		return '
		<h3 id="middle-part"><img src="images/check_med.png" alt="" /> Receive 1 FREE MONTH of Lavu for each friend who becomes a new customer</h3>';
	}

	function create_new_referral_code($length=4)
	{
		$done = false;
		$count = 0;
		while(!$done)
		{
			$str = "";
			for($i=0; $i<$length; $i++)
			{
				$n = rand(0,35);
				if($n < 10) // 0-9
				{
					$setn = 48 + $n;
				}
				else if($n<=35) // 10-35
				{
					$setn = 55 + $n;
				}
				else if($n<=61) // 36-61
				{
					$setn = 61 + $n;
				}
				$str .= chr($setn);
			}

			$str = str_replace('0', 'A', $str);
			$str = str_replace('O', 'E', $str);

			$dup_query = mlavu_query("select count(*) as `count` from `poslavu_MAIN_db`.`restaurant_locations` where `referral_code` LIKE '[1]'",$str); // poslavu_MAIN_db
			if(mysqli_num_rows($dup_query))
			{
				$dup_read = mysqli_fetch_assoc($dup_query);
				$dup_count = $dup_read['count'];

				if($dup_count < 1)
				{
					$done = true;
				}
			}
			$count++;
			if($count > 100)
			{
				$done = true;
				$str = "";
			}
			else if($count==20 || $count==40 || $count==60 || $count==80)
			{
				$length++;
			}
		}

		return $str;
	}

	function draw_main_referral_page() {
		$ref_code_query = mlavu_query("select `id`,`referral_code` from `poslavu_MAIN_db`.`restaurant_locations` where `dataname`='[dataname]' and `dataname`!=''",array("dataname"=>admin_info("dataname"))); // poslavu_MAIN_db
		if(mysqli_num_rows($ref_code_query))
		{
			$ref_code_read = mysqli_fetch_assoc($ref_code_query);
			$ref_code = $ref_code_read['referral_code'];

			if(trim($ref_code)=="")
			{
				$ref_code = create_new_referral_code();

				mlavu_query("update `poslavu_MAIN_db`.`restaurant_locations` set `referral_code`='[1]' where `id`='[2]'",$ref_code,$ref_code_read['id']); // poslavu_MAIN_db
			}

			$show_rform = true;
			$default_refer_emails = "";
			if(isset($_POST['refer_email']))
			{
				require_once(dirname(__FILE__).'/../../sa_cp/resources/send_email.php');
				require_once(dirname(__FILE__).'/../objects/json_decode.php');

				global $data_name;
				$b_error_sending_email = FALSE;
				$default_refer_email = trim($_POST['refer_email']);
				$refer_name = trim($_POST['refer_name']);
				$now = strtotime('now');

				/*$remails = array();
				$referral_emails = explode(",",$_POST['refer_emails']);
				for($i=0; $i<count($referral_emails); $i++)
				{
					$inner_emails = explode(";",$referral_emails[$i]);
					for($n=0; $n<count($inner_emails); $n++)
					{
						$semail = trim($inner_emails[$n]);
						if($semail!="" && strpos($semail,".") > 0 && strpos($semail,"@") > 0)
						{
							$remails[] = $semail;
						}
					}
				}*/
				$remails = array();
				$remails[] = array("name"=>$refer_name,"email"=>$default_refer_email);

				if(count($remails) > 0)
				{
					for($i=0; $i<count($remails); $i++)
					{
						echo "Sending Referral To: " . $remails[$i]['name'] . "<br>";

						$to_email = $remails[$i]['email'];
						$to_name = $remails[$i]['name'];

						// get the from_name, from_email, and from_company_name
						$from_name = trim(admin_info('fullname'));//"Corey Fiala";
						$from_email = 'noreply@poslavu.com';//trim(admin_info('email'));//"corey@poslavu.com";
						if ($from_email == '') $from_email = 'noreply@poslavu.com';
						$from_company_name = trim(admin_info('company_name'));//"The funky monkey's uncle";
						//if ($from_company_name == '') {
							$a_restaurants = ConnectionHub::getConn('poslavu')->getDBAPI()->getAllInTable('locations', NULL, FALSE);
							if (count($a_restaurants) == 0) {
								$b_error_sending_email = TRUE;
								$s_error_message = 'Your restaurant couldn\'t be found in our databases.';
								break;
							}
							$from_company_name = $a_restaurants[0]['title'];
						//}

						$subject = "";
						$subject .= "What we are using at [from_company_name]";

						$msg = "";
						$msg .= "Hi [to_name], I am sending you this email because you may be interested in\n";
						$msg .= "the Point of Sale product we are using at [from_company_name].  The product\n";
						$msg .= "is called POSLavu, and if you decide to try it you can get your first\n";
						$msg .= "month of hosting waived by using this link:\n\n";
						$msg .= "http://www.poslavu.com/en/ipad-pos-signup?rf=" . $ref_code;

						$msg = str_replace("[to_name]",$to_name,$msg);
						$msg = str_replace("[from_company_name]",$from_company_name,$msg);
						$subject = str_replace("[to_name]",$to_name,$subject);
						$subject = str_replace("[from_company_name]",$from_company_name,$subject);

						$a_insert_vars = array('from_dataname'=>$data_name, 'from_email'=>$from_email, 'from_name'=>$from_name, 'to_email'=>$to_email, 'to_name'=>$to_name, 'datetime'=>date('Y-m-d H:i:s',$now), 'timestamp'=>$now, 'ipaddress'=>$_SERVER['REMOTE_ADDR'], 'allowed_to_send'=>'1', 'rejection_reason'=>'');
						$s_insert_clause = ConnectionHub::getConn('poslavu')->getDBAPI()->arrayToInsertClause($a_insert_vars);
						if (!check_referral_emails($to_email, $from_email, $b_error_sending_email, $s_error_message)) {
							$a_insert_vars['allowed_to_send'] = '0';
							$a_insert_vars['rejection_reason'] = $s_error_message;
							mlavu_query("INSERT INTO `poslavu_MAIN_db`.`referrals_sent` $s_insert_clause", $a_insert_vars);
							break;
						} else {
							mlavu_query("INSERT INTO `poslavu_MAIN_db`.`referrals_sent` $s_insert_clause", $a_insert_vars);
						}

						// send the email and get the response
						$a_success = send_email_through_sendgrid($to_email, $from_email, $subject, $msg, array('fromname'=>$from_name));
						if ($a_success['type'] == 'response') {
							$a_json = (array)Json::unencode($a_success['response']);
							if ($a_json['message'] == 'error') {
								$b_error_sending_email = TRUE;
								$s_error_message = $a_json['errors'][0];
							}
						} else {
							$s_response = $a_success['response'];
							$b_error_sending_email = TRUE;
							$s_error_message = 'There is '.$s_response;
						}

						// check for errors
						if ($b_error_sending_email)
							error_log('error: '.$s_error_message);
					}
					echo "<br>";
					$show_rform = false;
				}
				else
				{
					echo "<b>None of the emails you entered are valid.  Please correct them to continue</b><br><br>";
				}
			}

			if ($b_error_sending_email) {
				echo "
				<div style='font-weight:bold;'>
					".ucfirst($s_error_message)."
				</div>";
			}

			echo '<div id="main-container">';
			echo referral_form_style_tostring();
			if($show_rform) {
				echo referral_form_form_tostring($mode);
				echo referral_form_hr_tostring(39, 6);
			}
			echo referral_form_promo_tostring();
			echo referral_form_hr_tostring(6,8);
			echo referral_form_code_tostring($ref_code);
			echo '</div><br />';

		}
		else
		{
			echo "Sorry, your account is not yet ready to supply a referral code<br><br>";
		}
	}

	// get the referral code for a restaurant
	// @$s_dataname: if NULL, returns the referral code for this restaurant
	// @return: the referral code as it stands in the database (might be blank)
	function get_referral_code($s_dataname = NULL) {
		global $ref_code;

		if (isset($ref_code) && $s_dataname === NULL) {
			return $ref_code;
		} else {
			if ($s_dataname === NULL)
				$s_dataname = admin_info("dataname");
			$a_rlocations = ConnectionHub::getConn('poslavu')->getDBAPI()->getAllInTable('restaurant_locations', array('dataname'=>$s_dataname), TRUE, array('selectclause'=>'`id`,`referral_code`'));
			return $a_rlocations[0]['referral_code'];
		}
	}

	// get the promo code for a signup
	// @$s_dataname: if NULL, returns the referral code for this signup
	// @return: the promo code as it stands in the database (might be blank)
	function get_promo_code($s_dataname = NULL) {
		global $ref_code;

		if (isset($ref_code) && $s_dataname === NULL) {
			return $ref_code;
		} else {
			if ($s_dataname === NULL)
				$s_dataname = admin_info("dataname");
			$a_signups = ConnectionHub::getConn('poslavu')->getDBAPI()->getAllInTable('signups', array('dataname'=>$s_dataname), TRUE, array('selectclause'=>'`id`,`promo`'));
			return $a_signups[0]['promo'];
		}
	}

	// updates the signup's promo code
	// if the promo code can't be found, return 'no promo code'
	// if promo code == 'used:%' then replace 'used:' with '' and return 'success' or 'failed to update'
	// otherwise, return 'not yet used'
	function set_promo_code_not_used($s_dataname = NULL) {

		// get the promo code and check that it is set
		$s_promo_code = trim(get_promo_code($s_dataname));
		if ($s_promo_code == '')
			return 'no promo code';

		if (strpos($s_promo_code, 'used:') == 0) {
			$s_promo_code = str_replace('used:', '', $s_promo_code);
			$query_string = "UPDATE `poslavu_MAIN_db`.`signups` SET `promo`='[promo]' WHERE `dataname`='[dataname]'";
			$query_vars = array('promo'=>$s_promo_code, 'dataname'=>$s_dataname);
			mlavu_query($query_string, $query_vars); // poslavu_MAIN_db
			error_log(ConnectionHub::getConn('poslavu')->getDBAPI()->drawQuery($query_string, $query_vars));
			if (ConnectionHub::getConn('poslavu')->affectedRows() > 0)
				return 'success';
			else
				return 'failed to update';
		}

		return 'not yet used';
	}

	// sanitizes the referral code
	function sanitize_referral_code(&$s_referral_code) {

		// upper case the referral code
		$s_referral_code = strtoupper($s_referral_code);
		$a_matches = NULL;

		// grab the first four letters/numbers
		preg_match_all('/[A-Z0-9]/', $s_referral_code, $a_matches);
		$s_referral_code = '';
		if (count($a_matches) > 0)
			$a_matches = $a_matches[0];
		for($i = 0; $i < min(4, count($a_matches)); $i++) {
			$s_referral_code .= $a_matches[$i];
		}
	}

	// checks if the referral code is valid
	// @$s_referral_code: to be searched for in `restaurant_locations`.`referral_code`
	// @$s_referrer_dataname: set to the dataname of the matching restaurant location
	// @return: TRUE or FALSE
	function get_referral_code_is_valid($s_referral_code, &$s_referrer_dataname = NULL) {

		// sanitize the referral code and check that it isn't blank
		sanitize_referral_code($s_referral_code);
		if ($s_referral_code == '')
			return FALSE;

		// check that the referral code is valid
		$a_restaurants = ConnectionHub::getConn('poslavu')->getDBAPI()->getAllInTable('restaurant_locations', array('referral_code'=>$s_referral_code), TRUE, array('selectclause'=>'`dataname`'));
		if (count($a_restaurants) == 0)
			return FALSE;

		// the referral code is valid, update the $s_referrer_dataname and return TRUE
		$s_referrer_dataname = $a_restaurants[0]['dataname'];
		return TRUE;
	}

	// applies the supplied referral code to the restaurant's signup
	// @$s_dataname: the restaurant to apply the referral to, can be empty string ''
	// @$s_signup_id: the restaurant to apply the referral to if there is no dataname, can be empty string '' if the dataname is provided
	// @$s_referral_code: the code to apply
	// @$b_ignore_if_same: if TRUE, does nothing and returns 'referral code already applied' if $s_referral_code is already applied
	// @return: 'invalid referral code', 'no signup', 'referral code already applied', 'failed to update database', or 'success'
	function apply_referral_code($s_dataname, $s_signup_id, $s_referral_code, $b_ignore_if_same = TRUE) {

		sanitize_referral_code($s_referral_code);

		// check that the referral code is valid
		if (!get_referral_code_is_valid($s_referral_code))
			return 'invalid referral code';

		// check that the restaurant has a signup
		$s_restaurant_id = 0;
		if ((string)$s_dataname != '') {
			$a_restaurants = ConnectionHub::getConn('poslavu')->getDBAPI()->getAllInTable('restaurants', array('data_name'=>$s_dataname), TRUE);
			if (count($a_restaurants) != 0) {
				if ($s_signup_id == '')
					$s_signup_id == (int)$a_restaurants[0]['signupid'];
				$s_restaurant_id = $a_restaurants[0]['id'];
				$a_restaurant = $a_restaurants[0];
			}
		}
		require_once(dirname(__FILE__).'/../../sa_cp/billing/payment_profile_functions.php');
		$a_signup = get_signup_by_id_dataname($s_restaurant_id, $s_dataname);
		if (count($a_signup) == 0)
			return 'no signup';
		$s_signup_id = $a_signup['id'];

		// check if a referral code is already applied
		$s_old_referral_code = $a_signup['promo'];
		$s_referrer_dataname = '';
		if (($s_old_referral_code != $s_referral_code) || ($s_old_referral_code == $s_referral_code && $b_ignore_if_same))
			if (get_referral_code_is_valid($s_old_referral_code, $s_referrer_dataname))
				return 'referral code already applied';

		// try to apply the referral code
		mlavu_query("UPDATE 	`poslavu_MAIN_db`.`signups` SET `promo`='[promo]' WHERE `id`='[id]'",
			array('promo'=>$s_referral_code, 'id'=>$a_signup['id']));
		if (ConnectionHub::getConn('poslavu')->affectedRows() == 0)
			return 'failed to update database';

		// waive the first months hosting
		if (isset($a_restaurant) && count($a_restaurant) > 0) {
			require_once(dirname(__FILE__).'/../../sa_cp/billing/procareas/procpay_functions.php');
			$a_package = find_package_status($s_restaurant_id);
			$i_package = (int)$a_package['package'];
			$i_created = strtotime($a_restaurant['created']);
			$a_props = array('created'=>date('Y-m-d',$i_created));
			$a_prop_parts = get_setup_props($a_props, $i_package, $a_signup['status']);
			$i_license_add_days = (int)$a_prop_parts['license_billing_add_days'];
			$now = strtotime('now');
			$i_license_billdate = strtotime($a_props['created'].' +'.$i_license_add_days.' days');
			$i_billdate =$i_license_billdate;
			$stuff = array();
			redeem_referral_code_create_waived_invoice(date('Y-m-d H:i:s', $now), $a_restaurant, admin_info('username'), admin_info('fullname'), $s_dataname, $s_referrer_dataname, $i_billdate, $s_dataname, $s_restaurant_id, $i_package, $stuff);
		}

		return 'success';
	}

	// uses the referral code that was applied to the restaurant's signup
	// @$s_dataname: dataname of the restaurant to be redeemed
	// @$s_user: used for the billing log
	// @$s_full_username: used for the billing log
	// @$b_award_free_month_to_referree: if TRUE, then the referree gets a free month
	// @$b_award_free_month_to_referrer: if TRUE, then the referrer gets a free month
	// @return: 'restaurant not found', 'no referral code', 'invalid referral code', 'referral already used', 'failed to update database', 'no record of restaurant payments or distributor license', 'could not update automatic recurring billing', or 'success'
	function redeem_referral_code($s_dataname, $s_user, $s_full_username, $b_award_free_month_to_referree = FALSE, $b_award_free_month_to_referrer = TRUE) {

		// mlavu connection

		// used to undo any changes in case the changes fail
		$a_reverts = array();
		$now = date('Y-m-d H:i:s');

		// check that the restaurant can be found and it has a referral code
		require_once(dirname(__FILE__).'/../../sa_cp/billing/payment_profile_functions.php');
		$a_restaurants = ConnectionHub::getConn('poslavu')->getDBAPI()->getAllInTable('restaurants', array('data_name'=>$s_dataname), TRUE);
		if (count($a_restaurants) == 0) {
			return 'restaurant not found';
		}
		$a_restaurant = $a_restaurants[0];
		$s_restaurant_id = $a_restaurant['id'];
		$a_signup = get_signup_by_id_dataname($s_restaurant_id, $s_dataname);
		if (count($a_signup) == 0) {
			return 'restaurant signup not found';
		}
		if (trim($a_signup['promo']) == '') {
			return 'no referral code';
		}
		$a_package = find_package_status($a_restaurant['id']);
		$i_package = $a_package['package'];

		// check that the referral code is valid and not already used
		// get the dataname of the referrer
		if (strstr($a_signup['promo'], 'used:')) {
			return 'referral already used';
		}
		$s_dataname_referrer = '';
		if (!get_referral_code_is_valid($a_signup['promo'], $s_dataname_referrer)) {
			return 'invalid referral code';
		}

		// check that the referrer has a signup record and get their billing profiles
		$a_restaurant_referrer = ConnectionHub::getConn('poslavu')->getDBAPI()->getAllInTable('restaurants', array('data_name'=>$s_dataname_referrer), TRUE, array('selectclause'=>'`id`,`created`'));
		if (count($a_restaurant_referrer) > 0) {
			$a_restaurant_referrer = $a_restaurant_referrer[0];
			$a_signup_referrer = get_signup_by_id_dataname($a_restaurant_referrer['id'], $s_dataname_referrer);
			if (count($a_signup_referrer) > 0) {
				$a_package_referrer = find_package_status($a_restaurant_referrer['id']);
				$i_package_referrer = $a_package_referrer['package'];
			}
		}

		// get the next bill dates and billing profiles for both the referrer and referree
		$a_billing_profiles = array();
		$a_billing_profiles_referrer = array();
		$i_next_billdate_referree = redeem_referral_code_get_next_bill_date($s_dataname, $i_package, $a_restaurant['created'], $a_restaurant['id'], $a_signup, $a_billing_profiles);
		if (count($a_signup_referrer) > 0)
			$i_next_billdate_referrer = redeem_referral_code_get_next_bill_date($s_dataname_referrer, $i_package_referrer, $a_restaurant_referrer['created'], $a_restaurant_referrer['id'], $a_signup_referrer, $a_billing_profiles_referrer);

		// update the signups table of the referree
		$s_update_string = ConnectionHub::getConn('poslavu')->getDBAPI()->arrayToUpdateClause($a_signup);
		$a_reverts[] = array('string'=>"UPDATE `poslavu_MAIN_db`.`signups` $s_update_string WHERE `id`='[id]'", 'vars'=>$a_signup);
		mlavu_query("UPDATE `poslavu_MAIN_db`.`signups` SET `promo`='[promo]' WHERE `id`='[id]'", array('promo'=>'used:'.$a_signup['promo'], 'id'=>$a_signup['id']));
		if (ConnectionHub::getConn('poslavu')->affectedRows() == 0) {
			return 'failed to update database';
		}

		// update/insert the waived invoice for the referree
		$i_invoice_id_referree = 0;
		$i_invoice_id_referrer = 0;
		if ($b_award_free_month_to_referree) {
			if (!redeem_referral_code_create_waived_invoice($now, $a_restaurant, $s_user, $s_full_username, $s_dataname_referree, $s_dataname_referrer, $i_next_billdate_referree, $s_dataname, $a_restaurant['id'], $i_package, $a_reverts, $i_invoice_id_referree)) {
				foreach($a_reverts as $a_revert)
					mlavu_query($a_revert['string'], $a_revert['vars']);//poslavu_MAIN_db
				return 'failed to update database';
			}
		}

		// update/insert the waived invoice for the referrer
		if (count($a_signup_referrer) > 0 && $b_award_free_month_to_referrer) {
			if (!redeem_referral_code_create_waived_invoice($now, $a_restaurant_referrer, $s_user, $s_full_username, $s_dataname_referree, $s_dataname_referrer, $i_next_billdate_referrer, $s_dataname_referrer, $a_restaurant_referrer['id'], $i_package_referrer, $a_reverts, $i_invoice_id_referrer)) {
				foreach($a_reverts as $a_revert)
					mlavu_query($a_revert['string'], $a_revert['vars']);//poslavu_MAIN_db
				return 'failed to update database';
			}
		}

		// create the log to reset the authorize.net amount
		if ($b_award_free_month_to_referree && !redeem_referral_code_set_reset_arb_amount($a_billing_profiles['subscriptionid'], $s_dataname, date('Y-m-d',$i_next_billdate_referree), $a_reverts)) {
			foreach($a_reverts as $a_revert)
				mlavu_query($a_revert['string'], $a_revert['vars']);//poslavu_MAIN_db
			return 'failed to update database';
		}
		if (count($a_signup_referrer) > 0 && $b_award_free_month_to_referrer) {
			if (!redeem_referral_code_set_reset_arb_amount($a_billing_profiles_referrer['subscriptionid'], $s_dataname_referrer, date('Y-m-d',$i_next_billdate_referrer), $a_reverts)) {
				foreach($a_reverts as $a_revert)
					mlavu_query($a_revert['string'], $a_revert['vars']);//poslavu_MAIN_db
				return 'failed to update database';
			}
		}

		// update authorize.net for both
		if (!redeem_referral_code_authorize_dot_net($a_billing_profiles, $a_billing_profiles_referrer, 0.01, 0.01, $a_revert, $now, $s_dataname, $s_dataname_referrer, $a_restaurant, $a_restaurant_referrer, $s_user, $s_full_username, $i_invoice_id_referree, $i_invoice_id_referrer, $b_award_free_month_to_referree, $b_award_free_month_to_referrer)) {
			foreach($a_reverts as $a_revert)
				mlavu_query($a_revert['string'], $a_revert['vars']);//poslavu_MAIN_db
			return 'could not update automatic recurring billing';
		}

		return 'success';
	}

	if (!isset($b_dont_draw_referrals) || !$b_dont_draw_referrals) {
		draw_main_referral_page();
	}
	echo "\n\n<!--<message_start>";
	if (isset($_GET['ajax'])) {
		switch($_POST['action']) {
		case 'get_referral_code':
			echo get_referral_code($_POST['dn']);
			break;
		case 'get_promo_code':
			echo get_promo_code($_POST['dn']);
			break;
		case 'set_promo_code_not_used':
			echo set_promo_code_not_used($_POST['dn']);
			break;
		case 'apply_referral_code':
			echo apply_referral_code($_POST['dn'], '', $_POST['code'], FALSE);
			break;
		case 'redeem_referral_code':
			echo redeem_referral_code($_POST['dn'], admin_info('username'), admin_info('fullname'));
			break;
		case 'check_reset_arb':
			$a_resets = redeem_referral_code_check_reset_arb($_POST['dn'], $_POST['date']);
			echo '<div style="text-align:left;"><pre>'.(count($a_resets) > 0 ? print_r($a_resets,TRUE) : 'no resets found').'</pre></div>';
			break;
		case 'reset_arb':
			$a_resets = redeem_referral_code_reset_arb($_POST['dn'], $_POST['date']);
			echo '<div style="text-align:left;"><pre>'.(count($a_resets) > 0 ? print_r($a_resets,TRUE) : 'no resets found').'</pre></div>';
			break;
		}
	}
	echo "<message_start>-->\n\n";

	?>
	<script type='text/javascript'>
		function get_referral_code() {
			var dn=$('#get_referral_code_dn').val();
			$('#get_referral_code_output').html('');
			$.ajax({
				async: true,
				cache: false,
				url: '?mode=refer_somebody&ajax=1',
				data: { action: 'get_referral_code', dn: dn },
				type: 'POST',
				success: function(message) {
					message = message.split('<message_start>')[1];
					if (message == '')
						message = 'referral code not set';
					$('#get_referral_code_output').html(message);
				},
			});
		}

		function get_promo_code() {
			var dn=$('#get_promo_code_dn').val();
			$('#get_promo_code_output').html('');
			$.ajax({
				async: true,
				cache: false,
				url: '?mode=refer_somebody&ajax=1',
				data: { action: 'get_promo_code', dn: dn },
				type: 'POST',
				success: function(message) {
					message = message.split('<message_start>')[1];
					if (message == '')
						message = 'promo code not set';
					$('#get_promo_code_output').html(message);
				},
			});
		}

		function set_promo_code_not_used() {
			var dn=$('#get_promo_code_dn').val();
			$('#set_promo_code_not_used_output').html('');
			$.ajax({
				async: true,
				cache: false,
				url: '?mode=refer_somebody&ajax=1',
				data: { action: 'set_promo_code_not_used', dn: dn },
				type: 'POST',
				success: function(message) {
					message = message.split('<message_start>')[1];
					if (message == '')
						message = 'failed to update';
					$('#set_promo_code_not_used_output').html(message);
					get_promo_code();
				},
			});
		}

		function apply_referral_code() {
			var code=$('#apply_referral_code_code').val();
			var dn=$('#apply_referral_code_dn').val();
			$('#apply_referral_code_output').html('');
			$.ajax({
				async: true,
				cache: false,
				url: '?mode=refer_somebody&ajax=1',
				data: { action: 'apply_referral_code', dn: dn, code: code },
				type: 'POST',
				success: function(message) {
					message = message.split('<message_start>')[1];
					if (message == '')
						message = 'no returned string';
					$('#apply_referral_code_output').html(message);
				},
			});
		}

		function redeem_referral_code() {
			var dn=$('#redeem_referral_code_dn').val();
			$('#redeem_referral_code_output').html('');
			$.ajax({
				async: true,
				cache: false,
				url: '?mode=refer_somebody&ajax=1',
				data: { action: 'redeem_referral_code', dn: dn },
				type: 'POST',
				success: function(message) {
					message = message.split('<message_start>')[1];
					if (message == '')
						message = "something's wrong";
					$('#redeem_referral_code_output').html(message);
				},
			});
		}

		function check_reset_arb() {
			var dn=$('#check_reset_arb_dn').val();
			var date=$('#check_reset_arb_date').val();
			$('#check_reset_arb_output').html('');
			$.ajax({
				async: true,
				cache: false,
				url: '?mode=refer_somebody&ajax=1',
				data: { action: 'check_reset_arb', dn: dn, date: date },
				type: 'POST',
				success: function(message) {
					message = message.split('<message_start>')[1];
					if (message == '')
						message = "something's wrong";
					$('#check_reset_arb_output').html(message);
				},
			});
		}

		function reset_arb() {
			var dn=$('#check_reset_arb_dn').val();
			var date=$('#check_reset_arb_date').val();
			$('#reset_arb_output').html('');
			$.ajax({
				async: true,
				cache: false,
				url: '?mode=refer_somebody&ajax=1',
				data: { action: 'reset_arb', dn: dn, date: date },
				type: 'POST',
				success: function(message) {
					message = message.split('<message_start>')[1];
					if (message == '')
						message = "something's wrong";
					$('#reset_arb_output').html(message);
					check_reset_arb();
				},
			});
		}
	</script>
	<?php

	if ($data_name == '17edison' && (!isset($b_dont_draw_referrals) || !$b_dont_draw_referrals)) {
		?>
		<div>
			<br /><br />
			Get referral code for: <input type='textbox' size='20' placeholder='dataname' id='get_referral_code_dn' /> <input type='button' onclick='get_referral_code();' value='Submit' /> <font id='get_referral_code_output'>&nbsp;</font><br />
			<br /><br />
			Get promo code for: <input type='textbox' size='20' placeholder='dataname' id='get_promo_code_dn' /> <input type='button' onclick='get_promo_code();' value='Submit' /> <font id='get_promo_code_output'>&nbsp;</font> <input type='button' onclick='set_promo_code_not_used();' value='Set To Not Used' /> <font id='set_promo_code_not_used_output'>&nbsp;</font><br />
			<br /><br />
			Apply a referral code to a restaurant: <input type='textbox' size='20' placeholder='code' id='apply_referral_code_code' /> <input type='textbox' size='20' placeholder='dataname' id='apply_referral_code_dn' /> <input type='button' onclick='apply_referral_code();' value='Submit' /> <font id='apply_referral_code_output'>&nbsp;</font><br />
			<br /><br />
			Redeem a referral code for a restaurant: <input type='textbox' size='20' placeholder='dataname' id='redeem_referral_code_dn' /> <input type='button' onclick='redeem_referral_code();' value='Submit' /> <font id='redeem_referral_code_output'>&nbsp;</font><br />
			<br /><br />
			Check for reset ARB rows in a restaurant: <input type='textbox' size='20' placeholder='dataname' id='check_reset_arb_dn' /> <input type='textbox' size='20' placeholder='date (yyyy-mm-dd)' id='check_reset_arb_date' /> <input type='button' onclick='check_reset_arb();' value='Submit' /> <font id='check_reset_arb_output'>&nbsp;</font><br />
			<br /><br />
			Reset ARB rows in a restaurant: <input type='button' onclick='reset_arb();' value='Reset Arbs' /> <font id='reset_arb_output'>&nbsp;</font><br />
		</div>
		<?php
	}
}

?>
