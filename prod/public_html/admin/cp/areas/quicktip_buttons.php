<?php	
	if($in_lavu) {

		$form_posted = (isset($_POST['posted']));
		
		if($form_posted) {
		
			$q_update = "";
			$q_fields = "";
			$q_values = "";
			$b_vars = array();
			$b_vars['calc1'] = $_POST['calc1'];
			$b_vars['calc2'] = $_POST['calc2'];
			$b_vars['calc3'] = $_POST['calc3'];
			$keys = array_keys($b_vars);
			foreach ($keys as $key) {
				if ($q_update != "") { $q_update .= ", "; }
				$q_update .= "`$key` = '[$key]'";
			}
			$b_vars['loc_id'] = $locationid;
			$keys = array_keys($b_vars);
			foreach ($keys as $key) {
				if ($q_fields != "") { $q_fields .= ", "; $q_values .= ", "; }
				$q_fields .= "`$key`";
				$q_values .= "'[$key]'";
			}
			
			$check_buttons = lavu_query("SELECT * FROM `quick_tip_buttons` WHERE `loc_id` = '[1]' LIMIT 1", $locationid);
			if (mysqli_num_rows($check_buttons) > 0) $update_buttons = lavu_query("UPDATE `quick_tip_buttons` SET $q_update WHERE `loc_id` = '[loc_id]' ", $b_vars);
			else $create_buttons = lavu_query("INSERT INTO `quick_tip_buttons` ($q_fields) VALUES ($q_values)", $b_vars);
			
			$check_config = lavu_query("SELECT `id` FROM `config` WHERE `location` = '[1]' AND `type` = 'location_config_setting' AND `setting` = 'quick_tip_entry_mode'", $locationid);
			if (mysqli_num_rows($check_config) > 0) {
				$cinfo = mysqli_fetch_assoc($check_config);
				$update_config = lavu_query("UPDATE `config` SET `value` = '[1]' WHERE `id` = '[2]'", $_POST['quick_tip_entry_mode'], $cinfo['id']);
			} else $insert_config = lavu_query("INSERT INTO `config` (`location`, `type`, `setting`, `value`) VALUES ('[1]', 'location_config_setting', 'quick_tip_entry_mode', '[2]')", $locationid, $_POST['quick_tip_entry_mode']);
			
			$check_config = lavu_query("SELECT `id` FROM `config` WHERE `location` = '[1]' AND `type` = 'location_config_setting' AND `setting` = 'tip_adjust_befor_after_tax'", $locationid);
			if (mysqli_num_rows($check_config) > 0) {
				$cinfo = mysqli_fetch_assoc($check_config);
				$update_config = lavu_query("UPDATE `config` SET `value` = '[1]' WHERE `id` = '[2]'", $_POST['tip_adjust_befor_after_tax'], $cinfo['id']);
			} else $insert_config = lavu_query("INSERT INTO `config` (`location`, `type`, `setting`, `value`) VALUES ('[1]', 'location_config_setting', 'tip_adjust_befor_after_tax', '[2]')", $locationid, $_POST['tip_adjust_befor_after_tax']);
				
			
			$location_info['quick_tip_entry_mode'] = $_POST['quick_tip_entry_mode'];
			$location_info['tip_adjust_befor_after_tax'] = $_POST['tip_adjust_befor_after_tax'];
		}
		
		$get_buttons = lavu_query("SELECT * FROM `quick_tip_buttons` WHERE `loc_id` = '$locationid' LIMIT 1");
		if (mysqli_num_rows($get_buttons) > 0) {
			$button_info = mysqli_fetch_assoc($get_buttons);
		} else {
			$button_info['calc1'] = "0.18";
			$button_info['calc2'] = "0.20";
			$button_info['calc3'] = "0.22";
		}
		
		$qtem_amt_selected = ($location_info['quick_tip_entry_mode']=="amount")?" SELECTED":"";
		$qtem_amtt_selected = ($location_info['tip_adjust_befor_after_tax']=="0")?" SELECTED":"";
	}
?>

<br><br>
<table cellspacing='0' cellpadding='3'>
	<!--<tr><td align='center'>Customizable signature screen tip buttons will be available in POSLavu version 2.0</td></tr>-->
	<tr><td>&nbsp;</td></tr>
	<tr>
		<td style='border:2px solid #DDDDDD' align='center'>
			<form name='ing' method='post' action=''>
				<input type='hidden' name='posted' value='1'>
					<table>
						<tr>
							<td>
								<table cellspacing=0 cellpadding=0>
									<tbody>
										<tr>
											<td></td>
											<td align='center'><?php echo speak("Tip Value"); ?></td>
										</tr>
										<tr>
											<td><?php echo speak("Button")." 1"; ?> &nbsp;</td>
											<td><input alt='<?php echo speak("Tip Value"); ?>' title='<?php echo speak("Tip Value"); ?>' type='text' size='15' name='calc1' value='<?php echo $button_info['calc1']; ?>' style='color: #1c591c; ' onfocus='this.style.color = "#1c591c"; if(this.value=="<?php echo speak("Tip Value"); ?>") this.value = ""; ' onblur='if(this.value=="") {this.value = "<?php echo speak("Tip Value"); ?>"; this.style.color = "#aaaaaa";} '></td>
										</tr>
										<tr>
											<td><?php echo speak("Button")." 2"; ?> &nbsp;</td>
											<td><input alt='<?php echo speak("Tip Value"); ?>' title='<?php echo speak("Tip Value"); ?>' type='text' size='15' name='calc2' value='<?php echo $button_info['calc2']; ?>' style='color: #1c591c; ' onfocus='this.style.color = "#1c591c"; if(this.value=="<?php echo speak("Tip Value"); ?>") this.value = ""; ' onblur='if(this.value=="") {this.value = "<?php echo speak("Tip Value"); ?>"; this.style.color = "#aaaaaa";} '></td>
										</tr>
										<tr>
											<td><?php echo speak("Button")." 3"; ?> &nbsp;</td>
											<td><input alt='<?php echo speak("Tip Value"); ?>' title='<?php echo speak("Tip Value"); ?>' type='text' size='15' name='calc3' value='<?php echo $button_info['calc3']; ?>' style='color: #1c591c; ' onfocus='this.style.color = "#1c591c"; if(this.value=="<?php echo speak("Tip Value"); ?>") this.value = ""; ' onblur='if(this.value=="") {this.value = "<?php echo speak("Tip Value"); ?>"; this.style.color = "#aaaaaa";} '></td>
										</tr>
										<tr>
											<td align='center' colspan='2' style='padding:5px 0px 5px 0px;'>
												<table>
													<tr><td align='right'><?php echo speak("Entry mode").":"; ?></td><td><select name='quick_tip_entry_mode'><option value='percentage'><?php echo speak("Percentage"); ?></option><option value='amount'<?php echo $qtem_amt_selected; ?>><?php echo speak("Amount"); ?></option></select></td></tr>
													<tr><td align='right'><?php echo speak("Calculate tip").":"; ?></td><td><select name='tip_adjust_befor_after_tax'><option value='1'><?php echo speak("Before tax"); ?></option><option value='0'<?php echo $qtem_amtt_selected; ?>><?php echo speak("After tax"); ?></option></select></td></tr>
												</table>
											</td>
										</tr>
									</tbody>
								</table>
							</td>
						</tr>
					</table>
				<input type='submit' value='<?php echo speak("Save"); ?>' style='width:120px'>
			</form>
		</td>
	</tr>
</table>