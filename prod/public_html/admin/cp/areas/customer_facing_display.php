<?php 
$dataname=$_SESSION[last_data_name];
$error=array();
if(!empty($_POST['save'])){
    $result = lavu_query("UPDATE `config` SET `value2`='$_POST[time_interval]' WHERE `setting`='CustomerFacingDisplay' and value='time_interval' "); 
    $valid_ext = array( "jpg", "jpeg", "gif", "png", "bmp"); // whitelist of file extensions
    $error=upload_images($dataname, $_FILES, $valid_ext);
}
$customer_facing_data=getConfigCustomerFacingDisplayOptions();
$path = "//admin.poslavu.com/images/".$dataname."/";
$html="<div style='padding:10px;'>";

if($error[extension]!="" ){
    $html.="<div style='color:#FF0000;font-size:14px;margin-bottom:5px;'>Please check ".ucwords(str_replace("_"," ",trim($error[extension],',')))." are not a valid image</div>";
}
if($error[size]!=""   ){
    $html.="<div style='color:#FF0000;font-size:14px;margin-bottom:5px;' > Please check ".ucwords(str_replace("_"," ",trim($error[size],',')))." file are  too large. File must be less than 15 KB </div>";
}
$html.= "  <form action='' method='POST' enctype='multipart/form-data'>
	<table style='border-collapse:collapse; border:1px solid black; width:98%;' border='2'>
		<tr>
			<th style='text-align:center; border:1px solid black; background-color:#aecd37; color:white;padding: 7px;width: 24%;'>".speak('Customer Facing Display')."</th>
		</tr>
		<tr>
			<td>
                <div >
                     <div style='width: 40%;float:left;padding: 10px;'>".speak('Background Image')."</div> 
                     <div style='width: 30%;float:left;padding: 10px;'><input type='file' name='background_image'/></div>  
                    <div>";if($customer_facing_data[background_image]!=''){ $html.="<img src='".$path.$customer_facing_data[background_image]."' style='max-height:60px'>";} $html.=" </div>
                </div>
            </td>

		</tr>
        <tr>
			<td>
                <div >
                     <div style='width: 40%;float:left;padding: 10px;'>".speak('Display Logo')."</div> 
                     <div style='width: 30%;float:left;padding: 10px;'><input type='file' name='display_logo'/></div>  
                     <div>";if($customer_facing_data[display_logo]!=''){ $html.="<img src='".$path.$customer_facing_data[display_logo]."' style='max-height:60px'>";} $html.=" </div> 
                </div>
            </td>

		</tr>
        <tr>
			<td>
                <div >
                     <div style='width: 40%;float:left;padding: 10px;'>".speak('Screen Saver Images')."</div> 
                     <div style='width: 30%;float:left;padding: 10px;'><input type='file' name='screen_saver1'/></div>  
                     <div>";if($customer_facing_data[screen_saver1]!=''){ $html.="<img src='".$path.$customer_facing_data[screen_saver1]."' style='max-height:60px'>";} $html.=" </div>
                </div>
                <div >
                     <div style='width: 40%;float:left;padding: 10px;'>".speak('Screen Saver Images 2 ')."</div> 
                     <div style='width: 30%;float:left;padding: 10px;'><input type='file' name='screen_saver2'/></div>  
                    <div>";if($customer_facing_data[screen_saver2]!=''){ $html.="<img src='".$path.$customer_facing_data[screen_saver2]."' style='max-height:60px'>";} $html.=" </div> 
                </div>
                <div >
                     <div style='width: 40%;float:left;padding: 10px;'>".speak('Screen Saver Images 3')."</div> 
                     <div style='width: 30%;float:left;padding: 10px;'><input type='file' name='screen_saver3'/></div>  
                     <div>";if($customer_facing_data[screen_saver3]!=''){ $html.="<img src='".$path.$customer_facing_data[screen_saver3]."' style='max-height:60px'>";} $html.=" </div>
                </div>
                <div >
                     <div style='width: 40%;float:left;padding: 10px;'>".speak('Screen Saver Images 4')."</div> 
                     <div style='width: 30%;float:left;padding: 10px;' ><input type='file' name='screen_saver4'/></div>  
                     <div>";if($customer_facing_data[screen_saver4]!=''){ $html.="<img src='".$path.$customer_facing_data[screen_saver4]."' style='max-height:60px'>";} $html.=" </div>
                </div>
               
            </td>

		</tr>
        <tr>
			<td>
                <div>
                     <div style='width: 40%;float:left;padding: 10px;'>".speak('Promotion Images')."</div> 
                     <div style='width: 30%;float:left;padding: 10px;'><input type='file' name='promotion1'/></div>  
                     <div>";if($customer_facing_data[promotion1]!=''){ $html.="<img src='".$path.$customer_facing_data[promotion1]."' style='max-height:60px'>";} $html.=" </div> 
                </div>
                <div>
                     <div style='width: 40%;float:left;padding: 10px;'>".speak('Promotion Images 2')."</div> 
                     <div style='width: 30%;float:left;padding: 10px;'><input type='file' name='promotion2' /></div>  
                    <div>";if($customer_facing_data[promotion2]!=''){ $html.="<img src='".$path.$customer_facing_data[promotion2]."' style='max-height:60px'>";} $html.=" </div> 
                </div>
                <div>
                     <div style='width: 40%;float:left;padding: 10px;'>".speak('Promotion Images 3')."</div> 
                     <div style='width: 30%;float:left;padding: 10px;'><input type='file' name='promotion3' /></div>  
                    <div>";if($customer_facing_data[promotion3]!=''){ $html.="<img src='".$path.$customer_facing_data[promotion3]."' style='max-height:60px'>";} $html.=" </div> 
                </div>
                <div>
                     <div style='width: 40%;float:left;padding: 10px;'>".speak('Promotion Images 4')."</div> 
                     <div style='width: 30%;float:left;padding: 10px;'><input type='file' name='promotion4'/></div>  
                     <div>";if($customer_facing_data[promotion4]!=''){ $html.="<img src='".$path.$customer_facing_data[promotion4]."' style='max-height:60px'>";} $html.=" </div> 
                </div>
           
            </td>

		</tr>
         <tr>
			<td>
                <div >
                     <div style='width: 40%;float:left;padding: 10px;'>".speak('Time Interval')."</div> 
                     <div style='width: 30%;float:left;padding: 10px;'>
                        <select name='time_interval' style='width:100px;' >
                            ";
                        
                        for($i=1; $i<11;$i++){
                            if($i==$customer_facing_data[time_interval]){ $sec='selected'; }else{ $sec=''; }
                            $html.= "<option $sec  value=".$i.">".$i."</option>";
                        }
$html.= "            </select>
                     </div>  
                    
                </div>
            </td>

		</tr>
      <tr>
        <td>
                     <div style='margin-left: 44%;'><input type='submit' name='save' value='".speak('Save')."' class='addBtn'></div>
           </td>
		</tr>
        </form>
        </table>
</div>";
echo $html;
//Database functions below
function getConfigCustomerFacingDisplayOptions(){
    $result = lavu_query("SELECT * FROM `config` WHERE `setting`='CustomerFacingDisplay'");
    if(!mysqli_num_rows($result)){
        insertInitialRowsIntoConfig();
    }
    $result = lavu_query("SELECT id,value,value2 FROM `config` WHERE `setting`='CustomerFacingDisplay'");
    while($customer_facing_read = mysqli_fetch_assoc($result)){
        $customer_facing_data[$customer_facing_read[value]]=$customer_facing_read[value2];
    } 
    return $customer_facing_data;
}
function insertInitialRowsIntoConfig(){
    $loc_id=$_SESSION[sessname('locationid')];
    //Keeping the columns in same internal order as the `config` table.
    $initQ  = "INSERT INTO `config` ";
    $initQ .= "(`location`,`setting`,`value`,`value2`,`type`,`_deleted`) ";
    $initQ .= "VALUES ";
        //      loc       setting                value       value2    type   _del 
    $initQ .= "($loc_id, 'CustomerFacingDisplay', 'background_image', '','file', '0'),";//Back Ground Image
        $initQ .= "($loc_id, 'CustomerFacingDisplay', 'display_logo',  '', 'file', '0'),";//Display Logo Image 
        $initQ .= "($loc_id, 'CustomerFacingDisplay', 'screen_saver1',  '','file', '0'),";//screen Saver Image
        $initQ .= "($loc_id, 'CustomerFacingDisplay', 'screen_saver2',  '','file', '0'),";//screen Saver Image
        $initQ .= "($loc_id, 'CustomerFacingDisplay', 'screen_saver3', '', 'file',  '0'),";//screen Saver Image
        $initQ .= "($loc_id, 'CustomerFacingDisplay', 'screen_saver4', '', 'file',  '0'),";//screen Saver Image
        $initQ .= "($loc_id, 'CustomerFacingDisplay', 'promotion1', '', 'file',  '0'),";//Promotion Image
        $initQ .= "($loc_id, 'CustomerFacingDisplay', 'promotion2', '', 'file',  '0'),";//Promotion Image
        $initQ .= "($loc_id, 'CustomerFacingDisplay', 'promotion3', '', 'file',  '0'),";//Promotion Image
        $initQ .= "($loc_id, 'CustomerFacingDisplay', 'promotion4', '', 'file',  '0'),";//Promotion Image
        $initQ .= "($loc_id, 'CustomerFacingDisplay', 'time_interval', '5', 'select', '0')";// Time Interval sec
   
     //   echo $initQ; exit;
    $insertResult = lavu_query($initQ);
    if(!$insertResult){
        echo "SQL error when trying to perform initialization insert, mysql error: " . lavu_dberror();
        exit();
    }
}
function upload_images($dataname, &$a_files, &$valid_ext)
{
    $extension=''; $size='';
    $base_path = __DIR__."/../../images/".$dataname."/";
    if (!is_dir($base_path))
    {
        mkdir($base_path,0775,true);
    }
    $code=time();
    foreach ($a_files as $key=>$files){
        if($files['name']!=''){
            if (!in_array(end(explode(".", strtolower($files['name']))), $valid_ext))
            {
                $extension=$extension.$key.",";
            }else{
                if($files['size'] > '15361'){
                    $size=$size.$key.",";
                }
                else{
                    $basename = basename($files['name']);
                    $target = $base_path . $basename; // append file to path explode
                    $filename = explode(".",$basename);
                    $filename[0] = str_replace(" ", "_", $filename[0]); // replace whitespaces in filename with underscore
                    $myImage = new Imagick($files['tmp_name']);
                    $myImage->thumbnailImage(300, 300, true);
                    $myImage->writeImage($base_path.$filename[0]."_thumb.".$filename[1]);
                    $myImage->thumbnailImage(300, 300, true);
                    $image_name=$key."_".$code.".".$filename[1];
                    $myImage->writeImage($base_path.$image_name);
                    $result = lavu_query("UPDATE `config` SET `value2`='$image_name' WHERE `setting`='CustomerFacingDisplay' and value='$key' ");
                }
            }
        }
    }
    $ex_size[extension]=$extension;
    $ex_size[size]=$size;
    return  $ex_size;
}
?>