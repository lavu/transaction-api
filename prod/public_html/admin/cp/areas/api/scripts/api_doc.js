
function show(id, callingEle)
{
	//$('#' + id).css("display", "block");
	var ele = document.getElementById(id);
	ele.setAttribute("style", "display: block;");
	ele.setAttribute("width", "600px");
	
	callingEle.setAttribute("onClick", "hide(\'" + id + "\', this);");
	
}

function hide(id, callingEle)
{
	var ele = document.getElementById(id);
	ele.setAttribute("style", "display: none;");
	
	callingEle.setAttribute("onClick", "show(\'" + id + "\', this);");
}

var currentPage;

function swapPage(id)
{
	var ele = document.getElementById(id);
	if(currentPage)
		currentPage.setAttribute("style", "display: none;");
	
	ele.setAttribute("style", "display: block");
	currentPage = ele;
}