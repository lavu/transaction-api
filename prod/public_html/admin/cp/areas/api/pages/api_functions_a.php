<?php 
include_once('functions.php'); 
?>
<p>Welcomes to the <b>Advanced API Functions</b> section of the API Documentation.  This section contains information about modifying and adding values to the Lavu database.  Since this section is all about creating/modifying data, there will be very few examples/results outlined for viewing purposes.  The entire section will go over, in detail, the functionality of the commands for the API Calls, and what will be required when making these commands.</p>
<p>Here is the list of commands that are covered in this section:</p>
<li><code class="params"><dfn>list</dfn></code></li>
<li><code class="params"><dfn>update</dfn></code></li>
<li><code class="params"><dfn>insert</dfn></code></li>
<li><code class="params"><dfn>replace</dfn></code></li>
<br />
<h3>Basic Syntax</h3>
<hr />
<p>For every command used in this section, the syntax is identical.  This is the basic syntax of every command:</p>
<br />
SYNTAX:<br />
<code class="params">&<var>cmd</var>=<dfn>&lt;CMD></dfn>&<var>contents</var>=<dfn>&lt;CONTENTS_STR></dfn></code><br />
<br />
DEFINITION:<br />
<p>The only command specified that doesn't use <code class="params"><var>contents</var></code>, is the <code class="params"><dfn>list</dfn></code> command, which is the default command.  The <code class="params"><dfn>list</dfn></code> command just returns data, whereas the other commands insert, or modify data.</p>
<li><code class="params"><var>cmd</var></code>: The command to run with the API Call, please note that <b>not</b> every command will work with filters.</li>
<li><code class="params"><var>contents</var></code>: Required for every command except list.  This refers to the contents you want to insert/modify into the database table.  The contents <b>must</b> be provided in the same style as the default output for an API Call with the list command:</li>
<div class="code_examp">
	<code class="xml">
&lt;<var>row</var>><br />
&nbsp;&nbsp;&lt;<var>field</var>><dfn>value</dfn>&lt;/<var>field</var>><br />
&lt;/<var>row</var>><br />
	</code>
</div>
<br />
<p>Please note that for specific commands you will need to specify certain things.  Each following section will outline each of the commands in detail.</p>
<br />




<h3>List</h3>
<hr />
<p>The <b>List</b> Command is the command that is used by default when no other command has been specified by the API Call.  This command will output the results of the API Call utilizing the information provided to, as outlined in <span class="section" onclick="swapPage('api_functions');">API Functions</span>.</p><br />
SYNTAX:<br />
<code class="params">&<var>cmd</var>=<dfn>list</dfn></code><br />
<br />
RESULT:<br />
<p>The data as specified by the parameters of your API Call. See <span class="section" onclick="swapPage('query_list');">Query Results</span> for example output.</p><br />





<h3>Update</h3>
<hr />
<p>The <b>Update</b> Command will update the table specified with the contents specified by <code class="params"><dfn>&lt;CONTENTS_STR></dfn></code>.  please note, that if you are using the <b>Update</b> Command, you will be required to specify an id as a field in the contents.</p>
<br />
SYNTAX:<br />
<code class="params">&<var>cmd</var>=<dfn>update</dfn>&<var>contents</var>=<dfn>&lt;CONTENTS_STR></dfn></code><br />
<br />
DEFINITION:<br />
<li><code class="params"><var>contents</var></code>: (REQUIRED) The fields you'd like to update within the entry must be contained within the contents specified. Please note, that because you are using the update command, you will be required to specify an <b>id</b> within the contents, so the API Call knows which specific record to update.  If you are using the <b>orders</b> table, then you are able to use <b>order_id</b> instead of <b>id</b>. But, you should get into the habit of specifying the <b>id</b> of the entry you are attempting to update.</li>
<p>Please note, that you can specify multiple entries to update at once, provided they are in the same table, by specifying a separate <b>row</b> tag pair for each entry.  Another thing to note, is that you can only update with fields that exist within the current table.  If you provide a field for a table that doesn't exist, the result cannot be guaranteed to be successful.</p><br />
RESULT:<br />
<p>On Success, update will return the <b>id</b>/<b>order_id</b> (depending on what was used) of the updated object, or 0, if it failed.</p><br />
<br />
EXAMPLE:<br />
<div style="width: 80%; margin: 0 auto;">
<?php
	function update_examp()
	{
		//echo phpinfo();
		global $dataname;
		global $api_key;
		global $api_token;
		
		$params = generateVars($dataname,$api_key,$api_token,"order_contents","0,2") . "&column=order_id&value=1-1";
		$response = post_to_api($params);
		$json = responseToJSON($response, false);
		
		$json = str_ireplace("\n", "", $json);
		$json = str_ireplace("\t", "", $json);
		$json = str_ireplace("/", "|", $json);
		
		$obj = json_decode($json);
		echo "Sample Data to Update<br />";
		echo "<div class=\"code_examp\">";
		
		$vars = Array( "id",
					   "loc_id",
					   "order_id",
					   "item",
					   "price",
					   "quantity",
					   "check",
					   "seat",
					   "item_id",
					   "printer",
					   "course",
					   "subtotal",
					   "subtotal_with_mods",
					   "void",
					   "device_time");
		
		
		
		if($obj)
		{
			$str = "";
			foreach($obj as $key => $val) //Each Row
			{
				$inner = "";
				foreach($val as $k => $v)
				{
					if(in_array($k, $vars))
					$inner .= "\t<$k>$v</$k>\n";
				}
				
				$str .= "<row>\n$inner</row>\n";
				//echo "$key => $val";
			}
			
			$str = htmlspecialchars($str);
			echo "<pre>";
			echo $str;
			$str = str_ireplace("\n", "", $str);
			$str = str_ireplace("\t", "", $str);
			echo "</pre>";
			echo "</div><br />";
			echo "INPUT:<br />";
			$p = "<code class=\"params\"><var>dataname</var>=<dfn>$dataname</dfn>&<var>key</var>=<dfn>$key</dfn>&<var>token</var>=<dfn>$token</dfn>&<var>table</var>=<dfn>order_contents</dfn>&<var>cmd</var>=<dfn>update</dfn>&<var>contents</var>=<dfn>$str</dfn></code><br />";
			echo $p;
			echo "<br />EXAMPLE RESULT:<br />";
			echo "<div class=\"code_examp\">";
			
			$res = "";
			foreach($obj as $key => $val)
			{
				$inner = "";
				foreach($val as $k => $v)
				{
					if($k != "id" && $k != "order_id")
						continue;
					
					$inner.= "\t<$k>$v</$k>\n";
						
				}
				$res .= "<result>\n$inner</result>\n";
			}
			echo "<pre>" . htmlspecialchars($res) . "</pre>";
			echo "</div>";
		}
		else
		{
			echo "</div>";
		}
	}
	
	update_examp();

	
?>
</div>



<h3>Insert</h3>
<hr />
<p>The <b>Insert</b> Command works very much like the <b>Update</b> Command.  The exception is that for <b>Insert</b>, you should not provide an <b>id</b> for the entries being inserted.</p><br />
SYNTAX:<br />
<code class="params">&<var>cmd</var>=<dfn>insert</dfn>&<var>contents</var>=<dfn>&lt;CONTENTS_STR></dfn></code><br />
<br />
DEFINITION:<br />
<li><code class="params"><var>contents</var></code>: (REQUIRED) Much like the <b>Update</b> Command, the <b>Insert</b> Command will input the fields specified by the contents into the specified table.  The distinction, is that you need not provide an <b>id</b> when inserting information into a table.  There is a check to see if you are trying to, and it will stop you if you do.  For other details, please refer to the <b>Update</b> Section.</li><br />
<p>This is incredibly useful if you want to add <b>order_contents</b> to an order.  In order to do this, you must specify the <b>order_id</b> of the item in order for the <b>order_contents</b> to be specified to that particular order.  The same is true of <b>order_transactions</b></p>
RESULT:<br />
<p>On Success, update will return the <b>id</b> of the updated row, or 0, if it failed.  If it succeeded, and it is applicable, it will also return the <b>order_id</b> of the inserted row.</p><br />
<br />
EXAMPLE:<br />
<div style="width: 80%; margin: 0 auto;">
<?php
	function insert_examp()
	{
		//echo phpinfo();
		global $dataname;
		global $api_key;
		global $api_token;
		
		$params = generateVars($dataname,$api_key,$api_token,"menu_groups","0,2");
		$response = post_to_api($params);
		$json = responseToJSON($response, false);
		
		$json = str_ireplace("\n", "", $json);
		$json = str_ireplace("\t", "", $json);
		$json = str_ireplace("/", "|", $json);
		
		$obj = json_decode($json);
		echo "Sample Data to Insert<br />";
		echo "<div class=\"code_examp\">";
		if($obj)
		{
			$str = "";
			foreach($obj as $key => $val) //Each Row
			{
				$inner = "";
				foreach($val as $k => $v)
				{
					if($k != "id")
						$inner .= "\t<$k>$v</$k>\n";
				}
				
				$str .= "<row>\n$inner</row>\n";
				//echo "$key => $val";
			}
			
			$str = htmlspecialchars($str);
			echo "<pre>";
			echo $str;
			$str = str_ireplace("\n", "", $str);
			$str = str_ireplace("\t", "", $str);
			//$str = str_ireplace("\n", "", $str);
			echo "</pre>";
			echo "</div><br />";
			echo "INPUT:<br />";
			$p = "<code class=\"params\"><var>dataname</var>=<dfn>$dataname</dfn>&<var>key</var>=<dfn>$key</dfn>&<var>token</var>=<dfn>$token</dfn>&<var>table</var>=<dfn>menu_groups</dfn>&<var>cmd</var>=<dfn>insert</dfn>&<var>contents</var>=<dfn>$str</dfn></code><br />";
			echo $p;
			echo "<br />EXAMPLE RESULT:<br />";
			echo "<div class=\"code_examp\">";
			
			$res = "";
			foreach($obj as $key => $val)
			{
				$inner = "";
				foreach($val as $k => $v)
				{
					if($k != "id" && $k != "order_id")
						continue;
					
					$inner.= "\t<$k>$v</$k>\n";
						
				}
				$res .= "<result>\n$inner</result>\n";
			}
			echo "<pre>" . htmlspecialchars($res) . "</pre>";
			echo "</div>";
		}
		else
		{
			echo "</div>";
		}
	}
	
	insert_examp();

	
?>
</div>


<h3>Replace</h3>
<hr />
<p>The <b>Replace</b> Command is a powerful command that allows you to make a multiple changes at once. This command is most applicable to the <b>order_contents</b> and <b>order_transactions</b> tables, as these tables may reference the same <b>order_id</b> multiple times.  This command will seek out all <b>id</b>s/<b>order_id</b>s of the value specified and replace all entries with the data provided.  If the contents contain more than one entry with the same <b>id</b>/<b>order_id</b>, it will only delete the entries for the specified <b>id</b>/<b>order_id</b> once, and then add everything afterwards.</p><br />
SYNTAX:<br />
<code class="params">&<var>cmd</var>=<dfn>replace</dfn>&<var>contents</var>=<dfn>&lt;CONTENTS_STR></dfn></code><br />
<br />
DEFINITION:<br />
<li><code class="params"><var>contents</var></code>: (REQUIRED) Much like the <b>Update</b> Command, the <b>Replace</b> Command will require either an <b>id</b> field, or an <b>order_id</b> field when attempting to replace information.  Please refer to the <b>Update</b> Section for specifics about <code class="params"><var>contents</var></code>.</li>
<p><b>Be Wary</b> this command will <b>delete</b> all previous references to <b>id</b>/<b>order_id</b> within the table.  As such this command is <b>not</b> to be used to modify entries.</p><br />
RESULT:<br />
<p>On Success, update will return the <b>id</b> of the updated row, or 0, if it failed.  If it succeeded, and it is applicable, it will also return the <b>order_id</b> of the inserted row.</p><br />
