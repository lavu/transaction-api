<?php
require_once($_SERVER['DOCUMENT_ROOT']."/cp/areas/header.php");
?>
<p>Welcome to the Lavu API Documentation.  This document is meant to outline the functionality, implementation, and feature access of the Lavu API.  Please review the information contained within, and feel free to review our examples, and even use our code, to get the functionality you desire.</p>
<p>In order to facilitate this, we've broken the API up into sections to help organize the information you may be looking for.  The Sections are as follows: <?php global $sections; $list = ""; foreach($sections as $label => $id){ if($list != ""){$list .= ", "; } $list .= " <span class=\"section\" onclick=\"swapPage('$id');\">$label</span>"; echo $list; }?>.  These sections are meant to break down the API into sections that are easy to navigate, and simple to remember.</p>
<p>The first thing to know is that the API works as a POST based request.  Every request that is sent <b>must</b> be done through a POST request, otherwise the function will not resolve.  The next thing to know is that the API requires 3 credentials to be passed in order to operate.  Here's the information for your credentials, as well as the current API URL:</p>
<br />
<li><b>API URL</b>: <?php 
echo $api_url 
?></li>
<li><b>dataname</b>: <?php echo $dataname ?></li>
<li><b>token</b>: <?php echo $api_token ?></li>
<li><b>key</b>: <?php echo $api_key ?></li>
<br />
<p>These are the primary credentials needed in order to use the API.  The only thing required beyond the credentials is the <b>table</b> attribute.  This also must be included in every API call, so the API knows the data types it is working with.  By providing all four of these required fields as post variables to the <b>API URL</b> you will get query results.  This is the most basic usage of the API.</p>
<p>Please note, that the API itself is a synchronous request system, that only allows for a single API request to be processed at a time.  If you try to submit more than one request, per set of API credentials at a time, you may fail to get results for your subsequent submissions.  In addition to the simultaneous restriction for request, there is also a limiter to the number of requests you can make within a time frame.  As of this point in time, this limiter is set to 1000 requests per hour, for a set of credentials.</p>
<p>Next Step: <span class="section" onclick="swapPage('query_list');">Query Results</span></p>
