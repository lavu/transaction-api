<?php

	require_once($_SERVER['DOCUMENT_ROOT']."/cp/areas/header.php");

	if(!(isset($_REQUEST['dataname']) && isset($_REQUEST['api_key']) && isset($_REQUEST['api_token'])))
	{
		echo "Need dataname, api_key, and api_token";
		exit();
	}
	
	function tab($i){
		$result = "";
		for($j = 0; $j < $i; $j++)
		{
			$result .= "&nbsp;&nbsp;";
		}
		return $result;
	}
	
	$default_dataname = "lavu_inc";
	$default_api_key = "zXQboIuIvSTpjEfqsGY6";
	$default_api_token = "LVSLnELK7nj6LAi9TcSO";
	
	$dataname = $_REQUEST['dataname'];
	$api_key = $_REQUEST['api_key'];
	$api_token = $_REQUEST['api_token'];

	$sub_directory = substr($_SERVER['REQUEST_URI'], 0, 4);
	$api_url = "https://api.poslavu.com{$sub_directory}reqserv/";
	
	function post_to_api($postvars)
	{
		global $api_url;
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, str_replace("https", "http", $api_url));
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $postvars);
		$response = curl_exec($ch);
		curl_close ($ch);
		
		return $response;
	}
	
	function generateVars($dataname, $key, $token, $table, $limit="0,10")
	{
		return "dataname={$dataname}&key={$key}&token={$token}&table={$table}&valid_xml=1&limit={$limit}";
	}

	$tables = Array(
		"clock_punches",
		"devices",
		"discount_groups",
		"discount_types",
		"emp_classes",
		"forced_modifier_groups",
		"forced_modifier_lists",
		"forced_modifiers",
		"happyhours",
		"ingredient_categories",
		"ingredient_usage",
		"ingredients",
		"locations",
		"meal_period_types",
		"meal_periods",
		"med_customers",
		"menu_categories",
		"menu_groups",
		"menu_items",
	    	"menu_item_meta_data_link_details",
		"modifier_categories",
		"modifiers",
		"modifiers_used",
	    	"nutrition_info_link_details",
		"order_checks",
		"order_contents",
		"order_payments",
		"orders",
		"payment_types",
		"revenue_centers",
		"super_groups",
		"tables",
		"tax_profiles",
		"tax_rates",
		"users"
		);

	function getInformation()
	{
		global $dataname;
		global $api_key;
		global $api_token;
		global $api_url;
		global $tables;

				
		foreach($tables as $t)
		{
			$postvars = generateVars($dataname, $api_key, $api_token, $t);
			echo "<a name=\"query_results_{$t}\"><a>";
			echo "<h2>{$t}</h2><hr />";
			echo getTableInformationForTableName( $t );
			//echo "<b>Request URL: </b>" .$api_url . "?" .$postvars . "<br />";
			$response = post_to_api($postvars);
			//echo $response . "<br />";
			$str = "<var>dataname</var>=<dfn>{$dataname}</dfn>&<var>key</var>=<dfn>{$api_key}</dfn>&<var>token</var>=<dfn>{$api_token}</dfn>&<var>table</var>=<dfn>${t}</dfn>";
			echo "Actual passed paramaters: <br/>";
			echo "Raw : <br/><code class=\"params\">$str</code><br />";
			echo "XML : <br/><code class=\"params\">$str&<var>valid_xml</var>=<dfn>1</dfn></code><br />";
			if ($t == "menu_item_meta_data_link_details" || $t == "nutrition_info_link_details") {
                		echo "JSON : <br/><code class=\"params\">$str&<var>format</var>=<dfn>json</dfn></code><br />";
			}
			echo "<div class=\"code_examp\" onclick='show(\"" . $t . "RAW\", this)';>Show Raw</div>";
			echo "<pre class=\"code_examp\" id='" . $t .  "RAW' name='RAW' style='display: none;'>" . htmlspecialchars($response)  . "</pre>";
			echo "<div class=\"code_examp\" onclick='show(\"" . $t . "XML\", this)';>Show XML</div>";
			echo "<div class=\"code_examp\" id='" . $t .  "XML' name='XML' style='display: none;'><div><code class='xml'>" . responsetoXML($response)  . "</code></div></div>";
			echo "<div class=\"code_examp\" onclick='show(\"" . $t . "JSON\", this)';>Show JSON</div>";
			echo "<div class=\"code_examp\" id='" . $t .  "JSON' name='JSON' style='display: none;'><div><pre class='json'>" . responseToJSON($response) . "</pre></div></div>";
			generateTable($response);
		}
		
	}

	function getTableInformationForTableName( $table ){
		$result = "";
		$file_name = dirname(__FILE__) . "/table_descriptions/{$table}.htm";
		if( file_exists( $file_name ) ){
			$result = '<div class="table_descriptions">' . file_get_contents( $file_name ) . '</div>';
		}

		return $result;
	}
	
	
	function responseToJSON($response, $tags=true)
	{
		$dom = new DOMDocument;
		$dom->loadXML($response);
		
		$doc = $dom->documentElement;
		$first = 0;
		
		$result = "";
		foreach($doc->childNodes as $row)
		{
			if($first % 2 == 1)
			{	
				if($result != "")
					$result .= ",\n";
				$inner = "";
				foreach($row->childNodes as $entry)
					if($entry->nodeName != "#text")
					{
						if($inner != "")
							$inner .= ",\n\t\t";
						$inner .= '"';
						if($tags)
							$inner .= '<var>';
							
						$inner .= $entry->nodeName;
						if($tags)
							$inner.= '</var>';
							
						$inner .= '" : "';
						
						if($tags)
							$inner.= '<dfn>';
							
						$inner .= $entry->nodeValue;
						
						if($tags)
							$inner .= '</dfn>';
						$inner.='"';
					}
					
				$result .= "\t{\n\t\t" . $inner . "\n\t}";
			}
			$first++;
		}
		
		$result = "[\n" . $result . "\n]\n";
		
		return $result;
	}
	
	function responseToXML($response, $tag=array())
	{
		$dom = new DOMDocument;
		$dom->loadXML($response);
		
		$doc = $dom->documentElement;
		$first = 0;
		
		$str = "";
		$str .= "&lt;?<var>xml</var> <strong>version</strong>=\"<dfn>1.0</dfn>\" <strong>encoding</strong>=\"<dfn>utf-8</dfn>\"?><br />";
		$str .= "&lt;<var>results</var>><br />" ; 
		foreach($doc->childNodes as $row)
		{
			if($first %2 == 1)
			{	
				$str .= tab(1) . "&lt;<var>row</var>><br />";
				foreach($row->childNodes as $entry)
				{
					if($entry->nodeName != "#text")
					{
						$str .= tab(2);
						$found = in_array($entry->nodeName, $tag);
						
						if($found)
							$str .= "<b style=\"background-color: white;\">";
						$str .= "&lt;<var>" . $entry->nodeName . "</var>><dfn>" . $entry->nodeValue . "</dfn>&lt;<var>/" . $entry->nodeName . "</var>>";
						if($found)
							$str .= "</b>";						
						$str .= "<br />";
					}
				}
			
				$str .= tab(1) . "&lt;<var>/row</var>><br />";
			}
			$first++;
			
		}
		$str .= "&lt;<var>/results</var>><br />";
		return $str;
	}
	
	function generateTable($response)
	{
		//$rows = explode("</row>", $response);
		//$rows = str_ireplace("<row>", "", $rows);
		
    
		$dom = new DOMDocument;
		$dom->loadXML($response);
		
		$doc = $dom->documentElement;
		$first = 0;
		echo "<div style='overflow: auto; width: 100%;'>";
		echo "<table>";
		
		foreach($doc->childNodes as $row)
		{
			if($first % 2 == 1)
			{
				if($first == 1){
					echo "<thead><tr>";
					foreach($row->childNodes as $entry)
					{
						if($entry->nodeName != "#text")
							echo "<th>" . $entry->nodeName . "</th>";
					}
					echo "</tr></thead><tbody>";
				}
				
				echo "<tr";
					if((($first - 1) / 2) % 2 == 0)
					{
						echo " class='alt' ";
					}
				echo ">";
				foreach($row->childNodes as $entry)
					if($entry->nodeName != "#text")
					{
						echo "<td>" . $entry->nodeValue . "</td>";
					}
				echo "</tr>";
			}
			$first++;
		}
		echo "</tbody></table>";
		echo "</div>";
				
	}
	
	$sections = Array ( "Introduction" => "start",
						"Query Results" => "query_list",
						"API Functions" => "api_functions",
						"Advanced API Functions" => "api_functions_a",
						"Example Code" => "code");
	
	//require_once("../resources/core_functions.php");
	//require_once("../resources/lavuquery.php");
	
	//require_once("api.php");
