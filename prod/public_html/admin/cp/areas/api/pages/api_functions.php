<?php 
require_once("functions.php");
?>
<p>This Page is all about the various functions our API has provided to you.  These functions allow for a level of customization when utilizing the API for the various tasks you require it for.  This section will primarily concern the basic features of the API, whereas the <span class="section" onclick="swapPage('api_functions_a');">Advanced API Functions</span> will address some of the more advanced functionality of the API that you may require.</p><p>Here is a list of the parameters that will be covered in this section:</p>
<li><code class="params"><var>column</var></code></li>
<li><code class="params"><var>value</var></code></li>
<li><code class="params"><var>value_min</var></code></li>
<li><code class="params"><var>value_max</var></code></li>
<li><code class="params"><var>limit</var></code></li>
<li><code class="params"><var>valid_xml</var></code></li>
<li><code class="params"><var>filters</var></code></li>

<!-- <li>cmd</li> -->
<!-- BASIC FILTER BEGIN -->
<h3>Basic Filter</h3>
<hr />
<p>The <b>Basic Filter</b> allows for quick customization of your API Call, regardless of context.  The basic filter is one that allows you to filter the results of an API Call based on what you put into the filter.  The Basic Filter follows the following syntax in POST variables: <code class="params">&<var>column</var>=<dfn>&lt;field_name></dfn>&<var>value</var>=<dfn>&lt;VAL></dfn></code>.  By adding these two params to the POST variables, when calling our API, the results will be filtered by the <code class="params"><dfn>&lt;field_name></dfn></code> by the <code class="params"><dfn>&lt;VAL></dfn></code>.  This is useful specifically for tables like <b>order_contents</b> as the field, <b>order_id</b> will likely appear many times for the same order.</p>
<br />

SYNTAX: <br />
<code class="params">&<var>column</var>=<dfn>&lt;field_name></dfn>&<var>value</var>=<dfn>&lt;VAL></dfn></code><br />
<br />
DEFINITION:
<li><code class="params"><var>column</var></code>: Specifies that the specific table field will be the subject being filtered.</li>
<li><code class="params"><var>value</var></code>: Specifies the specific value that the <code class="params"><var>column</var></code> will need to be equal to.</li> <br />
POTENTIAL PITFALLS:<br />
<p>This filter is entirely reliant on the <code class="params"><var>value</var></code> parameter.  This is essentially an "Is Equals" comparison.  This will only return the results for which the value of the field specified by <code class="params"><var>column</var></code> is equal to the value specified by <code class="params"><var>value</var></code>.</p>
<br />
EXAMPLE: <br />

<b>example 1:</b>
params: <code class="params"><var>dataname</var>=<dfn><?php echo $dataname; ?></dfn>&<var>key</var>=<dfn><?php echo $api_key; ?></dfn>&<var>token</var>=<dfn><?php echo $api_token; ?></dfn>&<var>table</var>=<dfn>order_contents</dfn>&<var>column</var>=<dfn>order_id</dfn>&<var>value</var>=<dfn>1-1</dfn>&<var>valid_xml</var>=<dfn>1</dfn></code><br />
<p><b>results 1:</b></p>
	<div class="code_examp" onclick="show('bfex1', this);">Show Results</div>
	<div class="code_examp" id="bfex1" style="display: none;"><code class="xml">
		<?php echo responseToXML(post_to_api(generateVars($dataname, $api_key, $api_token, "order_contents") . "&column=order_id&value=1-1"), Array("order_id")); ?>
	</code></div>
<!-- BASIC FILTER END -->

<!-- BASIC BETWEEN FILTER BEGIN -->	
<h3>Basic Between Filter</h3>
<hr />
<p>The <b>Basic Between Filter</b> specifies the minimum and maximum values to consider while filtering your result.  Unlike the <b>Basic Filter</b> which results only in results that are equal to the value specified by the parameter <code class="params"><var>&lt;value></var></code>, this returns results bounded by the two parameters <code class="params"><var>&lt;value_min></var></code> and <code class="params"><var>&lt;value_max></var></code>.  In order to use the <b>Basic Between Filter</b>, you must include these three parameters when making the POST call to our API, using the following syntax: <code class="params">&<var>column</var>=<dfn>&lt;field_name></dfn>&<var>value_min</var>=<dfn>&lt;VAL_MIN></dfn>&<var>value_max</var>=<dfn>&lt;VAL_MAX></dfn></code>. This is useful for when you want to look at <b>id</b>s of specific ranges.</p>
<br />

SYNTAX: <br />
<code class="params">&<var>column</var>=<dfn>&lt;field_name></dfn>&<var>value_min</var>=<dfn>&lt;VAL_MIN></dfn>&<var>value_max</var>=<dfn>&lt;VAL_MAX></dfn></code><br />
<br />
DEFINITION:
<li><code class="params"><var>column</var></code>: Specifies the specific table field that will be filtered.</li>
<li><code class="params"><var>value_min</var></code>: Specifies the lower Range of values to filter <code class="params"><var>column</var></code> by.</li>
<li><code class="params"><var>value_max</var></code>: Specifies the upper Range of values to filter <code class="params"><var>column</var></code> by.</li>
POTENTIAL PITFALLS:<br />
<p>The <b>Basic Between Filter</b> requires all three parameters in order to function.  If you are going to use <code class="params"><var>value_min</var></code>, you need to specify <code class="params"><var>value_max</var></code>, and the same also applies in reverse.  You also need to specify a <code class="params"><var>column</var></code> when using this type of filter.  Please note that this filter does not work in conjunction with the <b>Basic Filter</b>, so it will ignore any <code class="params"><var>value</var></code> specified.</p><br />
EXAMPLE: <br />

<b>example 1:</b>
params: <code class="params"><var>dataname</var>=<dfn><?php echo $dataname; ?></dfn>&<var>key</var>=<dfn><?php echo $api_key; ?></dfn>&<var>token</var>=<dfn><?php echo $api_token; ?></dfn>&<var>table</var>=<dfn>menu_groups</dfn>&<var>column</var>=<dfn>id</dfn>&<var>value_min</var>=<dfn>0</dfn>&<var>value_max</var>=<dfn>200</dfn>&<var>valid_xml</var>=<dfn>1</dfn></code><br />
<p><b>results 1:</b></p>
<div class="code_examp" onclick="show('bbfex1', this);">Show Results</div>
<div class="code_examp" id="bbfex1" style="display: none;"><code class="xml">
	<?php
		$vars = generateVars($dataname, $api_key, $api_token, "menu_groups") . "&column=id&value_min=0&value_max=200";
		$response =  post_to_api($vars);
	 echo responseToXML($response, Array("id")) ?>
</code></div>
<!-- BASIC BETWEEN FILTER END -->

<!-- LIMIT FEATURE BEGIN -->
<h3>Limit Command</h3>
<hr />
<p>The <b>Limit Command</b> is a command that allows for you to limit how many results you get immediately.  This is especially useful if you want to page your results with a set number of entries.  Here's the syntax for the command: <code class="params">&<var>limit</var>=<dfn>x</dfn>,<dfn>y</dfn></code>. Think of it like a search engine, it will give you results numbered <code class="params"><dfn>x</dfn></code> to <code class="params"><dfn>x</dfn></code>+<code class="params"><dfn>y</dfn></code>.</p><br />

SYNTAX: <br />
<code class="params">&<var>limit</var>=<dfn>x</dfn>,<dfn>y</dfn></code><br />
<br />
DEFINITION:
<li><code class="params"><var>limit</var></code>: Specifies the starting and ending range of the results you'd like to display.  Please note this order is specified by the order of the results that are returned.  The limit will return the results numbered from the range of <code class="params"><dfn>x</dfn></code> to <code class="params"><dfn>x</dfn></code>+<code class="params"><dfn>y</dfn></code>.  Again, this is based on the order specified by the result.  The default value of the <b>Limit Command</b> is <code class="params"><dfn>0</dfn>,<dfn>40</dfn></code>, and will be used unless a specific limit is specified.</li><br />
<br />
POTENTIAL PITFALLS:<br />
<p>The <b>Limit Command</b> is really setup to limit the number of results being displayed.  This isn't something that filters the information in the API call itself.  This function just returns a subset of the overall data returned by the API Calls.  If you do not specify a comma separated pair, then it will assume that the first value is 0, and that the second value is whatever you specify.</p><br />
<br />

<b>example 1:</b>
params: <code class="params"><var>dataname</var>=<dfn><?php echo $dataname; ?></dfn>&<var>key</var>=<dfn><?php echo $api_key; ?></dfn>&<var>token</var>=<dfn><?php echo $api_token; ?></dfn>&<var>table</var>=<dfn>orders</dfn>&<var>limit</var>=<dfn>0</dfn>,<dfn>5</dfn>&<var>valid_xml</var>=<dfn>1</dfn></code><br />
<p><b>results 1:</b></p>
<div class="code_examp" onclick="show('lfex1', this);">Show Results</div>
<div class="code_examp" id="lfex1" style="display: none;"><code class="xml">
	<?php
		$vars = generateVars($dataname, $api_key, $api_token, "orders") . "&limit=0,5";
		$response =  post_to_api($vars);
	 echo responseToXML($response) ?>
</code></div>
<!-- LIMIT FEATURE END -->

<!-- VALID XML BEGIN -->
<h3>Valid XML Flag</h3>
<hr />
<p>The <b>Valid XML Flag</b> is a flag that accepts 1 or 0 as it's value.  This informs the API Call to return the results wrapped in a &lt;result>&lt;/result> tag, along with a proper XML header. Syntax: <code class="params">&<var>valid_xml</var>=<dfn>1</dfn></code>.  When utilized, this flag ensures that the result is returned in a style that can be read and parsed using XML implementations.</p><br />

SYNTAX: <br />
<code class="params">&<var>valid_xml</var>=<dfn>1</dfn></code><br />
<br />
DEFINITION:
<li><code class="params"><var>valid_xml</var></code>: is just a flag that tells the API to append to the result the neccessary XML header, as well as wrap the result in result tags.  By default, this is turned off, so your result is something of the following nature:</li><br />
<div class="code_examp">
	<code class="xml">
&lt;<var>row</var>><br />
&nbsp;&nbsp;&lt;<var>field</var>><dfn>value</dfn>&lt;/<var>field</var>><br />
&lt;/<var>row</var>><br />
	</code>
</div>
<p>With <code class="params"><var>valid_xml</var></code> set to 1 the result becomes </p>
<div class="code_examp">
	<code class="xml">
&lt;?<var>xml</var> <strong>version</strong>="<dfn>1.0</dfn>" <strong>encoding</strong>="<dfn>utf-8</dfn>"?><br />
&lt;<var>result</var>><br />
&nbsp;&nbsp;&lt;<var>row</var>><br />
&nbsp;&nbsp;&nbsp;&nbsp;&lt;<var>field</var>><dfn>value</dfn>&lt;/<var>field</var>><br />
&nbsp;&nbsp;&lt;/<var>row</var>><br />
&lt;/<var>result</var>><br />
	</code>
</div>
<br />
POTENTIAL PITFALLS:<br />
<p>The default results are returned in a tag based representation, so it's easy to confuse them for actual XML.  Please note, that by default no XML scheme is specified for the results returned by the API.</p><br />
<br />
<!-- VALID XML END -->

<!-- FILTERS BEGIN -->
<h3>Filters</h3>
<hr />
<p>The <b>Filters</b> parameter is a parameter that allows for maximum customization on filtering your results.  The syntax: <code class="params">&<var>filters</var>=<dfn>JSON_STR</dfn></code> specifies that you must provide the value as a <b>JSON</b> represented object.  This allows for you specify multiple filters at once, and specify multiple fields to filter across.  It's very powerful as far as customizing the results of your API Call.</p><br />
SYNTAX: <br />
<code class="params">&<var>filters</var>=<dfn>JSON_STR</dfn></code><br />
<br />
DEFINITION:
<li><code class="params"><var>filters</var></code>: Specifies the filters you'd like to be applied to your API Call.  The value passed must be a valid JSON string.  Beyond that there are futher requirements on the JSON itself.</li>
<li><code class="params"><dfn>JSON_STR</dfn></code>: Here is the format that the filters are expected to be in (newlines, tabs, and multiple spaces are not important):</li>
<div class="code_examp">
	<code class="json">
[<br />
&nbsp;&nbsp;{<br />
&nbsp;&nbsp;&nbsp;&nbsp;"<var>field</var>" : "<dfn>fieldname</dfn>",<br />
&nbsp;&nbsp;&nbsp;&nbsp;"<var>operator</var>" : "<dfn>OP</dfn>",<br />
&nbsp;&nbsp;&nbsp;&nbsp;"<var>value1</var>" : "<dfn>value1</dfn>",<br />
&nbsp;&nbsp;&nbsp;&nbsp;"<var>value2</var>" : "<dfn>value2</dfn>"<br />
&nbsp;&nbsp;},<br />
&nbsp;&nbsp;. . .<br />
]<br />
<br />
DEFINITION:
<li><var>field</var>: (REQUIRED) This is the field name you would like the filter to be applied to.</li>
<li><var>value1</var>: (REQUIRED) This is the value you'd like to filter by.  It can be numerical or a string.  It doesn't matter.</li>
<li><var>value2</var>: (OPTIONAL) This value will only be used if you use an <var>operator</var> that requires another value.</li>
<li><var>operator</var>: (REQUIRED) This is the specific operator you would like to apply to the <var>field</var> using <var>value1</var>.  Depending on the operator, you may be required to specify a <var>value2</var>.  Here is the list of currently supported Filter Operators:<br />
	<ol>
		<li><dfn>&lt;=</dfn>: Less Than or Equal To</li>
		<li><dfn>>=</dfn>: Greater Than or Equal To</li>
		<li><dfn>&lt;</dfn>: Less Than</li>
		<li><dfn>></dfn>: Greater Than</li>
		<li><dfn>=</dfn>: Equal To</li>
		<li><dfn>&lt;></dfn>: Not Equal To</li>
		<li><dfn>BETWEEN</dfn>: Between (Requires <var>value2</var>)</li>
		<li><dfn>LIKE</dfn>: Uses String Matching to return things similar to <var>value1</var>, supports % wildcards.</li>
		<li><dfn>NOT LIKE</dfn>: Uses String Matching to return things different than <var>value1</var>, supports % wildcards.</li>
		<li><dfn>!=</dfn>: Alternate Not Equal To</li>
	</ol>
</li>
	</code>
	</div><br />
POTENTIAL PITFALLS:<br />
<p>There are a lot of potential dangers here.  If the JSON provided to the API Call isn't valid, we can not guarantee that the API Call results will come out.  If there is an error in the JSON that has been detected, instead of results, an error message will be returned, attempting to specify where the error is located.  Also be wary of use, the more filters you have, the less likely results will come up.  Everything by default is an AND operation between all filters, so in order for results to come true all filters must be met for the particular entries.</p><br />
<br />
EXAMPLE:<br />
<b>example 1:</b><br />
<?php $json = "{ \"field\" : \"category\", \"operator\" : \"=\", \"value1\" : \"2\" }"; ?>
params: <code class="params"><var>dataname</var>=<dfn><?php echo $dataname; ?></dfn>&<var>key</var>=<dfn><?php echo $api_key; ?></dfn>&<var>token</var>=<dfn><?php echo $api_token; ?></dfn>&<var>table</var>=<dfn>modifiers</dfn>&<var>filters</var>=<dfn><?php echo $json; ?></dfn>&<var>valid_xml</var>=<dfn>1</dfn></code><br />
<p><b>results 1:</b></p>
<div class="code_examp" onclick="show('fex1', this);">Show Results</div>
<div class="code_examp" id="fex1" style="display: none;"><code class="xml">
	<?php
		$vars = generateVars($dataname, $api_key, $api_token, "modifiers") . "&filters=" . $json;
		$response =  post_to_api($vars);
	 echo responseToXML($response, Array("category")) ?>
</code></div>

<b>example 2:</b><br />
<?php $json = "[{ \"field\" : \"category\", \"operator\" : \"=\", \"value1\" : \"2\" },{ \"field\" : \"id\", \"operator\" : \"BETWEEN\", \"value1\" : \"2\", \"value2\" : \"6\" }]"; ?>
<br />
params: <code class="params"><var>dataname</var>=<dfn><?php echo $dataname; ?></dfn>&<var>key</var>=<dfn><?php echo $api_key; ?></dfn>&<var>token</var>=<dfn><?php echo $api_token; ?></dfn>&<var>table</var>=<dfn>modifiers</dfn>&<var>filters</var>=<dfn><?php echo $json; ?></dfn>&<var>valid_xml</var>=<dfn>1</dfn></code><br />
<p><b>results 2:</b></p>
<div class="code_examp" onclick="show('fex2', this);">Show Results</div>
<div class="code_examp" id="fex2" style="display: none;"><code class="xml">
	<?php
		$vars = generateVars($dataname, $api_key, $api_token, "modifiers") . "&filters=" . $json;
		$response =  post_to_api($vars);
	 echo responseToXML($response, Array("category", "id")) ?>
</code></div>
<br />

<b>example 3:</b><br />
<br />
<?php $json = "[{ \"field\": \"order_id\", \"operator\": \"IN\",\"value1\": [\"1-12\",\"1-13\"]}]"; ?>
params: <code class="params"><var>dataname</var>=<dfn><?php echo $dataname; ?></dfn>&<var>key</var>=<dfn><?php echo $api_key; ?></dfn>&<var>token</var>=<dfn><?php echo $api_token; ?></dfn>&<var>table</var>=<dfn>order_contents</dfn>&<var>filters</var>=<dfn><?php echo $json; ?></dfn>&<var>valid_xml</var>=<dfn>1</dfn></code><br />
<p><b>results 3:</b></p>
<div class="code_examp" onclick="show('fex3', this);">Show Results</div>
<div class="code_examp" id="fex3" style="display: none;"><code class="xml">
	<?php
		$vars = generateVars($dataname, $api_key, $api_token, "order_contents") . "&filters=" . $json;
		$response =  post_to_api($vars);
		//echo $response;
	 echo responseToXML($response, Array("order_id")); ?>
</code></div>
<br />

<p>Utilizing the filtering features of the API, it is possible to specify exactly the type of information you'd like.  The <b>API Functions</b> specified here have been mainly centered around shaping the results of your API Call in a specific way.  There are more functions that allow for a different set of operations, but those will be covered in the next section.</p>
<p>Next Step <span class="section" onclick="swapPage('api_functions_a');">Advanced API Functions</span></p>

<!--FILTERS END -->
<br />
