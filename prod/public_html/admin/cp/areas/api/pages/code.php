<?php
require_once($_SERVER['DOCUMENT_ROOT']."/cp/areas/header.php");
?>	
<div>
		<h3>Example PHP to Query Lavu API</h3>
			<div class="code_examp">
				<pre class="php">
<em>function</em> <func>post_to_api</func>(<var>$postvars</var>)
{
	<var>$ch</var> = <func>curl_init</func>();
	<func>curl_setopt</func>(<var>$ch</var>, <var>CURLOPT_URL</var>, <dfn>"https://admin.poslavu.com/cp/reqserv/"</dfn>);
	<func>curl_setopt</func>(<var>$ch</var>, <var>CURLOPT_RETURNTRANSFER</var>, 1);
	<func>curl_setopt</func>(<var>$ch</var>, <var>CURLOPT_POST</var>, 1);
	<func>curl_setopt</func>(<var>$ch</var>, <var>CURLOPT_POSTFIELDS</var>, <var>$postvars</var>);
	<var>$response</var> = <func>curl_exec</func>(<var>$ch</var>);
	<func>curl_close</func>(<var>$ch</var>);

	<em>return</em> <var>$response</var>;
}
				</pre>
			</div>
	</div>

	<div>
		<h3>Example PHP to Convert Response to JSON</h3>
			<div class="code_examp">
				<pre class="php">
<em>function</em> <func>responseToJSON</func>(<var>$response</var>)
{
	<var>$dom</var> = <em>new</em> <strong>DOMDocument</strong>;
	<var>$dom</var>-><func>loadXML</func>(<var>$response</var>);

	<var>$doc</var> = <var>$dom</var>-><func>documentElement</func>;
	<var>$first</var> = 0;

	<var>$result</var> = <dfn>""</dfn>;
	<em>foreach</em>(<var>$doc</var>-><func>childNodes</func> as <var>$row</var>)
	{
		<em>if</em>(<var>$first</var> % 2 == 1)
		{
			<em>if</em>(<var>$result</var> != <dfn>""</dfn>)
				<var>$result</var> .= <dfn>",\n"</dfn>;
			<var>$inner</var> = <dfn>""</dfn>;
			<em>foreach</em>(<var>$row</var>-><func>childNodes</func> as <var>$entry</var>)
				<em>if</em>(<var>$entry</var>-><func>nodeName</func> != <dfn>"#text"</dfn>)
				{
					<em>if</em>(<var>$inner</var> != <dfn>""</dfn>)
						<var>$inner</var> .= <dfn>", "</dfn>;
						
					<var>$inner</var> .= <dfn>'"'</dfn> . <var>$entry</var>-><func>nodeName</func> . <dfn>'" : "'</dfn> . <var>$entry</var>-><func>nodeValue</func> . <dfn>'"'</dfn>;
				}

			<var>$result</var> .= <dfn>"\t{\n\t\t"</dfn> . <var>$inner</var> . <dfn>"\n\t}"</dfn>;
		}
		<var>$first</var>++;
	}

	<var>$result</var> = <dfn>"[\n"</dfn> . <var>$result</var> . <dfn>"\n]\n"</dfn>;

	<em>return</em> <var>$result</var>;
}
				</pre>
			</div>
	</div>
	
	<div>
		<h3>Example using C# to Pull Data</h3>
			<div class="code_examp">
				<pre class="csharp">
<em>using System;</em>
<em>using</em> System.Collections.Generic;
<em>using</em> System.ComponentModel;
<em>using</em> System.Data;
<em>using</em> System.Drawing;
<em>using</em> System.Linq;
<em>using</em> System.Text;
<em>using</em> System.Windows.Forms;
<em>using</em> System.Web;
<em>using</em> System.IO;
<em>using</em> System.Net;

<em>namespace</em> <var>api_example</var>
{
    <em>public</em> <em>partial</em> <em>class</em> <strong>Form1</strong> : <strong>Form</strong>
    {
        <em>public</em> <func>Form1</func>()
        {
            <func>InitializeComponent</func>();
        }

        <em>private</em> <em>void</em> <func>Form1_Load</func>(<strong>object</strong> <var>sender</var>, <strong>EventArgs</strong> <var>e</var>)
        {
                <strong>string</strong> <var>api_url</var> = <dfn>"https://admin.poslavu.com/cp/reqserv/"</dfn>;
                <strong>string</strong> <var>api_dataname</var> = <dfn>"<?php echo $dataname ?>"</dfn>;
                <strong>string</strong> <var>api_key</var> = <dfn>"<?php echo $api_key; ?>"</dfn>;
                <strong>string</strong> <var>api_token</var> = <dfn>"<?php echo $api_token; ?>"</dfn>;

                <strong>string</strong> <var>postvars</var> = <dfn>"dataname="</dfn> + <var>api_dataname</var> + <dfn>"&key="</dfn> + <var>api_key</var> + <dfn>"&token="</dfn> + <var>api_token</var>;
                <var>postvars</var> += <dfn>"&table=tables"</dfn>;

            <strong>string</strong> <var>return_val</var> = <func>PostData</func>(<var>api_url</var>, <var>postvars</var>);
            <strong>MessageBox</strong>.<func>Show</func>(<dfn>"api response: \n"</dfn> + <var>return_val</var>);
        }

        <em>private</em> <strong>string</strong> <func>PostData</func>(<strong>string</strong> <var>url</var>, <strong>string</strong> <var>postData</var>)
        {
            <strong>HttpWebRequest</strong> <var>request</var> = <em>null</em>;
            <strong>Uri</strong> <var>uri</var> = <em>new</em> <strong>Uri</strong>(<var>url</var>);
            <var>request</var> = (<strong>HttpWebRequest</strong>)<strong>WebRequest</strong>.<func>Create</func>(<var>uri</var>);
            <var>request</var>.<func>Method</func> = <dfn>"POST"</dfn>;
            <var>request</var>.<var>ContentType</var> = <dfn>"application/x-www-form-urlencoded"</dfn>;
            <var>request</var>.<var>ContentLength</var> = <var>postData</var>.<var>Length</var>;
            <em>using</em> (<strong>Stream</strong> <var>writeStream</var> = <var>request</var>.<func>GetRequestStream</func>())
            {
                <strong>UTF8Encoding</strong> <var>encoding</var> = <em>new</em> <strong>UTF8Encoding</strong>();
                <em>byte</em>[] <var>bytes</var> = <var>encoding</var>.<func>GetBytes</func>(<var>postData</var>);
                <var>writeStream</var>.<func>Write</func>(<var>bytes</var>, <dfn>0</dfn>, <var>bytes</var>.<var>Length</var>);
            }
            <em>try</em>
            {
                <strong>string</strong> <var>result</var> = <strong>string</strong>.<var>Empty</var>;
                <em>using</em> (<strong>HttpWebResponse</strong> <var>response</var> = (<strong>HttpWebResponse</strong>)<var>request</var>.<func>GetResponse</func>())
                {
                    <em>using</em> (<strong>Stream</strong> <var>responseStream</var> = <var>response</var>.<func>GetResponseStream</func>())
                    {
                        <em>using</em> (<strong>StreamReader</strong> <var>readStream</var> = <em>new</em> <func>StreamReader</func>(<var>responseStream</var>, <strong>Encoding</strong>.<var>UTF8</var>))
                        {
                            <var>result</var> = <var>readStream</var>.<func>ReadToEnd</func>();
                        }
                    }
                }
                <em>return</em> <var>result</var>;
            }
            <em>catch</em>
            {
                <em>return</em> <dfn>""</dfn>;
            }
        }
    }
}
				</pre>
			</div>
			<br />
	</div>
