<?php
include_once("functions.php");
?>
<p>Now that we know what is necessary in order to make a basic query, let's take a look at some of the results each POST would return.  In order to get started, we need to know what kinds of information the API allows access to.  Here is the list of every table the API currently allows access to:</p><br />
<br />
<?php
	global $tables;
	foreach($tables as $t)
		echo "<li><b><a href=\"#query_results_{$t}\">$t</a></b></li>";
?>
<br />
<p>What follows is the results of sending each of these tables to the API using your <b>dataname</b>, <b>token</b>, and <b>key</b> values.  This is data that is actually present in your database.  If no information shows up, this means there is no data within that table.  In order to get data, be sure to use our app!</p>

<?php
getInformation(); ?>
<br />
<p>This should provide a good idea of what's contained within each table, and the amount of information that is available to you through the API.</p>
<br />
<p>Next Step: <span class="section" onclick="swapPage('api_functions');">API Functions</span>.</p><br />
