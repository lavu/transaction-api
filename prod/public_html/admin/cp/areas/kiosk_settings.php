<?php

if (!$in_lavu)
{
	exit();
}
$dn_loc = $data_name.":".$locationid;
$currGW = $location_info['gateway'];

$available_gateways = array();
$available_gateways[''] = "---".speak("Select a gateway")."---";

// All Gateways besides PayPal will be removed from the selection per Nick Fuqua (KIOSK-201)
$available_gateways['PayPal'] = "PayPal";

$tablename = "kiosk_settings";
$rowid = $locationid;
$include_session = "?session_id=".session_id();
$include_data_name = "&dn=".sessvar("admin_dataname");
$phone_carriers = array('att' => "AT&T", 'sprint' => "Sprint", 'tmobile' => "T-Mobile", 'verizon' => "Verizon");
$config_query = lavu_query("select * from `kiosk_settings` where _deleted = 0");
if ($config_query === false){
	error_log(lavu_dberror($config_query));
}
while($config_read = mysqli_fetch_assoc($config_query))
{
	$location_info[$config_read['setting']] = $config_read['value'];
}

if (!empty($_POST['posted'])) {
	// Get the Kiosk device limit
	$payment_status = array('custom_max_kiosk' => 0);
	$payment_statuses = mlavu_query("SELECT `custom_max_kiosk` FROM `poslavu_MAIN_db`.`payment_status` WHERE `dataname` = '[1]'", $data_name);
	if ($payment_statuses === false) {
		error_log(mlavu_dberror());
	}
	else {
		$payment_status = mysqli_fetch_assoc($payment_statuses);
	}
	$device_count_max = (int) $payment_status['custom_max_kiosk'];

	$activate_ids = array(0);
	$keys = array_keys($_POST);
	foreach ($keys as $key) {
		if (substr($key, 0, 7) == "device_") {
			$device_id_parts = explode("_", $key);
			$device_id = $device_id_parts[1];
			$activate_ids[] = $device_id;

			$device_usage_type = $_POST['usage_type_'.$device_id];

			if ($device_usage_type == "kiosk") {
				$device_count++;
			}
		}
	}

	if ($device_count > $device_count_max) {
		echo "<script language='javascript'>alert(\"You've tried to activate too many Kiosk devices. The limit is ".$device_count_max.".\");</script>";
	} else {
		$update_active = lavu_query("UPDATE `devices` SET `slot_claimed` = IF(`id` IN (".implode(",", $activate_ids)."), '[1]', ''),  `inactive` = IF(`id` IN (".implode(",", $activate_ids)."), 0, 1) WHERE `loc_id` = '[2]' AND `inactive` = '0'", date("Y-m-d"), $locationid);
		schedule_remote_to_local_sync_if_lls($location_info, $locationid, "table updated", "devices");
	}
}

$fields   = array();
$fields[] = array(speak("Kiosk General Settings"),"Kiosk Settings","seperator");
$fields[] = array(speak("Theme color picker").":","kiosk_config:kiosk_theme","text","size:10","value:#");
$fields[] = array(speak("Allow tips").":","kiosk_config:kiosk_allow_tips","select",array('1' => speak("Yes I want to allow tips"),'0' => speak("No I do not want to allow tips")));
$fields[] = array(speak("Dining Option").":","kiosk_config:kiosk_dine_option","bool_multiple");
// KIOSK-380: Remove cellphone and email notification settings from Kiosk Settings Page per Nick Fuqua
// $fields[] = array(speak("Cell phone carrier for order alert text messages").":","kiosk_config:kiosk_phone_carrier","select",$phone_carriers);
// $fields[] = array(speak("Phone number for order alert messages (format: 5051234567)").":","kiosk_config:kiosk_phone","text","size:20");
// $fields[] = array(speak("Email for order alert notifications").":","kiosk_config:kiosk_email","text","size:20");
$fields[] = array(speak("Minimum Kiosk Order Total").":","kiosk_config:kiosk_minimum_order_total","text","size:4");
$fields[] = array(speak("Enable Lavu Kiosk Splash Page").":","kiosk_config:kiosk_enable_splash_page","bool");
$fields[] = array(speak("Choose Image Option").":","kiosk_config:kiosk_image_options","select",array('1' => speak("Menu Images"),'0' => speak("Splash Image")));
$fields[] = array(speak("Upload Image").":","kiosk_config:kiosk_splash_filename","file");
$fields[] = array(speak("Splash Change Interval Seconds").":","kiosk_config:kiosk_splash_change_interval_seconds","number","size:4");
$fields[] = array(speak("Splash Timeout Seconds").":","kiosk_config:kiosk_splash_timeout_seconds","number","size:4");

$fields[] = array(speak("Kiosk Credit Card Integration Settings"),"Credit Card Integration Settings","seperator");

$fields[] = array(speak("Payment Gateway").":","kiosk_config:kiosk_gateway","select",$available_gateways);

$fields[] = array(speak("Integration data 1").":","kiosk_config:kiosk_integration1","enc_text","key:".integration_keystr());
$fields[] = array(speak("Integration data 2").":","kiosk_config:kiosk_integration2","enc_text","key:".integration_keystr());
$fields[] = array(speak("Integration data 3").":","kiosk_config:kiosk_integration3","enc_text","key:".integration_keystr());
$fields[] = array(speak("Integration data 4").":","kiosk_config:kiosk_integration4","enc_text","key:".integration_keystr());
$fields[] = array(speak("Integration data 5").":","kiosk_config:kiosk_integration5","enc_text","key:".integration_keystr());
$fields[] = array(speak("Integration data 6").":","kiosk_config:kiosk_integration6","enc_text","key:".integration_keystr());
$fields[] = array(speak("Integration data 7").":","kiosk_config:kiosk_integration7","enc_text","key:".integration_keystr());
$fields[] = array(speak("Integration data 8").":","kiosk_config:kiosk_integration8","enc_text","key:".integration_keystr());

if (is_lavu_admin())
{
	$fields[] = array($lavu_only.speak("Integration data 9").":","kiosk_config:kiosk_integration9","enc_text","key:".integration_keystr());
	$fields[] = array($lavu_only.speak("Integration data 10").":","kiosk_config:kiosk_integration10","enc_text","key:".integration_keystr());
}

$fields[] = array(
	speak("Default transaction type").":",
	"kiosk_config:kiosk_cc_transtype",
	"select",
	array(
		''		=> "",
		'Auth'	=> "Auth",
		'Sale'	=> "Sale"
	)
);

$fields[] = array(speak("Save"),"submit","submit");
$form_fixed_width = 700;
require_once(resource_path() . "/form.php");

// Kiosk Receipt Printers
echo "<div style='width:697px; padding:0px 0px 5px 0px; border:2px solid #aaaaaa;'><div class= 'divider' style='display:inline-block; width:100%; background-color:#98b624; margin:2px 0px 2px 0px; padding:10px 0px 10px 0px; color:white; font-size:12px; font-weight:bold;'> Kiosk Receipt Printers </div><div style='width:650px; height:500px; overflow:hidden;' ><iframe style='border:0px; position:relative; width:645px; height:500px' src='print_wrap.php?pw_mode=kiosk_printers&dn_loc=$dn_loc' scrolling='no' onload='iframeLoaded(this);'></iframe></div></div>";

// Kiosk Devices
echo "<div style='width:697px; padding:0px 0px 5px 0px; border:2px solid #aaaaaa;'><div class= 'divider' style='display:inline-block; width:100%; background-color:#98b624; margin:2px 0px 2px 0px; padding:10px 0px 10px 0px; color:white; font-size:12px; font-weight:bold;'> Kiosk Devices </div>";

echo "<form method='post'>
		<input type='hidden' name='posted' value='1'>
		<input type='hidden' name='posted_dataname' value='{$data_name}'>
		<table cellspacing=1 cellpadding=2>
		<tbody>
			<tr height='25px' bgcolor='#EEEEEE'>";
if (is_lavu_admin()) {
	echo "		<td></td>";
}
	echo "		<td align='center' width='100px'>".speak("Name")."</td>
				<td align='center' width='100px'>".speak("Usage Type")."</td>
				<td align='center' width='100px'>".speak("IP Address")."</td>
				<td align='center' width='100px'>".speak("Device ID")."</td>
				<td align='center' width='100px'>".speak("Last Check In")."</td>
				<td align='center' width='100px'>".$lavu_only.speak("Active")."</td>
				";
// if (is_lavu_admin()) {
// 	echo "		<td align='center' width='100px'>".$lavu_only.speak("Active")."</td>";
// }


$vars = array();
$vars['loc_id'] = $locationid;
$lavu_admin_filter = is_lavu_admin() ? " AND `lavu_admin` != '1'" : '';

$lavu_only = "<font color='#588604' style='font-size:14px'><b>*</b></font> ";
$lavu_admin = "<font color='#0000CC'><b>*</b></font>";

$get_kiosk_devices = lavu_query("SELECT `id`, `name`, `specific_model`, `usage_type`, `system_version`, `ip_address`, `UUID`, `UDID`, `slot_claimed`, `lavu_admin`, `app_name`, `poslavu_version`, `device_time`, `reseller_admin`, `last_checkin` FROM `devices` WHERE `loc_id` = '[loc_id]' AND `usage_type` = 'kiosk' AND `inactive` = '0' {$lavu_admin_filter} ORDER BY `model`, `name`", $vars);

if (mysqli_num_rows($get_kiosk_devices) === 0) {
	echo "<br><br>No active devices found...";
}

while ($info = mysqli_fetch_assoc($get_kiosk_devices)) {

	$checked = ($info['slot_claimed'] == date('Y-m-d')) ? " checked" : "";
	$checkbox = "<input name='device_{$info['id']}' type='checkbox' value='{$info['model']}' {$checked} onclick='showDevicesSaveButton()'>";
	$usage_type = "<input type='hidden' name='usage_type_{$info['id']}' value='{$info['usage_type']}'>";

	$device_id = "";
	if (!empty($info['UUID']) && $info['UUID'] != "-") {
		$uuid = explode("-", $info['UUID']);
		$device_id = $uuid[0];
		if (count($uuid) >= 2) $device_id .= "-".$uuid[1];
	}
	if (empty($device_id)) {
		$device_id = $info['UDID'];
	}

	$admin_indicator = ($info['lavu_admin'] == "1") ? "<font color='#0000CC'><b>*</b></font>" : '';

	echo "<tr>";
	if (is_lavu_admin()) {
		echo "<td>".$admin_indicator."</td>";
	}
	echo "
		<td align='left' style='font-size:11px;'>".$info['name']."</td>
		<td align='center' style='font-size:10px;'>".$info['usage_type']."</td>
		<td align='center' style='font-size:10px;'>".$info['ip_address'] ."</td>
		<td align='center' style='font-size:10px;'>".$device_id."</td>
		<td align='center' style='font-size:10px;'>".$info['last_checkin']."</td>";
	if (is_lavu_admin()) {
		echo "<td align='center'>{$checkbox}{$usage_type}</td></tr>";
	}
}

echo "</tbody></table><input type='submit' id='devices_save_btn' value='".speak("Save")."' style='width:120px; display:none'></form></div>";


if (is_lavu_admin()) {
	echo "<br>".$lavu_only."Indicates items visible only to Lavu Admin users (i.e., developers and support staff)<br>";
	echo "<br>".$lavu_admin." Indicates Lavu Admin devices<br>";
}


?>
<link href="/cp/css/select2-4.0.3.min.css" rel="stylesheet" />
<script src="/cp/scripts/select2-4.0.3.min.js"></script>
<script src="/cp/scripts/jscolor.js"></script>
<script type="text/javascript">	
function showDevicesSaveButton() {
	document.getElementById('devices_save_btn').style.display = 'block';
}
$(document).ready(function() {
		 $('input[name^=\'kiosk_config:kiosk_theme\']').addClass("jscolor");
		 var input = $('input[name^=\'kiosk_config:kiosk_theme\']');
		 input.val("#"+input.val() )

});

</script>
