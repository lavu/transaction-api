var customFieldsDataStore = {};


function loadCustomFields(translationMapJSONStr, customFieldInformationJSONStr)
{
	customFieldsDataStore.customerFieldInformation = customFieldInformationJSONStr;
	customFieldsDataStore.translationMap = translationMapJSONStr;

	var fieldTable = document.getElementById('field_table');
	if (!fieldTable || fieldTable.rows.length == 0) {
		return;
	}
	
	customFieldsDataStore.rowStart = fieldTable.rows.length;

	//There will now be a <td> for delete for each of the following custom rows rows, we add the empty td to all
	//rows above the custom rows.
	for (var i = 0; i < fieldTable.rows.length; i++) {
		var currRow = fieldTable.rows[i];
		currRow.appendChild(document.createElement('td'));
	}
	renderCustomRowsAndAddButton();

	var saveButton = document.getElementById('save_submit_button');
	saveButton.onclick = saveCustomerInfoFormClicked;
}

/*
	For keeping things sane, it may actually be best to remove all custom fields from the table,
	and then re-add them from the data store.  This way we know that what is on the screen is
	exactly what is in the data structure on form submission, this includes new rows as well
	as deletions.
*/
function renderCustomRowsAndAddButton(){
	
	var fieldTable = document.getElementById('field_table');
	var cellCount = fieldTable.rows[0].cells.length;

	// 1.) remove all custom fields and custom field add button
	var currRow = fieldTable.rows[customFieldsDataStore.rowStart];
	var iter = 0;
	while(true){	
		if(!currRow){
			break;
		}
		fieldTable.removeChild(currRow);
		currRow = fieldTable.rows[customFieldsDataStore.rowStart];
		iter++;
	}

	// 2.) Re-add all custom field rows
	var usedCustomFields = customFieldsDataStore.customerFieldInformation['used_custom_fields'];
	//Add all currently used custom fields to the table.
	for(var i = 0; i < usedCustomFields.length; i++){



		currentUsedCustomField = usedCustomFields[i];
		var customFieldRow = document.createElement('tr');


		if(customFieldsDataStore.customerFieldInformation['used_custom_fields'][i]['customer_removed']){
			var deletedTD = document.createElement('td');
			deletedTD.setAttribute('colspan', cellCount);
			var deletedDisplayDiv = document.createElement('div');
			deletedDisplayDiv.style.textAlign = 'left';
			deletedDisplayDiv.style.color = 'red';
			deletedDisplayDiv.innerHTML = 'Deleted';
			deletedTD.appendChild(deletedDisplayDiv);
			customFieldRow.appendChild(deletedTD);
			fieldTable.appendChild(customFieldRow);
			continue;
		}

		//1.) Label Input: MAPPED TO `value`
		var labelInput = document.createElement('input');
		labelInput.placeholder = customFieldsDataStore.translationMap['label_place_holder'];
		labelInput.value = currentUsedCustomField['value'].replace(new RegExp('_', 'g'), ' ').trim();
		labelInputTD = document.createElement('td');
		labelInputTD.appendChild(labelInput);
		customFieldRow.appendChild(labelInputTD);

		//2.) Include Select: MAPPED TO `_deleted`
		var includeSelect = document.createElement('select');
		var includeOption = document.createElement('option');
		includeOption.innerHTML = customFieldsDataStore.translationMap['Include'];
		includeOption.value = '0';
		var excludeOption = document.createElement('option');
		excludeOption.innerHTML = customFieldsDataStore.translationMap['--Hide--'];
		excludeOption.value = '1';
		includeSelect.appendChild(includeOption);
		includeSelect.appendChild(excludeOption);
		includeSelectTD = document.createElement('td');
		includeSelectTD.appendChild(includeSelect);
		customFieldRow.appendChild(includeSelectTD);
		setSelectIndexBySupposedValue(includeSelect,  currentUsedCustomField['_deleted']);

		//3.) Required Select
		var requiredSelect = document.createElement('select');
		var optionalOption = document.createElement('option');
		optionalOption.innerHTML = customFieldsDataStore.translationMap['optional_option_title'];
		optionalOption.value = '';
		var requiredOption = document.createElement('option');
		requiredOption.innerHTML = customFieldsDataStore.translationMap['required_option_title'];
		requiredOption.value = 'requiredField';
		requiredSelect.appendChild(optionalOption);
		requiredSelect.appendChild(requiredOption);
		requiredSelectTD = document.createElement('td');
		requiredSelectTD.appendChild(requiredSelect);
		customFieldRow.appendChild(requiredSelectTD);
		setSelectIndexBySupposedValue(requiredSelect,  currentUsedCustomField['value6']);

		//4.) Order Select
		var orderSelect = document.createElement('select');
		for(var k = 3; k < 100; k++){
			var currOrderOption = document.createElement('option');
			currOrderOption.innerHTML = k;
			currOrderOption.value = k;
			orderSelect.appendChild(currOrderOption);
		}
		var orderSelectTD = document.createElement('td');
		orderSelectTD.appendChild(orderSelect);
		customFieldRow.appendChild(orderSelectTD);
		setSelectIndexBySupposedValue(orderSelect,  currentUsedCustomField['value10']);

		//5.) Print Select //Options: No, Kitchen, Receipt, Both
		var printSelect = document.createElement('select');
		var noOption = document.createElement('option');
		noOption.value = '';
		noOption.innerHTML = customFieldsDataStore.translationMap['print_option_no'];
		var kitchenOption = document.createElement('option');
		kitchenOption.value = 'k';
		kitchenOption.innerHTML = customFieldsDataStore.translationMap['print_option_kitchen'];
		var receiptOption = document.createElement('option');
		receiptOption.value = 'r';
		receiptOption.innerHTML = customFieldsDataStore.translationMap['print_option_reciept'];
		var bothOption = document.createElement('option');
		bothOption.value = 'b';
		bothOption.innerHTML = customFieldsDataStore.translationMap['print_option_both'];
		printSelect.appendChild(noOption);
		printSelect.appendChild(kitchenOption);
		printSelect.appendChild(receiptOption);
		printSelect.appendChild(bothOption);
		var printSelectTD = document.createElement('td');
		printSelectTD.appendChild(printSelect);
		customFieldRow.appendChild(printSelectTD);
		setSelectIndexBySupposedValue(printSelect,  currentUsedCustomField['value12']);

		//6.) Report Select
		var reportSelect = document.createElement('select');
		var reportNoOption = document.createElement('option');
		reportNoOption.value = '';
		reportNoOption.innerHTML = customFieldsDataStore.translationMap['report_option_no'];
		var reportYesOption = document.createElement('option');
		reportYesOption.value = '150';
		reportYesOption.innerHTML = customFieldsDataStore.translationMap['report_option_yes'];
		reportSelect.appendChild(reportNoOption);
		reportSelect.appendChild(reportYesOption);
		var reportSelectTD = document.createElement('td');
		reportSelectTD.appendChild(reportSelect);
		customFieldRow.appendChild(reportSelectTD);
		setSelectIndexBySupposedValue(reportSelect,  currentUsedCustomField['value11']);

		//7.) Delete Button
		var deleteButtonDiv = document.createElement('div');
		deleteButtonDiv.style.cursor = 'pointer';
		deleteButtonDiv.innerHTML = 'X';
		deleteButtonTD = document.createElement('td');
		deleteButtonTD.appendChild(deleteButtonDiv);
		customFieldRow.appendChild(deleteButtonTD);
		deleteButtonDiv.addEventListener('click', deleteCustomFieldJSButtonClicked);
		deleteButtonDiv.tag = i;
		customFieldRow.id = 'table_row_'+currentUsedCustomField['value2']+'_id';
		fieldTable.appendChild(customFieldRow);
	}

	// 3.) Add the 'Add Custom Field' button.
	//Add the add custom field button
	var addCustomFieldButtonTitle = customFieldsDataStore.translationMap['add_custom_row_title'];
	var addCustomFieldButton = document.createElement('input');
	addCustomFieldButton.setAttribute('type','submit');
	addCustomFieldButton.addEventListener('click', addCustomRowClicked);
	addCustomFieldButton.value = addCustomFieldButtonTitle;
	addCustomFieldButton.style.fontSize = "12px";
	addCustomFieldButton.style.padding = '4px';
	addCustomFieldButton.style.width = '180px';
	var addCustomFieldRow = document.createElement('tr');
	addCustomFieldTD = document.createElement('td');
	addCustomFieldTD.setAttribute('colspan', cellCount);
	addCustomFieldTD.appendChild(addCustomFieldButton);
	addCustomFieldTD.style.textAlign = 'center';
	addCustomFieldRow.appendChild(addCustomFieldTD);
	fieldTable.appendChild(addCustomFieldRow);
	
	if(customFieldsDataStore.customerFieldInformation['used_custom_fields'].length >= 5){
			addCustomFieldButton.disabled = true;
			addCustomFieldButton.style.background = 'gray';
	}
}

function addCustomRowClicked(e){
	e.preventDefault();
	saveCustomRowsDomDataToCustomFieldInformation();
	//customFieldsDataStore.customerFieldInformation['used_custom_fields']

	//return array('static_fields' => $staticFields, 'used_custom_fields' => $usedCustomFields, 'available_custom_columns' => $availableFields);

	// Pull the available column name and create the row.
	var availableColumn = customFieldsDataStore.customerFieldInformation['available_custom_columns'][0];
	customFieldsDataStore.customerFieldInformation['available_custom_columns'].shift();

	var fieldTable = document.getElementById('field_table');
	var newColumnInfo = {
		'type':'input', 
		'value3':'CustomField',
		'value2':availableColumn, //Column of med customers it maps to.
		'_deleted':'0', //whether to display or hide field
		'value':'', //Label name
		'setting':'GetCustomerInfo',
		'value11':'150', //Report value (either '150' or '')
		'value6':'', //required or optional ('requiredField' or '')
		'value12':'', //Print 'k' Kitchen, 'r' Receipt, 'b' Both, '' NO
		'value10':fieldTable.rows.length - 1 //Display order
	};
	
	customFieldsDataStore.customerFieldInformation['used_custom_fields'].push(newColumnInfo);
	renderCustomRowsAndAddButton();

	return false
}

function deleteCustomFieldJSButtonClicked(e){
	customFieldsDataStore.customerFieldInformation['used_custom_fields'][e.target.tag]['customer_removed'] = true;
	renderCustomRowsAndAddButton();
}

function saveCustomRowsDomDataToCustomFieldInformation(){

	//There will now be a <td> for delete for each of the following custom rows rows, we add the empty td to all
	//rows above the custom rows.

	for(var i = 0; i < customFieldsDataStore.customerFieldInformation['used_custom_fields'].length; i++){
		currentUsedCustomField = customFieldsDataStore.customerFieldInformation['used_custom_fields'][i];
		var currDomRowId = 'table_row_' + currentUsedCustomField['value2'] + '_id';
		var currFieldRow = document.getElementById(currDomRowId);
		if(!currFieldRow){
			console.log("Field Row by id:["+currDomRowId+"] Not found.")
			continue;
		}
		var rowsTDs = currFieldRow.childNodes;
		
		// 1.) field name
		customFieldsDataStore.customerFieldInformation['used_custom_fields'][i]['value'] = rowsTDs[0].childNodes[0].value;
		// 2.) Include / --Hide-- 
		var includeSelect = rowsTDs[1].childNodes[0];
		var includeSelectOptionValue = includeSelect.options[includeSelect.selectedIndex].value;
		customFieldsDataStore.customerFieldInformation['used_custom_fields'][i]['_deleted'] = includeSelectOptionValue;
		// 3.) Optional / Required
		var optionalRequiredSelect = rowsTDs[2].childNodes[0];
		var optionalRequiredOptionValue = optionalRequiredSelect.options[optionalRequiredSelect.selectedIndex].value;
		customFieldsDataStore.customerFieldInformation['used_custom_fields'][i]['value6'] = optionalRequiredOptionValue;
		// 4.) Display order
		var displayOrderSelect = rowsTDs[3].childNodes[0];
		var displayOrderSelectValue = displayOrderSelect.options[displayOrderSelect.selectedIndex].value;
		customFieldsDataStore.customerFieldInformation['used_custom_fields'][i]['value10'] = displayOrderSelectValue;
		// 5.) Print
		var printSelect = rowsTDs[4].childNodes[0];
		var printSelectValue = printSelect.options[printSelect.selectedIndex].value;
		customFieldsDataStore.customerFieldInformation['used_custom_fields'][i]['value12'] = printSelectValue;
		// 6.) Report
		var reportSelect = rowsTDs[5].childNodes[0];
		reportSelectValue = reportSelect.options[reportSelect.selectedIndex].value;
		customFieldsDataStore.customerFieldInformation['used_custom_fields'][i]['value11'] = reportSelectValue;
		// 7.) Delete (Handled differently)

	}
}

function setSelectIndexBySupposedValue(selectElem, value){
	var toSetIndex = 0;
	for(var i = 0; i < selectElem.options.length; i++){
		var currOption = selectElem.options[i];
		if(value == currOption.value){
			toSetIndex = i;
			break;
		}
	}
	selectElem.selectedIndex = toSetIndex;
}


function saveCustomerInfoFormClicked(e){
	e.preventDefault();
	saveCustomRowsDomDataToCustomFieldInformation();
	var save_form = document.getElementById('save_form');

	var hiddenJSONField = document.createElement('input');
	hiddenJSONField.setAttribute('type', 'hidden');
	hiddenJSONField.setAttribute('name', 'custom_customer_field_information');
	hiddenJSONField.value = JSON.stringify(customFieldsDataStore.customerFieldInformation);
	save_form.appendChild(hiddenJSONField);
	save_form.submit();
}
