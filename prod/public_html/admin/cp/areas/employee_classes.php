<?php
define("TIPOUT_TYPE", "evenly");
	if($in_lavu)
	{
		echo "<br><br>";
		$form_post = $_POST;
		//print_r($form_post);
		function getConfigSettingByName($locationid,$setting){
			$check_for_update = lavu_query("SELECT `id`,`value` FROM `config` WHERE `setting` = '[2]' AND `location` = '[1]'", $locationid,$setting);
			$info = array();
			if (@mysqli_num_rows($check_for_update)) {
				$info = mysqli_fetch_assoc($check_for_update);
			}
			return $info;
		}
		
		$today_timestamp = date("Y-m-d");
		$empInfo_query = lavu_query("select * from `emp_classes_temp` where `_deleted` = 0 AND `class_updated_date` ='".$today_timestamp."' and `emp_class_id` in (select `id` from `emp_classes` where `_deleted` = 0)");
		
		while ($empData = mysqli_fetch_assoc($empInfo_query)) {
			$update_emp_class = "UPDATE `emp_classes` SET `title` = '".$empData['title']."' ,`type` = '".$empData['type']."', `_deleted` = '".$empData['_deleted']."',`tipout_rules` = '".$empData['tipout_rules']."' ,`tiprefund_rules` = '".$empData['tiprefund_rules']."', `refund_type` = '".$empData['refund_type']."', `enable_tip_sharing` = '".$empData['enable_tip_sharing']."',`tip_sharing_rule` = '".$empData['tip_sharing_rule']."' ,`include_server_owes` = '".$empData['include_server_owes']."', `tip_sharing_period` = '".$empData['tip_sharing_period']."', `tipout_break` = '".$empData['tipout_break']."' where `id` = '".$empData['emp_class_id']."'";
		    lavu_query($update_emp_class);
		    
		    $update_emp_temp_class = "UPDATE `emp_classes_temp` SET `_deleted` = 1 where `emp_class_id` = '".$empData['emp_class_id']."' AND `class_updated_date`  = '".$today_timestamp."'";
		    lavu_query($update_emp_temp_class);
		}
		
		if(isset($_POST) && isset($_POST['server_summary_tipout_overlay'])){
			$config_info_tipout_overlay = getConfigSettingByName($locationid,'server_summary_tipout_overlay');
			if(count($config_info_tipout_overlay)>0){
				lavu_query("UPDATE `config` SET `value` = '".$form_post['server_summary_tipout_overlay']."' WHERE `id` = '[1]'", $config_info_tipout_overlay['id']);
		   	} 
		   	else { 
		   		lavu_query("INSERT INTO `config` (`location`, `type`, `setting`, `value`) VALUES ('[1]', 'setting', 'server_summary_tipout_overlay', '[2]')", $locationid,$form_post['server_summary_tipout_overlay']);
		   	}
		   	$config_interval_for_tipout = getConfigSettingByName($locationid,'server_tips_time_interval_for_tipout');
		   	if(count($config_interval_for_tipout)>0){
		   		lavu_query("UPDATE `config` SET `value` = '".$form_post['server_tips_time_interval_for_tipout']."' WHERE `id` = '[1]'", $config_interval_for_tipout['id']);
		   	}
		   	else {
		   		lavu_query("INSERT INTO `config` (`location`, `type`, `setting`, `value`) VALUES ('[1]', 'setting', 'server_tips_time_interval_for_tipout', '[2]')", $locationid,$form_post['server_tips_time_interval_for_tipout']);
		   	}
		   	unset($_POST);
		}
		$tablename = "emp_classes";
		$forward_to = "index.php?mode={$section}_{$mode}";
		
		$fields = array();
		$fields[] = array(speak("Title"),"title","text","","list:yes");
		$fields[] = array(speak("Type"),"type","select",array(""=>"","FOH"=>"FOH","BOH"=>"BOH"),"list:yes");
		$fields[] = array(speak("Tipout Rules"),"tipout_rules","tipout_rules",array("emp_classes","id","title",""),"list:yes");
		$fields[] = array(speak("Tip Refund Rules"),"tiprefund_rules","tiprefund_rules",array("emp_classes","id","title",""),"list:yes");
		$fields[] = array(speak("Calculate based on"),"refund_type","refund_type",array("emp_classes","id","title",""),"list:yes");
	    $fields[] = array(speak("Enable Tip Sharing"),"enable_tip_sharing","bool","","","enableTipSharing");
	    $fields[] = array(speak("Tip Sharing Rule"),"tip_sharing_rule","tip_sharing_rule",array("emp_classes","id","title",""),"list:yes","tipSharingRules");
	    $fields[] = array(speak("Include Tip In amount for Tip Sharing in Server Owes House / House Owes Server calculation"),"include_server_owes","bool","","","includeServerOwes");
	    $fields[] = array(speak("Time period for Tip Sharing"),"tip_sharing_period","tip_sharing_period",array("emp_classes","id","title",""),"list:yes","timePeriod");
		$fields[] = array(speak("Save"),"emp_classes_submit","emp_classes_submit");

		require_once(resource_path() . "/browse.php");
		$overLay = getConfigSettingByName($locationid,'server_summary_tipout_overlay');
		$checked = '';
		if(count($overLay)>0 && $overLay['value']=='1'){
			$checked = 'checked';
			$overLayValue=1;
		}
		$time_interval = getConfigSettingByName($locationid,'server_tips_time_interval_for_tipout');
		$timearr = array('0','30','45','1','2','3','4');
		$selected = '';
		$options = '';
		for($i=0;$i<count($timearr);$i++){
		    if($timearr[$i]=='0' ){
		        $selected = ($time_interval['value'] == $timearr[$i])? "selected" : "";
		        $options .="<option value='0' $selected>Disable</option>";
		    }else if($timearr[$i]!='45' && $timearr[$i]!='30' ){
				$selected = ($time_interval['value']/60 == $timearr[$i])? "selected" : "";
				$calcval = $timearr[$i]*60;
				$options .="<option value='$calcval' $selected>".$timearr[$i]." ".speak('hours').'</option>';
			}
			else{
				$selected = ($time_interval['value'] == $timearr[$i])? "selected" : "";
				$options .="<option value='$timearr[$i]' $selected >".$timearr[$i]." ".speak('minutes').'</option>';
			}
		}
		if(!empty($_REQUEST['ks']) || (empty($_REQUEST['rowid']) &&  empty($_REQUEST['addnew'])) ){
		echo '</br></br>
			<form id="emp_class_form" name="emp_class_form" enctype="multipart/form-data" method="post" action="">
				<table class="tableSpacing" style="border:solid 2px #aaaaaa;">
					<tr>
						<td align=\'right\' bgcolor=\'#eeeeee\' style=\'padding-left:32px;\'>'.speak("Prompt servers to enter tip out amounts before printing Server Summary").': </td>
						<td align="left" valign="top" style="padding-right:32px"><input type="hidden" name="server_summary_tipout_overlay" id="server_summary_tipout_overlay" value="'.$overLayValue.'"><input type="checkbox" onclick="if (this.checked) setval = &quot;1&quot;; else setval = &quot;0&quot;; document.getElementById(&quot;server_summary_tipout_overlay&quot;).value = setval; " '.$checked.'></td>
					</tr>
					<tr>
						<td align="right" bgcolor="#eeeeee" style="padding-left:32px;" valign="middle"><div class="help_text_dimensional_form_setting_help_text_seperator_Reports" style="position:absolute;left:5px;display:none;"></div>'.speak("Time interval to include other employee in server tipout").':</td>
					<td align="left" valign="top" style="padding-right:32px"><select id="server_tips_time_interval_for_tipout" name="server_tips_time_interval_for_tipout">'.$options.'</select></td>
					</tr>
					<tr>
						<td colspan=\'2\' align=\'center\'>&nbsp;<input type=\'submit\' value="'.speak("Save").'" class=\'saveBtn\'><br>&nbsp;</td>
					</tr>
				</table>
			</form>';
		}
	}
?>

<script type="text/javascript">
$( document ).ready(function() {
    var data = $('#savedValues').val();
    		data = data ? data.split(',') : [];
    		$.each(data, function(key, value){
        		if( value === "<?php echo TIPOUT_TYPE;?>")
        		{
        			$('#break_'+key).val(value);
        			$('#evenly_tipout_rules_item'+key).attr('checked', true);
            	}else {
            		$('#break_'+key).val(value);
            		$('#hoursworked_tipout_rules_item'+key).attr('checked', true);
            }
        });        
});

function empClassHasUser() {
	var classId = [];
	$('select[id^="primary_tipout_rules_item"]').each(function () {
		var selectId = $("#"+this.id).val();
		
		if (selectId && selectId !== null) {
    	 classId.push(selectId);
		}
    });
	$('select[id^="tipout_rules_item"]').each(function () {
		var selectId1 = $("#"+this.id).val();
		if (selectId1 && selectId1 !== null) {
	     classId.push(selectId1);
		}
    });
	tipout_rules_updated();
	$.ajax({
		async: false,
		type: "POST",
		url: "areas/checkUserExistEmpClass.php",
		data: {"empClsId" : classId.toString(), "dataname" : "<?= $data_name ?>", "type" : "all" },
		success: function (response) {
			var obj = JSON.parse(response);
			if (obj.status == 'failure') {
				alert(obj.message + ' classes does not have any user. Please select other');
				return false;
			} else {
				form1_validate();
			}
			
		}
	});
}
</script>

<div id='enableTipSharing' title="Enable Tip Sharing" style= 'display:none'>
	<ul>
		<li><b>Enable Tip Sharing - </b>Enables tip sharing for this employee class. All employees clocked in using this employee class will be a part of the applicable tip sharing rules.
		</li>
	</ul>
</div>

<div id='tipSharingRules' title='Tip Sharing Rule' style= 'display:none'>
	<ul>
		<li><b>Tip Sharing Rule - </b>Setting this rule to Evenly will split up the tips evenly amongst all servers. Setting this rule to Hours Worked, will calculate the tips generated per hour and pay the hourly tips to each server based on the hours worked.
		</li>
	</ul>
</div>
<div id='includeServerOwes' title='Include Server Owes' style= 'display:none'>
	<ul>
		<li><b>Include Tip In amount for Tip Sharing in Server Owes House / House Owes Server calculation - </b> Enabling this setting will automatically add a servers Tip In amount for tip sharing into their Server Owes House calculation on their server summary.
		</li>
	</ul>
</div>
<div id='timePeriod' title='Time Period' style= 'display:none'>
	<ul>
		<li><b>Time Period for Tip Sharing -</b> This setting specifies how often the tip pool is generated. If "By Shift" is selected, you will need to specify when the shift change occurs. All users that clock in before the shift change are a part of the first shift and all users that clock in after the shift change are a part of the second shift.
		</li>
	</ul>
</div>
<div id='tipOut' title='Tip-Out Rule' style= 'display:none'>
	<ul>
		<li>Setting this rule to Evenly will split up the tips evenly amongst all employees. </li>
		<li>Setting this rule to Hours Worked, will calculate the tips generated per hour and pay the hourly tips to each employee based on the hours worked.
		</li>
	</ul>
</div>	
				

<script language="javascript">
$(document).ready(function () {
	if(document.getElementById('enable_tip_sharing'))
	{
	if(document.getElementById('enable_tip_sharing').value == '0'){
		document.getElementById('tip_sharing_ruleid').style.display = 'none';
        document.getElementById('include_server_owesid').style.display = 'none';
        document.getElementById('tip_sharing_periodid').style.display = 'none';
	}
	else{
		document.getElementById('tip_sharing_ruleid').style.display = '';
        document.getElementById('include_server_owesid').style.display = '';
        document.getElementById('tip_sharing_periodid').style.display = '';
	}
	}
})
function showHideTipSettings() {
	var tip = document.getElementById('enable_tip_sharing').value;
	if(tip == 1) {
		document.getElementById("evenly").checked = true;
		document.getElementById("daily").checked = true;
		document.getElementById('include_server_owes').value = '1';
		document.getElementById('tip_sharing_ruleid').style.display = '';
        document.getElementById('include_server_owesid').style.display = '';
        document.getElementById('tip_sharing_periodid').style.display = '';
	}else{
		document.getElementById("evenly").checked = false;
		document.getElementById("daily").checked = false;
		document.getElementById('include_server_owes').value = '0';
		document.getElementById('tip_sharing_ruleid').style.display = 'none';
        document.getElementById('include_server_owesid').style.display = 'none';
        document.getElementById('tip_sharing_periodid').style.display = 'none';
	}
}
$('#shift').click(function(){
	$('#tip_refundtr').show();
	$('#weekly_tip').hide();
});
$('#daily').click(function(){
	$('#tip_refundtr').hide();
	$('#weekly_tip').hide();
});
$('#weekly').click(function(){
	$('#weekly_tip').show();
	$('#tip_refundtr').hide();
});
function openHelpPopup(popUpID) {
    $( "#"+popUpID ).dialog({
       	resizable: false,
       	height: 'auto',
		width: '42em',
		modal: true,
        });
}
/**
 * This functionality is added to validate TipoutRule , Tip Refund Ruples.
 */
$("#list_tipout_rules").on("keypress keyup", ".tipPercentage", function (event) {
   var $txtBox = $(this);
   if ($txtBox.val() > 100) {
	   $txtBox.val($txtBox.val().slice(0,-1));
	   return false;
   }

   var charCode = (event.which) ? event.which : event.keyCode
   if (charCode > 31 && (charCode < 48 || charCode > 57) && charCode != 46)
       return false;
   else {
       var len = $txtBox.val().length;
       var index = $txtBox.val().indexOf('.');

       if (index > -1 && charCode == 46) {
         return false;
       }
       if (index > -1) {
           var charAfterdot = (len + 1) - index;
           if (charAfterdot > 3) {
               return false;
           }
       }
   }
   return true;
});

$("#tiprefund_rules").on("keypress keyup", function (event) {
	   var $txtBox = $(this);
	   if ($txtBox.val() > 100) {
		   $txtBox.val($txtBox.val().slice(0,-1));
		   return false;
	   }

	   var charCode = (event.which) ? event.which : event.keyCode
	   if (charCode > 31 && (charCode < 48 || charCode > 57) && charCode != 46)
	       return false;
	   else {
	       var len = $txtBox.val().length;
	       var index = $txtBox.val().indexOf('.');

	       if (index > -1 && charCode == 46) {
	         return false;
	       }
	       if (index > -1) {
	           var charAfterdot = (len + 1) - index;
	           if (charAfterdot > 3) {
	               return false;
	           }
	       }
	   }
	   return true;
	});
</script>
