
 <?php

// START POP UP 
// shop.lavu.com pop up
echo "<div id='shopPop'>
  <p>
    Announcing Shop Lavu, Lavu's new online store.<br />
    Please visit <a href='https://shop.lavu.com/?utm_source=Customer&utm_campaign=Shop_Lavu&utm_medium=CP_Pop_Up' target='_blank'>http://shop.lavu.com/</a> for all your POS hardware needs.
</p>
<div class='closeBtn' id='closeBtn'>X</div><br />

</div>
<script>
  var pop = document.getElementById('shopPop');
  var close = document.getElementById('closeBtn').onclick = function() {closePop()};
  var isClosed = false;
  
  function GetCookie(name) {
    var arg=name+'=';
    var alen=arg.length;
    var clen=document.cookie.length;
    var i=0;
    while (i<clen) {
      var j=i+alen;
      if (document.cookie.substring(i,j)==arg)
      return 'here';
      i=document.cookie.indexOf(' ',i)+1;
      if (i==0) break;
    }
      return null;
  }
    var visit=GetCookie('COOKIE1');
    if (visit==null) { 
      var expire=new Date();
      pop.style.display= 'inline-block';
      expire=new Date(expire.getTime()+7776000000);
      document.cookie='COOKIE1=here; expires='+expire;
    }
  
  function closePop(div) {
    pop.style.display = 'none';
    console.log('closed');
    isClosed = true;
  }

</script>"

// END POP UP
?>