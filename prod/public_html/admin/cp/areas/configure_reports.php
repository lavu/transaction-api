<?php

	if ($in_lavu) {

		function checkGroup($action ='', $chain_id, $group_name, $selected_group_ids, $group_id ='') {

			$whereStr = ($group_id) ? ' and id != '.$group_id : '';
			$exist_query = mlavu_query("select * from `poslavu_MAIN_db`.`chain_reporting_groups` where `_deleted`!='1' and `chainid`='[1]' and (`name` LIKE '[3]' or `contents` LIKE '[2]')".$whereStr,$chain_id,$selected_group_ids,$group_name);

			if($exist_query->num_rows)
			{
				$group_read = mysqli_fetch_assoc($exist_query);
				if($action == 'new' && ($group_name == $group_read['name'] || $selected_group_ids == $group_read['contents'])) {
					return true;
				} else if ($action == 'edit' && $group_name == $group_read['name']){
					return true;
				} else if ($action == 'edit' && ($selected_group_ids == $group_read['contents']) && $group_name != $group_read['name']) {
					return true;

				} else if ($action == 'edit' && ($selected_group_ids != $group_read['contents']) && $group_name == $group_read['name']) {
					return true;

				} else {
					return false;
				} 
			}
			return false;

		}
	
		$return_mode = (isset($_GET['return_mode']))?$_GET['return_mode']:"";
		$rbase_url = "index.php?mode={$section}_{$mode}&return_mode=$return_mode";
		echo "<a href='index.php?mode=$return_mode'><< ".speak("Back to Reports")."</a><br><br>";

		$company_info = getCompanyInfo(admin_info("dataname"),"data_name");
		if($company_info['chain_id']!="")
		{
			$chain_exists = true;
		}
		else $chain_exists = false;

		if($chain_exists)
		{
			$chain_locations = admin_getChainLocations();
			$chain_id = "";
			if(count($chain_locations) > 1)
			{
				$chain_restaurants = array();
				echo "<table cellspacing=0 cellpadding=6 bgcolor='#777777'><tr><td width='700' align='center' style='color:#ffffff; font-weight:bold'>Reporting Groups</td></tr></table><br>";
				
				$str = "";
				$js = "";
				$js .= "var setstr = ''; var lcount = 0; ";
				$str .= "<table>";
				if(isset($_GET['edit_group']) && $_GET['edit_group'] > 0 && !isset($_GET['edit_reporting_group'])) {
					$group_id = $_GET['edit_group'];
					$js .= "var group_name = ''; ";
					$group_query = mlavu_query("select * from `poslavu_MAIN_db`.`chain_reporting_groups` where `chainid`='[1]' and `id`='[2]' and `_deleted`!='1'",$_GET['chainid'], $group_id);
					$group_read = mysqli_fetch_assoc($group_query);
					$contents = explode(",", $group_read['contents']);
					for($i=0; $i<count($chain_locations); $i++)
					{
						$loc = $chain_locations[$i];
						$cid = "lcheckbox_".$i;
						$c_loc_id = $loc['r_id'];
						$chain_id = $loc['r_chain_id'];
						$chain_restaurants[$c_loc_id] = $loc;
						$checked = (in_array($c_loc_id,  $contents)) ? 'checked' : '';
						$js .= "if(document.getElementById('$cid').checked) { lcount++; if(setstr!='') setstr += ','; setstr += '$c_loc_id'; } ";
						$str .= "<tr><td align='right'><input type='checkbox' name='$cid' id='$cid' $checked></td><td align='left'>#" . (100000 + $c_loc_id) . "</td><td align='left'>" . $loc['title'] . "<br>" . "</td></tr>";
						//foreach($chain_locations[$i] as $key => $val)
						//	echo $key . " = " . $val . "<br>";
					}
					$js .= "group_name = document.getElementById('group_name').value;";
					$str .= "<tr><td align='right'>Group Name</td><td align='left' colspan='2'><input type='text' name='group_name' id='group_name' value='".$group_read['name']."'></td></tr>";
					$str .= "<tr><td></td><td align='left' colspan='2'><input type='button' value='Save Group >>' onclick='edit_reporting_group()'></td></tr>";
					$str .= "</table>";
					$str .= "<br>";
					echo "<script language='javascript'>";
					echo "function edit_reporting_group() { ";
					echo $js;
					echo "  if(document.getElementById('group_name').value=='') alert('Provide a group name to continue'); ";
					echo "  else if(lcount < 2) alert('Please choose at least 2 locations\\nto create a new reporting group'); else window.location = \"$rbase_url&edit_group=$group_id&group_name=\" + document.getElementById('group_name').value + \"&edit_reporting_group=\" + setstr ";
					echo "} ";
					echo "</script>";
					echo "<table>";
					echo "<tr><td valign='top'>";
					echo $str;
					echo "</td></tr>";
					echo "</table>";


				} else {
					for($i=0; $i<count($chain_locations); $i++)
					{
						$loc = $chain_locations[$i];
						$cid = "lcheckbox_".$i;
						$c_loc_id = $loc['r_id'];
						$chain_id = $loc['r_chain_id'];
						$chain_restaurants[$c_loc_id] = $loc;
						
						$js .= "if(document.getElementById('$cid').checked) { lcount++; if(setstr!='') setstr += ','; setstr += '$c_loc_id'; } ";
						$str .= "<tr><td align='right'><input type='checkbox' name='$cid' id='$cid'></td><td align='left'>#" . (100000 + $c_loc_id) . "</td><td align='left'>" . $loc['title'] . "<br>" . "</td></tr>";
						//foreach($chain_locations[$i] as $key => $val)
						//	echo $key . " = " . $val . "<br>";
					}
					$str .= "<tr><td align='right'>Group Name</td><td align='left' colspan='2'><input type='text' name='group_name' id='group_name'></td></tr>";
					$str .= "<tr><td></td><td align='left' colspan='2'><input type='button' value='Create New Reporting Group >>' onclick='create_new_reporting_group()'></td></tr>";
					$str .= "</table>";
					$str .= "<br>";
		
					if(isset($_GET['new_reporting_group']))
					{
						$set_group_name = $_GET['group_name'];
						$set_group_ids = $_GET['new_reporting_group'];

						if (checkGroup('new', $chain_id, $set_group_name, $set_group_ids)) {
							echo "The group you are trying to create already exists<br><br>";
						} else {
							mlavu_query("insert into `poslavu_MAIN_db`.`chain_reporting_groups` (`chainid`,`contents`,`name`) values ('[1]','[2]','[3]')",$chain_id,$set_group_ids,$set_group_name);
						}

					}
					else if(isset($_GET['edit_reporting_group']))
					{
						$set_group_name = $_GET['group_name'];
						$set_group_ids = $_GET['edit_reporting_group'];
						$set_id = $_GET['edit_group']; 
						if (checkGroup('edit', $chain_id, $set_group_name, $set_group_ids, $set_id)) {
							echo "The group you are trying to change already exists<br><br>";
						} else {
							mlavu_query("update `poslavu_MAIN_db`.`chain_reporting_groups` set `contents`='[1]',`name`='[2]' where `chainid`='[3]' and `id`='[4]'",$set_group_ids, $set_group_name, $chain_id, $set_id);
						}
					}
					else if(isset($_GET['remove_group']))
					{
						mlavu_query("update `poslavu_MAIN_db`.`chain_reporting_groups` set `_deleted`='1' where `chainid`='[1]' and `id`='[2]'",$chain_id,$_GET['remove_group']);
					}
					
					$liststr = "";
					$group_query = mlavu_query("select * from `poslavu_MAIN_db`.`chain_reporting_groups` where `chainid`='[1]' and `chainid`!='' and `_deleted`!='1'",$chain_id);
					if(mysqli_num_rows($group_query))
					{
						while($group_read = mysqli_fetch_assoc($group_query))
						{
							$group_id = $group_read['id'];
							$liststr .= "<table cellspacing=0 cellpadding=4 style='border:solid 1px #aaaaaa' bgcolor='#eeeeee'>";
							
							$liststr .= "<tr><td bgcolor='#dddddd' align='center'>";
							$liststr .= "<table cellspacing=0 cellpadding=0 width='100%'><tr><td>&nbsp;</td>";
							$liststr .= "<td align='center'>" . $group_read['name'] . "</td>";
							$liststr .= "<td align='right' valign='top'>&nbsp;&nbsp;<font style='color:#99bc08; font-weight:bold; cursor:pointer' onclick='window.location = \"$rbase_url&edit_group=$group_id&chainid=$chain_id\"'>Edit</font>&nbsp;&nbsp;<font style='color:red; font-weight:bold; cursor:pointer' onclick='if(confirm(\"Are you sure you want to remove this reporting group?\")) window.location = \"$rbase_url&remove_group=$group_id\"'>x</font></td></tr></table>";
							$liststr .= "</td></tr>";
							$liststr .= "<tr><td align='left'>";
							$group_ids = explode(",",$group_read['contents']);
							for($n=0; $n<count($group_ids); $n++)
							{
								if(isset($chain_restaurants[$group_ids[$n]]))
								{
									$loc = $chain_restaurants[$group_ids[$n]];
									$c_loc_id = $loc['r_id'];
									$liststr .= "#" . (100000 + $c_loc_id) . " - " . $loc['title'] . "<br>";
								}
								else $liststr .= "cannot find " . $group_ids[$n] . "<br>";
							}
							$liststr .= "</td></tr></table><br>";
						}
					}
					else
					{
						$liststr .= "<table cellspacing=0 cellpadding=6 style='border:solid 1px #aaaaaa' bgcolor='#eeeeee'><tr><td>You do not have any<br>reporting groups<br>defined</td></tr></table>";
					}
					
					echo "<script language='javascript'>";
					echo "function create_new_reporting_group() { ";
					echo $js;
					echo "  if(document.getElementById('group_name').value=='') alert('Provide a group name to continue'); ";
					echo "  else if(lcount < 2) alert('Please choose at least 2 locations\\nto create a new reporting group'); else window.location = \"$rbase_url&group_name=\" + document.getElementById('group_name').value + \"&new_reporting_group=\" + setstr; ";
					echo "} ";
					echo "</script>";
					echo "<table>";
					echo "<tr><td valign='top'>";
					echo $liststr;
					echo "</td><td width='20'>&nbsp;</td><td valign='top'>";
					echo $str; // for creating a new group or making modifications
					echo "</td></tr>";
					echo "</table>";
				}
			}
		}
	
		//$cgroups = getChainReportingGroups(admin_info('dataname'));
		//for($n=0; $n<count($cgroups); $n++) { foreach($cgroups[$n] as $key => $val) echo $key . " = " . $val . "<br>"; }
		
		echo "<table cellspacing=0 cellpadding=6 bgcolor='#777777'><tr><td width='700' align='center' style='color:#ffffff; font-weight:bold'>Display Settings</td></tr></table>";
		echo "<br>Check the individual reports you would like to appear in your reports section.<br><br>";

		$allowed_restricted = str_replace(array("||", "|"), array(",", ""), (!empty($location_info['show_restricted_report_ids'])?$location_info['show_restricted_report_ids']:"0"));
		
		$report_list = array();
		$get_reports = mlavu_query("SELECT `id`, `title` FROM `poslavu_MAIN_db`.`reports` WHERE `categoryid` != '' AND `display` = '1' AND (`restrict` = '0' OR `id` IN (".$allowed_restricted.")) ORDER BY `title` ASC");
		while ($info = mysqli_fetch_assoc($get_reports)) {
			$report_list[] = array("id"=>$info['id'], "title"=>$info['title']);
		}
		
		//echo print_r($_POST, true);
		
		if (isset($_POST['posted'])) {		
			$hide_these = array();
			foreach ($report_list as $report) {
				if (!isset($_POST['rid_'.$report['id']])) $hide_these[] = $report['id'];
			}
			if (count($hide_these) > 0) $hide_str = "|".implode("||", $hide_these)."|";
			else $hide_str = "";
			if (isset($location_info['hide_report_ids'])) 
			{
				echo "Updating...<br>";
				$update = lavu_query("UPDATE `config` SET `value` = '[1]' WHERE `location` = '[2]' AND `type` = 'location_config_setting' AND `setting` = 'hide_report_ids'", $hide_str, $locationid);
			}
			else 
			{
				echo "Inserting...<br>";
				$update = lavu_query("INSERT INTO `config` (`location`, `type`, `setting`, `value`) VALUES ('[1]', 'location_config_setting', 'hide_report_ids', '[2]')", $locationid, $hide_str);
			}
			if ($update) $location_info['hide_report_ids'] = $hide_str;

			echo "<script language='javascript'>window.location.replace('index.php?mode=".$_REQUEST['return_mode']."')</script>";
		}
				
		$hidden_reports = explode(",", str_replace(array("||", "|"), array(",", ""), (isset($location_info['hide_report_ids'])?$location_info['hide_report_ids']:"")));
	
		if (count($report_list) > 0) {
			echo "<form name='form1' action='' method='POST'>";
			echo "<table cellpadding='2'>";
			$count = 0;
			foreach($report_list as $report) {
				if (($count % 3) == 0) echo "<tr>";
				$checked = " CHECKED";
				if (in_array($report['id'], $hidden_reports)) $checked = "";
				echo "<td align='right' style='padding: 2px 2px 2px 22px'><input type='checkbox' name='rid_".$report['id']."' value='1'".$checked."></td><td align='left'>".$report['title']."</td>";
				$count++;
				if (($count % 3) == 0) echo "</tr>";
				else echo "<td>&nbsp;&nbsp;&nbsp;</td>";
			}
			echo "<tr><td><input type='hidden' name='return_mode' value='".$_REQUEST['return_mode']."'>&nbsp;</td></tr>";
			echo "<tr><td><input type='hidden' name='posted' value='1'>&nbsp;</td></tr>";
			echo "<tr><td colspan='8' align='center'><input type='submit' value='Submit'></td></tr>";
			echo "</table>";
			echo "</form>";
		}
	}
?>