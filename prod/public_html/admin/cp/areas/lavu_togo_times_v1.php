<?php
	require_once(dirname(__FILE__)."/../resources/core_functions.php");
	require_once(resource_path()."/lavuquery.php");
	$mode = "settings_edit_lavutogo_payment";
	$edit = "1";
	$display = "<head>";
	session_start($_GET['session_id']);
	$data_name =sessvar("admin_dataname");
	$req_schm = "https";
	if ($_SERVER['HTTP_HOST'] === 'admin.localhost' || $_SERVER['HTTP_HOST'] === 'localhost:8888') {
		$req_schm = "http";
	}
	$display .= "<form name='frm' id='myForm' method='post' action='' >";
	$display .="<h2>Days Open</h2>";
	$display .= "<table cellspacing=0 align='center' style='border:solid 2px #77777,width:200px'>";
	$display_values=array();
	$query = "select value from config where setting='ltg_start_end_time'";
	$data = lavu_query($query);
	while ($row = mysqli_fetch_assoc($data)) {
		$display_values = $row;
	}
	$decode=json_decode($display_values['value']);
	$arr=json_decode($display_values['value'], true);
	$days_of_week	=array( "Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday" );
	foreach ($days_of_week as $index => $day) {		 	
		$checked = "";
		$starttime = "";
		$endtime = "";

		if (array_key_exists($day, $arr)) {
			$checked = "checked";
			$starttime =explode(":", $arr[$day]['starttime']);
			$endtime =explode(":", $arr[$day]['endtime']);
		}

		$display .= "<tr>";
		$display.="<td><input type='checkbox'  name='week[]' id='week' value=".$day." ".$checked.">".$day."</td>";

		$display.="<tr>";
		$display.="<td>Start time:</td>";
		$display.="<td><select name='starthours[]'>";
		for ($i=01;$i<=12;$i++) {
			$hselected = "";
			if (!empty($starttime)) {
				if ($i == $starttime[0]) {
					$hselected = "selected";
				} else {
					$hselected = "";
				}
			}
			if ($i<10) {
				$display.="<option value='0".$i."' ".$hselected.">0$i</option>";
			} else {
				$display.="<option value='".$i."' ".$hselected.">$i</option>";
			}
		}
		$display.="</select>";
		$display.="</td>";
		$display.="<td><select name='startminutes[]'>";
		for ($i=00;$i<60;$i++) {
			$mselected = "";
			if ($i==$starttime[1]) {
				$mselected = "selected";
			} else {
				$mselected = "";
			}
			if ($i<10) {
				$display.="<option value='0".$i."' ".$mselected.">0$i</option>";
			} else {
				$display.="<option value='".$i."' ".$mselected.">$i</option>";
			}
		}
		$display.="</select>";
		$display.="</td>";
		$display.="<td><select name='sampm[]'>";
		$ampmselected = "";
		if ($starttime[2]=="pm") {
			$ampmselected = "selected";
		} else {
			$ampmselected = "";
		}
		$display.="<option value='am' ".$ampmselected." >am</option>";
		$display.="<option value='pm' ".$ampmselected." >pm</option>";
		$display.= "</select>";
		$display.= "</td>";
		$display.="<td>End time:</td>";
		$display.="<td><select name='endhours[]'>";
		for ($i=01;$i<=12;$i++) {
			$ehselected = "";
			if (!empty($endtime)) {
				if ($i == $endtime[0]) {
					$ehselected = "selected";
				} else{
					$ehselected = "";
				}
			}
			if($i<10) {
				$display.="<option value='0".$i."' ".$ehselected." >0$i</option>";
			} else {
				$display.="<option value='".$i."' ".$ehselected." >$i</option>";
			}
		}
		$display.="</select>";
		$display.="</td>";
		$display.="<td><select name='endminutes[]'>";
		for($i=00;$i<60;$i++) {
			$emselected = "";
			if($i == $endtime[1]) {
				$emselected = "selected";
			} else {
				$emselected = "";
			}
			if($i<10) {
				$display.="<option value='0".$i."' ".$emselected.">0$i</option>";
			} else {
				$display.="<option value='".$i."'  ".$emselected.">$i</option>";
			}
		}
		$display.="</select>";
		$display.="</td>";
		$eampmselected = "";
		if($endtime[2]=="pm") {
			$eampmselected = "selected";
		} else {
			$eampmselected = "";
		}
		$display.="<td><select name='eampm[]'>";
		$display.="<option value='am' ".$eampmselected.">am</option>";
		$display.="<option value='pm' ".$eampmselected.">pm</option>";
		$display.= "</select>";
		$display.= "</td>";
		$display.="</tr>";
	}
	$display .= "</table>";
	$display .= "<br>";
	$display .= "&nbsp;<input type='submit' value='".speak("Save")."' name='sub' class='saveBtn'><br>&nbsp;";
	$display .= "</form>";
	echo $display;
	if(isset($_POST['sub'])) {
		$week=$_POST['week'];
		$shours=$_POST['starthours'];
		$ehours=$_POST['endhours'];
		$startminutes=$_POST['startminutes'];
		$endminutes=$_POST['endminutes'];
		$sampm=$_POST['sampm'];
		$eampm=$_POST['eampm'];
		$weeks='';
		$datetime = date('Y-m-d');
		$day_num = date('w', strtotime($datetime));
		$startDateFormat = $sampm[$day_num];
		if($startDateFormat == "am") {
			$lavu_togo_start_time  = $shours[$day_num].$startminutes[$day_num];
		} else {
			$startTime = $shours[$day_num].":".$startminutes[$day_num]." ".$startDateFormat;
			$lavu_togo_start_time = DATE("Hi", STRTOTIME($startTime));
		}
		$endDateFormat = $eampm[$day_num];
		if($endDateFormat == "am") {
			$lavu_togo_end_time = $ehours[$day_num].$endminutes[$day_num];
		} else {
			$endTime = $ehours[$day_num].":".$endminutes[$day_num]." ".$endDateFormat;
			$lavu_togo_end_time = DATE("Hi", STRTOTIME($endTime));
		}

		foreach ($week as $key=>$wk) {
			$week_key = array_search($wk, $days_of_week);
			$wk_time[$wk]['starttime'] = $shours[$week_key].":".$startminutes[$week_key].":".$sampm[$week_key];
			$wk_time[$wk]['endtime'] = $ehours[$week_key].":".$endminutes[$week_key].":".$eampm[$week_key];
		}

		$weekJson=json_encode($wk_time);
		$query = "update config set value='$weekJson' where setting='ltg_start_end_time'";
		$result = lavu_query($query);

		$query = "select `setting` from config where `setting` = 'lavu_togo_start_time' || `setting` = 'lavu_togo_end_time' ";
		$result_select = lavu_query($query);
		$numRec = mysqli_num_rows($result_select);

		if($numRec > 0) {
			$queryUpdateStart = "update config set value='$lavu_togo_start_time' where setting='lavu_togo_start_time'";
			$resultStart = lavu_query($queryUpdateStart);

			$queryUpdateEnd = "update config set value='$lavu_togo_end_time' where setting='lavu_togo_end_time'";
			$resultEnd = lavu_query($queryUpdateEnd);

		} else {
			$locationID = sessvar("locationid");
			$startinsert_query = "INSERT INTO `config` (`location`, `type`, `setting`, `value`) VALUES ('".$locationID."', 'location_config_setting', 'lavu_togo_start_time', '".$lavu_togo_start_time."')";
			$successStart = lavu_query($startinsert_query);
			$startinsert_query = "INSERT INTO `config` (`location`, `type`, `setting`, `value`) VALUES ('".$locationID."', 'location_config_setting', 'lavu_togo_end_time', '".$lavu_togo_end_time."')";
			$successStart = lavu_query($startinsert_query);
		}
		unset($_POST);
		header('location:'.$_SERVER['REQUEST_URI']);
	}
?>


