<?php
/**
 * Created by PhpStorm.
 * User: Leif
 * Author :Irshad Ahmad
 * Date: 17/04/18
 * Time: 10:15 AM
 */

if (isset($_POST['type']) && $_POST['type'] != "") {
    $action = $_POST['type'];
    $tplId = $_POST['tpl_id'];
    $data = $_POST['data'];
    $dataname = $_POST['dataname'];
    $result = "";
    require_once(dirname(__FILE__)."/../resources/core_functions.php");
    $fieldDtls = arrangeFormData($data);
  
    if ($action == "preview") {
        $result = getFiscalTemplatePreview($fieldDtls, $dataname, $tplId);
    }
    else if ($action=="save") {
        $result = saveFiscalTemplate($fieldDtls, $dataname);
    }
   
    echo $result;
}

if (isset($_POST['action']) && $_POST['action'] == "isrequiredfieldvaluemissing") {
	include_once($_SERVER['DOCUMENT_ROOT'].'/lib/jcvs/FiscalReceiptTplApi.php');
	$obj = new FiscalReceiptTplApi($_POST['dataname'], array('tpl_id'=>$_POST['tpl_id'], 'action'=>$_POST['action']));
	$reqFieldStatus = $obj->doProcess();
	echo $reqFieldStatus;
}

function getFiscalTemplatePreview($fieldDtls, $dataname, $tplId) {
   
	include_once($_SERVER['DOCUMENT_ROOT'].'/lib/jcvs/FiscalReceiptTplApi.php');
	$obj=new FiscalReceiptTplApi($dataname,array('action'=>'gettplfields', 'tpl_id'=>$tplId));
	$result=$obj->doProcess();

    $preview = "<table width='100%'>";
    $fieldCnt = 0;
  
	foreach($result as $val){ 

	if(!$fieldDtls[$val['fiscal_receipt_tpl_field_id']]) continue;

	$fieldData=$fieldDtls[$val['fiscal_receipt_tpl_field_id']];

    	if ($fieldData['type']=="div")
    		$fieldData['display']=1;
    	
        if($fieldData['display']){
            $preview .= "<tr width='450px' style='font-size: 10px;'>";
            
           $textAlign = "left";
           if ($fieldData['pos'] == 'R') {
            	$textAlign = "right";
            }
            else if ($fieldData['pos'] == 'C') {
            	$textAlign = "center";
            }
            
            if ($fieldData['type']=='field') {
            	$showTextPreview = ($fieldData['value'])? $fieldData['value'] : $fieldData['fieldLabel'];
            	$preview .= "<td  style='justify-content: center;'><div style='text-align:".$textAlign."'>".$showTextPreview."</div></td>";
            }
            else if ($fieldData['type'] == 'system') {
            	
            	if ($fieldData['title'] == 'items_totals_payments') {
            		$orderTableView = sampleOrderDetailsPreview();
            		$preview .= "<td  style='justify-content: center;'><div style='text-align:".$textAlign."'>".$orderTableView."</div></td>";
            	}
            	else {
            		$showLabelPreview = ($fieldData['label'])? $fieldData['label'] : $fieldData['fieldLabel'];
            		$preview .= "<td  style='justify-content: center;'><div style='text-align:".$textAlign."'>".$showLabelPreview."</div></td>";
            	}
            	
            }
            else if($fieldData['type']=='div'){
            	$preview .= "<td  style='justify-content: center;'><div style='border: 1px solid gray;margin: 5px 0;'></div></td>";
            }
            
            $preview .= "</tr>";
            $fieldCnt++;
        }
        
    }
    $preview .= "</table>";
   
    $previewData = $fieldCnt."@#$".$preview;
    return $previewData;
}

//Todo: test with LP-6218.
function saveFiscalTemplate($fieldDtls, $dataname) {
    include_once($_SERVER['DOCUMENT_ROOT'].'/lib/jcvs/ApiHelper.php');
    ApiHelper::includeOrm('FiscalReceiptTplLocal');
    $obj = new FiscalReceiptTplLocal($dataname);
   
    foreach ($fieldDtls as $rec) {
        $obj->tplId = $rec['tpl_id'];
        $obj->tplFieldId = $rec['tplFieldId'];
        $obj->label = $rec['label'];
        $obj->value = trim($rec['value']);
        $obj->display = $rec['display'];
        $obj->updatedBy = $rec['updated_by'];
        $obj->replaceDb();
    }
}

function arrangeFormData($data) {
   
    foreach ($data as $fieldInfo) {
        $fieldDtls = explode("^^^!", $fieldInfo);
        $fieldId = $fieldDtls[0];
        $elementId = $fieldDtls[1];
        $elmName = explode("-", $elementId);
        $elementType = $elmName[0];
        $field = explode("_", $fieldDtls[5]);
        
        $rowData[$fieldId][$elementType] = $fieldDtls[2];
        $rowData[$fieldId]['tplFieldId'] = $fieldDtls[3];
        $rowData[$fieldId]['updated_by'] = $fieldDtls[4];
        $rowData[$fieldId]['type'] = $field[0];
        $rowData[$fieldId]['pos'] = $field[1];
        $rowData[$fieldId]['tplFieldId'] = $fieldId;
        $rowData[$fieldId]['title'] = $fieldDtls[7];
        $rowData[$fieldId]['fieldLabel'] = $fieldDtls[6];
        
     }
    return $rowData;
}

function sampleOrderDetailsPreview() {
	 $orderPreview = "<table style='width:100%'>
 		<tr> <td colspan='2'>Order Details:</td></tr>
 		<tr><td colspan='2'><div style='border: 1px solid gray;margin: 5px 0;'></div></td></tr>
 		<tr><td>1. ".speak("Item Name")."</td><td><div style='float:right'>$ 7.50</div></td></tr>
 		<tr><td>2. ".speak("Item Name")."</td><td><div style='float:right'>$ 7.50</div></td></tr>
 		<tr><td colspan='2'><div style='border: 1px solid gray;margin: 5px 0;'></div></td></tr>
 		<tr><td><div style='text-align:right'>".speak("Subtotal").":</div></td><td><div style='float:right'>$ 15.00</div></td></tr>
 		<tr><td><div style='text-align:right'>".speak("No Tax").":</div></td><td><div style='float:right'>$ 0.00</div></td></tr>
 		<tr><td><div style='text-align:right'>".speak("Total").":</div></td><td><div style='float:right'>$ 15.00</div></td></tr>
 		<tr><td></td></tr>
 		<tr><td><div style='text-align:right'>".speak("Amount Due").":</div></td><td><div style='float:right'>$ 15.00</div></td></tr>
 		</table>";
	 return $orderPreview;
}
?>

