<?php

	ini_set('display_errors',1);
	require_once(dirname(__FILE__).'/../resources/core_functions.php');
	require_once(dirname(__FILE__).'/../resources/csv_input_output.php');

	if(!empty($_POST['submit_upload'])){
		handleUploadSubmit();
	}else{
		getUploadCSVFileHTML();
	}


	function getUploadCSVFileHTML(){
?>
		<form action="customers_import.php" method="post" enctype="multipart/form-data">
			Select csv file to update:
			<input type="file" name="csv_upload_list" id="csv_upload_file_chooser_id" style="background-color:gray;"><br>
			<input type="submit" value="Add customers in list." name="submit_upload" style="width:200px;">
		</form>
<?php
	}

	function getCustomerCSVTitleToDBFieldMap(){
	/*
     *         1.) First Name                  f_name    input
     *         2.) Last Name                   l_name    input
     *         3.) Email                       field1    input
     *         4.) Phone Number                field2    input      value4 = number THIS IS FOR FUTURE, TO USE/INCLUDE NUMPAD
     *         5.) Street Address              field3    input
     *         6.) City                        field4    input
     *         7.) State                       field5    list_menu  value4 = useTool, value5 = State
     *         8.) Zipcode                     field6    input      value4 = number THIS IS FOR FUTURE, TO USE/INCLUDE NUMPAD
     *         9.) Birthday                    field7    date
     *        10.) Company                     field8    input
     *        11.) How did you hear about us?  field9    input
     *        --.) Work Phone                  field10   input      Not a standard, default field but Brian wanted the field used for Lavu Fit documented for consistency
     *        --.) Cell Phone                  field11   input      (Same as above)
     *        --.) Street Address 2            field12   input      (Same as above)
     */
		return array( "First Name" => "f_name",
			          "Last Name" => "l_name",
			          "Email" => "field1",
			          "Phone Number" => "field2",
			          "Street Address" => "field3",
			          "City" => "field4",
			          "State" => "field5",
			          "Zip Code" => "field6",
			          "Birthday" => "field7",
			          "Company" => "field8",
			          "How did you hear about us?" => "field9"
			          );

	}

	function handleUploadSubmit(){
		lavu_connect_dn($data_name='demo_brian_d',$setdb='poslavu_demo_brian_d_db');
		$tempFilePath=$_FILES["csv_upload_list"]["tmp_name"];
		$customerCSVTitles = getCustomerCSVTitleToDBFieldMap();
		setCSVFieldToDBFieldMap( $customerCSVTitles );
		$totalRowsInserted = performInsertForCSVFile($tempFilePath, 'med_customers');
		echo "Total number of rows inserted:".$totalRowsInserted;
	}



?>