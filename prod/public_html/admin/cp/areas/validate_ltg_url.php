<?php
if (!isset($_SESSION)) {
	session_start();
}
require_once (dirname(dirname(dirname(__FILE__))) ."/cp/resources/core_functions.php");
$data_name = sessvar("admin_dataname");
$result = array();
$result ['status'] = 'Failure';
$result ['msg'] = "The Lavu To Go Unique URL you entered is ".$_REQUEST['ltg_restaurant_name'] ." already in use, please create another unique URL. \nTip: Try inserting underscores ('_') in between words EX. Instead of _johnnystaurant use johnnys_restaurant.";
if ($_REQUEST['ltg_restaurant_name'] != '') {
	$ltg_restaurant_query = mlavu_query("SELECT `id` FROM `poslavu_MAIN_db`.`restaurant_locations` WHERE `lavu_togo_name` = '[1]' AND `dataname` != '[2]'", $_REQUEST['ltg_restaurant_name'], $data_name);
	if (mysqli_num_rows($ltg_restaurant_query) == 0) {
		$result ['status'] = "Success";
	}
}
echo json_encode($result);
?>
