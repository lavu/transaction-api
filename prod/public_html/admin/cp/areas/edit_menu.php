<?php
require_once(__DIR__."/../resources/core_functions.php");
if (isset($_POST['price_validation'])) {
	$formData = $_POST;
	$resultInfo = checkMenuPriceValidation($formData);
	echo $resultInfo;
	exit;
} else if (isset($_POST['track86ModifiedStatus'])) {
	$itemIdList = $_POST['trackIdList'];
	$dataName 	= 'poslavu_'.$_POST['dataName'].'_db';
	$resultInfo = updateUnavilableInfo($itemIdList, $dataName, 'menu_items');
	echo $resultInfo;
	exit;
}

if (!$in_lavu)
{
	return;
}

?>
<style>
.loader-style {
	width:32px;
	border-radius:4px; 
	margin-top:24px; 
	height:32px;
}
.center-style {
	color:white;
	font-size:14px;
	font-weight:bold
}
</style>
<?php
$menu_id = $location_info['menu_id'];

// This function just takes an image name and returns all its information from the new uploads table
// we get the last image in case there are multiple ones we want the most updated
// $imageName: It's the name of the image you want to query on the DB
// returns resultRead which contains all information associated to the image that matched
function getUploadRecordInfo($imageName)
{
	$resultRaw = lavu_query("SELECT * FROM file_uploads WHERE `name` = '[1]' OR `storage_key` = '[1]' ORDER BY id DESC LIMIT 1 ", $imageName);
	$resultRead = mysqli_fetch_assoc($resultRaw);
	return $resultRead;
}

// Fix for duplicating menu items because of POST parameters missing due to weird characters in request var names
function getFullPostVars() {
	$vars = array();
	$post_params = explode('&', file_get_contents('php://input'));
	foreach ($post_params as $namevaluepair) {
		list($name, $value) = explode('=', $namevaluepair, 2);
		$vars[urldecode($name)] = urldecode($value);
	}
	return $vars;
}
$_POST = getFullPostVars();
// Add code to adjust the AWS storage key for all images after on the $_POST array.
// Why not just adjust the current functionality which selects new image? The reason
// is that current functionality does not interact with database just relies on file
// system and the image names doing the adjustments on the frontend, to adjust AWS 
// storage key we need to do query the database to obtain the keys of images which 
// were not uploaded and instead were just selected. This code simply check images
// that changed, queries for those AWS keys and adjusts the POST field with the AWS
// key
// we iterate over categories, since we dont know how many we may get we just go until
// the variable is not set and break, categorie image format ic_1_col_image where number
// corresponds to the number of the category, and key is ic_1_col_storage_key.
// for each category we iterate over each product which again we dont know how many they
// will be so we keep going until the variable is not set, product image format: 
// 	ic_1_image_1 where first number is the categorie and second the product, the key 
// format is ic_1_storage_key_1
// for each image changed we need to query the DB and get the corresponding AWS Storage
// key from the DB and set it on the Storage key field to it updated once the data is 
// saved


$categoryCounter = 1;
while(array_key_exists("ic_".$categoryCounter."_col_image" ,$_POST)) {
		$currentCategoryImage = "ic_" . $categoryCounter . "_col_image";
		$currentCategoryStorageKey = 'ic_' . $categoryCounter . '_col_storage_key';
		//query DB for aws key
		if(strlen($_POST[$currentCategoryImage]) > 0)
		{
			$imageInfoCategory = getUploadRecordInfo($_POST[$currentCategoryImage]);
			$_POST[$currentCategoryStorageKey] = $imageInfoCategory['storage_key'];
		}
		$productCounter = 1;
		//Loop over all products in that category
		while(array_key_exists("ic_" . $categoryCounter . "_image_" . $productCounter, $_POST)){
			$currentProductImage = "ic_" . $categoryCounter . "_image_" . $productCounter;
			$currentProductStorageKey = 'ic_' . $categoryCounter . '_storage_key_'. $productCounter;
			//query DB for aws key 
			if(strlen($_POST[$currentProductImage]) > 0)
			{
				$imageInfoProduct = getUploadRecordInfo($_POST[$currentProductImage]);
				$_POST[$currentProductStorageKey] = $imageInfoProduct['storage_key'];
			}
			$productCounter++;
		}
		$categoryCounter++;
}

$menu_query = lavu_query("SELECT * FROM `menus` WHERE `id` = '[1]'", $menu_id);
$menu_read = mysqli_fetch_assoc($menu_query);
$menu_updated = FALSE;

$data_name = admin_info("dataname");
$edit_menu_group_mode = "edit_menu_group";
if(isset($_SESSION['poslavu_234347273_combo'])){
$_SESSION['poslavu_234347273_combo_copy'] = $_SESSION['poslavu_234347273_combo'];
unset($_SESSION['poslavu_234347273_combo']);

}
if(isset($_GET['set_groupid']))
{
	$set_groupid = $_GET['set_groupid'];
	$_SESSION['set_groupid_' . $data_name] = $set_groupid;
}
else if(isset($_SESSION['set_groupid_' . $data_name]))
{
	$set_groupid = $_SESSION['set_groupid_' . $data_name];
}
if($set_groupid!=$edit_menu_group_mode)
{
	set_sessvar("menu_group",$set_groupid);
	$group_query = lavu_query("select * from `menu_groups` where `menu_id`='$menu_id' AND `_deleted` != '1' and id='$set_groupid'");
	if(!mysqli_num_rows($group_query))
	{
		$set_groupid = "0";
	}
}

if($set_groupid==$edit_menu_group_mode)
{
	if(isset($_GET['delete_group']))
	{
		lavu_query("UPDATE `menu_groups` SET `_deleted` = '1' where `id`='[1]'",$_GET['delete_group']);
		$menu_updated = TRUE;
	}
	if(isset($_POST['group_name']))
	{
		$set_group_name = $_POST['group_name'];
		$group_query = lavu_query("select * from `menu_groups` where `group_name`='[1]' and `menu_id`='[2]' AND `_deleted` != '1'",$set_group_name,$menu_id);
		if(mysqli_num_rows($group_query)) //
		{
			echo "<br><br><div><b>Cannot create group:  A group with that name already exists</b></div>";
		}
		else
		{
			$ordby = 'alpha';
			lavu_query("insert into `menu_groups` (`menu_id`,`group_name`,`orderby`) values ('[1]','[2]','[3]')",$menu_id,$set_group_name,$ordby);
			$menu_updated = TRUE;
		}
	}
}

echo speak("Menu Group").": <select id='menu_group_selector' onchange='window.location = \"index.php?mode={$section}_{$mode}&set_groupid=\" + this.value'>";
$group_query = lavu_query("select * from `menu_groups` where `menu_id`='$menu_id' AND `_deleted` != '1' order by `_order` ASC");
while($group_read = mysqli_fetch_assoc($group_query))
{
	if($set_groupid=="0" && $set_groupid!=$edit_menu_group_mode)
		$set_groupid = $group_read['id'];
		echo "<option value='".$group_read['id']."'";
		if($set_groupid==$group_read['id'])
			echo " selected";
			echo ">".$group_read['group_name']."</option>";
}
echo "<option value='$edit_menu_group_mode' style='color:#888888'";
if($set_groupid==$edit_menu_group_mode) echo " selected";
echo ">".speak("Add / Edit Menu Groups")."</option>";
echo "</select>";
$rename_groupid = (isset($_GET['rename']))?$_GET['rename']:false;
$rename_groupto = (isset($_GET['rename_group']))?$_GET['rename_group']:false;
$moveup = (isset($_GET['moveup']))?$_GET['moveup']:false;
$movedown = (isset($_GET['movedown']))?$_GET['movedown']:false;
$change_chain_reporting_group = (isset($_GET['change_chain_reporting_group']) && isset($_GET['crg'])) ? $_GET['change_chain_reporting_group'] : FALSE;
$copy_move_item = (isset($_GET['action']) && ($_GET['action'] == 'copy_move_item' || $_GET['action'] == 'copy_move_category')) ? TRUE : FALSE;
if($moveup || $movedown)
{
	$move_id = 0;
	$set_ord = 1;
	$group_query = lavu_query("select * from `menu_groups` WHERE `menu_id`='[1]' AND `_deleted` != '1' order by _order asc, id asc", $menu_id);
	while($group_read = mysqli_fetch_assoc($group_query))
	{
		lavu_query("update `menu_groups` set `_order`='[1]' where `id`='[2]'",$set_ord,$group_read['id']);
		if($group_read['id']==$moveup || $group_read['id']==$movedown)
			$move_id = $set_ord;
			$set_ord++;
			$menu_updated = TRUE;
	}
	if($moveup && $move_id)
	{
		lavu_query("update `menu_groups` set `_order`=`_order`+2 where `_order`>='[1]' and `id`!='[2]'",$move_id - 1,$moveup);
		$menu_updated = TRUE;
	}
	else if($movedown && $move_id)
	{
		lavu_query("update `menu_groups` set `_order`=`_order`+2 where `_order`>'[1]' or `id`='[2]'",$move_id + 1,$movedown);
		$menu_updated = TRUE;
	}
}

$group_query = lavu_query("select * from `menu_groups` where `group_name`='[1]' and `menu_id`='[2]' AND `_deleted` != '1'",$rename_groupto,$menu_id);
if(mysqli_num_rows($group_query))
{
	echo "<br><br><b>Cannot rename group:  A group with that name already exists</b>";
}
else
{
	if($rename_groupto)
	{
		lavu_query("update `menu_groups` set `group_name`='[1]' where `id`='[2]'",$rename_groupto,$rename_groupid);
		$rename_groupid = false;
		$menu_updated = TRUE;
	}
}

if ($change_chain_reporting_group !== FALSE)
{
	$i_menu_group_id = (int)$change_chain_reporting_group;
	$i_chain_reporting_group = (int)$_GET['crg'];
	$s_query_string = "UPDATE `menu_groups` SET `chain_reporting_group`='[chain_reporting_group]' WHERE `id`='[id]'";
	$a_query_vars = array('chain_reporting_group'=>$i_chain_reporting_group, 'id'=>$i_menu_group_id);
	lavu_query($s_query_string, $a_query_vars);
	$menu_updated = TRUE;
}

if ($copy_move_item)
{
	copy_move_item_category();
}

if ($set_groupid==$edit_menu_group_mode)
{
	require_once('/home/poslavu/public_html/admin/sa_cp/manage_chain_functions.php');
	$b_part_of_chain = getIsDataNameInChain($data_name);

	echo "<br>&nbsp;";
	echo "<table cellpadding=4 cellspacing=0 bgcolor='#eeeeee' style='border:solid 2px #bbbbbb'>";
	$group_query = lavu_query("select * from `menu_groups` where `menu_id`='$menu_id' AND `_deleted` != '1' order by `_order` asc, id asc");
	while($group_read = mysqli_fetch_assoc($group_query))
	{
		$sgid = $group_read['id'];
		echo "<tr>";
		echo "<td><a href='index.php?mode={$section}_{$mode}&set_groupid=$edit_menu_group_mode&movedown=$sgid' id='downArrow'>&#9662;</a></td>";
		echo "<td><a href='index.php?mode={$section}_{$mode}&set_groupid=$edit_menu_group_mode&moveup=$sgid' id='upArrow'>&#9662;</a></td>";

		// get the reporting group options
		if ($b_part_of_chain && DEV ) {
			$s_group_chain_reporting_group = getChainReportingGroupOptions($group_read['chain_reporting_group'], $data_name, 0, FALSE, '', $b_has_access);
			if ($b_has_access)
				$s_group_chain_reporting_group = "<td><select onchange='window.location=\"index.php?mode={$section}_{$mode}&set_groupid=edit_menu_group&change_chain_reporting_group=$sgid&crg=\"+this.value;'>".$s_group_chain_reporting_group."</select></td>";
				else
					$s_group_chain_reporting_group = "<td><select style='display:none;'>".$s_group_chain_reporting_group."</select></td>";
		}

		if($group_read['id'] == 1)
		{
			echo "<td>".$group_read['group_name']."</td>";
			echo $s_group_chain_reporting_group;
			echo "<td colspan='2'>&nbsp;</td>";
		}
		else
		{
			if($rename_groupid==$sgid)
			{
				echo "<td><input type='text' id='rename_group' value=\"".str_replace("\"","&quot;",$group_read['group_name'])."\"></td>";
				echo $s_group_chain_reporting_group;
				echo "<td><input type='button' value='Update' onclick='window.location = \"index.php?mode={$section}_{$mode}&set_groupid=$set_groupid&rename=$rename_groupid&rename_group=\" + document.getElementById(\"rename_group\").value'></td>";
			}
			else
			{
				echo "<td>".$group_read['group_name']."</td>";
				echo $s_group_chain_reporting_group;
				echo "<td><a href='index.php?mode={$section}_{$mode}&set_groupid=$edit_menu_group_mode&rename=$sgid' class='renameBtn'>rename</a></td>";
			}
			echo "<td><a style='cursor:pointer;' onclick='if(confirm(\"Are you sure you want to delete this group?\")) window.location = \"index.php?mode={$section}_{$mode}&set_groupid=$edit_menu_group_mode&delete_group=$sgid\"' class='closeBtn'>X</a></td>";
		}
	}
	echo "</table>";
	echo "<br>";
	echo "<form name='add_new' method='post' action='index.php?mode={$section}_{$mode}&set_groupid=$edit_menu_group_mode' onsubmit='return validateForm()'>";
	echo "Add New Group:";
	// disabling save button unless there is text in text box by
	echo "<br><input type='text' name='group_name' required>";
	echo "<br>";
	echo "<br><input type='submit' value='".speak("Save")."' class='saveBtn' id='Menu' />";
	// safari JS work around
	///////////////
	echo "<script> function validateForm() {
		var menuGroupName = document.forms['add_new'] ['group_name'].value;
		if (menuGroupName == '')
		{alert('Please fill out form');
		return false;}
		}
	</script>";
	echo "</form>";
}
else
{
	// order by
	if(isset($_GET['orderby']))
	{
		lavu_query("update `menu_groups` set `orderby`='[1]' where `id`='[2]'",$_GET['orderby'],$set_groupid);
		$menu_updated = TRUE;
	}
	$group_orderby = "manual";
	$group_query = lavu_query("select * from `menu_groups` where `menu_id`='[1]' and id='[2]'",$menu_id,$set_groupid);
	if(mysqli_num_rows($group_query))
	{
		$group_read = mysqli_fetch_assoc($group_query);
		if(isset($group_read['orderby']))
		{
			$group_orderby = $group_read['orderby'];

			echo "<select onchange='window.location = \"index.php?mode={$section}_{$mode}&set_groupid=$set_groupid&orderby=\" + this.value'>";
			echo "<option value='alpha'".(($group_orderby=="alpha"||$group_orderby=="")?" selected":"").">".speak("Order Alphabetically")."</option>";
			echo "<option value='manual'".(($group_orderby=="manual")?" selected":"").">".speak("Order Manually")."</option>";
			echo "</select>";
		}
	}

	$force_mod_groups = array();
	$force_mods_query = lavu_query("select * from `forced_modifier_groups` where `_deleted`!='1' AND `menu_id` = '[1]' order by `title` asc", $menu_id);
	while($force_mods_read = mysqli_fetch_assoc($force_mods_query))
	{
		$force_mod_groups[] = array("group: " . $force_mods_read['title'],$force_mods_read['id']);
	}

	$lavu_gift_enabled = $modules->hasModule("extensions.payment.lavu.gift");
	$lavu_gift_integrated_workflow = (locationSetting("lavu_gift_integrated_workflow") == "1");

	$lavu_loyalty_enabled = $modules->hasModule("extensions.payment.lavu.loyalty");
	$lavu_loyalty_integrated_workflow = (locationSetting("lavu_loyalty_integrated_workflow") == "1");

	$pepper_enabled = $modules->hasModule("extensions.loyalty.pepper");

	$force_mod_singles = array();
	$fmod_query = lavu_query("SELECT * FROM `forced_modifier_lists` WHERE `_deleted` != '1' AND (`menu_id` = '[1]' OR `menu_id` = '0') ORDER BY `title` ASC", $menu_id);
	while($fmod_read = mysqli_fetch_assoc($fmod_query))
	{
		$mtitle	= $fmod_read['title'];
		$mtype	= $fmod_read['type'];

		if ($mtype=="lavu_gift" && (!$lavu_gift_enabled || !$lavu_gift_integrated_workflow))
		{
			// Skip Lavu Gift integrated modifier types if Lavu Gift is not enabled or is not using the integrated workflow
			continue;
		}

		if ($mtype=="lavu_loyalty" && (!$lavu_loyalty_enabled || !$lavu_loyalty_integrated_workflow))
		{
			// Skip Lavu Loyalty integrated modifier types if Lavu Loyalty is not enabled or is not using the integrated workflow
			continue;
		}

		if (strstr(strtolower($mtitle), "lavu gift") && $mtype=="custom" && (!$lavu_gift_enabled || $lavu_gift_integrated_workflow))
		{
			// Skip Lavu Gift custom modifier type if Lavu Gift is not enabled or is using the integrated workflow
			continue;
		}

		if (strstr(strtolower($mtitle), "lavu loyalty") && $mtype=="custom" && (!$lavu_loyalty_enabled || $lavu_loyalty_integrated_workflow))
		{
			// Skip Lavu Loyalty custom modifier type if Lavu Loyalty is not enabled or is using the integrated workflow
			continue;
		}

		$force_mod_singles[] = array( $mtitle, "f".$fmod_read['id'] );
	}

	addGiftAndLoyaltyExtensions($force_mod_singles);

	$force_mods = array_merge(array(array("Forced Modifiers","0")),$force_mod_singles,$force_mod_groups);
	$force_mods_items = array_merge(array(array("Forced Modifiers","C")),$force_mod_singles,$force_mod_groups);

	$opt_mods = array(array("Optional Modifiers",""));
	$opt_mods_query = lavu_query("select * from `modifier_categories` where `_deleted`!='1' AND `menu_id` = '[1]' order by `title` asc", $menu_id);
	while($opt_mods_read = mysqli_fetch_assoc($opt_mods_query))
	{
		$opt_mods[] = array($opt_mods_read['title'],$opt_mods_read['id']);
	}

	$form_posted = (isset($_POST['posted']));

	$display = "";
	$field = "menu_items";

	$qtitle_category = "Category";
	$qtitle_item = "Item";

	$category_tablename = "menu_categories";
	$inventory_tablename = "menu_items";
	$category_fieldname = "id";
	$inventory_fieldname = "category_id";
	$inventory_select_condition = "";
	$inventory_primefield = "name";
	$category_primefield = "name";
	$combo_tablename='combo_items';

	$inventory_orderby = $inventory_primefield;
	$category_orderby = $category_primefield;
	if ($group_orderby=="manual")
	{
		$inventory_orderby = "_order";
		$category_orderby = "_order";
	}

	// check that this restaurant is part of a chain and get a list of the chain items
	require_once('/home/poslavu/public_html/admin/sa_cp/manage_chain_functions.php');
	$a_chain_groups = array();
	$a_chain_items = array();
	$b_part_of_chain = getReportingGroupOptionsForDimensionalForm($data_name, $a_chain_groups, $a_chain_items);

	// get a list of the printers
	$printer_list = array();
	$printer_list[] = array("No Printer","");
	$pr_query = lavu_query("select * from `config` where `location`='$locationid' and `type`='printer' and `_deleted`!='1' ORDER BY `value2` ASC");
	while ($pr_read = mysqli_fetch_assoc($pr_query))
	{
		if (strtolower(substr($pr_read['setting'],0,7)=="kitchen"))
		{
			$k = substr($pr_read['setting'],7);
			if($k=="") $k = 1;
			$printer_list[] = array($pr_read['value2'],$k);
		}
	}

	$pgroup_query = lavu_query("select * from `config` where `type`='printer_group' AND `location` = '[1]' AND `_deleted` != '1' ORDER BY `setting` ASC", $location_info['id']);
	while($pgroup_read = mysqli_fetch_assoc($pgroup_query))
	{
		$printer_list[] = array($pgroup_read['setting'],$pgroup_read['id'] + 1000);
	}

	$use_ltg = ($location_info['use_lavu_togo']);
	$ltg_ext_enabled = $modules->hasModule("extensions.ordering.lavutogo");

	$cfields = array();
	$cfields[] = array("modifier_list_id","modifier_list_id",speak("Optional Modifiers"),"select",$opt_mods);
	$cfields[] = array("forced_modifier_group_id","forced_modifier_group_id",speak("Forced Modifier Group"),"select",$force_mods);
	if ($b_part_of_chain && lavuDeveloperStatus() )
	{
		$cfields[] = array("chain_reporting_group", "chain_reporting_group", speak("Chain Reporting Group"), "select_exclusive", $a_chain_groups);
	}
	$cfields[] = array("printer","printer",speak("Printer"),"select",$printer_list);
	$cfields[] = array("image","image","Picture","picture","/images/".admin_info("dataname")."/categories/");
	$cfields[] = array("storage_key","storage_key","Document Storage","hidden");
	$cfields[] = array("active","active","<font size='-2' color='#FFFFFF'>".speak('App')."</font><br>","bool","1");
	if ($use_ltg || $ltg_ext_enabled) {
		$cfields[] = array("ltg_display", "ltg_display", "<font size='-2' color='#FFFFFF'>".speak('LTG')."</font><br>", "bool", "1"); // 0 = hide, 1 = show
	}

	if ($pepper_enabled)
	{
		$cfields[] = array("loyalty_display","loyalty_display","<font size='-2' color='#FFFFFF'>".speak('LPL')."</font><br>","bool","1"); // 0 = hide, 1 = show
	}
	if (isset($location_info['kiosk_enabled']) && $location_info['kiosk_enabled'] == 1)
	{
		$cfields[] = array("kiosk_display","kiosk_display","<font size='-2' color='#FFFFFF'>".speak('Kiosk')."</font><br>","bool","1");
	}
	$cfields[] = array("print","print","Print","hidden","1");
	$cfields[] = array("group_id","group_id","Group","hidden","$set_groupid");
	$cfields[] = array("allow_ordertype_tax_exempt","allow_ordertype_tax_exempt","Allow Tax Exemption","hidden","1");
	$chidden = array("description", "tax_inclusion", "_order", "super_group_id", "happyhour", "tax_profile_id", "price_tier_profile_id", "apply_taxrate", "custom_taxrate", "print_order", "no_discount", "enable_reorder","display_on_digital_menu_board");

	for ($h=0; $h<count($chidden); $h++)
	{
		$cfields[] = array($chidden[$h],$chidden[$h],$chidden[$h],"hidden");
	}

	$category_filter_by = "menu_id";
	$category_filter_value = $menu_id;
	$category_filter_extra = "`group_id`='$set_groupid'";
	$inventory_filter_by = "menu_id";
	$inventory_filter_value = $menu_id;
	// form:
	// array($qname, $qfield=$qname, $qtitle=$qname, $qtype=false, $qprop=array(), $qextraval=false)
	//---------------------------------------------------------------------------------
	// $qfields contains the meta data to generate the form for items later, the parameters are:
	// $field-name: which will be used for the name attribute and therefore in the backend
	// $id-name: which will be used for the id attribute on the form field
	// $label: which will be used as a label if the field is uses it
	// $type: which will be used on the type attribute and thereform define the type of input
	//---------------------------------------------------------------------------------
	$qfields = array();
	$qfields[] = array("name","name","Item","input",array("size=6"));
	$qfields[] = array("cost","price","Price","input",array("size=6"));
	$check_for_combo = lavu_query("SELECT `value` FROM `config` WHERE `setting` = 'disable_combo_bulider' AND `location` = '[1]'", $locationid);
	$combo_info = mysqli_fetch_assoc($check_for_combo);
	if($combo_info['value']==1){
	$qfields[] = array("combo","combo","<font size='-2'>".speak('Combo')."</font>","bool","0");
	}else {
		$combo_filter_by = "combo";
		$combo_filter_value = 0;
	}
	$qfields[] = array("modifier_list_id","modifier_list_id",speak("Optional Modifiers"),"select",$opt_mods);
	$qfields[] = array("forced_modifier_group_id","forced_modifier_group_id",speak("Forced Modifier Group"),"select",$force_mods_items);
	if ($b_part_of_chain && lavuDeveloperStatus() )
	{
		$qfields[] = array("chain_reporting_group", "chain_reporting_group", speak("Chain Reporting Group"), "select_exclusive", $a_chain_items);
	}
	$qfields[] = array("ingredients","ingredients", "Ingredients","special");
	$qfields[] = array("comboitems","comboitems","Combo Items","special");

	if ($modules->hasModule("extensions.ordering.lavutogo") || $modules->hasModule("extensions.ordering.kiosk"))
	{
		$qfields[] = array("nutrition","nutrition","Nutrition","special","",); // LP-1411
	}
	/* KIOSK-838 Commenting out this code to hide the Dietary Info column from users
	until it is fully functional in the Kiosk App. FYI - all functionality in CP is working.
	Dietary info is correctly saving to the DB. Uncommenting the following lines will make
	the Dietary Info column reappear in the Menu Items section of CP */
	// if ($modules->hasModule("extensions.ordering.kiosk"))
	// {
	// 	$qfields[] = array("dietary_info","dietary_info","Dietary Info","special");
	// }
	$qfields[] = array("description","description","Description","hidden");
	$qfields[] = array("UPC","UPC","UPC","hidden");
	if ($location_info['display_product_code_and_tax_code_in_menu_screen']) {
		$qfields[] = array("product_code", "product_code", "Product Code", "hidden");
		$qfields[] = array("tax_code", "tax_code", "Tax Code", "hidden");
	}
	$qfields[] = array("print","print","Print","hidden","1");
	$qfields[] = array("printer","printer","Printer","hidden","0");
	// LP-3273
	$qfields[] = array("heart_healthy","heart_healthy","Heart Healthy","hidden","0");
	$qfields[] = array("contains_gluten","contains_gluten","Contains Gluten","hidden","0");
	$qfields[] = array("contains_dairy","contains_dairy","Contains Dairy","hidden","0");
	$qfields[] = array("allow_ordertype_tax_exempt", "allow_ordertype_tax_exempt", "Allow Tax Exemption", "hidden", "Default");
	$qhidden = array("quick_item", "tax_inclusion", "open_item", "hidden_value", "hidden_value2", "hidden_value3", "allow_deposit", "happyhour", "tax_profile_id", "track_86_count", "inv_count", "price_tier_profile_id", "apply_taxrate", "custom_taxrate", "no_discount","super_group_id", "enable_reorder", "currency_type","display_on_digital_menu_board", "kitchen_nickname");
	$qfields[] = array("dining_option","dining_option","Dine in type","hidden","Pickup,Delivery,Dine-In");

	for($h=0; $h<count($qhidden); $h++)
	{
		$qfields[] = array($qhidden[$h],$qhidden[$h],$qhidden[$h],"hidden");
	}

	$qfields[] = array("image","image","Picture","picture",array("/images/".admin_info("dataname")."/items/",array("image2","image3"),"misc_content"));
	$qfields[] = array("image2","image2","Picture 2","hidden");
	$qfields[] = array("image3","image3","Picture 3","hidden");
	$qfields[] = array("misc_content","misc_content","Misc Content","hidden");
	$qfields[] = array("storage_key","storage_key","Document Storage","hidden");
	$qfields[] = array("active","active","<font size='-2'>".speak('App')."</font>","bool","1");
	if ($use_ltg || $ltg_ext_enabled) {
		$qfields[] = array("ltg_display", "ltg_display", "<font size='-2'>".speak('LTG')."</font>", "bool", "1"); // 0 = hide, 1 = category default
	}
	if ($pepper_enabled)
	{
		$qfields[] = array("loyalty_display","loyalty_display","<font size='-2'>".speak('LPL')."</font>","bool","1"); // 0 = hide, 1 = category default
		$qfields[] = array("loyalty_points","loyalty_points","Loyalty Points","hidden");
	}
	if (isset($location_info['kiosk_enabled']) && $location_info['kiosk_enabled'] == 1)
	{
		$qfields[] = array("kiosk_display","kiosk_display","<font size='-2'>".speak('Kiosk')."</font>","bool","1"); 
	}
	$qfields[] = array("id");
	$qfields[] = array("delete");
	require_once(resource_path() . "/dimensional_form.php");
	$qdbfields = get_qdbfields($qfields);

	if ($form_posted)
	{
		function should_schedule_image_sync($row_id, $new_pic_id, $old_pic_id, $type, $command) {

			global $data_name;
			global $locationid;

			if (isset($_POST[$new_pic_id]) && isset($_POST[$old_pic_id]) && $_POST[$new_pic_id]!="" && $_POST[$new_pic_id]!=$_POST[$old_pic_id]) {
				$query_statement = "SELECT `image` FROM `menu_".$type."` WHERE `id` = '[1]'";
				$qResult = lavu_query($query_statement, $row_id);
				if ($resultAssoc = mysqli_fetch_assoc($qResult))
				{
					$fileName = $resultAssoc['image'];
					$dirBase = "/home/poslavu/public_html/admin/images/".$data_name."/".$type."/main/";
					if (is_file($dirBase.$fileName))
					{
						schedule_lls_special_command($data_name, $locationid, $command, $fileName);
					}
				}
			}
		}

		$lls = ((int)$location_info['use_net_path']>0 && !strstr($location_info['net_path'], "poslavu.com") && !strstr($location_info['net_path'], "localhost"));
		if ($lls)
		{
			require_once(dirname(__FILE__) . "/../resources/msync_functions.php");	//Here we sync down the image that was changed to.
		}

		$menu_updated = TRUE;
		if($_POST['posted_dataname']!=$data_name)
		{
			echo "<b>Error: you are no longer logged into $data_name</b><br>";
			exit();
		}

		$cat_count = 1;
		$cacheMenuItems = array();
		while(isset($_POST["ic_".$cat_count."_category"]))
		{
			$category_name = $_POST['ic_'.$cat_count.'_category'];
			$category_id = isset($_POST['ic_'.$cat_count.'_categoryid'])?$_POST['ic_'.$cat_count.'_categoryid']:"";
			$category_delete = $_POST['ic_'.$cat_count.'_delete_0'];
			if ($category_name==$qtitle_category . " Name")
			{
				$category_name = "";
			}
			$cat_delete='0';
			if ($category_delete=="1")
			{
				delete_poslavu_dbrow($category_tablename, $category_id);
				$cat_delete='1';
			}

			if ($category_name!="")
			{ //Update menu_categories table
				$dbfields = array();
				$dbfields[] = array("name",$category_name);
				$dbfields[] = array("menu_id",$menu_id);
				for($n=0; $n<count($cfields); $n++)
				{
					$cfieldname = $cfields[$n][0];
					$dbfields[] = array($cfieldname,$_POST['ic_'.$cat_count.'_col_'.$cfieldname]);
				}
				$categoryid_indb = update_poslavu_dbrow($category_tablename,$dbfields, $category_id);

				if ($lls)
				{
					$new_picture_id = "ic_" . $cat_count . "_col_image";
					$old_picture_id = "previous_value_" . $new_picture_id;
					should_schedule_image_sync($categoryid_indb, $new_picture_id, $old_picture_id, "categories", "download_category_images");
				}

				if ($category_fieldname=="name")
				{
					$categoryid_indb = $category_name;
				}
				$enable_reorder = array();
				$enable_reorder_query = lavu_query("select `enable_reorder` from menu_categories where id = '".$category_id."'");
				
				if(mysqli_num_rows($enable_reorder_query)>0){
				    $enable_reorder = mysqli_fetch_assoc($enable_reorder_query);
				}
				
				$item_count = 1;
				while (isset($_POST["ic_".$cat_count."_".$inventory_primefield."_".$item_count]))
				{
					$id_name= $combo_display = 'ic_' . $cat_count . '_comboitems_' . $item_count;
					$item_value = array(); // get posted values
					for($n=0; $n<count($qfields); $n++)
					{
						$qname = $qfields[$n][0];
						$qtitle = (isset($qfields[$n][2]))?$qfields[$n][2]:false;
						$set_item_value = $_POST['ic_' . $cat_count . '_' . $qname . '_' . $item_count];
						if($qtitle && $set_item_value==$qtitle) $set_item_value = "";
						if($qname=="cost")
						{
							$set_item_value = formatValueForMonetaryDisplay( $set_item_value );
						}
						$item_value[$qname] = $set_item_value;
					}
					$item_id = $item_value['id'];
					$item_delete = $item_value['delete'];
					$item_name = $item_value[$inventory_primefield];

					if($item_delete=="1")
					{//If the item is set to be deleted, delete it from the menu_items table.
						delete_poslavu_dbrow($inventory_tablename, $item_id);
						//If the item is set to be deleted, delete it from the 86count table.
						delete_poslavu_86count($inventory_tablename, $item_id);
						//If the item is set to be deleted, delete it from the combo_items table.
						delete_poslavu_dbrow($combo_tablename, $item_id);
					}
					else if($item_name!="")
					{//If the item is not set to be deleted, add/update in the menu_items table.

						$dbfields = array();
						$dbfields[] = array($inventory_fieldname,$categoryid_indb);
						$cacheMenuItems[$item_id]['id'] = $item_id;
						$cacheMenuItems[$item_id]['inv_count'] = $item_value['inv_count'];
						$cacheMenuItems[$item_id]['track_86_count'] = $item_value['track_86_count'];
						$cacheMenuItems[$item_id]['combo'] = $item_value['combo'];
						for($n=0; $n<count($qdbfields); $n++)
						{
							$qname = $qdbfields[$n][0];
							$qfield = (isset($qdbfields[$n][1]))?$qdbfields[$n][1]:$qname;
							if("enable_reorder" === $qfield){
							    $enableReorder = (isset($enable_reorder["enable_reorder"]))?$enable_reorder["enable_reorder"]:0;
							    $dbfields[] = array($qfield,$enableReorder);
							}else{
								$dbfields[] = array($qfield,$item_value[$qname]);
							}
						}
						$dbfields[] = array("menu_id",$menu_id);
						$orderby_tag = 'icrow_'.$cat_count.'_'.$item_count.'_order';
						if(isset($_POST[$orderby_tag]))
						{
							$dbfields[] = array("_order",$_POST[$orderby_tag]);
						}
						$item_id_indb = update_poslavu_dbrow($inventory_tablename,$dbfields, $item_id);
						if ($item_id_indb!=1){
							if(isset($_SESSION['poslavu_234347273_combo_copy'][$id_name])){
							$commbo=commboInsert($item_id_indb,$_SESSION['poslavu_234347273_combo_copy'][$id_name],$cat_delete);
							unset($_SESSION['poslavu_234347273_combo_copy'][$id_name]);
							}
						}
						if ($lls) {
							$new_picture_id = "ic_" . $cat_count . "_image_" . $item_count;
							$old_picture_id = "previous_value_" . $new_picture_id;
							should_schedule_image_sync($item_id_indb, $new_picture_id, $old_picture_id, "items", "download_item_images");
						}
					}
					$item_count++;
				}
			}
			$cat_count++;
		}
		//Storing menu item id, inv_count, track_86_count and combo value into cache
		setCacheMenuItems($cacheMenuItems);
		$dev = false;
		if (function_exists("lavuDeveloperStatus"))
		{
			$dev = lavuDeveloperStatus();
		}

		// LOCAL SYNC RECORD
		schedule_remote_to_local_sync_if_lls($location_info, $locationid, "table updated", "menu_items");
		schedule_remote_to_local_sync_if_lls($location_info, $locationid ,"table updated", "menu_categories");
		// END LOCAL SYNC RECORD

		$check_for_last_update = lavu_query("SELECT `id` FROM `config` WHERE `setting` = 'menu_last_updated' AND `location` = '[1]'", $locationid);
		if (@mysqli_num_rows($check_for_last_update))
		{
			$uinfo = mysqli_fetch_assoc($check_for_last_update);
			lavu_query("UPDATE `config` SET `value` = now() WHERE `id` = '[1]'", $uinfo['id']);
		}
		else
		{
			lavu_query("INSERT INTO `config` (`location`, `type`, `setting`, `value`) VALUES ('[1]', 'admin_activity', 'menu_last_updated', now())", $locationid);
		}

		lavu_query("UPDATE `devices` SET `needs_reload` = '1' WHERE `loc_id` = '[1]'", $locationid);

		if (loyalTreeEnabledForLocation($location_info))
		{
			require_once(resource_path()."/../../../inc/loyalty/loyaltree/LoyalTreeIntegration.php");
			$args = array(
					'action'     => 'update',
					'dataname'   => $data_name,
					'loc_id'     => $location_info['id'],
					'businessid' => $location_info['loyaltree_id'],
					'chain_id'   => (isset($location_info['loyaltree_chain_id']) ? $location_info['loyaltree_chain_id'] : '0'),
					'storeid'    => admin_info('companyid').$location_info['id'],
					'menu_id'    => $location_info['menu_id'],
			);
			try {
				$result = LoyalTreeIntegration::update($args);
			}
			catch (Exception $e) {
				error_log(__FILE__ ." got exception on menu edit with args=". json_encode($args) .": ". $e->getMessage());
			}
		}
		//If we add or modify category, menu items, modifier, forced modifier then flush menu category cache
		clearMenuCategoryCache();
	}

	$details_column = "Details";
	$send_details_field = false;
	$category_details_column = "Category Details";
	$category_send_details_field = false;

	$qinfo = array();
	$qinfo["qtitle_category"] = $qtitle_category;
	$qinfo["qtitle_item"] = $qtitle_item;
	$qinfo["category_tablename"] = $category_tablename;
	$qinfo["inventory_tablename"] = $inventory_tablename;
	$qinfo["category_fieldname"] = $category_fieldname;
	$qinfo["category_primefield"] = $category_primefield;
	$qinfo["inventory_fieldname"] = $inventory_fieldname;
	$qinfo["inventory_select_condition"] = $inventory_select_condition;
	$qinfo["inventory_primefield"] = $inventory_primefield;
	$qinfo["details_column"] = $details_column;
	$qinfo["send_details"] = $send_details_field;
	$qinfo["category_details_column"] = $category_details_column;
	$qinfo["category_send_details"] = $category_send_details_field;
	$qinfo["field"] = $field;

	$qinfo["category_filter_by"] = $category_filter_by;
	$qinfo["category_filter_value"] = $category_filter_value;
	$qinfo["category_filter_extra"] = $category_filter_extra;
	$qinfo["inventory_filter_by"] = $inventory_filter_by;
	$qinfo["inventory_filter_value"] = $inventory_filter_value;
	$qinfo["combo_filter_by"] = $combo_filter_by;
	$qinfo["combo_filter_value"] = $combo_filter_value;

	$qinfo["category_orderby"] = $category_orderby;
	$qinfo["inventory_orderby"] = $inventory_orderby;

	$field_code = create_dimensional_form($qfields,$qinfo,$cfields);

	$display .= "<br><br><form name='sform' method='post' action='' onsubmit='return checkPriceInformation(this, \"name\") && confirmKitchenNickname(this)'>";
	$display .= "<input type='hidden' name='posted_dataname' value='$data_name'>";
	$display .= "<div class='menu_price_validation' style='color:#ff0000;'></div><br>&nbsp;";
	$display .= "&nbsp;<input type='submit' value='".speak("Save")."' class='saveBtn'><br>&nbsp;";
	$display .= "<input type='hidden' name='posted' value='1'>";
	$display .= $field_code;
	$display .= ($pepper_enabled) ? "<input type='button' name='pepper_menu_json' value='". speak("Get Pepper Menu JSON") ."' onclick='open_dialog_box(555, 448, \"/cp/areas/pepper_menu_json.php?dataname={$data_name}\")'><br><br>" : "";
	$display .= "&nbsp;<input type='submit' value='".speak("Save")."' class='saveBtn'><br>&nbsp;";
	$display .= "<div id='confirmKitchenNickname' style='display:none'></div><div id='duplicateNicknameValidation' style='display:none' title='Warning'></div>";
	$display .= "</form>";

	echo create_info_layer(700,480);
	echo $display;
	echo draw_copy_move_item_category_script();
	require_once(resource_path()."/dimensional_form_js.php");
}

if ($menu_updated && $data_name == 'foo')
{
	require_once('/home/poslavu/public_html/admin/sa_cp/manage_chain_functions.php');
	syncChainDatabases($data_name);
}

function addGiftAndLoyaltyExtensions(&$force_mod_singles)
{
	$get_payment_types = lavu_query("select `type`, `special` from `payment_types` where `_deleted`= '0' and (`special` like 'gift_extension:%' or `special` like 'loyalty_extension:%') order by `type` asc" );
	if (!$get_payment_types)
	{
		return;
	}

	if (mysqli_num_rows($get_payment_types) <= 0)
	{
		return;
	}

	while ($paytype_info = mysqli_fetch_assoc($get_payment_types))
	{
		$force_mod_singles[] = array($paytype_info['type'], $paytype_info['special']);
	}
}

function draw_copy_move_item_category_script()
{
	global $data_name;

	$dbapi = ConnectionHub::getConn('rest')->getDBAPI();
	$set_groupid = (isset($_GET['set_groupid'])) ? '&set_groupid='.$_GET['set_groupid'] : '';
	$copy_move_copy_default = '';
	$copy_move_move_default = 'SELECTED';


	// load defaults from the database
	$a_default_wherevars = array('setting'=>'edit_menu_copy_move_default', 'type'=>'location_config_setting');
	$a_method_wherevars = array('setting'=>'edit_menu_copy_move_method', 'type'=>'location_config_setting');
	$a_append_text_wherevars = array('setting'=>'edit_menu_copy_move_append_text', 'type'=>'location_config_setting');
	$a_copy_move_default_rows = $dbapi->getAllInTable('config', $a_default_wherevars, FALSE);
	$a_copy_move_method_rows = $dbapi->getAllInTable('config', $a_method_wherevars, FALSE);
	$a_copy_move_append_text_rows = $dbapi->getAllInTable('config', $a_append_text_wherevars, FALSE);

	// get and set the copy_move default
	if (count($a_copy_move_default_rows) !== 0 && $a_copy_move_default_rows[0]['value'] == 'copy') {
		$copy_move_copy_default = 'SELECTED';
		$copy_move_move_default = '';
	}
	if (isset($_POST['action_type'])) {
		if ($_POST['action_type'] == 'copy') { $copy_move_copy_default = 'SELECTED'; $copy_move_move_default = ''; }
		if ($_POST['action_type'] == 'move') { $copy_move_copy_default = ''; $copy_move_move_default = 'SELECTED'; }
	}
	$a_update_vars = array_merge($a_default_wherevars, array('value'=>(($copy_move_copy_default == '') ? 'move' : 'copy')));
	$dbapi->insertOrUpdate('poslavu_'.$data_name.'_db', 'config', $a_update_vars, $a_default_wherevars);

	// get and set the copy method
	$copy_method = count($a_copy_move_method_rows) > 0 ? $a_copy_move_method_rows[0]['value'] : 'change_name';
	if (isset($_POST['copy_method']) && $_POST['copy_method'] !== $copy_method)
		$dbapi->insertOrUpdate('poslavu_'.$data_name.'_db', 'config', array_merge(array('value'=>$copy_method), $a_method_wherevars), $a_method_wherevars);

		// get and set the copy append text
		$copy_append_text = count($a_copy_move_append_text_rows) > 0 ? $a_copy_move_append_text_rows[0]['value'] : 'Copy';
		if (isset($_POST['copy_append_text']) && $_POST['copy_append_text'] !== $copy_append_text)
			$dbapi->insertOrUpdate('poslavu_'.$data_name.'_db', 'config', array_merge(array('value'=>$copy_append_text), $a_append_text_wherevars), $a_append_text_wherevars);

			return "
	<style type='text/css'>
		div .loading_image {
			background-image: url('images/loading_60x.gif');
		}
	</style>
	<script type='text/javascript' src='//ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js'> </script>
	<script type='text/javascript'>

		function get_menu_groups() {
			var menu_groups = {};
			var jselect = $(\"#menu_group_selector\");
			var options = jselect.children(\"option\");
			var i = 1;

			$.each(options, function(k,v) {
				var joption = $(v);
				var id = joption.val();
				var name = joption.text();
				if (id !== \"edit_menu_group\") {
					menu_groups[i] = { name: name, id: id };
					i++;
				}
			});

			return menu_groups;
		}

		function get_menu_group() {
			var jselect = $(\"#menu_group_selector\");
			var first = jselect.children('option')[0];
			var jselected = jselect.children('option[selected]');
			if (jselected.length > 0)
				return { id: jselected.val(), name: jselected.text() };
			else
				return { id: $(first).val(), name: $(first).text() };
		}

		function load_categories(menu_group_id, data_view_id) {
			menu_group_id = parseInt(menu_group_id);

			// delete the previous iframe
			var jiframe = $(\"#load_categories_iframe\");
			if (jiframe.length > 0) {
				jiframe.parent().remove();
				$(\"#load_categories_blackout\").remove();
			}

			var jdiv = $(\"#data_view_object_new_category_id\");
			var blackbox_left = jdiv.offset().left;
			var blackbox_top = jdiv.position().top;
			var loading_left = jdiv.width()/2-30;
			var loading_top = jdiv.height()/2-30;

			jdiv.html(\"\");
			jdiv.append(\"<div style='display:none;'><iframe id='load_categories_iframe' src='index.php?widget=edit_menu&set_groupid=\"+menu_group_id+\"' /></div>\");
			jdiv.parent().append(\"<div style='position:absolute; left:\"+blackbox_left+\"px; top:\"+blackbox_top+\"px; width:\"+jdiv.width()+\"px; height:\"+jdiv.height()+\"px; background-color:rgba(0,0,0,0);' id='load_categories_blackout'><div style='position:relative; left:\"+loading_left+\"px; top:\"+loading_top+\"px; width:60px; height:60px;' class='loading_image'>&nbsp;</div></div>\");
		}

		function load_categories_finished(category_names, category_ids) {
			var other_cat_count = 0;
			$.each(category_names, function(k,v) {
				other_cat_count++;
			});

			var jparent = $(\"#data_view_object_new_category_id\").parent();
			jparent.html(\"\");
			jparent.append(draw_data_view('new_category_id', 5, category_names, category_ids));
		}

		function draw_copy_move_cat(category_index) {
			draw_copy_move_item(category_index, 0);
		}

		function draw_copy_move_item(category_index, item_index) {
			var action = (item_index == 0) ? 'copy_move_category' : 'copy_move_item';
			var cat_ids = get_category_ids();
			var ids = get_item_category_id(category_index, item_index);
			var cat_names = get_category_names(cat_ids);
			var item_name = get_item_name(category_index, item_index);
			var other_names = get_category_names(cat_ids);
			var other_ids = get_category_ids();
			var menu_groups = get_menu_groups();
			var menu_group = get_menu_group();
			delete other_names[category_index];
			delete other_ids[category_index];

			var menu_group_names = {};
			var menu_group_ids = {};
			var menu_group_count = 0;
			$.each(menu_groups, function(k,v) {
				menu_group_names[k] = v.name;
				menu_group_ids[k] = v.id;
				menu_group_count++;
			});

			var copy_source_name = (action == 'copy_move_item') ? cat_names[category_index] : menu_group.name;
			var copy_source_kind = (action == 'copy_move_item') ? 'item' : 'category';
			var copy_dest_kind = (action == 'copy_move_item') ? 'category' : 'group';
			var choose_category_style = (action == 'copy_move_item') ? '' : 'display:none;';
			var item_display_name = (action == 'copy_move_item') ? item_name : cat_names[category_index];
			window.item_display_name = item_display_name;

			show_info('".str_replace(chr(13),"",str_replace("\n", "", "
			<form method=\"POST\" action=\"index.php?mode=".$_GET["mode"]."$set_groupid&action='+action+'\" id=\"copy_move_form\">
					<input type=\"hidden\" name=\"old_item_id\" value=\"'+ids.item+'\" />
					<input type=\"hidden\" name=\"old_cat_id\" value=\"'+ids.category+'\" />
					<table style=\"width: 555px; max-height: 448px; color: white; text-align: center; overflow-y: auto\" cellpadding=\"5px\">
					<thead>
					<tr>
					<th colspan=\"1000\">Copy / Move: '+item_display_name+' from '+copy_source_name+' to another menu '+copy_dest_kind+'</th>
					</tr>
					</thead>
					<tbody>
					<tr>
					<td style=\"text-align:right;\">Action</td>
					<td style=\"text-align:left;\"><select name=\"action_type\" id=\"copy_move_select\" onchange=\"show_hide_copy_options(this.value);\"><option value=\"move\">Move</option><option value=\"copy\">Copy</option></select></td>
					</tr>
					<tr id=\"copy_options\" style=\"display:none;\">
					<td style=\"text-align:right;\">&nbsp;</td>
					<td style=\"text-align:left;\">
					<table>
					<tr><td><input type=\"radio\" name=\"copy_method\" value=\"change_name\" onchange=\"copy_method_changed(this.value);\" /></td><td>Copy, Change Name</td></tr>
					<tr><td></td><td style=\"width:200px; min-width:200px; max-width:200px; overflow-x:visible;\"><div id=\"copy_append_text_chooser\" style=\"display:none; white-space:nowrap;\">
					<font id=\"copy_append_text_example\">Eg: '+item_display_name+', '+item_display_name+' '+window.copy_append_text+'</font><br />
					'+item_display_name+' <input type=\"textbox\" size=\"20\" placeholder=\"Copy\" name=\"copy_append_text\" '+(window.copy_append_text == 'Copy' ? '' : 'value=\"'+window.copy_append_text+'\"')+' onkeydown=\"$(this).change();\" onkeyup=\"$(this).change();\" onchange=\"set_copy_append_text(this.value);\" />
					</div></td></tr>
					<tr><td><input type=\"radio\" name=\"copy_method\" value=\"exact\" onchange=\"copy_method_changed(this.value);\" /></td><td>Copy, Same Name</td></tr>
					<tr><td></td><td style=\"width:200px; min-width:200px; max-width:200px; overflow-x:visible;\"><div id=\"copy_exact_description\" style=\"display:none; white-space:nowrap;\">
					The '+copy_source_kind+' name and all settings will be duplicated.
					</div></div></td></tr>
					</table>
					</td>
					</tr>
					<tr>
					<td style=\"text-align:right;\">Menu Group</td>
					<td style=\"text-align:left;\">'+draw_data_view('new_menu_group_id', 5, menu_group_names, menu_group_ids, menu_group.id, 'load_categories')+'</td>
					</tr>
					<tr style=\"'+choose_category_style+'\">
					<td style=\"text-align:right;\">Category</td>
					<td style=\"text-align:left;\">'+draw_data_view('new_category_id', 5, cat_names, cat_ids, ids.category)+'</td>
					</tr>
					<tr>
					<td colspan=\"1000\"><input type=\"submit\" value=\"Submit\" /></td>
					</tr>
					<tr>
					<td colspan=\"1000\"><font style=\"color:red;\">Warning</font>: You will lose unsaved changes to your menu.</td>
					</tr>
					</tbody>
					</table>
					</form>"))."');

			$(\"#copy_move_select\").val(window.copy_move_default);
			$(\"#copy_move_select\").change();
			eval('set_'+window.copy_move_default+'_defaults();');

			$(\"#copy_move_form\").submit(function(){
				var catId = $(\"#data_view_object_new_category_id_data_view_value\").val();
				if (catId == 'false') {
					alert('Please Select Menu Category to move the Menu Item.');
					return false;
				}
			  });
		}

		function set_copy_defaults() {
			$(\"#copy_options\").find(\"input[type=radio]\").removeAttr(\"checked\");
			$(\"#copy_move_form\").find(\"input[type=submit]\").attr(\"disabled\", true);
			$('#copy_append_text_chooser').hide();
			$('#copy_exact_description').hide();
		}

		function set_move_defaults() {
			window.copy_method = '';
			$(\"#copy_move_form\").find(\"input[type=submit]\").removeAttr(\"disabled\");
		}

		function show_hide_copy_options(s_copy_move) {
			window.copy_move_default = s_copy_move;
			if (s_copy_move == 'copy') {
				$('#copy_options').show();
				set_copy_defaults();
			} else if (s_copy_move == 'move') {
				$('#copy_options').hide();
				set_move_defaults();
			}
		}

		function copy_method_changed(method) {
			window.copy_method = method;
			if (method == 'exact') {
				$('#copy_append_text_chooser').hide(200);
				$('#copy_exact_description').show(200);
			} else if (method == 'change_name') {
				$('#copy_append_text_chooser').show(200);
				$('#copy_exact_description').hide(200);
			}
			$(\"#copy_move_form\").find(\"input[type=submit]\").removeAttr(\"disabled\");
		}

		function set_copy_append_text(text) {
			if (text == '')
				text = 'Copy';
			text = $.trim(text);
			window.copy_append_text = text;
			$('#copy_append_text_example').html('Eg: '+window.item_display_name+' -> '+window.item_display_name+' '+text);
		}

		document.menuLoaded = function(category_names, category_ids) {
			load_categories_finished(category_names, category_ids);
		}

		if (parent.document && parent.document.menuLoaded && parent.document != document) {
			parent.document.menuLoaded(get_category_names(), get_category_ids());
		}
		window.copy_move_default = '".($copy_move_copy_default == '' ? 'move' : 'copy')."';
		window.copy_method = '';
		set_copy_append_text('$copy_append_text');
		$('#copy_move_select').val(('$copy_move_copy_default' == '') ? 'move' : 'copy');
		$('#copy_move_select').change();

		window.thingcount = 0;
		function remove_item_cat_hovertexts(){
		$('.copy_item_cat_hovertext').remove();
}
$('.copy_cat_div').mouseover(function() {
remove_item_cat_hovertexts();
var jitem = $(this);
jitem.parent().append('<div class=\"copy_item_cat_hovertext thing_'+thingcount+'\" style=\"z-index:1000;position: absolute; top: '+(parseInt(jitem.position().top)-8)+'px; left: '+(parseInt(jitem.position().left)+24)+'px; background-color: white; border: 2px solid white; border-radius: 3px; display: none;\" onmouseout=\"remove_item_cat_hovertexts();\">Copy / Move Category</div>');
setTimeout('$(\".thing_'+thingcount+'\").show();', 1000);
thingcount++;
});
$('.copy_cat_div').mouseout(function() {
remove_item_cat_hovertexts();
});
$('.copy_item_div').mouseover(function() {
remove_item_cat_hovertexts();
var jitem = $(this);
jitem.parent().append('<div class=\"copy_item_cat_hovertext thing_'+thingcount+'\" style=\"position: absolute; top: '+(parseInt(jitem.position().top)-8)+'px; left: '+(parseInt(jitem.position().left)+24)+'px; background-color: white; border: 2px solid white; border-radius: 3px; display: none;\" onmouseout=\"remove_item_cat_hovertexts();\">Copy / Move Item</div>');
setTimeout('$(\".thing_'+thingcount+'\").show();', 1000);
thingcount++;
});
$('.copy_item_div').mouseout(function() {
remove_item_cat_hovertexts();
});
</script>";
}

// copies or moves an item or category
// @global $data_name: the current dataname of the restaurant
// @global $location_info: the current location_info of the restaurant
// @global $locationid: the current locationid of the restaurant
// @$_POST['new_menu_group_id']: the destination menu group (applicable only to categories)
// @$_POST['new_category_id']: the destination menu category (applicable only to items)
// @$_POST['old_item_id']: the source menu item (applicable only to items)
// @$_POST['old_cat_id']: the source menu category (applicable only to categories)
// @$_POST['action_type']: 'copy' or 'move'
function copy_move_item_category() {
	$dbapi = ConnectionHub::getConn('rest')->getDBAPI();
	global $data_name;
	global $location_info;
	global $locationid;

	$new_menu_group_id = postvar('new_menu_group_id');
	$new_category_id = postvar('new_category_id');
	$old_item_id = postvar('old_item_id');
	$old_cat_id = postvar('old_cat_id');
	$action_type = postvar('action_type');
	$copy_method = postvar('copy_method');
	$copy_append_text = postvar('copy_append_text');

	if ($copy_append_text === '') $copy_append_text = 'Copy';
		
	if ($new_category_id === FALSE || $new_menu_group_id === FALSE || $old_item_id === FALSE || $action_type === FALSE)
		return;

		// try to update
		$b_updated = FALSE;

		// copy/move the menu item
		if ($_GET['action'] == 'copy_move_item') {

			// choose whether to copy/move the menu item
			// copies/moves between menu categories
			switch($action_type) {
				case 'copy':

					copy_menu_item_to_another_category($data_name, $old_item_id, $new_category_id, $copy_method, $copy_append_text);
					break;
				case 'move':

					// update the row's category
					$s_query_string = "UPDATE `[database]`.`menu_items` SET `category_id`='[category]' WHERE `id`='[id]'";
					$a_query_vars = array('database'=>'poslavu_'.$data_name.'_db', 'category'=>$new_category_id, 'id'=>$old_item_id);
					lavu_connect_dn($data_name,'poslavu_'.$data_name.'_db');
					lavu_query($s_query_string, $a_query_vars);
					break;
			}

			// copy/move the menu category
		} else if ($_GET['action'] == 'copy_move_category') {

			// choose whether to copy/move the menu category
			// copies/moves between menu groups
			switch($action_type) {
				case 'copy':

					// duplicate the row
					$a_old_row = NULL;
					$new_cat_id = $dbapi->duplicateRow('poslavu_'.$data_name.'_db', 'menu_categories', $old_cat_id, $a_old_row);

					// update the row's group
					if ($new_cat_id !== 0) {
						$s_name = ($copy_method == 'exact') ? $a_old_row['name'] : $a_old_row['name'].' '.$copy_append_text;
						$s_database = 'poslavu_'.$data_name.'_db';
						$s_query_string = "UPDATE `[database]`.`menu_categories` SET `group_id`='[group]',`name`='[name]' WHERE `id`='[id]'";
						$a_query_vars = array('database'=>$s_database, 'group'=>$new_menu_group_id, 'id'=>$new_cat_id, 'name'=>$s_name);
						lavu_connect_dn($data_name,'poslavu_'.$data_name.'_db');
						lavu_query($s_query_string, $a_query_vars);
						if (ConnectionHub::getConn('rest')->affectedRows() == 0) {
							lavu_query("DELETE FROM `[database]`.`menu_categories` WHERE `id`='[id]'", array('database'=>$s_database, 'id'=>$new_cat_id));
						} else {

							// duplicate all of the associated menu items
							$a_categories = $dbapi->getAllInTable('menu_items', array('category_id'=>$old_cat_id, '_deleted'=>'0'), TRUE, array('selectclause'=>'`id`', 'databasename'=>$s_database));
							foreach($a_categories as $a_category)
								copy_menu_item_to_another_category($data_name, $a_category['id'], $new_cat_id, $copy_method, $copy_append_text);
						}
					}
					break;
				case 'move':

					// update the row's group
					$s_query_string = "UPDATE `[database]`.`menu_categories` SET `group_id`='[group]' WHERE `id`='[id]'";
					$a_query_vars = array('database'=>'poslavu_'.$data_name.'_db', 'group'=>$new_menu_group_id, 'id'=>$old_cat_id);
					lavu_connect_dn($data_name,'poslavu_'.$data_name.'_db');
					lavu_query($s_query_string, $a_query_vars);
					break;
			}
		}

		if ($b_updated) {
			// set the copy/move default
			$a_where_vars = array('setting'=>'edit_menu_copy_move_default', 'type'=>'location_config_setting');
			$a_update_vars = array_merge($a_where_vars, array('value'=>(($action_type == 'move') ? 'move' : 'copy')));
			ConnectionHub::getConn('rest')->getDBAPI()->insertOrUpdate('poslavu_'.$data_name.'_db', 'config', $a_update_vars, $a_where_vars);

			// LOCAL SYNC RECORD
			schedule_remote_to_local_sync_if_lls($location_info, $locationid, "table updated", "menu_items");
		}
}

// copies a menu item from one category to another category
// @$data_name: data_name this menu item belongs to
// @$old_item_id: database id of the menu_item being copied
// @$new_category_id: id of the category to copy to
// @return: TRUE on success, FALSE on failure
function copy_menu_item_to_another_category($data_name, $old_item_id, $new_category_id, $copy_method, $copy_append_text) {

	$dbapi = ConnectionHub::getConn('rest')->getDBAPI();
	$s_database = 'poslavu_'.$data_name.'_db';

	// check that the category exists
	$a_categories = $dbapi->getAllInTable('menu_categories', array('id'=>$new_category_id), TRUE, array('selectclause'=>'`id`', 'databasename'=>$s_database));

	if (count($a_categories) == 0)
		return FALSE;

		// duplicate the row
		$a_old_row = NULL;
		$new_item_id = $dbapi->duplicateRow('poslavu_'.$data_name.'_db', 'menu_items', $old_item_id, $a_old_row);

		// update the row's category
		if ($new_item_id !== 0) {
			
			copyComboToNewItem($old_item_id,$new_item_id);
			copyIngredientsToNewItem($old_item_id, $new_item_id);
			copyNutritionInfoToNewItem($old_item_id, $new_item_id);
			copyDietaryInfoToNewItem($old_item_id, $new_item_id);
			
			$s_name = ($copy_method == 'exact') ? $a_old_row['name'] : $a_old_row['name'].' '.$copy_append_text;
			$s_query_string = "UPDATE `[database]`.`menu_items` SET `category_id`='[category]',`name`='[name]' WHERE `id`='[id]'";
			$a_query_vars = array('database'=>$s_database, 'category'=>$new_category_id, 'id'=>$new_item_id, 'name'=>$s_name);
			lavu_connect_dn($data_name,'poslavu_'.$data_name.'_db');
			lavu_query($s_query_string, $a_query_vars);
			if (ConnectionHub::getConn('rest')->affectedRows() == 0) {
				lavu_query("DELETE FROM `[database]`.`menu_items` WHERE `id`='[id]'", array('database'=>$s_database, 'id'=>$new_item_id));
				return FALSE;
			}
		}

		return TRUE;
}

function copyComboToNewItem($old_item_id,$new_item_id){
	if($new_item_id!==0)
	{
		$ing_query = lavu_query("SELECT * FROM  `combo_items`  WHERE `combo_menu_id`= '$old_item_id'");
		
		if(mysqli_num_rows($ing_query)>0)
		{
			$idArr = array();
			while ( $ing_read = mysqli_fetch_assoc ( $ing_query ) ) 
			{				
				if($ing_read['parent_id']>0)
				{
					if(array_key_exists($ing_read['parent_id'],$idArr))
					{
						$ing_read['parent_id'] = $idArr[$ing_read['parent_id']];
					}
				}
				
				lavu_query("INSERT INTO `combo_items` (`parent_id`,`custom_category_name`,`add_option`,`add_sub_option`,`combo_menu_id`,`menu_item_id`,`combo_header`,`qty`,`price_adjustment`,`_deleted`,`header_qty`) values ('".$ing_read['parent_id']."','".$ing_read['custom_category_name']."','".$ing_read['add_option']."','".$ing_read['add_sub_option']."','".$new_item_id."','".$ing_read['menu_item_id']."','".$ing_read['combo_header']."','".$ing_read['qty']."','".$ing_read['price_adjustment']."','".$ing_read['_deleted']."','".$ing_read['header_qty']."')");
				$idArr[$ing_read['id']] = lavu_insert_id();
				
			}
		}
	}
}

function copyIngredientsToNewItem($old_item_id, $new_item_id){
	if($new_item_id!==0)
	{
		$ing_query = lavu_query("SELECT * FROM  `menuitem_86`  WHERE `menuItemID`= '$old_item_id' and `_deleted` = '0'");

		if(mysqli_num_rows($ing_query)>0)
		{
			while ( $ing_read = mysqli_fetch_assoc ( $ing_query ) )
			{
				lavu_query("INSERT INTO `menuitem_86` (`menuItemID`,`inventoryItemID`,`quantityUsed`,`criticalItem`,`unitID`,`reservation`,`_deleted`) values ('".$new_item_id."','".$ing_read['inventoryItemID']."','".$ing_read['quantityUsed']."','".$ing_read['criticalItem']."','".$ing_read['unitID']."','0','".$ing_read['_deleted']."')");

			}
		}
	}
}

function copyNutritionInfoToNewItem($old_item_id, $new_item_id) {
	if ($new_item_id!==0) {
		$nut_info_link_query = lavu_query("SELECT * FROM  `nutrition_info_link`  WHERE `source_table_id`= '$old_item_id' and `source_table` = 'menu_items'");

		if (mysqli_num_rows($nut_info_link_query)>0) {
			while ($nut_info_read = mysqli_fetch_assoc($nut_info_link_query)) {
				lavu_query("INSERT INTO `nutrition_info_link` (`source_table`,`source_table_id`,`_created_time`,`_created_user`) values ('".$nut_info_read['source_table']."','".$new_item_id."','".$nut_info_read['_created_time']."','".$nut_info_read['_created_user']."')");
				$nut_insert_id = lavu_insert_id();

				$nut_query = lavu_query("SELECT * FROM `nutrition_info` WHERE `nutrition_info_link_id` = '".$nut_info_read['id']."'");
				if (mysqli_num_rows($nut_query)>0) {
					while ($nut_read = mysqli_fetch_assoc($nut_query)) {
						lavu_query("INSERT INTO `nutrition_info` (`nutrition_info_link_id`, `standard_nutrition_type_id`, `value`, `units`, `is_custom`, `sort_order`, `_deleted`, `_created_time`, `_created_user`, `_updated_time`, `_updated_user`) VALUES ('".$nut_insert_id."', '".$nut_read['standard_nutrition_type_id']."', '".$nut_read['value']."', '".$nut_read['units']."', '".$nut_read['is_custom']."', '".$nut_read['sort_order']."', '".$nut_read['_deleted']."', '".$nut_read['_created_time']."', '".$nut_read['_created_user']."', '".$nut_read['_updated_time']."', '".$nut_read['_updated_user']."')");
					}
				}
			}
		}
	}
}

function copyDietaryInfoToNewItem($old_item_id, $new_item_id) {
	if ($new_item_id!==0) {
		$dietary_query = lavu_query("SELECT * FROM `menu_item_meta_data_link` WHERE `menu_item_id` = '$old_item_id'");
		if (mysqli_num_rows($dietary_query)>0) {
			while ($dietary_read = mysqli_fetch_assoc($dietary_query)) {
				lavu_query("INSERT INTO `menu_item_meta_data_link` (`menu_item_id`, `meta_data_id`, `value`, `_deleted`) VALUES ('".$new_item_id."', '".$dietary_read['meta_data_id']."', '".$dietary_read['value']."', '".$dietary_read['_deleted']."')");
			}
		}
	}
}

function commboInsert($main_id, $combo_data, $cat_delete) {
	$test=json_decode($combo_data, 1);
	$count_test=count($test);
	$update= lavu_query("update combo_items set _deleted=1 where combo_menu_id={$main_id}");
	$update_combo = mysqli_fetch_assoc($update);
	if ($cat_delete == '0') {
		if ($test[import_price]=='on') { 
			$check=1; 
		} else { 
			$check=0;
		}
		$update_import_price= lavu_query("update menu_items set import_menu_price={$check} where id={$main_id}");
		$update_import_price_combo = mysqli_fetch_assoc($update_import_price);
		$index=1;
		foreach ($test as $key=>$data) {
			$menu="m_".$index;
			$menu_id=$menu."_selmenu";
			$menu_name=$menu."_seltext";
			$menu_header=$menu."_header";
			$menu_qty=$menu."_qty";
			$menu_hqty=$menu."_hqty";
			$menu_price=$menu."_price";
			$menu_deleted=$menu."_deleted";
	
			if (array_key_exists($menu_id, $test)) {
				//insert function with select function for id
				$id_combo[$menu.'_id']=insertComboItems(0, $test[$menu_name], 0, 0, $main_id, $test[$menu_id], $test[$menu_header], $test[$menu_qty], $test[$menu_price], $test[$menu_deleted], $test[$menu_hqty]);
				if ($id_combo[$menu.'_id'] == 0) { 
					return $id_combo[$menu.'_id']; 
				}
				for($join=1; $join<$count_test; $join++ ) {
					$menu_opt=$menu."_opt_".$join;
					$menu_opt_id=$menu_opt."_selmenu";
					$menu_opt_name=$menu_opt."_seltext";
					$menu_opt_header=$menu_opt."_header";
					$menu_opt_qty=$menu_opt."_qty";
					$menu_opt_hqty=$menu_opt."_hqty";
					$menu_opt_price=$menu_opt."_price";
					$menu_opt_deleted=$menu_opt."_deleted";
					$prarent_id=$id_combo[$menu.'_id'];
	
					if (array_key_exists($menu_opt_id, $test)) {
						//insert function with select function for id
						$id_combo[$menu_opt.'_id']=insertComboItems($prarent_id, $test[$menu_opt_name], 1, 0, $main_id, $test[$menu_opt_id], $test[$menu_opt_header], $test[$menu_opt_qty], $test[$menu_opt_price], $test[$menu_opt_deleted], $test[$menu_opt_hqty]);
						if ($id_combo[$menu_opt.'_id'] == 0) { 
							return $id_combo[$menu_opt.'_id']; 
						}
						for($kcount=1; $kcount<$count_test; $kcount++ ) {
							$menu_opt_subopt=$menu_opt."_subopt_".$kcount;
							$menu_opt_subopt_id=$menu_opt_subopt."_selmenu";
							$menu_opt_subopt_name=$menu_opt_subopt."_seltext";
							$menu_opt_subopt_header=$menu_opt_subopt."_header";
							$menu_opt_subopt_qty=$menu_opt_subopt."_qty";
							$menu_opt_subopt_price=$menu_opt_subopt."_price";
							$menu_opt_subopt_deleted=$menu_opt_subopt."_deleted";
							$prarent_id=$id_combo[$menu_opt.'_id'];
							if (array_key_exists($menu_opt_subopt_id, $test)) {
								//insert function with select function for id
								$id_combo[$menu_opt_subopt.'_id']=insertComboItems($prarent_id, $test[$menu_opt_subopt_name], 0, 1, $main_id, $test[$menu_opt_subopt_id], $test[$menu_opt_subopt_header], $test[$menu_opt_subopt_qty], $test[$menu_opt_subopt_price], $test[$menu_opt_subopt_deleted]);
								if ($id_combo[$menu_opt_subopt.'_id'] == 0) { 
									return $id_combo[$menu_opt_subopt.'_id']; 
								}
							} else {
								break; 
							}
						}
					} else { 
						break; 
					}
				}
				for($join=1; $join<$count_test; $join++ ) {
					$menu_subopt=$menu."_subopt_".$join;
					$menu_subopt_id=$menu_subopt."_selmenu";
					$menu_subopt_name=$menu_subopt."_seltext";
					$menu_subopt_header=$menu_subopt."_header";
					$menu_subopt_qty=$menu_subopt."_qty";
					$menu_subopt_price=$menu_subopt."_price";
					$menu_subopt_deleted=$menu_subopt."_deleted";
					$prarent_id=$id_combo[$menu.'_id'];
					if (array_key_exists($menu_subopt_id,$test)) {
						//insert function with select function for id
						$id_combo[$menu_subopt.'_id']=insertComboItems($prarent_id, $test[$menu_subopt_name], 0, 1, $main_id, $test[$menu_subopt_id], $test[$menu_subopt_header], $test[$menu_subopt_qty], $test[$menu_subopt_price], $test[$menu_subopt_deleted]);
						if ($id_combo[$menu_subopt.'_id'] == 0) { 
							return $id_combo[$menu_subopt.'_id']; 
						}
					} else { 
						break; 
					}
				}
			} else { 
				break; 
			}
			$index++;
		}
	}
	return $update_combo;
}

function insertComboItems($parent_id, $name, $add_option, $add_sub_option, $main_id, $menu_item_id, $combo_header='0', $qty, $price_adjustment='0', $menu_deleted, $hqty='0') {
	global $rdb,$temp_id;
	$dietary_query = lavu_query("SELECT * FROM `menu_items` WHERE `id` = '$menu_item_id' and `_deleted` = 0 ");
	if (mysqli_num_rows($dietary_query)>0) {
		if($combo_header == 'on'){ $combo_header= '1'; $menu_item_id=''; } else{ $combo_header=0; }
		$price_adjustment = formatValueForMonetaryDisplay($price_adjustment);
		$return=lavu_query("insert into `combo_items` (`parent_id`, `custom_category_name`, `add_option`, `add_sub_option`, `combo_menu_id`, `menu_item_id`, `combo_header`, `qty`, `price_adjustment`, `_deleted`, `header_qty`) values ('$parent_id', '[1]', '$add_option', '$add_sub_option', '$main_id', '$menu_item_id', '$combo_header', '$qty', '$price_adjustment', '$menu_deleted', '$hqty')", $name);
		$rdata=lavu_insert_id();
	} else {
		lavu_query("Update `combo_items` set `_deleted` = 1 WHERE `combo_menu_id` = '$main_id'");
		$rdata = 0;
	}
	return $rdata;
}

/**
 * This function is used to check all menu item price validation.
 * @param array $formData
 * it returns invalid message with menu item names.
 */
function checkMenuPriceValidation($formData) {
    $resultInfo = 'success';
	$monitarySymbol = $formData['monitarySymbol'];
	$decimals = $formData['decimals'];
	$decimalChar = $formData['decimalChar'];
	$leftRight = $formData['leftRight'];
	$itemInfo =$formData['formReqInfo'];

	if (!empty($itemInfo)) {
		$itemDetails =explode('&&', $itemInfo);
		$itemValidation = array();
		foreach($itemDetails as $menuitem) {
			$itemValidationData = checkPriceValidation($menuitem, $monitarySymbol, $decimals, $decimalChar, $leftRight);

			if($itemValidationData != '') {
				$itemValidation[] = $itemValidationData;
			}
		}

		if(!empty($itemValidation)) {
			$validation = implode('<br>',$itemValidation);
			$resultInfo = 'fail*^#'.$validation;
		}
	}
	return $resultInfo;
}

/**
 * This function is used to to check price validation.
 * @param string $menuitem
 * @param string $monitarySymbol
 * @param string $decimals
 * @param string $decimalChar
 * @param string $leftRight
 * @return string validation message
 */
function checkPriceValidation($menuitem, $monitarySymbol, $decimals, $decimalChar, $leftRight) {
	$resultInfo = '';
	$menuitemInfo =explode('^^!', $menuitem);
	$itemName = $menuitemInfo[0];
	$itemPrice = $menuitemInfo[1];
	$htmlEntitiesMonetary = htmlentities($itemPrice);
	$itemPriceEncode = preg_replace(($leftRight == 'left') ? "/&lrm;/" : "/&rlm;/", "", $htmlEntitiesMonetary);
	$itemPrice = html_entity_decode($itemPriceEncode);
	$maxDecimals = 16 - $decimals;
	$priceMonitary = str_replace($monitarySymbol, '', $itemPrice);
	$priceDecimalChar = explode($decimalChar, $priceMonitary);
	$decimalSeparator = ($decimalChar == '.') ? ',' : '.';
	$beforeNumber = str_replace($decimalSeparator, '', $priceDecimalChar[0]);
	$afterNumber = ($priceDecimalChar[1] != '') ? $priceDecimalChar[1] : '';
	$beforeDecimal = (strlen($beforeNumber)) ? strlen($beforeNumber)  : 0;

	if ($beforeDecimal > $maxDecimals) {
		$resultInfo = '<br>The maximum number of entered digits up to 16 has been reached for "'.$itemName.'"';
	}

	if ((!is_numeric($beforeNumber) && $beforeNumber != '') || (!is_numeric($afterNumber) && $afterNumber != '')) {
		$resultInfo .= '<br>Please enter valid price for "'.$itemName.'"';
	}

	return $resultInfo;
}

/**
 * This function is used to update unavailable_until field when menu items track86 details are modified from CP.
 *
 * @param string $itemList JSON collection of menu item IDs to update.
 * @param string $dataName Restaurant's data name.
 * @param string $table    DB Table that's going to be updated.
 *
 * @return string Message with status of the operation.
 */
function updateUnavilableInfo($itemList, $dataName, $table) {
	$resultInfo = "fail^&^Some thing went wrong, Please check after some time.";
	$itemList = json_decode($itemList);
	$itemIds = implode("', '", $itemList);
	$query = mlavu_query("SELECT `id` FROM  $dataName.`[1]` WHERE `id` IN('".$itemIds."') AND `_deleted` = '0' AND `unavailable_until` != '0' ", $table);
	if (mysqli_num_rows($query) > 0) {
		$updateStatus = mlavu_query("UPDATE $dataName.`[1]` SET `unavailable_until` = '0' WHERE `id` IN('".$itemIds."') AND `_deleted` = '0' AND `unavailable_until` != '0' ", $table);
		if ($updateStatus) {
			$resultInfo = "success^&^Modified item details are updated successfully.";
		}
	}
	return $resultInfo;
}

?>
