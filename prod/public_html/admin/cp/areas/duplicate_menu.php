<?php
	if($in_lavu)
	{
		$use_mysqli = true;
		
		function dm_mysql_fetch_assoc($res)
		{
			global $use_mysqli;
			if($use_mysqli)
				return mysqli_fetch_assoc($res);
			else
				return mysql_fetch_assoc($res);
		}
		
		function dm_mysql_num_rows($res)
		{
			global $use_mysqli;
			if($use_mysqli)
				return mysqli_num_rows($res);
			else
				return mysql_num_rows($res);
		}
		
		function dm_mysql_real_escape_string($str,$useConn=false)
		{
			global $use_mysqli;
			if($use_mysqli)
			{
				$conn = ConnectionHub::getConn('rest')->getLink('read');
				return mysqli_real_escape_string($conn,$str);
			}
			else
			{
				global $Conn;
				global $mConn;
				if(!$useConn)
				{
					if($mConn) $useConn = $mConn;
					else $useConn = $Conn;
				}
				return mysql_real_escape_string($str,$useConn);
			}
		}
		
		//ini_set("display_errors","1");
		if(isset($_GET['debug_mode']))
			$_SESSION['debug_mode'] = $_GET['debug_mode'];
		function debug_mode_on()
		{
			if(isset($_SESSION['debug_mode']) && $_SESSION['debug_mode']=="on")
				return true;
			else
				return false;
		}
		//--------------------------------------------- This is the start of the multi location selector, pulled from another area and modified - CF ------------------------//
		$showing_multi_report = (isset($_GET['multi']))?$_GET['multi']:"";
		if(isset($_GET['multi']))
		{
			$showing_multi_report = $_GET['multi'];
			$_SESSION['showing_multi_report_' . $data_name] = $showing_multi_report;
		}
		else
		{
			$showing_multi_report = "";
			if(isset($_SESSION['showing_multi_report_' . $data_name]))
				$showing_multi_report = $_SESSION['showing_multi_report_' . $data_name];
		}
		
		$multi = $showing_multi_report;
		$group_assoc = array();
		
		global $allow_multi_location;
		$allow_multi_location = true;
		$selector_str = "";
		$multi_location_ids = "";
		if($allow_multi_location)
		{
			$cr_dataname = $data_name;
			$cr_dataname = substr($cr_dataname,8);
			$cr_dataname = substr($cr_dataname,0,strlen($cr_dataname) - 3);
	
			// get the chain reporting groups
			require_once('/home/poslavu/public_html/admin/sa_cp/manage_chain_functions.php');
			$cr_groups = CHAIN_USER::get_user_reporting_groups();
			$a_chain_locations = getChainLocations($cr_dataname, FALSE, FALSE);

			// check that the currently selected group is one of the groups this user has access to
			// -1 means "all locations for this restaurant"
			if($multi!="")
			{
				if ($showing_multi_report !== FALSE && $showing_multi_report !== '-1') {
					$b_found = FALSE;
					$first_cr_group = NULL;
					foreach($cr_groups as $cr_group) {
						$first_cr_group = $cr_group;
						if ((int)$cr_group['id'] == (int)$showing_multi_report) {
							$b_found = TRUE;
							break;
						}
					}
					if (!$b_found)
						$showing_multi_report = ($first_cr_group !== NULL) ? $first_cr_group['id'] : FALSE;
				}
			}

			$cr_group_name = "";
			if( (count($cr_groups) > 0 || count($a_chain_locations) > 1) )
			{
				// find the group name and associated accounts
				foreach($cr_groups as $cr_group)
				{
					$group_assoc[$cr_group['id']] = $cr_group;
					if((int)$showing_multi_report == (int)$cr_group['id'])
					{
						$cr_group_name = $cr_group['name'];
						$multi_location_ids = $cr_group['contents'];
					}
				}

				// create a getvar string
				$uri = $_SERVER['QUERY_STRING'];
				$uri = str_ireplace(array("&multireport=0","&multireport=".$showing_multi_report), "", $uri);

				// create a dropdown select box for selecting the reporting group
				$selector_str .= "<select onchange='window.location = \"?mode=".$_GET['mode']."&multi=\" + this.value'>";
				$selector_str .= "<option value=''>Showing Current Location</option>";
				/*if (count($a_chain_locations) > 1) {
					$s_selected = ($showing_multi_report == '-1') ? "selected" : "";
					$selector_str .= "<option value='-1' $s_selected>Current Account - All Locations</option>";
				}*/
				if (count($cr_groups) > 0) {
					foreach($cr_groups as $cr_group)
					{
						$groupid = $cr_group['id'];
						$groupname = $cr_group['name'];
						$s_selected = ((int)$groupid == (int)$showing_multi_report) ? "selected" : "";
						$selector_str .= "<option value='$groupid' $s_selected>$groupname</option>";
					}
				}
				$selector_str .= "</select>";

				// create a dropdown with an option to view all locations in one report or
				// in separate reports
				/*if ($b_draw_multi_locations && $showing_multi_report != FALSE && $showing_multi_report !== '-1') {
					$s_group_selected = ($b_separate_locations) ? "" : "selected";
					$s_separate_selected = ($b_separate_locations) ? "selected" : "";
					$sep_uri = str_replace(array('&sep_loc=0','&sep_loc=1'),'',$uri);
					$sep_uri = str_replace(array('sep_loc=0&','sep_loc=1&'),'',$sep_uri);
					$sep_uri = str_replace(array('sep_loc=0','sep_loc=1'),'',$sep_uri);
					$selector_str .= "<select onchange='window.location = \"index.php?$sep_uri&sep_loc=\"+this.value'>
						<option value='0' $s_group_selected>Group Locations</option>
						<option value='1' $s_separate_selected>Separate Locations</option>
					</select>";
				}*/
				
				$current_location = admin_info("companyid");
				$other_loc_list = array();
				if(isset($group_assoc[$multi]))
				{
					//echo $group_assoc[$multi]['name'] . " - " . $group_assoc[$multi]['contents'] . "<br>";
					$group_locs = $group_assoc[$multi]['contents'];
					for($i=0; $i<count($group_locs); $i++) 
					{
						if($current_location != $group_locs[$i])
							$other_loc_list[] = $group_locs[$i];
					}
				}
				
				//--------------------------------------------------- End of Locator Selection Madness -------------------------------------------//
				
				function load_entire_location_menu($rdb)
				{
					$location_info = array();
					$menu_id = 1;
					
					$menu_info = array();
					$category_info = array();
					$items_info = array();
					
					$groups_by_id = array();
					$categories_by_id = array();
					
					$fmod_group_info = array();
					$fmod_list_info = array();
					$fmod_info = array();

					$fmod_group_info_by_id = array();
					$fmod_list_info_by_id = array();
					$fmod_info_by_id = array();
					
					$omod_list_info = array();
					$omod_list_info_by_id = array();
					$omod_info = array();
					$omod_info_by_id = array();
					
					$super_groups_info = array();
					$super_groups_info_by_id = array();
					
					$location_query = mlavu_query("select * from `[1]`.`locations` order by `_disabled` asc,`id` desc limit 1",$rdb);
					while($location_read = dm_mysql_fetch_assoc($location_query))
					{
						$location_info = $location_read;
						$menu_id = $location_info['menu_id'];
					}
					
					$super_query = mlavu_query("select * from `[1]`.`super_groups` where `_deleted`!='1' order by `title` asc",$rdb);
					//echo "Debugging: super groups found: " . dm_mysql_num_rows($super_query) . "<br>";
					//echo "select * from `$rdb`.`super_groups` where `_deleted`!='1' order by `title` asc<br>";
					while($super_read = dm_mysql_fetch_assoc($super_query))
					{
						$super_group_title = $super_read['title'];
						$super_group_id = $super_read['id'];
						//echo "Debugging: loading super group $super_group_title<br>";
						
						$super_groups_info[$super_group_title] = $super_read;
						$super_groups_info_by_id[$super_group_id] = $super_read;
					}
					
					$group_query = mlavu_query("select * from `[1]`.`menu_groups` where `menu_id`='[2]' and `_deleted`!='1' order by `group_name` asc",$rdb,$menu_id);
					while($group_read = dm_mysql_fetch_assoc($group_query))
					{
						$group_name = $group_read['group_name'];
						$group_id = $group_read['id'];
						
						$menu_info[$group_name] = $group_read;
						$groups_by_id[$group_id] = $group_read;
						$category_info[$group_name] = array();
						$items_info[$group_name] = array();
						
						$category_query = mlavu_query("select * from `[1]`.`menu_categories` where `group_id`='[2]' and `menu_id`='[3]' and `_deleted`!='1' order by `name` asc",$rdb,$group_id,$menu_id);
						while($category_read = dm_mysql_fetch_assoc($category_query))
						{
							$category_name = $category_read['name'];
							$category_id = $category_read['id'];
							
							$category_info[$group_name][$category_name] = $category_read;
							$categories_by_id[$category_id] = $category_read;
							$items_info[$group_name][$category_name] = array();
							
							$item_query = mlavu_query("select * from `[1]`.`menu_items` where `category_id`='[2]' and `menu_id`='[3]' and `_deleted`!='1' order by `name` asc",$rdb,$category_id,$menu_id);
							while($item_read = dm_mysql_fetch_assoc($item_query))
							{
								$item_name = $item_read['name'];
								$item_id = $item_read['id'];
								
								$items_info[$group_name][$category_name][$item_name] = $item_read;
							}
						}			
					}
					
					$fmod_group_query = mlavu_query("select * from `[1]`.`forced_modifier_groups` where `menu_id`='[2]' and `_deleted`!='1' order by `title` asc",$rdb,$menu_id);
					while($fmod_group_read = dm_mysql_fetch_assoc($fmod_group_query))
					{
						$group_name =  $fmod_group_read['title'];
						$group_id = $fmod_group_read['id'];
						
						$fmod_group_info[$group_name] = $fmod_group_read;
						$fmod_group_info_by_id[$group_id] = $fmod_group_read;
					}

					$fmod_list_query = mlavu_query("select * from `[1]`.`forced_modifier_lists` where `menu_id`='[2]' and `_deleted`!='1' order by `title` asc",$rdb,$menu_id);
					while($fmod_list_read = dm_mysql_fetch_assoc($fmod_list_query))
					{
						$list_name =  $fmod_list_read['title'];
						$list_id = $fmod_list_read['id'];
						
						$fmod_list_info[$list_name] = $fmod_list_read;
						$fmod_list_info_by_id[$list_id] = $fmod_list_read;
						$fmod_info[$list_name] = array();
						$fmod_info_by_id[$list_id] = array();
						
						$fmod_query = mlavu_query("select * from `[1]`.`forced_modifiers` where `list_id`='[2]' and `_deleted`!='1' order by `title` asc",$rdb,$list_id);
						while($fmod_read = dm_mysql_fetch_assoc($fmod_query))
						{
							$mod_name = $fmod_read['title'];
							$mod_id = $fmod_read['id'];
							
							$fmod_info[$list_name][$mod_name] = $fmod_read;
							$fmod_info_by_id[$list_id][$mod_id] = $fmod_read;
						}
					}

					$omod_list_query = mlavu_query("select * from `[1]`.`modifier_categories` where `menu_id`='[2]' and `_deleted`!='1' order by `title` asc",$rdb,$menu_id);
					while($omod_list_read = dm_mysql_fetch_assoc($omod_list_query))
					{
						$list_name =  $omod_list_read['title'];
						$list_id = $omod_list_read['id'];
						
						$omod_list_info[$list_name] = $omod_list_read;
						$omod_list_info_by_id[$list_id] = $omod_list_read;
						$omod_info[$list_name] = array();
						$omod_info_by_id[$list_id] = array();
						
						$omod_query = mlavu_query("select * from `[1]`.`modifiers` where `category`='[2]' and `_deleted`!='1' order by `title` asc",$rdb,$list_id);
						while($omod_read = dm_mysql_fetch_assoc($omod_query))
						{
							$mod_name = $omod_read['title'];
							$mod_id = $omod_read['id'];
							
							$omod_info[$list_name][$mod_name] = $omod_read;
							$omod_info_by_id[$list_id][$mod_id] = $omod_read;
						}
					}
					
					return array("super_groups"=>$super_groups_info,"super_groups_by_id"=>$super_groups_info_by_id,"groups"=>$menu_info,"categories"=>$category_info,"items"=>$items_info,"optional_modifier_lists"=>$omod_list_info,"optional_modifier_lists_by_id"=>$omod_list_info_by_id,"optional_modifiers"=>$omod_info,"optional_modifiers_by_id"=>$omod_info_by_id,"forced_modifier_groups"=>$fmod_group_info,"forced_modifier_groups_by_id"=>$fmod_group_info_by_id,"forced_modifier_lists"=>$fmod_list_info,"forced_modifier_lists_by_id"=>$fmod_list_info_by_id,"forced_modifiers"=>$fmod_info,"forced_modifiers_by_id"=>$fmod_info_by_id,"groups_by_id"=>$groups_by_id,"categories_by_id"=>$categories_by_id,"location_info"=>$location_info);
				}
				
				function copy_data_from_record_to_location($tablename,$from_id,$other_db,$other_id,$fields,&$menu_info,&$other_menu)
				{
					global $data_name;
					
					$in_debug_mode = false;//true;
					if(isset($_SESSION['debug_mode']) && $_SESSION['debug_mode']=="on")
						$in_debug_mode = true;
					$show_link_info = false;//true;
					if(is_string($fields)) $fields = translate_field_list_to_array($fields);
					
					$source_query = lavu_query("select * from `[1]` where `id`='[2]'",$tablename,$from_id);
					if(dm_mysql_num_rows($source_query))
					{
						$source_read = dm_mysql_fetch_assoc($source_query);
						$can_update = true;
						
						$update_code = "";
						$insert_code = "";
						$insert_vals = "";
						$select_duplicate_code = "";
						$update_vars = array();
						$update_vars['destdb'] = $other_db;
						$update_vars['tablename'] = $tablename;
						$update_vars['destid'] = $other_id;
						
						for($i=0; $i<count($fields); $i++)
						{
							$fieldname = trim($fields[$i]['name']);
							$fieldtype = $fields[$i]['type'];
							$fieldprops = $fields[$i]['props'];

							if($fieldtype=="autoset")
							{
								$set_field_value = $fieldprops;
							}
							else if($fieldtype=="link_to_table" || $fieldtype=="multi_link_to_table")
							{
								$set_field_value = "";
								$field_multi_delimiter = "";
								if($fieldtype=="multi_link_to_table")
								{
									$field_multi_delimiter = "|";
									$values_from_source_read = explode($field_multi_delimiter,$source_read[$fieldname]);
								}
								else
								{
									$values_from_source_read = array($source_read[$fieldname]);
								}
								
								$trans_info_list = array();
								for($m=0; $m<count($values_from_source_read); $m++)
								{
									$trans_info_list[] = translate_linked_table_value($values_from_source_read[$m],$fieldprops,$menu_info);
								}
								//echo $tablename . " from $from_id - ".$source_read['title']." " . $source_read[$fieldname] . " (".count($values_from_source_read).")<br>";
								
								if(count($trans_info_list))
								{
									for($m=0; $m<count($trans_info_list); $m++)
									{
										if($can_update)
										{
											$trans_info = $trans_info_list[$m];
											
											$trans_status = $trans_info['status'];
											$trans_value = $trans_info['value'];
											$trans_table = $trans_info['match_table'];
											$trans_ldchr = $trans_info['match_id'];
											$trans_field = $trans_info['match_field'];
											$trans_table_parents = array();
											
											if($trans_table=="categories")
											{
												$link_table_primary_id = $source_read[$fieldname];
												$link_table_primary_group_id = $menu_info['categories_by_id'][$link_table_primary_id]['group_id'];
												$link_table_primary_group_name = $menu_info['groups_by_id'][$link_table_primary_group_id]['group_name'];
												$trans_table_parents = array($link_table_primary_group_name);
											}
											else if($trans_table=="items")
											{
												$link_table_primary_id = $source_read[$fieldname];
												$link_table_primary_category_id = $menu_info['items_by_id'][$link_table_primary_id]['category_id'];
												$link_table_primary_category_name = $menu_info['categories_by_id'][$link_table_primary_category_id]['name'];
												$link_table_primary_group_id = $menu_info['categories_by_id'][$link_table_primary_category_id]['group_id'];
												$link_table_primary_group_name = $menu_info['groups_by_id'][$link_table_primary_group_id]['group_name'];
												$trans_table_parents = array($link_table_primary_group_name,$link_table_primary_category_name);
											}
											
											if($show_link_info) { foreach($trans_info as $key => $val) echo "TRANS INFO: $key = $val<br>"; }
											if($trans_status=="ok")
											{
												//echo "Find:<br>trans_table=$trans_table<br>tans_ldchr=$trans_ldchr<br>trans_value=$trans_value<br>";
												$link_info = find_linked_value_in_table($trans_table,$trans_ldchr,$trans_value,$other_menu,$trans_table_parents);
												if($link_info['status']=="ok") 
												{
													if($field_multi_delimiter!="")
													{
														if($set_field_value!="") $set_field_value .= $field_multi_delimiter;
														$set_field_value .= $link_info['setid'];
													}
													else
													{
														$set_field_value = $link_info['setid'];
													}
												}
												else
												{
													if($in_debug_mode) echo "Cannot update field $fieldname because linked value status is ".$link_info['status']."<br>";
													$set_field_value = "";	
													//If linked value cannot be found use blank instead											
													//$can_update = false;
												}
												if($show_link_info) { foreach($link_info as $key => $val) echo "LINK INFO: $key = $val<br>"; }
											}
											else
											{
												$set_field_value = "";	
												//If linked value cannot be found use blank instead											
												//$can_update = false;
											}
										}
									}
								}
								else
								{
									if($in_debug_mode) echo "Cannot update field $fieldname because trans_info count is false<br>";
									$can_update = false;
								}
							}
							else if($fieldtype=="location_info")
							{
								$set_field_value = "";
								if(isset($other_menu['location_info']) && isset($other_menu['location_info'][$fieldprops]))
								{
									$set_field_value = $other_menu['location_info'][$fieldprops];
								}
							}
							else if($fieldtype=="picture")
							{
								$set_field_value = $source_read[$fieldname];
								if($set_field_value!="")
								{
									$other_dataname = $other_db;
									if(substr($other_dataname,0,8)=="poslavu_" && substr($other_dataname,strlen($other_dataname)-3)=="_db")
									{
										$other_dataname = substr($other_dataname,8,strlen($other_dataname)-3-8);
										$pbasedir = "/home/poslavu/public_html/admin/";
										$origin_picture_dir = $pbasedir . "images/".$data_name."/$fieldprops/";
										$dest_picture_dir = $pbasedir . "images/".$other_dataname."/$fieldprops/";
										
										$psubdirs = array("","main/","full/");
										for($n=0; $n<count($psubdirs); $n++)
										{
											$pdir_origin = $origin_picture_dir . $psubdirs[$n] . $set_field_value;
											$pdir_dest = $dest_picture_dir . $psubdirs[$n] . $set_field_value;
											
											//echo "COPYING $pdir_origin TO $pdir_dest<br>";
											copy($pdir_origin,$pdir_dest);
										}
									}
								}
							}
							else
							{
								$set_field_value = $source_read[$fieldname];
							}
							
							$escaped_field = dm_mysql_real_escape_string($fieldname);
							if($insert_code!="") $insert_code .= ",";
							$insert_code .= "`$escaped_field`";
							if($insert_vals!="") $insert_vals .= ",";
							$insert_vals .= "'[$escaped_field]'";
							if($update_code!="") $update_code .= ",";
							$update_code .= "`$escaped_field`='[$escaped_field]'";
							$update_vars[$fieldname] = $set_field_value;
							if($select_duplicate_code!="") $select_duplicate_code .= " and ";
							$select_duplicate_code .= "`$escaped_field`='[$escaped_field]'";
						}
												
						if($can_update)
						{
							if($other_id=="new")
							{
								if($insert_code != "")
								{
									$find_duplicate_query = "select * from `[destdb]`.`[tablename]` where ($select_duplicate_code)";
									$insert_query = "insert into `[destdb]`.`[tablename]` ($insert_code) values ($insert_vals)";
									if($in_debug_mode)
									{
										foreach($update_vars as $ukey => $uval) { $insert_query = str_replace("[$ukey]",$uval,$insert_query); }
										foreach($update_vars as $ukey => $uval) { $find_duplicate_query = str_replace("[$ukey]",$uval,$find_duplicate_query); }
										echo $find_duplicate_query . "<br>";
										echo $insert_query . "<br>";
										
										//mail("corey@lavu.com","Duplicate Query",$find_duplicate_query,"From: dupquery@poslavu.com");
									}
									else
									{
										$found_existing_entry = false;
										$exist_query = mlavu_query($find_duplicate_query,$update_vars);
										while($exist_read = dm_mysql_fetch_assoc($exist_query))
										{
											if(isset($exist_read['_deleted']))
											{
												if($exist_read['_deleted']!="1")
												{
													$found_existing_entry = true;
												}
											}
											else $found_existing_entry = true;
										}
										if(!$found_existing_entry)
										{
											mlavu_query($insert_query,$update_vars);
										}
									}
								}
							}
							else
							{
								if($update_code != "")
								{
									$update_query = "update `[destdb]`.`[tablename]` set $update_code where `id`='[destid]'";
									if($in_debug_mode)
									{
										foreach($update_vars as $ukey => $uval) $update_query = str_replace("[$ukey]",$uval,$update_query);
										echo $update_query . "<br>";
									}
									else
									{
										mlavu_query($update_query,$update_vars);
									}
								}
							}
						}
					}
				}
				
				function translate_field_list_to_array($copy_fields)
				{
					$copy_field_list = array();
					$copy_field_arr = explode(",",$copy_fields);
					for($n=0; $n<count($copy_field_arr); $n++)
					{
						$copy_field_parts = explode(":",$copy_field_arr[$n]);
						$copy_field_name = trim($copy_field_parts[0]);
						$copy_field_type = (isset($copy_field_parts[1]))?trim($copy_field_parts[1]):"";
						$copy_field_props = (isset($copy_field_parts[2]))?trim($copy_field_parts[2]):"";
						$copy_field_list[] = array("name"=>$copy_field_name,"type"=>$copy_field_type,"props"=>$copy_field_props);
					}
					return $copy_field_list;
				}
				
				function translate_linked_table_value($val,&$check_field_props,&$menu_info)
				{
					$result = process_translate_linked_table_value($val,$check_field_props,$menu_info);
					if(substr($result,0,7)=="[error]")
					{
						$status = "error";
						$value = substr($result,7);
					}
					else
					{
						$status = "ok";
						$value = $result;
					}
					
					$value_parts = explode("[table]",$value);
					$value = trim($value_parts[0]);
					if(count($value_parts) > 1) $match_table = trim($value_parts[1]);
					else $match_table = "";
					
					$value_parts = explode("[id]",$match_table);
					$match_table = trim($value_parts[0]);
					if(count($value_parts) > 1) $match_id = trim($value_parts[1]);
					else $match_id = "";
					
					$value_parts = explode("[match_field]",$match_id);
					$match_id = trim($value_parts[0]);
					if(count($value_parts) > 1) $match_field = trim($value_parts[1]);
					else $match_field = "";
					
					return array("status"=>$status,"value"=>$value,"match_table"=>$match_table,"match_id"=>$match_id,"match_field"=>$match_field);
				}
				
				function process_translate_linked_table_value($val,&$check_field_props,&$menu_info)
				{
					$multi_props = explode("|",$check_field_props);
					for($m=0; $m<count($multi_props); $m++)
					{
						$inner_multi_props = explode("+",$multi_props[$m]);
						if(count($inner_multi_props) > 1)
						{
							$prop_lead_char = trim($inner_multi_props[0]);
							$prop_tablename = trim($inner_multi_props[1]);
						}
						else
						{
							$prop_lead_char = "";
							$prop_tablename = trim($inner_multi_props[0]);
						}
						
						if($prop_tablename=="modifier_categories")
						{
							$prop_tablename = "optional_modifier_lists";
						}
						else if($prop_tablename=="modifiers")
						{
							$prop_tablename = "optional_modifiers";
						}
						if(substr($prop_tablename,0,5)=="menu_") $prop_tablename = substr($prop_tablename,5); // The menu array does not use the menu_ prefix like the tables do
						
						//$prop_lead_char_match = false;
						//if($prop_lead_char!="" && $prop_lead_char==substr($val,0,strlen($prop_lead_char)))
						//	$prop_lead_char_match = true;
						if($prop_tablename=="" && $val==$prop_lead_char)
						{
							if($val=="C") return "Category Default";
							else return $val;
							//return "no table - $val";
						}
						else if($prop_tablename!="" && ($prop_lead_char=="" || $prop_lead_char==substr($val,0,strlen($prop_lead_char))))
						{
							$val_id = substr($val,strlen($prop_lead_char));
							$tablekey = $prop_tablename . "_by_id";
							//if(isset($menu_info[$tablekey])) echo "Menu info has $tablekey<br>";
							if(isset($menu_info[$tablekey]) && isset($menu_info[$tablekey][$val_id]))
							{
								$match_id_str = "[table]" . $prop_tablename . "[id]" . $prop_lead_char;// . $menu_info[$tablekey][$val_id]['id'];
								
								if(isset($menu_info[$tablekey][$val_id]['name']))
									return $menu_info[$tablekey][$val_id]['name'] . $match_id_str . "[match_field]name";
								else if(isset($menu_info[$tablekey][$val_id]['title']))
									return $menu_info[$tablekey][$val_id]['title'] . $match_id_str . "[match_field]title";
								else if(isset($menu_info[$tablekey][$val_id]['group_name']))
									return $menu_info[$tablekey][$val_id]['group_name'] . $match_id_str . "[match_field]group_name";
								else
									return "[error]Cannot determine the Name of the Record";
							}
							else 
							{
								$display_tablename = ucwords(str_replace("_"," ",$prop_tablename));
								return "[error]Cannot Find Linked Record for table \"$display_tablename\" with value \"$val\" (maybe it has been deleted)";
								//else return "$prop_tablename -  $val_id - no $tablekey";
							}
						}
					}
					return "[error]Cannot Find Linked Record with value \"$val\" (maybe it has been deleted)";
				}
				
				function find_linked_value_in_table($tablename,$lead_char,$val,&$menu_info,$table_parents=array())
				{
					//foreach($menu_info[$tablename] as $tkey => $tval) echo $tablename . " - $tkey - $tval<br>";
					$setid = "";
					$error = "";
					$status = "";
					$blankok = false;
					
					$check_menu_info = $menu_info;
					if($tablename=="" && ($val=="Category Default"))
					{
						$setid = "C";
						$error = "";
						$status = "ok";
					}
					else if($tablename=="" && $val=="0")
					{
						$blankok = true;
						$setid = "0";
						$error = "";
						$status = "ok";
					}
					else if($tablename=="" && $val=="")
					{
						$blankok = true;
						$setid = "";
						$error = "";
						$status = "ok";
					}
					else if(isset($check_menu_info[$tablename]))
					{
						$check_menu_info = $check_menu_info[$tablename];
						for($n=0; $n<count($table_parents); $n++)
						{
							$parentname = $table_parents[$n];
							if(isset($check_menu_info[$parentname]))
							{
								$check_menu_info = $check_menu_info[$parentname];
							}
							else
							{
								$error = "level ".(count($table_parents) - $n)." parent \"" . ucwords(str_replace("_"," ",$parentname)) . "\" cannot be found for table \"" . ucwords(str_replace("_"," ",$tablename)) . "\"";
							}
						}
					}
					else
					{
						$error = "table \"" . ucwords(str_replace("_"," ",$tablename)) . "\" cannot be found";
					}
					
					if($error=="")
					{
						if(isset($check_menu_info[$val]))
						{
							$setid = $lead_char . $check_menu_info[$val]['id'];
						}
						else if($blankok && $val=="")
						{
						}
						else $error = "\"$val\" cannot be found in " . ucwords(str_replace("_"," ",$tablename));
					}
					
					if($status=="")
					{
						if($setid!="") $status = "ok"; else $status = "error";
					}
					return array("status"=>$status,"error"=>$error,"setid"=>$setid);
				}
				
				function process_menu_inner_compare(&$copy_field_list,&$other_menu_arr,&$menu_info,&$other_menus,&$found_match,&$found_field_match,&$cannot_match_error,&$describe_mismatch_text,&$i,&$item_read)
				{
					$found_match = true;
					$found_field_match = true;
					for($n=0; $n<count($copy_field_list); $n++)
					{
						$check_field = $copy_field_list[$n]['name'];
						$check_field_type = $copy_field_list[$n]['type'];
						$check_field_props = $copy_field_list[$n]['props'];
						$check_field_title = ucwords(str_replace("_"," ",$check_field));
						
						$other_menu_raw_value = $other_menu_arr[$check_field];
						$raw_value = $item_read[$check_field];
						
						if($check_field_type=="autoset")
						{
						}
						else if($check_field_type=="link_to_table" || $check_field_type=="multi_link_to_table")
						{
							if($check_field_type=="multi_link_to_table")
							{
								//echo $fieldtype . ": " . $raw_value . "<br>";
								$field_multi_delimiter = "|";
								$raw_value_list = explode($field_multi_delimiter,$raw_value);
								$other_menu_raw_value_list = explode($field_multi_delimiter,$other_menu_raw_value);
							}
							else
							{
								$raw_value_list = array($raw_value);
								$other_menu_raw_value_list = array($other_menu_raw_value);
							}
							
							if(count($raw_value_list) != count($other_menu_raw_value_list))
							{
								$found_field_match = false;
								$describe_mismatch_text .= $check_field_title . ": " . $other_menu_raw_valuee . " -> " . $raw_valu ."\n";
							}
							else
							{
								for($m=0; $m<count($raw_value_list); $m++)
								{
									$raw_value = $raw_value_list[$m];
									$other_menu_raw_value = $other_menu_raw_value_list[$m];
									
									$linked_value = translate_linked_table_value($raw_value,$check_field_props,$menu_info);
									$linked_value_other = translate_linked_table_value($other_menu_raw_value,$check_field_props,$other_menus[$i]);
									//echo $linked_value['status'] . " --------" . $linked_value_other['status'] . "<br>";
																							
									if($linked_value_other['status']=="error" || $linked_value['value'] != $linked_value_other['value'])
									{
										$linked_info_other = find_linked_value_in_table($linked_value['match_table'],$linked_value['match_id'],$linked_value['value'],$other_menus[$i]);
										//$linked_id_other = translate_linked_table_value($linked_value['value'],$check_field_props,$other_menus[$i],"by_name");
										//foreach($linked_id_other as $lkey => $lval) echo $lkey . " = " . $lval . "<br>";
										
										if($linked_info_other['status']=="error")
										{
											$found_match = false;
											$found_field_match = false;
											$cannot_match_error = $linked_info_other['error']."\n";
										}
										else
										{
											$found_field_match = false;
											$describe_mismatch_text .= $check_field_title . ": " . $linked_value_other['value'] . " -> " . $linked_value['value']."\n";
										}
									}
									
									if($linked_value['status']=="error")
									{
										$found_match = false;
										$found_field_match = false;
										$cannot_match_error = $linked_value['value'];
									}
									else if($linked_value_other['status']=="error")
									{
										$found_match = false;
										$found_field_match = false;
									}
								}
							}
						}
						else
						{
							if($other_menu_raw_value != $raw_value)
							{
								$found_field_match = false;
								$describe_mismatch_text .= $check_field_title . ": " . "$other_menu_raw_value -> $raw_value\n";
							}
						}
					}
				}
				
				function load_all_menus(&$rdb,&$other_loc_list,&$menu_info,&$other_menus,&$other_dbs,&$other_rests)
				{
					$menu_info = load_entire_location_menu($rdb);
					$other_menus = array();
					$other_dbs = array();
					$other_rests = array();
					
					for($i=0; $i<count($other_loc_list); $i++)
					{
						$orest_query = mlavu_query("select id,data_name,company_name from `poslavu_MAIN_db`.`restaurants` where `id`='[1]'",$other_loc_list[$i]);
						if(dm_mysql_num_rows($orest_query))
						{
							$orest_read = dm_mysql_fetch_assoc($orest_query);
							$odataname = $orest_read['data_name'];
							if($odataname!="")
							{
								$ordb = "poslavu_" . $odataname . "_db";
								$other_menus[] = load_entire_location_menu($ordb);
								$other_dbs[] = $ordb;
								$other_rests[] = $orest_read['company_name'];
							}
						}
					}
				}

				if(!isset($_POST['posted_copy']))
				{
					echo "<div id='duplicate_menu_container'>";
				}
				echo $selector_str;
				echo "<br><br>";
				
				$copy_fields = "";
				$copy_fields .= "menu_id:location_info:menu_id";
				$copy_fields .= ",price,description";
				$copy_fields .= ",image:picture:items";
				$copy_fields .= ",forced_modifier_group_id:link_to_table:C+|f+forced_modifier_lists|forced_modifier_groups"; // Forced Mods
				$copy_fields .= ",modifier_list_id:link_to_table:0+|+|modifier_categories"; // Optional Mods
				$copy_fields .= ",print,show_in_app,printer:autoset:0";
				
				$copy_fields_new = "";
				$copy_fields_new .= "menu_id:location_info:menu_id";
				$copy_fields_new .= ",category_id:link_to_table:menu_categories";
				$copy_fields_new .= ",name,price,description";
				$copy_fields_new .= ",image:picture:items";
				$copy_fields_new .= ",forced_modifier_group_id:link_to_table:C+|f+forced_modifier_lists|forced_modifier_groups"; // Forced Mods
				$copy_fields_new .= ",modifier_list_id:link_to_table:0+|+|modifier_categories"; // Optional Mods
				$copy_fields_new .= ",print,show_in_app,printer:autoset:0";
								
				$copy_fields_category = "menu_id:location_info:menu_id,name,image:picture:categories,active,group_id:link_to_table:menu_groups";
				$copy_fields_group = "group_name,menu_id:location_info:menu_id,orderby";
				$copy_fields_fm = "title,cost,list_id:link_to_table:forced_modifier_lists,detour:link_to_table:+|l+forced_modifier_lists|g+forced_modifier_groups";
				$copy_fields_fml = "title,description,type,menu_id:location_info:menu_id";
				$copy_fields_om = "title,cost,category:link_to_table:modifier_categories";
				$copy_fields_oml = "title,description,menu_id:location_info:menu_id";
				$copy_fields_fmg = "title,include_lists:multi_link_to_table:forced_modifier_lists,menu_id:location_info:menu_id";
				$copy_fields_sg = "title";
				
				$copy_fields_category .= ",super_group_id:link_to_table:super_groups";
				
				$copy_field_list = translate_field_list_to_array($copy_fields);
				$copy_field_list_new = translate_field_list_to_array($copy_fields_new);
				$copy_field_list_category = translate_field_list_to_array($copy_fields_category);
				$copy_field_list_group = translate_field_list_to_array($copy_fields_group);
				$copy_field_list_fm = translate_field_list_to_array($copy_fields_fm);
				$copy_field_list_fml = translate_field_list_to_array($copy_fields_fml);
				$copy_field_list_om = translate_field_list_to_array($copy_fields_om);
				$copy_field_list_oml = translate_field_list_to_array($copy_fields_oml);
				$copy_field_list_fmg = translate_field_list_to_array($copy_fields_fmg);
				$copy_field_list_sg = translate_field_list_to_array($copy_fields_sg);
				
				if(count($other_loc_list) < 1)
				{
					echo "Please choose a Group that contains the locations you would like to copy to<br>(The group must contain at least one location other than the location you are copying from)<br><br>";
				}
				else
				{
					$menu_info = array();
					$other_menus = array();
					$other_dbs = array();
					$other_rests = array();
					
					//--------------------------- Begin Processing after main form is submitted --------------------------------------------------//
					//$ckid = "copy_".$item_id."_to_".$other_dbs[$i]."_id_".$other_menus[$i]['items'][$group_name][$category_name][$item_name]['id'];
					if(isset($_POST['posted_copy']))
					{
						load_all_menus($rdb,$other_loc_list,$menu_info,$other_menus,$other_dbs,$other_rests);
						
						foreach($_POST as $postkey => $postval)
						{
							//echo $postkey . " = " . $postval . "<br>";
							if(substr($postkey,0,5)=="copy_")
							{
								$copy_split = explode("copy_",$postkey);
								if(count($copy_split) > 1)
								{
									$to_split = explode("_to_",$copy_split[1]);
									if(count($to_split) > 1)
									{
										$from_id = $to_split[0];
										$id_split = explode("_id_",$to_split[1]);
										if(count($id_split) > 1)
										{
											$other_db = $id_split[0];
											$other_id = $id_split[1];
											$other_menu = array();
											
											for($n=0; $n<count($other_dbs); $n++)
											{
												if($other_dbs[$n]==$other_db)
												{
													$other_menu = $other_menus[$n];
												}
											}
											
											if(substr($postval,0,4)=="xadd")
											{
												$x_add_parts = explode("_",$postval);
												if(count($x_add_parts) > 2)
												{
													$xadd_type = trim($x_add_parts[1]);
													$xadd_source_id = trim($x_add_parts[2]);
													$xadd_source_secondary_id = (isset($x_add_parts[3]))?trim($x_add_parts[3]):"";
																										
													if($xadd_type=="G") $source_table = "menu_groups";
													else if($xadd_type=="C") $source_table = "menu_categories";
													else if($xadd_type=="I") $source_table = "menu_items";
													else if($xadd_type=="L") $source_table = "forced_modifier_lists";
													else if($xadd_type=="F") $source_table = "forced_modifiers";
													else if($xadd_type=="M") $source_table = "modifier_categories";
													else if($xadd_type=="P") $source_table = "modifiers";
													else if($xadd_type=="S") $source_table = "super_groups";
													else if($xadd_type=="Fg") 
													{
														$source_table = "forced_modifier_groups";
														if($xadd_source_id=="group") $xadd_source_id = $xadd_source_secondary_id;
													}
													else $source_table = "";
													
													if($source_table!="")
													{
														if($source_table=="menu_groups")
														{
															copy_data_from_record_to_location("menu_groups",$xadd_source_id,$other_db,"new",$copy_field_list_group,$menu_info,$other_menu);
														}
														else if($source_table=="menu_categories")
														{
															copy_data_from_record_to_location("menu_categories",$xadd_source_id,$other_db,"new",$copy_field_list_category,$menu_info,$other_menu);
														}
														else if($source_table=="menu_items")
														{
															copy_data_from_record_to_location("menu_items",$xadd_source_id,$other_db,"new",$copy_field_list_new,$menu_info,$other_menu);
														}
														else if($source_table=="forced_modifier_lists")
														{
															copy_data_from_record_to_location("forced_modifier_lists",$xadd_source_id,$other_db,"new",$copy_field_list_fml,$menu_info,$other_menu);
														}
														else if($source_table=="forced_modifiers")
														{
															copy_data_from_record_to_location("forced_modifiers",$xadd_source_id,$other_db,"new",$copy_field_list_fm,$menu_info,$other_menu);
														}
														else if($source_table=="modifier_categories")
														{
															copy_data_from_record_to_location("modifier_categories",$xadd_source_id,$other_db,"new",$copy_field_list_oml,$menu_info,$other_menu);
														}
														else if($source_table=="modifiers")
														{
															copy_data_from_record_to_location("modifiers",$xadd_source_id,$other_db,"new",$copy_field_list_om,$menu_info,$other_menu);
														}
														else if($source_table=="forced_modifier_groups")
														{
															copy_data_from_record_to_location("forced_modifier_groups",$xadd_source_id,$other_db,"new",$copy_field_list_fmg,$menu_info,$other_menu);
														}
														else if($source_table=="super_groups")
														{
															copy_data_from_record_to_location("super_groups",$xadd_source_id,$other_db,"new",$copy_field_list_sg,$menu_info,$other_menu);
														}
													}
												}
											}
											else
											{
												copy_data_from_record_to_location("menu_items",$from_id,$other_db,$other_id,$copy_field_list,$menu_info,$other_menu);
												//echo "From Id: " . $from_id . "<br>Other Db: " . $other_db . "<br>Other Id: " . $other_id . "<br>";
											}
										}
									}
								}
							}
						}
					}
					//--------------------------- End Processing after main form is submitted ------------------------------------------------//
					
					load_all_menus($rdb,$other_loc_list,$menu_info,$other_menus,$other_dbs,$other_rests);
										
					$groups = $menu_info['groups'];
					$categories = $menu_info['categories'];
					$items = $menu_info['items'];
					$forced_modifier_lists = $menu_info['forced_modifier_lists'];
					$forced_modifiers = $menu_info['forced_modifiers'];
					$optional_modifier_lists = $menu_info['optional_modifier_lists'];
					$optional_modifiers = $menu_info['optional_modifiers'];
					$forced_modifier_groups = $menu_info['forced_modifier_groups'];
					$forced_modifier_lists_by_id = $menu_info['forced_modifier_lists_by_id'];
					$super_groups = $menu_info['super_groups'];
					$super_groups_by_id = $menu_info['super_groups_by_id'];
					$equiv_lists = array();
					
					echo "<style>";
					echo "	table,td { font-size:11px; } ";
					echo "</style>";
					echo "<form name='form1' method='post' action=''>";
					echo "<input type='hidden' name='posted_copy' id='posted_copy' value='1' />";
					echo "<table><tr><td valign='top'>";
					
					//------------------------------------------------ Start Draw Menu Table -------------------------------------------------------//
					echo "<table style='border:solid 1px #777777' cellspacing=0 cellpadding=2>";
					//$group_query = lavu_query("select * from `menu_groups` where `_deleted`!='1' order by `group_name` asc");
					//while($group_read = dm_mysql_fetch_assoc($group_query))
					//{
					foreach($groups as $gkey => $group_read)
					{
						$group_name = $group_read['group_name'];
						$group_id = $group_read['id'];
						
						echo "<tr><td align='center' bgcolor='#777777' style='font-weight:bold; color:#ffffff' colspan=2>";
						echo $group_name;
						echo "</td>";
						for($i=0; $i<count($other_menus); $i++) 
						{
							echo "<td align='center' bgcolor='#777777' style='font-weight:bold; color:#ffffff; font-size:9px; width:60px'>";
							echo "<div style='overflow:hidden; width:60px'><nobr>&nbsp;" . $other_rests[$i] . "&nbsp;</nobr></div>";
							echo "</td>";
						}
						echo "</tr>";
						
						//$category_query = lavu_query("select * from `menu_categories` where `group_id`='[1]' and `_deleted`!='1' order by `name` asc",$group_id);
						//while($category_read = dm_mysql_fetch_assoc($category_query))
						//{
						foreach($categories[$group_name] as $ckey => $category_read)
						{
							$category_name = $category_read['name'];
							$category_id = $category_read['id'];	
							//$other_menu_text = array();
							
							//echo "<tr>";
							//echo "<td valign='top' align='right' bgcolor='#eeeeee' style='border-bottom:solid 2px #bbbbbb'>";							
							//echo "<nobr>&nbsp;$category_name&nbsp;</nobr>";
							//echo "</td><td valign='top' align='left' style='border-bottom:solid 2px #bbbbbb; color:#555555; line-height:14px'>";
							$category_name_shown = false;
							
							for($i=0; $i<count($other_menus); $i++) $other_menu_text[$i] = "";
							foreach($items[$group_name][$category_name] as $ikey => $item_read)
							{
								$item_name = $item_read['name'];
								$item_id = $item_read['id'];
								
								$topstyle = "";
								if(!$category_name_shown) $topstyle = "border-top:solid 2px #bbbbbb";
								
								echo "<tr>";
								echo "<td valign='top' align='right' bgcolor='#eeeeee' style='$topstyle'>";
								if(!$category_name_shown)
								{
									echo "<nobr>&nbsp;$category_name&nbsp;</nobr>";
									$category_name_shown = true;
								}
								else
								{
									echo "&nbsp;";
								}
								echo "</td>";
								echo "<td style='color:#555555; $topstyle'><nobr>&nbsp;" . $item_name . "</nobr></td>";
								
								for($i=0; $i<count($other_menus); $i++) 
								{
									$found_match = false;
									$found_field_match = false;
									$found_item_match = false;
									$cannot_match_error = "Unable to find an item by the name \"$item_name\"";
									$describe_mismatch_text = "";
									$creation_symbol = "X";
																		
									if(isset($other_menus[$i]['items'][$group_name]))
									{
										if(isset($other_menus[$i]['items'][$group_name][$category_name]))
										{
											if(isset($other_menus[$i]['items'][$group_name][$category_name][$item_name]))
											{
												$found_item_match = true;
												process_menu_inner_compare($copy_field_list,$other_menus[$i]['items'][$group_name][$category_name][$item_name],$menu_info,$other_menus,$found_match,$found_field_match,$cannot_match_error,$describe_mismatch_text,$i,$item_read);
											}
											else 
											{
												$item_cannot_match_error = "Cannot Find Item \"$item_name\" in Category \"$category_name\" in Group \"$group_name\"";
												$cannot_match_error =  $item_cannot_match_error;
												$creation_symbol = "I";
												$creation_info = $items[$group_name][$category_name][$item_name]['id'];
												process_menu_inner_compare($copy_field_list,$other_menus[$i]['items'][$group_name][$category_name][$item_name],$menu_info,$other_menus,$found_match,$found_field_match,$cannot_match_error,$describe_mismatch_text,$i,$item_read);
												if($item_cannot_match_error!=$cannot_match_error)
												{
													$creation_symbol = "X";
													$found_item_match = false;
												}
											}
										}
										else 
										{
											$cannot_match_error = "Cannot Find Category \"$category_name\" in Group \"$group_name\"";
											$creation_symbol = "C";
											$creation_info = $categories[$group_name][$category_name]['id'];
											
											if(1)//strpos($data_name,"lavu_chai")!==false)
											{
												$category_super_group_id = $categories[$group_name][$category_name]['super_group_id'];
												$category_super_group_name = "";
												if($category_super_group_id!="" && isset($super_groups_by_id[$category_super_group_id]))
												{
													$category_super_group_name = $super_groups_by_id[$category_super_group_id]['title'];
												}
												if($category_super_group_name!="")
												{
													if(!isset($other_menus[$i]['super_groups'][$category_super_group_name]))
													{
														$creation_symbol = "S";
														$creation_info = $super_groups[$category_super_group_name]['id'];
														$cannot_match_error .= " - Category requires Super Group \"$category_super_group_name\"";
													}
												}
											}
										}
									}
									else 
									{
										$cannot_match_error = "Cannot Find Group \"$group_name\"";
										$creation_symbol = "G";
										$creation_info = $groups[$group_name]['id'];									
									}
									
									$cellstyle = "";
									$ckid = "copy_".$item_id."_to_".$other_dbs[$i]."_id_".$other_menus[$i]['items'][$group_name][$category_name][$item_name]['id'];
									
									if($found_match) 
									{
										if($found_field_match)
										{
											// Item exists in both menus and is an exact match
											$add_chr = "<font style='color:#008800'><b>O</b></font>";
										}
										else
										{
											// Item exists in both menus but is not an exact match
											$add_chr = "<font style='color:#008800' title='$describe_mismatch_text'><b><input type='checkbox' id='$ckid' name='$ckid' onmouseup='checkbox_mouseup()'></b></font>"; 
										}
									}
									else 
									{
										$cannot_match_error_escaped = str_replace("\"","\\\"",$cannot_match_error);
										if($creation_symbol=="X")//($found_item_match)
										{
											$add_chr = "<font style='color:#aaaaaa' title='$cannot_match_error' id='x_$ckid'>$creation_symbol</font>";
											// Item is found but linked fields are not present (such as a modifier group, etc.)
											//$add_chr = "<font style='color:#008800' title='$describe_mismatch_text'><b><input type='checkbox' id='$ckid' name='$ckid'></b></font>";
											//$cellstyle = "bgcolor='#eeeeee'";
										}
										else
										{
											// Item does not exist in secondary menu
											//$xadd_str = "xadd_" . $creation_symbol . "_" . $creation_info;
											$add_chr = "<font style='color:#aaaaaa' title='$cannot_match_error' onclick='click_xadd_button(this,\"$creation_symbol\",\"$creation_info\",\"$ckid\",\"$i\")' id='x_$ckid'>$creation_symbol</font>";
											
											if(!isset($equiv_lists["R" . $i . "_" . $creation_info])) $equiv_lists["R" . $i . "_" . $creation_info] = array();
											$equiv_lists["R" . $i . "_" . $creation_info][] = $ckid;
										}
									}
									echo "<td style='cursor:pointer; $topstyle' align='center' $cellstyle>$add_chr</td>";
								}
								echo "</tr>";
							}
							
							//echo "</td>";
							//for($i=0; $i<count($other_menu_text); $i++)
							//{
							//	echo "<td style='border-bottom:solid 2px #bbbbbb; line-height:14px' align='center' valign='top'>" . $other_menu_text[$i] . "</td>";
							//}
							//echo "</tr>";
						}			
					}
					echo "</table>";
					//------------------------------------------------- End Draw Menu Table -------------------------------------------------------------------//
					
					echo "</td><td>&nbsp;</td><td valign='top'>";

					//---------------------------------------------- Begin Draw Forced Modifiers Table -------------------------------------------------------------//
					echo "<table style='border:solid 1px #777777' cellspacing=0 cellpadding=2>";
					//$group_query = lavu_query("select * from `menu_groups` where `_deleted`!='1' order by `group_name` asc");
					//while($group_read = dm_mysql_fetch_assoc($group_query))
					//{
					if(1)
					{
						echo "<tr><td align='center' bgcolor='#777777' style='font-weight:bold; color:#ffffff' colspan=2>";
						echo "Forced Modifiers";
						echo "</td>";
						for($i=0; $i<count($other_menus); $i++) 
						{
							echo "<td align='center' bgcolor='#777777' style='font-weight:bold; color:#ffffff; font-size:9px; width:60px'>";
							echo "<div style='overflow:hidden; width:60px'><nobr>&nbsp;" . $other_rests[$i] . "&nbsp;</nobr></div>";
							echo "</td>";
						}
						echo "</tr>";
						
						//$category_query = lavu_query("select * from `menu_categories` where `group_id`='[1]' and `_deleted`!='1' order by `name` asc",$group_id);
						//while($category_read = dm_mysql_fetch_assoc($category_query))
						//{
						foreach($forced_modifier_lists as $ckey => $category_read)
						{
							$category_name = $category_read['title'];
							$category_id = $category_read['id'];	
							//$other_menu_text = array();
							
							//echo "<tr>";
							//echo "<td valign='top' align='right' bgcolor='#eeeeee' style='border-bottom:solid 2px #bbbbbb'>";							
							//echo "<nobr>&nbsp;$category_name&nbsp;</nobr>";
							//echo "</td><td valign='top' align='left' style='border-bottom:solid 2px #bbbbbb; color:#555555; line-height:14px'>";
							$category_name_shown = false;
							
							for($i=0; $i<count($other_menus); $i++) $other_menu_text[$i] = "";
							foreach($forced_modifiers[$category_name] as $ikey => $item_read)
							{
								$item_name = $item_read['title'];
								$item_id = $item_read['id'];
								
								$topstyle = "";
								if(!$category_name_shown) $topstyle = "border-top:solid 2px #bbbbbb";
								
								echo "<tr>";
								echo "<td valign='top' align='right' bgcolor='#eeeeee' style='$topstyle'>";
								if(!$category_name_shown)
								{
									echo "<nobr>&nbsp;$category_name&nbsp;</nobr>";
									$category_name_shown = true;
								}
								else
								{
									echo "&nbsp;";
								}
								echo "</td>";
								echo "<td style='color:#555555; $topstyle'><nobr>&nbsp;" . $item_name . "</nobr></td>";
								
								for($i=0; $i<count($other_menus); $i++) 
								{
									$found_match = false;
									$found_field_match = false;
									$found_item_match = false;
									$cannot_match_error = "Unable to find a modifier by the name \"$item_name\"";
									$describe_mismatch_text = "";
									$creation_symbol = "X";
									
									if(1)
									{
										if(isset($other_menus[$i]['forced_modifiers'][$category_name]))
										{
											if(isset($other_menus[$i]['forced_modifiers'][$category_name][$item_name]))
											{
												$found_item_match = true;
												$found_field_match = true;
												$found_match = true;
												//process_menu_inner_compare($copy_field_list,$other_menus[$i]['items'][$group_name][$category_name][$item_name],$menu_info,$other_menus,$found_match,$found_field_match,$cannot_match_error,$describe_mismatch_text,$i,$item_read);
											}
											else 
											{
												$item_cannot_match_error = "Cannot Find Modifier \"$item_name\" in List \"$category_name\"";
												$cannot_match_error =  $item_cannot_match_error;
												$creation_symbol = "F";
												$creation_info = $forced_modifiers[$category_name][$item_name]['id'];
												/*process_menu_inner_compare($copy_field_list,$other_menus[$i]['items'][$group_name][$category_name][$item_name],$menu_info,$other_menus,$found_match,$found_field_match,$cannot_match_error,$describe_mismatch_text,$i,$item_read);
												if($item_cannot_match_error!=$cannot_match_error)
												{
													$creation_symbol = "X";
													$found_item_match = false;
												}*/
											}
										}
										else 
										{
											$cannot_match_error = "Cannot Find Modifier List \"$category_name\"";
											$creation_symbol = "L";
											$creation_info = $forced_modifier_lists[$category_name]['id'];
										}
									}
									else 
									{
									}
									
									$cellstyle = "";
									$ckid = "copy_fmod_".$item_id."_to_".$other_dbs[$i]."_id_".$other_menus[$i]['forced_modifiers'][$category_name][$item_name]['id'];
									
									if($found_match) 
									{
										if($found_field_match)
										{
											// Item exists in both menus and is an exact match
											$add_chr = "<font style='color:#008800'><b>O</b></font>";
										}
										else
										{
											// Item exists in both menus but is not an exact match
											$add_chr = "<font style='color:#008800' title='$describe_mismatch_text'><b><input type='checkbox' id='$ckid' name='$ckid'></b></font>"; 
										}
									}
									else 
									{
										$cannot_match_error_escaped = str_replace("\"","\\\"",$cannot_match_error);
										if($creation_symbol=="X")//($found_item_match)
										{
											$add_chr = "<font style='color:#aaaaaa' title='$cannot_match_error' id='x_$ckid'>$creation_symbol</font>";
											// Item is found but linked fields are not present (such as a modifier group, etc.)
											//$add_chr = "<font style='color:#008800' title='$describe_mismatch_text'><b><input type='checkbox' id='$ckid' name='$ckid'></b></font>";
											//$cellstyle = "bgcolor='#eeeeee'";
										}
										else
										{
											// Item does not exist in secondary menu
											//$xadd_str = "xadd_" . $creation_symbol . "_" . $creation_info;
											$add_chr = "<font style='color:#aaaaaa' title='$cannot_match_error' onclick='click_xadd_button(this,\"$creation_symbol\",\"$creation_info\",\"$ckid\",\"$i\",\"RR\")' id='x_$ckid'>$creation_symbol</font>";
											
											if(!isset($equiv_lists["RR" . $i . "_" . $creation_info])) $equiv_lists["RR" . $i . "_" . $creation_info] = array();
											$equiv_lists["RR" . $i . "_" . $creation_info][] = $ckid;
										}
									}
									echo "<td style='cursor:pointer; $topstyle' align='center' $cellstyle>$add_chr</td>";
								}
								echo "</tr>";
							}
							
							//echo "</td>";
							//for($i=0; $i<count($other_menu_text); $i++)
							//{
							//	echo "<td style='border-bottom:solid 2px #bbbbbb; line-height:14px' align='center' valign='top'>" . $other_menu_text[$i] . "</td>";
							//}
							//echo "</tr>";
						}			
					}
					//------------------------------------------------ End Draw Forced Modifiers -----------------------------------------------------------------------//					
					//------------------------------------------------ Start Draw Forced Modifier Groups -------------------------------------------------------------------//
					if(1)
					{
						echo "<tr><td align='center' bgcolor='#777777' style='font-weight:bold; color:#ffffff' colspan=2>";
						echo "Forced Modifier Groups";
						echo "</td>";
						for($i=0; $i<count($other_menus); $i++) 
						{
							echo "<td align='center' bgcolor='#777777' style='font-weight:bold; color:#ffffff; font-size:9px; width:60px'>";
							echo "<div style='overflow:hidden; width:60px'><nobr>&nbsp;" . $other_rests[$i] . "&nbsp;</nobr></div>";
							echo "</td>";
						}
						echo "</tr>";
						
						//$category_query = lavu_query("select * from `menu_categories` where `group_id`='[1]' and `_deleted`!='1' order by `name` asc",$group_id);
						//while($category_read = dm_mysql_fetch_assoc($category_query))
						//{
						foreach($forced_modifier_groups as $ckey => $category_read)
						{
							$category_name = $category_read['title'];
							$category_id = $category_read['id'];	
							//$other_menu_text = array();
							
							//echo "<tr>";
							//echo "<td valign='top' align='right' bgcolor='#eeeeee' style='border-bottom:solid 2px #bbbbbb'>";							
							//echo "<nobr>&nbsp;$category_name&nbsp;</nobr>";
							//echo "</td><td valign='top' align='left' style='border-bottom:solid 2px #bbbbbb; color:#555555; line-height:14px'>";
							$category_name_shown = false;
							
							$grouped_modifier_list_ids = explode("|",$category_read['include_lists']);
							$grouped_modifier_lists = array();
							for($i=0; $i<count($grouped_modifier_list_ids); $i++)
							{
								$gmodid = $grouped_modifier_list_ids[$i];
								$grouped_modifier_lists[] = $forced_modifier_lists_by_id[$gmodid];
							}
							
							for($i=0; $i<count($other_menus); $i++) $other_menu_text[$i] = "";
							if(1)
							{
								$item_name = "Group";
								$item_id = "group_" . $category_id;
								$show_category_name = "&nbsp;";
								$show_item_name = "<b>" . $category_name . "</b>";
								
								$topstyle = "";
								if(!$category_name_shown) $topstyle = "border-top:solid 2px #bbbbbb; background-color:#dddddd;";
								
								echo "<tr>";
								echo "<td valign='top' align='right' bgcolor='#eeeeee' style='$topstyle'>";
								if(!$category_name_shown)
								{
									echo "<nobr>&nbsp;$show_category_name&nbsp;</nobr>";
									$category_name_shown = true;
								}
								else
								{
									echo "&nbsp;";
								}
								echo "</td>";
								echo "<td style='color:#555555; $topstyle'><nobr>&nbsp;" . $show_item_name . "</nobr></td>";
								
								for($i=0; $i<count($other_menus); $i++) 
								{
									$found_match = false;
									$found_field_match = false;
									$found_item_match = false;
									$cannot_match_error = "Unable to find a modifier group by the name \"$category_name\"";
									$describe_mismatch_text = "";
									$creation_symbol = "X";
									
									$item_category_name = $item_name;
									$item_category_id = $item_id;
									if(1)
									{
										if(isset($other_menus[$i]['forced_modifier_groups'][$category_name]))
										{
											$found_item_match = true;
											//$found_field_match = true;
											//$found_match = true;
										
											//echo "cfl: ";
											//for($m=0; $m<count($copy_field_list); $m++)
											//foreach($copy_field_list as $key => $val) echo $key . "=" . $val . " ";
											//echo "<br>";
											process_menu_inner_compare($copy_field_list_fmg,$other_menus[$i]['forced_modifier_groups'][$category_name],$menu_info,$other_menus,$found_match,$found_field_match,$cannot_match_error,$describe_mismatch_text,$i,$category_read);
										}
										else
										{
											$missing_fmod_list = false;
											$first_missing_fmod_list = "";
											foreach($grouped_modifier_lists as $ikey => $ival)
											{
												$mod_name = $ival['title'];
												$mod_id = $ival['id'];
												
												$found_current_mod_list = (isset($other_menus[$i]['forced_modifier_lists'][$mod_name]));
												if(!$found_current_mod_list) 
												{
													$missing_fmod_list = true;
													if($first_missing_fmod_list=="")
													{
														$first_missing_fmod_list = $mod_name;
													}
												}
												//echo $item_category_name . " - " . $mod_name;
												//if($found_current_mod_list) echo " FOUND"; else echo " not-found";
												//echo "<br>";
												//----------------------------------------------------------------------------------------- LEFT OFF HERE -------------------------------------------------//
											}
											
											if($missing_fmod_list)
											{
												$creation_symbol = "X";
												$cannot_match_error = "Missing Modifier List \"$first_missing_fmod_list\"";
											}
											else
											{
												$creation_symbol = "Fg";
												$creation_info = $item_category_id;
											}
										}
										/*if(isset($other_menus[$i]['forced_modifier_lists'][$item_category_name]))
										{
											$found_item_match = true;
											$found_field_match = true;
											$found_match = true;												//process_menu_inner_compare($copy_field_list,$other_menus[$i]['items'][$group_name][$category_name][$item_name],$menu_info,$other_menus,$found_match,$found_field_match,$cannot_match_error,$describe_mismatch_text,$i,$item_read);
										}
										else 
										{
											$cannot_match_error = "Cannot Find Modifier List \"$item_category_name\"";
											$creation_symbol = "L";
											$creation_info = $item_category_id;
										}*/
									}
									else 
									{
									}
									
									$cellstyle = "";
									$ckid = "copy_fmod_".$item_category_id."_to_".$other_dbs[$i]."_id_".$item_id;
									
									if($found_match) 
									{
										if($found_field_match)
										{
											// Item exists in both menus and is an exact match
											$add_chr = "<font style='color:#008800'><b>O</b></font>";
										}
										else
										{
											// Item exists in both menus but is not an exact match
											$add_chr = "<font style='color:#008800' title='$describe_mismatch_text'><b><input type='checkbox' id='$ckid' name='$ckid'></b></font>"; 
										}
									}
									else 
									{
										$cannot_match_error_escaped = str_replace("\"","\\\"",$cannot_match_error);
										if($creation_symbol=="X")//($found_item_match)
										{
											$add_chr = "<font style='color:#aaaaaa' title='$cannot_match_error' id='x_$ckid'>$creation_symbol</font>";
											// Item is found but linked fields are not present (such as a modifier group, etc.)
											//$add_chr = "<font style='color:#008800' title='$describe_mismatch_text'><b><input type='checkbox' id='$ckid' name='$ckid'></b></font>";
											//$cellstyle = "bgcolor='#eeeeee'";
										}
										else
										{
											// Item does not exist in secondary menu
											//$xadd_str = "xadd_" . $creation_symbol . "_" . $creation_info;
											$add_chr = "<font style='color:#aaaaaa' title='$cannot_match_error' onclick='click_xadd_button(this,\"$creation_symbol\",\"$creation_info\",\"$ckid\",\"$i\",\"RR\")' id='x_$ckid'>$creation_symbol</font>";
											
											if(!isset($equiv_lists["RR" . $i . "_" . $creation_info])) $equiv_lists["RR" . $i . "_" . $creation_info] = array();
											$equiv_lists["RR" . $i . "_" . $creation_info][] = $ckid;
										}
									}
									echo "<td style='cursor:pointer; $topstyle' align='center' $cellstyle>$add_chr</td>";
								}
								echo "</tr>";
							}
							foreach($grouped_modifier_lists as $ikey => $item_read)
							{
								$item_name = $item_read['title'];
								$item_id = $item_read['id'];
								
								$topstyle = "";
								if(!$category_name_shown) $topstyle = "border-top:solid 2px #bbbbbb";
								
								echo "<tr>";
								echo "<td valign='top' align='right' bgcolor='#eeeeee' style='$topstyle'>";
								if(!$category_name_shown)
								{
									echo "<nobr>&nbsp;$category_name&nbsp;</nobr>";
									$category_name_shown = true;
								}
								else
								{
									echo "&nbsp;";
								}
								echo "</td>";
								echo "<td style='color:#555555; $topstyle'><nobr>&nbsp;" . $item_name . "</nobr></td>";
								
								for($i=0; $i<count($other_menus); $i++) 
								{
									$found_match = false;
									$found_field_match = false;
									$found_item_match = false;
									$cannot_match_error = "Unable to find a modifier list by the name \"$item_name\"";
									$describe_mismatch_text = "";
									$creation_symbol = "X";
									
									$item_category_name = $item_name;
									$item_category_id = $item_id;
									if(1)
									{
										if(isset($other_menus[$i]['forced_modifier_lists'][$item_category_name]))
										{
											$found_item_match = true;
											$found_field_match = true;
											$found_match = true;												//process_menu_inner_compare($copy_field_list,$other_menus[$i]['items'][$group_name][$category_name][$item_name],$menu_info,$other_menus,$found_match,$found_field_match,$cannot_match_error,$describe_mismatch_text,$i,$item_read);
										}
										else 
										{
											$cannot_match_error = "Cannot Find Modifier List \"$item_category_name\"";
											$creation_symbol = "L";
											$creation_info = $item_category_id;
										}
									}
									else 
									{
									}
									
									$cellstyle = "";
									$ckid = "copy_fmod_".$item_category_id."_to_".$other_dbs[$i]."_id_".$other_menus[$i]['forced_modifier_lists'][$item_category_name]['id'];
									
									if($found_match) 
									{
										if($found_field_match)
										{
											// Item exists in both menus and is an exact match
											$add_chr = "<font style='color:#008800'><b>O</b></font>";
										}
										else
										{
											// Item exists in both menus but is not an exact match
											$add_chr = "<font style='color:#008800' title='$describe_mismatch_text'><b><input type='checkbox' id='$ckid' name='$ckid'></b></font>"; 
										}
									}
									else 
									{
										$cannot_match_error_escaped = str_replace("\"","\\\"",$cannot_match_error);
										if($creation_symbol=="X")//($found_item_match)
										{
											$add_chr = "<font style='color:#aaaaaa' title='$cannot_match_error' id='x_$ckid'>$creation_symbol</font>";
											// Item is found but linked fields are not present (such as a modifier group, etc.)
											//$add_chr = "<font style='color:#008800' title='$describe_mismatch_text'><b><input type='checkbox' id='$ckid' name='$ckid'></b></font>";
											//$cellstyle = "bgcolor='#eeeeee'";
										}
										else
										{
											// Item does not exist in secondary menu
											//$xadd_str = "xadd_" . $creation_symbol . "_" . $creation_info;
											$add_chr = "<font style='color:#aaaaaa' title='$cannot_match_error' onclick='click_xadd_button(this,\"$creation_symbol\",\"$creation_info\",\"$ckid\",\"$i\",\"RR\")' id='x_$ckid'>$creation_symbol</font>";
											
											if(!isset($equiv_lists["RR" . $i . "_" . $creation_info])) $equiv_lists["RR" . $i . "_" . $creation_info] = array();
											$equiv_lists["RR" . $i . "_" . $creation_info][] = $ckid;
										}
									}
									echo "<td style='cursor:pointer; $topstyle' align='center' $cellstyle>$add_chr</td>";
								}
								echo "</tr>";
							}
							
							//echo "</td>";
							//for($i=0; $i<count($other_menu_text); $i++)
							//{
							//	echo "<td style='border-bottom:solid 2px #bbbbbb; line-height:14px' align='center' valign='top'>" . $other_menu_text[$i] . "</td>";
							//}
							//echo "</tr>";
						}			
					}					
					
					//------------------------------------------------ End Draw Forced Modifier Groups Table ------------------------------------------------------------------//
					//------------------------------------------------ Start Draw Optional Modifiers -------------------------------------------------------------------//
					if(1)
					{
						echo "<tr><td align='center' bgcolor='#777777' style='font-weight:bold; color:#ffffff' colspan=2>";
						echo "Optional Modifiers";
						echo "</td>";
						for($i=0; $i<count($other_menus); $i++) 
						{
							echo "<td align='center' bgcolor='#777777' style='font-weight:bold; color:#ffffff; font-size:9px; width:60px'>";
							echo "<div style='overflow:hidden; width:60px'><nobr>&nbsp;" . $other_rests[$i] . "&nbsp;</nobr></div>";
							echo "</td>";
						}
						echo "</tr>";
						
						//$category_query = lavu_query("select * from `menu_categories` where `group_id`='[1]' and `_deleted`!='1' order by `name` asc",$group_id);
						//while($category_read = dm_mysql_fetch_assoc($category_query))
						//{
						foreach($optional_modifier_lists as $ckey => $category_read)
						{
							$category_name = $category_read['title'];
							$category_id = $category_read['id'];	
							//$other_menu_text = array();
							
							//echo "<tr>";
							//echo "<td valign='top' align='right' bgcolor='#eeeeee' style='border-bottom:solid 2px #bbbbbb'>";							
							//echo "<nobr>&nbsp;$category_name&nbsp;</nobr>";
							//echo "</td><td valign='top' align='left' style='border-bottom:solid 2px #bbbbbb; color:#555555; line-height:14px'>";
							$category_name_shown = false;
							
							for($i=0; $i<count($other_menus); $i++) $other_menu_text[$i] = "";
							foreach($optional_modifiers[$category_name] as $ikey => $item_read)
							{
								$item_name = $item_read['title'];
								$item_id = $item_read['id'];
								
								$topstyle = "";
								if(!$category_name_shown) $topstyle = "border-top:solid 2px #bbbbbb";
								
								echo "<tr>";
								echo "<td valign='top' align='right' bgcolor='#eeeeee' style='$topstyle'>";
								if(!$category_name_shown)
								{
									echo "<nobr>&nbsp;$category_name&nbsp;</nobr>";
									$category_name_shown = true;
								}
								else
								{
									echo "&nbsp;";
								}
								echo "</td>";
								echo "<td style='color:#555555; $topstyle'><nobr>&nbsp;" . $item_name . "</nobr></td>";
								
								for($i=0; $i<count($other_menus); $i++) 
								{
									$found_match = false;
									$found_field_match = false;
									$found_item_match = false;
									$cannot_match_error = "Unable to find a modifier by the name \"$item_name\"";
									$describe_mismatch_text = "";
									$creation_symbol = "X";
									
									if(1)
									{
										if(isset($other_menus[$i]['optional_modifiers'][$category_name]))
										{
											if(isset($other_menus[$i]['optional_modifiers'][$category_name][$item_name]))
											{
												$found_item_match = true;
												$found_field_match = true;
												$found_match = true;
												//process_menu_inner_compare($copy_field_list,$other_menus[$i]['items'][$group_name][$category_name][$item_name],$menu_info,$other_menus,$found_match,$found_field_match,$cannot_match_error,$describe_mismatch_text,$i,$item_read);
											}
											else 
											{
												$item_cannot_match_error = "Cannot Find Optional Modifier \"$item_name\" in List \"$category_name\"";
												$cannot_match_error =  $item_cannot_match_error;
												$creation_symbol = "P";
												$creation_info = $optional_modifiers[$category_name][$item_name]['id'];
												/*process_menu_inner_compare($copy_field_list,$other_menus[$i]['items'][$group_name][$category_name][$item_name],$menu_info,$other_menus,$found_match,$found_field_match,$cannot_match_error,$describe_mismatch_text,$i,$item_read);
												if($item_cannot_match_error!=$cannot_match_error)
												{
													$creation_symbol = "X";
													$found_item_match = false;
												}*/
											}
										}
										else 
										{
											$cannot_match_error = "Cannot Find Optional Modifier List \"$category_name\"";
											$creation_symbol = "M";
											$creation_info = $optional_modifier_lists[$category_name]['id'];
										}
									}
									else 
									{
									}
									
									$cellstyle = "";
									$ckid = "copy_omod_".$item_id."_to_".$other_dbs[$i]."_id_".$other_menus[$i]['forced_modifiers'][$category_name][$item_name]['id'];
									
									if($found_match) 
									{
										if($found_field_match)
										{
											// Item exists in both menus and is an exact match
											$add_chr = "<font style='color:#008800'><b>O</b></font>";
										}
										else
										{
											// Item exists in both menus but is not an exact match
											$add_chr = "<font style='color:#008800' title='$describe_mismatch_text'><b><input type='checkbox' id='$ckid' name='$ckid'></b></font>"; 
										}
									}
									else 
									{
										$cannot_match_error_escaped = str_replace("\"","\\\"",$cannot_match_error);
										if($creation_symbol=="X")//($found_item_match)
										{
											$add_chr = "<font style='color:#aaaaaa' title='$cannot_match_error' id='x_$ckid'>$creation_symbol</font>";
											// Item is found but linked fields are not present (such as a modifier group, etc.)
											//$add_chr = "<font style='color:#008800' title='$describe_mismatch_text'><b><input type='checkbox' id='$ckid' name='$ckid'></b></font>";
											//$cellstyle = "bgcolor='#eeeeee'";
										}
										else
										{
											// Item does not exist in secondary menu
											//$xadd_str = "xadd_" . $creation_symbol . "_" . $creation_info;
											$add_chr = "<font style='color:#aaaaaa' title='$cannot_match_error' onclick='click_xadd_button(this,\"$creation_symbol\",\"$creation_info\",\"$ckid\",\"$i\",\"RR\")' id='x_$ckid'>$creation_symbol</font>";
											
											if(!isset($equiv_lists["RR" . $i . "_" . $creation_info])) $equiv_lists["RR" . $i . "_" . $creation_info] = array();
											$equiv_lists["RR" . $i . "_" . $creation_info][] = $ckid;
										}
									}
									echo "<td style='cursor:pointer; $topstyle' align='center' $cellstyle>$add_chr</td>";
								}
								echo "</tr>";
							}
							
							//echo "</td>";
							//for($i=0; $i<count($other_menu_text); $i++)
							//{
							//	echo "<td style='border-bottom:solid 2px #bbbbbb; line-height:14px' align='center' valign='top'>" . $other_menu_text[$i] . "</td>";
							//}
							//echo "</tr>";
						}			
					}					
					
					//------------------------------------------------ End Draw Optional Modifier Table ------------------------------------------------------------------//
					//------------------------------------------------ Start Draw Super Groups -------------------------------------------------------------------//
					if(1)
					{
						echo "<tr><td align='center' bgcolor='#777777' style='font-weight:bold; color:#ffffff' colspan=2>";
						echo "Super Groups";
						echo "</td>";
						for($i=0; $i<count($other_menus); $i++) 
						{
							echo "<td align='center' bgcolor='#777777' style='font-weight:bold; color:#ffffff; font-size:9px; width:60px'>";
							echo "<div style='overflow:hidden; width:60px'><nobr>&nbsp;" . $other_rests[$i] . "&nbsp;</nobr></div>";
							echo "</td>";
						}
						echo "</tr>";
						
						//$category_query = lavu_query("select * from `menu_categories` where `group_id`='[1]' and `_deleted`!='1' order by `name` asc",$group_id);
						//while($category_read = dm_mysql_fetch_assoc($category_query))
						//{
						if(1)//foreach($optional_modifier_lists as $ckey => $category_read)
						{
							$category_name = "";//$category_read['title'];
							$category_id = "";//$category_read['id'];	
							//$other_menu_text = array();
							
							//echo "<tr>";
							//echo "<td valign='top' align='right' bgcolor='#eeeeee' style='border-bottom:solid 2px #bbbbbb'>";							
							//echo "<nobr>&nbsp;$category_name&nbsp;</nobr>";
							//echo "</td><td valign='top' align='left' style='border-bottom:solid 2px #bbbbbb; color:#555555; line-height:14px'>";
							$category_name_shown = false;
							
							for($i=0; $i<count($other_menus); $i++) $other_menu_text[$i] = "";
							foreach($super_groups as $ikey => $item_read)
							{
								$item_name = $item_read['title'];
								$item_id = $item_read['id'];
								
								$topstyle = "";
								if(!$category_name_shown) $topstyle = "border-top:solid 2px #bbbbbb";
								
								echo "<tr>";
								echo "<td valign='top' align='right' bgcolor='#eeeeee' style='$topstyle'>";
								if(!$category_name_shown)
								{
									echo "<nobr>&nbsp;$category_name&nbsp;</nobr>";
									$category_name_shown = true;
								}
								else
								{
									echo "&nbsp;";
								}
								echo "</td>";
								echo "<td style='color:#555555; $topstyle'><nobr>&nbsp;" . $item_name . "</nobr></td>";
								
								for($i=0; $i<count($other_menus); $i++) 
								{
									$found_match = false;
									$found_field_match = false;
									$found_item_match = false;
									$cannot_match_error = "Unable to find a modifier by the name \"$item_name\"";
									$describe_mismatch_text = "";
									$creation_symbol = "X";
									
									if(1)
									{
										if(isset($other_menus[$i]['super_groups']))
										{
											if(isset($other_menus[$i]['super_groups'][$item_name]))
											{
												$found_item_match = true;
												$found_field_match = true;
												$found_match = true;
												//process_menu_inner_compare($copy_field_list,$other_menus[$i]['items'][$group_name][$category_name][$item_name],$menu_info,$other_menus,$found_match,$found_field_match,$cannot_match_error,$describe_mismatch_text,$i,$item_read);
											}
											else 
											{
												$item_cannot_match_error = "Cannot Find Super Group \"$item_name\"";
												$cannot_match_error =  $item_cannot_match_error;
												$creation_symbol = "S";
												$creation_info = $super_groups[$item_name]['id'];
												/*process_menu_inner_compare($copy_field_list,$other_menus[$i]['items'][$group_name][$category_name][$item_name],$menu_info,$other_menus,$found_match,$found_field_match,$cannot_match_error,$describe_mismatch_text,$i,$item_read);
												if($item_cannot_match_error!=$cannot_match_error)
												{
													$creation_symbol = "X";
													$found_item_match = false;
												}*/
											}
										}
									}
									else 
									{
									}
									
									$cellstyle = "";
									$ckid = "copy_sg_".$item_id."_to_".$other_dbs[$i]."_id_".$other_menus[$i]['super_groups'][$item_name]['id'];
									
									if($found_match) 
									{
										if($found_field_match)
										{
											// Item exists in both menus and is an exact match
											$add_chr = "<font style='color:#008800'><b>O</b></font>";
										}
										else
										{
											// Item exists in both menus but is not an exact match
											$add_chr = "<font style='color:#008800' title='$describe_mismatch_text'><b><input type='checkbox' id='$ckid' name='$ckid'></b></font>"; 
										}
									}
									else 
									{
										$cannot_match_error_escaped = str_replace("\"","\\\"",$cannot_match_error);
										if($creation_symbol=="X")//($found_item_match)
										{
											$add_chr = "<font style='color:#aaaaaa' title='$cannot_match_error' id='x_$ckid'>$creation_symbol</font>";
											// Item is found but linked fields are not present (such as a modifier group, etc.)
											//$add_chr = "<font style='color:#008800' title='$describe_mismatch_text'><b><input type='checkbox' id='$ckid' name='$ckid'></b></font>";
											//$cellstyle = "bgcolor='#eeeeee'";
										}
										else
										{
											// Item does not exist in secondary menu
											//$xadd_str = "xadd_" . $creation_symbol . "_" . $creation_info;
											$add_chr = "<font style='color:#aaaaaa' title='$cannot_match_error' onclick='click_xadd_button(this,\"$creation_symbol\",\"$creation_info\",\"$ckid\",\"$i\",\"RR\")' id='x_$ckid'>$creation_symbol</font>";
											
											if(!isset($equiv_lists["RR" . $i . "_" . $creation_info])) $equiv_lists["RR" . $i . "_" . $creation_info] = array();
											$equiv_lists["RR" . $i . "_" . $creation_info][] = $ckid;
										}
									}
									echo "<td style='cursor:pointer; $topstyle' align='center' $cellstyle>$add_chr</td>";
								}
								echo "</tr>";
							}
							
							//echo "</td>";
							//for($i=0; $i<count($other_menu_text); $i++)
							//{
							//	echo "<td style='border-bottom:solid 2px #bbbbbb; line-height:14px' align='center' valign='top'>" . $other_menu_text[$i] . "</td>";
							//}
							//echo "</tr>";
						}			
					}					
					
					//------------------------------------------------ End Draw Super Groups Table ------------------------------------------------------------------//
					echo "</table>";
					
					echo "</td></tr></table>";
					
					//echo "<input type='button' value='Submit >>' onclick='document.form1.submit()' />";
					echo "</form>";
					
					echo "<script type='text/javascript'>";
					echo "	postlock = 'off'; ";
					echo "	stimer = false; ";
					echo "	function click_xadd_button(elem,creation_symbol,creation_info,ckid,ri,rname) { ";
					echo "		if(postlock=='on') return; ";
					echo "		if(!rname) rname = 'R'; ";
					echo "		if(elem.innerHTML == creation_symbol) ";
					echo "		{ ";
					echo "			elem.innerHTML = \"<font style=\\\"color:#008800\\\"><b>+\" + creation_symbol + \"</b></font><input type=\\\"hidden\\\" id=\\\"\" + ckid + \"\\\" name=\\\"\" + ckid + \"\\\" value=\\\"xadd_\" + creation_symbol + \"_\" + creation_info + \"\\\" />\"; ";
					echo "			set_equiv_display(ckid,rname + ri + \"_\" + creation_info,\"none\"); ";
					echo "		} ";
					echo "		else ";
					echo "		{ ";
					echo "			elem.innerHTML = creation_symbol; ";
					echo "			set_equiv_display(ckid,rname + ri + \"_\" + creation_info,\"block\"); ";
					echo "		} ";
					echo "		if(stimer) clearTimeout(stimer); ";
					echo "		stimer = setTimeout('auto_submit_duplication_form()',2000); ";
					echo "	} ";
					echo "	function auto_submit_duplication_form() { ";
					echo "		var elemstr = ''; ";
					echo "		var elems = document.form1.elements; ";
					echo "		for(var i=0; i<elems.length; i++) { ";
					echo "			if(elems[i].name!='') { ";
					echo "				if(elems[i].type=='hidden' || elems[i].type=='input' || (elems[i].type=='checkbox' && elems[i].checked)) { ";
					echo "					elemstr += escape(elems[i].name) + '=' + escape(elems[i].value) + '&'; ";
					echo "				} ";
					echo "			} ";
					echo "		} ";
					//echo "		alert(elemstr); ";
					echo "		post_to_url_and_draw_to_div('/cp/index.php?widget=duplicate_menu',elemstr,'duplicate_menu_container'); ";
					echo "	} ";
					echo "	function checkbox_mouseup() { ";
					echo "		if(stimer) clearTimeout(stimer); ";
					echo "		stimer = setTimeout('auto_submit_duplication_form()',2000); ";
					echo "	} ";
					echo "\nequiv_lists = new Array(); ";
					foreach($equiv_lists as $ekey => $elist)
					{
						$elist_arr_str = "";
						for($n=0; $n<count($elist); $n++)
						{
							if($elist_arr_str!="") $elist_arr_str .= ",";
							$elist_arr_str .= "'".$elist[$n]."'";
						}
						echo "\nequiv_lists['$ekey'] = new Array($elist_arr_str); ";
					}
					echo "\nfunction set_equiv_display(ckid,ekey,set_display) { ";
					echo "\n	var eqlist = equiv_lists[ekey]; ";
					echo "\n	for(var i=0; i<eqlist.length; i++) { ";
					echo "\n		if(eqlist[i] != ckid) { ";
					echo "\n			document.getElementById('x_' + eqlist[i]).style.display = set_display; ";
					echo "\n		} ";
					echo "\n	} ";
					echo "\n} ";
					echo "\nfunction post_to_url_and_draw_to_div(strURL,querystr,divid) ";
					echo "\n{ ";
					echo "\n	if(postlock=='on') return; ";
					echo "\n	postlock = 'on'; ";
					echo "\n	document.getElementById(divid).style.opacity = 0.4; ";
					echo "\n 	var xmlHttpReq = false; ";
					echo "\n	var self = this; ";
					echo "\n	if (window.XMLHttpRequest)  { self.xmlHttpReq = new XMLHttpRequest(); } ";
					echo "\n	else if (window.ActiveXObject) { self.xmlHttpReq = new ActiveXObject('Microsoft.XMLHTTP'); } ";
					echo "\n	self.xmlHttpReq.open('POST', strURL, true); ";
					echo "\n	self.xmlHttpReq.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded'); ";
					echo "\n	self.xmlHttpReq.onreadystatechange = function()  ";
					echo "\n	{ ";
					echo "\n		if (self.xmlHttpReq.readyState == 4) ";
					echo "\n		{ ";
					echo "\n			var estr = self.xmlHttpReq.responseText; ";
					echo "\n			document.getElementById(divid).innerHTML = estr; ";
					echo "\n			var xhtml = estr.split(\"<sc\" + \"ript type='text/java\" + \"script'>\"); ";
					echo "\n			for(var n=1; n<xhtml.length; n++) ";
					echo "\n			{ ";
					echo "\n				var xhtml2 = xhtml[n].split('</sc' + 'ript>'); ";
					echo "\n				if(xhtml2.length > 1) ";
					echo "\n				{ ";
					echo "\n					run_xhtml = xhtml2[0]; ";
					echo "\n					eval(run_xhtml); ";
					echo "\n				} ";
					echo "\n			} ";
					echo "\n			postlock = 'off'; ";
					echo "\n			document.getElementById(divid).style.opacity = 1.0; ";
					echo "\n		} ";
					echo "\n	} ";
					echo "\n	self.xmlHttpReq.send(querystr); ";
					echo "\n} ";
					echo "</script>";
					//for($i=0; $i<count($other_loc_list); $i++) echo $other_loc_list[$i] . "<br>";
				}
				if(!isset($_POST['posted_copy']))
				{
					echo "</div>";
				}
			}
		}
	}
?>
