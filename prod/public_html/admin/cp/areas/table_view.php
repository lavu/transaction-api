<?php

	//ini_set('display_errors',1);
	//error_reporting(E_ALL|E_STRICT);
	
	if (!isset($mode)) $mode = "";
	
	if($in_lavu) {
		if($mode=="table_info")
		{
			$tablename = (isset($_GET['tablename']))?$_GET['tablename']:false;
			if($tablename)
			{
				session_start();
				$json_cc = lsecurity_name(admin_info("dataname"),admin_info("companyid"));
				$host = $_SERVER['HTTP_HOST'];
				if ($host != "admin.poslavu.com") $host = "localhost";
				$json_url = $host.'/lib/json_connect.php';
				$json_vars = 'cc='.$json_cc.'&m=4&loc_id='.$locationid.'&table='.urlencode($tablename);
				//echo $json_url . "<br>";
				$ch = curl_init();
				curl_setopt($ch, CURLOPT_URL, $json_url . '?' . $json_vars);
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
				$contents = curl_exec ($ch);
				curl_close ($ch);
				
				$tinfo = lavu_json_decode($contents);
				$tinfo = $tinfo[0];
				//echo "<br><br>" . str_replace("<","&lt;",$contents);
				if($tinfo['json_status']=="success")
				{
					$order_info = $tinfo['order_info'][0];
					echo "<table><td width='140' height='60' align='center' valign='top' style='cursor:default'>";
					if(isset($order_info['order_id'])) echo $order_info['order_id'] . "<br>";
					echo "<b>";
					echo $tablename . "<br>";
					echo $order_info['guests'] . " Guests";
					echo "<br>".speak("Total").": " . $order_info['total'];
					echo "</b>";
					echo "</td></table>";
				}
			}
		}
		else
		{
?>
			<style>
				body, table {
					font-size:10px;
					color:#676767;
				}
			</style>
            <body style='background: transparent'>
                <div id='root'>
                    <div id='table_setup_div' style='position:relative; left:0px; top:0px;'>
                        <table><tr><td id='room_floor' style='width:400px; height:400px;'><table width="100%" height="100%"><td>&nbsp;</td></table></td></tr></table>
                        <div style="position:absolute; left:0px; top:0px; z-index:8080; visibility:hidden" id="info_table_display"><table><td width='120' height='80' bgcolor="#eeeeff" style='border:solid 1px black;filter:alpha(opacity=80);-moz-opacity:.80;opacity:.80' id="info_table_display_content">Order Info</td></table></div>
                    </div>
                </div>
                <?php
                    $json_cc = lsecurity_name(admin_info("dataname"),admin_info("companyid"));
										$host = $_SERVER['HTTP_HOST'];
										if ($host != "admin.poslavu.com") $host = "localhost";
                    $json_url = $host.'/lib/json_connect.php?cc='.$json_cc.'&m=9&loc_id='.$locationid;
                    $ch = curl_init();
                    curl_setopt($ch, CURLOPT_URL, $json_url);
                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                    $contents = curl_exec ($ch);
                    curl_close ($ch);
                    
                    $tlist = "";
                    $cparts = explode("\"open_orders\":\"",$contents);
                    if(count($cparts) > 1)
                    {
                        $cparts = explode("\"",$cparts[1]);
                        $tparts = explode("||",$cparts[0]);
                        for($i=0; $i<count($tparts); $i++)
                        {
														//$tpart = trim($tparts[$i]); // no trim because for some reason some people place spaces in front of the table name
														$tpart = $tparts[$i];
                            if($tpart!="")
                            {
                                if($tlist!="")
                                    $tlist .= ",";
                                $tlist .= "'$tpart'";
                            }
                        }
                    }
                    echo "<script language='javascript'>";
                    echo "table_mode = 'view'; ";
                    echo "y_correct = 0; ";
                    echo "t_shrink = 2; ";
                    echo "t_winwidth = 380; ";
                    echo "t_winheight = 380; ";
                    echo "t_open_tables = new Array($tlist); ";
                    echo "</script>";
                    echo "<script language='javascript' src='resources/table_setup.js'></script>";
                    echo "<script language='javascript'>";
										$rdb = admin_info("database");
										lavu_connect_byid(admin_info("companyid"),$rdb);
										$tbl_query = lavu_query("SELECT * FROM `tables` WHERE `loc_id` = '$locationid'");
                    if(mysqli_num_rows($tbl_query))
                    {
                        $extract = mysqli_fetch_assoc($tbl_query);
                        $set_coord_x = $extract['coord_x'];
                        $set_coord_y = $extract['coord_y'];
                        $set_shapes = $extract['shapes'];
                        $set_widths = $extract['widths'];
                        $set_heights = $extract['heights'];
                        $set_names = $extract['names'];
                        
                        echo "loadTables('$set_coord_x', '$set_coord_y', '$set_shapes', '$set_widths', '$set_heights', '$set_names',t_open_tables); ";
                    }
                    echo "</script>";
                ?>        
            </body>
<?php
		}
	}
?>