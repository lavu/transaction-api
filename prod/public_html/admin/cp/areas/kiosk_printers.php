<?php

if (!$in_lavu)
{
	exit();
}

echo "<br><br>";

$forward_to	= "print_wrap.php?pw_mode=".$pw_mode;
$tablename	= "config";
$filter_by	= "`location` = '".$locationid."' AND `type` = 'kiosk_printer'";
$order_by	= "setting";
$lavu_only	= "<font color='#588604'><b>*</b></font> ";

$printer_settings = array(
	'receipt' => "receipt"
);
for($i = 2; $i <= 50; $i++)
{
	$printer_settings['receipt'.$i] = "receipt".$i;
}


$brands = array();
$command_sets = array(""=>"");
$pr_query = mlavu_query("SELECT * FROM `poslavu_MAIN_db`.`supported_printers` WHERE `_deleted` != '1' AND `brand` like 'Epson%' ORDER BY `_order` asc");
while ($pr_read = mysqli_fetch_assoc($pr_query))
{
	$brand	= $pr_read['brand'];
	$prid	= $pr_read['id'];
	$prname = $pr_read['name'];

	$brands[$brand]['Command Set']['options'][$prid] = $prname ;
	$brands[$brand]['Command Set']['save_type']      = "single_val";
	$brands[$brand]['Command Set']['column']         = "value6";

	$command_sets[$prid] = $prname;
}

$printerSeries = array();
$models_query = mlavu_query("SELECT * FROM `poslavu_MAIN_db`.`printer_models` WHERE `brand` like 'Epson%'");
while ($models_res = mysqli_fetch_assoc($models_query))
{
	$brand = $models_res['brand'];
	$model = $models_res['model'];

	$brands[$brand]['Model']['options'][$model]	= $model;
	$brands[$brand]['Model']['save_type']		= "json";
	$brands[$brand]['Model']['column']			= "value2_long";
	$brands[$brand]['Model']['hidden_vals']		= array( 'language' => "ANK", 'brand' => $brand );

	$printerSeries[$model] = $models_res['printer_series'];
}

$brands['Epson']['Lavu Controller pair mode']['options']['1'] = "Auto";
$brands['Epson']['Lavu Controller pair mode']['options']['0'] = "Manual";
$brands['Epson']['Lavu Controller pair mode']['save_type']    = "combined_json";
$brands['Epson']['Lavu Controller pair mode']['json_key']     = "pair_mode";
$brands['Epson']['Lavu Controller pair mode']['column']       = "value2_long";

$image_capabilities = array(
	'0' => speak("None"),
	'1' => speak("Bit Graphics")." (".speak("High Density").") (SP700)",
	//'2' => speak("Bit Graphics")." (".speak("Fine Density").")." (TSP650)",
	'3' => speak("Raster Graphics")." (TSP650)",
	'4' => "Epson ".speak("Graphics"),
);

$fields = array(
	array(
		speak("Setting").":",
		"setting",
		"select",
		$printer_settings,
		"list:yes"),
	array(
		speak("Name").":",
		"value2",
		"text",
		"",
		"list:yes"
	),
	array(
		speak("IP Address").":",
		"value3",
		"text",
		"",
		"list:yes"
	),
	array(
		speak("Port").":",
		"value5",
		"text",
		"",
		"setvalue:9100"
	),
	array(
		speak("Printer Type").":",
		"value4",
		"printers_dynamic_select",
		$brands,
		"list:yes",
		"label_cell_id:printer_type_label"
	),
	array(
		speak("Characters Per Line").":",
		"value7",
		"text",
		"size:20",
		""
	),
	array(
		speak("Characters Per Line")." (".speak("large font")."):",
		"value8",
		"text",
		"size:20",
		""
	),
	array(
		speak("Line Spacing").":",
		"value9",
		"text",
		"size:20",
		""
	),
	array(
		speak("Image Capability").":",
		"value10",
		"select",
		$image_capabilities
	),
);

if (is_lavu_admin())
{
	$fields[] = array(
		$lavu_only.speak("Skip IP Address Ping").":",
		"value13",
		"bool",
		"",
		"setvalue:1"
	);
}

$fields[] = array(
	speak("Perform status check")." (".speak("Epson or Star only")."):",
	"value11",
	"bool",
	"1"
);

$access_interwrite = (isset($_SESSION['access_interwrite']))?$_SESSION['access_interwrite']:"no";

if (is_lavu_admin() || $access_interwrite=="yes")
{
	$fields = array_merge( $fields, array(
		array(
			$lavu_only."Use ETB flag:",
			"value12",
			"bool"
		),
		array(
			$lavu_only."Inter-write pause:",
			"value14",
			"select",
			"0.25,0.5,0.75,1,1.25,1.5,1.75,2,2.25,2.5,2.75,3"
		)
	));
}

$fields = array_merge( $fields, array(
	// array(
	// 	speak("Print each item on it's own individual ticket").":",
	// 	"value_long",
	// 	"bool"
	// ),
	// array(
	// 	speak("Provide a Grouping Label")." (".speak("for reports")."):",
	// 	"value",
	// 	"text",
	// 	"size:20"
	// ),
	// array(
	// 	speak("Number of times to sound beeper when sending kitchen tickets").":",
	// 	"value15",
	// 	"select",
	// 	array(
	// 		'0' => speak("Disabled"),
	// 		'1' => "1",
	// 		'2' => "2",
	// 		'3' => "3"
	// 	)
	// ),
	array(
		"Location",
		"location",
		"hidden",
		"",
		"setvalue:".$locationid
	),
	array(
		"Type",
		"type",
		"hidden",
		"",
		"setvalue:kiosk_printer"
	),
	array(
		speak("Save"),
		"submit",
		"printer_submit"
	)
));

// Taken from /cp/areas/printers_kds.php
// Checking for post-back.  If is lls, we now need to sync down the config table.
if (!empty($_POST['posted']) && !empty($_POST['setting']) && !empty($_POST['posted_dataname']))
{
	$locinfo = sessvar('location_info');
	schedule_remote_to_local_sync_if_lls($locinfo, $locationid,  "table updated", "config", $sync_value_long="");
}

function run_post_action()
{
	global $locationid;

	lavu_query("DELETE FROM `config` WHERE `location` = '".$locationid."' AND `type`=''");
	$pr_query = lavu_query("SELECT * FROM `config` WHERE `location` ='".$locationid."' AND `type` = 'kiosk_printer' AND `_deleted` != '1'");
	while ($pr_read = mysqli_fetch_assoc($pr_query))
	{
		$vars = array(
			'location'	=> $locationid,
			'type'		=> "",
			'setting'	=> $pr_read['setting'],
			'value'		=> $pr_read['value2']
		);

		lavu_query("INSERT INTO `config` (`location`, `type`, `setting`, `value`) VALUES ('[location]', '[type]', '[setting]', '[value]')", $vars);
	}

	update_api_access_log(admin_info("dataname"), $locationid);
}

$form_max_width = 696;
require(resource_path()."/browse.php");

?>

<script type= 'text/javascript'>

	window.onload = function()
	{
		document.getElementById('zenbox_tab').style.display='none';
	}

	var a = document.getElementsByClassName('content')[0];
	document.getElementsByTagName('body')[0].innerHTML = a.innerHTML;
	document.getElementById('main_content_area').style.width = '600px';
	document.getElementsByTagName("body")[0].style.width = '600px';
	document.getElementsByTagName("body")[0].style.backgroundColor = 'white';
	document.getElementsByTagName("body")[0].style.overflowX = 'hidden';

	function adjustDynamicSelectElements(guide_element, tables, cells)
	{
		var guide_width = document.getElementById(guide_element).offsetWidth;
		var table_target_margin = (0 - guide_width - 7);
		var cell_target_width = (guide_width - 2);

		var table_ids = tables.split(",");
		for (var ti = 0; ti < table_ids.length; ti++)
		{
			var table_id = table_ids[ti];
			document.getElementById(table_id).style.marginLeft = table_target_margin + "px";
		}

		var cell_ids = cells.split(",");
		for (var ci = 0; ci < cell_ids.length; ci++)
		{
			var cell_id = cell_ids[ci];
			document.getElementById(cell_id).style.width = cell_target_width + "px";
		}
	}

	function isSettingReceipt()
	{
		var setting = document.getElementById('setting').value;

		return (setting.indexOf('receipt') >= 0);
	}

	// KIOSK-273
	function epsonModelSetSeries(model)
	{
		var printerSeries = <?= json_encode($printerSeries, JSON_HEX_APOS) ?>;
		//var printerSeries = JSON.parse('<?= json_encode($printerSeries, JSON_HEX_APOS) ?>');

		var hiddenValsInput = document.getElementsByName("hidden_vals");
		if (hiddenValsInput && hiddenValsInput[0]) hiddenValsInput = hiddenValsInput[0];

		if (hiddenValsInput && printerSeries && printerSeries[model])
		{
			// Get hidden_vals JSON
			var hiddenVals = JSON.parse(hiddenValsInput.value);

			// Re-write hidden_vals JSON with series
			hiddenVals.series = printerSeries[model];
			hiddenValsInput.value = JSON.stringify(hiddenVals);
		}
	}

	function epsonModelDidChange(sel)
	{
		var set_lcpm_row_display = "none";
		if (isSettingReceipt())
		{
			if (sel.value == "TM-T88V-i")
			{
				set_lcpm_row_display = "block";
			}
		}

		var lcpm_row = document.getElementById('lcpm_row');
		if (lcpm_row)
		{
			lcpm_row.style.display = set_lcpm_row_display;
		}

		epsonModelSetSeries(sel.value);
	}

	function printer_validate()
	{
		
		if(document.getElementsByName("value2")[0].value=='')
		{
			alert("Printer name required");
			return false;
		}
		else if(document.getElementsByName("value3")[0].value=='')
		{
			alert("IP address required");
			return false;
		}
		else if(document.getElementsByName("printer_type")[0].value=='')
        {
                alert("Printer type required");
                return false;
        }
		else if(document.getElementsByName("printer_type")[0].value!='' && (document.getElementsByName("command_set")[0].value=='' || document.getElementsByName("model")[0].value==''))
        {
                alert("Command and model required");
                return false;
        }

		else
		{
			form1.submit();
		}
	}
</script>
