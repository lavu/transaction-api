<?php
/**
 * This file is meant to host the functions necessary for managing currency interactions
 */

function getConversionRate($currency_code = "", $date = "", $chainID){
    if(empty($currency_code)){
        return 1;
    }
    $databaseIdentifier = "";
    if(!empty($chainID)){

        $PrimaryRestaurantID = getPrimaryChainRestaurant($chainID);

    }
    if($date === ""){
        $date = "00-00-0000";
    }
    $conversionQueryString = "SELECT * FROM `exchange_rates` where `effective_date` = (SELECT max(`effective_date`) FROM `exchange_rates` where `effective_date` <='".$date."' AND `from_currency_id` = '".$currency_code
        ."' AND `primary_restaurantid` = '".$PrimaryRestaurantID."') AND `from_currency_id` = '".$currency_code."' AND `primary_restaurantid` = '".$PrimaryRestaurantID."'";
    $conversionResults = mlavu_query($conversionQueryString);
    if($conversionResults === false) {
        error_log(lavu_dberror());
        return 1;
    }
    if(mysqli_num_rows($conversionResults) > 0) {
        $conversionArray = mysqli_fetch_assoc($conversionResults);
        if (isset($conversionArray['rate'])) {
            return $conversionArray['rate'];
        }
        else if (isset($conversionArray[0]['rate'])) {
            return $conversionArray[0]['rate'];
        }

    }
    else return 1;
}

/*
 * convertCurrency, takes 2 types of currency and a value.  Converts the value from arg[1] to arg[0]
 */
function convertCurrency($convertTo, $convertFrom, $value, $date, $chainID=""){
    if($convertTo === $convertFrom || empty($value)){
        return $value;
    }
    $conversionRateFrom = getConversionRate($convertFrom, $date, $chainID);
    $conversionRateTo = getConversionRate($convertTo, $date, $chainID);
    $intermediateValue = $value * $conversionRateFrom; //get value in terms of common currency
    $finalValue = $intermediateValue / $conversionRateTo; //convert from common denomination into target currency:
    return $finalValue;
}

