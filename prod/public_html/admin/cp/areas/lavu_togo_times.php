<script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"> </script>
<script type="text/javascript">
var tableRows = {};
$(document).ready(function() {
        initRows();
});

function initRows() {
        var count = 0;
        var previousDay = "";
        $(".table-row").each(function() {
                var self = $(this);
                var day = self.data("item");
                if (!tableRows[day]) {
                        tableRows[day] = self.clone();
                }
                if (previousDay !== day) {
                        count = 0;
                }

                self.find("select").each(function() {
                        makeRow(this, self, count);
                });

                previousDay = day;
                count++;
        });
}
function makeRow(element, self, key) {
        var name = $(element).attr("name");
        if (name.indexOf("[") === true) {
                var splitedName = name.split("]");
                name = splitedName[splitedName.length - 2].replace("[", "");
        }
        $(element).attr(
                "name",
                self.data("repeater") +
                        "[" +
                        self.data("item") +
                        "]" +
                        "[" +
                        key +
                        "][" +
                        name +
                        "]"
        );
}

function addNewStartTime(element){
 var day = $(element).data("day");
        addRow(day);
}

function addRow(day) {
        var rows = $(".table-row." + day);
        var lastChild = rows[rows.length - 1];
        var clonedRow = tableRows[day]
                .clone()
                .find("select")
                .val("")
                .end();
        $('[value="' + day + '"]').prop("checked", true);
        clonedRow.find("select").each(function() {
                makeRow(this, clonedRow, rows.length);
        });
        if (!lastChild) {
                clonedRow.insertAfter($(".table-header." + day));
        } else {
                clonedRow.insertAfter(lastChild);
        }
        $(".table-row." + day).show();
        $(".table-row." + day)
                .find("select")
                .attr("disabled", false);
}

function removeTableRow(element) {
        clearErrors($(element)
                        .closest("tr"));
        if (confirm("Are you sure would you like to remove?")) {
                var day = element.data("day");
                $(element)
                        .closest("tr")
                        .remove();
                if (!$(".table-row." + day).length) {
                        $('[value="' + day + '"]').prop("checked", false);
                }
                initRows();
        }
}

function clearErrors(row) {
        row.removeClass('row-error');
        var errorMessageRow = row.prev('tr');
        if (errorMessageRow && errorMessageRow.hasClass('row-error-message')) {
                errorMessageRow.remove();
        }
}

function weekDayChange(element){
	var day = $(element).val();
        if (!$(element).prop("checked")) {
                $(".table-row." + day).hide();
                $(".table-row." + day).removeClass('row-error');
               $('[class="add-time"][data-day="'+day+'"]').attr('disabled', true);
                $('.row-error-message.'+day).remove();
        } else {
                $('[class="add-time"][data-day="'+day+'"]').attr('disabled', false);
                if ($(".table-row." + day).length) {
                        $(".table-row." + day).show();
                } else {
                        addRow(day);
                }
        }
        $(".table-row." + day)
                .find("select")
                .attr("disabled", !$(element).prop("checked"));
}

</script>

<?php
require_once(dirname(__FILE__)."/../resources/core_functions.php");
require_once(resource_path()."/lavuquery.php");
session_start($_GET['session_id']);
$data_name = sessvar('admin_dataname');
$protocol = "https";
if ($_SERVER['HTTP_HOST'] == 'admin.localhost' || $_SERVER['HTTP_HOST'] == 'localhost:8888') {
	$protocol = "http";
}
 
$styleSheetPath = $protocol. "://" . $_SERVER['HTTP_HOST'] . "/cp/styles/styles.css";
$display .= "<html>";
$display .= "<head>";
$display .= "<meta http-equiv=\"Content-Type\" content=\"text/html;charset=ISO-8859-1\">";
$display .= "<link rel='stylesheet' type='text/css' href='".$styleSheetPath."'>";
$display .= "<link rel=\"stylesheet\" href=\"../styles/ios-checkbox.css\" type=\"text/css\" media=\"screen\" charset=\"utf-8\" />
		<link rel=\"stylesheet\" type=\"text/css\" href=\"../styles/styles.css\">
		<link rel=\"stylesheet\" type=\"text/css\" href=\"../styles/info_window.css\">
		<link rel=\"stylesheet\" type=\"text/css\" href=\"../styles/ieStyles.css\">
		<link rel=\"stylesheet\" type=\"text/css\" href=\"../styles/multiaccordion/jquery-ui-1.8.16.greenModifiers.min.css\">
		<link rel=\"stylesheet\" type=\"text/css\" href=\"../styles/multiaccordion/jquery-ui-1.8.16.grayMessages.min.css\">
		<script type=\"text/javascript\" src=\"//ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js\"> </script>
		<script type=\"text/javascript\" src=\"../scripts/multiAccordion.js\"> </script>
		<link rel=\"stylesheet\" type=\"text/css\" href=\"../styles/fontcustom/fontcustom.css\">
		<script type=\"text/javascrpt\" src = \"../scripts/pages/ministries.js\" ></script>
		<script type=\"text/javascript\" src=\"../scripts/customelements.js\"></script>";
if (isset($_GET['refresh_for_save']) && $_GET['refresh_for_save'] == '1') {
	$_GET['refresh_for_save'] = null;
	$display .= "<script type='text/javascript'>window.parent.document.location.reload();</script>";
}
$display .= "</head>";
$display .= "<body style='text-align:center;'>";
$display .= "<form name='frm' id='myForm' method='post' action='' onsubmit='return validateForm()'>";
$display .= "<table class='ltg-table' cellspacing=0 align='center'>";

$dataValues=array();
$query = "select value from config where setting='ltg_start_end_time'";
$data = lavu_query($query);
while($row = mysqli_fetch_assoc($data)){
	$dataValues = $row;
}

$weekDaysTimings = json_decode($dataValues['value'], true);
$weekDays = array("Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday");

foreach ($weekDays as $day) {
	$checked = isset($weekDaysTimings[$day]) ? 'checked' : '';
	$weekDayName = "weekDays[$day]";
	$display.= "<tr class='table-header $day'>";
	$display .="<td colspan='2'><label for='$weekDayName'><input id='$weekDayName' class='week-day $day' type='checkbox' onchange='weekDayChange($(this))'  name='$weekDayName' value=".$day." ".$checked.">".$day."</label></td><td colspan='7'><button type='button' onclick='addNewStartTime($(this))' class='add-time' data-day='$day' ".(!$checked ? 'disabled':'').">+ Add time</button></td></tr>";

	if (isset($weekDaysTimings[$day])) {
		if (isset($weekDaysTimings[$day]['starttime'])) {
			$temp = $weekDaysTimings[$day];
			unset($weekDaysTimings[$day]);
			$weekDaysTimings[$day][0] = $temp;
		}
		
		foreach ($weekDaysTimings[$day] as $data) {
			$display .= getRow($data, $day, $checked);
		}
	}else {
		$display .= getRow([], $day, $checked);
	}
}
$display .= "</table>";
$display .= "<br>";
$display .= "</div>&nbsp;<input type='submit' value='".speak("Save")."' name='submit' class='saveBtn' ><br>&nbsp;</div>";
$display .= "</form>";


if (isset($_POST['submit'])) {
	$restaurantTimings = $_POST['restaurantTimings'];
	$dayTimings = [];
	foreach ($restaurantTimings as $weekDay => $WeekDayTimings) {
		foreach ($WeekDayTimings as $key => $timing) {
			$dayTimings[$weekDay][$key]['starttime'] =  implode(':', [$timing['startHour'], $timing['startMinute'], $timing['startMeridiem']]);
			$dayTimings[$weekDay][$key]['endtime'] =  implode(':', [$timing['endHour'], $timing['endMinute'], $timing['endMeridiem']]);
		}
	}

	$weekJson=json_encode($dayTimings);
	$query = "select `setting` from config where `setting` = 'ltg_start_end_time'";
	$queryResult = lavu_query($query);
	$numRec = mysqli_num_rows($queryResult);

	if($numRec > 0) {
		$query = "update config set value='$weekJson' where setting='ltg_start_end_time'";
	} else {
		$locationId = sessvar("locationid");
		$query = "INSERT INTO `config` (`location`, `type`, `setting`, `value`) VALUES ('$locationId', 'location_config_setting', 'ltg_start_end_time', '$weekJson')";
	}
	lavu_query($query);
	
	unset($_POST);
	header("Location:".$_SERVER['REQUEST_URI']. "&refresh_for_save=1");
}

echo $display;

function getNumberOptions($generatTill = 12, $selected = '', $startAt = 1) {
	$options = '';
	for ($i=$startAt;$i<=$generatTill;$i++) {
		if ($i<10) {
			$options.="<option value='0".$i."' ".((int)$selected === (int)$i ? 'selected' : '').">0$i</option>";
		} else {
			$options.="<option value='".$i."' ".((int)$selected === (int)$i ? 'selected' : '').">$i</option>";
		}
	}

	return $options;
}

function getRow($data, $day, $checked) {
	$row = '';
	$startingTimes = isset($data['starttime']) ? explode(':',$data['starttime']) : [];
	$endingTimes = isset($data['endtime']) ? explode(':',$data['endtime']) : [];
	$weekDayName = "weekDays[$day]";
	$isChecked = trim($checked) !== '' ? true : false;
	$isDisabled = !$isChecked ? 'disabled="disabled"' : '';
	$isHidden = !$isChecked ? 'display:none' : '';

	$row.="<tr class='table-row $day' onclick='clearErrors($(this))' data-repeater='restaurantTimings' data-item='$day' style='$isHidden'>";
	$row.="<td>Start time:</td>";
	$row.="<td><select name='startHour' class='startHour' $isDisabled>";
	$row.= getNumberOptions(12, isset($startingTimes[0]) ? $startingTimes[0] : '');

	$row.="</select>";
	$row.="</td>";
	$row.="<td><select name='startMinute' class='startMinute' $isDisabled>";
	$row.= getNumberOptions(59, isset($startingTimes[1]) ? $startingTimes[1] : '', 0);
	$row.="</select>";
	$row.="</td>";
	$row.="<td><select name='startMeridiem' class='startMeridiem' $isDisabled>";
	$row.="<option value='am' ".(isset($startingTimes[2]) && $startingTimes[2] === 'am'? 'selected' : '')." >am</option>";
	$row.="<option value='pm' ".(isset($startingTimes[2]) && $startingTimes[2] === 'pm'? 'selected' : '')." >pm</option>";
	$row.= "</select>";
	$row.= "</td>";
	$row.="<td>End time:</td>";
	$row.="<td><select name='endHour' class='endHour' $isDisabled>";
	$row.= getNumberOptions(12,isset($endingTimes[0]) ? $endingTimes[0] : '');
	$row.="</select>";
	$row.="</td>";
	$row.="<td><select name='endMinute' class='endMinute' $isDisabled>";
	$row.= getNumberOptions(59,isset($endingTimes[1]) ? $endingTimes[1] : '', 0);
	$display.="</select>";
	$row.="</td>";
	$row.="<td><select name='endMeridiem' class='endMeridiem' $isDisabled>";
	$row.="<option value='am' ".(isset($endingTimes[2]) && $endingTimes[2] === 'am'? 'selected' : '').">am</option>";
	$row.="<option value='pm' ".(isset($endingTimes[2]) && $endingTimes[2] === 'pm'? 'selected' : '').">pm</option>";
	$display.= "</select>";
	$row.= "</td><td><button onclick='removeTableRow($(this))' data-day='$day' class='remove-time' type='button'>X</button></td>";
	$row.="</tr>";
	return $row;
}
?>
<script type="text/javascript">

function validateForm() {
	var status = true;
	let weekDayRows = {};
	$(".table-row").each(function() {
		var day = $(this).data("item");
		if (!weekDayRows[day]) {
			weekDayRows[day] = [];
		}
		weekDayRows[day].push({
			startTime: convertTime12to24(
				`${$(this)
					.find(".startHour")
					.val()}:${$(this)
					.find(".startMinute")
					.val()} ${$(this)
					.find(".startMeridiem")
					.val()}`
			),
			endTime: convertTime12to24(
				`${$(this)
					.find(".endHour")
					.val()}:${$(this)
					.find(".endMinute")
					.val()} ${$(this)
					.find(".endMeridiem")
					.val()}`
			)
		});
	});
	for (let day in weekDayRows) {
		for (let i in weekDayRows[day]) {
			var daysId = 'weekDays['+day+']';
			var isChecked = $('.'+day).is(':checked');
			if (!isValidTimeRange(weekDayRows[day][i]) && isChecked) {
				if (status === true) {
					status = false;
				}
				var row = $($(".table-row." + day)[i]);
				clearErrors(row);
				var errorRow = $("<tr>");
				errorRow.addClass("row-error-message");
				errorRow.addClass(day);
				errorRow.append(
					'<td colspan="9">End time must be greater than Start time.</td>'
				);
				row.addClass("row-error");
				$(errorRow).insertBefore(row);
			} else {
				for (
					let j = parseInt(i) + 1;
					j < weekDayRows[day].length;
					j++
				) {
					if (
						!isInTimeRange(weekDayRows[day][i], weekDayRows[day][j])
					) {
						if (status === true) {
							status = false;
						}
						var row = $($(".table-row." + day)[j]);
						clearErrors(row);
						row.addClass("row-error");
						var errorRow = $("<tr>");
						errorRow.addClass("row-error-message");
						errorRow.addClass(day);
						errorRow.append(
							'<td colspan="9">Overlapping time range.</td>'
						);
						$(errorRow).insertBefore(row);
					}
				}
			}
		}
	}
	return status;
}

function isInTimeRange(object1, object2) {
	return (
		// object 2 startime must be greater/equal to that of object 1
		// i.e first object 9 then second object can start 9 onwards
		object2.startTime.hours >= object1.endTime.hours ||
		// object 2 endTime must be less than that of object 1
		// i.e 0 <= 1
		object2.endTime.hours < object1.startTime.hours
	);
}

function isValidTimeRange(object) {
	return (
		object.endTime.hours > object.startTime.hours ||
		(object.endTime.hours === object.startTime.hours &&
			object.endTime.minutes > object.startTime.minutes)
	);
}

function convertTime12to24(timeWithMeridian, type) {
	if (timeWithMeridian) {
		let [hours, minutes, meridian] = timeWithMeridian
			.replace(" ", ":")
			.split(":");
		type = type || true;
		meridian = meridian ? meridian.toLowerCase() : "";

		if (hours === "12") {
			hours = 0;
		}

		if (meridian === "pm") {
			hours = parseInt(hours, 10) + 12;
		}
		hours = parseInt(hours);
		minutes = parseInt(minutes);

		if(hours === 0 && minutes === 0)
		{
			hours = 24;
		}
		return type === true ? { hours, minutes } : hours + ":" + minutes;
	} else {
		return "Not a valid Time";
	}
}
</script>
 

