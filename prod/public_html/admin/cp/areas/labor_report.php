<?php
if(!function_exists("lbr_display_money"))
{
	function lbr_date_add($d)
	{
		return date("Y-m-d H:i:s",(strtotime($d) + (24 * 60 * 60)));
	}
	function lbr_date_start_datetime()
	{
		global $location_info;
		$current_date = date("Y-m-d");
		$dmins = ($location_info['day_start_end_time']*1)%100;
		$dhours = (($location_info['day_start_end_time']*1)-$dmins)/100;
		$dstart_time = date("H:i:s",mktime($dhours, $dmins, 0,1,1,2000));
		$day_start_datetime = $current_date . " ". $dstart_time;
		$day_end_datetime  = date("Y-m-d",strtotime("+1 day")). " ". $dstart_time;
		return $day_start_datetime;
	}
	function lbr_date_subtract($dt,$d)
	{
		$dtparts = explode(" ",$dt);
		if(count($dtparts)==2)
		{
			$d_parts = explode("-",$dtparts[0]);
			$t_parts = explode(":",$dtparts[1]);
			if(count($d_parts)==3 && count($t_parts)==3)
			{
				$setdt = date("Y-m-d H:i:s",mktime($t_parts[0],$t_parts[1],$t_parts[2],$d_parts[1],$d_parts[2] * 1 - $d * 1,$d_parts[0]));
				//echo $dt . " - " . $d . " = " . $setdt . "<br>";
				return $setdt;
			}
		}
		return $dt;
	}
	function lbr_display_money($v,$color="")
	{
		global $mon_left_sym;
		global $mon_right_sym;
		global $mon_comma;
		global $mon_decimal;
		global $mon_no_of_decimals;

		//$leading_char = "$";
		//$dec_places = 2;

		$v = str_replace(",","",str_replace(@$leading_char,"",$v)) * 1;
		//$str = $leading_char . number_format($v,$dec_places);
		$str = trim($mon_left_sym) . number_format($v, $mon_no_of_decimals, $mon_decimal, $mon_comma) . trim($mon_right_sym);

		if($color=="data")
		{
			$dec_places = $mon_no_of_decimals;
			//return number_format($v,$dec_places,".","");
			return sprintf('%0.2f',$v);
		}
		else if($color!="")
		{
			if($v * 1 > 0) $str = "<font style='color:$color'>$str</font>";
			else $str = "<font style='color:#aaaaaa'>$str</font>";
		}
		return $str;
	}
	function lbr_showval($v,$mode="display")
	{
		if(substr($v,0,7)=="[money]")
		{
			if($mode=="actual") return str_replace(",","",substr($v,7));
			$v = lbr_display_money(substr($v,7));
		}
		if(substr($v,0,5)=="time(")
		{
			$v_parts = explode(")",substr($v,5),2);
			if(count($v_parts)==2)
			{
				$tdisplay = trim($v_parts[0]);
				if($mode=="actual") return trim($v_parts[1]);
				$v = date($tdisplay,strtotime(trim($v_parts[1])));
			}
		}
		return $v;
	}
	function lbr_val($v)
	{
		return lbr_showval($v,"actual");
	}
	function lbr_output_row_header()
	{
		$str = "";
		$str .= "<tr style='background-color:#dddddd'>";
		$str .= "<td>&nbsp;</td>";
		$str .= "<td>".speak("Sales")."*</td>";
		$str .= "<td>".speak("Labor Hours")."</td>";
		$str .= "<td>".speak("Est Labor Cost")."**</td>";
		$str .= "<td>&nbsp;</td>";
		$str .= "<td>&nbsp;</td>";
		$str .= "</tr>";

		return $str;
	}

	function lbr_output_row_vars($rowtitle,$vars,$type="html")
	{
		$hours_min_str = ""; $hours = 0; $minutes = 0;
		if($vars['sales_total'] * 1 > 0 && $vars['labor_cost'] * 1 > 0)
		{
			//$labor_calc = number_format(($vars['labor_cost'] * 1 / $vars['sales_total'] * 1) * 100,2) . "%";
			$labor_calc = sprintf('%0.2f', ($vars['labor_cost'] * 1 / $vars['sales_total'] * 1) * 100) . "%";
		}
		else if($vars['sales_total'] * 1 > 0)
		{
			$labor_calc = "0%";
		}
		else if($vars['labor_cost'] * 1 > 0)
		{
			$labor_calc = "---";
		}
		else
		{
			$labor_calc = "";
		}

		$data = array("title"=>$rowtitle,"sales_total"=>lbr_display_money($vars['sales_total'],"data"),"labor_hours"=>$vars['labor_hours'],"labor_cost"=>lbr_display_money($vars['labor_cost'],"data"),"labor_calc"=>$labor_calc);
		$datastr = "";
		foreach($data as $dkey => $dval)
		{
			if($datastr!="") $datastr .= ",";
			$datastr .= $dval;
		}
		$datastr .= "\n";
		//$datastr = $rowtitle . "," . lbr_display_money($vars['sales_total'],"data") . "," . $vars['labor_hours'] . "," . lbr_display_money($vars['labor_cost'],"data") . "," . $labor_calc . "\n";

		$expandid = "exp_" . str_replace(" ","_",strtolower(trim($rowtitle)));
		//	var_dump($vars['labor_hours']);
		$str = "";
		$str .= "<tr>";
		$str .= "<td>" . $rowtitle . "</td>";
		$str .= "<td align='right'>" . lbr_display_money($vars['sales_total'],"#007700")  . "</td>";
		//			convertToHoursMins(;
		//$hours = floor($vars['labor_hours'] / 60);
		//$minutes = ($vars['labor_hours'] % 60);
		//$minutes = sprintf("%02d", $minutes);
		$hours_min_str='';
		if(isset($_GET['format']) && 'decimalized' === $_GET['format']){
			$hours_min_str = format_currency($vars['labor_hours'], true );
		} else {
			$hours = floor($vars['labor_hours']);
			$minutes = ($vars['labor_hours'] * 60) % 60;
			$minutes = sprintf("%02d", $minutes);
			$hours_min_str = $hours." : ".$minutes;
		}
		$str .= "<td align='right'>" . $hours_min_str . "</td>";
		$str .= "<td align='right'>" . lbr_display_money($vars['labor_cost'],"#000077")  . "</td>";
		$str .= "<td align='right'>" . $labor_calc . "</td>";
		//$str .= "<td align='right'>" . lbr_display_money($vars['ingredient_cost'])  . "</td>";
		$str .= "<td>";

		if($vars['labor_hours'] * 1 > 0 || $vars['sales_total'] * 1 > 0)
		{
			$str .= "<input type='button' style='cursor:pointer; border-radius:4px; border:solid 1px #440088; background-color:#ddd8ee; font-size:10px; padding:2px; padding-left:20px; padding-right:20px' value='" . speak("Expand") . " >>' onmousedown='if(document.getElementById(\"$expandid\").style.display==\"table-row\") { document.getElementById(\"$expandid\").style.display = \"none\"; this.value = \"Expand >>\" } else { document.getElementById(\"$expandid\").style.display = \"table-row\"; this.value = \"<< Minimize\" } return false' style='cursor:pointer' />";
		}
		else $str .= "&nbsp;";

		$str .= "</td>";
		$str .= "</td>";
		$str .= "</tr>";

		$str .= "<tr id='$expandid' style='display:none'><td colspan='12' align='center'>";
		$str .= "<table style='border:solid 1px #aaaaaa' bgcolor='#ffffff' cellpadding=6><tr><td>";
		if(count($vars['labor_info']) < 1)
		{
			$str .= "no labor during this timespan";
		}
		else
		{
			$str .= "<table>";
			foreach($vars['labor_info'] as $lkey => $lvars)
			{
				$labor_info_vars = $lvars;
				if(isset($labor_info_vars['hour']) && $labor_info_vars['hour'] * 1 > 0)
				{
					//$hours = floor($labor_info_vars['hour'] / 60);
					//$minutes = ($labor_info_vars['hour'] % 60);
					//$minutes = sprintf("%02d", $minutes);
					//$hours_min_str = $hours." : ".$minutes;
					$hours_min_str='';
					if(isset($_GET['format']) && 'decimalized' === $_GET['format']){
						$hours_min_str = format_currency($labor_info_vars['hour'], true );
					} else {
						$hours = floor($labor_info_vars['hour']);
						$minutes = ($labor_info_vars['hour'] * 60) % 60;
						$minutes = sprintf("%02d", $minutes);
						$hours_min_str = $hours." : ".$minutes;
					}
						
					$str .= "<tr>";
					$str .= "<td align='right'>" . $labor_info_vars['server'] . "</td>";
					$str .= "<td align='right'>" . $hours_min_str . "</td>";
					$str .= "<td align='right'>" . lbr_display_money($labor_info_vars['pay'],"#0000aa") ."</td>";
					$str .= "</tr>";
				}
			}
			$str .= "</table>";
		}
		$str .= "</td></tr></table>";
		$str .= "</td></tr>";

		if($type=="data") return $data;
		else if($type=="datastr") return $datastr;
		else if($type=="all") return array("html"=>$str,"data"=>$data,"datastr"=>$datastr);
		else return $str;
	}
}

//------------------------------- Start Code --------------------------------------//
// datetime selector
require_once(dirname(__FILE__) . "/../reports_v2/report_functions.php");
require_once(dirname(__FILE__) . "/../reports_v2/area_reports.php");

global $location_info;
$mon_left_sym = "$ ";
$mon_right_sym = "";
$mon_comma = ",";
$mon_decimal = ".";
$mon_no_of_decimals = 2;
if(!isset($location_info) || !is_array($location_info) || count($location_info) < 1)
{
	$loc_query = lavu_query("select * from `locations` limit 1");
	while($loc_read = mysqli_fetch_assoc($loc_query))
	{
		$location_info = $loc_read;
	}
}
$formats = $location_info;
if($data_name=="pomme_bistro")
{
	//foreach($formats as $k => $v) echo $k . " = " . $v . "<br>";
}
if($formats['left_or_right'] == 'left')
{
	$mon_left_sym = $formats['monitary_symbol'] . " ";
	$mon_right_sym = "";
}
else
{
	$mon_right_sym = " " . $formats['monitary_symbol'];
	$mon_left_sym = "";
}
if($formats['decimal_char'] == ',')
{
	$mon_comma = ".";
	$mon_decimal = ",";
}
$mon_no_of_decimals = $formats['disable_decimal'] * 1;

$timezone = $location_info['timezone'];
if(strlen($timezone) > 2)
{
	date_default_timezone_set($timezone);
}

$arep_vars = array("base_url"=>"/cp/areas/labor_report.php");
$arep = new area_reports($arep_vars);
$arep->report_id = "autodirect:labor_report";
if(strpos($_SERVER['REQUEST_URI'],"widget")===false) $_GET['mode'] = "day";
$arep->report_selection = "Preset Date Ranges";
$arep_result = $arep->datetime_selector();
$day_start_datetime = $arep_result['vars']['start_datetime'];
$day_end_datetime = $arep_result['vars']['end_datetime'];

if(isset($_GET['run_report_start_datetime']) && isset($_GET['run_report_end_datetime']))
{
	$day_start_datetime = $_GET['run_report_start_datetime'];
	$day_end_datetime = $_GET['run_report_end_datetime'];
}

$lbrcontrols = $arep_result['output'];
$lbrcontrols = str_replace("</tr>","<td valign='top' align='left'>[export_button]</td></tr>",$lbrcontrols);
if(isset($_GET['format']))
{
	$lbrcontrols = str_replace("/cp/areas/labor_report.php?&mode=day","/cp/areas/labor_report.php?&mode=day&format=decimalized",$lbrcontrols);
	$lbrcontrols = str_replace("/cp/areas/labor_report.php?&mode=month","/cp/areas/labor_report.php?&mode=month&format=decimalized",$lbrcontrols);
	$lbrcontrols = str_replace("/cp/areas/labor_report.php?&mode=year","/cp/areas/labor_report.php?&mode=year&format=decimalized",$lbrcontrols);
}
// end datetime selctor

$special_message = "";
if(strtotime($day_end_datetime) - strtotime($day_start_datetime) > 60 * 60 * 24 * 45)
{
	$special_message = "Sorry, cannot run this report for more than a 45 day span";
	$day_start_datetime = date("Y-m-d") . " 00:00:00";
	$day_end_datetime = date("Y-m-d") . " 00:00:01";
}

$current_date = date("Y-m-d");
$dmins = ($location_info['day_start_end_time']*1)%100;
$dhours = (($location_info['day_start_end_time']*1)-$dmins)/100;
$dstart_time = date("H:i:s",mktime($dhours, $dmins, 0,1,1,2000));
//$day_start_datetime = $current_date . " ". $dstart_time;
//$day_end_datetime  = date("Y-m-d",strtotime("+1 day")). " ". $dstart_time;
$day_start_ts = strtotime($day_start_datetime);
$day_end_ts = strtotime($day_end_datetime);

$day_start_datetime_check = date("Y-m-d H:i:s",strtotime($day_start_datetime) - (60 * 60 * 24));

$vars = array();
$vars['loc_id'] = $location_info['id'];
$vars['min_datetime'] = $day_start_datetime;
$vars['min_datetime_check'] = $day_start_datetime_check;
$vars['max_datetime'] = $day_end_datetime;

$is_multi_day = false;
if(strtotime($day_end_datetime) - strtotime($day_start_datetime) > 60 * 60 * 28)
{
	$is_multi_day = true;
}

$hinfo = array(); // For Hours
for($h=0; $h<24; $h++)
{
	$h_ts = $day_start_ts + (60 * 60 * $h);
	$h_index = date("Y-m-d H",$h_ts);
	$h_datetime = date("Y-m-d H",$h_ts) . ":00:00";
	$h_max_datetime = date("Y-m-d H",$h_ts) . ":59:59";

	$hinfo[$h_index] = array("sales_total"=>0,"labor_hours"=>0,"labor_cost"=>0,"labor_info"=>array(),"ingredient_cost"=>0);
}
$day_info = array(); // For Days
$day_loop = substr($day_start_datetime,0,10);
$end_day_loop = substr($day_end_datetime,0,10);
$starting_labor_info = array("sales_total"=>0,"labor_hours"=>0,"labor_cost"=>0,"labor_info"=>array(),"ingredient_cost"=>0);
if($end_day_loop >= $day_loop)
{
	while($day_loop < $end_day_loop)
	{
		$day_info[substr($day_loop,0,10)] = $starting_labor_info;

		// LP-341 -- Fixed infinite loop caused by manually adding a day's worth of seconds to timestamps not working due to timezone differences
		$day_loop = date("Y-m-d",strtotime($day_loop .' +1 day'));
	}
}
$rinfo = array(); // For Entire Range
$rinfo['labor_info'] = $starting_labor_info;

$str = "";

$sales_query = lavu_query("select concat('time(h:ia)',date_format(order_contents.device_time, '%Y-%m-%d %H:00')) as item_added_hour, sum(order_contents.quantity) as quantity, if(count(distinct orders.ioid )>count( distinct orders.order_id ),count( distinct orders.ioid ),count( distinct orders.order_id )) as order_count, if(count(*) > 0, concat('[money]',format(sum((order_contents.price + order_contents.modify_price + order_contents.forced_modifiers_price)*order_contents.quantity - if( order_contents.quantity > '0', order_contents.itax, 0) ),2)), '[money]0.00') as gross, if(count(*) > 0, concat('[money]',format(sum(if(order_contents.quantity > 0, order_contents.discount_amount + order_contents.idiscount_amount, 0)),2)), '[money]0.00') as discount, if(count(*) > 0, concat('[money]',format(sum((order_contents.price + order_contents.modify_price + order_contents.forced_modifiers_price)*order_contents.quantity - if( order_contents.quantity > '0',order_contents.discount_amount + order_contents.idiscount_amount + order_contents.itax, 0)),2)), '[money]0.00') as net, if(count(*) > 0, concat('[money]',format(sum( if( order_contents.quantity != 0 AND order_contents.tax_exempt != '1', `order_contents`.`tax1` + `order_contents`.`tax2` + `order_contents`.`tax3` + `order_contents`.`tax4` + `order_contents`.`tax5` - `order_contents`.`itax`, 0 ) ),2)), '[money]0.00') as order_tax, if(count(*) > 0, concat('[money]',format(sum( if( order_contents.quantity != 0 AND order_contents.tax_exempt != '1', `order_contents`.`itax`, 0) ),2)), '[money]0.00') as included_tax, concat('time(h:ia)',date_format(order_contents.device_time, '%Y-%m-%d %H:00')) as `group_col_date_formatted` from `orders`LEFT JOIN `order_contents` ON `orders`.`order_id` = `order_contents`.`order_id` AND `orders`.`location_id` = `order_contents`.`loc_id` where orders.opened >= '[min_datetime]' and orders.opened < '[max_datetime]' and orders.void = '0' and orders.location_id='[loc_id]' and order_contents.item != 'SENDPOINT' group by concat('time(h:ia)',date_format(order_contents.device_time, '%Y-%m-%d %H:00'))",$vars);
$day_wise_loop = substr($day_start_datetime,0,13);
$next_day = date("Y-m-d H",strtotime($day_start_datetime .' +1 day'));
$dinfo = array();
while($sales_read = mysqli_fetch_assoc($sales_query))
{
        $h_ts = strtotime(lbr_val($sales_read['item_added_hour']));
        $h_index = date("Y-m-d H",$h_ts);
        $d_index = date("Y-m-d H",$h_ts);
        if(isset($hinfo[$h_index]))
        {
                $hinfo[$h_index]['sales_total'] += lbr_val($sales_read['net']);
        }
        if($day_wise_loop < $d_index){
        				if($day_wise_loop < $next_day){
        						$index = $day_wise_loop;
                                $day_index = substr($index,0,10);
                                $dinfo[trim($day_index)]['sales_total'] += lbr_val($sales_read['net']);
                        }else{
                                $day_index = substr($d_index,0,10);
                                $next_day = date("Y-m-d H",strtotime($day_wise_loop.' +1 day'));
                                $dinfo[trim($day_index)]['sales_total'] += lbr_val($sales_read['net']);
                        }
        }
        if(isset($hinfo[$h_index]) || isset($dinfo[substr($d_index,0,10)])){
                $rinfo['sales_total'] += lbr_val($sales_read['net']);
        }
        
        $day_wise_loop = $d_index;
                        // cols: item_added_hour, quantity, order_count, gross, discount, net, order_tax, included_tax
                        //foreach($sales_read as $k => $v) $str .=$k . " = " . lbr_showval($v) . "<br>";
                        //$str .="<br>";
}
foreach($day_info as $key => $val){
	if(!array_key_exists($key, $dinfo)){
		$dinfo[$key]['sales_total'] = "0.00";
	}
}
ksort($dinfo);
$min_week_ago = date("Y-m-d",strtotime($vars['min_datetime']) - (60 * 60 * 24 * 7)) . " 00:00:00";
$vars['min_week_ago'] = $min_week_ago;

$clock_hours = array();
$clock_query = lavu_query("select * from `clock_punches` where `time`>='[min_datetime_check]' and `server_time`!='' and `_deleted`!='1' and (`time_out`='' or `time_out`>'[min_datetime]') and `time`<'[max_datetime]' and `location_id`='[loc_id]' and `server`!='' and `punch_type`='Shift' order by `server_time` asc",$vars);
while($clock_read = mysqli_fetch_assoc($clock_query))
{
	$punch_start_ts = strtotime($clock_read['time']);
	if($clock_read['time_out']=="") $punch_end_ts = time();
	else $punch_end_ts = strtotime($clock_read['time_out']);

	//$str .= date("Y-m-d H:i:s",$punch_start_ts) . " - " . date("Y-m-d H:i:s",$punch_end_ts) . "<br>";
	if($punch_end_ts > $punch_start_ts && $punch_end_ts < $punch_start_ts + (60 * 60 * 24 * 7))
	{
		for($t=$punch_start_ts; $t<=$punch_end_ts+(60*60); $t+=(60*60))
		{
			$hour_start_datetime = date("Y-m-d H",$t) . ":00:00";
			$hour_end_datetime = date("Y-m-d H",$t) . ":59:59";
			$hour_start_ts = strtotime($hour_start_datetime);
			$hour_end_ts = strtotime($hour_end_datetime);

			if($hour_start_ts > $punch_end_ts)
			{
				$hour_add = 0;
			}
			else if($hour_end_ts < $punch_start_ts)
			{
				$hour_add = 0;
			}
			else if($punch_start_ts <= $hour_start_ts && $punch_end_ts >= $hour_end_ts)
			{
				$hour_add = 1;
			}
			else if($punch_start_ts <= $hour_start_ts && $punch_end_ts < $hour_end_ts)
			{
				$hour_add = number_format(($punch_end_ts - $hour_start_ts) / 60 / 60,2,".","");
				if($hour_add * 1 < 0)
				{
					echo date("Y-m-d H:i:s",$punch_start_ts) . " - " . date("Y-m-d H:i:s",$punch_end_ts) . "<br>";
				}
			}
			else if($punch_start_ts > $hour_start_ts && $punch_end_ts >= $hour_end_ts)
			{
				$hour_add = number_format(($hour_end_ts - $punch_start_ts) / 60 / 60,2,".","");
			}
			else if($punch_start_ts > $hour_start_ts && $punch_end_ts < $hour_end_ts)
			{
				$hour_add = number_format(($punch_end_ts - $punch_start_ts) / 60 / 60,2,".","");
			}
			else
			{
				$hour_add = 0;
			}
			//$cost_add = $clock_read['payrate'] * $hour_add;
			if ($clock_read['payrate'] == "")
			{
				$cost_query = lavu_query("SELECT * FROM `users` WHERE id = '[server_id]'", $clock_read);
				while ($cost_read = mysqli_fetch_assoc($cost_query))
				{
					$for_min = $cost_read['payrate'];
					$cost_add = $for_min * $hour_add;
				}
			}else{

				$for_min = $clock_read['payrate'];
				$cost_add = $for_min * $hour_add;
			}

			$hindex = date("Y-m-d H",$t);
			$dindex = date("Y-m-d",$t);

			$server_id = $clock_read['server_id'];
			$labor_info = array("server"=>$clock_read['server'],"server_id"=>$server_id,"hour"=>$hour_add,"pay"=>$cost_add);

			if(isset($hinfo[$hindex]))
			{
				$hinfo[$hindex]['labor_hours'] += $hour_add * 1;
				$hinfo[$hindex]['labor_cost'] += $cost_add * 1;
				if(isset($hinfo[$hindex]['labor_info'][$server_id]))
				{
					$hinfo[$hindex]['labor_info'][$server_id]['hour'] += $hour_add * 1 ;
					$hinfo[$hindex]['labor_info'][$server_id]['pay'] += $cost_add * 1;
				}
				else
					$hinfo[$hindex]['labor_info'][$server_id] = $labor_info;
			}

			if(isset($dinfo[$dindex]))
			{
				$dinfo[$dindex]['labor_hours'] += $hour_add * 1;
				$dinfo[$dindex]['labor_cost'] += $cost_add * 1;
				if(isset($dinfo[$dindex]['labor_info'][$server_id]))
				{
					$dinfo[$dindex]['labor_info'][$server_id]['hour'] += $hour_add  * 1;
					$dinfo[$dindex]['labor_info'][$server_id]['pay'] += $cost_add * 1;
				}
				else
					$dinfo[$dindex]['labor_info'][$server_id] = $labor_info;

					$rinfo['labor_hours'] += $hour_add * 1;
					$rinfo['labor_cost'] += $cost_add * 1;
					if(isset($rinfo['labor_info'][$server_id]['hour']))
					{
						$rinfo['labor_info'][$server_id]['hour'] += $hour_add * 1;
						$rinfo['labor_info'][$server_id]['pay'] += $cost_add * 1;
					}
					else
						$rinfo['labor_info'][$server_id] = $labor_info;
			}
			//$str .= date("Y-m-d H",$t) . " - $hour_add x " . $clock_read['payrate'] . " - $cost_add<br>";
		}
	}
	// cols: id,location,location_id,punch_type,server,server_id,time,hours,punched_out
	//		  time_out,server_time,server_time_out,punch_id,udid_in,udid_out,ip_in,ip_out
	//		  notes,_deleted,break,register,role_id,payrate,regular_time_seconds
	//		  overtime_seconds,double_time_seconds,needs_calculation
	//foreach($clock_read as $k => $v) $str .=$k . " = " . lbr_showval($v) . "<br>";
	//$str .="<br>";
}

// based on date format setting
$date_setting = "date_format";
$location_date_format_query = lavu_query( "SELECT `location`, `setting`, `value` FROM `config` WHERE `setting` = '[1]'",  $date_setting);
if( !$location_date_format_query || !mysqli_num_rows($location_date_format_query)){
	$date_format = array("value" => 2);
}else
	$date_format = mysqli_fetch_assoc( $location_date_format_query );

switch ($date_format['value']){
	case 1:$dateFormat = 'd/m/Y';
	break;
	case 2:$dateFormat = 'm/d/Y';
	break;
	case 3:$dateFormat = 'Y/m/d';
	break;
}

$header_output = "";
$ouput = "";

$header_output .= "<script type=\"text/javascript\" src=\"//ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js\"> </script>";
$header_output .= "<script>var cpDateFormatSetting='$dateFormat';</script><script language='javascript' src='reports_v2/ajax_lib.js'></script>
			<script language='javascript' src='reports_v2/floating_window.js'></script>
			<script type='text/javascript' src='resources/tigra/tcal.js'></script>
			<script type='text/javascript' src='reports_v2/chart.js'></script>
			<link rel='stylesheet' type='text/css' href='resources/tigra/tcal.css' />
			<link rel='stylesheet' type='text/css' href='styles/reports_v2.css' />";
$header_output .= "<script language='javascript' src='reports_v2/reports_v2.js'></script>";
$header_output .= "<div id='report_container'>";

//foreach($lbrcontrols as $k => $v) echo "lbr $k<br>";
//foreach($_GET as $k => $v) echo "get $k <br>";
$export_output = "<button class='rounded_button right export' onclick='window.location = \"/cp/index.php?widget=reports/v2_entry&render_frame=reports.display&report=autodirect:labor_report&/cp/areas/labor_report.php&mode=".$lbrcontrols['mode']."&run_report_start_datetime=$day_start_datetime&run_report_end_datetime=$day_end_datetime&output_mode=export\"' /><div class='icon'></div> Export To CSV</button><br>";

$header_output .= "<table><tr><td align='center' valign='bottom'>";
$header_output .= str_replace("[export_button]",$export_output,$lbrcontrols);
$header_output .= "</td></tr></table>";
$output_mode = (isset($_GET['output_mode']))?$_GET['output_mode']:"";

$datastr = "";
$data = array();
$output = "";

if($output_mode=="run_report"){
	$date_setting = "date_format";
	$location_date_format_query = lavu_query( "SELECT `location`, `setting`, `value` FROM `config` WHERE `setting` = '[1]'",  $date_setting);
	if( !$location_date_format_query || !mysqli_num_rows($location_date_format_query)){
		$date_format = array("value" => 2);
	}else{
		$date_option = mysqli_fetch_assoc( $location_date_format_query );
		$date_format = $date_option['value'];
	}
	$output .="<input type='button' align='right' value='".speak("Print By the Day")."' onclick='window.location = \"_DO:cmd=print&filename=management/labor_report_print.php&m=print&dn=$dataname&date_format=$date_format&loc_id=".($location_info['id'])."&register=receipt&vars=report_type=type(eq)by_the_day\"'><br />";
}

if($output_mode != "run_report"){
	$selected  = isset($_GET['format']) ? 'selected' : '';
	$output .= "<select id = 'labor_report_format' name = 'labor_report_format'>";
	$output .= "<option value = '0'>Hours Minutes Format</option>";
	$output .= "<option value = '1' $selected>Hours as Decimal Format</option>";
	$output .= "</select>";
}

if($special_message!="")
{
	$output .= "<br><br>" . $special_message . "<br><br>";
}
else
{
	$output .= "<table cellpadding=6 cellspacing=0 style='border:solid 1px #cccccc'>";
	if($is_multi_day)
	{
		$output .= "<tr><td colspan='12' align='center' bgcolor='#bbbbbb'>" . speak('Range Total') . "</td></tr>";
		$output .= lbr_output_row_header();
		$lbrarr = lbr_output_row_vars("Range",$rinfo,"all");
		$output .= $lbrarr['html'];
		$datastr .= $lbrarr['datastr'];
		$data[] = $lbrarr['data'];
	}
	$output .= "<tr><td colspan='12' align='center' bgcolor='#bbbbbb'>".speak("By the Day")."</td></tr>";
	$output .= lbr_output_row_header();
	foreach($dinfo as $d => $vars)
	{
		$d_datetime = $d . " 00:00:00";
		$d_ts = strtotime($d_datetime);
		// based on date format setting
		$date_setting = "date_format";
		$location_date_format_query = lavu_query( "SELECT `location`, `setting`, `value` FROM `config` WHERE `setting` = '[1]'",  $date_setting);
		if( !$location_date_format_query || !mysqli_num_rows($location_date_format_query)){
			$date_format = array("value" => 2);
		}else
			$date_format = mysqli_fetch_assoc( $location_date_format_query );

		switch ($date_format['value']){
			case 1:$date_format = date("d/m/Y",$d_ts);
			break;
			case 2:$date_format = date("m/d/Y",$d_ts);
			break;
			case 3:$date_format = date("Y/m/d",$d_ts);
			break;
		}
		//$lbrarr = lbr_output_row_vars(date("m/d/Y",$d_ts),$vars,"all");
		$lbrarr = lbr_output_row_vars($date_format,$vars,"all");
		$output .= $lbrarr['html'];
		$datastr .= $lbrarr['datastr'];
		$data[] = $lbrarr['data'];
	}
	if(!$is_multi_day)
	{
		$output .= "<tr><td colspan='12' align='center' bgcolor='#bbbbbb'>".speak("By the Hour")."</td></tr>";
		$output .= lbr_output_row_header();
		foreach($hinfo as $h => $vars)
		{
			$h_datetime = $h . ":00:00";
			$h_ts = strtotime($h_datetime);

			$lbrarr = lbr_output_row_vars(date("h:i a",$h_ts),$vars,"all");
			$output .= $lbrarr['html'];
			$datastr .= $lbrarr['datastr'];
			$data[] = $lbrarr['data'];
		}
	}
	$output .= "</table>";
	//$output .= "</table>";

	$output .= "<br><table>";
	$output .= "<tr><td valign='top'>*</td><td valign='top' style='font-size:12px; color:#777777; width:420px'>".speak("Sales is calculated by looking at items added during a specific hour, and is only included if the order was opened on the same day")."</td></tr>";
	$output .= "<tr><td valign='top'>**</td><td valign='top' style='font-size:12px; color:#777777; width:420px'>".speak("Estimated Labor Cost is calculated by hours x base payrate, and does not include additional costs such as overtime")."</td></tr>";
	$output .= "</table>";
	//$output .= $str;

	$output .= "</div>";
}

// To Auto Run a Specific Report:
// get:run_report_start_datetime=Y-m-d H:i:s
// get:run_report_end_datetime=Y-m-d H:i:s
// get:output_mode=run_report

$output_mode = (isset($_GET['output_mode']))?$_GET['output_mode']:"";
if($output_mode=="export")
{
	$export_filename = "Labor_Report_".date("YmdHis") . ".csv";
	header('Content-Type: text/csv; charset=utf-8');
	header('Content-Disposition: attachment; filename='.$export_filename);
	echo $datastr;
}
else if($output_mode=="data")
{
	if(isset($_GET['testlabor']))
	{
		//echo "Start: $day_start_datetime<br>";
		//echo "End: $day_end_datetime<br><br>";
	}
}
else if($output_mode=="run_report")
{
	echo $output;
}
else
{
	$header_output = str_replace("class='ttimInput'","class='ttimInput' style='display:none'",$header_output);
	$header_output = str_replace("input_wrapper leftBorder","",$header_output);

	echo $header_output;
	echo $output;
}
?>

<link href="/cp/css/select2-4.0.3.min.css" rel="stylesheet" />
<script src="/cp/scripts/select2-4.0.3.min.js"></script>
<script language="javascript">
(function($) {
	$(document).ready(function() {
		$('#labor_report_format').change(function() {
			var format = $("select[name^=\'labor_report_format\']").val();
			if(format == 1){
				//window.location = "index.php?mode=home_labor_report&format=decimalized";
				var ajax_url = 'index.php?widget=reports/v2_entry&render_frame=reports.display&report=autodirect:labor_report&/cp/areas/labor_report.php&mode=<?php echo $mode=getvar('mode'); ?>&format=decimalized';
				ajax_link('report_container',ajax_url,'','','f_tcalInit');
			}else{
				//window.location = "index.php?mode=home_labor_report";
				var ajax_url = 'index.php?widget=reports/v2_entry&render_frame=reports.display&report=autodirect:labor_report&/cp/areas/labor_report.php&mode=<?php echo $mode=getvar('mode'); ?>';
				ajax_link('report_container',ajax_url,'','','f_tcalInit');								
			}
		});
	});
})(jQuery);
</script>
