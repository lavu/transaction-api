<?php
require_once ("/home/poslavu/public_html/admin/lib/info_inc.php");

function getEmpClassTitle($empId, $dbname) {
	$query = mlavu_query("SELECT `title` FROM `$dbname`.`emp_classes` WHERE `id`= $empId");
	$row = mysqli_fetch_assoc($query);
	return $row['title'];
}

/**
 * This function is used to check selected employee class status.
 * @param unknown $empId
 * @param unknown $dbname
 * @return 0:unpaired, 1:paired
 */
function checkEmpClassStatus($empId, $dbname) {
	$getEmpStatus = 0;
	$getUserRoleQuery = mlavu_query("SELECT `role_id` from `$dbname`.`users` WHERE `_deleted` = 0 AND role_id != ''");
	while ($userData = mysqli_fetch_assoc($getUserRoleQuery)) {
		$userRoles =  explode(",", $userData['role_id']);
		$getEmpStatus = getEmpClassStatus($userRoles, $empId);
		if ($getEmpStatus) {
			break;
		}
	}
	return $getEmpStatus;
}

/**
 * This function is used to get employee class status.
 * @param unknown $userRoles
 * @param unknown $empId
 * @return 0:unpaired, 1:paired
 */
function getEmpClassStatus($userRoles, $empId) {
	$getEmpStatus = 0;
	if (!empty($userRoles)) {
		foreach ($userRoles as $userRole) {
			$empRoleInfo = explode('|', $userRole);
			$empClassId =  $empRoleInfo['0'];
			if ($empClassId == $empId) {
				$getEmpStatus = 1;
				break;
			}
		}
	}
	return $getEmpStatus;
}

$return = 0;
if(isset($_REQUEST["empClsId"]) && !empty($_REQUEST["empClsId"])) {
	$dbname = 'poslavu_'.$_REQUEST["dataname"].'_db';
	$classId = trim($_REQUEST["empClsId"]);
	if($_REQUEST["type"] == 'single') {
		$userStatus = checkEmpClassStatus($classId, $dbname);
		$return = $userStatus;
	} else {
		$classId = explode(',', $classId);
		$flag = 0;
		$dataArr = [];
		$msg = [];
		foreach ($classId as $id) {
		  $userStatus = checkEmpClassStatus($id, $dbname);
		  if ($userStatus == 0) {
		  	$flag = 1;
		  	$msg []= getEmpClassTitle($id, $dbname);
 		  }
		}
		if ($flag == 1) {
			$dataArr['status'] = 'failure';
			$dataArr['message'] = implode(',',$msg);
		} else {
			$dataArr['status'] = 'success';
			$dataArr['message'] = '';
		}
		$return = json_encode($dataArr);
	}
}
echo $return;

?>

