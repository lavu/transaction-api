<style type="text/css">
<!--
-->
</style>
<div style="position:relative" id="main_content_area">
              <script language='JavaScript' type="text/javascript">function form1_validate() { document.form1.submit();
} </script>
              
<table width="530" align="center" id="maintext" style='border:solid 0px #aaaaaa'>
  <tr>
    <td width="500" colspan='2' align="left">
    <table width="520" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td width="40"><img src="images/i_guide_sm.png" alt="POSLavu Set Up Guide" width="30" height="35" /></td>
        <td class="sectiontitle">Guide is Completed</td>
      </tr>
    </table></td>
    </tr>
  <tr>
    <td height="50" colspan="2" align='left'  valign='top' class="sharptext">Thank you for using our Guide to set up your POSLavu system.</td>
  </tr>
  <tr>
    <td height="50" colspan="2" align='left'  valign='bottom' class="titletext">Please use our Tutorials and Exercises to continue configuring POSLavu to meet your requirements.</td>
  </tr>
  <tr>
    <td colspan="2" align='left'  valign='top'>&nbsp;</td>
  </tr>
  <tr>
    <td colspan="2" align='left'  valign='top' bgcolor="#e4e4e4">
    <table width="530" border="0" align="center" cellpadding="8" cellspacing="0" id="maintext4" style='border:solid 0px #aaaaaa'>
      <tr>
        <td height="8" colspan="2" align='right' valign='middle' >
        </td>
        </tr>
      <tr>
        <td width="60" align='right' valign='middle' >&nbsp;</td>
        <td width="470" align="left" valign="middle" >. </td>
        </tr>
      <tr>
        <td align='right' valign='middle' >&nbsp;</td>
        <td style='padding-right:32px'>&nbsp;</td>
        </tr>
      <tr>
        <td align='right' valign='middle' >&nbsp;</td>
        <td align="left" valign="middle" >&nbsp;</td>
      </tr>
      <tr>
        <td align='right' valign='middle' >&nbsp;</td>
        <td align="left" valign="middle" >&nbsp;</td>
      </tr>
      </table></td>
  </tr>
  <tr>
    <td height="50" colspan='2' align="left" valign="bottom" class="titletext">Have fun and enjoy the power of POSLavu!</td>
  </tr>
  <tr>
    <td colspan='2' align="left" valign="top">&nbsp;</td>
    </tr>
</table>
 </div>