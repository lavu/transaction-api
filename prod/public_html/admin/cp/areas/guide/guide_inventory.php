<div style="position:relative" id="main_content_area">
  <script language='JavaScript' type="text/javascript">function form1_validate() { document.form1.submit();
} </script>
  <form action='' method='post' name='form1' id="form2">
    <input type='hidden' name='posted' value='1' />
    <table width="530" align="center" style='border:solid 0px #aaaaaa'>
      <tr>
        <td colspan='2' align="left"><table width="520" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td width="40"><img src="images/i_inventory_sm.png" width="30" height="35" /></td>
            <td class="sectiontitle">Inventory Example</td>
          </tr>
        </table></td>
      </tr>
      <tr>
        <td colspan="2" align='left'  valign='top'><p>Your inventory allows you to enter the ingredients and/or materials that make up your menu items. Use the fields below to establish your stock &quot;ingredients&quot;. </p>
          <p>The example below shows how you name your Inventory category (in blue - &quot;Vegetables&quot;), menu item (&quot;Lettuce Heads&quot;), quantity (&quot;296&quot;), and units of measure (&quot;heads&quot;). <br />
            <br />
        </p></td>
      </tr>
      <tr>
        <td colspan="2" align='center'  valign='top'><img src="images/inventory_sample.png" width="500" height="250" alt="inventory example 1" /></td>
      </tr>
      <tr>
        <td width="260" align='right'  valign='top'>&nbsp;</td>
        <td >&nbsp;</td>
      </tr>
      <tr>
        <td colspan="2" align='left'  valign='top'><p>When you set up your restaurant in the POSLavu backend, you will be able to enter your ingredients/materials and later keep track of them in reports.</p>
        <p>You can add new Categories to keep your Inventory organized. Use the red &quot;X&quot; to the right of each entry to delete. The item is not officially deleted until you click &quot;SAVE&quot;.</p>
        <p>Be sure to click &quot;SAVE&quot; before leaving the page if you want to save your work. </p></td>
      </tr>
      <tr>
        <td colspan='2'><table width="100%" border="0" cellspacing="2" cellpadding="0">
          <tr>
            <td width="50" align="center" valign="top" ><img src="images/i_exercise.png" width="40" height="40" /></td>
            <td align="left" valign="middle" class="titletext">Exercise: Inventory Made Easy!</td>
          </tr>
          <tr>
            <td align="center" valign="top" >&nbsp;</td>
            <td align="left" valign="top" class="maintext">Learn how to set up an inventory item and tied it into menu items.<br /> 
              Though not required for Set Up, trying the Exercise may help you understand the system and is recommended.</td>
          </tr>
          <tr>
            <td align="center" valign="top" >&nbsp;</td>
            <td align="left" valign="top" class="maintext">
            <table border="0" cellspacing="0" cellpadding="2">
              <tr>
                <td><img src="images/hand_right.png" width="21" height="16" /></td>
                <td align="left" valign="middle" class="maintext"><a href="index.php?mode=guide_exercise2">Inventory Items Integration</a></td>
              </tr>
              <tr>
                <td>&nbsp;</td>
                <td align="left" valign="middle" class="maintext">&nbsp;</td>
              </tr>
            </table></td>
          </tr>
        </table></td>
      </tr>
    </table>
  </form>
  <br />
</div>
