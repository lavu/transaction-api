<style type="text/css">
<!--
-->
</style>
<div style="position:relative" id="main_content_area">
  <script language='JavaScript' type="text/javascript">function form1_validate() { document.form1.submit();
} </script>
  <form action='' method='post' name='form1' id="form2">
    <input type='hidden' name='posted' value='1' />
        <table width="530" border="0" align="center" cellpadding="0" cellspacing="0" style='border:solid 0px #aaaaaa'>
      <tr>
        <td width="260" colspan='2' align="left"><table width="520" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td width="40"><img src="images/i_menu_sm.png" width="30" height="35" /></td>
            <td class="sectiontitle">Settings : Direct Printing</td>
          </tr>
        </table></td>
      </tr>
      <tr>
        <td colspan="2" align='center'  valign='top'>
        <table width="530" border="0" cellspacing="0" cellpadding="0" id="maintext">
          <tr>
            <td class="titletext">Enabling Direct Printing</td>
          </tr>
          <tr>
            <td>
                <p>
                    For easy, reliable printing, we recommend using hardwired printers.
                    The iPad will communicate wirelessly.
                </p>
              <p>
                  When you first use Lavu iPad POS, you may need to enable Direct Printing by clicking
                  &ldquolSettings&rdquo; from the home screen. Next, click on the &ldquo;Advanced
                  Locations&rdquo; tab.
              </p>
              <p>Always finish by clicking the &quot;Submit&quot; button.</p>
              <p>&nbsp;</p></td>
          </tr>
          <tr>
            <td><img src="images/direct_printing_1.png" /></td>
          </tr>
          <tr>
            <td>
                <p>Click on the &ldquo;Enable Direct Printing&rdquo; box,. Once finished, scroll down
                and click on &ldquo;Submit.&rdquo;</p>
                <p>
                    <img src="images/direct_printing_2.png" alt="">
                </p>
                <p>
                    Once printing is enabled, it’s time to set up printers in the
                    &ldquo;Printers/KDS&rdquo; section of your back of house settings. To start
                    click on &ldquo;add new&rdquo; and start filling in the printer's information.
                </p>
            </td>
          </tr>
          <tr>
            <td>
              <ul>
                  <li><b class="msg_bold">Setting</b> - This is set as either a kitchen or receipt. Each printer must be unique</li>
                  <li><b class="msg_bold">Name </b>- Give your printer a name to help organization</li>
                  <li><b class="msg_bold">IP Address </b>- Insert your printer’s IP address</li>
                  <li><b class="msg_bold">Port</b> - Port number will typically be 9100</li>
                  <li><b class="msg_bold">Command Set</b> - Set the printer to it’s specific command set</li>
              </ul>              </td>
          </tr>
          <tr>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td><img src="images/direct_printing_3.png" alt="" /></td>
          </tr>
        </table>
          </p></td>
      </tr>
      <tr>
        <td colspan="2" align='left'  valign='top' class="maintext"><p> The following fields are optional:</p>
          <ul>
              <li>Characters Per Line</li>
              <li>Characters Per Line (large font}</li>
              <li>Image Capability</li>
              <li>Perform Status Check</li>
              <li>Print each item on it's own individual ticket</li>
          </ul>
            <p>
                Once finished, don't forget to click on &ldquo;submit&rdquo; to save changes.
            </p>

            <h3 class="msg_bold">
                Kitchen Printers will Need to be Assigned in the Back of House Menu Section
            </h3>

            <p>
                For more information on Direct Printing visit our
                <a target="_blank" href="https://support.lavu.com/">Help Center</a>.
            </p>

        </td>
      </tr>
      <tr>
        <td colspan='2' align="left" valign="top">&nbsp;</td>
      </tr>
    </table>
  </form>
</div>
