<table width="530" align="center" style='border:solid 0px #aaaaaa' id="maintext">
      <tr>
        <td width="518" colspan='2' class="sectiontitle"><table width="520" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td width="40"><img src="images/i_settings_sm.png" width="30" height="35"></td>
            <td class="sectiontitle">Advanced Location Settings</td>
          </tr>
        </table></td>
      </tr>
      <tr>
        <td colspan="2" align='left' valign='top' class="titletext">Printing</td>
      </tr>
      <tr>
        <td colspan="2" align='left' valign='top' ><p>Direct Printing allows you to print without a computer and is strongly recommended.</p>
          <p>&nbsp;</p></td>
      </tr>
      <tr>
        <td colspan="2" align="left" valign="middle" bgcolor="#e4e4e4"><table width="530" border="0" align="center" cellpadding="8" cellspacing="0" id="maintext4" style='border:solid 0px #aaaaaa'>
          <tr>
            <td height="8" colspan="2" align='right' valign='middle' ></td>
          </tr>
          <tr>
            <td align='right' valign='middle' ><span style="padding-left:32px;">Display receipt print options at checkout?</span></td>
            <td align="left" valign="middle" ><input type='hidden' name='admin_PIN_terminal_set3' id='admin_PIN_terminal_set3' value="1" />
              <span style="padding-right:32px">
                <input type='checkbox' name='ask4email_at_checkout' [lcheckbox:ask4email_at_checkout] />
              </span></td>
          </tr>
          <tr>
            <td align='right' valign='middle' ><span style="padding-left:32px;">Print receipt regardless of option selected above?</span></td>
            <td align="left" valign="middle" ><span style="padding-right:32px">
              <input type='checkbox' name='default_receipt_print' [lcheckbox:default_receipt_print] />
            </span></td>
          </tr>
          <tr>
            <td align='right' valign='middle' ><span style="padding-left:32px;">Auto send order to kitchen at checkout?</span></td>
            <td align="left" valign="middle" ><span style="padding-right:32px">
              <input type='checkbox' name='auto_send_at_checkout' [lcheckbox:auto_send_at_checkout] />
            </span></td>
          </tr>
        </table></td>
      </tr>
    </table>