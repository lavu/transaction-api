<style type="text/css">
<!--
-->
</style>
<div style="position:relative" id="main_content_area">
              <script language='JavaScript' type="text/javascript">function form1_validate() { document.form1.submit();
} </script>
              
<table width="530" align="center" id="maintext" style='border:solid 0px #aaaaaa'>
  <tr>
    <td width="500" colspan='2' align="left">
    <table width="520" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td width="40"><img src="images/i_guide_sm.png" title="POSLavu Set Up Guide" width="30" height="35" /></td>
        <td class="sectiontitle">Set Up Guide</td>
      </tr>
    </table></td>
    </tr>
  <tr>
    <td height="50" colspan="2" align='left'  valign='top' class="sharptext">
        The Lavu iPad POS system is easy to use and fully customizable to fit the
        needs of your business. Learn more about proper setup and operation basics.
    </td>
  </tr>
  <tr>
    <td colspan="2" align='left'  valign='bottom' class="titletext">
        Lavu Guide Tips
    </td>
  </tr>
  <tr>
    <td colspan="2" align='left'  valign='top'>
        Here are a few helpful thoughts to keep in mind.
    </td>
  </tr>
  <tr>
    <td colspan="2" align='left'  valign='top' bgcolor="#e4e4e4">
    <table width="530" border="0" align="center" cellpadding="8" cellspacing="0" id="maintext4" style='border:solid 0px #aaaaaa'>
      <tr>
        <td height="8" colspan="2" align='right' valign='middle' >
        </td>
        </tr>
      <tr>
        <td width="60" align='right' valign='middle' ><img src="images/ideabulb.png" title="Helpful Information" width="40" height="40" /></td>
        <td width="470" align="left" valign="middle" >
            Any information that you enter on your account’s back of house can be edited later. Nothing is final.
        </td>
        </tr>
      <tr>
        <td align='right' valign='middle' ><img src="images/ideabulb.png" title="Helpful Information" width="40" height="40" /></td>
        <td style='padding-right:32px'>
            When you see this icon <img src="images/question_sm.png" width="17" height="15" title="That's how it works!" />, roll your mouse over it for a brief explanation. In some cases, clicking the same icon will bring up a window with more information.
        </td>
        </tr>
      <tr>
        <td align='right' valign='middle' >&nbsp;</td>
        <td align="left" valign="middle" ><table width="400" height="20" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td width="20" align="left" valign="bottom" background=""><img src="images/i_important.png" title="Important!!" width="14" height="14" /></td>
            <td width="380" align="left" valign="bottom" class="msg">
                Information with this warning  deserves your attention.
            </td>
            </tr>
          </table></td>
      </tr>
      <tr>
        <td align='right' valign='middle' >&nbsp;</td>
        <td align="left" valign="middle" ><table width="400" height="20" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td width="20" align="left" valign="bottom" background=""><img src="images/i_recommended_mini.png" title="Recommended!!" width="14" height="14" /></td>
            <td width="380" align="left" valign="bottom" class="msg">
                This symbol means we highly recommend enabling a certain feature or option.
            </td>
          </tr>
        </table></td>
      </tr>
      </table></td>
  </tr>
  <tr>
    <td height="50" colspan='2' align="left" valign="bottom" class="titletext">Ok, let's go.</td>
  </tr>
  <tr>
    <td colspan='2' align="left" valign="top">
        Use the <strong>Continue</strong>** button at the bottom of each screen to move forward.<br />
        **When you use that button, the information you have entered on the current page will be saved.</td>
    </tr>
</table>
 </div>