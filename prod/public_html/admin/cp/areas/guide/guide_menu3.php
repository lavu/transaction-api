<style type="text/css">
<!--
-->
</style>
<div style="position:relative" id="main_content_area">
  <script language='JavaScript' type="text/javascript">function form1_validate() { document.form1.submit();
} </script>
    <table width="530" align="center" style='border:solid 0px #aaaaaa' id="maintext">
      <tr>
        <td width="260" colspan='2' align="left">
        <table width="520" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td width="40"><img src="images/i_menu_sm.png" width="30" height="35" /></td>
            <td class="sectiontitle">Clear the Default Settings</td>
          </tr>
        </table></td>
      </tr>
      <tr>
        <td colspan="2" align='left'  valign='top'>
            When you are set to enter items into your own menu, you can easily clear out
            generic content in the back of house of Lavu iPad POS. By clicking on the X
            you can delete individual items or entire categories.
        </td>
      </tr>
      <tr>
        <td colspan='2'><img src="images/inventory_items.png" /></td>
      </tr>
      <tr>
        <td colspan='2'>&nbsp;</td>
      </tr>
      <tr>
        <td colspan='2'>
            <p>
                Now you can delete the other tab information: Optional Modifiers, Forced
                Modifiers, Modifier Groups, Super Groups, and Happy Hours.
            </p>
        </td>
      </tr>
      <tr>
        <td colspan='2' align="left" valign="top" >
            <img src="images/menu_clear_2.png" alt="" />
        </td>
      </tr>
      <tr>
        <td colspan='2' align="left" valign="top" >&nbsp;</td>
      </tr>
      <tr>
        <td colspan='2' align="left" valign="top" >Deletions are not finalized until you click the &quot;Save&quot; button. </td>
      </tr>
    </table>
  <br />
</div>
