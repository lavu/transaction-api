    <table width="530" align="center" style='border:solid 0px #aaaaaa'>
      <tr>
        <td width="520" colspan='2' align="left" class="sectiontitle">
        <table width="520" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td width="40"><img src="images/i_settings_sm.png" width="30" height="35" /></td>
            <td class="sectiontitle">Location Settings</td>
          </tr>
        </table></td>
      </tr>
      <tr>
        <td colspan="2" align='left'  valign='top'><p>The information below has be entered automatically from your initial signup.<br />
          Please review and complete the fields below. <br />
          <br />
        </p></td>
      </tr>
      <tr>
        <td colspan="2" align='right' valign='top' bgcolor="#E4E4E4"><table width="530" border="0" align="center" cellpadding="8" cellspacing="0" id="maintext2" style='border:solid 0px #aaaaaa'>
          <tr>
            <td height="8" colspan="2" align='right' valign='middle' ></td>
          </tr>
          <tr>
            <td width="440" align="right" valign="middle">What is the  tax rate applicable to your business? </td>
            <td width="90" align="center" valign="middle"><input type="text" name="taxrate" id="taxrate" value="[lsetting:taxrate]" /></td>
          </tr>
          <tr>
            <td height="8" colspan="2" align="right" valign="middle"></td>
          </tr>
        </table></td>
      </tr>
      <tr>
        <td colspan="2" align='left' valign='top'>&nbsp;</td>
      </tr>
      <tr>
        <td colspan="2" align='left'  valign='top'><table width="500" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td width="20" align="left"><img src="images/i_important.png" width="14" height="14" /></td>
            <td width="480" align="left" valign="middle" class="msg">This information will appear on any receipts issued from your business.</td>
          </tr>
        </table>
          <p>&nbsp;</p></td>
      </tr>
      <tr>
        <td colspan='2' bgcolor="#e4e4e4"><table width="530" border="0" align="center" cellpadding="8" cellspacing="0" id="maintext" style='border:solid 0px #aaaaaa'>
          <tr>
            <td height="8" colspan="2" align='right' valign='middle' ></td>
          </tr>
          <!--<tr>
            <td align="right" valign="middle">What currency does your business use?</td>
            <td align="center" valign="middle"><input type="text" name="textfield" id="textfield" /></td>
          </tr>-->
          <tr>
            <td width="440" align="right" valign="middle">Enter monetary symbol:</td>
            <td align="left" valign="middle"><input type='text' name='monitary_symbol' size="8" value="[lsetting:monitary_symbol]" /></td>
          </tr>
          <tr>
            <td height="8" colspan="2" align="right" valign="middle"></td>
          </tr>
        </table></td>
      </tr>
    </table>
    <br />
