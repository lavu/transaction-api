<style type="text/css">
<!--
-->
</style>
<div style="position:relative" id="main_content_area">
  <script language='JavaScript' type="text/javascript">function form1_validate() { document.form1.submit();
} </script>
        <table width="530" border="0" align="center" cellpadding="0" cellspacing="0" style='border:solid 0px #aaaaaa'>
      <tr>
        <td width="260" colspan='2' align="left"><table width="520" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td width="40"><img src="images/i_menu_sm.png" width="30" height="35" /></td>
            <td class="sectiontitle">Set Up Your Menu</td>
          </tr>
        </table></td>
      </tr>
      <tr>
        <td colspan="2" align='center'  valign='top'><table width="530" border="0" cellspacing="0" cellpadding="0" id="maintext">
          <tr>
            <td>The POSLavu Menu set up is organized as follows:</td>
          </tr>
          <tr>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td align="center"><table width="530" border="0" cellspacing="0">
              <tr>
                <td align="center"><table height="20" border="0" cellpadding="3" cellspacing="0">
                  <tr>
                    <td align="left" valign="bottom" ><span class="msg_bold">Menu Groups</span></td>
                    <td width="16" align="left" valign="bottom" class="maintext"><strong><img src="images/arrowline_sm.png" width="12" height="12" /></strong></td>
                    <td align="left" valign="bottom" class="maintext"><span class="msg_bold">Menu Categories</span></td>
                    <td width="16" align="left" valign="bottom" class="maintext"><strong><img src="images/arrowline_sm.png" width="12" height="12" /></strong></td>
                    <td align="left" valign="bottom" class="maintext"><span class="msg_bold">Menu Items</span></td>
                    </tr>
                </table></td>
              </tr>
              <tr>
                <td align="center"><img src="images/menu_heirarchy.jpg" width="402" height="97" /></td>
              </tr>
              <tr>
                <td>&nbsp;</td>
              </tr>
            </table></td>
          </tr>
          <tr>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td>
                <ul>
                    <li>
                        <span class="msg_bold">Menu Groups:</span> Top level  Category group like
                        &quot;Beverages&quot;, &quot;Lunch&quot;, and &quot;Dinner&quot;
                    </li>
                    <li>
                        <span class="msg_bold">Menu Categories:</span> Groups of Menu Items such
                        as &quot;Coffee Drinks&quot; or &quot;Sandwiches&quot;
                    </li>
                    <li>
                        <span class="msg_bold">Menu Items:</span> Individual Items like Spaghetti
                        Dinner or Iced Coffee
                    </li>
                </ul>

          </tr>
          <tr>
            <td>

            </td>
          </tr>
          <tr>
            <td>

            </td>
          </tr>
          <tr>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td height="30">&nbsp;</td>
          </tr>
          <tr>
            <td height="30">
<!--                <img src="images/screen_map_abc.jpg" width="530" height="182" />-->
                <h2>Back of the House</h2>
                <p>
                    <img src="images/menu_back.png" alt="">
                </p>
                <h2>
                    Front of the House
                </h2>
                <p>
                    <img src="images/menu_front.png" alt="">
                </p>
            </td>
          </tr>
          <tr>
            <td>&nbsp;</td>
          </tr>
          </table>
          </p></td>
      </tr>
      <tr>
        <td colspan="2" align='left'  valign='top' class="maintext">

            <p>Menu items details can be applied after they are set in menu categories. </p>

            <ul>
                <li>
                    <span class="msg_bold">Menu Item Price</span> - set base price here
                </li>
                <li>
                    <span class="msg_bold">Optional Modifiers</span> -
                    added to menu item already placed i.e. extra cheese
                </li>
                <li>
                    <span class="msg_bold">Forced Modifier</span> - required to place menu item i.e.
                    medium rare, salad dressing
                </li>
                <li>
                    <span class="msg_bold">Item Graphic</span> - visible on front of house
                </li>
                <li>
                    <span class="msg_bold">Item Details</span> - includes customized tax rates,
                    item descriptions, etc.
                </li>
            </ul>
        </td>
      </tr>
      <tr>
        <td colspan="2" align='left'  valign='top' class="maintext">&nbsp;</td>
      </tr>
      <tr>
        <td colspan="2" align='left'  valign='top' class="maintext"><img src="images/menu_modifiers.png" width="530" height="233" /></td>
      </tr>
      <tr>
        <td colspan="2" align='left'  valign='top' class="maintext">&nbsp;</td>
      </tr>
      <tr>
        <td colspan="2" align='left'  valign='top' class="maintext">
           <p>
               <strong>Forced modifiers can be grouped together.</strong> This happens when more than one option is used to place a menu item.
           </p>
        </td>
      </tr>
      <tr>
        <td colspan="2" align='left'  valign='top' class="maintext">&nbsp;</td>
      </tr>
      <tr>
        <td colspan="2" align='left'  valign='top' class="maintext">
          The Lavu iPad POS Menu system has many features and we have only begun to explore
            them together. You can find examples and explanations of features at our
            <a href="https://support.lavu.com/" target="_blank">Help Center.</a>
        </td>
      </tr>
      <tr>
        <td colspan='2' align="left" valign="top">&nbsp;</td>
      </tr>
    </table>
</div>
