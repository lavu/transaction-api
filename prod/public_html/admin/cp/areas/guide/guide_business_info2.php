    <table width="530" border="0" align="center" cellpadding="0" cellspacing="0" id="maintext" style='border:solid 0px #aaaaaa'>
      <tr>
        <td width="538" colspan='2' align="left" valign="top" class="sectiontitle"><table width="520" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td width="40"><img src="images/i_settings_sm.png" width="30" height="35"></td>
            <td class="sectiontitle">Advanced Location Settings</td>
          </tr>
        </table></td>
      </tr>
      <tr>
        <td colspan='2' align="left" valign="top">
            The following options define how the system operates. These options will help with basic setup.<br>
<table width="500" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="20" align="left" valign="middle"><img src="images/i_important.png" width="14" height="14" /></td>
    <td align="left" valign="middle" class="msg">
        There are further Advanced Location Settings in your back of house that may apply to your business.
    </td>
  </tr>
</table></td>
      </tr>
     
      <tr>
        <td height="30" colspan='2' align="left" valign="bottom"></td>
      </tr>
      <tr>
        <td colspan='2' align="left" valign="top"class="titletext"><table width="530" border="0" align="center" cellpadding="10" cellspacing="0" id="maintext3" style='border:solid 0px #aaaaaa'>
          <tr>
            <td height="8" colspan="2" align='right' valign='middle' ></td>
          </tr>
          <tr>
            <td width="120" height="150" align="right" valign="top"><img src="images/table_layout.jpg" alt="" width="120" height="64"></td>
            <td align="left" valign="top"><span class="titletext">Table Layout</span><br>
                You can drag/drop tables to match the layout of your restaurant.
                <br> (This is the typical restaurant/fine dining mode)
            </td>
          </tr>
          <tr>
            <td height="130" align="right" valign="top"><img src="images/tab_layout.png" width="120" height="64"></td>
            <td align="left" valign="top" ><span class="titletext">Tab Layout</span><br>
                Tab Layout shows only open orders in a list. Create tabs by name or by swiping a credit card. <br/>
                (Commonly used in bars or nightclubs)
          </tr>
          <tr>
            <td height="100" align="right" valign="top"><img src="images/quickserve_layout.png" width="120" height="64"></td>
            <td align="left" valign="top"><span class="titletext">Quick Serve</span><br>
                Quick serve defaults to the order screen. This is used for single instance ordering. <br/>
                (Commonly used for fast food establishments, food-trucks, and coffee shops)
            </td>
          </tr>
          <tr>
            <td height="8" colspan="2" align="right" valign="middle"></td>
          </tr>
        </table></td>
      </tr>
      <tr>
        <td height="50" colspan='2' align="left" valign="bottom" class="titletext">Choose your Order Type</td>
      </tr>
      <tr>
        <td colspan='2' align="left" valign="top"><p>
                Set your account’s default layout here.
            </p></td>
      </tr>
      <tr>
        <td colspan="2" align='left' valign='middle' bgcolor="#E4E4E4" >
            <table width="530" border="0" align="center" cellpadding="8" cellspacing="0" id="maintext2" style='border:solid 0px #aaaaaa'>
          <tr>
            <td height="8" colspan="2" align='right' valign='middle' ></td>
          </tr>
          <tr>
            <td width="440" align="right" valign="middle">Which layout do you prefer as your default screen?</td>
            <td width="120" align="left" valign="middle">
              <input name='tab_view' type='radio' value="3" [lradio:tab_view:3] />
              Tables<br />
  <input name='tab_view' type='radio' value="1"[lradio:tab_view:1] />
              Tabs<br />
  <input name='tab_view' type='radio' value="2" [lradio:tab_view:2]/>
              Quick Serve

            </td>
          </tr>
          <tr>
            <td align="right" valign="middle">&nbsp;</td>
            <td align="center" valign="middle">&nbsp;</td>
          </tr>
          <tr>
            <td width="440" align="right" valign="middle"><span style="padding-left:32px;">Allow toggling between table and tab layout?</span></td>
            <td width="120" align="center" valign="middle"><span style="padding-right:32px">
              <input type='checkbox' name='allow_tab_view_toggle' [lcheckbox:allow_tab_view_toggle] />
            </span></td>
          </tr>
          <tr>
            <td height="8" colspan="2" align="right" valign="middle"></td>
          </tr>
        </table></td>
      </tr>
    </table>
