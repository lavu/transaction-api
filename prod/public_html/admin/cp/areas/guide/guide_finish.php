<style type="text/css">
<!--
.examtext {			
			color: #919191;
			font-family: Arial, Helvetica, sans-serif;
			font-size:12px;		
}
#boxlinks {color: #fff;
			font-family: Arial, Helvetica, sans-serif;
			font-size:14px;
			font-weight:600;
}
-->
</style>
<div style="position:relative" id="main_content_area">
              <script language='JavaScript' type="text/javascript">function form1_validate() { document.form1.submit();
} </script>
              
<table width="530" align="center" id="maintext" style='border:solid 0px #aaaaaa'>
  <tr>
    <td width="500" colspan='2' align="left">
        <table width="520" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td width="40">
                <img src="images/i_guide_sm.png" alt="POSLavu Set Up Guide" width="30" height="35" />
            </td>
            <td class="sectiontitle">Guide Set Up Process is Completed</td>
          </tr>
        </table>
    </td>
  </tr>
  <tr>
    <td colspan="2" align='left'  valign='top' class="sharptext">
        Thank you for using our Guide to set up your Lavu iPad POS system.
    </td>
  </tr>
  <tr>
    <td colspan="2" align='left'  valign='top'>
        <h3>NOW: Configure your Lavu system!</h3>
        Use our <a href="/cp/index.php?mode=assistance_tutorials">Tutorials and Exercises</a> to continue configuring Lavu iPad POS.
        <a href="/cp/index.php?mode=assistance_tutorials"><img src="images/youtube_box_full.jpg" height="370" /></a>
        <!-- YOUTUBE video embed - FrontEnd + BackEnd -->
        <script language="javascript">
          function youtube_code(videoid) {
            return '<object width="425" height="344"><param name="movie" value="https://www.youtube.com/v/'+videoid+'?hl=en&fs=1"></param><param name="allowFullScreen" value="true"></param><param name="allowscriptaccess" value="always"></param><embed src="https://www.youtube.com/v/'+videoid+'?hl=en&fs=1" type="application/x-shockwave-flash" allowscriptaccess="always" allowfullscreen="true" width="425" height="344"></embed></object>';
          }
        </script>
        <div id="video_area"></div>

         <!-- END YOUTUBE -->
    </td>
  </tr>
  <tr>
    <td colspan="2" align='left'  valign='top' ><table width="530" border="0" align="center" cellpadding="8" cellspacing="1">
      <tr>
        <td align="center" valign="top"><table width="150" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td align="center">
<!--                <a href="http://admin.poslavu.com/cp/index.php?mode=assistance_hardware_start">-->
<!--                    <img src="images/i_hardware.png" width="75" height="75" border="0" />-->
<!--                </a>-->
                <a target="_blank" href="http://www.poslavu.com/en/find-a-distributor"><img src="images/i_hardware.png" width="75" height="75" border="0" /></a>
            </td>
          </tr>
          <tr>
            <td align="center"><a href="http://admin.poslavu.com/cp/index.php?mode=hardware_start">Hardware Guide</a></td>
          </tr>
        </table></td>
        <td align="center" valign="top"><table width="150" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td align="center"><a href="http://support.poslavu.com"><img src="images/i_tute.png" width="75" height="75" border="0" /></a></td>
          </tr>
          <tr>
            <td align="center"><a href="/cp/index.php?mode=assistance_tutorials">Tutorials</a></td>
          </tr>
        </table></td>
        <td align="center" valign="top"><table width="150" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td align="center"><a href="http://admin.poslavu.com/cp/index.php"><img src="images/i_home.png" width="75" height="75" border="0" /></a></td>
          </tr>
          <tr>
            <td align="center"><a href="http://admin.poslavu.com/cp/index.php">Main Menu</a><a href="http://admin.poslavu.com/cp/index.php?mode=hardware_start"></a></td>
          </tr>
        </table></td>
      </tr>
      <tr>
        <td align="center" valign="top" bgcolor="#E6E6E6">
            <span class="examtext">
                Review our prefered hardware guide, and get connected with a Lavu Specialist
                for personalized hardware support.
            </span>
        </td>
        <td align="center" valign="top" bgcolor="#E6E6E6">
            <span class="examtext">
                Videos, documents, and diagrams to help you get started and
                to keep things running smoothly.
            </span>
        </td>
        <td align="center" valign="top" bgcolor="#E6E6E6">
            <span class="examtext">
                Return to the Lavu iPad POS main menu to continue dialing in your
                settings and configuring your system.
            </span>
        </td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td height="50" colspan='2' align="left" valign="bottom" >Have fun and enjoy the power of POSLavu!</td>
  </tr>
  <tr>
    <td colspan='2' align="left" valign="top">&nbsp;</td>
    </tr>
</table>
</div>