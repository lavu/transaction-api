<style type="text/css">
<!--
-->
</style>
<div style="position:relative" id="main_content_area">
  <script language='JavaScript' type="text/javascript">function form1_validate() { document.form1.submit();
} </script>
    <table width="640" border="0" align="center" cellpadding="0" cellspacing="0" style='border:solid 0px #aaaaaa'>
      <tr>
        <td width="260" colspan='2' align="left"><table width="520" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td width="40"><img src="images/i_menu_sm.png" width="30" height="35" /></td>
            <td class="sectiontitle">Menu Set Up Exercise</td>
          </tr>
        </table></td>
      </tr>
      <tr>
        <td colspan="2" align='center'  valign='top'></p></td>
      </tr>
      <tr>
        <td colspan="2" align='left'  valign='top' class="titletext">
        <table width="640" border="0" cellspacing="0" cellpadding="5" id="maintext">
          <tr>
            <td >
            <table width="620" border="0" cellspacing="0" cellpadding="0" class="overview_greentext">
              <tr>
                <td width="50" align="left" class="overview_greentext"><img src="images/ideabulb.png" width="40" height="40" /></td>
                <td width="570">This example will show you how to make a MENU ITEM with REQUIRED selections <br />
(&quot;Ice Cream Flavor&quot; and &quot;Sauce Type&quot;) and OPTIONAL selections (toppings)</td>
              </tr>
            </table></td>
          </tr>
          <tr>
            <td ></td>
          </tr>
          <tr>
            <td align="center"><table width="630" border="0" cellpadding="0" cellspacing="0">
              <tr>
                <td align="center"><table width="630" border="0" cellspacing="5" cellpadding="5">
                  <tr>
                      <td align="center" bgcolor="#e4e4e4" class="overview_blacktext">We will create an Ice Cream Sundae menu item complete with Forced and Optional Modifiers.</td>
                      </tr>
                  </table></td>
              </tr>
              <tr>
                <td align="center"><img src="images/ice_cream_sundae_overview.jpg" width="620" height="107" /></td>
              </tr>
              <tr>
                <td><table width="630" border="0" cellspacing="5" cellpadding="5">
                  <tr>
                    <td width="310" align="left" bgcolor="#e4e4e4" class="overview_blacktext">Selections such as flavor of Ice Cream and type of sauce are *Required* selections and  are set up as Forced Modifiers</td>
                    <td width="310" align="left" bgcolor="#e4e4e4" class="overview_blacktext">Selections such as Free Toppings and Extra Toppings are -not Required- selections and  are set up as Optional Modifiers</td>
                  </tr>
                </table></td>
              </tr>
            </table></td>
          </tr>
          <tr>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td align="center">&nbsp;</td>
          </tr>
        </table>
        <table width="640" border="0" cellspacing="0" cellpadding="0" id="maintext3">
          <tr>
            <td width="24"><table width="19" height="19" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td align="center" valign="middle" class="numbox">1</td>
              </tr>
            </table></td>
            <td>Login to the POSLavu backend and click the &ldquo;MENU&rdquo; button.</td>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td>&nbsp;</td>
            <td><table height="20" border="0" cellspacing="0" cellpadding="2">
              <tr>
                <td width="20" align="left" background=""><img src="images/i_important.png" title="Important!!" width="14" height="14" /></td>
                <td align="left" class="msg">Click this ANY TIME you make changes</td>
                <td width="18" align="center" valign="middle" class="msg"><img src="images/arrow_L_sm.png" width="18" height="9" /></td>
                <td width="92" align="left" class="msg"><img src="images/save_btn.png" title="Important!!" width="85" height="16" /></td>
              </tr>
            </table></td>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td>&nbsp;</td>
            <td height="30">&nbsp;</td>
            <td>&nbsp;</td>
          </tr>
        </table></td>
      </tr>
      <tr>
        <td colspan='2' align="left" valign="top">
        <table width="640" border="0" align="center" cellpadding="1" cellspacing="0" id="maintext2" style='border:solid 0px #aaaaaa'>
          <tr>
            <td width="23" height="19" align='left' valign='middle' >
            <table width="19" height="19" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td align="center" valign="middle" class="numbox">2</td>
              </tr>
            </table></td>
            <td align='left' valign='middle' >
            <table  border="0" cellpadding="0" cellspacing="0" id="maintext">
              <tr>
                <td align="left" background="">Click This</td>
                <td width="18" align="center" valign="middle" class="msg"><img src="images/arrow_L_sm.png" width="18" height="9" /></td>
                <td width="92" align="right" class="msg"><img src="images/btn_add_category.png"  width="123" height="22" /></td>
              </tr>
            </table></td>
          </tr>
          <tr>
            <td height="30"  colspan="2" align='center' valign='middle' >&nbsp;</td>
          </tr>
          <tr>
            <td  colspan="2" align='center' valign='middle' >
            <table width="640" border="0" cellspacing="0" cellpadding="0" id="maintext">
              <tr>
                <td width="24">
                <table width="19" height="19" border="0" cellspacing="0" cellpadding="0">
                  <tr>
                    <td align="center" valign="middle" class="numbox">3</td>
                  </tr>
                </table></td>
                <td>Type in a <strong>Title</strong>: Ice Cream </td>
                <td rowspan="2"><img src="images/e_addtitle.png" width="136" height="44" /></td>
              </tr>
              <tr>
                <td>&nbsp;</td>
                <td align="right">&nbsp;</td>
              </tr>
            </table></td>
          </tr>
          <tr>
            <td height="30"  colspan="2" align='center' valign='middle' >&nbsp;</td>
          </tr>
        </table></td>
      </tr>
      <tr>
        <td colspan='2' align="left" valign="top">
        <table width="640" border="0" cellspacing="0" cellpadding="0" id="maintext">
          <tr>
            <td width="24">
            <table width="19" height="19" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td align="center" valign="middle" class="numbox">4</td>
              </tr>
            </table></td>
            <td>Click &ldquo;Add Item&rdquo; and type in a <strong>Title</strong>: Ice Cream Sundae</td>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td>&nbsp;</td>
            <td colspan="2" align="center"><img src="images/e_add_item.jpg" width="460" height="78" /></td>
            </tr>
          <tr>
            <td>&nbsp;</td>
            <td height="30" colspan="2" align="center">&nbsp;</td>
          </tr>
        </table></td>
      </tr>
      <tr>
        <td colspan='2' align="left" valign="top"><table width="640" border="0" cellspacing="0" cellpadding="0" id="maintext">
          <tr>
            <td width="24">
            <table width="19" height="19" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td align="center" valign="middle" class="numbox">5</td>
                </tr>
              </table></td>
            <td><strong>Upload</strong> or select an image for an <strong>Item</strong> and for the <strong>Category</strong> - Click the box to the left of &quot;Details&quot; (above)</td>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td>&nbsp;</td>
            <td height="50">&nbsp;</td>
            <td>&nbsp;</td>
          </tr>
        </table></td>
      </tr>
      <tr>
        <td colspan='2' align="left" valign="top"><table width="640" border="0" cellspacing="0" cellpadding="0" id="maintext">
          <tr>
            <td width="24"><table width="19" height="19" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td align="center" valign="middle" class="numbox">6</td>
              </tr>
            </table></td>
            <td colspan="2"><strong>Forced Modifiers</strong> are used for required decisions such as &quot;Ice Cream Flavor&quot; or &quot;Type of Sauce&quot;.</td>
            </tr>
          <tr>
            <td>&nbsp;</td>
            <td align="left">
            <table border="0" cellspacing="0" cellpadding="0" id="maintext">
              <tr>
                <td width="20"><img src="images/arrowline_sm.png" width="12" height="12" /></td>
                <td>Select the &ldquo;Forced Modifiers&rdquo; tab. </td>
              </tr>
              <tr>
                <td><img src="images/pointline_sm.png" width="12" height="12" /></td>
                <td>Add a new <strong>List</strong> called &ldquo;Flavors&rdquo;</td>
              </tr>
              <tr>
                <td><img src="images/pointline_sm.png" width="12" height="12" /></td>
                <td>Add 3 <strong>Choices</strong>: Chocolate, Vanilla, Strawberry</td>
              </tr>
              <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
              </tr>
              <tr>
                <td><img src="images/arrowline_sm.png" width="12" height="12" /></td>
                <td> Add a new <strong>List</strong> &ldquo;Sauces&rdquo; </td>
              </tr>
              <tr>
                <td><img src="images/pointline_sm.png" width="12" height="12" /></td>
                <td>Add 3 <strong>Choices</strong>: Hot Fudge, Strawberry, Pineapple</td>
              </tr>
              <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
              </tr>
              <tr>
                <td><img src="images/arrowline_sm.png" width="12" height="12" /></td>
                <td>Add a new <strong>List</strong> &ldquo;Sundae Free Tops&rdquo; </td>
              </tr>
              <tr>
                <td><img src="images/pointline_sm.png" width="12" height="12" /></td>
                <td>Select &quot;Checklist&quot; </td>
              </tr>
              <tr>
                <td><img src="images/pointline_sm.png" width="12" height="12" /></td>
                <td>Add 2 <strong>Choices</strong>: Cherry, Whipped Cream</td>
              </tr>
            </table>
              <p><br />
              </p></td>
            <td width="200" align="center"><img src="images/e_forced1.jpg" width="200" height="75" /></td>
          </tr>
          <tr>
            <td>&nbsp;</td>
            <td colspan="2" align="left">We used &quot;Checklist&quot; for &quot;Sundae Free Tops&quot; because we want a customer to be asked if they want each topping, but to be able to select both, one or neither.</td>
            </tr>
          <tr>
            <td>&nbsp;</td>
            <td height="50" align="left">&nbsp;</td>
            <td align="center">&nbsp;</td>
          </tr>
        </table></td>
      </tr>
      <tr>
        <td colspan='2' align="left" valign="top"><table width="640" border="0" cellspacing="0" cellpadding="0" id="maintext">
          <tr>
            <td width="24" align="left" valign="top"><table width="19" height="19" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td align="center" valign="middle" class="numbox">7</td>
              </tr>
            </table></td>
            <td colspan="2"> <strong>Modifier Groups</strong> combine options into a sequential list, such as &ldquo;'Pick an Ice Cream Flavor', next 'pick  a Sauce Type', and then 'Sundae Free Tops'.&rdquo;</td>
          </tr>
          <tr>
            <td>&nbsp;</td>
            <td align="left">
            <table border="0" cellspacing="0" cellpadding="0" id="maintext">
              <tr>
                <td width="20"><img src="images/arrowline_sm.png" width="12" height="12" /></td>
                <td>Select &rdquo;Add List&rdquo;</td>
              </tr>
              <tr>
                <td><img src="images/pointline_sm.png" width="12" height="12" /></td>
                <td>Select &ldquo;Flavors&rdquo;</td>
              </tr>
              <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
              </tr>
              <tr>
                <td><img src="images/arrowline_sm.png" width="12" height="12" /></td>
                <td>Select &rdquo;Add List&rdquo;</td>
              </tr>
              <tr>
                <td><img src="images/pointline_sm.png" width="12" height="12" /></td>
                <td>Select &ldquo;Sundae Sauce&rdquo;</td>
              </tr>
              <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
              </tr>
              <tr>
                <td><img src="images/arrowline_sm.png" width="12" height="12" /></td>
                <td>Select &rdquo;Add List&rdquo;</td>
              </tr>
              <tr>
                <td><img src="images/pointline_sm.png" width="12" height="12" /></td>
                <td>Select &ldquo;Sundae Free Tops&rdquo;</td>
              </tr>
            </table>
              <p><br />
              </p></td>
            <td width="200" align="center"><img src="images/e_modgroups1.jpg" width="200" height="75" /></td>
          </tr>
          <tr>
            <td>&nbsp;</td>
            <td height="30" align="left">&nbsp;</td>
            <td align="center">&nbsp;</td>
          </tr>
        </table></td>
      </tr>
      <tr>
        <td colspan='2' align="left" valign="top"><table width="640" border="0" cellspacing="0" cellpadding="0" id="maintext">
          <tr>
            <td width="24" align="left" valign="top"><table width="19" height="19" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td align="center" valign="middle" class="numbox">8</td>
              </tr>
            </table></td>
            <td colspan="2"><strong>Optional Modifiers</strong> are used for add ons that are not required but are optional.</td>
          </tr>
          <tr>
            <td>&nbsp;</td>
            <td align="left">
            <table border="0" cellspacing="0" cellpadding="0" id="maintext">
              <tr>
                <td width="20"><img src="images/arrowline_sm.png" width="12" height="12" /></td>
                <td>Select &ldquo;Add Category&rdquo;</td>
              </tr>
              <tr>
                <td><img src="images/pointline_sm.png" width="12" height="12" /></td>
                <td><strong>Name</strong> it &ldquo;Sundae Toppings&rdquo;</td>
              </tr>
              <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
              </tr>
              <tr>
                <td><img src="images/arrowline_sm.png" width="12" height="12" /></td>
                <td>Select &ldquo;Add Items&rdquo;</td>
              </tr>
              <tr>
                <td><img src="images/pointline_sm.png" width="12" height="12" /></td>
                <td><strong>Add Items</strong>: Nuts, Sprinkles, etc</td>
              </tr>
            </table>
              <p><br />
              </p></td>
            <td width="200" align="center"><img src="images/e_op_mods1.jpg" width="200" height="90" /></td>
          </tr>
          <tr>
            <td>&nbsp;</td>
            <td height="30" align="left">&nbsp;</td>
            <td align="center">&nbsp;</td>
          </tr>
        </table></td>
      </tr>
      <tr>
        <td colspan='2' align="left" valign="top"><table width="640" border="0" cellspacing="0" cellpadding="0" id="maintext">
          <tr>
            <td width="24" align="left" valign="top"><table width="19" height="19" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td align="center" valign="middle" class="numbox">9</td>
              </tr>
            </table></td>
            <td>Click back to &quot;Menu Items&quot;</td>
          </tr>
          <tr>
            <td>&nbsp;</td>
            <td align="left"><strong>Assign</strong> the Optional Modifier &ldquo;Sundae Toppings&rdquo; and Modifier Group &ldquo;Sundae Options&rdquo;<br />
to your &ldquo;Ice Cream Sundae&rdquo; Menu Item.</td>
          </tr>
          <tr>
            <td>&nbsp;</td>
            <td align="center"><p><img src="images/e_menu_items1.jpg" width="451" height="54" /><br />
            </p></td>
            </tr>
          <tr>
            <td>&nbsp;</td>
            <td align="center">&nbsp;</td>
          </tr>
        </table></td>
      </tr>
      <tr>
        <td height="30" colspan='2' align="left" valign="top"><br />
          <table width="640" border="0" cellspacing="0" cellpadding="0" id="maintext">
            <tr>
              <td width="24" align="left" valign="top"><table width="19" height="19" border="0" cellspacing="0" cellpadding="0">
                <tr>
                  <td align="center" valign="middle" class="numbox">10</td>
                </tr>
              </table></td>
              <td><strong>Finished! </strong><br />
                Now when you test the POSLavu frontend App, you will see that &ldquo;Ice Cream&rdquo; <br />
              is a &ldquo;Category&rdquo; with a Menu Item of &ldquo;Ice Cream Sundae&rdquo; with the Options assigned.</td>
            </tr>
            <tr>
              <td>&nbsp;</td>
              <td align="left"><p><br />
              </p></td>
            </tr>
        </table></td>
      </tr>
      <tr>
        <td colspan='2' align="left" valign="top">&nbsp;</td>
      </tr>
    </table>

</div>
