<style type="text/css">
<!--
-->
</style>
<div style="position:relative" id="main_content_area">
  <script language='JavaScript' type="text/javascript">function form1_validate() { document.form1.submit();
} </script>
    <table width="640" border="0" align="center" cellpadding="0" cellspacing="0" style='border:solid 0px #aaaaaa'>
      <tr>
        <td width="260" colspan='2' align="left"><table width="520" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td width="40"><img src="images/i_inventory_sm.png" width="30" height="35" /></td>
            <td class="sectiontitle">Inventory Set Up Exercise</td>
          </tr>
        </table></td>
      </tr>
      <tr>
        <td colspan="2" align='center'  valign='top'></p></td>
      </tr>
      <tr>
        <td colspan="2" align='left'  valign='top' class="titletext">
        <table width="640" border="0" cellspacing="0" cellpadding="5" id="maintext">
          <tr>
            <td >&nbsp;</td>
          </tr>
          <tr>
            <td ></td>
          </tr>
          <tr>
            <td align="center"><table width="630" border="0" cellspacing="0" cellpadding="5">
              <tr>
                <td align="center" bgcolor="#e4e4e4" class="maintext" >
                <table border="0" cellspacing="0" cellpadding="0" class="overview_blacktext">
                  <tr>
                    <td width="30" align="left" class="overview_blacktext"><img src="images/i_idea.png" width="20" height="20" /></td>
                    <td width="570">This example will show you how to make an INVENTORY ITEM  connected to a MENU ITEM</td>
                  </tr>
                </table></td>
              </tr>
            </table></td>
          </tr>
          <tr>
            <td>The goal is to track inventory in relation to sales. In this basic example, we will create an Inventory item that decreases in total Units (ounces in this case) when a related Menu Item is purchased.</td>
          </tr>
          <tr>
            <td>&nbsp;</td>
          </tr>
          </table>
        <table width="640" border="0" cellspacing="0" cellpadding="0" id="maintext">
          <tr>
            <td width="24"><table width="19" height="19" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td align="center" valign="middle" class="numbox">1</td>
              </tr>
            </table></td>
            <td>Login to the POSLavu backend and click the &ldquo;Inventory&rdquo; button.</td>
            </tr>
          <tr>
            <td>&nbsp;</td>
            <td><table height="20" border="0" cellspacing="0" cellpadding="2">
              <tr>
                <td width="20" align="left" background=""><img src="images/i_important.png" title="Important!!" width="14" height="14" /></td>
                <td align="left" class="msg">Click <strong>SAVE</strong> ANY TIME you make changes</td>
                <td width="18" align="center" valign="middle" class="msg"><img src="images/arrow_L_sm.png" width="18" height="9" /></td>
                <td width="92" align="left" class="msg"><img src="images/save_btn.png" title="Important!!" width="85" height="16" /></td>
                </tr>
            </table></td>
            </tr>
          <tr>
            <td>&nbsp;</td>
            <td height="30">&nbsp;</td>
            </tr>
        </table></td>
      </tr>
      <tr>
        <td colspan='2' align="left" valign="top">
        <table width="640" border="0" align="center" cellpadding="1" cellspacing="0" id="maintext2" style='border:solid 0px #aaaaaa'>
          <tr>
            <td width="23" height="19" align='left' valign='middle' >
            <table width="19" height="19" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td align="center" valign="middle" class="numbox">2</td>
              </tr>
            </table></td>
            <td align='left' valign='middle' >
            <table  border="0" cellpadding="0" cellspacing="0" id="maintext">
              <tr>
                <td align="left" background="">Click <strong>Add Category</strong></td>
                <td width="18" align="center" valign="middle" class="msg"><img src="images/arrow_L_sm.png" width="18" height="9" /></td>
                <td width="92" align="right" class="msg"><img src="images/btn_add_category.png"  width="123" height="22" /></td>
              </tr>
            </table></td>
          </tr>
          <tr>
            <td height="30"  colspan="2" align='center' valign='middle' >&nbsp;</td>
          </tr>
          <tr>
            <td  colspan="2" align='center' valign='middle' >
            <table width="640" border="0" cellspacing="0" cellpadding="0" id="maintext">
              <tr>
                <td width="24">
                <table width="19" height="19" border="0" cellspacing="0" cellpadding="0">
                  <tr>
                    <td align="center" valign="middle" class="numbox">3</td>
                  </tr>
                </table></td>
                <td>Type in a <strong>Title</strong>: Pizza Cheese</td>
                <td rowspan="2"><img src="images/e_addtitle_inv.jpg" width="136" height="44" /></td>
              </tr>
              <tr>
                <td>&nbsp;</td>
                <td align="right">&nbsp;</td>
              </tr>
            </table></td>
          </tr>
          <tr>
            <td height="30"  colspan="2" align='center' valign='middle' >&nbsp;</td>
          </tr>
        </table></td>
      </tr>
      <tr>
        <td colspan='2' align="left" valign="top">
        <table width="640" border="0" cellspacing="0" cellpadding="0" id="maintext">
          <tr>
            <td width="24">
            <table width="19" height="19" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td align="center" valign="middle" class="numbox">4</td>
              </tr>
            </table></td>
            <td>Click &ldquo;Add Item&rdquo; and type in a <strong>Title</strong>: Mozzarella.</td>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td>&nbsp;</td>
            <td colspan="2" align="center"><img src="images/inv_1.jpg" width="520" height="75" /></td>
            </tr>
          <tr>
            <td>&nbsp;</td>
            <td height="30" colspan="2" align="center">&nbsp;</td>
          </tr>
        </table></td>
      </tr>
      <tr>
        <td colspan='2' align="left" valign="top"><table width="640" border="0" cellspacing="0" cellpadding="0" id="maintext">
          <tr>
            <td width="24" align="left" valign="top">
            <table width="19" height="19" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td align="center" valign="middle" class="numbox">5</td>
                </tr>
              </table></td>
            <td><p>In the &quot;Quantity&quot; field, we want to enter 5 pounds since that is how much we bought. However, when we connect this inventory item to the menu item, we want to use ounces because we use 4 ounces of cheese for each Personal Pizza. Since there are 16 ounces in a pound, we multiplied 16(ounces) times 5 (pounds) and entered &quot;80&quot; for our Quantity. </p>
              <p>In the &quot;Unit&quot; field enter &quot;Oz&quot; for &quot;ounces&quot;.</p>
              <p>For &quot;cost&quot; we will enter &quot;$4.00&quot;. </p></td>
            </tr>
          <tr>
            <td>&nbsp;</td>
            <td height="50">&nbsp;</td>
            </tr>
        </table></td>
      </tr>
      <tr>
        <td colspan='2' align="left" valign="top"><table width="640" border="0" cellspacing="0" cellpadding="0" id="maintext">
          <tr>
            <td width="24" valign="top"><table width="19" height="19" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td align="center" valign="middle" class="numbox">6</td>
              </tr>
            </table></td>
            <td colspan="2">Now return to the <strong>Main Menu</strong> and click <strong>Menu</strong> and we will associate the inventory item with the menu item.</td>
            </tr>
          <tr>
            <td>&nbsp;</td>
            <td align="left"><p><br />
              </p></td>
            <td width="200" align="center">&nbsp;</td>
          </tr>
        </table></td>
      </tr>
      <tr>
        <td colspan='2' align="left" valign="top"><table width="640" border="0" cellspacing="0" cellpadding="0" id="maintext">
          <tr>
            <td width="24" align="left" valign="top"><table width="19" height="19" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td align="center" valign="middle" class="numbox">7</td>
              </tr>
            </table></td>
            <td colspan="2"> Select a menu item such as <strong>Lunch-Personal Pizza. <br />
            </strong>Click the &quot;<strong>Ingredients</strong>&quot; field to open the dialog box.</td>
          </tr>
          <tr>
            <td>&nbsp;</td>
            <td align="left">&nbsp;</td>
            <td align="center">&nbsp;</td>
          </tr>
          <tr>
            <td>&nbsp;</td>
            <td align="left"><table border="0" cellspacing="0" cellpadding="0" id="maintext">
              <tr>
                <td width="20"><img src="images/arrowline_sm.png" width="12" height="12" /></td>
                <td>Click <strong>Add More</strong></td>
              </tr>
              <tr>
                <td><img src="images/pointline_sm.png" width="12" height="12" /></td>
                <td>open the drop down menu</td>
              </tr>
              <tr>
                <td><img src="images/pointline_sm.png" width="12" height="12" /></td>
                <td>Select <strong>Mozzarella</strong> from the list</td>
              </tr>
              <tr>
                <td><img src="images/pointline_sm.png" width="12" height="12" /></td>
                <td>enter &quot;4&quot; into the field  for Units (ounces in this case)</td>
              </tr>
              <tr>
                <td><img src="images/pointline_sm.png" width="12" height="12" /></td>
                <td>click <strong>Apply</strong> to complete the process</td>
              </tr>
            </table></td>
            <td width="240" align="center"><img src="images/inv_2.jpg" width="240" height="149" /></td>
          </tr>
          <tr>
            <td>&nbsp;</td>
            <td align="left"><p><br />
              </p></td>
            <td width="200" align="center">&nbsp;</td>
          </tr>
        </table></td>
      </tr>
      <tr>
        <td colspan='2' align="left" valign="top"><table width="640" border="0" cellspacing="0" cellpadding="0" id="maintext">
          <tr>
            <td width="24" align="left" valign="top"><table width="19" height="19" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td align="center" valign="middle" class="numbox">8</td>
              </tr>
            </table></td>
            <td colspan="2">Use the Front End to purchase the menu item &quot;Personal Pizza&quot;. </td>
          </tr>
          <tr>
            <td>&nbsp;</td>
            <td colspan="2" align="right"><p><img src="images/inv_4.jpg" width="520" height="215" /></p></td>
            </tr>
          <tr>
            <td>&nbsp;</td>
            <td height="30" align="left">&nbsp;</td>
            <td width="240" align="center">&nbsp;</td>
          </tr>
        </table></td>
      </tr>
      <tr>
        <td colspan='2' align="left" valign="top"><table width="640" border="0" cellspacing="0" cellpadding="0" id="maintext">
          <tr>
            <td width="24" align="left" valign="top"><table width="19" height="19" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td align="center" valign="middle" class="numbox">9</td>
              </tr>
            </table></td>
            <td><strong>Finished! </strong><br />
You will see the Inventory has changed when you return to the backend: 80 oz is now 76 to reflect the sale of one Personal Pizza.</td>
          </tr>
          <tr>
            <td>&nbsp;</td>
            <td align="left"><img src="images/inv_3.jpg" width="520" height="73" /></td>
          </tr>
        </table></td>
      </tr>
      <tr>
        <td colspan='2' align="left" valign="top">&nbsp;</td>
      </tr>
    </table>
  <br />
</div>
