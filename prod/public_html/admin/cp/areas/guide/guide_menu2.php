<style type="text/css">
<!--
-->
</style>
<div style="position:relative" id="main_content_area">
  <script language='JavaScript' type="text/javascript">function form1_validate() { document.form1.submit();
} </script>
  
    <table width="530" align="center" style='border:solid 0px #aaaaaa' id="maintext">
      <tr>
        <td align="left">
        <table width="520" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td width="40"><img src="images/i_menu_sm.png" width="30" height="35" /></td>
            <td class="sectiontitle">Menu Set Up Exercises</td>
          </tr>
        </table></td>
      </tr>
      <tr>
        <td align='left' valign='top' class="titletext">You are ready to try out the system!</td>
      </tr>
      <tr>
        <td align='left'  valign='top'>
            <p>Your Lavu iPad POS app will be auto-filled with content based on your Starter Menu selection.<br />
                Take a look at the front of house on your iPad, then look at the back of house as well.
                To see changes made on your front of house, you’ll need to reload settings.
            </p>
            <p>
                Changes you make in the Back of House will be immediately seen in the Front of House,
                but only after you Reload Settings.
            </p>
        </td>
      </tr>
      <tr>
        <td align='left'  valign='bottom' >
            <table width="100%" border="0" cellspacing="0" cellpadding="2">
          <tr>
            <td align="left" valign="top" class="maintext">
                <img src="images/menu_settings.png"  />
            </td>
          </tr>
        </table></td>
      </tr>
      <tr>
        <td align='left'  valign='bottom' >&nbsp;</td>
      </tr>
      <tr>
        <td align='left'  valign='bottom' ><table width="100%" border="0" cellspacing="2" cellpadding="0">
          <tr>
            <td width="50" align="center" valign="top" ><img src="images/i_exercise.png" width="40" height="40" /></td>
            <td align="left" valign="middle" class="titletext">Exercise: Menu Modifiers Made Easy!</td>
          </tr>
          <tr>
            <td align="center" valign="top" >&nbsp;</td>
            <td align="left" valign="top" class="maintext">
                We have created an exercise to get you familiar with Lavu iPad POS. Though not required
                for setup, trying these exercises may help you understand the system.
            </td>
          </tr>
          <tr>
            <td align="center" valign="top" >&nbsp;</td>
            <td align="left" valign="top" class="maintext">&nbsp;</td>
          </tr>
          <tr>
            <td align="center" valign="top" >&nbsp;</td>
            <td align="left" valign="top" class="maintext">
            <table border="0" cellspacing="0" cellpadding="2">
              <tr>
                <td><img src="images/hand_right.png" width="21" height="16" /></td>
                <td align="left" valign="middle"><a href="index.php?mode=guide_exercise1">Forced Modifiers, Optional Modifiers, Modifier Groups Exercise</a></td>
              </tr>
              <tr>
                <td>&nbsp;</td>
                <td align="left" valign="middle" class="maintext">Set up a menu item that allows a customer to select options when ordering while still making sure that required choices are covered.</td>
              </tr>
            </table></td>
          </tr>
          <tr>
            <td align="center" valign="top" >&nbsp;</td>
            <td align="left" valign="top" class="maintext">&nbsp;</td>
          </tr>
        </table></td>
      </tr>
    </table>
 </div>
