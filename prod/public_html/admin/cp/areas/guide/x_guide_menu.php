<style type="text/css">
<!--
-->
</style>
<div style="position:relative" id="main_content_area">
  <script language='JavaScript' type="text/javascript">function form1_validate() { document.form1.submit();
} </script>
    <table width="530" align="center" style='border:solid 0px #aaaaaa' id="maintext">
      <tr>
        <td align="left">
        <table width="520" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td width="40"><img src="images/i_menu_sm.png" width="30" height="35" /></td>
            <td class="sectiontitle">Menu Set Up</td>
          </tr>
        </table></td>
      </tr>
      <tr>
        <td align='left'  valign='top' class="titletext">Choose a Starter Menu</td>
      </tr>
      <tr>
        <td align='left'  valign='top'><p>We have assembled a few Starter Menus to jumpstart the Set-Up process.<br />
        Select one below and you will instantly be able to try the POSLavu application complete with sample menu items that correlate to your Restaurant Type.</p></td>
      </tr>
      <tr>
        <td align='left'  valign='top'>&nbsp;</td>
      </tr>
      <tr>
        <td align='center'  valign='top' bgcolor="#e4e4e4"><p><span class="titletext">Pick the Starter Menu that is closest to the type you will set up. </span></p>
          <table width="100%" border="0" cellspacing="2" cellpadding="0" id="boxlinks">
            <tr>
              <td align="center" valign="middle" class="boxlinks">&nbsp;</td>
              <td align="center" valign="middle" class="boxlinks"><img src="images/type_1.png" width="100" height="60" /></td>
              <td align="center" valign="middle"><span class="boxlinks"><img src="images/type_2.png" width="100" height="60" /></span></td>
              <td align="center" valign="middle"><span class="boxlinks"><img src="images/type_4.png" width="100" height="60" /></span></td>
              <td align="center" valign="middle"><span class="boxlinks"><img src="images/type_3.png" width="100" height="60" /></span></td>
            </tr>
            <tr>
              <td align="center" valign="middle" class="boxlinks"><img src="images/arrow_2.png" width="20" height="20" /></td>
              <td align="center" valign="middle" bgcolor="#ADCD36" class="boxlinks"><table border="0" cellspacing="0" cellpadding="2">
                <tr>
                  <td><img src="images/i_question.png" width="14" height="14" title="Coffee Shop starter menu is best for Quick Serve environments generally using single transactions." /></td>
                  <td align="left" valign="middle" class="boxlinks"><a href="[choose:default_coffee]">Coffee Shop</a></td>
                </tr>
              </table></td>
              <td align="center" valign="middle" bgcolor="#ADCD36"><table border="0" cellspacing="0" cellpadding="2">
                <tr>
                  <td><img src="images/i_question.png" width="14" height="14" title="Select Bar/Lounge to install a Tab Layout menu where ongoing tabs are not tethered to a table." /></td>
                  <td align="left" valign="middle"><span class="boxlinks"><a href="[choose:defaultbar]">Bar/Lounge</a></span></td>
                </tr>
              </table></td>
              <td align="center" valign="middle" bgcolor="#ADCD36"><table border="0" cellspacing="0" cellpadding="2">
                <tr>
                  <td><img src="images/i_question.png" width="14" height="14" title="Pizza Shop menu uses Table layout." /></td>
                  <td align="left" valign="middle"><span class="boxlinks"><a href="[choose:default_pizza_]">Pizza Shop</a></span></td>
                </tr>
              </table></td>
              <td align="center" valign="middle" bgcolor="#ADCD36"><table border="0" cellspacing="0" cellpadding="2">
                <tr>
                  <td><img src="images/i_question.png" width="14" height="14" title="Restaurant starter menu uses Table Layout." /></td>
                  <td align="left" valign="middle"><span class="boxlinks"><a href="[choose:default_restau]">Restaurant</a></span></td>
                </tr>
              </table></td>
            </tr>
          </table>
        <p>&nbsp;</p></td>
      </tr>
      <tr>
        <td align='left'  valign='bottom' >&nbsp;</td>
      </tr>
      <tr>
        <td align='left'  valign='bottom' ><span class="maintextlight">(Your Starter Menu is only used as a guide and does not effect any future Menu Set-Up decisions. You will be able to reverse any choices you make in this initial Set-Up)</span></td>
      </tr>
      <tr>
        <td align='left'  valign='bottom' >&nbsp;</td>
      </tr>
    </table>
</div>
