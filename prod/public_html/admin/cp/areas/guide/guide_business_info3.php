 <table width="530" border="0" align="center" cellpadding="0" cellspacing="0" id="maintext" style='border:solid 0px #aaaaaa'>
      <tr>
        <td width="568" colspan='2' ><table width="520" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td width="40"><img src="images/i_settings_sm.png" width="30" height="35"></td>
            <td class="sectiontitle">Advanced Location Settings</td>
          </tr>
        </table></td>
      </tr>
      <tr>
        <td colspan="2" align='left' valign='top' class="titletext">
            Employee Information and Access
        </td>
      </tr>
      <tr>
        <td height="60" colspan="2" align='left' valign='top' >
            <p>
                Each user has an assigned PIN they must use to access the system. <br />
                Set the time limits and feature access below. This is for your front of house service.
            </p>
            <p>
                <img src="images/i_important.png" title="Important!!" width="14" height="14" />
                Do you want servers to be able to view other servers’ orders? You can choose whether
                or not to allow access.
            </p>
        </td>
      </tr>
      <tr>
        <td colspan="2" align='left' valign='top' bgcolor="#E4E4E4" ><table width="530" border="0" align="center" cellpadding="8" cellspacing="0" id="maintext3" style='border:solid 0px #aaaaaa'>
          <tr>
            <td height="8" colspan="2" align='right' valign='middle' ></td>
          </tr>
          <tr>
            <td width="440" align="right" valign="middle">
                Restrict order access by employee?
            </td>
            <td width="90" align="center" valign="middle"><span >
              <input type='checkbox' name='lock_orders' [lcheckbox:lock_orders] />
            </span></td>
          </tr>
          <tr>
            <td height="8" colspan="2" align="right" valign="middle"></td>
          </tr>
        </table></td>
      </tr>
      <tr>
        <td colspan="2" align='left' valign='top' ><p>&nbsp;</p>
          <table width="530" border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td width="100"><img src="images/employee_pin.jpg" width="80" height="132"></td>
              <td class="maintext"><p>PIN timeout controls the amount of time an employee has to interact with the system before inactivity logs them out. Re-entry requires a PIN code. </p>
              <p>Order screen time limit is set seperately for better control of your operation.</p></td>
            </tr>
            <tr>
              <td height="30">&nbsp;</td>
              <td class="maintext">&nbsp;</td>
            </tr>
          </table></td>
      </tr>
      <tr>
        <td colspan="2" align="right" valign="middle" bgcolor="#E4E4E4">
        <input type='hidden' name='admin_PIN_discount' id='admin_PIN_discount' value="1" />
        <table width="530" border="0" align="center" cellpadding="8" cellspacing="0" id="maintext2" style='border:solid 0px #aaaaaa'>
          <tr>
            <td height="8" colspan="2" align='right' valign='middle' ></td>
            </tr>
          <tr>
            <td width="468" align='right' valign='middle' >PIN time out  (in seconds) <br />
              <table border="0" cellspacing="0" cellpadding="2">
                <tr>
                  <td width="30" align="left"><img src="images/i_idea.png" width="20" height="20" /></td>
                  <td align="left" valign="middle" class="msg">0 disables</td>
                </tr>
              </table></td>
            <td width="100" align="center" valign="middle" ><input type='text' name='PINwaitM' size="8" value="[lsetting:PINwaitM]" /></td>
          </tr>
          <tr>
            <td align='right' valign='middle' >PIN time out while in order screen  (in seconds)<br />
              <table border="0" cellspacing="0" cellpadding="2">
                <tr>
                  <td width="30" align="left"><img src="images/i_idea.png" width="20" height="20" /></td>
                  <td align="left" valign="middle" class="msg">0 disables</td>
                </tr>
              </table></td>
            <td align="center" valign="middle" ><input type='text' name='PINwait' size="8" value="[lsetting:PINwait]" /></td>
          </tr>
          <tr>
            <td align='right' valign='top' >&nbsp;</td>
            <td style='padding-right:32px'>&nbsp;</td>
          </tr>
          <tr>
            <td align='right' valign='middle' >Require admin PIN to select receipt printer? </td>
            <td align="center" valign="middle" ><input type='hidden' name='admin_PIN_terminal_set2' id='admin_PIN_terminal_set2' value="1" />
              <input type='checkbox' name='admin_PIN_terminal_set' [lcheckbox:admin_PIN_terminal_set] /></td>
          </tr>
          <tr>
            <td align='right'  valign='middle'>Require admin PIN to print daily (till) report?</td>
            <td align="center" valign="middle" ><input type='hidden' name='admin_PIN_till_report2' id='admin_PIN_till_report2' value="1" />
              <input type='checkbox' name='admin_PIN_till_report' [lcheckbox:admin_PIN_till_report] /></td>
          </tr>
          <tr>
            <td align="right" valign="middle">&nbsp;</td>
            <td align="center" valign="middle">&nbsp;</td>
          </tr>
          <tr>
            <td align="right" valign="middle">Require admin PIN for discounts?</td>
            <td align="center" valign="middle"><input type='hidden' name='admin_PIN_discount2' id='admin_PIN_discount2' value="1" />
              <input type='checkbox' name='admin_PIN_discount' [lcheckbox:admin_PIN_discount]  /></td>
          </tr>
          <tr>
            <td align="right" valign="middle">Require admin PIN to void an order?</td>
            <td align="center" valign="middle"><input type='hidden' name='admin_PIN_void2' id='admin_PIN_void2' value="1" />
             <input type='checkbox' name='admin_PIN_void' [lcheckbox:admin_PIN_void]  /></td>
          </tr>
          <tr>
            <td height="8" colspan="2" align="right" valign="middle">&nbsp;</td>
            </tr>
        </table></td>
      </tr>
    </table>