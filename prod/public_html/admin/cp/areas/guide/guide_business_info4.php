 <table width="530" align="center" style='border:solid 0px #aaaaaa' id="maintext">
      <tr>
        <td colspan='2' align="left" valign="top" >
        <table width="520" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td width="40"><img src="images/i_settings_sm.png" width="30" height="35"></td>
            <td class="sectiontitle">Advanced Location Settings</td>
          </tr>
        </table></td>
      </tr>
      <tr>
        <td colspan='2' align="left" valign="top" class="titletext">Advanced Settings</td>
      </tr>
      <tr>
        <td colspan="2" align='left'  valign='top' bgcolor="#e4e4e4"><table width="530" border="0" align="center" cellpadding="8" cellspacing="0" id="maintext4" style='border:solid 0px #aaaaaa'>
          <tr>
            <td height="8" colspan="2" align='right' valign='middle' ></td>
          </tr>
          <tr>
            <td width="468" align='right' valign='middle' >Would you like to allow orders to be re-sent to the kitchen,<br>
              so they may be reprinted? (recommended)</td>
            <td width="100" align="left" valign="middle" >
             <input type='checkbox' name='allow_resend' [lcheckbox:allow_resend] />
             </td>
          </tr>
          <tr>
            <td align='right' valign='top' >Minimum number of guests required to add gratuity automatically:</td>
            <td style='padding-right:32px'><input type='text' name='ag1' size="8" value="[lsetting:ag1]" /></td>
          </tr>
          <tr>
            <td align='right' valign='middle' ><span style="padding-left:32px;">Gratuity percent (enter as a decimal- ex: .2 = 20%)                            :</span></td>
            <td align="left" valign="middle" ><input type='hidden' name='admin_PIN_terminal_set3' id='admin_PIN_terminal_set3' value="1" />
             <input type='text' name='ag2' size="8" value="[lsetting:ag2]" /></td>
          </tr>
        </table>
        </td>
      </tr>
      <tr>
        <td colspan='2'></td>
      </tr>
      <tr>
        <td width="468" align='right' valign='middle' style='padding-left:32px;'>&nbsp;</td>
        <td width="50"></td>
      </tr>
    </table>