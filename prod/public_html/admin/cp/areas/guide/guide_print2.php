<style type="text/css">
<!--
-->
</style>
<div style="position:relative" id="main_content_area">
  <script language='JavaScript' type="text/javascript">function form1_validate() { document.form1.submit();
} </script>
        <table width="530" border="0" align="center" cellpadding="0" cellspacing="0" style='border:solid 0px #aaaaaa'>
      <tr>
        <td width="260" colspan='2' align="left"><table width="520" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td width="40"><img src="images/i_menu_sm.png" width="30" height="35" /></td>
            <td class="sectiontitle">Set Up Your Menu</td>
          </tr>
        </table></td>
      </tr>
      <tr>
        <td colspan="2" align='center'  valign='top'>
        <table width="530" border="0" cellspacing="0" cellpadding="0" id="maintext">
          <tr>
            <td class="titletext">Printing Done Right</td>
          </tr>
          <tr>
            <td><p>No one wants to set up their printer twice, right? Follow our lead and be sure to set up your hardware to spec. If you do, you will have the best experience with Lavu iPad POS.</p>
              <p>Use Thermal Printers for the Custormer Receipt printer.</p>
              <ul>
                <li>They are faster than Impact Printers.</li>
                <li>They far more quiet than Impact Printers.</li>
              </ul>
              <p>Use Impact Printers for the Kitchen. </p>
              <ul>
                <li>They are loud. This can help staff be aware of a new order.</li>
                <li>They don't use Thermal paper. Heat in the kitchen can turn the paper black.</li>
              </ul>              </td>
          </tr>
          <tr>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td>After you set up your printers and Before you begin using the system with customers, <strong>test multiple times on each printer.</strong></td>
          </tr>
          <tr>
            <td>&nbsp;</td>
          </tr>
          </table>
          </p></td>
      </tr>
      <tr>
        <td colspan='2' align="left" valign="top">&nbsp;</td>
      </tr>
    </table>
</div>
