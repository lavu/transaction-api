<?php

	session_start();
	//require_once($_SERVER['DOCUMENT_ROOT']."/resources/core_functions.php");
	//$week_query  = mlavu_query("SELECT * FROM `poslavu_MAIN_db`.`messages` WHERE `_deleted`='0' AND DATE_SUB(CURDATE(),INTERVAL 30 DAY) <= DATE(`timestamp`);  ");

	$html= "
	<script type='text/javascript'>
		function open_feature_content(content){
			document.getElementById('overlay').style.display='block';
			document.getElementById('overlay_content').innerHTML= content;
		}
		function close_overlay(elem){
			elem.parentNode.style.display='none';
		}
	</script>
	<table style='border-collapse:collapse'>
		<tr>
			<th colspan='3'>".speak('Message Log')."</th>
		</tr>
		<tr>
			<td style='text-align:center; border:1px solid black; background-color:#aecd37; color:white;'>Date</td>
			<td style='text-align:center; border:1px solid black; background-color:#aecd37; color:white;'>Title</td>
			<td style='text-align:center; border:1px solid black; background-color:#aecd37; color:white;'>Message</td>
		</tr>";
		require_once($_SERVER['DOCUMENT_ROOT']."/cp/resources/messagesDB.php");
		$messagesDB = new messagesDB();
		$messages = $messagesDB->getMessages();
		$maxMessages = 50;
		if(count($messages) < $maxMessages){
			$maxMessages = count($messages);
		}
		// based on date format setting
		$date_setting = "date_format";
		$location_date_format_query = lavu_query( "SELECT `location`, `setting`, `value` FROM `config` WHERE `setting` = '[1]'",  $date_setting);
		if( !$location_date_format_query || !mysqli_num_rows($location_date_format_query)){
			$date_format = array("value" => 2);
		}else
			$date_format = mysqli_fetch_assoc( $location_date_format_query );

		for ($x = 0; $x < $maxMessages; $x++) {
			$message = $messages[$x];

			$split_date = explode(" ", $message['created_date']);
			$change_format = explode("-", $split_date[0]);
			switch ($date_format['value']){
				case 1:$message['created_date'] = $change_format[2]."-".$change_format[1]."-".$change_format[0]." ".$split_date[1];
				break;
				case 2:$message['created_date'] = $change_format[1]."-".$change_format[2]."-".$change_format[0]." ".$split_date[1];
				break;
				case 3:$message['created_date'] = $change_format[0]."-".$change_format[1]."-".$change_format[2]." ".$split_date[1];
				break;
			}

			$date	 = $message['created_date'];
			$title 	 = $message['title'];
			$content = $message['content'];
			//echo $content;
		
			if(strlen($content) > 50 )
				$content_trimmed= strip_tags(substr($content,0,50));
			else
				$content_trimmed= strip_tags($content);
			
			$html.="
			<tr>
				<td style='border:1px solid black;padding:5px'>{$date}</td>
				<td style='border:1px solid black;padding:5px'>{$title}</td>
				<td class='feature_content' onclick=\"open_feature_content($(this).children().html())\" style='padding:5px;'>{$content_trimmed}...<div id='dialog' class='feature_content_all'>$content</div> </td>
			</tr>";
		}
	$html.="</table>
		<div id='overlay'>
			<div id='close_overlay' onclick='close_overlay(this)'></div>
			<div id='overlay_content'>
			</div>
		</div>
		
		
	";
	echo $html;
?>