<?php
	// LP-9721 (Remove and disable LTG 1.0 from all accounts)
    echo "<h1>404 Not Found</h1>";
    exit;
	if ($in_lavu)
	{
		$lavu_only		= "<font color='#588604' style='font-size:14px'><b>*</b></font> ";
		$forward_to		= "index.php?mode=".$section."_".$mode;
		$tablename		= "locations";
		$rowid			= $locationid;

		$notice_times = array(
			'5'		=> "5 minutes",
			'10'		=> "10 minutes",
			'15'		=> "15 minutes",
			'20'		=> "20 minutes",
			'25'		=> "25 minutes",
			'30'		=> "30 minutes",
			'35'		=> "35 minutes",
			'40'		=> "40 minutes",
			'45'		=> "45 minutes",
			'50'		=> "50 minutes",
			'55'		=> "55 minutes",
			'60'		=> "1 hour",
			'120'	=> "2 hours",
			'180'	=> "3 hours",
			'240'	=> "4 hours",
			'300'	=> "5 hours",
			'360'	=> "6 hours",
			'420'	=> "7 hours",
			'480'	=> "8 hours",
			'540'	=> "9 hours",
			'600'	=> "10 hours",
			'660'	=> "11 hours",
			'720'	=> "12 hours",
			'780'	=> "13 hours",
			'840'	=> "14 hours",
			'900'	=> "15 hours",
			'960'	=> "16 hours",
			'1020'	=> "17 hours",
			'1080'	=> "18 hours",
			'1140'	=> "19 hours",
			'1200'	=> "20 hours",
			'1260'	=> "21 hours",
			'1320'	=> "22 hours",
			'1380'	=> "23 hours",
			'1440'	=> "1 day",
			'2880'	=> "2 days",
			'4320'	=> "3 days"
		);

		// supported phone carriers for LTG
		$phone_carriers = array(
			'att'		=> "AT&T",
			'sprint'		=> "Sprint",
			'tmobile'	=> "T-Mobile",
			'verizon'	=> "Verizon"
		);

		$delivery_options = array(
			'Pickup Only'			=> speak("Pickup Only"),
			'Delivery and Pickup'	=> speak("Delivery and Pickup")
		);

		$payment_options = array(
			1 => speak("Prepay Only")." (".speak("credit card integration required").")",
			2 => speak("Payment Upon Receipt Only"),
			3 => speak("Prepay and Payment Upon Receipt")
		);

// - - - - - - - - Lavu ToGo Online Ordering - - - - - - - - //
		$fields = array(
			array(
				"Lavu ToGo ".speak("Online Ordering 1.0"),
				"Lavu ToGo Online Ordering",
				"seperator"
			),
			array(
				speak("Enable")." Lavu ToGo 1.0:",
				"config:use_lavu_togo",
				"bool"
			)
		);

		$fields = array_merge($fields, array(
			array(
				"ToGo ".speak("Type").":",
				"config:use_togo_type",
				"hidden",
				"defaultvalue:Lavu ToGo"
			),
			array(
				speak("Use Optional Modifiers").":",
				"config:ltg_use_opt_mods",
				"bool"
			),
			array(
				"Lavu ToGo ".speak("Unique URL").": <strong style='color:#336699'>https://www.lavutogo.com/</strong>",
				"config:lavu_togo_name",
				"text",
                "size:30"
			),
			array(
				"Lavu ToGo ".speak("Embed code")." (".speak("copy/paste it into your website")."):",
				"",
				""
			),
			array(
				"<div id='ltg_embed' style='width:600px; border:1px solid #ccc; padding:3px; margin:auto;'>&nbsp;</div>",
				"",
				"custom"
			),

// - - - - - - - - Lavu ToGo General Settings - - - - - - - - //

			array(
				"Lavu ToGo ".speak("General Settings"),
				"Lavu ToGo General Settings",
				"seperator"
			),
			array(
				"Lavu ToGo ".speak("Start Time").":",
				"config:lavu_togo_start_time",
				"time_no_late_night"
			),
			array(
				"Lavu ToGo ".speak("End Time").":",
				"config:lavu_togo_end_time",
				"time_no_late_night"
			)
		));

		if (is_lavu_admin())
		{
			$fields[] = array(
				$lavu_only.speak("Days Open").":",
				"config:lavu_togo_days_open",
				"day_of_week"
			);
		}

		$fields = array_merge( $fields, array(
			/*array(
				"Lavu ToGo register setting:",
				"config:lavu_togo_register_setting",
				"select",
				$register_settings_array
			),*/
			array(
				speak("Number of days to allow orders in advance").":",
				"config:max_advance_days",
				"select",
				"0,1,2,3,4,5,6,7"						// comma delimited list for select options
			),
			array(
				speak("Minimum preparation time for order fulfillment").":",
				"config:minimum_notice_time",
				"select",
				$notice_times							// array for select options
			),
			array(
				speak("Enable in-app order polling").":",
				"config:enable_togo_polling",
				"bool"
			)
			/*array(
				speak("Seconds to elapse between order scans").":",
				"config:order_scan_seconds",
				"select",
				"30,45,60,75,90,105,120"
			)*/
		));

		/*if (is_lavu_admin())
		{
			$fields[] = array(
				$lavu_only.speak("Minutes prior to pickup time to have to go orders print automatically").":",
				"config:auto_print_minutes",
				"select",
				"5,10,15,20,25,30,35,40,45,50,55,60"
			);
		}*/

		$fields = array_merge( $fields, array(
			/*array(
				speak("Sound alarm when auto print fails").":",
				"config:ltg_print_failed_alarm",
				"bool"
			),*/
			array(
				speak("Cell phone carrier for order alert text messages").":",
				"config:ltg_phone_carrier",
				"select",
				$phone_carriers
			),
			array(
				speak("Phone number for order alert notifications")." (".speak("format").": 5051234567):",
				"config:ltg_phone_number",
				"text",
                "size:30"
			),
			array(
				speak("Email for order alert notifications").":",
				"config:ltg_email",
				"text",
                "size:30"
			),

// - - - - - - - - Lavu ToGo Customer Settings - - - - - - - - //

		array(
				"Lavu ToGo ".speak("Customer Settings"),
				"Lavu ToGo Customer Settings",
				"seperator"
			),
			array(
				speak("Customer Delivery Option").":",
				"config:ltg_delivery_option",
				"select",
				$delivery_options
			),
			array(
				speak("Customer Payment Options").":",
				"config:ltg_payment_option",
				"select",
				$payment_options
			)
		));

		if (is_lavu_admin())
		{
			$fields = array_merge( $fields, array(
				array(
					$lavu_only.speak("Enable order fetching for local server").":",
					"config:lls_fetch_api_orders",
					"bool"
				),

// - - - - - - - - Lavu ToGo Technical Settings - - - - - - - - //

				array(
					$lavu_only."Lavu ToGo Technical Settings",
					"Lavu ToGo Technical Settings",
					"seperator"
				),
				array(
					$lavu_only."Lavu Central Server URL for Lavu ToGo Transactions:",
					"config:ltg_use_central_server_url",
					"text",
                    "size:30"),
				array(
					$lavu_only."Enable Lavu ToGo debugging:",
					"config:ltg_debug",
					"bool")
			));
		}

		$fields[] = array(
			speak("Save"),
			"lavutogo_submit",
			"lavutogo_submit"
		);

		require_once(resource_path()."/form.php");

		if (is_lavu_admin())
		{
			echo "<br>"."Indicates items visible only to Lavu Admin users (i.e., developers and support staff)";
		}

		if (isset($_GET['show_connector']))
		{
			echo "<br><br>Location Id: ".$locationid;
			echo "<br>Connector Name: ".admin_info("dataname");
			echo "<br>Connector Key: ".lsecurity_key(admin_info("companyid"));
		}
		echo "<br><br>";
	}
?>
<script type="text/javascript">

	if (document.form1.elements['config:use_lavu_togo'].value=="1" && document.form1.elements['config:lavu_togo_name'].value!="")
	{
		var ltgname = document.form1.elements['config:lavu_togo_name'].value;
		var iframecode = "<iframe src='https://www.lavutogo.com/" + ltgname + "' width='900' height='800' frameborder='0' scrolling='no'></iframe>";
		// build iframe code for embeddable ltg
		document.getElementById('ltg_embed').innerHTML = "<pre style='white-space:normal;' id='ifc'>" + iframecode.replace(/&/g, "&amp;").replace(/</g, "&lt;") + "</pre>";
		document.getElementById('ifc').focus();
	}
	else
	{
		document.getElementById('ltg_embed').style.display = "none";
	}
	/**
	 * This method is used to check The Lavu To Go Unique URL and call ajax page 
	 * @ POst data : lavu_togo_name
	 * @return string  Success | Failure
	 */
	$('#_setting_btn_submit_id').click(function(){
		var ltgname = document.form1.elements['config:lavu_togo_name'].value;
		$.ajax({
			url: "areas/validate_ltg_url.php",
			data: { ltg_restaurant_name : ltgname },
			type: "POST",
			async: true,
			success: function(data) {
				var json = JSON.parse(data);
				if (json.status == 'Failure') {
					alert(json.msg);
					return false;
				} else {
					document.form1.submit();
				}
			},
			error: function() {
				alert("Seems problem with server loading the Lavu To Go URL");
			}
		});
		return false;
	});
</script>