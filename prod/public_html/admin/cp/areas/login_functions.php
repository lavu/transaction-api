<?php

function update_cp_login_status($success,$username)
{
	global $maindb;
	if($success)
		$set_field = "succeeded";
	else
		$set_field = "failed";
		
	$ipaddress = getClientIp() || $_SERVER['REMOTE_ADDR'];
	$mints = time() - (60 * 15);
	$currentts = time();
	$currentdate = date("Y-m-d H:i:s");

	// log to error log ip attempting to log in
	error_log("[log-in] count REMOTE_ADD: " . $_SERVER['REMOTE_ADDR'] . " REAL IP: " . $ipaddress);
	
	$li_query = mlavu_query("select * from `$maindb`.`login_log` where `ipaddress`='$ipaddress' and `$set_field`>'0' and `ts`>='$mints'");
	if(mysqli_num_rows($li_query))
	{
		$li_read = mysqli_fetch_assoc($li_query);
		$li_id = $li_read['id'];
		mlavu_query("update `$maindb`.`login_log` set `ts`='$currentts', `date`='$currentdate', `users`=CONCAT(`users`,',[1]'), `$set_field`=`$set_field`+1 where `id`='$li_id'",$username);
	}
	else
	{
		mlavu_query("insert into `$maindb`.`login_log` (`ts`,`date`,`$set_field`,`ipaddress`,`users`) values ('$currentts','$currentdate','1','$ipaddress','[1]')",$username);
	}
}

function get_cp_login_count($field)
{
	global $maindb;
	
	$ipaddress = $_SERVER['REMOTE_ADDR'];
	$mints = time() - (60 * 15);

	// log to error log ip attempting to log in
	error_log("[log-in] count REMOTE_ADD: " . $_SERVER['REMOTE_ADDR'] . " REAL IP: " . $ipaddress);
	
	$li_query = mlavu_query("select * from `$maindb`.`login_log` where `ipaddress`='$ipaddress' and `$field`>'0' and `ts`>='$mints'");
	if(mysqli_num_rows($li_query))
	{
		$li_read = mysqli_fetch_assoc($li_query);
		return $li_read[$field];
	}
	else return false;
}

function getClientIp(){
	foreach (array('HTTP_CLIENT_IP', 'HTTP_X_FORWARDED_FOR', 'HTTP_X_FORWARDED', 'HTTP_X_CLUSTER_CLIENT_IP', 'HTTP_FORWARDED_FOR', 'HTTP_FORWARDED', 'REMOTE_ADDR') as $key){
			if (array_key_exists($key, $_SERVER) === true){
					foreach (explode(',', $_SERVER[$key]) as $ip){
							$ip = trim($ip); // just to be safe
							if (filter_var($ip, FILTER_VALIDATE_IP, FILTER_FLAG_NO_PRIV_RANGE | FILTER_FLAG_NO_RES_RANGE) !== false){
									return $ip;
							}
					}
			}
	}
}

?>