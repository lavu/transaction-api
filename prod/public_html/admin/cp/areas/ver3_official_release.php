<style>
	.pr_header { background-color:#849e20; color:#ffffff; font-family:Verdana, Geneva, sans-serif; font-size:16px; }
	.pr_general_text { font-family:Verdana, Geneva, sans-serif; font-size:12px; color:#555555; }
	.pr_title { font-size:18px; color:#aaaaaa; }
	.pr_subheader { color:#000000; font-size:14px; }
	.pr_subheader a:link,a:visited { color:#445510; font-size:14px; }
	.pr_subheader a:hover { color:#849e20; font-size:14px; }
	.pr_list_title { font-weight:bold }
	.pr_sig { font-weight:bold; }
</style>

<input type='button' value='<< Back' onclick='window.location = "?mode=home_home"' /><br><br>

<table bgcolor='#f2f6f2' style='border: solid 1px #849e20' cellpadding=12 width='600'>
	<tr><td align='center' class='pr_header' width='600'>
            Lavu POS Version 3
            <br>Official Release!
    </td></tr>
    <tr><td align='center'>
            <img src='/cp/images/Lavu_Monster_Icon_Large.png' />
    </td></tr>
    <tr><td align='justify' class='pr_subheader'>
        <font class='pr_general_text' style='color:blue'><b>IMPORTANT:</b> Merchants using Lavu Local Server (LLS) <u>should not</u> update at this time. Lavu POS 3.0 requires an LLS update that will be available soon. If you are running LLS, do not change the version of Lavu POS you are currently running.</font><br><br>
        <font class='pr_general_text'><b>We recommend merchants avoid updating business critical systems immediately before or during service of any kind. Any update should happen outside of normal operating hours.</b></font><br>
    	<p align='center'><a href='https://support.lavu.com/hc/en-us/articles/200069713-Upgrading-the-POSLavu-application-' target='_blank'>(Click to view best practices for updating)</a></p>
    </td></tr>
    <tr><td align='left' class='pr_general_text'>
    
   <font class='pr_title'>New Features & Improvements</font>
    
    <ul>
    	<li><font class='pr_list_title'>Background Order Syncing</font>
    		<br>We’ve improved the way Lavu POS sends information to the database, making for a seamless user experience. This feature will help users with slow Internet or poor network conditions and improves the workflow for all users. Credit card transactions will continue to experience connection speeds consistent with your Internet / network, when contacting your MSP’s gateway. However, you should see vast improvements in app-side order speed, especially on larger orders and split checks.
    	</li><br>
        <li><font class='pr_list_title'>Cloud / DB Status Pop Up</font>
            <br>A quick and easy way to check your device's connection to the Internet and our cloud servers. Just tap the cloud icon in the top right corner to see connections, byte information, and sync status for items / orders.
        </li><br>
    
    	<li><font class='pr_list_title'>Check Splitting Enhancement</font>
    		<br>Finally! You’ve got a bunch of checks and want to split items over some, but not all checks? We’ve got you covered.
		    <br><br>
            <table width='100%'><tr><td align='center' class='pr_subheader'><a href='http://youtu.be/bBybPsua4dw' target='_blank'>(Click here to check out the split check video)</a></td></tr></table>
    	</li><br>
    	<li><font class='pr_list_title'>Menu Layout</font>
    		<br>* Up to 60% more menu items are accessible with just one tap.
    		<br>* Adjust the menu icon style: rounded corners or square.
    		<br>* Max menu category icons displayed without scrolling: 7, 8, or 9.
    		<br>* Menu items per row: 4 or 5.
    	</li><br>
    	<li><font class='pr_list_title'>Lavu Gift</font></li>
    	<li><font class='pr_list_title'>Lavu Loyalty</font></li>
    	<li><font class='pr_list_title'>New App Icon</font></li>
        <li><font class='pr_list_title'>Kitchen Printer Buzzer</font></li>
    	<li><font class='pr_list_title'>Evo Snap credit card integration</font></li>
    	<li><font class='pr_list_title'>Moneris Canada credit card integration</font></li>
    	<li><font class='pr_list_title'>Last, but not least …. Lots of bugs squashed!</font></li>
    </ul>
    
    <font class='pr_title'>Lavu Retro</font>
    <br><br>Downloaded Lavu POS version 3 but you’re not ready to update yet? Lavu Retro represents the best of the 2.3.x series of the POS Lavu Client app.
    
    </td></tr>
	<tr><td width='100%' align='center' class='pr_general_text'>
        <img src='/cp/images/Lavu_Retro_Icon_Large.png' />
    </td></tr>
    <tr><td align='left' class='pr_general_text'>
    Lavu Retro version 2.3.13 is available in the App Store and it’s free! Enjoy the familiar look and feel of POS Lavu version 2.3.13, as well as all its benefits and features. We do not recommend using a combination of different apps (Lavu Retro and Lavu POS) at the same location, at the same time. All location devices should be running either Lavu Retro or Lavu POS, but not both simultaneously.
    
    </td></tr>
    <tr><td align='center' class='pr_general_text'>
    <br><br>Thank You and Enjoy!
    <br><font class='pr_sig'>- The Lavu Team</font>
    <br><br>
</td></tr></table>
