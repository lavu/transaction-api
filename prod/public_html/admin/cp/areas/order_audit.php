<?php
	if($in_lavu)
	{
		require_once(dirname(__FILE__) . "/../../lib/lds/LdsDataValidation.php");
		$conn = ConnectionHub::getConn('rest');
		$match_count = 0;
		$mismatch_count = 0;
		$mismatch_str = "";
		$min_days_ago = 30;
		if(isset($_GET['ago']) && is_numeric($_GET['ago']) && $_GET['ago'] * 1 < 3650)
		{
			$min_days_ago = $_GET['ago'];
		}
		$min_datetime = date("Y-m-d",(time() - 60 * 60 * 24 * $min_days_ago)) . " 00:00:00";
		$order_query = lavu_query("select * from `orders` where `opened`>='[1]'",$min_datetime);
		while($order_read = mysqli_fetch_assoc($order_query))
		{
			$order_id = $order_read['order_id'];
			$content_tax_total = 0;
						
			$content_query = lavu_query("select * from `order_contents` where `order_id`='[1]'",$order_id);
			while($content_read = mysqli_fetch_assoc($content_query))
			{
				$altrow = LdsValidateData("order_contents", $order_id, $order_read['ioid'], $order_read['no_of_checks'], $content_read);
				if($altrow != $content_read)
				{
					$mismatch_count++;
					$mismatch_str .= "<font style='color:#880000'>Found Mismatch: $order_id - ".$content_read['id']."</font><br>";
					$mismatch_str .= "subtotal with mods: " . $content_read['subtotal_with_mods'] . "<br>";
					$mismatch_str .= "<font style='color:#888888'>discount value: " . $content_read['discount_value'] . "</font><br>";
					$mismatch_str .= "after discount: " . $content_read['after_discount'] . "<br>";
					$mismatch_str .= "<font style='color:#888888'>tax_rate1: " . $content_read['tax_rate1'] . "</font><br>";
					$mismatch_str .= "tax_amount: " . $content_read['tax_amount'] . "<br>";

					$mismatch_str .= "<font style='color:#558855'>Fixed:<br>";
					$mismatch_str .= "subtotal with mods: " . $altrow['subtotal_with_mods'] . "<br>";
					$mismatch_str .= "<font style='color:#888888'>discount value: " . $altrow['discount_value'] . "</font><br>";
					$mismatch_str .= "after discount: " . $altrow['after_discount'] . "<br>";
					$mismatch_str .= "<font style='color:#888888'>tax_rate1: " . $altrow['tax_rate1'] . "</font><br>";
					$mismatch_str .= "tax_amount: " . $altrow['tax_amount'] . "<br>";
					$mismatch_str .= "</font>";
					$mismatch_str .= "<br>";
					
					$row = array();
					$row['id'] = $content_read['id'];
					$row['order_id'] = $content_read['order_id'];
					$row['after_discount'] = LdsPrNumber($calc_after_discount);
					$row['tax_subtotal1'] = LdsPrNumber($calc_after_discount);
					$row['tax_amount'] = LdsPrNumber($calc_tax_amount);
					$row['tax1'] = LdsPrNumber($calc_tax_amount);
					$row['total_with_tax'] = LdsPrNumber($calc_after_discount + $calc_tax_amount);
					
					$success = lavu_query("update `order_contents` set `after_discount`='[after_discount]',
																		`tax_subtotal1`='[tax_subtotal1]',
																		`tax_amount`='[tax_amount]',
																		`total_with_tax`='[total_with_tax]'
															where `id`='[id]' and `order_id`='[order_id]'",$row);
					if($success) $mismatch_str .= "--------Query Succeeded (".$conn->affectedRows()." affected)<br>";
					else $mismatch_str .= "--------Query Failed<br>";
				}
				else 
				{
					$match_count++;
					//echo "<font style='color:#008800'>Match: $order_id - ".$content_read['id']."</font><br>";
				}
			}
			
			$altorder = LdsValidateData("orders", $order_id, $order_read['ioid'], $order_read['no_of_checks'], $order_read);
			if($altorder != $order_read)
			{
				$mismatch_count++;
				$mismatch_str .= "Order Tax Does Not Add Up: " . $order_read['order_id'] . " " . $order_read['opened'] . " " . $order_read['tax'] . "=".$order_read['total']." / " . $altorder['tax'] . "=".$altorder['total']."<br>";
				$success = lavu_query("update `orders` set `tax`='[1]',`total`='[2]' where `id`='[3]' and `order_id`='[4]'",$altorder['tax'],$altorder['total'],$order_read['id'],$order_read['order_id']);
				if($success)
				{
					$mismatch_str .= "--------Query Succeeded (".$conn->affectedRows()." affected)<br>";
				}
				else $mismatch_str .= "--------Query Failed<br>";
			}
			else
			{
				$match_count++;
			}
		}
		echo "<font style='color:#008800'>Match Count: $match_count</font><br>";
		echo "<font style='color:#aaaaaa'>MisMatch Count: $mismatch_count</font><br>";
		echo "<br>";
		echo $mismatch_str;
		echo "<br><br>";
	}
?>
