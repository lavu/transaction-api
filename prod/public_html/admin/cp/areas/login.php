<?php
require_once('white_list.php');

if ((getenv('DEV')=='1' && !defined('DEV')) || $dev_flag=='1') {
       define('DEV', 1);
} else if (!defined('DEV')) {
       define('DEV', 0);
}

if( DEV ){
	function logMail( $to, $subject, $message, $header ){
		$log_fmt = <<<LOG
	{$header}
	To: {$to}
	Subject: {$subject}

	Message:
	{$message}
LOG;
		error_log( $log_fmt );
	}
}
if(session_id() == ''){
	session_start();
}
ini_set("display_errors",1);
if(!$in_lavu)
{
	return;
}

function show_new_pass_area($token, $username){
	return <<<HTML
	<script type="text/javascript">
	window.onload = function( token, username ){
		document.getElementById('token_field').value		= '{$token}';
		document.getElementById('username_action').value	= '{$username}';
		swapToSection( 'reset_password_section' );
	}
	</script>
HTML;
}

if( !defined( 'LAVU_MINIMUM_LOGIN_LEVEL' ) ){
	define( 'LAVU_MINIMUM_LOGIN_LEVEL', 3 );
}

$extra_scripts = '';
$login_message = '';
if( isset($_GET['mode']) && $_GET['mode']=='reset_password' && isset($_GET['token']) && !empty($_GET['token'])) {
	$query = mlavu_query("SELECT * FROM `poslavu_MAIN_db`.`password_reset_tokens` where `token`='[1]'  and `valid`='1' ", $_GET['token']);
	if(mysqli_num_rows($query)){
		$res = mysqli_fetch_assoc($query);
		$extra_scripts .= show_new_pass_area($_GET['token'],$res['username']);
	}
} else if(isset($_POST['mode']) && $_POST['mode'] == 'perform_reset_pass' ) {
	$query = mlavu_query("SELECT * FROM `poslavu_MAIN_db`.`password_reset_tokens` where `valid`='1' and `token`='[1]' and `username`='[2]'", $_POST['token'], $_POST['username_action']);
	if(mysqli_num_rows($query)){
		$res = mysqli_fetch_assoc($query);
		if( ($_POST['password1'] != $_POST['password2']) || $_POST['password1']==''){
			$login_message= "Passwords do not match";
		}else{
			mlavu_query("UPDATE `poslavu_[1]_db`.`users` set `password`=PASSWORD('[2]') where `username`='[3]' limit 1", $res['data_name'], $_POST['password1'], $res['username']);
			mlavu_query("UPDATE `poslavu_MAIN_db`.`password_reset_tokens` SET `valid`='0' WHERE `username`='[1]' and `token`='[2]'",$res['username'], $_POST['token']);
			$_POST['username'] = $res['username'];
			$_POST['password1'] = $_POST['password2'];

			echo "{ \"status\": \"success\", \"forward\": \"\"}";
			return;
		}
	}else{
		$login_message = "Error Token is invalid";

	}

	echo "{\"status\":\"error\", \"message\":\"{$login_message}\"}";
	return;
}

if(isset($_POST['username']) && isset($_POST['password'])) {

	function update_cp_login_status($success,$username) {
		global $maindb;
		if($success)
			$set_field = "succeeded";
		else
			$set_field = "failed";

		$ipaddress = $_SERVER['REMOTE_ADDR'];
		$mints = time() - (60 * 15);
		$currentts = time();
		$currentdate = date("Y-m-d H:i:s");

		$li_query = mlavu_query("select * from `[1]`.`login_log` where `ipaddress`='[2]' and `[3]`>'0' and `ts`>='[4]' and `users`='[5]'",$maindb,$ipaddress,$set_field,$mints, $username);
		if(mysqli_num_rows($li_query))
		{
			$li_read = mysqli_fetch_assoc($li_query);
			$li_id = $li_read['id'];
			mlavu_query("update `[2]`.`login_log` set `ts`='[3]', `date`='[4]', `[5]`=`[5]`+1 where `id`='[6]'",$username,$maindb,$currentts,$currentdate,$set_field,$li_id);
		}
		else
		{
			mlavu_query("insert into `[2]`.`login_log` (`ts`,`date`,`[3]`,`ipaddress`,`users`) values ('[4]','[5]','1','[6]','[1]')",$username,$maindb,$set_field,$currentts,$currentdate,$ipaddress);
		}
	}

	function get_cp_login_count($field, $username) {
		global $maindb;

		$ipaddress = getClientIp() || $_SERVER['REMOTE_ADDR'];
        $mints = time() - (60 * 15);
        
        // log to error log ip attempting to log in
        error_log("[log-in] count REMOTE_ADD: " . $_SERVER['REMOTE_ADDR'] . " REAL IP: " . $ipaddress);

		$li_query = mlavu_query("select * from `[1]`.`login_log` where `ipaddress`='[2]' and `[3]`>'0' and `ts`>='[4]' and `users`='[5]'",$maindb,$ipaddress,$field,$mints,$username);
		if(mysqli_num_rows($li_query))
		{
			$li_read = mysqli_fetch_assoc($li_query);
			return $li_read[$field];
		}
		else return false;
    }
    
    function getClientIp(){
        foreach (array('HTTP_CLIENT_IP', 'HTTP_X_FORWARDED_FOR', 'HTTP_X_FORWARDED', 'HTTP_X_CLUSTER_CLIENT_IP', 'HTTP_FORWARDED_FOR', 'HTTP_FORWARDED', 'REMOTE_ADDR') as $key){
            if (array_key_exists($key, $_SERVER) === true){
                foreach (explode(',', $_SERVER[$key]) as $ip){
                    $ip = trim($ip); // just to be safe
                    if (filter_var($ip, FILTER_VALIDATE_IP, FILTER_FLAG_NO_PRIV_RANGE | FILTER_FLAG_NO_RES_RANGE) !== false){
                        return $ip;
                    }
                }
            }
        }
    }

	$folder_path = $_SERVER['REQUEST_URI'];
	$folders = explode('/', $folder_path);
	$i = 0;
	foreach($folders as $folder) {
		if (strpos($folder, '.') !== FALSE || empty($folder)) {
			unset($folders[$i]);
		}
		$i++;
	}
	if(!empty($folders) && in_array('webgift', $folders)){
		global $maindb;
		$cust_query = mlavu_query("select `id`, `dataname` from `[2]`.`customer_accounts` where `username`='[1]'", $_POST['username'], $maindb);
		$cust_read = mysqli_fetch_assoc($cust_query);
		$custid = $cust_read['id'];
		$dataname = $cust_read['dataname'];
		$data_name = $dataname; // for lavu_query
		$rdb = "poslavu_".$dataname."_db";
		$s_pass_check = "AND (`password` = PASSWORD('[pass]') OR `password` = OLD_PASSWORD('[pass]'))";
		$user_level_query = lavu_query("SELECT `access_level` FROM `[rest_db]`.`users` WHERE `access_level`>='[min_login_level]' AND `_deleted`!='1' AND `active`='1' AND `username`='[username]' {$s_pass_check}", array("username" => $_POST['username'], "pass" => $_POST['password'], "rest_db" => $rdb, "min_login_level" => LAVU_MINIMUM_LOGIN_LEVEL));
		if(!$user_level_query){
			update_cp_login_status(false, $_POST['username']);
			$login_message = "Invalid Account or Password";
		} else if($user_level_query !== FALSE && mysqli_num_rows($user_level_query)) {
			$user_level_read = mysqli_fetch_assoc($user_level_query);

			if($user_level_read["access_level"] < 3){
				$login_forward = "error_page.php";
				echo "{ \"status\": \"success\", \"forward\": \"{$login_forward}\"}";
				return;
			}
		}
	}

	$failed_login_attempts = get_cp_login_count("failed", $_POST['username']);
	if($failed_login_attempts >= 1000 && !isset($whiteList[$ipaddress]) && !$isLocalIP) {	
		update_cp_login_status(false,$_POST['username']);
		$login_message = "Exceeded Max Login Attempts";

		if($failed_login_attempts==100 || $failed_login_attempts ==10) {
			if($failed_login_attempts==100)
				$msubject = "Attack Detected";
			else
				$msubject = "Exceeded Login Attempts";

			if( DEV ){
				logMail("cp_notifications@lavu.com","$msubject - PosLavu","$msubject\nusername: " . $_POST['username'] . "\npassword: " . $_POST['password'] . "\nip address: " . $_SERVER['REMOTE_ADDR'],"From: security@poslavu.com");
			} else {
				mail("cp_notifications@lavu.com","$msubject - PosLavu","$msubject\nusername: " . $_POST['username'] . "\npassword: " . $_POST['password'] . "\nip address: " . $_SERVER['REMOTE_ADDR'],"From: security@poslavu.com");
			}
		}
	} else {
		$cust_query = mlavu_query("select * from `[2]`.`customer_accounts` where `username`='[1]'",$_POST['username'],$maindb);

		if( $cust_query === FALSE && DEV ){
			$login_message = 'Cust Query Failed: ' . mlavu_dberror();
		} else if(mysqli_num_rows($cust_query)) {
			$cust_read = mysqli_fetch_assoc($cust_query);
			$custid = $cust_read['id'];
			$dataname = $cust_read['dataname'];
			$data_name = $dataname; // for lavu_query
			$rdb = "poslavu_".$dataname."_db";

			if (!databaseExists($rdb))
			{
				$company_disabled = true;
				$user_query = false;
			}
			else {
				// check for keys from auto_login.php
				global $global_login_key;
				$s_pass_check = "AND (`password`=PASSWORD('[pass]') OR `password` = OLD_PASSWORD('[pass]'))";
				if (isset($global_login_key) && trim($global_login_key) != "" && isset($_POST["key"])) {
					if ($global_login_key == $_POST["key"]) {
						$s_pass_check = "";
					}
				}
				$user_query = lavu_query("SELECT * FROM `[rest_db]`.`users` WHERE `access_level`>='[min_login_level]' AND `_deleted`!='1' AND `active`='1' AND `username`='[username]' {$s_pass_check}", array("username"=>$_POST['username'], "pass"=>$_POST['password'], "rest_db"=>$rdb, "min_login_level"=>LAVU_MINIMUM_LOGIN_LEVEL));
			}

			if( $user_query === FALSE && DEV ){
				update_cp_login_status(false,$_POST['username']);
				$login_message = "Invalid Account or Password";
			} else if($user_query !== FALSE && mysqli_num_rows($user_query)) {
					//Code added by Ravi tiwari to show Business info prompt on 8th Mar 2019
					$_SESSION['prompted'] = 1;
					$rest_query = mlavu_query("select * from `[2]`.`restaurants` where `data_name`='[1]'",$dataname,$maindb);
					if( $rest_query === FALSE && DEV ){
						$login_message = 'Rest Query Failed: ' . mlavu_dberror();
					} else if(mysqli_num_rows($rest_query)) {
						$rest_read = mysqli_fetch_assoc($rest_query);
						if($rest_read['disabled']=="1" || $rest_read['disabled']=="2")
							$company_disabled = true;
						else
							$company_disabled = false;
						$companyid = $rest_read['id'];
						$chain_id = $rest_read['chain_id'];
						$version_number = $rest_read['version_number'];
						$dev = $rest_read['dev'];
						$company_name = $rest_read['company_name'];
					}
					else
					{
						$company_disabled = true;
						$companyid = 0;
						$chain_id = 0;
					}
	
					if($company_disabled)
					{
						update_cp_login_status(false,$_POST['username']);
						$login_message = "Invalid Account or Password";
					}
					else
					{
						$query = mlavu_query("SELECT * FROM `poslavu_MAIN_db`.`tos_agreements` where `username`='[1]' and `data_name`='[2]'", $_POST['username'], $dataname);
						$current_version = 2;
						$agreed = false;
						if( $query === FALSE && DEV ){
							$login_message =  'TOS Query Failed: ' . mlavu_dberror();
						} else if( mysqli_num_rows($query)){
							$history = mysqli_fetch_assoc($query);
							require_once(dirname(__FILE__)."/../resources/json.php");
							$vers = LavuJson::json_decode($history['history']);
							foreach($vers as $obj){
								if($obj['version'] == $current_version){
									$agreed = true;
									break;
								}
							}
						}
						if($agreed){
	
							$user_read = mysqli_fetch_assoc($user_query);
							admin_login($dataname,$user_read['id'],$user_read['username'],trim($user_read['f_name']." ".$user_read['l_name']),$user_read['email'],
								$companyid, $version_number, $dev, $user_read['access_level'], $company_name, $user_read['phone'], $user_read['loc_id'], $user_read['lavu_admin'], $chain_id);
							set_sessvar('admin_dataname', $dataname);
							set_sessvar('locationid', $user_read['loc_id']);
							update_cp_login_status(true,$_POST['username']);
	
							if(isset($_POST['stay_logged_in']))
							{
								$autokey = session_id() . rand(1000,9999);
								setcookie("poslavu_cp_login", $autokey, mktime(0,0,0,date("m")+1,date("d"),date("Y")));
								mlavu_query("update `poslavu_MAIN_db`.`customer_accounts` set `autokey`=AES_ENCRYPT('[1]','autokey') where `id`='[2]'",$autokey,$custid);
							}
	
							if(isset($_SESSION['login_forward_to']) && $_SESSION['login_forward_to']!="")
								$login_forward = $_SESSION['login_forward_to'];
							else
								$login_forward = "index.php";
	
							if ( strstr($_SERVER['HTTP_REFERER'], 'register.' ) || strstr($_SERVER['HTTP_REFERER'], 'dev.lavu.com') || strstr($_SERVER['HTTP_REFERER'], '.lavu.com') ){  // just checking for register subdomain to make this work for poslavu.com, lavu.com, and local dev servers
								$login_type = (isset($_POST['login_type']))?$_POST['login_type']:"";
								if($login_type!="auto")
								{
									echo "{ \"status\": \"success\", \"forward\": \"{$login_forward}\"}";
									if(strstr($_SERVER['HTTP_REFERER'], 'dev.lavu.com' ))
									{
										return;
									}
								}
								header('Refresh: 0');
								return;
							}
	
							echo "{ \"status\": \"success\", \"forward\": \"{$login_forward}\"}";
							return;
						}else{
							if(/*$_POST['agreed']*/ true){ //auto agree on login.
	
								$query = mlavu_query("SELECT * FROM `poslavu_MAIN_db`.`tos_agreements` where `username`='[1]' and `data_name`='[2]'", $_POST['username'], $dataname);
								if( $query === FALSE && DEV ){
									$login_message =  'TOS Query Failed: ' . mlavu_dberror();
								} else if( mysqli_num_rows($query)){
									$res  = mysqli_fetch_assoc($query);
									$vers = LavuJson::json_decode($res['history']);
									$vers[]= array("timestamp"=>date("Y-m-d H:i:s"),"version"=>2);
									$encoded = LavuJson::json_encode($vers);
									mlavu_query("UPDATE `poslavu_MAIN_db`.`tos_agreements` SET `history` = '[1]' WHERE `username`='[2]' limit 1", $encoded, $_POST['username']);
								}else
								mlavu_query("INSERT INTO `poslavu_MAIN_db`.`tos_agreements` (`username`,`data_name`,`history`) VALUES ('[1]','[2]','[{\"timestamp\":\"[3]\",\"version\":\"2\"}]')", $_POST['username'], $dataname, date("Y-m-d H:i:s"));
								if (mlavu_dberror()){
									echo mlavu_dberror();
								}else{
									$user_read = mysqli_fetch_assoc($user_query);
									admin_login($dataname,$user_read['id'],$user_read['username'],trim($user_read['f_name']." ".$user_read['l_name']),$user_read['email'],
										$companyid, $version_number, $dev, $user_read['access_level'], $company_name, $user_read['phone'], $user_read['loc_id'], $user_read['lavu_admin'], $chain_id);
									set_sessvar('admin_dataname', $dataname);
									set_sessvar('locationid', $user_read['loc_id']);
									update_cp_login_status(true,$_POST['username']);
	
									if(isset($_POST['stay_logged_in']))
									{
										$autokey = session_id() . rand(1000,9999);
										setcookie("poslavu_cp_login", $autokey, mktime(0,0,0,date("m")+1,date("d"),date("Y")));
										mlavu_query("update `poslavu_MAIN_db`.`customer_accounts` set `autokey`=AES_ENCRYPT('[1]','autokey') where `id`='[2]'",$autokey,$custid);
									}
	
									if(isset($_SESSION['login_forward_to']) && $_SESSION['login_forward_to']!="")
										$login_forward = $_SESSION['login_forward_to'];
									else
										$login_forward = "index.php";
	
									if ( strstr($_SERVER['HTTP_REFERER'], 'register.') || strstr($_SERVER['HTTP_REFERER'], '.lavu.com') || empty($_SERVER['HTTP_REFERER']) ){  // just checking for register subdomain to make this work for poslavu.com, lavu.com, and local dev servers
										header('Refresh: 0');
										return;
									}
									echo "{\"status\":\"success\", \"forward\":\"{$login_forward}\"}";
									return;
								}
							}else{
								// echo "<script type='text/javascript'>";
								// echo "var dn='';";
								// echo "window.onload=function(){document.getElementById('hiddenUsername').value='".$_POST['username']."';document.getElementById('hiddenPassword').value='".$_POST['password']."'; dn='".$dataname."'; openNotice()};";
								// echo "</script>";
								return;
							}
						}
					}
			}
			else
			{
				update_cp_login_status(false,$_POST['username']);
				$login_message = "Invalid Account or Password";
			}
		}
		else
		{
			update_cp_login_status(false,$_POST['username']);
			$login_message = "Invalid Account or Password";
		}
	}
	if( !empty($login_message) ){
		echo "{\"status\":\"error\", \"message\":\"{$login_message}\"}";
		return;
	}
} else if(isset($_POST['username_reset']) && !empty($_POST['username_reset'])) {
	$username = $_POST['username_reset'];
	require_once(dirname(__FILE__) . "/../resources/lavuquery.php");
	$query = mlavu_query("SELECT * FROM `poslavu_MAIN_db`.`customer_accounts` WHERE `username`= '[1]'", $username);
	if( mysqli_num_rows($query)){
		$res  = mysqli_fetch_assoc($query);
		$info = mysqli_fetch_assoc(mlavu_query("SELECT * FROM `poslavu_[1]_db`.`users` where `username`='[2]'", $res['dataname'],$username));
		if(!empty($info['email'])){
			$token = md5(uniqid(rand(), true));
			if(mysqli_num_rows(mlavu_query("SELECT * FROM `poslavu_MAIN_db`.`password_reset_tokens` where `valid`='1' and `username`='[1]'", $username)) > 5  ){
				$login_message= "Maximum reset tokens for this username reached.";
				error_log("Maximum reset tokens for this username reached.");
					//sreturn;
			}
			mlavu_query("INSERT INTO `poslavu_MAIN_db`.`password_reset_tokens` (`username`, `email`,`data_name`,`token`,`valid`) VALUES ('[1]','[2]','[3]','[4]','[5]')", $username, $info['email'], $res['dataname'],$token, 1);

			$base_requri_parts = explode("?",$_SERVER['REQUEST_URI']);
			$base_requri = $base_requri_parts[0];
			$message  =
			"Hello \n \nWe received a request to change the password associated with the email address ({$info['email']}). \nIf you would like to change your password, please click on the link below. If you didn't make this request, \n or no longer need to change your password, you can ignore this email. \n\n https://".$_SERVER['HTTP_HOST'].$base_requri."?mode=reset_password&token=$token \n \nWe hope you are enjoying your Lavu experience - please don't hesitate to contact us at https://support.lavu.com if you have questions or suggestions. \n\n-Lavu ";

			if( DEV ){
				logMail( $info['email'], "Lavu Password Reset",$message, "From: noreply@poslavu.com" );
			} else {
				mail($info['email'], "Lavu Password Reset",$message, "From: noreply@poslavu.com");
			}
			// $login_message = "Reset account email sent to : ". $info['email'];
			error_log("Reset account email sent to : ". $info['email']);

			echo "{ \"status\": \"success\", \"message\": \"Email was successfully sent to {$info['email']}\"}";
			return;
		}else{
			$query = mlavu_query("SELECT * FROM `poslavu_[1]_db`.`users` where `id`='1' and `email` != '' ", $res['dataname']);
			if(mysqli_num_rows($query)){
				$res2= mysqli_fetch_assoc($query);
				$message2="Hello, \n\nA request to change the password has been made for the following user: \n\n{$username}\n\nThere is no email specified for this user. As the primary account holder you have been notified to help {$username} regain access to the Lavu Control Panel (admin.poslavu.com). We recommend verifying that this user would like to change their password before saving any changes. \n\nWe hope you are enjoying your Lavu experience - please don't hesitate to contact us at https://support.lavu.com if you have questions or suggestions. \n\n~ Lavu";
				if( DEV ){
					logMail($res2['email'], "Lavu Password Reset, User has not specified an email address",$message2,"From: noreply@poslavu.com");
				} else {
					mail($res2['email'], "Lavu Password Reset, User has not specified an email address",$message2,"From: noreply@poslavu.com");
				}
				$login_message = "This user does not have an email address associated with their username. <br> An email has been sent to the primary account holder for assistance.";
				error_log("This user does not have an email address associated with their username. <br> An email has been sent to the primary account holder for assistance.");
					//return;
			}else{
				$login_message= "Main account holder does not have email address.";
				error_log("Main account holder does not have email address.");
					//return;
			}
		}
	}else{
		error_log("invalid username");
		$login_message= "invalid username";
			//return;
	}
}

if(!isset($_GET['logout'])){
	$_SESSION['login_forward_to'] = $_SERVER['REQUEST_URI'];
} else {
	$_SESSION['login_forward_to'] = "index.php";
}

unset($_COOKIE['cp3_beta_notify_disabled']);
setcookie('cp3_beta_notify_disabled', null, -1, '/');

ob_start();
require_once dirname(__FILE__) . '/templates/login.html.php';
$output = ob_get_contents();
ob_end_clean();
$output = str_replace('<![CDATA[EXTRA_SCRIPTS_TO_RUN]]>', $extra_scripts, $output);
echo $output;
