<?php
	if($in_lavu)
	{
		session_start();
		
		function api_response($json_vars)
		{
			global $json_cc;
			if(!isset($json_cc))
				$json_cc = lsecurity_name(admin_info("dataname"),admin_info("companyid"));
			$json_vars = "cc=".$json_cc."&".$json_vars;
			$json_url = 'http://admin.poslavu.com/lib/json_connect.php';
			//echo $json_url . "<br>";
			$ch = curl_init();
			curl_setopt($ch, CURLOPT_URL, $json_url . '?' . $json_vars);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			$contents = curl_exec ($ch);
			curl_close ($ch);
			
			$tinfo = json_decode($contents);
			$tinfo = $tinfo[0];
			return $tinfo;
		}
		
		if(!isset($foldername))
			$foldername = admin_info("dataname");
		$imgpath = "http://admin.poslavu.com/images/".$foldername;
		$linfo = api_response('m=2');		
		$tinfo = api_response('m=3&loc_id='.$locationid.'&menu_id='.$location_info['menu_id']);
		
		$lread = false;
		if($linfo['json_status']=="success")
		{
			for($i=0; $i<count($linfo['locations']); $i++)
			{
				if($linfo['locations'][$i]['id']==$locationid)
				{
					$lread = $linfo['locations'][$i];
				}
			}
		}
		
		if(isset($_GET['test_json']))
		{
			display_decoded_json($linfo);
			echo "<br><br>";
			display_decoded_json($tinfo);
		}
		else if($lread && $tinfo['json_status']=="success")
		{
			$loc_taxrate = $lread['taxrate'];
			//display_decoded_json($tinfo);
			$setup_js = "";
			
			$groups = array();
			$active_group = false;
			$menu_group_list = array();
			//echo "menu group count: " . count($tinfo['menu_groups']) . "<br>";
			if(isset($_GET['menu_group'])) $active_group = $_GET['menu_group'];
			$setup_js .= "menu_group_list = new Array();\n";
			$setup_js .= "menu_group_selected = 0;\n";
			echo "<table cellspacing=2 cellpadding=4>";
			for($i=0; $i<count($tinfo['menu_groups']); $i++)
			{
				$group_id = $tinfo['menu_groups'][$i]['id'];
				$group_name = $tinfo['menu_groups'][$i]['group_name'];
				//echo "group $group_id = $group_name<br>";
				$groups[$group_id] = $group_name;
				$menu_group_list[] = $group_id;
				
				if(!$active_group) $active_group = $group_id;
				if($active_group==$group_id) 
				{
					$bgcolor = "#eeeeee";
					$setup_js .= "menu_group_selected = ".$group_id.";\n";
				}
				else $bgcolor = "#cccccc";
				echo "<td bgcolor='$bgcolor' style='border:solid 1px black' id='group_button_$group_id'><a style='cursor:pointer; color:#444444; text-decoration:none' onclick='select_menu_group($group_id)'>$group_name</a></td>";
				$setup_js .= "menu_group_list[$group_id] = new Array();\n";
			}
			echo "</table>";
			//echo "active group: $active_group ";

			function menu_group_exists($groupid)
			{
				global $menu_group_list;
				$group_found = false;
				for($i=0; $i<count($menu_group_list); $i++)
				{
					if($menu_group_list[$i]==$groupid)
						$group_found = true;
				}
				return $group_found;
			}
			
			$setup_cats = "";
			$setup_js .= "item_lists = new Array();\n";
			for($i=0; $i<count($tinfo['menu_categories']); $i++)
			{
				$cat_name = $tinfo['menu_categories'][$i]['name'];
				$cat_image = $tinfo['menu_categories'][$i]['image'];
				$cat_group = $tinfo['menu_categories'][$i]['group_id'];
				$cat_active = $tinfo['menu_categories'][$i]['active'];
				$cat_deleted = $tinfo['menu_categories'][$i]['_deleted'];
				$cat_id = $tinfo['menu_categories'][$i]['id'];
				$cat_forcemod = $tinfo['menu_categories'][$i]['forced_modifier_group_id'];
				
				$cat_name_in_quotes = str_replace("\"","&quot;",$cat_name);
				$cat_image_in_quotes = str_replace("\"","&quot;",$cat_image);
				
				if(menu_group_exists($cat_group))
				{
					$setup_js .= "menu_group_list[$cat_group].push($cat_id);\n";
					
					$setup_js .= "cat_info[$cat_id] = new Array(\"$cat_name_in_quotes\",\"$cat_image_in_quotes\",\"$cat_group\",\"$cat_forcemod\");\n";
					$setup_js .= "item_lists[$cat_id] = new Array();\n";
					
					if($cat_group==$active_group && $cat_active=="1" && $cat_deleted!="1")
					{
						$setup_cats .= "<td width='120' align='center' valign='top'>";
						if(trim($cat_image)=="")
						{
							$setup_cats .= "<table cellspacing=0 cellpadding=0><td style='width:100px; height:80px; background-color:#999999; text-align:center; vertical-align:middle; font-size:32px; cursor:pointer' onclick='menu_category_click($cat_id)'>" . substr($cat_name,0,2) . "</td></table>";
						}
						else
						{
							$setup_cats .= "<img src='$imgpath/categories/$cat_image' height='80' style='cursor:pointer' onclick='menu_category_click($cat_id)'/><br>";
						}
						$setup_cats .= $cat_name;
						$setup_cats .= "</td>";
					}
				}
			}
			for($i=0; $i<count($tinfo['menu_items']); $i++)
			{
				$it_cat_id = $tinfo['menu_items'][$i]['category_id'];
				$it_name = $tinfo['menu_items'][$i]['name'];
				$it_image = $tinfo['menu_items'][$i]['image'];
				$it_price = $tinfo['menu_items'][$i]['price'];
				$it_active = $tinfo['menu_items'][$i]['active'];
				$it_deleted = $tinfo['menu_items'][$i]['_deleted'];
				$it_id = $tinfo['menu_items'][$i]['id'];
				$it_forcemod = $tinfo['forced_modifier_group_id'];
				
				$it_name_in_quotes = str_replace("\"","&quot;",$it_name);
				$it_image_in_quotes = str_replace("\"","&quot;",$it_image);
				$it_price_in_quotes = str_replace("\"","&quot;",$it_price);
				//if($cat_active=="1" && $cat_deleted!="1")
				//{
					$setup_js .= "item_info[$it_id] = new Array(\"$it_name_in_quotes\",\"$it_image_in_quotes\",\"$it_price_in_quotes\",\"$it_forcemod\",\"$it_cat_id\");\n";
					$setup_js .= "item_lists[$it_cat_id].push($it_id);\n";
				//}
			}
			for($i=0; $i<count($tinfo['forced_modifier_groups']); $i++)
			{
				$mg_title = $tinfo['forced_modifier_groups'][$i]['title'];
				$mg_lists = $tinfo['forced_modifier_groups'][$i]['include_lists'];
				$mg_id = $tinfo['forced_modifier_groups'][$i]['id'];
				
				$mg_title_in_quotes = str_replace("\"","&quot;",$mg_title);
				$setup_js .= "new_fmod_grouplist = \"$mg_lists\"; ";
				$setup_js .= "fmod_groups[$mg_id] = new Array(\"$mg_title_in_quotes\",new_fmod_grouplist.split(\"|\"));\n";
			}
			for($i=0; $i<count($tinfo['forced_modifier_lists']); $i++)
			{
				$fm_title = $tinfo['forced_modifier_lists'][$i]['title'];
				$fm_type = $tinfo['forced_modifier_lists'][$i]['type'];
				$fm_mods = $tinfo['forced_modifier_lists'][$i]['modifiers'];
				$fm_id = $tinfo['forced_modifier_lists'][$i]['id'];
				
				$fm_title_in_quotes = str_replace("\"","&quot;",$fm_title);
				$fm_type_in_quotes = str_replace("\"","&quot;",$fm_type);
				$setup_js .= "new_fmods = new Array();\n";
				for($n=0; $n<count($fm_mods); $n++)
				{
					$m_title = $tinfo['forced_modifier_lists'][$i]['modifiers'][$n]['title'];
					$m_cost = $tinfo['forced_modifier_lists'][$i]['modifiers'][$n]['cost'];
					$m_detour = $tinfo['forced_modifier_lists'][$i]['modifiers'][$n]['detour'];
					
					$m_title_in_quotes = str_replace("\"","&quot;",$m_title);
					$m_cost_in_quotes = str_replace("\"","&quot;",$m_cost);
					$m_detour_in_quotes = str_replace("\"","&quot;",$m_detour);
					$setup_js .= "new_fmods.push(new Array(\"$m_title_in_quotes\",\"$m_cost_in_quotes\",\"$m_detour_in_quotes\"));\n";
				}
				$setup_js .= "fmod_lists[$fm_id] = new Array(\"$fm_title_in_quotes\",\"$fm_type_in_quotes\",new_fmods);\n";
			}
			
			echo "<div style='position:relative'>";
			echo "<table>";
			echo "<tr>";
			echo "<td width='400'>";
			echo "<div style='width:400px; height:400px; overflow:auto; border:solid 1px black' id='order_container'>&nbsp;</div>";
			echo "</td>";
			echo "<td width='400'>";
			echo "<div style='width:400px; height:400px; overflow:auto; border:solid 1px black' id='item_container'>&nbsp;</div>";
			echo "</td>";
			echo "</tr>";
			echo "<tr>";
			echo "<td colspan='2'>";
						
			echo "<div style='width:808px; height:140px; overflow:auto; border:solid 1px black' id='category_container'>";
			echo "<table style='table-layout:fixed'>";
			echo $setup_cats;
			echo "</table>";
			echo "</div>";
			
			echo "</td>";
			echo "</tr>";
			echo "</table>";
			echo "<div style='position:absolute; left:0px; top:0px; opacity: 0.5; filter:alpha(opacity=50); visibility:hidden; z-index:400;' id='grey_overlay'><table><td bgcolor='#555555' width='808' height='544'>&nbsp;</td></table></div>";
			echo "<div style='position:absolute; left:0px; top:0px; visibility:hidden; z-index:800;' id='main_overlay'><table><td width='808' height='544' align='center' valign='middle'><table><td bgcolor='#ffffff' style='border:solid 2px #999999' width='220' height='300' id='mod_box' align='center' valign='middle'>&nbsp;</td></table></td></table></div>";
			echo "</div>";
			echo "</div>";
			?>
            
			<script language="javascript">
				cart_contents = new Array();
				cat_info = new Array(); // [0]name [1]image [2]group [3]forcemods
				item_info = new Array(); // [0]name [1]image [2]price [3]fmod_group [4]category
				fmod_groups = new Array(); // [0]title [1](listid,listid,listid)
				fmod_lists = new Array(); // [0]title [1]type [2]([0]title [1]cost [2]detour,etc...)
				item_image_path = "<?php echo $imgpath?>";
				loc_taxrate = "<?php echo $loc_taxrate?>";
				<?php echo $setup_js; ?>
				
				function select_menu_group(groupid)
				{
					var str = "";
					str += "<table style='table-layout:fixed'>";
					var group_count = menu_group_list[groupid].length;
					for(i=0; i<group_count; i++)
					{
						var category_id = menu_group_list[groupid][i];
						var category_name = cat_info[category_id][0];
						var category_image = cat_info[category_id][1];
						var category_fmods = cat_info[category_id][3];
						
						str += "<td width='120' align='center' valign='top'>";
						if(category_image=="")
						{
							str += "<table cellspacing=0 cellpadding=0><td style='width:100px; height:80px; background-color:#888888; text-align:center; vertical-align:middle; font-size:32px; cursor:pointer' onclick='menu_category_click(" + category_id + ")'>" + category_name.substring(0,2) + "</td></table>";
						}
						else
						{
							str += "<img src='" + item_image_path + "/categories/" + category_image + "' height='80' style='cursor:pointer' onclick='menu_category_click(" + category_id + ")'/><br>";
						}
						str += category_name;
						str += "</td>";
					}
					str += "</table>";
					if(menu_group_selected > 0)
					{
						document.getElementById('group_button_' + menu_group_selected).style.backgroundColor = "#cccccc";
					}
					menu_group_selected = groupid;
					document.getElementById('group_button_' + menu_group_selected).style.backgroundColor = "#eeeeee";
					document.getElementById('category_container').innerHTML = str;
				}
				
				function menu_category_click(catid)
				{
					var str = "<table style='table-layout:fixed'><tr>";
					var row = 0;
					var col = 0;
					var item_count = item_lists[catid].length;
					for(i=0; i<item_count; i++)
					{
						var item_id = item_lists[catid][i];
						var item_name = item_info[item_id][0];
						var item_image = item_info[item_id][1];
						var item_price = item_info[item_id][2];
						str += "<td valign='top' style='cursor:pointer' onclick='menu_item_click(" + item_id + ")'>";
						if(item_image=="") 
						{
							str += "<div style='width:80px; height:55px; background-color:#999999; text-align:center; vertical-align:middle; font-size:32px;'>" + item_name.substring(0,2) + "</div>";
						}
						else 
						{
							str += "<img src='" + item_image_path + "/items/" + item_image + "' border='0' width='80'/><br>";
						}
						str += "<font style='font-size:10px'>" + item_name + "</font>";
						str += "</td>";
						col++;
						if(col>3) {str += "</tr><tr>";row++;col=0;}
					} 
					str += "</tr></table>";
					document.getElementById('item_container').innerHTML = str;
				}
				
				mod_queue = new Array();
				mod_answers = new Array();
				mod_item = false;
				function menu_item_click(itemid) {
					mod_queue = new Array();
					mod_answers = new Array();
					mod_item = itemid;
					mgroup_id = item_info[itemid][3];
					if(mgroup_id=="" || mgroup_id==0)
					{
						ccat_id = item_info[itemid][4];
						mgroup_id = cat_info[ccat_id][3];
					}
					if(mgroup_id!="" && mgroup_id!=0)
					{
						mlists = fmod_groups[mgroup_id][1];
						for(n=0; n<mlists.length; n++)
						{
							mod_queue.push(mlists[n]);
						}
					}
					if(mod_queue.length > 0)
					{
						ask_mod_question();
					}
					else
					{
						add_to_cart(itemid,mod_answers);
					}
				}
				function ask_next_mod_question()
				{
					modcopy = mod_queue;
					mod_queue = new Array();
					for(n=1; n<modcopy.length; n++)
						mod_queue.push(modcopy[n]);
					ask_mod_question();
				}
				function ask_mod_question()
				{
					if(mod_queue.length > 0)
					{
						str = "";
						listid = mod_queue[0];
						type = fmod_lists[listid][1];
						choices = fmod_lists[listid][2];
						if(type=="seat" || type=="course")
						{
							ask_next_mod_question();
						}
						else
						{
							if(mod_item > 0)
							{
								str += "<b>" + item_info[mod_item][0] + "</b><br><br>";
							}
							str += "<table cellspacing=6 cellpadding=6>";
							for(n=0; n<choices.length; n++)
							{
								str += "<tr><td bgcolor='#ddddee' style='cursor:pointer; border:solid 2px #777799' onmouseover='this.bgColor = \"#ffffff\"' onmouseout='this.bgColor = \"#ddddee\"' onclick='answer_mod(\"" + choices[n][0] + "\",\"" + choices[n][1] + "\",\"" + choices[n][2] + "\")'>" + choices[n][0] + "</td></tr>";
							}
							str += "</table>";
							str += "<br><br><input type='button' value='Cancel' onclick='close_mod()'>";
							document.getElementById('grey_overlay').style.visibility = "visible";
							document.getElementById('main_overlay').style.visibility = "visible";
							document.getElementById('mod_box').style.visibility = "visible";
							document.getElementById('mod_box').innerHTML = str;
						}
					}
					else 
					{
						add_to_cart(mod_item,mod_answers);
					}
				}
				function answer_mod(ctitle,ccost,cdetour) {
					mod_answers.push(new Array(ctitle, ccost));
					document.getElementById('grey_overlay').style.visibility = "hidden";
					document.getElementById('main_overlay').style.visibility = "hidden";
					document.getElementById('mod_box').style.visibility = "hidden";
					ask_next_mod_question();
				}
				function close_mod() {
					document.getElementById('grey_overlay').style.visibility = "hidden";
					document.getElementById('main_overlay').style.visibility = "hidden";
					document.getElementById('mod_box').style.visibility = "hidden";
				}
				function set_item_qty(qrow,qset) {
					if(qset==0)
					{
						old_cart = cart_contents;
						cart_contents = new Array();
						for(k=0; k<old_cart.length; k++)
						{
							if(k!=qrow)
								cart_contents.push(old_cart[k]);
						}
					}
					else
						cart_contents[qrow][1] = qset;
					show_cart_contents();
				}
				function edit_cart_item(rowid) {
					str = "";

					sitem = item_info[cart_contents[rowid][0]];
					setqty = cart_contents[rowid][1];
					smods = cart_contents[rowid][2];
					set_item_name = sitem[0];
					set_unit_cost = sitem[2];
					set_cost = (set_unit_cost * setqty).toFixed(2);
					str += "<b>" + set_item_name + "</b>";
					str += "<br><br>Qty: <select name='setqty' onchange='set_item_qty(" + rowid + ",this.value); close_mod();'>";
					maxqty = (setqty * 1) + 15;
					for(n=0; n<=maxqty; n++) {
						str += "<option value='" + n + "'";
						if(n==setqty) str += " selected";
						str += ">" + n + "</option>";
					}
					str += "</select>";
					
					str += "<br>Cost: $" + set_cost;

					if(smods.length > 0)
					{
						mod_cost = 0;
						str += "<br>"; 
						if(smods.length > 0)
						{
							str += "<br><table cellpadding=2><tr><td><u>Options:</u></td></tr><tr><td>";
						}
						for(m=0; m<smods.length; m++)
						{
							if(m==0) str += "";
							else str += ", ";
							str += smods[m][0];
							if(smods[m][1] > 0)
								mod_cost += (smods[m][2] * 1);
						}
						if(smods.length > 0)
						{
							str += "</td></tr></table>";
						}
						mod_cost = mod_cost.toFixed(2);
						if(mod_cost > 0)
							str += " $" + mod_cost;
						else
							str += " &nbsp;";
					}

					str += "<br><br><input type='button' value='Close' onclick='close_mod()'>";
					str += "<br><br><a style='cursor:pointer' onclick='set_item_qty(" + rowid + ",0); close_mod();'><font color='red'>Remove Item</font></a>";
					document.getElementById('grey_overlay').style.visibility = "visible";
					document.getElementById('main_overlay').style.visibility = "visible";
					document.getElementById('mod_box').style.visibility = "visible";
					document.getElementById('mod_box').innerHTML = str;
				}
				function add_to_cart(itemid,itemmods) {
					var item_found = false;
					for(i=0; i<cart_contents.length; i++)
					{
						if(itemid==cart_contents[i][0] && itemmods==cart_contents[i][2])
						{
							cart_contents[i][1]++;
							item_found = true;
						}
					}
					if(!item_found)
						cart_contents.push(new Array(itemid,1,itemmods));
					show_cart_contents();
				}
				function show_cart_contents() {
					var total_cost = 0;
					var setcart = "<table cellpadding=6><td><table>";
					for(i=0; i<cart_contents.length; i++)
					{
						sitem = item_info[cart_contents[i][0]];
						setqty = cart_contents[i][1];
						smods = cart_contents[i][2];
						set_item_name = sitem[0];
						set_unit_cost = sitem[2];
						set_cost = (set_unit_cost * setqty).toFixed(2);
						setcart += "<tr>";
						setcart += "<td valign='top'><input type='button' style='font-size:10px' value='edit' onclick='edit_cart_item(" + i + ")'></td>";
						setcart += "<td align='right' valign='top' width='30'>" + setqty + " x</td>";
						setcart += "<td width='270' valign='top'>";
						setcart += set_item_name;
						setcart += "</td>";
						setcart += "<td valign='top'>$" + set_cost + "</td>";
						setcart += "</tr>";

						if(smods.length > 0)
						{
							mod_cost = 0;
							setcart += "<tr><td>&nbsp;</td><td>&nbsp;</td><td>"; 
							for(m=0; m<smods.length; m++)
							{
								if(m==0) setcart += " - ";
								else setcart += ", ";
								setcart += smods[m][0];
								if(smods[m][1] > 0)
									mod_cost += (smods[m][2] * 1);
							}
							setcart += "</td>";
							setcart += "<td align='right'>";
							mod_cost = mod_cost.toFixed(2);
							if(mod_cost > 0)
								setcart += "$" + mod_cost;
							else
								setcart += "&nbsp;";
							setcart += "</td>";
							setcart += "</tr>";
						}
						
						total_cost = (total_cost * 1 + set_cost * 1).toFixed(2);
					}
					if(cart_contents.length > 0)
					{
						subtotal = total_cost;
						setcart += "<tr><td>&nbsp;</td><td colspan='3' style='border-top:solid 1px black' align='right'>SubTotal: $" + subtotal + "</td></tr>";
						tax_amount = (subtotal * loc_taxrate).toFixed(2);
						total_cost = (subtotal *1 + tax_amount * 1).toFixed(2);
						setcart += "<tr><td>&nbsp;</td><td colspan='3' align='right'>Tax (" + loc_taxrate + "%): $" + tax_amount + "</td></tr>";
						setcart += "<tr><td>&nbsp;</td><td colspan='3' align='right'>Total: $" + total_cost + "</td></tr>";
					}
					setcart += "</table></td></table>";
					document.getElementById("order_container").innerHTML = setcart;
				}
            </script>
            
            <?php
		}
		else echo "failed";
	}
?>