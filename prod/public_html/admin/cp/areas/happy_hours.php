<?php
//	ini_set("display_errors","1");

	global $b_multiple_hhs;
	$b_multiple_hhs = multiple_happy_hours_per_day();
	require_once(dirname(dirname(dirname(__FILE__))).'/sa_cp/manage_chain_functions.php');
	$a_chain_groups = array();
	$a_chain_items = array();
	$b_part_of_chain = getReportingGroupOptionsForDimensionalForm($data_name, $a_chain_groups, $a_chain_items);

	// check for the multiple happy hours per day config setting
	function multiple_happy_hours_per_day() {

		// get the dataname
		global $data_name;
		if (!isset($data_name))
			$data_name = admin_info("dataname");

		// query the db
		global $locationid;
		$querystring = "SELECT `value` FROM `config` WHERE `setting`='multiple_happy_hours_per_day' AND `type`='location_config_setting' AND `location`='[location]' LIMIT 1";
		$query = lavu_query($querystring, array('location'=>$locationid));
		if ($query === FALSE || mysqli_num_rows($query) == 0)
			$query = lavu_query(str_replace(" AND `location`='[location]'", "", $querystring));
		if ($query === FALSE || mysqli_num_rows($query) == 0)
			return TRUE;

		// read the db
		$row = mysqli_fetch_assoc($query);
		if ($row['value'] == '0')
			return FALSE;
		return TRUE;
	}

	function sync_table($table_name, $locationID) {
		require_once(dirname(dirname(__FILE__)).'/resources/core_functions.php');
		$get_location_info = lavu_query("SELECT `use_net_path`, `net_path` FROM `locations` WHERE `id`= '[1]'", $locationID);
		if (mysqli_num_rows($get_location_info) > 0) {
			$location_info = mysqli_fetch_assoc($get_location_info);
			schedule_remote_to_local_sync_if_lls($location_info, $locationID, "table updated", $table_name);
		}
	}

	if (isset($_POST['save'])) {
		if (strlen($_POST['save']) > 0) {
			$hhs = $_POST['save'];
			$hhs = explode("|", $hhs);

			// check that there aren't multiple happy hours on the same day
			$day_used = array();
			for ($i = 0; $i < 7; $i++) $day_used[$i] = FALSE;
			foreach($hhs as $key=>$hh) {
				$values = explode(":", $hh);
				$weekdayindex = 2;
				if (!$b_multiple_hhs) {
					for ($i = 0; $i < 7; $i++) {
						if (substr($values[$weekdayindex],$i,1) != "_" && $day_used[$i] == FALSE) {
						// the day hasn't been used, yet
							$day_used[$i] = TRUE;
						} else {
						// the day has been used
							$values[$weekdayindex] = substr($values[$weekdayindex],0,$i)."_".substr($values[$weekdayindex],$i+1,max(6-$i,0));
						}
					}
				}
				$hhs[$key] = implode(":", $values);
			}

			// get a test happy hour
			$hh_test_query = lavu_query("SELECT * FROM `happyhours` LIMIT 1");
			$a_test = array();
			if (($hh_test_query !== FALSE && mysqli_num_rows($hh_test_query)))
				$a_test = mysqli_fetch_assoc($hh_test_query);

			// save the happy hours
			foreach($hhs as $hh) {
				// get the happy hour values
				$values = explode(":", $hh);
				$a_extended_properties = "";
				for($i = 8; $i < count($values); $i++) {
					$a_extended_properties[] = $values[$i];
				}
				$s_extented_properties = implode(":", $a_extended_properties);
				$newvals = array("id"=>$values[4],"name"=>$values[5],"start_time"=>$values[0],"end_time"=>$values[1],"weekdays"=>$values[2],"property"=>$values[3],"_deleted"=>$values[6],"chain_reporting_group"=>$values[7],"extended_properties"=>$s_extented_properties);

				// remove any values that don't fit in the database
				foreach($newvals as $k=>$v)
					if (!isset($a_test[$k]) && count($a_test) > 0)
						unset($newvals[$k]);

				if ($newvals['id'] == '-1') {

					// check if the happy hour is a new happy hour (needs to be inserted)
					$newvals['property']=str_replace(',','.',$newvals['property']);  //LP-4194 add functionality for add decimal character as per cp setting
					$querystring = "INSERT INTO `happyhours` (";
					$valuesstring = " VALUES (";
					$first = TRUE;
					foreach ($newvals as $key=>$value) {
						if ($key != "id") {
							if ($first) {
								$first = FALSE;
							} else {
								$querystring .= ",";
								$valuesstring .= ",";
							}
							$querystring .= "`".$key."`";
							$valuesstring .= "'[".$key."]'";
						}
					}
					$querystring .= ")";
					$valuesstring .= ")";
					$querystring .= $valuesstring;
					$result = lavu_query($querystring,$newvals);
					// 	sync_table("happyhours", $locationid);
				} else {

					// otherwise it is a pre-existing happy hour (update it)
					$newvals['property']=str_replace(',','.',$newvals['property']); //LP-4194 add functionality for add decimal character as per cp setting
					$querystring = "UPDATE `happyhours` SET ";
					$first = TRUE;
					foreach ($newvals as $key=>$value) {
						if ($key != "id") {
							if ($first) {
								$first = FALSE;
							} else {
								$querystring .= ",";
							}
							$querystring .= "`".$key."`='[".$key."]' ";
						}
					}
					$querystring .= " WHERE `id` = '[id]'";
					$result = lavu_query($querystring, $newvals);
					if ($b_part_of_chain && DEV)
						sync_table("happyhours", $locationid);
				}
			}
		}

		// sync to the chains
		require_once('/home/poslavu/public_html/admin/sa_cp/manage_chain_functions.php');
		if (!isset($data_name))
			$data_name = admin_info("dataname");
		syncChainDatabases($data_name);
	}

	// queries the server for the happy hours and returns them in a colon-delimeted values array
	function get_happy_hours() {
		global $data_name;
		global $location_info;
		$decimal=$location_info['decimal_char'];
		lavu_connect_dn($data_name);
		$hhs = array();
		$hh_query = lavu_query("SELECT * FROM `happyhours` WHERE `_deleted` != '1'");
		if ($hh_query) {
			while ($row = mysqli_fetch_assoc($hh_query)) {
				//START LP-4194 add functionality for add decimal character as per cp setting START
				if($decimal == ','){
					$property=str_replace('.',',',$row["property"]);
				}
				else{
					$property=$row["property"];
				}//END LP-4194 add functionality for add decimal character as per cp setting END
				$hh = $row['start_time'].":".$row['end_time'].":".$row['weekdays'].":".$property.":".$row['id'].":".$row['name'].":".$row['_deleted'];
				if (isset($row['chain_reporting_group']))
					$hh .= ":".$row['chain_reporting_group'];
				else
					$hh .= ":0";
				if (isset($row['extended_properties']))
					$hh .= ":".$row['extended_properties'];
				else
					$hh .= ":none";
				$hhs[] = $hh;
			}
		}
		return $hhs;
	}

	function getFullURL() {
		return 'https://'.$_SERVER[HTTP_HOST].$_SERVER[REQUEST_URI];
	}

	function echo_save_button($s_which="primary") {
		if ($s_which == "primary") {
			echo '
			<form method="POST" id="save_happyhour_form" action="'.getFullURL().'">
				<input type="button" id="save_happyhour_form_primary_button" value="'.speak("Save").'" class="saveBtn" onclick="save_happy_hours();$(\'save_happyhour_form\').submit();"><br>
				<input type="hidden" name="save" value="" id="save_happyhour_hidden_input" />
			</form>';
		} else {
			echo '
			<form method="POST" id="save_happyhour_form" action="'.getFullURL().'">
				<input type="button" value="'.speak("Save").'" class="saveBtn" onclick="$(\'#save_happyhour_form_primary_button\').click();"><br>
			</form>';
		}
	}



	// eg input: array(2,1,1)
	// returns -1 if the first version is less than the second
	// returns 1 if the second version is less than the first
	// returns 0 if the versions are equal
	function cmp_app_version($version_array_1, $version_array_2) {
		for($i = 0; $i < count($version_array_1); $i++) {
			if (count($version_array_2) < $i+1)
				return -1;
			$first = (int)$version_array_1[$i];
			$second = (int)$version_array_2[$i];
			if ($first < $second)
				return -1;
			if ($first > $second)
				return 1;
		}
		if (count($version_array_2) > count($version_array_1))
			return 1;
		return 0;
	}

	function echo_need_new_app_version() {
		global $maindb;
		$s_dataname = admin_info("dataname");

		if (!isset($maindb) || $maindb == '')
			$maindb = 'poslavu_MAIN_db';
		if ($s_dataname == '')
			return;

		$app_version_query = mlavu_query("SELECT `version_number` FROM `[1]`.`restaurants` WHERE `data_name`='[2]'",$maindb,$s_dataname);
		if ($app_version_query) {
			if (mysqli_num_rows($app_version_query) > 0) {
				$s_app_version = mysqli_fetch_assoc($app_version_query);
				$s_app_version = $s_app_version['version_number'];
				$a_app_version = explode('.', $s_app_version);
				if (count($a_app_version) > 3)
					return;
				if (cmp_app_version($a_app_version, array(2,1,2)) < 0)
					echo "<font style='color:gray;'>".speak("Please update your app to the most recent version to use this feature.")."</font><br /><br />";
			}
		}
	}

	$happyhours = get_happy_hours();
	if ($b_part_of_chain) {
		$a_restaurant_chain = get_restaurant_chain($data_name);
		$i_chain_id = (int)$a_restaurant_chain['id'];
		$a_chain_reporting_groups = get_chain_reporting_groups($i_chain_id, get_restaurants_by_chain($i_chain_id));
	}
?>

<script type="text/javascript" src="scripts/jquery.min.1.8.3.js"></script>
<script type="text/javascript">
	window.b_part_of_chain = <?php
		if ($b_part_of_chain && DEV) {
			echo "true;
	a_all_chain_groups = ".json_encode($a_chain_reporting_groups).";
	a_chain_groups = ".json_encode($a_chain_groups).";
	a_chain_items = ".json_encode($a_chain_items).";";
		} else {
			echo "false;";
		}
	?>
	<?php require(dirname(__FILE__).'/happyhours/happyhour.js'); ?>
</script>
<script type="text/javascript">
	setTimeout(function() {
		$(document).ready(function() {
			hh_echohappyhour("<?php if (count($happyhours) > 0){echo implode("|", $happyhours);} ?>", "happyhours_parent", "happyhour_hiddeninput");
			hh_savehappyhours("happyhour_hiddeninput");
		});
	}, 500);

	function save_happy_hours() {
		var saveinput = $("#save_happyhour_hidden_input");
		if (saveinput && saveinput.length > 0) {
			saveinput.val($("#happyhour_hiddeninput").val());
		}
		$("#save_happyhour_form").submit();
	}
</script>
<?php echo_need_new_app_version(); ?>
<?php echo_save_button(); ?>
<div style="display:none;"><button id="tc_submit_time_button" onclick="hh_submit_time();">Submit Time</button></div>
<input type="hidden" id="happyhour_hiddeninput" name="happyhour_hiddeninput" value="" />
<table><tr><td id="happyhours_parent"></td></tr></table>
<?php echo_save_button("secondary"); ?>
<?php require_once("scheduling_files/php/time_chart.php"); ?>
