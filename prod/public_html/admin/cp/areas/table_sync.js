

//Checks if primary and handles accordingly.
function generateLocationRowForTable(locationInfo, tableName){

	var dataname = locationInfo['data_name'];

	//Required static information on row count
	var rowCount = staticData.sync_permissions.account_table_row_counts[locationInfo.data_name][tableName];
	rowCount = rowCount ? rowCount : 0;

	var primaryLocationRow = document.createElement('tr');
	var locationNameTD = document.createElement('td');
	locationNameTD.innerHTML = locationInfo['title'];
	var datanameTD = document.createElement('td');
	datanameTD.innerHTML = dataname;
	var tableRowCountTD = document.createElement('td');
	tableRowCountTD.innerHTML = rowCount; // <--------------------------------  TODO GET DATA INITIALLY.

	var verifyTableTD = document.createElement('td');
	var lastSyncTimeTD = document.createElement('td');
	var syncTableTD = document.createElement('td');
	if(locationInfo['is_primary']){
		verifyTableTD.innerHTML = 'Primary Account';
		syncTableTD.innerHTML = 'Primary Account';
		lastSyncTimeTD.innerHTML = 'Primary Account';
	}else{
		if(staticData.allow_verification_of_sync){
			//Verify Table TD
			verifyTableButton = document.createElement('button');
			verifyTableButton.innerHTML = 'Verify Table';
			verifyTableButton.onclick = function(){
				verifyLocationTableSynced(locationInfo, tableName);
			};
			verifyTableTD.appendChild(verifyTableButton);
		}
		//Last Sync time TD
		if(staticData.last_sync_times && staticData.last_sync_times[dataname] && staticData.last_sync_times[dataname][tableName]){
			lastSyncTimeTD.innerHTML = staticData.last_sync_times[dataname][tableName];
		}else{
			lastSyncTimeTD.innerHTML = 'Last Sync Time not Found';
		}
		
		//Sync Table TD
		syncTableButton = document.createElement('button');
		syncTableButton.innerHTML = 'Sync Table';
		syncTableButton.onclick = function(){
			syncTable(locationInfo, tableName);
		};
		syncTableTD.appendChild(syncTableButton);
	}

	primaryLocationRow.appendChild(locationNameTD);
	primaryLocationRow.appendChild(datanameTD);
	primaryLocationRow.appendChild(tableRowCountTD);
	if(staticData.allow_verification_of_sync){
		primaryLocationRow.appendChild(verifyTableTD);
	}
	primaryLocationRow.appendChild(lastSyncTimeTD);
	primaryLocationRow.appendChild(syncTableTD);

	return primaryLocationRow;
}


function generateSpecifiedTableSyncManagerGUI(tableName){
	
	var columnCount = staticData.allow_verification_of_sync ? 6 : 5;
	var displayTable = document.createElement('table');
	displayTable.className = 'sync_table_user_table';
	displayTable.setAttribute('border',1);

	//Title Row
	var tableTitleRow = document.createElement('tr');
	var tableTitleTD = document.createElement('td');
	tableTitleTD.setAttribute('colspan', columnCount);
	tableTitleTD.innerHTML = tableName;
	tableTitleRow.appendChild(tableTitleTD);
	displayTable.appendChild(tableTitleRow);

	//Column Titles
	var columnTitlesRow = document.createElement('tr');
	var locationTD = document.createElement('td');
	locationTD.innerHTML = 'Location Name';
	var datanameTD = document.createElement('td');
	datanameTD.innerHTML = 'Dataname';
	var rowCount = document.createElement('td');
	rowCount.innerHTML = 'Row Count';
	if(staticData.allow_verification_of_sync){
		var verifyAllTablesButtonTD = document.createElement('td');
		verifyAllTablesButtonTD.innerHTML = 'Verify Tables Synced';
	}
	var lastSyncTimeTD = document.createElement('td');
	lastSyncTimeTD.innerHTML = 'Last Sync Time';



	var syncAllTablesFromPrimaryButtonTD = document.createElement('td');
	syncAllTablesFromPrimaryButtonTD.innerHTML = 'Sync All From Primary';
	columnTitlesRow.appendChild(locationTD);
	columnTitlesRow.appendChild(datanameTD);
	columnTitlesRow.appendChild(rowCount);
	if(staticData.allow_verification_of_sync){
		columnTitlesRow.appendChild(verifyAllTablesButtonTD);
	}
	columnTitlesRow.appendChild(lastSyncTimeTD);
	columnTitlesRow.appendChild(syncAllTablesFromPrimaryButtonTD);
	displayTable.appendChild(columnTitlesRow);


	var datanamesCount = staticData.sync_permissions.datanames_in_chain.length;
	//alert('datanamesCount'+datanamesCount);
	for(var i = 0; i < datanamesCount; i++){
		var currDatanameInfo = staticData.sync_permissions.datanames_in_chain[i];
		var row = generateLocationRowForTable(currDatanameInfo, tableName);
		displayTable.appendChild(row);
	}


	background_div.appendChild(displayTable);
}


/* Example packet
	{
		"primary_id": 42161,
		"primary_dataname": "acts_dev",
		"datanames_in_chain": [{
			"id": "42161",
			"data_name": "acts_dev",
			"title": "Acts - Dev",
			"is_primary": true
		}, {
			"id": "42337",
			"data_name": "acts_bistro_de",
			"title": "Acts Bistro Dev ",
			"is_primary": false
		}],
		"sync_allowed_tables": ["med_customers"]
	}
*/
function buildArea(){
	//alert('json data:'+JSON.stringify(staticData.sync_permissions.sync_allowed_tables.length));
	var tablesCount = staticData.sync_permissions.sync_allowed_tables.length;
	for(var i = 0; i < tablesCount; i++){
		var currentTableName = staticData.sync_permissions.sync_allowed_tables[i];
		generateSpecifiedTableSyncManagerGUI(currentTableName);
		var brk = document.createElement('br');
		background_div.appendChild( document.createElement('br'));
		background_div.appendChild( document.createElement('br'));
	}
}


// Event methods
function verifyLocationTableSynced(locationInfo, tableName){
	var continueVerify = confirm("Verify table:"+tableName+" location:"+JSON.stringify(locationInfo['title'])+'?');
	if(!continueVerify){
		return;
	}
}
function verifyLocationTableSyncedResponse(returnData){
	//May not need to be implemented... focusing priorities and will return if necessary.
}

function syncTable(locationInfo, tableName){
	var continueVerify = confirm("Sync table:["+tableName+"] of location:["+locationInfo['title']+"] from primary account?");
	if(!continueVerify){
		return;
	}
	var ajaxURL = '/cp/areas/table_sync.php';
	var postObj = {};
	postObj['table_sync_request'] = 'sync_table';
	postObj['child_dataname'] = locationInfo['data_name'];
	postObj['table_2_sync'] = tableName;
	var postString = tableSyncUrlEncodeObject(postObj);
	tableSyncPerformAJAX(ajaxURL, postString, syncTableResponse, syncTableFailResponse); 
}
function syncTableResponse(returnData){
	window.location='';
}
function syncTableFailResponse(){
	alert("Failed Response");
}


/**
*  Performs a URL serialization to x-www-form-urlencoded from flat object.\n
*  Flat object meaning a non-embedded key to value map, where key and value are strings/numbers\n
*  This should be used prior to launching an ajax command for x-www-form-urlencoded (Basically url formatted data);\n
*/
function tableSyncUrlEncodeObject(obj) {
	var strBuilder = '';
	var keys = Object.keys(obj);
	for (i = 0; i < keys.length; i++) {
		var currKey = keys[i];
		strBuilder += encodeURIComponent(currKey) + '=' + encodeURIComponent(obj[currKey]);
		if (i < keys.length - 1) {
			strBuilder += '&';
		}
	}
	return strBuilder;
}

/**
*	A helper function to abstract the process of ajax calling.\n
*   @param url the url path to send the ajax request to.\n
*   @param postData the content body of the post request.  For most cases postData is the url-encoded form of data to send.\n
*   @param failHandler a callback method that is called when an ajax call does not receive a code 200 OK.\n
*   @param Optional contentType http header specifying the body type of the content data, defaults to application/x-www-form-urlencoded.\n
*/
function tableSyncPerformAJAX(url, postData, successHandler, failHandler, contentType) {
	if (!contentType) {
		contentType = 'application/x-www-form-urlencoded';
	}
	var xhttp = new XMLHttpRequest();
	xhttp.open("POST", url, true);
	xhttp.setRequestHeader("Content-type", contentType);
	xhttp.onreadystatechange = function () {
		if (xhttp.readyState == 4 && xhttp.status == 200) {
			successHandler(xhttp.responseText);
		} else {
			console.log(xhttp);
		}
	};
	xhttp.send(postData);
}


