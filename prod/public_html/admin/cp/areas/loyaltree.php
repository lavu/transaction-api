<script type='text/javascript'>

	var konamiCode=[76,65,86,85,76,79,89,65,76,56,56];
	var counter=0;

	$(document).bind("keyup",function(e) {
		if (counter==2 && e.keyCode==76) {}
		else if (konamiCode[counter] == e.keyCode) counter++;
		else counter=0;

		if (counter == konamiCode.length) {
			if (confirm('Do you want to reset LoyalTree for this account?')) window.location= "index.php?mode=settings_loyaltree&loyaltree_reset=1";
		}
	});

	function open_disclamer() {
		document.getElementById('disclamer').style.display='block';
	}

	function close_disclamer(){
		document.getElementById('disclamer').style.display='none';
	}

</script><?php

	// konamiCode = lavuloyal88

	ini_set('display_errors',1);
	global $locationid;
	global $location_info;

	if ($in_lavu) {

		echo "<br><img src='areas/extensions/images/loyaltree_lavu.png' width='700px'/><br>";

		if (isset($_REQUEST['loyaltree_reset'])) {

			$reset= lavu_query("DELETE FROM `config` where `setting`='loyaltree_chain_id' OR `setting`= 'loyaltree_id' OR `setting`='enable_loyaltree'");
			$location_info['loyaltree_username'] = "";
			$location_info['loyaltree_password'] = "";
			header("Location: index.php?mode=settings_loyaltree");

		} else if (isset($_POST['posted'])) {

			if (isset($_POST['update_settings'])) {

				do_update();

			} else {

				$enable_loyaltree_rules = null;

				if ($_POST['enable_loyaltree'] == 1) {
					$do_update = 1;
					do_update();
				} else if ($_POST['enable_loyaltree'] == 2) {
					$do_update=0;
				}

				require_once(resource_path()."/../../../inc/loyalty/loyaltree/LoyalTreeIntegration.php");
				$chain_id = $_POST['loyaltree_chain_id'];
				$rest_id = mysqli_fetch_assoc(mlavu_query("SELECT * FROM `poslavu_MAIN_db`.`restaurants` WHERE `data_name`='[1]'",admin_info('dataname')));
				$args = array(
					'action'        => 'signup',
					'dataname'      => $data_name,
					'loc_id'        => $location_info['id'],
					'chainid'       => $chain_id,
					'app'           => LoyalTreeConfig::BLUELABEL_APP,
				);
				$result_xml = LoyalTreeIntegration::signup($args);
				$xml = simplexml_load_string($result_xml);

				if(isset($xml->success)){
					if ( mysqli_num_rows(lavu_query("select * from `config` WHERE `setting`='enable_loyaltree'")))
						lavu_query("DELETE FROM `config` WHERE `setting`='enable_loyaltree'");
					if ( mysqli_num_rows(lavu_query("select * from `config` WHERE `setting`='loyaltree_id'")))
						lavu_query("DELETE FROM `config` WHERE `setting`='loyaltree_id'");
					if ( mysqli_num_rows(lavu_query("select * from `config` WHERE `setting`='loyaltree_chain_id'")))
						lavu_query("DELETE FROM `config` WHERE `setting`='loyaltree_chain_id'");

					if(!$chain_id){
						$chain_id='0';
					}

					lavu_query("INSERT INTO `config` (`location`,`setting`,`value`,`type`) VALUES('[1]','loyaltree_id', '[2]', 'location_config_setting')", $locationid,($rest_id['id']*72990) );
					lavu_query("INSERT INTO `config` (`location`,`setting`,`value`,`type`) VALUES('[1]','enable_loyaltree', '1', 'location_config_setting')",$locationid);
					lavu_query("INSERT INTO `config` (`location`,`setting`,`value`,`type`) VALUES('[1]','loyaltree_chain_id', '[2]', 'location_config_setting')", $locationid,$chain_id);
					if (lavu_dberror())
						echo "<script type='text/javascript'>alert('".lavu_dberror()."');</script>";
					else{
						schedule_remote_to_local_sync_if_lls($location_info, $locationid, "table updated", "config");
						go_to_loyaltree($xml->url, $do_update, $rest_id['id']*72990, $chain_id);
					}

				} else {
					echo "<div> There was an error signing up for Loyaltree. Please contact Lavu Support. <br> Error: ".print_r($xml,1)."</div>";
				}
			}

		} else {

			if (!mysqli_num_rows(lavu_query("SELECT * FROM `config` WHERE `setting`='loyaltree_id'")) || !mysqli_num_rows(lavu_query("SELECT * FROM `config` WHERE `setting`='loyaltree_chain_id'"))) {
				echo "

				<input type='button' id='loyalTreeBtn' onclick='open_disclamer()' value='Enable Loyaltree!'/>
				<div id='disclamer'>
					<div class='close_overlay' onclick='close_disclamer()'> </div>
					<div class='text_container'>
						LoyalTree is a fully-integrated mobile loyalty program that works seamlessly with POS Lavu. <br>After you setup a rewards program it will automatically work with your Lavu system.<br><br>
						By enabling LoyalTree, you agree to allow loyaltree to access your menu,inventory, location settings, and discount settings.<br> <br>
						For your LoyalTree account to be completely enabled, LoyalTree will also need to change the following settings: <br><br>
						<span style='padding-left:20px'>Discount types: </span> <br>
						<span style='padding-left:35px'>LoyalTree will create a new discount type that enables loyaltree discounts to be applied.</span><br>
						<span style='padding-left:20px'>Settings:</span><br>
						<span style='padding-left:35px'>Display receipt print options at checkout will be disabled.</span> <br>
						<span style='padding-left:35px'>Print receipt by default will be enabled.</span> <br>
						<span style='padding-left:35px'>Primary receipt printer will print receipts using raster graphics.</span> <br>
						<br>
						If you approve these settings changes select \"Enable LoyalTree\". If you would like to try out LoyalTree but do not want your settings 
						changed, select \"Enable without Settings Change\". You can always come back to this page
						and change your mind at any time.
						<br>
						<br>
						<div class= 'button_container'>";

						$forward_to = "index.php?mode=$mode";
						$tablename  = "locations";
						$rowid 		= $locationid;
						$fields 	= array();
						$rest_id    = mysqli_fetch_assoc(mlavu_query("SELECT * FROM `poslavu_MAIN_db`.`restaurants` WHERE `data_name`='[1]'",admin_info('dataname')));
						//echo"The rest id is:". print_r($rest_id,1);

						$fields[] = array("loyaltree_chain_id", "loyaltree_enable",   "hidden", "", "setvalue:1");
						$fields[] = array("loyaltree_chain_id", "loyaltree_chain_id", "hidden", "", "setvalue:".($rest_id['chain_id']*72990) );
						$fields[] = array("loyaltree_rest_id",  "loyaltree_rest_id",  "hidden", "", "setvalue:".($rest_id['id']*72990));
						//if(!mysqli_num_rows(lavu_query("SELECT * FROM `config` WHERE `setting`='receipt'"))){
						//	echo"<div style='color:red'>Error: You must have a printer set up to allow LoyalTree to work correctly. Please set up a printer to continue using LoyalTree. </div>";
						//	$fields[] = array(speak("Enable LoyalTree"),"enable_loyaltree","select",array(""=>"","2"=>"Enable LoyalTree Without Settings Change"));
						//}else{
							$fields[] = array(speak("Enable LoyalTree"),"enable_loyaltree","select",array(""=>"","1"=>"Enable LoyalTree With Settings Change","2"=>"Enable LoyalTree Without Settings Change"));
						//}

						$fields[] = array(speak("Submit"),"submit","submit");

						require_once(resource_path() . "/form.php");
					echo "</div></div><br><br></div>";

			} else {

				if( !mysqli_num_rows(lavu_query("SELECT * FROM `discount_types` WHERE `special`='loyaltree' AND `_deleted`!='1'")) ||
					!mysqli_num_rows(lavu_query("SELECT * FROM `locations` WHERE `ask4email_at_checkout`='1'")) ||
					!mysqli_num_rows(lavu_query("SELECT * FROM `config` WHERE `setting`='receipt' AND `value10` != '3' AND `_deleted`!='1'"))){
						$forward_to = "index.php?mode=$mode";
						$tablename  = "locations";
						$rowid 		= $locationid;
						$fields 	= array();
						$fields[] 	= array("update_settings","update_settings","hidden","","setvalue:1");
						$fields[] 	= array(speak("Change Settings To Enable LoyalTree"),"submit","submit");

						require_once(resource_path() . "/form.php");
				}

				if (empty($location_info['loyaltree_chain_id']) || !is_numeric($location_info['loyaltree_chain_id'])) $location_info['loyaltree_chain_id'] = 0;

				echo "<br><br><form name='go_to_loyaltree' method='post' action='https://backend.loyaltree.com/login.php' target='_blank'>";
				echo "<input type='hidden' name='businessid' value='".$location_info['loyaltree_id']."'>";
				echo "<input type='hidden' name='chainid' value='".$location_info['loyaltree_chain_id']."'>";
				echo "<input type='hidden' name='pos' value='poslavu'>";
				echo "<input type='button' value='Go to Your LoyalTree Account' onclick='go_to_loyaltree.submit();'>" ;
				echo "</form>";

			}
		}
	}

	function do_update(){

		global $locationid;
		global $location_info;
		echo "The location id is: ". $locationid;
		if(!mysqli_num_rows(lavu_query("SELECT * FROM `discount_types` WHERE `special`='loyaltree' AND `_deleted`!='1' ")))
			lavu_query("INSERT INTO `discount_types` (`loc_id`, `title`, `label`, `code`,`special`) VALUES ('[1]', 'LoyalTree Discount', 'LoyalTree Discount', 'p','loyaltree')",$locationid);
		if(!mysqli_num_rows(lavu_query("SELECT * FROM `locations` WHERE `ask4email_at_checkout`='1' AND `id`='[1]'",$locationid)))
			lavu_query("UPDATE `locations` SET `ask4email_at_checkout`='1', `default_receipt_print`='1' WHERE `id`='[1]'",$locationid);
		if(!mysqli_num_rows(lavu_query("SELECT * FROM `config` WHERE `setting`='receipt' AND `value10`='3' AND `location`='[1]' and `_deleted`!= '1'", $locationid))){
			if( mysqli_num_rows(lavu_query("SELECT * FROM `config` where `setting`='receipt' and `location`='[1]'", $locationid)))
				lavu_query("UPDATE `config` SET `value10`='3' WHERE `setting`='receipt' AND `location`='[1]' LIMIT 1 ", $locationid);
		}
		if(lavu_dberror())
			echo lavu_dberror();
		else
			schedule_remote_to_local_sync_if_lls($location_info, $locationid, "table updated", "config");
	}

	function go_to_loyaltree($url, $settings_changed, $business_id, $chain_id) {
		echo "<form name='go_to_loyaltree' method='GET' action='http://loyaltree.com/poslavu' target='_blank'>";
		echo "<input type='hidden' name='settings_changed' value='".$settings_changed."'>";
		echo "<input type='hidden' name='businessid' value='".$business_id."'>";
		echo "<input type='hidden' name='chainid' value='".$chain_id."'>";
		echo "<input type='hidden' name='pos' value='poslavu'>";
		echo "</form>";
		echo '<script type="text/javascript"> document.go_to_loyaltree.submit(); </script>';
	}

?>
