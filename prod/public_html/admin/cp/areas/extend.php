<?php
//	ini_set("display_errors",1);
	require_once($_SERVER['DOCUMENT_ROOT']."/cp/objects/json_decode.php");
	require_once($_SERVER['DOCUMENT_ROOT']."/cp/objects/extendObject.php");
	
	
	$extendPage=null;

	if($in_lavu){
	
		$extendPage= new extendPage();
		$extensions= $extendPage->getPages();
		
	}else
		exit();

	?>
	<div id='grayBackground'></div>
	<div id ='extendPage'>
		<div id='extendPageHeader'> ADD ON TO YOUR LAVU SYSTEM </div>
		<div id='extensions'>
			<?php
			foreach($extensions as $extension){
				echo "
					<div class='extension'>
						<div class= 'extensionUsage'>".    $extension->getParsedUsage().        "</div>
						<div class= 'extensionLogo'>".     $extension->getLogoAsHTML().         "</div>
						<div class= 'extensionTitle'>".    $extension->getTitle().              "</div>
						<div class= 'extensionFeatures'>". $extension->getFeaturesAsHTMLList(). "</div>
						<div class= 'extensionTypeImage'>".$extension->getTypeImageAsHTML().    "</div>
						<div class= 'extensionLink'>".	   $extension->getLinkAsHTML().         "</div>
					</div>
				";
			}
			?>
		</div>
	
	</div>

	<div id = 'permissionsRequest'>
		<div id='disclamer'>This app is requesting access to the following:</div>
		<div id='informationRequests'></div>
		<div id='approveOrDeny'></div>
	</div>
	
	<div id='extensionBackend'>
		<div id='extensionBackBtn' onclick='showExtensions()'>
			Back
		</div>
	
		<iframe id='extendBackend'></iframe>
	</div>
	
	
