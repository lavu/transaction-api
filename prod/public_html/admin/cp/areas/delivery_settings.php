<?php

    //Two entry points, first for general entry, second if posting to itself to save.
    if(!empty($_POST['handling_form_save_post'])){

        $usingDeliverManagementPage = !empty($_POST['using_delivery_option']) ? "true" : "false";
        $deliverySettingsToSaveArr =
            array( $_POST['btn_1_select'],
                   $_POST['btn_2_select'],
                   $_POST['btn_3_select'],
                   $_POST['btn_4_select'],
                   $usingDeliverManagementPage );
        $deliverySettingsToSaveStr = implode("|o|", $deliverySettingsToSaveArr);

        updateDeliveryOptions($deliverySettingsToSaveStr);
        mainDisplay();
    }
    else{
        mainDisplay();
    }


    function mainDisplay(){
        $deliveryOptionsString = getConfigDeliveryOptions();
        $deliveryOptionsArr = explode("|o|", $deliveryOptionsString);
        $dispHTML = '';
        $dispHTML .= getEditorHTML($deliveryOptionsArr);
        echo $dispHTML;
    }

    function getEditorHTML($deliveryOptions){

        //The selects that will be used
        $quickButton1Select = generateSelectForQuickButton('btn_1_select', $deliveryOptions[0]);
        $quickButton2Select = generateSelectForQuickButton('btn_2_select', $deliveryOptions[1]);
        $quickButton3Select = generateSelectForQuickButton('btn_3_select', $deliveryOptions[2]);
        $intervalSelect     = generateSelectForIntervalChoice('btn_4_select',$deliveryOptions[3]);

        $speak = 'speak';
        $checked = $deliveryOptions[4] == 'true' ? "CHECKED" : "";
        $returnHTML = <<<MYHTML
          <form action='' method='POST'>
            <table border='0'>
                <tr>
                    <td>&nbsp;{$speak('Enable Customer Delivery')}:&nbsp;</td>
                    <td> <input type='checkbox' name='using_delivery_option' $checked/> </td>
                </tr>
            </table>
            <input type='hidden' name='handling_form_save_post' value='true'/>
            <table border='1'>


                <tr>
                    <td>&nbsp;Delivery Button 1&nbsp;</td>
                    <td>$quickButton1Select</td>
                </tr>
                <tr>
                    <td>&nbsp;Delivery Button 2&nbsp;</td>
                    <td>$quickButton2Select</td>
                </tr>
                <tr>
                    <td>&nbsp;Delivery Button 3&nbsp;</td>
                    <td>$quickButton3Select</td>
                </tr>
                <tr>
                    <td>&nbsp;Select Button Interval&nbsp;</td>
                    <td>$intervalSelect</td>
                </tr>
            </table>
            <br><br>
            <input type='submit' value='{$speak("Save")}'/>
          </form>
MYHTML;

        return $returnHTML;
    }

    //HTML generator functions
    function generateSelectForQuickButton($selectsID, $selectOnAmount){
        $selectHTML = "<select id='$selectsID' name='$selectsID'>";
        for($i = 5; $i <= 60 * 3; $i+=5){
            $p = $i % 60;
            $q = floor($i / 60);
            $m = $p < 10 ? '0' . $p : $p;
            $h = $q ? "$q:" : "";
            $d = "$h$m";
            $disp = '';
            if($h == ""){
                $disp = "$p Minutes";
            }else if($d == "1:00"){
                $disp = "$q Hour";
            }else if($m == "00"){
                $disp = "$q Hours";
            }else{
                $disp = "$d Hours";
            }
            $isSelectedStr = $selectOnAmount == $d || $selectOnAmount == $i ? "selected" : "";

            $selectHTML .= "<option id='{$selectsID}_option_$d' value='$d' $isSelectedStr>&nbsp;$disp</option>";

        }
        $selectHTML .= "</select>";
        return $selectHTML;
    }

    function generateSelectForIntervalChoice($selectsID, $selectOnAmount){

        $selectedOption10 = $selectOnAmount == '10' ? 'selected' : '';
        $selectedOption15 = $selectOnAmount == '15' ? 'selected' : '';
        $selectedOption30 = $selectOnAmount == '30' ? 'selected' : '';
        $selectedOption1h = $selectOnAmount == '1:00' ? 'selected' : '';
        $selectedOption2h = $selectOnAmount == '2:00' ? 'selected' : '';

        $selectHTML  = "<select id='$selectsID' name='$selectsID'>";
        $selectHTML .=   "<option id='{$selectsID}_option_1' value='10' $selectedOption10>10 Minutes</option>";
        $selectHTML .=   "<option id='{$selectsID}_option_2' value='15' $selectedOption15>15 Minutes</option>";
        $selectHTML .=   "<option id='{$selectsID}_option_3' value='30' $selectedOption30>30 Minutes</option>";
        $selectHTML .=   "<option id='{$selectsID}_option_4' value='1:00' $selectedOption1h>1 Hour</option>";
        $selectHTML .=   "<option id='{$selectsID}_option_5' value='2:00' $selectedOption2h>2 Hours</option>";
        $selectHTML .= "</select>";
        return $selectHTML;
    }

    //Database functions below
    function getConfigDeliveryOptions(){
        $result = lavu_query("SELECT * FROM `config` WHERE `setting`='delivery_options'");
        if(!$result){
            error_log("MySQL Error in delivery options -ksjdf3- error: " . lavu_dberror());
            return false;
        }
        $deliveryOptions;
        if(mysqli_num_rows($result)){
            $row = mysqli_fetch_assoc($result);
            $deliveryOptions= $row['value'];
        }else{
            $deliveryOptions = insertAndReturnDefaultDeliveryOptions();
        }
        return $deliveryOptions;
    }

    function insertAndReturnDefaultDeliveryOptions(){
        $optionsArr = array("15", "30", "1:30", "15", "false");
        $encodedString = implode("|o|", $optionsArr);

        //We should have already made sure this setting does not exist.
        $result = lavu_query("INSERT INTO `config` (`setting`,`value`) VALUES ('delivery_options', '[1]')", $encodedString);
        if(!$result){
            error_log("MySQL Error in delivery options -aesefe- error: " . lavu_dberror());
        }
        return $encodedString;
    }

    function updateDeliveryOptions($newDeliveryOptions){
        $result = lavu_query("UPDATE `config` SET `value`='[1]' WHERE `setting`='delivery_options'", $newDeliveryOptions);
        if(!$result){
            error_log("MySQL Error in delivery options -aesefe- error: " . lavu_dberror());
        }
    }

?>
