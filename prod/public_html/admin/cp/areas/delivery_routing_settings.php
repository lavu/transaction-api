<?php

	// global vars
	global $b_routing_enabled;
	$b_routing_enabled === NULL;

	// require a file within an output buffer and return the ob results
	if (!function_exists('require_once_get_contents')) {
		function require_once_get_contents($s_filename) {
			ob_start();
			require_once($s_filename);
			$s_retval = ob_get_contents();
			ob_end_clean();
			return $s_retval;
		}
	}
		
	$routing_locinfo_fields = array("address","city","state","zip");
	$routing_locinfo_titles = array("zip"=>"Postal Code");
	$routing_config_setting = "ers_routing_setting";
	for($i=0; $i<count($routing_locinfo_fields); $i++)
	{
		$rname = $routing_locinfo_fields[$i];
		$rfieldname = "start_" . $rname;
		$rconfigname = "routing_start" . $rname;
		if(isset($location_info[$rname]))
			$routing_locinfo[$rname] = $location_info[$rname];
		else
			$routing_locinfo[$rname] = "";
		$rconfig_id = 0;
		$rconfig_query = lavu_query("select * from `config` where `setting`='[1]' and `type`='[2]'",$rconfigname,$routing_config_setting);
		if(mysqli_num_rows($rconfig_query))
		{
			$rconfig_read = mysqli_fetch_assoc($rconfig_query);
			$routing_locinfo[$rname] = $rconfig_read['value'];
			$rconfig_id = $rconfig_read['id'];
		}
		if(isset($_POST[$rfieldname])) 
		{
			if($rconfig_id)
			{
				$success = lavu_query("update `config` set `value`='[1]' where `id`='[2]'",$_POST[$rfieldname],$rconfig_id);
			}
			else
			{
				$success = lavu_query("insert into `config` (`location`,`setting`,`value`,`type`) values ('[1]','[2]','[3]','[4]')",$location_info['id'],$rconfigname,$_POST[$rfieldname],$routing_config_setting);
			}
			if($success)
				$routing_locinfo[$rname] = $_POST[$rfieldname];
		}
	}

	$a_api_credentials = DELIVERY_ROUTING_SETTINGS::get_api_credentials();
	
	class DELIVERY_ROUTING_SETTINGS {

		// gets or sets the routing functionality
		// @$b_enable: TRUE or FALSE, or NULL if the value is just being retrieved
		// @return: TRUE if routing is enabled, FALSE otherwise
		function routing_enabled($b_enable = NULL) {

			global $data_name;
			global $b_routing_enabled;

			$conn = ConnectionHub::getConn('rest');

			$spec = array('setting'=>'routing_location_setting', 'type'=>'routing_location_setting');

			if ($b_enable === NULL) {
				// GET the config setting
				if ($b_routing_enabled === NULL) {
					$result = $conn->getConfig($spec);
					if (($result->num_rows !== 0) and ((bool) $result->fetch_assoc()['value'] === TRUE)) {
						$b_routing_enabled = TRUE;
					}
				}
				return $b_routing_enabled;
			} else {
				// SET the config setting
				$vals = array('value' => (bool) $b_enable);
				$conn->setConfig($spec, $vals);
				return $b_enable;
			}
		}

		// builds a key to authenticate this account with ERS
		// @$i_time: the time to create a key for (usually strtotime('now'))
		// @return: an md5 hash key
		function build_key($i_time) {
			global $location_info;
			global $a_api_credentials;

			$s_minute = date('i',strtotime( (-(int)date('i')%10).' minutes' ));
			$s_time = date('Y').date('d').$s_minute;
			$s_seed = $a_api_credentials['api_key'].$s_time.'Brian Dennedy';
			$s_key_time = md5($s_seed);
			return rawurlencode($s_key_time);
		}

		// gets the location to refresh the page to with window.location =
		function reload_location() {
			//$a_uri = explode('index.php?', $_SERVER['REQUEST_URI']);
			//$s_uri = 'index.php?'.$a_uri[1];
			$s_uri = "?mode=settings_delivery_routing_settings";
			return $s_uri;
		}

		// gets the location to do a curl to
		function curl_location() {
			//$a_uri = explode('index.php?', str_replace('mode=', 'widget=', $_SERVER['REQUEST_URI']));
			//$s_uri = 'index.php?'.$a_uri[1].'&drs_ajax';
			$s_uri = "index.php?widget=delivery_routing_settings&drs_ajax";
			return $s_uri;
		}

		// get the api key and token
		function get_api_credentials() {
			global $location_info;
			require_once_get_contents(dirname(__FILE__).'/api.php');
			return areas_api_get_api_credentials($location_info['id']);
		}

		// get a routing setting from the config table
		// @return: the value
		function get_setting($s_setting, $s_default = '', $s_config_type = 'ers_routing_setting') {
			global $location_info;
			$conn = ConnectionHub::getConn('rest');
			$result = $conn->getConfig(array('setting'=>$s_setting, 'type'=>$s_config_type, 'location'=>$location_info['id']));
			if ($result->num_rows === 1)
				return $result->fetch_assoc()['value'];
			else
				return $s_default;
		}

		// update a routing setting in the config table
		// @return: the new setting, or FALSE on failure
		function update_setting($s_setting, $s_default, $s_value, $s_value_type, $s_config_type = 'ers_routing_setting') {
			global $location_info;
			global $data_name;

			// get and update the current value
			switch ($s_value_type) {
				case 'hour':
				case 'minute':
				case 'ampm':
					$a_time = ''; $am_pm = '';
					self::split_setting($s_setting, $s_default, $s_config_type, $a_time, $am_pm);
					$s_am_pm = ($am_pm === NULL ? '' : $am_pm);
					if ($s_value_type == 'hour')
						$s_value = str_pad($s_value,2,'0',STR_PAD_LEFT).':'.$a_time[1].$s_am_pm;
					else if ($s_value_type == 'minute')
						$s_value = $a_time[0].':'.str_pad($s_value,2,'0',STR_PAD_LEFT).$s_am_pm;
					else
						$s_value = $a_time[0].':'.$a_time[1].$s_value;
					break;
				case 'string':
				default:
					$s_old_value = self::get_setting($s_setting, $s_default, $s_config_type);
					$s_value = $s_value;
			}

			$a_where_vars = array('setting'=>$s_setting, 'type'=>$s_config_type, 'location'=>$location_info['id']);
			$a_update_vars = array_merge($a_where_vars, array('value'=>$s_value));
			ConnectionHub::getConn('rest')->setConfig($a_where_vars, $a_update_vars);

			return $s_value;
		}

		// get the day start end time (dset) in a form compatible with routing
		// @return: the day start end time, in the form hh:mm
		function get_dset() {
			global $location_info;

			$s_dset = (string)$location_info['day_start_end_time'];
			$s_dset = str_pad($s_dset, 4, '0', STR_PAD_LEFT);
			$a_dset = array(substr($s_dset, 0, 2), substr($s_dset, 2, 2));
			$s_retval = date('H:ia', strtotime('2013-07-18 '.$a_dset[0].':'.$a_dset[1].':00'));
			return $s_retval;
		}

		// splits a time setting into its constituent parts
		// @$s_setting, $s_default, $s_type: passed on to get_setting()
		// @$a_time: the return value of the time, in the form array((int)hour, (int)minute)
		// @$am_pm: the return value of the ampm, one of 'am', 'pm', or NULL
		function split_setting($s_setting, $s_default, $s_config_type, &$a_time, &$am_pm) {
			$s_time = ($s_config_type !== NULL ? self::get_setting($s_setting, $s_default, $s_config_type) : self::get_setting($s_setting, $s_default));
			$a_time = explode(':', str_ireplace(array('am','pm'), '', $s_time));
			if (stripos($s_time, 'am') !== FALSE)
				$am_pm = 'am';
			else if (stripos($s_time, 'pm') !== FALSE)
				$am_pm = 'pm';
			else
				$am_pm = NULL;
		}

		// creates a series of select boxes to update the times of the given setting
		// @$s_setting, $s_default, $s_type: passed on to get_setting()
		// @return: a string of html code that includes select boxes
		//     includes an hour and minutes, and includes an ampm if the value passed in with the setting has an ampm part
		function create_setting_time_select($s_setting, $s_default, $s_type = NULL) {

			// get the current value of the setting
			$a_time = ''; $am_pm = '';
			self::split_setting($s_setting, $s_default, $s_type, $a_time, $am_pm);

			// create the hour select
			$s_select['hour'] = "<select onchange='apply_setting(\"hour\", \"{$s_setting}\", window.account_settings.settings[\"{$s_setting}\"], $(this).val());'>";
			for($i = 0; $i < ($am_pm !== NULL ? 24 : 12); $i++) {
				$s_selected = ($i == (int)$a_time[0] ? ' SELECTED' : '');
				if ($am_pm !== NULL)
					$s_printval = date('g', strtotime("2013-07-18 {$i}:00:00"));
				else
					$s_printval = $i;
				$s_select['hour'] .= "<option value='{$i}'{$s_selected}>{$s_printval}</option>";
			}
			$s_select['hour'] .= "</select>";

			// create the minute select
			$s_select['min'] = "<select onchange='apply_setting(\"minute\", \"{$s_setting}\", window.account_settings.settings[\"{$s_setting}\"], $(this).val());'>";
			for($i = 0; $i < 60; $i++) {
				$s_selected = ($i == (int)$a_time[1] ? ' SELECTED' : '');
				$s_select['min'] .= "<option value='{$i}'{$s_selected}>".date('i', strtotime("2013-07-18 00:{$i}:00"))."</option>";
			}
			$s_select['min'] .= "</select>";

			// create the am/pm select
			if ($am_pm !== NULL) {
				$s_select['ampm'] = "<select onchange='apply_setting(\"ampm\", \"{$s_setting}\", window.account_settings.settings[\"{$s_setting}\"], $(this).val());'>";
				foreach(array('am','pm') as $ampm) {
					$s_selected = ($ampm == $am_pm ? ' SELECTED' : '');
					$s_select['ampm'] .= "<option value='{$ampm}'{$s_selected}>{$ampm}</option>";
				}
				$s_select['ampm'] .= "</select>";
			}

			return implode('', $s_select);
		}

		// creates a series of select boxes to update the times of the given setting
		// @$s_setting, $s_default, $s_type: passed on to get_setting()
		// @$a_values: an array of possible values (as strings)
		// @return: a string of html code that includes a select box
		function create_setting_custom_select($s_setting, $a_values, $s_default, $s_type = NULL) {

			// get the current value of the setting
			$s_value = ($s_type !== NULL ? self::get_setting($s_setting, $s_default, $s_type) : self::get_setting($s_setting, $s_default));

			// create the select
			$s_select['val'] = "<select onchange='apply_setting(\"string\", \"{$s_setting}\", \"{$s_value}\", $(this).val());'>";
			foreach($a_values as $v) {
				$s_selected = ($v == $s_value ? ' SELECTED' : '');
				$s_select['val'] .= "<option value='{$v}'{$s_selected}>{$v}</option>";
			}
			$s_select['val'] .= "</select>";

			return implode('', $s_select);
		}

		function draw_routing() {
			global $routing_locinfo;
			global $location_info;
			global $routing_locinfo_fields;
			global $routing_locinfo_titles;
			
			$s_update_enable = self::routing_enabled() ? speak("Save") : speak("Enable Routing");
			
?>
<style type="text/css">
	a {
		cursor: pointer;
	}
	td.form_header {
		background-color: #98b624;
		color: #fff;
		font-weight: bold;
		height: 27px;
		padding: 5px 0 2px 0;
		text-align: center;
	}
	td.form_header span {
		font-size: 12px;
		color: #fff;
	}
	td.form_row_left {
		text-align: right;
		background-color: #eee;
		padding-left: 32px;
		vertical-align: top;
	}
	td.form_row_left font {
		color: #588604;
		font-size: 14px;
	}
	td.form_row_right {
		text-align: left;
		background-color: #eee;
		vertical-align: top;
		padding-right: 32px;
	}
</style>
<!--<div style="font-weight:bold;">
	<font>Note: To enable the Routing feature please contact Lavu Support. </font><a href="#" onclick="var me = $(this); var expand = me.siblings('.expand'); var other = me.siblings('font'); expand.css({ width: other.css('width') }); expand.show(500); me.hide();">>>></a>
	<div style="display:none; font-weight:normal; text-align:left;" class="expand">
		Webpage: <a href="?mode=support">Support</a><br />
		Email: <a href="mailto:support@poslavu.com">support@poslavu.com</a><br />
		Phone: 855-528-8457<br />
	</div>
</div>-->
<br />
<table cellpadding=4>
		
	<tr>
		<td colspan="2" class="form_header">Edit Routing Settings</td>
	</tr>
<?php for($n=0; $n<count($routing_locinfo_fields); $n++)  { 
		$rname = $routing_locinfo_fields[$n];
		$rfieldname = "start_" . $rname;
		$rconfigname = "routing_start" . $rname;
?>
	<tr>
		<td class="form_row_left">
			<?php if(isset($routing_locinfo_titles[$rname])) $rtitle = $routing_locinfo_titles[$rname]; else $rtitle = $rname; echo speak('Starting ' . ucwords($rtitle)); ?>
		</td>
		<td class="form_row_right">
        	<?php 
				$rfieldvalue = str_replace("\"","&quot;",$routing_locinfo[$rname]);
				echo "<input type='text' name='$rfieldname' value=\"$rfieldvalue\" onkeyup=\"window.account_settings.warehouse.$rname = this.value; window.account_settings.address.$rname = this.value; window.account_settings.company.$rname = this.value\" onblur='apply_setting(\"string\", \"$rconfigname\", \"\", this.value);'/>";
			?>
        </td>
    </tr>
<?php } 
if(1==2) {
?>
	<tr>
		<td class="form_row_left">
			<?php echo $s_update_enable; ?>
		</td>
		<td class="form_row_right">
			<?php
				global $location_info;
				$anchor_start = "<a style='cursor:pointer; font-family:Verdana; font-size:12px; color:#000088' onclick='if(confirm(\"Navigate to Location Settings?\")) window.location = \"index.php?mode=settings_location_settings\"'>";
				if(trim($location_info['address'])=="")
				{
					echo "** You have no address.  Please click ".$anchor_start."here</a> to enter your address";
				}
				else if(trim($location_info['city'])=="")
				{
					echo "** You have no city.  Please click ".$anchor_start."here</a> to enter your address";
				}
				else if(trim($location_info['state'])=="")
				{
					echo "** You have no state.  Please click ".$anchor_start."here</a> to enter your address";
				}
				else if(trim($location_info['zip'])=="")
				{
					echo "** You have no zip.  Please click ".$anchor_start."here</a> to enter your address";
				}
				else
				{
					echo $anchor_start . $location_info['address'] . " " . $location_info['city'] . ", " . $location_info['state'] . " " . $location_info['zip'] . "</a>";
				}
			?>
		</td>
	</tr>
<?php } ?>
	<tr>
		<td class="form_row_left">
			<?php echo speak('Delivery Earliest Departure'); ?>
		</td>
		<td class="form_row_right">
			<?php echo self::create_setting_time_select('delivery earliest departure', self::get_dset()) ?>
		</td>
	</tr>
	<tr>
		<td class="form_row_left">
			<?php echo speak('Delivery Window / Maximum Early Arrival Allowed'); ?>
		</td>
		<td class="form_row_right">
			<?php echo self::create_setting_time_select('delivery window', '0:30') ?>
		</td>
	</tr>
	<tr>
		<td class="form_row_left">
			<?php echo speak('Time At Delivery Location'); ?>
		</td>
		<td class="form_row_right">
			<?php echo self::create_setting_time_select('delivery setup time', '0:02') ?>
		</td>
	</tr>
	<tr>
		<td class="form_row_left">
			<?php echo $s_update_enable; ?>
		</td>
		<td class="form_row_right">
			<input type="button" value="<?php echo $s_update_enable; ?>" onclick="apply_account_settings_to_ers()" />
		</td>
	</tr>
	
</table>
<div id="loading" style="display:none;">
	<div class="overlay" style="position:fixed; left:0; top:0; width:100%; height:100%; background-color:rgba(0,0,0,0.5);"></div>
	<div class="popup" style="position:fixed; left:500px; top:200px; width:300px; height:150px; background-color:#fff; border:1px solid black;">
		<table style="width:300px; text-align:center; margin-top:65px;">
			<tr>
				<td>Updating...</td>
			</tr>
		</table>
	</div>
</div>
<?php
			self::draw_javascript();
		}

		function draw_javascript() {

			// get the globals
			global $data_name;
			global $location_info;
			$s_requires_update = (self::get_setting('requires_update', '0') == '1' ? 'true' : 'false');

?>
<script type="text/javascript">

	window.requires_update = <?php echo $s_requires_update ?>;

	window.account_settings = {
		<?php
			global $routing_locinfo;
		?>
		address: {
			address: '<?php echo $routing_locinfo['address'] ?>',
			city: '<?php echo $routing_locinfo['city'] ?>',
			state: '<?php echo $routing_locinfo['state'] ?>',
			zip: '<?php echo $routing_locinfo['zip'] ?>'
		},
		warehouse: {
			address: '<?php echo $routing_locinfo['address'] ?>',
			city: '<?php echo $routing_locinfo['city'] ?>',
			state: '<?php echo $routing_locinfo['state'] ?>',
			zip: '<?php echo $routing_locinfo['zip'] ?>'
		},
		company: {
			address: '<?php echo $routing_locinfo['address'] ?>',
			city: '<?php echo $routing_locinfo['city'] ?>',
			state: '<?php echo $routing_locinfo['state'] ?>',
			zip: '<?php echo $routing_locinfo['zip'] ?>'
		},
		settings: {
			'delivery earliest departure': '<?php echo self::get_setting('delivery earliest departure', self::get_dset()) ?>',
			'delivery window': '<?php echo self::get_setting('delivery window', '0:30') ?>',
			'delivery setup time': '<?php echo self::get_setting('delivery setup time', '0:02') ?>',
			'default map provider': '<?php echo self::get_setting('default map provider', 'Google') ?>',
			'timezone': '<?php echo $location_info['timezone'] ?>'
		}
	};

	// get or set if an update of the settings is required before the routing page is viewed
	function update_required(set) {
		if (typeof(set) == 'boolean') {
			window.requires_update = set;
			$.ajax({
				url: '<?php echo self::curl_location() ?>&update_required',
				data: { requires_update: (set ? 1 : 0) },
				type: 'POST',
				async: true
			});
			return set;
		}
		return window.requires_update;
	}

	// open the routing page, updating settings if necessary
	function window_view_routing_page() {
		if (update_required()) {
			apply_account_settings_to_ers();
		}
		$('#a_view_ers_routing').click();
	}

	// update the ERS server with the new account settings
	// @complete: the javascript string to call when the settings have been updated
	function apply_account_settings_to_ers(complete) {
		var dataname = '<?php echo rawurlencode($data_name) ?>';
		var timezone = '<?php echo $location_info['timezone'] ?>';
		var api_token = '<?php global $a_api_credentials; echo rawurlencode($a_api_credentials['api_token']); ?>';
		var api_key = '<?php global $a_api_credentials; echo rawurlencode($a_api_credentials['api_key']); ?>';

		var loading = $('#loading');
		var overlay = loading.children(".overlay");
		var popup = loading.children(".popup");
		popup.css({ left: ($(window).width()/2-popup.width()/2) });
		popup.css({ top: ($(window).height()/2-popup.height()/2) });
		window.animate_loading_index++;
		animate_loading(window.animate_loading_index);
		loading.show();

		setTimeout(function() {

			// send the basic information
			$.ajax({
				url: '<?php echo self::curl_location() ?>&send_data_ers',
				data: { date:'2013-06-22', read_from_lavu:1, action:'init_account=1', init_account:1, dataname:('dataname='+dataname), key:'key=1', timezone:(timezone), api_token:(api_token), api_key:(api_key) },
				type: 'POST',
				async: true,
				success: function(m) {
					//alert(m);
					//console.log('a.success');
					update_ers_settings(loading, dataname, timezone, api_token, api_key, window.account_settings, complete);
				},
				error: function(a,b,c) {
					//console.log('a.error');
					update_ers_settings(loading, dataname, timezone, api_token, api_key, window.account_settings, complete);
				}
			});
		}, 100);
	}

	// update ers information
	function update_ers_settings(loading, dataname, timezone, api_token, api_key, account, complete) {
		var values = {};
		var length = 0;
		$.each(account, function(tablename,tabledata) {
			$.each(tabledata, function(setting, value) {
				values[length] = {
					tablename:(encodeURIComponent(tablename)), setting:(encodeURIComponent(setting)), value:(encodeURIComponent(value))
				};
				length++;
			});
		});
		data = { values: values, date:'2013-06-22', read_from_lavu:1, action:'update_account=1', update_account:1, dataname:('dataname='+dataname), key:('key='+'<?php echo self::build_key(strtotime('now')) ?>') };
		$.ajax({
			url: '<?php echo self::curl_location() ?>&send_data_ers',
			data: data,
			type: 'POST',
			async: true,
			success: function(m) {
				console.log('b.success');
				if (typeof(complete) == 'string')
					eval(complete);
				update_required(false);
				set_ers_enabled(loading, dataname, timezone, api_token, api_key, account);
			},
			error: function(a,b,c) {
				console.log('b.error');
				if (typeof(complete) == 'string')
					eval(complete);
				update_required(false);
				set_ers_enabled(loading, dataname, timezone, api_token, api_key, account);
			}
		});
	}

	// show the account as having been enabled
	function set_ers_enabled(loading, dataname, timezone, api_token, api_key, account) {
		$.ajax({
			url: '<?php echo self::curl_location() ?>&set_account_enabled',
			data: {},
			type: 'POST',
			async: true,
			success: function(m) {
				$('#loading').hide();
				window.location = '<?php echo self::reload_location() ?>';
			},
			error: function(a,b,c) {
				$('#loading').hide();
				window.location = '<?php echo self::reload_location() ?>';
			}
		});
	}

	window.animate_loading_index = 0;
	function animate_loading(index) {
		if (index != window.animate_loading_index)
			return;

		var jtd = $('#loading').children('.popup').find('td');
		var text = jtd.text();
		var length = text.length;
		var newlength = (length == 11 ? 8 : length+1);
		jtd.text("Updating...".substr(0, newlength));
		
		setTimeout('animate_loading('+index+');', 500);
	}

	// applies changes to settings to the account (as config settings)
	// and to the window.account_settings.settings variable
	function apply_setting(type, setting, dfault, value) {
		var data =  { type: type, setting: setting, dfault: dfault, value: value };
		update_required(true);
		$.ajax({
			url: '<?php echo self::curl_location() ?>&update_setting',
			data: data,
			type: 'POST',
			async: true,
			success: function(m) {
				actions = JSON.parse(m.trim());
				$.each(actions, function(k,v) {
					if (k == 'update_setting')
						window.account_settings.settings[setting] = v;
				});
			},
		});
	}

	setTimeout(function() {
		window.location = '<?php echo self::reload_location() ?>';
	}, 1000*60*10);

</script>
<?php
		}
	}
	
	$istr = "";
	$istr .= "<table width='100%'><tr><td width='100%' bgcolor='#eeeeee' align='center'>";
	$istr .= "<img src='/cp/images/routing_diagram.png' border='0' />";
	$istr .= "</td></tr></table>";
	$istr .= "<table><tr><td align='left'>";
	$istr .= "<br>Our Routing add-on gives you an easy to use interface for assigning drivers to upcoming orders";
	$istr .= "<br>and automatically finds the most efficient route, while still getting you there on time.";
	//$istr .= "<br>Although our routing addon is fully integrated with Lavu POS, it is a seperate product and";
	//$istr .= "<br>does include a monthly fee of $29.95.  However, we are offering free usage through August the 31st";
	//$istr .= "<br>for those that would like to give it a try.";
	$istr .= "<br>";
	$intro_description = $istr;
	
	if (($comPackage=="pizza" || $comPackage=="customer" || $comPackage=="deliver") && $modules->hasModule("packages.gold")) 
	{
		$delivery_options_enabled = false;
		$doptions_query = lavu_query("SELECT * FROM `config` WHERE `setting`='delivery_options'");
		if(mysqli_num_rows($doptions_query))
		{
			$doptions_read = mysqli_fetch_assoc($doptions_query);
			$doptions = explode("|o|",$doptions_read['value']);
			//for($n=0; $n<count($doptions); $n++)
			//{
			//	echo $n . ": " . $doptions[$n] . "<br>";
			//}
			if(isset($doptions[4]) && strtolower(trim($doptions[4]))=="true")
			{
				$delivery_options_enabled = true;
			}
		}
		if($delivery_options_enabled)
		{
			 $compackagenum_required = 23;
			 $comPackageNum = "0";
			 if(isset($location_info) && isset($location_info['component_package'])) $comPackageNum = $location_info['component_package'];
			 if(isset($location_info) && isset($location_info['id'])) $location_info_id = $location_info['id'];
			 
			 if($comPackageNum==$compackagenum_required) 
			 {
				if (isset($_GET['drs_ajax'])) 
				{
					if (isset($_GET['send_data_ers'])) 
					{
						$s_data = 'data='.rawurlencode(json_encode($_POST));
						$s_url = 'http://routing.lavu.com/maps/rwlavu.php?date=2013-06-22&read_from_lavu=1&'.$_POST['action'].'&'.$_POST['key'].'&'.$_POST['dataname'];
						echo $s_url;
						echo "\n------------------------------------------------------------------------------------------------";
						echo "\n------------------------------------------------------------------------------------------------";
						echo "\n------------------------------------------------------------------------------------------------\n";
						//mail("corey@lavu.com","Json stuff",$_POST['timezone'],"From: json@lavu.com");
						$cu = curl_init();
						curl_setopt($cu, CURLOPT_URL, $s_url);
						curl_setopt($cu, CURLOPT_PORT, 80);
						curl_setopt($cu, CURLOPT_POST, 1);
						curl_setopt($cu, CURLOPT_POSTFIELDS, $s_data);
						$success = curl_exec($cu);
						if (!curl_errno($cu)) {
							$info = curl_getinfo($cu);
							echo $info;
						} else {
							echo 'Curl error: '.curl_error($cu);
						}
						curl_close($cu);
						echo "\n------------------------------------------------------------------------------------------------";
						echo "\n------------------------------------------------------------------------------------------------";
						echo "\n------------------------------------------------------------------------------------------------\n";
						var_dump($success);
					} 
					else if (isset($_GET['set_account_enabled'])) 
					{
						DELIVERY_ROUTING_SETTINGS::routing_enabled(TRUE);
					} 
					else if (isset($_GET['update_setting'])) 
					{
						if (isset($_POST['setting']) && isset($_POST['dfault']) && isset($_POST['value']) && isset($_POST['type'])) 
						{
							$success = DELIVERY_ROUTING_SETTINGS::update_setting($_POST['setting'], $_POST['dfault'], $_POST['value'], $_POST['type']);
							if ($success !== FALSE)
								echo '{"update_setting":"'.$success.'"}';
						}
					} 
					else if (isset($_GET['update_required'])) 
					{
						DELIVERY_ROUTING_SETTINGS::update_setting('requires_update', '0', $_POST['requires_update'], 'boolean');
					}
				} 
				else 
				{
					if(isset($_GET['disable_routing']))
					{
						$success = lavu_query("update `locations` set `component_package`='[1]' where `id`='[2]'","",$location_info_id);
						if($success)
						{
							echo "In App Routing has been disabled.  Please reload settings in Lavu POS.<br><br><input type='button' value='Continue >>' onclick='window.location = \"index.php?mode={$section}_{$mode}\"' />";
						}
						else
						{
							echo speak("Unable to update your account");
						}
					}
					else
					{
						DELIVERY_ROUTING_SETTINGS::draw_routing();
	
						if(isset($_GET['autoenable']) && $_GET['autoenable']=="1")
						{
							echo "<script language='javascript'>";
							echo "apply_account_settings_to_ers(); ";
							echo "</script>";
						}
						else
						{
							
							echo "<br><br>";
							echo "<input type='button' value='".speak('Disable In-App Routing')."' onclick='if(confirm(\"Are you sure you want to disable routing for this account?\")) window.location = \"index.php?mode={$section}_{$mode}&disable_routing=1\"' />";
						}
					}
				}
			} 
			else 
			{
				//require_once(dirname(__FILE__) . "/../../sa_cp/tools/convert_product.php");
				if(isset($_GET['enable_routing']))
				{
					$success = lavu_query("update `locations` set `component_package`='[1]' where `id`='[2]'",$compackagenum_required,$location_info_id);
					if($success)
					{
						echo speak("Enabling In App Routing")."...<br><br>";
						echo "<script language='javascript'>";
						echo "window.location.replace(\"index.php?mode={$section}_{$mode}&autoenable=1\"); ";
						echo "</script>";
						//echo "In App Routing has been enabled.  Please continue to setup<br><br><input type='button' value='Continue >>' onclick='window.location = \"index.php?mode={$section}_{$mode}\"' />";
					}
					else
					{
						echo speak("Unable to update your account");
					}
				}
				else //---------------------- Routing is not enabled
				{
					echo $intro_description;
					echo "<br><li>Routing is currently disabled for this account.  Would you like to enable it?<br><br><input type='button' value='".speak('Enable In App Routing')."' onclick='window.location = \"index.php?mode={$section}_{$mode}&enable_routing=1\"' />";
					echo "</td></tr></table>";
				}
			}
		}
		else //---------------------- Delivery Features are not enabled
		{
			echo $intro_description;
			$deliveries = 'Navigate to "Deliveries" setup page';
			echo "<br><li>To use routing features, you must collect delivery information when taking your orders. <br><br>Please visit this page after enabling delivery settings<br>if you would like to sign up for routing features<br>";
			echo "<br><br><input type='button' class='navigateBtn' value='".speak($deliveries)."' onclick='window.location = \"?mode=settings_delivery_settings\"' />";
		}
	} 
	else  //----------------------- Customer Management is not enabled
	{
		echo $intro_description;
		$management = 'Navigate to "Customer Management" setup page';
		if($modules->hasModule("components.customer"))
		{
			echo "<br><li>Routing requires customer management in Lavu POS to be enabled<br><br>Please vist this page after enabling customer management<br>if you would like to sign up for routing features<br>";
			echo "<br><br><input type='button' class='navigateBtn' value='".speak($management)."' onclick='window.location = \"?mode=settings_customer_settings\"' />";
		}
		else
		{
			echo "<br><li>Routing requires customer management in Lavu POS to be enabled<br><br>Customer management is included with Gold and Pro Licenses, and requires an upgrade.<br><br>To visit the Customer Management Page, please click below<br>";
			echo "<br><br><input type='button' class='navigateBtn' value='".speak($management)."' onclick='window.location = \"?mode=settings_customer_settings\"' />";
		}
	}
	
?>
