<?php
require_once(dirname(__FILE__)."/resources/core_functions.php");
$data_name = $_GET['dn'];
$modal_number = $_GET['modal_number'];
require_once(resource_path()."/lavuquery.php");
require_once(resource_path()."/action_tracker.php");
session_start($_GET['session_id']);
// $req_schm = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] != 'off') ? 'https' : 'http';
// $styleSheetPath = $req_schm. "://" . $_SERVER['HTTPS_HOST'] . "/cp/styles/styles.css";
// $styleSheetPath = "/styles/styles.css";
$display = "<!DOCTYPE html> ";
$display .= "<html>";
$display .= "<head>";
$display .= "<meta http-equiv=\"Content-Type\" content=\"text/html;charset=ISO-8859-1\">";
$display .= "<link rel='stylesheet' type='text/css' href='styles/styles.css'>";
$display .= "<script type=\"text/javascript\" src=\"/cp/scripts/jquery.min.1.8.3.js\"></script>
				<link href=\"/cp/css/select2-4.0.3.min.css\" rel=\"stylesheet\" />
				<script src=\"/cp/scripts/select2-4.0.3.min.js\"></script>";

$display .= "</head>";
$display .= "<body>";
$display .= "<p align='center'>".speak("Add or Remove Inventory Categories.").".</p>";

        $field = "";

        $qtitle_category = false;
        $qtitle_item = "Category";
        $inventory_tablename = "inventory_categories";
        $inventory_primefield = "name";

        $inventory_orderby = "name";

        $qfields = array();
        $qfields[] = array("name", "name", speak("Value"), "input");
        $qfields[] = array("id","id");
        $qfields[] = array("delete");

        require_once(resource_path() . "/dimensional_form.php");
        $qdbfields = get_qdbfields($qfields);

        $details_column = "";//"Zip Codes";
        $send_details_field = "";//"zip_codes";

        $cat_count = 1;

        if (isset($_POST['posted']))
        {

            $item_count = 1;
            while(isset($_POST["ic_".$cat_count."_".$inventory_primefield."_".$item_count]))
            {
                $item_value = array(); // get posted values
                for($n=0; $n<count($qfields); $n++)
                {
                    $qname = $qfields[$n][0];
                    $qtitle = (isset($qfields[$n][2]))?$qfields[$n][2]:false;
                    $set_item_value = $_POST['ic_'.$cat_count.'_'.$qname.'_'.$item_count];
                    if($qtitle && $set_item_value==$qtitle) $set_item_value = "";

                    $item_value[$qname] = $set_item_value;
                }
                $item_id = $item_value['id'];
                $item_delete = $item_value['delete'];
                $item_name = $item_value[$inventory_primefield];

                if ($item_id == "0") {

                    $dbfields = array();
                    for($n=0; $n<count($qdbfields); $n++)
                    {
                        $qname = $qdbfields[$n][0];
                        $qfield = (isset($qdbfields[$n][1]))?$qdbfields[$n][1]:$qname;
                        $dbfields[] = array($qfield,$item_value[$qname]);
                    }
                    update_poslavu_dbrow($inventory_tablename,$dbfields, "");

                } else {

                    if($item_delete=="1")
                    {
                        delete_poslavu_dbrow($inventory_tablename, $item_id);
                    }
                    else if($item_name!="")
                    {
                        $dbfields = array();
                        for($n=0; $n<count($qdbfields); $n++)
                        {
                            $qname = $qdbfields[$n][0];
                            $qfield = (isset($qdbfields[$n][1]))?$qdbfields[$n][1]:$qname;
                            $dbfields[] = array($qfield,$item_value[$qname]);
                        }
                        update_poslavu_dbrow($inventory_tablename,$dbfields, $item_id);
                    }
                }
                $item_count++;
            }

            // LOCAL SYNC RECORD
            schedule_remote_to_local_sync($locationid,"table updated","inventory_categories");
            // END LOCAL SYNC RECORD
            $display .= "<script type='text/javascript'>window.parent.CloseModalPopUp($modal_number);</script>";
        }

        $qinfo = array();
        $qinfo["qtitle_category"] = $qtitle_category;
        $qinfo["qtitle_item"] = $qtitle_item;
        $qinfo["inventory_tablename"] = $inventory_tablename;
        $qinfo["inventory_orderby"] = $inventory_orderby;

        $form_name = "tilling";
        $field_code = create_dimensional_form($qfields,$qinfo,'');
        $display .= "<table cellspacing='0' cellpadding='3' align='center'><tr><td align='center' style='border:2px solid #DDDDDD'>";
        $display .= "<form name='tilling' method='post' action='' align='center'>";
        $display .= "<input type='hidden' name='posted' value='1'>";
        $display .= $field_code;
        $display .= "&nbsp;<br><input type='submit' value='".speak("Save")."' style='width:120px'>";
        $display .= "</form>";
        $display .= "</td></tr></table>";

        echo $display;


?>