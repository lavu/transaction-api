<?php

	require_once(dirname(__FILE__)."/../resources/json.php");

	define('UNKNOWN', 0);
	define('DISTRIBUTOR', 1);
	define('USER', 2);
	define('LAVUADMIN', 3);

	class HistoryObject {

		function __construct($s_json, $s_action = "", $s_details = "") {
			if ($s_json != "") {
				$object = LavuJson::json_decode($s_json);
				foreach($object as $k=>$v)
					$this->$k = $v;
				return;
			}

			$s_datetime = date("Y-m-d H:i:s");
			$o_userstats = self::get_user_stats();
			$this->$s_datetime = new stdClass();
			$this->$s_datetime->action = $s_action;
			$this->$s_datetime->details = $s_details;
			$this->$s_datetime = (object) array_merge((array)$this->$s_datetime, (array)$o_userstats);
		}

		public function updateHistory(&$a_update) {

			// check the input
			if (!is_array($a_update) || !isset($a_update['action']) || !isset($a_update['details']))
				return FALSE;

			// get some variables
			$o_new_history = self::create_standard_history($a_update['action'], $a_update['details']);

			// update the history
			foreach($o_new_history as $k=>$v)
				$this->$k = $v;

			return TRUE;
		}

		public function toString() {
			return LavuJson::json_encode($this);
		}

		/********************************************************************************
		 * History object functions
		 ********************************************************************************/

		// create a standard history object
		public static function create_standard_history($s_action = 'created', $s_details = '') {
			return new HistoryObject("", $s_action, $s_details);
		}

		// get the status of the user, for the history
		public static function get_user_stats() {

			global $loggedin;
			global $lavu_admin;

			// create the object
			$o_retval = new stdClass();
			$o_retval->username = "";
			$o_retval->usertype = UNKNOWN;

			// set it's values
			if (isset($loggedin)) {
				$o_retval->username = $loggedin;
				$o_retval->usertype = USER;
			}
			if (isset($lavu_admin)) {
				$o_retval->usertype = LAVUADMIN;
			}

			return $o_retval;
		}

		// update the history of the invoice
		// @$a_update: an array representing the update, in the form:
		//     array('action'=>string, 'details'=>string)
		// @$s_json_history: the json string of the history, also used as the return value
		// @return: TRUE on success or FALSE on failure
		public static function update_history(&$a_update, &$s_json_history) {

			// update the history
			$o_history = new HistoryObject($s_json_history);
			$o_history->updateHistory($a_update);

			// return the new history
			$s_json_history = $o_history->toString();
			return TRUE;
		}
	}

?>