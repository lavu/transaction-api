<?php
class BackendUser {
	protected
		$id,
		$company_code,
		$username,
		$f_name,
		$l_name,
		$email,
		$access_level,
		$PIN,
		$quick_serve,
		$loc_id,
		$service_type,
		$address,
		$phone,
		$mobile,
		$lavu_admin,
		$force_remote,
		$rf_id,
		$role_id,
		$deleted_date,
		$created_date,
		$employee_class,
		$payrate,
		$active,
		$clocked_in,
		$info,
		$_use_tunnel,
		$reporting_groups,
		$report_access,
		$viewed_messages,
		$mobile_carrier,
		$restricted_user,
		$access_white_list;

	private function __construct(){
		$this->id = null;
		$this->company_code = null;
		$this->username = null;
		$this->f_name = null;
		$this->l_name = null;
		$this->email = null;
		$this->access_level = null;
		$this->PIN = null;
		$this->quick_serve = null;
		$this->loc_id = null;
		$this->service_type = null;
		$this->address = null;
		$this->phone = null;
		$this->mobile = null;
		$this->lavu_admin = null;
		$this->force_remote = null;
		$this->rf_id = null;
		$this->role_id = array();
		$this->deleted_date = null;
		$this->created_date = null;
		$this->employee_class = null;
		$this->payrate = null;
		$this->active = null;
		$this->clocked_in = null;
		$this->info = null;
		$this->_use_tunnel = null;
		$this->reporting_groups = null;
		$this->report_access = null;
		$this->viewed_messages = array();
		$this->mobile_carrier = null;
		$this->restricted_user = false;
		$this->access_white_list = array();

		$current_username = admin_info('username');
		if (empty($current_username))
		{
			return;
		}

		if (($query_result = lavu_query("SELECT * FROM `users` WHERE `username` = '[1]' AND `_deleted` = '0'", $current_username)) !== FALSE)
		{
			if(mysqli_num_rows($query_result) ){
				$row = mysqli_fetch_assoc( $query_result );
				$this->id = $row['id'];
				$this->company_code = $row['company_code'];
				$this->username = $row['username'];
				$this->f_name = $row['f_name'];
				$this->l_name = $row['l_name'];
				$this->email = $row['email'];
				$this->access_level = $row['access_level'];
				$this->PIN = $row['PIN'];
				$this->quick_serve = $row['quick_serve'];
				$this->loc_id = $row['loc_id'];
				$this->service_type = $row['service_type'];
				$this->address = $row['address'];
				$this->phone = $row['phone'];
				$this->mobile = $row['mobile'];
				$this->lavu_admin = $row['lavu_admin'];
				$this->force_remote = $row['force_remote'];
				$this->rf_id = $row['rf_id'];
				$this->role_id = explode('|',$row['role_id']);
				$this->deleted_date = $row['deleted_date'];
				$this->created_date = $row['created_date'];
				$this->employee_class = $row['employee_class'];
				$this->payrate = $row['payrate'];
				$this->active = $row['active'];
				$this->clocked_in = $row['clocked_in'];
				$this->info = $row['info'];
				$this->_use_tunnel = $row['_use_tunnel'];
				$this->reporting_groups = $row['reporting_groups'];
				$this->report_access = $row['report_access'];
				$this->viewed_messages = explode(',',$row['viewed_messages']);
				$this->mobile_carrier = $row['mobile_carrier'];
				$this->restricted_user = $row['restricted_user'];
				$this->access_white_list = explode(',',$row['access_white_list']);

			}  else if( admin_info('username') == 'poslavu_admin' ){
				//might be a backend user
				$this->username = 'poslavu_admin';
				$this->f_name = 'poslavu';
				$this->l_name = 'admin';
				$this->active = '1';
				$this->lavu_admin = '1';
				$this->report_access = '99';
				$this->restricted_user = '0';
			}
		}
	}

	function ID() {
		return $this->id;
	}
	function companyCode() {
		return $this->company_code;
	}
	function username() {
		return $this->username;
	}
	function firstName() {
		return $this->f_name;
	}
	function lastName() {
		return $this->l_name;
	}
	function email() {
		return $this->email;
	}
	function accessLevel() {
		return $this->access_level;
	}
	function quickServe() {
		return $this->quick_serve;
	}
	function locID() {
		return $this->loc_id;
	}
	function serviceType() {
		return $this->service_type;
	}
	function address() {
		return $this->address;
	}
	function phone() {
		return $this->phone;
	}
	function mobile() {
		return $this->mobile;
	}
	function isLavuAdmin() {
		return $this->lavu_admin == '1';
	}
	function forceRemote() {
		return $this->force_remote == '1';
	}
	function rfID() {
		return $this->rf_id;
	}
	function roles() {
		return $this->role_id;
	}
	function deletedDate() {
		return $this->deleted_date;
	}
	function createdDate() {
		return $this->created_date;
	}
	function employeeClass() {
		return $this->employee_class;
	}
	function payrate() {
		return $this->payrate;
	}
	function isActive() {
		return $this->active == '1';
	}
	function isClockedIn() {
		return $this->clocked_in;
	}
	function info() {
		return $this->info;
	}
	function _use_tunnel() {
		return $this->_use_tunnel;
	}
	function reportingGroups() {
		return $this->reporting_groups;
	}
	function reportAccess() {
		return $this->report_access;
	}
	function viewedMessages() {
		return $this->viewed_messages;
	}
	function mobileCarrier() {
		return $this->mobile_carrier;
	}

	function isRestrictedUser(){
		return $this->restricted_user == '1';
	}

	function accessWhiteList() {
		return $this->access_white_list;
	}

	function isInWhiteList( $entry ){
		return (!$this->isRestrictedUser()) || ($this->restricted_user && in_array($entry, $this->access_white_list));
	}

	public static function currentBackendUser(){
		static $currentUser = null;
		if( !$currentUser ){
			$currentUser = new BackendUser();
		}
		return $currentUser;
	}
}