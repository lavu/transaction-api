<?php
class extendPage{
		
		private $pageArray=array();
		private $fromManage=false;
		
		function __construct($fromManage=false){
			$this->fromManage= $fromManage;
			$this->setPages();
		}
		
		private function setPages(){
			$query= mlavu_query("SELECT * FROM `poslavu_MAIN_db`.`extensions`");
			while($extensionRead= mysqli_fetch_assoc($query)){
				
				$this->pageArray[$extensionRead['id']]= new extension($extensionRead, $this->fromManage);
				
			}
		}	
		public function getPages(){
			return $this->pageArray;
		}			
		public function getPageById($id){
			return $this->pageArray[$id];
		}
	}
	class extension{
	
			private $json 	 	  ='';
			private $id			  ='';
			private $usage        ='';
			private $logo         ='';
			private $title        ='';
			private $features     ='';
			private $typeImg      ='';
			private	$link         ='';
			private $hasExtension =false;
			private $permNeeded   = '';			//permissions needed from the extender
			private $siteLink	  = '';
			private $loginLink	  = '';
			private $signupLink   = '';
			//private $LocalDB	  = '';
			private $loginComms   = '';
			private $jsonLinks    = '';
			private $fromManage   =false;
			public function __construct($extensionRead, $fromManage=false){
				$this->fromManage=$fromManage;
				$this->json= new Json();
				//$this->LocalDB = new Local_DB();
				
				$this->setID($extensionRead['id']);
				$this->setUsage($extensionRead['usage']);
				$this->setLogo($extensionRead['logo']);
				$this->setTitle($extensionRead['title']);
				$this->setFeatures($extensionRead['features']);
				$this->setTypeImage($extensionRead['typeImg']);
				$this->setLinks($extensionRead['link']);
				$this->setPermNeeded($extensionRead['permissions']);
				$this->setHasExtension();
				$this->setJsonLinks($extensionRead['link']);
				if(!$fromManage){
					$this->setLoginComms($extensionRead['login_communication']);
				}else{
					$this->loginComms='{}';	
				}
			}
			private function setID($id){
				$this->id= $id;
			}
			private function setUsage($usage){
				$this->usage = $usage;
			}
			private function setLogo($logo){
				$this->logo= $logo;
			}
			private function setTitle($title){
				$this->title = $title;
			}
			private function setFeatures($features){
				$this->features= $features;
			}
			private function setTypeImage($typeImg){
				$this->typeImg= $typeImg;
			}
			private function setLoginComms($loginComms){
			
				$this->loginComms= $this->buildLoginComms($loginComms);
			}
			private function setLinks($link){
				
				$decoded= $this->json->decode($link);
				$this->siteLink   = $decoded->siteLink;
				$this->loginLink  = $decoded->loginLink;
				$this->signupLink = $decoded->signupLink;
				
			}
			private function setJsonLinks($links){
				$this->jsonLinks= $links;
			}
			private function setPermNeeded($permNeeded){
				$this->permNeeded= $permNeeded;
			}
			private function setHasExtension(){
			
				$query= lavu_query("SELECT * FROM `config` where `setting`= 'extension' ");

				if( mysqli_num_rows($query)){
					while($result = mysqli_fetch_assoc($query)){
						if($result['value']==$this->id)
							$this->hasExtension=true;
					}
				}
			}
			private function buildLoginComms($loginComms){
			//echo "loginCOMMS ". $loginComms;
				$returnString='resellerId=poslavu';
				$decoded= $this->json->decode($loginComms);			
				//echo "<pre style='text-align:left'>".print_r($decoded,true)."</pre>";
				foreach($decoded as $location=>$val){
					$databaseInfo= explode("::", $location);
					
					$queryString='SELECT ';
					$keyArray=array();
					foreach($val as $k=>$info){
						$keyArray[]=$info;
						$needsDecode= explode("::", $k);
						if($needsDecode[0]=='decode'){
							if($databaseInfo[0]=='user'){
								if( $needsDecode[1]=='api_key')
									$queryString.="AES_DECRYPT(`".$needsDecode[1]."`,'lavuapikey') as `".$needsDecode[1]."`,";
								else if($needsDecode[1]=='api_token'){
									$queryString.="AES_DECRYPT(`".$needsDecode[1]."`,'lavutokenapikey') as `".$needsDecode[1]."`,";
								}
							}
							
						}else
							$queryString.="`".$k."`,";
					}
					
					$queryString= trim($queryString,",");
					$database= admin_info('database');
					global $location_info;
					
					$queryString.="FROM `[database]`.`".$databaseInfo[1]."` WHERE `id`='". $location_info['id']."'";
					//echo $queryString;
					if($databaseInfo[0]=='user'){	
						//echo $queryString;
						$result= mysqli_fetch_assoc(lavu_query($queryString, array("database"=>$database)));
						//echo print_r($result,true);
						$counter=0;
						foreach($result as $key=>$dbInfo){
							
							$returnString.="&".$keyArray[$counter++]."=". htmlspecialchars($dbInfo);
						}
						$queryString= trim($queryString,"&");
						
					}else if($databaseInfo[0]=='MAIN'){
						$counter=0;
						$result= mysqli_fetch_assoc(mlavu_query($queryString,array("database"=>"poslavu_MAIN_db")));
						foreach($result as $key=>$dbInfo){
							$returnString.="&".$keyArray[$counter++]."=".htmlspecialchars($dbInfo);
						}
						$queryString= trim($queryString,"&");

					}
					else if($databaseInfo[0]=='loginvars'){
						$counter=0;
						foreach($val as $locData)
							$returnString.="&".$keyArray[$counter++]."=". htmlspecialchars(admin_info($locData));
					}		
					
				}
				return $returnString;				
			}
			public function getJsonLinks(){
				return $this->jsonLinks;
			}
			public function getID(){
				return $this->id;
			}
			public function getParsedUsage(){
				return $this->parseUsage($this->usage);
			}
			public function getUsage(){
				return $this->usage;
			}
			public function getLogo(){
				return $this->logo;
			}
			public function getLogoAsHTML(){
				return "<a href= '".$this->siteLink."'><img src= 'http://admin.poslavu.com/manage/misc/AdvancedToolsFunctions/extensionLogos/". $this->logo."' width='240px' height='60px'></a>";
			}
			public function getTitle(){
				return $this->title;
			}
			public function getFeatures(){
			
				return $this->features;
			}
			public function getTypeImage(){
				return $this->typeImg;
			}
			public function getTypeImageAsHTML(){
				return "<img src= 'http://admin.poslavu.com/cp/images/". $this->typeImg.".png'>";
			}
			public function getLinkAsHTML(){
				if(!$this->permNeeded)
					return "<img src= 'http://admin.poslavu.com/images/arrowcircle.png' onclick='openExtensionPage(\"".$this->signupLink."\")'>";
				else{
					if($this->hasExtension)
						return "<img src= 'http://admin.poslavu.com/images/arrowcircle.png' onclick=' openExtensionPage(\"".$this->loginLink."?".$this->loginComms."\")'>";
					else{
						return "<img src= 'http://admin.poslavu.com/images/arrowcircle.png' onclick=' var permNeeded=".$this->permNeeded."; linkClicked(permNeeded,\"".$this->loginLink."?".$this->loginComms."\", \"".$this->id."\", \"".admin_info('loc_id')."\", \"".admin_info('dataname')."\")'>";
						}		
				}
			}
			public function getHasExtension(){
				return $this->hasExtension;
			}	
			private function parseUsage($jsonString){
										
				$decoded= $this->json->decode($jsonString);
		
				$returnString= $decoded->usage."<span style= 'color:#aecd37'>: </span>";
				foreach($decoded->features as $feature){
					$returnString.= $feature." <span style= 'color:#aecd37'>|</span> ";
				}
				return $returnString;

			}
			public function getFeaturesAsHTMLList(){
				
				$featuresArray= $this->json->decode($this->features);
				$returnString='<ul>';
				foreach($featuresArray as $feat){
					$returnString.="<li style='margin-bottom:5px;'>".$feat."</li>";	
				}
				
				$returnString.="</ul>";
				return $returnString;
			}
		
		}

?>