<?php
	// if( $_SERVER['REMOTE_ADDR'] == '74.95.18.90'){
	// 	error_log( '['.__FILE__.':'.__LINE__.'] ' . print_r( debug_backtrace(), true ) );
	// }
	// 
if(! interface_exists( 'MAIN_DB_Interface' ) ){
	interface MAIN_DB_Interface{
		
		public function insertIntoMAIN($table,$columns,$values, $condition, $limit, $order );
		
		public function updateMAIN($table, $columns, $values, $condition, $limit, $order );
		
			// returns an array with the data, or returns an empty array
		public function readFromMAIN($table,$columns, $values='', $condition='', $limit='', $order='');
		
	}	

	interface Local_DB_Interface{
		
		public function insertIntoLocation($table,$columns,$values, $condition='', $limit='', $order='' );
		
		public function updateLocation($table, $columns, $values,$condition='', $limit='', $order='' );
		
		public function readFromLocation($table,$columns, $condition='', $limit='', $order='' );
	}
}
