<?php
	// ONE OF THESE DAYS WE WILL FINISH THIS FILE, BUT NOT TODAY

	// DO NOT USE $_SERVER['DOCUMENT_ROOT']! That messes up things like the register page.
if( !class_exists('DB') ){
	require_once(dirname(__FILE__)."/DB_interface.php");
	if (!function_exists("admin_loggedin"))
	require_once(dirname(__FILE__)."/../../resources/core_functions.php");

	class DB {

		/* This class has been converted to be a non-static class. Code should get an instance
		   of it by calling:

		   $DB = ConnectionHub::getConn('rest')->getDBAPI();
		or
		   $DB = ConnectionHub::getConn('poslavu')->getDBAPI();

			getDBAPI() is written in such a way that only once instance of DB will be created
			for each conn (ie. one for 'rest' and one for 'poslavu') for efficiency. So it's
			OK to call the above code in each function that uses Ben Bean's DB functions.

			- drobbins

		*/

		function __construct($conn) {
			// This contructor is called by getDBAPI() and should not normally be called
			// by regular code.
			$this->conn = $conn;
		}

		public  function drawQuery($s_query, $a_vars) {
			foreach($a_vars as $k=>$v)
				$s_query = str_replace("[$k]", $v, $s_query);
			return $s_query;
		}

		 public function arrayToInsertClause(&$indexed_array) {
			$a_inserts = array();
			$a_values = array();
			foreach($indexed_array as $k=>$v) {
				$nk = $this->conn->escapeString($k);
				$a_inserts[] = '`'.$nk.'`';
				$a_values[] = '\'['.$nk.']\'';
			}
			return '('.implode(',',$a_inserts).') VALUES ('.implode(',',$a_values).')';
		}

		 public function arrayToInClause(&$indexed_array, $b_use_value = FALSE) {
			$a_vals = array();
			if ($b_use_value) {
				foreach($indexed_array as $v)
					if ($v !== '')
						$a_vals[] = $this->conn->escapeString($v);
			} else {
				foreach($indexed_array as $k=>$v)
					if ($k !== '')
						$a_vals[] = $this->conn->escapeString($k);
			}
			if (count($a_vals) == 0)
				return 'IN ()';
			return 'IN (\''.implode('\',\'', $a_vals).'\')';
		}

		 public function arrayToSelectClause(&$indexed_array) {
			$a_vals = array();
			foreach($indexed_array as $k=>$v)
				if ($k !== '')
					$a_vals[] = $this->conn->escapeString($k);
			return '`'.implode('`,`', $a_vals).'`';
		}

		 public function arrayToUpdateClause(&$indexed_array) {
			$a_updates = array();
			foreach($indexed_array as $k=>$v) {
				$nk = $this->conn->escapeString($k);
				$a_updates[] = '`'.$nk.'`=\'['.$nk.']\'';
			}
			return 'SET '.implode(',',$a_updates);
		}

		 public function arrayToWhereClause(&$indexed_array) {
			$a_wheres = array();
			foreach($indexed_array as $k=>$v) {
				$nk = $this->conn->escapeString($k);
				$a_wheres[] = '`'.$k.'`=\'['.$nk.']\'';
			}
			if (count($a_wheres) == 0)
				return '';
			return 'WHERE '.implode(' AND ', $a_wheres);
		}

		 private function getAllInTable_hasDeletedField($s_databasename, $s_tablename) {
			global $getAllInTable_tablesWithDelete;

			// check that the structure wasn't already loaded for this table
			if (isset($getAllInTable_tablesWithDelete[$s_tablename]))
				return $getAllInTable_tablesWithDelete[$s_tablename];

			// check if the table has a '_deleted' flag
			$b_has_deleted = FALSE;
			$columns_query = mlavu_query("SHOW COLUMNS IN `[databasename]`.`[tablename]`", array('databasename'=>$s_databasename, 'tablename'=>$s_tablename));
			while ($row = mysqli_fetch_assoc($columns_query)) {
				if ($row['Field'] == '_deleted') {
					$b_has_deleted = TRUE;
					break;
				}
			}
			mysqli_free_result($columns_query);

			$getAllInTable_tablesWithDelete[$s_tablename] = $b_has_deleted;
			return $b_has_deleted;
		}

		// returns whether it believes that you should use mlavu_query or not (TRUE or FALSE)
		 public function userMlavuQuery($s_dataname) {
			if ($s_dataname != '' && function_exists('admin_info') && admin_info('dataname') == $s_dataname)
				return FALSE;
			return TRUE;
		}
		 public function useMlavuQuery($s_dataname) {
			return $this->userMlavuQuery($s_dataname);
		}

		// duplicates a row in a table and returns the new row's id
		// @$s_database: database of the row
		// @$s_tablename: tablename of the row
		// @$s_id: id of the row
		// @$a_row: set to the row that is being duplicated
		// @return: the id of the row, or 0 on failure
		 public function duplicateRow($s_database, $s_tablename, $s_id, &$a_row = NULL) {
			$a_rows = $this->getAllInTable($s_tablename, array('id'=>$s_id), TRUE, array('databasename'=>$s_database));
			if (count($a_rows) == 0)
				return 0;
			$a_row = $a_rows[0];
			unset($a_row['id']);
			$s_insertclause = $this->arrayToInsertClause($a_row);
			$s_query_string = "INSERT INTO `[db_duplicateRow_database]`.`[db_duplicateRow_table]` $s_insertclause";
			$a_query_vars = array_merge(array('db_duplicateRow_database'=>$s_database, 'db_duplicateRow_table'=>$s_tablename), $a_row);
			$this->conn->LegacyQuery($s_query_string, $a_query_vars);

			$insert_id = $this->conn->insertID();
			if ($insert_id === FALSE)
				return 0;
			return $insert_id;
		}

		// returns an array of all rows in the table, or an empty array
		// $s_tablename the table name to read from
		// $a_wherevars an indexed array of values that must match
		// $b_use_maindb whether to use mlavu_query or lavu_query
		// $a_extras see below
		// $s_store_querystring a variable to return the full query string in
		 public function getAllInTable($s_tablename, $a_wherevars = NULL, $b_use_maindb = FALSE, $a_extras = NULL, &$s_store_querystring = NULL, &$a_passbyreff_retval = NULL) {
			$s_databasename = 'poslavu_MAIN_db';
			$s_selectclause = '*';
			$s_groupbyclause = '';
			$s_orderbyclause = '';
			$s_limitclause = '';
			$s_store_querystring = FALSE;
			$b_check_deleted = FALSE;
			$b_collapse_arrays = FALSE;
			$b_print_query = FALSE;

			$s_whereclause = '';
			if (is_array($a_wherevars) && count($a_wherevars) > 0)
				$s_whereclause = $this->arrayToWhereClause($a_wherevars);
			else
				$a_wherevars = array();

			if ($a_extras !== NULL) {
				// used to provide the databasename if $b_use_maindb is FALSE and the database is something beside poslavu_MAIN_db
				$s_databasename = isset($a_extras['databasename']) ? $a_extras['databasename'] : $s_databasename;
				// used to replace the default select clause (default is "*")
				$s_selectclause = isset($a_extras['selectclause']) ? $a_extras['selectclause'] : $s_selectclause;
				// used to replace the default where clause (default is built from $a_wherevars)
				$s_whereclause = isset($a_extras['whereclause']) ? $a_extras['whereclause'] : $s_whereclause;
				// used to replace the default group by clause (default is "")
				$s_groupbyclause = isset($a_extras['groupbyclause']) ? $a_extras['groupbyclause'] : $s_groupbyclause;
				// used to replace the default orderby clause (default is "")
				$s_orderbyclause = isset($a_extras['orderbyclause']) ? $a_extras['orderbyclause'] : $s_orderbyclause;
				// used to replace the default limit clause (default is "")
				$s_limitclause = isset($a_extras['limitclause']) ? $a_extras['limitclause'] : $s_limitclause;
				// if TRUE, does a query first to see if the table contains "_deleted" and adds "AND `_deleted`='0'" to the where clause
				$b_check_deleted = isset($a_extras['check_deleted']) ? $a_extras['check_deleted'] : FALSE;
				// if TRUE, prints the query to the error log
				$b_print_query = isset($a_extras['print_query']) ? $a_extras['print_query'] : FALSE;
				// if TRUE and the results only have one return value in them, collapses them all into the parent array
				$b_collapse_arrays = isset($a_extras['collapse_arrays']) ? $a_extras['collapse_arrays'] : FALSE;
			}

			if ($b_check_deleted) {
				// check if the table has a '_deleted' flag
				$b_has_deleted = $this->getAllInTable_hasDeletedField($s_databasename, $s_tablename);
				if ($b_has_deleted) {
					if ($s_whereclause == '') {
						$s_whereclause = "WHERE (`_deleted` = '' || `_deleted` = '0')";
					} else {
						$s_whereclause .= " AND (`_deleted` = '' || `_deleted` = '0')";
					}
				}
			}

			if ($b_use_maindb) {
				$s_query_string = "SELECT $s_selectclause FROM `[getAllInTable_database]`.`[getAllInTable_tablename]` $s_whereclause $s_groupbyclause $s_orderbyclause $s_limitclause";
				$a_query_vars = array_merge(array('getAllInTable_database'=>$s_databasename, 'getAllInTable_tablename'=>$s_tablename), $a_wherevars);
				$query = mlavu_query($s_query_string, $a_query_vars);
			} else {
				$s_query_string = "SELECT $s_selectclause FROM `[getAllInTable_tablename]` $s_whereclause $s_groupbyclause $s_orderbyclause $s_limitclause";
				$a_query_vars = array_merge(array('getAllInTable_tablename'=>$s_tablename), $a_wherevars);
				$query = lavu_query($s_query_string, $a_query_vars, "[_empty_]", "[_empty_]", "[_empty_]", "[_empty_]", "[_empty_]", TRUE, ConnectionHub::$useReplica);
			}
			if ($s_store_querystring !== NULL)
				$s_store_querystring = $this->drawQuery($s_query_string, $a_query_vars);

			if ($query === FALSE) {
				$a_infile_location = debug_backtrace();
				$s_infile_location = $a_infile_location[0]['file'].' on line '.$a_infile_location[0]['line'];
				if ($b_use_maindb)
					error_log('bad get all query in '.$s_infile_location.' (tablename: '.$s_tablename.', database: '.$s_databasename.', wherevars: '.print_r($a_wherevars,TRUE).') query ('.$this->drawQuery($s_query_string, $a_query_vars).')');
				else
					error_log('bad get all query in '.$s_infile_location.' (tablename: '.$s_tablename.', wherevars: '.print_r($a_wherevars,TRUE).') query ('.$this->drawQuery($s_query_string, $a_query_vars).')');
				return array();
			} else {
				if ($b_print_query)
					error_log($this->drawQuery($s_query_string, $a_query_vars));
			}
			if (mysqli_num_rows($query) == 0) {
				mysqli_free_result($query);
				if ($a_passbyreff_retval !== NULL) {
					$a_passbyreff_retval = array();
					return;
				}
				return array();
			}
			$a_retval = array();

			// grab the data from the query
			if ($b_collapse_arrays) {
				while ($row = mysqli_fetch_assoc($query)) {
					if (count($row) == 1 && !(isset($s_colname)))
						foreach($row as $k=>$v)
							$s_colname = $k;
					if (isset($s_colname))
						$a_retval[] = $row[$s_colname];
					else
						$a_retval[] = $row;
				}
			} else {
				while ($row = mysqli_fetch_assoc($query))
					$a_retval[] = $row;
			}
			unset($row);

			mysqli_free_result($query);
			if ($a_passbyreff_retval !== NULL) {
				$a_passbyreff_retval = $a_retval;
				unset($a_retval);
				return;
			}
			return $a_retval;
		}

		// helper function of getTableColumns()
		private  function getTableColumnsUseSelect($wt_rows) {
			$a_row = mysqli_fetch_assoc($wt_rows);
			mysqli_free_result($wt_rows);

			$a_retval = array();
			foreach($a_row as $k=>$v)
				$a_retval[] = $k;
			return $a_retval;
		}

		// returns an array of column names, or an empty array
		public  function getTableColumns($b_use_mlavu_query, $s_table, $s_database = '') {
			$table_query_vars = array('database'=>$s_database, 'table'=>$s_table);

			// try to do a select, first
			if ($b_use_mlavu_query)
				$table_query = mlavu_query("SELECT * FROM `[database]`.`[table]` LIMIT 1", $table_query_vars);
			else
				$table_query = lavu_query("SELECT * FROM `[table]` LIMIT `", $table_query_vars);
			if ($table_query !== FALSE && mysqli_num_rows($table_query) > 0)
				return $this->getTableColumnsUseSelect($table_query);

			// submit a query
			if ($b_use_mlavu_query && $s_database !== '')
				$table_query = mlavu_query("SHOW COLUMNS FROM `[database]`.`[table]`", $table_query_vars);
			else
				$table_query = lavu_query("SHOW COLUMNS FROM `[table]`", $table_query_vars);

			// check for a bad query
			if ($table_query === FALSE || mysqli_num_rows($table_query) == 0)
				return array();

			// get a list of the column names
			$a_retval = array();
			while($row = mysqli_fetch_assoc($table_query))
				$a_retval[] = $row['Field'];
			mysqli_free_result($table_query);
			return $a_retval;
		}

		// returns an array of all table names in the given database
		// returns an empty array() on failure
		public  function getDatabaseTables($s_database) {
			$query = mlavu_query("SHOW TABLES FROM `[database]`", array('database'=>$s_database));
			if ($query === FALSE || mysqli_num_rows($query) == 0)
				return array();
			$a_retval = array();
			while ($row = mysqli_fetch_assoc($query)) {
				foreach($row as $k=>$v) {
					$a_retval[] = $v;
					break;
				}
			}
			return $a_retval;
		}

		public function insertIntoMAIN($table,$columns,$values, $condition, $limit, $order ){

		}

		public function updateMAIN($table, $columns, $values, $condition, $limit, $order ){

		}

		// returns an array with the requested values, or an empty array
		public function readFromMAIN($table, $columns, $values='', $condition='', $limit='', $order=''){

		}

		// checks if the entry exists and updates/inserts it, as appropriate
		// $a_update_vars and $a_where_vars should be arrays indexed by column name, with values for the column values
		// @$wt_where_vars: either an array of values (`column_name`=>'value'), or an array of these arrays (any of which can match)
		// @$a_original_row: the value of the original row, if it is an update
		// @$i_insert_update_id: id of the inserted/updated row (if the insert succeeds or exactly one row was updated, and the table has an `id` column, and $i_insert_update_id !== NULL)
		// @$s_query_performed: if not NULL, is used to return the query just performed
		// @return: 'i'.mlavu_insert_id() if inserted, 'u'.mysql_affected_rows() if updated, or 'b' if the query was bad
		public  function insertOrUpdate($s_database, $s_table, &$a_update_vars, &$wt_where_vars, &$a_original_row = NULL, &$i_insert_update_id = NULL, &$s_query_performed = NULL) {
			$s_first_index = '';
			foreach($a_update_vars as $k=>$v) {
				$s_first_index = $k;
				break;
			}
			if ($i_insert_update_id !== NULL)
				$i_insert_update_id = 0;
			$wt_first_key = '';
			if (is_array($wt_where_vars) && count($wt_where_vars) > 0) {
				foreach ($wt_where_vars as $k=>$v) {
					$wt_first_key = $k;
					break;
				}
			}

			// there is a where clause, check if there are multiple possible matches or just one
			$b_insert = NULL;
			if (is_array($wt_where_vars) && count($wt_where_vars) > 0 && isset($wt_where_vars[$wt_first_key])) {

				// there are multiple possible matches, check each one
				// set $b_insert to TRUE if there are no matches and FALSE if there is at least one
				$firstItemIsArray = false;
				foreach($wt_where_vars as $key=>$val)
				{
					if($key && is_array($val)){
						$firstItemIsArray = true;
					}
					break;
				}

				if (is_array($wt_where_vars) && $firstItemIsArray) {

					// do a query for each possible match
					foreach($wt_where_vars as $a_where_vars) {
						$s_whereclause = $this->arrayToWhereClause($a_where_vars);
						$s_query = "SELECT `[first_index]` FROM `[insertOrUpdate_database]`.`[insertOrUpdate_table]` $s_whereclause";
						$a_query = array_merge(array('insertOrUpdate_database'=>$s_database, 'insertOrUpdate_table'=>$s_table, 'first_index'=>$s_first_index), $a_where_vars);
						$query = mlavu_query($s_query, $a_query);
						if ($query === FALSE) {
							if ($s_query_performed !== NULL) $s_query_performed = $this>drawQuery($s_query, $a_query);
							return 'b';
						}
						if (mysqli_num_rows($query) > 0) {
							$b_insert = FALSE;
							break;
						}
					}

					// none of the matches succeeded, insert
					// set $b_insert to TRUE
					if ($b_insert === NULL)
						$b_insert = TRUE;

				// there is only one possible match, check it
				// set $b_insert to TRUE if there are no matches and FALSE if there is at least one
				} else {
					$a_where_vars = $wt_where_vars;
					$s_whereclause = $this->arrayToWhereClause($a_where_vars);
					$s_selectclause = ($i_insert_update_id !== NULL ? '*' : "`{$s_first_index}`");
					$s_query = "SELECT [selectclause] FROM `[insertOrUpdate_database]`.`[insertOrUpdate_table]` $s_whereclause";
					$a_query = array_merge(array('insertOrUpdate_database'=>$s_database, 'insertOrUpdate_table'=>$s_table, 'selectclause'=>$s_selectclause), $a_where_vars);
					$query = mlavu_query($s_query, $a_query);
					if ($query === FALSE) {
						if ($s_query_performed !== NULL) $s_query_performed = $this->drawQuery($s_query, $a_query);
						return 'b';
					}
					if (mysqli_num_rows($query) > 0) {
						$b_insert = FALSE;
					} else {
						$b_insert = TRUE;
					}
				}

				// grab the id of the row to be updated
				if ($i_insert_update_id !== NULL) {
					$a_id_read = mysqli_fetch_assoc($query);
					if (isset($a_id_read['id']))
						$i_insert_update_id = $a_id_read['id'];
					unset($a_id_read);
				}

			// there is no where clause, assume it is an insertion
			} else {
				$b_insert = TRUE;
			}

			// do the insert or update, as prescribed by $b_insert
			// if $b_insert is TRUE then insert, otherwise update
			if ($b_insert) {
				$s_insertclause = $this->arrayToInsertClause($a_update_vars);
				$s_query = "INSERT INTO `[insertOrUpdate_database]`.`[insertOrUpdate_table]` $s_insertclause";
				$a_query = array_merge(array('insertOrUpdate_database'=>$s_database, 'insertOrUpdate_table'=>$s_table), $a_update_vars);
				$this->conn->LegacyQuery($s_query, $a_query);
				$i_insert_update_id = (int)mlavu_insert_id();
				$s_retval = 'i'.$i_insert_update_id;
			} else {
				$a_original_row = mysqli_fetch_assoc($query);
				$s_updateclause = $this->arrayToUpdateClause($a_update_vars);
				$s_query = "UPDATE `[insertOrUpdate_database]`.`[insertOrUpdate_table]` $s_updateclause $s_whereclause";
				$a_query = array_merge(array('insertOrUpdate_database'=>$s_database, 'insertOrUpdate_table'=>$s_table), $a_update_vars, $a_where_vars);
				$this->conn->LegacyQuery($s_query, $a_query);
				$s_retval = 'u'.$this->conn->affectedRows();
			}

			if ($s_query_performed !== NULL) $s_query_performed = $this->drawQuery($s_query, $a_query);
			if (isset($query) && is_resource($query)) mysqli_free_result($query);
			return $s_retval;
		}

		// attempts to find all matches rows in the given table/database that match all given rules
		// $a_params a list of extra parameters, including
		//     rules:
		//         colname: the name to match
		//         match_value: the value to match to
		//         [ operator: the operator to match with (eg '=', optional) ]
		//         [ match_degree: the degree to match by (0-100, optional) ]
		public  function findMatch($s_database, $s_tablename, $a_params) {
			$a_match_by_degree = array();
			$a_where_vars = array();
			$a_wheres = array();
			$a_selects = array('id'=>'id');

			// get all nested parameters in $a_params
			foreach($a_params as $k=>$a_param) {
				switch($k) {

				// get every rule
				case 'rules':
					foreach($a_param as $a_rule) {
						$s_colname = $this->conn->escapeString($a_rule['colname']);
						$i_match_degree = isset($a_rule['match_degree']) ? (int)$a_rule['match_degree'] : 100;
						$s_match_value = $a_rule['match_value'];
						$s_operator = (isset($a_rule['operator']) && $i_match_degree == 100) ? $a_rule['operator'] : '=';

						// build a query to match against
						if ($i_match_degree == 100) {
							$a_where_vars[$s_colname] = $s_match_value;
							$a_wheres[] = '`'.$s_colname.'`'.$s_operator.'\'['.$s_colname.']\'';
						} else {
							$a_match_by_degree[] = array('col'=>$s_colname, 'val'=>$s_match_value, 'deg'=>$i_match_degree);
						}
						$a_selects[$s_colname] = $s_colname;
					}
					break;
				}
			}

			// create a query for the absolute rules
			$s_selectclause = $this->arrayToSelectClause($a_selects);
			$s_whereclause = 'WHERE '.implode(' AND ',$a_wheres);
			if ($s_whereclause == 'WHERE ') $s_whereclause = '';

			// get the rows that match absolute rules
			$a_matching_rows = $this->getAllInTable($s_tablename, $a_where_vars, TRUE,
				array('databasename'=>$s_database, 'selectclause'=>$s_selectclause, 'whereclause'=>$s_whereclause));

			// reduce it by the rows that don't match the match_degree rules
			$a_to_unset = array();
			foreach($a_match_by_degree as $a_rule) {
				foreach($a_matching_rows as $k=>$a_row) {
					$i_percent_matching = 0;
					similar_text($a_rule['val'], $a_row[$a_rule['col']], $i_percent_matching);
					if ($i_percent_matching < $a_rule['deg'])
						$a_to_unset[$k] = $k;
				}
			}
			foreach($a_to_unset as $k)
				unset($a_matching_rows[$k]);

			// return the restult
			return $a_matching_rows;
		}

	}
}
