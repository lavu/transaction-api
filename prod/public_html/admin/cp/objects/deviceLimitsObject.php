<?php

require_once(dirname(__FILE__)."/../resources/json.php");
require_once(dirname(__FILE__)."/../../sa_cp/billing/package_levels_object.php");

global $o_device_limits;
if (!isset($o_device_limits))
	$o_device_limits = new DeviceLimitsObject();
global $o_package_container;
if (!isset($o_package_container))
	$o_package_container = new package_container();

class DeviceLimitsObject {

	function __construct() {
		$this->dbapi = ConnectionHub::getConn('poslavu')->getDBAPI();
	}

	/**
	 * Use to determine if a dataname must limit ALL iPad connections, even from
	 *     administrator accounts or distributor accounts
	 * @param  string  $s_dataname The account dataname to check
	 * @return boolean             TRUE if a forced limit account, FALSE otherwise
	 */
	public function isForcediPadLimitAccount($s_dataname) {

		// load the device forced limits datanames, if not loaded yet
		if (!isset($this->a_forced_limit_datanames)) {
			$this->a_forced_limit_datanames = $this->loadForcedLimitsDatanames();
		}

		$b_forced = FALSE;
		if (isset($this->a_forced_limit_datanames[$s_dataname])) {
			$b_forced = $this->a_forced_limit_datanames[$s_dataname];
		}

		return $b_forced;
	}

	/**
	 * Saves the forced limit dataname into the settings config row
	 * @param  string  $s_dataname The dataname of the account
	 * @param  boolean $b_forced   TRUE or FALSE
	 * @return boolean             TRUE on success, FALSE on failure
	 */
	public function saveForcedLimitsDatanames($s_dataname, $b_forced) {

		// get the current settings
		$a_settings = $this->loadForcedLimitsDatanames();

		// add the setting
		if ($b_forced) {
			$a_settings[$s_dataname] = TRUE;
		} else {
			if (isset($a_settings[$s_dataname])) {
				unset($a_settings[$s_dataname]);
			}
		}

		// save the settings
		$s_content = LavuJson::json_encode($a_settings);
		$s_today = date("Y-m-d");
		$query = lavu_query("UPDATE `config` SET `value_long`='[content]',`value`='[today]' WHERE `setting`='forced limit datanames' AND `type`='lavu global settings'", array('content'=>$s_content, 'today'=>$s_today));
		if ($query === FALSE) {
			return FALSE;
		}

		// success!
		if (isset($this->a_forced_limit_datanames)) {
			unset($this->a_forced_limit_datanames);
		}
		return TRUE;
	}

	/**
	 * Sets the base iPad limit for the account. Also sets the custom iPad limit if that value is less than the base value.
	 * For a description of the base iPad limit, see self::getBaseiPadLimit()
	 * @param  integer $i_base_limit The base limit to be set
	 * @param  string $s_dataname    The dataname of the account
	 * @return array                 An array with the values
	 *     success (boolean), customiPadLimit (int), reason (one of "success", "base limit invalid", "dataname invalid")
	 */
	public function setBaseiPadLimit($i_base_limit, $s_dataname) {

		// get some initial values
		$i_base_limit = (int)$i_base_limit;
		$s_dataname = trim($s_dataname);
		$s_database = "poslavu_{$s_dataname}_db";

		// get the current ipad limit
		$a_package_info = $this->getCurrentPackageInfo($s_dataname);
		$i_custom_ipad_limit = (int)$a_package_info['custom_max_ipads'];

		// check the base limit value
		if ($i_base_limit <= 0)
			return array("success"=>FALSE, "customiPadLimit"=>$i_custom_ipad_limit, "reason"=>"base limit invalid");

		// set the base limit
		$a_where_vars = array("setting"=>"base_ipad_limit", "type"=>"config");
		$a_update_vars = array_merge($a_where_vars, array("value"=>$i_base_limit));
		$s_result = $this->dbapi->insertOrUpdate($s_database, "config", $a_update_vars, $a_where_vars);
		if ($s_result == "b")
			return array("success"=>FALSE, "customiPadLimit"=>$i_custom_ipad_limit, "reason"=>"dataname invalid");

		// set the custom iPad limit
		$i_new_custom_ipad_limit = $i_custom_ipad_limit;
		if ($i_custom_ipad_limit < $i_base_limit) {
			mlavu_query("UPDATE `poslavu_MAIN_db`.`payment_status` SET `custom_max_ipads`='[base_limit]' WHERE `dataname`='[dataname]'", array("base_limit"=>$i_base_limit, "dataname"=>$s_dataname));
			$i_new_custom_ipad_limit = $i_base_limit;
		}

		return array("success"=>TRUE, "customiPadLimit"=>$i_new_custom_ipad_limit, "reason"=>"success");
	}

	/**
	 * Get the base iPad limit for the account.
	 * The base iPad limit defines how many iPads are allowed before starting to charge for iPads.
	 * @param  integer $i_package_limit The iPad limit based on the package (eg 2 for "Gold", 3 for "Pro")
	 * @param  string  $s_dataname      The dataname of the account
	 * @return integer                  The base iPad limit
	 */
	public function getBaseiPadLimit($i_package_limit, $s_dataname) {

		// get some initial values
		$s_database = "poslavu_{$s_dataname}_db";
		$i_base_limit = (int)$i_package_limit;

		// load the base iPad limit from the database
		$base_limit_values = $this->dbapi->getAllInTable("config", array("setting"=>"base_ipad_limit", "type"=>"config"), TRUE, array("databasename"=>$s_database, "selectclause"=>"`value`", "limitclause"=>"LIMIT 1"));
		if (count($base_limit_values) > 0)
			$i_base_limit = (int)$base_limit_values[0]['value'];

		return $i_base_limit;
	}

	/**
	 * Sets the custom iPhone and/or iPad limit, depending on the values passed in.
	 * @param integer  $iPhone_limit Sets the value in `payment_status` if positive
	 * @param integer  $iPad_limit   Sets the value in `payment_status` if positive
	 * @param integer  $kds_limit    Sets the value in `payment_status` if positive
	 * @param string   $s_dataname   The dataname of the account to set these settings for
	 * @return boolean               TRUE on success, FALSE on failure
	 */
	public function setCustomiPhoneiPadKDSLimit($iPhone_limit, $iPad_limit, $kds_limit, $kiosk_limit, $s_dataname, $tablesideIpadLimit) {

		// get some values
		$iPhone_limit = (int)$iPhone_limit;
		$iPad_limit = (int)$iPad_limit;
		$kds_limit = (int)$kds_limit;
		$kiosk_limit = (int)$kiosk_limit;

		// get the update clause
		$a_update_vars = array();
		if ($iPhone_limit >= 0) $a_update_vars['custom_max_ipods'] = $iPhone_limit;
		if ($iPad_limit >= 0) $a_update_vars['custom_max_ipads'] = $iPad_limit;
		if ($kds_limit >= 0) $a_update_vars['custom_max_kds'] = $kds_limit;
		if ($kiosk_limit >= 0) $a_update_vars['custom_max_kiosk'] = $kiosk_limit;
		if (isset($tablesideIpadLimit) && $tablesideIpadLimit >= 0){
			$a_update_vars['tableside_max_ipads'] = $tablesideIpadLimit;
		} 
		$s_update_string = $this->dbapi->arrayToUpdateClause($a_update_vars);
		if ($s_update_string == "")
			return TRUE;

		// do the update
		$a_update_vars = array_merge($a_update_vars, array('dataname'=>$s_dataname));
		$query = mlavu_query("UPDATE `poslavu_MAIN_db`.`payment_status` {$s_update_string} WHERE `dataname`='[dataname]'", $a_update_vars);
		return ($query !== FALSE);
	}

	/**
	 * Gets the package info related to the iPod and iPad limits from the `payment_status` table
	 * @param  string $data_name     The dataname of the account
	 * @param  int    $restaurant_id The id of the account (optional)
	 * @return array                 An array with the values
	 *     current_package (string), custom_max_ipads (empty string on LLS), custom_max_ipods (empty string on LLS), annual_agreement
	 */
	public function getCurrentPackageInfo($data_name, $restaurant_id = 0) {

		global $lls;
		global $o_package_container;

		$package_info = array();
		$package_info['current_package'] = "3";
		$package_info['custom_max_ipads'] = "";
		$package_info['tableside_max_ipads'] = "";
		$package_info['custom_max_ipods'] = "";
		$package_info['annual_agreement'] = false;

		if ($lls)
			return $package_info;

		$query = mlavu_query("SELECT `signups`.`package` AS `package`, `payment_status`.`custom_max_ipads` AS `custom_max_ipads`, `payment_status`.`tableside_max_ipads`, `payment_status`.`custom_max_ipods` AS `custom_max_ipods`, `payment_status`.`annual_agreement` AS `annual_agreement` FROM `poslavu_MAIN_db`.`payment_status` LEFT JOIN `poslavu_MAIN_db`.`signups` ON `signups`.`dataname` = `payment_status`.`dataname` WHERE `payment_status`.`dataname` = '[1]'", $data_name);
		if ($query !== FALSE && mysqli_num_rows($query) > 0) {
			$info = mysqli_fetch_assoc($query);
			$package_info['custom_max_ipads'] = $info['custom_max_ipads'];
			$package_info['tableside_max_ipads'] = $info['tableside_max_ipads'];
			$package_info['custom_max_ipods'] = $info['custom_max_ipods'];
			$package_info['annual_agreement'] = $info['annual_agreement'];
			$set_package = "3";

			// get the package level
			if (is_numeric($info['package'])) {
				$set_package = $info['package'];
			} else {
				$set_package = $o_package_container->get_level_by_attribute('name', 'Platinum3'); //If none set, default to platinum
			}
			require_once(dirname(__FILE__)."/../../sa_cp/billing/payment_profile_functions.php");
			if ($restaurant_id == 0) {
				$a_restaurants = $this->dbapi->getAllInTable('restaurants', array('data_name'=>$data_name), TRUE, array("selectclause"=>"`id`", "limitclause"=>"LIMIT 1"));
				if (count($a_restaurants) > 0)
					$restaurant_id = (int)$a_restaurants[0]['id'];
			}
			if ($restaurant_id > 0) {
				$a_package_status = find_package_status($restaurant_id);
				$set_package = $a_package_status['package'];
			}
			if($set_package == 'none' ){
				$set_package = $o_package_container->get_level_by_attribute('name', 'Platinum3'); //If none set, default to platinum (should only apply to reseller trial accounts)
			}
			$package_info['current_package'] = $set_package;
		}
		return $package_info;
	}

	/**
	 * Gets the package info related to the iPod and iPad limits from the `payment_status` table
	 * and compare it to the package values, returning the custom value if it is set or the package
	 * value if the custom value is not set.
	 * @param  string $data_name The dataname of the account
	 * @return array             An array with the values
	 *     current_package (int), custom_max_ipads (empty string on LLS), custom_max_ipods (empty string on LLS),
	 *     pName ("Lavu " + the printed package name), iPod_max (int), iPad_max (int)
	 */
	public function getDeviceLimitsByDataname($s_dataname) {

		global $o_package_container;

		$package_info = $this->getCurrentPackageInfo($s_dataname);

		$pName = $o_package_container->get_printed_name_by_attribute('level', $package_info['current_package']);
		if (strpos($pName, 'Lavu') === false) $pName = "Lavu {$pName}";
		$iPod_max = max(1, $o_package_container->get_ipod_limt_by_attribute('level', $package_info['current_package']));
		$iPad_max = max(1, $o_package_container->get_ipad_limt_by_attribute('level', $package_info['current_package']));
		$tablesideIpadMax = max(1, $o_package_container->getTablesideIpadLimtByAttribute('level', $package_info['current_package']));

		if ($iPad_max!=$package_info['custom_max_ipads'] && !empty($package_info['custom_max_ipads'])) $iPad_max = $package_info['custom_max_ipads'];
		if ($iPod_max!=$package_info['custom_max_ipods'] && !empty($package_info['custom_max_ipods'])) $iPod_max = $package_info['custom_max_ipods'];
		if ($tablesideIpadMax != $package_info['tableside_max_ipads'] && !empty($package_info['tableside_max_ipads'])){
			$tablesideIpadMax = $package_info['tableside_max_ipads'];
		}

		$package_info['pName'] = $pName;
		$package_info['iPod_max'] = $iPod_max;
		$package_info['iPad_max'] = $iPad_max;
		$package_info['tableside_max_ipads'] = $tablesideIpadMax;

		return $package_info;
	}

	/**
	 * Gets the cost to upgrade by one more iPad
	 * @param  array $package_info          The package info related to the iPod and iPad limits from the `payment_status` table
	 * @return float                        The cost for a single additional iPad
	 */
	public function getCostPeriPad($package_info) {
		$per_ipad_cost = 20.00;
		$package = $package_info['current_package'];
		$annual_agreement = $package_info['annual_agreement'];

		if ($package == '25' && !$annual_agreement) {
			$per_ipad_cost = 69.00;
		}
		else if ($package == '25' && $annual_agreement) {
			$per_ipad_cost = 59.00;
		}

		return $per_ipad_cost;
	}

	/**
	 * loads the forced limit datanames from the settings file
	 * @return array An array of datanames and their forced status
	 */
	private function loadForcedLimitsDatanames() {

		// get the row
		$query = lavu_query("SELECT `value_long`,`value` FROM `config` WHERE `setting`='forced limit datanames' AND `type`='lavu global settings'");
		if ($query === FALSE) {
			return array();
		}
		if (mysqli_num_rows($query) == 0) {
			lavu_query("INSERT INTO `config` (`setting`,`type`) VALUES ('forced limit datanames','lavu global settings')");
			return array();
		}

		// get the settings
		$read = mysqli_fetch_assoc($query);
		if (strtotime($read['value']) < strtotime('today')) {
			$s_today = date("Y-m-d");
			lavu_query("UPDATE `config` SET `value_long`='',`value`='[today]' WHERE `setting`='forced limit datanames' AND `type`='lavu global settings'", array('today'=>$s_today));
		}
		$s_contents = $read['value_long'];

		// return the settings
		if ($s_contents == "") {
			return array();
		}
		$a_retval = (array)LavuJson::json_decode($s_contents);
		return $a_retval;
	}
}

?>
