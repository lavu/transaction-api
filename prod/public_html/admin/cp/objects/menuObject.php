<?php
class linkObj{

	private $link='';
	private $image='';
	private $title='';
	private $parentVal='';
	private $children=array();
	private $modulesList=array();
	private $submode='';
	public  $isDisabled=false;
	private $record_clicks= true; 		// This is for a statistical analysis of how many people use the reports and what reports they look at
	function __construct($link = null, $title = null, $image = null, $children = null, $modulesList = null, $submode = null){

		if(is_array($children))
			foreach($children as $child){
				$child->setParent($link);
			}

		$this->setChildren($children);
		$this->setTitle($title);
		$this->setLink($link);
		$this->setImage($image);
		$this->setModules($modulesList);
		$this->setSubmode($submode);

	}

	public function disable(){
		$this->isDisabled = true;
	}
	public function getSubmode() {
		return $this->submode;
	}
	public function setSubmode($submode) {
		if ($submode === null) $submode = '';
		$this->submode = $submode;
	}
	public function getLink(){
		return $this->link;
	}
	private function setLink($linkIn){
		$this->link= $linkIn;
	}
	public function getTitle(){
		return $this->title;
	}
	private function setTitle($title){
		$this->title= $title;
	}
	private function setChildren($array){

			$this->children= $array;
	}
	private function setParent($parent){
		$this->parentVal= $parent;
	}
	private function setModules($modulesList) {
		$delimeters = array('|', ',');
		if (is_array($modulesList)) {
			$this->modulesList = $modulesList;
		} else if (is_string($modulesList)){
			foreach($delimeters as $delimeter) {
				if (count(explode($delimeter, $modulesList)) > 0) {
					$modules = explode($delimeter, $modulesList);
					$this->modulesList = $modules;
					break;
				}
			}
		}
	}

	public function getChildren(){
		return $this->children;
	}
	public function getParent(){
		return $this->parentVal;
	}
	private function setImage($image){
		$this->image= $image;
	}
	public function getImage(){
		return $this->image;
	}
	private function getChild($childName){
		if( is_array($this->getChildren()) && $childName){

			foreach($this->getChildren() as $child){
				if($child->getLink() == $childName){

					return $child->getChildren();

				}else if(count($child->getChildren()) && $child->getChildren())
					$this->getChild($child->getLink());
			}
		}

		return false;
	}
	// find the child with link name $childName and return the object
	public function findNestedChild($childName){
		if( is_array($this->getChildren()) && $childName){
			foreach($this->getChildren() as $child){
				if($child->getLink() == $childName){
					return $child;
				}else if(count($child->getChildren()) && $child->getChildren()){
					if ($child->findNestedChild($childName) !== FALSE)
						return $child->findNestedChild($childName);
				}
			}
		}

		return false;
	}
	// depends on the global $modules variable
	public function hasRequiredModules() {
		//return TRUE;
		global $modules;
		if (is_array($this->modulesList)) {
			foreach ($this->modulesList as $module) {
				if (!$modules->hasModule($module)) {
					return FALSE;
				}
			}
		}
		return TRUE;
	}

	public function addChild(linkObj $child, $parent){
		//echo $parent. " ";
		if($child){
			if(!is_array($this->children))
				$this->children=array();

			array_push($this->children, $child);
			$child->setParent($parent);
		}
		else
			return false;
	}

	public function __toString(){
		return $this->buildList();
	}
	public function __toString2(){
		return $this->buildList_mode2();
	}
	private function buildList(){

		$returnString='';

		if( $this->link ){
			if( $this->image){
				if( $_GET['parent']==$this->link)
					$returnString.="<li><div class='selectedLink'><a class='navLink' href='?mode=".$this->link."'><img src='".$this->image."' style='border:0px;' ><br/>".speak($this->title)."</a></div>";
				else
					$returnString.="<li><a class='navLink' href='?mode=".$this->link."'><img src='".$this->image."' style='border:0px;' ><br/>".speak($this->title)."</a>";
			}
			else
				$returnString.="<li><a class='navLink' href='?mode=".$this->link."'><br/>".speak($this->title)."</a>";

		}
		$obj = new DBConnector();
		$memc_conn = $obj->getLink('readreplica');
		if ($this->link == "reports_reports_v2" && $memc_conn == "") {
			return $returnString;
		}
		// add children as long as they have the modules
		$children = array();
		if(is_array($this->children) && count($this->children)){
			foreach($this->children as $child) {

				//error_log("child: ".print_r($child, true));

				if ($child->hasRequiredModules()) {
					$children[] = $child;
				}
			}
		}
		// return the string representing these children
		if(count($children)){
			//if($this->getParent()){

			//	$returnString .="<ul id='".$this->getParent()."'> ";
			if($this->link){
				//echo $this->getParent() ." ";
				$returnString .="<ul id='".$this->link."'> ";
			}else
				$returnString .="<ul></li> ";

			foreach($children as $child){
				$returnString.=$child->buildList();
			}
			$returnString .="</ul>";
		}
		if ($this->link == 'shop.lavu.com') {
			$returnString = str_replace("href='?mode=".$this->link."'", "href='https://shop.lavu.com/?utm_source=Control_Panel&utm_campaign=Shop_Lavu&utm_medium=Navbar_Link' target='_blank'", $returnString);
		}
		
		// String replace for NRA Inventory demo
		if ($this->link == 'inventory') {
			$configStatus = sessvar("INVENTORY_MIGRATION_STATUS");
			if($configStatus === "Legacy" || $configStatus === "" ){
                $returnString = str_replace("href='?mode=".$this->link."'", "href='?mode=inventory_edit_ingredients'", $returnString);
			}
			else{
                $returnString = str_replace("href='?mode=".$this->link."'", "href='areas/inventory/dashboard'", $returnString);
			}
		}
				// String replace for ABI Dashboard
		if ($this->link == 'dashboard') {
			$returnString = str_replace("href='?mode=".$this->link."'", "href='areas/abi-dashboard'", $returnString);
		}
		return $returnString;
	}
	private function buildList_mode2($depth=0){

		$returnString='';

		if( $this->link ){
			if( $this->image){
				if( str_replace("_newdev","",$_GET['parent'])==str_replace("_newdev","",$this->link))
					$returnString.="<li><div class='selectedLink'><a class='navLink' href='?mode=".$this->link."'><img src='".$this->image."' style='border:0px;' ><br/>".speak($this->title)."</a></div>";
				else
					$returnString.="<li><a class='navLink' href='?mode=".$this->link."'><img src='".$this->image."' style='border:0px;' ><br/>".speak($this->title)."</a>";
			}
			else
				$returnString.="<li><a class='navLink' href='?mode=".$this->link."'><br/>".speak($this->title)."</a>";

		}
		// add children as long as they have the modules
		$children = array();
		if($depth < 1)
		{
			if(is_array($this->children) && count($this->children)){
				foreach($this->children as $child) {
	
					//error_log("child: ".print_r($child, true));
	
					if ($child->hasRequiredModules()) {
						$children[] = $child;
					}
				}
			}
		}
		// return the string representing these children
		if(count($children)){
			//if($this->getParent()){

			//	$returnString .="<ul id='".$this->getParent()."'> ";
			if($this->link){
				//echo $this->getParent() ." ";
				$returnString .="<ul id='".$this->link."'> ";
			}else
				$returnString .="<ul></li> ";

			foreach($children as $child){
				$returnString.=$child->buildList_mode2($depth+1);
			}
			$returnString .="</ul>";
		}
		if ($this->link == 'shop.lavu.com') {
			$returnString = str_replace("href='?mode=".$this->link."'", "href='https://shop.lavu.com/?utm_source=Control_Panel&utm_campaign=Shop_Lavu&utm_medium=Navbar_Link' target='_blank'", $returnString);
		}
		// String replace for NRA Inventory demo
		if ($this->link == 'inventory') {
			$configStatus = sessvar("INVENTORY_MIGRATION_STATUS");
            if($configStatus === "Legacy" || $configStatus === "" ){
                $returnString = str_replace("href='?mode=".$this->link."'", "href='?mode=inventory_edit_ingredients'", $returnString);
            }
            else{
                $returnString = str_replace("href='?mode=".$this->link."'", "href='areas/inventory/dashboard'", $returnString);
            }
		}

		// String replace for ABI Dashboard
		if ($this->link == 'dashboard') {
			$returnString = str_replace("href='?mode=".$this->link."'", "href='areas/abi-dashboard'", $returnString);
		}
		return $returnString;
	}
	public function makeInternalMenu($childName, $headerClickable=true, $numRows=6){
		$children= $this->getChild($childName);
		if( $childName == $this->getLink() ){
			$children = $this->getChildren();
		}
		$returnString='';

		$currentLink = $this->findNestedChild( $_GET['mode'] );
		if( !$currentLink ){
			$currentLink = $this;
		}
		// echo 'parent?: <pre style="text-align: left;">' . print_r( $currentLink->getParent(), true ) . '</pre>';

		if(count($children)>1 && $childName){
			$newChildren = array();
			foreach($children as $child) {
				if ($child->hasRequiredModules()) {
					$newChildren[] = $child;
				}
			}
			$children = $newChildren;
		}		

		if(count($children)>1 && $childName){

			$returnString="<table class='internalMenu'><tr>";
			$counter=1;


			foreach($children as $child){
				$isActiveMenu = '';
				if($headerClickable){

					$b_submode_matches = (isset($_GET[$child->submode]) && $_GET['mode'].'&'.$child->submode.'='.$_GET[$child->submode] == $child->link);
					$isActiveMenu = stristr($_GET['mode'],$child->title) || $_GET['mode']==$child->link || $b_submode_matches;
					if($isActiveMenu)
						$returnString.="<td class='internalMenuItemSelected'><a class='internalMenuItemLinkSelected' href='?mode=".$child->link."'> ".speak($child->title)."</a> ";
					else
						$returnString.="<td class='internalMenuItem'><a class='internalMenuItemLink' href='?mode=".$child->link."'> ".speak($child->title)."</a> ";
				}else
					$returnString.="<td class='reportMenu'><div class='menuHeader'> ".speak($child->title)."</div>";

				if( $child->getChildren()){
					$returnString.="<table>";

					foreach($child->getChildren() as $childchild){
						$href = $childchild->isDisabled ? "href='#'" : "href='?mode=".$childchild->link."'";
						$class = $childchild->isDisabled ? 'subInternalMenuItemLinkDisabled' : 'subInternalMenuItemLink';
						$disabled = $childchild->isDisabled ? "disabled" : "";
						$record_clicks = '';
						if($this->record_clicks && stristr($_GET['mode'],'report'))
							$record_clicks= "onclick= record_click('".str_replace(" ", "", $childchild->title)."','".admin_info('dataname')."')";
						if ($childchild->hasRequiredModules()){

							$returnString.="<tr><td class='subInternalMenuItem'>";
							if($_GET['mode']== $childchild->link)
								$returnString.="<a class='subInternalMenuItemLinkSelected' $record_clicks $href> ".speak($childchild->title)."</a> </td></tr>";
							else
								$returnString.="<a class='$class' $record_clicks $href> ".speak($childchild->title)."</a> </td></tr>";
						}

						if( ($isActiveMenu || $_GET['mode'] == $childchild->link || $childchild->link == $currentLink->getParent() ) && $childchild->getChildren() ){ // skip bottom level?
							// echo '<pre style="text-align: left;">Child: ' . print_r( $child, true ) . '</pre>';
							// echo '<pre style="text-align: left;">ChildChild: ' . print_r( $childchild, true ) . '</pre>';
							return $child->makeInternalMenu( $child->getLink(), $headerClickable, $numRows );
						}
					}
					$returnString.="</table>";
				}
				$returnString.="</td>";
				if(($counter % $numRows) == 0)
					$returnString.="</tr><tr>";

				$counter++;
			}
			$returnString.='</table>';
		}
		return $returnString;
	}

	public function getParentOf($childName){

		if( $this->getLink() == $childName){
			//echo "foundParent: ". $this->getParent();
			return $this->getParent();
		}
		else if($this->getChildren()){
			foreach($this->getChildren() as $child){
				$temp= $child->getParentOf($childName);
				if( $temp)
					return $temp;
			}
		}
		else
			return false;
	}
	public function getRootParentOf($childName){

		$parent= $this->getParentOf($childName);
		if($parent){

			return $this->getRootParentOf($parent);
		}else
			return $childName;


	}

	// returns html for a list of tabs
	// @$s_mode: the mode to make the tablist for
	// @$a_tabs: an array(array('name'=>string, 'submode_name'=>string, 'submode_value'=>string), ...)
	// @return: array('success'=>TRUE/FALSE, 'value'=>raw html)
	public static function tabsTostr($s_mode, $a_tabs) {

		// check the input
		if ($s_mode == '')
			return array('success'=>FALSE, 'value'=>'mode must not be empty');
		if (count($a_tabs) < 2)
			return array('success'=>FALSE, 'value'=>'must have more than one tab');

		// create the tabs
		$a_menu_tabs = array();
		foreach($a_tabs as $a_tab) {
			if (count($a_tab) < 3) {
				return array('success'=>FALSE, 'value'=>'tabs incomplete');
			}
			$a_menu_tabs[] = new linkObj($s_mode.'&'.$a_tab['submode_name'].'='.$a_tab['submode_value'], $a_tab['name'], NULL, NULL, NULL, $a_tab['submode_name']);
		}
		$a_children = array(
			new linkObj($s_mode, '', NULL, $a_menu_tabs)
		);
		$o_menu = new linkObj(NULL, NULL, NULL, $a_children);
		$s_menu = $o_menu->makeInternalMenu($s_mode, 'true', 7);

		return array('success'=>TRUE, 'value'=>$s_menu);
	}
}
?>
