<?php
  $mobileBrowsers = [
    [
      'label' => 'Google Chrome',
      'icon' => 'images/browserIcons/chrome.svg',
    ],
    [
      'label' => 'Apple Safari',
      'icon' => 'images/browserIcons/safari-ios.svg',
    ],
    [
      'label' => 'Mozilla Firefox',
      'icon' => 'images/browserIcons/firefox.svg',
    ],
    [
      'label' => 'Microsoft Edge',
      'icon' => 'images/browserIcons/edge.svg',
    ],
  ];

  $desktopBrowsers = [
    [
      'label' => 'Google Chrome',
      'icon' => 'images/browserIcons/chrome.svg',
      'download' => getenv('CHROME_DOWNLOAD_URL'),
    ],
    [
      'label' => 'Apple Safari',
      'icon' => 'images/browserIcons/safari.svg',
      'download' => getenv('SAFARI_DOWNLOAD_URL'),
    ],
    [
      'label' => 'Mozilla Firefox',
      'icon' => 'images/browserIcons/firefox.svg',
      'download' => getenv('FIREFOX_DOWNLOAD_URL'),
    ],
    [
      'label' => 'Microsoft Edge',
      'icon' => 'images/browserIcons/edge.svg',
      'download' => getenv('EDGE_DOWNLOAD_URL'),
    ],
  ];

  $locales = [
    'en' => [
      'browserNotSupported' => 'Looks like your browser is not supported.',
      'installOtherBrowser' => 'Please upgrade to any of these options:',
      'installNow' => 'Install now',
      'goBackToCP' => 'Go back to Control Panel',
    ],
    'es' => [
      'browserNotSupported' => 'Parece que tu navegador no está soportado.',
      'installOtherBrowser' => 'Por favor instala alguna de estas opciones:',
      'installNow' => 'Instalar ahora',
      'goBackToCP' => 'Volver al Panel de Control',
    ],
  ];

  $lang = $_COOKIE['CP4_LOCALE'];
  $values = $lang ? $locales[$lang] : $locales['en'];
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN""http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <link rel="stylesheet" href="https://unpkg.com/semantic-ui-css@2.4.1/semantic.min.css">
  <link rel="stylesheet" href="https://unpkg.com/normalize.css@8.0.1/normalize.css">
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,400i,600,600i,700" rel="stylesheet">
  <style>
    * {
      box-sizing: border-box;
    }

    body {
      font-size: 1.6rem;
      margin: 0;
      font-family: 'Open Sans', sans-serif;
      color: #23163f
    }

    img {
      border-style: none;
    }

    .pageWrapper { 
      height: 100%;
      background-color: #E3E3E3;
      padding: 30px;
    }

    .logoWrapper {
      position: absolute;
      top: 30px;
      left: 50px;
    }

    .logoLavu {
      margin-top: -6px;
      width: 92px;
      height: 32px;
    }

    .wrapper {
      display: -webkit-box;
      display: -webkit-flex;
      display: -ms-flexbox;
      display: flex;
      -webkit-flex: 1;
      -ms-flex: 1;
      flex: 1;
      -webkit-align-items: center;
      -webkit-box-align: center;
      -ms-flex-align: center;
      align-items: center;
      -webkit-box-pack: center;
      -webkit-justify-content: center;
      -ms-flex-pack: center;
      justify-content: center;
      height: 100vh;
    }

    .illustration {
      width: 400px;
    }

    .textWrapper {
      margin-left: 50px;
      display: -webkit-box;
      display: -webkit-flex;
      display: -ms-flexbox;
      display: flex;
      -webkit-flex-direction: column;
      -ms-flex-direction: column;
      flex-direction: column;
      position: relative;
    }

    .titleText {
      font-size: 20px;
      color: #23163f;
      display: inline-block;
      min-width: 0px;
      text-align: inherit;
      font-weight: 600;
    }

    .browserItem {
      margin-top: 18px;
      display: -webkit-box;
      display: -webkit-flex;
      display: -ms-flexbox;
      display: flex;
      -webkit-align-items: center;
      -webkit-box-align: center;
      -ms-flex-align: center;
      align-items: center;
    }

    .browserIcon {
      width: 30px;
      margin-right: 18px;
    }

    .browserName {
      font-size: 14px;
      font-weight: 100;
      color: #23163F;
      display: inline-block;
      min-width: 0px;
      text-align: inherit;
    }

    .browserDownload {
      font-size: 14px;
      margin-left: 18px;
      -webkit-text-decoration: none;
      text-decoration: none;
      color: #3ca5d9;
      background-color: transparent;
      -webkit-text-decoration-skip: objects;
    }

    .goBackItem {
      position: absolute;
      bottom: -45px;
      display: -webkit-box;
      display: -webkit-flex;
      display: -ms-flexbox;
      display: flex;
      -webkit-text-decoration: none;
      text-decoration: none;
      color: #3ca5d9;
      background-color: transparent;
      -webkit-text-decoration-skip: objects;
    }

    .goBackLink {
      font-size: 14px;
      margin-left: 15px;
    }

    .goBackArrow {
      width: 16px;
      height: 31px;
    }
  </style>
  <title>POSLavu Admin Control Panel</title>
</head>
<body>
  <div class="pageWrapper">
    <div class="logoWrapper">
      <img src="images/lavu_logo_dark.svg" alt="Lavu logo" class="logoLavu">
    </div>
    <div class="wrapper">
      <img src="images/unsupported_illustration.svg" alt="Unsupported Browser" class="illustration">
      <div class="textWrapper">
        <span class="titleText">
          <?php echo $values['browserNotSupported'] ?>
          <br>
          <?php echo $values['installOtherBrowser'] ?>
        </span>
        <br>
        <?php
          $browsers = isMobileBrowser($browserInfo) ? $mobileBrowsers : $desktopBrowsers;

          foreach ($browsers as $browser) {
            $link = $browser['download'] ? "<a target='_blank' href='{$browser['download']}' class='browserDownload'>{$values['installNow']}</a>" : "";

            echo "
              <div class='browserItem'>
                <img src='{$browser['icon']}' alt='{$browser['label']}' class='browserIcon'>
                <span class='browserName'>{$browser['label']}</span>
                {$link}
              </div>
            ";
          }
        ?>
        <a href="/cp/index.php" class="goBackItem">
          <img src="images/browserIcons/backIcon.svg" alt="Go Back" class="goBackArrow">
          <span class="goBackLink">
            <?php echo $values['goBackToCP'] ?>
          </span>
        </a>
      </div>
    </div>
  </div>
</body>
</html>
