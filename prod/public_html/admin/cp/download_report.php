<?php
require_once ("/home/poslavu/public_html/admin/cp/resources/core_functions.php");
require_once ("/home/poslavu/public_html/admin/cp/reports_v2/emailReports.php");
class download_report extends emailReports {
    private static $_ERROR_INVALID = 'invalid';
    private static $_ERROR_TOKEN_EXPIRY = 'tokenExpiry';
    private static $_ERROR_FAILED = 'requestFailed';
    private static $_ERROR_FILE_ERROR = 'fileHasError';
    private static $_ERRORS = [
        'invalid' => 'Sorry, invalid file request!',
        'tokenExpiry' => 'Sorry, the requested file token either expired or is incorrect.',
        'fileHasError' => 'Sorry, the requested file failed due to error please create a new request and try again, if the problem still persist contact our support team..',
        'requestFailed' => 'Sorry, something went wrong please try reloading, if the problem still persist contact our support team.',
    ];
    public $hasError = false;
    public $errorMessage = '';
    function __construct($token) {
        if ($token === false || empty($token)) {
            $this->fileDownloadError(self::$_ERROR_INVALID);
        } else {
            parent::__construct($token);
            if (empty($this->id)) {
                $this->fileDownloadError(self::$_ERROR_INVALID);
            } else if ($this->status === 'error') {
                $this->fileDownloadError(self::$_ERROR_FILE_ERROR);
            }
        }
    }

    private function fileDownloadError($error) {
        $this->hasError = true;
        $this->errorMessage = self::$_ERRORS[$error];
    }

    private function getFileData() {
        if (!$this->hasError) {
            $query = 'SELECT `file` FROM [0] WHERE `token`="[1]" AND `process_id`="[2]" LIMIT 1';
            $result = mlavu_query($query, [$this->getTableName(), $this->token, $this->id]);
            $data = $result !== false ? mysqli_fetch_assoc($result) : [];
            if ($data !== false && isset($data['file']) && !empty($data['file'])) {
                return $data['file'];
            }
            $this->fileDownloadError(self::$_ERROR_TOKEN_EXPIRY);
        }
        return false;
    }

    public function download() {
        $data = $this->getFileData();
        $senddates = $this->getRecordByToken($this->token);
        $mdates = json_decode($senddates[queries], 1);
        $emaildates = substr($mdates[dates], 0, 10)."-".substr($mdates[dates], 20, 10);
        $file_name= $this->dataname."-".$this->getReportName()."-".$emaildates;
        if (!$this->hasError) {
            header("Content-type: application/octet-stream");
            header("Content-Disposition: attachment; filename=\"".$file_name.".csv\"");
            echo $data;
        }
    }

}
$token = isset($_GET['token']) ? trim($_GET['token']) : false;
$downloadObject = new download_report($token);
$downloadObject->download();
if ($downloadObject->hasError) {
    echo $downloadObject->errorMessage;
}