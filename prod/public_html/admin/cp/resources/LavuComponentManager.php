<?php

require_once( dirname(__FILE__) ."/lavuquery.php" );

/**
*  <product /> = component_packages
*  <tab />  = components_layouts
*  <area /> = components
*/

class LavuComponentManager
{
	private $xml;
	private $xmlObj;
	private $xmlArray;
	private $generatedXml;	
	private $namespace;
	private $componentIds;
	private $componentLayoutIds;
	private $origComponentPackageId;
	private $componentPackageIsEnabled;

	// const-like vars
	private $filenameRedirScript;
	private $packageCode;

	function __construct($namespace = 'default')
	{
		$this->namespace = $namespace;
		$this->_initialize();
	}

	private function _initialize()
	{
		$this->componentPackageIsEnabled = false;
		$this->componentIds = array();  // Really a 2 dimensional array: 1st key level is the component_layout ID and then that contains an array of the component IDs, in order

		$this->filenameRedirScript = 'component_manager.php?r=';
		$this->packageCode = 'component_manager';
	}

	public function processXml($xml = '')
	{
		$this->parseXml($xml);  // TO DO : this should throw an error
		$this->storeParsedXmlObj();
	}

	public function parseXml($xml = '')
	{
		if ( empty($xml) ) {
			die( __METHOD__ .' called with empty XML string!' );
		}

		$this->xml = $xml;
		$this->xmlObj = simplexml_load_string($this->xml);

		// We use this trick of JSON encoding the XML Obj and then JSON decoding it to easily produce an object with arrays, since that's a little easier to work with.
		$json = json_encode($this->xmlObj);
		$this->xmlArray = json_decode($json, true);

		//echo '<pre>'. print_r($this->xmlArray, true) .'</pre>';  //debug
	}

	private function parseDatabase()
	{
		// Start with the component_packages table and get get layout_ids then populate some XML.

		$sql = "select * from `poslavu_MAIN_db`.`component_packages` where title = '[title]'";
		$query = mlavu_query($sql, array('title' => $this->namespace) );
		if ($query === FALSE) die(__METHOD__ . " got this error ". mlavu_dberror() ." running this query {$sql}");

		$product_title = '';
		$product_notes = '';

		// If this is the first time they're using the Component Manager and they don't have a component_package row created for their dataname yet
		// then at least populate the POS tab for them, since everyone will most likely have that.
		// TO DO : check to see if they have a value for `locations`.`component_package` first before assuming they see the POS tab (since they might have Lavu Fit or Lavu Tithe)
		if ( mysqli_num_rows($query) === 0 ) {
			$this->componentLayoutIds = array('0');
		}
		// This branch means they already have a components_package row so just load its values from the database.
		else {
			$componentPackage = mysqli_fetch_assoc($query);
			$this->componentLayoutIds = ($componentPackage['layout_ids'] == '') ? array() : explode( ',', $componentPackage['layout_ids'] );
			$product_title = (empty($componentPackage['title'])) ? '' : $componentPackage['title'];
			$product_notes = (empty($componentPackage['notes'])) ? '' : $componentPackage['notes'];
		}

		$this->xml = <<<XML
<product>
	<title>{$product_title}</title>
	<notes>{$product_notes}</notes>
	<tabs>
XML;

		foreach ( $this->componentLayoutIds as $componentLayoutId ) {
			// Skip the POS tab since its component settings are built-in.
			if (intval($componentLayoutId) === 0) {
				$this->xml .= <<<XML

		<tab>
			<title>P.O.S.</title>
			<type>pos</type>
			<id>0</id>
		</tab>
XML;
				continue;
			}

			// Component Layout settings from the database.
			$sql = "select * from `poslavu_MAIN_db`.`component_layouts` where id = '[id]'";
			$query = mlavu_query($sql, array('id' => $componentLayoutId) );
			if ($query === FALSE) die(__METHOD__ . " got this error ". mlavu_dberror() ." running this query {$sql}");
			$componentLayout = mysqli_fetch_assoc($query);

			// XML values.
			$this->componentIds[$componentLayoutId] = explode( ',', $componentLayout['c_ids'] );
			$unique_name = str_replace( "{$this->namespace}:", '', $componentLayout['unique_name'] );
			$title = empty($componentLayout['title']) ? '' : $componentLayout['title'];
			$background = empty($componentLayout['background']) ? '' : str_replace($this->filenameRedirScript, '', $componentLayout['background']);
			$special_type = empty($componentLayout['special_type']) ? '' : $componentLayout['special_type'];
			$modules_required = empty($componentLayout['modules_required']) ? '' : $componentLayout['modules_required'];
			$notes = empty($componentLayout['notes']) ? '' : $componentLayout['notes'];

			$this->xml .= <<<XML

		<tab>
			<name>{$unique_name}</name>
			<title>{$title}</title>
			<type>webview</type>
			<background>{$background}</background>
			<special_type>{$special_type}</special_type>
			<modules_required>{$modules_required}</modules_required>
			<id>{$componentLayoutId}</id>
			<notes>{$notes}</notes>
			<areas>
XML;

			// Expand the comma-delimited settings for each Component, so we expand them into an array first so we can then use them as we iterate over each Component.
			$c_xs = explode(',', $componentLayout['c_xs']);
			$c_ys = explode(',', $componentLayout['c_ys']);
			$c_ws = explode(',', $componentLayout['c_ws']);
			$c_hs = explode(',', $componentLayout['c_hs']);
			$c_zs = explode(',', $componentLayout['c_zs']);
			$c_types = explode(',', $componentLayout['c_types']);

			$i = 0;
			foreach ( $this->componentIds[$componentLayoutId] as $componentId ) {

				// Component settings from the database.
				$sql = "select * from `poslavu_MAIN_db`.`components` where id = '[id]'";
				$query = mlavu_query($sql, array('id' => $componentId) );
				if ($query === FALSE) die(__METHOD__ . " got this error ". mlavu_dberror() ." running this query {$sql}");
				$component = mysqli_fetch_assoc($query);

				// XML values.
				$name = str_replace("{$this->namespace}:", '', $component['name']);
				$title = $component['title'];
				$filename = str_replace($this->filenameRedirScript, '', $component['filename']);
				$allow_scroll = $component['allow_scroll'];
				$c_x = empty($c_xs[$i]) ? '0' : $c_xs[$i];
				$c_y = empty($c_ys[$i]) ? '0' : $c_ys[$i];
				$c_w = empty($c_ws[$i]) ? '1024' : $c_ws[$i];
				$c_h = empty($c_hs[$i]) ? '620' : $c_hs[$i];
				$c_z = empty($c_zs[$i]) ? '1' : $c_zs[$i];
				$c_type = empty($c_types[$i]) ? 'v' : $c_types[$i];

				$this->xml .= <<<XML

				<area>
					<name>{$name}</name>
					<title>{$title}</title>
					<id>{$componentId}</id>
					<src>{$filename}</src>
					<scrollable>{$allow_scroll}</scrollable>
					<coord_x>{$c_x}</coord_x>
					<coord_y>{$c_y}</coord_y>
					<width>{$c_w}</width>
					<height>{$c_h}</height>
					<z-index>{$c_z}</z-index>
					<type>{$c_type}</type>
				</area>
XML;

				// Remove empty internal|optional tags.  NOTE : this depends on the indent levels
				$this->xml = str_replace( array("\t<title>{$this->namespace}</title>\n", "\t\t\t\t\t<id></id>\n", "\t\t\t<special_type></special_type>\n", "\t\t\t<modules_required></modules_required>\n"), '', $this->xml );

				$i++;
			}

		$this->xml .= <<<XML

			</areas>
		</tab>
XML;

		}

		$this->xml .= <<<XML

	</tabs>
</product>

XML;

	}

	public function getXmlFromDatabase()
	{
		$this->parseDatabase();

		return $this->xml;
	}

	public function getStructure()
	{
		return print_r($this->xmlArray, true);
	}

	public function storeParsedXmlObj()
	{
		if ( !isset($this->xmlObj) ) {
			die( __METHOD__ .' called with unset XML object!' );
		}

		//error_log("DEBUG: about to call storeComponents()");  //debug
		$this->storeComponents();
		$this->storeComponentsLayouts();
		$this->storeComponentsPackage();
	}

	private function storeComponents()
	{
		$sql = '';
		$compCounter = 1;

		//error_log("DEBUG: xmlArray:". print_r($this->xmlArray, true) );  //debug

		foreach ( $this->getArrayFromCollectionElement('tabs', $this->xmlArray) as $tab ) {

			// Skip the POS tab since its component settings are build-in.
			if ( isset($tab['type']) && $tab['type'] == 'pos' ) {
				continue;
			}

			$componentLayoutId = isset($tab['id']) ? $tab['id'] : '';

			//error_log("DEBUG: area: ". print_r($this->getArrayFromCollectionElement('areas', $tab), true) );  //debug

			$areas = $this->getArrayFromCollectionElement('areas', $tab);
			///if ( empty($areas) ) $areas[] = array('id' => '', 'name' => '', 'title' => '');

			foreach ( $areas as $area ) {
				// If none of the area fields were filled out, then don't proceed with trying to insert/update stuff.
				if ( empty($area['name']) && empty($area['title']) && empty($area['src']) && empty($area['scrollable']) ) {
					continue;
				}

				//error_log("DEBUG: iterating over area: ". print_r($area, true));  //debug

				$id = 0;
				$isUpdate = false;
				if ( !empty($area['id']) ) {
					// TO DO : make sure namespace values match before accepting that ID
					$id = $area['id'];
					$isUpdate = true;
					$this->componentIds[$componentLayoutId][] = $id;
				}

				// Prepare fields to be used in sql column values. 
				$name = empty($area['name']) ? str_replace( ' ', '_', strtolower($area['title']) ) : $area['name'];
				if ( empty($name) && empty($area['title']) )  $name = 'component'. $compCounter++;
				$title = empty($area['title']) ? ucwords(str_replace('_', ' ', $name)) : $area['title'];
				$type = 'php';  // Hard-coded to php because we'll always redirect through /components/component_manager.php?r=<blah>
				$allow_scroll = empty($area['scrollable']) ? '1' : $area['scrollable'];
				$filename = empty($area['src']) ? '' : $area['src'];
				if ( substr($filename, 0, 4) == 'http' ) $filename = $this->filenameRedirScript . $filename;

				if ( $id === '0' ) {
					$preexistingRowQuerySql = 'select `id` from `poslavu_MAIN_db`.`components` where `name` = "[1]" and `namespace` = "[2]"';
					$preexistingRowQuery = mlavu_query($preexistingRowQuerySql, $name, $this->namespace);
					if ($preexistingRowQuery !== FALSE && mysqli_num_rows($preexistingRowQuery) > 0) {
						$isUpdate = true;

						// Get the row ID of the pre-existing row. 
						$preexistingRow = mysqli_fetch_assoc($preexistingRowQuery);
						$id = $preexistingRow['id'];
						$this->componentIds[$componentLayoutId][] = $id;
					}
					else if ($preexistingRowQuery === FALSE) {
						die(__METHOD__ . " got this error ". mlavu_dberror() ." running this query {$preexistingRowQuerySql}");
					}
				}

				//echo "DEBUG: id={$id}". print_r($this->componentIds) ."\n";  //debug

				if ( $isUpdate ) {
					$sql = "update `poslavu_MAIN_db`.`components` set `title`='[title]', `name`='[name]', `type`='[type]', `allow_scroll`='[allow_scroll]', `filename`='[filename]' where id='[id]' and namespace='[namespace]'";
					$query = mlavu_query($sql, array('title' => $title, 'name' => $name, 'type' => $type, 'allow_scroll' => $allow_scroll, 'filename' => $filename, 'id' => $id, 'namespace' => $this->namespace) );
					if ($query === FALSE) die(__METHOD__ . " got this error ". mlavu_dberror() ." running this query {$sql}");
				}
				else {
					$sql = "insert into `poslavu_MAIN_db`.`components` (`title`, `name`, `type`, `allow_scroll`, `filename`, `namespace`) values ('[title]', '[name]', '[type]', '[allow_scroll]', '[filename]', '[namespace]')";
					$query = mlavu_query($sql, array('title' => $title, 'name' => $name, 'type' => $type, 'allow_scroll' => $allow_scroll, 'filename' => $filename, 'namespace' => $this->namespace) );
					if ($query === FALSE) die(__METHOD__ . " got this error ". mlavu_dberror() ." running this query {$sql}");
					
					$this->componentIds[$componentLayoutId][] = mlavu_insert_id();
				}
			}
		}

		return $sql;
	}

	private function storeComponentsLayouts()
	{
		$sql = '';

		foreach ( $this->getArrayFromCollectionElement('tabs', $this->xmlArray) as $tab ) {
			$id = empty($tab['id']) ? '0' : $tab['id'];

			//error_log("<pre>id={$id} type=". $tab['type'] ." print_r(tab)=". print_r($tab, true) ."</pre>");  //debug

			// Skip the POS tab since its component settings are build-in.
			if ($tab['type'] == 'pos') {
				$this->componentLayoutIds[] = '0';  // We don't use $id here to make sure we don't introduce an error if they changed the XML element for id, for some reason. 
				continue;
			}

			// Prepare fields to be used in sql column values.
			$title = empty($tab['title']) ? ucwords(str_replace('_', ' ', $tab['name'])) : $tab['title'];
			$name = empty($tab['name']) ? str_replace( ' ', '_', strtolower($title) ) : $tab['name'];
			$unique_name = $this->namespace .':'. $name;
			$background = empty($tab['background']) ? '' : $tab['background'];
			if ( substr($background, 0, 4) == 'http' ) $background = $this->filenameRedirScript . $background;
			$special_type = empty($tab['special_type']) ? '' : $tab['special_type'];
			$modules_required = empty($tab['modules_required']) ? '' : $tab['modules_required'];
			$reload_behavior = empty($tab['reload_behavior']) ? 'js_refresh' : $tab['reload_behavior'];
			$kiosk_mode = empty($tab['kiosk_mode']) ? '0' : $tab['kiosk_mode'];
			$notes = empty($tab['notes']) ? '' : $tab['notes'];

			// Values comprised of multiple, comma-delimited values
			$c_ids =  empty($this->componentIds[$id]) ? '' : join(',', $this->componentIds[$id]);
			$c_xs = '';
			$c_ys = '';
			$c_ws = '';
			$c_hs = '';
			$c_zs = '';
			$c_types = '';

			foreach ( $this->getArrayFromCollectionElement('areas', $tab) as $area )  {
				$c_x = (empty($area['coord_x']) && $area['coord_x'] !== '0') ? '' : $area['coord_x'];
				$c_y = (empty($area['coord_y']) && $area['coord_y'] !== '0') ? '' : $area['coord_y'];
				$c_w = (empty($area['width'])   && $area['width']   !== '0') ? '' : $area['width'];
				$c_h = (empty($area['height'])  && $area['height']  !== '0') ? '' : $area['height'];
				$c_z = (empty($area['z-index']) && $area['z-index'] !== '0') ? '' : $area['z-index'];
				$c_type = empty($area['type']) ? 'v' : $area['type'];

				$c_xs .= empty($c_xs) && $c_xs !== '0' ? $c_x : ','. $c_x;
				$c_ys .= empty($c_ys) && $c_ys !== '0' ? $c_y : ','. $c_y;
				$c_ws .= empty($c_ws) && $c_ws !== '0' ? $c_w : ','. $c_w;
				$c_hs .= empty($c_hs) && $c_hs !== '0' ? $c_h : ','. $c_h;
				$c_zs .= empty($c_zs) && $c_zs !== '0' ? $c_z : ','. $c_z;
				$c_types .= empty($c_types) ? $c_type : ','. $c_type;
			}

			$isUpdate = false;

			$preexistingRowQuerySql = 'select `id` from `poslavu_MAIN_db`.`component_layouts` where id = "[1]" and unique_name like "[2]:%"';
			$preexistingRowQuery = mlavu_query($preexistingRowQuerySql, $id, $this->namespace);
			if ($preexistingRowQuery !== FALSE && mysqli_num_rows($preexistingRowQuery) > 0) {
				$isUpdate = true;

				// Get the row ID of the pre-existing row. 
				$preexistingRow = mysqli_fetch_assoc($preexistingRowQuery);
				$id = $preexistingRow['id'];
				$this->componentLayoutIds[] = "{$id}";
			}
			else if ($preexistingRowQuery === FALSE) {
				die(__METHOD__ . " got this error ". mlavu_dberror() ." running this query {$preexistingRowQuerySql}");
			}


			if ( $isUpdate ) {
				$sql = "update `poslavu_MAIN_db`.`component_layouts` set `unique_name`='[unique_name]', `title`='[title]', `background`='[background]', `c_ids`='[c_ids]', `c_xs`='[c_xs]', `c_ys`='[c_ys]', `c_ws`='[c_ws]', `c_hs`='[c_hs]', `c_zs`='[c_zs]', `c_types`='[c_types]', `special_type`='[special_type]', `modules_required`='[modules_required]', `reload_behavior`='[reload_behavior]', `kiosk_mode`='[kiosk_mode]', `notes`='[notes]' where id='[id]'";
				$query = mlavu_query( $sql, array('unique_name' => $unique_name, 'title' => $title, 'background' => $background, 'c_ids' => $c_ids, 'c_xs' => $c_xs, 'c_ys' => $c_ys, 'c_ws' => $c_ws, 'c_hs' => $c_hs, 'c_zs' => $c_zs, 'c_types' => $c_types, 'special_type' => $special_type, 'modules_required' => $modules_required, 'reload_behavior' => $reload_behavior, 'kiosk_mode' => $kiosk_mode, 'notes' => $notes, 'id' => $id) );
				if ($query === FALSE) die(__METHOD__ . " got this error ". mlavu_dberror() ." running this query {$sql}");
			}
			else {
				$sql = "insert into `poslavu_MAIN_db`.`component_layouts` (`unique_name`, `title`, `background`, `c_ids`, `c_xs`, `c_ys`, `c_ws`, `c_hs`, `c_zs`, `c_types`, `special_type`, `modules_required`, `reload_behavior`, `kiosk_mode`, `notes`) values ('[unique_name]', '[title]', '[background]', '[c_ids]', '[c_xs]', '[c_ys]', '[c_ws]', '[c_hs]', '[c_zs]', '[c_types]', '[special_type]', '[modules_required]', '[reload_behavior]', '[kiosk_mode]', '[notes]')";
				$query = mlavu_query($sql, array('unique_name' => $unique_name, 'title' => $title, 'background' => $background, 'c_ids' => $c_ids, 'c_xs' => $c_xs, 'c_ys' => $c_ys, 'c_ws' => $c_ws, 'c_hs' => $c_hs, 'c_zs' => $c_zs, 'c_types' => $c_types, 'special_type' => $special_type, 'modules_required' => $modules_required, 'reload_behavior' => $reload_behavior, 'kiosk_mode' => $kiosk_mode, 'notes' => $notes) );
				if ($query === FALSE) die(__METHOD__ . " got this error ". mlavu_dberror() ." running this query {$sql}");
				
				$this->componentLayoutIds[] = mlavu_insert_id();
			}

		}

		return $sql;
	}

	private function storeComponentsPackage()
	{
		$sql = '';
		$id = 0;

		$product = $this->xmlArray;

		$title = $this->namespace;
		//$layout_ids = empty($product['layout_ids']) ? join(',', $this->componentLayoutIds) : $product['layout_ids'];
		$layout_ids = implode(',', $this->componentLayoutIds);  // TO DO : concat list of ids
		$parent = empty($product['parent']) ? '0' : $product['parent'];
		$package_code = empty($product['package_code']) ? $this->packageCode : $product['package_code'];
		$order_addons = empty($product['order_addons']) ? '' : $product['order_addons'];
		$notes = empty($product['notes']) ? '' : $product['notes'];
		$modules_required = empty($product['modules_required']) ? '' : $product['modules_required'];
		$modules_included = empty($product['modules_included']) ? '' : $product['modules_included'];

		$isUpdate = false;

		$preexistingRowQuerySql = 'select * from `poslavu_MAIN_db`.`component_packages` where title = "[1]"';
		$preexistingRowQuery = mlavu_query($preexistingRowQuerySql, $title);
		if ($preexistingRowQuery !== FALSE && mysqli_num_rows($preexistingRowQuery) > 0) {
			$isUpdate = true;

			// Get the row ID of the pre-existing row. 
			$preexistingRow = mysqli_fetch_assoc($preexistingRowQuery);
			$id = $preexistingRow['id'];
		}
		else if ($preexistingRowQuery === FALSE) {
			die(__METHOD__ . " got this error ". mlavu_dberror() ." running this query {$preexistingRowQuerySql}");
		}

		if ( $isUpdate ) {
			// Commented out the version of the update statement that will update most of the fields.
			//$sql = "update `poslavu_MAIN_db`.`component_packages` set `title`='[title]', `layout_ids`='[layout_ids]', `parent`=[parent], `package_code`='[package_code]', `order_addons`='[order_addons]', `notes`='[notes]', `modules_required`='[modules_required]', `modules_included`='[modules_included]' where id='[id]'";
			//$query = mlavu_query($sql, array('title' => $title, 'layout_ids' => $layout_ids, 'parent' => $parent, 'package_code' => $package_code, 'order_addons' => $order_addons, 'notes' => $notes, 'modules_required' => $modules_required, 'modules_included' => $modules_included, 'id' => $id));
			$sql = "update `poslavu_MAIN_db`.`component_packages` set `title`='[title]', `layout_ids`='[layout_ids]', `notes`='[notes]' where id='[id]'";
			$query = mlavu_query($sql, array('title' => $title, 'layout_ids' => $layout_ids, 'notes' => $notes, 'id' => $id));
			if ($query === FALSE) die(__METHOD__ . " got this error ". mlavu_dberror() ." running this query {$sql}");
		} else {
			$sql = "insert into `poslavu_MAIN_db`.`component_packages` (`title`, `layout_ids`, `parent`, `package_code`, `order_addons`, `notes`, `modules_required`, `modules_included`) values ('[title]', '[layout_ids]', '[parent]', '[package_code]', '[order_addons]', '[notes]', '[modules_required]', '[modules_included]')";
			$query = mlavu_query($sql, array('title' => $title, 'layout_ids' => $layout_ids, 'parent' => $parent, 'package_code' => $package_code, 'order_addons' => $order_addons, 'notes' => $notes, 'modules_required' => $modules_required, 'modules_included' => $modules_included));
			if ($query === FALSE) die(__METHOD__ . " got this error ". mlavu_dberror() ." running this query {$sql}");
		}


		return $sql;
	}


	public function isEnabled($locationId)
	{
		$this->processEnabledStatus( $locationId );
		return $this->componentPackageIsEnabled;
	}

	/**
	* @parameter $enable  1 = enable, 0|<blank> = disable, -1 = don't do any of the updates statements.
	*/

	public function processEnabledStatus( $locationId, $enable = '-1' )
	{
		if ( empty($locationId) ) {
			return;
		}

		$customComponentPackageId = '';
		$activeComponentPackageId = '';
		$secondaryComponentPackageId = '';

		$activeComponentPackage = array();
		$secondaryComponentPackage = array();

		$sql = "select * from `poslavu_MAIN_db`.`component_packages` where `title` = '[title]' and `package_code` = '[package_code]'";
		$query = mlavu_query($sql, array('title' => $this->namespace, 'package_code' => $this->packageCode));
		if ( $query === false ) die(__METHOD__ . " got this error ". mlavu_dberror() ." running this query {$sql}");
		if ( mysqli_num_rows($query) > 0) {
			$component_package = mysqli_fetch_assoc($query);
			$customComponentPackageId = (isset($component_package['id'])) ? $component_package['id'] : '';
		}

		$sql = "select `component_package`, `component_package2` from `poslavu_{$this->namespace}_db`.`locations` where `id` = '[id]'";
		$query = lavu_query($sql, array('id' => $locationId));
		if ( $query === false ) die(__METHOD__ . " got this error ". lavu_dberror() ." running this query {$sql}");
		if ( mysqli_num_rows($query) > 0) {
			$locations = mysqli_fetch_assoc($query);
			$activeComponentPackageId = (isset($locations['component_package'])) ? $locations['component_package'] : '';
			$secondaryComponentPackageId = (isset($locations['component_package2'])) ? $locations['component_package2'] : '';
		}

		$sql = "select * from `poslavu_MAIN_db`.`component_packages` where `id` = '[id]'";
		if ( $activeComponentPackageId !== '0' ) {
			$query = mlavu_query($sql, array('id' => $activeComponentPackageId));
			$activeComponentPackage = mysqli_fetch_assoc($query);
		}
		if ( $secondaryComponentPackageId !== '0' ) {
			$query = mlavu_query($sql, array('id' => $secondaryComponentPackageId));
			$secondaryComponentPackage = mysqli_fetch_assoc($query);
		}

		$this->componentPackageIsEnabled = ($activeComponentPackageId === $customComponentPackageId);

		//error_log( "DEBUG: In component_manager with enable={$enable} componentPackageIsEnabled={$componentPackageIsEnabled} activeComponentPackageId={$activeComponentPackageId} customComponentPackageId={$customComponentPackageId}" );  //debug

		// The custom component_package is not enabled but it needs to be.
		if ( $enable === '1' && !$this->componentPackageIsEnabled ) {
			// First, as an unrelated data fix, copy any values in `order_addons` from the active component_package to the
			// custom component_package before enabling it, to make sure things like Customer Management still works.
			if ( !empty($activeComponentPackage['order_addons'] ) ) {
				$sql = "update `poslavu_MAIN_db`.`component_packages` set `order_addons` = '[1]' where id = '[2]'";
				$query = mlavu_query($sql, $activeComponentPackage['order_addons'], $customComponentPackageId);
				if ($query === FALSE) die(__METHOD__ . " got this error ". mlavu_dberror() ." running this query {$sql}");
			}

			// Save the active component_package ID in component_package2 so we can restore it later, if they disable the custom component package.
			$sql = "update `poslavu_{$this->namespace}_db`.`locations` set `component_package2` = '[1]' where id = '[2]'";
			$query = lavu_query($sql, $activeComponentPackageId, $locationId);
			if ($query === FALSE) die(__METHOD__ . " got this error ". lavu_dberror() ." running this query {$sql}");

			// Set the custom component_package ID to be the active component_package for the specified location.
			$sql = "update `poslavu_{$this->namespace}_db`.`locations` set `component_package` = '[1]' where id = '[2]'";
			$query = lavu_query($sql, $customComponentPackageId, $locationId);
			if ($query === FALSE) die(__METHOD__ . " got this error ". lavu_dberror() ." running this query {$sql}");

		}
		// The custom component_package is enabled but we need to disable it.
		else if ( $enable == '' && $this->componentPackageIsEnabled ) {
			$sql = "update `poslavu_{$this->namespace}_db`.`locations` set `component_package` = '[1]' where id = '[2]'";
			$query = lavu_query($sql, $secondaryComponentPackageId, $locationId);
			if ($query === FALSE) die(__METHOD__ . " got this error ". lavu_dberror() ." running this query {$sql}");

			// Blank out `locations`.`component_package2` if it has a non-zero value.
			if ( $secondaryComponentPackageId !== '0' ) {
				$sql = "update `poslavu_{$this->namespace}_db`.`locations` set `component_package2` = '0' where `id` = '[1]'";
				$query = lavu_query($sql, $locationId);
				if ($query === FALSE) die(__METHOD__ . " got this error ". lavu_dberror() ." running this query {$sql}");
			}
		}

	}

	/**
 	* This helper function was made for taking the <tabs>|<areas> collection arrays and extracting an array of the individual <tab>|<area> elements.
 	* It was needed because when we have just one <tab|area> element in the collection, our friend simplexml_load_string() (possibly in concert
 	* with our weird JSON json_encode()/json_decode()) trick) causes that single <tab>|<area> element to be stored in $this->xmlArray['blahs']['blah']
 	* while multiple <blah> elements get stored as indexed elements of $this->xmlArray['blahs'], like we'd expect.
	*/

	private function getArrayFromCollectionElement($collectionKey, $collectionElement)
	{
		$collectionArray = array();

		$baseKey = substr($collectionKey, 0, -1);

		//echo "<pre>In getArrayFromCollectionElement() with collectionKey={$collectionKey} baseKey={$baseKey}:". print_r($collectionElement[$collectionKey], true) .'</pre>';  //debug

		if ( isset($collectionElement[$collectionKey]) && is_array($collectionElement[$collectionKey]) ) {
	
			//echo 'isset(collectionElement[collectionKey][baseKey])='. isset($collectionElement[$collectionKey][$baseKey]) .' count(collectionElement[$collectionKey])='. count($collectionElement[$collectionKey]) .' collectionElement: <pre>'. print_r($collectionElement[$collectionKey], true) .'</pre>';  //debug

			if ( isset($collectionElement[$collectionKey][$baseKey]) && (isset($collectionElement[$collectionKey][$baseKey]['id']) || isset($collectionElement[$collectionKey][$baseKey]['type'])) ) {
				// Just one <baseKey> element, so stick the <tab> element in the return array.
				$collectionArray[] = $collectionElement[$collectionKey][$baseKey];
			}
			else {
				// There are multiple <baseKey> elements, so just overwrite the return array.
				$collectionArray = $collectionElement[$collectionKey][$baseKey];
			}
		}

		return $collectionArray;
	}
}

?>
