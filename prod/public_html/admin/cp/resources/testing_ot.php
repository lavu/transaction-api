<?php
	ini_set("display_errors",1);
	// prepare to load the file
	//ob_start();
	// load the file
	require_once(dirname(__FILE__).'/core_functions.php');
	require_once(dirname(__FILE__).'/../areas/reports/time_cards_obj.php');
	require_once("/home/poslavu/public_html/admin/lib/modules.php");
	// cleanup
	//$s_contents = ob_get_contents();
	//ob_end_clean();
	echo "THe end time is".$end_time;
	//$modules  = getModules();
	$data_name = "utterly_delici";
	TimeCardViews::draw_report();
	lavu_connect_dn($data_name, "poslavu_".$data_name."_db");
	echo "The total labor cost is:".print_r(get_total_labor_cost());

	function get_total_labor_cost() {
			$o_retval = new stdClass();
			global $time_cards_results;
			$data = array();
			foreach( $time_cards_results['users'] as $user_id => $user ){
				//id
				//work_weeks
				//processed
				//userfound
				//username
				//full_name
				//access
				//locationid
				//lavu_admin
				//deleted
				//classes
				//payrates
				//clocked_in
				//timePunches

				$payrates = $user->payrates;
				$classes = $user->classes;
				$full_name = $user->full_name;
				$user_id = $user->id;
				$punches = array();
				//foreach( $user->work_weeks as $work_week_start_ts => $work_week ){
					//week
					//next_week
					//work_days
					//seconds
					//days
					//processed
					//time_punches
					foreach($user->timePunches as $punch_id => $punch ){
						//id
						//locationid
						//userid
						//time_in
						//time_out
						//clocked_out
						//clocked_out_via_app
						//delete
						//rate_id
						//in_requested_time_period
						//role_id
						//start_week
						//end_week
						//contribution
						$punches[$punch->id] = $punch;
					}
				//}

				foreach( $punches as $punch_id => $punch ){

					$report_start_time = TimeCardAccountSettings::settings()->start_time;
					$report_end_time = TimeCardAccountSettings::settings()->end_time;

					if( !(($punch->time_out >= $report_start_time && $punch->time_out <= $report_end_time) ||
						($punch->time_in >= $report_start_time && $punch->time_in <= $report_end_time) ||
						($punch->time_in <= $report_start_time && $punch->time_out >= $report_end_time)) ||
						!$punch->in_requested_time_period ){
						continue;
					}

					$role = $classes->getRole( $punch->role_id );
					$row_result = array(
						$user_id,
						$full_name,
						$punch->time_in,//'m/d h:i A'
						($punch->clocked_out?'':'e') . $punch->time_out
					);
					$regular_seconds = 0;
					$regular_rate = 0;
					$overtime_rate = 0;
					$doubletime_rate = 0;

					foreach( $punch->contribution[REGULAR] as $role_id => $seconds ){
						$regular_seconds += $seconds;
						static $it = 0;

						if( $punch->payrate ){
							$regular_rate = $punch->payrate / 3600.0;
						} else if( $role_id || $role_id == 0 ){
							$regular_rate = $payrates->get_rate_rate( $role_id ) / 3600.0;
						}
					}

					$overtime_seconds = 0;
					foreach( $punch->contribution[OVERTIME] as $role_id => $seconds ){
						$overtime_seconds += $seconds;
						if( $punch->payrate ){
							$overtime_rate = $punch->payrate / 3600.0;
						} else if( $role_id || $role_id == 0  ){
							$overtime_rate = $payrates->get_rate_rate( $role_id ) / 3600.0;
						}
					}

					$double_time_seconds = 0;
					foreach( $punch->contribution[DOUBLE_TIME] as $role_id => $seconds ){
						$double_time_seconds += $seconds;
						if( $punch->payrate ){
							$doubletime_rate = $punch->payrate / 3600.0;
						} else if( $role_id || $role_id == 0  ){
							$doubletime_rate = $payrates->get_rate_rate( $role_id ) / 3600.0;
						}
					}

					$total_seconds = $regular_seconds + $overtime_seconds + $double_time_seconds;

					$regular_paid = $regular_seconds * $regular_rate * 1.0;
					$overtime_paid = $overtime_seconds * $overtime_rate * 1.5;
					$double_time_paid = $double_time_seconds * $doubletime_rate * 2.0;
					$total_paid = $regular_paid + $overtime_paid + $double_time_paid;

					$row_result[] = $total_seconds;
					$row_result[] = $regular_seconds;
					$row_result[] = $overtime_seconds;
					$row_result[] = $double_time_seconds;

					$row_result[] = $total_paid;
					$row_result[] = $regular_paid;
					$row_result[] = $overtime_paid;
					$row_result[] = $double_time_paid;
					$row_result[] = $punch_id;
					$data[] = $row_result;
				}
			}
			$o_retval->total = new stdClass();
			$o_retval->total->total_hours = 0;
			$o_retval->total->regular_hours = 0;
			$o_retval->total->overtime_hours = 0;
			$o_retval->total->doubletime_hours = 0;
			$o_retval->total->total_paid = 0;
			$o_retval->total->regular_paid = 0;
			$o_retval->total->overtime_paid = 0;
			$o_retval->total->doubletime_paid = 0;
			foreach( $data as $emp_entry ){
				$o_retval->total->total_hours += $emp_entry[4];
				$o_retval->total->regular_hours += $emp_entry[5];
				$o_retval->total->overtime_hours += $emp_entry[6];
				$o_retval->total->doubletime_hours += $emp_entry[7];
				$o_retval->total->total_paid += $emp_entry[8];
				$o_retval->total->regular_paid += $emp_entry[9];
				$o_retval->total->overtime_paid += $emp_entry[10];
				$o_retval->total->doubletime_paid += $emp_entry[11];
			}

			return $o_retval;
	}
	function getModules(){
		global $location_info;
		//$has_LLS = !($location_info['use_net_path'] == "0" || strpos($location_info['net_path'],".poslavu.com")!==false);
		//if((!sessvar_isset("modules")) || (sessvar("modules_dn") != admin_info('dataname'))) {
		$module_query = mlavu_query("SELECT `modules`, `package_status`,`id` FROM `poslavu_MAIN_db`.`restaurants` WHERE `data_name`='[1]' LIMIT 1", $data_name);
		$module_result = mysqli_fetch_assoc($module_query);
		
		// get the package
		$restaurant_query = mlavu_query("SELECT `id` FROM `poslavu_MAIN_db`.`restaurants` WHERE `dataname`='[1]' LIMIT 1", $data_name);
		$restaurant_result = array();
		if($restaurant_query && mysqli_num_rows($restaurant_query))
		{
			$restaurant_result = mysqli_fetch_assoc($restaurant_query);
		}
		else
		{
			$restaurant_result['id'] = $module_result['id'];
		}
		require_once(dirname(__FILE__).'/../sa_cp/billing/payment_profile_functions.php');
		//$package_result = find_package_status($restaurant_result['id']);
		
		// get the package
		/*$package_query = mlavu_query("SELECT `package` FROM `poslavu_MAIN_db`.`signups` WHERE `dataname`='[1]' LIMIT 1", admin_info('dataname'));
		if($package_query && mysqli_num_rows($package_query))
		{
			$package_result = mysqli_fetch_assoc($package_query);
		}
		else
		{
			$package_result['package'] = $module_result['package_status'];
		}*/
		
		$component_package = '';
		if( !empty($location_info['component_package_code']) ) {
			$component_package = $location_info['component_package_code'];
		}
		set_sessvar("modules",$module_result['modules']);
		set_sessvar("package",$package_result['package']);
		set_sessvar("isLLS",$has_LLS);
		set_sessvar("modules_dn",admin_info('dataname'));
		set_sessvar("componentPackage", $component_package);
		
		//}
		$modules = getActiveModules(sessvar('modules'),sessvar('package'),sessvar('isLLS'),sessvar('componentPackage'));
		
		return $modules;
	}
?>
