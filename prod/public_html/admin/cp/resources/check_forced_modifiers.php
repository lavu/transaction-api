<?php
	if (!isset($_SESSION)) {
		session_start();
	}

	require_once($_SERVER['DOCUMENT_ROOT']."/cp/resources/core_functions.php");
	$data_name = sessvar("admin_dataname");
	$data = array();
	if (isset($_GET['category_id'])) {
		$check_forced_modifiers = lavu_query("SELECT `title`, `ordertype_id` FROM `forced_modifiers` WHERE `list_id` = '[1]' AND `_deleted` != 1", $_GET['category_id']);
		while ($read = mysqli_fetch_assoc($check_forced_modifiers)) {
			$check_order_type = lavu_query("SELECT `id` FROM `order_tags` WHERE `id` = '[1]' AND `_deleted` != 1", $read['ordertype_id']);
			if (mysqli_num_rows($check_order_type)>0) {
				$data[] = $read['ordertype_id'];
			}
		}
		echo json_encode($data);
	}
?>