<?php

/*

Note:: This code will log database errors to the file ~/logs/dberrors.txt.  A
separate file is used (rather than error_log()) so that full tracebacks can be
included for each database error, which greatly aids troubleshooting.

== lavuquery.php ==

The new lavuquery.php has been designed to use mysqli and be optimized for
MySQL Cluster. It also defines legacy functions like lavu_query().

The cluster-aware functionality is implemented by using two underlying mysqli
'links' that are initialized on-demand, one for database reads and one for
database writes. The legacy functions like lavu_query(), as well as the new
autoquery() method, will inspect the query to determine whether it is a read or
write and direct it to the appropriate mysqli link. This is optimal for MySQL
Cluster, where it is best practice to send all writes to a single cluster node,
while distributing reads across all cluster nodes.  Our infrastructure has a
special 'database HAProxy' set up which is configured in such a way to support
this, while still allowing for true high-availability.

Please note that this is the *official* method for database access. It is
important that database queries are handled by these methods so they access the
cluster optimally, so no 'roll-your-own' ad-hoc DB access code should be used.
Every effort has been made in the design of this code to make it very easy to
extend, and add new types of DB connections for various uses without cluttering
up the code or PHP runtime environment with lots of extra DB access functions.
Following these guidelines will allow Lavu to adapt to future database changes
with relative ease.

=== Initial Setup ===

To use, add this to your code:

 require_once("../path/to../../cp/resources/lavuquery.php");

Lavuquery.php implements 'standard' functionality that defines two database
connections: 'rest' (restaurant) and 'poslavu' (poslavu account.)

The following files are also available, and can be included for enhanced
functionality. Note that these should be included *in place* of lavuquery.php,
as they will pull in lavuquery.php base functionality themselves:

;lavuquery_quickbooks.php: adds a quickbooks db connection.

;lavuquery_safe.php: initializes the 'rest' connection in a 'safe' mode
that will pre-query inserts to cc_transactions and make sure the record
doesn't already exist before inserting. If it already exists, then it
will silently avoid overwriting the existing data. This functionality
was written to act as a safe-guard against recording duplicate transactions
in the database.

;lavuquery_base.php: Defines all classes and legacy methods, but does not
actually initialize the database connections. This is typically not used by
devs directly, but is included by lavuquery_*.php files to extend base
functionality.

=== Legacy vs. New Method ===

The legacy way of querying a a restaurant database is as follows:

 lavu_query('SELECT * from foo.bar where `x` = '[1]');

or

 mlavu_query(...);

The better way of doing this is now:

 $conn = ConnectionHub::getConn('rest');
 $result = $conn->legacyQuery(...);

Note that the string 'rest' above is a literal string, not the dataname. The
restaurant connection will divine the correct dataname via the $data_name 
global. You can also specify 'poslavu' for a connection that uses the poslavu
user. Like mlavu_query, the poslavu_MAIN_db will be selected by default.

 $conn = ConnectionHub::getConn('poslavu');
 $result = $conn->legacyQuery(...);

legacyQuery() has identical function call semantics as lavu_query and
mlavu_query(). Like mlavu_query(), $conn

Once you have $conn defined, it can be used to call a variety of helper
functions:

 // select another database; typically only useful with 'poslavu' connection:
 $conn->selectDB('dbname');

 // properly escape string for the encoding of the db connection:
 $conn->escapeString('mystring');

 // get the ID of the last insert on the connection (uses the 'write' db link.)
 $conn->insertID();

 // get the number of affected rows from the last database operation on the
 // connection (like insertID(), always uses the 'write' mysqli link.)
 $conn->affectedRows();

If you have a function that uses $conn only once, it can be more efficient to
not define $conn but to access the new methods this way:

 result = ConnectionHub::getConn('poslavu')->legacyQuery(...);

lavu_query calls $conn->autoquery() behind the scenes which will automatically
determine if a read or write operation is being performed and use the appropriate
underlying database connection. To access underlying database connections, you
can use the following methods:

 $result = $conn->query($mode, $querystring);

or (to auto-detect read or write):

 $result = $conn->autoquery($querystring);

Above, set $mode for query() to 'read' or 'write'. $querystring is passed directly to
mysqli::query so it does not have features of legacyQuery()/autoQuery().

=== Ben Bean's DB Class ===

Much of the Lavu code-base uses Ben Bean's DB class to construct and run queries.
Ben's class has been converted to work with our new system. To get a reference to
the DB class, use the following method:

 $dbapi = ConnectionHub::getConn('rest').getDBAPI();

You could also specify 'poslavu' instead of 'rest', above, to get a DB object that
uses 'poslavu' credentials for queries.

Note:: Ben's DB class methods have been converted from static to 'regular'
methods, so use $dbapi->method() rather than the old DB::method() approach.

=== Config Settings ===

A lot of Lavu code has issues where duplicate values are written to the 'config' table,
because the code does not check to see if the config setting already exists. For this
reason, it's strongly recommended to use the methods RestaurantDBConnector::setConfig()
and getConfig() to set and retrieve config settings. They can be used as follows:

 $conn = ConnectionHub::getConn('rest');
 $spec = array('setting'=>'foo', 'type'=>'bar', 'location'=>'oni');

 $settings = mysqli_fetch_assoc($conn->getConfig($spec));

Currently, getConfig() will return mysqli::result object that contains at most
a single row from the config table that matches.

To set a config setting, use the following code:

 $vals = array('foo'=>'bleh');
 $conn->getConfig($spec, $vals);

Above, getConfig() will update values in the config table to $vals on any row that
matches the fields defined in $spec. At most *one* row will be updated. If there is
no existing match, then a new row will be added.

If you would like to use similar behavior on other tables, the following methods
are available:

 $conn->getValue($spec, $table, $db=null);
 $conn->setValue($spec, $vals, $table, $db=null);

They use the same underlying code as getConfig() and setConfig() but allow you to
specify the table name, and if using the 'poslavu' connection, the db name.

*/

require_once(__DIR__ . "/lavuquery_base.php");

ConnectionHub::addConn('poslavu', new DBConnector());
ConnectionHub::addConn('rest', new RestaurantDBConnector());
ConnectionHub::addConn('poslavu_chain', new DBConnector('poslavu_CHAIN_DATA_db'));

