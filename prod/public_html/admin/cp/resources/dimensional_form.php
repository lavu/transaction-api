<?php
require_once("gt_functions.php");
require_once(resource_path()."/chain_functions.php");
require_once("inventory_functions.php");
require_once dirname( __FILE__ ).'/../../lib/constants.php';

function format_currency($monitary_amount, $dont_use_monetary_symbol = false, $monitary_symbol = "") {

    global $location_info;

    $needToAddMinus = "";
    $number = $monitary_amount;

    $precision = isset($location_info['disable_decimal']) ? $location_info['disable_decimal'] : 2;
    if ($location_info['decimal_char'] != "") {
	    $decimal_char = isset($location_info['decimal_char']) ? $location_info['decimal_char'] : '.';
	    $thousands_char = (isset($location_info['decimal_char']) && $location_info['decimal_char']==".") ? "," : '.';
    }

    $thousands_sep = ",";
    if( !empty($thousands_char) ) {
		$thousands_sep = $thousands_char;
	}

	if ($number < 0) {
		$number = abs($number);
		$needToAddMinus = "-";
	}

    if ($monitary_symbol === "") {
        $monitary_symbol = isset($location_info['monitary_symbol']) ? htmlspecialchars($location_info['monitary_symbol']) : '';
    }
    $left_or_right = isset($location_info['left_or_right']) ? $location_info['left_or_right'] : 'left';

    $left_side = "";
    $right_side = "";

    if (!$dont_use_monetary_symbol) {
        if ($left_or_right == 'left') {
            $left_side = ($location_info['primary_currency_code'] == 'USD') ? $monitary_symbol : html_entity_decode($monitary_symbol."&lrm;");
        } else {
            $right_side = ($location_info['primary_currency_code'] == 'USD') ? $monitary_symbol : html_entity_decode($monitary_symbol."&rlm;");
        }
    }

    $output = $needToAddMinus.$left_side.number_format($number, $precision, $decimal_char, $thousands_sep).$right_side;
    return $output;
}

function parse_currency( $monitary_str ){
	global $location_info;

	$decimal_char = '.';
	$thousands_char = ',';
	if( $location_info['decimal_char'] != '.' ){
		$decimal_char = $location_info['decimal_char'];
		$thousands_char = '.';
	}

	$monitary_symbol = $location_info['monitary_symbol'];

	//Could be either
	// - $monitary_symbol \d$thousands_char\d\d\d$decimal_char\d\d\d
	// $monitary_symbol - \d$thousands_chard\d\d\d$decimal_Char\d\d\d
	// - \d$thousands_char\d\d\d$decimal_char\d\d\d $monitary_symbol

	$reg_chars = str_split('\^$.[]|()?*+{}');

	//escape all characters in monitary_symbol for regex
	$escaped_monitary_symbol = preg_quote($monitary_symbol);
	$escaped_monitary_symbol = str_ireplace('/', '\/', $escaped_monitary_symbol);
	//$msarr = str_split( $monitary_symbol );
	//for($i = 0; $i < count($msarr); $i++ ){
	//  if( in_array( $msarr[$i], $reg_chars ) ){
	//    $msarr[$i] = "\\" . $msarr[$i];
	//  }
	//}
	//$escaped_monitary_symbol = implode('', $msarr);

	//$monitary_str = str_replace($monitary_symbol, "", $monitary_str);
  		// strip out everything that comes before a digit
	// 1 negative sign
	// 2 monitary symbol
	// 3 negative sign
	// 4 amount
	// 5 monitary symbol
	$preg = "/(-)?\s*({$escaped_monitary_symbol})?\s*(-)?\s*([\d\\{$decimal_char}\\{$thousands_char}]*)\s*({$escaped_monitary_symbol})?/";

	preg_match( $preg , $monitary_str, $a_matches);
	if (count($a_matches) > 0) {
		$monitary_str = $a_matches[1] . $a_matches[3] . $a_matches[4];
	}

	// strip out everything that is not a digit, decimal car, or -
	$monitary_str = preg_replace("/[^\d{$decimal_char}-]/", "", $monitary_str);

	// unify everything with a "." decimal char
	$monitary_str = str_replace($decimal_char, ".", $monitary_str );

	$dashes = explode("-", $monitary_str);

	if(count($dashes) > 1){
		$monitary_str = '-'.$dashes[1];
	}

	$decimal_char = isset($location_info['decimal_char'])?$location_info['decimal_char']:'.';
	$thousands_char = ',';
	if( $decimal_char != '.' && $decimal_char != ' ' ){
		$thousands_char = '.';
	}
	$monitary_symbol = isset($location_info['monitary_symbol'])?$location_info['monitary_symbol']:'$';
	return $monitary_str * 1;
}

function cat_row_code($qtitle_category, $cfields, $name="",$id="", $category_read=false, $cinfo=false, $inventory_tablename=null)
{
	$readOnlyFields = array();
	$readOnly = "";
	if(isset($category_read['access_level'])){
		if(admin_info('access_level') < $category_read['access_level']){
			foreach($cfields as $field){
				$readOnlyFields[] = $field[0];
			}
			$readOnly = " READONLY";
		}
	}
	$cat_row_code = "<div id='icrow_(cat)_0' style='display:block'>";

	$cat_row_code .= "<table cellspacing=0 cellpadding=0 width='100%'><tr>";
	$cat_row_code .= "<td align='left' width='100%'>";
	$cat_row_code .= gt_input("ic_(cat)_category",$qtitle_category." Name","#666","font-weight:bold' size='18",$name,$readOnly)."</td>";
	$cat_row_code .= "</td>";
	$cat_row_code .= "<td align='right' width='360' style='text-align:right;'>";

	$cat_row_code .= "<table cellspacing=0 cellpadding=0><tbody><td>";
	$ival = array();
	$ival['storage_key'] = isset($category_read['storage_key'])? $category_read['storage_key'] : "";
	$cat_row_code .= "</td>";
	$cat_row_code .= row_code_fields("ic_(cat)_col_[qname]",$cfields,$ival,$category_read, $readOnlyFields, "");
	$cat_row_code .= "</tbody></table>";

	$cat_row_code .= gt_hidden("ic_(cat)_categoryid",$id);
	$cat_row_code .= gt_hidden("ic_(cat)_delete_0",0);

	$cat_row_code .= "</td>";
	if($cinfo && is_array($cinfo))
	{
		$details_column = $cinfo['details_column'];
		$send_details = $cinfo['send_details'];
		$category_primefield = $cinfo['category_primefield'];

		if($details_column)
		{
			if($send_details) $send_details = "ic_(cat)_col_".$send_details;
			$cat_row_code .= "<td>".gt_details("ic_(cat)_col_details",$details_column,"ic_(cat)_category",$send_details,$id)."</td>";
		}
	}

	// copy move icon and stuff
	if (isset($_GET['mode']) && $_GET['mode'] == 'menu_edit_menu' ) {
		$cat_row_code .= str_replace("\n", '', "
			<td class='cat_move_td' style='cursor: pointer;' onclick='draw_copy_move_cat((cat));'>
				<div style='background-image: url(\"images/btn_move_copy_2page_dk.png\"); background-repeat: no-repeat; width: 20px; height: 18px; margin-left: 4px;' class='copy_cat_div'>&nbsp;</div>
				<input type='hidden' class='category_index' value='(cat)' />
			</td>");
	}

	if ($inventory_tablename == '`exchange_rates`' || $inventory_tablename == '`secondary_exchange_rates`') {
		$cat_row_code .= "<td align='right'>&nbsp;<a class='closeBtn' onclick='click_delete((cat),0, \"true\")'>X</a></td>";
	}
	$cat_row_code .= "<td align='right'>&nbsp;<a class='closeBtn' onclick='click_delete((cat),0, \"false\")'>X</a></td>";
	$cat_row_code .= "</tr></table>";
	$cat_row_code .= "</div>";
	$cat_row_code .= "<div id='icrow_del_(cat)_0' style='display:none'>";
	$cat_row_code .= "<table width='100%'><tr><td id='icrow_delinfo_(cat)_0' style='color:red' width='360'>&nbsp;</td><td width='10'><a style='cursor:pointer' onclick='click_undelete((cat),0)'><img src='images/undoArrow.png' /></a></td></tr></table>";
	$cat_row_code .= "</div>";
	return $cat_row_code;
}

function row_code_fields($set_qcol_name, $qdbfields, $ival, $inventory_read, $readonly_fields=array(), $isOrderTag="")
{	global $location_info;// add for decimal character format,LP-4194
	$decimal=$location_info['decimal_char'];
	global $inventory_primefield;
	global $inventory_tablename;
	global $location_info; // add for LP-4492
	global $data_name;
	$decimal=$location_info['decimal_char']; // add for LP-4492
	$data_name = sessvar("admin_dataname");
	$item_row_code = "";
	$item_row_code_hidden = "";

	if($ival['id']){
		// Added here to remove same menu item query call multiple times LP-7689 By Hayat
		$item_query = lavu_query("select currency_type, combo, forced_modifier_group_id, modifier_list_id  from menu_items where id = '[1]'", $ival['id']);
		$item_details = mysqli_fetch_assoc($item_query);
	}

	for($i = 0; $i < count($qdbfields); $i++)
	{
		$qname = (isset($qdbfields[$i][0]))?$qdbfields[$i][0]:"";
		$qfield = (isset($qdbfields[$i][1]))?$qdbfields[$i][1]:$qname;
		$qtitle = (isset($qdbfields[$i][2]))?$qdbfields[$i][2]:$qname;
		$qtype = (isset($qdbfields[$i][3]))?$qdbfields[$i][3]:false;
		$qprop = (isset($qdbfields[$i][4]))?$qdbfields[$i][4]:array();
		$qextraval = (isset($qdbfields[$i][5]))?$qdbfields[$i][5]:false;
		$qprop_value = $qprop;
		if (is_array($qprop)) {
			for($n=0; $n<8; $n++) {
				if(!isset($qprop[$n])) $qprop[$n] = "";
			}
		}

		if($qtype=="set_hidden" && $qprop && !is_array($qprop))
		{
			$ival[$qname] = $qprop;
		}
		else if($inventory_read)
		{
			$ival[$qname] = isset($inventory_read[$qfield]) ? $inventory_read[$qfield] : "";
			$fldTypeCurrency = array( "cost", "price" );

			if ($ival[$qname] !== "" && $qname !== "" && in_array($qname, $fldTypeCurrency))
			{
				if ($item_details['currency_type'] == 1) {
					$ival[$qname] = $ival[$qname];
				} else {
					$ival[$qname] = format_currency($ival[$qname]);
				}
			}
		}
		else if ($qtype=="hidden" && ($qprop || $qprop=="0") && !is_array($qprop) )
		{
			$ival[$qname] = $qprop;
		}
		else
			$ival[$qname] = "";
		
		if($qextraval && isset($ival[$qextraval])) $show_qextraval = $ival[$qextraval];
		else $show_qextraval = "";

		$qcol_name = str_replace("[qname]",$qname,$set_qcol_name);
		$qrow_name = str_replace($qname,$inventory_primefield,$qcol_name);

		$readonly = "";
		if ($readonly_fields && in_array($qname, $readonly_fields)) {
			$readonly = " readonly";
		}

		if($qtype=="input") {
			// To prevent warnings for accessing unset keys ($qprop[0], $ival[$qname])
			if(!isset($qprop[0]))
			{
				$qprop[0]="";
			}
			if(!isset($qname) || !isset($ival[$qname])){
				if(!isset($qname)){
					$qname = 0;
				}
				$ival[$qname] = "";
			}
			if($qname=="calc"){ //START LP-4492
			    if($decimal == ',')
			    {
			        $ival[$qname]=str_replace('.',',',$ival[$qname]);
			    }
			} //END LP-4492
			$item_row_code .= "<td>".gt_input($qcol_name,$qtitle,"#1c591c",$qprop[0],$ival[$qname],'',$readonly,$ival)."</td>";
		}
		else if($qtype=="hidden" || $qtype=="set_hidden")
		{
			if ($qname == "isOrderTag") {
				if(!empty($inventory_read)) {
					if ($inventory_read['isOrderTag'] == 0) {
						$ival[$qname] = 0;
					} else if ($inventory_read['isOrderTag'] == 1) {
						$ival[$qname] = 1;
					}
				} else {
					if ($isOrderTag == 0) {
						$ival[$qname] = 0;
					} else if ($isOrderTag == 1) {
						$ival[$qname] = 1;
					}
				}
			}

			if($qcol_name=="ic_(cat)_happyhour_(item)"){//START LP-4194
				if($decimal == ','){
					$ival[$qname]=str_replace('.',',',$ival[$qname]);
				}//END LP-4194
			}
			$item_row_code_hidden .= gt_hidden($qcol_name,$ival[$qname]);
		}
		else if($qtype=="picture")
			$item_row_code .= "<td>".gt_picture($qcol_name,$ival[$qname],$qprop,"",$ival['storage_key'])."</td>";
		else if(strpos($qtype, 'picture_grid_') === 0)
			$item_row_code .= "<td>".gt_picture_grid($qcol_name,$ival[$qname],$qprop,$ival['storage_key'])."</td>";
		else if($qtype=="select" || $qtype=="select_exclusive") {
			$item_row_code .= "<td>".gt_select($qcol_name, $qprop_value, $ival[$qname], "#1c591c", $qtype, $ival['id'], $item_details)."</td>";
		}else if($qtype=="bool")
			$item_row_code .= "<td style='padding-right:2px;'>".gt_bool($qcol_name,$qprop_value,$ival[$qname],$qtitle)."</td>";
		else if($qtype=="special") {
			if($qfield=='comboitems' || $qfield=='ingredients' )
			{
				$show_qextraval_id = $ival['id'];
			}
            if ($qtitle == "Ingredients") {

                //TODO:- Removed this if condition after full migration implementation
                if (sessvar("INVENTORY_MIGRATION_STATUS") == "Completed_Ready" || sessvar("INVENTORY_MIGRATION_STATUS") == "Skipped_Ready") {
                    $ival[$qname] = $inventory_read['inventoryItems'];
                    $show_qextraval = isset($inventory_read['id'])?$inventory_read['id']:false;

                } else {
                    $ival[$qname] = $ival[$qname];
                }
            }
			if($qfield=='comboitems' || $qfield=='ingredients' ){ $show_qextraval = $ival['id'];}
			$item_row_code .= "<td>";
			$item_row_code .= gt_special($qcol_name, $qtitle, $ival[$qname], $qrow_name, "#1c591c", $qprop[0], $qprop[1], $qprop[2], $qprop[3], $show_qextraval, $show_qextraval_id, $item_details, $qname);
			$item_row_code .= "</td>";
		}
	}
	if($item_row_code_hidden!="")
	{
		$item_row_code .= "<td>" . $item_row_code_hidden . "</td>";
	}

	$ival = array();

	$ival['id'] = isset($inventory_read['id']) ? $inventory_read['id'] : '';

	return $item_row_code;
}

function itemOrderTypeRowCode($qdbfields, $inventory_read=false, $deletable=false) {
	global $inventory_tablename;

	$orderTypeFields = $qdbfields;
	unset($orderTypeFields[0]);
	$orderTypesQuery = lavu_query("SELECT `id`, `name` FROM `order_tags` WHERE `_deleted` != '1' AND `active` != '0' AND `name` NOT LIKE '%Lavu ToGo%'");
	$orderTypesList[] = array('Select Order Type', '');
	while ($orderTypesRead = mysqli_fetch_assoc($orderTypesQuery)) {
		$orderTypesList[] = array($orderTypesRead['name'], $orderTypesRead['id']);
	}
	$orderTypeFields[0] = array("title", "title", speak("Order Type"), "select", $orderTypesList);

	$ival = array();
	$ival['id'] = isset($inventory_read['id'])? $inventory_read['id'] : "";
	$item_row_code = "";
	$item_row_code .= "<div id='icrow_(cat)_(item)' name='icrow_(cat)_(item)' style='display:block' onmouseover='if(1==2) this.style.borderBottom = \"solid 1px #777777\"' onmouseout='if(1==2) this.style.borderBottom = \"solid 0px #ffffff\"'>";
	$item_row_code .= "<table cellspacing=0 cellpadding=0 class='_cls_tbl_' ><tbody><tr class='_cls_row_' data-id='_row_(cat)_(item)'>";
	$delete_button_code = ($deletable) ? "<img src='images/blank_19x19.png' />" : "<a class='closeBtn' onclick='click_delete((cat),(item),\"false\")'>X</a>";
	$item_row_code .= row_code_fields("ic_(cat)_[qname]_(item)", $orderTypeFields, $ival, $inventory_read, array(), 1);
	$item_row_code .= "<td>&nbsp;".$delete_button_code."</td>";
	$item_row_code .= "</tr></tbody></table>";
	$item_row_code .= gt_hidden("ic_(cat)_id_(item)",$ival['id']);
	$item_row_code .= gt_hidden("ic_(cat)_delete_(item)",0);
	$item_row_code .= "</div>";
	$item_row_code .= "<div id='icrow_del_(cat)_(item)' name='icrow_del_(cat)_(item)' style='display:none'>";
	$item_row_code .= "<table width='100%'><tbody><tr><td id='icrow_delinfo_(cat)_(item)' style='color:red;'>&nbsp;</td><td width='10'><a style='cursor:pointer' onclick='click_undelete((cat),(item))'><img src='images/undoArrow.png' /></a></td></tr></tbody></table>";
	$item_row_code .= "</div>";
	return $item_row_code;
}

function item_row_code($qdbfields, $details_column=false, $send_details=false, $inventory_primefield=false, $field=false, $inventory_read=false, $allow_reorder=false, $deletable=false, $readonly_fields=array())
{
	global $inventory_tablename;
	$ival = array();
	$ival['id'] = isset($inventory_read['id'])? $inventory_read['id'] : "";
	$ival['storage_key'] = isset($inventory_read['storage_key'])? $inventory_read['storage_key'] : "";
	$item_row_code = "";
	$item_row_code .= "<div id='icrow_(cat)_(item)' name='icrow_(cat)_(item)' style='display:block' onmouseover='if(1==2) this.style.borderBottom = \"solid 1px #777777\"' onmouseout='if(1==2) this.style.borderBottom = \"solid 0px #ffffff\"'>";
	$item_row_code .= "<table cellspacing=0 cellpadding=0 class='_cls_tbl_' ><tbody><tr class='_cls_row_' data-id='_row_(cat)_(item)'>";

	if($allow_reorder)
	{
		// reorder icon and stuff
		$item_row_code .= "<td>";
		$item_row_code .= "<table cellspacing=0 cellpadding=0><tr><td width='24' height='18' class='moveArrow' onselectstart='return false;' onmousedown='return moveitem_mousedown(\"(cat)\",\"(item)\");' onmouseup='moveitem_mouseup(\"(cat)\",\"(item)\")'  /> <img src=\"images/movebox.png\" height=\"96%\" width=\"17px\"/>";
		if($inventory_read)
		{
			if(isset($_GET['test'])) $row_order_type = "text' style='width:24px"; else $row_order_type = "hidden";
			$item_row_code .= "&nbsp;<input type='$row_order_type' name='icrow_(cat)_(item)_order' id='icrow_(cat)_(item)_order' value='".$inventory_read['_order']."'>";
		}
		else
			$item_row_code .= "&nbsp;";
		$item_row_code .= "</td></tr></table>";
		$item_row_code .= "</td>";
	}
	if ($inventory_tablename == 'order_tags') {
	    $checkTags = array(PICKUP_LABEL, DELIVERY_LABEL, DINE_IN_LABEL);
		if ( in_array($inventory_read['name'], $checkTags)) {
			$readonly_fields[] = 'name';
		}
	}

	$item_row_code .= row_code_fields("ic_(cat)_[qname]_(item)", $qdbfields, $ival, $inventory_read, $readonly_fields, 0);

    if ($details_column) {

		if ($send_details) {
			$send_details = "ic_(cat)_".$send_details."_(item)";
		}

		$extra2 = "";
		if ($inventory_tablename == "menu_items") {
			$extra2 = $inventory_read['forced_modifier_group_id'];
		}

		$item_row_code .= "<td>".gt_details("ic_(cat)_details_(item)",$details_column,"ic_(cat)_".$inventory_primefield."_(item)",$send_details,$ival['id'],$extra2)."</td>";
	}

	// copy move icon and stuff
	if (isset($_GET['mode']) && $_GET['mode'] == 'menu_edit_menu') {
		$item_row_code .= str_replace("\n", '', "
			<td class='item_post_move_td' style='cursor: pointer;' onclick='draw_copy_move_item((cat), (item));'>
				<div style='background-image: url(\"images/btn_move_copy_2page.png\"); background-repeat: no-repeat; width: 20px; height: 18px; margin-left: 4px;' class='copy_item_div'>&nbsp;</div>
				<input type='hidden' class='item_index' value='(item)' />
				<input type='hidden' class='category_index' value='(cat)' />
			</td>");
	}
	if($inventory_tablename != '`master_currencies`') {
		$fVals = strtolower(str_replace(' ','',$inventory_read['name']));
		$tdIds = "id='td_$fVals'";
		if ($inventory_tablename == '`exchange_rates`' || $inventory_tablename == '`secondary_exchange_rates`') {
			$delete_button_code = ($deletable) ? "<img src='images/blank_19x19.png' />" : "<a class='closeBtn' onclick='click_delete((cat),(item),\"true\")'>X</a>";
		} else {
			if (isset($ival['id']) && $ival['id'] != "") {
				$query_statement = "SELECT `id` FROM `forced_modifiers` WHERE `ordertype_id` = ".$ival['id']."  AND `_deleted` != '1'";
				$qResult = lavu_query($query_statement);
			}
			if ($inventory_tablename == 'order_tags' && mysqli_num_rows($qResult) != 0) {
				$delete_button_code = ($deletable) ? "<img src='images/blank_19x19.png' />" : "<a class='closeBtn' onclick='clickDeleteOrderType(\"This order type is being used as a forced modifier. Deleting it will remove it from all modifier lists that it is a part of. Are you sure you want to proceed\",(cat),(item),\"false\")' id='del_$fVals'>X</a>";
			} else {
				$delete_button_code = ($deletable) ? "<img src='images/blank_19x19.png' />" : "<a class='closeBtn' onclick='click_delete((cat),(item),\"false\")'>X</a>";
			}
		}
	$item_row_code .= "<td $tdIds>&nbsp;".$delete_button_code."</td>";
	}
	$item_row_code .= "</tr></tbody></table>";
	$item_row_code .= gt_hidden("ic_(cat)_id_(item)",$ival['id']);
	$item_row_code .= gt_hidden("ic_(cat)_delete_(item)",0);
	$item_row_code .= "</div>";
	$item_row_code .= "<div id='icrow_del_(cat)_(item)' name='icrow_del_(cat)_(item)' style='display:none'>";
	if($inventory_tablename == '`exchange_rates`' || $inventory_tablename == '`secondary_exchange_rates`'){
	 $item_row_code .= "<table width='100%'><tbody><tr><td id='icrow_delinfo_(cat)_(item)' style='color:red;'>&nbsp;</td><td width='10'><a style='cursor:pointer' onclick='click_undelete((cat),(item))'><img src='../images/undoArrow.png' /></a></td></tr></tbody></table>";
}
else {
	$item_row_code .= "<table width='100%'><tbody><tr><td id='icrow_delinfo_(cat)_(item)' style='color:red;'>&nbsp;</td><td width='10'><a style='cursor:pointer' onclick='click_undelete((cat),(item))'><img src='images/undoArrow.png' /></a></td></tr></tbody></table>";
}
	$item_row_code .= "</div>";
	return $item_row_code;
}

function get_qdbfields($qfields)
{
	$qdbfields = array();
	for ($n = 0; $n < count($qfields); $n++) {
		if (count($qfields[$n]) > 1) {
			$qdbtitle = (isset($qfields[$n][2]))?$qfields[$n][2]:"";
			$qdbtype = (isset($qfields[$n][3]))?$qfields[$n][3]:"";
			$qdbprop = (isset($qfields[$n][4]))?$qfields[$n][4]:array();
			$qdbextraval = (isset($qfields[$n][5]))?$qfields[$n][5]:"";
			$qdbfields[] = array($qfields[$n][0],$qfields[$n][1],$qdbtitle,$qdbtype,$qdbprop,$qdbextraval);
		}
	}
	return $qdbfields;
}

function wizard_button_new($img,$title,$prop,$w,$h,$clr="#000000")
{
	if ($img == "order_type") {
		$style = "style=\"width:230px;\"";
	} else {
		$style = "";
	}
	$str = "";
	$w=117;
	$str .= "<span id='add_button_with_text'>";
	$str .= "<table class='addBtn' cellspacing=0 cellpadding=0 $style $prop>";
	$str .= "<td id=\"tdButton\" width='26' style='color:white; padding-left:25px; padding-top:4px' valign='top'>".$title."</td>";
	$str .= "</table>";
	$str .= "</span>";
	
	return $str;
}

function create_dimensional_form($qfields,$qinfo,$cfields=false)
{
	global $location_info;
	global $inventory_tablename;

	if (isset($qinfo['qtitle_category'])) {
		$qtitle_category = $qinfo['qtitle_category'];
	}
	if (isset($qinfo['qtitle_category_plural'])) {
		$qtitle_category_plural = $qinfo['qtitle_category_plural'];
	}
	if (isset($qinfo['qtitle_item'])) {
		$qtitle_item = $qinfo['qtitle_item'];
	}
	if (isset($qinfo['category_tablename']))
	{
		$category_tablename = $qinfo['category_tablename'];
	}
	if($qinfo['inventory_tablename'])
	{
		$inventory_tablename = $qinfo['inventory_tablename'];
	}
	if(isset($qinfo['category_fieldname']))
	{
		$category_fieldname = $qinfo['category_fieldname'];
	}
	if(isset($qinfo['inventory_fieldname']))
	{
		$inventory_fieldname = $qinfo['inventory_fieldname'];
	}
	if(isset($qinfo['inventory_select_condition']))
	{
		$inventory_select_condition = $qinfo['inventory_select_condition'];
	}
	if(isset($qinfo['inventory_primefield']))
	{
		$inventory_primefield = $qinfo['inventory_primefield'];
	}
	if(isset($qinfo['details_column']))
	{
		$details_column = $qinfo['details_column'];
	}
	if(isset($qinfo['category_primefield']))
	{
		$category_primefield = $qinfo['category_primefield'];
	}
	if(isset($qinfo['field']))
	{
		$field = $qinfo['field'];
	}
	if(isset($qinfo['qformname'])) $qformname = $qinfo['qformname']; else $qformname = "";


	if($inventory_tablename == "MealPlanUser"){
		$chain_ids = array();
		$chainLocations = getChainAccounts(admin_info('dataname'));
		foreach($chainLocations as $location){
			$chain_ids[] = "'". $location['id'] . "'";
    	}
	}

	$category_filter_by = $qinfo['category_filter_by'];
	$category_filter_value = $qinfo['category_filter_value'];
	$category_filter_extra = (isset($qinfo['category_filter_extra']))?$qinfo['category_filter_extra']:"";
	$page_categories_by = (isset($qinfo['page_categories_by']))?$qinfo['page_categories_by']:0;;

	$forward_to = (isset($qinfo['forward_to']))?$qinfo['forward_to']:"";

	$inventory_filter_by 	= (isset($qinfo['inventory_filter_by']))?$qinfo['inventory_filter_by']:"";
	$inventory_filter_value = (isset($qinfo['inventory_filter_value']))?$qinfo['inventory_filter_value']:"";
	$combo_filter_by = (isset($qinfo['combo_filter_by']))?$qinfo['combo_filter_by']:"";
	$combo_filter_value = (isset($qinfo['combo_filter_value']))?$qinfo['combo_filter_value']:"";

	$category_details_column = (isset($qinfo['category_details_column']))?$qinfo['category_details_column']:false;
	$category_send_details = (isset($qinfo['category_send_details']))?$qinfo['category_send_details']:false;

	$cinfo = array();
	$cinfo['details_column'] = $category_details_column;
	$cinfo['send_details'] = $category_send_details;
	$cinfo['category_primefield'] = $category_primefield;

	$send_details = (isset($qinfo['send_details']))?$qinfo['send_details']:false;
	$datasource = (isset($qinfo['datasource']))?$qinfo['datasource']:"database";
	$datafields = (isset($qinfo['datafields']))?$qinfo['datafields']:array();
	$data = (isset($qinfo['data']))?$qinfo['data']:array();

	$category_orderby = (isset($qinfo['category_orderby']))?$qinfo['category_orderby']:$category_primefield;
	$inventory_orderby = (isset($qinfo['inventory_orderby']))?$qinfo['inventory_orderby']:$inventory_primefield;
	if($category_orderby=="_order") $category_allow_reorder = true;
	$inventory_allow_reorder = (substr($inventory_orderby, 0, 6)=="_order")?true:false;

	$qdbfields = get_qdbfields($qfields);
	$use_categories = $qtitle_category;

	$maxInvCount = 0;
	if ($category_tablename == "tax_profiles") $maxInvCount = 5;

	$whole_cat_code_start = "<table cellspacing=0 style='border:solid 2px #777777'><tbody>";

	$whole_cat_code_start .= "<tr><td colspan='2' bgcolor='#444444' class='menuHead'>(cat_row_code)</td></tr>";
	$whole_cat_code_start .= "<tr><td>";
	$whole_cat_code_start .= "<div id='ic_area_cat(cat_count)'>";
	$whole_cat_code_end = "</div>";
	$whole_cat_code_end .= "<div id='ic_cat(cat_count)_addbtn' class='btnAlignment'><div>";
	$whole_cat_code_end .= wizard_button_new("","<span class='addBtn'> + Add $qtitle_item </span>", "onclick='add_new_item((cat_count), ".$maxInvCount.")'",122,30,"#666666");
	$whole_cat_code_end .= "</div><div>";
	global $mode;
	$checking_forced_tab = explode("/", $mode);
	if ($checking_forced_tab[1] == "edit_forced_modifiers" && $location_info['order_type_as_forced_modifier'] == 1) {
		$whole_cat_code_end .= wizard_button_new("order_type","<span class='addBtn'> + Add order type choice </span>", "onclick='addOrderTypeItem((cat_count), ".$maxInvCount.")'",122,30,"#666666");
	}
	$whole_cat_code_end .= "</div></div>";
	$whole_cat_code_end .= "</td></tr>";
	$whole_cat_code_end .= "</tbody></table><br>";
	$whole_cat_code_end .= "<style>
			.btnAlignment div {
				float:left;
			}
	</style>";

	$no_cat_code_start = "<div id='ic_area_cat1'>";
	$no_cat_code_end = "</div>".wizard_button_new("","<span class='addBtn'> + Add $qtitle_item </span>", "onclick='add_new_item(1, ".$maxInvCount.")'",122,30,"#666666");

	require(dirname(__FILE__) . "/dimensional_form_js.php");

	$field_code = "<div id='ic_area'>";
	$cat_count = 0;
	$item_count_list = "0";

	if($use_categories)
	{
			$category_filter_code = "";
			if($category_filter_by!="")
		{
			$cfval = array();
			if(is_array($category_filter_value))
			{
				$cfval = $category_filter_value;
			}
			else
			{
				$cfval[] = $category_filter_value;
			}
			$category_filter_code = " and ";
			if(count($cfval) > 1) $category_filter_code .= "(";
				for($x=0; $x<count($cfval); $x++)
				{
					if($x > 0) $category_filter_code .= " or ";
					$category_filter_code .= "`$category_filter_by`='".str_replace("'","''",$cfval[$x])."'";
				}
				if(count($cfval) > 1) $category_filter_code .= ")";
if($category_filter_extra!="")
{
	$category_filter_code .= " and " . $category_filter_extra;
}
}
$inventory_filter_code = "";
if($inventory_filter_by!="")
{
	$inventory_filter_code = " and `$inventory_filter_by`='".str_replace("'","''",$inventory_filter_value)."'";
}
 if($combo_filter_by!="")
{
	$inventory_filter_code .= " and `combo`='".str_replace("'","''",$combo_filter_value)."'";
}
$limit_code = "";
if ($page_categories_by > 0) {

	$page = urlvar("page");
	if (!$page && sessvar_isset("edfomo_page")) $page = sessvar("edfomo_page");
	else if (!$page) $page = 1;
	if ($page < 1) $page = 1;
	set_sessvar("edfomo_page", $page);

		if(FALSE !== strrpos($category_tablename, "poslavu_MAIN_db")){
			$get_count = mlavu_query("SELECT COUNT(`id`) FROM `$category_tablename` WHERE `_deleted` != '1'" . $category_filter_code);
		}else {
	$get_count = lavu_query("SELECT COUNT(`id`) FROM `$category_tablename` WHERE `_deleted` != '1'".$category_filter_code);
		}
	$total_count = mysqli_result($get_count, 0);

	$limit_code = " LIMIT ".(((int)$page - 1) * $page_categories_by).", ".$page_categories_by;

	echo "<table cellpadding='5' width='500px'><tr>";

	if ($page > 1) echo "<td width='250px' align='left'><a href='$forward_to&page=".($page - 1)."'><< Previous $page_categories_by ".$qtitle_category_plural."</a></td>";
	else echo "<td width='150px'>&nbsp;</td>";

	$next_count = $page_categories_by;
	$show_qtitle = $qtitle_category_plural;
	if ($total_count < (($page + 1) * $page_categories_by)) $next_count = ($total_count % ($page * $page_categories_by));
	if ($next_count == 1) $show_qtitle = $qtitle_category;
	if ($total_count > ($page * $page_categories_by)) echo "<td width='225px' align='right'><a href='$forward_to&page=".($page + 1)."'>"."Next $next_count ".$show_qtitle." >></a></td>";
	else echo "<td width='150px'>&nbsp;</td>";

	echo "</tr></table>";

}

		$master_currencies_in_category_tablename = strpos($category_tablename, "master_currencies");
		if($category_tablename === "MealPlanGroup"){
			$commaChainID = implode(",", $chain_ids);
			$category_query = clavuQuery("select * from $category_tablename where `_deleted`!='1'".$category_filter_code." AND `restaurant_id` IN ($commaChainID) order by `$category_orderby` asc".$limit_code);
		}
		else if($master_currencies_in_category_tablename === false) {
			$category_query = lavu_query("select * from $category_tablename where `_deleted`!='1'".$category_filter_code." order by `$category_orderby` asc".$limit_code);
		}
    while($category_read = mysqli_fetch_assoc($category_query))
    {
    
    	$cat_count++;
    	$setname = $category_read[$category_primefield];
    	$setid = $category_read['id'];
    	$matchfield = $category_read[$category_fieldname];
    
    	$ccode = $whole_cat_code_start . "(inventory)" . $whole_cat_code_end;
    		$ccode = str_replace("(cat_row_code)",cat_row_code($qtitle_category,$cfields,$setname,$setid,$category_read,$cinfo,$inventory_tablename), $ccode);
    	$ccode = str_replace("(cat_count)",$cat_count, $ccode);
    	$ccode = str_replace("(cat)",$cat_count, $ccode);
    
    	$inv_count = 0;
    	$icode = "";
    	if($datasource=="comma delimited" || substr(strtolower($datasource),0,12)=="delimited by")
    	{
    		if(substr(strtolower($datasource),0,12)=="delimited by")
    		{
    			$delimiter = trim(substr($datasource,12));
    		}
    		else
    		{
    			$delimiter = ",";
    		}
    		$parse_rows = explode($delimiter,$category_read[$category_fieldname]);
    		$data = array();
    		for($i=0; $i<count($parse_rows); $i++)
    		{
    			$inv_count++;
    
    			$send_vals = array();
    			$send_vals[$inventory_primefield] = $parse_rows[$i];
    			$item_code = item_row_code($qdbfields, $details_column, $send_details, $inventory_primefield, $field, $send_vals);
    			$item_code = str_replace("(cat)",$cat_count, $item_code);
    			$item_code = str_replace("(item)",$inv_count, $item_code);
    			$icode .= $item_code;
    		}
    	}
    	else
    	{
    		$qvars = array();
    		$inventory_data = array();
    		$inventoryItems = array();
    		$item_ids = array();
    		$qvars['inventory_fieldname'] = $inventory_fieldname;
    		$qvars['inventory_primefield'] = $inventory_primefield;
    		$qvars['inventory_orderby'] = $inventory_orderby;
    		$qvars['matchfield'] = $matchfield;
    		
    		if($inventory_tablename == "MealPlanUser"){
    			$commaChainID = implode(",", $chain_ids);
    			$inventory_query = clavuQuery("select * from $inventory_tablename where _deleted!=1".$inventory_filter_code." and `[inventory_fieldname]`='[matchfield]'".$inventory_select_condition." AND `restaurant_id` IN ($commaChainID) order by [inventory_orderby] asc", $qvars);
    		}
    		else {
    		    $selectPart = "";
    		    if ($inventory_tablename == "forced_modifiers") {
    		       $selectPart = " , IF(ISNULL(`ordertype_id`), 0, 1) as isOrderTag "; 
    		    }
    			$inventory_query = lavu_query("select * $selectPart from $inventory_tablename where _deleted!=1" . $inventory_filter_code . " and `[inventory_fieldname]`='[matchfield]'" . $inventory_select_condition . " order by [inventory_orderby] asc", $qvars);
    		}

    		while($inventory_row = mysqli_fetch_assoc($inventory_query))
    		{
    			$item_ids[] = $inventory_row['id'];
    			$inventory_data[] = $inventory_row;
    		}
    		$itemIds = implode(";", $item_ids);
    		if (sessvar("INVENTORY_MIGRATION_STATUS") == "Completed_Ready" || sessvar("INVENTORY_MIGRATION_STATUS") == "Skipped_Ready")
    		{
    			set_sessvar('inventory_tablename', $inventory_tablename);
    			$inventoryItems = get_poslavu_inventory_items_86count($inventory_tablename, $itemIds);
    		}
    		foreach ($inventory_data as $inventory_read) {
    			$readonly_fields = array();
    			if($qinfo['inventory_tablename'] === 'MealPlanUser') {
    				if(isset($category_read['access_level'])) {
    					if(admin_info("access_level") < $category_read['access_level']) {
    						foreach($qdbfields as $field){
    							$readonly_fields[] = $field[0]; // get the name of the read only field.
    						}
    					}
    				}
    			}
    			if (sessvar("INVENTORY_MIGRATION_STATUS") == "Completed_Ready" || sessvar("INVENTORY_MIGRATION_STATUS") == "Skipped_Ready") {
    				if (array_key_exists($inventory_read['id'], $inventoryItems)) {
    					$ingredientofitems = get_items_86countByItemId($inventoryItems[$inventory_read['id']]);
    					$inventory_read['inventoryItems'] = $ingredientofitems;
    				}	
    			}
    			$inv_count++;
    			$item_code = item_row_code($qdbfields, $details_column, $send_details, $inventory_primefield, $field, $inventory_read, $inventory_allow_reorder, false, $readonly_fields);
    			$item_code = str_replace("(cat)",$cat_count, $item_code);
    			$item_code = str_replace("(item)",$inv_count, $item_code);
    			$icode .= $item_code;
			}

    	}
        $ccode = str_replace("(inventory)",$icode,$ccode);
    	if($item_count_list!="") $item_count_list .= ",";
    	$item_count_list .= $inv_count;
    	$field_code .= $ccode;
    }
}
else
{
	if($datasource=="database")
	{
		$data = array();

		//This is for master exchange rates:
		if($inventory_tablename == '`master_currencies`') {
			$companyId = sessvar("admin_companyid");
			require_once(resource_path()."/chain_functions.php");
			$primaryResIdData = getCompanyInfo($companyId, 'id');
			$primaryResChainId = (!empty($primaryResIdData['chain_id']))?$primaryResIdData['chain_id']:null;
			$primaryResId = null;
			if($primaryResChainId!=null){
				$primaryResId = getPrimaryChainRestaurant($primaryResChainId);
			}

			$inventory_query = mlavu_query("select * from  `poslavu_MAIN_db`.`master_currencies` where `_archived` != 1 AND `master_id`=$primaryResId order by `_enabled` DESC, `country_region_name` ASC");

			while ($inventory_read = mysqli_fetch_assoc($inventory_query)) {
				$cur_row['enabled'] = $inventory_read['_enabled'];
				$cur_row['master_currency_code'] = $inventory_read['country_region_id'];
				$cur_row['archive'] = 0;
				$cur_row['id'] = $inventory_read['id'];
				$data[] = $cur_row;
			}
			//end master exchange rates

		}else if($inventory_tablename == '`exchange_rates`'){

			$companyId = sessvar("admin_companyid");
			$primaryResIdData = getCompanyInfo($companyId, 'id');
			$primaryResChainId = (!empty($primaryResIdData['chain_id']))?$primaryResIdData['chain_id']:null;
			$primaryResId = null;
			if($primaryResChainId!=null){
				$primaryResId = getPrimaryChainRestaurant($primaryResChainId);
			}

			$data = array();

			if($primaryResId != null){
				$inventory_query = mlavu_query("select * from  `poslavu_MAIN_db`.`exchange_rates` where primary_restaurantid=".$primaryResId." AND `_deleted` != 1 ORDER BY `effective_date` DESC, `from_currency_code` ASC, `to_currency_code` ASC");

				while ($inventory_read = mysqli_fetch_assoc($inventory_query)) {
					$date = date_format(date_create($inventory_read['effective_date']),"m/d/y");
					$rate = rtrim($inventory_read['rate'], "0");
					$rate = rtrim($rate, ".");
					$cur_row = array();
					$cur_row['id'] = $inventory_read['id'];
					$cur_row['primary_restaurantid'] = $inventory_read['primary_restaurantid'];
					$cur_row['from_currency_id'] = $inventory_read['from_currency_id'];
					$cur_row['to_currency_id'] = $inventory_read['to_currency_id'];
					$cur_row['from_currency_code'] = getDisplayableCurrencyInfoFromCurrencyId($inventory_read['from_currency_id']);
					$cur_row['to_currency_code'] = getDisplayableCurrencyInfoFromCurrencyId($inventory_read['to_currency_id']);
					$cur_row['rate'] = $rate;
					$cur_row['effective_date'] = $date;
					$cur_row['deleted'] = 0;
					$data[] = $cur_row;
				}
			}
		}else if($inventory_tablename == '`secondary_exchange_rates`'){
		    
		    $companyId = sessvar("admin_companyid");
		    $data = array();
		    $dataname = "poslavu_".sessvar("admin_dataname")."_db";
		    $inventory_query = mlavu_query("select * from  `$dataname`.`secondary_exchange_rates` where `_deleted` = 0 AND `custom` = 0 ORDER BY `effective_date` DESC, `from_currency_code` ASC, `to_currency_code` ASC");
		    
		    while ($inventory_read = mysqli_fetch_assoc($inventory_query)) {
		        $date = date_format(date_create($inventory_read['effective_date']),"m/d/y");
		        $rate = rtrim($inventory_read['rate'], "0");
		        $rate = rtrim($rate, ".");
		        $cur_row = array();
		        $cur_row['id'] = $inventory_read['id'];
		        $cur_row['from_currency_id'] = $inventory_read['from_currency_id'];
		        $cur_row['to_currency_id'] = $inventory_read['to_currency_id'];
		        $cur_row['from_currency_code'] = getDisplayableCurrencyInfoFromCurrencyId($inventory_read['from_currency_id']);
		        $cur_row['to_currency_code'] = getDisplayableCurrencyInfoFromCurrencyId($inventory_read['to_currency_id']);
		        $cur_row['rate'] = $rate;
		        $cur_row['effective_date'] = $date;
		        $cur_row['deleted'] = 0;
		        $data[] = $cur_row;
		    }
		}
		else
		{
			if (isset($qinfo["qsortby_preference"])) {
				if (strrpos($inventory_tablename, "poslavu_MAIN_db") !== false) {
					$inventory_query = mlavu_query("select * from $inventory_tablename where _deleted!=1" . $inventory_select_condition . " order by `[1]`" . $qinfo["qsortby_preference"], $inventory_orderby);
				}
				else if($inventory_tablename == "MealPlanUser"){
					$commaChainID = implode(",", $chain_ids);
					$inventory_query = clavuQuery("select * from $inventory_tablename where _deleted!=1".$inventory_select_condition." AND `restaurant_id` IN ($commaChainID) order by `[1]`".$qinfo["qsortby_preference"], $inventory_orderby);
				}
				else {
					$inventory_query = lavu_query("select * from $inventory_tablename where _deleted!=1".$inventory_select_condition." order by `[1]`".$qinfo["qsortby_preference"], $inventory_orderby);
				}
			}else {
				if (strrpos($inventory_tablename, "poslavu_MAIN_db") !== false) {
					$inventory_query = mlavu_query("select * from $inventory_tablename where _deleted!=1" . $inventory_select_condition . " order by `[1]` asc", $inventory_orderby);
				}
				else if($inventory_tablename == "MealPlanUser"){
                    $commaChainID = implode(",", $chain_ids);
                    $inventory_query = clavuQuery("select * from $inventory_tablename where _deleted!=1".$inventory_select_condition." AND `restaurant_id` IN ($commaChainID) order by `[1]` asc", $inventory_orderby);
                }
                else if($inventory_tablename == "order_tags"){
                	$inventory_query = lavu_query("select * from $inventory_tablename where _deleted!=1".$inventory_select_condition." order by name='Dine In' desc, id asc");
                }
				else {
					$inventory_query = lavu_query("select * from $inventory_tablename where _deleted!=1".$inventory_select_condition." order by `[1]` asc", $inventory_orderby);
				}
			}
			if(isset($inventory_query)) {
				while ($inventory_read = mysqli_fetch_assoc($inventory_query)) {
					if (array_key_exists('effective_date', $inventory_read)) { //Ensure date is in DD/MM/YY Format for re-display.
						$inventory_read['effective_date'] = date_format(date_create($inventory_read['effective_date']), 'm/d/y');
					}
					if (array_key_exists('rate', $inventory_read) && strpos($inventory_read['rate'], '.')) { //Ensure rate removes trailing zeros, and if the last character is a '.' remove that too.
						$inventory_read['rate'] = rtrim(rtrim($inventory_read['rate'], "0"), ".");

					}

					$data[] = $inventory_read;
				}
			}
		}
	}
	else if($datasource=="comma delimited" || substr(strtolower($datasource),0,12)=="delimited by")
	{
		if(substr(strtolower($datasource),0,12)=="delimited by")
		{
			$delimiter = trim(substr($datasource,12));
		}
		else
		{
			$delimiter = ",";
		}
		$parse_rows = explode($delimiter,$data);
		$data = array();
		for($i=0; $i<count($parse_rows); $i++)
		{
			$datarow = array();
			$row_vals = array();
			$parse_row = $parse_rows[$i];
			$parse_cols = explode("(",$parse_row);
				for($c=0; $c<count($parse_cols); $c++)
				{
					$parse_col = $parse_cols[$c];
					$parse_col = explode(")",$parse_col);
					$parse_col = $parse_col[0];
					$row_vals[$c] = $parse_col;
				}
				for($x=0; $x<count($datafields); $x++)
				{
					$datafield = $datafields[$x];
					$datavalue = (isset($row_vals[$x]))?$row_vals[$x]:false;
					$datarow[$datafield] = trim($datavalue);
				}
				$datarow['id'] = ($i + 1);
				$data[] = $datarow;
			}
		}

		$inv_count = 0;
		$icode = "";
		for($i=0; $i<count($data); $i++)
		{
			$inventory_read = $data[$i];
			$inv_count++;

			$undeletable = false;
			$readonly_fields = array();
			if ($inventory_tablename=="order_tags" && (int)$inventory_read['id']<=0) {
				$undeletable = true;
			}

			$item_code = item_row_code($qdbfields, $details_column, $send_details, $inventory_primefield, $field, $inventory_read, $inventory_allow_reorder, $undeletable, $readonly_fields);
			$item_code = str_replace("(cat)",1, $item_code);
			$item_code = str_replace("(item)",$inv_count, $item_code);

			$icode .= $item_code;
		}
		$field_code .= $no_cat_code_start;
		$field_code .= $icode;
		if($inventory_tablename == '`exchange_rates`' || $inventory_tablename == '`secondary_exchange_rates`' ) {
			$field_code .= "<hr>";
		}
		$field_code .= $no_cat_code_end;
		$item_count_list = $inv_count . "," . $inv_count;
	}
	$field_code .= "</div>";
	if($use_categories)
		$field_code .= wizard_button_new("","<span class='plus'> + </span> Add ".$qtitle_category,"onclick='add_new_category()'",144,30,"#000067");
	
	echo "<script language='javascript'>";
	echo "cat_count = ".$cat_count."; ";
	echo "item_count = new Array($item_count_list); ";
	echo "</script>";

	return $field_code;
}

function delete_poslavu_dbrow($tablename, $send_id)
{
    if($send_id!="")
	{
		global $section;
		global $mode;
		CPActionTracker::track_action($section . '/' . $mode, 'deleted_' . $tablename);
		if ($tablename =='combo_items') {
			$combo_query = lavu_query("SELECT DISTINCT `combo_menu_id` as combo_id from `combo_items` where `menu_item_id` = $send_id And `_deleted` = 0");
			$combo_menu_id = "'".$send_id."',";
			while ($combo_menu_ids = mysqli_fetch_assoc($combo_query)) {
				$combo_menu_id.= "'".$combo_menu_ids['combo_id']."',";
			}
			$combo_menu_id = trim($combo_menu_id, ",");
			$query = "update $tablename set `_deleted`=1 where `combo_menu_id` in ($combo_menu_id)";
		} else {
			$query = "update $tablename set `_deleted`=1 where id=[1]";
		}
		lavu_query($query, $send_id);
	}
}

function update_poslavu_dbrow($tablename,$fields, $id)
{
		global $data_name;
		$insert_cols = "";
    $insert_vals = "";
    $update_code = "";
    $deleteIng = "";
    if ($tablename == "menu_categories" || $tablename == "menu_items") $update_code = "`last_modified_date`=now()";
    $a_field_update_list = array();
    $a_field_display_names = array();
    for ($i = 0; $i < count($fields); $i++) {
        $col = $fields[$i][0];
        $val = str_replace("'", "''", $fields[$i][1]);
        //LP-4492 update the decimal character at tax profile page START
        if($tablename=="tax_rates" && $col =="calc"){
            $val=str_replace(',','.',$val);
        }
        //LP-4492 update the decimal character at tax profile page START

		if(($tablename=="tax_rates" && $col =="calc")||($tablename=="menu_items" && $col =="happyhour")){
            $val=str_replace(',','.',$val);
        }

        if ((sessvar("INVENTORY_MIGRATION_STATUS") == "Completed_Ready" || sessvar("INVENTORY_MIGRATION_STATUS") == "Skipped_Ready") && $col == 'ingredients') {
  	     /**
            * To Check whether ingredient is deleted or not
   	     **/
            if (strpos($val, 'xx') !== false) {
    			$deleteIng = 1;
	     }
            $val = str_replace("'", "''", $fields[$i][1]);
            $ingredientsUnits = explode(' xx ', $val);
            $ingredients = $val = $ingredientsUnits[0];
            $units = $ingredientsUnits[1];
        }

        if (isset($fields[$i][2]))
            $s_title = $fields[$i][2];
        else
            $s_title = '';

        if ((sessvar("INVENTORY_MIGRATION_STATUS") == "Completed_Ready" || sessvar("INVENTORY_MIGRATION_STATUS") == "Skipped_Ready")&& $col == 'ingredients') {
            continue;
        }

        if ($insert_cols != "") $insert_cols .= ", ";
        if ($insert_vals != "") $insert_vals .= ", ";
        if ($update_code != "") $update_code .= ", ";
        $insert_cols .= "`$col`";
        $insert_vals .= "'$val'";
        $update_code .= "`$col`='$val'";
        $a_field_update_list[$col] = $val;
        $a_field_display_names[$col] = $s_title;
    }

	if ($data_name == '17edison') {
		if ($id === "")
			$id = NULL;
		$newid = log_poslavu_dbrow_update($tablename, $id, $a_field_update_list, $a_field_display_names, $data_name);
	} else {

		if($id!="")
		{
			$query = "update $tablename set $update_code where id=[1]";
			lavu_query($query, $id);
			$newid = $id;
		}
		else
		{
		    $query = "insert into $tablename ($insert_cols) values ($insert_vals)";
			lavu_query($query);
			$error = lavu_dberror();
			if( $error ){
				error_log( __FILE__ . ':' . __LINE__ . '|' . $error );
			}

			$newid = lavu_insert_id();
		}
        //TODO:- Removed this if else condition after full migration implementation
        if (sessvar("INVENTORY_MIGRATION_STATUS") == "Completed_Ready" || sessvar("INVENTORY_MIGRATION_STATUS") == "Skipped_Ready") {
            // Call 86Count table to update and insert records
            if(isset($ingredients)){
            	/**
            	* Added New delete Ingredient Flag to rectify actual delete for item rather than perform delete ingredient for all the
            	  items 
            	**/
                update_poslavu_86count($tablename, $newid, $ingredients, $units, $deleteIng);
            }
        }
	}
    return $newid;
}
