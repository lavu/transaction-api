<?php

class LoyalTree
{
	private static  $dev_key    = '5ubvjs85js7v7vusarfd';
	private static  $version    = "3.0";
	private static  $dev_name   = "poslavu";
	private static  $base_url   = "https://loyaltree.com/pos/poslavu/";
	private static  $signup_url = 'signup.php';
	private static  $username   = '';
	private static  $password   = '';
	private static  $menu_id    = '';
	private static  $multiplier = '72990';

	public static function getItems($menu_id)
	{
		$items = lavu_query("SELECT i.`id`, i.`name`, i.`category_id`, i.`description` FROM `menu_items` i, `menu_categories` c WHERE i.category_id = c.id AND i.menu_id = '[1]' AND c._deleted != 1 AND i._deleted != 1", $menu_id);
		echo lavu_dberror();
		$return = array();
		while ($item = mysqli_fetch_assoc($items)){
			$return[] = $item;
		}
		return $return;
	}

	public static function getItem($id)
	{
		$items = lavu_query("SELECT `id`,`category_id`,`name`,`description`,`price` FROM `menu_items` WHERE id = $id");
		echo lavu_dberror();
		while ($item = mysqli_fetch_assoc($items)){
			return $item;
		}
	}

	public static function getCats($menu_id)
	{
		$cats = lavu_query("SELECT `id`,`name`,`description` FROM `menu_categories` WHERE _deleted != 1 AND `menu_id` = '[1]'", $menu_id);
		echo lavu_dberror();
		$return = array();
		while ($cat = mysqli_fetch_assoc($cats)){
			$return[] = $cat;
		}
		return $return;
	}

	public static function getLocations($companyid)
	{
		$locs = lavu_query("SELECT `id`,`title`,`phone`,`address`,`city`,`state`,`zip`,`country` FROM `locations`");
		$l = 0;
		$result = array();
		while ($info = mysqli_fetch_assoc($locs)) {	
			$result[$l] = array();
			$result[$l]['storeid'] = $companyid.$info['id'];
			$result[$l]['name']    = $info['title'];
			$result[$l]['phone']   = $info['phone'];
			$result[$l]['street']  = $info['address'];;
			$result[$l]['city']    = $info['city'];
			$result[$l]['state']   = $info['state'];
			$result[$l]['code']    = $info['zip'];
			$result[$l]['country'] = $info['country'];
			$l++;
		}
		return $result;
	}

	public static function xmlencode(&$array)
	{
		foreach ($array as $k => $array_part)
		{
			if (is_array($array_part))
			{
				self::xmlencode($array[$k]);
			}
			else if (is_string($array_part))
			{
				$array[$k] = htmlspecialchars($array_part);
			}
		}
	}

	public static function preload($data, $categories, $items)
	{
		self::xmlencode($data);
		self::xmlencode($categories);
		self::xmlencode($items);

		$sxe = new SimpleXMLElement('<?xml version="1.0" encoding="UTF-8"?><purchase/>');

		$sxe->addChild('devid', LoyalTree::$dev_key);
		$sxe->addChild('version', LoyalTree::$version);
		$sxe->addChild('businessid', $data['businessid']);
		$sxe->addChild('chainid', $data['chain_id']);
		$sxe->addChild('storeid', $data['storeid']);

		$cats = $sxe->addChild('cats');
		foreach ($categories as $category)
		{
			$x_cat = $cats->addChild('cat');
			$x_cat->catid = $category['id'];
			$x_cat->description = $category['name'];
			$x_cat->description2 = $category['description'];
		}

		$x_items = $sxe->addChild('items');
		foreach ($items as $item)
		{
			$x_item = $x_items->addChild('item');
			$x_item->itemid = $item['id'];
			$x_item->catid = $item['category_id'];
			$x_item->sku = $item['id'];
			$x_item->description = $item['name'];
			$x_item->description2 = $item['description'];
		}

		$xml = simplexml_load_string(LoyalTree::curl_query(LoyalTree::$base_url."preload.php",$sxe->asXML()));

		return $xml;
	}

	private static function paymentIsCard($payment)
	{
		if (!empty($payment['pay_type_id']))
		{
			return ((int)$payment['pay_type_id'] == 2);
		}

		if (!empty($payment['type']))
		{
			return ($payment['type'] == "Card");
		}

		return FALSE;
	}

	public static function purchase($data, $items, $payments)
	{
		$sxe = new SimpleXMLElement('<?xml version="1.0" encoding="UTF-8"?><purchase/>');

		$sxe->addChild('devid', LoyalTree::$dev_key);
		$sxe->addChild('version', LoyalTree::$version);
		$sxe->addChild('businessid', $data['businessid']);
		$sxe->addChild('chainid', $data['chainid']);
		$sxe->addChild('storeid', $data['storeid']); 
		$sxe->addChild('serverid', $data['serverid']);
		$sxe->addChild('table', $data['table']);
		$sxe->addChild('guest', $data['guest']);
		$sxe->addChild('transactionid', $data['transactionid']);
		$sxe->addChild('totalamount', $data['totalamount']);

		$x_items = $sxe->addChild('items');
		foreach ($items as $item)
		{
			$x_item = $x_items->addChild('item');
			$x_item->addChild('itemid', $item['id']);
			$x_item->addChild('catid', $item['category_id']);
			$x_item->addChild('qty', $item['quantity']);
			$x_item->addChild('itemamount', $item['price']);
			$x_item->addChild('modifieramount', $item['modifiers']);
			$x_item->addChild('discountamount', $item['discount']);
			$x_item->addChild('taxincludedamount', $item['itax']);
			$x_item->addChild('ordertime', self::getUTCforDateTime($item['time'], $data['time_zone']));
		}

		$x_pays = $sxe->addChild('payments');
		foreach ($payments as $pay)
		{
			$x_pay = $x_pays->addChild('payment');
			$x_pay->addChild('paymentid', $pay['paymentid']);
			$x_pay->addChild('amount', $pay['amount']);
			$x_pay->addChild('void', $pay['void']);
			$x_pay->addChild('type', $pay['type']);
			$x_pay->addChild('paymenttime', self::getUTCforDateTime($pay['paymenttime'], $data['time_zone']));
			$x_pay->addChild('current', $pay['current']);

			if (LoyalTree::paymentIsCard($pay))
			{
				if (isset($pay['card_desc']) && !empty($pay['card_desc']))
				{
					$x_pay->addChild('lastfour',crypt($pay['card_desc'], 'Loyaltree_salt_8923782'));
				}
				else
				{
					$x_pay->addChild('lastfour',"");
				}

				if (isset($pay['first_four']) && !empty($pay['first_four']))
				{
					$x_pay->addChild('firstfour',crypt($pay['first_four'], 'Loyaltree_salt_951548623'));
				}
				else
				{
					$x_pay->addChild('firstfour',"");
				}

				$first_name = " ";
				$last_name = " ";
				if (isset($pay['info']))
				{
					$first_last = explode(" ",trim($pay['info']));	
					if (count($first_last) > 1)
					{
						$first_name = $first_last[0];
						$last_name = $first_last[count($first_last)-1];
						if (!$last_name)
						{
							$last_name = " ";
						}
					}
				}
				$x_pay->addChild('firstname', $first_name);
				$x_pay->addChild('lastname', $last_name);
			}
		}

		$response = LoyalTree::curl_query(LoyalTree::$base_url."qrcode.php",$sxe->asXML());	

		if (empty($response))
		{
			return '{"json_status":"error","error":"empty response"}';
		}

		$xml = simplexml_load_string($response);
		if ($xml->error)
		{
			return '{"json_status":"error","error":"'.$xml->error.'"}';
		}
		else
		{
			return '{"json_status":"success","version":"'.$xml->version.'","text":"'.$xml->text.'","qrstring":"'.$xml->qrstring.'"}';
		}
	}

	public static function email_receipt($data, $items, $payments)
	{
		$sxe = new SimpleXMLElement('<?xml version="1.0" encoding="UTF-8"?><emailreceipt/>');

		$sxe->addChild('devid', LoyalTree::$dev_key);
		$sxe->addChild('version', LoyalTree::$version);
		$sxe->addChild('businessid', $data['businessid']);
		$sxe->addChild('chainid', $data['chainid']);
		$sxe->addChild('storeid', $data['storeid']); 
		$sxe->addChild('serverid', $data['serverid']);
		$sxe->addChild('table', $data['table']);
		$sxe->addChild('guest', $data['guest']);
		$sxe->addChild('transactionid', $data['transactionid']);
		$sxe->addChild('totalamount', $data['totalamount']);

		$x_items = $sxe->addChild('items');
		foreach ($items as $item)
		{
			$x_item = $x_items->addChild('item');
			$x_item->addChild('itemid', $item['id']);
			$x_item->addChild('catid', $item['category_id']);
			$x_item->addChild('qty', $item['quantity']);
			$x_item->addChild('itemamount', $item['price']);
			$x_item->addChild('modifieramount', $item['modifiers']);
			$x_item->addChild('discountamount', $item['discount']);
			$x_item->addChild('taxincludedamount', $item['itax']);
			$x_item->addChild('ordertime', self::getUTCforDateTime($pay['paymenttime'], $data['time_zone']));
		}

		$x_pays = $sxe->addChild('payments');
		foreach ($payments as $pay)
		{
			$x_pay = $x_pays->addChild('payment');
			$x_pay->addChild('useremail', $data['email']);
			$x_pay->addChild('paymentid', $pay['paymentid']);
			$x_pay->addChild('amount', $pay['amount']);
			$x_pay->addChild('void', $pay['void']);
			$x_pay->addChild('type', $pay['type']);
			$x_pay->addChild('paymenttime', self::getUTCforDateTime($pay['paymenttime'], $data['time_zone']));
			$x_pay->addChild('current', $pay['current']);

			if (LoyalTree::paymentIsCard($pay))
			{
				if (isset($pay['card_desc']) && !empty($pay['card_desc']))
				{
					$x_pay->addChild('lastfour', crypt($pay['card_desc'], 'Loyaltree_salt_8923782'));
				}
				else
				{
					$x_pay->addChild('lastfour', "");
				}

				if (isset($pay['first_four']) && !empty($pay['first_four']))
				{
					$x_pay->addChild('firstfour', crypt($pay['first_four'], 'Loyaltree_salt_951548623'));
				}
				else
				{
					$x_pay->addChild('firstfour', "");
				}

				$first_name = " ";
				$last_name = " ";
				if (isset($pay['info']))
				{
					$first_last = explode(" ", $pay['info']);
					if (count($first_last) > 1)
					{
						$first_name = $first_last[0];
						$last_name  = $first_last[count($first_last)-1];
					}
				}

				$x_pay->addChild('firstname',$first_name);
				$x_pay->addChild('lastname', $last_name );
			}		
		}

		$response = LoyalTree::curl_query(LoyalTree::$base_url."qrcode.php", $sxe->asXML());
		if (empty($response))
		{
			return '{"json_status":"error","error":"empty response"}';
		}

		$xml = simplexml_load_string($response);
		if ($xml->error)
		{
			return '{"json_status":"error","error":"'.$xml->error.'"}';
		}
		else
		{
			return '{"json_status":"success","version":"'.$xml->version.'","text":"'.$xml->text.'","qrstring":"'.$xml->qrstring.'"}';	
		}
	}

	public static function redeem($data)
	{
		$sxe = new SimpleXMLElement('<?xml version="1.0" encoding="UTF-8"?><redemption/>');

		$sxe->addChild('devid', LoyalTree::$dev_key);
		$sxe->addChild('version', LoyalTree::$version);
		$sxe->addChild('businessid', $data['businessid']);
		$sxe->addChild('chainid', $data['chainid']);
		$sxe->addChild('storeid', $data['storeid']);
		$sxe->addChild('serverid', $data['serverid']);
		$sxe->addChild('redeemcode', $data['redeemcode']);
		$sxe->addChild('transactionid', $data['transactionid']);

		$xml = simplexml_load_string(LoyalTree::curl_query(LoyalTree::$base_url."redeem.php", $sxe->asXML()));
		if ($xml->error)
		{
			echo '{"json_status":"error","error":"'.$xml->error.'"}';
		}
		else
		{
			$ret_value= '{"json_status":"success","id":"'.$xml->id.'", "percent":"'.$xml->percent.'","isdeal":"'.$xml->isdeal.'","amount":"'.$xml->amount.'","highestValue":"'.$xml->highestValue.'","items":[';
			$item_arr = array();

			foreach ($xml->items as $item)
			{
				foreach ($item as $i)
				{
					$item_arr[]= '{"catid":"'.$i->catid.'","itemid":"'.$i->itemid.'"}';
				}
			}
			$ret_value .= implode(",", $item_arr);
			$ret_value .= ']}';

			echo $ret_value;
			return $ret_value;
		}
	}

	public static function reinstate($data)
	{
		$sxe = new SimpleXMLElement('<?xml version="1.0" encoding="UTF-8"?><reinstate/>');

		$sxe->addChild('devid', LoyalTree::$dev_key);
		$sxe->addChild('version', LoyalTree::$version);
		$sxe->addChild('businessid', $data['businessid']);
		$sxe->addChild('chainid', $data['chainid']);
		$sxe->addChild('storeid', $data['storeid']);
		$sxe->addChild('serverid', $data['serverid']);
		$sxe->addChild('id', $data['redemptionid']);
		$sxe->addChild('isdeal', $data['isdeal']);
		$sxe->addChild('transactionid', $data['transactionid']);
		$sxe->addChild('reason', $data['reason']);

		$xml = simplexml_load_string(LoyalTree::curl_query(LoyalTree::$base_url."redeem_reinstate.php", $sxe->asXML()));

		if ($xml->error)
		{
			$response = '{"json_status":"error","error":"'.$xml->error.'"}';
		}
		else
		{
			$response = '{"json_status":"success"}';
		}

		if (isset($data['no_echo']))
		{
			return $response;
		}
		else
		{
			echo $response;
		}
	}

	public static function confirm_redeem($data) // no longer will be used - 2012-08-20
	{
		$sxe = new SimpleXMLElement('<?xml version="1.0" encoding="UTF-8"?><redemption/>');

		$sxe->addChild('devid', LoyalTree::$dev_key);
		$sxe->addChild('version', LoyalTree::$version);
		$sxe->addChild('businessid', $data['businessid']);
		$sxe->addChild('chainid', $data['chainid']);
		$sxe->addChild('redeemed', $data['redeemed']);
		$sxe->addChild('id', $data['id']);

		$response = LoyalTree::curl_query(LoyalTree::$base_url."redeem_response.php", $sxe->asXML());
		// decide what to do in case it fails
	}

	public static function signup($rest_id, $chain, $name, $locations, $categories, $items, $do_update)
	{
		$sxe = new SimpleXMLElement('<?xml version="1.0" encoding="UTF-8"?><signup/>');

		if ($chain == "")
		{
			$chain="0";
		}

		$sxe->addChild('devid', LoyalTree::$dev_key);
		$sxe->addChild('version', LoyalTree::$version);
		$sxe->addChild('businessid', $rest_id);
		$sxe->addChild('chainid', $chain);
		$sxe->addChild('retailername', $name);

		$stores = $sxe->addChild('stores');
		foreach ($locations as $location)
		{
			$x_store = $stores->addChild('store');
			$x_store->storeid = $location['storeid']; 
			$x_store->name = $location['name'];
			$x_store->phone = $location['phone'];
			$x_store->street = $location['street'];
			$x_store->city = $location['city'];
			$x_store->state = $location['state'];
			$x_store->code = $location['code'];
			$x_store->country = $location['country'];
		}

		$menu = $sxe->addChild('menu');

		$cats = $menu->addChild('cats');
		foreach ($categories as $category)
		{
			$x_cat = $cats->addChild('cat');
			$x_cat->catid = $category['id'];
			$x_cat->description = $category['name'];
			$x_cat->description2 = $category['description'];
		}

		$x_items = $menu->addChild('items');
		foreach ($items as $item)
		{
			$x_item = $x_items->addChild('item');
			$x_item->itemid = $item['id'];
			$x_item->catid = $item['category_id'];
			$x_item->sku = $item['id'];
			$x_item->description = $item['name'];
			$x_item->description2 = $item['description'];
		}

		$xml = simplexml_load_string(LoyalTree::curl_query(LoyalTree::$base_url.LoyalTree::$signup_url, $sxe->asXML()));
		if ($do_update)
		{
			LoyalTree::update_fields();
		}

		return $xml;
	}

	public static function update_chain($data)
	{
		if (isset($data['chainid']['id']))
		{
			$chain_id = $data['chainid']['id'];
		}
		else
		{
			$chain_id = $data['chainid'];
		}

		$sxe = new SimpleXMLElement('<?xml version="1.0" encoding="UTF-8"?><update/>');

		$sxe->addChild('devid', LoyalTree::$dev_key);
		$sxe->addChild('version', LoyalTree::$version);
		$sxe->addChild('businessid', $data['businessid']);
		$sxe->addChild('chainid', $chain_id);

		if ($xml->error)
		{
			$response = '{"json_status":"error","error":"'.$xml->error.'"}';
		}
		else
		{
			$response = '{"json_status":"success"}';
			$dn_arr= mysqli_fetch_assoc(mlavu_query("select * from `poslavu_MAIN_db`.`restaurants` where `id`='[1]'",$data['businessid'] ));
			lavu_connect_dn($dn_arr['data_name'],'poslavu_'.$dn_arr['data_name'].'_db');
			if(mysqli_num_rows(lavu_query("SELECT * FROM `poslavu_[1]_db`.`config` where `setting`='loyaltree_chain_id'", $dn_arr['data_name']))){
				lavu_query("UPDATE `poslavu_[1]_db`.`config` SET `value`='[2]' where `setting`='loyaltree_chain_id' limit 1",$dn_arr['data_name'],$chain_id );
			}else{
				lavu_query("INSERT INTO `poslavu_[1]_db`.`config` (`location`, `setting`, `value`, `type`) VALUES('[2]', 'loyaltree_chain_id', '[3]', 'location_config_setting')", $dn_arr['data_name'], admin_info('loc_id'), $chain_id);
			}
		}

		return $xml;
	}	

	public static function curl_query($url, $post)
	{
		$start_time = microtime(true);

		LoyalTree::loyalTreeDebug("start request - ".$post);

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
		curl_setopt($ch, CURLOPT_HEADER, 0);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 10);
		curl_setopt($ch, CURLOPT_TIMEOUT, 10);
		$data = curl_exec($ch);
		curl_close($ch);

		$end_time = microtime(true);
		$process_time = round((($end_time - $start_time) * 1000), 2) . "ms";

		LoyalTree::loyalTreeDebug("end request - duration: ".$process_time);

		return $data;
	}

	private static function update_fields() // this function does the db modifications upon signup to enable loyaltree completely
	{
		lavu_query("INSERT INTO `discount_types` (`loc_id`, `title`, `label`, `code`,`special`) VALUES ('[1]', 'LoyalTree Discount', 'LoyalTree Discount', 'p','loyaltree')", admin_info('loc_id'));
		lavu_query("UPDATE `locations` SET `ask4email_at_checkout` = '1' AND `default_receipt_print` = '1'");
		lavu_query("UPDATE `config` SET `value_9` = '3' WHERE `setting` = 'receipt' LIMIT 1");
	}

	public static function getUTCforDateTime($datetime, $timezone)
	{
		$old_tz = date_default_timezone_get();

		if (!empty($timezone))
		{
			date_default_timezone_set($timezone);
		}

		$dt = strtotime($datetime);
		date_default_timezone_set('UTC');	
		$rtn_dt = date("Y-m-d G:i:s", $dt);

		date_default_timezone_set($old_tz);

		return $rtn_dt;
	}

	public static function loyalTreeDebug($msg)
	{
		global $loyaltree_debug;
		global $data_name;

		if ($loyaltree_debug) error_log("LoyalTree - ".$data_name." - req_id: ".urlvar("YIG")." - ".$msg);
	}
}

?>
