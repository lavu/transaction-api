<?php
require_once('language_dictionary.php');
require_once('LavuConstants.php');

global $__LAVU_CORE_FUNCTIONS_H__;

if (!isset($__LAVU_CORE_FUNCTIONS_H__))
{
	$__LAVU_CORE_FUNCTIONS_H__ = 1;

	if( ! function_exists('boolval'))
	{
			/**
			 * Get the boolean value of a variable
			 *
			 * @param mixed The scalar value being converted to a boolean.
			 * @return boolean The boolean value of var.
			 */
			function boolval($var)
			{
					return !! $var;
			}
}

	function lsecurity_key1($id)
	{
		return (($id * 87) + 47283);
	}

	function lsecurity_id1($key)
	{
		return (($key - 47283) / 87);
	}

	function lsecurity_key($id)
	{
		$key_query = mlavu_query("select AES_DECRYPT(jkey,'".integration_keystr2()."') as `key2` from `poslavu_MAIN_db`.`restaurants` where `id`='[1]'",$id);
		if(mysqli_num_rows($key_query))
		{
			$key_read = mysqli_fetch_assoc($key_query);
			return $key_read['key2'];
		}
		else return "";
	}

	function lsecurity_key_by_dn($dn)
	{
		$key_query = mlavu_query("select AES_DECRYPT(jkey,'".integration_keystr2()."') as `key2` from `poslavu_MAIN_db`.`restaurants` where `data_name`='[1]'",$dn);
		if(mysqli_num_rows($key_query))
		{
			$key_read = mysqli_fetch_assoc($key_query);
			return $key_read['key2'];
		}
		else return "";
	}

	function lsecurity_id($key,$data_name="")
	{
		if (sessvar("ID4".$key))
		{
			return sessvar("ID4".$key);
		}
		else if($data_name!="")
		{
			$letter = substr($data_name,0,1);
			if(ord($letter) >= ord("a") && ord($letter) <= ord("z"))
				$letter = $letter;
			else
				$letter = "OTHER";
			$tablename = "rest_" . $letter;

			$body = "select `id` from `poslavu_MAINREST_db`.`$tablename` where `jkey`!='' and AES_DECRYPT(jkey,'".integration_keystr2()."')='".$key."'";
			//mail("corey@poslavu.com","query",$body,"From: info@poslavu.com");
			$id_query = mlavu_query("select `companyid` as `id` from `poslavu_MAINREST_db`.`$tablename` where `jkey`!='' and AES_DECRYPT(jkey,'".integration_keystr2()."')='[1]'",$key);
			if(mysqli_num_rows($id_query))
			{
				$id_read = mysqli_fetch_assoc($id_query);
				set_sessvar("ID4".$key, $id_read['id']);
				return $id_read['id'];
			}
			else return false;
		}
		else
		{
			$body = "select `id` from `poslavu_MAIN_db`.`restaurants` where `jkey`!='' and AES_DECRYPT(jkey,'".integration_keystr2()."')='".$key."'";
			//mail("corey@poslavu.com","query",$body,"From: info@poslavu.com");
			$id_query = mlavu_query("select `id` from `poslavu_MAIN_db`.`restaurants` where `jkey`!='' and AES_DECRYPT(jkey,'".integration_keystr2()."')='[1]'",$key);
			if(mysqli_num_rows($id_query))
			{
				$id_read = mysqli_fetch_assoc($id_query);
				set_sessvar("ID4".$key, $id_read['id']);
				return $id_read['id'];
			}
			else return false;
		}
	}

	function old_lsecurity_id($key)
	{
		$body = "select `id` from `poslavu_MAIN_db`.`restaurants` where `jkey`!='' and AES_DECRYPT(jkey,'".integration_keystr2()."')='".$key."'";
		//mail("corey@poslavu.com","query",$body,"From: info@poslavu.com");

		$id_query = mlavu_query("select `id` from `poslavu_MAIN_db`.`restaurants` where `jkey`!='' and AES_DECRYPT(jkey,'".integration_keystr2()."')='[1]'",$key);
		if(mysqli_num_rows($id_query))
		{
			$id_read = mysqli_fetch_assoc($id_query);
			return $id_read['id'];
		}
		else return false;
	}

	function lsecurity_pass($cc)
	{
		global $build_number;

		$cc_parts = explode(":", $cc);

		if (isset($cc_parts[1])) {
			$build_number = $cc_parts[1];
		}

		$ccrt = array();
		$ccrt['cc'] = $cc_parts[0];
		$ccrt['key'] = "";

		$cc = explode("_key_",$cc_parts[0]);
		$count = count($cc);
		if($count > 1)
		{
			if ($count > 2) {
				$cc[0] = join("_key_", array_slice($cc, 0, -1));
				$cc[1] = $cc[($count - 1)];
			}
			$ccrt['cc'] = trim($cc[0]);
			$ccrt['key'] = trim($cc[1]);
		}
		else
		{
		}
		return $ccrt;
	}

	function lsecurity_name($cc,$id)
	{
		//return $cc;
		return $cc . "_key_" . lsecurity_key($id);
	}

	function integration_keystr()
	{
		return "AIH3476ag7gaGAn84nbaZZZ48hah";
	}

	function integration_keystr2()
	{
		return "a7gb8a4agfSFDfna4igsoihq4oz";
	}


	$lavu_mail_path = dirname(__FILE__) . "/LavuMail.php";
	if(file_exists($lavu_mail_path)){
		require_once($lavu_mail_path);
	}
	else {
		//error_log("Lavu Mail Not Found!");
	}
	$gateway_log_path = "/home/poslavu/logs/company/";

	function resource_path()
	{
		return dirname(__FILE__);
	}

	function display_header($str)
	{
		echo "<div style='font-size:18px; color:#ffffff; background-color:#acacac; font-weight:bold; width:360px; text-align:center'>$str</div>";
	}

	//------------ time
	function display_date($d)
	{
		$dt = explode("-",$d);
		if(count($dt) > 2)
		{
			return $dt[1] . "/" . $dt[2] . "/" . $dt[0];
		}
		else return $d;
	}

	function display_time($t)
	{
		$m = $t % 100;
		$h = (($t - $m) / 100);
		if($m < 10) $m = "0" . ($m * 1);
		$a = "am";
		if($h==0 || $h==24)
		{
			$h = 12;
		}
		else if($h==12)
		{
			$a = "pm";
		}
		else if($h>24)
		{
			$h -= 24;
			$a = "am";
		}
		else if($h>12)
		{
			$h -= 12;
			$a = "pm";
		}
		return ($h . ":" . $m . " " . $a);
	}

	// ---- syncing
	function schedule_remote_to_local_sync($locationid,$sync_setting,$sync_value,$sync_value_long="")
	{
		$exist_query = lavu_query("select * from `config` where `setting`='[1]' and `location`='[2]' and `value`='[3]' and `value_long`='[4]'",$sync_setting,$locationid,$sync_value,$sync_value_long);
		if(mysqli_num_rows($exist_query))
		{
		}
		else
		{
			lavu_query("insert into `config` (`setting`,`location`,`value`,`value_long`,`type`) values('[1]','[2]','[3]','[4]','log')", $sync_setting, $locationid, $sync_value, $sync_value_long);
		}
	}

	function schedule_remote_to_local_sync_if_lls($location_info, $locationid, $sync_setting, $sync_value, $sync_value_long="")
	{
		if (isLLS($location_info))
		{
			schedule_remote_to_local_sync($locationid, $sync_setting, $sync_value, $sync_value_long);
		}
	}

	//--------------------------------- session / login functions -------------------------//
	function sessname($str)
	{
		return "poslavu_234347273_" . $str;
	}

	function sessvar($str,$def=false)
	{
		if(isset($_SESSION[sessname($str)]))
			return $_SESSION[sessname($str)];
		else
			return $def;
	}

	function sessvar_isset($str)
	{
		return isset($_SESSION[sessname($str)]);
	}

	function set_sessvar($str,$val)
	{
		$_SESSION[sessname($str)] = $val;
	}

	function unset_sessvar($str, $val)
	{
		unset($_SESSION[sessname($str)]);
	}

	function admin_loggedin()
	{
		return sessvar("admin_loggedin");
	}

	function admin_login($dataname,$id,$username="",$fullname="",$email="",$companyid="",$version_number="",$dev="",$access_level="",$company_name="",$phone="",$loc_id="0",$lavu_admin="0",$chain_id="0")
	{
		set_sessvar("admin_dataname",$dataname);
		set_sessvar("admin_database","poslavu_".$dataname."_db");
		set_sessvar("admin_loggedin",$id);
		set_sessvar("admin_username",$username);
		set_sessvar("admin_fullname",$fullname);
		set_sessvar("admin_email",$email);
		set_sessvar("admin_companyid",$companyid);
		set_sessvar("admin_chain_id",$chain_id);
		set_sessvar("admin_version",$version_number);
		set_sessvar("admin_dev",$dev);
		set_sessvar("admin_access_level",$access_level);
		set_sessvar("admin_company_name",$company_name);
		set_sessvar("admin_phone",$phone);
		set_sessvar("admin_loc_id",$loc_id);
		set_sessvar("admin_lavu_admin",$lavu_admin);
		set_sessvar("admin_root_companyid",$companyid);
		set_sessvar("admin_root_dataname",$dataname);

		$l_vars = array();
		$q_fields = "";
		$q_values = "";
		$l_vars['action'] = "Logged in to back end";
		$l_vars['user'] = $username;
		$l_vars['user_id'] = $id;
		$l_vars['ipaddress'] = $_SERVER['REMOTE_ADDR'];
		$l_vars['order_id'] = "0";
		$l_vars['data'] = $fullname;
		$l_vars['detail1'] = "Access level: ".$access_level;
		$l_vars['loc_id'] = $loc_id;
		$keys = array_keys($l_vars);
		foreach ($keys as $key) {
			if ($q_fields != "") { $q_fields .= ", "; $q_values .= ", "; }
			$q_fields .= "`$key`";
			$q_values .= "'[$key]'";
		}
		$l_vars['dataname'] = $dataname;
		lavu_connect_dn($l_vars['dataname'],'poslavu_'.$l_vars['dataname'].'_db');
		if($dataname!= ""){
		$log_this = lavu_query("INSERT INTO `poslavu_[dataname]_db`.`admin_action_log` ($q_fields, `server_time`) VALUES ($q_values, now())", $l_vars);
	}
		if ($lavu_admin != "1") mlavu_query("UPDATE `poslavu_MAIN_db`.`restaurants` SET `last_activity` = now() WHERE `id` = '[1]'", $companyid);

		// set cookie for notifications
		setcookie("isLoggedIn", 1);
	}

	function admin_logout()
	{
		$l_vars = array();
		$q_fields = "";
		$q_values = "";
		$l_vars['action'] = "Logged out of back end";
		$l_vars['user'] = sessvar("admin_username");
		$l_vars['user_id'] = sessvar("admin_loggedin");
		$l_vars['ipaddress'] = $_SERVER['REMOTE_ADDR'];
		$l_vars['order_id'] = "0";
		$l_vars['data'] = sessvar("admin_fullname");
		$l_vars['detail1'] = "Access level: ".sessvar("admin_access_level");
		$l_vars['loc_id'] = sessvar("admin_loc_id");
		$keys = array_keys($l_vars);
		foreach ($keys as $key) {
			if ($q_fields != "") { $q_fields .= ", "; $q_values .= ", "; }
			$q_fields .= "`$key`";
			$q_values .= "'[$key]'";
		}
		$l_vars['dataname'] = sessvar("admin_dataname");
		//lavu_connect_dn($l_vars['dataname'],'poslavu_'.$l_vars['dataname'].'_db');
		if($l_vars['dataname']!=""){
		$log_this = lavu_query("INSERT INTO `poslavu_[dataname]_db`.`admin_action_log` ($q_fields, `server_time`) VALUES ($q_values, now())", $l_vars);
		}
		setcookie("poslavu_cp_login", "", time() - 100);
		set_sessvar("admin_loggedin",false);

		// set cookie for notifications
		setcookie("isLoggedIn", 0);

		// disable NEW CP notification Bar
		setcookie('DISABLE_NEW_CP_NOTIFICATION', null);

		setcookie('PHPSESSID', null);

		foreach($_SESSION as $key => $val)
		{
			if(substr($key, 0, 7) == "poslavu")
				unset($_SESSION[$key]);
		}
	}

	function admin_info($str)
	{
		return sessvar("admin_" . $str);
	}

	function lavu_auto_login()
	{
		if(!admin_loggedin() && isset($_COOKIE['poslavu_cp_login']))
		{
			$cust_query = mlavu_query("select * from `poslavu_MAIN_db`.`customer_accounts` where `autokey`!='' and `autokey`=AES_ENCRYPT('[1]','autokey')",$_COOKIE['poslavu_cp_login']);
			if(mysqli_num_rows($cust_query))
			{
				$cust_read = mysqli_fetch_assoc($cust_query);
				$custid = $cust_read['id'];
				$dataname = $cust_read['dataname'];
				$rdb = "poslavu_".$dataname."_db";
				$username = $cust_read['username'];
				$maindb = "poslavu_MAIN_db";
				//lavu_connect_dn($dataname,'poslavu_'.$dataname.'_db');
				$user_query = mlavu_query("select * from `$rdb`.`users` where `access_level`>='3' and `username`='[1]'",$username);
				if(mysqli_num_rows($user_query))
				{
					$rest_query = mlavu_query("select * from `$maindb`.`restaurants` where `data_name`='[1]'",$dataname); // poslavu_MAIN_db
					if(mysqli_num_rows($rest_query))
					{
						$rest_read = mysqli_fetch_assoc($rest_query);
						if($rest_read['disabled']=="1")
							$company_disabled = true;
						else
							$company_disabled = false;
						$companyid = $rest_read['id'];
						$version_number = $rest_read['version_number'];
						$dev = $rest_read['dev'];
						$company_name = $rest_read['company_name'];
					}
					else
					{
						$company_disabled = true;
						$companyid = 0;
					}

					if(!$company_disabled)
					{
						$user_read = mysqli_fetch_assoc($user_query);
						admin_login($dataname,$user_read['id'],$user_read['username'],trim($user_read['f_name']." ".$user_read['l_name']),$user_read['email'], $companyid, $version_number, $dev, $user_read['access_level'], $company_name, $user_read['phone'], $user_read['loc_id'], $user_read['lavu_admin']);
						//update_cp_login_status(true,$username);

						$autokey = session_id() . rand(1000,9999);
						setcookie("poslavu_cp_login", $autokey, mktime(0,0,0,date("m")+1,date("d"),date("Y")));
						mlavu_query("update `poslavu_MAIN_db`.`customer_accounts` set `autokey`=AES_ENCRYPT('[1]','autokey') where `id`='[2]'",$autokey,$custid);
					}
				}
				//echo "found login autokey";
			}
			//"update `poslavu_MAIN_db`.`customer_accounts` set `autokey`=AES_ENCRYPT('[1]','autokey') where `id`='[2]'"
		}
	}

	function get_product_name($location_info)
	{
		$product = "";
		if(isset($location_info['component_package']))
		{
			$com_query = mlavu_query("select * from `poslavu_MAIN_db`.`component_packages` where id='[1]'",$location_info['component_package']);
			if(mysqli_num_rows($com_query))
			{
				$com_read = mysqli_fetch_assoc($com_query);
				$product = explode("_",$com_read['title']);
				$product = $product[0];
				if(substr($product,strlen($product)-4)==" Dev")
					$product = trim(substr($product,0,strlen($product)-4));
			}
		}
		return $product;
	}

	function get_requested_company_code() {
		if (isset($_REQUEST['cc'])) {
			$company_code = $_REQUEST['cc'];
		} else if (isset($_SESSION['company_code'])) {
			$company_code = $_SESSION['company_code'];
		} else {
			$company_code = "";
		}
		return $company_code;
	}

	function get_company_code_info($company_code)
	{
		$ccrt = lsecurity_pass($company_code);
		$company_code = $ccrt['cc'];
		$company_code_key = $ccrt['key'];
		if(isset($_POST['cc']))
		{
			$ccrt = lsecurity_pass($_POST['cc']);
			$_POST['cc'] = $ccrt['cc'];
		}
		if(isset($_GET['cc']))
		{
			$ccrt = lsecurity_pass($_GET['cc']);
			$_GET['cc'] = $ccrt['cc'];
		}
		if(isset($_REQUEST['cc']))
		{
			$ccrt = lsecurity_pass($_REQUEST['cc']);
			$_REQUEST['cc'] = $ccrt['cc'];
		}
		return $ccrt;
	}

	require_once(dirname(__FILE__) . "/lavuquery.php");
	//------------------------------ misc functions -------------------------------------------//
	function spaces($n)
	{
		$str = "";
		for($i=0; $i<$n; $i++)
			$str .= "&nbsp;";
		return $str;
	}

	function urlvar($str,$def=false)
	{
		if(isset($_GET[$str]))
			return $_GET[$str];
		else
			return $def;
	}

	function postvar($str,$def=false)
	{
		if(isset($_POST[$str]))
			return $_POST[$str];
		else
			return $def;
	}

	function reqvar($str,$def=false)
	{
		if(isset($_POST[$str]))
			return $_POST[$str];
		else if(isset($_GET[$str]))
			return $_GET[$str];
		else if(isset($_REQUEST[$str]))
			return $_REQUEST[$str];
		else
			return $def;
	}

	function removeEmpty($old_array)
	{
		$new_array = array();
		foreach ($old_array as $old)
		{
			if ($old != "") $new_array[] = $old;
		}
		return $new_array;
	}

	function languagePackForUserID($user_id)
	{
		$get_user_language_pack_id = lavu_query("SELECT `use_language_pack` FROM `users` WHERE id = '[1]'", $user_id);
		if ($get_user_language_pack_id)
		{
			$user_info = mysqli_fetch_assoc($get_user_language_pack_id);
			$pack_id = $user_info['use_language_pack'];
			if ((int)$pack_id >= 0)
			{
				return $pack_id;
			}
		}

		// Default to location default language pack, and failing that, English (pack_id=1)
		return locationSetting("use_language_pack", "1");
	}

	function load_language_pack($pack_id, $loc_id) // take a given language pack ID and load it
	{
		$active_language_pack = array();
		global $active_language_pack_id;
		$active_language_pack_id = $pack_id;
		$active_language_pack = getLanguageDictionary($loc_id);
		if (empty($active_language_pack)) {
			if ($pack_id > 0) {
				$get_language_pack = mlavu_query("SELECT * FROM `poslavu_MAIN_db`.`language_dictionary` WHERE `pack_id` = '[1]' AND (`used_by` = '' OR `used_by` = 'cp')", $pack_id);
			}
			if ($pack_id >= 0)
			{
				if ($pack_id == 0)
				{
					$get_language_pack = lavu_query("SELECT * FROM `custom_dictionary` WHERE `loc_id` = '[1]' AND (`used_by` = '' OR `used_by` = 'cp')", $loc_id);
				}

				while ($pack_info = mysqli_fetch_assoc($get_language_pack))
				{
					$wordOrPhrase = ($pack_info['tag'] != '') ? $pack_info['tag'] : $pack_info['word_or_phrase'];
					$translation = $pack_info['translation'];
					if ($pack_info['tag'] != '' && $pack_info['translation'] == '') {
						$translation = $pack_info['word_or_phrase'];
					}
					$active_language_pack[$wordOrPhrase] = $translation;
				}
			} else {
				while ($pack_info = mysqli_fetch_assoc($get_language_pack))
				{
					if (isset($pack_info['tag']) && $pack_info['tag'] != '') {
						$active_language_pack[$pack_info['tag']] = ($pack_info['translation']) ? $pack_info['translation'] : $pack_info['word_or_phrase'];
					}
				}
			}
		}
		return $active_language_pack;
	}

	function safeString($str){
		if(mb_detect_encoding($str) !== false){
			return $str;
		}else{
			return "";
		}
	}

	function speak($str, $allow_auto_create=TRUE)
	{
		$str = trim($str);
		if ((string)$str == "")
		{
			return "";
		}

		if (is_numeric($str))
		{
			return $str;
		}

		if ($str == "-")
		{
			return $str;
		}

		$safeStr = safeString($str);
		if ($safeStr == "")
		{
			$hex_string = "";
			for ($c = 0; $c < strlen($str); $c++)
			{
				$hex_string .= str_pad(dechex(ord($string[$c])), 2, "0", STR_PAD_LEFT);
			}

			error_log("*** *** *** speak is blocking an unsafe word_or_phrase: ".$str." (".$hex_string.")");
			$backtrace = debug_backtrace();
			foreach ($backtrace as $bt)
			{
				error_log("bt: ".json_encode($bt));
			}
			return $str;
		}

		global $active_language_pack_id;
		if (!isset($active_language_pack_id))
		{
			$active_language_pack_id = getActiveLanguagePackId();
			//$active_language_pack_id = sessvar("active_language_pack_id");
		}

		if ($active_language_pack_id === FALSE)
		{
			$active_language_pack_id = locationSetting("use_language_pack", "1");
		}

		if ((int)$active_language_pack_id == 1)
		{
			return $str;
		}

		global $active_language_pack;
		if (!$active_language_pack)
		{
			$active_language_pack = getLanguageDictionary(sessvar("locationid"));
			//$active_language_pack = sessvar("active_language_pack");
		}

		if (isset($active_language_pack[$str]))
		{
			if ((string)$active_language_pack[$str] != "")
			{
				return $active_language_pack[$str];
			}
			else
			{
				return $str;
			}
		}

		if (!$allow_auto_create || $active_language_pack_id == 0)
		{
			// By providing the option to not allow language pack entry auto-creation for words or phrases
			// not already defined, we can safely use speak() in situations in which we may or may not be
			// handling a user defined value, such as with certain report column headers
			return $str;
		}

		// `word_or_phrase` doesn't seem to be defined, so let's check the language_dictionary

		$check_english_record = mlavu_query("SELECT `id` FROM `poslavu_MAIN_db`.`language_dictionary` WHERE `pack_id` = '1' AND `word_or_phrase` COLLATE latin1_general_cs = '[1]'", $str, NULL, NULL, NULL, NULL, NULL, FALSE);
		if (mysqli_num_rows($check_english_record) >= 1)
		{
			return $str;
		}

		// `word_or_phrase` isn't defined in the `language_dictionary`, so let's create a new set of entries to hold translations

		$utc_datetime = UTCDateTime(TRUE);

		$backtrace = debug_backtrace();

		$create_english_record = mlavu_query("INSERT INTO `poslavu_MAIN_db`.`language_dictionary` (`pack_id`, `word_or_phrase`, `used_by`, `created_date`, `modified_date`, `backtrace`) VALUES ('1', '[1]', 'cp', '[2]', '[2]', '[3]')", $str, $utc_datetime, json_encode($backtrace), NULL, NULL, NULL, FALSE);
		$english_id = mlavu_insert_id();

		$get_packs = mlavu_query("SELECT `id` FROM `poslavu_MAIN_db`.`language_packs` WHERE `id` != '1'");
		while ($pack = mysqli_fetch_assoc($get_packs))
		{
			$check_translation = mlavu_query("SELECT `id` FROM `poslavu_MAIN_db`.`language_dictionary` WHERE `word_or_phrase` COLLATE latin1_general_cs = '[1]' AND `pack_id`= '[2]'", $str, $pack['id'], NULL, NULL, NULL, NULL, FALSE);
			if (mysqli_num_rows($check_translation) > 0)
			{
				$translation = mysqli_fetch_assoc($check_translation);
				$set_english_id = mlavu_query("UPDATE `poslavu_MAIN_db`.`language_dictionary` SET `english_id` = '[1]' WHERE `id` = '[2]'", $english_id, $translation['id'], NULL, NULL, NULL, NULL, FALSE);
			}
			else
			{
				$add_translation = mlavu_query("INSERT INTO `poslavu_MAIN_db`.`language_dictionary` (`pack_id`, `word_or_phrase`, `english_id`, `used_by`, `created_date`, `modified_date`) VALUES ('[1]', '[2]', '[3]', 'cp', '[4]', '[4]')", $pack['id'], $str, $english_id, $utc_datetime, NULL, NULL, FALSE);
			}
		}
		deleteAllLanguagePack();
		return $str;
	}

	//-------------------------- floating info window -----------------------------------//
	function transparency($opacity)
	{
		return "style='filter:alpha(opacity=".$opacity.");-moz-opacity:.".$opacity.";opacity:.".$opacity."'";
	}

	$dialog_iframe_size = array(555,448);
	function create_info_layer($dwidth=false, $dheight=false)
	{
		global $dialog_iframe_size;
		if($dwidth) $dialog_iframe_size[0] = $dwidth;
		if($dheight) $dialog_iframe_size[1] = $dheight;

		$str = "";
		$str .= "<style>";
		// $str .= ".close_btn, .close_btn a:link, .close_btn a:visited, .close_btn a:hover {font-size:18px;} ";
		$str .= "</style>";

		$str .= "<div id='info_window_bg' name='info_window_bg'>";
		$str .= "<table width='$dwidth' height='$dheight' id='info_area_bgtable'  ".transparency(90).">";
		$str .= "<td align='center' valign='top'>";
		$str .= "&nbsp;";
		$str .= "</td>";
		$str .= "</table>";
		$str .= "</div>";

		$str .= "<div id='info_window' name='info_window'>";
		$str .= "<table width='$dwidth' height='$dheight' id='info_area_table'>";
		$str .= "<td align='center' valign='top'>";
		$str .= "<table width='100%'><td width='100%' align='right'><a onclick='info_visibility()' class='closeBtn'>X</a></td></table>";
		$str .= "<div id='info_area' name='info_area'>";
		$str .= "&nbsp;";
		$str .= "</div>";
		$str .= "</td>";
		$str .= "</table>";
		$str .= "</div>";
		$str .= "<script language='javascript' src='resources/info_window.js'></script>";
		$str .= "<script language='javascript'>";
		$str .= "inventory_window_exists = true;";
		$str .= "window.onscroll = set_info_location;";
		$str .= "</script>";

		return $str;
	}

	if(!function_exists("json_decode"))
	{
		function json_decode($str, $output_steps=false)
		{
			if($str[0]=="[")
				$str = trim($str,"[]");
			if($str[0]=="{" && substr($str, -1)!="}")
				$str = trim($str,"{}");
			return json_decode_step($str, $output_steps);
		}
	}

	function lavu_json_decode($str, $output_steps=false)
	{
		if(!empty($str) && $str[0]=="[")
			$str = trim($str,"[]");
		if(!empty($str) && $str[0]=="{" && substr($str, -1)!="}")
			$str = trim($str,"{}");
		return json_decode_step($str, $output_steps);
	}

	function json_decode_step($str, $output_steps)
	{
		if ($output_steps) echo "<br><br>".$str;

		$in_quotes = false;
		$esc_next = false;
		$qt_name = "";
		$qt_content = "";
		$colon_found = false;
		$brackets = array();
		$bracket_contents = "";
		$output = "";
		$vars = array();

		for($i=0; $i<strlen($str); $i++)
		{
			$ch = $str[$i];

			if($in_quotes)
			{
				if($esc_next)
				{
					$esc_next = false;
					$qt_content .= $ch;
				}
				else if($ch=="\\")
				{
					$esc_next = true;
				}
				else if($ch=="\"")
				{
					$in_quotes = false;
					if(count($brackets) < 1)
					{
						if($colon_found)
						{
							$vars[$qt_name] = $qt_content;
							$qt_content = "";
						}
						else
						{
							$qt_name = $qt_content;
							//if ($qt_name == "config_settings" || $qt_name == "qp_titles" || $output_steps==1) echo "<br><br>".$qt_name;
							$qt_content = "";
						}
					}
					$output .= $ch;
				}
				else
				{
					$qt_content .= $ch;
					if ($output_steps) echo "<br>".$qt_content;
				}
			}
			else
			{
				if($ch=="["||$ch=="{")
				{
					if(count($brackets) < 1)
					{
						$output .= $ch;
						$bracket_contents = "";
					}
					array_push($brackets,$ch);
				}
				else if($ch=="]"||$ch=="}")
				{
					//if($qt_name=="modifiers") echo count($brackets) . ":" . $bracket_contents . "<br><br>";
					if(count($brackets) > 0)
						array_pop($brackets);
					if(count($brackets) < 1)
					{
						$os = false;
						if($colon_found)
						{
							//$vars[$qt_name] = "testing...";
							//if ($qt_name == "config_settings" || $qt_name == "qp_titles") { $os = true; echo "<br><br>".$bracket_contents; }
							$vars[$qt_name] = lavu_json_decode($bracket_contents, $os);
						}
						else
						{
							$bracket_contents = trim($bracket_contents,"{}");
							//if ($qt_name == "config_settings" || $qt_name == "qp_titles") { $os = true; echo "<br><br>".$bracket_contents; }
							$vars[] = lavu_json_decode($bracket_contents, $os);
						}
						$output .= $ch;
					}
				}
				else if($ch=="\"")
				{
					$in_quotes = true;
				}
				else if(count($brackets) > 0)
				{
				}
				else if($ch==":")
				{
					$colon_found = true;
				}
				else if($ch==",")
				{
					if ($qt_name != "" && !$colon_found) $vars[] = $qt_name;
					$qt_name = "";
					$qt_content = "";
					$colon_found = false;
				}
				else
				{
					$output .= $ch;
				}
			}
			if(count($brackets) > 0)
			{
				$bracket_contents .= $ch;
			}
		}
		if ($qt_name != "" && !$colon_found) $vars[] = $qt_name;
		//if($colon_found)
		//{
			//echo $qt_name . ": " . $bracket_contents . "<br><br>";
		//}
		return $vars;
	}

	function display_decoded_json($tinfo)
	{
		echo "<table>";
		foreach($tinfo as $var => $val)
		{
			echo "<tr><td valign='top'>" . $var . "</td><td valign='top'>";
			if(is_array($val))
			{
				display_decoded_json($val);
			}
			else
				echo $val;
			echo "</td></tr>";;
		}
		echo "</table>";
	}

	// lavu to go functions

	function sendInfoToLavuToGo($ltg_name, $loc_id, $company_name, $logo_img, $enabled, $use_optional_modifiers="0")
	{
		global $location_info;

		$ltg_debug		= !empty($location_info['ltg_debug']);
		$dn				= admin_info("dataname");
		$j_key			= lc_encode(lsecurity_name($dn, admin_info("companyid")));
		$lavu_code		= lc_encode("lavu2go:".$dn.":".date("Ymd"));
		$use_lavu_url	= !empty($location_info['ltg_use_central_server_url'])?$location_info['ltg_use_central_server_url']:"";

		$vars = array(
			'company_name'	=> $company_name,
			'data_name'		=> $dn,
			'enabled'		=> $enabled,
			'is_dev'			=> admin_info("dev"),
			'j_key'			=> $j_key,
			'lc'				=> $lavu_code,
			'location_id'	=> $loc_id,
			'logo_img'		=> $logo_img,
			'ltg_debug'		=> $ltg_debug?"1":"0",
			'ltg_name'		=> $ltg_name,
			'useoptmod'		=> $use_optional_modifiers,
			'use_lavu_url'	=> $use_lavu_url,
			'XIG'			=> lavuRequestID()
		);

		$post_vars = "";
		foreach ($vars as $key => $val)
		{
			if (!empty($post_vars))
			{
				$post_vars .= "&";
			}
			$post_vars .= $key."=".rawurlencode($val);
		}

		if ($ltg_debug)
		{
			// ** DEBUG LOGGING ** //
			error_log(__FUNCTION__." - ".$company_name." - ".$ltg_name." - ".($enabled?"enabled":"disabled")." - ".$post_vars);
		}

		$url = "https://lavutogo.com/app/company_info_api.php?YIG=".lavuRequestID();
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $post_vars);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

		$curl_start = microtime(true);

		$response = curl_exec($ch);

		$curl_end = microtime(true);
		$curl_duration = round((($curl_end - $curl_start) * 1000), 2);
		$curl_info = curl_getinfo($ch);
		$curl_error = curl_error($ch);

		if ($ltg_debug)
		{
			$info_and_error = (empty($response))?" - info: ".json_encode($curl_info)." - error: ".$curl_error:"";
			// ** DEBUG LOGGING ** //
			error_log(__FUNCTION__." - response: ".$response." - duration: ".$curl_duration." ms".$info_and_error);
		}

		curl_close($ch);
	}

	function lc_encode($data, $type="ltg") {

		if ($type=="jc") $key_access = array(1, 2, 4, 7, 8);
		else $key_access = array(0, 2, 3, 7, 9);
		$text_string = $data;
		$key = array(rand(1,8),rand(1,8),rand(1,8),rand(1,8),rand(1,8),rand(1,8),rand(1,8),rand(1,8),rand(1,8),rand(1,8));
		$return_number = "";
		for ($c = 0; $c < strlen($data); $c++) {
			$code = ord($data[$c]);
			$mult = $key[$key_access[($c % 5)]];
			$return_number .= str_pad(($mult * $code), 4, "0", STR_PAD_LEFT);
		}

		return implode("", $key).$return_number;
	}

	function lc_decode($data, $type="ltg") {

		if ($type=="jc") $key_access = array(1, 2, 4, 7, 8);
		else $key_access = array(0, 2, 3, 7, 9);
		$ecodes = array();
		$dchars = array();
		$number_string = $data;
		$key = substr($number_string, 0, 10);
		$edata = substr($number_string, 10, (strlen($number_string) - 10));
		for ($g = 0; $g < floor(strlen($edata) / 4); $g++) {
			$dchars[] = chr(((int)substr($edata, ($g * 4), 4) / $key[$key_access[($g % 5)]]));
		}
		return join("", $dchars);
	}

	// form functions
	function alter_properties_to_assoc($field_props)
	{
		if($field_props!="") // alter properties from comma delimited list into associative array
		{
			require_once dirname(__FILE__) . '/json.php';
			$res= LavuJson::json_validate($field_props, false);
			if($res){
				$field_props= $res;
			}else{
				$props = explode(",",$field_props);
				$field_props = array();
				for($n=0; $n<count($props); $n++)
				{
					$prop_parts = explode(":",str_replace("::","[colon]",$props[$n]));
					$prop_key = trim($prop_parts[0]);
					if(count($prop_parts) > 1) $prop_val = trim($prop_parts[1]);
					else $prop_val = $prop_key;

					$field_props[$prop_key] = str_replace("[colon]",":",$prop_val);
				}
			}
		}
		else
			$field_props = array();
		return $field_props;
	}

	class FormField {
		public $title;
		public $name;
		public $type;
		public $props;
		public $props_core;
		public $props2;
		public $help_link;
		public $label_cell_id;
		public $value;
		public $db_include;
		public $encode_field;

		public function __construct($field_info) {
			$this->title = $field_info[0];
			$this->name = $field_info[1];
			$this->type = $field_info[2];

			if(!isset($field_info[3])) $field_info[3] = array();
			if(is_array($field_info[3]))
			{
				$this->props = $field_info[3];
			}
			else
			{
				$this->props = alter_properties_to_assoc($field_info[3]);
			}

			if(!isset($field_info[4])) $field_info[4] = false;
			if(is_array($field_info[4]))
			{
				$this->props_core = $field_info[4];
			}
			else
			{
				$this->props_core = alter_properties_to_assoc($field_info[4]);
			}
			foreach($this->props_core as $key => $val)
			{
				$this->$key = $val;
			}

			if (!isset($field_info[5])) $field_info[5] = "";
			if (is_array($field_info[5]))
			{
				$this->props2 = $field_info[5];
			}
			else if (substr($field_info[5], 0, 14) == "label_cell_id:")
			{
				$lci_parts = explode(":", $field_info[5]);
				$this->label_cell_id = $lci_parts[1];
			}
			else
			{
				$this->help_link = $field_info[5];
			}
		}
	}

	function lavu_json_encode($a=false) {

		if (is_null($a)) return 'null';
		if ($a === false) return 'false';
		if ($a === true) return 'true';
		if (is_scalar($a)) {
			if (is_float($a)) {
					// Always use "." for floats.
				return '"'.floatval(str_replace(",", ".", strval($a))).'"';
			}

			if (is_string($a)) {
				static $jsonReplaces = array(array("\\", "/", "\n", "\t", "\r", "\b", "\f", '"'), array('\\\\', '\\/', '\\n', '\\t', '\\r', '\\b', '\\f', '\"'));
				return '"' . str_replace($jsonReplaces[0], $jsonReplaces[1], $a) . '"';
			} else {
				return '"'.$a.'"';
			}
		}
		$isList = true;

		for ($i = 0, reset($a); $i < count($a); $i++, next($a)) {
			if (key($a) !== $i) {
				$isList = false;
				break;
			}
		}
		$result = array();

		if ($isList) {
			foreach ($a as $v) $result[] = lavu_json_encode($v);
			return '[' . join(',', $result) . ']';
		} else {
			foreach ($a as $k => $v) $result[] = lavu_json_encode($k).':'.lavu_json_encode($v);
			return '{' . join(',', $result) . '}';
		}
	}

	if (!function_exists('json_encode')) {

		function json_encode($a=false) {
			if (is_null($a)) return 'null';
			if ($a === false) return 'false';
			if ($a === true) return 'true';
			if (is_scalar($a)) {
				if (is_float($a)) {
					// Always use "." for floats.
					return '"'.floatval(str_replace(",", ".", strval($a))).'"';
				}

				if (is_string($a)) {
					static $jsonReplaces = array(array("\\", "/", "\n", "\t", "\r", "\b", "\f", '"'), array('\\\\', '\\/', '\\n', '\\t', '\\r', '\\b', '\\f', '\"'));
					return '"' . str_replace($jsonReplaces[0], $jsonReplaces[1], $a) . '"';
				} else {
					return '"'.$a.'"';
				}
			}
			$isList = true;

			for ($i = 0, reset($a); $i < count($a); $i++, next($a)) {
				if (key($a) !== $i) {
					$isList = false;
					break;
				}
			}
			$result = array();

			if ($isList) {
				foreach ($a as $v) $result[] = json_encode($v);
				return '[' . join(',', $result) . ']';
			} else {
				foreach ($a as $k => $v) $result[] = json_encode($k).':'.json_encode($v);
				return '{' . join(',', $result) . '}';
			}
		}
	}

	function create_datepicker($name,$setdate,$onselect)
	{
		echo "<script type=\"text/javascript\" src=\"resources/datepickercontrol.js\"></script>";
		echo "<link type=\"text/css\" rel=\"stylesheet\" href=\"resources/datepickercontrol.css\">";
		echo "<script language='javascript'>";
		echo "DatePickerControl.onSelect = function(inputid) { ";
		echo "  " . str_replace("[value]","document.getElementById(inputid).value",$onselect);
		echo "} ";
		echo "</script>";

		$set_date_start = $setdate;
		$date_start_ts = mktime(0,0,0,substr($set_date_start,5,2),substr($set_date_start,8,2),substr($set_date_start,0,4));

		echo "<input type=\"hidden\" id=\"DPC_TODAY_TEXT\" value=\"today\">";
		echo "<input type=\"hidden\" id=\"DPC_BUTTON_TITLE\" value=\"Open calendar...\">";
		echo "<input type=\"hidden\" id=\"DPC_MONTH_NAMES\" value=\"['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December']\">";
		echo "<input type=\"hidden\" id=\"DPC_DAY_NAMES\" value=\"['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat']\">";
		echo "<input type=\"text\" name=\"$name\" id=\"$name\" datepicker=\"true\" datepicker_format=\"YYYY-MM-DD\" value=\"".date("m/d/Y",$date_start_ts)."\">";
	}

	function current_ts($min_offset=0)
	{
		$hour = date("H") - 2;
		$min = date("i") + $min_offset;
		$sec = date("s");
		$mon = date("m");
		$day = date("d");
		$year = date("Y");
		return mktime($hour,$min,$sec,$mon,$day,$year);
	}

	function update_api_access_log($company_dataname,$set_location)
	{
		$qinfo = array();
		$qinfo['ipaddress'] = $_SERVER['REMOTE_ADDR'];
		$qinfo['dataname'] = $company_dataname;
		$qinfo['time'] = date("Y-m-d H:i:s");
		$qinfo['location'] = $set_location;
		mlavu_query("insert into `poslavu_MAIN_db`.`api_access_log` (`ipaddress`,`dataname`,`time`,`location`) values ('[ipaddress]','[dataname]','[time]', '[location]')",$qinfo);
	}

	function getFieldInfo($known_info, $known_field, $tablename, $get_field) {
		if (($tablename == "menus") && ($known_field == "id") && ($known_info == 0)) {
			return "No menu selected...";
		}

		$field_info = "";

		$get_field_info = lavu_query("SELECT $get_field FROM $tablename WHERE $known_field = '$known_info' LIMIT 1");
		$extract = mysqli_fetch_assoc($get_field_info);
		$field_info = $extract[$get_field];

		return $field_info;
	}

	function assignNext($field, $tablename, $where_field, $where_value) {
		$get_last = lavu_query("SELECT $field FROM $tablename WHERE $where_field = '$where_value' ORDER BY $field * 1 DESC LIMIT 1");
		if (@mysqli_num_rows($get_last) > 0) {
			$extract = mysqli_fetch_assoc($get_last);
			$next_value = $extract[$field] + 1;
		} else {
			$next_value = 1;
		}

		return $next_value;
	}

	function assignNextWithPrefix($field, $tablename, $where_field, $where_value, $prefix) {

		global $data_name;

		$n = strlen($prefix);

		// new method 1 - order descending by second portion of order id but check recent first before checking all using old method

		$minimum_datetime = date("Y-m-d H:i:s", time() - 259200);
		$maximum_datetime = date("Y-m-d 00:00:00", time() + 604800);

		//$check4ltg = "";
		//if ($field == "order_id") $check4ltg = " OR LEFT(`order_id`, 2) = '2-'";

		$need_new_orderid = true;
		$method1_next_value = "";
		//$get_last = lavu_query("SELECT `$field` FROM `$tablename` WHERE `$where_field` = '$where_value' AND ((`opened` != '' AND `opened` != '0000-00-00 00:00:00' AND `opened` > '$minimum_datetime' AND `opened` < '$maximum_datetime')".$check4ltg.") AND LEFT ($field, $n) = '$prefix' ORDER BY SUBSTR($field, ($n + 1)) * 1 DESC LIMIT 1");
		//$get_last = lavu_query("SELECT `$field` FROM `$tablename` WHERE `$where_field` = '$where_value' AND (`opened` != '' AND `opened` != '0000-00-00 00:00:00' AND `opened` > '$minimum_datetime' AND `opened` < '$maximum_datetime') AND LEFT ($field, $n) = '$prefix' ORDER BY SUBSTR($field, ($n + 1)) * 1 DESC LIMIT 1");
		//$get_last = lavu_query("SELECT `[1]` FROM (SELECT `[1]` FROM `[2]` WHERE `[3]` = '[4]' AND (`opened` != '' AND `opened` != '0000-00-00 00:00:00' AND `opened` > '$minimum_datetime' AND `opened` < '$maximum_datetime')) AS `[2]` WHERE LEFT ([1], $n) = '[5]' ORDER BY SUBSTR([1], ($n + 1)) * 1 DESC LIMIT 1", $field, $tablename, $where_field, $where_value, $prefix);
		$get_last = lavu_query("SELECT `[1]` FROM (SELECT `[1]` FROM `[2]` WHERE `[3]` = '[4]' AND (`opened` != '' AND `opened` != '0000-00-00 00:00:00' AND `opened` > '$minimum_datetime' AND `opened` < '$maximum_datetime')) AS `[2]` WHERE `[1]` LIKE '[5]%' ORDER BY SUBSTR([1], ($n + 1)) * 1 DESC LIMIT 1", $field, $tablename, $where_field, $where_value, $prefix);
		if (mysqli_num_rows($get_last) > 0) {
			$extract = mysqli_fetch_assoc($get_last);
			$method1_next_value = $prefix.((substr($extract[$field], $n) * 1) + 1);

			$confirm_query = lavu_query("SELECT `[1]` FROM `[2]` WHERE `[1]`='$method1_next_value'", $field, $tablename);
			if(mysqli_num_rows($confirm_query))
			{
			}
			else
			{
				$need_new_orderid = false;
			}
		}

		if($need_new_orderid) {
			//$get_last = lavu_query("SELECT `[1]` FROM `[2]` WHERE `[3]` = '[4]' AND LEFT ([1], $n) = '[5]' ORDER BY SUBSTR([1], ($n + 1)) * 1 DESC LIMIT 1", $field, $tablename, $where_field, $where_value, $prefix);
			$get_last = lavu_query("SELECT `[1]` FROM `[2]` WHERE `[3]` = '[4]' AND `[1]` LIKE '[5]%' ORDER BY SUBSTR([1], ($n + 1)) * 1 DESC LIMIT 1", $field, $tablename, $where_field, $where_value, $prefix);
			if (@mysqli_num_rows($get_last) > 0) {
				$extract = mysqli_fetch_assoc($get_last);
				$method1_next_value = $prefix.((substr($extract[$field], $n) * 1) + 1);
			} else {
				if (strlen($prefix) >= 3) $method1_next_value = $prefix."1";
				else $method1_next_value = $prefix.assignNext($field, $tablename, $where_field, $where_value);
			}
		}
		$next_value = $method1_next_value;
		return $next_value;
	}

	function get_integration_from_location($locid, $use_db="") {
		if($use_db&&$use_db!="") $use_db = "`$use_db`.";
		preg_match("/poslavu_(.*)_db/", $use_db, $data_name);
		//lavu_connect_dn( $data_name[1],use_db);
		$newQueryFieldsStr="";

		$refQuery = lavu_query("select * from ".$use_db."locations where _disabled=0 limit 1");

		$row = mysqli_fetch_assoc($refQuery);

		if(isset($row['integration9'])){
			$newQueryFieldsStr .= " , AES_DECRYPT(`integration9`,'".integration_keystr()."') as integration9 ";
		}

		if(isset($row['integration10'])){
			$newQueryFieldsStr .= " , AES_DECRYPT(`integration10`,'".integration_keystr()."') as integration10 ";
		}

		$selectQueryPart = " SELECT AES_DECRYPT(`integration1`,'".integration_keystr()."') as integration1, AES_DECRYPT(`integration2`,'".integration_keystr()."') as integration2, AES_DECRYPT(`integration3`,'".integration_keystr()."') as integration3, AES_DECRYPT(`integration4`,'".integration_keystr()."') as integration4, AES_DECRYPT(`integration5`,'".integration_keystr()."') as integration5, AES_DECRYPT(`integration6`,'".integration_keystr()."') as integration6, AES_DECRYPT(`integration7`,'".integration_keystr()."') as integration7, AES_DECRYPT(`integration8`,'".integration_keystr()."') as integration8, `gateway`, `cc_transtype` ".$newQueryFieldsStr;

		$get_integration_settings = lavu_query($selectQueryPart." from ".$use_db."`locations` where id='[1]'", $locid, null, null, null, null, null, false);

		if(mysqli_num_rows($get_integration_settings)) {
			$integration_extract = mysqli_fetch_assoc($get_integration_settings);
		}
		else $integration_extract = false;
		return $integration_extract;
	}

	function get_location_info($data_name, $loc_id) {
		$get_location_info = lavu_query("SELECT * FROM `poslavu_".$data_name."_db`.`locations` WHERE `id` = '[1]'", $loc_id);
		if (mysqli_num_rows($get_location_info) > 0) {
			$location_info = mysqli_fetch_assoc($get_location_info);
			return $location_info;
		} else {
			return false;
		}
	}

	function checkUsernameAvailability($username, $id) {

		global $company_info;

		$username_established = FALSE;
		$new_username = $username;
		$append_number = 0;
		$filter = "";

		if ($id != "") {
			$filter = " AND id != '".$id."' AND company_code != '".$_SESSION['company_code']."'";
		}

		$conn = ConnectionHub::getConn('poslavu');

		while ($username_established == FALSE) {
			$found = FALSE;
			$get_data_names = $conn->query('read', "SELECT * FROM restaurants");
			if (@mysqli_num_rows($get_data_names) > 0) {
				while ($c_info = mysqli_fetch_array($get_data_names)) {
					$conn->selectDB("poslavu_".$c_info['data_name']."_db");
					$check_usernames = $conn->query('read', "SELECT * FROM users WHERE username = '".$new_username."'".$filter);
					if (@mysqli_num_rows($check_usernames) > 0) {
						$found = TRUE;
						$append_number++;
						$new_username = $username.$append_number;
						break;
					}
				}
				if ($found == FALSE) {
					$username_established = TRUE;
				}
			}
		}
		return $new_username;
	}

	function JS_alert($message) {
		echo "<script language='javascript'>alert(\"".str_replace('"','&quot;',$message)."\");</script>";
	}

	function write_log_entry($type, $info1, $info2, $path="")
	{
		// For details related to Lavu's logging system, refer to http://wiki.lavu/Server_Logs

		global $gateway_log_path;

		$use_date_time	= MountainDateTime();
		$use_dt_parts	= explode(" ", $use_date_time);
		$use_date		= $use_dt_parts[0];
		$log_line_start	= number_format(microtime(TRUE), 1, ".", "")." - ".$use_date_time." Mountain - ";

		$ids = array();
		$uuid = reqvar("UUID");
		if ($uuid)
		{
			$ids[] = "DEVICE: ".shortDeviceUUID($uuid);
		}
		$zig = reqvar("ZIG");
		if ($zig)
		{
			$ids[] = "ZIG: ".$zig;
		}
		$yig = reqvar("YIG");
		if ($yig)
		{
			$ids[] = "YIG: ".$yig;
		}
		$ids[] = "PID: ".getmypid();

		$log_path = $path;
		$log_filename = "";
		$log_string = "";

		switch ($type)
		{
			case "gateway":
			{
				if (!empty($info1))
				{
					$log_path = $gateway_log_path.$info1."/operational";
					$log_filename = "gateway_".$use_date.".log";
					$log_string = $log_line_start.str_replace("[--CR--]LOCATION:", " - ".implode(" - ", $ids)."[--CR--]LOCATION:", $info2);
				}

				break;
			}

			case "sync_conflict":
			{
				if (!empty($info1))
				{
					$log_path = "/home/poslavu/logs/company/".$info1."/debug";
					$log_filename = "sync_conflicts_".$use_date.".log";
					$log_string = $log_line_start.$info2;
				}

				break;
			}

			default:
				break;
		}

		if (!empty($log_path) && !empty($log_filename) && !empty($log_string))
		{
			if (!is_dir($log_path))
			{
				mkdir($log_path, 0755, TRUE);
			}

			$full_path = $log_path."/".$log_filename;

			$log_file = fopen($full_path, "a");
			fputs($log_file, $log_string);
			fclose($log_file);

			chmod($full_path, 0775);
		}
	}

	function gateway_debug($dataname, $loc_id, $gateway, $transtype, $approved, $text)
	{
		// For details related to Lavu's logging system, refer to http://wiki.lavu/Server_Logs

		global $gateway_log_path;

		$gateway_debug_path = $gateway_log_path.$dataname."/debug/gateway_debug";

		if (!is_dir($gateway_debug_path))
		{
			mkdir($gateway_debug_path, 0755, TRUE);
		}

		$rndm = rand(111, 999);

		$use_date_time	= str_replace(array( "-", ":", " "), array( "", "", "_"), MountainDateTime());
		$file_name		= $loc_id."_".$use_date_time."_".$rndm."_".str_replace(" ", "_", $gateway)."_".$transtype."_".$approved;
		$full_path		= $gateway_debug_path."/".$file_name;

		$log_file = fopen($full_path, "a");
		$written = fputs($log_file, $text."\n\n");
		fclose($log_file);

		chmod($full_path, 0775);
	}

	function UTCDateTime($add_z, $ts="")
	{
		if ($ts == "")
		{
			$ts = time();
		}

		$utc_tz = new DateTimeZone("UTC");
		$datetime = new DateTime(NULL, $utc_tz);
		$datetime->setTimestamp($ts);

		$datetime_str = $datetime->format("Y-m-d H:i:s");
		if ($add_z)
		{
			$datetime_str .= "Z";
		}

		return $datetime_str;
	}

	function MountainDateTime($ts="")
	{
		return DateTimeForTimeZone("America/Denver", $ts);
	}

	function DateTimeForTimeZone($timezone, $ts="")
	{
		if ($ts == "")
		{
			$ts = time();
		}

		if (empty($timezone))
		{
			return UTCDateTime(TRUE, $ts);
		}

		$tz = new DateTimeZone($timezone);
		$datetime = new DateTime(NULL, $tz);
		$datetime->setTimestamp($ts);

		$datetime_str = $datetime->format("Y-m-d H:i:s");

		return $datetime_str;
	}

	function localize_datetime($timestamp, $to_tz)
	{
		$old_tz = date_default_timezone_get();
		$unix_time = strtotime($timestamp);
		if (!empty($to_tz))
			date_default_timezone_set($to_tz);
		$result = strftime("%Y-%m-%d %H:%M:%S", $unix_time);
		date_default_timezone_set($old_tz);

		return $result;
	}

	function localize_date($timestamp, $to_tz) {
		$old_tz = date_default_timezone_get();
		$unix_time = strtotime($timestamp);
		if (!empty($to_tz))
			date_default_timezone_set($to_tz);
		$result = strftime("%Y-%m-%d", $unix_time);
		date_default_timezone_set($old_tz);

		return $result;
	}

	function localize_time($timestamp, $to_tz) {
	    $old_tz = date_default_timezone_get();
	    $unix_time = strtotime($timestamp);
	    if (!empty($to_tz)){
	        date_default_timezone_set($to_tz);
	    }
	    $result = strftime("%H:%M:%S", $unix_time);
	    date_default_timezone_set($old_tz);
	    return $result;
	}

	function timize($time_string) {

		$h = substr($time_string, 0, -2);
		$m = substr($time_string, -2);
		$ap = "am";
		if ((int)$h > 12) { $h -= 12; $ap = "pm"; }
		if ((int)$h == 0) { $h = 12; }

		return $h.":".$m.$ap;
	}

	function json_escape($string) {
		static $jsonReplaces = array(array("\\", "/", "\n", "\t", "\r", "\b", "\f", '"'), array('\\\\', '\\/', '\\n', '\\t', '\\r', '\\b', '\\f', '\"'));
		return str_replace($jsonReplaces[0], $jsonReplaces[1], $string);
	}

	function lavu_echo($text, $close_db=true, $exit=true, $flush=false) {

		if ($close_db) {
			mlavu_close_db();
			lavu_close_db();
		}

		echo $text;

		if ($flush) {
			flush();
		}

		if ($exit) {
			lavu_exit();
		}
	}

	function lavu_exit($reactMode = false)
	{
		global $NewRelic;

		ConnectionHub::closeAll();

		if (isset($NewRelic) && $NewRelic!=NULL && is_object($NewRelic))
		{
			if (is_a($NewRelic, "NewRelicHelper"))
			{
				$NewRelic->trackRequestEnd();
			}
		}
		// Remove session entirely which still persisted when user logout from CP3
		if($reactMode){
			session_unset();
			session_destroy();
		}
		exit();
	}

	function determineDeviceIdentifier($req) {

		$identifier = "";
		if (isset($req['UDID'])) $identifier = $req['UDID'];
		else if (isset($req['UUID'])) $identifier = $req['UUID'];
		else if (isset($req['MAC'])) $identifier = $req['MAC'];

		return $identifier;
	}

	function register_connect($type, $restaurantid, $dataname, $loc_id="", $device_type="", $macaddress="") {

		$vars = array();
		$vars['type'] = $type;
		$vars['restaurantid'] = $restaurantid;
		$vars['dataname'] = $dataname;
		$vars['loc_id'] = $loc_id;
		$vars['device_type'] = $device_type;
		$vars['macaddress'] = $macaddress;
		$vars['ipaddress'] = (!empty($_SERVER['HTTP_X_FORWARDED_FOR']))?$_SERVER['HTTP_X_FORWARDED_FOR']:$_SERVER['REMOTE_ADDR'];

		$filter = "";
		$keys = array_keys($vars);
		foreach ($keys as $key) {
			if ($filter != "") $filter .= " AND ";
			$filter .= "`$key` = '[$key]'";
		}

		$hour_column = "hour_".date("H")."00";

		$vars['count'] = 1;
		$vars[$hour_column] = 1;
		$vars['first_connect'] = date("Y-m-d H:i:s");
		$vars['last_connect'] = $vars['first_connect'];

		$q_fields = "";
		$q_values = "";
		$keys = array_keys($vars);
		foreach ($keys as $key) {
			if ($q_fields != "") { $q_fields .= ", "; $q_values .= ", "; }
			$q_fields .= "`$key`";
			$q_values .= "'[$key]'";
		}

		$check_connects = mlavu_query("SELECT `id`, `count`, `$hour_column` FROM `poslavu_MAIN_db`.`ap_connects` WHERE $filter", $vars);
		if (mysqli_num_rows($check_connects) > 0) {

			$info = mysqli_fetch_assoc($check_connects);
			$vars['id'] = $info['id'];
			$vars['count'] = ((int)$info['count'] + 1);
			$vars[$hour_column] = ((int)$info[$hour_column] + 1);
			$update_record = mlavu_query("UPDATE `poslavu_MAIN_db`.`ap_connects` SET `count` = '[count]', `$hour_column` = '[$hour_column]', `last_connect` = '[last_connect]' WHERE `id` = '[id]'", $vars);

		} else {

			$create_record = mlavu_query("INSERT INTO `poslavu_MAIN_db`.`ap_connects` ($q_fields) VALUES ($q_values)", $vars);
		}
	}

	function get_use_date_end_date($location_info, $datetime) {

		$se_hour = substr(str_pad($location_info['day_start_end_time'], 4, "0", STR_PAD_LEFT), 0, 2);
		$se_min = substr(str_pad($location_info['day_start_end_time'], 4, "0", STR_PAD_LEFT), 2, 2);

		$idatetime = explode(" ", $datetime);
		$idate = explode("-", $idatetime[0]);
		$itime = explode(":", $idatetime[1]);
		$yts = mktime($itime[0], $itime[1], $itime[2], $idate[1], ($idate[2] - 1), $idate[0]);
		$nowts = mktime($itime[0], $itime[1], $itime[2], $idate[1], $idate[2], $idate[0]);
		$tts = mktime($itime[0], $itime[1], $itime[2], $idate[1], ($idate[2] + 1), $idate[0]);

		if ($itime[0].$itime[1].$itime[2] >= $se_hour.$se_min."00") {
			$use_date = date("Y-m-d", $nowts);
			$end_date = date("Y-m-d", $tts);
		} else {
			$use_date = date("Y-m-d", $yts);
			$end_date = date("Y-m-d", $nowts);
		}

		return array($use_date, $end_date, $use_date." ".$se_hour.":".$se_min.":00", $end_date." ".$se_hour.":".$se_min.":00");
	}

	function llsFileStructureContainsString($string) {

		$get_file_structure = lavu_query("SELECT `value_long` FROM `config` WHERE `setting` = 'local_file_structure'");
		if (mysqli_num_rows($get_file_structure) > 0) {
			$info = mysqli_fetch_assoc($get_file_structure);
			return (strpos($info['value_long'], $string));
		} else return false;
	}

	function updateCentralLastActivity($data_name, $app_name, $version, $build) {

		$lavu_code = lc_encode("cenlastact:$data_name:".date("Ymd"));

		$vars = "lc=$lavu_code&data_name=$data_name&app_name=$app_name&version=$version&build=$build";

		$url = 'https://admin.poslavu.com/lib/update_last_activity.php';
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $vars);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		$response = curl_exec($ch);
		curl_close ($ch);
	}

	function generateToken($length=12)
	{
		$str = "";
		for($i=0; $i<$length; $i++)
		{
			$n = rand(0,61);
			if($n < 10) // 0-9
			{
				$setn = 48 + $n;
			}
			else if($n<=35) // 10-35
			{
				$setn = 55 + $n;
			}
			else if($n<=61) // 36-61
			{
				$setn = 61 + $n;
			}
			$str .= chr($setn);
		}
		return $str;
	}

	function appVersionMinMax($loc_id, $app_name, &$min, &$max)
	{
		if (empty($loc_id))
		{
			$min = 0;
			$max = 0;

			return;
		}

		$min = 999999;
		$max = 0;

		$app_name_filter = ($app_name == "POSLavu Client")?"`app_name` IN ('POSLavu Client', 'POS Lavu Client', 'Lavu Client', 'Lavu POS', 'Lavu Retro')":"`app_name` = '[2]'";

		$check_devices = lavu_query("SELECT `poslavu_version` FROM `devices` WHERE `loc_id` = '[1]' AND ".$app_name_filter." AND `inactive` = '0'", $loc_id, $app_name);
		if (mysqli_num_rows($check_devices) > 0) {
			while ($info = mysqli_fetch_assoc($check_devices)) {
				$version_n_build = explode(" ", $info['poslavu_version']);
				$version_parts = explode(".", $version_n_build[0]);
				while (count($version_parts) < 3) {
					$version_parts[] = "00";
				}
				$version_str = "";
				foreach ($version_parts as $part) {
					$version_str .= str_pad($part, 2, 0, STR_PAD_LEFT);
				}
				$min = min($min, (int)$version_str);
				$max = max($max, (int)$version_str);
			}
		}
	}

	function getDevicePrefix() {

		$device_prefix = "";

		$check_field = "UDID";
		if (isset($_REQUEST['UUID'])) $check_field = "UUID";
		else if (isset($_REQUEST['MAC'])) $check_field = "MAC";

		$get_device_prefix = lavu_query("SELECT `prefix` FROM `devices` WHERE `$check_field` = '[1]'", $_REQUEST[$check_field]);
		if (mysqli_num_rows($get_device_prefix) > 0) {
			$device_info = mysqli_fetch_assoc($get_device_prefix);
			$device_prefix = str_pad((string)$device_info['prefix'], 2, "0", STR_PAD_LEFT);
		}

		return $device_prefix;
	}

	function appVersionCompareValue($version_string) {

		$v = 0;

		$v_array = explode(".", $version_string);
		$v += ($v_array[0] * 10000);
		if (count($v_array) >= 2) $v += ($v_array[1] * 100);
		if (count($v_array) >= 3) $v += ($v_array[2] * 1);

		return $v;
	}

	function getUserIdFromPunch($punch_id) {

		$userid = "";
		$get_user_id = lavu_query("SELECT `server_id` FROM `clock_punches` WHERE `id` = '[1]'", $punch_id);
		if (mysqli_num_rows($get_user_id) > 0) {
			$uinfo = mysqli_fetch_assoc($get_user_id);
			$userid = $uinfo['server_id'];
		}

		return $userid;
	}

	function checkPunchStatusAndSetFlag($user_id, $location_info) {

		if (!empty($user_id)) {

			$clocked_in = 0;
			$punch_query = lavu_query("SELECT `id` FROM `clock_punches` WHERE `punched_out` = '0' AND `_deleted` != '1' AND `server_id` = '[1]' AND `location_id` = '[2]' ORDER BY `time` DESC LIMIT 1", $user_id, $location_info['id']);
			if (mysqli_num_rows($punch_query)) $clocked_in = 1;
			$flag_query = "UPDATE `users` SET `clocked_in` = '".$clocked_in."' WHERE `id` = '".$user_id."'";
			$update_user = lavu_query($flag_query);
			schedule_remote_to_local_sync_if_lls($location_info, $location_info['id'], "run local query", "users", $flag_query);
		}
	}

	function newInternalID() {

		global $company_info;
		global $location_info;

		$prefix = $company_info['id'].$location_info['id']."0";
		$randomString = str_pad((string)rand(1, 9999), 4, "0", STR_PAD_LEFT);

		$str = $prefix."-".$randomString."-".date("YmdHis");

		return $str;
	}

	function shortDeviceUUID($uuid) {

		$str = "";
    	$uuid_parts = explode("-", $uuid);
    	if (count($uuid_parts) >= 2) $str = $uuid_parts[0]."-".$uuid_parts[1];
    	if (empty($str)) $str = $uuid;

 	   return $str;
	}

	function lavuShellEscapeArg($cmd){
		$cmd = escapeshellarg($cmd);
		return $cmd;
	}

	function logExecCall($cmd){
		global $data_name;
		$dn = empty($data_name) ? "" : $data_name;
		$efp = fopen("/home/poslavu/logs/lavushellexecs.txt","a");
		$logStr = date('Y-m-d H:i:s')."\t".$_SERVER['REMOTE_ADDR']."\t".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']."\t"."dataname:".$dn."\t".$cmd."\n\n";
		fwrite($efp,$logStr);
		fclose($efp);
	}

	function lavuShellExec($cmd){
		logExecCall($cmd);
		$cmd = escapeshellcmd($cmd);
		return shell_exec($cmd);
	}

	function lavuExec($cmd, &$output = null, &$returnValue = null){
		logExecCall($cmd);
		$cmd = escapeshellcmd($cmd);
		if(is_array($output)){
			return exec($cmd, $output, $returnValue);
		}else{
			return exec($cmd);
		}

	}

	function iOSplatformList() {

		$use_platform_list = array();
		$default_platform_count = 53; // should have at least this many (platforms available as of 2/11/2016)

		$platform_list = file($_SERVER['DOCUMENT_ROOT']."/ios_platform_list.txt");
		if (count($platform_list) >= $default_platform_count) {

			foreach ($platform_list as $list_line) {

				$platform_to_name = explode(chr(9), $list_line);
				if (count($platform_to_name) >= 2) {

					$platform = trim($platform_to_name[0]);
					$name = trim($platform_to_name[1]);
					if (strlen($platform)>0 && strlen($name)>0) {
						$use_platform_list[$platform] = $name;
					}
				}
			}
		}

		return $use_platform_list;
	}

	function distributeAutoGratuity($ag, $cash_paid, $card_paid, $gc_paid, $alt_paid, &$ag_cash, &$ag_card, &$ag_other) {

		if ($cash_paid >= $ag) {

			$ag_cash = $ag;

		} else {

			$ag_cash = $cash_paid;
			$ag -= ($ag_cash * 1);
			if ($card_paid >= $ag) {

				$ag_card = $ag;

			} else {

				$ag_card = $card_paid;
				$ag -= ($ag_card * 1);
				$use_other_paid = ($gc_paid + $alt_paid);

				if ($use_other_paid >= $ag) {

					$ag_other = $ag;

				} else {

					$ag_other = $use_other_paid;
					$ag -= ($ag_other * 1);
				}
			}
		}
	}

	function reliableRemoteIP()
	{
		return (!empty($_SERVER['HTTP_X_FORWARDED_FOR']))?$_SERVER['HTTP_X_FORWARDED_FOR']:$_SERVER['REMOTE_ADDR'];
	}

	function arrayValueForKey($arr, $key, $default="")
	{
		if (!is_array($arr))
		{
			return $default;
		}

		return (isset($arr[$key]))?$arr[$key]:$default;
	}

	function determineOrderStatusForOrder($order_stub)
	{
		// return empty string if unable to determine

		if (!is_array($order_stub))
		{
			return "";
		}

		$opened		= arrayValueForKey($order_stub, "opened");
		$closed		= arrayValueForKey($order_stub, "closed");
		$reopened	= arrayValueForKey($order_stub, "reopened_datetime");
		$reclosed	= arrayValueForKey($order_stub, "reclosed_datetime");
		$void		= arrayValueForKey($order_stub, "void");

		if ($void == "1")
		{
			return "voided";
		}

		if ($void == "2")
		{
			return "merged";
		}

		if (!empty($reclosed))
		{
			return "reclosed";
		}

		if (!empty($reopened))
		{
			return "reopened";
		}

		if (!empty($closed) && $closed!="0000-00-00 00:00:00")
		{
			return "closed";
		}

		if (!empty($opened) && $opened!="0000-00-00 00:00:00")
		{
			return "open";
		}

		return "";
	}

//relocated from jc_func8 and gateway_functions
	function convertPIN($PIN, $name_or_id="") {

		if ($PIN != "") {
			$get_auth_name = lavu_query("SELECT `id`, `f_name`, `l_name` FROM `users` WHERE `PIN` = '[1]' LIMIT 1", $PIN);
			if (@mysqli_num_rows($get_auth_name) > 0) {
				$extract = mysqli_fetch_assoc($get_auth_name);
				$auth_by = $extract['f_name']." ".$extract['l_name'];
				$auth_by_id = $extract['id'];
			} else {
				$auth_by = "PIN mismatch (".$PIN.")";
				$auth_by_id = "0";
			}
		} else {
			$auth_by = "No PIN to match";
			$auth_by_id = "0";
		}

		if ($name_or_id == "name") return $auth_by;
		else return $auth_by_id;
	}

	function locationSetting($key, $default="")
	{
		global $location_info;

		if ($location_info == NULL)
		{
			return $default;
		}

		if (!is_array($location_info))
		{
			return $default;
		}

		if (!isset($location_info[$key]))
		{
			return $default;
		}

		return $location_info[$key];
	}

	function isLLS($location_info)
	{
		if ($location_info == NULL)
		{
			return FALSE;
		}

		if (!is_array($location_info))
		{
			return FALSE;
		}

		$use_net_path = (int)$location_info['use_net_path'];
		if ($use_net_path <= 0)
		{
			return FALSE;
		}

		$net_path = $location_info['net_path'];
		if (strlen($net_path) == 0)
		{
			return FALSE;
		}

		if (isset($location_info['net_path_is_lls']))
		{
			return ((int)$location_info['net_path_is_lls'] == 1);
		}

		return (!strstr($net_path, "lavu") || strstr($net_path, "lsvpn"));
	}

	/*
	 * lavuDeveloperStatus
	 * Checks all primary areas where we attempt to
	 * discern whether or not we are in a DEV environment.
	 * Returns TRUE, 1 or 2 if we are a DEV
	 * Returns FALSE or 0 if we are not.
	 */
	function lavuDeveloperStatus(){
	    $isDEV = FALSE;

        if(isset($_REQUEST['is_dev'])){
            $isDEV = $_REQUEST['is_dev'];
        }
        if(isset($_SESSION['is_dev']) && !$isDEV){
            $isDEV = $_SESSION['is_dev'];
        }
        if(isset($_SESSION['mt_is_dev']) && !$isDEV){ // management tools reqsessvar()
            $isDEV = $_SESSION['mt_is_dev'];
        }
        if(isset($_SERVER['SERVER_NAME']) && strstr($_SERVER['SERVER_NAME'], "dev") && !$isDEV){
            $isDEV = true;
        }
        if(isset($_SESSION['poslavu_234347273_admin_dev'] ) && !$isDEV){
            $isDEV = $_SESSION['poslavu_234347273_admin_dev'];
        }

        if($isDEV === FALSE || (int)$isDEV === 0){
            return FALSE;
        }
        return TRUE;
	}

	/*
	 * isLocalhost
	 * Checking the HTTP_HOST to see if it match admin.localhost
	 * or 'localhost:8888' or 127.0.0.1
	 * Also checking _SERVER['SERVER_NAME'] if it match localhost
	 */
	function isLocalhost(){
		return $_SERVER['HTTP_HOST'] === 'admin.localhost' ||
					 $_SERVER['HTTP_HOST'] === 'localhost:8888' ||
					 $_SERVER['SERVER_NAME'] === 'localhost' ||
					 $_SERVER['SERVER_NAME'] === "cp.devlavu.test";
	}

	/**
	 * isCP3BrowserSupported
	 * Checks if the browser making the request is mobile
	 * 
	 * @param WhichBrowser\Parser $browserInfo
	 */
	function isMobileBrowser($browserInfo) {
		return $browserInfo->isMobile();
	}

	/**
	 * isCP3BrowserSupported
	 * Checks if the browser making the request if supported
	 * 
	 * @param WhichBrowser\Parser $browserInfo
	 */

	function isCP3BrowserSupported ($browserInfo) {
		return in_array($browserInfo->browser->name, ['Chrome', 'Safari', 'Mobile Safari', 'Firefox', 'Edge']);
	}

	/**
	 * isExternalAdmin
	 * Checks if the current user comes from Manage or Distro (Exteneral Admin Pages)
	 */
	function isExternalAdmin() {
		return substr(sessvar('admin_username'), -strlen('poslavu_admin')) == 'poslavu_admin' || substr(sessvar('admin_username'), 0, strlen('poslavu_distro_')) == 'poslavu_distro_';
	}

	// isCustomerCP3Beta will return false if ENV variable for CP3 is not set to 
	// true or not set at all
	function isCustomerCP3Beta(){
		if (!isset($_SERVER['IS_CP3_ENV']) || strtolower($_SERVER['IS_CP3_ENV']) !== 'true') {
			return false;
		}

		global $modules;
		return (sessvar('admin_access_level') == 3 || sessvar('admin_access_level') == 4) && $modules->hasModule('extensions.limited_cp_beta');
	}

	/**
	 * Logic to check if customer is using CP3 Beta
	 */
	function isNewControlPanelActive() {
		if (!isset($_SERVER['IS_CP3_ENV']) || strtolower($_SERVER['IS_CP3_ENV']) !== 'true') {
			return false;
		}

		$data_name = sessvar("admin_dataname");

		$query = mlavu_query("SELECT is_new_control_panel_active
													FROM `poslavu_MAIN_db`.`restaurants`
													WHERE `disabled` = '0' AND `data_name`='".$data_name."' LIMIT 1");
		$query_read = mysqli_fetch_assoc($query);
		$isNewControlPanel = $query_read['is_new_control_panel_active'];

		return boolval($isNewControlPanel);
	}

	/**
	 * Function that populates superglobal $_ENV with
	 * environment information set in the env.json file
	 */
	function populateEnvVars() {
		$envFilePath = dirname(__FILE__) . "/../../../../env.json";

		// Copy variables set in $_SERVER to $_ENV
		foreach($_SERVER as $key => $value) {
			$_ENV[$key] = $value;
		}

		// Override environment variables using config file

		/*
									*****WARNING*****
			this file will not exist on the server when its deployed, do not rely on this scheme
			we will be removing it pending a discussion with devops
		*/
		if (file_exists($envFilePath)) {
			$envVars = json_decode(
				file_get_contents($envFilePath),
				true
			);
			$currentEnv = isset($_SERVER['ENVIRONMENT']) ? $_SERVER['ENVIRONMENT'] : 'development';

			foreach($envVars[$currentEnv] as $key => $envVar) {
				// Only override ENV variables if there's an actual value to set
				if ($envVar) {
					$_ENV[$key] = $envVar;
				}
			}
		}
		// 							*******END*******

	}

	function ltgDebug($filepath, $message, $force=false)
	{
		global $ltg_debug;
		global $data_name;
		global $lavu_req_id;

		if (!$ltg_debug && !$force)
		{
			return;
		}

		if (empty($data_name))
		{
			$data_name = "<no data_name>";
		}

		if (empty($lavu_req_id))
		{
			$lavu_req_id = "S".lavuRequestID();
			// ** DEBUG LOGGING ** //
			error_log("LTG - ".$data_name." - assigned req_id: ".$lavu_req_id);
		}

		// ** DEBUG LOGGING ** //
		error_log("LTG - ".$data_name." - req_id: ".$lavu_req_id." - ".filePathToName($filepath)." - ".$message);
	}

	function lavuRequestID()
	{
		return str_pad((string)rand(1, 9999), 4, "0", STR_PAD_LEFT);
	}

	function currentLavuBackendVersion()
	{
		$current_version = "3.7.7.0"; // The 0 at the end will help indicate that the default value is being returned.

		$release_txt_path = __DIR__."/../../release.txt"; // /home/poslavu/public_html/admin/release.txt
		if (file_exists($release_txt_path))
		{
			$current_version = trim(file_get_contents($release_txt_path, TRUE, null, 0, 64));
			if ((strlen($current_version) == "40") && !strstr($current_version, "."))
			{
				// Seems to be a container deployment, so return the first 8 characters of the commit sha-1
				$current_version = substr($current_version, 0, 8);
			}
		}

		return $current_version;
	}

	function filePathToName($filepath)
	{
		$fp_parts = explode("/", $filepath);
		$fp_count = count($fp_parts);
		$filename = $fp_parts[($fp_count - 1)];

		return $filename;
	}

	function loyalTreeEnabledForLocation($loc_info)
	{
		return (false && $loc_info['enable_loyaltree']=="1" && (($loc_info['loyaltree_username']!="" &&  $loc_info['loyaltree_password']!="") || ($loc_info['loyaltree_id']!="" && $loc_info['loyaltree_chain_id']!="")));
	}

	function formatValueForMonetaryDisplay($monetary_str, $displayMonetarySymbol=FALSE){
		if(empty($monetary_str)){
			$monetary_str = "0";
		}
		global $location_info;
		//Get money format information
		$left_or_right = $location_info['left_or_right'];
		$htmlEntitiesMonetary = htmlentities($monetary_str);
		$monetary_str = preg_replace(($left_or_right == 'left') ? "/&lrm;/" : "/&rlm;/", "", $htmlEntitiesMonetary);
		empty($location_info['monitary_symbol'])    ? $monetary_symbol  ="$"  : $monetary_symbol=$location_info['monitary_symbol'];
		empty($location_info['decimal_char'])       ? $decimal_char     ="."  : $decimal_char=$location_info['decimal_char'];
		empty($location_info['thousands_char'])     ? $thousands_char   =","  : $thousands_char=$location_info['thousands_char'];
		//Who the fuck names a field for the number of decimal places "DISABLE_DECIMAL"? You've gotta be fucking kidding me...
		//My guess is someone created a disable decimal field but then decided to extend it's functionality. Bold move.
		(!isset($location_info['disable_decimal'])) ? $decimal_places   = 2   : $decimal_places=$location_info['disable_decimal'];

		$monetary_str = str_ireplace($monetary_symbol, '', $monetary_str);
		$decimal_char_position = strpos($monetary_str, $decimal_char);
		if($decimal_char_position === false){
			//Append decimal char if it's not there so next steps work predictably.
			$monetary_str .= $decimal_char;
			$decimal_char_position = strpos($monetary_str, $decimal_char);
		}

		//Replace everything but digits, and then reinsert the decimal character.
		$removeAllButRegexp = "/[^0-9]/";
		$pre_decimal = substr($monetary_str, 0, $decimal_char_position);
		$needToAddMinus="";
		if(strpos($pre_decimal,"-") !== false){
			$needToAddMinus = "-";
		}
		$post_decimal = substr($monetary_str, $decimal_char_position+1);
		$pre_decimal = preg_replace($removeAllButRegexp, "", $pre_decimal);
		$post_decimal = preg_replace($removeAllButRegexp, "", $post_decimal);
		$monetary_str = $needToAddMinus . $pre_decimal . $decimal_char . $post_decimal;
		$monetary_str = trim($monetary_str, "0"); //remove leading zeros
		$monetary_str = rtrim($monetary_str, "0"); //remove trailing zeros

		if ($decimal_char == ",") {
			$monetary_str = unFormatCurrencyForInsert($monetary_str);
			return $monetary_str;
		}

		// LP-1514 -- Remove monetary_symbol from monetary_str before decimal_char_position is calculated
		// with multi-byte string functions (for monteary_symbols comprised of multiple bytes, like £) and
		// without just doing a search and replace over the whole string (in case the monetary_symbol is
		// somehow a substring of the monetary_str [semes like that wouldn't happen but you never know])
		if (!empty($monetary_symbol)) {
			// Get length and positioning of the monetary_symbol
			$monetary_symbol_length = mb_strlen($monetary_symbol, 'utf-8');
			$monetary_symbol_offset = (!empty($location_info['left_or_right']) && $location_info['left_or_right'] == 'right') ? -1 * $monetary_symbol_length : 0;

			// Get chunk of monetary_str where the monetary_symbol should be
			$monetary_str_length = mb_strlen($monetary_str, 'utf-8');
			$monetary_str_start_or_end = mb_substr($monetary_str, $monetary_symbol_offset, $monetary_symbol_length, 'utf-8');

			// See if the monetary_symbol apepars in the monetary_str and strip it out, if so
			if ($monetary_str_start_or_end == $monetary_symbol) {
				$monetary_str_offset = ($monetary_symbol_offset < 0) ? 0 : $monetary_symbol_length;
				$monetary_str = mb_substr($monetary_str, $monetary_str_offset, $monetary_str_length - $monetary_symbol_length, 'utf-8');
			}
		}

		//Get number of decimals after the decimal character.
		$decimal_char_position = strpos($monetary_str, $decimal_char);

		//If the decimal point is the first character, insert a 0 as a 1's place-holder, and update the decimal char position.
		if($decimal_char_position === 0)
		{
			$monetary_str = "0".$monetary_str;
			$decimal_char_position = 1;
		}
		$money_length = strlen($monetary_str);

		//remaining_decimal_places = Decimal places needed minus existing decimal places.
		$remaining_decimal_places = $decimal_places -  (($money_length-1) - $decimal_char_position);

		if($remaining_decimal_places >= 0)
		{
			//append 0's if there aren't enough decimal places.
			while($remaining_decimal_places > 0)
			{
				$monetary_str .= "0";
				$remaining_decimal_places--;
			}
		}
		else
		{
			//trim off excess decimals if there are too many decimal places.
			while($remaining_decimal_places < 0)
			{
				$monetary_str = substr($monetary_str,0,-1);
				$remaining_decimal_places++;
			}
		}

		if($displayMonetarySymbol) {
			if ($left_or_right == "left") {
				$monetary_str = $monetary_symbol . $monetary_str;
			} else if ($left_or_right == "right") {
				$monetary_str .= $monetary_symbol;
			}
		}
//		$monetary_num = floatval($monetary_str); //Convert to float
		return $monetary_str;
	}

	function unFormatCurrencyForInsert($monetary_str, $monitary_symbol=FALSE) {

		global $location_info;

		if ($monitary_symbol === false) $monitary_symbol = isset($location_info['monitary_symbol']) ? trim($location_info['monitary_symbol']) : '';

		$decimal_char = isset($location_info['decimal_char']) ? trim($location_info['decimal_char']) : '.';

		$monetary_str = str_replace($monitary_symbol, '', $monetary_str);
		$explodeResultArray = explode($decimal_char, $monetary_str);
		$lastPart = "";
		$final_monetary_str = "";
		if (count($explodeResultArray) >1 )	{
			$lastPart = array_pop($explodeResultArray);
		}

		$firstPart = implode($explodeResultArray, '');

		if (!empty($firstPart)) $final_monetary_str .= str_replace(array('.', ','), '', $firstPart);
		if (!empty($lastPart)) $final_monetary_str .= ".". $lastPart;

		return $final_monetary_str;
	}

	/**
	 * Added for LP-3837, This method is to return err string with title and body based on the language pack id setting for respective location

	 * @param String $dataname 	location dataname
	 * @param String $tag 	error tag name
	 * @return Array $err 	array element title and body
	*/
	function getErrMessage($tag, $replace = '') {
		$err=array('title'=>'Unknown Error','body'=>'Unknown Error');
		$sessarr=sessvar('location_info');
		$active_language_pack_id = $sessarr['use_language_pack'];
		if ($tag && $sessarr['use_language_pack']) {
			$message = json_decode(translator($tag), 1);
			if (!empty($message)) {
				if (is_array($replace)){
					$message['body'] = str_replace('{val1}',$replace['val1'],$err['body']);
				}
				$err['title'] = $message['title'];
				$err['body'] = $message['body'];
			}
		}
		return $err;
	}

	/**
	 * Get transaltor of message based on tag
	 * @param $tag string (like ERR_DEFAULT_1)
	 * @return string
	 */
	function translator($tag, $placeholder = array(), $placeholderValue = array())
	{
		$defaultTag = 'ERR_DEFAULT_1';
		$tag = trim($tag);
		if ((string)$tag == "")
		{
			return "";
		}

		global $active_language_pack_id, $active_language_pack;

		if (!isset($active_language_pack_id))
		{
			$active_language_pack_id = getActiveLanguagePackId();
			//$active_language_pack_id = sessvar("active_language_pack_id");
		}

		if ($active_language_pack_id === FALSE)
		{
			$active_language_pack_id = locationSetting("use_language_pack", "1");
		}

		if (!$active_language_pack)
		{
			$active_language_pack = getLanguageDictionary(sessvar("locationid"));
			//$active_language_pack = sessvar("active_language_pack");
		}

		if (isset($active_language_pack[$tag]) && $active_language_pack[$tag] != "")
		{
			return replacePlaceholder($active_language_pack[$tag], $placeholder, $placeholderValue);
		} else {
			// `word_or_phrase` isn't defined in the `language_dictionary`, so let's create a new set of entries to hold translations

			$utc_datetime = UTCDateTime(TRUE);

			$backtrace = debug_backtrace();
			$english_query = mlavu_query("SELECT `id`, `word_or_phrase`, `used_by` FROM `poslavu_MAIN_db`.`language_dictionary` WHERE `pack_id` = '[1]' AND `tag` = '[2]'", 1, $tag);
			if (mysqli_num_rows($english_query) > 0) {
				$english_record = mysqli_fetch_assoc($english_query);
				$english_id = $english_record['id'];
				$english_word_phrase = $english_record['word_or_phrase'];
				$used_by = $english_record['used_by'];

				$get_packs = mlavu_query("SELECT `id` FROM `poslavu_MAIN_db`.`language_packs` WHERE `id` != '1'");
				while ($pack = mysqli_fetch_assoc($get_packs)) {
					$check_translation = mlavu_query("SELECT `id`, `word_or_phrase` FROM `poslavu_MAIN_db`.`language_dictionary` WHERE `tag` = '[1]' AND `pack_id`= '[2]'", $tag, $pack['id'], NULL, NULL, NULL, NULL, FALSE);
					if (mysqli_num_rows($check_translation) > 0) {
						$translation = mysqli_fetch_assoc($check_translation);
						if ($english_word_phrase != $translation['word_or_phrase']) {
							$set_english_id = mlavu_query("UPDATE `poslavu_MAIN_db`.`language_dictionary` SET `word_or_phrase` = '[1]' WHERE `id` = '[2]'", $english_word_phrase, $translation['id'], NULL, NULL, NULL, NULL, FALSE);
						}
					} else {
						$add_translation = mlavu_query("INSERT INTO `poslavu_MAIN_db`.`language_dictionary` (`pack_id`, `word_or_phrase`, `english_id`, `used_by`, `created_date`, `modified_date`, `tag`) VALUES ('[1]', '[2]', '[3]', '[4]', '[5]', '[5]', '[6]')", $pack['id'], $english_word_phrase, $english_id, $used_by, $utc_datetime, $tag, NULL, NULL, FALSE);
					}
				}
				deleteAllLanguagePack();
				return replacePlaceholder($english_word_phrase, $placeholder, $placeholderValue);
			} else {
				return $active_language_pack[$defaultTag] . ' ' . $tag;
			}
		}
	}

	/**
	 * @param $str message string with placeholder
	 * @param array $placeholder array of placeholder
	 * @param array $placeholderValue array of actual value
	 * @return string with actual value
	 */

	function replacePlaceholder($str, $placeholder, $placeholderValue) {
		if (!empty($placeholder) && !empty($placeholderValue)) {
			$str = str_replace($placeholder, $placeholderValue, $str);
		}
		return $str;
	}

	/**
	 * This method is used to get migration status of Inventory
	 * @return string Completed_Ready|Skipped_Ready|Legacy|''
	 */
	function getInventoryStatus() {
		$query = "select `value` from `config` where `setting` = 'INVENTORY_MIGRATION_STATUS' AND `_deleted` = 0";
		$result = lavu_query($query);
		$resultArray = mysqli_fetch_assoc($result);
		if ( isset($resultArray['value']) ) {
			return $resultArray['value'];
		} else {
			return '';
		}
	}

	/**
	 * This is common main config method to get records of specific setting
	 * @param $setting string
	 * @return value numeric|json|string
	 */
	function getMainConfig($setting) {
		$query = "select `value` from `main_config` where `setting` = '[1]' AND `_deleted` = 0";
		$result = mlavu_query($query, $setting);
		$resultArray = mysqli_fetch_assoc($result);
		return $resultArray['value'];
	}

	/**
	 * This method is used to check cache is enabled or not first get from session if not available then trigger query
	 * @input
	 * @return boolean 0|1
	 */
	function isCacheEnabled() {
		$enabledCache = getenv('ENABLED_REDIS');
		set_sessvar("ENABLED_REDIS", $enabledCache);
		return $enabledCache;
	}

	/**
	 * Function to get the client ip address, $_SERVER is used to get the values from the web server (e.g. apache)
	 * @return string|IPAddress
	 */
	function getClientIPServer() {
		if (isset($_SERVER['HTTP_CLIENT_IP']) && $_SERVER['HTTP_CLIENT_IP'])
			$ipaddress = $_SERVER['HTTP_CLIENT_IP'];
		else if (isset($_SERVER['HTTP_X_FORWARDED_FOR']) && $_SERVER['HTTP_X_FORWARDED_FOR'])
			$ipaddress = $_SERVER['HTTP_X_FORWARDED_FOR'];
		else if (isset($_SERVER['HTTP_X_FORWARDED']) && $_SERVER['HTTP_X_FORWARDED'])
			$ipaddress = $_SERVER['HTTP_X_FORWARDED'];
		else if (isset($_SERVER['HTTP_FORWARDED_FOR']) && $_SERVER['HTTP_FORWARDED_FOR'])
			$ipaddress = $_SERVER['HTTP_FORWARDED_FOR'];
		else if (isset($_SERVER['HTTP_FORWARDED']) && $_SERVER['HTTP_FORWARDED'])
			$ipaddress = $_SERVER['HTTP_FORWARDED'];
		else if (isset($_SERVER['REMOTE_ADDR']) && $_SERVER['REMOTE_ADDR'])
			$ipaddress = $_SERVER['REMOTE_ADDR'];
		else
			$ipaddress = 'UNKNOWN';

		return $ipaddress;
	}
	function db_error(&$resp)
	{
		$resp['status']	= "no_db";
		$resp['info']	= mlavu_dberror();
	}
	function rdb_error(&$resp)
	{
		$resp['status']	= "no_db";
		$resp['info']	= lavu_dberror();
	}
	
	/**
	 * To check extension is enable or disable
	 *
	 * @param string extension name
	 */
	function toGetExtensionStatus($ex_title, $dataname) {
		$return = 'disable';
		$modules = mlavu_query("SELECT `modules` FROM `poslavu_MAIN_db`.`restaurants` WHERE `data_name` = '[1]' ", $dataname);
		$modules = mysqli_fetch_assoc($modules);
		$modules = $modules['modules'];
		if( strpos( $modules, $ex_title ) !== false) {
			$return = 'enable';
		}
		return $return;
	}

	/**
	 * This function is used to get kiosk settings.
	 * @param int $locid
	 * @param string $use_db
	 * @return array
	 */
	function getKioskSettings ($locid, $use_db="") {

		//To get for kiosk integration details.
		$enc_query = lavu_query("SELECT setting, AES_DECRYPT(`value`, '[1]') as `value` FROM ".$use_db."kiosk_settings WHERE `setting` like 'kiosk_integration%' AND _deleted = 0 and location='[2]'", integration_keystr(), $locid);
		$kioskSettings = array();
		while ($kioskInfo = mysqli_fetch_assoc($enc_query))
		{
			$kioskSettings[$kioskInfo['setting']] = $kioskInfo['value'];
		}

		$refQuery = lavu_query("select setting, value from ".$use_db."kiosk_settings where _deleted = 0 and location='[1]' and `setting` in ('kiosk_gateway', 'kiosk_cc_transtype')", $locid);
		while ($kioskRefInfo = mysqli_fetch_assoc($refQuery))
		{
			$kioskSettings[$kioskRefInfo['setting']] = $kioskRefInfo['value'];
		}

		return $kioskSettings;
	}

	/**
	 * This function is used to get kiosk related gateway integration details.
	 * @param int $locid
	 * @param string $gateway
	 * @param string $use_db
	 * @return boolean|array
	 */
	function getIntegrationFromKioskSettings($locid, $gateway, $use_db="") {
		$kioskIntegration = array();
		if($use_db && $use_db != "") {
			$use_db = "`$use_db`.";

			$newQueryFieldsStr="";
			$kioskSettings = getKioskSettings($locid, $use_db);

			if(isset($kioskSettings['kiosk_gateway']) && $kioskSettings['kiosk_gateway'] == $gateway) {
				$kioskIntegration['integration1'] = $kioskSettings['kiosk_integration1'];
				$kioskIntegration['integration2'] = $kioskSettings['kiosk_integration2'];
				$kioskIntegration['integration3'] = $kioskSettings['kiosk_integration3'];
				$kioskIntegration['integration4'] = $kioskSettings['kiosk_integration4'];
				$kioskIntegration['integration5'] = $kioskSettings['kiosk_integration5'];
				$kioskIntegration['integration6'] = $kioskSettings['kiosk_integration6'];
				$kioskIntegration['integration7'] = $kioskSettings['kiosk_integration7'];
				$kioskIntegration['integration8'] = $kioskSettings['kiosk_integration8'];
				$kioskIntegration['integration9'] = $kioskSettings['kiosk_integration9'];
				$kioskIntegration['integration10'] = $kioskSettings['kiosk_integration10'];
				$kioskIntegration['gateway'] = $kioskSettings['kiosk_gateway'];
				$kioskIntegration['cc_transtype'] = $kioskSettings['kiosk_cc_transtype'];
			}
		}

		return $kioskIntegration;
	}

	/**
	 * This function is used to get gateway of order.
	 * @param string $orderid
	 * @param int $locid
	 * @param string $use_db
	 * @return string $gateway.
	 */
	function getKioskGateway($orerid, $locid, $use_db="") {
		$gateway = '';
		if($use_db && $use_db != "") {
			$use_db = "`$use_db`.";

			$refQuery = lavu_query("select `gateway` from ".$use_db."cc_transactions where transaction_id != '' and order_id='[1]' and `transtype` = 'Sale' and `refunded` = '0' AND `processed` = '0' and `loc_id` = '[2]' and `voided` = '0'", $orerid, $locid);
			$resultArray = mysqli_fetch_assoc($refQuery);

			if(mysqli_num_rows($refQuery) > 0) {
				$gateway = $resultArray['gateway'];
			}

			if ($gateway == '') {
				$kioskSettings = getKioskSettings($locid, $use_db);
				$gateway = (isset($kioskSettings['kiosk_gateway'])) ? $kioskSettings['kiosk_gateway'] : '';
			}
		}

		return $gateway;
	}
	
	function apiSpeak($pack_id, $str) {
	    
	    global $active_language_pack_id;
	    global $active_language_pack;
	    
	    $active_language_pack_id = $pack_id;
	    $active_language_pack = load_language_pack($active_language_pack_id, 1);
	    return speak($str);
	}

	function check_location_id($rdb, $locationid) {
		if ((int)$locationid == 0 && trim($rdb) !== '') {
			$query = mlavu_query("SELECT `id` FROM `[1]`.`locations` WHERE `_disabled` = '0' LIMIT 1", $rdb);
			if ($query !== FALSE && mysqli_num_rows($query) > 0) {
				$query_read = mysqli_fetch_assoc($query);
				$locationid = $query_read['id'];
				set_sessvar('locationid', $locationid);
			}
		}

		return $locationid;
	}

	/**
	 * @purpose This function is used to get location is authorized or not
	 * @param string $dataName
	 * @return string $isAuth
	 */
	function isLocationAuthorized($dataName) {

		include_once("../../sa_cp/billing/billing_lockout_functions.php");

		//Check Dataname Active Status & Prevent API Calls if Disabled
		$dnStatus = '';
		$dnBillStatus = '';
		$disabled = 1;
		$isAuth = true;
		$datanameStatusQuery = "SELECT count(*) as rows FROM `poslavu_MAIN_db`.`restaurants` WHERE `data_name`='[1]' AND `disabled`='[2]'";
		$executeDatanameQuery = mlavu_query($datanameStatusQuery, $dataName, $disabled);
		if (mysqli_num_rows($executeDatanameQuery)) {
			$datanameStatus = mysqli_fetch_assoc($executeDatanameQuery);
			$dnStatus = $datanameStatus['rows'];
		}

		//Datamname Billing Status
		$dnBillStatus = get_collection_lockout_status();

		//Finalise Result
		if ($dnStatus == 1 || $dnBillStatus == 'locked') {
		$isAuth = false;
		}

		//Return Results
		return $isAuth;
	}

	/**
	* This function is used to get image details on the basis of s3 storage key.
	*
	* @param string $storageKey.
	*
	* @return array $imageInfo.
	*/
	function getImageInfo($storageKey)
	{
		$resultRaw = lavu_query("SELECT * FROM file_uploads WHERE `storage_key` = '[1]'", $storageKey);
		$imageInfo = mysqli_fetch_assoc($resultRaw);
		return $imageInfo;
	}

	/**
	* This function is used to get language dictionary from session or redis or DB
	* This method is called by web view. webview open in POS, If POS change language pack, 
	* then webview language should change accrodingly
	*
	* @param id $pack_id new selected language pack id.
	* @param id $usedLanguagePack used language pack.
	* @param id $locationId. location id
	*
	* @return array $active_language_pack language dictionary array.
	*/
	function getWebViewLanguagepack($pack_id, $locationId) {
		global $active_language_pack, $active_language_pack_id;
		$active_language_pack_id = getActiveLanguagePackId();
		if ($pack_id != '' && $pack_id != $active_language_pack_id) {
			$locationId = ($locationId) ? $locationId : 1;
			unset_sessvar("active_language_pack");
			unset_sessvar("active_language_pack_id");
			$active_language_pack_id = $pack_id;
            $active_language_pack = load_language_pack($active_language_pack_id, $locationId);
            setLanguageDictionary($active_language_pack, $locationId);
            setActiveLanguagePackId($pack_id);
		} else {
			if (empty($active_language_pack)) {
				$active_language_pack_id = getActiveLanguagePackId();
				$active_language_pack = load_language_pack($pack_id, $locationId);
				setLanguageDictionary($active_language_pack, $locationId);
			}
		}
		return $active_language_pack;
	}
} // end-if-(!isset($__LAVU_CORE_FUNCTIONS_H__))
?>