<?php


	global $a_helptexts_by_file;
	global $s_global_help_text_show_script;

	$a_helptexts_by_file = array();
	$s_global_help_text_show_script = "
				<script type='text/javascript'>
					window.count_keys_up_[i_helptext_id] = 0;
					window.func_count_keys_up_[i_helptext_id] = function(e) {
						if (e.keyCode == 69) {
							if (!window.count_keys_up_[i_helptext_id])
								window.count_keys_up_[i_helptext_id] = { last_press_time: 0, count: 0 };
							var currtime = new Date().getTime();
							if (currtime > window.count_keys_up_[i_helptext_id].last_press_time+300)
								window.count_keys_up_[i_helptext_id].count = 0;
							window.count_keys_up_[i_helptext_id].count++;
							window.count_keys_up_[i_helptext_id].last_press_time = currtime;
						}
						if (window.count_keys_up_[i_helptext_id] && window.count_keys_up_[i_helptext_id].count == 3) {
							$('#help_box_[i_helptext_id]').show();
							$('#help_box_[i_helptext_id]').css({ display: 'inline-block' });
						}
					}
					$(document).keyup(window.func_count_keys_up_[i_helptext_id]);
				</script>";
	$b_request_uri_dont_show = (strpos($_SERVER['REQUEST_URI'],"mode=billing") !== FALSE);

	// used to create help boxes for, example, dimensional_form.php
	// it finds a matching help string if one exists
	// it creates a new help string if one doesn't exist
	// @$s_filename: name of the file that called this funciton
	// @$s_setting: setting name that is being modified
	// @$s_table: table that is being modified
	// @return: the results of help_mark_tostring()
	function create_help_mark($s_filename, $s_table, $s_setting)
	{
		global $a_helptexts_by_file;

		// check if this has already been pulled
		if (isset($a_helptexts_by_file[$s_filename]) && isset($a_helptexts_by_file[$s_filename][$s_table]) && isset($a_helptexts_by_file[$s_filename][$s_table][$s_setting]))
		{
			return $a_helptexts_by_file[$s_filename][$s_table][$s_setting];
		}

		// load from the database for this filename
		if (!isset($a_helptexts_by_file[$s_filename]) && $s_filename!=="")
		{
			// choose which column to load as html
			$s_column = "contents";
			$s_hyperlinks_column = "hyperlinks";

			// load from database
			$a_table_rows = ConnectionHub::getConn('poslavu')->getDBAPI()->getAllInTable('helpText', array('page'=>$s_filename), TRUE);
			$a_helptexts_by_file[$s_filename] = array();
			foreach($a_table_rows as $s_id => $a_row)
			{
				if ($a_row['table'] == "")
				{
					continue;
				}

				if (!isset($a_helptexts_by_file[$s_filename][$a_row['table']]))
				{
					$a_helptexts_by_file[$s_filename][$a_row['table']] = array();
				}
				$a_helptexts_by_file[$s_filename][$a_row['table']][$a_row['setting']] = help_mark_tostring($a_row['id'], $a_row[$s_column], TRUE, $a_row[$s_hyperlinks_column]);
			}
		}

		// check again if this has already been pulled
		if (isset($a_helptexts_by_file[$s_filename]) && isset($a_helptexts_by_file[$s_filename][$s_table]) && isset($a_helptexts_by_file[$s_filename][$s_table][$s_setting]))
		{
			return $a_helptexts_by_file[$s_filename][$s_table][$s_setting];
		}

		// create it if not existing
		$a_insert_vars = array('page'=>$s_filename, 'table'=>$s_table, 'setting'=>$s_setting, '_deleted'=>'0');
		mlavu_query("INSERT INTO `poslavu_MAIN_db`.`helpText` ".ConnectionHub::getConn('poslavu')->getDBAPI()->arrayToInsertClause($a_insert_vars), $a_insert_vars);
		$s_html = help_mark_tostring(mlavu_insert_id());
		if (!isset($a_helptexts_by_file[$s_filename]))
		{
			$a_helptexts_by_file[$s_filename] = array();
		}
		if (!isset($a_helptexts_by_file[$s_filename][$s_table]))
		{
			$a_helptexts_by_file[$s_filename][$s_table] = array();
		}
		$a_helptexts_by_file[$s_filename][$s_table][$s_setting] = $s_html;

		return $s_html;
	}

	// draws a help marker question mark
	// that can be clicked on to open the help window
	// @$i_helptext_id: id of the help text in poslavu_MAIN_db
	// @$s_html: if not empty string then uses this string instead
	// @$b_force_empty: forces use of $s_html, even if it is empty
	function help_mark_tostring($i_helptext_id, $s_html = '', $b_force_empty = FALSE, $s_hyperlinks = '', $b_html_from_editor = FALSE)
	{
		global $s_global_help_text_show_script;

		$s_showscript = "";
		$s_display = "inline-block";
		$s_notempty_class = " not-empty";

		// find the help text
		if ($s_html=="" && !$b_force_empty)
		{
				$a_search = array('id'=>$i_helptext_id, '_deleted'=>0);

			$a_helptexts = ConnectionHub::getConn('poslavu')->getDBAPI()->getAllInTable('helpText', $a_search, TRUE);
			if (count($a_helptexts) == 0)
			{
				$s_html = "We're sorry. This help box is current empty.";
			}
			else
			{
				$s_column = "contents";
				$s_hyperlinks_column = "hyperlinks";
				$s_html = $a_helptexts[0][$s_column];
				$s_hyperlinks = $a_helptexts[0][$s_hyperlinks_column];
			}
		}

		// check that the help text isn't empty
		if ($s_html == "")
		{
			$s_html = "";
			$s_notempty_class = "";
		}

		if ($s_html!=="" && !$b_html_from_editor)
		{
			$s_html = help_mark_translate_html($s_html);
		}

		// replace some text
		$s_html = str_replace('(uline)', '<font style="text-decoration:underline;">', $s_html);
		$s_html = str_replace('(ulineend)', '</font>', $s_html);
		$s_html = str_replace('(bold)', '<font style="font-weight:bold;">', $s_html);
		$s_html = str_replace('(boldend)', '</font>', $s_html);
		$s_html = str_replace('(ital)', '<font style="font-style:italic;">', $s_html);
		$s_html = str_replace('(italend)', '</font>', $s_html);

		// insert hyperlinks
		if ($s_hyperlinks !== '') {
			require_once(dirname(__FILE__).'/../objects/json_decode.php');
			$o_json = JSON::unencode($s_hyperlinks);
			if (count($o_json) > 0) {
				foreach($o_json as $o_hyperlink) {
					$index = $o_hyperlink->index;
					$value = $o_hyperlink->value;
					$s_html = str_replace("(link{$index})", "<a href=\"{$value}\" target=\"_blank\">", $s_html);
					$s_html = str_replace("(linkend{$index})", '</a>', $s_html);
				}
			}
		}

		// get the edit id
		$i_edit_id = $i_helptext_id;

		if(!admin_info("lavu_admin")){
			$i_edit_id = 0;
		}

		$s_onclick = "display_help_box(null, \"help_mark_tostring_".$i_helptext_id."\", true, ".$i_edit_id.");";

		$s_retval = "";
		if ($s_html !== "")
		{
			$s_retval = get_help_mark_styling_using_html($s_display, $i_helptext_id, $s_html, $s_showscript, $s_onclick, $s_notempty_class);
		}

		return $s_retval;
	}

	function help_mark_translate_dom_document($o_dom, $i_child_index=-1, $o_parent=NULL)
	{
		// preventing translation via speak() for now pending reevaluation of this process - RJG 8/30/2016
		/*if (count($o_dom->childNodes) == 0)
		{
			$s_value = trim($o_dom->nodeValue);
			if ($s_value!=="" && $i_child_index!==-1)
			{
				$s_showval = $o_dom->nodeValue;
				$s_showval = speak($s_showval);
				$o_dom->nodeValue = $s_showval;
			}
		}
		else
		{
			foreach($o_dom->childNodes as $k => $child_node)
			{
				help_mark_translate_dom_document($child_node, $k, $o_dom);
			}
		}*/
	}

	function help_mark_translate_html($s_html)
	{
		// standardize whitespace
		$s_html = str_replace(array("\n", "\r", "\n\r", "\t"), ' ', $s_html);
		while(strpos($s_html, '  ') !== FALSE)
			$s_html = str_replace('  ', ' ', $s_html);
		$s_html = trim($s_html);
		if ($s_html == "")
			return "";

		// build the dom object
		$o_dom = new DOMDocument;
		$o_dom->loadHTML($s_html);
		help_mark_translate_dom_document($o_dom);
		$s_html = $o_dom->saveHTML();

		return $s_html;
	}

	function get_help_mark_styling_using_html($s_display, $i_helptext_id, $s_html, $s_showscript, $s_onclick, $s_class="")
	{
		return "
<div src='/cp/images/question.png' alt='help' style='cursor:pointer; background-image:url(\"/cp/images/question.png\"); width:14px; height:15px; border-radius:10px; background-position-x:-2px; background-position-y:-1px; display:".$s_display.";' onclick='".$s_onclick."' id='help_box_".$i_helptext_id."' class='".$s_class."'></div>
<div id='help_mark_tostring_".$i_helptext_id."' style='display:none;'>".$s_html."
</div>".$s_showscript;
	}

	function assign_file_table_setting_names($s_fieldname, $a_backtrace, &$s_filename, &$s_tablename, &$s_settingname) {

		// get some global vars
		global $s_browe_includes_form_tablename;

		// get the filename
		$s_use_filename = "";
		for ($i = 0; $i < count($a_backtrace); $i++)
		{
			$s_filename = basename($a_backtrace[$i]['file']);
			if (!in_array($s_filename, array( "form.php", "browse.php", "help_box.php" )))
			{
				$s_use_filename = $s_filename;
				break;
			}
		}
		$s_filename = $s_use_filename;

		// get the table and setting names
		if (strpos($s_fieldname, ':') !== FALSE) {
			$a_name_parts = explode(':', $s_fieldname);
			$s_tablename = $a_name_parts[0];
			$s_settingname = $a_name_parts[1];
		} else {
			if (isset($s_browe_includes_form_tablename))
				$s_tablename = $s_browe_includes_form_tablename;
			else
				$s_tablename = 'locations';
			$s_settingname = $s_fieldname;
		}
	}

?>

<script type="text/javascript" src="/manage/js/jquery/js/jquery-ui-1.10.0.custom.min.js"></script>
<link rel="stylesheet" href="/manage/js/jquery/css/smoothness/jquery-ui-1.10.0.custom.min.css" />

<script type="text/javascript">
	// puts either the provided html or the html contents of the provided div into the help box and makes it visible
	// @html: the html to put into the help box
	//     format:
	//         <h1>stuff</h1>
	//         <ol>
	//             <li><div class='h2'>title</div>text and other html</li>
	//             <li><div class='h2'>title</div>text and other html</li>
	//         </ol>
	// @div: put this into the helpbox (used when provided, otherwise html is used)
	// @apply_default_styles: whether to apply the default help box styles to its contents (defaults to true)
	// @edit_text_id: if not 0 provides a link to manage to edit the text of this helpbox
	display_help_box_jparent = null;
	function display_help_box(html, div, apply_default_styles, edit_text_id) {
		if (typeof(apply_default_styles) == 'undefined')
			apply_default_styles = true;

		var jwindow = $(window);
		var jdocument = $(document);
		var currScoll = jwindow.scrollTop();
		var mewindow = window;
		var parentWindow = window.parent.window;

		// scroll inside of manage
		var manageScrollTop = 0;
		if (parentWindow !== mewindow) {
			manageScrollTop = currScoll;
		}

		var jdiv = $('#'+div);
		if (!div || jdiv.length == 0) {
			div = "<div>"+html+"</div>";
			jdiv = $(div);
		}
		jdiv.dialog({ width: 700, height: 400 });

		var jparent = jdiv.parent();
		var jdialog = $(".ui-dialog[aria-labelledby=ui-dialog-title-"+div+"]");
		if (jdialog.length == 0)
			jdialog = $(".ui-dialog[aria-describedby="+div+"]");
		var jtitlebar = jdialog.find(".ui-dialog-titlebar");
		var jclosebox = jtitlebar.find(".ui-dialog-titlebar-close");
		var jcloseletter = jclosebox.children("span");
		var jwidget = jdialog;

		jdiv.css({ position:'relative', top:'10px' });
		jwidget.css({ background:'none' });
		jdialog.css({ 'background-color':'#bcc7cc', 'border-radius':0, 'border-width':'1px', 'border-color':'#858585', 'border-style':'solid' });
		jtitlebar.css({ padding:0, background:'none', border:0 });
		jclosebox.css({ 'border-width':'1px', 'border-color':'white', 'border-style':'solid', 'border-radius':'0px', right:'3px', top:'12px', width:'21px', height:'21px' });
		jcloseletter.css({ 'margin-left':'2px', 'margin-top':'2px' });

		jclosebox.append("<div style='width:"+jclosebox.width()+"px; height:"+jclosebox.height()+"px; position:absolute; bottom:0; right:0; z-index:1;' class='closebox_hoverthing'></div>");
		var jhoverthing = jclosebox.find(".closebox_hoverthing");
		jhoverthing.mouseover(function() {
			while (jclosebox.hasClass('ui-state-hover')) {
				jclosebox.removeClass('ui-state-hover');
			}
			jclosebox.addClass('ui-state-hover');
		});
		jhoverthing.mouseleave(function() {
			while (jclosebox.hasClass('ui-state-hover')) {
				jclosebox.removeClass('ui-state-hover');
			}
		});
		jhoverthing.click(function() {
			jdialog.hide();
		});

		if (apply_default_styles) {
			jdiv.css({ color:'#272727', 'font-family':'arial', 'font-size':'13px' });
			jdiv.find('div[class=h1]').css({ 'font-family':'verdana', 'font-size':'30px', color:'#171818' });
			jdiv.children().css({ 'margin-left':'17px' });
			var ordered_lists = jdiv.find('ol');
			ordered_lists.css({ 'margin-top':0, 'margin-left':0 });
			for (var i = 0; i < ordered_lists.length; i++) {
				var list_items = $(ordered_lists[i]).find('li');
				if (list_items.length > 0) {
					list_items.css({ 'font-family':'arial', 'font-size':'18px', color:'white', 'padding-top':'18px' });
					list_items.children().css({ 'font-size':'13px', color:'#272727', position:'relative', left:'-24px' });
					list_items.children('div[class=h2]').css({ 'font-family':'arial', 'font-size':'18px', color:'white', left:0 });
				}
			}
		}

		if (edit_text_id !== 0) {
			jdialog.append('<div style="position:absolute; top:5px; left:5px;"><form target="_blank" method="POST" action="/manage/misc/Standalones/help_box_editor.php"><input type="hidden" name="action" value="edit_text" /><input type="hidden" name="helptext_id" value="'+edit_text_id+'" /><!-- <input type="submit" value="edit" /> --></form></div>');
		}

		// scroll inside of manage
		if (parentWindow !== mewindow) {
			var top = 0;
			if (typeof(window.helpBox) == 'undefined')
				window.helpBox = {};
			calculateTop = function() {
				var parentTop = parseInt(parentWindow.$(parentWindow).scrollTop(),10);
				top = parseInt(jparent.offset().top) - parseInt($(mewindow).scrollTop(),10) - parentTop + Math.min(parentTop, 158);
			};
			calculateTop();
			jparent.mouseup(calculateTop);
			var scrollfunc = function() {
				if (jparent.length <= 0)
					return;
				var parentScroll = Math.max(0, parseInt(parentWindow.$(parentWindow).scrollTop(),10)-158);
				jparent.css("top", ((top+parseInt(mewindow.$(mewindow).scrollTop(),10)+parentScroll)+"px"));
			};
			parentWindow.$(parentWindow).scroll(scrollfunc);
			setTimeout(scrollfunc, 400);
		}

		jparent.css({ left: (jwindow.width()/2-jdiv.width()/2), top: (jwindow.height()/2-jdiv.height()/2+jdocument.scrollTop()) });
		jdialog.css({ height: (Math.min(jdialog.height(), jwindow.height()-100)+'px') });
		jdiv.css({ height:'330px' });
		display_help_box_scrollpos = jdocument.scrollTop();
		display_help_box_jparent = jparent;
		if (jparent.offset().top > jwindow.height())
			jparent.css("top", (50+parseInt(jwindow.scrollTop(),10)+parseInt(manageScrollTop,10)+"px"));
		jwindow.scrollTop(currScoll);

		// scroll inside of manage
		if (mewindow !== parentWindow) {
			parentWindow.$(parentWindow).scrollTop(manageScrollTop);
		}
	}

	display_help_box_scrollpos = 0;
	function help_box_document_scroll() {
		var newscroll = $(document).scrollTop();
		var jparent = display_help_box_jparent;

		if (jparent === null || jparent.length == 0)
			return;
		jparent.css({ top: (parseInt(jparent.css('top'))+newscroll-display_help_box_scrollpos) });

		display_help_box_scrollpos = newscroll;
	}

	$(document).scroll(help_box_document_scroll);
</script>

<style type="text/css">
	.ui-dialog {
		box-shadow: 5px 5px 3px rgba(0,0,0,0.5);
	}
</style>
