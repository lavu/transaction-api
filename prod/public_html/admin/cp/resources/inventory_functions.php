<?php
/*
Description:- Get 86Count table name based on given table name
Input:- Table name
Output:- array (tablename and column name)
*/
function get_poslavu_86Table($tablename) {
    $table86CountMaps = array("menu_items" => "menuitem_86", "modifiers" => "modifier_86", "forced_modifiers" => "forced_modifier_86");
    $table86ColumnMaps = array("menu_items" => "menuItemID", "modifiers" => "modifierID", "forced_modifiers" => "forced_modifierID");
    $table86name = $table86CountMaps[$tablename];
    $table86Column = $table86ColumnMaps[$tablename];
    return array($table86name, $table86Column);
}

/*
Description :- Used for add and update operations on menuitem_86, modifier_86 and forced_modifier_86 table
Input :-
	table name:- menu_items, modifiers, forced_modifiers
	ingredients:- 56 x 10,47 x 15
	id:- 12
	units:- 4
Output:- null
*/
function update_poslavu_86count($tablename, $id, $ingredients, $units, $deleteIngredient = 0) {
    // If ingredients has value then insert records in 86 cont table.
    if ($ingredients != "")
    {
        list($tablename, $table86Column) = get_poslavu_86Table($tablename);
        $ingredientsCounts = explode(",", $ingredients);
        $unitIds = explode(",", $units);
        foreach($ingredientsCounts as $key => $ingredientCount) {
            $idsCounts = explode( ' x ' , $ingredientCount);
            $inventoryItemId = $idsCounts[0];
            $quantityUsed = $idsCounts[1];
            $count86Arr[$key][$table86Column] = $id;
            $count86Arr[$key]['inventoryItemID'] = $inventoryItemId;
            $count86Arr[$key]['quantityUsed'] = $quantityUsed;
            $count86Arr[$key]['unitID'] = $unitIds[$key];
        }
        $data = json_encode($count86Arr, JSON_NUMERIC_CHECK);
        switch ($tablename) {
            case 'menuitem_86':
                $url = "/86Count/menuItems";
                break;
            case 'modifier_86':
                $url = "/86Count/modifiers";
                break;
            case 'forced_modifier_86':
                $url = "/86Count/forcedModifiers";
                break;
            default:
                null;
        }
        $result = curl_api('POST', $url, $data);
    } else if ($ingredients == "" && $deleteIngredient == 1) {
        //If user delete all inventory items of menu item then get soft delete all inventory items related to given menu
        // items
        delete_poslavu_86count($tablename, $id);
    }
}

/*
Description:- Delete soft delete rows from table
Input:- Table name, id
Output:- NULL
*/
function delete_poslavu_86count($tablename, $id)
{
    if($id!="")
    {
        global $section;
        global $mode;
        list($tablename, $table86Column) = get_poslavu_86Table($tablename);
        CPActionTracker::track_action($section . '/' . $mode, 'deleted_' . $tablename);
        switch ($tablename) {
            case 'menuitem_86':
                $url = "/86Count/menuItems";
                break;
            case 'modifier_86':
                $url = "/86Count/modifiers";
                break;
            case 'forced_modifier_86':
                $url = "/86Count/forcedModifiers";
                break;
            default:
                null;
        }
        $idArr = array(array("id" =>$id));
        $data = json_encode($idArr, JSON_NUMERIC_CHECK);
        $result = curl_api('DELETE', $url, $data);
    }
}

/*
Description:- Get inventory units id based on  menuItemID, modifierID, forced_modifierID from menuitem_86/modifier_86
    /forced_modifier_86 table
Input:- Table name, menuItemID/modifierID/forced_modifierID
Output:- string inventory items id x quantityUsed xx unit ids
*/

/**
* Updated By Dhruvpalsinh On 28-02-2019
* Purpose : To Filer array menuItemIdwise
**/
function get_poslavu_inventory_items_86count($tablename, $id)
{
    $menuData = array();
    if($id!="") {
        list($tablename, $table86Column) = get_poslavu_86Table($tablename);
        $result = get_86count($tablename, $id);
        $jsonResponse = json_decode($result, true)['DataResponse']['DataResponse'];
        foreach ($jsonResponse as $itemvalue) {
            if(isset($itemvalue['menuItemID']))
            {
                $menuData[$itemvalue['menuItemID']][] = $itemvalue;
            }//modifierID
            if(isset($itemvalue['modifierID']))
            {
               $menuData[$itemvalue['modifierID']][] = $itemvalue; 
            }//forced_modifierID
            if(isset($itemvalue['forced_modifierID']))
            {
               $menuData[$itemvalue['forced_modifierID']][] = $itemvalue; 
            }    
        }
    }
    return $menuData;
}

function get_items_86countByItemId($itemary)
{
    $units = array();
    $inventoryIDQuantity = array();
    foreach($itemary as $values) {
                $units[] = $values['unitID'];
                $inventoryIDQuantity[] = $values['inventoryItemID'] . ' x ' . $values['quantityUsed'];
        }
    $unitsStr = implode(",", $units);
    $inventoryIDQuantityStr = implode(",", $inventoryIDQuantity);
    $response = ($unitsStr != '') ? $inventoryIDQuantityStr . ' xx ' . $unitsStr : $inventoryIDQuantityStr;
    return $response;
}

/*
Description:- Get inventory units based on categoryID from inventory_units table
Input:- categoryID
Output:- array unit records
*/
function get_inventory_units($catId="")
{
    if($catId!="") {
        $url = "/Units/categorByID/" . $catId;
    } else {
        $url = "/Units";
    }
    $result = curl_api('GET', $url, false);
    $result = json_decode($result, true)['DataResponse']['DataResponse'];
    $units = array();
    foreach($result as $values) {
        $units[$values['id']] = $values['symbol'];
    }
    return $units;
}

/*
Description:- Get inventory items  from inventory_items table
Input:- null
Output:- array unit records
*/
function get_inventory_items()
{
    $url = "/InventoryItems";
    $result = curl_api('GET', $url, false);
    $inventoryItems = json_decode($result, true)['DataResponse']['DataResponse'];
    return $inventoryItems;
}

/*
Description:- Call this method from jc_inc8.php to get insert into inventory_audit table based on menuitemID for waste
item from APP
Input:- $data [{"id":"hayat", "quantity":2, "userID": 43456, "wasteReason":"Damage"}]
Output:- success/fail
*/
function insert_waste_inventory_audits($data)
{
    $url = "/Waste/Item/audit";
    $result = curl_api('POST', $url, $data);
    return $result;
}

/*
Description:- Call this method from jc_inc8.php to get All waste reason from inventory_waste_reason table
Input:- null
Output:- json string [{"id":1, "reason":"Damage"}, {"id":2, "reason":"Drop"}, {"id":3, "reason":"Spoil"}]
*/
function get_waste_reasons()
{
    $url = "/Waste/reasons";
    $result = curl_api('GET', $url, false);
    return $result;
}

/*
Description:- Call this method from jc_inc8.php to get All menu item status from inventory_items and 86count table
Input:- null/[{"id":740}, {"id":742}, {"id": 630}]
Output:- json string [{"682":"OUT_STOCK","685":"IN_STOCK","740":"IN_STOCK","741":"OUT_STOCK","742":"LOW_STOCK"}]
*/
function get_menuitem_status($menuItemIds)
{
    $strIds = '';
    if (!empty($menuItemIds)) {
        $menuItemIdsArr = array();
        foreach($menuItemIds as $ids) {
            $menuItemIdsArr[] = $ids['id'];
        }
        $strIds = implode(';', $menuItemIdsArr);
        $url = "/86Count/Status/menuItem/".$strIds;
    } else {
        $url = "/86Count/Status/menuItems";
    }

    $result = curl_api('GET', $url, false);
    return $result;
}

/*
Description:- Call this method from jc_inc8.php to get All forced modifier status from inventory_items and 86count table
Input:- null/[{"id":740}, {"id":742}, {"id": 630}]
Output:- json string [{"682":"OUT_STOCK","685":"IN_STOCK","740":"IN_STOCK","741":"OUT_STOCK","742":"LOW_STOCK"}]
*/
function get_forced_modifier_status($forcedModifierIds)
{
    $strIds = '';
    if (!empty($forcedModifierIds)) {
        $forcedModifierIdsArr = array();
        foreach($forcedModifierIds as $ids) {
            $forcedModifierIdsArr[] = $ids['id'];
        }
        $strIds = implode(';', $forcedModifierIdsArr);
        $url = "/86Count/Status/forcedModifier/".$strIds;
    } else {
        $url = "/86Count/Status/forcedModifiers";
    }

    $result = curl_api('GET', $url, false);
    return $result;
}

/*
Description:- Call this method from jc_inc8.php to get All modifier status from inventory_items and 86count table
Input:- null/[{"id":740}, {"id":742}, {"id": 630}]
Output:- json string [{"682":"OUT_STOCK","685":"IN_STOCK","740":"IN_STOCK","741":"OUT_STOCK","742":"LOW_STOCK"}]
*/
function get_modifier_status($modifierIds)
{
    $strIds = '';
    if (!empty($modifierIds)) {
        $modifierIdsArr = array();
        foreach($modifierIds as $ids) {
            $modifierIdsArr[] = $ids['id'];
        }
        $strIds = implode(';', $modifierIdsArr);
        $url = "/86Count/Status/modifier/".$strIds;
    } else {
        $url = "/86Count/Status/modifiers";
    }

    $result = curl_api('GET', $url, false);
    return $result;
}

/*
Description:- Call this method from jc_inc8.php to get All menu item from from 86 count table
Input:- null/[{"id":740}, {"id":742}, {"id": 630}]
Output:- json string [[{"id":"143","menuItemID":"740","inventoryItemID":"1","quantityUsed":"4","criticalItem":"0","unitID":"11","reservation":"0","_deleted":"0"},{"id":"144","menuItemID":"740","inventoryItemID":"35","quantityUsed":"2","criticalItem":"0","unitID":"12","reservation":"0","_deleted":"0"}]
*/
function get_menuitem_86count($menuItemIds)
{
    $strIds = '';
    if (!empty($menuItemIds)) {
        $menuItemIdsArr = array();
        foreach($menuItemIds as $ids) {
            $menuItemIdsArr[] = $ids['id'];
        }
        $strIds = implode(';', $menuItemIdsArr);
    }
    $result = get_86count('menuitem_86', $strIds);
    return $result;
}

/*
Description:- Call this method from jc_inc8.php to get All forced modifier data from 86 count table
Input:- null/[{"id":740}, {"id":742}, {"id": 630}]
Output:- json string [[{"id":"143","forcedModifierID":"740","inventoryItemID":"1","quantityUsed":"4","criticalItem":"0","unitID":"11","reservation":"0","_deleted":"0"},{"id":"144","forcedModifierID":"740","inventoryItemID":"35","quantityUsed":"2","criticalItem":"0","unitID":"12","reservation":"0","_deleted":"0"}]
*/
function get_forced_modifier_86count($forcedModifierIds)
{
    $strIds = '';
    if (!empty($forcedModifierIds)) {
        $forcedModifierIdsArr = array();
        foreach($forcedModifierIds as $ids) {
            $forcedModifierIdsArr[] = $ids['id'];
        }
        $strIds = implode(';', $forcedModifierIdsArr);
    }
    $result = get_86count('forced_modifier_86', $strIds);
    return $result;
}

/*
Description:- Call this method from jc_inc8.php to get All modifier data from 86 count table
Input:- [{"id":740}, {"id":742}, {"id": 630}]
Output:- json string [[{"id":"143","moodifierID":"740","inventoryItemID":"1","quantityUsed":"4","criticalItem":"0","unitID":"11","reservation":"0","_deleted":"0"},{"id":"144","moodifierID":"740","inventoryItemID":"35","quantityUsed":"2","criticalItem":"0","unitID":"12","reservation":"0","_deleted":"0"}]
*/
function get_modifier_86count($modifierIds)
{
    $strIds = '';
    if (!empty($modifierIds)) {
        $modifierIdsArr = array();
        foreach($modifierIds as $ids) {
            $modifierIdsArr[] = $ids['id'];
        }
        $strIds = implode(';', $modifierIdsArr);
    }
    $result = get_86count('modifier_86', $strIds);
    return $result;
}

/*
Description:- Common method to get 86Count records from respective 86count table
Input:- 86count table, ids (740;730;212)
Output:- json string [[{"id":"143","moodifierID":"740","inventoryItemID":"1","quantityUsed":"4","criticalItem":"0","unitID":"11","reservation":"0","_deleted":"0"},{"id":"144","moodifierID":"740","inventoryItemID":"35","quantityUsed":"2","criticalItem":"0","unitID":"12","reservation":"0","_deleted":"0"}]
*/
function get_86count($tablename, $id)
{
    $result = array();
    if($id!="") {
        switch ($tablename) {
            case 'menuitem_86':
            case 'menu_items':
                $url = "/86Count/menuItems/" . $id;
                break;
            case 'modifier_86':
            case 'modifiers':
                $url = "/86Count/modifiers/" . $id;
                break;
            case 'forced_modifier_86':
            case 'forced_modifiers':
                $url = "/86Count/forcedModifiers/" . $id;
                break;
            default:
                null;
        }
        $result = curl_api('GET', $url, false);
    }
        return $result;
}


/*
Description:- This method is used to update reservation while user placed order
Input:- [{"added":{"item_id":[{"id":740,"quantity":2},{"id":957,"quantity":3}],"forced_modifier_id":[{"id":57},{"id":58,"quantity":3}],"optional_modifier_id":[{"id":16,"quantity":2},{"id":31,"quantity":3}]},"removed":{"item_id":[{"id":740,"quantity":2},{"id":957,"quantity":3}],"forced_modifier_id":[{"id":57},{"id":58,"quantity":3}],"optional_modifier_id":[{"id":16,"quantity":2},{"id":31,"quantity":3}]}}]
Output:- json string {"DataResponse":{"DataResponse":{"RowsUpdated":19}},"RequestID":0,"Status":"Success"}
*/
function process_order($menuOrder)
{
    $result = array();
    $url = "/86Count/Item/processOrder";
    $result = curl_api('PUT', $url, $menuOrder);
    return $result;
}


/*
Description:- This method is used to update reservation and quantityOnHand while server sent order to kitchen
Input:- [{"item_id":[{"id":740,"quantity":2},{"id":957,"quantity":3}],"forced_modifier_id":[{"id":57},{"id":58,"quantity":3}],"optional_modifier_id":[{"id":16,"quantity":2},{"id":31,"quantity":3}]}]
Output:- json string {"DataResponse":{"DataResponse":{"RowsUpdated":19}},"RequestID":0,"Status":"Success"}
*/
function deliver_order($menuOrder)
{
    require_once(dirname(__FILE__).'/../../components/Inventory/RequestCompilations/Inventory86CountViewCompiler.php');
    $result = array();
    $count86ViewCompiler = new Inventory86CountViewCompiler();
    $result = $count86ViewCompiler->updateDeliverQuantity(json_decode($menuOrder, true));
    return $result;
}

/*
Description:- This method is used to deduct quantityOnHand for offline mode sale
Input:- [{"added":{"item_id":[{"id":1,"quantity":2},{"id":2,"quantity":3}],"forced_modifier_id":[{"id":1},{"id":2,"quantity":3}],"optional_modifier_id":[{"id":1,"quantity":2},{"id":2,"quantity":3}]},"removed":{"item_id":[{"id":1,"quantity":2},{"id":2,"quantity":3}],"forced_modifier_id":[{"id":1},{"id":2,"quantity":3}],"optional_modifier_id":[{"id":1,"quantity":2},{"id":2,"quantity":3}]}}]
Output:- json string {"DataResponse":{"DataResponse":{"RowsUpdated":19}},"RequestID":0,"Status":"Success"}
*/
function updateQuantityOnHand($menuOrder)
{
	$result = array();
	$url = "/86Count/quantityOnHand";
	$result = curl_api('PUT', $url, $menuOrder);
	return $result;
}

/*
Description:- This method is used get status of menu while server add item for order
Input:- [{"item_id":[{"id":740,"quantity":2}, {"id":741,"quantity":1}],"forced_modifier_id":[{"id":745,"quantity":2},{"id":746,"quantity":3}],"optional_modifier_id":[{"id":747,"quantity":2},{"id":748,"quantity":3}]}]
Output:- json string {"DataResponse":{"DataResponse":{"item_id":[{"id":740,"status":"IN_STOCK"},{"id":741,"status":"IN_STOCK"}],"forced_modifier_id":[{"id":745,"status":"OUT_STOCK"},{"id":746,"status":"IN_STOCK"}],"optional_modifier_id":[{"id":747,"status":"IN_STOCK"},{"id":748,"status":"OUT_STOCK"}]}},"RequestID":0,"Status":"Success"}
*/
function get_menu_status($menuStatus)
{
    $result = array();
    $url = "/86Count/Status/menu";
    $result = curl_api('POST', $url, $menuStatus);
    return $result;
}

/*
Description:- Call this method from jc_inc8.php to get All menu item status from inventory_items and 86count table based
on 86count entry
Input:- null/[{"id":740}, {"id":742}, {"id": 630}]
Output:- json string [{"682":"OUT_STOCK","685":"IN_STOCK","740":"IN_STOCK","741":"OUT_STOCK","742":"LOW_STOCK"}]
*/
function get_menuitem_86count_status($menuItemIds)
{
    $strIds = '';
    if (!empty($menuItemIds)) {
        $menuItemIdsArr = array();
        foreach($menuItemIds as $ids) {
            $menuItemIdsArr[] = $ids['id'];
        }
        $strIds = implode(';', $menuItemIdsArr);
        $url = "/86Count/Status/lowQuantity/menuItem/".$strIds;
    } else {
        $url = "/86Count/Status/lowQuantity/menuItems";
    }

    $result = curl_api('GET', $url, false);
    return $result;
}

/*
Description:- Call this method from jc_inc8.php to get All forced modifier status from inventory_items and 86count table
based on 86count entry
Input:- null/[{"id":740}, {"id":742}, {"id": 630}]
Output:- json string [{"682":"OUT_STOCK","685":"IN_STOCK","740":"IN_STOCK","741":"OUT_STOCK","742":"LOW_STOCK"}]
*/
function get_forced_modifier_86count_status($forcedModifierIds)
{
    $strIds = '';
    if (!empty($forcedModifierIds)) {
        $forcedModifierIdsArr = array();
        foreach($forcedModifierIds as $ids) {
            $forcedModifierIdsArr[] = $ids['id'];
        }
        $strIds = implode(';', $forcedModifierIdsArr);
        $url = "/86Count/Status/lowQuantity/forcedModifier/".$strIds;
    } else {
        $url = "/86Count/Status/lowQuantity/forcedModifiers";
    }

    $result = curl_api('GET', $url, false);
    return $result;
}

/*
Description:- Call this method from jc_inc8.php to get All modifier status from inventory_items and 86count table
based on 86count entry
Input:- null/[{"id":740}, {"id":742}, {"id": 630}]
Output:- json string [{"682":"OUT_STOCK","685":"IN_STOCK","740":"IN_STOCK","741":"OUT_STOCK","742":"LOW_STOCK"}]
*/
function get_modifier_86count_status($modifierIds)
{
    $strIds = '';
    if (!empty($modifierIds)) {
        $modifierIdsArr = array();
        foreach($modifierIds as $ids) {
            $modifierIdsArr[] = $ids['id'];
        }
        $strIds = implode(';', $modifierIdsArr);
        $url = "/86Count/Status/lowQuantity/modifier/".$strIds;
    } else {
        $url = "/86Count/Status/lowQuantity/modifiers";
    }

    $result = curl_api('GET', $url, false);
    return $result;
}

/*
Description:- This method is used get status of menu while server add item for order
Input:- [{"item_id":[{"id":740,"quantity":2}, {"id":741,"quantity":1}],"forced_modifier_id":[{"id":745,"quantity":2},{"id":746,"quantity":3}],"optional_modifier_id":[{"id":747,"quantity":2},{"id":748,"quantity":3}]}]
Output:- json string {"DataResponse":{"DataResponse":{"item_id":[{"id":740,"status":"IN_STOCK"},{"id":741,"status":"IN_STOCK"}],"forced_modifier_id":[{"id":745,"status":"OUT_STOCK"},{"id":746,"status":"IN_STOCK"}],"optional_modifier_id":[{"id":747,"status":"IN_STOCK"},{"id":748,"status":"OUT_STOCK"}]}},"RequestID":0,"Status":"Success"}
*/
function get_menu_86count_status($menuStatus)
{
    $result = array();
    $url = "/86Count/Status/lowQuantity/menu";
    $result = curl_api('POST', $url, $menuStatus);
    return $result;
}

/*
Description:- This method is used get status of all inventory items which is related to menu items, forced modifier and
modifier based on quantityOnHand, quantityUsed and reservation as well as get status of all inventory items which is related to menu items, forced modifier
and modifier based on quantityOnHand, quantityUsed and 86count
Input:-
Output:- json string {"DataResponse":{"DataResponse":{"86CountStatus":{"menuItem":{"12":"In_Stock","32":"In_Stock"},"forcedModifier":{"42":"In_Stock","62":"In_Stock"},"optionalModifier":{"52":"In_Stock","72":"In_Stock"}},"86CountInventoryStatus":{"menuItem":{"12":"In_Stock","32":"In_Stock"},"forcedModifier":{"Cheeze":"In_Stock","Salad":"In_Stock"},"optionalModifier":{"Pepperoni":"In_Stock","Red Onion":"In_Stock"}}},"RequestID":0,"Status":"Success"}
*/
function get_86count_inventory_status($categoryIdsArr = array())
{
    $result = array();
    if (!empty($categoryIdsArr)) {
	    $categoryId = implode(';', $categoryIdsArr);
	    $url = "/86Count/Status/category/inventory/".$categoryId;
    } else {
	    $url = "/86Count/Status/inventory";
    }
    $result = curl_api('GET', $url, false);
    return $result;
}

/*
Description:- Call this method to get insert inventory quantity into inventory_audit table
item from APP
Input:- $data [{"inventoryItemID":"143", "action":'86count', "quantity": 2, "unitID":1, "cost": 3, "userID":34, "userNotes":'Offline Quantity Decreased by Menu Item'}, {"inventoryItemID":"143", "action":'86count', "quantity": 2, "unitID":1, "cost": 3, "userID":34, "userNotes":'Offline Quantity Decreased by Forced Modifier'}]
Output:- success/fail
*/
function insert_inventory_audits($auditData)
{
    $url = "/Audit/add";
    $result = curl_api('POST', $url, $auditData);
    return $result;
}

/**
 * Description:- Set menu items into cache
 * @param $cacheData array('1119' => array("id":1119,"inv_count":0,"track_86_count":"86countInventory","combo":0), '1231' => array("id":1231,"inv_count":10,"track_86_count":"86count","combo":0))
 * @param return boolean true|false
 */
function setCacheMenuItems($cacheData) {
	//TODO:- Remove condition once caching will stable
	if (sessvar("ENABLED_REDIS")) {
		$cacheJsonData = json_encode($cacheData, JSON_NUMERIC_CHECK);
		$url = "/Cache/setMenuItems";
		$result = curl_api('POST', $url, $cacheJsonData);
		return $result;
	}
	return true;
}

/**
 * Description:- Set forced modifiers into cache
 * @param $cacheData array('59' => array("id":59,"title":'Garlic Fries'), '31' => array("id":31,"title":'Salad'))
 * @param return boolean true|false
 */
function setCacheForcedModifiers($cacheData) {
	//TODO:- Remove condition once caching will stable
	if (sessvar("ENABLED_REDIS")) {
		$cacheJsonData = json_encode($cacheData, JSON_NUMERIC_CHECK);
		$url = "/Cache/setForcedModifiers";
		$result = curl_api('POST', $url, $cacheJsonData);
		return $result;
	}
	return true;
}

/**
 * Description:- Set modifiers into cache
 * @param $cacheData array('91' => array("id":91,"title":'Gluten Free'), '11' => array("id":11,"title":'Bread'))
 * @param return boolean true|false
 */
function setCacheModifiers($cacheData) {
	//TODO:- Remove condition once caching will stable
	if (sessvar("ENABLED_REDIS")) {
		$cacheJsonData = json_encode($cacheData, JSON_NUMERIC_CHECK);
		$url = "/Cache/setModifiers";
		$result = curl_api('POST', $url, $cacheJsonData);
		return $result;
	}
	return true;
}

/*
Description:- delete menu category cache
Input:- cache keu
Output:- cachekey
*/
function clearMenuCategoryCache()
{
	//TODO:- Remove condition once caching will stable
	if (sessvar("ENABLED_REDIS")) {
		$url = "/Cache/MenuCategory/delete";
		$result = curl_api('DELETE', $url, false);
		$menuCategoryKey = json_decode($result, true)['DataResponse']['DataResponse'];
		return $menuCategoryKey;
	}
	return true;
}

/*
Description:- Common curl method to call inventory API in legacy code
Input:- $method (GET/POST/PUT/DELETE), $url, $data
Output:- json string
*/
function curl_api($method, $url, $data = false)
{
    $url = 'http://'.$_SERVER['HTTP_HOST'].'/api_entry_point/inventory/InventoryRequestRouter.php/inventory/v1.0'.$url;
    $_COOKIE['PHPSESSID'] = isset($_COOKIE['PHPSESSID']) ? $_COOKIE['PHPSESSID'] : session_id();
    $strCookie = 'PHPSESSID=' . $_COOKIE['PHPSESSID'] . '; path=/';
    session_write_close();
    $ch = curl_init();
    switch ($method)
    {
        case "POST":
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
            if ($data)
                curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
            break;
        case "PUT":
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'PUT');
            if ($data)
                curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
            break;
        case "GET":
            curl_setopt($ch, CURLOPT_HTTPGET, 1);
            break;
        case "DELETE":
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'DELETE');
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
            break;
        default:
            if ($data)
                $url = sprintf("%s?%s", $url, http_build_query($data));
    }
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_HEADER, 0);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_COOKIESESSION,true);
    curl_setopt($ch, CURLOPT_COOKIE, $strCookie);
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
    $output = curl_exec($ch);
    curl_close($ch);
    return $output;
}
