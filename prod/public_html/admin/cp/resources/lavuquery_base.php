<?php

require_once('/home/poslavu/private_html/myinfo.php');

if (extension_loaded("newrelic"))
{
	$dn = "UNKNOWN";

	if ((function_exists('session_status')  && session_status() !== PHP_SESSION_NONE) || session_id() ) {
	  $dn = $_SESSION['poslavu_234347273_admin_dataname'];
	}
	newrelic_add_custom_parameter("dataname", $dn);
}

/*
	ConnectionHub is a singleton class that is used to ensure that we instantiate only one connection to a particular
	database (or memcached.). It always returns the same instance.

	Use it like this:

		$poslavu_conn = ConnectionHub::getConn('poslavu');
		// poslavu_MAIN_db selected by default.
		$poslavu_conn->selectDB('poslavu_OTHER_db');
		$results = $poslavu_conn->legacyQuery(...);

	or

		$rest_conn = ConnectionHub::getConn('rest');
		// a selectDN is required to hook up to a particular DN.
		$rest_conn->selectDN('pomme_bistro');
		// the customer's DB is selected by default.
		$results = $poslavu_conn->legacyQuery(...);

*/
/* TODO: Implement 'ltg' Connector */

$dbconn_arr = array();

class ConnectionHub {

	public static $useReplica = FALSE;

	protected function __construct() {
	}

	public static function getConn($name) {
		global $dbconn_arr;
		return $dbconn_arr[$name];
	}

	public static function addConn($name, $conn) {
		global $dbconn_arr;
		$dbconn_arr[$name] = $conn;
	}

	public static function closeAll() {
		global $dbconn_arr;
		foreach ($dbconn_arr as $name => $conn) {
			if ($conn !== null) {
				$conn->close();
			}
		}
	}

	private function __clone() {}
	private function __wakeup() {}
}

class DBConnector {

	/*

	DBConnector supports querying a particular database using a particular set of credentials, using
	mysqli. It supports multiple kinds of connections, such as a 'read-only' connection and a 'write'
	connection. This is implemented to optimize querying of MySQL Cluster, where writes should be
	directed at a particular host for optimal performance.

	The constructor defines the following properties:

	$this->conn: an array to store the various kinds of connections.
	$this->desc: a string used for debugging purposes.
	$this->default_db: a string defining the database to connect to by default.

	This class has been designed to only initialize database connections upon first use. This prevents
	unnecessary database network activity.

	*/

	public function __construct($dbname = "poslavu_MAIN_db") {
		$this->conn = array( "read" => null, "write" => null, "readreplica" => null );
		$this->desc = "poslavu";
		$this->default_db = $dbname;
		$this->last_error = "";
		$this->check_hook = null;
		/* DBAPI is used to hold on to an instance of Ben Bean's DB class (in DB.php) that is connected to
		   this particular DB connector. DB.php had to be updated to reference a specific DBConnector to
		   make it compatible with mysqli for mysqli_real_escape_string().
		*/
		$this->DBAPI = null;
	}

	public function getDBAPI() {
		if (!$this->DBAPI) {
			require_once(__DIR__ . '/../objects/dbFunctions/DB.php');
			$this->DBAPI = new DB($this);
		}
		return $this->DBAPI;
	}

	public function sessvar($str, $def=false) {
		$sessname = "poslavu_234347273_" . $str;
		if(isset($_SESSION[$sessname]))
			return $_SESSION[$sessname];
		else
			return $def;
	}

	// getLink() returns a db link to either the 'read' or 'write' database connection, initializing if needed:

	public function getLink($mode) {
		if (
			!array_key_exists($mode, $this->conn) ||
			!$this->conn[$mode] ||
			!@$this->conn[$mode]->ping()
		) {
			$this->conn[$mode] = $this->connectDB($mode);
		}
		return $this->conn[$mode];
	}

	// connectDB() is a private method used for connecting to our database.

	private function connectDB($mode)
	{
		$creds = $this->getCredentials($mode === 'readreplica');
		$db = $this->default_db;
		if (array_key_exists("db_name", $creds))
		{
			$db = $creds['db_name'];
		}
		
		if($db == ""){
			return NULL;	#If data name is blank
		}

		$conn = mysqli_connect($creds['db_host'], $creds['db_user'], $creds['db_pass'], $db);
		if (mysqli_connect_errno())
		{
			
			error_log("Error for ".$db." [".mysqli_connect_error()."] - ".$mode);
			$backtrace = debug_backtrace();
			for ($b = 0; $b < count($backtrace); $b++)
			{
				$bt = $backtrace[$b];
				error_log("backtrace ".$b.": ".json_encode($bt));
			}
			return NULL;
		}

		return $conn;
	}

	protected function getCredentials($use_replica=FALSE) {
		return array( "db_host" => my_poslavu_host($use_replica), "db_pass" => my_poslavu_password(), "db_user" => my_poslavu_username() );
	}

	public function getValue($spec, $table, $db=null) {
		$conn = $this->getLink('read');


		$tblspec = "`" . $table . "`";
		if ($db !== null)
			$tblspec = "`" . $db . "`." . $tblspec;

		$w = "";
		$c = 0;
		foreach($spec as $key => $val) {
			$w .= '`' . $key . "` = '" . mysqli_escape_string($conn, $val) . "'" ;
			if ($c + 1 !== count($spec)) {
				$w .= ' AND ';
			}
			$c += 1;
		}
		$q = 'SELECT * from ' . $tblspec . ' WHERE ' . $w . ' LIMIT 1';
		$res = $conn->query($q);
		if ($res === false)
			error_log("lavuquery_base getValue failure on query: " . $q);
		return $res;
	}

	/* Function to carefully set settings without duplicating them. This is what you should use. */

	public function setValue($spec, $vals, $table, $db=null) {

		$tblspec = "`" . $table . "`";
		if ($db !== null)
			$tblspec = "`" . $db . "`." . $tblspec;

		$conn = $this->getLink('write');

		// build up WHERE and INSERT snippets, to be used later:
		$w = "";
		$v1 = "";
		$v2 = "";
		$c = 0;
		foreach($spec as $key => $val) {
			$w .= '`' . $key . "` = '" . mysqli_escape_string($conn, $val) . "'";
			$v1 .= '`' . $key . '`';
			$v2 .= "'" . $val . "'";
			if ($c + 1 !== count($spec)) {
				$w .= ' AND ';
				$v1 .= ', ';
				$v2 .= ', ';
			}
		}

		// build up SET clause, to be used later:
		$s = "";
		foreach ($vals as $key => $val) {
			$s .= '`' . $key . "` = '" . mysqli_escape_string($conn, $val) . "'";
			if ($c + 1 !== count($vals)) {
				$s .= ', ';
			}
		}

		if ($vals['type'] == 'ers_routing_setting') {
			$s = trim($s);
			$s = substr($s,0,strlen($s)-1);

			$w = trim($w);
			$w = substr($w,0,strlen($w)-4);
		}

		$result = $conn->query('UPDATE `config` SET ' . $s . ' WHERE ' . $w . ' LIMIT 1');

		if ($result->num_rows === 1) {
			return;
		} else if ($result->num_rows == 0) {

			if ($vals['type'] == 'ers_routing_setting') {

				$v1 = trim($v1);
				$v1 = substr($v1,0,strlen($v1)-1);

				$v2 = trim($v2);
				$v2 = substr($v2,0,strlen($v2)-1);

				$v1 .= ", `value`";
				$v2 .= ", '".$vals['value']."'";
			}

			$result = $conn->query('INSERT into ' . $tblspec . '(' . $v1 . ') VALUES (' . $v2 . ')');
		}
	}

	/* selectDB() doesn't actually issue 'select'. It just caches what DB to select prior to our next query. */

	public function selectDB($db) {
		/* TODO: iterate through array keys */
		// extra checks needed for manage so we don't select db that doesn't exist causing fatal error
		$r = $this->getLink('read');
		if ($r)
			$r->select_db($db);
		$w = $this->getLink('write');
		if ($w)
			$w->select_db($db);
	}

	public function insertID() {
		// we assume the link is enabled
		return $this->conn['write']->insert_id;
	}

	public function affectedRows() {
		// we assume the link is enabled
		return $this->conn['write']->affected_rows;
	}

	public function escapeString($str) {
		return $this->getLink('read')->escape_string($str);
	}

	public function closeDB() {
		if (($this->conn['read'] !== null) and is_object($this->conn['read'])) {
			$this->conn['read']->close();
			$this->conn['read'] = null;
		}
		if (($this->conn['write'] !== null) and is_object($this->conn['write'])) {
			$this->conn['write']->close();
			$this->conn['write'] = null;
		}
              	if (($this->conn['readreplica'] !== null) and is_object($this->conn['readreplica'])) {
                        $this->conn['readreplica']->close();
                        $this->conn['readreplica'] = null;
                }
		// perform any misc. cleanup actions.
		$this->finalize();
	}

	/* close() is used by the ConnectionHub's closeAll() call. */

	public function close() {
		$this->closeDB();
	}

	protected function finalize() {
		return;
	}

	public function error() {
		return $this->last_error;
	}

	// logQuery() will determine whether or not a given query should be logged to the operational log file and if so, do so

	private function logQuery($mode, $query, $should_log, $starttime, $endtime, $result_string)
	{
		global $data_name;
		global $got_loc_id;
		global $lavu_query_should_log;
		global $location_api_debug;
		global $location_lds_debug;
		global $lds_debug;

		if ($mode=="read" && $result_string=="{SUCCEEDED}" && !$location_api_debug && !$location_lds_debug && !$lds_debug)
		{
			return;
		}

		if (empty($data_name) || !$lavu_query_should_log || !$should_log)
		{
			return;
		}

		$loc_str = $got_loc_id?" - LOCATION: ".$got_loc_id:"";

		$user_str = "";
		$un = "";

		if (function_exists("admin_info"))
		{
			$un = admin_info("username");
		}

		if (!empty($un))
		{
			$user_str = " - User: ".$un;
		}

		$backtrace = debug_backtrace();
		$backtrace = $backtrace[4]; // This many steps back to the inital call tods lavuquery

		$details_str = " - Exec: ".round((($endtime - $starttime) * 1000), 2)."ms";
		if (!empty($backtrace))
		{
			$details_str .= " - From file: ".$backtrace['file']." - Line: ".$backtrace['line'];
		}

		$log_path = "/home/poslavu/logs/company/".$data_name."/operational";
		if (!is_dir($log_path))
		{
			mkdir($log_path, 0775, TRUE);
		}

		$use_date_time = MountainDateTime();
		$use_dt_parts = explode(" ", $use_date_time);
		$use_date = $use_dt_parts[0];

		$full_path = $log_path."/mysql_".$use_date.".log";

		$log_file = fopen($full_path, "a");

		$log_string = number_format($endtime, 1, ".", "")." - ".$use_date_time." Mountain".$loc_str.$user_str.$details_str." - ".stripslashes($query)." ".$result_string."[--CR--]\n";

		fputs($log_file, $log_string);
		fclose($log_file);

		chmod($full_path, 0775);
	}

	// centralLogDBError() is responsible for writing query errors with stack traces to the centrally located dberrors.txt file

	private function centralLogDBError($dn, $error, $query)
	{
		error_log("database query failed - ".(empty($dn)?"<no_dataname>":$dn)." - ".$error." - ".$query);

		$file = fopen('/home/poslavu/logs/dberrors.txt', 'a');
		if ($file === false)
		{
			error_log("Could not write to dberrors.txt file.");
		}
		fwrite($file, $error . "\n");
		fwrite($file, $query . "\n");

		try
		{
			throw new Exception;
		}
		catch (Exception $e)
		{
			fwrite($file, $e->getTraceAsString());
		}

		fwrite($file, "\n\n\n");
		fclose($file);
	}

	// query() takes a $mode that is typically 'read' or 'write' (depending on what kind of operation we're performing.)

	public function query($mode, $query, $should_log)
	{
		global $data_name;

		$starttime		= microtime(TRUE);
		$result			= NULL;
		$result_string	= "";

		$link = $this->getLink($mode);
		if (!$link)
		{
			$this->last_error	= "could not get database ".$mode." link";
			$result_string		= "{FAILED}[--CR--][--CR--]".$this->last_error;
			$this->centralLogDBError($data_name, $this->last_error, $query);
		}
		else
		{
			$result = $link->query($query);
			if ($link->error)
			{
				$this->last_error	= $link->error;
				$result_string		= "{FAILED}[--CR--][--CR--]".$link->error;

				$this->centralLogDBError($data_name, $this->last_error, $query);
			}
			else
			{
				$this->last_error	= "";
				$result_string		= "{SUCCEEDED}";
			}
		}

		$endtime = microtime(TRUE);

		$this->logQuery($mode, $query, $should_log, $starttime, $endtime, $result_string);

		return $result;
	}

	// autoquery() will try to detect whether we are performing a read or write operation and will then use the correct DB link.

	public function autoquery($query, $should_log, $use_replica=FALSE)
	{
		if (stripos($query, "select") === 0)
		{
			if($use_replica)
			{
				return $this->query('readreplica', $query, $should_log);
			}
			else
			{
				return $this->query('read', $query, $should_log);
			}
		}
		else
		{
			return $this->query('write', $query, $should_log);
		}
	}

	/* legacyQuery() implements mlavu_query() and lavu_query() functionality using autoquery() to direct to the correct
	connection. */

	public function legacyQuery($query, $var1="[_empty_]", $var2="[_empty_]", $var3="[_empty_]", $var4="[_empty_]", $var5="[_empty_]", $var6="[_empty_]", $should_log=TRUE, $use_replica=FALSE)
	{
		if (stripos($query, "select") === 0)
		{
			$mode = $use_replica ? 'readreplica' : 'read';
			$conn = $this->getLink($mode);
		}
		else
		{
			$conn = $this->getLink('write');
		}

		$raw_query = $query;

		/*
		If there is connection error, return blank result set. Ref. [LP-9339], [LP-8629]
		A MySQL connection is required before using mysql_real_escape_string()
		*/
		if (!$conn) {
			error_log("Connection not available");
			return new mysqli_result();
		}

		if (is_object($conn) && !$conn->thread_id) {
			error_log("Connection thread is not available");
			return new mysqli_result();
		}

		if ($conn)
		{
			if ($var1!="[_empty_]" && is_array($var1))
			{
				foreach ($var1 as $key => $val)
				{
					$query = str_replace("[".$key."]", mysqli_real_escape_string($conn, $val), $query);
				}
			}
			else
			{
				if ($var1 !== "[_empty_]") $query = str_replace("[1]", mysqli_real_escape_string($conn, $var1), $query);
				if ($var2 !== "[_empty_]") $query = str_replace("[2]", mysqli_real_escape_string($conn, $var2), $query);
				if ($var3 !== "[_empty_]") $query = str_replace("[3]", mysqli_real_escape_string($conn, $var3), $query);
				if ($var4 !== "[_empty_]") $query = str_replace("[4]", mysqli_real_escape_string($conn, $var4), $query);
				if ($var5 !== "[_empty_]") $query = str_replace("[5]", mysqli_real_escape_string($conn, $var5), $query);
				if ($var6 !== "[_empty_]") $query = str_replace("[6]", mysqli_real_escape_string($conn, $var6), $query);
			}
		}

		$location_info = $this->sessvar("location_info");

		if (is_array($location_info) && isset($location_info['chain_master_menu']) && $location_info['chain_master_menu']!="")
		{
			global $data_name;
			$query = $this->lavuParseQueryForward($query, $data_name, $location_info['chain_master_menu'], "main");
		}

		if ($this->check_hook)
		{
			if (!$this->check_hook($query))
			{
				// return empty result
				return new mysqli_result();
			}
		}
		return $this->autoquery($query, $should_log, $use_replica);
	}

	/*
		lavuParseQueryForward() is Corey's function that allows chained restaurants to be transparently supported.
		It modifies the original query to retrieve all required data.
	*/

	private function lavuParseQueryForward($query, $from, $to, $parse_type="account") {
		$todb = mysqli_real_escape_string($this->getLink('read'),"poslavu_" . $to . "_db");
		$fromdb = mysqli_real_escape_string($this->getLink('read'),"poslavu_" . $from . "_db");
		$query = trim($query);
		$lquery = strtolower($query);

		$keytables = array(	"menu_groups",
							"menu_categories",
							"menu_items",
							"forced_modifier_groups",
							"forced_modifier_lists",
							"forced_modifiers",
							"modifier_groups",
							"modifier_lists",
							"modifiers");
							// "ingredients");

		$keywords = array(	"insert into",
							"update",
							"select(%)from",
							"delete from");

		$keyword_found = "";
		$tablename_found = "";
		for($k=0; $k<count($keywords); $k++)
		{
			$keyword = $keywords[$k];
			$key_follow = "";
			if(strpos($keyword,"(%)"))
			{
				$key_parts = explode("(%)",$keyword);
				$key_follow = trim($key_parts[1]);
				$keyword = trim($key_parts[0]);
			}
			$keylen = strlen($keyword);

			if(substr($lquery,0,$keylen + 1)==$keyword . " ")
			{
				if($key_follow=="")
				{
					$qstr = substr($query,$keylen + 1);
					$qstr_before = substr($query,0,$keylen + 1);
				}
				else
				{
					$qstr_parts = explode(" $key_follow ",$lquery,2);
					$qstr_before = $qstr_parts[0] . " $key_follow ";
					if(count($qstr_parts)==2) $qstr = substr($query,strlen($qstr_before));//trim($qstr_parts[1]);
					else $qstr = "";
				}

				if($qstr!="")
				{
					$keyword_found = $keyword;
					if($parse_type=="account")
					{
						$tstr_parts = explode(" ",$qstr,2);
						if(count($tstr_parts)==2)
						{
							$tname = trim($tstr_parts[0]);//str_replace("`","",trim($tstr_parts[0]));
							$tafter = trim($tstr_parts[1]);

							if(in_array($tname,$keytables))
							{
								$query = $qstr_before . " `$todb`.`$tname` " . $tafter;
								$tablename_found = $tname;
								//return $new_query;
								//echo "key: $keyword table: $tname<br>";
								//echo $query . "<br>";
								//echo $new_query . "<br><br>";
							}
						}
					}
				}
			}
		}

		if ($keyword_found != "")
		{
			for($t = 0; $t < count($keytables); $t++)
			{
				$ktable = $keytables[$t];
				if (strpos($query,"`$ktable`") !== FALSE)
				{
					if($parse_type=="account")
					{
						$query = str_replace(" `$ktable`"," `$todb`.`$ktable`",$query);
					}
					$query = str_replace("`$fromdb`.`$ktable`","`$todb`.`$ktable`",$query);
					$tablename_found = $ktable;
				}
			}

			if ($parse_type=="account" && $tablename_found!="")
			{
				/* TODO: eliminate sessvar */
				$location_info = $this->sessvar("location_info");
				if(is_array($location_info) && isset($location_info['menu_id']))
				{
					// The Menu Id must be changed to reflect the menu_id of the master account
					$esc_menu_id = mysqli_real_escape_string($this->getLink('read'),$location_info['menu_id']);
					if(strpos($query,"`menu_id`='$esc_menu_id'"))
					{
						$query = str_replace("`menu_id`='$esc_menu_id'","`menu_id`='1'",$query);
					}
					if($keyword_found=="insert into")
					{
						if(strpos($query,"`menu_id`")!==FALSE)
						{
							// If an Insert contains the column menu_id, the query must be broken apart
							// and re-constituted to safely change the menu_id to 1

							$ins_parts = explode("`$tablename_found`",$query,2);
							if(count($ins_parts)==2)
							{
								$ins_parts = explode(" values ",$ins_parts[1],2);
								if(count($ins_parts)==1) $ins_parts = explode(" VALUES ",$ins_parts[0],2);
								if(count($ins_parts)==2)
								{
									$fields = trim($ins_parts[0]);
									$values = trim($ins_parts[1]);
									if(substr($fields,0,1)=="(" && substr($fields,strlen($fields)-1,1)==")" && substr($values,0,1)=="(" && substr($values,strlen($values)-1,1)==")")
									{
										$fields = substr($fields,1,strlen($fields)-2);
										$values = substr($values,1,strlen($values)-2);

										$fieldarr = explode(",",$fields);
										$valuearr = explode(",",$values);

										$fstr = "";
										$vstr = "";
										$newvstr = "";
										if(count($fieldarr)==count($valuearr))
										{
											for($n=0; $n<count($fieldarr); $n++)
											{
												if($fstr!="") $fstr .= ",";
												$fstr .= $fieldarr[$n];
												if($vstr!="") $vstr .= ",";
												$vstr .= $valuearr[$n];
												if($newvstr!="") $newvstr .= ",";
												if(trim(str_replace("`","",$fieldarr[$n]))=="menu_id" && trim(str_replace("'","",$valuearr[$n]))==$esc_menu_id)
													$newvstr .= "'1'";
												else $newvstr .= $valuearr[$n];
											}
											if($vstr!=$newvstr)
											{
												$query = str_replace($vstr,$newvstr,$query);
											}
										}
										//echo "<br>" . $keyword_found . ":" . $tablename_found . ":" . $query . "<br>";
									}
								}
							}
						}
					}
				}
			}
		}

		return $query;
	}
}

class RestaurantDBConnector extends DBConnector {

	public function __construct() {
		$this->conn = array( "read" => null, "write" => null, "readreplica" => null );
		$this->desc = "rest";
		$this->db_creds = null;
		$this->default_db = null;
		$this->check_hook = null;
		$this->cred_count = 0;
		$this->DBAPI = null;
		$this->initialize();
	}

	private function initialize() {
		return;
	}

	protected function finalize() {
		$this->db_creds = null;
	}

	public function selectDN($dataname) {
		if ((!$this->db_creds) || ($dataname !== $this->db_creds['dataname'])) {
			$this->closeDB();
			$this->db_creds = $this->initializeCredentialsFromDB($dataname, FALSE);
		}
		/* TODO: this is defined in lavuquery_future.php, and needs bug fixes from PHP 7 to not segfault PHP: */
		//$this->db_creds = $this->initializeCredentialsFromMemcached($dataname=$dataname);
	}

	public function selectCompanyID($companyid) {
		if ((!$this->db_creds) || (strval($companyid) !== $this->db_creds['companyid']))  {
			$this->closeDB();
			$this->db_creds = $this->initializeCredentialsFromDB(FALSE, $companyid);
		}
	}

	private function connectDB($mode) {
		$conn = mysqli_connect(my_poslavu_host(), $this->db_creds['db_user'], $this->db_creds['db_pass'], $this->db_creds['db_name']);
		$err = $conn->connect_error;
		if ($err != null)
			return $this->conn[$mode];
		else
			throw new Exception('Could not connect to restaurant database for dataname '. $this->db_creds['dataname'] );
	}

	public function setConfig($spec, $vals) {
		return $this->setValue($spec, $vals, 'config');
	}

	public function getConfig($spec) {
		return $this->getValue($spec, 'config' );
	}

	/* lqCredentialsFromDB() gets restaurant DB connection credentials from the database. */

	private function initializeCredentialsFromDB($dataname=FALSE, $companyid=FALSE) {
		// $t1 = microtime($get_as_float=true);
		$conn = ConnectionHub::getConn('poslavu');
		if ($dataname !== FALSE) {
			$rest_query = $conn->legacyQuery("SELECT `id`,`data_name`, AES_DECRYPT(`data_access`,'lavupass') AS `data_access` FROM `poslavu_MAIN_db`.`restaurants` WHERE `data_name` = '[1]'", $dataname);
		} else {
			$rest_query = $conn->legacyQuery("SELECT `id`,`data_name`, AES_DECRYPT(`data_access`,'lavupass') AS `data_access` FROM `poslavu_MAIN_db`.`restaurants` WHERE `id` = '[1]'", $companyid);
		}
		if ((!$rest_query) || (mysqli_num_rows($rest_query) <=0)) {
			error_log('Could not find credentials for dataname: ' . $dataname . ' companyid '. $companyid);
			return null;
		}
		$rest_read = mysqli_fetch_assoc($rest_query);
		$myout = array();
		$myout['db_user'] = 'usr_pl_' . $rest_read['id'];
		$myout['db_pass'] = $rest_read['data_access'];
		$myout['db_name'] = 'poslavu_'.$rest_read['data_name']."_db";
		$myout['dataname'] = $rest_read['data_name'];
		$myout['companyid'] = $rest_read['id'];
		$this->default_db = $myout['db_name'];
		$this->cred_count += 1;
		return $myout;
	}
}

class ReportDBConnector extends DBConnector {
}

/* LEGACY FUNCTIONS */


/* MLAVU - 100% Complete */

function mlavu_connect_byid() {
	return;
}

function mlavu_close_db() {
	ConnectionHub::getConn('poslavu')->closeDB();
}

function mlavu_select_db($setdb) {
	ConnectionHub::getConn('poslavu')->selectDB($setdb);
}

function mlavu_insert_id() {
	return ConnectionHub::getConn('poslavu')->insertID();
}

function mlavu_query($query, $var1="[_empty_]", $var2="[_empty_]", $var3="[_empty_]", $var4="[_empty_]", $var5="[_empty_]", $var6="[_empty_]", $should_log = TRUE, $use_replica=FALSE) {
	$conn = ConnectionHub::getConn('poslavu');
	if(!is_object($conn)) {
		debug_print_backtrace();
		error_log("NONOBJECT: ", $conn);
	}
	$res = $conn->legacyQuery($query, $var1, $var2, $var3, $var4, $var5, $var6, $should_log, $use_replica);
	return $res;
}

function mrpt_query($query, $var1="[_empty_]", $var2="[_empty_]", $var3="[_empty_]", $var4="[_empty_]", $var5="[_empty_]", $var6="[_empty_]", $should_log = TRUE, $use_replica=TRUE) {
	$res = mlavu_query($query, $var1, $var2, $var3, $var4, $var5, $var6, $should_log, $use_replica);
        return $res;
}

function mlavu_query_encode($str) {
	return mysqli_real_escape_string(ConnectionHub::getConn('poslavu')->getLink('read'),$str);
}

function mlavu_dberror() {
	return ConnectionHub::getConn('poslavu')->last_error;
}
/*
 * Chain Data queries
 */

function clavuQuery($query, $var1="[_empty_]", $var2="[_empty_]", $var3="[_empty_]", $var4="[_empty_]", $var5="[_empty_]", $var6="[_empty_]", $should_log=TRUE)
{
	$conn = ConnectionHub::getConn('poslavu_chain');

	$res = $conn->legacyQuery($query, $var1, $var2, $var3, $var4, $var5, $var6, $should_log);
	return $res;
}

function clavu_query_encode($str) {
    return mysqli_real_escape_string(ConnectionHub::getConn('poslavu_chain')->getLink('read'),$str);
}

function clavuInsertID(){
	return ConnectionHub::getConn('poslavu_chain')->insertID();
}

function clavuError(){
	return ConnectionHub::getConn('poslavu_chain')->last_error;
}
/* Restaurant - 100% Complete */

function lavu_connect_dn($data_name=FALSE, $setdb=FALSE) {
	if ($data_name !== FALSE)
		ConnectionHub::getConn('rest')->selectDN($data_name);
	if ($setdb !== FALSE)
		ConnectionHub::getConn('rest')->selectDB($setdb);
}

function lavu_connect_byid($companyid=FALSE, $setdb=FALSE) {
	if ($companyid !== FALSE)
		ConnectionHub::getConn('rest')->selectCompanyID($companyid);
	if ($setdb !== FALSE)
		ConnectionHub::getConn('rest')->selectDB($setdb);
}

function lavu_close_db() {
	ConnectionHub::getConn('rest')->closeDB();
}

function lavu_select_db($setdb) {
	ConnectionHub::getConn('rest')->selectDB($setdb);
}

function lavu_query_encode($str) {
    return mysqli_real_escape_string(ConnectionHub::getConn('rest')->getLink('read'), $str);
}

function lavu_insert_id() {
	return ConnectionHub::getConn('rest')->insertID();
}

function lavu_query($query, $var1="[_empty_]", $var2="[_empty_]", $var3="[_empty_]", $var4="[_empty_]", $var5="[_empty_]", $var6="[_empty_]", $should_log=TRUE, $use_replica=FALSE)
{
	global $data_name;

	$conn = ConnectionHub::getConn('rest');
	if ($conn->db_creds === null && $data_name)
	{
		$conn->selectDN($data_name);
	}
	$res = $conn->legacyQuery($query, $var1, $var2, $var3, $var4, $var5, $var6, $should_log, $use_replica);
	return $res;
}

function lavu_dberror() {
	return ConnectionHub::getConn('rest')->last_error;
}


/* rpt_query - used by reports v2

This function is the only one that passes a 9th parameter to the mlavu_query and
lavu_query to indicate it has to use a replica to do queries instead of
the main DB, the idea is to improve performance, since many functions are
triggered along the way this parameter was added the function definitions and
set to FALSE by default so it does not affect old code.

*/

function rpt_query($query,$var1="[_empty_]",$var2="[_empty_]",$var3="[_empty_]",$var4="[_empty_]",$var5="[_empty_]",$var6="[_empty_]", $shouldLog=true, $useReplica = true)
{
	   $e = "[_empty_]";
	   if(substr($query,0,6)=="debug>")
	   {
			   $query = substr($query,6);
	   }
	   if(strpos($query,"`poslavu_MAIN_db`")===false)
	   {
			   if( strpos( $query, "`reports`")!== false ){
					   $query = str_replace("`reports`","`poslavu_MAIN_db`.`reports_v2`",$query);
					   return mlavu_query($query, $var1, $var2, $var3, $var4, $var5, $var6, $shouldLog, $useReplica);//poslavu_MAIN_db
			   }
		 }
	   return lavu_query($query, $var1, $var2, $var3, $var4, $var5, $var6, $shouldLog, $useReplica);
}

function ers_query($query,$var1="[_empty_]",$var2="[_empty_]",$var3="[_empty_]",$var4="[_empty_]",$var5="[_empty_]",$var6="[_empty_]",$var7="[_empty_]",$var8="[_empty_]",$var9="[_empty_]",$var10="[_empty_]")
{
	   $e = "[_empty_]";
	   if(substr($query,0,6)=="debug>")
	   {
			   $query = substr($query,6);
	   }
	   if(strpos($query,"`poslavu_MAIN_db`")===false)
	   {
			   if( strpos( $query, "`reports`")!== false ){
					   $query = str_replace("`reports`","`poslavu_MAIN_db`.`reports_v2`",$query);
					   return mlavu_query($query,$var1,$var2,$var3,$var4,$var5,$var6,$var7,$var8,$var9,$var10);//poslavu_MAIN_db
			   }
		 }
	   return lavu_query($query,$var1,$var2,$var3,$var4,$var5,$var6,$var7,$var8,$var9,$var10);
}

function ers_sql_error() {
	   return ConnectionHub::getConn('rest')->error();
}

function sanitize($str) {
	   return mysqli_real_escape_string(ConnectionHub::getConn('rest')->getLink('read'),$str);
}

// databaseExists() checks for the existence of a particular database

function databaseExists($db)
{
	$res = mlavu_query("SELECT SCHEMA_NAME FROM INFORMATION_SCHEMA.SCHEMATA WHERE SCHEMA_NAME = '".$db."'");
	if (!$res)
	{
		error_log(__FUNCTION__." failed: ". mlavu_dberror());
		return FALSE;
	}

	return (mysqli_num_rows($res) > 0);
}

/* MISC - 100% complete */

function mysqli_result($res,$row=0,$col=0){
	$numrows = mysqli_num_rows($res);
	if ($numrows && $row <= ($numrows-1) && $row >=0){
		mysqli_data_seek($res,$row);
		$resrow = (is_numeric($col)) ? mysqli_fetch_row($res) : mysqli_fetch_assoc($res);
		if (isset($resrow[$col])){
			return $resrow[$col];
		}
	}
	return false;
}

function buildInsertFieldsAndValues($vars, &$fields, &$values)
{
	$fields = "";
	$values = "";

	$keys = array_keys($vars);
	foreach ($keys as $key)
	{
		if ($fields != "")
		{
			$fields .= ", ";
			$values .= ", ";
		}
		$fields .= "`$key`";
		$values .= "'[$key]'";
	}
}

function buildUpdate($vars, &$update)
{
	$update = "";

	$keys = array_keys($vars);
	foreach ($keys as $key)
	{
		if ($update != "")
		{
			$update .= ", ";
		}
		$update .= "`$key` = '[$key]'";
	}
}
