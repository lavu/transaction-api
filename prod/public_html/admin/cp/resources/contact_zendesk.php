<?php

if (isset($_GET['load_tickets']) && isset($_GET['bentest'])) {
	$b_contact_zendesk_run_zendesk = TRUE;
	$loggedin_id = TRUE;
}

if (isset($_POST['get_next_update_timer'])) {
	$b_get_next_update_timer = TRUE;
	$loggedin_id = TRUE;
}

if (!$loggedin_id)
	return;

require_once(dirname(__FILE__)."/json.php");

define("ZDAPIKEY", "Ypuivz71exKxpnBNRyHPTTxBXPqN8tKvryQXT1Ol");
define("ZDUSER", "it@lavu.com");
define("ZDURL", "https://lavu.zendesk.com/api/v2");

if ((isset($b_contact_zendesk_run_zendesk)) && ($b_contact_zendesk_run_zendesk)) {

	require_once(dirname(__FILE__).'/core_functions.php');
	require_once(dirname(__FILE__).'/lavuquery.php');

	echo "<pre>";
	if (isset($_GET['recently_updated'])) {
		print_r(ZENDESK::get_updated_in_last_hour());
		$s_recently_updated = '&recently_updated';
	} else {
		print_r(ZENDESK::load_tickets(-1,-1));
		//$s_recently_updated = '&recently_updated';
	}
	echo "</pre>";

	echo "<script type='text/javascript'>
		setTimeout(function(){
			window.location = 'http://admin.poslavu.com/cp/resources/contact_zendesk.php?load_tickets&bentest{$s_recently_updated}';
		}, 30*1000);
		setTimeout(function(){
			window.location = 'http://admin.poslavu.com/cp/resources/contact_zendesk.php?load_tickets&bentest{$s_recently_updated}';
		}, 600*1000);
		</script>";
}

if ((isset($b_get_next_update_timer)) && ($b_get_next_update_timer)) {
	echo ZENDESK::next_update_timer_tostr();
}

/* Note: do not put a trailing slash at the end of v2 */

function curlWrap($url, $json, $action, &$error, &$curl_info, &$decoded, $b_do_ticket_audit_replacements = TRUE, $b_draw_errors = FALSE)
{
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_FOLLOWLOCATION, TRUE);
	curl_setopt($ch, CURLOPT_MAXREDIRS, 10 );
	curl_setopt($ch, CURLOPT_URL, ZDURL.$url);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	curl_setopt($ch, CURLOPT_VERBOSE, 1);
	curl_setopt($ch, CURLOPT_HEADER, 1);
	curl_setopt($ch, CURLOPT_USERPWD, ZDUSER."/token:".ZDAPIKEY);
	switch($action){
		case "POST":
			curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
			curl_setopt($ch, CURLOPT_POSTFIELDS, $json);
			break;
		case "GET":
			curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
			break;
		case "PUT":
			curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");
			curl_setopt($ch, CURLOPT_POSTFIELDS, $json);
		default:
			break;
	}
	curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
	curl_setopt($ch, CURLOPT_TIMEOUT, 10);
	$output = curl_exec($ch);
	if (trim(curl_error($ch)) != "")
		$error = LavuJson::json_decode(curl_error($ch));
	$header_size = curl_getinfo($ch, CURLINFO_HEADER_SIZE);
	$curl_info = curl_getinfo($ch);
	$curl_info['body'] = substr($output, $header_size);
	if ($b_do_ticket_audit_replacements) {
		$curl_info['body'] = str_replace(',"public":true,', ',"public":1,', $curl_info['body']);
		$curl_info['body'] = str_replace(',"public":false,', ',"public":0,', $curl_info['body']);
		$curl_info['body'] = str_replace(',"trusted":true,', ',"trusted":1,', $curl_info['body']);
		$curl_info['body'] = str_replace(',"trusted":false,', ',"trusted":0,', $curl_info['body']);
		foreach(array('{"id":', '","id":', '","ticket_id":', '","author_id":') as $s_replace_val) {
			$a_matches = array();
			preg_match_all('/'.$s_replace_val.'[0-9]+/', $curl_info['body'], $a_matches);
			$a_matches = $a_matches[0];
			foreach($a_matches as $k=>$s_match) {
				$s_id = substr($s_match, strlen($s_replace_val));
				$curl_info['body'] = str_replace($s_replace_val.$s_id.',', $s_replace_val.'"'.$s_id.'",', $curl_info['body']);
			}
		}
	}
	curl_close($ch);
	$decoded = (array)LavuJson::json_decode($curl_info['body']);
	if ($b_draw_errors) {
		error_log($output);
		error_log($curl_info['body']);
	}
	if ((int)$curl_info['http_code'] < 300)
		return TRUE;
	return FALSE;
}

class ZENDESK {

	// gets a zendesk user id by the user's name
	// @$s_name string can either be a username or the user's full name
	// @$s_type string one of "agent" or "group"
	// @return  string the id, or 0 if the id can't be found
	public static function get_zendesk_user_id($s_name, $s_type = "agent") {
		$dbapi = ConnectionHub::getConn('poslavu')->getDBAPI();

		// check for groups
		if ($s_type == "group") {
			return self::get_zendesk_group_id($s_name);
		}

		// try to find the full name by the username
		$a_users = $dbapi->getAllInTable('accounts', array('username'=>$s_name), TRUE);
		if (count($a_users) > 0)
			$s_fullname = $a_users[0]['firstname'].' '.$a_users[0]['lastname'];
		else
			$s_fullname = $s_name;

		// try to find the user
		$a_zenusers = $dbapi->getAllInTable('zendesk_users', array('name'=>$s_fullname, 'type'=>'agent'), TRUE);
		if (count($a_zenusers) == 0)
			return 0;

		// return the id
		return (int)$a_zenusers[0]['zendesk_id'];
	}

	// returns an array for one of the customer fields to be submitted
	public static function get_customer_field_to_submit($s_name, $s_value) {
		switch($s_name) {
		case 'Phone Number':
			$a_retval = array(
				'id' => '22570291',
				'name' => 'Phone Number',
				'value' => $s_value,
			);
			break;
		case 'Customer Name':
			$a_retval = array(
				'id' => '22564852',
				'name' => 'Customer Name',
				'value' => $s_value,
			);
			break;
		case 'Location Name':
			$a_retval = array(
				'id' => '22567537',
				'name' => 'Location Name',
				'value' => $s_value,
			);
			break;
		case 'Data Name':
			$a_retval = array(
				'id' => '22575391',
				'name' => 'Data Name',
				'value' => $s_value,
			);
			break;
		}
		$object = new StdClass();
		$object->id = $a_retval['id'];
		$object->name = $a_retval['name'];
		$object->value = $a_retval['value'];
		return $object;
	}

	// gets all the internal notes from a ticket in zendesk
	// @$ticket_id: the id of the ticket to look up
	// @return: an array of internal notes, in the form array('dataname'=>dataname, 'internal notes'=>array(stdClass('time'=>datetime, 'note'=>note), ....)), can be empty
	//      or returns FALSE on error, or NULL if too many requests have been made
	public static function get_ticket_internal_notes($ticket_id) {

		// get the ticket from zendesk
		$s_dataname = "N/A";
		$a_internal_notes = array();
		$error = array();
		$curl_info = array();
		$decoded = array();
		$b_success = curlWrap('/tickets/'.$ticket_id.'/audits.json', NULL, 'GET', $error, $curl_info, $decoded, TRUE);

		// for debugging
		//print_r($error);
		//print_r($curl_info);
		//print_r($decoded);

		if ((int)$curl_info['http_code'] == 429) {
			return NULL;
		}

		// check that the ticket exists
		if (isset($decoded['description']) && $decoded['description'] == 'Not found') {
			//echo "Ticket not found";
			return FALSE;
		}

		// go through each audit of the ticket
		// audits contain events, some of which are internal notes
		foreach($decoded['audits'] as $audit_num=>$audit) {

			// check that events exist for this audit and that the audit is not an email
			$audit = self::array_to_object($audit);
			if (!is_object($audit) || !isset($audit->events))
				continue;
			if (!isset($audit->via) || !is_object($audit->via) || !isset($audit->via->source) || !is_object($audit->via->source) || !isset($audit->via->source->from) || !isset($audit->via->source->to))
				continue;
			$a_from = (array)$audit->via->source->from;
			$a_to = (array)$audit->via->source->to;
			if (count($a_from) > 0 || count($a_to) > 0)
				continue;
			$audit->events = (array)$audit->events;

			// search through all events for internal notes
			// disregard comments/emails by finding email notification events and creation events
			// finds the dataname based on create events
			$b_created_through_cp = FALSE;
			$a_event_retval = array();
			foreach($audit->events as $event_num=>$event) {
				if (isset($event->type) && ($event->type == 'Create' || $event->type == 'Change') && $event->field_name == '22575391' && strtolower(trim($event->value)) != 'na' && strtolower(trim($event->value)) != 'n/a') {
					$s_dataname = $event->value;
				}
				if (isset($event->value) && is_array($event->value)) {
					foreach($event->value as $value)
						if (strpos($value, 'created_through_cp_') === 0)
							$b_created_through_cp = TRUE;
				}
				if (!isset($event->public) || (string)$event->public != '0')
					continue;
				$o_internal_note = new stdClass();
				$o_internal_note->time = isset($audit->created_at) ? date('Y-m-d H:i:s', strtotime($audit->created_at)) : date('Y-m-d H:i:s',0);
				$o_internal_note->note = $event->body;
				$o_internal_note->zendesk_user_id = isset($event->author_id) ? $event->author_id : 0;
				$a_event_retval[] = $o_internal_note;
			}
			if (!$b_created_through_cp)
				$a_internal_notes = array_merge($a_event_retval, $a_internal_notes);
		}

		$a_retval = array(
			'dataname'=>$s_dataname,
			'internal_notes'=>$a_internal_notes,
		);
		return $a_retval;
	}

	public static function test_api() {
		$a_required_gets = array('url'=>'/tickets/12742/audits.json', 'json'=>'balls if I know', 'action'=>'GET, POST, PUT');
		foreach($a_required_gets as $req=>$options)
			if (!isset($_GET[$req]))
				return array('success'=>FALSE, 'error'=>"Please provide the GET variable {$req} (eg {$options})");

		$url = urldecode($_GET['url']);
		$json = $_GET['json'];
		$action = $_GET['action'];
		$a_error = array();
		$a_curl_info = array();
		$a_decoded = array();

		$success = curlWrap($url, $json, $action, $a_error, $a_curl_info, $a_decoded);

		return array('success'=>$success, 'error'=>$a_error, 'curl_info'=>$a_curl_info, 'decoded'=>$a_decoded);
	}

	// get an array all users
	// @return: array('success'=>TRUE/FALSE, 'error'=>''/'zendesk_api_rate_exceeded'/'other_error', 'users'=>array(o_user, ...))
	public static function get_users() {

		$a_users = array();

		foreach(array('agent', 'admin') as $s_role) {
			// do a curl to zendesk
			$a_error = array();
			$a_curl_info = array();
			$a_decoded = array();
			$success = curlWrap('/users.json?role='.$s_role, NULL, 'GET', $a_error, $a_curl_info, $a_decoded);

			// check for success
			if (!$success || (string)$a_curl_info['http_code'] == '429') {
				if ((string)$a_curl_info['http_code'] == '429')
					return array('success'=>FALSE, 'error'=>'zendesk_api_rate_exceeded', 'users'=>array());
				else
					return array('success'=>FALSE, 'error'=>'other_error', 'users'=>array());
			}
			if (!is_array($a_decoded['users']))
				return array('success'=>TRUE, 'error'=>'', 'users'=>array());

			$a_users = array_merge($a_users, $a_decoded['users']);
		}

		return array('success'=>TRUE, 'error'=>'', 'users'=>$a_users);
	}

	/**
	 * Gets all zendesk groups and saves them into poslavu_MAIN_db.zendesk_users
	 * @return integer -1 for error, or the number of users that were loaded
	 */
	public static function get_save_groups() {
		$dbapi = ConnectionHub::getConn('poslavu')->getDBAPI();

		// do a request to zendesk
		$a_error = array();
		$a_curl_info = array();
		$a_decoded = array();
		curlWrap("/groups.json", NULL, "GET", $a_error, $a_curl_info, $a_decoded);

		// check for errors
		if (count($a_error) > 0 || count($a_decoded) == 0 || !isset($a_decoded['groups']))
			return -1;

		// try to insert/update the groups
		$i_num_updated = 0;
		foreach($a_decoded['groups'] as $a_group) {
			$a_update_vars = array(
				'zendesk_id'=>$a_group['id'],
				'name'=>strtolower($a_group['name']),
				'account_prematch_id'=>0,
				'role'=>'',
				'type'=>'group'
			);
			$a_where_vars = array('zendesk_id'=>$a_update_vars['zendesk_id'], 'type'=>$a_update_vars['type']);
			$s_success = $dbapi->insertOrUpdate("poslavu_MAIN_db", "zendesk_users", $a_update_vars, $a_where_vars);
			if ($s_success !== 'b')
				$i_num_updated++;
		}

		return $i_num_updated;
	}

	// gets all users and saves them into poslavu_MAIN_db.zendesk_users
	// @return: -1 for error, or the number of users that were loaded
	public static function get_save_users() {
		$dbapi = ConnectionHub::getConn('poslavu')->getDBAPI();

		// load the users
		// check to make sure that the load succeeds
		$a_user_array = self::get_users();
		if (!$a_user_array['success'])
			return -1;
		$a_users = $a_user_array['users'];

		// unset zendesk users that aren't objects
		$a_to_unset = array();
		foreach($a_users as $k=>$v) {
			$a_users[$k] = self::array_to_object($v);
			if (!is_object($v))
				$a_to_unset[] = $k;
		}
		foreach($a_to_unset as $k)
			unset($a_users[$k]);

		// get the user first and last names
		foreach($a_users as $k=>$o_user) {
			$s_name = $o_user->name;
			$a_name = explode(' ', $s_name);
			if (count($a_name) >= 2) {
				$s_fname = $a_name[0];
				$s_lname = $a_name[count($a_name)-1];
			} else {
				$s_fname = '';
				$s_lname = '';
			}
			$a_users[$k]->fname = $s_fname;
			$a_users[$k]->lname = $s_lname;
		}

		// try to match to an account based on first name, last name
		foreach($a_users as $k=>$o_user) {
			$i_account_id = 0;
			$a_accounts = $dbapi->getAllInTable('accounts', array('firstname'=>$o_user->fname, 'lastname'=>$o_user->lname), TRUE, array('whereclause'=>"WHERE `firstname` LIKE '[firstname]' AND `lastname` LIKE '%[lastname]%'"));
			if (count($a_accounts) > 0) {
				$i_account_id = $a_accounts[0]['id'];
			}
			if ($i_account_id !== 0)
				$a_users[$k]->account_prematch_id = $i_account_id;
		}

		// save the users
		$a_example = $dbapi->getAllInTable('zendesk_users', array('id'=>1), TRUE);
		$a_example = $a_example[0];
		unset($a_example['id']);
		foreach($a_users as $o_user) {
			$o_user->zendesk_id = $o_user->id;
			$a_savevals = array();
			foreach($a_example as $k=>$v) {
				if (isset($o_user->$k))
					$a_savevals[$k] = $o_user->$k;
			}
			$a_wherevars = array('zendesk_id'=>$a_savevals['zendesk_id']);
			if (isset($a_savevals['zendesk_id']) && (int)$a_savevals['zendesk_id'] !== 0)
				$dbapi->insertOrUpdate('poslavu_MAIN_db', 'zendesk_users', $a_savevals, $a_wherevars);
		}

		return count($a_users);
	}

	// gets the last time that zendesk tickets wer updated
	// @return: the last time zendesk tickets were check for updates, plus 19 minutes
	public static function get_time_of_next_update(&$i_currently_being_updated = 0) {
		$i_last_check = 0;
		$a_vars = array(
			'last_check'=>array('load_from_file'=>TRUE, 'current_val'=>$i_last_check, 'default'=>0),
			'currently_being_updated'=>array('load_from_file'=>TRUE, 'current_val'=>0, 'default'=>0),
		);
		$a_vars = self::get_default_variables('get_updated_in_last_hour', $a_vars);
		if (in_array('currently_being_updated', $a_vars))
			$i_currently_being_updated = $a_vars['currently_being_updated'];
		$i_last_check = (int)$a_vars['get_updated_in_last_hour']['last_check'];
		$i_next_check = strtotime(date('Y-m-d H:i:s', $i_last_check).' +19 minutes');
		//error_log('it is now '.date('Y-m-d H:i:s').', next request should be at '.date('Y-m-d H:i:s',$i_next_check).' The file is '.($i_currently_being_updated == 0 ? 'not ' : '').'currently being updated.');
		return $i_next_check;
	}

	// get a list of ticket ids updated in the last hour
	// @return: an array('success'=>TRUE/FALSE, 'error'=>''/'previous_request_too_soon'/'zendesk_api_rate_exceeded'/'other_error', 'tickets'=>array(array('id'=>id,'updated'=>datetime), ...))
	public static function get_updated_in_last_hour($b_ignore_time_delay = FALSE) {

		// check that it's been at least 19 minutes since this was last called
		// reason: the zendesk api caches the results for 19 minutes on views
		$i_next_check = self::get_time_of_next_update();
		if (strtotime('now') < $i_next_check && !$b_ignore_time_delay)
			return array('success'=>FALSE, 'error'=>'previous_request_too_soon', 'tickets'=>array(), 'notes'=>'it is now '.date('Y-m-d H:i:s').', next request should be at '.date('Y-m-d H:i:s',$i_next_check));

		// query zendesk
		$a_error = array();
		$a_curl_info = array();
		$a_decoded = array();
		$a_vars = array(
			'currently_being_updated'=>array('load_from_file'=>FALSE, 'current_val'=>1, 'default'=>1),
		);
		$a_vars = self::get_default_variables('get_updated_in_last_hour', $a_vars);
		$success = curlWrap('/views/37190067/execute.json', NULL, 'GET', $a_error, $a_curl_info, $a_decoded);
		$a_vars = array(
			'currently_being_updated'=>array('load_from_file'=>FALSE, 'current_val'=>0, 'default'=>0),
		);
		$a_vars = self::get_default_variables('get_updated_in_last_hour', $a_vars);

		// check for success
		if (!$success || (string)$a_curl_info['http_code'] == '429') {
			if ((string)$a_curl_info['http_code'] == '429')
				return array('success'=>FALSE, 'error'=>'zendesk_api_rate_exceeded', 'tickets'=>array());
			else
				return array('success'=>FALSE, 'error'=>'other_error', 'tickets'=>array());
		}

		// get the ticket ids
		$a_ticket_ids = array();
		if (is_array($a_decoded) && isset($a_decoded['rows']) && is_array($a_decoded['rows'])) {
			foreach($a_decoded['rows'] as $k=>$o_row) {
				$o_row = self::array_to_object($o_row);
				$a_decoded['rows'][$k] = $o_row;
				if (is_object($o_row) && isset($o_row->ticket) && is_object($o_row->ticket) && $o_row->ticket->url) {
					$url = $o_row->ticket->url;
					$a_url = explode('/tickets/', $url);
					$a_id = explode('.json', $a_url[1]);
					$s_id = $a_id[0];
					$i_updated = strtotime($o_row->updated);
					$a_ticket_ids[] = array('id'=>$s_id, 'updated'=>date('Y-m-d H:i:s', $i_updated));
				}
			}
		}

		$i_last_check = strtotime('now');
		$a_vars = array(
			'last_check'=>array('load_from_file'=>FALSE, 'current_val'=>$i_last_check, 'default'=>$i_last_check),
		);
		$a_vars = self::get_default_variables('get_updated_in_last_hour', $a_vars);

		return array('success'=>TRUE, 'error'=>'', 'tickets'=>$a_ticket_ids);
	}

	// Get the highest ticket number from the past hour
	// try to only call this function once per minute
	// @return: the highest ticket number, -1 if there were not tickets in the last hour, or -2 if this api has been called too many times in the last minute
	public static function get_highest_ticket() {

		// check if it's been a minutes since the last request
		$i_last_check = 0;
		$i_highest_ticket = -2;
		$a_vars = array(
			'last_check'=>array('load_from_file'=>TRUE, 'current_val'=>$i_last_check, 'default'=>0),
			'highest_ticket'=>array('load_from_file'=>TRUE, 'current_val'=>$i_highest_ticket, 'default'=>-2),
		);
		$a_vars = self::get_default_variables('get_highest_ticket', $a_vars);
		$i_last_check = (int)$a_vars['get_highest_ticket']['last_check'];
		$i_highest_ticket = (int)$a_vars['get_highest_ticket']['highest_ticket'];
		if ((int)$i_last_check >= strtotime('1 minute ago'))
			return $i_highest_ticket;

		// do a curl to zendesk
		$a_error = array();
		$a_curl_info = array();
		$a_decoded = array();
		$success = curlWrap('/exports/tickets.json?start_time='.strtotime('-1 hours'), NULL, 'GET', $a_error, $a_curl_info, $a_decoded);

		// check for success
		if (!$success || (string)$a_curl_info['http_code'] == '429')
			return -2;
		if (count($a_decoded['results']) == 0)
			return -1;

		// get the highest ticket number
		foreach($a_decoded['results'] as $k=>$o_ticket) {
			$a_decoded['results'][$k] = self::array_to_object($o_ticket);
			if (!is_object($o_ticket) || !isset($o_ticket->url))
				continue;
			$s_url = $o_ticket->url;
			$a_url_parts = explode('/tickets/', $s_url);
			$s_ticket_num = $a_url_parts[1];
			$i_ticket_num = (int)$s_ticket_num;
			$i_highest_ticket = max($i_highest_ticket, $i_ticket_num);
		}

		// save variables
		$i_last_check = strtotime('now');
		$a_vars = array(
			'last_check'=>array('load_from_file'=>FALSE, 'current_val'=>$i_last_check, 'default'=>0),
			'highest_ticket'=>array('load_from_file'=>FALSE, 'current_val'=>$i_highest_ticket, 'default'=>-2),
		);
		$a_vars = self::get_default_variables('get_highest_ticket', $a_vars);

		return $i_highest_ticket;
	}

	// check if variables need to be loaded from the defaults file
	// if the file is opened, the values are saved back to the file (aka the file is opened once for reading, then again for writing)
	// @$s_classname: usually the function name that is calling this function
	// @$a_variables: array of variables to be loaded, in the format:
	//     array('varname'=>array('load_from_file'=>TRUE/FALSE, 'current_val'=>current value, 'default'=>default value), ...)
	// @return: the new variables, in the form
	//     array[$s_classname][$s_varname]=>value
	public static function get_default_variables($s_classname, &$a_variables) {

		// load values from the file
		require_once(dirname(__FILE__).'/../objects/json_decode.php');
		require_once(dirname(__FILE__).'/core_functions.php');
		$a_vars = array();
		$s_filename = '/var/tmp/contact_zendesk_variables.json';
		if (file_exists($s_filename)) {
			$previous_warn_level = error_reporting();
			error_reporting(-1);
			$s_contents = file_get_contents($s_filename);
			error_reporting($previous_warn_level);
			$a_vars = json_decode($s_contents, TRUE);
		}
		if (!isset($a_vars[$s_classname])) {
			$a_vars[$s_classname] = array();
		} else {
			$a_vars[$s_classname] = (array)$a_vars[$s_classname];
		}

		// either set the variable value to its current value, or
		// to the value in the file if that exists or the default value if it doesn't
		foreach($a_variables as $s_varname=>$a_var) {
			$b_load_from_file = $a_var['load_from_file'];
			$wt_current_val = $a_var['current_val'];
			if (!$b_load_from_file) {
				$a_vars[$s_classname][$s_varname] = $wt_current_val;
				continue;
			}
			$wt_default_value = $a_var['default'];
			$a_vars[$s_classname][$s_varname] = isset($a_vars[$s_classname][$s_varname]) ? $a_vars[$s_classname][$s_varname] : $wt_default_value;
		}

		// save the values
		file_put_contents($s_filename, json_encode($a_vars));

		return $a_vars;
	}

	// loads recently updated tickets from zendesk, up to the last update 150
	// @return: array('success'=>TRUE/FALSE, 'error'=>''/reason, 'last_check'=>datetime, 'max_updates_allowed'=>150, 'found'=>updated in last hour, 'num_updated'=>number of valid tickets, 'updated'=>array(ticketid=>update stats, ...), 'not_updated'=>array(ticketid=>reason, ...))
	public static function load_recently_updated($b_ignore_time_delay = FALSE) {

		// load some settings
		$i_quantity = 150;
		$i_last_check = 0;
		$a_vars = array(
			'last_check'=>array('load_from_file'=>TRUE, 'current_val'=>$i_last_check, 'default'=>0),
		);
		$a_vars = self::get_default_variables('load_recently_updated', $a_vars);
		$i_last_check = (int)$a_vars['load_recently_updated']['last_check'];
		$i_original_last_check = $i_last_check;

		// get the tickets
		// ignore tickets that were last updated before $i_last_updated
		$a_updated = self::get_updated_in_last_hour($b_ignore_time_delay);
		if ($a_updated['success'] == FALSE)
			return array('success'=>FALSE, 'error'=>$a_updated['error'], 'last_check'=>0, 'max_updates_allowed'=>$i_quantity, 'found'=>0, 'num_updated'=>0, 'updated'=>array(), 'not_updated'=>array());
		$a_updated = $a_updated['tickets'];
		$i_num_found = count($a_updated);
		foreach($a_updated as $k=>$a_ticket) {
			if (strtotime($a_ticket['updated']) < $i_last_check) {
				unset($a_updated[$k]);
			}
		}
		$i_last_check = strtotime('now');

		// save tickets
		$a_retval = array();
		$a_not_updated = array();
		$i_count = 0;
		foreach($a_updated as $a_ticket) {
			$a_saved_tickets = self::get_ticket_save_notes($a_ticket['id']);
			if ($a_saved_tickets['success'] === FALSE) {
				$a_not_updated[$a_ticket['id']] = $a_saved_tickets['error'];
				if ($a_saved_tickets['error'] == 'zendesk_api_rate_limit_exceeded')
					break;
				else
					continue;
			}
			$a_retval[$a_ticket['id']] = $a_saved_tickets['notes_imported'];
			$i_count++;
			if ($i_count > $i_quantity) {
				$i_last_check = strtotime($a_ticket['updated']);
				break;
			}
		}

		$a_vars = array(
			'last_check'=>array('load_from_file'=>FALSE, 'current_val'=>$i_last_check, 'default'=>$i_last_check),
		);
		$a_vars = self::get_default_variables('load_recently_updated', $a_vars);

		return array('success'=>TRUE, 'error'=>'', 'last_check'=>date('Y-m-d H:i:s',$i_original_last_check), 'max_updates_allowed'=>$i_quantity, 'found'=>$i_num_found, 'num_updated'=>$i_count, 'updated'=>$a_retval, 'not_updated'=>$a_not_updated);
	}

	// loads "$i_quantity" of tickets from zendesk, starting at ticket number "$i_start"
	// @$i_start: the ticket number to start at. If passed -1, it starts at dirname(__FILE__).'/contact_zendesk_variables.json'
	// @$i_quantity: the number of tickets to load. If passed -1, it defaults to 80
	// @return: array('start'=>the start value, 'quantity'=>the number of tickets that were tried to be loaded, 'loaded'=>array(the tickets that were loaded), 'error'=>any errors encountered)
	public static function load_tickets($i_start, $i_quantity) {

		$a_retval = array();

		// check if variables need to be loaded from the defaults file
		if ($i_start == -1 || $i_quantity == -1) {
			$a_vars = array(
				'start'=>array('load_from_file'=>($i_start == -1 ? TRUE : FALSE), 'current_val'=>$i_start, 'default'=>0),
				'quantity'=>array('load_from_file'=>($i_quantity == -1 ? TRUE : FALSE), 'current_val'=>$i_quantity, 'default'=>80),
			);
			$a_vars = self::get_default_variables('load_tickets', $a_vars);
			$i_start = (int)$a_vars['load_tickets']['start'];
			$i_quantity = (int)$a_vars['load_tickets']['quantity'];
		}
		$i_new_start = $i_start;

		// request the highest ticket available
		$i_highest_ticket = (int)self::get_highest_ticket();
		if ($i_highest_ticket < 0)
			return array('start'=>$i_start, 'quantity'=>$i_quantity, 'loaded'=>array(), 'error'=>"highest ticket is {$i_highest_ticket}");

		// load the tickets
		for ($i = 0; $i < $i_quantity; $i++) {

			// calculate the ticket number that is about to be loaded, and stop if it is greater than the currently highest ticket
			$i_ticket_num = $i_start + $i;
			if ($i_ticket_num > $i_highest_ticket)
				break;

			// load the ticket and save it to restaurant_notes
			// id, restaurant_id, message, ticket_id, created_on, updated_at, support_ninja
			$i_new_start = min($i_ticket_num, $i_highest_ticket);

			// save tickets
			$a_saved_tickets = self::get_ticket_save_notes($i_ticket_num);
			if ($a_saved_tickets['success'] === FALSE) {
				if ($a_saved_tickets['error'] == 'zendesk_api_rate_limit_exceeded')
					break;
				else
					continue;
			}
			$a_retval = array_merge($a_retval, $a_saved_tickets['notes_imported']);
		}

		// save the highest ticket loaded+1 as the start val
		$i_new_start = min($i_new_start, $i_highest_ticket);
		$s_now = date('Y-m-d H:i:s');
		$a_vars = array(
			'start'=>array('load_from_file'=>FALSE, 'current_val'=>$i_new_start+1, 'default'=>$i_new_start+1),
			'last_run'=>array('load_from_file'=>FALSE, 'current_val'=>$s_now, 'default'=>$s_now),
		);
		$a_vars = self::get_default_variables('load_tickets', $a_vars);

		return array('start'=>$i_start, 'quantity'=>$i_quantity, 'total'=>$i_highest_ticket, 'last_run'=>$s_now, 'loaded'=>$a_retval, 'error'=>'');
	}

	// queries zendesk for the ticket and saves any internal notes associated with it
	// @$i_ticket_num: the ticket id
	// @return: an array('success'=>TRUE/FALSE, 'error'=>''/'zendesk_api_rate_limit_exceeded'/'restaurant_not_found:dataname'/'ticket_not_found', 'notes_imported'=>array(array('restaurant_id','message','created_on','ticket_id','support_ninja'), ...))
	public static function get_ticket_save_notes($i_ticket_num) {
		$dbapi = ConnectionHub::getConn('poslavu')->getDBAPI();

		// get the ticket and check for errors
		$a_internal_notes = self::get_ticket_internal_notes($i_ticket_num);
		if ($a_internal_notes === FALSE) // some error
			return array('success'=>FALSE, 'error'=>'ticket_not_found', 'notes_imported'=>array());
		if ($a_internal_notes === NULL) // too many requests have been made
			return array('success'=>FALSE, 'error'=>'zendesk_api_rate_limit_exceeded', 'notes_imported'=>array());
		$s_dataname = $a_internal_notes['dataname'];
		$a_restaurants = $dbapi->getAllInTable('restaurants', array('data_name'=>$s_dataname), TRUE);
		if (count($a_restaurants) == 0)
			return array('success'=>FALSE, 'error'=>'restaurant_not_found:'.$s_dataname, 'notes_imported'=>array());

		// save each note
		$a_notes_imported = array();
		foreach($a_internal_notes['internal_notes'] as $o_internal_note) {
			$a_savevals = array(
				'restaurant_id'=>$a_restaurants[0]['id'],
				'message'=>$o_internal_note->note,
				'created_on'=>$o_internal_note->time,
				'ticket_id'=>$i_ticket_num,
				'support_ninja'=>'zendesk:'.$o_internal_note->zendesk_user_id,
			);
			$a_notes_imported[] = $a_savevals;
			$success = $dbapi->insertOrUpdate('poslavu_MAIN_db', 'restaurant_notes', $a_savevals, $a_savevals);
		}

		return array('success'=>TRUE, 'error'=>'', 'notes_imported'=>$a_notes_imported);
	}

	public static function next_update_timer_tostr() {

		$i_currently_being_updated = 0;
		$i_next_time = self::get_time_of_next_update($i_currently_being_updated);
		return "<input type='hidden' name='seconds_until_zendesk_update' value='".($i_next_time - strtotime('now')+60)."' /><script type='text/javascript'>start_zendesk_counter();</script><div style='display:inline-block;'></div>".($i_currently_being_updated == 1 ? "<font> (pulling data from ZenDesk now)</font>" : "");
	}

	/*******************************************************************
	 *                P R I V A T E   F U N C T I O N S                *
	 ******************************************************************/

	/**
	 * Get the user id of a zendesk group
	 * @param  string  $s_name group name
	 * @return integer         the group id
	 */
	private static function get_zendesk_group_id($s_name) {
		$dbapi = ConnectionHub::getConn('poslavu')->getDBAPI();

		$a_zenusers = $dbapi->getAllInTable('zendesk_users', array('name'=>strtolower($s_name), 'type'=>'group'), TRUE);
		if (count($a_zenusers) == 0)
			return 0;
		return (int)$a_zenusers[0]['zendesk_id'];
	}

	private static function array_to_object(&$a_value) {
		if (!is_array($a_value) && !is_object($a_value))
			return $a_value;
		foreach($a_value as $k=>$v) {
			$a_value[$k] = self::array_to_object($v);
		}
		return (object)$a_value;
	}
}

?>
