<?php
//echo "works";
	function query_db_ck($db_name, $query){
			
		$result = lavu_query($query);
			if (!$result) {
  			  die ('Can\'t use query : ' . lavu_dberror());
			}
	
		return $result;
		
	}//query_db_ck()
	
	
	function pad($number, $length) {
   		/* function will pad a number with leading zeroes so the resulting string is "length" length. For example if length is 2 and the passed in number is 5 the value returned is 05.*/
   			  $str = "" . $number;
    			while (strlen($str) < $length) {
        				$str = "0" . $str;
    			}
   
   			  return $str;

		}//pad()

?>
