<?php
session_start(); //call session_start before everything else.
require_once(dirname(__FILE__) . "/core_functions.php");
require_once(dirname(__FILE__)."/chain_functions.php");
lavu_select_db(sessvar("admin_database"));

if (count($_POST) > 1) { //form was submitted to reach here.
    $dataname = $_POST['dataname'];
	$location_id = sessvar("locationid");
	if ($_POST['mulCur']) {
		$enableMultiCurrency = enableMultiCurrency($location_id, $dataname);
	}
	$validationFunc = 'validateAssociatedArrayElementsForInsert';
	$exchange_rates_to_insert = getAllExchangeRatesToInsert($_POST, $validationFunc);
	if (isset($exchange_rates_to_insert)) { //Null value will return false. //INSERT FIRST
		//Format of insert: INSERT INTO tbl_name (a,b,c) VALUES (1,2,3),(4,5,6),(7,8,9);
		$query = "INSERT INTO `$dataname`.`secondary_exchange_rates` ( `primary_restaurantid`, `from_currency_id`, `to_currency_id`,`from_currency_code`,`to_currency_code`,`rate`,`effective_date`, `_deleted`) VALUES ".$exchange_rates_to_insert;
		$query_result = mlavu_query($query);
		if (lavu_dberror()) {
			error_log(__FILE__ . "  " . __LINE__ . " LAVU DB ERROR: " . lavu_dberror());
		}
	}
	$validationFunc = 'validateAssociatedArrayElementsForUpdate';
	$exchange_rates_to_update = getAllExchangeRatesToUpdate($_POST, $validationFunc);
	if (isset($exchange_rates_to_update)) {
		foreach ($exchange_rates_to_update as $key=>$val) {
			$query = "UPDATE `$dataname`.`secondary_exchange_rates` SET " . $val ."WHERE `id`= $key";
			$query_result = mlavu_query($query);
			if (lavu_dberror()){
				error_log(__FILE__ . "  " . __LINE__ . " LAVU DB ERROR: " . lavu_dberror());
			}
		}
	}
	header("Location: " . $_POST['returnURL'] . "&refresh_for_save=1");
}

/**********************************************************************************************************************/
/**********************************************************************************************************************/
/*                              BEGIN FUNCTIONS                                                                       */
/**********************************************************************************************************************/
/**********************************************************************************************************************/

function updateActionLog($details){
	$dbname = sessvar('admin_database');
	$action = "Add Currency Rate";
	$location_info = sessvar('location_info');
	$loc_id = $location_info['id'];
	$user = sessvar('admin_fullname');
	$user_id = admin_info('loggedin');
	$server_time = date("Y-m-d H:i:s");
	$details_short = "updating_admin_cp_currency_rates";

	$values = "";
	for ($i = 0; $i < count($details); $i++) {
		$details[$i] = str_replace("'", "", $details[$i]);
		$values .= "('$action', $loc_id, '$server_time' ,'$user', $user_id, '$server_time', '$details[$i]', '$details_short'),";
	}
	$values = rtrim($values, ",");
	$query = "INSERT INTO `$dbname`.`action_log` (`action`, `loc_id`, `time`, `user`, `user_id`, `server_time`, `details`, `details_short`) VALUES" . $values;
	mlavu_query($query);
	if (lavu_dberror()){
		error_log(__FILE__ . "  " . __LINE__ . " LAVU DB ERROR: " . lavu_dberror());
	}
}

function getAllExchangeRatesToInsert($postVars, $validationFunc){ //returns a comma separated string using implode on an array
	$submissionString = "";
	$usedKeySubstrings = array();
	$actionArray = array();
	$primary_restaurnat_id = sessvar("admin_companyid"); //$_SESSION['primary_restaurantid'];
	$arrayKeys = array_keys($postVars);
	foreach ($arrayKeys as $key) {
		$keySubstring = substr($key, strrpos($key, "_") + 1); //Gets everything after the last occurrence of the '_' character.

		if (!in_array($keySubstring, $usedKeySubstrings)) {
			$associatedArrayElements = getAllAssociatedArrayElements($postVars, $keySubstring);
			if ($validationFunc($associatedArrayElements)) { //We only want to use this if the fields are valid
				$effectiveDate = formatDateForDatabase(getValueFromKeyBeginningWith($associatedArrayElements, "ic_1_effective_date"));
				$rate = getValueFromKeyBeginningWith($associatedArrayElements, "ic_1_rate");
				$currencyFromId = getValueFromKeyBeginningWith($associatedArrayElements, "ic_1_from_currency_id");
				$currencyToId = getValueFromKeyBeginningWith($associatedArrayElements, "ic_1_to_currency_id");
				$currencyFrom = getDisplayableCurrencyNameOnlyFromCurrencyId($currencyFromId);
				$currencyTo = getDisplayableCurrencyNameOnlyFromCurrencyId($currencyToId);

				$submissionString .= " ($primary_restaurnat_id, $currencyFromId, $currencyToId, '$currencyFrom', '$currencyTo', $rate, '$effectiveDate', '0'),";
				$actionArray[] = "currencyFrom=$currencyFrom,currencyTo=$currencyTo,effectiveDate=$effectiveDate,rate=$rate";
			}
			$usedKeySubstrings[] = $keySubstring;
		}
	}
	if (count($actionArray) > 0) {
		updateActionLog($actionArray);
	}
//	remove the last comma from the string if we have some result, otherwise return null.
	if (strlen($submissionString) > 0) {
		return rtrim($submissionString, ",");
	}
	return null;
}

function getAllExchangeRatesToUpdate($postVars, $validationFunc){
	$submissionString = array();
	$usedKeySubstrings = array();
	$primary_restaurnat_id = sessvar("admin_companyid"); //$_SESSION['primary_restaurantid'];
	$arrayKeys = array_keys($postVars);
	foreach($arrayKeys as $key) {

		$keySubstring = substr($key, strrpos($key, "_") + 1); //Gets everything after the last occurrence of the '_' character.

		if (!in_array($keySubstring, $usedKeySubstrings)) {
			$associatedArrayElements = getAllAssociatedArrayElements($postVars, $keySubstring);
			if ($validationFunc($associatedArrayElements)) { //We only want to use this if the fields are valid
				$effectiveDate = formatDateForDatabase(getValueFromKeyBeginningWith($associatedArrayElements, "ic_1_effective_date"));
				$rate = getValueFromKeyBeginningWith($associatedArrayElements, "ic_1_rate");
				$currencyFromId = getValueFromKeyBeginningWith($associatedArrayElements, "ic_1_from_currency_id");
				$currencyToId = getValueFromKeyBeginningWith($associatedArrayElements, "ic_1_to_currency_id");
				$deletedFlag = getValueFromKeyBeginningWith($associatedArrayElements, "ic_1_delete");
				$id = getValueFromKeyBeginningWith($associatedArrayElements, "ic_1_id");
				$submissionString[$id] = " `primary_restaurantid`=$primary_restaurnat_id, `from_currency_id`='$currencyFromId', `to_currency_id`='$currencyToId',`from_currency_code`='',`to_currency_code`='', `rate`=$rate, `effective_date`='$effectiveDate', `_deleted`='$deletedFlag'";
			}
			$usedKeySubstrings[] = $keySubstring;
		}
	}
//	remove the last comma from the string if we have some result, otherwise return null.
	if (count($submissionString) > 0) {
		return $submissionString;
	}
	return null;
}

function formatDateForDatabase($dateString){
	$location_info = sessvar("location_info");
	$date_timestamp = date_timestamp_get(new DateTime($dateString)); //timestamp.

	$start_end_time = $location_info['day_start_end_time'];
	$hours = (int)substr($start_end_time, 0, 2);
	$minutes = (int)substr($start_end_time, 2, 2);

	$full_timestamp = $date_timestamp + (3600 * $hours) + (60 * $minutes);
	$date = new DateTime();
	$date->setTimestamp($full_timestamp);
	return $date->format('Y-m-d H:i:s');
}

//Gets elements which are associated with a single row in the submit exchange rates page.
function getAllAssociatedArrayElements($postVars, $keySubstring){
	$associatedArrayElements = array();
	foreach ($postVars as $key=>$val){
		$currentKeyNumber = substr($key, strrpos($key, "_") + 1);
		if (substr($key, 0, 5) == "ic_1_" && $keySubstring == $currentKeyNumber) {
			$associatedArrayElements[$key] = $val;
		}
	}
	return $associatedArrayElements;
}

//validateAssociatedArrayElements Returns True if the elements are valid, false if not.
function validateAssociatedArrayElementsForInsert($associatedArrayElements){
	if (count($associatedArrayElements) != 6) {
		return false;
	}

	$currencyFrom = getValueFromKeyBeginningWith($associatedArrayElements, "ic_1_from_currency_id");
	$effectiveDate = getValueFromKeyBeginningWith($associatedArrayElements, "ic_1_effective_date");
	$rate = getValueFromKeyBeginningWith($associatedArrayElements, "ic_1_rate");
	$deleteVal = getValueFromKeyBeginningWith($associatedArrayElements, "ic_1_delete");
	$idVal = getValueFromKeyBeginningWith($associatedArrayElements, "ic_1_id");

	if ($idVal != "") {
		return false;
	}

	if (!isset($currencyFrom) || $currencyFrom == "0") {
		error_log(__FILE__ . " " . __LINE__ . " select currency code");
		return false;
	}
	if (!isset($deleteVal) || $deleteVal == '1') {
		error_log(__FILE__ . " " . __LINE__ . " delete val");
		return false;
	}
	if (!isValidDate($effectiveDate)) { //order is important here for short-circuit logic
		error_log(__FILE__ . " " . __LINE__ . " isvalid date");
		return false;
	}
	if (!isValidRate($rate)) { //order is important here for short-circuit logic
		error_log(__FILE__ . " " . __LINE__ . " isvalid rate");
		return false;
	}
	return true;
}

function validateAssociatedArrayElementsForUpdate($associatedArrayElements){
	if (count($associatedArrayElements) != 6){
		return false;
	}

	$currencyFrom = getValueFromKeyBeginningWith($associatedArrayElements, "ic_1_from_currency_id");
	$currencyTo = getValueFromKeyBeginningWith($associatedArrayElements, "ic_1_to_currency_code");
	$effectiveDate = getValueFromKeyBeginningWith($associatedArrayElements, "ic_1_effective_date");
	$rate = getValueFromKeyBeginningWith($associatedArrayElements, "ic_1_rate");
	//$deleteVal = getValueFromKeyBeginningWith($associatedArrayElements, "ic_1_delete");
	$idVal = getValueFromKeyBeginningWith($associatedArrayElements, "ic_1_id");

	if ($idVal == "") {
		return false;
	}

	if (!isset($currencyFrom) || $currencyFrom == "0" || !isset($currencyTo) || $currencyTo == "0") {
		error_log(__FILE__ . " " . __LINE__ . " select currency code");
		return false;
	}
	if (!isValidDate($effectiveDate)) { //order is important here for short-circuit logic
		error_log(__FILE__ . " " . __LINE__ . " isvalid date");
		return false;
	}
	if (!isValidRate($rate)) { //order is important here for short-circuit logic
		error_log(__FILE__ . " " . __LINE__ . " isvalid rate");
		return false;
	}
	error_log(__FILE__ . " " . __LINE__ . " Made it");
	return true;
}

function getValueFromKeyBeginningWith($associativeArray, $keySubstring){
	foreach ($associativeArray as $key=>$val) {
		if (substr($key, 0, strlen($keySubstring)) == $keySubstring) {
			return $val;
		}
	}
	return null;
}

function isValidDate($date) {
	$current_date = new DateTime();
	$current_date->modify('Y-m-d');
	$current_date->setTime(0, 0, 0);

	$set_date = new DateTime($date);
	$set_date->modify('Y-m-d');
	$set_date->setTime(0, 0, 0);

	if ($set_date < $current_date) { //Ensure we're not saving the date if it is before the current date.
		return false;
	}

	if (preg_match("/^(0*[1-9]|1*[0-2])\/(0*[1-9]|[1-2]*[0-9]|3*[0-1])\/[0-9]{1,2}$/", $date)) {
		return true;
	} else {
		return false;
	}
}

function isValidRate($rate) {
	if (strlen($rate) > 22) {
		return false;
	}

	if (preg_match('/^(\d)*\.?(\d)*$/', $rate)) {
		return true;
	} else {
		return false;
	}
}

/**
* This function is used to auto update `enable_multiple_secondary_currency` setting.
*
* @param string $locationId : location id
* @param string $dataName : location db name
*
* @return boolean true/false
*/
function enableMultiCurrency($locationId, $dataName) {
	$result = false;
	$configQuery = mlavu_query( "SELECT `setting`, `value` FROM `$dataName`.`config` WHERE `setting` = 'enable_multiple_secondary_currency'");
	if (mysqli_num_rows($configQuery) > 0) {
		$result = mlavu_query("INSERT INTO `$dataName`.`config` (`location`, `type`, `setting`, `value`) VALUES ('[1]', 'location_config_setting', 'enable_multiple_secondary_currency', '1')", $locationId);
	} else {
		$result = mlavu_query("UPDATE `$dataName`.`config` SET `value` = '1' WHERE `location` = '[1]' AND `type` = 'location_config_setting' AND `setting` = 'enable_multiple_secondary_currency'", $locationId);
	}

	return $result;
}