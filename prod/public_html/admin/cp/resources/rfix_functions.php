<?php

	function process_fix($date_reports_by,$date_start,$date_end,$locationid,$order_id="",$special_query="")
	{
		process_fix_subtotal_mismatches($date_reports_by,$date_start,$date_end,$locationid,$order_id,$special_query);
		fix_blank_taxes_for_order_contents($date_reports_by,$date_start,$date_end,$locationid,$order_id,$special_query);
		process_fix_voided_contents_tax($date_reports_by,$date_start,$date_end,$locationid,$order_id,$special_query);
		process_fix_order_subtotal_mismatches($date_reports_by,$date_start,$date_end,$locationid,$order_id,$special_query);
		process_fix_transaction_to_order_mismatch($date_reports_by,$date_start,$date_end,$locationid,$order_id,$special_query);
		// process_fix_auto_gratuity($date_reports_by,$date_start,$date_end,$locationid,$order_id,$special_query);
		// process_fix_auto_gratuity_split_checks($date_reports_by,$date_start,$date_end,$locationid,$order_id,$special_query);
		// process_fix_auto_gratuity_breakout($date_reports_by,$date_start,$date_end,$locationid,$order_id,$special_query);

		//process_fix_itax($date_reports_by,$date_start,$date_end,$locationid,$order_id,$special_query);
		if (!empty($order_id)) process_fill_alt_transaction_totals($locationid,$order_id);
	}
	
	function process_fix_all($date_reports_by,$date_start,$date_end,$locationid)
	{
		return process_fix($date_reports_by,$date_start,$date_end,$locationid);
	}

	function process_fix_order($order_id,$locationid="")
	{
		return process_fix("","","",$locationid,$order_id);
	}

	$explain_done = false;
	function process_explain_mismatches($date_reports_by,$date_start,$date_end,$locationid)
	{
		global $explain_done;
		if($explain_done)
		{
			return;
		}
		else
		{
			$explain_done = true;
		}
		
		//process_fix_subtotal_mismatches($date_reports_by,$date_start,$date_end,$locationid);
		process_ag_data($date_reports_by,$date_start,$date_end,$locationid);
		
		$order_query = lavu_query("select * from `orders` where `[1]` >= '[2]' and `[1]` < '[3]' and `location_id`='[4]' and `void`!='1'",$date_reports_by,$date_start,$date_end,$locationid);
		while($order_query !== FALSE && $order_read = mysqli_fetch_assoc($order_query))
		{
			$idiscount = $order_read['idiscount_amount'];
			$subtotal = numformat($order_read['subtotal'] - $order_read['itax']);
			$discount = $order_read['discount'];
			$tax = $order_read['tax'] - $order_read['itax'];
			$total_before_ag = $order_read['total'] - $order_read['gratuity'];
			$ag_total = $order_read['gratuity'];
			$ag_card = $order_read['ag_card'];
			$ag_cash = $order_read['ag_cash'];
			$total = $order_read['total'];
			$cash_paid = $order_read['cash_applied'];
			$card_paid = $order_read['card_paid'];
			$alt_paid = $order_read['alt_paid'] + $order_read['gift_certificate'];
			$cash_tip = $order_read['cash_tip'];
			$card_tip = $order_read['card_gratuity'];
			$order_id = $order_read['order_id'];
			$order_opened = $order_read['opened'];
			$order_closed = $order_read['closed'];
			
			$extra_order_info = " <font color='#bbbbbb'>($order_id opened: $order_opened closed: $order_closed)</font>";
			
			$content_query = lavu_query("select sum(`tax_amount` + `itax`) as `tax`,sum((`price` * `quantity`) - `itax`) as `item_total`, sum((`modify_price` + `forced_modifiers_price`) * `quantity`) as `modifiers`, sum(`idiscount_amount`) as `idiscount`, `discount_amount` as `check_discount`, sum(`after_discount`) as `after_discount`, sum(`subtotal_with_mods` - `idiscount_amount` - `itax`) as `subtotal`, sum(`after_discount` + `discount_amount` - `itax`) as `subtotal2`, sum(`subtotal_with_mods` - `discount_amount` + `tax_amount` - `idiscount_amount`) as `total` from `order_contents` where `order_id`='[1]' and `loc_id`='[2]'",$order_read['order_id'], $locationid);
			if($content_query !== FALSE && mysqli_num_rows($content_query))
			{
				$content_read = mysqli_fetch_assoc($content_query);
				$contents_tax = $content_read['tax'];
				$contents_item_total = $content_read['item_total'];
				$contents_modifiers = $content_read['modifiers'];
				$contents_idiscount = $content_read['idiscount'];
				$contents_subtotal = numformat($content_read['subtotal']);
				$contents_check_discount = $content_read['check_discount'];
				$contents_after_discount = $content_read['after_discount'];
				$contents_total = $content_read['total'];

				if($contents_subtotal != $subtotal)
				{
					echo "Content Mismatch: contents:$contents_subtotal = order:$subtotal $extra_order_info<br>";
				}
			}
			
			$trans_totals = array();
			$trans_totals['cash'] = 0;
			$trans_totals['card'] = 0;
			$trans_totals['gc'] = 0;
			$trans_totals['other'] = 0;
			
			/*$trans_query = lavu_query("select `pay_type`,`action`,`voided`,`card_type`, IF(`voided`='1',0,IF(`action`='Refund', (0 - sum(`total_collected` + `tip_amount`)), sum(`total_collected` + `tip_amount`))) as `amount` from `cc_transactions` where `order_id`='[1]' group by concat(`pay_type`,' ',`card_type`,`action`,`voided`,IF(`cc_transactions`.`order_id` LIKE 'Paid%','PaidInOut','')) as `line_group`",$order_read['order_id']);
			while($trans_read = mysqli_fetch_assoc($trans_query))
			{
				$pay_amount = numformat($trans_read['amount']);
				$pay_type = $trans_read['pay_type'];
				if($pay_type=="Cash") $use_type = "cash";
				else if($pay_type=="Card") $use_type = "card";
				else if($pay_type=="Gift Certificate") $use_type = "gc";
				else $use_type = "Other";
				$trans_totals[$use_type] += $pay_amount;
			}
			
			if($trans_totals['cash']!=$cash_paid) echo "Pay Mismatch: Cash:" . $trans_totals['cash'] . " order:$cash_paid $extra_order_info<br>";
			if($trans_totals['card']!=$card_paid) echo "Pay Mismatch: Card:" . $trans_totals['card'] . " order:$card_paid $extra_order_info<br>";
			//if($trans_totals['gc']!=$gift_paid) echo "Pay Mismatch: GC:" . $trans_totals['gc'] . " order:$gift_paid $extra_order_info<br>";
			if($trans_totals['other']!=$alt_paid) echo "Pay Mismatch: Other:" . $trans_totals['other'] . " order:$alt_paid $extra_order_info<br>";*/
		}
	}
	
	function process_fix_transaction_to_order_mismatch($date_reports_by, $date_start, $date_end, $locationid, $order_id='', $special_query ='')
	{
		$where_cond = '';
		if(trim($special_query)!="")
			$where_cond = "($special_query)";
		else if($order_id!="")
			$where_cond = "(`orders`.`order_id` = '[order_id]' and `location_id`='[location_id]')";
		else
			$where_cond = "(`[date_reports_by]`>'[date_start]' and `[date_reports_by]`<'[date_end]' and location_id='[location_id]')";

		$query_params = array(
			'date_reports_by' => $date_reports_by,
			'date_start' => $date_start,
			'date_end' => $date_end,
			'location_id' => $locationid,
			'order_id' => $order_id
		);

		$query = "SELECT `orders`.`id`, `orders`.`order_id`, FORMAT(if(`cc_transactions`.`action`='Refund',-1,1)*SUM(`cc_transactions`.`total_collected`),4) as `collected`, FORMAT(SUM(`orders`.`card_paid` + `orders`.`cash_applied` + `orders`.`alt_paid` + `orders`.`gift_certificate`)/COUNT(*),4) as `order_collected` FROM `orders` JOIN `cc_transactions` ON `orders`.`order_id` = `cc_transactions`.`order_id` AND `orders`.`location_id` = `cc_transactions`.`loc_id` WHERE `cc_transactions`.`action` IN ('Sale','Refund') AND `cc_transactions`.`voided`='0' AND `orders`.`order_id` NOT LIKE 'Paid%' AND {$where_cond} GROUP BY `orders`.`id` HAVING `collected` != `order_collected`";

		if(( $query_result = lavu_query( $query, $query_params )) === FALSE ){
			return;
		}

		if( !mysqli_num_rows( $query_result ) ) {
			return;
		}

		if (number_format($correct_cc_amount,2) != number_format($order_read['card_paid'],2)) {
			//echo "HERE 4: $correct_cc_amount<br>";
			lavu_query("UPDATE `orders` SET `card_paid` = '[1]' WHERE `id` = '[2]'", number_format($correct_cc_amount, $decimal_places, ".", ""), $order_read['id']);
		}

		while( $query_order = mysqli_fetch_assoc( $query_result ) ){
			$query_order['id'];
			$query_order['order_id'];

			$update_args = array(
				'id' => $query_order['id'],
				'order_id' => $query_order['order_id'],
				'location_id' => $locationid
			);

			lavu_query("UPDATE `orders` SET `cash_applied` = (SELECT SUM(if(`cc_transactions`.`action`='Refund',-1,1)*`cc_transactions`.`total_collected`) as `sum` FROM `cc_transactions` WHERE `order_id` = '[order_id]' AND `loc_id`='[location_id]' AND `cc_transactions`.`action` IN ('Sale','Refund') AND `cc_transactions`.`voided`='0' AND `cc_transactions`.`pay_type_id`='1' AND `cc_transactions`.`order_id`='[order_id]') WHERE `orders`.`id` = '[id]' AND `location_id`='[location_id]' LIMIT 1", $update_args);
			lavu_query("UPDATE `orders` SET `card_paid` = (SELECT SUM(if(`cc_transactions`.`action`='Refund',-1,1)*`cc_transactions`.`total_collected`) as `sum` FROM `cc_transactions` WHERE `order_id` = '[order_id]' AND `loc_id`='[location_id]' AND `cc_transactions`.`action` IN ('Sale','Refund') AND `cc_transactions`.`voided`='0' AND `cc_transactions`.`pay_type_id`='2' AND `cc_transactions`.`order_id`='[order_id]') WHERE `orders`.`id` = '[id]' AND `location_id`='[location_id]' LIMIT 1", $update_args);
			lavu_query("UPDATE `orders` SET `gift_certificate` = (SELECT SUM(if(`cc_transactions`.`action`='Refund',-1,1)*`cc_transactions`.`total_collected`) as `sum` FROM `cc_transactions` WHERE `order_id` = '[order_id]' AND `loc_id`='[location_id]' AND `cc_transactions`.`action` IN ('Sale','Refund') AND `cc_transactions`.`voided`='0' AND `cc_transactions`.`pay_type_id`='3' AND `cc_transactions`.`order_id`='[order_id]') WHERE `orders`.`id` = '[id]' AND `location_id`='[location_id]' LIMIT 1", $update_args);
			lavu_query("UPDATE `orders` SET `alt_paid` = (SELECT SUM(if(`cc_transactions`.`action`='Refund',-1,1)*`cc_transactions`.`total_collected`) as `sum` FROM `cc_transactions` WHERE `order_id` = '[order_id]' AND `loc_id`='[location_id]' AND `cc_transactions`.`action` IN ('Sale','Refund') AND `cc_transactions`.`voided`='0' AND `cc_transactions`.`pay_type_id` > '3' AND `cc_transactions`.`order_id`='[order_id]') WHERE `orders`.`id` = '[id]' AND `location_id`='[location_id]' LIMIT 1", $update_args);
		}
	}
	
	function calculate_and_update_ag_breakouts( $order_id, $locationid ){

	}

	function process_fix_auto_gratuity_split_checks( $date_reports_by, $date_start, $date_end, $locationid, $order_id='', $special_query =''  ){
		$where_cond = '';
		if(trim($special_query)!="")
			$where_cond = "($special_query)";
		else if($order_id!="")
			$where_cond = "(`orders`.`order_id` = '[order_id]' and `location_id`='[location_id]')";
		else
			$where_cond = "(`[date_reports_by]`>'[date_start]' and `[date_reports_by]`<'[date_end]' and location_id='[location_id]')";

		$query_params = array(
			'date_reports_by' => $date_reports_by,
			'date_start' => $date_start,
			'date_end' => $date_end,
			'location_id' => $locationid,
			'order_id' => $order_id
		);
		$query = "SELECT `orders`.`order_id`, `orders`.`gratuity` as `order_gratuity`, SUM(`split_check_details`.`gratuity`) as `check_gratuity` FROM `orders` JOIN `split_check_details` ON `orders`.`order_id`=`split_check_details`.`order_id` AND `orders`.`location_id`=`split_check_details`.`loc_id` WHERE {$where_cond} GROUP BY `orders`.`order_id` HAVING `orders`.`gratuity` != SUM(`split_check_details`.`gratuity`)";

		if( !($query_result = lavu_query( $query, $query_params ) ) ){
			return;
		}

		if( !mysqli_num_rows( $query_result ) ) {
			return;
		}

		// order_id, check, subtotal, gratutity
		while( $query_order = mysqli_fetch_assoc( $query_result ) ){
			$sum_totals = 0;
			$gratutity_totals = array();
			if( !($check_query_result = lavu_query( "SELECT `id, `order_id`, `check`, `subtotal` + `tax` AS `total`, `order_id`, `gratutiy` FROM `split_check_details` WHERE `order_id` = '[order_id]' AND `loc_id` = '[loc_id]'", $check_query_params ) ) ){
				continue;
			}

			while( $check_query_result_read = mysqli_fetch_assoc( $check_query_result ) ){
				$check_query_result_read['gratuity'] = 0;
				$gratutity_totals[] = $check_query_result_read;
				$sum_totals += $check_query_result_read['total'];
			}

			$consumed_gratuity = round($query_order['gratuity'],3);
			foreach( $gratutity_totals as $key => $check_totals ){
				$gratutity_totals[ $key ]['weight'] = $gratutity_totals[ $key ]['total'] / $sum_totals;
				$check_query_result_read['gratuity'] = round($gratutity_totals[ $key ]['weight'] * $query_order['gratuity'],3);
				$consumed_gratuity -= $check_query_result_read['gratuity'];
			}

			$it = 0;
			while( $consumed_gratuity > 0 ){
				$check_query_result_read['gratuity'] += min( 0.001, $consumed_gratuity );
				$consumed_gratuity  -= min( 0.001, $consumed_gratuity );
				$it = ($it + 1) % count( $gratutity_totals );
			}

			foreach( $gratutity_totals as $key => $check_totals ){
				lavu_query( "UPDATE `split_check_details` SET `gratuity`='[gratuity]' WHERE `id`='[id]'", $check_totals ); 
			}
		}
	}

	function process_fix_auto_gratuity_breakout($date_reports_by, $date_start, $date_end, $locationid, $order_id='', $special_query ='' ){
		if(trim($special_query)!="")
			$where_cond = "($special_query)";
		else if($order_id!="")
			$where_cond = "(`orders`.`order_id` = '[order_id]' and `location_id`='[location_id]')";
		else
			$where_cond = "(`[date_reports_by]`>'[date_start]' and `[date_reports_by]`<'[date_end]' and location_id='[location_id]')";

		$query_params = array(
			'date_reports_by' => $date_reports_by,
			'date_start' => $date_start,
			'date_end' => $date_end,
			'location_id' => $locationid,
			'order_id' => $order_id
		);

		$query = "SELECT `id`, `order_id`, `location_id`, `gratuity`, `ag_card`, `ag_cash`, `ag_other` FROM `orders` WHERE {$where_cond} AND `gratuity` != (`ag_card` + `ag_cash` + `ag_other`)";

		if( !($query_result = lavu_query( $query, $query_params )) ){
			return;
		}

		if( !mysqli_num_rows( $query_result ) ){
			return;
		}
		while( $query_result_order = mysqli_fetch_assoc( $query_result ) ){
			$ag_cash = $ag_card = $ag_other = 0;
			if( !($split_check_query_result = lavu_query("SELECT `gratuity`, `cash_applied` as `cash`, `card`, `alt_paid` + `gift_certificate` as `alt`, `cash_applied` + `card` + `alt_paid` + `gift_certificate` as `total` FROM `split_check_details` WHERE `order_id`='[order_id]' AND `loc_id`='[location_id]'", $query_result_order)) ){
				continue;
			}

			while( $split_check_query_check = mysqli_fetch_assoc( $split_check_query_result ) ){
				$gratuity = round($split_check_query_check['gratuity'], 3);
				
				$amounts_paid = array();
				$amounts_paid[] = round($split_check_query_check['cash'], 3);
				$amounts_paid[] = round($split_check_query_check['card'], 3);
				$amounts_paid[] = round($split_check_query_check['alt'], 3);

				$gratuity_applied = array();
				$gratuity_applied[] = $gratuity * round( $cash / $split_check_query_check['total'], 3 );
				$gratuity_applied[] = $gratuity * round( $card / $split_check_query_check['total'], 3 );
				$gratuity_applied[] = $gratuity * round( $alt / $split_check_query_check['total'], 3 );

				$amounts_paid[0] -= $gratuity_applied[0];
				$amounts_paid[1] -= $gratuity_applied[1];
				$amounts_paid[2] -= $gratuity_applied[2];

				$gratuity = $gratuity - ( $gratuity_applied[0] + $gratuity_applied[1] + $gratuity_applied[2] );

				$it = 0;
				while( $gratuity >= 0.001 && ( $amounts_paid[0] != 0 && $amounts_paid[1] != 0 && $amounts_paid[2] != 0 ) ){
					$amt = min( min( $gratuity, 0.001 ), max( 0,$amounts_paid[$it]) );
					$gratuity_applied[$it] += $amt;
					$amounts_paid[$it] -= $amt;
					$gratuity -= $amt;

					$it = ($it + 1)%count( $gratuity_applied );
				}

				$ag_cash += $gratuity_applied[0];
				$ag_card += $gratuity_applied[1];
				$ag_other += $gratuity_applied[2];
			}

			$query_result_order['ag_cash'] = $ag_cash;
			$query_result_order['ag_card'] = $ag_card;
			$query_result_order['ag_other'] = $ag_other;
			lavu_query("UPDATE `orders` SET `ag_cash` = '[ag_cash]', `ag_card` = '[ag_card]', `ag_other` = '[ag_other]' WHERE `id` = '[id]'", $query_result_order);
		}
	}

	function process_fix_auto_gratuity($date_reports_by,$date_start,$date_end,$locationid,$order_id="",$special_query="")
	{
		if(trim($special_query)!="")
			$where_cond = "($special_query)";
		else if($order_id!="")
			$where_cond = "(`order_id` = '[5]' and `location_id`='[4]')";
		else
			$where_cond = "(`[1]`>'[2]' and `[1]`<'[3]' and location_id='[4]')";
			
		$query = "select order_id,subtotal,discount,tax,gratuity,rounding_amount,format((subtotal - discount + tax + gratuity + rounding_amount),2) as `added`,total,opened,closed,cash_paid,cash_applied,card_paid from orders where $where_cond and format((subtotal - discount + tax + gratuity + rounding_amount),2)!=format(`total`,2) and void < 1";
		
		$order_query = lavu_query($query,$date_reports_by,$date_start,$date_end,$locationid,$order_id);
		while($order_query !== FALSE && $order_read = mysqli_fetch_assoc($order_query))
		{
			$amount_paid = $order_read['cash_applied'] + $order_read['card_paid'] + $order_read['gift_certificate'] + $order_read['alt_paid'];
			if(isset($_GET['tt'])) echo "FIX AUTO GRAT: " . $order_read['order_id'] . " - added: " . number_format($order_read['added'],2) . " current: ".number_format($order_read['total'],2) . " paid: ".number_format($amount_paid,2)."<br>";
			if(number_format($amount_paid * 1,2)==number_format($order_read['total'],2))
			{
				$diff = str_replace(",","",number_format(($order_read['total'] * 1) - (($order_read['subtotal'] - $order_read['discount'] + $order_read['tax'] + $order_read['rounding_amount']) * 1),2));
				if($diff * 1 >= 0)
				{
					lavu_query("update `orders` set `gratuity`='[1]',`ag_cash`='',`ag_card`='',`ag_other`='' where `order_id`='[2]' and `location_id`='[3]' limit 2",$diff,$order_read['order_id'],$locationid);
				}
				else if($diff * 1 < 0 && ($order_read['discount'] * 1 == 0 || $order_read['discount']=="")) // If difference is negative make it a discount instead
				{
					lavu_query("update `orders` set `discount`='[1]' where `order_id`='[2]'",abs($diff),$order_read['order_id']); 
				}
				//echo "Paid: $amount_paid Total: " . $order_read['total'] . " Diff: ".$diff."<br>";
			}
			else if(number_format(abs($amount_paid * 1 - $order_read['added']),2) < .08)
			{
				if(isset($_GET['tt'])) echo "Total for " . $order_read['order_id'] . " should be " . number_format($order_read['added'],2) . " (it is currently ".number_format($order_read['total'],2).")<br>";
				lavu_query("update `orders` set `total`='[1]' where `order_id`='[2]' and `location_id`='[3]' limit 2",$order_read['added'],$order_read['order_id'], $locationid);
			}
		}
	}

	function process_fix_itax($date_reports_by,$date_start,$date_end,$locationid,$order_id="",$special_query=""){
		$params = array(
			'loc_id' => $locationid,
			'date_reports_by' => $date_reports_by,
			'date_start' => $date_start,
			'date_end' => $date_end,
			'order_id' => $order_id
		);
		global $location_info;
		$no_of_decimals = $location_info['disable_decimal']*1;
		$where_cond = "";

		if(trim($special_query)!=""){
			$where_cond = "({$special_query})";
		} else if($order_id!=""){
			$where_cond = "(`orders`.`order_id` ='[order_id]' AND `order_contents`.`loc_id`='[loc_id]')";
		} else {
			$where_cond = "(`[date_reports_by]`>'[date_start]' AND `[date_reports_by]`<'[date_end]' AND `order_contents`.`loc_id`='[loc_id]')";
			
		}

		$query = "SELECT `order_contents`.* FROM `order_contents` LEFT JOIN `orders` ON `orders`.`order_id` = `order_contents`.`order_id` WHERE {$where_cond} AND `order_contents`.`itax`*1 != 0 AND ((`order_contents`.`itax_rate`*1 != (`order_contents`.`tax_rate1`*1 + `order_contents`.`tax_rate2`*1 + `order_contents`.`tax_rate3`*1)) OR (`order_contents`.`itax`*1 != (`order_contents`.`tax1`*1 + `order_contents`.`tax2`*1 + `order_contents`.`tax3`*1)))";

		$order_contents_query = lavu_query($query, $params);
		if($order_contents_query && mysqli_num_rows($order_contents_query) ){
			echo "iTax Mismatch<br />";
			while($order_contents_query !== FALSE && $order_contents_read = mysqli_fetch_assoc($order_contents_query) ){
				if($order_contents_read['tax_amount']*1 == 0){
					// Just update the order_contents tax rates/ tax dollars, and don't update the prices, or the actual tax field
					$itax_rate = $order_contents_read['tax_rate1'] + $order_contents_read['tax_rate2'] + $order_contents_read['tax_rate3'];
					$itax = round($itax_rate * $order_contents_read['after_discount'], $no_of_decimals );
					$itax_acc = $itax;
					$tax_percentage1 = $order_contents_read['tax_rate1'] / $itax_rate;
					$tax_percentage2 = $order_contents_read['tax_rate2'] / $itax_rate;
					$tax_percentage3 = $order_contents_read['tax_rate3'] / $itax_rate;
					$tax1 = round( $tax_percentage1 * $itax, $no_of_decimals );
					$itax_acc -= $tax1;
					$tax2 = round( $tax_percentage2 * $itax, $no_of_decimals );
					$itax_acc -= $tax2;
					$tax3 = round( $tax_percentage3 * $itax, $no_of_decimals );
					$itax_acc -= $tax3;

					if($itax_acc > 0){
						$itax1 += $itax_acc;
					}

					$order_content_id = $order_contents_read['id'];

					$tax_params = array(
						"tax1" => $tax1,
						"tax2" => $tax2,
						"tax3" => $tax3,
						"itax" => $itax,
						"itax_rate" => $itax_rate,
						"id" => $order_content_id
					);

					lavu_query("UPDATE `order_contents` SET `tax1`='[tax1]', `tax2`='[tax2]', `tax3`='[tax3]', `itax_rate`='[itax_rate]', `itax`='[itax]' WHERE `id`='[id]' LIMIT 1", $tax_params);

				} else {
					///TODO MAKE THIS FIX STUFF
				}
			}
		}
	}
	
	function test_manual_tax_calculation($date_reports_by,$date_start,$date_end,$locationid)
	{
		$total_tax = 0;
		$contents_tax_total = 0;
		echo "Test manual for: $date_start to $date_end - $date_reports_by - $locationid<br>";
		$order_query = lavu_query("select `order_id`,`tax` from `orders` where `location_id`='[4]' and `[1]`>='[2]' and `[1]`<='[3]' and void < 1 order by order_id asc",$date_reports_by,$date_start,$date_end,$locationid);
		if($order_query)
		{
			while($order_query !== FALSE && $order_read = mysqli_fetch_assoc($order_query))
			{
				//echo "tax for " . $order_read['order_id'] . ": " . $order_read['tax'] . "<br>";
				$total_tax += ($order_read['tax'] * 1);
				
				$contents_tax = 0;
				$contents_query = lavu_query("select `tax_amount` from `order_contents` where `order_id`='[1]' and `loc_id`='[3]'and `quantity`>0",$order_read['order_id'],$locationid);
				while($contents_query !== FALSE && $contents_read = mysqli_fetch_assoc($contents_query))
				{
					$contents_tax += $contents_read['tax_amount'] * 1;
				}
				
				$diff = abs(($contents_tax * 1) - ($order_read['tax'] * 1));
				if($diff >= smallestMoney())
				{
					echo "Tax mismatch: " . $order_read['order_id'] . " - $contents_tax - ". $order_read['tax'] . "<br>";
				}
				else if($diff > 0.001)
				{
					echo "Partial Tax mismatch: " . $order_read['order_id'] . " - $contents_tax - ". $order_read['tax'] . "<br>";
				}
				$contents_tax_total += $contents_tax;
			}
		}
		else
		{
			echo "Error: " . lavu_dberror();
		}
		echo "Total Tax: $total_tax<br>";
		echo "Contents Tax: $contents_tax_total<br>";
	}
	
	function process_fix_voided_contents_tax($date_reports_by,$date_start,$date_end,$locationid,$order_id="",$special_query="")
	{
		//if(isset($_GET['tt'])) test_manual_tax_calculation($date_reports_by,$date_start,$date_end,$locationid);
		
		if(trim($special_query)!="")
			$where_cond = "($special_query)";
		else if($order_id!="")
			$where_cond = "(`orders`.`order_id` = '[5]' and `orders`.`location_id`='[4]' and `order_contents`.`loc_id` = `orders`.`location_id`)";
		else
			$where_cond = "(`orders`.`[1]` >= '[2]' and `orders`.`[1]` < '[3]' and `orders`.`location_id`='[4]' and `order_contents`.`loc_id` = `orders`.`location_id`)";
		
		$mm_query = lavu_query("select orders.order_id, `orders`.`subtotal` AS `order_subtotal`, orders.tax as `order_tax`, `orders`.`itax` as `order_itax`, abs(orders.tax * 1 - sum(`order_contents`.`tax_amount`)) as `offset` from orders left join order_contents on orders.order_id=order_contents.order_id where $where_cond and `orders`.`void` < 1 and `order_contents`.`quantity` > 0 group by `orders`.`order_id` order by `offset` desc limit 20",$date_reports_by,$date_start,$date_end,$locationid,$order_id);
		// sum(order_contents.tax_amount) as `contents_tax`
		while($mm_query !== FALSE && $mm_read = mysqli_fetch_assoc($mm_query))
		{
			if($mm_read['offset'] >= smallestMoney())
			{
				fix_order_based_on_order_contents("tax mismatch",$mm_read['order_id'],$date_reports_by,$date_start,$date_end,$locationid,$mm_read);
			}
		}
		$mm_query = lavu_query("select orders.order_id, `orders`.`subtotal` AS `order_subtotal`, orders.tax as `order_tax`, `orders`.`itax` as `order_itax`, sum(`order_contents`.`quantity`) as `contents_qty` from orders left join order_contents on orders.order_id=order_contents.order_id where $where_cond and `orders`.`void` < 1 group by `orders`.`order_id` order by `contents_qty` asc limit 20",$date_reports_by,$date_start,$date_end,$locationid,$order_id);
		// sum(order_contents.tax_amount) as `contents_tax`, abs(orders.tax * 1 - sum(`order_contents`.`tax_amount`)) as `offset`
		while($mm_query !== FALSE && $mm_read = mysqli_fetch_assoc($mm_query))
		{
			if($mm_read['contents_qty'] < 1)
			{
				fix_order_based_on_order_contents("tax mismatch",$mm_read['order_id'],$date_reports_by,$date_start,$date_end,$locationid,$mm_read);
			}
		}
	}
	
	function process_fix_subtotal_mismatches($date_reports_by,$date_start,$date_end,$locationid,$order_id="",$special_query="")
	{
		if(trim($special_query)!="")
			$where_cond = "($special_query)";
		else if($order_id!="")
			$where_cond = "(order_id = '[3]' AND `loc_id`='[4]')";
		else
			$where_cond = "(server_time >= '[1]' and server_time <= '[2]')";
		
		$success = lavu_query("update `order_contents` set `subtotal_with_mods`=(`quantity` * (`price` + `forced_modifiers_price` + `modify_price`) - IF(`tax_exempt` = '1',`itax`,0)) where void < 1 and $where_cond and `item`!='SENDPOINT' and ((`quantity` * (`price` + `forced_modifiers_price` + `modify_price`) - IF(`tax_exempt` = '1',`itax`,0)) != `subtotal_with_mods`) limit 2000",$date_start,$date_end,$order_id, $locationid);
	}
	
	function fix_blank_taxes_for_order_contents($date_reports_by,$date_start,$date_end,$locationid,$order_id="",$special_query="")
	{
		if(trim($special_query)!="")
			$where_cond = "($special_query)";
		else if($order_id!="")
			$where_cond = "(`orders`.`order_id` = '[5]' AND `location_id`='[4]')";
		else
			$where_cond = "(`orders`.`[1]`>='[2]' and `orders`.`[1]`<='[3]' and `orders`.`location_id`='[4]')";
			
		$content_query = lavu_query("select `order_contents`.`id` as `content_id` from `orders` left join `order_contents` on `orders`.`order_id`=`order_contents`.`order_id` where `order_contents`.`apply_taxrate`!='' and `order_contents`.`tax_amount`='' and $where_cond",$date_reports_by,$date_start,$date_end,$locationid,$order_id);
		while($content_query !== FALSE && $content_read = mysqli_fetch_assoc($content_query))
		{
			lavu_query("update `order_contents` set `tax_amount`=(`quantity` * (`price` + `forced_modifiers_price` + `modify_price`) * `apply_taxrate`) where `id`='[1]'",$content_read['content_id']);
		}
	}
	
	function useTaxAmount($contents_tax, $order_tax, $loc_id, $order_id) {
	
		global $location_info;
	
		$use_tax_amount = $order_tax;		
		if ($location_info['disable_check_tax_recalc'] == "1") $use_tax_amount = $contents_tax;
		else {
			// the sum of the contents' tax amounts is inaccurate, so let's add/subtract a penny here and there
			$diff = round(($order_tax - $contents_tax), (int)$location_info['disable_decimal']);
			$adj = 0;
			$sm = smallestMoney();
			$smt = ($sm / 10);
			if ($diff >= $sm) $adj = $sm;
			else if ($diff <= ($sm * -1)) $adj = ($sm * -1);
			if ($adj != 0) {
				$taxNum = 1;
				$get_contents = lavu_query("SELECT `id`, `tax_amount`, `tax1`, `tax2`, `tax3`, `tax4`, `tax5`, `total_with_tax` FROM `order_contents` WHERE `loc_id` = '[1]' AND `order_id` = '[2]' AND `quantity` > 0 AND `item` != 'SENDPOINT' AND `tax_amount` > 0 ORDER BY `tax_amount` DESC", $loc_id, $order_id);
				$rcount = mysqli_num_rows($get_contents);
				if ($rcount > 0) {
					$iterations = 0;
					while ((abs($diff) >= $sm) && ($iterations < 500)) {
						$item = mysqli_fetch_assoc($get_contents);
						if ($item['tax'.$taxNum] >= $sm) {
							$item['tax_amount'] += $adj;
							$item['tax'.$taxNum] += $adj;
							$item['total_with_tax'] += $adj;
							$update_item = lavu_query("UPDATE `order_contents` SET `tax_amount` = '[tax_amount]', `tax".$taxNum."` = '[tax".$taxNum."]', `total_with_tax` = '[total_with_tax]' WHERE `id` = '[id]'", $item);
							$diff -= $adj;
						}
						$iterations++;
						if (($iterations % $rcount) == 0) {
							mysqli_data_seek($get_contents, 0);
							$taxNum++;;
							if ($taxNum > 5) $taxNum = 1;
						}
					}
				}
			}
		}
		
		return $use_tax_amount;	
	}
	
	function fix_order_based_on_order_contents($type,$order_id,$date_reports_by,$date_start,$date_end,$locationid,$mm_read)
	{	
	
		$content_query = lavu_query("select sum(`tax_amount` + `itax`) as `tax`,sum(`tax_amount`) as `tax_amount`,sum(`itax`) as `itax`,sum((`price` * `quantity`) - `itax`) as `item_total`, sum((`modify_price` + `forced_modifiers_price`) * `quantity`) as `modifiers`, sum(`idiscount_amount`) as `idiscount`, `discount_amount` as `check_discount`, sum(`after_discount`) as `after_discount`, sum(`subtotal_with_mods` - `idiscount_amount`) as `subtotal`, sum(`after_discount` + `discount_amount` - `itax`) as `subtotal2`, sum(`subtotal_with_mods` - `discount_amount` + `tax_amount` - `idiscount_amount`) as `total` from `order_contents` where `order_id`='[1]' and `loc_id`='[2]' and `quantity` > 0",$order_id, $locationid);
		if($content_query !== FALSE && mysqli_num_rows($content_query))
		{
			$exempt_read['itax'] = 0;
			$exempt_query = lavu_query("SELECT SUM(`itax`) AS `itax` FROM `order_contents` WHERE `tax_exempt` = '1' AND `order_id`='[1]' AND `loc_id`='[2]' AND `quantity` > 0", $order_id, $locationid);
			if ($exempt_query !== FALSE && mysqli_num_rows($exempt_query)) {
				$exempt_read = mysqli_fetch_assoc($exempt_query);
			}
		
			$content_read = mysqli_fetch_assoc($content_query);
			$contents_tax = $content_read['tax'];
			$contents_item_total = $content_read['item_total'];
			$contents_modifiers = $content_read['modifiers'];
			$contents_idiscount = $content_read['idiscount'];
			$contents_subtotal = numformat($content_read['subtotal']);
			$contents_check_discount = $content_read['check_discount'];
			$contents_after_discount = $content_read['after_discount'];
			$use_tax_amount = useTaxAmount($content_read['tax_amount'], $mm_read['order_tax'], $locationid, $order_id);
			$contents_itax = ($content_read['itax'] - $exempt_read['itax']);
			$contents_total = ($content_read['total'] + ($use_tax_amount - $content_read['tax_amount']));

			if($contents_subtotal=="") $contents_subtotal = 0;
			if($contents_total=="") $contents_total = 0;
			if($use_tax_amount=="") $use_tax_amount = 0;
			if($contents_itax=="") $contents_itax = 0;		
		}
		else
		{
			$contents_subtotal = 0;
			$contents_total = 0;
			$use_tax_amount = 0;
			$contents_itax = 0;
		}
		
		//echo "MM Content Mismatch: contents:$contents_subtotal = order:$subtotal $extra_order_info<br>";
		$vars = array();
		$vars['order_id'] = $order_id;
		$vars['subtotal'] = $contents_subtotal;
		$vars['total'] = $contents_total;
		$vars['tax'] = $use_tax_amount;
		$vars['itax'] = $contents_itax;
		$vars['location_id'] = $locationid;
		$query = "update `orders` set `subtotal`='[subtotal]', `tax`='[tax]', `itax`='[itax]', `total`='[total]' where `order_id`='[order_id]' AND `location_id`='[location_id]' limit 2";
				
		//if(isset($_GET['tt'])) echo "Checking Order: $order_id - $use_tax_amount - ".$mm_read['order_tax']."<br>";
		if(abs($mm_read['order_tax'] - $use_tax_amount) > 0.005 || abs($mm_read['order_subtotal'] - $contents_subtotal) > smallestMoney() || abs($mm_read['order_itax'] - $contents_itax) > smallestMoney())
		{
			$testing = false;
			if($testing)
			{
				if(isset($_GET['tt']))
				{
					echo "<hr><b>$type</b><br>";
					$display_query = $query;
					foreach($vars as $key => $val) 
					{
						$display_query = str_replace("[".$key."]",ConnectionHub::getConn('rest')->escapeString($val),$display_query);
						echo $key . " = " . $order_read[$key] . "<br>";
					}
					echo $display_query . "<br><br>";
				}
			}
			else if($vars['order_id']!="")
			{
				//echo "Running query: " . $query . "<br>";
				$success = lavu_query($query,$vars);
			}
		}
	}
	
	function process_fix_order_subtotal_mismatches($date_reports_by,$date_start,$date_end,$locationid,$order_id="",$special_query="")
	{
		if(trim($special_query)!="")
			$where_cond = "($special_query)";
		else if($order_id!="")
			$where_cond = "(`orders`.`order_id` = '[5]' and `orders`.`location_id`='[4]' and `orders`.`location_id` = `order_contents`.`loc_id`)";
		else
			$where_cond = "(`orders`.`[1]` >= '[2]' and `orders`.`[1]` < '[3]' and `orders`.`location_id`='[4]' and `orders`.`location_id` = `order_contents`.`loc_id`)";
			
		$mm_query = lavu_query("select orders.order_id as `order_id`, `orders`.`subtotal` as `order_subtotal`, `orders`.`tax` as `order_tax`, `orders`.`itax` AS `order_itax`, sum(`order_contents`.`tax_amount`) as `contents_tax`, abs(orders.subtotal * 1 - sum((order_contents.price + order_contents.forced_modifiers_price + order_contents.modify_price) * order_contents.quantity - order_contents.idiscount_amount - order_contents.itax)) as `offset`, abs(orders.tax * 1 - sum(order_contents.tax_amount)) as `tax_offset` from orders left join order_contents on orders.order_id=order_contents.order_id where $where_cond and `orders`.`void` < 1 group by `orders`.`order_id` order by `offset` desc, `tax_offset` desc limit 20",$date_reports_by,$date_start,$date_end,$locationid,$order_id);
		// sum((order_contents.price + order_contents.forced_modifiers_price + order_contents.modify_price) * order_contents.quantity - order_contents.idiscount_amount - order_contents.itax) as `contents_subtotal` 
		while($mm_query !== FALSE && $mm_read = mysqli_fetch_assoc($mm_query))
		{
			if($mm_read['offset'] >= smallestMoney())
			{
				//echo $mm_read['order_id'] . ": " . $mm_read['order_subtotal'] . "/" . $mm_read['contents_subtotal'] . "<br>";
				//$subtotal = $mm_read['order_subtotal'];
				
				fix_order_based_on_order_contents("subtotal mismatch",$mm_read['order_id'],$date_reports_by,$date_start,$date_end,$locationid,$mm_read);
			}
			if($mm_read['tax_offset'] >= 0.005)
			{
				if(isset($_GET['tt'])) echo "Found tax mismatch for order " . $mm_read['order_id'] . ": " . number_format($mm_read['tax_offset'],2) . " ".number_format($mm_read['order_tax'],2)."/".number_format($mm_read['contents_tax'],2)."<br>";
				fix_order_based_on_order_contents("tax mismatch",$mm_read['order_id'],$date_reports_by,$date_start,$date_end,$locationid,$mm_read);
			}
		}
	}
	
	function process_fill_alt_transaction_totals($locationid, $order_id) {
	
		$tx_query = lavu_query("SELECT `check`, `total_collected`, `tip_amount`, `action`, `pay_type`, `pay_type_id`, `ioid` FROM `cc_transactions` WHERE `loc_id` = '[1]' AND `order_id` = '[2]' AND `pay_type_id` >= '4' AND `voided` = '0'", $locationid, $order_id);
		if (mysqli_num_rows($tx_query) > 0) {
			$payment_info = array();
			$ioid = "";
			while ($tx_read = mysqli_fetch_assoc($tx_query)) {
				$k = $tx_read['check']."_".$tx_read['pay_type_id'];
				if (!isset($payment_info[$k])) $payment_info[$k] = array("loc_id"=>$locationid, "order_id"=>$order_id, "ioid"=>$tx_read['ioid'], "check"=>$tx_read['check'], "type"=>$tx_read['pay_type'], "pay_type_id"=>$tx_read['pay_type_id'], "amount"=>0, "tip"=>0, "refund_amount"=>0);
				$a = $tx_read['action'];
				$tc = (float)$tx_read['total_collected'];
				if ($a == "Sale") {
					$payment_info[$k]['amount'] += $tc;
					$payment_info[$k]['tip'] += (float)$tx_read['tip_amount'];
				} else if ($a == "Refund") $payment_info[$k]['refund_amount'] += $tc;
			}
			
			$clear_previous = lavu_query("DELETE FROM `alternate_payment_totals` WHERE `loc_id` = '[1]' AND `order_id` = '[2]'", $locationid, $order_id);
			
			foreach ($payment_info as $info) {
				$q_fields = "";
				$q_values = "";
				foreach ($info as $key => $val) {
					if ($q_fields != "") { $q_fields .= ", "; $q_values .= ", "; }
					$q_fields .= "`$key`";
					$q_values .= "'[$key]'";
				}			
				$insert = lavu_query("INSERT INTO `alternate_payment_totals` ($q_fields) VALUES ($q_values)", $info);
			}
		}
	}
	
	function process_ag_data($date_reports_by,$date_start,$date_end,$locationid)
	{
		global $location_info;
		if( (!isset($location_info['ag_calculation_scheme'])) || $location_info['ag_calculation_scheme'] == '1' ){
			$ag_query = lavu_query("select `id`,`order_id`,`cash_applied`,`gratuity`,`card_paid`,`ag_cash`,`ag_card` from `orders` where `[1]` >= '[2]' and `[1]` < '[3]' and `location_id`='[4]' and `gratuity` > 0 and `gratuity`!='' and ((`cash_applied` > 0 and `ag_cash`='') or (`card_paid` > 0 and `ag_card`=''))",$date_reports_by,$date_start,$date_end,$locationid);
			while($ag_query !== FALSE && $ag_read = mysqli_fetch_assoc($ag_query)) {
				$cash_applied = $ag_read['cash_applied'];
				$use_ag = $ag_read['gratuity'];
				$use_cash_paid = ($cash_applied);
				$use_card_paid = $ag_read['card_paid'];
				$ag_cash = 0;
				$ag_card = 0;
				if($use_cash_paid >= $use_ag)
				{
					$ag_cash = $use_ag;
				}
				else
				{
					$ag_cash = $use_cash_paid;
					$use_ag -= $ag_cash * 1;
					if($use_card_paid >= $use_ag)
					{
						$ag_card = $use_ag;
					}
					else
					{
						$ag_card = $use_card_paid;
						$use_ag -= $ag_card * 1;
					}
				}
				$success = lavu_query("update `orders` set `ag_cash`='[1]', `ag_card`='[2]' where `id`='[3]'",$ag_cash,$ag_card,$ag_read['id']);
				if(isset($_GET['view_ag_process']))
				{
					if($success) echo "Updated ag_cash to $ag_cash and ag_card to $ag_card<br>";
					else echo "Unable to update: ".$ag_read['order_id']."<br>";
				}
			}
			if(isset($_GET['view_ag_process'])) echo "To Process: " . mysqli_num_rows($ag_query) . " ($date_reports_by $date_start $date_end $locationid)<br>";
		} else {
			process_fix_auto_gratuity($date_reports_by,$date_start,$date_end,$locationid);
			process_fix_auto_gratuity_split_checks($date_reports_by,$date_start,$date_end,$locationid);
			process_fix_auto_gratuity_breakout($date_reports_by,$date_start,$date_end,$locationid);
			// process_fix_auto_gratuity($date_reports_by,$date_start,$date_end,$locationid,$order_id,$special_query);
		}
	}
	
	function smallestMoney() {
	
		global $location_info;
		
		$sm = 1;
		$dp = (isset($location_info['disable_decimal']))?$location_info['disable_decimal']:2;
		if ($dp > 0) $sm = (1 / pow(10, $dp));
		
		return $sm;
	}
	
	if (!function_exists("numformat")) {
		
		function numformat($num) {
		
			global $decimal_places;
			
			if (!isset($decimal_places)) $decimal_places = 2;

			return number_format($num, $decimal_places, ".", "");
		}
	}
	
?>
