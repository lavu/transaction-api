<?php

class MemcachedRestaurantDBConnector extends RestaurantDBConnector {
	
	/* lqCredentialsFromMemcached() tries to get restaurant credentials from memcached, and
	   if not found, will retrieve them from the database but cache them in memcached for 
	   later retrieval. This is the preferred (faster) method for getting DB connection
	   credentials for a restaurant database. */

	private function initializeCredentialsFromMemcached($dn) {
		$t1 = microtime($get_as_float=true);
		$memc_conn = ConnectionHub::getConn('memcached')->getLink();

		$memc_key = "lavuquery_" . $dn;

		$lq_creds = $memc_conn->get($memc_key);
		$conn_code = $memc_conn->getResultCode();

		if ($conn_code == Memcached::RES_NOTFOUND) {
			// not found in memcached, so we want to store:
			$memc_conn->set($this->initializeCredentialsFromDB($dataname=$dn));
		} else {
			$t2 = microtime($get_as_float=true);
			error_log("initializeCredentialsfromMemcached: " . strval($t2-$t1) . " usec.");
		}
		return $lq_creds;
	}
}

class MemcachedConnector {

	public function __construct() {
		$this->conn = null;
	}

	public function getLink() {
		if ($this->conn !== null)
			return $this->conn;
		$this->conn = $this->connectMemcached();
		return $this->conn;
	}

	public function connectMemcached() {
		return my_memcached();
	}

	public function close() {
		/* TODO */
	}
}

