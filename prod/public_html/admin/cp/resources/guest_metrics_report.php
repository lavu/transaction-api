<?php
	error_reporting(error_reporting() & ~E_NOTICE & ~E_DEPRECATED & ~E_WARNING);
	ini_set("display_errors", 1);
	session_start();
	require_once(dirname(__FILE__) . '/core_functions.php');
	require_once(dirname(__FILE__) . '/backend_emergency_notification.php');
	require_once(dirname(__FILE__) . '/../../lib/modules.php');

	/* Config */

	//$globalPath = "/mnt/poslavu-files/GMFiles/";
	$globalPath = "/Users/tom/Desktop/GuestMetricsFiles/";
	$in_lavu = 1;
	$modules = null;
	$location_info = array();
	$data_name = "";
	$time_cards_results = null;
	$key ='your_mom';


	/* Command line stuff */
	/* 
	if(isset($argv[1]) && $argv[1]!='fish'){
		return "Error:proper params not sent";
		//startFile($db, $argv[1]);
	}else if(isset($_GET['date'])){
		startFile($_GET['date']);
	
	}else{
		return "Error: no arguments";
	}*/

	/* Globals */

	$itemRowCounts = array();
	$taxCodeNames = array();
	$discountCodes = array();
	$voidReasonCodes = array(0 => '(None)');
	$serviceChargeCodes = array(0 => '(None)');

	/* Main Entry Point */

	echo "<pre>";
	start_file("2014-06-26");
	//$date = "2014-05-28";


	/* Functions */
	
	function start_file($date){
		$query = mlavu_query("select `data_name`, `id`,`company_name` FROM `poslavu_MAIN_db`.`restaurants` where `dev`='1' AND `demo`='0' AND `notes` NOT LIKE '%AUTO-DELETED%' AND `notes` NOT LIKE '%vf_skipped%' and `app_name` NOT LIKE 'POSLavu Lite' and `data_name`='utterly_delici'");
		while( $restaurants_res = mysqli_fetch_assoc($query)){
			//echo print_r($restaurants_res,1);
			lavu_connect_dn($restaurants_res['data_name'], "poslavu_".$restaurants_res['data_name']."_db");
			$query = mlavu_query("SELECT * FROM `poslavu_MAIN_db`.`guest_metrics` where `restaurant_id`='[1]'", $restaurants_res['id']);
			if( !mysqli_num_rows($query) ){
				guest_metrics_insert($restaurants_res['id'], $restaurants_res['data_name']);
			}else{
				$guest_metrics_info = mysqli_fetch_assoc($query);
				$restaurants_res['location_id']= $guest_metrics_info['location_id'];
			}

			echo "dataname is:". $restaurants_res['data_name'];
			print_r($restaurants_res);  //debug

			$loc_data 	    					   = mysqli_fetch_assoc(lavu_query("SELECT * FROM `locations` WHERE `id`='[2]'", $data_name, $restaurants_res['location_id']));
			$ret_arr 							   = array();
			$ret_arr['SNAP_TransactionDetailType'] = get_transaction_detail_type();
			$ret_arr['SNAP_TransactionDetail']     = get_transaction_details($date, $loc_data, $discountCodes);  // We need to run get_transaction_details() before we run get_discounts(). 
			$ret_arr['SNAP_Transaction']   	   	   = get_transactions($date, $loc_data);
			$ret_arr['SNAP_Discount']              = get_discounts();
			$ret_arr['SNAP_DiscountType']  	       = get_discount_types($data_name);
			$ret_arr['SNAP_Employee']      	       = get_employees($data_name, $restaurants_res['location_id']);
			$ret_arr['SNAP_EmployeeTime']  	       = get_employee_time($restaurants_res['data_name'],$restaurants_res['company_name'], $restaurants_res['location_id'], $restaurants_res['id'], $date, $loc_data);
			$ret_arr['SNAP_Item']          	       = get_items($loc_data);
			$ret_arr['SNAP_ItemMajorCategory']     = get_major_cat_code($loc_data);
			$ret_arr['SNAP_ItemMinorCategory']     = get_minor_cat_code($loc_data);
			$ret_arr['SNAP_LaborCategory'] 	       = get_labor_category();
			$ret_arr['SNAP_OrderType']             = get_order_type($loc_data);
			$ret_arr['SNAP_Payment']               = get_payment( $restaurants_res['location_id'], $date);
			$ret_arr['SNAP_PaymentType']  	       = get_payment_types($restaurants_res['location_id']);
			$ret_arr['SNAP_ServiceCharge'] 	       = get_service_charge($loc_data);
			$ret_arr['SNAP_Tax']   			   	   = get_taxes($loc_data);
			$ret_arr['SNAP_VoidReason']   		   = get_void_reasons($date, $loc_data);
			echo 'DEBUG: About to write_files()...<br>';  //debug
			write_files($ret_arr, $restaurants_res['data_name'],$date );
			//echo "<pre>".print_r($ret_arr,1);
		}

	}
	function obscure_data($value) {
		return base_convert(crc32($value), 16, 10);
	}
	function csv_format($arr) {

	}

	function guest_metrics_insert($id, $data_name){
		global $debugging;
		
		$locations_query = lavu_query("SELECT * FROM `locations`", $data_name);
		if( lavu_dberror()){
			echo "Error getting location_data. The dataname was: ". $data_name. " the ID was: ".$id."<br>"; 
			return; 
		}
		while($locations_res = mysqli_fetch_assoc($locations_query)){
			$config_query    = lavu_query("SELECT * FROM `config` WHERE `location`='[1]' AND `setting` = 'guest_metrics_send_time'",$data_name);
			$send_time       = '';
			if(mysqli_num_rows($config_query)){
				$conf_res  = mysqli_fetch_assoc($config_query);
				$send_time = $conf_res['value'];
			}else{
				$send_time = $locations_res['day_start_end_time'];
			}
			if(mysqli_num_rows(mlavu_query("SELECT * FROM `poslavu_MAIN_db`.`guest_metrics`")) < 10000){
				mlavu_query("INSERT INTO `poslavu_MAIN_db`.`guest_metrics` (`restaurant_id`,`send_time`,`active`,`location_id`) VALUES ('[1]','[2]','1','[3]')", $id, $send_time, $locations_res['id']);
			}
			else
				error_log("out of provisioned id's");
		}
	}
	function get_cost_center_data(){
		$ret_arr = array('"CostCenterCode","CostCenterName"');
		$ret_arr[] = '"-1","(Unspecified)"';
		$query = lavu_query("SELECT * FROM `revenue_centers`");
		while($res = mysqli_fetch_assoc($query)){
			$ret_arr[]= '"'.$res['id'].'"'.',"'.$res['name'].'"';
		}	
		return implode("\r\n", $ret_arr);
	}
	function get_discounts(){
		global $discountCodes;
		$ret_arr = array('"DiscountCode","DiscountName","DiscountTypeId","AlternateCode"');
		echo "<br>discountCodes:<br>". print_r($discountCodes, true) ."<br>";  //debug
		ksort($discountCodes);
		foreach ( $discountCodes as $discount_code => $val ) {
			$query = lavu_query("SELECT * FROM `discount_types` WHERE `id`='[1]'", $discount_code);
			$res = mysqli_fetch_assoc($query);
			if ($res === FALSE) die("Error with query:". lavu_dberror()); 
			$ret_arr[] = '"'.$res['id'].'","'.$res['title'].'","1",""';
		}
		return implode("\r\n", $ret_arr);
	}
	function get_discount_types($loc_data){
		$ret_arr   = array('"DiscountTypeId","DiscountTypeName"');
		$ret_arr[] = '"1","Comp Discounts"';
		$ret_arr[] = '"2","Promotion Discounts"';
		return implode("\r\n", $ret_arr);
	}
	function get_employees($loc_data, $id){
		$ret_arr= array('"EmployeeCode","FirstName","LastName"');
		//$ret_arr= array();
		$query= lavu_query("SELECT * FROM `users` WHERE `_deleted`='0' AND (`loc_id`='0' OR `loc_id`='[1]')", $loc_data['id']);
		while($res = mysqli_fetch_assoc($query)){
			$ret_arr[]= '"'.$res['id'].'","'.$res['f_name'].'","'.$res['l_name'].'"';
		}
		return implode("\r\n", $ret_arr);
	}
	function get_employee_time($dn,$company_name, $loc_id, $id, $date, $loc_data){
		global $in_lavu;
		global $modules;
		global $location_info;
		global $data_name;
		$data_name 				= $dn;
		$_GET['user']           = 'all';
		$_GET['setdate']        = $date;
		$_GET['day_duration']   = 1;
		$_GET['nialls_testing'] = 1;
		
		admin_login($data_name,1000,"poslavu_admin_guest_metrics",$company_name,"",$id, 1, 0, 3, $company_name, "", $loc_id, 0);
		
		$location_info = mysqli_fetch_assoc(lavu_query("SELECT * FROM `locations` WHERE `id`='[1]' limit 1 ", $loc_id));

		$conf_query = lavu_query("select * from `config` where `type`= 'overtime_setting_day2' and `location`='[1]'", $loc_id);
		if(!mysqli_num_rows($conf_query)){
			lavu_query("INSERT INTO `config` (`location`, `setting`, `value`, `value2`, `value3`, `value4`, `value_long`, `type`, `value5`, `value6`, `value7`, `value8`, `_deleted`, `value9`, `value10`, `value11`, `value12`, `value13`, `value14`, `value_long2`) VALUES ('[1]', '', 'DNA', '0', 'DNA', 'DNA', '', 'overtime_setting_day2', 'DNA', 'daystartend', '', '', 0, '', '', '', '', '', '', '');", $loc_id);
		}else{
			$location_info['overtime_setting_day2'] = mysqli_fetch_assoc($conf_query);
		}
		$_SESSION['poslavu_234347273_locationid']= $location_info['id'];
		//$location_info['modules']='';
		//$_SESSION['poslavu_234347273_location_info']= $location_info;
		//$_SESSION['poslavu_234347273_modules']= '+testing,chains.+*,components.+pizza,components.+tithe,dining.+*,dining.+revenuecenters,financial.discount.+*,financial.discount.+groups,payments. autobatching,reports. *';
		//echo"<pre>". print_r($_SESSION,1);
		$modules  = getModules($data_name, $id, $loc_data);
		ob_start();
		require_once(dirname(__FILE__).'/../areas/reports/time_cards_obj.php');
		$s_contents = ob_get_contents();
		ob_end_clean();
		//echo print_r($s_contents,1);
		global $time_cards_results;
		$ret_arr  = array('"EmployeeTimeId", "BusinessDate","EmployeeCode","LaborCategoryCode","PayRateRegular","PayRateOT","ClockIn","ClockOut","PayTotal","PayRegular","PayOT","HoursTotal","HoursRegular","HoursOT"');
		$counter  = 1;
		//echo "<pre>the results are: ". print_r($time_cards_results,1);
		
		if($time_cards_results){
			foreach( $time_cards_results['users'] as $user_id => $user ){
			
			//id
			//work_weeks
			//processed
			//userfound
			//username
			//full_name
			//access
			//locationid
			//lavu_admin
			//deleted
			//classes
			//payrates
			//clocked_in
			//timePunches

			// echo '<pre style="text-align: left;">' . print_r( $user, true ) . '</pre>';
			$payrates = $user->payrates;
			$classes = $user->classes;
			$full_name = $user->full_name;
			$user_id = $user->id;
			$punches = array();

			//week
			//next_week
			//work_days
			//seconds
			//days
			//processed
			//time_punches
			foreach($user->timePunches as $punch_id => $punch ){
				//id
				//locationid
				//userid
				//time_in
				//time_out
				//clocked_out
				//clocked_out_via_app
				//delete
				//rate_id
				//in_requested_time_period
				//role_id
				//start_week
				//end_week
				//contribution
				$punches[$punch->id] = $punch;
			}
		
			foreach( $punches as $punch_id => $punch ){

				$report_start_time = TimeCardAccountSettings::settings()->start_time;
				$report_end_time = TimeCardAccountSettings::settings()->end_time;

				if( !(($punch->time_out >= $report_start_time && $punch->time_out <= $report_end_time) ||
					($punch->time_in >= $report_start_time && $punch->time_in <= $report_end_time) ||
					($punch->time_in <= $report_start_time && $punch->time_out >= $report_end_time)) || 
					!$punch->in_requested_time_period ){
					continue;
				}

				$role = $classes->getRole( $punch->role_id );
				$row_result = array(
					$user_id,
					$full_name,
					$classes->roles[$punch->role_id],
					$punch->time_in,//'m/d h:i A'
					($punch->clocked_out?'':'e') . $punch->time_out
				);
				$regular_seconds = 0;
				$regular_rate = 0;
				$overtime_rate = 0;
				$doubletime_rate = 0;

				foreach( $punch->contribution[REGULAR] as $role_id => $seconds ){
					$regular_seconds += $seconds;
					if( $punch->payrate ){
						$regular_rate = $punch->payrate / 3600.0;
					} else if( $role_id || $role_id == 0 ){
						$regular_rate = $payrates->get_rate_rate( $role_id ) / 3600.0;
					}
				}

				$overtime_seconds = 0;
				foreach( $punch->contribution[OVERTIME] as $role_id => $seconds ){
					$overtime_seconds += $seconds;
					if( $punch->payrate ){
						$overtime_rate = $punch->payrate / 3600.0;
					} else if( $role_id || $role_id == 0  ){
						$overtime_rate = $payrates->get_rate_rate( $role_id ) / 3600.0;
					}
				}

				$double_time_seconds = 0;
				foreach( $punch->contribution[DOUBLE_TIME] as $role_id => $seconds ){
					$double_time_seconds += $seconds;
					if( $punch->payrate ){
						$doubletime_rate = $punch->payrate / 3600.0;
					} else if( $role_id || $role_id == 0  ){
						$doubletime_rate = $payrates->get_rate_rate( $role_id ) / 3600.0;
					}
				}

				$total_seconds = $regular_seconds + $overtime_seconds + $double_time_seconds;

				$regular_paid = $regular_seconds * $regular_rate * 1.0;
				$overtime_paid = $overtime_seconds * $overtime_rate * 1.5;
				$double_time_paid = $double_time_seconds * $doubletime_rate * 2.0;
				$total_paid = $regular_paid + $overtime_paid + $double_time_paid;

				$row_result[] = $total_seconds;
				$row_result[] = $regular_seconds;
				$row_result[] = $overtime_seconds;
				$row_result[] = $double_time_seconds;

				$row_result[] = $total_paid;
				$row_result[] = $regular_paid;
				$row_result[] = $overtime_paid;
				$row_result[] = $double_time_paid;
				$row_result[] = $punch;
				
				$ret_arr[] = '"'.$counter.'","'.$date.'","'. $user_id .'","'. $punch->role_id .'","'. $punch->payrate.'","'.($punch->payrate*1.5).'","'.date("Y-m-d H:i:s",$punch->time_in).'","'.date("Y-m-d H:i:s",$punch->time_out).'","'.$total_paid.'","'.$regular_paid.'","'.$overtime_paid.'","'.($total_seconds/3600).'","'.($regular_seconds/3600).'","'.($overtime_seconds/3600).'"'; 
				
				$counter++;	
			}
			//echo "The ret array is: <pre>".print_r($ret_arr,1);
			return implode("\r\n", $ret_arr);
			}
		}

		function get_total_labor_cost() {
			$o_retval = new stdClass();
			global $time_cards_results;
			$data = array();
			foreach( $time_cards_results['users'] as $user_id => $user ){
				//id
				//work_weeks
				//processed
				//userfound
				//username
				//full_name
				//access
				//locationid
				//lavu_admin
				//deleted
				//classes
				//payrates
				//clocked_in
				//timePunches

				$payrates = $user->payrates;
				$classes = $user->classes;
				$full_name = $user->full_name;
				$user_id = $user->id;
				$punches = array();
				//foreach( $user->work_weeks as $work_week_start_ts => $work_week ){
					//week
					//next_week
					//work_days
					//seconds
					//days
					//processed
					//time_punches
					foreach($user->timePunches as $punch_id => $punch ){
						//id
						//locationid
						//userid
						//time_in
						//time_out
						//clocked_out
						//clocked_out_via_app
						//delete
						//rate_id
						//in_requested_time_period
						//role_id
						//start_week
						//end_week
						//contribution
						$punches[$punch->id] = $punch;
					}
				//}

				foreach( $punches as $punch_id => $punch ){

					$report_start_time = TimeCardAccountSettings::settings()->start_time;
					$report_end_time = TimeCardAccountSettings::settings()->end_time;

					if( !(($punch->time_out >= $report_start_time && $punch->time_out <= $report_end_time) ||
						($punch->time_in >= $report_start_time && $punch->time_in <= $report_end_time) ||
						($punch->time_in <= $report_start_time && $punch->time_out >= $report_end_time)) ||
						!$punch->in_requested_time_period ){
						continue;
					}

					$role = $classes->getRole( $punch->role_id );
					$row_result = array(
						$user_id,
						$full_name,
						$punch->time_in,//'m/d h:i A'
						($punch->clocked_out?'':'e') . $punch->time_out
					);
					$regular_seconds = 0;
					$regular_rate = 0;
					$overtime_rate = 0;
					$doubletime_rate = 0;

					foreach( $punch->contribution[REGULAR] as $role_id => $seconds ){
						$regular_seconds += $seconds;
						static $it = 0;

						if( $punch->payrate ){
							$regular_rate = $punch->payrate / 3600.0;
						} else if( $role_id || $role_id == 0 ){
							$regular_rate = $payrates->get_rate_rate( $role_id ) / 3600.0;
						}
					}

					$overtime_seconds = 0;
					foreach( $punch->contribution[OVERTIME] as $role_id => $seconds ){
						$overtime_seconds += $seconds;
						if( $punch->payrate ){
							$overtime_rate = $punch->payrate / 3600.0;
						} else if( $role_id || $role_id == 0  ){
							$overtime_rate = $payrates->get_rate_rate( $role_id ) / 3600.0;
						}
					}

					$double_time_seconds = 0;
					foreach( $punch->contribution[DOUBLE_TIME] as $role_id => $seconds ){
						$double_time_seconds += $seconds;
						if( $punch->payrate ){
							$doubletime_rate = $punch->payrate / 3600.0;
						} else if( $role_id || $role_id == 0  ){
							$doubletime_rate = $payrates->get_rate_rate( $role_id ) / 3600.0;
						}
					}

					$total_seconds = $regular_seconds + $overtime_seconds + $double_time_seconds;

					$regular_paid = $regular_seconds * $regular_rate * 1.0;
					$overtime_paid = $overtime_seconds * $overtime_rate * 1.5;
					$double_time_paid = $double_time_seconds * $doubletime_rate * 2.0;
					$total_paid = $regular_paid + $overtime_paid + $double_time_paid;

					$row_result[] = $total_seconds;
					$row_result[] = $regular_seconds;
					$row_result[] = $overtime_seconds;
					$row_result[] = $double_time_seconds;

					$row_result[] = $total_paid;
					$row_result[] = $regular_paid;
					$row_result[] = $overtime_paid;
					$row_result[] = $double_time_paid;
					$row_result[] = $punch_id;
					$data[] = $row_result;
				}
			}
			$o_retval->total = new stdClass();
			$o_retval->total->total_hours = 0;
			$o_retval->total->regular_hours = 0;
			$o_retval->total->overtime_hours = 0;
			$o_retval->total->doubletime_hours = 0;
			$o_retval->total->total_paid = 0;
			$o_retval->total->regular_paid = 0;
			$o_retval->total->overtime_paid = 0;
			$o_retval->total->doubletime_paid = 0;
			foreach( $data as $emp_entry ){
				$o_retval->total->total_hours += $emp_entry[4];
				$o_retval->total->regular_hours += $emp_entry[5];
				$o_retval->total->overtime_hours += $emp_entry[6];
				$o_retval->total->doubletime_hours += $emp_entry[7];
				$o_retval->total->total_paid += $emp_entry[8];
				$o_retval->total->regular_paid += $emp_entry[9];
				$o_retval->total->overtime_paid += $emp_entry[10];
				$o_retval->total->doubletime_paid += $emp_entry[11];
			}

			return $o_retval;
		}
		//return implode("\r\n", $ret_arr);
	}
	function get_items($loc_data){
		$ret_arr  = array('"ItemCode","ItemName","ItemMajorCategoryCode","ItemMinorCategoryCode","AlternateCode"');
		$ret_arr[] = '"0","(None)","0","0",""';

		$query = lavu_query("SELECT mi.*, mc.group_id FROM `menu_items` mi LEFT OUTER JOIN `menu_categories` mc ON mi.`category_id` = mc.`id` WHERE mi.`menu_id`='[1]' AND mi.`_deleted`='0' AND mi.`active`='1' order by mi.`id`", $loc_data['menu_id']);
		if ($query === false) die("Query error: ". lavu_dberror());
		while($res = mysqli_fetch_assoc($query)) {
			//$group_res = mysqli_fetch_assoc(lavu_query("SELECT * FROM `menu_categories` WHERE `id`='[1]' and _deleted = 0",$res['category_id']));
			$ret_arr[] = '"'.$res['id'].'","'.$res['name'].'","'.$res['group_id'].'","'.$res['category_id'].'",""';
		}

		$query = lavu_query("SELECT * FROM `forced_modifiers` ORDER BY `id`");
		if ($query === false) die("Query error: ". lavu_dberror());
		while ($res = mysqli_fetch_assoc($query)) {
			$modIdToItemId = -1 * intval($res['id']);
			$modListIdToMajorCode = -1 * intval($res['list_id']);
			$escapedTitle = str_replace('"', "''", $res['title']);
			$ret_arr[] = '"'.$modIdToItemId.'","'.$escapedTitle.'","'.$modListIdToMajorCode.'","0",""';
		}

		$query = lavu_query("SELECT * FROM `modifiers` ORDER BY `id`");
		if ($query === false) die("Query error: ". lavu_dberror());
 		while ($res = mysqli_fetch_assoc($query)) {
			$modIdToItemId = -1 * (intval($res['id']) + 1000000);
			$modListIdToMajorCode = -1 * (intval($res['list_id']) + 1000000);
			$escapedTitle = str_replace('"', "''", $res['title']);
			$ret_arr[] = '"'.$modIdToItemId.'","'.$escapedTitle.'","'.$modListIdToMajorCode.'","0",""';
		}

		return implode("\r\n", $ret_arr);
	}
	function get_major_cat_code($loc_data){
		$ret_arr   = array('"CategoryCode","CategoryName"');
		$ret_arr[] = '"0","(None)"';
		//$ret_arr= array();
		$query = lavu_query("SELECT * FROM `menu_groups` WHERE `menu_id`='[1]' ORDER BY id", $loc_data['menu_id']);
		if ($query === false) die("Query error: ". lavu_dberror());
		while ($res = mysqli_fetch_assoc($query)) {
			$ret_arr[] = '"'.$res['id'].'","'.$res['group_name'].'"';
		}

		$query = lavu_query("SELECT * FROM `forced_modifiers` ORDER BY id");
		if ($query === false) die("Query error: ". lavu_dberror());
		while ($res = mysqli_fetch_assoc($query)) {
			$modIdToItemId = -1 * intval($res['id']);
			$escapedTitle = str_replace('"', "''", $res['title']);
			$ret_arr[] = '"'.$modIdToItemId.'","'.$escapedTitle.'"';
		}

		$query = lavu_query("SELECT * FROM `modifiers` ORDER BY id");
		if ($query === false) die("Query error: ". lavu_dberror());
		while ($res = mysqli_fetch_assoc($query)) {
			$modIdToItemId = -1 * (intval($res['id']) + 1000000);
			$escapedTitle = str_replace('"', "''", $res['title']);
			$ret_arr[] = '"'.$modIdToItemId.'","'.$escapedTitle.'"';
		}

		return implode("\r\n", $ret_arr);
	}
	function get_minor_cat_code($loc_data){
		$ret_arr   = array('"CategoryCode","CategoryName"');
		$ret_arr[] = '"0","(None)"';
		//$ret_arr= array();
		$query 	   = lavu_query("SELECT * FROM `menu_categories` where `menu_id`='[1]' order by id", $loc_data['menu_id']);
		while($res = mysqli_fetch_assoc($query)){
			$ret_arr[] = '"'.$res['id'].'","'.$res['name'].'"';
		}
		return implode("\r\n", $ret_arr);
	}
	function get_labor_category(){
		$ret_arr   = array('"CategoryCode","CategoryName"');
		//$ret_arr= array();
		$query 	   = lavu_query("SELECT * FROM `emp_classes` where `_deleted`='0' ");
		while($res = mysqli_fetch_assoc($query)){
			$ret_arr[] = '"'.$res['id'].'","'.$res['title'].'"';
		}
		return implode("\r\n", $ret_arr);
	}
	function get_order_type($loc_data){
		$ret_arr   = array('"OrderTypeCode","OrderTypeName"');
		$ret_arr[] = '"0","DineIn"';
		$ret_arr[] = '"1","TOGO"';
		return implode("\r\n", $ret_arr);
	}
	function get_payment($loc_id, $date){
		$counter = 1;
		$ret_arr = array('"PaymentId","PaymentTypeCode","PatronName"');
		//$ret_arr= array();
		$query   = lavu_query("SELECT * FROM `cc_transactions` WHERE `voided`='0' and `loc_id`='[1]' AND `datetime` LIKE '[2]%'", $loc_id, $date);
		while ($res = mysqli_fetch_assoc($query)) {
			echo 'OrderId='. $res['order_id'] .' PaymentId='. obscure_data($res['order_id']) .'<br>';  //debug
			$res['order_id'] = obscure_data($res['order_id']);
			$ret_arr[] = '"'.$res['order_id'].'","'.$res['pay_type_id'].'","'.$res['info'].'"';
			$counter++;
		}
		return implode("\r\n", $ret_arr);
	}
	function get_payment_types($loc_id){
		$ret_arr   = array('"PaymentTypeCode","PaymentTypeName","AlternateCode"');
		//$ret_arr= array();
		$query 	   = lavu_query("SELECT * FROM `payment_types` where `_deleted`='0' and (`loc_id`='[1]' OR `loc_id`='0') ", $loc_id);
		while($res = mysqli_fetch_assoc($query)){
			$ret_arr[] = '"'.$res['id'].'","'.$res['type'].'",""';
		}
		return implode("\r\n", $ret_arr);
	}
	function get_service_charge($loc_data){
		global $serviceChargeCodes;
		if (!empty($loc_data['gratuity_label'])) $serviceChargeCodes[] = $loc_data['gratuity_label'];
		$ret_arr   = array('"ServiceChargeCode","ServiceChargeName","AlternateCode"');
		foreach ($serviceChargeCodes as $i => $serviceChargeCode) {
			$ret_arr[] = '"'.$i.'","'.$serviceChargeCode.'",""';
		}
		return implode("\r\n", $ret_arr);
	}
	function get_taxes($loc_data){
		global $taxCodeNames;
		$ret_arr   = array('"TaxCode","TaxName"');
		foreach ($taxCodeNames as $taxCodeName => $taxCode) {
			$ret_arr[] = '"'.$taxCode.'","'.$taxCodeName.'"';
		}
		return implode("\r\n", $ret_arr);
	}
	function get_transactions($date_time, $loc_data) {
		global $itemRowCounts;
		$ret_arr   = array('"TransactionId","BusinessDate","TransactionCode","OpenDateTime","CloseDateTime","CostCenterCode","PatronCount","ItemCount","ItemSum","TaxSum","ServiceChargeSum","VoidSum","PaymentSum","DiscountSum","TransactionTotal","SvcChgPctOfItemSum","OpenEmployeeCode","TableNumber","OrderTypeCode"');
		//$ret_arr = array();
		//$query = lavu_query("SELECT * FROM `orders` WHERE `closed` like '[1]%' AND `location_id`= '[2]' AND `void` != '1' ",$date_time, $loc_data['id']);
		//$query = lavu_query("SELECT * FROM `orders` WHERE `closed` like '[1]%' AND `location_id`= '[2]'",$date_time, $loc_data['id']);
		$query = lavu_query("SELECT count(`order_contents`.id) as `ItemCount`, count(`order_contents`.id) as `Total`, sum(`order_contents`.`void`) as `VoidedOrderContents`, `orders`.* FROM `order_contents` LEFT JOIN `cc_transactions`  on `order_contents`.`order_id` = `cc_transactions`.`order_id`  LEFT JOIN `orders` on `order_contents`.`order_id`= `orders`.`order_id` WHERE `order_contents`.`server_time` LIKE '[1]%' AND `order_contents`.`loc_id` ='[2]' group by `orders`.`order_id` order by `orders`.`id`", $date_time, $loc_data['id']);
		while ($res = mysqli_fetch_assoc($query)) {
			//echo print_r($res,1);
			$void_sum = 0.00;
			if ($res['VoidedOrderContents'] !== '0') {
				$items_q = lavu_query("SELECT * FROM `order_contents` WHERE `order_id`='[1]'", $res['order_id']);
				while ($items_res = mysqli_fetch_assoc($items_q)) {
					if ($items_res['void'] !== '0') $void_sum += floatval($items_res['price']) * floatval($items_res['void']);
				}
			}
			if ($res['gratuity_percent'] < 1){
				$res['gratuity_percent'] = $res['gratuity_percent'] * 100;
			}

			$transactionTotal = floatval($res['subtotal']) + floatval($res['tax']) + floatval($res['gratuity']) + floatval($res['card_gratuity']) - floatval($res['discount']) - $void_sum;
			$svcChgPctOfItemSum = (floatval($res['gratuity']) + floatval($res['card_gratuity'])) / floatval($res['subtotal']) * 100;
			$discountTotal = -1.00 * (floatval($res['discount']) + floatval($res['idiscount_amount']));  // WARNING: richard said idiscount_amount has already been taking out of subtotal so adding this could throw the balances off but I don't want to forget about idiscount_amount
			$itemSum = $res['subtotal'] - $void_sum;

			//$o_id = explode( "-", $res['order_id'] );
			$o_id = obscure_data($res['order_id']);
			$total_payments = $res['cash_paid'] + $res['card_paid'];
			$ret_arr[]  = '"'.$o_id.'","'.$date_time.'","'.str_replace("-","",$res['order_id']).'","'.$res['opened'].'","'.
						  $res['closed'].'","'.$res['revenue_center_id'].'","'.$res['guests'].'","'.$itemRowCounts[$o_id].'","'.
						  $itemSum.'","'.$res['tax'].'","'.$res['gratuity'].'","'.$void_sum.'","'.$total_payments.'","'.$discountTotal.'","'.$transactionTotal.'","'.
						  $svcChgPctOfItemSum.'","'.$res['cashier_id'].'","0","'.($res['togo_status']*1).'"';
			
		}
		//echo "<pre>".print_r($ret_arr,1);
		return implode("\r\n", $ret_arr);
	}
	function get_transaction_details($date_time, $loc_data){
		global $itemRowCounts;
		global $taxCodeNames;
		global $discountCodes;
		global $voidReasonCodes;
		global $serviceChargeCodes;

		$ret_arr = array('"TransactionDetailId","TransactionId","TransactionDetailTypeId","TransactionDetailDate","ItemCode","PaymentId","VoidReasonCode","TaxCode","ServiceChargeCode","DiscountCode","Amount","Quantity","EmployeeCode","SequenceNumber","SeatNumber","ParentTransactionDetailID","CostCenterCode"');
		$counter = 1;
		$row_counter = 1;
		$result_counter = 1;
		$current_id = '';
		$prev_res = '';
		$parent_id = 0;
		$prev_id = '';
		//$q = lavu_query("SELECT `orders`.`order_id` as `transactionId`, `order_contents`.`server_time` as `transactionDetailDate`,`order_contents`.`item_id` as `itemCode`, `cc_transactions`.`pay_type_id` as `paymentType`, `order_contents`.`void`, `orders`.`void_reason`, `order_contents`.`tax_amount`, `order_contents`.`apply_taxrate`, `order_contents`.`tax_rate1`, `order_contents`.`tax1`, `order_contents`.`tax_rate1`, `order_contents`.`tax2`, `order_contents`.`tax_rate2`, `order_contents`.`tax3`, `order_contents`.`tax_rate3`, `order_contents`.`tax4`, `order_contents`.`tax_rate4`,  `order_contents`.`tax5`, `order_contents`.`tax_rate5`, `order_contents`.`discount_amount` as `discount_amount`, `orders`.`discount_id`,`order_contents`.`price`, `order_contents`.`quantity`,`orders`.`server_id`,`order_contents`.`options`,`order_contents`.`forced_modifier_group_id`,`order_contents`.`forced_modifiers_price`,  `order_contents`.`seat`,`orders`.`revenue_center_id`,`orders`.`tax`,`orders`.`discount`,`orders`.`gratuity`,`orders`.`card_gratuity`,`order_contents`.`after_gratuity`,`orders`.`gratuity`,`cc_transactions`.`total_collected` FROM `order_contents` LEFT JOIN `cc_transactions`  on `order_contents`.`order_id` = `cc_transactions`.`order_id`  LEFT JOIN `orders` on `order_contents`.`order_id`= `orders`.`order_id` WHERE `order_contents`.`server_time` LIKE '[1]%' AND `order_contents`.`loc_id` ='[2]' order by `transactionId`", $date_time, $loc_data['id']);
		$sql =<<<SQL

(SELECT
 `order_contents`.`order_id` as `transactionId`,
 `server_time` as `transactionDetailDate`,
 `item_id` as `itemCode`,
 `order_contents`.`void`,
 `orders`.`void_reason`,
 `apply_taxrate`,
 `tax_rate1`,
 `order_contents`.`tax_rate2`,
 `order_contents`.`tax_rate3`,
 `order_contents`.`tax_rate4`,
 `order_contents`.`tax_rate5`,
 `order_contents`.`tax_amount`,
 `order_contents`.`tax1`,
 `order_contents`.`tax2`,
 `order_contents`.`tax3`,
 `order_contents`.`tax4`,
 `order_contents`.`tax5`,
 `order_contents`.`discount_amount` as `discount_amount`,
 `orders`.`discount_id`,
 `order_contents`.`subtotal`,
 `order_contents`.`quantity`,
 `orders`.`server_id`,
 `order_contents`.`options`,
 `order_contents`.`forced_modifier_group_id`,
 `order_contents`.`forced_modifiers_price`,
 `order_contents`.`seat`,
 `orders`.`revenue_center_id`,
 `orders`.`tax`,
 `orders`.`discount`,
 `orders`.`gratuity`,
 `orders`.`card_gratuity`,
 `order_contents`.`after_gratuity`,
 `orders`.`gratuity`,
 '0' as `total_collected`,
 `order_contents`.`icid`
FROM `order_contents`, `orders`
WHERE `order_contents`.`order_id` = `orders`.`order_id`
and `order_contents`.`server_time` like '[1]%'
and `order_contents`.`loc_id` = '[2]')

UNION

(SELECT
 t.`order_id` as `transactionId`,
 `server_time` as `transactionDetailDate`,
 '' as `itemCode`,
 '' as `void`,
 '' as `void_reason`,
 '' as `apply_taxrate`,
 '' as `tax_rate1`,
 '' as `tax_rate2`,
 '' as `tax_rate3`,
 '' as `tax_rate4`,
 '' as `tax_rate5`,
 '' as `tax_amount`,
 '' as `tax1`,
 '' as `tax2`,
 '' as `tax3`,
 '' as `tax4`,
 '' as `tax5`,
 '' as `discount_amount`,
 '' as `discount_id`,
 '' as `subtotal`,
 '1' as `quantity`,
 t.`server_id`,
 '' as `options`,
 '' as `forced_modifier_group_id`,
 '' as `forced_modifiers_price`,
 '' as `seat`,
 o.`revenue_center_id` as `revenue_center_id`,
 '' as `tax`,
 '' as `discount`,
 '' as `gratuity`,
 '' as `card_gratuity`,
 '' as `after_gratuity`,
 '' as `gratuity`,
 `total_collected`, 
 '' as `icid`
FROM `cc_transactions` t
LEFT OUTER JOIN `orders` o on t.`order_id` = o.`order_id`
WHERE `server_time` like '[1]%'
and `loc_id` = '[2]')

order by `transactionId`, `transactionDetailDate`

SQL;
		$q = lavu_query($sql, $date_time, $loc_data['id']);
		if ($q === false) die( lavu_dberror() );

		while ( $res = mysqli_fetch_assoc($q) )
		{
			//echo "Counter:  ".$counter."TransactionId= ".$res['transactionId']."<br>";
			$res['transactionId'] = obscure_data($res['transactionId']);

			// Start the sequence counter over every time we change Order ID.
			if ( $prev_res['transactionId'] != $res['transactionId'] || $result_counter == mysqli_num_rows($q) ) {
				$counter = 1;
			}
		
			if(empty($res['revenue_center_id'])){
				$res['revenue_center_id'] = -1;
			}

			// TransactionDetailTypeID 1 rows (Items) get created for each row and we may also generate a TransactionDetailTypeID 2 - 5 row from that same row, depending upon the row values.
			// However, TransactionDetailTypeID 6 rows (Payments) have their own rows in the db query results so we have to prevent TransactionDetailTypeID 1 rows from being created for them.
			// (Payments have their own rows is because they're stored as separate entities in the database and also since it's possible to have multiple payments per Order ID.) 

			// Item
			if ( intval($res['total_collected']) === 0) {
				$total_price = $res['subtotal'] + $res['modify_price'];  // richard said to add these together.  No longer adding $res['forced_modifiers_price'] since we'll be processing modifier groups separately. 
				$ret_arr[] = '"'.$row_counter.'","'.$res['transactionId'].'","1","'.$res['transactionDetailDate'].'","'.$res['itemCode'].'","0","0","0","0","0","'.$total_price.'","'.$res['quantity'].'","'.$res['server_id'].'","'.$counter.'","'.$res['seat'].'","0","'.$res['revenue_center_id'].'"';
				$itemRowCounts[$res['transactionId']] = (isset($itemRowCounts[$res['transactionId']])) ? $itemRowCounts[$res['transactionId']] + intval($res['quantity']) : intval($res['quantity']);
				$parent_id = $row_counter;
				$counter++;
				$row_counter++;

				// Forced Modifiers for the Item
				$forcedModifiersQuery = lavu_query("select * from `modifiers_used`, `forced_modifiers` where `modifiers_used`.`mod_id` = `forced_modifiers`.`id` and `icid` = '[1]' and `modifiers_used`.`type` = 'forced' order by `row`", $res['icid']);
				if ($forcedModifiersQuery === false) die(lavu_dberror());
				while ( $mod = mysqli_fetch_assoc($forcedModifiersQuery) ) {
					$modIdToItemId = -1 * intval($mod['id']);
					$ret_arr[] = '"'.$row_counter.'","'.$res['transactionId'].'","1","'.$res['transactionDetailDate'].'","'.$modIdToItemId.'","0","0","0","0","0","'.$mod['cost'].'","'.$mod['qty'].'","'.$res['server_id'].'","'.$counter.'","'.$res['seat'].'","'. $parent_id .'","'.$res['revenue_center_id'].'"';
					$itemRowCounts[$res['transactionId']] = (isset($itemRowCounts[$res['transactionId']])) ? $itemRowCounts[$res['transactionId']] + 1 : 1;
					$counter++;
					$row_counter++;					
				}

				// Optional Modifiers for the Item
				$optionalModifiersQuery = lavu_query("select * from `modifiers_used`, `modifiers` where `modifiers_used`.`mod_id` = `modifiers`.`id` and `icid` = '[1]' and `modifiers_used`.`type` != 'forced' order by `row`", $res['icid']);
				if ($optionalModifiersQuery === false) die(lavu_dberror());
				while ( $mod = mysqli_fetch_assoc($optionalModifiersQuery) ) {
					$modIdToItemId = -1 * (intval($mod['id']) + 1000000);
					$ret_arr[] = '"'.$row_counter.'","'.$res['transactionId'].'","1","'.$res['transactionDetailDate'].'","'.$modIdToItemId.'","0","0","0","0","0","'.$mod['cost'].'","'.$mod['qty'].'","'.$res['server_id'].'","'.$counter.'","'.$res['seat'].'","'. $parent_id .'","'.$res['revenue_center_id'].'"';
					$itemRowCounts[$res['transactionId']] = (isset($itemRowCounts[$res['transactionId']])) ? $itemRowCounts[$res['transactionId']] + 1 : 1;
					$counter++;
					$row_counter++;					
				}

			}

			/*//DO forced mods shit... 
			//echo "sup forced mod is: ".print_r($res,1);
			if($res['forced_modifier_group_id']){
				$g_id = preg_replace("/[^0-9,.]/", "", $res['forced_modifier_group_id']);
				//echo "the gid is; ". $g_id; 
				$ret_arr[] = '"'.$row_counter.'","'.$res['transactionId'].'","1","'.$res['transactionDetailDate'].'","'.$g_id.'","0","0","0","0","0","'.$res['forced_modifiers_price'].'","'.$res['quantity'].'","'.$res['server_id'].'","'.$counter.'","'.$res['seat'].'","'.($counter-1).'","'.$res['revenue_center_id'].'"';
				$counter++;
				$row_counter++;
			}*/

			// Discount
			if( $res['discount_id'] > 0 ){
				$ret_arr[] = '"'.$row_counter.'","'.$res['transactionId'].'","2","'.$res['transactionDetailDate'].'","'.$res['itemCode'].'","0","0","0","0","'. $res['discount_id'].'","'.-1.00 * (floatval($res['discount_amount']) + floatval($res['idiscount_amount'])).'","'.$res['quantity'].'","'.$res['server_id'].'","'.$counter.'","'.$res['seat'].'","0","'.$res['revenue_center_id'].'"';
				if (isset($res['discount_id'])) $discountCodes[$res['discount_id']] = 1;
				$counter++;
				$row_counter++;
			}

			// Service Charge
			$serviceChargeCode = '1';
			if ( $res['after_gratuity'] > 0 ) {
				$ret_arr[] = '"'.$row_counter.'","'.$res['transactionId'].'","3","'.$res['transactionDetailDate'].'","0","0","0","0","'. $serviceChargeCode .'","0","'.$res['after_gratuity'].'","'.$res['quantity'].'","'.$res['server_id'].'","'.$counter.'","'.$res['seat'].'","0","'. $res['revenue_center_id'].'"';
				$counter++;
				$row_counter++;
			}

			// Tax
			if ( $res['tax_amount'] > 0 ) {
				$i = 1;
				$encodedTaxProfiles = explode(';;', $res['apply_taxrate']);
				foreach ( $encodedTaxProfiles as $encodedTaxProfile ) {
					$hasPreTaxProfilesData = ( $res['apply_taxrate'] === $res['tax_rate1'] && strpos('::', $res['apply_taxrate']) === false );

					$taxProfile = explode('::', $encodedTaxProfile);
					$taxProfileId = isset($taxProfile[0]) ? $taxProfile[0] : '';
					$taxProfileName = (empty($taxProfile[1]) || $hasPreTaxProfilesData) ? '(Pre-Tax Profiles Data)' : $taxProfile[1];
					$taxRate = (empty($taxProfile[2]) || $hasPreTaxProfilesData) ? $res["tax_rate{$i}"] : $taxProfile[2];
					$taxProfileInclusive = isset($taxProfile[3]) ? $taxProfile[3] : '';  // Not sure if this is really what the field is, but we don't use it anyhow.

					// Skip tax profiles with no information to match how the app handles things.
					if ( empty($taxProfileId) && empty($taxProfileName) && empty($taxRate) && empty($taxProfileInclusive) ) continue;

					// This is kind of backwards from the way we do other codes: the code number is usually the array key but here
					// the name is the array key.  This is done so we can easily check if the tax profile name has already been registered
					// in $taxCodeNames.  With this system, two tax profiles with the same name will be treated as a single tax code but
					// that seems acceptable for Guest Metrics' specs since they just want to know the TaxCode names.
					$taxCode = '';
					if ( !isset( $taxCodeNames[$taxProfileName] ) ) {
						// The TaxCode name hasn't been registered by another Item yet so we need to claim the next sequential TaxCode integer using our TaxCode name.
						$taxCode = count($taxCodeNames) + 1;
						$taxCodeNames[$taxProfileName] = $taxCode;
					} else {
						// The TaxCode name has already been registered by another Item so just get the TaxCode using the name.
						$taxCode = $taxCodeNames[$taxProfileName];
					}

					$taxAmount = $res["tax{$i}"];

					// Generate the row.
					$ret_arr[] = '"'.$row_counter.'","'.$res['transactionId'].'","4","'.$res['transactionDetailDate'].'","0","0","0","'.$taxCode.'","0","0","'.$taxAmount.'","'.$res['quantity'].'","'.$res['server_id'].'","'.$counter.'","'.$res['seat'].'","0","'.$res['revenue_center_id'].'"';

					$i++;
					$counter++;
					$row_counter++;
				}
			}

			// Void
			if ( $res['void'] == '1' ) {
				// Miracle Zhou said we have to create 3 total rows for voids - the original transaction, the void transaction (next code chunk), and this nega-transaction item row. 
				$total_price = -1.00 * (floatval($res['subtotal']) + floatval($res['modify_price']));  // richard said to add these together.  No longer adding $res['forced_modifiers_price'] since we'll be processing modifier groups separately. 
				$ret_arr[] = '"'.$row_counter.'","'.$res['transactionId'].'","1","'.$res['transactionDetailDate'].'","'.$res['itemCode'].'","0","0","0","0","0","'.$total_price.'","'.$res['quantity'].'","'.$res['server_id'].'","'.$counter.'","'.$res['seat'].'","0","'.$res['revenue_center_id'].'"';
				$itemRowCounts[$res['transactionId']] = (isset($itemRowCounts[$res['transactionId']])) ? $itemRowCounts[$res['transactionId']] + intval($res['quantity']) : intval($res['quantity']);
				$counter++;
				$row_counter++;

				$voidReasonCode = count($voidReasonCodes);
				$voidReasonCodes[$voidReasonCode] = $res['void_reason'];
				$total_void = $res['subtotal'] + $res['modify_price'];
				$ret_arr[] = '"'.$row_counter.'","'.$res['transactionId'].'","5","'.$res['transactionDetailDate'].'","'.$res['itemCode'].'","0","'. $voidReasonCode.'","0","0","0","'.$total_void.'","'.$res['quantity'].'","'.$res['server_id'].'","'.$counter.'","'.$res['seat'].'","0","'.$res['revenue_center_id'].'"';
				$counter++;	
				$row_counter++;
			}

			// Payment
			if ( intval($res['total_collected']) > 0) {
				//echo 'OrderId='. $res['transactionId'] .' PrevPaymentId='. obscure_data($res['transactionId']) .' PaymentId='. obscure_data($res['transactionId']) .'<br>';  //debug
				//$payment_id = obscure_data($res['transactionId']);
				$payment_id = $res['transactionId'];
				$ret_arr[] = '"'.$row_counter.'","'.$res['transactionId'].'","6","'.$res['transactionDetailDate'].'","0","'.$payment_id.'","0","0","0","0","'.$res['total_collected'].'","'.$res['quantity'].'","'.$res['server_id'].'","'.$counter.'","'.$res['seat'].'","0","'.$res['revenue_center_id'].'"';
				$counter++;	
				$row_counter++;
			}

			$prev_res = $res;
			$result_counter++;
		}
		echo implode("\r\n", $ret_arr);
		
		return implode("\r\n", $ret_arr);
	}

	function get_transaction_detail_type(){
		$ret_arr   = array('"TransactionDetailTypeId","DetailTypeName"');
		$ret_arr[] = '"1","Item"';
		$ret_arr[] = '"2","Discount"';
		$ret_arr[] = '"3","Service Charge"';
		$ret_arr[] = '"4","Tax"';
		$ret_arr[] = '"5","Void"';
		$ret_arr[] = '"6","Payment"';

		return implode("\r\n", $ret_arr);
	}
	function get_void_reasons($date_time, $loc_data) {
		global $voidReasonCodes;
		$ret_arr   = array('"VoidReasonCode","VoidReasonName"');
		foreach ($voidReasonCodes as $i => $voidReasonCode) {
			$ret_arr[] = '"'.$i.'","'.$voidReasonCode.'",""';
		}
		return implode("\r\n", $ret_arr);
	}
	function getModules($data_name, $rest_id,  $location_info){
		global $o_package_container;
		$has_LLS = !($location_info['use_net_path'] == "0" || strpos($location_info['net_path'],".poslavu.com")!==false);
		$module_query = mlavu_query("SELECT `modules`, `package_status`,`id` FROM `poslavu_MAIN_db`.`restaurants` WHERE `data_name`='[1]' LIMIT 1", $data_name);
		$module_result = mysqli_fetch_assoc($module_query);
		
		require_once(dirname(__FILE__).'/../../sa_cp/billing/payment_profile_functions.php');
		$package_result = find_package_status($rest_id);
		
		$component_package = '';
		if( !empty($location_info['component_package_code']) ) {
			$component_package = $location_info['component_package_code'];
		}
		$modules = getActiveModules($module_result['modules'],$package_result['package'],$has_LLS, $component_package);
		return $modules;
	}
	function write_files_new($arr, $data_name, $date){
		echo $date;
		global $globalPath;
		$dir = $globalPath . $data_name;
		if (!is_dir($dir)){
    		mkdir($dir);         
		}
		if (!is_dir("{$dir}/{$date}")) {
			mkdir("{$dir}/{$date}");
		}
		foreach($arr as $name => $filedata){
			$fp = fopen("{$dir}/{$date}/{$name}.csv", 'w');
			foreach ($filedata as $line) {
 				fputcsv($fp, $line);
 			}
 			fclose($fp);
		}
	}
	function write_files($arr, $data_name, $date){
		echo $date;
		global $globalPath;
		$dir = $globalPath . $data_name;
		if (!is_dir($dir)){
    		mkdir($dir);         
		}
		if(!is_dir($dir."/".$date))
			mkdir($dir."/".$date);

		foreach($arr as $name=>$file){
			file_put_contents($dir."/".$date."/".$name.".csv", $file);
		}
		return implode("\r\n", $ret_arr);
	}

	function my_aes_encrypt($to_encrypt){
		global $key;
	    return rtrim(base64_encode(mcrypt_encrypt(MCRYPT_RIJNDAEL_256,$key, $to_encrypt, MCRYPT_MODE_ECB, mcrypt_create_iv( mcrypt_get_iv_size(MCRYPT_RIJNDAEL_256, MCRYPT_MODE_ECB), MCRYPT_RAND))), "\0");
	}
	function makeZip($date, $extension){
	    global $globalPath;
		$date= str_replace("-", "",$date);
		$myFile = $globalPath."GMFiles/Paris".$date.$extension.".txt";

		system("zip -P lavu0729 -D -j ".$globalPath."GMFiles/Paris".$date.$extension.".zip ".$globalPath."GMFiles/Paris".$date.$extension.".txt");
		exec(" rm ".$myFile);
		return $extension;
	}
	function doFTP($date, $extension){
		global $globalPath;
	    $date= str_replace("-", "",$date);
	    echo "running command: curl -u poslavu:fuG87RAcEhed! -T ".$globalPath."/Paris".$date."dataname.zip ftp://bihost01.gm360.net";
	    echo exec("curl -u poslavu:fuG87RAcEhed! -T ".$globalPath."/Paris".$date.$extension.".zip stp://bihost01.gm360.net");
	}

?>
