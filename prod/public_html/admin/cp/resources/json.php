<?php
if( !class_exists('LavuJson' ) ){
class LavuJson {
	private static $debug_level = 1;

	public static function json_decode( $str, $asArray=true) {
		$decoder = new LavuJSONDecoder( $asArray );
		$result = $decoder->decode( new LavuStringReader( $str ) );
		if( $decoder->hasError() ){
			if(self::$debug_level > 0) {
				if (self::$debug_level > 1) {
					$a_backtrace = debug_backtrace();
				}
				if (self::$debug_level == 1) {
					error_log("Unable to decode: {$str}, " . $decoder->getError());
				} else if (self::$debug_level == 2) {
					error_log("Unable to decode: {$str}, " . $decoder->getError() . ", debug backtrace[1]: ".print_r($a_backtrace[1],TRUE) );
				} else if (self::$debug_level == 3) {
					error_log("Unable to decode: {$str}, " . $decoder->getError() . ", debug backtrace: ".print_r($a_backtrace,TRUE) );
				}
			}
			return null;
		}
		unset( $decoder );
		$decoder = null;
		return $result;
	}

	public static function get_debug_level() {
		return self::$debug_level;
	}
	public static function set_debug_level($i_level) {
		$i_level = (int)$i_level;
		if ($i_level >= 0 && $i_level <= 3) {
			self::$debug_level = $i_level;
		}
	}

	/**
	 * Used to test for valid json and/or decode without printing to the error log
	 * @param  string       $str The JSON string
	 * @return weakly typed      An array/object on success, or NULL on failure
	 */
	public static function json_validate($str){
		$i_old_debug_level = self::get_debug_level();
		self::set_debug_level(0);
		$wt_retval = self::json_decode($str);
		self::set_debug_level($i_old_debug_level);
		return $wt_retval;
	}

	public static function json_encode( $item ) {
		$encoder = new LavuJSONEncoder();
		$result = $encoder->encode( $item );
		if($encoder->hasError()){
			error_log( $encoder->getError() );
			return null;
		}
		unset( $encoder );
		$encoder = null;
		return $result;
	}

	public static function stringify( $item ) {
		return JSON::json_encode( $item );
	}
	public static function parse( $str ) {
		return JSON::json_decode( $str );
	}
}

class LavuStringReader{
	private $str = null;
	private $index = 0;
	function __construct( $str ) {
		$this->str = $str;
		$this->index = 0;
	}

	function peek() {
		if( $this->index >= strlen($this->str) ){
			return null;
		}
		$c = substr($this->str, $this->index, 1);
		return $c;
	}

	function read() {
		if( $this->index >= strlen($this->str) ){
			return null;
		}
		$c = substr($this->str, $this->index++, 1);
		return $c;
	}

	function reset() {
		$this->index = 0;
	}

	function unread() {
		$this->index--;
	}

	function getNextNonWhiteSpace(){
		$c = "";
		while( ($c = $this->read()) !== null ) {
			if (!ctype_space( $c ) ){
				break;
			}
		}
		return $c;
	}
}

class LavuDecoder {
	protected $error = "Not Implemented";
	function decode( LavuStringReader $reader ) {
		return null;
	}

	function getError(){
		return $this->error;
	}

	function hasError(){
		return !empty( $this->error );
	}
}

class LavuJSONDecoder extends LavuDecoder {
	private $asArray;
	public function __construct( $asArray= true ){
		$this->asArray = $asArray;
	}
	function decode( LavuStringReader $reader ) {
		$this->error = "";
		$c = $reader->peek();
		{
			$decoder = null;
			$result = null;
			switch( $c ){
				case "{" : {
					$decoder = new LavuJSONObjectDecoder( $this->asArray );
					break;
				} case "[" : {
					$decoder = new LavuJSONArrayDecoder( $this->asArray );
					break;
				} default : {
					$this->error = "Unexpected Token";
					$reader->reset();
					return null;
				}
			}
			if ($decoder !== null) {
				$result = $decoder->decode( $reader );
				if( $decoder->hasError() ){
					$this->error = $decoder->getError();
					return null;
				}
				unset( $decoder );
				$decoder = null;
			}
			return $result;
		}

		$this->error = "Unexpected End of Input.";
		return null;
	}
}

class LavuJSONObjectDecoder extends LavuDecoder {
	private $asArray;
	public function __construct( $asArray= true ){
		$this->asArray = $asArray;
	}

	function decode( LavuStringReader $reader ) {
		$this->error = "";
		$c = $reader->read();
		if($c != "{" ){
			$this->error = "Unable to Find Object.";
			return null;
		}
		$result = null;
		if( $this->asArray ){
			$result = array();
		} else {
			$result = new stdClass();
		}

		while( $c !== null ){
			$c = $reader->getNextNonWhiteSpace();

			if($c == "}"){
				return $result;
			}
			if( $c != "\"" ){
				$this->error = "Expected a String, Found something else.";
				return null;
			}

			$decoder = new LavuJSONStringDecoder( $this->asArray );
			$reader->unread();
			$index_name = $decoder->decode( $reader );
			if(! $index_name ){
				$this->error = $decoder->getError();
				return $index_name;
			}
			unset($decoder);
			$decoder = null;

			$c = $reader->getNextNonWhiteSpace();
			if($c != ":" ){
				$this->error = "Expected Assignment (:), Found something else.";
				return null;
			}
			$c = $reader->getNextNonWhiteSpace();

			switch( $c ){
				case "}" :{
					$this->error = "Premature End Found, was expecting a value.";
					return null;
				} case "\"" : {
					$decoder = new LavuJSONStringDecoder();
					break;
				} case "[" : {
					$decoder = new LavuJSONArrayDecoder( $this->asArray );
					break;
				} case "{" : {
					$decoder = new LavuJSONObjectDecoder( $this->asArray );
					break;
				} default : {
					$decoder = new LavuJSONTokenDecoder();
					break;
				}
			}

			if($decoder !== null){
				$reader->unread();
				$value = $decoder->decode( $reader );
				if( $decoder->hasError() ) {
					$this->error = $decoder->getError();
					return null;
				}
				if( $this->asArray ){
					$result[ $index_name ] = $value;
				} else {
					$result->$index_name = $value;
				}
			}

			$c = $reader->getNextNonWhiteSpace();
			if($c == "," ){
				continue;
			} else if( $c == "}" ){
				$reader->unread();
				continue;
			} else {
				$this->error = "Found something unexpected.";
				return null;
			}
		}

		$this->error = "Unexpected end of input found.";
		return null;
	}
}

class LavuJSONArrayDecoder extends LavuDecoder {
	private $asArray;
	public function __construct( $asArray= true ){
		$this->asArray = $asArray;
	}

	function decode( LavuStringReader $reader ) {
		$this->error = "";
		$c = $reader->read();
		if( $c != '['){
			$this->error = "Unable to Find Open Array";
			$reader->unread();
			return null;
		}

		$result = array();
		while( $c !== null ){
			$c = $reader->getNextNonWhiteSpace();
			$decoder = null;
			switch( $c ) {
				case "\"" : {
					$decoder = new LavuJSONStringDecoder();
					break;
				} case "{" : {
					$decoder = new LavuJSONObjectDecoder( $this->asArray );
					break;
				} case "[" : {
					$decoder = new LavuJSONArrayDecoder( $this->asArray );
					break;
				} case "]" : {
					return $result;
				} case "," : {
					$this->error = "Unexpected Comma.";
					return null;
				} default : {
					$decoder = new LavuJSONTokenDecoder();
					break;
				}
			}

			if( $decoder !== null ){
				$reader->unread();
				$decode_result = $decoder->decode( $reader );
				if($decoder->hasError()){
					$this->error = $decoder->getError();
					return null;
				}
				$result[] = $decode_result;
				unset( $decoder );
				$decoder = null;

				$c = $reader->getNextNonWhiteSpace();
				if( $c == "," ){
					continue;
				} else if ($c == "]"){
					$reader->unread();
					continue;
				} else {
					$this->error = "Unexpected Token Found.";
					return null;
				}
			}
		}
		$this->error = "Unexpected end of input found.";
		return null;
	}
}

class LavuJSONStringDecoder extends LavuDecoder {
	function decode( LavuStringReader $reader ) {
		$this->error = "";
		$c = $reader->read();
		if( $c != '"'){
			$this->error = "Unable to Find Open String";
			$reader->unread();
			return null;
		}

		$result ="";

		while ( ($c = $reader->read()) !== null ){
			switch( $c ){
				case null : {
					$this->error = "Unexpected end of input found.";
					return null;
				} case "\\" : {
					$result .= $reader->read();
					break;
				} case "\"" : {
					return $result;
				} default : {
					$result .= $c;
					break;
				}
			}
		}
		$this->error = "Unexpected end of input.";
		return null;
	}
}

class LavuJSONTokenDecoder extends LavuDecoder {
	function decode( LavuStringReader $reader ) {
		$this->error = "";
		$c = $reader->read();
		$token_mapping = array(
			"true" => true,
			"false" => false,
			"null" => null
		);

		$stop_reading = array (
			",",
			"]",
			"}");

		$token = "";
		while( $c !== null ) {
			if( ctype_space( $c ) || in_array($c, $stop_reading)){
				$allowed_tokens = array_keys( $token_mapping );
				if( in_array($c, $stop_reading) ){
					$reader->unread();
				}
				if( in_array($token, $allowed_tokens) ){
					return $token_mapping[ $token ];
				}

				if( is_numeric( $token ) ){
					return $token * 1;
				}

				$this->error = "Found an Unexpected Token: {$token}";
				return null;
			}

			$token .= $c;
			$c = $reader->read();
		}

		$this->error = "Input ended unexpectedly.";
		return null;
	}
}

class LavuEncoder {
	protected $error = "Not Yet Implemented";

	public function hasError() {
		return ! empty($this->error);
	}

	public function getError() {
		return $this->error;
	}

	public function encoder( $item ) {
		return null;
	}
}

class LavuJSONEncoder extends LavuEncoder {
	function encode( $item ){
		$this->error="";
		$encoder = null;
		if( is_array( $item ) ){
			$encoder = new LavuJSONArrayEncoder();
		}

		if( is_object( $item ) ){
			$encoder = new LavuJSONObjectEncoder();
		}

		if(!$encoder){
			$this->error = 'Unable to encode ' . gettype( $item ) . ' expecting an object, or an array.';
			return null;
		}

		$result = $encoder->encode( $item );
		if( $encoder->hasError() ){
			$this->error = $encoder->getError();
			unset( $encoder);
			$encoder = null;
			return null;
		}
		unset( $encoder);
		$encoder = null;
		return $result;
	}
}

class LavuJSONArrayEncoder extends LavuEncoder {
	function encode( $item ) {
		$this->error="";
		if(! is_array( $item ) ){
			$this->error = "Expected an Array instead recieved: " . gettype( $item );
			return null;
		}

		$treat_as_object = false;
		$keys = array_keys( $item );
		foreach( $keys as $k => $v ){
			$treat_as_object |= is_string( $v );
		}

		if( $treat_as_object ){
			$encoder = new LavuJSONObjectEncoder();
			$obj_result = $encoder->encode((object) $item );
			if( $encoder->hasError() ){
				$this->error = $encoder->getError();
				return null;
			}
			return $obj_result;
		}

		$arr_arr = array();
		foreach( $item as $key => $val ){
			$encoder = new LavuJSONValueEncoder();
			$new_val = $encoder->encode( $val );
			if($encoder->hasError()){
				$this->error = $encoder->getError();
				unset( $encoder);
				$encoder = null;
				return null;
			}
			unset( $encoder);
			$encoder = null;

			$arr_arr[] = $new_val;
		}

		return "[" . implode(",", $arr_arr) . "]";
	}
}

class LavuJSONObjectEncoder extends LavuEncoder {
	function encode( $item ) {
		$this->error="";
		if( ! is_object( $item ) ){
			$this->error = "Was expecting an Object, instead received " . gettype( $item );
			return null;
		}

		$object_arr = array();
		foreach( $item as $key => $val ){
			$encoder = new LavuJSONValueEncoder();
			$new_val = $encoder->encode( $val );
			if($encoder->hasError()){
				$this->error = $encoder->getError();
				unset( $encoder);
				$encoder = null;
				return null;
			}
			unset( $encoder);
			$encoder = null;
			$object_arr[] = "\"{$key}\":{$new_val}";
		}

		return "{" . implode(",", $object_arr) . "}";
	}
}

class LavuJSONValueEncoder extends LavuEncoder {
	function encode( $item ) {
		$this->error="";
		$encoder = null;
		if( is_null( $item ) ){
			return "null";
		} else if ( is_bool( $item ) ) {
			return ($item) ? "true" : "false";
		} else if ( is_string( $item ) ) {
			return '"' . str_replace('"', '\"', str_replace( "\\", "\\\\", $item)) . '"';
		} else if ( is_numeric( $item ) ){
			return "". $item;
		} else if( is_array( $item ) ){
			$encoder = new LavuJSONArrayEncoder();
		} else if( is_object( $item ) ){
			$encoder = new LavuJSONObjectEncoder();
		} else {
			$this->error = "Found unexpected value type: " . gettype( $item );
			return null;
		}

		if($encoder){
			$result = $encoder->encode( $item );
			if($encoder->hasError()){
				$this->error = $encoder->getError();
				unset( $encoder);
				$encoder = null;
				return null;
			}
			unset( $encoder);
			$encoder = null;
			return $result;
		}

		$this->error="This should never happen, I have no idea what went wrong to get here.";
		return null;
	}
}
}
