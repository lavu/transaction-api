<?php
	//Written by Brian Dennedy

	//This is largely a 'view' wrapper for csv_input_output.
	//As such this really serves to be a user interface for displaying table data, exporting that data, and importing .csv lists with respect to a table.
	require_once(dirname(__FILE__).'/csv_input_output.php');
	require_once(dirname(__FILE__).'/core_functions.php');
	$lavuAdmin = admin_info("lavu_admin");
	// S E T    F U N C T I O N S . . .
	function setSimpleDisplayTableTitleName($pTableDisplayName){
		global $sdie_tableDisplayName;
		$sdie_tableDisplayName = $pTableDisplayName;
	}
	function setSimpleDisplayTable($pTable){
		global $sdie_table;
		$sdie_table=$pTable;
	}
	//Use only when current lavu_query db, doesn't apply.  Such as where data is stored in poslavu_MAIN_db or elsewhere.
	//If not set, poslavu_$dataname_db will be assumed.
	function setSimpleDisplayDatabase($pDatabase){
		global $sdie_database;
		$sdie_database = $pDatabase;
	}
	function setQueryFunctionForExportArrays($functionName){
		global $exportQueryFunctionReturnsArrOfArrs;
		$exportQueryFunctionReturnsArrOfArrs = $functionName;
	}
	//OVERRIDING FUNCTIONS
	function setSimpleDisplayTitles2DBColumnsMap($ptitles2DBColumnsMap){
		global $sdie_titles2DBColumnsMap, $sdie_DBColumns2TitlesMap;
		$sdie_titles2DBColumnsMap = $ptitles2DBColumnsMap;
		setCSVFieldToDBFieldMap($ptitles2DBColumnsMap);
		foreach($sdie_titles2DBColumnsMap as $titleAsKey => $dbColumnAsValue){
			$sdie_DBColumns2TitlesMap[$dbColumnAsValue] = $titleAsKey;
		}
	}
	function setSimpleDisplayDefaultValuesForDBMap($defaultMapArr){
		setDefaultValuesForDBMap($defaultMapArr);//IN csv_input_output.php
	}
	function setSimpleDisplayDisallowDuplicatesAcrossColumns($columnNames){
		setDisallowDuplicatesAcrossColumns($columnNames);//IN csv_input_output.php
	}
	function setSimpleDisplayUpdateWhenColumnsAreEqualArrays( $dbColumnsArr, $whichColumnsToUpdateArr){  //e.g.: array('dbcolumn'=>'value',  'points'=>'')
		setUpdateWhenColumnsAreEqualArrays( $dbColumnsArr, $whichColumnsToUpdateArr );//IN csv_input_output.php
	}
	function setSimpleDisplayOrderByColumn4TableDisplay($column2OrderBy){
		global $sdie_column2OrderByOnDisplay;
		$sdie_column2OrderByOnDisplay = $column2OrderBy;
	}
	function setSimpleDisplayMandatoryTitleColumns($mandatoryTitleColumns){
		global $sdie_mandatory_title_fields;
		$sdie_mandatory_title_fields = $mandatoryTitleColumns;
	}
	function setSimpleDisplayOptionalTitlesColumns($optionalTitleColumns){
		global $sdie_optional_title_fields;
		$sdie_optional_title_fields = $optionalTitleColumns;
	}
	/*
	function setSimpleDisplayScript4_getTableDataForExport($filePath){
		if(isPHPVersionGTEq5_4_0()){
			if (session_status() == PHP_SESSION_NONE) { session_start(); }
		}else{
			if(session_id() == '') { session_start(); }
		}
		$_SESSION['file_path_4_table_data_4_export'] = $filePath;
	}
	*/
	function setSimpleDisplayExportFilePathForPostbackURL($filePath4URL){
		global $sdie_urlFromFilePath;
		$filePath = explode('public_html/admin', $filePath4URL);
		//$sdie_urlFromFilePath = str_replace($_SERVER['DOCUMENT_ROOT'], '', $filePath4URL);
		$sdie_urlFromFilePath = $filePath[1];
	}
	function setSimpleDisplayCSVExampleText($exampleTextInCSVFormat){
		global $sdie_example_display_text;
		$sdie_example_display_text = $exampleTextInCSVFormat;
	}
	//Basically the 'start' method for this file, call this when all the globals are set.
	function simpleDisplayLaunch(){
		global $sdie_table, $sdie_database, $sdie_tableDisplayName, $exportQueryFunctionReturnsArrOfArrs, $sdie_DBColumns2TitlesMap;
		$sdie_tableDisplayName = !empty($sdie_tableDisplayName) ? $sdie_tableDisplayName : $sdie_table;

		//Determine if is a postback form call or not...
		if(!empty($_POST['submit_import_upload'])){

			$expected_sec_hash = sha1($_POST['data_name'].'_edamame');
			$passed_sec_hash = $_POST['sec_hash'];

			if(empty($expected_sec_hash) || $expected_sec_hash != $passed_sec_hash){ echo "Error hashes did not match."; return; }
			if( $_POST['table_name_4_import'] != $sdie_table){ echo "Error table not set."; return; }
			$tempFilePath=$_FILES["csv_upload_list_".$sdie_table]["tmp_name"];
			$set_database = !empty($sdie_database) ? $sdie_database : null;
			$totalRowsInserted = performInsertForCSVFile($tempFilePath, $sdie_table, $set_database);//We pass the optional 3rd Arg, MAIN db, to override database.
			echo "Total rows inserted:".$totalRowsInserted."<br>";
		}else if( !empty($_POST['submit_export_download']) ){
			if (empty($exportQueryFunctionReturnsArrOfArrs) || !function_exists($exportQueryFunctionReturnsArrOfArrs)){
				echo "Export data is not set using: setQueryFunctionForExportArrays().";
				return;
			}
			$expected_sec_hash = sha1($_POST['data_name'].'_'.$_POST['table_name_4_export'].'_'.$_POST['table_display_name_lower_case'].'_edamame');
			$passed_sec_hash = $_POST['sec_hash'];

			if(empty($expected_sec_hash) || $expected_sec_hash != $passed_sec_hash){ echo "Error hashes did not match."; return; }

			$sdie_table = !empty($_POST['table_name_4_export']) ? $_POST['table_name_4_export'] : null;
			startSessionIfNotStarted();

			//$displayNameLower = strtolower($sdie_tableDisplayName);
			$displayNameLower = strtolower($_POST['table_display_name_lower_case']);
			if(empty($displayNameLower)){
				echo "Cannot find export display name."; return;
			}
			$dataArrOfArrs4CSV = $exportQueryFunctionReturnsArrOfArrs();
			if(!is_array($dataArrOfArrs4CSV)){
				echo "Export data was not pulled correctly"; return;
			}
			if(empty($dataArrOfArrs4CSV)){
				echo "There were no records found for export"; return;
			}
			//For the first array which should be the title strings, not the values...
			$titleArr = &$dataArrOfArrs4CSV[0];
			foreach($titleArr as &$currTitle){
				$currTitle = !empty($sdie_DBColumns2TitlesMap[$currTitle]) ? $sdie_DBColumns2TitlesMap[$currTitle] : $currTitle;
			}
			$dataString = getCSVStringFromArrays($dataArrOfArrs4CSV, ",", '"');
			header('Content-disposition: attachment; filename='.$displayNameLower.'_'.date('YmdHis').'.csv');
			header ("Content-Type:text/txt");
			echo $dataString;
			exit;
		}else{
			_displayComponents();
		}
	}

	// D I S P L A Y / H T M L . . .
	function _displayComponents(){
		global $titleStr, $sdie_table, $sdie_tableDisplayName;
		if(empty($sdie_table)){ echo "Table not set in simple display."; return;}
		_displayPageTitle();
		_displayImportCSVHTML();
		_displayExportHTML();
		_displayTableContentHTML();
	}
	function _displayPageTitle(){
		global $sdie_tableDisplayName;
		$titleHTML = '';
		$titleHTML .= "<span style='color:#aecd37;font-size:20px'>".$sdie_tableDisplayName."</span><br><hr>";
		echo $titleHTML;
	}
	function _displayImportCSVHTML($showColumnsExplanation = true){
		global $titleStr, $sdie_table, $sdie_table, $sdie_tableDisplayName, $sdie_urlFromFilePath, $sdie_mandatory_title_fields, $sdie_optional_title_fields;
		global $sdie_example_display_text;
		$tableDisplayNameLowerCase = strtolower($sdie_tableDisplayName);
		$currMode = $_REQUEST['mode'];

		if($showColumnsExplanation){
			$createOuterDiv = count($sdie_mandatory_title_fields) || count($sdie_optional_title_fields);
			
			if($createOuterDiv){
				$html .= "<div style='display:inline-block;'>";
			}
			
			if(!empty($sdie_mandatory_title_fields)){
				$html .= "<br>Must have columns for import:<br>";
				$i = 0;
				$html .= "<table><tr>";
				foreach($sdie_mandatory_title_fields as $mandatoryFieldTitle){
					if($i % 5 == 0 && $i > 0){ $html .= "</tr><tr>"; }
					$html .= "<td style='border:1px solid gray; color:#F66'>".$mandatoryFieldTitle."</td>";
					$i++;
				}
				$html .= "</tr></table>";
			}
			if(!empty($sdie_optional_title_fields)){
				$html .= "Optional columns for import:<br>";
				$i = 0;
				$html .= "<table><tr>";
				foreach($sdie_optional_title_fields as $optionalFieldTitle){
					if($i % 5 == 0 && $i > 0){ $html .= "</tr><tr>"; }
					$html .= "<td style='border:1px solid gray; color:#66F'>".$optionalFieldTitle."</td>";
					$i++;
				}
				$html .= "</tr></table>";
			}
			if($createOuterDiv){
				$html .= "</div><br>";
			}
			if(!empty($sdie_example_display_text)){
				$html .= "<br>Example:<br><div style='display:inline-block;text-align:left;background-color:#EEE'><pre>".
							$sdie_example_display_text."</pre></div>";
			}
		}
		$html .= "<br><br>";
		//<form action="{$sdie_urlFromFilePath}" method="post" enctype="multipart/form-data">
		$sec_hash = sha1($data_name.'_edamame');
		$alertJS = "alert('Importing table $sdie_tableDisplayName, please wait for page to finish loading file import.');";
		$html .= <<<DSP_IMPORT_HTML
		<form action="" method="post" enctype="multipart/form-data">
			Select the {$titleStr} .csv file to import:
			<input type="file" name="csv_upload_list_{$sdie_table}" id="csv_upload_file_chooser_id" style="background-color:gray;"/><br><br>
			<input type='hidden' name="sec_hash" value="{$sec_hash}"/>
			<input type="hidden" name="table_name_4_import" value="{$sdie_table}"/>
			<input type='hidden' name='data_name' value="$data_name"/>
			<input type="submit" value="Add {$tableDisplayNameLowerCase} in list." 
											name="submit_import_upload" onclick="$alertJS" style="width:200px;"/>
		</form>
DSP_IMPORT_HTML;
		$html .= "<br><hr>";
		echo $html;
	}
	function _displayExportHTML(){
		global $sdie_table, $sdie_tableDisplayName, $sdie_urlFromFilePath, $data_name, $lavuAdmin;
		$tableDisplayNameLowerCase = strtolower($sdie_tableDisplayName);
		$currMode = $_REQUEST['mode'];
		$sec_hash = sha1($data_name.'_'.$sdie_table.'_'.$tableDisplayNameLowerCase.'_edamame');
		//<form action="?mode={$currMode}" method="post" enctype="multipart/form-data">
		$alertJS = "alert('Exporting table $sdie_tableDisplayName, please wait for page to finish.');";

		$deleteAll = "
				$('#delete_all_entries').dialog({
					resizable: false,
					height: 'auto',
					width: '42em',
					modal: true,
					buttons : [{
					text : 'No, I do not want to delete all entries',
					click : function() {
						$( this ).dialog( 'close' );
					}
					},
					{
					text : 'Yes, I want to delete all entries',
					style : 'background:#aecd37;color:#fff',
					click : function() {
						$('#form_delete_all').submit();
						$( this ).dialog( 'close' );
						setTimeout(function(){location.reload(true)}, 15000);
					}
					}]
					});";

		$deleteSelected = "
				var selectedEntries = getSelectedEntries();
				if (selectedEntries.length) {
					
					$('#delete_selected_entries').dialog({
						resizable: false,
						height: 'auto',
						width: '52em',
						modal: true,
						buttons : [{
						text : 'No, I do not want to delete the selected entries',
						click : function() {
							$( this ).dialog( 'close' );
						}
						},
						{
						text : 'Yes, I want to delete the selected entries',
						style : 'background:#aecd37;color:#fff',
						click : function() {
							var giftcardId = JSON.stringify(selectedEntries);
							$('#form_delete_selected #gift_card_id').val(giftcardId);
							$('#form_delete_selected').submit();
							$( this ).dialog( 'close' );
							setTimeout(function(){location.reload(true)}, 3000);
						}
						}]
						});

				  	}
					else {
						alert('Please select at least one entry to delete');
					}
				
				";

		$html = <<<DSP_EXPORT_HTML
		<br>
		<table><tr><td>
		<form action="{$sdie_urlFromFilePath}" method="post" enctype="multipart/form-data">
			<input type='hidden' name="sec_hash" value="{$sec_hash}"/>
			<input type='hidden' name="table_name_4_export" value="{$sdie_table}"/>
			<input type='hidden' name="table_display_name_lower_case" value="{$tableDisplayNameLowerCase}"/>
			<input type='hidden' name="data_name" value="$data_name"/>
			<input type="submit" value="Export {$tableDisplayNameLowerCase} to .csv file"
											name="submit_export_download" onclick="{$alertJS}" style="width:250px;"/>
		</form>
		</td>
DSP_EXPORT_HTML;

		if ($lavuAdmin) {
		$html .= <<<DSP_EXPORT_HTML
		<td>
		<form action="{$sdie_urlFromFilePath}" id="form_delete_all" method="post" enctype="multipart/form-data">
			<input type='hidden' name="action" id="action" value="delete_all_entries"/>
			<input type='hidden' name="data_name" value="{$data_name}"/>
			<input type="submit" value="Delete All Entries" id="delete_all" name="delete_all_entries" onclick="{$deleteAll};return false;" style="width:14em;"/>
			</form>
		</td>
		<td>
		<form action="{$sdie_urlFromFilePath}" id="form_delete_selected" method="post" enctype="multipart/form-data">
			<input type='hidden' name="action" id="action" value="delete_selected_entries"/>
			<input type='hidden' name="gift_card_id" id="gift_card_id" value=""/>
			<input type='hidden' name="data_name" id="data_name" value="{$data_name}"/>
			<input type="submit" value="Delete Selected Entries" id="delete_selected" name="delete_selected" onclick="{$deleteSelected};return false;" style="width:16em;"/>
		</form>
		</td>

		<div id="delete_all_entries" title="Delete All Entries" style="display:none">
		  <p style="font-size:1em">Deleting all entries will remove them from the database. Would you like to proceed?</p>
		</div>
		<div id="delete_selected_entries" title="Delete Selected Entries" style="display:none">
		  <p style="font-size:1em">Deleting the selected entries will remove them from the database. Would you like to proceed? </p>
		</div>
		<br>
DSP_EXPORT_HTML;
		}
		echo $html;
	}

	function _displayTableContentHTML(){
		global $sdie_DBColumns2TitlesMap, $sdie_column2OrderByOnDisplay, $lavuAdmin;
		global $exportQueryFunctionReturnsArrOfArrs;
		$orderBy = !empty($sdie_column2OrderByOnDisplay) ? $sdie_column2OrderByOnDisplay : 'id';
		//(e.g. table_display, etc...), order_by_column, barrier_value (value of order_by_value for paging), asc_desc_order, limit_count
		$contextArr = array('sdie_context'=>'table_display',
			                'sdie_order_by_column'=>$orderBy,
			                'sdie_barrier_value'=>'',
			                'sdie_asc_desc_order'=>'asc',
			                'sdie_limit_count'=>'20');
		if(!empty($_POST['asc_desc_order_field'])){ $contextArr['sdie_order_by_column'] = $_POST['asc_desc_order_field']; }
		if(!empty($_POST['asc_desc_order'])) { $contextArr['sdie_asc_desc_order'] = $_POST['asc_desc_order']; }
		$orderByColumn = $contextArr['sdie_order_by_column'];
		$ascDesc = $contextArr['sdie_asc_desc_order'];


		$prevFirstValue = isset($_POST['first_value_of_ordered_field'])   ?  $_POST['first_value_of_ordered_field']   : '';
		$prevLastValue  = isset($_POST['last_value_of_ordered_field']) ?  $_POST['last_value_of_ordered_field'] : '';
		$prevLeastValue = $prevFirstValue < $prevLastValue ? $prevFirstValue : $prevLastValue;
		$prevMostValue  = $prevFirstValue > $prevLastValue ? $prevFirstValue : $prevLastValue;

//echo "Post Array:<br><pre>".print_r($_POST,1)."</pre>";

		if( !empty($_POST['search_term_field'])){// Handle search
			$contextArr['sdie_context'] = 'search_table_display';
			$contextArr['sdie_search_value'] = $_POST['search_term_field'];
		}else if( !empty($_POST['paging_forward_backward_field']) ){  //Handle paging forward/backward if occured.
			$contextArr['sdie_paging_direction'] = $_POST['paging_forward_backward_field'];
			if( $contextArr['sdie_asc_desc_order'] == 'asc' ){
				if($_POST['paging_forward_backward_field'] == 'forward'){
					$contextArr['sdie_barrier_value']  = $prevLastValue;
					$contextArr['sdie_gt_lt_4_paging'] = '>';
				}else{
					$contextArr['sdie_barrier_value'] = $prevFirstValue;
					$contextArr['sdie_gt_lt_4_paging'] = '<';
				}
			}else{
				if($_POST['paging_forward_backward_field'] == 'backward'){
					$contextArr['sdie_barrier_value'] = $prevFirstValue;
					$contextArr['sdie_gt_lt_4_paging'] = '>';
				}else{
					$contextArr['sdie_barrier_value'] = $prevLastValue;
					$contextArr['sdie_gt_lt_4_paging'] = '<';
				}
			}
		}

		$tableDataArr = $exportQueryFunctionReturnsArrOfArrs($contextArr);
		//We pull the first and last value for the order by row.
		$firstValue=count($tableDataArr) > 1 ? $tableDataArr[1][array_search($orderByColumn,$tableDataArr[0])] : '';
		$lastValue =count($tableDataArr) > 1 ? $tableDataArr[count($tableDataArr)-1][array_search($orderByColumn,$tableDataArr[0])] : '';



		/*
		echo "first value:".$firstValue."<br>";
		echo "last value:".$lastValue."<br>";
		echo "previous first value:".$prevFirstValue."<br>";
		echo "previous last value:" .$prevLastValue ."<br>";
		echo "previous least value:".$prevLeastValue."<br>";
		echo "previous most value:" .$prevMostValue ."<br>";
		*/

		$html = '';
		//Callback form, used for redisplaying, reversing, ordering, and paging callbacks.
		$html .= "<form action='' method='POST' id='redisplay_form_id'>";
		$html .= "<input type='hidden' name='paging_forward_backward_field' id='paging_forward_backward_field_id'/>";
		$html .= "<input type='hidden' name='first_value_of_ordered_field'  id='first_value_of_ordered_field_id' value='".str_replace("'", "\\'", $firstValue)."'/>";//Used for paging
		$html .= "<input type='hidden' name='last_value_of_ordered_field'   id='last_value_of_ordered_field_id'  value='".str_replace("'", "\\'", $lastValue). "'/>";//Used for paging
		$html .= "<input type='hidden' name='asc_desc_order_field' id='asc_desc_order_field_input_id'/>";
		$html .= "<input type='hidden' name='asc_desc_order' id='asc_desc_order_input_id'/>";
		$html .= "<input type='hidden' name='search_term_field' id='search_term_field_id'/>";
		$html .= "</form>";
		//Make into global js vars.
		$html .= "<script type='text/javascript'>";
		$html .= "var paging_forward_backward_field = document.getElementById('paging_forward_backward_field_id');";
		$html .= "var redisplay_form = document.getElementById('redisplay_form_id');";
		$html .= "var asc_desc_order_field = document.getElementById('asc_desc_order_field_input_id');";
		$html .= "var asc_desc_order = document.getElementById('asc_desc_order_input_id');";
		//JS Page Up, Page Down and Search functions.
		$html .= <<<JScriptFuncs
			function page_up_clicked(){
				paging_forward_backward_field.value='backward';
				asc_desc_order_field.value='{$orderByColumn}';
				asc_desc_order.value='{$ascDesc}';
				redisplay_form.submit();
			}
			function page_down_clicked(){
				paging_forward_backward_field.value='forward';
				asc_desc_order_field.value='{$orderByColumn}';
				asc_desc_order.value='{$ascDesc}';
				redisplay_form.submit();
			}
			function search_clicked(){
				var searchFieldInputValue = document.getElementById('search_field_input_id').value;
				var searchFieldHidden = document.getElementById('search_term_field_id');
				searchFieldHidden.value = searchFieldInputValue;
				redisplay_form.submit();
			}
						
			$(document).ready(function () {
				$('#all_giftcards').click(function () {
					$('.check_giftcard').attr('checked', this.checked);
				});
			});

			function getSelectedEntries() {
				var selectedEntries = new Array();
			
				$(document).ready(function () {				
					$('input:checkbox[name=giftcard]:checked').each(function() {
						selectedEntries.push($(this).val());
					});
				});
				return selectedEntries;
			}
JScriptFuncs;
				
		$html .= "</script>";
		$searchWidth = ($lavuAdmin) ? '25em' : '15em';
		//Navigation table
		$html .= "<table border='1'>";
		$html .= "<tr>";
		$html .=   "<td><input type='button' onclick='page_up_clicked();' value='Previous Page'/></td>";
		$html .=   "<td><input type='input'  placeholder='Search' id='search_field_input_id' style='width:".$searchWidth."'/></td>";
		$html .=   "<td><input type='button' onclick='search_clicked();'' value='Search'/></td>";
		$html .=   "<td><input type='button' onclick='page_down_clicked();' value='Next Page'/></td>";
		$html .= "</tr>";
		$html .= "</table>";
		$html .= "<script type='text/javascript'>document.getElementById('search_field_input_id').onkeydown=function(e){ if(e.keyCode == 13){search_clicked();}};</script>";
		//The data display table.
		if (!empty($tableDataArr[0])) {
		$html .= "<table border='1'>";
		$titleRow = $tableDataArr[0];
		$html .= "<tr>";
		$columnCount = 1;
		foreach($titleRow as $currTitleVal){
			$isCurrColumnAlreadyAsc = $_POST['asc_desc_order_field'] == $currTitleVal && strtolower($_POST['asc_desc_order']) == 'asc';
			$ascDescOnClick = $isCurrColumnAlreadyAsc ? 'desc' : 'asc';
			$cursorCSS = 'pointer';
			$ultTitleVal = !empty($sdie_DBColumns2TitlesMap[$currTitleVal]) ? $sdie_DBColumns2TitlesMap[$currTitleVal] : $currTitleVal;

			$titleWidth = "";
			if ($columnCount == 1) {
				$titleWidth = "width:14em";
			}
			else if ($columnCount == 2) {
				$titleWidth = "width:8em";
			}
			else if ($columnCount == 3) {
				$titleWidth = "width:3em";
			}
			else if ($columnCount == 4) {
				$titleWidth = "width:3em";
			}
			else if ($columnCount == 5) {
				$titleWidth = "width:8em";
			}

			$columnCount++;
			$onclickJS = "asc_desc_order_field.value='$currTitleVal'; asc_desc_order.value='$ascDescOnClick'; redisplay_form.submit();";
			$html .= "<td style='background-color:#aaa; cursor:$cursorCSS;".$titleWidth.";' onclick=\"$onclickJS\">".$ultTitleVal."</td>";
		}
		if ($lavuAdmin) {
			$html .= "<td style='width:6.5em;background-color:#aaa; cursor:$cursorCSS;' onclick=\"\">Select All <input type='checkbox' class='select_all_entries' name='all_giftcards' id='all_giftcards'></td>";
		}
		$html .= "</tr>";
		for($i = 1; $i < count($tableDataArr); $i++){
			$currRowValues = &$tableDataArr[$i];
			$html .= "<tr>";
			$r=0;
			$uniqueId = $currRowValues[0];
			foreach($currRowValues as $currVal){
				$currColumnName = $titleRow[$r++ % count($titleRow)];
				$html .= "<td>".$currVal."</td>";
			}
			if ($lavuAdmin) {
				$html .= "<td style='text-align:center'><input type='checkbox' class='check_giftcard' name='giftcard' id='giftcard_$uniqueId' value='$uniqueId'></td>";
			}
			$html .= "</tr>";
		}
		$html .= "</table>";
		}
		echo $html;
	}

	//Other misc functions
	function isPHPVersionGTEq5_4_0(){
		$phpVersion = phpVersion();
		$phpVersionParts = explode('.', $phpVersion);
		if($phpVersionParts[0]  < 5) { return false; }
		if($phpVersionParts[0]  > 5) { return true;  }
		if($phpVersionParts[1]  < 4) { return false; }
		if($phpVersionParts[1] >= 4) { return true;  }
	}

	function startSessionIfNotStarted(){
		if(isPHPVersionGTEq5_4_0()){
			if (session_status() == PHP_SESSION_NONE) { session_start(); }
		}else{
			if(session_id() == '') { session_start(); }
		}
	}















