<?php
function jsvar($str)
{
	$str = str_replace(chr(13),"",$str);
	$str = str_replace("\n","",$str);
	$str = str_replace("\"","\\\"",$str);
	echo utf8_encode($str);
}

function gt_input($name, $txt, $color_on="#000000", $style="", $value="", $code="", $readonly="", $ival="")
{
	global $location_info;
	$color_off = "#aaaaaa";
	if($value!="")
	{
		$setval = $value;
		$style = "color: $color_on; " . $style;
	}
	else
	{
		$setval = $txt;
		$style = "color: $color_off; " . $style;
	}

	$focus_code = "this.style.color = \"$color_on\"; if(this.value==\"$txt\") this.value = \"\"; ";
	$blur_code = "if(this.value==\"\") {this.value = \"$txt\"; this.style.color = \"$color_off\";} ";

	$setval = str_replace("\"","&quot;",$setval);
	if ($name == "ic_(cat)_title_(item)") {
		$checkOrderTypeIdQuery = lavu_query("SELECT `ordertype_id` FROM `forced_modifiers` WHERE `id` = '[1]' AND `_deleted` != 1 AND `ordertype_id` IS NOT NULL AND `ordertype_id` > 0", $ival['id']);
		if (mysqli_num_rows($checkOrderTypeIdQuery) > 0) {
			$orderTypeId = mysqli_fetch_assoc($checkOrderTypeIdQuery);
			$orderTypesQuery = lavu_query("SELECT `id`, `name` FROM `order_tags` WHERE `_deleted` != '1' AND `active` != '0' AND `name` NOT LIKE '%Lavu ToGo%'");
			$orderTypesList[] = array('Select Order Type', '');
			while ($orderTypesRead = mysqli_fetch_assoc($orderTypesQuery)) {
				$orderTypesList[] = array($orderTypesRead['name'], $orderTypesRead['id']);
			}
			if ($location_info['display_product_code_and_tax_code_in_menu_screen']) {
				$rowStyle = "#1c591c; min-width:220px";
				$tdStyle = "style='min-width:226px;'";
			} else {
				$rowStyle = "#1c591c; min-width:172px";
				$tdStyle = "style='min-width:170px;'";
			}
			$str .= "<td $tdStyle>".gt_select($name, $orderTypesList, $orderTypeId['ordertype_id'], $rowStyle, "select", $ival['id'])."</td>";
			return $str;
		}
	}
	
	$monetary_symbol_attr = "";
	$decimal_char_attr = "";
	$allowdecimals_attr = "";
	$leftright_attr = "";

	if ($txt=='Price') {
		$allowDecimals = $location_info['disable_decimal'];
		$decimalChar = ($location_info['decimal_char'] =='') ? '.' : $location_info['decimal_char'];
		$monetarySymbol = $location_info['monitary_symbol'];
		$leftRight = $location_info['left_or_right'];

		$monetary_symbol_attr = "monetarysymbol='$monetarySymbol'";
		$decimal_char_attr = "decimalchar='$decimalChar'";
		$allowdecimals_attr = "allowdecimals='$allowDecimals'";
		$leftright_attr = "leftright='$leftRight'";
	}

	$str = "<input alt='$txt' title='$txt' type='text' name='$name' id='$name' $leftright_attr  $monetary_symbol_attr $decimal_char_attr $allowdecimals_attr value=\"$setval\" style='$style' onfocus='$focus_code' onblur='$blur_code'".$readonly;
	if($code) $str .= " ".$code;
	$str .= ">";
	return $str;
}

function gt_text($name, $txt, $color_on="#000000", $style="", $value="", $code="") {
	$color_off = "#aaaaaa";

	if($value!="")
	{
		$setval = $value;
		$style = "color: $color_on; " . $style;
	}
	else
	{
		$setval = $txt;
		$style = "color: $color_off; " . $style;
	}

	$focus_code = "this.style.color = \"$color_on\"; if(this.value==\"$txt\") this.value = \"\"; ";
	$blur_code = "if(this.value==\"\") {this.value = \"$txt\"; this.style.color = \"$color_off\";} ";

	$setval = str_replace("\"","&quot;",$setval);
	$str = "<textarea alt='$txt' title='$txt' type='text' name='$name' id='$name' style='$style' rows='3' cols='70' onfocus='$focus_code' onblur='$blur_code'";
	if($code) $str .= " ".$code;
	$str .= ">".$setval."</textarea>";
	return $str;
}

function gt_details($name, $txt, $rowname="",$send_details=false, $extra1="", $extra2="")
{
	global $dialog_iframe_size;
	global $inventory_tablename;
	global $form_name;
	if ($form_name == "") $form_name = "sform";


	$dwidth = $dialog_iframe_size[0];
	$dheight = $dialog_iframe_size[1];

	$send_value = "\"\"";
	if($send_details) $send_value = "document.".$form_name.".".$send_details.".value";
	$str = "";
	//$str .= "<input type='button' value='$txt' onclick='open_special_editor($dwidth, $dheight, \"$name\", $send_value, \"$txt\", document.".$form_name.".".$rowname.".value, \"\", \"\")' style='font-size:10px; background-color:#f4f4ff; border:solid 1px #777777' onmouseover='this.style.backgroundColor = \"#c4c4cf\";' onmouseout='this.style.backgroundColor = \"#f4f4ff\"'>";

	$itemText = speak($txt);
	if (stristr($rowname,'category')) {
		$str .= "<input type='button' value='$itemText' onclick='open_special_editor($dwidth, $dheight, \"$name\", $send_value, \"$itemText\", document.".$form_name.".".$rowname.".value, \"".$extra1."\", \"\")' class='categoryDetails'>";
	} else {
		$str .= "<input type='button' class='itemDetails' value='$itemText' onclick='open_special_editor($dwidth, $dheight, \"$name\", $send_value, \"$itemText\", document.".$form_name.".".$rowname.".value, \"".$extra1."\", \"".$extra2."\")'>";
	}

	return $str;
}

function gt_special($name, $txt, $value = "", $rowname = "", $clr = "#000000", $tbl = "", $field = "", $index = "", $cond = "", $extra_value = "", $extra_value_id = "", $cmrow = array(), $type = '')
{
	global $dialog_iframe_size;
	global $form_name;
	global $inventory_tablename;
	if ($form_name == "") $form_name = "sform";
	//$active_language_pack = sessvar("active_language_pack");
	$active_language_pack = getLanguageDictionary(sessvar("locationid"));

	$dwidth = $dialog_iframe_size[0];
	$dheight = $dialog_iframe_size[1];
	$color_off = "#aaaaaa";
	$combo_style = "display:none";
	$spec_extra = "false";
	$spec_extra2 = "false";
	$combo_flag = $cmrow['combo'];
	//echo $txt;
	if ($field != "")
	{
		$setval = "";
		$sp_query = ers_query("select * from `$tbl` where `$index`='$value'");
		if(mysqli_num_rows($sp_query))
		{
			$sp_read = mysqli_fetch_assoc($sp_query);
			$setval = $sp_read[$field];
		}
	}
	else if ($txt == "Contents")
	{
		$setval = "";
		/*$items = explode(",",$value);
		for($i=0; $i<count($items); $i++)
		{
			$itemid = trim($items[$i]);
			$sp_query = ers_query("select * from `$tbl` where `id`='$itemid'");
			if(mysqli_num_rows($sp_query))
			{
				$sp_read = mysqli_fetch_assoc($sp_query);
				if($setval!="") $setval .= ", ";
				$setval .= $sp_read['name'];
			}
		}*/
		if($value!="") $setval = "Package";
		else $setval = "x " . $extra_value;
		$spec_extra = "document.".$form_name.".".str_replace("contents","qty",$name).".value";
		$spec_extra2 = "document.".$form_name.".".str_replace("contents","type",$name).".value";
	}

	else if ($txt == "Ingredients" || $type == "ingredients")
	{
		if ($combo_flag=="0" || $combo_flag=="")
		{
			$combo_style = "display:block";
		}
		//$spec_extra = $extra_value;
		$ingredientsValue = explode(' xx ', $value);
		if($ingredientsValue[0]=="")
		{
			$setval = "";
		}
		else
		{
			$ivals = explode(",",trim($ingredientsValue[0]));
			$setval = count($ivals);
			if (count($ivals) > 1) $setval .= " ".speak("Ingredients");
			else $setval .= " ".speak("Ingredients");
		}
		$spec_extra2 = ($cond) ? $cond : $spec_extra;
		//error_log("config status: $spec_extra, $spec_extra2");
	}
	else if($txt=="Combo Items" || $type == "comboitems"){
		if($combo_flag==1)
		{
			$combo_style="display:block";
		}
	}
	else if($txt == "Nutrition" || $type == "nutrition")
	{
		$spec_extra2 = '"'. $inventory_tablename .'"';

		$combo_style="display:block";

		if($value=="")
		{
			$setval = "";
		}
		else
		{
			$ivals = explode(",",$value);
			if (count($ivals) > 1) $setval = " ".speak("Nutritions");
			else $setval = " ".speak("Nutritions");
			$setval .= ' ('.count($ivals).')';

		}
	}
	else if($txt == "Dietary Info")
	{
		if($combo_flag==0)
		{
			$combo_style="display:block";
		}

		if($value=="")
		{
			$setval = "";
		}
		else
		{
			$ivals = explode(",",$value);
			$setval = count($ivals);
			if (count($ivals) > 1) $setval .= " ".speak("Dietary Info");
			else $setval .= " ".speak("Dietary Info");
		}
	}
	if($setval!="")
	{
		$style = "color: $clr; ";
	}
	else
	{
		$setval = speak($txt);
		$style = "color: $color_off; ";
	}
	if($extra_value_id=='')
	{
		$extra_value_id = 0;
	}
	$str = "<input type='hidden' name='$name' id='$name' value=\"$value\">";
	//$str .= "<input type='text' name='".$name."_display' id='".$name."_display' readonly style='$style' size='12' onfocus='this.blur(); open_special_editor($dwidth, $dheight, \"$name\", document.".$form_name.".".$name.".value, \"$txt\", document.".$form_name.".".$rowname.".value, $spec_extra, $spec_extra2);' value='$setval'>";
	$str .= "<input type='text' name='".$name."_display' id='".$name."_display' readonly style='$style $combo_style' size='12' onfocus='this.blur(); open_special_editor($dwidth, $dheight, \"$name\", document.".$form_name.".".$name.".value, \"$txt\", document.".$form_name.".".$rowname.".value, $spec_extra, $spec_extra2, $extra_value_id, \"$type\");' value='$setval'>";
	return $str;

	/*if($cond!="") $cond .= " and ";
	$cond .= "_deleted!='1'";
	if($index=="") $index = $field;
	$options = array(array("",""));
	$op_query = ers_query("select * from `$tbl` where $cond");
	while($op_read = mysqli_fetch_assoc($op_query))
		$options[] = array($op_read[$field],$op_read[$index]);
	return gt_select($name,$options,$value,$clr,"Customize");*/
	//$code = "readonly size='12' onclick='open_special_editor($dwidth, $dheight, \"$name\", document.sform.".$name.".value, \"$txt\")'";
	//return gt_input($name, $txt, $clr, "", $value, $code);
}

// @$qtype: if "select_exclusive" will only draw all $options if $value in $options, otherwise will just draw $value as a hidden field
function gt_select($name, $options, $value = "", $clr = "#000000", $qtype, $rowId = "", $cmrow = array())
{
	$b_found = false;
	$disable="";
	$onchange = "";
	$code = "";
	//$code .= "<select name='$name' $disableid='$name' style='color:$clr'>";$disable="";
	$exp_name=explode("_",$name);
	if ($rowId != '') {
		if($exp_name[2]=='modifier' || $exp_name[2]=='forced'){
			$combo_flag = $cmrow['combo'];
			if($combo_flag==1) { $disable="disabled";
			if($exp_name[2]=='forced'){
				$forced_modi=$cmrow['forced_modifier_group_id'];
				$code .= "<input type='hidden' name='$name' value='$forced_modi' />";
			}
			if($exp_name[2]=='modifier'){
				$modifer_hid=$cmrow['modifier_list_id'];
				$code .= "<input type='hidden' name='$name' value='$modifer_hid' />";
			}
			}
		}
	}
	if ($exp_name[3] == 'type' && $exp_name[4] != 'option'){
		$onchange = "onchange='setForcedModifierTypeOption(this.id, this.value)'";
	}
	if ($exp_name[3] == 'type' && $exp_name[4] == 'option'){
		$onchange = "onchange='setForcedModifierType(this.id, this.value)'";
	}
	$code .= "<select name='$name' $disable id='$name' style='color:$clr' $onchange>";
	for($i=0; $i<count($options); $i++)
	{
		if(is_array($options[$i]))
		{
			$option = $options[$i][0];
			$index = $options[$i][1];
		}
		else
		{
			$option = $options[$i];
			$index = $option;
		}
		$code .= "<option value='$index'";

		if($value==$index) {
			$code .= " selected";
			$b_found = TRUE;
		}
		if($index==""||$index=="C") $code .= " style='color:#aaaaaa'";
		$code .= ">$option</option>";
	}

	if (!$b_found && strstr($name, "_col_printer")) {
		$use_opt = "kitchen";
		if ($value > 1) $use_opt .= $value;
		$code .= "<option value='$value' selected>$use_opt (undefined)</option>";
	}

	$code .= "</select>";

	if ($qtype == "select_exclusive" && !$b_found) {
		$code = "<input type='hidden' name='$name' id='$name' value='$value' />";
	}

	return $code;
}

function gt_bool($name,$default_val="0",$current_val,$title) {

	$val = ($current_val == "")?$default_val:$current_val;
	$combo_onclick='';
	$combo_check = explode("_",$name);
	if($combo_check[2]=='combo')
	{
		$combo_onclick = 'combooncheck(this.id,this.checked)';
	}

	$code = $title."<input type='hidden' name='".$name."' id='".$name."' value='".$val."'>";
	//$code .= "<input id='".$name."_cb' type='checkbox' onclick='var setval = this.checked?\"1\":\"0\"; document.getElementById(\"$name\").value = setval; '";
	$code .= "<input id='".$name."_cb' type='checkbox' onclick='var setval = this.checked?\"1\":\"0\"; document.getElementById(\"$name\").value = setval;$combo_onclick; '";
	if ($val=="1") $code .= " checked";
	$code .= ">";

	return $code;
}

function gt_hidden($name,$value="")
{
	$value = str_replace("\"","&quot;",$value);
	return "<input type='hidden' name='$name' id='$name' value=\"$value\">";
}

function gt_picture_grid($name,$value="",$properties=FALSE,$storageKey="") {
	return gt_picture($name,$value,$properties,'_grid',$storageKey);
}

// helper function of gt_picture() and gt_picture_grid()
// $properties is a weakly typed variable and thus needs to be interpretted
function gt_pictures_process_properties($properties, &$set_picture_path, &$extra_pictures, &$extra_misc) {

	if(!$properties)
		return FALSE;

	$set_picture_path = "";
	$extra_pictures = array();
	$extra_misc = array();

	if(is_array($properties))
	{
		$set_picture_path = $properties[0];
		if(count($properties) > 1)
		{
			if(is_array($properties[1]))
			{
				$extra_pictures = $properties[1];
			}
			else if($properties[1]!="")
			{
				$extra_pictures[] = $properties[1];
			}
			if(count($properties) > 2)
			{
				if(is_array($properties[2]))
				{
					$extra_misc = $properties[2];
				}
				else if($properties[2]!="")
				{
					$extra_misc[] = $properties[2];
				}
			}
		}
	}
	else
	{
		$set_picture_path = $properties;
	}

	return TRUE;
}

// helper function of gt_picture() and gt_picture_grid()
// verifies the path of a picture, setting it to empty string and updating the database if necessary
function gt_picture_verify_picture_path(&$value, $set_picture_path) {
	if($value != "")
	{
		//echo "/mnt/poslavu-files" . $set_picture_path . $value . ": ";
		if(!file_exists("/mnt/poslavu-files" . $set_picture_path . $value) || is_dir("/mnt/poslavu-files" . $set_picture_path . $value))
		{
			/**
			 * Updates Entries Where Images Don't Actually Exist.
			 */
			if(stristr($set_picture_path, "items") !== FALSE){
				#lavu_query("UPDATE `menu_items` SET `image`='' WHERE `image` = '[1]'", $value);
			}else if(stristr($set_picture_path, "categories") !== FALSE){
				#lavu_query("UPDATE `menu_categories` SET `image`='' WHERE `image` = '[1]'", $value);
			}
			$value = "";
		}
	}
}

function gt_picture($name,$value="",$properties=false,$function_extension='',$storageKey="")
{
	// get some globals
	global $dialog_iframe_size;
	global $form_name;

	// check the form name
	if ($form_name == "") $form_name = "sform";

	// check the properties input
	$set_picture_path = ""; $extra_pictures = array(); $extra_misc = array();
	if (!gt_pictures_process_properties($properties, $set_picture_path, $extra_pictures, $extra_misc))
		return "";

	$dwidth = $dialog_iframe_size[0];
	$dheight = $dialog_iframe_size[1];

	$displayname = $name . "_display";

	$extras_code = "";
	if(count($extra_pictures) > 0)
	{
		$name2 = str_replace("image",$extra_pictures[0],$name);
		$extras_code .= ", document.".$form_name.".".$name2.".value";
		if(count($extra_pictures) > 1)
		{
			$name3 = str_replace("image",$extra_pictures[1],$name);
			$extras_code .= ", document.".$form_name.".".$name3.".value";

			if(count($extra_misc) > 0)
			{
				$name_misc = str_replace("image",$extra_misc[0],$name);
				$extras_code .= ", document.".$form_name.".".$name_misc.".value";
			}
		}
		//$str .= "<table><td width='16' height='16' style='border:solid 1px #777777; cursor:pointer' align='center' valign='middle' onclick='open_picture_editor($dwidth, $dheight, \"$name\", document.sform.".$name.".value, document.sform.".$name2.".value, document.sform.".$name3.".value, document.sform.".$name_misc.".value)' id='$displayname' name='$displayname'>";
	}

	// verifies the path of a picture, setting it to empty string and updating the database if necessary
	gt_picture_verify_picture_path($value, $set_picture_path);

	$str = "";
	if ($function_extension == "") {
		$function_name = "open_picture_editor";
	} else if ($function_extension == "_grid") {
		$function_name = "open_picture_editor_grid";
	} else {
		$function_name = "open_picture_editor{$function_extension}";
	}
	if($storageKey != ""){
		$set_picture_path = getenv('S3IMAGEURL');
		$imagePath = $set_picture_path.$storageKey;
		$value = $storageKey;
	}else{
		$imagePath = "$set_picture_path/$value";
	}
	$str .= "<table><td width='16' height='16' class='menuImg' align='center' valign='middle' onclick='{$function_name}(\"$set_picture_path\", $dwidth, $dheight, \"$name\", document.".$form_name.".".$name.".value".$extras_code.")' id='$displayname' name='$displayname'>";

	$str .= "<img src='$imagePath' width='16' height='16'/>";
		
	$str .= "</td></table>";
	$str .= "<input type='hidden' name='$name' id='$name' value='$value'>";
	$str .= "<input type='hidden' name='previous_value_$name' id='previous_value_$name' value='$value'>";
	return $str;
}
?>
