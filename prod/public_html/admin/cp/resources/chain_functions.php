<?php
	require_once('/home/poslavu/public_html/admin/sa_cp/billing/payment_profile_functions.php');

	function getChainReportingGroups($dataname, $company_info = false) {

		$chainGroups = array();

		if (!$company_info) $company_info = getCompanyInfo($dataname, "data_name");

		if ($company_info['chain_id']!="" && is_numeric($company_info['chain_id'])) {

			$chain_id = $company_info['chain_id'];

			$userName = sessvar('admin_username');

			$userTable =  'poslavu_' . $company_info['data_name']  . '_db' . ".users"; 

			$rawSQL = <<<DEFAULTSQL
			 SELECT * FROM poslavu_MAIN_db.chain_reporting_groups WHERE chainid = '{$chain_id}' AND _deleted !=1
DEFAULTSQL;

		    $userGrpSql =  "SELECT reporting_groups FROM {$userTable} WHERE username = '[1]' "; 

			$resRepGrp =  mlavu_query($userGrpSql, $userName);

			$userRepGrp = mysqli_fetch_assoc($resRepGrp);


			if (!empty($userRepGrp['reporting_groups'])) {

			$rawSQL = <<<USERGRPSQL
			SELECT chain_report_group.*
			    FROM(
			        SELECT reporting_groups FROM {$userTable} WHERE username = '{$userName}'
			    ) AS user_reporting_group

			INNER JOIN
			(
			    SELECT * FROM poslavu_MAIN_db.chain_reporting_groups WHERE chainid = '{$chain_id}' AND _deleted !=1
			) AS chain_report_group

			ON user_reporting_group.reporting_groups = chain_report_group.id
USERGRPSQL;

			}

		    $groupQuery = mlavu_query($rawSQL);


			if(mysqli_num_rows($groupQuery)) {
				while($groupRead = mysqli_fetch_assoc($groupQuery))	{
					$chainGroups[] = $groupRead;
				}
			}
		}
		return $chainGroups;
	}

	function getChainAccounts($dataname, $company_info=false, $get_chains=true) {

		if (!$company_info) $company_info = getCompanyInfo($dataname, "data_name");

		$chain_accounts = array();
		if ($company_info['disabled']=="0") $chain_accounts[] = $company_info;
		if ($company_info && $get_chains) {
			if ($company_info['chain_id']!="" && is_numeric($company_info['chain_id'])) {
				//$get_chain_accounts = mlavu_query("SELECT `poslavu_MAIN_db`.`restaurants`.`id` AS `id`, `poslavu_MAIN_db`.`restaurants`.`data_name` AS `data_name`, `poslavu_MAIN_db`.`restaurants`.`company_name` AS `company_name`, `poslavu_MAIN_db`.`restaurants`.`company_code` AS `company_code`, `poslavu_MAIN_db`.`restaurants`.`logo_img` AS `logo_img`, `poslavu_MAIN_db`.`restaurants`.`receipt_logo_img` AS `receipt_logo_img`, `poslavu_MAIN_db`.`restaurants`.`dev` AS `dev`, `poslavu_MAIN_db`.`restaurants`.`demo` AS `demo`, `poslavu_MAIN_db`.`restaurants`.`disabled` AS `disabled`, `poslavu_MAIN_db`.`restaurants`.`chain_id` AS `chain_id`, `poslavu_MAIN_db`.`restaurants`.`lavu_lite_server` AS `lavu_lite_server`, `poslavu_MAIN_db`.`restaurants`.`version_number` AS `version_number`, `poslavu_MAIN_db`.`payment_status`.`force_allow_tabs_n_tables` AS `allow_tnt_mode` FROM `poslavu_MAIN_db`.`restaurants` LEFT JOIN `poslavu_MAIN_db`.`payment_status` ON `poslavu_MAIN_db`.`payment_status`.`restaurantid` = `poslavu_MAIN_db`.`restaurants`.`id` WHERE `poslavu_MAIN_db`.`restaurants`.`chain_id` = '[1]' AND `poslavu_MAIN_db`.`restaurants`.`data_name` != '[2]' AND `poslavu_MAIN_db`.`restaurants`.`disabled` = '0' ORDER BY `poslavu_MAIN_db`.`restaurants`.`company_name`", $company_info['chain_id'], $dataname);
				$get_chain_accounts = mlavu_query("SELECT `id`, `data_name`, `company_name`, `company_code`, `logo_img`, `receipt_logo_img`, `dev`, `demo`, `disabled`, `chain_id`, `lavu_lite_server`, `version_number` FROM `poslavu_MAIN_db`.`restaurants` WHERE `chain_id` = '[1]' AND `data_name` != '[2]' AND `disabled` = '0' ORDER BY `company_name`", $company_info['chain_id'], $dataname);
				if (mysqli_num_rows($get_chain_accounts) > 0) {
					while ($chain_account = mysqli_fetch_assoc($get_chain_accounts)) {
						$chain_accounts[] = $chain_account;
					}
				}
			}
		}

		return $chain_accounts;
	}

	function getChainLocations($dataname, $company_info=false, $get_chains=true) {
		$locations = array();
		$chain_accounts = getChainAccounts($dataname, $company_info, $get_chains);
		for($i=0; $i<count($chain_accounts); $i++)
		{
			$rdb = "poslavu_" . $chain_accounts[$i]['data_name'] . "_db";
			$account_arr = array();
			foreach($chain_accounts[$i] as $ckey => $cval)
			{
				$account_arr['r_' . $ckey] = $cval;
			}

			$chain_query = mlavu_query("select * from `[1]`.`locations`",$rdb);
			while($chain_read = mysqli_fetch_assoc($chain_query))
			{
				//$chain_read['data_name'] = $chain_accounts[$i]['data_name'];
				$locations[] = array_merge($account_arr,$chain_read);
			}
		}
		return $locations;
	}

	function admin_getChainLocations() {
		return getChainLocations(admin_info('dataname'), false, (admin_info('access_level')>=4));
	}

	function getChainLocationSelector($locationid, $reload_language_pack, &$location_info)
	{
		if (empty($locationid))
		{
			return;
		}

		$company_info = false;
		if (strstr($locationid, "-"))
		{
			$split_loc_id = explode("-", $locationid);
			$locationid = $split_loc_id[1];
			if ($split_loc_id[0] != admin_info('companyid'))
			{
				$company_info = changeActiveChainAccount($split_loc_id[0], $locationid);
				set_sessvar("locationid", $locationid);
				$a_gets = array();
				foreach($_GET as $k => $v)
				{
					$a_gets[] = $k.'='.$v;
				}
				$s_location = 'index.php?'.implode("&", $a_gets);
				echo "<script type='text/javascript'>/*".__FILE__."*/ setTimeout(function(){ window.location='".$s_location."'; }, 100);</script>";
			}
		}

		// if location cannot be found then set location to false so it will be auto set
		$location_query = lavu_query("SELECT * FROM `locations` WHERE `id` = '[1]' AND `_disabled` != '1'", $locationid);
		if (mysqli_num_rows($location_query) < 1)
		{
			$locationid = false;
		}

		//$chain_accounts = getChainAccounts(admin_info('dataname'), $company_info, (admin_info('access_level')>=4)); // get all chains only with access level 4 or higher
		require_once('/home/poslavu/public_html/admin/sa_cp/manage_chain_functions.php');
		$chain_accounts = CHAIN_USER::get_user_restaurants_access();

		$root_dataname 		= admin_info("root_dataname");
		$users_login_id		= admin_info("loggedin");
		$use_language_pack	= -1;
		$loc_select			= "";

		if (!empty($root_dataname) && !empty($users_login_id))
		{
			$get_user_language_pack = mlavu_query("SELECT `use_language_pack` FROM `poslavu_".$root_dataname."_db`.`users` WHERE `id` = '[1]' AND `_deleted` != '1'", $users_login_id);
			if ($get_user_language_pack && mysqli_num_rows($get_user_language_pack)>0)
			{
				$user_info = mysqli_fetch_assoc($get_user_language_pack);
				$use_language_pack = (int)$user_info['use_language_pack'];
			}
		}

		foreach ($chain_accounts as $account)
		{
			$data_name = $account['data_name'];

			if (!empty($data_name))
			{
				$location_query = mlavu_query("SELECT * FROM `poslavu_".$data_name."_db`.`locations` WHERE `_disabled` != '1' ORDER BY `title` ASC");
				
				if($location_query) {
					while ($location_read = mysqli_fetch_assoc($location_query)) {
						if (admin_info("loc_id") == $location_read['id'] || admin_info("loc_id") == "0" || admin_info("access_level") == "4") {
							if (!$locationid || (admin_info("loc_id") != "0" && admin_info("access_level") != "4")) // auto set location if there is none selected or if user access level is not 4
							{
								$locationid = $location_read['id'];
							}

							$loc_select .= "<option value='" . $account['id'] . "-" . $location_read['id'] . "'";

							if ($account['id'] == admin_info('companyid') && $location_read['id'] == $locationid) {
								$loc_select .= " selected";
								$location_info = $location_read;
								$location_gateway = $location_read['gateway'];

								if ($use_language_pack < 0) {
									$use_language_pack = (int)$location_info['use_language_pack'];
								}
								$get_location_config = mlavu_query("SELECT `setting`, `value` FROM `poslavu_" . $account['data_name'] . "_db`.`config` WHERE `type` = 'location_config_setting' AND `location` IN ('[1]', '[2]')", $locationid, admin_info("companyid"));
								while ($config_info = mysqli_fetch_assoc($get_location_config)) {
									$location_info[$config_info['setting']] = $config_info['value'];
								}

								set_sessvar("location_info", $location_info);
								set_sessvar("locationid", $location_info['id']);
							}

							$loc_select .= ">" . $location_read['title'] . ((count($chain_accounts) > 1) ? " (#" . ($account['id'] + 100000) . ")" : "") . "</option>";
						}
					}
				}
			}
		}

		global $active_language_pack_id;
		global $active_language_pack;

		if (reqvar("locationid")) {
			unset_sessvar("active_language_pack");
		}
		if (!$active_language_pack_id)
		{
			$active_language_pack_id = getActiveLanguagePackId($users_login_id, $locationid);
			//$active_language_pack_id = sessvar("active_language_pack_id");
		}

		//$session_language_pack = sessvar("active_language_pack");
		$session_language_pack = getLanguageDictionary($locationid);
		$reload_language_pack = ($use_language_pack != $active_language_pack_id);
		$active_language_pack_id = ($reload_language_pack) ? $use_language_pack : $active_language_pack_id;

		if ($session_language_pack && !$reload_language_pack)
		{
			$active_language_pack = $session_language_pack;
		}
		else if ($use_language_pack >= 0)
		{
			$active_language_pack = getLanguageDictionary($locationid);
			if (empty($active_language_pack) || $reload_language_pack) {
				unset_sessvar("active_language_pack");
				$active_language_pack = getLanguageDictionary($locationid);
				if (empty($active_language_pack)) {
					$active_language_pack = load_language_pack($active_language_pack_id, $locationid);
					setLanguageDictionary($active_language_pack, $locationid);
				}
			}
			//set_sessvar("active_language_pack", $active_language_pack);

			$active_language_pack_id = $use_language_pack;
			setActiveLanguagePackId($use_language_pack, $users_login_id, $locationid);
			//set_sessvar("active_language_pack_id", $use_language_pack);
		}

		return $loc_select;
	}

	function getPrimaryChainRestaurant($chainID){
        $PrimaryRestQuery = "SELECT primary_restaurantid FROM `poslavu_MAIN_db`.`restaurant_chains` where `id` = '".$chainID."'";
        $getPrimaryRest = mlavu_query($PrimaryRestQuery);
        if(mysqli_num_rows($getPrimaryRest) >0){
            $chain_info = mysqli_fetch_assoc($getPrimaryRest);
            if(isset($chain_info['primary_restaurantid'])){
                return $chain_info['primary_restaurantid'];
            }
            else if(isset($chain_info[0]['primary_restaurantid'])){
                return $chain_info[0]['primary_restaurantid'];
            }
        }
    }

    function getDisplayableCurrencyInfoFromCurrencyId($currencyId){
	    $query = "SELECT `name`, `alphabetic_code` FROM `poslavu_MAIN_db`.`country_region` WHERE `id`=$currencyId LIMIT 1";
	    $query_returned = mlavu_query($query);
	    $row = mysqli_fetch_assoc($query_returned);
	    $name = $row['name'];
	    $code = $row['alphabetic_code'];
	    return formatEncodeCountryCodeWithName($code,$name);
    }

	function getDisplayableCurrencyNameOnlyFromCurrencyId($currencyId){
		$query = "SELECT `name` FROM `poslavu_MAIN_db`.`country_region` WHERE `id`=$currencyId LIMIT 1";
		$query_returned = mlavu_query($query);
		$row = mysqli_fetch_assoc($query_returned);
		return formatEncodeNameOnly($row['name']);
	}
	function formatEncodeNameOnly($name){
		$encoding = "UTF-8";
		$html_encoding  = "HTML-ENTITIES";
		$name = mb_convert_encoding($name, $encoding);
		$name = mb_convert_encoding($name, $html_encoding,$encoding);
		return $name;
	}

	function formatEncodeCountryCodeWithName($code, $name){
		$encoding = "UTF-8";
		$html_encoding  = "HTML-ENTITIES";
		$code = mb_convert_encoding($code, $encoding);
		$code = mb_convert_encoding($code,$html_encoding, $encoding);
		$name = mb_convert_encoding($name, $encoding);
		$name = mb_convert_encoding($name, $html_encoding,$encoding);
		return  "[".$code."] " . $name;
	}

	function getCompanyInfo($info, $field) {

		$company_info = false;
		$get_company_info = mlavu_query("SELECT `id`, `data_name`, `company_name`, `company_code`, `logo_img`, `receipt_logo_img`, `dev`, `demo`, `disabled`, `chain_id`, `lavu_lite_server`, `version_number` FROM `poslavu_MAIN_db`.`restaurants` WHERE `[1]` = '[2]' LIMIT 1", $field,$info);
		if (mysqli_num_rows($get_company_info) > 0) {
			$company_info = mysqli_fetch_assoc($get_company_info);
		}

		return $company_info;
	}

	function changeActiveChainAccount($to_companyid, $loc_id = 1)
	{
		$company_info = getCompanyInfo($to_companyid, "id");

		if ($company_info)
		{
			$from_company_name = admin_info('company_name');
			$from_dataname = admin_info('dataname');
			logChangeActiveChainAccount("To", $company_info['company_name'], $company_info['data_name']);

			set_sessvar("admin_dataname",$company_info['data_name']);
			set_sessvar("admin_database","poslavu_".$company_info['data_name']."_db");
			set_sessvar("admin_companyid",$company_info['id']);
			set_sessvar("admin_chain_id",$company_info['chain_id']);
			set_sessvar("admin_version",$company_info['version_number']);
			set_sessvar("admin_dev",$company_info['dev']);
			set_sessvar("admin_company_name",$company_info['company_name']);
			set_sessvar("admin_loc_id",$loc_id);

			$set_username = admin_info('username');
			if (strstr($set_username, ":"))
			{
				$split_un = explode(":", $set_username);
				$set_username = $split_un[1];
			}

			if ($company_info['id'] != admin_info('root_company_id'))
			{ 
				$set_username = $company_info['data_name'].":".$set_username;
			}

			set_sessvar("admin_username",$set_username);

			logChangeActiveChainAccount("From", $from_company_name, $from_dataname);
		}

		return $company_info;
	}

	function logChangeActiveChainAccount($to_or_from, $cname, $dname)
	{
		$l_vars = array();
		$q_fields = "";
		$q_values = "";
		$l_vars['action'] = "Switched Chain Account";
		$l_vars['user'] = admin_info('username');
		$l_vars['user_id'] = admin_info('loggedin');
		$l_vars['ipaddress'] = $_SERVER['REMOTE_ADDR'];
		$l_vars['order_id'] = "0";
		$l_vars['data'] = admin_info('fullname');
		$l_vars['detail1'] = "Access level: ".admin_info('access_level');
		$l_vars['detail2'] = $to_or_from." ".$cname." (".$dname.")";
		$l_vars['loc_id'] = sessvar("locationid");
		$keys = array_keys($l_vars);
		foreach ($keys as $key) {
			if ($q_fields != "") { $q_fields .= ", "; $q_values .= ", "; }
			$q_fields .= "`$key`";
			$q_values .= "'[$key]'";
		}
		$data_name = admin_info('dataname');
		$l_vars['dataname'] = $data_name;
		$log_this = mlavu_query("INSERT INTO `poslavu_[dataname]_db`.`admin_action_log` ($q_fields, `server_time`) VALUES ($q_values, now())", $l_vars);

		if ($to_or_from == "To") $update_last_activity = mlavu_query("UPDATE `poslavu_MAIN_db`.`restaurants` SET `last_activity` = now() WHERE `data_name` = '[1]'", $dname);
	}

	// checks that the currently logged in dataname can access the given restaurant id
	// @return: TRUE if access is allowed, redirects to index.php?locationid=$dn_loggedin_id if access is not allowed
	function verifyCanAccessRestaurantID($dn_loggedin, $dn_admininfo, $restaurant_id) {
		// sanitize the input
		if (strpos((string)$restaurant_id, '-') !== FALSE) {
			$restaurant_id = explode('-', $restaurant_id);
			$restaurant_id = $restaurant_id[0];
		}

		// check if the datanames are equal or that an admin is logged in
		//if (admin_loggedin())
		//	return TRUE;

		if ($dn_loggedin == '') {
			error_log('Dataname not logged in');
			//header("Location: login.php");
			//exit();
		}

		// get the location id of $dn_loggedin
		$a_loggedin_restaurant = get_restaurant_from_dataname($dn_loggedin);
		$s_loggedin_restaurant_id = $a_loggedin_restaurant['id'];
		$s_redirect = "Location: index.php?locationid=$s_loggedin_restaurant_id";
		if ($s_loggedin_restaurant_id == (string)$restaurant_id)
			return TRUE;

		// check that the restaurant exists
		$a_restaurant = get_restaurant_from_id($restaurant_id);
		if (count($a_restaurant) == 0) {
			error_log("User trying to access a restaurant that doesn't exist (restaurant id $restaurant_id)");
			header($s_redirect);
			exit();
		}

		// get the restaurants this user has access to
		require_once('/home/poslavu/public_html/admin/sa_cp/manage_chain_functions.php');
		$a_restaurants = CHAIN_USER::get_user_restaurants_access();

		// check that $dn_loggedin is a part of the same chain as $restaurant_id
		$b_found_dn = FALSE;
		foreach($a_restaurants as $a_other_rest) {
			if ($a_other_rest['data_name'] == $a_restaurant['data_name']) {
				$b_found_dn = TRUE;
				break;
			}
		}
		if (!$b_found_dn) {
			error_log("User trying to access restaurant that it doesn't belong to (username ".admin_info('username').", restaurant id $restaurant_id)");
			header($s_redirect);
			exit();
		}

		return TRUE;
	}

	// Checks if the chain was just changed with $_GET['locationid'] and if $_GET['set_groupid'] is set.
	// If so, tries to match the menu_groupid of $old_dn with $new_rest_id and does a header redirect.
	function checkSetGroupid($old_dn, $new_rest_id) {
		if (!isset($_GET['mode']) || !isset($_GET['locationid']) || !isset($_GET['set_groupid']) ||
			$_GET['mode'] != 'edit_menu')
			return;

		$group_query = mlavu_query("SELECT `group_name` FROM `poslavu_[dataname]_db`.`menu_groups` WHERE `id`=[group_id]",
			array('dataname'=>$old_dn, 'group_id'=>$_GET['set_groupid']));
		if ($group_query === FALSE || mysqli_num_rows($group_query) === 0)
			return;
		$group_read = mysqli_fetch_assoc($group_query);
		$group_name = $group_read['group_name'];

		$a_restaurant = get_restaurant_from_id($new_rest_id);
		if (count($a_restaurant) == 0)
			return;
		$matching_query = mlavu_query("SELECT `id` FROM `poslavu_[dataname]_db`.`menu_groups` WHERE `group_name`='[group_name]'",
			array('dataname'=>$a_restaurant['data_name'], 'group_name'=>$group_name));
		if ($matching_query === FALSE || mysqli_num_rows($matching_query) === 0) {
			$matching_id = 1;
		} else {
			$matching_read = mysqli_fetch_assoc($matching_query);
			$matching_id = $matching_read['id'];
		}

		if ($matching_id !== $_GET['set_groupid']) {
			$_GET['set_groupid'] = $matching_id;
			$s_redirect = 'Location: index.php?';
			$a_redirect = array();
			foreach($_GET as $k=>$v)
				$a_redirect[] = $k.'='.$v;
			$s_redirect .= implode('&', $a_redirect);
			header($s_redirect);
		}
	}


	//Get all chain locations, then change to them and execute a query on all locations in the chain.
	function updateChainLocationDatabases($chainId, $customUnits) {
		require_once('/home/poslavu/public_html/admin/sa_cp/manage_chain_functions.php');
		$currentLocation = sessvar("admin_companyid");
		$currentDataname = sessvar("admin_dataname");
		addCustomUnitsToChainAccount ($chainId, $customUnits, $currentDataname);
		changeActiveChainAccount($currentLocation);
		lavu_connect_byid($currentLocation);
    }

    /**
     * This function is used to get chain location details with their respective region for access level 4 user.
     * @param $dataname location dataname
     * @param $chainId location chain id
     * @param $regions Specify region to get their locations
     * @return array location details and locations with region
     */
    function getRegionWiseChainLocations($dataname, $chain_id = '', $regions)
    {
        $chainReportingGroups = array();
        $reportLocations = array();
        if ($chain_id == '') {
            $company_info = getCompanyInfo($dataname, "data_name");
            if ($company_info['chain_id'] != "" && is_numeric($company_info['chain_id'])) {
                $chain_id = $company_info['chain_id'];

            }
        }
        $locationChainGroups = getChainReportingGroups($dataname);
        foreach($locationChainGroups as $chainData) {
            if (in_array($chainData['name'], $regions)) {
                $locationDetails = array();
                $locations = array();
                if ($chainData['contents'] != "" && (admin_info('access_level')>=4)) {
                    $chain_restaurant_query = mlavu_query("select * from `poslavu_MAIN_db`.`restaurants` where `chain_id`='[1]' and id in ([2])", $chain_id, $chainData['contents']);
                    while ($chain_restaurant_read = mysqli_fetch_assoc($chain_restaurant_query)) {
                        if ($chain_restaurant_read['data_name'] != "") {
                            $locationDetail['company_id'] = $chain_restaurant_read['id'];
                            $locationDetail['company_name'] = $chain_restaurant_read['company_name'];
                            $locationDetail['data_name'] = $chain_restaurant_read['data_name'];
                            $locationDetails[] = $locationDetail;
                            $locations[$locationDetail['data_name']] = $locationDetail['data_name'];
                        }
                    }

                    $reportLocations[$chainData['name']] = $locations;
                    $chainReportingGroups[$chainData['name']] = $locationDetails;
                }
            }
        }

        return array($chainReportingGroups, $reportLocations);//Location detail with region, Locations with region
    }

	function getCountryCodeList() {
		$result = mlavu_query("SELECT * FROM `poslavu_MAIN_db`.`countries` WHERE code != '' ORDER BY `name`");
		$countryCodes = array();
		if(mysqli_num_rows($result)>0) {
			while ($region_info = mysqli_fetch_assoc($result)) {
				$countryCodes[] = $region_info;
			}
		}
		return $countryCodes;
	}

	function getCountryListOption() {
		$countryCodeList = getCountryCodeList();
		$countryOption = "";
		if (!empty($countryCodeList)) {
			foreach ($countryCodeList as $countryInfo) {
				$displayedValue = formatEncodeCountryCodeWithName($countryInfo['code'], $countryInfo['name']);
				$value = $countryInfo['name']."_".$countryInfo['code'];
				$countryOption .= "<option value='".$value."'>".$displayedValue."</option>";
			}
		}
		return $countryOption;
	}


	//LP-8191 & subtasks LP-8192, LP-8295
	// Things we need to know:
	// 1.) Is dataname primary
	// 2.) Is dataname primary with table sync abilities over other datanames?
	// Things we want to do:
	// 1.) Sync primary dataname with sync capabilities across all child accouts.
	

	/** 
	*	Returns an array with key value map:
	*	restaurant_id => (int) restaurant_id (from poslavu_MAIN_restaurants)
	*   chain_id => (int) chain_id ( or 0 if not part of a chain )
	*   primary_restaurant_id => (int) primary_restaurant_id (or 0 if not part of a chain)
	*
	*   returns false on error.
	*/ 
	function getRestaurantIDChainIDAndPrimaryID($dataname){
		$datanameChainQuery = <<<DATANAME_CHAIN_QUERY
			select
			`poslavu_MAIN_db`.`restaurants`.`id` as `restaurant_id`,
			`poslavu_MAIN_db`.`restaurants`.`chain_id` as `chain_id`,
			`poslavu_MAIN_db`.`restaurant_chains`.`primary_restaurantid` as `primary_restaurant_id`
			FROM `poslavu_MAIN_db`.`restaurants` 
			LEFT JOIN `poslavu_MAIN_db`.`restaurant_chains` ON 			`poslavu_MAIN_db`.`restaurants`.`chain_id`=`poslavu_MAIN_db`.`restaurant_chains`.`id`
			WHERE `poslavu_MAIN_db`.`restaurants`.`data_name`='[1]';
DATANAME_CHAIN_QUERY;
		$chainInfoResult = mlavu_query($datanameChainQuery, $dataname);
		$chainInfoRow = mysqli_fetch_assoc($chainInfoResult);
		if(!is_array($chainInfoRow)){
			return false;
		}
		$chainInfoRow['restaurant_id'] = intval($chainInfoRow['restaurant_id']);
		$chainInfoRow['chain_id'] = intval($chainInfoRow['chain_id']);
		$chainInfoRow['primary_restaurant_id'] = intval($chainInfoRow['primary_restaurant_id']);

		return $chainInfoRow;
	}


	function getSyncTablesConfigSettings($dataname){
		$result = mlavu_query("SELECT `id`,`setting`,`value` FROM `poslavu_[1]_db`.`config` WHERE `setting`='sync_primary_of_chain_table_settings'", $dataname);
		if(!$result || !mysqli_num_rows($result)){
			return false;
		}
		$row = mysqli_fetch_assoc($result);
		$deserialized = json_decode($row['value'],1);
		if(empty($deserialized) || !is_array($deserialized)){
			error_log("Function:".__FUNCTION__.' inside:'.__FILE__.' could not json deserialize value:'.$row['value']);
		}
		if(!is_array($deserialized) || empty($deserialized)){
			return false;
		}
		return $deserialized;
	}

	function getRestaurantRowsOfChain($chain_id){
		$result = mlavu_query("SELECT `id`,`data_name` FROM `poslavu_MAIN_db`.`restaurants` WHERE `chain_id`='[1]'", $chain_id);
		if(!$result || !mysqli_num_rows($result)){
			return false;
		}
		$loadedArr = array();
		while($curr = mysqli_fetch_assoc($result)){
			$loadedArr[] = $curr;
		}
		return $loadedArr;
	}

	function getRestaurantRowsOfChainWithLocationTitle($chain_id){
		$restaurantInfo = getRestaurantRowsOfChain($chain_id);
		$restaurantInfoWithLocation = array();
		foreach($restaurantInfo as $currRestaurantRow){
			$result = mlavu_query("SELECT `title` FROM `poslavu_[1]_db`.`locations` LIMIT 1", $currRestaurantRow['data_name']);
			$locationRow = mysqli_fetch_assoc($result);
			if(is_array($locationRow) && !empty($locationRow)){
				$currRestaurantRow['title'] = $locationRow['title'];
			}
			$restaurantInfoWithLocation[] = $currRestaurantRow;
		}
		return $restaurantInfoWithLocation;
	}


	/**
	*	IF 
	*   1.) dataname is in a chain (and)
	*   2.) dataname is primary of a chain (and)
	*   3.) dataname has config setting `sync_table_primary_2_chains_cp_enable`
	*   THEN RETURNS an array with key value pairs:
	*   1.) primary_id => the id of the dataname in the restaurants table (restaurant id) that is proved primary
	*   2.) primary_dataname => the dataname that was passed and proved to be primary
	*   3.) children_restaurants => array of children datanames who's listed tables may be overwritten by this dataname.
	*   4.) sync_allowed_tables => numeric array of table names that are allowed to sync to children
	*   ELSE:
	*   returns FALSE; (has no sync permissions)
	*/
	function getTableSyncPermissionsAndInfo($dataname){
		static $returnPackageArr = null; //Hold in ram, just in case called again.  No bother re-querying so much information.
		if(!empty($returnPackageArr)){
			return $returnPackageArr;
		}

		$datanamesChainInfo = getRestaurantIDChainIDAndPrimaryID($dataname);

		// If query failed / or restaurant info not present (which should never happen)
		if(empty($datanamesChainInfo)){ 
			return false; 
		}

		// If not part of a chain
		if(empty($datanamesChainInfo['chain_id'])){
			return false;
		}
		// If not the primary of the chain account
		if($datanamesChainInfo['restaurant_id'] != $datanamesChainInfo['primary_restaurant_id']){
			return false;
		}

		//Get config setting for tables that may sync
		$tableSyncConfigSettings = getSyncTablesConfigSettings($dataname);

		//Verify has tables to sync.
		if(empty($tableSyncConfigSettings) || 
		   empty($tableSyncConfigSettings['sync_allowed_tables']) ||
		   !is_array($tableSyncConfigSettings['sync_allowed_tables'])){
				return false;
		}

		//Get the children the account may sync to.
		$restaurantRowsOfChain = getRestaurantRowsOfChainWithLocationTitle($datanamesChainInfo['chain_id']);

		//Order the locations by their restaurant id, except the primary is always the first.
		$primaryRow = null;
		$orderedLocations = array();
		for($i = 0; $i < count($restaurantRowsOfChain); $i++){
			if($restaurantRowsOfChain[$i]['id'] == $datanamesChainInfo['primary_restaurant_id']){
				$primaryRow = $restaurantRowsOfChain[$i];
				$primaryRow['is_primary'] = true;
			}else{
				$restaurantRowsOfChain[$i]['is_primary'] = false;
				$orderedLocations[] = $restaurantRowsOfChain[$i];
			}
		}
		$orderedLocations = array_merge(array($primaryRow), $orderedLocations);
		$orderedLocations = array_values($orderedLocations);
		if(!$primaryRow || !is_array($orderedLocations) || empty($orderedLocations)){
			return false;
		}

		//Get the row count of the sync tables
		$rowCountDatanameByTable2DArr = array();
		foreach($orderedLocations as $currLocationInfo){
			$currDataname = $currLocationInfo['data_name'];
			$rowCountDatanameByTable2DArr[$currDataname] = array();
			foreach($tableSyncConfigSettings['sync_allowed_tables'] as $currTable){
				$currTableTrimmed = trim($currTable);
				if(empty($currTableTrimmed)){
					continue;
				}
				$result = mlavu_query("SELECT COUNT(*) as count FROM `[1]`.`[2]`",'poslavu_'.$currDataname."_db", $currTable);
				if(!$result){
					error_log("Error in ".__FILE__." check sync_primary_of_chain_table_settings in config of dataname:".$currDataname);
					return false;
				}
				$resultRow = mysqli_fetch_assoc($result);
				$rowCountDatanameByTable2DArr[$currDataname][$currTable] = $resultRow['count'];
			}
		}

		//Prepare return data structure.
		$returnPackageArr = array();
		$returnPackageArr['primary_id'] = $datanamesChainInfo['primary_restaurant_id'];
		$returnPackageArr['primary_dataname'] = $dataname;
		$returnPackageArr['datanames_in_chain'] = $orderedLocations;
		$returnPackageArr['sync_allowed_tables'] = $tableSyncConfigSettings['sync_allowed_tables'];
		$returnPackageArr['account_table_row_counts'] = $rowCountDatanameByTable2DArr;
		return $returnPackageArr;
	}

	function getTableSyncPermissionsAndInfoOfLocationsPrimary($dataname){
		

		$chainInfo = getRestaurantIDChainIDAndPrimaryID($dataname);
		if(empty($chainInfo)){
			return false;
		}
		$primaryID = $chainInfo['primary_restaurant_id'];
		$primaryDatanameQuery = "SELECT `id`,`data_name` FROM `poslavu_MAIN_db`.`restaurants` WHERE `id`='[1]'";
		$result = mlavu_query($primaryDatanameQuery, $primaryID);
		if(!$result || mysqli_num_rows($result) == 0){
			return false;
		}
		$restaurantRow = mysqli_fetch_assoc($result);
		return getTableSyncPermissionsAndInfo($restaurantRow['data_name']);
	}


	function syncTableToAllChildAccounts($parentDataname, $tableName){
		$syncPermissionsOnParent = getTableSyncPermissionsAndInfo($parentDataname);
		$datanamesInChain = $syncPermissionsOnParent['datanames_in_chain'];
		$locationSuccesses = array();
		$locationFails = array();
		foreach($datanamesInChain as $currLocation){
			$currDataname = $currLocation['data_name'];
			if($currDataname == $parentDataname){
				continue;
			}
			$success = syncTableToChildAccount($parentDataname, $currDataname, $tableName);
			if($success){
				$locationSuccesses[] = $currDataname;
			}else{
				$locationFails[] = $currDataname;
			}
		}
		return array("success_datanames" => $locationSuccesses, "fail_datanames" => $locationFails);
	}

	function syncTableToChildAccount($parentDataname, $childDataname, $tableName){
		// 1.) Obviously, lets first verify that the parentDataname is a primary of a chain!!!
		// 2.) Second, lets verify that the childDataname is in the same chain and not primary.
		// 3.) Make sure the table is in the list of allowed tables that can be copies.
		// 4.) Lastly, never, ever just copy the locations table.
		// These two are taken care of within function getTableSyncPermissionsAndInfo. We'll verify in same mechanism.
		$syncPermissionsOnParent = getTableSyncPermissionsAndInfo($parentDataname);
		if(!$syncPermissionsOnParent){
			error_log("Did not have sync permissions.");
			return false;
		}

		//Verify it is the primary of the account:
		if($parentDataname !== $syncPermissionsOnParent['primary_dataname']){
			error_log("Warning ".$parentDataname." is not a primary of chain... called in function ".__FUNCTION__." of file:".__FILE__);
			return false;
		}

		//Verify that the child dataname belongs to the chain!
		$isProperChild = false;
		foreach($syncPermissionsOnParent['datanames_in_chain'] as $currDatanameArr){
			if($currDatanameArr['data_name'] === $childDataname){
				$isProperChild = true;
				break;
			}
		}
		if(!$isProperChild){
			error_log("Warning ".$childDataname." is not a secondary of primary:[$parentDataname]... called in function ".__FUNCTION__." of file:".__FILE__);
			return false;
		}

		//Verify that the table is allowed to be copied per the config setting: sync_primary_of_chain_table_settings
		if(!in_array($tableName, $syncPermissionsOnParent['sync_allowed_tables'])){
			return false;
		}

		///error_log("Performing DB table sync.");
		// ------------------------------------------------------------------------------------------------------------
		// TODO PLEASE IN CODE REVIEW THINK OF SECURITY, AM I MISSING ANYTHING ELSE? Kind of a sensitive operation here.
		// ------------------------------------------------------------------------------------------------------------

		// Make Backup table, move data to backup table, delete from target, move from primary to target, 
		// validate primary against target, if bad... fix from backup.

		//TODO if syncing issues... can check for the existence of the BackupTable and report as error if that's the case.


		$backupTableName = $tableName.'_syncBK';

		// 1.) Make backup table if does not exist.
		$backupCreate = mlavu_query("CREATE TABLE IF NOT EXISTS `[1]`.`[2]` LIKE `[3]`.`[4]`",
							'poslavu_'.$childDataname.'_db',  $backupTableName,
							'poslavu_'.$childDataname.'_db',  $tableName);
		///error_log("Create backup query:".print_r($backupCreate,1));

		// 2.) Clear backup table.
		$backupClear  = mlavu_query("TRUNCATE TABLE `[1]`.`[2]`", 'poslavu_'.$childDataname.'_db',  $backupTableName);
		///error_log("Backup Clear Query:".print_r($backupClear,1));

		// 3.) Insert table into backup.
		$backupInsert = mlavu_query("INSERT INTO `[1]`.`[2]` SELECT * FROM `[3]`.`[4]`",
							'poslavu_'.$childDataname.'_db',  $backupTableName,
						    'poslavu_'.$childDataname.'_db',  $tableName);
		///error_log("Backup Insert Query:".print_r($backupInsert,1));


		// 4.) Verify backup by number of rows.
		$backupCountResult = mlavu_query("SELECT COUNT(*) as count FROM `[1]`.`[2]`",
								'poslavu_'.$childDataname.'_db',  $backupTableName);
		$backupResultRow = mysqli_fetch_assoc($backupCountResult);
		$backupRowCount = $backupResultRow['count'];
		$targetTableCountResult = mlavu_query("SELECT COUNT(*) as count FROM `[1]`.`[2]`",
								'poslavu_'.$childDataname.'_db',  $tableName);
		$targetCountRow = mysqli_fetch_assoc($targetTableCountResult);
		$targetRowCount = $targetCountRow['count'];
		if(!$backupCountResult || !$targetTableCountResult || $backupRowCount != $targetRowCount){
			error_log("Table could not be successfully backed up during table sync in ".__FILE__);
			return false;
		}

		// 5.) DELETE FROM THE TARGET
		$deleteResult = mlavu_query("TRUNCATE TABLE `[1]`.`[2]`",
							'poslavu_'.$childDataname.'_db',  $tableName);
		///error_log("Delete target query:".print_r($deleteResult,1));

		// Validate deleted
		$targetTableCountResult = mlavu_query("SELECT COUNT(*) as count FROM `[1]`.`[2]`",
								'poslavu_'.$childDataname.'_db',  $tableName);
		$targetCountRow = mysqli_fetch_assoc($targetTableCountResult);
		$targetRowCount = $targetCountRow['count'];
		if($targetRowCount != 0){
			error_log("Could not delete from target child dataname in ".__FILE__);
			return false;
		}

		// 6.) INSERT FROM PRIMARY
		$insertResult = mlavu_query("INSERT INTO `[1]`.`[2]` SELECT * FROM `[3]`.`[4]`",
							'poslavu_'.$childDataname.'_db', $tableName,
							'poslavu_'.$parentDataname.'_db',  $tableName);
		//error_log("Cross Insert Result:".print_r($insertResult,1));
		

		// 7.) VALIDATE transfer by row count.
		$sourceTableCountResult = mlavu_query("SELECT COUNT(*) as count FROM `[1]`.`[2]`",
								'poslavu_'.$parentDataname.'_db',  $tableName);
		$sourceTableCountRow = mysqli_fetch_assoc($sourceTableCountResult);
		$sourceTableCount = $sourceTableCountRow['count'];
		$targetTableCountResult = mlavu_query("SELECT COUNT(*) as count FROM `[1]`.`[2]`",
								'poslavu_'.$childDataname.'_db',  $tableName);
		$targetTableCountRow = mysqli_fetch_assoc($targetTableCountResult);
		$targetTableCount = $targetTableCountRow['count'];
		if($sourceTableCount != $targetTableCount){
			error_log("Table Sync was not successful!!! Error in ".__FILE__);
		}else{
			// 8.) Drop the backup table if successful
			mlavu_query("DROP TABLE `[1]`.`[2]`", 'poslavu_'.$childDataname.'_db', $backupTableName);
		}

		setLastTableSyncTimesFromConfig($childDataname, $tableName);


		return array("source_data_name" => $parentDataname, "target_data_name" => $childDataname, "table_name" => $tableName, "source_row_count" => $sourceTableCount, "target_row_count" => $targetTableCount);
	}

	function getLastTableSyncTimesRowFromConfig(){
		$row = array();
		$key = 'sync_primary_of_chain_table_last_times';
		$result = lavu_query("SELECT `id`,`setting`,`value` FROM `config` WHERE `setting`='sync_primary_of_chain_table_sync_times'");
		if(!$result){
			error_log("SQL error in: ".__FILE__.' -shrsre8-');
			return false;
		}else{
			if(mysqli_num_rows($result)){
				$row = mysqli_fetch_assoc($result);
			}
		}
		return $row;
	}

	function setLastTableSyncTimesFromConfig($childDataname, $tableName){

		global $location_info;
		if(empty($location_info)){
			$result = lavu_query("SELECT `timezone` FROM `locations` LIMIT 1");
			$location_info = mysqli_fetch_assoc($result);
		}
		$timeZone = $location_info['timezone'];

		$localizedDateTime = localize_datetime(date('Y-m-d H:i:s'), $timeZone);

		$currSyncTimesRow = getLastTableSyncTimesRowFromConfig();
		
		$currSyncTimesMap = json_decode($currSyncTimesRow['value'],1);

		if(!is_array($currSyncTimesMap)){
			$currSyncTimesMap = array();
		}

		if(!is_array($currSyncTimesMap[$childDataname])){
			$currSyncTimesMap[$childDataname] = array();
		}

		$currSyncTimesMap[$childDataname][$tableName] = $localizedDateTime;

		$currSyncTimesJSON = json_encode($currSyncTimesMap);

		$settingName = 'sync_primary_of_chain_table_sync_times';
		if($currSyncTimesRow['id']){
			//Update
			$result = lavu_query("UPDATE `config` SET `value`='[1]' WHERE `setting`='[2]'",$currSyncTimesJSON,$settingName);
			if(!$result){
				error_log("Error updating last sync table:[$tableName] in file:".__FILE__);
			}
		}else{
			//Insert
			$result = lavu_query("INSERT INTO `config` (`setting`,`value`) VALUES ('[1]','[2]')",$settingName,$currSyncTimesJSON);
			if(!$result){
				error_log("Error inserting last sync table:[$tableName] in file:".__FILE__);
			}
		}

	}
















