<?php

function get_menu_meta_data() {
	$sql = mlavu_query("SELECT `id`, `name` FROM `poslavu_MAIN_db`.`menu_meta_data`");
	if($sql === false) error_log(mlavu_dberror());
	while($row = mysqli_fetch_assoc($sql)) {
		$menu_meta_data[] = $row;
	}
	return $menu_meta_data;
}

function get_dietary_info_checked($database, $item_id) {
	$db = $database;
	$table = '`menu_item_meta_data_link`';
	$sql = mlavu_query("SELECT meta_data_id, value FROM ".$db.".".$table . " WHERE menu_item_id = ".$item_id);
	if($sql === false) error_log(mlavu_dberror());
	while ($row = mysqli_fetch_assoc($sql)) {
		$meta_data_id = $row['meta_data_id'];
		$value = $row['value'];

		if ($value == 1) {
			$dietary_info[$meta_data_id] = $value;
		}
	}
	return $dietary_info;
}

//mysqli_real_escape_string($conn, $str)
function update_dietary_info($database, $boolean_arr, $item_id) {
	$conn = ConnectionHub::getConn('poslavu')->getLink("read");
	$query = "INSERT INTO ".$database.".`menu_item_meta_data_link` (`menu_item_id`, `meta_data_id`, `value`)";
	error_log(json_encode($boolean_arr));
	foreach ($boolean_arr as $key => $boolean_arr_instance){
		$meta_data_id = $key + 1;
		if ($key == 0) {
			$query .= " VALUES ";
		}
		$query .= "(". mysqli_escape_string($conn, $item_id) . ", ". mysqli_escape_string($conn, $meta_data_id) .", ". mysqli_escape_string($conn, $boolean_arr_instance). ")";
		if ($key < count($boolean_arr) - 1) {
			$query .= ", ";

		}
	}
	$query .= " ON DUPLICATE KEY UPDATE `value`= VALUES(`value`);";
	error_log("query = ".$query);
	$result = mlavu_query($query);
	if($result === false) error_log(mlavu_dberror());
	return $result;
}

function get_standard_nutrition_types() {
	$sql = mlavu_query("SELECT `id`, `label` FROM `poslavu_MAIN_db`.`standard_nutrition_types`");
	if($sql === false) error_log(mlavu_dberror());
	while($row = mysqli_fetch_assoc($sql)) {
		$standard_nutrition_types[] = $row;
	}
	return $standard_nutrition_types;
}

function upsert_nutrition_info($dbargs) {
	if (empty($dbargs['record_exists'])) {
		$query = "INSERT INTO [db].`nutrition_info_link` (`source_table`, `source_table_id`) VALUES ('[source_table]', '[source_table_id]')";
		$update_ok = mlavu_query($query, $dbargs);
		if (!$update_ok) {
			error_log(mlavu_dberror());
		}
		$dbargs['nutrition_info_link_id'] = mlavu_insert_id();
		$query2 = "INSERT INTO [db].`nutrition_info` (`nutrition_info_link_id`, `standard_nutrition_type_id`, `value`, `units`, `_deleted`) VALUES ([nutrition_info_link_id], [standard_nutrition_type_id], [value], '[units]', '[enabled]')";
		$query2 .= "ON DUPLICATE KEY UPDATE `value`= VALUES(`value`);";
		$update_ok2 = mlavu_query($query2, $dbargs);
	}

	else {
		$query2 = "UPDATE [db].`nutrition_info` SET `standard_nutrition_type_id` = [standard_nutrition_type_id], `value` = [value], `units` = '[units]', `_deleted` = '[enabled]' WHERE `nutrition_info_link_id` = [nutrition_info_link_id]";
		$update_ok2 = mlavu_query($query2, $dbargs);
	}
}