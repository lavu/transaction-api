<?php
	/******************************************************************
	Contains all the methods that handle the DB queries.
	*******************************************************************/
	class messagesDB{
		/******************************************************************
		Handles connection to the DB trough the custom inhouse DB libraries
		
		Params:		---			---
		
		Returns: 	---
		*******************************************************************/
		function __construct(){
			
			if (!isset($_SESSION)) {
				session_start();
			}
			
//			ini_set("display_errors",1);
			//echo dirname(__FILE__).'<br/><br/>';
			//require_once($_SERVER['DOCUMENT_ROOT']."/cp/resources/core_functions.php");
			global $fromCP;
			if(!isset($fromCP))
			{
				require_once($_SERVER['DOCUMENT_ROOT']."/cp/resources/core_functions.php");
			}
			$maindb = "poslavu_MAIN_db";
			$data_name = sessvar("admin_dataname");
			$in_lavu = true;
			lavu_auto_login();
			$rdb = admin_info("database");
			lavu_connect_byid(admin_info("companyid"),$rdb);
		}
		
		function updateEntry($title, $type, $content, $id){
				$query = "UPDATE poslavu_MAIN_db.messages set title=\"[1]\", type=\"[2]\", content=\"[3]\" WHERE id=\"[4]\"";
				$result = mlavu_query($query, $title, $type, $content, $id);
				return $result;
		}//updateEntry()
		
		function insertNewMessage($title, $type, $content){
				$query = "INSERT INTO poslavu_MAIN_db.messages (title, type, content) VALUES (\"[1]\",\"[2]\",\"[3]\")";
				$res = mlavu_query($query,$title,$type,$content);
				return $res;
		}//insertNewMessage()
		
		function getMessageInfo($id){
			$query = "SELECT * FROM poslavu_MAIN_db.messages WHERE id='".$id."'";//
			$result = mlavu_query($query);
			return $result;
		}//queryDB()
		function saveNewMessage($title, $type, $content){
			return mlavu_query('insert into poslavu_MAIN_db.messages (title, type, content) values ("'.$title.'". '.$type.', "'.$content.'")');
		}
		
		function getMessage($id){
			$result = array();
			$query = mlavu_query('select * from poslavu_MAIN_db.messages where id='.$id);
			while ($read = mysqli_fetch_assoc($query)) {
				$result[] = $read;
			}	
			return $result;
		}
		
		function getMessages(){
			$result = array();
			if(strstr($_SERVER['REQUEST_URI'],"v2") )
				$query = mlavu_query("select * from `poslavu_MAIN_db`.`messages` WHERE `_deleted`= '0' OR `_deleted`= '2' AND `type` NOT IN ('5', '7') order by `created_date` desc");
        	else
        		$query = mlavu_query("select * from `poslavu_MAIN_db`.`messages` WHERE `_deleted`= '0' AND `type` NOT IN ('5', '7') order by `created_date` desc");
        	
			while ($read = mysqli_fetch_assoc($query)) {
				if($this->checkReleventMessage($read['limiting_query'] ,$read['limiting_module']))
				{
					$result[] = $read;
				}
			}
			return $result;
		}
		
		function checkReleventMessage($query, $mod){
			$query    = trim($query);
			$mod      = trim($mod);
			require_once(__DIR__."/../objects/json_decode.php");
			global $modules;
			
			$mod  = JSON::unencode($mod);
			
			if( is_object($mod) && $mod->mod_combiner=='and')
				$can_view = true;
			else 
				$can_view= false;
			
			if(!empty($query)){
				$query = lavu_query($query);
				if(false === $query){
					//echo "DB Error";
					$can_view=false;	
				}
				if(mysqli_num_rows( $query )){
					$can_view= true;
				}
			}
			if( empty($mod)){
				$can_view=true;
			}else{
				//echo "starting can view is:". $can_view;
				if($mod->module_use==1){			// has module
					//echo "has mod";
					foreach($mod->limiting_module as $check_mod){
						if( $mod->mod_combiner=='and'){
							//echo"$check_mod and ";	
							$can_view &=  $modules->hasModule($check_mod);
							//echo $can_view."<br>";
						}else if($mod->mod_combiner=='or'){
							//echo"$check_mod or ";	
							$can_view |= $modules->hasModule($check_mod);
							//echo $can_view."<br>";
						}
					}
				}else if($mod->module_use==0) {
					//echo "doesnt have mod";
					foreach($mod->limiting_module as $check_mod){
						if( $mod->mod_combiner=='and'){
							//echo"$check_mod and ";	
							$can_view = $can_view && !($modules->hasModule($check_mod));
							//echo $can_view."<br>";
						}else if($mod->mod_combiner=='or'){
						//echo"$check_mod or ";	
							$can_view = $can_view || !($modules->hasModule($check_mod));
							//echo $can_view."<br>";
						}
					}
				}
			}

			return $can_view;	
		}	
	}
?>
