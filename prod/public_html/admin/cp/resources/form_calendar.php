<?php
	function create_form_calendar($fieldname, $set_day=false, $set_month=false, $set_year=false)
	{
		$js_jump_to_day = $fieldname."_jump_to_day";
		$js_selected = $fieldname."_date_selected";
		$js_cal_click = $fieldname."_cal_click";
		$js_select_month = $fieldname."_select_month";
		$js_monthid = $fieldname."_month_";
		$js_month_selected = $fieldname."_month_selected";
		$js_month_selector = $fieldname."_month_selector";
		
		$calwidth = 30;
		$calheight = 30;
		
		$allow_past = false;
		$quick_tags = array();
		$quick_tags[] = array("Today",0);
		$quick_tags[] = array("Tommorrow",1);
		
		if($set_day)
			$setvalue = $set_year . "-" . $set_month . "-" . $set_day;
		else
			$setvalue = "";
		$str = "<input type='hidden' name='$fieldname' id='$fieldname' value='$setvalue'>";
		
		$r=1;
		$day=1;
		if(!$set_month)
			$set_month = date("m");
		if(!$set_year)
			$set_year = date("Y");
		
		$str .= "<table><td align='left'>";
		$str .= "<select name='$js_month_selector' id='$js_month_selector' onchange='$js_select_month(this.value)'>";
		$month_list = array();
		for($i=0; $i<6; $i++)
		{
			$setm = date("m") + $i;
			$sety = date("Y");
			if($setm > 12)
			{
				$setm-=12;
				$sety++;
			}
			$month_list[] = array($setm,$sety);
		}
													 
		for($i=0; $i<count($month_list); $i++)
		{
			$month = $month_list[$i][0];
			$year = $month_list[$i][1];
			
			$sdate=mktime(0,0,0,$month,1,$year);
			$str .= "<option value='".date("Y",$sdate)."-".date("m",$sdate)."'";
			if($set_month==date("m",$sdate)&&$set_year==date("Y",$sdate))
				$str .= " selected";
			$str .= ">".date("F",$sdate) . " " . date("Y",$sdate)."</option>";
		}
		$str .= "</select>";
		
		$cal_js = "cal_colors = new Array(); selected_datenum = false; ";
		for($i=0; $i<count($month_list); $i++)
		{
			$month = $month_list[$i][0];
			$year = $month_list[$i][1];

			if($month < 10) $month = "0" . ($month * 1);
			$current_month = $year."-".$month;
			$monthid = $fieldname."_month_".$current_month;
			$set_display = "none";
			if($current_month==$set_year . "-" . $set_month)
				$set_display = "block";
			$str .= "<div id='".$monthid."' style='display:$set_display'>";
			$str .= "<table cellspacing=0>";
			$r=1;$day=1;
			while($r<=6)
			{
				$str .= "<tr>";
				if($r==1)
				{
					$c=date("w",mktime(0, 0, 0, $month, 1, $year))+1;
					$x=1;
					while($x<$c)
					{
					  $str .= "<td width=$calwidth height=$calheight>&nbsp;</td>";
					  $x=$x+1;
					}
				}
				else $c=1;
				
				while($c<=7)
				{
					if($day<10) $day="0".$day;
					$datecheck = $year."-".$month."-".$day;
					
					if((($r+$c)%2)==0) 
						$sbg="#9fd4f0";
					else 
						$sbg="#5bb4e2";
						
					if(checkdate($month,$day,$year))
					{						
						$dayoftheweek = date("w",mktime(0,0,0,$month,$day,$year)) * 1 + 1;
	
						$msg = date("l",mktime(0,0,0,$month,$day,$year));
						$clr = "000000";
						
						if($allow_past || date("Y-m-d") <= date("Y-m-d",mktime(0,0,0,$month,$day,$year)))
							$allow = true;
						else
						{
							$allow = false;
							$clr = "666699";
						}
						
						$datenum = ($month * 100) + $day;
						$cal_js .= "cal_colors[$datenum] = '$sbg'; ";
						if($month==$set_month && $year==$set_year && $day==$set_day)
						{
							$extra_style_code = "border:solid 2px black; ";
							$sbg = "#00ff00";
							$cal_js .="selected_datenum = '$datenum'; ";
						}
						else $extra_style_code = "";
						
						$str .= "<td id='".$fieldname."_date_$datecheck' class='cal' title='$msg' alt='$msg'";
						$str .= " style='cursor:pointer; background-color:$sbg;";
						$str .= $extra_style_code;
						$str .= "'";
						if($allow)
							$str .= " onclick='".$fieldname."_cal_click($year, $month, $day)' onMouseOver='this.style.backgroundColor=\"#00ff00\"' onMouseOut='if(selected_datenum==$datenum) this.style.backgroundColor=\"#00ff00\"; else this.style.backgroundColor=cal_colors[$datenum]'";
						$str .= " width=$calwidth height=$calheight align='center' valign='center'>";
						$str .= "<font color='#$clr'>";
						$str .= ($day*1);
						$str .= "</font>";
						$str .= "</td>";						
					}
					else $str .= "<td width=$calwidth height=$calheight>&nbsp;</td>";
		
					$c++;$day++;
				}
				$str .= "</tr>";
				$r++;
			}
			$str .= "</table>";
			$str .= "</div>";
		}
		$str .= "</td>";
		$str .= "<td valign='top'>";
		$str .= "&nbsp;<br>";
		$str = "<script language='javascript'>$cal_js</script>" . $str;
		
		for($i=0; $i<count($quick_tags); $i++)
		{
			$qtitle = $quick_tags[$i][0];
			$qdays = $quick_tags[$i][1];
			$qdate = date("'Y','m','d'",mktime(0,0,0,date("m"),date("d")+$qdays,date("Y")));
			
			$str .= "<br><input type='button' value='$qtitle' class='sbtn' onclick=\"$js_jump_to_day($qdate)\">";
		}
		$str .= "</td>";
		$str .= "</table>";
				
		$str .= "<script language='javascript'>";
		$str .= "$js_selected = '$setvalue'; ";
		$str .= "function $js_cal_click(year, month, day) ";
		$str .= "{ ";
		$str .= "if((month * 1) < 10) month = '0' + (month * 1); ";
		$str .= "if((day * 1) < 10) day = '0' + (day * 1); ";
		$str .= "if($js_selected!='') { document.getElementById('".$fieldname."_date_' + $js_selected).style.border = '0px solid white'; ";
		$str .= " if(selected_datenum!='') {document.getElementById('".$fieldname."_date_' + $js_selected).style.backgroundColor = cal_colors[selected_datenum];} } ";
		$str .= "beforenum = selected_datenum; selected_datenum = (month * 100) + (day *1);";
		$str .= "$js_selected = year + '-' + month + '-' + day; ";
		$str .= "document.getElementById('".$fieldname."_date_' + $js_selected).style.border = '2px solid black'; ";
		$str .= "document.getElementById('".$fieldname."_date_' + $js_selected).style.backgroundColor = '#00ff00'; ";
		$str .= "document.getElementById('".$fieldname."').value = $js_selected; ";
		$str .= "} ";
		$str .= "$js_month_selected = '".$set_year."-".$set_month."'; ";
		$str .= "function $js_select_month(setmonth) ";
		$str .= "{ ";
		//$str .= "alert(setmonth); return;";
		$str .= "document.getElementById('".$js_monthid."' + $js_month_selected).style.display = 'none'; ";
		$str .= "$js_month_selected = setmonth; ";
		$str .= "document.getElementById('".$js_monthid."' + $js_month_selected).style.display = 'block'; ";
		$str .= "} ";
		$str .= "function $js_jump_to_day(jyear,jmonth,jday) {";
		$str .= "document.getElementById('$js_month_selector').value = jyear + '-' + jmonth; ";
		$str .= "$js_select_month(jyear + '-' + jmonth); ";
		$str .= "$js_cal_click(jyear, jmonth, jday); ";
		$str .= "} ";
		$str .= "</script>";
		return $str;
	}
?>
