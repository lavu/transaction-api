<?php
	require_once("/home/poslavu/private_html/rs_upload.php");
	
	function import_area($area)
	{
		$import_fields = false;
		
		// hard code fields for customer_accounts table for now
		
		$import_fields = array();
		$import_fields[] = array("First Name", "f_name");
		$import_fields[] = array("Last Name", "l_name");
		$import_fields[] = array("Email", "email");
		$import_fields[] = array("RFID", "rf_id");
		$import_fields[] = array("Employee ID", "employee_id");
		$import_fields[] = array("ADP File #", "adp_file_no");
		
		if($import_fields)
			return create_import_area($area,$import_fields);
		else
			return false;
	}
	
	function gvar($str, $def=false)
	{
		if(isset($_GET[$str])) return $_GET[$str]; else return $def;
	}
	
	function create_import_area($earea, $import_fields)
	{
		global $mode;
		
		$import_path = "/home/poslavu/public_html/admin/import/".admin_info('dataname')."/";
		$fname = gvar("fname");		
		$action = gvar("action");
		
		if(isset($_FILES['ifile']))
		{
			$basename = str_replace(" ", "_", basename($_FILES['ifile']['name']));
			if($basename!="")
			{
				$setpath = $import_path.$basename;
				if(!is_dir($import_path))
				{
					mkdir($import_path);
				}
				$uploadfile = $import_path.$basename;
				if(rs_move_uploaded_file($_FILES['ifile']['tmp_name'], $uploadfile, "",false,array("xls","txt","csv"))) {
					$fname = $basename;
					echo "<script language='javascript'>";
					echo "window.location.replace('index.php?mode=$mode&mass_import=$earea&fname=$fname'); ";
					echo "</script>";
					exit();
				} else {
					echo "<script language='javascript'>";
					echo "alert('Invalid file type. Please try again with a .txt or .xls file.');";
					echo "</script>";
				}
			}
		}
		
		if($fname)
		{
			echo "<form name='import_form' method='post' action='index.php?mode=$mode&mass_import=$earea&fname=$fname&action=import'>";
			echo "Import File: $fname";
			if(!$action)
			{
				echo "<br><br><input type='button' value='Submit' onclick='document.import_form.submit()'>";
				echo "<br><br>Action: <select name='import_type'><option value='import'>Import</option>";
				if($earea=="orders")
				{
					echo "<option value='update'>Update</option>";
				}
				echo "</select>";
				echo "<br><br>Omit first row: <select name='omit_first_row'><option value='yes'>Yes</option><option value='no'>No</option></select>";
				echo "&nbsp;&nbsp;&nbsp;<font color='#777777'><br>(Omit the first row if it is made up of column titles)</font><br><br>";
			}
			
			$filetype = substr($fname, strrpos($fname, '.') + 1);	
			$filename = $import_path.$fname;
			$data_read_success = false;
			
			$row_d = "\n"; // find a way to determine whether to use \n or \r
			$col_d = "\t";
			
			if ($filetype=="txt")
			{
				$fp = fopen($filename,"r");
				if($fp)
				{
					$data = fread($fp, filesize($filename));
					fclose($fp);
					$data_read_success = true;
					$rows = explode($row_d, $data);
					$maxcol = 0;
					for($i=0; $i<count($rows); $i++)
					{
						$rows[$i] = explode($col_d, $rows[$i]);
						if(count($rows[$i]) > $maxcol) $maxcol = count($rows[$i]);
					}
					$maxrow = count($rows);

					/*$delimiter = "\t";
					$data_read_success = true;
					$rows = array();
					$maxcol = 0;
					while ($row = str_replace(chr(0), "", fgets($fp))) {
						$split_row = explode($delimiter, $row);
						$rows[] = $split_row;
						if (count($split_row) > $maxcol) $maxcol = count($split_row);
					}
					$maxrow = count($rows);
					fclose($fp);*/
				}
			}
			else if ($filetype=="csv")
			{
				$col_d = ",";
				$fp = fopen($filename,"r");
				if($fp)
				{
					$data = fread($fp, filesize($filename));
					fclose($fp);
					$data_read_success = true;
					$rows = explode($row_d, $data);
					$maxcol = 0;
					for($i=0; $i<count($rows); $i++)
					{
						$rows[$i] = explode($col_d, $rows[$i]);
						if(count($rows[$i]) > $maxcol) $maxcol = count($rows[$i]);
					}
					$maxrow = count($rows);
				}
			}
			/*
			else if($filetype=="xls")
			{
				// TODO : use https://github.com/PHPOffice/PHPExcel to rewrite -- this stuff is OOOOLD and broken in PHP 7 
				require_once("Excel/reader.php");
				$data = new Spreadsheet_Excel_Reader();
				$data->setOutputEncoding('ASCII');	
				$data->setRowColOffset(0);
				$data->read($filename);

				$maxrow = $data->sheets[0]['numRows'];
				$maxcol = $data->sheets[0]['numCols'];
				$rows = $data->sheets[0]['cells'];
				$data_read_success = true;
			}
			*/
			
			//-------------------------------------------------//
			/*------------------------*/ $testing_import = false;
			
			if($data_read_success)
			{
				$filter_list = array();
				$set_auto_assign_cols = "";
				$set_auto_assign_vals = "";
				
				$option_code = "<select name='[name]'>";
				$option_code .= "<option value=''></option>";
				for ($f = 0; $f < count($import_fields); $f++) {
					$option_code .= "<option value='field$f'>".$import_fields[$f][0]."</option>";
					$filter_list['field'.$f] = array($import_fields[$f][0],"assign",$import_fields[$f][1],"","");
				}
				$option_code .= "</select>";
				
				$set_auto_assign_cols .= "`loc_id`";
				$set_auto_assign_vals .= "'".$locationid."'";

				/*$filter_query = lavu_query("select * from `import_fields` where `area`='$earea' order by `name` asc");
				while($filter_read = mysqli_fetch_assoc($filter_query))				
				{
					$optionid = $filter_read['id'];
					$option_name = $filter_read['name'];
					$option_action = $filter_read['action'];
					$option_target = $filter_read['target'];
					$option_setting1 = $filter_read['setting1'];
					$option_setting2 = $filter_read['setting2'];
					
					if($option_action=="value_assign")
					{
						if($set_auto_assign_cols!="") $set_auto_assign_cols .= ",";
						if($set_auto_assign_vals!="") $set_auto_assign_vals .= ",";
						$set_auto_assign_cols .= "`$option_target`";
						$set_auto_assign_vals .= "'".str_replace("'","''",$option_setting2)."'";
					}
					else
					{
						$option_code .= "<option value='$optionid'>$option_name</option>";
						$filter_list[$optionid] = array($option_name,$option_action,$option_target,$option_setting1,$option_setting2);
					}
				}
				$option_code .= "</select>";*/
								
				$importid = date("YmdHis");
				$insert_count = 0;
				if($action=="import")
					$maxrowcount = $maxrow;
				else
					$maxrowcount = 6;
				$linkage_cols = "";
				
				$omit_first_row = (isset($_POST['omit_first_row']))?$_POST['omit_first_row']:"yes";
				if($omit_first_row=="yes" && $action=="import") $startrow = 1; else $startrow = 0;
				$import_type = (isset($_POST['import_type']))?$_POST['import_type']:"import";
				
				//$testing_import = true;
				//----------------------------//
				
				echo "<input type='hidden' name='auto_assign_cols' value=\"".str_replace("\"","&quot;",$set_auto_assign_cols)."\">";
				echo "<input type='hidden' name='auto_assign_vals' value=\"".str_replace("\"","&quot;",$set_auto_assign_vals)."\">";
				
				echo "<table border='1' cellpadding='2'>";
				
				if($maxrow > $maxrowcount) $maxrow = $maxrowcount;
				for($i=$startrow; $i<$maxrow; $i++)
				{
					$insert_cols = "";
					$insert_vals = "";
					$update_cols = "";
					$db_values_set = array();
					$sched_item_list = array();
					
					$schedule_inventory_items = false;
					$non_blank_count = 0;
					$rstr = "<tr>";
					$opt_row = "<tr>";
					
					$cols = $rows[$i];
					
					$dupe_check_field = "employee_id";
					
					for($c=0; $c<$maxcol; $c++)
					{
						$linkid = "link_" . $c;
						if(isset($cols[$c]))
							$val = $cols[$c];
						else
							$val = "";
						$val = trim($val,chr(13));
						$val = trim($val,"\n");
						$val = trim($val);
						$val = trim($val,chr(10));
						$val = trim($val,"\t");
						$val = trim($val);
						$val = trim($val,chr(13));
						
						if($val!="") $non_blank_count ++;
						$showval = trim(trim(trim(preg_replace("/[^A-Za-z0-9\s\[\\\^\$\.\|\?\*\+\(\)\{\}!,~@#%&=<>:;'\"`]/","",$val)),"\n"));
						if($showval=="") $showval = "&nbsp;";
						$rstr .= "<td>" . $showval . "</td>";
						if($i==0)
						{
							$current_opt_row = str_replace("[name]",$linkid,$option_code);
							if(isset($_SESSION['import_setopt_'.$c])) 
							{
								$select_option = $_SESSION['import_setopt_'.$c];
								$current_opt_row = str_replace("<option value='$select_option'>","<option value='$select_option' selected>",$current_opt_row);
							}
							$opt_row .= "<td>".$current_opt_row."</td>";
						}
						if($action=="import")
						{
							//echo "<br>" . $linkid . ": " . $_POST[$linkid];
							if(isset($_POST[$linkid]) && $i==$startrow)
							{
								$_SESSION['import_setopt_'.$c] = $_POST[$linkid];
							}
							if(isset($_POST[$linkid]) && $_POST[$linkid]!="")
							{
								$colname = false;
								$colval = false;

								$col_filter = $filter_list[$_POST[$linkid]];
								$col_filter_action = $col_filter[1];
								$col_filter_target = $col_filter[2];
								$col_filter_setting1 = $col_filter[3];
								$col_filter_setting2 = $col_filter[4];
								
								//------------------------------- FILTER HANDLING ---------------------------------//
								if($col_filter_action=="assign")
								{
									$colname = $col_filter_target;
									$colval = $val;
								}
								else if($col_filter_action=="numeric")
								{
									$strval = $val;
									$val = "";
									for($x=0; $x<strlen($strval); $x++)
									{
										$chr = substr($strval,$x,1);
										if(is_numeric($chr) || $chr==".") $val .= $chr;
									}
									$val = $val * 1;
								
									$colname = $col_filter_target;
									$colval = $val;
								}
								else if($col_filter_action=="date")
								{
									if(is_numeric($val) && $val >= 36500 && $val <= 73000)
									{
										$ts = mktime(0,0,0,1,$val,1900);
										$val = date("Y-m-d");
									}
									else if(strpos($val,"/")!==false)
									{
										$date_parts = explode("/",$val);
										$month = $date_parts[0] * 1;
										$day = $date_parts[1] * 1;
										$year = $date_parts[2] * 1;
										
										if($month < 10) $month = "0".$month;
										if($day < 10) $day = "0".$day;
										if($year < 100) $year = (2000 + $year * 1);
										
										$val = $year . "-" . $month . "-" . $day;
									}
									
									$colname = $col_filter_target;
									$colval = $val;
								}
								else if($col_filter_action=="time")
								{
									$ap = false;
									$val = trim(strtolower($val));
									if(strpos($val,"am"))
									{
										$val = trim(str_replace("am","",$val));
										$ap = "am";
									}
									else if(strpos($val,"pm"))
									{
										$val = trim(str_replace("pm","",$val));
										$ap = "pm";
									}
									
									if(strpos($val,":"))
									{
										$time_parts = explode(":",$val);
										$hours = $time_parts[0];
										$minutes = $time_parts[1];
										if($ap=="am"&&$hours==12) $hours = 0;
										if($ap=="pm"&&$hours<12) $hours += 12;
										$val = ($hours * 100) + ($minutes * 1);
									}
									
									if(is_numeric($val))
									{
										$colname = $col_filter_target;
										$colval = $val;
									}
								}
								else if($col_filter_action=="link")
								{
									$colname = $col_filter_target;
									$colval = "{filter:linkto}".$val;
									if($i==$startrow)
									{
										if($linkage_cols!="") $linkage_cols .= ",";
										$linkage_cols .= $col_filter_target."|".$col_filter_setting1;
									}
								}
								else if($col_filter_action=="inventory_schedule")
								{
									$delimit_items = "\n";
									$delimit_qty = " x ";
									$qty_position = "front";
									
									$colname = false;
									$colval = false;
									
									$schedule_inventory_items = true;
									$schedule_inv = array();
									
									$sched_split = explode($delimit_items,$val);
									for($n=0; $n<count($sched_split); $n++)
									{
										$sched_item = $sched_split[$n];
										$sched_qty = 1;
										if($qty_position=="front")
										{
											if(strpos($sched_item,$delimit_qty)!==false)
											{
												$sched_qty = substr($sched_item,0,strpos($sched_item,$delimit_qty));
												$sched_item = substr($sched_item,strpos($sched_item,$delimit_qty) + strlen($delimit_qty));
											}
										}
										else if($qty_position=="back")
										{
											if(strpos($sched_item,$delimit_qty)!==false)
											{
												$sched_qty = substr($sched_item,strrpos($sched_item,$delimit_qty) + strlen($delimit_qty));
												$sched_item = substr($sched_item,0,strrpos($sched_item,$delimit_qty));
											}
										}
										$sched_item_list[] = array($sched_item, $sched_qty);
										//echo "<br><br>Item: $sched_item<br>Qty: $sched_qty";
									}
								}
								//---------------------------------------------------------------------------------//
								
								if($colname && $non_blank_count > 0)
								{									
									$colnames = explode(",",$colname);
									for($cn=0; $cn<count($colnames); $cn++)
									{
										$c_colname = trim($colnames[$cn]);
										if($c_colname!="")
										{
											if($insert_cols!="")
											{
												$insert_cols .= ",";
												$insert_vals .= ",";
												$update_cols .= ",";
											}
											$colval = preg_replace("/[^A-Za-z0-9\s\[\\\^\$\.\|\?\*\+\(\)\{\}!,~@#%&=<>:;'\"`]/","",$colval);
											$db_values_set[$c_colname] = $colval;
											
											$insert_cols .= "`$c_colname`";
											$insert_vals .= "'".str_replace("'","''",$colval)."'";
											$update_cols .= "`$c_colname`='".str_replace("'","''",$colval)."'";
										}
									}
								}
							}
						}
					}
					$rstr .= "</tr>";
					$opt_row .= "</tr>";
					
					if($action=="import")
					{
						if($insert_cols!="")
						{
							$query_secondary = array();
							if(isset($_POST['auto_assign_cols']) && isset($_POST['auto_assign_vals']))
							{
								if($_POST['auto_assign_cols']!="" && $_POST['auto_assign_vals']!="")
								{
									$insert_cols .= ",".$_POST['auto_assign_cols'];
									$insert_vals .= ",".$_POST['auto_assign_vals'];
								}
							}
							
							$insert_cols .= ",`import`";
							$insert_vals .= ",'$importid'";
							if($import_type=="import") {
								$check_for_duplicate = lavu_query("SELECT `id` FROM `$earea` WHERE `[1]` = '[2]'", $dupe_check_field, $db_values_set[$dupe_check_field]);
								if (mysqli_num_rows($check_for_duplicate)) {
									$info = mysqli_fetch_assoc($check_duplicate);
									$query = "UPDATE `$earea` SET $update_cols WHERE `id` = '".$info['id']."'";
								}
								else
									$query = "insert into `$earea` ($insert_cols) values ($insert_vals)";
							}
							else if($import_type=="update")
							{
								if(isset($db_values_set['date']) && isset($db_values_set['date_end']) && isset($db_values_set['time']) && isset($db_values_set['time_end']) && isset($db_values_set['import_total']))
								{
									$start_date = $db_values_set['date'];
									$start_time = $db_values_set['time'];
									$end_date = (isset($db_values_set['date_end']))?$db_values_set['date_end']:"";
									$end_time = (isset($db_values_set['time_end']))?$db_values_set['time_end']:"";
									if($end_date=="") $end_date = $start_date;
									if($end_time=="") $end_time = 2359;								
									$ts_start = datetime_ts($start_date,$start_time);
									$ts_end = datetime_ts($end_date,$end_time);
									$import_total = $db_values_set['import_total'];
									$findquery_str = "select * from `$earea` where `ts_start`='$ts_start' and `ts_end`='$ts_end' and `import_total`='$import_total'";
									$find_query = lavu_query($findquery_str);
									//echo "<br><br>FIND___________ " . $findquery_str."<br><br>";
									if(mysqli_num_rows($find_query))
									{
										$find_read = mysqli_fetch_assoc($find_query);
										$update_colid = $find_read['id'];
										
										$query = "update `$earea` set $update_cols where id='".$update_colid."'";
									}
									else $query = false;
								}
								else
								{
									$query = false;
								}
							}
							else
								$query = false;
							$conn = ConnectionHub::getConn('rest');
							if($query)
							{
								if($testing_import)
								{
									echo "<br><br>" . $query;
									for($x=0; $x<count($query_secondary); $x++)
										echo "<br>" . $query_secondary[$x];
								}
								else
								{
									$success = $conn->query('write', $query);
									$insertid = $conn->insertID();
									if($success) $insert_count++;
									for($x=0; $x<count($query_secondary); $x++)
									{
										$squery = str_replace("[query_insert_id]",$insertid,$query_secondary[$x]);
										$conn->query('write', $squery);
									}
								}
							}
						}
					}
					else if(!$action)
					{
						if($i==0) echo $opt_row;
						echo $rstr;
					}
				}
				echo "</table>";
				
				if($action=="import")
				{
					if($import_type=="update") $import_action_str = "updated"; else $import_action_str = "imported";

					if($insert_count > 0)
					{
						if($linkage_cols=="") $import_status = "done";
						else $import_status = "inserted";
						
						if($testing_import)
						{
							echo "<br>Test complete<br>";
						}
						else
						{
							//update_system_log("import",$importid,$import_status,$linkage_cols);
							//lavu_query("insert into `auto_track` (`event`,`details`,`date`,`time`) values ('import','','".date("Y-m-d")."','".date("Y-m-d H:i:s")."')");
							//echo "<br>".$import_action_str." $insert_count new records<br>";
						}
					}
					else
						echo "<br>No columns where ".$import_action_str."<br>";
				}
			}
			echo "</form>";
		}
		else if($action=="edit_filter")
		{
			$filterid = gvar('filterid');
			
			if(isset($_POST['filter_name']))
			{
				$set_name = str_replace("'","''",$_POST['filter_name']);
				$set_action = str_replace("'","''",$_POST['filter_action']);
				$set_target = str_replace("'","''",$_POST['filter_target']);
				$set_target2 = str_replace("'","''",$_POST['filter_target2']);
				if(trim($set_target2)!="")
					$set_target .= ",".$set_target2;
				$set_setting1 = (isset($_POST['filter_setting1']))?str_replace("'","''",$_POST['filter_setting1']):"";
				$set_setting2 = (isset($_POST['filter_setting2']))?str_replace("'","''",$_POST['filter_setting2']):"";
				
				if($filterid)
					$query = "update `import_fields` set `area`='$earea', `name`='$set_name', `action`='$set_action', `target`='$set_target', `setting1`='$set_setting1', `setting2`='$set_setting2' where id='$filterid'";
				else
					$query = "insert into `import_fields` (`area`,`name`,`action`,`target`,`setting1`,`setting2`) values ('$earea','$set_name','$set_action','$set_target','$set_setting1','$set_setting2')";
				lavu_query($query);
				echo "<script language='javascript'>";
				echo "window.location.replace('index.php?mode=$mode&mass_import=$earea&action=manage_filters'); ";
				echo "</script>";
				//echo $query;
			}
			else
			{
				//------------------ option code for assignment ------------------------//
				$option_code = "<select name='filter_target' style='color:#000044'>";
				$option_code .= "<option value=''></option>";
				$option_list = array();
				for($i=0; $i<count($import_fields); $i++)
				{
					$option = $import_fields[$i][0];
					$option_type = $import_fields[$i][1];
					if($option_type!="submit" && $option_type!="title" && $option_type!="sameas" && $option_type!="sameas_db")
						$option_list[] = $option;
				}
				sort($option_list);
				for($i=0; $i<count($option_list); $i++)
				{
					$option = $option_list[$i];
					$option_code .= "<option value='$option'>$option</option>";
				}
				$option_code .= "</select>";

				//---------------- option code for linkage -----------------------------//
				$option_tables = "<select name='filter_setting1' style='color:#000044'>";
				$option_tables .= "<option value=''></option>";
				$table_query = lavu_query("SHOW TABLES");
				while($table_read = mysqli_fetch_row($table_query))
				{
					$option = $table_read[0];
					$option_tables .= "<option value='$option'>$option</option>";
				}
				$option_tables .= "</select>";

				$value_assign_input = "<input name='filter_setting2' style='color:#000044' value=''>";
				
				$form_url_code = "";
				$set_name = "";
				$set_action = "";
				$set_target = "";
				
				$option_code2 = str_replace("filter_target","filter_target2",$option_code);
				if($filterid)
				{
					$filter_query = lavu_query("select * from `import_fields` where id='$filterid'");
					if(mysqli_num_rows($filter_query))
					{
						$filter_read = mysqli_fetch_assoc($filter_query);
						$form_url_code = "&filterid=$filterid";
						$set_name = $filter_read['name'];
						$set_action = $filter_read['action'];
						$set_target = $filter_read['target'];
						$set_target2 = "";
						
						if(strpos($set_target,","))
						{
							$target_pieces = explode(",",$set_target);
							$set_target = trim($target_pieces[0]);
							$set_target2 = trim($target_pieces[1]);
						}
						$set_setting1 = $filter_read['setting1'];
						$set_setting2 = $filter_read['setting2'];
						
						$option_code = str_replace("<option value='$set_target'>","<option value='$set_target' selected>",$option_code);
						$option_code2 = str_replace("<option value='$set_target2'>","<option value='$set_target2' selected>",$option_code2);
						if($set_action=="link")
							$option_tables = str_replace("<option value='$set_setting1'>","<option value='$set_setting1' selected>",$option_tables);
						$value_assign_input = str_replace("value=''","value=\"".str_replace("\"","&quot;",$set_setting2)."\"",$value_assign_input);
					}
				}
				
				echo "<form name='assign' method='post' action='index.php?mode=$mode&mass_import=$earea&action=edit_filter".$form_url_code."'>";
				echo "<script language='javascript'>";
				echo "function filter_submit() { ";
				echo "  if(document.assign.filter_name.value=='') alert('Please enter a name for this filter'); ";
				echo "  else if(document.assign.filter_action.value=='') alert('Please choose the filter action'); ";
				echo "  else if(document.assign.filter_target.value=='' && document.assign.filter_action.value!='inventory_schedule' && document.assign.filter_action.value!='inventory_quantity') alert('Please choose the target for this filter'); ";
				echo "  else document.assign.submit(); ";
				echo "} ";
				echo "function filter_action_changed(val) { ";
				echo "  if(val=='link') setvis = 'block'; else setvis = 'none'; ";
				echo "  document.getElementById('area_setting_table').style.display = setvis; ";
				echo "  if(val=='value_assign') setvis = 'block'; else setvis = 'none'; ";
				echo "  document.getElementById('area_setting_value_assign').style.display = setvis; ";
				echo "} ";
				echo "</script>";
				echo "<table><td style='border-bottom:solid 1px black' width='400'>";
				if($filterid)
					echo "Edit";
				else
					echo "New";
				echo " field assignment or filter:";
				echo "</td></table>";
				echo "<table>";
				echo "<tr><td align='right'>Name</td><td><input type='text' name='filter_name' value=\"".str_replace("\"","&quot;",$set_name)."\"></td></tr>";
				echo "<tr><td align='right' style='color:#004400'>Action</td><td><select name='filter_action' style='color:#004400' onchange='filter_action_changed(this.value)'>";
				echo "<option value=''></option>";
				echo "<option value='assign'".($set_action=='assign'?" selected":"").">assign</option>";
				echo "<option value='numeric'".($set_action=='numeric'?" selected":"").">numeric</option>";
				echo "<option value='link'".($set_action=='link'?" selected":"").">link</option>";
				echo "<option value='date'".($set_action=='date'?" selected":"").">date</option>";
				echo "<option value='time'".($set_action=='time'?" selected":"").">time</option>";
				echo "<option value='value_assign'".($set_action=='value_assign'?" selected":"").">value assign</option>";
				echo "<option value='inventory_schedule'".($set_action=='inventory_schedule'?" selected":"").">inventory schedule</option>";
				echo "<option value='inventory_quantity'".($set_action=='inventory_quantity'?" selected":"").">inventory quantity</option>";
				echo "</select></td></tr>";
				echo "<tr><td align='right' style='color:#000044'>Target</td><td>";
				echo $option_code;
				echo $option_code2;				
				echo "</td></tr>";
				echo "<tr><td></td><td>";
				
				echo "<div id='area_setting_table' style='display:".($set_action=="link"?"block":"none")."'>Link to Table: $option_tables</div>";
				echo "<div id='area_setting_value_assign' style='display:".($set_action=="value_assign"?"block":"none")."'>Set Value: $value_assign_input</div>";
				
				echo "</td></tr>";
				echo "<tr><td></td><td><input type='button' value='Submit' onclick='filter_submit()'></td></tr>";
				echo "</table>";
				echo "</form>";
			}
		}
		else if($action=="manage_filters")
		{
			echo "<input type='button' value='Add Field Assignment or Filter' onclick='window.location = \"index.php?mode=$mode&mass_import=$earea&action=edit_filter\"'><br>";
			$filter_query = lavu_query("select * from `import_fields` where `area`='$earea' order by `name` asc");
			if(mysqli_num_rows($filter_query))
			{
				echo "<br>";
				echo "<table style='border:solid 1px black' cellpadding=4 cellspacing=0>";
				echo "<tr bgcolor='#dddddd'>";
				echo "<td style='border-bottom:solid 1px black'>Name</td>";
				echo "<td style='border-bottom:solid 1px black'>Action</td>";
				echo "<td style='border-bottom:solid 1px black'>Target</td>";
				echo "</tr>";
				while($filter_read = mysqli_fetch_assoc($filter_query))
				{
					echo "<tr onmouseover='this.bgColor = \"#aabbee\"' onmouseout='this.bgColor = \"\"' style='cursor:pointer' onclick='window.location = \"index.php?mode=$mode&action=edit_filter&filterid=".$filter_read['id']."\"'>";
					echo "<td style='border-right:solid 1px #777777'>" . $filter_read['name'] . "</td>";
					echo "<td style='border-right:solid 1px #777777; color:#004400'>" . $filter_read['action'] . "</td>";
					echo "<td style='color:#000044'>" . $filter_read['target'] . "</td>";
					echo "</tr>";
				}
				echo "</table>";
			}
			else echo "<br>There are no field assignments or filters for this area<br><br>";
						
			for($i=0; $i<count($import_fields); $i++)
			{
				$option = $import_fields[$i][0];
				$option_display = (isset($import_fields[$i][6]))?$import_fields[$i][6]:$option;
				$option_type = $import_fields[$i][1];
			}
		}
		else if($action=="link")
		{
			$col = gvar("col");
			$table = gvar("table");
			$maintable = gvar("maintable");
			
			if($col && $table)
			{
				if($maintable) $marea = $maintable; else $marea = $earea;
				
				if(isset($_POST['linkage_count']))
				{
					$linkage_updated = 0;
					$linkage_count = $_POST['linkage_count'];
					for($i=1; $i<=$linkage_count; $i++)
					{
						$linkfrom = $_POST['form_linkfrom_'.$i];
						$linkto = $_POST['form_linkto_'.$i];
						
						if($linkto!="")
						{
							if($linkto=="empty")
								$set_linkto_value = "";
							else
								$set_linkto_value = $linkto;
							$query = "update `$marea` set `$col`='".str_replace("'","''",$set_linkto_value)."' where `$col`='".trim(str_replace("'","''",$linkfrom),chr(13))."'";
							//echo "<br><br>$query";
							$success = lavu_query($query);
							$linkage_updated += $success;
						}
					}
					echo $linkage_updated . " updates executed";
					echo "<br><input type='button' value='Continue>>' onclick='window.location = \"index.php?mode=$mode&mass_import=$earea\"'>";
				}
				else
				{
					$titlecol = false;
					$title_query = lavu_query("select * from `$table` where _deleted != '1' limit 1");
					if(mysqli_num_rows($title_query))
					{
						$title_read = mysqli_fetch_assoc($title_query);
						if(isset($title_read['title'])) 
							$titlecol = "title";
						else if(isset($title_read['name'])) 
							$titlecol = "name";
						else if(isset($title_read[$col])) 
							$titlecol = $col;
						$col_parts = explode(" ",$col);
						for($n=0; $n<count($col_parts); $n++)
						{
							if(trim($col_parts[$n])!="" && isset($title_read[trim($col_parts[$n])]))
								$titlecol = trim($col_parts[$n]);
						}
					}
					
					echo "<table>";
					echo "<td valign='top'>";
					 
					echo "<script language='javascript'>";
					echo "table_col_selected = 0; ";
					echo "table_col_selected_title = ''; ";
					echo "function table_col_mouseover(colid) { ";
					echo "  if(colid!=table_col_selected) ";
					echo "    document.getElementById('tablecol_' + colid).style.backgroundColor = '#ccddff'; ";
					echo "} ";
					echo "function table_col_mouseout(colid) { ";
					echo "  if(colid!=table_col_selected) ";
					echo "    document.getElementById('tablecol_' + colid).style.backgroundColor = '#ffffff'; ";
					echo "} ";
					echo "function table_col_click(colid) { ";
					echo "  if(table_col_selected) ";
					echo "    document.getElementById('tablecol_' + table_col_selected).style.backgroundColor = '#ffffff'; ";
					echo "  table_col_selected = colid; ";
					echo "  table_col_selected_title = document.getElementById('tablecol_' + table_col_selected).innerHTML; ";
					echo "  document.getElementById('tablecol_' + colid).style.backgroundColor = '#99aaee'; ";
					echo "} ";
					echo "function link_col_click(colid) { ";
					echo "  apply_id = table_col_selected; ";
					echo "  if(apply_id) {set_sep = '=>'; set_title = table_col_selected_title; set_id = table_col_selected;} else {set_sep = '&nbsp;'; set_title = '&nbsp;'; set_id = ''; } ";
					echo "  if(set_id!='' && document.getElementById('form_linkto_' + colid).value==set_id) { ";
					echo "    set_sep = '&nbsp;'; set_title = '&nbsp;'; set_id = ''; ";
					echo "  } ";
					echo "  document.getElementById('linksep_' + colid).innerHTML = set_sep; ";
					echo "  document.getElementById('linkcol_' + colid).innerHTML = set_title; ";
					echo "  document.getElementById('form_linkto_' + colid).value = set_id; ";
					echo "} ";
					echo "</script>";
					
					$divheight = "400px";
					
					if(!$titlecol)
					{
						echo "Problem: Unable to locate titles";
					}
					else
					{
						echo "<div style='width:240px; height:$divheight; overflow:auto; border:solid 2px #999999'>";
						echo "<table>";
						$val_query = lavu_query("select * from `$table` where _deleted != '1' order by `$titlecol` asc");
						while($val_read = mysqli_fetch_assoc($val_query))
						{
							$val_title = $val_read[$titlecol];
							$val_id = $val_read['id'];
							echo "<tr><td id='tablecol_$val_id' style='cursor:pointer' onmouseover='table_col_mouseover($val_id)' onmouseout='table_col_mouseout($val_id)' onclick='table_col_click($val_id)'>".$val_title."</td></tr>";
						}
						echo "<tr><td id='tablecol_empty' style='cursor:pointer' onmouseover='table_col_mouseover(\"empty\")' onmouseout='table_col_mouseout(\"empty\")' onclick='table_col_click(\"empty\")'>[Empty Column]</td></tr>";
						echo "</table>";
						echo "</div>";
					}
					 
					echo "</td><td width='40'>&nbsp;</td><td valign='top'>";
					
					$link_query = lavu_query("select distinct `$col` as `$col` from `$marea` where _deleted!='1' and LEFT(`$col`,15)='{filter:linkto}' order by `$col` asc");
					echo "<div style='width:600px; height:$divheight; overflow:auto; border:solid 2px #999999'>";
					echo "<table>";
					$linkid = 0;
					$link_form = "";
					while($link_read = mysqli_fetch_assoc($link_query))
					{
						$fullterm = $link_read[$col];
						$term = substr($fullterm,15);
						$linkid++;
						echo "<tr style='cursor:pointer' onclick='link_col_click($linkid)'>";
						echo "<td>";
						if(trim($term)=="")
							echo "(blank)";
						else
							echo $term;
						echo "</td>";
						echo "<td width='30' align='center' id='linksep_$linkid'>&nbsp;</td>";
						echo "<td id='linkcol_$linkid'>&nbsp;</td>";
						echo "</tr>";
						$link_form .= "<input type='hidden' name='form_linkto_$linkid' id='form_linkto_$linkid' value=''>";
						$link_form .= "<input type='hidden' name='form_linkfrom_$linkid' id='form_linkfrom_$linkid' value=\"".str_replace("\"","&quot;",$fullterm)."\">";						
					}
					echo "</table>";
					echo "</div>";
					
					echo "</td>";
					echo "</table>";

					echo "<input type='button' value='Update' onclick='document.linkage.submit()'>";
					echo "<form name='linkage' method='post' action='index.php?mode=$mode&mass_import=$earea&action=link&col=$col&table=$table";
					if($maintable) echo "&maintable=$maintable";
					echo "'>";
					echo "<input type='hidden' name='linkage_count' value='$linkid'>";
					echo $link_form;
					echo "</form>";
				}
			}
		}
		else if(!$action)
		{
			echo "<form name='upload' method='post' enctype='multipart/form-data' action=''>";
			echo "<table><tr><td align='center' style='padding:1px 1px 4px 1px;'>You may import data by uploading a tab delimited text (.txt) file or an Excel 97 (.xls) file.</td></tr>";
			echo "<tr><td align='center'>Following successful upload of your file you will be prompted to assign columns</td></tr>";
			echo "<tr><td align='center'>of the parsed data to their respective fields in the POSLavu database.</td></tr>";
			echo "<tr><td align='center' style='padding:15px 1px 1px 40px;'>".speak("Upload your file here").": &nbsp; <input type='file' name='ifile' onchange='document.upload.submit()'></td></tr>";
			echo "</table>";
			
			$filelist = array();
			if ($handle = opendir($import_path)) {
				while (false !== ($file = readdir($handle))) {
					if ($file != "." && $file != "..") {
						$filelist[] = $file;
					}
				}
				closedir($handle);
			}
			sort($filelist);
			if (count($filelist) > 0) {
				echo "<br>".speak("Process existing files").":<br>";
				echo "<select onchange='window.location = \"index.php?mode=$mode&mass_import=$earea&fname=\" + this.value'>";
				echo "<option value=''></option>";
				for($f=0; $f<count($filelist); $f++)
				{
					$filename = $filelist[$f];
					echo "<option value='$filename'>$filename</option>";
				}
				echo "</select>";
			}
			echo "</form>";
		}
		
		$undo = gvar("undo");
		
		if($undo && $undo!="")
		{
			lavu_query("delete from `$earea` where `import`='$undo'");
			//lavu_query("update `$earea` set `_deleted`='1' where `import`='$undo'");
			echo "Records from import $undo undone / deleted<br><br>";
		}
		
/*		if($action=="import" || !$action)
		{
			$import_query = lavu_query("select * from `$earea` where `_deleted`!='1' and `import`!=''");
			if(mysqli_num_rows($import_query))
			{
				echo "<br><u>Imports:</u>";
				$imp_query = lavu_query("select distinct `import` as impid from `$earea` where `_deleted`!='1' and `import`!='' ORDER BY `id` DESC LIMIT 5");
				while($imp_read = mysqli_fetch_assoc($imp_query))
				{
					$impid = $imp_read['impid'];
					echo "<br><br>Import $impid ";
										
					echo "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
					//if((date("YmdHis") * 1) - ($impid * 1) < 120000)
					echo "<a style='cursor:pointer' onclick='if(confirm(\"Are you sure you want to delete these items?\")) window.location = \"index.php?mode=$mode&mass_import=$earea&undo=$impid\"'><font color='red'>(undo)</font></a>";

					/*$log_query = lavu_query("select * from `system_log` where `action`='import' and `info1`='$impid' orde n  r by id desc limit 1");
					if(mysqli_num_rows($log_query))
					{
						$log_read = mysqli_fetch_assoc($log_query);
						$import_status = $log_read['info2'];
						$import_linkage_cols = $log_read['info3'];
						$import_linkage_cols = explode(",",$import_linkage_cols);
						for($k=0; $k<count($import_linkage_cols); $k++)
						{
							$ilinkcol = explode("|",$import_linkage_cols[$k]);
							if(count($ilinkcol)>1)
							{
								$linkagecol = $ilinkcol[0];
								$linkagetable = $ilinkcol[1];
								$link_count_query = lavu_query("select * from `$earea` where _deleted!='1' and LEFT(`$linkagecol`,15)='{filter:linkto}'");
								$linkage_needed = mysqli_num_rows($link_count_query);
								if($linkage_needed)
								{
									echo "<br>Linkage needed: $linkagecol to $linkagetable - $linkage_needed ";
									echo "<a href='index.php?mode=$mode&mass_import=$earea&action=link&col=$linkagecol&table=$linkagetable'>(match)</a>";
								} 
							}
						}
					}*/
				//}	
			//}
		//}

		/*if(!$action && is_lavu_admin())
		{
			echo "<br><br><br><br>";
			echo "<a href='index.php?mode=$mode&mass_import=$earea&action=manage_filters'>Manage Field Assignment and Filters</a>";
			echo "<br><br>";
		}*/
	}
?>
