<?php
/**
 * Created by PhpStorm.
 * User: Leif
 * Date: 1/10/17
 * Time: 4:39 PM
 */
//error_log(__FILE__ . " " . __LINE__ . " " . print_r($_POST,1));
session_start(); //call session_start before everything else.
require_once(dirname(__FILE__) . "/core_functions.php");
require_once(dirname(__FILE__) . "/chain_functions.php");
lavu_select_db(sessvar("admin_database"));
$company_id = sessvar('admin_companyid');
if(count($_POST) > 1){ //form was submitted to reach here.
	$validationFunc = 'validateAssociatedArrayElementsForInsert';
	$exchange_rates_to_insert = getAllExchangeRatesToInsert($company_id, $_POST, $validationFunc);
	if(isset($exchange_rates_to_insert)) {
		$query = "INSERT INTO `poslavu_MAIN_db`.`master_currencies` (`_enabled`,`country_region_id`,`_archived`, `master_id`, `country_region_name`) VALUES ".$exchange_rates_to_insert;
		error_log(__FILE__ . " " . __LINE__ . "\n\n\n\n Query to insert===>: " . $query ."\n\n\n\n");
		$query_result = mlavu_query($query);
		if(lavu_dberror()){
			error_log(__FILE__ . "  " . __LINE__ . " LAVU DB ERROR: " . lavu_dberror());
		}
	}

	$validationFunc = 'validateAssociatedArrayElementsForUpdate';
	$exchange_rates_to_update = getAllExchangeRatesToUpdate($company_id, $_POST, $validationFunc);
	if(isset($exchange_rates_to_update)){
		foreach($exchange_rates_to_update as $key=>$val)
		{
			if (empty($key) || !is_numeric($key))
			{
				continue;
			}

			$query = "UPDATE `poslavu_MAIN_db`.`master_currencies` SET " . $val ." WHERE `id`= $key";
			$query_result = mlavu_query($query);
			if(lavu_dberror()){
				error_log(__FILE__ . "  " . __LINE__ . " LAVU DB ERROR: " . lavu_dberror());
			}
		}
	}
	header("Location: " . $_POST['returnURL'] . "&refresh_for_save=1");
}

/**********************************************************************************************************************/
/**********************************************************************************************************************/
/*                              BEGIN FUNCTIONS                                                                       */
/**********************************************************************************************************************/
/**********************************************************************************************************************/

function getAllExchangeRatesToInsert($company_id, $postVars, $validationFunc){ //returns a comma separated string using implode on an array
	$submissionString = "";
	$usedKeySubstrings = array();
	foreach($postVars as $key=>$val){

		$last_underscore_index = strrpos($key, "_");
		if($last_underscore_index !== 0) {//Remove everything that doesn't have an underscore in it.
			$keySubstring = substr($key, $last_underscore_index + 1); //Gets everything after the last occurrence of the '_' character.
			if (!in_array($keySubstring, $usedKeySubstrings) && substr($key, 0, 5) === "ic_1_") {
				$associatedArrayElements = getAllAssociatedArrayElements($postVars, $keySubstring);
				if ($validationFunc($associatedArrayElements)) { //We only want to use this if the fields are valid //TODO
					$enabled = getValueFromKeyBeginningWith($associatedArrayElements, "ic_1_enabled");
					$master_currency_code = getValueFromKeyBeginningWith($associatedArrayElements, "ic_1_master_currency_code"); //TODO get code-id
					$archive = getValueFromKeyBeginningWith($associatedArrayElements, "ic_1_delete");
					$master_id = $company_id;
					$country_region_name = getDisplayableCurrencyNameOnlyFromCurrencyId($master_currency_code);
//					$id = getValueFromKeyBeginningWith($associatedArrayElements, "ic_1_id");
					$submissionString .= " ('$enabled', '$master_currency_code', '$archive', $master_id, '$country_region_name'),";
				}

				$usedKeySubstrings[] = $keySubstring;
			}
		}
	}
//	remove the last comma from the string if we have some result, otherwise return null.
	if(strlen($submissionString) > 0){
		return rtrim($submissionString, ",");
	}
	return null;
}

function getAllExchangeRatesToUpdate($company_id, $postVars, $validationFunc){
	$submissionString = array();
	$usedKeySubstrings = array();
	$updateCurrencyInfo = array();
	foreach($postVars as $key=>$val){

		$last_underscore_index = strrpos($key, "_");
		if($last_underscore_index !== 0) {//Remove everything that doesn't have an underscore in it.
			$keySubstring = substr($key, $last_underscore_index + 1); //Gets everything after the last occurrence of the '_' character.

			if (!in_array($keySubstring, $usedKeySubstrings)) {
				$associatedArrayElements = getAllAssociatedArrayElements($postVars, $keySubstring);
				if ($validationFunc($associatedArrayElements)) { //We only want to use this if the fields are valid
					$enabled = getValueFromKeyBeginningWith($associatedArrayElements, "ic_1_enabled");
//					$master_currency_code = getValueFromKeyBeginningWith($associatedArrayElements, "ic_1_master_currency_code");
//					$archive = getValueFromKeyBeginningWith($associatedArrayElements, "ic_1_delete");
					$id = getValueFromKeyBeginningWith($associatedArrayElements, "ic_1_id");
//					$master_name = getDisplayableCurrencyNameOnlyFromCurrencyId($master_currency_code);
					$submissionString[$id] = " `_enabled`='$enabled'";
					if($enabled==0){
						$updateCurrencyInfo[]=$id;
					}
				}
				$usedKeySubstrings[] = $keySubstring;
			}
		}
	}
	
	if(count($updateCurrencyInfo)>0){
		removeCurrencyFromLocations($updateCurrencyInfo);
	}
	
//	remove the last comma from the string if we have some result, otherwise return null.
	if(count($submissionString) > 0){
		return $submissionString;
	}
	return null;
}
//remove restaurants currency information.
function removeCurrencyFromLocations($updateCurrencyInfo){
	$currencyIdArray = array();
	$masterCurrenciesStr = implode(",", $updateCurrencyInfo);
	$queryCountryRegionId = " SELECT A.country_region_id, B.alphabetic_code FROM poslavu_MAIN_db.master_currencies as A JOIN poslavu_MAIN_db.country_region as B ON (A.country_region_id=B.id) where A.id in ($masterCurrenciesStr) ";
	$result = mlavu_query($queryCountryRegionId);
	if(lavu_dberror()){
		error_log(__FILE__ . "  " . __LINE__ . " LAVU DB ERROR: " . lavu_dberror());
	}
	else{
		if(mysqli_num_rows($result)>0){
			while ($countryCurrencyId = mysqli_fetch_assoc($result)) {
				$currencyIdArray[] = $countryCurrencyId['country_region_id'];
			}
		}
	}
	
	$dataNameArray = array();
	$query = " select id, data_name from poslavu_MAIN_db.restaurants where chain_id=(select DISTINCT B.chain_id from poslavu_MAIN_db.master_currencies as A JOIN poslavu_MAIN_db.restaurants as B ON (A.master_id=B.id) where A.id in ($masterCurrenciesStr)) ";
	$result = mlavu_query($query);
	if(lavu_dberror()){
		error_log(__FILE__ . "  " . __LINE__ . " LAVU DB ERROR: " . lavu_dberror());
	}
	else{
		if(mysqli_num_rows($result)>0){
			while ($region_info = mysqli_fetch_assoc($result)) {
				$dataNameArray[] = $region_info['data_name'];
			}
		}
	}
	//error_log(print_r($dataNameArray,1));
	//exit;
	if(count($dataNameArray)>0){
		$currencyCodeStr="";
		foreach($currencyIdArray as $key => $currencyCode ){
			if($currencyCodeStr!=""){
				$currencyCodeStr .=",'".$currencyCode."'";
			}else{
				$currencyCodeStr .="'".$currencyCode."'";
			}
		}
		foreach ($dataNameArray as $key2 => $restaurantDB){
			$query = (" SELECT setting, value  FROM `poslavu_".$restaurantDB."_db`.`config` where value in (".$currencyCodeStr.") AND type='location_config_setting' and setting='primary_currency_code' ");
			$result = lavu_query($query);
			if(lavu_dberror()){
				error_log(__FILE__ . "  " . __LINE__ . " LAVU DB ERROR: " . lavu_dberror());
			}
			else{
				if(mysqli_num_rows($result)>0){
					lavu_query(" UPDATE `poslavu_".$restaurantDB."_db`.`config` SET value=''  WHERE type='location_config_setting' and setting='primary_currency_code' ");
					
					lavu_query(" UPDATE `poslavu_".$restaurantDB."_db`.`locations` SET monitary_symbol='' ");
				}
			}
		}
	}
}

//Gets elements which are associated with a single row in the submit exchange rates page.
function getAllAssociatedArrayElements($postVars, $keySubstring){
	$associatedArrayElements = array();
	foreach($postVars as $key=>$val){
		$currentKeyNumber = substr($key, strrpos($key, "_") + 1);
		if(substr($key,0,5) == "ic_1_" && $keySubstring == $currentKeyNumber){
			$associatedArrayElements[$key] = $val;
		}
	}
	return $associatedArrayElements;
}

//validateAssociatedArrayElements Returns True if the elements are valid, false if not.
function validateAssociatedArrayElementsForInsert($associatedArrayElements){
	if(count($associatedArrayElements) != 4){
		return false;
	}

//	$enabled = getValueFromKeyBeginningWith($associatedArrayElements, "ic_1_enabled");
	$master_currency_code = getValueFromKeyBeginningWith($associatedArrayElements, "ic_1_master_currency_code");
	$idVal = getValueFromKeyBeginningWith($associatedArrayElements, "ic_1_id");
	$deleteVal = getValueFromKeyBeginningWith($associatedArrayElements, "ic_1_delete");

	if($idVal != "" || $idVal == "0"){
		return false;
	}
	if(!isset($master_currency_code) || $master_currency_code == "0"){
		return false;
	}
	if(!isset($deleteVal) || $deleteVal == '1'){
		return false;
	}
	return true;
}

function validateAssociatedArrayElementsForUpdate($associatedArrayElements){
	$countElements = count($associatedArrayElements);
	if($countElements != 4 && $countElements != 3){
		return false;
	}
//	$enabled = getValueFromKeyBeginningWith($associatedArrayElements, "ic_1_enabled");
//	$master_currency_code = getValueFromKeyBeginningWith($associatedArrayElements, "ic_1_master_currency_code");
//	$idVal = getValueFromKeyBeginningWith($associatedArrayElements, "ic_1_id");
//	$deleteVal = getValueFromKeyBeginningWith($associatedArrayElements, "ic_1_delete");
//	if(!isset($master_currency_code) || $master_currency_code == "0"){
//		return false;
//	}
	return true;
}

function getValueFromKeyBeginningWith($associativeArray, $keySubstring){
	foreach($associativeArray as $key=>$val){
		if(substr($key,0,strlen($keySubstring)) == $keySubstring){
			return $val;
		}
	}
	return null;
}

function isValidDate($date) {
	if (preg_match("/^(0*[1-9]|1*[0-2])\/(0*[1-9]|[1-2]*[0-9]|3*[0-1])\/[0-9]{1,2}$/",$date))
	{
		return true;
	} else {
		return false;
	}
}

function isValidRate($rate) {
	if(preg_match('/^(\d)+\.?(\d)*$/',$rate)){
		return true;
	}else{
		return false;
	}
}