inventory_window_exists = false;

function set_info_location()
{
	if(inventory_window_exists)
	{
		iwinpos = findPos(document.getElementById("main_content_area"));
		var topy = iwinpos[1] - 25;
		var leftx = iwinpos[0];
		var widthx = document.getElementById("main_content_area").offsetWidth;
		var areawidth = document.getElementById("info_area_table").offsetWidth;
		
		var top = document.body.scrollTop;
		if(top > topy)
			gotoy = top - topy;
		else
			gotoy = 0;
		
		gotox = 0 + (widthx / 2) - (areawidth / 2);
		
		if(document.getElementById("info_window_bg").style.visibility=="visible")
		{
			//document.getElementById("info_window_bg").style.top = gotoy;
			//document.getElementById("info_window_bg").style.left = gotox;
		}
		if(document.getElementById("info_window").style.visibility=="visible")
		{
				
			//document.getElementById("info_window").style.top = gotoy;
			//document.getElementById("info_window").style.left = gotox;
		}
	}
}

function findPos(obj) {
	var curleft = curtop = 0;
	if (obj.offsetParent) {
		do {
			curleft += obj.offsetLeft;
			curtop += obj.offsetTop;
		} while (obj = obj.offsetParent);
	}
	return new Array(curleft,curtop);
}


function show_info(content)
{
	//set_info_location();	
	
	content = unescape(content);
	content = content.replace(/\+/g, " ");
	document.getElementById("info_area").innerHTML = content;
	info_visibility("on");
	set_info_location();
}

function info_visibility(setvis)
{
	infowin = document.getElementById("info_window");
	infowinbg = document.getElementById("info_window_bg");
	if(infowin.style.visibility=="hidden" || setvis=="on")
	{
		infowin.style.visibility = "visible";
		infowinbg.style.visibility = "visible";
		
	}
	else
	{
		infowin.style.visibility = "hidden";
		infowinbg.style.visibility = "hidden";
	}
}
