<?php
	interface OutputableChange {
		function toHTML();
	}

	abstract class Change implements OutputableChange {
		protected
			$contents;

		protected function __construct( $contents='' ){
			$this->contents = $contents;
		}

		public function setContents( $contents ) {
			$this->contents = $contents;
		}

		public function contents() {
			return $this->contents;
		}
	}

	class FixChange extends Change {
		public function __construct( $contents ){
			parent::__construct( $contents );
		}

		public function toHTML(){
			return '<fix>Fixed: ' . $this->contents() . '</fix>';
		}
	}

	class AddedChange extends Change {
		public function __construct( $contents ){
			parent::__construct( $contents );
		}

		public function toHTML(){
			return '<add>Added: ' . $this->contents() . '</add>';
		}
	}

	class RemovedChange extends Change {
		public function __construct( $contents ){
			parent::__construct( $contents );
		}
		public function toHTML(){
			return '<remove>Removed: ' . $this->contents() . '</remove>';
		}
	}

	class ModifiedChange extends Change {
		public function __construct( $contents ){
			parent::__construct( $contents );
		}
		public function toHTML(){
			return '<modify>Modified: ' . $this->contents() . '</modify>';
		}
	}

	class ChangeList implements OutputableChange {
		private
			$section_name,
			$list_of_changes;

		public function __construct( $section ){
			$this->section_name = $section;
			$this->list_of_changes = array();
		}

		public function addChange( Change $change ){
			if( !in_array($change, $this->list_of_changes)){
				$this->list_of_changes[] = $change;
			}
		}

		// public function removeChange( Change $change ){
		// 	if( in_array($change, $this->list_of_changes)){

		// 	}
		// }

		public function toHTML(){
			$result = '<h3>' . $this->section_name . ':</h3>';
			foreach( $this->list_of_changes as $change_row => $change ){
				$result .= $change->toHTML();
			}

			return $result;
		}
	}

	class ChangeNotes implements OutputableChange {
		private
			$title,
			$timestamp,
			$sections;

		public function __construct( $title='', $timestamp=null ){
			if( !$timestamp ){
				$timestamp = date('c');
			}
			$this->title = $title;
			$this->timestamp = $timestamp;
			$this->sections = array();
		}

		public function addSection( ChangeList $section ){
			if( !in_array($section, $this->sections)){
				$this->sections[] = $section;
			}
		}

		public function toHTML(){
			$result = '<fieldset class="patchnotes">';
			$result .= '<legend><h2>' . $this->title . 'Update <time datetime="'.$this->timestamp.'">' . '</time> Notes</h2></legend>';
			foreach( $this->sections as $section_row => $section ){
				$result .= $section->toHTML();
			}
			$result .= '</fieldset>';
			return $result;
		}
	}