<?php
	function lavu_process_log_enabled()
	{
		if(function_exists("my_poslavu_enable_process_logging"))
		{
			return my_poslavu_enable_process_logging();
		}
		return false;
	}
	
	function lavu_write_process_info($vars=array(),$use_conn_type="account",$method="db")
	{
		// disabled for all
		return;
		
		global $data_name;
		global $dataname;
		
		if(!lavu_process_log_enabled()) return;
				
		$vars['remote_addr'] = $_SERVER['REMOTE_ADDR'];
		$vars['datetime'] = date("Y-m-d H:i:s");
		$vars['ts'] = time();
		$vars['http_host'] = $_SERVER['HTTP_HOST'];
		$vars['request_uri'] = $_SERVER['REQUEST_URI'];
		$vars['forward_ip'] = (isset($_SERVER['HTTP_X_FORWARDED_FOR']))?$_SERVER['HTTP_X_FORWARDED_FOR']:"";
		$vars['remote_ip'] = ($vars['forward_ip']!="")?$vars['forward_ip']:$vars['remote_addr'];
		$vars['jcmode'] = reqvar("m", "");
		$vars['app_name'] = reqvar("app_name", "");
		$vars['app_version'] = reqvar("app_version", "");
		$vars['app_build'] = reqvar("app_build", "");
		$vars['data_name'] = (isset($data_name))?$data_name:((isset($dataname))?$dataname:"");
		$vars['connection_type'] = $use_conn_type;
		
		$pid = "no_pid";
		if($use_conn_type=="main") $pid_query = mlavu_query("SELECT CONNECTION_ID() as `pid`");
		else $pid_query = lavu_query("SELECT CONNECTION_ID() as `pid`");
		if(mysqli_num_rows($pid_query))
		{
			$pid_read = mysqli_fetch_assoc($pid_query);
			$pid = $pid_read['pid'];
		}
		$vars['pid'] = $pid;
		
		$str = lavu_process_encode_array($vars);
		if($method=="db")
		{
			$vars['info'] = $str;
			mlavu_query("insert into `poslavu_MAIN_db`.`monitor_query` (`pid`,`dataname`,`datetime`,`ts`,`info`) values ('[pid]','[data_name]','[datetime]','[ts]','[info]')",$vars);
		}
		else if($method=="file")
		{
			$subdir = "/home/poslavu/private_html/processlist/" . date("YmdHi");
			if(!is_dir($subdir)) mkdir($subdir,0755);
			$fp = fopen($subdir . "/".$pid.".txt","w");
			fwrite($fp,$str);
			fclose($fp);
		}
	}
	
	function lavu_read_process_info($pid,$prop="",$method="db")
	{
		$fstr = "";
		
		if($method=="db")
		{
			$pid_query = mlavu_query("select * from `poslavu_MAIN_db`.`monitor_query` where `pid`='[1]'",$pid);
			if(mysqli_num_rows($pid_query))
			{
				$pid_read = mysqli_fetch_assoc($pid_query);
				$fstr = $pid_read['info'];
			}
		}
		else if($method=="file")
		{
			$file_found = false;
			$min_offset = 0;
			while(!$file_found)
			{
				$fname = "/home/poslavu/private_html/processlist/".date("YmdHi",time() - 60 * $min_offset)."/".$pid.".txt";
				if(file_exists($fname))
				{
					$file_found = true;
				}
				else
				{
					$min_offset++;
					if($min_offset > 10) $file_found = true;
				}
			}
			if(file_exists($fname))
			{
				$fp = fopen($fname,"r");
				$fstr = fread($fp,filesize($fname));
				fclose($fp);			
			}
		}
		
		if($fstr!="")
		{
			$props = lavu_process_decode_string($fstr);
			if($prop!="" && isset($props[$prop]))
				return $props[$prop];
			else
				return $props;
		}
		else return "";
	}
	
	function lavu_process_encode_array($arr)
	{
		$str = "";
		foreach($arr as $k => $v)
		{
			$str .= "&" . urlencode($k) . "=" . urlencode($v) . "&\n";
		}
		return $str;
	}
	
	function lavu_process_decode_string($str)
	{
		$arr = array();
		$str_parts = explode("&",$str);
		for($i=0; $i<count($str_parts); $i++)
		{
			$inner_parts = explode("=",$str_parts[$i]);
			if(count($inner_parts)==2)
			{
				$k = urldecode($inner_parts[0]);
				$v = urldecode($inner_parts[1]);
				$arr[$k] = $v;
			}
		}
		return $arr;
	}
?>
