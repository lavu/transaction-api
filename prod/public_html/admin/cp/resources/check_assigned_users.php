<?php
	if (!isset($_SESSION)) {
		session_start();
	}

	require_once($_SERVER['DOCUMENT_ROOT']."/cp/resources/core_functions.php");
	
	$data_name = sessvar("admin_dataname");
	$count = 0;
	if(isset($_GET['row_id'])){
		$list_of_users = lavu_query('SELECT `id`, `role_id` FROM poslavu_'.$data_name.'_db.`users` WHERE `_deleted` = 0');
		while($each_user = mysqli_fetch_assoc($list_of_users))
		{
			if (!empty($each_user['role_id']))
			{
				if (strpos($each_user['role_id'], '|'))
				{
					$multiple_roles = explode(',', $each_user['role_id']);
					foreach ($multiple_roles as $rp)
					{
						$rp_parts = explode('|', $rp);
						$db_role_id = $rp_parts[0];
						if ($db_role_id == $_GET['row_id'])
						{
							$count++;
						}
					}
				}
			}
		}
 		if($count <= 0){
			echo "Success";
		}else{
			echo "Failure";
		}
	}
?>