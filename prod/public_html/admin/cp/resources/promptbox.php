<?php

global $promptbox_html;
global $hide_trial_extension;
global $promptboxes_to_display;
global $companyid;
global $cp_username;
global $cp_fullname;
global $cp_user_is_lavu_admin;

$next_promptbox = getNextPromptboxMap($promptboxes_to_display);

//error_log("DEBUG: next_promptbox=". print_r($next_promptbox, true));  //debug

// Hide/show logic implemented through inline styles

$lavu_admin_close_button_css = ($cp_user_is_lavu_admin) ? '' : 'display:none';
$trial_extension_text_and_button_css = ($hide_trial_extension) ? 'display:none' : '';

// Display Business Location prompt lightbox.

$promptbox_html = <<<HTML

			<style>

			@font-face {
				font-family: 'din';
				src: url(/assets/fonts/din/din-webfont.eot);
				src: url(/assets/fonts/din/din-webfont.eot?#iefix) format('embedded-opentype'),
				url(/assets/fonts/din/din-webfont.woff) format('woff'),
				url(/assets/fonts/din/din-webfont.ttf) format('truetype'),
				url(/assets/fonts/din/din-webfont.svg#dinregular) format('svg');
				font-weight: normal;
				font-style: normal;
			}

			@font-face {
				font-family: 'din';
				src: url(/assets/fonts/din_light/din_light-webfont.eot);
				src: url(/assets/fonts/din_light/din_light-webfont.eot?#iefix) format('embedded-opentype'),
				url(/assets/fonts/din_light/din_light-webfont.woff) format('woff'),
				url(/assets/fonts/din_light/din_light-webfont.ttf) format('truetype'),
				url(/assets/fonts/din_light/din_light-webfont.svg#din_lightregular) format('svg');
				font-weight: lighter;
				font-style: normal;
			}

			#darkenedbg {
				display: none;
				width: 100%;
				height: 100%;
				z-index: 10001;
				position: fixed;
				top: 0px;
				left: 0px;
				background-color: rgba(0,0,0,.6);
			}

			.promptbox {
				display: none;
				z-index: 10001;
				background-color: white;
				border-radius: 10px;
				box-shadow: 2px 2px black;
				position: fixed;
				width: 700px;
				top: 25%;
				left: 25%;
				margin: 0 auto;
				padding-bottom: 10px;
				font-family: Arial;
				font-size: 12pt;
				color: #8b8b8b;
			}

			#promptbox_close_button {
				cursor: pointer;
				width: 46px;
				height: 44px;
				position: absolute;
				top: 0;
				right: 0;
				color: #bbbbbb;
				font-size: 30pt;
				margin-top: 10px;
				margin-right: 10px;
			}

			#promptbox_submit {
				color: white;
			}

			#promptbox_submit:active {
				color: #bbbbbb;
				border-color: white;
			}

			.promptbox h1 {
				font-family: 'din', arial;
				font-weight: lighter;
				font-size: 21pt;
				border: 0px;
				border-bottom: solid 2px #accd52;
				padding-top: 30px;
				margin-left: 30px;
				margin-right: 30px;
				padding-bottom: 20px;
			}

			.promptbox hr {
				width: 640px;
				border: 0px;
				border-bottom: 2px dotted #accd52;
				margin-bottom: 20px;
			}

			.promptbox p {
				font-size: 12pt;
				font-family: 'din', arial;
				color: #bbbbbb;
				width: 500px;
			}

			.promptbox fieldset {
				border: 0px;
				display: inline-block; /* make it only as widt as inputs */
				background-color: #f4f4f4;
				margin-bottom: 1px;
				margin-top: 1px;
				overflow: none;
			}

			.promptbox fieldset input,
			.promptbox fieldset select {
				width: 250px;
				height: 35px;
				color: #8b8b8b;
				border-width: 2px;
				border-color: #8b8b8b;
				border-style: solid;  /* removed the inset shadow */
				border-radius: 5px;
			}

			.promptbox fieldset .two_inputs {
				width: 504px;
			}

			.promptbox_submit {
				cursor: pointer;
				margin: 25px auto;
				color: white;
				height: 35px;
				font-size: 12pt;
				background-color: #58595b;
				border-color: #58595b;
			}

			.promptbox a:link,
			.promptbox a:visited,
			.promptbox a:hover {
				color: #accd52;
				font-family: inherit;
				font-size: inherit;
			}

			.rounded-button {
				border-radius: 5px;
				display: inline-block;
				font-weight: normal;
				font-family: 'din', Verdana, Helvetica, sans-serif;
				margin: 0;
				text-transform: uppercase;
				text-decoration: none;
				-moz-box-sizing: border-box;
				box-sizing: border-box;
				vertical-align: middle;
				/*line-height: 3em;*/
				padding: 0 1.5em;
				height:54px;
				box-shadow:none;
				border: 2px solid;
				/*background: none;*/
			}

			.rounded-button:active {
				color: #bbbbbb;
				border-color: white;
			}

			.rounded-button:disabled {
				color: #bbbbbb;
				border-color: white;
			}

			.radio-button {
				font-weight: normal;
				font-family: 'din', Verdana, Helvetica, sans-serif;
				vertical-align: middle;
				height:35px;
				margin-left: 18%;
			}

			.radio-group {
				text-align:left;
				margin-bottom: 5%;
			}

			.radio-group label {
				color: #bbbbbb;
			}

			.clear {
				clear: both;
			}

			.green {
				color: #accd52;
			}

			</style>


			<div id="darkenedbg">

				<div class="promptbox" id="change_password">

					<img src="/cp/images/promptbox_close_btn.png" id="promptbox_close_button" onclick="closePromptbox('change_password')" style="{$lavu_admin_close_button_css}" />

					<h1>Update Password</h1>

					<p>Would you like to change your password?</p>
					<br>
					<br>

					<input type="button" id="promptbox_submit" class="promptbox_submit rounded-button" value="Change Password" onclick="toggleNewPasswordFields()" />
					<input type="button" id="promptbox_submit" class="promptbox_submit rounded-button" value="Keep Current Password" onclick="showPromptbox('{$next_promptbox['change_password']}'); return false" />
					<br>
					<br>
					<br>

					<div id="new_password_fields" style="display:none">

						<form name="changePasswordForm" method="post">

							<input type="hidden" name="mode" value="update_password" />
							<input type="hidden" name="username" value="{$cp_username}" />

							<fieldset>
								<input type="password" id="password1" name="password1" placeholder="NEW PASSWORD" required /><br>
								<input type="password" id="password2" name="password2" placeholder="CONFIRM PASSWORD" required /><br>
								<br>
								<input type="submit" id="change_password_button" name="submit" value="UPDATE PASSWORD" />
							</fieldset>
							<br>

						</form>

					</div>

				</div>

				<div class="promptbox" id="password_updated">

					<img src="/cp/images/promptbox_close_btn.png" id="promptbox_close_button" onclick="closePromptbox('change_password')" style="{$lavu_admin_close_button_css}" />

					<h1>Password Changed</h1>

					<p>Your password has been updated successfully.</p>

					<input type="button" id="promptbox_submit" class="promptbox_submit rounded-button" value="Continue" onclick="showPromptbox('{$next_promptbox['password_updated']}')" />

				</div>

				<div class="promptbox" id="password_not_updated">

					<img src="/cp/images/promptbox_close_btn.png" id="promptbox_close_button" onclick="closePromptbox('change_password')" style="{$lavu_admin_close_button_css}" />

					<h1>Password Not Changed</h1>

					<p>Your password was not updated.</p>

					<input type="button" id="promptbox_submit" class="promptbox_submit rounded-button" value="Continue" onclick="showPromptbox('{$next_promptbox['password_not_updated']}')" />

				</div>

				<div class="promptbox" id="business_location">

					<img src="/cp/images/promptbox_close_btn.png" id="promptbox_close_button" onclick="closePromptbox('business_location')" style="{$lavu_admin_close_button_css}" />

					<h1>Business Location</h1>

					<p>Enter the address of your location as it should appear on POS receipts.</p>
					<p>It will also be used for tax purposes.</p>

					<form name="businessLocationForm" id="businessLocationForm" method="post" onsubmit="updateBusinessLocation(this.name); return false">

						<input type="hidden" name="dataname" value="{$dataname}" />
						<input type="hidden" name="companyid" value="{$companyid}" />
						<input type="hidden" name="locationid" value="{$locationid}" />
						<input type="hidden" name="username" value="{$cp_username}" />
						<input type="hidden" name="fullname" value="{$cp_fullname}" />

						<fieldset>
							<select name="country" id="pb_country" required onchange="countrySelected(this.options[this.selectedIndex].value)">
								<option value="">COUNTRY</option>
								<option value="USA">United States</option>
								<option value="CAN">Canada</option>
								<option value="ABW">Aruba</option>
								<option value="AFG">Afghanistan</option>
								<option value="AGO">Angola</option>
								<option value="AIA">Anguilla</option>
								<option value="ALA">Åland Islands</option>
								<option value="ALB">Albania</option>
								<option value="AND">Andorra</option>
								<option value="ARE">United Arab Emirates</option>
								<option value="ARG">Argentina</option>
								<option value="ARM">Armenia</option>
								<option value="ASM">American Samoa</option>
								<option value="ATA">Antarctica</option>
								<option value="ATF">French Southern Territories</option>
								<option value="ATG">Antigua and Barbuda</option>
								<option value="AUS">Australia</option>
								<option value="AUT">Austria</option>
								<option value="AZE">Azerbaijan</option>
								<option value="BDI">Burundi</option>
								<option value="BEL">Belgium</option>
								<option value="BEN">Benin</option>
								<option value="BES">Bonaire, Sint Eustatius and Saba</option>
								<option value="BFA">Burkina Faso</option>
								<option value="BGD">Bangladesh</option>
								<option value="BGR">Bulgaria</option>
								<option value="BHR">Bahrain</option>
								<option value="BHS">Bahamas</option>
								<option value="BIH">Bosnia and Herzegovina</option>
								<option value="BLM">Saint Barthélemy</option>
								<option value="BLR">Belarus</option>
								<option value="BLZ">Belize</option>
								<option value="BMU">Bermuda</option>
								<option value="BOL">Bolivia, Plurinational State of</option>
								<option value="BRA">Brazil</option>
								<option value="BRB">Barbados</option>
								<option value="BRN">Brunei Darussalam</option>
								<option value="BTN">Bhutan</option>
								<option value="BVT">Bouvet Island</option>
								<option value="BWA">Botswana</option>
								<option value="CAF">Central African Republic</option>
								<option value="CCK">Cocos (Keeling) Islands</option>
								<option value="CHE">Switzerland</option>
								<option value="CHL">Chile</option>
								<option value="CHN">China</option>
								<option value="CIV">Côte d'Ivoire</option>
								<option value="CMR">Cameroon</option>
								<option value="COD">Congo, the Democratic Republic of the</option>
								<option value="COG">Congo</option>
								<option value="COK">Cook Islands</option>
								<option value="COL">Colombia</option>
								<option value="COM">Comoros</option>
								<option value="CPV">Cabo Verde</option>
								<option value="CRI">Costa Rica</option>
								<option value="CUB">Cuba</option>
								<option value="CUW">Curaçao</option>
								<option value="CXR">Christmas Island</option>
								<option value="CYM">Cayman Islands</option>
								<option value="CYP">Cyprus</option>
								<option value="CZE">Czech Republic</option>
								<option value="DEU">Germany</option>
								<option value="DJI">Djibouti</option>
								<option value="DMA">Dominica</option>
								<option value="DNK">Denmark</option>
								<option value="DOM">Dominican Republic</option>
								<option value="DZA">Algeria</option>
								<option value="ECU">Ecuador</option>
								<option value="EGY">Egypt</option>
								<option value="ERI">Eritrea</option>
								<option value="ESH">Western Sahara</option>
								<option value="ESP">Spain</option>
								<option value="EST">Estonia</option>
								<option value="ETH">Ethiopia</option>
								<option value="FIN">Finland</option>
								<option value="FJI">Fiji</option>
								<option value="FLK">Falkland Islands (Malvinas)</option>
								<option value="FRA">France</option>
								<option value="FRO">Faroe Islands</option>
								<option value="FSM">Micronesia, Federated States of</option>
								<option value="GAB">Gabon</option>
								<option value="GBR">United Kingdom</option>
								<option value="GEO">Georgia</option>
								<option value="GGY">Guernsey</option>
								<option value="GHA">Ghana</option>
								<option value="GIB">Gibraltar</option>
								<option value="GIN">Guinea</option>
								<option value="GLP">Guadeloupe</option>
								<option value="GMB">Gambia</option>
								<option value="GNB">Guinea-Bissau</option>
								<option value="GNQ">Equatorial Guinea</option>
								<option value="GRC">Greece</option>
								<option value="GRD">Grenada</option>
								<option value="GRL">Greenland</option>
								<option value="GTM">Guatemala</option>
								<option value="GUF">French Guiana</option>
								<option value="GUM">Guam</option>
								<option value="GUY">Guyana</option>
								<option value="HKG">Hong Kong</option>
								<option value="HMD">Heard Island and McDonald Islands</option>
								<option value="HND">Honduras</option>
								<option value="HRV">Croatia</option>
								<option value="HTI">Haiti</option>
								<option value="HUN">Hungary</option>
								<option value="IDN">Indonesia</option>
								<option value="IMN">Isle of Man</option>
								<option value="IND">India</option>
								<option value="IOT">British Indian Ocean Territory</option>
								<option value="IRL">Ireland</option>
								<option value="IRN">Iran, Islamic Republic of</option>
								<option value="IRQ">Iraq</option>
								<option value="ISL">Iceland</option>
								<option value="ISR">Israel</option>
								<option value="ITA">Italy</option>
								<option value="JAM">Jamaica</option>
								<option value="JEY">Jersey</option>
								<option value="JOR">Jordan</option>
								<option value="JPN">Japan</option>
								<option value="KAZ">Kazakhstan</option>
								<option value="KEN">Kenya</option>
								<option value="KGZ">Kyrgyzstan</option>
								<option value="KHM">Cambodia</option>
								<option value="KIR">Kiribati</option>
								<option value="KNA">Saint Kitts and Nevis</option>
								<option value="KOR">Korea, Republic of</option>
								<option value="KWT">Kuwait</option>
								<option value="LAO">Lao People's Democratic Republic</option>
								<option value="LBN">Lebanon</option>
								<option value="LBR">Liberia</option>
								<option value="LBY">Libya</option>
								<option value="LCA">Saint Lucia</option>
								<option value="LIE">Liechtenstein</option>
								<option value="LKA">Sri Lanka</option>
								<option value="LSO">Lesotho</option>
								<option value="LTU">Lithuania</option>
								<option value="LUX">Luxembourg</option>
								<option value="LVA">Latvia</option>
								<option value="MAC">Macao</option>
								<option value="MAF">Saint Martin (French part)</option>
								<option value="MAR">Morocco</option>
								<option value="MCO">Monaco</option>
								<option value="MDA">Moldova, Republic of</option>
								<option value="MDG">Madagascar</option>
								<option value="MDV">Maldives</option>
								<option value="MEX">Mexico</option>
								<option value="MHL">Marshall Islands</option>
								<option value="MKD">Macedonia, the former Yugoslav Republic of</option>
								<option value="MLI">Mali</option>
								<option value="MLT">Malta</option>
								<option value="MMR">Myanmar</option>
								<option value="MNE">Montenegro</option>
								<option value="MNG">Mongolia</option>
								<option value="MNP">Northern Mariana Islands</option>
								<option value="MOZ">Mozambique</option>
								<option value="MRT">Mauritania</option>
								<option value="MSR">Montserrat</option>
								<option value="MTQ">Martinique</option>
								<option value="MUS">Mauritius</option>
								<option value="MWI">Malawi</option>
								<option value="MYS">Malaysia</option>
								<option value="MYT">Mayotte</option>
								<option value="NAM">Namibia</option>
								<option value="NCL">New Caledonia</option>
								<option value="NER">Niger</option>
								<option value="NFK">Norfolk Island</option>
								<option value="NGA">Nigeria</option>
								<option value="NIC">Nicaragua</option>
								<option value="NIU">Niue</option>
								<option value="NLD">Netherlands</option>
								<option value="NOR">Norway</option>
								<option value="NPL">Nepal</option>
								<option value="NRU">Nauru</option>
								<option value="NZL">New Zealand</option>
								<option value="OMN">Oman</option>
								<option value="PAK">Pakistan</option>
								<option value="PAN">Panama</option>
								<option value="PCN">Pitcairn</option>
								<option value="PER">Peru</option>
								<option value="PHL">Philippines</option>
								<option value="PLW">Palau</option>
								<option value="PNG">Papua New Guinea</option>
								<option value="POL">Poland</option>
								<option value="PRI">Puerto Rico</option>
								<option value="PRK">Korea, Democratic People's Republic of</option>
								<option value="PRT">Portugal</option>
								<option value="PRY">Paraguay</option>
								<option value="PSE">Palestine, State of</option>
								<option value="PYF">French Polynesia</option>
								<option value="QAT">Qatar</option>
								<option value="REU">Réunion</option>
								<option value="ROU">Romania</option>
								<option value="RUS">Russian Federation</option>
								<option value="RWA">Rwanda</option>
								<option value="SAU">Saudi Arabia</option>
								<option value="SDN">Sudan</option>
								<option value="SEN">Senegal</option>
								<option value="SGP">Singapore</option>
								<option value="SGS">South Georgia and the South Sandwich Islands</option>
								<option value="SHN">Saint Helena, Ascension and Tristan da Cunha</option>
								<option value="SJM">Svalbard and Jan Mayen</option>
								<option value="SLB">Solomon Islands</option>
								<option value="SLE">Sierra Leone</option>
								<option value="SLV">El Salvador</option>
								<option value="SMR">San Marino</option>
								<option value="SOM">Somalia</option>
								<option value="SPM">Saint Pierre and Miquelon</option>
								<option value="SRB">Serbia</option>
								<option value="SSD">South Sudan</option>
								<option value="STP">Sao Tome and Principe</option>
								<option value="SUR">Suriname</option>
								<option value="SVK">Slovakia</option>
								<option value="SVN">Slovenia</option>
								<option value="SWE">Sweden</option>
								<option value="SWZ">Swaziland</option>
								<option value="SXM">Sint Maarten (Dutch part)</option>
								<option value="SYC">Seychelles</option>
								<option value="SYR">Syrian Arab Republic</option>
								<option value="TCA">Turks and Caicos Islands</option>
								<option value="TCD">Chad</option>
								<option value="TGO">Togo</option>
								<option value="THA">Thailand</option>
								<option value="TJK">Tajikistan</option>
								<option value="TKL">Tokelau</option>
								<option value="TKM">Turkmenistan</option>
								<option value="TLS">Timor-Leste</option>
								<option value="TON">Tonga</option>
								<option value="TTO">Trinidad and Tobago</option>
								<option value="TUN">Tunisia</option>
								<option value="TUR">Turkey</option>
								<option value="TUV">Tuvalu</option>
								<option value="TWN">Taiwan, Province of China</option>
								<option value="TZA">Tanzania, United Republic of</option>
								<option value="UGA">Uganda</option>
								<option value="UKR">Ukraine</option>
								<option value="UMI">United States Minor Outlying Islands</option>
								<option value="URY">Uruguay</option>
								<option value="UZB">Uzbekistan</option>
								<option value="VAT">Holy See (Vatican City State)</option>
								<option value="VCT">Saint Vincent and the Grenadines</option>
								<option value="VEN">Venezuela, Bolivarian Republic of</option>
								<option value="VGB">Virgin Islands, British</option>
								<option value="VIR">Virgin Islands, U.S.</option>
								<option value="VNM">Viet Nam</option>
								<option value="VUT">Vanuatu</option>
								<option value="WLF">Wallis and Futuna</option>
								<option value="WSM">Samoa</option>
								<option value="YEM">Yemen</option>
								<option value="ZAF">South Africa</option>
								<option value="ZMB">Zambia</option>
								<option value="ZWE">Zimbabwe</option>
								<option value="Other">Other</option>
							</select>
							<input type="text" name="zip" id="pb_zip" required placeholder="POSTAL CODE">
						</fieldset>

						<fieldset>
							<input type="text" name="address" id="pb_address" required placeholder="ADDRESS" class="two_inputs">
						</fieldset>

						<fieldset id="cityStateFieldSet">
							<input type="text" name="city" id="pb_city" required placeholder="CITY">
							<input type="text" name="state" id="pb_state" required placeholder="STATE/PROVINCE">
						</fieldset>

						<div>
							<input type="submit" id="promptbox_submit" class="promptbox_submit rounded-button" value="SUBMIT">
						</div>

					</form>

				</div>

				<div class="promptbox" id="trial_countdown">

						<img src="/cp/images/promptbox_close_btn.png" id="promptbox_close_button" onclick="closePromptbox('trial_countdown')" />

						<h1>Lavu Free Trial - {$trial_days_left} Days Left</h1>

						<h2 class="green">Special Offer: Keep Lavu FREE</h2>

						<p>Continue using Lavu for FREE after your trial ends by setting up credit card processing services with one of our preferred partners.</p>

						<form name="trialCountdownForm" method="post" onsubmit="setupIntegratedPayments(this.name); return false">

							<input type="hidden" name="dataname" value="{$dataname}" />
							<input type="hidden" name="companyid" value="{$companyid}" />
							<input type="hidden" name="locationid" value="{$locationid}" />
							<input type="hidden" name="username" value="{$cp_username}" />
							<input type="hidden" name="fullname" value="{$cp_fullname}" />

							<div>
								<input type="submit" id="promptbox_submit" class="promptbox_submit rounded-button" value="Setup Integrated Payments" />
							</div>

						</form>

						<hr>

						<p>If you would like to choose a different payment processor please <a href="/cp/?mode=billing&enter_cc=1">click here</a> to fill out your credit card details.  Monthly services fees and/or license fees may apply.</p>

				</div>

				<div class="promptbox" id="trial_lockout">

						<img src="/cp/images/promptbox_close_btn.png" id="promptbox_close_button" onclick="closePromptboxAndEnterCC('trial_lockout')" />

						<h1>Free Account On Hold - Action Required</h1>

						<p>To continue with your free account, <span style="{$trial_extension_text_and_button_css}">click "Request 7 Trial Day Extension" or</span> setup credit card processing services with one of our preferred partners.</p>

						<form name="trialLockoutForm" method="post" onsubmit="return false">

							<input type="hidden" name="dataname" value="{$dataname}" />
							<input type="hidden" name="companyid" value="{$companyid}" />
							<input type="hidden" name="locationid" value="{$locationid}" />
							<input type="hidden" name="username" value="{$cp_username}" />
							<input type="hidden" name="fullname" value="{$cp_fullname}" />

							<div>
								<input type="submit" id="request_trial_extension" name="request_trial_extension" class="promptbox_submit rounded-button" value="Request 7 Day Trial Extension" onclick="request7DayExtension('trialLockoutForm')" style="{$trial_extension_text_and_button_css}" />
								<input type="submit" id="promptbox_submit" name="promptbox_submit" class="promptbox_submit rounded-button" value="Setup Integrated Payments" onclick="setupIntegratedPayments('trialLockoutForm')" />
							</div>

						</form>

						<hr>

						<p>If you would like to choose a different payment processor please <a href="/cp/?mode=billing&enter_cc=1">click here</a> to fill out your credit card details.  Monthly services fees and/or license fees may apply.</p>

				</div>

				<div class="promptbox" id="setup_integrated_payments">

						<img src="/cp/images/promptbox_close_btn.png" id="promptbox_close_button" onclick="closePromptbox('setup_integrated_payments')" />

						<h1>Thank You</h1>

						<h2 class="green">You're in Business with Lavu... Absolutely Free!</h2>

						<p>The Lavu Sales team will contact you within one business day to get things going.</p>

						<!--<form name="integratedPaymentsForm" method="post" onsubmit="requestOfferEmailResend('integratedPaymentsForm'); return false">

							<input type="hidden" name="dataname" value="{$dataname}" />
							<input type="hidden" name="companyid" value="{$companyid}" />
							<input type="hidden" name="locationid" value="{$locationid}" />
							<input type="hidden" name="username" value="{$cp_username}" />
							<input type="hidden" name="fullname" value="{$cp_fullname}" />

							<fieldset>
								<p>If you do not receive an email within 5 minutes, please enter your email address below and click the re-send button.</p>
								<input type="text" id="resend_email" name="resend_email" placeholder="E-MAIL ADDRESS" required />
								<input type="button" id="promptbox_submit" class="promptbox_submit rounded-button" value="Re-send Email" />
							</fieldset>

						</form>-->

						<hr>

						<p>If you would like to choose a different payment processor please <a href="/cp/?mode=billing&enter_cc=1">click here</a> to fill out your credit card details.  Monthly services fees and/or license fees may apply.</p>

				</div>


				<div class="promptbox" id="requested_trial_extension">

						<img src="/cp/images/promptbox_close_btn.png" id="promptbox_close_button" onclick="closePromptbox('requested_trial_extension'); window.location.reload()" />

						<h1>Thank You!</h1>

						<p>Your free trial has been extended 7 days.  The page will refresh when you close this message.</p>

						<hr>

						<p>If you would like to choose a different payment processor please <a href="/cp/?mode=billing&enter_cc=1">click here</a> to fill out your credit card details.  Monthly services fees and/or license fees may apply.</p>

				</div>

				<div class="promptbox" id="freelavu_processor_changeaway">

					<h1>Warning</h1>

					<h2 class="green">Changing your Payment Gateway will<br>cancel your Free Lavu promomtion.</h2>

					<div>
						<input type="submit" id="request_trial_extension" name="request_trial_extension" class="promptbox_submit rounded-button" value="I Understand and Want to Proceed" onclick="closePromptbox('freelavu_processor_changeaway')" />
						<input type="submit" id="request_trial_extension" name="request_trial_extension" class="promptbox_submit rounded-button" value="I'll Keep Free Lavu and {$currGW}" onclick="closePromptboxAndChangeGateway('freelavu_processor_changeaway', '{$currGW}')" />
					</div>

					<hr>

					<p>If you change your Payment Gateway, you will be prompted to enter a credit card for monthly payments.</p>

				</div>

				<div class="promptbox" id="proposed_billing_change">

					<img src="/cp/images/promptbox_close_btn.png" id="promptbox_close_button" onclick="closePromptbox('proposed_billing_change'); showPromptbox('{$next_promptbox['proposed_billing_change']}')" style="{$lavu_admin_close_button_css}" />

					<p>
						<h1>Billing Change</h1>

						<p>To accept the billing changes discussed with {$billing_log_event['fullname']} from Lavu Sales, agree to the Terms of Service and click Continue.</p>

						<h2>{$proposed_billing_change['name']}</h2>

						<form name="pbcForm" method="post" onsubmit="return false">

						<input type="hidden" name="dataname" value="{$dataname}" />
						<input type="hidden" name="companyid" value="{$companyid}" />
						<input type="hidden" name="locationid" value="{$locationid}" />
						<input type="hidden" name="username" value="{$cp_username}" />
						<input type="hidden" name="fullname" value="{$cp_fullname}" />
						<input type="hidden" name="proposed_billing_change" value="{$proposed_billing_change['json-html']}" />

						<div class="radio-group">
							<input type="radio" name="accept_proposed_billing_changes" id="accept_proposed_billing_changes_y" class="radio-button" value="1" onclick="document.getElementById('pbc_accept').disabled = false">
							<label for="accept_proposed_billing_changes_y">Yes, I agree to the <a target="_blank" href="https://www.lavu.com/terms-of-service">terms of service</a> for discounted annual billing</label><br>

							<input type="radio" name="accept_proposed_billing_changes" id="accept_proposed_billing_changes_n" class="radio-button" value="0" onclick="document.getElementById('pbc_accept').disabled = false">
							<label for="accept_proposed_billing_changes_n">No, I do not want to change my billing</label><br>

							<input type="radio" name="accept_proposed_billing_changes" id="accept_proposed_billing_changes_s" class="radio-button" value="skip" onclick="document.getElementById('pbc_accept').disabled = false">
							<label for="accept_proposed_billing_changes_s">Skip this for now and I'll decide later</label>
						</div>

						<div>
							<input type="submit" id="pbc_accept" name="pbc_accept" class="promptbox_submit rounded-button" value="Continue" onclick="processBillingChange('pbcForm')" disabled />
						</div>

						</form>
					</p>

				</div>

			</div>

			<script>

			var cityStateFieldSet = document.getElementById("cityStateFieldSet");

			function countrySelected(country) {
				var stateControl = document.getElementById("pb_state");
				var newStateControl = null;

				cityStateFieldSet.removeChild(stateControl);

				switch (country) {
					case "USA":
					case "United States":
						newStateControl = document.createElement("select");
						newStateControl.setAttribute("id", "pb_state");
						newStateControl.setAttribute("name", "state");
						newStateControl.setAttribute("required", "");
						var placeHolderOption = document.createElement("option");
						placeHolderOption.text = "STATE";
						placeHolderOption.value = "";
						newStateControl.add(placeHolderOption);
						var stateList = getStateListUS();
						for (var stateCode in stateList) {
							placeHolderOption = document.createElement("option");
							placeHolderOption.text = stateList[stateCode];
							placeHolderOption.value = stateCode;
							newStateControl.add(placeHolderOption);
						}
						break;

					case "CAN":
					case "Canada":
						newStateControl = document.createElement("select");
						newStateControl.setAttribute("id", "pb_state");
						newStateControl.setAttribute("name", "state");
						newStateControl.setAttribute("required", "");
						var placeHolderOption = document.createElement("option");
						placeHolderOption.text = "PROVINCE";
						placeHolderOption.value = "";
						newStateControl.add(placeHolderOption);
						var stateList = getStateListCA();
						for (var stateCode in stateList) {
							placeHolderOption = document.createElement("option");
							placeHolderOption.text = stateList[stateCode];
							placeHolderOption.value = stateCode;
							newStateControl.add(placeHolderOption);
						}
						break;

					default:
						newStateControl = document.createElement("input");
						newStateControl.setAttribute("id", "pb_state");
						newStateControl.setAttribute("name", "state");
						newStateControl.setAttribute("placeholder", "STATE/PROVINCE");
						newStateControl.setAttribute("type", "text");
						newStateControl.setAttribute("value", "");
						break;
				}
				cityStateFieldSet.appendChild(newStateControl);
			}

			function getStateListUS() {
				return 	{
					AL : "Alabama",
					AK : "Alaska",
					AS : "American Samoa",
					AZ : "Arizona",
					AR : "Arkansas",
					CA : "California",
					CO : "Colorado",
					CT : "Connecticut",
					DE : "Delaware",
					DC : "District of Columbia",
					FM : "Federated States of Micronesia",
					FL : "Florida",
					GA : "Georgia",
					GU : "Guam",
					HI : "Hawaii",
					ID : "Idaho",
					IL : "Illinois",
					IN : "Indiana",
					IA : "Iowa",
					KS : "Kansas",
					KY : "Kentucky",
					LA : "Louisiana",
					ME : "Maine",
					MH : "Marshall Islands",
					MD : "Maryland",
					MA : "Massachusetts",
					MI : "Michigan",
					MN : "Minnesota",
					MS : "Mississippi",
					MO : "Missouri",
					MT : "Montana",
					NE : "Nebraska",
					NV : "Nevada",
					NH : "New Hampshire",
					NJ : "New Jersey",
					NM : "New Mexico",
					NY : "New York",
					NC : "North Carolina",
					ND : "North Dakota",
					MP : "Northern Mariana Islands",
					OH : "Ohio",
					OK : "Oklahoma",
					OR : "Oregon",
					PW : "Palau",
					PA : "Pennsylvania",
					PR : "Puerto Rico",
					RI : "Rhode Island",
					SC : "South Carolina",
					SD : "South Dakota",
					TN : "Tennessee",
					TX : "Texas",
					UT : "Utah",
					VT : "Vermont",
					VI : "Virgin Islands",
					VA : "Virginia",
					WA : "Washington",
					WV : "West Virginia",
					WI : "Wisconsin",
					WY : "Wyoming"
				};
			}

			function getStateListCA() {
				return {
					AB : "Alberta",
					BC : "British Columbia",
					MB : "Manitoba",
					NB : "New Brunswick",
					NL : "Newfoundland and Labrador",
					NS : "Nova Scotia",
					NT : "Northwest Territories",
					NU : "Nunavut",
					ON : "Ontario",
					PE : "Prince Edward Island",
					QC : "Quebec",
					SK : "Saskatchewan",
					YT : "Yukon"
				};
			}

			function updateBusinessLocation(formName) {
				var promptboxForm = $('form[name="'+formName+'"]');

				if ( ($.trim($("#pb_country").val()).length == 0) || ($.trim($("#pb_address").val()).length == 0) || ($.trim($("#pb_city").val()).length == 0) || ($.trim($("#pb_state").val()).length == 0) || ($.trim($("#pb_zip").val()).length == 0) ) {
					alert("Please fill in all address fields.");
					return false;
				}

				$.ajax( {
					url : "/cp/resources/promptbox_responder.php",
					type : "POST",
					data : "action=update_business_location&" + promptboxForm.serialize(),
					async : true,
					success : function (string) {
						returnVal = string;
					}
				} );

				// Display either countdown|lockout
				var selectedCountry = $('#pb_country option:selected').val();
				if ((selectedCountry == 'USA' || selectedCountry == 'CAN') && '{$next_promptbox['business_location']}' != '') {
					showPromptbox('{$next_promptbox['business_location']}');
				} else {
					closePromptbox('business_location');
				}

				return false;
			}

			function processBillingChange(formName, acceptedBillingChange) {
				var promptboxForm = $('form[name="'+formName+'"]');

				$.ajax( {
					url : "/cp/resources/promptbox_responder.php",
					type : "POST",
					data : "action=process_billing_change&" + promptboxForm.serialize(),
					async : true,
					success : function (string) {
						returnVal = string;
					}
				} );

				// Done
				closePromptbox('proposed_billing_change');
				showPromptbox('{$next_promptbox['proposed_billing_change']}');

				return false;
			}

			function setupIntegratedPayments(formName) {
				var promptboxForm = $('form[name="'+formName+'"]');

				$.ajax( {
					url : "/cp/resources/promptbox_responder.php",
					type : "POST",
					data : "action=setup_integrated_payments&" + promptboxForm.serialize(),
					async : true,
					success : function (retval) {
						var retval_split = retval.split(/[|]/, 2);
						// split input & populate email input
						console.log("(retval_split[0]="+ retval_split[0] +" retval_split[1]="+ retval_split[1] +" test="+ (retval_split[0] == "1" && retval_split[1] != "") );  //debug
						if (retval_split[0] == "1" && retval_split[1] != "") document.getElementById('resend_email').value = retval_split[1];
					}
				} );

				showPromptbox('setup_integrated_payments');
			}

			function request7DayExtension(formName) {
				var promptboxForm = $('form[name="'+formName+'"]');

				$.ajax( {
					url : "/cp/resources/promptbox_responder.php",
					type : "POST",
					data : "action=request_trial_extension&" + promptboxForm.serialize(),
					async : true,
					success : function (retval) {
						var retval_split = retval.split(/[|]/, 2);
						// split input & populate email input
						console.log("(retval_split[0]="+ retval_split[0] +" retval_split[1]="+ retval_split[1] +" test="+ (retval_split[0] == "1" && retval_split[1] != "") );  //debug
					}
				} );

				closePromptbox('trial_lockout');
				showPromptbox('requested_trial_extension');
			}

			var resend_email = document.getElementById('resend_email');
			var resend_email_button = document.getElementById('promptbox_submit');
			function requestOfferEmailResend(formName) {
				var promptboxForm = $('form[name="'+formName+'"]');

				if ( resend_email_button.disabled || resend_email.value == "" ) return;

				$.ajax( {
					url : "/cp/resources/promptbox_responder.php",
					type : "POST",
					data : "action=request_offer_email_resend&" + promptboxForm.serialize(),
					async : true,
					success : function (retval) {
						var retval_split = retval.split(/[|]/, 2);
						// split input & populate email input
						console.log("(retval_split[0]="+ retval_split[0] +" retval_split[1]="+ retval_split[1] );  //debug
						resend_email_button.disabled = true;
					}
				} );
			}

			function closePromptbox(divId) {
				$('#darkenedbg').css('display', 'none');
				$('#'+divId).css('display', 'none');
			}

			function closePromptboxAndEnterCC(divId) {
				closePromptbox(divId);
				window.location.replace('/cp/?mode=billing&enter_cc=1');
			}

			function closePromptboxAndChangeGateway(divId, processorName) {
				closePromptbox(divId);
				changeGateway(processorName);
			}

			function changeGateway(processorName) {
				$('#gateway').val(processorName);
			}

			function toggleNewPasswordFields() {
				if ( $("#new_password_fields").css('display') == 'none' || $("#new_password_fields").css('display') == '' ) {
					$("#new_password_fields").css('display', 'block');
				} else {
					$("#new_password_fields").css('display', 'none');
				}
			}

			function showPromptbox(divId) {
				switch (divId) {
					case "change_password":
					case "password_updated":
					case "password_not_updated":
					case "business_location":
					case "trial_countdown":
					case "trial_lockout":
					case "setup_integrated_payments":
					case "requested_trial_extension":
					case "freelavu_processor_changeaway":
					case "proposed_billing_change":
 						$('#darkenedbg').css('display', 'block');
						$("#change_password").css('display', 'none');
						$("#password_updated").css('display', 'none');
						$("#password_not_updated").css('display', 'none');
						$("#business_location").css('display', 'none');
						$("#trial_countdown").css('display', 'none');
						$("#trial_lockout").css('display', 'none');
						$("#setup_integrated_payments").css('display', 'none');
						$("#requested_trial_extension").css('display', 'none');
						$("#freelavu_processor_changeaway").css('display', 'none');
						$("#proposed_billing_change").css('display', 'none');
						$('#'+divId).css('display', 'block');
						break;

					default:
						break;
				}
			}

			showPromptbox("{$promptboxes_to_display[0]}");

			</script>

HTML;

//error_log("promptboxes_to_display[0]=". $promptboxes_to_display[0]);  //debug

/*
 * Iterate through the list of promptboxes to display and create a mapping of which promptbox comes next in the sequence for each promptbox.
 * The name of the promptbox in question the array key and the next promptbox in the sequence is the array value.
 */
function getNextPromptboxMap($promptboxes_to_display) {
	$next_promptbox_map = array();

	$current_promptbox = '';
	$last_promptbox = '';
	for ($i = 0; $i < count($promptboxes_to_display); $i++) {
		$current_promptbox = $promptboxes_to_display[$i];
		if (!empty($last_promptbox)) $next_promptbox_map[$last_promptbox] = $current_promptbox;
		$last_promptbox = $current_promptbox;
	}

	// We signify that the last promptbox is the last one by marking its next as blank.
	$next_promptbox_map[$last_promptbox] = '';

	return $next_promptbox_map;
}
