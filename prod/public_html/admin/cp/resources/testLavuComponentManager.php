<?php

require_once( dirname(__FILE__) . "/LavuComponentManager.php" );

// Test XML => Database
/*
$xml = <<<XML

<product>
	<tab>
		<name>member_list</name>
		<title>Member List</title>
		<type>webview</type>
		<background>http://fit.lavu.com/customer_info/bg.jpg</background>
		<area>
			<name>member_info</name>
			<src>http://fit.lavu.com/member_info/</src>
			<scrollable>0</scrollable>
			<coord_x>144</coord_x>
			<coord_y>144</coord_y>
			<width>1600</width>
			<height>1480</height>
			<z-index>12</z-index>
		</area>
		<area>
			<name>member_wrapper</name>
			<src>http://fit.lavu.com/member_wrapper/</src>
			<scrollable>0</scrollable>
			<coord_x>0</coord_x>
			<coord_y>0</coord_y>
			<width>680</width>
			<height>550</height>
			<z-index>1</z-index>
		</area>
	</tab>
	<tab>
		<title>POS</title>
		<type>pos</type>
	</tab>
</product>

XML;

$componentMgr = new LavuComponentManager('motivity');
$componentMgr->processXml($xml);

echo $componentMgr->getStructure();
*/

// Database => Test XML

$componentMgr = new LavuComponentManager('motivity');
echo $componentMgr->getXmlFromDatabase();


?>