<?php
	ini_set("display_errors",1);
	require_once(dirname(__FILE__)."/core_functions.php");
	
	/*
		If we are in development mode this is true.
	*/
	$dev=true;
	
	/* 
	
		This class contains all the information to auto batch credit cards at a time specified by a user. 
		
	*/
	class Action{
		
		private $result='';
		
		/*
			the point of this constructor is to make sure that it is ok to continue and actually perform an action
		*/
		function __construct($currentTime, $action) {
			
			global $result;
			if($currentTime== '')  // if no time is passed then die. 
				$result= "error::No Time received. ";							
			else{					//check to see if action desired is valid. 
				$numRows= mysqli_num_rows(mlavu_query("select * from `poslavu_MAIN_db`.`actions` where `action` ='[1]' ",$action));
				
				if(!$numRows)
					$result= "error::invalid action. ";							
				else	
					$result='success:: valid action';
			}	
		}
	
		public function auto_batch($dataname, $locationid){
					
			$detailsArray= $this->getDetails($dataname, $locationid);
			
			lavu_connect_dn($dataname, "poslavu_".$dataname."_db");
		
			$data_name= $dataname;
			
			if($detailsArray['dev']){	
				require_once(dirname(__FILE__)."/../../dev/lib/gateway_functions.php");
				require_once(dirname(__FILE__)."/../../dev/lib/mpshc_functions.php");
			}else{
				require_once(dirname(__FILE__)."/../../lib/gateway_functions.php");
				require_once(dirname(__FILE__)."/../../lib/mpshc_functions.php");
			}
			
			$get_location_info = lavu_query("SELECT * FROM `locations` WHERE `id` = '[1]'", $locationid);
			$location_info = mysqli_fetch_assoc($get_location_info);
			$get_location_config = lavu_query("SELECT `setting`, `value` FROM `config` WHERE `type` = 'location_config_setting' AND `location` = '[1]'", 		
			$locationid);
			while ($config_info = mysqli_fetch_assoc($get_location_config)) {
				$location_info[$config_info['setting']] = $config_info['value'];
			}

			$fromExecuteActions=true; //This variable is to tell settle_batch to give me output;
			
			$process_info = array();
			$process_info['mag_data'] 		= "";
			$process_info['reader'] 		= "";
			$process_info['card_number'] 	= "";
			$process_info['card_cvn'] 		= "";
			$process_info['name_on_card'] 	= "";
			$process_info['exp_month'] 		= "";
			$process_info['exp_year'] 		= "";
			$process_info['card_amount'] 	= "";
			$process_info['company_id'] 	= $detailsArray['companyid'];
			$process_info['loc_id'] 		= $locationid;
			$process_info['ext_data'] 		= "";
			$process_info['data_name'] 		= $data_name;
			$process_info['register'] 		= "";
			$process_info['register_name'] 	= "";
			$process_info['device_udid'] 	= "";
			$process_info['server_id'] 		= "-9000";
			$process_info['server_name'] 	= "Auto Batch";
			$process_info['device_time'] 	= "";
			$process_info['set_pay_type'] 	= "";
			$process_info['set_pay_type_id']= "";
			$process_info['for_deposit'] 	= "";

			
			include(dirname(__FILE__)."/../areas/reports/settle_batch.php");
			
		}
		
		/*
			This function pulls the details needed for the settle_batch file. This file requires
			the restaurant id, server id(user id), usernaem and dataname.
		*/
		private function getDetails($dataname, $locationid){
			$detailsArray=array();
		
			$restID= mysqli_fetch_assoc(mlavu_query("select `id`, `dev` from `poslavu_MAIN_db`.`restaurants` where `data_name`='[1]'", $dataname));	
			
			$detailsArray['companyid']= $restID['id'];
			$detailsArray['dev']= $restID['dev'];
			return $detailsArray;
		}
		
		//This is the callback function for ob_start. this file does the parsing of the return values of settle_batch;
		
		private function parseResults($buffer){
			global $result;
			
			if(stristr($buffer, 'Error')){
				$errorName= explode(":", $buffer);
				$result= $errorName[1];
				return;
			}
			else
				$result= $buffer;
		}
		
		/*
			This is my output function. the global variable result gets populated as the results from settle_batch get back. 
		*/
		public function getResult(){
			global $result;
			return $result;
		}
	}
	//Current time gets passed in from the cron job. 
	//For testing i am setting $currentTime manually.
	$currentTime=14;
	$action= 'auto_batch';
	
	//error checking this assumes (possibly badly) that the action saved in the db is valid and that we know what to do with it. 
	$ActionObj= new Action($currentTime, $action);
	
	if( stristr($ActionObj -> getResult(), 'error')){
		$error=explode('::', $ActionObj ->getResult());
		echo $error[1];
		exit;
	}
	//echo "no error";
	$actionInfoQuery= mlavu_query("select `dataname`, `locationid` from `poslavu_MAIN_db`.`actions` where `action`='[2]' and `time`= '[1]' ", $currentTime, $action);
	//echo "about to call userfunc";
	while($actionInfoArray =mysqli_fetch_assoc($actionInfoQuery)){

		require_once(dirname(__FILE__)."/../../dev/lib/gateway_settings.php");  
		//The reason this must be inluded outside the class is because if we include 
		//it inside the class then what will happen is that we wont have access to our global variables. 
		call_user_func(array($ActionObj, $action), $actionInfoArray['dataname'], $actionInfoArray['locationid']);
		
		$result= $ActionObj->getResult();
		//echo $result;
	}
	
?>