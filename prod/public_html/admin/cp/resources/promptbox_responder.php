<?php

ini_set("display_errors",1);

session_start();

$dataname = isset( $_POST['action'] ) ? $_POST['action'] : '';
$action = isset( $_POST['action'] ) ? $_POST['action'] : '';

if ( empty($dataname) ) {
	error_log( __FILE__ ." called with no dataname" );
	echo "0|ERROR No dataname";
}

require_once(dirname(__FILE__)."/core_functions.php");
require_once(dirname(__FILE__)."/../../sa_cp/billing/procareas/SalesforceIntegration.php");
require_once(dirname(__FILE__)."/../../manage/globals/email_addresses.php");


// TO DO : check session (?) for security


// Routing Logic

switch ( $action ) {
	case 'update_business_location':
		update_business_location($_POST);
		break;

	case 'process_billing_change':
		update_tos_date($_POST);
		break;

	case 'setup_integrated_payments':
		setup_integrated_payments($_POST);
		break;

	case 'request_trial_extension':
		request_trial_extension($_POST);
		break;

	case 'request_offer_email_resend':
		request_offer_email_resend($_POST);
		break;

	default:
		error_log( __FILE__ . " encountered unknown action: {$action}" );
		break;
}


// Functions

function update_business_location($data)
{
	$conn = ConnectionHub::getConn('rest');
	$conn->selectDN($data['dataname']);

	$result = $conn->LegacyQuery("UPDATE `poslavu_[dataname]_db`.`locations` SET `country`='[country]', `zip`='[zip]', `address`='[address]', `city`='[city]', `state`='[state]' WHERE `id`='[locationid]' AND `zip`='' AND `address`='' AND `city`='' AND `state`='' LIMIT 2", $data);  // Not checking country because it seems to get set to USA.  ("U-S-A... U-S-A...")
	if ($result === false) {
		error_log( __FILE__ ." got MySQL error during locations table update: ". $conn->error() );
		echo "0|ERROR: ". lavu_dberror();
	}
	else if ( $conn->affectedRows() > 0 ) {

		// Update session'd location_info
		$location_info = sessvar('location_info');
		$location_info['country'] = $data['country'];
		$location_info['zip'] = $data['zip'];
		$location_info['city'] = $data['city'];
		$location_info['state'] = $data['state'];
		set_sessvar('location_info', $location_info);

		// Create billing history event
		///$billing_log_created = create_billing_log_event('Free Lavu User Event in CP', 'User updated Business Location', $data);

		echo "1|Locations table updated successfully";

		SalesforceIntegration::createSalesforceEvent( $data['dataname'], 'update_opportunity', json_encode( array( 'User_Updated_Business_Location__c' => date('r') ) ) );
		SalesforceIntegration::createSalesforceEvent( $data['dataname'], 'update_account', json_encode( array( 'Address__c' => $data['address'], 'City__c' => $data['city'], 'State_Provence__c' => $data['state'], 'Zip_Postal_Code__c' => $data['zip'], 'Country__c' => $data['country'] ) ) );
	}
	else {
		echo "0|Locations table not updated";
	}
	set_sessvar('completed_promptbox', '1');
}

function update_tos_date($data)
{
	global $loggedin;
	global $loggedin_fullname;
	global $o_emailAddressesMap;

	$conn = ConnectionHub::getConn('poslavu');

	$q = $conn->LegacyQuery("SELECT * FROM `poslavu_MAIN_db`.`signups` WHERE `dataname`='[dataname]' AND `dataname`!='[]'", $data);
	if ($q === false || $conn->affectedRows() === -1) {
		error_log( __FILE__ .' encountered MySQL error: '. $conn->error() );
	}
	$signup = mysqli_fetch_assoc($q);

	// Accept the billing change
	if ($data['accept_proposed_billing_changes'] == '1') {
		// Calculate the new trial expiration date 7 days out and then update the databases.
		$data['tos_date'] = date('Y-m-d H:i:s');

		$result = $conn->LegacyQuery("UPDATE `poslavu_MAIN_db`.`payment_status` SET `tos_date` = '[tos_date]', `proposed_billing_change` = '' WHERE `dataname`='[dataname]'", $data);
		if ($result === false || $conn->affectedRows() === -1) {
			error_log( __FILE__ .' encountered MySQL error: '. $conn->error() );
		}
		else {
			create_billing_log_event("Accepted Proposed Billing Change", "User accepted proposed billing changes for {$data['proposed_billing_changes_name']} in CP promptbox", $data);
			create_billing_log_event("Agreed to Terms of Service", 'User agreed to TOS in CP promptbox', $data);

			// TO DO : this is where we'd automate changing the package level + ARB amount instead of emailing Billing to do it

			// Email Billing to make billing changes
			$o_emailAddressesMap->sendEmailToAddress("billing_notifications", "Customer Accepted Billing Change", "{$signup['company']} ({$data['dataname']}) accepted billing changes and agreed to TOS in CP\n\n{$data['proposed_billing_change']}\n\nPlease update their billing accordingly.");
		}
	}
	// Skip the Proposed Billing Change temporarily (prompted again next login)
	else if ($data['accept_proposed_billing_changes'] == 'skip') {
		create_billing_log_event("Skipped Proposed Billing Change", "User skipped proposed billing changes for {$data['proposed_billing_changes_name']} in CP promptbox", $data);
	}
	// Reject the Proposed Billing Change (blank out the proposed_billing_change in the db)
	else {
		$result = $conn->LegacyQuery("UPDATE `poslavu_MAIN_db`.`payment_status` SET `proposed_billing_change` = '' WHERE `dataname`='[dataname]'", $data);
		if ($result === false || $conn->affectedRows() === -1) {
			error_log( __FILE__ .' encountered MySQL error: '. $conn->error() );
		}

		$salesforce_baseurl = 'https://na42.salesforce.com';
		$salesforce_account_link = (empty($salesforce_baseurl) || empty($signup['salesforce_id'])) ? '' : "Salesforce Account link -- {$salesforce_baseurl}/{$signup['salesforce_id']}";
		$salesforce_opportunity_link = (empty($salesforce_baseurl) || empty($signup['salesforce_opportunity_id'])) ? '' : "Salesforce Opportunity link -- {$salesforce_baseurl}/{$signup['salesforce_opportunity_id']}";

		create_billing_log_event("Rejected Proposed Billing Change", "User rejected proposed billing changes for {$data['proposed_billing_changes_name']} in CP promptbox", $data);
		$o_emailAddressesMap->sendEmailToAddress("sales_pbc_rejections", "Customer Rejected Billing Change", "{$signup['company']} ({$data['dataname']}) rejected billing changes in CP\n\n{$data['proposed_billing_change']}\n\n{$salesforce_account_link}\n{$salesforce_opportunity_link}");
	}

	echo "1";
	set_sessvar('completed_promptbox', '1');
}

function setup_integrated_payments($data)
{
	// Get email address from signup record (and therefore Salesforce) that the email will be sent to.
	$result = mlavu_query("SELECT * FROM `poslavu_MAIN_db`.`signups` WHERE `dataname`='[dataname]'", $data);
	if ($result === false) error_log( __FILE__ .' encountered MySQL error: '. mlavu_dberror() );
	$signup = mysqli_fetch_assoc($result);

	echo "1|". $signup['email'];
	set_sessvar('completed_promptbox', '1');

	SalesforceIntegration::createSalesforceEvent( $data['dataname'], 'update_opportunity', json_encode( array( 'User_Wants_Integrated_Payments__c' => date('r') ) ) );
}

function request_trial_extension($data)
{
	$conn = ConnectionHub::getConn('poslavu');
	// Calculate the new trial expiration date 7 days out and then update the databases.
	$new_trial_end = date('Y-m-d', strtotime('+7 days') );

	$result = $conn->LegacyQuery("UPDATE `poslavu_MAIN_db`.`payment_status` SET `trial_end` = '[trial_end]' WHERE `dataname`='[dataname]'", array('trial_end' => $new_trial_end, 'dataname' => $data['dataname']) );
	if ($result === false || $conn->affectedRows() === -1) {
		error_log( __FILE__ .' encountered MySQL error: '. $conn->error() );
	}
	else {
		SalesforceIntegration::createSalesforceEvent( $data['dataname'], 'update_opportunity', json_encode( array( 'User_Requested_7_Day_Trial_Extension__c' => date('r') ) ) );
		create_billing_log_event("Extend Trial Out To {$new_trial_end}", 'User clicked "Request 7 Day Trial Extension" button in CP promptbox', $data);
		echo "1";
	}
	set_sessvar('completed_promptbox', '1');
}

function request_offer_email_resend($data)
{
	SalesforceIntegration::createSalesforceEvent( $data['dataname'], 'update_opportunity', json_encode( array( 'User_Requested_Offer_Email_Resend__c' => date('r') ) ) );

	echo "1|";
	set_sessvar('completed_promptbox', '1');
}

function create_billing_log_event($action, $details, $data)
{
	$data['username'] = !empty($_SESSION['posadmin_lavu_admin']) ? $_SESSION['admin_username']    : $data['username'];
	$data['fullname'] = !empty($_SESSION['posadmin_lavu_admin']) ? $_SESSION['posadmin_fullname'] : $data['fullname'];
	$data['datetime'] = date("Y-m-d H:i:s");
	$data['ipaddress'] = $_SERVER['X_FORWARDED_FOR'];
	$data['action'] = $action;
	$data['details'] = $details;

	$result = mlavu_query("INSERT `poslavu_MAIN_db`.`billing_log` (`datetime`, `dataname`, `restaurantid`, `username`, `fullname`, `ipaddress`, `action`, `details`) VALUES ('[datetime]', '[dataname]', '[companyid]', '[username]', '[fullname]', '[ipaddress]', '[action]', '[details]')", $data);
	if ($result === false) error_log( __FILE__ ." got MySQL error during billing_log table insert: ". mlavu_dberror() );

	return $result;
}
