<?php
function create_location_config_setting_form($locationid,$setting_list) {
	$str = "";
	
	for($i=0; $i<count($setting_list); $i++)
	{
		$config_title = $setting_list[$i]['title'];
		$config_setting = $setting_list[$i]['setting'];
		$fieldid = "frm_" . $config_setting;
		
		$read_value = "";
		$config_query = lavu_query("select * from `config` where `type`='location_config_setting' and `setting` LIKE '[1]'",$config_setting);
		if(mysqli_num_rows($config_query))
		{
			$config_read = mysqli_fetch_assoc($config_query);
			$read_value = $config_read['value'];
			$setting_exists = true;
		}
		else 
		{
			$read_value = "";
			$setting_exists = false;
		}
		
		$str .= "<br><br>";
		$str .= "<table>";
		
		$rstr = "";
		if(isset($_POST[$fieldid]))
		{
			if($setting_exists)
			{
				$success = lavu_query("update `config` set `value`='[1]' where `type`='location_config_setting' and `setting` LIKE '[2]'",$_POST[$fieldid],$config_setting);
			}
			else
			{
				$success = lavu_query("insert into `config` (`location`,`type`,`setting`,`value`) values ('[1]','location_config_setting','[2]','[3]')",$locationid,$config_setting,$_POST[$fieldid]);
			}
			if($success)
			{
				$rstr = "<td style='color:#008800;font-weight:bold'>Updated</td>";
				$read_value = $_POST[$fieldid];
			}
			else
			{
				$rstr = "<td style='color:#880000;font-weight:bold'>Failed to Update</td>";
			}
		}
		$value_escaped = str_replace("\"","&quot;",$read_value);
		$str .= "<tr><td align='right'>$config_title</td><td><input type='text' name='$fieldid' id='$fieldid' value=\"$value_escaped\"></td>";
		$str .= $rstr;
		
		$str .= "</tr>";
		$str .= "</table>";
	}
	return $str;
}
