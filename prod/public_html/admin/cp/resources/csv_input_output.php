<?php
//Written by Brian Dennedy

// Comma Seperated Values



//###############################################################################################################################################################################
//
// CSV FROM FILE TO DB
//
// filePath to the .csv file, table that it will be loaded into, fields that should have specific values (i.e. setting insert date etc.)
function performInsertForCSVFile($filePath, $table, $database = null){
	global $setDatabase;
	$setDatabase = !empty($database) && is_string($database) ? $database : false;

	global $dbConn;
	if($setDatabase){
		$dbConn = ConnectionHub::getConn('poslavu');
	}else{
		$dbConn = ConnectionHub::getConn('rest');
	}

	global $csvField2DBFieldMap;
	if(!isset($csvField2DBFieldMap)){ $csvField2DBFieldMap = array(); }

	global $defaultDBValuesMap;
	if(!isset($defaultDBValuesMap)){ $defaultDBValuesMap = array(); }

	global $disallowDuplicatesAcrossColumnsArr;
	if(!isset($disallowDuplicatesAcrossColumnsArr)) { $disallowDuplicatesAcrossColumnsArr = array(); }

	global $tableForCSV; global $fp;
	$tableForCSV = $table;

	if (! _openCSVFile($filePath) ) {echo "Error trying to open .csv file."; return false;}
	if( ($badRow = validateThatAllMandatoryFieldsHaveValuesReturnsFirstErrorRowElseTrue($fp)) !== true){
		if($badRow === 0){
			echo "Error on insert, mandatory column not included in title row.<br>";
		}else{
			echo "Error on insert, mandatory column not set at row: ".$badRow."<br>";
		}
		return 0;
	}

	
	if (! _setCSVTitlesForwardToData() ) {fclose($fp); return false;}
	_setColumnNamesForTable();//Used for field validation, etc.
	transposeCSVTitles2DBColumnNamesByMap();//The CSV titles may have a map to their corresponding db columns e.g. FirstName => 'f_name' so it is more clear to user.
	if (! _validateFieldNamesForDBInsert() ) {fclose($fp); return false;}
	$mysqlEscapedColumnNames=_getMySQLEscapedColumnsArr();//Being paranoid, verified that these columns exist anyways, so couldn't be injections.
	$totalAffectedRows = _transferCSVDataSectionIntoDatabase();
	fclose($fp);
	return $totalAffectedRows;//Return
}

function setCSVFieldToDBFieldMap($mapArr){ //TODO TEST THIS...
	global $csvField2DBFieldMap;
	$csvField2DBFieldMap = $mapArr;
}

//NEW
//Key is the sql table column name, the value is what it will be set to.
function setDefaultValuesForDBMap($defaultMapArr){
	global $defaultDBValuesMap;
	$defaultDBValuesMap = $defaultMapArr;
}
//The column of the db's name.
function setDisallowDuplicatesAcrossColumns($columnNames){
	global $disallowDuplicatesAcrossColumnsArr;
	$disallowDuplicatesAcrossColumnsArr = $columnNames;
}
//End-New

function _openCSVFile($filePath){
	global $fp;
	$cvsFieldNames = array();
	$fp = fopen($filePath, "r");
	return $fp ? true : false;
}
function _setCSVTitlesForwardToData(){
	global $fp;
	global $csvFilesTitleArr;
	$temp_csv_FilesTitleArr = fgetcsv($fp, 1000, ",");
	$csvFilesTitleArr = array();
	foreach($temp_csv_FilesTitleArr as $val){
		$csvFilesTitleArr[$val]=$val;
	}
	return $csvFilesTitleArr ? true : false;
}
function _setColumnNamesForTable(){
	global $tableForCSV;
	global $columnNamesInTable;
	global $setDatabase;
	$dbAndTable = $setDatabase ? '`'.$setDatabase.'`.`'.$tableForCSV.'`' : $tableForCSV;
	if($setDatabase){
		$result = mlavu_query("SELECT * FROM [1] LIMIT 1",$dbAndTable);
		if(!$result){error_log("Mysql error in ".__FILE__." -eg87o- mysql_error:".mlavu_dberror());}
	}else{
		$result = lavu_query("SELECT * FROM `[1]` LIMIT 1",$tableForCSV);
		if(!$result){error_log("Mysql error in ".__FILE__." -eg87o- mysql_error:".lavu_dberror());}
	}
	if(mysqli_num_rows($result)){ $columnNamesInTable = array_keys(mysqli_fetch_assoc($result)); return; }
	if($setDatabase){
		$result = mlavu_query("SHOW COLUMNS FROM [1]",$dbAndTable);
		if(!$result){error_log("Mysql error in ".__FILE__." -crsnohs- mysql_error:".mlavu_dberror());}
	}else{
		$result = lavu_query("SHOW COLUMNS FROM `[1]`",$tableForCSV);
		if(!$result){error_log("Mysql error in ".__FILE__." -crsnohs- mysql_error:".lavu_dberror());}
	}
	$columnsArr = array();
	while($currRow = mysqli_fetch_assoc($result)){
		$columnsArr[] = $currRow['Field'];
	}
	$columnNamesInTable = $columnsArr; return;
}
function transposeCSVTitles2DBColumnNamesByMap(){
	global $csvField2DBFieldMap;
	global $csvFilesTitleArr;
	$tempArrayForSwap = $csvFilesTitleArr;
	foreach($csvField2DBFieldMap as $key => $val){//key is old title, value is new title
		if(isset($csvFilesTitleArr[$key])){
			$tempArrayForSwap[$key] = $val;
		}
	}
	$csvFilesTitleArr = array();
	foreach($tempArrayForSwap as $key => $val){
		$csvFilesTitleArr[$val] = $val;
	}
}
function _validateFieldNamesForDBInsert(){
	global $csvFilesTitleArr;
	global $columnNamesInTable;
	global $csvField2DBFieldMap;
	//We just make sure that for every field in $csvFilesTitleArr that it actually exists as a column in the table where the columns been listed in $columnNamesInTable
	foreach($csvFilesTitleArr as $currTitle){

		if(!in_array($currTitle, $columnNamesInTable) && !in_array($csvField2DBFieldMap[$currTitle],$columnNamesInTable)){ echo "Error: could not find field named:'$currTitle' in field list."; return false; }
	}
	return true;
}
function _getMySQLEscapedColumnsArr(){
	global $csvFilesTitleArr;
	global $dbConn;

	$escapedTitlesArr = array();
	foreach($csvFilesTitleArr as $currTitle){
		$escapedTitlesArr[] = $dbConn->escapeString($currTitle);
	}
	return $escapedTitlesArr;
}
//Next Delegate
function _transferCSVDataSectionIntoDatabase(){
	global $dbConn;
	$totalAffectedRows = 0;
	while($currArrOfRows = _readCSVRowsToArrayAndForwardFilePtr()){
		$queryString = _convertArrayOfRowsIntoSanitizedInsertStatement($currArrOfRows);
		//$result = lavu_query($queryString);
		$result = $dbConn->query('write',$queryString);//Contains the database in the query.
		$totalAffectedRows += $dbConn->affectedRows();
		if(!$result){ error_log("mysql error in -chsis- mysql_error:".$dbConn->error()); return false;}
	}
	return $totalAffectedRows;
}
function _readCSVRowsToArrayAndForwardFilePtr($rowsPerInsert = 10){
	global $fp;
	global $csvFilesTitleArr;
	global $disallowDuplicatesAcrossColumnsArr;
	$returnRowsArr = array();
	$i = 0;
	$csvFilesTitleArrCount = count($csvFilesTitleArr);
	while( $i < $rowsPerInsert && ($currCSVRow = fgetcsv($fp, 1000, ","))){
		$j = 0;
		foreach($csvFilesTitleArr as $key => $currTitle){
			$currRowArr[$currTitle] = empty($currCSVRow[$j]) ? "" : $currCSVRow[$j];
			$j++;
		}
		if(!empty($disallowDuplicatesAcrossColumnsArr)){
			$isADuplicate = __wouldBeDuplicateRow($disallowDuplicatesAcrossColumnsArr, $currRowArr);
			if(!$isADuplicate){
				$returnRowsArr[] = $currRowArr;
			}else{
				continue;
			}
		}else{
			$returnRowsArr[] = $currRowArr;
		}
		$i++;
	}
	return empty($returnRowsArr) ? false : $returnRowsArr;
}
function __wouldBeDuplicateRow($disallowDuplicateColumns, &$csvRow){
	//echo "<pre>Calling __wouldBeDuplicateRow passing: \n".print_r($disallowDuplicateColumns,1)."\nAnd:\n".print_r($csvRow,1)."</pre>\n";
	global $setDatabase;
	global $dbConn;
	global $tableForCSV;
	global $defaultDBValuesMap;
	static $hasReportedError = false;
	$whereStatement = '';
	$iter = 0;
	foreach($disallowDuplicateColumns as $currDoNotDuplColumn){//Is the name of the actual table's column, not the .csv title.
		$csvRowValue = isset($csvRow[$currDoNotDuplColumn]) ? $csvRow[$currDoNotDuplColumn] : $defaultDBValuesMap[$currDoNotDuplColumn];
		$whereStatement .= '`'.$dbConn->escapeString($currDoNotDuplColumn)."`='".$dbConn->escapeString($csvRowValue)."'";
		if($iter++ < count($disallowDuplicateColumns) - 1){
			$whereStatement .= ' AND ';
		}
	}
	//echo "Where statement:".$whereStatement."<br>";
	$error_code = '';
	if($setDatabase){
		$error_code = '-dnor8-';
		$existsResult = mlavu_query("SELECT * FROM `[1]`.`[2]` WHERE $whereStatement", $setDatabase, $tableForCSV);
	}else{
		$error_code = '-wcnsc-';
		$existsResult = lavu_query("SELECT * FROM `[1]` WHERE $whereStatement", $tableForCSV);
	}
	if(!$existsResult){
		if(!$hasReportedError){//Don't want to see this 2000 times.
			error_log( "Mysql error $error_code :".lavu_dberror() . " " . mlavu_dberror() );
		}
		$hasReportedError = true;
	}
	else{
		if(mysqli_num_rows($existsResult)){
			return true;
		}else{
			return false;
		}
	}
	return true;//It's erroring out, we don't want the row.
}

/*
	#arrOfRows are the rows that will be used for the insert as read from the .csv.  They
	should be in the same order as were inserted in the .csv.  Often, there will only be
	one row at a time if certain filters are used.
 */
function _convertArrayOfRowsIntoSanitizedInsertStatement($arrOfRows){
	global $tableForCSV;
	global $dbConn;
	global $csvFilesTitleArr;//These are the titles of the columns as exists in the csv's first row.
	//global $setSpecificFeildsArr; //I Don't think this is being used anymore :/
	global $defaultDBValuesMap;//These are the fields that the backend server sets, not the client for certain columns.
	global $setDatabase;

	//We need to remove anything from the defaultDBValuesMap that has been specified by the import
	foreach($defaultDBValuesMap as $def_Key => $def_Val){
		if(isset($csvFilesTitleArr[$def_Key])){
			unset($defaultDBValuesMap[$def_Key]);
		}
	}

	#1.)Sanitize
	$tableSanitized = $dbConn->escapeString($tableForCSV);//This is not variable anyways, but out of principle.
	$databaseSanitizedPart = $setDatabase ? '`'.$dbConn->escapeString($setDatabase).'`.' : '';
	$sanitizedArrOfRows = array();
	foreach($arrOfRows as $currRow){
		$sanitizedRow = array();
		foreach($currRow as $column => $val){
			$sanitizedRow[$dbConn->escapeString($column)] = $dbConn->escapeString($val);
		}
		$sanitizedArrOfRows[] = $sanitizedRow;
	}

	#2.)Append the default pairs (titles to their values)
	$additionalColumnValues = array();
	$defaultTitleKeys = array();
	if(!empty($defaultDBValuesMap) && is_array($defaultDBValuesMap)){
		$additionalColumnValues = array_values($defaultDBValuesMap);
		$defaultTitleKeys = array_keys($defaultDBValuesMap);
	}

	#3.)Build query
	$allTitles = array_merge($csvFilesTitleArr, $defaultTitleKeys);
	$query = "INSERT INTO ".$databaseSanitizedPart."`$tableSanitized` (`".implode("`,`",$allTitles)."`) VALUES ";
	$rowCount = count($sanitizedArrOfRows);
	for($i = 0; $i < $rowCount; $i++){
		$currRow = array_merge($sanitizedArrOfRows[$i], $additionalColumnValues);
		$query .= "('" . implode("','",$currRow) . "'),";
	}
	$query=substr($query, 0, -1);//Remove last comma.
	return $query;
}

function validateThatAllMandatoryFieldsHaveValuesReturnsFirstErrorRowElseTrue($filePointer){
	global $sdie_mandatory_title_fields;//May be set in simple_display_import_export.php
	$titleRow = fgetcsv($filePointer, 1000, ",");
	$mandatoryIndexes = array();
	for($i = 0; $i < count($titleRow); $i++){
		$mandatoryIndexes[] = in_array($titleRow[$i], $sdie_mandatory_title_fields) ? true : false;
	}
	foreach($sdie_mandatory_title_fields as $currMandatoryTitle){
		if(!in_array($currMandatoryTitle, $titleRow)){
			return 0;
		}
	}
	//Checking all values that mandatory fields are met.
	if(!empty($sdie_mandatory_title_fields)){
		$k = 1;
		while( ($currCSVRow = fgetcsv($filePointer, 1000, ",")) ){
			for($j=0;$j < count($currCSVRow); $j++){
				if( $mandatoryIndexes[$j] && empty($currCSVRow[$j]) ){
					return $k;
				}
			}
			$k++;
		}
	}
	rewind($filePointer);
	return true;
}


//###############################################################################################################################################################################
//
// ARRAY TO CSV FILE  (TODO)
//
function getCSVStringFromArrays($inputArrOfArrs,$delimiter,$enclosure){
	$returnStr = '';
	$fp = tmpfile();
	foreach($inputArrOfArrs as $currLineAsArr){
		fputcsv($fp,$currLineAsArr,$delimiter,$enclosure);
	}
	rewind($fp);
	if ($fp) {
		while (($buffer = fgets($fp, 4096)) !== false) {
		    $returnStr .= $buffer;
		}
		fclose($fp);
	}
	return $returnStr;
}

?>
