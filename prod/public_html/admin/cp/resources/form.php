
<style>
.switch-field {
  font-family: "Lucida Grande", Tahoma, Verdana, sans-serif;
  overflow: hidden;
}

.switch-title {
  margin-bottom: 6px;
}

.switch-field input {
    position: absolute !important;
    clip: rect(0, 0, 0, 0);
    height: 1px;
    width: 1px;
    border: 0;
    overflow: hidden;
}

.switch-field2 input {
    position: absolute !important;
    clip: rect(0, 0, 0, 0);
    height: 1px;
    width: 1px;
    border: 0;
    overflow: hidden;
}

.switch-field label {
  float: left;
}

.switch-field label {
  display: inline-block;
  width: 60px;
  background-color: #white;
  color: rgba(0, 0, 0, 0.6);
  font-size: 14px;
  font-weight: normal;
  text-align: center;
  text-shadow: none;
  padding: 6px 14px;
  border: 1px solid rgba(0, 0, 0, 0.2);
  -webkit-box-shadow: inset 0 1px 3px rgba(0, 0, 0, 0.3), 0 1px rgba(255, 255, 255, 0.1);
  box-shadow: inset 0 1px 3px rgba(0, 0, 0, 0.3), 0 1px rgba(255, 255, 255, 0.1);
  -webkit-transition: all 0.1s ease-in-out;
  -moz-transition:    all 0.1s ease-in-out;
  -ms-transition:     all 0.1s ease-in-out;
  -o-transition:      all 0.1s ease-in-out;
  transition:         all 0.1s ease-in-out;
}

.switch-field2 label {
  display: inline-block;
  width: 60px;
  background-color: #white;
  color: rgba(0, 0, 0, 0.6);
  font-size: 14px;
  font-weight: normal;
  text-align: center;
  text-shadow: none;
  padding: 6px 14px;
  border: 1px solid rgba(0, 0, 0, 0.2);
  -webkit-box-shadow: inset 0 1px 3px rgba(0, 0, 0, 0.3), 0 1px rgba(255, 255, 255, 0.1);
  box-shadow: inset 0 1px 3px rgba(0, 0, 0, 0.3), 0 1px rgba(255, 255, 255, 0.1);
  -webkit-transition: all 0.1s ease-in-out;
  -moz-transition:    all 0.1s ease-in-out;
  -ms-transition:     all 0.1s ease-in-out;
  -o-transition:      all 0.1s ease-in-out;
  transition:         all 0.1s ease-in-out;
}

.switch-field label:hover {
    cursor: pointer;
}

.switch-field input:checked + label {
  background-color: #e4e4e4;
  -webkit-box-shadow: none;
  box-shadow: none;
}

.switch-field2:checked + label {
    background-color: #e4e4e4;
    -webkit-box-shadow: none;
    box-shadow: none;
}

.switch-field label:first-of-type {
  border-radius: 4px 4px 4px 4px;
  width: 112px;
}

.switch-field label:last-of-type {
  border-radius: 4px 4px 4px 4px;
  width: 112px;
}
.calendarImg{
	background: url('/cp/resources/tigra/img/cal.gif') 100% 50% no-repeat;
	padding-right: 20px;
	cursor: pointer;
}
</style>

<?php
	require_once(__DIR__."/action_tracker.php");
	require_once("/home/poslavu/private_html/rs_upload.php");


	$auto_go_to_loyaltree = false;
	$forward_url = (isset($forward_to))?$forward_to:"";

	if (!$in_lavu)
	{
		return;
	}

	function advance_digit($digit, $add, $max=100)
	{
		if ($max < 100)
		{
			$dig_m = $digit % 100;
			$dig_h = (($digit - $dig_m) / 100);
			$add_m = $add % 100;
			$add_h = (($add - $add_m) / 100);

			$h = ($dig_h + $add_h);
			$m = ($dig_m + $add_m);

			while($m >= $max)
			{
				$m -= $max;
				$h ++;
			}

			return (($h * 100) + $m);
		}
		else
		{
			return ($digit + $add);
		}
	}

	function create_help_text_for_form_element_seperator(&$a_style, &$s_help_text_class, &$s_settingname, &$s_tablename, $s_seperator_name, $s_global_help_text_show_script, &$s_extra_help_box, &$b_optional_seperator_descriptor, $s_filename)
	{
		// settings
		$s_tablename					= "dimensional_form_seperator_label";
		$s_settingname					= $s_seperator_name;
		$s_optional_descriptor_setting	= str_replace(" ", "_", $s_settingname)."_optional_descriptor";
		$s_optional_descriptor_class	= $s_optional_descriptor_setting;
		$s_optional_descriptor			= "";
		$i_helptext_id					= "help_text_seperator_".str_replace(" ", "_", $s_seperator_name);
		$s_help_text_class				= "help_text_dimensional_form_setting_".$i_helptext_id;

		// create the style
		$s_style = "";
		foreach ($a_style as $k => $v)
		{
			$s_style .= $k.":".$v.";";
		}

		// create the optional help_box descriptor
		if ($s_settingname!="" && $s_filename!="" && $s_tablename!="")
		{
			$a_optional_descriptors = ConnectionHub::getConn('rest')->getDBAPI()->getAllInTable("helpText", array( 'page' => $s_filename, 'table' => $s_tablename, 'setting' => $s_optional_descriptor_setting), TRUE);

			// replace $s_extra_help_box with the optional seperator descriptor, if it exists
			if (count($a_optional_descriptors) > 0)
			{
				$s_optional_descriptor = str_replace("display:none;", "", "<div style='{".$s_style."}'>").str_replace("not-empty", "not-empty ".$s_optional_descriptor_class, create_help_mark($s_filename, $s_tablename, $s_optional_descriptor_setting))."</div>
				<script type=\"text/javascript\">
					$(document).ready(function()
					{
						var help_texts = $(\"div.".$s_help_text_class."\");
						help_texts.show();
						help_texts.css({ display:\"inline-block\" });
					});
				</script>";
			}
		}
		$s_optional_descriptor = str_replace(array("\n", "\r", "\n\r"), "\n", $s_optional_descriptor);

		// if the optional help box exists and is set, then don't make the form help texts dependent on the toggler
		if (isset($a_optional_descriptors) && count($a_optional_descriptors) > 0 && $a_optional_descriptors[0]['contents'] !== "")
		{
			$b_optional_seperator_descriptor = TRUE;
		}

		// create the expansion help box
		$a_style['display'] = "none";
		$s_style = "";
		foreach ($a_style as $k => $v)
		{
			$s_style .= $k.":".$v.";";
		}
		$s_display = "";
		$s_html = "";
		$s_showscript = "<script type=\"text/javascript\">
				$(document).ready(function()
				{
					var help_texts = $(\"div.".$s_help_text_class."\");
					if (help_texts.children(\".not-empty\").length > 0) {
						$(\"#".$i_helptext_id."\").show();
					}
				});
			</script>";

		$s_onclick = "
			var help_texts = $(\"div.".$s_help_text_class."\");
			var jseperator = $(this);
			var on = jseperator.hasClass(\"on\");
			$.each(help_texts, function(k,v)
			{
				v = $(v);
				v.stop(true,true);
				if (!on)
				{
					v.show();
					v.css({ display: \"inline-block\" });
					v.hide();
					v.show(200);
				}
				else
				{
					v.hide(200);
				}
			});
			if (on)
			{
				jseperator.removeClass(\"on\");
			}
			else
			{
				jseperator.addClass(\"on\");
			}";

		$s_extra_help_box = get_help_mark_styling_using_html($s_display, $i_helptext_id, $s_html, $s_showscript, $s_onclick);
		if ($s_extra_help_box !== "")
		{
			$s_extra_help_box = "<div id='".$i_helptext_id."' style='".$s_style."'>".$s_extra_help_box."</div>";
		}
		$s_extra_help_box = str_replace(array("\n","\r","\n\r"), "\n", $s_extra_help_box);

		// merge the optional descriptor and the extra help text
		if ($b_optional_seperator_descriptor)
		{
			$s_extra_help_box = $s_optional_descriptor;
		}

		// styles
		$a_style['position'] = "relative";
		global $s_seperator_title;
		if (isset($s_seperator_title) && trim($s_seperator_title) !== "")
		{
			$a_style['right']	= "5px";
			$a_style['top']		= "3px";
		}
		$a_style['display'] = "inline-block";

		unset($a_style['left']);
	}

	function create_help_text_for_form_element($field, $a_extra_vars=NULL)
	{
		// get some globals
		global $s_global_help_text_show_script;
		global $s_help_text_prepend_settingname;
		global $s_help_text_class;

		if (!isset($s_help_text_class))
		{
			$s_help_text_class = "";
		}

		// parse extra variables
		$b_is_seperator = FALSE;
		$s_seperator_name = "";
		$s_seperator_title = "";
		if ($a_extra_vars!==NULL && is_array($a_extra_vars))
		{
			if (isset($a_extra_vars['is_seperator']))
			{
				$b_is_seperator = $a_extra_vars['is_seperator'];
			}
			if (isset($a_extra_vars['seperator_name']))
			{
				$s_seperator_name = $a_extra_vars['seperator_name'];
			}
			if (isset($a_extra_vars['seperator_title']))
			{
				$s_seperator_title = $a_extra_vars['seperator_title'];
			}
		}

		// set the help text variables
		$s_filename = "";
		$s_tablename = "";
		$s_settingname = "";
		$s_extra_help_box = "";
		$b_optional_seperator_descriptor = FALSE;

		assign_file_table_setting_names($field->name, debug_backtrace(), $s_filename, $s_tablename, $s_settingname);

		// set the help text style
		$a_style = array(
			'position'	=> "absolute",
			'left'		=> "5px"
		);

		// check for a seperator
		// modifies $a_style, $s_help_text_class, $s_settingname, $s_tablename, $s_extra_help_box, and $b_optional_seperator_descriptor
		if ($b_is_seperator)
		{
			create_help_text_for_form_element_seperator($a_style, $s_help_text_class, $s_settingname, $s_tablename, $s_seperator_name, $s_global_help_text_show_script, $s_extra_help_box, $b_optional_seperator_descriptor, $s_filename);
		}

	// check if this is a dimensional form thing
		$a_style['display'] = "inline-block";
		if (isset($s_help_text_class) && $s_help_text_class!=="" && !$b_optional_seperator_descriptor)
		{
			$a_style['display'] = "none";
		}

		// create the style string
		$s_style = "";
		foreach($a_style as $k => $v)
		{
			$s_style .= $k.":".$v.";";
		}

		// check for a global prepend
		if (isset($s_help_text_prepend_settingname))
		{
			$s_settingname = $s_help_text_prepend_settingname.$s_settingname;
		}

		// create the help text
		$s_helptext = "";
		if ($field->type == "submit")
		{
			$s_help_text_class = "";
		}
		else if ($s_settingname!="" && $s_tablename!="" && $s_filename!="" && $s_filename!=="form.php")
		{
			$s_helptext = create_help_mark($s_filename, $s_tablename, $s_settingname);
			$s_helptext = $s_extra_help_box."<div class='".$s_help_text_class."' style='".$s_style."'>".$s_helptext."</div>";
		}

		return $s_helptext;
	}

	function update_restaurant_location_data($rest_read, $loc_read)
	{
		$rsp = "";
		$restid = $rest_read['id'];
		$dataname = $rest_read['data_name'];
		$dbname = "poslavu_".$dataname."_db";

		$flist1 = array( "title", "address", "city", "state", "zip", "country", "timezone", "phone", "email", "website", "manager" );
		$flist2 = array( "taxrate", "net_path", "use_net_path", "component_package", "component_package2", "product_level", "gateway", "component_package_code" );
		$flist3 = array( "lavu_togo_name", "hours_of_operation" );
		$flist = array_merge($flist1, $flist2);

		$setlist = array(
			array("dataname",		$dataname),
			array("restaurantid",	$restid),
			array("locationid",		$loc_read['id']),
			array("last_activity",	$rest_read['last_activity']),
			array("created",			$rest_read['created']),
			array("disabled",		$rest_read['disabled'])
		);

		for($n = 0; $n < count($flist); $n++)
		{
			$fieldname = $flist[$n];
			$setlist[] = array($fieldname, $loc_read[$fieldname]);
		}

		for($n = 0; $n < count($flist3); $n++)
		{
			$flist_name = $flist3[$n];
			$config_query = mlavu_query("SELECT `value` FROM `[1]`.`config` WHERE `type` = 'location_config_setting' AND `setting` = '[2]' AND `location` = '[3]'", $dbname, $flist_name, $loc_read['id']);
			if ($config_query->num_rows)
			{
				$config_read = mysqli_fetch_assoc($config_query);
				$setlist[] = array($flist_name, $config_read['value']);
			}
		}

		$field_code = "";
		$value_code = "";
		$update_code = "";
		$update_vals = array();

		for ($n = 0; $n < count($setlist); $n++)
		{
			$fieldname = $setlist[$n][0];
			$fieldvalue = $setlist[$n][1];

			if ($field_code!="")
			{
				$field_code .= ",";
			}
			if ($value_code!="")
			{
				$value_code .= ",";
			}
			if ($update_code!="")
			{
				$update_code .= ",";
			}

			$update_vals[$fieldname] = $fieldvalue;
			$field_code .= "`$fieldname`";
			$value_code .= "'[$fieldname]'";
			$update_code .= "`$fieldname`='[$fieldname]'";
		}

		$exist_query = mlavu_query("SELECT * FROM `poslavu_MAIN_db`.`restaurant_locations` WHERE `dataname` = '[1]' AND `restaurantid` = '[2]' AND `locationid` = '[3]'", $dataname, $restid, $loc_read['id']);
		if ($exist_query->num_rows)
		{
			$exist_read = mysqli_fetch_assoc($exist_query);
			$update_vals['existid'] = $exist_read['id'];

			mlavu_query("UPDATE `poslavu_MAIN_db`.`restaurant_locations` SET ".$update_code." WHERE `id` = '[existid]'", $update_vals);
			$rsp .= "<font color='#000088'>Updating ".$dataname." - location: ".$loc_read['id']." - ".$loc_read['title']."</font><br>";
		}
		else
		{
			mlavu_query("insert into `poslavu_MAIN_db`.`restaurant_locations` (".$field_code.") values (".$value_code.")", $update_vals);
			$rsp .= "<font color='#008800'>Inserting into ".$dataname." - location: ".$loc_read['id']." - ".$loc_read['title']."</font><br>";
		}

		return $rsp;
	}

	function create_form_select($title, $name, $setvalue, $type, $property=false, $property2=false, $property3=false)
	{
		$str = "";
		$str .= "<select name='".$name."'>";
		$str .= "<option value=''>".$title."</option>";
		$options = array();
		if ($type=="numeric" || $type=="time" || $type=="time_no_late_night")
		{
			$max = ($type=="time" || $type=="time_no_late_night")?60:100;

			if ($property3 == false)
			{
				if ($property > $property2)
				{
					$property3 = -1;
				}
				else
				{
					$property3 = 1;
				}
			}

			if ($property3 > 0)
			{
				for ($i = $property; $i<=$property2; $i = advance_digit($i, $property3, $max))
				{
					$option = ($i * 1);
					$diff = strlen($property) - strlen($option);
					if ($diff > 0)
					{
						for($n = 0; $n < $diff; $n++)
						{
							$option = "0" . $option;
						}
					}
					$options[] = $option;
				}
			}
			else
			{
				for($i = $property; $i >= $property2; $i = advance_digit($i, $property3, $max))
				{
					$option = ($i * 1);
					$diff = strlen($property) - strlen($option);
					if ($diff > 0)
					{
						for($n = 0; $n < $diff; $n++)
						{
							$option = "0".$option;
						}
					}
					$options[] = $option;
				}
			}
		}
		else
		{
			$options = $property;
		}

		for($i = 0; $i < count($options); $i++)
		{
			$str .= "<option value='".$options[$i]."'";
			if ($setvalue == $options[$i])
			{
				$str .= " selected";
			}
			$str .= ">".$options[$i]."</option>";
		}
		$str .= "</select>";
		return $str;
	}

	function form_javascript_tostring($js_init, $js_validate)
	{
		ob_start();
?>
		<script language='javascript'>

<?php
			for ($i = 0; $i < count($js_init); $i++)
			{
				echo $js_init[$i]."\n";
			}
?>

			// try and validate the form values and submit the form
			function form1_validate() {
				<?php
				for ($i = 0; $i < count($js_validate); $i++)
				{
				?>
				<?= $js_validate[$i] ?>
			else
				<?php
				}
				?>
				if (document.getElementsByClassName('multi_select').length) {
					var container = document.form1.getElementsByClassName('multi_select')[0].children;
					var elems_to_remove = [];
					for (var i = 0; i < container.length; i++) {
						if (container[i].style.display == "none") {
							elems_to_remove.push(container[i]);
						}
					}
					//LP-6515
					var printer = document.form1.querySelector('.outer_select > option[selected=true]');
					if (printer != null) {
						var selModelId = "model_" + printer.value;
						var selModel = document.form1.querySelector('#' + selModelId + ' > option[selected=true]');
						if (selModel && selModel != null) {
							modelVal = selModel.value;
							var el = document.createElement("input");
							el.type = "hidden";
							el.name = "model";
							el.value = modelVal;
							document.form1.appendChild(el);
						}
					}
					//LP-6094
					var commandSet = document.form1.querySelector('.inner_select > option[name=printer][selected=true]');

					if (commandSet && commandSet != null) {
						commandSetVal = commandSet.value;
						var el = document.createElement("input");
						el.type = "hidden";
						el.name = "command_set";
						el.value = commandSetVal;
						document.form1.appendChild(el);
					}
					for (var i = 0; i < elems_to_remove.length; i++) {
						elems_to_remove[i].parentNode.removeChild(elems_to_remove[i]);
					}
				}

				var drawerSettings = document.getElementById('value17');
				if (drawerSettings && Number(drawerSettings.value) === 1) {
					var printer = document.form1.querySelector('.outer_select > option[selected=true]');
					var commandSet = document.form1.querySelector('.inner_select > option[name=printer][selected=true]');
					var dualAlert = "Dual Cash Register requires printer type and commandset! Please select printer type and commandset type";
					var dualAlert_commandset = "Dual Cash Register requires commandset! Please select commandset type";
					if (commandSet == null && printer == null) {
						alert(dualAlert);
						return false;
					}
					else if (printer == null) {
						alert(dualAlert);
						return false;
					}
					else if (commandSet == null) {
						alert(dualAlert_commandset);
						return false;
					}
					else {
						var commandSetVal = commandSet.value;
						if (commandSetVal == "") {
							alert(dualAlert_commandset);
							return false;
						}
					}
					var url = 'resources/check_dual_cash_drawers.php?command_set_id=' + commandSetVal;
					var xhr = window.XMLHttpRequest ? new XMLHttpRequest() : new ActiveXObject('Microsoft.XMLHTTP');
					xhr.open('GET', url);
					xhr.onreadystatechange = function () {
						if (xhr.readyState > 3 && xhr.status == 200) {
							if (xhr.responseText == 'Failure') {
								alert('This CommandSet does not support dual cash register');
								return false;
							}
						}
					};
					xhr.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
					xhr.send();
				} else if (document.forms["form1"]["title"] && document.forms["form1"]["type"] ) {
					var chkFlg = 0;
					var txttipoutruleVal = '';
					var txttipoutruleValId = '';
					var RE = /^\d{0,3}(\.\d{1,2})?$/;
					$('input[id^="percentage_tipout_rules_item"]').each(function () {
					txttipoutruleVal = $("#"+this.id).val();
					txttipoutruleValId = $(this).attr('id');
					if(txttipoutruleVal != '') {
					if(!RE.test(txttipoutruleVal)){
				        chkFlg++;
				        document.getElementById(txttipoutruleValId).focus();
				        }
				        }
				        });
					if(chkFlg > 0) {
				        alert('Enter valid numeric value in tipout rules ranging from : 00.00 to 100.00');
				        return false;
					}
					var chkFlgTipRefund = 0;
					var txttipRefundRulesval = document.forms["form1"]["tiprefund_rules"].value;
					console.log("Tip Refund Rules :"+txttipRefundRulesval);
					if(txttipRefundRulesval != '') {
					if(!RE.test(txttipRefundRulesval)){
					alert('Enter valid numeric value in tip refund rules ranging from : 00.00 to 100.00');
					document.getElementById("tiprefund_rules").focus();
					return false;	
				        }
				        }
					var titleEmp = document.forms["form1"]["title"].value;
					var typeEmp = document.forms["form1"]["type"].value;
					if ( document.forms["form1"]["tip_sharing_period"] ) {
						var time_for_tip_sharing = document.forms["form1"]["tip_sharing_period"].value;
					}
					if ( document.forms["form1"]["shift_time"] ) {
					var shifttime = document.forms["form1"]["shift_time"].value;
					var isValidtime =/^([0-1]?[0-9]|2[0-4]):([0-5][0-9]) [APap][mM]?$/.test(shifttime);
					}
					if (titleEmp.trim() == "") {
						alert("Please enter title");
						return false;
					}
					if (typeEmp == "") {
						alert("Please select type");
						return false;
					}
					if ( document.forms["form1"]["shift_time"] ) {
						if (shifttime == "" && time_for_tip_sharing == "ByShift" ) {
							alert("Please enter what day does your work week start on");
							return false;
							}
						else if (time_for_tip_sharing == "ByShift") {
							if (!isValidtime) {
								alert("Please enter valid time, time format(04:00 AM/PM)");
								return false;
								}
							}
						}
					}

				//START LP-6217
				var enablefiscaltplchkbox = document.form1.querySelector("input[name^='config:enable_fiscal_receipt_templates']");
				if (enablefiscaltplchkbox != null) {
					if (enablefiscaltplchkbox.value == "1") {
						var fiscalTemplate = document.form1.querySelector("select[name^='config:fiscal_receipt_template']");
						if (fiscalTemplate != null) {
							if (fiscalTemplate.value == 0) {
								alert("Please select Fiscal Receipt Template.");
								return false;
							}
							else {
								var dataname = document.form1.querySelector("input[name^='posted_dataname']").value;
								requiredFieldStatus = checkFiscalTemplateFieldStatus(fiscalTemplate.value, dataname);
								if (requiredFieldStatus == 1) {
									alert("Please specify value for required fields in Fiscal Receipt Template");
									return false;
								}
							}
						}
					}
                    var enablefiscalchkbox = document.form1.querySelector("input[name^='config:enable_fiscal_invoice']").value;
                    if (enablefiscalchkbox == "1") {
                            var invoiceStart = parseInt(document.form1.querySelector("input[name^='config:invoice_no_series_start'][type='number']").value);
                            var invoiceEnd = parseInt(document.form1.querySelector("input[name^='config:invoice_no_series_end'][type='number']").value);
                            var invoicePrefix = $("input[name^='invoice_no_prefix']").val().trim();
                            var invoicePostfix = $("input[name^='invoice_no_postfix']").val().trim();

                            if (!(invoiceStart) || !(invoiceEnd)) {
                                    alert("Please set a valid Invoice Number Series.");
                                    return false;
                            } else if ((invoiceStart < 0 ) || (invoiceEnd < 0 )) {
	                            	alert("Please set Invoice Number Serie greater than zero.");
	                                return false;
                            } else if (String(invoiceStart).length > 10 || String(invoiceEnd).length > 10) {
                                    alert("Invoice Number Series should not be more then 10 digit.");
                                    return false;
                            } else if (invoiceStart >= invoiceEnd) {
                                    alert("The starting value for an Invoice Number Series cannot be greater than its ending value.");
                                    return false;
                            } else if (invoicePrefix.length > 10 || invoicePostfix.length > 10) {
                                    alert("Invoice prefix and suffix allow upto 10 character.");
                                    return false;
                            }
                    }
            }
			var enableSecondaryCurr = document.form1.querySelector("input[name^='config:enable_secondary_currency']");
			if ( enableSecondaryCurr && (enableSecondaryCurr.value == 1) ) {
				var secondaryCurr = document.form1.querySelector("select[name='config:secondary_currency_code']");
				if (secondaryCurr && (secondaryCurr.value == '' || secondaryCurr.value == null) ) {
					alert("Please select Secondary currency code");
                    return false;
				}
			}

			//LP-8096 LP-8520
            var codeObj = document.getElementById("code");
            var calcObj = document.form1.calc;

            if(codeObj && calcObj) {
                var code = codeObj.value;
                var calc = calcObj.value;
                
                if (!(parseFloat(calc) == calc)) {
                    alert('Discount must be numeric');
                    return false;
                }
                else {
                    if (code == "p") {
                        if (!(calc >= 0 && calc <= 1)) {
                            alert('Percentage discount should be between 0.00 and 1.00');
                            return false;
                        }
                    }
                    else if (code != "d") {
                        alert('Invalid calculation type');
                        return false;
                    }
                }
            }
			//Company setting validation Added by Ravi Tiwari on 3rd April 2019
			var request_mode = document.getElementById("request_mode").value;
			if(request_mode == 'settings_company_settings'){
				
				var beforeConfigBusinessCountry = $("input[name^='value_before_config:business_country']").val();
				var old_business_country = $("input[name^='old_config:business_country']").val();

				if (beforeConfigBusinessCountry != '' || old_business_country !='') {

					var business_name = $.trim($("input[name^='company_name']").val());
					var business_city = $.trim($("input[name^='config:business_city']").val());
					var business_state = $.trim($("input[name^='config:business_state']").val());
					var business_zip = $.trim($("input[name^='config:business_zip']").val());
					var business_address = $.trim($("input[name^='config:business_address']").val());
					var business_country = $.trim($("input[name^='config:business_country']").val());
					
					var businessNameError = (business_name == "") ? 1 : 0 ;
					var addressError = (business_address == "") ? 1 : 0 ;
					var cityError = (business_city == "") ? 1 : 0 ;
					var stateError = (business_state == "") ? 1 : 0 ;
					var zipError = (business_zip == "") ? 1 : 0 ;
					var countryError = (business_country == "") ? 1 : 0 ;
					var oldCountryError = (old_business_country == "undefined") ? 1 : 0 ;

					var errorMsg = "";
					if(businessNameError){
						errorMsg+="Company Name \n "; 
					}
					if(addressError){
						errorMsg+="Business Address \n "; 
					}
					if(cityError){
						errorMsg+="Business City \n "; 
					}
					if(stateError){
						errorMsg+="Business State \n "; 
					}
					if(zipError){
						errorMsg+="Business Zip \n "; 
					}
					if(countryError && oldCountryError){
						errorMsg+="Business Country \n "; 
					}
					if(errorMsg){
						alert("Please Enter These Required Field(s) \n " + errorMsg);
						return false;
					}
				}
			}
            document.form1.submit();
			//END
		}

		function checkFiscalTemplateFieldStatus(tplId, dataname)
		{
			var result = 0;
			$.ajax({
				async: false,
				type: "POST",
				url: "areas/save_fiscaltemplate.php",
				data: {"tpl_id" : tplId, "action" : "isrequiredfieldvaluemissing", "dataname" : dataname },
				success: function (response) {
					result = response;
				}
			});
			return result;
		}

		// prevent any keys that aren't 0-9 or '.' from firing events on the element
		// allows for paste keys if the 'v' key is pressed at the same time as the meta or ctrl keys
		function form_enforce_decimal_values(element, event)
		{
			if (!event.which && !event.keyCode)
			{
				return true;
			}

			if (typeof(event.metaKey)=="undefined" || typeof(event.ctrlKey)=="undefined")
			{
				return true;
			}

			var keyCode = (event.keyCode)?event.keyCode:event.which;
			var metaKey = (event.metaKey)?event.metaKey:false;
			var ctrlKey = (event.ctrlKey)?event.ctrlKey:false;
			var pasteKey = (metaKey || ctrlKey);

			if (keyCode==8 || keyCode==46 || (keyCode>=48 && keyCode<=57) || (keyCode>=96 && keyCode<=105))
			{
				return true;
			}
			else if ((keyCode==190 || keyCode==110) && element.value.indexOf('.')==-1)
			{
				return true;
			}
			else if (keyCode==86 && pasteKey)
			{
				return true;
			}
			else
			{
				if (event.stopPropagation)
				{
					event.stopPropagation();
				}

				if (event.cancelBubble)
				{
					event.cancelBubble();
				}

				if (event.preventDefault)
				{
					event.preventDefault();
				}

				if (event.bubbles)
				{
					event.bubbles = false;
				}

				return false;
			}
		}

		// only allows the given regex in an element
		function match_input_by_regex(element, regex)
		{
			var val = element.value;
			var matches = val.match(regex);
			if (matches === null)
			{
				return;
			}

			var newval = '';
			for (var i = 0; i < matches.length; i++)
			{
				newval += matches[i];
			}

			if (newval != val)
			{
				element.value = newval;
			}
		}
	</script>

<?php

		$s_script = ob_get_contents();
		ob_end_clean();

		$s_script = str_replace(array("\n", "\r", "\n\r"), "\n", $s_script);

		return $s_script;
	}

	function upload_company_logo(&$admin_info, &$a_files, &$valid_ext, &$update_code)
	{
		$base_path = __DIR__."/../../images/".admin_info("dataname")."/";
		if (!is_dir($base_path))
		{
			mkdir($base_path,0775,true);
		}

		if (!in_array(end(explode(".", strtolower($a_files['logo_img']['name']))), $valid_ext))
		{
			echo "Please upload a valid image.";
			exit();
		}

		$basename = basename($a_files['logo_img']['name']);
		$target = $base_path . $basename; // append file to path
		$filename = split("[/\\.]",$basename); // TODO maybe change this to path_info()
		$filename[0] = str_replace(" ", "_", $filename[0]); // replace whitespaces in filename with underscore
		$myImage = new Imagick($a_files['logo_img']['tmp_name']);
		if (empty($myImage))
		{
			echo " Malformed image. Please upload a valid image.";
			exit();
		}

		$myImage->thumbnailImage(300, 300, true);
		$AWSkey = rs_move_uploaded_file($myImage, $a_files['logo_img']['name'], $a_files['logo_img']['name'], true);
		//Update new storage_key_logo
		if($AWSkey)
		{
			lavu_query("UPDATE " . admin_info("database") . ".locations SET storage_key_logo='[1]' WHERE id='[2]'",$AWSkey, sessvar("locationid"));
		}

		if (is_file($base_path.$filename[0]."_thumb.".$filename[1]))
		{
			unlink($base_path.$filename[0]."_thumb.".$filename[1]);
		}
		$myImage->writeImage($base_path.$filename[0]."_thumb.".$filename[1]);
		
		$update_code = str_replace(stristr($update_code, "`logo_img"), "`logo_img`='".$filename[0]."_thumb.".$filename[1]."'", $update_code);
	}

	function upload_form_image(&$admin_info, &$a_files, &$valid_ext, &$update_code)
	{
		$base_path = __DIR__."/../../images/".admin_info("dataname")."/";
		if (!is_dir($base_path))
		{
			mkdir($base_path, 0775, true);
		}

		if (!in_array(end(explode(".", strtolower($a_files['image']['name']))), $valid_ext))
		{
			echo "Please upload a valid image.";
			exit();
		}

		$basename = basename($a_files['image']['name']);
		$target = $base_path . $basename; // append file to path
		$filename = split("[/\\.]",$basename); // TODO maybe change this to path_info()
		$filename[0] = str_replace(" ", "_", $filename[0]); // replace whitespaces in filename with underscore
		$myImage = new Imagick($a_files['image']['tmp_name']);

		if (empty($myImage))
		{
			echo "Malformed image. Please upload a valid image.";
			exit();
		}
		$myImage->thumbnailImage(300, 300, true);
		if (is_file($base_path.$filename[0]."_thumb.".$filename[1]))
		{
			unlink($base_path.$filename[0]."_thumb.".$filename[1]);
		}
		$myImage->writeImage($base_path.$filename[0]."_thumb.".$filename[1]);

		$update_code = str_replace(stristr($update_code, "`image"), "`image`='".$filename[0]."_thumb.".$filename[1]."'", $update_code);
	}

	function upload_receipt_logo(&$admin_info, &$a_files, &$valid_ext, &$update_code)
	{
		$base_path = __DIR__."/../../images/".admin_info("dataname")."/";
		if (!is_dir($base_path))
		{
			mkdir($base_path, 0775, true);
		}

		if (!in_array(end(explode(".", strtolower($a_files['receipt_logo_img']['name']))), $valid_ext))
		{
			echo "Please upload a valid image.";
			exit();
		}

		$basename = basename($a_files['receipt_logo_img']['name']);
		$target = $base_path . $basename; // append file to path
		$filename = split("[/\\.]", $basename);
		$filename[0] = str_replace(" ", "_", $filename[0]); // replace whitespaces in filename with underscore

		$myImage = new Imagick($a_files['receipt_logo_img']['tmp_name']);
		if (empty($myImage))
		{
			echo "Malformed image. Please upload a valid image!";
			exit();
		}

		$myImage->thumbnailImage(300, 300, true);
		$max = $myImage->getQuantumRange();
		$max = $max["quantumRangeLong"];
		$myImage->thresholdImage(0.7 * $max);
		$myImage->setImageColorSpace(Imagick::COLORSPACE_GRAY);
		if (is_file($base_path.$filename[0]."_bw.".$filename[1]))
		{
			unlink($base_path.$filename[0]."_bw.".$filename[1]);
		}
		$myImage->writeImage($base_path . $filename[0].'_bw.'.$filename[1]);
		$AWSkey = rs_move_uploaded_file($myImage, $a_files['receipt_logo_img']['name'], $a_files['receipt_logo_img']['name'], true);
		//Update new storage_key_receipt
		if($AWSkey)
		{
			lavu_query("UPDATE " . admin_info("database") . ".locations SET storage_key_receipt='[1]' WHERE id='[2]'",$AWSkey, sessvar("locationid"));
		}
		$update_code = str_replace(stristr($update_code, "`receipt_logo_img"), "`receipt_logo_img`='".$filename[0].'_bw.'.$filename[1]."'", $update_code);
	}
	
	function upload_kiosk_image(&$admin_info, &$a_files, &$valid_ext, &$update_code)
	{
		$base_path = __DIR__."/../../images/".admin_info("dataname")."/";
		$full_path = __DIR__."/../../images/".admin_info("dataname")."/full/";
		if (!is_dir($base_path))
		{
			mkdir($base_path, 0775, true);
		}
		
		if (!is_dir($full_path))
		{
			mkdir($full_path, 0775, true);
		}
	
		if (!in_array(end(explode(".", strtolower($a_files['kiosk_config:kiosk_splash_filename']['name']))), $valid_ext))
		{
			echo "Please upload a valid image.";
			exit();
		}
	
		$basename = basename($a_files['kiosk_config:kiosk_splash_filename']['name']);
		$target = $base_path . $basename; // append file to path
		$filename = split("[/\\.]", $basename);
		$filename[0] = str_replace(" ", "_", $filename[0]); // replace whitespaces in filename with underscore
	
		$myImage = new Imagick($a_files['kiosk_config:kiosk_splash_filename']['tmp_name']);
		if (empty($myImage))
		{
			echo "Malformed image. Please upload a valid image!";
			exit();
		}
	
		$AWSkey = rs_move_uploaded_file($myImage, $a_files['kiosk_config:kiosk_splash_filename']['name'], $a_files['kiosk_config:kiosk_splash_filename']['name'], true);
		//Update new storage_key_kiosk
		if($AWSkey)
		{
			error_log("storage_key_kiosk = " . $AWSkey . " ");
			lavu_query("UPDATE " . admin_info("database") . ".locations SET storage_key_kiosk='[1]' WHERE id='[2]'",$AWSkey, sessvar("locationid"));
		}
		$myImage->thumbnailImage(300, 300, true);
		if (is_file($base_path.$filename[0]."_kiosk.".$filename[1]))
		{
			unlink($base_path.$filename[0]."_kiosk.".$filename[1]);
		}
		$myImage->writeImage($base_path . $filename[0].'_kiosk.'.$filename[1]);

		////Upload Full Image
		
		$fullImage = new Imagick($a_files['kiosk_config:kiosk_splash_filename']['tmp_name']);
		
		if (is_file($full_path.$filename[0]."_kiosk.".$filename[1]))
		{
			unlink($full_path.$filename[0]."_kiosk.".$filename[1]);
		}
		$fullImage->writeImage($full_path . $filename[0].'_kiosk.'.$filename[1]);
		$update_code =$filename[0].'_kiosk.'.$filename[1];
	}

	// end of function delcarations
	global $use_db;
	if ($rowid)
	{
		if (isset($use_db) && $use_db == "`poslavu_MAIN_db`.")
		{
			$row_query = mlavu_query("SELECT * FROM `poslavu_MAIN_db`.`[1]` WHERE `id` = '[2]'", $tablename ,$rowid);
		}
		else
		{
			$row_query = lavu_query("SELECT * FROM `".$tablename."` where `id` = '[1]'", $rowid);
		}

		if ($row_query->num_rows)
		{
			$row_read = mysqli_fetch_assoc($row_query);
		}
	}

	$width_restraint = "";
	if (isset($form_fixed_width))
	{
		$width_restraint = " min-width:".$form_fixed_width."px; max-width:".$form_fixed_width."px; width:".$form_fixed_width."px;";
	}
	else if (isset($form_max_width))
	{
		$width_restraint = " max-width:".$form_max_width."px;";
	}

	$formcode 			= "";
	$insert_keys 		= "";
	$insert_vals 		= "";
	$update_code 		= "";
	$is_epson_kds_checked = 0;
	$js_validate 		= array();
	$js_init 			= array();
	$special_query_list = array();
	$form_posted 		= false;
	$field_update_list  = array();
	$field_update_titles= array();

	$form_posted = (isset($_POST['posted']));

	$ltg_enabled_before_update = $location_info['use_lavu_togo'];
	$ltg_enabled_after_update = $ltg_enabled_before_update;

	$track_values = array();
	for ($i = 0; $i < count($fields); $i++)
	{
		$field = new FormField($fields[$i]);
		//Force set cash tips record mode to daily total if server summary for cash tips is used.
		if ($field->name == "config:cash_tips_as_daily_total"){
			if ($location_info["server_summary_enter_tips"] == '1' && $location_info['cash_tips_as_daily_total'] != '1'){
				if (admin_info('dataname')) {
					mlavu_query("UPDATE `poslavu_".admin_info('dataname')."_db`.`config` set `value` = '1' where `setting` = 'cash_tips_as_daily_total'");
				}
				$location_info['cash_tips_as_daily_total'] = 1;
			}
		}
		//End of adding stuff...

		if (isset($_POST[$field->name]))
		{
		    if ($field->name == "config:primary_currency_code" || $field->name == "config:secondary_currency_code") {
			    if (admin_info('dataname')) {
			        $primary_currency_code= $_POST[$field->name];
			        $res = mysqli_fetch_assoc(mlavu_query("select * from `poslavu_MAIN_db`.`country_region` where  `id` = '$primary_currency_code'"));
			        $setting_names=ltrim($field->name,"config:");
			        if ((!isset($_POST['config:primary_currency_code']))|| ($_POST['config:primary_currency_code'] != $_POST['config:secondary_currency_code']) || $_POST['config:enable_secondary_currency']==0) {
			        	mlavu_query("UPDATE `poslavu_".admin_info('dataname')."_db`.`config` set `value2` = '$primary_currency_code' where `setting` = '$setting_names'");
			        }
			        $field->value = $res[alphabetic_code];
			    }
			}
			else if ($field->name == "value10" && isset($_POST['epson_kds']) && $_POST['epson_kds'] == 1) {
			    $field->value = 0;
			}
			else {
			    $field->value = $_POST[$field->name];
			}
		}
		else if (isset($row_read) && isset($row_read[$field->name]))
		{
			$field->value = $row_read[$field->name];
		}
		else if (substr($field->name, 0, 7) == "config:" && isset($location_info[substr($field->name, 7)]))
		{
			$field->value = $location_info[substr($field->name, 7)];
			//LP-7283.
			if ($field->name == 'config:business_country') {
				$resData = mysqli_fetch_assoc(mlavu_query("select code from `countries` where  `name` = '$field->value'"));
				$countryCode = $resData['code'];
				$setting_name = ltrim($field->name,"config:");
				lavu_query("UPDATE `config` set `value2` = '$countryCode' where `setting` = '$setting_name'");
			}
		}
		else if (substr($field->name, 0, 17) == "inventory_config:" && isset($location_info[substr($field->name, 17)]))
		{
			$field->value = $location_info[substr($field->name, 17)];
		}
		else if (substr($field->name, 0, 13) == "kiosk_config:" && isset($location_info[substr($field->name, 13)]))
		{
			$field->value = $location_info[substr($field->name, 13)];
		}
		else if (isset($field->defaultvalue) && $field->defaultvalue)
		{
			$field->value = $field->defaultvalue;
		}
		else if($field->name == "enable_cp3_beta") {
		    global $modules;
		    if ($modules->hasModule('extensions.limited_cp_beta')) {
		        
		        $field->value = 1;
		    }
		    else {
		        $field->value = 0;
		    }
		}
		else
		{
			$field->value = "";
		}

		$field->description = (isset($descriptions[$field->name]))?$descriptions[$field->name]:"";

		if (isset($field->setvalue) && $field->setvalue)
		{
			$field->value = $field->setvalue;
		}

		$field_in_quote_value = str_replace("\"", "&quot;", $field->value);
		$field->db_include 	  = true;
		$show_coltitle 		  = true;
		$rowcode = "";
		$colcode = "";

		if (isset($field->props_core['attr']) && $field->props_core['attr'] == "disabled")
		{
			$colcode .= "<input type='hidden' name='value_before_".$field->name."_disabled_field' value=\"".$field_in_quote_value."\">";
		}
		else
		{
			$colcode .= "<input type='hidden' name='value_before_".$field->name."' value=\"".$field_in_quote_value."\">";
		}
		$special_query = "";
		if ($field->type == "text")
		{
		    	if ( $is_epson_kds_checked == 1 && ($field->name == "value7" || $field->name == "value8") ) {
		        	$field->props['readonly'] = 'readonly';
		    	}
		    
			if (isset($field->props_core['attr']) && $field->props_core['attr'] != 'disabled'){
				$colcode .= "<input type='text' name='$field->name'";
			}
			else {
				$colcode .= "<input type='text'";
				if (!empty($field->props) || !empty($field->props_core)){
					$colcode .= " name=\"$field->name\"";
				}
			}

			foreach ($field->props as $prop_key => $prop_val)
			{
				$colcode .= " ".$prop_key."=\"".str_replace("\"", "\\\"", $prop_val)."\"";
			}
			if (isset($field->props_core['db']) && $field->props_core['db'] && $field->props_core['table'])
			{
				$res = mysqli_fetch_assoc(mlavu_query("SELECT * FROM `poslavu_[1]_db`.`[2]` where ".$field->props_core['where_clause'], $field->props_core['db'], $field->props_core['table']));
				if ($field->props_core['attr'] == "disabled")
				{
					$field->db_include=false;
					$colcode .=" name =".$field->name."_disabled_field disabled ";
				}
				$colcode .= "value= \"".$res[$field->name]."\">";
			}
			else
			{
				if($field->name === 'config:ltg_delivery_fee' && $field_in_quote_value == '')
				{
					$config_setting = mysqli_fetch_assoc(lavu_query("select * from `config` where setting='ltg_delivery_fee'"));
					$colcode .= sizeof($config_setting) > 0 ? " value=\"0.00\">" : '';
				}
				if($field->name === 'config:ltg_max_distance_miles' && $field_in_quote_value == '')
				{
					$colcode .= " value=\"10\">";
				}
				else
				{
					$colcode .= " value=\"$field_in_quote_value\">";
				}
			}

			if ($field->name=="email" && $tablename=="lk_customers" && $rowid)
			{
				if (preg_match("/^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*$/i", $field->value))
				{
					update_emailing_list("subscriberadd", "racer", $data_name, $location_info, $rowid);
				}
			}

		}
		else if ($field->type == "text_editCountry")//This condition to prepare business country text box with edit button.
		{
			if (isset($field->props_core['attr']) && $field->props_core['attr'] != 'disabled') {
				$colcode .= "<input type='text'  name='old_$field->name'";
			} else {
				$colcode .= "<input type='text'";
				if (!empty($field->props) || !empty($field->props_core)) {
					$colcode .= " name=\"old_$field->name\"";
				}
			}

			foreach ($field->props as $prop_key => $prop_val) {
				$colcode .= " ".$prop_key."=\"".str_replace("\"", "\\\"", $prop_val)."\"";
			}

			$colcode .= " value=\"$field_in_quote_value\">";

			if ($field->props['edit_businesscountry'] && isset($field->props_core)) {
				$colcode .= "<input type='button' id='edit_business_country' style='height:1.5em;padding-top:3px;' value = 'Edit' onclick = 'editBusinessCountry()'> <br>";
				$colcode .= "<select id= '".$field->name."' name='hidden_".$field->name."'  style='display:none' onchange='selectNewCountry()'>";
				$colcode .= "<option view='' value=''>Select Country</option>";
				foreach ($field->props_core as $prop_key => $prop_val)
				{
					$view = "[".$prop_key."] ".$prop_val;
					$selected = $field_in_quote_value;
					$colcode .= "<option view=\"".str_replace("\"", "&quot;", $view)."\" value=\"".str_replace("\"", "&quot;", $prop_val)."\"";
					if ($prop_val == $selected) {
						$colcode .= " selected";
					}
					$colcode .= ">".$view."</option>";
				}
				$colcode .= "</select>";
			}
		}
		else if ($field->type == "button")
		{
			if ($field->props_core['onclick'])
			{
				$colcode .= "<input type='button' value = '".$field->title."' onclick = ".$field->props_core['onclick']."> ";
			}
			else
			{
				$colcode .= "<input type='button' value = '".$field->title."'/> ";
			}
		}
		else if ($field->type == "number")
		{
			$colcode .= "<input type='number' name='".$field->name."'";
			foreach ($field->props as $prop_key => $prop_val)
			{
				$colcode .= " ".$prop_key."=\"".str_replace("\"","\\\"",$prop_val)."\"";
			}
			$colcode .= " value=\"".$field_in_quote_value."\">";
		}
		else if ($field->type=="decimal" || (strpos($field->type, "decimal:")===0))
		{
			$s_default = substr($field->type, strlen("decimal:"));
			$field_in_quote_value = ($field_in_quote_value == "")?$s_default:$field_in_quote_value;

			$colcode .= "<input type='text' name='".$field->name."'";
			foreach($field->props as $prop_key => $prop_val)
			{
				$colcode .= " ".$prop_key."=\"".str_replace("\"","\\\"",$prop_val)."\"";
			}
			$colcode .= " value=\"".$field_in_quote_value."\" onkeydown=\"form_enforce_decimal_values(this, event);\" onkeypress=\"form_enforce_decimal_values(this, event);\" onkeyup=\"form_enforce_decimal_values(this, event);\" oninput=\"match_input_by_regex(this,/[0-9*]\\.[0-9]*/g);\">";
		}
		else if ($field->type == "readonly")
		{
			$colcode .= "&nbsp;&nbsp;".$field->value;
			$field->db_include = false;
		}
		else if ($field->type == "textarea")
		{
			$rows = "25";
			$cols = "60";
			if(isset($field->props) && isset($field->props['custom']) && $field->props['custom'] === 'true')
			{
				$rows = isset($field->props['rows']) && (int) $field->props['rows'] > 0 ? $field->props['rows'] : $rows;
				$cols = isset($field->props['cols']) && (int) $field->props['cols'] > 0 ? $field->props['cols'] : $cols;
			}
			$colcode .= "<textarea name='".$field->name."' cols='".$cols."' rows='".$rows."'>".$field->value."</textarea>";
		}
		else if ($field->type == "multi_text")
		{
			$dname = $field->name;
			$width = "15";
			$height = "5";
			$type = "textarea";
			if (isset($field->props))
			{
				$cols= (isset($field->props['width']))?$field->props['width']:"15";
				$rows= (isset($field->props['height']))?$field->props['height']:"5";
				$type= (isset($field->props['type']))?$field->props['type']:"textarea";
			}
			$colcode .= "
				<ul id='multi_text_list_".$dname."'>";
				$setvals = explode("[_sep_]", $field->value);
				for($n = 0; $n < count($setvals); $n++)
				{
					$setval= $setvals[$n];
					$setname = $field->name;
					if ($n > 0)
					{
						$setname = $n.$setname;
					}

					$colcode .= "<li>
						<div class='multi_input_container' >
							<span class='move_arrow'> </span>
							<span class='multi_text_input'>";
								if ($type == "textarea")
								{
									$colcode.="<textarea name='".$setname."' cols='".$width."' rows='".$height."'>$setval</textarea>";
								}
								else
								{
									$colcode.="<input type='text' name='".$setname."' size='".$cols."' value=\"".str_replace("\"", "&quot;", $setval)."\">";
								}
					$colcode .= "</span>
						</div>
					</li>";
				}
				$colcode .= "</ul>
				<input type='button' value='add more' name='textarea_add_more_".$dname."' onclick='multi_text_add_more_".$dname."(this)'>
				";
			$colcode .= "";

			$get_js_code .= "
				function multi_text_add_more_".$dname."(elem)
				{
					$('#multi_text_list_".$dname."').sortable();
					var mai_ul= elem.previousElementSibling.children;

					var cloned = $(mai_ul[mai_ul.length-1]).clone();
					var name   = cloned[0].children[0].children[1].children[0].getAttribute('name');
					var num    = parseInt(name);
					name       = name.replace(/[0-9]/g,'');
					cloned[0].children[0].children[1].children[0].value = '';

					if (!isNaN(num))
						num+=1;
					else
						num=1;

					cloned[0].children[0].children[1].children[0].setAttribute('name',num+name)
					document.getElementById('multi_text_list_$dname').appendChild(cloned[0]);

				}";
			$init_js_code = "				$('#multi_text_list_".$dname."').sortable();";
			$colcode .= "<script language='javascript'>";
			$colcode .= $get_js_code . "\n";
			$colcode .= $init_js_code . "\n";
			$colcode .= "</script>";

			if ($form_posted)
			{
				if (isset($_POST[$field->name]))
				{
					$newvalue = "";
					$sep = "[_sep_]";
					$keys= array_keys($_POST);
					$new_keys= preg_grep('/'.$field->name.'/', $keys);
					foreach ($new_keys as $key => $value)
					{
						if (strlen($value)==strlen($field->name) || (strlen($value)==strlen($field->name) + 1 && is_numeric(substr($value,0,1))) || (strlen($value)==strlen($field->name) + 2 && is_numeric(substr($value,0,2))))
						{
							echo $key." => ".$value." = ".$_POST[$value]."<br>";
							if ($_POST[$value] != "")
							{
								if ($newvalue != "")
								{
									$newvalue .= $sep;
								}
								$newvalue .= $_POST[$value];
							}
						}
					}
					$_POST[$field->name] = $newvalue;
					$field->value = $newvalue;
				}
			}
		}
		else if ($field->type == "hidden")
		{
			$rowcode = "<input type='hidden' name='".$field->name."' value=\"".$field_in_quote_value."\">";
		}
		else if ($field->type == "browse_button")
		{
			$rowcode = " ";
			$field->db_include = false;
			$show_coltitle = false;
		}
		else if ($field->type == "redirect_button" || $field->type == "open_modal_button")
		{
			$rowcode = "";
			$field->db_include = false;
			$show_coltitle = true;
			if ($field->title == "")
			{
				$field->title = "&nbsp;";
			}
			$buttonprefix = "";
			$buttontitle = "Redirect";
			$buttonlink = "";
			$inputvalues = $field->props;
			if (is_array($inputvalues))
			{
				if (isset($inputvalues['value']))
				{
					$buttontitle = $inputvalues['value'];
				}

				if (isset($inputvalues['link']))
				{
					$buttonlink = $inputvalues['link'];
				}

				if (isset($inputvalues['prefix']))
				{
					$buttonprefix = $inputvalues['prefix'];
				}
			}
			else if ($field->value != "")
			{
				$buttonlink = $field->value;
			}
			else
			{
				foreach ($inputvalues as $key => $value)
				{
					$buttonlink = $value;
					break;
				}
			}
			$closeEvent = "";
			$tpl_name = "";
			if ($field->type == "redirect_button") {
				$colcode = $buttonprefix . "<input type='button' value='" . $buttontitle . "' onclick='window.open(\"" . $buttonlink . "\",\"_blank\")'>";
			}
			else if ($field->type == "open_modal_button")
			{
				$colcode = "";
				if($field->name == "config:fiscal_receipt_template")
				{
					$closeEvent = "fiscalTemplate";
					if ($form_posted)
					{
						$templateId = $_POST[$field->name];
						$field->value = $templateId;
						$_POST[$field->name] = $field->value;
					}
					//Populate fiscal template title
					$selected = ($field->value) ? $field->value : 0;
					$colcode .= "<select id= '".$field->name."' name='".$field->name."' onchange='set_fiscaltemp_select(this,\"advanced_settings_modal_" . $inputvalues['modal_number'] . "\")'>";
					foreach ($fiscalTemplatesArr as $tplId => $tplTitle) {
						$colcode .= "<option value=\"".$tplId."\"";
						if ($tplId == $field_in_quote_value) {
							$colcode .= " selected";
							$tpl_name = " (".$tplTitle.")";
						}
						$colcode .= ">".$tplTitle."</option>";
					}
					$colcode .= "</select>";
				}
				if ($field->name == "secondary_exchange_rates")
				{
					$closeEvent = "multipleCurrency";
				}
				$colcode .= $buttonprefix;
				$include_session = "?session_id=".session_id();
				$include_data_name = "&dn=".$data_name;
				$iframe_src = $inputvalues['iframe_src']."&modal_number=".$inputvalues['modal_number']."&tpl_id=".$selected;

				$colcode .= "<span id='outer_modal_span_".$inputvalues['modal_number']."'>
								<input id='modal_button_".$inputvalues['modal_number']."' type='button' value='$buttontitle' onclick='addModalButtonFunctionality(this.id, \"block\")'>

								<div id='advanced_settings_modal_".$inputvalues['modal_number']."' class='advanced_settings_modal_".$inputvalues['modal_number']."' role='document'>

									<div class='modal_content'>
										<div class='modal_header'>
											<span id='modal_close_".$inputvalues['modal_number']."' class='modal_close_".$inputvalues['modal_number']."' onclick='addModalButtonFunctionality(this.id, \"none\")' from='".$closeEvent."'>&times;</span>
											<h2><span id='modal_header_text_".$inputvalues['modal_number']."'>".$inputvalues['header_text'].$tpl_name."</span></h2>
										</div>
										<div id='modal_body' class='modal_body'>
											<iframe id='modal_iframe' src=\"".$iframe_src."\"></iframe>
										</div>
										<div class='modal_footer'>
										</div>
									</div>

								</div>
							</span> <!--end outer_modal_span-->

							<script type='text/javascript'>

								function addModalButtonFunctionality(idVal, displayVal){
										var mulCur = document.getElementById('config:enable_multiple_secondary_currency');
										mulCur = mulCur?`&mulCurr=`+mulCur.value:'';
										var getiframMulc = document.querySelector('#'+'advanced_settings_modal_' + idVal.substring(idVal.lastIndexOf('_')+1)+' #modal_body #modal_iframe').getAttribute('src');
										document.querySelector('#'+'advanced_settings_modal_' + idVal.substring(idVal.lastIndexOf('_')+1)+' #modal_body #modal_iframe').setAttribute('src',getiframMulc+mulCur);
                                                var timeStamp = Math.floor(Date.now());
                                        var modalId = 'advanced_settings_modal_' + idVal.substring(idVal.lastIndexOf('_')+1); 
                                        document.getElementById(modalId).style.display = displayVal;
                                        var eventFrom = document.getElementById(idVal).getAttribute('from');
                                        if (eventFrom == \"fiscalTemplate\"){
                                                var getifram = document.querySelector('#'+'advanced_settings_modal_' + idVal.substring(idVal.lastIndexOf('_')+1)+' #modal_body #modal_iframe').getAttribute('src');
                                                var timeStamp = Math.floor(Date.now());
                                                document.querySelector('#'+'advanced_settings_modal_' + idVal.substring(idVal.lastIndexOf('_')+1)+' #modal_body #modal_iframe').setAttribute('src',getifram+'&ts='+timeStamp);
                                        }

                                        if (displayVal==\"none\" && eventFrom != \"fiscalTemplate\"){
										if (eventFrom != \"multipleCurrency\") {
										 window.location.reload();
                                        }
                                        }
                                }

								function getAllModals(){
									var elements = [];
									var divs = document.getElementsByTagName(\"div\");

									if (divs){
										for(var i = 0; i < divs.length; i++){
											if (divs[i].id.startsWith(\"advanced_settings_modal_\")){
												elements.push(divs[i]);
											}
										}
										}
									return elements;
								}

								window.onclick = function(event) {
									var modals = getAllModals();
									for(var i = 0; i < modals.length; i++){
										if (event.target == modals[i]) {
											document.getElementById(\"advanced_settings_modal_\"+i).style.display = \"none\";
											return;
										}
									}
								};

								String.prototype.startsWith = function(prefix) {
									return this.indexOf(prefix) === 0;
								}

							</script>
				";
			}
		}

		else if ($field->type == "file")
		{
			//Condition added by Ravi Tiwari on 22nd Mar 2019
			if($field->value != 'poslavu.png'){
		
				$bpath = "/images/".admin_info('dataname')."/";

				if ($_GET['mode'] == "display_settings"){
					$fpath = empty($field->value)?"":$bpath."ad_images/".$field->value;
					$colcode .= "<img src='".$fpath."' style='max-width:200px; max-height:200px'/><br /><br />";
					$colcode .= "<input type='file' name='".$field->name."' id='".$field->name."' />";
				}else{
					$fpath = empty($field->value)?"":$bpath.$field->value;
					$colcode .= "<img src='".$fpath."' style='max-width:200px; max-height:200px'/><br /><br />";
					$colcode .= "<input type='file' name='".$field->name."' id='".$field->name."' />";
				}

			}else{
				$fpath = getenv('S3IMAGEURL').'images/'.$field->value;
				$colcode .= "<img src='".$fpath."' style='max-width:200px; max-height:200px'/><br /><br />";
				$colcode .= "<input type='file' name='".$field->name."' id='".$field->name."' />";
			}	
		}
		else if ($field->type == "date_created")
		{
			if (!$rowid)
			{
				$field_in_quote_value = date("Y-m-d H:i:s");
			}
			$rowcode = "<input type='hidden' name='$field->name' value=\"$field_in_quote_value\">";
		}
		else if ($field->type == "last_activity")
		{
			$field_in_quote_value = date("Y-m-d H:i:s");
			$rowcode = "<input type='hidden' name='$field->name' value=\"$field_in_quote_value\">";
		}
		else if ($field->type == "confirmed_by_attendant")
		{
			if ($field->value > 0 && $field->value!="")
			{
				$act_query = lavu_query("select * from `users` where id='[1]'",$field->value);
				if ($act_query->num_rows)
				{
					$act_read = mysqli_fetch_assoc($act_query);
					$colcode .= trim($act_read['f_name']." ".$act_read['l_name']);
				}
				else
					$colcode .= "Account not Found";
			}
			else
			{
				$colcode .= "<font color='#777777'>Not Confirmed</font>";
			}
		}
		else if ($field->type=="calendar")
		{
			require_once(resource_path()."/form_calendar.php");

			$setyear = false;
			$setmonth = false;
			$setday = false;
			if ($field->value != "")
			{
				$date_parts = explode("-",$field->value);
				if (count($date_parts) > 2)
				{
					$setyear = $date_parts[0];
					$setmonth = $date_parts[1];
					$setday = $date_parts[2];
				}
			}
			$colcode .= create_form_calendar($field->name,$setday,$setmonth,$setyear);
		}
		else if ($field->type == "hours_of_operation")
		{
			$true_tz = getenv('TZ');
			putenv("TZ=UTC");
			$times = array();
			for ($t = 0; $t < 86400; $t+=1800)
			{
				$times[date("g:i a", $t)] = date("Hi", $t);
			}
			$times[speak("Closed")] = "c";
			putenv("TZ=$true_tz");

			$val_array = explode(";", $field->value);
			foreach ($val_array as $index => $val)
			{
				$val_array[$index] = explode("-", $val);
			}

			$days_of_week = array( "Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday" );
			$colcode .= "<input type='hidden' id='".$field->name."' name='".$field->name."' value='".$field->value."'>
			<script language='javascript'>

				function updateHOOinput(db_field)
				{
					var val_array = [];
					for (var i = 0; i < 7; i++)
					{
						var start_time = document.getElementById(db_field + \"_\" + i + \"_start\").value;
						var end_time = document.getElementById(db_field + \"_\" + i + \"_end\").value;
						val_array.push(start_time + \"-\" + end_time);
					}

					document.getElementById(db_field).value = val_array.join(\";\");
				}

			</script>";
			$colcode .= "<table>";
			foreach ($days_of_week as $index => $day)
			{
				$colcode .= "<tr><td align='right'>".speak($day).":</td><td align='left'>";
				$colcode .= "<select id='".$field->name."_".$index."_start'  onchange='updateHOOinput(\"".$field->name."\");'>";
				foreach ($times as $key => $val)
				{
					$selected = (isset($val_array[$index][0]) && $val == $val_array[$index][0])?" SELECTED":"";
					$colcode .= "<option value='".$val."'".$selected.">".$key."</option>";
				}
				$colcode .= "</select> to ";
				$colcode .= "<select id='".$field->name."_".$index."_end' onchange='updateHOOinput(\"".$field->name."\");'>";

				foreach ($times as $key => $val)
				{
					$selected = (isset($val_array[$index][1]) && $val == $val_array[$index][1])?" SELECTED":"";
					$colcode .= "<option value='".$val."'".$selected.">".$key."</option>";
				}
				$colcode .= "</select>";
				$colcode .= "</td></tr>";
			}
			$colcode .= "</table>";
		}
		else if ($field->type == "day_of_week")
		{
			$days_of_week = array( "Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday" );

			foreach ($days_of_week as $index => $day)
			{
				$value = 1<<$index;
				$checked = (($value & $field->value)?"checked":"");

				$colcode .= "<div style='display: table;'>
					<input type='checkbox' id='".$field->name."_".$index."' value='".$value."' ".$checked." />
					".$day."
				</div>";
			}

			$colcode .= "<input type='hidden' name='".$field->name."' value='".$field->value."' />";
			$colcode .= "<script type='text/javascript'>
					(function()
					{
						function handlEvent(event)
						{
							var input = null;
							if ((event.target.tagName == 'INPUT' && event.target.type=='checkbox'))
							{
								//No Need to Trigger the check
								input = event.target;
							}
							else
							{
								if (event.target.tagName == 'DIV')
								{
									input = event.target.children[0];
									input.checked = !input.checked;
								}
								else
								{
									input = event.target.parentNode.children[0];
								}
							}

							if (input.checked)
							{
								input.setAttribute('checked', '');
							}
							else
							{
								if (input.hasAttribute('checked'))
								{
									input.removeAttribute('checked');
								}
							}

							var elements = document.querySelectorAll('input[id^=\"".$field->name."_\"][checked]');
							var value = 0;
							for (var i = 0; i < elements.length; i++)
							{
								value += Number(elements[i].value);
							}

							var hidden_input = document.querySelector('input[name=\"".$field->name."\"]');
							if (hidden_input)
							{
								hidden_input.value = value;
							}
						}

						var elements = document.querySelectorAll('input[id^=\"".$field->name."_\"]');
						for (var i = 0; i < elements.length; i++)
						{
							elements[i].parentNode.handleEvent = handlEvent.bind(elements[i].parentNode);
							elements[i].parentNode.addEventListener('click', elements[i].parentNode);
						}
					})();
				</script>";
		}
		else if ($field->type=="time" || $field->type=="time_no_late_night")
		{
			if ($form_posted)
			{
				$sethour = $_POST[$field->name."_hour"];
				$setminute = $_POST[$field->name."_minute"];
				$setam = $_POST[$field->name."_am"];
				if (strtolower($setam)=="am" || $setam=="")
				{
					if ($sethour=="12")
					{
						$field->value = $setminute;
					}
					else
					{
						$field->value = $sethour.$setminute;
					}
				}
				else if (strtolower($setam) == "am - late night")
				{
					if ($sethour == "12")
					{
						$field->value = ($sethour * 1 + 12).$setminute;
					}
					else
					{
						$field->value = ($sethour * 1 + 24).$setminute;
					}
				}
				else
				{
					if ($sethour=="12")
					{
						$field->value = $sethour.$setminute;
					}
					else
					{
						$field->value = ($sethour * 1 + 12).$setminute;
					}
				}

				$_POST[$field->name] = $field->value;
			}

			$setminute = "";
			$sethour = "";
			$setam = "";
			if ($field->value!="" && is_numeric($field->value))
			{
				$setminute = $field->value % 100;
				if ($setminute < 10)
				{
					$setminute = "0" . ($setminute * 1);
				}
				$sethour = ($field->value - $setminute) / 100;
				if ($sethour == 0)
				{
					$sethour = "12";
					$setam = "am";
				}
				else if ($sethour == 12)
				{
					$setam = "pm";
				}
				else if ($sethour == 24)
				{
					$sethour -= 12;
					$setam = "am - late night";
				}
				else if ($sethour > 24)
				{
					$sethour -= 24;
					$setam = "am - late night";
				}
				else if ($sethour > 12)
				{
					$sethour -= 12;
					$setam = "pm";
				}
				else
				{
					$setam = "am";
				}
			}

			$colcode = create_form_select("Hour", $field->name."_hour", $sethour, "numeric", "01", "12")."&nbsp;";
			$colcode .= create_form_select("Minute", $field->name."_minute", $setminute, "numeric", "00", "59")."&nbsp;";

			$ampm = array( "am", "pm" );
			if ($field->type != "time_no_late_night")
			{
				$ampm[] = "am - late night";
			}
			$colcode .= create_form_select("am/pm", $field->name."_am", $setam, "list", $ampm);
			$field_in_table = true;

			$check_required = array(
				array( $field->name."_hour",		$field->title." Hour" ),
				array( $field->name."_minute",	$field->title." Minute" ),
				array( $field->name."_am",		$field->title." am/pm" )
			);
		}
		else if ($field->type == "timespan")
		{
			if ($form_posted)
			{
				$sethour = $_POST[$field->name."_hour"];
				$setminute = $_POST[$field->name."_minute"];

				$field->value = $sethour.$setminute;
				$_POST[$field->name] = $field->value;
			}
			$setminute = "";
			$sethour = "";
			if ($field->value!="" && is_numeric($field->value))
			{
				$setminute = $field->value % 100;
				if ($setminute < 10)
				{
					$setminute = "0".($setminute * 1);
				}
				$sethour = ($field->value - $setminute) / 100;
			}

			$colcode = create_form_select("Hour", $field->name."_hour", $sethour, "numeric", "00", "12")."&nbsp;";
			$colcode .= create_form_select("Minute", $field->name."_minute", $setminute, "numeric", "00", "59")."&nbsp;";
			$field_in_table = true;

			$check_required = array(
				array( $field->name."_hour",		$field->title." Hour" ),
				array( $field->name."_minute",	$field->title." Minute" )
			);
		}
		else if ($field->type == "password")
		{
			$colcode = '<input type="password" value="'.$field->value.'" name="'.$field->name.'"';
			foreach ($field->props as $prop_key => $prop_val)
			{
				$colcode .= " ".$prop_key."=\"".str_replace("\"", "\\\"", $prop_val)."\"";
			}
			$colcode .= ">";
		}
		else if ($field->type == "create_password")
		{
			$field->title .= "<br>Confirm Password";
			$colcode .= "<input type='password' name='".$field->name."' value=\"\">";
			$colcode .= "<br><input type='password' name='confirm_".$field->name."' value=\"\">";
			$js_validate[] = "if (document.form1.".$field->name.".value!='' && document.form1.".$field->name.".value != document.form1.confirm_".$field->name.".value) alert('The values for \"".$field->name."\" must match to continue'); ";
			if ($field->value=="")
			{
				$field->db_include = false;
			}
			else
			{
				$field->encode_field = "PASSWORD";
			}
		}
		else if (in_array($field->type, array( "text_encrypted", "integration_encrypted" )))
		{
			if ($rowid)
			{
				if (isset($_POST['posted']))
				{
					$special_query = "UPDATE ".$use_db."`".$tablename."` SET `".$field->name."`=AES_ENCRYPT('".str_replace("'", "''", $field->value)."', '".$field->props['key']."') WHERE `id` = '".$rowid."'";
				}
				else
				{
					$enc_query = lavu_query("SELECT AES_DECRYPT(`".$field->name."`, '[1]') as `".$field->name."` FROM ".$use_db."`".$tablename."` WHERE id = '[2]'", $field->props['key'], $rowid);
					if ($enc_query->num_rows)
					{
						$enc_read = mysqli_fetch_assoc($enc_query);
						$field->value = $enc_read[$field->name];
						$field_in_quote_value = str_replace("\"", "&quot;", $field->value);
					}
				}
			}
			$input_type = ($field->type == "integration_encrypted")?"text":"password";
			$colcode .= "<input style='width:165px' type='".$input_type."' name='".$field->name."' value=\"".$field_in_quote_value."\">";
			$field->db_include = false;
		}
		else if ($field->type=="enc_text")
		{
			if ($rowid)
			{
				$kioskIntegration = explode(':',$field->name);
				$kioskIntegrationName = $kioskIntegration[1];
				if (isset($_POST['posted']))
				{
					$special_query = "UPDATE ".$use_db."`".$tablename."` SET `value`=AES_ENCRYPT('".str_replace("'", "''", $field->value)."', '".$field->props['key']."') WHERE `setting` = '".$kioskIntegrationName."'";
				}
				else
				{
					$enc_query = lavu_query("SELECT AES_DECRYPT(`value`, '[1]') as `value` FROM ".$use_db."`".$tablename."` WHERE `setting` = '".$kioskIntegrationName."'", $field->props['key']);

					if ($enc_query->num_rows)
					{
						$enc_read = mysqli_fetch_assoc($enc_query);
						$field->value = $enc_read['value'];
						$field_in_quote_value = str_replace("\"", "&quot;", $field->value);
					}
				}
			}
			$input_type = ($field->type == "enc_text")?"text":"password";
			$field->db_include = false;

			if ($location_info['kiosk_gateway'] == 'PayPal') 
			{
				$colcode .= "<input style='width:165px background-color:black' type='".$input_type."' name='".$field->name."' value=\"".$field_in_quote_value."\" >";
				lavu_query("UPDATE ".$use_db."`".$tablename."` SET `value`='Sale' WHERE `setting` = 'kiosk_cc_transtype'");
			}
			else {
				$colcode .= "<input style='width:165px' type='".$input_type."' name='".$field->name."' value=\"".$field_in_quote_value."\">";
			}
		}

		else if ($field->type=="bool")
		{
			$default = (int)(isset($field->props) && is_array($field->props) && count($field->props) > 0 && in_array("TRUE", $field->props));
			if ($field_in_quote_value == "")
			{
				$field_in_quote_value = "$default";
			}
			if ($field->name=="config:show_hold_and_fire"){
				$click1='disabled_chkbox();';
			}
			else if ( $field->name == "value17" ){
			    $click1 = 'getSelectedPrinterTypes(this.checked);';
			}
			else{
				$click1='';
			}
			if ($field->name == "enable_cp3_beta") {
			    global $modules;
			    if ($modules->hasModule('extensions.limited_cp_beta')) {
			        $field->value = 1;
			    }
			    else {
			        $field->value = 0;
			    }
			}
			$colcode .= "<input type='hidden' name='".$field->name."' id='".$field->name."' value=\"".$field_in_quote_value."\">";
			$onClick = '';
			if ($field->name == "config:enable_secondary_currency")
			{
			    $onClick = 'showOnSecondaryCurrency(this.checked)';
			}
			if ($field->name == "config:enable_multiple_secondary_currency")
			{
				$onClick = 'showOnMultipleCurrency(this.checked)';
			}
			if ($field->name == "enable_tip_sharing")
			{
				$onClick = 'showHideTipSettings(this.checked)';
			}
			if ($field->name == "include_server_owes") {
			    if ($field->value == 1 || $field->value == "") {
			        $checked_tip = " checked";
			    } else {
			        $checked_tip = "";
			    }
			}

			if ($field->name == "config:norway_mapping") {
			    $colcode .= "<input type='checkbox' onchange='getNorwayConfigFields()' onclick='if (this.checked) setval = \"1\"; else setval = \"0\"; document.getElementById(\"".$field->name."\").value = setval; '";
			} else if ($field->name == "config:enable_fiscal_receipt_templates")
			{
				$colcode .= "<input type='checkbox' onchange='getFiscalFields(this.checked)' onclick='if (this.checked) setval = \"1\"; else setval = \"0\"; document.getElementById(\"".$field->name."\").value = setval; '";
			}
			else if ($field->name == "config:enable_fiscal_receipt_templates") {

            }
			else if ($field->name == "config:enable_fiscal_invoice")
			{
				$colcode .= "<input type='checkbox' onchange='enablefiscalinvoice(this.checked)' onclick='if (this.checked) setval = \"1\"; else setval = \"0\"; document.getElementById(\"".$field->name."\").value = setval; '";
			}
			else if ($field->name == "config:offline_cc_transaction") {
				$colcode .= "<input type='checkbox' onchange='showOfflineCCToolTips(this.checked)' onclick='if (this.checked) setval = \"1\"; else setval = \"0\"; document.getElementById(\"".$field->name."\").value = setval; '";
			}
			else if ($field->name == "config:manage_item_availability") {
				$colcode .= "<input type='checkbox' onchange='getMenuItemAccesslevelField()' onclick='if (this.checked) setval = \"1\"; else setval = \"0\"; document.getElementById(\"".$field->name."\").value = setval; '";
			} else {
			    $colcode .= "<input type='checkbox' onclick='if (this.checked) setval = \"1\"; else setval = \"0\"; document.getElementById(\"".$field->name."\").value = setval; $click1 $onClick ' $checked_tip";
			} 

			if ($field->value == "1")
			{
				$colcode .= " checked";
			}
			$colcode .= ">";
		}
		else if ($field->type=="negative bool")
		{
			$default = (int)(isset($field->props) && is_array($field->props) && count($field->props) > 0 && in_array("FALSE", $field->props));
			if ($field_in_quote_value == "")
			{
				$field_in_quote_value = "$default";
			}
			$colcode .= "<input type='hidden' name='".$field->name."' id='".$field->name."' value=\"".$field_in_quote_value."\">";
			$colcode .= "<input type='checkbox' onclick='if (this.checked) setval = \"0\"; else setval = \"1\"; document.getElementById(\"".$field->name."\").value = setval; '";
			if ($field->value == "0")
			{
				$colcode .= " checked";
			}
			$colcode .= ">";
		}
		else if ($field->type == "toggle_list")
		{
			$currentValues = explode(',', $field->value );
			$newValues = array();
			foreach ($field->props as $prop_key => $prop_val)
			{
				$sub_field_name = $prop_key;
				$posted_field_name = $field->name."_".$sub_field_name;

				$checked = "";
				if (in_array($sub_field_name, $currentValues))
				{
					$checked = 'checked';
				}
				$colcode .= "<div onclick='if (event.target==this) this.children[0].click();' style='cursor: pointer;'><input type='checkbox' name='".$posted_field_name."' ".$checked." />".$prop_val."</div>";

				if (isset($_POST[$posted_field_name]) && $_POST[$posted_field_name]=="on")
				{
					$newValues[] = $sub_field_name;
				}
			}

			if ($form_posted)
			{
				$field->value = implode(',', $newValues);
			}
		}
		else if ($field->type == "radio")
		{
			foreach ($field->props as $prop_key => $prop_val)
			{
				$colcode .= "<input name='".$field->name."' type='radio' value=\"".str_replace("\"", "&quot;", $prop_key)."\"";
				if ($prop_key == $field->value)
				{
					$colcode .= " checked";
				}
				$colcode .= ">".$prop_val."<br>";
			}
		}
		else if ($field->type == "select")
		{
			if ($field->name == "config:primary_currency_code" || $field->name == "config:secondary_currency_code" || $field->name == "config:business_country" ) {
				$setting_name=ltrim($field->name,"config:");
				$res = mysqli_fetch_assoc(mlavu_query("select * from `poslavu_".admin_info('dataname')."_db`.`config` where  `setting` = '$setting_name'"));
				$selected = $res[value2];

				if ($field->name == "config:business_country") {
					$selected = $res[value];
				}
			} else {
				$selected=$field->value;
			}
			$onchange_pickup = '';
			if ($field->name == "config:order_number_for_customer_pick_up") {
				$onchange_pickup = 'showPickUpNumber(this.value)';
			}
		    	$disabled = "";
		    	if ($is_epson_kds_checked == 1 && $field->name == "value10") {
		        	$disabled = 'disabled';
		    	}

			if ($field->name == "config:access_level_manage_item_availability") {
				if (empty($field->value)) {
					$selected = 3;
				}
			}
			if ($field->name == "config:level_to_access_dual_cash_drawer_pin") {
				if (empty($field->value)) {
					$selected = 2;
				}
			}
			$colcode .= "<select id= '".$field->name."' name='".$field->name."' onchange='".$onchange_pickup."' ".$disabled.">";
			$track_values[$field->name] = $field->value;
			foreach ($field->props as $prop_key => $prop_val)
			{

				if ($field->name == "config:business_country" && $prop_key == '' && trim($selected) !='') {
					continue;
				}

				$colcode .= "<option value=\"".str_replace("\"", "&quot;", $prop_key)."\"";
				if ($prop_key == $selected)
				{
					$colcode .= " selected";
				}
				$colcode .= ">".$prop_val."</option>";
			}
			$colcode .= "</select>";
		}
		else if ($field->type == "printers_dynamic_select")
		{
			$selections = mysqli_fetch_assoc(lavu_query("SELECT * FROM `config` WHERE `id` = '[1]'", $rowid));

			$long = (!empty($selections['value_long2']))?LavuJson::json_decode($selections['value_long2']):array();

			if (!isset($long['pair_mode']))
			{
				$long['pair_mode'] = "1";
			}

			if (!isset($long['post_print_pause']))
			{
				$long['post_print_pause'] = "0";
			}
			
			if (!isset($long['epson_kds']))
			{
				$long['epson_kds'] = "0";
			}

			$outer_select = "<select class='outer_select' id='printertype' name='printer_type' onchange='show_inner_select(this); change_character_image_setting();' ><option value=''> </option>";
			$props = $field->props;

			$ds_adjust_tables = array();
			$ds_adjust_cells = array();

			foreach ($props as $opt => $inner)
			{
				$ds_adjust_tables[] = $opt."_table";
				$outer_select .= "<option value='".$opt."'".(($opt == $long['brand'])?" selected='true'":"").">".$opt."</option>";
				$inner_select = "<table id='".$opt."_table' style='border:0;margin-left: -290px;'>";

				foreach ($inner as $inner_key => $inner_select_opts)
				{
					if ($inner_select_opts['hidden_vals'])
					{
						$encoded = LavuJson::json_encode($inner_select_opts['hidden_vals']);
						if (!empty($selections['value_long2'])) $encoded = $selections['value_long2'];  // KIOSK-273
						$hidden_vals = "<input type='hidden' name='hidden_vals' value='".$encoded."'>";
						$inner_select .= $hidden_vals;
					}

					$submit_name = str_replace(" ", "_", strtolower($inner_key));
					$ds_adjust_cells[] = $opt."_".$submit_name."_cell";

					$lcpm = ($inner_key == "Lavu Controller pair mode");
					$pp_p = ($inner_key == "Post-print pause");
					$insel_row_display = ($lcpm && (!strstr($track_values['setting'], "receipt") || $track_values[Epson_model]!="TM-T88V-i"))?"none":"block";
					$insel_onchange = ($opt=="Epson" && $inner_key=="Model")?" onchange = 'epsonModelDidChange(this);'":"";
					$insel_onchange_model = ($opt!="Epson" && $inner_key=="Model")?" onchange = 'set_model_select(this)'":""; 
					if($submit_name == 'epson_kds') {
					    $checked = ($long['epson_kds'] == 1) ? " checked" : "";
					    if($long['epson_kds'] == 1) {
					        $is_epson_kds_checked = 1;
					    }
					    $inner_select .=  "<tr style='display:block;'><td id='".$opt."_".$submit_name."_cell' style='width:283px; border-top:2px solid white; text-align:right;'>".$inner_key."</td><td><input type='checkbox' id='".$submit_name."' name='".$submit_name."' value='1' ".$checked." onchange='change_character_image_setting()'></td></tr>";
					}
					else {
					    if($submit_name == 'command_set'){
					        $inner_select.= "<tr".($lcpm?" id='lcpm_row'":"")." style='display:".$insel_row_display.";'><td id='".$opt."_".$submit_name."_cell' style='width:283px; border-top:2px solid white; text-align:right;'>".$inner_key.":</div></td><td><select ".$insel_onchange." name='".$submit_name."' class='inner_select' id='printer_command_$opt' onchange='set_command_select(this)' save_type='".$inner_select_opts['save_type']."' col='".$inner_select_opts['column']."'>";
    					    }
    					    else{
    					        $inner_select.= "<tr".($lcpm?" id='lcpm_row'":"")." style='display:".$insel_row_display.";'><td id='".$opt."_".$submit_name."_cell' style='width:283px; border-top:2px solid white; text-align:right;'>".$inner_key.":</div></td><td><select ".$insel_onchange." ".$insel_onchange_model." id='".$submit_name."_".$opt."' name='".$submit_name."' class='inner_select' save_type='".$inner_select_opts['save_type']."' col='".$inner_select_opts['column']."'>";
    					    }
    
    					    $found = 0;
    					    if (!$lcpm && !$pp_p) $inner_select .= "<option></option>";
    					    foreach ($inner_select_opts['options'] as $inner_key_opts => $inner_opts_val)
    					    {
    						$selected = "";
    						if ($selections['value6']==$inner_key_opts || $inner_opts_val==$long['model'] || ($lcpm && $inner_key_opts==$long['pair_mode']) || ($pp_p && $inner_key_opts==$long['post_print_pause']))
    						{
    							$selected = " selected='true'";
    							$track_values[$opt."_".$submit_name] = $inner_opts_val;
    							$found = 1;
    						}
    						$inner_select .= "<option ".(($submit_name == 'command_set')?" name='printer'":"")."  value ='$inner_key_opts'".$selected.">".$inner_opts_val."</option>";
    					    }
    					    $inner_select .= "</select></td></tr>";
				        }
				}
				$inner_select .= "</table>";

				$div_container .= "<div class='".$opt." inner_select_container' style='display:".(($opt == $long['brand'])?"block":"none").";'>".$inner_select."</div>";
			}
			$outer_select .= "</select>";

			$outer_select .= "<script language='javascript'>window.setTimeout(function(){adjustDynamicSelectElements('".$field->label_cell_id."', '".implode(",", $ds_adjust_tables)."', '".implode(",", $ds_adjust_cells)."')}, 200);</script>";

			$multi_container_div = "<div class ='multi_select'>".$outer_select.$div_container."</div>";
			$colcode .= $multi_container_div;

			if ($form_posted)
			{
				$decoded = (!empty($_POST['hidden_vals']))?LavuJson::json_decode($_POST['hidden_vals']):array('language'=>"ANK", "brand"=>$_POST['printer_type']);
				$decoded['brand'] = $_POST['printer_type'];
				$decoded['model'] = !empty($_POST['model'])?$_POST['model']:"default";
				if ($decoded['model'] == "TM-T88V-i")
				{
					$decoded['pair_mode'] = isset($_POST['lavu_controller_pair_mode'])?$_POST['lavu_controller_pair_mode']:"1";
				}

				if (isset($_POST['post-print_pause']))
				{
					$decoded['post_print_pause'] = $_POST['post-print_pause'];
				}
				
				if(isset($_POST['printer_type']) && strtolower($_POST['printer_type']) == 'epson' && $_POST['epson_kds'] == 1) {
				    $decoded['epson_kds'] =  $_POST['epson_kds'];
				}
				else {
				    unset($decoded['epson_kds']);
				}

				$update_code .= ", `value6` = '".$_POST['command_set']."', `value_long2` = '".LavuJSON::json_encode($decoded)."'";
				$field->value = $_POST['printer_type'];

				if (isset($_GET['rowid']) && !empty($_GET['rowid']))
				{
					lavu_query("UPDATE `config` SET `value6` = '[1]', `value_long2` = '[2]' where `id` = '[3]'", $_POST['value6'], $_POST['value_long2'], $_GET['rowid']);
				}
				else
				{
					$_POST['value_long2'] = LavuJSON::json_encode($decoded);
					$insert_keys.= ", `value6`, `value_long2`";
					$insert_vals.= ",'".$_POST['command_set']."', '".LavuJSON::json_encode($decoded)."'";
				}

				if (lavu_dberror())
				{
					error_log("error updating printers". lavu_dberror());
				}
			}
		}
		else if ($field->type == "date_chooser")
		{
			//Created by Brian D.
			//TODO FOR FUTURE DEV, WE MAY WANT TO HIDE CONSIDERING OTHER VALUES
			//We want to disable expiration for every discount_type that is not passbook
			/*
			$abortInclusion = false;
			if ( $_GET['mode'] == 'discount_types' && $tablename == 'discount_types' && !empty($_GET['rowid']) ){
				// Found $row_read as the original row from `$tablename` up at the top of this .php page.
				$specialVal = $row_read['special'];
				$abortInclusion = $specialVal != 'passbook';
			}
			*/
			ob_start();
			require_once(__DIR__."/../scripts/dateChooser.php");
			$colcode .= ob_get_contents();
			ob_end_clean();
			$fieldVal = $field->value;
			$colcode .= "<input type='hidden' id='".$field->name."' name='".$field->name."' value='".$fieldVal."'>";
			$defaultDate = !empty($field->props['default_date']) ? $field->props['default_date'] : $field->value;
			$colcode .= create_date_picker("$field->name", 0, 5, 0, 1, $defaultDate);
		}//STARTß
		else if ($field->type == "invoice_no_series_textbox")
		{
			if ($form_posted)
			{
				$series_start = $_POST[$field->name."_start"];
				$series_end= $_POST[$field->name."_end"];
				$series_prefix = trim($_POST['invoice_no_prefix']);
				$series_postfix = trim($_POST['invoice_no_postfix']);
				$invoice_valid_till = ($_POST['invoice_series_valid_till'])?date('Y-m-d H:i:s', strtotime($_POST['invoice_series_valid_till'])):'';
				$invoice_valid_till = ($invoice_valid_till)?strtotime(localize_datetime($invoice_valid_till, $location_info['timezone'])):'';
				$jsonval = array("start" => "$series_start", "end" => "$series_end", "prefix" => "$series_prefix", "postfix" => "$series_postfix", "validtill" => "$invoice_valid_till" );
				$jsonins = json_encode($jsonval);
				$field->value = $jsonins;
				$_POST[$field->name] = $field->value;
			}
			$dtype = array("start", "end");

			$invoiceSeriesData = json_decode(html_entity_decode($field_in_quote_value), true);
			$to = ' : ';
			foreach($dtype as $dvalkey)
			{
				$colcode .= "<input type='hidden' name='".$field->name."_".$dvalkey."' id='".$field->name."_".$dvalkey."' value=\"".$field_in_quote_value."\">";
				$colcode .= "<input type='number'  name='".$field->name."_".$dvalkey."' min='1' step='1' onpaste='return false' onkeypress='return isNumberKey(event)' value=\"".$invoiceSeriesData[$dvalkey]."\" id='".$field->name."' > " . $to;
				$to = '';
			}
		}
		else if ($field->type=="date_picker")
		{
			$validtill_time = $invoiceSeriesData['validtill'];
			$validtillDate = ($validtill_time)?date('m/d/Y', $validtill_time):'';
			$fieldName = $field->name;
			$colcode .= "<script type='text/javascript'>
							$(document).ready(function() {
								$('[name=$fieldName]').datepicker({
										changeMonth: true,
										changeYear: true,
										inline: true,
										minDate: 0,
										dateFormat: 'mm/dd/yy'
									});
							});
						</script>";
			$colcode .= "<input type='text' size='12' onKeyDown='checkDatePicker(this)' name='".$field->name."' id='".$field->name."' class='calendarImg' value='".$validtillDate."'>";
			$field->db_include = false;
		}
		else if ($field->type == "invoice_no_prefix_textbox") {
			$prefixVal = $invoiceSeriesData['prefix'];
			$colcode .= "<input type='text' size='10' name='".$field->name."' value=\"".$prefixVal."\" id='".$field->name."'>";
			$field->db_include = false;
		}
		else if ($field->type == "invoice_no_postfix_textbox") {
			$postfixVal = $invoiceSeriesData['postfix'];
			$colcode .= "<input type='text' size='10' name='".$field->name."' id='".$field->name."' value=\"".$postfixVal."\">";
			$field->db_include = false;
		}
		//end
		else if (in_array($field->type, array( "dbselect_list", "tipout_rules", "employee_classes", "choice_cost", "tiprefund_rules", "refund_type","tip_sharing_rule","tip_sharing_period","include_server_owes","enable_tip_sharing", "tipout_breakdownL2329
				")))
		{
			$dname = $field->name;

			$apos = "&apos;";
			$select_options = "";

			if ($field->type == "employee_classes")
			{
				foreach ($field->props as $key => $val)
				{
					$select_options .= "<option value=\"".$key."\">".str_replace("'", $apos, $val)."</option>";
				}
			}
			else
			{
				$select_options = "<option value=\"\"></option>";

				$dbselect_table = $field->props[0];
				$dbselect_id = $field->props[1];
				$dbselect_title = $field->props[2];
				$dbselect_locationid = $field->props[3];

				$extra_query = "";
				if ($dbselect_table=="lk_event_types" || $dbselect_table=="emp_classes")
				{
					$extra_query = "`_deleted`!='1'";
				}

				if ($dbselect_locationid == "")
				{
					if ($extra_query != "")
					{
						$extra_query = " WHERE ".$extra_query;
					}
					$sel_query = lavu_query("SELECT * FROM `[1]`".$extra_query." ORDER BY `[2]` ASC", $dbselect_table, $dbselect_title);
				}
				else
				{
					if ($extra_query != "")
					{
						$extra_query = " AND " . $extra_query;
					}
					$sel_query = lavu_query("SELECT * FROM `[1]` WHERE `[4]` = '[5]'".$extra_query." ORDER BY `[2]` ASC", $dbselect_table, $dbselect_title, $dbselect_id, $dbselect_locationid, $locationid);
				}
				while ($sel_read = mysqli_fetch_assoc($sel_query))
				{
					$select_options .= "<option value=\"".$sel_read[$dbselect_id]."\">".str_replace("'", $apos, $sel_read[$dbselect_title])."</option>";
				}
			}

			$get_js_code = $dname."_itemnum = 0;
				function add_more_".$dname."(isetval, qsetval)
				{
					if (!isetval)
					{
						isetval = '';
					}

					if (!qsetval)
					{
						qsetval = '1';
					}
					var item_options = '".$select_options."';
					var t = document.createElement('div');";

			$extra_js_code = "";
			$extra_getset_code = "";
			$supergroups_code = $tipout_breakdown_options = "";
			global $modules; 
			if ($field->type == "tipout_rules")
			{
				$break_down_data = $row_read['tipout_break'];
				$colcode .= "<input type='hidden' name='savedValues' id='savedValues' value=\"$break_down_data\">";
				$configdata = lavu_query("select * from `config` where setting='management_server_tips_total_or_cash_tips'");
				if ($configdata->num_rows){
					$defaultConfigTipOut = mysqli_fetch_assoc($configdata);
					$tipoutconfig = ($defaultConfigTipOut['value']=='cash_only') ? 'cashtipsonly' : 'totaltips';
				}
				$extra_js_code .= "<td>%<input type=\"text\" class=\"tipPercentage\" id=\"percentage_".$dname."_item' + ".$dname."_itemnum + '\" size=\"3\" onkeyup=\"".$dname."_updated()\" oncopy=\"return false\" onpaste=\"return false\" autocomplete=\"off\" /></td>";
				$extra_js_code .= "<td><select id=\"primary_".$dname."_item' + ".$dname."_itemnum + '\" onchange=\"checkUserExistInClass(this.value, this, \'primary\');".$dname."_updated()\">' + item_options + '</select></td>";
				$extra_getset_code .= "
					if (isetparts.length > 1) document.getElementById('percentage_".$dname."_item' + ".$dname."_itemnum).value = isetparts[1];
					if (isetparts.length > 2) document.getElementById('primary_".$dname."_item' + ".$dname."_itemnum).value = isetparts[2]; ";
				$extra_set_code .= "iival += '|' + document.getElementById('percentage_".$dname."_item' + i).value;
					iival += '|' + document.getElementById('primary_".$dname."_item' + i).value;
					if (iival == '||')
					{
						iival = '';
					}";
				
				$supergrp_query = lavu_query("SELECT * FROM `super_groups` WHERE `_deleted` != '1'");
				
				$super_i="0";
				while ($supergrp_read = mysqli_fetch_assoc($supergrp_query))
				{
					$supergroups_code .= "<tr class=\"supergrouprows_".$dname."_item' + ".$dname."_itemnum + '\" style=\"display:none;\" id=\"spgrprows_".$super_i.$dname."_item' + ".$dname."_itemnum + '\"><td></td><td class=\"switch-field\"><input type=\"checkbox\"  id=\"spgrp2_".$super_i.$dname."_item' + ".$dname."_itemnum + '\" name=\"spgrp2_".$dname."_item' + ".$dname."_itemnum + '\" value=\"".$supergrp_read['id']."\" onclick=\"".$dname."_updated()\"/><label for=\"spgrp2_".$super_i.$dname."_item' + ".$dname."_itemnum + '\">".$supergrp_read['title']."</label></td></tr>";
					$super_i++;
				}
				$hours_worked_tipout ="";
				$tipout_breakdown_options .=  "<tr id=\"tiprows2_".$dname."_item' + ".$dname."_itemnum + '\"><input type=\"hidden\" name=\"myvalues[]\" id=\"break_'+ ".$dname."_itemnum + '\" ><td class=\"switch-field\"><input type=\"radio\" class=\"switch-field2\" id=\"evenly_".$dname."_item' + ".$dname."_itemnum + '\" name=\"breakdown_".$dname."_item' + ".$dname."_itemnum + '\" value=\"evenly\" onclick=\"break_updated('+ ".$dname."_itemnum + ')\"/><label for=\"evenly_".$dname."_item' + ".$dname."_itemnum + '\">Evenly</label></td><td class=\"switch-field\"><input type=\"radio\" class=\"switch-field2\" id=\"hoursworked_".$dname."_item' + ".$dname."_itemnum + '\" name=\"breakdown_".$dname."_item' + ".$dname."_itemnum + '\" value=\"hoursworked\" onclick=\"break_updated('+ ".$dname."_itemnum + ')\" /><label for=\"hoursworked_".$dname."_item' + ".$dname."_itemnum + '\">Hours Worked</label></td><td class=\"switch-field\">&nbsp;</td></tr>";
				
			}
			else if ($field->type == "choice_cost")
			{
				$extra_js_code .= "<td>Choice:<input type=\"text\" id=\"choice_".$dname."_item' + ".$dname."_itemnum + '\" size=\"3\" onkeyup=\"".$dname."_updated()\"/></td>";
				$extra_js_code .= "<td>Cost:<select id=\"cost_".$dname."_item' + ".$dname."_itemnum + '\" onchange=\"".$dname."_updated()\">' + item_options + '</select></td>";
				$extra_getset_code .= "
					if (isetparts.length > 1) document.getElementById('choice_".$dname."_item' + ".$dname."_itemnum).value = isetparts[1];
					if (isetparts.length > 2) document.getElementById('cost_".$dname."_item' + ".$dname."_itemnum).value = isetparts[2]; ";
				$extra_set_code .= "iival += '|' + document.getElementById('choice_".$dname."_item' + i).value;
					iival += '|' + document.getElementById('cost_".$dname."_item' + i).value;
					if (iival == '||')
					{
						iival = '';
					}";
			}
			else if ($field->type=="employee_classes" && $modules->hasModule('employees.payrates'))
			{
				$extra_js_code .= "<td><input type=\"number\" id=\"rate_".$dname."_item' + ".$dname."_itemnum + '\" size=\"3\" onkeyup=\"".$dname."_updated()\" onmouseup=\"".$dname."_updated()\" placeholder=\"Rate\"/></td>";
				$extra_getset_code .= "
					isetparts = isetval.split('|');
					if (isetparts.length > 1) document.getElementById('rate_".$dname."_item' + ".$dname."_itemnum).value = isetparts[1]; ";
				$extra_set_code .= "iival += '|' + document.getElementById('rate_".$dname."_item' + i).value;
					if (iival == '|')
					{
						iival = '';
					}";
			}
			if ($field->type != "tipout_rules" && $field->type != "tiprefund_rules" && $field->type != "refund_type"){
				$get_js_code .= "
					t.innerHTML += '<table><tr>".$extra_js_code."<td><select id=\"".$dname."_item' + ".$dname."_itemnum + '\" onchange=\"".$dname."_updated()\">' + item_options + '</select></td></tr></table>';";
			}
			else if ($field->type == "tipout_rules"){
			$get_js_code .= "
					t.innerHTML += '<table><tr>".$extra_js_code."<td><select id=\"".$dname."_item' + ".$dname."_itemnum + '\" onchange=\"checkUserExistInClass(this.value, this, \'secondary\');".$dname."_updated();validatePrimary(this);\">' + item_options + '</select></td></tr><tr><td class=\"switch-field\"><input type=\"radio\" class=\"switch-field2\" id=\"tip_".$dname."_item' + ".$dname."_itemnum + '\" name=\"outtype_".$dname."_item' + ".$dname."_itemnum + '\" value=\"tips\" onclick=\"".$dname."_updated();show_tips(this);\"/><label for=\"tip_".$dname."_item' + ".$dname."_itemnum + '\">Tips</label></td><td class=\"switch-field\"><input type=\"radio\" class=\"switch-field2\" id=\"sale_".$dname."_item' + ".$dname."_itemnum + '\" name=\"outtype_".$dname."_item' + ".$dname."_itemnum + '\" value=\"sales\"  onclick=\"".$dname."_updated();hide_tips(this);\"/><label for=\"sale_".$dname."_item' + ".$dname."_itemnum + '\">Sales</label></td></tr><tr id=\"tiprows_".$dname."_item' + ".$dname."_itemnum + '\"><td class=\"switch-field\"><input type=\"radio\" class=\"switch-field2\" id=\"totaltips2_".$dname."_item' + ".$dname."_itemnum + '\" name=\"outtype2_".$dname."_item' + ".$dname."_itemnum + '\" value=\"totaltips\" onclick=\"".$dname."_updated()\"/><label for=\"totaltips2_".$dname."_item' + ".$dname."_itemnum + '\">Total Tips</label></td><td class=\"switch-field\"><input type=\"radio\" class=\"switch-field2\" id=\"cashtipsonly2_".$dname."_item' + ".$dname."_itemnum + '\" name=\"outtype2_".$dname."_item' + ".$dname."_itemnum + '\" value=\"cashtipsonly\" onclick=\"".$dname."_updated()\" /><label for=\"cashtipsonly2_".$dname."_item' + ".$dname."_itemnum + '\">Cash Tips Only</label></td><td class=\"switch-field\"><input type=\"radio\" class=\"switch-field2\" id=\"cardtipsonly2_".$dname."_item' + ".$dname."_itemnum + '\" name=\"outtype2_".$dname."_item' + ".$dname."_itemnum + '\" value=\"cardtipsonly\"  onclick=\"".$dname."_updated()\" /><label for=\"cardtipsonly2_".$dname."_item' + ".$dname."_itemnum + '\">Card Tips Only</label></td></tr><tr id=\"salesrows_".$dname."_item' + ".$dname."_itemnum + '\" style=\"display:none;\"><td class=\"switch-field\"><input type=\"radio\" class=\"switch-field2\" id=\"totalsales2_".$dname."_item' + ".$dname."_itemnum + '\" name=\"tsalestype2_".$dname."_item' + ".$dname."_itemnum + '\" value=\"totalsales\" onclick=\"".$dname."_updated();hide_supergroups(this);\"/><label for=\"totalsales2_".$dname."_item' + ".$dname."_itemnum + '\">All Sales</label></td><td class=\"switch-field\"><input type=\"radio\" class=\"switch-field2\" id=\"supergroups2_".$dname."_item' + ".$dname."_itemnum + '\" name=\"tsalestype2_".$dname."_item' + ".$dname."_itemnum + '\" value=\"supergroups\" onclick=\"".$dname."_updated();show_supergroups(this);\" /><label for=\"supergroups2_".$dname."_item' + ".$dname."_itemnum + '\">Super Groups</label></td></tr>".$supergroups_code."".$tipout_breakdown_options."</table>';";
			}
			$get_js_code .= "
					if (document.getElementById('list_$dname')) {
						document.getElementById('list_$dname').appendChild(t);
					}
					isetparts = isetval.split('|');
					if (document.getElementById('".$dname."_item' + ".$dname."_itemnum)) {
                        document.getElementById('".$dname."_item' + ".$dname."_itemnum).value = isetparts[0];
    			     	}
                    if (isetparts.length > 0){
                        if (typeof isetparts[3] == 'undefined' || typeof isetparts[4] === 'undefined') {
                            isetparts[1] = '';
                            isetparts[4] = '".$tipoutconfig."';
                            isetparts[3] = 'tips';
                        }
                        if (isetparts[3] != '' && isetparts[3]=='tips'){
                            if (document.getElementById('tip_".$dname."_item' + i)) {  
                                document.getElementById('tip_".$dname."_item' + i).checked = 'true';
                            }
                        }
                        if (isetparts[3] != '' && isetparts[3] == 'sales'){
                            if (document.getElementById('tip_".$dname."_item' + i)) {
                                document.getElementById('sale_".$dname."_item' + i).checked = 'true';
                            }
                            document.getElementById('salesrows_".$dname."_item' + i).style.display = 'table-row';
                            document.getElementById('tiprows_".$dname."_item' + i).style.display = 'none';
                            if (isetparts[4] != '' && isetparts[4] == 'supergroups') {
                                document.getElementById('supergroups2_".$dname."_item' + i).checked = 'true';
            					   show_supergroups(document.getElementsByClassName('supergrouprows_".$dname."_item' + i));
                                var spids =isetparts.slice(5);
                                var spgrps =document.getElementsByClassName('supergrouprows_".$dname."_item' + i);
                                for(var sp=0;sp<spgrps.length;sp++){
                                    if (spids.indexOf(document.getElementById('spgrp2_'+sp+'".$dname."_item' + i).value)>-1) {
                                        document.getElementById('spgrp2_'+sp+'".$dname."_item' + i).checked = 'true';
                                    }
                                }
                            }
                            else if (isetparts[4] != '' && isetparts[4] == 'totalsales') {
                                if (document.getElementById('totalsales2_".$dname."_item' + i)) {
                                    document.getElementById('totalsales2_".$dname."_item' + i).checked = 'true';
                                }
                            }
                        }
                        if (isetparts[4] != '' && isetparts[4] == 'totaltips') {
                            if (document.getElementById('totaltips2_".$dname."_item' + i)) {
                                document.getElementById('totaltips2_".$dname."_item' + i).checked = 'true';
                            }
                        }
                        if (isetparts[4] != '' && isetparts[4] == 'cashtipsonly'){
                            if (document.getElementById('cashtipsonly2_".$dname."_item' + i)) {
                                document.getElementById('cashtipsonly2_".$dname."_item' + i).checked = 'true';
                            }
                        }
                        if (isetparts[4] != '' && isetparts[4] == 'cardtipsonly'){
                            if (document.getElementById('cashtipsonly2_".$dname."_item' + i)) {  
                                document.getElementById('cardtipsonly2_".$dname."_item' + i).checked = 'true';
                            }
                        }
                    }
                    ".$extra_getset_code."
				    ".$dname."_itemnum ++;
                }
				setvals = \"".$field_in_quote_value."\";
				setvals = setvals.split(',');
				for(i = 0; i < setvals.length; i++)
				{
					add_more_$dname(setvals[i]);
				}";

			$set_js_code = "
				setval = '';
				sel_count = 0;
				var ids_spg = '';
				for(i=0; i<".$dname."_itemnum; i++) {
                    var iival2 = iival3 = '';
					
					var item_element = document.getElementById('".$dname."_item' + i);

                    if (document.getElementById('tip_".$dname."_item' + i) && document.getElementById('tip_".$dname."_item' + i).checked) {
                    var item_element2 = document.getElementById('tip_".$dname."_item' + i);
                    iival2 = item_element2.value;
                    }
                     if (document.getElementById('sale_".$dname."_item' + i) && document.getElementById('sale_".$dname."_item' + i).checked) {
                    var item_element2 = document.getElementById('sale_".$dname."_item' + i);
                    iival2 = item_element2.value;
                    }
                    if (document.getElementById('totaltips2_".$dname."_item' + i) && document.getElementById('totaltips2_".$dname."_item' + i).checked) {
                    var item_element3 = document.getElementById('totaltips2_".$dname."_item' + i);
                    iival3 = item_element3.value;
                    }
                    if (document.getElementById('cashtipsonly2_".$dname."_item' + i) && document.getElementById('cashtipsonly2_".$dname."_item' + i).checked) {
                    var item_element3 = document.getElementById('cashtipsonly2_".$dname."_item' + i);
                    iival3 = item_element3.value;
                    }    
                    if (document.getElementById('cardtipsonly2_".$dname."_item' + i) && document.getElementById('cardtipsonly2_".$dname."_item' + i).checked) {
                    var item_element3 = document.getElementById('cardtipsonly2_".$dname."_item' + i);
                    iival3 = item_element3.value;
                    }
					
					if (document.getElementById('totalsales2_".$dname."_item' + i) && document.getElementById('totalsales2_".$dname."_item' + i).checked) {
	                    var item_element3 = document.getElementById('totalsales2_".$dname."_item' + i);
	                    iival3 = item_element3.value;
						//console.log(iival3);
                    }
					
					if (document.getElementById('supergroups2_".$dname."_item' + i) && document.getElementById('supergroups2_".$dname."_item' + i).checked) {
	                    var spgrps =document.getElementsByClassName('supergrouprows_".$dname."_item' + i) ;
						
						iival3 = document.getElementById('supergroups2_".$dname."_item' + i).value;
						
						var ids_spg = '';
						for(var sp=0;sp<spgrps.length;sp++){
							if (document.getElementById('spgrp2_'+sp+'".$dname."_item' + i).checked){
								ids_spg += document.getElementById('spgrp2_'+sp+'".$dname."_item' + i).value+'|';
							}
						}
						if (ids_spg != ''){
							iival3 += '|'+ids_spg; 
						}
                    }
					 if (document.getElementById('totalsales2_".$dname."_item' + i) && document.getElementById('totalsales2_".$dname."_item' + i).checked) {
	                    var item_element3 = document.getElementById('totalsales2_".$dname."_item' + i);
	                    iival3 = item_element3.value;
                    }
						
					if (!item_element )
					{
						continue;
					}
					var iival = item_element.value;

                    ". $extra_set_code ."
					if (iival != '')
					{
						if (setval != '')
						{
							setval += ',';
						}
						if (iival2 == 'tips' && iival3 == ''){
							iival3 = 'totaltips';
						}
						setval += iival;
                        setval += '|'+iival2;
                       
                        setval += '|'+iival3;
                      
                        
						sel_count++;
					}
				}
				document.getElementById('$dname').value = setval;";
				
			if ($field->type != "tiprefund_rules" && $field->type != "refund_type"){
				$colcode .=  "<input type='hidden' id='".$dname."' name='".$dname."' value=\"".$field_in_quote_value."\"></input>
				<div id='list_".$dname."'>
							</div>
							<div class='addMoreBtn' onclick='add_more_$dname()'><span class='plus'> + </span>  <span>".speak("Add More")."</span></div>";
			}
				
			if ($field->type == "tiprefund_rules"){
				$colcode .=  "<table><tr><td>%<input type=\"text\" id=\"tiprefund_rules\" name=\"tiprefund_rules\" size=\"3\"  value=\"".$field_in_quote_value."\" oncopy=\"return false\" onpaste=\"return false\" /></td></tr></table>";
			}
			if ($field->type == "refund_type") {
				$checkedNo=$checkedYes="";
				 
				if ($field_in_quote_value == 'Card Sales') {
					$checkedNo = 'checked';
				}
				if ($field_in_quote_value == 'Card Tips') {
					$checkedYes = 'checked';
				}
				$colcode .=  "<table><tr><td class=\"switch-field\"><input type=\"radio\" id=\"switch_left\" name=\"refund_type\" value=\"Card Tips\" $checkedYes/><label for=\"switch_left\">Card Tips</label></td></tr></table>";
			}
			if ($field->type == "tip_sharing_rule") {
				$evenly = $row_read['tip_sharing_rule'] == 'Evenly'? 'checked': '';
				$hours_worked = $row_read['tip_sharing_rule'] == 'Hours Worked'? 'checked': '';
				
				$colcode = " ";
				$colcode .=  "<table><tr><td class=\"switch-field\"><input type=\"radio\" $evenly id=\"evenly\" name=\"tip_sharing_rule\" value=\"Evenly\" /><label for=\"evenly\">Evenly</label></td><td class=\"switch-field\">
				 <input type=\"radio\" $hours_worked  id=\"hoursWorked\" name=\"tip_sharing_rule\" value=\"Hours Worked\"/><label for=\"hoursWorked\">Hours Worked</label></td></tr>
				 </table>";
				}
				if ($field->type == "tip_sharing_period") {
					list($tip_Sharing_type,$time) = explode('|',$row_read['tip_sharing_period']);
					$ByShift = "";
					$tiprefundtr = "display:none";
					$weeklytip = "display:none";
					$ByShiftvalue = "";
					if ($tip_Sharing_type == 'ByShift') {
						$ByShiftvalue = $time;
						$ByShift = 'checked';
						$tiprefundtr = '';
						$weeklytip = 'display:none';
					}
					
					if ($tip_Sharing_type == 'Weekly') {
						$Weekly = 'checked';
						$weeklytip = '';
						$tiprefundtr = 'display:none';
					}
					
					$Daily = $tip_Sharing_type == 'Daily'? 'checked': '';
					$Weekly = $tip_Sharing_type == 'Weekly'? 'checked': '';
					$days_of_week=array(" ", "Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday");
					$select_options = "";
					for ($day=0;$day<8;$day++) {
					    if ($time == $days_of_week[$day]) {
					        $day_selected = 'selected';
					    }
					    else {
					        $day_selected = '';
					    }
					    $select_options .= "<option $day_selected  value=\"".$days_of_week[$day]."\">".$days_of_week[$day]."</option>";
					}
					$checkedNo = $checkedYes = "";
					$colcode = "";
					$colcode .=  "<table><tr><td class=\"switch-field\">
					 <input type=\"radio\" id=\"shift\" $ByShift name=\"tip_sharing_period\" value=\"ByShift\"\><label for=\"shift\">By Shift</label></td><td class=\"switch-field\">
					 <input type=\"radio\" id=\"daily\" $Daily name=\"tip_sharing_period\" value=\"Daily\"/><label for=\"daily\">Daily</label></td><td class=\"switch-field\">
					 <input type=\"radio\" id=\"weekly\" $Weekly name=\"tip_sharing_period\" value=\"Weekly\"/><label for=\"weekly\">Weekly</label>
					 </td></tr>
					 <tr id=\"tip_refundtr\" style=\"$tiprefundtr\"><td colspan=\"3\"><label for=\"text\">When does shift change occur?</label><input type=\"text\" id=\"text\" name=\"shift_time\" size=\"8\" value = \"$ByShiftvalue\" /></td></tr>
					 <tr id=\"weekly_tip\"  style=\"$weeklytip\"><td colspan=\"3\"><label for=\"select\">What day does your work week start on</label><select id= \"weeklytip \" name=\"weeklytip\">".$select_options."</select></td></tr>
					 </table>";
				}
				$colcode .= "<script language='javascript'>
					function break_updated(id) {
                            if (document.getElementById('evenly_tipout_rules_item' + id) && document.getElementById('evenly_tipout_rules_item' + id).checked) {
		                        var item_element4 = document.getElementById('evenly_tipout_rules_item' + id);
		                        var breakdownval = item_element4.value;
	                            document.getElementById('break_'+id).value = breakdownval;    
                            }
                            if (document.getElementById('hoursworked_tipout_rules_item' + id) && document.getElementById('hoursworked_tipout_rules_item' + id).checked) {
		                        var item_element4 = document.getElementById('hoursworked_tipout_rules_item' + id);
		                        var breakdownval = item_element4.value;
                                document.getElementById('break_'+id).value = breakdownval;
                            }
                        }
				    function ".$dname."_updated() {". 
				                                     $set_js_code ."
                                                    }". $get_js_code ."\n
			                 </script>";
				$colcode = str_replace(array("\r\n", "\n\r", "\n", "\r"), "\n", $colcode);
				$colcode = str_replace(array("\n			", "\n				"), "\n", $colcode);
		}
		else if ($field->type == "seperator")
		{
			$seperator_label = "";
			if ($field->title != "")
			{
				$seperator_label = "<span style='font-size:12px; color:#ffffff;'><b>".$field->title."</b></span>";
			}
			$help_link = "";
			if ($field->help_link != "")
			{
				$help_link = "<img onclick='openHelp(\"".$field->help_link."\");' src='images/btn_tutorials.png' width='18' height='15' style='border:none; margin: 0px 0px -3px 0px; cursor:pointer;'>";
			}
			$s_helptext = create_help_text_for_form_element($field, array( 'is_seperator' => TRUE, 'seperator_name' => $field->name, 'seperator_title' => $field->title));
			$rowcode = "<tr><td colspan='2' bgcolor='#98b624' style='color:#ffffff; font-weight:bold; height:27px; padding:5px 0px 2px 0px;' align='center'>".$s_helptext."&nbsp;".$seperator_label."&nbsp;".$help_link."</td></tr>";
			$field->db_include = false;
		}
		else if ($field->type == "custom") // allow custom html to be sent
		{
			$custom_label = "";
			if ($field->title != "")
			{
				$custom_label = $field->title;
			}

			$rowcode = "<tr><td colspan='2'>".$custom_label."</td></tr>";
			$field->db_include = false;
		}
		else if ($field->type == "submit")
		{
			$colcode .= "<input id='_setting_btn_submit_id' type='button' value='".$field->title."' onclick='form1_validate()'>";
			$field->db_include = false;
			$show_coltitle = false;
		}
		else if ($field->type == "emp_classes_submit")
		{
			$colcode .= "<input id='_setting_btn_submit_id' type='button' value='".$field->title."' onclick='empClassHasUser()'>";
			$field->db_include = false;
			$show_coltitle = false;
		}
		else if ($field->type == "lavutogo_submit")
		{
			$colcode .= "<input id='_setting_btn_submit_id' type='button' value='".$field->title."' onclick='lavutogo_validate();'>";
			$field->db_include = false;
			$show_coltitle = false;
		}
		else if ($field->type == "printer_submit")
		{
			$colcode .= "<input id='_setting_btn_submit_id' type='button' value='".$field->title."' onclick='printer_validate()'>";
			$field->db_include = false;
			$show_coltitle = false;
		}
		else if ($field->type=="bool_multiple") {

			if($field->name ==='kiosk_config:kiosk_dine_option') {
				$dtype = array("Pickup", "Dine-In");
				$default = 'Pickup,Dine-In';
			} else if($field->name ==='config:ltg_dine_in_option') {
				$dtype = array("Pickup", "Delivery");
				$default = (int)(isset($field->props) && is_array($field->props) && count($field->props) > 0 && in_array("TRUE", $field->props));
			} else {
				$dtype = array("Pickup", "Delivery", "Dine-In");
				$default = (int)(isset($field->props) && is_array($field->props) && count($field->props) > 0 && in_array("TRUE", $field->props));
			}
			
			if ($field_in_quote_value == "")
			{
				$field_in_quote_value = "$default";
			}
				
			$field_arr = explode(',',$field_in_quote_value);
				
			$colcode .= "<input type='hidden' name='".$field->name."' id='".$field->name."' value=\"".$field_in_quote_value."\">";
			foreach($dtype as $dval)
			{
				$chk_opt = '';
				if (in_array($dval, $field_arr))
				{
					$chk_opt = 'checked';
				}
				if($field->name ==='kiosk_config:kiosk_dine_option' && empty($field_arr))
				{
					$chk_opt = 'checked';
				}
				
				$colcode .= "<input type='checkbox' value='$dval' id='ltg_".$dval."' onclick='getDiningOption(this.value,\"".$field->name."\",this.checked)' $chk_opt>$dval";
			}
				
		}
		else if ($field->type=="bool_pickup_ordertypes")
		{
			$dtype = array("Quick Serve", "Tables", "Tabs");

			//Temporarily commented this code, refer LP-6114.
			/*
			//To check Kiosk .
			$kiosk_status=checkKioskStatus();

			 if ($kiosk_status){
				array_push($dtype, "Kiosk");
			}
			//To check LTG.
			$ltg_status=checkLTGStatus();
			if ($ltg_status){
				array_push($dtype, "LTG");
			} */

			$default = (int)(isset($field->props) && is_array($field->props) && count($field->props) > 0 && in_array("TRUE", $field->props));
			if ($field_in_quote_value == "")
			{
				$field_in_quote_value = "$default";
			}

			$field_arr = explode(',',$field_in_quote_value);

			$colcode .= "<input type='hidden' name='".$field->name."' id='".$field->name."' value=\"".$field_in_quote_value."\">";
			foreach($dtype as $dval)
			{
				$chk_opt = '';
				if (in_array($dval, $field_arr))
				{
					$chk_opt = 'checked';
				}
				$colcode .= "<input type='checkbox' value='$dval' id='print_".$dval."' onclick='getPrintOrdTypeOption(this.value,\"".$field->name."\",this.checked)' $chk_opt>$dval";
			}

		}
		else
		{
			$colcode .= "&nbsp;";
			$field->db_include = false;
		}

		if ($rowcode!="")
		{
			$formcode .= $rowcode;
		}
		else
		{
			$s_helptext		= create_help_text_for_form_element($field);
			$use_valign		= "top";
			$title_margin	= "";

			switch ($field->type)
			{
				case "printers_dynamic_select":
					$title_margin = "6px 0px 0px 0px";
					break;
				case "redirect_button":
				case "select":
				case "time_no_late_night":
					$use_valign = "middle";
					break;
				case "invoice_no_series_textbox":
					$use_valign = "middle";
					break;
				case "invoice_series_valid_date":
					$use_valign = "middle";
					break;
				default:
					break;
			}

			if ($field->description != "")
			{
				$formcode .= "<tr>";
				$formcode .= "<td align='center'  bgcolor='#dddddd' colspan='2'>&nbsp;</td>";
				$formcode .= "</tr>";
				$formcode .= "<tr>";
				$formcode .= "<td align='right' bgcolor='#eeeeee' style='padding-left:32px;' valign='".$use_valign."'>".$s_helptext;
				$formcode .= "<table cellpsacing=0 cellpadding=0><tr><td width='220' align='left' ><table><tr><td align='left'><font style='font-size:10px; color:#777777'><font style='font-size:11px; color:#333333'><b>".$field->title."</b></font> ".$field->description."</font></td></tr></table></td></tr></table>";
				$formcode .= "</td>";
			}
			else
			{
			    $trids = '';
			    $settrid = '';
			    if ($field->name == 'secondary_currency_receipt_label' || $field->name == 'config:secondary_currency_code' ||  $field->name == 'secondary_monitary_symbol' || $field->name == 'secondary_exchange_rates' || $field->name == 'config:accessLevelToEditSecondaryExchangeRates' )
			    {
			        $esc_query = lavu_query("select value from `config` where setting='enable_secondary_currency'");
			        $esc_read  = mysqli_fetch_assoc($esc_query);
			        $esc_value = $esc_read[value];
			        $esc_style = "";
			        if ($esc_value=='0' || $esc_value==''){ $esc_style="display:none"; }
			        $trids = $field->name.'id';
			        $settrid = 'id="'.$trids.'" style="'.$esc_style.'"';
			    }
				if($field->name == 'config:pickup_number_format' || $field->name == 'config:place_order_number_on' || $field->name == 'config:print_pickup_preference' || $field->name == 'config:print_pickup_ordertype'){
					$esc_query = lavu_query("select value from `config` where setting='order_number_for_customer_pick_up'");
			    	$esc_read  = mysqli_fetch_assoc($esc_query);
			    	$esc_value = $esc_read[value];
			    	$esc_style = "";
					if ($esc_value=='disable' )
					{
						 $esc_style="display:none";
					}
					else if($esc_value == 'prompt_for_number' && $field->name == 'config:pickup_number_format')
					{
						$esc_style="display:none";
					}
					else{
						$esc_style = '';
					}
			    	$trids = $field->name.'id';
			    	$settrid = 'id="'.$trids.'" style="'.$esc_style.'"';
				}
				if ($field->name == 'tip_sharing_rule' || $field->name == 'include_server_owes' ||$field->name == 'tip_sharing_period' || $field->name == 'enable_tip_sharing') {
					$trids = $field->name.'id';
			    	$settrid = 'id="'.$trids.'"';
				}

			    $formcode .= "<tr $settrid>";
				$label_cell_id = !empty($field->label_cell_id)?" id='".$field->label_cell_id."'":"";
				$formcode .= "<td".$label_cell_id ." align='right' bgcolor='#eeeeee' style='padding-left:32px;' valign='".$use_valign."'>".$s_helptext;

				if ($show_coltitle)
				{
					if (!empty($title_margin))
					{
						$formcode .= "<div style='margin:".$title_margin."'>".$field->title."</div>";
					}
					else
					{
						if ($field->title == "Tipout Rules") { 
							$field->title = "Tipout Rules <br><br> calculate based on <br><br><br><br><br> Tipout Breakdown&nbsp;<img onclick='openHelpPopup(\"tipOut\");' src='images/btn_tutorials.png' width='18' height='15' style='border:none; margin: 0px 0px -3px 0px; cursor:pointer;'>:"; 
						} else if ($field->title == "Enable Tip Sharing") {
					         $field->title = "Enable Tip Sharing &nbsp;<img onclick='openHelpPopup(\"".$field->help_link."\");' src='images/btn_tutorials.png' width='18' height='15' style='border:none; margin: 0px 0px -3px 0px; cursor:pointer;'>:";
					    } else if ($field->title == "Tip Sharing Rule") {
					        $field->title = "Tip Sharing Rule &nbsp;<img onclick='openHelpPopup(\"".$field->help_link."\");' src='images/btn_tutorials.png' width='18' height='15' style='border:none; margin: 0px 0px -3px 0px; cursor:pointer;'>:";
					    } else if ($field->title == "Include Tip In amount for Tip Sharing in Server Owes House / House Owes Server calculation") {
					        $field->title = "Include Tip In amount for Tip Sharing in Server Owes House / House Owes Server calculation &nbsp;<img onclick='openHelpPopup(\"".$field->help_link."\");' src='images/btn_tutorials.png' width='18' height='15' style='border:none; margin: 0px 0px -3px 0px; cursor:pointer;'>:";
					    } else if ($field->title == "Time period for Tip Sharing") {
					        $field->title = "Time period for Tip Sharing &nbsp;<img onclick='openHelpPopup(\"".$field->help_link."\");' src='images/btn_tutorials.png' width='18' height='15' style='border:none; margin: 0px 0px -3px 0px; cursor:pointer;'>:";
					    } else if ($field->name == "config:offline_cc_transaction") {
					        $field->title = "Allow offline credit card transactions &nbsp;<img onclick='openHelp(\"".$field->help_link."\");' src='images/btn_tutorials.png' width='18' height='15' style='border:none; margin: 0px 0px -3px 0px; cursor:pointer;'>:";
					    }
					    $formcode .= $field->title;
					}
				}
				else
				{
					$formcode .= "&nbsp;";
				}
				$formcode .= "</td>";
			}
			$valign = "";
			if ($field->help_link != "")
			{
				$valign = " valign='middle'";
			}
			$valign = " valign='top'";
			$formcode .= "<td align='left'".$valign." style='padding-right:32px'>";
			$formcode .= $colcode;
			$formcode .= "</td>";
			$formcode .= "</tr>";
		}

		if (substr($field->name, 0, 7) == "config:")
		{
			$field_update_list[] = $field->name;
			$field_update_titles[] = $field->title;

			$actual_name = substr($field->name, 7);

			if ($form_posted)
			{
				if (isset($location_info[$actual_name]))
				{
					$special_query = "UPDATE `config` SET `value` = '".str_replace("'", "''", $field->value)."' WHERE `location` = '".$rowid."' AND `type` = 'location_config_setting' AND `setting` = '".substr($field->name, 7)."'";
				}
				else
				{
					$special_query = "INSERT INTO `config` (`location`, `type`, `setting`, `value`) VALUES ('".$rowid."', 'location_config_setting', '".substr($field->name, 7)."', '".str_replace("'", "''", $field->value)."')";
				}

				switch ($actual_name)
				{
					case "use_lavu_togo":

						$ltg_enabled_after_update = $_POST[$field->name]*1;
						$location_info['use_lavu_togo'] = $ltg_enabled_after_update;
						break;

					case "lavu_togo_name":

						$save_ltg_name = $_POST[$field->name];
						break;

					case "ltg_use_opt_mods":

						$ltg_use_opt_mods = $_POST[$field->name];
						break;

					case "ltg_debug":
					case "ltg_use_central_server_url":

						$location_info[$actual_name] = $_POST[$field->name];
						break;

					default:
						break;
				}
			}
		}
		else if (substr($field->name, 0, 17) == "inventory_config:")
		{
			$field_update_list[] = $field->name;
			$field_update_titles[] = $field->title;

			$actual_name = substr($field->name, 17);

			if ($form_posted)
			{
				if (isset($location_info[$actual_name]))
				{
					$special_query = "UPDATE `inventory_settings` SET `value` = '".str_replace("'", "''", $field->value)."' WHERE `setting` = '".substr($field->name, 17)."'";
				}
				else
				{
					$special_query = "INSERT INTO `inventory_settings` (`setting`, `value`) VALUES ('".substr($field->name, 17)."', '".str_replace("'", "''", $field->value)."')";
				}
			}
		}

		else if (substr($field->name, 0, 13) == "kiosk_config:")
		{
			$field_update_list[] = $field->name;
			$field_update_titles[] = $field->title;

			$actual_name = substr($field->name, 13);

			if ($form_posted)
			{
				if($actual_name!='kiosk_splash_filename')
				{
					$kioskIntegration = explode(':',$field->name);
					$kioskIntegrationName = $kioskIntegration[1];
					if (isset($location_info[$actual_name]))
					{
						if (preg_match("/^kiosk_integration(.*)/i", trim($kioskIntegrationName)) > 0) {
							$special_query = "UPDATE `kiosk_settings` SET `value`=AES_ENCRYPT('".str_replace("'", "''", $field->value)."', '".$field->props['key']."') WHERE `setting` = '".$kioskIntegrationName."'";
						} else {
							$special_query = "UPDATE `kiosk_settings` SET `value` = '".str_replace("'", "''", $field->value)."' WHERE `setting` = '".$kioskIntegrationName."'";
						}
					}
					else
					{
						if (preg_match("/^kiosk_integration(.*)/i", trim($kioskIntegrationName)) > 0) {
							$special_query = "INSERT INTO `kiosk_settings` (`setting`, `value`, `location`) VALUES ('".$kioskIntegrationName."', AES_ENCRYPT('".str_replace("'", "''", $field->value)."', '".$field->props['key']."'), $locationid)";
						} else {
							$special_query = "INSERT INTO `kiosk_settings` (`setting`, `value`, `location`) VALUES ('".$kioskIntegrationName."', '".str_replace("'", "''", $field->value)."', $locationid)";
						}
					}
				}
			}
		}

		else if ($field->db_include && $field->name!="")
		{
		    if($field->name != "enable_cp3_beta") {
		    
    			$field_update_list[] = $field->name;
    			$field_update_titles[] = $field->title;
    
    			if ($insert_keys != "")
    			{
    				$insert_keys .= ",";
    			}
    
    			if ($insert_vals != "")
    			{
    				$insert_vals .= ",";
    			}
    
    			if ($update_code != "")
    			{
    				$update_code .= ",";
    			}
    
    			$row_keycode = "`".str_replace("`", "", $field->name)."`";
    
    			if ($field->encode_field && $field->encode_field!="")
    			{
    				$row_valcode = $field->encode_field . "('".str_replace("'", "''", $field->value)."')";
    			}
    			else
    			{
    				$row_valcode = "'".str_replace("'", "''", $field->value)."'";
    			}
    			if ($field->name === 'tip_sharing_period')
    			{
    				$flag_tip_sharing = 0;
    				if ($_POST['enable_tip_sharing']== 1) {
    					$flag_tip_sharing = 1;
    				}
    				$field_val = "''";
    				if ($_POST['tip_sharing_period'] === 'ByShift' && $flag_tip_sharing == 1)
    				{
    					$field_val = "'".$_POST['tip_sharing_period'].'|'.$_POST['shift_time']."'";
    				}
    				if ($_POST['tip_sharing_period'] === 'Daily' && $flag_tip_sharing == 1)
    				{
    					$day_start_end_time = lavu_query("select day_start_end_time from `locations`");
    					$row = $day_start_end_time->fetch_assoc();
    					$field_val = "'".$_POST['tip_sharing_period'].'|'.$row['day_start_end_time']."'";
    				}
    				if ($_POST['tip_sharing_period'] === 'Weekly' && $flag_tip_sharing == 1) {
    				    $field_val = "'".$_POST['tip_sharing_period'].'|'.$_POST['weeklytip']."'";
    				}
    
    				$insert_keys .= $row_keycode;
    				$insert_vals .= $field_val;
    				$update_code .= $row_keycode . "=" . $field_val;
    			} else if ($field->name == 'tip_sharing_rule' && $_POST['enable_tip_sharing']== 0) {
    			    $field_val = "''";
    			    $insert_keys .= $row_keycode;
    			    $insert_vals .= $field_val;
    			    $update_code .= $row_keycode . "=" . $field_val;
    			} else if ($field->name == 'value10' && isset($_POST['printer_type']) && strtolower($_POST['printer_type']) == 'epson' && $_POST['epson_kds'] == 1) {
    			    $insert_keys .= $row_keycode;
    			    $insert_vals .= '0';
    			    $update_code .= $row_keycode . "=" . '0';
    			} else if ($field->name == "access_white_list" && $form_posted) {
							$selected_modules = $field->value ? explode(",", $field->value) : [];
							$current_user_modules = explode(",", $user_result['access_white_list']);
							$cp4_modules = ['marketplace', 'webgift'];

							foreach ($cp4_modules as $cp4_module) {
								if (in_array($cp4_module, $current_user_modules)) {
									$selected_modules[] = $cp4_module;
								}
							}

							$modules_vals = "'".implode(",", $selected_modules)."'";
							$insert_keys .= $row_keycode;
							$insert_vals .= $modules_vals;
							$update_code .= $row_keycode . "=" . $modules_vals;
					} else {
    			    $insert_keys .= $row_keycode;
    			    $insert_vals .= $row_valcode;
    			    $update_code .= $row_keycode . "=" . $row_valcode;
    			}
		    }
		}
		if ($field->name == "config:fiscal_tax_code")
		{
			$fiscal_tax_code =  $_POST[$field->name];
			$fiscal_code_exist = lavu_query("select * from `config` where setting='GetCustomerInfo' and value2='field10' AND value6='requiredField'");
			if ($fiscal_code_exist->num_rows)
			{
				$special_query_new = "UPDATE `config` set `value`='".$fiscal_tax_code."' where setting='GetCustomerInfo' and value2='field10' AND value6='requiredField'";
			}

		}
		
		if ($field->name == "enable_cp3_beta") {
		    if ($form_posted)
		    {
    		    $resp = array();
    		    $resp['info'] = array('description'	=> "",'error'=> "");
    		    $resp['status'] = "success";
    		    addRemoveModule("extensions.limited_cp_beta", "", $resp, $_POST[$field->name]);
		    }
		}

		if ($special_query!="")
		{
			$special_query_list[] = $special_query;
		}
	}

	$show_form = true;
	if (isset($_POST['posted_dataname']))
	{
		$posted_dataname = $_POST['posted_dataname'];
	}

	if (isset($_POST['posted']) && (!isset($posted_dataname) || $posted_dataname=="" || $posted_dataname != admin_info("dataname")))
	{
		echo "<b>Error: you are no longer logged into the account ".$posted_dataname."</b><br>";
		exit();
	}

	if (isset($_POST['posted']))
	{
		$valid_ext = array( "jpg", "jpeg", "gif", "png", "bmp"); // whitelist of file extensions
		// if upload file exists (company logo)
		if (!empty($_FILES) && $_FILES['logo_img'] && basename($_FILES['logo_img']['name'])!="" && stristr($update_code, "logo_img"))
		{
			if (!isset($update_code))
			{
				$update_code = "";
			}
			upload_company_logo($admin_info, $_FILES, $valid_ext, $update_code);
		}
		else if (!empty($_FILES) && $_FILES['receipt_logo_img'] && basename($_FILES['receipt_logo_img']['name']) != "" && stristr($update_code, 'receipt_logo_img'))
		{
			if (!isset($update_code))
			{
				$update_code = "";
			}
			upload_receipt_logo($admin_info, $_FILES, $valid_ext, $update_code);

		}
		else if (!empty($_FILES) && $_FILES['image'] && basename($_FILES['image']['name']) != "" && stristr($update_code, 'image'))
		{
			if (!isset($update_code))
			{
				$update_code = "";
			}
			upload_form_image($admin_info, $_FILES, $valid_ext, $update_code);
		}
		else if (!empty($_FILES) && $_FILES['kiosk_config:kiosk_splash_filename'] && basename($_FILES['kiosk_config:kiosk_splash_filename']['name'])!="")
		{
			if (!isset($update_code))
			{
				$update_code = "";
			}
			upload_kiosk_image($admin_info, $_FILES, $valid_ext, $update_code);
		}

		if ($tablename=="")
		{
			$show_form = form_submitted();
		}
		else
		{
			//----- flag all location devices to indicate that a change has been made -----//
			//echo "<pre>".print_r($_POST,1);
			global $section;
			global $mode;

			CPActionTracker::track_action($section."/".$mode, 'saved');
			lavu_query("UPDATE `devices` SET `needs_reload` = '1' WHERE `loc_id` = '[1]'", $locationid);

	  /** 2017-07-24, LP-727 modified by david.einerson@lavu.com regarding
		* pizza creator bug where fmod lists were not sync'd from cloud to LLS, nor POS, when a list was changed in the control panel.
		* Added two tables to this array: forced_modifier_lists and forced_modifiers_groups
		* Intended to sync the forced_modifier_lists table from cloud to LLS when changes are made to the pizza fmod lists.
		* The forced_modifier_groups table was added for good measure.
		* See also: ~/Repos/lavu/prod/public_html/admin/cp/areas/edit_forced_modifiers.php
		*/
			$notify_lls_tables = array(
				"config",
				"customer_accounts",
				"emp_classes",
				"forced_modifiers",
				"forced_modifier_lists",
				"forced_modifiers_groups",
				"happyhours",
				"ingredients",
				"lk_event_sequences",
				"lk_event_types",
				"lk_karts",
				"lk_parts",
				"lk_rooms",
				"lk_sales_reps",
				"lk_service_info",
				"lk_service_types",
				"lk_tracks",
				"lk_waivers",
				"locations",
				"meal_periods",
				"meal_period_types",
				"menu_items",
				"messages",
				"modifiers",
				"payment_types",
				"super_groups",
				"tax_profiles",
				"tax_rates",
				"users"
			);

			if (in_array($tablename, $notify_lls_tables))
			{
				schedule_remote_to_local_sync_if_lls($location_info, $locationid, "table updated", $tablename);
				if ($tablename == "locations")
				{
					schedule_remote_to_local_sync_if_lls($location_info, $locationid, "table updated", "config");
				}
			}

			$can_proceed = true;
			//-------------------------------- check conditions for insert/update --------------------//
			if ($tablename == "users")
			{
				$user_query = mlavu_query("select * from `poslavu_MAIN_db`.`customer_accounts` where `username`='[1]'",$_POST['username']);
				if ($user_query->num_rows)
				{
					$user_read = mysqli_fetch_assoc($user_query);
					if ($user_read['dataname']==admin_info("dataname"))
					{
						$act_query = lavu_query("select * from `users` where `username`='[1]' and `id`!='[2]'",$_POST['username'],$rowid);
						if ($act_query->num_rows)
						{
							$can_proceed = false;
							$error_message = "Sorry, that username is already in use";
						}
					}
					else
					{
						$can_proceed = false;
						$error_message = "Sorry, that username is already in use";
					}
				}
				$pin = isset($_POST['PIN']) ? $_POST['PIN'] : '';
				$act_query = lavu_query("select * from `users` where `PIN`='[1]' and `id`!='[2]'",$pin,$rowid);
				if ($act_query->num_rows)
				{
					$can_proceed = false;
					$error_message = "Sorry, that pin number is not available";
				}

				if ($insert_keys!="")
				{
					$insert_keys .= ",";
				}

				if ($insert_vals!="")
				{
					$insert_vals .= ",";
				}

				$row_keycode = "`created_date`";
				$row_valcode = "'".date("Y-m-d H:i:s")."'";
				$insert_keys .= $row_keycode;
				$insert_vals .= $row_valcode;
			}
			else if ($tablename == "lk_events") // insert time stamp based on posted date and time
			{
				$dparts = explode("-", $_POST['date']);
				if (count($dparts) > 2)
				{
					$dyear = $dparts[0];
					$dmonth = $dparts[1];
					$dday = $dparts[2];
					$tmin = ($_POST['time'] % 100);
					$thour = ($_POST['time'] - $tmin) / 100;
					$ts = mktime($thour, $tmin, 0, $dmonth, $dday, $dyear);

					if ($insert_keys != "")
					{
						$insert_keys .= ",";
					}

					if ($insert_vals != "")
					{
						$insert_vals .= ",";
					}

					if ($update_code != "")
					{
						$update_code .= ",";
					}

					$row_keycode = "`ts`";
					$row_valcode = "'$ts'";
					//$insert_keys .= $row_keycode;
					//$insert_vals .= $row_valcode;
					//$update_code .= $row_keycode . "=" . $row_valcode;
				}
			}
			else if ($tablename == "lk_waivers")
			{
				$other_vals = array();
				if (!$rowid)
				{
					$other_vals['version'] = assignNext("version", "lk_waivers", "loc_id", $locationid);
				}
				$other_vals['datetime'] = date("Y-m-d H:i:s");
				$other_vals['saved_by'] = admin_info('fullname');

				foreach ($other_vals as $okey => $oval)
				{
					if ($insert_keys != "")
					{
						$insert_keys .= ",";
					}

					if ($insert_vals != "")
					{
						$insert_vals .= ",";
					}

					if ($update_code != "")
					{
						$update_code .= ",";
					}

					$insert_keys .= "`".addslashes($okey)."`";
					$insert_vals .= "'".addslashes($oval)."'";
					$update_code .= "`".addslashes($okey)."`='".addslashes($oval)."'";
				}
			}
			else if ($tablename == "revenue_centers")
			{
				$revenue_query = lavu_query("select * from `revenue_centers` where `name`='[1]' and `_deleted` = '0' ",$_POST['name']);
				if ($revenue_query->num_rows)
				{
					$can_proceed = false;
					$error_message = "Sorry, that name is already in use";
				}
			}
			else if ($tablename == "discount_types") {
			    $error_message = array();
			    $_POST['calc'] = trim($_POST['calc']);
			    $_POST['loyalty_cost'] = trim($_POST['loyalty_cost']);
			    $_POST['title'] = trim($_POST['title']);
			    $_POST['label'] = trim($_POST['label']);
			    
			    if ($_POST['title'] == "") {
			        $can_proceed = false;
			        $error_message[] = "Please enter a button title";
			    }
			    
			    if ($_POST['label'] == "") {
			        $can_proceed = false;
			        $error_message[] = "Please enter a receipt label";
			    }
			    
			    if (!($_POST['code'] == "p" || $_POST['code'] == "d")) {
			        $can_proceed = false;
			        $error_message[] = "Please select a valid calculation type";
			    }
			    else if(!(preg_match("/^[\d\.]+(\.\d{1,2}){0,1}$/", $_POST['calc']))) {
			        $can_proceed = false;
			        $error_message[] = "Please enter a valid discount";
			    }
			    
			    if ($_POST['code'] == 'p' && !($_POST['calc'] >= 0 && $_POST['calc'] <= 1)) {
			        $can_proceed = false;
			        $error_message[] = "Percentage discount should be between 0.00 and 1.00";
			    }
			    
			    if ($_POST['loyalty_cost'] != "" && !(preg_match("/^\d+(\.\d{1,2}){0,1}$/", $_POST['loyalty_cost']))) {
			        $can_proceed = false;
			        $error_message[] = "Please enter a valid loyalty points";
			    }
			    
			    $error_message = implode('<br>', $error_message);
			}
			if ($can_proceed)
			{
			    if ((!isset($_POST['config:primary_currency_code']))|| ($_POST['config:primary_currency_code'] != $_POST['config:secondary_currency_code']) || $_POST['config:enable_secondary_currency']==0) {
			    if ($rowid)
			    {
					if ($tablename == "emp_classes") {
						$act_query = lavu_query("select * from `emp_classes` where `title`='[1]' and `id` != '[2]' and `_deleted` != 1",$_POST['title'],$rowid);
						if ($act_query->num_rows)
						{
							$can_proceed = false;
							$error = "Sorry, that title is already in use";
						}
					}
					if ($can_proceed) {
						if($tablename == "kiosk_settings"){
							$act_query = lavu_query("select * from $tablename where `setting`='kiosk_splash_filename'");
							if ($act_query->num_rows < 1)
							{
								$query = "INSERT INTO `kiosk_settings` (`setting`, `value`, `location`) VALUES ('kiosk_splash_filename', '$update_code', $locationid)";
							}
							else{
								$query = "UPDATE ".$use_db."`".$tablename."` SET value = '$update_code' WHERE `setting` = 'kiosk_splash_filename'";
							}
						}
						else {
						    $tipout_error = "";
						    if ($tablename == "emp_classes") {
						    date_default_timezone_set('Asia/Kolkata');
						    $today_timestamp = date("Y-m-d")." 00:00:00";
						    $insert_keys = $insert_keys.",`tipout_break` ";
						    $breakdownVal = '';
						    if (count($_POST['myvalues']) > 0) {
						    	$breakdownVal = implode(",", $_POST['myvalues']);
						    }
						    $update_code = $update_code.", `tipout_break` = '".$breakdownVal."'";
						    $order_query = lavu_query("select * from `orders` where `closed`>='[1]' ", $today_timestamp);
						    $order_count = mysqli_num_rows($order_query);
						    if ($order_count > 0) {
						        $can_proceed = false;
						        $tipout_error = "Sales have already been made for today, your change will go into effect at next day.";
						        $tablename = "emp_classes_temp";
						        $insert_vals = $insert_vals.", '".$breakdownVal."'";
						        $time_for_tip_sharing = $_POST['tip_sharing_period'];
						        if ($time_for_tip_sharing == "ByShift" || $time_for_tip_sharing == "Daily") {
						            $tomorrowDate =  date("Y-m-d",strtotime("tomorrow"));
						        } else {
						            $nextday = $_POST['weeklytip'];
						            $date = new DateTime();
						            $date->modify('next '.$nextday);
						            $tomorrowDate = $date->format('Y-m-d');
						        }
						        $emp_query = lavu_query("select * from `emp_classes_temp` where `emp_class_id`='[1]' AND `_deleted` = 0 AND `class_updated_date` = '".$tomorrowDate."' ", $rowid);
						        $emp_count = mysqli_num_rows($emp_query);
						        if ($emp_count == 0) {
						            $insert_keys_emp = $insert_keys.",`emp_class_id`,`class_updated_date`,`_deleted`";
						            $insert_vals_emp = $insert_vals.",'".$rowid."','".$tomorrowDate."',0";
						            $insert_query = "INSERT INTO ".$use_db."`".$tablename."` (".$insert_keys_emp.") VALUES (".$insert_vals_emp.")";
						            lavu_query($insert_query);
						            $update_code ="";
						        } else {
						            $update_code_emp = $update_code.",`class_updated_date`='".$tomorrowDate."' ";
						            $update_query = "UPDATE ".$use_db."`".$tablename."` SET ".$update_code_emp." WHERE `emp_class_id` = '".$rowid."'";
						            lavu_query($update_query);
						            $update_code ="";
						        }
						        
						    }
						  }
							if($tipout_error == '') {
							    $query = "UPDATE ".$use_db."`".$tablename."` SET ".$update_code." WHERE `id` = '".$rowid."'";
							}
						}
					}
					if ($tablename == "locations")
					{
						$query = str_replace(",`loc_info`=''", "", $query);
					}
				}
				else
				{
					if ($tablename == "emp_classes") {
					   $act_query = lavu_query("select * from `emp_classes` where `title`='[1]' and `_deleted` != 1",$_POST['title']);
					   if ($act_query->num_rows)
					   {
					   		$can_proceed = false;
					   		$error = "Sorry, that title is already in use";
					   }
					}
				   if ($can_proceed) {
					   	if ($tablename == "emp_classes") {
					   		$breakdownVal = "";
					   		$insert_keys = $insert_keys.",`tipout_break` ";
					   		if (count($_POST['myvalues']) > 0) {
					   			$breakdownVal = implode(",", $_POST['myvalues']);
					   		}
					   		$insert_vals = $insert_vals.", '".$breakdownVal."'";
					   	}
				   		$query = "INSERT INTO ".$use_db."`".$tablename."` (".$insert_keys.") VALUES (".$insert_vals.")";
				   }
				}
				
				$log_changes = ($tablename=="locations" || $tablename=="users" || $tablename=="config");

				if (admin_info("dataname") != "")
				{
					$change_str = "";
					for ($n = 0; $n < count($field_update_list); $n++)
					{
						$ufield = $field_update_list[$n];
						$ufield_title = $field_update_titles[$n];

						global $section;
						global $mode;

						if (isset($_POST['value_before_'.$ufield]) && isset($_POST[$ufield]))
						{
							$first_val = $_POST['value_before_'.$ufield];
							$second_val = $_POST[$ufield];
							if ($first_val != $second_val)
							{
								CPActionTracker::track_action($section . "/" . $mode, 'updated_' . $ufield);
								$change_str .= "\t<field>\n";
								$change_str .= "\t\t<name>$ufield</name>\n";
								$change_str .= "\t\t<title>$ufield_title</title>\n";
								$change_str .= "\t\t<before>$first_val</before>\n";
								$change_str .= "\t\t<after>$second_val</after>\n";
								$change_str .= "\t</field>\n";
								//$change_str .= "field: $ufield\noriginal value: " . $first_val . "\nset value: " . $second_val . "\n";
							}
						}
					}
					if ($log_changes && $change_str!="")
					{
						$change_row = "";
						$change_row .= "<change>\n";
						$change_row .= "\t<dataname>" . admin_info("dataname") . "</dataname>\n";
						$change_row .= "\t<table>$tablename</table>\n";
						$change_row .= "\t<rowid>$rowid</rowid>\n";
						$change_row .= "\t<accountid>" . admin_info("loggedin") . "</accountid>\n";
						$change_row .= "\t<accountname>" . admin_info("fullname") . "</accountname>\n";

						$log_sess_vars = array("posadmin_loggedin","posadmin_fullname");
						for ($z=0; $z<count($log_sess_vars); $z++)
						{
							$log_varname = trim($log_sess_vars[$z]);
							if (isset($_SESSION[$log_varname]))
							{
								$change_row .= "\t<".$log_varname.">" . $_SESSION[$log_varname] . "</".$log_varname.">\n";
							}
						}

						$change_row .= "\t<datetime>" . date("Y-m-d H:i:s") . "</datetime>\n";
						$change_row .= "\t<ipaddress>" . $_SERVER['REMOTE_ADDR'] ."</ipaddress>\n";
						$change_row .= $change_str;
						$change_row .= "</change>\n";
						//$change_str = "Values changed for $use_db $tablename at " . date("Y-m-d H:i:s") . " by " . admin_info("loggedin") . "\n" . $change_str;
						//echo str_replace("<","&lt;",str_replace("\t","&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;",str_replace("\n","<br>",$change_str)));
						$fp = fopen("/home/poslavu/logs/company/".admin_info("dataname")."/form_change_log_".date("Y-m").".txt","a");
						fwrite($fp,$change_row);
						fclose($fp);
					}
				}

				$show_warning_if_not_passing = true;
				if (isset($insert_direct_to_local_server) && $insert_direct_to_local_server==true)
				{
					$query = str_replace($use_db, "", $query);
					schedule_remote_to_local_sync_if_lls($location_info, $locationid, "run local query", "config", $query);

					echo "Your update will be made to your local server.  It may take up to five minutes to show up here.";
					echo "<br><br><input type='button' value='Continue >>' onclick='window.location = \"$forward_url\"' />";

					$show_form = false;
					$show_warning_if_not_passing = false;
				}
				else
				{
					if ($update_code=="" && $insert_keys=="" && $insert_vals=="")
					{
						// nothing to update in the table
						$success = true;
					}
					else if ($use_db == "`poslavu_MAIN_db`.")
					{
						$success = mlavu_query($query);
					}
					else
					{
						$success = lavu_query($query);
					}

					for ($i = 0; $i < count($special_query_list) && $success; $i++)
					{
						$special_query = $special_query_list[$i];
						if ($use_db=="`poslavu_MAIN_db`." && strpos($special_query, "`config`")===false)
						{
							$success = mlavu_query($special_query);
						}
						else
						{
							$success = lavu_query($special_query);
						}
					}
					if (isset($special_query_new) && $special_query_new!='')
					{
						lavu_query($special_query_new);
					}

				}

				if ($success)
				{
					//----------------------------------- actions after update ----------------------//
					if ($tablename=="users")
					{
						$verify_action = $rowid?"update":"insert";
						verify_user_main($verify_action, admin_info('dataname'), $_POST['username'], $_POST['email']);

						$for_user_id = $rowid?$rowid:lavu_insert_id();
						set_primary_employee_class($for_user_id);
					}
					else if (isset($call_post_action_function) && $call_post_action_function)
					{
						run_post_action();
					}

					if ($tablename=="locations")
					{
						$rest_query = mlavu_query("SELECT * FROM `poslavu_MAIN_db`.`restaurants` WHERE `data_name` = '[1]'", admin_info("dataname"));
						if ($rest_query->num_rows)
						{
							global $locationid;

							$rest_read = mysqli_fetch_assoc($rest_query);
							$loc_query = lavu_query("SELECT * FROM `locations` WHERE `id` = '[1]'", $locationid);
							if ($loc_query->num_rows)
							{
								$loc_read = mysqli_fetch_assoc($loc_query);
								update_restaurant_location_data($rest_read, $loc_read);
							}
						}
					}

					if (($tablename=="restaurants" || $tablename=="locations") && $mode=="lavu_togo" && $form_posted)
					{
						$get_company_info = mlavu_query("SELECT `company_name`, `logo_img` FROM `poslavu_MAIN_db`.`restaurants` WHERE `id` = '[1]'", admin_info("companyid"));
						$cinfo = mysqli_fetch_assoc($get_company_info);
						sendInfoToLavuToGo($save_ltg_name, $locationid, $cinfo['company_name'], $cinfo['logo_img'], $ltg_enabled_after_update, $ltg_use_opt_mods);
					}

					if ($auto_go_to_loyaltree)
					{
						$forward_url .= "&agtlt=1";
					}


					//UPDATE checkout layout editor as changed Allow Tax Exempt Enable
					$curentCheckoutLayout = '';
					$resCheckout = '';
					$isEnable = reqvar("allow_tax_exempt");
					//Change Layout Setting
					if ($isEnable == 0) {
						$removeVal = '[tax_exemption]';
						//Get Current Checkout Layout
						$checkoutLayoutquery =lavu_query("SELECT `value` FROM `config` WHERE `setting`= 'lower_checkout_panel_layout' and `type`='location_config_setting' and `location`='[1]'",$locationid);
						//Check For Row Available
						if(mysqli_num_rows($checkoutLayoutquery)){
						$resCheckout = mysqli_fetch_assoc($checkoutLayoutquery);
						//Current Layout Value
						$curentCheckoutLayout = $resCheckout['value'];
						//Explode By |**|
						$checkoutSplit = explode('|**|', $curentCheckoutLayout);
						//Count For Exploded String
						$countCheckout = count($checkoutSplit);
						if ($countCheckout > 0) {
							$checkoutFirstSplit = explode('|*|', $checkoutSplit[0]);
							$checkoutSecondSplit = explode('|*|', $checkoutSplit[1]);
							$flgChkFirst = 0;
							$flgChkSecond = 0;
							//Check In First Splitted String
							if (($tax_exemption_first = array_search($removeVal, $checkoutFirstSplit)) !== false) {
							    unset($checkoutFirstSplit[$tax_exemption_first]);
							    $checkoutFirstSplit = array_values($checkoutFirstSplit);
							    $flgChkFirst++;
							}
							//Check In Second Splitted String
							if (($tax_exemption_second = array_search($removeVal, $checkoutSecondSplit)) !== false) {
							    unset($checkoutSecondSplit[$tax_exemption_second]);
							    $checkoutSecondSplit = array_values($checkoutSecondSplit);
							    $flgChkSecond++;
							}
							$finalCheckoutStr = '';
							$checkoutLayoutStr = '';
							if ($flgChkFirst > 0) {
								$countFirstSplit = count($checkoutFirstSplit);
								$conStrFirst = '';
								for ($chkf=0; $chkf < $countFirstSplit; $chkf++) {
									if ($chkf > 0) {
										$conStrFirst = '|*|';
									}
									$finalCheckoutStr .= $conStrFirst.$checkoutFirstSplit[$chkf];
								}
								$checkoutLayoutStr = $finalCheckoutStr.'|**|'.$checkoutSplit[1];
							}
							if ($flgChkSecond > 0) {
								$countSecondSplit = count($checkoutSecondSplit);
								$conStrSecond = '';
								for ($chks=0; $chks < $countSecondSplit; $chks++) {
									if ($chks > 0) {
										$conStrSecond = '|*|';
									}
									$finalCheckoutStr .= $conStrSecond.$checkoutSecondSplit[$chks];
								}
								$checkoutLayoutStr = $checkoutSplit[0].'|**|'.$finalCheckoutStr;
							}
							if ($checkoutLayoutStr != '') {
								$checkoutLayoutquery =lavu_query("UPDATE `config` set value = '$checkoutLayoutStr' WHERE `setting`= 'lower_checkout_panel_layout' and `type`='location_config_setting' and `location`='[1]'", $locationid);
							}
						}
						}
					}
					echo "<script language='javascript'>";
					echo "window.location.replace('$forward_url'); ";
					echo "</script>";
					exit();
				}
				else if ($show_warning_if_not_passing)
				{
				    if ($tipout_error=="") {
				        echo "<b>Warning: There was a problem updating the database: ".$error."</b>";
				    } else {
				        echo "<b>".$tipout_error."</b>";
				    }
				}
			}else{
			    echo "<b>Primary currency code and Secondary currency code can't be same</b>";
			}
			}
			else
			{
				echo "<br><b>".$error_message."</b><br>";
			}
		}
	}

	if ($show_form)
	{

?>
		<script language='javascript'>

			function checkDatePicker(elem){
				var keyCode = (event.keyCode ? event.keyCode : event.which);
			    if (keyCode > 47 && keyCode < 58 || keyCode > 95 && keyCode < 107 ){
			        event.preventDefault();
			    }
			}	

			 function isNumberKey(evt)
		      {
		         var charCode = (evt.which) ? evt.which : event.keyCode
		         if (charCode > 31 && (charCode < 48 || charCode > 57))
		            return false;

		         return true;
		      }	

			function hide_tips(newEle) {
				var id1 = $(newEle).attr('id');
				$('#'+id1).closest('tr').next('tr').hide();
				$('#'+id1).closest('tr').next('tr').next('tr').show();
				var lastChar = id1.substr(id1.length - 1);
				var rowid = "supergrouprows_tipout_rules_item"+lastChar;
				$('.'+rowid).hide();
			}
			function validateNumberNutrition(evnt) {
                var iKeyCode = evnt.which || evnt.keyCode;
                if (iKeyCode == 45 || iKeyCode == 34 || iKeyCode == 35 || iKeyCode == 39 || iKeyCode == 33 || iKeyCode == 44 || iKeyCode == 94 || iKeyCode == 42) {
                    return false;
                }
            }
			function show_tips(newEle) {
				var id1 = $(newEle).attr('id');
				$('#'+id1).closest('tr').next('tr').show();
				$('#'+id1).closest('tr').next('tr').next('tr').hide();
				var lastChar = id1.substr(id1.length - 1);
				var rowid = "supergrouprows_tipout_rules_item"+lastChar;
				$('.'+rowid).hide();
			}
			function show_supergroups(eleObj) {
				var id1 = $(eleObj).attr('id');
				var lastChar = id1.substr(id1.length - 1);
				var rowid = "supergrouprows_tipout_rules_item"+lastChar;
				$('.'+rowid).show();
			}
			function hide_supergroups(eleObj) {
				var id1 = $(eleObj).attr('id');
				var lastChar = id1.substr(id1.length - 1);
				var rowid = "supergrouprows_tipout_rules_item"+lastChar;
				$('.'+rowid).hide();
			}
			function validatePrimary(eleObj) {
				var id1 = $(eleObj).attr('id');
				var lastChar = id1.substr(id1.length - 1);
				var rowid = "primary_tipout_rules_item"+lastChar;
				if ($('#'+rowid).val()=='' || $('#'+rowid).val()== null ) {
					alert('Please select primary employee class');
					$(eleObj).val(0);
					return false;
				}
			}
			function checkUserExistInClass(val, itemInfo, type) {
				var selectId = itemInfo.id;
				var itemnum = selectId.charAt(selectId.length - 1);
				var tipOutVal = $('#tipout_rules').val();
				var splitVal = tipOutVal.split(',');
				var splitRow = '';
				if (splitVal[itemnum]) {
					splitRow = splitVal[itemnum].split('|');
				}
				var opt = 0;
				if (type == 'primary') {
					opt = 2
				}
				$.ajax({
					async: false,
					type: "POST",
					url: "areas/checkUserExistEmpClass.php",
					data: {"empClsId" : val, "dataname" : "<?= $data_name ?>", "type" : "single" },
					success: function (response) {
						if (response == 0 && val != '') {	
							alert("The selected Employee Class does not have any user. Please select other.");
							if (tipOutVal == '') {
								$('#'+ selectId +' option[value=""]').attr('selected','selected');
							} else {
								if (splitRow == '') {
									$('#'+ selectId +' option[value=""]').attr('selected','selected');
								} else {
									$('#'+ selectId +' option[value='+ splitRow[opt] +']').attr('selected','selected');
								}
							}
						}
					}
				});
			}
			
			function getContentWidthOfElement(element_id) {

				alert(element_id + " ... " + document.getElementById(element_id).offsetWidth);

				return document.getElementById(element_id).offsetWidth;
			}

			function getDiningOption(option,hidval,type)
			{
				
				var hid_ltg = document.getElementById(hidval);
				
				if(hidval==='kiosk_config:kiosk_dine_option')
				{
					if(type===false)
					{
						var split_val = hid_ltg.value.split(',');
						if(split_val.length === 1)
						{
							document.getElementById('ltg_'+hid_ltg.value).checked = true;
							return false;
						}
					}
				}
				if(hid_ltg.value == '0' || hid_ltg.value == '')
				{
					if(type === true)
					{
						hid_ltg.value = option;
					}
				}
				else{
					if(type === true)
					{
						hid_ltg.value += ','+option;
					}
					else
					{
						var split_str = hid_ltg.value.split(',');
						if(split_str.length>1)
						{
							split_str.toString();
							remove(split_str, option);
							hid_ltg.value = split_str.join(",");
						}
						else{
							hid_ltg.value = '0';
						}
					}
				}
				
				
			}

			//START for norway_VAT_ID show/hide complete row
			$(document).ready(function () {
				disabled_chkbox();
				if ($("input[name^='config:manage_item_availability']").val() == "0") {
					$("select[name^='config:access_level_manage_item_availability']").closest('tr').hide();
				}
				if ($("input[name^='config:norway_mapping']").val()=="0") {
					$("#norway_vat_id, textarea[name^='config:norway_private_key'], textarea[name^='config:norway_public_key']").closest('tr').hide();
				}
				if ($("input[name^='config:enable_fiscal_receipt_templates']").val() == "0") {
					$("select[name^='config:fiscal_receipt_template']").closest('tr').hide();
				}
				if (document.getElementById('outer_modal_span_template')) {
					document.getElementById('outer_modal_span_template').style.display = 'none';
				}
				if ($("select[name^='config:fiscal_receipt_template']").val() > 0) {
					document.getElementById('outer_modal_span_template').style.display = 'inline';
                }
				if ($("input[name^='config:enable_fiscal_invoice']").val() == "0") {
					$("input[name^='config:invoice_no_series']").closest('tr').hide();
					$("input[name^='invoice_no_prefix']").closest('tr').hide();
					$("input[name^='invoice_no_postfix']").closest('tr').hide();
					$("input[name^='invoice_series_valid_till']").closest('tr').hide();
				}
				// LP-8736 Setting to enable offline transactions when a merchant goes offline
				// In CC Integration when the user's gateway is set to PayPal, there would be a
				// setting called "Allow offline credit card transactions
				if ($("input[name^='value_before_gateway']").val() != "PayPal") {
					$("input[name^='value_before_config:offline_cc_transaction']").closest('tr').hide();
				}
				$('#gateway').change(function() {
					if ($(this).val() == "PayPal") {
						$("input[name^='config:offline_cc_transaction']").closest('tr').show();
					} else {
						$("input[name^='config:offline_cc_transaction']").val('0');
						$("input[name^='config:offline_cc_transaction']").closest('tr').hide();
					}
				});
			});

			function getNorwayConfigFields() {
				var chkboxvalue = '0';
				chkboxvalue = $("input[name^='config:norway_mapping']").val();
				if (chkboxvalue == "1") {
					$("#norway_vat_id, textarea[name^='config:norway_private_key'], textarea[name^='config:norway_public_key']").closest('tr').show();
				} else {
					$("#norway_vat_id, textarea[name^='config:norway_private_key'], textarea[name^='config:norway_public_key']").closest('tr').hide();
				}
			}
			//END
			//This function is used show/hide 'Access level required to manage item availability' field in Advance location settings.
			function getMenuItemAccesslevelField() {
				var chkboxvalue = '0';
				chkboxvalue = $("input[name^='config:manage_item_availability']").val();
				if (chkboxvalue == "1") {
					$("select[name^='config:access_level_manage_item_availability']").closest('tr').show();
				} else {
					$("select[name^='config:access_level_manage_item_availability']").closest('tr').hide();
				}
			}

			function disabled_chkbox(){
					var chkboxvalue = '0';
					chkboxvalue = $("input[name^='config:show_hold_and_fire']").val();

					if (chkboxvalue == "1") {
						 $("input[name^='auto_send_at_checkout']").val('0');
						 $("input[name^='auto_send_at_checkout']").closest('tr').hide();
						 $("input[name^='config:display_course_icon']").val('0');
						 $("input[name^='config:display_course_icon']").closest('tr').hide();
					} else {
					$("input[name^='auto_send_at_checkout']").closest('tr').show();
					$("input[name^='config:display_course_icon']").closest('tr').show();
					}
				   
			}
			function getFiscalFields(fiscalStatus){
				if (fiscalStatus) {
					$("select[name^='config:fiscal_receipt_template']").closest('tr').show();
				} else {
					$("select[name^='config:fiscal_receipt_template']").closest('tr').hide();
				}
			}
			function enablefiscalinvoice(invoiceStatus){
				if (invoiceStatus) {
					$("input[name^='config:invoice_no_series']").closest('tr').show();
					$("input[name^='invoice_no_prefix']").closest('tr').show();
					$("input[name^='invoice_no_postfix']").closest('tr').show();
					$("input[name^='invoice_series_valid_till']").closest('tr').show();
				} else {
					$("input[name^='config:invoice_no_series']").closest('tr').hide();
					$("input[name^='invoice_no_prefix']").closest('tr').hide();
					$("input[name^='invoice_no_postfix']").closest('tr').hide();
					$("input[name^='invoice_series_valid_till']").closest('tr').hide();
				}
			}
			//LP-5990
			function getPrintOrdTypeOption(option,hidval,type){
				var hid_print = document.getElementById(hidval);
				if (hid_print.value == '0' || hid_print.value == '')
				{
					if (type === true)
					{
						hid_print.value = option;
					}
				}
				else {
					if (type === true)
					{ 
						hid_print.value += ','+option;
					}
					else
					{ 
						var split_str = hid_print.value.split(',');
						if (split_str.length>1)
						{
							split_str.toString();
							remove(split_str, option);
							hid_print.value = split_str.join(",");
						}
						else {
							hid_print.value = '0';
						}
					}
				}
			}
			
			function remove(array, element) {
				const index = array.indexOf(element);
				array.splice(index, 1);
			}

			function change_character_image_setting() {
				var v7 = document.form1.querySelector("input[name^='value7']");
				var v8 = document.form1.querySelector("input[name^='value8']");
				var v10 = document.form1.querySelector("select[name='value10']");
				var v7_before = document.form1.querySelector("input[name^='value_before_value7']");
				var v8_before = document.form1.querySelector("input[name^='value_before_value8']");
				var v10_before = document.form1.querySelector("input[name='value_before_value10']");

    				if ( document.getElementById("printertype").value == 'Epson' && document.getElementById("epson_kds") && document.getElementById("epson_kds").checked == true) {
    					v7.value = 56;
    					v8.value = 42;
    					v10.selectedIndex = 0;
    					v7.setAttribute("readonly", true);
    					v8.setAttribute("readonly", true);
    					v10.setAttribute("disabled", true);
    				}
    				else {
    					v7.value = v7_before.value;
    					v8.value = v8_before.value;
    					for (var i = 0; i < v10.options.length; i++) {
    						if(v10.options[i].value == v10_before.value) {
    							v10.selectedIndex = i;
    							break;
    						}
    					}
    					v7.removeAttribute("readonly");
    					v8.removeAttribute("readonly");
    					v10.removeAttribute("disabled");
    				}
			}
			/*
			* Functiuon added by Ravi T for Lavu to go 2.0 form validation on 10th July 2019
			* This function will validate the required fields of LTG 2.0 setting form and submit the form
			* Ticket LP-9927 
			*/
			function lavutogo_validate(){
				var deliveryFees = $('[name="config:ltg_delivery_fee"]').val();
				var maxDeliveyDistance = $('[name="config:ltg_max_distance_miles"]').val();
				var checkoutMessage = $('[name="config:ltg_on_checkout_success_message"]').val();
				var minOrderAmount = $('[name="config:ltg_minimum_delivery_amount"]').val();
				var regexToCheckOnlyAlpha = /^[a-zA-Z\s.,0-9]*$/;
				var regexpatter = /^\s*(?=.*[0-9])\d*(?:\.\d{1,9})?\s*$/;
				var regexpatterDistance = /^\s*(?=.*[1-9])\d*(?:\.\d{1,9})?\s*$/;
				var validateField = (fieldName) => { 
					return isNaN(fieldName) || fieldName < '0' || fieldName === '-0';			
				}
				if (isNaN(deliveryFees) || deliveryFees == '' || deliveryFees < 0 || !regexpatter.test(deliveryFees)) {
					alert('Default Delivery fee cannot be character, symbol or numbers less then zero');
					return false;
				}
				else if (isNaN(maxDeliveyDistance) || maxDeliveyDistance == '' || maxDeliveyDistance < 0 || !regexpatterDistance.test(maxDeliveyDistance)) {
					alert('Maximum Delivery Distance in Miles cannot be character,null or zero');
					return false;
				}
				else if (isNaN(minOrderAmount) || minOrderAmount == '' || minOrderAmount < 0 || !regexpatter.test(minOrderAmount)) {
					alert('Minimum Order Amount cannot be character, symbol or numbers less then zero');
					return false;
				}
				else if (checkoutMessage.replace(/\s/g,'').length < 1 && regexToCheckOnlyAlpha.test(checkoutMessage)) {
					alert('Please enter a valid checkout success message.');
					return false;
				}
			}
			function printer_validate()
			{
				if(document.getElementsByName("value2")[0].value=='')
				{
					alert("Printer name required");
					return false;
				}
				else if(document.getElementsByName("value3")[0].value=='')
				{
					alert("IP address required");
					return false;
				}
				else if(document.getElementsByName("printer_type")[0].value=='')
		        	{
		                	alert("Printer type required");
		                	return false;
		        	}
				else if(document.getElementsByName("printer_type")[0].value!='')
		        	{
					var commandPrinter = 'printer_command_'+document.getElementsByName("printer_type")[0].value;
					var model = 'model_'+document.getElementsByName("printer_type")[0].value;
					if(document.getElementById(commandPrinter)!=null && document.getElementById(commandPrinter).value=='')
					{
						alert("Printer Command required");
						return false;
					}
					else if(document.getElementById(model)!=null && document.getElementById(model).value=='')
					{
						alert("Model required");
						return false;
					}
					else
					{
						form1_validate();
					}
		        	}
				else
				{
					form1_validate();
				}
			}

			function showOfflineCCToolTips(val) {
				if (val) {
					openHelp("offlineCCTransaction");
				}
			}
	</script>
<?php
		// include the standard javascript
		echo form_javascript_tostring($js_init, $js_validate);
		echo "<form id='_setting_form_id' name='form1' method='post' action='' enctype='multipart/form-data'height:100%>";
		if (!isset($data_name))
		{
			$data_name = sessvar("admin_dataname");
		}

		$posted_dataname_type = "hidden";
		if (isset($_GET['show_dataname']))
		{
			$posted_dataname_type = "text";
		}
		echo "<input type='".$posted_dataname_type."' name='posted_dataname' value='".$data_name."'>";
		echo "<input type='hidden' name='request_mode' id='request_mode' value='".$_REQUEST['mode']."'>";

		if (isset($back_to))
		{
			echo "<a href='".$back_to."'>".$back_to_title."</a><br><br>";
		}

		echo "<input type='hidden' name='posted' value='1'>";
		echo "<table id='_setting_tbl_id' style='border:solid 2px #aaaaaa;".$width_restraint."' class='tableSpacing'>";
		echo $formcode;
		echo "</table>";
		echo "</form>";

		if ($rowid && $tablename=="lk_customers")
		{
			if (!function_exists("schedule_msync"))
			{
				function schedule_msync($dataname, $locationid, $sync_setting, $sync_value="", $sync_value_long="", $sync_value2="")
				{
					$exist_query = mlavu_query("SELECT * FROM `poslavu_[1]_db`.`config` WHERE `setting` = '[2]' AND `location` = '[3]' AND `value` = '[4]' AND `value_long` = '[5]' AND `value2` = '[6]'", $dataname, $sync_setting, $locationid, $sync_value, $sync_value_long, $sync_value2);
					if ($exist_query->num_rows)
					{
					}
					else
					{
						mlavu_query("insert into `poslavu_[1]_db`.`config` (`setting`, `location`, `value`, `value_long`, `value2`, `type`) values('[2]', '[3]', '[4]', '[5]', '[6]', 'log')", $dataname, $sync_setting, $locationid, $sync_value, $sync_value_long, $sync_value2);
					}
				}
			}

			$waiver_sig_name = "signature_".$rowid.".jpg";
			$waiver_sig_relative = "/components/lavukart/companies/".$data_name."/signatures/".$waiver_sig_name;
			$waiver_sig_root = "/home/poslavu/public_html/admin".$waiver_sig_relative;

			if (isset($_GET['get_signature']))
			{
				//echo "Waiver Retrieval Scheduled: " . $waiver_sig_name . "<br>If waiver exists for this customer it will show up in 2-5 minutes";
				schedule_msync($data_name, $locationid, "upload waiver", $waiver_sig_name);
				echo "<script language='javascript'>";
				echo "window.location.replace('index.php?mode=kart_edit_customers&rowid=".$rowid."'); ";
				echo "</script>";
			}

			if (is_file($waiver_sig_root))
			{
				echo "<table width='700' cellpadding=12 style='border:solid 1px #999999'><tr><td align='center'>";
				echo "<a href='index.php?widget=components/lavukart/waivers&view_waiver=".$rowid."' target='_blank'><img src='".$waiver_sig_relative."' border='0' width='400' /></a>";
				echo "</td></tr></table>";
			}
			else
			{
				$sched_query = lavu_query("SELECT * FROM `config` WHERE `setting` = '[1]' AND `location` = '[2]' AND `value` = '[3]'", "upload waiver", $locationid, $waiver_sig_name);
				if ($sched_query->num_rows)
				{
					echo "Signature Retrieval Scheduled - ".$waiver_sig_name;
				}
				else
				{
					echo "<a style='cursor:pointer' href='index.php?mode=".$section."_".$mode."&rowid=".$rowid."&get_signature=1'>Check for Waiver Signature</a>";
				}
			}
		}
	}
$requestMode = isset($_REQUEST['mode']) ? trim($_REQUEST['mode']) : "";

//add request mode for the page we need to add
$arrModes = array('settings_advanced_location_settings','settings_credit_card_integration');
if ( in_array($requestMode, $arrModes) )
{
	/*
	 <script language="javascript">
	$(document).ready(function() {
			jQuery('#mainTable #_setting_form_id #_setting_tbl_id tr').live("click", function(e) {
			e = e ? e : event;
			var source = e.target || e.srcElement;
			thisType = source.type;
			if (thisType == 'select-one' || thisType == 'text' || thisType == 'checkbox' || thisType == 'radio' || thisType == 'textarea') {
				$("._dyn_input_btn").remove();
				var html = "<input class='_dyn_input_btn' style='float:right;' type='button' value='Save All' onclick='form1_validate();'>";
				$(this).find('td:last').append(html);
			}
		});

		jQuery('#mainTable #_setting_form_id').live("mouseleave", function(e) {
			$("._dyn_input_btn").remove();
		});

	});
	</script>
	 */
	$scriptCode = <<<SCRIPTCODE
	<!--  Code below are for submit button to be displayed along with the edited element-->
	<style type="text/css">
		.content {
			padding-bottom: 0px !important;
		}
		#_setting_btn_submit_id {
			height: 30px;
			width: 120px;
			z-index: 100;
			position: fixed;
			bottom: 0;
			right: 200px;
		}
	</style>
SCRIPTCODE;

echo $scriptCode;
}

function set_primary_employee_class($for_user_id) {

		$get_role_id = lavu_query("SELECT `role_id` FROM `users` WHERE `id` = '[1]'", $for_user_id);
		if (mysqli_num_rows($get_role_id) > 0) {
			$rinfo = mysqli_fetch_assoc($get_role_id);
			$roles = explode(",", $rinfo['role_id']);
			$primary_role = explode("|", $roles[0]);
			if (!empty($primary_role[0])) $set_employee_class = lavu_query("UPDATE `users` SET `employee_class` = '[1]' WHERE `id` = '[2]'", $primary_role[0], $for_user_id);
		}
	}

//Temporarily commented this code, refer LP-6114.
/*
 * function checkLTGStatus()
{
	$status=0;
	$ltg_query = lavu_query("SELECT value FROM `config` WHERE `setting` = 'use_lavu_togo' AND `location` = '1'");
	if (mysqli_num_rows($ltg_query) > 0)
	{
		$settingInfo = mysqli_fetch_assoc($ltg_query);
		$status = $settingInfo['value'];
	}
	return $status;
}

function checkKioskStatus()
{
	$status = 0;
	$koisk_query = lavu_query("SELECT value FROM `config` WHERE `setting` = 'modules' AND `location` = '1' and `value` like '%extensions.ordering.+kiosk%'");
	if (mysqli_num_rows($koisk_query) > 0) {
		$status = 1;
	}
	return $status;
} */
?>
