<?php session_start();
require_once("lavuquery.php");
require_once("core_functions.php");
$rdb = admin_info("database");
$location_info = sessvar('location_info');
$disable_decimal = $location_info['disable_decimal'];
$decimal_char = $location_info['decimal_char'];
$precision = isset ($disable_decimal) ? $disable_decimal : 2;
if ($decimal_char != "") {
	$decimal_char = isset ( $decimal_char ) ? $decimal_char : '.';
	$thousands_char = (isset ( $decimal_char ) && $decimal_char == ".") ? "," : '.';
}
$thousands_sep = ",";
if (! empty ( $thousands_char )) {
	$thousands_sep = $thousands_char;
}

$array = $_POST;
if(!empty($array))
{
	foreach($array as $k => $v)
	{
	 if (strpos($k, '_price') !== false)
	 {
	  if($v==''){
	  $item_id=str_replace(price,selmenu,$k);
	  $item_id=$array[$item_id];
	  $group_query = mlavu_query("select price from `$rdb`.`menu_items` where `id`=$item_id");
	  $group_read = mysqli_fetch_assoc($group_query);
	  $mod_price = number_format ($group_read[price], $precision, $decimal_char, $thousands_sep);
	  $data[$k]=$mod_price;
	  }else{
	   $data[$k]=$array[$k];
	  }
	  
	 }
	}
	if(!empty($data))
	{
		echo json_encode($data);
	}
	else 
	{
		echo 0;
	}
}
else
{
	echo 0;
}
?>