<?php
if (!isset($_SESSION)) {
	session_start();
}

require_once($_SERVER['DOCUMENT_ROOT']."/lib/jcvs/FiscalReceiptInvoiceApi.php");

$locationSessArr=sessvar('location_info');
$locTime = strtotime(localize_datetime(date('Y-m-d H:i:s'), $locationSessArr['timezone']));
$dataname  = sessvar("admin_dataname");
$data=array(
	'action'=>'allowinvoiceupdate',
	'loc_time' => $locTime,
	);
$obj= new FiscalReceiptInvoiceApi($dataname,$data);
$res=$obj->doProcess();
echo $res;
?>
