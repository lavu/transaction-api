<?php 
/**
 * Format of the input data expected:
 * <object> {
 *     background : <string>,
 *     lines : <array> [
 *         <object>+ {
 *             text : <string>,
 *             font-size : <integer>,
 *             coords : <object> {
 *                 top : <integer>,
 *                 left : <integer>
 *             }
 *         }
 *     ],
 *     first-bold : <boolean>,
 *     debug : <boolean>
 * }
 */

function imageCreateCorners($src, $radius) {

      $w = 354;
      $h = 275;
     
    # create corners

      $q = 2; # change this if you want
      $radius *= $q;

      # find unique color
      do {
        $r = rand(0, 255);
        $g = rand(0, 255);
        $b = rand(0, 255);
        }
      while (imagecolorexact($src, $r, $g, $b) < 0);

      $nw = $w*$q;
      $nh = $h*$q;

      $img = imagecreatetruecolor($nw, $nh);
      $alphacolor = imagecolorallocatealpha($img, $r, $g, $b, 127);
      imagealphablending($img, false);
      imagesavealpha($img, true);
      imagefilledrectangle($img, 0, 0, $nw, $nh, $alphacolor);

      imagefill($img, 0, 0, $alphacolor);
      imagecopyresampled($img, $src, 0, 0, 0, 0, $nw, $nh, $w, $h);

      imagearc($img, $radius-1, $radius-1, $radius*2, $radius*2, 180, 270, $alphacolor);
      imagefilltoborder($img, 0, 0, $alphacolor, $alphacolor);
      imagearc($img, $nw-$radius, $radius-1, $radius*2, $radius*2, 270, 0, $alphacolor);
      imagefilltoborder($img, $nw-1, 0, $alphacolor, $alphacolor);
      imagearc($img, $radius-1, $nh-$radius, $radius*2, $radius*2, 90, 180, $alphacolor);
      imagefilltoborder($img, 0, $nh-1, $alphacolor, $alphacolor);
      imagearc($img, $nw-$radius, $nh-$radius, $radius*2, $radius*2, 0, 90, $alphacolor);
      imagefilltoborder($img, $nw-1, $nh-1, $alphacolor, $alphacolor);
      imagealphablending($img, true);
      imagecolortransparent($img, $alphacolor);

      # resize image down
      $dest = imagecreatetruecolor($w, $h);
      imagealphablending($dest, false);
      imagesavealpha($dest, true);
      imagefilledrectangle($dest, 0, 0, $w, $h, $alphacolor);
      imagecopyresampled($dest, $img, 0, 0, 0, 0, $w, $h, $nw, $nh);

      # output image
      $res = $dest;
      imagedestroy($src);
      imagedestroy($img);

    return $res;
}



$img_width  = 354;
$img_height = 275;
//var_dump ( trim( $_REQUEST["text"][0] ) ) ;
$debugging = ( $_GET['debug']  == "true" )? true :  false;
$lines = $_REQUEST["lines"];
// echo "<pre>";
// print_r($lines);
// echo "</pre><br />";
$img = ( $debugging )?
       imagecreate( $img_width, 460 ) :
       imagecreate( $img_width, $img_height );
$background = $_GET["background"];
$bg_red = hexdec( substr( $_GET["background"], 0, 2 ) );
$bg_green = hexdec( substr( $_GET["background"], 2, 2 ) );
$bg_blue = hexdec( substr ($_GET["background"], 4 ) ); 
$background_color = imagecolorallocate( $img, $bg_red, $bg_green, $bg_blue );
$text_color = null;
if ( $background == "ffffff" || $background == "ffff55" || $background == "00ced1" ) {
	$text_color = imagecolorallocate( $img, 0, 0, 0 );
} else {
	$text_color = imagecolorallocate( $img, 255, 255, 255 );
}
$first_line_bold = ( $_REQUEST["first-bold"] == "true")? true : false;
$black = imagecolorallocate ( $img, 0, 0, 0 );
$normal_font = 'fonts/muli-light/muli-light-webfont.ttf';
$bold_font = 'fonts/muli-regular/muli-regular-webfont.ttf';
if ( isset( $_GET["picmode"] ) && $_GET["picmode"] === "finish_thumbnail" ) {
// if this is the case, the script is bring included from form_dialog.php
// therefore the paths to the font files have to be adjusted
    $normal_font = "thumbnail-generator/$normal_font";
    $bold_font = "thumbnail-generator/$bold_font";
}
$num_lines = count ( $lines );
$padding_height = $img_height / ( $num_lines + 1 );
if ( $debugging ) {
	imagefilledrectangle($img, 0, 310, 459, 459, $text_color);
}
foreach( $lines as $index => $line) {
	$font = ($index == 0 && $first_line_bold )? $bold_font : $normal_font;
	$box = imagettfbbox( 28, 0, $font, trim( $line["text"] ) );
	$box_width = $box[2] - $box[0];
	$box_height = $box[1] - $box[7];
// 	$x_coord = ( $img_width - $box_width) / 2;
    $x_coord = $line["coords"]["left"];
//     echo "$x_coord<br />";
// 	$y_coord = $padding_height * ( $index + 1 ) + $box_height / 2;
    $y_coord = $line["coords"]["top"]  + $line["font-size"] - 10;
//     echo "$y_coord<br />";
    $text = trim( $line["text"] );
    $fontsize = $line["font-size"] * 0.75;
	imagettftext( $img, $fontsize, 0, $x_coord, $y_coord, $text_color, $font, $text );
	if ( $debugging ) {
		$i = $index + 1;
		$txt = "line $i : x, y = ( $x_coord, $y_coord) w = $box_width  h = $box_height";
		imagettftext( $img, 10, 0, 5,  330 + 20 * $index, $black, $font, $txt );
	}
}


// Uncomment this line below if you don't want rounded corners
$img = imageCreateCorners( $img, 18 );
//
if ( isset( $_GET["picmode"] ) && $_GET["picmode"] === "finish_thumbnail" ) {
    //$debug  = print_r( $_GET, true );
    $create_image_folder = $_SERVER['DOCUMENT_ROOT'] . $_GET['dir'];
	$filename = str_replace ( ' ', '-', strtolower( $_GET['item-name'] ) );
	$filename .= "-" . $_GET['updateid'];
	$filename = str_replace( '_', '-', $filename );
	$filename .= "-thumbnail.png";
	$full_path = "$create_image_folder$filename";
	$full_path = str_replace( '//', '/', $full_path );
	//echo "\n$full_path\n";
	//echo "\n$normal_font\n";
	//echo "\n$bold_font\n";
	if ( file_exists( $full_path ) ) {
	    $old = getcwd(); // Save the current directory
        chdir( $create_image_folder );
        $b = unlink( $filename );
        chdir( $old ); // Restore the old working directory 
        if ( !$b ) echo "<script>alert('file could not be deleted')</script>\n";
	}
	$GLOBALS["new_filename"] = $filename;
    $GLOBALS["thumbnail-success"] = imagepng( $img, $full_path );
} else {
	// Send it to the browser
    header( "Expires: Mon, 26 Jul 1997 05:00:00 GMT" );
    header( "Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT" );
    header( "Cache-Control: no-store, no-cache, must-revalidate" );
    header( "Cache-Control: post-check=0, pre-check=0", false );	header( "Pragma: no-cache" );
    header( "Content-type: image/png" );
    imagepng( $img );
}
imagedestroy( $img );
?>