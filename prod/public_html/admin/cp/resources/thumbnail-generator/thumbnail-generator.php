<?php $no_cache = $_SERVER['REQUEST_TIME'];?>
<link rel="stylesheet" href="thumbnail-generator/style.css?<?= $no_cache;?>" media="screen" />

<div id="thumbnail-prototype">
		<div id="steps"> 
		    <div class="window-header" id="step1-header">
		        <h2>Color</h2>
		        <p>Choose background color:</p>
		    </div>
		    <hr class="header-separator" />
			<h2 id="step2b">
			    Step 1 of 4
			    <span class="step-indicator current">&#8226;</span>
			    <span class="step-indicator">&#8226;</span>
			    <span class="step-indicator">&#8226;</span>
			    <span class="step-indicator">&#8226;</span>
			</h2>

			    <input type="hidden" id="selected-color" value="#000000" />

				<div class="step-container">
					<table id="color-table">
						<tr>
							<td width="100" height="100" style="background-color:#ffffff;">
							<input type="hidden" name="back-color-ffffff" value="ffffff" />
							<p>&nbsp;</p>
							</td>
							<td width="100" height="100" style="background-color:#000000;">
							<input type="hidden" name="back-color-000000" value="000000" />
							<p>&nbsp;</p>
							</td>
							<td width="100" height="100" style="background-color:#aecd37;">
							<input type="hidden" name="back-color-aecd37" value="aecd37" />
							<p>&nbsp;</p>
							</td>
							<td width="100" height="100" style="background-color:#ff8888;">
							<input type="hidden" name="back-color-ff8888" value="ff8888" />
							<p>&nbsp;</p>
							</td>
						</tr>
						<tr>
							<td width="100" height="100" style="background-color:#8888ff;">
							<input type="hidden" value="8888ff" />
							<p>&nbsp;</p>
							</td>
							<td width="100" height="100" style="background-color:#ffff55;">
							<input type="hidden" value="ffff55" />
							<p>&nbsp;</p>
							</td>
							<td width="100" height="100" style="background-color:#cd853f;">
							<input type="hidden" value="cd853f" />
							<p>&nbsp;</p>
							</td>
							<td width="100" height="100" style="background-color:#00ced1;">
							<input type="hidden" value="00ced1" />
							<p>&nbsp;</p>
							</td>
						</tr>
					</table>
					<div class="button-wrapper">
					    <button class="rounded next"
					    onclick="scroll_to('#step2-header');">Next &#9659;</button>
					</div>
				</div>
				
                <div class="window-header" id="step2-header">
                    <h2>Layout</h2>
                    <p>Choose thumbnail layout</p>
                </div>
		        <hr class="header-separator" />
				
				<div class="step-container">
					<h2 id="step3">
					    Step 2 of 4
					    <span class="step-indicator">&#8226;</span>
           			    <span class="step-indicator current">&#8226;</span>
			            <span class="step-indicator">&#8226;</span>
			           <span class="step-indicator">&#8226;</span>
					</h2>

					<table id="layout-table">
						<tr>
							<td width="160" height="100" class="single-line"
							style="background-color:#000000;">
							<input type="hidden" value="1-line-normal" />
							<p>
								first line
							</p></td>
							<td width="160" height="100" class="two-lines"
							style="background-color:#000000;">
							<input type="hidden" value="2-line-normal" />
							<p>
								first line
							</p>
							<p>
								second line
							</p></td>
							<td width="160" height="100" class="three-lines"
							style="background-color:#000000;">
							<input type="hidden" value="3-line-normal" />
							<p>
								first line
							</p>
							<p>
								second line
							</p>
							<p>
								third line
							</p></td>
						</tr>
						<tr>
							<td width="160" height="100" class="single-line-bold"
							style="background-color:#000000;">
							<input type="hidden" value="1-line-bold" />
							<p>
								<strong>first line bold</strong>
							</p></td>
							<td width="160" height="100" class="two-lines"
							style="background-color:#000000;">
							<input type="hidden" value="2-line-bold" />
							<p>
								<strong>first line bold</strong>
							</p>
							<p>
								second line
							</p></td>
							<td width="160" height="100" class="three-lines"
							style="background-color:#000000;">
							<input type="hidden" value="3-line-bold" />
							<p>
								<strong>first line bold</strong>
							</p>
							<p>
								second line
							</p>
							<p>
								third line
							</p></td>
						</tr>
					</table>
                    <div class="button-wrapper">
                        <button class="rounded prev"
                        onclick="scroll_to('#step1-header');">
                            &#9669; Go Back
                        </button>
                        <button class="rounded next"
                        onclick="scroll_to('.window-header:eq(2)');">
                            Next &#9659;
                        </button>
                    </div>
				</div>
				<div class="window-header" id="step3-header">
				    <h2>Content</h2>
				    <p>Type your content</p>
				</div>
		        <hr class="header-separator" />
				<div class="step-container">
					<h2 id="step4">
					    Step 3 of 4
					    <span class="step-indicator">&#8226;</span>
         			    <span class="step-indicator">&#8226;</span>
		         	    <span class="step-indicator current">&#8226;</span>
			            <span class="step-indicator">&#8226;</span>
					</h2>
					<div id="previewer" class="single-line" style="background-color:#000000;">
						<p><span contenteditable="true">First line</span></p>
						<p><span contenteditable="true">Second line</span></p>
						<p><span contenteditable="true">Third line</span></p>
					</div>
					<div class="button-wrapper">
						<button class="rounded prev"
						onclick="scroll_to('#step2-header');">
							&#9669; Go Back
						</button>
						<button class="rounded next"
						onclick="scroll_to('#step4-header');"
						id="thumbnail-submit">
							Next &#9659;
						</button>
					</div>
				</div>
				<div class="window-header" id="step4-header">
				    <h2>Preview</h2>
				    <p>Is this what you want?</p>
				</div>
				<hr class="header-separator" />
				<div class="step-container">
					<h2 id="result">
					    Step 4 of 4
					    <span class="step-indicator">&#8226;</span>
         			    <span class="step-indicator">&#8226;</span>
		         	    <span class="step-indicator">&#8226;</span>
			            <span class="step-indicator current">&#8226;</span>
					</h2>
					<p id="result-image">
					    <img width="354" height="275" id="preview-thumbnail"
					    src="thumbnail-generator/trans.png" />
					</p>
					<div class="button-wrapper">
					    <button class="rounded prev"
					    onclick="scroll_to('#step3-header');">
					        &#9669; Go Back
					    </button>
				        <button class="rounded next" id="finish-thumbnail">
				            Apply &#9659;
				        </button> 
					</div>
				</div>
		</div>
</div>
<script src="thumbnail-generator/jquery.js" type="text/javascript"></script>
<script src="thumbnail-generator/thumbnail-generator.js?<?= $no_cache?>"
type="text/javascript"></script>

