<?php
$no_cache = $_SERVER['REQUEST_TIME'];
$fwd_pic_url = "form_dialog.php?mode=set_inventory_picture&updateid=$updateid";
$fwd_pic_url .= $fwd_picture_vars . $extra_onchange_code;
?>
<link rel="stylesheet" media="screen"
 href="thumbnail-generator/set-inventory-picture.css?<?= $no_cache;?>" />
<form name='upload' enctype='multipart/form-data' id="main-form" method='post'
action='form_dialog.php?mode=set_inventory_picture&updateid=<?= $updateid . $fwd_picture_vars ?>'>
    <h2 class="first">Add/Edit Image</h2>
    <hr />
    <div class="left-column">
        <p>
            <img src="thumbnail-generator/search_library_icon.png" alt="Search" />
            <label for="file_selector">Image Search</label>
            <select id='file_selector'> 
                <option value='SEARCH &#9662;'></option>
<?php
for( $x=0; $x < count( $piclist ); $x++ ) {
    $fname = $piclist[$x];
	echo "<option value='$fname'";
    if( $fname == $selected_picture )
        echo " selected";
    echo ">$fname</option>";
}
?>
            </select>
        </p>
        <p>
            <img src="thumbnail-generator/upload_icon.png" alt="Upload" />
            <label for="image-uploader">Upload Image</label>
<?php //<input type='file' name='picture' id="image-uploader" onChange='document.upload.submit()'>
?>

            <label id="fake-file-input">
                Browse...&#9662;
                <input type='file' name='picture' id="image-uploader">
            </label>
        </p>
    </div>
    <div class="right-column">
        <p id="preview-label">
            Preview
        </p>
        <div id='picture_controls'>
<?php
if( isset( $update_message ) && $update_message != "" ) {
    echo $update_message;
}
echo $tab_text;
?>
        </div>
    </div>
    <div class="clearfix"></div>
    <p>
        <input id="apply-button" type="button" value="APPLY &#9656;" />
    </p>
    <hr />
    <h2>Icon Maker</h2>
    <p>
        <img src="thumbnail-generator/make_icon.png" alt="" />
        <label for="icon-maker">Make your own image</label>
         <input type='button' value='LAUNCH BUILDER &#9656;' id="icon-maker" />
    </p>

<?php /*
<select id='file_selector' onchange='window.location="form_dialog.php?mode=set_inventory_picture&updateid=<?= $updateid ?>&tab_selected=" + escape(document.getElementById("tab_selected").value) + "&setfile=" + escape(this.value) + "<?= $fwd_picture_vars . $extra_onchange_code ?>"'>
<input type='button' value='BUILDER &#9656;' id="icon-maker" onclick='window.location="<?= $fwd_pic_url ?>&picmode=generate"' />
*/ ?>
    
</form>
<script type="text/javascript" src="thumbnail-generator/jquery.js"></script>
<script type="text/javascript">
$( document ).ready( function () {
    $( "#file_selector" ).change( function () {
        var params = {
            mode : "set_inventory_picture",
            updateid  : "<?= $updateid ?>",
            tab_selected : $( "#tab_selected" )[0].value,
            setfile : this.value
        };
        var unescaped_params = "<?= $fwd_picture_vars . $extra_onchange_code ?>";
        var new_location = $.param( params ) + unescaped_params;
        new_location = "form_dialog.php?" + new_location;
        window.location = new_location;
    } );
    $( "#image-uploader" ).change( function() {
        $( "#main-form" )[0].submit();
    } );
    $( "#icon-maker" ).click( function () {
        window.location="<?= $fwd_pic_url ?>&picmode=generate";
    } );
    $( "#apply-button" ).click( picture_apply );
} );
</script>