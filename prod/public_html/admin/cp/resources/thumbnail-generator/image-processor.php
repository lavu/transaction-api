<?php
/**
 * Format of the input data expected:
 * <object> {
 *     background : <string>,
 *     lines : <array> [
 *         <object>+ {
 *             text : <string>,
 *             font-size : <integer>,
 *             coords : <object> {
 *                 top : <integer>,
 *                 left : <integer>
 *             }
 *         }
 *     ],
 *     first-bold : <boolean>,
 *     debug : <boolean>
 * }
 */
if(session_id() == ''){
	session_start();
}

if(empty($_SESSION['poslavu_234347273_admin_loggedin'])){
	trigger_error('Not Logged In');
	return;
}

$img_width  = 354;
$img_height = 275;
$lines = $_REQUEST["lines"];
$img = imagecreate( $img_width, $img_height );
$background = $_GET["background"];
$bg_red   = hexdec( substr( $_GET["background"], 0, 2 ) );
$bg_green = hexdec( substr( $_GET["background"], 2, 2 ) );
$bg_blue  = hexdec( substr ($_GET["background"], 4 ) );
$background_color = imagecolorallocate( $img, $bg_red, $bg_green, $bg_blue );
$text_color = null;

if ( $background == "ffffff" || $background == "ffff55" || $background == "00ced1" ) {
	$text_color = imagecolorallocate( $img, 0, 0, 0 );
} else {
	$text_color = imagecolorallocate( $img, 255, 255, 255 );
}

$first_line_bold = ( $_REQUEST["first-bold"] == "true")? true : false;
$black = imagecolorallocate ( $img, 0, 0, 0 );
$normal_font = __DIR__.'/fonts/muli-light/muli-light-webfont.ttf';
$bold_font   = __DIR__.'/fonts/muli-regular/muli-regular-webfont.ttf';
$num_lines = count ( $lines );
$padding_height = $img_height / ( $num_lines + 1 );

foreach( $lines as $index => $line) {
	$font = ($index == 0 && $first_line_bold )? $bold_font : $normal_font;
	$box = imagettfbbox( 28, 0, $font, trim( $line["text"] ) );
	$box_width = $box[2] - $box[0];
	$box_height = $box[1] - $box[7];
	$x_coord = $line["coords"]["left"];
	$y_coord = $line["coords"]["top"]  + $line["font-size"] - 10;
	$text = trim( $line["text"] );
	$fontsize = $line["font-size"] * 0.70;
	imagettftext( $img, $fontsize, 0, $x_coord, $y_coord, $text_color, $font, $text );
}

if ( isset( $_GET["picmode"] ) && $_GET["picmode"] === "finish_thumbnail" ) {
	$newItemName = preg_replace("/[^A-Za-z0-9]/", '', $_GET['item-name']);
	$newItemName .= '-'.substr(md5($_GET['item-name']. $_GET['updateid'].uniqid()),0,6);
	$newItemName .= "-thumbnail.png";
	$dataname = $_SESSION['poslavu_234347273_admin_dataname'];
	if(strpos($_GET['dir'],'/categories/')){
		$newPath = "{$_SERVER['DOCUMENT_ROOT']}/images/{$dataname}/categories/";
	}else{
		$newPath = "{$_SERVER['DOCUMENT_ROOT']}/images/{$dataname}/items/";
	}
	SaveAndOverwriteImage($newPath,$img,$newItemName);
} else {
	// Send it to the browser
	header( "Expires: Mon, 26 Jul 1997 05:00:00 GMT" );
	header( "Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT" );
	header( "Cache-Control: no-store, no-cache, must-revalidate" );
	header( "Cache-Control: post-check=0, pre-check=0", false );
	header( "Pragma: no-cache" );
	header( "Content-type: image/png" );
	imagepng( $img );
}
imagedestroy( $img );

function SaveAndOverwriteImage($basePath,$image,$fileName){
	#error_log(__FILE__.__FUNCTION__.": {$basePath} : {$image} : {$fileName}");
	checkAndCreateImageDirectories($basePath);
	$GLOBALS["new_filename"] = $fileName;
	$paths = array();
	$paths[] = $basePath.$fileName;
	$paths[] = $basePath.'full/'.$fileName;
	$paths[] = $basePath.'main/'.$fileName;

	foreach ($paths as $iPath) {
		if ( file_exists( $iPath ) ) {
			unlink( $iPath );
		}
		$GLOBALS["thumbnail-success"] = imagepng( $image, $iPath );
	}
}


function checkAndCreateImageDirectories($create_image_folder){
	if(!is_dir($create_image_folder)){
		mkdir($create_image_folder.'main/',0775,true);
		mkdir($create_image_folder.'full/',0775,true);
	}
	if(!is_dir($create_image_folder.'main/')){
		mkdir($create_image_folder.'main/',0775,true);
	}
	if(!is_dir($create_image_folder.'full/')){
		mkdir($create_image_folder.'full/',0775,true);
	}
	return;
}

?>
