/**
 * Function to convert the get parameters into a javascript object
 * "Borrowed" from :
 * http://stackoverflow.com/questions/4297765/make-a-javascript-array-from-url
 */
function URLToArray(url) {
    var request = {};
    var pairs = url.substring(url.indexOf('?') + 1).split('&');
    for (var i = 0; i < pairs.length; i++) {
        var pair = pairs[i].split('=');
        request[decodeURIComponent(pair[0])] = decodeURIComponent(pair[1]);
    }
    return request;
}
/* GLOBAL VARIABLES (used to keep track of the selections)*/
var color = "000000";
var layout = "1-line-normal";
var text = [];
var window_position = 0;
// a copy of the get parameters as a 
var url_params = URLToArray( location.search );
/* END OF GLOBAL VARIABLES*/

function scroll( pixel_amount ) {
    window_position += pixel_amount;
    $( "#thumbnail-prototype" )[0].scrollTop = window_position;
}

function scroll_to( id ){
    var offset = Math.floor( $( id )[0].getBoundingClientRect().top );
    window_position = window_position + offset;
    $( "#thumbnail-prototype" ).animate({scrollTop:window_position},100);
}


$( document ).ready( function () {
	$( "#color-table td" ).click( function () {
		color = $( this ).children( "input[type=hidden]" ).val();
		var color_val = '#' + color;
		$( "#selected-color" ).val( color_val );
		$( "#layout-table td" ).css( { background : color_val  });
		$( "#previewer" ).css( { background : color_val  });
		if ( color_val == "#ffffff" || color_val == "#ffff55" || color_val == "#00ced1" ) {
			$( "#layout-table td p" ).add( "#previewer p" ).css( "color", "#000000" );
		} else {
			$( "#layout-table td p" ).add( "#previewer p" ).css( "color", "#ffffff" );
		}
		$( "#color-table td" ).removeClass( "selected" );
		$( this ).addClass( "selected" );
	} );
	$( "#layout-table td" ).click( function () {
	    layout = $( this ).children( "input[type=hidden]" ).val();
		var selection = layout.split('-');
		$( "#previewer" ).removeClass();
		if ( selection[0] === '1') {
			$( "#previewer" ).addClass( 'single-line' );
			if ( selection[2]  === 'bold' ){
				$( "#previewer p:nth-child(1)" ).addClass( "bold" );
			} else {
				$( "#previewer p:nth-child(1)" ).removeClass( "bold" );
			}
		} else if ( selection[0] === '2' ) {
			$( "#previewer" ).addClass( 'two-lines' );
			if ( selection[2]  === 'bold' ){
				$( "#previewer p:nth-child(1)" ).addClass( "bold" );
			} else {
				$( "#previewer p:nth-child(1)" ).removeClass( "bold" );
			}
		} else if ( selection[0] === '3' ) {
			$( "#previewer" ).addClass( 'three-lines' );
			if ( selection[2]  === 'bold' ){
				$( "#previewer p:nth-child(1)" ).addClass( "bold" );
			} else {
				$( "#previewer p:nth-child(1)" ).removeClass( "bold" );
			}
		}
		$( "#layout-table td" ).removeClass( "selected" );
		$( this ).addClass( "selected" );
	} );
	$( "#previewer p span[contenteditable=true]" )
	    .keypress(function(e){ return e.which != 13; })
	        .keyup( autosize )
	            .blur( function ( e ) {
                    var tmp  = this.innerHTML;
                    this.innerHTML = tmp.match( /^\s+$/ )? tmp : true_trim( tmp );
	            } );
	//$( "#previewer p" ).keyup( function(e){console.log(e.which);return true;} );
	$( "#thumbnail-submit" ).click( process_thumbnail );
} );

function true_trim( str ) {
    return str.trim().replace(/^(?:&nbsp;)*/, "").replace( /(?:&nbsp;)*$/, "" ).trim();
}
/**
 * Function to automatically resize the text in the thumbnail editor
 * @param event
 */
function autosize( event ) {
	var key_pressed = event.which? event.which : "";
	var enter_key = 13;
	var backspace_key = 8;
	var delete_key = 46;
	if ( key_pressed == enter_key  ) return false;
	if( ( key_pressed == delete_key || 
	        key_pressed == backspace_key )
	          && this.innerHTML == "" ) {
	    this.innerHTML = "&nbsp;";
	} 
	/*else {
	    this.innerHTML = this.innerHTML.trim();
	}*/
	var current_fontsize = parseInt( $( this ).css( "font-size" ).slice( 0, -2 ) );
	var line_width = $( this ).width();
	var line_height = $( this ).height();
	var $previewer = $( "#previewer" );
	var max_fontsize = 177;
	if ( $previewer.hasClass( "single-line" ) ) {
		max_fontsize = 177;
	} else if ( $previewer.hasClass( "two-lines" ) ) {
		max_fontsize = 88;
	} else {
		max_fontsize = 67;
	}
//	if ( line_height > ( current_fontsize * 2 ) ) {
	if ( line_width > 350 ) {
		//make_smaller( this );
//		while ( line_height > ( current_fontsize * 2 ) ) {
		while ( line_width > 350 ) {
			$( this ).css( "font-size", --current_fontsize );
			line_width = $( this ).width();
		}
	} else if ( line_width < 354 ) {
		while ( line_width < ( 318 ) && current_fontsize < max_fontsize ) {
		    $( this ).css( "font-size", ++current_fontsize );
		    line_width = $( this ).width();
		}
	}
}

function replace_img( src ) {
   var new_img = document.createElement('img');
   $( new_img ).load( function () {
       var img_already_exists = $( '#preview-thumbnail' );
       if ( img_already_exists ) $( '#preview-thumbnail' ).remove();
       this.setAttribute( 'id', 'preview-thumbnail' );
       $( this ).appendTo( '#result-image' );
   } );
   new_img.setAttribute( 'src', src );
}
/**
 * Packs all the data as an object and sends it to the script to generate the
 * icon image.
 * @returns {Boolean}
 */
function process_thumbnail ( event ) {
    replace_img( 'thumbnail-generator/lavu_spin.gif' );
	lines = [];
	var num_lines = layout.split('-')[0];
	for ( var i = 0; i < num_lines; i++ ) {
		var $current_p = $( "#previewer p" ).eq( i );
		var object = {};
	    object["text"] = $current_p.text();
	    var p_offset = $current_p.position();
	    var $child_span = $current_p.children( "span[contenteditable=true]" );
	    var span_offset = $child_span.position();
	    var fontsize = parseInt( $child_span.css( "font-size" ).slice( 0 , -2 ) );
	    object["coords"] = {
	    		"top" : p_offset.top + span_offset.top, 
	    		"left": span_offset.left
	    };
	    object["font-size"] = fontsize;
	    lines.push( object );
	}
	var thumbnail_params = {
		"background" : color,
		"lines" : lines,
		"first-bold" : (layout.split('-')[2] == "bold"),
		"debug" : false
	};
	//	if (console) console.log( $.param ( thumbnail_params ) );
	var src = "thumbnail-generator/image-processor.php?" + $.param( thumbnail_params );
	replace_img( src );
	var new_params = {
	    "picmode" : "finish_thumbnail",
	    "item-name" : item_name // this parameter is passed in a script tag.
	};
	$( "#finish-thumbnail" ).click( function ( event ) {
	    var merged_params = $.extend( true, url_params, thumbnail_params, new_params );
	    var new_get = $.param( merged_params );
	    window.location = "?" + new_get;
	} );
    event.preventDefault();
	return false;
}