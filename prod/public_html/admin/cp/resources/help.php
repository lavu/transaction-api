<?php
	$mode = (isset($_GET['m']))?$_GET['m']:"";

	
	$display = "";
	
	if ($mode == "GatewayNotes") {
	
		$display = "<div style='width:628px; background-color:white; padding: 5px 10px;'>
		  <h2 style='text-align:center;'>IMPORTANT NOTE FOR ALL GATEWAYS</h2>
		  <div style='font-size:14px; line-height:18px; border-bottom: solid 1px #ccc; text-align:left;'>
		    <p>
		      Credit card batches run from Batch Settlement to Batch Settlement and do not necessarily line up with the date or the day start/end time. A merchant set up for manual batch settlement may keep a batch open for several days, though it is recommended that the batch be settled at least once a day. In case a merchant does keep a batch open for more than one day and wants to be able to submit tips, all tips for all days must be submitted before the batch is settled in order for the tips to be processed.
		    </p>
		    <p>
		      For instance, if a batch is left open from August 12th to August 15th, pressing the &#34;Settle Credit Card Batch&#34; while on the End of Day screen for the 12th after submitting the tips for the 12th will settle all the transactions that occurred from the 12th to the 15th, not just the transactions from the 12th. All the transactions from the 12th to the 15th belong to the same batch. The tip adjustments must first be submitted for the 12th, 13th, 14th, and 15th before settling the batch in order for the tips to be processed.
		    </p>
		    <p>
		      Any clients using Auth as the default transactions must be sure to submit captures for all their transactions each day prior to submitting the batch for settlement. Failure to do so will result in transactions being left off of the batch, and the funds will not be transferred. To ensure that all transactions are submitted for capture each day, the owner or a store manager should go to the All Users detailed End of Day report and click the &#34;Submit Tip Changes / Capture Authorizations&#34; button at the bottom of the page. This action should be included in the daily or nightly routine prior to the daily or nightly batch settlement.
		    </p>
		  </div>
			
		  <h3 style='text-align:center;'>PayPal</h3>
		  <div style='font-size:14px; line-height:18px; border-bottom: solid 1px #ccc; text-align:left;'>
		    <p>
		      Lavu’s integration with PayPal as a CC gateway and processing platform does not require entry of any integration data as is the case with other preferred gateway partners. Instead, PayPal integration is handled from the Extensions area of the Admin Control Panel through a simple guided onboarding process. 
		    </p>
		    <a href='https://support.lavu.com/hc/en-us/articles/216768898-Lavu-PayPal-EMV-Solution-' target='_blank' style='color:#aecd37; text-decoration:none;'>Learn more about setting up payment integration with PayPal.</a>
		    <p>
		      <span style='text-decoration:underline;'>Compatible Card Readers:</span> PayPal Miura EMV Reader (Sale only), Magtek iDynamo (Sale OR Auth, non-EMV)
		    </p>
		    <p>
		      <span style='text-decoration:underline;'>Settlement:</span> Transactions are settled and migrated to the merchant’s PayPal account upon completion of the sale. The funds are available immediately via PayPal, and a sweep of completed transactions from PayPal to the merchant’s bank account is conducted daily at either 5 PM or 12 AM PST. The availability of swept funds varies depending on the banking institution, but may be accessible as soon as 24 hours after transfer. 
		    </p>
		    <p>
		      <span style='text-decoration:underline;'>Tips:</span> In accordance with EMV processing rules, tips must be added to transactions processed on the Miura EMV reader at the time of sale. Tip adjustments after payment are not supported. 
		    </p>
		    <p>
		      Auth transactions are supported for the Magtek iDynamo, which permits tab preauthorizations. Tips for this transaction type must be added during preauth capture. Because the Miura reader only supports Sale transactions, merchants whose transaction type is set to Auth cannot also utilize Lavu + Paypal’s EMV solution.
		    </p>
		  </div>
			
		  <h3 style='text-align:center;'>Vantiv</h3>
		  <div style='font-size:14px; line-height:18px; border-bottom: solid 1px #ccc; text-align:left;'>
		    <ul style='list-style-type:none;'>
		      <li>
		        <span style='font-style:italic; margin-right:7px;'>Integration data 1:</span>MERCHANT NUMBER
		      </li>
		      <li>
		        <span style='font-style:italic; margin-right:7px;'>Integration data 2:</span>WEB SERVICES PASSWORD
		      </li>
		      <li>
		        <span style='font-style:italic; margin-right:7px;'>Integration data 3:</span>HOSTED CHECKOUT MANUAL ENTRY TERMINAL ID
		      </li>
		      <li>
		        <span style='font-style:italic; margin-right:7px;'>Integration data 4:</span>HOSTED CHECKOUT PASSWORD
		      </li>
		      <li>
		        <span style='font-style:italic; margin-right:7px;'>Integration data 5:</span><span style='color:#333;'>leave blank</span>
		      </li>
		    </ul>
		    <a href='https://support.lavu.com/hc/en-us/articles/204855270-What-Integration-Data-do-I-enter-for-Mercury-' target='_blank' style='color:#aecd37; text-decoration:none;'>Learn more about Vantiv Integration Data.</a>
		    <p>
		      <span style='text-decoration:underline;'>Compatible Card Readers:</span> Magtek iDynamo, IDTech USB Reader (requires Epson TM-T88V-i smart printer)
		    </p>
		    <p>
		      The primary merchant account cannot be used to process both keyed-in transactions and End-To-End Encryption (using iDynamo swipes). Keyed-in transactions must then be processed through Vantiv's Hosted Checkout system, which requires a separate set of credentials.
		    </p>
		    <p>
		      Supports Tip Adjustments (Adjusts) for Sale transactions and PreAuthCaptures for Auth (PreAuth) transactions. The value to use for the default transaction type when a merchant wants to be able to perform tip adjustments may depend upon the processor, merchant service provider, or bank being used by the merchant. Vantiv should advise.
		    </p>
		    <p>
		      Credit card batch is held by Vantiv's system. The merchant has the option of having the batch automatically settle at a certain time each day or to be manually settled by the merchant at the merchant's discretion. Some processors may not support manual batch settlement.
		    </p>
		    <p>
		      The batch is manually settled when the back end user presses &#34;Settle Credit Card Batch&#34;, at which point a CaptureAll (BatchSummary & BatchClose) is sent to Vantiv.
		    </p>
		    <p>
		      The Hosted Checkout batch is separate from the primary batch but runs parallel to it, so any manual batch settlement requests will be sent for both when the end user presses &#34;Settle Credit Card Batch&#34;. In case the merchant opts for auto batch settlement, the time of settlement should be the same for both batches.
		    </p>
		  </div>
			
		  <h3 style='text-align:center;'>Bridgepay</h3>
		  <div style='font-size:14px; line-height:18px; border-bottom: solid 1px #ccc; text-align:left;'>
		    <ul style='list-style-type:none;'>
		      <li>
		        <span style='font-style:italic; margin-right:7px;'>Integration data 1:</span>USERNAME
		      </li>
		      <li>
		        <span style='font-style:italic; margin-right:7px;'>Integration data 2:</span>PASSWORD
		      </li>
		      <li>
		        <span style='font-style:italic; margin-right:7px;'>Integration data 3:</span><span style='color:#333;'>leave blank</span>
		      </li>
		      <li>
		        <span style='font-style:italic; margin-right:7px;'>Integration data 4:</span><span style='color:#333;'>leave blank</span>
		      </li>
		      <li>
		        <span style='font-style:italic; margin-right:7px;'>Integration data 5:</span><span style='color:#333;'>leave blank</span>
		      </li>
		    </ul>
		    <a href='https://support.lavu.com/hc/en-us/articles/204589794-What-Integration-Data-do-I-enter-for-BridgePay-TGate-' target='_blank' style='color:#aecd37; text-decoration:none;'>Learn more about Bridgepay Integration Data.</a>
		    <p>
		      <span style='text-decoration:underline;'>Compatible Card Readers:</span> Magtek iDynamo, IDTech USB Reader (requires Epson TM-T88V-i smart printer)
		    </p>
		    <p>
					Supports Tip Adjustments (Adjustments) for Sale transactions and PreAuthCaptures (Force Auths) for Auth transactions. The value to use for the default transaction type when a merchant wants to be able to perform tip adjustments may depend upon the processor, merchant service provider, or bank being used by the merchant. Bridgepay should advise.		   
				</p>
				<p>
					Credit card batch is held by Bridgepay's system. The merchant has the option of having the batch automatically settle at a certain time each day or to be manually settled by the merchant at the merchant's discretion. Some processors may not support manual batch settlement.		    
				</p>
		    <p>
					The batch is manually settled when the back end user presses &#34;Settle Credit Card Batch&#34;, at which point a CaptureAll is sent to Bridgepay.		    
				</p>
		  </div>
			
			<h3 style='text-align:center;'>Heartland</h3>
		  <div style='font-size:14px; line-height:18px; border-bottom: solid 1px #ccc; text-align:left;'>
		    <ul style='list-style-type:none;'>
		      <li>
		        <span style='font-style:italic; margin-right:7px;'>Integration data 1:</span>SOFTWARE USER ID
		      </li>
		      <li>
		        <span style='font-style:italic; margin-right:7px;'>Integration data 2:</span>SOFTWARE USER PASSWORD
		      </li>
		      <li>
		        <span style='font-style:italic; margin-right:7px;'>Integration data 3:</span>LICENSE ID
		      </li>
		      <li>
		        <span style='font-style:italic; margin-right:7px;'>Integration data 4:</span>SITE ID
		      </li>
		      <li>
		        <span style='font-style:italic; margin-right:7px;'>Integration data 5:</span>DEVICE ID
		      </li>
		    </ul>
			<a href='https://support.lavu.com/hc/en-us/articles/204855290-What-Integration-Data-do-I-enter-for-Heartland-' target='_blank' style='color:#aecd37; text-decoration:none;'>Learn more about Heartland Integration Data.</a>
			<p>
				<span style='text-decoration:underline;'>Compatible Card Readers:</span>IDTech USB Reader (requires Epson TM-T88V-i smart printer)
			</p>
			<p>
				Supports Tip Adjustments (CreditTxnEdits) for Sale transactions and PreAuthCaptures (CreditAddToBatches) for Auth transactions. The value to use for the default transaction type when a merchant wants to be able to perform tip adjustments may depend upon the processor, merchant service provider, or bank being used by the merchant. Heartland should advise.
			</p>
			<p>
				Credit card batch is held by Heartland's system. The merchant has the option of having the batch automatically settle at a certain time each day or to be manually settled by the merchant at the merchant's discretion.
			</p>
			<p>
				The batch is manually settled when the back end user presses &#34;Settle Credit Card Batch&#34;, at which point a BatchClose is sent to Heartland.
			</p>
		</div>";
	} else if($mode == "tipoutRules") {
		$display = "<div style = 'font-size: 20px'><ul style='font: italic 20px/1.5 Helvetica, Verdana, sans-serif;'>
				<li style='margin-bottom:20px;'>Setting this rule to Evenly will split up the tips evenly amongst all employees. </li><li>Setting this rule to Hours Worked, will calculate the tips generated per hour and pay the hourly tips to each employee based on the hours worked.</li></ul></div>";
	} else if ($mode == "offlineCCTransaction") {
		$display = "<div style='width:628px; background-color:white; padding: 5px 10px;'>
		<h2 style='text-align:center;'>Important Notice From Lavu and PayPal:</h2>
		<div style='font-size:14px; line-height:18px; border-bottom: solid 1px #ccc; text-align:left;'>
			<p>
			Offline mode is only available with EMV-capable card readers. Also, offline mode forces the supported card readers to only accept swipe transactions.
			You are subject to the following limits on activity with the offline payments feature:
			</p>
			<p>
			You can only process transactions under USD $5,000.
			</p>
			<p>
			You can only process a total of USD $50,000 while offline.
			</p>
			<p>
			You must reconnect to the Internet within 72 hours or the offline transactions expire.
			</p>
			<p>
			Tip adjustment for offline authorizations is not supported. Offline transactions can only be captured for the initial amount that was ran on the card while the card was present.
			</p>
			<p>
			These limits are subject to change at PayPal's sole discretion. PayPal informs you of changes by email.
			</p>
			<p>
			You as the merchant assume all liability for any offline transactions, including those that are subsequently declined, expired, or disputed. You cannot dispute declined offline transactions. You also assume all liability for offline transactions if the device is lost, stolen, damaged, or you delete your app before re-connecting to the Internet. Any refunds are processed in the normal course after you are re-connected to the Internet.
			</p>
			</div>";
	} else {
	    $display = "Unknown mode passed to help script...";
	}
?>

<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<title>Help</title>

		<style type="text/css">
			body { background-color: transparent; margin-left: 0px; margin-top: 0px; margin-right: 0px; margin-bottom: 0px; font-family:Verdana, Arial, Helvetica, sans-serif; }
		</style>
		
		<script language='javascript'>
		
			setTimeout(function () {
				var startY = 0;
				var startX = 0;
				var b = document.body;
				b.addEventListener('touchstart', function (event) {
					//parent.window.scrollTo(0, 1);
					startY = event.targetTouches[0].pageY;
					startX = event.targetTouches[0].pageX;
				});
				b.addEventListener('touchmove', function (event) {
					event.preventDefault();
					var posy = event.targetTouches[0].pageY;
					var h = parent.document.getElementById("iframe_scroller");
					var sty = h.scrollTop;
	
					var posx = event.targetTouches[0].pageX;
					var stx = h.scrollLeft;
					h.scrollTop = sty - (posy - startY);
					h.scrollLeft = stx - (posx - startX);
					startY = posy;
					startX = posx;
				});
			}, 1000);
		</script>
	</head>
	
	<body style="padding: 2px 5px;">
		<?php echo $display; ?>
	</body>
</html>