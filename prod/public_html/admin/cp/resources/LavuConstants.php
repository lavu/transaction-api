<?php
/*
This file is used to add value in constant. Please take initiative use constant in code instead of hardcoded
value like gateway, Transtype etc
*/

define('GATEWAY_TYPE_PAYPAL', 'paypal');
define('PAYMENT_TYPE_CASH', "Cash");
define('PAYMENT_TYPE_CARD', "Card");
define('TRANSACTION_TYPE_AUTH', 'Auth');
define('EDIT_TYPE', 'EDITABLE');
define('NONEDIT_TYPE', 'NONEDITABLE');