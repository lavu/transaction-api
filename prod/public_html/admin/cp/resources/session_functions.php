<?php

global $__LAVU_SESSION_FUNCTIONS_H__;

if (!isset($__LAVU_SESSION_FUNCTIONS_H__))
{
  /**
   * This function has the intention to get the subdomain of the current HTTP_HOST
   * in order to allow sharing the PHPSESSID cookie alongside several Lavu products.
   * 
   * Ex:
   *  standarizeSessionDomain('localhost:8888') => 'localhost'
   *  standarizeSessionDomain('admin.poslavu.com') => '.poslavu.com'
   */


  function getStandardDomain() {
    $urlWithoutPort = preg_replace('/:[0-9]+/', '', $_SERVER['HTTP_HOST']);
    $domainArray = explode('.', $urlWithoutPort);
    $domainArraySize = count($domainArray);

    if ($domainArraySize === 1) {
            $domain = $domainArray[0];
    }
    else if ($domainArraySize === 2) {
            $domain = '.' . $domainArray[0] . '.' . $domainArray[1];
    }
    else if ($domainArraySize > 2) {
            $domain = '.' . substr(strchr($urlWithoutPort, "."), 1);
    }

    return $domain;
  }


  function standarizeSessionDomain() {
    $domain = getStandardDomain();

    session_set_cookie_params(0, '/', $domain);

    return $domain;
  }
}
?>
