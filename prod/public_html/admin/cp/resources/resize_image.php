<?php
	class Resize_Image 
	{
		var $image_to_resize;
		var $new_width;
		var $new_height;
		var $ratio;
		var $new_ratio;
		var $new_image_name;
		var $save_folder;
		
		function resize()
		{
			if(!file_exists($this->image_to_resize))
			{
			  exit("File ".$this->image_to_resize." does not exist.");
			}
			
			$info = GetImageSize($this->image_to_resize);
			
			if(empty($info))
			{
			  exit("The file ".$this->image_to_resize." doesn't seem to be an image.");
			}
			
			$width = $info[0];
			$height = $info[1];
			$mime = $info['mime'];
					
			if($this->ratio)
			{
				if (isset($this->new_ratio))
				{
					$this->new_width = $width * $this->new_ratio;
					$this->new_height = $height * $this->new_ratio;
				}
				
				if (isset($this->new_width))
				{
					$factor = (float)$this->new_width / (float)$width;
					$this->new_height = $factor * $height;
				}
				else if (isset($this->new_height))
				{
					$factor = (float)$this->new_height / (float)$height;
					$this->new_width = $factor * $width;
				}
				else
				{
					exit("Width or Height is not set");
				}
				
				$type = substr(strrchr($mime, '/'), 1);
				
				switch ($type)
				{
					case 'jpeg':
						$image_create_func = 'ImageCreateFromJPEG';
						$image_save_func = 'ImageJPEG';
						$new_image_ext = 'jpg';
						break;
					
					case 'png':
						$image_create_func = 'ImageCreateFromPNG';
						$image_save_func = 'ImagePNG';
						$new_image_ext = 'png';
						break;
					
					case 'bmp':
						$image_create_func = 'ImageCreateFromBMP';
						$image_save_func = 'ImageBMP';
						$new_image_ext = 'bmp';
						break;
					
					case 'gif':
						$image_create_func = 'ImageCreateFromGIF';
						$image_save_func = 'ImageGIF';
						$new_image_ext = 'gif';
						break;
					
					case 'vnd.wap.wbmp':
						$image_create_func = 'ImageCreateFromWBMP';
						$image_save_func = 'ImageWBMP';
						$new_image_ext = 'bmp';
						break;
					
					case 'xbm':
						$image_create_func = 'ImageCreateFromXBM';
						$image_save_func = 'ImageXBM';
						$new_image_ext = 'xbm';
						break;
					
					default:
						$image_create_func = 'ImageCreateFromJPEG';
						$image_save_func = 'ImageJPEG';
						$new_image_ext = 'jpg';
				}
				
				// New Image
				$white    = imagecolorallocate($image, 0xFF, 0xFF, 0xFF);
				$image_c = ImageCreateTrueColor($this->new_width, $this->new_height);
				
				// allow transparency
				imagealphablending($image_c, false);
				imagesavealpha($image_c,true);
				$transparent = imagecolorallocatealpha($image_c, 255, 255, 255, 127);
				imagefilledrectangle($image_c, 0, 0, $this->new_width, $this->new_height, $transparent);
				imagecopyresampled($image_c, $im, 0, 0, 0, 0, $this->new_width, $this->new_height, $width, $height);
							
				$new_image = $image_create_func($this->image_to_resize);
			
				ImageCopyResampled($image_c, $new_image, 0, 0, 0, 0, $this->new_width, $this->new_height, $width, $height);
		
				if($this->save_folder)
				{
				   if($this->new_image_name)
				   {
					$new_name = $this->new_image_name.'.'.$new_image_ext;
				   }
				   else
				   {
					$new_name = $this->new_thumb_name( basename($this->image_to_resize) ).'_resized.'.$new_image_ext;
				   }
		
					$save_path = $this->save_folder.$new_name;
				}
				else
				{
					/* Show the image without saving it to a folder */
				   header("Content-Type: ".$mime);
		
				   $image_save_func($image_c);
		
				   $save_path = '';
				}
		
				$process = $image_save_func($image_c, $save_path);
		
				return array('result' => $process, 'new_file_path' => $save_path);
		
			}
		}
	
		function new_thumb_name($filename)
		{
			$string = trim($filename);
			$string = strtolower($string);
			$string = trim(ereg_replace("[^ A-Za-z0-9_]", " ", $string));
			$string = ereg_replace("[ tnr]+", "_", $string);
			$string = str_replace(" ", '_', $string);
			$string = ereg_replace("[ _]+", "_", $string);
		
			return $string;
		}
	}
	if(isset($_GET['image']) && isset($_GET['ratio']))
	{
		$file = $_GET['image'];
		$ratio = $_GET['ratio'];
		if(!is_numeric($ratio))
			$ratio = 1;
		
		$img = new Resize_Image;
		$img->image_to_resize = $file;
		$img->new_ratio = $ratio;
		$img->ratio = true;
		$img->new_image_name = false;
		$img->save_folder = false;
		$img->resize();
	}
	else if(isset($_GET['image']) && isset($_GET['width']) && isset($_GET['height']))
	{
		$file = $_GET['image'];
		$width = $_GET['width'];
		$height = $_GET['height'];
		
		$image_info = GetImageSize($file);
		$current_width = $image_info[0];
		$current_height = $image_info[1];
		
		$ratio_width = $width / $current_width;
		$ratio_height = $height / $current_height;
		
		if($ratio_width < $ratio_height) $ratio = $ratio_height;
		else $ratio = $ratio_width;
		
		$img = new Resize_Image;
		$img->image_to_resize = $file;
		$img->new_ratio = $ratio;
		$img->ratio = true;
		$img->new_image_name = false;
		$img->save_folder = false;
		$img->resize();
	}
?>