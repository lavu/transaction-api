<?php
session_start();
require_once(dirname(__FILE__) . "/../core_functions.php");
lavu_select_db(sessvar("admin_database"));
if(isset($_GET['als_ajax'])) {
	if(isset($_GET['regionid']) && $_GET['regionid'] != "") {
		$query = "SELECT distinct alphabetic_code
							FROM `poslavu_MAIN_db`.`country_region`
							WHERE region_id=".$_GET['regionid']." AND active=1
							 ORDER BY name asc ";
		$result = lavu_query($query);
		if(!$result){
			error_log("mysql error in ".__FILE__." mysql error:" . lavu_dberror());
			header('Content-type: application/json');
			echo '{}';
			exit;
		}
		else{
			while ($currency_info = mysqli_fetch_assoc($result)) {
				$selectedRegionCurrencyInfo[] = $currency_info;
			}
			var_dump(json_encode($selectedRegionCurrencyInfo));
			exit;
			//header('Content-type: application/json');
			echo json_encode($selectedRegionCurrencyInfo);
			exit;
		}
	} else {
		error_log("problem with ajax request, unable to get regionid parameter.");
		header('Content-type: application/json');
		echo '{}';
		exit;
	}

}

if(isset($_GET['get_monetary_symbol'])) {
	if(isset($_POST['code']) && $_POST['code'] != "") {
		$query = " SELECT monetary_symbol
							FROM `poslavu_MAIN_db`.`country_region`
							WHERE alphabetic_code='[1]' AND active=[2]
								AND region_id=[3] limit 1 ";
		$result = lavu_query($query,$_POST['code'],1,$_POST['regionId']);
		if(!$result){
			error_log("mysql error in ".__FILE__." mysql error:" . lavu_dberror());
			header('Content-type: application/json');
			echo '{}';
			exit;
		}
		else{
			$selectedRegionCurrencyInfo = array();
			while ($currencyCode = mysqli_fetch_assoc($result)) {
				$selectedRegionCurrencyInfo[] = $currencyCode;
			}
			header('Content-type: application/json');
			echo json_encode($selectedRegionCurrencyInfo);
			exit;
		}
	} else {
		error_log("problem with ajax request, unable to get regionid parameter.");
		header('Content-type: application/json');
		echo '{}';
		exit;
	}
}
