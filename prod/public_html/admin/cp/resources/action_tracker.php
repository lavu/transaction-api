<?php

require_once __DIR__."/lavuquery.php";
require_once __DIR__."/action_tracker_settings.php";

if (isset($_POST['track_action']))
{
	$action_type = $_POST['action_type'];
	$action = $_POST['action'];
	$origin = $_POST['origin']; //Not sure whether to use this. Not using it right now
	$reset = false;
	if (isset($_POST['reset']))
	{
		$reset = $_POST['reset'];
	}

	CPActionTracker::track_action($action_type, $action, NULL/*,$origin*/, $reset);
}

class CPActionTracker
{
	//Any action type set in filters will become a blacklist. Blacklist specific items by setting them to true.
	private static $blacklist_filters = array(
		'menu/edit_menu' => array(
			'updated_modifier_list_id' => true
		)
	);

	private static $action_append_get = array(
		'reports/reports_reports_v2' => 'report'
	);

	// Any action type set here will become whitelisted for emails.
	// Title/translation is different from action to enhance human readability
	private static $email_filters = array(
		// 'billing/billing' => array(
		// 	'cancel_step_info' => array(
		// 		'title' => 'Cancel Reason Page',
		// 		'recipients' => array(
		// 			// 'ezra@lavu.com',
		// 			'ag@lavu.com',
		// 			'support@lavu.com',
		// 			'lucas@lavu.com',
		// 			'jon@lavu.com'
		// 		),
		// 	),
		// 	'cancel_step_back1' => array(
		// 		'title' => 'Return from Cancel Reason Page',
		// 		'recipients' => array(
		// 			// 'ezra@lavu.com',
		// 			'ag@lavu.com',
		// 			'support@lavu.com',
		// 			'lucas@lavu.com',
		// 			'jon@lavu.com'
		// 		),
		// 	),
		// 	'cancel_step_back2' => array(
		// 		'title' => 'Return from Upgrade Page',
		// 		'recipients' => array(
		// 			// 'ezra@lavu.com',
		// 			'ag@lavu.com',
		// 			'support@lavu.com',
		// 			'lucas@lavu.com',
		// 			'jon@lavu.com'
		// 		),
		// 	),
		// 	'info_switch_three' => array(
		// 		'title' => 'Upgrade Page',
		// 		'recipients' => array(
		// 			// 'ezra@lavu.com',
		// 			'ag@lavu.com',
		// 			'support@lavu.com',
		// 			'lucas@lavu.com',
		// 			'jon@lavu.com'
		// 		),
		// 	),
		// 	'info_switch_three_posted' => array(
		// 		'title' => 'Upgrade Page Submit',
		// 		'recipients' => array(
		// 			// 'ezra@lavu.com',
		// 			'ag@lavu.com',
		// 			'support@lavu.com',
		// 			'lucas@lavu.com',
		// 			'jon@lavu.com'
		// 		),
		// 	),
		// 	'cancel_step_continue' => array(
		// 		'title' => 'Confirm Cancel Popup ( Submit On Cancel Reason Page )',
		// 		'recipients' => array(
		// 			// 'ezra@lavu.com',
		// 			'ag@lavu.com',
		// 			'support@lavu.com',
		// 			'lucas@lavu.com',
		// 			'jon@lavu.com'
		// 		),
		// 	),
		// 	// 'billing_info' => array(
		// 	// 	'title' => 'My Account Page',
		// 	// 	'recipients' => array(
		// 	// 		// 'ezra@lavu.com',
		// 	// 		'ag@lavu.com',
		// 	'support@lavu.com',
		// 	// 		'lucas@lavu.com',
		// 	// 		'jon@lavu.com'
		// 	// 	),
		// 	// ),
		// 	'cancel_step_final' => array(
		// 		'title' => 'Customer Canceled Account',
		// 		'recipients' => array(
		// 			// 'ezra@lavu.com',
		// 			'ag@lavu.com',
		// 			'support@lavu.com',
		// 			'lucas@lavu.com',
		// 			'jon@lavu.com'
		// 		),
		// 	),
		// )
	);

	//Generates email message and sends it, according to email filtering metadata in $email_filters
	private static function send_email($email_filter) {
		$recipients = implode(',',$email_filter['recipients']);
		$translation = $email_filter['title'];
		$dataname = admin_info('dataname');
		$username = admin_info('username');

		// Get the signup row for the dataname's package level
		$result = mlavu_query("SELECT `package` FROM `poslavu_MAIN_db`.`signups` WHERE `dataname`='[dataname]'", array('dataname' => $dataname));
		if ($result === false) die(mlavu_dberror());
		$signup = mysqli_fetch_assoc($result);
		// Get package levels object
		global $o_package_container;
		if (!isset($o_package_container)) $o_package_container = new package_container();
		// Get the package name
		$package_name = $o_package_container->get_printed_name_by_attribute('level', $signup['package']);
		if(!$package_name) $package_name = 'None';

		// Get the restaurant name
		$result = mlavu_query("SELECT `company_name`,`email`,`phone`,`created` FROM `poslavu_MAIN_db`.`restaurants` WHERE `data_name`='[dataname]'", array('dataname' => $dataname));
		if ($result === false) die(mlavu_dberror());
		$company = mysqli_fetch_assoc($result);
		$company_name = $company['company_name'];
		$company_phone = $company['phone'] || "Not On File";
		$company_email = $company['email'] || "Not On File";
		$company_created = $company['created'] || "Not On File";

		// Get user data
		$result = lavu_query("SELECT `email`, `phone` FROM `poslavu_".$dataname."_db`.`users` WHERE `username`='[username]'", array('username' => $username));
		if ($result === false) die(mlavu_dberror());
		$user = mysqli_fetch_assoc($result);
		$user_email = $user['email'] || "Not On File";
		$user_phone = $user['phone'] || "Not On File";

		$message = "";
		$message .= "This is an auto-generated email. Please do not reply to this message.\n\n";
		$message .= "DATANAME: " . $dataname . "\n";
		$message .= "TIMESTAMP (GMT -6:00, Chicago): " . date("Y-m-d H:i:s") . "\n";
		$message .= "ACTION: " . $translation . "\n";
		$message .= "\n";
		$message .= "LOCATION  : " . $company_name . "\n";
		$message .= "LOCATION P: " . $company_phone . "\n";
		$message .= "LOCATION E: " . $company_email . "\n";
		$message .= "PKG LEVEL: " . $package_name . "\n";
		$message .= "CREATION DATE " . $company_created . "\n";
		$message .= "\n";
		$message .= "USERNAME: " . $username . "\n";
		$message .= "USER P: " . $user_phone . "\n";
		$message .= "USER E: " . $user_email . "\n";
		// error_log($message);
		// $message .= "Action Type: " . $action_type . "\n";
		mail($recipients,"CP Alert: $action",$message,"From: noreply@poslavu.com");
	}

	// Public hook to log an action. Unless a blacklist filter is specified for $action_type, this function will log everything blindly.
	// Creates chains of actions in the database that reset by default when the root action_type changes.
	public static function track_action($action_type, $action, $origin=NULL, $reset=FALSE)
	{
		global $cp_action_tracker_enabled;

		if(!$cp_action_tracker_enabled)
		{
			return;
		}

		$dataname = admin_info('dataname');
		if($dataname == 'poslavu_admin') return;
		// error_log($action_type . " , " . $action . " , " . $origin . " , " . $reset);
		$resetOverride = false;
		//Check filters
		if (isset($email_filters[$action_type]))
		{
			if(CPActionTracker::$email_filters[$action_type] && CPActionTracker::$email_filters[$action_type][$action]){
				// error_log("sending email");
				CPActionTracker::send_email(CPActionTracker::$email_filters[$action_type][$action]);
			}
		}

		if (isset($blacklist_filters[$action_type]))
		{
			if(CPActionTracker::$blacklist_filters[$action_type] && CPActionTracker::$blacklist_filters[$action_type][$action]){
				// error_log("filtered: " . $action_type . " : " . $action);
				return;
			}
		}
		
		// error_log("foo");
		if (isset($action_append_get[$action_type]))
		{
			if( CPActionTracker::$action_append_get[$action_type] ){
				if(isset($_GET[CPActionTracker::$action_append_get[$action_type]])){
					// error_log($_GET[CPActionTracker::$action_append_get[$action_type]]);
					$action_type .= ':' . $_GET[CPActionTracker::$action_append_get[$action_type]];
					$resetOverride = true;
				}
			}
		}

		$origin = $origin || $_SERVER['REQUEST_URI'];
		// error_log($action_type . ',' . $action . ',' . $origin .','.$reset);

		$args = array(
			'dataname' => $dataname,
			'action_type' => $action_type,
			'action' => $action,
			'timestamp' => date("Y-m-d H:i:s"),
			'origin' => $origin,
			'username' => admin_info('username'),
			'access_level' => admin_info('access_level'),
			'chainid' => admin_info('chain_id'),
			'sequence_last' => 0,
			'sequence_head' => 0
		);
		if($reset !== true){
			$prev_query = mlavu_query("SELECT `action_type`, `id`, `sequence_head` FROM `poslavu_MAIN_db`.`cp_action_log` WHERE  `dataname`='[dataname]' AND `username`='[username]' ORDER BY `id` DESC LIMIT 1", $args);
			if(!$prev_query){
				
				error_log("couldn't retrieve previous action");
			}
			else if(mysqli_num_rows($prev_query)) {
				$prev = mysqli_fetch_assoc($prev_query);
				$t1 = explode('/', $prev['action_type']);
				$t1 = $t1[0];
				$t2 = explode('/', $action_type);
				$t2 = $t2[0];
				if($t2 != $t1) {
					$reset = true;
				}
				else {
					$args['sequence_last'] = $prev['id'];
					$args['sequence_head'] = $prev['sequence_head'] ? $prev['sequence_head'] : $prev['id'];
				}
			}
			else $resetOverride = true;
		}

		if($reset){
			// error_log('resetting chain ' . $reset);
		}
		if(!$reset && $action == 'enter' && !$resetOverride) {
			return;
		}

		mlavu_query("INSERT INTO `poslavu_MAIN_db`.`cp_action_log` SET `dataname`='[dataname]', `action_type`='[action_type]', `action`='[action]', `timestamp`='[timestamp]', `origin`='[origin]', `username`='[username]', `access_level`='[access_level]', `sequence_last`='[sequence_last]', `sequence_head`='[sequence_head]'", $args);
		$e = mlavu_dberror();
		if( $e ){
			error_log("Unable to log cp action\n");
			error_log($e);
			return $e;
		}
		return "success";
	}

	// Public hook to get a javascript function that can be used to access this file.
	public static function get_javascript() {
		$js = <<<JS
			<script type='text/javascript'>
			function cp_track_action(action_type, action, origin, reset) {
				origin = origin || window.location.href;
				reset = reset || false;

				var xmlHttpReq;
				if (window.XMLHttpRequest) // Mozilla/Safari
				{
					xmlHttpReq = new XMLHttpRequest();
				}
				else if (window.ActiveXObject) // IE
				{
					xmlHttpReq = new ActiveXObject("Microsoft.XMLHTTP");
				}
				xmlHttpReq.open('POST', "?widget=../resources/action_tracker", true);
				xmlHttpReq.setRequestHeader('Content-Type', "application/x-www-form-urlencoded");
				xmlHttpReq.send( "track_action=true&origin="+encodeURIComponent(JSON.stringify(origin))+"&action_type="+encodeURIComponent(JSON.stringify(action_type))+"&action="+encodeURIComponent(JSON.stringify(action))+"&reset="+encodeURIComponent(JSON.stringify(reset)) );
			}
			</script>
JS;
		return $js;
	}
}
