
<?php
session_start ();
ini_set ( "display_errors", "0" );
require_once (__DIR__ . "/core_functions.php");
require_once (__DIR__ . "/inventory_functions.php");
require_once (__DIR__ . "/action_tracker.php");
require_once ("/home/poslavu/private_html/rs_upload.php");
require_once ("/home/poslavu/public_html/admin/lib/modules.php");
require_once (__DIR__ . "/dietary_info_functions.php");

echo "<head>";
echo "<link rel='stylesheet' type='text/css' href='../styles/styles.css' />";
echo "</head>";
echo "<script type='text/javascript' src='//ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js'> </script>";
	$location_info = sessvar("location_info");
	$active_language_pack = getLanguageDictionary(sessvar("locationid"));
	$modules = getActiveModules(sessvar('modules'),sessvar('package'),sessvar('isLLS'));
	$toprint = "";
	
	if(empty($location_info)){return;
}
	function get_profile_list($loc_id, $type, $subtype) {


		$profile_list = array();
		$ntpval = "";
		if ($subtype == "item") {
			$profile_list[] = array(speak("Category Default"),
			""
		);$ntpval = "0";
		}
		if ($type == "tax") {
			$profile_list[] = array(speak("No tax profile"), $ntpval
		);	$get_profiles = mlavu_query("SELECT `id`, `title` FROM `[1]`.`tax_profiles` WHERE `loc_id` = '[2]' AND `_deleted` = '0' ORDER BY `title` ASC", admin_info("database"), $loc_id);
		} else if ($type == "price_tier") {
			$profile_list[] = array(speak("No price tier profile"), $ntpval
		);	$get_profiles = mlavu_query("SELECT `id`, `value` AS `title` FROM `[1]`.`config` WHERE `location` = '[2]' AND `type` = 'price_tier_profile' AND `_deleted` = '0' ORDER BY `value` ASC", admin_info("database"), $loc_id);
		}
		while ($get_profiles !== FALSE && $info = mysqli_fetch_assoc($get_profiles)) {
			$profile_list[] = array($info['title'], $info['id']
		);
}
		return $profile_list;
	}



	function forcedModifierType($fmod_group_id) {

		$fmod_type = "";

		if (substr($fmod_group_id, 0, 1) == "f") {
			$list_id = trim($fmod_group_id, " f");
			$check_forced_mods = lavu_query("SELECT `type` FROM `forced_modifier_lists`	WHERE `id` = '[1]'", $list_id);
			if ($check_forced_mods !== FALSE) {
				if (mysqli_num_rows($check_forced_mods) > 0) {
					$mod_info = mysqli_fetch_assoc($check_forced_mods);
					$fmod_type = $mod_info['type'];
				}
			}
		}

		return $fmod_type;
	}

	function includeExtensionOptions($gift_or_loyalty, &$special_options) {

		$ext_designator = str_replace("_card", "_extension:", $gift_or_loyalty);
		if (in_array($ext_designator, array("gift_extension:", "loyalty_extension:"
			) )) {
			$check_payment_types = lavu_query("SELECT `type`, `special` FROM `payment_types` WHERE `special` LIKE '".$ext_designator."%' AND `_deleted` = '0'");
			if ($check_payment_types !== FALSE) {
				if (mysqli_num_rows($check_payment_types) > 0) {
					$info = mysqli_fetch_assoc($check_payment_types);
					$special_options[] = array($info['type'], $info['special']
				);
			}
		}
	}
		}
	function create_tab_area($tabs,$tab_content,$tab_script,$selected_tab,$extras=false){

		global $toprint;
		$tab_buttons = "";
		$tab_script_names = "";
		$tab_script_selected = "";
		$tab_area = "";
		if(!$extras) $extras = array();
		for($i=0; $i<count($tabs); $i++){

			$tab_title = $tabs[$i];
			$tab_name = str_replace(" ","_",strtolower($tab_title));
			$tab_area_name = "tab_" . $tab_name . "_area";
			$tab_btn_name = "tab_" . $tab_name . "_btn";

			if($tab_title==$selected_tab){

				$setbgcolor = "#f2f2f2";
				$area_display = "block";
			}
			else{

				$setbgcolor = "#d2d2d2";
				$area_display = "none";
			}
			if(count($tabs) > 1){

				$tab_buttons .= "<td style='border:solid 2px #aaaaaa; cursor:pointer' bgcolor='$setbgcolor' onclick='pedit_display_area(\"$tab_area_name\")' id='$tab_btn_name'>";
				$tab_buttons .= $tab_title;
				$tab_buttons .= "</td>";
			}

			if($tab_script_names!="") $tab_script_names .= ",";
			$tab_script_names .= "new Array('$tab_area_name','$tab_btn_name',\"$tab_title\")";
			if(isset($tab_content[$tab_name]))
				$set_tab_content = $tab_content[$tab_name];
			else
				$set_tab_content = "&nbsp;";
			if(isset($tab_script[$tab_name])){

				$tab_script_selected .= "if(areaid=='$tab_area_name') { " . $tab_script[$tab_name] . " } ";
			}
			$tab_area .= "<div id='$tab_area_name' style='display:$area_display'>".$set_tab_content."</div>";
		}
		if(isset($extras['tab_right_content'])){

			$tab_buttons .= "<td width='30'>&nbsp;</td><td>".$extras['tab_right_content']."</td>";
		}

		$str = "";
		$str .= "<input type='hidden' name='tab_selected' id='tab_selected' value='$selected_tab'>";
		$str .= "<script type='text/javascript'>";
		$str .= "parea_list = new Array($tab_script_names); ";
		$str .= "function pedit_display_area(areaid) { ";
		$str .= "  for(i=0; i<parea_list.length; i++) { ";
		$str .= "    parea_name = parea_list[i][0]; ";
		$str .= "    pbtn_name = parea_list[i][1]; ";
		$str .= "    parea_title = parea_list[i][2]; ";
		$str .= "    if(parea_name==areaid) { ";
		$str .= "       set_bgcolor = '#f2f2f2'; set_display = 'block'; ";
		$str .= "       document.getElementById('tab_selected').value = parea_title; ";
		$str .= "       $tab_script_selected ";
		$str .= "    } else { ";
		$str .= "       set_bgcolor = '#d2d2d2'; set_display = 'none'; ";
		$str .= "    } ";
		$str .= "    document.getElementById(pbtn_name).bgColor = set_bgcolor; ";
		$str .= "    document.getElementById(parea_name).style.display = set_display; ";
		$str .= "  } ";
		$str .= "} ";
		$str .= "</script>";
		$str .= "<table cellpadding=4>";
		$str .= $tab_buttons;
		$str .= "</table>";
		$str .= $tab_area;
		return $str;
	}
	function uvar($str, $def=false){

		if(isset($_GET[$str])) return $_GET[$str]; else return $def;
	}
	function strvar($str){

		return strtolower(str_replace(" ","_",$str));

	}
	$toprint .= "<body style='background-color: transparent;color:white;' >";
	$toprint .= "<style>";
	$toprint .= "body, table {font-family:Verdana,Arial; font-size:12px;} ";
	$toprint .= "</style>";

	$mode = (isset($_GET['mode']))?$_GET['mode']:"";
	$setdir = (isset($_GET['dir']))?$_GET['dir']:false;
	$inv_main_size = array(480,320);
	$inv_thumb_size = array(120,80);

if ($mode == "timecard") {
	$in_lavu = true;
	$rdb = admin_info ( "database" );
	lavu_connect_byid ( admin_info ( "companyid" ), $rdb );

	$locationid = sessvar ( "locationid" );
	if ($locationid) {
		require_once (dirname ( __FILE__ ) . "/../areas/reports/time_cards.php");
	}
} else if ($mode == "subarea") {
	$area = (isset ( $_GET ['area'] )) ? $_GET ['area'] : false;
	$toprint .= "subarea for $area";
	if ($area) {
		$in_lavu = true;
		$rdb = admin_info ( "database" );
		lavu_connect_byid ( admin_info ( "companyid" ), $rdb );

		$locationid = sessvar ( "locationid" );

		require_once (dirname ( __FILE__ ) . "/../areas/" . $area . ".php");
	}
	}else if($mode=="set_inventory_picture"){

		if(!$setdir) exit();
		$set_ers_path = $setdir;
		$updateid = uvar("updateid");
		$category_name=uvar("category");

		$category_id = uvar ( "category_id" );  
		$display_main_dir = $set_ers_path . "main/";
		$display_full_dir = $set_ers_path . "full/";
		$display_thumb_dir = $set_ers_path . "";

		$main_dir = $_SERVER['DOCUMENT_ROOT'] . $display_main_dir;
		$full_dir = $_SERVER['DOCUMENT_ROOT'] . $display_full_dir;
		$thumb_dir = $_SERVER['DOCUMENT_ROOT'] . $display_thumb_dir;

		if(!is_dir($thumb_dir)){
			if(!is_dir($_SERVER['DOCUMENT_ROOT'] . "/images"))
			{
      	mkdir($_SERVER['DOCUMENT_ROOT'] . "/images");
				chmod($_SERVER['DOCUMENT_ROOT'] . "/images", 0775);
			}
			if(!is_dir($_SERVER['DOCUMENT_ROOT'] . "/images/".admin_info("dataname")))
			{
        mkdir($_SERVER['DOCUMENT_ROOT'] . "/images/".admin_info("dataname"));
				chmod($_SERVER['DOCUMENT_ROOT'] . "/images/".admin_info("dataname"), 0775);
			}
			mkdir($thumb_dir,0755);
			if(!is_dir($main_dir)){
				mkdir($main_dir,0755);
			}
			if(!is_dir($full_dir)){
				mkdir($full_dir,0755);
			}
		}

		$tabs = array("Main Picture"
	);	
		$selected_tab = reqvar("tab_selected","Main Picture");
		$selected_tabname = strvar($selected_tab);

		$tab_pictures = array();
		for($x=0; $x<count($tabs); $x++){

			$tabname = strvar($tabs[$x]);
			$tab_pictures[$tabname] = reqvar($tabname);
		}
		$set_misc_content = reqvar('misc_content');

		$update_picture = false;
		$update_message = "";
		if(isset($_GET['setfile'])){
			$update_picture = urldecode($_GET['setfile']);
		}
		else if(isset($_FILES['picture'])){
			$update_picture = false;
			$basename = basename($_FILES['picture']['name']);
			$basename_parts = explode(".",$basename);
			if(count($basename_parts) > 1){
				$basename_ext = strtolower($basename_parts[count($basename_parts)-1]);
				$allowed_ext = array("gif","jpg","jpe","jpeg","png");	
				$is_allowed = false;
				for($x=0; $x<count($allowed_ext); $x++){
					if($allowed_ext[$x]==$basename_ext)
						$is_allowed = true;
				}
				if($is_allowed){
					$uploadfile = $full_dir . $basename;
					if(rs_move_uploaded_file($_FILES['picture']['tmp_name'], $uploadfile, $basename)){
						chmod($uploadfile,0775);
						$mainfile = $main_dir . $basename;
						$convertcmd = "convert ".lavuShellEscapeArg($uploadfile)." -thumbnail ".$inv_main_size[0]."x".$inv_main_size[1]."> ".lavuShellEscapeArg($mainfile);
						lavuExec($convertcmd);
						chmod($mainfile,0775);
						$thumbfile = $thumb_dir . $basename;
						$convertcmd = "convert ".lavuShellEscapeArg($uploadfile)." -thumbnail ".$inv_thumb_size[0]."x".$inv_thumb_size[1]."> ".lavuShellEscapeArg($thumbfile);
						lavuExec($convertcmd);
						chmod($thumbfile,0775);
					}
					$update_picture = $basename;
				}
				else{
					$update_message = "<b>This file supplied is not supported.  Please upload a gif, jpg, or png image.</b><br>";
				}
			}
		}
		if($update_picture){

			if($update_picture=="none")
				$tab_pictures[$selected_tabname] = "";
			$tab_pictures[$selected_tabname] = $update_picture;
		}
		$selected_picture = $tab_pictures[$selected_tabname];
		$fwd_picture_vars = "&dir=".$setdir;
		for($x=0; $x<count($tabs); $x++){

			$tabname = strvar($tabs[$x]);
			$fwd_picture_vars .= "&".$tabname."=".urlencode($tab_pictures[$tabname]);
		}

		$tab_content = array();
		$tab_script = array();
		$awsDataId = isset($awsResponse) && isset($awsResponse['storage_key']) ? $awsResponse['storage_key'] : '';
		for($i=0; $i<count($tabs); $i++){

			$tabname = strtolower(str_replace(" ","_",$tabs[$i]));
			$set_tab_picture = (isset($tab_pictures[$tabname]))?$tab_pictures[$tabname]:false;
			if($set_tab_picture && $set_tab_picture!="" && $set_tab_picture!="none"){

				$new_picture_display = $set_ers_path . $set_tab_picture;
				$tab_content[$tabname] = "<img src='" . $new_picture_display . "' />";
				$tab_script[$tabname] = "document.getElementById('file_selector').value = \"$set_tab_picture\"; ";
			}
			else
				$tab_script[$tabname] = "document.getElementById('file_selector').value = \"\"; ";
			$tab_script[$tabname] .= "document.getElementById('picture_controls').style.display = 'block'; ";
		}

		$extra_apply_code = "";
		$extra_onchange_code = "";

		$apply_main_picture = $tab_pictures['main_picture'];
		if(isset($tab_pictures['picture_2'])){

			$apply_picture2 = $tab_pictures['picture_2'];
			$extra_apply_code .= ",\"$apply_picture2\"";
		}
		if(isset($tab_pictures['picture_3'])){

			$apply_picture3 = $tab_pictures['picture_3'];
			$extra_apply_code .= ",\"$apply_picture3\"";
		}
		$new_picture_display = $set_ers_path . $apply_main_picture;

		// get a list of all available pictures
		$piclist = array();
		if ($handle = opendir($thumb_dir)) {
			while (false !== ($file = readdir($handle))) {
				if ($file != "." && $file != "..") {
					if(is_file($thumb_dir . "/" . $file))
						$piclist[] = $file;
				}
			}
			closedir($handle);
		}
		sort($piclist);

		// get the tabs
		$tab_extras = array();
		$tab_extras['tab_right_content'] = "<input type='button' value='Apply' onclick='picture_apply()'>";
		$tab_text = create_tab_area($tabs,$tab_content,$tab_script,$selected_tab,$tab_extras);

		if (isset($_GET['view_grid']) && $_GET['view_grid'] == '1')
			$toprint .= set_inventory_picture_grid_tohtml($set_ers_path, $piclist, $fwd_picture_vars, $selected_picture, $updateid, $extra_apply_code);
		else
			$toprint .= set_inventory_picture_tohtml($extra_apply_code, $fwd_picture_vars, $tab_text, $extra_onchange_code, $piclist, $update_message, $selected_picture, $updateid,$category_name, $category_id);

		$toprint .= "<script type='text/javascript'>
			function picture_apply() {
				window.parent.set_inv_picture(\"" . $updateid . "\",\"" . $apply_main_picture . "\",\"" . $new_picture_display . "\",\"" . $extra_apply_code . "\",false , false,\"" . $awsDataId . "\");
			}
		</script>";
}
else if ($mode == "set_special") {
    
	$item_name = uvar ( "item_name" );
	$item_title = uvar ( "item_title" );
	$item_value = uvar ( "item_value" );
	$item_edit = uvar ( "item_edit" );
	$item_extra1 = uvar ( "extra1" ); // row id for menu_categories, menu_items
	$item_extra2 = uvar ( "extra2" ); // forced_modifier_group_id for menu_items
	$row_name = uvar ( "rowname" );
	$row_name = urldecode($row_name);
	$type = uvar("type");
	$special_table = false;
	$special_cond = "";
	$dfields = array ();

	$profile_list = array ();

	$lavu_only = "<font color='#AECD37'><b>*</b></font> ";

	if ($item_title == speak ( "Details" )) {

		$locationid = sessvar ( "locationid" );
		$rdb = admin_info ( "database" );
		lavu_connect_byid ( admin_info ( "companyid" ), $rdb );

		$edit_type = "form";
		$special_table = "menu_items";
		$special_title = speak ( "Details" ) . " - " . $row_name . "&nbsp";
		$special_subtitle = (is_numeric ( $item_extra1 ) && $item_extra1 > 0) ? speak ( "Item ID" ) . ": " . $item_extra1 : "";

		$printer_list = array ();
		$printer_list [] = array ("Category Default","0");
		$printer_list [] = array ("No Printer","");
		$pr_query = lavu_query ( "select * from `config` where `location`='[1]' and `type`='printer' and `_deleted`!='1' ORDER BY `value2` ASC", $locationid );
		while ( $pr_read = mysqli_fetch_assoc ( $pr_query ) ) {

			if (strtolower ( substr ( $pr_read ['setting'], 0, 7 ) == "kitchen" )) {

				$k = substr ( $pr_read ['setting'], 7 );
				if ($k == "")
					$k = 1;
				$printer_list [] = array ($pr_read ['value2'],$k);
			}
		}
		$pgroup_query = lavu_query ( "select * from `config` where `location` = '[1]' AND `type`='printer_group' AND `_deleted` != '1' ORDER BY `setting` ASC", $locationid );
		while ( $pgroup_read = mysqli_fetch_assoc ( $pgroup_query ) ) {

			$printer_list[] = array(
				$pgroup_read ['setting'],
				$pgroup_read ['id'] + 1000
			);
		}
		$super_group_list = array(
			array(
				speak( "Category Default", "" )
			)
		);

		$sgroup_query = lavu_query ( "select * from `super_groups` where `_deleted` = '0' order by `_order` asc" );
		while ( $sgroup_read = mysqli_fetch_assoc ( $sgroup_query ) ) {

			$super_group_list [] = array (
					$sgroup_read ['title'],
					$sgroup_read ['id']
			);
		}
		$special_options = array ();
		$special_options [] = array ("","");
		$gift_or_loyalty = forcedModifierType ( $item_extra2 );
		if (in_array ( $gift_or_loyalty, array (
				"gift_card",
				"loyalty_card"
		) )) {
			includeExtensionOptions ( $gift_or_loyalty, $special_options );
		}

		$special_options [] = array (
				"Weigh Multiple Items Tare",
				"wmi_tare"
		); // hidden_value = tare value with unit (i.e., 4.5 oz); hidden_value3 = secondary item id to which remaining weight is applied
		if ($modules->hasModule ( "customers.billing" )) {
			$special_options [] = array (
					speak ( "Recurring Billing Item" ),
					"recurring_item"
			); // hidden_value = amount to use for future recurring billing invoices
		}
		if ($location_info ['component_package_code'] == "lavukart") {
			$special_options [] = array (
					speak ( "Facility Buyout" ),
					"facility buyout"
			); // hidden_value = duration of buyout in minutes
			$special_options [] = array (
					speak ( "Membership" ),
					"membership"
			); // hidden_value = numer of race credit to apply
			$special_options [] = array (
					speak ( "Race Credits" ),
					"race credits"
			); // hidden_value = number of race credits to apply
			$special_options [] = array (
					speak ( "Race Sequence" ),
					"race sequence"
			); // hidden_value = event sequence title
			$special_options [] = array (
					speak ( "Room Rental" ),
					"group event"
			); // hidden_value = duration of event in minutes
			$special_options [] = array (
					speak ( "Track Rental" ),
					"track rental"
			); // hidden_value = duration of rental in minutes
			$special_options [] = array (
					speak ( "Liquor Sales" ),
					"liquor sales"
			);
		}
		$dfields [] = array (
				"description",
				"textarea",
				speak ( "Description" )
		);
		$dfields[] = array (
				"kitchen_nickname",
				"text",
				speak ( "Kitchen Nickname" )
		);
		$dfields [] = array (
				"UPC",
				"text",
				speak ( "UPC" )
		);
		if ($location_info ['display_product_code_and_tax_code_in_menu_screen']) {
			$dfields [] = array (
					"product_code",
					"text",
					speak ( "Product Code" )
			);
			$dfields [] = array (
					"tax_code",
					"text",
					speak ( "Tax Code" )
			);
		}
		$dfields [] = array (
				"active",
				"select",
				speak ( "Display in App" ),
				array (
						array (
								speak ( "Category Default" ),
								"1"
						),
						array (
								speak ( "No" ),
								"0"
						)
				)
		);
		if ($location_info ['use_lavu_togo'])
			$dfields [] = array (
					"ltg_display",
					"select",
					"Display in Lavu ToGo",
					array (
							array (
									"Category Default",
									"1"
							),
							array (
									"No",
									"0"
							)
					)
			);
		if ($modules->hasModule("extensions.loyalty.pepper"))
		{
			$dfields[] = array(
				"loyalty_display",
				"select",
				"Display in Loyalty App",
				array(
					array(
						"Category Default",
						"1"
					),
					array(
						"No",
						"0"
					)
				)
			);
		}
		$dfields [] = array (
				"quick_item",
				"select",
				speak ( "Include in Quick Item List" ),
				array (
						array (
								speak ( "No" ),
								"0"
						),
						array (
								speak ( "Yes" ),
								"1"
						)
				)
		);
		$dfields [] = array (
				"print",
				"select",
				speak ( "Print" ),
				array (
						array (
								speak ( "Yes" ),
								"1"
						),
						array (
								speak ( "No" ),
								"0"
						)
				)
		);
		$dfields [] = array (
		    "display_on_digital_menu_board",
		    "select",
		    speak ( "Display on Digital Menu Board" ),
		    array (
		        array (
		            speak ( "Yes" ),
		            "1"
		        ),
		        array (
		            speak ( "No" ),
		            "0"
		        )
		    )
		);
		$dfields [] = array (
				"printer",
				"select",
				speak ( "Printer" ),
				$printer_list,
				"Default:0"
		);
		if (admin_info ( "lavu_admin" ) == 1)
			$dfields [] = array (
					"apply_taxrate",
					"select",
					$lavu_only . speak ( "Apply Tax Rate" ),
					array (
							array (
									speak ( "Default" ),
									"Default"
							),
							array (
									speak ( "Custom" ),
									"Custom"
							)
					)
			);
		if (admin_info ( "lavu_admin" ) == 1)
			$dfields [] = array (
					"custom_taxrate",
					"text",
					$lavu_only . speak ( "Custom Tax Rate" )
			);
		$dfields [] = array (
				"tax_profile_id",
				"select",
				speak ( "Tax Profile" ),
				get_profile_list ( $locationid, "tax", "item" )
		);
		$dfields [] = array (
				"allow_ordertype_tax_exempt",
				"select",
				speak ( "Allow Tax Exemption" ),
				array (
						array (
								speak ( "Category Default" ),
								"Default"
						),
						array (
								speak ( "Yes" ),
								"1"
						),
						array (
								speak ( "No" ),
								"0"
						)
				)
		);
		$dfields [] = array (
				"tax_inclusion",
				"select",
				speak ( "Tax included in price" ),
				array (
						array (
								speak ( "Category Default" ),
								"Default"
						),
						array (
								speak ( "No" ),
								"0"
						),
						array (
								speak ( "Yes" ),
								"1"
						)
				)
		);
		$dfields [] = array (
				"no_discount",
				"select",
				speak ( "Allow Discounts" ),
				array (
						array (
								speak ( "Category Default" ),
								""
						),
						array (
								speak ( "Yes" ),
								"0"
						),
						array (
								speak ( "Amount Discounts Only" ),
								"2"
						),
						array (
								speak ( "Percentage Discounts Only" ),
								"3"
						),
						array (
								speak ( "No" ),
								"1"
						)
				)
		);
		$dfields [] = array (
				"allow_deposit",
				"select",
				speak ( "Allow Deposit" ),
				array (
						array (
								speak ( "No" ),
								"0"
						),
						array (
								speak ( "Yes" ),
								"1"
						)
				)
		);
		$dfields [] = array (
				"open_item",
				"select",
				speak ( "Open Item" ),
				array (
						array (
								speak ( "No" ),
								"0"
						),
						array (
								speak ( "Name/Price" ),
								"1"
						),
						array (
								speak ( "Price Only" ),
								"2"
						)
				)
		);
		
		$has_sacoa_cards = $modules->hasModule("extensions.payment.sacoa");
		if($has_sacoa_cards)
		{
			$dfields[] = array("currency_type","select",speak("Currency Type"),array(array(speak("Currency"),"0"),array(speak("Points"),"1")));
		}
		
		$dfields [] = array (
				"hidden_value2",
				"select",
				speak ( "Special" ),
				$special_options
		);
		$dfields [] = array (
				"hidden_value",
				"text",
				speak ( "Special Details 1" )
		);
		$dfields [] = array (
				"hidden_value3",
				"text",
				speak ( "Special Details 2" )
		);
		if ($modules->hasModule ( "dining.price_tiers" ))
			$dfields [] = array (
					"price_tier_profile_id",
					"select",
					"Price tier profile",
					get_profile_list ( $locationid, "price_tier", "item" )
			);
		$dfields [] = array (
				"super_group_id",
				"select",
				"Super Group Id",
				$super_group_list
		);
		$dfields [] = array (
				"happyhour",
				"happyhour",
				"Happy Hour"
		);
		$dfields[] = array("heart_healthy","select",speak("Heart Healthy"),array(array(speak("Yes"),"1"),array(speak("No"),"0")));
		$dfields[] = array("contains_gluten","select",speak("Contains Gluten"),array(array(speak("Yes"),"1"),array(speak("No"),"0")));
		$dfields[] = array("contains_dairy","select",speak("Contains Dairy"),array(array(speak("Yes"),"1"),array(speak("No"),"0")));
		if ($modules->hasModule ( "dining.86countdown" )) {
			// TODO:- Removed this if else condition after full migration implementation
			if (sessvar ( "INVENTORY_MIGRATION_STATUS" ) == "Completed_Ready" || sessvar ( "INVENTORY_MIGRATION_STATUS" ) == "Skipped_Ready") {
				$dfields [] = array (
						"track_86_count",
						"select",
						"Track 86 countdown",
						array (
								array (
										speak ( "Don't track" ),
										""
								),
								array (
										"Track by Menu Item Level",
										"86count"
								),
								array (
										"Track by Inventory Item Level",
										"86countInventory"
								)
						)
				); // ,array("Track by ingredients","ingredients")
			} else {
				$dfields [] = array (
						"track_86_count",
						"select",
						"Track 86 countdown",
						array (
								array (
										speak ( "Don't track" ),
										""
								),
								array (
										"Track by 86 count",
										"86count"
								)
						)
				); // ,array("Track by ingredients","ingredients")
			}
			$dfields [] = array (
					"inv_count",
					"number",
					"86 count"
			);
			$dfields[] = array("dining_option","select",speak("Dine in type"),array(array(speak("Pickup"),"Pickup"),array(speak("Delivery"),"Delivery"),
					array(speak("Dine-In"),"Dine-In"),
					array(speak("Pickup & Delivery"),"Pickup,Delivery"),
					array(speak("Pickup & Dine-In"),"Pickup,Dine-In"),
					array(speak("Delivery & Dine-In"),"Delivery,Dine-In"),
					array(speak("Pickup,Delivery & Dine-In"),"Pickup,Delivery,Dine-In")
			));
		}
		if ($modules->hasModule("extensions.loyalty.pepper"))
		{
			$dfields[] = array("loyalty_points", "text", speak("Loyalty Points"));
		}
	}else if ($item_title == speak("Category Details")){

			$locationid = sessvar("locationid");
			$rdb = admin_info("database");
			lavu_connect_byid(admin_info("companyid"),$rdb);

			$edit_type = "form";
			$special_table = "menu_items";
			$special_title = speak("Category Details")." - " . $row_name;

			$print_order = array();
			for ($i = 1; $i <= 20; $i++) {
				$print_order[] = array($i, $i);
			}

			$dfields[] = array("description","textarea",speak("Description"));
			$dfields[] = array("active","select",speak("Display in App"),array(array(speak("Yes"),"1"),array(speak("No"),"0")));
			if ($location_info['use_lavu_togo']) $dfields[] = array("ltg_display","select","Display in Lavu ToGo",array(array("Yes","1"),array("No","0")));
			if ($modules->hasModule("extensions.loyalty.pepper")) $dfields[] = array("loyalty_display","select","Display in Loyalty App",array(array("Yes","1"),array("No","0")));
			$dfields[] = array("print","select",speak("Print"),array(array(speak("Yes"),"1"),array(speak("No"),"0")));
			$dfields[] = array("display_on_digital_menu_board","select",speak("Display on Digital Menu Board"),array(array(speak("Yes"),"1"),array(speak("No"),"0")));
			$dfields[] = array("print_order","select",speak("Print Order"),$print_order);
			$dfields[] = array("_order","text",speak("Display Order"));
			if (admin_info("lavu_admin") == 1) $dfields[] = array("apply_taxrate","select",$lavu_only.speak("Apply Taxrate"),array(array(speak("Default"),"Default"),array(speak("Custom"),"Custom")));
			if (admin_info("lavu_admin") == 1) $dfields[] = array("custom_taxrate","text",$lavu_only.speak("Custom Taxrate"));
			$dfields[] = array("enable_reorder","select",speak("Enable Reorder"),array(array(speak("Yes"),"1"),array(speak("No"),"0")));
			$dfields[] = array("allow_ordertype_tax_exempt","select",speak("Allow Tax Exemption"),array(array(speak("Yes"),"1"),array(speak("No"),"0")));
			$dfields[] = array("tax_profile_id","select",speak("Tax Profile"),get_profile_list($locationid, "tax", "category"));
			$dfields[] = array("tax_inclusion","select",speak("Tax included in prices"),array(array(speak("Location Default"),"Default"),array(speak("No"),"0"),array(speak("Yes"),"1")));
			$dfields[] = array("no_discount","select",speak("Allow Discounts"),array(array(speak("Yes"),"0"),array(speak("Amount Discounts Only"),"2"), array(speak("Percentage Discounts Only"),"3"), array(speak("No"),"1")));
			if ($modules->hasModule("dining.price_tiers"))
				$dfields[] = array("price_tier_profile_id", "select", "Price tier profile", get_profile_list($locationid, "price_tier", "category"));
			$super_group_list = array(array("",""));
			$sgroup_query = lavu_query("select * from `super_groups` where `_deleted` = '0' order by `_order` asc");
			while($sgroup_read = mysqli_fetch_assoc($sgroup_query)){
				$super_group_list[] = array($sgroup_read['title'],$sgroup_read['id']);
			}
			$dfields[] = array("super_group_id","select","Super Group Id",$super_group_list);

			/****************TODO **************/
			//$dfields[] = array("copy_move","select","Copy/Move");
			//$dfields[] = array("copy_move","select","To");

			$dfields[] = array("happyhour", "happyhour_category", "Happy Hour"
		);	//$dfields[] = array("open_item","select","Open Item",array(array("No","0"),array("Yes","1")));
			//$dfields[] = array("hidden_value2","text","Special");
			//$dfields[] = array("hidden_value","text","Special Details 1");
			//$dfields[] = array("hidden_value3","text","Special Details 2");
			//$dfields[] = array("allow_deposit","select","Allow Deposit",array(array("No","0"),array("Yes","1")));*/
		}
		else if ($item_title == speak("Ingredients") || $type == "ingredients") {

			$edit_type = "form";
			$special_table = "menu_items";
			$special_title = "Ingredients - " . $row_name;
			$dfields[] = array("ingredients","ingredients","Ingredients");
			$showVal = speak('Ingredient');
            $showVals = speak('Ingredients');
		}else if ($item_title == speak("Nutrition") || $type == "nutrition") {
			$edit_type = "form";
			$special_table = "menu_items";
			$special_title = "Nutrition - " . $row_name;
			$dfields[] = array("nutrition","nutrition","Nutrition");
		}else if ($item_title == speak("Combo Items") || $type == "comboitems"){

			$edit_type = "form";
			$special_table = "menu_items";
			$special_title = "Combo Items - " . $row_name;
			$dfields[] = array("comboitems","comboitems","comboitems");

		} else if ($item_title == speak("Dietary Info")) {
			$edit_type = "form";
			$special_table = "menu_meta_data";
			$special_title = "Dietary Info - " . $row_name;
			$dfields[] = array("dietary_info","dietary_info","Dietary Info");

		}else if ($item_title == speak("Discount Details")){//------------------------------------
		    global $ifNotPassbook_js;
		    $edit_type = "form";
			$special_table = "discount_types";

			$special_title = speak("Discount Details")." - " . $row_name;

			$item_discount_id = str_replace("details","special",$item_name);
			$get_discount_type_js = "discount_type_var = window.parent.document.getElementById('$item_discount_id').value;";

			//Note as messy as this is there's no way in php to see what the discount type is, we can only detect it JavaScript after the fact
			//  and then remove the elements which we previously added using php.
			//  --We do this by removing all tables following the first one which is the header.
			$ifNotPassbook_js  = " <script type='text/javascript'> discount_type_var = window.parent.document.getElementById('$item_discount_id').value;
			                         if(discount_type_var != 'passbook') {
			                             var allElems = document.getElementsByTagName(\"table\");
			                             for(var k = 1; k < allElems.length; k++){
			                                 allElems[k].parentNode.removeChild(allElems[k--]);
			                             }
			                             document.write('There are no detail options for this discount type.');
			                         }
			                         else{

			                         }
			                      </script>";


			// params:                                      // These will only be used if empty
    		// array(yearsBeforeThisYear, yearsAfterThisYear, yearPosition, default to todays date);
    		// note that if defaultToTodaysDate is set, then yearPosition will naturally be overridden to 0.
    		//                  table           column_name           type           label     params.
    		//The last array has 4 entries: (years prior to this year, years after this year, start position of select, default to todays date).
    		//The last two fields will be overridden if data is pulled from the database, it will be defaulted to that value then.


    		////$dfields[] = array("valid_start_date_inc","date_selector", "Begin Sending",    array(0,  10,  0, true));
    		////$dfields[] = array("valid_end_date_inc",  "date_selector", "End Sending",      array(0,  10,  0, true));
    		$dfields[] = array("expiration_date", "date_selector", "Expiration Date",  array(0,  10,  0, true
)
		);
	}	if($edit_type=="form"){

			if (!empty($special_title)) $toprint .= "<table class='flex' cellpadding=6>
				<tr>
					<td width='650px' bgcolor='#777777' class='flex' style='justify-content: center;'>
						<span class='headerTxt'>" . $special_title . "</span>";
		if (! empty ( $special_subtitle ))
			$toprint .= "<br><span class='headerTxt' style='justify-content: center;'>" . $special_subtitle . "</span>";
		if (! empty ( $special_title ))
			$toprint .= "
					</td>
				</tr>
			</table>";


			if (!empty($special_subtitle)) $toprint .= "<tr></tr>";

			function drow_code($dtitle,$dcode)
			{

				if($dtitle=='calories'|| $dtitle=='proteins' || $dtitle=='sodium' || $dtitle=='carbs' || $dtitle=='fat'
						||  $dtitle=='saturated_fat' || $dtitle=='fiber' || $dtitle=='sugar'){

				$str = "
					<tr>
						<td align='right' valign='top'>";
			if (strlen ( $dtitle ) > 0)
				$str .= "
							" . $dtitle;
			$str .= "
						</td>
						<td>";
			if (strlen ( $dcode ) > 0)
				$str .= "
							" . $dcode;
			$str .= "<select>
									<option value='gm'>mg</option>
									</select>
						</td>
					</tr>";
									return $str;
				}
				else {
					$str = "
					<tr>
						<td align='right' valign='top'>";
					if (strlen($dtitle) > 0)
						$str .= "
							".$dtitle;
						$str .= "
						</td>
						<td>";
						if (strlen($dcode) > 0)
							$str .= "
							".$dcode;
							$str .= "
						</td>
					</tr>";
			return $str;
		}}

		// used by the javascript to retrieve the catagory id
		$catagoryid_index_parentid = "";
		$itemid = "";
		{
			$itemcatagoryid = str_replace ( "details", "categoryid", $item_name );
			$parts = explode ( "_", $itemcatagoryid );
			$catagoryid_index_parentid = $parts [0] . "_" . $parts [1] . "_" . $parts [2];
			$itemid = $parts [3];
		}

		$js_getvals = "";
		$js_setvals = "";
		$toprint .= "
			<form name='dform' id= 'dForm' method='post'>
				<table>";
			for($i=0; $i<count($dfields); $i++){

				$dname = $dfields[$i][0];
				$dtype = $dfields[$i][1];
				$dtitle = (isset($dfields[$i][2]))?$dfields[$i][2]:$dname;
				$dprop = (isset($dfields[$i][3]))?$dfields[$i][3]:false;
				$dval = "";
				if (isset($dfields[$i][4]) && substr($dfields[$i][4], 0, 8)=="Default:") {
					$sep = explode(":", $dfields[$i][4]);
					$dval = $sep[1];
				}
				$get_js_code = "setval = window.parent.document.getElementById('".str_replace("details",$dname,$item_name)."').value; carryValueOrSetDefault(\"".$dname."\" ,document.dform.$dname, setval);";
				$name_on_parent = str_replace("details",$dname,$item_name);
				$set_js_code = "setval = document.dform.$dname.value;window.parent.document.getElementById('".$name_on_parent."').value = setval;";
				if ($dname =="active" || $dname=="ltg_display" || $dname=="loyalty_display") {
					$set_js_code .= "var chkbox = window.parent.document.getElementById('".$name_on_parent."_cb'); if (chkbox) chkbox.checked = (setval == '1');";
				}

				if($dtype=="text"){

					$dval = str_replace('"','&quot;',$dval);
					$toprint .= drow_code($dtitle,"<input type='text' name='$dname' value=\"$dval\">");
				}
				else if($dtype=="number"){

					$toprint .= drow_code($dtitle,"<input type='number' name='$dname' value=\"$dval\">");
				}
				else if($dtype=="date_selector"){

				    ob_start();
				    require_once(dirname(__FILE__) . "/../scripts/dateChooser.php");
				    $toprint .= ob_get_contents();
				    ob_end_clean();

				    //create_date_picker takes the id of the html element whos value will change.
				    $toprint .= "<input type='hidden' id='$dname' name='$dname' value=\"$dval\">";
				    $toprint .= "<br>";
				    $params = $dprop;

				    //We need to set the specific javascript variable for `create_date_picker` 's javascript functions to be able to pick
				    // up the value.
				    $get_js_code .= "date_picker_setval_$dname = setval;";

				    //Note, if `setvar` is in the namespace 'create_date_picker' will use that time as default.
				    $toprint .=  drow_code($dtitle, create_date_picker($dname, $params[0], $params[1], $params[2], $params[3]) );
				    //$toprint .=  create_date_picker($dname, $params[0], $params[1], $params[2], $params[3]);

				}
				else if($dtype=="happyhour" || $dtype=="happyhour_category"){

					$toprint .= '<script type="text/javascript" src="../scripts/jquery.min.1.8.3.js"></script>';
					$toprint .= '<script type="text/javascript" src="/../../manage/js/jquery/js/jquery-confirm.min.js"></script>';
					ob_start();
					require_once(dirname(__FILE__).'/../areas/happyhours/happyhour.js');
					$s_hh_js = ob_get_contents();
					ob_end_clean();
					$toprint .= '<script type="text/javascript">'.$s_hh_js.'</script>';
					$parentid = 'happyhour_parent';
					$val = str_replace("\"","&quot;",$dval);
					$dname_happyhours = $dname . "_happyhours";
					$val_happyhours = array();
					$hh_query = lavu_query("SELECT * FROM `happyhours` WHERE `_deleted` != '1'");
					if ($hh_query) {
						while ($row = mysqli_fetch_assoc($hh_query)) {
							array_push($val_happyhours, $row['start_time'].":".$row['end_time'].":".$row['weekdays'].":".$row['property'].":".$row['id'].":".$row['name'].":".$row['_deleted']);
						}
					}
					$val_happyhours = implode("|", $val_happyhours);
					$category_id_text = "category_id=window.parent.document.getElementById('$catagoryid_index_parentid').value";
					if ($dtype=="happyhour_category")
						$category_id_text = "category_id=null";
					if ($val_happyhours == "")
						$toprint .= drow_code($dtitle, "\n<div id='$parentid'></div><input type='hidden' name='$dname' id='$dname' value=\"$val\"><input type='hidden' name='$dname_happyhours' id='$dname_happyhours' value=\"$val_happyhours\">".speak("There are no happy hours available for this account.")."\n");
					else
						$toprint .= drow_code($dtitle, "\n<div id='$parentid'></div><input type='hidden' name='$dname' id='$dname' value=\"$val\"><input type='hidden' name='$dname_happyhours' id='$dname_happyhours' value=\"$val_happyhours\"><script type='text/javascript'>$category_id_text;setTimeout(function(){ hh_loadhappyhour(\"$parentid\", \"$dname\",\"$dname_happyhours\") }, 30);</script>\n");

				}
				else if($dtype=="textarea"){

					$toprint .= drow_code($dtitle,"<textarea name='$dname' cols='30' rows='3'>$dval</textarea>");
				}
				else if($dtype=="select"){

					$str = "<select name='$dname'>";
					for($s=0; $s<count($dprop); $s++){

						if(is_array($dprop[$s])){

							$sval = isset($dprop[$s][0]) ? $dprop[$s][0] : "";
							$sindex = isset($dprop[$s][1]) ? $dprop[$s][1] : "";
						}
						else{

							$sval = $dprop[$s];
							$sindex = $dprop[$s];
						}
						$sval = str_replace('"','&quot;',$sval);

						$str .= "<option value=\"$sindex\"";
						if ($dval == $sindex) $str .= " selected";
						$str .= ">$sval</option>";
					}
					$str .= "</select>";
					$toprint .= drow_code($dtitle,$str);
				}else if( $dtype=='multi_text'){
					$str= "<input type='text' value='your mom'>";
					$toprint .= drow_code($dtitle, $str);
				}

				else if ($dtype == "dietary_info" && $modules->hasModule("extensions.ordering.kiosk"))
				{
					$database=admin_info("database");
					$item_id = $_GET['item_id'];
					$dietary_settings = get_menu_meta_data();
					$dietary_info = get_dietary_info_checked($database, $item_id);
					if ($item_id !== '0') 
					{
						$toprint .= "<label>Please select the dietary information for this item.</label>\n";
					}
					$toprint .= "<table><tbody>";

					// Do not allow dietary info fields to be entered in popup if the menu item has not been committed to the db yet (because dietary info needs menu_item ID)
					if ($item_id === '0') {
						$toprint .= '<tr><td><div style="text-align:center; margin:2em;">'. speak("Please save your new menu item first") .'</div></td></tr>';
						continue;
					}
					foreach($dietary_settings as $dietary_setting) {
						$meta_data_id = $dietary_setting['id']; // meta_data_id
						$label = $dietary_setting['name'];
						if ($dietary_setting['id'] % 2 == 1 ) {
							$toprint .= "<tr>";
						}
						if ($dietary_setting['id'] == 14) {
							$toprint .= "<tr>";
						}
						if ($dietary_info[$meta_data_id] == 1){
							$toprint .= "<td><input type='checkbox' name='selected_dietary[]' value='$meta_data_id' checked><label>$label</label></td>";
						}
						else {
							$toprint .= "<td><input type='checkbox' name='selected_dietary[]' value='$meta_data_id'><label>$label</label></td>";
						}
						if ($dietary_setting['id'] % 2 == 0 ) {
							$toprint .= "</tr>";
						}
					}
					$toprint .= "</tr></tbody></table>";

					if(isset($_POST['submit_dietary'])) {
						$selected_dietarys = $_POST['selected_dietary'];
						$boolean_dietary_arr = array();
						$item_id = $_GET['item_id'];
						$database = admin_info('database');
						foreach($dietary_settings as $key => $dietary_setting) {
							if (in_array($dietary_setting['id'], $selected_dietarys)) {
								$boolean_dietary_arr[$key] = 1;
							} else {
								$boolean_dietary_arr[$key] = 0;
							}
						}
						foreach ($boolean_dietary_arr as $key => $boolean_arr_instance) {
							$toprint .= drow_code("", 'Key: ' . $key . ' Array Value: ' . $boolean_arr_instance);
						}
						update_dietary_info($database, $boolean_dietary_arr, $item_id);
					}
				}

				else if ($dtype == "nutrition" && $modules->hasModule("extensions.ordering.kiosk"))
				{
					$database=admin_info("database");
					$item_id = $_GET['item_id'];
					$tablename = $_GET['extra2'];
					$standard_nutrition_types = get_standard_nutrition_types();
					$toprint .= "<table><tbody>";

					// Do not allow Nutrition fields to be entered in popup if the menu item has not been committed to the db yet (because Nutrition needs menu_item ID)
					if ($item_id === '0') {
						$toprint .= '<tr><td><div style="text-align:center; margin:2em;">'. speak("Please save your new menu item first") .'</div></td></tr>';
						continue;
					}

					$existing_records = array();
					$dbargs = array('db' => $database, 'source_table' => $tablename, 'source_table_id' => $item_id, 'standard_nutrition_type_id' => $nutrition_type_id, 'value' => $kiosk_nutrition_amounts[$key], 'units' => $kiosk_nutrition_units[$key], 'enabled' => $enabled[$key], 'record_exists' => isset($existing_record[$nutrition_type_id]));
					$record_exists_query = "SELECT nil.id, nil.source_table, nil.source_table_id, ni.id, ni.nutrition_info_link_id as 'nutrition_info_link_id', ni.standard_nutrition_type_id, ni.value, ni.units, ni._deleted FROM [db].`nutrition_info_link` nil JOIN [db].`nutrition_info` ni on nil.id = ni.nutrition_info_link_id WHERE source_table = '[source_table]' and source_table_id = '[source_table_id]'";
					$record_exists = mlavu_query($record_exists_query, $dbargs);
					while ($data = mysqli_fetch_assoc($record_exists))
					{
						$existing_records[$data['standard_nutrition_type_id']] = $data;
					}
					$nutrition_values = array();
					foreach($standard_nutrition_types as $i => $standard_nutrition_type)
					{
						$nutrition_type_id = $standard_nutrition_type['id'];
						$label = $standard_nutrition_type['label'];
						$values = '';
						$selected_g = '';
						$selected_mg = '';
						$_enabled = '';
						$_disabled = '';
						if (isset($existing_records[$nutrition_type_id])) {
							$existing_record = $existing_records[$nutrition_type_id];
							$values = $existing_record['value'];
							if ($existing_record['units'] == 'g') $selected_g = 'selected';
							if ($existing_record['units'] == 'mg') $selected_mg = 'selected';
							if ($existing_record['_deleted'] == '0') $_enabled = 'checked';
							if ($existing_record['_deleted'] == '1') $_disabled = 'checked';
						}
						else {
							$_disabled = 'checked';
							$values = 0;
						}
						if ($label == 'Calories' || $label == 'Calories from Fat'){
							$toprint .= "<tr><td>".$label."</td><td><input type='number' name='kiosk_nutrition_label[]' size='4' value='{$values}'></td><td><select style='background-color:#e0e0e0;' disabled name='kiosk_nutrition_unit[]'><option value='g' {$selected_g}>N/A</option><option value='mg' {$selected_mg}>mg</option></select></td><td><input type='radio' name='".$nutrition_type_id."_enabled_disabled' value='enabled' {$_enabled}><label>Enabled</label><input type='radio' name='".$nutrition_type_id."_enabled_disabled' value='disabled' {$_disabled}><label>Disabled</label></td></tr><tr></tr>";
						}
						else {
							$toprint .= "<tr><td>".$label."</td><td><input type='number' name='kiosk_nutrition_label[]' size='4' value='{$values}'></td><td><select name='kiosk_nutrition_unit[]'><option value='g' {$selected_g}>g</option><option value='mg' {$selected_mg}>mg</option></select></td><td><input type='radio' name='".$nutrition_type_id."_enabled_disabled' value='enabled' {$_enabled}><label>Enabled</label><input type='radio' name='".$nutrition_type_id."_enabled_disabled' value='disabled' {$_disabled}><label>Disabled</label></td></tr>";
						}
						$nutrition_values[$i] = $label;
					}
					$toprint .= "</tbody></table>";
					$kiosk_nutrition_amounts = array();
					$kiosk_nutrition_units = array();
					$kiosk_nutrition_enabled = array();
					$enabled = array();
					if(isset($_POST['submit_kiosk_nutrition'])) 
					{
					 	$kiosk_nutrition_amounts = $_POST['kiosk_nutrition_label'];
					 	$kiosk_nutrition_units = $_POST['kiosk_nutrition_unit'];

					 	foreach($standard_nutrition_types as $key => $standard_nutrition_type) 
					 	{
					 		$label = $standard_nutrition_type['label'];
					 		$nutrition_type_id = $standard_nutrition_type['id'];
					 		$kiosk_nutrition_enabled[$key] = $_POST[$nutrition_type_id.'_enabled_disabled'];

					 		if ($kiosk_nutrition_enabled[$key] == 'enabled')
					 		{
					 			$enabled[$key] = 0;
					 		}
					 		else if ($kiosk_nutrition_enabled[$key] == 'disabled')
					 		{
					 			$enabled[$key] = 1;
					 		}
					 		upsert_nutrition_info(array(
					 			'db' => $database,
					 			'source_table' => $tablename,
					 			'source_table_id' => $item_id,
					 			'standard_nutrition_type_id' => $nutrition_type_id,
					 			'value' => $kiosk_nutrition_amounts[$key],
					 			'units' => $kiosk_nutrition_units[$key],
					 			'enabled' => $enabled[$key],
					 			'record_exists' => isset($existing_records[$nutrition_type_id]),
					 			'nutrition_info_link_id' => $existing_records[$nutrition_type_id]['nutrition_info_link_id'],
					 		));
					 	}
					 }
				}

				else if ($dtype == "nutrition") {
				$rdb = admin_info ( "database" );
				// lavu_select_db($rdb);
				lavu_connect_byid ( admin_info ( "companyid" ), $rdb );
				$locationid = sessvar ( "locationid" );

				$apos = ""; // put the character for apostrophe here
				$ingredient_options = array (
						"<option value=''></option>"
				);
				$ing_query = lavu_query ( "select title from  nutrition_information" );
				$a_replace_chars = array (
						"'",
						'"',
						"\n"
				);
				while ( $ing_read = mysqli_fetch_assoc ( $ing_query ) ) {
					$ingredient_options [] = "<option value='{$ing_read['title']}'>" . $ing_read ['title'] . "</option>";
				}
				$sql = lavu_query ( "select title,units from  nutrition_information" );
				$a_replace_chars = array (
						"'",
						'"',
						"\n"
				);
				while ( $result = mysqli_fetch_assoc ( $sql ) ) {
					$result_options [] = $result;
				}
				$mainarr = array ();
				foreach ( $result_options as $key => $val ) {
					$units = $val ['units'];
					$units = explode ( '|', $units );
					foreach ( $units as $unit ) {
						$mainarr [$val ['title']] [] = $unit;
					}
				}
				$mainarr = json_encode ( $mainarr );
				$get_js_code = "
				" . $dname . "_itemnum = 0;
				function add_more_" . $dname . "(isetval,qsetval,usetval,radval) {
					if(!isetval) isetval = '';
					if(!qsetval) qsetval = '0';
					if(!usetval) usetval = '';
					if(!radval) radval = '';
					var item_options = \"";

				foreach ( $ingredient_options as $s_option ) {
					$get_js_code .= $s_option;
				}
				$get_js_code .= "\";";
				$get_js_code .= "
		
		
					var t = document.createElement('div');
					    t.innerHTML += '<table>';
						t.innerHTML +=  '<tr>';
						  if(isetval=='' || isetval==='calories' || isetval=='proteins' || isetval=='fat' || isetval=='sodium' || isetval=='carbs'
								|| isetval=='sugar' || isetval=='saturated_fat' || isetval=='fiber'){
						t.innerHTML += '<td><select id=\"" . $dname . "_item' + " . $dname . "_itemnum + '\" onchange=\"changecat(this.id,this.value)\">' +item_options + '</select></td>';
						t.innerHTML += '<td><input id=\"" . $dname . "_qty' + " . $dname . "_itemnum + '\" type=\"text\" size=\"3\" value=\"' + qsetval + '\" /></td>';
						t.innerHTML += '<td><select id=\"" . $dname . "_unit' + " . $dname . "_itemnum + '\"><option value=\"' + usetval + '\">'+usetval+'</option></select></td>';
							if(radval==''){
									t.innerHTML += '<td><input name=\"" . $dname . "_enable' + " . $dname . "_itemnum + '\" type=\"radio\"  value=\"' + 1 + '\">Enable</td>';
								    t.innerHTML += '<td><input name=\"" . $dname . "_enable' + " . $dname . "_itemnum + '\" type=\"radio\"  value=\"' + 0 + '\" checked>Disable</td>';
								 }
								if(radval==='1')
								{
								t.innerHTML += '<td><input name=\"" . $dname . "_enable' + " . $dname . "_itemnum + '\" type=\"radio\"  value=\"' + 1 + '\" checked>Enable</td>';
								t.innerHTML += '<td><input name=\"" . $dname . "_enable' + " . $dname . "_itemnum + '\" type=\"radio\"  value=\"' + 0 + '\">Disable</td>';
								}
								if(radval==='0')
								{
								t.innerHTML += '<td><input name=\"" . $dname . "_enable' + " . $dname . "_itemnum + '\" type=\"radio\"  value=\"' + 1 + '\" >Enable</td>';
								t.innerHTML += '<td><input name=\"" . $dname . "_enable' + " . $dname . "_itemnum + '\" type=\"radio\"  value=\"' + 0 + '\" checked>Disable</td>';
								}
			
			
						 }
							  else {
									t.innerHTML += '<td><input id=\"" . $dname . "_item' + " . $dname . "_itemnum + '\" type=\"text\" size=\"10\"  value=\"' + isetval + '\" /></td>';
									t.innerHTML += '<td><input id=\"" . $dname . "_qty' + " . $dname . "_itemnum + '\" type=\"text\" size=\"3\" value=\"' + qsetval + '\" /></td>';
						            t.innerHTML += '<td><input id=\"" . $dname . "_unit' + " . $dname . "_itemnum + '\" type=\"text\" size=\"3\" value=\"' + usetval + '\"></td>';
						             if(radval==''){
									t.innerHTML += '<td><input name=\"" . $dname . "_enable' + " . $dname . "_itemnum + '\" type=\"radio\"  value=\"' + 1 + '\">Enable</td>';
								    t.innerHTML += '<td><input name=\"" . $dname . "_enable' + " . $dname . "_itemnum + '\" type=\"radio\"  value=\"' + 0 + '\" checked >Disable</td>';
								    }
						          if(radval==='1'){
									t.innerHTML += '<td><input name=\"" . $dname . "_enable' + " . $dname . "_itemnum + '\" type=\"radio\"  value=\"' + 1 + '\" checked>Enable</td>';
									t.innerHTML += '<td><input name=\"" . $dname . "_enable' + " . $dname . "_itemnum + '\" type=\"radio\"  value=\"' + 0 + '\">Disable</td>';
								    }
								if(radval==='0'){
									t.innerHTML += '<td><input name=\"" . $dname . "_enable' + " . $dname . "_itemnum + '\" type=\"radio\"  value=\"' + 1 + '\" >Enable</td>';
									t.innerHTML += '<td><input name=\"" . $dname . "_enable' + " . $dname . "_itemnum + '\" type=\"radio\"  value=\"' + 0 + '\" checked>Disable</td>';
								  }
					
						     }
						t.innerHTML += '</tr>';
						t.innerHTML += '</table>';
					document.getElementById('list_" . $dname . "').appendChild(t);
					document.getElementById('" . $dname . "_item' + " . $dname . "_itemnum).value = isetval;
					" . $dname . "_itemnum ++;
			
				}
				function add_more_custom(isetval,qsetval,usetval,radval) {
			
			
					if(!isetval) isetval = '';
					if(!qsetval) qsetval = '0';
					if(!usetval) usetval = 'g';
					if(!radval) radval = '';
					var item_options = \"";

				foreach ( $ingredient_options as $s_option ) {
					$get_js_code .= $s_option;
				}
				$get_js_code .= "\";";
				$get_js_code .= "
					var t = document.createElement('div');
					    t.innerHTML += '<table>';
						t.innerHTML +=  '<tr>';
						t.innerHTML += '<td><input id=\"" . $dname . "_item' + " . $dname . "_itemnum + '\" type=\"text\" size=\"10\"  value=\"' + qsetval + '\" onkeypress=\"return validateNumberNutrition(event);\"/></td>';
						t.innerHTML += '<td><input id=\"" . $dname . "_qty' + " . $dname . "_itemnum + '\" type=\"text\" size=\"3\" value=\"' + qsetval + '\" /></td>';
						t.innerHTML += '<td><input id=\"" . $dname . "_unit' + " . $dname . "_itemnum + '\" type=\"text\" size=\"3\" value=\"' + usetval + '\" /></td>';
						   if(radval==''){
									t.innerHTML += '<td><input name=\"" . $dname . "_enable' + " . $dname . "_itemnum + '\" type=\"radio\"  value=\"' + 1 + '\">Enable</td>';
								    t.innerHTML += '<td><input name=\"" . $dname . "_enable' + " . $dname . "_itemnum + '\" type=\"radio\"  value=\"' + 0 + '\" checked>Disable</td>';
								}
								if(radval==='1')
								{
								t.innerHTML += '<td><input name=\"" . $dname . "_enable' + " . $dname . "_itemnum + '\" type=\"radio\"  value=\"' + 1 + '\" checked>Enable</td>';
								t.innerHTML += '<td><input name=\"" . $dname . "_enable' + " . $dname . "_itemnum + '\" type=\"radio\"  value=\"' + 0 + '\">Disable</td>';
								}
								if(radval==='0')
								{
								t.innerHTML += '<td><input name=\"" . $dname . "_enable' + " . $dname . "_itemnum + '\" type=\"radio\"  value=\"' + 1 + '\" >Enable</td>';
								t.innerHTML += '<td><input name=\"" . $dname . "_enable' + " . $dname . "_itemnum + '\" type=\"radio\"  value=\"' + 0 + '\" checked>Disable</td>';
								}
						t.innerHTML += '</tr>';
						t.innerHTML += '</table>';
					document.getElementById('list_addcustom').appendChild(t);
					document.getElementById('" . $dname . "_item' + " . $dname . "_itemnum).value = isetval;
					" . $dname . "_itemnum ++;
				}
                function changecat(id,selvalue,item_options){
                   var options= " . $mainarr . ";
			                   console.log(options);
			                   var idd=id.replace('item','unit');
			                   var myDb = $mainarr;
			                   console.log(myDb);
			                   var unitarr= [];
			                   var mapped = Object.keys( myDb  ).map(function( value ){
			                   var jsonval = (myDb[value ].value = value)
			
			                   if(jsonval===selvalue)
			                   {
			
			                   unitarr.push(myDb[value]);
			
			}
			});
			var catOptions = '';
			for (var i = 0; i < unitarr[0].length; i++)
			{
			catOptions += '<option value=\''+ unitarr[0][i] +'\'>' + unitarr[0][i] + '</option>';
			}
			document.getElementById(idd).innerHTML = catOptions;
			}
			setvals = window.parent.document.getElementById('" . $item_name . "').value;
						//alert(setvals);
				setvals = setvals.split(',');
				for(i=0; i<setvals.length; i++) {
					setval = setvals[i].split(' - ');
					if(setval.length > 1 && setval[0]!='') {
						add_more_" . $dname . "(setval[0],setval[1],setval[2],setval[3]);
					}
				}";
				$set_js_code = "
					setval = '';
					ing_count = 0;
		
					for(i=0; i<" . $dname . "_itemnum; i++) {
						iival = document.getElementById('" . $dname . "_item' + i).value;
						iqval = document.getElementById('" . $dname . "_qty' + i).value;
						iuval = document.getElementById('" . $dname . "_unit' + i).value;
					    var form = document.dform;
					    var radios = form.elements['" . $dname . "_enable' + i];
			
			
						if(iival!='') {
							if(setval!='') setval += ','; setval += iival + ' - ' + iqval+' - '+iuval + ' - '+ radios.value; ing_count++;
						}
					}
					if(setval=='') {
						setcol = '#aaaaaa';
						showval = 'Nutritions';
			
					} else {
						setcol = '#1c591c'; showval = 'Nutrition'; if(ing_count > 1) showval +='s';
					    showval +=' (' +ing_count +')';
					}
					window.parent.document.getElementById('" . $item_name . "').value = setval;
					window.parent.document.getElementById('" . $item_name . "' + '_display').style.color = setcol;
					window.parent.document.getElementById('" . $item_name . "' + '_display').value = showval;";

				if (strpos ( $toprint, "<tr>" ) < 0) {
					$toprint .= "
					<tr><td>";
				}
				$toprint .= "
						<input type='hidden' name='" . $dname . "'>
						<div id='list_" . $dname . "'>&nbsp;</div>
						<input type='button' id='Addmore' value='Add More >>' onclick='add_more_" . $dname . "()'>
					";
				$toprint .= "
						<input type='hidden' name='addcustom'>
						<div style='padding-top:10px' id='list_addcustom' ></div>
						<input type='button' value='Add Custom >>' id='Addcustom' onclick='add_more_custom()'>
					";
				}else if ($dtype == "nutrition")
				{
					$rdb = admin_info("database");
					lavu_connect_byid(admin_info("companyid"),$rdb);
					$locationid = sessvar("locationid");

					$apos = ""; // put the character for apostrophe here
					$ingredient_options = array("<option value=''></option>");
					$ing_query = lavu_query("select title from  nutrition_information");
					$a_replace_chars = array("'", '"', "\n");
					while($ing_read = mysqli_fetch_assoc($ing_query))
					{
						$ingredient_options[] = "<option value='{$ing_read['title']}'>".$ing_read['title']."</option>";
					}
					$sql= lavu_query("select title,units from  nutrition_information");
					$a_replace_chars = array("'", '"', "\n");
					while($result = mysqli_fetch_assoc($sql))
					{
						$result_options[] = $result;
					}
					$mainarr = array();
					foreach($result_options as $key=>$val){
						$units = $val['units'];
						$units = explode('|',$units);
						foreach($units as $unit)
						{
							$mainarr[$val['title']][] = $unit;
						}

					}
					$mainarr = json_encode($mainarr);
					$get_js_code = "
				".$dname."_itemnum = 0;
				function add_more_".$dname."(isetval,qsetval,usetval,radval) {
					if(!isetval) isetval = '';
					if(!qsetval) qsetval = '0';
					if(!usetval) usetval = '';
					if(!radval) radval = '';
					var item_options = \"";


					foreach($ingredient_options as $s_option) {
						$get_js_code .= $s_option;
					}
					$get_js_code.="\";";
					$get_js_code .= "
			
			
					var t = document.createElement('div');
					    t.innerHTML += '<table>';
						t.innerHTML +=  '<tr>';
						  if(isetval=='' || isetval==='calories' || isetval=='proteins' || isetval=='fat' || isetval=='sodium' || isetval=='carbs'
								|| isetval=='sugar' || isetval=='saturated_fat' || isetval=='fiber'){
						t.innerHTML += '<td><select id=\"".$dname."_item' + ".$dname."_itemnum + '\" onchange=\"changecat(this.id,this.value)\">' +item_options + '</select></td>';
						t.innerHTML += '<td><input id=\"".$dname."_qty' + ".$dname."_itemnum + '\" type=\"text\" size=\"3\" value=\"' + qsetval + '\" /></td>';
						t.innerHTML += '<td><select id=\"".$dname."_unit' + ".$dname."_itemnum + '\"><option value=\"' + usetval + '\">'+usetval+'</option></select></td>';
							if(radval==''){
									t.innerHTML += '<td><input name=\"".$dname."_enable' + ".$dname."_itemnum + '\" type=\"radio\"  value=\"' + 1 + '\">Enable</td>';
								    t.innerHTML += '<td><input name=\"".$dname."_enable' + ".$dname."_itemnum + '\" type=\"radio\"  value=\"' + 0 + '\" checked>Disable</td>';
								 }
								if(radval==='1')
								{
								t.innerHTML += '<td><input name=\"".$dname."_enable' + ".$dname."_itemnum + '\" type=\"radio\"  value=\"' + 1 + '\" checked>Enable</td>';
								t.innerHTML += '<td><input name=\"".$dname."_enable' + ".$dname."_itemnum + '\" type=\"radio\"  value=\"' + 0 + '\">Disable</td>';
								}
								if(radval==='0')
								{
								t.innerHTML += '<td><input name=\"".$dname."_enable' + ".$dname."_itemnum + '\" type=\"radio\"  value=\"' + 1 + '\" >Enable</td>';
								t.innerHTML += '<td><input name=\"".$dname."_enable' + ".$dname."_itemnum + '\" type=\"radio\"  value=\"' + 0 + '\" checked>Disable</td>';
								}
				
					
						 }
							  else {
									t.innerHTML += '<td><input id=\"".$dname."_item' + ".$dname."_itemnum + '\" type=\"text\" size=\"10\"  value=\"' + isetval + '\" /></td>';
									t.innerHTML += '<td><input id=\"".$dname."_qty' + ".$dname."_itemnum + '\" type=\"text\" size=\"3\" value=\"' + qsetval + '\" /></td>';
						            t.innerHTML += '<td><input id=\"".$dname."_unit' + ".$dname."_itemnum + '\" type=\"text\" size=\"3\" value=\"' + usetval + '\"></td>';
						             if(radval==''){
									t.innerHTML += '<td><input name=\"".$dname."_enable' + ".$dname."_itemnum + '\" type=\"radio\"  value=\"' + 1 + '\">Enable</td>';
								    t.innerHTML += '<td><input name=\"".$dname."_enable' + ".$dname."_itemnum + '\" type=\"radio\"  value=\"' + 0 + '\" checked >Disable</td>';
								    }
						          if(radval==='1'){
									t.innerHTML += '<td><input name=\"".$dname."_enable' + ".$dname."_itemnum + '\" type=\"radio\"  value=\"' + 1 + '\" checked>Enable</td>';
									t.innerHTML += '<td><input name=\"".$dname."_enable' + ".$dname."_itemnum + '\" type=\"radio\"  value=\"' + 0 + '\">Disable</td>';
								    }
								if(radval==='0'){
									t.innerHTML += '<td><input name=\"".$dname."_enable' + ".$dname."_itemnum + '\" type=\"radio\"  value=\"' + 1 + '\" >Enable</td>';
									t.innerHTML += '<td><input name=\"".$dname."_enable' + ".$dname."_itemnum + '\" type=\"radio\"  value=\"' + 0 + '\" checked>Disable</td>';
								  }
						   
						     }
						t.innerHTML += '</tr>';
						t.innerHTML += '</table>';
					document.getElementById('list_".$dname."').appendChild(t);
					document.getElementById('".$dname."_item' + ".$dname."_itemnum).value = isetval;
					".$dname."_itemnum ++;
				
				}
				function add_more_custom(isetval,qsetval,usetval,radval) {
				
				
					if(!isetval) isetval = '';
					if(!qsetval) qsetval = '0';
					if(!usetval) usetval = '0';
					if(!radval) radval = '';
					var item_options = \"";


					foreach($ingredient_options as $s_option) {
						$get_js_code .= $s_option;
					}
					$get_js_code.="\";";
					$get_js_code .= "
					var t = document.createElement('div');
					    t.innerHTML += '<table>';
						t.innerHTML +=  '<tr>';
						t.innerHTML += '<td><input id=\"".$dname."_item' + ".$dname."_itemnum + '\" type=\"text\" size=\"10\"  value=\"' + qsetval + '\" /></td>';
						t.innerHTML += '<td><input id=\"".$dname."_qty' + ".$dname."_itemnum + '\" type=\"text\" size=\"3\" value=\"' + qsetval + '\" /></td>';
						t.innerHTML += '<td><input id=\"".$dname."_unit' + ".$dname."_itemnum + '\" type=\"text\" size=\"3\" value=\"' + usetval + '\" /></td>';
						   if(radval==''){
									t.innerHTML += '<td><input name=\"".$dname."_enable' + ".$dname."_itemnum + '\" type=\"radio\"  value=\"' + 1 + '\">Enable</td>';
								    t.innerHTML += '<td><input name=\"".$dname."_enable' + ".$dname."_itemnum + '\" type=\"radio\"  value=\"' + 0 + '\" checked>Disable</td>';
								}
								if(radval==='1')
								{
								t.innerHTML += '<td><input name=\"".$dname."_enable' + ".$dname."_itemnum + '\" type=\"radio\"  value=\"' + 1 + '\" checked>Enable</td>';
								t.innerHTML += '<td><input name=\"".$dname."_enable' + ".$dname."_itemnum + '\" type=\"radio\"  value=\"' + 0 + '\">Disable</td>';
								}
								if(radval==='0')
								{
								t.innerHTML += '<td><input name=\"".$dname."_enable' + ".$dname."_itemnum + '\" type=\"radio\"  value=\"' + 1 + '\" >Enable</td>';
								t.innerHTML += '<td><input name=\"".$dname."_enable' + ".$dname."_itemnum + '\" type=\"radio\"  value=\"' + 0 + '\" checked>Disable</td>';
								}
						t.innerHTML += '</tr>';
						t.innerHTML += '</table>';
					document.getElementById('list_addcustom').appendChild(t);
					document.getElementById('".$dname."_item' + ".$dname."_itemnum).value = isetval;
					".$dname."_itemnum ++;
				}
                function changecat(id,selvalue,item_options){
                   var options= ".$mainarr.";
				                   console.log(options);
				                   var idd=id.replace('item','unit');
				                   var myDb = $mainarr;
				                   console.log(myDb);
				                   var unitarr= [];
				                   var mapped = Object.keys( myDb  ).map(function( value ){
				                   var jsonval = (myDb[value ].value = value)
				                   	
				                   if(jsonval===selvalue)
				                   {
				                   	
				                   unitarr.push(myDb[value]);
				
				}
				});
				var catOptions = '';
				for (var i = 0; i < unitarr[0].length; i++)
				{
				catOptions += '<option value=\''+ unitarr[0][i] +'\'>' + unitarr[0][i] + '</option>';
				}
				document.getElementById(idd).innerHTML = catOptions;
				}
				setvals = window.parent.document.getElementById('".$item_name."').value;
						//alert(setvals);
				setvals = setvals.split(',');
				for(i=0; i<setvals.length; i++) {
					setval = setvals[i].split(' - ');
					if(setval.length > 1 && setval[0]!='') {
						add_more_".$dname."(setval[0],setval[1],setval[2],setval[3]);
					}
				}";
					$set_js_code = "
					setval = '';
					ing_count = 0;
			
					for(i=0; i<".$dname."_itemnum; i++) {
						iival = document.getElementById('".$dname."_item' + i).value;
						iqval = document.getElementById('".$dname."_qty' + i).value;
						iuval = document.getElementById('".$dname."_unit' + i).value;
					    var form = document.dform;
					    var radios = form.elements['".$dname."_enable' + i];
				
				
						if(iival!='') {
							if(setval!='') setval += ','; setval += iival + ' - ' + iqval+' - '+iuval + ' - '+ radios.value; ing_count++;
						}
					}
					if(setval=='') {
						setcol = '#aaaaaa';
						showval = 'Nutritions';
				
					} else {
						setcol = '#1c591c'; showval = ing_count + ' Nutrition'; if(ing_count > 1) showval +='s';
					}
					window.parent.document.getElementById('".$item_name."').value = setval;
					window.parent.document.getElementById('".$item_name."' + '_display').style.color = setcol;
					window.parent.document.getElementById('".$item_name."' + '_display').value = showval;";

					if (strpos($toprint, "<tr>") < 0) {
						$toprint .= "
					<tr><td>";
					}
					$toprint .= "
						<input type='hidden' name='".$dname."'>
						<div id='list_".$dname."'>&nbsp;</div>
						<input type='button' id='Addmore' value='Add More >>' onclick='add_more_".$dname."()'>
					";
					$toprint .= "
						<input type='hidden' name='addcustom'>
						<div style='padding-top:10px' id='list_addcustom' ></div>
						<input type='button' value='Add Custom >>' id='Addcustom' onclick='add_more_custom()'>
					";
				}			else if ($dtype == "ingredients"){

					$rdb = admin_info("database");
					lavu_connect_byid(admin_info("companyid"),$rdb);
					$locationid = sessvar("locationid");
                    //TODO:- Removed this if else condition after full migration implementation
                    if (sessvar("INVENTORY_MIGRATION_STATUS") == "Completed_Ready" || sessvar("INVENTORY_MIGRATION_STATUS") == "Skipped_Ready") {
                        $apos = ""; // put the character for apostrophe here
                        $ingredient_options = array("<option value=''></option>"
                        );$a_replace_chars = array("'",
							'"',"\n"
                        );// Getting inventory items from inventory_items table through inventory api call
                       
                        $inventory_read = [];
                        $units = get_inventory_units();
						$inventoryItemsUnits = array();
                        function cmp($value1, $value2) {
                         return strcmp($value1["name"], $value2["name"]);
                        }
                        usort($inventory_read, "cmp");
                        foreach ($inventory_read as $invValues) {
                            $ingredient_options[] = "<option value='{$invValues['id']}'>" . trim(str_replace($a_replace_chars, $apos, $invValues['name'])) . "</option>";
                            $inventoryItemsUnits[$invValues['id']] = $invValues['salesUnitID'];
                        }

                        $get_js_code = "
						var itemsUnits = new Array(); // inventory_items id and unit symbol mapping
						itemsUnits = ";
					$get_js_code .= json_encode ( $inventoryItemsUnits );
					$get_js_code .= ";";
					$get_js_code .= "
						var units = new Array(); // unit id and unit symbol mapping
						units = ";
						$get_js_code .= json_encode ( $units );
						$get_js_code .= ";";
						$get_js_code .= "
						" . $dname . "_itemnum = 0;
						function add_more_" . $dname . "(isetval,setunit,qsetval) {
							if(!isetval) isetval = '';
							if(!qsetval) qsetval = '1';
							if(!setunit) setunit = '';
							var item_options = \"";
							foreach ( $ingredient_options as $s_option ) {
								$get_js_code .= $s_option;
							}
							$get_js_code .= "\";";
							$get_js_code .= "
							var t = document.createElement('div');
							var usetval = '';
							if(window.parent.window.ingredientItems) {
								item_options = window.parent.window.ingredientItems.options;
							}
							
							if(units[setunit] != undefined) {
								usetval = units[setunit];
							}
							t.setAttribute('id', 'ingredient-'+" . $dname . "_itemnum);
							t.innerHTML += '<table><tr><td><select id=\"" . $dname . "_item' + " . $dname . "_itemnum + '\" onChange=\'click_item(this, \"" . $dname . "_unit_display' + " . $dname . "_itemnum + '\", \"" . $dname . "_unit' + " . $dname . "_itemnum + '\")\' data-selected=\"'+ isetval +'\">' + item_options + '</select></td><td><input id=\"" . $dname . "_qty' + " . $dname . "_itemnum + '\" type=\"text\" size=\"3\" value=\"' + qsetval + '\" /></td><td><input id=\"" . $dname . "_unit_display' + " . $dname . "_itemnum + '\" type=\"text\" size=\"5\" value=\"' + usetval + '\" disabled /><input type=\"hidden\" id=\"" . $dname . "_unit' + " . $dname . "_itemnum + '\" type=\"text\" size=\"5\" value=\"' + setunit + '\"  />&nbsp;&nbsp;<a class=\'closeBtn\' onclick=\'click_delete(\"ingredient-' + " . $dname . "_itemnum + '\")\'>X</a></td></tr></table>';
							document.getElementById('list_" . $dname . "').appendChild(t);
							document.getElementById('" . $dname . "_item' + " . $dname . "_itemnum).value = isetval;
				
							" . $dname . "_itemnum ++;
						}
						
						function click_delete(divId) {
							var element = document.getElementById(divId);
							element.parentNode.removeChild(element);
						}
				
						// Set UOM on inventory item change
						function click_item(itemObj, unitDisplayObj, unitObj) {
							if(window.parent.window.ingredientItems) {
								itemsUnits = window.parent.window.ingredientItems.units;
							}
							var id  = itemObj.value;
							var uid = itemsUnits[id];
							document.getElementById(unitDisplayObj).value = units[uid];
							document.getElementById(unitObj).value = itemsUnits[id];
						}
						
						setvals = window.parent.document.getElementById('" . $item_name . "').value;
						ingredientunitvals = setvals.split(' xx ');
						setvals = ingredientunitvals[0].split(',');
						var setunits = [];
						if(ingredientunitvals.length > 0) {
							if (ingredientunitvals[1] !== undefined) {
								setunits = ingredientunitvals[1].split(',');
							}
						}
						for(i=0; i<setvals.length; i++) {
							setval = setvals[i].split(' x ');
							if(setval.length > 1 && setval[0]!='') {
								add_more_" . $dname . "(setval[0],setunits[i],setval[1]);
							}
						}";

					$set_js_code = "
						setval = '';
						setunit = '';
						ing_count = 0;
						for(i=0; i<" . $dname . "_itemnum; i++) {
							element = document.getElementById('" . $dname . "_item' + i);
							if (element != null) {
								iival = document.getElementById('" . $dname . "_item' + i).value;
								iqval = document.getElementById('" . $dname . "_qty' + i).value;
								iuval = document.getElementById('" . $dname . "_unit' + i).value;
								if(iival!='') {
									if(setval!='') setval += ','; setval += iival + ' x ' + iqval; ing_count++;
									if(setunit!='') setunit += ','; setunit += iuval;
								}
							}
						}
						if(setval=='') {
							setcol = '#aaaaaa';
							showval = '" . $showVal . "';
						} else {
							setcol = '#1c591c'; showval = ing_count + ' " . $showVal . "'; if(ing_count > 1) showval = ing_count + ' " . $showVals . "';
						}
						window.parent.document.getElementById('" . $item_name . "').value = setval + ' xx ' + setunit;
						window.parent.document.getElementById('" . $item_name . "' + '_display').style.color = setcol;
						window.parent.document.getElementById('" . $item_name . "' + '_display').value = showval;";

					if (strpos ( $toprint, "<tr>" ) < 0) {
						$toprint .= "
						<tr><td>";
					}
					$toprint .= "
							<input type='hidden' name='" . $dname . "'>
							<div id='list_" . $dname . "'>&nbsp;</div>
							<input type='button' value='Add More >>' onclick='add_more_" . $dname . "()'>
						";
                    } else {
                        $apos = ""; // put the character for apostrophe here
                        $ingredient_options = array("<option value=''></option>"
                        );$ing_query = lavu_query("select `ingredients`.`title` as `title`, `ingredients`.`id` as `id`, `ingredients`.`unit` as `unit` from `ingredients` left join `ingredient_categories` on `ingredients`.`category`=`ingredient_categories`.`id` where `ingredients`.`loc_id` = '".$locationid."' AND `ingredients`.`_deleted`!='1' and `ingredient_categories`.`_deleted`!='1' order by `ingredients`.`title` asc");
                        $a_replace_chars = array("'",
							'"',"\n"
                        );while($ing_read = mysqli_fetch_assoc($ing_query))
                        {
                            $ingredient_options[] = "<option value='{$ing_read['id']}'>".trim(str_replace($a_replace_chars,$apos,$ing_read['title'])." (".str_replace($a_replace_chars,$apos,$ing_read['unit']).")")."</option>";
                        }

                        $get_js_code = "
						".$dname."_itemnum = 0;

						function add_more_".$dname."(isetval,qsetval) {
							if(!isetval) isetval = '';
							if(!qsetval) qsetval = '1';
							var item_options = \"";
					foreach ( $ingredient_options as $s_option ) {
						$get_js_code .= $s_option;
					}
					$get_js_code .= "\";";
					$get_js_code .= "
							var t = document.createElement('div');
							t.innerHTML += '<table><tr><td><select id=\"" . $dname . "_item' + " . $dname . "_itemnum + '\">' + item_options + '</select></td><td><input id=\"" . $dname . "_qty' + " . $dname . "_itemnum + '\" type=\"text\" size=\"3\" value=\"' + qsetval + '\" /></td></tr></table>';
							document.getElementById('list_" . $dname . "').appendChild(t);
							document.getElementById('" . $dname . "_item' + " . $dname . "_itemnum).value = isetval;
							" . $dname . "_itemnum ++;
						}
				
						setvals = window.parent.document.getElementById('" . $item_name . "').value;
						setvals = setvals.split(',');
						for(i=0; i<setvals.length; i++) {
							setval = setvals[i].split(' x ');
							if(setval.length > 1 && setval[0]!='') {
								add_more_" . $dname . "(setval[0],setval[1]);
							}
						}";

					$set_js_code = "
						setval = '';
						ing_count = 0;
						for(i=0; i<" . $dname . "_itemnum; i++) {
							iival = document.getElementById('" . $dname . "_item' + i).value;
							iqval = document.getElementById('" . $dname . "_qty' + i).value;
							if(iival!='') {
								if(setval!='') setval += ','; setval += iival + ' x ' + iqval; ing_count++;
							}
						}
						if(setval=='') {
							setcol = '#aaaaaa';
							showval = '" . $showVal . "';
						} else {
							setcol = '#1c591c'; showval = ing_count + ' " . $showVal . "'; if(ing_count > 1) showval = ing_count + ' " . $showVals . "';
						}
						window.parent.document.getElementById('" . $item_name . "').value = setval;
						window.parent.document.getElementById('" . $item_name . "' + '_display').style.color = setcol;
						window.parent.document.getElementById('" . $item_name . "' + '_display').value = showval;";

					if (strpos ( $toprint, "<tr>" ) < 0) {
						$toprint .= "
						<tr><td>";
					}
					$toprint .= "
							<input type='hidden' name='" . $dname . "'>
							<div id='list_" . $dname . "'>&nbsp;</div>
							<input type='button' value='Add More >>' onclick='add_more_" . $dname . "()'>
						";
                    }

				}elseif ($dtype == "comboitems"){
					$rdb = admin_info("database");
					lavu_connect_byid(admin_info("companyid"),$rdb);
					$locationid = sessvar("locationid");

					$apos = ""; // put the character for apostrophe here
					$combo_options = array("<option value=''></option>"
				);	$ing_query = lavu_query("select `menu_items`.`name` as `title`, `menu_items`.`id` as `id` , `menu_items`.`price` as `item_price`   from `menu_items` left join `menu_categories` on `menu_items`.`category_id`=`menu_categories`.`id` left join `menu_groups` on `menu_categories`.`group_id`=`menu_groups`.`id` where `menu_items`.`combo` != '1' AND `menu_items`.`_deleted`!='1' and `menu_categories`.`_deleted`!='1' and `menu_groups`.`_deleted`!='1'  and  `menu_items`.`id`!='".$_GET['item_id']."' and `menu_categories`.`menu_id`='1'order by `menu_items`.`name` asc");
					$a_replace_chars = array("'",
						'"',"\n"
				);	$mcnt=0;
				
				$precision = isset ( $location_info ['disable_decimal'] ) ? $location_info ['disable_decimal'] : 2;
				if ($location_info ['decimal_char'] != "") {
					$decimal_char = isset ( $location_info ['decimal_char'] ) ? $location_info ['decimal_char'] : '.';
					$thousands_char = (isset ( $location_info ['decimal_char'] ) && $location_info ['decimal_char'] == ".") ? "," : '.';
				}
				$thousands_sep = ",";
				if (! empty ( $thousands_char )) {
					$thousands_sep = $thousands_char;
				}

				while($ing_read = mysqli_fetch_assoc($ing_query))
				{
					//$ingredient_options[] = "<option value='{$ing_read['id']}'>".trim(str_replace($a_replace_chars,$apos,$ing_read['title'])." ")."</option>";
					$menu_item[$mcnt]['id'] = $ing_read['id'];
					$menu_item[$mcnt]['text'] = $ing_read['title'];

					$item_price[$mcnt]['id'] = $ing_read['id'];
					$item_price[$mcnt]['item_price'] = number_format ( $ing_read['item_price'], $precision, $decimal_char, $thousands_sep );
					$mcnt++;
				}

					$menu_json=json_encode ($menu_item);
					$item_price_json=json_encode ($item_price);
					$get_js_code = "
							function add_more_comboitems(type,elemid) {						
						        
						         var elemnt = document.getElementById('ival').value;
								 var parentids = elemnt;
						         var count = document.getElementById('ival').value;
						         var tablecnt = document.getElementById('itable').value;
						         var mtable = 'mtable_'+tablecnt;
								 var selmenuid = 'm_'+elemnt+'_selmenu';
								 var price_id = 'm_'+elemnt+'_price';
							     var selmenutd = 'm_'+elemnt+'_td';
								 var qtytd = 'm_'+elemnt+'_qtytd';
								 var hqtytd = 'm_'+elemnt+'_hqtytd';
								 var txttd = 'm_'+elemnt+'_txt';
								 var startcnt = 1;
								 var hid = 'mtext_'+tablecnt;
								 var deltype = 'maintab';

						         var htmd = '<table id=\"'+mtable+'\" style=\"margin: 10px 0\"><tr><td><div class=\"flex\"><span id=\"'+selmenutd+'\" style=\"width:220px;\"><select class=\"js-example-data-array\" id=\"m_'+elemnt+'_selmenu\" name=\"m_'+elemnt+'_selmenu\" style=\"width: 95%\" onchange=\"get_menu_price(this.value,\'m_'+elemnt+'_price\')\"></select></span><span id=\"'+txttd+'\" style=\"display:none;width:220px\"><input type=\"text\" style=\"width:95%;\" id=\"m_'+elemnt+'_seltext\" name=\"m_'+elemnt+'_seltext\"/></span><span id=\"'+qtytd+'\" style=\"width:5%;\"><input id=\"m_'+elemnt+'_qty\" name=\"m_'+elemnt+'_qty\" title=\"Quantity\" onkeypress=\"return validateNumber(event);\"  value=\"1\" type=\"text\" size=\"3\" style=\"width:100%;\"/></span><span id=\"'+hqtytd+'\" style=\"display:none;width:5%;\"><input id=\"m_'+elemnt+'_hqty\" name=\"m_'+elemnt+'_hqty\" title=\"Heder Quantity\" onkeypress=\"return validateNumber(event);\"  value=\"1\" type=\"text\" size=\"3\" style=\"width:100%;\"/></span> <input id=\"m_'+elemnt+'_deleted\" name=\"m_'+elemnt+'_deleted\" value=\"\" type=\"hidden\" size=\"3\" /> <span style=\"padding-left: 12px; width:15%;\"> <input type=\"button\" id=\"m_option_'+elemnt+'\" name=\"m_option_'+elemnt+'\" value=\"+ OPTION\" onclick=\"add_more_opt(this.id,'+elemnt+',\''+mtable+'\',\''+parentids+'\',\''+hid+'\',\''+startcnt+'\')\"> </span> <span style=\"width:15%\"> <input type=\"button\" id=\"m_subopt_'+elemnt+'\" name=\"m_subopt_'+elemnt+'\" value=\"+ SUB OPTION\" onclick=\"add_more_subopt(this.id,'+elemnt+',\''+mtable+'\',\''+parentids+'\',\''+startcnt+'\')\"> </span>  <span style=\"padding-left: 20px\"><input id=\"m_'+elemnt+'_price\" name=\"m_'+elemnt+'_price\" title=\"Price\" type=\"text\" size=\"4\" style=\"width:100%;\"/></span><span style=\"padding-left: 50px\"><label>Header</label><input type=\"checkbox\" id=\"m_'+elemnt+'_header\" name=\"m_'+elemnt+'_header\" onclick=\"displayheader(this.checked,\''+selmenutd+'\',\''+mtable+'\')\" > </span><span style=\"padding-left: 50px;\"><a class=\"closeBtn\" id=\"m_'+elemnt+'_closebtn\" onclick=\"deloption(\''+mtable+'\',\''+deltype+'\')\"  >X</a></span></div></td></tr></table>';
						         
								var form = document.getElementById('comboitems');
								 
								 if(tablecnt==1)
								 {
						         	form.innerHTML = form.innerHTML + htmd;
								 }
							    else
								{
								  var tbl = $( \"div table:last-child\" );
								  
								  $(htmd).insertAfter(tbl);
								}
								get_menu_price(selmenuid,price_id,'add');
								var input = document.createElement(\"input\");

								input.setAttribute(\"type\", \"hidden\");
								
								input.setAttribute(\"id\", hid);
								
								input.setAttribute(\"value\", \"\");
								
								 //append to form element that you want .
								 document.getElementById(\"dForm\").appendChild(input);
						         count++;
						         tablecnt++;
						         document.getElementById('ival').value = count;
						         document.getElementById('itable').value = tablecnt;
								 document.getElementById('ioptval').value='';
								 selectauto();
					         }";

				$get_js_code .= "
							       function add_more_opt(ids,count,tableid,parentids,hid,startcnt='',type=''){
							        var ioptval = document.getElementById('ioptval').value.toString();
							        var split_cnt = '';
									var opttab = '';
							        var setparentid = parentids;
							        var styletab = '';
							        var getcount = document.getElementById(hid).value;
							        var tablecnt = document.getElementById('itable').value;
									if(tableid.indexOf('_opt_')>-1)
									{
										var opttab = tableid;
									}
									else
									{
										var opttab = tableid+'_opt_';
									}
									var findopt = $('table[id^='+opttab+']');
							        if(tableid.indexOf('_opt_')>-1)
							        {
							         var y = tableid.split('_');
							         var x= y[0]+'_'+y[1]+'_'+y[2]+'_';
							        }
									else if(tableid.indexOf('_opt_')==-1 && (findopt.length)==0)
									{
										var searchtab = tableid+'_subopt_';
										var findsubopt = $('table[id^='+searchtab+']');
										var x = $(findsubopt[findsubopt.length-1]).attr('id');
									}
							        else
							        {
							         var x = tableid+'_opt_';
							        }
							        var alltab = $('table[id^=\"'+x+'\"]')
							        for(var i=0; i<alltab.length; i++)
							        {
							         var rendertabid = $(alltab[i]).attr('id');
							        }
							        if(!rendertabid)
							        {
							         split_cnt = 1;
							         rendertabid = tableid;
							        }
							        else
							        {
										 if(rendertabid.indexOf('_subopt_')>-1)
										 {
											if(findopt.length>0)
											{
												var y = $(findopt[findopt.length-1]).attr('id');
												var tabsplit = y.split('_');
									         	split_cnt = parseInt(tabsplit[3])+1;
											}
											else
											{
												if(findopt.length>0)
												{
													var tabsplit = rendertabid.split('_');
									         	    split_cnt = parseInt(tabsplit[3])+1;
												}
												else
												{
													split_cnt = 1;
												}
											}
										 }
										 else
										 {
											var tabsplit = rendertabid.split('_');
								         	split_cnt = parseInt(tabsplit[3])+1;
										 }
							        }
												
							        if(type=='subopt')
							        {
							         styletab = 'style=\"margin-left:40px;\"';
							        }
									else
									{
										//styletab = 'style=\"margin-left:10px;\"';
									}
							        var deltype = 'opttab';
							        var mtable = 'mtable_'+setparentid+'_opt_'+split_cnt;
							        var selmenuid = 'm_'+setparentid+'_opt_'+split_cnt+'_selmenu';
									var price_id = 'm_'+setparentid+'_opt_'+split_cnt+'_price';
							        var selmenutd = 'm_'+setparentid+'_opt_'+split_cnt+'_td';
									var qtytd = 'm_'+setparentid+'_opt_'+split_cnt+'_qtytd';
									var hqtytd = 'm_'+setparentid+'_opt_'+split_cnt+'_hqtytd';
							        var txttd = 'm_'+setparentid+'_opt_'+split_cnt+'_txt';
									var htmd = '<table id=\"'+mtable+'\" '+styletab+'><tr><td>OR</td></tr><tr><td><div class=\"flex\"><span id=\"'+selmenutd+'\" style=\"width:220px;\"><select style=\"width:95%;\" class=\"js-example-data-array\" id=\"m_'+setparentid+'_opt_'+split_cnt+'_selmenu\"  name=\"m_'+setparentid+'_opt_'+split_cnt+'_selmenu\" onchange=\"get_menu_price(this.value,\'m_'+setparentid+'_opt_'+split_cnt+'_price\')\"></select></span><span id=\"'+txttd+'\" style=\"display:none;width:220px;\"><input type=\"text\" style=\"width:95%;\" id=\"m_'+setparentid+'_opt_'+split_cnt+'_seltext\" name=\"m_'+setparentid+'_opt_'+split_cnt+'_seltext\"/></span><span id=\"'+qtytd+'\" style=\"width:5%;\"><input id=\"m_'+setparentid+'_opt_'+split_cnt+'_qty\" name=\"m_'+setparentid+'_opt_'+split_cnt+'_qty\" title=\"Quantity\" onkeypress=\"return validateNumber(event);\" value=\"1\" type=\"text\" size=\"3\" style=\"width:100%;\" /></span> <span id=\"'+hqtytd+'\" style=\"width:5%;display:none;\"><input id=\"m_'+setparentid+'_opt_'+split_cnt+'_hqty\" name=\"m_'+setparentid+'_opt_'+split_cnt+'_hqty\" title=\"Quantity\" onkeypress=\"return validateNumber(event);\" value=\"1\" type=\"text\" size=\"3\" style=\"width:100%;\" /></span><input id=\"m_'+setparentid+'_opt_'+split_cnt+'_deleted\" name=\"m_'+setparentid+'_opt_'+split_cnt+'_deleted\" type=\"hidden\"  value=\"0\" /> <span style=\"padding-left: 12px; width:15%;\"> <input type=\"button\" id=\"m_'+setparentid+'_opt_'+split_cnt+'\" name=\"m_'+setparentid+'_opt_'+split_cnt+'\" value=\"+ OPTION\" onclick=\"add_more_opt(this.id,'+split_cnt+',\''+mtable+'\','+setparentid+',\''+hid+'\',\'\')\"> </span> <span style=\"width:15%;\"> <input type=\"button\" id=\"m_'+setparentid+'_subopt_'+split_cnt+'\" name=\"m_subopt_'+split_cnt+'\" value=\"+ SUB OPTION\" onclick=\"add_more_subopt(this.id,'+split_cnt+',\''+mtable+'\','+setparentid+',1)\"> </span>  <span style=\"padding-left: 20px\"><input id=\"m_'+setparentid+'_opt_'+split_cnt+'_price\" title=\"Price\" name=\"m_'+setparentid+'_opt_'+split_cnt+'_price\" type=\"text\" size=\"4\" /></span><span style=\"padding-left: 50px\"><label>Header</label><input type=\"checkbox\" id=\"m_'+setparentid+'_opt_'+split_cnt+'_header\" name=\"m_'+setparentid+'_opt_'+split_cnt+'_header\" onclick=\"displayheader(this.checked,\''+selmenutd+'\',\''+mtable+'\')\" > </span><span style=\"padding-left: 50px\"><a class=\"closeBtn\" id=\"m_'+setparentid+'_opt_'+split_cnt+'_closebtn\" onclick=\"deloption(\''+mtable+'\',\''+deltype+'\')\" >X</a></div></td></tr></table>';
									$(htmd).insertAfter('#'+rendertabid);
									get_menu_price(selmenuid,price_id,'add');
							        tablecnt++;
							   
							        document.getElementById('ioptcount').value = split_cnt;
							   
							        //document.getElementById(hid).value = concatval;
							   
							        selectauto('#'+mtable);
							               }";

				$get_js_code .= "
					function add_more_subopt(ids,count,tableid,pid,startcnt='',type=''){
					if($('#'+tableid).find('input[type=\"checkbox\"]:checked').length==0 && tableid.indexOf('_subopt_')==-1)
					{
						alert('Only Combo Header can have the Sub-Option');
						return false;
					}
					var ioptval = document.getElementById('isuboptval').value.toString();
					var split_cnt = '';
					var spids = ids.split('_');
					var optid = '';
					var settype = '';
					if(spids.length==3)
					{
						settype = 'subopt_opt';
					}
					else
					{
						settype = type;
					}
					
					var optpos='';
					if(tableid.indexOf('_opt_')>-1 && tableid.indexOf('_subopt_')==-1)
					{
						var x = tableid+'_subopt_';
						optpos = 3;
					}
					else if(tableid.indexOf('_opt_')==-1 && tableid.indexOf('_subopt_')>-1)
					{
						var searchtab = tableid;
						var y = searchtab.split('_');
						var x = y.slice(0, y.length - 1).join(\"_\") + \"_\";
					}
					else if(tableid.indexOf('_opt_')>-1 && tableid.indexOf('_subopt_')>-1)
					{
						var searchtab = tableid;
						var y = searchtab.split('_');
						var x = y.slice(0, y.length - 1).join(\"_\") + \"_\";
						optpos = 3;
					}
					else
					{
						var x = tableid+'_subopt_';
					}
					
					var alltab = $('table[id^=\"'+x+'\"]')
					for(var i=0; i<alltab.length; i++)
					{
						var rendertabid = $(alltab[i]).attr('id');
				    }
					if(!rendertabid)
					{
						var tabsplit = x.split('_');
						if(optpos!='')
						{
							optid = tabsplit[optpos];
							optid = '_opt_'+optid;
						}
						var rendertabid = tableid;
						split_cnt = 1;
					}
					else
					{
						var tabsplit = rendertabid.split('_');
						if(optpos!='')
						{
							optid = tabsplit[optpos];
							optid = '_opt_'+optid;
							split_cnt = parseInt(tabsplit[5])+1;
						}
						else
						{
							split_cnt = parseInt(tabsplit[3])+1;
						}
					}
					
					var deltype = 'subtab';
					startcnt++;
					var tablecnt = document.getElementById('itable').value;
					var mtable = 'mtable_'+pid+''+optid+'_subopt_'+split_cnt;
					var selmenuid = 'm_'+pid+''+optid+'_subopt_'+split_cnt+'_selmenu';
					var price_id = 'm_'+pid+''+optid+'_subopt_'+split_cnt+'_price';			
					var selmenutd = 'm_'+pid+'_opt_'+optid+'_subopt_'+split_cnt+'_td';
					var txttd = 'm_'+pid+'_opt_'+optid+'_subopt_'+split_cnt+'_txt';
					var htmd = '<table style=\"margin-left:40px;margin-top:10px;\" id=\"'+mtable+'\"><tr><td><div class=\"flex\"><span id=\"'+selmenutd+'\" style=\"width:195px; margin-right:12px;\"><select class=\"js-example-data-array\" id=\"m_'+pid+''+optid+'_subopt_'+split_cnt+'_selmenu\" name=\"m_'+pid+''+optid+'_subopt_'+split_cnt+'_selmenu\" onchange=\"get_menu_price(this.value,\'m_'+pid+''+optid+'_subopt_'+split_cnt+'_price\')\" style=\"width:100%;\"></select></span><span id=\"'+txttd+'\" style=\"display:none; width:175px; margin-right:12px;\"><input type=\"text\" style=\"width:100%;\" id=\"m_'+pid+''+optid+'_subopt_'+split_cnt+'_seltext\" name=\"m_'+pid+''+optid+'_subopt_'+split_cnt+'_seltext\"></span><span style=\"width:35px; margin-right:12px;\"><input style=\"width:100%;\" id=\"m_'+pid+''+optid+'_subopt_'+split_cnt+'_qty\" title=\"Quantity\" name=\"m_'+pid+''+optid+'_subopt_'+split_cnt+'_qty\"onkeypress=\"return validateNumber(event);\"  value=\"1\" type=\"text\" size=\"3\" /></span><input id=\"m_'+pid+''+optid+'_subopt_'+split_cnt+'_deleted\" name=\"m_'+pid+''+optid+'_subopt_'+split_cnt+'_deleted\" value=\"0\" type=\"hidden\" /> <span style=\"margin-right:86px;\"> <input type=\"button\" id=\"m_opt_subopt_'+split_cnt+'\" name=\"m_opt_subopt_'+split_cnt+'\" value=\"+ OPTION\" onclick=\"add_more_subopt(this.id,'+split_cnt+',\''+mtable+'\','+pid+','+startcnt+',\''+settype+'\')\"> </span> <span style=\"width:10.8%;\"> <input style=\"display:none;\" type=\"button\" id=\"m_opt_subopt_'+split_cnt+'\" name=\"m_opt_subopt_'+split_cnt+'\" value=\"+ SUB OPTION\" onclick=\"alert(\'Maximum 3 level is supported\')\"> </span>  <span><input id=\"m_'+pid+''+optid+'_subopt_'+split_cnt+'_price\" name=\"m_'+pid+''+optid+'_subopt_'+split_cnt+'_price\" title=\"Price\" type=\"text\" size=\"4\" /></span><span style=\"margin-left:160px;\"><a class=\"closeBtn\" id=\"m_'+pid+''+optid+'_subopt_'+split_cnt+'_closebtn\" onclick=\"deloption(\''+mtable+'\',\''+deltype+'\')\">X</a></span><span>&nbsp;</span></div></td></tr></table>';
										
					$(htmd).insertAfter('#'+rendertabid);
					get_menu_price(selmenuid,price_id,'add');		
					tablecnt++;
					split_cnt++;
					selectauto('#'+mtable);
					}
							function displayheader(inputcheck,selmenutd,tableid)
							{
						        var x= tableid+'_subopt_';
						        var alltab = $('table[id^=\"'+x+'\"]')
						        
								var res = selmenutd.replace(\"td\", \"txt\");
								var input_price = selmenutd.replace(\"td\", \"price\");
								var qty = selmenutd.replace(\"td\", \"qtytd\");
								var hqty = selmenutd.replace(\"td\", \"hqtytd\");
								var selid = selmenutd.replace(\"td\", \"selmenu\");
								if(inputcheck==true)
								{
									document.getElementById(selmenutd).style.display='none';
									document.getElementById(res).style.display='block';
									//document.getElementById(input_price).value='0';
									document.getElementById(input_price).style.visibility='hidden';
									document.getElementById(qty).style.display='none';
									document.getElementById(hqty).style.display='block';
									for(var i=0; i<alltab.length; i++)
							        {
							          var rendertabid = $(alltab[i]).attr('id');
									  dispsuboption(rendertabid)
							        }
								}
								else
								{
									document.getElementById(selmenutd).style.display='block';
									document.getElementById(res).style.display='none';
									document.getElementById(input_price).style.visibility='visible';
									document.getElementById(qty).style.display='block';
									document.getElementById(hqty).style.display='none';
									get_menu_price(selid,input_price);
									for(var i=0; i<alltab.length; i++)
							        {
							          var rendertabid = $(alltab[i]).attr('id');
									  deloption(rendertabid,'subtab','no')
							        }
					
								}
								selectauto();
							    
							}
							
							function dispsuboption(tableid)
							{
								var res = tableid.replace('mtable_', 'm_');
								res = res+'_deleted';
								if(document.getElementById(res).value==2)
								{
									document.getElementById(res).value=0;
									document.getElementById(tableid).style.display='block';
								}
							}
							
							function deloption(tableid,tabtype,cnf='')
							{
								strconfirm = true;
								if(cnf=='')
								{
									var strconfirm = confirm(\"Are you sure you want to delete?\");
									var delval = 1;
								}
								else
								{
									var delval = 2;
								}
								if (strconfirm == true) 
								{
									if(tabtype=='subtab')
									{
										var res = tableid.replace('mtable_', 'm_');
										res = res+'_deleted';
										console.log(document.getElementById(res).value);
										if(document.getElementById(res).value!=1)
										{
											document.getElementById(res).value=delval;
										}
										document.getElementById(tableid).style.display='none';
									}
									if(tabtype=='opttab' || tabtype=='maintab')
									{	
										var alltab = $('table[id^=\"'+tableid+'\"]')
										for(var i=0; i<alltab.length; i++)
										{
											var tabid = $(alltab[i]).attr('id');
											var res = tabid.replace('mtable_', 'm_');
											res = res+'_deleted';
											document.getElementById(res).value= delval;
											document.getElementById(tabid).style.display='none';
										}
										
									}
								}
								else
								{
									return false;
								}
							}
							";

				$set_js_code = "
					setval = '';
					ing_count = 0;
					for(i=0; i<" . $dname . "_itemnum; i++) {
						iival = document.getElementById('" . $dname . "_item' + i).value;
						iqval = document.getElementById('" . $dname . "_qty' + i).value;
						if(iival!='') {
							if(setval!='') setval += ','; setval += iival + ' x ' + iqval; ing_count++;
						}
					}
					
					if(setval=='') {
						setcol = '#aaaaaa';
						showval = 'Combo Items';
					} else {
						setcol = '#1c591c'; showval = ing_count + ' Ingredient'; if(ing_count > 1) showval += 's';
					}
					window.parent.document.getElementById('" . $item_name . "').value = setval;
					window.parent.document.getElementById('" . $item_name . "' + '_display').style.color = setcol;
					window.parent.document.getElementById('" . $item_name . "' + '_display').value = showval;";

				if (strpos ( $toprint, "<tr>" ) < 0) {
					$toprint .= "
					<tr><td>";
					}




					function categoryParentChildTree($parent = 0, $spacing = '', $category_tree_array = '') {

						if (!is_array($category_tree_array))
							$category_tree_array = array();
							//echo "hhh"; die;

								$sqlCategory = lavu_query("select * from `combo_items` where parent_id ='".$parent."' and combo_menu_id='".$_GET['item_id']."' and `_deleted`=0");


								if($sqlCategory){


								while($rowCategories = mysqli_fetch_assoc($sqlCategory)) {
									if($rowCategories['add_option']==1){

										$level = 'option';
									}
									if($rowCategories['add_sub_option']==1){

										$level = 'suboption';
									}

									$category_tree_array[] = array("id" => $rowCategories['id'], "parent_id" => $rowCategories['parent_id'], "menu_item_id" => $rowCategories['menu_item_id'], "level" => $level, "custom_name" => $rowCategories['custom_category_name'], "qty" => $rowCategories['qty'], "price_adjustment" => $rowCategories['price_adjustment'], "header" => $rowCategories['combo_header'],
									"header_qty" => $rowCategories ['header_qty']
							);$category_tree_array = categoryParentChildTree($rowCategories['id'], '&nbsp;&nbsp;&nbsp;&nbsp;'.$spacing . '-&nbsp;', $category_tree_array);
								}

									return $category_tree_array;
								}
								else{
									return 0;
								}
					}

					function createtable($menu_name,$menu_id,$header_chk,$quantity,$optprice,$deleted_val,$pid,$optid,$suboptid,$type, $hquantity = ''){
					$textdisp = 'style="display:none;width:220px;"';
						$hqtystyle = 'style="display:none;width:5%;"';
						$pricedisp = 'style="visibility:visible;"';
						$ortab = '';
						$suboptstyle = '';
						$subopttdstyle = 'style="width:15%;"';
						$pricetd = 'style = "padding-left: 20px;"';
						$chk = '';
					$style = 'margin-top:10px;';
					$selwidth = '';
					$hqtyatt = 'width:5%;';
					$optstyle = 'padding-left: 12px; width:15%;';
					$closebtnstyle = 'padding-left: 50px;';	if($deleted_val=='' || $deleted_val==0){

							$disp = '';
						}
						else{

							 $disp = 'display:none;';
						}

						if($type=='parent'){
						$selwidth = '220px';
						$textwidth = '95%';
						$seldisp = 'style="display:block;width:' . $selwidth . ';"';
							$tableid = 'mtable_'.$pid;
							$elemid = $pid;
							$optbtnid = 'm_option_'.$pid;
							$suboptbtnid = 'm_subopt_'.$pid;
							$optclik = 'onclick="add_more_opt(this.id,'.$pid.',\'mtable_'.$pid.'\','.$pid.',\'mtext_'.$pid.'\',1)"';
							$suboptclik = 'onclick="add_more_subopt(this.id,'.$pid.',\'mtable_'.$pid.'\','.$pid.',1)"';
							$delopt = 'maintab';
}


						if($type=='option'){
						$selwidth = '220px';
						$textwidth = '95%';
						$seldisp = 'style="display:block;width:' . $selwidth . ';"';
							$tableid = 'mtable_'.$pid.'_opt_'.$optid;
							$elemid = $pid.'_opt_'.$optid;
							$optbtnid = 'm_'.$pid.'_opt_'.$optid;
							$suboptbtnid = 'm_'.$pid.'_subopt_'.$optid;
							$optclik = 'onclick="add_more_opt(this.id,'.$optid.',\'mtable_'.$pid.'_opt_'.$optid.'\','.$pid.',\'mtext_'.$pid.'\',\'\')"';
							$suboptclik = 'onclick="add_more_subopt(this.id,'.$optid.',\'mtable_'.$pid.'_opt_'.$optid.'\','.$pid.',1)"';
							$delopt = 'opttab';
							//$style= 'margin-left:10px;';
							$ortab = '<tr><td>OR</td></tr>';
						}
						if($type=='suboption'){
						$selwidth = '195px';
						$textwidth = '100%';
						$seldisp = 'style="display:block;width:' . $selwidth . ';margin-right:12px;"';
						$qtystyle = 'width:35px;margin-right:12px;';
						$optstyle = 'margin-right:86px;';
						$pricetd = '';
							$suboptstyle='style="display:none;"';
							$subopttdstyle = 'style="width:10.8%;"';
						$closebtnstyle = 'margin-left:160px;';
							if($optid==0){

								$tableid = 'mtable_'.$pid.'_subopt_'.$suboptid;
								$style= 'margin-left:40px;margin-top:10px;';
								$elemid = $pid.'_subopt_'.$suboptid;
								$optbtnid = 'm_'.$pid.'_opt_subopt_'.$suboptid;
								$suboptbtnid = 'm_'.$pid.'_subopt_'.$suboptid;
								$optclik = 'onclick="add_more_subopt(this.id,'.$suboptid.',\'mtable_'.$pid.'_subopt_'.$suboptid.'\','.$pid.','.$suboptid.','.($suboptid+1).',\'subopt_opt\')"';
								$isuboptval = 'm_'.$pid.'_subopt_'.$suboptid;
							}
							else{

								$tableid = 'mtable_'.$pid.'_opt_'.$optid.'_subopt_'.$suboptid;
								$style= 'margin-left:40px;margin-top:10px;';
								$elemid = $pid.'_opt_'.$optid.'_subopt_'.$suboptid;
								$optbtnid = 'm_'.$pid.'_opt_'.$optid.'_subopt_'.$suboptid;
								$suboptbtnid = 'm_'.$pid.'_opt_'.$optid.'_subopt_'.$suboptid;
								$optclik = 'onclick="add_more_subopt(this.id,'.$suboptid.',\'mtable_'.$pid.'_opt_'.$optid.'_subopt_'.$suboptid.'\','.$pid.','.($suboptid+1).',\'\')"';
							}
							$suboptclik = 'onclick="alert(\'Maximum 3 level is supported\')"';
							$delopt = 'subtab';
}


						$tableid = 'mtable_'.$elemid;
						$selid = 'm_'.$elemid.'_selmenu';
						$seltext = 'm_'.$elemid.'_seltext';
						$qty = 'm_'.$elemid.'_qty';
						$hqty = 'm_' . $elemid . '_hqty';
					$deleted = 'm_'.$elemid.'_deleted';
						$price = 'm_'.$elemid.'_price';
						$header = 'm_'.$elemid.'_header';
						if($header_chk=='on'){
						 $chk = 'checked';
						 $seldisp = 'style="display:none;width:' . $selwidth . ';"';
						 $textdisp = 'style="display:block;width:' . $selwidth . ';"';
						 $hqtystyle = 'style="display:block;"';
						$qtystyle = $qtystyle . 'display:none;';
						 $pricedisp = 'style="visibility:hidden;"';
					}
						$chtm.='<table id="'.$tableid.'" style="'.$style.$disp.'"><tbody>'.$ortab.'<tr><td>';
						$chtm.='<div class="flex">';
					$chtm .= '<span id="m_'.$elemid.'_td" '.$seldisp.' ><select style="width:' . $textwidth . ';" class="js-example-data-array" id="'.$selid.'" name="'.$selid.'" onchange="get_menu_price(this.value,\''.$price.'\')"><option value="'.$menu_id.'" selected="selected">'.getmenunamebyid($menu_id).'</option></select></span>';
						$chtm.='<span id="m_'.$elemid.'_txt" '.$textdisp.' ><input style="width:95%;" type="text" id="'.$seltext.'" value="'.$menu_name.'" name="'.$seltext.'"></span>';
						$chtm.='<span id="m_' . $elemid . '_qtytd" style="' . $qtystyle . '"><input id="'.$qty.'" name="'.$qty.'" value="'.$quantity.'" title="Quantity" type="text" size="3"style="width:100%;" onkeypress="return validateNumber(event)";></span>';
					$chtm .= '<span id="m_' . $elemid . '_hqtytd" ' . $hqtystyle . '><input id="' . $hqty . '" name="' . $hqty . '" value="' . $hquantity . '" title="Quantity" type="text" size="3" style="width:100%;" onkeypress="return validateNumber(event)";></span>';
						$chtm.='<input id="'.$deleted.'" name="'.$deleted.'" value="'.$deleted_val.'" type="hidden">';
						$chtm.='<span style="' . $optstyle . '"> <input type="button" id="'.$optbtnid.'" name="'.$optbtnid.'" value="+ OPTION" '.$optclik.'> </span>';
						$chtm.='<span '.$subopttdstyle.'> <input '.$suboptstyle.' type="button" id="'.$suboptbtnid.'" name="'.$suboptbtnid.'" value="+ SUB OPTION" '.$suboptclik.' > </span>';
						$chtm.='<span '.$pricetd.'><input '.$pricedisp.' id="'.$price.'" name="'.$price.'" title="Price" value="'.$optprice.'" type="text" size="4"></span>';
						if($type=='suboption'){
							$chtm.='<span>&nbsp;</span>';
						}else{

							$chtm.='<span style="padding-left: 50px;"><label>Header</label>';
							$chtm.='<input type="checkbox" id="'.$header.'" name="'.$header.'" '.$chk.' onclick="displayheader(this.checked,\'m_'.$elemid.'_td\',\'' . $tableid . '\')" > </span>';
						}
						$chtm.='<span style="' . $closebtnstyle . '"><a class="closeBtn" id="m_' . $elemid . '_closebtn" onclick="deloption(\'' . $tableid . '\',\'' . $delopt . '\')">X</a></span>';
					$chtm .= '</td></tr></tbody></table>';
						return $chtm;
					}
					
					function chkparentid($pid){

						$sqlCategory = lavu_query("select parent_id from `combo_items` where id ='".$pid."'");
						$rowCategories = mysqli_fetch_assoc($sqlCategory);
						return $rowCategories['parent_id'];
					}
					function getmenunamebyid($mid){

						$sqlCategory = lavu_query("select name from `menu_items` where id ='".$mid."'");
						$rowCategories = mysqli_fetch_assoc($sqlCategory);
						return $rowCategories['name'];}
				function getmenuimpportmenupricebyid($mid) {
					$sqlimportprice = lavu_query ( "select import_menu_price from `menu_items` where id ='" . $mid . "'" );
					$rowimportprice = mysqli_fetch_assoc ( $sqlimportprice );
					return $rowimportprice ['import_menu_price'];
				}
				function combo_format_currency($monitary_amount, $dont_use_monetary_symbol = false, $monitary_symbol = "") {
					global $location_info;

					$needToAddMinus = "";
					$number = $monitary_amount;

					$precision = isset ( $location_info ['disable_decimal'] ) ? $location_info ['disable_decimal'] : 2;
					if ($location_info ['decimal_char'] != "") {
						$decimal_char = isset ( $location_info ['decimal_char'] ) ? $location_info ['decimal_char'] : '.';
						$thousands_char = (isset ( $location_info ['decimal_char'] ) && $location_info ['decimal_char'] == ".") ? "," : '.';
					}
$thousands_sep = ",";
					if (! empty ( $thousands_char )) {
						$thousands_sep = $thousands_char;
					}

					if (strpos ( $number, "-" ) !== false) {
						$find_minus = explode ( "-", $number );
						$needToAddMinus = "-";
					}

					if ($monitary_symbol === "") {
						$monitary_symbol = isset ( $location_info ['monitary_symbol'] ) ? htmlspecialchars ( $location_info ['monitary_symbol'] ) : '';
					}
					$left_or_right = isset ( $location_info ['left_or_right'] ) ? $location_info ['left_or_right'] : 'left';

					$left_side = "";
					$right_side = "";

					if (! $dont_use_monetary_symbol) {
						if ($left_or_right == 'left') {
							$left_side = $monitary_symbol;
						} else {
							$right_side = $monitary_symbol;
						}
					}

					$output = number_format ( $number, $precision, $decimal_char, $thousands_sep ) . $right_side;
					return $output;
				}
					$pid=0;
					$optid=0;
					$suboptid=0;
					$hidarr = array();
					if(isset($_SESSION['poslavu_234347273_combo']) && array_key_exists($_GET['item_name'],$_SESSION['poslavu_234347273_combo'])){


						$json = $_SESSION['poslavu_234347273_combo'][$_GET['item_name']];

						$test = json_decode($json,1);
						$i=1;
						$chtm.='';
						foreach ($test as $key=>$data){
							$menu="m_".$i;
							$menu_id="m_".$i."_selmenu";
							$menu_name="m_".$i."_seltext";
							$menu_header="m_".$i."_header";
							$menu_qty="m_".$i."_qty";
							$menu_hqty = "m_" . $i . "_hqty";
						$menu_price="m_".$i."_price";
							$menu_deleted="m_".$i."_deleted";
							if(array_key_exists($menu_id,$test)){
								$pid = $pid + 1;
								$hidarr[$i] = 0;
								$chtm.= createtable($test[$menu_name],$test[$menu_id],$test[$menu_header],$test[$menu_qty],$test[$menu_price],$test[$menu_deleted],$i,0,0,'parent', $test [$menu_hqty]);
								for($j=1; $j<count ( $test ); $j++ ){
									$menu_subopt=$menu."_subopt_".$j;
									$menu_subopt_id=$menu_subopt."_selmenu";
									$menu_subopt_name=$menu_subopt."_seltext";
									$menu_subopt_header=$menu_subopt."_header";
									$menu_subopt_qty=$menu_subopt."_qty";
									$menu_subopt_price=$menu_subopt."_price";
									$menu_subopt_deleted=$menu_subopt."_deleted";

									if(array_key_exists($menu_subopt_id,$test)){
										$chtm.= createtable($test[$menu_subopt_name],$test[$menu_subopt_id],$test[$menu_subopt_header],$test[$menu_subopt_qty],$test[$menu_subopt_price],$test[$menu_subopt_deleted],$i,0,$j,'suboption');
									}else {
break;
								}
							}
						} else {
							break;
						}
						for($j = 1; $j < count ( $test ); $j ++) {
							$menu_opt = $menu . "_opt_" . $j;
							$menu_opt_id = $menu_opt . "_selmenu";
							$menu_opt_name = $menu_opt . "_seltext";
							$menu_opt_header = $menu_opt . "_header";
							$menu_opt_qty = $menu_opt . "_qty";
							$menu_opt_hqty = $menu_opt . "_hqty";
							$menu_opt_price = $menu_opt . "_price";
							$menu_opt_deleted = $menu_opt . "_deleted";

							if (array_key_exists ( $menu_opt_id, $test )) {
								$optid = $optid + $j;
								$hidarr [$i] = $j;
								$chtm .= createtable ( $test [$menu_opt_name], $test [$menu_opt_id], $test [$menu_opt_header], $test [$menu_opt_qty], $test [$menu_opt_price], $test [$menu_opt_deleted], $i, $j, 0, 'option', $test [$menu_opt_hqty] );
								for($k = 1; $k < count ( $test ); $k ++) {
									$menu_opt_subopt = $menu_opt . "_subopt_" . $k;
									$menu_opt_subopt_id = $menu_opt_subopt . "_selmenu";
									$menu_opt_subopt_name = $menu_opt_subopt . "_seltext";
									$menu_opt_subopt_header = $menu_opt_subopt . "_header";
									$menu_opt_subopt_qty = $menu_opt_subopt . "_qty";
									$menu_opt_subopt_price = $menu_opt_subopt . "_price";
									$menu_opt_subopt_deleted = $menu_opt_subopt . "_deleted";

									if (array_key_exists ( $menu_opt_subopt_id, $test )) {
										$chtm .= createtable ( $test [$menu_opt_subopt_name], $test [$menu_opt_subopt_id], $test [$menu_opt_subopt_header], $test [$menu_opt_subopt_qty], $test [$menu_opt_subopt_price], $test [$menu_opt_subopt_deleted], $i, $j, $k, 'suboption' );
									} else {
										break;
									}
								}
} else {
								break;
							}
						}
							$i++;
						}
					}
					else{
					if($_GET ['item_id'] !=0){
						$categoryList = categoryParentChildTree();
}
						$newCategoryarr = array();
						foreach($categoryList as $k=>$v){

							if($v['parent_id'] == 0){

								$newCategoryarr[] = $v;
								foreach($categoryList as $k1=>$v1){

									if(@$v['id'] == $v1['parent_id'] &&  $v1['level'] == 'suboption'){

										$newCategoryarr[] = $v1;
									}
								}
								foreach($categoryList as $k2=>$v2){

									if(@$v['id'] == $v2['parent_id'] &&  $v2['level'] == 'option'){

										$newCategoryarr[] = $v2;
										foreach($categoryList as $k3=>$v3){

											if(@$v2['id'] == $v3['parent_id'] &&  $v3['level'] == 'suboption'){

												$newCategoryarr[] = $v3;
											}
										}
									}
								}
							}
						}


						if($newCategoryarr!=0){


							$tableid = '';

							$chtm = '';
							$subparent_id = 0;
							$optparent_id = 0;

							foreach($newCategoryarr as $key => $value){
								$hqtystyle = 'style="display:none;width:5%;"';
								$textdisp = 'style="display:none;"';
								$pricedisp = 'style="visibility:visible;"';
								$ortab = '';
								$suboptstyle='';
								$subopttdstyle= 'style="width:15%;"';
								$pricetd = 'style="padding-left: 20px;"';
								$chk = '';
								$style = 'style="margin-top:10px;"';
							$selwidth = '';
							$qtystyle = 'width:5%;';
							$optstyle = 'padding-left: 12px; width:15%;';
							$closebtnstyle = 'padding-left: 50px;';
								if($value['parent_id']==0){
								$selwidth = '220px';
								$textwidth = '95%';
								$seldisp = 'style="display:block;width:' . $selwidth . ';"';
									if($optparent_id==$value['parent_id']){

										$pid++;
									}
									else{
										$optparent_id = $value['parent_id'];
										$suboptid = 0;
										$pid++;
									}
									$tableid = 'mtable_'.$pid;
									$elemid = $pid;
									$optbtnid = 'm_option_'.$pid;
									$suboptbtnid = 'm_subopt_'.$pid;
									$optclik = 'onclick="add_more_opt(this.id,'.$pid.',\'mtable_'.$pid.'\','.$pid.',\'mtext_'.$pid.'\',1)"';
									$suboptclik = 'onclick="add_more_subopt(this.id,'.$pid.',\'mtable_'.$pid.'\','.$pid.',1)"';
									$hidarr[$pid] = 0;
									$delopt = 'maintab';
								}

								if($value['parent_id']!=0 && $value['level']=='option'){
								$selwidth = '220px';
								$textwidth = '95%';
								$seldisp = 'style="display:block;width:' . $selwidth . ';"';
									if($npid == $pid){


										$optid++;
									}
									else{



										$npid = $pid;
										$optid = 0;
										$optid++;
									}
									$hidarr[$pid] = $optid;

									$tableid = 'mtable_'.$pid.'_opt_'.$optid;
									$elemid = $pid.'_opt_'.$optid;
									$optbtnid = 'm_'.$pid.'_opt_'.$optid;
									$suboptbtnid = 'm_'.$pid.'_subopt_'.$optid;
									$optclik = 'onclick="add_more_opt(this.id,'.$optid.',\'mtable_'.$pid.'_opt_'.$optid.'\','.$pid.',\'mtext_'.$pid.'\',\'\')"';
									$suboptclik = 'onclick="add_more_subopt(this.id,'.$optid.',\'mtable_'.$pid.'_opt_'.$optid.'\','.$pid.',1)"';
									$delopt = 'opttab';
									$ortab = '<tr><td>OR</td></tr>';
								}
								if($value['parent_id']!=0 && $value['level']=='suboption'){
								$selwidth = '195px';
								$textwidth = '100%';
								$seldisp = 'style="display:block;width:' . $selwidth . ';margin-right:12px;"';
								$qtystyle = 'width:35px;margin-right:12px;';
								$optstyle = 'margin-right:86px;';
								$pricetd = '';
									$chkparent = chkparentid($value['parent_id']);
									$suboptstyle = 'style="display:none;"';
									$subopttdstyle = 'style="width:10.8%;"';
								$closebtnstyle = 'margin-left:160px;';
									if($subparent_id==$value['parent_id']){

										$suboptid++;
									}
									else{
										$subparent_id = $value['parent_id'];
										$suboptid = 0;
										$suboptid++;
									}

									if($chkparent==0){

										$tableid = 'mtable_'.$pid.'_subopt_'.$suboptid;
										$style = 'style="margin-left:40px;margin-top:10px;"';
										$elemid = $pid.'_subopt_'.$suboptid;
										$optbtnid = 'm_'.$pid.'_opt_subopt_'.$suboptid;
										$suboptbtnid = 'm_'.$pid.'_subopt_'.$suboptid;
										$optclik = 'onclick="add_more_subopt(this.id,'.$suboptid.',\'mtable_'.$pid.'_subopt_'.$suboptid.'\','.$pid.','.$suboptid.','.($suboptid+1).',\'subopt_opt\')"';
										$isuboptval = 'm_'.$pid.'_subopt_'.$suboptid;
									}
									else{

										$tableid = 'mtable_'.$pid.'_opt_'.$optid.'_subopt_'.$suboptid;
										$style = 'style="margin-left:40px;margin-top:10px;"';
										$elemid = $pid.'_opt_'.$optid.'_subopt_'.$suboptid;
										$optbtnid = 'm_'.$pid.'_opt_'.$optid.'_subopt_'.$suboptid;
										$suboptbtnid = 'm_'.$pid.'_opt_'.$optid.'_subopt_'.$suboptid;
										$optclik = 'onclick="add_more_subopt(this.id,'.$suboptid.',\'mtable_'.$pid.'_opt_'.$optid.'_subopt_'.$suboptid.'\','.$pid.','.($suboptid+1).',\'\')"';
									}
									$suboptclik = 'onclick="alert(\'Maximum 3 level is supported\')"';
									$delopt = 'subtab';
}


								$tableid = 'mtable_'.$elemid;
								$selid = 'm_'.$elemid.'_selmenu';
								$seltext = 'm_'.$elemid.'_seltext';
								$qty = 'm_'.$elemid.'_qty';
								$hqty = 'm_' . $elemid . '_hqty';
							$deleted = 'm_'.$elemid.'_deleted';
								$price = 'm_'.$elemid.'_price';
								$header = 'm_'.$elemid.'_header';
								$sel_opt = '<option value="'.$value['menu_item_id'].'" selected="selected">'.getmenunamebyid($value['menu_item_id']).'</option>';
								if($value['header']==1){
									$chk = 'checked';
									$seldisp = 'style="display:none;width:' . $selwidth . ';"';
									$textdisp = 'style="display:block;width:' . $selwidth . ';"';
									$pricedisp = 'style="visibility:hidden;"';
								$qtystyle = $qtystyle . 'display:none;';
									$hqtystyle = 'style="display:block;"';
									$sel_opt = '';
								}

								$chtm.='<table id="'.$tableid.'" '.$style.'><tbody>'.$ortab.'<tr><td>';
								$chtm.='<div class="flex">';
							$chtm .= '<span id="m_'.$elemid.'_td" '.$seldisp.' ><select style="width:' . $textwidth . ';" class="js-example-data-array" id="'.$selid.'" name="'.$selid.'" onchange="get_menu_price(this.value,\''.$price.'\')">'.$sel_opt.'</select></span>';
								$chtm.='<span id="m_'.$elemid.'_txt" '.$textdisp.' ><input style="width:95%;" type="text" id="'.$seltext.'" value="'.$value['custom_name'].'" name="'.$seltext.'"></span>';
								$chtm.='<span id="m_' . $elemid . '_qtytd" style="' . $qtystyle . '"><input id="'.$qty.'" name="'.$qty.'" value="'.$value['qty'].'" title="Quantity" type="text" size="3"style="width:100%;" onkeypress="return validateNumber(event)";></span>';
							$chtm .= '<span id="m_' . $elemid . '_hqtytd" ' . $hqtystyle . '><input id="' . $hqty . '" name="' . $hqty . '" value="' . $value ['header_qty'] . '" title="Quantity" type="text" size="3" style="width:100%;" onkeypress="return validateNumber(event)";></span>';
								$chtm.='<input id="'.$deleted.'" name="'.$deleted.'" value="0" type="hidden">';
								$chtm.='<span style="' . $optstyle . '"> <input type="button" id="'.$optbtnid.'" name="'.$optbtnid.'" value="+ OPTION" '.$optclik.'> </span>';
								$chtm.='<span '.$subopttdstyle.'> <input '.$suboptstyle.' type="button" id="'.$suboptbtnid.'" name="'.$suboptbtnid.'" value="+ SUB OPTION" '.$suboptclik.' > </span>';
								$chtm.='<span '.$pricetd.'><input '.$pricedisp.' id="'.$price.'" name="'.$price.'" title="Price" value="'.combo_format_currency ($value['price_adjustment']).'" type="text" size="4"></span>';
								if($value['level']=='suboption'){
								//
									$chtm.='<span>&nbsp;</span>';
								}else{

									$chtm.='<span style="padding-left: 50px;"><label> Header</label>';
									$chtm.='<input type="checkbox" id="'.$values['header'].'" name="'.$header.'" '.$chk.' onclick="displayheader(this.checked,\'m_'.$elemid.'_td\',\'' . $tableid . '\')" > </span>';
								}
								$chtm.='<span style="' . $closebtnstyle . '"><a class="closeBtn" id="m_' . $elemid . '_closebtn" onclick="deloption(\'' . $tableid . '\',\'' . $delopt . '\')">X</a></span>';
							$chtm .= '</td></tr></tbody></table>';
							}
						}
					}
					foreach($hidarr as $arrkey=>$hidfield){

						$toprint.="<input type='hidden' id='mtext_".$arrkey."' value='".$hidfield."' >";}
				if (isset ( $test ['temp_id'] )) {
					$test1 = "qweqe";
					if ($test [import_price] == 'on') {
						$test1 .= "yes1235";
						$import_check = 'checked';
					}
} // getmenuimpportmenupricebyid
else {
					$import_menu_price = getmenuimpportmenupricebyid ( $_GET ['item_id'] );
					if ($import_menu_price == 1) {
						$import_check = 'checked';
					}
				}
					$toprint .="<tr><td colspan='7' align='right'><input type='checkbox' $import_check  name='import_price'id='import_price' onclick='import_item_price()' >Import Menu Price</td></tr>";
					$toprint .= "<tr><td style='font-size:10px;width:200px;'>Menu Item / Header</td><td style='font-size:10px;width:35px;'>Qty</td><td style='font-size:10px;width:86px'>+ Option</td> <td style='font-size:10px;width:110px'> + Sub Option </td> <td style='font-size:10px;width:14%'> Adjust Price</td><td style='font-size:10px;'>Make Header</td></tr>
					";
				$toprint .= "
							</table><div id=\"comboitems\">" . $chtm . "</div>
						 <input type=\"hidden\" id=\"ival\" value=\"" . ($pid + 1) . "\" >
				         <input type=\"hidden\" id=\"itable\" value=\"" . ($pid + 1) . "\" />
				         
				         <input type=\"hidden\" id=\"ioptval\" value=\"\" />
				         <input type=\"hidden\" id=\"ioptcount\" value=\"" . $optid . "\" />
				         
				          <input type=\"hidden\" id=\"isuboptval\" value=\"" . $isuboptval . "\" />
				         
						 <input type=\"hidden\" name=\"main_menu_id\" value='" . $_GET ['item_id'] . "'/>
						 <input type=\"hidden\" name=\"temp_id\" value='" . $_GET ['item_name'] . "'/>
						 		
				         <input type=\"button\" id=\"addmorebtn\" value=\"Add More >>\" onclick=\"add_more_comboitems('parent',this.id)\">
								
					";
			} else if ($dtype == "extra") {
				$get_js_code = "d_extra_values = window.parent.document.getElementById('" . str_replace ( "details", $dname, $item_name ) . "').value; ";
				$get_js_code .= "function read_extra_value(dext,edname) { ";
				$get_js_code .= " dext = dext.split('[[' + edname + ']]'); ";
				$get_js_code .= " if(dext.length > 1) { ";
				$get_js_code .= "   dext = dext[1]; ";
				$get_js_code .= "   dext = dext.split('{{'); ";
				$get_js_code .= "   if(dext.length > 1) { ";
				$get_js_code .= "     dext = dext[1]; ";
				$get_js_code .= "     dext = dext.split('}}'); ";
				$get_js_code .= "     return dext[0]; ";
				$get_js_code .= "   } ";
				$get_js_code .= " } ";
				$get_js_code .= " return ''; ";
				$get_js_code .= "} ";

				$set_js_code = "ds_extra_values = ''; ";

				for($n = 0; $n < count ( $efields ); $n ++) {
					$efield = trim ( $efields [$n] );
					if ($efield != "") {
						$edname = $dname . "_" . $n;
						$efield_ins = str_replace ( '"', '&quot;', $efield );
						$fieldval = get_multi_field_value ( $efield, $dval, "" );
						$fieldval = str_replace ( '"', '&quot;', $fieldval );
						$toprint .= drow_code ( $efield, "<input type='text' name='$edname' value=\"$fieldval\">" );
						$get_js_code .= "document.dform.$edname.value = read_extra_value(d_extra_values,\"$efield_ins\"); ";
						$set_js_code .= "ds_extra_values += \"[[$efield_ins]]{{\" + document.dform.$edname.value + \"}}\"; ";
					}
				}
				$set_js_code .= "window.parent.document.getElementById('" . str_replace ( "details", $dname, $item_name ) . "').value = ds_extra_values; ";
			}

			$js_getvals .= $get_js_code;
			$js_setvals .= $set_js_code;
			$toprint .= "</td>";
			$toprint .= "</tr>";
		}
		if ($dtype == "comboitems") {
			$toprint .= drow_code ( "", "<br><br><input type='button' value='Apply' onclick='apply_detail_comboupdate();'>" );
		}

		else if ($dtype == "dietary_info" && $modules->hasModule("extensions.ordering.kiosk")) {
			$btn_label = ($item_id === '0') ? speak("Close") : speak("Save Changes");
			$toprint .= drow_code ( "", "<input type='submit' name='submit_dietary' value='{$btn_label}' onclick='window.parent.info_visibility()' />");
		}

		else if ($dtype == "nutrition" && $modules->hasModule("extensions.ordering.kiosk")) {
			$btn_label = ($item_id === '0') ? speak("Close") : speak("Save Changes");
			$toprint .= drow_code ( "", "<input type='submit' name='submit_kiosk_nutrition' id='close_nutrition' value='{$btn_label}' onclick='window.parent.info_visibility()' />" );
		}

		else {
			$toprint .= drow_code ( "", "<input type='button' value='Apply' onclick='apply_detail_update();'>" );
		}
		$srclink = '';
		$select2 = '';
		if ($dtype == "comboitems") {
			$srclink = "<link href='/cp/css/select2-4.0.3.min.css' rel='stylesheet' /><script src='/cp/scripts/select2-4.0.3.min.js'></script>";

			$select2 = "
						$( document ).ready(function() {
							selectauto();
						});
						function selectauto(comid)
						{
							var wrapper = $(document.body);
							if (comid) {wrapper = $(comid);}
							var data = $menu_json;
						    $(\".js-example-data-array\", wrapper).select2({
	  						   data: data
							})
						}
						
						function get_menu_price(menu_id,price_id,type='')
						{
							if(document.getElementById('import_price').checked==true)
							{
								var Json = " . $item_price_json . ";
								if(type=='')
								{
									 for (var i = 0; i < Json.length; i++) {
								        var element = Json[i];
								        if (element.id == menu_id) {
										   document.getElementById(price_id).value = element.item_price;
								       }
								    }
								}
								else
								{
								    var element = Json[0];
									document.getElementById(price_id).value = element.item_price;
								
								}
							}
							
						}
				";
		}
		$toprint .= "
				</table>
			</form>
					$srclink
			<script type='text/javascript'>
				function validateNumber(evnt){
	                var iKeyCode = evnt.which || evnt.keyCode;
	                    if (iKeyCode == 45) {return false;}
	                }
                function validateNumberNutrition(evnt){
                    var iKeyCode = evnt.which || evnt.keyCode;
                    if (iKeyCode == 45 || iKeyCode == 34 || iKeyCode == 35 || iKeyCode == 39 || iKeyCode == 33 || iKeyCode == 44 || iKeyCode == 94 || iKeyCode == 42) {
                        return false;
                    }
                }
                        
					$select2
				function carryValueOrSetDefault(dname, element, cval) {

					if (element.nodeName == 'SELECT') {
						var defv = '';
						var has_cval = false;
						for (var i = 0; i < element.length; i++) {
							var optv = element.options[i].value;
							if (i == 0) defv = optv;
							if (optv == cval) {
								has_cval = true;
								break;
							}
						}
						if (!has_cval) {
							if (dname == 'printer') {
								var opt_text = 'kitchen';
								if (cval > 1) opt_text = 'kitchen' + cval;
								var new_opt = document.createElement(\"option\");
								new_opt.text = opt_text + ' (undefined)';
								new_opt.value = cval;
								element.add(new_opt);
							} else cval = defv;
						}
					}

					element.value = cval;
				}

				" . $js_getvals . "
				var text = '#dForm [name=kitchen_nickname]';
				var iframeBody = document.getElementsByTagName(\"body\")[0];
				var parentjQuery = function (selector) { return parent.jQuery(selector, iframeBody); };
				function validateName(value) {
					if(!isNaN(value) || value.length > 30) {
						$('#dForm').append('<div id=\'verify\'><div><h4> Kitchen nickname should be 30 characters long  with at least one alphanumeric character or symbol</h4></div></div>');
						parentjQuery('#verify').dialog({
							draggable: false,
	                       	resizable: false,
			                height: 'auto',
			                width: '30em',
			                modal: true,
			                title: 'Warning',
			                buttons : {
		                       'Ok': function() {
		                       		parentjQuery(this).remove();
		                       		return false;
			                    }
	                       	}
                      	});
                      	return false;
                   	}
					else {
						return true;
					}				
				}
				var isNameAvailable= false;
				$('#dForm [name=kitchen_nickname]').live('focus', function(){
      				$(this).attr('oldValue',$(this).val());
				});

				function checkDuplicateNickName(callBack) {
					var oldValue = $(text).attr('oldValue');
  					var currentValue = $(text).val();
  					if(oldValue !=undefined && oldValue != currentValue) {
						$.ajax({
				            url: 'validateMenuItemNickname.php?value='+currentValue,
				            type: 'GET',
				            success: function(response) { 
				              data = JSON.parse(response);
				              isNameAvailable = data['nameExist'] ? true : false;
				              callBack(isNameAvailable);
			            	},
			            	error: function() {
			            		callBack(false);
			            	}
          				}); 
  					} else {
  						callBack(false);
  					}
				}

				function apply_detail_update() {
					var fieldvalue = $(text).val();
					if(fieldvalue) {
						var nickname = validateName(fieldvalue);
						if(!nickname) {
							return false;
						} else {
							checkDuplicateNickName(function(isNickNameAvailable) {
								if(isNickNameAvailable === true) {
									$('#verify-dialogue').remove();
									$('#dForm').append('<div id=\'verify-dialogue\'><div><h4>The entered Kitchen Nickname is the same as another items Kitchen Nickname. Are you sure you want to proceed?</h4></div></div>');
								   	parentjQuery('#verify-dialogue').dialog({
								        modal: true, title: 'Warning', zIndex: 10000, autoOpen: true,
								        width: 'auto', resizable: false,
								        draggable: false,
								        dialogClass: 'custom-ui-widget-header-accessible',
								        buttons: {
								            Yes: function () {
								            	" . $js_setvals . "
								            	parentjQuery(this).remove();
												window.parent.info_visibility();
												return true;
								            },
								            No: function () {
								            	$(text).val($(text).attr('oldValue'));
								            	parentjQuery(this).remove();
								            	window.parent.info_visibility();
								            	return false;
											}
				            			},
				            			close: function (event, ui) {
					            			$('#verify-dialogue').remove();
					            		}	
					        		});
					        		return false;
								} else {
									" . $js_setvals . "
									window.parent.info_visibility();
									return true;
								}
							});
						}
					} else {
						" . $js_setvals . "
						if (typeof ingredients_itemnum !== 'undefined') {
	                        if (ingredients_itemnum < 1){
			                    alert('At least one ingredient should required.');
			                    window.parent.info_visibility();
			                    return false;
	                        }
	                    }
						window.parent.info_visibility();
						return true;
					}
  				} 

				function import_item_price()
				{
						if(document.getElementById('import_price').checked==true)
						{
						   $.ajax({
						   type: 'POST',  // http method
						   url: 'import_item_price_combo.php',
						   data:  $('#dForm').serialize(),
							  error: function() {
						      //alert('ffalse');
						   },
						   success: function(data) {
							if(data!='0')
							{
								var Json = data;
								var result = $.parseJSON(Json);
								$.each(result, function(k, v) {
									document.getElementById(k).value = v;
								});
							}
						   }
						   
								
						});
					}
				}
				function apply_detail_comboupdate(){
				
					$.ajax({
							type: 'POST',  // http method
					   url: 'add_combo.php',
					   data:  $('#dForm').serialize(),
						  error: function() {
					      //alert('ffalse');
					   },
					   success: function(data) {
						if(data=='error')
							{
								alert('Combo header should have sub options');
							    return false;
							}
						if(data=='header_not_set')
							{
								alert('Only Combo Header can have the Sub-Option');
							    return false;
							}	
						else if(data=='empty')
							{
								alert('Menu item / Header should not be empty');
							    return false;
							}
						else if(data=='NoItem'){
								alert('At least one combo item should required.');
								return false;
                          }
						else{
							window.parent.info_visibility()
						  }
					    console.log(data);
					   }
					   
							
					});	
				}
				
				function import_item_price()
				{
						if(document.getElementById('import_price').checked==true)
						{
						   $.ajax({
						   type: 'POST',  // http method
						   url: 'import_item_price_combo.php',
						   data:  $('#dForm').serialize(),
							  error: function() {
						      //alert('ffalse');
						   },
						   success: function(data) {
							if(data!='0')
							{
								var Json = data;
								var result = $.parseJSON(Json);
								$.each(result, function(k, v) {
									document.getElementById(k).value = v;
								});
							}
						   }
						   
								
						});
					}
				}
				function apply_detail_comboupdate(){
				
					$.ajax({
							type: 'POST',  // http method
					   url: 'add_combo.php',
					   data:  $('#dForm').serialize(),
						  error: function() {
					      //alert('ffalse');
					   },
					   success: function(data) {
						if(data=='error')
							{
								alert('Combo header should have sub options');
							    return false;
							}
						if(data=='header_not_set')
							{
								alert('Only Combo Header can have the Sub-Option');
							    return false;
							}	
						else if(data=='empty')
							{
								alert('Menu item / Header should not be empty');
							    return false;
							}
						else if(data=='NoItem'){
								alert('At least one combo item should required.');
								return false;
                          }
						else{
							window.parent.info_visibility()
						  }
					    console.log(data);
					   }
					   
							
					});	
				}
				
				function import_item_price()
				{
						if(document.getElementById('import_price').checked==true)
						{
						   $.ajax({
						   type: 'POST',  // http method
						   url: 'import_item_price_combo.php',
						   data:  $('#dForm').serialize(),
							  error: function() {
						      //alert('ffalse');
						   },
						   success: function(data) {
							if(data!='0')
							{
								var Json = data;
								var result = $.parseJSON(Json);
								$.each(result, function(k, v) {
									document.getElementById(k).value = v;
								});
							}
						   }
						   
								
						});
					}
				}
			</script>";
		}

	}else $toprint .= "Invalid mode: $mode";

$toprint .= "</body>";

	echo $toprint;
	echo isset($ifNotPassbook_js) ? $ifNotPassbook_js : "";

	function set_inventory_picture_tohtml($extra_apply_code, $fwd_picture_vars, $tab_text, $extra_onchange_code, &$piclist, $update_message, $selected_picture, $updateid="",$category_name="", $category_id = "") {

		$toprint = '';

		$picmode = (isset($_GET['picmode']))?$_GET['picmode']:"main";

		$toprint .= "<form name='upload' enctype='multipart/form-data' method='post' action='form_dialog.php?mode=set_inventory_picture&updateid=$updateid".$fwd_picture_vars."'>";

		$toprint .= $tab_text;

		$toprint .= "<br><br><br>";
		// We need to keep #picture_controls
		$toprint .= "<div id='picture_controls'>";
		$toprint .= "<table style='border:solid 1px black' cellpadding=12><td>";
		$toprint .= "<select id='file_selector' onchange='window.location = \"form_dialog.php?mode=set_inventory_picture&updateid=$updateid&tab_selected=\" + escape(document.getElementById(\"tab_selected\").value) + \"&setfile=\" + escape(this.value) + \"".$fwd_picture_vars.$extra_onchange_code."\"'>";
		$toprint .= "<option value='none'></option>";
		for($x=0; $x<count($piclist); $x++){

			$fname = $piclist[$x];
			$toprint .= "<option value='$fname'";
			if($fname==$selected_picture) $toprint .= " selected";
			$toprint .= ">$fname</option>";
		}
		$toprint .= "</select>";
		$toprint .= "<br><br>";
		if(isset($update_message) && $update_message!="") $toprint .= $update_message;
		$toprint .= "Upload: <input type='file' name='picture' onChange='document.upload.submit()' style='color:#bbbbbb'>";
		$toprint .= "</td></table>";
		$toprint .= "</div>";
		// end of #picture_controls

        $fwd_pic_url = "form_dialog.php?mode=set_inventory_picture&updateid=$updateid".$fwd_picture_vars.$extra_onchange_code;
        $toprint .= "<input type='button' value='Make your Own' onclick='window.location = \"$fwd_pic_url&picmode=generate\"' />";

		$toprint .= "</form>";

		ob_start();
		include 'thumbnail-generator/set-inventory-picture.php';
		$toprint = ob_get_clean();

		if($picmode=="generate")
		{
		    // Insert thumbnail generator code HERE!!
		    $thumbnail_code_path = "thumbnail-generator/thumbnail-generator.php";
		    ob_start();
		    include $thumbnail_code_path;
		    $thumbnail_generator_html = ob_get_clean();
            //comment this line below to make the thumbnail generator go away
			$toprint = $thumbnail_generator_html;

			$create_image_folder = $_SERVER['DOCUMENT_ROOT'] . $_GET['dir'];
			$item_name_id = str_replace("image","name",$_GET['updateid']);
            $toprint .= <<<JS
<script language='javascript'>
   var item_name = window.parent.document.getElementById('$item_name_id'.replace( '_col_name', '_category' ) ).value;
</script>
JS;
		// $toprint .= "<br><br><input type='button' value='Test Setting Picture' onclick='window.location = \"$fwd_pic_url&setfile=images-10.jpg\"' />";
	} else if ($picmode == "finish_thumbnail") {

		include "thumbnail-generator/image-processor.php";
		if ($GLOBALS ["thumbnail-success"]) {
			$toprint = <<<HTML
  <div id="thumbnail-success-message">
     <h2 style="text-align:center;color:gray;">Thumbnail created!</h2>
  </div>
<script>

window.parent.set_inv_picture( "{$_GET['updateid']}", "{$GLOBALS['new_filename']}", "{$_GET['dir']}{$GLOBALS['new_filename']}");
</script>
HTML;
		}
	}

	return $toprint;
}
function set_inventory_picture_grid_tohtml($s_picpath, &$a_piclist, $fwd_picture_vars, $selected_picture, $updateid, $extra_apply_code) {
	$s_retval = "";

	// draw some scripts
	$s_retval .= "
			<script type='text/javascript' src='https://ajax.googleapis.com/ajax/libs/jquery/1.6.2/jquery.min.js'></script>
			<script type='text/javascript'>
				setTimeout('createFunctions();', 500);
				setTimeout('createFunctions();', 1000);
				setTimeout('createFunctions();', 2000);
				setTimeout('createFunctions();', 4000);
				setTimeout('createFunctions();', 8000);

				function createFunctions() {
					window.getSelectedName = function() {
						var jdiv = $('div.pic_div.pic_selected');
						var name = jdiv.find('input[name=name]').val();
						return name;
					}

					window.selectAppliedPicture = function(element) {
						var jdiv = $(element);
						var jselected = $('div.pic_div.pic_selected');
						var jname = $('#pic_selected_name_display');
						jselected.removeClass('pic_selected');
						jdiv.addClass('pic_selected');
						jname.html(getSelectedName());
						setTimeout('applyPicture();', 100);
					}

					window.applyPicture = function() {
						window.parent.set_inv_picture('{$updateid}',getSelectedName(),'{$s_picpath}'+getSelectedName()+'{$extra_apply_code}');
					}
				}
			</script>";

	// draw the styling
	$s_retval .= "
			<style type='text/css'>
				.picture_div {
					max-width:120px;
					max-height:60px;
					position:relative;
					top:-60px;
				}
				div.pic_div {
					width:120px;
					height:60px;
					padding:10px;
					border:0px solid none;
					display:inline-block;
					text-align:center;
				}
				div.pic_div.pic_selected {
					padding:8px;
					border:2px solid white;
					background-color:rgba(255,255,255,0.25);
				}
				img.pizza_background {
					width:120px;
					height:60px;
					padding:0;
					margin:0 auto;
					border:0px solid none;
					display:block;
				}
			</style>";

	// draw the apply button
	if (FALSE)
		$s_retval .= "
			<input type='button' value='Apply' onclick='apply_picture();' />
			<br />";

	// draw the grid of pictures
	$s_retval .= "<div id='picture_chooser'>";
	foreach ( $a_piclist as $s_picname ) {
		if (strpos ( $s_picname, 'top_' ) !== 0)
			continue;
		$selected = ($s_picname == $selected_picture) ? ' pic_selected' : '';
		$s_retval .= "
				<div class='pic_div {$selected}' onclick='selectAppliedPicture(this);'>
					<img class='pizza_background' src='/components/pizza/images/pizza_sizer_sauce_cheese.png' />
					<img class='picture_div' src='{$s_picpath}{$s_picname}' />
					<input type='hidden' name='name' value='{$s_picname}' />
				</div>";
	}
	$s_retval .= "
			<div id='pic_selected_name_display' style='display:none;'>{$selected_picture}</div>
			</div>";

	// draw the upload dialog
	if (FALSE)
		$s_retval .= "
			<br />
			<form name='upload' enctype='multipart/form-data' method='post' action='form_dialog.php?mode=set_inventory_picture&updateid=$updateid" . $fwd_picture_vars . "'>
				Upload: <input type='file' name='picture' onChange='document.upload.submit()' style='color:#bbbbbb'>
			</form>";

		// sanitize and return
		$s_retval = str_replace(array("\n", "\r", "\n\r"), "\n", $s_retval);
		return $s_retval;
	}
?>
