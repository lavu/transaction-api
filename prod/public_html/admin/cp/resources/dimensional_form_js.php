<script language="javascript">
		$(function(){
			menuItemchangeTracker = {};
			$('[alt="Item"]').on('focus', function() {
			    var itemId = $(this).attr('id');
			    var value = $(this).val();
				if (!menuItemchangeTracker[itemId]) {
					menuItemchangeTracker[itemId] = {
			            previous : value,
			            current : value,
			        };
				}
			});

			$('[alt="Item"]').on('change', function() {
			   var itemId = $(this).attr('id');
			   menuItemchangeTracker[itemId].current = $(this).val();
			});

			kitchenNicknameTracker = {};
			$('form[name=sform]').find('input:hidden').each(function() {
                var IdName = $(this).attr('id');
                if(IdName && IdName.indexOf('_kitchen_nickname_') > 0) {
	                var kitchenNickname = $(this).val();
	                if(kitchenNickname != '') {
		                kitchenNicknameTracker[IdName] = kitchenNickname;
	                }
                }
			});

			track86Status = {};
			const hiddenFields = $('form[name="sform"] input[type="hidden"]');
			track86Status = hiddenFields.toArray().reduce(function(acc, el) {
			const $el= $(el);
			const id = $el.attr('id') || '';
			if (id.indexOf('_track_86_count_') >= 0 || id.indexOf('_inv_count_') >= 0) {
				acc[id] = $el.val();
			}
			return acc;
			}, {});
		});

			function clickDeleteOrderType(message, cat, itm, archived) {
			    $('<div></div>').appendTo('body')
			    .html('<div><h4>'+message+'?</h4></div>')
			    .dialog({
			        modal: true, title: 'WARNING', zIndex: 10000, autoOpen: true,
			        width: '500', resizable: false,
			        buttons: {
			            Yes: function () {
					click_delete((cat),(itm),archived);
			                $(this).dialog('close');
			            },
			            No: function () {
			                $(this).dialog('close');
			            }
			        },
			        close: function (event, ui) {
			            $(this).remove();
			        }
			    });
			}
			
			function click_delete(cat,itm, archived)
			{

				if(itm==0) // deleting a category
				{
					document.getElementById("icrow_delinfo_" + cat + "_" + itm).innerHTML = document.getElementById("ic_" + cat + "_category").value;
					document.getElementById("ic_area_cat" + cat).style.display = "none";
					document.getElementById("ic_cat" + cat + "_addbtn").style.display = "none";
				}
				else
				{
					var menu_item_id= document.getElementById("ic_" + cat + "_id_" + itm).value ;

				    if(document.getElementById("ic_"+cat+"_combo_"+itm+"_cb") && menu_item_id!=''){
					  check_menuitem_combo(menu_item_id,cat,itm);
					  return false;
					}

//					if(archived == "true"){
//						setdval = "archived";//document.getElementById("ic_" + cat + "_<?php //echo $inventory_primefield?>//_" + itm).value;
//					}else {
						setdval = "deleted";//document.getElementById("ic_" + cat + "_<?php echo $inventory_primefield?>_" + itm).value;
//					}
                    document.getElementById("icrow_delinfo_" + cat + "_" + itm).innerHTML = setdval;

				}
				document.getElementById("icrow_" + cat + "_" + itm).style.display = "none";
				document.getElementById("icrow_del_" + cat + "_" + itm).style.display = "block";
				document.getElementById("ic_" + cat + "_delete_" + itm).value = 1;
			}
			
			function click_undelete(cat,itm)
			{
				document.getElementById("icrow_del_" + cat + "_" + itm).style.display = "none";
				document.getElementById("icrow_" + cat + "_" + itm).style.display = "block";
				document.getElementById("ic_" + cat + "_delete_" + itm).value = 0;
				if(itm==0) // undeleting a category
				{
					document.getElementById("ic_area_cat" + cat).style.display = "block";
					document.getElementById("ic_cat" + cat + "_addbtn").style.display = "block";
				}
			}
			
			function add_new_category()
			{
				cat_count++;
				item_count[cat_count] = 0;
				
				new_cat_code = "<?php jsvar($whole_cat_code_start . $whole_cat_code_end)?>";
				new_cat_code = new_cat_code.replace(/\(cat_row_code\)/ig,"<?php jsvar(cat_row_code($qtitle_category,$cfields,"","",false,$cinfo))?>");
				new_cat_code = new_cat_code.replace(/\(cat_count\)/ig,cat_count);			
				new_cat_code = new_cat_code.replace(/\(cat\)/ig,cat_count);
				append_html_to_id("ic_area", new_cat_code);
				//document.getElementById("ic_area").innerHTML += new_cat_code;
			}
			
			function add_new_item(cat, maxCount)
			{
				if (item_count[cat]<maxCount || maxCount==0) {

					item_count[cat]++;

					new_item_code = "";
					//if(item_count[cat] > 1) new_item_code += "<br>";
					new_item_code += "<?php jsvar(item_row_code($qdbfields, $details_column, $send_details, $inventory_primefield, $field)); ?>";
					new_item_code = new_item_code.replace(/\(cat\)/ig,cat);
					new_item_code = new_item_code.replace(/\(item\)/ig,item_count[cat]);
					append_html_to_id("ic_area_cat" + cat, new_item_code);

				} else alert("The maximum number allowed has been reached.");
			}

			function addOrderTypeItem(cat, maxCount)
			{
				if (item_count[cat]<maxCount || maxCount==0) {

					item_count[cat]++;
					var cat_id = document.getElementById("ic_" + cat + "_categoryid").value;
					new_item_code = "";
					var url = 'resources/check_forced_modifiers.php?category_id=' + cat_id;
					var xhr = window.XMLHttpRequest ? new XMLHttpRequest() : new ActiveXObject('Microsoft.XMLHTTP');
					xhr.open('GET', url, true);
					xhr.onreadystatechange = function () {
						if (xhr.readyState > 3 && xhr.status == 200) {
							if (xhr.responseText != '') {
								var oType = JSON.parse(xhr.responseText);
								var orTyLists = document.getElementById("ic_" + cat + "_title_" + item_count[cat]);
							    var i;
							    for (i = 0; i < orTyLists.length; i++) {
							        for(var j = 0; j < oType.length; j++) {
								        if (oType[j] == orTyLists[i].value) {
									        orTyLists[i].disabled = true;
								        }
							        }
							    }
							    filterOptions();
								return true;
							}
						}
					};
					xhr.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
					xhr.send();
					new_item_code += "<?php
					jsvar(itemOrderTypeRowCode($qdbfields, $details_column, $send_details, $inventory_primefield, $field));
					?>";
					new_item_code = new_item_code.replace(/\(cat\)/ig,cat);
					new_item_code = new_item_code.replace(/\(item\)/ig,item_count[cat]);
					append_html_to_id("ic_area_cat" + cat, new_item_code);

				} else alert("The maximum number allowed has been reached.");
			}
			
			function append_html_to_id(areaid, newhtml)
			{
				var t = document.createElement('div');
				t.innerHTML = newhtml;
				document.getElementById(areaid).appendChild(t);
				return;
			}
			
			movefrom_cat = "";
			movefrom_item = "";
			function moveitem_mousedown(mcat,mitem)
			{
				movefrom_cat = mcat;
				movefrom_item = mitem;
				return false;
			}
			
			function moveitem_mouseup(mcat,mitem)
			{
				moveto_cat = mcat;
				moveto_item = mitem;
				
				if(movefrom_cat==moveto_cat)
				{
					var n_container = document.getElementById("icrow_" + movefrom_cat + "_" + movefrom_item).parentNode;
					var n_from = document.getElementById("icrow_" + movefrom_cat + "_" + movefrom_item);
					var n_to = document.getElementById("icrow_" + moveto_cat + "_" + moveto_item);
					
					if(n_to.offsetTop > n_from.offsetTop)
					{
						if(n_container.lastChild==n_to)
							n_container.appendChild(n_to);
						else
							n_container.insertBefore(n_from,n_to.nextSibling);
					}
					else
						n_container.insertBefore(n_from,n_to);
					
					var str = "";
					var setorder = 0;
					for(ic = 0; ic < n_container.childNodes.length; ic++)
					{
						var icnm = n_container.childNodes[ic];
						if(icnm.id.indexOf("del") > 1)
						{
						}
						else
						{
							str += icnm.id + "\n";
							if(document.getElementById(icnm.id + "_order")!=undefined)
							{
								setorder++;
								document.getElementById(icnm.id + "_order").value = setorder;
							}
						}
					}
					//alert(setorder + " / " + n_container.childNodes.length + "\n" + str);
				}
				
				//alert("movefrom: " + movefrom_cat + " - " + movefrom_item + "\nmoveto: " + moveto_cat + " - " + moveto_item);
			}
			
			function append_html_to_id_v1(areaid, newhtml)
			{				
				var id_vals = new Array();
				var tag_stack = new Array();
				var cmd = "";
				var props = new Array();
				var prop_value = "";
				var str = "";
				var debug = "";
				var in_tag = false;
				var tag_spaces = 0;
				var tag_level = 0;
				var in_symbol = "";
				var escape_next = false;
				var oldhtml = document.getElementById(areaid).innerHTML;
				
				for(var n=0; n<oldhtml.length; n++)
				{
					chr = oldhtml.charAt(n);
					if(chr=="'" || chr=="\"")
					{
						if(escape_next)
						{
							str += chr;
							escape_next = false;
						}
						else if(in_symbol==chr)
						{
							str += chr;
							in_symbol = "";
						}
						else if(in_symbol=="")
						{
							str += chr;
							in_symbol = chr;
							prop_value = "";
						}
						else
						{
							prop_value += chr;
						}
					}
					else if(escape_next)
					{
						str += "\\" + chr;
					}
					else if(chr=="\\")
					{
						escape_next = true;
					}
					else
					{
						if(in_symbol=="")
						{
							if(in_tag)
							{
								if(chr==">")
								{
									if(tag_spaces==0)
									{
										cmd = str;
									}
									else if(tag_spaces > 0)
									{
										propparts = str.split("=");
										propname = propparts[0];
										if(propparts.length > 1)
										{
											if(propparts[1].indexOf("'") > -1 || propparts[1].indexOf('"') > -1)
											{
											}
											else
											{
												prop_value = propparts[1];
											}
											props[props.length] = new Array(propname,prop_value);
										}
									}
									if(cmd!="")
									{
										if(cmd.charAt(0)!="/")
										{
											// Opening Tag
											var elem = document.createElement(cmd);
											var elem_id = "";
											for(m=0; m<props.length; m++)
											{
												if(props[m][0]=="id")
												{
													elem_id = props[m][1];
												}
												//elem.setAttribute(props[m][0],props[m][1]);
											}
											if(elem_id!="")
											{
												id_vals.push(new Array(elem_id, ""));
											}
											tag_stack.pop(new Array(cmd, elem));
											if(tag_stack.length > 1)
											{
												var parent_elem = tag_stack[tag_stack.length - 2];
												//parent_elem.appendChild(elem);
											}
											else
											{
												var parent_elem = document.getElementById(areaid);
												//parent_elem.appendChild(elem);
											}
											tag_level++;
										}
										show_spaces = "";
										for(m=0; m<tag_level; m++)
											show_spaces += "    ";
										debug += show_spaces + cmd;
										for(m=0; m<props.length; m++)
										{
											debug += " " + props[m][0] + "=" + props[m][1];
										}
										debug += "\n";
										
										if(cmd=="input" || cmd.charAt(0)=="/")
										{
											// Closing Tag
											tag_stack.pop();
											tag_level--;
										}
									}
									str = "";
									in_tag = false;
								}
								else if(chr==" ")
								{
									if(tag_spaces==0)
									{
										cmd = str;
									}
									else if(tag_spaces > 0)
									{
										propparts = str.split("=");
										propname = propparts[0];
										if(propparts.length > 1)
										{
											if(propparts[1].indexOf("'") > -1 || propparts[1].indexOf('"') > -1)
											{
											}
											else
											{
												prop_value = propparts[1];
											}
											props[props.length] = new Array(propname,prop_value);
										}
									}
									tag_spaces++;
									str = "";
								}
								else
								{
									str += chr;
								}
							}
							else if(chr=="<")
							{
								if(str!="")
								{
									// Text node
									var elem = document.createTextNode(str);
									if(tag_stack.length > 1)
									{
										var parent_elem = tag_stack[tag_stack.length - 2];
										//parent_elem.appendChild(elem);
									}
									else
									{
										var parent_elem = document.getElementById(areaid);
										//parent_elem.appendChild(elem);
									}
									
									debug += str + "\n";
								}
								str = "";
								cmd = "";
								props = new Array();
								tag_spaces = 0;
								in_tag = true;
							}
							else
							{
								str += chr;
							}
						}
						else
						{
							prop_value += chr;
						}
					}
				}
				
				for(i=0; i<id_vals.length; i++)
				{
					id_set = id_vals[i][0];
					id_val = document.getElementById(id_set).value;
					id_vals[i][1] = id_val;
					//alert(id_set + " - ");
				}
				document.getElementById(areaid).innerHTML += newhtml;
				for(i=0; i<id_vals.length; i++)
				{
					id_set = id_vals[i][0];
					id_val = id_vals[i][1];
					document.getElementById(id_set).value = id_val;
				}
				
				return;
				//alert(newhtml);
				//alert(debug);
				//document.body.appendChild(tag_stack[0][1]);
				//document.getElementById(areaid).innerHTML += newhtml;
			}
			
			function open_dialog_box(dwidth, dheight, src, postvars, onloadCallback)
			{
				onloadCallback = onloadCallback || '';
				if(postvars)
				{
					str = "<iframe id='dframe' name='dframe' width='" + dwidth + "' height='" + dheight + "' allowtransparency='true' frameborder='0' src='' onload='resize_dialog_box_timed();'></iframe>";
					str += "<form name='dialog_post' method='post' action='" + src + "' target='dframe'>";
					postvars = postvars.split("&");
					for(v=0; v<postvars.length; v++)
					{
						pv = postvars[v].split("=");
						if(pv.length>1)
						{
							pv_name = pv[0];
							pv_value = pv[1];
							str += "<br><input type='hidden' name='" + pv_name + "' value='" + pv_value + "'>";
						}
					}
					str += "</form>";
					show_info(str);
					document.dialog_post.submit();
				} else {
					show_info("<div id='frame_wrapper' style='position: relative;'><div class='loading' style='position: absolute;width: 100%;'> <img src='resources/thumbnail-generator/lavu_spin.gif'/></div><iframe id='dframe' width='" + dwidth + "' height='" + dheight + "' allowtransparency='true' frameborder='0' src='" + src + "' onload='resize_dialog_box_timed();"+onloadCallback+"'></iframe></div>");
				}
			}
			
           function open_picture_editor(setdir, dwidth, dheight, pictureid, filename, picture2, picture3, misc_content, get_extension)
			{
	           if (!get_extension) get_extension = '';
				durl = "resources/form_dialog.php?mode=set_inventory_picture&dir=" + setdir + "&updateid=" + pictureid + "&main_picture=" + filename + get_extension;
				if(picture2) durl += "&picture_2=" + picture2;
				if(picture3) durl += "&picture_3=" + picture3;
				if(misc_content) durl += "&misc_content=" + misc_content;
				open_dialog_box(dwidth, dheight, durl);
			}
			
			function open_picture_editor_grid(setdir, dwidth, dheight, pictureid, filename, picture2, picture3, misc_content) {
				return open_picture_editor(setdir, dwidth, dheight, pictureid, filename, picture2, picture3, misc_content, '&view_grid=1');
			}

			function open_special_editor(dwidth, dheight, item_name, item_value, item_title, row_name, extra1, extra2, item_id, type) {
				row_name = row_name.replace(/'/g, "%27");
				document.getElementById("info_area").innerHTML = "<center class='center-style' ><div>Loading Please Wait...</div><img src='images/kpr_activity.gif' class= 'loader-style'></center>";
				info_visibility("on");
				var src = "resources/form_dialog.php?mode=set_special&item_value=" + item_value + "&item_name=" + item_name + "&item_title=" + item_title + "&rowname=" + encodeURIComponent(encodeURIComponent(row_name)) + "&extra1=" + extra1 + "&extra2=" + extra2 + "&item_id=" + item_id + "&type=" + type;

				if(item_title && item_title.toLowerCase().trim() === "ingredients") {
					if(!ingredientItems) {
						loadItemsOptions(function(){
							open_dialog_box(dwidth, dheight, src);
						});
					} else {
						open_dialog_box(dwidth, dheight, src);
					}
				} else {
					open_dialog_box(dwidth, dheight, src); // double URI encoding row_name for # in menu item names because show_info() unescapes()
				}
				
			}

			function resize_dialog_box_timed() {
				resize_dialog_box();
				setTimeout(function() { resize_dialog_box(); }, 500);
				setTimeout(function() { resize_dialog_box(); }, 1000);
				setTimeout(function() { resize_dialog_box(); }, 3000);
				setTimeout(function() { resize_dialog_box(); }, 5000);
			}
			
			function resize_dialog_box() {
				$("#frame_wrapper .loading").hide();
				var jframe = $('#dframe');
				jframe.show();
				var jtable = $('#info_area_table');
				var jtable2 = $('#info_area_bgtable');
				if (!jframe || !jtable || !jtable2)
					return;
				var jhtml = jframe.contents().find('html');
				if (!jhtml)
					return;
				
				// get the current size as the original size
				if (!window.dialog_box_size) {
					var os = {};
					os.frame = { width: jframe.css('width'), height: jframe.css('height') };
					os.table = { width: jtable.css('width'), height: jtable.css('height') };
					window.dialog_box_size = {};
					window.dialog_box_size.original_size = os;
				}
				
				// revert back to the original size
				var os = window.dialog_box_size.original_size;
				jframe.stop(true,true);
				jtable.stop(true,true);
				jtable2.stop(true,true);
				jframe.css(os.frame);
				jtable.css(os.table);
				jtable2.css(os.table);
				
				// calculate the new size
				var diff = parseInt(os.table.height) - parseInt(os.frame.height);
				var html_height = jhtml.height();
				var window_height = $(window).height() - 100;
				var max_size = {
					frame: { height: (window_height - diff) },
					table: { height: window_height }
				};
				var new_size = {
					frame: { height: (Math.max(parseInt(os.frame.height), Math.min(max_size.frame.height, html_height))+'px') },
					table: { height: (Math.max(parseInt(os.table.height), Math.min(max_size.table.height, (html_height+diff)))+'px') }
				};
				window.dialog_box_size.new_size = new_size;
				
				// apply the new size
				jframe.stop(true,true);
				jtable.stop(true,true);
				jframe.css(new_size.frame);
				jtable.css(new_size.table);
				jtable2.css(new_size.table);
			}
			
			//---------------------------------------------------------------------------------
			// set_inv_picture will be currently called when the apply button is clicked, it will
			// populate fields with the old picture, new picture and now AWS key on hidden fields
			//---------------------------------------------------------------------------------

			function set_inv_picture(pictureid, filename, displayfile, picture2, picture3, misc_content, awsPIC)
			{
				var imgcode = "<img src='" + displayfile + "' width='16' height='16' />";
				document.getElementById(pictureid).value = filename;
				document.getElementById(pictureid + "_display").innerHTML = imgcode;
				if(picture2)
				{
					pictureid_2 = pictureid.replace(/image/ig,"image2");
					document.getElementById(pictureid_2).value = picture2;
				}
				if(picture3)
				{
					pictureid_3 = pictureid.replace(/image/ig,"image3");
					document.getElementById(pictureid_3).value = picture3;
				}
				if(misc_content)
				{
					pictureid_misc = pictureid.replace(/image/ig,"misc_content");
					document.getElementById(pictureid_misc).value = misc_content;
				}
				if(awsPIC)
				{
					picture_aws = pictureid.replace(/image/ig,"storage_key");
					document.getElementById(picture_aws).value = awsPIC;
				}
				info_visibility();
			}
			
			function set_special_extra_value(set_itemid, set_itemvalue)
			{
				document.getElementById(set_itemid).value = set_itemvalue;
			}
			
			function set_special_value(set_itemid, show_itemvalue, set_itemvalue)
			{
				document.getElementById(set_itemid).value = set_itemvalue;
				
				show_itemid = set_itemid + "_display";

				if(show_itemvalue=="") set_itemcolor = "#aaaaaa";
				else set_itemcolor = "#999999";
				
				document.getElementById(show_itemid).value = show_itemvalue;
				document.getElementById(show_itemid).style.color = set_itemcolor;
				info_visibility();
			}

			// gets the item and category ids for the given indices
			// @category_index: index of the category (in the order it appears, starting at 1)
			// @item_index: index of the item (in the order it appears, starting at 1 for each category)
			// @return: an object {'item': item id or null, 'category': cat id or null}
			function get_item_category_id(category_index, item_index) {
				return {'item': get_item_id(category_index, item_index), 'category': get_category_id(category_index)};
			}
			
			// gets the item id for the given indices
			// @category_index: index of the category (in the order it appears, starting at 1)
			// @item_index: index of the item (in the order it appears, starting at 1 for each category)
			// @return: item id or null
			function get_item_id(category_index, item_index) {
				var iteminput = $('#ic_'+category_index+'_id_'+item_index);
				var item_id = (iteminput.length > 0) ? parseInt(iteminput.val()) : null;
				return item_id;
			}
			
			// gets the category id for the given index
			// @category_index: index of the category (in the order it appears, starting at 1)
			// @return: category id or null
			function get_category_id(category_index) {
				var catinput = $('#ic_'+category_index+'_categoryid');
				var category_id = (catinput.length > 0) ? parseInt(catinput.val()) : null;
				return category_id;
			}
			
			// gets all ids for all categories
			// @return: an object {1: cat id for index 1, 2: cat id for index 2, ..., cat_count: cat id for the last category}
			function get_category_ids() {
				cat_ids = {};
				var j = 1;
				for(var i = 1; i < cat_count+1; i++) {
					var cat_id = get_category_id(i);
					if (cat_id !== null) {
						cat_ids[j] = cat_id;
						j++;
					}
				}
				
				return cat_ids;
			}
			
			// gets all names for all categories
			// @return: an object {1: cat name for index 1, 2: cat name for index 2, ..., cat_count: cat name for the last category}
			function get_category_names(cat_ids) {
				if (!cat_ids)
					cat_ids = get_category_ids();
				var names = {};
				for (var i = 1; i < cat_count+1; i++) {
					if (!cat_ids[i])
						break;
					names[i] = $('#ic_'+i+'_category').val();
				}
				return names;
			}
			
			// gets the name of item
			// @category_index: index of the category (in the order it appears, starting at 1)
			// @item_index: index of the item (in the order it appears, starting at 1 for each category)
			// @return: the name of the item or null if that item can't be found
			function get_item_name(category_index, item_index) {
				var item = $('#ic_'+category_index+'_name_'+item_index);
				if (item.length > 0)
					return item.val();
				return null;
			}
			
			// a data_view object is like a select box with multiple lines
			// @input_name: the name of the input element to be created with this select box (also used to identify this data_view)
			// @view_height: the number of lines to be displayed
			// @view_elements: the names of the elements
			// @view_values: the values of the elements (must have the same indices as view_elements)
			// @default_value: the value originally selected (matches the value in view_values)
			// @onchange_callback: name of a function(change_value as a string,data_view_id as a string) to call when the value changes, optional
			// @return: the html for a data_view
			function draw_data_view(input_name, view_height, view_elements, view_values, default_value, onchange_callback) {
				var str = '';
				var font_size = 12;
				var padding = 4;
				var data_view_id = 'data_view_object_'+input_name;
				var selected_values = {};
				window.draw_data_view_first_value = null;
				
				if (!onchange_callback)
					onchange_callback = '';
				
				// find the selected value
				var default_found = false;
				$.each(view_values, function(k,v) {
					if (default_found === false && v == default_value) {
						default_found = v;
						selected_values[k] = data_view_id+'_data_view_selected';
					} else {
						selected_values[k] = '';
					}
				});
				if (!default_found) {
					$.each(view_values, function(k,v) {
						if (default_found === false) {
							default_found = v;
							selected_values[k] = data_view_id+'_data_view_selected';
						}
					});
				}
				
				// draw the select area
				str += '<div class="'+data_view_id+'_data_view_object" id="'+data_view_id+'">';
				$.each(view_elements, function(k,v) {
					str += '<div class="'+data_view_id+'_data_view_element '+selected_values[k]+'" id="'+data_view_id+'_'+k+'" onclick="data_view_select(\''+data_view_id+'\', '+k+', '+view_values[k]+', \''+onchange_callback+'\');">'+v+'</div>';
				});
				str += '</div>';
				
				// draw the input element
				str += '<input type="hidden" name="'+input_name+'" id="'+data_view_id+'_data_view_value" value="'+default_found+'\" />';
				
				// draw the css
				str += '<?php echo str_replace(array("\r", "\r\n", "\n"), "", "
				<style type=\"text/css\">
					.'+data_view_id+'_data_view_object { color: black; background-color: white; width: 220px; height: '+(view_height*(font_size+padding))+'px; font-size: '+font_size+'px; overflow-y: auto; }
					.'+data_view_id+'_data_view_element { color: black; background-color: white; width: 200px; height: '+(font_size+padding)+'px; cursor: pointer; }
					.'+data_view_id+'_data_view_selected { color: white; background-color: #8b8b8b; }
				</style>"); ?>';
				
				return str;
			}
			
			// helper function for draw_data_view()
			function data_view_select(data_view_id, k, value, onchange_callback) {
				$('#'+data_view_id).children('.'+data_view_id+'_data_view_selected').removeClass(data_view_id+'_data_view_selected');
				$('#'+data_view_id+'_'+k).addClass(data_view_id+'_data_view_selected');
				var jvalue = $('#'+data_view_id+'_data_view_value');
				var oldval = jvalue.val();
				jvalue.val(value);
				if (onchange_callback && oldval !== value)
					eval(onchange_callback+'("'+value+'","'+data_view_id+'")');
			}
			// Happy Hours Onchange in Option and  modifilers 
			function happyhours(value,col){
				var num=$("#ic_area_cat"+col+" > div").length;
				num=(num/2)+1;
				var i;
				for(i=1;i<num;i++){
				$("#ic_"+col+"_happyhours_id_"+i).val(value);
				}
			}
			function combooncheck(id,checked){
		         var combo = id;
		         var res = combo.split("_");
		         var cb_type = res[2];
		         var cbid = res[3];
		         if(cb_type=='combo')
		         {
		          var ingredients = res[0]+"_"+res[1]+"_ingredients_"+cbid+"_display";
		          var combo = res[0]+"_"+res[1]+"_comboitems_"+cbid+"_display";
		          var modifier=res[0]+"_"+res[1]+"_modifier_list_id_"+cbid;
		          var forcedmodifier=res[0]+"_"+res[1]+"_forced_modifier_group_id_"+cbid;
		          var modifier_value=  document.getElementById(modifier).value;
		          var forcedmodifier_value=  document.getElementById(forcedmodifier).value;
		          if(checked==true)
		          {
		           document.getElementById(ingredients).style.display='none';
		           document.getElementById(combo).style.display='block';
		           document.getElementById(modifier).disabled = true;
		           document.getElementById(forcedmodifier).disabled = true;
		           var input = document.createElement("input");
		           var force_input = document.createElement("input");
		           input.setAttribute("type", "hidden");
		           input.setAttribute("name", modifier);
		           input.setAttribute("value", modifier_value);
		           force_input.setAttribute("type", "hidden");
		           force_input.setAttribute("name", forcedmodifier);
		           force_input.setAttribute("value", forcedmodifier_value);
		           //append to form element that you want .
		           document.getElementById(id).appendChild(input);
		           document.getElementById(id).appendChild(force_input);
		          }
		          else{
		           document.getElementById(ingredients).style.display='block';
		           document.getElementById(combo).style.display='none';
		           document.getElementById(modifier).disabled = false;
		           document.getElementById(forcedmodifier).disabled = false;
		          }
		         }
					}

            //This function added to check menu item availability in combo or not.LP-3986
			function check_menuitem_combo(menu_item_id,cat,itm)
             	{
				   $.ajax({
				   type: 'POST',  // http method
				   url: '/cp/resources/combo_item_details.php',
				   data: {menu_item_id:menu_item_id},
					  error: function() {
				       //alert('false');
				   },
				   success: function(result) {

						 if(result=='')
						 {
							setdval = "deleted";
							document.getElementById("icrow_delinfo_" + cat + "_" + itm).innerHTML = setdval;
							document.getElementById("icrow_" + cat + "_" + itm).style.display = "none";
							document.getElementById("icrow_del_" + cat + "_" + itm).style.display = "block";
							document.getElementById("ic_" + cat + "_delete_" + itm).value = 1;
						}
						else
						{
							var split_str = result.split('@#$');
							var msg = 'This item cannot be deleted because it is associated with ('+split_str[1]+') combo, to delete, please remove it from ('+split_str[1]+') combo first and then delete this item.';
							alert(msg);
						}

					 }


				 });

	        }


	        var ingredientItems = '';
	function loadItemsOptions(callBack) {
		$.ajax({
			url: '/cp/areas/ajax_fetch_inventory_units.php',
			method: 'GET',
			dataType: 'JSON',
			success: function(apiResponse) {
				ingredientItems = apiResponse;
				if(callBack){
					callBack();
				}
			},
			error:function(error){
				callBack(error);
			}
		});
	}
	//end of LP-3986.

	function checkPriceInformation(elem, title) {
				checkTrack86Status();
				var formName = $(elem).attr('name');
				var formReqInfo = '';
				var itemName = '';
				var itemPrice = '';
				var monitarySymbol = '';
				var decimals = '';
				var decimalChar = '';
				var leftRight = '';
				var rowStatus = 1;
				var catStatus = 1;

				$('form[name='+formName+']').find('input:text').each(function() {
					var Name = $(this).attr('name');

					if(Name.indexOf('_category') > 0) {
						var catfieldId = Name.split('_');

						var catId = '#icrow_'+catfieldId[1]+'_0';
						catInfo =  $(this).parents(catId).attr('style');
						if(catInfo.indexOf('display: none;') >= 0) {
							catStatus = 0;
						}
					}

					if(catStatus == 1) {
						if(Name.indexOf('_'+title+'_') > 0) {
							itemName = $(this).attr("value");
						}

						if(Name.indexOf('_cost_') > 0) {
							var fieldId = Name.split('_');

							var rowId = '#icrow_'+fieldId[1]+'_'+fieldId[3];
							rowInfo =  $(this).parents(rowId).attr('style');
							if(rowInfo.indexOf('display: none;') >= 0) {
								rowStatus = 0;
							}

							if(rowStatus == 1) {
								if(monitarySymbol == '') {
									monitarySymbol = $(this).attr('monetarysymbol');
								}
								decimals = $(this).attr('allowdecimals');
								decimalChar = $(this).attr('decimalchar');
								leftRight = $(this).attr('leftright');
								itemPrice = $(this).attr("value");

								if (itemPrice != 'Price') {
									formReqInfo += itemName+'^^!'+itemPrice+'&&';
								}
							}
						}
					}
				});

					formReqInfo = formReqInfo.slice(0, -2);
					var url = '/cp/areas/edit_menu.php';
					var result = true;
					$.ajax({
			            type: 'POST',
			            url: url,
			            async: false,
						data: {'price_validation':1,'formReqInfo':formReqInfo,'monitarySymbol':monitarySymbol,'decimals':decimals,'decimalChar':decimalChar,'leftRight':leftRight},
			            success: function(data) {
							 var getResult = data.split('*^#');
							if(getResult[0] == 'fail') {
								$('.menu_price_validation').html(getResult[1]);
								result = false;
							}
			            }
			        });

				return result;
			}
			
	//LP-7870: This function is used to confirm kitchen nicknames with menu item names.
	function confirmKitchenNickname(elem) {
		var result = true;
		 var kitchenStatus = 0;
		 var menuKitchenInfo = '';
		 $.each(menuItemchangeTracker, function( id, itemName ) {
			if (itemName.previous != itemName.current) {
			  kitchenId = id.replace('name', 'kitchen_nickname');
			  kitchenNickname = $('#'+kitchenId).val();
			  if(kitchenNickname != '') {
				 kitchenStatus = 1;
				 menuKitchenInfo += '<tr><td><span class="kitchenItem">'+itemName.current+'</span></td><td class="kitchenItem"><input type="text" id="pop_'+kitchenId+'" name="'+kitchenId+'" style="padding-left:2px;" value="'+kitchenNickname+'"></td></tr>';
				 menuItemchangeTracker[id] = {
				        previous : itemName.previous,
				        current : itemName.current,
			            kitchenId : kitchenId,
			            kitchenName : kitchenNickname,
			        };
			  }
			}
			});

			if (kitchenStatus) {
				result = false;
			var formName = $(elem).attr('name');
			var headerMsg = '<div class="headerMsg">We noticed you updated these menu item names, do you need to update their kitchen nicknames?</div><div id="nicknameValidation" style="display:none" title="Warning">Kitchen nickname should be 30 characters long with at least one alphanumeric character or symbol</div>';
            var menuItemInfo = '<div><table><tr> <th class="kitchenMenuHeader">Menu Item Name</th> <th class="kitchenMenuHeader">Current Kitchen Nickname</th></tr>';
				menuItemInfo += menuKitchenInfo;
				menuItemInfo += '</table></div>';

			$('form[name='+formName+'] #confirmKitchenNickname').html(headerMsg+menuItemInfo);

			$('#confirmKitchenNickname').dialog({
				resizable: false,
				height: 'auto',
				width: '50em',
				modal: true,
				buttons : [{
				text : 'Cancel',
				click : function() {
					getMenuItemPreviousValues(formName,menuItemchangeTracker);
					$(this).remove();
					document.sform.submit();
				}
				},
				{
				text : 'Save Changes',
				style : 'background:#aecd37;color:#fff',
				click : function() {
					validateKitchenNickname(menuItemchangeTracker, kitchenNicknameTracker);
				}
				}],
                close: function (event, ui) {
					getMenuItemPreviousValues(formName,menuItemchangeTracker);
					document.sform.submit();
                }
				});
			}
			else {
				getKitchenNicknames();
				$.each(kitchenNicknameTracker, function(kId, kName) {
					if (typeof kName !== 'undefined') {
						result = false;
						checkKitchennickname(kitchenNicknameTracker);
					}
				});
			}
		return result;
	}

	function getKitchenNicknames() {
		kitchenNicknameTracker = {};
		$('form[name=sform]').find('input:hidden').each(function() {
            var IdName = $(this).attr('id');
            if(IdName && IdName.indexOf('_kitchen_nickname_') > 0) {
                var kitchenNickname = $(this).val();
                if(kitchenNickname != '') {
	                kitchenNicknameTracker[IdName] = kitchenNickname;
                }
            }
          });
	}
	//This function is used to assign previous values to selected menu item name and kitchen nickname.
	function getMenuItemPreviousValues(formName, menuItemchangeTracker) {
		$.each(menuItemchangeTracker, function(id, itemInfo) {
			if (typeof itemInfo.kitchenName !== 'undefined') {
				$('form[name='+formName+'] #'+id).val(itemInfo.previous);
				$('form[name='+formName+'] #'+itemInfo.kitchenId).val(itemInfo.kitchenName);
			}
		});
     }

	//This function is used to set new kitchen nickname.
	function setKitchenNicknames(newKitchenData) {
		$.each(newKitchenData, function(kitchenId, kitchenName) {
			$('form[name=sform] #'+kitchenId).val(kitchenName);
		});
     }

	//This function is used to validate kitchen nickname and to check selected name already available or not.
	function validateKitchenNickname(menuItemchangeTracker, kitchenInfo) {
		var duplicate = '';
		var newKitchenData = {};
		var flag = 1;
		$.each(menuItemchangeTracker, function(id, itemInfo) {
			var kitchenId = itemInfo.kitchenId;
			var oldNickname = itemInfo.kitchenName;
			var newNickname = $('#pop_'+itemInfo.kitchenId).val();
				newKitchenData[kitchenId] = newNickname;
			if(oldNickname != newNickname) {
				if(!isNaN(newNickname) || newNickname.length > 30) {
					flag = 0;
					$('#nicknameValidation').dialog({
					resizable: false,
					height: 'auto',
					width: '30em',
					modal: true,
					buttons : {
					'Ok': function() {
						flag = 1;
						$( this ).dialog( 'close' );
					}
					}
					});
					return false;
				}
				else {
					$.each(kitchenInfo, function(kId, kName) {
						if(kitchenId != kId && newNickname == kName) {
							duplicate = kName +', '+ duplicate;
						}
					});
				}
			}
		});

		if(duplicate != '') {
			duplicate = duplicate.substr(0, duplicate.length-2);
			validation = showKitchenNicknameDuplicats(duplicate, newKitchenData);
		} else if(flag) {
			setKitchenNicknames(newKitchenData);
			document.sform.submit();
		}
	}

	//This function is used to show Kitchen nickname duplicate info.
    function showKitchenNicknameDuplicats(duplicate, newKitchenData) {
		var message = 'The entered Kitchen Nickname('+duplicate+') is the same as another items Kitchen Nickname.<br>Are you sure you want to proceed?';
		$('#duplicateNicknameValidation').html(message);
		$('#duplicateNicknameValidation').dialog({
			resizable: false,
			height: 'auto',
			width: '30em',
			modal: true,
			buttons : {
				 Yes: function() {
					 $(this).dialog("close");
					 setKitchenNicknames(newKitchenData);
					 document.sform.submit();
					 },
				 No: function() {
					 $(this).dialog("close");
					 }
			}
			});
    }

    function checkKitchennickname(kitchenNicknameTracker) {
		var keys = Object.keys(kitchenNicknameTracker);
		var dupe = false;
		duplicate = {};
		itemName = '';
		kitchenName = '';
		itemNamearr =[];
		var flag = 0;
		for(var i=0;i<keys.length;i++){
			itemId = keys[i].replace('kitchen_nickname', 'name');
			itemName = $('form[name=sform] #'+itemId).val();
			itemNamearr.push(itemName);
			for(var j=i+1;j<keys.length;j++){
	    	   if(kitchenNicknameTracker[keys[i]] === kitchenNicknameTracker[keys[j]]){
	    	     dupe = true;
	    	     itemId = keys[j].replace('kitchen_nickname', 'name');
				 itemName = $('form[name=sform] #'+itemId).val();
				 itemNamearr.push(itemName); 
				 flag =1;
	    	     kitchenName =  kitchenNicknameTracker[keys[j]];
	    	   }
	    	 }
	    	 var result = Array.from(new Set(itemNamearr));
	    	  if(flag == 1) 
	    	   	break;
    	}

    	 if(dupe){
			 var message = 'The entered Kitchen Nickname "'+kitchenName+'" is same for menu items '+result+'. <br>Are you sure you want to proceed?';
				$('#duplicateNicknameValidation').html(message);
				$('#duplicateNicknameValidation').dialog({
					resizable: false,
					height: 'auto',
					width: '30em',
					modal: true,
					buttons : {
						 Yes: function() {
							 $(this).dialog("close");
							 document.sform.submit();
							 },
						 No: function() {
							 $(this).dialog("close");
							 }
					}
					});
		} else {
			 document.sform.submit();
		}
    }
    function setForcedModifierTypeOption(typeid, value) {
    	var option=document.getElementById(typeid+"_option").value;
    	if (value == 'choice' && option != 1) {
    		document.getElementById(typeid+"_option").value = 1;
    	}
    	if (value == 'checklist' && option == 1) {
    		document.getElementById(typeid+"_option").value = 0;
    	}
    }
    function setForcedModifierType(optionid, value) {
	    var	type_id = optionid.slice(0, -7);
    	var type_value = document.getElementById(type_id).value;
    	if (value == 1 && type_value != 'choice'){
    		document.getElementById(type_id).value = 'choice';
    	}
    	if (value != 1 && type_value == 'choice'){
    		document.getElementById(type_id).value = 'checklist';
    	}
    }

	/*
	* This function is used update unavaiable_untill flag, when track_86_count, inv_count modifications done from CP.
	*/
	function checkTrack86Status() {
		trackStatus = new Array();
		$.each(track86Status, function(trackId, trackName) {
			trackIdSplit = trackId.split('_');
			idName = 'ic_'+trackIdSplit['1']+'_id_'+trackIdSplit[trackIdSplit.length-1];
			itemId = $('#'+idName).val();
			oldValue = trackName;
			modifiedValue = $('#'+trackId).val();
			if(oldValue != modifiedValue && trackStatus.indexOf(itemId) == -1) {
				trackStatus.push(itemId);
			}
		});

		if (trackStatus.length > 0) {
			trackIdList = JSON.stringify(trackStatus);
			dataName = $('input[name=posted_dataname]').val();
			var url = '/cp/areas/edit_menu.php';
			$.ajax({
		        type: 'POST',
		        url: url,
		        async: false,
				data: {'track86ModifiedStatus':1,'trackIdList':trackIdList, 'dataName':dataName},
		        success: function(data) {
					//console.log(data);
		        }
		    });
		}
	}

	</script>
