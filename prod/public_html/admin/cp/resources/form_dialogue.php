<?php

	session_start();
	ini_set("display_errors","0");
	require_once(__DIR__."/core_functions.php");
	require_once(__DIR__."/inventory_functions.php");
	require_once(__DIR__."/action_tracker.php");
	require_once("/home/poslavu/private_html/rs_upload.php");
	require_once("/home/poslavu/public_html/admin/lib/modules.php");
	echo "<head>";
	echo "<link rel='stylesheet' type='text/css' href='../styles/styles.css' />";
	echo "</head>";
	echo "<script type='text/javascript' src='//ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js'> </script>";


	$location_info = sessvar("location_info");
	//$active_language_pack = sessvar("active_language_pack");
	$active_language_pack = getLanguageDictionary(sessvar("locationid"));
	$modules = getActiveModules(sessvar('modules'),sessvar('package'),sessvar('isLLS'));
	$toprint = "";
	if(empty($location_info)){return;}

	function get_profile_list($loc_id, $type, $subtype) {


		$profile_list = array();
		$ntpval = "";
		if ($subtype == "item") {
			$profile_list[] = array(speak("Category Default"),"");
			$ntpval = "0";
		}
		if ($type == "tax") {
			$profile_list[] = array(speak("No tax profile"), $ntpval);
			$get_profiles = mlavu_query("SELECT `id`, `title` FROM `[1]`.`tax_profiles` WHERE `loc_id` = '[2]' AND `_deleted` = '0' ORDER BY `title` ASC", admin_info("database"), $loc_id);
		} else if ($type == "price_tier") {
			$profile_list[] = array(speak("No price tier profile"), $ntpval);
			$get_profiles = mlavu_query("SELECT `id`, `value` AS `title` FROM `[1]`.`config` WHERE `location` = '[2]' AND `type` = 'price_tier_profile' AND `_deleted` = '0' ORDER BY `value` ASC", admin_info("database"), $loc_id);
		}
		while ($get_profiles !== FALSE && $info = mysqli_fetch_assoc($get_profiles)) {
			$profile_list[] = array($info['title'], $info['id']);
		}

		return $profile_list;
	}
	
	function get_menucode_list() {
	    
	    $file_path = "../../norway_reports/codes/Norwegian SAF-T Cash Register data - Code list - PredefinedBasicID-04.csv";
	    $file = fopen($file_path,"r");
	    $list = array();
	    while(! feof($file))
	    {
	        $list = fgetcsv($file,2048);
	        $newArray = explode(';',$list[0]);
	        $itemsList[] = array($newArray[1],$newArray[0]);
	    }
	    fclose($file);
	    
	    array_pop($itemsList);
	    array_shift($itemsList);
	    array_unshift($itemsList, array("--Select--",""));
	    return $itemsList;
	}

	function forcedModifierType($fmod_group_id) {
		
		$fmod_type = "";
		
		if (substr($fmod_group_id, 0, 1) == "f") {
			$list_id = trim($fmod_group_id, " f");
			$check_forced_mods = lavu_query("SELECT `type` FROM `forced_modifier_lists`	WHERE `id` = '[1]'", $list_id);
			if ($check_forced_mods !== FALSE) {
				if (mysqli_num_rows($check_forced_mods) > 0) {
					$mod_info = mysqli_fetch_assoc($check_forced_mods);
					$fmod_type = $mod_info['type'];
				}
			}
		}
		
		return $fmod_type;
	}
	
	function includeExtensionOptions($gift_or_loyalty, &$special_options) {
		
		$ext_designator = str_replace("_card", "_extension:", $gift_or_loyalty);
		if (in_array($ext_designator, array("gift_extension:", "loyalty_extension:"))) {
			
			$check_payment_types = lavu_query("SELECT `type`, `special` FROM `payment_types` WHERE `special` LIKE '".$ext_designator."%' AND `_deleted` = '0'");
			if ($check_payment_types !== FALSE) {
				if (mysqli_num_rows($check_payment_types) > 0) {
					$info = mysqli_fetch_assoc($check_payment_types);
					$special_options[] = array($info['type'], $info['special']);
				}
			}
		}
	}
		
	function create_tab_area($tabs,$tab_content,$tab_script,$selected_tab,$extras=false)
	{
		global $toprint;
		$tab_buttons = "";
		$tab_script_names = "";
		$tab_script_selected = "";
		$tab_area = "";
		if(!$extras) $extras = array();
		for($i=0; $i<count($tabs); $i++)
		{
			$tab_title = $tabs[$i];
			$tab_name = str_replace(" ","_",strtolower($tab_title));
			$tab_area_name = "tab_" . $tab_name . "_area";
			$tab_btn_name = "tab_" . $tab_name . "_btn";

			if($tab_title==$selected_tab)
			{
				$setbgcolor = "#f2f2f2";
				$area_display = "block";
			}
			else
			{
				$setbgcolor = "#d2d2d2";
				$area_display = "none";
			}
			if(count($tabs) > 1)
			{
				$tab_buttons .= "<td style='border:solid 2px #aaaaaa; cursor:pointer' bgcolor='$setbgcolor' onclick='pedit_display_area(\"$tab_area_name\")' id='$tab_btn_name'>";
				$tab_buttons .= $tab_title;
				$tab_buttons .= "</td>";
			}

			if($tab_script_names!="") $tab_script_names .= ",";
			$tab_script_names .= "new Array('$tab_area_name','$tab_btn_name',\"$tab_title\")";
			if(isset($tab_content[$tab_name]))
				$set_tab_content = $tab_content[$tab_name];
			else
				$set_tab_content = "&nbsp;";
			if(isset($tab_script[$tab_name]))
			{
				$tab_script_selected .= "if(areaid=='$tab_area_name') { " . $tab_script[$tab_name] . " } ";
			}
			$tab_area .= "<div id='$tab_area_name' style='display:$area_display'>".$set_tab_content."</div>";
		}
		if(isset($extras['tab_right_content']))
		{
			$tab_buttons .= "<td width='30'>&nbsp;</td><td>".$extras['tab_right_content']."</td>";
		}

		$str = "";
		$str .= "<input type='hidden' name='tab_selected' id='tab_selected' value='$selected_tab'>";
		$str .= "<script type='text/javascript'>";
		$str .= "parea_list = new Array($tab_script_names); ";
		$str .= "function pedit_display_area(areaid) { ";
		$str .= "  for(i=0; i<parea_list.length; i++) { ";
		$str .= "    parea_name = parea_list[i][0]; ";
		$str .= "    pbtn_name = parea_list[i][1]; ";
		$str .= "    parea_title = parea_list[i][2]; ";
		$str .= "    if(parea_name==areaid) { ";
		$str .= "       set_bgcolor = '#f2f2f2'; set_display = 'block'; ";
		$str .= "       document.getElementById('tab_selected').value = parea_title; ";
		$str .= "       $tab_script_selected ";
		$str .= "    } else { ";
		$str .= "       set_bgcolor = '#d2d2d2'; set_display = 'none'; ";
		$str .= "    } ";
		$str .= "    document.getElementById(pbtn_name).bgColor = set_bgcolor; ";
		$str .= "    document.getElementById(parea_name).style.display = set_display; ";
		$str .= "  } ";
		$str .= "} ";
		$str .= "</script>";
		$str .= "<table cellpadding=4>";
		$str .= $tab_buttons;
		$str .= "</table>";
		$str .= $tab_area;
		return $str;
	}
	function uvar($str, $def=false)
	{
		if(isset($_GET[$str])) return $_GET[$str]; else return $def;
	}
	function strvar($str)
	{
		return strtolower(str_replace(" ","_",$str));
	}
	
	$nor_way = mlavu_query("SELECT `value` FROM  `[1]`.`config` WHERE `setting` = 'norway_mapping' ", admin_info("database"));
	$norway_values = mysqli_fetch_assoc($nor_way);
	$norway_status=$norway_values[value];
	
	$toprint .= "<body style='background-color: transparent;color:white;' >";
	$toprint .= "<style>";
	$toprint .= "body, table {font-family:Verdana,Arial; font-size:12px;} ";
	$toprint .= "</style>";

	$mode = (isset($_GET['mode']))?$_GET['mode']:"";
	$setdir = (isset($_GET['dir']))?$_GET['dir']:false;
	$inv_main_size = array(480,320);
	$inv_thumb_size = array(120,80);


	if($mode=="timecard")
	{
		$in_lavu = true;
		$rdb = admin_info("database");
		lavu_connect_byid(admin_info("companyid"),$rdb);

		$locationid = sessvar("locationid");
		if($locationid)
		{
			require_once(dirname(__FILE__) . "/../areas/reports/time_cards.php");
		}
	}
	else if($mode=="subarea")
	{
		$area = (isset($_GET['area']))?$_GET['area']:false;
		$toprint .= "subarea for $area";
		if($area)
		{
			$in_lavu = true;
			$rdb = admin_info("database");
			lavu_connect_byid(admin_info("companyid"),$rdb);

			$locationid = sessvar("locationid");

			require_once(dirname(__FILE__) . "/../areas/".$area.".php");
		}
	}
	else if($mode=="set_inventory_picture")
	{
		if(!$setdir) exit();
		$set_ers_path = $setdir;
		$updateid = uvar("updateid");
		$category_name=uvar("category");

		$display_main_dir = $set_ers_path . "main/";
		$display_full_dir = $set_ers_path . "full/";
		$display_thumb_dir = $set_ers_path . "";

		$main_dir = $_SERVER['DOCUMENT_ROOT'] . $display_main_dir;
		$full_dir = $_SERVER['DOCUMENT_ROOT'] . $display_full_dir;
		$thumb_dir = $_SERVER['DOCUMENT_ROOT'] . $display_thumb_dir;

		if(!is_dir($thumb_dir))
		{
			mkdir($thumb_dir,0755);
			if(!is_dir($main_dir))
			{
				mkdir($main_dir,0755);
			}
			if(!is_dir($full_dir))
			{
				mkdir($full_dir,0755);
			}
		}

		$tabs = array("Main Picture");

		$selected_tab = reqvar("tab_selected","Main Picture");
		$selected_tabname = strvar($selected_tab);

		$tab_pictures = array();
		for($x=0; $x<count($tabs); $x++)
		{
			$tabname = strvar($tabs[$x]);
			$tab_pictures[$tabname] = reqvar($tabname);
		}
		$set_misc_content = reqvar('misc_content');

		$update_picture = false;
		$update_message = "";
		if(isset($_GET['setfile']))
		{
			$update_picture = urldecode($_GET['setfile']);
		}
		else if(isset($_FILES['picture']))
		{
			$update_picture = false;
			$basename = basename($_FILES['picture']['name']);

			$basename_parts = explode(".",$basename);
			if(count($basename_parts) > 1)
			{
				$basename_ext = strtolower($basename_parts[count($basename_parts)-1]);
				$allowed_ext = array("gif","jpg","jpe","jpeg","png");
				$is_allowed = false;
				for($x=0; $x<count($allowed_ext); $x++)
				{
					if($allowed_ext[$x]==$basename_ext)
						$is_allowed = true;
				}

				if($is_allowed)
				{
					$uploadfile = $full_dir . $basename;
					if(rs_move_uploaded_file($_FILES['picture']['tmp_name'], $uploadfile))
					{
						chmod($uploadfile,0775);

						$mainfile = $main_dir . $basename;
						$convertcmd = "convert ".lavuShellEscapeArg($uploadfile)." -thumbnail ".$inv_main_size[0]."x".$inv_main_size[1]."> ".lavuShellEscapeArg($mainfile);
						lavuExec($convertcmd);
						chmod($mainfile,0775);

						$thumbfile = $thumb_dir . $basename;

						$convertcmd = "convert ".lavuShellEscapeArg($uploadfile)." -thumbnail ".$inv_thumb_size[0]."x".$inv_thumb_size[1]."> ".lavuShellEscapeArg($thumbfile);
						lavuExec($convertcmd);
						chmod($thumbfile,0775);
					}

					$update_picture = $basename;
				}
				else
				{
					$update_message = "<b>This file supplied is not supported.  Please upload a gif, jpg, or png image.</b><br>";
				}
			}
		}
		if($update_picture)
		{
			if($update_picture=="none")
				$tab_pictures[$selected_tabname] = "";
			$tab_pictures[$selected_tabname] = $update_picture;
		}
		$selected_picture = $tab_pictures[$selected_tabname];
		$fwd_picture_vars = "&dir=".$setdir;
		for($x=0; $x<count($tabs); $x++)
		{
			$tabname = strvar($tabs[$x]);
			$fwd_picture_vars .= "&".$tabname."=".urlencode($tab_pictures[$tabname]);
		}

		$tab_content = array();
		$tab_script = array();
		for($i=0; $i<count($tabs); $i++)
		{
			$tabname = strtolower(str_replace(" ","_",$tabs[$i]));
			$set_tab_picture = (isset($tab_pictures[$tabname]))?$tab_pictures[$tabname]:false;
			if($set_tab_picture && $set_tab_picture!="" && $set_tab_picture!="none")
			{
				$new_picture_display = $set_ers_path . $set_tab_picture;
				$tab_content[$tabname] = "<img src='" . $new_picture_display . "' />";
				$tab_script[$tabname] = "document.getElementById('file_selector').value = \"$set_tab_picture\"; ";
			}
			else
				$tab_script[$tabname] = "document.getElementById('file_selector').value = \"\"; ";
			$tab_script[$tabname] .= "document.getElementById('picture_controls').style.display = 'block'; ";
		}

		$extra_apply_code = "";
		$extra_onchange_code = "";

		$apply_main_picture = $tab_pictures['main_picture'];
		if(isset($tab_pictures['picture_2']))
		{
			$apply_picture2 = $tab_pictures['picture_2'];
			$extra_apply_code .= ",\"$apply_picture2\"";
		}
		if(isset($tab_pictures['picture_3']))
		{
			$apply_picture3 = $tab_pictures['picture_3'];
			$extra_apply_code .= ",\"$apply_picture3\"";
		}

		$new_picture_display = $set_ers_path . $apply_main_picture;

		// get a list of all available pictures
		$piclist = array();
		if ($handle = opendir($thumb_dir)) {
			while (false !== ($file = readdir($handle))) {
				if ($file != "." && $file != "..") {
					if(is_file($thumb_dir . "/" . $file))
						$piclist[] = $file;
				}
			}
			closedir($handle);
		}
		sort($piclist);

		// get the tabs
		$tab_extras = array();
		$tab_extras['tab_right_content'] = "<input type='button' value='Apply' onclick='picture_apply()'>";
		$tab_text = create_tab_area($tabs,$tab_content,$tab_script,$selected_tab,$tab_extras);

		if (isset($_GET['view_grid']) && $_GET['view_grid'] == '1')
			$toprint .= set_inventory_picture_grid_tohtml($set_ers_path, $piclist, $fwd_picture_vars, $selected_picture, $updateid, $extra_apply_code);
		else
			$toprint .= set_inventory_picture_tohtml($extra_apply_code, $fwd_picture_vars, $tab_text, $extra_onchange_code, $piclist, $update_message, $selected_picture, $updateid,$category_name);

		$toprint .= "<script type='text/javascript'>
			function picture_apply() {
				window.parent.set_inv_picture(\"".$updateid."\",\"".$apply_main_picture."\",\"".$new_picture_display."\"".$extra_apply_code.");
			}
		</script>";
	}
	else if($mode=="olo_set_inventory_picture")
	{
	    if(!$setdir) exit();
	    $set_ers_path = $setdir;
	    $updateid = uvar("updateid");
	    $category_name=uvar("category_name");
	    $catName=$category_name."_banner";
	    
	    
	    $display_main_dir = $set_ers_path . "main/";
	    $display_full_dir = $set_ers_path . "full/";
	    $display_thumb_dir = $set_ers_path . "";
	    
	    $main_dir = $_SERVER['DOCUMENT_ROOT'] . $display_main_dir;
	    $full_dir = $_SERVER['DOCUMENT_ROOT'] . $display_full_dir;
	    $thumb_dir = $_SERVER['DOCUMENT_ROOT'] . $display_thumb_dir;
	    
	    if(!is_dir($thumb_dir))
	    {
	        mkdir($thumb_dir,0755);
	        if(!is_dir($main_dir))
	        {
	            mkdir($main_dir,0755);
	        }
	        if(!is_dir($full_dir))
	        {
	            mkdir($full_dir,0755);
	        }
	    }
	    
	    $tabs = array("Main Picture");
	    if(1==2)
	    {
	        $tabs[] = "Picture 2";
	        $tabs[] = "Picture 3";
	    }
	    $selected_tab = reqvar("tab_selected","Main Picture");
	    $selected_tabname = strvar($selected_tab);
	    
	    $tab_pictures = array();
	    for($x=0; $x<count($tabs); $x++)
	    {
	        $tabname = strvar($tabs[$x]);
	        $tab_pictures[$tabname] = reqvar($tabname);
	    }
	    $set_misc_content = reqvar('misc_content');
	    
	    $update_picture = false;
	    $update_message_olo = "";
	    
	    
	    
	    if(isset($_FILES['olo_picture']))
	    {
	        
	        
	        $update_picture = false;
	        $basename = basename($_FILES['olo_picture']['name']);
	        $basename_parts = explode(".",$basename);
	        if(count($basename_parts) > 1)
	        {
	            $basename_ext = strtolower($basename_parts[count($basename_parts)-1]);
	            $allowed_ext = array("jpg","jpeg","png");
	            $is_allowed = false;
	            for($x=0; $x<count($allowed_ext); $x++)
	            {
	                if($allowed_ext[$x]==$basename_ext)
	                    $is_allowed = true;
	            }
	            
	            if($is_allowed)
	            {
	                //$lavu_togo_name = $location_info['lavu_togo_name'];
	                //if($lavu_togo_name != ''){
	                if(strpos($_GET['dir'],'/categories/')){
	                    $fixed_width = 1500;
	                    $fixed_height = 300;
	                    $tmp_name = $_FILES['olo_picture']['tmp_name'];
	                    $image_info   = getimagesize( $tmp_name );
	                    $image_width  = $image_info[0];
	                    $image_height = $image_info[1];
	                    
	                    $required_name=$catName.".".$basename_ext;
	                    
	                    if($image_width >= $fixed_width && $image_height >= $fixed_height){
	                        
	                        $uploadfile = $thumb_dir.$required_name;
	                        
	                        $delete_png=unlink($thumb_dir.$catName.".png");
	                        $delete_jpg=unlink($thumb_dir.$catName.".jpg");
	                        $delete_jpeg=unlink($thumb_dir.$catName.".jpeg");
	                        if(rs_move_uploaded_file($_FILES['olo_picture']['tmp_name'], $uploadfile))
	                        {
	                            chmod($uploadfile,0755);
	                            $update_message_olo="success##".$uploadfile;
	                        }
	                        else{
	                            
	                            $update_message_olo="<b>Please check the upload file path.</b>";
	                        }
	                    }
	                    else
	                    {
	                        $update_message_olo = "<b>Please upload a image with minimum dimension (1500 * 300)px.</b><br>";
	                    }
	                }
	                
	                //}
	                
	                
	                }
	                else
	                {
	                    $update_message_olo = "<b>This file supplied is not supported.  Please upload a jpg or png image.</b><br>";
	                }
	            }
	        }
	        
	        if($update_picture)
	        {
	            if($update_picture=="none")
	                $tab_pictures[$selected_tabname] = "";
	                $tab_pictures[$selected_tabname] = $update_picture;
	        }
	        $selected_picture = $tab_pictures[$selected_tabname];
	        $fwd_picture_vars = "&dir=".$setdir;
	        for($x=0; $x<count($tabs); $x++)
	        {
	            $tabname = strvar($tabs[$x]);
	            $fwd_picture_vars .= "&".$tabname."=".urlencode($tab_pictures[$tabname]);
	        }
	        
	        $tab_content = array();
	        $tab_script = array();
	        for($i=0; $i<count($tabs); $i++)
	        {
	            $tabname = strtolower(str_replace(" ","_",$tabs[$i]));
	            $set_tab_picture = (isset($tab_pictures[$tabname]))?$tab_pictures[$tabname]:false;
	            if($set_tab_picture && $set_tab_picture!="" && $set_tab_picture!="none")
	            {
	                $new_picture_display = $set_ers_path . $set_tab_picture;
	                $tab_content[$tabname] = "<img src='" . $new_picture_display . "' />";
	                $tab_script[$tabname] = "document.getElementById('file_selector').value = \"$set_tab_picture\"; ";
	            }
	            else
	                $tab_script[$tabname] = "document.getElementById('file_selector').value = \"\"; ";
	                $tab_script[$tabname] .= "document.getElementById('picture_controls').style.display = 'block'; ";
	        }
	        
	        $extra_apply_code = "";
	        $extra_onchange_code = "";
	        
	        $apply_main_picture = $tab_pictures['main_picture'];
	        if(isset($tab_pictures['picture_2']))
	        {
	            $apply_picture2 = $tab_pictures['picture_2'];
	            $extra_apply_code .= ",\"$apply_picture2\"";
	        }
	        if(isset($tab_pictures['picture_3']))
	        {
	            $apply_picture3 = $tab_pictures['picture_3'];
	            $extra_apply_code .= ",\"$apply_picture3\"";
	        }
	        
	        $new_picture_display = $set_ers_path . $apply_main_picture;
	        
	        // get a list of all available pictures
	        $piclist = array();
	        if ($handle = opendir($thumb_dir)) {
	            while (false !== ($file = readdir($handle))) {
	                if ($file != "." && $file != "..") {
	                    if(is_file($thumb_dir . "/" . $file))
	                        $piclist[] = $file;
	                }
	            }
	            closedir($handle);
	        }
	        sort($piclist);
	        
	        // get the tabs
	        $tab_extras = array();
	        $tab_extras['tab_right_content'] = "<input type='button' value='Apply' onclick='picture_apply()'>";
	        $tab_text = create_tab_area($tabs,$tab_content,$tab_script,$selected_tab,$tab_extras);
	        
	        if (isset($_GET['view_grid']) && $_GET['view_grid'] == '1')
	            $toprint .=olo_set_inventory_picture_grid_tohtml($set_ers_path, $piclist, $fwd_picture_vars, $selected_picture, $updateid, $extra_apply_code);
	            else
	                $toprint .= olo_set_inventory_picture_tohtml($extra_apply_code, $fwd_picture_vars, $tab_text, $extra_onchange_code, $piclist, $update_message_olo, $selected_picture, $updateid,$category_name);
	                
	                
	}//end of olo....
	else if($mode=="set_special")
	{
		$item_name = uvar("item_name");
		$item_title = uvar("item_title");
		$item_value = uvar("item_value");
		$item_edit = uvar("item_edit");
		$item_extra1 = uvar("extra1"); // row id for menu_categories, menu_items
		$item_extra2 = uvar("extra2"); // forced_modifier_group_id for menu_items
		$row_name = uvar("rowname");

		$special_table = false;
		$special_cond = "";
		$dfields = array();

		$profile_list = array();

		$lavu_only = "<font color='#AECD37'><b>*</b></font> ";

		if ($item_title == speak("Details"))
		{
			$locationid = sessvar("locationid");
			$rdb = admin_info("database");
			lavu_connect_byid(admin_info("companyid"),$rdb);

			$edit_type = "form";
			$special_table = "menu_items";
			$special_title = speak("Details")." - " . $row_name;
			$special_subtitle = (is_numeric($item_extra1) && $item_extra1>0)?speak("Item ID").": ".$item_extra1:"";

			$printer_list = array();
			$printer_list[] = array("Category Default","0");
			$printer_list[] = array("No Printer","");

			$pr_query = lavu_query("select * from `config` where `location`='[1]' and `type`='printer' and `_deleted`!='1' ORDER BY `value2` ASC", $locationid);
			while($pr_read = mysqli_fetch_assoc($pr_query))
			{
				if(strtolower(substr($pr_read['setting'],0,7)=="kitchen"))
				{
					$k = substr($pr_read['setting'],7);
					if($k=="") $k = 1;
					$printer_list[] = array($pr_read['value2'],$k);
				}
			}

			$pgroup_query = lavu_query("select * from `config` where `location` = '[1]' AND `type`='printer_group' AND `_deleted` != '1' ORDER BY `setting` ASC", $locationid);
			while($pgroup_read = mysqli_fetch_assoc($pgroup_query))
			{
				$printer_list[] = array($pgroup_read['setting'],$pgroup_read['id'] + 1000);
			}

			$super_group_list = array(array(speak("Category Default","")));
			$sgroup_query = lavu_query("select * from `super_groups` where `_deleted` = '0' order by `_order` asc");
			while($sgroup_read = mysqli_fetch_assoc($sgroup_query))
			{
				$super_group_list[] = array($sgroup_read['title'],$sgroup_read['id']);
			}

			$special_options = array();
			$special_options[] = array("", "");

			$gift_or_loyalty = forcedModifierType($item_extra2);
			if (in_array($gift_or_loyalty, array("gift_card", "loyalty_card"))) {
				includeExtensionOptions($gift_or_loyalty, $special_options);	
			}

			$special_options[] = array("Weigh Multiple Items Tare", "wmi_tare"); // hidden_value = tare value with unit (i.e., 4.5 oz); hidden_value3 = secondary item id to which remaining weight is applied
			if ($modules->hasModule("customers.billing")) {
				$special_options[] = array(speak("Recurring Billing Item"), "recurring_item"); // hidden_value = amount to use for future recurring billing invoices
			}
			if ($location_info['component_package_code'] == "lavukart") {
				$special_options[] = array(speak("Facility Buyout"), "facility buyout"); // hidden_value = duration of buyout in minutes
				$special_options[] = array(speak("Membership"), "membership"); // hidden_value = numer of race credit to apply
				$special_options[] = array(speak("Race Credits"), "race credits"); // hidden_value = number of race credits to apply
				$special_options[] = array(speak("Race Sequence"), "race sequence"); // hidden_value = event sequence title
				$special_options[] = array(speak("Room Rental"), "group event"); // hidden_value = duration of event in minutes
				$special_options[] = array(speak("Track Rental"), "track rental"); // hidden_value = duration of rental in minutes
				$special_options[] = array(speak("Liquor Sales"), "liquor sales");
			}

			$dfields[] = array("description","textarea",speak("Description"));
			$dfields[] = array("UPC","text", speak("UPC"));

			if ($location_info['display_product_code_and_tax_code_in_menu_screen']) {
				$dfields[] = array("product_code","text",speak("Product Code"));
				$dfields[] = array("tax_code","text",speak("Tax Code"));
			}

			$dfields[] = array("active","select",speak("Display in App"),array(array(speak("Category Default"),"1"),array(speak("No"),"0")));
			if ($location_info['use_lavu_togo']) $dfields[] = array("ltg_display","select","Display in Lavu ToGo",array(array("Category Default","1"),array("No","0")));
			$dfields[] = array("quick_item","select",speak("Include in Quick Item List"),array(array(speak("No"),"0"),array(speak("Yes"),"1")));
			$dfields[] = array("print","select",speak("Print"),array(array(speak("Yes"),"1"),array(speak("No"),"0")));
			$dfields[] = array("printer","select",speak("Printer"),$printer_list,"Default:0");
			if (admin_info("lavu_admin") == 1) $dfields[] = array("apply_taxrate","select",$lavu_only.speak("Apply Tax Rate"),array(array(speak("Default"),"Default"),array(speak("Custom"),"Custom")));
			if (admin_info("lavu_admin") == 1) $dfields[] = array("custom_taxrate","text",$lavu_only.speak("Custom Tax Rate"));
			$dfields[] = array("tax_profile_id","select",speak("Tax Profile"),get_profile_list($locationid, "tax", "item"));
			$dfields[] = array("tax_inclusion","select",speak("Tax included in price"),array(array(speak("Category Default"),"Default"),array(speak("No"),"0"),array(speak("Yes"),"1")));
			$dfields[] = array("no_discount","select",speak("Allow Discounts"),array(array(speak("Category Default"),""), array(speak("Yes"),"0"),array(speak("No"),"1")));
			$dfields[] = array("allow_deposit","select",speak("Allow Deposit"),array(array(speak("No"),"0"),array(speak("Yes"),"1")));
			$dfields[] = array("open_item","select",speak("Open Item"),array(array(speak("No"),"0"),array(speak("Name/Price"),"1"),array(speak("Price Only"),"2")));
			$dfields[] = array("hidden_value2","select",speak("Special"),$special_options);
			$dfields[] = array("hidden_value","text",speak("Special Details 1"));
			$dfields[] = array("hidden_value3","text",speak("Special Details 2"));
			if ($modules->hasModule("dining.price_tiers"))
				$dfields[] = array("price_tier_profile_id", "select", "Price tier profile", get_profile_list($locationid, "price_tier", "item"));
			$dfields[] = array("super_group_id","select","Super Group Id",$super_group_list);
			$dfields[] = array("happyhour", "happyhour", "Happy Hour");
			if ($modules->hasModule("dining.86countdown")) {
				//TODO:- Removed this if else condition after full migration implementation
                if (sessvar("INVENTORY_MIGRATION_STATUS") == "Completed_Ready" || sessvar("INVENTORY_MIGRATION_STATUS") == "Skipped_Ready") {
                    $dfields[] = array("track_86_count", "select", "Track 86 countdown", array(array(speak("Don't track"), ""), array("Track by Menu Item Level", "86count"), array("Track by Inventory Item Level", "86countInventory")));  // ,array("Track by ingredients","ingredients")
                } else {
                    $dfields[] = array("track_86_count","select","Track 86 countdown",array(array(speak("Don't track"),""),array("Track by 86 count","86count")));  // ,array("Track by ingredients","ingredients")
				}
					$dfields[] = array("inv_count","number","86 count");
			}

		}
		else if ($item_title == speak("Category Details"))
		{
			$locationid = sessvar("locationid");
			$rdb = admin_info("database");
			lavu_connect_byid(admin_info("companyid"),$rdb);

			$edit_type = "form";
			$special_table = "menu_items";
			$special_title = speak("Category Details")." - " . $row_name;

			$print_order = array();
			for ($i = 1; $i <= 20; $i++) {
				$print_order[] = array($i, $i);
			}

			$dfields[] = array("description","textarea",speak("Description"));
			if($norway_status=='1'){ 
			    $dfields[] = array("norway_menu_mapping","select",speak("Norway Menu Mapping"),get_menucode_list());
			}
			$dfields[] = array("active","select",speak("Display in App"),array(array(speak("Yes"),"1"),array(speak("No"),"0")));
			if ($location_info['use_lavu_togo']) $dfields[] = array("ltg_display","select","Display in Lavu ToGo",array(array("Yes","1"),array("No","0")));
			$dfields[] = array("print","select",speak("Print"),array(array(speak("Yes"),"1"),array(speak("No"),"0")));
			$dfields[] = array("print_order","select",speak("Print Order"),$print_order);
			$dfields[] = array("_order","text",speak("Display Order"));
			if (admin_info("lavu_admin") == 1) $dfields[] = array("apply_taxrate","select",$lavu_only.speak("Apply Taxrate"),array(array(speak("Default"),"Default"),array(speak("Custom"),"Custom")));
			if (admin_info("lavu_admin") == 1) $dfields[] = array("custom_taxrate","text",$lavu_only.speak("Custom Taxrate"));
			$dfields[] = array("enable_reorder","select",speak("Enable Reorder"),array(array(speak("Yes"),"1"),array(speak("No"),"0")));
			$dfields[] = array("tax_profile_id","select",speak("Tax Profile"),get_profile_list($locationid, "tax", "category"));
			$dfields[] = array("tax_inclusion","select",speak("Tax included in prices"),array(array(speak("Location Default"),"Default"),array(speak("No"),"0"),array(speak("Yes"),"1")));
			$dfields[] = array("no_discount","select",speak("Allow Discounts"),array(array(speak("Yes"),"0"),array(speak("No"),"1")));
			if ($modules->hasModule("dining.price_tiers"))
				$dfields[] = array("price_tier_profile_id", "select", "Price tier profile", get_profile_list($locationid, "price_tier", "category"));

			$super_group_list = array(array("",""));
			$sgroup_query = lavu_query("select * from `super_groups` where `_deleted` = '0' order by `_order` asc");
			while($sgroup_read = mysqli_fetch_assoc($sgroup_query))
			{
				$super_group_list[] = array($sgroup_read['title'],$sgroup_read['id']);
			}
			$dfields[] = array("super_group_id","select","Super Group Id",$super_group_list);


			/***************TODO **************/

			//$dfields[] = array("copy_move","select","Copy/Move");
			//$dfields[] = array("copy_move","select","To");

			$dfields[] = array("happyhour", "happyhour_category", "Happy Hour");
			//$dfields[] = array("open_item","select","Open Item",array(array("No","0"),array("Yes","1")));
			//$dfields[] = array("hidden_value2","text","Special");
			//$dfields[] = array("hidden_value","text","Special Details 1");
			//$dfields[] = array("hidden_value3","text","Special Details 2");
			//$dfields[] = array("allow_deposit","select","Allow Deposit",array(array("No","0"),array("Yes","1")));*/
		}
		else if ($item_title == "Ingredients")
		{
			$edit_type = "form";
			$special_table = "menu_items";
			$special_title = "Ingredients - " . $row_name;
			$dfields[] = array("ingredients","ingredients","Ingredients");
            $showVal = speak('Ingredient');
            $showVals = speak('Ingredients');
		}
		else if ($item_title == "Nutrition")
		{
			$edit_type = "form";
			$special_table = "menu_items";
			$special_title = "Nutrition - " . $row_name;
			$dfields[] = array("nutrition","nutrition","Nutrition");
			$showVal = speak('nutrition');
			$showVals = speak('Nutrition');
		}		
		else if ($item_title == "Dietary Info")
		{
			$edit_type = "form";
			$special_table = "menu_items";
			$special_title = "Dietary Info - " . $row_name;
			$dfields[] = array("dietary_info","dietary_info","Dietary Info");
			$showVal = speak('dietary_info');
			$showVals = speak('Dietary Info');
		}
		else if ($item_title == speak("Combo Items"))
		{
			$edit_type = "form";
			$special_table = "menu_items";
			$special_title = "Combo Items - " . $row_name;
			$dfields[] = array("comboitems","comboitems","comboitems");
		}
		else if ($item_title == speak("Discount Details")){//------------------------------------
		    global $ifNotPassbook_js;
		    $edit_type = "form";
			$special_table = "discount_types";

			$special_title = speak("Discount Details")." - " . $row_name;

			$item_discount_id = str_replace("details","special",$item_name);
			$get_discount_type_js = "discount_type_var = window.parent.document.getElementById('$item_discount_id').value;";

			//Note as messy as this is there's no way in php to see what the discount type is, we can only detect it JavaScript after the fact
			//  and then remove the elements which we previously added using php.
			//  --We do this by removing all tables following the first one which is the header.
			$ifNotPassbook_js  = " <script type='text/javascript'> discount_type_var = window.parent.document.getElementById('$item_discount_id').value;
			                         if(discount_type_var != 'passbook') {
			                             var allElems = document.getElementsByTagName(\"table\");
			                             for(var k = 1; k < allElems.length; k++){
			                                 allElems[k].parentNode.removeChild(allElems[k--]);
			                             }
			                             document.write('There are no detail options for this discount type.');
			                         }
			                         else{

			                         }
			                      </script>";


			// params:                                      // These will only be used if empty
    		// array(yearsBeforeThisYear, yearsAfterThisYear, yearPosition, default to todays date);
    		// note that if defaultToTodaysDate is set, then yearPosition will naturally be overridden to 0.
    		//                  table           column_name           type           label     params.
    		//The last array has 4 entries: (years prior to this year, years after this year, start position of select, default to todays date).
    		//The last two fields will be overridden if data is pulled from the database, it will be defaulted to that value then.


    		////$dfields[] = array("valid_start_date_inc","date_selector", "Begin Sending",    array(0,  10,  0, true));
    		////$dfields[] = array("valid_end_date_inc",  "date_selector", "End Sending",      array(0,  10,  0, true));
    		$dfields[] = array("expiration_date", "date_selector", "Expiration Date",  array(0,  10,  0, true));

		}
		if($edit_type=="form")
		{
			if (!empty($special_title)) $toprint .= "<table class='flex' cellpadding=6>
				<tr>
					<td width='650px' bgcolor='#777777' class='flex' style='justify-content: center;'>
						<span class='headerTxt'>" . $special_title . "</span>";
			if (!empty($special_subtitle)) $toprint .= "<br><span class='headerTxt' style='justify-content: center;'>" . $special_subtitle . "</span>";
			if (!empty($special_title)) $toprint .= "
					</td>
				</tr>
			</table>";


			if (!empty($special_subtitle)) $toprint .= "<tr></tr>";

			//$qhidden = array("print","printer","quick_item","apply_taxrate","custom_taxrate");


			//$dfields[] = array("reminder","textarea","Reminder (customer must checkmark)");
			//$dfields[] = array("notes","textarea","Notes (for receipt)");
			//$dfields[] = array("available","select","Available to Customer",array("Yes","Display Only","No"));
			//$dfields[] = array("taxable","select","Taxable",array("","Yes","No"));
			//$dfields[] = array("setup_area","text","Setup Area");
			//$dfields[] = array("actual_size","text","Actual Size");
			//$dfields[] = array("outlets","text","Outlets");
			//$dfields[] = array("monitors","text","Monitors");
			//$dfields[] = array("extra_fields","extra");

			function drow_code($dtitle,$dcode)
			{

				$str = "
					<tr>
						<td align='right' valign='top'>";
				if (strlen($dtitle) > 0)
					$str .= "
							".$dtitle;
				$str .= "
						</td>
						<td>";
				if (strlen($dcode) > 0)
					$str .= "
							".$dcode;
				$str .= "
						</td>
					</tr>";
				return $str;
			}

			// used by the javascript to retrieve the catagory id
			$catagoryid_index_parentid = "";
			$itemid = "";
			{
				$itemcatagoryid = str_replace("details","categoryid",$item_name);
				$parts = explode("_", $itemcatagoryid);
				$catagoryid_index_parentid = $parts[0]."_".$parts[1]."_".$parts[2];
				$itemid = $parts[3];
			}

			$js_getvals = "";
			$js_setvals = "";
			$toprint .= "
			<form name='dform' id= 'dForm' method='post'>
				<table>";
			for($i=0; $i<count($dfields); $i++)
			{
				$dname = $dfields[$i][0];
				$dtype = $dfields[$i][1];
				$dtitle = (isset($dfields[$i][2]))?$dfields[$i][2]:$dname;
				$dprop = (isset($dfields[$i][3]))?$dfields[$i][3]:false;
				$dval = "";
				if (isset($dfields[$i][4]) && substr($dfields[$i][4], 0, 8)=="Default:") {
					$sep = explode(":", $dfields[$i][4]);
					$dval = $sep[1];
				}
				$get_js_code = "setval = window.parent.document.getElementById('".str_replace("details",$dname,$item_name)."').value; carryValueOrSetDefault(\"".$dname."\" ,document.dform.$dname, setval);";
				$name_on_parent = str_replace("details",$dname,$item_name);
				$set_js_code = "setval = document.dform.$dname.value;window.parent.document.getElementById('".$name_on_parent."').value = setval;";
				if ($dname =="active" || $dname=="ltg_display") {
					$set_js_code .= "var chkbox = window.parent.document.getElementById('".$name_on_parent."_cb'); if (chkbox) chkbox.checked = (setval == '1');";
				}

				if($dtype=="text")
				{
					$dval = str_replace('"','&quot;',$dval);
					$toprint .= drow_code($dtitle,"<input type='text' name='$dname' value=\"$dval\">");
				}
				else if($dtype=="number")
				{
					$toprint .= drow_code($dtitle,"<input type='number' name='$dname' value=\"$dval\">");
				}
				else if($dtype=="date_selector")
				{
				    ob_start();
				    require_once(dirname(__FILE__) . "/../scripts/dateChooser.php");
				    $toprint .= ob_get_contents();
				    ob_end_clean();

				    //create_date_picker takes the id of the html element whos value will change.
				    $toprint .= "<input type='hidden' id='$dname' name='$dname' value=\"$dval\">";
				    $toprint .= "<br>";

				    //array(0,10,false))
				    $params = $dprop;

				    //$get_js_code .= " alert('$dname = ' + setval); ";

				    //We need to set the specific javascript variable for `create_date_picker` 's javascript functions to be able to pick
				    // up the value.
				    $get_js_code .= "date_picker_setval_$dname = setval;";

				    //Note, if `setvar` is in the namespace 'create_date_picker' will use that time as default.
				    $toprint .=  drow_code($dtitle, create_date_picker($dname, $params[0], $params[1], $params[2], $params[3]) );
				    //$toprint .=  create_date_picker($dname, $params[0], $params[1], $params[2], $params[3]);

				}
				else if($dtype=="happyhour" || $dtype=="happyhour_category")
				{
					$toprint .= '<script type="text/javascript" src="../scripts/jquery.min.1.8.3.js"></script>';
					ob_start();
					require_once(dirname(__FILE__).'/../areas/happyhours/happyhour.js');
					$s_hh_js = ob_get_contents();
					ob_end_clean();
					$toprint .= '<script type="text/javascript">'.$s_hh_js.'</script>';
					$parentid = 'happyhour_parent';
					$val = str_replace("\"","&quot;",$dval);
					$dname_happyhours = $dname . "_happyhours";
					$val_happyhours = array();
					$hh_query = lavu_query("SELECT * FROM `happyhours` WHERE `_deleted` != '1'");
					if ($hh_query) {
						while ($row = mysqli_fetch_assoc($hh_query)) {
							array_push($val_happyhours, $row['start_time'].":".$row['end_time'].":".$row['weekdays'].":".$row['property'].":".$row['id'].":".$row['name'].":".$row['_deleted']);
						}
					}
					$val_happyhours = implode("|", $val_happyhours);
					$category_id_text = "category_id=window.parent.document.getElementById('$catagoryid_index_parentid').value";
					if ($dtype=="happyhour_category")
						$category_id_text = "category_id=null";
					//echo drow_code($dtitle, "\n<div id='$parentid'></div><input type='hidden' name='$dname' id='$dname' value=\"$val\"><input type='hidden' name='$dname_happyhours' id='$dname_happyhours' value=\"$val_happyhours\">\n");
					if ($val_happyhours == "")
						$toprint .= drow_code($dtitle, "\n<div id='$parentid'></div><input type='hidden' name='$dname' id='$dname' value=\"$val\"><input type='hidden' name='$dname_happyhours' id='$dname_happyhours' value=\"$val_happyhours\">".speak("There are no happy hours available for this account.")."\n");
					else
						$toprint .= drow_code($dtitle, "\n<div id='$parentid'></div><input type='hidden' name='$dname' id='$dname' value=\"$val\"><input type='hidden' name='$dname_happyhours' id='$dname_happyhours' value=\"$val_happyhours\"><script type='text/javascript'>$category_id_text;setTimeout(function(){ hh_loadhappyhour(\"$parentid\", \"$dname\",\"$dname_happyhours\") }, 30);</script>\n");

				}
				else if($dtype=="textarea")
				{
					$toprint .= drow_code($dtitle,"<textarea name='$dname' cols='30' rows='3'>$dval</textarea>");
				}
				else if($dtype=="select")
				{
					$str = "<select name='$dname'>";
					for($s=0; $s<count($dprop); $s++)
					{
						if(is_array($dprop[$s]))
						{
							$sval = isset($dprop[$s][0]) ? $dprop[$s][0] : "";
							$sindex = isset($dprop[$s][1]) ? $dprop[$s][1] : "";
						}
						else
						{
							$sval = $dprop[$s];
							$sindex = $dprop[$s];
						}
						$sval = str_replace('"','&quot;',$sval);

						$str .= "<option value=\"$sindex\"";
						if ($dval == $sindex) $str .= " selected";
						$str .= ">$sval</option>";
					}
					$str .= "</select>";
					$toprint .= drow_code($dtitle,$str);
				}else if( $dtype=='multi_text'){
					$str= "<input type='text' value='your mom'>";
					$toprint .= drow_code($dtitle, $str);
				}
				else if ($dtype == "ingredients")
				{ //echo "hai1"; exit;
					$rdb = admin_info("database");
					//lavu_select_db($rdb);
					lavu_connect_byid(admin_info("companyid"),$rdb);
					$locationid = sessvar("locationid");
                    //TODO:- Removed this if else condition after full migration implementation
                    if (sessvar("INVENTORY_MIGRATION_STATUS") == "Completed_Ready" || sessvar("INVENTORY_MIGRATION_STATUS") == "Skipped_Ready") {
                        $apos = ""; // put the character for apostrophe here
                        $ingredient_options = array("<option value=''></option>");
                        $a_replace_chars = array("'", '"', "\n");
                        // Getting inventory items from inventory_items table through inventory api call
                        $inventory_read = get_inventory_items();
                        $units = get_inventory_units();
						$inventoryItemsUnits = array();
                        function cmp($value1, $value2) {
                         return strcmp($value1["name"], $value2["name"]);
                        }
                        usort($inventory_read, "cmp");
                        foreach ($inventory_read as $invValues) {
                            $ingredient_options[] = "<option value='{$invValues['id']}'>" . trim(str_replace($a_replace_chars, $apos, $invValues['name'])) . "</option>";
                            $inventoryItemsUnits[$invValues['id']] = $invValues['salesUnitID'];
                        }

                        $get_js_code = "
						var itemsUnits = new Array(); // inventory_items id and unit symbol mapping
						itemsUnits = ";
                        $get_js_code .= json_encode($inventoryItemsUnits);
                        $get_js_code .= ";";
                        $get_js_code .= "
						var units = new Array(); // unit id and unit symbol mapping
						units = ";
                        $get_js_code .= json_encode($units);
                        $get_js_code .= ";";
                        $get_js_code .= "
						" . $dname . "_itemnum = 0;
						function add_more_" . $dname . "(isetval,setunit,qsetval) {
							if(!isetval) isetval = '';
							if(!qsetval) qsetval = '1';
							if(!setunit) setunit = '';
							var item_options = \"";
                        foreach ($ingredient_options as $s_option) {
                            $get_js_code .= $s_option;
                        }
                        $get_js_code .= "\";";
                        $get_js_code .= "
							var t = document.createElement('div');
							var usetval = '';
							if(units[setunit] != undefined) {
								usetval = units[setunit];
							}
							t.setAttribute('id', 'ingredient-'+" . $dname . "_itemnum);
							t.innerHTML += '<table><tr><td><select id=\"" . $dname . "_item' + " . $dname . "_itemnum + '\" onChange=\'click_item(this, \"" . $dname . "_unit_display' + " . $dname . "_itemnum + '\", \"" . $dname . "_unit' + " . $dname . "_itemnum + '\")\'>' + item_options + '</select></td><td><input id=\"" . $dname . "_qty' + " . $dname . "_itemnum + '\" type=\"text\" size=\"3\" value=\"' + qsetval + '\" /></td><td><input id=\"" . $dname . "_unit_display' + " . $dname . "_itemnum + '\" type=\"text\" size=\"5\" value=\"' + usetval + '\" disabled /><input type=\"hidden\" id=\"" . $dname . "_unit' + " . $dname . "_itemnum + '\" type=\"text\" size=\"5\" value=\"' + setunit + '\"  />&nbsp;&nbsp;<a class=\'closeBtn\' onclick=\'click_delete(\"ingredient-' + " . $dname . "_itemnum + '\")\'>X</a></td></tr></table>';
							document.getElementById('list_" . $dname . "').appendChild(t);
							document.getElementById('" . $dname . "_item' + " . $dname . "_itemnum).value = isetval;
	
							" . $dname . "_itemnum ++;
						}
	
						function click_delete(divId) {
							var element = document.getElementById(divId);
							element.parentNode.removeChild(element);
						}
	
						// Set UOM on inventory item change
						function click_item(itemObj, unitDisplayObj, unitObj) {
							var id  = itemObj.value;
							var uid = itemsUnits[id];
							document.getElementById(unitDisplayObj).value = units[uid];
							document.getElementById(unitObj).value = itemsUnits[id];
						}
	
						setvals = window.parent.document.getElementById('" . $item_name . "').value;
						ingredientunitvals = setvals.split(' xx ');
						setvals = ingredientunitvals[0].split(',');
						var setunits = [];
						if(ingredientunitvals.length > 0) {
							if (ingredientunitvals[1] !== undefined) {
								setunits = ingredientunitvals[1].split(',');
							}
						}
						for(i=0; i<setvals.length; i++) {
							setval = setvals[i].split(' x ');
							if(setval.length > 1 && setval[0]!='') {
								add_more_" . $dname . "(setval[0],setunits[i],setval[1]);
							}
						}";

                        $set_js_code = "
						setval = '';
						setunit = '';
						ing_count = 0;
						for(i=0; i<" . $dname . "_itemnum; i++) {
							element = document.getElementById('" . $dname . "_item' + i);
							if (element != null) {
								iival = document.getElementById('" . $dname . "_item' + i).value;
								iqval = document.getElementById('" . $dname . "_qty' + i).value;
								iuval = document.getElementById('" . $dname . "_unit' + i).value;
								if(iival!='') {
									if(setval!='') setval += ','; setval += iival + ' x ' + iqval; ing_count++;
									if(setunit!='') setunit += ','; setunit += iuval;
								}
							}
						}
						if(setval=='') {
							setcol = '#aaaaaa';
							showval = '".$showVal ."';
						} else {
							setcol = '#1c591c'; showval = ing_count + ' ".$showVal."'; if(ing_count > 1) showval = ing_count + ' ".$showVals."';
						}
						window.parent.document.getElementById('" . $item_name . "').value = setval + ' xx ' + setunit;
						window.parent.document.getElementById('" . $item_name . "' + '_display').style.color = setcol;
						window.parent.document.getElementById('" . $item_name . "' + '_display').value = showval;";

                        if (strpos($toprint, "<tr>") < 0) {
                            $toprint .= "
						<tr><td>";
                        }
                        $toprint .= "
							<input type='hidden' name='" . $dname . "'>
							<div id='list_" . $dname . "'>&nbsp;</div>
							<input type='button' value='Add More >>' onclick='add_more_" . $dname . "()'>
						";
                    } else {
                        $apos = ""; // put the character for apostrophe here
                        $ingredient_options = array("<option value=''></option>");
                        $ing_query = lavu_query("select `ingredients`.`title` as `title`, `ingredients`.`id` as `id`, `ingredients`.`unit` as `unit` from `ingredients` left join `ingredient_categories` on `ingredients`.`category`=`ingredient_categories`.`id` where `ingredients`.`loc_id` = '".$locationid."' AND `ingredients`.`_deleted`!='1' and `ingredient_categories`.`_deleted`!='1' order by `ingredients`.`title` asc");
                        $a_replace_chars = array("'", '"', "\n");
                        while($ing_read = mysqli_fetch_assoc($ing_query))
                        {
                            $ingredient_options[] = "<option value='{$ing_read['id']}'>".trim(str_replace($a_replace_chars,$apos,$ing_read['title'])." (".str_replace($a_replace_chars,$apos,$ing_read['unit']).")")."</option>";
                        }

                        $get_js_code = "
						".$dname."_itemnum = 0;
						function add_more_".$dname."(isetval,qsetval) {
							if(!isetval) isetval = '';
							if(!qsetval) qsetval = '1';
							var item_options = \"";
                        foreach($ingredient_options as $s_option) {
                            $get_js_code .= $s_option;
                        }
                        $get_js_code.="\";";
                        $get_js_code .= "
							var t = document.createElement('div');
							t.innerHTML += '<table><tr><td><select id=\"".$dname."_item' + ".$dname."_itemnum + '\">' + item_options + '</select></td><td><input id=\"".$dname."_qty' + ".$dname."_itemnum + '\" type=\"text\" size=\"3\" value=\"' + qsetval + '\" /></td></tr></table>';
							document.getElementById('list_".$dname."').appendChild(t);
							document.getElementById('".$dname."_item' + ".$dname."_itemnum).value = isetval;
							".$dname."_itemnum ++;
						}
		
						setvals = window.parent.document.getElementById('".$item_name."').value;
						setvals = setvals.split(',');
						for(i=0; i<setvals.length; i++) {
							setval = setvals[i].split(' x ');
							if(setval.length > 1 && setval[0]!='') {
								add_more_".$dname."(setval[0],setval[1]);
							}
						}";

                        $set_js_code = "
						setval = '';
						ing_count = 0;
						for(i=0; i<".$dname."_itemnum; i++) {
							iival = document.getElementById('".$dname."_item' + i).value;
							iqval = document.getElementById('".$dname."_qty' + i).value;
							if(iival!='') {
								if(setval!='') setval += ','; setval += iival + ' x ' + iqval; ing_count++;
							}
						}
						if(setval=='') {
							setcol = '#aaaaaa';
							showval = '".$showVal ."';
						} else {
							setcol = '#1c591c'; showval = ing_count + ' ".$showVal."'; if(ing_count > 1) showval = ing_count + ' ".$showVals."';
						}
						window.parent.document.getElementById('".$item_name."').value = setval;
						window.parent.document.getElementById('".$item_name."' + '_display').style.color = setcol;
						window.parent.document.getElementById('".$item_name."' + '_display').value = showval;";

                        if (strpos($toprint, "<tr>") < 0) {
                            $toprint .= "
						<tr><td>";
                        }
                        $toprint .= "
							<input type='hidden' name='".$dname."'>
							<div id='list_".$dname."'>&nbsp;</div>
							<input type='button' value='Add More >>' onclick='add_more_".$dname."()'>
						";
                    }
                }
				elseif ($dtype == "comboitems"){
					$rdb = admin_info("database");
					//lavu_select_db($rdb);
					lavu_connect_byid(admin_info("companyid"),$rdb);
					$locationid = sessvar("locationid");

					$apos = ""; // put the character for apostrophe here
					$combo_options = array("<option value=''></option>");
					$ing_query = lavu_query("select `menu_items`.`name` as `title`, `menu_items`.`id` as `id` , `menu_items`.`price` as `item_price`   from `menu_items` left join `menu_categories` on `menu_items`.`category_id`=`menu_categories`.`id` left join `menu_groups` on `menu_categories`.`group_id`=`menu_groups`.`id` where `menu_items`.`combo` != '1' AND `menu_items`.`_deleted`!='1' and `menu_categories`.`_deleted`!='1' and `menu_groups`.`_deleted`!='1'  and  `menu_items`.`id`!='".$_GET['item_id']."' order by `menu_items`.`name` asc");
					$a_replace_chars = array("'", '"', "\n");
					$mcnt=0;
					while($ing_read = mysqli_fetch_assoc($ing_query))
					{
						//$ingredient_options[] = "<option value='{$ing_read['id']}'>".trim(str_replace($a_replace_chars,$apos,$ing_read['title'])." ")."</option>";
						$menu_item[$mcnt]['id'] = $ing_read['id'];
						$menu_item[$mcnt]['text'] = $ing_read['title'];

						$item_price[$mcnt]['id'] = $ing_read['id'];
						$item_price[$mcnt]['item_price'] = $ing_read['item_price'];
						$mcnt++;
					}

					$menu_json=json_encode ($menu_item);
					$item_price_json=json_encode ($item_price);
					$get_js_code = "
							function add_more_comboitems(type,elemid) {						
						        
						         var elemnt = document.getElementById('ival').value;
								 var parentids = elemnt;
						         var count = document.getElementById('ival').value;
						         var tablecnt = document.getElementById('itable').value;
						         var mtable = 'mtable_'+tablecnt;
								 var selmenuid = 'm_'+elemnt+'_selmenu';
							     var selmenutd = 'm_'+elemnt+'_td';
								 var txttd = 'm_'+elemnt+'_txt';
								 var startcnt = 1;
								 var hid = 'mtext_'+tablecnt;
								 var deltype = 'maintab';
						         var htmd = '<table id=\"'+mtable+'\" style=\"margin: 10px 0\"><tr><td><div class=\"flex\"><span id=\"'+selmenutd+'\" style=\"width:220px;\"><select class=\"js-example-data-array\" id=\"m_'+elemnt+'_selmenu\" name=\"m_'+elemnt+'_selmenu\" style=\"width: 95%\" onchange=\"get_menu_price(this.value,\'m_'+elemnt+'_price\')\"></select></span><span id=\"'+txttd+'\" style=\"display:none;\"><input type=\"text\" style=\"width:220px;\" id=\"m_'+elemnt+'_seltext\" name=\"m_'+elemnt+'_seltext\"/></span><span style=\"width:5%;\"><input id=\"m_'+elemnt+'_qty\" name=\"m_'+elemnt+'_qty\" title=\"Quantity\" value=\"1\" type=\"text\" size=\"3\" style=\"width:100%;\"/></span> <input id=\"m_'+elemnt+'_deleted\" name=\"m_'+elemnt+'_deleted\" value=\"\" type=\"hidden\" size=\"3\" /> <span style=\"padding-left: 12px; width:15%;\"> <input type=\"button\" id=\"m_option_'+elemnt+'\" name=\"m_option_'+elemnt+'\" value=\"+ OPTION\" onclick=\"add_more_opt(this.id,'+elemnt+',\''+mtable+'\',\''+parentids+'\',\''+hid+'\',\''+startcnt+'\')\"> </span> <span style=\"width:15%\"> <input type=\"button\" id=\"m_subopt_'+elemnt+'\" name=\"m_subopt_'+elemnt+'\" value=\"+ SUB OPTION\" onclick=\"add_more_subopt(this.id,'+elemnt+',\''+mtable+'\',\''+parentids+'\',\''+startcnt+'\')\"> </span>  <span style=\"padding-left: 20px\"><input id=\"m_'+elemnt+'_price\" name=\"m_'+elemnt+'_price\" title=\"Price\" type=\"text\" size=\"4\" style=\"width:100%;\"/></span><span style=\"padding-left: 50px\"><label>Header</label><input type=\"checkbox\" id=\"m_'+elemnt+'_header\" name=\"m_'+elemnt+'_header\" onclick=\"displayheader(this.checked,\''+selmenutd+'\')\" > </span><span style=\"padding-left: 50px;\"><a class=\"closeBtn\" id=\"m_'+elemnt+'_closebtn\" onclick=\"deloption(\''+mtable+'\',\''+deltype+'\')\"  >X</a></span></div></td></tr></table>';
						         
								var form = document.getElementById('comboitems');
								 
								 if(tablecnt==1)
								 {
						         	form.innerHTML = form.innerHTML + htmd;
								 }
							    else
								{
								  var tbl = $( \"div table:last-child\" );
								  
								  $(htmd).insertAfter(tbl);
								}
								var input = document.createElement(\"input\");

								input.setAttribute(\"type\", \"hidden\");
								
								input.setAttribute(\"id\", hid);
								
								input.setAttribute(\"value\", \"\");
								
								 //append to form element that you want .
								 document.getElementById(\"dForm\").appendChild(input);
						         count++;
						         tablecnt++;
						         document.getElementById('ival').value = count;
						         document.getElementById('itable').value = tablecnt;
								 document.getElementById('ioptval').value='';
								 selectauto();
					         }";

					$get_js_code.="
							       function add_more_opt(ids,count,tableid,parentids,hid,startcnt='',type=''){
							        var ioptval = document.getElementById('ioptval').value.toString();
							        var split_cnt = '';
									var opttab = '';
							        var setparentid = parentids;
							        var styletab = '';
							        var getcount = document.getElementById(hid).value;
							        var tablecnt = document.getElementById('itable').value;
									if(tableid.indexOf('_opt_')>-1)
									{
										var opttab = tableid;
									}
									else
									{
										var opttab = tableid+'_opt_';
									}
									var findopt = $('table[id^='+opttab+']');
									console.log(opttab)
							        if(tableid.indexOf('_opt_')>-1)
							        {
							         var y = tableid.split('_');
							         var x= y[0]+'_'+y[1]+'_'+y[2]+'_';
							        }
									else if(tableid.indexOf('_opt_')==-1 && (findopt.length)==0)
									{
										var searchtab = tableid+'_subopt_';
										var findsubopt = $('table[id^='+searchtab+']');
										var x = $(findsubopt[findsubopt.length-1]).attr('id');
									}
							        else
							        {
							         var x = tableid+'_opt_';
							        }
							        var alltab = $('table[id^=\"'+x+'\"]')
							        for(var i=0; i<alltab.length; i++)
							        {
							         var rendertabid = $(alltab[i]).attr('id');
							        }
							        if(!rendertabid)
							        {
							         split_cnt = 1;
							         rendertabid = tableid;
							        }
							        else
							        {
										 if(rendertabid.indexOf('_subopt_')>-1)
										 {
											if(findopt.length>0)
											{
												var y = $(findopt[findopt.length-1]).attr('id');
												var tabsplit = y.split('_');
									         	split_cnt = parseInt(tabsplit[3])+1;
											}
											else
											{
												if(findopt.length>0)
												{
													var tabsplit = rendertabid.split('_');
									         	    split_cnt = parseInt(tabsplit[3])+1;
												}
												else
												{
													split_cnt = 1;
												}
											}
										 }
										 else
										 {
											var tabsplit = rendertabid.split('_');
								         	split_cnt = parseInt(tabsplit[3])+1;
										 }
							        }
												
							        if(type=='subopt')
							        {
							         styletab = 'style=\"margin-left:40px;\"';
							        }
									else
									{
										//styletab = 'style=\"margin-left:10px;\"';
									}
							              var deltype = 'opttab';
							              var mtable = 'mtable_'+setparentid+'_opt_'+split_cnt;
							        var selmenuid = 'm_'+setparentid+'_opt_'+split_cnt+'_selmenu';
							           var selmenutd = 'm_'+setparentid+'_opt_'+split_cnt+'_td';
							        var txttd = 'm_'+setparentid+'_opt_'+split_cnt+'_txt';
							        var htmd = '<table id=\"'+mtable+'\" '+styletab+'><tr><td>OR</td></tr><tr><td id=\"'+selmenutd+'\"><select style=\"width:262px;\" class=\"js-example-data-array\" id=\"m_'+setparentid+'_opt_'+split_cnt+'_selmenu\"  name=\"m_'+setparentid+'_opt_'+split_cnt+'_selmenu\" onchange=\"get_menu_price(this.value,\'m_'+setparentid+'_opt_'+split_cnt+'_price\')\"></select></td><td id=\"'+txttd+'\" style=\"display:none;\"><input type=\"text\" style=\"width:90%;margin-top:4px;;\" id=\"m_'+setparentid+'_opt_'+split_cnt+'_seltext\" name=\"m_'+setparentid+'_opt_'+split_cnt+'_seltext\"/></td><td><input id=\"m_'+setparentid+'_opt_'+split_cnt+'_qty\" name=\"m_'+setparentid+'_opt_'+split_cnt+'_qty\" title=\"Quantity\" value=\"1\" type=\"text\" size=\"3\" /></td> <input id=\"m_'+setparentid+'_opt_'+split_cnt+'_deleted\" name=\"m_'+setparentid+'_opt_'+split_cnt+'_deleted\" value=\"0\" /> <td> <input type=\"button\" id=\"m_'+setparentid+'_opt_'+split_cnt+'\" name=\"m_'+setparentid+'_opt_'+split_cnt+'\" value=\"+ OPTION\" onclick=\"add_more_opt(this.id,'+split_cnt+',\''+mtable+'\','+setparentid+',\''+hid+'\',\'\')\"> </td> <td> <input type=\"button\" id=\"m_'+setparentid+'_subopt_'+split_cnt+'\" name=\"m_subopt_'+split_cnt+'\" value=\"+ SUB OPTION\" onclick=\"add_more_subopt(this.id,'+split_cnt+',\''+mtable+'\','+setparentid+',1)\"> </td>  <td><input id=\"m_'+setparentid+'_opt_'+split_cnt+'_price\" title=\"Price\" name=\"m_'+setparentid+'_opt_'+split_cnt+'_price\" type=\"text\" size=\"4\" /></td><td>Header</td><td><input type=\"checkbox\" id=\"m_'+setparentid+'_opt_'+split_cnt+'_header\" name=\"m_'+setparentid+'_opt_'+split_cnt+'_header\" onclick=\"displayheader(this.checked,\''+selmenutd+'\')\" > </td><td><a class=\"closeBtn\" id=\"m_'+setparentid+'_opt_'+split_cnt+'_closebtn\" onclick=\"deloption(\''+mtable+'\',\''+deltype+'\')\" >X</a></td></tr></table>';
									$(htmd).insertAfter('#'+rendertabid);
							        tablecnt++;
							   
							        document.getElementById('ioptcount').value = split_cnt;
							   
							        //document.getElementById(hid).value = concatval;
							   
							        selectauto();
							               }";

					$get_js_code.="
					function add_more_subopt(ids,count,tableid,pid,startcnt='',type=''){
						
					var ioptval = document.getElementById('isuboptval').value.toString();
					var split_cnt = '';
					var spids = ids.split('_');
					var optid = '';
					var settype = '';
					if(spids.length==3)
					{
						settype = 'subopt_opt';
					}
					else
					{
						settype = type;
					}
					
					var optpos='';
					if(tableid.indexOf('_opt_')>-1 && tableid.indexOf('_subopt_')==-1)
					{
						var x = tableid+'_subopt_';
						optpos = 3;
					}
					else if(tableid.indexOf('_opt_')==-1 && tableid.indexOf('_subopt_')>-1)
					{
						var searchtab = tableid;
						var y = searchtab.split('_');
						var x = y.slice(0, y.length - 1).join(\"_\") + \"_\";
					}
					else if(tableid.indexOf('_opt_')>-1 && tableid.indexOf('_subopt_')>-1)
					{
						var searchtab = tableid;
						var y = searchtab.split('_');
						var x = y.slice(0, y.length - 1).join(\"_\") + \"_\";
						optpos = 3;
					}
					else
					{
						var x = tableid+'_subopt_';
					}
					
					var alltab = $('table[id^=\"'+x+'\"]')
					for(var i=0; i<alltab.length; i++)
					{
						var rendertabid = $(alltab[i]).attr('id');
				    }
					if(!rendertabid)
					{
						var tabsplit = x.split('_');
						if(optpos!='')
						{
							optid = tabsplit[optpos];
							optid = '_opt_'+optid;
						}
						var rendertabid = tableid;
						split_cnt = 1;
					}
					else
					{
						var tabsplit = rendertabid.split('_');
						if(optpos!='')
						{
							optid = tabsplit[optpos];
							optid = '_opt_'+optid;
							split_cnt = parseInt(tabsplit[5])+1;
						}
						else
						{
							split_cnt = parseInt(tabsplit[3])+1;
						}
					}
					
					var deltype = 'subtab';
					//split_cnt = parseInt(startcnt);
					startcnt++;
					var tablecnt = document.getElementById('itable').value;
					var mtable = 'mtable_'+pid+''+optid+'_subopt_'+split_cnt;
					var selmenuid = 'm_'+pid+'_opt_'+optid+'_subopt_'+split_cnt+'_selmenu';
					var selmenutd = 'm_'+pid+'_opt_'+optid+'_subopt_'+split_cnt+'_td';
					var txttd = 'm_'+pid+'_opt_'+optid+'_subopt_'+split_cnt+'_txt';
						
					var htmd = '<table style=\"margin-left:40px;\" id=\"'+mtable+'\"><tr><td id=\"'+selmenutd+'\"><select style=\"width:262px;\" class=\"js-example-data-array\" id=\"m_'+pid+''+optid+'_subopt_'+split_cnt+'_selmenu\" name=\"m_'+pid+''+optid+'_subopt_'+split_cnt+'_selmenu\" onchange=\"get_menu_price(this.value,\'m_'+pid+''+optid+'_subopt_'+split_cnt+'_price\')\"></select></td><td><td id=\"'+txttd+'\" style=\"display:none;\"><input type=\"text\" style=\"width:220px;\" id=\"m_'+pid+''+optid+'_subopt_'+split_cnt+'_seltext\" name=\"m_'+pid+''+optid+'_subopt_'+split_cnt+'_seltext\"></td><td><input id=\"m_'+pid+''+optid+'_subopt_'+split_cnt+'_qty\" title=\"Quantity\" name=\"m_'+pid+''+optid+'_subopt_'+split_cnt+'_qty\" value=\"1\" type=\"text\" size=\"3\" /></td><input id=\"m_'+pid+''+optid+'_subopt_'+split_cnt+'_deleted\" name=\"m_'+pid+''+optid+'_subopt_'+split_cnt+'_deleted\" value=\"0\" type=\"hidden\" /> <td> <input type=\"button\" id=\"m_opt_subopt_'+split_cnt+'\" name=\"m_opt_subopt_'+split_cnt+'\" value=\"+ OPTION\" onclick=\"add_more_subopt(this.id,'+split_cnt+',\''+mtable+'\','+pid+','+startcnt+',\''+settype+'\')\"> </td> <td style=\"width:10.8%;\"> <input style=\"display:none;\" type=\"button\" id=\"m_opt_subopt_'+split_cnt+'\" name=\"m_opt_subopt_'+split_cnt+'\" value=\"+ SUB OPTION\" onclick=\"alert(\'Maximum 3 level is supported\')\"> </td>  <td><input id=\"m_'+pid+''+optid+'_subopt_'+split_cnt+'_price\" name=\"m_'+pid+''+optid+'_subopt_'+split_cnt+'_price\" title=\"Price\" type=\"text\" size=\"4\" /></td><td><a class=\"closeBtn\" id=\"m_'+pid+''+optid+'_subopt_'+split_cnt+'_closebtn\" onclick=\"deloption(\''+mtable+'\',\''+deltype+'\')\">X</a></td><td>&nbsp;</td><td>&nbsp; </td></tr></table>';
					
					$(htmd).insertAfter('#'+rendertabid);
					tablecnt++;
					split_cnt++;
					selectauto();
					}
							function displayheader(inputcheck,selmenutd)
							{
								
								var res = selmenutd.replace(\"td\", \"txt\");		
								var input_price = selmenutd.replace(\"td\", \"price\");
								if(inputcheck==true)
								{
									document.getElementById(selmenutd).style.display='none';
									document.getElementById(res).style.display='block';
									document.getElementById(input_price).value='0';
									document.getElementById(input_price).style.visibility='hidden';
								}
								else
								{
									document.getElementById(selmenutd).style.display='block';
									document.getElementById(res).style.display='none';
									document.getElementById(input_price).style.visibility='visible';
					
								}
								selectauto();
							    
							}
							
							function deloption(tableid,tabtype)
							{
								var strconfirm = confirm(\"Are you sure you want to delete?\");
								if (strconfirm == true) 
								{
									if(tabtype=='subtab')
									{
										var res = tableid.replace('mtable_', 'm_');
										res = res+'_deleted';
										document.getElementById(res).value=1;
										document.getElementById(tableid).style.display='none';
									}
									if(tabtype=='opttab' || tabtype=='maintab')
									{	
										var alltab = $('table[id^=\"'+tableid+'\"]')
										for(var i=0; i<alltab.length; i++)
										{
											var tabid = $(alltab[i]).attr('id');
											var res = tabid.replace('mtable_', 'm_');
											res = res+'_deleted';
											document.getElementById(res).value='1';
											document.getElementById(tabid).style.display='none';
										}
										
									}
								}
								else
								{
									return false;
								}
							}
							";

					$set_js_code = "
					setval = '';
					ing_count = 0;
					for(i=0; i<".$dname."_itemnum; i++) {
						iival = document.getElementById('".$dname."_item' + i).value;
						iqval = document.getElementById('".$dname."_qty' + i).value;
						if(iival!='') {
							if(setval!='') setval += ','; setval += iival + ' x ' + iqval; ing_count++;
						}
					}
					
					if(setval=='') {
						setcol = '#aaaaaa';
						showval = 'Combo Items';
					} else {
						setcol = '#1c591c'; showval = ing_count + ' Ingredient'; if(ing_count > 1) showval += 's';
					}
					window.parent.document.getElementById('".$item_name."').value = setval;
					window.parent.document.getElementById('".$item_name."' + '_display').style.color = setcol;
					window.parent.document.getElementById('".$item_name."' + '_display').value = showval;";

					if (strpos($toprint, "<tr>") < 0) {
						$toprint .= "
					<tr><td>";
					}




					function categoryParentChildTree($parent = 0, $spacing = '', $category_tree_array = '') {

						if (!is_array($category_tree_array))
							$category_tree_array = array();
							//echo "hhh"; die;
							if($_GET['item_id']==0)
							{
								$sqlCategory = lavu_query("select * from `combo_items` where parent_id ='".$parent."' and temp_id='".$_GET['item_name']."_".$_SESSION[randcomboid]."' and `_deleted`=0");
							}
							else
							{
								$sqlCategory = lavu_query("select * from `combo_items` where parent_id ='".$parent."' and combo_menu_id='".$_GET['item_id']."' and `_deleted`=0");
							}

								if($sqlCategory)
								{

								while($rowCategories = mysqli_fetch_assoc($sqlCategory)) {
									if($rowCategories['add_option']==1)
									{
										$level = 'option';
									}
									if($rowCategories['add_sub_option']==1)
									{
										$level = 'suboption';
									}

									$category_tree_array[] = array("id" => $rowCategories['id'], "parent_id" => $rowCategories['parent_id'], "menu_item_id" => $rowCategories['menu_item_id'], "level" => $level, "custom_name" => $rowCategories['custom_category_name'], "qty" => $rowCategories['qty'], "price_adjustment" => $rowCategories['price_adjustment'], "header" => $rowCategories['combo_header']);
									$category_tree_array = categoryParentChildTree($rowCategories['id'], '&nbsp;&nbsp;&nbsp;&nbsp;'.$spacing . '-&nbsp;', $category_tree_array);
								}

									return $category_tree_array;
								}
								else{
									return 0;
								}
					}

					function createtable($menu_name,$menu_id,$header_chk,$quantity,$optprice,$deleted_val,$pid,$optid,$suboptid,$type)
					{
						$seldisp = 'style="display:block;"';
						$pricedisp = 'style="display:block;"';
						$textdisp = 'style="display:none;"';
						$pricetd = '';
						$ortab = '';
						$suboptstyle = '';
						$subopttdstyle = '';
						$style = '';
						$disp = '';
						$chk = '';
						if($deleted_val=='' || $deleted_val==0)
						{
							$disp = '';
						}
						else
						{
							 $disp = 'display:none;';
						}

						if($type=='parent')
						{
							$tableid = 'mtable_'.$pid;
							$elemid = $pid;
							$optbtnid = 'm_option_'.$pid;
							$suboptbtnid = 'm_subopt_'.$pid;
							$optclik = 'onclick="add_more_opt(this.id,'.$pid.',\'mtable_'.$pid.'\','.$pid.',\'mtext_'.$pid.'\',1)"';
							$suboptclik = 'onclick="add_more_subopt(this.id,'.$pid.',\'mtable_'.$pid.'\','.$pid.',1)"';
							$delopt = 'maintab';

						}

						if($type=='option')
						{
							$tableid = 'mtable_'.$pid.'_opt_'.$optid;
							$elemid = $pid.'_opt_'.$optid;
							$optbtnid = 'm_'.$pid.'_opt_'.$optid;
							$suboptbtnid = 'm_'.$pid.'_subopt_'.$optid;
							$optclik = 'onclick="add_more_opt(this.id,'.$optid.',\'mtable_'.$pid.'_opt_'.$optid.'\','.$pid.',\'mtext_'.$pid.'\',\'\')"';
							$suboptclik = 'onclick="add_more_subopt(this.id,'.$optid.',\'mtable_'.$pid.'_opt_'.$optid.'\','.$pid.',1)"';
							$delopt = 'opttab';
							//$style= 'margin-left:10px;';
							$ortab = '<tr><td>OR</td></tr>';
						}
						if($type=='suboption')
						{
							$suboptstyle='style="display:none;"';
							$subopttdstyle = 'style="width:11.5%;"';
							if($optid==0)
							{
								$tableid = 'mtable_'.$pid.'_subopt_'.$suboptid;
								$style= 'margin-left:40px;';
								$elemid = $pid.'_subopt_'.$suboptid;
								$optbtnid = 'm_'.$pid.'_opt_subopt_'.$suboptid;
								$suboptbtnid = 'm_'.$pid.'_subopt_'.$suboptid;
								$optclik = 'onclick="add_more_subopt(this.id,'.$suboptid.',\'mtable_'.$pid.'_subopt_'.$suboptid.'\','.$pid.','.$suboptid.','.($suboptid+1).',\'subopt_opt\')"';
								$isuboptval = 'm_'.$pid.'_subopt_'.$suboptid;
							}
							else
							{
								$tableid = 'mtable_'.$pid.'_opt_'.$optid.'_subopt_'.$suboptid;
								$style= 'margin-left:40px;';
								$elemid = $pid.'_opt_'.$optid.'_subopt_'.$suboptid;
								$optbtnid = 'm_'.$pid.'_opt_'.$optid.'_subopt_'.$suboptid;
								$suboptbtnid = 'm_'.$pid.'_opt_'.$optid.'_subopt_'.$suboptid;
								$optclik = 'onclick="add_more_subopt(this.id,'.$suboptid.',\'mtable_'.$pid.'_opt_'.$optid.'_subopt_'.$suboptid.'\','.$pid.','.($suboptid+1).',\'\')"';
							}
							$suboptclik = 'onclick="alert(\'Maximum 3 level is supported\')"';
							$delopt = 'subtab';

						}

						$tableid = 'mtable_'.$elemid;
						$selid = 'm_'.$elemid.'_selmenu';
						$seltext = 'm_'.$elemid.'_seltext';
						$qty = 'm_'.$elemid.'_qty';
						$deleted = 'm_'.$elemid.'_deleted';
						$price = 'm_'.$elemid.'_price';
						$header = 'm_'.$elemid.'_header';
						if($header_chk=='on'){
						 $chk = 'checked';
						 $seldisp = 'style="display:none;"';
						 $textdisp = 'style="display:block;"';
						 $pricedisp = 'style="display:none;"';
						 $pricetd = 'style="width:7.5%;"';
						 }

						$chtm.='<table id="'.$tableid.'" style='.$style.$disp.'><tbody>'.$ortab.'<tr>';
						$chtm.='<td id="m_'.$elemid.'_td" '.$seldisp.' ><select style="width:262px;" class="js-example-data-array" id="'.$selid.'" name="'.$selid.'" onchange="get_menu_price(this.value,\''.$price.'\')"><option value="'.$menu_id.'" selected="selected">'.getmenunamebyid($menu_id).'</option></select></td>';
						$chtm.='<td id="m_'.$elemid.'_txt" '.$textdisp.' ><input style="width:220px;" type="text" id="'.$seltext.'" value="'.$menu_name.'" name="'.$seltext.'"></td>';
						$chtm.='<td><input id="'.$qty.'" name="'.$qty.'" value="'.$quantity.'" title="Quantity" type="text" size="3"></td>';
						$chtm.='<input id="'.$deleted.'" name="'.$deleted.'" value="'.$deleted_val.'" type="hidden">';
						$chtm.='<td> <input type="button" id="'.$optbtnid.'" name="'.$optbtnid.'" value="+ OPTION" '.$optclik.'> </td>';
						$chtm.='<td '.$subopttdstyle.'> <input '.$suboptstyle.' type="button" id="'.$suboptbtnid.'" name="'.$suboptbtnid.'" value="+ SUB OPTION" '.$suboptclik.' > </td>';
						$chtm.='<td '.$pricetd.'><input '.$pricedisp.' id="'.$price.'" name="'.$price.'" title="Price" value="'.$optprice.'" type="text" size="4"></td>';
						$chtm.='<td><a class="closeBtn" id="m_'.$elemid.'_closebtn" onclick="deloption(\''.$tableid.'\',\''.$delopt.'\')">X</a></td>';
						if($type=='suboption')
						{
							$chtm.='<td>&nbsp;</td>';
							$chtm.='<td>&nbsp;</td>';
						}
						else
						{
							$chtm.='<td>Header</td>';
							$chtm.='<td><input type="checkbox" id="'.$header.'" name="'.$header.'" '.$chk.' onclick="displayheader(this.checked,\'m_'.$elemid.'_td\')" > </td>';
						}
						$chtm.='</tr></tbody></table>';
						return $chtm;
					}

					function chkparentid($pid)
					{
						$sqlCategory = lavu_query("select parent_id from `combo_items` where id ='".$pid."'");
						$rowCategories = mysqli_fetch_assoc($sqlCategory);
						return $rowCategories['parent_id'];
					}
					function getmenunamebyid($mid)
					{
						$sqlCategory = lavu_query("select name from `menu_items` where id ='".$mid."'");
						$rowCategories = mysqli_fetch_assoc($sqlCategory);
						return $rowCategories['name'];
					}

					$pid=0;
					$optid=0;
					$suboptid=0;
					$hidarr = array();
					if(isset($_SESSION['poslavu_234347273_combo']) && array_key_exists($_GET['item_name'],$_SESSION['poslavu_234347273_combo']))
					{

						$json = $_SESSION['poslavu_234347273_combo'][$_GET['item_name']];

						$test = json_decode($json,1);
						$i=1;
						$chtm.='';
						foreach ($test as $key=>$data){
							$menu="m_".$i;
							$menu_id="m_".$i."_selmenu";
							$menu_name="m_".$i."_seltext";
							$menu_header="m_".$i."_header";
							$menu_qty="m_".$i."_qty";
							$menu_price="m_".$i."_price";
							$menu_deleted="m_".$i."_deleted";
							if(array_key_exists($menu_id,$test)){
								$pid = $pid + 1;
								$hidarr[$i] = 0;
								$chtm.= createtable($test[$menu_name],$test[$menu_id],$test[$menu_header],$test[$menu_qty],$test[$menu_price],$test[$menu_deleted],$i,0,0,'parent');
								for($j=1; $j<30; $j++ ){
									$menu_subopt=$menu."_subopt_".$j;
									$menu_subopt_id=$menu_subopt."_selmenu";
									$menu_subopt_name=$menu_subopt."_seltext";
									$menu_subopt_header=$menu_subopt."_header";
									$menu_subopt_qty=$menu_subopt."_qty";
									$menu_subopt_price=$menu_subopt."_price";
									$menu_subopt_deleted=$menu_subopt."_deleted";

									if(array_key_exists($menu_subopt_id,$test)){
										$chtm.= createtable($test[$menu_subopt_name],$test[$menu_subopt_id],$test[$menu_subopt_header],$test[$menu_subopt_qty],$test[$menu_subopt_price],$test[$menu_subopt_deleted],$i,0,$j,'suboption');
									}else { break; }

								}
								}else { break; }
								for($j=1; $j<30; $j++ ){
									$menu_opt=$menu."_opt_".$j;
									$menu_opt_id=$menu_opt."_selmenu";
									$menu_opt_name=$menu_opt."_seltext";
									$menu_opt_header=$menu_opt."_header";
									$menu_opt_qty=$menu_opt."_qty";
									$menu_opt_price=$menu_opt."_price";
									$menu_opt_deleted=$menu_opt."_deleted";

									if(array_key_exists($menu_opt_id,$test)){
										$optid = $optid + $j;
										$hidarr[$i] = $j;
										$chtm.= createtable($test[$menu_opt_name],$test[$menu_opt_id],$test[$menu_opt_header],$test[$menu_opt_qty],$test[$menu_opt_price],$test[$menu_opt_deleted],$i,$j,0,'option');
										for($k=1; $k<30; $k++ ){
											$menu_opt_subopt=$menu_opt."_subopt_".$k;
											$menu_opt_subopt_id=$menu_opt_subopt."_selmenu";
											$menu_opt_subopt_name=$menu_opt_subopt."_seltext";
											$menu_opt_subopt_header=$menu_opt_subopt."_header";
											$menu_opt_subopt_qty=$menu_opt_subopt."_qty";
											$menu_opt_subopt_price=$menu_opt_subopt."_price";
											$menu_opt_subopt_deleted=$menu_opt_subopt."_deleted";

											if(array_key_exists($menu_opt_subopt_id,$test)){
												$chtm.= createtable($test[$menu_opt_subopt_name],$test[$menu_opt_subopt_id],$test[$menu_opt_subopt_header],$test[$menu_opt_subopt_qty],$test[$menu_opt_subopt_price],$test[$menu_opt_subopt_deleted],$i,$j,$k,'suboption');

											}else { break; }

										}

									}else { break; }
								}

							$i++;
						}

						//$pid = $pid-1;

					}
					else
					{
						$categoryList = categoryParentChildTree();

						$newCategoryarr = array();
						foreach($categoryList as $k=>$v)
						{
							if($v['parent_id'] == 0)
							{
								$newCategoryarr[] = $v;
								foreach($categoryList as $k1=>$v1)
								{
									if(@$v['id'] == $v1['parent_id'] &&  $v1['level'] == 'suboption')
									{
										$newCategoryarr[] = $v1;
									}
								}
								foreach($categoryList as $k2=>$v2)
								{
									if(@$v['id'] == $v2['parent_id'] &&  $v2['level'] == 'option')
									{
										$newCategoryarr[] = $v2;
										foreach($categoryList as $k3=>$v3)
										{
											if(@$v2['id'] == $v3['parent_id'] &&  $v3['level'] == 'suboption')
											{
												$newCategoryarr[] = $v3;
											}
										}
									}
								}
							}
						}


						if($newCategoryarr!=0)
						{

							$tableid = '';

							$chtm = '';
							$subparent_id = 0;
							$optparent_id = 0;

							foreach($newCategoryarr as $key => $value){
								$seldisp = 'style="display:block;"';
								$textdisp = 'style="display:none;"';
								$pricedisp = 'style="display:block;"';
								$ortab = '';
								$suboptstyle='';
								$subopttdstyle= '';
								$pricetd = '';
								$chk = '';
								$style = '';
								if($value['parent_id']==0)
								{
									if($optparent_id==$value['parent_id'])
									{
										$pid++;
									}
									else{
										$optparent_id = $value['parent_id'];
										$suboptid = 0;
										$pid++;
									}
									$tableid = 'mtable_'.$pid;
									$elemid = $pid;
									$optbtnid = 'm_option_'.$pid;
									$suboptbtnid = 'm_subopt_'.$pid;
									$optclik = 'onclick="add_more_opt(this.id,'.$pid.',\'mtable_'.$pid.'\','.$pid.',\'mtext_'.$pid.'\',1)"';
									$suboptclik = 'onclick="add_more_subopt(this.id,'.$pid.',\'mtable_'.$pid.'\','.$pid.',1)"';
									$hidarr[$pid] = 0;
									$delopt = 'maintab';
								}

								if($value['parent_id']!=0 && $value['level']=='option')
								{
									if($npid == $pid)
									{

										$optid++;
									}
									else
									{


										$npid = $pid;
										$optid = 0;
										$optid++;
									}
									$hidarr[$pid] = $optid;

									$tableid = 'mtable_'.$pid.'_opt_'.$optid;
									$elemid = $pid.'_opt_'.$optid;
									$optbtnid = 'm_'.$pid.'_opt_'.$optid;
									$suboptbtnid = 'm_'.$pid.'_subopt_'.$optid;
									$optclik = 'onclick="add_more_opt(this.id,'.$optid.',\'mtable_'.$pid.'_opt_'.$optid.'\','.$pid.',\'mtext_'.$pid.'\',\'\')"';
									$suboptclik = 'onclick="add_more_subopt(this.id,'.$optid.',\'mtable_'.$pid.'_opt_'.$optid.'\','.$pid.',1)"';
									$delopt = 'opttab';
									//$style = 'style="margin-left:10px;"';
									$ortab = '<tr><td>OR</td></tr>';
								}
								if($value['parent_id']!=0 && $value['level']=='suboption')
								{
									$chkparent = chkparentid($value['parent_id']);
									$suboptstyle = 'style="display:none;"';
									$subopttdstyle = 'style="width:11.5%;"';
									if($subparent_id==$value['parent_id'])
									{
										$suboptid++;
									}
									else{
										$subparent_id = $value['parent_id'];
										$suboptid = 0;
										$suboptid++;
									}

									if($chkparent==0)
									{
										$tableid = 'mtable_'.$pid.'_subopt_'.$suboptid;
										$style = 'style="margin-left:40px;"';
										$elemid = $pid.'_subopt_'.$suboptid;
										$optbtnid = 'm_'.$pid.'_opt_subopt_'.$suboptid;
										$suboptbtnid = 'm_'.$pid.'_subopt_'.$suboptid;
										$optclik = 'onclick="add_more_subopt(this.id,'.$suboptid.',\'mtable_'.$pid.'_subopt_'.$suboptid.'\','.$pid.','.$suboptid.','.($suboptid+1).',\'subopt_opt\')"';
										$isuboptval = 'm_'.$pid.'_subopt_'.$suboptid;
									}
									else
									{
										$tableid = 'mtable_'.$pid.'_opt_'.$optid.'_subopt_'.$suboptid;
										$style = 'style="margin-left:40px;"';
										$elemid = $pid.'_opt_'.$optid.'_subopt_'.$suboptid;
										$optbtnid = 'm_'.$pid.'_opt_'.$optid.'_subopt_'.$suboptid;
										$suboptbtnid = 'm_'.$pid.'_opt_'.$optid.'_subopt_'.$suboptid;
										$optclik = 'onclick="add_more_subopt(this.id,'.$suboptid.',\'mtable_'.$pid.'_opt_'.$optid.'_subopt_'.$suboptid.'\','.$pid.','.($suboptid+1).',\'\')"';
									}
									$suboptclik = 'onclick="alert(\'Maximum 3 level is supported\')"';
									$delopt = 'subtab';

								}

								$tableid = 'mtable_'.$elemid;
								$selid = 'm_'.$elemid.'_selmenu';
								$seltext = 'm_'.$elemid.'_seltext';
								$qty = 'm_'.$elemid.'_qty';
								$deleted = 'm_'.$elemid.'_deleted';
								$price = 'm_'.$elemid.'_price';
								$header = 'm_'.$elemid.'_header';
								$sel_opt = '<option value="'.$value['menu_item_id'].'" selected="selected">'.getmenunamebyid($value['menu_item_id']).'</option>';
								if($value['header']==1){
									$chk = 'checked';
									$seldisp = 'style="display:none;"';
									$textdisp = 'style="display:block;"';
									$pricedisp = 'style="display:none;"';
									$pricetd = 'style="width:7.5%;"';
									$sel_opt = '';
								}

								$chtm.='<table id="'.$tableid.'" '.$style.'><tbody>'.$ortab.'<tr>';
								$chtm.='<td id="m_'.$elemid.'_td" '.$seldisp.' ><select style="width:262px;" class="js-example-data-array" id="'.$selid.'" name="'.$selid.'" onchange="get_menu_price(this.value,\''.$price.'\')">'.$sel_opt.'</select></td>';
								$chtm.='<td id="m_'.$elemid.'_txt" '.$textdisp.' ><input style="width:220px;" type="text" id="'.$seltext.'" value="'.$value['custom_name'].'" name="'.$seltext.'"></td>';
								$chtm.='<td><input id="'.$qty.'" name="'.$qty.'" value="'.$value['qty'].'" title="Quantity" type="text" size="3"></td>';
								$chtm.='<input id="'.$deleted.'" name="'.$deleted.'" value="0" type="hidden">';
								$chtm.='<td> <input type="button" id="'.$optbtnid.'" name="'.$optbtnid.'" value="+ OPTION" '.$optclik.'> </td>';
								$chtm.='<td '.$subopttdstyle.'> <input '.$suboptstyle.' type="button" id="'.$suboptbtnid.'" name="'.$suboptbtnid.'" value="+ SUB OPTION" '.$suboptclik.' > </td>';
								$chtm.='<td '.$pricetd.'><input '.$pricedisp.' id="'.$price.'" name="'.$price.'" title="Price" value="'.$value['price_adjustment'].'" type="text" size="4"></td>';
								$chtm.='<td><a class="closeBtn" id="m_'.$elemid.'_closebtn" onclick="deloption(\''.$tableid.'\',\''.$delopt.'\')">X</a></td>';
								if($value['level']=='suboption')
								{
									$chtm.='<td>&nbsp;</td>';
									$chtm.='<td>&nbsp;</td>';
								}
								else
								{
									$chtm.='<td>Combo Header</td>';
									$chtm.='<td><input type="checkbox" id="'.$values['header'].'" name="'.$header.'" '.$chk.' onclick="displayheader(this.checked,\'m_'.$elemid.'_td\')" > </td>';
								}
								$chtm.='</tr></tbody></table>';
							}
						}
					}
					foreach($hidarr as $arrkey=>$hidfield)
					{
						$toprint.="<input type='hidden' id='mtext_".$arrkey."' value='".$hidfield."' >";
					}

					$toprint .="<tr><td colspan='7' align='right'><input type='checkbox' id='import_price' onclick='import_item_price()' >Import Menu Prices</td></tr>";
					$toprint .= "<tr><td style='font-size:10px;width:216px;'>Menu Item / Header</td><td style='font-size:10px;width:35px;'>Qty</td><td style='font-size:10px;width:86px'>+ Option</td> <td style='font-size:10px;width:110px'> + Sub Option </td> <td style='font-size:10px;width:14%'> Adjust Price</td><td style='font-size:10px;'>Make Header</td></tr>
					";
					$toprint .= "
							</table><div id=\"comboitems\">".$chtm."</div>
						 <input type=\"hidden\" id=\"ival\" value=\"".($pid+1)."\" >
				         <input type=\"hidden\" id=\"itable\" value=\"".($pid+1)."\" />
				         
				         <input type=\"hidden\" id=\"ioptval\" value=\"\" />
				         <input type=\"hidden\" id=\"ioptcount\" value=\"".$optid."\" />
				         
				          <input type=\"hidden\" id=\"isuboptval\" value=\"".$isuboptval."\" />
				         
						 <input type=\"hidden\" name=\"main_menu_id\" value='".$_GET['item_id']."'/>
						 <input type=\"hidden\" name=\"temp_id\" value='".$_GET['item_name']."'/>
						 		
				         <input type=\"button\" id=\"addmorebtn\" value=\"Add More >>\" onclick=\"add_more_comboitems('parent',this.id)\">
								
					";
				}
				else if($dtype=="extra")
				{
					$get_js_code = "d_extra_values = window.parent.document.getElementById('".str_replace("details",$dname,$item_name)."').value; ";
					$get_js_code .= "function read_extra_value(dext,edname) { ";
					$get_js_code .= " dext = dext.split('[[' + edname + ']]'); ";
					$get_js_code .= " if(dext.length > 1) { ";
					$get_js_code .= "   dext = dext[1]; ";
					$get_js_code .= "   dext = dext.split('{{'); ";
					$get_js_code .= "   if(dext.length > 1) { ";
					$get_js_code .= "     dext = dext[1]; ";
					$get_js_code .= "     dext = dext.split('}}'); ";
					$get_js_code .= "     return dext[0]; ";
					$get_js_code .= "   } ";
					$get_js_code .= " } ";
					$get_js_code .= " return ''; ";
					$get_js_code .= "} ";

					$set_js_code = "ds_extra_values = ''; ";

					for($n=0; $n<count($efields); $n++)
					{
						$efield = trim($efields[$n]);
						if($efield!="")
						{
							$edname = $dname . "_" . $n;
							$efield_ins = str_replace('"','&quot;',$efield);
							$fieldval = get_multi_field_value($efield, $dval,"");
							$fieldval = str_replace('"','&quot;',$fieldval);
							$toprint .= drow_code($efield,"<input type='text' name='$edname' value=\"$fieldval\">");
							$get_js_code .= "document.dform.$edname.value = read_extra_value(d_extra_values,\"$efield_ins\"); ";
							$set_js_code .= "ds_extra_values += \"[[$efield_ins]]{{\" + document.dform.$edname.value + \"}}\"; ";
						}
					}
					$set_js_code .= "window.parent.document.getElementById('".str_replace("details",$dname,$item_name)."').value = ds_extra_values; ";
				}

				$js_getvals .= $get_js_code;
				$js_setvals .= $set_js_code;
				$toprint .= "</td>";
				$toprint .= "</tr>";
			}
			if($item_title=="Combo Items"){
				$toprint .= drow_code("","<br><br><input type='button' value='Apply' onclick='apply_detail_comboupdate();'>");
			}else{
				$toprint .= drow_code("","<input type='button' value='Apply' onclick='apply_detail_update(); window.parent.info_visibility()'>");
			}
			$srclink = '';
			$select2 ='';
			if ($dtype == "comboitems"){
				$srclink="<link href='/cp/css/select2-4.0.3.min.css' rel='stylesheet' /><script src='/cp/scripts/select2-4.0.3.min.js'></script>";

				$select2="
						$( document ).ready(function() {
							selectauto();
						});
						function selectauto()
						{
							var data = $menu_json;
						    $(\".js-example-data-array\").select2({
	  						   data: data
							})
						}
						
						function get_menu_price(menu_id,price_id)
						{
							var  frmData = 'menu_id='+menu_id+'&type=individual';
							if(document.getElementById('import_price').checked==true)
							{
								var Json = ".$item_price_json.";
								 for (var i = 0; i < Json.length; i++) {
							        var element = Json[i];
							        if (element.id == menu_id) {
									   document.getElementById(price_id).value = element.item_price;
							       }
							    }
							}		
							
						}
				";

			}
			$toprint .= "
				</table>
			</form>
			
					$srclink
			<script type='text/javascript'>
				
					$select2
				function carryValueOrSetDefault(dname, element, cval) {

					if (element.nodeName == 'SELECT') {
						var defv = '';
						var has_cval = false;
						for (var i = 0; i < element.length; i++) {
							var optv = element.options[i].value;
							if (i == 0) defv = optv;
							if (optv == cval) {
								has_cval = true;
								break;
							}
						}
						if (!has_cval) {
							if (dname == 'printer') {
								var opt_text = 'kitchen';
								if (cval > 1) opt_text = 'kitchen' + cval;
								var new_opt = document.createElement(\"option\");
								new_opt.text = opt_text + ' (undefined)';
								new_opt.value = cval;
								element.add(new_opt);
							} else cval = defv;
						}
					}

					element.value = cval;
				}

				".$js_getvals."

				function apply_detail_update() {
					".$js_setvals."
                    var ingredients_itemnum;
                    if(ingredients_itemnum<1){
                     alert('At least one ingredient should required.');
                     window.parent.info_visibility();
                     return false;       
                  }
				}
				function apply_detail_comboupdate(){
				
					$.ajax({
							type: 'POST',  // http method
					   url: 'add_combo.php',
					   data:  $('#dForm').serialize(),
						  error: function() {
					      //alert('ffalse');
					   },
					   success: function(data) {
						if(data=='error')
							{
								alert('Combo header should have sub options');
							    return false;
							}
						else if(data=='empty')
							{
								alert('Menu item / Header should not be empty');
							    return false;
							}
                         else if(data=='NoItem'){
                             alert('At least one combo item should required.');
                              return false;
                            }
							else{
							window.parent.info_visibility()
							}
					     console.log(data);
					   }
					   
							
					});	
				}
				
				function import_item_price()
				{
						if(document.getElementById('import_price').checked==true)
						{
						   $.ajax({
						   type: 'POST',  // http method
						   url: 'import_item_price_combo.php',
						   data:  $('#dForm').serialize(),
							  error: function() {
						      //alert('ffalse');
						   },
						   success: function(data) {
							if(data!='0')
							{
								var Json = data;
								var result = $.parseJSON(Json);
								$.each(result, function(k, v) {
									document.getElementById(k).value = v;
								});
							}
						   }
						   
								
						});
					}
				}
			</script>";
		}
	}
	else $toprint .= "Invalid mode: $mode";

	 //alert(\"".$dname." \" + setval ); document.dform.$dname.value = setval;

	$toprint .= "</body>";

	//$toprint = str_replace(">", ">\n", $toprint);
	echo $toprint;
	echo isset($ifNotPassbook_js) ? $ifNotPassbook_js : "";

	function set_inventory_picture_tohtml($extra_apply_code, $fwd_picture_vars, $tab_text, $extra_onchange_code, &$piclist, $update_message, $selected_picture, $updateid="",$category_name="") {

		$toprint = '';

		$picmode = (isset($_GET['picmode']))?$_GET['picmode']:"main";

		$toprint .= "<form name='upload' enctype='multipart/form-data' method='post' action='form_dialog.php?mode=set_inventory_picture&updateid=$updateid".$fwd_picture_vars."'>";

		$toprint .= $tab_text;

		$toprint .= "<br><br><br>";
		// We need to keep #picture_controls
		$toprint .= "<div id='picture_controls'>";
		$toprint .= "<table style='border:solid 1px black' cellpadding=12><td>";
		$toprint .= "<select id='file_selector' onchange='window.location = \"form_dialog.php?mode=set_inventory_picture&updateid=$updateid&tab_selected=\" + escape(document.getElementById(\"tab_selected\").value) + \"&setfile=\" + escape(this.value) + \"".$fwd_picture_vars.$extra_onchange_code."\"'>";
		$toprint .= "<option value='none'></option>";
		for($x=0; $x<count($piclist); $x++)
		{
			$fname = $piclist[$x];
			$toprint .= "<option value='$fname'";
			if($fname==$selected_picture) $toprint .= " selected";
			$toprint .= ">$fname</option>";
		}
		$toprint .= "</select>";
		$toprint .= "<br><br>";
		if(isset($update_message) && $update_message!="") $toprint .= $update_message;
		$toprint .= "Upload: <input type='file' name='picture' onChange='document.upload.submit()' style='color:#bbbbbb'>";
		$toprint .= "</td></table>";
		$toprint .= "</div>";
		// end of #picture_controls

        $fwd_pic_url = "form_dialog.php?mode=set_inventory_picture&updateid=$updateid".$fwd_picture_vars.$extra_onchange_code;
        $toprint .= "<input type='button' value='Make your Own' onclick='window.location = \"$fwd_pic_url&picmode=generate\"' />";

		$toprint .= "</form>";

		ob_start();
		include 'thumbnail-generator/set-inventory-picture.php';
		$toprint = ob_get_clean();

		if($picmode=="generate")
		{
		    // Insert thumbnail generator code HERE!!
		    $thumbnail_code_path = "thumbnail-generator/thumbnail-generator.php";
		    ob_start();
		    include $thumbnail_code_path;
		    $thumbnail_generator_html = ob_get_clean();
            //comment this line below to make the thumbnail generator go away
			$toprint = $thumbnail_generator_html;

			$create_image_folder = $_SERVER['DOCUMENT_ROOT'] . $_GET['dir'];
// 			$toprint .= "<br><br>Folder: $create_image_folder";
			$item_name_id = str_replace("image","name",$_GET['updateid']);
            $toprint .= <<<JS
<script language='javascript'>
   var item_name = window.parent.document.getElementById('$item_name_id'.replace( '_col_name', '_category' ) ).value;
</script>
JS;
		}
		else if( $picmode=="finish_thumbnail" )
		{

		    include "thumbnail-generator/image-processor.php" ;
			if( $GLOBALS["thumbnail-success"] ) {
			    $toprint = <<<HTML
  <div id="thumbnail-success-message">
     <h2 style="text-align:center;color:gray;">Thumbnail created!</h2>
  </div>
<script>
	window.parent.set_inv_picture( "{$_GET['updateid']}", "{$GLOBALS['new_filename']}", "{$_GET['dir']}{$GLOBALS['new_filename']}");
</script>
HTML;
			}
		}

		return $toprint;
	}

	function set_inventory_picture_grid_tohtml($s_picpath, &$a_piclist, $fwd_picture_vars, $selected_picture, $updateid, $extra_apply_code) {

		$s_retval = "";

		// draw some scripts
		$s_retval .= "
			<script type='text/javascript' src='https://ajax.googleapis.com/ajax/libs/jquery/1.6.2/jquery.min.js'></script>
			<script type='text/javascript'>
				setTimeout('createFunctions();', 500);
				setTimeout('createFunctions();', 1000);
				setTimeout('createFunctions();', 2000);
				setTimeout('createFunctions();', 4000);
				setTimeout('createFunctions();', 8000);

				function createFunctions() {
					window.getSelectedName = function() {
						var jdiv = $('div.pic_div.pic_selected');
						var name = jdiv.find('input[name=name]').val();
						return name;
					}

					window.selectAppliedPicture = function(element) {
						var jdiv = $(element);
						var jselected = $('div.pic_div.pic_selected');
						var jname = $('#pic_selected_name_display');
						jselected.removeClass('pic_selected');
						jdiv.addClass('pic_selected');
						jname.html(getSelectedName());
						setTimeout('applyPicture();', 100);
					}

					window.applyPicture = function() {
						window.parent.set_inv_picture('{$updateid}',getSelectedName(),'{$s_picpath}'+getSelectedName()+'{$extra_apply_code}');
					}
				}
			</script>";

		// draw the styling
		$s_retval .= "
			<style type='text/css'>
				.picture_div {
					max-width:120px;
					max-height:60px;
					position:relative;
					top:-60px;
				}
				div.pic_div {
					width:120px;
					height:60px;
					padding:10px;
					border:0px solid none;
					display:inline-block;
					text-align:center;
				}
				div.pic_div.pic_selected {
					padding:8px;
					border:2px solid white;
					background-color:rgba(255,255,255,0.25);
				}
				img.pizza_background {
					width:120px;
					height:60px;
					padding:0;
					margin:0 auto;
					border:0px solid none;
					display:block;
				}
			</style>";

		// draw the apply button
		if (FALSE)
		$s_retval .= "
			<input type='button' value='Apply' onclick='apply_picture();' />
			<br />";

		// draw the grid of pictures
		$s_retval .= "<div id='picture_chooser'>";
		foreach($a_piclist as $s_picname) {
			if (strpos($s_picname, 'top_') !== 0)
				continue;
			$selected = ($s_picname == $selected_picture) ? ' pic_selected' : '';
			$s_retval .= "
				<div class='pic_div {$selected}' onclick='selectAppliedPicture(this);'>
					<img class='pizza_background' src='/components/pizza/images/pizza_sizer_sauce_cheese.png' />
					<img class='picture_div' src='{$s_picpath}{$s_picname}' />
					<input type='hidden' name='name' value='{$s_picname}' />
				</div>";
		}
		$s_retval .= "
			<div id='pic_selected_name_display' style='display:none;'>{$selected_picture}</div>
			</div>";

		// draw the upload dialog
		if (FALSE)
		$s_retval .= "
			<br />
			<form name='upload' enctype='multipart/form-data' method='post' action='form_dialog.php?mode=set_inventory_picture&updateid=$updateid".$fwd_picture_vars."'>
				Upload: <input type='file' name='picture' onChange='document.upload.submit()' style='color:#bbbbbb'>
			</form>";

		// sanitize and return
		$s_retval = str_replace(array("\n", "\r", "\n\r"), "\n", $s_retval);
		return $s_retval;
	}
	
	//olo banner image LP-1402..
	function olo_set_inventory_picture_tohtml($extra_apply_code, $fwd_picture_vars, $tab_text, $extra_onchange_code, &$piclist, $update_message_olo, $selected_picture, $updateid="",$category_name="") {
	    
	    $pos = strpos($update_message_olo,"success");
	    $olo_banner="";
	    $olo_banner_status="";
	    
	    if($pos!==false){
	        $olo_banner_status="success";
	        
	        $protocal="https:/";
	        if(isset($_SERVER['HTTPS']))
	        {
	            $protocal="https:/";
	        }
	        else
	        {
	            $protocal="http:/";
	        }
	        
	        $olo_banner_loc =strstr($update_message_olo,'images');
	        
	        $currentPath = $_SERVER['HTTP_HOST'];
	        $olo_banner=$protocal.$currentPath."/".$olo_banner_loc;
	        
	    }
	    
	    
	    
	    $toprint = '';
	    $picmode = (isset($_GET['picmode']))?$_GET['picmode']:"main";
	    
	    $toprint .= "<form name='upload' enctype='multipart/form-data' method='post' action='form_dialog.php?mode=set_inventory_picture&updateid=$updateid".$fwd_picture_vars."'>";
	    $toprint .= $tab_text;
	    
	    $toprint .= "<br><br><br>";
	    // We need to keep #picture_controls
	    $toprint .= "<div id='picture_controls'>";
	    $toprint .= "<table style='border:solid 1px black' cellpadding=12><td>";
	    $toprint .= "<select id='file_selector' onchange='window.location = \"form_dialog.php?mode=set_inventory_picture&updateid=$updateid&tab_selected=\" + escape(document.getElementById(\"tab_selected\").value) + \"&setfile=\" + escape(this.value) + \"".$fwd_picture_vars.$extra_onchange_code."\"'>";
	    $toprint .= "<option value='none'></option>";
	    for($x=0; $x<count($piclist); $x++)
	    {
	        $fname = $piclist[$x];
	        $toprint .= "<option value='$fname'";
	        if($fname==$selected_picture) $toprint .= " selected";
	        $toprint .= ">$fname</option>";
	    }
	    $toprint .= "</select>";
	    $toprint .= "<br><br>";
	    //echo "Upload a new Picture:";
	    if(isset($update_message) && $update_message!="") $toprint .= $update_message;
	    $toprint .= "Upload: <input type='file' name='picture' onChange='document.upload.submit()' style='color:#bbbbbb'>";
	    $toprint .= "</td></table>";
	    $toprint .= "</div>";
	    // end of #picture_controls
	    
	    $fwd_pic_url = "form_dialog.php?mode=set_inventory_picture&updateid=$updateid".$fwd_picture_vars.$extra_onchange_code;
	    $toprint .= "<input type='button' value='Make your Own' onclick='window.location = \"$fwd_pic_url&picmode=generate\"' />";
	    
	    $toprint .= "</form>";
	    $toprint.="";
	    $toprint .= "<form name='upload' enctype='multipart/form-data' method='post' action='form_dialog.php?mode=olo_set_inventory_picture&category_name=$category_name&updateid=$updateid".$fwd_picture_vars."'>";
	    $toprint .= "<div id='olo_picture_controls'>";
	    if(isset($update_message_olo) && $update_message_olo!="") $toprint .= $update_message_olo;
	    $toprint .= "Upload: <input type='file' name='olo_picture' onChange='document.upload.submit()' style='color:#bbbbbb'>";
	    
	    
	    
	    $toprint .= "</div>";
	    
	    $toprint .= "</form>";
	    
	    ob_start();
	    include 'thumbnail-generator/set-inventory-picture.php';
	    $toprint = ob_get_clean();
	    if($picmode=="generate")
	    {
	        // Insert thumbnail generator code HERE!!
	        $thumbnail_code_path = "thumbnail-generator/thumbnail-generator.php";
	        ob_start();
	        include $thumbnail_code_path;
	        $thumbnail_generator_html = ob_get_clean();
	        //comment this line below to make the thumbnail generator go away
	        $toprint = $thumbnail_generator_html;
	        
	        $create_image_folder = $_SERVER['DOCUMENT_ROOT'] . $_GET['dir'];
	        $item_name_id = str_replace("image","name",$_GET['updateid']);
	        $toprint .= <<<JS
<script language='javascript'>
   var item_name = window.parent.document.getElementById('$item_name_id'.replace( '_col_name', '_category' ) ).value
</script>
JS;
	    }
	    else if( $picmode=="finish_thumbnail" )
	    {
	        
	        include "thumbnail-generator/image-processor.php" ;
	        if( $GLOBALS["thumbnail-success"] ) {
	            $toprint = <<<HTML
  <div id="thumbnail-success-message">
     <h2 style="text-align:center;color:gray;">Thumbnail created!</h2>
  </div>
<script>
	window.parent.set_inv_picture( "{$_GET['updateid']}", "{$GLOBALS['new_filename']}", "{$_GET['dir']}{$GLOBALS['new_filename']}");
</script>
HTML;
	        }
	    }
	    if($olo_banner_status=='success'){
	        $toprint .= <<<HTML
<script>
document.getElementById("olo-picture_controls").innerHTML = '<a target="_blank" href="$olo_banner" style="color:#fff">Click here for olo banner</a>';
</script>
HTML;
	    }
	    else{
	        $toprint .= <<<HTML
<script>
document.getElementById("olo-picture_controls").innerHTML ='$update_message_olo';
</script>
HTML;
	        
	        
	    }
	    return $toprint;
	}
	
	function olo_set_inventory_picture_grid_tohtml($s_picpath, &$a_piclist, $fwd_picture_vars, $selected_picture, $updateid, $extra_apply_code) {
	    $s_retval = "";
	    // draw some scripts
	    $s_retval .= "
                        <script type='text/javascript' src='https://ajax.googleapis.com/ajax/libs/jquery/1.6.2/jquery.min.js'></script>
                        <script type='text/javascript'>
                                setTimeout('createFunctions();', 500);
                                setTimeout('createFunctions();', 1000);
                                setTimeout('createFunctions();', 2000);
                                setTimeout('createFunctions();', 4000);
                                setTimeout('createFunctions();', 8000);
                                
                                function createFunctions() {
                                        window.getSelectedName = function() {
                                                var jdiv = $('div.pic_div.pic_selected');
                                                var name = jdiv.find('input[name=name]').val();
                                                return name;
                                        }
                                        
                                        window.selectAppliedPicture = function(element) {
                                                var jdiv = $(element);
                                                var jselected = $('div.pic_div.pic_selected');
                                                var jname = $('#pic_selected_name_display');
                                                jselected.removeClass('pic_selected');
                                                jdiv.addClass('pic_selected');
                                                jname.html(getSelectedName());
												setTimeout('applyPicture();', 100);
                                                 }
                                                 
                                        window.applyPicture = function() {
                                                window.parent.set_inv_picture('{$updateid}',getSelectedName(),'{$s_picpath}'+getSelectedName()+'{$extra_apply_code}');
                                        }
                                }
                        </script>";
	    
	    // draw the styling
	    $s_retval .= "
                        <style type='text/css'>
                                .picture_div {
                                        max-width:120px;
                                        max-height:60px;
                                        position:relative;
                                        top:-60px;
                                }
                                div.pic_div {
                                        width:120px;
                                        height:60px;
                                        padding:10px;
                                        border:0px solid none;
                                        display:inline-block;
                                        text-align:center;
                                }
                                div.pic_div.pic_selected {
                                        padding:8px;
                                        border:2px solid white;
                                        background-color:rgba(255,255,255,0.25);
                                }
                                img.pizza_background {
                                        width:120px;
                                        height:60px;
                                        padding:0;
                                        margin:0 auto;
                                        border:0px solid none;
                                        display:block;
                                }
                        </style>";
	    
	    // draw the apply button
	    if (FALSE)
	        $s_retval .= " <input type='button' value='Apply' onclick='apply_picture();' />
                        <br />";
	        // draw the grid of pictures
	        $s_retval .= "<div id='picture_chooser'>";
	        foreach($a_piclist as $s_picname) {
	            if (strpos($s_picname, 'top_') !== 0)
	                continue;
	                $selected = ($s_picname == $selected_picture) ? ' pic_selected' : '';
	                $s_retval .= "
                                <div class='pic_div {$selected}' onclick='selectAppliedPicture(this);'>
                                        <img class='pizza_background' src='/components/pizza/images/pizza_sizer_sauce_cheese.png' />
                                        <img class='picture_div' src='{$s_picpath}{$s_picname}' />
                                        <input type='hidden' name='name' value='{$s_picname}' />
                                </div>";
	        }
	        $s_retval .= "
                        <div id='pic_selected_name_display' style='display:none;'>{$selected_picture}</div>
                        </div>";
	        
	        // draw the upload dialog
	        if (FALSE)
	            $s_retval .= "
                        <br />
                        <form name='upload' enctype='multipart/form-data' method='post' action='form_dialog.php?mode=set_inventory_picture&updateid=$updateid".$fwd_picture_vars."'>
                                Upload: <input type='file' name='picture' onChange='document.upload.submit()' style='color:#bbbbbb'>
                        </form>";
	            
	            // sanitize and return
	            $s_retval = str_replace(array("\n", "\r", "\n\r"), "\n", $s_retval);
	            return $s_retval;
	}
	//end of LP-1402.
	

?>