<?php
	if(isset($using_manage) && $using_manage==1){
		ini_set('display_errors',1);
		require_once($_SERVER['DOCUMENT_ROOT']."/cp/resources/json.php");
		require_once($_SERVER['DOCUMENT_ROOT']."/cp/resources/core_functions.php");
	}
	if($in_lavu)
	{
		if (!function_exists('create_help_text_for_browse_element')) {
			function create_help_text_for_browse_element($field, $s_table) {

				// get some globals
				global $s_help_text_prepend_settingname;

				// set the help text variables
				$s_filename = '';
				$s_tablename = '';
				$s_settingname = '';
				assign_file_table_setting_names($field->name, debug_backtrace(), $s_filename, $s_tablename, $s_settingname);
				$s_tablename = $s_table;

				// set the help text style
				$a_style = array(
					'position'=>'relative',
					'left'=>'5px',
					'display'=>'inline-block'
				);

				// create the style string
				$s_style = '';
				foreach($a_style as $k=>$v)
					$s_style .= "{$k}:{$v};";

				// check for a global prepend
				if (isset($s_help_text_prepend_settingname))
					$s_settingname = $s_help_text_prepend_settingname.$s_settingname;

				// create the help text
				$s_helptext = "";
				if ($field->type !== 'submit' && $s_settingname !== '' && $s_tablename !== '' && $s_filename !== '') {
					$s_helptext = create_help_mark($s_filename, $s_tablename, $s_settingname);
					$s_helptext = "<div style='{$s_style}'>{$s_helptext}</div>";
				}
				return $s_helptext;
			}
		}

		$rowid = urlvar("rowid");
		$addnew = urlvar("addnew");
		$removeid = urlvar("removeid");

		if(!isset($search_mode))
			$search_mode = "";
		if(!isset($search_terms))
			$search_terms = array();
		if(!isset($filter_by))
			$filter_by = "";
		if(!isset($option_links))
			$option_links = "";
		if(!isset($per_page))
			$per_page = 0;

		$search_page = urlvar("page");
		if (!$search_page && sessvar_isset("search_page"))
			$search_page = sessvar("search_page");
		else if (!$search_page)
			$search_page = 1;
		if ($search_page < 1) $search_page = 1;
			set_sessvar("search_page", $search_page);

		$keep_search = urlvar("ks");

		if($removeid)
		{
			$set_deleted_date = "";
			if ($tablename=="users") $set_deleted_date = ", `deleted_date` = now()";
			lavu_query("UPDATE `$tablename` SET `_deleted` = '1'$set_deleted_date WHERE `id` = '[1]'", $removeid);

			if (in_array($tablename, array("users", "messages", "super_groups", "lk_events", "lk_tracks", "lk_karts", "lk_event_types", "config", "locations", "lk_sales_reps", "lk_rooms", "lk_event_sequences", "lk_waivers", "meal_periods", "meal_period_types", "happyhours", "lk_parts", "lk_service_types", "lk_service_info", "discount_types")) || (isset($sync_to_lls_on_delete) && $sync_to_lls_on_delete==true))
			{
				//schedule_remote_to_local_sync_if_lls($locationid,"table updated",$tablename);
				schedule_remote_to_local_sync($locationid,"table updated",$tablename);
			}
		}

		if($rowid || $addnew)
		{
			$back_to = $forward_to."&ks=1";
			$forward_url = $forward_to;

			if(!isset($back_to_title))
				$back_to_title = "&#10094; ".speak("View List");
			global $s_browe_includes_form_tablename;
			$s_browe_includes_form_tablename = $tablename.'_edit_page';
			require_once(resource_path() . "/form.php");
			unset($s_browe_includes_form_tablename);
		}
		else
		{
			$allow_add_new = true;

			if ($tablename == "tax_rates") {
				$count_query = lavu_query("SELECT COUNT(`id`) FROM `tax_rates` WHERE $filter_by AND `_deleted` != '1'");
				$row_count = mysqli_result($count_query, 0);
				if ($row_count > 2) {
					$allow_add_new = false;
				}
			}

			if ($allow_add_new) {
				echo "<a href='$forward_to&addnew=1' class='addNewBtn'><span class='plus'> + </span>  ".speak("Add New")."</a>$option_links<br>&nbsp;";
			} else {
				echo "<br>&nbsp;";
			}

			$browse_title = "";
			$run_query = true;
			if($search_mode=="only" || $search_mode=="combined")
			{
				if ($search_mode=="only") $run_query = false;

				$searchfor = postvar("searchfor");
				if (!$searchfor && sessvar("searchfor") && $keep_search=="1") $searchfor = sessvar("searchfor");
				set_sessvar("searchfor", $searchfor);

				if ($searchfor)
				{
					$browse_title = speak("Search Results for")." \"$searchfor\":";
					$run_query = true;

					if($filter_by!="") $filter_by .= " and ";
					$filter_by .= "(";
					for($i=0; $i<count($search_terms); $i++)
					{
						if($i > 0)
							$filter_by .= " or ";
						$sterm = $search_terms[$i];
						if(strpos($sterm,"[")!==false)
						{
							$cparts = array();
							$tparts = explode("[",$sterm);
							if($tparts[0]!="") $cparts[] = "'".str_replace("'","''",$tparts[0])."'";
							for($n=0; $n<count($tparts); $n++)
							{
								$tsplit = explode("]",$tparts[$n]);
								if(count($tsplit) > 1)
								{
									$cparts[] = "`".$tsplit[0]."`";
									if($tsplit[1]!="") $cparts[] = "'".str_replace("'","''",$tsplit[1])."'";
								}
							}
							if(count($cparts)>0)
							{
								$filter_by .= "CONCAT(";
								for($n=0; $n<count($cparts); $n++)
								{
									if($n > 0) $filter_by .= ",";
									$filter_by .= $cparts[$n];
								}
								$filter_by .= ") LIKE '%".str_replace("'","''",$searchfor)."%'";
							}
						}
						else
						{
							$filter_by .= "`$sterm` LIKE '%".str_replace("'","''",$searchfor)."%'";
						}
					}
					$filter_by .= ")";
				}

				$special_searchfor = postvar("special_searchfor");
				if (isset($special_search) && is_array($special_search) && $special_searchfor && $special_searchfor!="") {
					$run_query = true;
					if($filter_by!="") $filter_by .= " AND ";
					$filter_by .= "`".$special_search[0]."` = '".str_replace("'", "''", $special_searchfor)."'";
				}
			}

			if ($search_mode!="")
			{
				echo "<form method='post' action='$forward_to'>";
				if (isset($special_search) && is_array($special_search)) {
					if ($special_search[1] == "select") {
						echo "Filter search by: <select name='special_searchfor'>";
						echo "<option value=''>".$special_search[3]."</option>";
						$keys = array_keys($special_search[2]);
						foreach ($keys as $key) {
							if ($key != "") {
								$selected = "";
								if ($key == ($special_searchfor?$special_searchfor:"")) $selected = " selected";
								echo "<option value='$key'$selected>".$special_search[2][$key]."</option>";
							}
						}
						echo "</select><br><br>";
					}
				}
				echo "<input type='text' name='searchfor' size='40'><input type='hidden' name='page' value='1'><input type='submit' value='".speak("Search")."'>";
				echo "</form>";
			} else echo "<br>";

			if($run_query)
			{

				$query = "`$tablename` where `_deleted` != '1'";
				if($filter_by!="") $query .= " AND $filter_by";
				if (isset($order_by) && ($order_by != "")) $query .= " ORDER BY `$order_by` $order_dir";

				if(isset($_GET['test']))
				{
					echo "SELECT COUNT(`id`) FROM ".$query . "<br>";
				}
				$get_count = lavu_query("SELECT COUNT(`id`) FROM ".$query);
				$total_count = mysqli_result($get_count, 0);

				if ($browse_title!="") echo "<b>".$browse_title . "</b><br>";
				if (isset($searchfor) && ($searchfor!="")) echo $total_count." ".speak("records contain matches")."<br>";
				else if ($total_count > 1) echo $total_count." ".speak("records found")."<br>";
				else if ($total_count == 1) echo $total_count." ".speak("record found")."<br>";

				if ($per_page != 0) {

					$query .= " LIMIT ".(((int)$search_page - 1) * $per_page).", ".$per_page;

					echo "<table cellpadding='5' width='350px'><tr>";

					if ($search_page > 1) echo "<td width='175px' align='left'><a href='$forward_to&page=".($search_page - 1)."'><< ".speak("Previous")." $per_page</a></td>";
					else echo "<td width='150px'>&nbsp;</td>";

					$next_count = $per_page;
					if ($total_count < (($search_page + 1) * $per_page)) $next_count = ($total_count % ($search_page * $per_page));
					if ($total_count > ($search_page * $per_page)) echo "<td width='175px' align='right'><a href='$forward_to&page=".($search_page + 1)."'>".speak("Next")." $next_count >></a></td>";
					else echo "<td width='150px'>&nbsp;</td>";

					echo "</tr></table>";
				} else echo "<br>";

				echo "<table id='_tbl_browse_id' style='border:solid 1px black;".(isset($form_max_width)?" max-width:".$form_max_width."px;":"")."' cellspacing=0 cellpadding=4>";
				echo "<tr>";
				$fldCnt = count($fields);
				for ($i = 0; $i < $fldCnt; $i++)
				{
					if ( !empty($fields[$i]) ) 
					{
						$field = new FormField($fields[$i]);
						
						if($field->list == "yes" || $field->type=="browse_button")
						{
							$click_code = "";
							$style_code = "";
							if (isset($allow_order_by) && ($allow_order_by)) {
								$set_dir = "asc";
								if ($field->name==$order_by && $order_dir=="asc") $set_dir = "desc";
								$click_code = " onclick='window.location = \"$forward_to&ob=".$field->name."&od=$set_dir\"'";
								$style_code = " cursor:s-resize;";
								if ($set_dir == "desc") $style_code = " cursor:n-resize;";
							}
							$s_helptext = create_help_text_for_browse_element($field, $tablename);
							echo "<td$click_code align='center' style='padding-left:4px; padding-right:4px; border-bottom:solid 1px black;$style_code' bgcolor='#cdcdcd'>" . $field->title . "{$s_helptext}</td>";
						}					
					}
				}
				echo "<td bgcolor='#cdcdcd' style='border-bottom:solid 1px black'>&nbsp;</td>";
				echo "</tr>";

				// based on date format setting
				$date_setting = "date_format";
				$location_date_format_query = lavu_query( "SELECT `location`, `setting`, `value` FROM `config` WHERE `setting` = '[1]'",  $date_setting);
				if( !$location_date_format_query || !mysqli_num_rows($location_date_format_query)){
					$date_format = array("value" => 2);
				}else
					$date_format = mysqli_fetch_assoc( $location_date_format_query );

				if(isset($_GET['a1'])) echo $query . "<br>";
				$row_query = lavu_query("select * from ".$query);
				$row_count = 0;
				if(mysqli_num_rows($row_query))
				{
					while($row_read = mysqli_fetch_assoc($row_query))
					{
						$row_count++;
						$colcount = 0;
						$rowid = $row_read['id'];
						if ((array_key_exists('access_level', $row_read) && (admin_info("access_level") >= $row_read['access_level'])) || ($tablename != "users")) {
							$click_code = "onclick='window.location = \"$forward_to&rowid=$rowid\"'";
						} else {
							$click_code = "onclick='alert(\"".speak("Access denied.")."\");'";
						}
						echo "<tr style='cursor:pointer' onmouseover='this.bgColor = \"#eeeeee\"' onmouseout='this.bgColor = \"#ffffff\"'>";
						for($i=0; $i<count($fields); $i++)
						{
							$field = new FormField($fields[$i]);
							if(property_exists($field, 'list') && $field->list=="yes")
							{
								$colcount++;
								echo "<td valign='top' $click_code>";
								if($field->name == "datetime"){
									$split_date = explode(" ", $row_read[$field->name]);
									$change_format = explode("-", $split_date[0]);
									switch ($date_format['value']){
										case 1:$rowval = $change_format[2]."-".$change_format[1]."-".$change_format[0]." ".$split_date[1];
										break;
										case 2:$rowval = $change_format[1]."-".$change_format[2]."-".$change_format[0]." ".$split_date[1];
										break;
										case 3:$rowval = $change_format[0]."-".$change_format[1]."-".$change_format[2]." ".$split_date[1];
										break;
									}
								} else if($field->name == "tip_sharing_period") { 
								    $rowval = $row_read[$field->name];
								    if (stripos($rowval, "ByShift") !== false) {
								        $rowval = str_replace("ByShift", "By Shift", $rowval);
								    }
								} else {
									$rowval = $row_read[$field->name];
								}
								//$rowval = $row_read[$field->name];
								if($field->type=="time" || $field->type=="time_no_late_night")
									echo display_time($rowval);
								else if($field->type=="select")
								{
									if(isset($field->props) && isset($field->props[$rowval]))
										echo $field->props[$rowval];
									else echo $rowval;
								}
								else if($field->type=='dynamic_select'){
									if(!empty($row_read['value_long2'])){
										$decoded = LavuJson::json_decode($row_read['value_long2']);
										echo $decoded['brand'];
										//error_log(print_r($decoded,1));
									}else{
										echo "No type selected";
										//error_log("No Type Selected");
									}
									//echo"<pre>". print_r($row_read,1);
								}
								else if ($field->type == "bool")
								{
									if ($rowval == "1") echo "Yes";
									else echo "No";
								}
								else if ($field->type == "negative bool")
								{
									if ($rowval == "1") echo "No";
									else echo "Yes";
								}
								else if ($field->type == "employee_classes")
								{ //6|2.00,1|3.50,2|7.00,5|3.00
									$class_ids = explode(",", $rowval);
									$class_id = "";
									if (!empty($class_ids[0]) ) {
										$class_id = explode("|",$class_ids[0]);
										$class_id = $class_id[0];
									}

									if (!empty($class_id)) {
									    echo $field->props2[$class_id];
									}
									else echo $class_id;
								}
								else if($field->type=="tipout_rules")
								{
									if(trim($rowval)=="")
									{
										echo "&nbsp;";
									}
									else
									{
										$show_rowval = "";

										$rowval_parts = array();
										
										if (!empty($rowval))
										{
											$rowval_parts = explode(",",$rowval);
										}
										
										if(count($rowval_parts)==1) 
											echo "1 Group";
										else 
											echo count($rowval_parts) . " Groups";
									}
								}
								else
									echo htmlspecialchars($rowval);
								echo "</td>";
							}
							else if($field->type=="browse_button")
							{
								$set_target = "";
								if(isset($field->props) && isset($field->props['target']))
									$set_target = "_blank";
								if(isset($field->props) && isset($field->props['link']))
								{
									$set_link = str_replace("[id]",$rowid,$field->props['link']);
									if(isset($row_read['trackid'])) $set_link = str_replace("[trackid]",$row_read['trackid'],$set_link);
									echo "<td valign='top'><input type='button' style='font-size:10px' value='$field->title' onclick='window.location = \"".$set_link."\"' target=\"$set_target\"></td>";
								}
								else
									echo "<td valign='top'><input type='button' style='font-size:10px' value='$field->title' onclick='window.location = \"$forward_to&$field->name=$rowid\"'></td>";
							}
						}
						if ($mode == "employee_classes") {
							echo "<td><a style='cursor:pointer' onclick='var rowid = $rowid; check_users(rowid); '><img src='images/little_trash.gif' border='0'/></a></td>";
						}
						else if ( ( array_key_exists('access_level', $row_read) && (admin_info("access_level") >= $row_read['access_level'])) || ( $mode != "edit_users" ) ) {
						    echo "<td><a style='cursor:pointer' onclick='if(confirm(\"".speak("Are you sure you want to remove this item?")."\")) window.location = \"$forward_to&removeid=$rowid\"'><img src='images/little_trash.gif' border='0'/></a></td>";
						}
						else {
						    echo "<td onclick='alert(\"".speak("Access denied.")."\");'><img src=\"images/little_trash.gif\" border=\"0\"/></td>";
						}
						echo "</tr>";
					}
				}
				else
				{
					echo "<tr><td colspan='8' align='center'>".speak("No Records Found")."</td></tr>";
				}
				echo "</table>";
			}
		}
	}
?>
<script language='javascript'>
function check_users(rowid){
	var forwardto = "<?php echo $forward_to ;?>";
	var url = 'resources/check_assigned_users.php?row_id='+rowid;
	var xhr = window.XMLHttpRequest ? new XMLHttpRequest() : new ActiveXObject('Microsoft.XMLHTTP');
	xhr.open('GET', url);
	xhr.onreadystatechange = function() {
	    if (xhr.readyState>3 && xhr.status==200) 
	    {
			if(xhr.responseText == 'Success'){
				if(confirm("Are you sure you want to remove this item?"))
				{
					window.location =  forwardto+"&removeid="+rowid;
				}
			}
			if(xhr.responseText == 'Failure')
			{
				alert('Before deletion, disassociate this class from employees!');
			}
	    }
	};
	xhr.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
	xhr.send();
}
</script>