<?php
	ini_set('display_errors',1);
	require_once("lavuquery.php");
	require_once("json.php");

	$query =mlavu_query("SELECT * FROM `poslavu_MAIN_db`.`messages` WHERE DATE(`removal_date`) > DATE(NOW()) AND `_deleted`!='1' and `type`='5'  limit 1");
	if(mysqli_num_rows($query)){
		
		$res= mysqli_fetch_assoc($query);
		$res['status']="message";
		$res['content']= trim(preg_replace('/\t|\n/', ' ',$res['content']));
		echo LavuJson::json_encode($res);

	}else{
		echo'{"status":"no_message"}';
	}

?>