<?php
/**
 * Created by PhpStorm.
 * User: hayat
 * Date: 7/3/18
 * Time: 6:15 PM
 */

require_once(__DIR__.'/core_functions.php');
require_once(__DIR__.'/../../api_private/Components/DataControllerBase.php');
require_once(__DIR__.'/../../api_private/Components/Redis/RedisDataController.php');
require_once(__DIR__.'/../../api_private/Redis/RedisConnection.php');
require_once(__DIR__.'/../../api_private/Components/Redis/RedisQueries.php');

$redisLanguageDictionary = getenv('REDIS_LANGUAGE_DICTIONARY');
$redisLanguageDictionary = ($redisLanguageDictionary) ? $redisLanguageDictionary : 0;
$redisEnabled = getenv('ENABLED_REDIS');

/**
 * Description:- get language dictionary data for given key
 * @param return cached data
 */
function getLanguageDictionary($locId = 1, $used = 'cp') {
    global $data_name, $active_language_pack_id, $redisLanguageDictionary, $redisEnabled;
    $key = 'active_language_pack';
    //TODO:- Remove condition once caching will stable
    if ($redisEnabled && $redisLanguageDictionary) {
        $sessData = sessvar($key);
        if ($sessData) {
            $response = $sessData;
        } else {
            $locationId = ($locId) ? $locId : sessvar("locationid");
            $usedBy = ($used == 'app') ? '_app' : '';
            $rkey = ($active_language_pack_id > 0) ? $key . $usedBy . '_'.$active_language_pack_id : $key . $usedBy . '_'.$locationId.'_'.$active_language_pack_id.'_'.$data_name;
            $args['Items']['key'] = $rkey;
            $response =  unserialize(urldecode(RedisDataController::action_getLanguageDictionary($args)));
            if (!empty($response)) {
                set_sessvar($key, $response);
            }
        }
        return $response;
    } else {
        return sessvar($key);
    }
}

/**
 * Description:- set language dictionary data for given key
 * @param $data urlencode(serialize(data))
 * @param return cached data
 */
function setLanguageDictionary($data, $locId = 1, $used = 'cp') {
    global $data_name, $active_language_pack_id, $redisLanguageDictionary, $redisEnabled;
    $key = 'active_language_pack';
    //TODO:- Remove condition once caching will stable
    if ($redisEnabled && $redisLanguageDictionary) {
        $locationId = ($locId) ? $locId : sessvar("locationid");
        $usedBy = ($used == 'app') ? '_app' : '';
        $rkey = ($active_language_pack_id > 0) ? $key . $usedBy . '_'.$active_language_pack_id : $key . $usedBy . '_'.$locationId.'_'.$active_language_pack_id.'_'.$data_name;
        $cacheData['key'] = $rkey;
        $cacheData['data'] = $data;
        $cacheJsonData = json_encode(urlencode(serialize($cacheData)));
        $args['Items'] = $cacheJsonData;
        set_sessvar($key, $data);
        RedisDataController::action_setLanguageDictionary($args);
    } else {
        set_sessvar($key, $data);
    }
    return true;
}

/**
 * Description:- get cache data for given key
 * @param return cached data
 */
function getActiveLanguagePackId($userId = '', $locId = '') {
    global $data_name, $redisLanguageDictionary, $redisEnabled;
    $key = 'active_language_pack_id';
    //TODO:- Remove condition once caching will stable
    if ($redisEnabled && $redisLanguageDictionary) {
        $packId = sessvar($key);
        if ($packId === '') {
            $user_id = ($userId) ? $userId : admin_info("loggedin");
            $locationid = ($locId) ? $locId : sessvar("locationid");
            $args['Items']['key'] = $key;
            $usersPackIds = json_decode(RedisDataController::action_getCacheData($args), 1);
            if ($user_id) {
                $packId = $usersPackIds['user'][$data_name][$locationid][$user_id];
            } else {
                $packId = $usersPackIds['no-user'][$data_name][$locationid];
            }
            if ($packId !== '') {
                set_sessvar($key, $packId);
            }
        }
        return $packId;
    } else {
        return sessvar($key);
    }
}

/**
 * Description:- get cache data for given key
 * @param $data integer pack id
 * @param return cached data
 */
function setActiveLanguagePackId($data, $userId = '', $locId = '') {
    global $data_name, $redisLanguageDictionary, $redisEnabled;
    $key = 'active_language_pack_id';
    //TODO:- Remove condition once caching will stable
    if ($redisEnabled && $redisLanguageDictionary) {
        $user_id = ($userId) ? $userId : admin_info("loggedin");
        $locationid = ($locId) ? $locId : sessvar("locationid");
        $args['Items']['key'] = $key;
        $usersPackIds = json_decode(RedisDataController::action_getCacheData($args), 1);
        $usersPackIds = ($usersPackIds) ? $usersPackIds : array();
        if ($user_id) {
            $usersPackIds['user'][$data_name][$locationid][$user_id] = $data;
        } else {
            $usersPackIds['no-user'][$data_name][$locationid] = $data;
        }
        $args['Items']['key'] = $key;
        $args['Items']['data'] = json_encode($usersPackIds);
        set_sessvar($key, $data);
        return RedisDataController::action_setCacheData($args);
    } else {
        set_sessvar($key, $data);
    }
    return true;
}

/**
 * Description:- get cache data for given key
 * @param return cached data
 */
function getRedisAvailableLanguagePacks() {
    global $redisLanguageDictionary, $redisEnabled;
    $key = 'available_language_packs';
    $result = array();
    //TODO:- Remove condition once caching will stable
    if ($redisEnabled && $redisLanguageDictionary) {
        $result = sessvar($key);
        if (empty($result)) {
            $args['Items']['key'] = $key;
            $result = json_decode(RedisDataController::action_getCacheData($args));
            if (!empty($result)) {
                set_sessvar($key, $result);
            }
        }
        return $result;
    } else {
        return sessvar($key);
    }
}

/**
 * Description:- get cache data for given key
 * @param $data urlencode(serialize(data))
 * @param return cached data
 */
function setRedisAvailableLanguagePacks($data) {
    global $redisLanguageDictionary, $redisEnabled;
    $key = 'available_language_packs';
    //TODO:- Remove condition once caching will stable
    if ($redisEnabled && $redisLanguageDictionary) {
        $args['Items']['key'] = $key;
        $args['Items']['data'] = json_encode($data);
        set_sessvar($key, $data);
        RedisDataController::action_setCacheData($args);
    } else {
        set_sessvar($key, $data);
    }
    return true;
}

/**
 * Description:- delete cached redis key
 * @param $keys array
 * @param return true | false
 */
function deleteAllLanguagePack($pack_id = '', $locId = '') {
    global $data_name, $redisLanguageDictionary, $redisEnabled;
    $response = true;
    //TODO:- Remove condition once caching will stable
    if ($redisEnabled && $redisLanguageDictionary) {
        $keyLanguage = 'active_language_pack';
        $locationId = ($locId) ? $locId : sessvar("locationid");
        if ($pack_id === '') {
            $get_packs = mlavu_query("SELECT `id` FROM `poslavu_MAIN_db`.`language_packs` WHERE `_deleted` != '1'");
            while ($pack = mysqli_fetch_assoc($get_packs))
            {
                $languagePackKeys[] = $keyLanguage.'_'.$pack['id'];
                $languagePackKeys[] = $keyLanguage.'_app_'.$pack['id'];
            }
        } else {
            $languagePackKeys[] = ($pack_id > 0) ? $keyLanguage . '_' . $pack_id : $keyLanguage . '_' . $locationId . '_' . $pack_id . '_' . $data_name;
            $languagePackKeys[] = ($pack_id > 0) ? $keyLanguage . '_app_' . $pack_id : $keyLanguage . '_app_' . $locationId . '_' . $pack_id . '_' . $data_name;
        }
        if (!empty($languagePackKeys)) {
            $args['Items'] = $languagePackKeys;
            $response = RedisDataController::action_deleteRedisKeys($args);
        }
    }
    return $response;
}
