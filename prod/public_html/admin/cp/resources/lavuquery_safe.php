<?php

require_once(__DIR__ . "/lavuquery_base.php");

class SafeRestaurantDBConnector extends RestaurantDBConnector {

	private function initialize() {
		$this->check_hook = $this->check;
		$this->rules = array(
			"cc_transactions" => array(
				"insert" => $this->check_CCTransactionsInsert
				)
		);
	}
		
	private function check_CCTransactionsInsert($values_and_columns) {

		//TODO: remove some of the fields, we don't want to check everything (like timestamps, in particular)
	
		$keysThatMatter = array('`order_id`', '`check`', '`loc_id`', '`amount`', '`total_collected`', '`pay_type`', '`action`', '`ioid`');

		$interlaced_kv_pairs = array();
		foreach($values_and_columns as $key => $value) {
			if(in_array($key, $keysThatMatter)) {
				$interlaced_kv_pairs[] = $key . "=" . $value;
			}
		}
		$dupe_query_str = "select `id` from cc_transactions where " . implode(", ", $interlaced_kv_pairs);
		$dupe_query = $this->getLink('read')->query($dupe_query_str);
		if(mysqli_num_rows($dupe_query) > 0) {
			return FALSE;
		}
		return TRUE;
	}

	public function check($query) {
		
		$type = $this->checkQueryType($query);
		if ( (empty($type)) || ($type == "select" || $type == "delete") )
			return FALSE;

		$table = $this->checkQueryTable($query);
        if(empty($this->rules[$table]) || empty($this->rules[$table][$type]))
			return FALSE;

		$values_and_columns = $this->stripValues($type, $query);
		if(empty($values_and_columns)){
			return FALSE;
		}
        
		return $this->rules[$table][$type]($values_and_columns);
	}

	private function checkQueryType($query) {
		if(empty($query))
			return "";
		$split = explode(" ", $query);
		return strtolower($split[0]);
	}

	private function checkQueryTable($query) {
		if(empty($query))
			return "";
		$type = $this->checkQueryType($query);
		if(empty($type))
			return "";
		$table = "";
		$query = strtolower($query); //don't want to worry about FROM vs from, etc
		if($type == "update") {
			$table =  explode(" ", $query)[1];
		}
		else if($type == "insert") {
			$table =  explode("into ", $query)[1];
		}
		if($table[0] == '`') $table = preg_replace("/[^A-Za-z0-9 ]/", '', $table); //remove ticks
		return $table;
	}

	private function stripValues($type, $query) {
		$values = null;
		$columns = null;
		if($type == "update") {
			//Get the stuff between the set and the where
			$split1 = explode("set ", $query);
			$split2 = explode(" where ", $split1[1]);
			$combined =  preg_split("/(, |,)/", $split2[0]);
			foreach($combined as $item) {
				$split3 = preg_split("/( = |= | =|=)/", $item);
				$columns[] = $split3[0];
				$values[] = $split3[1];
			}
		}
		else if($type == "insert") {
			preg_match('#\((.*?)\)#', $query, $match);
			$columns = explode(", ", $match[0]);
			$values = explode(", ", $match[1]);
		}
		return array("values" => $values, "columns" => $columns);
	}
}

ConnectionHub::addConn('poslavu', new DBConnector());
ConnectionHub::addConn('rest', new SafeRestaurantDBConnector());


