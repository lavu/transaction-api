<?php
/**
 * Created by PhpStorm.
 * User: Chris
 * Date: 10/31/17
 * Time: 8:09 AM
 * File to host all of the functions for the app to interact with mealplans.
 */
require_once("/home/poslavu/public_html/admin/cp/resources/core_functions.php");
/*
 * takes an object containing user information, primarily an ID.
 * returns the stipend - daily amount.  If The daily_usage_update column is not from "today" just return the stipend amount.
 */
function getStipendBalanceForUser($userInfo)
{
    $timeZone = locationSetting('timezone');
    $timeZonePrevious = date_default_timezone_get();
    date_default_timezone_set($timeZone);

    $lastUsageDate = date('Y-m-d', strtotime($userInfo['daily_usage_update']));
    $currentDate = date('Y-m-d');
    if($timeZone != ""){
        $lastUsageDate = date('Y-m-d',strtotime($userInfo['daily_usage_update']." ".$timeZone));
        $currentDate = date("Y-m-d", strtotime(dateTimeForTimeZone($timeZone)));
    }

    $mealPlanGroup = getMealPlanGroupInfo($userInfo['MealPlanGroupID']);

    date_default_timezone_set($timeZonePrevious);

    if($lastUsageDate < $currentDate){
        //if no usage today, then return the whole stipend
        return $mealPlanGroup['stipend'];
    }
    else{
        //otherwise, return the stipend less daily usage
        return $mealPlanGroup['stipend'] - $userInfo['daily_usage'];
    }
}

function getUserByRFID($rfid, $restaurantid = "")
{
    if(empty($restaurantid)){
        $restaurantid = admin_info("companyid");
    }
    $mealPlanUserQuery = clavuQuery("SELECT * FROM `MealPlanUser` WHERE `rf_id` = '[1]' AND `restaurant_id` = '[2]'", $rfid, $restaurantid);
    if(mysqli_num_rows($mealPlanUserQuery) == 0){
        return false;
    }
    else{
        $mealPlanUser = mysqli_fetch_assoc($mealPlanUserQuery);
        return $mealPlanUser;
    }
}

function getUserByID($id)
{
    $selectArr = implode(',', $select);
    $select = empty($select) ? '*' :  $selectArr;
    // Not using _deleted in where clause because OLO uses _deleted to determine whether Meal Plan account is enabled
    $mealPlanUsers = clavuQuery("SELECT ".$select." FROM `MealPlanUser` WHERE `id` = '[1]' AND  `_deleted` ='0' ", $id);
    if ($mealPlanUsers === false) {
        error_log("Could not get MealPlanUser: ". lavu_dberror());
        return false;
    }
    else if (mysqli_num_rows($mealPlanUsers) <= 0) {
        return false;
    }

    $mealPlanUser = mysqli_fetch_assoc($mealPlanUsers);

    return $mealPlanUser;
}

/*
 * Deduct transaction amount from remaining meal plan stipend balance.  If transaction amount is MORE than the meal plan, deduct remaining balance.
 * Returns remaining balance.
 */
function useMealPlan($args)
{
    $result = array(
        'success'         => false,
        'msg'             => '',
    );
    $mealPlanUser = isset($args['userId']) ? getUserByID($args['userId']) : getUserByRFID($args['rfid'], $args['restaurantid']);
    if (empty($mealPlanUser) || empty($mealPlanUser['id'])) {
        $result['msg'] = "Could not get Meal Plan user ID ". $args['userId'] ." RFID ". $args['rfid'];
        error_log($result['msg']);
        return $result;
    }

    $mealPlanGroup = getMealPlanGroupInfo($mealPlanUser['MealPlanGroupID']);

    if (empty($mealPlanGroup)) {
        $result['msg'] = "Could not get Meal Plan Group for user ". $mealPlanUser['id'] ." with Meal Plan Group ID ". $mealPlanUser['MealPlanGroupID'];
        error_log($result['msg']);
        return $result;
    }

    $cashBalance = (double) $mealPlanUser['cash_balance'];
    $orderAmount = (double) $args['amount'];
    $stipendAvailable = getStipendBalanceForUser($mealPlanUser);

    // Use the stipend balance - must have enough to cover the whole check, no partial pay is currently supported
    if (  $stipendAvailable >0 && $orderAmount <= ($stipendAvailable+$cashBalance)) {
    	if($stipendAvailable < $orderAmount ) {
    		$stipendToUse = $stipendAvailable;
    	} else {
    		$stipendToUse = $orderAmount;
    	}
        $newStipendAvailable = $stipendAvailable - $stipendToUse;
        $currentTimeStamp = dateTimeforTimeZone(locationSetting('timezone'));

        $query = "UPDATE `MealPlanUser` SET `daily_usage` = CASE WHEN DATE(`daily_usage_update`) < DATE('[2]') THEN '[1]' ELSE `daily_usage` + '[1]' END, `daily_usage_update` = '[2]', `last_update` = '[2]' WHERE `id` = '[3]'";
        $updateUserQuery = clavuQuery($query, $stipendToUse, $currentTimeStamp, $mealPlanUser['id']);

        $result['success'] = $updateUserQuery;
        $result['amount']  = $stipendToUse;
        $result['stipend_balance'] = $newStipendAvailable;
        if($stipendAvailable < $orderAmount) {
        	$args['mealPlanUser'] = $mealPlanUser;
        	$args['amount'] = $orderAmount - $stipendAvailable;
        	$result = useCashBalance($args);
        }
    }
    // Allow fall back to cash balance if stipend is depleted - unless flag is set
    else if (empty($args['use_stipend_only']))
    {
        $args['mealPlanUser'] = $mealPlanUser;
        $result = useCashBalance($args);
    }

    return $result;
}

/*
 * Deduct transaction amount from cash balance.  If transaction amount is more than the cash balance, deduct
 * remaining balance from transaction amount and return remaining balance.
 */
function useCashBalance($args){

    $result = array(
        'success'      => false,
        'msg'          => '',
    );

    if (empty($args['mealPlanUser'])) {
        $mealPlanUser = isset($args['userId']) ? getUserByID($args['userId']) : getUserByRFID($args['rfid'], $args['restaurantid']);
    }
    else {
        $mealPlanUser = $args['mealPlanUser'];
    }

    if (empty($mealPlanUser) || empty($mealPlanUser['id'])) {
        $result['msg'] = "Could not get Meal Plan user ID ". $args['userId'] ." RFID ". $args['rfid'];
        error_log($result['msg']);
        return $result;
    }

    $cashToUse   = (double) $args['amount'];
    $cashBalance = (double) $mealPlanUser['cash_balance'];

    // Make sure they have the cash to cover the entire check, no partial payments are currently supported
    if ($cashToUse <= $cashBalance)
    {
        $newCashBalance = $cashBalance - $cashToUse;
        $currentTimeStamp = dateTimeforTimeZone(locationSetting('timezone'));

        $query = "UPDATE `MealPlanUser` SET `cash_balance` = '[1]', `last_update` = '[2]' WHERE `id` = '[3]'";
        $updateUserQuery = clavuQuery($query, $newCashBalance, $currentTimeStamp, $mealPlanUser['id']);

        $result['success'] = $updateUserQuery;
        $result['amount']  = $cashToUse;
        $result['cash_balance'] = $newCashBalance;
        $result['stipend_balance'] = '0';
    }
    else
    {
        $result['msg'] = 'Insufficient funds';
        $result['cash_balance'] = $cashBalance;
    }

    return $result;
}

/*
 * Add cash balance to user
 */
function addCashBalance($args){
    if (empty($args['userId']) || empty($args['amount'])) {
        return false;
    }

    $args['last_update'] = dateTimeforTimeZone(locationSetting('timezone'));

    $query = "UPDATE `poslavu_CHAIN_DATA_db`.`MealPlanUser` SET `cash_balance` = `cash_balance` + [amount], `last_update` = '[last_update]' WHERE `id` = [userId]";
    $updateUserQuery = mlavu_query($query, $args);

    return ($updateUserQuery) ? true : false;
}

function getMealPlanGroupInfo($GroupID){
    $query = "SELECT * FROM `MealPlanGroup` WHERE `id` = '$GroupID'";
    $mealPlanGroupData = clavuQuery($query);
    if(mysqli_num_rows($mealPlanGroupData) == 0){
        return null;
    }
    else{
        $mealPlanGroup = mysqli_fetch_assoc($mealPlanGroupData);
        return $mealPlanGroup;
    }
}

function insertMealPlanActionlog($transaction_info){
    $userID = $transaction_info['userID'];
    $mealPlanGroupID = $transaction_info['groupID'];
    $amount = $transaction_info['amount'];
    $type = $transaction_info['type'];
    $datetime = $transaction_info['datetime'];
    $location_id = $transaction_info['location_id'];

    $insertQuery = "INSERT INTO `MealPlanUsage` SET `MealPlanUserID` = '$userID', `MealPlanGroupID` = '$mealPlanGroupID',
                    `amount` = '$amount', `datetime` = '$datetime', `payment_type` = '$type', `restaurant_id` = '$location_id'";

    clavuQuery($insertQuery);
}

function getMealPlanReportData($args){
    $startTime = $args['start_datetime'];
    $endTime = $args['end_datetime'];
    $type = $args['type'];
    $locations = getChainAccounts(admin_info("dataname"));
    $chainIDs = array();
    foreach($locations as $location){
        $chainIDs[] = "'".$location['id']."'";
    }
    $chainIDStatement = implode(",",$chainIDs);
    $query = "SELECT  `MealPlanGroup`.`name` as 'Group Name',
              `MealPlanUsage`.`restaurant_id` as 'Location Name',
              `MealPlanUsage`.`MealPlanGroupID`,
              SUM(`MealPlanUsage`.`amount`) as 'Amount' 
              FROM `MealPlanUsage` JOIN `MealPlanGroup`
              ON `MealPlanUsage`.`MealPlanGroupID` = `MealPlanGroup`.`id` 
              WHERE `MealPlanUsage`.`payment_type` = '$type' 
              AND `MealPlanUsage`.`datetime` >= '$startTime' 
              AND `MealPlanUsage`.`datetime` <= '$endTime'
              AND `MealPlanUsage`.`restaurant_id` IN ($chainIDStatement) 
              GROUP BY `MealPlanUsage`.`MealPlanGroupID`, `MealPlanUsage`.`restaurant_id`
              ORDER BY `MealPlanGroup`.`name` ASC";

    $results = clavuQuery($query);
    $data = array();
    if(mysqli_num_rows($results) == 1){
        $data[] = parseResults(mysqli_fetch_assoc($results));
    }
    else{

        while($result = mysqli_fetch_assoc($results)){
            $data[] = parseResults($result);
        }
    }
    return $data;
}

function parseResults($result){
    $dataRow = array();
    foreach($result as $key => $val){
        $new_key = preg_replace('/([a-z0-9])([A-Z])/', '${1} ${2}', $key);
        $new_key = str_ireplace(' Of ', ' of ', $new_key);

        // Skip ID fields and quantity fields
        if (preg_match('/ID$/', $new_key)){
            continue;
        }
        // Add [money] keyword to financial figures
        else if (preg_match('/[0-9][.][0-9]/', $val)|| preg_match('/Amount/', $new_key)) {
            $dataRow[$new_key] = '[money]'. $val;
        }
        else if (preg_match('/Location/', $new_key)) {
            $companyInfo = getCompanyInfo($val, 'id');
            $locationName = isset($companyInfo['company_name'])?str_replace("_", " ", $companyInfo['company_name']):"";
            $dataRow[$new_key] = $locationName;
        }
        else {
            $dataRow[$new_key] = $val;
        }
    }
    return $dataRow;
}

function mealPlanEnable($dataname, $userId) {
    $mealPlanInfo = ["mealPlanEnable" => "false"];
    if(!empty($dataname) && !empty($userId)) {
        $dataname = 'poslavu_'.$dataname.'_db';
        $query = "select id , type from `$dataname`.`payment_types` where `type`='MEAL PLAN' and `_deleted`= 0";
        $result=mlavu_query($query);
        $payment_types = mysqli_fetch_assoc($result);

        if (!empty($payment_types)) {
            $MealPlanInfoData = getUserByID($userId);
            if (isset($MealPlanInfoData['restaurant_id'])) {
                $restaurantId= $MealPlanInfoData['restaurant_id'];
                $chainId = $MealPlanInfoData['chain_id'];
                $whereColumnArr = array("id" => $restaurantId);
                $restaurantsAddrColNameArr = array("id","chain_id","data_name");
                $restaurantRowsResult = CQ::getRowsWithRequiredColumnsMainDB('restaurants', $restaurantsAddrColNameArr, $whereColumnArr,'');
                if (
                    isset($restaurantRowsResult[0]) && 
                    (
                        $restaurantRowsResult[0]["id"] == $restaurantId || 
                        $chainId == $restaurantRowsResult[0]["chain_id"]
                    )
                ) {
                    $mealPlanInfo = array(
                        'userId'          => $userId,
                        'enabled'         => !$MealPlanInfoData['_deleted'],
                        'badge_number'    => (empty($MealPlanInfoData['badge_id'])) ? '0' : $MealPlanInfoData['badge_id'],
                        'stipend_balance' => (double) getStipendBalanceForUser($MealPlanInfoData),
                        'card_balance'    => (double) $MealPlanInfoData['cash_balance'],
                        'mealPlanEnable'  => 'true'
                    );
                }
            }
        }
    }
    
    return $mealPlanInfo;
}
