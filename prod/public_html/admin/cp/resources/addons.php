<?php

	require_once(dirname(__FILE__)."/json.php");
	require_once(dirname(__FILE__)."/../../sa_cp/billing/procareas/monthly_parts.php");
	require_once(dirname(__FILE__)."/../../sa_cp/billing/procareas/invoice_parts.php");

	global $o_resourcesAddonsManager;
	if (!isset($o_resourcesAddonsManager))
		$o_resourcesAddonsManager = new ResourcesAddonsManager();

	class AccountAddonsManager {

		// gets the addon object of every addon an account has
		// @$s_dataname: the dataname on the account
		// @$b_use_mlavu: uses mlavu query for all of its queries if TRUE, use lavu_query if FALSE
		function __construct($s_dataname, $b_use_mlavu) {
			
			$dbapi = ConnectionHub::getConn('poslavu')->getDBAPI();
			// initialize some variables
			$s_database = "poslavu_{$s_dataname}_db";
			$b_use_mlavu = $b_use_mlavu;

			// load all the addons
			$o_all_addons = ResourcesAddonsManager::getAllAddons();
			$a_configs = $this->dbapi->getAllInTable('config', array('setting'=>'lavu_addons', 'type'=>'addon'), $b_use_mlavu, array('databasename'=>$s_database, 'selectclause'=>'`value` AS `addon_list`, `value2` AS `addon_history`'));
			if (count($a_configs) == 0)
				return new stdClass();
			$this->addon_list = (array)LavuJson::json_decode($a_configs[0]['addon_list']);
			$this->history = LavuJson::json_decode($a_configs[0]['addon_history']);

			// compare account addons to all addons
			$this->addons = new stdClass();
			foreach($o_all_addons as $o_addon) {
				$id = $o_addon->id;
				if (in_array($id, $this->addon_list))
					$this->addons->$id = $o_addon;
			}
			getTrialStatus($this->addons, $s_dataname);

			$this->dataname = $s_dataname;
			$this->database = $s_database;
			$this->use_mlavu = $b_use_mlavu;
			$this->error = "";
		}

		// @return: all addons this user has
		public function getAllAddons() {
			return $this->addons;
		}

		// @return: TRUE or FALSE
		public function hasAddon($s_addon_id) {
			if (isset($this->addons->$s_addon_id))
				return TRUE;
			return FALSE;
		}

		// @return: the trial status object(available, in_trial, and days_left),
		//     or NULL if the account doesn't have this addon
		public function getTrialStatus($s_addon_id) {
			if (!$this->hasAddon($s_addon_id))
				return NULL;
			return $this->addons->$s_addon_id->trial_status;
		}

		// starts the trial, if a trial is available and hasn't already been started
		// @return: TRUE on success, FALSE on failure (check $this->error),
		//     or NULL if the account doesn't have this addon
		public function startTrial($s_addon_id) {
			if (!$this->hasAddon($s_addon_id))
				return NULL;
			$o_addon = $this->addons->$s_addon_id;
			$o_trial_status = $o_addon->trial_status;
			if (!$o_addon->has_trial || !$o_trial_status->available ||
				$o_trial_status->in_trial || $o_trial_status->days_left == 0) {
				$s_name = $o_addon->properties->name;
				if ($o_trial_status->days_left == 0) $this->error = "Your trial period for \"{$s_name}\" has expired.";
				if ($o_trial_status->in_trial) $this->error = "You already started a trial for \"{$s_name}\".";
				if (!$o_trial_status->available) $this->error = "The trial period for \"{$s_name}\" isn't available.";
				if (!$o_addon->has_trial) $this->error = "The addon \"{$s_name}\" provides no trial period.";
				return FALSE;
			}

			// start the trial
			$s_start_date = date("Y-m-d");
			$s_end_date = $o_addon->properties->trial_end;
			$i_available = 0;
			$i_in_trial = 1;
			$i_days_left = ResourcesAddonsManager::calculateDaysDifference($s_start_date, $s_end_date);
			$a_where_vars = array("setting"=>"trial_status", "type"=>"addon", "value"=>$o_addon->getID());
			$a_update_vars = array_merge($a_where_vars, array("value2"=>$s_start_date, "value4"=>$s_end_date, "value5"=>$i_available, "value6"=>$i_in_trial, "value7"=>$i_days_left));
			$this->dbapi->insertOrUpdate($this->database, 'config', $a_update_vars, $a_where_vars);

			// add the trial to the history object
			$id = $o_addon->id;
			$this->modTrialHistory($id, (object)array('start_time'=>date("Y-m-d H:i:s")));
			$this->save_history();

			return TRUE;
		}

		// enable the addon for this account
		// @$s_addon_id: the id of the addon to enable/disable
		// @$o_modules: the modules object for this account
		// @return: TRUE on success, FALSE on failure (check $this->error),
		//     or NULL if the account doesn't have this addon
		public function enableAddon($s_addon_id, $o_modules) {
			if (!$this->hasAddon($s_addon_id))
				return NULL;
			$o_addon = $this->addons->$s_addon_id;
			$s_name = $o_addon->properties->name;

			// check that the addon isn't already enabled
			if (in_array($s_addon_id, $this->addon_list)) {
				$this->error = "\"{$s_name}\" is already enabled.";
				return FALSE;
			}

			// load the history
			$a_configs = $this->dbapi->getAllInTable('config', array('setting'=>'lavu_addons', 'type'=>'addon'), $b_use_mlavu, array('databasename'=>$s_database, 'selectclause'=>'`value3` AS `addon_history`'));
			if (count($a_configs) > 0)
				$o_history = LavuJson::json_decode($a_configs[0]['addon_history']);
			else
				$o_history = new stdClass();

			// check if the addon has been enabled too many times
			if ($o_addon->enabledTooManyTimes($o_history)) {
				$s_time = $o_addon->properties->max_enable_times->since;
				$i_num_times = ((int)$o_addon->properties->max_enable_times->max)-1;
				$this->error = "You have enabled \"{$s_name}\" more than {$i_num_times} times in the past {$s_time}. Please contact Lavu Support if you wish to enable this addon again.";
				return FALSE;
			}

			// check that the basic requirements are met
			if (!$o_addon->meetsRequirements($o_modules)) {
				$this->error = "You must meet the basic requirements for \"{$s_name}\" in order to enable it.";
				return FALSE;
			}

			// create the monthly part
			if (isset($o_addon->properties->monthly_part)) {
				$a_db_vars = array('dataname'=>$s_dataname, 'mp_start_day'=>date("Y-m-d"));
				$a_db_vars = array_merge($a_db_vars, $o_addon->properties->monthly_part);
				$o_monthly_part = new MonthlyInvoiceParts(0, $a_db_vars);
				$o_monthly_part->save_to_db();
			}

			// enable
			$this->_enableAddon($s_addon_id);
			return TRUE;
		}

		// enable the addon, save the list of enabled addons to the database,
		// and add to the addon history
		private function _enableAddon($s_addon_id) {

			// enable
			$this->addon_list[] = $s_addon_id;

			// edit history
			if (!isset($this->history->$s_addon_id))
				$this->history->$s_addon_id = new stdClass();
			if (!isset($this->history->$s_addon_id->enable))
				$this->history->$s_addon_id->enable = new stdClass();
			$o_history = $this->history->$s_addon_id->enable;
			$s_date = date("Y-m-d H:i:s");
			$o_history->$s_date = (object)array(
				'source'=>'/resources/addons.php'
			);

			// save to db
			$this->saveData(array(
				'value'=>LavuJson::json_encode($this->addon_list),
				'value2'=>LavuJson::json_encode($this->history)
			));
		}

		// @return: TRUE on success, FALSE on failure (check $this->error),
		//     or NULL if the account doesn't have this addon
		public function disableAddon($s_addon_id) {
			if (!$this->hasAddon($s_addon_id))
				return NULL;
			$o_addon = $this->addons->$s_addon_id;
			$s_name = $o_addon->properties->name;

			// check that the user currently has the addon
			if (!in_array($s_addon_id, $this->addon_list)) {
				$this->error = "You must have \"{$s_name}\" enabled in order to disable it.";
				return FALSE;
			}

			// add the addon to the next month's billing if it has a monthly part
			// and the monthly part was just enabled
			if (isset($o_addon->properties->monthly_part)) {
				$this->addMonthlyPartToNextHosting($o_addon, strtotime("now"));
			}

			// disable the addon
		}

		// adds the cost of the monthly part to the next month's hosting
		// if the hosting date can't be predicted, it sends an email to 'billing@poslavu.com'
		// @$o_addon: the addon to charge for
		// @$i_today: the datetime of the current day (usually strtotime("now"))
		private function addMonthlyPartToNextHosting($o_addon, $i_today) {

			// set some values
			$s_addon_id = $o_addon->getID();
			$s_name = $o_addon->properties->name;
			$s_today = date("Y-m-d", $i_today);
			$i_date_today = strtotime($s_today);

			// find the most recent monthly part
			$a_db_vars = array('dataname'=>$this->dataname);
			$a_db_vars = array_merge($o_addon->properties->monthly_part, $a_db_vars);
			$a_monthly_parts = $this->dbapi->getAllInTable('monthly_parts', $a_db_vars, TRUE, array('orderbyclause'=>'ORDER BY `mp_start_day` DESC', 'limitclause'=>'LIMIT 1'));
			if (count($a_monthly_parts) == 0)
				return FALSE;
			$o_monthly_part = new MonthlyInvoiceParts(0, $a_monthly_parts[0]);
			if ($o_monthly_part->db_values->mp_end_day == "") {
				if (strtotime($o_monthly_part->db_values->mp_start_day) > $i_date_today)
					$o_monthly_part->db_values->mp_end_day = $o_monthly_part->db_values->mp_start_day;
				else
					$o_monthly_part->db_values->mp_end_day = $s_today;
			}

			// check that the monthly part started within the last month
			// and that the monthly part comes on or before this date
			$db = $o_monthly_part->db_values;
			$i_mp_start_day = strtotime($db->mp_start_day);
			if ($i_mp_start_day > $i_date_today)
				return TRUE;
			if ($i_mp_start_day < strtotime("-1 months", $i_date_today))
				return TRUE;

			// figure out the cost for the addon, based off this single monthly part
			$o_invoice_part = InvoiceParts::createNewInvoicePartWithProperties($db, $i_date_today, $s_today, $this->dataname, 0, $db->waived);
			$f_amount = $o_invoice_part->db_values->amount;
			$s_amount = number_format($f_amount, 2);

			// predict the previous month's and next month's hosting
			$a_invoice_parts = $this->dbapi->getAllInTable('invoice_parts', array('dataname'=>$this->dataname, 'type'=>'Hosting', 'due_date'=>$s_today), TRUE, array('whereclause'=>"WHERE `dataname`='[dataname]' AND `type` LIKE '[type]' AND `due_date`<='[due_date]'", 'orderbyclause'=>'ORDER BY `due_date` DESC', 'limitclause'=>'LIMIT 1'));
			$a_invoices = $this->dbapi->getAllInTable('invoices', array('dataname'=>$this->dataname, 'type'=>'Hosting', 'due_date'=>$s_today), TRUE, array('whereclause'=>"WHERE `dataname`='[dataname]' AND `type`='[type]' AND `due_date`<='[due_date]'", 'orderbyclause'=>'ORDER BY `due_date` DESC', 'limitclause'=>'LIMIT 1'));
			if (count($a_invoices) == 0 && count($a_invoice_parts) == 0) {
				$s_dataname = $this->dataname;
				mail("billing@poslavu.com", "Signup For Addon", "The client {$s_dataname} has enabled the addon {$s_name} within the last 30 days and now has disabled it again. They need to be billed for this addon but the next month's hosting was unable to be determined. Please create an invoice part for the addon with a cost of {$s_amount} for the next hosting date.", "From: noreply@poslavu.com");
				return TRUE;
			}
			if (count($a_invoices) > 0)
				$s_previous_date = $a_invoices[0]['due_date'];
			else
				$s_previous_date = $a_invoice_parts[0]['due_date'];
			$i_prev_date = strtotime($s_previous_date);
			$i_next_date = strtotime("+1 months", $i_previous_date);
			$s_next_date = date("Y-m-d", $i_next_date);

			// check that the loaded monthly part's start date falls
			// between the predicted previous and next month's date
			if ($i_mp_start_day < $i_prev_date || $i_mp_start_day > $i_next_date) {
				return TRUE;
			}

			// check that an invoice part for the next month's hosting doesn't
			// already exist and change it's amount to max(`amount`,$f_amount)
			$a_next_invoice_parts = $this->dbapi->getAllInTable('invoice_parts', array('dataname'=>$this->dataname, 'type'=>'Hosting', 'due_date'=>$s_next_date), TRUE, array('whereclause'=>"WHERE `dataname`='[dataname]' AND `type` LIKE '[type]' AND `due_date`='[due_date]'", 'orderbyclause'=>'ORDER BY `due_date` DESC', 'limitclause'=>'LIMIT 1'));
			if (count($a_next_invoice_part) > 0) {
				$o_next_invoice_part = new InvoiceParts(0, $a_next_invoice_parts[0]);
				$f_previous_amount = (float)$o_next_invoice_part->db_values->amount;
				$f_amount = min((float)$db->amount, $f_previous_amount + $f_amount);
				$i_num_days = ResourcesAddonsManager::calculateDaysDifference($db->mp_start_day, $db->mp_end_day);
				$o_next_invoice_part->update(array('amount'=>$f_amount), TRUE, "User canceled subscription to {$s_addon_id}, number of days {$i_num_days}, previous amount ".number_format($f_previous_amount,2).",");
				return TRUE;
			}

			// create an invoice part for the next month's hosting
			$o_invoice_part = NULL;
			if ($o_monthly_part->create_invoice_part($s_next_date, $this->dataname, 0, FALSE, "User canceled subscription to {$s_addon_id}")) {
				return TRUE;
			}
			return FALSE;
		}

		// mod the account's trial history and
		// save the account's history object to the db
		private function modTrialHistory($addon_id, $o_trial_history) {
			if (!isset($this->history->$id))
				$this->history->$id = new stdClass();
			$this->history->$id->trial = $o_trial_history;
			$this->saveData(array('value2'=>LavuJson::json_encode($this->history)));
		}

		// save data to the account's db row in the config table
		private function saveData($a_values) {
			$s_query_string = "UPDATE `[database]`.`config` ".$this->dbapi->arrayToUpdateClause($a_values)." WHERE `setting`='lavu_addons' AND `type`='addon'";
			$a_query_vars = array_merge(array('database'=>$this->database), $a_values);
			if ($this->use_mlavu) {
				mlavu_query($s_query_string, $a_query_vars); // use_mlavu
			} else {
				lavu_query(str_replace("`[database]`","",$s_query_string), $a_query_vars);
			}
		}
	}

	class ResourcesAddonsManager {

		// get all addons
		// the addons can have the following properties:
		//     name (required): the name of the addon
		//     requirements (required): what is required in order to enable this adddon (eg, modules), associated with the following properties
		//         modules: names of the required modules
		//     monthly_part: the monthly part to create upon enabling
		//     has_trial: TRUE or FALSE
		//     trial_end: the last date on which the trial is allowed, if the account wants to enable the trial
		//     draw (required): TRUE if the addon is still available, FALSE otherwise
		//     max_enable_times: the maximum number of times that the addon can be disabled within a time period
		//         example: array('since'=>'-3 months', 'max'=>2)
		public static function getAllAddons() {

			// check if already set
			global $o_resourcesAddonsManager;
			if (isset($o_resourcesAddonsManager->allAddons))
				return $o_resourcesAddonsManager->allAddons;

			// create the representative object
			$o_retval = new stdClass();
			$o_retval->routing = new LavuAddon('routing', (object)array(
				'name'=>'Routing',
				'requirements'=>array('modules'),
				'modules'=>array('packages.gold'),
				'monthly_part'=>array('description'=>'Routing', 'amount'=>10, 'pro_rate'=>1, 'mp_end_day'=>''),
				'has_trial'=>TRUE,
				'trial_end'=>date("Y-m-d", strtotime("+1 week")),
				'draw'=>TRUE,
				'max_enable_times'=>array('since'=>'3 months', 'max'=>2)
			));
			$o_resourcesAddonsManager->allAddons = $o_retval;

			return $o_retval;
		}

		// get the addons available to the account
		// @$s_filter: 'draw' means filter out addons that shouldn't be drawn,
		//     'all' means not to filter
		// @$o_modules: the account's modules object can be passed in, here,
		//      if the global modules object shouldn't be used
		// @$s_dataname: the account to load via mlavu query, accounts lavu_query otherwise
		// @return: an object containing all addons available to the account, including
		//     the value "trial_status"=array("available"=>TRUE/FALSE, "in_trial"=>TRUE/FALSE, "days_left"=>integer)
		public static function getAvailableAddons($s_filter = 'draw', $o_modules = NULL, $s_dataname = NULL) {

			global $modules;
			global $o_resourcesAddonsManager;

			// check for the modules object
			if ($o_modules === NULL)
				$o_modules = $modules;

			// check if already set
			if (isset($o_resourcesAddonsManager->availableAddons) &&
			    $o_resourcesAddonsManager->availableAddons->filter == $s_filter &&
			    $o_resourcesAddonsManager->availableAddons->modules = $o_modules)
				return $o_resourcesAddonsManager->availableAddons->addons;

			// get all addons and compare their triggers
			$o_all_addons = self::getAllAddons();
			$o_current_addons = new stdClass();
			foreach($o_all_addons as $k=>$o_addon) {

				// check if the account meets the basic requirements
				if (!$o_addon->meetsRequirements($o_modules))
					continue;

				// check if the filter matches
				if (!$o_addon->meetsFilter($s_filter))
					continue;

				$o_current_addons->$k = $o_addon;
				$index++;
			}

			// get the trial statuses
			self::getTrialStatuses($o_current_addons, $s_dataname);

			if (!isset($o_resourcesAddonsManager->availableAddons))
				$o_resourcesAddonsManager->availableAddons = stdClass();

			$o_resourcesAddonsManager->availableAddons->addons = $o_current_addons;
			return $o_current_addons;
		}

		// helper function of getAvailableAddons
		// loads the trial status info from the database and applies it to the addons
		// @$o_current_addons: the addons to be processed
		// @$s_dataname: the dataname of account, can be NULL for the current account
		public static function getTrialStatus($o_current_addons, $s_dataname) {
			$s_database = "poslavu_{$s_dataname}_db";
			$b_use_mlavu = ($s_dataname === NULL) ? FALSE : TRUE;
			$a_trials = ConnectionHub::getConn('poslavu')->getDBAPI()->getAllInTable('config', array('setting'=>'trial_status', 'type'=>'addon'), $b_use_mlavu, array('databasename'=>$s_database, 'selectclause'=>'`value` AS `addon_id`, `value2` AS `start_date`, `value4` AS `end_date`, `value5` AS `available`, `value6` AS `in_trial`, `value7` AS `days_left`'));
			if (count($a_trials) > 0)
				self::evaluateTrials($o_current_addons, $a_trials, $s_database, $b_use_mlavu);
		}

		// helper function of getAvailableAddons
		// processes addons and attaches trial information to them
		// @$o_addons: an object of addon objects
		// @$a_trials: an array of trial information, loaded form the config table
		// @$s_database: the database name to save changes to
		// @$b_use_mlavu: TRUE/FALSE to use mlavu query to make changes
		public static function evaluateTrials($o_addons, $a_trials, $s_database, $b_use_mlavu) {

			// index the trials
			foreach($a_trials as $a_trial)
				$a_trials[$a_trial['addon_id']] = (object)$a_trial;

			// process trial information and attach it to the addon
			foreach($o_addons as $o_addon) {

				// initialize some values
				$b_available = FALSE;
				$b_in_trial = FALSE;
				$i_days_left = 0;

				// get the trial and process it
				$o_trial = $a_trials[$o_addon->getID()];
				$b_available = ((int)$o_trial->available == 1) ? TRUE : FALSE;
				$b_in_trial = ((int)$o_trial->in_trial == 1) ? TRUE : FALSE;
				if ($b_in_trial) {
					$i_days_left = self::calculateDaysDifference(date("Y-m-d"), $o_trial->end_date);
					if ($i_days_left != (int)$o_trial->days_left) {
						$s_query_string = "UPDATE `[database]`.`config` SET `value7` AS `[days_left]` WHERE `setting`='trial_status' AND `type`='addon' AND `value`='[addon_id]'";
						$a_query_vars = array('database'=>$s_database, 'days_left'=>$i_days_left, 'addon_id'=>$o_trial->addon_id);
						if ($b_use_mlavu)
							mlavu_query($s_query_string, $a_query_vars); // use_mlavu
						else
							lavu_query(str_replace("`[database]`.", "", $s_query_string), $a_query_vars);
					}

					// apply the trial to the addon
					$o_addon->trial_status = new stdClass();
					$o_addon->trial_status->available = $b_available;
					$o_addon->trial_status->in_trial = $b_in_trial;
					$o_addon->trial_status->days_left = $i_days_left;
				}
			}
		}

		// gets the addon object of every addon a account's account has
		// @$s_dataname: the account to load via mlavu query, accounts lavu_query otherwise
		// @return: an object containing all account addons
		public static function getAccountAddons($s_dataname = NULL) {

			// check for previous values
			global $o_resourcesAddonsManager;
			if (isset($o_resourcesAddonsManager->accountAddons) && $o_resourcesAddonsManager->accountAddons->dataname === $s_dataname)
				return $o_resourcesAddonsManager->accountAddons->addons;

			// get the account addons manager and the addons from it
			$o_accountAddonsManager = new AccountAddonsManager($s_dataname);
			$o_resourcesAddonsManager->accountAddons->addons = $o_accountAddonsManager->getAllAddons();
			unset($o_accountAddonsManager);

			$o_resourcesAddonsManager->accountAddons->dataname = $s_dataname;
			return $o_resourcesAddonsManager->accountAddons->addons;
		}

		// get the number of days between $s_date2 and $s_date1
		// if the dates are the same, returns 0
		// @return: 0 if $s_date2 <= $s_date1, or the number of days between
		public static function calculateDaysDifference($s_date1, $s_date2) {
			$i_date1 = strtotime($s_date1);
			$i_date2 = strtotime($s_date2);
			$i_days = 0;
			while ($i_date1 < $i_date2) {
				$i_date1 = strtotime("+1 days", $i_date1);
				$i_days++;
			}
			return $i_days;
		}
	}

	class LavuAddon {
		function __construct($id, $o_properties) {
			$this->properties = $o_properties;
			$this->id = $id;
		}

		public function getID() {
			return $this->id;
		}

		// checks if the account meets the basic requirements to enable an addon
		// @$o_modules: the account's modules object
		// @return: TRUE if all requirements are met, FALSE otherwise
		public function meetsRequirements($o_modules) {

			// initiate some variables
			$b_meets_requirements = TRUE;

			// check that each of this module's requirements match
			for($i = 0; $i < count($this->properties->requirements); $i++) {
				$s_requirement = $this->properties->requirements[$i];
				$b_has_required = TRUE;
				switch($s_requirement) {

				// check that it has the required modules
				case 'modules':
					foreach($o_addon->modules as $s_module) {
						if (!$o_modules->hasModule($s_module))
							$b_has_required = FALSE;
					}
					break;
				}

				// check that the requirement was met
				if (!$b_has_required) {
					$b_meets_requirements = FALSE;
					break;
				}
			}
		}

		// checks if the addon meets the given filter
		// @$s_filter: one of the filters in the following switch statement
		// @return: TRUE if it meets it, FALSE otherwise
		public function meetsFilter($s_filter) {
			switch($s_filter) {
			case 'draw':
				if ($this->properties->draw)
					return TRUE;
			default:
				return TRUE;
			}
			return FALSE;
		}

		// checks if the addon has been enabled too many times in the past x time
		// @return: TRUE or FALSE
		public function enabledTooManyTimes($o_history) {
			$id = $this->id;

			// check that this addon has such a property
			if (!isset($this->properties->max_enable_times))
				return FALSE;

			// gather how many times it's been enabled, recently
			if (!isset($o_history->$id) || !isset($o_history->$id->enabled))
				return FALSE;
			$o_enabled_history = $o_history->$id->enabled;
			$i_enabled_times = 0;
			$i_since = strtotime($this->properties->max_enable_times->since);
			foreach($o_enabled_history as $s_time=>$o_props) {
				if (strtotime($s_time) >= $i_since)
					$i_enabled_times++;
			}

			// compare to how many times it can be enabled
			$i_max = $this->properties->max_enable_times->max;
			if ($i_enabled_times >= $i_max)
				return TRUE;
			return FALSE;
		}
	}

?>
