<?php

require_once(__DIR__."/lavuquery.php");

function sec_dberror() {
	return ConnectionHub::getConn('quickbooks')->error();
}

function sec_query($queryStr, $arg1=null){
	$conn = ConnectionHub::getConn('quickbooks');
	if(!empty($arg1) && is_array($arg1)){
		$funcArgs = $arg1;
	}else{
		$funcArgs = func_get_args();
		unset($funcArgs[0]);
	}
	foreach($funcArgs as $key => $val){
		$queryStr = str_replace('['.$key.']', $conn->escapeString($val), $queryStr);
	}
	return $conn->autoquery($queryStr);
}

class QuickbooksDBConnector extends DBConnector { }

ConnectionHub::addConn('quickbooks', new QuickbooksDBConnector());


