<?php
	ini_set('display_errors','1');
	ini_set('memory_limit','64M');
	require_once(dirname(__FILE__ ) . '/resources/core_functions.php');
	require_once(dirname(__FILE__ ) . '/resources/json.php');


	$date_threshold = date('Y-m-d h:i:s', mktime(0, 0, 0, date('m'), date('d')-7, date('Y')));
	$date_start = '2014-01-01 00:00:00';
	$date_end = '2014-03-25 24:00:00';

	$restaurants = mlavu_query("SELECT `data_name` FROM `poslavu_MAIN_db`.`restaurants` WHERE `disabled`='0' AND `dev`='0' AND `last_activity` >= '{$date_start}'");

	$data_names = array();
	while($data_names[] = mysqli_fetch_assoc($restaurants));
	array_pop($data_names);
	mysqli_free_result($restaurants);
	$results = array();  // gateway - Swipper Name - Count

	foreach($data_names as $k => $dataname){
		$dataname = $dataname['data_name'];
		$query_str = "SELECT DATE_FORMAT(`datetime`,'%Y-%m-%d') as `date`, `gateway`, COUNT(*) as `count` FROM `poslavu_{$dataname}_db`.`cc_transactions` WHERE `datetime` BETWEEN '{$date_start}' AND '{$date_end}' AND `pay_type` = 'Card' AND `voided`='0' GROUP BY DATE_FORMAT(`datetime`,'%Y-%m-%d')";
		$transaction_data = mlavu_query($query_str);
		if(!$transaction_data){
			continue;
		}
		while( $result = mysqli_fetch_assoc($transaction_data) ){
			$gateway = $result['gateway'];
			$date = $result['date'];
			$count = $result['count'];
			// if(!isset($results[$dataname])){
			// 	$results[$dataname] = array();
			// }
			if(!isset($results[$gateway])){
				$results[$gateway] = array();
			}

			if(!isset($results[$gateway][$date])){
				$results[$gateway][$date] = $count;
			} else {
				$results[$gateway][$date] += $count;
			}
			unset( $gateway );
			unset( $date );
			unset( $count );
			unset( $result );
		}
		mysqli_free_result($transaction_data);
	}

echo LavuJson::json_encode($results);

	/*$restaurants = mlavu_query("SELECT `data_name` FROM `poslavu_MAIN_db`.`restaurants` WHERE `disabled`='0' AND `dev`='0' AND `last_activity` >='{$date_threshold}' ");

	$data_names = array();
	while($data_names[] = mysqli_fetch_assoc($restaurants));
	array_pop($data_names);
	mysqli_free_result($restaurants);
	$results = array();  // gateway - location count

	foreach($data_names as $k => $dataname){
		$dataname = $dataname['data_name'];
		$query_str = "SELECT `gateway` FROM `poslavu_{$dataname}_db`.`locations` WHERE `_disabled`=0";
		$location_data = mlavu_query($query_str);
		if(!$location_data){
			continue;
		}
		while( $result = mysqli_fetch_assoc($location_data) ){
			$gateway = $result['gateway'];
			if(!isset($results[$gateway])){
				$results[$gateway] = 0;
			}
			$results[$gateway]++;
		}
		mysqli_free_result($location_data);
	}

echo LavuJson::json_encode($results);*/