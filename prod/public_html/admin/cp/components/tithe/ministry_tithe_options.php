<?php
	ini_set("display_errors",1);
	require_once($_SERVER['DOCUMENT_ROOT']."/cp/resources/core_functions.php");
	echo "
	<script type ='text/javascript' src ='components/tithe/ministries_js.js'> </script>
	<script type='text/javascript'> 
		var dn = '".admin_info('dataname')."'; 
		var loc_id = '".$location_info['id']."'; 
		var key ='lavu_tithe_awesome'; 
		window.onload= function(){
			initialize_amount_page();
			initialize_amount_listeners();
		}
	</script>";
	$speak = 'speak';
	echo <<<COWS
	<div class='edit_tithe_amounts'>
		<br>
		<div class='add_payment_amt_btn'>Add Payment Amount</div>
		<br><br>
		<div class= 'amount_container'>
			<ul class= 'amounts_sortable'>
			</ul>
			<br>
			<input type='button' class ='save_tithe_amounts_btn' value="{$speak('Save')}" />
		</div>

	</div>
	<script type='text/javascript'>
	</script>
COWS;
?>