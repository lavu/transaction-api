<?php

function get_ministries($data_name = '',$loc_id = ''){
	if(empty($data_name) || empty($loc_id)){
		error_log(__FUNCTION__.'Lacking Information');
		return;
	}
	lavu_connect_dn($data_name, "poslavu_".$data_name."_db");
	$query = lavu_query("SELECT * FROM `config` where `setting`='lavu_tithe_id' and `_deleted` !='1' and `location`='[1]'", $loc_id);
	$return_arr = array();
	if(mysqli_num_rows($query)){
		$tithe_id = mysqli_fetch_assoc($query);
		$ministry_query = lavu_query("SELECT * FROM `menu_items` where `active` = 1 AND `_deleted` !='1' and `menu_id`='[1]' ", $tithe_id['value']);
		while($ministries = mysqli_fetch_assoc($ministry_query)){
			$return_arr[] = $ministries;
		}
	}else{
		$return_arr[] = array();
	}
	return $return_arr;
}
