 	var URL = "/cp/components/tithe/edit_ministries_data.php";
 	/*
	function initialize_ministry_page(){
		var ministries = performAJAX(URL, "function=init_ministries", false, "POST");
		console.log(ministries);
	}*/

	function initialize_ministry_page(){
		var ministries = performAJAX("/cp/components/tithe/edit_ministries_data.php", "function=init_ministries", false, "POST");
		for( var i=0; i< ministries.length; i++)
			add_new_ministry(ministries[i].name,ministries[i].active, ministries[i].id);
	}
	function initialize_amount_page(){
		var amounts = performAJAX("/cp/components/tithe/edit_ministries_data.php", "function=init_tithe_amounts", false, "POST");
		if(amounts.value){
			var amt = JSON.parse(amounts.value);
			if( amt.length > 4)
				amt= amt.slice(0,4);
			for( var i=0; i< amt.length; i++)
				add_new_amount(amt[i].tithe_amt, amt[i].status);
		}
	}
	function initialize_ministry_listeners(){
		var add_ministry_btn = document.getElementsByClassName("add_ministry_btn")[0];
			add_ministry_btn.addEventListener("click", add_new_ministry);

		var save_ministries_btn = document.getElementsByClassName("save_ministries_btn")[0];
			save_ministries_btn.addEventListener("click",save_ministries);
	}
	function initialize_amount_listeners(){
		var add_amts_btn = document.getElementsByClassName("add_payment_amt_btn")[0];
			add_amts_btn.addEventListener("click", add_new_amount);

		var save_tithe_amts_btn = document.getElementsByClassName("save_tithe_amounts_btn")[0];
			save_tithe_amts_btn.addEventListener("click",save_tithe_amts);
	}
	function add_new_amount(value, status){
		//console.log(value);
		//console.log(status);
		var container = document.getElementsByClassName("amounts_sortable")[0];
		
		if (typeof status === 'undefined') {
			var kidCount = countTheKids(container);
			if (kidCount >= 4) {
				alert("The current limit for amount definitions is 4.");
				return;
			}
		}
		
		var row = document.createElement("li");
			row.className = "amount_row";

		var amt_input = document.createElement("input");
			amt_input.setAttribute("type","text");
			amt_input.className = "amount_input";
			amt_input.setAttribute("placeholder","New Tithe Amount");
			if(value && value.length)
				amt_input.value= value;

		var amt_container = document.createElement("ministry_name_container");
			amt_container.className = "amount_container";
			amt_container.appendChild(amt_input);

		var amt_status = document.createElement("div");
			amt_status.className = "amt_status_contaner";

		var amt_status_select = document.createElement("select");
			amt_status_select.className ="amt_status";
		
		var opt1 = document.createElement("option");
			opt1.value = "";
			opt1.innerHTML ="Select a Status";

		var opt2 = document.createElement("option");
			opt2.value = "0";
			opt2.innerHTML ="disabled";
			if(status && status == "0")
				opt2.selected=true;

		var opt3 = document.createElement("option");
			opt3.value 	   = "1";
			opt3.innerHTML = "enabled";
			if(status && status == "1")
				opt3.selected=true;

		amt_status_select.appendChild(opt1);
		amt_status_select.appendChild(opt2);
		amt_status_select.appendChild(opt3);
		row.appendChild(amt_container);
		row.appendChild(amt_status_select);
		//row.appendChild(hidden_id);
		container.appendChild(row);
		$(container).sortable();
	}
	
	function countTheKids(parent) {
		var realKids = 0;
		var kids = parent.childNodes.length;
		var i = 0;
		while(i < kids){
			if (parent.childNodes[i].nodeType != 3) {
				realKids++;
			}
			i++;
		}
		
		return realKids;
	}
	
	function add_new_ministry(name, status, id){
		var container = document.getElementsByClassName("ministries_sortable")[0];
		
		if (typeof id === 'undefined') {
			var kidCount = countTheKids(container);
			if (kidCount >= 10) {
				alert("The current limit for ministry definitions is 10.");
				return;
			}
		}
		
		var row = document.createElement("li");
			row.className = "ministry_row";

		var min_name = document.createElement("input");
			min_name.setAttribute("type","text");
			min_name.className = "ministry_name_input";
			min_name.setAttribute("placeholder","New Ministry Name");
			if(name && name.length)
				min_name.value= name;

		var min_name_container = document.createElement("ministry_name_container");
			min_name_container.className = "ministry_name_container";
			min_name_container.appendChild(min_name);

		var min_status = document.createElement("div");
			min_status.className = "min_status_contaner";

		var min_status_select = document.createElement("select");
			min_status_select.className ="ministry_status";
		var opt1 = document.createElement("option");
			opt1.value = "";
			opt1.innerHTML ="Select a Status";

		var opt2 = document.createElement("option");
			opt2.value = "0";
			opt2.innerHTML ="disabled";
			if(status && status == "0")
				opt2.selected=true;

		var opt3 = document.createElement("option");
			opt3.value 	   = "1";
			opt3.innerHTML = "enabled";
			if(status && status == "1")
				opt3.selected=true;

		var hidden_id = document.createElement("input");
			hidden_id.setAttribute("type", "hidden");
			hidden_id.className = "ministry_id";
		if(id)
			hidden_id.value = id;

		min_status_select.appendChild(opt1);
		min_status_select.appendChild(opt2);
		min_status_select.appendChild(opt3);
		/*
		var save_btn = document.createElement("input");
			save_btn.setAttribute("type","button");
			save_btn.value = "Save";
			save_btn.addEventListener("click",save_ministries);*/
		row.appendChild(min_name_container);
		row.appendChild(min_status_select);
		row.appendChild(hidden_id);
	
		
		container.appendChild(row);
		$(container).sortable();
	}
	function save_ministries(event){
		var elem = event.target;
		var rows = document.getElementsByClassName("ministry_row");
		var save_obj = [];
		for(var i =0; i < rows.length; i++){
			var name = rows[i].getElementsByClassName("ministry_name_input")[0].value;
			var status = rows[i].getElementsByClassName("ministry_status")[0].value;
			var id = rows[i].getElementsByClassName("ministry_id")[0].value;
			save_obj.push({"ministry_name" : name,"status":status, "ministry_id":id});
		}
		//console.log(JSON.stringify(save_obj));

		performAJAX("/cp/components/tithe/edit_ministries_data.php", "function=save_ministries&ministries="+encodeURIComponent(JSON.stringify(save_obj)), false, "POST");
	}
	function save_tithe_amts(){

		var elem = event.target;
		var rows = document.getElementsByClassName("amount_row");
		var save_obj = [];
		for(var i =0; i < rows.length; i++){
			var amt = rows[i].getElementsByClassName("amount_input")[0].value;
			var status = rows[i].getElementsByClassName("amt_status")[0].value;
			save_obj.push({"tithe_amt" : amt,"status":status});
		}
		//console.log(save_obj);
		performAJAX("/cp/components/tithe/edit_ministries_data.php", "function=save_tithe_amts&tithe_amts="+encodeURIComponent(JSON.stringify(save_obj)), false, "POST");
	}

	function performAJAX(URL, urlString,asyncSetting,type ){
		urlString =urlString+"&data_name="+dn+"&loc_id="+loc_id+"&key="+key;
		var returnVal='';
		//loading();
		$.ajax({
			url: URL,
			type: type,
			data:urlString,
			async:asyncSetting,
			success: function (string){
					try{
						returnVal = JSON.parse(string);
						console.log(returnVal);
					}catch(e){
						returnVal= string;
						console.log(returnVal);
					}
				}
		});
		//doneLoading();
		return returnVal;
	}
