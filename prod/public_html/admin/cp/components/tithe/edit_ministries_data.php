<?php
	ini_set("display_errors",1);
	require_once($_SERVER['DOCUMENT_ROOT']."/cp/resources/lavuquery.php");
	require_once($_SERVER['DOCUMENT_ROOT']."/cp/resources/json.php");
	
	$data_name = $_POST['data_name'];
	//echo print_r($_POST,1);
	if ( isset($_POST['function'])){
		if (function_exists($_POST['function'])){
			if(isset($_POST['data_name']) && isset($_POST['loc_id']) && isset($_POST['key'])){
				lavu_connect_dn($_POST['data_name'], "poslavu_".$_POST['data_name']."_db");

				if( $_POST['key']=='lavu_tithe_awesome'){
				//if(mysqli_num_rows(lavu_query("select * from `locations` where `api_key`='[1]'", urldecode($_POST['key'])))){
					$fun_name = $_POST['function'];
					unset($_POST['function']);
					$fun_name($_POST);
				}else{
					echo '{"error":"invalid key: '.$_POST['key'].'"}';
				}
			}else{
				echo '{"error":"credentials not sent. "}';
			}
		}else
			echo '{"error":"no function of name: '. $_POST['function'].'"}';
	}else
		echo '{"error": "no function name given."}';

	function init_ministries($args){
		//echo print_r($args,1);
		$query = lavu_query("SELECT * FROM `config` where `setting`='lavu_tithe_id' and `_deleted` !='1' and `location`='[1]'", $args['loc_id']);
		$return_arr = array();
		if(mysqli_num_rows($query)){
			$tithe_id = mysqli_fetch_assoc($query);
			
			$ministry_query = lavu_query("SELECT * FROM `menu_items` where  `_deleted` !='1' and `menu_id`='[1]' order by `_order` asc ", $tithe_id['value']);
			while($ministries = mysqli_fetch_assoc($ministry_query)){
				$return_arr[] = $ministries;
			}
		}else{
			$return_arr[] = array();
		}
		if(1 != $args['no_print']){
			echo LavuJson::json_encode($return_arr);
		}
		return $return_arr;
	}
	function init_tithe_amounts($args){
		if( mysqli_num_rows(lavu_query("SELECT * FROM `config` where `setting`='lavu_tithe_amts' and `type`='location_config_setting' and `_deleted` !='1' and `location`='[1]'", $args['loc_id'])))
			$res = mysqli_fetch_assoc(lavu_query("SELECT * FROM `config` where `setting`='lavu_tithe_amts' and `type`='location_config_setting' and `_deleted` !='1' and `location`='[1]'", $args['loc_id']));
		else
			$res= array();
		echo LavuJson::json_encode($res);
	}
	function save_ministries($args){
		$query = lavu_query("SELECT * FROM `config` where `setting`='lavu_tithe_id' and `_deleted` !='1' and `location`='[1]'", $args['loc_id']);
		$tithe_id =0;
		if(mysqli_num_rows($query)){
			$tithe_id_arr =mysqli_fetch_assoc($query);
			$tithe_id = $tithe_id_arr['value'];
		}else{
			$cur_menu_id= mysqli_fetch_assoc(lavu_query("select MAX(`id`) as `id` from `menus`"));
			$tithe_id = $cur_menu_id['id']+1;
			lavu_query("INSERT INTO `config` (`setting`, `value`, `location`) VALUES ('lavu_tithe_id', '[1]','[2]') ", $tithe_id, $args['loc_id']);
		}
		$ministries = LavuJson::json_decode($args['ministries']);
		$counter =1;
		foreach($ministries as $ministry){ 
			if(isset($ministry['ministry_id']) && mysqli_num_rows(lavu_query("SELECT * FROM `menu_items` where `id` = '[1]' and `menu_id`='[2]'", $ministry['ministry_id'], $tithe_id)))
				lavu_query("UPDATE `menu_items` SET `_order` ='[1]', `name`='[2]', `active`='[3]' where `id`='[4]'", $counter,$ministry['ministry_name'], $ministry['status'],$ministry['ministry_id']);
			else{
				
				lavu_query("INSERT INTO `menu_items` (`name`, `_order`, `active`,`menu_id`) VALUES ('[1]','[2]','[3]','[4]')", $ministry['ministry_name'], $counter, $ministry['status'], $tithe_id);
			
			}

			$counter++;
		}
		if(lavu_dberror()){
			echo '{"mysql_error: ":"'.lavu_dberror().'"}';	
		}else{
			echo '{"status":"success"}';
		}
		//echo print_r(LavuJson::json_decode($args['ministries']),1);
	}
	function make_ministry_buttons($args)
	{
		$args['no_print'] = 1;
		$ministrysList = init_ministries($args);
		$ministryButton = '<button class="MinistryButton" type="button" onclick=("MinistryClick([ministryID])")>[ministryName]</button><br>';
		$retString = '';
		foreach ($ministrysList as $item){
			$tempString = $ministryButton;
			$tempString = str_replace('[ministryID]',$item['id'],$tempString);
			$tempString = str_replace('[ministryName]',$item['name'],$tempString);
			$retString .= $tempString;
		}
		echo $retString;
		return $retString;
	}
	function save_tithe_amts($args){
		$val = $args['tithe_amts'];
		if( mysqli_num_rows(lavu_query("SELECT * FROM `config` where `setting`='lavu_tithe_amts' and `type`='location_config_setting' and `location`='[1]'", $args['loc_id'])))
			lavu_query("DELETE FROM `config` where `setting`='lavu_tithe_amts' and `type`='location_config_setting' and `location`='[1]' limit 1", $args['loc_id']);

		lavu_query("INSERT INTO `config` (`setting`,`value`,`type`,`location`) VALUES ('lavu_tithe_amts', '[1]', 'location_config_setting', '[2]')", $args['tithe_amts'], $args['loc_id']);
		
		if(lavu_dberror())
			echo lavu_dberror();
		else
			echo '{"status":"success"}';
	}

?>
