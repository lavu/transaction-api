<?php
	ini_set("display_errors",1);
	require_once($_SERVER['DOCUMENT_ROOT']."/cp/resources/core_functions.php");
	echo "
	<script type ='text/javascript' src ='components/tithe/ministries_js.js'> </script>
	<script type='text/javascript'> 
		var dn = '".admin_info('dataname')."'; 
		var loc_id = '".$location_info['id']."'; 
		var key ='lavu_tithe_awesome'; 
		window.onload= function(){
			initialize_ministry_page();
			initialize_ministry_listeners();
			$( '.ministries_sortable' ).sortable();
    		$('.ministries_sortable' ).disableSelection();

		}
	</script>";
	
	$speak = 'speak';
	echo <<<COWS
	<div class='edit_ministries'>
		<br>
		<div class='add_ministry_btn'>{$speak('Add New Ministry')} </div>
		<br><br>
		<div class= 'ministry_container'>
			<ul class= 'ministries_sortable'>
			</ul>
			<br>
			<input type='button' class ='save_ministries_btn' value="{$speak('Save')}" />
		</div>

	</div>
	<script type='text/javascript'>
	</script>
COWS;
?>