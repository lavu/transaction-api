<?php
	if($in_lavu)
	{
		display_header("Group Event Checklist");
		echo "<br><br>";
		
		$tablename = "config";
		$forward_to = "index.php?mode={$section}_{$mode}";
		$filter_by = "`location`='$locationid' and `setting`='Group Event Checklist' and `type`='collection'";
		$back_to_title = "<< Back to Checklist";
		
		$fields = array();
		$fields[] = array(speak("Title"),"value","text","","list:yes");
		$fields[] = array("Setting","setting","hidden","","setvalue:Group Event Checklist");
		$fields[] = array("Type","type","hidden","","setvalue:collection");
		$fields[] = array("Location","location","hidden","","setvalue:$locationid");
		$fields[] = array(speak("Submit"),"submit","submit");

		require_once(resource_path() . "/browse.php");
	}
?>
