<form name='display_save' action='https://admin.poslavu.com/cp/index.php?mode=display_settings' method='post' enctype="multipart/form-data">
	<table style='border:1px solid black'>
		<tr>
			<th colspan='2' style='background-color:#aecd37'>Kart Display Settings</th>
		</tr>
		<tr>
			<td>Use Advertisements</td>
			<td><input type='checkbox' value='1'  name='use_ads' id= 'use_ads'></td>
		</tr>
		<tr>	
			<td>Advertisement 1</td><td><div id='p2r_ad_1'> </div><input type='file' name='p2r_ad_1'></td>
		</tr>
		<tr>	
			<td>Advertisement 2</td><td><div id='p2r_ad_2'> </div><input type='file' name='p2r_ad_2'></td>
		</tr>
		<tr>	
			<td>Advertisement 3</td><td><div id='p2r_ad_3'> </div><input type='file' name='p2r_ad_3'></td>
		</tr>
		<tr>	
			<td>Advertisement 4</td><td><div id='p2r_ad_4'> </div><input type='file' name='p2r_ad_4'></td>
		</tr>
		<tr>
			<td> Seconds between race end and Advertisement Start </td>
			<td>
				<select id="p2r_display_seconds_between" name="p2r_display_seconds_between">
					<option value="5">    5 seconds</option>
					<option value="10">  10 seconds</option>
					<option value="30">  30 seconds</option>
					<option value="60">  60 seconds</option>
					<option value="90">  90 seconds</option>
					<option value="180">180 seconds</option>
				</select>
			</td>
		</tr>
		<tr>
			<td>Advertisement Duration</td>
			<td>
				<select id="p2r_display_duration" name="p2r_display_duration">
					<option value="1">   1 second</option>
					<option value="2">  2 seconds</option>
					<option value="5">  5 seconds</option>
					<option value="10">10 seconds</option>
					<option value="15">15 seconds</option>
					<option value="30">30 seconds</option>
				</select>
			</td>
		</tr>
		<tr>
			<td><input type='hidden' value='posted' name='posted'></td>
			<td><input type='submit' value='save'></td>
		</tr>
	</table>
</form>
<?php
	if(isset($_POST['posted'])){
		
		if(!isset($_POST['use_ads']))
			$_POST['use_ads']="0";
		
		//error_log("posted: ". print_r($_POST,1));
		form_submitted();
		lavu_query("DELETE FROM `config` WHERE `setting`='p2r_display_seconds_between' LIMIT 1");
 		lavu_query("DELETE FROM `config` WHERE `setting`='p2r_display_duration' LIMIT 1");
 		lavu_query("DELETE FROM `config` WHERE `setting`='use_ads' LIMIT 1");
 		
 		lavu_query("INSERT INTO `config` (`setting`,`value`) VALUES ('p2r_display_seconds_between','[1]')",$_POST['p2r_display_seconds_between']);
 		lavu_query("INSERT INTO `config` (`setting`,`value`) VALUES ('p2r_display_duration','[1]')",$_POST['p2r_display_duration']);
 		//error_log('here use ads is: '. $_POST['use_ads']);
 		lavu_query("INSERT INTO `config` (`setting`,`value`) VALUES ('use_ads','[1]')",$_POST['use_ads']);
 		if (lavu_dberror())
 			echo lavu_dberror();
 			
 		echo "<script type='text/javascript'> 
				document.getElementById('p2r_display_duration').value='{$_POST['p2r_display_duration']}';
				document.getElementById('p2r_display_seconds_between').value='{$_POST['p2r_display_seconds_between']}';";
		if( $_POST['use_ads']==1)
			echo "document.getElementById('use_ads').setAttribute('checked');";
		else
			echo "document.getElementById('use_ads').removeAttribute('checked');";
				
		$counter=1;
		$q_arr= array();
		$query3 = lavu_query("SELECT * FROM `config` WHERE `setting`='p2r_ad_1'");
		$query4 = lavu_query("SELECT * FROM `config` WHERE `setting`='p2r_ad_2'");
		$query5 = lavu_query("SELECT * FROM `config` WHERE `setting`='p2r_ad_3'");
		$query6 = lavu_query("SELECT * FROM `config` WHERE `setting`='p2r_ad_4'");
		if( mysqli_num_rows($query3))
			$q_arr[] = mysqli_fetch_assoc($query3);
		if( mysqli_num_rows($query4))
			$q_arr[] = mysqli_fetch_assoc($query4);
		if( mysqli_num_rows($query5))
			$q_arr[] = mysqli_fetch_assoc($query5);
		if( mysqli_num_rows($query6))
			$q_arr[] = mysqli_fetch_assoc($query6);
		foreach($q_arr as $res ){

				echo "document.getElementById('p2r_ad_$counter').style.backgroundImage=\"url('https://admin.poslavu.com/images/".admin_info('dataname')."/ad_images/{$res['value']}')\";\n";
				echo "document.getElementById('p2r_ad_$counter').style.display=\"block\";\n";
				echo "document.getElementById('p2r_ad_$counter').style.backgroundSize= \"100px, 200px\"\n";
				echo "document.getElementById('p2r_ad_$counter').style.backgroundRepeat=\"no-repeat\"\n";
				echo "document.getElementById('p2r_ad_$counter').style.width=\"200px\"\n";
				echo "document.getElementById('p2r_ad_$counter').style.height=\"100px\"\n";
				$counter++;
		}
		echo"</script>";

	}else{
		$query1 = lavu_query("SELECT * FROM `config` WHERE `setting`='p2r_display_seconds_between'");
		$query2 = lavu_query("SELECT * FROM `config` WHERE `setting`='p2r_display_duration'");
		$query3 = lavu_query("SELECT * FROM `config` WHERE `setting`='p2r_ad_1'");
		$query4 = lavu_query("SELECT * FROM `config` WHERE `setting`='p2r_ad_2'");
		$query5 = lavu_query("SELECT * FROM `config` WHERE `setting`='p2r_ad_3'");
		$query6 = lavu_query("SELECT * FROM `config` WHERE `setting`='p2r_ad_4'");
		$query7 = lavu_query("SELECT * FROM `config` WHERE `setting`='use_ads' ");
		$q_arr=array();
		if( mysqli_num_rows($query1))
			$result1 = mysqli_fetch_assoc($query1);
		if( mysqli_num_rows($query2))
			$result2 = mysqli_fetch_assoc($query2);
		if( mysqli_num_rows($query3))
			$q_arr[] = mysqli_fetch_assoc($query3);
		if( mysqli_num_rows($query4))
			$q_arr[] = mysqli_fetch_assoc($query4);
		if( mysqli_num_rows($query5))
			$q_arr[] = mysqli_fetch_assoc($query5);
		if( mysqli_num_rows($query6))
			$q_arr[] = mysqli_fetch_assoc($query6);
		if( mysqli_num_rows($query7))
			$result7 = mysqli_fetch_assoc($query7);

		echo"<script type='text/javascript'> 
				document.getElementById('p2r_display_duration').value='{$result2['value']}';\n
				document.getElementById('p2r_display_seconds_between').value='{$result1['value']}';\n";
		$counter=1;
		foreach($q_arr as $res ){
			echo "document.getElementById('p2r_ad_$counter').style.backgroundImage=\"url('https://admin.poslavu.com/images/".admin_info('dataname')."/ad_images/{$res['value']}')\";\n";
			echo "document.getElementById('p2r_ad_$counter').style.display=\"block\";\n";
			echo "document.getElementById('p2r_ad_$counter').style.backgroundSize= \"100px, 200px\"\n";
			echo "document.getElementById('p2r_ad_$counter').style.backgroundRepeat=\"no-repeat\"\n";
			echo "document.getElementById('p2r_ad_$counter').style.width=\"200px\"\n";
			echo "document.getElementById('p2r_ad_$counter').style.height=\"100px\"\n";
			$counter++;
		}
		if($result7){
			if($result7['value']==1){
	
				echo "document.getElementById('use_ads').setAttribute('checked')";
			}
		}
		echo"</script>";
		
	}
	function form_submitted(){
		
		foreach($_FILES as $key => $value ){

			$valid_ext = array('jpg','jpeg','gif','png','bmp'); // whitelist of file extensions
			if(basename($_FILES[$key]['name']) != "") {
			
				$base_path = $_SERVER['DOCUMENT_ROOT']. "/images/".admin_info('dataname')."/ad_images/";
			
				if(!in_array(end(explode(".", strtolower(basename($_FILES[$key]['name'])))), $valid_ext)) {
					echo 'Please upload a valid image!'; 
					exit;
				}
				
				$basename = basename($_FILES[$key]['name']);
				$target = $base_path . $basename; // append file to path
				if(move_uploaded_file($_FILES[$key]['tmp_name'], $target)) {
					lavu_query("DELETE FROM `config` WHERE `setting`='[1]' LIMIT 1", $key);
					lavu_query("INSERT INTO `config` (`setting`,`value`) VALUES ('[1]','[2]')",$key,$basename);
				}else{
					echo "There was an error uploading the file, please try again!";
					exit;
				}
			}
		}
	}
?>
