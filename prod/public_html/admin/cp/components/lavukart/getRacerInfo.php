<?php

	if( isset($_POST['signup_date']) && isset($_POST['location']) && isset($_POST['id'])){
		getData($_POST['signup_date'], $_POST['location'], $_POST['id']);
	}else{
		echo "no data sent.";
		exit();
	}

	function getData($createdDate, $location, $id){
		require_once($_SERVER['DOCUMENT_ROOT'].'/cp/resources/core_functions.php');
		lavu_connect_dn($location,'poslavu_'.$location.'_db');
		if( $createdDate)
			$query=lavu_query("select * from `poslavu_[1]_db`.`lk_customers` where `date_created`= '[2]' limit 1", $location, $createdDate);
		else
			$query=lavu_query("select * from `poslavu_[1]_db`.`lk_customers` where `id`= '[2]' limit 1", $location, $id);

		if( mysqli_num_rows($query)){
			$resultArray= mysqli_fetch_assoc($query);
			$encoded= json_encode($resultArray);
			echo $encoded;
		}else
			echo "";
	}
?>