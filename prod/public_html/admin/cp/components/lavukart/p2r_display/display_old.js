var usr_key     = '100';
var images_json = null;
var dataname= 'pole_position_raceway';
window.onload=function(){

	get_tracks();
	debug_access();
	images_json = get_ad_images();

	var vars= getUrlVars();
	if(vars.display){
		if(vars.display=='scoreboard'){
			document.getElementsByClassName('p2r_btn')[0].click();
		}else if(vars.display=='kart_assignment'){
			document.getElementsByClassName('p2r_btn')[1].click();
		}else if( vars.display=='upcoming_races'){
			document.getElementsByClassName('p2r_btn')[2].click();
		}
		if(vars.track){
			$("#track_selector").val(vars.track);
			display_view(document.getElementById("track_selector"));
		}
	}else if(!vars.no_auto_load){
		if(display_type=='scoreboard'){
			document.getElementsByClassName('p2r_btn')[0].click();
		}else if(display_type=='kart_assignment'){
			document.getElementsByClassName('p2r_btn')[1].click();
		}else if( display_type=='upcoming_races'){
			document.getElementsByClassName('p2r_btn')[2].click();
		}
		if(track){
			$("#track_selector").val(track);
			display_view(document.getElementById("track_selector"));
		}
	}
};
function get_tracks(){
	var tracks_json= performAJAX("./get_race_data.php", "function=get_tracks&dataname="+dataname,false,"POST");
	var track_selector=document.getElementById("track_selector");
	for(var i =0; i< tracks_json.length; i++){
		var option   = document.createElement("option");
		option.value = tracks_json[i].id;
		option.text  = tracks_json[i].title;
		try{
			track_selector.add(option,track_selector.options[null]);
		}catch (e){
			track_selector.add(option,null);
		}
	}
}
function get_race_info(obj){

	if(typeof(obj)=='object'){
		var prev_id    = obj.new_id;
		var track_id   = obj.track_id;
		var table_name = obj.table_name;
		var display    = obj.display_type;	
		event_id='';
		if( obj.event_id)
			var event_id="&event_id="+obj.event_id;
		
		var send_str   = "key="+usr_key+"&prev_id="+prev_id+"&dataname="+dataname+"&function=get_updates&track_id="+track_id+"&table_name="+table_name+event_id+"&display_type="+display;
		var result= performAJAX("./get_race_data.php",send_str,true,'POST',"get_race_info");
	}
}
function show_track_selector(elem){
	
	for( var i=0; i<document.getElementsByClassName("p2r_btn").length; i++)
		document.getElementsByClassName("p2r_btn")[i].removeAttribute("selected");
		
	elem.setAttribute("selected", "");
	document.getElementById("track_selector").style.display='table';
	document.getElementById("debug").innerHTML='';
}
function performAJAX(URL, urlString,asyncSetting,type, callback ){
	document.getElementById('debug').innerHTML+=""+urlString+" <br/>";
	var returnVal='';
	$.ajax({ 
		url:   URL,
		type:  type,
		data:  urlString,
		async: asyncSetting,
		success: 
		function (string){
			document.getElementById('debug').innerHTML+=""+string+" <br/>";
			if(check_json(string)){

				var obj= jQuery.parseJSON( string );
				if(obj.error){
					console.log(obj.error);
					return;
				}
				if(callback){
					var fn = window[callback];
					update_data(obj)
					fn(obj);
				}else{
					returnVal= obj;
				}
			}else{
				console.log(string);
			}
		}
	});	
	return returnVal; 
}
function check_json(text){
	
	if (/^[\],:{}\s]*$/.test(text.replace(/\\["\\\/bfnrtu]/g, '@').
	replace(/"[^"\\\n\r]*"|true|false|null|-?\d+(?:\.\d*)?(?:[eE][+\-]?\d+)?/g, ']').
	replace(/(?:^|:|,)(?:\s*\[)+/g, ''))) {
		return true;
	}else{
  		return false;
  	}
}
function debug_access(){
	var konamiCode=[38,38,40,40,37,39,37,39];
	var counter=0;
	$(document).bind("keyup",function(e){			
		
		if(counter==2 && e.keyCode==38){}
			 
		else if(konamiCode[counter]==e.keyCode)
			counter++;
		else
			counter=0;
		
		if(counter==konamiCode.length){
			$("#debug_container").css("display","table");
		}
	});	
}
function display_view(elem){

	for( var i=0; i<document.getElementsByClassName("p2r_btn").length; i++){
			//console.log('{"new_id":-1,"track_id":"'+elem.value+'","table_name":"lk_event_schedules","display_type":"kart_assignment"}');
		if( document.getElementsByClassName("p2r_btn")[i].getAttribute("selected")!=null){
			if(document.getElementsByClassName("p2r_btn")[i].value=='Scoreboard'){
				get_race_info(jQuery.parseJSON('{"new_id":-1,"track_id":"'+elem.value+'","table_name":"lk_laps","display_type":"scoreboard"}'));
				show_page("scoreboard");
			}else if( document.getElementsByClassName("p2r_btn")[i].value=='Kart Assignment'){
				get_race_info(jQuery.parseJSON('{"new_id":-1,"track_id":"'+elem.value+'","table_name":"lk_event_schedules","display_type":"kart_assignment"}'));
				show_page("kart_assignment");
			}else if( document.getElementsByClassName("p2r_btn")[i].value=='Upcoming Races'){
				get_race_info(jQuery.parseJSON('{"new_id":-1,"track_id":"'+elem.value+'","table_name":"lk_event_schedules","display_type":"upcoming_races"}'));
				show_page("upcoming_races");
			}
		}
	}
}
function show_page(area_id){
	document.getElementById(area_id).style.display	        = 'table';
	document.getElementById("main_container").style.display = 'none';
}
function update_data(obj){
	if( obj.results && obj.results.length > 0){
		hide_ads();
		if(obj.display_type=='upcoming_races'){
			console.log( obj.results);
			document.getElementById('upcoming_races').style.display = 'table';
			update_upcoming_races(obj.results);
		}else if( obj.display_type=='kart_assignment'){
				document.getElementById('kart_assignment').style.display = 'table';
				update_kart_assignment(obj.results);
		}else if( obj.display_type=='scoreboard'){
			document.getElementById('scoreboard').style.display = 'table';
			$(".race_name").html("Race Type: "+obj.event_name);
			update_scoreboard(obj.results,obj.num_laps, obj.laps_left);
		}else{
			console.log("No display type sent");
		}
	}else{
		if( typeof p2r_display_seconds_between !="undefined"){
			document.getElementById('kart_assignment').style.display = 'none';
			document.getElementById('upcoming_races').style.display = 'none';
			if(obj.display_type == 'scoreboard')
				setTimeout(function(){display_ads()},p2r_display_seconds_between*1000 );
			else
				display_ads();
			return;
		}
	}

}
function update_upcoming_races(obj){
	$('[id^="upcoming_races_data_"]').each(function (){
		remove_rows('data_row', $(this));
		
	});
	for(var i=0; i < 3; i++)
		document.getElementById("upcoming_races_data_"+i).style.display='none';
	
	var len= 3;
	if( obj.length < 3)
		len=obj.length;
	for(var i=0; i < len; i++){
		var table=document.getElementById("upcoming_races_data_"+i);
		table.style.display='table';
		
		var t_header= document.getElementById("upcoming_race_"+i).innerHTML= obj[i].event_name;
		for( var c=0; c<obj[i].racers.length; c++){
			var row    = table.insertRow(-1);
			var cell1  = row.insertCell(-1);	// this is the kart number
			var cell2  = row.insertCell(-1);	// this is the racer name 
			var c_name = "scoreboard_col";
			
			row.className   = "data_row";
			cell2.className = cell1.className= c_name;
			
			cell1.innerHTML = c+1;
			cell2.innerHTML = obj[i].racers[c];
			
			cell2.style.fontSize='30px';
			cell1.style.fontSize='30px';
			cell1.style.color='red';
		}
	}
}
function update_kart_assignment(obj){
	
	var table=document.getElementById("kart_assignment_data");
	remove_rows('data_row', table);
	for(var i=0; i<obj.length; i++){
		var row    = table.insertRow(-1);
		var cell1  = row.insertCell(-1);	// this is the kart number
		var cell2  = row.insertCell(-1);	// this is the racer name 
		var c_name = "scoreboard_col";
		
		row.className  = "data_row";
		cell2.className=cell1.className= c_name;
		cell1.innerHTML= obj[i].kart_number;
		cell2.innerHTML= obj[i].racer_name;
		cell2.style.fontSize='30px';
		cell1.style.fontSize='30px';
		cell1.style.color='red';
	}
}
function update_scoreboard(obj, tot_laps, laps_left){
	var table=document.getElementById("scoreboard_data");
	var racer_names = [];
	var racers		= {};
	var i=0;
	$(".racer_name").each(function(){
		racer_names[i]= $(this).html();
		i++;
	});
	i=0;
	$(".current_lap").each(function(){
		racers[racer_names[i]]= $(this).parent(); 
		i++;
	});
	
	remove_rows('data_row', table);

	var race_done=false;
	for(var i=0; i<obj.length; i++){
	
		var row  = table.insertRow(-1);
		
		var cell0= document.createElement("td"); // this is the kart number
		var cell1= document.createElement("td"); // this is the kart number
		var cell2= document.createElement("td"); // this is the racer name 
		var cell3= document.createElement("td"); // last lap
		var cell4= document.createElement("td"); // gap time
		var cell5= document.createElement("td"); // gap time
	
		var c_name= "scoreboard_col";
		
		if((tot_laps - obj[i].lap) <= 0)
			race_done=true;
		else
			race_done=false;
		
		if(i==0){
			c_name="first_pos";
		
			document.getElementById('lap_number').innerHTML=laps_left;
			cell3.style.color    = 'red';
			cell2.style.fontSize = '40px';
			cell2.className	    += " racer_name";
			cell5.className		+= " current_lap";
			
		}else{
			cell2.style.fontSize='25px';
		}
		row.className  = "data_row";
		
		cell5.className=cell0.className= cell4.className=cell3.className= cell2.className=cell1.className= c_name;
		cell2.className+=" racer_name";
		cell5.className+=" current_lap";
		
		cell0.style.fontSize='30px';
		cell0.style.textAlign="center";
		cell1.style.textAlign="center";
		
		cell3.style.textAlign='center';
		cell4.style.textAlign='center';
		cell5.style.textAlign='center';
		cell1.style.color='red';
		cell4.style.color='red';
		
		cell0.innerHTML= i+1;
		cell1.innerHTML= obj[i].number;
		cell2.innerHTML= obj[i].racer_name;
		cell3.innerHTML= obj[i].last_lap;
		cell4.innerHTML= obj[i].gap;
		cell5.innerHTML= obj[i].lap;
		
		row.appendChild(cell0);
		row.appendChild(cell1);
		row.appendChild(cell2);
		row.appendChild(cell3);
		row.appendChild(cell4);
		row.appendChild(cell5);
		
		if(racers[obj[i].racer_name]){

			if($(racers[obj[i].racer_name][0]).find(".current_lap").html()!= obj[i].lap){
				var name= $(racers[obj[i].racer_name][0]).find(".racer_name").html();
				
				$(racers[obj[i].racer_name][0]).children().each(function(){
					
					var tableRow = $("td").filter(function() {
						return $(this).text() == name;
					}).closest("tr");
					tableRow.children().each(function(){
						var elem=$(this);
						if( i==0){
							elem.css("background-color","red");
							setTimeout(function(){
								elem.css("background-color","rgba(99,99,99,.3)");
							}, 500);
						}else{
							elem.css("background-color","rgba(100,100,100,1)");
							setTimeout(function(){
								elem.css("background-color","rgba(0,0,0,.3)");
							}, 500);
						}
					});
					
				});
			}
		}
	}
	if( typeof p2r_display_seconds_between !="undefined"){
		if(race_done && laps_left==0){
			setTimeout(function(){
				display_ads();
			}, p2r_display_seconds_between*1000);
		}
	}
}
function remove_rows(class_name, table){
	var rows= document.getElementsByClassName(class_name);

	while (rows.length)
		rows[0].parentElement.removeChild(rows[0]);
}
function display_ads(){
	document.getElementById('scoreboard').style.display 	 = 'none';
	document.getElementById('kart_assignment').style.display = 'none';
	document.getElementById('upcoming_races').style.display  = 'none';
	var ad_space= document.getElementById("p2r_ads");
	if(!ad_space.innerHTML.length){
		for( var i=0; i<images_json.length; i++){
			var ad = document.createElement("img");
				ad.setAttribute('src', "http://admin.poslavu.com/images/"+dataname+"/ad_images/"+images_json[i]);
				//ad.setAttribute('alt', images_json[i].extra_info);
				ad.setAttribute('height', '600px');
				ad.setAttribute('width', '500px');
				ad.className="images";
				ad.setAttribute("id", "image"+i);
				
					ad.style.display='none';
				ad_space.appendChild(ad);
		}
		ad_space.style.display='table';
		if( images_json.length > 1 )
			image_scroller();
	}
}
function hide_ads(){
	$("#p2r_ads").hide();
}
function image_scroller(){
	var count=$("#p2r_ads").find(".images").length;
	var i=0;
	if( i==0){
		$(".images").fadeOut(1000);
		$("#image"+i+"").fadeIn(1000);
		i++;
	}

	var interval_var= setInterval("$.fn.slideshow()",p2r_display_duration*1000);
	$.fn.slideshow=function(){
		if($("#p2r_ads").css("display")=='none'){
			clearInterval(interval_var);
			return;
		}
		$(".images").fadeOut(1000);
		$("#image"+i+"").fadeIn(1000);
		i++;
		if(i >= count)
			i=0;
	};
}
function get_ad_images(){
	var images_json= performAJAX("./get_race_data.php", "function=get_ad_images&dataname="+dataname,false,"POST");

	return images_json;
}
function getUrlVars() {
    var vars = {};
    var parts = window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi, function(m,key,value) {
        vars[key] = value;
    });
    return vars;
}

