<?php
require_once("json_encode.php");
require_once("json_decode.php");

//require_once("/poslavu-local/cp/resources/lavuquery.php");
//require_once("/poslavu-local/local/local_settings.php");
require_once(__DIR__."/../lib/info_inc.php");

function check_selection(){
	
	
   
	$js_obj= new Json();
	$json= $js_obj->decode(file_get_contents("./display_settings.txt"));	
	echo"<script type='text/javascript'>";
	if($json->display_type)
		echo "display_type='".$json->display_type."';";
	else{
		echo "display_type='';";
	}
	if($json->track)
		echo "track=".$json->track.";";
	else{
		echo "track='';";
	}

	if($json->dataname)
		echo "dataname='".$json->dataname."';";
	
	$query= lavu_query("SELECT * FROM `config` where `setting`='p2r_display_seconds_between'");	
	if( lavu_dberror()){
		error_log("MYSQL ERROR FROM p2r_display/index.php   ". lavu_dberror());
	}
	if(mysqli_num_rows($query)){
		$result= mysqli_fetch_assoc($query);
		echo "p2r_display_seconds_between='".$result['value']."';";
	}
	$query= lavu_query("SELECT * FROM `config` where `setting`='p2r_display_duration'");	
	if(mysqli_num_rows($query)){
		$result= mysqli_fetch_assoc($query);
		echo "p2r_display_duration='".$result['value']."';";
	}

	
	echo"</script>";
}
check_selection();
?>
<html>
	<head>
		<title>P2R Display </title>
		<script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
		<script type="text/javascript" src="./display.js"> </script>	
		<link rel="stylesheet" type="text/css" href="./index.css">
	</head>
	<body>
		<div class='over_header' onclick='window.location="?no_auto_load=1"'>
			<span class='race_name'>Standard Race</span> 
		</div>
		<div id='main_container'>
			<!--<div id="header"><img src="./images/logo_black.png" style="float:left;" alt=""><div class='bg_img'></div> </div>-->
			<div id="divider"><img src="./images/divider.jpeg" style="float:left;" alt=""></div>
			<div class='button_container'>
				<input type='button' class='p2r_btn' value='Scoreboard'     onclick='show_track_selector(this)'/>
				<input type='button' class='p2r_btn' value='Kart Assignment'onclick='show_track_selector(this)'/>
				<input type='button' class='p2r_btn' value='Upcoming Races' onclick='show_track_selector(this)'/>
				<br>
				<select id='track_selector' style='margin:0 auto; margin-top:10px; display:none;' onchange= 'display_view(this)'>
					<option value=''>Select a track</option>
				</select>
			</div>
		</div>
		
		<div id='kart_assignment'>
			<table id='kart_assignment_data'>
				<tr>
					<td class='header_col'>Kart Number</td><td class='header_col'>Name</td>
				</tr>
				
			</table>
		</div>
		
		<div id='upcoming_races'>
			<table id='upcoming_races_data_0'>
				<tr>
					<th colspan=2 id='upcoming_race_0'> </th>
				</tr>
			</table>
			<table id='upcoming_races_data_1'>
				<tr>
					<th colspan=2 id='upcoming_race_1'>  </th>
				</tr>
			</table>
			<table id='upcoming_races_data_2'>
				<tr>
					<th colspan=2 id='upcoming_race_2'> </th>
				</tr>
			</table>
		</div>	
		
		<div id='scoreboard'>
			<table id='scoreboard_data'>
				<tr>
					<td class='header_col' style='text-align:center'>POSITION</td>
					<td class='header_col' style='text-align:center'>KART NUMBER</td>
					<td class='header_col'>
						
						<span class='lap_container'>
							<span class='laps_r'>Laps Remaining: </span>
							<span id='lap_number'></span>
						</span>
					</td>
					<td class='header_col' style='text-align:center'> LAP TIME</td>
					<td class='header_col' style='text-align:center'> GAP TIME</td>
					<td class='header_col' style='text-align:center'> Current Lap </td> 
				</tr>
			</table>
		</div>
		<div id='p2r_ads'></div>
		<div id='debug_container'>
			<div id='debug'></div>
		</div>
	</body>	
</html>
