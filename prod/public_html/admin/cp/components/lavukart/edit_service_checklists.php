<?php
	//We need to pull all the service infos (which act as checklist items for their corresponding service_id).
	$query = "SELECT * FROM `[1]`.`lk_service_types` WHERE `locationid`='[2]' AND `_deleted` != '1'";
	lavu_connect_dn($data_name,'poslavu_'.$data_name.'_db');
	$queryResult = lavu_query($query, 'poslavu_'.$data_name.'_db' , $locationid);
    //We need to pull all the Service Types and make a map from
	//We need an array that ties the name to the parents id.
	$serviceIDToServiceNameMap = array();
	while($currRow = mysqli_fetch_assoc($queryResult)){
    	$serviceIDToServiceNameMap [$currRow['id']] = $currRow['name'];
	}
	//When a service type is deleted, we delete all checklist items that point to it
	//so that they do not appear in the edit screen.
	$queryStr  = "UPDATE `[1]`.`lk_service_info` SET `_deleted`='1' WHERE `service_id` in ";
	$queryStr .= "(SELECT `id` FROM `[2]`.`lk_service_types` WHERE (`_deleted`<>'0' && `_deleted`<>' ') )";
	$result    = lavu_query($queryStr, 'poslavu_' . $data_name . '_db', 'poslavu_' . $data_name . '_db');
	if(!$result){
	    echo 'fail';
    	echo ConnectionHub::getConn('rest')->affectedRows();
	}

	// The bottom editor for adding/removing service type checklists.
	display_header("Service Type Checklists");
	$tablename = "lk_service_info";
	$forward_to = "index.php?mode={$section}_{$mode}";
	$filter_by = "`locationid`='$locationid'";
	$back_to_title = "<< View Checklists";
	$fields = array();
	//    Fields     ( Title,  db_field_name,  guiType,  dictionary, list/setvalue ).
	$fields[] = array(speak("Checklist Name"), "name", "text","","list:yes");
	$fields[] = array("Location","locationid","hidden","","setvalue:$locationid");
	$fields[] = array(speak("Service Type"),"service_id","select", $serviceIDToServiceNameMap , "list:yes");
	$fields[] = array(speak("Submit"),"submit","submit");
	require(resource_path() . "/browse.php");
?>
