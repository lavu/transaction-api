<?php
	if($in_lavu)
	{
		// monitoring
		/*$qvals = array();
		$qvals['ipaddress'] = $_SERVER['REMOTE_ADDR'];
		$qvals['date'] = date("Y-m-d");
		$qvals['time'] = date("H:i:s");
		$qvals['ts'] = time();
		$qvals['ccname'] = (isset($company_code))?$company_code:"";
		$qvals['locationid'] = (isset($locationid))?$locationid:"";
		$qvals['mode'] = $mode;
		$qvals['eventid'] = $event_request;
		$qvals['trackid'] = $trackid;*/
		//$monitor_query = lavu_query("select * from `lk_monitor` where `ipaddress`='[ipaddress]' and `mode`='[mode]' and `ccname`='[ccname]' and `locationid`='[locationid]' and `trackid`='[trackid]' order by `ts` desc",$qvals);
		
		function monitor_info($col)
		{
			echo "<td style='padding-left:10px'>" . $col . "</td>";
		}
		function monitor_title($col)
		{
			echo "<td align='center'><b>$col</b></td>";
		}
		
		if(isset($_GET['reload']))
		{
			lavu_query("update `lk_monitor` set `reload`='[1]' where `id`='[2]'",time(),$_GET['reload']);
		}
		
		echo "<table>";
		echo "<tr>";
		monitor_title("ipaddress");
		monitor_title("mode");
		//monitor_title("ccname");
		//monitor_title("locationid");
		monitor_title("trackid");
		//monitor_title("date");
		//monitor_title("time");
		//monitor_title("eventid");
		monitor_title("details");
		monitor_title("elapsed");
		monitor_title(" ");
		echo "</tr>";
		$monitor_query = lavu_query("select * from `lk_monitor` where `ts`>='[1]' order by `ipaddress` asc, `mode` asc, `trackid` asc, `ts` desc ",(time() - 600));
		while($mr = mysqli_fetch_assoc($monitor_query))
		{
			$elapsed = (time() - $mr['ts']);
			$seconds = $elapsed % 60;
			$minutes = ($elapsed - $seconds) / 60;
			if($minutes < 10) $minutes = "0" . $minutes;
			if($seconds < 10) $seconds = "0" . $seconds;
			$elapsed = $minutes . ":" . $seconds;
			
			$show_mode = $mr['mode'];
			if($show_mode=="data" && ($mr['eventid']=="current" || $mr['eventid']=="active"))
				$show_mode = "Scoreboard";
			else if($show_mode=="kart_assignment" && $mr['eventid']=="current_pit")
				$show_mode = "Pit";
			else if($show_mode=="upcoming")
				$show_mode = "Upcoming Races";
			
			echo "<tr>";
			monitor_info($mr['ipaddress']);
			monitor_info($show_mode);
			//monitor_info($mr['ccname']);
			//monitor_info($mr['locationid']);
			$track_query = lavu_query("select * from `lk_tracks` where `id`='[1]'",$mr['trackid']);
			if(mysqli_num_rows($track_query))
			{
				$track_read = mysqli_fetch_assoc($track_query);
				monitor_info($track_read['title']);
			}
			else
				monitor_info($mr['trackid']);
			//monitor_info($mr['date']);
			//monitor_info($mr['time']);
			//monitor_info($mr['eventid']);
			monitor_info($mr['details']);
			monitor_title($elapsed);
			monitor_info("<input type='button' value='Reload' onclick='window.location = \"index.php?mode=$mode&reload=".$mr['id']."\"'>");
			echo "</tr>";
		}
		echo "</table>";
		
		echo "<br><br>";
		if(isset($_GET['autorefresh']))
		{
			echo "<input type='button' value='Turn Off Auto-Refresh' onclick='window.location = \"index.php?mode=admin_monitoring\"'/>";
			echo "<script language='javascript'>";
			echo "function refresh_page() { ";
			echo "  window.location = 'index.php?mode=admin_monitoring&autorefresh=1'; ";
			echo "} ";
			echo "setTimeout('refresh_page()',1000); ";
			echo "</script>";
		}
		else
		{
			echo "<input type='button' value='Turn On Auto-Refresh' onclick='window.location = \"index.php?mode=admin_monitoring&autorefresh=1\"'/>";
		}
		// end monitoring
	}
?>
