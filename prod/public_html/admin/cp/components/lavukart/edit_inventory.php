

<?php
$insert_queries = "";
$delimiter = "[_query_]";
	/* This code was written to clean up parts list data which currently has a bunch of unwanted characters. The entry in the config database is inserted and it's presence keeps this code from ever running more than once. To run the code again, delete the entry in the config table and click the 'Parts' tab in the app */
$sql_query = "SELECT * FROM config WHERE location=\"".$locationid."\" AND type=\"CleanDBData\" AND setting=\"parts_list\"";
$insert_queries .= $sql_query.$delimiter;
$checkUpDateResult = lavu_query($sql_query);
$checkUpDateRows = mysqli_fetch_assoc($checkUpDateResult);


if (!isset($checkUpDateRows['id'])){
			$sql_query = "INSERT INTO config (location,type,setting) VALUES(\"".$locationid."\", \"CleanDBData\", \"parts_list\")";
			$insert_queries .= $sql_query.$delimiter;
			lavu_query($sql_query);
			
			$sql_query = "SELECT * FROM lk_parts WHERE locationid=\"".$locationid."\"";
			$insert_queries .= $sql_query.$delimiter;
			$sanitizeDBResults = lavu_query($sql_query);
				
			while ($cleanDB = mysqli_fetch_assoc($sanitizeDBResults)){
					//clean database of problematic characters
					$name = clean($cleanDB['name']);
					$description = clean($cleanDB['description']);
					$qty = clean($cleanDB['qty']);
					$cost = clean($cleanDB['cost']);
					$id = clean($cleanDB['id']);
					$cleanCategory = clean($cleanDB['category']);		
					$custom_part_id = clean($cleanDB['custom_part_id']);
					
					if ($custom_part_id == ""){
							$sql_query = "UPDATE lk_parts SET custom_part_id='".$id."' WHERE id='".$id."' AND locationid='".$locationid."'";
							$insert_queries .= $sql_query.$delimiter;
							lavu_query($sql_query);
					}//if
				$sql_query = "UPDATE lk_parts SET name=\"".$name."\", description=\"".$description."\", qty=\"".$qty."\", cost=\"".$cost."\" WHERE id=\"".$id."\" AND locationid=\"".$locationid."\"";
				$insert_queries .= $sql_query.$delimiter;
				lavu_query($sql_query);
				
				$sql_query = "UPDATE lk_parts SET category=\"".$cleanCategory."\" WHERE id=\"".$id."\" AND locationid=\"".$locationid."\"";
				$insert_queries .= $sql_query.$delimiter;
				lavu_query($sql_query);
							
			}//while
}//if


/*
$sql_query = "SELECT * FROM config WHERE location=\"$locationid\" AND setting='last_date_email_was_sent'";
$email_sent_res = lavu_query($sql_query);
if (mysqli_num_rows($email_sent_res)*1 != 0){
			$email_sent_row = mysqli_fetch_assoc($email_sent_res);
			$last_date_sent_ts = strtotime($email_sent_row['value']);
			$cur_date_ts = strto
			//$cur_timestamp_arr = date($last_date_sent);
			//$cur_timestamp = $cur_timestamp_arr[0];
			echo "cur: $last_date_sent_ts<br>";
}//if
if(mysqli_num_rows($email_sent_res)*1 == 0){
			$use_this_date = date("Y-m-d");
			$sql_query = "INSERT INTO config (location,setting,value) VALUES ('$locationid','last_date_email_was_sent','$use_this_date')";
			lavu_query($sql_query);			
}//if

*/



/* Insert entry into config table to store notification email (If it isn't already there) */
$sql_query = "SELECT * FROM config WHERE setting='notify_email' AND location='".$locationid."'";
$insert_queries .= $sql_query.$delimiter;
$resIsThere = lavu_query($sql_query);
//$numRows = mysqli_num_rows($resIsThere);
//echo "numRows: $numRows<br/>";

if (mysqli_num_rows($resIsThere)*1==0){
		$notify_by_email = $_POST['notify_by_email'];
		$sql_query = "INSERT INTO config (setting,location,value) VALUES ('notify_email','".$locationid."','".$notify_by_email."')";
	//echo "doit <br/>";
		$insert_queries .= $sql_query.$delimiter;
		lavu_query($sql_query);
}//if


if (isset($_POST['notify_by_email'])){
		$notify_by_email = $_POST['notify_by_email'];
		//echo "HERE: $notify_by_email<br/>";
		$sql_query = "UPDATE config SET value='$notify_by_email' WHERE location='$locationid' AND setting='notify_email'";
	//echo "doit <br/>";
		//$insert_queries .= $sql_query.$delimiter;
		lavu_query($sql_query);
}//if




		//echo "NOG: $notify_by_email and loc: $locationid<br/><br/>";
	//	exit();
		


$rowIndex = $_POST['max_index_val'];
//echo "rowIndex: $rowIndex<br/>";

$maxVal = "";
if ($rowIndex != ""){
		
		$db_info = array();
		for ($i=0; $i<=$rowIndex*1;$i++){
				//$maxVal = $i;
				$cust_part_id = $_POST["partID_".$i];
				$db_info["custom_part_id"] = $cust_part_id;
				$entryIDID = "entryID_".$i;
				$entryID = $_POST[$entryIDID];
				$desc = "partDesc_".$i;
				$db_info["name"] = $_POST[$desc]; //it's actually the name, not the description
				
			//	$tempID = rand(0,99999).date("Y-m-d H:i:s");
				//$res = lavu_query("INSERT INTO lk_parts (name) VALUES ('[1]')",$tempID);
			//	$res2 = lavu_query("SELECT * FROM lk_parts WHERE name='[1]'",$tempID);
			//	$row = mysqli_fetch_assoc($res2);
			//	$id = $row['id'];
				
				
				$cost = "partCost_".$i;
				$db_info["cost"] = $_POST[$cost];
				$qty = "partQty_".$i;
				$db_info["qty"] = $_POST[$qty];
				$minQty = "minQty_".$i;
				$db_info["min_val"] = $_POST[$minQty];
				$maxQty = "maxQty_".$i;
				$db_info["max_val"] = $_POST[$maxQty];
				$deleteThisID = "deleteIndex_".$i;
				$deleteThis = $_POST[$deleteThisID];
			//	echo "entryID: $entryID<br/>";
		
				if ($deleteThis != "yes"){//update the form
							
							foreach($db_info as $key => $value){
									//echo "$key: $value: id: $entryID<br/>";
									$sql_query = "UPDATE lk_parts SET $key='$value' WHERE id='".$entryID."'";
									$insert_queries .= $sql_query.$delimiter;
									lavu_query($sql_query);
									
									//lavu_query("UPDATE lk_parts SET $key='$value' WHERE locationid='[1]'",$locationid);
							}//foreach
						
				}//if
				if ($deleteThis == "yes"){
				//	echo "$locationid and $entryID<br/>";
				//	exit();
						//mail("ck@poslavu.com","deleteThis: $deleteThis, ID; $deleteThisID","");
						echo "DELETE THIS: $deleteThis<br/>locationid: $locationid <br/>entryID: $entryID<br/>";
						$sql_query = "DELETE FROM lk_parts WHERE locationid='".$locationid."' AND id='".$entryID."'";
						$insert_queries .= $sql_query.$delimiter;
						lavu_query($insert_queries);
						
				}//if
				
		}//for
		
		//echo "maxVal: $maxVal<br/>rowIndex: $rowIndex<br/>";
		
}//if

?>


<head>

<style type="text/css">

.row{
	width:100%;
	
	
}

.rowElement{
	float:left;
}

.re1{
	width:100px;
	margin-left:150px;
	/*background-color:#393;*/
}


.re2{
	width:200px;
	/*background-color:#693;*/
}



.re3{
	width:100px;
	/*background-color:#CC6633;*/
}


.re4{
	width:75px;
	/*background-color:#FF6633;*/
}


.re5{
	width:40px;
	/*background-color:#CF3;*/
}

.re6{
	width:50px;
	/*background-color:#CF3;*/
}


.clearRE{
	clear:both;
}

.leftForm{
	float:left;
	width:150px;
	margin-left:300px;
	text-align:right;
}

.rightForm{
	float:left;
	width:180px;
	text-align:left;
	padding-left:8px;
}

.clearForm{
	clear:both;
}

</style>

<script type="text/javascript">
function deleteEntry(rowNum){
	
		answer =confirm("Do you wish to delete this entry? Your changes will not be committed until you click 'Submit'.");
		id = "deleteIndex_"+rowNum;
		rowID = "row_"+rowNum;
		if (answer){
					document.getElementById(rowID).style.display = "none";
					document.getElementById(id).value = "yes";
		}//if
}//delete()


function changeToRefresh(entry_index){
		/* This function exists because when people update inventory on the app side, they click "Update" on this page thinking it will refresh the page (it does now!), instead of submitting the currently displayed data to the database (it used to do this and changes on the app side were being overwritten making it look like the update never happened at all. It was just getting overwritten);*/
		//alert(entry_index);
		cur_entry_id = "isModified_"+entry_index;
		
		//change entry mode to update so it gets updated
		document.getElementById(cur_entry_id).value = "update";
		
		//change this value to update so the altered values get updated, other wise the page just refreshes
		document.getElementById("update_or_refresh").value = "update";
		return;
}//changeToRefresh()


function doWhat(){
		/*Should we just refresh the page (nothing was altered) or should we update (something was altered)*/
		//alert("hi");
		doThis = document.getElementById("update_or_refresh").value;
		//alert(doThis);
		if (doThis == "update"){
			alert("update");
				return true;
		}
		if (doThis == "refresh"){
				alert("refresh");
				window.location.href="http://admin.poslavu.com/cp/index.php?mode=admin_inventory";
		}//if
		
		return false;
}//doWhat()

</script>

</head>

<body>
<p>It will take a few minutes for your changes to take effect.</p>
<form name="parts_inventory_form" action="" method="post" onSubmit="return doWhat();">
<input type="hidden" name="update_or_refresh" id="update_or_refresh" value="refresh"/>
<br><br>
<input name="Submit" type="submit" value="Update">

<div style="height:35px;">&nbsp;</div>

<?php 
/* This block inserts an entry into the config table. The entry is used to record which parts have had notification emails sent about them. This way a user doesn't get a notification email every time this page is run */





?>



<?php
$sql_query = "SELECT * FROM config WHERE location='".$locationid."' AND setting='notify_email'";
$insert_queries .= $sql_query.$delimiter;
$notify_email_res = lavu_query($sql_query);
$notify_email_row = mysqli_fetch_assoc($notify_email_res);
$notify_email = $notify_email_row['value'];
?>
<div class="leftForm">
<label>Email notifications to this address</label>
</div>
<div class="rightForm">
<input type="text" onChange="document.getElementById('update_or_refresh').value = 'update';" name="notify_by_email" id="notify_by_email" value="<?php echo $notify_email; ?>" style="width:130px;"/>
</div>
<div class="clearForm"></div>

<div style="height:35px;;">&nbsp;</div>

<div class="row">

		<div class="rowElement re1"><?php speak("Part ID") ?></div>
		
		<div class="rowElement re2"><?php speak("Description") ?></div>
		
		<div class="rowElement re3"><?php speak("Price Per Part") ?> &times;</div>
		
		<div class="rowElement re4"><?php speak("Qty.") ?> &#61;</div>
		
		<div class="rowElement re4"><?php speak("Total") ?></div>
		
		<div class="rowElement re5"><?php speak("Min Qty.") ?></div>
		<div class="rowElement re5"><?php speak("Max Qty.") ?></div>
		
		<div class="rowElement re6"><?php speak("Delete") ?></div>
		<div class="clearRE"></div>
</div><!--row-->

<?php

$sql_query = "SELECT * FROM lk_parts WHERE locationid='".$locationid."'";
$insert_queries .= $sql_query.$delimiter;
$res = lavu_query($sql_query);
$index = 0;
$total_part_cost = 0;
while($row = mysqli_fetch_assoc($res)){
	//echo $row['name']."<br/>";
			//$id = $row['id'];
		//	echo "id: $id<br/>";

			echo "<div class=\"row\" id=\"row_".$index."\">";
					$custom_part_id = $row['custom_part_id'];
					echo "<div class=\"rowElement re1\"><input onchange=\"changeToRefresh('$index');\" type='text' name='partID_".$index."' value='".strtoupper($custom_part_id)."' style='width:90px;'/></div>";
					
					$entry_id = $row['id'];
					$part_name = $row['name'];
					echo "<div class=\"rowElement re2\"><textarea onchange=\"changeToRefresh('$index');\" name='partDesc_".$index."' style='width:190px; height:50px;'>$part_name</textarea></div>";
					
					$part_cost = $row['cost'];
					echo "<div class=\"rowElement re3\"><input onchange=\"changeToRefresh('$index');\" type='text' name='partCost_".$index."' value='$part_cost' style='width:80px;'/>&times;</div>";
					
					$part_qty = $row['qty'];
					echo "<div class=\"rowElement re4\"><input onchange=\"changeToRefresh('$index');\" type='text' name='partQty_".$index."' value='$part_qty' style='width:50px;'/>&#61;</div>";
					$entry_total_cost = $part_qty*1 * $part_cost*1;
					$total_part_cost = $total_part_cost + $entry_total_cost;
					echo "<div class=\"rowElement re4\">&#36;".number_format($entry_total_cost,2)."</div>";
					$minVal = $row['min_val'];
					
					
					
/*
					$sql_query = "SELECT * FROM config WHERE setting=\"notification_min\" AND location=\"$locationid\"";
					$insert_queries .= $sql_query.$delimiter;
					$resGetRecord = lavu_query($sql_query);
					if (mysqli_num_rows($resGetRecord) == 0){
							$sql_query = "INSERT INTO config (setting,location,value) VALUES (\"notification_min\",\"$locationid\",\"$date_initiated\")";
							lavu_query($sql_query);
							$insert_queries .= $sql_query.$delimiter;
					}//if
*/
					
					
					//should we send a email notification?
					$sql_query = "SELECT * FROM config WHERE setting=\"notification_min\" AND location=\"$locationid\" AND value2=\"$entry_id\"";
								//$insert_queries .= $sql_query.$delimiter;
					$resGetRecord = lavu_query($sql_query);
					$date_initiated = date("Y-m-d");
					if (mysqli_num_rows($resGetRecord) == 0){
								
								$sql_query = "INSERT INTO config (setting,location,value,value2) VALUES (\"notification_min\",\"$locationid\",\"$date_initiated\",\"$entry_id\")";
								
								lavu_query($sql_query);
											//$insert_queries .= $sql_query.$delimiter;
					}//if
					$resGetRow = mysqli_fetch_assoc($resGetRecord);
					$last_update_str = $resGetRow['value'];
					
					/* If the last update was sent at lest two days ago, send it again */
					if (((strtotime(date("Y-m-d")) - strtotime($last_update_str)) >= 86400*2 ) && ($part_qty <= $minVal) ){
							
								//$sql_query = "SELECT * FROM config WHERE location=\"$locationid\" AND setting=\"notification_min\" AND value2=\"$entry_id\"";
								$sql_query = "UPDATE config SET value=\"$date_initiated\" WHERE location=\"$locationid\" AND value2=\"$entry_id\"";
								lavu_query($sql_query);
						//echo "SEND THE min MAIL!!<br/>";
								mail($notify_email,"LAVUNotification: A part has reached a minimum quantity", "Greetings, please attend to the following:\n\nPart id: $custom_part_id \n Description: $part_name \n Quantity: $part_qty \n Minimum quantity: $minVal \n \n It is time to order more of these parts.");
					}//if
					
					echo "<div class=\"rowElement re5\"><input onchange=\"changeToRefresh('$index');\" type='text' name='minQty_".$index."' style='width:35px;' value='$minVal'/></div>";
					
					
					$maxVal = $row['max_val'];
					
					//should we send a email notification?
					$sql_query = "SELECT * FROM config WHERE setting=\"notification_max\" AND location=\"$locationid\" AND value2=\"$entry_id\"";
								//$insert_queries .= $sql_query.$delimiter;
					$resGetRecord = lavu_query($sql_query);
					$date_initiated = date("Y-m-d");
					if (mysqli_num_rows($resGetRecord) == 0){
								
								$sql_query = "INSERT INTO config (setting,location,value,value2) VALUES (\"notification_max\",\"$locationid\",\"$date_initiated\",\"$entry_id\")";
								
								lavu_query($sql_query);
											//$insert_queries .= $sql_query.$delimiter;
					}//if
					$resGetRow = mysqli_fetch_assoc($resGetRecord);
					$last_update_str = $resGetRow['value'];
					
					/* If the last update was sent at lest two days ago, send it again */
					if ( ((strtotime(date("Y-m-d")) - strtotime($last_update_str)) >= 86400*2 ) && ($part_qty >= $maxVal)){
							$sql_query = "UPDATE config SET value=\"$date_initiated\" WHERE location=\"$locationid\" AND value2=\"$entry_id\"";
							lavu_query($sql_query);
							//echo "SEND THE max MAIL!!<br/>";
							mail($notify_email,"LAVUNotification: A part has reached a maximium quantity", "Greetings, The following parts are at a maximum value:\n\nPart id: $custom_part_id \n Description: $part_name \n Quantity: $part_qty \n Maximum quantity: $maxVal \n \n You do not need any more of these parts.");
					}//if
				
					echo "<div class=\"rowElement re5\"><input onchange=\"changeToRefresh('$index');\" type='text' name='maxQty_".$index."' style='width:35px;' value='$maxVal'/></div>";
					
					
					echo "<div class=\"rowElement re6\"><a href=\"javascript: deleteEntry('$index'); changeToRefresh('$index');\">X</a><input type='hidden' id='deleteIndex_".$index."' name='deleteIndex_".$index."'/></div>";
					
					echo "<div class=\"clearRE\"></div>";
					echo "<input type='hidden' name='entryID_".$index."' value='".$entry_id."'/>";
					
					echo "<input type='hidden' name='isModified_".$index."' id='isModified_".$index."' value='refresh'/>";
					
			echo "</div><!--row-->";
			
			$index++;
}//while
?>

<div class="row">

		<div class="rowElement re1">&nbsp;</div>
		
		<div class="rowElement re2">&nbsp;</div>
		
		<div class="rowElement" style="width:60px;">&nbsp;</div>
		
		<div class="rowElement" style="width:100px;">Total Inventory &#61;</div>
		
		<div class="rowElement" style="width:100px;border-top:#000 solid 2px;">&#36; <?php echo number_format($total_part_cost,2); ?></div>
		
		<div class="rowElement re5">&nbsp;</div>
		<div class="rowElement re5">&nbsp;</div>
		
		<div class="rowElement re6">&nbsp;</div>
		<div class="clearRE"></div>
</div><!--row-->

<input name="Submit" type="submit" value="Update">
<input type="hidden" value="<?php echo $index; ?>" name="max_index_val"/>

</form>

</body>

<?php
if ($insert_queries != "") {
					schedule_remote_to_local_sync($locationid, "run local query", "config", $insert_queries);
}
?>
