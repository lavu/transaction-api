<?php
	if($in_lavu)
	{
		display_header("Events");
		echo "<br><br>";
		
		$tablename = "lk_events";
		$forward_to = "index.php?mode={$section}_{$mode}";
		$filter_by = "`locationid`='$locationid'";
		$back_to_title = "<< View Event List";
		
		$setdate = urlvar("setdate",date("Y-m-d"));
		create_datepicker("edit1",$setdate,"window.location = '$forward_to&setdate=' + [value];");
		
		$scheduleid = urlvar("scheduleid");
		if($scheduleid)
		{
			$event_query = lavu_query("select * from `lk_events` LEFT JOIN `lk_event_types` ON `lk_events`.`type` = `lk_event_types`.`id` where `lk_events`.`id`='[1]'",$scheduleid);
			if(mysqli_num_rows($event_query))
			{
				$event_read = mysqli_fetch_assoc($event_query);
				echo "<br><br>Schedule Event: <b>" . $event_read['title'] . "</b> - <font color='#007700'>" . display_time($event_read['time']) . "</font>";
				echo "<br><br>";

				/*$automate_race = urlvar("automate_race");
				if($automate_race)
				{
					$sched_query = lavu_query("select * from `lk_event_schedules` LEFT JOIN `lk_customers` ON `lk_event_schedules`.`customerid`=`lk_customers`.`id` where `lk_event_schedules`.`eventid`='[1]'",$scheduleid);
					while($sched_read = mysqli_fetch_assoc($sched_query))
					{
						$maxlaps = $event_read['laps'];
						if(!is_numeric($maxlaps)) $maxlaps = 8;
						if($maxlaps > 99) $maxlaps = 99;
						$set_laptime = 0;
						for($i=1; $i<=$maxlaps; $i++)
						{
							$lap_query = lavu_query("select * from `lk_laps` where `scheduleid`='[1]' and `pass`='[2]'",$sched_read['id'],$i);
							if(mysqli_num_rows($lap_query))
							{
								$lap_read = mysqli_fetch_assoc($lap_query);
								$set_laptime = $lap_read['ms_this_lap'];
							}
							else
							{
								$newlap = $set_laptime + (20 + rand(0,20)) * 1000;
								$vars = array();
								$vars['scheduleid'] = $sched_read['id'];
								$vars['pass'] = $i;
								$vars['ms_this_lap'] = $newlap;
								$vars['ms_last_lap'] = $set_laptime;
								$vars['ms_duration'] = ($newlap - $set_laptime);
								
								lavu_query("insert into `lk_laps` (`scheduleid`,`pass`,`ms_this_lap`,`ms_last_lap`,`ms_duration`) values ('[scheduleid]','[pass]','[ms_this_lap]','[ms_last_lap]','[ms_duration]')",$vars);
								$set_laptime = $newlap;
							} 
						}
					}
				}
				else
				{
					echo "<a href='$forward_to&scheduleid=$scheduleid&automate_race=1'>(automate race)</a>";
					echo "<br><br>";
				}*/
				
				/*$sample_lnk = "admin.poslavu.com/components/lavukart/race_scores.php?mode=results&cc=DEVKART_key_88347&loc_id=8&trackid=1&eventid=current";
				$set_cc = admin_info("dataname") . "_key_" . lsecurity_key(admin_info("companyid"));
				$set_trackid = $event_read['trackid'];
				$set_eventid = $scheduleid;
				$set_mode = "results";
				echo "<a href='http://www.poslavu.com/admin/components/lavukart/race_scores.php?eventid=$set_eventid&trackid=$set_trackid&mode=$set_mode&loc_id=$locationid&cc=$set_cc' target='_blank'>View Race Result Sheet</a>";
				echo "<br><br><b>Passes:</b><br>";	
				$sched_query = lavu_query("select `f_name`,`l_name`,`racer_name`,`lk_event_schedules`.`id` as `id` from `lk_event_schedules` LEFT JOIN `lk_customers` ON `lk_event_schedules`.`customerid`=`lk_customers`.`id` where `lk_event_schedules`.`eventid`='[1]'",$scheduleid);
				if(mysqli_num_rows($sched_query))
				{
					echo "<table>";
					while($sched_read = mysqli_fetch_assoc($sched_query))
					{
						echo "<tr>";
						echo "<td>" . $sched_read['racer_name'] . "</td><td>" . trim($sched_read['f_name'] . " " . $sched_read['l_name']) . "</td>";
						$lap = 1;
						$lap_query = lavu_query("select * from `lk_laps` where `scheduleid`='[1]'",$sched_read['id']);
						while($lap_read = mysqli_fetch_assoc($lap_query))
						{
							if(is_numeric($lap_read['pass']) && $lap_read['pass'] < 999)
							{
								while($lap < $lap_read['pass'])
								{
									echo "<td>-</td>";
									$lap++;
								}
								echo "<td>".$lap_read['ms_duration']."</td>";
							}
						}
						echo "</tr>";
					}
					echo "</table>";
				}
				else
				{
					echo "Nobody Scheduled for this Event";
				}*/
			}
		}
		else if($setdate)
		{
			$forward_to = $forward_to . "&setdate=" . $setdate;
			$filter_by .= " and `date`='".str_replace("'","''",$setdate)."' and `ms_start` != '' ";
			echo "<br><br>";
			
			$track_list = array();
			$track_query = lavu_query("select * from `lk_tracks` where `locationid`='$locationid' order by `title` asc");
			while($track_read = mysqli_fetch_assoc($track_query))
			{
				$trackid = $track_read['id'];
				$track_list[$trackid] = $track_read['title'];
			}
			$etype_list = array();
			$etype_query = lavu_query("select * from `lk_event_types` order by `title` asc");
			while($etype_read = mysqli_fetch_assoc($etype_query))
			{
				$etypeid = $etype_read['id'];
				$etype_list[$etypeid] = $etype_read['title'];
			}
			
			$set_cc = admin_info("dataname") . "_key_" . lsecurity_key(admin_info("companyid"));
			$set_browse_link = "http:://admin.poslavu.com/dev/components/lavukart/race_scores.php?eventid=[id]&trackid=[trackid]&mode=results&loc_id=$locationid&cc=$set_cc";
			$set_info_link = "http:://admin.poslavu.com/dev/components/lavukart/race_scores.php?eventid=[id]&trackid=[trackid]&mode=info&loc_id=$locationid&cc=$set_cc";
			
			$fields = array();
			$fields[] = array(speak("Type"),"type","select",$etype_list,"list:yes");
			//$fields[] = array("Date","date","calendar","","list:yes");
			$fields[] = array("Date","date","hidden","","setvalue:$setdate");
			$fields[] = array(speak("Time"),"time","time","","list:yes");
			$fields[] = array(speak("Track"),"trackid","select",$track_list,"list:yes");
			$fields[] = array("Location","locationid","hidden","","setvalue:$locationid");
			$fields[] = array(speak("Race Sheet"),"race_sheet","browse_button","link:$set_browse_link, target:_blank");
			$fields[] = array(speak("Info"),"info","browse_button","link:$set_info_link, target:_blank");
			$fields[] = array(speak("Submit"),"submit","submit");
	
			require_once(resource_path() . "/browse.php");
		}
	}
?>
