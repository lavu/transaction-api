<?php
	//ini_set('display_errors',1);

	if($in_lavu)
	{

		if(isset($_GET['view_waiver']))
		{
			$rowid = $_GET['view_waiver'];
			$waiver_sig_name = "signature_" . $rowid . ".jpg";
			$waiver_sig_relative = "/components/lavukart/companies/" . $data_name . "/signatures/" . $waiver_sig_name;
			$waiver_sig_root = "/home/poslavu/public_html/admin" . $waiver_sig_relative;
	
			echo "<head>";
			echo "<title>View Waiver</title>";
			echo "<style>";
			echo "  body { font-family:Verdana,Arial; font-size:12px; }";
			echo "  table { font-family:Verdana,Arial; font-size:12px; }";
			echo "</style>";
			echo "</head>";
			echo "<body>";
			echo "<center>";
			
			

			list($waiver_width, $waiver_height, $waiver_type, $waiver_attr) = getimagesize($waiver_sig_root);
			
			if($waiver_height > 200)
			{
				echo "<table width='700' cellpadding=12 style='border:solid 1px #999999'><tr><td align='left'>";
				$waiver_query = lavu_query("select * from `lk_waivers` where `loc_id`='[1]' order by id desc limit 1",$locationid);
				if(mysqli_num_rows($waiver_query))
				{
					$waiver_read = mysqli_fetch_assoc($waiver_query);
					
					echo str_replace("\n","<br>",$waiver_read['minor_parent']);
				}
				echo "</td></tr><tr><td align='center'>";
				
				$show_width = 400;
				$half_height = ($waiver_height / 2) * ($show_width / $waiver_width);
				//echo "<table style='background:URL(" . $waiver_sig_relative . "); background-repeat:no-repeat; background-position:0px 0px; width:".$waiver_width."px; height:".($waiver_height / 2)."px'><td>&nbsp;</td></table>";
				echo "<div style='width:400px; height:".$half_height."px; overflow:hidden'>";
				echo "<img src='$waiver_sig_relative' width='$show_width' style='position:absolute; top:0px; left:0px'/>";
				echo "</div>";
				echo "</td></tr></table>";

				echo "<table width='700' cellpadding=12 style='border:solid 1px #999999'><tr><td align='left'>";
				$waiver_query = lavu_query("select * from `lk_waivers` where `loc_id`='[1]' order by id desc limit 1",$locationid);
				if(mysqli_num_rows($waiver_query))
				{
					$waiver_read = mysqli_fetch_assoc($waiver_query);
					
					echo str_replace("\n","<br>",$waiver_read['minor_participant']);
				}
				echo "</td></tr><tr><td align='center'>";
				//echo "<table style='background:URL(" . $waiver_sig_relative . "); background-repeat:no-repeat; background-position:0px ".(0 - $waiver_height / 2)."px; width:".$waiver_width."px; height:".($waiver_height / 2)."px'><td>&nbsp;</td></table>";
				echo "<div style='width:400px; height:".$half_height."px; overflow:hidden'>";
				echo "<img src='$waiver_sig_relative' width='$show_width' style='position:absolute; top:-".$half_height."px; left:0px'/>";
				echo "</div>";
				echo "</td></tr></table>";
			}
			else
			{
				echo "<table width='700' cellpadding=12 style='border:solid 1px #999999'><tr><td align='left'>";
				$waiver_query = lavu_query("select * from `lk_waivers` where `loc_id`='[1]' order by id desc limit 1",$locationid);
				if(mysqli_num_rows($waiver_query))
				{
					$waiver_read = mysqli_fetch_assoc($waiver_query);
					
					echo str_replace("\n","<br>",$waiver_read['adult']);
				}
				echo "</td></tr><tr><td align='center'>";
				echo "<img src='" . $waiver_sig_relative . "' border='0' width='400' />";
				echo "</td></tr></table>";
			}
			echo "</center>";
			echo "</body>";
		}
		else
		{
			echo "<br><br>";
			
			$tablename = "lk_waivers";
			$forward_to = "index.php?mode={$section}_{$mode}";
					
			$fields = array();
			$fields[] = array(speak("Version"),"version","readonly","","list:yes");
			$fields[] = array(speak("Last Updated By"),"saved_by","readonly","","list:yes");
			$fields[] = array(speak("User ID"),"saved_by_id","hidden","","setvalue:".admin_info("loggedin"));
			$fields[] = array(speak("Location ID"),"loc_id","hidden","","setvalue:$locationid");
			$fields[] = array(speak("Date/Time"),"datetime","readonly","","list:yes");
			$fields[] = array("","","seperator");
			$fields[] = array(speak("Adult Waiver"),"adult","textarea");
			$fields[] = array("","","seperator");
			$fields[] = array(speak("Minor Waiver (Parent)"),"minor_parent","textarea");
			$fields[] = array("","","seperator");
			$fields[] = array(speak("Minor Waiver (Participant)"),"minor_participant","textarea");
			$fields[] = array("","","seperator");

			if(does_contain_type_field()){
				//$fields[] = array(speak("Default service type").":","tab_view","select",array("2"=>"Quick Serve","3"=>"Tables","1"=>"Tabs")); //SELECT EXAMPLE
				$fields[] = array(speak("Waiver Type"), "type", "select", array("waiver"=>"Waiver","contract"=>"Contract"));
			}

			$fields[] = array(speak("Save"),"submit","submit");
			$filter_by = "`loc_id` = '$locationid'";

			require_once(resource_path() . "/browse.php");
			}
		}

		function does_contain_type_field(){
			$queryResult = lavu_query("SELECT * FROM `lk_waivers` LIMIT 1");
			if($queryResult && mysqli_num_rows($queryResult)){
				$row = mysqli_fetch_assoc($queryResult);
				if(isset($row['type'])){
					return true;
				}
			}
			return false;
		}
?>
