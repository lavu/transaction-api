<?php

	ini_set("display_errors",'1');
	ini_set('memory_limit', '1024M');
	require_once(dirname(__FILE__) ."/../../../cp/resources/core_functions.php");

	isset($_GET['debugging'])?$debugging=true:$debugging= false;

	isset($_GET['force_time_update'])?$force_time_update=true:$force_time_update= false;

	$locationsArray=getLocations();

	if(!count($locationsArray)){
		echo "No dataname passed. Exiting.";
		exit;
	}

	//This guy here is the main loop of awesomeness.
	echo "<pre>";
		print_r($locationsArray);
		$startTime='';

	foreach($locationsArray as $dataname){
		if($dataname=='DEVKART' ||$dataname=='17kart'||$dataname=='demokart'||$dataname=='test_p2r')
			continue;
		$transferTime= getLastTransferTime($dataname);

		//$transferTime= "2013-01-05 00:00:01";
		$idRead = mysqli_fetch_assoc(mlavu_query("select `id` from `poslavu_MAIN_db`.`restaurants` where `data_name`= '[1]'", $dataname));
		$id      = $idRead['id'];

		echo "the dataname is: $dataname has rest_id:$id\n";

		getRaceData($dataname, $transferTime, $id);
	}
	echo "</pre>";


	function getRaceData($dataname, $transferTime, $id){
		//echo "$dataname  $transferTime $id <br>";
		lavu_connect_dn($dataname,'poslavu_'.$dataname.'_db');
		$event_query = mlavu_query("select `id`, `type`, `ms_start`,`ms_end`,`trackid`, `date` from `poslavu_[1]_db`.`lk_events` where `ms_end` > '[2]' order by `ms_end` asc limit 20",$dataname,$transferTime);

		if(!mysqli_num_rows($event_query)){
			echo "No Data to send for ". $dataname." at time: ".$transferTime." continuing.\n";
			return;
		}else{
			echo "Number of row for dataname:[$dataname] = ".mysqli_num_rows($event_query)."<br>";
		}

		$error_string		= '';
		$debugging_str  	= "Dataname - Event id -      MS end        - Event type \n";
		$latest_time 		= '';
		$race_details_array	= array();

		while($event= mysqli_fetch_assoc($event_query)){
			$track_query = mysqli_fetch_assoc(mlavu_query("select * from `poslavu_[1]_db`.`lk_tracks` where `id` = '[2]'",$dataname, $event['trackid']));
			$event['track']=$track_query['title'];
			$error_occurred=0;
			$debugging_str .= $dataname . " - " . $event['id'] . " - " . $event['ms_end'] . " - ".$event['type']. "\n";
			$latest_time= $event['ms_end'];
			$type_query = mlavu_query("select * from `poslavu_[2]_db`.`lk_event_types` where `id`='[1]'",$event['type'], $dataname);

			if(mysqli_num_rows($type_query)){
				$type_read = mysqli_fetch_assoc($type_query);
				if(isset($type_read['scores_page_title']) && !empty($type_read['scores_page_title']))
					$event['scores_page_title']= $type_read['scores_page_title'];
				else
					$event['scores_page_title'] ="";
				$event['type']= $type_read['title'];		//at this point we have the string representation of the event type.
			}else{
				$error_string.= "type query failed for id : ".$event['type'].". dataname: $dataname\n ";
				$error_occurred=true;
			}

echo "    Event type:".$event['type'].'<br>';

			$sched_query = mlavu_query("select `kartid`,`f_name`,`l_name`,`racer_name`,`lk_event_schedules`.`id` as `sched_id`,`lk_customers`.`id` as `custid`,`lk_karts`.`number` as `number` from `poslavu_[2]_db`.`lk_event_schedules` LEFT JOIN `poslavu_[2]_db`.`lk_customers` ON `poslavu_[2]_db`.`lk_event_schedules`.`customerid`=`poslavu_[2]_db`.`lk_customers`.`id`  LEFT JOIN `poslavu_[2]_db`.`lk_karts` ON `poslavu_[2]_db`.`lk_event_schedules`.`kartid`=`poslavu_[2]_db`.`lk_karts`.`id` where `poslavu_[2]_db`.`lk_karts`.`number`!='' and `poslavu_[2]_db`.`lk_event_schedules`.`eventid`='[1]'",$event['id'],$dataname);
			
			$numRowsSchedQuery = mysqli_num_rows($sched_query);
echo "	  Num Rows:".$numRowsSchedQuery."<br>";
			if(mysqli_num_rows($sched_query)){

				$racer_info = array();
				$best_times = array();

				while($sched_read = mysqli_fetch_assoc($sched_query)){

					if($sched_read['custid'])
						$racer_details=getRacerDetails($sched_read['custid'], $dataname);
					else{
echo "Could not get customerID.<br>";
						$error_string.="No Customer found for  Dataname: ". $dataname. " Event id:". $event['id']. " sched id: ". $sched_read['sched_id'];
echo "Could not get racer details.!!!!!!!!!<br>";
						continue;
					}


					if($racer_details=='failed'){
						$error_string.="No Customer found for customer_id: ". $sched_read['custid'].
						" Dataname: ". $dataname. " Event id:". $event['id']. " sched id: ". $sched_read['sched_id'];
						continue;
					}
					else $racer_info[]= $racer_details;

					$lap_query = mlavu_query("select * from `poslavu_[2]_db`.`lk_laps` where `scheduleid`='[1]'  order by `ms_this_lap` asc", $sched_read['sched_id'], $dataname);

					if( mysqli_num_rows($lap_query)){

						$best_time= 999999;
						while($lap_read = mysqli_fetch_assoc($lap_query)){

							if(($lap_read['ms_duration']*1)==0)
								$lap_read['ms_duration']= (($lap_read['ms_this_lap']*1)/1000);

							if( $lap_read['ms_duration'] < $best_time)
								$best_time= $lap_read['ms_duration'];
						}
					}else{
						$error_string.="No laps found event ID: ". $event['id']." schedule id is: ".$sched_read['sched_id']."\n";
						$error_occurred=true;
					}
					$best_times[$sched_read['custid']]= $best_time;
				}

			}else{
				$error_string.="Sched Query failed for event ID: ". $event['id']. "\n";
				$error_occurred=1;
			}
			if($error_occurred!=1){
				$details=array();
				$details['event_info']= array("eventid"=>$event['id'],"date"=>$event['date'],"ms_start"=>$event['ms_start'],"ms_end"=>$event['ms_end'],"track"=>$event['track'], "type"=>$event['type'],"scores_page_title"=>$event['scores_page_title'], "dataname"=> $dataname, "race_info"=>$best_times);
				//$details['race_info']= $best_times;
				$details['customers']= $racer_info;
				$race_details_array[]= $details;
			}
		}
		$JSON= createJSON($race_details_array);
echo "JSON Packet:".$JSON."<br><br>";
		global $debugging;
		global $force_time_update;
		if($debugging){
			if($error_string){
				echo "Errors are: \n". $error_string;
			}else{
				echo "Debugging output is:\n".$debugging_str;
			}
			echo" the latest time is:".$latest_time;
			echo "The json is:".print_r($JSON, true);
		}
                performCURL($JSON);
		updateTransferTime($latest_time, $dataname);
	}
	function log_error($message){
		mail("niallsc@poslavu.com","Race Results Error", $message);
		echo "\nThe error message is: \n". $message;
	}
	function getLocations(){
		$names=array();
		$query =mlavu_query("select `dataname` from `poslavu_MAIN_db`.`restaurant_locations` where `component_package`='4' OR `component_package`='1' ");
		while($read= mysqli_fetch_assoc($query)){
			$names[]=$read['dataname'];
		}
		return $names;
	}
	function getRacerDetails($custID,$dataname){
		lavu_connect_dn($dataname,'poslavu_'.$dataname.'_db');
		$custQuery=lavu_query("select `f_name`,`l_name`,`racer_name`,`email`,`last_activity`,`phone_number`,`birth_date`,`id` as `customer_id` from `poslavu_[2]_db`.`lk_customers` where `id`='[1]'", $custID, $dataname);
		if(mysqli_num_fields($custQuery)){
			$updateQuery=lavu_query("update `poslavu_[2]_db`.`lk_customers` set `send_status`='1' where `id`='[1]'", $custID, $dataname);
			$return_array=  mysqli_fetch_assoc($custQuery);
			foreach($return_array as $key=>$cust_info ){
				$return_array[$key]= clean($cust_info);
			}
			$return_array['dataname']= $dataname;
			return $return_array;
		}else
			return "failed";
	}
	function getLastTransferTime($dataname){
	    /*
		if( file_exists( dirname(__FILE__) . "/sendInformation/transferTime_".$dataname.".txt")){
			$myFile = dirname(__FILE__) . "/sendInformation/transferTime_".$dataname.".txt";
			$fh = fopen($myFile, 'r');
			if($fh)
			{
				$time = fread($fh, filesize($myFile));
				fclose($fh);
				return $time;
			}else{
				return "";
			}
		}else{
			return "";
		}
             */
	     $result = mlavu_query("SELECT * FROM `racer_data`.`transfer_times` WHERE `dataname`='[1]'",$dataname);
	     if(!$result){ error_log("Query error -lisuhdfa-"); }
	     if(mysqli_num_rows($result) == 0){ error_log("no data for query -srer4s-"); }
             $row = mysqli_fetch_assoc($result);
	     return $row['last_transfer_time'];
	}
	function updateTransferTime($lastTime, $dataname){
	   /*
		if(!stristr($lastTime,'error')){
			$myFile = dirname(__FILE__) . "/sendInformation/transferTime_".$dataname.".txt";
			$fh = fopen($myFile, 'w');
			$time = fwrite($fh, $lastTime);
			fclose($fh);
			error_log("successfully updated transfer time for : ". $dataname);
			return true;
		}else{
			error_log("There was an error updating last transfer time for dataname: ". $dataname);
			return false;
		}

	    */
	    $result = mlavu_query("UPDATE `racer_data`.`transfer_times` SET `last_transfer_time`='[1]' WHERE `dataname`='[2]'",$lastTime,$dataname);
	    return $result ? true : false;	
	}
	function createJSON($eventArray){
		return json_encode($eventArray);
	}
	function clean($var){
		return preg_replace('/[^A-Za-z0-9\-]/', '', $var); // Removes special chars.;
	}
	function performCURL($JSON){
		srand(make_seed());
		$rand= rand();
		//$json_url = 'http://54.243.176.215/getUpdate_v2.php?debugging=true&randomNumber='.$rand;  // jSON URL which should be requested
		$json_url = 'http://localhost/p2r_race_scores.php?debugging=true&randomNumber='.$rand;
		$json_string = $JSON;				// jSON String for request
		$ch = curl_init( $json_url );		// Initializing curl
		$options = array(					// Configuring curl options
			CURLOPT_FRESH_CONNECT => 1,
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_POST=> 1,
			CURLOPT_POSTFIELDS => "json=".$json_string
		);
		curl_setopt_array( $ch, $options );	// Setting curl options
		$result =  curl_exec($ch); // Getting jSON result string
		curl_close($ch);
echo "Sending JSON:".$JSON."<br>";
echo "Curl Result:".$result."<br>";
	    return $result;
	}
	function make_seed(){
	  list($usec, $sec) = explode(' ', microtime());
	  return (float) $sec + ((float) $usec * 100000);
	}
?>
