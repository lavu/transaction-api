<?php

	$display = "";

	if($in_lavu) {
		
		$mode = (isset($_REQUEST['m']))?$_REQUEST['m']:"list";

		$all_users = array();
		$active_users = array();
		
		$get_user_list = lavu_query("SELECT * FROM `users`");
		while ($user_info = mysqli_fetch_assoc($get_user_list)) {
			$all_users[$user_info['id']] = $user_info['f_name']." ".$user_info['l_name'];
			if ($user_info['_deleted'] != "1") {
				$active_users[$user_info['id']] = $user_info['f_name']." ".$user_info['l_name'];
			}
			/*if (($user_info['loc_id'] = "0") || ($user_info['loc_id'] == $locationid)) {
				$location_users[$user_info['id']] = $user_info['f_ name']." ".$user_info['l_name'];
			}*/
		}
		
		if (isset($_REQUEST['posted']) && ($_REQUEST['posted'] == 1)) {
			$local_time = localize_datetime(date("Y-m-d H:i:s"), $location_info['timezone']);
			$vars = array();
			$vars['loc_id'] = $locationid;
			$vars['local_time'] = $local_time;
			$vars['subject'] = $_REQUEST['subject'];
			$vars['message'] = $_REQUEST['message'];
			$vars['type'] = "lk_server";
			$vars['from_id'] = admin_info("loggedin");
			$vars['from_name'] = admin_info("fullname");
			$vars['seq_no'] = assignNext("seq_no", "messages", "loc_id", $locationid);
			$vars['sent_to'] = $_REQUEST['recipient'];
			if ($_REQUEST['recipient'] == "All") {
				$user_ids = array_keys($active_users);
				foreach ($user_ids as $user_id) {
					$q_fields = "";
					$q_values = "";
					$vars['server_id'] = $user_id;
					$vars['server_name'] = $active_users[$user_id];
					$keys = array_keys($vars);
					foreach ($keys as $key) {
						if ($q_fields != "") { $q_fields .= ", "; $q_values .= ", "; }
						$q_fields .= "`$key`";
						$q_values .= "'[$key]'";
					}
					$send_message = lavu_query("INSERT INTO `messages` ($q_fields, `server_time`) VALUES ($q_values, now())", $vars);
				}
			} else {
				$q_fields = "";
				$q_values = "";
				$vars['server_id'] = $_REQUEST['recipient'];
				$vars['server_name'] = $all_users[$_REQUEST['recipient']];
				$keys = array_keys($vars);
				foreach ($keys as $key) {
					if ($q_fields != "") { $q_fields .= ", "; $q_values .= ", "; }
					$q_fields .= "`$key`";
					$q_values .= "'[$key]'";
				}
				$send_message = lavu_query("INSERT INTO `messages` ($q_fields, `server_time`) VALUES ($q_values, now())", $vars);
			}
			
			schedule_remote_to_local_sync($locationid, "table updated", "messages");
			
			$mode = "list";
		}
		
		echo "<div style='font-size:18px; color:#ffffff; background-color:#ACACAC; font-weight:bold; width:360px; text-align:center'>Messages</div>";
		
		if ($mode == "new") {
					
			//getFieldInfo($user_info['loc_id'], "id", "locations", "title");
			//<option value='Location'>".speak("This Location Employees")."</option>";

			$display = "<form name='form1' method='post' action=''><br><br>
				<a href='index.php?mode=admin_messages'><< View List</a><br><br>
				<input type='hidden' name='posted' value='1'>
				<table style='border:solid 2px #aaaaaa'>
					<tr><td colspan='2' bgcolor='#cccccc' align='center' style='height:27px; padding:5px 0px 2px 0px;'>&nbsp;<span style='font-size:12px; color:#333333;'><b>New Message</b></span>&nbsp;</td></tr>
					<tr>
						<td align='right' bgcolor='#eeeeee' style='padding-left:32px;' valign='top'>".speak("Recipient(s)").":</td>
						<td align='left' style='padding-right:32px'>
							<select name='recipient'>
								<option value=''></option>
								<option value='All'>".speak("All Employees")."</option>";
			$user_keys = array_keys($all_users);
			foreach ($user_keys as $key) {
				$display .= "<option value='".$key."'>".$all_users[$key]."</option>";
			}
			$display .= "</select>
						</td>
					</tr>
					<tr>
						<td align='right' bgcolor='#eeeeee' style='padding-left:32px;' valign='top'>".speak("Subject").":</td>
						<td align='left' style='padding-right:32px'><input type='text' name='subject' size='80'></td></tr>
					<tr>
						<td align='right' bgcolor='#EEEEEE' style='padding-left:32px;' valign='top'>".speak("Message").":</td>
						<td align='left' style='padding-right:32px'><textarea name='message' cols='60' rows='10'></textarea></td>
					</tr>
					<tr>
						<td align='right' bgcolor='#EEEEEE' style='padding-left:32px;' valign='top'></td>
						<td align='left' style='padding-right:32px'><input type='button' value='".speak("Send")."' onclick='form1_validate()'></td>
					</tr>
				</table>
			</form>";
			
		} else if ($mode == "view") {
		
			$get_message_info = lavu_query("SELECT * FROM `messages` WHERE `loc_id` = '[1]' AND `seq_no` = '[2]' LIMIT 1", $locationid, $_REQUEST['seq_no']);
			$message_info = mysqli_fetch_assoc($get_message_info);
			$sent_to = "All";
			if ($message_info['sent_to'] != "All") {
				$sent_to = $all_users[$message_info['sent_to']];
			}
			$display = "<br><br><a href='index.php?mode=admin_messages'><< View List</a><br><br>
				<table style='border:solid 2px #AAAAAA'>
					<tr><td colspan='2' bgcolor='#CCCCCC' align='center' style='height:9px; padding:5px 0px 2px 0px;'></td></tr>
					<tr>
						<td align='right' bgcolor='#EEEEEE' style='padding-left:32px;' valign='top'>".speak("Sent To").":</td>
						<td align='left' bgcolor='#FFFFFF' style='padding-right:32px'>".$sent_to."</td>
					</tr>
					<tr>
						<td align='right' bgcolor='#EEEEEE' style='padding-left:32px;' valign='top'>".speak("Sent By").":</td>
						<td align='left' bgcolor='#FFFFFF' style='padding-right:32px'>".$message_info['from_name']."</td>
					</tr>
					<tr>
						<td align='right' bgcolor='#EEEEEE' style='padding-left:32px;' valign='top'>".speak("Date/Time").":</td>
						<td align='left' bgcolor='#FFFFFF' style='padding-right:32px'>".$message_info['local_time']."</td>
					</tr>
					<tr>
						<td align='right' bgcolor='#EEEEEE' style='padding-left:32px;' valign='top'>".speak("Subject").":</td>
						<td align='left' bgcolor='#FFFFFF' style='padding-right:32px'>".$message_info['subject']."</td>
					</tr>
					<tr><td colspan='2' bgcolor='#CCCCCC' align='center' style='height:5px; padding:5px 0px 2px 0px;'></td></tr>
					<tr>
						<td align='right' bgcolor='#EEEEEE' style='padding-left:32px;' valign='top'>".speak("Message").":</td>
						<td align='left' bgcolor='#FFFFFF' style='padding-right:32px'>".nl2br($message_info['message'])."</td>
					</tr>
					<tr><td colspan='2' bgcolor='#CCCCCC' align='center' style='height:9px; padding:5px 0px 2px 0px;'>&nbsp;</td></tr>
				</table>";
			
		} else if ($mode == "list") {
		
			$display = "<br><br><a href='index.php?mode=admin_messages&m=new'><img src='images/send_new.png' border='0'/></a><br>&nbsp;";

			$page = (isset($_REQUEST['page']))?$_REQUEST['page']:"1";

			$vars['loc_id'] = $locationid;
			$vars['type'] = "lk_server";
			$vars['page'] = $page;
	
			$filter = "WHERE `loc_id` = '[loc_id]' AND `type` = '[type]' GROUP BY `seq_no`";
 			$limit = "LIMIT ".(((int)$vars['page'] - 1) * 25).", 25";
	
			$get_count = lavu_query("SELECT COUNT(`id`) FROM `messages` $filter", $vars);
			$message_count = mysqli_result($get_count, 0);

			if ((($vars['page'] - 1) * 25) > $message_count) {
				$vars['page'] = ($vars['page'] - 1);
			}
	
			$prev_code = "";
			if ($vars['page'] > 1) {
				$prev_code = ($vars['page'] - 1);
			}
			$next_code = "";
			if ($message_count > ($vars['page'] * 25)) {
				$next_code = ($vars['page'] + 1);
			}
			if (($prev_code != "") || ($next_code != "")) {
				$nav_code = "<tr><td align='center'><table width='100%' cellspacing='0' cellpadding='3'><tr><td align='left'>$prev_code</td><td align='right'>$next_code</td></tr></table></td></tr>";
			}
			
			$get_messages = lavu_query("SELECT * FROM `messages` $filter ORDER BY `server_time` DESC $limit", $vars);
			if (@mysqli_num_rows($get_messages) > 0) {
				$display .= "<table style='border:solid 1px black' cellspacing='0' cellpadding='4'>
					<tr>
						<td align='center' style='padding-left:4px; padding-right:4px; border-bottom:solid 1px black' bgcolor='#CDCDCD'>Sent To</td>	
						<td align='center' style='padding-left:4px; padding-right:4px; border-bottom:solid 1px black' bgcolor='#CDCDCD'>Sent By</td>
						<td align='center' style='padding-left:4px; padding-right:4px; border-bottom:solid 1px black' bgcolor='#CDCDCD'>Date/Time</td>
						<td align='center' style='padding-left:4px; padding-right:4px; border-bottom:solid 1px black' bgcolor='#CDCDCD'>Subject</td>
					</tr>";
				while ($message_info = mysqli_fetch_assoc($get_messages)) {
					$sent_to = "All";
					if ($message_info['sent_to'] != "All") {
						$sent_to = $all_users[$message_info['sent_to']];
					}
					$display .= "<tr onmouseover='this.bgColor = \"#AABBEE\"' onmouseout='this.bgColor = \"#FFFFFF\"' style='cursor:pointer'>
						<td valign='top' onclick='window.location = \"index.php?mode=admin_messages&m=view&seq_no=".$message_info['seq_no']."\"'>".$sent_to."</td>
						<td valign='top' onclick='window.location = \"index.php?mode=admin_messages&m=view&seq_no=".$message_info['seq_no']."\"'>".$message_info['from_name']."</td>
						<td valign='top' onclick='window.location = \"index.php?mode=admin_messages&m=view&seq_no=".$message_info['seq_no']."\"'>".$message_info['local_time']."</td>
						<td valign='top' onclick='window.location = \"index.php?mode=admin_messages&m=view&seq_no=".$message_info['seq_no']."\"'>".$message_info['subject']."</td>
					</tr>";
				}
				$display .= "</table>";
			} else {
				$display .= "<br>No messages on record.";
			}
		}
	}
?>

<script language='javascript'>
	function isEmpty(data) {
		for (var i=0; i<data.length; i++) {
			if(data.substring(i,i+1) != " ")
				return false;
			}
			return true;
		}

	function form1_validate() {
		if(isEmpty(form1.recipient.value)) {
			alert("Please select a recipient...");
			form1.recipient.focus();
		} else if(isEmpty(form1.subject.value)) {
			alert("Please enter a subject...");
			form1.subject.focus();
		} else {
			document.form1.submit();
		}
	}
</script>

<?php echo $display; ?>