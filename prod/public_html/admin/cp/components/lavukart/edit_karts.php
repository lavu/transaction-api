<?php
	if($in_lavu)
	{
		display_header("Karts");
		echo "<br><br>";
		
		$tablename = "lk_karts";
		$forward_to = "index.php?mode={$section}_{$mode}";
		$filter_by = "`locationid`='$locationid'";
		$back_to_title = "<< View Kart List";
		
		$track_list = array();
		$track_query = lavu_query("select * from `lk_tracks` where `locationid`='$locationid' order by `title` asc");
		while($track_read = mysqli_fetch_assoc($track_query))
		{
			$trackid = $track_read['id'];
			$track_list[$trackid] = $track_read['title'];
		}
		
		$lane_list = array("");
		for($i=1; $i<=6; $i++)
		{
			$lane_list[$i] = $i;
		}
		
		$fields = array();
		$fields[] = array(speak("Number"),"number","text","","list:yes");
		$fields[] = array(speak("Track"),"trackid","select",$track_list,"list:yes");
		$fields[] = array(speak("Transponder Id"),"transponderid","text","","list:yes");
		$fields[] = array(speak("Lane"),"lane","select",$lane_list,"list:yes");
		$fields[] = array("Location","locationid","hidden","","setvalue:$locationid");
		$fields[] = array(speak("Submit"),"submit","submit");

		require_once(resource_path() . "/browse.php");
	}
?>
