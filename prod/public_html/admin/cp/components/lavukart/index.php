<?php
	if($in_lavu)
	{
		if($mode=="billing")
		{
			$p_area = "customer_billing_info";
			$suppress_billing_alert = true;
			require_once("areas/billing.php");
		}
		else if(substr($mode,0,5)=="edit_") //$mode=="edit_customers" || $mode=="edit_events" || $mode=="edit_tracks" || $mode=="edit_event_types")
		{
			require_once($comdir . "/" . $mode . ".php");
		}
		else if(substr($mode,0,6)=="events")
		{
			create_tabs(array("View Events","events_edit_events"),
						//array("Schedule Events","events_schedule_events"),
						array("List Race Results","events_list_results"),
						array("Edit Event Types","events_edit_event_types"),
						array("Edit Event Sequences","events_edit_event_sequences"));
			if(substr($mode,0,7)=="events_")
			{
				require_once($comdir . "/" . substr($mode,7) . ".php");
			}
		}
		else if(substr($mode,0,4)=="view")
		{
			require_once($mode . ".php");
		}
		else if(substr($mode,0,5)=="admin")
		{
		
		    
		    
		    
		    if($data_name == '17kart'){
    		    create_tabs(array("Edit Tracks","admin_edit_tracks"),
    						array("Edit Rooms","admin_edit_rooms"),
    						array("Edit Event Types","admin_edit_event_types"),
    						array("Edit Event Sequences","events_edit_event_sequences"),
    						array("Email Marketing","admin_email_marketing"),
    						array("Messages","admin_messages"),
    						array("Monitoring","admin_monitoring"),
    						array("Group Event Checklist","edit_event_checklist"),
    						array("Waivers","admin_waivers"),
    						array("Sales Reps","edit_sales_reps"),
    						array("Inventory","admin_edit_inventory"),
    						array("Service Types","admin_edit_service_types"), 
    						array("Service Checklists", "admin_edit_service_checklists") );
		    }
		    else{
    			create_tabs(array("Edit Tracks","admin_edit_tracks"),
    						array("Edit Rooms","admin_edit_rooms"),
    						array("Edit Event Types","admin_edit_event_types"),
    						array("Edit Event Sequences","events_edit_event_sequences"),
    						array("Email Marketing","admin_email_marketing"),
    						array("Messages","admin_messages"),
    						array("Monitoring","admin_monitoring"),
    						array("Group Event Checklist","edit_event_checklist"),
    						array("Waivers","admin_waivers"),
    						array("Sales Reps","edit_sales_reps"),
    						array("Inventory","admin_edit_inventory") );
            }
						
						
						
						
						
			if(substr($mode,0,6)=="admin_")
			{
				require_once($comdir . "/" . substr($mode,6) . ".php");
			}
		}
		else if(substr($mode,0,7)=="reports")
		{
			$report_mode = substr($mode,8);
			
			$tablist = array();
			$report_query = mlavu_query("select * from `poslavu_MAIN_db`.`reports` where `display`='1' and `component_package`='1' order by `title` asc");
			while($report_read = mysqli_fetch_assoc($report_query))
			{
				$tablist[] = array($report_read['title'],"reports_id_".$report_read['id']);
			}
			create_tabs("array",$tablist);
			
			if($report_mode!="menu" && $report_mode!="")
			{
				if(substr($report_mode,0,3)=="id_")
				{
					$date_mode = "unified";
					$reportid = substr($report_mode,3);
					require_once("resources/custom_report.php");
				}
				else
				{
					require_once("areas/reports/".$report_mode.".php");
				}
			}
		}
		else
		{
			//echo "<a href='index.php?mode=edit_customers'>Edit Customers</a>";
			//echo "<br><br><a href='index.php?mode=edit_tracks'>Edit Tracks</a>";
			//echo "<br><br><a href='index.php?mode=edit_event_types'>Edit Event Types</a>";
			//echo "<br><br><a href='index.php?mode=edit_events'>Edit Events</a>";
			//echo "<br><br><a href='index.php?menu_mode=pos'>POS</a>";
			?>
            <table><td height="62">&nbsp;</td></table>
            <table width="924" height="344" border="0" cellpadding="0" cellspacing="0" background="images/content_menu_bottom.png">
                <tr>
                    <td width="84" height="245">&nbsp;</td>
                    <td width="134" align="left" valign="top"><a href='index.php?mode=edit_karts'><img src="components/lavukart/images/btn_1.png" width="126" height="219" border="0"></a></td>
                    <td width="163" align="left" valign="top"><a href='index.php?mode=edit_customers'><img src="components/lavukart/images/btn_2.png" width="141" height="229" border="0"></a></td>
                    <td width="162" align="left" valign="top"><a href='index.php?mode=admin'><img src="components/lavukart/images/btn_3.png" width="153" height="225" border="0"></a></td>
                    <td width="163" align="left" valign="top"><a href='index.php?mode=reports'><img src="components/lavukart/images/btn_4.png" width="141" height="229" border="0"></a></td>
                    <td width="134" align="left" valign="top"><a href='index.php?mode=events'><img src="components/lavukart/images/btn_5.png" width="131" height="219" border="0"></a></td>
                    <td width="84" align="left" valign="top">&nbsp;</td>
                </tr>
                <tr>
                    <td height="99" colspan="7" align="center" valign="middle">&nbsp;</td>
                </tr>
            </table>            
            <?php
		}
	}
?>
