<?php
	if($in_lavu)
	{
		display_header("Schedule Events");
		echo "<br><br>";
		
		$track_list = array();
		$track_query = lavu_query("select * from `lk_tracks` where `locationid`='$locationid' order by `title` asc");
		while($track_read = mysqli_fetch_assoc($track_query))
		{
			$trackid = $track_read['id'];
			$track_list[$trackid] = $track_read['title'];
		}
		$etype_list = array();
		$etype_query = lavu_query("select * from `lk_event_types` order by `title` asc");
		while($etype_read = mysqli_fetch_assoc($etype_query))
		{
			$etypeid = $etype_read['id'];
			$etype_list[$etypeid] = $etype_read['title'];
		}
		
		$tablename = "";
		$fields = array();
		$fields[] = array("Date","date","calendar","","list:yes");
		$fields[] = array("First Time","first_time","time","","list:yes");
		$fields[] = array("Time Span","time_span","timespan");
		$fields[] = array("Last Time","last_time","time","","list:yes");
		$fields[] = array("Track","trackid","select",$track_list);
		$fields[] = array("Location","locationid","hidden","","setvalue:$locationid");
		$fields[] = array("Type","type","select",$etype_list,"list:yes");
		$fields[] = array("Submit","submit","submit");

		function form_submitted()
		{
			$time_span = $_POST['time_span'];
			$first_time = $_POST['first_time'];
			$last_time = $_POST['last_time'];
			
			function time_advance($time, $add)
			{
				$time_hours = $time - ($time % 100);
				$time_minutes = $time % 100;
				$add_hours = $add - ($add % 100);
				$add_minutes = $add % 100;
				$hours = $time_hours + $add_hours;
				$minutes = $time_minutes + $add_minutes;
				if($minutes >= 60)
				{
					$hours += 100;
					$minutes -= 60;
				}
				return $hours + $minutes;
			}
			
			for($t=$first_time; $t<=$last_time;  $t=time_advance($t,$time_span))
			{
				$ev_query = lavu_query("select * from `lk_events` where `type`='$set_type' and `date`='$set_date' and `time`='$t'");
				if(mysqli_num_rows($ev_query) < 1)
				{
					$set_locationid = $_POST['locationid'];
					$set_type = $_POST['type'];
					$set_date = $_POST['date'];
					$set_trackid = $_POST['trackid'];
					$dparts = explode("-",$set_date);
					$set_year = $dparts[0];
					$set_month = $dparts[1];
					$set_day = $dparts[2];
					$set_min = $t % 100;
					$set_hour = (($t - $set_min) / 100);
					$set_time = $t;
					$set_ts = mktime($set_hour,$set_min,0,$set_month,$set_day,$set_year);
					lavu_query("insert into `lk_events` (`locationid`,`type`,`date`,`time`,`ts`,`trackid`) values ('$set_locationid','$set_type','$set_date','$set_time','$set_ts','$set_trackid')");
				}
			}
			//echo "<br>time span: $time_span";
			//echo "<br>first time: $first_time";
			//echo "<br>last time: $last_time";
		}
		
		require_once(resource_path() . "/form.php");
	}
?>
