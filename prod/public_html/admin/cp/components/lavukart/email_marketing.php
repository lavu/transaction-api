<?php
		$forward_to = "index.php?mode={$section}_{$mode}";
		$tablename = "locations";
		$rowid = $locationid;
		
		$ml_type_list = array();
		$ml_type_list[""] = "";
		$ml_type_list["Critical Impact"] = "Critical Impact";
		
		$fields = array();
		$fields[] = array(speak("Mailing List Type"),"ml_type","select",$ml_type_list);
		$fields[] = array(speak("Mailing List Username"),"ml_un","text","size:40");
		$fields[] = array(speak("Mailing List Password"),"ml_pw","text_encrypted","size:40,key:p2r_key");
		$fields[] = array(speak("Mailing List ListId"),"ml_listid","text","size:40");
		
		$fields[] = array(speak("Submit"),"submit","submit");
		
		require_once(resource_path() . "/form.php");	
?>
