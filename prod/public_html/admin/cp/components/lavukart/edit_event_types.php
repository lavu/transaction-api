<?php
    if($in_lavu)
    {
        display_header("Event Types");
        echo "<br><br>";

        $tablename = "lk_event_types";
        $forward_to = "index.php?mode={$section}_{$mode}";
        $filter_by = "`locationid`='$locationid'";
        $back_to_title = "<< View Event Type List";

        $fields = array();
        $fields[] = array(speak("Title"),"title","text","enabled","list:yes");
        $fields[] = array(speak("Scores Page Title"),"scores_page_title","text","enabled","list:yes");
        $fields[] = array("Location","locationid","hidden","enabled","setvalue:$locationid");
        $fields[] = array(speak("Score By"),"score_by","select",array("Best Lap","Position"),"list:yes");
        $fields[] = array(speak("Laps"),"laps","text","Enabled");
        $fields[] = array(speak("Slots"),"slots","text","Enabled");
        $fields[] = array(speak("Use Credits"),"use_credits","select",array("Yes"=>"Yes","No"=>"No"),"list:yes");
        $fields[] = array(speak("Advance by Position"),"advance_by_position","select",array("No"=>"No","Yes"=>"Yes"),"list:yes");
        $fields[] = array(speak("Display Order"),"_order","text","enabled");
        $fields[] = array(speak("Submit"),"submit","submit");

        require_once(resource_path() . "/browse.php");
    }
?>
