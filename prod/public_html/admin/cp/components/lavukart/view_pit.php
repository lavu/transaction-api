<?
	session_start();

	function com_path()
	{
		if(isset($_GET['dev']) && $_GET['dev']=="0")
			return "/components";
		else
			return "/dev/components";
	}
	//ini_set("display_errors","1");
	require_once("/home/poslavu/public_html/admin/cp/resources/core_functions.php");
	$maindb = "poslavu_MAIN_db";
	$in_lavu = true;
	$rdb = admin_info("database");
	lavu_connect_byid(admin_info("companyid"),$rdb);
	//lavu_select_db($rdb);
	//if(isset($_GET['locationid'])) set_sessvar("locationid",$_GET['locationid']);
	$loc_id = sessvar("locationid");
	
	
	if(admin_loggedin())
	{
		$cc = admin_info("dataname") . "_key_" . lsecurity_key(admin_info("companyid"));
		$dn = admin_info("dataname");
	}
	else
	{
		echo "Access Denied";
		exit();
	}
	
	// comp_name
	// cc
	// dn
	// loc_id
	// server_id
	// server_name
	// tab_name
	
	//$cc = "DEVKART_key_88347";//"pole_position_raceway_key_113838";
	//$dn = "DEVKART";//"pole_position_racekway";
	//$loc_id = 8;//9;
	//$cc = "pole_position_raceway_key_113838";
	//$dn = "pole_position_racekway";
	//$loc_id = 9;
	$server_id = 1;
	$server_name = "tmp";
	if(isset($_GET['ctab']))
	{
		$ctab = $_GET['ctab'];
		$_SESSION['ctab'] = $ctab;
	}
	else if(isset($_SESSION['ctab']))
		$ctab = $_SESSION['ctab'];
	else
		$ctab = 1;
	if($ctab==1) $tab_name = "Schedule";
	else if($ctab==2) $tab_name = "Customers";
	else if($ctab==3) $tab_name = "Pit";
	else if($ctab==4) $tab_name = "Scoreboard";
	else if($ctab==5) $tab_name = "Group Events";
	else if($ctab==6) $tab_name = "Pit 2";
	else if($ctab==8) $tab_name = "New Schedule";
	
	$main_comvars = "cc=$cc&dn=$dn&tab_name=$tab_name&loc_id=$loc_id&server_id=$server_id&server_name=$server_name";
	
	ob_start();
	?>
		<script language="javascript">
			String.prototype.trim = function () {
				return this.replace(/^\s*/, "").replace(/\s*$/, "");
			}
			
			function do_com_cmd(str)
			{
				vars = new Array();
				url_split = str.split("?");
				if(url_split.length > 1)
					url_vars = url_split[1];
				else
					url_vars = "";
				str = url_split[0];
				parts = str.split("&");
				for(i=0; i<parts.length; i++)
				{
					part = parts[i].split("=");
					if(part.length > 1)
					{
						set_key = part[0].trim();
						set_val = part[1].trim();
						vars[set_key] = set_val;
					}
				}
				
				cmd = vars["cmd"];
				if(cmd=="show_pit_setup")
				{
					//_DO:cmd=create_overlay&f_name=lavukart/newevent_wrapper.php;lavukart/select_track.php&c_name=select_track_wrapper;select_track&dims=112,200,423,374;112,200,423,374
					cmd = "create_overlay";
					vars["f_name"] = "lavukart/newevent_wrapper.php;lavukart/select_grid.php";
					vars["c_name"] = "select_grid_wrapper;select_grid";
					vars["dims"] = "112,200,423,374;112,200,423,374";
					vars["scroll"] = "0;0";
					url_vars = "locid=<?php echo $loc_id?>";
				}
				if(cmd=="create_overlay")
				{
					f_name = vars["f_name"].split(";");
					c_name = vars["c_name"].split(";");
					dims = vars["dims"].split(";");
					if(typeof(vars["scroll"])!="undefined")
					{
						scrollable = vars["scroll"].split(";");
						scroll_exists = true;
					}
					else
					{
						scrollable = "";
						scroll_exists = false;
					}
					
					for(i=0; i<f_name.length; i++)
					{
						dimlist = dims[i].split(",");
						if(dimlist.length > 3)
						{
							set_left = dimlist[0];
							set_top = dimlist[1];
							set_width = dimlist[2];
							set_height = dimlist[3];
							set_zindex = (php_zindex * 1 + 1000);
							if(scroll_exists) set_scroll = scrollable[i];
							else set_scroll = "0";
							//alert(set_zindex);
							window.parent.create_overlay(f_name[i],c_name[i],set_left,set_top,set_width,set_height,set_scroll,set_zindex,url_vars);
						}
					}
					window.parent.record_overlay_group(c_name);
				}
				else if(cmd=="close_overlay")
				{
					if(typeof(vars["f_name"])!="undefined")
					{
						f_name = vars["f_name"].split(";");
						c_name = vars["c_name"].split(";");
						
						for(i=0; i<f_name.length; i++)
						{
							window.parent.component_load_new(c_name[i],f_name[i],url_vars);
						}
					}
					window.parent.close_overlay("php_self");
				}
				else if(cmd=="load_url")
				{
					if(typeof(vars["f_name"])!="undefined")
					{
						f_name = vars["f_name"].split(";");
						c_name = vars["c_name"].split(";");
						
						for(i=0; i<f_name.length; i++)
						{
							window.parent.component_load_new(c_name[i],f_name[i],url_vars);
						}
					}
				}
				else if(cmd=="send_js")
				{
					c_name = vars["c_name"];
					c_js = vars["js"];
					
					window.parent.component_run_script(c_name,c_js);
				}
				else if(cmd=="make_hidden" || cmd=="make_visible")
				{
					if(typeof(vars["c_name"])!="undefined")
					{
						c_name = vars["c_name"].split(";");
						
						for(i=0; i<c_name.length; i++)
						{
							window.parent.component_set_visibility(c_name[i],cmd.substring(5));
						}
					}
				}
				else
				{
					alert("unknown command: " + cmd);
				}
			}
		</script>
	<?php
	$com_script_code = ob_get_contents();
	ob_end_clean(); 	
	
	if(isset($_GET['drawmode']) && $_GET['drawmode']=="draw_component")
	{
		$fname = $_GET['fname'];
		/*$cc = $_GET['cc'];
		$dn = $_GET['dn'];
		$tab_name = $_GET['tab_name'];
		$loc_id = $_GET['loc_id'];
		$server_id = $_GET['server_id'];
		$server_name = $_GET['server_name'];*/
		$self = $_GET['self'];
		$zindex = $_GET['zindex'];
		$vars = $main_comvars;
		
		$gvars = "";
		foreach($_GET as $gkey => $gval)
		{
			if(trim($gkey)!="" && strpos($vars,trim($gkey)."=")===false)
			{
				if($gvars=="") $gvars .= "?"; else $gvars .= "&";
				$gvars .= trim($gkey) . "=" . trim($gval);
			}
		}
		
		/*$ch = curl_init("http://admin.poslavu.com/components/".$fname.$gvars);
		curl_setopt($ch, CURLOPT_HEADER, 0);
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $vars);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		$output = curl_exec($ch);
		curl_close($ch);*/
		ob_start();
		
		$var_parts = explode("&",$vars);
		for($n=0; $n<count($var_parts); $n++)
		{
			$var_part = explode("=",$var_parts[$n]);
			if(count($var_part) > 1)
			{
				$var_key = trim($var_part[0]);
				$var_val = trim($var_part[1]);
				if($var_key!="")
				{
					$_POST[$var_key] = $var_val;
					$_REQUEST[$var_key] = $var_val;
				}
			}
		}
		$var_parts = explode("&",$gvars);
		for($n=0; $n<count($var_parts); $n++)
		{
			$var_part = explode("=",$var_parts[$n]);
			if(count($var_part) > 1)
			{
				$var_key = trim($var_part[0]);
				if(strlen($var_key) > 0 && substr($var_key,0,1)=="?")
					$var_key = substr($var_key,1);
				$var_val = trim($var_part[1]);
				if($var_key!="")
				{
					$_GET[$var_key] = $var_val;
					$_POST[$var_key] = $var_val;
					$_REQUEST[$var_key] = $var_val;
					//echo $var_key . " = " . $var_val . "<br>";
				}
			}
		}
		require_once("/home/poslavu/public_html/admin".com_path()."/".$fname);
		
		$output = ob_get_contents();
		ob_end_clean();
		
		$com_script_code = str_replace("php_self",$self,$com_script_code);
		$com_script_code = str_replace("php_zindex",$zindex,$com_script_code);
		$output = str_replace("<head>","<head>\n" . $com_script_code,$output);
		$output = str_replace("images/",com_path() . "/lavukart/images/",$output);
		$output = str_replace("'".basename($fname)."?","'view_pit.php?drawmode=draw_component&fname=$fname&self=$self&zindex=$zindex&",$output);
		$output = str_replace("\"".basename($fname)."?","\"view_pit.php?drawmode=draw_component&fname=$fname&self=$self&zindex=$zindex&",$output);
		
		$output = str_replace("'race_scores.php?","'".com_path()."/lavukart/race_scores.php?",$output);
		$output = str_replace("\"race_scores.php?","\"".com_path()."/lavukart/race_scores.php?",$output);
		
		function parse_cmd($str,$leading,$type)
		{
			//$str_sep = strpos($str,$leading);
			$rc = $leading;
			if($type=="a")
			{
				$ac = "href=".$leading;
			}
			else
			{
				$ac = $leading;
			}
			$ldone = false;
			$in_quote = true;
			$lnk = "";
			$oplead = "";
			if($leading=="'") $oplead = '"';
			else if($leading=='"') $oplead = "'";
			for($n=0; $n<strlen($str); $n++)
			{
				$ch = $str[$n];
				if($ldone)
				{
					$rc .= $ch;
					$ac .= $ch;
				}
				if($ch==$leading)
					$in_quote = !$in_quote;
				if(!$ldone && !$in_quote && ($ch==";" || $ch==">" || $ch=="/" || ($ch=="\"" && $leading!="\"") || ($ch=="'" && $leading!="'")))
				{
					$rc .= "#".$leading.$ch;
					$ac .= "#".$leading;
					
					$inner_code = "";
					$l_parts = explode("_DO:",$lnk);
					for($lp=0; $lp<count($l_parts); $lp++)
					{
						if($lp > 0) $inner_code .= "; ";
						$inner_code .= "do_com_cmd(".$leading.$l_parts[$lp];
						if(count($l_parts) > $lp + 1)
							$inner_code .= $leading;
						$inner_code .= ")";
					}
					if($type=="a")
					{
						$ac .= " onclick=".$oplead.$inner_code.$oplead;
					}
					else
					{
						$ac .= "; ".$inner_code;
					}
					
					$ac .= $ch;
					$ldone = true;
				}
				if(!$ldone)
					$lnk .= $ch;
			}
			return array($ac,$rc,$lnk);
			//return $leading . "#" . $leading . substr($str,$str_sep + 1);
		}
	
		$output_parts = explode("href='_DO:",$output);
		$output = "";
		$output .= $output_parts[0];
		for($i=1; $i<count($output_parts); $i++)
		{
			$pc = parse_cmd($output_parts[$i],"'","a");
			$output .= $pc[0];
		}

		$output_parts = explode('href="_DO:',$output);
		$output = "";
		$output .= $output_parts[0];
		for($i=1; $i<count($output_parts); $i++)
		{
			$pc = parse_cmd($output_parts[$i],'"',"a");
			$output .= $pc[0];
		}
		
		$output_parts = explode("'_DO:",$output);
		$output = "";
		$output .= $output_parts[0];
		for($i=1; $i<count($output_parts); $i++)
		{
			$pc = parse_cmd($output_parts[$i],"'","js");
			$output .= $pc[0];
		}

		$output_parts = explode('"_DO:',$output);
		$output = "";
		$output .= $output_parts[0];
		for($i=1; $i<count($output_parts); $i++)
		{
			$pc = parse_cmd($output_parts[$i],'"',"js");
			$output .= $pc[0];
		}
		$output = str_replace("window.location = '#';","",$output);
		$output = str_replace('window.location = "#";',"",$output);
		$output = str_replace("location = '#';","",$output);
		$output = str_replace('location = "#";',"",$output);
		$output = str_replace("href='#'","",$output);
		$output = str_replace('href="#"',"",$output);
		echo $output;	
	}
	else
	{
		?>
		
		<script language="javascript">
			function append_html_to_id(areaid, newhtml, setid)
			{
				var t = document.createElement('div');
				t.id = setid;
				t.name = setid;
				t.innerHTML = newhtml;
				document.getElementById(areaid).appendChild(t);
				//document.getElementById(areaid).innerHTML += newhtml;
				return;
			}
			function component_run_script(c_name,c_js)
			{
				//alert("document.getElementById('com_" + c_name + "').contentWindow." +c_js);
				eval("document.getElementById('com_" + c_name + "').contentWindow." + c_js);
			}
			function component_set_visibility(c_name,set_vis)
			{
				if(set_vis=="hidden") set_vis = "none"; else set_vis = "block";
				document.getElementById('com_' + c_name).style.display = set_vis;			
			}
			function component_load_new(c_name,f_name,url_vars)
			{
				set_zindex = z_index_list[c_name];
				set_comvars = "fname=" + f_name + "&self=" + c_name + "&zindex=" + set_zindex;
				if(url_vars!="")
					set_comvars += "&" + url_vars;
				document.getElementById('com_' + c_name).src = "view_pit.php?drawmode=draw_component&" + set_comvars;
			}
			var com_groups = new Array();
			function create_overlay(f_name,c_name,set_left,set_top,set_width,set_height,scrollable,set_zindex,set_urlvars)
			{
				set_comname = "com_" + c_name;
				div_comname = "comdiv_" + c_name;
				con_comname = "comcon_" + c_name;
				set_comvars = "fname=" + f_name + "&self=" + c_name + "&zindex=" + set_zindex;
				if(set_urlvars!="") set_comvars += "&" + set_urlvars;
				set_top = set_top - 138; // tabs 50, middle bar 50 bottom bar 38 
				if(scrollable==1) set_scrollable = "auto"; else set_scrollable = "no";

				comcode = "";
				comcode += "<div style='position:absolute; left:" + set_left + "; top:" + set_top + "; z-index:" + set_zindex + "' name='" + div_comname + "' id='" + div_comname + "'>";
				comcode += "<iframe allowtransparency='true' frameborder='0' scrolling='" + set_scrollable + "' style='visibility:hidden; width:" + set_width + "; height:" + set_height + "' name='" + set_comname + "' id='" + set_comname + "' src='view_pit.php?drawmode=draw_component&" + set_comvars + "' onload='this.contentWindow.document.body.style.backgroundColor = \"transparent\"; this.style.visibility = \"visible\"'></iframe>";
				comcode += "</div>";
				append_html_to_id("stage", comcode, con_comname);
				z_index_list[c_name] = set_zindex;
				//alert("create overlay: " + f_name);
			}
			function record_overlay_group(c_names)
			{
				com_groups[com_groups.length] = c_names;
			}
			function close_overlay(c_name)
			{
				found_in_group = false;
				found_in_group_index = 0;
				
				for(i=0; i<com_groups.length; i++)
				{
					for(n=0; n<com_groups[i].length; n++)
					{
						if(com_groups[i][n]==c_name)
						{
							found_in_group = true;
							found_in_group_index = i;
						}
					}
				}
				
				if(found_in_group)
				{
					for(n=0; n<com_groups[found_in_group_index].length; n++)
					{
						document.getElementById("stage").removeChild(document.getElementById("comcon_" + com_groups[found_in_group_index][n]));
						com_groups[found_in_group_index][n] = "_________";
					}
				}
				else
				{
					document.getElementById("comdiv_" + c_name).innerHTML = "";
				}
				return;
			}
		</script>
		
		<?php
		
		echo "<body bgcolor='#000000'>";
		echo "<center>";
		
		$layout_query = mlavu_query("select * from `poslavu_MAIN_db`.`component_layouts` where `id`='[1]'",$ctab);
		if(mysqli_num_rows($layout_query))
		{
			$layout_read = mysqli_fetch_assoc($layout_query);
			
			$background_image = $layout_read['background'];
			$c_ids = explode(",",$layout_read['c_ids']);
			$c_xs = explode(",",$layout_read['c_xs']);
			$c_ys = explode(",",$layout_read['c_ys']);
			$c_ws = explode(",",$layout_read['c_ws']);
			$c_hs = explode(",",$layout_read['c_hs']);
			$c_zs = explode(",",$layout_read['c_zs']);
			$c_types = explode(",",$layout_read['c_types']);
			
			$coms = array();
			for($i=0; $i<count($c_ids); $i++)
			{
				$placement = 0;
				for($n=0; $n<count($coms); $n++)
				{
					if($c_zs[$i] > $c_zs[$n])
						$placement = $n + 1; 
				}
				for($n=count($coms); $n>$placement; $n--)
				{
					$coms[$n] = $coms[$n-1];
				}
				$insert_com = array();
				$insert_com['id'] = $c_ids[$i];
				$insert_com['x'] = $c_xs[$i];
				$insert_com['y'] = $c_ys[$i];
				$insert_com['w'] = $c_ws[$i];
				$insert_com['h'] = $c_hs[$i];
				$insert_com['z'] = $c_zs[$i];
				$insert_com['type'] = $c_types[$i];
				
				$coms[$placement] = $insert_com;
			}
			
			$z_index_setup = "z_index_list = new Array(); \n";
			echo "<table cellspacing=0 cellpadding=0 width='1024'>";
			echo "<tr><td height='50' valign='bottom' align='left'>";
			
			function make_tab($title,$tnum,$selected)
			{
				$tabfile = "tab1.png";
				if($selected==$tnum)
					$tabfile = "tab2.png";
				echo "<td width='195' height='45' style='cursor:pointer; color:#ffffff; font-weight:bolde; font-family:Arial; font-size:16px; background: URL(http://admin.poslavu.com".com_path()."/lavukart/images/$tabfile); background-position:bottom middle; background-repeat:no-repeat' align='center' valign='middle' onclick='window.location = \"?ctab=".$tnum."\"'>".$title."</td>";
			}
			
			echo "<table cellspacing=0 cellpadding=0>";
			make_tab("Schedule",1,$ctab);
			make_tab("Group Events",5,$ctab);
			make_tab("Pit 2",6,$ctab);
			make_tab("New Schedule",8,$ctab);
			//make_tab("Pit",3,$ctab);
			//make_tab("Scoreboard",4,$ctab);
			//make_tab("Customer",2,$ctab);
			echo "</table>";
						
			echo "</td></tr>";
			echo "<tr><td height='88' style='background: URL(http://admin.poslavu.com".com_path()."/lavukart/images/bgtop.jpg); background-repeat:repeat-x; background-position: bottom middle'>&nbsp;</td></tr>";
			echo "<tr>";
			echo "<td style='width:1024px; height:620px' background='http://admin.poslavu.com".com_path()."/$background_image' align='left' valign='top'>";
			
			echo "<div style='position:relative' id='stage' name='stage'>";
			for($i=0; $i<count($coms); $i++)
			{
				$id = $coms[$i]['id'];
				$left = $coms[$i]['x'] . "px";
				$top = $coms[$i]['y'] . "px";
				$width = $coms[$i]['w'] . "px";
				$height = $coms[$i]['h'] . "px";
				$zindex = ($coms[$i]['z'] * 1000) + 1;
				$type = $coms[$i]['type'];
				
				echo "<div style='position:absolute; left:$left; top:$top; z-index:$zindex'>";
				
				$com_query = mlavu_query("select * from `poslavu_MAIN_db`.`components` where `id`='[1]'",$id);
				if(mysqli_num_rows($com_query))
				{
					$com_read = mysqli_fetch_assoc($com_query);
					$com_name = $com_read['name'];
					$com_filename = $com_read['filename'];
					$allow_scroll = $com_read['allow_scroll'];
					$com_type = $com_read['type'];
					$com_title = $com_read['title'];
					
					if($allow_scroll=="1") $scroll_mode = "auto"; else $scroll_mode = "no";
					
					$comname = "com_".$com_name;
					$z_index_setup .= "z_index_list['".$com_name."'] = $zindex; \n";
					$vars = "fname=$com_filename&zindex=$zindex&self=$com_name";
					echo "<iframe allowtransparency='true' frameborder='0' scrolling='$scroll_mode' style='width:$width; height:$height' name='$comname' id='$comname' src='view_pit.php?drawmode=draw_component&$vars'></iframe>";
				}
				echo "</div>";
			}
			
			echo "</div>";
			echo "</td>";
			echo "</tr>";
			echo "</table>";
			echo "<script language='javascript'>";
			echo $z_index_setup;
			echo "</script>";
		}
		
		echo "</center>";
		echo "</body>";
	}
?>
