<?php
	if($in_lavu)
	{
		display_header("Event Sequences");
		echo "<br><br>";
		
		$tablename = "lk_event_sequences";
		$forward_to = "index.php?mode={$section}_{$mode}";
		$filter_by = "`locationid`='$locationid'";
		$back_to_title = "<< View Event Sequence List";
		
		$fields = array();
		$fields[] = array(speak("Title"),"title","text","","list:yes");
		$fields[] = array("Location","locationid","hidden","","setvalue:$locationid");
		$fields[] = array(speak("Events"),"event_list","dbselect_list",array("lk_event_types","id","title","locationid"),"");
		
		//$fields[] = array("Score By","score_by","select",array("Best Lap","Position"),"list:yes");
		//$fields[] = array("Laps","laps","text");
		$fields[] = array(speak("Slots"),"slots","text","","Enabled");
		
		$fields[] = array(speak("Submit"),"submit","submit");

		require_once(resource_path() . "/browse.php");
	}
?>
