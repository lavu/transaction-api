<?php
	ini_set("display_errors",1);
	require_once("/home/poslavu/public_html/admin/cp/resources/lavuquery.php");

	$locations= get_locations();

	foreach($locations as $name){

		$email_list = get_email_list($name);
		$result	    = send_email_list($email_list, $name);

		if( $result != 'success'){
			mail("niallsc@poslavu.com","P2R membership email failed", "Email failed due to : ". $result);
			mail("corey@poslavu.com"  ,"P2R membership email failed", "Email failed due to : ". $result);
			mail("ag@poslavu.com"     ,"P2R membership email failed", "Email failed due to : ". $result);
		}
	}
	function get_locations(){
		$names=array();
		$query =mlavu_query("select `dataname` from `poslavu_MAIN_db`.`restaurant_locations` where `component_package`='4' OR `component_package`='1' ");

		while($read= mysqli_fetch_assoc($query)){

			if(!stristr($read['dataname'], "dev") && $read['dataname']!='17kart')
				$names[]=$read['dataname'];
		}
		return $names;

	}
	function send_email_list($email_list, $name){

		$subject 	   = "Pole Position Raceway Membership Expiring!";
		$headers  	   = "From:notice@polepositionraceway.com\r\n";
		$headers 	  .= "MIME-Version: 1.0\r\n";
		$headers 	  .= "Content-Type: text/html; charset=ISO-8859-1\r\n";
		$template	   = file_get_contents(dirname(__FILE__)."/email_template.tpl");
		$location_info = get_location_info($name);
		$result		   ='';
		$email_content = get_email_content($template, array('f_name'=>'Nialls','l_name'=>'Chavez','membership_expiration'=>'1/1/1111'), $location_info );
		//mail("niallsc@poslavu.com", $subject, $email_content, $headers);

		foreach($email_list as $cust_info){
			$email_content = get_email_content($template, $cust_info, $location_info );
			/*
			if(!mail($cust_info['email'], $subject, $email_content, $headers))
				$result.="Email Failed for: ". $cust_info['email']. " at location: ". $name;
			*/
		}
		if(!$result)
			return "success";
		else
			return $result;
	}
	function get_email_list($name){
		$email_arr= array();
		lavu_connect_dn($name,'poslavu_'.$name.'_db');
		$query = lavu_query("SELECT `email`, `f_name`, `l_name`,`membership_expiration` FROM `poslavu_[1]_db`.`lk_customers` where DATE(`membership_expiration`) BETWEEN CURDATE() - INTERVAL 30 DAY AND CURDATE()", $name);
		while($result= mysqli_fetch_assoc($query)){
			if(validate_email($result['email']))
				$email_arr[]= array($result['email'],$result['f_name'], $result['l_name'], $result['membership_expiration']);
		}
		return $email_arr;
	}
	function validate_email($email) {
		// First, we check that there's one @ symbol,
		// and that the lengths are right.
		if (!ereg("^[^@]{1,64}@[^@]{1,255}$", $email)) {
			// Email invalid because wrong number of characters
			// in one section or wrong number of @ symbols.
			return false;
		}
		// Split it into sections to make life easier
		$email_array = explode("@", $email);
		$local_array = explode(".", $email_array[0]);
		for ($i = 0; $i < sizeof($local_array); $i++) {
			if(!ereg("^(([A-Za-z0-9!#$%&'*+/=?^_`{|}~-][A-Za-z0-9!#$%&'*+/=?^_`{|}~\.-]{0,63})|(\"[^(\\|\")]{0,62}\"))$",$local_array[$i])) {
				return false;
			}
		}
		// Check if domain is IP. If not,
		// it should be valid domain name
		if (!ereg("^\[?[0-9\.]+\]?$", $email_array[1])) {
			$domain_array = explode(".", $email_array[1]);
			if (sizeof($domain_array) < 2) {
				return false; // Not enough parts to domain
			}
			for ($i = 0; $i < sizeof($domain_array); $i++) {
				if(!ereg("^(([A-Za-z0-9][A-Za-z0-9-]{0,61}[A-Za-z0-9])|([A-Za-z0-9]+))$",
					$domain_array[$i])) {
					return false;
				}
			}
		}
		return true;
	}
	function get_email_content($template, $cust_info, $location_info){

		$template = str_replace("<!--NAME-->"	 ,$cust_info['f_name']." ". $cust_info['l_name'], $template);
		$template = str_replace("<!--DATE-->"	 ,$cust_info['membership_expiration'], $template);
		$template = str_replace("<!--LOCATION-->",$location_info, $template);

		return $template;
	}
	function get_location_info($loc){
		lavu_connect_dn($loc,'poslavu_'.$loc.'_db');
		$result= mysqli_fetch_assoc(lavu_query("SELECT `address`,`city`,`state`,`zip` FROM `poslavu_[1]_db`.`locations`", $loc));
		return implode(", ",$result);
	}
?>