<html>
	<body style='width:800px;font-weight:lighter; font-family:verdana, times;'>
	<img src= 'https://register.aca-nynj.org/vendor_logos/1360000590_logocolor.jpg' width="300px" height="100px">
		<div class ='email_title' style='color:red; '> Hello, <!--NAME -->! </div>
		
		<div class = 'email_content' style='color:black;font-size:smaller'>
			
			We would like you to know that your membership at Pole Position Raceway will be expiring on <span style='color:red'> <!--DATE--> </span>.
			To continue enjoying the benefits of your Pole Position Raceway Membership, please visit <a href='http://polepositionraceway.com'>polepositionraceway.com</a>
			or come in to our location at: <!--LOCATION--> to renew your membership.
			<br>
			Thank you, and we hope to see you again soon! 
			<br>
			--Pole Position Raceway, Lets Race!  	
		</div>	
	</body>
</html>