<?php

   // echo 'locationid:' . $locationid;
   // echo 'data_name:'  . $data_name;
    // The top editor for adding/removing Service types.
    display_header("Service Types");
    echo "<br><br>";
		
	$tablename = "lk_service_types";
	$forward_to = "index.php?mode={$section}_{$mode}";
	$filter_by = "`locationid`='$locationid'";
	$back_to_title = "<< View Service Types";
	$timeDurationOptions = array('months_1' => 'Monthly', 'days_7' => 'Weekly');
	
	$fields = array();
	$fields[] = array(speak("Title"),"name","text","","list:yes");
	$fields[] = array(speak("Duration"), "duration", "select", $timeDurationOptions, "list:yes");
	$fields[] = array("Location","locationid","hidden","","setvalue:$locationid");
	$fields[] = array(speak("Submit"),"submit","submit");

	require(resource_path() . "/browse.php");
	
?>