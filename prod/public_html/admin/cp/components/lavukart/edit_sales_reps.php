<?php
	if($in_lavu)
	{
		display_header("Sales Reps");
		echo "<br><br>";
		
		$tablename = "lk_sales_reps";
		$forward_to = "index.php?mode={$section}_{$mode}";
		$filter_by = "`loc_id`='$locationid'";
		$back_to_title = "<< View Sales Reps";
		
		$fields = array();
		$fields[] = array(speak("First Name"),"firstname","text","","list:yes");
		$fields[] = array(speak("Last Name"),"lastname","text","","list:yes");
		$fields[] = array("Location","loc_id","hidden","","setvalue:$locationid");
		$fields[] = array(speak("Submit"),"submit","submit");

		require_once(resource_path() . "/browse.php");
	}
?>
