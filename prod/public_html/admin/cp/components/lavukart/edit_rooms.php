<?php
	if($in_lavu)
	{
		display_header("Rooms");
		echo "<br><br>";
		
		$tablename = "lk_rooms";
		$forward_to = "index.php?mode={$section}_{$mode}";
		$filter_by = "`locationid`='$locationid'";
		$back_to_title = "<< View Room List";
		
		$fields = array();
		$fields[] = array(speak("Title"),"title","text","","list:yes");
		$fields[] = array(speak("Abbreviation"),"abbreviation","text","","list:yes");
		$fields[] = array("Location","locationid","hidden","","setvalue:$locationid");
		/*$fields[] = array("First Time","time_start","time","","list:yes");
		$fields[] = array("Time Span","time_interval","timespan");
		$fields[] = array("Last Time","time_end","time","","list:yes");
		$fields[] = array("Seconds - Intermediate","seconds_intermediate","text","","");
		$fields[] = array("Seconds - Advanced","seconds_advanced","text","","");
		$fields[] = array("Print Ratio (a percentage)","print_ratio","text","","");*/
		$fields[] = array(speak("Submit"),"submit","submit");

		require_once(resource_path() . "/browse.php");
	}
?>
