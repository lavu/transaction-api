<?php
	if($in_lavu)
	{
		display_header("Tracks");
		echo "<br><br>";
		
		$printerArr = array();
		$printerArr['default'] = '';
		$printerArr['p1'] = 'Printer 1';
		$printerArr['p2'] = 'Printer 2';
		$printerArr['choose'] = 'Choose Printer';
		
		$tablename = "lk_tracks";
		$forward_to = "index.php?mode={$section}_{$mode}";
		$filter_by = "`locationid`='$locationid'";
		$back_to_title = "<< View Track List";
		
		$fields = array();
		$fields[] = array(speak("Title"),"title","text","enabled","list:yes");
		$fields[] = array(speak("Abbreviation"),"abbreviation","text","enabled","list:yes");
		$fields[] = array(speak("First Time"),"time_start","time","enabled","list:yes");
		$fields[] = array(speak("Time Span"),"time_interval","timespan");
		$fields[] = array(speak("Last Time"),"time_end","time","enabled","list:yes");
		$fields[] = array("Location","locationid","hidden","enabled","setvalue:$locationid");
		$fields[] = array(speak("Minimum valid lap time"),"min_lap_time","text","enabled","Enabled");
		$fields[] = array(speak("Seconds - Intermediate"),"seconds_intermediate","text","enabled","Enabled");
		$fields[] = array(speak("Seconds - Advanced"),"seconds_advanced","text","enabled","Enabled");
		$fields[] = array(speak("Print Ratio (a percentage)"),"print_ratio","text","enabled","Enabled");
		
		$fields[] = array(speak("Track Printer"), "printer", "select",$printerArr,"list:yes");
		
		$fields[] = array(speak("Submit"),"submit","submit");



if(!empty($_POST)){
	error_log( "POST:".json_encode($_POST) );
}
		require_once(resource_path() . "/browse.php");
		

	}
?>
