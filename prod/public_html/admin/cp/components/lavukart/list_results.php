<?php
	if($in_lavu)
	{
		display_header("Events");
		echo "<br><br>";
		
		$tablename = "lk_events";
		$forward_to = "index.php?mode=$mode";
		$filter_by = "`locationid`='$locationid'";
		$back_to_title = "<< View Event List";
		
		$setdate = urlvar("setdate",date("Y-m-d"));
		create_datepicker("edit1",$setdate,"window.location = '$forward_to&setdate=' + [value];");
		
		echo "<table>";
		$results_query = lavu_query("select * from `lk_events` where `date`='[1]' order by `date` asc, `time` asc",$setdate);
		while($results_read = mysqli_fetch_assoc($results_query))
		{
			echo "<tr>";
			echo "<td colspan='4' bgcolor='#777777' style='color:#ffffff; font-weight:bold' align='center'>" . display_time($results_read['time']) . "</td>";
			echo "</tr>";
			
			$team_scores = array();
			for($i=0; $i<=20; $i++)
			{
				$team_scores[$i] = 0;
				$team_members[$i] = 0;
			}
			
			$race_result_query = lavu_query("select * from `lk_race_results` where `eventid`='[1]' and `bestlap` > 0 order by `bestlap` asc",$results_read['id']);
			while($race_result_read = mysqli_fetch_assoc($race_result_query))
			{
				echo "<tr>";
				echo "<td>" . $race_result_read['place'] . "</td>";
				echo "<td>" . $race_result_read['bestlap'] . "</td>";
				$customerid = $race_result_read['customerid'];
				$cust_query = lavu_query("select * from `lk_customers` where `id`='[1]'",$customerid);
				$sched_query = lavu_query("select * from `lk_event_schedules` where `eventid`='[1]' and `customerid`='[2]'",$results_read['id'],$customerid);
				if(mysqli_num_rows($cust_query) && mysqli_num_rows($sched_query))
				{
					$cust_read = mysqli_fetch_assoc($cust_query);
					$sched_read = mysqli_fetch_assoc($sched_query);
					
					echo "<td>" . $cust_read['racer_name'] . "</td>";
					echo "<td>" . trim($cust_read['f_name'] . " " . $cust_rad['l_name']) . "</td>";
					$team = $cust_read['team'];
					echo "<td>";
					if($team!="") echo "Team " . $team; else echo "&nbsp;";
					echo "</td>";
					if($team!="")
					{
						$team_scores[$team] += $race_result_read['bestlap'] * 1;
						$team_members[$team] ++;
					}
				}
				echo "</tr>";
			}
			for($i=0; $i<=20; $i++)
			{
				if($team_members[$i] > 0)
				{
					echo "<tr>";
					echo "<td colspan='2' align='center'>Team " . $i . "</td>";
					echo "<td colspan='3'>" . $team_scores[$i] . " / " . $team_members[$team] . " = " . ($team_scores[$i] / $team_members[$team]) . "</td>";
					echo "</tr>";
				}
			}
		} 
		echo "</table>";
	}
?>
