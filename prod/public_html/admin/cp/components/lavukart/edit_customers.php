<?php
	if($in_lavu)
	{		

		require_once("/home/poslavu/public_html/admin/lib/ml_connect.php");

		$forward_to = "index.php?mode={$section}_{$mode}";

		$upcoming_events = array();
		$upcoming_events[''] = "";
		$today = date("Y-m-d");
		if (isset($location_info) && ($location_info['timezone'] != "")) {
			$today = localize_date(date("Y-m-d H:i:s"), $location_info['timezone']);
		}
		$check_events = lavu_query("SELECT `id`, `title`, `event_date`, `event_time` FROM `lk_group_events` WHERE `event_date` >= '$today' AND `cancelled` != '1' AND `_deleted` != '1' AND `loc_id` = '[1]' ORDER BY `event_date` ASC", $locationid);
		if (@mysqli_num_rows($check_events) > 0) {
			while ($event_info = mysqli_fetch_assoc($check_events)) {
				$upcoming_events[$event_info['id']] = $event_info['event_date']." ".timize($event_info['event_time'])." - ".$event_info['title'];
			}
		}

		if (isset($_REQUEST['export']) && ($_REQUEST['export'] == "lk_customers")) {
			display_header("Customers - Export");
			echo "<br><br>";
			
			$cust_query = lavu_query("select * from lk_customers order by id desc limit 1");
			if(mysqli_num_rows($cust_query))
			{
				$cust_read = mysqli_fetch_assoc($cust_query);
				
				$keys = array();
				foreach($cust_read as $key => $val)
				{
					if(substr($key,0,1)!="_")
					{
						$keys[] = $key;
					}
				}

				$total_cols = 4;
				$rows_per_col = ceil(count($keys) / $total_cols);
				sort($keys);
				
				$rows = 0;
				$cstr = "";
				$keynames = "";
				for($i=0; $i<count($keys); $i++)
				{
					$keyname = $keys[$i];
					if($keynames!="") $keynames .= ",";
					$keynames .= "'$keyname'";
					
					$cstr .= "<tr><td>";
					$cstr .= "<input type='checkbox' id='export_include_$keyname'>";
					$cstr .= $keyname;
					$cstr .= "</td></tr>";
					
					$rows++;
					if($rows>=$rows_per_col)
					{
						$cstr .= "</table>";
						$cstr .= "</td><td width='40'>&nbsp;</td><td valign='top'>";
						$cstr .= "<table>";
						$rows = 0;
					}
				}

				echo "<script language='javascript'>";
				echo "var keynames = new Array($keynames); ";
				echo "function check_all_click() { ";
				echo "  var btn_elem = document.getElementById('export_check_all_btn'); ";
				echo "  var new_phrase = 'Uncheck All'; ";
				echo "  var set_checked = true; ";
				echo "  if(btn_elem.value=='Uncheck All') { new_phrase = 'Check All'; set_checked = false; } ";
				echo "  for(var n=0; n<keynames.length; n++) document.getElementById('export_include_' + keynames[n]).checked = set_checked; ";
				echo "  btn_elem.value = new_phrase; ";
				echo "} ";
				echo "function submit_export() { ";
				echo "  var str = ''; ";
				echo "  for(var n=0; n<keynames.length; n++) {if(document.getElementById('export_include_' + keynames[n]).checked) str += keynames[n] + '|'; } ";
				echo "  window.location = \"index.php?reportid=export_fields_from_table&widget=reports/export&tablename=lk_customers&fields=\" + str; ";
				echo "} ";
				echo "</script>";
				echo "<form name='export_form' method='post' action=''>";
				echo "<input type='button' value='Check All' onclick='check_all_click()' id='export_check_all_btn' /><br>";
				
				echo "<table><td valign='top'>";
				echo "<table>";
				echo $cstr;
				echo "</table>";
				echo "</td></table>";
				echo "<br><br><input type='button' value='Export >>' onclick='submit_export()' />";
				echo "</form>";
			}
		}
		else if (isset($_REQUEST['mass_import']) && ($_REQUEST['mass_import'] == "lk_customers")) {
	
			$step = (isset($_REQUEST['step']))?$_REQUEST['step']:1;
			$customer_list = (isset($_SESSION['lk_import_customer_list']))?$_SESSION['lk_import_customer_list']:"";
			$selected_event = (isset($_SESSION['lk_import_selected_event']))?$_SESSION['lk_import_selected_event']:0;
	
			display_header("Customers - Mass Import");
			echo "<br><br>";
			
			?>
			
			<script language='javascript'>
				function import_validate(step) {
					if (document.getElementById('customer_list') && (document.getElementById('customer_list').value == "") && (step == 2)) {
						alert('Please enter at least one name before submitting.');
					} else {
						document.mass_import.submit();
					}
				}
			</script>

			<?php
			
			if ($step == 1) {
			
				if (count($upcoming_events) > 0) {		
					$event_select_code = "<tr>
							<td align='right' style='padding-left:32px;'>Tie to event:</td>
							<td align='left'>
								<select name='tie_to_event'>
									<option value='0'>Do not tie to an event</option>";
					$keys = array_keys($upcoming_events);
					foreach ($keys as $key) {
						if ($key != "") {
							$selected = "";
							if ($key == $selected_event) $selected = " selected";
							$event_select_code .= "<option value='$key'$selected>".$upcoming_events[$key]."</option>";
						}
					}
					$event_select_code .= "</select>
							</td>	
						</tr>";
				} else {
					$event_select_code = "<tr><td align='center' colspan='2'>No upcoming events found.<input type='hidden' name='tie_to_event' value='0'></td></tr>";
				}
		
				?>
			
				<form name='mass_import' method='POST' action=''>
					<input type='hidden' name='mass_import' value='lk_customers'>
					<input type='hidden' name='step' value='2'>
					<table width='475px' style='border:solid 2px #aaaaaa' cellpadding='3px'>
						<tr><td colspan='2' align='left' style='padding:5px 5px 1px 5px;'>Please type or paste the list of new customer names to add below. Be sure to use a carriage return after each name.<br><br>The expected format is [FIRST NAME] [space] [LAST NAME], i.e. John Doe.</td></tr>
						<tr><td colspan='2' align='center'><textarea name='customer_list' id='customer_list' cols='55' rows='20'><?php echo $customer_list; ?></textarea></td></tr>
						<?php echo $event_select_code; ?>
						<tr><td colspan='2' align='center'><input type='button' value='Preview' onclick='import_validate(2);'></td></tr>
					</table>
				</form>
				
				<?php
				
			} else if ($step == 2) {
			
				$_SESSION['lk_import_customer_list'] = $_REQUEST['customer_list'];
				$_SESSION['lk_import_selected_event'] = $_REQUEST['tie_to_event'];

				if ($_REQUEST['tie_to_event'] != 0)
					$event_selected_code = "<tr><td align='center'><b>".$upcoming_events[$_REQUEST['tie_to_event']]."</b></td></tr>";
				else
					$event_selected_code = "<tr><td align='center'><b>These accounts will not be tied to an event.</b></td></tr>";
			
				$customer_list_code = "<table cellspacing='0' cellpadding='4'><tr style='font-size:10px;' color='#555555'><td align='center' style='padding-right:15px; border-bottom:2px solid #AAAAAA;'><b>First Name</b></td><td align='center' style='padding-right:15px; border-bottom:2px solid #AAAAAA;'><b>Last Name</b></td></tr>";
				$customer_list = explode("\n", $_REQUEST['customer_list']);
				foreach ($customer_list as $customer_name) {
					$name_parts = explode(" ", $customer_name);
					$cnt = count($name_parts);
					$last_name = "";
					if ($cnt > 1) $last_name = array_pop($name_parts);
					$first_name = implode(" ", $name_parts);
					$customer_list_code .= "<tr style='font-size:14px'><td align='left' style='border-top:1px solid #CCCCCC;'>$first_name</td><td align='left' style='border-top:1px solid #CCCCCC;'>$last_name</td></tr>";
				}
				$customer_list_code .= "</table>";
			
				?>
					
				<form name='mass_import' method='POST' action=''>
					<input type='hidden' name='mass_import' value='lk_customers'>
					<input type='hidden' name='step' value='3'>
					<table width='475px' style='border:solid 2px #aaaaaa' cellpadding='3px'>
						<tr><td align='center' style='padding:5px 5px 1px 5px;'>Please look over the list before final submission.</td></tr>
						<?php echo $event_selected_code; ?>
						<tr><td align='center' style='padding: 15px 5px 15px 5px;'><?php echo $customer_list_code; ?></td></tr>
						<tr><td align='center'><input type='button' value='Go Back' onclick='window.location = "index.php?mode=edit_customers&mass_import=lk_customers";'> &nbsp;&nbsp;&nbsp; <input type='button' value='Submit' onclick='import_validate(3);'></td></tr>
					</table>
				</form>
				
				<?php
				
			} else if ($step == 3) {

				$tie_to_event = "";
				if ($_SESSION['lk_import_selected_event'] != 0) $tie_to_event = $_SESSION['lk_import_selected_event'];
				
				$insert_queries = "";
				$customer_list = explode("\n", $_SESSION['lk_import_customer_list']);
				foreach ($customer_list as $customer_name) {
					$name_parts = explode(" ", str_replace(array("\n", "\r"), array("", ""), $customer_name));
					$cnt = count($name_parts);
					$last_name = "";
					if ($cnt > 1) $last_name = array_pop($name_parts);
					$first_name = implode(" ", $name_parts);
					$racer_name = str_replace(" ", "", $customer_name);
					if ($insert_queries != "") $insert_queries .= "[_query_]";
					$insert_queries .= "INSERT INTO `lk_customers` (`f_name`, `l_name`, `racer_name`, `locationid`, `event_id`, `date_created`, `last_activity`) VALUES ('$first_name', '$last_name', '$racer_name', '$locationid', '$tie_to_event', now(), now())";
				}
				if ($insert_queries != "") {
					schedule_remote_to_local_sync($locationid, "run local query", "config", $insert_queries);
				}
	
				$_SESSION['lk_import_customer_list'] = "";
				$_SESSION['lk_import_selected_event'] = 0;
			
				echo "Your update will be made to your local server.  It may take up to five minutes to show up here.";
				echo "<br><br><input type='button' value='Continue >>' onclick='window.location = \"index.php?mode=edit_customers\"' />";	
			}

		} else {
		
			display_header("Customers");
			echo "<br><br>";
			
			$_SESSION['lk_import_customer_list'] = "";
			$_SESSION['lk_import_selected_event'] = 0;
	
			$insert_direct_to_local_server = true;
					
			$tablename = "lk_customers";
			$back_to_title = "<< View Customer List";
			$filter_by = "`locationid`='$locationid'";
			$search_mode = "only";
			$search_terms = array("f_name","l_name","[f_name] [l_name]","email","racer_name");
			$special_search = array("event_id", "select", $upcoming_events, "Any event");
			
			$states = array();
			$states[''] = "";
			$states['International'] = "International";
			$get_states = lavu_query("SELECT * FROM `states` ORDER BY `state_name` ASC");
			if (@mysqli_num_rows($get_states) > 0) {
				while ($extract = mysqli_fetch_assoc($get_states)) {
					$states[$extract['state_name']] = $extract['state_name'];
				}
			}
	
			$sources = array();
			$get_sources = lavu_query("SELECT * FROM `lk_source_list` ORDER BY `_order` ASC");
			if (@mysqli_num_rows($get_sources) > 0) {
				while ($extract = mysqli_fetch_assoc($get_sources)) {
					$sources[$extract['title']] = $extract['title'];
				}
			}
	
			$fields = array();
			$fields[] = array(speak("Group Event"),"event_id","select",$upcoming_events);
			$fields[] = array(speak("First Name"),"f_name","text","","list:yes");
			$fields[] = array(speak("Last Name"),"l_name","text","","list:yes");
			$fields[] = array(speak("Email"),"email","text","size:30");
			$fields[] = array(speak("Racer Name"),"racer_name","text");
			$fields[] = array(speak("Birth Date"),"birth_date","text");
			$fields[] = array(speak("State"),"state","select",$states);
			$fields[] = array(speak("Postal Code"),"postal_code","text");
			$fields[] = array(speak("Country"),"country","text");
			$fields[] = array(speak("Source"),"source","select",$sources);
			$fields[] = array(speak("License / Exp"),"license_exp","text");
			$fields[] = array(speak("Credits"),"credits","text","","list:yes");
			$fields[] = array(speak("Date Created"),"date_created","date_created","","list:yes");
			$fields[] = array(speak("Last Activity"),"last_activity","last_activity","","list:yes");
			$fields[] = array(speak("Minor / Adult"),"minor_or_adult_at_signup","minor_or_adult_at_signup","","list:yes");
			$fields[] = array(speak("Confirmed by Attendant"),"confirmed_by_attendant","confirmed_by_attendant");
			$fields[] = array("Location","locationid","hidden","","setvalue:$locationid");
			$fields[] = array(speak("Submit"),"submit","submit");
			
			$option_links = "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href='$forward_to&mass_import=lk_customers'><img src='images/mass_import.png' border='0'/></a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href='$forward_to&export=lk_customers'>(export)</a>";
	
			require_once(resource_path() . "/browse.php");
		}
	}
?>