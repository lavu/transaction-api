<?php	

	$fullMode = $section."_".$mode;
	$lsetting = "registration_expiration_info";
	$current_exp_value = (isset($location_info[$lsetting]))?$location_info[$lsetting]:"";
	
	if(isset($_GET['update_exp_type']) && isset($_GET['update_exp_amount']))
	{
		$set_exp_value = $_GET['update_exp_type'] . ":" . $_GET['update_exp_amount'];
		
		$lvars = array();
		$lvars['type'] = "location_config_setting";
		$lvars['setting'] = $lsetting;
		$lvars['location'] = $locationid;
		$lvars['value'] = $set_exp_value;
		
		if(isset($location_info[$lsetting]))
		{
			$success = lavu_query("update `config` set `value`='[value]' where `location`='[location]' and `type`='[type]' and `setting`='[setting]'",$lvars);
		}
		else
		{
			$success = lavu_query("insert into `config` (`value`,`location`,`type`,`setting`) values ('[value]','[location]','[type]','[setting]')",$lvars);
		}
		if($success)
			$current_exp_value = $set_exp_value;
	}
	
	$exp_parts = explode(":",$current_exp_value);
	if(count($exp_parts) > 1)
	{
		$current_exp_type = $exp_parts[0];
		$current_exp_amount = $exp_parts[1];
	}
	else
	{
		$current_exp_type = "";
		$current_exp_amount = "";
	}
	
	echo "<br><br><table cellpadding=6><tr><td width='700' bgcolor='#555560' align='center' style='color:#ffffff; font-size:14px'><b>General Registration Settings</b></td></tr></table><br>";
	
	echo "Registration Expiration:<br>";
	echo "<select name='exp_type' id='exp_type'>";
	echo "<option value=''>No Expiration</option>";
	echo "<option value='days'".($current_exp_type=="days"?" selected":"").">After x Days</option>";
	echo "<option value='yearly'".($current_exp_type=="yearly"?" selected":"").">On Day x of the next Year</option>";
	echo "<option value='monthly'".($current_exp_type=="monthly"?" selected":"").">On Day x of the next Month</option>";
	echo "</select>";
	echo "<input type='text' name='exp_amount' id='exp_amount' value='$current_exp_amount'>";
	//echo "<input type='button' value='Update' onclick='window.location = \"index.php?mode=$mode&update_exp_type=\" + document.getElementById(\"exp_type\").value + \"&update_exp_amount=\" + document.getElementById(\"exp_amount\").value'>";
	echo "<input type='button' value='Update' onclick='window.location = \"index.php?mode=$fullMode&update_exp_type=\" + document.getElementById(\"exp_type\").value + \"&update_exp_amount=\" + document.getElementById(\"exp_amount\").value'>";
	echo "<br><br>";
	echo "<table cellpadding=6><tr><td width='700' bgcolor='#555560' align='center' style='color:#ffffff; font-size:14px'><b>Customer Fields</b></td></tr></table>";
	echo "<br><br>";
	require_once(dirname(__FILE__) . "/edit_customer_fields.php");
?>
