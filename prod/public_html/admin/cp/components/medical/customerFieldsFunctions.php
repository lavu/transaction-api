<?php


function insertIntoThisField($locationid){
	/* This function determines the next column in the med_customers table to be used for storing customer information. It can create a column when there are no
			more available columns. It returns the name of the field (column) name to be used */
        //Edited by Brian D.
        //Just added 'LIMIT 1' to the end of the the lavu_query since `somebody` is using a select
        //to find the db structure. med_customers could have gotten extremely huge so this is 
        //linear when it could be a constant O() function.
		$res2 = lavu_query("SELECT * FROM med_customers LIMIT 1");
		$res2Rows = mysqli_fetch_assoc($res2);
		$lastFieldNum = 0;
		foreach ($res2Rows as $key => $value){
			/* This foreach statement effectively gets the last field name */
					$lastFieldNumArray = $key;
		}//foreach
		
		/*fields are labeled field1, field2, field3, etc. We want the number*/
		/* I think this block of code never gets used, it can probably be removed */
		$lastFieldNumPieces = explode("field", $lastFieldNumArray); 
		$lastFieldNum = $lastFieldNumPieces[1];
		$lastFieldNum = $lastFieldNum*1;
		$listOfCurrentFields;
		$firstAvailableFieldName;
		$useEmptyField;
		
		$res = lavu_query("SELECT value2 FROM config WHERE location=\"[1]\" AND setting=\"GetCustomerInfo\"", $locationid);
		$fieldsArray;//should be fieldArray with no S
		$index = 0;
		/* Gather all the field names from the config table */ 
		while ($row = mysqli_fetch_assoc($res)){
				$curField = $row['value2'];
				$curFieldArr = explode("field",$curField);
				$curFieldNumber = $curFieldArr[1];
				$fieldArray[$index] = $curFieldNumber*1;
				$index++;
		}//while
		
		/* Sort the field numbers in ascending order so we know what the next field will be numbered */
		sort($fieldArray);
		
		/* The highest field number is located at the end of the array */
		$maxFieldVal = $fieldArray[count($fieldArray)-1];
		
		/* Create the name of the next field, the number is incremented by one */
		$newFieldVal = $maxFieldVal*1 + 1;
		$newFieldName = strval("field".$newFieldVal);
		lavu_query("ALTER TABLE med_customers ADD $newFieldName varchar(255) NOT NULL");
		
		/* Return the new field name so it can be used by processing scripts */
		return $newFieldName;
		
}//insertIntoThisField()

function addFirstAndLastNameFieldsIfNotExists($locationid){
		/* This function guarantees that the 'First Name' and 'Last Name' entries are in the configuration table. It makes sure that first names and last names
		   are placed in the field names 'f_name' and 'l_name', this is necessary for the customer search feature to work. */
		/* function also adds one entry into the med_customers database which is later used to determine how many fields are in the table. This feature is probably obsolete and can likely be removed*/
		
		/* Check if the entries are there. If they are not, create them. This feature runs every time the user views the web interface part of this application called 'Edit Customer Fields */
		$res = lavu_query("SELECT * FROM config WHERE location=\"[1]\" AND value=\"First_Name\"", $locationid);
		$row = mysqli_fetch_assoc($res);
		$isIdSet = $row['id']; //if ID was set then we know the entry is there
		if ($isIdSet == ""){
			lavu_query("INSERT INTO config (location, setting, value, value2, value4,type, value6, value10) VALUES (\"[1]\", \"GetCustomerInfo\", \"First_Name\", \"f_name\", \"useTool\", \"input\", \"requiredField\", \"1\")", $locationid);
		}//if
		
		$res = lavu_query("SELECT * FROM config WHERE location=\"[1]\" AND value=\"Last_Name\"", $locationid);
		$row = mysqli_fetch_assoc($res);
		$isIdSet = $row['id']; //if ID was set then we know the entry is there
		if ($isIdSet == ""){
lavu_query("INSERT INTO config (location, setting, value, value2, value4,type, value6, value10) VALUES (\"[1]\", \"GetCustomerInfo\", \"Last_Name\", \"l_name\", \"useTool\", \"input\", \"requiredField\", \"2\")", $locationid);
				
		}//if
	
		/* This block of code can likely be removed, I think it is no longer being used by anything. We no longer need 'howManyFields' to be created so I will likely remove this later */
		$res2 = lavu_query("SELECT * FROM med_customers WHERE f_name=\"howManyFields\" AND locationid=\"[1]\"", $locationid);
		$res2Row = mysqli_fetch_assoc($res2);
		$isEntryThere = $res2Row['id'];
		if ($isEntryThere == ""){//entry is not there
				//so put it there
				/* This value is called by insertIntoThisField() to determine how many fields are in the med_customer database. It exists because calling this one row returns fewer results than calling every row in the med_customers table, and we can be sure that it will be there */
				lavu_query("INSERT INTO med_customers (f_name, locationid) VALUES(\"howManyFields\", \"[1]\")", $locationid);
		}//if
	
}//addFirstAndLastNameFieldsIfNotExists()

function convertHTMLFormElementToPretty($input){
		/* Names are stored in the config table with spaces replaced by '_'. This is necessary for creating HTML form element names and ids, but doesn't look good when the user see it in the app, so we replace the '_'s with spaces. */
		$output = "";
		if ($input == "input"){
			$output = "Text Field";
			return $output;
		}//
		if ($input == "checkbox"){
			$output = "Check Box";
			return $output;
		}//
		if ($input == "list_menu"){
			$output = "Drop Down Menu";
			return $output;
		}//
		if ($input == "radio"){
			$output = "Radio Group";
			return $output;
		}//
		if($input == "date") {
			$output = "Date";
			return $output;
		}
		
}//convertHTMLFormElementToPretty()



function clean($string){
		/* Sanitize user inputs. Take out characters that would not be acceptable for a html field name or id */
		//return trim(preg_replace('/[^a-zA-Z0-9_\s]/', '', $string)); 
		$labelStr = $string;
		$labelStr = str_replace(",","",$labelStr);
		$labelStr = str_replace("\n","",$labelStr);
		$labelStr = str_replace("`","",$labelStr);
		return $string;
}//clean()

?>