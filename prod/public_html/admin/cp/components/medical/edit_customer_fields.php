<?php

require_once(dirname(__FILE__) . "/customerFieldsFunctions.php");
addFirstAndLastNameFieldsIfNotExists($locationid);



//echo "licked Submit: $clickedSubmit<br />";

/* Decide which form the clicked */
if (isset($_GET['form_posted'])){
		$whichForm = $_GET['form_posted'];
		//echo "whichForm: $whichForm<br /><br />";

			$useThisField = insertIntoThisField($locationid);


			if ($whichForm == "edit"){

					require_once(dirname(__FILE__) . "/processEditFields.php");

			}//if

			if ($whichForm == "restore"){

					require_once(dirname(__FILE__) . "/processEditFields.php");

			}//if

			if ($whichForm == "add"){
					$userIsAdding = $_POST['addThis']; //drop menu, text field, radio, checkbox, etc...

					if ($userIsAdding == "Menu"){
							require_once(dirname(__FILE__) . "/processAddDropDownMenu.php");
					}//if

					if ($userIsAdding == "Text Field"){
							require_once(dirname(__FILE__) . "/processAddTextField.php");
					}//if

					if ($userIsAdding == "Checkbox"){
							require_once(dirname(__FILE__) . "/processAddCheckBox.php");
					}//if

					if ($userIsAdding == "Radio Group"){
							require_once(dirname(__FILE__) . "/processAddRadioGroup.php");
					}//if

					if ($userIsAdding == "State Option"){
							require_once(dirname(__FILE__) . "/processAddStateDropDownOption.php");
					}//if

					if ($userIsAdding == "Date"){
							require_once(dirname(__FILE__) . "/processAddDateField.php");
					}//if
			}//if
			schedule_remote_to_local_sync_if_lls($location_info, $locationid, "table updated", "config");
}//if

//$locationid = 1;

?>



<style type="text/css">

.component{

	float:left;
	border:0px solid #666;
	width:130px;
	margin-right:5px;
	font-size:7pt;

	padding:5px 0 5px 0;

}

.popUp{
	width:30%;

	min-width:400px;
	height:auto;
	background-color:#FFF;
	border:0px solid #000;
	margin-bottom:20px;

}

.popUpLeftForm{
	float:left;
	width:100px;
	border: 0px solid #ccc;
	text-align:right;
	padding:0 5px 0 0;
}

.popUpRightForm{
	float:left;
	width:200px;
	border: 0px solid #ccc;
	text-align:left;
}

.popUpClearBoth{
	clear:both;
	height:15px;

}

.separator{
 	height:1px;
	background-color:#333333;
	margin-top:5px;
	width:100%;
}

.divHeader{
	font-weight:700;
}

.popUpLeftFormAlign{
	float:left;
	text-align:right;
	width:40px;
	border:0px solid #333;
	padding-left:2px;
}

.popUpRightFormAlign{
	float:left;
	text-align:left;
	width:40px;
	border:0px solid #333;
}

.popUpFormAlignClearBoth{
	clear:both;
	height:1px;
}

.clearBoth{
	clear:both;
	height:10px;
}

.info{
	display:none;
	border:0px solid #000;
	float:right;
}

</style>

<script type="text/javascript">
document.getElementById("ClickedSubmit").value = "false";


function showPopUp(input){
		document.getElementById("popUp").style.display = "block";
}//showPopUp()


function closePopUp(input){
		document.getElementById("popUp").style.display = "none";
}//showPopUp()

function addField(){

		displayThis = "";

		addThis = document.getElementById("addThis").value;

		if (addThis == "Text Field"){
				addTextField();
		}//if

		if (addThis == "Menu"){

				addMenu();
				return;
		}//if

		if (addThis == "Radio Group"){
				addRadio();
		}//if

		if (addThis == "Checkbox"){
				displayThis += addCheckbox();
		}//if

		if (addThis == "Date"){
				displayThis += addDate();
		}

		//document.getElementById("showStuff").innerHTML = displayThis;

}//addField()

function addTextField(){
	//nothing needs to be done
	currentHTML = "";
	document.getElementById("showStuff").innerHTML = currentHTML;
	return;
}//addMenu()



function addCheckbox(){
	//nothing needs to be done
	currentHTML = "";
	document.getElementById("showStuff").innerHTML = currentHTML;
	return;
}//addMenu()

function addDate(){
	//nothing needs to be done
	currentHTML = "";
	document.getElementById("showStuff").innerHTML = currentHTML;
	return;
}//addMenu()

function addRadio(){

	document.getElementById("NumberOfFields").value = "1";

	currentHTML = "";

				currentHTML += "<div class=\"popUpLeftForm\">";
				currentHTML += "<label>Option Name<label>";
				currentHTML += "</div>";
				currentHTML += "<div class=\"popUpRightForm\">";
				currentHTML += "<input type=\"text\" class=\"Entry\" name=\"Entry_1\" id=\"Entry_1\" onfocus=\"editAndOrAddOption('1');\"  />";
				currentHTML += "</div>";
				currentHTML += "<div class=\"popUpClearBoth\"></div>";
				document.getElementById("showStuff").innerHTML = currentHTML;

	return;
}//addMenu()

function addMenu(){

	document.getElementById("NumberOfFields").value = "1";

	currentHTML = "";

				currentHTML += "<div class=\"popUpLeftForm\">";
				currentHTML += "<label>Option Name<label>";
				currentHTML += "</div>";
				currentHTML += "<div class=\"popUpRightForm\">";
				currentHTML += "<input type=\"text\" class=\"Entry\" name=\"Entry_1\" id=\"Entry_1\" onfocus=\"editAndOrAddOption('1');\"  />";
				currentHTML += "</div>";
				currentHTML += "<div class=\"popUpClearBoth\"></div>";
				document.getElementById("showStuff").innerHTML = currentHTML;

	return;
}//addMenu()

function editAndOrAddOption(fieldNumber){

		numberOfFields = document.getElementById("NumberOfFields").value;
		if (numberOfFields == fieldNumber){
				storeData = getData(numberOfFields);

				addElement();
				putData(storeData);

				fieldTheyClicked = "Entry_" + fieldNumber;
				document.getElementById(fieldTheyClicked).focus();
		}//if


}//editAndOrAddOption()



function edit_editAndOrAddOption(input){

		getInfo = input.split("_");
		entryID = getInfo[1];
		fieldNumber = getInfo[2];

		numFieldsID = "numFields_" + entryID;
		numberOfFields = document.getElementById(numFieldsID).value;

		if (numberOfFields == fieldNumber){
				storeData = edit_getData(numberOfFields, entryID);
				//alert("storeAdata: "+ storeData);
				edit_addElement(entryID, numberOfFields, numFieldsID);

				edit_putData(storeData, entryID);
				focusToThisField = fieldNumber-1;
				fieldTheyClicked = "options_" + entryID + "_" + focusToThisField;
				document.getElementById(fieldTheyClicked).focus();
		}//if


}//editAndOrAddOption()


function putData(storeData){
	/* Get data already input by the user */

		//alert("putData: " + storeData);

		index = 0;
		for (i=1; i < storeData.length; i++){

				curID = "Entry_" + i;
				document.getElementById(curID).value = storeData[index];
				index++;


		}//for

		return storeData;
}//putData()


function edit_putData(storeData, entryID){
	/* Get data already input by the user */

		//alert("putData: " + storeData);

		index = 0;
		for (i=1; i < storeData.length; i++){

				curID = "options_" + entryID + "_" + i;
				document.getElementById(curID).value = storeData[index];
				index++;


		}//for

		return storeData;
}//putData()


function edit_getData(numberOfFields, entryID){
	/* Get data already input by the user */
		storeData = new Array();

		index = 1
		for (i=0; i < numberOfFields; i++){
				curID = "options_" + entryID + "_" + index;
				storeData[i] = document.getElementById(curID).value;
				index++;
		}//for
		//alert("storeData: " + storeData);
		return storeData;
}//getData()

function getData(numberOfFields){
	/* Get data already input by the user */
		storeData = new Array();

		index = 1
		for (i=0; i < numberOfFields; i++){
				curID = "Entry_" + index;
				storeData[i] = document.getElementById(curID).value;
				index++;
		}//for

		return storeData;
}//getData()

function addElement(){
			//add another element and update necessary fields
				id = parseInt(document.getElementById("NumberOfFields").value) + 1;
				currentHTML = document.getElementById("showStuff").innerHTML;

				currentHTML += "<div class=\"popUpLeftForm\">";
				currentHTML += "<label>Option Name<label>";
				currentHTML += "</div>";
				currentHTML += "<div class=\"popUpRightForm\">";
				currentHTML += "<input type=\"text\" class=\"Entry\" name=\"Entry_" + id  + "\" id=\"Entry_" + id  + "\" onfocus=\"editAndOrAddOption('" + id + "');\" />";
				currentHTML += "</div>";
				currentHTML += "<div class=\"popUpClearBoth\"></div>";

				document.getElementById("showStuff").innerHTML = currentHTML;
				document.getElementById("NumberOfFields").value = id;
				//return currentHTML;

}//addElement()


function edit_addElement(entryID, numberOfFields, numFieldsID){
			//for editing fields

				showStuffID = "showStuff_" + entryID;
				currentHTML = document.getElementById(showStuffID).innerHTML;

				fieldNumber = fieldNumber*1 + 1;
				currentHTML += "<input onfocus=\"edit_editAndOrAddOption('editField_"+entryID+"_"+fieldNumber+"')\" type=\"text\" name=\"options_"+entryID+"_"+fieldNumber+"\" id=\"options_"+entryID+"_"+fieldNumber+"\" style=\"width:120px;\" /><br/>";


				document.getElementById(showStuffID).innerHTML = currentHTML;
				document.getElementById(numFieldsID).value = fieldNumber;
				//return currentHTML;

}//addElement()


function processAddForm(){
	////alert("Imma wipe out your work!");

	currentValue = document.getElementById("showCurrentElements").innerHTML;

	//alert("ha ha ha");

	//return false;
}//processAddForm()

function formWasClicked(){
		/* This sets the hidden text field to true, so the processing scripts know the form was submitted instead of the page just being refreshed*/
		document.getElementById("ClickedSubmit").value = "true";
		return true;
}//formWasClicked()


function deleteThisEntry(id){
	/* This function is used to delete the inputted id from the config table */
	//alert("deleteThis: " + id);
	var answer = confirm("Are you sure you want to delete this entry? You will be able to restore it using the 'Erased Entries' section.");

	if (answer){
			document.getElementById("eraseThisEntryIDs").value = id;
			document.forms["editForm"].submit();
	}//if
}//deleteThisEntry()


function restoreThisEntry(id){
	/* This function is used to delete the inputted id from the config table */
	//alert("deleteThis: " + id);
	var answer = confirm("Are you sure you want to restore this entry?");

	if (answer){
			document.getElementById("restoreThisEntryIDs").value = id;
			document.forms["restoreForm"].submit();
	}//if
}//deleteThisEntry()



function enforceOrder(idNum){
		/* Make sure the order of appearance stays ordered from 1 to max value and increments by one. We don't want duplicate values assigned
			to two or more different entries */



}//enforceOrder()

function enforceUniqueLabelNames(){
		/* If the user submits duplicate label name values to the database, the whole application fails. This function prevents them from being able to do that */
		//alert("works");
		if (ensureNoBlankLabels() == false){
				alert("Please enter a value for each Label");
				return false;
		}//if

		entryIDList = document.getElementById("entryIDs").value + "label";
		entryIDArray = entryIDList.split(",");
		//entryIDArray += "label";
		//entry

		//alert("entryIDArray: " + entryIDArray);
		for (i=0;i<entryIDArray.length;i++){

				firstLoopID = "label_" + entryIDArray[i+1];
				firstLoopVal = document.getElementById(firstLoopID).value;
				//alert("firstLoopID: " + firstLoopID + "\nfirstLoopVal: " + firstLoopVal);

				//alert("firstLoopVal: " + firstLoopVal);
				for (j=0;j<entryIDArray.length;j++){
						secondLoopID = "label_" + entryIDArray[j];
						secondLoopVal = document.getElementById(secondLoopID).value;

						if ( (firstLoopID != secondLoopID) && (firstLoopVal == secondLoopVal) ){
								alert("You have duplicate labels named: '" + firstLoopVal + "'. Please ensure that all entries have a unique label name.");
								return false;
						}//if

				}//for

		}//for

		return true;

}//enforceUniqueLabelNames()

function ensureNoBlankLabels(){
			/* Make sure all label fields are filled in */
			var entryIDs = document.getElementById("entryIDs").value;
			entryIDsArr = entryIDs.split(",");
			for (i=0;i<entryIDsArr.length-1;i++){

					curID = "label_" + entryIDsArr[i];
					curVal = document.getElementById(curID).value;
					if (curVal == ""){
								return false;
					}//if

			}//for


			return true;
}//ensureNoBlankLabels()

</script>
<!--
<h1><span style="text-decoration:blink;">In development on 3/22/12 @ 3:46pm</span></h1>
-->
<div style="margin-top:20px; width:100%; text-align:center;">
<a name="createAField" id="createAField"></a>
	<a href="#createAField">Create A Field</a> |
	<a href="#editFields">Edit Fields</a> |
	<a href="#erasedEntries">Erased Fields</a>
</div>
<div class="separator">&nbsp;</div>
<div class="popUp" id="popUp">



	<h2>Create A Field</h2>
	<!-- Edited by Brian D. add settings_ for post back to access correctly -->
	<!--   <form action="?mode=registration_settings&form_posted=add" method="post"  name="popUp" onsubmit="return enforceUniqueLabelNames();">  -->
	<form action="?mode=settings_registration_settings&form_posted=add" method="post"  name="popUp" onsubmit="return enforceUniqueLabelNames();">
	<!-- END EDIT -->
	
	<input type="hidden" name="ClickedSubmit" value="false" id="ClickedSubmit" />
	<input type="hidden" name="formPosted" value="popUp"/>
	<div class="popUpLeftForm">
			<label>Add</label>
			<img class="info" src="components/medical/info_bore.png" width="20" height="auto" />
	</div><!--popUpLeftForm-->
	<div class="popUpRightForm">
			<select id="addThis" name="addThis" onchange="addField();">
					<option value=""></option>
					<option value="State Option">Add State Drop Down Option</option>
					<option value="Text Field">Text Field</option>
					<option value="Menu">Drop Menu</option>
					<option value="Radio Group">Radio Group</option>
					<option value="Checkbox">Checkbox</option>
					<option value="Date">Date</option>

			</select>

	</div><!--popUpRightFoimages/info_cool.pngrm-->

	<div class="popUpClearBoth"></div>

	<div class="popUpLeftForm">Required</div>
	<div class="popUpRightForm">
				  <div class="popUpLeftFormAlign"><label>Yes</label></div>
				  <div class="popUpRightFormAlign"><input type="radio" name="IsRequired" value="yes" /></div>
				  <div class="popUpFormAlignClearBoth"></div>
				  <div class="popUpLeftFormAlign"><label>No</label></div>
				  <div class="popUpRightFormAlign"><input type="radio" name="IsRequired" value="no" checked/></div>
				  <div class="popUpFormAlignClearBoth"></div>
	</div>

	<div class="popUpClearBoth"></div>
			 <!-- <div class="popUpLeftForm">Order of Appearance</div>
			  <div class="popUpRightForm"><select name="OrderOfAppearance">
					<?php
				/*	for ($i=1;$i<5;$i++){
							  echo "<option value=\"$i\">$i</option>";
					}//for
				*/	?>
			  </select></div>
			  <div class="popUpClearBoth"></div>
	-->
	<div class="popUpLeftForm">
		<label>Label</label>
	</div>
	<div class="popUpRightForm">
			<input type="text" name="label_label" id="label_label" onblur="enforceUniqueLabelNames();" style="width:90px;" />
	</div>
	<div class="popUpClearBoth"></div>


	<input type="hidden" id="NumberOfFields" name="NumberOfFields" />
	<span id="showStuff">

	</span>


	<div style="clear:both;"></div>

	<div class="popUpLeftForm">
				&nbsp;
	</div><!--popUpLeftForm-->
	<div class="popUpRightForm">
			<input name="Submit" type="submit" value="Add" />
	</div><!--popUpRightForm-->

	<div class="popUpClearBoth"></div>
	</form>

</div><!--popUp-->






















<input type="hidden" name="formPosted" value="Edit"/>

<div style="margin-top:20px; width:100%; text-align:center;">
	<a name="editFields" id="editFields"></a>
	<a href="#createAField">Create A Field</a> |
	<a href="#editFields">Edit Fields</a> |
	<a href="#erasedEntries">Erased Fields</a>
</div>

<div class="separator">&nbsp;</div>

<h2>Edit Fields</h2>
<br />

<!-- Edited by Brian D. add settings_ for post back to access correctly -->
<!-- <form action="?mode=registration_settings&form_posted=edit" method="post" name="editForm" onsubmit="return enforceUniqueLabelNames();"> -->
<form action="?mode=settings_registration_settings&form_posted=edit" method="post" name="editForm" onsubmit="return enforceUniqueLabelNames();">
<!-- END EDIT -->

<div style="clear:both;">
<input name="Submit" type="submit" value="Save Changes" />
</div>
<br />
<div class="component divHeader">
Label
</div><!--leftForm-->




<div class="component divHeader">
Required
</div>



<div class="component divHeader">
Order of Appearance
</div>


<div class="component divHeader">
Options
</div>

<div class="component divHeader">
Type
</div><!--leftForm-->

<!-- INSERTED BY BRIAN D. -->
<div class="component divHeader">
Receipt Printing
</div>
<!-- END INSERT -->

<div class="component divHeader">
Remove
</div><!--leftForm-->

<div class="clearBoth"></div>


<div id="showCurrentElements">

<?php

	$res = lavu_query("SELECT * FROM config WHERE setting=\"GetCustomerInfo\" AND _deleted!=\"1\" ORDER BY value10*1");


	$getMaxOrderOfAppeareanceValRes = lavu_query("SELECT value10 FROM config WHERE setting=\"GetCustomerInfo\" AND location=\"[1]\" AND _deleted!=\"1\"", $locationid);
	$howManyRows = 0;
	while ($maxOrderOfAppearanceRows = mysqli_fetch_assoc($getMaxOrderOfAppeareanceValRes)){
			$howManyRows++;
	}//while
	$index = 0;
	//echo "MAX: $howManyRows<br /><br /><br />";
	$maxOOAVal = $howManyRows;
	echo "<input name=\"NumberOfRows\" id=\"NumberOfRows\" type=\"hidden\" value=\"$maxOOAVal\" />";
	$listOfEntryIDs;
	while ($rows = mysqli_fetch_assoc($res)){
				$index++;
				$type = $rows['type'];
				$required = $rows['value6'];
				$orderOfAppearance = $rows['value10'];
				$labelStr = $rows['value'];
				$labelArr = explode("_",$labelStr);
				$label = implode(" ",$labelArr);

				$value4 = $rows['value4'];//the drop down menu options (or radio options)
				$value5 = $rows['value5'];
				$curID = $rows['id'];

				/* INSERTED BY BRIAN D. */
				$printValueAsStored = $rows['value12'];
				/* END INSERT */

				$listOfEntryIDs .= $curID . ",";

				/* We don't want them to delete the First Name and Last Name fields, or the customer search feature won't work. We also don't want the customer to 'unrequire' at least one of the name fields (First Name/Last Name) since the customer search feature relies on this  */
								$FirstNameRes = lavu_query("SELECT * FROM config WHERE value2=\"f_name\" AND location=\"[1]\" AND setting=\"GetCustomerInfo\" ", $locationid);
								$FirstNameRows = mysqli_fetch_assoc($FirstNameRes);
								$firstNameLabel = str_replace("_"," ", $FirstNameRows['value']);

								$LastNameRes = lavu_query("SELECT * FROM config WHERE value2=\"l_name\" AND location=\"[1]\" AND setting=\"GetCustomerInfo\" ", $locationid);
								$LastNameRows = mysqli_fetch_assoc($LastNameRes);
								$lastNameLabel = str_replace("_"," ", $LastNameRows['value']);


					echo "<div class=\"component\">
							<input id=\"label_" . $curID . "\" name=\"label_" . $curID . "\" onblur=\"enforceUniqueLabelNames();\" style=\"width:120px;\" value=\"$label\" />
					</div><!--component-->";

					echo"<div class=\"component\">";
						$isItChecked = "";
						if ($required == "requiredField"){
								$isItChecked = "checked";
						}//if

						$isDisabled = "";
					if( ($label == $firstNameLabel) || ($label == $lastNameLabel) ){

								/* This is to make sure that even if the field is disabled the data still gets put in the $_POST array */
								$isDisabled = "disabled=\"disabled\"";
								if ($isDisabled == "disabled=\"disabled\""){
											echo "<input type=\"hidden\" name=\"IsRequired_".$curID."\" value=\"yes\" />";
								}//if
								if ($isDisabled == ""){
										/* This if statement shouldn't be necessary as the user cannont make the required field to 'no' for the First and Last names */
											echo "<input type=\"hidden\" name=\"IsRequired_".$curID."\" value=\"no\" />";
								}//if
					}//if
						//requiredField

						echo"
							<label>Yes</label><input type=\"radio\" id=\"yes_IsRequired_".$curID."\" name=\"IsRequired_".$curID."\" $isDisabled value=\"yes\" $isItChecked/>
							<br />";
						$isItChecked = "";
					if ($required != "requiredField"){
								$isItChecked = "checked";
						}//if

							echo "
							<label>No&nbsp;</label><input type=\"radio\" id=\"no_IsRequired_".$curID."\" name=\"IsRequired_".$curID."\" $isDisabled value=\"no\" $isItChecked/>";


					echo "</div><!--component-->";

					//Order of appearance...
						echo "<div class=\"component\">";
							echo "<select id=\"OrderOfAppearance_" . $curID . "\" name=\"OrderOfAppearance_" . $curID . "\" onchange=\"enforceOrder('$curID');\">";

							for ($i=1; $i<=$maxOOAVal;$i++){
									echo "<option value=\"$i\"";
									if ($i == $orderOfAppearance){
											echo "selected";
									}//if
									echo ">$i</option>";
							}//for
							echo "</select>";
					echo "</div><!--component-->";

					//Options
					echo "<div class=\"component\">";
					$fieldNumber = 1;
					if ($type == "radio" || $type == "list_menu"){
							if ($value4 != "useTool"){
											echo "<span id=\"showStuff_".$curID."\">";
											$rezOptions = lavu_query("SELECT * FROM config WHERE id=\"[1]\" AND location=\"[2]\"", $curID, $locationid);
											$rowOptions = mysqli_fetch_assoc($rezOptions);
											$optionsVals = $rowOptions['value4'];
											$optionsArr = explode(",",$optionsVals);
											$fieldNumber = 1;

											for($i=0; $i<count($optionsArr);$i++){
													echo "<input onclick=\"edit_editAndOrAddOption('editField_".$curID."_".$fieldNumber."')\" type=\"text\" value=\"" . $optionsArr[$i] . "\" name=\"options_".$curID."_".$fieldNumber."\" id=\"options_".$curID."_".$fieldNumber."\" style=\"width:120px;\" /><br/>";
													$fieldNumber++;
											}//for
											//	$curNumberOfFields = $index + 1;
												echo "<input onfocus=\"edit_editAndOrAddOption('editField_".$curID."_".$fieldNumber."')\" type=\"text\" name=\"options_".$curID."_".$fieldNumber."\" id=\"options_".$curID."_".$fieldNumber."\" style=\"width:120px;\" /><br/>";
							}//if

							if ($value4 == "useTool"){
									echo "<span style=\"font-weight:700;\">Tool</span>";
									echo "<input onclick=\"edit_editAndOrAddOption('editField_".$curID."_".$fieldNumber."')\" type=\"hidden\" value=\"$value4\" name=\"options_".$curID."_".$fieldNumber."\" id=\"options_".$curID."_".$fieldNumber."\" /><br/>";
							}//if
					}//if
					echo "</span><!--id=showstuff_entryid-->";
					echo "<input type=\"hidden\" name=\"numFields_".$curID."\" id=\"numFields_".$curID."\" value=\"$fieldNumber\"  /><br/>";
					echo "</div><!--component-->";




					//Type (Text, Date, State Drop Down, etc.)
						echo "<div class=\"component\">";
						//$type
										echo "<select name=\"Type_".$curID."\" id=\"Type_".$curID."\" $isDisabled >";
															echo "<option value=\"input\">Text</option>
																		<option value=\"list_menu\">Drop Down Menu</option>
																		<option value=\"radio\">Radio Group</option>
																		<option value=\"checkbox\">Checkbox</option>
																		<option value=\"date\">Date</option>
																		<option value=\"list_menu_state\">State Drop Down</option>
																		";

										echo "</select>";

										if( ($label == $firstNameLabel) || ($label == $lastNameLabel) ){

													/* This is to make sure that even if the field is disabled the data still gets put in the $_POST array */
													//$isDisabled = "disabled=\"disabled\"";
													if ($isDisabled == "disabled=\"disabled\""){
																echo "<input type=\"hidden\" name=\"Type_".$curID."\" id=\"Type_".$curID."\" value=\"$type\" />";
													}//if

										}//if

										$hiddenFieldID = "whichType_".$curID;
										if ( ($value4 == "useTool") && ($value5 == "State") ){
												$type .= "_state";
										}//if
										echo "<input type=\"hidden\" name=\"$hiddenFieldID\" id=\"$hiddenFieldID\" value=\"$type\"/>";

										echo "<script type=\"text/javascript\">
																val = document.getElementById('$hiddenFieldID').value;
																document.getElementById('Type_".$curID."').value = val;
																//alert(\"val: \" + val);
													</script>";
						echo "</div><!--component-->";

					//Printing goes here...
					/* INSERTED BY BRIAN D */
					$selectedString_k = $printValueAsStored == 'k' ? 'selected' : '';
					$selectedString_r = $printValueAsStored == 'r' ? 'selected' : '';
					$selectedString_b = $printValueAsStored == 'b' ? 'selected' : '';
					echo "<div class=\"component\">";
						echo "<select name=\"Printing_".$curID."\" id=\"Printing_".$curID."\">";
							echo "<option value=''>Neither</option>";
							echo "<option value='k' $selectedString_k>Kitchen</option>";
							echo "<option value='r' $selectedString_r>Receipt</option>";
							echo "<option value='b' $selectedString_b>Both</option>";
						echo "</select>";
					echo "</div><!--component-->";
					/* END INSERT */

					//Remove
					echo "<div class=\"component\">";

								/* We don't want them to be able to delete the first name and last name fields, the customer search needs these */
								if( ($label != $firstNameLabel) && ($label != $lastNameLabel) ){
											echo "<img src=\"./components/medical/images/btn_delete_x.png\" onclick=\"deleteThisEntry('$curID');\" height=\"20\" />";
								}//if
					echo "</div><!--component-->";
					echo "<div class=\"clearBoth\"></div>";
	}//while


?>

<div class="popUpLeftForm">
				&nbsp;
	</div><!--popUpLeftForm-->
	<div class="popUpRightForm">

	</div><!--popUpRightForm-->

<div style="clear:both;">
	<input name="Submit" type="submit" value="Save Changes" />
</div>
	<div class="popUpClearBoth"></div>

	<?php
		echo "<input type=\"hidden\" name=\"entryIDs\" id=\"entryIDs\" value=\"$listOfEntryIDs\" />";
		echo "<input type=\"hidden\" name=\"eraseThisEntryIDs\" id=\"eraseThisEntryIDs\"  />";
	?>
</form>

















<div style="margin-top:20px; width:100%; text-align:center;">
	<a name="erasedEntries" id="erasedEntries"></a>
	<a href="#createAField">Create A Field</a> |
	<a href="#editFields">Edit Fields</a> |
	<a href="#erasedEntries">Erased Fields</a>
</div>
<div class="separator">&nbsp;</div>


<h2>Erased Entries</h2>
<!-- Edited by Brian D. add settings_ for post back to access correctly -->
<!-- <form action="?mode=registration_settings&form_posted=restore" method="post" name="restoreForm"> -->
<form action="?mode=settings_registration_settings&form_posted=restore" method="post" name="restoreForm">
<!-- END EDIT -->

<div class="component divHeader">
Label
</div><!--leftForm-->




<div class="component divHeader">
Required
</div>



<div class="component divHeader">
Order of Appearance
</div>


<div class="component divHeader">
Options
</div>

<div class="component divHeader">
Type
</div><!--leftForm-->

<!-- Inserted by Brian D. -->
<div class="component divHeader">
Receipt Printing
</div>
<!-- END INSERT -->

<div class="component divHeader">
Restore
</div><!--leftForm-->

<div class="clearBoth"></div>


<?php

	$res = lavu_query("SELECT * FROM config WHERE setting=\"GetCustomerInfo\" AND _deleted=\"1\" ORDER BY value10*1");


	$getMaxOrderOfAppeareanceValRes = lavu_query("SELECT value10 FROM config WHERE setting=\"GetCustomerInfo\" AND location=\"[1]\" AND _deleted!=\"1\"", $locationid);
	$howManyRows = 0;
	while ($maxOrderOfAppearanceRows = mysqli_fetch_assoc($getMaxOrderOfAppeareanceValRes)){
			$howManyRows++;
	}//while
	$index = 0;
	//echo "MAX: $howManyRows<br /><br /><br />";
	$maxOOAVal = $howManyRows;
	echo "<input name=\"NumberOfRows\" id=\"NumberOfRows\" type=\"hidden\" value=\"$maxOOAVal\" />";
	$listOfEntryIDs;
	while ($rows = mysqli_fetch_assoc($res)){
				$index++;
				$type = $rows['type'];
				$required = $rows['value6'];
				$orderOfAppearance = $rows['value10'];
				$labelStr = $rows['value'];
				$labelArr = explode("_",$labelStr);
				$label = implode(" ",$labelArr);

				$value4 = $rows['value4'];//the drop down menu options (or radio options)
				$curID = $rows['id'];
				$listOfEntryIDs .= $curID . ",";
				/* INSERTED BY BRIAN D. */
				$printValueAsStored = $rows['value12'];
				/* END INSERT */
					echo "<div class=\"component\">
							<input id=\"label_" . $curID . "\" name=\"label_" . $curID . "\" onblur=\"enforceUniqueLabelNames();\" style=\"width:120px;\" value=\"$label\" disabled />
					</div><!--component-->";

					echo"<div class=\"component\">";
						$isItChecked = "";
						if ($required == "requiredField"){
								$isItChecked = "checked";
						}//if

						//requiredField
						echo"
							<label>Yes</label><input type=\"radio\" id=\"yes_IsRequired_".$curID."\" name=\"IsRequired_".$curID."\" value=\"yes\" disabled $isItChecked/>
							<br />";
						$isItChecked = "";
					if ($required != "requiredField"){
								$isItChecked = "checked";
						}//if

							echo "
							<label>No&nbsp;</label><input type=\"radio\" id=\"no_IsRequired_".$curID."\" name=\"IsRequired_".$curID."\" value=\"no\" disabled $isItChecked/>
					</div><!--component-->";

					//Order of appearance
						echo "<div class=\"component\">";
							echo "<select id=\"OrderOfAppearance_" . $curID . "\" name=\"OrderOfAppearance_" . $curID . "\" onchange=\"enforceOrder('$curID');\" disabled>";

							for ($i=1; $i<=$maxOOAVal;$i++){
									echo "<option value=\"$i\"";
									if ($i == $orderOfAppearance){
											echo "selected";
									}//if
									echo ">$i</option>";
							}//for
							echo "</select>";
					echo "</div><!--component-->";

					//Options
					echo "<div class=\"component\">";
					$fieldNumber = 1;
					if ($type == "radio" || $type == "list_menu"){
							if ($value4 != "useTool"){
											echo "<span id=\"showStuff_".$curID."\">";
											$rezOptions = lavu_query("SELECT * FROM config WHERE id=\"[1]\" AND location=\"[2]\"", $curID, $locationid);
											$rowOptions = mysqli_fetch_assoc($rezOptions);
											$optionsVals = $rowOptions['value4'];
											$optionsArr = explode(",",$optionsVals);
											$fieldNumber = 1;

											for($i=0; $i<count($optionsArr);$i++){
													echo "<input onclick=\"edit_editAndOrAddOption('editField_".$curID."_".$fieldNumber."')\" type=\"text\" value=\"" . $optionsArr[$i] . "\" name=\"options_".$curID."_".$fieldNumber."\" id=\"options_".$curID."_".$fieldNumber."\" style=\"width:120px;\" disabled /><br/>";
													$fieldNumber++;
											}//for
											//	$curNumberOfFields = $index + 1;
												echo "<input onfocus=\"edit_editAndOrAddOption('editField_".$curID."_".$fieldNumber."')\" type=\"text\" name=\"options_".$curID."_".$fieldNumber."\" id=\"options_".$curID."_".$fieldNumber."\" style=\"width:120px;\" disabled=\"disabled\" /><br/>";
							}//if

							if ($value4 == "useTool"){
									echo "<span style=\"font-weight:700;\">Tool</span>";
									echo "<input onclick=\"edit_editAndOrAddOption('editField_".$curID."_".$fieldNumber."')\" type=\"hidden\" value=\"$value4\" name=\"options_".$curID."_".$fieldNumber."\" id=\"options_".$curID."_".$fieldNumber."\" /><br/>";
							}//if
					}//if
					echo "</span><!--id=showstuff_entryid-->";
					echo "<input type=\"hidden\" name=\"numFields_".$curID."\" id=\"numFields_".$curID."\" value=\"$fieldNumber\"  /><br/>";
					echo "</div><!--component-->";


					//Type (Text, Date, StateDropdown, etc. )
					echo "<div class=\"component\">";
						//$type
										echo "<select name=\"Type_".$curID."\" id=\"Type_".$curID."\" disabled=\"disabled\">";
															echo "<option value=\"input\">Text</option>
																		<option value=\"list_menu\">Drop Down Menu</option>
																		<option value=\"radio\">Radio Group</option>
																		<option value=\"checkbox\">Checkbox</option>
																		<option value=\"date\">Date</option>
																		<option value=\"list_menu_state\">State Drop Down</option>
																		";

										echo "</select>";
										$hiddenFieldID = "whichType_".$curID;
										if ( ($value4 == "useTool") && ($value5 == "State") ){
												$type .= "_state";
										}//if
										echo "<input type=\"hidden\" name=\"$hiddenFieldID\" id=\"$hiddenFieldID\" value=\"$type\"/>";

										echo "<script type=\"text/javascript\">
																val = document.getElementById('$hiddenFieldID').value;
																document.getElementById('Type_".$curID."').value = val;
																//alert(\"val: \" + val);
													</script>";
						echo "</div><!--component-->";

					/*
						echo "<div class=\"component\">" . convertHTMLFormElementToPretty($type) . "</div><!--component-->";
					*/
					
					//Printing goes here...
					/* INSERTED BY BRIAN D */
					$selectedString_k = $printValueAsStored == 'k' ? 'selected' : '';
					$selectedString_r = $printValueAsStored == 'r' ? 'selected' : '';
					$selectedString_b = $printValueAsStored == 'b' ? 'selected' : '';
					echo "<div class=\"component\">";
						echo "<select name=\"Printing_".$curID."\" id=\"Printing_".$curID."\" disabled='disabled' >";
							echo "<option value='' >Neither</option>";
							echo "<option value='k' $selectedString_k>Kitchen</option>";
							echo "<option value='r' $selectedString_r>Receipt</option>";
							echo "<option value='b' $selectedString_b>Both</option>";
						echo "</select>";
					echo "</div><!--component-->";
					/* END INSERT */

					echo "<div class=\"component\">
								<img src=\"./components/medical/images/btn_restore_x.png\" onclick=\"restoreThisEntry('$curID');\" height=\"20\" />
								<!--<input type=\"button\" value=\"Restore\" onclick=\"restoreThisEntry('$curID');\" width=\"auto\" height=\"20\" />-->
					</div><!--component-->";
					echo "<div class=\"clearBoth\"></div>";
	}//while

	echo "<input type=\"hidden\" name=\"restoreThisEntryIDs\" id=\"restoreThisEntryIDs\"  />";
?>

</form>
