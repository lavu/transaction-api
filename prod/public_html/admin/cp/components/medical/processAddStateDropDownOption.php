<?php
$db_info = array();


$labelStr = clean($_POST['label_label']);
$labelArr = explode(" ",$labelStr);
$label = implode("_", $labelArr);
$db_info["type"] = "list_menu";



$db_info["value4"] = "useTool"; /*identifies this entry as a tool set that is processed differently than other entries. the 'State' tool prevents from having to store 50 states in a database, rather the user simply specifies in the web interface that they want a State drop down menu, and a script is processed that places the State drop down menu in their app.
 */
 
$db_info["value5"] = "State"; /* This value must be stored as 'State' in the value5 field in the config table for the scripts to know that this tool is going to be a State drop down menu */

$userIsAdding = $_POST['addThis']; //drop menu, text field, radio, checkbox, etc... don't need this here
$required = $_POST['IsRequired'];
$db_info["value6"] = ""; 
if ($required == "yes"){
		$db_info["value6"] = "requiredField";
}//if

$orderOfAppearance = $_POST['OrderOfAppearance'];

$numberOfFields = $_POST['NumberOfFields'];
$numberOfFields -= 1; //the last field is always blank so we omit it
			

$tempID = rand(0,9999999) . date("Y-m-d H:i:s");//use this to get the real ID for storing more info

lavu_query("INSERT INTO config (location, setting, value) VALUES (\"[1]\", \"GetCustomerInfo\", \"[2]\")", $locationid, $tempID);
$res = lavu_query("SELECT * FROM config WHERE value=\"[1]\" AND location=\"[2]\"", $tempID, $locationid);

$rows = mysqli_fetch_assoc($res);
$rowID = $rows['id'];




$db_info["value"] = $label;




/* Loop through the options for their drop down menu, I don't think this is necessary for adding a State drop down menu since everything is preconfigured in the code, this section of code should probably be removed */
$dropDownOptions = "";
for ($i=1; $i<= $numberOfFields; $i++){
		$fieldName = strval("Entry_" . $i);
		$curFieldVal = $_POST[$fieldName];
		
		$dropDownOptions = $dropDownOptions . $curFieldVal . ",";
		
}//for


/* I think this next section was for figuring out which field name to use in the med_customers table, but I think that is no longer needed since this is already taken care of with the $useThisField = insertIntoThisField($locationid); code in the edit_customer_fields.php file */
$res2 = lavu_query("SELECT * FROM med_customers");
$res2Rows = mysqli_fetch_assoc($res2);
$lastFieldNum = 0;
foreach ($res2Rows as $key => $value){
			$lastFieldNumArray = $key;
}//foreach

$lastFieldNumPieces = explode("field", $lastFieldNumArray);
$lastFieldNum = $lastFieldNumPieces[1];
$lastFieldNum = $lastFieldNum*1 + 1;
$db_info["value2"] = $useThisField;

$db_info["value10"] = $orderOfAppearance;

$newFields = "field" . strval($lastFieldNum);


foreach ($db_info as $key => $value){
		lavu_query("UPDATE config SET $key=\"[1]\" WHERE id=\"[2]\" AND location=\"[3]\"", $value, $rowID, $locationid);
		
}//foreach

?>