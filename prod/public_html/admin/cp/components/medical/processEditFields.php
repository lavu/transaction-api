<?php

/* Erase values if any are present in the 'delete' text field. Entries aren't really deleted, but their _deleted value is set to 1, which prevents it from showing up anymore. This can be unset back to 0 later if the user wishes for it to be available again. */
$eraseThisEntry = trim($_POST['eraseThisEntryIDs']);
lavu_query("UPDATE config SET _deleted=\"1\" WHERE id=\"[1]\" AND location=\"[2]\"",$eraseThisEntry, $locationid);


/* Restore the specified entries if there are any. */
$restoreThisEntry = trim($_POST['restoreThisEntryIDs']);
lavu_query("UPDATE config SET _deleted=\"0\" WHERE id=\"[1]\" AND location=\"[2]\"",$restoreThisEntry, $locationid);



/* All the entries on the web page have their config table id numbers stored as comma seperated values in a hidden field called 'entryIDs' */
$entryIDList = $_POST['entryIDs'];
$entryIDListArr = explode(",",$entryIDList);

/* store values in $db_info array. They will be submitted to the database with a foreach()*/
$db_info = array();

/*
$db_info["value"] stores the HTML Label (or name) that the user specified
$db_info["value4"] stores the options if it's a drop menu or radio group, or it stores "useTool" if this entry is a tool provided for the user (like a state drop down menu

$db_info["value6"] stores "isRequired" if this field is required for the customer info form to be submitted in the app 

*/


/* Loop through the id numbers we just got so we know what to process */
for ($i=0;$i<count($entryIDListArr)-1;$i++){//dont need last comma
			
			
			$curID = $entryIDListArr[$i];
			$labelField = "label_".$curID;
			$labelStr = clean($_POST[$labelField]);
			/*$labelStr = $_POST[$labelField];
			$labelStr = str_replace(",","",$labelStr);
			$labelStr = str_replace("\n","",$labelStr);
			$labelStr = str_replace("`","",$labelStr);*/
			
			$labelArr = explode(" ",$labelStr);
			$labelVal = implode("_", $labelArr);
			
			
			$typeName = "Type_".$curID;
			$db_info["type"] = $_POST[$typeName];
			/* INSERTED BY BRIAN D. */
			$printingName = "Printing_".$curID;
			$db_info["value12"] = $_POST[$printingName];
			/* END INSERT */
			$skipSettingValue4ToUseTool = true;
			//this one has to be on top
			if ($db_info["type"] != "list_menu_state"){
					//$db_info["type"] = "list_menu";
					$db_info["value5"] = "";
					$db_info["value4"] = "";
			}//if
			
			if ($db_info["type"] == "list_menu_state"){
					$db_info["type"] = "list_menu";
					$db_info["value5"] = "State";
					$db_info["value4"] = "useTool";
					$skipSettingValue4ToUseTool = false;
			}//if
			
			
			
			$orderOfAppearanceRankID = "OrderOfAppearance_" . $curID;
			$orderOfAppearanceRank = $_POST[$orderOfAppearanceRankID];
			$db_info["value10"] = $orderOfAppearanceRank;
			
			$isRequiredField = "IsRequired_".$curID;
			$isRequiredVal = $_POST[$isRequiredField];
			$isRequiredDBVal = "";
			if ($isRequiredVal == "yes"){
					$isRequiredDBVal = "requiredField";
			}//if
			
			$numberOfFieldsField = "numFields_".$curID;
			$numberOfFieldsVal = $_POST[$numberOfFieldsField];
			$numberOfFieldsVal = $numberOfFieldsVal*1; //make sure it's treated as a number, not a string
			if ($db_info["value4"] != "useTool"){
						$db_info["value4"] = "";
						for ($f=1; $f<=$numberOfFieldsVal;$f++){
									$curFieldField = "options_".$curID."_".$f;
							
									$curFieldVal = trim($_POST[$curFieldField]);
									
									if ($curFieldVal == "useTool" && $skipSettingValue4ToUseTool == false){
											/* useTool entries cannot have the 'useTool' values changed or they won't show up correctly */
												$db_info["value4"] = "useTool";
									}//if
									
									if ($curFieldVal != "useTool"){
												if ($curFieldVal != ""){
															$db_info["value4"] .= $curFieldVal;
															/* We don't want commas at the end of the string */
															if ( $f < ($numberOfFieldsVal-1)){
																		$db_info["value4"] .= ",";
															}//if
												}//if
									}//if
									
						}//for
			}//if
			
			
		$db_info["value"] = $labelVal;
		$db_info["value6"] = $isRequiredDBVal;
		
		foreach($db_info as $key => $value){
			//echo $key . " = " . $value . "<br>";
					lavu_query("UPDATE config SET $key=\"[1]\" WHERE id=\"[2]\" AND location=\"[3]\"", $value, $curID, $locationid);
		}//foreach
			
}//for

?>