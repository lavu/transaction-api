$(document).ready(function () {
    $.fn.multiAccordion = function(options) {
    	
    	//options validation
    	var default_args = {
			'topShade'	:	false,
			'adding': 		false,
            'onHoverScript': function(){},
            'onClickScript': function(){}
		}
		
		for(var index in default_args) {
			if(typeof options[index] == "undefined") options[index] = default_args[index];
		}
		
		//if you are just adding a new h3 to the accordion
		if(options['adding']){
	        addAccordionFunctionality($(this), options);
	        
		//if you are using a whole div to create the accordion
		}else{
	    	//accordion basic functionlity
	    	$(this).addClass("ui-accordion ui-accordion-icons ui-widget ui-helper-reset")
	          .find("h3").each(function(){
	          		 addAccordionFunctionality($(this), options);
	          	});
	          	
	      	//adding the buttons if they have been specified  
	        if(options['buttonContainerName'] != undefined){
	        	var buttonContainerHTML = '<div id="' + options['buttonContainerName']+'">';
	        		
	        		//add all the buttons
	        		var counter = 0;
	        		for (var i in options['buttonLabels']){
	        			buttonContainerHTML += '<div class="accordionButton" id="'+i+'" >'+options['buttonLabels'][i]+'</div>';
	        			counter++;
	        		}
	        		
	        	buttonContainerHTML += '</div>';
	        	
	        	//apend the html code
	        	$(this).prepend(buttonContainerHTML);
	        	
	        	//give functionality to the buttons
	        	mainDiv = $(this);
	        	counter=0;
	        	//for all buttons
	        	for (var i in options['buttonLabels']){
	        		//find the button reference
	        		buttonRef = $(this).find($('#'+i));
	        		
	        		//find all affected accordion h3
	        		var accH3 = mainDiv.children("h3");
	        		//console.log(accH3);
	        		//var accH3 = mainDiv.children("#messagesAccordion").children("h3");
	        		//console.log(mainDiv);
	        		
	        		
	        		//implement new functionalities bellow	
	        		if(options['functionality'][counter]=='expandAll'){ //if the current bucket on the functionality says expandAll
	        		//if(true){
	        				//when somebody clicks in this button then...
	        				buttonRef.click(function(){			
	        					//console.log('expand all');
	        				
	        					//transverse through all h3s and if the class is ui-state-default click on it
	        					console.log(accH3.length);
	        					for(i=0;i<accH3.length;i++){
	        						if($(accH3[i]).hasClass('ui-state-default')){ 
	        							$(accH3[i]).click();
	        						}
	        					}
	        				});
	        				
	        		}else if(options['functionality'][counter]=='contractAll'){ //if the current bucket on the functionality says contractAll
	        				//when somebody clicks in this button then...
	        				buttonRef.click(function(){
	        					//transverse through all h3s and if the class is ui-accordion-header-active click on it
	        					console.log(accH3.length);
	        					for(i=0;i<accH3.length;i++){
	        						//if($(accH3[i]).hasClass('ui-accordion-header-active')){
	        						if($(accH3[i]).hasClass('ui-state-active')){
	        							$(accH3[i]).click();
	        						}
	        					}
	        				});	
	        				
	        		}
	        		
	        		counter++;
	        	}
	        }
        
    	}
    };
});

function addAccordionFunctionality(element, options){
	element.addClass("ui-accordion-header ui-helper-reset ui-state-default ui-corner-top ui-corner-bottom")
        .hover(function() { 
        	options['onHoverScript'](element, options);
        	
        	element.toggleClass("ui-state-hover"); 
        })
        .prepend('<span class="ui-icon ui-icon-triangle-1-e"></span>')
        .click(function() {
          element
            .toggleClass("ui-accordion-header-active ui-state-active ui-state-default ui-corner-bottom")
            .find("> .ui-icon").toggleClass("ui-icon-triangle-1-e ui-icon-triangle-1-s").end()
            .next().toggleClass("ui-accordion-content-active").slideToggle(250, "easeOutQuad");
            
            options['onClickScript'](element, options);
            
          return false;
        })
        .next()
          .addClass("ui-accordion-content ui-helper-reset ui-widget-content ui-corner-bottom")
          .css("display", "block")
          .hide();
         
   	//adding the shade on top of the tittle   
    if(options['topShade']){    
     element.prepend('<div id="accordionShade"><img src="images/accordionShade.png"/> </div>');      
    }
}
