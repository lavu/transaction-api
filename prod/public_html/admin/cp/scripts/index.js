	function openHelp(thisHelp) {

		var UserWidth = 0, UserHeight = 0;
		if( typeof( window.innerWidth ) == 'number' ) {
			//Non-IE
			UserWidth = window.innerWidth;
			UserHeight = window.innerHeight;
		} else if( document.documentElement && ( document.documentElement.clientWidth || document.documentElement.clientHeight ) ) {
			//IE 6+ in 'standards compliant mode'
			UserWidth = document.documentElement.clientWidth;
			UserHeight = document.documentElement.clientHeight;
		} else if( document.body && ( document.body.clientWidth || document.body.clientHeight ) ) {
			//IE 4 compatible
			UserWidth = document.body.clientWidth;
			UserHeight = document.body.clientHeight;
		}

		var ScrollTop = document.body.scrollTop;
		if (ScrollTop == 0) {
			if (window.pageYOffset) {
				ScrollTop = window.pageYOffset;
			} else {
				ScrollTop = (document.body.parentElement) ? document.body.parentElement.scrollTop : 0;
			}
		}

		document.getElementById("help_iframe").src = "resources/help.php?m=" + thisHelp;
		document.getElementById("help_popup").style.left = ((UserWidth / 2) - 362) + "px";
		document.getElementById("help_popup").style.top = (ScrollTop + 75) + "px";
		document.getElementById("help_popup").style.display = 'inline';
	}

	function hideHelp() {
		document.getElementById("help_popup").style.display = 'none';
	}

		//window.currentMenu='';

	/*
	var isHovering=false;
	var hoverElement=null;
	var subMenuHoverElement=null;
	var stop=false;
	*/
	$(document).ready( function(){

		$('#center').click(function(){
			$("nav ul ul").fadeOut('slow',function(){$("nav ul ul").css('display','')});
			//('display', 'none');
			//$("nav ul ul").css('display', 'none');
		});

		/*
			$("nav").mouseover(function (){
				var subMenu=$("nav").find('ul ul:visible')
				console.log(subMenu);
				if(!subMenu){
					subMenu.mouseout(function(){
						$(subMenu).fadeOut(1500,function(){
						$(subMenu).css("display", '');
					});
					});

				}
			});

		*/
/*
		$("nav").mouseover(function (){
			isHovering=true;
			stop=false;

		});

		$("nav ul li").mouseover(function(){

			var oldHoverElem = hoverElement;

			hoverElement= $("nav ul ul").find(":visible").parent().attr('id');

			//$.each(hover)

			console.log(hoverElement);
			console.log($("nav ul ul ").find(":visible"))

			if(hoverElement != oldHoverElem ){
				$("#"+oldHoverElem).stop(true, true);
				$("#"+oldHoverElem).css("opacity","");
				$("#"+oldHoverElem).css("display","");
				stop=false;
			}else{
				oldHoverElem=hoverElement;
				$("#"+hoverElement).stop(true, false);
				$("#"+hoverElement).css("opacity","1");

				stop=false;
			}
		});

		$("nav ul li ").mouseout(function (){
			isHovering=false;

			if(hoverElement){

				$("#"+hoverElement).css("display","block");

				setTimeout(
			    	function(){

						if(!isHovering){
							if(!stop){
								stop=true;
								if(hoverElement){
									$("#"+hoverElement).fadeOut(1000,function(){
										$("#"+hoverElement).css("opacity","");
										$("#"+hoverElement).css("display","");

									});
								}
							}
						}
					}, 0
				);
			}
		});
*/
		var previous=null;
		var setPrevious= function(p){
			previous=p;
		}
		var getPrevious=function (){
			return previous;
		}


		$('nav ul li').hover(function(){
			id= $(this).parent().attr('id');
			setPrevious(id);
			$("#"+id).stop(true,true);
			$("#"+id).css("display","");
		},
		function(){

			var pvious= getPrevious();
			var id= $(this).parent().attr('id');
			$("#"+id).stop(true,true);
			$("#"+id).css("display","block");

			if(pvious!=id && pvious){
				$("#"+pvious).fadeOut(750,function(){
					$("#"+pvious).css("opacity","");
					$("#"+pvious).css("display","");
					$("#"+id).css("display","");
					$("nav ul ul ul").css('display','');

				});
			}
		});



		$("#use_language_pack").change(function(){

			if($("#use_language_pack").val()==0 )
				$("#use_language_pack").after("<a class='use_custom_pack'href='?parent=settings&mode=settings_custom_language'><br>Click here to modify your language pack <br>(Advanced users only) </a> ");
			else
				$(".use_custom_pack").css("display","none");
		});

		if( window.initMenu)
			$("#"+window.initMenu).css("display", "none");
			/*
		$(window).resize(function() {
			showMenu(window.currentMenu)
		});
*/
		$("nav ul li").click(
				$("#borderDivActive").parent("li").children('ul').css({display:"block", color:"white", "margin-top":"12px"})
		);


	$("#messagesAccordion").multiAccordion({
		buttonLabels:{
			expandAllMessages: ' + ',
			contractAllMessages: ' &mdash; '
		},
		functionality:['expandAll','contractAll'],
		buttonContainerName: 'messagesButtonContainer'
	});


	$(".ui-accordion-header").click(function(){

		if($(this).attr('class').indexOf('auto_open')!=-1){
			$(this).removeClass('auto_open');
			return;
		}
		var message_id = $(this).children().next().attr("message_id");
		var username   = $(this).children().next().attr("message_username");

		if(typeof message_id==="undefined")
			message_id = $(this).children().next().next().attr("message_id");
		if(typeof message_id==="undefined")
			message_id = $(this).attr("message_id");
		if(typeof username === "undefined")
			username   = $(this).children().next().next().attr("message_username");
		if(typeof username === "undefined")
			username   = $(this).attr("message_username");
		//setTimeout(function(){
			if($(".ui-accordion-content-active").length|| $(".unread_message").length){
				var returnVal='';

			    $('.unread_message').each(function(elem){
			    	var id=$($(".unread_message")[elem]).attr('message_id');

					if(id == message_id){
						var e= $($(".unread_message")[elem]);
						e.removeClass("unread_message");
						e.addClass("read_message");

						$('.read_message').parent().css('background-color','');
						$('.unread_message').parent().css('background-color','#f5fed0');
						var src= $('.read_message').children().attr('src').replace("new_","");

						//var src= $('.read_message').children().first().attr('src');
						//src= [src.slice(0, 7), '', src.slice(7)].join('');
						$('.read_message').children().attr('src',src);

					}

			    });

			    $.ajax({
					url: "resources/update_viewed_messages.php",
					type: "POST",
					data:"username="+username+"&message_id="+message_id,
					async:true,
					success:
						function (string){
							if(string =='success'){
								//console.log("username: "+username+ " message_id "+ message_id );
								if( $(".unread_messages").html()!=""){
									var num_unread= parseInt($(".unread_messages").html());
									if($(".unread_messages").html()!='' && (num_unread-1)==0)
										$(".unread_messages").html("");
									else
										$(".unread_messages").html( (num_unread-1)+" unread messages");
								}
							}
						}
				});
			}
		//}, 1000);
	});

	$(".ui-icon-triangle-1-e ").css('margin-top','16px');
		//$('#notify').hide();

		/*if ( $.cookie('isLoggedIn') != null && $.cookie('isLoggedIn') == 1 ) {

			$('#notify').howdyDo({
				action		: 'hover',
				effect		: 'slide',
				delay		: 2000,
				//hideAfter	: 10000,
				keepState	: true,
				closeAnchor	: '<img src="images/close-16x16.png" border="0" />',
				openAnchor	: '<img src="images/show-bar.png" border="0" />'
			});

		}*/

		//console.log($.cookie('isLoggedIn'));
/*
		var onchange_checkbox = ($('.on_off :checkbox')).iphoneStyle({

			checkedLabel: 'Yes',
			uncheckedLabel: 'No',

			onChange: function(elem, value) {

				if ( value === true ) {

					var ltg_link = "<a href='https://admin.poslavu.com/cp/index.php?mode=lavu_togo'>https://admin.poslavu.com/cp/index.php?mode=lavu_togo</a>";

					$('td#status').html("<br />Please enable & configure your online ordering menu here: "+ltg_link).show();

				} else {

					$('td#status').hide();
				}

			}
		});	*/
	
		$("#aboveHeaderContCancel").click(function(){
			$.cookie('cp3_beta_notify_disabled', '1', {path: '/'});
			$("#aboveHeaderContDiv").hide();
		});
	});

	function record_click(link, dataname){
		var URL		  	 = 'misc/record_clicks.php';
		var type	  	 = "POST";
		var urlString 	 = "link_clicked="+link+"&dataname="+dataname;
		var returnVal 	 ='';
		var asyncSetting = false;

	     $.ajax({
			url: URL,
			type: type,
			data:urlString,
			async:asyncSetting,
			success:
				function (string){
					returnVal= string;
					if(returnVal!='success')
						alert(returnVal);
					//alert(returnVal);
				}
		});

	   return returnVal;
	}

	function initialize_ministry_page(){
		var ministries = performAJAX("components/tithe/edit_ministries_data.php", "function=init_ministries", false, "POST");
		for( var i=0; i< ministries.length; i++)
			add_new_ministry(ministries[i].name,ministries[i].active, ministries[i].id);
	}
	function initialize_amount_page(){
		var amounts = performAJAX("components/tithe/edit_ministries_data.php", "function=init_tithe_amounts", false, "POST");
		if(amounts.value){
			var amt = JSON.parse(amounts.value);
			if( amt.length > 4)
				amt= amt.slice(0,4);
			for( var i=0; i< amt.length; i++)
				add_new_amount(amt[i].tithe_amt, amt[i].status);
		}
	}
	function initialize_ministry_listeners(){
		var add_ministry_btn = document.getElementsByClassName("add_ministry_btn")[0];
			add_ministry_btn.addEventListener("click", add_new_ministry);

		var save_ministries_btn = document.getElementsByClassName("save_ministries_btn")[0];
			save_ministries_btn.addEventListener("click",save_ministries);
	}
	function initialize_amount_listeners(){
		var add_amts_btn = document.getElementsByClassName("add_payment_amt_btn")[0];
			add_amts_btn.addEventListener("click", add_new_amount);

		var save_tithe_amts_btn = document.getElementsByClassName("save_tithe_amounts_btn")[0];
			save_tithe_amts_btn.addEventListener("click",save_tithe_amts);
	}
	function add_new_amount(value, status){
		//console.log(value);
		//console.log(status);
		var container = document.getElementsByClassName("amounts_sortable")[0];
		
		if (typeof status === 'undefined') {
			var kidCount = countTheKids(container);
			if (kidCount >= 4) {
				alert("The current limit for amount definitions is 4.");
				return;
			}
		}
		
		var row = document.createElement("li");
			row.className = "amount_row";

		var amt_input = document.createElement("input");
			amt_input.setAttribute("type","text");
			amt_input.className = "amount_input";
			amt_input.setAttribute("placeholder","New Tithe Amount");
			if(value && value.length)
				amt_input.value= value;

		var amt_container = document.createElement("ministry_name_container");
			amt_container.className = "amount_container";
			amt_container.appendChild(amt_input);

		var amt_status = document.createElement("div");
			amt_status.className = "amt_status_contaner";

		var amt_status_select = document.createElement("select");
			amt_status_select.className ="amt_status";
		
		var opt1 = document.createElement("option");
			opt1.value = "";
			opt1.innerHTML ="Select a Status";

		var opt2 = document.createElement("option");
			opt2.value = "0";
			opt2.innerHTML ="disabled";
			if(status && status == "0")
				opt2.selected=true;

		var opt3 = document.createElement("option");
			opt3.value 	   = "1";
			opt3.innerHTML = "enabled";
			if(status && status == "1")
				opt3.selected=true;

		amt_status_select.appendChild(opt1);
		amt_status_select.appendChild(opt2);
		amt_status_select.appendChild(opt3);
		row.appendChild(amt_container);
		row.appendChild(amt_status_select);
		//row.appendChild(hidden_id);
		container.appendChild(row);
		$(container).sortable();
	}
	
	function countTheKids(parent) {
		var realKids = 0;
		var kids = parent.childNodes.length;
		var i = 0;
		while(i < kids){
			if (parent.childNodes[i].nodeType != 3) {
				realKids++;
			}
			i++;
		}
		
		return realKids;
	}
	
	function add_new_ministry(name, status, id){
		var container = document.getElementsByClassName("ministries_sortable")[0];
		
		if (typeof id === 'undefined') {
			var kidCount = countTheKids(container);
			if (kidCount >= 10) {
				alert("The current limit for ministry definitions is 10.");
				return;
			}
		}
		
		var row = document.createElement("li");
			row.className = "ministry_row";

		var min_name = document.createElement("input");
			min_name.setAttribute("type","text");
			min_name.className = "ministry_name_input";
			min_name.setAttribute("placeholder","New Ministry Name");
			if(name && name.length)
				min_name.value= name;

		var min_name_container = document.createElement("ministry_name_container");
			min_name_container.className = "ministry_name_container";
			min_name_container.appendChild(min_name);

		var min_status = document.createElement("div");
			min_status.className = "min_status_contaner";

		var min_status_select = document.createElement("select");
			min_status_select.className ="ministry_status";
		var opt1 = document.createElement("option");
			opt1.value = "";
			opt1.innerHTML ="Select a Status";

		var opt2 = document.createElement("option");
			opt2.value = "0";
			opt2.innerHTML ="disabled";
			if(status && status == "0")
				opt2.selected=true;

		var opt3 = document.createElement("option");
			opt3.value 	   = "1";
			opt3.innerHTML = "enabled";
			if(status && status == "1")
				opt3.selected=true;

		var hidden_id = document.createElement("input");
			hidden_id.setAttribute("type", "hidden");
			hidden_id.className = "ministry_id";
		if(id)
			hidden_id.value = id;

		min_status_select.appendChild(opt1);
		min_status_select.appendChild(opt2);
		min_status_select.appendChild(opt3);
		/*
		var save_btn = document.createElement("input");
			save_btn.setAttribute("type","button");
			save_btn.value = "Save";
			save_btn.addEventListener("click",save_ministries);*/
		row.appendChild(min_name_container);
		row.appendChild(min_status_select);
		row.appendChild(hidden_id);
	
		
		container.appendChild(row);
		$(container).sortable();
	}
	function save_ministries(event){
		var elem = event.target;
		var rows = document.getElementsByClassName("ministry_row");
		var save_obj = [];
		for(var i =0; i < rows.length; i++){
			var name = rows[i].getElementsByClassName("ministry_name_input")[0].value;
			var status = rows[i].getElementsByClassName("ministry_status")[0].value;
			var id = rows[i].getElementsByClassName("ministry_id")[0].value;
			save_obj.push({"ministry_name" : name,"status":status, "ministry_id":id});
		}

		performAJAX("components/tithe/edit_ministries_data.php", "function=save_ministries&ministries="+encodeURIComponent(JSON.stringify(save_obj)), false, "POST");
		location.reload();
	}
	function save_tithe_amts(){

		var elem = event.target;
		var rows = document.getElementsByClassName("amount_row");
		var save_obj = [];
		for(var i =0; i < rows.length; i++){
			var amt = rows[i].getElementsByClassName("amount_input")[0].value;
			var status = rows[i].getElementsByClassName("amt_status")[0].value;
			save_obj.push({"tithe_amt" : amt,"status":status});
		}
		console.log(save_obj);
		performAJAX("components/tithe/edit_ministries_data.php", "function=save_tithe_amts&tithe_amts="+encodeURIComponent(JSON.stringify(save_obj)), false, "POST");
	}
	function show_inner_select(elem){
		var inner_select_containers = elem.parentNode.getElementsByClassName('inner_select_container');
		for(var i=0; i< inner_select_containers.length; i++){
			inner_select_containers[i].style.display='none';
		}
		elem.parentNode.getElementsByClassName(elem.value)[0].style.display='block';
		set_command_select(elem,'printer');
	}
	
	function set_command_select(elem, type){
		var option;
		var id= elem.id;
		var sel_value= elem.value;
		var select = document.getElementById(id);
		
		for(var j=0; j<select.options.length;j++) {
			option = select.options[j];
		  if (option.value != '' && option.value == sel_value) {
			  option.setAttribute('selected', true);
           } 
		  else{
			  option.removeAttribute('selected', true);
		  }
		}
		
		 if(type == 'printer'){
			 var selPrinter = document.querySelector('#_setting_tbl_id .inner_select > option[name=printer][selected=true]');
			 if(selPrinter) {
				 selPrinter.removeAttribute('selected', true);
			 }
			  
		  }
	}
	
	function set_fiscaltemp_select(elem,modelId){
		var sel_temp = elem.value;
		var sel_tempName = elem.options[elem.selectedIndex].text;
		var getifram = document.querySelector('#_setting_tbl_id #'+modelId+' #modal_body #modal_iframe').getAttribute('src');
		var ifrmInfo = getifram.split("&tpl_id=");
		var setifram = ifrmInfo['0']+"&tpl_id="+sel_temp;
		document.querySelector('#_setting_tbl_id #'+modelId+' #modal_body #modal_iframe').setAttribute('src',setifram);
		document.querySelector('#_setting_tbl_id #'+modelId+' .modal_header #modal_header_text_template').innerHTML = 'Edit Fiscal Template ('+sel_tempName+')';
	}

	function submit_businessInformation(){
	var formData = $("#businessInfo_form" ).serializeArray();

		$.ajax({
		url: '/cp/index.php',
		type: 'GET',
		data: 'mode=home_home&set_rest_vertical='+encodeURIComponent(JSON.stringify(formData)),
		async: 'false',
		success: function (string){
			location.reload();
			}
		});
	}

	/*
		Author: Function added by Ravi Tiwari on 11th Mar 2019
		Purpose: This function is being called when user will click skip button on cp business information prompt
	*/
	function skipBusinessInformation(){
			$.ajax({
			url: '/cp/index.php',
			type: 'GET',
			data: 'mode=home_home&reset_prompted=1',
			async: 'false',
			success: function (string){
				location.reload();
				}
			});
		}

	function checkBusinessInfoFormData() {

	if (($("#business_name").val() != "" && $("#business_address").val() != "" && $("#business_country").val() != "" && $("#business_city").val() != "" && $("#business_state").val() != "" && $("#business_zip").val() != "") && ($("#table_service").val() != "" || $("#quick_service").val() != "" || $("#bar_lounge").val() != "" || $("#coffee_shop").val() != "" || $("#food_truck").val() !="" || $("#pizzeria").val() != "" || $("#other").val() != "")){
					  $('#businessInfoSubmit').removeAttr("disabled");
					  $('#businessInfoSubmit').removeAttr("title");
					  $('#businessInfoSubmit').css({'background':'#99bc08'});
				}
			      else {
					  $('#businessInfoSubmit').attr("disabled", "disabled");
					  $('#businessInfoSubmit').css({'background':'#ccc'});
				}
	}

	 $(function () {
		checkBusinessInfoFormData();
		$("#business_name, #business_address, #business_country, #business_city, #business_state, #business_zip, #service_table_service, #service_quick_service, #service_bar_lounge, #service_coffee_shop, #service_food_truck, #service_pizzeria, #service_other").bind("change keyup click",
		  function () {

		      if (($("#business_name").val() != "" && $("#business_address").val() != "" && $("#business_country").val() != "" && $("#business_city").val() != "" && $("#business_state").val() != "" && $("#business_zip").val() != "") && ($("#table_service").val() != "" || $("#quick_service").val() != "" || $("#bar_lounge").val() != "" || $("#coffee_shop").val() != "" || $("#food_truck").val() !="" || $("#pizzeria").val() != "" || $("#other").val() != "")){
				  $('#businessInfoSubmit').removeAttr("disabled");
				  $('#businessInfoSubmit').removeAttr("title");
				  $('#businessInfoSubmit').css({'background':'#99bc08'});
			}
		      else {
				  $('#businessInfoSubmit').attr("disabled", "disabled");
				  $('#businessInfoSubmit').css({'background':'#ccc'});
			}
		      });
		});

	//This function is for select/deselect service on cp company information	prompt 
	function selectService(id, service){

		 if ( $( '#service_'+id ).hasClass( "ActiveService" ) ) {
			$( '#service_'+id ).removeClass( "ActiveService" );
			$( '#service_'+id ).addClass( "InActiveService" );
			$( '#service_'+id ).css('background','#fff');
			$( '#'+id ).val('');
		    }
		else{
			$( '#service_'+id ).removeClass( "InActiveService" );
			$( '#service_'+id ).addClass( "ActiveService" );
			$( '#service_'+id ).css('background','#ccc');
			$( '#'+id ).val(service);
		}
	}
	
	/*
	function performAJAX(URL, urlString,asyncSetting,type ){
		urlString = urlString+"&data_name="+dn+"&loc_id="+loc_id+"&key="+key;

		var returnVal='';
		$.ajax({
			url: URL,
			type: type,
			data:urlString,
			async:asyncSetting,
			success: function (string){
					try{
						returnVal = JSON.parse(string);
						//console.log(returnVal);
					}catch(e){
						returnVal= decodeURIComponent(string);
						console.log("not json: "+ decodeURIComponent(returnVal));
					}
					
				}
		});
		//doneLoading();
		return returnVal;
	}*/

	// Hiding Notification Top bar forswithcing to new CP
	function hideNotificationBar(el) {
		$.cookie("DISABLE_NEW_CP_NOTIFICATION", 1);
		$(el).slideUp()
	}
