/********************************************************************************
 * About:
 * The iPad doesn't have overflow set to auto on its iframes and extra content
 * spills out onto the page. The solution is to wrap every iframe in a <div> with
 * overflow:auto and -webkit-overflow-scrolling:touch.
 * 
 * @author: Benjamin G. Bean (benjamin@poslavu.com)
 ********************************************************************************/

$(function(){
	if (navigator.userAgent.match(/iPhone|iPod|iPad/g) !== null) {
		ipad_scroll_iframes_init();
	}
});

function ipad_scroll_iframes_init() {
	// can be found int /newcp/backend/js/jquery/iframe_ipad.js
	document.addEventListener("DOMNodeInserted", function(event) {
		if ($(event.target)[0].tagName.toLowerCase() == 'iframe') {
			setTimeout(function() {
				ipad_scroll_iframes();
			}, 100);
			ipad_scroll_iframes();
		}
	});
}

function ipad_scroll_iframes() {
	// can be found int /newcp/backend/js/jquery/iframe_ipad.js
	var iframes = $('iframe');
	
	$.each(iframes, function(k, v) {
		// get the iframe parent
		var jparent = $(v).parent();
		
		// if this id hasn't been processed yet, then wrap it in a div to make it scrollable
		if (!jparent.hasClass('iframe_ipad_scrollable_container')) {
			$(v).wrap(function(){
				var $this = $(this);
				return $('<div class="iframe_ipad_scrollable_container" />').css({
					width: $this.attr('width'),
					height: $this.attr('height'),
					overflow: 'auto',
					'-webkit-overflow-scrolling': 'touch'
				});
			});
			console.log('assigned');
		}
	});
}