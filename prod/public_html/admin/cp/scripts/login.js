(function(){
	window.Lavu = (window.Lavu===void 0)?{}:window.Lavu;
	Lavu.elements = (Lavu.elements===void 0)?{}:Lavu.elements;
	Lavu.login = (Lavu.login===void 0)?{}:Lavu.login;

	function startLoadingBar(){
		try {
			var e = document.querySelector('lavu-color-collection, [isa=lavu-color-collection]');
			e.setAttribute('animate','');
		} catch(e){}
	}

	function stopLoadingBar(){
		try {
			var e = document.querySelector('lavu-color-collection, [isa=lavu-color-collection]');
			e.removeAttribute('animate');
		} catch(e){}
	}

	/**
	 * Returns the Appropriate AJAX Obect
	 *
	 * @returns an XMLHttpRequest object, if it can, or an ActiveXObject equivelent.
	 **/
	function getAjaxObject() {
		if( window.XMLHttpRequest) {
			return new XMLHttpRequest();
		} else {
			return new ActiveXObject("Microsoft.XMLHTTP");
		}
	}

	/**
	 * A Conviencience function that allows for the quick sending/submitting of POST
	 * requests to the url specified.
	 *
	 * @param url: the url to connect to.  It should be relative to the current site,
	 *			since all others would result in an error otherwise.  You don't have
	 *			to submit the full url path.
	 *
	 * @param variables: The variables you'd like to submit to the POST request,
	 *			recommended to be set as an object with fields mapping to values.
	 *			Will also accept a pre-processed URI encoded string, or an array.
	 *
	 * @param callback: The function/object that the request should return the result to.
	 *			Since the AJAX request is asyncronus, this is neccessary.  Please note that
	 *			the function passed will be stored within the AJAX request object, so you
	 *			can get to it by calling this, unless it is an object.
	 *				Ex: this.status, this.responseText.
	 *				Ex: event.target.status, event.target.responseText
	 *
	 *	@returns the AJAXRequest Object that was created in the process.
	 *
	 **/
	function sendPOST( url, variables, callback ) {
		var ajaxRequest = Lavu.getAjaxObject();
		if( ajaxRequest.addEventListener ){
			ajaxRequest.addEventListener( 'readystatechange', callback );
		} else {
			ajaxRequest['onreadystatechange'] = callback;
		}
		console.log(url);
		ajaxRequest.open("POST",url,true);

		var vars = "";
		switch( typeof variables ) {
			case "string" :
				vars = variables;
				break;
			case "object" :
				for( var key in variables ) {
					if(vars) {
						vars += "&";
					}
					vars += encodeURIComponent(key) + "=" + encodeURIComponent(variables[key]);
				}
				break;
			case "array" :
				for( var i = 0; i < variables.length; i++ ){
					if(vars) {
						vars += "&";
					}

					vars += i + "=" + encodeURIComponent( variables[i] );
				}
				break;
			default : break;
		}
		
		ajaxRequest.setRequestHeader("Content-type","application/x-www-form-urlencoded");
		ajaxRequest.send( vars );
		return ajaxRequest;
	}
	Lavu.getAjaxObject = getAjaxObject;
	Lavu.sendPOST = sendPOST;
	
	(function(){
		function SubmissionValidator(){
			this._fields = {}
			this._button = null;
			this._request = null;
			this._message_area = null;
		}

		function submissionURL(){
			return location.href;
		}
		function submissionData(){
			var obj = [];
			for( var name in this._fields ){
				obj.push( encodeURIComponent( name ) + '=' + encodeURIComponent( this._fields[name].value ) );
			}

			return obj.join('&');
		}

		function setMessageArea( message_area ){
			this._message_area = message_area;
		}

		function _setMessage( message ) {
			while( this._message_area.childNodes.length ) {
				this._message_area.removeChild( this._message_area.childNodes[0] );
			}

			this._message_area.appendChild( document.createTextNode( message ) );
		}

		function setErrorMessage( message ){
			this._message_area.removeAttribute('success');
			this._message_area.removeAttribute('error');
			this._setMessage( message );
			if( message != '' ){
				this._message_area.setAttribute('error','');
			}
			
			this._message_area.removeAttribute('success');
		}

		function setSuccessMessage( message ) {
			this._message_area.removeAttribute('success');
			this._message_area.removeAttribute('error');
			this._setMessage( message );

			if( message != '' ){
				this._message_area.setAttribute('success','');
			}
		}

		function addField( name, field ){
			if( name === undefined || name === null ||
				field === undefined || field === null ){
				return;
			}
			if( this._fields[name] !== undefined ){
				field.removeEventListener('keydown', this, false );
			}
			this._fields[name] = field;
			field.addEventListener( 'keydown', this, false );
		}
		function fields(){
			return this._fields;
		}
		function setSubmissionButton( button ){
			if( button === null || button === undefined ){
				return;
			}
			if( this._button !== null ){
				this._button.removeEventListener('click', this, false );	
				this._button.removeEventListener('touchup', this, false );
			}
			this._button = button;
			this._button.addEventListener('click', this, false );	
			this._button.addEventListener('touchup', this, false );
		}
		function button(){
			return this._button;
		}

		function submit(){}

		function validateAll(){
			for( var name in this._fields ){
				if( this._fields[name].validate === undefined ){
					continue;
				}
				if( !this._fields[name].validate() ) {
					return false;
				}
			}
			return true;
		}

		function handleEvent( event ){
			//submissions
			switch( event.type ){
				case 'keydown':
				if( (event.keyCode == 0x0D || event.keyIdentifier == 'Enter' || event.which == 0x0D ) && this.validateAll() ){
					// We should submit
					this.submit();
				}
				break;
				case 'click':
				case 'touchup':
					this.submit();
				break;
				default:
				//que?
			}
		}

		Lavu.login.SubmissionValidator = SubmissionValidator;
		SubmissionValidator.prototype = Object.create( Object.prototype );
		SubmissionValidator.prototype.constructor = SubmissionValidator;
		SubmissionValidator.prototype.fields = fields;
		SubmissionValidator.prototype.addField = addField;
		SubmissionValidator.prototype.button = button;
		SubmissionValidator.prototype.setSubmissionButton = setSubmissionButton;
		SubmissionValidator.prototype.submissionURL = submissionURL;
		SubmissionValidator.prototype.submissionData = submissionData;
		SubmissionValidator.prototype.handleEvent = handleEvent;
		SubmissionValidator.prototype.submit = submit;
		SubmissionValidator.prototype.validateAll = validateAll;

		SubmissionValidator.prototype.setMessageArea = setMessageArea;
		SubmissionValidator.prototype._setMessage = _setMessage;
		SubmissionValidator.prototype.setErrorMessage = setErrorMessage;
		SubmissionValidator.prototype.setSuccessMessage = setSuccessMessage;
	})();

	(function(){
		function LoginSubmissionValidator(){
			Lavu.login.SubmissionValidator.call( this );
		}

		function submit(){
			this.setErrorMessage('');
			if( !this.validateAll() ){
				return;
			}

			/*
			* @jira-url https://jira.lavu.com/browse/LP-6768
			* Commented for the issue related to CP-Login that was causing double form submit, 
			* 
			* One via ajax using sendPost down below, Line:254
			* One via document.forms[0].submit();
			* Thus to removing double form submission that creates CPU utilization to maximum
			*/
			//document.forms[0].submit();
			// does nothing other than prompt browser to consider inputs to store credentials

			startLoadingBar();
			var self = this;
			var request = sendPOST( this.submissionURL(), this.submissionData(), function(){
				if( request && request.readyState === 4 ){
					stopLoadingBar();
					if( request.status === 200 ) {
						var result = JSON.parse(request.responseText);

						if( result.status == 'success' ){
							if( result.forward ){
								window.location.replace( result.forward );
							}

							if( result.message  ){
								self.setSuccessMessage( result.message );
							}
						} else {
							self.setErrorMessage( result.message );
						}
					} else {
						self.setErrorMessage('Server side error!');
					}
				}
			} );
			self._request = request;
		}

		var _validator = null;
		function sharedValidator(){
			if( _validator === null ){
				_validator = new LoginSubmissionValidator();
			}
			return _validator;
		}

		Lavu.login.LoginSubmissionValidator = LoginSubmissionValidator;
		LoginSubmissionValidator.prototype = Object.create( Lavu.login.SubmissionValidator.prototype );
		LoginSubmissionValidator.prototype.constructor = LoginSubmissionValidator;
		LoginSubmissionValidator.prototype.submit = submit;

		LoginSubmissionValidator.sharedValidator = sharedValidator;
	})();

	(function(){
		function ForgotPasswordSubmissionValidator(){
			Lavu.login.SubmissionValidator.call( this );
		}

		function submit(){
			this.setErrorMessage('');
			if( !this.validateAll() ){
				return;
			}

			startLoadingBar();
			var self = this;
			var request = sendPOST( this.submissionURL(), this.submissionData(), function(){
				if( request && request.readyState === 4 ){
					stopLoadingBar();
					if( request.status === 200 ) {
						var result = JSON.parse(request.responseText);

						if( result.status == 'success' ){
							if( result.forward ){
								window.location.replace( result.forward );
							}

							if( result.message  ){
								self.setSuccessMessage( result.message );
							}
						} else {
							self.setErrorMessage( result.message );
						}
					} else {
						self.setErrorMessage('Server side error!');
					}
				}
			} );
			self._request = request;
		}

		var _validator = null;
		function sharedValidator(){
			if( _validator === null ){
				_validator = new ForgotPasswordSubmissionValidator();
			}
			return _validator;
		}

		Lavu.login.ForgotPasswordSubmissionValidator = ForgotPasswordSubmissionValidator;
		ForgotPasswordSubmissionValidator.prototype = Object.create( Lavu.login.SubmissionValidator.prototype );
		ForgotPasswordSubmissionValidator.prototype.constructor = ForgotPasswordSubmissionValidator;
		ForgotPasswordSubmissionValidator.prototype.submit = submit;

		ForgotPasswordSubmissionValidator.sharedValidator = sharedValidator;
	})();

	(function(){
		function ResetPasswordSubmissionValidator(){
			Lavu.login.SubmissionValidator.call( this );
		}

		function submissionURL(){
			return '?';
		}

		function submit(){
			this.setErrorMessage('');
			if( !this.validateAll() ){
				return;
			}

			startLoadingBar();
			var self = this;
			var request = sendPOST( this.submissionURL(), this.submissionData(), function(){
				if( request && request.readyState === 4 ){
					stopLoadingBar();
					if( request.status === 200 ) {
						var result = JSON.parse(request.responseText);

						if( result.status == 'success' ){
							if( result.forward ){
								window.location.replace( result.forward );
							}

							if( result.message  ){
								self.setSuccessMessage( result.message );
							}
						} else {
							self.setErrorMessage( result.message );
						}
					} else {
						self.setErrorMessage('Server side error!');
					}
				}
			} );
			self._request = request;
		}

		var _validator = null;
		function sharedValidator(){
			if( _validator === null ){
				_validator = new ResetPasswordSubmissionValidator();
			}
			return _validator;
		}

		Lavu.login.ResetPasswordSubmissionValidator = ResetPasswordSubmissionValidator;
		ResetPasswordSubmissionValidator.prototype = Object.create( Lavu.login.SubmissionValidator.prototype );
		ResetPasswordSubmissionValidator.prototype.constructor = ResetPasswordSubmissionValidator;
		ResetPasswordSubmissionValidator.prototype.submissionURL = submissionURL;
		ResetPasswordSubmissionValidator.prototype.submit = submit;

		ResetPasswordSubmissionValidator.sharedValidator = sharedValidator;
	})();

	document.addEventListener('DOMContentLoaded', function(){
		(function(){
			/**
			 * Login Section
			 */
			var loginUserName = document.querySelector( '#login_section lavu-text-input, #login_section [isa=lavu-text-input]' );
			var loginPassword = document.querySelector( '#login_section lavu-password-input, #login_section [isa=lavu-password-input]' );
			var loginButton = document.querySelector( '#login_section lavu-button, #login_section [isa=lavu-button]' );
			var lavuError = document.querySelector( '#login_section lavu-error, #login_section [isa=lavu-error]' );

			var loginLoginSubmissionValidator = Lavu.login.LoginSubmissionValidator.sharedValidator();
			loginLoginSubmissionValidator.addField( 'username', loginUserName );
			loginLoginSubmissionValidator.addField( 'password', loginPassword );
			loginLoginSubmissionValidator.setSubmissionButton( loginButton );
			loginLoginSubmissionValidator.setMessageArea( lavuError );
		})();

		(function(){
			/**
			 * Forgot Section
			 */
			var loginUserName = document.querySelector( '#forgot_section lavu-text-input, #forgot_section [isa=lavu-text-input]' );
			var loginButton = document.querySelector( '#forgot_section lavu-button, #forgot_section [isa=lavu-button]' );
			var lavuError = document.querySelector( '#forgot_section lavu-error, #login_section [isa=lavu-error]' );

			var forgotPasswordSubmissionValidator = Lavu.login.ForgotPasswordSubmissionValidator.sharedValidator();
			forgotPasswordSubmissionValidator.addField( 'username_reset', loginUserName );
			forgotPasswordSubmissionValidator.setSubmissionButton( loginButton );
			forgotPasswordSubmissionValidator.setMessageArea( lavuError );
		})();

		(function(){
			/**
			 * Reset Password Section
			 */
			var loginUserName = document.querySelector( '#reset_password_section lavu-text-input, #reset_password_section [isa=lavu-text-input]' );
			var passwordFields = document.querySelectorAll( '#reset_password_section lavu-password-input, #reset_password_section [isa=lavu-password-input]' );
			var loginPassword1 = passwordFields[0];
			var loginPassword2 = passwordFields[1];
			var tokenField = document.querySelector( 'input#token_field' );
			var loginButton = document.querySelector( '#reset_password_section lavu-button, #reset_password_section [isa=lavu-button]' );
			var lavuError = document.querySelector( '#reset_password_section lavu-error, #login_section [isa=lavu-error]' );

			var resetPasswordSubmissionValidator = Lavu.login.ResetPasswordSubmissionValidator.sharedValidator();
			resetPasswordSubmissionValidator.addField( 'token', tokenField );
			resetPasswordSubmissionValidator.addField( 'username_action', loginUserName );
			resetPasswordSubmissionValidator.addField( 'password', loginPassword1 );
			resetPasswordSubmissionValidator.addField( 'password1', loginPassword2 );
			resetPasswordSubmissionValidator.setSubmissionButton( loginButton );
			resetPasswordSubmissionValidator.setMessageArea( lavuError );
		})();
	}, false);
})();
