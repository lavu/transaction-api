(function(){
	var usingCustomLoader = false;

	/**
	 * IE8 Compatability
	 */
	(function(){

		if( !window.console ){
			window.console = {
				log: function(){}
			};
		}

		if (!Object.create) {
			Object.create = (function(){
				function F(){}

				return function(o){
					if (arguments.length != 1) {
						throw new Error('Object.create implementation only accepts one parameter.');
					}
					F.prototype = o;
					return new F()
				}
			})();
		}

		if( Array.prototype.indexOf === undefined ){
			Array.prototype.indexOf = function( ele ) {
				for( var i = 0; i < this.length; i++ ){
					if( this[i] == ele ) {
						return i;
					}
				}
				return -1;
			}
		}

		if( Array.prototype.forEach === undefined ) {
			Array.prototype.forEach = function( func ) {
				for( var i = 0; i < this.length; i++ ){
					func( this[i], i, this );
				}
			}
		}

		if( window.HTMLElement === undefined ){
			window.HTMLElement = Element;
		}

		if( HTMLElement.prototype.addEventListener === undefined ){
			HTMLElement.prototype.addEventListener = function( eventName, func, block ){
				var eventName = 'on' + eventName;
				if( typeof func == 'object' && func.handleEvent !== undefined ){
					this.attachEvent( eventName, func.handleEvent.bind( func ) );
				} else {
					this.attachEvent( eventName, func );
				}
			}
			HTMLDocument.prototype.addEventListener = HTMLElement.prototype.addEventListener;
		}

		if( HTMLElement.prototype.removeEventListener === undefined ){
			HTMLElement.prototype.removeEventListener = function( eventName, func, block ) {
				var eventName = 'on' + eventName;
				if( typeof func == 'object' && func.handleEvent !== undefined ){
					this.detachEvent( eventName, func.handleEvent.bind( func ) );
				} else {
					this.detachEvent( eventName, func );
				}
			}
			HTMLDocument.prototype.removeEventListener = HTMLElement.prototype.removeEventListener;
		}

		try {
			if( document.documentElement.classList === undefined ){

				function classList( ele ){
					this._ele = ele;
					this._arr = [];
					if( ele.getAttribute('class') !== null && ele.getAttribute('class') !== undefined ){
						this._arr = ele.getAttribute('class').split( ' ' );
					}
				};

				function add( classStr ) {
					if( this.contains( classStr ) ){
						return;
					}

					this._arr.push( classStr );
					this.update();
				}

				function remove( classStr ) {
					var index = this._arr.indexOf( classStr );
					if( index === -1 ){
						return;
					}
					this._arr.splice( index, 1 );
					this.update();
				}

				function contains( classStr ) {
					return this._arr.indexOf( classStr ) !== -1;
				}

				function toggle( classStr ) {
					if( this.contains( classStr ) ){
						this.remove( classStr );
					} else {
						this.add( classStr );
					}
				}

				function item( i ){
					return this._arr[i];
				}

				function update(){
					this._ele.setAttribute( 'class', this._arr.join(' ') );
				}

				classList.prototype = Object.create( Object.prototype );
				classList.prototype.constructor = classList;
				classList.prototype.add = add;
				classList.prototype.contains = contains;
				classList.prototype.item = item;
				classList.prototype.remove = remove;
				classList.prototype.toggle = toggle;
				classList.prototype.update = update;

				Object.defineProperty( HTMLElement.prototype, 'classList', {
					get: function() { return new classList(this) },
				});
			}
		} catch( e ) {

		}

		if( HTMLElement.prototype.remove === undefined ){
			HTMLElement.prototype.remove = function() {
				if( this.parentNode !== null && this.parentNode !== undefined ){
					this.parentNode.removeChild( this );
				}
			}
		}

		if (!Function.prototype.bind) {
			Function.prototype.bind = function(oThis) {
				if (typeof this !== 'function') {
					// closest thing possible to the ECMAScript 5
					// internal IsCallable function
					throw new TypeError('Function.prototype.bind - what is trying to be bound is not callable');
				}

				var aArgs   = Array.prototype.slice.call(arguments, 1),
					fToBind = this,
					fNOP    = function() {},
					fBound  = function() {
						return fToBind.apply(this instanceof fNOP && oThis
								? this
								: oThis,
								aArgs.concat(Array.prototype.slice.call(arguments)));
					};

				fNOP.prototype = this.prototype;
				fBound.prototype = new fNOP();

				return fBound;
			};
		}

		(function(){
			function IE8RestructureMisinterprettedDOM() {
				this._element = document.body;
				this.iterate( this._element, [] );
			}

			function getElementListForElement( element ) {
				var result = [];
				for( var i = 0; i < element.childNodes.length; i++ ){
					result.push( element.childNodes[i] );
				}
				return result;
			}

			function isCustomElement( element ) {
				if( element instanceof Text ){
					return false;
				}
				return element.tagName.indexOf('-') !== -1;
			}

			function iterate( element, arr ) {
				var myChildren = this.getElementListForElement( element );

				for( var i = 0 ; i < myChildren.length; i++ ){
					arr.splice( i, 0, myChildren[i] );
				}

				after = [];

				while( arr.length ){
					var ele = arr.splice( 0, 1 );
					ele = ele[0];
					if( ele instanceof Text ){
						element.appendChild( ele );
						continue;
					}

					if( ele.tagName.substr(0,1) == '/' ){
						if( ele.tagName.substr(1) != element.tagName || ele.getAttribute('isa') != ele.tagName.substr(1) ){
							console.log('WEIRD OR FALLBACK');
						}
						ele.remove();
						break;
					}

					if( this.isCustomElement( ele ) ) {
						// In this case, let's eval this node, and append it to element
						ele.remove();
						var newElem = document.createElement( ele.tagName );

						for( var i = 0; i < ele.attributes.length; i++ ){
							newElem.setAttribute( ele.attributes[i].name, ele.attributes[i].value );
						}

						element.appendChild( newElem );
						this.iterate( newElem, arr );
					} else {
						element.appendChild( ele );
						after.push( ele );
					}
				}

				for( var i = 0; i < after.length; i++ ){
					this.iterate( after[i], [] );
				}
			}
			window.IE8RestructureMisinterprettedDOM = IE8RestructureMisinterprettedDOM;
			IE8RestructureMisinterprettedDOM.prototype = Object.create( Object.prototype );
			IE8RestructureMisinterprettedDOM.prototype.constructor = IE8RestructureMisinterprettedDOM;
			IE8RestructureMisinterprettedDOM.prototype.getElementListForElement = getElementListForElement;
			IE8RestructureMisinterprettedDOM.prototype.isCustomElement = isCustomElement;
			IE8RestructureMisinterprettedDOM.prototype.iterate = iterate;
		})();
	})();

	(function(){
		function CustomElementLoader(){
			this._registeredElements = {};
			this.ogce = document.createElement;
		}

		var _customElementLoader;
		function loader(){
			if( !_customElementLoader ){
				_customElementLoader = new CustomElementLoader();
			}
			return _customElementLoader;
		}

		function shouldFallback( node ){
			return !(node instanceof HTMLElement) ||
			document.documentElement.classList.contains( 'ie9' ) ||
			document.documentElement.classList.contains( 'ie8' ) ||
			navigator.userAgent.indexOf( 'MSIE 10' ) !== -1;
		}

		function createElement(){
			var node = this.ogce.apply(document,arguments);
			var nodeName = node.tagName.toLowerCase();

			if( this._registeredElements[ nodeName ] !== undefined && !(node instanceof this._registeredElements[ nodeName ]) ){
				if( node.setPrototypeOf ){
					node.setPrototypeOf( this._registeredElements[ nodeName ].prototype );
				} else if( node.__proto__ !== void 0 ){
					//for most modern browsers that don't have the setPrototypeOf
					node.__proto__ = this._registeredElements[ nodeName ].prototype;
				} else {
					// node = Object.create( this._registeredElements[ nodeName ].prototype );
					node = new this._registeredElements[ nodeName ]();
				}

				if( shouldFallback(node) ) {
					// Some stupid Inhertance chaining is preventing us from doing
					// this nicely... let's try to force things, by abandoning the
					// custom element type, and falling back
					// 
					// this will consume FAR more memory, and will probably slow
					// things down.
					// 
					// primarily for compatability
					node = document.createElement('div');
					node.setAttribute( 'isa', nodeName );
					
					node.__already_built = true;
					node.__fallback = true;

					// var methods = [
					// 	'attachedCallback',
					// 	'attributeChangedCallback',
					// 	'createdCallback',
					// 	'detachedCallback'
					// ];

					for( var name in this._registeredElements[ nodeName ].prototype ) {
						if( node[name] !== undefined ){
							continue;
						}
						node[name] = this._registeredElements[ nodeName ].prototype[name];
					}
				}

				if( node.createdCallback && "function" == typeof node.createdCallback ){
					node.createdCallback();
				}
			} else if( nodeName.indexOf('-') !== -1 && this._registeredElements[ nodeName ] === undefined && shouldFallback(node) ) {
				// console.log( nodeName );
				node = document.createElement('div');
				node.setAttribute( 'isa', nodeName );
				
				node.__already_built = true;
				node.__fallback = true;				
			}

			return node;
		}

		function replaceNode( node, nodeName ){
			var newNode = document.createElement( nodeName );
			node.parentNode.replaceChild( newNode, node );
			while( node.childNodes.length ) {
				newNode.appendChild( node.childNodes[0] );
			}

			for( var i = 0; i < node.attributes.length; i++ ){
				newNode.setAttribute( node.attributes[i].name, node.attributes[i].value );
			}

			return newNode;
		}

		function lookForCustomNodes( node, recur ){
			if(recur === void 0){
				recur = true;
			}
			if( !node.tagName ){
				return;
			}
			var nodeName = node.tagName.toLowerCase();
			if( 'template' == node.tagName.toLowerCase() ){
				return;
			}
			if( this._registeredElements[ nodeName ] !== undefined && !(node instanceof this._registeredElements[ nodeName ]) ){
				//This has been previously registered
				if( node.setPrototypeOf ){
					node.setPrototypeOf( this._registeredElements[ nodeName ].prototype );
				} else  if( node.__proto__ !== void 0 ){
					//for most modern browsers that don't have the setPrototypeOf
					node.__proto__ = this._registeredElements[ nodeName ].prototype;
				} else if( node.__already_built === undefined || node.__already_built === false ) {
					node = replaceNode( node, nodeName );
				}

				if( node.createdCallback && "function" == typeof node.createdCallback && !(node.__fallback && node.__already_built) ){
					node.createdCallback();
				}
			} else if ( this._registeredElements[ nodeName ] === undefined && nodeName.indexOf('-') !== -1 && shouldFallback( node ) ) {
				node = replaceNode( node, nodeName );

				console.log( nodeName );
			}

			if( node.attachedCallback && "function" == typeof node.attachedCallback ){
				node.attachedCallback();
			}

			// if( node.__fallback ){
			// 	if( this._registeredElements[ nodeName ].prototype.attachedCallback && 'function' == typeof this._registeredElements[ nodeName ].prototype.attachedCallback ) {
			// 		this._registeredElements[ nodeName ].prototype.attachedCallback.call( node );
			// 	}
			// }

			if( recur ){
				for( var i = 0; i < node.childNodes.length; i++ ){
					this.lookForCustomNodes( node.childNodes[i], recur );
				}
			}
		}

		function removeNodeEvent( event ) {
			this.nodeRemoved( event.target );
		}

		function nodeRemoved( removedNode ){
			if( !(removedNode instanceof HTMLElement) ){
				return;
			}

			if( removedNode.detachedCallback && "function" == typeof removedNode.detachedCallback ){
				removedNode.detachedCallback();
			}			
		}

		function addNodeEvent( event ){
			this.nodeAdded( event.target );
		}

		function nodeAdded( addedNode ) {
			if( !(addedNode instanceof HTMLElement) ){
				return;
			}

			this.lookForCustomNodes( addedNode );
		}

		function attributeModifiedEvent( event ) {
			this.nodeAttributeModified( event.target, event.attrName, event.prevValue, event.newValue );
		}

		function nodeAttributeModified( node, name, oldValue, newValue ){
			if( !(node instanceof HTMLElement) ){
				return;
			}
			if( node.attributeChangedCallback && "function" == typeof node.attributeChangedCallback ){
				node.attributeChangedCallback( name, oldValue, newValue );
			}
		}


		function mutationObservation( mutations ){
			// console.log( mutations );
			for( var i = 0; i < mutations.length; i++ ){
				var mutationRecord = mutations[i];
				for( var j = 0; j < mutationRecord.addedNodes.length; j++ ){
					var addedNode = mutationRecord.addedNodes[j];
					// this.lookForCustomNodes( addedNode );
					this.nodeAdded( addedNode );
				}

				for( var j = 0; j < mutationRecord.removedNodes.length; j++ ){
					var removedNode = mutationRecord.removedNodes[j];
					if( !(removedNode instanceof HTMLElement) ){
						continue;
					}

					this.nodeRemoved( removedNode );
				}

				if( mutationRecord.type == 'attributes' ){
					var tar = mutationRecord.target;
					this.nodeAttributeModified( tar, mutationRecord.attributeName, mutationRecord.oldValue, tar.getAttribute( mutationRecord.attributeName ) );
				}
			}
		}

		function registerElement( tagname, obj ){

			var lowerTagName = tagname.toLowerCase();
			var replacedTagName = tagname.replace(/-/g, '_' );

			if( this._registeredElements[lowerTagName] ){
				var err = new Error("Failed to execute 'registerElement' on 'Document': Registration failed for type '"+tagname+"'. A type with that name is already registered.");
				err.code = 11;
				throw err;
			}

			this._registeredElements[lowerTagName] = function(){};
			this._registeredElements[lowerTagName].prototype = Object.create( obj.prototype );

			return this._registeredElements[lowerTagName];
		}

		window.CustomElementLoader = CustomElementLoader;
		CustomElementLoader.prototype.constructor = CustomElementLoader;
		CustomElementLoader.prototype = Object.create( Object.prototype );
		CustomElementLoader.prototype.lookForCustomNodes = lookForCustomNodes;
		CustomElementLoader.prototype.registerElement = registerElement;
		CustomElementLoader.prototype.createElement = createElement;
		CustomElementLoader.prototype.mutationObservation = mutationObservation;
		CustomElementLoader.prototype.removeNodeEvent = removeNodeEvent;
		CustomElementLoader.prototype.nodeRemoved = nodeRemoved;
		CustomElementLoader.prototype.addNodeEvent = addNodeEvent;
		CustomElementLoader.prototype.nodeAdded = nodeAdded;
		CustomElementLoader.prototype.attributeModifiedEvent = attributeModifiedEvent;
		CustomElementLoader.prototype.nodeAttributeModified = nodeAttributeModified;
		CustomElementLoader.loader = loader;

		if( !document.registerElement ){
			usingCustomLoader = true;
			if( window.MutationObserver !== void 0 ){
				var observer = new MutationObserver( CustomElementLoader.loader().mutationObservation.bind( CustomElementLoader.loader() ) );
				observer.observe( document.documentElement, {
					childList: true,
					attributes: true,
					characterData: true,
					subtree: true,
					attributeOldValue: true,
					characterDataOldValue: true
				});
			} else {
				//Fall back onto MutationEvents
				document.documentElement.addEventListener('DOMNodeInserted',  CustomElementLoader.loader().addNodeEvent.bind( CustomElementLoader.loader() )  );
				document.documentElement.addEventListener('DOMNodeRemoved',  CustomElementLoader.loader().removeNodeEvent.bind( CustomElementLoader.loader() )  );
				document.documentElement.addEventListener('DOMAttrModified',  CustomElementLoader.loader().attributeModifiedEvent.bind( CustomElementLoader.loader() )  );
			}

			document.registerElement = CustomElementLoader.loader().registerElement.bind( CustomElementLoader.loader() );
			document.createElement =  CustomElementLoader.loader().createElement.bind( CustomElementLoader.loader() );
		}
	})();

	(function(){
		function CustomElement(){
			var templates = document.getElementsByTagName( 'template' );
			var template;
			for( var i = 0; i < templates.length; i++ ){
				if( templates[i].id.toLowerCase() == this.tagName.toLowerCase() ){
					template = templates[i];
				}
			}
			if( template ){
				if( template.content ){
					this.appendChild( document.importNode( template.content, true ));
				} else {
					for( var j = 0; j < template.childNodes.length; j++ ){
						this.appendChild( document.importNode( template.childNodes[j], true ) );
					}
				}
			}
		}

		window.CustomElement = CustomElement;
		CustomElement.prototype = Object.create( HTMLElement.prototype );
		CustomElement.prototype.constructor = CustomElement;
	})();

	(function(){
		var tabIndex = 1;
		function CustomFocusableElement(){
			CustomElement.call( this );
			if( !this.hasAttribute( 'tabindex' ) ){
				this.setAttribute( 'tabindex', tabIndex++ );
			}
		}

		window.CustomFocusableElement = CustomFocusableElement;
		CustomFocusableElement.prototype = Object.create( CustomElement.prototype );
		CustomFocusableElement.prototype.constructor = CustomFocusableElement;
	})();

	var f = function( event ) {
		document.body.appendChild(document.createTextNode(''));
		document.removeEventListener('DOMContentLoaded', f, false);
		if( usingCustomLoader ){
 			CustomElementLoader.loader().lookForCustomNodes( document.body, true );
		}
	}
	document.addEventListener('DOMContentLoaded', f, false );
})();