<!- 
    Written by Brian D.
    Creates a date chooser that will be used in form_dialog.
    Uses 3 html selects.
 ->

<script language='javascript'>


    function parse_int(s_value) {
       if (typeof(s_value) == "number")
               return s_value;
       if (!s_value)
               return 0;
       if (s_value.length == 0)
               return 0;
       s_value = s_value.replace(/^0*/, '');
       return parseInt(s_value);
    }

 

    function dc_combine_date_parts(outputElementID, initializing){
        
        var currentVar = window[ "date_picker_setval_" + outputElementID ];
        
        var loadYear;
        var loadMonth;
        var loadDay;
        var loadingFromDatabase = false;
        if(currentVar && currentVar != ''){
            var parts = currentVar.split('-');
            if(parts.length > 2){
                loadYear = parse_int(parts[0]);
                loadMonth = parse_int(parts[1]);
                loadDay = parse_int(parts[2]);
                loadingFromDatabase = true;
            }
        }

        var bd_year  = document.getElementById('dc_date_year_part_select_'  + outputElementID);
        var bd_month = document.getElementById('dc_date_month_part_select_' + outputElementID);
        var bd_day   = document.getElementById('dc_date_day_part_select_'   + outputElementID);
        
        if(loadingFromDatabase && initializing){
            bd_month.selectedIndex = loadMonth-1;
            bd_day.selectedIndex = loadDay-1;
            for(var k = 0; k < bd_year.options.length; k++){
                if(bd_year.options[k].value == loadYear){
                    bd_year.selectedIndex = k;
                }
            }
        }
        
        
        //alert(bd_month.selectedIndex);
        var bd_y_str = bd_year.options[bd_year.selectedIndex].value;
        
        var bd_m_str = bd_month.options[bd_month.selectedIndex].value;
        //alert('bd_m_str: ' + bd_m_str);
        //alert('parsed as int:' + parse_int(bd_m_str));
        var bd_d_str = bd_day.options[bd_day.selectedIndex].value;
   
        //Finally, we want to make sure that the date is valid.  
        //We change the number of options in the date select based
        //on what the month and year is.
        //alert("year:[" + bd_y_str + "]\nMonth:[" + bd_m_str + "]\n\n");
        var daysInSelectedMonth = dc_days_in_month(bd_y_str, bd_m_str);
        var daySelectedIndex = bd_day.selectedIndex;

        //Reset the amount of days in the selected month to be accurate
        bd_day.options.length = 0;
        var i;
        for(i = 0; i < daysInSelectedMonth; i++){
            var dayVal = i+1;
            dayVal = dayVal > 9 ? dayVal : "0" + (i+1);
            bd_day.options[i] = new Option(dayVal, dayVal);
        }
        if( daySelectedIndex < daysInSelectedMonth ){
            bd_day.selectedIndex = daySelectedIndex;
        }else{
            bd_day.selectedIndex = daysInSelectedMonth - 1;
        }
        
        bd_d_str = bd_day.options[bd_day.selectedIndex].value;
        var outputValue = bd_y_str + '-' + bd_m_str + '-' + bd_d_str;
        
        
        //The output element is the javascript object in form_dialoge
        //that is set, then upon (save) is posted and updated in the db.
        document.getElementById(outputElementID).value = outputValue;
    }
    
    //Takes the year and month as integers and returns how many days 
    //are in each month, we need year to account for Feb, leap year.
    function dc_days_in_month(year, month){
        
        month = parse_int(month);
        year  = parse_int(year);
        
        //alert('month:' + month + '  year:' + year);
        var leapYearOffset = 0;
        if(year % 4 == 0)
            leapYearOffset = 1;
        switch(month){
            case 1: 
                return 31;
            case 2:
                return 28 + leapYearOffset;
            case 3:
                return 31;
            case 4:
                return 30;
            case 5:
                return 31;
            case 6:
                return 30;
            case 7:
                return 31;
            case 8:
                return 31;
            case 9:
                return 30;
            case 10:
                return 31;
            case 11:
                return 30;
            case 12:
                return 31;
        }        
    }
    

</script>
    

<?php
    function dc_month_num_2_abr($monthInt){

        switch($monthInt){
            case '1': 
                return 'Jan';
            case '2':
                return 'Feb';
            case '3':
                return 'Mar';
            case '4':
                return 'Apr';
            case '5':
                return 'May';
            case '6':
                return 'Jun';
            case '7':
                return 'Jul';
            case '8':
                return 'Aug';
            case '9':
                return 'Sep';
            case '10':
                return 'Oct';
            case '11':
                return 'Nov';
            case '12':
                return 'Dec';
        }        
    }
    
    //Returns a string that when echoed will output the necessary html components.
    function create_date_picker($elementID, $yearsBackward, $yearsForward, $yearPos, $defaultToTodaysDate = false, $initialValue = null){
        
        
        //echo "Element ID: " . $elementID;
        
        $yearsBackward = abs($yearsBackward);
        
        //$elementID = "\"$elementID\"";
        
        //TODO we need to set these based on the mercants timzone.
        $nowYear  = date('Y');
        $nowMonth = date('m');
        $nowDay   = date('d');  
    
        //What we be defaulted on the date chooser
        $initializedYear  = $nowYear;
        $initializedMonth = $nowMonth;
        $initializedDay   = $nowDay;
        
        
        if($initialValue){
            $parts = explode('-', $initialValue);
            $regExpDateMatch = preg_match("/^[0-9][0-9][0-9][0-9]$/", $parts[0]) &&
                               preg_match("/^[0-1][0-9]$/", $parts[1]) &&
                               preg_match("/^[0-3][0-9]$/", $parts[2]);
            if($regExpDateMatch && $initialValue){
                $initializedYear  = $parts[0];
                $initializedMonth = $parts[1];
                $initializedDay   = $parts[2];
            }
        }

        $initializingDate = $defaultToTodaysDate || !empty($initialValue);
        $htmlOutputString = '';
        
	    //Month
	    $htmlOutputString .= "<select id='dc_date_month_part_select_$elementID' onchange='dc_combine_date_parts(\"$elementID\")' >";
	    for($i = 1; $i < 13; $i++){
	        $di = $i > 9 ? $i : '0' . $i;
	        $frontOfTag = ($initializingDate && $initializedMonth == $i) ? "<option value='$di' selected>" : "<option value='$di'>";
		    $htmlOutputString .= $frontOfTag . dc_month_num_2_abr($i) . "</option>";
	    }
	    $htmlOutputString .= "</select>";
	    
	    //Day
	    $htmlOutputString .= "<select id='dc_date_day_part_select_$elementID' onchange='dc_combine_date_parts(\"$elementID\")' >";
	    for($i = 1; $i < 32; $i++){
	        $di = $i > 9 ? $i : '0' . $i;
	        $frontOfTag = ($initializingDate && $initializedDay == $i) ? "<option value='$di' selected>" : "<option value='$di'>";
		    $htmlOutputString .= $frontOfTag . $di . "</option>";
	    }
	    $htmlOutputString .= "</select>";
	    
	    //Year
	    $yearLowerBound = $nowYear - $yearsBackward;
	    $yearUpperBound = $nowYear + $yearsForward;
	    $htmlOutputString .= "<select id='dc_date_year_part_select_$elementID' onchange='dc_combine_date_parts(\"$elementID\")' >";
	    for($i = $yearLowerBound; $i <= $yearUpperBound; $i++){
	        if($yearPos < 0){
    	        $frontOfTag = ($yearLowerBound == $i) ? "<option value='$i' selected>" : "<option value='$i'>";
	        }
	        else if($yearPos > 0){
    	        $frontOfTag = ($yearUpperBound == $i) ? "<option value='$i' selected>" : "<option value='$i'>";
	        }
	        else{
    	        $frontOfTag = ($initializedYear == $i) ? "<option value='$i' selected>" : "<option value='$i'>";
	        }
	        $frontOfTag = ($initializingDate && $initializedYear == $i) ? "<option value='$i' selected>" : $frontOfTag;
		    $htmlOutputString .= $frontOfTag.$i."</option>";
	    }
	    $htmlOutputString .= "</select>";
	    
	    
	    //We need to initialize the value of the element whos id is passed.
	    //$htmlOutputString .= "<script language='javascript'> 'dc_combine_date_parts($elementID);' </script>";
	    $htmlOutputString .= "<script language='javascript'> setTimeout('dc_combine_date_parts(\"$elementID\", true)', 5); </script>";
	    return $htmlOutputString;
    }

?>    

