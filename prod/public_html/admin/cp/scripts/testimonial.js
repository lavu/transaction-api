///////////////
// Java script used for lavu_testimonial.php
//
//
//
//
/////////////
$(document).ready(function(){
// listens for new attachment and then does stuff

/*
* This checks the number of current entries and if we already have the max amount, we
* will display a message letting client know immediately before getting hopes up. 
*
* !! PROBABLY TAKE OFF THE DISPLAY FOR THIS ON BACKEND AS SOON AS WE KNOW.. BUT THIS !!
* !! CAN ACT AS BACKUP IN MEAN TIME.                                                 !!
*/
media_count();
	

/*
* This is a listener for the file upload button. 
* When media is uploaded, it checks for correctness before proceeding with upload.
*  
*/
$('#upload_media').change(function(){
	var file = this.files[0];
	var attach_name = file.name;
	var attach_size = file.size;
	var attach_type = file.type;
	console.log('-------file info:: name> '+attach_name+', size> '+attach_size+', type> '+attach_type);
	
	// file_check() will handle displaying error message on bad file size or type. 
	var file_good_boolean = file_check(file);
	
	
		if(file_good_boolean){
			var name = $('#name').val();
			var business_name = $('#business_name').val();
			var to_send = {name:name, business:business_name};
			/*var success = check_db(to_send);
			
			if(success){
				//display a successful upload dialog.
				console.log("sent file successfully");
				dialog_message("Thank you for your submission. We are excited to see your creation!", true);
			} else{
				//display a error in upload dialog.
				console.log("error in uploading file.. Max number of submissions? ");
				dialog_message("Already received maximum number of submissions:(", false);				
			}*/
			
		}
	
	
});


// this is the container for the message the person will receive when attempting to upload media
$("#dialog_message").dialog({
		autoOpen: false,
		height: 300,
		width: 500,
		resizable: false,
		buttons:{
			Ok: function(){
				$(this).dialog("close");
				//$("dialog_message").style("display","none");
			}
		}
	});

});//close document.ready()

/*checks:
file size,
previous submission (or possibly override previous)?
number of submissions in bank. 
...
*/
function file_check(file){

	var filename = file.name;
	//valid extensions accepted
	var extensions = ['jpg','jpeg','mov', 'png', 'mpeg', 'wmv', 'avi'];
	var ext = filename.split('.');
	ext = ext.reverse();
	
	// sends error message if file type is not supported. 
	if($.inArray(ext[0].toLowerCase(), extensions) < 0){
		console.log("file extention:: "+ext[0]+" not an allowable file type.");
		dialog_message("Not an allowable filetype. Supported types are:: .jpg, .jpeg, .png, .mpeg4",false);
		return false;
	} 
	//check file size
	if(file.size > 100000000){ //828 900 87 (83MB) was test .mov video of 1:02
		console.log("file size error");
		dialog_message("File size exceeded. Largest allowed size is... " , false);		
		return false;
	}
	
	return true;
}

/***
*html to display in the dialog. can be used for 
* fail, or pass message. 
*
***/
function dialog_message(message, success_boolean){
	
	$("#message").text(message);
	
	if(success_boolean){ 	
		//change message to success message in code
		$("#dialog_message").dialog('option',"title", "Succesfully uploaded media!");		
	}else{
		//change message to fail message in code
		$("#dialog_message").dialog('option',"title", "Cannot upload media");
			
	}
	console.log("display dialog");
	$("#dialog_message").dialog("open");
	$('#dialog_message').css('display', 'block');
	// display dialog with message
	
	return true;
}


// return false if too many files in db or error sending.
/*function check_db(bname,fname){

	$.post('./lavu_testimonial.php',{
		duplicate: "true",
		data_name:"test",
		restaurant:bname,
		media_name:fname},
		function(data){
			console.log("previous entry? "+data.previous+" entry count? "+data.full);
			if(data.previous){
				//display some kind of "will override previous entry" message
				dialog_message("It seems your restaurant name is already associated with a video. Any further submission will override previous.", true);
			}
		}, "json");
	
	return true;
}*/


/**
*Need to change link when moving over to live
**/
function media_count(){
	var limit = 20;
	var info = {mediaCount: "mediaCount"};
	var maxed_out = false;
	console.log("media count");
	
	var m = $.post('./testimonial_processing.php',info,
	function(data){
	    console.log(JSON.stringify(data));
		console.log("mediaCount:: "+ data.media_count);
		
		if(data.media_count >= limit){
		console.log("will return true");
			maxed_out_message();
		}
	}, "json");
	
	return maxed_out;
}


/**
* Changes the site to a we are full message instead of normal page
*
**/
function maxed_out_message(){
	var max_submissions = 'It seems we have reached the max number of submissions. We Apologize. Stay tuned for more info!';
	console.log(max_submissions);
	$("#max_reached").text(max_submissions);
	$("#whole_page").css('display', 'none');	
	
}