(function()
{
	function AJAXRequest(url, requestType, params, async)
	{
		if (arguments.length === 0)
		{
			return;
		}

		if (!requestType)
		{
			requestType = "";
		}

		if (!async)
		{
			async = false;
		}
		else
		{
			async = true;
		}

		switch (requestType.toUpperCase().trim())
		{
			case "POST":
			case "PATCH":
			case "PUT":

				break;

			case "GET":
			default :

				requestType = "GET";
				break;
		}

		if (!params)
		{
			params = "";
		}
		else if (params instanceof Object)
		{
			obj = params;
			arr = [];
			for (var k in obj)
			{
				arr.push(encodeURIComponent(k) + "=" + encodeURIComponent(obj[k]));
			}

			params = arr.join("&");
		}
		else if ("string" != typeof params)
		{
			params = '';
		}

		this._request = new XMLHttpRequest();
		this._request.open(requestType, url, async);
		this._request.addEventListener("readystatechange", this, false);
		if (requestType == "POST")
		{
			this._request.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
		}

		this._request.send(params);
	}

	function handleEvent(event)
	{
		if (event.type != "readystatechange")
		{
			return;
		}

		if (this._request.readyState === 4)
		{
			this._request.removeEventListener("readystatechange", this, false);
			if (this._request.status === 200)
			{
				this.success(this._request.responseText);
			}
			else
			{
				this.failure(this._request.status, this._request.responseText);
			}
		}
	}

	function success(responseText)
	{
		alert(responseText);
	}

	function failure(status, responseText)
	{
		alert(status);
		alert(responseText);
	}

	window.AJAXRequest = AJAXRequest;
	AJAXRequest.prototype = {};
	AJAXRequest.prototype.constructor = AJAXRequest;
	AJAXRequest.prototype.handleEvent = handleEvent;
	AJAXRequest.prototype.success = success;
	AJAXRequest.prototype.failure = failure;
})();
