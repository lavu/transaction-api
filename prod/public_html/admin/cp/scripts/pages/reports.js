	function showSignature(dataname, loc_id, order_id, check, transaction_id, amount, tip, authcode) {
	
		var UserWidth = 0, UserHeight = 0;
		if( typeof( parent.window.innerWidth ) == 'number' ) {
			//Non-IE
			UserWidth = parent.window.innerWidth;
			UserHeight = parent.window.innerHeight;
		} else if( parent.document.documentElement && ( parent.document.documentElement.clientWidth || parent.document.documentElement.clientHeight ) ) {
			//IE 6+ in 'standards compliant mode'
			UserWidth = parent.document.documentElement.clientWidth;
			UserHeight = parent.document.documentElement.clientHeight;
		} else if( parent.document.body && ( parent.document.body.clientWidth || parent.document.body.clientHeight ) ) {
			//IE 4 compatible
			UserWidth = parent.document.body.clientWidth;
			UserHeight = parent.document.body.clientHeight;
		}

		var ScrollTop = document.body.scrollTop;
		if (ScrollTop == 0) {
			if (window.pageYOffset) {
				ScrollTop = window.pageYOffset;
			} else {
				ScrollTop = (document.body.parentElement) ? document.body.parentElement.scrollTop : 0;
			}
		}

		document.getElementById("signature_transaction_info").value = "Order #" + order_id + "  Check #" + check + "   Amount: " + amount + "  Tip: " + tip;
		if (authcode == "") {
			document.getElementById("signature_image").src = "/images/" + dataname + "/signatures/" + loc_id + "-" + order_id + "-" + check + "-" + transaction_id + ".jpg";
		} else {
			document.getElementById("signature_image").src = "/images/" + dataname + "/signatures/" + loc_id + "-" + order_id + "-" + check + "-" + transaction_id + "-" + authcode + ".jpg";
		}
		//document.getElementById("customer_signature").style.left = ((UserWidth / 2) - 262) + "px";
		//document.getElementById("customer_signature").style.top = (ScrollTop + 200) + "px";
		document.getElementById("customer_signature").style.display = 'inline';
	}
	
	function hideSignature() {
		document.getElementById("customer_signature").style.display = 'none';
	}
	
	function getX(oElement) {
		var iReturnValue = 0;
		while(oElement != null) {
			iReturnValue += oElement.offsetLeft;
			oElement = oElement.offsetParent;
		}
		return iReturnValue;
	}

	function getY(oElement) {
		var iReturnValue = 0;
		while(oElement != null) {
			iReturnValue += oElement.offsetTop;
			oElement = oElement.offsetParent;
		}
		return iReturnValue;
	}
	/*function record_click(id){
		console.log(id);
	}*/
	function showAltPayments(id, sender) {
		document.getElementById(id).style.left = (getX(sender) + sender.offsetWidth + 10) + "px"; 
		document.getElementById(id).style.top = (getY(sender) - 40) + "px";
		document.getElementById(id).style.display = 'inline';
	}
	
	function hideAltPayments(id) {
		document.getElementById(id).style.display = 'none';
	}
	