function openExtensionPage(link){

	$("#extendPage").fadeOut('slow', function(){
		$("#extensionBackend").fadeIn('slow', function(){
			$("#extendBackend").attr('src',link);
			$("#extendBackend").css("height", '1245');
		
			$("#content").css("height", '1245');
		});
		
	});
	
}
function showExtensions(){
	$('#extensionBackend').fadeOut('slow', function(){
		$('#extendPage').fadeIn('slow', function(){
			
		});
	});
}


function showPermissionsRequest(){
	$("#grayBackground").css("display","block");
	$("#permissionsRequest").css("display","block");
}

function hidePermissionsRequest(){
	$("#grayBackground").css("display","none");
	$("#permissionsRequest").css("display","none");
}

function approvePermissionsRequest(answer, link, loc_id, dataname){
	if(answer=='deny')
		hidePermissionsRequest();
	else{
		var result=saveExtensionInfo(answer, loc_id, dataname, link);
		hidePermissionsRequest();
	}
}
function setPermissions(permissionsNeeded, id, link, loc_id,dataname){
	var list="<ul class='permissionsUL'>";
	
	for(var perm in permissionsNeeded){
		if(permissionsNeeded.hasOwnProperty(perm)){
			
			list+="<li class='permissionsLI'>"+perm+"</li>";
			list+="<ul class='permissionsUL'>";
			
			for(var item in permissionsNeeded[perm]){
				if(permissionsNeeded[perm].hasOwnProperty(item))
					list+="<li class='permissionsLI'>" +permissionsNeeded[perm][item]+ "</li>";
			}
			list+='</ul>';
		}
	}
	list+='</ul>';
	$("#approveOrDeny").html("	Do you accept these terms? <br> <input type='button' class='permissionsButton' id='approve' value='approve' onclick='approvePermissionsRequest(\""+id+"\", \""+link+"\",\""+loc_id+"\",\""+dataname+"\" )'> <input class='permissionsButton' type='button' value='deny' onclick='approvePermissionsRequest( $(this).val() )'>");
	$('#informationRequests').html(list);
}

function linkClicked(permissionsNeeded, link, id, loc_id, dataname){
		
		setPermissions(permissionsNeeded, id, link, loc_id, dataname);
		showPermissionsRequest();

}
function saveExtensionInfo(id, loc_id, dataname,link){
	var urlString= "id="+id+"&loc_id="+loc_id+"&dataname="+dataname;
	
	$.ajax({
	
		url: "/cp/objects/dbFunctions/saveExtensionData.php",
		type: "POST",
		data:urlString,
		async:false,
		success: function (string){
		
			if(string=='success'){
				openExtensionPage(link);
				return true;
			}else{
				return false;
			}
		}
	});	
}
