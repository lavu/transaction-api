(function(){
	function div( classes ){
		var result = document.createElement('div');
		if( classes !== undefined ){
			if( classes instanceof Array ){
				for( var i = 0; i < classes.length; i++ ){
					result.classList.add( classes[i] );
				}
			} else if( typeof classes == 'string' ) {
				result.setAttribute( 'class', classes );
			}
		}
		return result;
	}
	window.Lavu = (window.Lavu===void 0)?{}:window.Lavu;
	Lavu.elements = (Lavu.elements===void 0)?{}:Lavu.elements;

	(function(){
		function ValidationHandler(){}
		function allowKeyCode( keyCode ) { return true; }
		function allowCharacter( charCode ){ return true; }
		function isValidInput( input ){ return input !== ''; }
		function getErrorMessageForInput( input ){ if( input === '' ) return "This field cannot be empty!"; }

		Lavu.elements.ValidationHandler = ValidationHandler; //External Binding
		ValidationHandler.prototype = Object.create( Object.prototype );

		ValidationHandler.prototype.constructor = ValidationHandler;
		ValidationHandler.prototype.allowKeyCode = allowKeyCode;
		ValidationHandler.prototype.allowCharacter = allowCharacter;
		ValidationHandler.prototype.isValidInput = isValidInput;
		ValidationHandler.prototype.getErrorMessageForInput = getErrorMessageForInput;

		ValidationHandler.defaultHandler = new ValidationHandler();
	})();

	var lns = {};
	(function(){
		function LavuTextInput( type ){
			// PlaceholderLabel
			// Outline
			// input container
			// Error Area
			// External Validation Handler
			if( type === undefined ){
				type = 'text';
			}
			this._place_holder_label = div( 'place_holder_label' );
			this._outline_area = div( 'outline_area' );
			this._error_area = div( 'error_area' );
			this._input_container = div( 'input_container' );

			this._ele = this;
			this._ele.appendChild( this._outline_area );
			this._ele.appendChild( this._place_holder_label );
			this._ele.appendChild( this._input_container );
			this._ele.appendChild( this._error_area );

			this._input = document.createElement('input');
			this._input.setAttribute('type', type);

			if( this.hasAttribute('name') ){
				this._input.setAttribute('name', this.getAttribute('name') );
				this._input.setAttribute('autocomplete', 'on' );
				this.removeAttribute('name');
			}

			this._input_container.appendChild( this._input );
			Object.defineProperty( this._ele, 'value', {
				get: function() {
					return this._input.value;
				},
				set: function(newValue) {
					this._input.value = newValue;
					this.validate();
				},
				configurable: true
			  } );

			this._validation_handler = Lavu.elements.ValidationHandler.defaultHandler;
		}

		function setPlaceHolderText( text ){
			while( this._place_holder_label.childNodes.length ){
				this._place_holder_label.removeChild( this._place_holder_label.childNodes[0] );
			} 
			this._place_holder_label.appendChild( document.createTextNode( text ) );
		}

		function createdCallback(){
			LavuTextInput.call( this );
		}

		function handleEvent( event ) {
			switch( event.type ){
				case 'focus':
					this.setAttribute('active', '');
					break;
				case 'blur':
					this.removeAttribute('active');
					break;
				case 'keypress':
					if( !this._validation_handler.allowCharacter( event.charCode ? event.charCode : event.keyCode ) ) {
						event.preventDefault()
					}
					break;
				case 'keydown':
					if( !this._validation_handler.allowKeyCode( event.keyCode ) ) {
						event.preventDefault()
					}
					break;	
				case 'input':
					this.validate();
					break;
			}
		}

		function attachedCallback(){
			this._input.addEventListener( 'focus', this, false );
			this._input.addEventListener( 'blur', this, false );
			// this._input.addEventListener( 'keydown', this, false );
			this._input.addEventListener( 'keypress', this, false );
			this._input.addEventListener( 'input', this, false );
			for( var i = 0; i < this.attributes.length; i++ ){
				var attr = this.attributes.item(i);
				this.attributeChangedCallback( attr.name, undefined, attr.value );
			}
		}

		function detachedCallback(){
			this._input.removeEventListener( 'focus', this, false );
			this._input.removeEventListener( 'blur', this, false );
			// this._input.removeEventListener( 'keydown', this, false );
			this._input.removeEventListener( 'keypress', this, false );
			this._input.removeEventListener( 'input', this, false );
		}
		function attributeChangedCallback( attributeName, oldValue, newValue ) {
			switch( attributeName ){
				case 'placeholder':
					this.setPlaceHolderText( newValue );
					break;
				case 'value':
					this._input.value = newValue;
					break;
				case 'disabled':
					this._input.setAttribute(attributeName,newValue);
					if( newValue === null ){
						this._input.removeAttribute(attributeName);
					}
					break;
			}
		}

		function validate(){
			var is_valid = this._validation_handler.isValidInput( this.value );
			this._error_area.innerText = is_valid ? '' : this._validation_handler.getErrorMessageForInput( this.value );
			if( this._validation_handler.isValidInput( this.value ) ){
				this._ele.removeAttribute('error');
			} else {
				this._ele.setAttribute('error','');
			}
			if( this._input.value != '' ){
				this._ele.setAttribute('hasvalue','');
			} else {
				this._ele.removeAttribute('hasvalue');
			}

			return is_valid;
		}

		LavuTextInput.prototype = Object.create( CustomElement.prototype );
		LavuTextInput.prototype.constructor = LavuTextInput;

		LavuTextInput.prototype.setPlaceHolderText = setPlaceHolderText;
		LavuTextInput.prototype.createdCallback = createdCallback;
		LavuTextInput.prototype.attachedCallback = attachedCallback;
		LavuTextInput.prototype.detachedCallback = detachedCallback;
		LavuTextInput.prototype.attributeChangedCallback = attributeChangedCallback;
		LavuTextInput.prototype.validate = validate;

		LavuTextInput.prototype.handleEvent = handleEvent;

		lns.LavuTextInput = LavuTextInput;

		document.registerElement( 'lavu-text-input', LavuTextInput );
	})();

	(function(){
		function LavuPasswordInput(){
			lns.LavuTextInput.call( this, 'password' );
		}

		function createdCallback(){
			LavuPasswordInput.call( this );
		}

		LavuPasswordInput.prototype = Object.create( lns.LavuTextInput.prototype );
		LavuPasswordInput.prototype.constructor = LavuPasswordInput;

		LavuPasswordInput.prototype.createdCallback = createdCallback;

		document.registerElement( 'lavu-password-input', LavuPasswordInput );
	})();

	(function(){
		function NumberValidationHandler(){
			var fmt = new Number( 123456.789 ).toLocaleString();
			fmt = fmt.replace(/\d/g, '0' );

			var matches = fmt.match( /((?:0)+([^0]+))?((?:0)+([^0]+))?((?:0)+)([^0]+)((?:0)+)/ );

			// [0] full match
			// [1] fourth iteration, usually undefined/empty (ignored)
			// [2] fourth iteration separating character (ignored?)
			// [3] third section, repeatable IIF this is present, could be undefined if no grouping character present
			// [4] third iteration seperating character
			// [5] pre decimal without grouping, should be guaranteed to exist
			// [6] decimal character
			// [7] post decimal, should just be 3 numbers
			
			// fourth group, IIF optional, repeated: *
			// third group, IIF optional, repeated IIF no fourth group: * | ?
			// second group, required, repeated IIF no third group: * | +
			// first group, optional, repeated beyond formatting: *
			
			// (\d{1,2}\,)*\d{3}
			// \d{1,3}
			// (\.\d+)?
			
			this._decimalChar = matches[6];
			this._groupChar = '';
			var preGroupLength = matches[5].length;

			var repGroup = '';
			var preGroup = '\\d{1,' + preGroupLength+ '}';
			var postDecimal = '(\\'+this._decimalChar+'\\d+)?';

			var groupLength = 0;

			if( matches[1] === undefined ) {
				// No Group in Pattern
				this._groupChar = '';
				preGroups = '\\d+';

			} else if( matches[3] === undefined ) { 
				// Single Group in Pattern
				this._groupChar = matches[2];	

				groupLength = (matches[1].length - matches[2].length);
			} else {
				// We have Two Groups in Pattern
				groupLength = (matches[3].length - matches[4].length);
			}

			if( groupLength > 0 ){
				repGroup = '(\\d{1,'+groupLength+'}\\'+this._groupChar+')*\\d{'+groupLength+'}|';
			}

			this._regex = new RegExp();
			this._regex.compile( '^(?:' + repGroup + preGroup + ')' + postDecimal + '$' );

			this._pgl = preGroupLength;
			this._gl = groupLength;
		}

		function allowCharacter( charCode ){
			var str = String.fromCharCode( charCode );
			return /\d/.test( str ) || str == this._decimalChar || str == this._groupChar;
		}

		function isValidInput( input ){
			return this._regex.test( input );
		}

		function getErrorMessageForInput( input ){
			if( !this.isValidInput( input ) ){
				return 'Expected something of the form ' + ( (this._gl>0?(new Array(this._gl+1).join('#') + this._groupChar):'') + new Array(this._pgl).join('#') + '0' + this._decimalChar + '#' );
			} else {
				return '';
			}
		}

		NumberValidationHandler.prototype = Object.create( Lavu.elements.ValidationHandler.prototype );
		NumberValidationHandler.prototype.constructor = NumberValidationHandler;

		NumberValidationHandler.prototype.allowCharacter = allowCharacter;
		NumberValidationHandler.prototype.isValidInput = isValidInput;
		NumberValidationHandler.prototype.getErrorMessageForInput = getErrorMessageForInput;

		Lavu.elements.NumberValidationHandler = NumberValidationHandler;
	})();

	(function(){
		function LavuPercentageInput(){
			lns.LavuTextInput.call( this );
			this._validation_handler = new Lavu.elements.NumberValidationHandler();
		}

		function createdCallback(){
			LavuPercentageInput.call( this );
		}

		LavuPercentageInput.prototype = Object.create( lns.LavuTextInput.prototype );
		LavuPercentageInput.prototype.constructor = LavuPercentageInput;

		LavuPercentageInput.prototype.createdCallback = createdCallback;

		document.registerElement( 'lavu-percentage-input', LavuPercentageInput );
	})();

	(function(){
		function NameValidationHandler(){}

		function allowCharacter( charCode ){
			var str = String.fromCharCode( charCode );
			return /[\w\ \.-]/.test( str ) || str == this._decimalChar || str == this._groupChar;
		}

		function isValidInput( input ){
			return /[\w\ \.-]+/.test( input );
		}

		function getErrorMessageForInput( input ){
			if( !this.isValidInput( input ) ){
				return 'Unexpected Character for a Name';
			} else {
				return '';
			}
		}

		NameValidationHandler.prototype = Object.create( Lavu.elements.ValidationHandler.prototype );
		NameValidationHandler.prototype.constructor = NameValidationHandler;

		NameValidationHandler.prototype.allowCharacter = allowCharacter;
		NameValidationHandler.prototype.isValidInput = isValidInput;
		NameValidationHandler.prototype.getErrorMessageForInput = getErrorMessageForInput;

		Lavu.elements.NameValidationHandler = NameValidationHandler;
	})();

	(function(){
		function LavuNameInput(){
			lns.LavuTextInput.call( this );
			this._validation_handler = new Lavu.elements.NameValidationHandler();
		}

		function createdCallback(){
			LavuNameInput.call( this );
		}

		LavuNameInput.prototype = Object.create( lns.LavuTextInput.prototype );
		LavuNameInput.prototype.constructor = LavuNameInput;

		LavuNameInput.prototype.createdCallback = createdCallback;

		document.registerElement( 'lavu-name-input', LavuNameInput );
	})();

	(function(){
		function EmailValidationHandler(){}

		function allowCharacter( charCode ){
			var str = String.fromCharCode( charCode );
			return /[+\w@\._-]/.test( str ) || str == this._decimalChar || str == this._groupChar;
		}

		function isValidInput( input ){
			return /[+\w\._-]+@[\w-\._]+\.\w+/.test( input );
		}

		function getErrorMessageForInput( input ){
			if( !this.isValidInput( input ) ){
				return 'Expecting something of the form me@example.com';
			} else {
				return '';
			}
		}

		EmailValidationHandler.prototype = Object.create( Lavu.elements.ValidationHandler.prototype );
		EmailValidationHandler.prototype.constructor = EmailValidationHandler;

		EmailValidationHandler.prototype.allowCharacter = allowCharacter;
		EmailValidationHandler.prototype.isValidInput = isValidInput;
		EmailValidationHandler.prototype.getErrorMessageForInput = getErrorMessageForInput;

		Lavu.elements.EmailValidationHandler = EmailValidationHandler;
	})();

	(function(){
		function LavuEmailInput(){
			lns.LavuTextInput.call( this );
			this._validation_handler = new Lavu.elements.EmailValidationHandler();
		}

		function createdCallback(){
			LavuEmailInput.call( this );
		}

		LavuEmailInput.prototype = Object.create( lns.LavuTextInput.prototype );
		LavuEmailInput.prototype.constructor = LavuEmailInput;

		LavuEmailInput.prototype.createdCallback = createdCallback;

		document.registerElement( 'lavu-email-input', LavuEmailInput );
	})();

	(function(){
		function PhoneValidationHandler(){}

		function allowCharacter( charCode ){
			var str = String.fromCharCode( charCode );
			return /[\(\)\ \-\d]/.test( str ) || str == this._decimalChar || str == this._groupChar;
		}

		function isValidInput( input ){
			return /(?:\((\d{3})\)|(\d{3}))\ ?(\d{3})\ ?-?\ ?(\d{4})/.test( input );
		}

		function getErrorMessageForInput( input ){
			if( !this.isValidInput( input ) ){
				return 'Expecting something of the (###) ### - ####';
			} else {
				return '';
			}
		}

		PhoneValidationHandler.prototype = Object.create( Lavu.elements.ValidationHandler.prototype );
		PhoneValidationHandler.prototype.constructor = PhoneValidationHandler;

		PhoneValidationHandler.prototype.allowCharacter = allowCharacter;
		PhoneValidationHandler.prototype.isValidInput = isValidInput;
		PhoneValidationHandler.prototype.getErrorMessageForInput = getErrorMessageForInput;

		Lavu.elements.PhoneValidationHandler = PhoneValidationHandler;
	})();

	(function(){
		function LavuPhoneInput(){
			lns.LavuTextInput.call( this );
			this._validation_handler = new Lavu.elements.PhoneValidationHandler();
		}

		function createdCallback(){
			LavuPhoneInput.call( this );
		}

		LavuPhoneInput.prototype = Object.create( lns.LavuTextInput.prototype );
		LavuPhoneInput.prototype.constructor = LavuPhoneInput;

		LavuPhoneInput.prototype.createdCallback = createdCallback;

		document.registerElement( 'lavu-phone-input', LavuPhoneInput );
	})();

	(function(){
		function LavuSelectInput(){
			this._place_holder_label = div( 'place_holder_label' );

			this._outline_area = div( 'outline_area' );
			this._error_area = div( 'error_area' );
			this._input_container = div( 'input_container icon-down' );

			this._input = document.createElement('select');
			this._input.appendChild( document.createElement('option') ); // default (no option)

			for( var i = 0; i < this.children.length; i++ ){
				if( this.children[i].tagName != 'OPTION' ) {
					continue;
				}

				this._input.appendChild( this.children[i] );
				i--;
			}

			Object.defineProperty( this, 'value', {
				get: function() { return this._input.value; },
				set: function(newValue) { this._input.value = newValue; },
				configurable: true
			  } );

			this.appendChild( this._outline_area );
			this.appendChild( this._place_holder_label );
			this.appendChild( this._input_container );
			this.appendChild( this._error_area );

			this._input_container.appendChild( this._input );
		}

		function setPlaceHolderText( text ){
			while( this._place_holder_label.childNodes.length ){
				this._place_holder_label.removeChild( this._place_holder_label.childNodes[0] );
			} 
			this._place_holder_label.appendChild( document.createTextNode( text ) );
		}

		function addOption( option ) {
			this._input.appendChild( option );
		}

		function createdCallback(){
			LavuSelectInput.call( this );
		}
		function attachedCallback(){
			this._input.addEventListener( 'change', this, false );
			for( var i = 0; i < this.attributes.length; i++ ){
				var attr = this.attributes.item(i);
				this.attributeChangedCallback( attr.name, undefined, attr.value );
			}
		}
		function detachedCallback(){
			this._input.removeEventListener( 'change', this, false );
		}
		function attributeChangedCallback( attributeName, oldValue, newValue ) {
			switch( attributeName ){
				case 'placeholder':
					this.setPlaceHolderText( newValue );
					break;
				case 'value':
					this._input.value = newValue;
					break;
			}
		}

		function handleEvent( event ) {
			switch( event.type ){
				case 'change':
					if( this._input.selectedIndex !== 0 ){
						this.setAttribute('hasvalue','');
					} else {
						this.removeAttribute('hasvalue');
					}
			}
		}

		function addEventListener() {
			this._input.addEventListener.apply( this, arguments );
		}

		function removeEventListener() {
			this._input.removeEventListener.apply( this, arguments );	
		}

		LavuSelectInput.prototype = Object.create( CustomElement.prototype );
		LavuSelectInput.prototype.constructor = LavuSelectInput;

		LavuSelectInput.prototype.createdCallback = createdCallback;
		LavuSelectInput.prototype.attachedCallback = attachedCallback;
		LavuSelectInput.prototype.detachedCallback = detachedCallback;
		LavuSelectInput.prototype.attributeChangedCallback = attributeChangedCallback;
		LavuSelectInput.prototype.addOption = addOption;

		LavuSelectInput.prototype.setPlaceHolderText = setPlaceHolderText;
		LavuSelectInput.prototype.handleEvent = handleEvent;
		LavuSelectInput.prototype.addEventListener = addEventListener;
		LavuSelectInput.prototype.removeEventListener = removeEventListener;

		document.registerElement( 'lavu-select-input', LavuSelectInput );
	})();
})();