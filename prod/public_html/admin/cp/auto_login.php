<?php


global $o_autoLogin;
$o_autoLogin = new autoLoginType();

if (isset($_POST["check_auto_login"])) {
	$o_autoLogin->autoLogin();
}

class autoLoginType {

	function __construct() {
		$this->dbapi = ConnectionHub::getConn('poslavu')->getDBAPI();
	}

	/**
	 * Attempts to auto login the user.
	 * If it fails, it echos out an error message.
	 */
	public function autoLogin() {

		// connect to the database
		require_once(dirname(__FILE__)."/resources/core_functions.php");

		// try to log in
		$a_allowed = $this->checkAutoLoginCredentials();
		if ($a_allowed["success"]) {
			$login_forward_to = (isset($_POST["login_forward_to"])) ? $_POST["login_forward_to"] : "";
			$this->logInAsUser($a_allowed["dataname"], $a_allowed["user"], $login_forward_to);
		} else {
			echo $a_allowed["error"];
			return;
		}
	}

	/**
	 * Checks if a user can auto-login with the given $_POST credentials
	 * @return array array("success"=>boolean, "error"=>message, ["dataname"=>dataname, "userid"=>userid])
	 */
	public function checkAutoLoginCredentials() {

		// check for ssl connection
		if ($_SERVER["HTTPS"] == "") {
			return array("success"=>FALSE, "error"=>"Invalid Source");
		}

		// check that the data is set
		if (!isset($_POST["dn"]) || !isset($_POST["user"]) || !isset($_POST["key"])) {
			return array("success"=>FALSE, "error"=>"Missing Data");
		}

		// verify the key
		$a_verification = $this->verifyCredentials($_POST["dn"], $_POST["user"], $_POST["key"]);
		if ($a_verification["success"]) {
			return array("success"=>TRUE, "error"=>"", "dataname"=>$_POST["dn"], "user"=>$_POST["user"]);
		} else {
			return $a_verification;
		}
	}

	/**
	 * Verifies that a user can log in, and that the credentials are correct
	 * @param  string  $s_dataname Dataname of the user's account
	 * @param  integer $i_userid   ID of the user
	 * @param  string  $s_key      Key credentials
	 * @return array               array("success"=>boolean, "error"=>message)
	 */
	public function verifyCredentials($s_dataname, $i_userid, $s_key) {
		$a_credentials = $this->getCredentials($s_dataname, $i_userid);
		if ($a_credentials["key"] != $s_key) {
			return array("success"=>FALSE, "error"=>"Bad Credentials");
		} else if (!$this->getUserCanAutoLogin($s_dataname, $i_userid)) {
			return array("success"=>FALSE, "error"=>"<script language='javascript'>window.location.replace('');</script>");
		}
		return array("success"=>TRUE, "error"=>"");
	}

	/**
	 * Creates the credentials necessary to auto-login
	 * @param  string  $s_dataname The dataname of the account
	 * @param  integer $i_userid   ID of the user that wants to use auto-login
	 * @return array               array("dn"=>dataname, "user"=>user id, "key"=>md5 hash)
	 */
	public function getCredentials($s_dataname, $i_userid) {
		$key = md5($s_dataname + ($i_userid*1337) + "yourmamma");
		return array("dn"=>$s_dataname, "user"=>$i_userid, "key"=>$key);
	}

	/**
	 * Check if a user an use the auto-login feature
	 * @param  string  $s_dataname The dataname of the user to check
	 * @param  integer $i_userid   The id of the user to check
	 * @return boolean             TRUE if the user exists and can auto-login, FALSE otherwise
	 */
	public function getUserCanAutoLogin($s_dataname, $i_userid) {
		$i_userid = (int)$i_userid;
		$a_disalowed = $this->getDisallowedUsers($s_dataname);

		// check if the account exists
		lavu_connect_dn($s_dataname,'poslavu_'.$s_dataname.'_db');
		if (count($a_disalowed) == 0) {
			$query = lavu_query("SELECT * FROM `poslavu_[dataname]_db`.`config` LIMIT 1", array("dataname"=>$s_dataname));
			if ($query === FALSE) {
				return FALSE;
			}
		} else {
			$query = lavu_query("SELECT * FROM `poslavu_[dataname]_db`.`users` WHERE `id`='[userid]'", array("dataname"=>$s_dataname, "userid"=>$i_userid));
			if ($query === FALSE) {
				return FALSE;
			}
		}

		// check if the user is allowed to log in
		if (!in_array($i_userid, $a_disalowed)) {
			return TRUE;
		} else {
			return FALSE;
		}
	}

	/**
	 * Sets the config value to allow a user to autologin, or not
	 * @param  string  $s_dataname Dataname of the user's account
	 * @param  integer $i_userid   The user's id
	 * @param  boolean $b_enable   TRUE to allow, FALSE to deny
	 * @return boolean             TRUE
	 */
	public function setUserCanAutoLogin($s_dataname, $i_userid, $b_enable) {
		$i_userid = (int)$i_userid;
		$a_disalowed = $this->getDisallowedUsers($s_dataname);
		$key = array_search($i_userid, $a_disalowed);
		if ($b_enable) {
			if ($key !== FALSE) {
				unset($a_disalowed[$key]);
				$a_wherevars = array("setting"=>"disallowed_users_auto_login", "type"=>"setting");
				$a_update_vars = array_merge(array("value"=>implode(",", $a_disalowed)), $a_wherevars);
				$this->dbapi->insertOrUpdate("poslavu_{$s_dataname}_db", "config", $a_update_vars, $a_wherevars);
			}
		} else {
			if ($key === FALSE) {
				$a_disalowed[] = $i_userid;
				$a_wherevars = array("setting"=>"disallowed_users_auto_login", "type"=>"setting");
				$a_update_vars = array_merge(array("value"=>implode(",", $a_disalowed)), $a_wherevars);
				$this->dbapi->insertOrUpdate("poslavu_{$s_dataname}_db", "config", $a_update_vars, $a_wherevars);
			}
		}
		return TRUE;
	}

	/**
	 * Log in to an account as a specific user
	 * @param  string  $s_dataname      The dataname to log in to
	 * @param  integer $i_userid        The user to log in as
	 * @param  string  login_forward_to The page to be forwarded to once logged in
	 * @return none                     N/A
	 */
	public function logInAsUser($s_dataname, $i_userid, $login_forward_to = "") {

		// set the first page to forward to once the user has logged in
		if ($login_forward_to !== "") {
			if (!session_id()) {
				session_start();
			}
			$_SESSION["login_forward_to"] = $login_forward_to;
			session_write_close();
		}

		// set the login key
		global $global_login_key;
		$a_credentials = $this->getCredentials($s_dataname, $i_userid);
		$global_login_key = $a_credentials["key"];
		$_POST["key"] = $global_login_key;

		// set the username
		$a_users = $this->dbapi->getAllInTable("users", array("id"=>$i_userid), TRUE, array("databasename"=>"poslavu_".ConnectionHub::getConn('poslavu')->escapeString($s_dataname)."_db"));
		$_POST["username"] = $a_users[0]["username"];

		// set necessary login variabls
		global $in_lavu;
		global $maindb;
		$in_lavu = TRUE;
		$maindb = "poslavu_MAIN_db";
		$_POST["password"] = "";
		function is_dev() {
			if(strpos($_SERVER['REQUEST_URI'],"v2/")!==false)
				return true;
			else
				return false;
		}

		// load the login page
		require(dirname(__FILE__)."/areas/login.php");
	}

	/***************************************************************************
	 *                   P R I V A T E   F U N C T I O N S                     *
	 **************************************************************************/

	/**
	 * Get a list of all users not allowed to use auto-login for a specific account
	 * @param  string        $s_dataname Dataname of the account
	 * @return weakly typed              An array of userids not allowed to use autologin, or FALSE if the account doesn't exist
	 */
	private function getDisallowedUsers($s_dataname) {
		$a_users_row = $this->dbapi->getAllInTable("config", array("setting"=>"disallowed_users_auto_login", "type"=>"setting"), TRUE, array("databasename"=>"poslavu_".ConnectionHub::getConn('poslavu')->escapeString($s_dataname)."_db"));
		$a_disalowed = explode(",", $a_users_row[0]["value"]);
		for($i = 0; $i < count($a_disalowed); $i++) {
			$a_disalowed[$i] = (int)$a_disalowed[$i];
		}
		return $a_disalowed;
	}
}

?>
