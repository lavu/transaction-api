<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN""http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

	<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">

			<meta http-equiv="X-UA-Compatible" content="IE=9">
			 <meta http-equiv="X-UA-Compatible" content="IE=edge" />
		<title>POSLavu Admin Control Panel</title>
		<!-- Google Tag Manager -->
		<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push(
		{'gtm.start': new Date().getTime(),event:'gtm.js'}
		);var f=d.getElementsByTagName(s)[0],
		j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
		'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
		})(window,document,'script','dataLayer','GTM-WH5DR2D');</script>
		<!-- End Google Tag Manager -->
		<script type="text/javascript" src="/cp/resources/gridster/assets/jquery-1.11.3.js"></script>
		<link rel="stylesheet" href="styles/ios-checkbox.css" type="text/css" media="screen" charset="utf-8" />
		<link rel="stylesheet" type="text/css" href="<?php echo Lavu\Helpers\getAssetPath("styles/styles.css"); ?>">
		<link rel="stylesheet" type="text/css" href="<?php echo Lavu\Helpers\getAssetPath("styles/info_window.css"); ?>">
		<link rel="stylesheet" type="text/css" href="<?php echo Lavu\Helpers\getAssetPath("styles/ieStyles.css"); ?>">
		<link rel="stylesheet" type="text/css" href="<?php echo Lavu\Helpers\getAssetPath("styles/multiaccordion/jquery-ui-1.8.16.greenModifiers.min.css"); ?>">
		<link rel="stylesheet" type="text/css" href="<?php echo Lavu\Helpers\getAssetPath("styles/multiaccordion/jquery-ui-1.8.16.grayMessages.min.css"); ?>">
		<script type="text/javascript" src="<?php echo Lavu\Helpers\getAssetPath("scripts/multiAccordion.js"); ?>"> </script>
		<link rel="stylesheet" type="text/css" href="<?php echo Lavu\Helpers\getAssetPath("styles/fontcustom/fontcustom.css"); ?>">
		<script type="text/javascrpt" src = "<?php echo Lavu\Helpers\getAssetPath("scripts/pages/ministries.js"); ?>"></script>
		<script type="text/javascript" src="<?php echo Lavu\Helpers\getAssetPath("scripts/customelements.js"); ?>"></script>
		<link rel="stylesheet" type="text/css" href="/cp/resources/gridster/assets/jquery.gridster.css">
		<link rel="stylesheet" type="text/css" href="/cp/resources/gridster/assets/demo.css">
		<script src="/cp/resources/gridster/assets/jquery.gridster.js" type="text/javascript" charset="utf-8"></script>

        <style>
		body {
			min-width: 1024px;
		}
			#h_mainTable{
				display: table;
				width:100%;
			}
			.h_content{
				background-color:#f3f3f4;
				height:100%;
				padding-bottom: 90px;
				text-align:left;
			}




			#h_header{
				position: relative;
				width:100%;
				top: 0px;
				min-width: 900px;
				background-color:#ffffff;
				padding-top:0px;
				padding-bottom:0px;
			}
			nav {
				width:100%;

			}
			nav ul {

				width:100%;
				padding: 0 0px;
				border-top-right-radius: 10px;
				border-top-left-radius: 10px;

				list-style: none;
				position: relative;
				display: inline-table;

				margin-bottom:0px;
				height:auto;
				margin: 0 auto;
				text-align: center;
			}
			nav ul ul {
				background: #ededed;
				background: rgba( 224, 224, 224, 1 );
				border-radius: 0px;
				border-top:solid 12px #313235;
				padding: 0;
				position: absolute;
				width: 100%;
				top: 100%;
				left:0px;
				height:40px;
				}
			.selectedLink{
				position:relative;
				height:20%;
				margin-bottom:0px;
				background-color:white;
			}
			.internalMenuItemSelected{
				padding: 10px;
				color:black;
				font-weight: bold;
				/*border-top:1px solid #aecd37;*/
				/*border-right:1px solid #aecd37;*/
				/*border-left:1px solid #aecd37;*/
				/*border-left:1px solid #a0bb33;*/
				padding-left: 30px;
				padding-right: 30px;
				background-color:#ffffff;
			}
			.internalMenuItem{
				/*border-right:1px solid #a0bb33;*/
				/*border-left:1px solid #a0bb33;*/
				padding-left: 30px;
				padding-right: 30px;
				background-color:#dedede;
				padding-top: 10px;
				padding-bottom: 10px;
			}
			@font-face {
			  font-family: 'din';
			  src: url(/cp/styles/fonts/din/din-webfont.eot);
			  src: url(/cp/styles/fonts/din/din-webfont.eot?#iefix) format('embedded-opentype'), url(/cp/styles/fonts/din/din-webfont.woff) format('woff'), url(/cp/styles/fonts/din/din-webfont.ttf) format('truetype'), url(/cp/styles/fonts/din/din-webfont.svg#dinregular) format('svg');
			  font-weight: normal;
			  font-style: normal; }

			@font-face {
			  font-family: 'din';
			  src: url(/cp/styles/fonts/din_bold/din_bold-webfont.eot);
			  src: url(/cp/styles/fonts/din_bold/din_bold-webfont.eot?#iefix) format('embedded-opentype'), url(/cp/styles/fonts/din_bold/din_bold-webfont.woff) format('woff'), url(/cp/styles/fonts/din_bold/din_bold-webfont.ttf) format('truetype'), url(/cp/styles/fonts/din_bold/din_bold-webfont.svg#dinbold) format('svg');
			  font-weight: bold;
			  font-style: normal; }

			@font-face {
			  font-family: 'din';
			  src: url(/cp/styles/fonts/din-light/din_light-webfont.eot);
			  src: url(/cp/styles/fonts/din-light/din_light-webfont.eot?#iefix) format('embedded-opentype'), url(/cp/styles/fonts/din-light/din_light-webfont.woff) format('woff'), url(/cp/styles/fonts/din-light/din_light-webfont.ttf) format('truetype'), url(/cp/styles/fonts/din-light/din_light-webfont.svg#din_lightregular) format('svg');
			  font-weight: lighter;
			  font-style: normal; }
		</style>

		<![CDATA[EXTRA_BEFORE_SCRIPTS]]>
	</head>
<body style="margin:0px; background-color:#ECEEEF">
	<!-- Google Tag Manager (noscript) -->
	<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-WH5DR2D"
	height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
	<!-- End Google Tag Manager (noscript) -->
	 <!-- sticky notification -->
	 <!-- sticky notification -->
	<div id='help_popup' style='position:absolute; left:0px; top:0px; z-index:200; display:none;'>
		<table cellspacing='0' cellpadding='0' style='border: 1px solid black;'>
			<tr>
				<td style='background:URL(images/popup_bg.png); swidth:724px; height:471px' align='left' valign='top'>
					<table cellspacing='0' cellpadding='0'>
						<tr><td colspan='3' height='22px'>&nbsp;</td></tr>
						<tr>
							<td width='21px'>&nbsp;</td>
							<td height='20px' align='right' style='background:URL(images/popup_bar.png);'><a onclick='hideHelp();' style='cursor:pointer; color:#EEEEEE;'><b>X</b></a> &nbsp; </td>
							<td width='25px'>&nbsp;</td>
						</tr>
						<tr>
							<td width='21px'>&nbsp;</td>
							<td align='left'><div id='iframe_scroller' style='width:679px; height:405px; overflow:hidden; margin:0px 0px 0px 0px; padding:0px 0px 0px 0px;'>
							<iframe id='help_iframe' width='675px' height='401px' frameborder='1' scrolling='yes' src='' style='width:675px; height:401px;'></iframe></div></td>
							<td width='25px'>&nbsp;</td>
						</tr>
					</table>
				</td>
			</tr>
		</table>
	</div>
	<!--<div id='aboveHeader'>
	</div>-->
		<!--<div id='underHeader'></div>-->


																																																<!-- Added 5/19/16 by EV per BT 6920. Adds unique class to left nav version of CP -->
	<div id='center' style='margin-bottom:0px; height:100%; position:absolute; left:0px; top:0px;' class="leftNav">
		<div id='lockout' style='position:absolute; top:150px;'></div>

		<div id='h_mainTable' style='height:100%'>
			<table width='100%' height='100%' cellspacing="0" cellpadding=0 >

				<!--<tr class='menu'>
					<td width='100%' style='background-color:#313235; padding-bottom:12px' colspan=3>
						[-menuContent]
					</td>
				</tr>-->
                <tr>
                	<td colspan='4' bgcolor='#f4f4f2'>

                        <table bgcolor='#ffffff' cellspacing=0 cellpadding=12 width='100%'>
                        		<tr><td width='100%' align='right'>
                                	<table>
                                    <tr>

                                        <td>
                                            [billingNotification]
                                        </td>
                                        <td>

                                            <div id="userName">
                                                [username]
                                            </div>
                                            <div id="separator">
                                                <img src="images/arrowIcon.jpg"/>
                                            </div>

                                            <div id="logStatus">
                                                <a style='text-decoration:none; color: #aecd37' href="index.php?logout=1" id="loggedIn">
                                                    LOGOUT
                                                    <img src="images/loggedIn.jpg" style='border:0;'class="logStatusIcon"/>
                                                </a>
                                            </div>
                                            <div id= "menuMyAccount">
                                                <a style='text-decoration:none; color: #aecd37' href='?mode=billing'>MY ACCOUNT</a>
                                            </div>

                                            <div id="navigationDropDown">
                                                [locations]
												<div id= "databasename">
													[databaseview]
												</div>
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                            </td></tr>
                        </table>

                  </td>
                </tr>

				 <tr>
                	<td colspan='4' align='left' style='height:50px' bgcolor='#2b2b2b' valign='middle'>
                     <table><tr><td valign='middle'>
	                    	<img src='/cp/images/lavulogo_171x25.png' style='margin-left:25px' height='20' />&nbsp;&nbsp;
                        </td>
							<td valign='middle' style='border-left:solid 2px #f4f4f4; font-family:Din,Arial; font-size:20px; color:#f4f4f4; font-weight:lighter'>
	                      &nbsp;&nbsp;CONTROL PANEL
                        </td></tr></table>
                  </td>
               </tr>
				<tr >
                	<td style='width:220px' width='220' bgcolor='#343434' valign='top'>

						<table cellspacing=0 cellpadding=0 style='width:220px'>
                        	<tr>
                            	<td style='border-bottom:solid 1px #555555; width:100%; color:#bdbdbd; font-family:Verdana,Arial; font-size:14px' align='center' valign='middle'>
                    				<br>[h_logo]<br><br>
                                <nobr>[h_restaurant_name]</nobr><br><br><br>
                             </td>
                         </tr>
                         <tr>
                         		<td>
                                [h_nav]
                             </td>
                         </tr>
                     </table>
                  </td>
					<td class='h_content' colspan=3 valign='top'>


	                    	<table><tr><td style='padding-top:10px; padding-left:30px'>

                            <div style="position:relative" id="main_content_area" >
                                [internalMenu]
                                [content]
                            </div>
                         </td></tr></table>
					</td>
				</tr>
			</table>
		</div>
	</div>
	<lavu-blackout hidden>
		<lavu-initial-setup-area></lavu-initial-setup-area>
	</lavu-blackout>
</body>
<script type="text/javascript" src="<?php echo Lavu\Helpers\getAssetPath("scripts/pages/reports.js"); ?>"></script>
<script type="text/javascript" src="<?php echo Lavu\Helpers\getAssetPath("scripts/pages/extensions.js"); ?>"></script>
<script src="scripts/jquery.cookie.js" type="text/javascript" charset="utf-8"></script>
<script src="scripts/jquery.howdydo-bar.js" type="text/javascript" charset="utf-8"></script>
<script src="scripts/iphone-style-checkboxes.js" type="text/javascript" charset="utf-8"></script>
<script type="text/javascript" src="<?php echo Lavu\Helpers\getAssetPath("scripts/index.js"); ?>"> </script>
<!--[if lte IE 8]><script language="javascript" type="text/javascript" src="scripts/jquery/flot/excanvas.min.js"></script><![endif]-->

<script language="javascript" type="text/javascript" src="scripts/jquery/flot/jquery.flot.js"></script>
<script language="javascript" type="text/javascript" src="scripts/jquery/flot/jquery.flot.pie.js"></script>
<script language="javascript" type="text/javascript" src="scripts/jquery/iframe_ipad.js"></script>

<script type="text/javascript" src="//assets.zendesk.com/external/zenbox/v2.6/zenbox.js"></script>
<style type="text/css" media="screen, projection">
  @import url(//assets.zendesk.com/external/zenbox/v2.6/zenbox.css);
</style>

<!--ADDED 6/1 by EV per LAVU-82
adds copyright footer to the left nav CP  -->
<script type="text/javascript">
 $("<div class='footer'><span class=\"copyright-footer\">© 'Lavu 2010-<span class=\"currYear\">2017</span> | All Rights Reserved | <a style='color:gray' href=\"https://www.lavu.com/terms-of-service.pdf\" target=\"_blank\">Terms of Service | Privacy Policy</a></span></div>").insertAfter("#main_content_area");
// footer date dynamicly display
$(".currYear").text(new Date().getFullYear());
</script>


<!-- Start of lavu Zendesk Widget script -->
<!--[if IE 8]>
    <script type="text/javascript">
	    $(document).ready( function(){
	    	$("nav ul").css("width", "96%");
	    });
    </script>
<![endif]-->
<!--[if IE 9]>
    <script type="text/javascript">
	    $(document).ready( function(){
	    	$("nav ul").css("width", "96%");
	    });
    </script>
<![endif]-->
</html>
