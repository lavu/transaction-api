

<style type="text/css">

.component{
	
	float:left;
	border:1px solid #666;
	width:130px;
	margin-right:5px;
	font-size:7pt;
	
	padding:5px 0 5px 0;
	
}

.popUp{
	width:30%;
	
	min-width:400px;
	height:auto;
	background-color:#FFF;
	border:1px solid #000;
	
	
}

.popUpLeftForm{
	float:left;
	width:100px;
	border: 0px solid #ccc;
	text-align:right;
	padding:0 5px 0 0;
}

.popUpRightForm{
	float:left;
	width:200px;
	border: 0px solid #ccc;
	text-align:left;
}

.popUpClearBoth{
	clear:both;
	height:15px;
	
}

.popUpLeftFormAlign{
	float:left;
	text-align:right;
	width:40px;
	border:0px solid #333;
	padding-left:2px;
}

.popUpRightFormAlign{
	float:left;
	text-align:left;
	width:40px;
	border:0px solid #333;
}

.popUpFormAlignClearBoth{
	clear:both;
	height:1px;
}

.clearBoth{
	clear:both;
	height:10px;
}

.info{
	display:none;
	border:0px solid #000;
	float:right;
}

</style>

<script type="text/javascript">



function showPopUp(input){
		document.getElementById("popUp").style.display = "block";
}//showPopUp()


function closePopUp(input){
		document.getElementById("popUp").style.display = "none";
}//showPopUp()

function addField(){
		
		displayThis = "";
		
		addThis = document.getElementById("addThis").value;
		
		if (addThis == "Text Field"){
				addTextField();
		}//if
		
		if (addThis == "Menu"){
				
				addMenu();
				return;
		}//if
		
		if (addThis == "Radio Group"){
				addRadio();
		}//if
		
		if (addThis == "Checkbox"){
				displayThis += addCheckbox();
		}//if
		
		//document.getElementById("showStuff").innerHTML = displayThis;
		
}//addField()

function addTextField(){
	//nothing needs to be done
	currentHTML = "";
	document.getElementById("showStuff").innerHTML = currentHTML;
	return;
}//addMenu()



function addCheckbox(){
	//nothing needs to be done
	currentHTML = "";
	document.getElementById("showStuff").innerHTML = currentHTML;
	return;
}//addMenu()

function addRadio(){
	
	document.getElementById("NumberOfFields").value = "1";
	
	currentHTML = "";
	
				currentHTML += "<div class=\"popUpLeftForm\">";
				currentHTML += "<label>Option Name<label>";
				currentHTML += "</div>";
				currentHTML += "<div class=\"popUpRightForm\">";
				currentHTML += "<input type=\"text\" class=\"Entry\" id=\"Entry_1\" onfocus=\"editAndOrAddOption('1');\"  />";
				currentHTML += "</div>";
				currentHTML += "<div class=\"popUpClearBoth\"></div>";
				document.getElementById("showStuff").innerHTML = currentHTML;
				
	return;
}//addMenu()

function addMenu(){
	
	document.getElementById("NumberOfFields").value = "1";
	
	currentHTML = "";
	
				currentHTML += "<div class=\"popUpLeftForm\">";
				currentHTML += "<label>Option Name<label>";
				currentHTML += "</div>";
				currentHTML += "<div class=\"popUpRightForm\">";
				currentHTML += "<input type=\"text\" class=\"Entry\" id=\"Entry_1\" onfocus=\"editAndOrAddOption('1');\"  />";
				currentHTML += "</div>";
				currentHTML += "<div class=\"popUpClearBoth\"></div>";
				document.getElementById("showStuff").innerHTML = currentHTML;
				
	return;
}//addMenu()

function editAndOrAddOption(fieldNumber){
		
		numberOfFields = document.getElementById("NumberOfFields").value;
		if (numberOfFields == fieldNumber){
				storeData = getData(numberOfFields);
				
				addElement();
				putData(storeData);
			
				fieldTheyClicked = "Entry_" + fieldNumber;
				document.getElementById(fieldTheyClicked).focus();
		}//if
				
				
}//editAndOrAddOption()


function putData(storeData){
	/* Get data already input by the user */
		
		//alert("putData: " + storeData);
		
		index = 0;
		for (i=1; i < storeData.length; i++){
				
				curID = "Entry_" + i;
				document.getElementById(curID).value = storeData[index];
				index++;
				
				
		}//for
		
		return storeData;
}//putData()


function getData(numberOfFields){
	/* Get data already input by the user */
		storeData = new Array();
		
		index = 1
		for (i=0; i < numberOfFields; i++){
				curID = "Entry_" + index;
				storeData[i] = document.getElementById(curID).value;
				index++;
		}//for
		
		return storeData;
}//getData()

function addElement(){
			//add another element and update necessary fields
				id = parseInt(document.getElementById("NumberOfFields").value) + 1;
				currentHTML = document.getElementById("showStuff").innerHTML;	
				
				currentHTML += "<div class=\"popUpLeftForm\">";
				currentHTML += "<label>Option Name<label>";
				currentHTML += "</div>";
				currentHTML += "<div class=\"popUpRightForm\">";
				currentHTML += "<input type=\"text\" class=\"Entry\" id=\"Entry_" + id  + "\" onfocus=\"editAndOrAddOption('" + id + "');\" />";
				currentHTML += "</div>";
				currentHTML += "<div class=\"popUpClearBoth\"></div>";
				
				document.getElementById("showStuff").innerHTML = currentHTML;
				document.getElementById("NumberOfFields").value = id;
				//return currentHTML;
			
}//addElement()

</script>

<div class="popUp" id="popUp">
	<div style="closePopUp">
		<a href="javascript:closePopUp();">Close Pop Up</a>
	</div>
	<h2>Create A Field</h2>
	<form action="processAddField.php" method="post" name="myForm">
	<div class="popUpLeftForm">
			<label>Add</label>
			<img class="info" src="components/medical/info_bore.png" width="20" height="auto" />
	</div><!--popUpLeftForm-->
	<div class="popUpRightForm">
			<select id="addThis" name="addThis" onchange="addField();">
					<option value=""></option>
					<option value="Text Field">Text Field</option>
					<option value="Menu">Drop Menu</option>
					<option value="Radio Group">Radio Group</option>
					<option value="Checkbox">Checkbox</option>
			</select>
	
	</div><!--popUpRightFoimages/info_cool.pngrm-->
	
	<div class="popUpClearBoth"></div>

	<div class="popUpLeftForm">Required</div>
	<div class="popUpRightForm">
				  <div class="popUpLeftFormAlign"><label>Yes</label></div>
				  <div class="popUpRightFormAlign"><input type="radio" name="IsRequired" value="yes" /></div>
				  <div class="popUpFormAlignClearBoth"></div>
				  <div class="popUpLeftFormAlign"><label>No</label></div>
				  <div class="popUpRightFormAlign"><input type="radio" name="IsRequired" value="no" checked/></div>
				  <div class="popUpFormAlignClearBoth"></div>
	</div>
	
	<div class="popUpClearBoth"></div>			
			  <div class="popUpLeftForm">Order of Appearance</div>
			  <div class="popUpRightForm"><select name="OrderOfAppearance">
					<?php
					for ($i=1;$i<5;$i++){
							  echo "<option value=\"$i\">$i</option>";
					}//for
					?>
			  </select></div>
			  <div class="popUpClearBoth"></div>
	
	<div class="popUpLeftForm">
		<label>Label</label>
	</div>
	<div class="popUpRightForm">
			<input type="text" style="width:90px;" />
	</div>
	<div class="popUpClearBoth"></div>
	
	
	<input type="hidden" id="NumberOfFields" />
	<span id="showStuff">
			
	</span>
	

	<div style="clear:both;"></div>
	
	<div class="popUpLeftForm">
				&nbsp;
	</div><!--popUpLeftForm-->
	<div class="popUpRightForm">
			<input name="Submit" type="submit" value="Add" />
	</div><!--popUpRightForm-->

	<div class="popUpClearBoth"></div>
	</form>
	
</div><!--popUp-->


<p>

<br />
Standard<br />
<a href="javascript:showPopUp('textField');">Add Text Field</a>
<a href="">Add Menu</a>
<a href="">Add Radio Group</a>
<a href="">Add Checkbox</a>
</p>
<p>

<br />
Tool<br />
<a href="">Add State Menu</a>
</p>
<form action="" method="get">




<div class="component">
Type
</div><!--leftForm-->

<div class="component">
Required
</div>



<div class="component">
Order of Appearance
</div>

<div class="component">
Label
</div><!--leftForm-->

<div class="component">
Remove
</div><!--leftForm-->

<div class="clearBoth"></div>




<div class="component">
Text Field
</div><!--leftForm-->

<div class="component">
<label>Yes</label><input type="radio" name="IsRequired" value="yes" />
<br />
<label>No</label><input type="radio" name="IsRequired" value="no" checked/>
</div><!--leftForm-->


<div class="component">
		<select name="">
				
				<?php
					
					for ($i=1;$i<5;$i++){
							echo "<option value=\"$i\">$i</option>";
					}//for
				?>
				
		</select>
</div>

<div class="component">
<input type="text" style="width:90px;" value="Name of Frog" />
</div><!--leftForm-->

<div class="component">
-
</div><!--leftForm-->


<div class="clearBoth"></div>




<div class="component">
Menu
</div><!--leftForm-->

<div class="component">
<label>Yes</label><input type="radio" name="IsRequired" value="yes" />
<br />
<label>No</label><input type="radio" name="IsRequired" value="no" checked/>
</div><!--leftForm-->

<div class="component">
		<select name="">
				
				<?php
					
					for ($i=1;$i<5;$i++){
							echo "<option value=\"$i\">$i</option>";
					}//for
				?>
				
		</select>
</div>

<div class="component">
<input type="text" style="width:90px;" value="Name of Frog" />
</div><!--leftForm-->

<div class="component">
-
</div><!--leftForm-->


<div class="clearBoth"></div>




</form>


