<?php

	require("../vendor/autoload.php");
	// This file is being included to load the report_alias class before serialize() called. [LP-10060]
	require_once("/home/poslavu/public_html/admin/cp/reports_v2/defs.php");

	// if we want to manually block traffic from certain IP addresses
	if (in_array($_SERVER['REMOTE_ADDR'], array("107.141.64.164")))
	{
		//exit();
	}
	require_once(__DIR__."/resources/session_functions.php");
	$domain = standarizeSessionDomain();
	session_start();
	ini_set('memory_limit','120M');
	ini_set("display_errors",0);
	putenv("TZ=America/Chicago");

	// keep this around for now until it is completely phased out

	require_once(__DIR__."/resources/core_functions.php");
	require_once(__DIR__."/resources/action_tracker.php");
	require_once("/home/poslavu/public_html/admin/sa_cp/billing/billing_lockout_functions.php");
	require_once("/home/poslavu/public_html/admin/lib/modules.php");
	require_once(__DIR__."/objects/menuObject.php");
	require_once(__DIR__."/resources/backend_emergency_notification.php");
	require_once(__DIR__."/../../inc/olo/config.php");
	require_once(resource_path()."/chain_functions.php");
	include_once($_SERVER['DOCUMENT_ROOT'].'/orm/AdminActionLog.php');
	include_once($_SERVER['DOCUMENT_ROOT'].'/orm/ConfigLocal.php');
	require_once(__DIR__."/../lib/helpers/url_helpers.php");

	// to have all the session variables echoed in the browser for debugging
	if (isset($_GET['tst2']))
	{
		foreach ($_SESSION as $key => $val)
		{
			echo $key."=".$val."<br>";
		}
	}

	// Check if JSON should be returned
	$return_json = isset($_SERVER['CONTENT_TYPE']) &&  $_SERVER['CONTENT_TYPE'] == 'application/json';

	if ($return_json) {
		header('Content-Type: application/json');
	}

/***********************GLOBALS **************************/
	// Populate env variables from env.json file into the $_ENV superglobal
	populateEnvVars();

	$browserInfo = new WhichBrowser\Parser($_SERVER['HTTP_USER_AGENT']);
	$lavu_minimum_login_level = 3;

	$template_dir     = "";
	$content_dir      = "";
	$content_status   = "";
	$content          = "";
	$location_gateway = "";
	$cpcode			  = "";
	$menu_mode        = "";
	$locSelector	  = "";

	$location_info  = array();
	$package_result = array();

	$in_lavu      			= true;
	$suppress_billing_alert = false;
	$loggedin_is_distro 	= false;
	$reload_language_pack   = false;
	$has_LLS				= false;
	$use_special_menu 		= false;
	$package_component		= NULL;
	$in_backend				= true;
	$first_login            = false;

	$mode           = urlvar("mode");
	$submode        = urlvar("submode", 1);
	$action         = urlvar("action");
	$parent_mode    = urlvar("parent_mode");
	$parent_submode = urlvar("parent_submode");
	//print_r($_SESSION);
	// load global information
	$rdb 			= admin_info("database");
	$loggedin_id 	= admin_info("loggedin");
	$data_name      = sessvar("admin_dataname");
	$locationid     = check_location_id($rdb, sessvar("locationid"));
	$company_id     = sessvar("admin_companyid");
	$got_location_id = $locationid;

	require_once(resource_path()."/chain_functions.php");
	if (isset($_GET['locationid']) && $loggedin_id)
	{
		verifyCanAccessRestaurantID($data_name, admin_info('dataname'), $_GET['locationid']);
		checkSetGroupid($data_name, $_GET['locationid']); // updates the group id if the location id has changed
	}

	if (isset($_GET['locationid']))
	{
		$req_locationid = $_GET['locationid'];
	}
	else
	{
		$req_locationid = $locationid;
	}

	if ($mode == 'webgift') {
		$protocol = isset($_SERVER["HTTPS"]) ? 'https' : 'http';
		header('Location: '. $protocol. '://'. $_SERVER['HTTP_HOST'].'/'.$mode);
		exit;
	}

	if ($mode == 'cp4_unsupported') {
		require("./unsupported_browser_cp3.html.php");
		exit;
	}

	$section = "";
	if ($mode != "billing")
	{
		$section_end_index = strpos($mode, '_');
		$section = substr($mode, 0, $section_end_index );
		$mode = substr($mode, $section_end_index+1);
	}
	else
	{
		$section = $mode;
	}

	$reload_language_pack = ($mode=="location_settings" || $mode=="custom_language");

	$locSelector = getChainLocationSelector($req_locationid, $reload_language_pack, $location_info);

	$location_info = sessvar("location_info");
	$comPackage = $location_info['component_package_code'];

	$maindb = "poslavu_MAIN_db";
	$template_render_mode = "";

	$cpmode_handle = "set_viewcp_mode-".$loggedin_id;
	if(isset($_GET['viewcp']))
	{
		$_SESSION['viewcp_mode'] = $_GET['viewcp'];

		mlavu_query("insert into `poslavu_MAIN_db`.`temp_info` (`dataname`,`type`,`datetime`,`request`,`info`) values ('[1]','[2]','[3]','[4]','[5]')",$data_name,$cpmode_handle,date("Y-m-d H:i:s"),"",$_SESSION['viewcp_mode']);
	}
	else if(!isset($_SESSION['viewcp_mode']) || !isset($_SESSION['last_data_name']) || $_SESSION['last_data_name'] != $data_name)
	{
		if($data_name!="")
		{
			$viewcp_query = mlavu_query("select * from `poslavu_MAIN_db`.`temp_info` where `dataname`='[1]' and `type`='[2]' order by `id` desc limit 1",$data_name,$cpmode_handle);
			if(mysqli_num_rows($viewcp_query))
			{
				$viewcp_read = mysqli_fetch_assoc($viewcp_query);
				$_SESSION['viewcp_mode'] = $viewcp_read['info'];
			}
			else
			{
				$_SESSION['viewcp_mode'] = "default";
			}
		}
	}

	$_SESSION['last_data_name'] = $data_name;

	if((isset($_GET['mode']) && $_GET['mode']=="home_home_newdev") || (isset($_SESSION['viewcp_mode']) && $_SESSION['viewcp_mode']=="new"))
	{
		if ($mode == "home")
		{
			$mode = "home_newdev";
		}
		$template_file = "main_menu_new_skin_grid.tpl";
		$template_render_mode = "render_mode2";
	}
	else if ($comPackage == "lavukart")
	{
		$template_file = "main_menu_kart.tpl";
	}
	else
	{
		$template_file = "main_menu_new_skin.tpl";
	}

	// ***** reset these in case of chain location change *****

	$rdb				= admin_info("database");
	$loggedin_id		= admin_info("loggedin");
	$data_name			= sessvar("admin_dataname");
	$locationid			= sessvar("locationid");
	$got_location_id	= $locationid;

	// *****

	/***************************** DB CONNECTS ********************************/
	//check_lockout_status(3);
	lavu_auto_login();
	lavu_connect_byid(admin_info("companyid"),$rdb);
	/**************************** END DB CONNECTS *****************************/

	require_once(__DIR__."/resources/log_process_info.php");
	lavu_write_process_info(array());

	appVersionMinMax($locationid, "POSLavu Client", $min_POSLavu_version, $max_POSLavu_version); // returns minimum and maximum version found in devices table
	$modules = getModules();

/***************************** CP3 Config & Redirections ********************************/
	$isNewControlPanel = isNewControlPanelActive();
	$cp3Mode = isset($_GET['cp3mode']) ? $_GET['cp3mode'] : false;

	if(isCustomerCP3Beta() && !isset($_GET['logout']) && !isset($_POST['login'])) {
		if($_GET['mode'] == 'switch_to_new_cp') {
			if (isCP3BrowserSupported($browserInfo)) {
				// set isNewControlPanel to 1, this will set the CP view for this specific location
				// to the new design
				$updateQuery = "UPDATE `poslavu_MAIN_db`.`restaurants` SET is_new_control_panel_active=1 WHERE data_name='".$data_name."'";
				lavu_query($updateQuery);

				// Reset welcome message modal flag so that we always show this modal to the user which
				// performs the switching to CP4. See more: https://jira.lavu.com/browse/CP3-2387
				$user_id = admin_loggedin();
				$welcomeMessageQuery = "UPDATE `users` SET viewed_welcome_message = 0 WHERE id = '". $user_id ."'";
				lavu_query($welcomeMessageQuery);

				updateSessionCP3Token();
				browserGoToCP3($domain);
			} else {
				header('Location: ?mode=cp4_unsupported');
			}

			exit();
		}
		// this is when changing chain location from the new Control Panel
		if( !$_GET['cp3_updating_chain_location'] ) {
			if (($isNewControlPanel && !isset($_COOKIE['CP3_ENABLED'])) || ($isNewControlPanel && !isset($_SERVER['HTTP_REFERER']))) {
				// usernames ending in poslavu_admin are those of Manage Users
				if (substr(sessvar('admin_username'), -strlen('poslavu_admin')) === 'poslavu_admin') {
					// If a Manage user enters and the location is set to new CP it will be sent to
					// the new Manage login
					header('Location: '.$_SERVER['CP3_URL'].'/manage');
				} else {
					// This means the user is attempting reach CP and has already switched to CP3
					updateSessionCP3Token();
					browserGoToCP3($domain);
				}

				exit();
			}
		} else {
			// if the request is for updating chain location then end here the query as all the
			// session updates have already been performed in previous lines
			exit();
		}
	}

	if ($isNewControlPanel) {

		$settingsPaths = ["settings_company_settings",
			"settings_location_settings",
			"settings_advanced_location_settings",
			"settings_inventory_settings",
			"settings_order_types",
			"settings_meal_periods",
			"settings_revenue_centers",
			"settings_silvo",
			"settings_lavu_togo",
			"settings_chain_management",
			"settings_checkout_layout_editor",
			"settings_general_ledger",
			"settings_kiosk_settings",
			"settings_terminal_configuration_settings",
			"settings_lavu_togo2"
		];

		$userPaths = ["settings_edit_users",
			"settings_mealplan_settings",
			"settings_scheduling",
			"settings_employee_classes",
			"settings_overtime_settings",
			"settings_shift_management",
			"settings_pizza_settings",
			"settings_pizza_settings",
			"settings_customer_settings",
			"settings_email_report_settings",
			"settings_waivers",
			"settings_delivery_settings",
			"settings_waivers",
			"settings_registration_settings",
			"settings_delivery_routing_settings",
			"settings_hold_fire_settings"
		];

		$printerPaths = ["settings_devices",
			"settings_printers",
			"settings_sync_status",
			"settings_api",
			"settings_message_log",
			"settings_component_manager",
		];

		print_r($printPaths);

		$paymentPaths = ["settings_credit_card_integration",
			"settings_quickbooks_integration",
			"settings_quickbooks_integration",
			"settings_payment_methods",
			"settings_discount_types",
			"settings_discount_groups",
			"settings_quickpay_buttons",
			"settings_quicktip_buttons",
			"settings_loyaltree",
			"settings_tax_rates",
			"settings_tax_profiles",
			"settings_tax_exemptions"
		];

		if(in_array($section."_".$mode, $settingsPaths) || $cp3Mode === 'location') {
			$menuCookie = 1;
			$cp3Mode = 'location';
		}elseif (in_array($section."_".$mode, $paymentPaths)|| $cp3Mode === 'payment' ) {
			$cp3Mode = 'payment';
			$menuCookie = 2;
		}elseif (in_array($section."_".$mode, $printerPaths) || $cp3Mode === 'printer' ) {
			$cp3Mode = 'printer';
			$menuCookie = 3;
		}elseif (in_array($section."_".$mode, $userPaths) || $cp3Mode === 'users' ) {
			$cp3Mode = 'users';
			$menuCookie = 4;
		}
	}
	$cp3ModeCookie = isset($menuCookie) ? $menuCookie : false;

	// END CP3 and SE Checks


	if (isset($_GET['locationid']) && $loggedin_id)
	{
		verifyCanAccessRestaurantID($data_name, admin_info('dataname'), $_GET['locationid']);
		checkSetGroupid($data_name, $_GET['locationid']); // updates the group id if the location id has changed
	}

	if (isset($_GET['locationid']))
	{
		$req_locationid = $_GET['locationid'];
	}
	else
	{
		$req_locationid = $locationid;
	}

	/***************************** END CP3 Config & Redirections *********************/



	$product  = get_product_name($location_info);
	$rootLink = new linkObj(null,null,null,getRootLinkChildren());
	//echo "<pre>";
	if(!isset($_GET['parent']) || $_GET['parent']==''){
		$_GET['parent']= $rootLink->getRootParentOf($section.'_'.$mode);
		if(strstr($_GET['parent'], 'hardware') ||strstr($_GET['parent'], 'guide'))
			$_GET['parent']='tutorials';
	}

	$req_locationid = $locationid;
	if (isset($_GET['locationid'])) {
		//set_sessvar("locationid",$_GET['locationid']);
		$req_locationid = $_GET['locationid'];
		$reload_language_pack = true;
	}


	//if ( isset($location_info['component_package_code']) && ($location_info['component_package_code']=="medical" || $location_info['component_package_code']=="ers") ){

	//$locSelector=getLocationSelector($locationid, $reload_language_pack, $location_info);

	//error_log("index.php");

	ob_start();
	require_once(dirname(__FILE__).'/resources/help_box.php');
	$s_help_box = ob_get_contents();
	ob_end_clean();

	/******************************* END GLOBALS ******************************/

	/******************************* NOTIFICATION *****************************/
	//email_alexoid_if_cc_info_changed();
	function email_alexoid_if_cc_info_changed(){
		if( isset($_POST['first_name']) &&
			isset($_POST['last_name']) &&
			isset($_POST['card_number']) &&
			isset($_POST['card_expiration']) &&
			isset($_POST['card_code']) ){
			mail('ahassley@poslavu.com',
				   admin_info("dataname") . 'has changed their cc info',
				   'Hi Alexiod, dataname: ' . admin_info("dataname") .
				   ' has changed their cc info.');
		}
	}
	/***************************** END NOTIFICATION ***************************/

	/************************** INITIALIZING VARIABLES ************************/

	if (isset($_GET['logout'])) {
		admin_logout();

		// skip redirection only if redirect is set and equals false
		if (!isset($_GET['redirect']) || $_GET['redirect'] != "false") {
			header('Location: /cp/index.php');
		}

		lavu_exit();
	}

	if (!admin_loggedin() || isset($_POST['login'])) {
		$content_status .= "&nbsp;";
		require("areas/login.php");
		lavu_exit();
	}

	require_once 'objects/backend_user.php';
	BackendUser::currentBackendUser();

	if(isset($_GET['menu_mode']))
		$_SESSION['menu_mode'] = $_GET['menu_mode'];
	if(!isset($_SESSION['menu_mode']))
		$_SESSION['menu_mode'] = "default";
	$menu_mode=$_SESSION['menu_mode'];

	// Storing inventory migration status into session
	$inventoryStatus = getInventoryStatus();
	set_sessvar("INVENTORY_MIGRATION_STATUS", $inventoryStatus);
	//Get cache enabled flag and stored into session, not required output because using it from session
	isCacheEnabled();

	if(isset($_POST['mode']) && $_POST['mode']=='update_password'){
		//echo print_r($_POST,1);
		if( isset($_POST['password1']) && !empty($_POST['password1'])&& isset($_POST['password2']) && !empty($_POST['password2']) && $_POST['password2']==$_POST['password1'] && isset($_POST['username']) && !empty($_POST['username'])){
			$update_query= lavu_query("UPDATE `users` SET `password`=PASSWORD('[1]') WHERE `username`='[2]' LIMIT 1 ",  $_POST['password1'], $_POST['username']);
			if(lavu_dberror()){
				$error_string=lavu_dberror();
				return;
			}else{
				lavu_query("INSERT INTO `config` (`setting`,`value`) VALUES ('main_account_password_updated','1')");
				$_GET['show_update_password']=0;
			}
		}else{
			$error_string= "username or password not sent";
		}
	}

	if(isset($_GET['show_page']) && $_GET['show_page']!="")
	{
		if(strpos($_GET['show_page'],"..")==false)
		{
			require_once("areas/" . $_GET['show_page'] . ".php");
		}
		exit();
	}

	if(isset($_GET['widget']))
	{
		if(admin_loggedin())
		{
			$rdb = admin_info("database");

			lavu_connect_dn($data_name,$rdb);
			//lavu_connect_byid(admin_info("companyid"),$rdb);

			//lavu_select_db($rdb);
			if(isset($_GET['locationid'])) set_sessvar("locationid",$_GET['locationid']);
			$locationid = sessvar("locationid");

			if(strpos($_GET['widget'],"components/")!==false)
			{
				$widget_file = $_GET['widget'] . ".php";
			}
			else
			{
				$widget_file = "areas/" . $_GET['widget'] . ".php";
			}
			if(is_file($widget_file))
			{
				require_once($widget_file);
			}
			exit();
		}
	}

	if (!checkLoginStatus($loggedin_id))
	{
		echo "Access Denied";
		admin_logout();
		exit();
	}

	$cpcode = locationSetting("component_package_code");

	/********************* END INITIALIZING VARIABLES *************************/

	/******************* CHECK FOR FIRST LOCATION LOGIN ***********************/
	if (!isset( $location_info['first_location_backend_login'] ) ) {
		set_sessvar("first_login", 1);
		$arguments = array(
			'location_id' => $locationid,
			'first_location_backend_login' => gmdate('Y-m-d H:i:s')
		);
		$res = lavu_query("INSERT INTO `config` ( `location`, `setting`, `type`, `value` ) VALUES ( '[location_id]', 'first_location_backend_login', 'location_config_setting', '[first_location_backend_login]')", $arguments );
		if( $res === FALSE && DEV ){
			echo 'Error Inserting Config Setting: ' . lavu_dberror() .' <br />';
		}
	}
	/***************** END CHECK FOR FIRST LOCATION LOGIN *********************/

	function buildPage($template_dir, $content_dir, $template, $content, $rootLink, $mode, $locSelector, $req_locationid, $suppress_billing_alert, $s_help_box = '')
	{
		global $location_info;
		global $data_name;
		global $loggedin_id;
		global $section;
		global $mode;
		global $company_id;
		global $isNewControlPanel;

		CPActionTracker::track_action($section."/".$mode, "enter");

		//error_log("buildPage: ".print_r($_GET, true));
		if($template_dir!="") $template = str_replace("images/",$template_dir . "/images/",$template);
		if($content_dir!="") $content = str_replace("images/",$content_dir . "/images/",$content);

		$string = ' <nav>'. $rootLink->__toString(). '</nav>';
		if($_GET['parent']){
			//echo $_GET['parent'];
			if( $section == 'reports' ){
				$cid = admin_info('companyid');
				if( $mode == 'reports_reports_v2' ){
					$template = str_replace("[internalMenu]", '', $template);
				} else {
					$conf_report_links = "";
					$conf_report_links .= "<table width='100%'><tr>";
					$conf_report_links .= "<td width='50%' align='left'><a style='font-family:Verdana; color:#3481a9; font-weight:bold; cursor:pointer' onclick='window.location=\"?mode=settings_configure_reports&return_mode=reports_menu\"'>Configure Reports</a></td>";

					if( !$isNewControlPanel ) {
						$conf_report_links .= "<td width='50%' align='right'><a href=\"?mode=reports_reports_v2\" style=\"color: transparent; display: inline-table; margin: 2px;\"><div style=\"background: #3382A9; padding: 5px 10px; border-radius: 3px; color: white;\">View New Reports</div></a></td>";
					}

					$conf_report_links .= "</tr></table>";
					$internal_menu = $conf_report_links.$rootLink->makeInternalMenu($_GET['parent'],'',7);
					$template=str_replace("[internalMenu]", $internal_menu, $template);
				}
			}else if($_GET['parent']=='settings_location_settings' || $_GET['parent']=='kart_kart'){
				$template=str_replace("[internalMenu]", $rootLink->makeInternalMenu($_GET['parent'],'',7), $template);
			}else
				$template=str_replace("[internalMenu]", $rootLink->makeInternalMenu($_GET['parent'],'true', 7), $template);
		}

		if($_GET['parent'])
			$functionString="window.initMenu=\"".$_GET['parent']."\";";

		$template= str_replace("[menuContent]",$string, $template);

	//error_log("url_req 1: ".$_SERVER['REQUEST_URI']);
	//error_log("req_locationid: ".$req_locationid);
		$url_req = str_replace(array("locationid=$req_locationid","locationid="),array("","&"),$_SERVER['REQUEST_URI']);
	//error_log("url_req 2: ".$url_req);
		$url_req = trim($url_req,"?");
		$url_req = trim($url_req,"&");
		if (strpos($url_req,"?")!==false) $url_req .= "&locationid=";
		else $url_req .= "?locationid=";
	//error_log("url_req 3: ".$url_req);

		$template= str_replace("[locations]", "<select name='location_select' onchange='window.location = \"$url_req\" + this.value'>".$locSelector. "</select> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" , $template);
			// adding data_name to cp by: Kelly Campbell
 		$template= str_replace("[databaseview]", sessvar('admin_dataname'), $template);
		$template= str_replace("[username]", sessvar('admin_fullname'), $template);
		$selected = isset($_GET['selected']) ? $_GET['selected']:'';
		// echo $selected;
		$template= str_replace("[content_status]",getContentStatus(),$template);
		$accountError=accountError($suppress_billing_alert);
		$status= get_collection_lockout_status();
		$template=str_replace("[billingNotification]",$accountError,$template);
		$locales = [
			'en' => [
			  'New' => 'NEW',
			  'newLook' => 'Lavu Control Panel has a fresh new look.',
			  'Update now' => 'Update now',
			  'Learn more' => 'Learn more',
			  'newCP' => 'Go to the new Control Panel',
			],
			'es' => [
			  'New' => 'Nuevo',
			  'newLook' => 'Lavu tiene una nueva interfaz de usuarios.',
			  'Update now' => 'Actualizar ahora',
			  'Learn more' => 'Más información',
			  'newCP' => 'Ir al nuevo Panel de Control',
			],
		  ];
		
		  $lang = $_COOKIE['CP4_LOCALE'];
		  $values = $lang=='es' ? $locales['es'] : $locales['en'];

		if ($status == 'allow' && !isExternalAdmin() && isCustomerCP3Beta()) {
			$template = str_replace("[cp4BetaToggleLink]","<a href='/cp/index.php?mode=switch_to_new_cp' class='switchcp--link'>". $values['newCP'] ."</a>",$template);
			$template = str_replace("[cp4BetaToggleBanner]","
				<div class=\"notification__newcp\">
					<p id=\"aboveHeaderContDiv\" class=\"notification__newcp--header\">
						<b id='aboveHeaderContText1' class=\"notification__newcp--new titletext\">". $values['New'] ."</b>
						". $values['newLook'] ."
						<span class=\"notification__newcp--link\" onclick=\"window.location.href='/cp/index.php?mode=switch_to_new_cp'\">". $values['Update now'] ."</span>
						<span class=\"notification__newcp--link\" onclick=\"window.location.href='https://support.lavu.com/hc/en-us/categories/360001810793-Control-Panel-4-The-New-Lavu-Control-Panel'\">". $values['Learn more'] ."</span>
						<span id=\"aboveHeaderContCancel\" class=\"notification__newcp--remove\" onclick='hideNotificationBar(\".notification__newcp\")'>X</span>
					</p>
				</div>
			",$template);
			$template = str_replace("[cp4BetaToggleClass]",$accountError != "" ? "logoWithAlert" : "",$template);
		} else {
			$template = str_replace("[cp4BetaToggleLink]","",$template);
			$template = str_replace("[cp4BetaToggleBanner]","",$template);
			$template = str_replace("[cp4BetaToggleClass]","",$template);
		}

		//code added by Ravi Tiwari on 8th Mar 2019
		if(isset($_GET['reset_prompted']))
		{
			$_SESSION['prompted'] = 0;
		}
		if(isset($_GET['set_rest_vertical']))
		{
			//Code added by Ravi tiwari to stop Business info prompt on 8th Mar 2019
			$_SESSION['prompted'] = 0;

			$businessInfoDtls = LavuJson::json_decode($_REQUEST['set_rest_vertical']);
			$serviceList = array();
			$mailBody = "<table>";
			$settings = "";
			foreach ($businessInfoDtls as $businessInfo) {
				if ($businessInfo['name'] == 'service_type' || $businessInfo['name'] == 'business_name')
					continue;
				$settings .= "'" . $businessInfo['name'] . "', ";
			}

			$settings .= "'vertical_selected'";

			$result = lavu_query("SELECT `setting` FROM `config` where `type` = 'location_config_setting' and _deleted = 0 and `setting` in ($settings)");
			while ($resultdata = mysqli_fetch_assoc($result)) {
				$configSettings[$resultdata['setting']] = $resultdata['setting'];
			}

			foreach ($businessInfoDtls as $businessInfo) {
				if ($businessInfo['name'] == 'service_type' && $businessInfo['value'] != '') {
					$serviceList[] = $businessInfo['value'];
				}

				if ($businessInfo['name'] == 'business_name') {
					mlavu_query("UPDATE `restaurants` SET `company_name` = '[1]' where `id` = '[2]'", $businessInfo['value'], $company_id);
					$location_info[$businessInfo['name']] = $businessInfo['value'];
				}

				if ($businessInfo['name'] != 'business_name' && $businessInfo['name'] != 'service_type') {
					if (isset($configSettings[$businessInfo['name']])) {
						if ($businessInfo['name'] == 'business_country') {
							$countryInfo = explode('_', $businessInfo['value']);
							$country = $countryInfo[0];
							$countryCode = $countryInfo[1];
							lavu_query("UPDATE `config` SET `value` = '[1]', `value2` = '[2]' where`type` = 'location_config_setting' and _deleted = 0 and `setting` = '[3]'", $country, $countryCode, $businessInfo['name']);
						} else {
							lavu_query("UPDATE `config` SET `value` = '[1]' where`type` = 'location_config_setting' and _deleted = 0 and `setting` = '[2]'", $businessInfo['value'], $businessInfo['name']);
						}
						$location_info[$businessInfo['name']] = $businessInfo['value'];
					} else {
						if ($businessInfo['name'] == 'business_country') {
							$countryInfo = explode('_', $businessInfo['value']);
							$country = $countryInfo[0];
							$countryCode = $countryInfo[1];
							lavu_query("INSERT INTO `config` ( `location`, `setting`, `type`, `value`, `value2` ) VALUES ( '[1]', '[2]', 'location_config_setting', '[3]', '[4]')", $company_id, $businessInfo['name'], $country, $countryCode);
						} else {
							lavu_query("INSERT INTO `config` ( `location`, `setting`, `type`, `value` ) VALUES ( '[1]', '[2]', 'location_config_setting', '[3]')", $company_id, $businessInfo['name'], $businessInfo['value']);
						}
							$location_info[$businessInfo['name']] = $businessInfo['value'];
					}
				}

				$businessInfoTitle = ($businessInfo['name'] == 'business_zip') ? 'business_zip / postal code':$businessInfo['name'];
				if ($businessInfo['name'] != 'service_type') {
					$mailBody .= "<tr><td width='200'>".ucwords(str_replace('_', ' ', $businessInfoTitle))."</td><td>:</td><td>".$location_info[$businessInfo['name']]."</td></tr>";
				}
			}

			$serviceType = implode('|', $serviceList);
			if (isset($configSettings['vertical_selected'])) {
				lavu_query("UPDATE `config` SET `value` = '[1]' where`type` = 'location_config_setting' and _deleted = 0 and `setting` = '[2]'", $serviceType, 'vertical_selected');
			} else {
				lavu_query("INSERT INTO `config` ( `location`, `setting`, `type`, `value` ) VALUES ( '[1]', 'vertical_selected', 'location_config_setting', '[2]')", $company_id, $serviceType);
			}
			$location_info['vertical_selected'] = $serviceType;

			$selectedServices = implode(', ', $serviceList);
			$mailBody .= "<tr><td width='200'>Selected Service List</td><td>:</td><td>".$selectedServices."</td></tr>";
			$mailBody .= "<tr><td width='200'>Dataname</td><td>:</td><td>".$data_name."</td></tr>";
			$mailBody .= "<tr><td width='200'>User</td><td>:</td><td>".$loggedin_id."</td></tr>";
			$mailBody .= "</table>";

			$currentTimeStamp = date('Y-m-d H:i:s');
			$user = sessvar('admin_username');
			$accessLevel = sessvar('admin_access_level');
			$chainId = sessvar('admin_chain_id');
			$lastLogRecord = mysqli_fetch_assoc(mlavu_query("SELECT `id` from `cp_action_log` ORDER BY id DESC LIMIT 1"));
			$sequence_last = $lastLogRecord['id'];
			$userId = sessvar('admin_loggedin');
			$ipaddress = $_SERVER['REMOTE_ADDR'];
			$data = sessvar("admin_fullname");
			$detail1 = "Access level: ".$accessLevel;
			$locationId = sessvar("locationid");

			mlavu_query("INSERT INTO `cp_action_log` ( `dataname`, `timestamp`, `action_type`, `action`, `origin`, `username`, `access_level`, `chainid`, `sequence_last`, `sequence_head` ) VALUES ( '[1]', '[2]', 'settings/company_settings', 'saved', '1',      '[3]', '[4]', '[5]'     , '[6]', '[6]')", $data_name, $currentTimeStamp, $user, $accessLevel, $chainId, $sequence_last);

			lavu_query("INSERT INTO `admin_action_log` ( `action`, `time`, `user`, `user_id`, `ipaddress`, `data`, `order_id`, `detail1`, `detail2`, `detail3`, `loc_id`, `server_time`) VALUES ('Inserted Business Information from CP Prompt', '', '[1]', '[2]', '[3]', '[4]', '0', '[5]', '', '', '[6]', now())", $user, $userId, $ipaddress, $data, $detail1, $locationId);

			// To send HTML mail, the Content-type header must be set
			$headers  = 'MIME-Version: 1.0' . "\r\n";
			$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
			$headers .= 'From: vertical@lavu.com';

			mail("brad@lavu.com,corey@lavu.com", "Lavu Submitted Business Information","Lavu Submitted Business Information at " . date("m/d/Y h:i a") . "\n" . $mailBody, $headers);
		}

		if(isset($location_info['vertical_selected']))
			$vertical_selected =	 $location_info['vertical_selected'];
		else
			$vertical_selected = "";

		$chainAccountStatus = sessvar('admin_chain_id');
		//$accountLoginStatus = checkNewAccountStatus();
		if(!isset($_SESSION['prompted'])){
			$_SESSION['prompted'] = 1;
		}

		#if account has been unlocked
		if(sessvar_isset('lockout_status') && sessvar('lockout_status') == "allow"){
			unset_sessvar('lockout_status');
		}

		if($status!='allow' && !$suppress_billing_alert){


			//Line commented by Ravi T to redirect locked out account to billing page
			/*
			$template.="<script type='text/javascript'>";
			$template .= "window.currentMenu= '".$selected."';".$functionString."; $('#lockout_billing').css('display','block'); $('body').css('overflow', 'hidden');";

			if(isset($_SESSION['viewcp_mode']) && $_SESSION['viewcp_mode']=="new")
			{
				$template .= "var winHeight = 4000;
				set_lockout_height = winHeight - 150;
				if(set_lockout_height < 600) set_lockout_heiht = 600;
				document.getElementById('lockout_billing').style.height = set_lockout_height + \"px\";
				";
			}
			$template .= "</script>";*/
			set_sessvar("lockout_status","locked");
			header('Location: /cp/index.php?mode=billing');

		} else if ( !$isNewControlPanel && !is_lavu_admin()  && (isset($_SESSION['prompted']) && $_SESSION['prompted'] == 1)  && cheackBusinessPromptAppearing($data_name)) {
			$template.="<script type='text/javascript'>";
			$template .= "window.currentMenu= '".$selected."';".$functionString."; $('#lockout').css('display','block'); ";
			$template .= "document.getElementById('lockout').style.top = '-120px'; ";
			if(isset($_SESSION['viewcp_mode']) && $_SESSION['viewcp_mode']=="new")
			{
				$template .= "var winHeight = 4000;
				set_lockout_height = winHeight - 150;
				if(set_lockout_height < 600) set_lockout_heiht = 600;
				document.getElementById('lockout').style.height = set_lockout_height + \"px\";
				";
			}

			$rest_types = array();
			$rest_types[] = array("name"=>"Table Service","info"=>"Guests are seated and then give order to waitstaff");
			$rest_types[] = array("name"=>"Quick Service","info"=>"Guests order at a counter and then sit");
			$rest_types[] = array("name"=>"Bar / Lounge","info"=>"Revenue is mainly alcohol sales");
			$rest_types[] = array("name"=>"Coffee Shop","info"=>"Revenue is mainly coffee drink sales, some food");
			$rest_types[] = array("name"=>"Food Truck","info"=>"You operate a mobile business");
			$rest_types[] = array("name"=>"Pizzeria","info"=>"Revenue is mainly pizza, some other food");
			$rest_types[] = array("name"=>"Other","info"=>"Your establishment does not fall into the above options");
			$rest_type_cols = 4;
			$rest_type_col = 0;

			$restaurantInfo = mysqli_fetch_assoc(mlavu_query("SELECT `company_name` FROM `restaurants` WHERE id = '[1]' ", $company_id));
			$businessName = $restaurantInfo['company_name'];

			// Coded added by Ravi Tiwari on 11th Mar 2019
			$configSettings = array(
				'business_country' => '',
				'business_city' => '',
				'business_state' => '',
				'business_address' => '',
				'business_zip' => ''
			);

			//Code added By Ravi Tiwari on 20th Mar 2019
			$comonConfig = new ConfigLocal($data_name);
			$configSettingsData = $comonConfig->getInfo("setting,value,value2","type = 'location_config_setting' and _deleted = 0 and setting in ('business_name', 'business_country', 'business_address', 'business_city', 'business_state', 'business_zip', 'service_type', 'vertical_selected')");

			foreach($configSettingsData as $configData){
				$configSettings[$configData['setting']] = $configData['value'];
			}
			$country = $configSettings['business_country'];
			$address = $configSettings['business_address'];
			$city = $configSettings['business_city'];
			$state = $configSettings['business_state'];
			$zip = $configSettings['business_zip'];
			$verticalSelectedData['value'] = $configSettings['vertical_selected'];

			$inner_content = "<form id='businessInfo_form' type='post'><div class='business_info'><div style='font-family:Verdana; font-size:24px; color:#787878'>Basic Business Information</div><br>";
			$inner_content .= "<div class='note_data'>To get started, please confirm the information provided when your account was created and fill in any areas that were missed.</div><br>";
			$inner_content .= "<div style='width:600px'><div style='width:300px;float:left'><label>Business Name</label><span class='required'>*</span><br><input type='text' id='business_name' name='business_name' value='$businessName'></div> <div style='width:285px;float:right'><label >Country</label><span class='required'>*</span><br><select id='business_country' name='business_country' class='business_country' style='max-width:100%'><option value=''>Select Country</option>";

			$countryCodeList = getCountryCodeList();
			 if (!empty($countryCodeList)) {
				$selected = "";
				foreach ($countryCodeList as $countryInfo) {
					$selected = ($countryInfo['name'] == $country) ? 'selected' : '';
					$displayedValue = formatEncodeCountryCodeWithName($countryInfo['code'], $countryInfo['name']);
					$value = $countryInfo['name']."_".$countryInfo['code'];
					$inner_content .= "<option value='".$value."' $selected>".$displayedValue."</option>";
				}
			}

			$inner_content .= "</select></div>	</div><br>";
			$inner_content .= "<div style='width:600px;float:left'><label>Business Address</label><span class='required'>*</span><input type='text' id='business_address' name='business_address' value='$address'></div>";
			$inner_content .= "<div style='width:600px'><div style='width:190px;float:left'><label>City</label><span class='required'>*</span><br><input type='text' id='business_city' name='business_city' value='$city'></div> <div style='width:190px;float:left;margin-left:15px'><label>State</label><span class='required'>*</span><br><input type='text' id='business_state' name='business_state' value='$state'></div>  <div style='width:190px;float:right'><label>Zip / Postal Code</label><span class='required'>*</span><br><input type='text' id='business_zip' name='business_zip' value='$zip'></div> 	</div><br>";
			$inner_content .= "<div style='width:600px;float:left'><label>Service Style - Please select all that apply</label><span class='required'> *</span> <span class='note_data'>(minimum one required)</span></div>";
			$inner_content .= "<div style='width:600px;float:left'>";
			$inner_content .= "<br><table cellspacing=0 cellpadding=6>";
			$inner_content .= "<tr>";

		 	for($n=0; $n<count($rest_types); $n++)
			{
				$rest_type = $rest_types[$n]['name'];
				$set_rest_type = str_replace(" ","_",strtolower($rest_type));

				$rest_info = (isset($rest_types[$n]['info']))?$rest_types[$n]['info']:"";
				$rest_title_code = "";
				if($rest_info!="") $rest_title_code = " title='$rest_info'";

				$rest_type_col++;
				if($rest_type_col > 1) $inner_content .= "<td>&nbsp;</td>";

				$service_type_id = $set_rest_type;
				if(strpos($set_rest_type, '_/_')) {
					$service_type_id = str_replace("_/_","_", $set_rest_type);
				}
				$activeColor = "";
				$selected_service_type = "";
				$ActiveServiceClass = "";

				$verticalSelectedType = explode("|", $verticalSelectedData['value']);
				if(in_array($set_rest_type, $verticalSelectedType)) {
					$activeColor = "background: rgb(204, 204, 204)";
					$selected_service_type = $set_rest_type;
				}
				if($selected_service_type != ''){
					$ActiveServiceClass = "ActiveService";
				}
				$inner_content .= "<td> <input type='button' id='service_$service_type_id' name='service_type_btn' value='$rest_type' status='0' style='width:8em;height:40px; color:#787878; font-size:16px; cursor:pointer;".$activeColor."' align='center' valign='middle'$rest_title_code  class='$ActiveServiceClass' onclick='selectService(\\\"$service_type_id\\\", \\\"$set_rest_type\\\")'><input type='hidden' id='$service_type_id' name='service_type' value='".$selected_service_type."'> </td>";

				if($rest_type_col >= $rest_type_cols)
				{
					$rest_type_col = 0;
					$rest_type_cols--;
					$inner_content .= "</tr></table><br><table cellspacing=0 cellpadding=6><tr>";
				}
			}
			$inner_content .= "</tr></table>";
			$inner_content .= "</div>";
			$inner_content .= "<div style='width:600px;float:left'><center><input type='button' style='padding: 14px 30px 25px 30px;background: #ccc;font-weight: bold;font-size: 15px;' value='Skip' id='businessInfoSkip' title='Skip' onclick='skipBusinessInformation()'>&nbsp;<input type='button' style='padding: 14px 30px 25px 30px;background: #ccc;font-weight: bold;font-size: 15px;' value='Submit' id='businessInfoSubmit' disabled title='Please fill all required fields.' onclick='submit_businessInformation()'></center><br><div><span class='required'>* </span><span class='note_data'>Indicates required field.</span></div></div>";
			$inner_content .= "</div></form>";
			$inner_template = "<table cellspacing=0 cellpadding=20><tr><td  valign='middle' width='600' height='300' bgcolor='#ffffff' style='border-radius:20px; border:solid 2px #afcc45'>$inner_content</td></tr></table>";
			$template .= "	document.body.innerHTML += \"<div style='position:absolute; z-index:88888; top:0px; left:0px; width:100%; height:100%'><table cellspacing=0 cellpadding=0 width='100%' height='100%'><tr><td align='center' valign='middle' width='100%' height='100%'>$inner_template</td></tr></table></div>\"; ";

			$template .= "</script>";
			$template.="<script type='text/javascript' src='/cp/scripts/select2-4.0.3.min.js'></script>";
			$template.="<link href='/cp/css/select2-4.0.3.min.css' rel='stylesheet'/>";
			$template .= "<script type='text/javascript'>
					$('#business_country').select2();
					$('.select2-container--default .select2-selection--single').attr('style', 'max-width:285px !important;height:37px;padding: 10px; margin: 8px 0;');
					$('.select2-container--default .select2-selection--single .select2-selection__arrow').css({'padding-top':'30px'});
					$('.select2-container--default .select2-selection--single .select2-selection__rendered').css({'font-size':'13px'});
					$('.select2-container').attr('style', 'border:none;width:285px;');
					$('.select2-container').click(function() {
					$('.select2-container--open').css({'z-index':'999999'});
					});
					</script>";

		} else
			$template.="<script type='text/javascript'>window.currentMenu= '".$selected."';".$functionString.";</script>";

		$template= str_replace("[content]", $content.$s_help_box, $template);
		$template.="<script type='text/javascript'>window.currentMenu= '".$selected."';".$functionString."</script>";
		$template.=CPActionTracker::get_javascript();
		return $template;
	}
	function check_lockout_status($lockout_level){
		$lockfname = dirname(__FILE__) . "/../../../private_html/connects_lockout.txt";
		if(!file_exists($lockfname)){
			return;
		}
		$fp = fopen($lockfname,"r");
		$lockout_status = fread($fp,filesize($lockfname));
		fclose($fp);
		if($lockout_status!="")
		{
			$lockout_parts = explode("|",$lockout_status);
			if(count($lockout_parts) > 1 && $lockout_parts[0] * 1 >= $lockout_level && $lockout_parts[1] * 1 > time() - 300)
			{
				echo "We apologize for the inconvenience but the POS Lavu BackEnd is temporarily Unavailable. Please check back later.";
				exit();
			}
		}
	}
	function getReportMenu() {
		global $comPackage;
		global $location_info;
		global $data_name;
		global $countNumber;
		global $isNewControlPanel;
		$countNumber = 0;

		$obj = new DBConnector();
		$memc_conn = $obj->getLink('readreplica');
		if ($memc_conn == '' && ($_GET['mode'] != "reports_reports_v2" || isset($_GET['report'])) && substr_count($_GET['mode'] , 'reports_') > 0) {
			header('Location: ?mode=reports_reports_v2&redirect');
		}
		if( sessvar_isset('admin_dataname') ) {
		    $dataName = sessvar('admin_dataname');
		    $dataName = trim($dataName);
		    if ( $dataName != "" ) {
		        $locationDatabase = "poslavu_".$dataName."_db";
		        $enableTipSharing = mlavu_query("select count(*) as 'numOfEnables' from `".$locationDatabase."`.`emp_classes` where `_deleted` = 0 AND `enable_tip_sharing` = 1 ");
		        $countTipSharing = mysqli_fetch_assoc($enableTipSharing);
		        $countNumber = $countTipSharing['numOfEnables'];
		    }
		}

		$returnArray= array();
		// $reportsV2Obj = new linkObj('reports_reports_v2','Reports v2');

		$reports_query = mlavu_query("SELECT `title`, `id` FROM `poslavu_MAIN_db`.`reports_v2` where `title` NOT LIKE 'test%' and `name` NOT LIKE 'alt%' and `_deleted`!='1'");
		$reports_result = array();
		while( $reports_result[] = mysqli_fetch_assoc( $reports_query ) ); array_pop( $reports_result );
		foreach( $reports_result as $key => $report_entry ){
			$reports_link = "reports_reports_v2&report={$report_entry['id']}";
			if ($report_entry['title'] =='Tip Sharing' && $countNumber == 0) {
			    continue;
			}
			$reports_v2_linkObj = new linkObj($reports_link, $report_entry['title']);
			array_push($returnArray, $reports_v2_linkObj );
		}

		$oldReportsObj = new linkObj("reports_menu","Version One Reports");

		$rcat_query = mlavu_query("select * from `poslavu_MAIN_db`.`report_categories` order by `_order` asc, `id` asc");
		global $modules; // Yeah i should just pass it in.. im sorry... this file still needs a lot more refactoring.

		$filter = "";
		if(isset($location_info['hide_report_ids'])){
			$report_ids = explode("|", $location_info['hide_report_ids']);

//			if($report_ids) {
//				for ($i = 0; $i < count($report_ids); $i++) {
//					if (empty($report_ids[$i])) {
//						unset($report_ids[$i]);
//					}
//				}
//			}
			foreach($report_ids as $key){
				if(isset($key) && empty($report_ids[$key])){
					unset( $report_ids[$key] );
				}
			}

			if( ! function_exists("surround_elements_with_single_quotes") ){
				function surround_elements_with_single_quotes( $str ){
					return "'{$str}'";
				}
			}
			if(count( $report_ids )){
				$filter_str = implode(",", array_map("surround_elements_with_single_quotes", $report_ids) );

				$filter = " AND `id` NOT IN ( {$filter_str} ) ";
			}
		}

		$restricted_filter = " AND (`restrict` = '0')";
		$show_restricted_report_ids = trim(locationSetting("show_restricted_report_ids"));
		if (strlen($show_restricted_report_ids) > 0)
		{
			$numeric_allowed_restricted = array();
			$allowed_restricted = explode(",", trim(str_replace(array("||", "|"), array(",", ""), $show_restricted_report_ids)));
			foreach ($allowed_restricted as $ar)
			{
				if (is_numeric($ar))
				{
					$numeric_allowed_restricted[] = $ar;
				}
			}

			if (count($numeric_allowed_restricted) > 0)
			{
				$restricted_filter = str_replace("'0')", "'0' OR `id` IN (".implode(",", $numeric_allowed_restricted)."))", $restricted_filter);
			}
		}

		$cat_info = array();
		while($rcat_read = mysqli_fetch_assoc($rcat_query))
		{
			$new_cat_info = $rcat_read;
			$new_cat_info['query'] = "select * from `poslavu_MAIN_db`.`reports` where `display`='1' and `categoryid`='[1]' and `component_package`='0' ".$filter.$restricted_filter." order by `title` asc";
			$cat_info[] = $new_cat_info;
		}

		if($comPackage=='lavukart')
		{
			$new_cat_info = array();
			$new_cat_info['query'] = "select * from `poslavu_MAIN_db`.`reports` where `display`='1' and `component_package`='1' ".$filter.$restricted_filter." order by `title` asc";
			$new_cat_info['id'] = "";
			$new_cat_info['title'] = "Kart";

			$cat_info[] = $new_cat_info;
		}

		//while($rcat_read = mysqli_fetch_assoc($rcat_query))
		for($n=0; $n<count($cat_info); $n++)
		{
			$rcat_read = $cat_info[$n];
			$cat_query = $cat_info[$n]['query'];

			$report_query = mlavu_query($cat_query, $rcat_read['id'], NULL, NULL, NULL, NULL, NULL, FALSE);
			if(@mysqli_num_rows($report_query))
			{
				$link=strtolower(str_replace(" ", "_", $rcat_read['title']));
				$link='reports_menu_'.$link;

				$linkObj = new linkObj($link,$rcat_read['title']);

				if($rcat_read['title']=="General"){
					
					$linkObj->addChild( new linkObj("reports_end_of_day_new","End of Day"), $link);
					$linkObj->addChild( new linkObj("reports_time_cards","Time Cards"), $link);
					
					if( !$isNewControlPanel ) {	
						$linkObj->addChild( new linkObj("reports_manage_orders","Manage Orders"), $link);
					}
					if($modules->hasModule("reports.customer_query")) {
						$linkObj->addChild( new linkObj("reports_customer_query","Custom Query"), $link);
					}
				}
				if($rcat_read['title']=="Activity Reports") {
					$linkObj->addChild(new linkObj("reports_kitchen_send_log","Send Log"), $link);
					$lobject = new linkObj("reports_all_discounts","All Discounts", null, null, "reports.alldiscounts");
					$linkObj->addChild($lobject, $link);

				}
				if($rcat_read['title']=="Special")
				{
					$linkObj->addChild(new linkObj("reports_show_overtime_wages_and_hours","Overtime"),$link);
					//$linkObj->addChild(new linkObj("settings_customers_export","Customer Export"),$link);
					$linkObj->addChild(new linkObj("reports_customers_export","Customer Export"),$link);
					$linkObj->addChild(new linkObj("reports_customers_import","Customer Import"),$link);
					$linkObj->addChild(new linkObj("reports_gift_cards_import_export","Lavu Gift Card Import/Export", null, null, "extensions.payment.lavu.gift"),$link);
				}

				if($rcat_read['title']=="Payment Reports"){
					$linkObj->addChild(new linkObj("reports_batch_details","Batch Details", null, null, "reports.batchdetails"),$link);
					$linkObj->addChild(new linkObj("reports_tipouts","Tipouts", null, null, "reports.batchdetails"),$link);
					$linkObj->addChild(new linkObj("reports_payments_with_taxes", "Payments With Taxes", null, null, "reports.paymentswithtaxes"), $link);
					$linkObj->addChild(new linkObj("reports_payments_in_vs_labor", "Payments In Vs. Labor"), $link);
					$linkObj->addChild(new linkObj("reports_employee_calss_vs_labor", "Employee Class Vs  Labor"), $link);
				}
				if($rcat_read['title']=="Sales Made By"){
					$linkObj->addChild(new linkObj("reports_group_sales", "Group Sales"), $link);
					if(strtolower($location_info['gateway']) == "paypal")
						$linkObj->addChild(new linkObj("reports_tips_paypal", "Batch Tips Processing"), $link);
				}

				while($report_read= mysqli_fetch_assoc($report_query)){
					$has_required_module = TRUE;
					$mods = explode("&", $report_read['required_modules']);

					for ($i = 0; $i < count($mods); $i++) {
						$mod = $mods[$i];
						if ($mod == NULL || $mod == "")
							continue;
						$has_required_module = FALSE;
						$or_mods = explode("|", $mod);
						foreach ($or_mods as $omod) {
							if ($modules->hasModule($omod)) {
								$has_required_module = TRUE;
								break;
							}
						}
						if (!$has_required_module) break;
					}
					if ($has_required_module) {
						$reportLink=strtolower(str_replace(" ", "_", $report_read['id']));

						$excludeModulesForCp3Ids = [53, 52, 13, 45, 96, 74, 10];
						$excludeModulesForCp3Labels = [ "Closed Orders", "Open Orders", "Order List",
							"Transactions - Cash", "Voided Transactions", "Reopened Orders", "Voided Orders" ];

						if( ( $isNewControlPanel &&
									(	!in_array($report_read['id'], $excludeModulesForCp3Ids) ||
								 		!in_array($report_read['title'], $excludeModulesForCp3Labels)
									)
								) || !$isNewControlPanel ) {
							// Closed Orders => 53
							// Open Orders => 52
							// Order List => 13
							// Transactions => 38
							// Transactions - Cash => 45
							// Voided Transactions => 96
							// Cashier Sales => 7
							// Reopened Orders => 74
							// Voided Orders => 10
						  $linkObj->addChild( new linkObj("reports_id_".$reportLink, $report_read['title']), $link);
						}
					}
				}
				$oldReportsObj->addChild( $linkObj, 'reports_menu' );
			}
		}
		array_push($returnArray, $oldReportsObj);

		// echo '<pre style="text-align: left;">' . print_r( $returnArray, true ) . '</pre>';

		return $returnArray;
	}
	function is_dev(){
		return false;
	}
	function is_lavu_admin(){
		if (admin_info("lavu_admin") == 1)
			return true;
		else
			return false;
	}
	function create_widget($type, $width, $height, $extra=""){
		echo "<iframe allowtransparency='true' frameborder='0' scrolling='no' width='$width' height='$height' src='index.php?widget=table_view' $extra></iframe>";
	}
	function getRootLinkChildren(){
		/**************************** THIS IS THE MENU ************************/

		global $modules;
		global $comPackage;
		global $min_POSLavu_version;
		global $data_name;
		global $extension_group_mode_to_id;
		global $cp3Mode, $cp3ModeCookie, $isNewControlPanel;
		global $location_info;

		$selected= isset($_GET['selected'])?$_GET['selected']:'';
		$subMenuSelected= isset($_GET['mode'])?$_GET['mode']:'';
		$menuSub = [];
		if($comPackage=='lavukart' )
			$menu_item_name= "Products";
		else
			$menu_item_name="Menu Items";
			
		if(!$isNewControlPanel)
		{
			$menuSub = array(
				new linkObj("menu_edit_menu",$menu_item_name),
				new linkObj("menu_modifiers/edit_optional_modifiers","Optional Modifiers"),
				new linkObj("menu_modifiers/edit_forced_modifiers","Forced Modifiers"),
				new linkObj("menu_modifiers/edit_modifier_groups","Modifier Groups"),
				new linkObj("menu_edit_super_groups","Super Groups"),
				new linkObj("menu_happy_hours","Happy Hours"),
				new linkObj("menu_price_tiers","Price Tiers", null, null, "dining.price_tiers")
			);
		}

		$duplicate_menu_strings_allowed = array("demo_lavu","crunch_it","coccinelle_sys","cafe_nate","il_vicino","bones_","letz_sushi_","demo_nikita_","pomme_bistro","louann_inv_1","nyu_headquarte","nyu_cmc_kiosk","nyu_science_ca","nyu_science_ce","nyu_tisch_or","demo_peaches","cafe_mauritz","caf_mauritz_sk","chained_1","cerveza_patago8","ar040045","ar040013","letter_d","tanamera_ld","tanamera_th","tanamera_sp","tanamera_pp","tanamera_pik","tanamera_coffe3","tanamera_jogja","tanamera_sby1","the_coffee_col","the_coffee_col1","the_coffee_col2","the_coffee_col3","wake_up_coffee","wake_up_call_e","wake_up_call_f","wake_up_call_t","wake_up_call_n","wake_up_call_n1","wake_up_call_n2","wake_up_call_p","wake_up_call_s","red_leaf_organ","red_leaf_organ1","red_leaf_organ2","red_leaf_organ3","chuys_rosedale","chuys_stevenso","chuys_new_stin","chuys_simi_val","mercantile_pro1","mercantile_pro2");
		$duplicate_menu_allowed = false;
		
		for($d=0; $d<count($duplicate_menu_strings_allowed); $d++)
		{
			if(strpos(strtolower(admin_info("dataname")),$duplicate_menu_strings_allowed[$d])!==false)
			{
				$duplicate_menu_allowed = true;
			}
		}

		if($duplicate_menu_allowed) 
		{
			if(!$isNewControlPanel)
			{
				$menuSub[] = new linkObj("menu_duplicate_menu","Duplicate Menu");
			}
		}

		/*$queryLavutogo = lavu_query("select `value` from config where `setting` = 'use_lavu_togo' ");
		$infoLavutogo = mysqli_fetch_assoc($queryLavutogo); */
		$result = lavu_query("select `value` from config where `setting` = 'use_lavu_togo_2' ");
		$infoLavutogo2 = mysqli_fetch_assoc($result);
		$locationSettings=array(
			new linkObj("settings_company_settings","Company Settings"),
			//new linkObj("settings_customer_facing_display","Customer Facing Display"), //we will show this link once it will be released
			new linkObj("settings_location_settings","Basic Location Settings"),
			new linkObj("settings_advanced_location_settings","Advanced Location Settings"),
			new linkObj("settings_inventory_settings","Inventory Settings"),
			new linkObj("settings_order_types","Order Types"),
			new linkObj("settings_meal_periods", "Meal Periods", null, null, "dining.mealperiods"),
			new linkObj("settings_revenue_centers", "Revenue Centers", null, null, "dining.revenuecenters"),
			new linkObj("settings_silvo", "Silvo", null, null, "silvo")

		);
		// LP-9721 (Remove and disable LTG 1.0 from all accounts)
		/*if ($infoLavutogo['value'] == 1 || is_lavu_admin() ) {
			if($infoLavutogo['value'] != 1) {
				$green_star = "<font color='#588604' style='font-size:14px'><b>*</b></font>";
			}
			$locationSettings[] = new linkObj("settings_lavu_togo", " $green_star Lavu ToGo 1.0", null, null, "lavutogo");
		}*/
		if ($infoLavutogo2['value'] == 1 ) {
			$locationSettings[] = new linkObj("settings_lavu_togo2", LAVUTOGO2);
		}
		$locationSettings[] = new linkObj("settings_chain_management", "Configure Chains", null, null, "chains.join");
		$locationSettings[] = new linkObj("settings_checkout_layout_editor", "Checkout Layout Editor");
		$locationSettings[] = new linkObj("settings_general_ledger", "General Ledger Settings");
		if ($cp3ModeCookie!==1) {
			$locationSettings[] = new linkObj("settings_kiosk_settings", "Kiosk Settings", null, null, "extensions.ordering.kiosk");
		}

		if($modules->hasModule('extensions.payment.econduit')){
			$locationSettings[] = new linkObj("settings_terminal_configuration_settings", "Terminal Settings");
		}

		$checkSettings = array();
		$tp_title = "Tax Profiles";

		/**
		 * LavuPay's configuration is done in CP4, therefore
		 * skip _Card Integration_ link if the location
		 * has the LavuPay extension enabled.
		 */
		if (!$modules->hasModule('extensions.payment.lavupay')) {
			$checkSettings[] = new linkObj("settings_credit_card_integration","Card Integration");
		}
		///New added by brian d.

		if($modules->hasModule('accounting.quickbooks')){ ///While in dev phase, we don't want it to show up at all.
			if($modules->hasModule('accounting.quickbooks')){ ///If not in dev phase, we want to show the blank link if they don't have the module.
					$myLinkObj = new linkObj("settings_quickbooks_integration","Quickbooks Integration");
					$checkSettings[] = $myLinkObj;
			}else{
				$myLinkObj = new linkObj("settings_quickbooks_integration","Quickbooks Integration");
				$myLinkObj->disable();
				$checkSettings[] = $myLinkObj;
			}
		}

		///End insert (for copying to cp).
		$checkSettings[] = new linkObj("settings_payment_methods","Payment Methods");
		$checkSettings[] = new linkObj("settings_discount_types","Discount Types");
		$checkSettings[] = new linkObj("settings_discount_groups","Discount Groups", null, null, "financial.discount.groups");
		$checkSettings[] = new linkObj("settings_quickpay_buttons","Quickpay Buttons");
		$checkSettings[] = new linkObj("settings_quicktip_buttons","Quicktip Buttons");
		if ($modules->hasModule("extensions.loyalty.loyaltree")) {
			$checkSettings[] = new linkObj("settings_loyaltree", "LoyalTree");
		}

		if ($min_POSLavu_version < 20200) {
			$tp_title .= " (2.2+)";
			$checkSettings[] = new linkObj("settings_tax_rates","Tax Rates (2.1.6-)");
		}
		$checkSettings[] = new linkObj("settings_tax_profiles",$tp_title);
		$checkSettings[] = new linkObj("settings_tax_exemptions","Tax Exemptions");
		/*$enabled = mlavu_query("select * from `extensions` where `id`='23' and `_deleted`='0'");
		if($_SESSION['poslavu_234347273_admin_access_level'] == 4 && mysqli_num_rows($enabled)>0){
			$checkSettings[] = new linkObj("settings_square_reconciliation","Square Reconciliation");
		}
		*/
	// }

	$usersCustomersSub = array();
	if ($cp3ModeCookie!==4) {
		array_push($usersCustomersSub,
			new linkObj("settings_edit_users","Edit Users"),
			new linkObj("settings_mealplan_settings", "Meal Plan Settings", null, null, "extensions.payment.lavu.mealplan")
		);
	}
	if (!$isNewControlPanel) {
		array_push($usersCustomersSub, new linkObj("settings_scheduling", "Scheduling"));
	}
	if ($cp3ModeCookie!==4) {
		array_push($usersCustomersSub,
			new linkObj("settings_employee_classes", "Employee Classes", null, null, "employees.classes"),
			new linkObj("settings_overtime_settings","Overtime Settings"),
			new linkObj("settings_shift_management", "Shift Management")
		);
	}
	if($comPackage=="pizza" && !($cp3ModeCookie === 4)) {
		array_push($usersCustomersSub, new linkObj("settings_pizza_settings","Pizza Settings"));
		//array_push($usersCustomersSub, new linkObj("delivery_settings","Delivery Settings"));
	}
	if ($comPackage=="tithe"){
		//We allow Customer Settings (Customer Management) and if compackage == '',
		//then we give them the option to convert product.
		//This comtype check will occur inside customer_settings.php
		if($modules->hasModule("components.customer")){
			//array_push($usersCustomersSub, new linkObj("customer_settings","Customer Management"));
		}
	}
	if ($comPackage==""){
		//We allow Customer Settings and if compackage == '', then we give them the option to convert product.
		//This comtype check will occur inside customer_settings.php
		if($modules->hasModule("components.customer")){
			//array_push($usersCustomersSub, new linkObj("customer_settings","Customer Management"));
		}
	}

	//We allow Pizza Settings and if compackage == '' || compackage == 'customer'
	//then we let them upgrade to pizza.
	if (($comPackage=="" || $comPackage=="customer") && !($cp3ModeCookie === 4)){

		//if($modules->hasModule("components.pizza")){
			array_push($usersCustomersSub, new linkObj("settings_pizza_settings","Pizza Settings"));
			//}

		}

		//if($modules->hasModule("components.customer")){
			array_push($usersCustomersSub, new linkObj("settings_customer_settings","Customer Management"));
			//}

			//This link will only display for ABI
			if (sessvar('admin_chain_id') == '564' && sessvar('admin_access_level') >= 4) {
				array_push($usersCustomersSub, new linkObj("settings_email_report_settings", "Manage Email Reports"));
			}

			if ($comPackage=="pizza" || $comPackage=="customer" || $comPackage=="fitness" || $comPackage=="delivery") {
				array_push($usersCustomersSub, new linkObj("settings_delivery_settings", "Deliveries"));
				global $data_name;
				//if (strstr(dirname(__FILE__),'/v2') || $data_name == 'pc_everything_')
				//	array_push($usersCustomersSub, new linkObj("delivery_routing_settings", "Routing"));
			}
			if($comPackage=="ers" || $comPackage=="hotel"){
				array_push($usersCustomersSub, new linkObj("settings_waivers","Waivers"));
				if($comPackage=="ers"){
					array_push($usersCustomersSub, new linkObj("settings_registration_settings","Registration Settings"));
				}
			}
			if(1)//$_SERVER['REMOTE_ADDR']=="216.243.114.158")
			{
				array_push($usersCustomersSub, new linkObj("settings_delivery_routing_settings", "Routing"));
			}

			array_push($usersCustomersSub, new linkObj("settings_hold_fire_settings", "Hold & Fire"));

			$printersTechnicalSub = array(new linkObj("settings_devices","Devices"), new linkObj("settings_printers","Printers / KDS"));
			if (isset($location_info['dtt_enabled']) && $location_info['dtt_enabled']) {
				array_push($printersTechnicalSub, new linkObj("settings_dtt_integration", "DTT Integration Settings"));
			}
			array_push($printersTechnicalSub, new linkObj("settings_sync_status","Sync Status"));

			//LP-9797
			if (is_lavu_admin()) {
				array_push($printersTechnicalSub, new linkObj("settings_api","API"));
			}

			array_push($printersTechnicalSub, new linkObj("settings_message_log","Message Log"));

			//LP-8295 -- determining if they have tables to sync.
			$syncTablesPermissions = getTableSyncPermissionsAndInfo($data_name); //Will return false if does not have any permissions.
			$hasSyncTablePermissions = is_array($syncTablesPermissions) && !empty($syncTablesPermissions);//
			if($hasSyncTablePermissions && sessvar('admin_access_level') >= 4){
				$printersTechnicalSub[] = new linkObj("settings_table_sync","Sync Tables");
			}
			//End LP-8295

			if ( is_lavu_admin() || $modules->hasModule("extensions.util.lavu.component_manager") )
			$printersTechnicalSub[] = new linkObj("settings_component_manager","Component Manager");

			// if reactmode is enable let check if the cp3Moda is location and
			// cp3ModeCookie is 1
			if ($isNewControlPanel) {
				if ($cp3Mode === 'location' && $cp3ModeCookie === 1) {
					$settingsSub=array(
						new linkObj("settings_location_settings","Location Settings",null,$locationSettings ),
						new linkObj(),
					);
				} elseif ($cp3Mode === 'payment' && $cp3ModeCookie === 2) {
					$settingsSub = array(
						new linkObj("settings_payment_methods","Payment Settings",null, $checkSettings),
						new linkObj(),
					);
				} elseif ($cp3Mode === 'printer' && $cp3ModeCookie === 3) {
					$settingsSub = array(
						new linkObj("settings_printers","Printers / Technical",null, $printersTechnicalSub ),
						new linkObj(),
					);
				} elseif ($cp3Mode === 'users' && $cp3ModeCookie === 4) {
					$settingsSub = array(
						new linkObj("settings_edit_users","Users / Customers",null,		$usersCustomersSub),
						new linkObj(),
					);
				}
			} else {
				$settingsSub=array(
					new linkObj("settings_location_settings","Location Settings",null,$locationSettings ),
					new linkObj("settings_payment_methods","Payment Settings",null, $checkSettings),
					new linkObj("settings_edit_users","Users / Customers",null,$usersCustomersSub),
					new linkObj("settings_printers","Printers / Technical",null, $printersTechnicalSub ),
				);
			}


			// $generalSub=array(
			// 	new linkObj("end_of_day","End Of Day"),
			// 	new linkObj("time_cards","Time Cards"),
			// 	new linkObj("manage_orders","Manage Orders"),

			// );
			// $reportsSub=array(
			// 	new linkObj("general","General",null, $generalSub),
			// 	new linkObj("payment_reports","Payment Reports"),
			// 	new linkObj("sales_by","Sales By"),
			// 	new linkObj("product_reports","Product Reports"),
			// 	new linkObj("activity_reports","Activity Reports"),
			// 	new linkObj("special","Special")

		// );
		$ministrySub = array(
			new linkObj("settings_edit_ministries","Edit Ministries"),
			new linkObj("settings_ministry_tithe_options","Tithe Amount Options"),
		);
		$kartEventsSub=array(
			new linkObj("kart_events_edit_events","View Events"),
			new linkObj("kart_events_list_results","List Race Results"),
			new linkObj("kart_events_edit_event_types","Edit Event Types"),
			new linkObj("kart_events_edit_event_sequences","Edit Event Sequences"),
		);
		$kartAdminSub= array(
			new linkObj("kart_admin_edit_tracks","Edit Tracks"),
			new linkObj("kart_admin_edit_rooms","Edit Rooms"),
			new linkObj("kart_edit_karts","Edit Karts"),
			new linkObj("kart_edit_event_checklist","Group Event Checklist"),
			new linkObj("kart_admin_edit_sales_reps","Sales Reps"),
			// new linkObj("kart_newsletter","NewsLetter"),
		);
		$kartCustomersSub=array(
			new linkObj("kart_edit_customers","Edit Customers"),
			new linkObj("kart_admin_email_marketing","Email Marketing"),
			new linkObj("kart_admin_messages","Messages"),
			//new linkObj("kart_admin_monitoring","Monitoring"),
			new linkObj("kart_admin_waivers","Waivers"));

		$kartGarageSub=array(
			new linkObj("kart_admin_edit_service_types","Service Types"),
			new linkObj("kart_admin_edit_service_checklists","Service Checklists"),
			new linkObj("kart_edit_inventory","Garage Inventory"),
		);

		$kartSub= array(
			//new linkObj("kart_edit_karts","Kart Pit",null, $kartPitSub),
			new linkObj("kart_admin","Admin",null, $kartAdminSub),
			new linkObj("kart_edit_customers","Customers",null, $kartCustomersSub),
			//new linkObj("kart_reports","Reports",null, null),
			new linkObj("kart_edit_events","Events",null, $kartEventsSub),
			new linkObj("kart_edit_garage","Garage",null, $kartGarageSub),
		);

		$assistanceSub=array(
			new linkObj("assistance_guide_start","Guide",null, null),
			//new linkObj("assistance_hardware_start","Hardware", null, null),
			new linkObj("assistance_tutorials","Tutorials",null,null),
			new linkObj("assistance_support","Support",null,null)
		);

		require_once(dirname(__FILE__)."/areas/extensions/ext_func.php");
		$extension_groups = getExtensionGroups();
		$extension_group_mode_to_id = array();
		$extensionsSub = array(new linkObj("extensions_all", "All", null, null));
		$extension_group_mode_to_id['all'] = 0;

		foreach ($extension_groups as $title => $id) {
			$as_mode = strtolower(str_replace(" ", "_", $title));
			$extension_group_mode_to_id[$as_mode] = $id;
			$extensionsSub[] = new linkObj("extensions_".$as_mode, $title, null, null);
		}

		require_once 'objects/backend_user.php';

		$rootLinkChildren = array();
		if( BackendUser::currentBackendUser()->isInWhiteList('home') )
			$rootLinkChildren[] = new linkObj('home_home','', 'images/iconHouse.png' ,null);

		// creates the ABI dashboard button if user is part of the ABI chain group
		if (sessvar('admin_chain_id') == 564) {
			$rootLinkChildren[] = new linkObj('dashboard', 'Dashboard', 'images/dashboardIcon.png', null);
		}

		if($comPackage=='lavukart' || $comPackage=='tithe' ){
			if( BackendUser::currentBackendUser()->isInWhiteList('menu') )
				$rootLinkChildren[] = new linkObj('menu_edit_menu', 'Products', 'images/menu.png', $menuSub);
		} else {
			if( BackendUser::currentBackendUser()->isInWhiteList('menu') )
				$rootLinkChildren[] = new linkObj('menu_edit_menu', 'Menu', 'images/menu.png', $menuSub);
		}


		if( $comPackage == 'lavukart' ) {
			if( BackendUser::currentBackendUser()->isInWhiteList('kart') )
				$rootLinkChildren[] = new linkObj('kart_kart', 'Kart', 'images/kart_logo.png',$kartSub);
		}else if ($comPackage=='tithe'){
			if( BackendUser::currentBackendUser()->isInWhiteList('ministries') )
				$rootLinkChildren[] = new linkObj('ministries_edit_ministries', 'Ministries', 'images/menu.png', $ministrySub);
		} else {
			// if( BackendUser::currentBackendUser()->isInWhiteList('inventory') )
			// 	$rootLinkChildren[] = new linkObj('inventory_edit_ingredients', 'Inventory','images/inventory.png',null);
			//
			// 	ADDED by EV 5/11 for NRA Inventory demo
			if( BackendUser::currentBackendUser()->isInWhiteList('inventory') )
				$rootLinkChildren[] = new linkObj('inventory', 'Inventory','images/inventory.png',null);

			if( BackendUser::currentBackendUser()->isInWhiteList('layout') )
				$rootLinkChildren[] = new linkObj('layout_table_setup', 'Layout', 'images/layout.png',null);
		}

		if( BackendUser::currentBackendUser()->isInWhiteList('settings') )
			$rootLinkChildren[] = new linkObj('settings_location_settings', 'Settings','images/settings.png', $settingsSub);
		if(!(admin_info('username') && mysqli_num_rows(lavu_query("SELECT * FROM `users` where `report_access`='1' and `username`='[1]' ", admin_info('username'))))){
			if( BackendUser::currentBackendUser()->isInWhiteList('reports') )
				$rootLinkChildren[] = new linkObj('reports_reports_v2', 'Reports', 'images/reports.png',getReportMenu());
		}

		if( BackendUser::currentBackendUser()->isInWhiteList('assistance') )
			$rootLinkChildren[] = new linkObj('assistance_tutorials', 'Assistance', 'images/assistance.png',$assistanceSub);

		if( BackendUser::currentBackendUser()->isInWhiteList('extensions') )
			$rootLinkChildren[] = new linkObj('extensions_all', 'Extensions', 'images/extensions.png', $extensionsSub);

			// Added 10/12 by EV per LP-148
		if( BackendUser::currentBackendUser()->isInWhiteList('shop') )
			$rootLinkChildren[] = new linkObj('shop.lavu.com','Buy Hardware', 'images/shopIcon.png' ,null);
		return $rootLinkChildren;
	}

	if( BackendUser::currentBackendUser()->isRestrictedUser() && !isset($_GET['mode']) && !isset($_GET['redirect'])){
		$link = $rootLink->getChildren();
		if( count($link) ){
			$link = $link[0]->getLink();
			header('Location: ?mode=' . $link . '&redirect');
		}
	}

	/***************** GETTING THE CONTENTS OF THE PAGE TO LOAD ***************/
	ob_start();
		if($product!="")
		{
			$compath = "components/" . strtolower($product);
			$comdir = dirname(__FILE__) . "/" . $compath;
			$comfile = $comdir . "/index.php";
			/*if(is_file($comfile))
			{
				if($menu_mode=="pos")
				{
					$use_special_menu = false;
					$template_file = "pos_menu.tpl";
					$template_dir = "components/lavukart";
					$content_dir = "";
				}
				else
				{
					$use_special_menu = true;
					$template_file = "kart_menu.tpl";
					$template_dir = "components/lavukart";
					$content_dir = "";
					require_once($comfile);
				}
			}*/
		}

		$a_aliases = array(
			'settings'=>array('require'=>'areas/location_settings.php'),
			'refer'=>array('require'=>'areas/refer_somebody.php'),
			'menu_menu'=>array('require'=>'areas/edit_menu.php'),
			'registration_settings'=>array('require'=> 'components/medical/registration_settings.php'),
			'tutorials'=>array('require'=>'/home/poslavu/public_html/tutorials/show_tutorials.php', 'no_params_functions'=>array('show_tutorials')),
		);

		if( !BackendUser::currentBackendUser()->isInWhiteList($section) ){
			require_once "areas/unauthorized.php";
		} else if ($section == "extensions") {
			require("areas/extensions/extensions.php");
		} else if(file_exists("areas/".$mode.".php") && ($section!='reports' || $mode=='reports_v2') ){
			if( $mode=='billing'){
				$p_area = "customer_billing_info";
				$suppress_billing_alert = true;
			} else if ($mode == 'support') {
				$suppress_billing_alert = true;
			} else if($mode=='home') {
				show_promptbox($data_name, $locationid, $location_info);
			}
			require("areas/".$mode.".php");
		} else if(file_exists("components/lavukart/".$mode.".php") /*|| $section=='reports'*/){
			require("components/lavukart/".$mode.".php");
		} else if (file_exists("components/tithe/".$mode.".php")){
			require("components/tithe/".$mode.".php");
		} else if( isset($a_aliases[$mode]) ){
			require_once($a_aliases[$mode]['require']);
			if (isset($a_aliases[$mode]['no_params_functions']))
				foreach($a_aliases[$mode]['no_params_functions'] as $s_function_name)
					call_user_func($s_function_name);
		} else if(substr($mode,0,5)=="guide"){
			require_once("areas/guide.php");
			// Manually setting these to keep the guide submenu from disappearing after viewing any other guide pages.
			$_GET['parent'] = 'assistance_tutorials';
			$_GET['mode'] = 'assistance_guide_start';
			show_promptbox($data_name, $locationid, $location_info);

		} else if(substr($mode,0,5)=="admin"){
			require_once("components/lavukart/".substr($mode,6, strlen($mode)).".php");

		} else if(substr($mode,0,6)=="events"){
			require_once("components/lavukart/".substr($mode,7, strlen($mode)).".php");

		} else if( $mode == 'menu' && $section == 'reports' && !mysqli_num_rows(lavu_query("SELECT * FROM `users` where `report_access`='1' and `username`='[1]' ", admin_info('username')))){
			header('Location: ?mode=reports_menu_general' );
			// $mode='reports_menu';
			require_once("areas/reports.php");
		} else if ($section == "reports" && !mysqli_num_rows(lavu_query("SELECT * FROM `users` where `report_access`='1' and `username`='[1]' ", admin_info('username')))){
			require_once("areas/reports.php");
		} else if(substr($mode,0,8)=="hardware"){
			require_once("areas/hardware.php");
		} else if($mode=="labor_report"){
			require_once("areas/labor_report.php");
/*
		} else if(substr($mode,0,18)=="registrar_settings"){
			require_once("areas/registrar_settings");
*/
		} else if(substr($mode,0,18)=="customer_management"){
			require_once("areas/customer_management");
		} else if(substr($mode,0,5)=="pizza_settings"){
			require_once("areas/pizza_settings");//--------------- new pizza.
		} else if(strpos($mode, "edit_states") === 0){
			require_once("areas/edit_states.php");
		} else if(strpos($mode, "customers_export") === 0){
			require_once("areas/customers_export.php");
		} else{
			$section = 'home';
			$mode='home';

			$link = $rootLink->getChildren();
			if( count($link) ){
				$link = $link[0]->getLink();
				if( !isset($_GET['redirect'])){
				header('Location: ?mode=' . $link . '&redirect' );
			}
			}

			// if(isset($_GET['home_dev'])) {
			// 	require_once("areas/home_dev.php");
			// } else {
			// 	require_once("areas/home.php");
			// }
		}
		$content = ob_get_contents();
	ob_end_clean();

	/***************** END THE CONTENTS OF THE PAGE TO LOAD *******************/

	/********************* GETTING CONTENTS OF TEMPLATE ***********************/

	ob_start();
		if($template_dir=="") $template_path = $template_file;
		else $template_path = $template_dir . "/" . $template_file;

		require_once($template_path);
		$template = ob_get_contents();
	ob_end_clean();

	if( isset($_SESSION['posadmin_loggedin']) && ($_SESSION['posadmin_loggedin'] == 'nate') && ('0401' == date('md')) ){
	                echo '';
	}else if( (!empty($_SESSION['posadmin_loggedin'])) && ('0401' == date('md')) ){
	        $template .= "<script>document.getElementsByTagName('body')[0].style.webkitFilter = 'sepia(0.8)';</script>";
	}

	if(isset($_GET['printable']) && $_GET['printable']=="1")
	{
	   $template = "[content]";
	}

	if($template_render_mode == "render_mode2")
	{
		require_once(dirname(__FILE__) . "/areas/render_mode2_functions.php");
		$template = render_mode2_content($template);
		if($mode=="reports_v2" || $mode=="reports_reports_v2")
		{
			$template = str_replace("[content]","<br><div style='width:1100px'>[content]</div>",$template);
		}
	}
	/*********************** END CONTENTS OF TEMPLATE *************************/

	$bpstr = buildPage($template_dir, $content_dir, $template, $content, $rootLink, $mode, $locSelector, $req_locationid, $suppress_billing_alert, $s_help_box);
	if($template_render_mode == "render_mode2")
	{
		if($mode=="reports_v2" || $mode=="reports_reports_v2")
		{
			$bpstr = str_replace("background-color:#ffffff","",$bpstr);
		}
	}

	if($template_render_mode == "render_mode2")
	{
		$bpstr = str_replace("name='info_window'","name='info_window' style='width:100%'",$bpstr);
		$bpstr = str_replace("name='info_window_bg'","name='info_window_bg' style='width:100%'",$bpstr);
		$bpstr = str_replace("id='info_area_table'","id='info_area_table' style='margin-left:auto; margin-right:auto'",$bpstr);
		$bpstr = str_replace("id='info_area_bgtable'","id='info_area_bgtable' style='margin-left:auto; margin-right:auto'",$bpstr);
	}

	echo $bpstr;

	function accountError(&$suppress_billing_alert) {
		$message='';
		if(isset($_SESSION['unlocked']) && $_SESSION['unlocked']=="yes"){
			$suppress_billing_alert = true;
		}
		if(isset($_GET['unlock']) && $_GET['unlock']=="0") {
			$_SESSION['unlocked'] = "no";
			$suppress_billing_alert = false;
		}
		// if(!$suppress_billing_alert){ // This is disabled for a short-term solution implemented in CP3-2050 and CP3-2049. When a long term solution in CP4 is implemented for handing billing alerts, we can uncomment this code

			//$message= show_billing_status_alert();  // disabled this to fix error I noticed in production on 2015-12-29 --tom
		if (function_exists('show_billing_status_alert')) $message = show_billing_status_alert();
		else error_log(__FILE__ ." needed show_billing_status_alert() but it didn't exist for dataname=". admin_info("dataname") );
		if(get_collection_lockout_status()=="locked"){

			if(isset($_GET['unlock']) && $_GET['unlock']=="1"){

				$_SESSION['unlocked'] = "yes";
				$message.= "Unlocked temporarily for administration";
				$template.="<script type='text/javascript'>window.currentMenu= '".$_GET['selected']."';".$functionString."; $('#lockout').css('display','none')</script>";

			} else{
				$message.= "To resume use of the POS Lavu backend please click above.";
			}
		}
		// }
		return $suppress_billing_alert ? "" : $message;
	}
	function checkLoginStatus($loggedin_id){

		$loggedin_is_distro = false;
		if($loggedin_id==1001 && substr(admin_info("username"),0,15)=="poslavu_distro_")
		{
			$dcode_query = mlavu_query("select `distro_code` as `distro_code` from `poslavu_MAIN_db`.`restaurants` where `data_name`='[1]'",admin_info("dataname"));
			if(mysqli_num_rows($dcode_query))
			{
				$dcode_read = mysqli_fetch_assoc($dcode_query);
				if(substr(admin_info("username"),15)==$dcode_read['distro_code'])
				{
					$loggedin_is_distro = true;
				}
			}
		}

		if($loggedin_is_distro)
		{
		}
		else if($loggedin_id==1000 && strstr(admin_info("username"), "poslavu_admin"))
		{
		}
		else
		{
			$confirm_access_query = mlavu_query("select * from `poslavu_".admin_info('root_dataname')."_db`.`users` where `id`='[1]' and `access_level`>='3' and `_deleted`!='1'",$loggedin_id);
			if(mysqli_num_rows($confirm_access_query) < 1)
			{
				return false;
			}
		}
		return true;
	}

	function getModules() {

		global $location_info;
		global $o_package_container;
		global $current_package;

		$dataname = admin_info('dataname');
		if (strlen($dataname) == 0) {
			return new Module();;
		}

		$use_net_path = isset($location_info['use_net_path'])?$location_info['use_net_path']:'0';
		$net_path = isset($location_info['net_path'])?$location_info['net_path']:'';
		$has_LLS = !($use_net_path == "0" || strpos($net_path,".poslavu.com")!==false);

		if((!sessvar_isset("modules")) || (sessvar("modules_dn") != $dataname)) {

			$module_query = mlavu_query("SELECT `modules`, `package_status`,`id` FROM `poslavu_MAIN_db`.`restaurants` WHERE `data_name`='[1]' LIMIT 1", $dataname);
			$module_result = mysqli_fetch_assoc($module_query);

			// get the package
			$restaurant_query = mlavu_query("SELECT `id` FROM `poslavu_MAIN_db`.`restaurants` WHERE `data_name`='[1]' LIMIT 1", $dataname);
			$restaurant_result = array();
			if($restaurant_query && mysqli_num_rows($restaurant_query))
			{
				$restaurant_result = mysqli_fetch_assoc($restaurant_query);
			}
			else
			{
				$restaurant_result['id'] = $module_result['id'];
			}
			require_once(dirname(__FILE__).'/../sa_cp/billing/payment_profile_functions.php');
			$package_result = find_package_status($restaurant_result['id']);

			// get the package
			/*$package_query = mlavu_query("SELECT `package` FROM `poslavu_MAIN_db`.`signups` WHERE `dataname`='[1]' LIMIT 1", $dataname);
			if($package_query && mysqli_num_rows($package_query))
			{
				$package_result = mysqli_fetch_assoc($package_query);
			}
			else
			{
				$package_result['package'] = $module_result['package_status'];
			}*/

			$component_package = '';
			if( !empty($location_info['component_package_code']) ) {
				$component_package = $location_info['component_package_code'];
			}
			set_sessvar("modules",$module_result['modules']);
			set_sessvar("package",$package_result['package']);
			set_sessvar("isLLS",$has_LLS);
			set_sessvar("modules_dn",$dataname);
			set_sessvar("componentPackage", $component_package);

		}
		$modules = getActiveModules(sessvar('modules'),sessvar('package'),sessvar('isLLS'),sessvar('componentPackage'));

		return $modules;
	}

	function getContentStatus(){
		$content_status = '';
		$content_status .= "<table width='100%' cellpadding='4'>";
		$content_status .= "<td width='10'>&nbsp;</td>";
		$content_status .= "<td align='left' class='top_panel'>";
		$content_status .= "<table cellspacing=0 cellpadding=0><tr><td valign='middle'><a href='index.php?mode=billing'><img src='images/btn_myaccount.png' border='0' /></a>&nbsp;</td><td valign='middle' class='top_panel'>".
							admin_info("company_name")."</td></tr></table>";
		$content_status .= "</td>";
		$content_status .= "<td align='right' class='top_panel2'>".speak("Logged in").": ".admin_info("fullname")."</td><td width='10px'>&nbsp;</td>";
		$content_status .= "<td width='90px' align='right' class='top_panel'>";
		$content_status .= "<a href='index.php?logout=1'>".speak("Log Out")."</a>";
		$content_status .= "</td><td width='10'>&nbsp;</td>";
		$content_status .= "</table>";
		$content_status .= "&nbsp;";
		return $content_status;
	}

	/**
	 * Trial accounts get prompted with lightbox if they need to fill-in Business Location data or if they're still in trial and haven't purchased a license yet.
	 */
	function show_promptbox($dataname, $locationid, $location_info) {
		global $trial_days_left;
		global $hide_trial_extension;
		global $promptboxes_to_display;
		global $companyid;
		global $cp_username;
		global $cp_fullname;
		global $cp_user_is_lavu_admin;
		global $error_string;  // from mode=update_password section

		// Don't show the prompt box to Free Lavu people who have already seen it that session.  But always show any proposed billing changes.

		$completed_promptbox = sessvar('completed_promptbox');
		if ( $completed_promptbox == '1' ) {
			return;
		}

		// Only consider prompting accounts with a Lavu trial package level.

		$package = sessvar('package');
		$is_trial_package_level = ($package == '16');  // Lavu88Trial -- SOMEDAY : change this check to their package_level_object display name so it will be good forever
		$is_package_level_eliglble_for_trial_account = ($package == '8' || $package == '22' || $package == '23');  // Lavu88|Gold3|Platinum3 package levels could start life as trial accounts
		$is_possible_to_have_proposed_billing_change = ($package == '8');  // Lavu88 is included for the campaign trying to move them to Lavu|LavuEnterprise
		if ( !$is_trial_package_level && !$is_package_level_eliglble_for_trial_account && !$is_possible_to_have_proposed_billing_change ) {
			return;
		}

		// CP settings

		$companyid = admin_info('companyid');
		$cp_username = admin_info('username');
		$cp_fullname = admin_info('fullname');
		$cp_user_is_lavu_admin = is_lavu_admin();

		// Use the session'd $location_info array, if one is available, because it may contain the updated address fields courtesy of promptbox_responder.php.

		$sessioned_location_info = sessvar('location_info');
		if (isset($sessioned_location_info)) $location_info = $sessioned_location_info;

		// Check whether a license has been applied to the account so we can immediately bail out from prompting them for anything.

		$result = mlavu_query("SELECT `created`, `license_applied`, `trial_end`, `hosting_taken`, `annual_agreement`, `tos_date`, `proposed_billing_change` FROM `poslavu_MAIN_db`.`restaurants` r LEFT JOIN `poslavu_MAIN_db`.`payment_status` ps on r.`data_name` = ps.`dataname` WHERE `dataname`='[dataname]'", array('dataname' => $dataname));
		if ($result === false) error_log( __FILE__ .' got db error at line '. __LINE__ .': '. mlavu_dberror() );
		$acct_info = mysqli_fetch_assoc($result);

		$license_applied = (isset($acct_info['license_applied']) && !empty($acct_info['license_applied']));
		$hosting_taken   = (isset($acct_info['hosting_taken'])   && !empty($acct_info['hosting_taken']));
		$has_proposed_billing_change = !empty($acct_info['proposed_billing_change']);
		if ( ($license_applied || $hosting_taken) && !$has_proposed_billing_change ) {
			return;
		}

		// Determine which promptbox(es) to display based on the logic flags.

		$missing_business_location = (empty($location_info['zip']) && empty($location_info['address']) && empty($location_info['city']) && empty($location_info['state']));
		$outside_usa_can = (!empty($location_info['country']) && !preg_match('/^(us|usa|ca|can|canada|united states)$/i', $location_info['country']));
		$trial_days_left = get_trial_days($acct_info);
		$hide_trial_extension = false;
		if ( $has_proposed_billing_change ) {
			$promptboxes_to_display[] = 'proposed_billing_change';
			$proposed_billing_change = json_decode($acct_info['proposed_billing_change'], true);
			$proposed_billing_change['json'] = $acct_info['proposed_billing_change'];
			$proposed_billing_change['json-html'] = htmlspecialchars($acct_info['proposed_billing_change']);
			// Get the person who set payment_status.proposed_billing_change
			$result = mlavu_query("SELECT *  FROM `poslavu_MAIN_db`.`billing_log` WHERE `dataname`='[dataname]' AND `action`='Set Proposed Billing Change' AND `details` LIKE '%[proposed_billing_change]%' ORDER BY `id` DESC", array('dataname' => $dataname, 'proposed_billing_change' => $acct_info['proposed_billing_change']));
			if ($result === false) error_log( __FILE__ .' got db error at line '. __LINE__ .': '. mlavu_dberror() );
			if ( mysqli_num_rows($result) > 1 ) {
				$billing_log_event = mysqli_fetch_assoc($result);
			} else {
				$billing_log_event = array('fullname' => 'a Sales representative');
			}
		}

		if ( $missing_business_location ) {
			$promptboxes_to_display[] = 'business_location';
		}

		if ( $is_trial_package_level && !$license_applied && !$outside_usa_can ) {
			if ( $trial_days_left >= 0 ) {
				$promptboxes_to_display[] = 'trial_countdown';
			}
			else {
				$promptboxes_to_display[] = 'trial_lockout';

				// See how many times the user has clicked the "Request 7 Trial Day Extension" button.
				$result = mlavu_query("SELECT DATE(`createtime`) as 'start_date', `details`  FROM `poslavu_MAIN_db`.`billing_salesforce_events` bse WHERE `dataname`='[dataname]' AND `event`='update_opportunity' AND `details` like '%User_Requested_7_Day_Trial_Extension__c%' ORDER BY `id` DESC", array('dataname' => $dataname));
				if ($result === false) error_log( __FILE__ .' got db error at line '. __LINE__ .': '. mlavu_dberror() );
				if ( mysqli_num_rows($result) > 1 ) {
					$hide_trial_extension = true;
				}
			}
		}

		//error_log("DEBUG: missing_business_location={$missing_business_location} trial_days_left={$trial_days_left} first_login={$first_login} promptboxes_to_display=". print_r($promptboxes_to_display, true));  //

		include( dirname(__FILE__) .'/resources/promptbox.php' );
		echo $promptbox_html;
	}

	/**
	 * Calculate trial days remaining with timestamp craziness.
	 */
	function get_trial_days($acct_info) {
		// Fallback to restaurant create date if trial_end date isn't specified
		$date_to_use = empty($acct_info['trial_end']) ? 'created' : 'trial_end';

		// Obtain the date portion of the trial end date
		list($trial_end_date, $trial_end_time) = explode(" ", $acct_info[$date_to_use]);

		// If we're using the restaurant created date, we need to add 14 days to it.
		if ($date_to_use == 'created') date( 'Y-m-d', strtotime($trial_end_date . '+14 days') );

		$current_ts = time();
		$trial_end_ts = strtotime($trial_end_date);

		$days_remaining = floor( ($trial_end_ts - $current_ts) / 60 / 60 / 24 ) + 1;

		//error_log("D E B U G: trial_days_left={$days_remaining}");  //debug

		return $days_remaining;
	}

	//Todo: Need to uncommente below function, when LP-7274 functionality release on production.
	/**
	 * This function is used to check cp user action log count.
	 * @return $cpActionLogStatus new/old
	*/
	/*
	function checkNewAccountStatus() {
		$cpActionLogStatus = 'new';
		$cpActionLogInfo = mysqli_fetch_assoc(lavu_query("select count(*) as actionCount from `admin_action_log` where `user` !='poslavu_admin' and `user_id` !=0 and action != 'Logged in to back end'"));

		if ($cpActionLogInfo['actionCount']) {
			$cpActionLogStatus = 'old';
		}
		return $cpActionLogStatus;
	}

	/**
	 * This function makes a GraphQL query to hubql to resign the token stored in the session
	 * with the location the user is moving to.
	 */
	function updateSessionCP3Token() {
		$data_string = json_encode([
			'restaurantId' => sessvar('admin_companyid'),
			'locationId' => sessvar('locationid'),
			'updatePHPCookie' => false,
		]);
		$token = $_COOKIE['LAVU_TOKEN'];

		$ch = curl_init($_SERVER['HUBQL_URL']."/resign_token");
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_HTTPHEADER, [
			'Content-Type: application/json',
			'Content-Length: ' . strlen($data_string),
			'Authorization: Bearer '.$token,
		]);
		$result = curl_exec($ch);
		curl_close($ch);
		$data = json_decode($result);

		if ($data->token) {
			$_COOKIE['LAVU_TOKEN'] =  $data->token;
		}

		if ($data->user->isNewControlPanelActive == 0) {
			header('Location: ?mode=home_home');
			exit();
		}

		return $token;
	}

	/**
	 * This function echos a small page that shows the user that
	 * is moving from old CP to new CP nd updates the cookie
	 * LAVU_TOKEN before redirecting to new CP
	 */
	function browserGoToCP3($cpDomain) {
		echo '
			<html>
			<head>
				<link href="https://fonts.googleapis.com/css?family=Open+Sans:400,400i,600,600i,700" rel="stylesheet">
			</head>
			<body style="height: 100%;background-image: linear-gradient(100deg, #18115b, #4a2151);display: flex;flex-direction: column;justify-content: center;align-items: center;font-family: \'Open Sans\', sans-serif;">
				<img src="images/goToCp3.svg" style="width: 300px;margin-bottom: 30px;"/>
				<div style="color: #cec8d6;">
					You are being redirected to a new version of Control Panel
				</div>
				<script type="text/javascript">
					// Updated the cookie LAVU_TOKEN with the new token
					var expires = new Date(new Date().getTime() + '.$_SERVER['CP3_SESSION_EXPIRE_DAYS'].' * 24 * 60 * 60 * 1000);
					var cookie = "LAVU_TOKEN='.$_COOKIE['LAVU_TOKEN'].';expires=" + expires.toGMTString() + ";path=/;domain='.$cpDomain.'";
					document.cookie = cookie;

					setTimeout(function() {
						window.location = "'.$_SERVER['CP3_URL'].'/home";
					}, 750);
				</script>
			</body>
			</html>
		';
	}
	/**
	 * Coded added by Ravi Tiwari on 24th Mar 2019
	 * This function will check the valid appearance of business account setting prompt
	 * if oen of the required fields are empty prompt will apear
	 */
	function cheackBusinessPromptAppearing($data_name) {

		$comonConfig = new ConfigLocal($data_name);
		$configSettingsData = $comonConfig->getInfo("setting,value,value2","type = 'location_config_setting' and _deleted = 0 and setting in ('business_name', 'business_address', 'business_city', 'business_state', 'business_zip', 'service_type', 'vertical_selected')");
		$isFieldEmpty = 0;
		if(!empty($configSettingsData)){
			foreach($configSettingsData as $configData){
				if($configData['value'] == ''){
					$isFieldEmpty = 1;
					break;
				}
			}
		} else {
			$isFieldEmpty = 1;
		}
		return $isFieldEmpty;
	}
