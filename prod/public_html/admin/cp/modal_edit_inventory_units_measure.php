<?php
require_once(dirname(__FILE__)."/resources/core_functions.php");
$data_name = $_GET['dn'];
$modal_number = $_GET['modal_number'];
require_once(resource_path()."/lavuquery.php");
require_once(resource_path()."/action_tracker.php");
require_once(resource_path()."/chain_functions.php");

session_start($_GET['session_id']);
// $req_schm = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] != 'off') ? 'https' : 'http';
// $styleSheetPath = $req_schm. "://" . $_SERVER['HTTP_HOST'] . "/cp/styles/styles.css";
// $styleSheetPath = "/styles/styles.css";
$display = "<!DOCTYPE html> ";
$display .= "<html>";
$display .= "<head>";
$display .= "<meta http-equiv=\"Content-Type\" content=\"text/html;charset=ISO-8859-1\">";
$display .= "<link rel='stylesheet' type='text/css' href='styles/styles.css'>";
$display .= "<script type=\"text/javascript\" src=\"/cp/scripts/jquery.min.1.8.3.js\"></script>
				<link href=\"/cp/css/select2-4.0.3.min.css\" rel=\"stylesheet\" />
				<script src=\"/cp/scripts/select2-4.0.3.min.js\"></script>";

$display .= "</head>";
$display .= "<body>";

$display .= "<div id='inventory_units_measure_page_content'>";

    function units_measure_crud($value){
        $unitID = explode("|", $value)[0];
        $categoryID = explode("|", $value)[2];
        $id = explode("|", $value)[3];

        if(isset($id) && $id != "deleted")
        {
            $scheck = lavu_query("SELECT * from `inventory_unit` where `id` = '[1]' and `_deleted` != '1'", $id);

            if (@mysqli_num_rows($scheck)) {
                $_deleted = explode("|", $value)[4];
                if(isset($_deleted) && $_deleted == "deleted")
                {
                    lavu_query("UPDATE `inventory_unit` SET `_deleted` = '[1]' WHERE `id` = '[2]'", 1,$id);
                }
                else{
                    lavu_query("UPDATE `inventory_unit` SET `unitID` = '[1]', `categoryID` = '[2]'  WHERE `id` = '[3]' ", $unitID, $categoryID, $id);
                }
            }
        }
        else{
            if(!isset($id) && $id != "deleted")
            {
                $scheck = lavu_query("SELECT `id`,`_deleted` from `inventory_unit` where `unitID` = '[1]'", $unitID);

                if (@mysqli_num_rows($scheck)) {
                    $del_data = mysqli_fetch_assoc($scheck);
                    if($del_data['_deleted'] == '1')
                    {
                        lavu_query("UPDATE `inventory_unit` SET `_deleted` = '[1]' WHERE `id` = '[2]'", 0,$del_data['id']);
                    }
                }
                else{
                    lavu_query("INSERT INTO `inventory_unit` (`unitID`,`categoryID`) VALUES ('[1]','[2]')", $unitID, $categoryID);
                }
            }

        }

    }

    if (isset($_POST['posted'])) {
        foreach ($_POST['weight_unit'] as $value) {
            units_measure_crud($value);
        }

        foreach ($_POST['volume_unit'] as $value) {
            units_measure_crud($value);
        }

        // custom units
        if(isset($_POST['custom_unit']))
        {
            $custom_units = $_POST['custom_unit'];
            $custom_unit_names = $_POST['custom_unit_name'];
            $custom_unit_symbols = $_POST['custom_unit_symbol'];
            $custom_unit_cats = $_POST['custom_unit_cat'];
            $custom_unit_conversions = $_POST['custom_unit_conversion'];
            $custom_cnt =  count($custom_units);

            for ($i=0; $i<=count($custom_units); $i++) {

                $del_str = end(explode("|",$custom_units[$i]));

                if(isset($del_str) && $del_str == "deleted"){
                    $str = explode("|",$custom_units[$i]);

                    $name = $str[0];
                    $categoryID = 3;//$str[1];

                    if(count($str) == 7){
                        $id = $str[2];
                        $conversion = $str[3];
                        $unitID = '0';//$str[4];
                    }
                    else
                    {
                        $conversion = $str[2];
                        $unitID = '0';//$str[3];
                    }

                }
                else{
                    $id = $_POST['custom_unit_id'][$i];//explode("|",$custom_unit_cats[$i])[1];
                    $conversion = $custom_unit_conversions[$i];
                    $name = $custom_unit_names[$i];
                    $symbol = ($custom_unit_symbols[$i] != "") ? $custom_unit_symbols[$i] : $name;
                    $unitID = '0';//$custom_units[$i];
                    $categoryID = 3;//explode("|",$custom_unit_cats[$i])[0];
                    $symbol = strtolower($symbol);
                }
                if($name != "" && $conversion != "")
                {
                    $customUnits = array();
                    $customUnits[$symbol]['id'] = $id;
                    $customUnits[$symbol]['name'] = $name;
                    $customUnits[$symbol]['symbol'] = $symbol;
                    $customUnits[$symbol]['categoryID'] = $categoryID;
                    $customUnits[$symbol]['unitID'] = $unitID;
                    $customUnits[$symbol]['conversion'] = $conversion;
                    $customUnits[$symbol]['_deleted'] = 0;

                    if(isset($id) && $id != "")
                    {
                        $scheck = lavu_query("SELECT * from `custom_units_measure` where `id` = '[1]' and `_deleted` != '1'", $id);

                        if (@mysqli_num_rows($scheck)) {

                            if(isset($del_str) && $del_str == "deleted")
                            {
                                lavu_query("UPDATE `custom_units_measure` SET `_deleted` = '[1]' WHERE `id` = '[2]'", 1,$id);
                                lavu_query("UPDATE `inventory_unit` SET `_deleted` = '[1]' WHERE `unitID` = '[2]'", 1,$id);
                            }
                            else{
                                $symbol = preg_replace("/[^a-z\\d]/i", '', $symbol);
                                //lavu_query("UPDATE `custom_units_measure` SET `conversion` = '[1]', `name` = '[2]', `symbol` = '[3]', `unitID` = '[4]', `categoryID` = '[5]'  WHERE `id` = '[6]' ", $conversion, $name, $symbol, $unitID, $categoryID, $id);
                            }
                        }
                    }
                    else{
                        if(!isset($id) && $del_str != "deleted")
                        {
                            $scheck = lavu_query("SELECT `id`,`_deleted` from `custom_units_measure` where `name` = '[1]' and `symbol` = '[2]' and `categoryID` = '[3]'", $name, $symbol, $categoryID);

                            if (@mysqli_num_rows($scheck)) {
                                $del_data = mysqli_fetch_assoc($scheck);
                                if($del_data['_deleted'] == '1')
                                {
                                    //Allow Deletion of units, simplest case.  We will only delete that unit from the location database performing the delete.
                                    lavu_query("UPDATE `custom_units_measure` SET `_deleted` = '[1]' WHERE `id` = '[2]'", 0,$del_data['id']);
                                    lavu_query("UPDATE `inventory_unit` SET `_deleted` = '[1]' WHERE `unitID` = '[2]'", 0,$del_data['id']);
                                }
                            }
                            else {
                                $symbol = preg_replace("/[^a-z\\d]/i", '', $symbol);

                                //initial insert
                                lavu_query("INSERT INTO `custom_units_measure` (`conversion`, `name`, `symbol`, `unitID`, `categoryID`) VALUES ('[1]','[2]','[3]','[4]','[5]')", $conversion, $name, $symbol, $unitID, $categoryID);
                                $customId = lavu_insert_id();
                                //Insert inventory_unit
                                lavu_query("INSERT INTO `inventory_unit` (`unitID`,`categoryID`) VALUES ('[1]', '[2]')", $customId, 3);

                                updateChainLocationDatabases (sessvar('admin_chain_id'), $customUnits);

                            }
                        }

                    }
                }
                unset($id);
            }
        }

    }


    $lib_result = mlavu_query("SELECT * from `standard_units_measure` ORDER BY name ASC");

    $weight_lib_values = array();
    $volume_lib_values = array();
    $unitid_lib_values = array();
    $unit_category_array = array();

    while($lib_info = mysqli_fetch_assoc($lib_result)) {

        $unitid_lib_values[$lib_info['id']] = array('cat_id' => $lib_info['categoryID'], 'symbol' => $lib_info['symbol'], 'name' => $lib_info['name']);

        if($lib_info['categoryID'] == "1")
        {
            $weight_lib_values[$lib_info['id']."|".$lib_info['symbol']."|".$lib_info['categoryID']] = $lib_info['name'];
        }
        else if($lib_info['categoryID'] == "2")
        {
            $volume_lib_values[$lib_info['id']."|".$lib_info['symbol']."|".$lib_info['categoryID']] = $lib_info['name'];
        }
    }

    $display .= "<p align='center'>".speak("Add or Remove Inventory Units of Measure").".</p>";

    $scheck = lavu_query("SELECT * from `unit_category` where `_deleted` != '[1]' ", 1);
    while($unit_category_result = mysqli_fetch_assoc($scheck)){

        $unit_category_array[$unit_category_result['id']] =  $unit_category_result['name'];
    }
    $scheck = lavu_query("SELECT iu.`id`,iu.`unitID`,iu.`categoryID` FROM `inventory_unit` iu LEFT JOIN `poslavu_MAIN_db`.`standard_units_measure` su ON iu.`unitID` = su.`id`  WHERE `_deleted` != '[1]' ORDER BY name ASC ", 1);

    $weight_units_row = "";
    $vol_units_row = "";
    $i=1;
    $j=1;
    $totalcount_weight=0;
    $totalcount_volume=0;
    $inUseQuery = lavu_query("SELECT DISTINCT salesUnitID, recentPurchaseUnitID FROM inventory_items;");
    if (@mysqli_num_rows($inUseQuery)) {
        while($inUseResult = mysqli_fetch_assoc($inUseQuery)){
            $useUnits[$inUseResult['salesUnitID']] = $inUseResult['salesUnitID'];
            $useUnits[$inUseResult['recentPurchaseUnitID']] = $inUseResult['recentPurchaseUnitID'];
        }
    }

    if (@mysqli_num_rows($scheck)) {
        while($loc_units_info = mysqli_fetch_assoc($scheck)){
            if($loc_units_info['categoryID'] == "1")
            {
                $inUseUnit = in_array($loc_units_info['unitID'], $useUnits) ? true : false;
                $totalcount_weight++;
                $weight_select_code = '<select name="weight_unit[]" id="weight_unit_'.$i.'" style="color:#1c591c;width:115px;" onChange="populateSymbol('.$unitid_lib_values[$loc_units_info['unitID']]['cat_id'].','.$i.',this.value)">';

                foreach($unitid_lib_values as $key => $value){
                    $option_key = $key."|".$value['symbol']."|".$value['cat_id']."|".$loc_units_info['id'];
                    if($value['cat_id'] == '1')
                    {
                        if($loc_units_info['unitID'] == $key)
                        {
                            $weight_select_code .= '<option value="'.$option_key.'" selected>'.$value['name'].'</option>';
                        }
                        else{
                            $weight_select_code .= '<option value="'.$option_key.'">'.$value['name'].'</option>';
                        }
                    }
                }

                $weight_select_code .= '</select>';

                $weight_units_row .= '<tr class="_cls_row_">
                                <td>'.$weight_select_code.'</td>
                                <td><input alt="symbol" size="10" id="weight_unit_symbol_'.$i.'" title="symbol" readOnly="true" type="text" value="'.$unitid_lib_values[$loc_units_info['unitID']]['symbol'].'" style="color: #1c591c;" >
                                <input name="weight_unit_usage" id="weight_unit_usage_' . $i . '" type="hidden" readonly value="' . $inUseUnit . '"></td>
                                <td>&nbsp;<a class="closeBtn" onclick="click_delete('.$unitid_lib_values[$loc_units_info['unitID']]['cat_id'].','.$i.', this)">X</a></td>
                            </tr>';
                unset($unitid_lib_values[$loc_units_info['unitID']]);
                $i++;
            }
            else if($loc_units_info['categoryID'] == "2")
            {
                $inUseUnit = in_array($loc_units_info['unitID'], $useUnits) ? true : false;
                $totalcount_volume++;
                $volume_select_code = '<select name="volume_unit[]" id="volume_unit_'.$j.'" style="color:#1c591c;width:115px;" onChange="populateSymbol('.$unitid_lib_values[$loc_units_info['unitID']]['cat_id'].','.$j.',this.value)">';

                foreach($unitid_lib_values as $key => $value){

                    $option_key = $key."|".$value['symbol']."|".$value['cat_id']."|".$loc_units_info['id'];
                    if($value['cat_id'] == '2')
                    {
                        if($loc_units_info['unitID'] == $key)
                        {
                            $volume_select_code .= '<option value="'.$option_key.'" selected>'.$value['name'].'</option>';
                        }
                        else{
                            $volume_select_code .= '<option value="'.$option_key.'">'.$value['name'].'</option>';
                        }
                    }
                }
                $volume_select_code .= '</select>';

                $vol_units_row .='<tr class="_cls_row_">
                            <td>'.$volume_select_code.'</td>
                            <td><input alt="symbol" title="symbol" id="volume_unit_symbol_'.$j.'" size="10" readOnly="true" type="text" value="'.$unitid_lib_values[$loc_units_info['unitID']]['symbol'].'" style="color: #1c591c; " >
                            <input name="volume_unit_usage" id="volume_unit_usage_' . $j . '" type="hidden" readonly value="' . $inUseUnit . '"></td>
                
                            <td>&nbsp;<a class="closeBtn" onclick="click_delete('.$unitid_lib_values[$loc_units_info['unitID']]['cat_id'].','.$j.', this)">X</a></td>
                        </tr>';
                unset($unitid_lib_values[$loc_units_info['unitID']]);
                $j++;
            }



        }
    } else {
        $weight_select_code = '<select name="weight_unit[]" id="weight_unit_'.$i.'" style="color:#1c591c" onChange="populateSymbol('.explode("|", array_keys($weight_lib_values)[0])[2].','.$i.',this.value)">';

        foreach($weight_lib_values as $key => $name){
            $weight_select_code .= '<option value="'.$key.'">'.$name.'</option>';
        }
        $weight_select_code .= '</select>';

        $weight_units_row .= '<tr class="_cls_row_">
                                <td>'.$weight_select_code.'</td>
                                <td><input alt="symbol" size="10" id="weight_unit_symbol_'.$i.'" title="symbol" readOnly="true" type="text" value="'.explode("|", array_keys($weight_lib_values)[0])[1].'" style="color: #1c591c;" ></td>
                                <td>&nbsp;<a class="closeBtn" onclick="click_delete('.explode("|", array_keys($weight_lib_values)[0])[2].','.$i.', this)">X</a></td>
                            </tr>';

        $volume_select_code = '<select name="volume_unit[]" id="volume_unit_'.$i.'" style="color:#1c591c" onChange="populateSymbol('.explode("|", array_keys($volume_lib_values)[0])[2].','.$i.',this.value)">';
        foreach($volume_lib_values as $key => $name){
            $volume_select_code .= '<option value="'.$key.'" >'.$name.'</option>';
        }
        $volume_select_code .= '</select>';

        $vol_units_row .='<tr class="_cls_row_">
                            <td>'.$volume_select_code.'</td>
                            <td><input alt="symbol" title="symbol" id="volume_unit_symbol_'.$i.'" size="10" readOnly="true" type="text" value="'.explode("|", array_keys($volume_lib_values)[0])[1].'" style="color: #1c591c; " ></td>
                                
                            <td>&nbsp;<a class="closeBtn" onclick="click_delete('.explode("|", array_keys($volume_lib_values)[0])[2].','.$i.', this)">X</a></td>
                        </tr>';
    }

    $field_code .=  '<div style="text-align: center; margin-top: 5px">WEIGHT</div><div id="weight_del_values"></div><table cellspacing="0" cellpadding="0" class="_cls_tbl_" id="weight_customFields">
                        <tbody>'.$weight_units_row.'</tbody>
                    </table>
                    <table border="0" class="addBtn" cellspacing="0" cellpadding="0" onclick="add_new_item('.explode("|", array_keys($weight_lib_values)[0])[2].')">
                        <tbody>
                    		<tr><td><input type="hidden" name="total_weight" id="total_weight" value="'.$totalcount_weight.'"></td></tr>
                            <tr>
                                <td id="tdButton" width="26" style="color:white; padding-left:25px; padding-top:4px" valign="top"><span class="addBtn"> + Add WEIGHT UNIT </span></td>
                            </tr>
                        </tbody>
                    </table>';


    $field_code .=  '<div style="text-align: center; margin-top: 40px">VOLUME</div><table cellspacing="0" cellpadding="0" class="_cls_tbl_" id="volume_customFields">
                        <tbody>'.$vol_units_row.'</tbody>
                    </table>
                    <table class="addBtn" cellspacing="0" cellpadding="0" onclick="add_new_item('.explode("|", array_keys($volume_lib_values)[0])[2].')">
                        <tbody>
                    		<tr><td><input type="hidden" name="total_volume" id="total_volume" value="'.$totalcount_volume.'"></td></tr>
                            <tr>
                                <td id="tdButton" width="26" style="color:white; padding-left:25px; padding-top:4px" valign="top"><span class="addBtn"> + Add VOLUME UNIT </span></td>
                            </tr>
                        </tbody>
                    </table>';

            $scheck = lavu_query("SELECT `cUOM`.*, `iu`.`id` as invID FROM `inventory_unit` iu
            LEFT JOIN  `custom_units_measure` cUOM ON `cUOM`.`id` = `iu`.`unitID` WHERE  `iu`.`_deleted` != '[1]' AND `cUOM`.`_deleted` != '[1]' AND `iu`.`categoryID` = '[3]' ORDER BY name ASC", 1, 1, 3);

            $custom_units_row = "";
            $i=1;

            if (@mysqli_num_rows($scheck)) {
                while($loc_units_info = mysqli_fetch_assoc($scheck)){
                    $id = $loc_units_info['invID'];
/*
                    $custom_cat_select_code = '<select name="custom_unit_cat[]" id="custom_unit_cat_'.$i.'" style="color:#1c591c" onChange="populateUnit('.$i.',this.value)">';

                    foreach($unit_category_array as $key => $value){
                        $option_key = $key."|".$loc_units_info['id'];
                        if($loc_units_info['categoryID'] == $key)
                        {
                            $custom_cat_select_code .= '<option value="'.$option_key.'" selected>'.$value.'</option>';
                        }
                        else{
                            $custom_cat_select_code .= '<option value="'.$option_key.'">'.$value.'</option>';
                        }
                    }

                    $custom_cat_select_code .= '</select>';

                    $custom_unit_select_code = '<select name="custom_unit[]" id="custom_unit_'.$i.'" style="color:#1c591c">';

                    if($loc_units_info['categoryID'] == "3")
                    {
                        $custom_unit_select_code .= '<option value="0" selected>Each</option>';
                    }
                    else
                    {
                        foreach($unitid_lib_values as $key => $value){

                            if($value['cat_id'] == $loc_units_info['categoryID'])
                            {
                                if($loc_units_info['unitID'] == $key)
                                {
                                    $custom_unit_select_code .= '<option value="'.$key.'" selected>'.$value['name'].'</option>';
                                }
                                else{
                                    $custom_unit_select_code .= '<option value="'.$key.'">'.$value['name'].'</option>';
                                }
                            }
                        }
                    }

                    $custom_unit_select_code .= '</select>';
*/

                    $inUseUnit = in_array($id, $useUnits) ? true : false;
                    if ($inUseUnit) {
                        $custom_units_row .= '<tr class="_cls_row_">
                                    <td><input name="custom_unit_id[]" id="custom_unit_id_' . $i . '" type="hidden" readonly value="' . $loc_units_info['id'] . '">
                                    <input alt="name" title="name" name="custom_unit_name[]" class="custom_unit_name" id="custom_unit_name_' . $i . '" size="10" type="text" readonly style="color: #1c591c; " value="' . $loc_units_info['name'] . '"></td>
                                    <td><input name="custom_unit_usage" id="custom_unit_usage_' . $i . '" type="hidden" readonly value="true"></td>
                                    <td><input alt="symbol" title="symbol" name="custom_unit_symbol[]" class="custom_unit_symbol" id="custom_unit_symbol_' . $i . '" size="10" type="text" readonly style="color: #1c591c;" value="' . $loc_units_info['symbol'] . '"></td>
                                    <td><input alt="Unit Category" title="Unit Category" name="custom_unit_cat[]" id="custom_unit_cat_' . $i . '" size="10" type="text" readonly style="color: #1c591c;" value="Custom"></td>
                                    <!--<td>' . $custom_cat_select_code . '</td>-->
                                    <td> <input alt="conversion" size="10" class="custom_unit_conversion" id="custom_unit_conversion_' . $i . '" name="custom_unit_conversion[]" title="conversion" type="number" value="' . $loc_units_info['conversion'] . '" readonly style="color: #1c591c; width: 75px"></td>
                                    <!--<td>' . $custom_unit_select_code . '</td>-->
                                    <td><input alt="Custom Unit" title="Custom Unit" name="custom_unit[]" id="custom_unit_' . $i . '" size="10" type="text" readonly style="color: #1c591c;" value="Each"></td>
                                    <td>&nbsp;<a class="closeBtn" onclick="click_delete(3,'.$i.', this)">X</a></td>
                                </tr>';
                    }
                else if (strtolower($loc_units_info['name']) === "each") {
                    $custom_units_row .= '<tr class="_cls_row_">
                                                    <td><input name="custom_unit_id[]" id="custom_unit_id_'.$i.'" type="hidden" value="'.$loc_units_info['id'].'">
                                                     <td><input name="custom_unit_usage" id="custom_unit_usage_' . $i . '" type="hidden" readonly value="true">
                                                    <input alt="name" title="name" name="custom_unit_name[]" class="custom_unit_name" id="custom_unit_name_'.$i.'" size="10" type="text" readonly style="color: #1c591c; " value="'.$loc_units_info['name'].' "></td>
                                                    <td><input alt="symbol" title="symbol" name="custom_unit_symbol[]" class="custom_unit_symbol" id="custom_unit_symbol_'.$i.'" size="10" type="text" readonly style="color: #1c591c;" value="'.$loc_units_info['symbol'].'"></td>
                                                    <td><input alt="Unit Category" title="Unit Category" name="custom_unit_cat[]" id="custom_unit_cat_'.$i.'" size="10" type="text" readonly style="color: #1c591c;" value="Custom"></td>
                                                    <!--<td>'.$custom_cat_select_code.'</td>-->
                                                    <td> <input alt="conversion" size="10" class="custom_unit_conversion" id="custom_unit_conversion_'.$i.'" name="custom_unit_conversion[]" title="conversion" type="number" value="'.$loc_units_info['conversion'].'" readonly style="color: #1c591c; width: 75px"></td>
                                                    <!--<td>'.$custom_unit_select_code.'</td>-->
                                                    <td><input alt="Custom Unit" title="Custom Unit" name="custom_unit[]" id="custom_unit_'.$i.'" size="10" type="text" readonly style="color: #1c591c;" value="Each"></td>
                                                    <td>&nbsp;<a class="closeBtn" onclick="click_delete(3,'.$i.', this)">X</a></td>
                                                </tr>';
                }
               else {
                   $custom_units_row .= '<tr class="_cls_row_">
                                    <td><input name="custom_unit_id[]" id="custom_unit_id_' . $i . '" type="hidden" readonly value="' . $loc_units_info['id'] . '">
                                    <input alt="name" title="name" name="custom_unit_name[]" class="custom_unit_name" id="custom_unit_name_' . $i . '" size="10" type="text" readonly style="color: #1c591c; " value="' . $loc_units_info['name'] . '"></td>
                                    <td><input name="custom_unit_usage" id="custom_unit_usage_' . $i . '" type="hidden" readonly value="false"></td>
                                    <td><input alt="symbol" title="symbol" name="custom_unit_symbol[]" class="custom_unit_symbol" id="custom_unit_symbol_' . $i . '" size="10" type="text" readonly style="color: #1c591c;" value="' . $loc_units_info['symbol'] . '"></td>
                                    <td><input alt="Unit Category" title="Unit Category" name="custom_unit_cat[]" id="custom_unit_cat_' . $i . '" size="10" type="text" readonly style="color: #1c591c;" value="Custom"></td>
                                    <!--<td>' . $custom_cat_select_code . '</td>-->
                                    <td> <input alt="conversion" size="10" class="custom_unit_conversion" id="custom_unit_conversion_' . $i . '" name="custom_unit_conversion[]" title="conversion" type="number" value="' . $loc_units_info['conversion'] . '" readonly style="color: #1c591c; width: 75px"></td>
                                    <!--<td>' . $custom_unit_select_code . '</td>-->
                                    <td><input alt="Custom Unit" title="Custom Unit" name="custom_unit[]" id="custom_unit_' . $i . '" size="10" type="text" readonly style="color: #1c591c;" value="Each"></td>
                                    <td>&nbsp;<a class="closeBtn" onclick="click_delete(3,'.$i.', this)">X</a></td>
                                </tr>';
               }
               $i++;

                }
            }

            $field_code .=  '<div style="text-align: center; margin-top: 40px">CUSTOM UNITS</div>
                            <div id="custom_unit_error" style="color:#ff0000; margin-top: 5px"></div>
            <table cellspacing="0" cellpadding="0" class="_cls_tbl_" id="custom_unit_tbl">
                                <tbody>'.$custom_units_row.'</tbody>
                            </table>
                            <table class="addBtn" cellspacing="0" cellpadding="0" onclick="add_new_custom_item()">
                                <tbody>
                                    <tr>
                                        <td id="tdButton" width="26" style="color:white; padding-left:25px; padding-top:4px" valign="top"><span class="addBtn"> + Add CUSTOM UNIT </span></td>
                                    </tr>
                                </tbody>
                            </table>';

            $display .= "<table cellspacing='0' cellpadding='3' align='center'><tr><td align='center' style='border:2px solid #DDDDDD'>";
            $display .= "<form name='units_measure' id='units_measure' method='post' action=''>";
            $display .= "<input type='hidden' name='posted' value='1'>";
            $display .= $field_code;
            $display .= "&nbsp;<br><input type='button' id='saveBtn' value='".speak("Save")."' style='width:120px' onclick='add_unit_measures($modal_number)'>";
            $display .= "</form>";
            $display .= "</td></tr></table>";
            $display .= "</body>";
            $display .="</html>";
            echo $display;



			?>

<script language="javascript" type="text/javascript">

    function populateSymbol(cat,id,val) {
        if(cat == 1){
			var start=id+1;
			var valw=val.split("|");
			var countW = document.getElementById('total_weight').value;
			$inc=0;
			for(var cw=1; cw<=countW;cw++){ 
				if(cw!=id){
					if(document.getElementById("weight_unit_"+cw)){
				var e = document.getElementById("weight_unit_"+cw);
				var selected_val_w = e.options[e.selectedIndex].value;
				var unitid=selected_val_w.split("|");
				if(unitid[0] == valw[0]){
					$inc++;
				}
					}
				}
			}
			if($inc > 0){
                alert("Please select unique weight unit");
                var options = document.querySelectorAll('#weight_unit_'+id+' option');
                for(var i=0;i<options.length;i++)
                {
                    options[i].selected=options[i].defaultSelected;
                }
				return false;
			}
            $("#weight_unit_symbol_"+id).val(val.split("|")[1]);
        }else if(cat == 2){
        	var start=id+1;
			var valw=val.split("|");
			var countV = document.getElementById('total_volume').value;
			$inc=0;
			// loop start from next dropdown as of now
			for(var cw=1; cw<=countV;cw++){ 
				if(cw!=id){
					if(document.getElementById("volume_unit_"+cw)){
                        var e = document.getElementById("volume_unit_"+cw);
                        var selected_val_w = e.options[e.selectedIndex].value;
                        var unitid=selected_val_w.split("|");
                        if(unitid[0] == valw[0]){
                            $inc++;
                        }
					}
				}
			}
			if($inc > 0){
                var options = document.querySelectorAll('#volume_unit_'+id+' option');
                for(var i=0;i<options.length;i++)
                {
                    options[i].selected=options[i].defaultSelected;
                }
				alert("Please select unique volume unit");
				return false;
			}
            $("#volume_unit_symbol_"+id).val(val.split("|")[1]);

        }
    }

    function populateUnit(cnt,val) {
        if(val.split("|")[0] == "1") // weight
        {
            $("#custom_unit_"+cnt).find("option").remove();
            var items = <?= json_encode($weight_lib_values); ?>;
            for(var index in items) {
                $("#custom_unit_"+cnt)
                    .append($("<option></option>")
                        .attr("value",index.split("|")[0])
                        .text(items[index]));
            }
        }
        else if(val.split("|")[0] == "2") // volume
        {
            $("#custom_unit_"+cnt).find("option").remove();
            var items = <?= json_encode($volume_lib_values); ?>;
            for(var index in items) {
                $("#custom_unit_"+cnt)
                    .append($("<option></option>")
                        .attr("value",index.split("|")[0])
                        .text(items[index]));
            }
        }
        else // count
        {
            $("#custom_unit_"+cnt).find("option").remove();
            $("#custom_unit_"+cnt)
                .append($("<option></option>")
                    .attr("value",0)
                    .text("Each"));

        }
    }

    function add_new_item(cat)
    {
        var select_code = "<tr class='_cls_row_'>";

        if (cat == 1) {
            //var count = document.getElementById("weight_customFields").getElementsByTagName("tbody")[0].getElementsByTagName("tr").length;
        	var total_wight_units = <?php echo count($weight_lib_values);?>;
			var selectedunits = [];
			var count = document.getElementById('total_weight').value;
			for(var n=1;n<=count;n++){
				if(document.getElementById("weight_unit_"+n)){
				var select = document.getElementById("weight_unit_"+n);
				var selected_val = select.options[select.selectedIndex].value;
				selectedunits.push(selected_val);
				}
			}
            count++;
            if(selectedunits.length < total_wight_units){
				var items = <?php echo json_encode($weight_lib_values); ?>;
				select_code += '<td><select name="weight_unit[]" id="weight_unit_'+count+'" style="color:#1c591c;width:115px;" onChange="populateSymbol('+cat+','+count+',this.value)">';
				var s=0; 
				var k = "";
				for(var index in items) {
					var result = containsAny(index , selectedunits);
					if(result==null || result==""){
					select_code += '<option value="'+index+'">'+ items[index] +'</option>';
					if(s==0){ k=index; } s++; 
					}
					
				}

            //var items = <?= json_encode($weight_lib_values); ?>;
            /*select_code += '<td><select name="weight_unit[]" id="weight_unit_'+count+'" style="color:#1c591c" onChange="populateSymbol('+cat+','+count+',this.value)">';
            for(var index in items) {
                select_code += '<option value="'+index+'">'+ items[index] +'</option>';
            }*/
            select_code += '</select></td>';
            select_code += '<td><input alt="symbol" size="10" id="weight_unit_symbol_'+count+'" title="symbol" readOnly="true" type="text" value="'+k.split("|")[1]+'" style="color: #1c591c;" ></td>';
            select_code += '<td>&nbsp;<a class="closeBtn" onclick="click_delete('+cat+','+count+',this)">X</a></td></tr>';
            $("#weight_customFields").append(select_code);
            document.getElementById('total_weight').value=count;
			}else{
				alert("No more weight unit available");
				return false;
			}
        } else if(cat == 2) {
            //var count = document.getElementById("volume_customFields").getElementsByTagName("tbody")[0].getElementsByTagName("tr").length;
        	var selectedunits = [];
			var total_volume_units = <?php echo count($volume_lib_values);?>;
			var count = document.getElementById('total_volume').value;
			for(var n=1;n<=count;n++){
				if(document.getElementById("volume_unit_"+n)){
				var select = document.getElementById("volume_unit_"+n);
				var selected_val = select.options[select.selectedIndex].value;
				selectedunits.push(selected_val);
				}
			}
            count++;
            if(selectedunits.length < total_volume_units){
				var items = <?php echo json_encode($volume_lib_values); ?>;
				select_code += '<td><select name="volume_unit[]" id="volume_unit_'+count+'" style="color:#1c591c;width:115px;" onChange="populateSymbol('+cat+','+count+',this.value)">';
				var s=0; 
				var k = ""; 
				for(var index in items) {
					var result = containsAny(index , selectedunits);
					if(result==null || result==""){
					select_code += '<option value="'+index+'">'+ items[index] +'</option>';
					if(s==0){ k=index; } s++;
					}
				}
            //var items = <?= json_encode($volume_lib_values); ?>;
            /*select_code += '<td><select name="volume_unit[]" id="volume_unit_'+count+'" style="color:#1c591c" onChange="populateSymbol('+cat+','+count+',this.value)">';
            for(var index in items) {
                select_code += '<option value="'+index+'">'+ items[index] +'</option>';
            }*/
            select_code += '</select></td>';
            select_code += '<td><input alt="symbol" size="10" id="volume_unit_symbol_'+count+'" title="symbol" readOnly="true" type="text" value="'+k.split("|")[1]+'" style="color: #1c591c;" ></td>';
            select_code += '<td>&nbsp;<a class="closeBtn" onclick="click_delete('+cat+','+count+',this)">X</a></td></tr>';
            $("#volume_customFields").append(select_code);
            document.getElementById('total_volume').value=count;
			}else{
				alert("No more volume unit available");
				return false;
			}
        }

    }

	function containsAny(str, substrings) {
	    for (var i = 0; i != substrings.length; i++) {
	       var substring = substrings[i];
	       if (substring.indexOf(str) != - 1) {
	         return substring;
	       }
	    }
	    return null; 
	}

    function add_new_custom_item() {
        var count = document.getElementById('custom_unit_tbl').getElementsByTagName("tbody")[0].getElementsByTagName("tr").length;
        count++;

        var select_code = '<tr class="_cls_row_">';
        select_code += '<td><input alt="name" title="name" name="custom_unit_name[]" class="custom_unit_name" id="custom_unit_name_'+count+'" placeholder="Name" size="10" type="text" style="color: #1c591c; "></td>';
        select_code += '<td><input alt="usage" title="name" name="custom_unit_usage[]" class="custom_unit_usage" id="custom_unit_usage_'+count+'" placeholder="Name" size="10" type="hidden" style="color: #1c591c; "></td>';
        select_code += '<td><input alt="name" title="symbol" name="custom_unit_symbol[]" class="custom_unit_symbol" id="custom_unit_symbol_'+count+'" placeholder="Symbol" size="10" type="text" style="color: #1c591c;"></td>';
        //Commented because in beta v1 we are allowing only custom category

        //var items = <?= json_encode($unit_category_array); ?>;
        /*
        select_code += '<td><select name="custom_unit_cat[]" id="custom_unit_cat_'+count+'" style="color:#1c591c" onChange="populateUnit('+count+',this.value)">';
        for(var index in items) {
            select_code += '<option value="'+index+'">'+ items[index] +'</option>';
        }
        select_code += '</select></td>';
        */
        select_code += '<td><input alt="Unit Category" name="custom_unit_cat[]" size="10" id="custom_unit_cat_'+count+'" placeholder="Unit Category" title="Unit Category" type="text" value="Custom" readonly style="color: #1c591c;"></td>';
        select_code += '<td><input alt="conversion" name="custom_unit_conversion[]" size="10" class="custom_unit_conversion" id="custom_unit_conversion_'+count+'" placeholder="Conversion" title="conversion" type="number" min="1" style="color: #1c591c; width:75px;"></td>';

        //items = <?= json_encode($weight_lib_values); ?>;
        /*
        select_code += '<td><select name="custom_unit[]" id="custom_unit_'+count+'" style="color:#1c591c" >';
        for(var index in items) {
            select_code += '<option value="'+index.split("|")[0]+'">'+ items[index] +'</option>';
        };
        select_code += '</select></td>';
        */
        select_code += '<td><input alt="Custom Unit" name="custom_unit[]" size="10" id="custom_unit_'+count+'" placeholder="Custom Unit" title="Custom Unit" type="text" value="Each" readonly style="color: #1c591c;"></td>';
        select_code += '<td>&nbsp;<a class="closeBtn" onclick="click_delete(3,'+count+',this)">X</a></td></tr>';
        $("#custom_unit_tbl").append(select_code);
    }

    function deleteRow(btndel) {
        if (typeof(btndel) == "object") {
            $(btndel).closest("tr").remove();
            return true;
        } else {
            return false;
        }
    }
    function click_delete(cat, cnt, btndel)
    {
        var unit_value = "";
        if(cat == 1){
            if($("#weight_customFields tr").length > 1)
            {
                unit_value = $("#weight_unit_"+cnt).val();
                if($("#weight_unit_usage_"+cnt).val()){
                    alert("Unable to remove a unit that is currently in use.");
                    return;
                }
                if(deleteRow(btndel))
                {
                    var str = '<select name="weight_unit[]" style="display:none"><option value="'+unit_value+"|deleted"+'" selected></option></select>';
                    $("#weight_del_values").append(str);
                }
            }
            else
            {
                alert("Atleast one unit should be mandatory in Weight Category");
                return;
            }
        }
        else if(cat == 2){
            if($("#volume_customFields tr").length > 1)
            {
                unit_value = $("#volume_unit_"+cnt).val();
                if($("#volume_unit_usage_"+cnt).val()){
                    alert("Unable to remove a unit that is currently in use.");
                    return;
                }
                if(deleteRow(btndel))
                {
                    var str = '<select name="volume_unit[]" style="display:none"><option value="'+unit_value+"|deleted"+'" selected></option></select>';
                    $("#weight_del_values").append(str);
                }
            }
            else
            {
                alert("Atleast one unit should be mandatory in Volume Category");
                return;
            }
        }
        else if(cat == 3){
            unit_value = $("#custom_unit_name_"+cnt).val()+'|'+$("#custom_unit_symbol_"+cnt).val()+'|'+$("#custom_unit_id_"+cnt).val()+'|'+$("#custom_unit_cat_"+cnt).val()+'|'+$("#custom_unit_conversion_"+cnt).val()+'|'+$("#custom_unit_"+cnt).val();
            if($("#custom_unit_usage_"+cnt).val() == 'true'){
                alert("Unable to remove a unit that is currently in use.");
                return;
            }
            if(deleteRow(btndel))
            {
                /*
                var str = '<select name="custom_unit[]" style="display:none">';
                str += '<option value="'+unit_value+'|deleted" selected></option>';
                str += '</select>';
                */
                var str = '<input name="custom_unit[]" style="display:none" type="text" value="'+unit_value+'|deleted">';
                //str += '<select name="custom_unit_cat[]" style="display:none"><option value="'+$("#custom_unit_cat_"+cnt).val()+"></option></select>';

                $("#weight_del_values").append(str);
            }
        }
    }

    function check_custom_unit_quantity() {
        var str = '';
        var nameFlag = false;
        var symbols = ["kg", "lbs", "oz", "gal", "l", "g", "mg", "mL", "qt", "pt", "c", "fl oz", "tbs", "tsp", "hl", "each"];
        var bad_symbols = ["bbl", "cp", "cd", "mol"];
        $(".custom_unit_name").each(function() {
            var name = $(this).val();
            name = name.trim();
            if (name == "")
            {
                str += "<div style='margin-top:2px; margin-bottom:2px'>Please input custom unit name.</div>";
            }/* else if (name.toLowerCase() == 'each') {
                nameFlag = true;
                str += "<div style='margin-top:2px; margin-bottom:2px'>\"" + name + "\" Using as standard unit name, Please input different unit</div>";
            }*/
        });
        $(".custom_unit_symbol").each(function() {
            var symbol = $(this).val();
            symbol = symbol.trim();
            symbol = symbol.toString().toLowerCase();
            if (symbol != "")
            {
                if(symbols.includes(symbol)){
                    str += "<div style='margin-top:2px; margin-bottom:2px'>Duplicate Symbols are not allowed.</div>";
                }
                else if(bad_symbols.includes(symbol)){
                    str +=  "<div style='margin-top:2px; margin-bottom:2px'>Symbol is not allowed: ";
                        str+= symbol + "</div>";
                }
                else{
                    symbols.push(symbol);
                    var validSymbol = /^[a-z0-9]+$/i;
                    var validFirstSymbol = /^[a-z]$/i;
                    if ((!symbol[0].match(validFirstSymbol)) || (!symbol.match(validSymbol))) {
                        str += "<div style='margin-top:2px; margin-bottom:2px'>" + symbol + " Please input alphanumeric characters start with alpha only.</div>";
                    }
                }
                /*if(symbol.toLowerCase() == 'each' && !nameFlag) {
                    str += "<div style='margin-top:2px; margin-bottom:2px'>\"" + symbol + "\" Using as standard unit symbol, Please input different unit</div>";
                }*/
            } else {
                str += "<div style='margin-top:2px; margin-bottom:2px'>Please input alphanumeric characters start with alpha as Symbol.</div>";
            }

        });
        $(".custom_unit_conversion").each(function() {
            var num = Number($(this).val());
            if (num <= 0)
            {
                str += "<div style='margin-top:2px; margin-bottom:2px'>" + num + " is not valid custom unit conversion.</div>";
            }
        });

        if(str != "") {
            $('#custom_unit_error').html(str);
            return false;
        }
        return true;
    }


    function add_unit_measures(modal) {
        var check = check_custom_unit_quantity();
        if(check) {
            $('#units_measure').submit();
            window.parent.CloseModalPopUp(modal);
        }
    }

</script>
