<?php

require_once('/home/poslavu/public_html/admin/cp/reqserv/oapi/LavuOrder.php');
require_once('/home/poslavu/public_html/admin/cp/reqserv/oapi/PepperLavuOrder.php');

// Slurp in JSON from request var
$payloadJson = $_REQUEST['contents'];
$payload = json_decode($payloadJson);

if (empty($payload))
{

}
else
{
	// Needed for oapi so just stuff in _REQUEST
	if (empty($_REQUEST['restaurant_id'])) $_REQUEST['restaurant_id'] = $restid;
	if (empty($_REQUEST['loc_id'])) $_REQUEST['loc_id'] = $locationid;

	// Parse payload into a LavuOrder
	$src = $_REQUEST['src'];
	switch ($src)
	{
		case 'pepper':
			$lavuOrder = new PepperLavuOrder($_REQUEST, $payload);
			break;

		case 'bite':
		default:
			$lavuOrder = new LavuOrder($_REQUEST, $payload);
			break;
	}

	if ($lavuOrder->error)
	{
		$response = $lavuOrder->getError();
	}
	else
	{
		// Attempt to Insert the LavuOrder into the db
		$response = $lavuOrder->commit();
	}

	// Send API response
	if ($src == 'pepper') header('Content-Type: text/xml');
	echo $response;
}
