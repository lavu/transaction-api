<?php

	//This was set for LP-7251
	ini_set('memory_limit', '200M');

	if(!isset($cmd))
		exit();

	function microtime_float()
	{
		list($usec, $sec) = explode(" ", microtime());
		return ((float)$usec + (float)$sec);
	}

	//-------------------------------------------------------------------- Do not use DB for Last mapi REQ ------------------------------------------------------//
	$lldir1 = "/home/poslavu/logs/api";
	$lldir2 = $lldir1 . "/" . substr($dataname,0,1);
	$lldir3 = $lldir2 . "/" . $dataname;
	$llfname = "last_mapi_req";

	$host_parts = explode("api-",$_SERVER['HTTP_HOST']);
	if(count($host_parts)==2)
	{
		$host_parts = explode(".lavu.com",$host_parts[1]);
		if(count($host_parts)==2)
		{
			$host_part_str = trim($host_parts[0]);
			if(strlen($host_part_str) > 1 && ctype_alpha($host_part_str))
			{
				$llfname = "last_mapi_req_" . $host_part_str;
			}
		}
	}

	$llstartvalue = time() . "|processed|init";

	$tseting = false;

	$use_llfile = false;
	$llfile = $lldir3 . "/" . $llfname;
	if(!is_file($llfile))
	{
		if(!is_dir($lldir3))
		{
			if(!is_dir($lldir2))
			{
				if(!is_dir($lldir1))
				{
					mkdir($lldir1,0755);
				}
				mkdir($lldir2,0755);
			}
			mkdir($lldir3,0755);
		}
		if(is_dir($lldir3))
		{
			$lfp = fopen($llfile,"w");
			fwrite($lfp,$llstartvalue);
			fclose($lfp);
		}

		if(is_file($llfile))
		{
			$last_mapi_sync = time();
			$last_mapi_status = "processed";
		}
		else
		{
			echo genError(0,"Lockout Status Creation Failed.");
			exit();
		}
	}
	else
	{
		$lfp = fopen($llfile,"r");
		$fsize = filesize($llfile);
		$lfpstr = "";
		if ($fsize > 0) $lfpstr = fread($lfp, $fsize);
		fclose($lfp);

		$lfpstr_parts = explode("|",$lfpstr);
		if(count($lfpstr_parts) > 1 && is_numeric($lfpstr_parts[0]))
		{
			$last_mapi_sync = $lfpstr_parts[0] * 1;
			$last_mapi_status = trim($lfpstr_parts[1]);
		}
		else
		{
			$lfp = fopen($llfile,"w");
			fwrite($lfp,$llstartvalue);
			fclose($lfp);
			echo genError(0,"Lockout Status was Empty, Expected a Value and Saw Nothing.");
			exit();
		}
	}
	function update_last_mapi_value($status,$cmd,$cmd_table="",$cmd_duration="")
	{
		global $llfile;
		$llsetvalue = time() . "|" . $status . "|" . $cmd . "|" . $cmd_table . "|" . $cmd_duration;

		$lfp = fopen($llfile,"w");
		fwrite($lfp,$llsetvalue);
		fclose($lfp);
	}

	$has_pepper = stripos($rest_read['modules'], 'extensions.loyalty.+pepper') !== false;

	//-----------------------------------------------------------------------------------------------------------------------------------------------------------//

	if($last_mapi_status=="started")
		$mapi_script_active = true;
	else
		$mapi_script_active = false;
	if((time() - $last_mapi_sync <= 1150) && $mapi_script_active)
	{
		//quit();
		$is_valid ? $str = "<?xml version=\"1.0\" encoding=\"utf-8\"?>".PHP_EOL."<results>".PHP_EOL : $str = '';
		$str .= "<row><error>Already Executing Api Script</error><code>0</code></row>";
		$is_valid ? $str .= "</results>" : $str .= '';
		echo $str;
		exit();
	}
	else
	{
		update_last_mapi_value("started",$cmd);
		//lavu_query("update `config` set `value`='[1]', `value2`='started', `value3`='[3]' where `location`='[2]' and `setting`='last_mapi_req'",time(),$locationid,$cmd);
	}
	$last_mapi_start_seconds = microtime_float();
	
	$allow_json_format = array("menu_item_meta_data_link_details", "nutrition_info_link_details");
	
	$format = strtolower(trim($format));
	if(!in_array($cmd_table, $allow_json_format)) {
	    $format = 'xml';
	}
	
	$filter_required = true; // bit to determine a filter is needed in query
	$filter_deleted = false;
	$show_deleted_col = false;
	
	if($cmd_table=="menu_groups")
	{
		$auto_filter_column = "menu_id";
		$auto_filter_value = $menu_id;
		$filter_deleted = true;
	}
	else if($cmd_table=="menu_categories")
	{
		$auto_filter_column = "menu_id";
		$auto_filter_value = $menu_id;
		$filter_deleted = true;
	}
	else if($cmd_table=="menu_items")
	{
		$auto_filter_column = "menu_id";
		$auto_filter_value = $menu_id;
		$filter_deleted = true;
	}
	else if($cmd_table=="tables")
	{
		$auto_filter_column = "loc_id";
		$auto_filter_value = $locationid;
	}
	else if($cmd_table=="orders")
	{
		$auto_filter_column = "location_id";
		$auto_filter_value = $locationid;
	}
	else if($cmd_table=="order_contents")
	{
		$auto_filter_column = "loc_id";
		$auto_filter_value = $locationid;
	}
	else if($cmd_table=="order_payments")
	{
		$auto_filter_column = "loc_id";
		$auto_filter_value = $locationid;
	}
	else if($cmd_table=="locations")
	{
		$auto_filter_column = "id";
		$auto_filter_value = $locationid;
	}
	else if($cmd_table=="clock_punches")
	{
		$auto_filter_column = "location_id";
		$auto_filter_value = $locationid;
		$filter_deleted = true;
	}
	else if($cmd_table=="lk_karts")
	{
		$auto_filter_column = "locationid";
		$auto_filter_value = $locationid;
	}
	else if($cmd_table=="lk_waivers")
	{
		$auto_filter_column = "loc_id";
		$auto_filter_value = $locationid;
	}

	else if($cmd_table=="modifiers_used")
	{
		$auto_filter_column = "loc_id";
		$auto_filter_value = $locationid;
	}

	else if($cmd_table=="modifiers")
	{

		// do not need filter (no location id)
		$filter_required = false;
		$filter_deleted = true;
	}

	else if($cmd_table=="forced_modifiers")
	{

		// do not need filter (no location id)
		$filter_required = false;
		$filter_deleted = true;
	}

	else if($cmd_table=="ingredients")
	{
		$auto_filter_column = "loc_id";
		$auto_filter_value = $locationid;
		$filter_deleted = true;
	}
	else if($cmd_table=="ingredient_categories")
	{
		$auto_filter_column = "loc_id";
		$auto_filter_value = $locationid;
		$filter_deleted = true;
	}
	else if($cmd_table=="ingredient_usage")
	{
		$auto_filter_column = "loc_id";
		$auto_filter_value = $locationid;
	}
	else if($cmd_table=="users")
	{
		$auto_filter_column = "lavu_admin";
		$auto_filter_value = "0";
		$filter_deleted = true;
	}
	else if($cmd_table=="emp_classes")
	{
		$filter_required = false;
		$filter_deleted = true;
	}
	else if($cmd_table=="discount_types")
	{
		$filter_required = false;
		$filter_deleted = true;
	}
	else if($cmd_table=="discount_groups")
	{
		$filter_required = false;
		$filter_deleted = true;
	}
	else if($cmd_table=="order_checks")
	{
		$filter_required = true;
		$filter_deleted = false;
		$auto_filter_column = "loc_id";
		$auto_filter_value = $locationid;
	}
	else if($cmd_table == "meal_periods")
	{
		$filter_required = false;
		$filter_deleted = false;
	}
		else if($cmd_table == "meal_period_types")
	{
		$filter_required = false;
		$filter_deleted = false;
	}
		else if($cmd_table == "revenue_centers")
	{
		$filter_required = false;
		$filter_deleted = false;
	}
	else if($cmd_table=="med_customers")
	{
		$auto_filter_column = "locationid";
		$auto_filter_value = $locationid;
		$filter_required = true;
		$filter_deleted = false;
	}
	else if($cmd_table=="config")
	{
		$auto_filter_column = "location";
		$auto_filter_value = $locationid;
		$filter_required = true;
		$filter_deleted = true;
	}
	else if($cmd_table=="super_groups"){
		$filter_required = false;
		$filter_deleted = true;
	}
	else if($cmd_table=="forced_modifier_lists"){
		$filter_required = true;
		$filter_deleted = true;
		$auto_filter_column = "menu_id";
		$auto_filter_value = $menu_id;
	}
	else if($cmd_table=="forced_modifier_groups"){
		$filter_required = true;
		$filter_deleted = true;
		$auto_filter_column = "menu_id";
		$auto_filter_value = $menu_id;

	}
	else if($cmd_table=="modifier_categories"){
		$filter_required = true;
		$filter_deleted = true;
		$auto_filter_column = "menu_id";
		$auto_filter_value = $menu_id;
	}
	else if($cmd_table=="devices")
	{
		$filter_required = true;
		$filter_deleted = false;
		$auto_filter_column = "loc_id";
		$auto_filter_value = $locationid;
	}
	else if($cmd_table=="payment_types")
	{
		$filter_required = true;
		$filter_deleted = false;
		$auto_filter_column = "loc_id";
		$auto_filter_value = $locationid;
	}
	else if($cmd_table=="alternate_payment_totals")
	{
		$filter_required = true;
		$filter_deleted = false;
		$auto_filter_column = "loc_id";
		$auto_filter_value = $locationid;
	}
	else if($cmd_table=="tax_profiles")
	{
		$filter_required = true;
		$filter_deleted = false;
		$auto_filter_column = "loc_id";
		$auto_filter_value = $locationid;
		$show_deleted_col = true;
	}
	else if($cmd_table=="tax_rates")
	{
		$filter_required = true;
		$filter_deleted = false;
		$auto_filter_column = "loc_id";
		$auto_filter_value = $locationid;
		$show_deleted_col = true;
	} else if ($cmd_table == "lk_action_log"){
		$auto_filter_column="loc_id";
		$auto_filter_value=$locationid;
		$filter_required = true;
		$filter_deleted = true;
	} else if ($cmd_table == "lk_categories"){
		$auto_filter_column="locationid";
		$auto_filter_value=$locationid;
		$filter_required = true;
		$filter_deleted = true;
	} else if ($cmd_table == "loc_id"){
		$auto_filter_column="loc_id";
		$auto_filter_value=$locationid;
		$filter_required = true;
		$filter_deleted = true;
	} else if ($cmd_table == "lk_customers"){
		$auto_filter_column="locationid";
		$auto_filter_value=$locationid;
		$filter_required = true;
		$filter_deleted = true;
	} else if ($cmd_table == "lk_event_schedules"){
		$filter_required = false;
		$filter_deleted = true;
	} else if ($cmd_table == "lk_event_sequences"){
		$auto_filter_column="locationid";
		$auto_filter_value=$locationid;
		$filter_required = true;
		$filter_deleted = true;
	} else if ($cmd_table == "lk_event_types"){
		$auto_filter_column="locationid";
		$auto_filter_value=$locationid;
		$filter_required = true;
		$filter_deleted = true;
	} else if ($cmd_table == "lk_events"){
		$auto_filter_column="locationid";
		$auto_filter_value=$locationid;
		$filter_required = true;
		$filter_deleted = true;
	} else if ($cmd_table == "lk_group_events"){
		$auto_filter_column="loc_id";
		$auto_filter_value=$locationid;
		$filter_required = true;
		$filter_deleted = true;
	} else if ($cmd_table == "lk_karts"){
		$auto_filter_column="locationid";
		$auto_filter_value=$locationid;
		$filter_required = true;
		$filter_deleted = true;
	} else if ($cmd_table == "lk_laps"){
		$filter_required = false;
		$filter_deleted = true;
	} else if ($cmd_table == "lk_monitor"){
		$auto_filter_column="locationid";
		$auto_filter_value=$locationid;
		$filter_required = true;
		$filter_deleted = true;
	} else if ($cmd_table == "lk_parts"){
		$auto_filter_column="locationid";
		$auto_filter_value=$locationid;
		$filter_required = true;
		$filter_deleted = true;
	} else if ($cmd_table == "lk_parts_received"){
		$auto_filter_column="locationid";
		$auto_filter_value=$locationid;
		$filter_required = true;
		$filter_deleted = true;
	} else if ($cmd_table == "lk_process"){
		$filter_required = false;
		$filter_deleted = true;
	} else if ($cmd_table == "lk_race_log"){
		$auto_filter_column="loc_id";
		$auto_filter_value=$locationid;
		$filter_required = true;
		$filter_deleted = true;
	} else if ($cmd_table == "lk_race_results"){
		$filter_required = false;
		$filter_deleted = true;
	} else if ($cmd_table == "lk_rooms"){
		$auto_filter_column="locationid";
		$auto_filter_value=$locationid;
		$filter_required = true;
		$filter_deleted = true;
	} else if ($cmd_table == "lk_sales_reps"){
		$auto_filter_column="loc_id";
		$auto_filter_value=$locationid;
		$filter_required = true;
		$filter_deleted = true;
	} else if ($cmd_table == "lk_service"){
		$auto_filter_column="locationid";
		$auto_filter_value=$locationid;
		$filter_required = true;
		$filter_deleted = true;
	} else if ($cmd_table == "lk_service_complete"){
		$filter_required = false;
		$filter_deleted = true;
	} else if ($cmd_table == "lk_service_info"){
		$auto_filter_column="locationid";
		$auto_filter_value=$locationid;
		$filter_required = true;
		$filter_deleted = true;
	} else if ($cmd_table == "lk_service_types"){
		$auto_filter_column="locationid";
		$auto_filter_value=$locationid;
		$filter_required = true;
		$filter_deleted = true;
	} else if ($cmd_table == "lk_source_list"){
		$filter_required = false;
		$filter_deleted = true;
	} else if ($cmd_table == "lk_tracks"){
		$auto_filter_column="locationid";
		$auto_filter_value=$locationid;
		$filter_required = true;
		$filter_deleted = true;
	} else if ($cmd_table == "lk_waivers"){
		$auto_filter_column="loc_id";
		$auto_filter_value=$locationid;
		$filter_required = true;
		$filter_deleted = true;
	} else if ($cmd_table == "happyhours"){
		$filter_required = false;
		$filter_deleted = true;
	} else if ($cmd_table == "user_schedules"){
		$auto_filter_column="loc_id";
		$auto_filter_value=$locationid;
		$filter_required = false;
		$filter_deleted = false;
	}
	else if ($cmd_table == "menu_item_meta_data_link_details"){
	    $filter_deleted = true;
	    $filter_required = false;
	}
	else if ($cmd_table == "nutrition_info_link_details"){
	    $filter_deleted = true;
	    $filter_required = false;
	}
	
	$filter_deleted = $filter_deleted && ($show_deleted == "0");
	$table_aliases['order_payments'] = "cc_transactions";
	$table_aliases['order_checks'] = 'split_check_details';

	$allow = false;
	$allowed_columns=null;

	if( $integratorKey){

		$decoded= $json->decode($integrator_read['permissions']);
		if(isset( $decoded->$cmd_table)){
			$allow=true;
			$allowed_columns= $decoded->$cmd_table;
		}

	}else{

		$allowed_tables =
			array(
				'clock_punches',
				'config',
				'devices',
				'discount_groups',
				'discount_types',
				'emp_classes',
				'forced_modifier_groups',
				'forced_modifier_lists',
				'forced_modifiers',
				'happyhours',
				'ingredient_categories',
				'ingredient_usage',
				'ingredients',
				'lk_action_log',
				'lk_categories',
				'lk_checklist_catergories',
				'lk_customers',
				'lk_event_schedules',
				'lk_event_sequences',
				'lk_event_types',
				'lk_events',
				'lk_group_events',
				'lk_karts',
				'lk_laps',
				'lk_monitor',
				'lk_parts',
				'lk_parts_received',
				'lk_process',
				'lk_race_log',
				'lk_race_results',
				'lk_rooms',
				'lk_sales_reps',
				'lk_service',
				'lk_service_complete',
				'lk_service_info',
				'lk_service_types',
				'lk_source_list',
				'lk_tracks',
				'lk_waivers',
				'locations',
				'meal_period_types',
				'meal_periods',
				'med_customers',
				'menu_categories',
				'menu_groups',
				'menu_items',
			    	'menu_item_meta_data_link_details',
				'modifier_categories',
				'modifiers',
				'modifiers_used',
			    	'nutrition_info_link_details',
				'order_checks',
				'order_contents',
				'order_payments',
				'orders',
				'payment_types',
				'revenue_centers',
				'super_groups',
				'tables',
				'tax_profiles',
				'tax_rates',
				'users',
				'user_schedules'
			);
		for($i=0; $i<count($allowed_tables); $i++){
			if($allowed_tables[$i]==$cmd_table){
				$allow = true;
				if(isset($table_aliases[$cmd_table])){
					$cmd_table = $table_aliases[$cmd_table];
				}
			}
		}
	}

	$filter_out = Array(
		'api_key',
		'api_token',
		'force_remote',
		'ip_address',
		'lavu_admin',
		'license_exp',
		'MAC',
		'net_path',
		'net_path_print',
		'password',
		'PIN',
		'remote_ip',
		'rf_id',
		'UDID',
		'use_net_path',
		'UUID'
		);

	$conn = ConnectionHub::getConn('rest');

	if($allow)
	{

		if($filter_column!="" || $auto_filter_column!="" || !$filter_required)
		{

			$cmd_table = $conn->escapeString($cmd_table);//str_replace("`","",$cmd_table);
			$filter_column = $conn->escapeString($filter_column);//str_replace("`","",$filter_column);

			$vars = array();
			$vars['filter_value'] = $filter_value;
			$vars['auto_filter_value'] = $auto_filter_value;

			$filter_code = "";
			if($filter_column!="")
			{
				if(strpos($filter_column,",")!==false)
				{
					$f_columns = explode(",",$filter_column);
					$f_value_min = explode(",",$filter_value_min);
					$f_value_max = explode(",",$filter_value_max);
					$f_values = explode(",",$filter_value);
					if(count($f_value_min)==count($f_columns) && count($f_value_max)==count($f_columns))
					{
						for($n=0; $n<count($f_columns); $n++)
						{
							$current_filter_column = $conn->escapeString($f_columns[$n]);

							if($filter_code != "") $filter_code .= " and ";
							$vars['filter_value_min'.$n] = $f_value_min[$n];
							$vars['filter_value_max'.$n] = $f_value_max[$n];
						    	$filter_code .= "`$current_filter_column`>='[filter_value_min".$n."]' and `$current_filter_column`<='[filter_value_max".$n."]'";
						}
					}
					else if(count($f_columns)==count($f_values))
					{
					}
				}
				else
				{
					if($filter_code != "") $filter_code .= " and ";
					
					if($filter_value_min != "" && $filter_value_max != "")
					{
						$vars['filter_value_min'] = $filter_value_min;
						$vars['filter_value_max'] = $filter_value_max;
                        			$filter_code .= "`$filter_column`>='[filter_value_min]' and `$filter_column`<='[filter_value_max]'";
					}
					else {
						$filter_code .= "`$filter_column`='[filter_value]'";
					}
				}
			}

			/**  FILTERS JSON
			*    The purpose here is to allow for the maximum flexibility for filtering data
			* In order to do this, we want to represent our filters in JSON  Here's an example
			*	[  //List of filters
			*		{
			*			"field" : "fieldname",
			*			"operator" : "OP",
			*			"value1" : "value1",
			*			"value2" : "value2" //Optional depending on operator
			*		},
			*		. . .
			*   ]
			*
			*   or
			*
			*   {  //Single Filter
			*		"field" : "fieldname",
			*		"operator" : "OP",
			*		"value1" : "value1",
			*		"value2" : "value2" //Optional depending on operator
			*	}
			*
			*
			*
			*/
			$filtersstr = "";

			if(isset($_POST['filters']))
			{
				$filtersstr = $_POST['filters'];

				$filters = json_decode($filtersstr);

				if(is_null($filters)) //Ensures non-null (No decoding errors)
				{
					echo genError("Error Parsing Filters","There was an Error parsing the JSON for filters.  This is likely due to incorrectly formatted JSON.");
					quit();
					exit();
				}

				//echo $filter_code . "<br />";
				//echo count($filters);
				if(is_array($filters))
				{
					foreach($filters as $key => $filter)
					{
				    		$sqlf = getFilterText($filter);
						if($filter_code != "")
							$filter_code .= " AND ";
						$filter_code .= $sqlf;
					}
				}
				else if(is_object($filters))
				{
			    	$filter = getFilterText((array)$filters);
					if($filter_code != "")
						$filter_code .= " AND ";
					$filter_code .= $filter;
				}
				else
				{
					echo genError("JSON ERROR","This error should never come up.  The interpretted JSON string was parsed as neither an Object or an Array.");
					quit();
					exit();
				}

				//echo $filter_code . "<br />";
			}

			/** END JSON FILTERS */

			if($filter_deleted && !in_array($cmd_table, $allow_json_format))
			{
				if($filter_code != "")
					$filter_code .= " AND ";
				$filter_code .= "`_deleted` <> '1'";
			}

			if($cmd_table == "forced_modifier_lists"){
				if($filter_code != "")
					$filter_code .= " AND ";
				$filter_code .= "LEFT(`title`,1) != '*'";
			}

			if($auto_filter_column!="")
			{
				if($filter_code != "") $filter_code .= " and ";
				$filter_code .= "`$auto_filter_column`='[auto_filter_value]'";
			}
			if( ($cmd=="insert" || $cmd=="update" || $cmd=="replace") && !in_array($cmd_table, $allow_json_format) )
			{
				$contents = postvar("contents");
				$set_vars = array();

				$in_row = false;
				$in_def = false;
				$def_name = "";
				$tag_contents = "";
				$tags = array();
				$varstack = array();
				$in_quotes = "";
				$escape_next = false;
				$rowcount = 0;
				for($n=0; $n<strlen($contents); $n++) {
					$c = substr($contents,$n,1);

					if($in_def) {
						if($c==">") {
							$in_def = false;
							if(count($tags) > 0) {
								if(substr($def_name,0,1)=="/") {
									$top_tag = $tags[count($tags)-1];
									if("/".$top_tag==$def_name) {
										$the_tag = & $varstack;
										//echo "\nEnded Tag: " . $def_name . " - " . $tag_contents;
										if($top_tag == 'row' && $in_row){
											$in_row = false;
										}
										for($m=0; $m<count($tags); $m++) {
											$rhandle = $tags[$m];
											//if($rhandle=="row") $rhandle = $rhandle . $rowcount;
											if(!isset($the_tag[$rhandle]))
												$the_tag[$rhandle] = array();
											$the_tag = & $the_tag[$rhandle];
											if($rhandle=="row" && !$m) {
												if(!isset($the_tag[$rowcount]))
													$the_tag[$rowcount] = array();
												$the_tag = & $the_tag[$rowcount];
											}
										}
										if(!is_array($the_tag) || count($the_tag) < 1){
											$the_tag = $tag_contents;
										} else if(is_array($the_tag)) {
										}
										array_pop($tags);
									}
								} else {
									$tag_contents = "";
									if($def_name=="row" && !$in_row){
										$rowcount++;
										$in_row = true;
									}
									array_push($tags,$def_name);
								}
							} else {
								$tag_contents = "";
								if($def_name=="row" && !$in_row){
									$rowcount++;
									$in_row = true;
								}
								array_push($tags,$def_name);
							}
						} else {
							$def_name .= $c;
						}
					} else if($c=="'" || $c=="\"") {
						$tag_contents .= $c;
						if($in_quotes=="") {
							$in_quotes = $c;
						} else if($c==$in_quotes) {
							$in_quotes = "";
						}
					} else if($c=="<") {
						$in_def = true;
						$def_name = "";
					} else {
						$tag_contents .= $c;
					}
				}

				function read_varstack($varstack,$lvl)
				{
					if(is_array($varstack))
					{
						foreach($varstack as $key => $val)
						{
							echo "\n";
							for($l=0; $l<$lvl; $l++)
								echo "--";
							echo $key . " = ";
							read_varstack($val,$lvl+1);
						}
					}
					else
					{
						echo $varstack;
					}
				}
				//read_varstack($varstack,0);

				if(isset($varstack['row']))
				{
					$replacement_cmd_ran = array();

					$rows = $varstack['row'];
					if(isset($rows['id']))
					{
					}
					foreach($rows as $rowkey => $rowval)
					{
						$row_info = $rowval;
						$insert_keys = "";
						$insert_vals = "";
						$update_code = "";
						echo "\n-------------------------- Row $rowkey ---------------------------\n";
						if($auto_filter_column!="")
						{
							$row_info[$auto_filter_column] = $auto_filter_value;
						}
						if($cmd_table=="orders" && (!isset($row_info['order_id']) || $row_info['order_id']==""))
						{
							$next_order_id = assignNextWithPrefix("order_id", "orders", "location_id", $locationid, "7-");
							$row_info['order_id'] = $next_order_id;
						}

						$now_ts = time();

						switch ($cmd_table) {

							case "orders":

								$row_info['last_mod_device'] = "API ".strtoupper($cmd);
								$row_info['last_mod_ts'] = $now_ts;
								$row_info['pushed_ts'] = $now_ts;

								// LP-3590 - Fix data issues in orders created by Pepper connector
								if ($has_pepper && $cmd == 'insert') $row_info = fixPepperOrdersRow($row_info);

								break;

							case "order_contents":

								$row_info['last_mod_ts'] = $now_ts;

								// LP-3590 - Fix data issues in orders created by Pepper connector
								if ($has_pepper && $cmd == 'insert') $row_info = fixPepperOrderContentsRow($row_info);

								break;

							case "modifiers_used":

								// LP-3590 - Fix data issues in orders created by Pepper connector
								if ($has_pepper && $cmd == 'insert') $row_info = fixPepperModifiersUsedRow($row_info);

								break;


							case "cc_transactions":

								$row_info['last_mod_ts'] = $now_ts;

								// LP-958 -- Fixed API-created payments duplicating when their order is opened by a version 3 app due to a blank internal_id
								if (empty($company_info['id']))  $company_info['id']  = $rest_read['id'];
								if (empty($location_info['id'])) $location_info['id'] = $locationid;
								$row_info['internal_id'] = newInternalID();

								// LP-3590 - Fix data issues in cc_transactions created by Pepper connector
								if ($has_pepper && $cmd == 'insert') $row_info = fixPepperCcTransactionsRow($row_info);

								break;


							case "split_check_details":

								// LP-3590 - Fix data issues in orders created by Pepper connector
								if ($has_pepper && $cmd == 'insert') $row_info = fixPepperSplitCheckDetailsRow($row_info);

								break;

							default:
								break;
						}

						foreach($row_info as $key => $val)
						{
							if($cmd=="insert" && $key=="id")
								continue; //Ensure that we aren't trying to insert with an id, so we can auto-increment

							if(in_array($key, $filter_out))
								continue;

							if($integratorKey){
								if(!isset($allowed_columns[$key]) || $allowed_columns[$key]==1){
									echo genError("Permission error", "You do not have permission to modify ". $key);
									quit();
									exit();
								}
							}

							if($insert_keys!="") $insert_keys .= ",";
							if($insert_vals!="") $insert_vals .= ",";
							if($update_code!="") $update_code .= ",";
							$insert_keys .= "`".$conn->escapeString($key)."`";
							$insert_vals .= "'".$conn->escapeString($val)."'";
							$update_code .= "`".$conn->escapeString($key)."`='".$conn->escapeString($val)."'";
						}

						$debug_result = "";

						if($cmd=="update")
						{
							$update_by = "";
							if($cmd_table=="orders" && isset($row_info['order_id']))
							{
								$update_by = "order_id";
							}
							else if(isset($row_info['id']))
							{
								$update_by = "id";
							}

							if($update_by!="")
							{
								$result = lavu_query("update $cmd_table set $update_code where `[1]`='[2]' limit 1",$update_by,$row_info[$update_by]);
								if($result)
								{
									$debug_result = "success";
									echo "<result><".$update_by.">".$row_info[$update_by]."</".$update_by."></result>";
									// if (!isset($row_info['order_id']))
									// 	error_log("here's row_info: ".print_r($row_info, true));
								}
								else
								{
									$debug_result = "error - ".lavu_dberror();
									echo "<result><".$update_by.">0</".$update_by."></result>";
								}
							}
						}
						else if($cmd=="replace")
						{
							$update_by = "";
							if(($cmd_table=="order_contents" || $cmd_table=="cc_transactions") && isset($row_info['order_id']))
							{
								$update_by = "order_id";
							}
							else if(isset($row_info['id']))
							{
								$update_by = "id";
							}

							$update_by_filter = $row_info[$update_by];

							if($update_by!="" && $update_by_filter!="")
							{
								if(!isset($replacement_cmd_ran[$update_by_filter]))
								{
									lavu_query("delete from $cmd_table where `[1]`='[2]'",$update_by,$update_by_filter);
									$replacement_cmd_ran[$update_by_filter] = true;
								}
								$result = lavu_query("insert into $cmd_table ($insert_keys) values ($insert_vals)");
								if($result)
								{
									$debug_result = "success";
									$newid = lavu_insert_id();
									echo "<result><id>$newid</id>";
									if(isset($row_info['order_id']))
										echo "<order_id>".$row_info['order_id']."</order_id>";
									echo "</result>";
								}
								else
								{
									$debug_result = "error - ".lavu_dberror();
									echo "<result><id>0</id></result>";
								}
							}
						}
						else // this really should be else if($cmd=='insert') {blah} else { error}
						{
							$result = lavu_query("insert into $cmd_table ($insert_keys) values ($insert_vals)");
							if($result)
							{
								$debug_result = "success";
								$newid = lavu_insert_id();
								echo "<result><id>$newid</id>";
								if(isset($row_info['order_id']))
									echo "<order_id>".$row_info['order_id']."</order_id>";
								echo "</result>";
							}
							else
							{
								$debug_result = "error - ".lavu_dberror();
								echo "<result><id>0</id></result>";
							}
						}
						//echo "\n";
						//echo "\nupdate $cmd_table set $update_code";

						if ($location_api_debug) {
							error_log("API: ".$dataname." - ".$cmd." - ".$cmd_table." - ".$debug_result);
						}
					}
				}
			}
			else if($cmd=="list")
			{
			    	if($filter_code!="") $where_code = "where "; else $where_code = "";
			    
				if($field_list != "") {

					//retrieve specified fields
					if($integratorKey){

						$field_list_array= trim( explode(",",$field_list));		// if you are an integrator we need to make
						foreach ($field_list_array as $f){						// sure you have access to the cols you are requesting
							if(!isset($allowed_columns[$f])){
								genError("Permission Error","You do not have permission to select column: ". $f);
								quit();
								exit();
							}
						}
					}
					$rquery = "select $field_list from `$cmd_table` ".$where_code.$filter_code.$query_extra.$limit_code;

				} else {

					if( $integratorKey){

						$colArray=array();
						while(list($col, $perm)= each($allowed_columns)){
							$colArray[]= "`".$col."`";
						}
						$rquery= "SELECT " . implode(",", $colArray) . "FROM `$cmd_table` ".$where_code.$filter_code.$query_extra.$limit_code;
						//echo $rquery;
					}else{
						//retrieve all fields
						$rquery = "select * from `$cmd_table` ".$where_code.$filter_code.$query_extra.$limit_code;
					}
				}

				if($debug)
				{
					echo $rquery;
					foreach($vars as $key => $val)
					{
						echo "\n[" . $key . "] = " . $val;
					}
					echo "\n\n";
				}

				//$str = "";
				$row_query = lavu_query($rquery,$vars);
				/*
				if (!mysqli_num_rows($row_query)){
					genError("Empty Query Results ","Your query has returned no values");
					quit();
					exit();
				}*/

				//print_r($vars);

				if (!$row_query) {
					error_log("API query failed for ".$dataname.": ".lavu_dberror()." ... ".$rquery);
					error_log("filter_code: ".$filter_code);
					error_log("filtersstr: ".$filtersstr);
				}

				// valid xml response header
				if($format != 'json') {
				    ($is_valid ? $str = "<?xml version=\"1.0\" encoding=\"utf-8\"?>".PHP_EOL."<results>".PHP_EOL : $str = '');
				}
				if($row_query && isset($testing) && $testing){
					echo "your mom";
					echo mysqli_num_rows($row_query);
					echo $rquery;
				}
				$cols = mysqli_num_fields($row_query);
				while($row_read = @mysqli_fetch_assoc($row_query))
				{

					if(isset($row_read['_deleted']) && $row_read['_deleted']==1 && $filter_deleted)
					{
					}
					else
					{
						// build xml or json strings
					    	$str .= ($format == 'json') ? "{" : "<row>".PHP_EOL;
						if(isset($_POST['extra_row_contents']))
						{
							$erc_parts = explode(",",$_POST['extra_row_contents']);
							for($y=0; $y<count($erc_parts); $y++)
							{
								$erc = trim($erc_parts[$y]);

								if($erc=="server_time") $str .= ($format == 'json') ? '"server_time" : "'.date("Y-m-d H:i:s").'",' : "<server_time>".date("Y-m-d H:i:s")."</server_time>".PHP_EOL;
								else if($erc=="server_ts") $str .= ($format == 'json') ? '"server_ts" : "'.time().'",' : "<server_ts>".time()."</server_ts>".PHP_EOL;
							}
						}
						$i = 1;
						foreach($row_read as $key => $val)
						{
							if(substr($key,0,1)=="_")
							{
							    if($show_deleted_col && $key == "_deleted") {
							        $str .= "<" . $key . ">".$val."</" . $key . ">".PHP_EOL;
							    }
							}
							else if(substr($key,0,11)=="integration")
							{
							}
							else if(in_array($key, $filter_out))
							{
							}
							else
							{
								//$str .= "\n\t<" . $key . ">" . $val . "</" . $key . ">";
							    	$str .= ($format == 'json') ? '"'.$key.'" : ' : "<" . $key . ">";

								// LP-1317 -- Strip Form Feed binary character inserted by NETS integration since it is not a legal character in XML
								if ($key == 'extra_info') $val = preg_replace('/[\x0c]/', '', $val);

								if((isset($encode_contents) && $encode_contents=="1"))
								{
									if($is_valid)
										$val = htmlspecialchars($val);
									$val = str_replace("&","&amp;",$val);
									$val = str_replace('"',"&quot;",$val);
									$val = str_replace("'","&apos;",$val);
									$val = str_replace("<","&lt;",$val);
									$val = str_replace(">","&gt;",$val);
									$str .= ($format == 'json') ? '"'.$val.'",' : $val;
								}
								else {
								    $str .= ($format == 'json') ? '"'.$val.'",' : $val;
								}
								if($format == 'json') {
								    ($i == $cols) ? $str = substr($str, 0, -1) : $i++;
								}
								
								$str .= ($format == 'json') ? '' : "</" . $key . ">".PHP_EOL;
								//echo "<br>" . $row_read['id'];
							}
							
						}
						$str .= ($format == 'json') ? '},' : "</row>".PHP_EOL;

					}
				}
				
				if($format == 'json') {
				    $str = substr($str, 0, -1);
				    $str = "[".$str."]";
				}
				else {
				    ($is_valid ? $str .= "</results>" : $str .= PHP_EOL);
				}
				
				echo $str;
			}else if($cmd == 'sync_table'){ // LP-8192
				handleTableSyncRequest();
			}
		}else{
			echo genError("Permission error: ", "No filters specified for table " . $cmd_table);
			quit();
			exit();
		}

	}else{


		echo genError("Permission error: ", "You do not have permission to access ". $cmd_table);
		quit();
		exit();
	}

	// Special Command Triggers - CF
	if(isset($_REQUEST['cmd_trigger']) && $_REQUEST['cmd_trigger']!="")
	{
		require_once(dirname(__FILE__) . "/tapi.php");
		$tstr = run_api_cmd_trigger($_REQUEST['cmd_trigger']);
		echo $tstr;
	}

	/*
	$current_time = microtime_float();
	$last_mapi_duration = number_format(($current_time - $last_mapi_start_seconds),8);
	lavu_query("update `config` set `value`='[1]', `value2`='processed', `value3`='[3]', `value4`='[4]', `value5`='[5]' where `location`='[2]' and `setting`='last_mapi_req'",time(),$locationid,$cmd,$cmd_table,$last_mapi_duration);*/
	quit();

	function quit()
	{
		global $locationid;
		global $cmd;
		global $cmd_table;
		global $last_mapi_start_seconds;

		$current_time = microtime_float();
		$last_mapi_duration = number_format(($current_time - $last_mapi_start_seconds),8);

		update_last_mapi_value("processed",$cmd,$cmd_table,$last_mapi_duration);
		//lavu_query("update `config` set `value`='[1]', `value2`='processed', `value3`='[3]', `value4`='[4]', `value5`='[5]' where `location`='[2]' and `setting`='last_mapi_req'",time(),$locationid,$cmd,$cmd_table,$last_mapi_duration);
	}

	function genError($code, $reason)
	{
		$str = "<row>";
		$str .= "<error>$code</error>";
		$str .= "<reason>$reason</reason>";
		$str .= "</row>";

		return $str;
	}

	function getFilterText($obj)
	{
		$conn = ConnectionHub::getConn('rest');
		$result = "";
		$obj = (array)$obj;
	    	$field = $conn->escapeString($obj['field']);
		
		$operator = strtoupper($conn->escapeString($obj['operator']));
		$value1 = $obj['value1'];

		//	print_r($obj);
		$allowed_operators = Array(
		"<=",
		">=",
		"<",
		">",
		"=",
		"<>",
		"BETWEEN",
		"LIKE",
		"NOT LIKE",
		"!=",
		"IN");

		$a = in_array($operator, $allowed_operators);

		if($a)
		{
			if($operator == "BETWEEN")
			{
				$value2 = $obj['value2'];
				$result = "`" . $field . "` " . $operator . " '" . $conn->escapeString($value1). "' AND '" . $conn->escapeString($value2) ."'";
			}
			else if($operator == "IN")
			{
				$list_array = $obj['value1'];
				if (count($list_array) == 0) {
					return "0";
				}

				$result = "`" . $field . "` " . $operator . " ";
				$list = "";
				foreach($list_array as $index => $val)
				{
					if($list != "")
						$list .= ", ";
					$list.= "'" . $conn->escapeString($val) . "'";
				}
				$result .= "(" . $list . ")";
			}
			else
			{
			    	$result = "`" . $field . "` " . $operator . " '" . $conn->escapeString($value1) . "'";   
			}
		}

		return $result;
	}
	function testing($string){				//testing output
		global $testing;
		if(isset($_POST['testing']) || $testing)
			echo $string;
	}

	function fixPepperOrderContentsRow($row_info)
	{
		// Get menu_item to get the missing modifier list and/or forced modifier group
		$menu_items = lavu_query("SELECT `modifier_list_id`, `forced_modifier_group_id`, `print`, `printer` FROM `menu_items` WHERE `id` = '[item_id]'", $row_info);
		if ($menu_items === false || mysqli_num_rows($menu_items) === 0) error_log("Lavu API could not get menu_item for fixPepperOrderContentsRow: ". lavu_dberror());
		$menu_item = mysqli_fetch_assoc($menu_items);

		// Set modifier list, if missing
		if ( empty($row_info['modifier_list_id']) && !empty($menu_item['modifier_list_id']) )
		{
			$row_info['modifier_list_id'] = $menu_item['modifier_list_id'];
		}

		// Set forced modifier group, if missing
		if ( empty($row_info['forced_modifier_group_id']) && !empty($menu_item['forced_modifier_group_id']) )
		{
			$row_info['forced_modifier_group_id'] = $menu_item['forced_modifier_group_id'];
		}

		// Strip any unnecessary trailing delimiters in the modifier list string
		if ( substr($row_info['options'], -4) == "\n - " )
		{
			$row_info['options'] = substr($row_info['options'], 0, -4);
		}

		// If item-level printer setting because Pepper just sends a hard-coded 1
		if ( !empty($menu_item['print']) )
		{
			$row_info['printer'] = $menu_item['printer'];
		}

		return $row_info;
	}

	function fixPepperOrdersRow($row_info)
	{
		// Need to move value in orders.gratuity to orders.other_tip
		if ( !empty($row_info['gratuity']) )
		{
			$row_info['other_tip'] = $row_info['gratuity'];
			$row_info['gratuity'] = '';
		}

		return $row_info;
	}

	function fixPepperSplitCheckDetailsRow($row_info)
	{
		// Need to blank out split_check_details.gratuity because it triggers auto-gratuity in the POS
		if ( !empty($row_info['gratuity']) )
		{
			$row_info['gratuity'] = '';
		}

		return $row_info;
	}

	function fixPepperModifiersUsedRow($row_info)
	{
		// Option.quantity is not a required field for Pepper so they may not have it to send.
		// However, you can only have 1 Modifier Option in the Pepper app so we can assume qty = 1 (for now).
		// This block should come before the block below since it uses row_info[qty] in a calculation.
		if ( empty($row_info['qty']) )
		{
			$row_info['qty'] = '1';
		}

        if ( strpos($row_info['list_id'], ':') !== false )
        {
                list($list_type, $list_id) = explode(':', $row_info['list_id']);
                $row_info['list_id'] = $list_id;
        }

		if ( strpos($row_info['mod_id'], ':') !== false )
		{
			list($mod_type, $mod_id) = explode(':', $row_info['mod_id']);
			$row_info['mod_id'] = $mod_id;

			// TO DO : query for appropriate table to get modifier info
			if ( !empty($mod_id) && !empty($mod_type) )
			{
				$dbtable = ($mod_type == 'optional_modifier') ? 'modifiers' : 'forced_modifiers';
				$dbargs = array('mod_id' => $mod_id, 'dbtable' => $dbtable);
				$modifiers = lavu_query("SELECT `id`, `title`, `cost` FROM `[dbtable]` WHERE `id` = [mod_id]", $dbargs);
				if ($modifiers === false || mysqli_num_rows($modifiers) === 0) error_log("Lavu API could not get {$dbtable} with dbargs=". json_encode($dbargs) ." :". lavu_dberror());
				$modifier = mysqli_fetch_assoc($modifiers);

				$row_info['title'] = $modifier['title'];
				$row_info['mod_id'] = $modifier['id'];
				$row_info['unit_cost'] = $modifier['cost'];
				$row_info['cost'] = (double) $modifier['cost'] * (double) $row_info['qty'];
			}
		}

		/* TO DO : wait until Pepper fixes mod_id coming in blank and then re-work logic (to use `mod_id` and `type` to get modifier info and then fill in `list_id`, `name`, `price`, `unit_price`)
		// Need to set most modifier_used if the characteristic fields are empty
		if ( empty($row_info['mod_id']) && empty($row_info['list_id']) && !empty($row_info['title']) )
		{
			// Split value in modifiers_used.title using ':'
			$modifier_info = array();
			list($modifier_container_name, $modifier_info['list_id']) = explode(':', $row_info['title']);

			if ( $modifier_container_name == 'forced_modifier_list' )
			{
				// Query for forced_modifier by container
				$modifier_info['row'] = empty($row_info['row']) ? '0' : $row_info['row'];
				$modifiers = lavu_query("SELECT * FROM `forced_modifiers` WHERE `list_id` = [list_id] ORDER BY `_order`, `id` LIMIT [row], 1", $modifier_info);
				if ($modifiers === false || mysqli_num_rows($modifiers) === 0) error_log("Lavu API could not get forced_modifier for container_info=". json_encode($modifier_info) ." :". lavu_dberror());
				$modifier = mysqli_fetch_assoc($modifiers);

				$row_info['title'] = $modifier['title'];
				$row_info['mod_id'] = $modifier['id'];
				$row_info['unit_cost'] = $modifier['cost'];
				$row_info['cost'] = (double) $modifier['cost'] * (double) $row_info['qty'];
			}
			else if ( $modifier_container_name == 'optional_modifier_list' )
			{
				// Query for forced_modifier by container
				$modifier_info['row'] = empty($row_info['row']) ? '0' : $row_info['row'];
				$modifiers = lavu_query("SELECT * FROM `modifiers` WHERE `category` = [list_id] ORDER BY `_order`, `id` LIMIT [row], 1", $modifier_info);
				if ($modifiers === false || mysqli_num_rows($modifiers) === 0) error_log("Lavu API could not get modifier with container_info=". json_encode($modifier_info) .": ". lavu_dberror());
				$modifier = mysqli_fetch_assoc($modifiers);

				$row_info['title'] = $modifier['title'];
				$row_info['mod_id'] = $modifier['id'];
				$row_info['unit_cost'] = $modifier['cost'];
				$row_info['cost'] = (double) $modifier['cost'] * (double) $row_info['qty'];
			}
			// TO DO : see if forced_modifier_group is a legal value
		}
		*/

		return $row_info;
	}

	function fixPepperCcTransactionsRow($row_info)
	{
		// Get orders to get the correct ioid (to prevent payments not getting picked up in the POS)
		$orders = lavu_query("SELECT `ioid` FROM `orders` WHERE `order_id` = '[order_id]'", $row_info);
		if ($orders === false || mysqli_num_rows($orders) === 0) error_log("Lavu API could not get orders ioid for fixPepperCcTransactionsRow: ". lavu_dberror());
		$order = mysqli_fetch_assoc($orders);

		// LP-4073 - Fix Pepper app payments not showing up on pre-orders in the POS
		if ( !empty($order['ioid']) )
		{
			$row_info['ioid'] = $order['ioid'];
		}

		return $row_info;
	}


	// LP-8192
	function handleTableSyncRequest(){
		global $dataname;
		$tableName = $_POST['table'];
		// TODO Put a block on how mandatory duration between table syncs.
		require_once(__DIR__.'/../resources/chain_functions.php');
		$successFails = syncTableToAllChildAccounts($dataname, $tableName);
	}
