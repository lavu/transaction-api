<?php

	// check the mysql load level
	// @$lockout_level: 1 (where the api gets cut off) or 2 (where the backend gets cut off entirely)
	// @$b_exit:
	//     if TRUE then exit() when the server load gets too high,
	//     if FALSE then return TRUE if about the lockout level and FALSE if the load level is still ok
	function check_lockout_status($lockout_level, $b_exit = TRUE)
	{
		global $dataname;

		if ((time() % 30) == 0) {
			//error_log("check_lockout_status for ".$dataname." - ".dirname(__FILE__));
		}
		
		$lockfname = "/home/poslavu/private_html/connects_lockout.txt";
		$fp = fopen($lockfname,"r");
		$lockout_status = fread($fp,filesize($lockfname));
		fclose($fp);

		if ($lockout_status!="")
		{
			$lockout_parts = explode("|",$lockout_status);
			if (count($lockout_parts) > 1 && $lockout_parts[0] * 1 >= $lockout_level && $lockout_parts[1] * 1 > time() - 300)
			{
				// let us know that a lockout is occurring
				error_log("API would be locked for ".$dataname." (connects_lockout)  - lockout level: ".$lockout_level." - lockout status: ".($lockout_parts[0] * 1)." ts compare: ".($lockout_parts[1] * 1)." > ".(time() - 300));

				//echo "We apologize for the inconvenience but the POSLavu BackEnd is temporarily Unavailable. Please check back later.";
				/*if (isset($_REQUEST['is_valid']) && $_REQUEST['is_valid'] == '1') {
					echo "<?xml version=\"1.0\" encoding=\"utf-8\"?><results>";
				}
				echo "<row><error>API Temporarily Unavailable</error><reason>We apologize for the inconvenience, but the API has been temporarily disabled due to increased traffic.  Please try back later.</reason></row>";
				if (isset($_REQUEST['is_valid']) && $_REQUEST['is_valid'] == '1') {
					echo "</results>";
				}
				if ($b_exit) {
					exit();
				} else {
					return TRUE;
				}*/
			}
		}
		return FALSE;
	}

	// check the server load level
	function check_local_lockout_status($lockout_level) // for the load level
	{
		global $dataname;

		if ((time() % 30) == 0) {
			//error_log("check_local_lockout_status for ".$dataname." - ".dirname(__FILE__));
		}

		$lockfname = "/home/poslavu/private_html/loadlevel.txt";
		$fp = fopen($lockfname,"r");
		$lockout_status = fread($fp,filesize($lockfname));
		fclose($fp);

		if($lockout_status!="" && is_numeric($lockout_status))
		{
			if($lockout_status * 1 >= $lockout_level)
			{
				// let us know that a lockout is occurring
				error_log("API would be locked for ".$dataname." (loadlevel) - lockout level: ".$lockout_level." - lockout status: ".$lockout_status);
				
				//echo "We apologize for the inconvenience but due to extremely high traffic ERS is temporarily Unavailable. Please check back in 1/2 hour.";
				/*if (isset($_REQUEST['is_valid']) && $_REQUEST['is_valid'] == '1') {
					echo "<?xml version=\"1.0\" encoding=\"utf-8\"?><results>";
				}
				echo "<row><error>API Temporarily Unavailable</error><reason>We apologize for the inconvenience, but the API has been temporarily disabled due to increased traffic.  Please try back later.</reason></row>";
				if (isset($_REQUEST['is_valid']) && $_REQUEST['is_valid'] == '1') {
					echo "</results>";
				}
				exit();*/
			}
		}
	}
?>