<?php

require_once('/home/poslavu/public_html/admin/cp/reqserv/oapi/OrderApiObject.php');

class OrderContent extends OrderApiObject
{
	function __construct($args)
	{
		// Defaults
		$this->data = array(
			'order_id'               => null,
			'loc_id'                 => '1',
			'quantity'               => '1',
			'check'                  => '1',
			'print_order'            => '1',
			'open_item'              => '0',
			'forced_modifiers_price' => '0.00',
			'subtotal'               => '0.00',
			'allow_deposit'          => '0',
			'discount_amount'        => '0.00',
			'discount_value'         => '0.000000',
			'after_discount'         => '0.00',
			'subtotal_with_mods'     => '0.00',
			'tax_amount'             => '0.00',
			'total_with_tax'         => '0.00',
			'tax_rate1'              => '0',
			'tax_rate2'              => '0',
			'tax_rate3'              => '0',
			'tax_rate4'              => '0',
			'tax_rate5'              => '0',
			'tax1'                   => '0.000000',
			'tax2'                   => '0.000000',
			'tax3'                   => '0.000000',
			'tax4'                   => '0.000000',
			'tax5'                   => '0.000000',
			'tax_subtotal1'          => '0.000000',
			'tax_subtotal2'          => '0.000000',
			'tax_subtotal3'          => '0.000000',
			'tax_subtotal4'          => '0.000000',
			'tax_subtotal5'          => '0.000000',
			'after_gratuity'         => '0.000000',
			'void'                   => '0',
			//'server_time'          => now(),  // TO DO : need a mysql now
			'device_time'            => date('Y-m-d H:i:s'),  // TO DO : see if pepper will send
			'split_factor'           => '1',
			'tax_inclusion'          => '0',    // TO DO : get from menu_item
			'sent'                   => '0',
			'tax_exempt'             => '0',
			'checked_out'            => '0',
			'price_override'         => '0',
			'override_id'            => '0',
			'auto_saved'             => '0',
		);

		// Override/add with any passed-in values
		$this->data = $args + $this->data;
	}
}