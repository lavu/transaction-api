<?php

require_once('/home/poslavu/public_html/admin/cp/reqserv/oapi/LavuOrder.php');
require_once('/home/poslavu/public_html/inc/loyalty/pepper/PepperConfig.php');

class PepperLavuOrder extends LavuOrder
{
	// Pepper IDs
	private $tenantId;
	private $locationId;
	private $orderId;

	// Pepper fields
	private $user;
	private $basket;
	private $label;
	private $redemptions;

	public function __construct($args, $order)
	{
		$this->dataname = $args['dataname'];
		$this->location = $this->getLocation($this->dataname);
		$this->restaurant_id = empty($args['restaurant_id']) ? $this->getRestID($this->dataname) : $args['restaurant_id'];
		$this->loc_id = $this->location['id'];

		empty($this->location['timezone']) || date_default_timezone_set($this->location['timezone']);

		empty($this->location['timezone']) || date_default_timezone_set($this->location['timezone']);

		// Pepper-specific data
		$this->orderId = $order->orderId;
		$this->tenantId = $order->tenantId;
		$this->locationId = $order->locationId;
		$this->user = $order->user;
		$this->redemptions = isset($order->redemptions) ? $order->redemptions : array();

		// Pepper-related static strings
		$this->label = new stdClass();
		$this->label->appName = 'Loyalty App';  // was "Mobile App"
		$this->label->customer = 'Customer';  // LP-3997 - Changed Loyalty User => Customer per Dan in Product and to allow POS to print customer name on kitchen ticket
		$this->label->componentLayoutID = PepperConfig::COMPONENT_LAYOUT_ID;
		$this->label->loyaltyInfo = 'loyalty_info';
		$this->label->appPayType = $this->label->appName;  // was "App"
		$this->label->appServerName = $this->label->appName;

		// Insert order row into db right away to lock in our actual, unique order_id to use in other methods
		$this->ioid = $this->newInternalID();
		$this->order_id = $this->insertOrderStub();
		$this->app_pay_type_id = $this->getPayTypeID('app');

		$this->total = 0.00;
		$this->subtotal = 0.00;
		$this->discount = 0.00;
		$this->idiscount = 0.00;
		$this->tax = 0.00;
		$this->itax = 0.00;
		$this->tip = $order->tipAmount;

		$this->order_contents = array();
		$this->split_check_details = array();
		$this->cc_transactions = array();
		$this->modifiers_used = array();

		// Process the order contents first (which also covers tax and discounts) to total up the line items so we can process the order and then payments
		$this->processDiscount();
		$this->processOrderContents($order);
		$this->processOrder($order);
		$this->processPayments($order);
	}

	// Taken from core_functions - but we couldn't use that method since it relied on $location_info and $company_info
	private function newInternalID()
	{
		$prefix = $this->restaurant_id.$this->loc_id."0";
		$randomString = str_pad((string)rand(1, 9999), 4, "0", STR_PAD_LEFT);

		$str = $prefix."-".$randomString."-".date("YmdHis");

		return $str;
	}

	private function getPayTypeID($pay_type, $return_default = true)
	{
		$pay_type_id = '';

		$payment_ext_identifier = 'payment_extension:' . (($pay_type == 'points') ? PepperConfig::PAYMENT_EXTENSION_ID_POINTS : PepperConfig::PAYMENT_EXTENSION_ID_APP);

		$pay_type = $this->queryDatabase('payment_type', array('dataname' => $this->dataname, 'loc_id' => $this->loc_id, 'special' => $payment_ext_identifier, 'pay_type' => $pay_type));

		if (empty($pay_type['id']))
		{
			// We default to returning the pay_type_id for cash so mobile app payments will not disappear from POS (which could cause customers to be double charged)
			// If a customer ever deletes their custom payment methods for Pepper, we could retroactively fix their db records with pay_type = <$pay_type> && pay_type_id = 1
			if ($return_default) $pay_type_id = '1';
			error_log("No {$pay_type} payment_type found for Pepper dataname={$this->dataname}; defaulting to pay_type_id for Cash");
		}
		else
		{
			$pay_type_id = $pay_type['id'];
		}

		return $pay_type_id;
	}

	private function processDiscount()
	{
		// Process Discount

		$this->discount  = 0.00;
		$this->idiscount = 0.00;

		foreach ($this->redemptions as $redemption)  // TO DO : figure out if pepper is planning on sending more than one redemption (their app doesn't seem to support it currently)
		{
			if (empty($redemption->token)) continue;

			// Get the discount_type db row for our discount_id and then reduce the totals
			$discount_type_row = $this->queryDatabase('discount_type', array('dataname' => $this->dataname, 'id' => $redemption->token));

			// Make sure the token corresponds to a discount_id of ours
			if (!empty($discount_type_row))
			{
				// These fields will be needed for the order, order_contents, and split_check_details rows
				$this->discount_id    = $discount_type_row['id'];
				$this->discount_type  = $discount_type_row['type'];
				$this->discount_code  = $discount_type_row['code'];
				$this->discount_calc  = $discount_type_row['calc'];
				$this->discount_level = $discount_type_row['level'];

				$this->discount_value = $this->discount_calc;

				if (!empty($discount_type_row['limit_to']))
				{
					$limit_to = str_replace('id=', '', $discount_type_row['limit_to']);
					$this->discount_limitto = explode(',', $limit_to);
				}

				return;
			}
		}
	}

	private function processOrderContents($order)
	{
		// Process Basket order items

		foreach ($order->basket as $item)
		{
			$menu_item = $this->getMenuItem($item->id);
			$icid = newInternalID();
			$now_ts = time();

			// Override menu_item printer with any category-level pass-thru values
			$menu_item['printer'] = $this->getPrinter($menu_item);

			// TO DO : print error_log if order.price differs from menu_item.price, just to see if someone's tampering with the payload or the price was changed in CP mid-flight

			$order_content = new OrderContent( array(  // TO DO : move this down below, if necessary
				'loc_id'                   => $this->loc_id,
				'order_id'                 => $this->order_id,
				'icid'                     => $icid,
				'ioid'                     => $this->ioid,
				'category_id'              => $menu_item['category_id'],
				'item_id'                  => $item->id,
				'item'                     => $menu_item['name'],
				'price'                    => $menu_item['price'],
				'quantity'                 => '1',
				'check'                    => '1',
				'seat'                     => '1',
				'course'                   => '1',
				'print'                    => $menu_item['print'],
				'printer'                  => $menu_item['printer'],
				'modifier_list_id'         => $menu_item['modifier_list_id'],
				'forced_modifier_group_id' => $menu_item['forced_modifier_group_id'],
				//'print_order'            => '0',  // TO DO : check on this
				'allow_deposit'            => $menu_item['allow_deposit'],
				'open_item'                => $menu_item['open_item'],
				'subtotal'                 => $menu_item['price'],
				'after_discount'           => 0.00,  //$item->price,  // TO DO : check on this
				'subtotal_with_mods'       => 0.00,  //$item->price,  // TO DO : check on this
				'last_mod_ts'              => $now_ts,
				'last_change_ts'           => $now_ts,
			));

			// Process Modifiers

			$optional_modifier_total = 0.00;
			$optional_modifiers_titles = array();

			$forced_modifiers_total  = 0.00;
			$forced_modifiers_titles = array();

			// Pepper modifiers are Lavu modifiers groups / modifier lists
			foreach ($item->modifiers as $modifier)
			{
				list($modifier_group_type, $modifier_group_id) = explode(':', $modifier->id);

				// Pepper options are Lavu modifiers
				foreach ($modifier->options as $option)
				{
					list($modifier_type, $modifier_id) = explode(':', $option->id);
					$modifierdb = ($modifier_type == 'forced_modifier' || $modifier_type == 'forced') ? $this->getForcedModifier($modifier_id) : $this->getOptionalModifier($modifier_id);
					$modifier_used = new ModifierUsed( array(
						'order_id'  => $this->order_id,
						'loc_id'    => $order->loc_id,
						'mod_id'    => $modifier_id,
						'type'      => $modifier_type,
						'title'     => $modifierdb['title'],
						'qty'       => '1',
						'cost'      => number_format($modifierdb['cost'], 2),  // normally cost = qty * unit_cost but qty is always 1 for pepper because they send separate items for multiple quantities
						'unit_cost' => number_format($modifierdb['cost'], 2),
						'ioid'      => $this->ioid,
						'icid'      => $icid,
						'list_id'   => $modifier_group_id,
					));

					$this->modifiers_used[] = $modifier_used->toArray();

					// Modifiers type-specific logic
					if ($modifier_type == 'optional_modifier')
					{
						// Built the dash delimited string of optional modifiers that goes on order_contents
						$optional_modifiers_titles[] = $modifierdb['title'];
						$optional_modifier_total += (double) $modifierdb['cost'];
					}
					else if ($modifier_type == 'forced_modifier')
					{
						// Built the dash delimited string of optional modifiers that goes on order_contents
						$forced_modifiers_titles[] = $modifierdb['title'];
						$forced_modifiers_total += (double) $modifierdb['cost'];
					}
				}
			}

			// Field values from iterating over modifiers
			$order_content->special = implode(', ', $optional_modifiers_titles);
			$order_content->options = implode("\n - ", $forced_modifiers_titles);
			$order_content->modify_price = number_format($optional_modifier_total, 2);
			$order_content->forced_modifiers_price = number_format($forced_modifiers_total, 2);
			$order_content->subtotal_with_mods = $order_content->subtotal + $optional_modifier_total + $forced_modifiers_total;

			// Discount (item-level)
			if (!empty($this->discount_id) && ($this->discount_level == 'items' || $this->discount_level == 'lt_item') && in_array($item->id, $this->discount_limitto))
			{
				// Set order_content-level fields from already-set order discount fields
				$order_content->discount_value = $this->discount_value;
				$order_content->discount_type  = $this->discount_code;
				$order_content->discount_id    = $this->discount_id;

				// Calculate the discount amount
				if ($this->discount_code == 'p')
				{
					$order_content->discount_amount = ((double) $order_content->subtotal_with_mods * (double) $this->discount_calc);
				}
				else if ($this->discount_code == 'd')
				{
					$order_content->discount_amount = $this->discount_calc;
				}
			}


			// Totals (needed for tax calculations)
			$order_content->after_discount = $order_content->subtotal_with_mods - $order_content->discount_amount;

			// Taxes

			$has_itax = $this->getIncludedTaxFlag($menu_item);
			if ($has_itax)
			{
				// iTax
				$order_content->tax_inclusion = '1';
				$order_content->itax = $this->calculateIncludedTaxAmountForMenuItem($menu_item, $order_content->after_discount);
				$order_content->itax_rate = $this->calculateTaxRateForMenuItem($menu_item);
				$order_content->tax_name1 = isset($this->tax_rates[1]) ? $this->tax_rates[1]['title'] : '';  // Need to run calculateTaxRateForMenuItem() before this->tax_rates are populated
			}
			else
			{
				// Tax
				for ($i = 1; $i <= 1; $i++)  // originally written to support all 5 tax rates but pepper only supports 1 in their app
				{
					$taxN = "tax{$i}";
					$tax_rateN = "tax_rate{$i}";
					$tax_nameN = "tax_name{$i}";
					$tax_subtotalN = "tax_subtotal{$i}";

					$order_content->$tax_rateN = $this->calculateTaxRateForMenuItem($menu_item, $i);  // Need to run calculateTaxRateForMenuItem() before next two lines
					$order_content->$tax_nameN = $this->tax_rates[$i]['title'];
					$order_content->$taxN = $this->calculateTaxAmountForMenuItem($menu_item, $order_content->$tax_rateN, $order_content->after_discount);
					$order_content->$tax_subtotalN = $order_content->$taxN;
				}
			}

			// TO DO : confirm this is taken care of in the Tax logic
			//// Apply_Taxrate
			$order_content->apply_taxrate = $this->getApplyTaxRate($menu_item);

			// Totals (involving tax)
			$order_content->tax_amount = 0;
			$order_content->total_with_tax = 0;
			$order_content->tax_amount = $order_content->tax1 + $order_content->tax2 + $order_content->tax3 + $order_content->tax4 + $order_content->tax5;
			$order_content->total_with_tax = $order_content->after_discount + $order_content->tax_amount;

			// Order Totals
			$this->total     += $order_content->total_with_tax;
			$this->subtotal  += $order_content->subtotal_with_mods;
			$this->discount  += $order_content->discount_amount;
			$this->idiscount += $order_content->idiscount_amount;
			$this->tax  += $order_content->tax_amount;
			$this->itax += $order_content->itax;

			// Rounding
			$order_content->price = number_format((float) $order_content->price, 2);
			$order_content->modify_price = number_format((float) $order_content->modify_price, 2);
			$order_content->forced_modifiers_price = number_format((float) $order_content->forced_modifiers_price, 2);
			$order_content->after_discount = number_format((float) $order_content->after_discount, 2);
			$order_content->total_with_tax = number_format((float) $order_content->total_with_tax, 2);
			$order_content->subtotal = number_format((float) $order_content->subtotal, 2);
			$order_content->subtotal_with_mods = number_format((float) $order_content->subtotal_with_mods, 2);
			$order_content->discount_amount = number_format((float) $order_content->discount_amount, 2);
			$order_content->idiscount_amount = number_format((float) $order_content->idiscount_amount, 2);
			$order_content->tax_amount = number_format((float) $order_content->tax_amount, 2);
			$order_content->tax1 = number_format((float) $order_content->tax1, 2);
			$order_content->tax2 = number_format((float) $order_content->tax2, 2);
			$order_content->tax3 = number_format((float) $order_content->tax3, 2);
			$order_content->tax4 = number_format((float) $order_content->tax4, 2);
			$order_content->tax5 = number_format((float) $order_content->tax5, 2);
			$order_content->tax_subtotal1 = number_format((float) $order_content->tax_subtotal1, 2);
			$order_content->tax_subtotal2 = number_format((float) $order_content->tax_subtotal2, 2);
			$order_content->tax_subtotal3 = number_format((float) $order_content->tax_subtotal3, 2);
			$order_content->tax_subtotal4 = number_format((float) $order_content->tax_subtotal4, 2);
			$order_content->tax_subtotal5 = number_format((float) $order_content->tax_subtotal5, 2);
			$order_content->itax = number_format((float) $order_content->itax, 2);

			$this->order_contents[] = $order_content->toArray();
		}

		$this->total     = number_format((float) $this->total, 2);
		$this->subtotal  = number_format((float) $this->subtotal, 2);
	}

	public function processOrder($order)
	{
		$now_ts = time();

		// Check-level Discount
		if (!empty($this->discount_id) && $this->discount_level == 'check')
		{
			if ($this->discount_code == 'p')
			{
				$this->discount = ((double) $this->subtotal * (double) $this->discount_calc);
			}
			else if ($this->discount_code == 'd')
			{
				$this->discount = $this->discount_calc;
			}

			$this->subtotal -= $this->discount;
			$this->total    -= $this->discount;
			// TO DO : recalc tax fields
		}

		$order = new Order( array(
			'order_id'        => $this->order_id,
			'location_id'     => $order->loc_id,
			'location'        => '{'. $this->restaurant_id .':'. $this->dataname .':'. $this->loc_id .'}',
			'opened'          => date('Y-m-d H:i:s'),
			'original_id'     => $this->label->componentLayoutID .'|o|'. $this->label->customer .'|o|'. $this->user->fullName .'|o|'. $this->user->_id .'|o|{"loyaltyInfo":{"orderId":"'. $this->orderId .'"}, "print_info":[{"field":"'. $this->label->customer .'", "print":"b", "info":"'. str_replace('"', '', $this->user->fullName) .'"}]}',  // TO DO : add |*| delimiters for multiple entries (like olo)?
			'ioid'            => $this->ioid,
			'subtotal'        => $this->subtotal,
			'discount'        => number_format((float) $this->discount, 2),
			'discount_id'     => number_format((float) $this->discount_id, 2),
			'discount_value'  => number_format((float) $this->discount_value, 2),
			'tax'             => number_format((float) $this->tax, 2),
			'itax'            => number_format((float) $this->itax, 2),
			'total'           => number_format((float) $this->total, 2),
			'alt_paid'        => number_format((float) $this->total, 2),
			'other_tip'       => $this->tip,
			'cashier_id'      => '0',
			'register'        => $this->label->appName,  // TO DO : see if we should really look up register
			'register_name'   => $this->label->appName,
			'server'          => $this->label->appName,  // TO DO : see if this will get overwritten on checkout by server manning register
			//'server_id'     => $serverIdForPepperApp,  // TO DO : see if we should query for this user
			'guests'          => '1',
			'tablename'       => $this->label->appName,
			'togo_status'     => '1',
			'togo_name'       => $this->user->fullName,
			'togo_time'       => date('Y-m-d H:i:s', strtotime('+15 minutes')),  // TO DO : timezone, get time from payload (if Pepper sends it), or from config setting (?)
			'togo_type'       => $this->label->appName,
		));

		$this->order = $order->toArray();

		$split_check_detail = new SplitCheckDetail( array(
			'order_id'          => $this->order_id,
			'ioid'              => $this->ioid,
			'loc_id'            => $this->loc_id,
			'check'             => '1',
			'subtotal'          => number_format((float) $this->subtotal, 2),
			'discount'          => number_format((float) $this->discount, 2),
			'discount_id'       => number_format((float) $this->discount_id, 2),
			'discount_value'    => number_format((float) $this->discount_value, 2),
			'idiscount_amount'  => number_format((float) $this->idiscount, 2),
			'tax'               => number_format((float) $this->tax, 2),
			'itax'              => number_format((float) $this->itax, 2),
			'check_total'       => number_format((float) $this->total, 2),
			'alt_paid'          => number_format((float) $this->total, 2),
			'loyalty_info'      => $this->label->loyaltyInfo .'|'. $this->user->fullName .'|'. $this->user->_id,
		));

		$this->split_check_details[] = $split_check_detail->toArray();

		return $this->error;
	}

	private function processPayments($order)
	{
		$now_ts = time();
		$cc_transaction = new CcTransaction( array(
			'order_id'          => $this->order_id,
			'ioid'              => $this->ioid,
			'loc_id'            => $this->loc_id,
			'action'            => 'Sale',
			'check'             => '1',
			'amount'            => number_format($this->total, 2),
			'total_collected'   => number_format($this->total, 2),
			'processed'         => '1',
			'datetime'          => date('Y-m-d H:i:s'),  // TO DO : timezone, client vs. server time
			'pay_type'          => $this->label->appPayType,
			'pay_type_id'       => $this->app_pay_type_id,  // TO DO : logic for points vs. app pay
			'got_response'      => '1',   // TO DO : check on this
			'tip_amount'        => number_format($order->tipAmount, 2),
			//'tip_for_id'      => '0',   // TO DO : ensure this ends up being set to the server at the cash register who checks the person out
			'server_time'       => date('Y-m-d H:i:s'),  // TO DO : mysql now()
			'signature'         => '0',
			'device_udid'       => $this->newInternalID(),  // TO DO : see if pepper will send
			'internal_id'       => $this->newInternalID(),
			'last_mod_ts'       => $now_ts,
		));
		$this->cc_transactions[] = $cc_transaction->toArray();
	}

	public function getResponse()
	{
		/*return array(
			'order' => array(
				'id' => $this->order_id,
			),
			'payment' => array(
				'id'     => $this->order_id,
				'amount' => $this->total,
				'tip'    => $this->tip,
			),
		);*/

		// Should only ever have one of these because we insert one payment for the amount of the entire order
		$cc_transaction = array_shift($this->cc_transactions);

		// Pepper asked for XML to be returned
		$response = "\n-------------------------- Row 1 ---------------------------\n";
		$response .= "<result><id>{$this->id}</id><order_id>{$this->order_id}</order_id><payment><id>{$cc_transaction['id']}</id><amount>{$this->total}</amount><tip>{$this->tip}</tip></payment></result>";

		return $response;
	}
}