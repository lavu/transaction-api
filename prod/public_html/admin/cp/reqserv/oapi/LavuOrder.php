<?php

require_once('/home/poslavu/public_html/admin/cp/resources/core_functions.php');
require_once('/home/poslavu/public_html/admin/cp/reqserv/oapi/Order.php');
require_once('/home/poslavu/public_html/admin/cp/reqserv/oapi/OrderContent.php');
require_once('/home/poslavu/public_html/admin/cp/reqserv/oapi/SplitCheckDetail.php');
require_once('/home/poslavu/public_html/admin/cp/reqserv/oapi/CCTransaction.php');
require_once('/home/poslavu/public_html/admin/cp/reqserv/oapi/ModifierUsed.php');

class LavuOrder
{
	protected $dataname;
	protected $location;
	protected $restaurant_id;
	protected $order_id;
	protected $loc_id;
	protected $ioid;

	protected $total;
	protected $subtotal;
	protected $tax;
	protected $itax;
	protected $tip;

	protected $order;
	protected $order_contents;
	protected $split_check_details;
	protected $cc_transactions;
	protected $modifiers_used;

	protected $tax_rates;

	protected $discount;
	protected $discount_calc;
	protected $discount_code;
	protected $discount_id;
	protected $discount_sh;
	protected $discount_info;
	protected $discount_type;
	protected $discount_level;
	protected $discount_value;
	protected $idiscount;

	protected $success;
	public    $error;

	protected $dbconn;

	public function __construct($args, $order)
	{

		$this->dataname = $args['dataname'];
		$this->location = $this->getLocation($this->dataname);
		$this->restaurant_id = empty($args['restaurant_id']) ? $this->getRestID($this->dataname) : $args['restaurant_id'];
		$this->loc_id = $this->location['id'];

		empty($this->location['timezone']) || date_default_timezone_set($this->location['timezone']);

		$this->success = false;
		$this->error_msg = '';

		// Insert order row into db right away to lock in our actual, unique order_id to use in other methods
		$this->ioid = $this->newInternalID();
		$this->order_id = $this->insertOrderStub();

		$this->total = 0.00;
		$this->subtotal = 0.00;
		$this->discount = 0.00;
		$this->idiscount = 0.00;
		$this->tax = 0.00;
		$this->itax = 0.00;
		$this->tip = 0.00;

		$this->order_contents = array();
		$this->split_check_details = array();
		$this->cc_transactions = array();
		$this->modifiers_used = array();

		// Process the order contents first (which also covers tax and discounts) to total up the line items so we can process the order and then payments
		$this->processOrderContents($order);  // TO DO : add error check so we bail out if the order stub wasn't created
		$this->processOrder($order);
		$this->processPayments($order);
	}

	public function insertOrderStub()
	{
		// standard prefix for API-created orders
		$order_prefix = '7-';  // MAYBE : make this configurable
		$insert_id = null;

		// Try 10 times to get a unique order_id where we only insert on the first time and then keep updating that order_id until there are no conflicts.
		// Inserting will help stake our claim on a unique order_id, in case there are simultaneous API calls.
		$order = array();
		for ($i = 1; $i <= 50; $i++)
		{
			$order_prefix_check = $this->queryDatabase('order-highest-sequence-number', array('dataname' => $this->dataname, 'prefix' => $order_prefix));
			$sequence_number = empty($order_prefix_check) ? 0 : (int) $order_prefix_check['sequence_number'];
			$sequence_number += $i;
			$order_id = $order_prefix . $sequence_number;

			if (empty($insert_id))
			{
				// Insert orders row
				$order['order_id'] = $order_id;
				$order['location_id'] = $this->loc_id;
				$order['location'] = '{'. $this->restaurant_id .':'. $this->dataname .':'. $this->loc_id .'}';
				$order['closed'] = 'INPROGRESS';  // `closed` of blank or 0000-00-00 00:00:00 will get picked up in open orders query
				$order['last_mod_device'] = 'API CREATE_ORDER';

				$insert_ok = $this->queryDatabase('orders-insert', array('database_table' => 'orders', 'dataname' => $this->dataname), $order);
				$insert_id = ($insert_ok) ? lavu_insert_id() : null;
				$this->id = $insert_id;
			}
			else
			{
				// Update already-inserted order row
				$order['order_id'] = $order_id;
				$this->queryDatabase('orders-update', array('database_table' => 'orders', 'dataname' => $this->dataname, 'id' => $this->id), $order);
			}

			// Make sure our order_id wasn't duplicated in the time that we inserted it
			$order_check = $this->queryDatabase('order', array('dataname' => $this->dataname, 'order_id' => $order_id));

			// Break out of this loop if our order_id is still unique, otherwise keep trying to generate the next order in the sequence
			if (mysqli_num_rows($order_check) === 1)
			{
				///$this->id = $insert_id;
				$this->order_id = $order_id;
				return $order_id;
			}
		}

		return $order_id;
	}

	public function processOrder($order)
	{
		// TO DO : populate protected vars
	}

	protected function applyTax($order)
	{
		// TO DO

		// split_check_details->tax_info = Sales|-|.05125|-|9.51|-|0.49|*|
	}

	protected function applyDiscounts($order)
	{
		// TO DO
	}

	protected function calculateTaxRateForMenuItem($menu_item, $n = 1)
	{
		$tax_rate = 0;
		$tax_profile_id = '';

		$this->getTaxRates();

		// Check menu_item-level
		if (!empty($menu_item['tax_profile_id']) || $menu_item['tax_profile_id'] == '0')
		{
			$tax_profile_id = $menu_item['tax_profile_id'];
		}
		else if (!empty($menu_item['apply_taxrate']))
		{
			// TO DO
		}
		else if (!empty($menu_item['custom_taxrate']))
		{
			$tax_rate = (double) $menu_item['custom_taxrate'];
		}

		// Check menu_category (if we have to)
		if (empty($tax_rate) && empty($tax_profile_id) && $tax_profile_id !== '0')
		{
			$menu_category = $this->queryDatabase('menu_category', array('dataname' => $this->dataname, 'id' => $menu_item['category_id']));

			if (!empty($menu_category['tax_profile_id']))
			{
				$tax_profile_id = $menu_category['tax_profile_id'];
			}
			else if (!empty($menu_category['apply_taxrate']))
			{
				// TO DO
			}
			else if (!empty($menu_category['custom_taxrate']))
			{
				$tax_rate = (double) $menu_category['custom_taxrate'];
			}
		}

		// If we've got a tax_profile_id from our searches but still no tax_rate, use it to find the tax_rate
		if (empty($tax_rate) && !empty($tax_profile_id))
		{
			foreach ($this->tax_rates as $i => $tax_rate_row)
			{
				// $i/$n correspond to taxN, tax_rateN, tax_subotalN
				//if (intval($i) !== intval($n)) continue;

				if (intval($tax_rate_row['profile_id']) == intval($tax_profile_id))
				{
					$tax_rate = (double) $tax_rate_row['calc'];
					break;  // TO DO : see if we need to add support for multiple rate rates, liked stacked taxes (I don't think Pepper supports it)
				}
			}
		}

		return $tax_rate;
	}

	protected function calculateTaxAmountForMenuItem($menu_item, $tax_rate = 0.00, $taxed_amount = 0.00)
	{
		if (empty($tax_rate))
		{
			$tax_rate = $this->calculateTaxRateForMenuItem($menu_item);
		}

		$tax = ($taxed_amount * $tax_rate);

		return $tax;
	}

	protected function calculateIncludedTaxAmountForMenuItem($menu_item, $taxed_amount = 0.00)
	{
		$tax_rate = $this->calculateTaxRateForMenuItem($menu_item);
		$itax = $taxed_amount - ($taxed_amount / (1.0 + $tax_rate));

		return $itax;
	}

	public function commit()
	{
		// TO DO : add error check so we bail out if the order stub wasn't created

		// Insert order_contents, split_check_details, cc_transactions, modifiers_used;
		$this->insertRows('order_contents', $this->order_contents);
		$this->insertRows('split_check_details', $this->split_check_details);
		$this->insertRows('cc_transactions', $this->cc_transactions);
		$this->insertRows('modifiers_used', $this->modifiers_used);

		// Update order time stamps and flag record for pick-up by ipad sync and KDS / printer auto-sending, now that the order's constituent parts are complete
		$now_ts = time();
		$this->order['id'] = $this->id;
		$this->order['closed'] = '0000-00-00 00:00:00';
		$this->order['auto_send_status'] = '1';
		$this->order['last_mod_device'] = 'API CREATE_ORDER';
		$this->order['last_mod_ts'] = $now_ts;
		$this->order['pushed_ts'] = $now_ts;

		$this->updateRow('orders', $this->order);

		return $this->getResponse();
	}

	protected function getLocation($dataname)
	{
		$location = $this->queryDatabase('location_highest_nondeleted', array('dataname' => $dataname));
		return $location;
	}

	protected function getLocID($dataname)
	{
		$location = $this->queryDatabase('location_highest_nondeleted', array('dataname' => $dataname));
		$this->location = $location;
		return $location['id'];
	}

	protected function getRestID($dataname)
	{
		$restaurant = $this->queryDatabase('restaurant', array('dataname' => $dataname));
		return $restaurant['id'];
	}

	protected function getForcedModifier($id)
	{
		return $this->queryDatabase('forced_modifier', array('dataname' => $this->dataname, 'id' => $id));
	}

	protected function getOptionalModifier($id)
	{
		return $this->queryDatabase('modifier', array('dataname' => $this->dataname, 'id' => $id));
	}

	protected function getMenuItem($id)
	{
		return $this->queryDatabase('menu_item', array('dataname' => $this->dataname, 'id' => $id));
	}

	protected function getApplyTaxRate($menu_item)
	{
		if ($menu_item['apply_taxrate'] == 'Custom') {
			$return = '0::Tax::'.$menu_item['custom_taxrate'].'::0::::::::0';
		} else {
			$profile_id = $menu_item['tax_profile_id'];
			if ($profile_id == '') {
				$menu_category = $this->queryDatabase('menu_category', array('dataname' => $this->dataname, 'id' => $menu_item['category_id']));
				$profile_id = $menu_category['tax_profile_id'];
			}
			if ($profile_id == 0) {
				$return = '0::Tax::0::0::::::::0';
			} else {
				$apply_taxrate = '';
				$tax_rates_data = $this->queryDatabase('tax_rates_by_profile_id', array('dataname' => $this->dataname, 'profile_id' => $profile_id));
				foreach ($tax_rates_data as $tax_rates) {
					$apply_taxrate .= $tax_rates['profile_id'] . '::' . $tax_rates['title'] . '::' . $tax_rates['calc'] . '::' . $tax_rates['stack'] . '::::::::' . $tax_rates['id'].';;';
				}
				$return = trim($apply_taxrate, ";;");
			}
		}
		return $return;
	}

	protected function getIncludedTaxFlag($menu_item)
	{
		// Check menu_item-level itax flag
		$has_itax = ($menu_item['tax_inclusion'] == '1');

		// Check menu_category-level itax flag (tax_inclusion = 0 means do not pass-thru to Category)
		if (!$has_itax && ($menu_item['tax_inclusion'] === 'Default' || $menu_item['tax_inclusion'] === ''))
		{
			$menu_category = $this->queryDatabase('menu_category', array('dataname' => $this->dataname, 'id' => $menu_item['category_id']));
			$has_itax = ($menu_category['tax_inclusion'] == '1');

			// Check location-level itax flag
			if (!$has_itax && ($menu_category['tax_inclusion'] === 'Default' || $menu_category['tax_inclusion'] === ''))
			{
				$has_itax = ($this->location['tax_inclusion'] == '1');
			}
		}

		return $has_itax;
	}

	protected function getPrinter($menu_item)
	{
		$printer = isset($menu_item['printer']) ? $menu_item['printer'] : '';  // Blank means no printer

		// Check for Category pass-thru
		if ($printer === '0')
		{
			$menu_category = $this->queryDatabase('menu_category', array('dataname' => $this->dataname, 'id' => $menu_item['category_id']));
			$printer = $menu_category['printer'];
		}

		return $printer;
	}

	protected function getResponse()
	{
		return array(
			'success' => $this->success,
			'error'   => $this->error
		);
	}

	protected function getTaxRates()
	{
		if (empty($this->tax_rates))
		{
			$n = 1;
			$tax_rates = $this->queryDatabase('tax_rates', array('dataname' => $this->dataname));
			foreach ($tax_rates as $tax_rate)
			{
				// $n corresponds to taxN, tax_rateN, tax_subtotalN
				$this->tax_rates[$n++] = $tax_rate;
			}
		}

		return $this->tax_rates;
	}

	protected function insertRows($dbtable, $rows)
	{
		foreach($rows as $row)
		{
			$row['order_id'] = $this->order_id;
			$row_id = $this->queryDatabase("{$dbtable}-insert", array('dataname' => $this->dataname, 'database_table' => $dbtable), $row);

			// Improve this later
			if ($dbtable == 'cc_transactions' && isset($this->cc_transactions[0]))
			{
				$this->cc_transactions[0]['id'] = lavu_insert_id();
			}
		}
	}

	protected function updateRow($dbtable, $row)
	{
		// Unset the ID field so we don't update it but save it so we can pass it in as an arg
		$id = isset($row['id']) ? $row['id'] : null;
		unset($row['id']);

		$row['order_id'] = $this->order_id;
		$this->queryDatabase("{$dbtable}-update", array('dataname' => $this->dataname, 'database_table' => $dbtable, 'id' => $id), $row);
	}

	protected function getInsertFieldsAndValues($fields)
	{
		$insert_fields = '';
		$insert_values = '';

		foreach($fields as $key => $val)
		{
			$key_sql = "`{$key}`";

			// Allow mysql now() to pass thru, otherwise prep for lavuquery field replacement
			$val_sql = (strtolower($val) == 'now()') ? $val : "'[{$key}]'";

			$insert_fields .= empty($insert_fields) ? $key_sql : ', '. $key_sql;
			$insert_values .= empty($insert_values) ? $val_sql : ', '. $val_sql;
		}

		return array($insert_fields, $insert_values);
	}

	protected function getUpdateFieldsAndValues($fields)
	{
		$update_fields = '';

		foreach($fields as $key => $val)
		{
			$key_sql = "`{$key}`";

			// Allow mysql now() to pass thru, otherwise prep for lavuquery field replacement
			$val_sql = (strtolower($val) == 'now()') ? $val : "'[{$key}]'";

			$key_val_sql = $key_sql .'='. $val_sql;
			$update_fields .= empty($update_fields) ? $key_val_sql : ', '. $key_val_sql;
		}

		return $update_fields;
	}

	/**
	 * Handles all database queries and connections
	 *
	 * @param  $query_name String identifying query to run
	 * @param  $args Array of input arguments, if needed for query
	 *
	 * @return mixed Returns mysql resource when $return_single_row is false, array when $return_single_row is true, bool for inserts, updates, failures
	 */
	protected function queryDatabase($query_name, $args = array(), $insert_or_update_fields = array())
	{
		$use_pos_db            = true;
		$return_single_row     = true;
		$return_all_rows_array = false;
		$die_on_zero_rows      = true;
		$die_on_db_errors      = true;
		$update_or_insert      = false;

		switch ( $query_name )
		{
			case 'discount_type':
				$sql = "SELECT * FROM `poslavu_[dataname]_db`.`discount_types` WHERE `id` = '[id]'";
				$die_on_zero_rows  = false;
				$return_single_row = true;
				break;

			case 'restaurant':
				$sql = "SELECT * FROM `poslavu_MAIN_db`.`restaurants` WHERE `data_name` = '[dataname]' ORDER BY `id`";
				$use_pos_db = false;
				break;

			case 'location_highest_nondeleted':
				$sql = "SELECT * FROM `poslavu_[dataname]_db`.`locations` WHERE `_disabled` = 0 ORDER BY `id` DESC";
				$die_on_zero_rows = false;
				break;

			case 'order':
				$sql = "SELECT * FROM `poslavu_[dataname]_db`.`orders` WHERE `order_id` = '[order_id]'";
				$die_on_zero_rows  = false;
				$return_single_row = false;
				break;

			case 'order-highest-sequence-number':
				$sql = "SELECT SUBSTRING_INDEX(SUBSTRING_INDEX(`order_id`, '-', 2), '-', -1) AS `sequence_number` FROM `poslavu_[dataname]_db`.`orders` WHERE `order_id` LIKE '[prefix]%' ORDER BY cast(SUBSTRING_INDEX(SUBSTRING_INDEX(`order_id`, '-', 2), '-', -1) AS UNSIGNED) DESC LIMIT 1";
				$return_single_row = true;
				$die_on_zero_rows = false;
				break;

			case 'orders-insert':
			case 'order_contents-insert':
			case 'split_check_details-insert':
			case 'cc_transactions-insert':
			case 'modifiers_used-insert':
				$args = array_merge($args, $insert_or_update_fields);
				list($insert_fields, $insert_values) = $this->getInsertFieldsAndValues($insert_or_update_fields);
				$sql = "INSERT INTO `poslavu_[dataname]_db`.`[database_table]` ({$insert_fields}) VALUES ({$insert_values})";
				$update_or_insert = true;
				break;

			case 'orders-update':
				$args = array_merge($args, $insert_or_update_fields);
				$update_fields = $this->getUpdateFieldsAndValues($insert_or_update_fields);
				$sql = "UPDATE `poslavu_[dataname]_db`.`[database_table]` SET {$update_fields} WHERE `order_id` = '[order_id]' AND `id` = [id]";
				$update_or_insert = true;
				break;

			case 'forced_modifier':
				$sql = "SELECT * FROM `poslavu_[dataname]_db`.`forced_modifiers` WHERE `id` = '[id]' ORDER BY `id`";
				$die_on_zero_rows  = false;
				break;

			case 'modifier':
				$sql = "SELECT * FROM `poslavu_[dataname]_db`.`modifiers` WHERE `id` = '[id]' ORDER BY `id`";
				$die_on_zero_rows  = false;
				break;

			case 'menu_item':
				$sql = "SELECT * FROM `poslavu_[dataname]_db`.`menu_items` WHERE `id` = '[id]'";
				$die_on_zero_rows  = false;
				$return_single_row = true;
				break;

			case 'menu_category':
				$sql = "SELECT * FROM `poslavu_[dataname]_db`.`menu_categories` WHERE `id` = '[id]' ORDER BY `_order`, `id`";
				$die_on_zero_rows  = false;
				$return_single_row = true;
				break;

			case 'payment_type':
				$sql = "SELECT * FROM `poslavu_[dataname]_db`.`payment_types` WHERE `special` = '[special]' AND `type` LIKE '%[pay_type]%' AND `loc_id` = '[loc_id]' AND `_deleted` = 0";
				$die_on_zero_rows  = false;
				$return_single_row = true;
				break;

			case 'tax_profiles':
				$sql = "SELECT * FROM `poslavu_[dataname]_db`.`tax_profiles` WHERE `_deleted` = 0 ORDER BY `id`";
				$die_on_zero_rows  = false;
				$return_all_rows_array = true;
				break;

			case 'tax_rates_by_profile_id':
				$sql = "SELECT * FROM `poslavu_[dataname]_db`.`tax_rates` WHERE `profile_id` = '[profile_id]' AND `_deleted` = 0 ORDER BY `id`";
				$die_on_zero_rows  = false;
				$return_all_rows_array = true;
				break;

			case 'tax_rates':
				$sql = "SELECT * FROM `poslavu_[dataname]_db`.`tax_rates` WHERE `_deleted` = 0 ORDER BY `id`";
				$die_on_zero_rows  = false;
				$return_all_rows_array = true;
				break;

			default:
				die(__METHOD__ ." got unrecognized query {$query_name} for dataname {$this->dataname}\n");
				break;
		}

		// Run query
		if ($use_pos_db)
		{
			// lavuquery needs global $data_name to create client db connection
			global $data_name;
			$data_name = $args['dataname'];

			$result = lavu_query($sql, $args);
		}
		else
		{
			$result = mlavu_query($sql, $args);
		}

		// Process results
		if ( $result === false && $die_on_db_errors )
		{
			$error_msg = ($use_pos_db) ? lavu_dberror() : mlavu_dberror();
			throw new Exception("Database error - {$query_name} query got: {$error_msg}\n");
		}
		else if ( $result !== false && $return_all_rows_array )
		{
			$all_rows = array();
			while ( $row = mysqli_fetch_assoc($result) )
			{
				$all_rows[] = $row;
			}
			return $all_rows;
		}
		else if ( $update_or_insert )
		{
			return $result;
		}
		else if ( mysqli_num_rows($result) == 0 && $die_on_zero_rows )
		{
			error_log("No row for '{$query_name}' query for {$this->dataname}: sql={$sql} args=". json_encode($args));
			throw new Exception("Database error - No row for '{$query_name}' query for {$this->dataname}: sql={$sql} args=". json_encode($args));
		}

		return ($return_single_row) ? mysqli_fetch_assoc($result) : $result;
	}

}