<?php

require_once('/home/poslavu/public_html/admin/cp/reqserv/oapi/OrderApiObject.php');

class CCTransaction extends OrderApiObject
{
	function __construct($args)
	{
		// Defaults
		$this->data = array(
			'order_id'          => null,
			'ioid'              => null,
			'loc_id'            => '1',
			'check'             => '1',
			'amount'            => '0.00',
			'refunded'          => '0',
			'refunded_by'       => '0',
			'auth'              => '0',
			'processed'         => '0',
			'datetime'          => date('Y-m-d H:i:s'),  // TO DO : timezone, client vs. server time
			'pay_type'          => null,
			'voided'            => '0',
			'voided_by'         => '0',
			'got_response'      => '0',
			'voice_auth'        => '0',
			'tip_for_id'        => '0',
			'server_time'       => null,
			'signature'         => '0',
			'device_udid'       => null,
			'internal_id'       => null,
			'last_mod_ts'       => null,
		);

		// Override/add with any passed-in values
		$this->data = $args + $this->data;
	}
}