<?php

require_once('/home/poslavu/public_html/admin/cp/reqserv/oapi/OrderApiObject.php');

class ModifierUsed extends OrderApiObject
{
	function __construct($args)
	{
		// Defaults
		$this->data = array(
			'order_id' => null,
			'loc_id' => '1',
			'mod_id' => null,
			'qty' => '1',
			'type' => null,
			'row' => '0',
			'cost' => null,
			'unit_cost' => null,
			'ioid' => null,
			'icid' => null,
			'title' => null,
			'list_id' => '0',
		);

		// Override/add with any passed-in values
		$this->data = $args + $this->data;
	}
}