<?php

require_once('/home/poslavu/public_html/admin/cp/reqserv/oapi/OrderApiObject.php');

class SplitCheckDetail extends OrderApiObject
{
	function __construct($args)
	{
		// Defaults
		$this->data = array(
			'order_id'          => null,
			'loc_id'            => '1',
			'check'             => '1',
			'subtotal'          => '0.00',
			'discount'          => '0.00',
			'tax'               => '0.00',
			'gratuity'          => '0.00',
			'cash'              => '0.00',
			'card'              => '0.00',
			'gift_certificate'  => '0.00',
			'refund'            => '0.00',
			'remaining'         => '0.00',
			'check_total'       => '0.00',
			'change'            => '0.00',
			'refunded'          => '0',
			'refund_cc'         => '0.00',
			'cash_applied'      => '0.00',
			'rounding_amount'   => '0.00',
			'checked_out'       => '0',
			'refund_gc'         => '0.00',
			'alt_paid'          => '0.00',
			'alt_refunded'      => '0.00',
			'idiscount_amount'  => '0.00',
			'itax'              => '0.00',
			'idiscount_amount'  => '0.00',
		);

		// Override/add with any passed-in values
		$this->data = $args + $this->data;
	}
}