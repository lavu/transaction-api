<?php

require_once('/home/poslavu/public_html/admin/cp/reqserv/oapi/OrderApiObject.php');

class Order extends OrderApiObject
{
	function __construct($args)
	{
		// Defaults
		$this->data = array(
			'order_id'                 => null,
			'location_id'              => '1',
			'location'                 => '',  // <restaurant_id>:<dataname>:<loc_id> but we can't pass those fields in the args otherwise it might try to insert those fields, so we're just keeping this placeholder here as a reminder to set the field in args
			'opened'                   => date('Y-m-d H:i:s'),  // TO DO : timezone, client vs. server time
			'closed'                   => 'INPROGRESS',
			'order_status'             => 'open',
			'ioid'                     => '1',
			'no_of_checks'             => '1',
			'send_status'              => '1',
			'subtotal'                 => '0.00',
			'tax'                      => '0.00',
			'total'                    => '0.00',
			'discount'                 => '0.00',
			'gratuity'                 => '0',
			'card_gratuity'            => '0.00',
			'card_paid'                => '0.00',
			'change_amount'            => '0.00',
			'reopen_refund'            => '0',
			'void'                     => '0',
			'auth_by_id'               => '0',
			'permission'               => '0',
			'check_has_printed'        => '0',
			'multiple_tax_rates'       => '0',
			'tab'                      => '0',
			'deposit_status'           => '0',
			'refunded'                 => '0.00',
			'refunded_cc'              => '0.00',
			'refunded_by'              => '0',
			'refunded_by_cc'           => '0',
			'subtotal_without_deposit' => '0.00',
			'auto_gratuity_is_taxed'   => '0',
			'refunded_gc'              => '0.00',
			'alt_paid'                 => '0.00',
			'alt_refunded'             => '0.00',
			'last_course_sent'         => '0',
			'tax_exempt'               => '0',
			'recloser_id'              => '0',
			'checked_out'              => '0',
			'itax'                     => '0.00',
			'force_closed'             => '0.00',
			'serial_no'                => '0',
			'check_has_printed'        => '0',
		);

		// Override/add with any passed-in values
		$this->data = $args + $this->data;
	}
}