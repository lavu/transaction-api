<?php

class OrderApiObject
{
	protected $data;

	public function __construct($args = array())
	{
		$this->data = $args;
	}

	public function __set($fieldName, $fieldValue)
	{
		$this->data[$fieldName] = $fieldValue;
	}

	public function &__get($fieldName)
	{
		return $this->data[$fieldName];
	}

	public function __isset($fieldName)
	{
		return isset($this->data[$fieldName]);
	}

	public function toArray()
	{
		return $this->data;
	}
}