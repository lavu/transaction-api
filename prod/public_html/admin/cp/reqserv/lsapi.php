<?php


	//LLS's contact reqserv/index.php which then includes this file.
	ini_set('memory_limit','64M');
	if( in_array($_SERVER['REMOTE_ADDR'], array( '24.234.184.98', '65.125.117.178', '216.243.114.158' ))) {
		error_log( $_SERVER['REMOTE_ADDR'] . ': ' . $dataname . " - ". $cmd);
	}

//error_log(print_r($_REQUEST['dataname',1));
//

	if(!isset($cmd)) {
		checkin_before_close();
		exit();
	}

	$cmd_descriptor = $cmd;
	if(isset($_POST['table']))
		$cmd_descriptor .= " " . $_POST['table'];
	else if(isset($_POST['tablename']))
		$cmd_descriptor .= " " . $_POST['tablename'];
	if(isset($_POST['lastid']))
		$cmd_descriptor .= " " . $_POST['lastid'];

	if(trim($dataname)=="")
	{
		checkin_before_close();
		exit();
	}
	$lldir1 = "/home/poslavu/logs/api/" . substr($dataname,0,1);
	$lldir2 = $lldir1 . "/" . $dataname;
	$use_llfile = false;
	if(!is_dir($lldir2))
	{
		if(!is_dir($lldir1))
		{
			mkdir($lldir1,0755);
		}
		mkdir($lldir2,0755);
		if(is_dir($lldir2))
		{
			$llfile = $lldir2 . "/last_ll_sync_" . str_replace(" ","_",$cmd_descriptor);
			$lfp = fopen($llfile,"w");
			fwrite($lfp,time());
			fclose($lfp);
		}

		$llfile = "";
		$last_ll_sync = time();
	}
	else
	{
		$llfile = $lldir2 . "/last_ll_sync_" . str_replace(" ","_",$cmd_descriptor);
		if(!is_file($llfile))
		{
			if(!is_dir($lldir1))
			{
				mkdir($lldir1,0755);
			}
			if(!is_dir($lldir2))
			{
				mkdir($lldir2,0755);
			}
			if(is_dir($lldir2))
			{
				$llfile = $lldir2 . "/last_ll_sync_" . str_replace(" ","_",$cmd_descriptor);
				$lfp = fopen($llfile,"w");
				fwrite($lfp,time() - 100);
				fclose($lfp);
			}
		}

		$lfp = fopen($llfile,"r");
		$fsize = filesize($llfile);
		$lfpstr = "";
		if ($fsize > 0) $lfpstr = fread($lfp, $fsize);
		fclose($lfp);

		if(is_numeric($lfpstr))
		{
			$last_ll_sync = $lfpstr;
			$use_llfile = true;
		}
	}

	if(!$use_llfile)
	{
		// last sync time
		$lsync_query = lavu_query("select SQL_NO_CACHE * from `config` where `location`='[1]' and `setting`='last_ll_sync [2]' order by id asc limit 1",$locationid,$cmd_descriptor);
		if(mysqli_num_rows($lsync_query))
		{
			$lsync_read = mysqli_fetch_assoc($lsync_query);
			$last_ll_sync = $lsync_read['value'];
		}
		else
		{
			$last_ll_sync = 0;
			lavu_query("insert into `config` (`location`,`setting`,`value`) values ('[1]','[2]','[3]')",$locationid,"last_ll_sync ".$cmd_descriptor,time());
		}
	}

	if($dataname=="farinha")
	{
		//mail("corey@lavu.com","$dataname sync",$cmd_descriptor . " - " . time() . " - " . $last_ll_sync . " (".(time() - $last_ll_sync).")","From: sync@poslavu.com");
	}
	if($_SERVER['REMOTE_ADDR']=="209.112.209.184" || $_SERVER['REMOTE_ADDR']=="61.4.77.14")//189.234.192.214")
	{
		//mail("corey@lavu.com","blocking ip","ipaddress: " . $_SERVER['REMOTE_ADDR'] ."\ndataname: $dataname\n","From: block@poslavu.com");
		checkin_before_close();
		exit();
		//mail("corey@poslavu.com","Ls-sync - $dataname","$dataname $cmd_descriptor","From: info@poslavu.com");
	}

	function get_current_config_setting($locationid,$setting,$key="value")
	{
		$config_query = lavu_query("select SQL_NO_CACHE * from `config` where `location`='[1]' and `setting`='[2]' order by id desc limit 1",$locationid,$setting);
		if(mysqli_num_rows($config_query))
		{
			$config_read = mysqli_fetch_assoc($config_query);
			if($key=="" || $key=="all")
				return $config_read;
			else
				return $config_read[$key];
		}
		else return false;
	}

	function update_sync_config_setting($locationid,$setting,$short_vals,$long_val="",$long_val2="")
	{
		global $dataname;

		if(count($short_vals) > 0) $value = $short_vals[0]; else $value = "";
		if(count($short_vals) > 1) $value2 = $short_vals[1]; else $value2 = "";
		if(count($short_vals) > 2) $value4 = $short_vals[2]; else $value4 = "";
		if(count($short_vals) > 3) $value5 = $short_vals[3]; else $value5 = "";
		if(count($short_vals) > 4) $value6 = $short_vals[4]; else $value6 = "";

		$value3 = $long_val;
		$value_long = $long_val2;

		$set_vals = array();
		$set_vals['locationid'] = $locationid;
		$set_vals['setting'] = $setting;
		$set_vals['value'] = $value;
		$set_vals['value2'] = $value2;
		$set_vals['value3'] = $value3;
		$set_vals['value4'] = $value4;
		$set_vals['value5'] = $value5;
		$set_vals['value6'] = $value6;
		$set_vals['value_long'] = $value_long;

		$rcsync_query = lavu_query("select SQL_NO_CACHE * from `config` where `location`='[locationid]' and `setting`='[setting]' order by id asc limit 1",$set_vals);
		if(mysqli_num_rows($rcsync_query))
		{
			$rcsync_read = mysqli_fetch_assoc($rcsync_query);
			$set_vals['id'] = $rcsync_read['id'];
			lavu_query("update `config` set `value`='[value]',`value2`='[value2]',`value3`='[value3]',`value4`='[value4]',`value5`='[value5]',`value6`='[value6]',`value_long`='[value_long]' where `id`='[id]'",$set_vals);
		}
		else
		{
			lavu_query("insert into `config` (`location`,`setting`,`value`,`value2`,`value3`,`value4`,`value5`,`value6`,`value_long`) values ('[locationid]','[setting]','[value]','[value2]','[value3]','[value4]','[value5]','[value6]','[value_long]')",$set_vals);
		}
	}
	
	function get_col_list_from_array($order_cols,$order_read,$ignore_cols="")
	{
		$ignore_cols = explode(",",$ignore_cols);
		if(count($order_cols)<1)
		{
			foreach($order_read as $key => $val)
			{
				if($key!="lastmod" && $key!="lastsync")
				{
					$include_col = true;
					for($n=0; $n<count($ignore_cols); $n++)
					{
						if($ignore_cols[$n]==$key)
							$include_col = false;
					}
					if($include_col)
						$order_cols[] = $key;
				}
			}
		}
		return $order_cols;
	}
	
	function get_val_list_from_arrays($order_cols,$order_read,$ignore_cols="")
	{
		$ignore_cols = explode(",",$ignore_cols);
		$val_list = "";
		$ncount = 0;
		for($n=0; $n<count($order_cols); $n++)
		{
			$colname = $order_cols[$n];
			if (empty($colname))
			{
				continue;
			}

			$include_col = true;
			for($m=0; $m<count($ignore_cols); $m++)
			{
				if($ignore_cols[$m]==$colname)
					$include_col = false;
			}
			if($include_col)
			{
				if($ncount > 0)
					$val_list .= "[_col_]";
				$val_list .= $order_read[$colname];
				$ncount++;
			}
		}
		return $val_list;
	}
	
	function output_array_cols($order_cols)
	{
		$str_cols = "";
		for($i=0; $i<count($order_cols); $i++)
		{
			$key = $order_cols[$i];
			if($str_cols != "")
				$str_cols .= "[_col_]";
			$str_cols .= $key;
		}
		return $str_cols;
	}

	function appendNewApiOrders($loc_id, $known_orders) {
	
		global $dataname;
		
		//error_log("appendNewApiOrders (".$dataname.") - known_orders: ".(empty($known_orders)?"none":$known_orders));
		
		$ts = time();
		$six_hours_ago = date("Y-m-d H:i:s", ($ts - 21600));
		$six_hours_from_now = date("Y-m-d H:i:s", ($ts + 21600));
		$get_timezone = lavu_query("SELECT `timezone` FROM `locations` WHERE `id` = '[1]'", $loc_id);
		if (mysqli_num_rows($get_timezone) > 0) {
			$loc_info = mysqli_fetch_assoc($get_timezone);
			if (!empty($loc_info['timezone'])) {		
				$old_tz = getenv('TZ');
				putenv("TZ=".$loc_info['timezone']);
				$six_hours_ago = strftime("%Y-%m-%d %H:%M:%S", ($ts - 21600));
				$six_hours_from_now = strftime("%Y-%m-%d %H:%M:%S", ($ts + 21600));
				putenv("TZ=$old_tz");
			}
		}
		
		$new_api_orders = "";
		$remove[] = "'";
		$remove[] = '"';
		$known_orders = str_replace($remove, "", $known_orders);		
		$order_query = lavu_query("SELECT * FROM `orders` WHERE `location_id` = '[1]' AND (`order_id` LIKE '2-%' OR `order_id` LIKE '7-%') AND `opened` >= '".$six_hours_ago."' AND `opened` < '".$six_hours_from_now."' AND `order_id` NOT IN ('".str_replace(",", "','", $known_orders)."') ORDER BY `togo_time` ASC, `opened` ASC LIMIT 2", $loc_id);
	
		// consider a conditional order clause if possible so
	
		if (mysqli_num_rows($order_query) > 0) {
			$new_api_orders .= "[_new_api_orders_start_]";

			$orders_cols = array();
			$orders_vals = "";
			$order_contents_cols = array();
			$order_contents_vals = "";
			$modifiers_used_cols = array();
			$modifiers_used_vals = "";
			$transactions_cols = array();
			$transactions_vals = "";
			$split_checks_cols = array();
			$split_checks_vals = "";
			$alt_payments_cols = array();
			$alt_payments_vals = "";

			$new_api_order_ids = array();
			while ($order_read = mysqli_fetch_assoc($order_query)) {
				$new_api_order_ids[] = $order_read['order_id'];
				$orders_cols = get_col_list_from_array($orders_cols, $order_read, "id");
				if ($orders_vals != "") $orders_vals .= "[_row_]";
				$orders_vals .= get_val_list_from_arrays($orders_cols, $order_read, "id");
			}
			$new_api_orders .= "[_api_order_ids_start_]".implode(",", $new_api_order_ids)."[_api_order_ids_end_]";
			$new_api_orders .= "[_orders_cols_start_]".output_array_cols($orders_cols)."[_orders_cols_end_]";
			$new_api_orders .= "[_orders_vals_start_]".$orders_vals."[_orders_vals_end_]";
			
			$orders_str = "'".implode("','", $new_api_order_ids)."'";
			
			$content_query = lavu_query("SELECT * FROM `order_contents` WHERE `loc_id` = '[1]' AND `order_id` IN (".$orders_str.")", $loc_id);
			while($content_read = mysqli_fetch_assoc($content_query)) {
				$order_contents_cols = get_col_list_from_array($order_contents_cols, $content_read, "id");
				if ($order_contents_vals != "") $order_contents_vals .= "[_row_]";
				$order_contents_vals .= get_val_list_from_arrays($order_contents_cols, $content_read, "id");
			}
			$new_api_orders .= "[_order_contents_cols_start_]".output_array_cols($order_contents_cols)."[_order_contents_cols_end_]";
			$new_api_orders .= "[_order_contents_vals_start_]".$order_contents_vals."[_order_contents_vals_end_]";

			$mu_query = lavu_query("SELECT * FROM `modifiers_used` WHERE `loc_id` = '[1]' AND `order_id` IN (".$orders_str.")", $loc_id);
			while($mu_read = mysqli_fetch_assoc($mu_query)) {
				$modifiers_used_cols = get_col_list_from_array($modifiers_used_cols, $mu_read, "id");
				if ($modifiers_used_vals != "") $modifiers_used_vals .= "[_row_]";
				$modifiers_used_vals .= get_val_list_from_arrays($modifiers_used_cols, $mu_read, "id");
			}
			$new_api_orders .= "[_modifiers_used_cols_start_]".output_array_cols($modifiers_used_cols)."[_modifiers_used_cols_end_]";
			$new_api_orders .= "[_modifiers_used_vals_start_]".$modifiers_used_vals."[_modifiers_used_vals_end_]";
								 
			$trans_query = lavu_query("SELECT * FROM `cc_transactions` WHERE `loc_id` = '[1]' AND `order_id` IN (".$orders_str.")", $loc_id);
			while ($trans_read =mysqli_fetch_assoc($trans_query)) {
				$transactions_cols = get_col_list_from_array($transactions_cols, $trans_read, "id");
				if ($transactions_vals != "") $transactions_vals .= "[_row_]";
				$transactions_vals .= get_val_list_from_arrays($transactions_cols, $trans_read, "id");
			}
			$new_api_orders .= "[_transactions_cols_start_]".output_array_cols($transactions_cols)."[_transactions_cols_end_]";
			$new_api_orders .= "[_transactions_vals_start_]".$transactions_vals."[_transactions_vals_end_]";
			
			$split_chk_query = lavu_query("SELECT * FROM `split_check_details` WHERE `loc_id` = '[1]' AND `order_id` IN (".$orders_str.")", $loc_id);
			while ($splitchk_read =mysqli_fetch_assoc($split_chk_query)) {
				$split_checks_cols = get_col_list_from_array($split_checks_cols, $splitchk_read, "id");
				if ($split_checks_vals != "") $split_checks_vals .= "[_row_]";
				$split_checks_vals .= get_val_list_from_arrays($split_checks_cols, $splitchk_read, "id");
			}
			$new_api_orders .= "[_split_checks_cols_start_]".output_array_cols($split_checks_cols)."[_split_checks_cols_end_]";
			$new_api_orders .= "[_split_checks_vals_start_]".$split_checks_vals."[_split_checks_vals_end_]";

			$alt_payments_query = lavu_query("SELECT * FROM `alternate_payment_totals` WHERE `loc_id` = '[1]' AND `order_id` IN (".$orders_str.")", $loc_id);
			while ($alt_payments_read = mysqli_fetch_assoc($alt_payments_query)) {
				$alt_payments_cols = get_col_list_from_array($alt_payments_cols, $alt_payments_read, "id");
				if ($alt_payments_vals != "") $alt_payments_vals .= "[_row_]";
				$alt_payments_vals .= get_val_list_from_arrays($alt_payments_cols, $alt_payments_read, "id");
			}
			$new_api_orders .= "[_alt_payments_cols_start_]".output_array_cols($alt_payments_cols)."[_alt_payments_cols_end_]";
			$new_api_orders .= "[_alt_payments_vals_start_]".$alt_payments_vals."[_alt_payments_vals_end_]";
			
			$new_api_orders .= "[_new_api_orders_end_]";
				
		} else $new_api_order_ids[] = "none";

		//error_log("new_api_orders (".$dataname."): ".implode(", ", $new_api_order_ids));	
		
		return $new_api_orders;
	}

	if($cmd=="sync_updated") // exceptions for the 5 minute rule
	{
		//$local_query = lavu_query("select SQL_NO_CACHE * from `config` where `location`='[1]' and (`setting`='open tunnel' or `setting`='copy remote file') order by id asc limit 1",$locationid);
		$s_check_update_package = "or (`setting`='lls install record' AND `type`='log' AND `value` > '".date("Y-m-d H:i:s",strtotime("-1 day"))."')";
		$s_query_string = "select SQL_NO_CACHE `id`, `setting`, `value`, `value_long` from `config` where `location`='[1]' and (`setting`='open tunnel' or `setting`='copy remote file' {$s_check_update_package}) order by id asc limit 1";
		$local_query = lavu_query($s_query_string,$locationid);

		while ($local_read = mysqli_fetch_assoc($local_query))
		{

			$b_do_exit = FALSE;
			if($local_read['setting']=="open tunnel")
			{
				lavu_query("delete from `config` where `id`='[1]'",$local_read['id']);
				echo "open tunnel||";
				$b_do_exit = TRUE;
			}
			else if($local_read['setting']=="copy remote file")
			{
				lavu_query("delete from `config` where `id`='[1]'",$local_read['id']);
				echo "copy remote file|".trim($local_read['value'])."|".trim($local_read['value_long']);
				$b_do_exit = TRUE;
			}
			else if($local_read['setting']=="lls install record")
			{
				//LsapiFunctions::check_update_package($dataname, $locationid);
			}

			if ($b_do_exit) {
				checkin_before_close();
				exit();
			}
		}
	}
	if($cmd == "company_id_query"){

        $comp_id_query_str = "SELECT `id` FROM `poslavu_MAIN_db`.`restaurants` WHERE `data_name`='[1]'";
        $result = mlavu_query($comp_id_query_str, $_POST['dataname']);// poslavu_MAIN_db
        if(!$result){
            error_log("1 MYSQL Error in lsapi.php, error: " . mlavu_error());
            echo '{"status":"failure","reason":"query broke"}';
            exit();
        }
        if(mysqli_num_rows($result)){
            $row = mysqli_fetch_assoc($result);
            echo '{"status":"success","company_id":"'.$row['id'].'"}';
        }
        else{
            echo '{"status":"failure","reason":"no row for dataname: '.$_POST['dataname'].'"}';
            exit();
        }

	}
	if($cmd == "validate_api_key_token")
	{

		// simply return success
		// passed dataname/api key, token checks

		$authorizedInDB = getLLSAuthorizedValue($dataname);
		//error_log("Authorized... dataname: [$dataname] value: [$authorizedInDB]");
		if($authorizedInDB){
    		echo "success";
		}else{
            $alertDropDownText = "Your account must first be enabled to use Lavu Local Server prior to installation.  Please contact support to enabled your account to use Lavu Local Server.";
    		$textFieldText = "Please contact Lavu Support to enable this account for Lavu Local Server.";
			$returnJSONArr = array("status" => "failed",
                                   "drop_down_text" => $alertDropDownText,
                                   "text_field_text" => $textFieldText);
    		echo json_encode($returnJSONArr);
		}
		exit();

	}

	$sync_diff = (time() - $last_ll_sync);
	$allow_sync = ($sync_diff>50 || in_array($cmd, array("records_check", "read_file_structure", "read file structure", "sync database structure", "get_cckey", "get_repo_file")));

	if ($location_api_debug) {
		error_log("LSAPI ".$dataname." - cmd: ".$cmd." - sync_diff: ".$sync_diff." - allowed: ".($allow_sync?"yes":"no"));
	}

	if (!$allow_sync) {

		checkin_before_close();
		exit();
	
	} else {

		//if($dataname=="p2r_dallas")
			//mail("corey@poslavu.com","Syncing - $dataname $cmd","syncing at " . time(),"From: sync@poslavu.com");
		if ($use_llfile) {

			$lfp = fopen($llfile,"w");
			fwrite($lfp,time());
			fclose($lfp);
		
		} else {

			lavu_query("update `config` set `value`='[1]' where `location`='[2]' and `setting`='last_ll_sync [3]'",time(),$locationid,$cmd_descriptor);
		}
	}
	if($cmd=="records_check")
	{
	

		//checkin_before_close();
		//exit();
		// begin check for last records check
		$lsync_query = lavu_query("select SQL_NO_CACHE * from `config` where `location`='[1]' and `setting`='last_ll_rc' order by id asc limit 1",$locationid);
		if(mysqli_num_rows($lsync_query))
		{
			$lsync_read = mysqli_fetch_assoc($lsync_query);
			$last_ll_rc = $lsync_read['value'];
		}
		else
		{
			$last_ll_rc = 0;
			lavu_query("insert into `config` (`location`,`setting`,`value`) values ('[1]','[2]','[3]')",$locationid,"last_ll_rc",time());
		}
		if(time() - $last_ll_rc <= 900)
		{
			checkin_before_close();
			exit();
		}
		// end check for last records check
		lavu_query("update `config` set `value`='[1]' where `location`='[2]' and `setting`='last_ll_rc'",time(),$locationid);

		$start_ts = time();
		function run_records_check_for_orders($locationid,$compare_str,$records,$startvals)
		{
			global $dataname;
			$no_match_list = $startvals['no_match_list'];
			$no_match_details = $startvals['no_match_details'];
			$no_match_count = $startvals['no_match_count'];
			$debug_info = $startvals['debug_info'];

			$start_point = 0;
			$end_point = count($records);
			if ($end_point > count($records)) $end_point = count($records);

			$order_ids = array();
			$match_vals = array();
			for ($i = $start_point; $i < $end_point; $i++) {
				$record_info = explode("^", $records[$i]);
				if (count($record_info) > 1) {
					$order_id = $record_info[0];				
					$order_ids[] = $order_id;
					$match_vals[$order_id] = $record_info[1];
				}
			}
			
			$found_order_ids = array();
			$compare_str = urldecode($compare_str); #In Api request  its coming as urlencoded
			$compare_query = lavu_query("SELECT ".$compare_str." AS `val`, `order_id` AS `order_id` FROM `orders` WHERE `order_id` IN ('".implode("','", $order_ids)."') AND `location_id` = '[1]'", $locationid);
			
			if (mysqli_num_rows($compare_query)) {
				while ($compare_read = mysqli_fetch_assoc($compare_query)) {
					
					$order_id = $compare_read['order_id'];
					$found_order_ids[] = $order_id;
					$val = $compare_read['val'];
					
					if ($val == $match_vals[$order_id]) $no_match_details .= "<RESULT>MATCHED";
					else {	
						if ($no_match_list != "") $no_match_list .= ",";
						$no_match_list .= $order_id;
						$no_match_count++;
						$no_match_details .= "<RESULT>NOMATCH";
					}
					$no_match_details .= "<ORDERID>".$order_id."<REMOTE_ORDER>".$val."<LOCAL_ORDER>".$match_vals[$order_id]."\n";
				}
			}
			
			foreach ($order_ids as $order_id) {
				if (!in_array($order_id, $found_order_ids)) $no_match_details .= "<RESULT>NOTFOUND<ORDERID>".$order_id."<REMOTE_ORDER><LOCAL_ORDER>".$match_vals[$order_id]."\n";
			}
			
			$open_auths = array();
			$check_open_auths = lavu_query("SELECT `order_id` FROM `cc_transactions` WHERE `loc_id` = '[1]' AND `order_id` NOT IN ('".str_replace(",", "','", $no_match_list)."') AND `pay_type_id` = '2' AND `auth` != '0' AND `got_response` = '1' AND `voided` = '0' AND `datetime` > '[2]' GROUP BY `order_id` ORDER BY `datetime` DESC LIMIT 5", $locationid, date("Y-m-d H:i:s", (time() - 2592000)));
			if (mysqli_num_rows($check_open_auths) > 0) {
				while ($info = mysqli_fetch_assoc($check_open_auths)) {
					$open_auths[] = "'".$info['order_id']."'";
				}
			}
			
			//rlavu_close_db();

			return array("no_match_list"=>$no_match_list,"no_match_details"=>$no_match_details,"no_match_count"=>$no_match_count,"debug_info"=>$debug_info,"open_auths"=>implode(",", $open_auths));
		}

		ini_set("memory_limit","32M");
		$compare_str = postvar("compare");
		$record_list = urldecode(postvar("records")); #In Api request  its coming as urlencoded
		
	
		$recent_check_count = 1200;
		$traverse_check_count = 1200;

		$debug_info = "";
		$no_match_list = "";
		$no_match_details = "";
		$no_match_count = 0;
		$records = explode("|",$record_list);

		$rc_end_point = get_current_config_setting($locationid,"records check endpoint");

		if(!$rc_end_point)
		{
			$start_point = 0;
		}
		else if($rc_end_point >= count($records))
		{
			$start_point = 0;
		}
		else
		{
			$start_point = $rc_end_point;
		}

		$end_point = $start_point + $traverse_check_count;
		if($end_point > count($records))
			$end_point = count($records);

		update_sync_config_setting($locationid,"records check endpoint",array($end_point,count($records),$traverse_check_count,$recent_check_count));

		if($start_point > $recent_check_count)
			$second_check_range = array("start"=>0,"end"=>$recent_check_count);
		else if($start_point > 0 && $start_point <= $recent_check_count)
			$second_check_range = array("start"=>0,"end"=>$start_point);
		else
			$second_check_range = array();

		$check_records_list = array();
		$debug_info .= "Procesing " . $start_point . " to " . $end_point . " / " . count($records) . "\n";
		for($n=$start_point; $n<$end_point; $n++)
			$check_records_list[] = $records[$n];
		if(count($second_check_range) > 0)
		{
			$debug_info .= "Procesing " . $second_check_range['start'] . " to " . $second_check_range['end'] . "\n";
			for($n=$second_check_range['start']; $n<$second_check_range['end']; $n++)
				$check_records_list[] = $records[$n];
		}

		$rc_result = array("no_match_list"=>$no_match_list,"no_match_details"=>$no_match_details,"no_match_count"=>$no_match_count,"debug_info"=>$debug_info);
		$rc_result = run_records_check_for_orders($locationid,$compare_str,$check_records_list,$rc_result);
		/*if($start_point > 200)
		{
			$rc_result = run_records_check_for_orders($locationid,$compare_str,$records,0,200,$rc_result);
		}
		else if($start_point > 0 && $start_point <= 200)
		{
			$rc_result = run_records_check_for_orders($locationid,$compare_str,$records,0,$start_point,$rc_result);
		}*/
		$no_match_list = $rc_result['no_match_list'];
		$no_match_details = $rc_result['no_match_details'];
		$no_match_count = $rc_result['no_match_count'];
		$debug_info = $rc_result['debug_info'];
		$open_auths = $rc_result['open_auths'];

		if($no_match_details=="")
			$no_match_details = "Couldn't parse record list: \n" . $record_list;
		//if($no_match_list!="" && strlen($no_match_list) > 60)
		//if($dataname=="p2r_murrieta")
		//mail("corey@poslavu.com","NoMatch List - $dataname",$no_match_list,"From: match@poslavu.com");
		$end_ts = time();
		$span_ts = ($end_ts - $start_ts);
		$debug_info .= "Execution Time: " . $span_ts . " second";
		if($span_ts != 1) $debug_info .= "s";
		$debug_info .= "\n";
		if($dataname=="apple_inc" || $dataname=="DEVKART" || $dataname=="p2r_corona")
		{
			//mail("corey@poslavu.com","Records Check $dataname ($start_point to $end_point) - " . count($records),$debug_info . "\n\n" . $no_match_list . "\n\n" . $no_match_details,"From:check@poslavu.com");
		}

		$show_ranges = "";
		$short_vals = array(time(),$_SERVER['REMOTE_ADDR'],$compare_str,date("Y-m-d H:i:s"),$show_ranges);
		echo "1|" . $no_match_list."|".$open_auths;
		checkin_before_close();
	}
	else if($cmd=="get_cckey")
	{
		$key_query = mlavu_query("select AES_DECRYPT(jkey,'".integration_keystr2()."') as `jkey` from `poslavu_MAIN_db`.`restaurants` where `data_name`='[1]'",$dataname);
		if(mysqli_num_rows($key_query))
		{
			$key_read = mysqli_fetch_assoc($key_query);
			$jkey = strrev(substr($key_read['jkey'],0,5)) . strrev(substr($key_read['jkey'],5));

			echo "1|".$jkey;
		}
		checkin_before_close();
	}
	else if($cmd=="set_netpath") // set netpath for LLS setup process
	{

		// prepend protocol on netpath ip
		$ip_addy = "http://".$ip_addy;

		// record the installation
		$a_insert_vars = array('location'=>$locationid,'setting'=>'lls install record','type'=>'log','value'=>date("Y-m-d H:i:s"),'value2'=>"ip_addy: {$ip_addy}");
		$query = lavu_query("SELECT `id` FROM `config` WHERE `location`='[location]' AND `setting`='[setting]' AND `type`='[type]'", $a_insert_vars);
		if ($query !== FALSE && mysqli_num_rows($query) > 0) {
			$query_read = mysqli_fetch_assoc($query);
			lavu_query("UPDATE `config` SET `location`='[location]',`setting`='[setting]',`type`='[type]',`value`='[value]',`value2`='[value2]' WHERE `id`='[id]'", array_merge($a_insert_vars, array('id'=>$query_read['id'])));
		} else {
			lavu_query("INSERT INTO `config` (`location`,`setting`,`type`,`value`,`value2`) VALUES ('[location]','[setting]','[type]','[value]','[value2]')", $a_insert_vars);
		}

		$np_query = lavu_query("UPDATE locations SET use_net_path = 1, net_path = '[1]' WHERE id ='[2]'",$ip_addy,$locationid);
		if($np_query)
		{

			echo "1|success";
		} else {

			echo "0|failure";
		}
		checkin_before_close();
	}
	else if($cmd=="upload_signature")
	{
		$signature_name = postvar("signature_name");
		if(isset($_FILES['signature_file']))
		{
			if(isset($_POST['signature_type'])) $signature_type = $_POST['signature_type'];
			else $signature_type = "regular";

			if($signature_type=="waiver")
				$sig_dirname = "/home/poslavu/public_html/admin/components/lavukart/companies/$dataname/signatures";
			else
				$sig_dirname = "/home/poslavu/public_html/admin/images/$dataname/signatures";
			$val = $_FILES['signature_file'];
			if(!is_dir($sig_dirname))
				mkdir($sig_dirname,0755);
			move_uploaded_file($val['tmp_name'],$sig_dirname . "/".$val['name']);
		}
		checkin_before_close();
	}
	else if($cmd=="get_repo_file")
	{
		$filename = trim(postvar("filename",""));
		$full_filename = dirname(__FILE__) . "/repo/" . $filename;

		if(is_file($full_filename) && $dataname != "84_salval" )
		{
			$fp = fopen($full_filename,"r");
			$str = fread($fp,filesize($full_filename));
			fclose($fp);

			echo "<!--download success-->" . $str;
		}
		else
		{
			error_log("<!--download failed-->".$full_filename);
			echo "";
		}
		checkin_before_close();
	}
	else if($cmd=="transmit_local_file")
	{
		$filename = postvar("filename","");
		$data = postvar("data","");

		$local_query = lavu_query("select SQL_NO_CACHE * from `config` where `location`='[1]' and `setting`='retrieved_local_file' and `value`='[2]' order by id asc limit 1",$locationid,$filename);
		if(mysqli_num_rows($local_query))
		{
			$local_read = mysqli_fetch_assoc($local_query);
			lavu_query("update `config` set `value_long`='[1]', `value2`='[2]', `value4`='[3]' where `setting`='retrieved_local_file' and `value`='[4]'",$data,date("Y-m-d H:i:s"),$_SERVER['REMOTE_ADDR'],$filename);
		}
		else
		{
			lavu_query("insert into `config` (`location`,`setting`,`value`,`value2`,`value4`,`value_long`) values ('[1]','[2]','[3]','[4]','[5]','[6]')",$locationid,"retrieved_local_file",$filename,date("Y-m-d H:i:s"),$_SERVER['REMOTE_ADDR'],$data);
		}
		checkin_before_close();
	}
	else if($cmd=="transmit_query_result")
	{
		$query_command = postvar("query_command","");
		$query_result = postvar("query_result","");

		lavu_query("insert into `config` (`location`,`setting`,`value`,`value2`,`value4`,`value_long`) values ('[1]','[2]','[3]','[4]','[5]','[6]')",$locationid,"local_query_result",$query_command,date("Y-m-d H:i:s"),$_SERVER['REMOTE_ADDR'],$query_result);
		checkin_before_close();
	}
	else if($cmd=="transmit_local_file_structure")
	{
		$data = postvar("data","");
		$tabledata = postvar("tabledata","");

		$local_query = lavu_query("select SQL_NO_CACHE * from `config` where `location`='[1]' and `setting`='local_file_structure' order by id asc limit 1",$locationid);
		if(mysqli_num_rows($local_query))
		{
			$local_read = mysqli_fetch_assoc($local_query);
			lavu_query("update `config` set `value`='[1]', `value2`='[2]', `value3`='[3]', `value_long`='[4]' where `setting`='local_file_structure'",$_SERVER['REMOTE_ADDR'],date("Y-m-d H:i:s"),$tabledata,$data);
		}
		else
		{
			lavu_query("insert into `config` (`location`,`setting`,`value`,`value2`,`value3`,`value_long`) values ('[1]','[2]','[3]','[4]','[5]','[6]')",$locationid,"local_file_structure",$_SERVER['REMOTE_ADDR'],date("Y-m-d H:i:s"),$tabledata,$data);
		}
		checkin_before_close();
	}
	else if($cmd=="database_structure")
	{
		$db_info = get_database_info($rdb);
		$db_tables = $db_info['tables'];
		$db_fields = $db_info['fields'];

		$str = "";
		for($i=0; $i<count($db_tables); $i++)
		{
			$tablename = $db_tables[$i];
			$fieldlist = $db_fields[$tablename][0];
			$fields = $db_fields[$tablename][1];

			$str .= "<db_table>";
			$str .= "<db_tablename>" . $tablename . "</db_tablename>";
			for($n=0; $n<count($fieldlist); $n++)
			{
				$fieldname = $fieldlist[$n];
				$fieldtype = $fields[$fieldname]['type'];
				$fieldnull = $fields[$fieldname]['null'];
				$fieldkey = $fields[$fieldname]['key'];
				$fielddefault = $fields[$fieldname]['default'];
				$fieldextra = $fields[$fieldname]['extra'];

				$str .= "<db_field>";
				$str .= "<db_fieldname>".$fieldname."</db_fieldname>";
				$str .= "<db_fieldtype>".$fieldtype."</db_fieldtype>";
				$str .= "<db_fieldnull>".$fieldnull."</db_fieldnull>";
				$str .= "<db_fieldkey>".$fieldkey."</db_fieldkey>";
				$str .= "<db_fielddefault>".$fielddefault."</db_fielddefault>";
				$str .= "<db_fieldextra>".$fieldextra."</db_fieldextra>";
				$str .= "</db_field>";
			}
			$str .= "</db_table>";
		}
		echo $str;
		checkin_before_close();
	}
	else if($cmd=="sync_finished")
	{
		$ls_interval = postvar("local_sync_interval","");
		if($ls_interval!="") $ls_interval = "/" . $ls_interval;

		lavu_query("delete from `config` where `setting`='last sync finished'");
		schedule_remote_to_local_sync($locationid,"last sync finished",time() . $ls_interval,date("Y-m-d H:i:s") . "\n" . $_SERVER['REMOTE_ADDR']);
		checkin_before_close();
	}
	else if($cmd=="sync_updated")
	{

		$ls_interval = postvar("local_sync_interval","");
		if($ls_interval!="") $ls_interval = "/" . $ls_interval;

		lavu_query("delete from `config` where `setting`='last sync request'");
		schedule_remote_to_local_sync($locationid,"last sync request",time() . $ls_interval,date("Y-m-d H:i:s") . "\n" . $_SERVER['REMOTE_ADDR']);
		//$utables = array("users","lk_tracks","lk_event_types","lk_karts");
		if ($_POST['dataname'] == '84_salval') error_log("84_salval is looking for commands.");

		$local_query = lavu_query("select SQL_NO_CACHE `id`, `setting`, `value`, `value_long`, `value2`, `value5` from `config` where `location`='[1]' and (LEFT(`setting`,9)='sync_cmd:' or `setting` IN ('records_check', 'run local query', 'health check', 'create_log_dir', 'create_directory', 'sync database', 'open tunnel', 'copy remote file', 'remove local file', 'upload signature', 'upload waiver', 'read file structure', 'retrieve local file', 'exec_command', 'update_or_insert_query') or (`setting`='retrieved_local_file' and `value5`='write')) order by (`setting`='sync database' or `setting`='copy remote file' or `setting`='open tunnel' or `setting`='read file structure') desc, (`setting`='health check' or `setting`='retrieve local file') desc, id asc limit 1",$locationid);
		if(mysqli_num_rows($local_query))
		{
			$local_read = mysqli_fetch_assoc($local_query);

			if($local_read['setting']=="sync database")
			{
				lavu_query("delete from `config` where `id`='[1]'",$local_read['id']);
				echo "sync database||";
			}
			else if(substr($local_read['setting'],0,9)=="sync_cmd:")
			{
				$sync_cmd = substr($local_read['setting'],9);
				lavu_query("delete from `config` where `id`='[1]'",$local_read['id']);
				echo $sync_cmd."|".$local_read['value']."|";
			}
			else if($local_read['setting']=="open tunnel")
			{
				lavu_query("delete from `config` where `id`='[1]'",$local_read['id']);
				echo "open tunnel||";
			}
			else if($local_read['setting']=="health check")
			{
				lavu_query("delete from `config` where `id`='[1]'",$local_read['id']);
				echo "health check||";
			}
			else if($local_read['setting']=="create_log_dir")
			{
				lavu_query("delete from `config` where `id`='[1]'",$local_read['id']);
				echo "create_log_dir||";
			}
			else if($local_read['setting']=="create_directory")
			{
				lavu_query("delete from `config` where `id`='[1]'",$local_read['id']);
				echo "create_directory|".$local_read['value']."|";
			}
			else if($local_read['setting']=="records_check")
			{
				lavu_query("delete from `config` where `id`='[1]'",$local_read['id']);
				echo "records_check|".$local_read['value']."|".$local_read['value2']."|".$local_read['value_long'];
			}
			else if($local_read['setting']=="copy remote file")
			{
				lavu_query("delete from `config` where `id`='[1]'",$local_read['id']);
				echo "copy remote file|".$local_read['value']."|".$local_read['value_long'];
			}
			else if($local_read['setting']=="remove local file")
			{
				lavu_query("delete from `config` where `id`='[1]'",$local_read['id']);
				echo "remove local file|".$local_read['value']."|".$local_read['value_long'];
			}
			else if($local_read['setting']=="read file structure")
			{
				lavu_query("delete from `config` where `id`='[1]'",$local_read['id']);
				echo "read file structure|".$local_read['value']."|".$local_read['value_long'];
			}
			else if($local_read['setting']=="retrieve local file")
			{
				lavu_query("delete from `config` where `id`='[1]'",$local_read['id']);
				echo "retrieve local file|".$local_read['value']."|".$local_read['value_long'];
			}
			else if($local_read['setting']=="upload signature")
			{
				lavu_query("delete from `config` where `id`='[1]'",$local_read['id']);
				echo "upload signature|".$local_read['value']."|".$local_read['value_long'];
			}
			else if($local_read['setting']=="upload waiver")
			{
				lavu_query("delete from `config` where `id`='[1]'",$local_read['id']);
				echo "upload waiver|".$local_read['value']."|".$local_read['value_long'];
			}
			else if($local_read['setting']=="retrieved_local_file" && $local_read['value5']=="write")
			{
				lavu_query("update `config` set `value5`='written' where `id`='[1]'",$local_read['id']);
				echo "write to local file|".$local_read['value']."|".$local_read['value_long'];
			}
			else if($local_read['setting']=="run local query")
			{
				lavu_query("delete from `config` where `id`='[1]'",$local_read['id']);
				echo "run local query|".$local_read['value']."|".$local_read['value_long'];
			}
			else if($local_read['setting']=="update_or_insert_query"){//Inserted by Brian D. originally to update cash_data settings, which isn't in config.
				lavu_query("delete from `config` where `id`='[1]'",$local_read['id']);
				echo "update_or_insert_query|".$local_read['value_long'];
			}
			else if($local_read['setting']=="exec_command")//Inserted by Brian D. run bash on client machine.
			{
    			lavu_query("delete from `config` where `id`='[1]'",$local_read['id']);
    			echo "exec_command|".$local_read['value_long'];
			}
		}
		else
		{
			$void_query = lavu_query("select SQL_NO_CACHE `id`, `value`, `value_long` from `config` where `location`='[1]' and `setting`='void orders' order by id asc limit 1",$locationid);
			if(mysqli_num_rows($void_query))
			{
				$void_read = mysqli_fetch_assoc($void_query);

				lavu_query("delete from `config` where `id`='[1]'",$void_read['id']);
				echo "void orders|".$void_read['value']."|".$void_read['value_long'];
			}
			else
			{
				$up_query = lavu_query("select SQL_NO_CACHE `id`, `value` from `config` where `location`='[1]' and `setting`='table updated' order by id asc limit 1",$locationid);
				if(mysqli_num_rows($up_query))
				{
					$up_read = mysqli_fetch_assoc($up_query);

					lavu_query("delete from `config` where `id`='[1]' or (`location`='[2]' and `setting`='table updated' and `value`='[3]')",$up_read['id'],$locationid,$up_read['value']);
					echo "entire_table|".$up_read['value'];
				}
				else
				{
					$rup_query = lavu_query("select SQL_NO_CACHE `id`, `value`, `value_long` from `config` where `location`='[1]' and `setting`='rows updated' order by id asc limit 1",$locationid);
					if(mysqli_num_rows($rup_query))
					{
						$rup_read = mysqli_fetch_assoc($rup_query);

						lavu_query("delete from `config` where `id`='[1]'",$rup_read['id']);
						echo "rows|".$rup_read['value']."|".$rup_read['value_long'];
					}
					else echo "0|0";//|".$locationid."|".$dataname;
				}
			}
		}
		checkin_before_close();
	}

	else if($cmd=="get_order_id_month_ago") // get order_id as of 30 days ago, 1 year for p2r's
	{
		if($dataname=="p2r_dallas")//p2r may have open orders based on deposites for a race.
		{
	 		$x_months_ago = date("Y-m-d",mktime(0,0,0,date("m")-12,date("d"),date("Y")));
	 		$order_query = lavu_query("SELECT `order_id`, `opened` FROM `orders` WHERE STR_TO_DATE(`opened`,'%Y-%m-%d')>='[1]' ORDER BY `opened` ASC LIMIT 1",$x_months_ago);
		}
		else
		{
	 		$month_ago = date("Y-m-d",mktime(0,0,0,date("m")-1,date("d"),date("Y")));
	 		$order_query = lavu_query("SELECT `order_id`, `opened` FROM `orders` WHERE STR_TO_DATE(`opened`,'%Y-%m-%d')>='[1]' ORDER BY `opened` ASC LIMIT 1",$month_ago);
	 	}
	 	// TODO return last order id from a month ago..
	 	// on localserver setup sync only go back a month ago etc etc
	 	$num_rows = mysqli_num_rows($order_query);
	 	if($num_rows) {

			$order_id_assoc = mysqli_fetch_assoc($order_query);

			// return order_id of last order within a month
			echo $order_id_assoc['order_id'].PHP_EOL;
		}

		checkin_before_close();
	}
	else if($cmd=="sync_down")
	{
		$mode = postvar("mode","");
		$col_names = "";
		$col_values = "";
		$cols = array();

		$extra_info = "";

		if($mode=="entire_table"){
			//Fix to bug #3844, users doesn't sync up when clocking in, clock punches do.  We need to update the users table and set the 
			//clocked in flag based on the clock punches prior to syncing down the table.
			if($cmd_table == 'users'){ 
				$queryStr = "UPDATE `users` u JOIN `clock_punches` c ON `c`.`server_id`=`u`.`id` SET `u`.`clocked_in`='1' WHERE `u`.`clocked_in`='0' AND `c`.`time_out`=''";
				$result = lavu_query($queryStr);
				if(!$result){
					error_log("Failed updating users table for clock_punches in:".__FILE__);
				}
			}
			$row_query = lavu_query("select * from `[1]`",$cmd_table);
		}
		else if($mode=="partial")
		{
			$sync_step = postvar("step",100);
			$sync_lastid = postvar("lastid",0);
			$sync_min_order_id = postvar("min_order_id", "");


			$orderIDsResult = false;
			if($dataname == 'p2r_dallas'){
				$orderIDsResult = lavu_query("SELECT `order_id` FROM `orders` WHERE `opened`>'".date('Y-m-d', strtotime("-12 month", time()))."'");
			}else{
				$orderIDsResult = lavu_query("SELECT `order_id` FROM `orders` WHERE `closed`>'".date('Y-m-d', strtotime("-1 month", time()))."'");
			}
			$orderIDsArr = array();
			while($currRow = mysqli_fetch_assoc($orderIDsResult)){
				$orderIDsArr[] = $currRow['order_id'];
			}
			$inStatement = " AND `order_id` IN ('" . implode("','", $orderIDsArr) . "')";



			// only get orders from a month ago -- tables that have an order_id field only!
			if ( $sync_min_order_id != "" ) {

				// strip prefix from order_id
				$min_order_id = explode("-",$sync_min_order_id);
				$compare_min_order_id = $min_order_id[1];

				//$min_order_id_condition = " AND SUBSTR(`order_id`, LOCATE('-',`order_id`)+1)*1 >= '[4]'";
				//// EDITTED OUT BY BRIAN D, ASSUMPTION THAT `CLOSED` COLUMNS EXIST.$min_order_id_condition = " AND `closed`>'".date('Y-m-d', strtotime("-1 month", time()) )."' ";////
				$min_order_id_condition = $inStatement;
			}
			else {
				$compare_min_order_id = "";
				$min_order_id_condition = "";
			}


			$after_query = lavu_query("select count(*) as `record_count` from `[1]` where `id`>'[2]'$min_order_id_condition",$cmd_table,$sync_lastid,'',$compare_min_order_id);
			if(mysqli_num_rows($after_query))
			{
				$after_read = mysqli_fetch_assoc($after_query);
				$sync_records_left = $after_read['record_count'];
				$extra_info = "records_left=" . $sync_records_left;
			}
			$row_query = lavu_query("select * from `[1]` where `id`>[2]$min_order_id_condition order by id asc limit 0,[3]",$cmd_table,$sync_lastid,$sync_step,$compare_min_order_id);
		}
		else if($mode=="condition")
		{
			$condition = postvar("condition","");
			$row_query = lavu_query("select * from `[1]` where ".$condition,$cmd_table);
		}
		else if($mode=="rows")
		{
			$vars = array();
			$vars['tablename'] = $cmd_table;
			$where_code = "";
			$rows = postvar("rows");
			$rows_parts = explode("|",$rows);
			for($r=0; $r<count($rows_parts); $r++)
			{
				if($where_code!="") $where_code .= " or ";
				$where_code .= "`id`='[row_".$r."]'";
				$vars['row_'.$r] = trim($rows_parts[$r]);
			}
			$row_query = lavu_query("select * from `[tablename]` where ".$where_code,$vars);
		}
		else
			$row_query = false;

		$lastid_found = 0;
		if($row_query)
		{
			while($row_read = mysqli_fetch_assoc($row_query))
			{
				if(isset($row_read['id']))
					$lastid_found = $row_read['id'];
				if($col_names=="")
				{
					foreach($row_read as $key => $val)
					{
						$cols[] = $key;
					}
					for($n=0; $n<count($cols); $n++)
					{
						$col_names .= $cols[$n];
						if($n < count($cols) - 1) $col_names .= "[_sep_]";
					}
				}
				$col_values .= "[_row_]";
				for($n=0; $n<count($cols); $n++)
				{
					$col_values .= $row_read[$cols[$n]];
					if($n < count($cols)-1) $col_values .= "[_sep_]";
				}
			}
		}

		if($mode=="partial")
		{
			if($extra_info!="") $extra_info .= "&";
			$extra_info .= "lastid=" . $lastid_found;
		}

		if($extra_info!="")
			echo $extra_info . "[_response_info_]";
		echo $col_names;
		echo $col_values;

		checkin_before_close();
	}
	else if($cmd=="sync_records_up")
	{
		$data = postvar("data","");
		$tablename = postvar("tablename","");
		$use_ialid = (postvar("use_ialid", "0")=="1" && $tablename=="action_log");

		$cols = explode("[_col_]",get_special_tag_contents("cols",$data));
		$record_list = get_special_tag_contents("rows",$data);
		$record_list = explode("[_row_]",$record_list);

		$success_list = "";
		$output_list = "";
		for($i=0; $i<count($record_list); $i++)
		{
			$row_id = get_special_tag_contents("row_id",$record_list[$i]);
			$ialid = get_special_tag_contents("ialid",$record_list[$i]);
			
			$use_id = $row_id;
			$id_field = "id";
			if ($use_ialid && strlen($ialid)>0) {
				$use_id = $ialid;
				$id_field = "ialid";	
			}
			
			$row_contents = explode("[_col_]",get_special_tag_contents("row_contents",$record_list[$i]));

			lavu_query("delete from `$tablename` where `".$id_field."`='[1]'",$use_id);
			$query_str = create_insert_query_str($tablename,$cols,$row_contents, !$use_ialid);
			$success = lavu_query($query_str);
			if($success) $output_list .= "(success for row $row_id)"; else $output_list .= "(failed for row $row_id):".$query_str."<br>";

			if($success_list!="")
				$success_list .= ",";
			$success_list .= $row_id;
		}

		if ($location_api_debug) {
			error_log("LSAPI ".$dataname." - cmd: ".$cmd." - Tablename: ".$tablename." - Record Count: ".count($record_list));
			error_log("LSAPI ".$dataname." - cmd: ".$cmd." - Success List: ".$success_list);
			error_log("LSAPI ".$dataname." - cmd: ".$cmd." - Output List: ".$output_list);
		}

		echo "1|".$success_list."|".$output_list;
		checkin_before_close();
	}
	else if ($cmd=="sync_orders_up")
	{
		$start_ts = time();

		$json_data = postvar("json_data", false);	
		if ($json_data) {
		
			error_log("json_data being used by ".$dataname);
		
			$decoded_json = json_decode($json_data, true);
		
			$order_cols				= $decoded_json['order_cols'];
			$contents_cols			= $decoded_json['contents_cols'];
			$modifiers_used_cols	= $decoded_json['modifiers_cols'];
			$transactions_cols		= $decoded_json['transactions_cols'];
			$split_check_cols		= $decoded_json['split_check_cols'];
			$alternate_payment_cols	= $decoded_json['alt_payment_cols'];
			$orders_list			= $decoded_json['orders'];
			
		} else {

			$data = postvar("data","");
			if (substr($data, 0, 6) == "BASE64") {
				$data_parts = explode("|_|", $data);
				$data = base64_decode($data_parts[1]);
			}

			$order_cols = explode("[_col_]",get_special_tag_contents("order_cols",$data));
			$contents_cols = explode("[_col_]",get_special_tag_contents("order_contents_cols",$data));
			$modifiers_used_cols = explode("[_col_]",get_special_tag_contents("modifiers_used_cols",$data));
			$transactions_cols = explode("[_col_]",get_special_tag_contents("order_transactions_cols",$data));
			$split_check_cols = explode("[_col_]",get_special_tag_contents("order_split_checks_cols",$data));
			$alternate_payment_cols = explode("[_col_]",get_special_tag_contents("order_alternate_payments_cols",$data));
			$orders_list = get_special_tag_contents("orders",$data);
			$orders_list = explode("[_order_row_]",$orders_list);
		}

		$logstr = "";
		$sync_down_transactions = "";
		$success_list = "";
		$output_list = "";

		for ($i=0; $i<count($orders_list); $i++) {

			if (is_array($orders_list[$i]) || trim($orders_list[$i])!="") {

				$order_contents_txt = "";
				$modifiers_used_txt = "";
				$order_transactions_txt = "";
				$order_split_checks_txt = "";
				$order_alternate_payments_txt = "";

				if ($json_data) {
					
					$this_order = $orders_list[$i];
					
					$order_id					= $this_order['order_id'];
					$ioid						= $this_order['ioid'];
					$order_properties			= $this_order['properties'];
					$order_contents			= $this_order['contents'];
					$modifiers_used			= $this_order['modifiers'];
					$order_transactions		= $this_order['transactions'];
					$order_split_checks		= $this_order['split_checks'];
					$order_alternate_payments	= $this_order['alt_payments'];
					
				} else {

					$order_id = get_special_tag_contents("order_id",$orders_list[$i]);
					$ioid = get_special_tag_contents("ioid",$orders_list[$i]);
					$order_properties = explode("[_col_]",get_special_tag_contents("order_properties",$orders_list[$i]));
	
					$order_contents_txt = get_special_tag_contents("order_contents",$orders_list[$i]);
					$order_contents = explode("[_row_]",$order_contents_txt);
	
					$modifiers_used_txt = get_special_tag_contents("modifiers_used",$orders_list[$i]);
					$modifiers_used = explode("[_row_]",$modifiers_used_txt);
	
					$order_transactions_txt = get_special_tag_contents("order_transactions",$orders_list[$i]);
					$order_transactions = explode("[_row_]",$order_transactions_txt);
	
					$order_split_checks_txt = get_special_tag_contents("order_split_checks",$orders_list[$i]);
					$order_split_checks = explode("[_row_]",$order_split_checks_txt);
	
					$order_alternate_payments_txt = get_special_tag_contents("order_alternate_payments",$orders_list[$i]);
					$order_alternate_payments = explode("[_row_]",$order_alternate_payments_txt);
				}

				if (trim($order_id)!="") {

					lavu_query("delete from `orders` where `order_id` = '[1]' and `location_id` = '[2]'", $order_id, $locationid);

					$order_query_str = create_insert_query_str("orders", $order_cols, $order_properties, false);
					$logstr .= $order_query_str."\n";

					$success = lavu_query($order_query_str);
					if ($success) {
						$output_list .= "(success for order ".$order_id.")";
					} else {
						$output_list .= "(failed for order ".$order_id.")<br>".$order_query_str."<br>";
					}

					// order contents

					lavu_query("delete from `order_contents` where `order_id`='[1]' and `loc_id`='[2]'", $order_id, $locationid);

					$success_contents = true;
					if (trim($order_contents_txt)!="" || $json_data) {

						for ($n=0; $n<count($order_contents); $n++) {

							$order_content_row = $order_contents[$n];
							if (!is_array($order_content_row)) {
								$order_content_row = explode("[_col_]", $order_contents[$n]);
							}

							// Brian D. hotfix LP-8349
							$contents_columns_4_insert = $contents_cols;
							$indexOfOrderTagID = array_search('ordertag_id', $contents_columns_4_insert);
							if($indexOfOrderTagID !== false){
							  if( empty($order_content_row[$indexOfOrderTagID]) ){
							    unset($contents_columns_4_insert[$indexOfOrderTagID]);
							    unset($order_content_row[$indexOfOrderTagID]);
							  }
							}
							//End hotfix LP-8349

							$contents_query_str = create_insert_query_str("order_contents", $contents_columns_4_insert, $order_content_row, false);
							$query_success = lavu_query($contents_query_str);
							if (!$query_success) {

								$success_contents = false;
								$output_list .= "(failed for order_contents on ".$order_id." [".lavu_dberror()."])<br>".$contents_query_str."<br>";
								break;
							}
						}
					}

					// modifiers used

					lavu_query("delete from `modifiers_used` where `order_id`='[1]' and `loc_id`='[2]'", $order_id, $locationid);

					$success_modifiers_used = true;
					if (trim($modifiers_used_txt)!="" || $json_data) {

						for ($n=0; $n<count($modifiers_used); $n++) {

							$modifiers_row = $modifiers_used[$n];
							if (!is_array($modifiers_row)) {
								$modifiers_row = explode("[_col_]", $modifiers_used[$n]);
							}

							// Brian D. hotfix LP-8349
							$modifiers_used_cols_4_insert = $modifiers_used_cols;
							$indexOfOrderTagID = array_search('ordertag_id', $modifiers_used_cols_4_insert);
							if($indexOfOrderTagID !== false){
							  if( empty($modifiers_row[$indexOfOrderTagID]) ){
							    unset($modifiers_used_cols_4_insert[$indexOfOrderTagID]);
							    unset($modifiers_row[$indexOfOrderTagID]);
							  }
							}
							//End hotfix LP-8349
							
							$modifiers_used_query_str = create_insert_query_str("modifiers_used", $modifiers_used_cols_4_insert, $modifiers_row, false);
							$query_success = lavu_query($modifiers_used_query_str);
							if (!$query_success) {

								$success_modifiers_used = false;
								$output_list .= "(failed for modifiers_used on ".$order_id." [".lavu_dberror()."])<br>".$modifiers_used_query_str."<br>";
								break;
							}
						}
					}

					// transactions

					$synced_up_internal_ids = array("");

					$success_transactions = true;
					if (trim($order_transactions_txt)!="" || special_tag_exists("order_transactions", $orders_list[$i]) || $json_data) {

						if ($order_id=="Paid In" || $order_id=="Paid Out") {
							$trans_min_datetime = date("Y-m-d H:i:s", mktime(0, 0, 0, date("m"), date("d")-2, date("Y")));
						} else {
							$trans_min_datetime = "0000-00-00 00:00:00";
						}

						lavu_query("delete from `cc_transactions` where `order_id`='[1]' and `loc_id`='[2]' AND `internal_id` = '' and `datetime`>='[3]'", $order_id, $locationid, $trans_min_datetime);

						if (trim($order_transactions_txt)!="" || $json_data) {
							
							for ($n=0; $n<count($order_transactions); $n++) {

								$trans_datetime = "0000-00-00 00:00:00";
								$trans_internal_id = "";
								$trans_last_mod_ts = 0;
								$trans_voided = "0";
								
								$otparts = $order_transactions[$n];
								if (!is_array($otparts)) {
									$otparts = explode("[_col_]", $order_transactions[$n]);
								}

								for ($m=0; $m<count($transactions_cols); $m++) {

									switch ($transactions_cols[$m]) {

										case "datetime":
											$trans_datetime = $otparts[$m];
											break;

										case "internal_id":
											$trans_internal_id = $otparts[$m];
											break;

										case "last_mod_ts":
											$trans_last_mod_ts = (int)$otparts[$m];
											break;
											
										case "voided":
											$trans_voided = $otparts[$m];
											break;

										default:
											break;										
									}									
								}

								if (!empty($trans_internal_id)) {
									$trans_min_datetime = "0000-00-00 00:00:00";
									if (!in_array($trans_internal_id, $synced_up_internal_ids)) {
										$synced_up_internal_ids[] = $trans_internal_id;
									}
								}
								
								if ($trans_datetime >= $trans_min_datetime) {

									/*$tdfname = "/home/poslavu/private_html/sdocs/apache_misc/lsapi_transactions.txt";
									$fp = fopen($tdfname,"a");
									fwrite($fp,date("Y-m-d h:i:s") . " ($i - $n) " . postvar("dataname") . " " . $order_id . " " . $trans_datetime . " \n");
									fclose($fp);*/

									$transactions_query_str = "";
									$should_insert = true;
									if (!empty($trans_internal_id)) { // check for existing transaction record
										
										$check_previous = lavu_query("SELECT `id`, `voided`, `last_mod_ts` FROM `cc_transactions` WHERE `internal_id` = '[1]' ORDER BY `id` DESC", $trans_internal_id);
										if ($check_previous) {
											$matches = mysqli_num_rows($check_previous);
											if ($matches > 0) {
												
												$should_insert = false;
												
												$existing_tx = mysqli_fetch_assoc($check_previous);
												$existing_id = $existing_tx['id'];
												$existing_last_mod_ts = (int)$existing_tx['last_mod_ts'];

												$update_existing = false;
												if ($trans_voided=="1" && $existing_tx['voided']=="0") {
													$update_existing = true;
												}
												if ($trans_last_mod_ts>$existing_last_mod_ts || ($trans_last_mod_ts==0 && $existing_last_mod_ts==0)) {
													$update_existing = true;
												}
												
												if ($update_existing) {
													$transactions_query_str = create_update_query_str("cc_transactions", $existing_id, $transactions_cols, $otparts);
												}
												
												$update_lls_record = false;
												if ($trans_voided=="0" && $existing_tx['voided']=="1") {
													$update_lls_record = true;
												}
												if ($trans_last_mod_ts < $existing_last_mod_ts) {
													$update_lls_record = true;
												}
											
												if ($matches > 1) {
													$remove_duplicates = lavu_query("DELETE FROM `cc_transactions` WHERE `internal_id` = '[1]' AND `id` != '[2]'", $trans_internal_id, $existing_id);	
												}

												if ($update_lls_record) { // queue transaction record to be sent to LLS in sync response
													$get_full_transaction = lavu_query("SELECT * FROM `cc_transactions` WHERE `internal_id` = '[1]'", $trans_internal_id);
													if ($get_full_transaction!==FALSE && mysqli_num_rows($get_full_transaction)>0) {
														$full_transaction = mysqli_fetch_assoc($get_full_transaction);
														if (!empty($sync_down_transactions)) {
															$sync_down_transactions .= "[_row_]";
														}
														$sync_down_transactions .= get_val_list_from_arrays($transactions_cols, $full_transaction, "id");
													}
												}
											}
										}
									}

									if ($should_insert) {
										$transactions_query_str = create_insert_query_str("cc_transactions", $transactions_cols, $otparts, false);									
									}
									
									if (!empty($transactions_query_str)) {	
										$query_success = lavu_query($transactions_query_str);
										if (!$query_success) {
											$output_list .= "(failed for order_transaction on ".$order_id." [".lavu_dberror()."])<br>".$transactions_query_str."<br>".str_replace("<", "&lt;", get_special_tag_contents("order_transactions", $orders_list[$i]))."<br><br>";
											$success_transactions = false;
											break;
										}
									}
								}
							}
						}
					}
					
					if (!empty($ioid)) {
					
						$check_for_more_transactions = lavu_query("SELECT * FROM `cc_transactions` WHERE `ioid` = '[1]' AND `internal_id` NOT IN ('".implode("','", $synced_up_internal_ids)."')", $ioid);
						if ($check_for_more_transactions) {
							if (mysqli_num_rows($check_for_more_transactions) > 0) {
								while ($tx_info = mysqli_fetch_assoc($check_for_more_transactions)) {
									if (!empty($sync_down_transactions)) {
										$sync_down_transactions .= "[_row_]";
									}
									$sync_down_transactions .= get_val_list_from_arrays($transactions_cols, $tx_info, "id");
								}
							}
						}
					}

					// split checks

					lavu_query("delete from `split_check_details` where `order_id` = '[1]' and `loc_id` = '[2]'", $order_id, $locationid);

					$success_split_checks = true;
					if (trim($order_split_checks_txt)!="" || $json_data) {

						for ($n=0; $n<count($order_split_checks); $n++) {

							$split_check_row = $order_split_checks[$n];
							if (!is_array($split_check_row)) {
								$split_check_row = explode("[_col_]", $order_split_checks[$n]);
							}

							$splitchk_query_str = create_insert_query_str("split_check_details", $split_check_cols, $split_check_row, false);

							$query_success = lavu_query($splitchk_query_str);
							if (!$query_success) {

								//$output_list .= "(failed for split checks $order_id)<br>";
								$output_list .= "(failed for split_checks on ".$order_id." [".lavu_dberror()."])<br>".$splitchk_query_str."<br>".str_replace("<", "&lt;", get_special_tag_contents("order_split_checks", $orders_list[$i]))."<br><br>";
								$success_split_checks = false;
								break;
							}
						}
					}

					// alternate_payment_totals

					lavu_query("delete from `alternate_payment_totals` where `order_id`='[1]' and `loc_id`='[2]'", $order_id, $locationid);

					$success_alternate_payments = true;
					if (trim($order_alternate_payments_txt)!="" || $json_data) {
	
						for ($n=0; $n<count($order_alternate_payments); $n++) {

							$alt_payment_row = $order_alternate_payments[$n];
							if (!is_array($alt_payment_row)) {
								$alt_payment_row = explode("[_col_]", $order_alternate_payments[$n]);
							}

							$alt_pay_query_str = create_insert_query_str("alternate_payment_totals", $alternate_payment_cols, $alt_payment_row, false);
							$query_success = lavu_query($alt_pay_query_str);
							if (!$query_success) {

								$output_list .= "(failed for alternate_payment_totals on ".$order_id." [".lavu_dberror()."])<br>".$alt_pay_query_str."<br>".str_replace("<", "&lt;", get_special_tag_contents("order_alternate_payments", $orders_list[$i]))."<br><br>";
								$success_alternate_payments = false;
								break;
							}
						}
					}

					if (!$success_alternate_payments) {
						$output_list .= "(failed for alternate_payments on ".$order_id.")<br>";
					}

					if ($success && $success_contents && $success_modifiers_used && $success_transactions && $success_split_checks && $success_alternate_payments) {

						if ($success_list!="") {
							$success_list .= ",";
						}
						$success_list .= $order_id;

					} else {

						$output_list .= "(unable to sync ".$order_id." - _ ".$success." _ ".$success_contents." _ ".$success_modifiers_used." _ ".$success_transactions." _ ".$success_split_checks." _ ".$success_alternate_payments.")<br>";
					}
					//$contents_query_str = create_insert_query_str($contents_cols,$order_contents);
					//$transactions_query_str = create_insert_query_str($transactions_cols,$order_transactions);

					//echo "<br><br>" . $contents_query_str;
					//echo "<br><br>" . $transactions_query_str;
					//echo $order_id;
				}
			}
		}
		
		$output_list .= "|";
		if (isset($_POST['known_api_orders'])) {
			$output_list .= appendNewApiOrders($locationid, $_POST['known_api_orders']);
		}

		$output_list .= "|";
		if (!empty($sync_down_transactions)) {
			$cols_str = "[_sync_down_txs_cols_start_]".output_array_cols($transactions_cols)."[_sync_down_txs_cols_end_]";
			$output_list .= "[_sync_down_txs_start_]".$cols_str."[_sync_down_txs_vals_start_]".$sync_down_transactions."[_sync_down_txs_vals_end_][_sync_down_txs_end_]";
		}	

		$short_vals = array(time(),$_SERVER['REMOTE_ADDR'],"",date("Y-m-d H:i:s"));
		update_sync_config_setting($locationid,"orders synced up",$short_vals,$success_list."|".$output_list,$logstr);

		if(trim($success_list)!="")
		{
			mlavu_query("update `poslavu_MAIN_db`.`restaurants` set `last_activity`='[1]' where `data_name`='[2]'",date("Y-m-d H:i:s"),$dataname);
		}
		
		echo "1|".$success_list."|".$output_list;
		checkin_before_close();
	}
	else if($cmd=="sync")
	{
		$rowid = postvar("rowid","");
		$col_names = postvar("colnames","");
		$col_vals = postvar("colvals","");

		$col_names = explode("[_sep_]",$col_names);
		$col_vals = explode("[_sep_]",$col_vals);

		$col_list = "";
		$val_list = "";

		if($rowid!="")
		{
			$exist_query = lavu_query("select * from `[1]` where `id`='[2]'",$cmd_table,$rowid);
			if(mysqli_num_rows($exist_query))
			{
				//already exists
				echo "1";
			}
			else
			{
				$vars = array();
				$vars['tablename'] = $cmd_table;
				for($n=0; $n<count($col_names); $n++)
				{
					if($col_list!="") $col_list .= ",";
					if($val_list!="") $val_list .= ",";

					$col_list .= "`[colname_".$n."]`";
					$val_list .= "'[colval_".$n."]'";

					$vars["colname_".$n] = $col_names[$n];
					$vars["colval_".$n] = $col_vals[$n];
				}

				$success = lavu_query("insert into `[tablename]` ($col_list) values ($val_list)",$vars);

				if($success) echo "1";
				else
				{

					$output = "insert into `[tablename]` ($col_list) values ($val_list)";
					foreach($vars as $key => $val)
					{
						$output = str_replace("[".$key."]",$val,$output);
					}
					//echo $output;
					//echo "0|hello";
					echo "0";
				}
			}
		}
		checkin_before_close();
	}

    function getLLSAuthorizedValue($dataName_){
        //We pull their information from authorizations table.
    	$authResult = mlavu_query("SELECT * FROM `poslavu_MAIN_db`.`authorizations` WHERE `data_name`='[1]'", $dataName_);
        if(!$authResult){
            error_log('Semantic sequel error in lls_authorizations.php: ' . mlavu_dberror());
            return false;
        }
        if(mysqli_num_rows($authResult)){
            $authRow = mysqli_fetch_assoc($authResult);
            return $authRow['lls_authorized'];
        }
        //Row doesn't exist
        return false;
    }



    //class LsapiFunctions {

	    /**
	     * checks if the package level should be updated on the local server
	     * @param  string  $dataname   dataname of the restaurant
	     * @param  int     $locationid location id of the restaurant
	     * @return boolean             TRUE if the package is updated, FALSE otherwise
	     */
	    /*public static function check_update_package($dataname, $locationid) {

	    	// find the previous update and the restaurant
			$query = lavu_query("SELECT `id` FROM `config` WHERE `location`='[location]' AND `type`='log' AND `setting`='lls update package' AND `value` > '[date]'",array('location'=>$locationid,'date'=>date("Y-m-d H:i:s",strtotime("-10 minutes"))));
			$rest_query_string = "SELECT `id` FROM `poslavu_MAIN_db`.`restaurants` WHERE `data_name`='[dataname]' LIMIT 1";
			$rest_query_vars = array('dataname'=>$dataname);
			$rest_query = mlavu_query($rest_query_string, $rest_query_vars);// poslavu_MAIN_db

			// check if the last update had more than 10 minutes ago
			if ($query === FALSE || mysqli_num_rows($query) > 0 || $rest_query === FALSE || mysqli_num_rows($rest_query) == 0)
				return FALSE;

			// get some necessary functions
			require_once(dirname(__FILE__)."/../../manage/misc/LLSFunctions/schedule_msync.php");
			require_once(dirname(__FILE__)."/../../sa_cp/billing/payment_profile_functions.php");

			// get the restaurant and package
			$rest_read = mysqli_fetch_assoc($rest_query);
			$a_package = find_package_status($rest_read['id']);

			// try to update the package
			if (!LLSFunctionsScheduleMsync::setPackage($dataname, (int)$a_package['package']))
				return FALSE;

			lavu_query("DELETE FROM `config` WHERE `location`='[location]' AND `type`='log' AND `setting`='lls update package'",array('location'=>$locationid));
			lavu_query("INSERT INTO `config` (`location`,`type`,`setting`,`value`) VALUES ('[location]','[type]','[setting]','[value]')", array('location'=>$locationid, 'type'=>'log', 'setting'=>'lls update package', 'value'=>date("Y-m-d H:i:s")));

			return TRUE;
		}
	}*/
?>
