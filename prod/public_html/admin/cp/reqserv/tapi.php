<?php
	// Special Triggers Api
	
	function run_api_cmd_trigger($cmd_trigger)
	{
		$rsp = "";
		$dbg = "";
		
		if($cmd_trigger=="verify_content_items")
		{
			require_once("/home/poslavu/public_html/admin/dev/lib/jcvs/jc_addons/jc_addon_functions/hotel_comm_functions.php");
			$dbg .= "\n--------------- $cmd_trigger -------------------\n";
			$content_query = lavu_query("select * from `order_contents` where `item_id`='0' and `item`!='SENDPOINT'");
			while($content_read = mysqli_fetch_assoc($content_query))
			{
				$hd4_info = urldecode_string(urldecode($content_read['hidden_data4']));
				$item_name = $content_read['item'];
				$item_price = $content_read['price'];
				$item_category = (isset($hd4_info['category']))?$hd4_info['category']:"";
				$item_query = lavu_query("select * from `menu_items` where `name` LIKE '[1]'",$item_name);
				if(mysqli_num_rows($item_query)) // If Item Already Exists connect the item_id
				{
					$item_read = mysqli_fetch_assoc($item_query);
					lavu_query("update `order_contents` set `item_id`='[1]' where `id`='[2]'",$item_read['id'],$content_read['id']);
					$dbg .= "Item found: $item_name\n";
				}
				else 
				{
					if(trim($item_category) != "") // Only Add if a category is defined
					{
						$menu_id = 0;
						$menu_group_id = 0;
						$category_id = 0;
						$category_name = "";
						$category_query = lavu_query("select * from `menu_categories` where `name` LIKE '[1]'",$item_category);
						if(mysqli_num_rows($category_query)) // Find a matching category
						{
							$dbg .= "Found Category: $item_category\n";
							$category_read = mysqli_fetch_assoc($category_query);
							$category_name = $category_read['name'];
							$category_id = $category_read['id'];
							$menu_id = $category_read['menu_id'];
							$menu_group_id = $category_read['group_id'];
						}
						else
						{
							$menu_group_query = lavu_query("select * from `menu_groups` where `group_name`='ERS'"); 
							if(mysqli_num_rows($menu_group_query)) // Find a matching menu group
							{
								$menu_group_read = mysqli_fetch_assoc($menu_group_query);
								$menu_group_id = $menu_group_read['id'];
								$menu_id = $menu_group_read['menu_id'];
							}
							else // Create a menu group if one cannot be found
							{
								$dbg .= "Creating Menu Group: ERS\n";
								lavu_query("insert into `menu_groups` (`menu_id`,`group_name`,`_deleted`) values ('200','ERS','0')");
								$menu_group_query = lavu_query("select * from `menu_groups` where `group_name`='ERS'"); 
								if(mysqli_num_rows($menu_group_query))
								{
									$menu_group_read = mysqli_fetch_assoc($menu_group_query);
									$menu_group_id = $menu_group_read['id'];
									$menu_id = $menu_group_read['menu_id'];
								}
							}
							if($menu_id > 0) // Only add category if a menu group exists
							{
								$dbg .= "Creating Category: $item_category\n";
								$category_vars = array("menu_id"=>$menu_id,"group_id"=>$menu_group_id,"name"=>$item_category,"active"=>"1");
								lavu_query("insert into `menu_categories` (`menu_id`,`group_id`,`name`,`active`) values ('[menu_id]','[group_id]','[name]','[active]')",$category_vars);
								$category_query = lavu_query("select * from `menu_categories` where `name` LIKE '[1]'",$item_category);
								if(mysqli_num_rows($category_query)) // Find a matching category
								{
									$category_read = mysqli_fetch_assoc($category_query);
									$category_name = $category_read['name'];
									$category_id = $category_read['id'];
									$menu_id = $category_read['menu_id'];
									$menu_group_id = $category_read['group_id'];
								}
							}
						}
						if($category_id > 0) // Add Item only if category exists
						{
							$item_vars = array( "category_id"=>$category_id,
												"menu_id"=>$menu_id,
												"name"=>$item_name,
												"price"=>$item_price,
												"active"=>"1",
												"last_modified_date"=>date("Y-m-d H:i:s"));
							$col_list = "";
							$val_list = "";
							$conn = ConnectionHub::getConn('rest');
							foreach($item_vars as $colname => $colval)
							{
								if($col_list!="") $col_list .= ",";
								if($val_list!="") $val_list .= ",";
								$col_list .= "`" . $conn->escapeString($colname) . "`";
								$val_list .= "'[" . $conn->escapeString($colname) . "]'";
							}
							$dbg .= "Creating Item: $item_name\n";
							lavu_query("insert into `menu_items` ($col_list) values ($val_list)",$item_vars);
							$item_query = lavu_query("select * from `menu_items` where `name` LIKE '[1]'",$item_name);
							if(mysqli_num_rows($item_query)) // If Item Has been created then apply the item_id
							{
								$item_read = mysqli_fetch_assoc($item_query);
								lavu_query("update `order_contents` set `item_id`='[1]' where `id`='[2]'",$item_read['id'],$content_read['id']);
								$dbg .= "Item connected: $item_name\n";
							}
						}
					}
				}
			}
			hotel_comm_log($dbg);
		}
		
		return "";
	}
?>
