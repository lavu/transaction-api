<?php

$checkcmd = (isset($_POST['cmd']))?$_POST['cmd']:"";
if(!in_array($checkcmd, array("list", "insert", "update", "replace", "create_order"))) {
	//exit();
}

error_reporting(E_ALL);  //debug
require_once("../resources/core_functions.php");
$maindb = "poslavu_MAIN_db";
$dataname		= postvar("dataname");
$key			= postvar("key");
$token			= postvar("token");
$integratorKey	= postvar('integrator_key');

//Check Dataname Active Status & Billing Status to Prevent API Calls if Disabled
$chkIsAuth = isLocationAuthorized($dataname);

//Prevent to use API's
if ($chkIsAuth === false) {
$msg = 'Account Is Disabled';
//mapi call
if (in_array($checkcmd, array("list", "insert", "update", "replace", "sync_table"))) {
$code = 0;
$str = "<row>";
$str .= "<error>$code</error>";
$str .= "<reason>$msg</reason>";
$str .= "</row>";
echo $str;
exit;
} else if ($checkcmd == 'create_order') {
//oapi call
$res = array(
			'success' => false,
			'error'   => $msg
		);
echo json_encode($res);
exit;
} else {
//lsapi call
echo "0|".$msg;
exit;
}
}

if (trim($dataname)=="" && !isset($_GET['spec'])) {
	exit();
}

if (extension_loaded('newrelic')) {
	newrelic_add_custom_parameter("dataname", $dataname);
    newrelic_add_custom_parameter("client_ip", getClientIPServer());
}

$debug_blocking_for_dataname = ""; // mambo_italia_l
$debug_blocking = ($dataname == $debug_blocking_for_dataname);

if ($debug_blocking) {
	error_log("reqserv (".$dataname.") - ".dirname(__FILE__)." - ".(isset($_POST['cmd'])?$_POST['cmd']:"")." - ".json_encode($_REQUEST));
}

monitorAPIRequest($dataname);

//table=order_payments&column=id&value_min=1111&value_max=1111&limit=0,1000&valid_xml=1
/*if(isset($_POST['table']) && $_POST['table']=="order_payments" && isset($_POST['column']) && $_POST['column']=="id" && isset($_POST['limit']) && $_POST['limit']=="0,1000")
{
	exit();
}*/

//Check distro code
$priority_distro_code = false;
$url = $_SERVER['HTTP_HOST'];
if($url == 'api-crunchit.lavu.com' || $url == 'api-crunchitreports.lavu.com'){
	$priority_distro_code = true;
}

// Give lockout priority to restaurants with these datanames.
if (!$priority_distro_code) {
	require_once(dirname(__FILE__)."/server_load_functions.php");

	if (in_array($_SERVER['REMOTE_ADDR'], array("184.106.5.226", "184.106.112.36", "127.0.0.1"))) {
		check_lockout_status(2);//Priority
		check_local_lockout_status(40);
	} else if (in_array($dataname, array("honey_salt", "demo_avero", "mercury_expres", "paypalcafe_dem"))) {
		check_lockout_status(2);//Priority
		check_local_lockout_status(30);
	} else {
		check_lockout_status(1);//Normal
		check_local_lockout_status(15);
	}
}

require_once(dirname(__FILE__) . "/../resources/log_process_info.php");
lavu_write_process_info(array());

if ($debug_blocking) {
	error_log($dataname." checkpoint 1 ".postvar("cmd"));
}

if (trim($dataname)=="" && !isset($_GET['spec'])) {
	lavu_exit();
}

if ($dataname=="funcathappyhouseplanteatingpeople")
{
}
else
{
	$req_cmd = postvar("cmd","list");

	if ($req_cmd=="sync_orders_up" || $req_cmd=="records_check") {

		if ($debug_blocking) {
			error_log($dataname." checkpoint 2 ".$req_cmd);
		}

		if (am_i_blocked($dataname)) {
			if ($debug_blocking) {
				error_log($dataname." is blocked");
			}
			sync_blocked_handler($dataname); // will exit()
		}
		$dns_running = get_running_syncs();
		$runcount = count($dns_running);
		// maximum connections to LSAPI
		$i_max_concurrent_connects = 20;
		if ($runcount > $i_max_concurrent_connects) {
			if ($debug_blocking) {
				error_log($dataname." is blocked due to runcount (".$runcount.")");
			}
			mlavu_query("INSERT into `poslavu_MAIN_db`.`temp_info` (`type`,`dataname`,`datetime`) values ('sync orders block','[1]','[2]')",$dataname,date("Y-m-d H:i:s"));
			mlavu_query("DELETE from `poslavu_MAIN_db`.`temp_info` where `type`='sync orders running' and `dataname`='[1]'",$dataname);
			mlavu_query("UPDATE `poslavu_MAIN_db`.`temp_info` set `count`=`count` + 1 where `type`='lls sync orders blocked' limit 1");
			mlavu_close_db();
			exit();
		}

		// If connection allowed
		mlavu_query("UPDATE `poslavu_MAIN_db`.`temp_info` set `count`=`count` + 1 where `type`='lls sync orders allow' limit 1");
		$url_request =(isset($url_request))?$url_request:'';
		$request_info =(isset($request_info))?$request_info:'';
		mlavu_query("DELETE from `poslavu_MAIN_db`.`temp_info` where `type`='sync orders running' and `dataname`='[1]'",$dataname);
		$tempQuery = "INSERT INTO `poslavu_MAIN_db`.`temp_info` (`type`,`dataname`,`request`,`datetime`,`info`) values ('[1]','[2]','[3]','[4]','[5]')";
		mlavu_query($tempQuery,"sync orders running",$dataname,$url_request,date("Y-m-d H:i:s"),$request_info); // poslavu_MAIN_db
		$record_info_id = mlavu_insert_id();

		function checkin_before_close()
		{
			global $record_info_id;
			mlavu_query("DELETE from `poslavu_MAIN_db`.`temp_info` where `id`='[1]'",$record_info_id);
			mlavu_close_db();
			exit();
		}

	} else if ($req_cmd == "schedule_package_level_sync") {

		require_once(dirname(__FILE__)."/../../manage/misc/LLSFunctions/schedule_msync.php");
		require_once(dirname(__FILE__)."/../../sa_cp/billing/payment_profile_functions.php");
		$b_doquit = (postvar("doquit", "") == "1");
		// check that the restaurant exists
		$query = mlavu_query("SELECT `id` FROM `poslavu_MAIN_db`.`restaurants` WHERE `data_name`='[dataname]'", array('dataname'=>$dataname));
		if ($query === FALSE || mysqli_num_rows($query) == 0 || $dataname == "") {
			echo "restaurant '{$dataname}' not found";
			exit();
		}
		$query_read = mysqli_fetch_assoc($query);
		$restid = $query_read['id'];
		// get the package
		$a_package = find_package_status($restid);
		$i_package_level = (int)str_replace('none', '2', $a_package['package']);
		$i_package_level = max($i_package_level, 2);
		if (LLSFunctionsScheduleMsync::setPackage($dataname, $i_package_level)) {
			echo "success ({$i_package_level})";
		} else {
			echo "failure ({$i_package_level})";
		}
		if ($b_doquit)
			exit();
	}

	if (!function_exists("checkin_before_close")) {
		function checkin_before_close(){}
	}

	$debug = postvar("debug");
	$debug = ($debug=="debug8")?true:false;
	
	$mapi_version = postvar("version", "");	#Check mapi version in request

	if(isset($mapi_version) && strtolower($mapi_version) == 'v2'){
		$limit	= postvar("limit", "0,100");
	}else{
		$limit	= postvar("limit", "0,40");
	}

	$limit_code	= getQueryUpperLimit($limit,$mapi_version);
	if ($dataname && $key && $token)
	{
		$rest_query = mlavu_query("SELECT `id`, `data_name`, `modules` FROM `".$maindb."`.`restaurants` WHERE `data_name` = '[1]' AND `notes` NOT LIKE '%AUTO-DELETED%'", $dataname); // poslavu_MAIN_db
		if (mysqli_num_rows($rest_query))
		{
			$rest_read = mysqli_fetch_assoc($rest_query);
			if (isset($_POST['cmd']) && $_POST['cmd']=="validate_api_key_token")
			{
				if (strlen($token) > 30)
				{
					error_log("token greater than 30 for $dataname - shrinking token");
					$token = substr($token,0,20);
				}
			}

			$data_name = $dataname; // for lavu_query
			$rdb = "poslavu_".$dataname."_db";
			$restid = $rest_read['id'];

			if (!databaseExists($rdb))
			{
				echo "database not found";
				return;
			}

			lavu_connect_dn($dataname, $rdb);

			$loc_query = lavu_query("SELECT `id`, `menu_id`, `net_path`, `use_net_path` FROM `locations` WHERE `api_key` = AES_ENCRYPT('[1]', 'lavuapikey') AND `api_token` = AES_ENCRYPT('[2]' ,'lavutokenapikey')", $key, $token);
			if (mysqli_num_rows($loc_query))
			{
				$loc_read = mysqli_fetch_assoc($loc_query);
				$menu_id = $loc_read['menu_id'];
				$locationid = $loc_read['id'];
				$net_path = (isset($loc_read['net_path']))?$loc_read['net_path']:"";
				$use_net_path = (isset($loc_read['use_net_path']))?$loc_read['use_net_path']:"";
				if (trim($use_net_path)=="" || trim($use_net_path=="0"))
				{
					$loc_using_lls = false;
				}
				else if (strpos($net_path,"poslavu.com")!==false || strpos($net_path,"lavutogo.com")!==false)
				{
					$loc_using_lls = false;
				}
				else
				{
					$loc_using_lls = true;
				}

				if (!is_numeric($menu_id))
				{
					$menu_id = 0;
				}

				if (!is_numeric($locationid))
				{
					$locationid = 0;
				}

				$config_info = array();

				$cmd				= postvar("cmd", "list");
				$show_deleted			= postvar("show_deleted", "0");
				$cmd_table			= postvar("table");
				$filter_column			= postvar("column", "");
				$filter_value			= postvar("value", "");
				$filter_value_min		= postvar("value_min");
				$filter_value_max		= postvar("value_max");
				$auto_filter_column		= "";
				$auto_filter_value		= "";

				$field_list			= postvar("field_list",""); // list of field names
				$is_valid			= postvar("valid_xml",0);
				$encode_contents		= postvar("encode_contents",1);
				$format                 	= postvar("format", "xml");

				// ip address from localserver
				$ip_addy			= postvar("ip","");
				$query_extra                    = "";

				$location_api_debug = false;
				if (isset($config_info['api_debug'])) {
					$location_api_debug = ($config_info['api_debug'] == "1");
				}
				if ($location_api_debug) {
					error_log("Debugging API for ".$dataname." - cmd: ".$cmd." - ".$_SERVER['SERVER_NAME'].$_SERVER['REQUEST_URI']);
				}

				$lavu_query_should_log = $location_api_debug;

				$main_api = in_array($cmd, array("list", "insert", "update", "replace","sync_table")); //LP-8192
				$order_api = ($cmd == "create_order");

				if ($main_api) {

					// main api include
					//error_log("mapi: ".$dataname);

					#main api include
					require_once(dirname(__FILE__) . "/mapi.php");

				} else if ($order_api) {

					require_once(dirname(__FILE__) . "/oapi.php");

				} else {

					$a_lls_upload_commands = array('upload signature', 'upload waiver', 'sync_records_up', 'sync_orders_up', 'transmit_local_file', 'transmit_query_result', 'transmit_post_result', 'transmit_local_file_structure', 'database_structure');
					$a_blocked_lls_upload_commands = array('sync_records_up', 'sync_orders_up');
					if ($loc_using_lls || !in_array($cmd,$a_blocked_lls_upload_commands)) {
						update_lls_last_connected_by_day($maindb, $dataname, $locationid);
						// localserver api include
						require_once(dirname(__FILE__) . "/lsapi.php");
					}
				}

			} else {

				//else echo format_valid_xml('bad key or token',true); <--Original
				//Added final else, Brian D
				if (isset($cmd) && $cmd == 'validate_api_key_token') {

					$alertDropDownText = "The Lavu API Dataname you have provided does not match any of our records. Please check your settings in your backend control panel.";
					$textFieldText = "Invalid API Credentials.";
					$returnJSONArr = array(
						"status" => "failed",
						"drop_down_text" => $alertDropDownText,
						"text_field_text" => $textFieldText
						);
					echo json_encode($returnJSONArr);

				} else {

					echo "bad key or token";
					//echo format_valid_xml('bad key or token',true);
				}
			}

		} else {

			//--Original
			//echo "bad dataname display_text|This Dataname, key, and token set could not be found.  Please make sure your credentials have been entered correctly.";
			//Added By Brian D
			if (isset($cmd) && $cmd == 'validate_api_key_token'){

				$alertDropDownText = "The Lavu API Dataname you have provided does not match any of our records.  Please check your settings in your backend control panel.";
				$textFieldText = "Invalid API Credentials.";
				$returnJSONArr = array(
					"status" => "failed",
					"drop_down_text" => $alertDropDownText,
					"text_field_text" => $textFieldText
					);
				echo json_encode($returnJSONArr);

			} else {
				echo "bad dataname display_text|This Dataname, key, and token set could not be found. Please make sure your credentials have been entered correctly.";
			}
		}

	} else if ($dataname && $integratorKey && $token) {

		// for extensions
		$integratorQuery = mlavu_query("SELECT `id`, `permissions` FROM `poslavu_MAIN_db`.`extensions` WHERE `key`='[1]'", $integratorKey);
		if (mysqli_num_rows($integratorQuery)) {

			if (isset($_POST['testing'])) {
				ini_set('display_errors','1');
			}

			$integrator_read = mysqli_fetch_assoc($integratorQuery);
			$tempQuery = "SELECT `id`, `data_name` FROM `$maindb`.`restaurants` WHERE `data_name`='[1]' AND `notes` NOT LIKE '%AUTO-DELETED%'";
			$rest_query = mlavu_query($tempQuery,$dataname); // poslavu_MAIN_db
			if (mysqli_num_rows($rest_query)) {

				$rest_read = mysqli_fetch_assoc($rest_query);
				$rdb = "poslavu_" . $rest_read['data_name'] . "_db";
				lavu_connect_byid($rest_read['id'], $rdb);
				$tempQuery = "SELECT `id`, `menu_id` FROM `locations` WHERE `api_token`=AES_ENCRYPT('[1]','lavutokenapikey')";
				$loc_query = lavu_query($tempQuery,$token);
				if (mysqli_num_rows($loc_query)) {

					$loc_read = mysqli_fetch_assoc($loc_query);
					$tempQuery = "SELECT `id` FROM `config` WHERE `setting`='extension' AND `value` = '[1]' AND `location` = '[2]' ";
					$conf_query = lavu_query($tempQuery,$integrator_read['id'], $loc_read['id']);
					$locationid = $loc_read['id'];
					if (mysqli_num_rows($conf_query)) {

						require_once(dirname(__FILE__)."/../objects/json_decode.php");
						$json = new Json();
						$conf_read = mysqli_fetch_assoc($conf_query);
						$menu_id = $loc_read['menu_id'];
						if(!is_numeric($menu_id)){
							$menu_id = 0;
						}
						if(!is_numeric($locationid)){
							$locationid = 0;
						}
						$cmd = postvar("cmd","list");
						$show_deleted = postvar("show_deleted","0");
						$cmd_table = postvar("table");
						$filter_column = postvar("column","");
						$filter_value = postvar("value","");
						$filter_value_min = postvar("value_min");
						$filter_value_max = postvar("value_max");
						$auto_filter_column = "";
						$auto_filter_value = "";
						$field_list = postvar("field_list",""); // list of field names
						$is_valid = postvar("valid_xml",0);
						$encode_contents = postvar("encode_contents",1);
						// ip address from localserver
						$ip_addy = postvar("ip","");
						$query_extra = "";
						$permissions = $integrator_read['permissions'];
						$perms = $json->decode($permissions);			//this is the perms
						$testing = 0;
						if (postvar("testing")){
							$testing=1;
						}

						$main_api = in_array($cmd, array("list", "insert", "update", "replace"));

						if ($main_api) {
							#main api include
							require_once(dirname(__FILE__) . "/mapi.php");// main api include
						} else {
							require_once(dirname(__FILE__) . "/lsapi.php");	// localserver api include
						}

					} else echo "This user has not approved your integration.";

				} else echo "Invalid API token";

			} else echo "Invalid Dataname";

		} else echo "Invalid integrator ID";

	} else {

		$spec = (isset($_GET['spec']))?$_GET['spec']:"";
		if ($spec == "test") {

			//$postvars = "dataname=DEV&key=hjzqlFRu0CzIxCWryQuM&token=F3aRed41lckhFtMrxv8D";

			$postvars = "dataname=DEVKART&key=xeTcpowdJzigLQNjCjMs&token=MMhhOmMcgYgE8fXQ5YdJ";
			/*$set_contents = "
				<row>
					<transponderid>888888</transponderid>
				</row>
				";
			$postvars .= "&table=lk_karts&cmd=insert&contents=".$set_contents;*/
			//$postvars .= "&cmd=get_cckey";

			//$postvars .= "&table=orders&cmd=insert&contents=".$set_contents;
			//$postvars .= "&table=order_payments&column=order_id&value=1-2";

			//$postvars .= "&cmd=get_repo_file&filename=fwd_script.php";

			//$postvars .= "&table=order_payments";//&value_min=2011-01-01%2000:00:00&value_max=2011-08-01%2000:00:00&limit=0,20";
			$postvars .= "&cmd=list&table=menu_groups";
			//$postvars = "dataname=advanced_compu&key=g7U4fAz3mtql7rOiR7PR&token=yE0cbZjg0Rqk7zR73CpA&table=tables";
			//$postvars .= "&table=menu_categories&column=group_id&value=2";
			//$postvars .= "&table=menu_items&column=category_id&value=113";
			//$postvars .= "&table=menu_items&column=price&value=1250&encode_contents=1";
			//$postvars .= "&table=tables";//menu_categories&column=group_id&value=2";
			//$postvars .= "&table=orders&column=closed&value_min=2011-01-01%2000:00:00&value_max=2011-02-01%2000:00:00&limit=0,20";
			//$postvars .= "&table=order_contents&column=order_id&value=1";
			//$postvars .= "&cmd=database_structure";

			$ch = curl_init();
			//curl_setopt($ch, CURLOPT_URL, "https://api.poslavu.com/cp/reqserv/");
			curl_setopt($ch, CURLOPT_URL,"https://api-crunchit.lavu.com/cp/reqserv/");
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt($ch, CURLOPT_POST, 1);
			curl_setopt($ch, CURLOPT_POSTFIELDS, $postvars);
			$contents = curl_exec($ch);
			curl_close ($ch);

			$str = $contents;
			//$str = str_replace("<","&lt;",$str);
			//$str = str_replace(">","&gt;",$str);
			//$str = str_replace("\n","<br>",$str);
			//$str = str_replace("\t","&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;",$str);
			echo "<textarea rows='40' cols='150'>$str</textarea>";
			echo "<br>" . $_SERVER['HTTP_HOST'];
		}
	}
}

#To restrict the upper limit of query and fix it as 100
function getQueryUpperLimit($limit,$mapi_version){

	$limit_code 	= " limit ".$limit;		#for v1 or in absence of version key 
	if(strtolower($mapi_version) == "v2"){
		$query_limit = explode(",",$limit);
		if(count($query_limit)>1){
			if($query_limit[1] > 100){
				$query_limit[1] = 100;
			}
			$limit = implode(",",$query_limit);
			$limit_code 	= " limit ".$limit;
		}else{
			$limit_code  = " limit 0,100";
		}
	}
	return  $limit_code;
}

function get_special_tag_contents($tag,$data)
{
	$start_tag = "[_".$tag."_start_]";
	$end_tag = "[_".$tag."_end_]";
	if (strpos($data,$start_tag)!==false && strpos($data,$end_tag)!==false)
	{
		$start_pos = strpos($data,$start_tag) + strlen($start_tag);
		$end_pos = strpos($data,$end_tag);
		$content_length = $end_pos - $start_pos;
		return substr($data,$start_pos,$content_length);
	}
	else return "";
}

function special_tag_exists($tag, $data)
{
	if (is_array($data))
	{
		return false;
	}

	$exists = false;
	$start_tag = "[_".$tag."_start_]";
	$end_tag = "[_".$tag."_end_]";
	if (strpos($data, $start_tag)!==false && strpos($data,$end_tag)!==false) {
		$exists = true;
	}
	return $exists;
}

function create_insert_col_list($cols,$include_id=true)
{
	$clist = "";
	$icount = 0;
	for ($i=0; $i<count($cols); $i++) {
		if ($cols[$i]!="id" || $include_id) {
			if ($icount > 0) {
				$clist .= ",";
			}
			$clist .= "`".$cols[$i]."`";
			$icount++;
		}
	}
	return $clist;
}

function create_insert_val_list($vals,$cols=false,$include_id=true)
{
	$vlist = "";
	$icount = 0;
	for($i=0; $i<count($vals); $i++)
	{
		if($cols[$i]!="id" || $include_id)
		{
			if($icount > 0)
				$vlist .= ",";
			$vlist .= "'".ConnectionHub::getConn('poslavu')->escapeString($vals[$i])."'";
			$icount++;
		}
	}
	return $vlist;
}

function create_insert_query_str($tablename,$cols,$vals,$include_id=true)
{
	$col_list = create_insert_col_list($cols,$include_id);
	$val_list = create_insert_val_list($vals,$cols,$include_id);
	return "insert into `$tablename` ($col_list) values ($val_list)";
}

function create_update_query_str($tablename, $row_id, $cols, $vals)
{
	$update_list = "";
	for ($i = 0; $i < count($cols); $i++) {
		$col = $cols[$i];
		if ($col != "id") {
			if (!empty($update_list)) $update_list .= ", ";
			$update_list .= "`".$col."` = '".ConnectionHub::getConn('poslavu')->escapeString($vals[$i])."'";
		}
	}

	return "UPDATE ".$tablename." SET ".$update_list." WHERE `id` = '".$row_id."'";
}

function get_database_info($dbname)
{
	$table_list = array();
	$field_list = array();
	// creates array
	// dbinfo['tables'] = table_list
	// dbinfo['fields'] = field_list[tablename][0] = array of field names
	//                                         [1][fieldname] = associative array of field info
	$table_query = mlavu_query("SHOW tables from $dbname");
	while ($table_read = mysqli_fetch_row($table_query))
	{
		$tablename = $table_read[0];
		$table_list[] = $tablename;
		$field_list[$tablename] = array(array(),array());
		$field_query = mlavu_query("SHOW columns from `[1]`.`[2]`",$dbname,$tablename);
		while ($field_read = mysqli_fetch_row($field_query))
		{
			$fieldname = $field_read[0];

			$field_info = array();
			$field_info['name'] = $fieldname;
			$field_info['type'] = $field_read[1];
			$field_info['null'] = $field_read[2];
			$field_info['key'] = $field_read[3];
			$field_info['default'] = $field_read[4];
			$field_info['extra'] = $field_read[5];

			$field_list[$tablename][0][] = $fieldname;
			$field_list[$tablename][1][$fieldname] = $field_info;
		}
	}

	$dbinfo = array();
	$dbinfo['tables'] = $table_list;
	$dbinfo['fields'] = $field_list;
	return $dbinfo;
}

function simple_array2xml($arr)
{
	$xml = '<?xml version="1.0" encoding="UTF-8"?>'.PHP_EOL;
	$xml .= '<results>'.PHP_EOL;
	foreach ($arr as $element => $val) {
		$xml .= '<'.$element.'>'.$val.'</'.$element.'>'.PHP_EOL;
	}
	$xml .= '</results>'.PHP_EOL;

	return $xml;
}

function format_valid_xml(array $data = array(),$isError = false)
{
	$xml = '<?xml version="1.0" encoding="UTF-8"?>';
	if (!$isError) {
		// valid response
		$xml .= '<result><'.$data['key'].'>'.$data['val'].'</'.$data['key'].'></result>';
	} else {
		$xml .= '<result><error>'.$data['error'].'</error></result>';
	}

	return $xml;
}

// track which day a lls last connected on
function update_lls_last_connected_by_day($maindb, $dataname, $locationid)
{
	require_once("/home/poslavu/public_html/admin/manage/misc/LLSFunctions/getSyncVersion.php");
	if (insert_into_localserver_exists_table($maindb,$dataname,$locationid)){
		update_lls_sync_version_number($dataname, $locationid, FALSE);
	}
}

// returns true if the mysql entry was changed
function insert_into_localserver_exists_table($maindb,$dataname,$locationid)
{
	global $maindb;
	if (!isset($maindb) || $maindb == ''){
		$maindb = "poslavu_MAIN_db";
	}
	$database = "poslavu_".$dataname."_db";
	$tempQuery = "SELECT SQL_NO_CACHE `value` FROM `[database]`.`config` WHERE `location`='[locationid]' and `setting`='last sync finished' ORDER BY id ASC LIMIT 1";
	$lsync_query = mlavu_query($tempQuery,array('database'=>$database, 'locationid'=>$locationid));
	//lavu_connect_dn($dataname,$database);
	//$lsync_query = lavu_query($tempQuery,array('database'=>$database, 'locationid'=>$locationid));
	if (mysqli_num_rows($lsync_query)) {

		$lsync_read = mysqli_fetch_assoc($lsync_query);
		$last_ll_sync = $lsync_read['value'];
		$last_ll_sync = explode('/', $last_ll_sync);
		$last_ll_sync = (int)$last_ll_sync[0];

	} else {

		$last_ll_sync = 0;
	}
	if ($last_ll_sync < strtotime(date("Y-m-d")))
	{
		$encryption_type = "zendguard";
		if (insert_into_localserver_exists_table_using_ioncube($dataname,$locationid)) {
			$encryption_type = "ioncube";
		}
		// these values will be inserted into local_servers and some of them into config
		$vars = array();
		$vars["last_checked_in"] = time();
		$vars["encryption_type"] = $encryption_type;
		$vars["data_name"] = $dataname;
		$vars["last_ip_address"] = $_SERVER['REMOTE_ADDR'];
		$vars["location_id"] = $locationid;
		$vars["server"] = get_server_hostname();
		//Added by Brian D.
		$vars['lls_version'] = $_POST['lls_install_version'];
		$vars['lls_install_date'] = str_replace('_', '-', $_POST['lls_install_date'] );
		// these values will be inserted into config
		$allvars = $vars;
		$allvars["maindb"] = $maindb;
		$allvars["database"] = $database;
		$allvars["datetime"] = date("Y-m-d H:i:s");
		$keys = array();
		foreach($vars as $k=>$v){
			$keys[] = $k;
		}
		$values = "'[".implode("]','[", $keys)."]'";
		$keys = "`".implode("`,`", $keys)."`";
		$insert_string = "INSERT INTO `[maindb]`.`local_servers` (".$keys.") VALUES (".$values.")";
		$insert_last_ll_sync_string = "INSERT INTO `[database]`.`config` (`location`,`setting`,`value`,`value_long`,`type`) VALUES ('[location_id]','last sync finished','[last_checked_in]/60','[datetime]\n[last_ip_address]','log')";
		mlavu_query("DELETE FROM `[maindb]`.`local_servers` WHERE `data_name`='[data_name]' AND `location_id`='[location_id]'", $allvars); // poslavu_MAIN_db
		mlavu_query($insert_string, $allvars); // poslavu_MAIN_db
		mlavu_query("DELETE FROM `[database]`.`config` WHERE `location`='[location_id]' AND `setting`='last sync finished'", $allvars);
		mlavu_query($insert_last_ll_sync_string, $allvars);
		foreach ($allvars as $k=>$v) {
			$insert_string = str_replace("[$k]",$v,$insert_string);
		}
		return TRUE;

	} else {

		$ipQuery = mlavu_query("SELECT `last_ip_address` FROM `poslavu_MAIN_db`.`local_servers` WHERE `data_name`='[dataname]' LIMIT 1", array("dataname"=>$dataname));
		$ipQueryRead = ($ipQuery !== FALSE && mysqli_num_rows($ipQuery) > 0) ? mysqli_fetch_assoc($ipQuery) : FALSE;
		if ($ipQueryRead !== FALSE && $ipQueryRead['last_ip_address'] != $_SERVER['REMOTE_ADDR']) {
			mlavu_query("UPDATE `poslavu_MAIN_db`.`local_servers` SET `last_ip_address`='[new_ip_address]' WHERE `data_name`='[dataname]'", array("dataname"=>$dataname, "new_ip_address"=>$_SERVER['REMOTE_ADDR']));
		}
	}

	return FALSE;
}

function get_server_hostname()
{
	$s_hostname = $_SERVER['SERVER_NAME'];
	$a_hostname_parts = explode('.poslavu', $s_hostname);
	return $a_hostname_parts[0];
}

function insert_into_localserver_exists_table_using_ioncube($dataname,$locationid)
{
	$tempQuery = "SELECT `value_long` FROM `poslavu_[1]_db`.`config` WHERE `setting`='local_file_structure' AND `location`='[2]'";
	$locserv_query = mlavu_query($tempQuery, $dataname, $locationid);
	//lavu_connect_dn($dataname,'poslavu_'.$dataname.'_db');
	//$locserv_query = lavu_query($tempQuery, $dataname, $locationid);
	if (!$locserv_query) {
		return TRUE;
	}
	if (mysqli_num_rows($locserv_query) < 1) {
		return TRUE;
	}
	$locserv_read = mysqli_fetch_assoc($locserv_query);
	if ( (strpos($locserv_read['value_long'], "zendguard_loader") || strpos($locserv_read['value_long'], "ZendGuardLoader")) && (strpos($locserv_read['value_long'], "ioncube_loader")==0) ){
		return FALSE;
	} else {
		return TRUE;
	}
}

function get_running_syncs()
{
	$runcount = 0;
	$dns_running = array();
	$myReturn = array();
	$mindt = date("Y-m-d H:i:s",time() - 120);
	$tempQuery = "SELECT DISTINCT `dataname` from `poslavu_MAIN_db`.`temp_info` where `type`='sync orders running' and `datetime`>='[1]'";
	$syncs_query = mlavu_query($tempQuery,$mindt); // poslavu_MAIN_db
	while ($syncs_read = mysqli_fetch_assoc($syncs_query)){
		$dns_running[] = $syncs_read['dataname'];
	}
	return $dns_running;
}

function am_i_blocked($dataname)
{
	$tempQuery = "SELECT `type` from `poslavu_MAIN_db`.`temp_info` where `type`='sync orders block' and `dataname`='[1]'";
	$blocked_query = mlavu_query($tempQuery,$dataname); // poslavu_MAIN_db
	if (mysqli_num_rows($blocked_query)){
		return true;
	}
	return false;
}

function sync_blocked_handler($dataname)
{
	//Unblock
	mlavu_query("DELETE from `poslavu_MAIN_db`.`temp_info` where `type`='sync orders block' and `dataname`='[1]'",$dataname);
	mlavu_query("DELETE from `poslavu_MAIN_db`.`temp_info` where `type`='sync orders running' and `dataname`='[1]'",$dataname);
	//update blocked count
	mlavu_query("UPDATE `poslavu_MAIN_db`.`temp_info` set `count`=`count` + 1 where `type`='lls sync orders blocked' limit 1");
	mlavu_close_db();
	exit();
}

function monitorAPIRequest($dataname) {

	$settings_file = "monitor_settings.txt";
	if (!file_exists($settings_file)) {
		return;
	}

	$lines = file($settings_file);
	if (count($lines) == 0) {
		return;
	}

	$settings = array();

	foreach ($lines as $line) {
		$key_val = explode("=", $line);
		if (count($key_val) < 2) {
			continue;
		}
		$key = $key_val[0];
		$val = $key_val[1];
		if (strlen($key) == 0) {
			continue;
		}
		$settings[$key] = $val;
	}

	$request_size = (int) $_SERVER['CONTENT_LENGTH'];

	$log_notice = false;
	$record_request = false;

	if (isset($settings['log_notice_threshold'])) {
		$log_notice = ($request_size >= (int)$settings['log_notice_threshold']);
	}

	if (isset($settings['record_threshold'])) {
		$record_request = ($request_size >= (int)$settings['record_threshold']);
	}

	if (isset($settings['monitor_datanames'])) {
		$monitor_datanames = explode(",", trim($settings['monitor_datanames']));
		if (in_array($dataname, $monitor_datanames)) {
			$log_notice = true;
			$record_request = true;
		}
	}

	$cmd = postvar("cmd", "list (default)");

	if ($log_notice || $record_request) {
		error_log("API request for ".$dataname." - cp - ".$request_size." bytes - ".$cmd);
	}

	if ($record_request) {

		$path = "/home/poslavu/logs/company/".$dataname."/API";
		if (!file_exists($path)) {
			mkdir($path, 0775, true);
		}

		rename($path."/".$cmd, $path."/".$cmd."_last");

		$fp = fopen($path."/".$cmd, "w");
		fputs($fp, date("Y-m-d H:i:s")."\n".json_encode($_REQUEST));
		fclose($fp);
	}
}
