<?php
	if($api_type=="qb" && $dataname && $key)
	{
		//------------------- functions from functions.php -------------------//	
		function strfind($haystack,$needle)
		{
			return (strpos($haystack,$needle)!==false);
		}	
		function parse_chars($str,$dr)
		{
			$convert_chars = array();
			$convert_chars[] = array(",","comma");
			$convert_chars[] = array("-","dash");
			$convert_chars[] = array("(","leftparen");
			$convert_chars[] = array(")","rightparen");
			
			for($i=0; $i<count($convert_chars); $i++)
			{
				if($dr=="in")
					$str = str_replace($convert_chars[$i][0],"[".$convert_chars[$i][1]."]",$str);
				else if($dr=="out")
					$str = str_replace("[".$convert_chars[$i][1]."]",$convert_chars[$i][0],$str);
			}
			return $str;
		}	
		function create_order_option_array($order_option_read, $merchandise_cost)
		{		
			$options_array = explode(",",$order_option_read);
			$options_array_out = array();
			for($n=0; $n<count($options_array); $n++)
			{
				$option_data = explode("(", $options_array[$n]);
				if(count($option_data)>1)
				{
					$option_output = trim($option_data[0]);
					$option_cost = explode(")", $option_data[1]);
					$option_cost = explode("-", $option_cost[0]);
					if(count($option_cost)>1)
					{
						$option_cost = trim(str_replace("$", "", $option_cost[1]));
						if(strfind($option_cost, "%"))
						{
							$option_perc = str_replace("%", "", $option_cost);
							$option_cost = $merchandise_cost * ($option_perc / 100);
						}
						
						if($option_cost > 0)
						{
							$option_output = parse_chars($option_output,"out");
							
							array_push($options_array_out, array($option_output, $option_cost));
						}
					}				
				}
			}
			return $options_array_out;
		}
		//---------------------------------------------------------------------------------------------------//
		
		function apivar($var, $def=false)
		{
			if(isset($_POST[$var])) return $_POST[$var];
			else if(isset($_GET[$var])) return $_GET[$var];
			else return $def;
		}
		
		function qstr($str)
		{
			return str_replace("'","''",$str);
		}
		
		$api_success = 1;
		$api_error = "";
		$api_current_record = array();
		$api_records = array();
		
		function api_error($str)
		{
			global $api_success;
			global $api_error;
			
			$api_success = 0;
			$api_error = $str;
		}
		
		function api_add_record($name="record")
		{
			global $api_records;
			global $api_current_record;
			
			$api_records[] = array($name,$api_current_record);
			$api_current_record = array();
		}
		
		function api_add_element($element, $value)
		{
			global $api_current_record;
			
			$api_current_record[] = array($element, $value);
		}
		
		function api_web_display($output)
		{
			$output = str_replace("<","&lt;",$output);
			$output = str_replace(">","&gt;",$output);
			$output = str_replace("\n","<br>",$output);
			$output = str_replace("\t","&nbsp;&nbsp;&nbsp;&nbsp;",$output);
			return $output;
		}
		
		if($_SERVER['SERVER_PORT']==443)
		{
			//$login = apivar("api_login");
			//$password = apivar("api_password");
			
			$cmd = apivar("cmd");
			
			if($cmd=="customers")
			{
				/*$customer_query = lavu_query("select * from `customer_accounts`");
				while($customer_read = mysqli_fetch_assoc($customer_query))
				{
					$fullname = trim($customer_read['firstname'] . " " . $customer_read['lastname']);
					$phone = $customer_read['phone'];
					
					api_add_element("fullname",$fullname);
					api_add_element("phone",$phone);
					api_add_record();
				}*/
			}
			else if($cmd=="datetime")
			{
				api_add_element("date",date("Y-m-d"));
				api_add_element("time",date("H:i:s"));
				api_add_element("show_date",date("m/d/Y"));
				api_add_element("show_time",date("h:i a"));
				api_add_record();
			}
			else if($cmd=="order_list")
			{
				//$order_query = ers_query("select * from `orders` where `order_activated`='1' and `canceled`!='1' and ((`date`>='2011-01-01' and `qb_listid`='') or (`date`>='".date("Y-m-d")."')) UNION ALL select * from `orders_archive` where `order_activated`='1' and `canceled`!='1' and `date`>='2011-01-01' and `qb_listid`=''");
				$mindate = "2011-01-01";
				$order_query = lavu_query("select * from `orders` where `void`!='1' and `opened`>='[1] 05:00:00'");// UNION ALL select * from `orders_archive` where `void`!='1' and `opened`>='[1] 05:00:00'",$mindate);
				while($order_read = mysqli_fetch_assoc($order_query))
				{
					api_add_element("id",$order_read['id']);
					api_add_record();
				}
			}
			else if($cmd=="payment_list")
			{
				$mindate = "2011-01-01";
				$pay_query = lavu_query("select * from `orders_payments` where `date`>='[1] 05:00:00'",$mindate);
				while($pay_read = mysqli_fetch_assoc($pay_query))
				{
					api_add_element("id",$pay_read['id']);
					api_add_element("order_id",$pay_read['order_id']);
					api_add_element("amount",number_format($pay_read['amount'],2,".",""));
					api_add_element("type",$pay_read['type']);
					api_add_element("date",$pay_read['date']);
					api_add_record();
				}
			}
			else if($cmd=="order_info")
			{
				$orderid_list = explode(",",apivar("orderid")); 
				
				for($x=0; $x<count($orderid_list); $x++)
				{
					$orderid = $orderid_list[$x];
					$order_query = lavu_query("select * from `orders` where id='[1]' UNION ALL select * from `orders_archive` where `id`='[1]'",$orderid);
					while($order_read = mysqli_fetch_assoc($order_query))
					{
						//---------- inventory ------------------//
						$items = "";
						$sched_query = lavu_query("select `inventory`.`taxable` as `taxable`, `inventory`.`id` as `id`, `inventory`.`name` as `name`, `schedule`.`unit_cost` as `unit_cost`, `schedule`.`quantity` as `quantity`, `total_cost` as `total_cost` from `schedule` LEFT JOIN `inventory` ON `schedule`.`product`=`inventory`.`id` where `schedule`.`order`='[1]' UNION ALL select `inventory`.`taxable` as `taxable`, `inventory`.`id` as `id`, `inventory`.`name` as `name`, `schedule_archive`.`unit_cost` as `unit_cost`, `schedule_archive`.`quantity` as `quantity`, `total_cost` as `total_cost` from `schedule_archive` LEFT JOIN `inventory` ON `schedule_archive`.`product`=`inventory`.`id` where `schedule_archive`.`order`='[1]'",$orderid);
						while($sched_read = mysqli_fetch_assoc($sched_query))
						{
							$taxable = strtolower($sched_read['taxable']);
							if($taxable=="no")
								$taxable = "0";
							else
								$taxable = "1";
						
							$isep = chr(27);
							if($items!="") $items .= chr(28);
							$items .= $sched_read['name'] . $isep . $sched_read['unit_cost'] . $isep . $sched_read['quantity'] . $isep . $sched_read['total_cost'] . $isep . $sched_read['id'] . $isep . $taxable . $isep . "";
						}
						
						//----------- payments ---------------------//
						/*$payments = "";
						$pay_query = ers_query("select * from `orders_payments` where `orderid`='$orderid'");
						while($pay_read = mysqli_fetch_assoc($pay_query))
						{
						}*/
						
						//----------- order options ------------------//
						$merchandise_cost = $order_read['total'];
						$option_list = trim($order_read['order_options']);
						if(trim($order_read['order_options_top'])!="")
						{
							if($option_list!="") $option_list .= ",";
							$option_list .= trim($order_read['order_options_top']);
						}
							
						$options_array = create_order_option_array($option_list, $merchandise_cost);
						for($n=0; $n<count($options_array); $n++)
						{
							$option_output = $options_array[$n][0];
							$option_cost = $options_array[$n][1];
							
							//$total_cost += $option_cost;
							//$str_right .= "<br>" . $option_output . spaces(6) . price($option_cost);
							$taxable = "1";
							$isep = chr(27);
							if($items!="") $items .= chr(28);
							$items .= $option_output . $isep . $option_cost . $isep . "1" . $isep . $option_cost . $isep . "" . $isep . $taxable . $isep . "";
						}
						//------------------------------------------------//
						
						$date_ordered = explode(" ",$order_read['date_ordered']);
						$date_ordered = $date_ordered[0];
					
						api_add_element("id",$order_read['id']);
						api_add_element("customerid",$order_read['customerid']);
						api_add_element("date_ordered",$date_ordered);
						api_add_element("date_due",$order_read['date']);
						api_add_element("total",$order_read['total']);
						api_add_element("tax_rate",str_replace("%","",$order_read['tax_rate']));
						api_add_element("tax_amount",$order_read['tax_amount']);
						api_add_element("travel_fee",str_replace("$","",$order_read['travelfee']));
						api_add_element("discount_applied",$order_read['discount_applied']);
						api_add_element("setup_fee",$order_read['setup_fee']);
						api_add_element("order_total",$order_read['order_total']);
						
						api_add_element("firstname",$order_read['firstname']);
						api_add_element("lastname",$order_read['lastname']);
						api_add_element("phone",$order_read['phone']);
						api_add_element("email",$order_read['email']);
						
						api_add_element("address",$order_read['address']);
						api_add_element("city",$order_read['city']);
						api_add_element("state",$order_read['state']);
						api_add_element("zip",$order_read['zip']);
						api_add_element("country","USA");
						
						api_add_element("party address",$order_read['party address']);
						api_add_element("party city",$order_read['party city']);
						api_add_element("party state",$order_read['party state']);
						api_add_element("party zip",$order_read['party zip']);
						api_add_element("party country","USA");
						
						api_add_element("inventory",$items);
						api_add_record();
					}
				}
			}
			else api_error("Unknown command");
		}
		else api_error("Connection not secure");
		
		/*function get_xml_response()
		{
			$output = "";
			$output .= "\n<response>";
			$output .= "\n\t<status>";
			$output .= "\n\t\t<success>$api_success</sucess>";
			$output .= "\n\t\t<error>$api_error</error>";
			$output .= "\n\t</status>";
			$output .= "\n\t<records>";
			for($i=0; $i<count($api_records); $i++)
			{
				$record_name = $api_records[$i][0];
				$record_values = $api_records[$i][1];
				$output .= "\n\t\t<".$record_name.">";
				for($n=0; $n<count($record_values); $n++)
				{
					$element_name = $record_values[$n][0];
					$element_value = $record_values[$n][1];
					
					$output .= "\n\t\t\t<element>";
					$output .= "\n\t\t\t\t[name]$element_name[-name]";
					$output .= "\n\t\t\t\t[value]$element_value[-value]";
					$output .= "\n\t\t\t</element>";
				}
				$output .= "\n\t\t</".$record_name.">";
			}
			$output .= "\n\t</records>";
			$output .= "\n</response>";
			$output = trim($output,"\n");
			return $output;
		}*/
	
		function encode_element($str)
		{
			$str = str_replace("|","[[bar]]",$str);
			$str = str_replace("~","[[tilde]]",$str);
			return $str;
		}
		
		$output = "";
		$field_output = "";
		for($i=0; $i<count($api_records); $i++)
		{
			if($field_output=="") $collect_field_info = true; else $collect_field_info = false;
			
			$record_name = $api_records[$i][0];
			$record_values = $api_records[$i][1];
			if($output!="") $output .= "|";
					
			$elements = "";
			for($n=0; $n<count($record_values); $n++)
			{
				$element_name = $record_values[$n][0];
				$element_value = $record_values[$n][1];
	
				if($elements!="") $elements .= "~";
				$elements .= encode_element($element_value);
				if($collect_field_info)
				{
					if($field_output!="") $field_output .= "~";
					$field_output .= $element_name;
				}
			}
			$output .= $elements;
		}
		if($output!="")
			echo $field_output .= "|" . $output;
	}
?>
