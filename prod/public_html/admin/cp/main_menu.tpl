
<head>
	<title>POSLavu Admin Control Panel</title>
    <style>
		body, table {
			font-family:Verdana, Arial, Helvetica, sans-serif;
			font-size:12px;
		}
		a:link, a:visited {
			color: #95AE31;
			font-family: Arial, Helvetica, sans-serif;
			font-size:11px;
			font-weight:700;
			text-decoration:underline;
		}
		a:hover {
			color: #aecd37;
			font-family: Arial, Helvetica, sans-serif;
			font-size:11px;
			font-weight:700;
			text-decoration:underline;
		}
		.form_row_title {
			text-align:right;
			font-size:16px;
		}
		.form_submit_button {
			padding-right:20px;
			padding-left:20px;
			font-weight:bold;
			font-size:16px;
		}
		.top_panel {
			color:#c6ef26;
			font-weight:bold;
			font-size:16px;
		}
		.top_panel2 {
			color:#c6ef26;
			font-weight:bold;
			font-size:14px;
		}
		.top_panel a:link, .top_panel a:visited {
			color:#ffffff;
			font-weight:bold;
			text-decoration:none;
			font-size:16px;
		}
		.top_panel a:hover {
			color:#a6cf26;
			font-weight:bold;
			text-decoration:none;
			font-size:16px;
		}
		.top_nav {
			font-family:Verdana, Arial, Helvetica, sans-serif;
			color:#696969;
			font-weight:bold;
			font-size:10px;
		}
		.top_nav a {
			font-family:Verdana, Arial, Helvetica, sans-serif;
			color:#696969;
			font-weight:bold;
			text-decoration:none;
			font-size:10px;
		}
		.top_nav a:visited {
			font-family:Verdana, Arial, Helvetica, sans-serif;
			color:#696969;
			font-weight:bold;
			text-decoration:none;
			font-size:10px;
		}
		.top_nav a:hover {
			font-family:Verdana, Arial, Helvetica, sans-serif;
			color:#454545;
			font-weight:bold;
			text-decoration:none;
			font-size:10px;
		}
		.top_nav a:active {
			font-family:Verdana, Arial, Helvetica, sans-serif;
			color:#454545;
			font-weight:bold;
			text-decoration:none;
			font-size:10px;
		}
		.titletext {
			color: #414141;
			font-family: Arial, Helvetica, sans-serif;
			font-size:16px;
			font-weight:600;
		}
		.headline_text {
			color: #868686;
			font-family: 'Trebuchet MS', Arial, Helvetica, sans-serif;
			font-size:16px;
			font-weight:400;
		}
		.maintext {
			color: #414141;
			font-family: Arial, Helvetica, sans-serif;
			font-size:12px;
			}
		.maintextlight {
			color: #919191;
			font-family: Arial, Helvetica, sans-serif;
			font-size:12px;
			}
		.maintexthead {
			color: #414141;
			font-family: Arial, Helvetica, sans-serif;
			font-size:12px;
			}
		#maintext {
			color: #414141;
			font-family: Arial, Helvetica, sans-serif;
			font-size:12px;
			}
		#maintext h4{
			color: #95AE31;
			font-family: Arial, Helvetica, sans-serif;
			font-size:12px;
			font-weight:300;
		}
		#maintext h3{
			color: #797979;
			font-family: Arial, Helvetica, sans-serif;
			font-size:14px;
			font-weight:600;
		}
		#maintext h2{
			color: #797979;
			font-family: Arial, Helvetica, sans-serif;
			font-size:16px;
			font-weight:600;
		}
		#maintext a:link {
			color: #95AE31;
			font-family: Arial, Helvetica, sans-serif;
			font-size:11px;
			font-weight:700;
		}
		#maintext a:visited {
			color: #95AE31;
			font-family: Arial, Helvetica, sans-serif;
			font-size:11px;
			font-weight:700;
		}
		#maintext a:hover {
			color: #797979;
			font-family: Arial, Helvetica, sans-serif;
			font-size:11px;
			font-weight:700;
		}
		.overview_greentext {
			color: #95AE31;
			font-family: Arial, Helvetica, sans-serif;
			font-size:11px;
			font-weight:700;
			text-decoration: none;	
		}
		.overview_blacktext {
			color: #5c5c5c;
			font-family: Arial, Helvetica, sans-serif;
			font-size:11px;
			font-weight:700;
			text-decoration: none;
		}
		.help_greentext {
			color: #95AE31;
			background-color:e9eed3;
			font-family: Arial, Helvetica, sans-serif;
			font-size:12px;
			font-weight:500;
			text-decoration: none;
			padding:10;
		}
		.numbox {
			color: #fff;
			background-image:url(images/circ1.png);
			font-family: Arial, Helvetica, sans-serif;
			font-size:10px;
			font-weight:700;
		}
		.msg {
			color: #95AE31;
			font-family: Arial, Helvetica, sans-serif;
			font-size:12px;
			font-weight:300;
			text-decoration: none;
		}
		.msg_bold {
			color: #95AE31;
			font-family: Arial, Helvetica, sans-serif;
			font-size:12px;
			font-weight:700;
			text-decoration: none;
		}
		#boxlinks {	color: #fff;
			font-family: Arial, Helvetica, sans-serif;
			font-size:14px;
			font-weight:600;
		}
		#boxlinks a:link, #boxlinks a:visited {	
			color: #fff;
			font-family: Arial, Helvetica, sans-serif;
			text-decoration: none;
			font-size:14px;
			font-weight:600;
		}
		#boxlinks a:hover {	
			color: #505050;
			font-family: Arial, Helvetica, sans-serif;
			text-decoration: none;
			font-size:14px;
			font-weight:600;
		}
		.boxlinks {	color: #fff;
			font-family: Arial, Helvetica, sans-serif;
			font-size:14px;
			font-weight:600;
		}
		.boxlinks a:link, .boxlinks a:visited {	
			color: #fff;
			font-family: Arial, Helvetica, sans-serif;
			text-decoration: none;
			font-size:14px;
			font-weight:600;
		}
		.boxlinks a:hover {	
			color: #505050;
			font-family: Arial, Helvetica, sans-serif;
			text-decoration: none;
			font-size:14px;
			font-weight:600;
		}
	.msg a:link, .msg a:visited, .msg a:hover {
			color: #95AE31;
			font-family: Arial, Helvetica, sans-serif;
			font-size:12px;
			font-weight:300;
			text-decoration: underline;
		}
	.sectiontitle {	color: #9f9f9f;
			text-decoration: none;
			font-family: Arial, Helvetica, sans-serif;
			font-weight:400;
			font-size:24px;
		}
	.sectiontext { color: #9f9f9f;
			text-decoration: none;
			font-family: Arial, Helvetica, sans-serif;
			font-weight:400;
			font-size:12px;
		}
	.whitetitle {
			color: #fff;
			font-family: Arial, Helvetica, sans-serif;
			font-size:16px;
			font-weight:700;
		}
	.whitetitle_small {
			color: #fff;
			font-family: Arial, Helvetica, sans-serif;
			font-size:12px;
			font-weight:700;
		}
	.cct_labels {
			color:#999999;
			font-size:11px;
		}
	.cct_info {
			color:#666666;
			font-size:11px;
		}
	.videobox{
			background-image:url(images/youtube_box_full.jpg);
			
		}
	.table_header { font-family:Arial, Helvetica, sans-serif; font-size:12px; color:#006600; padding:1px 5px 1px 5px; }
	.info_value { font-family:Arial, Helvetica, sans-serif; font-size:14px; color:#000000; }
	.info_value2 { font-family:Arial, Helvetica, sans-serif; font-size:13px; color:#111111; }
	.info_value3 { font-family:Arial, Helvetica, sans-serif; font-size:12px; color:#222222; }

	/* howdy bar ------------------------------------------------------- */
	.howdydo-style {
		background: #EAEAAE;
		border-bottom: 2px solid #DBDB70;
		color: #D44942;
		font-size: 16px;
		text-align: center;

	}
	#howdydo-wrapper {
		overflow: visible;
		padding: 0;
		width: 100%;
		z-index: 999999;
	}
	.howdydo-box {
		display: none;
		padding: 10px 0;
		width: 100%;
	}
	#howdydo-close {
		position: absolute;
	/*	float: right; 
		margin: 0 30px 0 0; */
		bottom: 10px;
		right: 30px;
		border:none;
	
	}
	#howdydo-open {
		padding: 7px;
		position: absolute;
		right: 20px;
		top: 0;
		border:none;
		
	} 
	.howdydo-hover { position: absolute; top: 0;  left: 0; }
	.howdydo-scroll { position: fixed; top: 0;  left: 0; }
	.howdydo-push {	position: relative; clear: both; float: left; margin-bottom: 1px; }
	#notify a { color: #336699; font-size: 12px; } 

	</style>
	<link rel="stylesheet" href="styles/ios-checkbox.css" type="text/css" media="screen" charset="utf-8" />
	<script language='javascript'>
		
			function openHelp(thisHelp) {
			
				var UserWidth = 0, UserHeight = 0;
				if( typeof( parent.window.innerWidth ) == 'number' ) {
					//Non-IE
					UserWidth = parent.window.innerWidth;
					UserHeight = parent.window.innerHeight;
				} else if( parent.document.documentElement && ( parent.document.documentElement.clientWidth || parent.document.documentElement.clientHeight ) ) {
					//IE 6+ in 'standards compliant mode'
					UserWidth = parent.document.documentElement.clientWidth;
					UserHeight = parent.document.documentElement.clientHeight;
				} else if( parent.document.body && ( parent.document.body.clientWidth || parent.document.body.clientHeight ) ) {
					//IE 4 compatible
					UserWidth = parent.document.body.clientWidth;
					UserHeight = parent.document.body.clientHeight;
				}
		
				var ScrollTop = document.body.scrollTop;
				if (ScrollTop == 0) {
					if (window.pageYOffset) {
						ScrollTop = window.pageYOffset;
					} else {
						ScrollTop = (document.body.parentElement) ? document.body.parentElement.scrollTop : 0;
					}
				}
		
				document.getElementById("help_iframe").src = "resources/help.php?m=" + thisHelp;
				document.getElementById("help_popup").style.left = ((UserWidth / 2) - 362) + "px";
				document.getElementById("help_popup").style.top = (ScrollTop + 75) + "px";
				document.getElementById("help_popup").style.display = 'inline';
			}
			
			function hideHelp() {
				document.getElementById("help_popup").style.display = 'none';
			}

	</script>
	
</head>

<body style="margin:0px; background: URL(images/bg.jpg); background-repeat:no-repeat; background-position:top center; background-color:#000000">

 <!-- sticky notification -->
<!--
  <div id="notify">POS Lavu Client 2.0.4 available in the app store.&nbsp;&nbsp;<a href="http://itunes.apple.com/us/app/poslavu-client/id391800399?mt=8">Upgrade Now</a></div>
-->

  <!--<div id="notify">Attention Lavu Localserver users : Please DO NOT upgrade your Localservers to the latest Mac OS X Mountain Lion at this time.</div>-->
  
	<div id='help_popup' style='position:absolute; left:0px; top:0px; z-index:200; display:none;'>
		<table cellspacing='0' cellpadding='0'>
			<tr>
				<td style='background:URL(images/popup_bg.png); width:724px; height:471px' align='left' valign='top'>
					<table cellspacing='0' cellpadding='0'>
						<tr><td colspan='3' height='22px'>&nbsp;</td></tr>
						<tr>
							<td width='21px'>&nbsp;</td>
							<td height='20px' align='right' style='background:URL(images/popup_bar.png);'><a onclick='hideHelp();' style='cursor:pointer; color:#EEEEEE;'><b>X</b></a> &nbsp; </td>
							<td width='25px'>&nbsp;</td>
						</tr>
						<tr>
							<td width='21px'>&nbsp;</td>
							<td align='left'><div id='iframe_scroller' style='width:679px; height:405px; overflow:hidden; margin:0px 0px 0px 0px; padding:0px 0px 0px 0px;'><iframe id='help_iframe' width='675px' height='401px' frameborder='1' scrolling='yes' src='' style='width:675px; height:401px;'></iframe></div></td>
							<td width='25px'>&nbsp;</td>
						</tr>
					</table>
				</td>
			</tr>
		</table>
	</div>
	
	<center>
		<table>
			<tr><td align="center"><img src="images/poslavu_logo_sm.png" border="0" /></td></tr>
			<tr>
				<td>
					<table cellspacing=0 cellpadding=0>
						<tr>
							<td style="background: URL(images/panel_top_left.png); background-position:bottom right; background-repeat:no-repeat;" width="15" height="79">&nbsp;</td>
							<td style="background: URL(images/panel_top_mid.png); background-position:bottom; background-repeat:repeat-x" width="900" height="79">
								[content_status]
							</td>
							<td style="background: URL(images/panel_top_right.png); background-position:bottom left; background-repeat:no-repeat;" width="15" height="79">&nbsp;</td>
						</tr>
						<tr>
							<td style="background: URL(images/panel_mid_left.png); background-position:right; background-repeat:repeat-y;" width="15">&nbsp;</td>
							<td background="images/panel_mid_mid.png" height="400" align="center" valign="top">
								<table cellspacing=0 cellpadding=12>
									<tr>
										<td align='center'>
											<div style="position:relative" id="main_content_area">
												[content]
											</div>
										</td>
									</tr>
								</table>
							</td>
							<td style="background: URL(images/panel_mid_right.png); background-position:left; background-repeat:repeat-y;" width="15">&nbsp;</td>
						</tr>
						<tr>
							<td style="background: URL(images/panel_btm_left.png); background-position:top right; background-repeat:repeat-y;" width="15">&nbsp;</td>
							<td style="background: URL(images/panel_btm_mid.png); background-position:top; background-repeat:repeat-x;" height="30">&nbsp;</td>
							<td style="background: URL(images/panel_btm_right.png); background-position:top left; background-repeat:repeat-y;" width="15">&nbsp;</td>
						</tr>
					</table>
				</td>
			</tr>
		</table>

	</center>
	<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.6.2/jquery.min.js"></script>
	<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.14/jquery-ui.min.js"></script>
	<script src="scripts/jquery.cookie.js" type="text/javascript" charset="utf-8"></script>
	<script src="scripts/jquery.howdydo-bar.js" type="text/javascript" charset="utf-8"></script>

	<script src="scripts/iphone-style-checkboxes.js" type="text/javascript" charset="utf-8"></script>

	<script type="text/javascript" charset="utf-8">
		$(document).ready( function(){

			//$('#notify').hide();
			
			/*if ( $.cookie('isLoggedIn') != null && $.cookie('isLoggedIn') == 1 ) {
				
				$('#notify').howdyDo({
					action		: 'hover',
					effect		: 'slide',
					delay		: 2000,
					//hideAfter	: 10000,
					keepState	: true,
					closeAnchor	: '<img src="images/close-16x16.png" border="0" />',
					openAnchor	: '<img src="images/show-bar.png" border="0" />'
				});
				
			}*/

			//console.log($.cookie('isLoggedIn'));

			var onchange_checkbox = ($('.on_off :checkbox')).iphoneStyle({
				
				checkedLabel: 'Yes', 
				uncheckedLabel: 'No',
				
				onChange: function(elem, value) { 
				
					if ( value === true ) {
					
						var ltg_link = "<a href='https://admin.poslavu.com/cp/index.php?mode=lavu_togo'>https://admin.poslavu.com/cp/index.php?mode=lavu_togo</a>";
						
						$('td#status').html("<br />Please enable & configure your online ordering menu here: "+ltg_link).show();
						
					} else {
					
						$('td#status').hide();
					}
				
				  
				  //console.log(value);
				}
			});

			
					
		});
	</script>
</body>