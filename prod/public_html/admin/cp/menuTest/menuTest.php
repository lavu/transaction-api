<?php

	ini_set("display_errors",1);

	
	$menuSub= array( new linkObj("edit_menu","Menu Items"),
					 new linkObj("edit_optional_modifiers","Optional Modifiers"),
					 new linkObj("edit_forced_modifiers","Forced Modifiers"),
					 new linkObj("edit_modifier_groups","Modifier Groups"),
					 new linkObj("edit_super_groups","super Groups"));


	$settingsSub=array(
		new linkObj("company_settings","Company Settings"),
		new linkObj("location_settings","Location Settings"),
		new linkObj("advanced_location_settings","Advanced Location Settings"),
		new linkObj("payment_methods","Payment Methods"),
		new linkObj("tax_rates","Tax Rates"),
		new linkObj("tax_exemptions","Tax Exemptions"),
		new linkObj("discount_types","Discount Types"),
		new linkObj("discount_groups","Discount Groups"),
		new linkObj("quickpay_buttons","Quickpay Buttons"),
		new linkObj("quicktip_buttons","Quicktip Buttons"),
		new linkObj("devices","Devices"),
		new linkObj("printers_kds","Printers KDS"),
		new linkObj("printer_groups","Prnter Groups"),
		new linkObj("sync_status","Sync Status"),
		new linkObj("custom_language","Custom Language"),
		new linkObj("customer_accounts","Customer Accounts"),
		new linkObj("overtime_settings","Overtime Settings"),
		new linkObj("edit_customer_fields","Edit Customer Fields")

	);
	$generalSub=array(
		new linkObj("end_of_day","End Of Day"),
		new linkObj("time_cards","Time Cards"),
		new linkObj("manage_orders","Manage Orders"),
		
	);
	$reportsSub=array(
		new linkObj("general","General",null, $generalSub),
		new linkObj("payment_reports","Payment Reports"),
		new linkObj("sales_by","Sales By"),
		new linkObj("product_reports","Product Reports"),
		new linkObj("activity_reports","Activity Reports"),
		new linkObj("special","Special")

	);
	
	$rootLinkChildren= array(
		new linkObj('lavu','', 'images/iconHome.png' ,null),
		new linkObj('menu_menu', 'menu', 'images/menu.png', $menuSub),
		new linkObj('edit_ingredients', 'inventory','images/inventory.png',null),
		new linkObj('table_setup', 'layout', 'images/layout.png',null),
		new linkObj('settings', 'settings','images/settings.png', $settingsSub),
		new linkObj('reports_menu', 'reports', 'images/reports.png',$reportsSub),
		new linkObj('tutorials', 'assistance', 'images/assistance.png',null),
		new linkObj('extend', 'extend', 'images/iconExtend.png',null),
		new linkObj('home', '', 'images/iconHouse.png',null)
	);

	$rootLink= new linkObj(null,null,null,$rootLinkChildren);
/*

	$link8Children=array(

	new linkObj("eight_a_dot_com", "eight_a", ''),
	new linkObj("eight_b_dot_com", "eight_b", ''),
	new linkObj("eight_c_dot_com", "eight_c", '') 

	);
	$link8 = new linkObj("eight_dot_com",  "eight", '', $link8Children);
	$link7Children=array(
	
		new linkObj("seven_a_dot_com", "seven_a", ''),
		new linkObj("seven_b_dot_com", "seven_b", ''), 
		$link8
	
	);
	$link7 = new linkObj("seven_dot_com",  "seven", '', $link7Children);
	$link6 = new linkObj("six_dot_com", "six",  '',array($link7));
	
	$link5 = new linkObj("five_dot_com",  "five", ''         );
	$link4 = new linkObj("four_dot_com",  "four", '',  array($link5));
	$link3 = new linkObj("three_dot_com", "three",'', array($link4));
	$link2 = new linkObj("two_dot_com",   "two",  '', array($link3));
	
	$oneAChildren=array(

	new linkObj("one_a_a_dot_com", "one_a_a", ''),
	new linkObj("one_a_b_dot_com", "one_a_b", ''),
	new linkObj("one_a_c_dot_com", "one_a_c", '') 

	);
	
	$link1Children=array(
		new linkObj("one_a_dot_com", "one_a",'', $oneAChildren),
		new linkObj("one_b_dot_com", "one_b",''),
		new linkObj("one_c_dot_com", "one_c",''), 
		new linkObj("one_d_dot_com", "one_d",''),
		new linkObj("one_d_dot_com", "one_e",''),
		new linkObj("one_d_dot_com", "one_f",''),
		new linkObj("one_d_dot_com", "one_g",''),
		new linkObj("one_d_dot_com", "one_h",''),
	);
	$link1 = new linkObj("one_dot_com", "one", '',$link1Children);
	
	$rootLinkChildren= array($link1, $link2, $link6);
	
	$rootLink = new linkObj(null,null, null,$rootLinkChildren);
	//echo $link2->getChildren();

*/
/*
$selected= $_GET['selected'];
$subMenuSelected= $_GET['subMenuSelected'];
//echo "selected is: ". $selected;

//$child= new linkObj('asdf', 'asdf','');
$subMenuChildren1= array();
for($x=0; $x<15; $x++){
	array_push($subMenuChildren1, new linkObj($x, "link".$x, ''));	
}
$subMenuChildren2= array();
for($x=16; $x<20; $x++){
	array_push($subMenuChildren2, new linkObj($x, "link".$x, ''));	
}
$subMenuChildren3= array();
for($x=21; $x<45; $x++){
	array_push($subMenuChildren3, new linkObj($x, "link".$x, ''));	
}
$subMenuChildren4= array();
for($x=46; $x<70; $x++){
	array_push($subMenuChildren4, new linkObj($x, "link".$x, ''));	
}

$subMenuChildren5= array();
for($x=71; $x<73; $x++){
	array_push($subMenuChildren5, new linkObj($x, "link".$x, ''));	
}
$subMenu= new linkObj(null,null,null,$subMenuLinkChildren, true);

$rootLinkChildren= array(

	new linkObj('lavu','', 'iconHome.png', null ),
	new linkObj('menu', 'menu', 'menu.png',$subMenuChildren1),
	new linkObj('inventory', 'inventory','inventory.png',$subMenuChildren2),
	new linkObj('layout', 'layout', 'layout.png',$subMenuChildren3),
	new linkObj('settings', 'settings','settings.png',$subMenuChildren4),
	new linkObj('reports', 'reports', 'reports.png',$subMenuChildren5),
	new linkObj('assistance', 'assistance', 'assistance.png', null),
	new linkObj('extend', 'extend', 'iconExtend.png',null),
	new linkObj('home', '', 'iconHouse.png',null)
);


$rootLink= new linkObj(null,null,null,$rootLinkChildren);
*/


?>
<html>
	<head>
		<title>
			New Nav Menu Test
		</title>
		
		<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"> </script>
		<!--
		<script type="text/javascript" src="menuTest.js"> </script>
		<link rel="stylesheet" type="text/css" href="menuTest.css">
		-->
		<link rel="stylesheet" type="text/css" href="menuTestNew.css">
	</head>
	<body>

		<nav>
		<?php echo $rootLink->__toString();?>
		</nav>
			
	</body>
</html>

<?php
class linkObj{

	private $link='';	
	private $title='';
	private $children=array();
	private $image='';
	private $isSubMenu;
	private $parentVal=''; 
	function __construct($link = null, $title = null, $image = null, $children = null){

		if(is_array($children))
			foreach($children as $child){
				$child->setParent($link);
			}
		$this->setChildren($children);
		$this->setTitle($title);
		$this->setLink($link);
		$this->setImage($image);

	}
	public function getLink(){
		return $this->link;
	}
	private function setLink($linkIn){
		$this->link= $linkIn;
	}
	public function getTitle(){
		return $this->title;
	}
	private function setTitle($title){
		$this->title= $title;
	}
	private function setChildren($array){
		$this-> children= $array;
	}
	private function setParent($parent){
		$this->parentVal= $parent;
	}
	public function getChildren(){
		return $this->children;
	}
	public function getParent(){
		return $this-> parentVal;
	}
	private function setImage($image){
		$this->image= $image;
	}
	public function getImage(){
		return $this->image;
	}
	
	public function addChild(linkObj $child){
		if( is_array($this->children) && $child){
			array_push($this->children, $child);
		}
		else 	
			return false;
	}
		
	public function __toString(){
		return $this->buildList();			
	}
	private function buildList(){
	
		$returnString='';
		
		if( $this->link ){
			
			$returnString.="<li><a class='navLink' href='?mode=".$this->link."'><img src='".$this->image."'><br/>".$this->title."</a>";
		}
		if(is_array($this->children) && count($this->children)){
			if($this->link)
				$returnString .="<ul id='".$this->link."'> ";
			else
				$returnString .="<ul></li> ";
		
			foreach($this->children as $child){
				$returnString.=$child->buildList();
			}
			$returnString .="</ul>";
		}
		
		return $returnString;	
	}

}



?>