<?php
	$filePath = dirname(__FILE__);
	require_once $filePath .'/area_report_drawer.php';
	require_once $filePath .'/area_store.php';
	require_once $filePath ."/report_joins.php";
	require_once $filePath .'/sister_report.php';
	require_once $filePath .'/../areas/currency_util.php';
	require_once $filePath . "/../../lib/paginator.class.php";
	require_once $filePath .'/constants/reportConstants.php';
	require_once $filePath .'/emailReports.php';

	if (isset($_GET['email']) && $_GET['email'] != '' && isset($_GET['output_mode']) && $_GET['output_mode'] == 'csv')
	{
	    //set settings
	    ini_set('memory_limit', '-1');
	    set_time_limit(0);
	    ignore_user_abort(true);
	    //
	}

	class area_reports extends area_foundation
	{
		var $report_title;
		var $report_name;
		var $load_report;
		var $report_id;
		var $report_read;
		var $report_selection;
		var $report_selection_column;
		var $report_container;
		var $output_mode;
		var $click_to_page;
		var $base_url;
		var $display_query;

		var $allow_controls;
		var $autoselect_date;
		var $multiorder;

		var $firstcol;
		var $firstcol_name;
		var $groupby_cols;
		var $exec_drilldown;
		var $drillvars;
		var $sisters;
		var $rj;
		var $report_drawer;
		var $drill_dropdown;
		var $prepareQueriesForEmailReport;
		var $queries;
		var $dates;
		var $paginationSet = false;

		public function __construct($set_args)
		{
		    $_SESSION['drilldown']=0;
		    $set_report = $this->argval($set_args,"report","");
			
			if(isset($_REQUEST['alternate_report']))
			{
				$_SESSION['alternate_report_'.$set_report] = $_REQUEST['alternate_report'];
			}
			if(isset($_SESSION['alternate_report_'.$set_report]))
			{
				$this->load_report = $_SESSION['alternate_report_'.$set_report];
			}
			else
			{
				$this->load_report = $set_report;
			}
			
			//LP-5000 changes

			$this->base_url = decode_spelled_url($this->argval($set_args,"base_url",""));
			$this->allow_controls = $this->argval_bool($set_args,"allow_controls",true);
			$this->autoselect_date = $this->argval($set_args,"autoselect_date","");
			$this->multiorder = $this->argval($set_args,"multiorder","");
			$this->display_query = $this->argval($set_args,"display_query","");
			$this->output_mode = $this->argval($set_args,"output_mode","");

			$this->report_container = "report_container";//_" . $set_report;
			$this->click_to_page = "click_to_page";//_" . $set_report;
			$this->sisters = array();
			$this->init_drillvars();
			$this->v2report_name = '';

			if (isset($_GET['report'])) {
				$report_query = mlavu_query("SELECT `name` FROM `reports_v2` WHERE `id`='[1]'", $_GET['report']);
				if (mysqli_num_rows($report_query) > 0) {
					$report_read = mysqli_fetch_assoc($report_query);
					$this->v2report_name = $report_read['name'];
				}
			}

			if(is_numeric($this->load_report))
			{
				if(@$_REQUEST['sister_select'] == "#cash_sales"){
					$order_info = "alt_saleinfo";
					$report_query = mlavu_query("select * from `reports_v2` where `name`='[1]'",$order_info);
					if(mysqli_num_rows($report_query))
					{
						$current_report_read = mysqli_fetch_assoc($report_query);
					}
				}
				else if(@$_REQUEST['sister_select'] == "#order_time"){
					$order_info = "alt_orderinfo";
					$report_query = mlavu_query("select * from `reports_v2` where `name`='[1]'",$order_info);
					if(mysqli_num_rows($report_query))
					{
						$current_report_read = mysqli_fetch_assoc($report_query);
					}
				}
				else if(@$_REQUEST['sister_select'] == "#kiosk_order") {
					$order_info = "kioskOrders";
					$report_query = mlavu_query("select * from `reports_v2` where `name`='[1]'",$order_info);
					if(mysqli_num_rows($report_query)) {
						$current_report_read = mysqli_fetch_assoc($report_query);
					}
				}
				else if(@$_REQUEST['sister_select'] == "#lavu_togo_order") {
				    $order_info = "lavuToGoOrders";
				    $report_query = mlavu_query("select * from `reports_v2` where `name`='[1]'",$order_info);
				    if(mysqli_num_rows($report_query)) {
				        $current_report_read = mysqli_fetch_assoc($report_query);
				    }
				}
				else if(@$_REQUEST['sister_select'] == "#overpaid_order"){ // LP-5320 changes add
					$order_info = "overpaidOrders";
					$report_query = mlavu_query("select * from `reports_v2` where `name`='[1]'",$order_info);
					if(mysqli_num_rows($report_query))
					{
						$current_report_read = mysqli_fetch_assoc($report_query);
					}
				}
				else if(@$_REQUEST['sister_select'] == "#underpaid_order"){ // LP-5321 changes add
					$order_info = "underpaidOrders";
					$report_query = mlavu_query("select * from `reports_v2` where `name`='[1]'",$order_info);
					if(mysqli_num_rows($report_query))
					{
						$current_report_read = mysqli_fetch_assoc($report_query);
					}
				}else{
					$report_query = rpt_query("select * from `reports` where `id`='[1]'",$this->load_report);
					if(mysqli_num_rows($report_query))
					{
						$current_report_read = mysqli_fetch_assoc($report_query);
					}
				}
			}

			if(!isset($current_report_read))
			{

				if(@$_REQUEST['sister_select'] == "#cash_sales"){
					$order_info = "alt_saleinfo";
					$report_query = mlavu_query("select * from `reports_v2` where `name`='[1]'",$order_info);
					if(mysqli_num_rows($report_query))
					{
						$current_report_read = mysqli_fetch_assoc($report_query);
					}
				}
				else if(@$_REQUEST['sister_select'] == "#order_time"){
					$order_info = "alt_orderinfo";
					$report_query = mlavu_query("select * from `reports_v2` where `name`='[1]'",$order_info);
					if(mysqli_num_rows($report_query))
					{
						$current_report_read = mysqli_fetch_assoc($report_query);
					}
				}
				else if(@$_REQUEST['sister_select'] == "#kiosk_order") {
					$order_info = "kioskOrders";
					$report_query = mlavu_query("select * from `reports_v2` where `name`='[1]'",$order_info);
					if(mysqli_num_rows($report_query)) {
						$current_report_read = mysqli_fetch_assoc($report_query);
					}
				}
				else if(@$_REQUEST['sister_select'] == "#lavu_togo_order") {
				    $order_info = "lavuToGoOrders";
				    $report_query = mlavu_query("select * from `reports_v2` where `name`='[1]'",$order_info);
				    if(mysqli_num_rows($report_query)) {
				        $current_report_read = mysqli_fetch_assoc($report_query);
				    }
				}
				else if(@$_REQUEST['sister_select'] == "#overpaid_order"){// LP-5320 changes add
					$order_info = "overpaidOrders";
					$report_query = mlavu_query("select * from `reports_v2` where `name`='[1]'",$order_info);
					if(mysqli_num_rows($report_query))
					{
						$current_report_read = mysqli_fetch_assoc($report_query);
					}
				}
				else if(@$_REQUEST['sister_select'] == "#underpaid_order"){// LP-5321 changes add
					$order_info = "underpaidOrders";
					$report_query = mlavu_query("select * from `reports_v2` where `name`='[1]'",$order_info);
					if(mysqli_num_rows($report_query))
					{
						$current_report_read = mysqli_fetch_assoc($report_query);
					}
				}else{
					$report_query = rpt_query("select * from `reports` where `name` LIKE '[1]'",$this->load_report);
					if(mysqli_num_rows($report_query))
					{
						$current_report_read = mysqli_fetch_assoc($report_query);
					}
				}
			}
			
			if($this->load_report != $set_report)
			{
				if(is_numeric($set_report))
				{
					$main_report_query = rpt_query("select * from `reports` where `id`='[1]'",$set_report);
					if(mysqli_num_rows($main_report_query))
					{
						$main_report_read = mysqli_fetch_assoc($main_report_query);
					}
				}
				if(!isset($main_report_read))
				{
					$main_report_query = rpt_query("select * from `reports` where `name` LIKE '[1]'",$set_report);
					if(mysqli_num_rows($main_report_query))
					{
						$main_report_read = mysqli_fetch_assoc($main_report_query);
					}
				}
			}

			if(isset($current_report_read)) {
			    $this->report_read = $current_report_read;
				$this->report_id = $this->report_read['id'];
				$this->report_name = $this->report_read['name'];

				//Adding report name into array
				$reportNotShowingDateRange = array('inventory_lowinventory');
				
				if(isset($main_report_read)) {
					$this->report_id = $main_report_read['id'];
				}
				
				$this->report_title = $this->report_read['title'];
				//We are intentionally hiding date range for some report which is exist in $reportNotShowingDateRange list
				$this->report_selection = (in_array($this->report_read['name'], $reportNotShowingDateRange) ) ? '' : $this->report_read['selection'];
				$this->report_selection_column = $this->report_read['selection_column'];
			}
			$reportId = $set_args['report'];
			$this->drill_dropdown = $this->getDrillDropDown($reportId);
			
			$this->prepareQueriesForEmailReport = $this->argval($set_args, "output_mode", "") === OUTPUT_MODE_CSV_TO_EMAIL && emailReports::isReportAllowedToEmail($this->report_name);
			$this->queries = [];
		}

		public function init_drillvars()
		{
			$this->exec_drilldown = false;
			if(isset($_GET['drill_title']))
			{
				$this->drillvars = array();
				$this->fwd_drillvars = array();
				$drillvar_keys = array("title","subreport","title_col","filter_cols","filter_vals");
				$drillvar_key_aliases = array();
				$drillvar_key_aliases['title'] = "drill_title";
				$drillvar_key_aliases['subreport'] = "drill_down";

				for($n=0; $n<count($drillvar_keys); $n++)
				{
					$drillkey = $drillvar_keys[$n];
					if(isset($drillvar_key_aliases[$drillkey]))
						$var_init_value = urlvar($drillvar_key_aliases[$drillkey]);
					else
						$var_init_value = urlvar($drillkey);
					$this->fwd_drillvars[$drillkey] = $var_init_value;
					$var_parts = explode("[_dsep_]",$var_init_value);
					for($m=0; $m<count($var_parts); $m++)
					{
						$set_var_value = $var_parts[$m];
						if(substr($drillkey,0,7)=="filter_") $set_var_value = explode("(col)",$set_var_value);
						if(!isset($this->drillvars[$m])) $drillvars[$m] = array();
						$this->drillvars[$m][$drillkey] = $set_var_value;
					}
				}
				$this->exec_drilldown = true; // drillvars: title,title_col,subreport,filter_cols,filter_vals
			}
		}

		public function drillvar_query_string( $n = null )
		{
			$str = "";
			if(isset($this->drillvars) && isset($this->drillvars[0]) )
			{
				$titles = $this->fwd_drillvars['title'];
				$drill_downs = $this->fwd_drillvars['subreport'];
				$title_cols = $this->fwd_drillvars['title_col'];
				$filter_cols = $this->fwd_drillvars['filter_cols'];
				$filter_vals = $this->fwd_drillvars['filter_vals'];
				if ($n !== null && $n >= 0 ){ // go from 0 to $n
					$titles 		= explode('[_dsep_]', $this->fwd_drillvars['title']);
					$drill_downs 	= explode('[_dsep_]', $this->fwd_drillvars['subreport']);
					$title_cols 	= explode('[_dsep_]', $this->fwd_drillvars['title_col']);
					$filter_cols 	= explode('[_dsep_]', $this->fwd_drillvars['filter_cols']);
					$filter_vals 	= explode('[_dsep_]', $this->fwd_drillvars['filter_vals']);

					$titles = array_slice( $titles, 0, $n+1 );
					$drill_downs = array_slice( $drill_downs, 0, $n+1 );
					$title_cols = array_slice( $title_cols, 0, $n+1 );
					$filter_cols = array_slice( $filter_cols, 0, $n+1 );
					$filter_vals = array_slice( $filter_vals, 0, $n+1 );

					$titles 		= implode('[_dsep_]', $titles);
					$drill_downs 	= implode('[_dsep_]', $drill_downs);
					$title_cols 	= implode('[_dsep_]', $title_cols);
					$filter_cols 	= implode('[_dsep_]', $filter_cols);
					$filter_vals 	= implode('[_dsep_]', $filter_vals);

				}

				if( $n >= 0 || $n === null ){
					$str .= "drill_title=" . 				urlencode( $titles );
					$str .= "&drill_down=" . 				urlencode( $drill_downs );
					$str .= "&title_col=" . 				urlencode( $title_cols );
					$str .= "&filter_cols=" . 				urlencode( $filter_cols );
					$str .= "&filter_vals=" . 				urlencode( $filter_vals );
				}
			}

			$sister_links = '';
			$sister_select = urlencode(reqvar('sister_select',''));
			$sister_where = urlencode(reqvar('sister_where',''));
			$sister_groupby = urlencode(reqvar('sister_groupby',''));

			$sister_links .= ($sister_select)?'&sister_select='.$sister_select:'';
			$sister_links .= ($sister_where)?'&sister_where='.$sister_where:'';
			$sister_links .= ($sister_groupby)?'&sister_groupby='.$sister_groupby:'';

			if( $str ){
				$str .= '&';
			}
			$str .= $sister_links;

			$multi_report = reqvar('multi_report','');
			$sep_loc = reqvar('sep_loc','');

			if( $multi_report != '' ){
				if( $str ){
					$str .= '&';
				}
				$str .= 'multi_report=' . urlencode($multi_report);
			}

			if( $sep_loc != '' ){
				if( $str ){
					$str .= '&';
				}
				$str .= 'sep_loc=' . urlencode($sep_loc);
			}
			return $str;
		}

		public function is_field_currently_drilled( $fieldName, $index ){
			if( ! $this->drillvars ){
				return false;
			}
			$result = $index === 0;
			for( $i = 0; $i < count($this->drillvars)-1; $i++ ){
				$drillvar = $this->drillvars[$i];
				if( '@' === $drillvar['subreport'][0] ){
					$alias = report_alias::alias( substr( $drillvar['subreport'], 1 ) );
					$result = $result || ($fieldName == $alias->label());
				} else {
					$result = $result || strstr($drillvar['title_col'], $fieldName );
				}
			}
			return $result;
		}

		public function process_query_command($cmd,$cmd_template,$query)
		{
			$cmd_str = $cmd . '(';
			$cmd_str_len = strlen( $cmd_str );
			$cmd_str_starts = stripos($query, $cmd_str );

			if( $cmd_str_starts === FALSE ){
				return $query;
			}

			$parens_level = 0;
			$i = 0;

			for( $i=$cmd_str_starts+$cmd_str_len;$i<strlen($query);$i++ ){
				if( '(' === $query[$i] ){
					$parens_level++;
				}

				if( ')' === $query[$i] ){
					if( 0 === $parens_level ){
						break;
					} else {
						$parens_level--;
					}
				}
			}
			return str_ireplace('[val]', substr( $query, $cmd_str_starts+$cmd_str_len, $i-($cmd_str_starts+$cmd_str_len)), $cmd_template) . substr( $query , $i + 1);
		}

		public function parse_groupby_statement( $groupby_str ){
			$groupby_str = trim(str_ireplace("group by ", "", $groupby_str));
			$result = array();
			if( empty( $groupby_str ) ){
				return $result;
			}
			$i = 0;
			$parens_level = 0;
			$part = "";
			while( $i < strlen($groupby_str) ){
				if( $groupby_str[$i] == '(' ){
					$parens_level++;
				} else if( $groupby_str[$i] == ')' ){
					$parens_level--;
				}

				if( $parens_level === 0 ){
					if( $groupby_str[$i] == ',' ){
						$result[] = trim($part);
						$part = '';
						$i++;
						continue;
					}
				}

				$part .= $groupby_str[$i];
				$i++;
			}
			if( !empty($part) && $part != ''){
				$result[] = trim($part);
			}

			return $result;
		}

		public function parse_groupby_cols($groupby_str, $groupbys=null)
		{
			$groups = $this->parse_groupby_statement( $groupby_str );
			$result = array();
			foreach( $groups as $index => $group ){
				if(strstr($group,"date_format(") !== FALSE){
					$result[] = $group;
				} else if(strstr($group,"substr(") !== FALSE){
					$result[] = $group;
				} else if(strstr($group,"concat(") !== FALSE){
					$cparts = explode(",",$groupby_str);
					for($n=0; $n<count($cparts); $n++)
					{
						if( $count = preg_match("/(\\w+\\.\\w+)/", $cparts[$n], $a_matches) )
						{
							$result[] = $a_matches[1];
						}
					}
				} else if( $count = preg_match("/(\\w+\\.\\w+)/", $groupby_str, $a_matches) ){
					
					$result[] = $a_matches[1];
				} else {
					$result[] = $group;
				}
			}

			return $result;


			if( $count = preg_match_all("/(\\w+\\.\\w+)/", $groupby_str, $a_matches) ){

				$result = array();
				for( $i = 0; $i < $count; $i++ ){
					$result[] = $a_matches[1][$i];
				}
				return $result;
			}
			if(strstr($groupby_str,"date_format(")) return array(str_replace("group by ","",$groupby_str));
			// group by date_format(orders.closed, '%h:00 %p')

			$glist = array();
			$gparts = explode("group by ",$groupby_str);
			if(count($gparts) > 1)
			{
				$gparts = str_replace("concat(","(",$gparts[1]);
				$gparts = explode(",",$gparts);
				for($n=0; $n<count($gparts); $n++)
				{
					$ginner = $gparts[$n];
					$ginner = str_replace("(","",$ginner);
					$ginner = str_replace(")","",$ginner);
					$ginner = trim($ginner);
					if(trim(str_replace(".","",$ginner))!="" && is_alpha_numeric(str_replace(".","",$ginner)))
					{
						$glist[] = $ginner;
					}
				}
			}
			return $glist;
		}

		public function parse_tablecol_for_mysql($str,$maxcount=2)
		{
			if(strstr($str,"date_format(") && $maxcount==2) return str_replace("group by ","",$str);
			else if(strstr($str,"date_format(") && $maxcount==1) return "date_formatted";
			else if(strstr($str,"substr(") && $maxcount==2) return $str;
			else if(strstr($str,"substr(") && $maxcount==1) return "sub_stringed";

			$str_parts = explode(".",$str);
			if(count($str_parts) > 1)
			{
				$str = "";
				$str .= "`" . ConnectionHub::getConn('rest')->escapeString(str_replace("`","",$str_parts[0])) . "`.";
				$str .= "`" . ConnectionHub::getConn('rest')->escapeString(str_replace("`","",$str_parts[1])) . "`";
				if($maxcount==1)
				{
					$str = str_replace("`","",str_replace(".","_",$str));
				}
				return $str;
			}
			else
			{
				return "`" . ConnectionHub::getConn('rest')->escapeString(str_replace("`","",$str)) . "`";
			}
		}

		public function show_reported_value($val,&$report_read=false,$key="",$monetary_symbol="")
		{
			$rstr = $this->process_merge_commands($val,"display",$report_read,$key,$monetary_symbol);
			$rstr = str_replace("\n","<br>",$rstr);
			$rstr = str_replace("\\n","<br>",$rstr);

			return custom_display_value( $key, $rstr );
		}

		public function allow_drilldown($filters_available=1)
		{
			$filters_used = ((isset($this->drillvars))?count($this->drillvars):0);
			if($filters_available > $filters_used) return true;
			else return false;
		}

		public function verify_rj_loaded($isEnable=false)
		{
			if ($isEnable) {
				//Get All Dates
				global $start_date;
				global $start_time;
				global $end_date;
				global $end_time;
				global $filtermode;
				$selector_result = $this->datetime_selector();
				$selector_vars = $selector_result['vars'];
				$start_datetime = $selector_vars['start_datetime'];
				$end_datetime = $selector_vars['end_datetime'];
				$filtermode = $selector_result['mode'];
				$start_datetime_new = explode(" ", $start_datetime);
				$end_datetime_new = explode(" ", $end_datetime);
				$start_date = $start_datetime_new[0];
				$start_time = $start_datetime_new[1];
				$end_date = $end_datetime_new[0];
				$end_time = $end_datetime_new[1];
			}
			if(!isset($this->rj))
			{
				$this->rj = new report_joins($this->report_read);
			}
		}

		public function process_merge_commands($str,$mode="query",&$report_read=false,$key="",$monetary_symbol="")
		{
			if($mode=="query")
			{
				if ($locationInfo['cash_tips_as_daily_total'] != 1 && $this->report_id == 30) {
					$str = $this->process_query_command("perOrderTotalTips", "if(count(*) > 0, [val], '[money]0.00') as total_tip", $str);
				} else {
					$str = $this->process_query_command("sum", "sum([val])", $str);
				}
				$str = $this->process_query_command("sum_price","if(count(*) > 0, ".report_query_money_concat_code( 'sum([val])' ).", '[money]0.00')",$str);
				$str = $this->process_query_command("price","if(([val]) > 0, ".report_query_money_concat_code( '[val]' ).", '[money]0.00')",$str);
				$str = $this->process_query_command("sum_rounded","round(sum([val]))",$str);
				$str = $this->process_query_command("sum_float","sum([val])",$str);
				$str = $this->process_query_command("order_button","concat('[merge_order_button]',`order_id`,':',[val])",$str);
				$str = $this->process_query_command("title","concat('title:',[val])",$str);
				$str = $this->process_query_command("drill_down","concat('[drill_down]',[val])",$str);
			}
			else if($mode=="display")
			{
				if($key!="")
				{
					$alkey = "#" . $key;
					$this->verify_rj_loaded();
					$set_alkey = $this->rj->apply_aliases_to_string($alkey);
					if($set_alkey!=$alkey) {
						$str = str_replace("[value]",htmlspecialchars($str),$set_alkey);
					}
				}

				if(strpos($str,"[merge_order_button]")!==false)
				{
					$strval = explode(":",str_replace("[merge_order_button]","",$str));
					$strorder_id = $strval[0];
					$strfolder = $strval[1];
					if(count($strval) > 2)
						$strtitle = htmlspecialchars($strval[2]);
					else
						$strtitle = htmlspecialchars("#" . $strorder_id);
					$str = "<input type='button' value='{$strtitle}' onclick='window.location = \"/cp/review_events/$strorder_id/$strfolder/cart/\"' />";
				}

				$drill_down = false;
				if(strpos($str,"[drill_down]")!==false)
				{
					$drill_down = true;
					$strval = explode("[drill_down]",$str);
					if(count($strval) > 1)
					{
						$set_drill_title = '';
						if(isset($this->fwd_drillvars['title']) && $this->fwd_drillvars['title']!="")
						{
							$last_drillvar_subreport = trim($this->drillvars[count($this->drillvars) - 1]['subreport']);
							if( strlen( $last_drillvar_subreport ) && '@' == $last_drillvar_subreport[0] ){
								$this->verify_rj_loaded(true);
								$this->rj->verify_aliases_are_loaded();
								$alias = report_alias::alias( substr( $last_drillvar_subreport, 1 ) );
								$last_drillvar_subreport = $alias->statement();
							}
							$set_title_col = $last_drillvar_subreport;
							$group_col_name = "group_col_" . str_replace(".","_",$set_title_col);
							if(isset($report_read[0][$group_col_name]))
							{
							    $set_drill_title = $report_read[0][$group_col_name];
								$this->fwd_drillvars['last_subreport'] = $group_col_name;
								$this->fwd_drillvars['last_drill_title'] = $set_drill_title;
							} else if( isset( $report_read[0]['group_col_date_formatted'])){
							    $set_drill_title = $report_read[0]['group_col_date_formatted'];
								$this->fwd_drillvars['last_subreport'] = $group_col_name;
								$this->fwd_drillvars['last_drill_title'] = $set_drill_title;
							}
						}

						$strreport_parts = explode(",",$strval[1]);
						if($this->allow_drilldown(count($strreport_parts)))
						{
							$use_firstcol = $this->firstcol_name;
							if($use_firstcol=="") $use_firstcol = $this->firstcol;
							if($report_read && isset($report_read[0][$use_firstcol]))
							{
							    $show_firstcol_value = $this->processTimeFunction( $report_read[0][$use_firstcol] );
							}
							else $show_firstcol_value = "";
							$groupstr = "";
							$filtervals = "";
							for($g=0; $g<count($this->groupby_cols); $g++)
							{
								if($g>0) $groupstr .= "(col)";
								if($g>0) $filtervals .= "(col)";
								$groupstr .= $this->groupby_cols[$g];

								$groupby_colname = $this->parse_tablecol_for_mysql($this->groupby_cols[$g]);
								$groupby_set_colname = "group_col_" . str_replace("`","",$this->parse_tablecol_for_mysql($this->groupby_cols[$g],1));
								if( strstr($groupby_colname, 'time(') !== FALSE ){
									//We don't have the exact Column name, sadly
									//
									//Try to compare time(code) in the result, to match
									if( $count = preg_match("/time\([^\)]+\)/", $groupby_colname, $a_matches ) ){
										$match_str = $a_matches[0];
										foreach( $report_read as $report_key => $read_values ){
										    foreach( $read_values as $report_read_key => $report_read_value ){
        											if( substr($report_read_value, 0, strlen($match_str)) == $match_str ){
        												$filtervals .= $report_read_value;
        												break;
        											}
										    }
										}
									} 
								} else {
									if($this->groupby_cols[$g] == "menu_items.name"){
									    $filtervals .= $report_read[0]['name'];
									}else{
										$filtervals .= $report_read[0][$groupby_set_colname];
									}
								}

							}

							$set_drill_down_start = "";
							if(isset($set_drill_title)) {
								$set_drill_title = urlencode($show_firstcol_value);
							}
							$set_title_col = urlencode( $use_firstcol);
							$set_filter_cols = urlencode( $groupstr);
							
							// LP-5242 changes start
							if($filtervals == ''){
								$set_filter_vals = $set_drill_title;
							}else{
								$set_filter_vals = urlencode( $filtervals);
							}// LP-5242 changes end

							if(isset($this->fwd_drillvars['title']))
							{
								$last_drillvar_subreport = $this->drillvars[count($this->drillvars) - 1]['subreport'];
								if( strlen( $last_drillvar_subreport ) && '@' == $last_drillvar_subreport[0] ){
									$this->verify_rj_loaded(true);
									$this->rj->verify_aliases_are_loaded();
									$alias = report_alias::alias( substr( $last_drillvar_subreport, 1 ) );
									$last_drillvar_subreport = $alias->statement();
								}
								$set_title_col = urlencode(trim($last_drillvar_subreport) );
								$group_col_name = "group_col_" . str_replace(".","_",$set_title_col);
								if(isset($report_read[$group_col_name])) {
									$set_drill_title = urlencode( $report_read[$group_col_name] );
									$this->fwd_drillvars['last_subreport'] = $group_col_name;
									$this->fwd_drillvars['last_drill_title'] = $set_drill_title;
								} else if( isset( $report_read['group_col_date_formatted'])){
									$set_drill_title = $report_read['group_col_date_formatted'];
									$set_drill_title = $this->processTimeFunction( $set_drill_title );
									$this->fwd_drillvars['last_subreport'] = 'group_col_date_formatted';
									$this->fwd_drillvars['last_drill_title'] = $set_drill_title;
								}

								$set_drill_title = urlencode( $this->fwd_drillvars['title'] ) . "[_dsep_]" . urlencode($set_drill_title);
								$set_drill_down_start = urlencode( $this->fwd_drillvars['subreport'] ) . "[_dsep_]";
								$set_title_col = urlencode($this->fwd_drillvars['title_col']) . "[_dsep_]" . $set_title_col;
								$set_filter_cols = urlencode( $this->fwd_drillvars['filter_cols'] ) . "[_dsep_]" . $set_filter_cols;
								$set_filter_vals = urlencode( $this->fwd_drillvars['filter_vals'] ) . "[_dsep_]" . $set_filter_vals;

								$include_drilldown_selector = true;
							}
							else $include_drilldown_selector = true;

							$str = "";
							if($include_drilldown_selector)
							{
								$sister_links = '';
								$sister_select = urlencode(reqvar('sister_select',''));
								$sister_where = urlencode(reqvar('sister_where',''));
								$sister_groupby = urlencode(reqvar('sister_groupby',''));

								$sister_links .= ($sister_select)?'&sister_select='.$sister_select:'';
								$sister_links .= ($sister_where)?'&sister_where='.$sister_where:'';
								$sister_links .= ($sister_groupby)?'&sister_groupby='.$sister_groupby:'';

								$multi_report_link = '';
								{
									$multi_report = reqvar('multi_report','');
									$sep_loc = reqvar('sep_loc','');

									if( $multi_report != '' ){
										$multi_report_link .= '&multi_report=' . $multi_report . '&';

										if( $sep_loc != '' ) {
											$multi_report_link .= '&sep_loc=' . $sep_loc . '&';
										}
									}
								}

								if( $sister_links ){
									$sister_links .= '&';
								}
								$set_drill_title = str_replace("#", "", $set_drill_title);
                                                                if (urldecode($sister_select) == "#super_group" && $set_drill_title == "No Super Group" && $set_filter_vals == "No Super Group") {
                                                                        $set_filter_vals = "";
                                                                }
                                                                if(strpos($set_drill_title, "'")){
                                                                        $set_drill_title = str_replace("'", "%27", $set_drill_title);
                                                                        $set_filter_vals = str_replace("'", "%27", $set_filter_vals);
                                                                }
								//LP-5000 changes
								$str = str_replace('[CLICK_URL]',"click_to_page(\"".$this->url("{$sister_links}{$multi_report_link}clicked=drill_down&mode=" . getvar("mode") . "&drill_title=" . $set_drill_title) ."&title_col=" . $set_title_col ."&filter_cols=".$set_filter_cols."&filter_vals=".$set_filter_vals."&drill_down=".$set_drill_down_start."\" + encodeURI( this.value ) )", $this->drill_dropdown);
							}
						}
						else
						{
							$str = "";
							if(isset($_GET['test']))
							{
								$str .= "no-drilldown";
							}
						}
					}
				} else if( strpos($str,"[money]")!==false ) {
					$str = htmlspecialchars(report_money_output( $str, $monetary_symbol ));
				} else if( strpos($str,"[number]")!==false ){
					$str = htmlspecialchars(report_number_output( $str ));
				} else if( strpos($str,"[date]")!==false ){
					$str = htmlspecialchars(report_date_to_number( $str ));
				}
				if(strpos($str,"title:")!==false && strpos($str,"title:")==0) {
					$str = ucwords(str_replace("_"," ",substr($str,6)));
				}
				if(!$drill_down){
					$str = $this->processTimeFunction( htmlspecialchars($str) );
				} else {
					$str = $this->processTimeFunction($str);
				}
			}
			return $str;
		}

		public function processTimeFunction( $str ) {
			if(strpos($str,"time(")!==false && strpos($str,"time(")==0) {
				$time_parts = explode(")",substr($str,5));
				if(count($time_parts) > 1)
				{
					$tstr = $time_parts[0];
					if($tstr[0] == '#'){
						return $time_parts[1];
					}
					if($tstr){
						$str = date($tstr,strtotime($time_parts[1]));
					} else {
						$str = $time_parts[1];
					}
				}
			}
			return $str;
		}

		public function prepare_query_for_drilldown()
		{
			if($this->exec_drilldown) // drillvars: title,title_col,subreport,filter_cols,filter_vals
			{
			    $_SESSION['drilldown']=1;
			    for($r=0; $r<count($this->drillvars); $r++)
				{
					$newselect = "";
					$select_parts = explode("[_sep_]",$this->report_read['select']);
					for($n=0; $n<count($select_parts); $n++)
					{
						if($n > 0) $newselect .= "[_sep_]";
						if(substr(trim(strtolower($select_parts[$n])),0,11)=="drill_down(") {
							$newselect .= str_replace('@', '#', $this->drillvars[$r]['subreport'] )."[_sep_]";
						}
						$newselect .= $select_parts[$n];
					}
					$this->report_read['select'] = $newselect;

					for($i=0; $i<count($this->drillvars[$r]['filter_cols']); $i++)
					{
						if( !empty( $this->drillvars[$r]['filter_cols'][$i] ) ){
							if( empty( $this->drillvars[$r]['filter_vals'][$i] ) ){
								$this->report_read['where'] .= "[_sep_](" . $this->drillvars[$r]['filter_cols'][$i] . " = '" . ConnectionHub::getConn('rest')->escapeString($this->drillvars[$r]['filter_vals'][$i]) . "' or ". $this->drillvars[$r]['filter_cols'][$i] . " IS NULL )";
							} else {
								if ($this->drillvars[$r]['filter_vals'][$i] == 'No Super Group') {
									$this->report_read['where'] .= "[_sep_](" . $this->drillvars[$r]['filter_cols'][$i] . " = '" . ConnectionHub::getConn('rest')->escapeString($this->drillvars[$r]['filter_vals'][$i]) . "' or ". $this->drillvars[$r]['filter_cols'][$i] . " IS NULL )";
								} else {
									$this->report_read['where'] .= "[_sep_]" . $this->drillvars[$r]['filter_cols'][$i] . " = '" . ConnectionHub::getConn('rest')->escapeString($this->drillvars[$r]['filter_vals'][$i]) . "'";
								}
							}
						}
					}
					if($this->drillvars[$r]['subreport'] == '@kiosk_order') {
						$this->report_read['where'] .= "[_sep_]#kiosk_orders";
					}
					if($this->drillvars[$r]['subreport'] == '@lavu_togo_order') {
					    $this->report_read['where'] .= "[_sep_]#lavu_togo_orders";
					}
					$this->report_read['groupby'] = str_replace('@', '#', $this->drillvars[$r]['subreport'] );
				}
			}
		}

		private function preProcessReportDetails( &$report_read ) {
			if( empty( $report_read['groupby'] ) ){
				$select_parts = explode('[_sep_]', $report_read['select']);
				if( count( $select_parts ) ){
					if( strstr($select_parts[0], 'sister(' ) !== FALSE ){
						$report_read['groupby'] = $select_parts[0];
					}
				}
			}
			$report_read['where'] = $this->applySister( 'where', $report_read['where'] );
			$report_read['select'] = $this->applySister( 'select', $report_read['select'] );
			$report_read['groupby'] = $this->applySister( 'groupby', $report_read['groupby'] );
		}


		private function applySister( $type, $str ){
			$str_parts = explode('[_sep_]', $str);
			$result = array();
			foreach( $str_parts as $key => $str_part ){
				if( strstr($str_part, 'sister(' ) === FALSE ){
					$result[] = $str_part;
				} else {
					$sister_r = sister_report::sister( $type, $str_part );
					if( $sister_r ){
						$result[] = $sister_r->value();
					} else {
						$result[] = $str_part;
					}
				}
			}
			return implode('[_sep_]', $result );
		}
		
		public function search_array_ordertype ( array $searchArray, $term )
		{
		    foreach ( $searchArray as $key => $value )
		        if ( stripos( $value, $term ) !== false )
		            return $key;
		            
		            return false;
		}

		public function query_string($vars=false,$orderby="", $dataname="" )
		{
			$locationInfo = sessvar('location_info');
			$outputMode = reqvar('output_mode', '');
			$isPrintable = (int) reqvar('printable', 0) === 1;
			$isOutputModeSet = $outputMode === 'csv' || $this->prepareQueriesForEmailReport;
			$this->prepare_query_for_drilldown();
			$this->preProcessReportDetails( $this->report_read );
			$this->rj = new report_joins($this->report_read);
			$this->rj->apply_aliases('join');
			$this->rj->apply_aliases('select');
			$this->rj->apply_aliases('where');
			$this->rj->apply_aliases('groupby');
			$this->rj->apply_aliases('orderby');


			$this->report_read = $this->rj->get_current_report_read();
			$this->report_read['join'] = replaceStartAndEndTime( $this->report_read['join'] );
			$this->report_read['select'] = replaceStartAndEndTime( $this->report_read['select'] );
			$this->report_read['where'] = replaceStartAndEndTime( $this->report_read['where'] );
			$this->report_read['groupby'] = replaceStartAndEndTime( $this->report_read['groupby'] );
			$this->report_read['orderby'] = replaceStartAndEndTime( $this->report_read['orderby'] );
			/* condition to check report is lavu gift & loyalty */
			if ($this->report_name === TYPE_LAVU_GIFT_AND_LOYALITY) {
				$whereCondition = "or (chainid='[chainid]' and chainid!='')";
				/* Condition for current location */
				if(!isset($_GET['multi_report']) || $_GET['sep_loc'] == 1) {
					$this->report_read['where'] = str_replace($whereCondition," ",$this->report_read['where']);
				}
			} //End
			$primary_table = $this->report_read['table'];
			$joins = explode("[_sep_]",replaceLocationID( $this->report_read['join']));
			$selects = explode("[_sep_]", replaceLocationID( $this->report_read['select']));

			/**
			* This condition is used to get subtotal instead calculating gross total in sales report.
			*/
			if ($this->report_name == 'sales') {
				$this->report_read['where'].=" AND `order_contents`.`quantity`>'0'";
				$selects[] = "SUM(order_contents.subtotal_with_mods - order_contents.itax) as tgross, SUM(order_contents.after_discount-order_contents.itax) as tnet";
			}
			/**
			* This condition is used to get gross total in EOD report.
			*/
			if ($this->report_name == 'super_groups' || $this->report_name == 'categories') {
				$this->report_read['where'].=" AND `order_contents`.`quantity`>'0'";
				$selects[] = "SUM(order_contents.subtotal_with_mods - order_contents.itax) as tgross";
			}

			/**
			 * This condition is used to get per order based cash tip.
			 */
			$perOrderCashTip = 0;
			if ($locationInfo['cash_tips_as_daily_total'] != 1 && $this->report_name == 'payments_perorder' && (!isset($_REQUEST['sister_select']) || $_REQUEST['sister_select'] == '#payment_type_name')) {
				$perOrderCashTip = 1;
				$dayCashTipQry = '';
				foreach ($selects as $key => $column) {
					if (stripos($column, ' as total_tip') == true) {
						$dayCashTipQry = str_replace('sum_price', 'sum', str_replace('as total_tip', '', $column));
						$totalTipKey = $key;
					}
				}
				$selects[$totalTipKey] = "perOrderTotalTips(IF(orders.order_id NOT LIKE '777%'  AND cc_transactions.pay_type='Cash', concat('[money]', format(cast(sum(IF(orders.order_id NOT LIKE '777%' AND cc_transactions.pay_type='Cash',orders.cash_tip,0)) as decimal(30, 5)), 4)), concat('[money]',format(cast(".$dayCashTipQry." as decimal(30,5)),4)))";
			}


			$wheres = explode("[_sep_]", replaceLocationID($this->report_read['where'], $dataname));
			$selectLength = sizeof($selects);
			$count = "0";
			if ($_REQUEST['clicked'] == 'drill_down') {
				for ($count=0; $count<$selectLength; $count++) {
					if ($selects[$count] == 'orders.order_id as order_time') {
						$selects[$count] = "orders.opened as Order_Opened_Time";
						$selects = array_merge(array_slice($selects, 0, $count), array("orders.closed as Order_Closed_Time"), array_slice($selects, $count-1));
					}
				}
			}
			if ($_REQUEST['sister_select'] == "#order_time") {
				array_shift($wheres);
			}
			$groupbys = explode("[_sep_]", replaceLocationID( $this->report_read['groupby']));
			$orderbys = explode("[_sep_]",replaceLocationID( $this->report_read['orderby']));
			
			$orderTagsJoin ="";
			$isThere = $this->search_array_ordertype($wheres, 'order_tags.name');
			if ($_REQUEST['sister_select'] == "#order_tag" && $isThere > 0) {
			    $typeName = explode("=",$wheres[3]);
			    $typeNameValue = trim(trim($typeName[1]),"'");
			    $tag_query = "select `id` from `order_tags` where `name`  = '".$typeNameValue."' and  id = 0 ";
			    $res_query = lavu_query($tag_query);
			    $res_count = mysqli_num_rows($res_query);
			    if ($res_count == 0) {
			        $config_tagname = "select `value` from `config` where `setting` = 'dine_in_order_type_name' ";
			        $res_query_config = lavu_query($config_tagname);
			        $numOfRows = mysqli_num_rows($res_query_config);
			        if ($numOfRows > 0) {
			            unset($wheres[3]);
			        }
			    }
			}

			if($orderby!="")
			{
				$orderbys = array_merge(array($orderby),$orderbys);
			}

			$new_selects = array();
			for($i=0; $i<count($selects); $i++) // MULTI SELECT (such as adjustments)
			{
				$select_string = $selects[$i];
				if(strpos($select_string,"adjust_*")!==false)
				{
					$table_seed = seed_query_result($primary_table);
					foreach($table_seed as $skey => $sval)
					{
						if(substr($skey,0,7)=="adjust_")
						{
							$new_select_string = $select_string;
							$new_select_string = str_replace("adjust_*","`".$skey."`",str_replace("`adjust_*`","adjust_*",$new_select_string));
							$new_select_string = str_replace("adjust_title_*","`".str_replace("option_","",str_replace("adjust_","",$skey))."`",str_replace("`adjust_title_*`","adjust_title_*",$new_select_string));
							$new_selects[] = $new_select_string;
						}
					}
				}
				else $new_selects[] = $select_string;
			}

			$selects = $new_selects;

			$select_str = "";
			for($i=0; $i<count($selects); $i++) // SELECT
			{
				$select_string = $this->process_merge_commands($selects[$i]);
				if(strpos($select_string,"[drill_down]")!==false && !$this->allow_drilldown( count( explode(',', $select_string )) )) $include_select = false; else $include_select = true;

				if($include_select)
				{
					if($select_str!="") $select_str .= ", ";
					$select_str .= $select_string;
				}
			}
			if($select_str=="") $select_str = "*";

			$join_str = "";
			if(count($joins)==0 || (count($joins)==1 && trim($joins[0])=="") || isset($_GET['test_join']))
			{
				$join_str = $this->rj->auto_join_string();
				if ($orderTagsJoin!="") {
				    $join_str .= $orderTagsJoin;
				}
			}
			else
			{
				for($i=0; $i<count($joins); $i++) // JOINS
				{
					$join_string = $joins[$i];
					$join_parts = explode("=",$join_string);
					if(count($join_parts) > 1)
					{
						$join_table_parts = explode(".",$join_parts[1]);
						$join_table = sanitize(trim($join_table_parts[0]));

						$join_str .= " LEFT JOIN `$join_table` ON ".$join_string;
					}
				}
			}
			if ($perOrderCashTip) {
				$join_str .= ' LEFT JOIN `orders` ON `cc_transactions`.`order_id`=`orders`.`order_id` ';
			}

            $formatPatternsInWhereClause = array ("[number]", "[money]", "[date]");
			$where_str = "";
			for($i=0; $i<count($wheres); $i++) // WHERE
			{
			    if (trim($wheres[$i]) != "") { 
			    $where_string = str_replace($formatPatternsInWhereClause, "", $wheres[$i]);
				if ($where_str!="") $where_str .= " and ";
				$where_str .= $where_string;
			    }
			}
			if ($perOrderCashTip) {
				if ($where_str != '') {
					$where_str .= ' AND ';
				}
				$where_str .= '`orders`.`order_id` NOT LIKE "777%"';
			}

			if( isset($_REQUEST['sister_select']) && $_REQUEST['sister_select'] == "#contents_name") {
				$where_str .= " AND `order_contents`.is_combo != 1";
			}
			$groupby_str = ""; // GROUP BY
			for($i=0; $i<count($groupbys); $i++)
			{
				$groupby_string = trim($groupbys[$i]);

				if( strlen( $groupby_string ) && '#' === $groupby_string[0] ){
					$alias = report_alias::alias( substr( $groupby_string, 1 ) );
					$groupby_string = $alias->statement();
				}

				if($groupby_string!="")
				{
					if($groupby_str!="")
						$groupby_str .= ",";
				}
				$groupby_str .= $groupby_string;
			}

			if( $groupby_str != '' )
				$groupby_str = " group by " . $groupby_str;

			$orderby_str = "";
			for($i=0; $i<count($orderbys); $i++)
			{
				$orderby_string = trim($orderbys[$i]);
				if($orderby_string!="")
				{
					$orderby_string_parts = explode(" desc",$orderby_string);
					if(count($orderby_string_parts) > 1)
					{
						$orderby_string = $orderby_string_parts[0];
						$orderby_dir = "desc";
					}
					else
					{
						$orderby_string_parts = explode(" asc",$orderby_string);
						$orderby_string = $orderby_string_parts[0];
						$orderby_dir = "asc";
					}

					if( stristr("{ $select_str}", " {$orderby_string}") === FALSE ){
						$orderby_string = str_ireplace(' ', '_', $orderby_string );
						if( stristr(" {$select_str}", " {$orderby_string}") === FALSE ){
							continue;
							//ABANDON ALL HOPE FOR THIS ORDER BY TERM!
						}
					}

					if(is_alpha_numeric($orderby_string)) {
						for($n=0; $n<count($selects); $n++) { //SELECT
							$select_string = $selects[$n];
							if(strpos(strtolower($select_string)," as " . strtolower($orderby_string)) || strpos(strtolower($select_string)," as `" . strtolower($orderby_string) . "`")) {
								$select_string = $this->process_query_command("sum","(sum([val])*1)",$select_string);
								$select_string = $this->process_query_command("sum_price","(sum([val])*1)",$select_string);
								$select_string = $this->process_query_command("sum_rounded","(sum([val])*1)",$select_string);
								$select_string = $this->process_query_command("sum_float","(sum([val])*1)",$select_string);

								$select_string_parts = explode(" as ",$select_string);
								$orderby_string = trim($select_string_parts[0]);
							}
						}
					}
					if(substr(trim($orderby_string),0,11)=="drill_down(") $orderby_can_add = false;
					else $orderby_can_add = true;
					if($orderby_can_add) {
						if($orderby_str=="")
							$orderby_str .= " order by ";
						else
							$orderby_str .= ", ";

						$orderby_str .= "{$orderby_string}" . " " . $orderby_dir;
					}
				}
			}


			//echo "groupby_str====>".$groupby_str."groupbys====>".$groupbys."<br/><br/>";

			$this->groupby_cols = $this->parse_groupby_cols($groupby_str, $groupbys);
			if($select_str!="*" && count($this->groupby_cols) > 0)
			{
				for($n=0; $n<count($this->groupby_cols); $n++)
				{
					$groupby_colname = $this->parse_tablecol_for_mysql($this->groupby_cols[$n]);
					$groupby_set_colname = "`group_col_" . str_replace("`","",$this->parse_tablecol_for_mysql($this->groupby_cols[$n],1))."`";
					$select_str .= ", $groupby_colname as $groupby_set_colname";
				}
			}
			
			if($_REQUEST['sister_select'] == "#combo_sales"){

			    $groupby_str = " group by order_contents.combo_id";
				// X.quantity,
  $str = "select X.combo_group_id as combo_id,Y.name, X.order_count,X.gross,X.discount,X.net,X.order_tax,X.included_tax from 
( select order_contents.`combo_id` AS combo_group_id, sum(order_contents.quantity) as quantity, if(count(distinct orders.ioid )>count( distinct orders.order_id ),count( distinct orders.ioid ),
count( distinct orders.order_id )) as order_count, if(count(*) > 0, concat('[money]',
format(cast(sum((order_contents.price + order_contents.modify_price + order_contents.forced_modifiers_price)*order_contents.quantity - if( order_contents.quantity > '0', order_contents.itax, 0) )
as decimal(30,5)),2)), '[money]0.00') as gross, if(count(*) > 0, concat('[money]',format(cast(sum(if(order_contents.quantity > 0, order_contents.discount_amount + order_contents.idiscount_amount, 0))
as decimal(30,5)),2)), '[money]0.00') as discount, if(count(*) > 0, concat('[money]',
format(cast(sum((order_contents.price + order_contents.modify_price + order_contents.forced_modifiers_price)*order_contents.quantity - if( order_contents.quantity > '0',
if(order_contents.discount_amount<(order_contents.price + order_contents.modify_price + order_contents.forced_modifiers_price),order_contents.discount_amount,order_contents.price + 
order_contents.modify_price + order_contents.forced_modifiers_price)+ order_contents.idiscount_amount + order_contents.itax, 0))as decimal(30,5)),2)), '[money]0.00') as net, 
if(count(*) > 0, concat('[money]',format(cast(sum( if( order_contents.quantity != 0 AND order_contents.tax_exempt != '1', `order_contents`.`tax1` + `order_contents`.`tax2` + `order_contents`.`tax3` +
 `order_contents`.`tax4` + `order_contents`.`tax5` - `order_contents`.`itax`, 0 ) )as decimal(30,5)),2)), '[money]0.00') as order_tax, if(count(*) > 0, concat('[money]',
format(cast(sum( if( order_contents.quantity != 0 AND order_contents.tax_exempt != '1', `order_contents`.`itax`, 0) )as decimal(30,5)),2)), '[money]0.00') as included_tax, 
concat('[drill_down]','@menu_category_name,@super_group,@contents_name,@order_server,@order_cashier,@rev_center,@order_register,@meal_period,@order_register,@order_tag,@order_id,@kiosk_order,@lavu_togo_order,@content_options,
@closed_by_hour,@closed_by_day,@closed_by_weekday,@closed_by_week_for_year,@closed_by_month,@closed_by_year,@closed_actual_date_hour,@item_added_by_hour,@item_added_by_day,@item_added_by_weekday,
@item_added_by_week_for_year,@item_added_by_month,@item_added_by_year,@item_added_actual_date_hour,@order_reg_group,@order_time,@combo_sales') as filters, 
`menu_items`.`name` as `group_col_menu_items_name` from `orders`LEFT JOIN `order_contents` ON `orders`.`order_id` = `order_contents`.`order_id` AND `orders`.`location_id` = `order_contents`.`loc_id` 
LEFT JOIN `menu_items` ON `order_contents`.`item_id` = `menu_items`.`id`  where " . $where_str . $groupby_str . $orderby_str ."
 )AS X JOIN menu_items as Y ON X.combo_group_id = Y.id";

			}else{

			$str = "select " . $select_str . " from `" . $primary_table . "`" . $join_str . " where " . $where_str . $groupby_str . $orderby_str;
		}
			if(is_array($vars))
			{
				foreach($vars as $var_key=>$var_val)
				{
					if(is_string($var_val))
					{
						$str = str_replace("[".$var_key."]",$var_val,$str);
					}
				}
			}

			if (isset($_REQUEST['sister_select'])) {
			    $sisterSelect = substr($_REQUEST['sister_select'],1);
			    switch ($sisterSelect) {
			        case "order_time":
			        case "overpaid_order":
			        case "underpaid_order":
			        case "kiosk_order":
			        case "lavu_togo_order":
			            $str = str_replace($sisterSelect,"order_id",$str);
			            break;
			        default:
			            break;
			    }
			}
			if ($_REQUEST['sister_select'] == "#super_group") {
				if (strpos($str, 'group by payment_types.type') !== false) {
					$pay_change ="LEFT JOIN  ( SELECT `cc`.`order_id`, `cc`.`loc_id`, `cc`.`pay_type_id`, `cc`.`voided`  FROM   `cc_transactions` AS `cc` LEFT JOIN `orders` ON `cc`.`order_id` = `orders`.`order_id` where `orders`.`closed` >= '".$vars[start_datetime]."' and `orders`.`closed` < '".$vars[end_datetime]."' GROUP BY `cc`.`order_id`) AS `cc_transactions`";
					$str = str_replace("LEFT JOIN `cc_transactions`", $pay_change,$str);
				}
			}
			$this->dates = $vars[start_datetime].",".$vars[end_datetime];
			if ($this->report_read['name'] === TYPE_LAVU_GIFT_AND_LOYALITY && !$isPrintable && !$isOutputModeSet) {
				$paginationConfig = $this->getPaginationConfiguration();
				$str .= " limit ".$this->calculatePageOffset($paginationConfig->limit).",".$paginationConfig->limit;
			}
			return $str;
		}

		public function url($urlstr)
		{
			return add_to_url($this->base_url,$urlstr);
		}

		public function innerurl($urlstr,$include_drillvars=true)
		{
			//$urlstr .= $this->drillvar_query_string();
			if($urlstr!="") $url_addstr = "&".$urlstr;
			else $url_addstr = "";

			if($include_drillvars)
			{
				$urlstr = $this->drillvar_query_string() . $url_addstr;
			}
			else
			{
				$urlstr = $url_addstr;
			}
			return add_to_url($this->base_url,$urlstr);
		}
		public function chain_reporting_groups(){
		    $this->report_name = $this->report_read['name'];
		    $result =  get_chain_reporting_group($this->report_name);
			if( $result && isset( $result['display'] ) ){

				$result['display'] = str_ireplace('[url]', $this->innerurl( 'mode=' . getvar('mode','')), $result['display']);
			}
			return $result;
		}

		public function datetime_selector()
		{
			global $location_info;
			$output = "";
			$repeat = false;
			$vars = array();
			$vars_next = array();
			$vars_next2 = array();

			$vars['start_datetime'] = "";
			$vars['end_datetime'] = "";
			$vars['foldername'] = get_current_foldername();

			$earliest_year = 2005;
			$latest_year = (date("Y") * 1) + 2;
			$blocked_directional_button = "<input type='button' value='     ' style='color:#aaaaaa'>";

			$mode = getvar("mode","");
			$custom_fname_str = substr($this->report_read['special_code'], 9);
			$custom_fname_parts = explode("\n", $custom_fname_str);
			$custom_fname = trim(str_replace("..", "", $custom_fname_parts[0]));
			if($mode=="")
			{
				if($this->report_selection=="Preset Date Ranges" || $this->report_selection=="Repeating Date Ranges")
				{
					$mode = "day";
				}
			}
			$output .= "<div id=\"datetime_selector\" class=\"datetime_selector\" >\n";
			$output .= "<script language='javascript'>\n";
			$output .= "$(document).ready(function() {
				var str = '';
				$( '.report_table th' ).each(function() {
				str = $( this ).attr('class');
				if(str == 'alignRight'){
					$( this ).attr('class','alignLeft');
				}
				});
				});";
			$_history_url = $this->innerurl("mode={$mode}&".$this->drillvar_query_string()."&report={$this->report_id}");
			$allowedEmailReports = [
				TYPE_LAVU_GIFT_AND_LOYALITY => speak("Your CSV export request is being processed and will be delivered via email when it is complete. Please enter and confirm your email address below. After confirming your address, you may resume normal use of the Control Panel while awaiting the report.")
			];

			$userEmail = sessvar('admin_username') === 'poslavu_admin' ? $_SESSION['posadmin_email'] : sessvar('admin_email');
			$output .= "	if( window.history.state == null && window.history.replaceState ) { window.history.replaceState( '{$_history_url}', 'page', window.location.href ) };\n";
			$output .= " function validateEmail(email) {\n
	                       var re = /^(([^<>()\[\]\\.,;:\s@\"]+(\.[^<>()\[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;\n
	                       return re.test(email);\n
	                     }\n
	                     function getParameterByName(name, url) {\n
	                       if (!url) url = window.location.href;\n
	                       name = name.replace(/[\[\]]/g, \"\\$&\");\n
	                       var regex = new RegExp(\"[?&]\" + name + \"(=([^&#]*)|&|#|$)\"),\n
	                       results = regex.exec(url);\n
	                       if (!results) return null;\n
	                       if (!results[2]) return '';\n
	                       return decodeURIComponent(results[2].replace(/\+/g, \" \"));\n
						 }\n";
			$output .= "	var isPaginationSet = false;
						 	function printable(pageUrl) {
								if(isPaginationSet === true) {
									sendReportToEmail(pageUrl);
								} else {
									window.open(pageUrl, '_blank');
								}
							}
							function downloadReport(pageUrl) {
								if(isPaginationSet === true) {
									sendReportToEmail(pageUrl);
								} else {
									window.location = pageUrl;
								}
							}";
			$output .= "	function sendReportToEmail(pageUrl) {
								this.isAllowed = '".(isset($allowedEmailReports[$this->report_name]) ? 'yes' : 'no')."';
								var mode = getParameterByName('mode', pageUrl);
								if(this.isAllowed === 'yes') {
									var email = prompt('".$allowedEmailReports[$this->report_name]."', '".$userEmail."');
									pageUrl += '&output_mode=".OUTPUT_MODE_CSV_TO_EMAIL."&email=' + email;
									if (email === null) {
										return false;
									}
									if (email !== null && email.trim() === '') {
										alert('".speak("Email Id Cannot be empty")."');
										sendReportToEmail(pageUrl);
										return false;
									} else if(!validateEmail(email)) {
										alert('".speak("Enter valid Email Id")."');
										sendReportToEmail(pageUrl);
                                        return false;
									} else {
										var configUrl = '/cp/index.php' + pageUrl;
										$.ajax({
											url : configUrl,
											method: 'GET',
											dataType: 'JSON',
											success: function(response) {
												alert(response.message);
											}
										});
									}
								}
							}
						";
			$output .= "	function click_to_page(page_url){\n";
			$output .= "	    var report = '". $this->report_id."';
								var reportName = '".$this->report_name."';
								isPaginationSet = false;
								if(document.getElementById('tcal'))
								{
									document.getElementById('tcal').style.visibility='hidden';
								}
								if(document.getElementById('idayleftbtn') && document.getElementById('idayrightbtn'))
								{
									document.getElementById('idayleftbtn').style='pointer-events: none;';
									document.getElementById('idayrightbtn').style='pointer-events: none;';
								}
								if(document.getElementById('imnthleftbtn') && document.getElementById('imnthrightbtn'))
								{
									document.getElementById('imnthleftbtn').style='pointer-events: none;';
									document.getElementById('imnthrightbtn').style='pointer-events: none;';
								}
								if(document.getElementById('iyrleftbtn') && document.getElementById('iyrrightbtn'))
								{
									document.getElementById('iyrleftbtn').style='pointer-events: none;';
									document.getElementById('iyrrightbtn').style='pointer-events: none;';
								}

							    var mode = getParameterByName('mode',page_url);
							    var multi_report = getParameterByName('multi_report',page_url);
							    var sister_select = getParameterByName('sister_select',page_url);
							    var start_time = getParameterByName('set_start_time', page_url);
							    var end_time = getParameterByName('set_end_time', page_url);
							    var close = getParameterByName('close', page_url);
							    if (start_time != null && end_time != null && close != 1) {
							        var day = document.getElementById('day').value;
							        var secondDay = document.getElementById('second_day').value;
							        var days = new Date(day);
							        if (days == 'Invalid Date') {
							            var days = day.split('/');
							            var dayNumber = new Date(days[2], days[1], days[0], start_time, 0, 0).getTime();
							            var dayNumbers = secondDay.split('/');
							            var secondDayNumber = new Date(dayNumbers[2], dayNumbers[1], dayNumbers[0], end_time, 0, 0).getTime();
							        } else {
							            var dayNumber = new Date(days.getFullYear(), days.getMonth(), days.getDate(), start_time, 0, 0).getTime();
							            var dayNumbers = new Date(secondDay);
							            var secondDayNumber = new Date(dayNumbers.getFullYear(), dayNumbers.getMonth(), dayNumbers.getDate(), end_time, 0, 0).getTime();
							        }
							        if (dayNumber >= secondDayNumber) {
								        alert('Start time should be less than End time!');
								        document.getElementById('second_time').value = '';
								        return false;
							        }
							    }
                                var sep_loc = getParameterByName('sep_loc',page_url);
								
                                if(report =='42' && mode =='year' && multi_report != null && multi_report != '' && multi_report != 0)
                                {
                                    if(sister_select =='fiscal_accounting' || sister_select =='fiscal_daily_accounting')
                                    {
                                        var email = prompt('".speak("Please enter Email Id to send report")."', '');

                                        if (email === null)
                                        {
                                            window.location.href='?mode=reports_reports_v2&report=42';
                                            return false;
                                        }

                                        if (email != null && email != '')
                                        {
                                            if (validateEmail(email))
                                            {
                                                page_url = page_url.concat('&email='+email);
                                            }
                                            else
                                            {
                                                alert('".speak("Enter valid Email Id")."');
                                                return false;
                                            }
                                        }
                                        else
                                        {
                                            alert('".speak("Email Id Cannot be empty")."');
                                            window.location.href='?mode=reports_reports_v2&report=42';
                                            return false;
                                        }

                                        var year = prompt('".speak("Please enter Fiscal Year")."', '');

                                        if (year === null)
                                        {
                                            window.location.href='?mode=reports_reports_v2&report=42';
                                            return false;
                                        }

                                        if (year != null && year != '')
                                        {
                                            if(isNaN(year))
                                            {
                                                alert('".speak("Enter valid Fiscal Year")."');
                                                return false;
                                            }
                                            page_url = page_url.concat('&report_year='+year);
                                        }
                                        else
                                        {
                                            alert('".speak("Fiscal Year Cannot be empty")."');
                                            window.location.href='?mode=reports_reports_v2&report=42';
                                            return false;
                                        }
                                        if (sep_loc === null || sep_loc == null)
                                        {
                                            page_url = page_url.concat('&sep_loc=');
                                        }
                                        page_url = page_url.concat('&output_mode=csv');
                                    }

                                }";
			$output .= " 		Color.reset();\n";
			$output .= "		ajax_url = 'reports.display?report=" . $this->report_id;
			if(isset($_GET['printable']) && $_GET['printable']=="1")
			{
				$output .= "&printable=1";
			}
			$output .= "&' + page_url.replace(/\?/ig,'');\n";
			$output .= "		if(window.history.pushState) window.history.pushState(ajax_url, 'page', window.location.href);\n";
			if ($this->report_name === TYPE_LAVU_GIFT_AND_LOYALITY) {
				$output .= "	ajax_link('report_container',ajax_url,'','',['f_tcalInit', 'initPagination']);\n";
			} else {
				$output .= "	ajax_link('report_container',ajax_url,'','','f_tcalInit');\n";
			}
			$output .= "        if(report =='42' && mode =='year' && multi_report != null && multi_report != '' && multi_report != 0)
                                {
                                    if(sister_select =='fiscal_accounting' || sister_select =='fiscal_daily_accounting')
                                    {
                                        setTimeout(function () { alert('".speak("Please check your inbox for the requested report")."'); window.location.href='?mode=reports_reports_v2&report=42'; \n}, 5000);
                                    }";
			$output .= "        }";
			$output .= "	}\n";
			$output .= "alignColumn();";
			$output .= "function alignColumn(){\n
					var str = '';\n
				    $( '.report_table th' ).each(function() {\n
				      str = $( this ).attr('class');\n
				      if(str == 'alignRight'){\n
						$( this ).attr('class', 'alignLeft');\n
					}\n
				});\n
				}\n";
			$output .= "$('.toggle').click(function(e) {
                                      e.preventDefault();
                                      enableRows($(this).closest('tr').next());
                                   });
                                      function enableRows(row) {
                                               if(row.hasClass('show')) {
                                               row.slideToggle();
                                                 if($(row).next().hasClass('show')) {
                                                   enableRows($(row).next());
                                                 } else {
                                                   return;
                                                 }
                                               } else { return; }
                                     }";
			$output .= "</script>";

			if($this->report_selection=="Preset Date Ranges" || $this->report_selection=="Repeating Date Ranges")
			{
				$mode_list = array("Day", "Month", "Year");
				$output .= "<table cellspacing=2 cellpadding=8>";
				$output .= "<tr>";
				for($n=0; $n<count($mode_list); $n++)
				{
					$cmode = strtolower($mode_list[$n]);
					if($cmode==$mode)
						$set_attributes = "class='datetime_choice' selected";
					else
						$set_attributes = "class='datetime_choice'";
					$output .= "<td {$set_attributes} align='center' width='80' onclick='click_to_page(\"" . $this->innerurl("mode=$cmode") . "\")'>";
					if($this->report_selection=="Repeating Date Ranges")
					{
						$output .= "By ";
					}
					$output .= speak($mode_list[$n]);
					$output .= "</td>";
				}
				$output .= "</tr>";
				$output .= "</table>";
				$output .= "<br>";

				if($this->report_selection=="Repeating Date Ranges")
				{
					if($mode=="day") $mode_select = "month";
					else if($mode=="month") $mode_select = "year";
					else $mode_select = "all-years";
				}
				else
				{
					$mode_select = $mode;
				}

				if($mode_select=="day")
				{
					// based on date format setting
					$date_setting = "date_format";
					$location_date_format_query = lavu_query( "SELECT `location`, `setting`, `value` FROM `config` WHERE `setting` = '[1]'",  $date_setting);
					if( !$location_date_format_query || !mysqli_num_rows($location_date_format_query)){
						$date_format = array("value" => 2);
					}else
						$date_format = mysqli_fetch_assoc( $location_date_format_query );

					$datetime_str = DateTimeForTimeZone($location_info['timezone']);
					$datetime_arr = explode(" ", $datetime_str);
					$datetime = $datetime_arr[0];

					$mode_var = "report_day";
					if ($datetime != memvar($mode_var, date("Y-m-d"))) {
						$day = memvar($mode_var, date("Y-m-d"));
					} else {
						$day = $datetime;
						$status = respectTimezoneForAllReports();
						if ($status == -1) {
							$day = date('Y-m-d', strtotime("-1 day", strtotime($day)));
						}
                                                $se_mins = ($location_info['day_start_end_time']*1)%100;
                                                $se_hours = (($location_info['day_start_end_time']*1)-$se_mins)/100;
                                                $time = $se_hours.":".$se_mins;
                                                $currentDate = date("H:ia", strtotime($time));
                                                $output .= "<div style='word-wrap: break-word;width: 500px;'>*Records will appear as per 'Day start/end time' settings in Advanced Location Settings. Your current day started from $day $currentDate </div><br>";
					}

					$mode_var_second = "report_day_second";
					$day_second = memvar($mode_var_second,"");
					$output .= '<div class="datetime_interactive">';
					$output .= "<div class='arrow left' id='idayleftbtn' onclick='$this->click_to_page(\"" . $this->innerurl("mode=$mode&$mode_var_second=&$mode_var=" . basic_date_add($day,-1)) . "\")'>&#x25C0;</div>";
					$show_day = display_date($day);

					$change_format = explode("/", $show_day);
					switch ($date_format['value']){
						case 1:$show_day = $change_format[1]."/".$change_format[0]."/".$change_format[2];
						break;
						case 2:$show_day = $change_format[0]."/".$change_format[1]."/".$change_format[2];
						break;
						case 3:$show_day = $change_format[2]."/".$change_format[0]."/".$change_format[1];
						break;
						default:$show_day = $change_format[0]."/".$change_format[1]."/".$change_format[2];
					}

					$output .= "<script language='javascript'>";
					$output .= "	cal_select_day = function(selected_day) { $this->click_to_page(\"" . $this->innerurl("mode=$mode&$mode_var_second=&$mode_var=") . "\" + selected_day); } ";
					$output .= "</script>";
					$output .= "<div class='input_wrapper'><input type='text' size='12' readonly name='day' id='day' class='tcal' value='$show_day' /></div>";

					$day_for_next = $day;
					$day_second_for_next = "";
					$allow_date_range_selection = true;
					if($allow_date_range_selection)
					{
						$output .= "<script language='javascript'>";
						$output .= "	cal_select_second_day = function(selected_day) { $this->click_to_page(\"" . $this->innerurl("mode=$mode&$mode_var=$day&$mode_var_second=") . "\" + selected_day); } ";
						$output .= "</script>";
						if($day_second != "" && $day_second > $day)
						{
							$default_display_range = "none";
							$default_display_second_button = "inline";
							$day_for_next = $day_second;
							$day_second_for_next = basic_date_add($day_second,basic_date_diff($day,$day_second) + 1);
						}
						else
						{
							$default_display_range = "inline";
							$default_display_second_button = "none";
						}
						if($day_second=="" || $day_second < $day)
						{
							$day_second = $day;
						}
						$show_day_second = display_date($day_second);

						$change_format = explode("/", $show_day_second);
						switch ($date_format['value']){
							case 1:$show_day_second = $change_format[1]."/".$change_format[0]."/".$change_format[2];
							break;
							case 2:$show_day_second = $change_format[0]."/".$change_format[1]."/".$change_format[2];
							break;
							case 3:$show_day_second = $change_format[2]."/".$change_format[0]."/".$change_format[1];
							break;
							default:$show_day_second = $change_format[0]."/".$change_format[1]."/".$change_format[2];
						}

						$output .= "<div class='input_wrapper' style='display:$default_display_range' ><button style='display:$default_display_range' id='display_range_button' onclick='this.style.display = \"none\"; this.parentNode.style.display = \"none\"; document.getElementById(\"second_day\").style.display = \"\"; document.getElementById(\"second_day\").parentNode.style.display = \"\"; '>".speak('Range')."</button></div>";
						$output .= "<div class='input_wrapper' style='display:$default_display_second_button'><input type='text' size='12' id='second_day' readonly name='second_day' class='tcal' value='$show_day_second' style='display:$default_display_second_button' /></div>";
					}

					$output .= "<div class='arrow right' id='idayrightbtn' onclick='$this->click_to_page(\"" . $this->innerurl("mode=$mode&$mode_var_second=" . $day_second_for_next . "&$mode_var=" . basic_date_add($day_for_next,+1)) . "\")'>&#x25B6;</div>";
					$output .= '</div>';
					$allow_hour_span_controls = true;
					if($allow_hour_span_controls)
					{
						$set_start_time = memvar("set_start_time","");
						$set_end_time = memvar("set_end_time","");

						$set_time_selector_display = "none";
						$set_time_button_display = "inline";

						if($set_start_time!="" && $set_end_time!="")
						{
							$set_time_selector_display = "inline";
							$set_time_button_display = "none";
						}

						$set_time_click = "if(document.getElementById(\"first_time\").value!=\"\" && document.getElementById(\"second_time\").value!=\"\") $this->click_to_page(\"" . $this->innerurl("mode=$mode") . "&set_start_time=\" + document.getElementById(\"first_time\").value + \"&set_end_time=\" + document.getElementById(\"second_time\").value)";
						if ($custom_fname == 'get_tip_sharing') {
							$output .= '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<div class="datetime_interactive" style="display: none;">';
						} else {
							$output .= '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<div class="datetime_interactive">';
						}
						$output .= "<div class='input_wrapper leftBorder'><select class='ttimInput' id='first_time' style='font-size:10px; /*display:$set_time_selector_display*/' onchange='$set_time_click'>";
						$output .= "<option value=''>".speak("Start")."</option>";
						for($n=0; $n<24; $n++)
						{
							$output .= "<option value='$n'".(($n==$set_start_time && $set_start_time!="")?" selected":"").">" . date("h:i a",mktime($n,0,0,1,1,2014)) . "</option>";
						}
						$output .= "</select></div>";
						$output .= " ";
						$output .= "<div class='input_wrapper'><select class='ttimInput' id='second_time' style='font-size:10px; /*display:$set_time_selector_display*/' onchange='$set_time_click'>";
						$output .= "<option value=''>".speak("End")."</option>";
						for($n=1; $n<=24; $n++)
						{
							$output .= "<option value='$n'".($n==$set_end_time?" selected":"").">" . date("h:i a",mktime($n,0,0,1,1,2014)) . "</option>";
						}
						$output .= "</select></div>";
						$output .= '</div>';
						if($set_start_time!="" && $set_end_time!="")
						{
							if ($custom_fname != 'get_tip_sharing') {
								$output .= " <a style='cursor:pointer; color:#ccc; font-size:14px; float: right; padding: 5px; font: inherit !important;' onclick='$this->click_to_page(\"" . $this->innerurl("mode=$mode") . "&set_start_time=&set_end_time=&close=1\")'><b>X</b></a>";
							}
						}
						$startDate = $startDate ." ".$set_start_time;
					}

					if($this->report_selection=="Repeating Date Ranges")
					{
					}
					else
					{
						if($set_start_time!="" && $set_end_time!="")
						{
							$set_start_hour = lzero($set_start_time);
							$set_end_hour = lzero($set_end_time);
						}
						else
						{
							$set_start_hour = "00";
							$set_end_hour = "24";
						}
						$vars['start_datetime'] = $day . " ".$set_start_hour.":00:00";
						$vars['end_datetime'] = $day . " ".$set_end_hour.":00:00";

						if ($day_second != "" && $day_second > $day) {
							$vars['end_datetime'] = $day_second . " ".$set_end_hour.":00:00";
						}
					} 
				}
				else if($mode_select=="month")
				{
					$mode_var = "report_month";
					$month = memvar($mode_var,date("Y-m"));
					$output .= '<div class="datetime_interactive">';
					if($month > $earliest_year."-01")
						$output .= "<div class='arrow left' id='imnthleftbtn' onclick='$this->click_to_page(\"" . $this->innerurl("mode=$mode&$mode_var=" . basic_date_add($month,-1)) . "\")'>&#x25C0;</div>";
					else
						$output .= $blocked_directional_button;
					$output .= "<div class='input_wrapper'><select id ='month' mode onchange='$this->click_to_page(\"" . $this->innerurl("mode=$mode&$mode_var=") . "\" + encodeURI( this.value ) )'>";
					$output .= "<option value=''></option>";

					for($y=$earliest_year; $y<=$latest_year; $y++)
					{
						for($m=1; $m<=12; $m++)
						{
							$mn = lzero($m);
							$output .= "<option value='$y-$mn'";
							if("$y-$mn"==$month)
								$output .= " selected";
							$output .= ">".get_month_name($mn)." $y</option>";
						}
					}
					$output .= "</select></div>";
					if($month < $latest_year."12")
						$output .= "<div class='arrow right' id='imnthrightbtn' onclick='$this->click_to_page(\"" . $this->innerurl("mode=$mode&$mode_var=" . basic_date_add($month,+1)) . "\")'>&#x25B6;</div>";
					else
						$output .= $blocked_directional_button;

					if($this->report_selection=="Repeating Date Ranges")
					{
						$repeat = true;
						$vars = array();
						for($n=1; $n<=31; $n++)
						{
							$val = $month . "-" . lzero($n);
							$val_vars = array();
							$val_vars['start_datetime'] = $val . " 00:00:00";
							$val_vars['end_datetime'] = $val . " 24:00:00";
							$val_vars['row_title'] = get_month_name($month) . " " . $n;
							$val_vars['row_title_details'] = $val_vars['row_title'];
							$vars[] = $val_vars;

							$val = basic_date_subtract_year($val);
							$val_vars = array();
							$val_vars['start_datetime'] = $val . " 00:00:00";
							$val_vars['end_datetime'] = $val . " 24:00:00";
							$val_vars['row_title'] = "Last Year";
							$val_vars['row_title_details'] = get_month_name($month) . " " . $n;
							$vars_next[] = $val_vars;

							$val = basic_date_subtract_year($val);
							$val_vars = array();
							$val_vars['start_datetime'] = $val . " 00:00:00";
							$val_vars['end_datetime'] = $val . " 24:00:00";
							$val_vars['row_title'] = "Last Year";
							$val_vars['row_title_details'] = $val_vars['row_title'];
							$vars_next2[] = $val_vars;
						}
					}
					else
					{
						$month_parts = explode('-', $month);
						$year_part = $month_parts[0];
						$month_part = $month_parts[1];
						$vars['start_datetime'] = $year_part.'-'.$month_part. "-01 00:00:00";
						$vars['end_datetime'] = $year_part.'-'.($month_part < 9 ? '0'.($month_part+1) : $month_part+1). "-00 24:00:00";
					}
					$output .= "</div><br/><br/>";
				}
				else if($mode_select=="year")
				{
					$output .= '<div class="datetime_interactive">';
					$mode_var = "report_year";
					$year = memvar($mode_var,date("Y"));
					if($this->autoselect_date=="current_year") $year = date("Y");

					if($year > $earliest_year)
						$output .= "<div class='arrow left' id='iyrleftbtn' onclick='$this->click_to_page(\"" . $this->innerurl("mode=$mode&$mode_var=" . basic_date_add($year,-1)) . "\")'>&#x25C0;</div>";
					else
						$output .= $blocked_directional_button;

					$output .= "<div class='input_wrapper'><select id='year' onchange='$this->click_to_page(\"" . $this->innerurl("mode=$mode&$mode_var=") . "\" + encodeURI( this.value ) )'>";
					$output .= "<option value=''></option>";
					for($y=$earliest_year; $y<=$latest_year; $y++)
					{
						$output .= "<option value='$y'";
						if($y==$year)
							$output .= " selected";
						$output .= ">$y</option>";
					}
					$output .= "</select></div>";

					if($year < $latest_year)
						$output .= "<div class='arrow right' id='iyrrightbtn' onclick='$this->click_to_page(\"" . $this->innerurl("mode=$mode&$mode_var=" . basic_date_add($year,+1)) . "\")'>&#x25B6;</div>";
					else
						$output .= $blocked_directional_button;

					if($this->report_selection=="Repeating Date Ranges")
					{
						$repeat = true;
						$vars = array();
						for($n=1; $n<=12; $n++)
						{
							$val = $year . "-" . lzero($n);
							$val_vars = array();
							$val_vars['start_datetime'] = $year . "-" . lzero($n) . "-01 00:00:00";
							$val_vars['end_datetime'] = $year . "-" . lzero($n+1) . "-00 24:00:00";
							$val_vars['row_title'] = get_month_name($n);
							$val_vars['row_title_details'] = get_month_name($n) . " " . $year;
							$vars[] = $val_vars;

							if($n==date("m") && 1==2) // Temporarily disabled because previous year to date should be base on order "created" time, not the event start date
							{
								$set_previous_title = "Previous Year (to date)";
								$set_previous_end_datetime = "-" . date("d") . " 24:00:00";
							}
							else
							{
								$set_previous_title = "Previous Year";
								$set_previous_end_datetime = "-31 24:00:00";
							}

							$val = basic_date_subtract_year($val);
							$val_vars = array();
							$val_vars['start_datetime'] = $val . "-01 00:00:00";
							$val_vars['end_datetime'] = $val . $set_previous_end_datetime;
							$val_vars['row_title'] = $set_previous_title;
							$val_vars['row_title_details'] = get_month_name($n) . " " . ($year -1);
							$vars_next[] = $val_vars;

							$val = basic_date_subtract_year($val);
							$val_vars = array();
							$val_vars['start_datetime'] = $val . "-01 00:00:00";
							$val_vars['end_datetime'] = $val . $set_previous_end_datetime;
							$val_vars['row_title'] = $set_previous_title;
							$val_vars['row_title_details'] = get_month_name($n) . " " . ($year -2);
							$vars_next2[] = $val_vars;
						}
					}
					else
					{
						$vars['start_datetime'] = $year . "-01-01 00:00:00";
						$vars['end_datetime'] = $year . "-12-31 24:00:00";
					}
					$output .= "</div><br/><br/>";
				}
				else if($mode_select=="all-years")
				{
					if($this->report_selection=="Repeating Date Ranges")
					{
						$repeat = true;
						$vars = array();
						for($n=0; $n<=10; $n++)
						{
							$val = date("Y") - ($n * 1);
							$val_vars['start_datetime'] = $val . "-01-01 00:00:00";
							$val_vars['end_datetime'] = $val . "-12-31 24:00:00";
							$val_vars['row_title'] = $val;
							$val_vars['row_title_details'] = $val_vars['row_title'];
							$vars[] = $val_vars;
						}
					}
					else
					{
						$vars['start_datetime'] = "2000-01-01 00:00:00";
						$vars['end_datetime'] = "3000-00-00 00:00:00";
					}
				}
			}

			if(trim($this->report_selection_column)!="")
			{
				$selection_column_selected = memvar("column_select","");

				$report_tablename = $this->report_read['table'];
				$col_parts = explode(" as ",$this->report_selection_column);
				$col_id = trim($col_parts[0]);
				if(count($col_parts) > 1)
				{
					$col_name = trim($col_parts[1]);
				}
				else
				{
					$col_name = $col_id;
				}

				$output .= "<br>";
				$output .= "<select onchange='$this->click_to_page(\"" . $this->innerurl("mode=$mode&column_select=") . "\" + encodeURI( this.value ) )'>";
				$output .= "<option value=''>--- Choose ---</option>";
				$col_select_query = rpt_query("select [1] as `id`, [2] as `name` from `[3]`",$col_id,$col_name,$report_tablename);
				while($col_select_read = mysqli_fetch_assoc($col_select_query))
				{
					$col_title = $col_select_read['name'];
					$col_id = $col_select_read['id'];
					$output .= "<option value='$col_id'";
					if($col_id==$selection_column_selected)
						$output .= " selected";
					$output .= ">$col_title</option>";
				}

				$output .= "</select>";
				$vars['selectionid'] = $selection_column_selected;
			}
			if ($custom_fname=='get_tip_sharing') {
			    if (isset($_REQUEST['sharingType']) && $_REQUEST['sharingType']!="") {
			    	$sharingType = $_REQUEST['sharingType'];
			    }
			    if (isset($_REQUEST['sharingShowType']) && $_REQUEST['sharingShowType']!="") {
			    	$sharingShowType = $_REQUEST['sharingShowType'];
			    }

			    $selectedEMP = "selected='selected'";
			    $selectedShowTotals = "selected='selected'";

			    if ($sharingType == 'EmployeeClass') {
			    	$selectedClass = "selected='selected'";
			    } else if ($sharingType == 'ByEmployee'){
			    	$selectedEMP = "selected='selected'";
			    }

			    if ($sharingShowType == 'ShowAll') {
			    	$selectedShowAll = "selected='selected'";
			    } else if ($sharingShowType == 'ShowTotals') {
			    	$selectedShowTotals = "selected='selected'";
			    }
			    $firstDate = '';
			    if (isset($vars['start_datetime']) && $vars['start_datetime']!="") {
			    	$firstDate = date_create($vars['start_datetime']);
			    }
			    $secondDate = '';
				$dayCount = 0;
				if (isset($vars['end_datetime']) && $vars['end_datetime']!="") {
					$secondDate = date_create($vars['end_datetime']);
					$interval   = date_diff($firstDate, $secondDate);
					$dayCount   = $interval->format('%a');
				}

				$empStyle = 'padding: 4px 5px;border: 1px solid #888888;float: right;';
				$displayStatus = '';
				$main = '';
				if ($mode_select == "year" || $mode_select == "month") {
					$empStyle .= 'margin-right: 7%;';
					$main = 'float: left; width:100%;';
				} else {
					$empStyle .= 'margin-left: 3px;';
					if ($dayCount == 0 ) {
						$displayStatus = 'display: none;';
					}
				}
				$showStyle = $empStyle.$displayStatus;
			    $output .= "<div class='input_wrapper' style='$main'>";
			    $output .= "<div class='input_wrapper' style='$empStyle'>";
			    $output .= "<select style='width:106%;' id ='typeOption' name = 'typeOption' onchange='$this->click_to_page(\"" . $this->innerurl("mode=$mode&sharingShowType=$sharingShowType&sharingType=") . "\" + encodeURI( this.value ) )'><option value = 'ByEmployee' $selectedEMP >By Employee</option><option value = 'EmployeeClass' $selectedClass>Employee Class</option></select>";
			    $output .= "</div>";
			    $output .= "<div id='show_totals' class='input_wrapper' style='$showStyle'>";
			    $output .= "<select style='width:106%;' id ='typeOption' name = 'typeOption' onchange='$this->click_to_page(\"" . $this->innerurl("mode=$mode&sharingType=$sharingType&sharingShowType=") . "\" + encodeURI( this.value ) )'><option value = 'ShowTotals' $selectedShowTotals >Show Totals Only</option><option value = 'ShowAll' $selectedShowAll>Show All</option></select>";
			    $output .= "</div>";
			    $output .= "</div>";
			}
			$output .= "</div>";
			adjustStartAndEndTimes($vars);
			adjustStartAndEndTimes($vars_next);
			adjustStartAndEndTimes($vars_next2);
			return array("output"=>$output,"vars"=>$vars,"vars_next"=>$vars_next,"vars_next2"=>$vars_next2,"repeat"=>$repeat,"id"=>$this->report_id . "_" . $mode,"mode"=>$mode);
		}

		public function reportFullName(){
			$link = urlencode($this->drillvar_query_string());
			$this->favorites = loadFavoriteReports( $this->report_id );
			$already_saved = false;
			if( $this->favorites ){
				foreach( $this->favorites as $fav_key => $favorite ){
					if( $favorite['link'] == urldecode( $link ) ){
						$already_saved = true;
						return $favorite['name'];
					}
				}
			}

			$result = $this->report_title;
			$sister_select = sister_report::sister( 'select' );
			if( $sister_select ){
				foreach( $sister_select->options() as $key => $option ){
					if( reqvar("sister_select", '') == $option || $option == $sister_select->value() ){
						$displayOption = $option;
						if( '#' == $option[0] ){
							$alias = report_alias::alias( substr($option, 1 ) );
							$displayOption = $alias->label();
						}
						$result .= ' ' . speak('show'). ' ' . speak($displayOption);
					}
				}
			}

			for($r=0; $r<count($this->drillvars); $r++) {
				$filter_col = explode(".", $this->drillvars[$r]['filter_cols'][0]);
				$filter_col = $filter_col[ count( $filter_col ) - 1 ];
				$result .= ' with ' . $filter_col . '=' . $this->drillvars[$r]['filter_vals'][0];
			}

			return $result;
		}

		public function breadcrumbs(){
			$output = '';
			global $data_name;
			$kiosk_enabled_query = mlavu_query("SELECT `modules` FROM `poslavu_MAIN_db`.`restaurants` WHERE `data_name` = '[1]'", $data_name);
			$data = mysqli_fetch_assoc($kiosk_enabled_query);

			$sister_output = '';
			$sister_where   = sister_report::sister( 'where' );
			$mode = getvar("mode");
			if( $sister_where ){
				foreach( $sister_where->options() as $key => $option ){
					$selected ='';
					if( reqvar("sister_where", '') == $option || $option == $sister_where->value() ){
						$selected = ' selected';
					}

					$displayOption = $option;
					$encodedOption = urlencode( $option );
					if( '#' == $option[0] ){
						$alias = report_alias::alias( substr($option, 1 ) );
						$displayOption = str_replace('_', ' ', $alias->label() );
					}

					$sister_sel = reqvar( 'sister_select', '' );
					$sister_gro = reqvar( 'sister_groupby', '' );

					$sister_sel = ($sister_sel) ? '&sister_select=' . urlencode( $sister_sel ) : '';
					$sister_gro = ($sister_gro) ? '&sister_groupby=' . urlencode( $sister_gro ) : '';

					if(substr($option,0,4)=="alt:")
					{
						$allow_alternate_report_links = true;
						if($allow_alternate_report_links)
						{
							$displayOption = str_replace("alt:","",$displayOption);
							$displayName = str_replace("_"," ",$displayOption);
							$sister_output .= "<div{$selected} class='sub_nav_button' onclick=\"click_to_page('sister_where={$encodedOption}{$sister_sel}{$sister_gro}&mode={$mode}&alternate_report=$displayOption', false)\">".speak($displayName)."</div>";
						}
					}
					else
					{
							$displayName = str_replace("_"," ",$displayOption);
							$sister_output .= "<div{$selected} class='sub_nav_button' onclick=\"click_to_page('sister_where={$encodedOption}{$sister_sel}{$sister_gro}&mode={$mode}', false)\">".speak($displayName)."</div>";
					}
				}
			}

			$sister_select  = sister_report::sister( 'select' );
			$sister_groupby = sister_report::sister( 'groupby' );

			if( $sister_select ){
				foreach( $sister_select->options() as $key => $option ){
					$selected ='';
					if( reqvar("sister_select", '') == $option || $option == $sister_select->value() ){
						$selected = ' selected';
					}
					if ($option == '#kiosk_order' && (strpos($data['modules'], 'kiosk') == false)) {
						$option = str_replace("#kiosk_order", "", $option);
					}
					$displayOption = $option;
					if ($option == '#lavu_togo_order' && (strpos($data['modules'], 'lavutogo') == false)) {
					    $option = str_replace("#lavu_togo_order", "", $option);
					}
					$encodedOption = urlencode( $option );
					if( '#' == $option[0] ){
						$alias = report_alias::alias( substr($option, 1 ) );
						$displayOption = str_replace('_', ' ', $alias->label() );
					}

					if($option=="#combo_sales"){
					    $displayOption = "combo sales";
					}
					
					$sister_whe = reqvar( 'sister_where', '' );

					$sister_whe = ($sister_whe) ? '&sister_where=' . urlencode( $sister_whe ) : '';
					$option = trim($option);
					if ( $option !="" ) {
					    if(substr($option,0,4)=="alt:")
					    {
					        $allow_alternate_report_links = true;
					        if($allow_alternate_report_links)
					        {
					            $displayOption = str_replace("alt:","",$displayOption);
					            $show_displayOption = str_replace("alt_","",$displayOption);
					            $displayNameVal = str_replace("_", " ", $show_displayOption);
					            $sister_output .= "<div{$selected} class='sub_nav_button' onclick=\"click_to_page('sister_select={$encodedOption}{$sister_sel}{$sister_gro}&mode={$mode}&alternate_report=$displayOption', false)\">".speak($displayNameVal)."</div>";
					        }
					    }
					    else
					    {
					        $displayNameVal = str_replace("_", " ", $displayOption);
					        $sister_output .= "<div{$selected} class='sub_nav_button' onclick=\"click_to_page('sister_select={$encodedOption}{$sister_whe}&sister_groupby={$encodedOption}&mode={$mode}', false)\">".speak($displayNameVal)."</div>";
					    }
					}
				}
			}

			// Start Automatically assign custom sister list for all ABI chain accounts when looking at FiscalZReconciliation Report
			if(isset($this->report_read) && isset($this->report_read['name']) && $this->report_read['name']=="FiscalZReconciliation")
			{
				global $data_name;
				$company_id = admin_info("companyid");
				$fiscalCountryQuery = ers_query("SELECT `value` FROM `config` WHERE `setting` = 'business_country' AND `location` = '[1]'", $company_id);
				$fiscalCountryName = mysqli_fetch_assoc($fiscalCountryQuery);
				$countryName = $fiscalCountryName['value'];

				$rest_query = mrpt_query("select `chain_name` from `poslavu_MAIN_db`.`restaurants` where `data_name`='[1]'",$data_name);
				if(mysqli_num_rows($rest_query) || $countryName == "Paraguay" || $countryName == "Israel")
				{
					$rest_read = mysqli_fetch_assoc($rest_query);
					$chain_name = $rest_read['chain_name'];
					global $modules;
					if(substr($chain_name,0,3)=="ABI" || $modules->hasModule("reports.fiscal.uruguay") || $countryName == "Paraguay" || $countryName == "Israel")
					{
						$list_query = mrpt_query("select `custom_sister_list` from `poslavu_MAIN_db`.`reports_v2` where `name` LIKE 'fiscal_accounting'");
						if(mysqli_num_rows($list_query))
						{
							$list_read = mysqli_fetch_assoc($list_query);
							$this->report_read['custom_sister_list'] = $list_read['custom_sister_list'];
						}
					}
				}
			}
			// End Automatically assign custom sister list for all ABI chain accounts when looking at FiscalZReconciliation Report

			if(isset($this->report_read) && isset($this->report_read['custom_sister_list']))
			{
				$cslist = explode(",",$this->report_read['custom_sister_list']);
				for($csn=0; $csn<count($cslist); $csn++)
				{
					$option = $cslist[$csn];
					//$sister_output .= "option: $option ";
					$selected ='';

					if( reqvar("sister_select", '') == $option){
						$selected = ' selected';
					}

					$displayOption = $option;
					$encodedOption = urlencode( $option );					
					
					$allow_alternate_report_links = true;
					if($allow_alternate_report_links)
					{
						$displayOption = str_replace("alt:","",$displayOption);
						$show_displayOption = str_replace("alt_","",$displayOption);
						$title_query = mlavu_query("select `title` from `poslavu_MAIN_db`.`reports_v2` where `name` LIKE '[1]'",$show_displayOption);
						if(mysqli_num_rows($title_query))
						{
							$title_read = mysqli_fetch_assoc($title_query);
							$show_displayOption = speak($title_read['title']);
						}
						$displayNameVal = str_replace("_", " ", $show_displayOption);
						$sister_sel = isset($sister_sel)?$sister_sel:"";
						$sister_gro= isset($sister_gro)?$sister_gro:"";
						$sister_output .= "<div{$selected} class='sub_nav_button' onclick=\"click_to_page('sister_select={$encodedOption}{$sister_sel}{$sister_gro}&mode={$mode}&alternate_report=$displayOption', false)\">".speak($displayNameVal)."</div>";
					}
				}
			}

			$this->favorites = loadFavoriteReports( $this->report_id );

			if( $this->favorites && count( $this->favorites ) || $sister_output ){
				$output .=  "<div class='reporting_sub_navigation'>" . $sister_output;
			}

			if( $this->favorites && count( $this->favorites ) ){
				for( $i = 0; $i < count( $this->favorites ); $i++ ){
					$name = $this->favorites[$i]['name'];
					$link = $this->favorites[$i]['link'];
					if( $link == $this->drillvar_query_string() ){
						//On this Page
						if( !(isset($_GET['delete']) && $_GET['delete']) ){
							$output .= "<div class='sub_nav_button' selected>".$name."</div>";
						}
					} else {
						$output .= "<div class='sub_nav_button' onclick=\"click_to_page('{$link}&mode={$mode}', false)\">".$name."</div>";
					}
				}
				$output .=  "</div>";
			}

			$link = urlencode($this->drillvar_query_string());
			$already_saved = false;
			if( $this->favorites ){
				foreach( $this->favorites as $fav_key => $favorite ){
					if( $favorite['link'] == urldecode( $link ) ){
						if( isset( $_GET['delete'] ) && $_GET['delete'] ){
							if( deleteFavorite( $favorite['id'] )){
								header('Location: '.$_SERVER['REQUEST_URI'].'&delete=0');
								die;
							}
						}
						$already_saved = true;
						break;
					}
				}
			}

			if( isset( $_GET['save'] ) && $_GET['save'] ){
				$name = $_REQUEST['name'];
				$link = $_REQUEST['link'];
				if( saveFavoriteReport( $this->report_id, $name, $link ) ){

					header('Location: '.$_SERVER['REQUEST_URI'].'&save=0');
					die;
				}
			}
			$enableRemove = (isset($this->drillvars) && isset($this->drillvars[0]) || ( $sister_select && $sister_select->value() != $sister_select->defaultValue() ))?'':'disabled';

				$last_drillvar_subreport = $this->drillvars[count($this->drillvars) - 1]['subreport'];
				if( strlen($last_drillvar_subreport) && '@' === $last_drillvar_subreport[0] ){
					$this->verify_rj_loaded(true);
					$this->rj->verify_aliases_are_loaded();
					$alias = report_alias::alias( substr( $last_drillvar_subreport, 1 ) );
					$last_drillvar_subreport = $alias->label();
				}
				$ldparts = explode(".",$last_drillvar_subreport);
				if(count($ldparts) > 1)
				{
					if(trim($ldparts[1])=="name" || trim($ldparts[1])=="id" || trim($ldparts[1])=="title")
						$show_filter_by = $ldparts[0];
					else
						$show_filter_by = $ldparts[1];
				} else {
					$show_filter_by = $ldparts[0];
				}
				$show_filter_by = ucfirst(speak(str_replace("_"," ",$show_filter_by)));
				$mode = getvar('mode', 'day');
				$output .= "<br /><div class=\"breadcrumbs_stripe\">";
				$output .= "<button {$enableRemove} class=\"rounded_button left remove_filters\" onclick='click_to_page(\"".$this->innerurl("mode=$mode",false)."\")'>";
				if(count($this->drillvars) > 1){
					$output .= speak("Remove Filters");
				}else{
					$output .= speak("Remove Filter");
				}
				$output .= "</button>";



				$export_enabled = true;
				if($export_enabled)
				{
					$sharingType =  (isset($_REQUEST['sharingType'])) && ($_REQUEST['sharingType']!='') ? $_REQUEST['sharingType'] : '' ;
					$sharingParam = ($sharingType != '') ? '&sharingType='.$sharingType : '';
					
					$sharingShowType =  (isset($_REQUEST['sharingShowType'])) && ($_REQUEST['sharingShowType']!='') ? $_REQUEST['sharingShowType'] : '' ;
					$sharingShowParam = ($sharingShowType != '') ? '&sharingShowType='.$sharingShowType : '';
					
					$dvar_str = $this->drillvar_query_string();
					$report_vars = "report=".$this->report_id."&mode=".getvar("mode")."&output_mode=csv".$sharingParam.$sharingShowParam;
					if($dvar_str!="") $report_vars .= "&" . $dvar_str;
					$report_export_url = add_to_url( get_export_url(), $report_vars );
					
					$output .= $this->getDownloadToCsvButton($report_export_url);
					$report_printable_url = "?mode=reports_reports_v2&time_mode=".getvar("mode")."&report=".$this->report_id."&printable=1".$sharingParam;
					if($dvar_str!="") $report_printable_url .= "&" . $dvar_str;
					$output .= "<button class='rounded_button right printable' border='0' onclick=\"printable('".$report_printable_url."')\"><div class='icon'></div>".speak("Printable")."</button>";
				}

				if( $this->drillvar_query_string() ){ //It's Savable!
					$mode = getvar('mode');
					if( !$already_saved ){
						$output .= "<button class='rounded_button right addFavorites' onclick=\"(function(){var name=prompt('Please Provide a Name for this Favorite',''); if( name ){click_to_page('".$this->innerurl("save=1")."&mode={$mode}&report={$this->report_id}&link={$link}&name=' + encodeURIComponent(name) );}})()\"><div class='icon'></div>".speak("Save to Favorites")."</button>";
					} else {
						//Delete Link
						$output .= "<button class='rounded_button right removeFavorites' onclick=\"click_to_page('".$this->innerurl("delete=1")."&mode={$mode}')\"><div class='icon'></div>".speak("Remove From Favorites")."</button>";
					}
				}

				"&#9734;";


				$output .= "<div class=\"breadcrumbs large\">";
				$output .= "<div class=\"right_arrow large\">&#x25B6;</div>";
				$output .= "<a class=\"breadcrumb\" onclick='click_to_page(\"".$this->innerurl("mode={$mode}&".$this->drillvar_query_string(-1),false)."\")' >".speak(urldecode($this->report_title))."</a>";
				for($r=0; $r<count($this->drillvars); $r++) {
					$checkTitleCol = $this->drillvars[0]['title_col']; 
					$output .= "<div class=\"right_arrow\">&#x25B6;</div>";
					$title = '';
					if (empty($this->drillvars[$r]['title']) && empty( $this->drillvars['subreport'] ) ) {
					    $this->drillvars[$r]['title'] = str_replace("[_dsep_]", " ", $_REQUEST['filter_vals']);
					}
					if(empty($this->drillvars[$r]['title']) && empty( $this->drillvars['subreport'] ) ){
						$title .= '</b><i>('.speak("empty").')</i><b>';
					} else {
						if(empty($this->drillvars[$r]['title'])){
							$title_part = explode(chr(30), $this->drillvars['subreport']);
							$title_part = $title_part[0];
							$title = speak($title_part);
						} else {
							$title_part = explode(chr(30), $this->drillvars[$r]['title']);
							$title_part = $title_part[0];
							if(in_array($this->drillvars[$r]['subreport'], array('@action_device_uuid', '@order_cashier', '@transaction_action', '@card_type', '@order_server', '@transaction_payment_register', '@rev_center', '@meal_period', '@trans_occur_by_hour')) && in_array($this->drillvars[$r]['title'], array('Refund', 'Sale', 'Cash', 'Card', 'Gift Certificate', 'EGC', 'Visa', 'AMEX', 'MasterCard', 'DinersClub', 'enRoute', 'Discover', 'JCB', 'Debit', 'EBT', 'WAX', 'JCB', 'Voyager', 'cp') ) ) {
								if (preg_match("/^[a-z \/]+$/i", $title_part)) {
									$title .= speak($title_part);
								} else {
									$title .= $title_part;
								}

							} else if(in_array($checkTitleCol, array('action', 'payment_type', 'order_closed_weekday', 'item_added_hour', 'device', 'revenue_center', 'weekday', 'category', 'super_group', 'order_item', 'order_server', 'order_cashier', 'meal_period', 'register', 'order_type', 'order_id', 'content_options', 'register_group', 'ingredient_name') ) && in_array($this->drillvars[$r]['title'], array('Refund', 'Sale', 'Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December') ) && in_array($this->drillvars[$r]['subreport'], array('@payment_type_name', '@order_server', '@card_type', '@order_register', '@closed_by_hour', '@transaction_payment_register', '@rev_center', '@meal_period', '@trans_occur_by_hour', '@order_cashier', '@closed_by_month', '@order_tag', '@closed_by_hour', '@closed_by_month', '@trans_occur_by_week_of_year', '@trans_occur_by_year', '@menu_category_name', '@contents_name', '@order_id', '@closed_by_year', '@item_added_by_week_for_year', '@item_added_by_year', '@closed_by_week_for_year', '@closed_actual_date_hour', '@trans_occur_by_month', '@payment_type_name', '@item_added_by_day') ) ) {
								if (preg_match("/^[a-z \/]+$/i", $title_part)) {
										$title .= speak($title_part);
								} else {
									$title .= $title_part;
								}
							} else if($checkTitleCol == 'action' && in_array($this->drillvars[$r]['subreport'], array('@action_details', '@action_time', '@action_device_udid', '@action_user') ) ) {
								if (preg_match("/^[a-z \/]+$/i", $title_part)) {
										$title .= speak($title_part);
								} else {
									$title .= $title_part;
								}
							} else if($checkTitleCol == 'card_type' && in_array($this->drillvars[$r]['subreport'], array('@payment_type_name', '@rev_center', '@action_device_uuid', '@order_server', '@card_type', '@order_cashier', '@trans_occur_by_day', '@trans_occur_actual_date_hour', '@trans_occur_by_week_of_year', '@trans_occur_by_year'))) {
								if (preg_match("/^[a-z \/]+$/i", $title_part)) {
										$title .= speak($title_part);
								} else {
									$title .= $title_part;
								}
							} else if($checkTitleCol == 'order_cashier' && in_array($this->drillvars[$r]['subreport'], array('@payment_type_name', '@rev_center', '@action_device_uuid', '@card_type', '@order_cashier', '@trans_occur_by_day', '@trans_occur_actual_date_hour', '@trans_occur_by_week_of_year', '@trans_occur_by_year'))) {
								if (preg_match("/^[a-z \/]+$/i", $title_part)) {
										$title .= speak($title_part);
								} else {
									$title .= $title_part;
								}
							} else if($checkTitleCol == 'order_server' && in_array($this->drillvars[$r]['subreport'], array('@payment_type_name', '@rev_center', '@action_device_uuid', '@order_server', '@card_type', '@trans_occur_by_day', '@trans_occur_actual_date_hour', '@trans_occur_by_week_of_year', '@trans_occur_by_year'))) {
								if (preg_match("/^[a-z \/]+$/i", $title_part)) {
										$title .= speak($title_part);
								} else {
									$title .= $title_part;
								}
							} else {
								$title .= $title_part;
							}
						}
					}
					$date_setting = "date_format";
					$location_date_format_query = lavu_query("SELECT `location`, `setting`, `value` FROM `config` WHERE `setting` = '[1]'", $date_setting);
					if (! $location_date_format_query || ! mysqli_num_rows($location_date_format_query)) {
						$date_option = array(
								"value" => 2
						);
					} else {
						$date_option = mysqli_fetch_assoc($location_date_format_query);
					}
					if(strpos($title, "Merged") !== false && strpos($title, "#") === false){
							$format_title = explode("Order", $title);
							$title = trim($format_title[0])." Order #".trim($format_title[1]);
					}
					if(strpos($title, "time(#year)") !== false){
						$format_title = explode("time(#year)", trim($title));
						$title = $format_title[1];
					}
					if(strpos($title, "time(#month)") !== false){
						$format_title = explode("time(#month)", trim($title));
						$title = $format_title[1];
					}
					if(strpos($title, "time(#weekday)") !== false){
						$format_title = explode("time(#weekday)", trim($title));
						$title = $format_title[1];
					}
					if(strpos($title, "time(h:ia)") !== false){
						$format_title = explode("time(h:ia)", trim($title));
						$title = $format_title[1];
					}
					if(strpos($title, "time(#weekforyear)") !== false){
						$format_title = explode("time(#weekforyear)", trim($title));
						$title = $format_title[1];
					}
					if(strpos($title, "[number]") !== false){
						$format_title = explode("[number]", trim($title));
						$title = $format_title[1];
					}
					if(strpos($title, "time(#day)") !== false){
						$format_title = explode("time(#day)", trim($title));
						$split_date = explode(")", $format_title[1]);
						$change_format = explode("-", $split_date[0]);
						switch ($date_option['value']) {
							case 1:
								$title = trim($change_format[1]) . "-" . trim($change_format[0]);
								break;
							case 3:
								$title = trim($change_format[0]) . "-" . trim($change_format[1]);
								break;
							case 2:
							default:
								$title = trim($change_format[0]) . "-" . trim($change_format[1]);
						}
					}
					if(strpos($title, "[date]time(") !== false){
						// Actual Format [date]time(Y-m-d H:i:s)2017-05-17 20:00:00
						$format_title = explode("[date]", trim($title)); // time(Y-m-d H:i:s)2017-05-17 20:00:00
						$split_date_format = explode(")", $format_title[1]); // 2017-05-17 20:00:00
						$change_date = explode(" ", $split_date_format[1]); // 2017-05-17
						$change_format = explode("-", $change_date[0]); // date 2017 05 17
						$formatter = explode("(", $split_date_format[0]); // Y-m-d H:i:s
						$date_format = explode(" ", $formatter[1]); // Y-m-d
						$split_date = explode("-", $date_format[0]); // format Y m d
						switch ($date_option['value']) {
							case 1:
								$title = trim($change_format[2]) . "-" . trim($change_format[1]) . "-" . trim($change_format[0]) . " " . $change_date[1];
								break;
							case 3:
								$title = trim($change_format[0]) . "-" . trim($change_format[1]) . "-" . trim($change_format[2]) . " " . $change_date[1];
								break;
							case 2:
							default:
								$title = trim($change_format[1]) . "-" . trim($change_format[2]) . "-" . trim($change_format[0]) . " " . $change_date[1];
								
						}
						
					}
					if(strpos($title, "[date]") !== false && !preg_match('/time.*/', $title)){
						$format_title = explode("[date]", trim($title));
						$split_date = explode(" ", $format_title[1]);
						if(count($split_date) == 2){
							$change_format = explode("-", $split_date[0]);
							switch ($date_option['value']) {
								case 1:
									$title = trim($change_format[2]) . "-" . trim($change_format[1]) . "-" . trim($change_format[0]) . " " . $split_date[1];
									break;
								case 3:
									$title = trim($change_format[0]) . "-" . trim($change_format[1]) . "-" . trim($change_format[2]) . " " . $split_date[1];
									break;
								case 2:
								default:
									$title = trim($change_format[1]) . "-" . trim($change_format[2]) . "-" . trim($change_format[0]) . " " . $split_date[1];
							}
						}else{
							$title = str_replace("[date]", "", trim($title));
						}
					}
					if(strpos($title, "[date]") !== false && preg_match('[date]time', $title) === true){
						$format_title = explode("[date]", trim($title));
						$split_date = explode(" ", $format_title[1]);
						$change_format = explode("-", $split_date[0]);
						switch ($date_option['value']) {
							case 1:
								$title = trim($change_format[2]) . "-" . trim($change_format[1]) . "-" . trim($change_format[0]) . " " . $split_date[1];
								break;
							case 3:
								$title = trim($change_format[0]) . "-" . trim($change_format[1]) . "-" . trim($change_format[2]) . " " . $split_date[1];
								break;
							case 2:
							default:
								$title = trim($change_format[1]) . "-" . trim($change_format[2]) . "-" . trim($change_format[0]) . " " . $split_date[1];
						}
					}
					if($r === (count( $this->drillvars )-1) ){
						$title .= " ".speak("by"). " {$show_filter_by}";
					}
					$title = urldecode($title);
					$output .= "<a class=\"breadcrumb\" onclick='click_to_page(\"".$this->innerurl("mode={$mode}&".$this->drillvar_query_string($r),false)."\")' >{$title}</a>";

					if($r === (count( $this->drillvars )-1) && allow_special_access() ){
						$output .= "<a class=\"breadcrumb\" onclick='click_to_page(\"".$this->innerurl("mode={$mode}&show_query&".$this->drillvar_query_string($r),false)."\")' style='color: #fefefe;'>(*with query)</a>";
					}
				}

				if( !count($this->drillvars) && allow_special_access() ){
					$output .= "<a class=\"breadcrumb\" onclick='click_to_page(\"".$this->innerurl("mode={$mode}&show_query&".$this->drillvar_query_string(-1),false)."\")' style='color: #fefefe;'>(*with query)</a>";
				}
				$output .= "</div>";
				$output .= "</div>";
			// }
			return $output;
		}

		public function getDownloadToCsvButton($params) {
			$button = "<button class='rounded_button right export' ";
			$defaultAction = "onclick='downloadReport(\"".$params."\")'";
			$button .= $defaultAction;
			$button .= "><div class='icon'></div>".speak("Export To CSV")."</button>";
			return $button;
		}

		public function show_report_description()
		{
			$description_str = "";
			$sister_output = '';
			$sister_where   = sister_report::sister( 'where' );
			$mode = getvar("mode");
			if( $sister_where ){
				foreach( $sister_where->options() as $key => $option ){
					$selected ='';
					if( reqvar("sister_where", '') == $option || $option == $sister_where->value() ){
						$selected = ' selected';
					}

					$displayOption = $option;
					$encodedOption = urlencode( $option );
					if( '#' == $option[0] ){
						$alias = report_alias::alias( substr($option, 1 ) );
						$displayOption = str_replace('_', ' ', $alias->label() );
					}

					$sister_sel = reqvar( 'sister_select', '' );
					$sister_gro = reqvar( 'sister_groupby', '' );

					$sister_sel = ($sister_sel) ? '&sister_select=' . urlencode( $sister_sel ) : '';
					$sister_gro = ($sister_gro) ? '&sister_groupby=' . urlencode( $sister_gro ) : '';

					if(substr($option,0,4)=="alt:")
					{
						$sister_output .= "<div{$selected} class='sub_nav_button' onclick=\"click_to_page('sister_where={$encodedOption}{$sister_sel}{$sister_gro}&mode={$mode}', false)\">".$displayOption."</div>";
					} else {
						$sister_output .= "<div{$selected} class='sub_nav_button' onclick=\"click_to_page('sister_where={$encodedOption}{$sister_sel}{$sister_gro}&mode={$mode}', false)\">".speak($displayOption)."</div>";
					}
					if($selected==" selected")
					{
						$displayOption = str_replace("alt:","",$displayOption);
						$description_str .= $displayOption." ";
					}
				}
			}

			$sister_select  = sister_report::sister( 'select' );
			$sister_groupby = sister_report::sister( 'groupby' );

			if( $sister_select ){
				foreach( $sister_select->options() as $key => $option ){
					$selected ='';

					if( reqvar("sister_select", '') == $option || $option == $sister_select->value() ){
						$selected = ' selected';
					}

					$displayOption = $option;
					$encodedOption = urlencode( $option );
					if( '#' == $option[0] ){
						$alias = report_alias::alias( substr($option, 1 ) );
						$displayOption = str_replace('_', ' ', $alias->label() );
					}

					$sister_whe = reqvar( 'sister_where', '' );

					$sister_whe = ($sister_whe) ? '&sister_where=' . urlencode( $sister_whe ) : '';

					$sister_output .= "<div{$selected} class='sub_nav_button' onclick=\"click_to_page('sister_select={$encodedOption}{$sister_whe}&sister_groupby={$encodedOption}&mode={$mode}', false)\">".speak($displayOption)."</div>";
					if($selected == ' selected')
					{
						$description_str .= $displayOption . " ";
					}
				}
			}

				$last_drillvar_subreport = $this->drillvars[count($this->drillvars) - 1]['subreport'];
				if( strlen($last_drillvar_subreport) && '@' === $last_drillvar_subreport[0] ){
					$this->verify_rj_loaded(true);
					$this->rj->verify_aliases_are_loaded();
					$alias = report_alias::alias( substr( $last_drillvar_subreport, 1 ) );
					$last_drillvar_subreport = $alias->label();
				}
				$ldparts = explode(".",$last_drillvar_subreport);
				if(count($ldparts) > 1)
				{
					if(trim($ldparts[1])=="name" || trim($ldparts[1])=="id" || trim($ldparts[1])=="title")
						$show_filter_by = $ldparts[0];
					else
						$show_filter_by = $ldparts[1];
				} else {
					$show_filter_by = $ldparts[0];
				}
				$show_filter_by = ucfirst(speak(str_replace("_"," ",$show_filter_by)));

			$output = "";
			//start LP-4462 date format setting
			// based on date format setting
			$date_setting = "date_format";
			$location_date_format_query = lavu_query( "SELECT `location`, `setting`, `value` FROM `config` WHERE `setting` = '[1]'",  $date_setting);
			if( !$location_date_format_query || !mysqli_num_rows($location_date_format_query)){
				$date_option = array("value" => 2);
			}else
				$date_option = mysqli_fetch_assoc( $location_date_format_query );

			for($r=0; $r<count($this->drillvars); $r++) {
				$output .= "<div class=\"right_arrow\">&#x25B6;</div>";
				$title = '';
				if(empty($this->drillvars[$r]['title']) && empty( $this->drillvars['subreport'] ) ){
					$title .= '</b><i>('.speak("empty").')</i><b>';
				} else {
					if(empty($this->drillvars[$r]['title'])){
						$title_part = explode(chr(30), $this->drillvars['subreport']);
						$title_part = $title_part[0];
						$title = $title_part;
					} else {
						$title_part = explode(chr(30), $this->drillvars[$r]['title']);
						$title_part = $title_part[0];
						$title .= $title_part;
					}
				}
				if(strpos($title, "Merged") !== false && strpos($title, "#") === false){
						$format_title = explode("Order", $title);
						$title = trim($format_title[0])." Order #".trim($format_title[1]);
				}
				if(strpos($title, "time(#year)") !== false){
					$format_title = explode("time(#year)", trim($title));
					$title = $format_title[1];
				}
				if(strpos($title, "time(#month)") !== false){
					$format_title = explode("time(#month)", trim($title));
					$title = $format_title[1];
				}
				if(strpos($title, "time(#weekday)") !== false){
					$format_title = explode("time(#weekday)", trim($title));
					$title = $format_title[1];
				}
				if(strpos($title, "time(h:ia)") !== false){
					$format_title = explode("time(h:ia)", trim($title));
					$title = $format_title[1];
				}
				if(strpos($title, "time(#weekforyear)") !== false){
					$format_title = explode("time(#weekforyear)", trim($title));
					$title = $format_title[1];
				}
				if(strpos($title, "time(h:ia)") !== false){
					$format_title = explode("time(h:ia)", trim($title));
					$title = $format_title[1];
				}
				if(strpos($title, "[number]") !== false){
					$format_title = explode("[number]", trim($title));
					$title = $format_title[1];
				}
				if(strpos($title, "time(#day)") !== false){
					$format_title = explode("time(#day)", trim($title));
					$split_date = explode(")", $format_title[1]);
					$change_format = explode("-", $split_date[0]);
					switch ($date_option['value']) {
						case 1:
							$title = trim($change_format[1]) . "-" . trim($change_format[0]);
							break;
						case 3:
							$title = trim($change_format[0]) . "-" . trim($change_format[1]);
							break;
						case 2:
						default:
							$title = trim($change_format[0]) . "-" . trim($change_format[1]);
					}
				}
				if(strpos($title, "[date]time(") !== false){
					// Actual Format [date]time(Y-m-d H:i:s)2017-05-17 20:00:00
					$format_title = explode("[date]", trim($title)); // time(Y-m-d H:i:s)2017-05-17 20:00:00
					$split_date_format = explode(")", $format_title[1]); // 2017-05-17 20:00:00
					$change_date = explode(" ", $split_date_format[1]); // 2017-05-17
					$change_format = explode("-", $change_date[0]); // date 2017 05 17
					$formatter = explode("(", $split_date_format[0]); // Y-m-d H:i:s
					$date_format = explode(" ", $formatter[1]); // Y-m-d
					$split_date = explode("-", $date_format[0]); // format Y m d
					switch ($date_option['value']) {
						case 1:
							$title = trim($change_format[2]) . "-" . trim($change_format[1]) . "-" . trim($change_format[0]) . " " . $change_date[1];
							break;
						case 3:
							$title = trim($change_format[0]) . "-" . trim($change_format[1]) . "-" . trim($change_format[2]) . " " . $change_date[1];
							break;
						case 2:
						default:
							$title = trim($change_format[1]) . "-" . trim($change_format[2]) . "-" . trim($change_format[0]) . " " . $change_date[1];
							
					}
					
				}
				if(strpos($title, "[date]") !== false && !preg_match('/time.*/', $title)){
					$format_title = explode("[date]", trim($title));
					$split_date = explode(" ", $format_title[1]);
					if(count($split_date) == 2){
						$change_format = explode("-", $split_date[0]);
						switch ($date_option['value']) {
							case 1:
								$title = trim($change_format[2]) . "-" . trim($change_format[1]) . "-" . trim($change_format[0]) . " " . $split_date[1];
								break;
							case 3:
								$title = trim($change_format[0]) . "-" . trim($change_format[1]) . "-" . trim($change_format[2]) . " " . $split_date[1];
								break;
							case 2:
							default:
								$title = trim($change_format[1]) . "-" . trim($change_format[2]) . "-" . trim($change_format[0]) . " " . $split_date[1];
						}
					}else{
						$title = str_replace("[date]", "", trim($title));
					}
				}
				if(strpos($title, "[date]") !== false && preg_match('[date]time', $title) === true){
					$format_title = explode("[date]", trim($title));
					$split_date = explode(" ", $format_title[1]);
					$change_format = explode("-", $split_date[0]);
					switch ($date_option['value']) {
						case 1:
							$title = trim($change_format[2]) . "-" . trim($change_format[1]) . "-" . trim($change_format[0]) . " " . $split_date[1];
							break;
						case 3:
							$title = trim($change_format[0]) . "-" . trim($change_format[1]) . "-" . trim($change_format[2]) . " " . $split_date[1];
							break;
						case 2:
						default:
							$title = trim($change_format[1]) . "-" . trim($change_format[2]) . "-" . trim($change_format[0]) . " " . $split_date[1];
					}
				}
				if($r === (count( $this->drillvars )-1) ){
					$title .= " " . speak("by") . " {$show_filter_by}";
				}
				$title = urldecode($title);
				$output .= "<a class=\"breadcrumb\" onclick='click_to_page(\"".$this->innerurl("mode={$mode}&".$this->drillvar_query_string($r),false)."\")' >{$title}</a>";

				if($r === (count( $this->drillvars )-1) && allow_special_access() ){
					$output .= "<a class=\"breadcrumb\" onclick='click_to_page(\"".$this->innerurl("mode={$mode}&show_query&".$this->drillvar_query_string($r),false)."\")' style='color: #fefefe;'>(*with query)</a>";
				}
			}

			$output = "<b>" . ucwords(trim($description_str)) . "</b> " . $output;
			$output .= "<br>";

			$selector_result = $this->datetime_selector();
			if($this->allow_controls)
				$selector_output = $selector_result['output'];
			else
				$selector_output = "";
			$selector_vars = $selector_result['vars'];
			$selector_vars_next = $selector_result['vars_next'];
			$selector_vars_next2 = $selector_result['vars_next2'];
			$selector_repeat = $selector_result['repeat'];
			$selector_id = $selector_result['id'];
			$selector_mode = $selector_result['mode'];

			if (isset($selector_vars['start_datetime']) || isset($selector_vars['start_date'])) {
                                if ($selector_vars['start_datetime'] != "" && $selector_vars['end_datetime'] != "") {
                                        $start_datetime = $selector_vars['start_datetime'];
                                        $end_datetime = $selector_vars['end_datetime'];
                                } else {
                                        $start_datetime = $selector_vars['start_date']." ".$selector_vars['start_time'];
                                        $end_datetime = $selector_vars['end_date']." ".$selector_vars['end_time'];
                                }

				if($selector_mode=="year")
				{
					$output .= speak("Full Year").": " . $this->show_parsed_date($start_datetime,"Y");
				}
				else if($selector_mode=="month")
				{
					$output .= speak("Full Month").": " . $this->show_parsed_date($start_datetime,"M Y");
				}
				else if($selector_mode=="day")
				{
						switch ($date_option['value'])
						{
							case 1:$dateFormat = 'd/m/Y';
							break;
							case 2:$dateFormat = 'm/d/Y';
							break;
							case 3:$dateFormat = 'Y/m/d';
							break;
							default:$dateFormat = 'm/d/Y';
						}
						//End LP-4462 date format setting
						if($this->show_parsed_date($start_datetime,$dateFormat)!=$this->show_parsed_date($end_datetime,$dateFormat)) // m/d/y replace with $dateFormat
						{
							$output .= speak("Day Range").": " . $this->show_parsed_date($start_datetime,$dateFormat) . " - " . $this->show_parsed_date($end_datetime,$dateFormat); // m/d/y replace with $dateFormat
						}
						else
						{
							$output .= speak("Full Day").": " . $this->show_parsed_date($start_datetime,$dateFormat);// m/d/y replace with $dateFormat
						}
				}
				else
				{
					$output .= $this->show_parsed_date($start_datetime,"$dateFormat h:i a") . " - " . $this->show_parsed_date($end_datetime,"$dateFormat h:i a") . "<br>";// m/d/y replace with $dateFormat
				}
			}
			return $output;
		}

		public function show_parsed_date($dt,$fmt)
		{
			$dt_parts = explode(" ",$dt);
			if(count($dt_parts) > 1)
			{
				$tm_parts = explode(":",$dt_parts[1]);
				$dt_parts = explode("-",$dt_parts[0]);
				if(count($dt_parts) > 2 && count($tm_parts) > 2)
				{
					$ts = mktime($tm_parts[0],$tm_parts[1],$tm_parts[2],$dt_parts[1],$dt_parts[2],$dt_parts[0]);
					return date($fmt,$ts);
				}
			}
			return $dt;
		}

		public function process_extra_code_for_row($vars,$report_read,$special_code)
		{
			$vars['row'] = $report_read;

			$report_store = new area_store(array("order_id"=>"0"));
			$result = $report_store->apply_rules($vars,false,"build_report",$special_code, $this);
			return array_merge( $report_read, $result );
			//run_internal_subreport($run_report,$run_vars)
		}

		public function should_show_col($colname)
		{
			if(substr($colname,0,9)=="group_col") return false;
			else return true;
		}

		public function output_export_string($filename,$str)
		{
			$known_mime_types=array(
				"pdf" => "application/pdf",
				"txt" => "text/plain",
				"html" => "text/html",
				"htm" => "text/html",
				"exe" => "application/octet-stream",
				"zip" => "application/zip",
				"doc" => "application/msword",
				"xls" => "application/vnd.ms-excel",
				"ppt" => "application/vnd.ms-powerpoint",
				"gif" => "image/gif",
				"png" => "image/png",
				"jpeg"=> "image/jpg",
				"jpg" =>  "image/jpg",
				"php" => "text/plain",
				"csv" => "text/csv"
			);

			if (isset($_GET['email']) && $_GET['email'] != '' && isset($_GET['output_mode']) && $_GET['output_mode'] == 'csv')
			{
                $fileName = explode(".",$filename);
                $filename = $fileName[0].date('_Y-m-d_H:i:s_').rand().".".$fileName[1];
			}

			$file_extension = strtolower(substr(strrchr($filename,"."),1));
			$mime_type = (isset($known_mime_types[$file_extension]))?$known_mime_types[$file_extension]:$file_extension;

			header("Content-Type: $mime_type; charset=UTF-8");
			header('Content-Disposition: attachment; filename="'.$filename.'"');
			header("Content-Transfer-Encoding: binary");
			header('Accept-Ranges: bytes');

			header("Cache-control: private");
			header('Pragma: private');
			header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");

			header("Content-Length: ".strlen($str));
			$str =  chr(239).chr(187).chr(191). $str;

			if (isset($_GET['email']) && $_GET['email'] != '' && isset($_GET['output_mode']) && $_GET['output_mode'] == 'csv')
            {
                $relativePath = __DIR__ . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR  . 'images' .DIRECTORY_SEPARATOR .'abi_fiscal_exports';

                if (!file_exists($relativePath))
                {
                    mkdir($relativePath, 0777, true);
                }

                global $data_name;
                $relativePath .= DIRECTORY_SEPARATOR . $data_name;

                if (!file_exists($relativePath))
                {
                    mkdir($relativePath, 0777, true);
                }

                $myfile = fopen($relativePath .  DIRECTORY_SEPARATOR . $filename, "w");
                chmod ($relativePath .  DIRECTORY_SEPARATOR . $filename, 0777);
                file_put_contents($relativePath . DIRECTORY_SEPARATOR . $filename, $str);

                $urlPath = (isset($_SERVER['HTTPS']) ? "https" : "http") . "://".$_SERVER['HTTP_HOST']. DIRECTORY_SEPARATOR . 'images' .DIRECTORY_SEPARATOR .'abi_fiscal_exports';

                $headers = "Content-type: text/plain; charset=iso-8859-1"."\r\n";
                $headers .= "From: Lavu POS <receipts@lavusys.com>"."\r\n";

                mail($_GET['email'], $filename . ' '. speak('Report'), speak('Please download the file ').'@ '.$urlPath. DIRECTORY_SEPARATOR . $data_name . DIRECTORY_SEPARATOR . $filename, $headers);
                lavu_exit();
            }

            echo $str;
		}

		public function reorder( $report_row1, $report_row2 ){
			$orderby_parts = explode(' ', $this->orderby );
			$orderby = $orderby_parts[0];
			$direction = isset( $orderby_parts[1] ) ? $orderby_parts[1] : 'asc';

			if( !isset( $report_row1[0][$orderby] ) || !isset( $report_row2[0][$orderby] ) ){
				return 0;
			}
			
			$value1 = $report_row1[0][$orderby];
			$value2 = $report_row2[0][$orderby];

			if( report_value_is_money( $value1 ) ){
				$value1 = report_money_to_number( $value1 );
			} else if( report_value_is_number( $value1 ) ){
				$value1 = report_number_to_float( $value1 );
			}

			if( report_value_is_money( $value2 ) ){
				$value2 = report_money_to_number( $value2 );
			} else if( report_value_is_number( $value2 ) ){
				$value2 = report_number_to_float( $value2 );
			}

			if( $value1 == $value2 ){
				return 0;
			}

			if( $direction == 'asc' ){
				if( $value1 < $value2){
					return -1;
				} else {
					return 1;
				}
			} else {
				if( $value1 < $value2){
					return 1;
				} else {
					return -1;
				}
			}
			return 0;
		}

		//public function combineReportResults( &$report_results, &$temp_report_results, $separate ){
		public function combineReportResults( &$report_results, &$temp_report_results, $separate, $reportCountry = "", $tempReportCountry = "", $conversionDate = "0") {

			global $location_info;
			$monetary_symbol = $location_info['monitary_symbol'];
			$monetaryQuery = ers_query("SELECT * FROM `locations` WHERE `id` = '1' ");
			if(mysqli_num_rows($monetaryQuery) > 0)
			{
				$monetaryArray = mysqli_fetch_assoc($monetaryQuery);
				if(isset($monetaryArray['monitary_symbol'])){
					$monetary_symbol = $monetaryArray['monitary_symbol'];
				}
				else{
					$monetary_symbol = $monetaryArray[0]['monitary_symbol'];
				}
			}
			foreach ($temp_report_results as $temp_report_row_index => $temp_report_row) {
			    foreach ($temp_report_row as $key => $coloumn_values) {
			        foreach($coloumn_values as $colKey => $colValue) {
						if (report_value_is_money($colValue)) {
			           		if($separate == '1') {
				                $companyId = sessvar("admin_companyid");
				                $primaryResIdData = getCompanyInfo($companyId, 'id');
				                $primaryResChainId = (!empty($primaryResIdData['chain_id']))?$primaryResIdData['chain_id']: null;
				                $temp_report_row[$colKey] = report_float_to_money(convertCurrency($reportCountry, $tempReportCountry, report_money_to_number($colValue), $conversionDate, $primaryResChainId));
			            	}else {
			                	$temp_report_row[$colKey] = report_float_to_money(report_money_to_number($colValue), $monetary_symbol);
			                }
			            $temp_report_results[$temp_report_row_index][0][$colKey] = $temp_report_row[$colKey];
			    		}       
				 	}
				}
			}
			if(count($report_results) > 0){
                $index = count( $report_results ) - 1;
			}
			else{
                $index = 0;
            }
			if( !$separate ){
				for( $i = 0; $i < count( $report_results[$index] ); $i++ ){
					$report_row = $report_results[$index][$i];
					if( !is_array( $report_row ) ){
						continue;
					}
					
					foreach( $temp_report_results as $temp_report_row_index => $temp_report_row ){
						$match = true;
						
						foreach( $report_row as $fieldName => $fieldValue ){
							if( substr( $fieldName, 0, 10 ) == 'group_col_' ){
								continue;
							}
							if( !(report_value_is_money( $fieldValue ) || report_value_is_number( $fieldValue )) ){
								$match = $match && $report_row[$fieldName] == $temp_report_row[$fieldName];
							}
						}
						
						if(!$match) { 
							if ($this->groupby_cols['0'] != 'order_contents.item'){
								#code added by Ravi Tiwari on 1st April 2019
								$res = $this->checkDuplicateCategory($report_results[$index],$temp_report_row[0]);
								if ($res) {
									unset( $temp_report_results[$temp_report_row_index ]);
								}
							}
							continue;
						}

						foreach( $report_row[0] as $fieldName => $fieldValue ) {
							$reportRowValue = $fieldValue;
							$tempReportRowValue = $temp_report_row[0][$fieldName];
							if( report_value_is_money( $reportRowValue ) ){
							    $report_results[$index][$i][0][$fieldName] = report_float_to_money( report_money_to_number( $reportRowValue  ) +  report_money_to_number($tempReportRowValue));
							} else if ( report_value_is_number( $reportRowValue ) ){
							    $report_results[$index][$i][0][$fieldName] = report_float_to_number( report_number_to_float( $reportRowValue ) + report_number_to_float($tempReportRowValue));
							} else {
								continue;
							}
						}
						unset( $temp_report_results[$temp_report_row_index ]);
					}
				}
			} else if( $separate != 0 ) {
				$report_results[] = array();
				$index = count( $report_results ) - 2;

				$report_results['location_names'][$index] = get_location_name();
				if($separate == 2){
				    $index = count( $report_results ) - 3;
					$report_results['location_names'][$index] = get_location_name();
					if(!isset($report_results['location_currencies'])){
					    $report_results['location_currencies'] = array();
					}
					$report_results['location_currencies'][$index] = $monetary_symbol;
   				}
   				
			}

			foreach( $temp_report_results as $key => $value ){
				if( !is_array( $value )) {
					continue;
				}

				$report_results[$index][] = $value;
				unset( $temp_report_results[$key] );
			}

		}

		private function preorder_results($report_results,$orderby)
		{
			$dir = "asc";
			if(strpos($orderby," desc")!==false)
			{
				$orderby = trim(str_replace(" desc","",$orderby));
				$dir = "desc";
			}
			if(isset($report_results[0][0][0][$orderby]))
			{
				$arr = $this->sort_array_by_property($report_results,$orderby,"",true);
				if($dir=="desc") $arr = array_reverse($arr);
				return $arr;
			}
			else return $report_results;
		}

		private function sort_array_by_property($arr,$col,$col2="",$allow_duplicate=false)
		{
			$newarr = array();
			for($i=0; $i<count($arr); $i++)
			{
				if($allow_duplicate)
					$extra_key = "I" . $i;
				else
					$extra_key = "";
				if($col2!="")
					$newarr["S" . strtolower($arr[0][$i][0][$col]) . "S" . strtolower($arr[0][$i][0][$col2]) . $extra_key] = $arr[$i];
				else
				{
					$arrcolval = $arr[0][$i][0][$col];
					if(substr($arrcolval,0,7)=="[money]")
					{
						$arrcolval = 100000000 + (substr($arrcolval,7) * 1);
					}
					if(substr($arrcolval,0,8)=="[number]")
					{
						$arrcolval = 100000000 + (substr($arrcolval,8) * 1);
					}
					$newarr["S" . strtolower($arrcolval) . $extra_key] = $arr[$i];
				}
			}
			ksort($newarr);

			$arr = array();
			foreach($newarr as $k => $v)
			{
				$arr[] = $v;
			}
			return $arr;
		}

		public function get_report_info_from_getvar($colname="")
		{
			$reportid = (isset($_REQUEST['report']))?$_REQUEST['report']:0;
			$alternate_report = (isset($_REQUEST['alternate_report']))?$_REQUEST['alternate_report']:"--";
			if($alternate_report=="--")
			{
				if(isset($_SESSION['altreport_for_'.$reportid]))
					$alternate_report = $_SESSION['altreport_for_'.$reportid];
				else
					$alternate_report = "";
			}
			else
			{
				$_SESSION['altreport_for_'.$reportid] = $alternate_report;
			}

			if(isset($this->remember_report_read))
			{
				$report_read = $this->remember_report_read;
			}
			else
			{
				if ($alternate_report != "")
				{
					$report_query = mlavu_query("select * from `poslavu_MAIN_db`.`reports_v2` where `name`='[1]'",$alternate_report);
				}
				else
				{
					$report_query = mlavu_query("select * from `poslavu_MAIN_db`.`reports_v2` where `id`='[1]'",$reportid);
				}
				if(mysqli_num_rows($report_query))
				{
					$report_read = mysqli_fetch_assoc($report_query);
					$this->remember_report_read = $report_read;
				}
			}
			if(isset($report_read) && isset($report_read['id']))
			{
				if($colname!="")
				{
					if(isset($report_read[$colname]))
						return $report_read[$colname];
					else
						return "";
				}
				else return $report_read;
			}
			return false;
		}
		private function getReportCountry() {
			$companyLocationDataQuery = "select * from `config` where `type`='location_config_setting' and `setting` like '%primary_currency_code%'";
			$companyLocationResults = ers_query($companyLocationDataQuery);
			if(mysqli_num_rows($companyLocationResults) >0 ){
				$companyLocationData = mysqli_fetch_assoc($companyLocationResults);
				if(isset($companyLocationData['value'])){
					return $companyLocationData['value2'];
				}
				else if(isset($companyLocationData[0]['value'])){
					return $companyLocationData[0]['value2'];
				}
			}
			return "USA"; //Default to the USA Currency code.
		}

		private function get_report_data( $i_index, $orderby, $vars_for_main_query, $vars_for_secondary_query ) {
				global $location_info;
				$company_id = admin_info("companyid");
				$reporting_groups = $this->chain_reporting_groups();
				$reportCountry = $this->getReportCountry();
				$fiscalCountryName['value'] = $location_info['business_country'];
				$conversionDate = isset($vars_for_main_query[0]['end_datetime'])?$vars_for_main_query[0]['end_datetime']:$vars_for_main_query[0]['start_datetime'];
				if ($this->report_read['special_code']!="" && strtolower(substr($this->report_read['special_code'],0,9))=="//custom:") { // specialized reports using php code
					$custom_fname_str = substr($this->report_read['special_code'],9);
					$custom_fname_parts = explode("\n",$custom_fname_str);
					$custom_fname = trim(str_replace("..","",$custom_fname_parts[0]));
				if ($custom_fname!="" && is_file(dirname(__FILE__) . "/custom_reports/" . $custom_fname . ".php")) {
					require_once(dirname(__FILE__) . "/custom_reports/" . $custom_fname . ".php");
					$start_vars = $vars_for_main_query[$i_index];
					$report_info = $this->get_report_info_from_getvar();
					$start_vars['report_name'] = $report_info['name'];

					if ($reporting_groups['multi_report'] != '0') {
						$chain_comp_id = implode(',',$reporting_groups['rests']);
						$currentCompanyID = admin_info("companyid");
						$currentSession = $_COOKIE['PHPSESSID'];
						$_report_results_next = array();
						$this->report_results = array();
						for($j=0; $j<count($reporting_groups['rests']); $j++ )
						{
							$company_id = $reporting_groups['rests'][$j];
							if(!connect_to_chain_database($company_id))
							{
								continue;
							}
							changeActiveChainAccount($company_id);
							session_write_close(); // commit the session changes so they persist through the API call.
							$next_report_results = $custom_fname($start_vars);
							$next_report_results = $this->preorder_results($next_report_results,$orderby);
							$reportdata[]=$next_report_results;
							$tempReportCountry = $this->getReportCountry();
							session_start($currentSession); //restart the session so everything works as expected
						}

						$counter = count($reportdata);
						$combinedDataArray = array();
						$topLevelCounter=0;
						for($eachArray=0; $eachArray<$counter; $eachArray++){
						    $firstLevelArray = $reportdata[$eachArray][0];
						    $firstLevelCounter = count($firstLevelArray);
						    for($firstLevelIndex=0; $firstLevelIndex<$firstLevelCounter; $firstLevelIndex++){
						        $combinedDataArray[$topLevelCounter][0]=$firstLevelArray[$firstLevelIndex][0];
						        $topLevelCounter++;
						    }
						}
						$this->combineReportResults( $this->report_results, $combinedDataArray , $reporting_groups['seperate_locations'], $reportCountry, $tempReportCountry, $conversionDate );

						changeActiveChainAccount($currentCompanyID);
						connect_to_chain_database( $currentCompanyID );
						$_COOKIE['PHPSESSID']=$currentSession;
					} else {
						$report_results = $custom_fname($start_vars);
						$report_results = $this->preorder_results($report_results,$orderby);

						if (count($report_results) > 0) {
						    $this->report_results = $report_results;
						} else {
						    $this->report_results[0]=$report_results;
						}
					}
				}
			} else if ($this->report_read['special_code']!="" && strtolower(substr($this->report_read['special_code'],0,8))!="//append") { // specialized reports using php code
				$report_store = new area_store(array("order_id"=>"0"));
				$start_vars = $vars_for_main_query[$i_index];
				$file_link=explode(":",$this->report_read['special_code']);
				$file_name=$file_link[1];
				$results = $report_store->apply_rules($start_vars,false,"build_report",$this->report_read['special_code'], $this);
				if( area_reports::maxArrayDepth( $results ) > 2 ) { // multi location report, accept full results, instead of just assuming it's one set
					if($file_name=="end_of_day"){
					    $this->report_results = $results;
					}
					else{
					    $this->report_results[0] = $results;
					}
				}
				else {
					$this->report_results[0] = $results;
				}
			} else {// reports using a database query (this is the normal way to do it)
				$start_datetime = $vars_for_main_query[0][start_datetime];
				$end_datetime = $vars_for_main_query[0][end_datetime];
				$levels = array(3, 4);
				$order_ids = array();
				if( $reporting_groups['multi_report'] != '0' ) { //multi_query
					//Load Restaurant Database
					//Run Query
					//Combine Query Results using information from the reporting_groups
					$start_vars = $vars_for_main_query[$i_index];
					$start_vars['report_name'] = 'FiscalZReconciliation';
					$chain_comp_id = implode(',',$reporting_groups['rests']);
					$report_data = array();
					$tempReportCountry = $this->getReportCountry();
					if (isset($_GET['show_query'])) {
						echo $query . '1111<br />';
					}

					$temp_report_results = array();
					/*  get seperate location data for 'lavu_gift_and_loyalty'  report*/
					if ($reporting_groups['seperate_locations'] == 1 && $this->report_name == TYPE_LAVU_GIFT_AND_LOYALITY) {
						for ($j = 0;$j < count($reporting_groups['rests']);$j++) {
							if(!connect_to_chain_database($reporting_groups['rests'][$j])) {
								continue;
							}
							$chainId = $reporting_groups['rests'][$j];
							$chaninLocationDataname = "SELECT  `dataname` ,`title` as locationName FROM `poslavu_MAIN_db`.restaurant_locations  WHERE restaurantid in ('".$chainId."')";// get chain dataname based on chain id
								$result = rpt_query($chaninLocationDataname);
                while ($row = mysqli_fetch_assoc($result)) {
								$sessionOrderBy = sessvar(strtolower('order location by - '.$row['locationName']));
								$locationWiseOrderBy = $sessionOrderBy ? $sessionOrderBy : $orderby;
								$query = $this->query_string($vars_for_main_query[$i_index], $locationWiseOrderBy, $row['dataname']); // main query
                            	$query = preg_replace("/and\\s+`?orders`?\\.`?location_id`?\\s*=\\s*'?\\d+'?/", '', $query);
								if ($this->prepareQueriesForEmailReport) {
									$this->queries[$row['dataname']] = $query;
								} else {
									$result = rpt_query($query);
									while ($report_read = mysqli_fetch_assoc($result)) {
										foreach ( $report_read as $k => $v ) {
											if ( report_value_is_number( $v ) && $k != 'dataname' && $k != 'account_number') {
												$report_read[$k] = "[number]{$v}";
											}
										}
										$temp_report_results[][] = $report_read;
									}
								}
							}
							if(!$this->prepareQueriesForEmailReport) {
								$this->combineReportResults( $this->report_results, $temp_report_results, $reporting_groups['seperate_locations'], $reportCountry, $tempReportCountry, $conversionDate );
							}
						}
                    } else {
						if ($reporting_groups['multi_report'] != '0') {
						$this->verify_rj_loaded(true);
						}
						$query = $this->query_string($vars_for_main_query[$i_index],$orderby); // Process main
						$query = preg_replace("/and\\s+`?orders`?\\.`?location_id`?\\s*=\\s*'?\\d+'?/", '', $query);
						if ($this->v2report_name == "FiscalZReconciliation") {
							if (in_array($fiscalCountryName['value'], array('Paraguay', 'Israel')) && $this->v2report_name == 'FiscalZReconciliation' && in_array(intval(sessvar("admin_access_level")), $levels)) {
								// Skip invoice number column for Israel, It is not required for Israel fiscal report
								$invoiceNumber = '';
								$customerName = '';
								$guestCount = '';
								if (!in_array($fiscalCountryName['value'], array('Israel'))) {
									$invoiceNumber = "`o`.`invoice_number` AS 'Fiscal Invoice Number', ";
									$customerName = "concat(`mc`.`l_name`,' ',`mc`.`f_name`) AS 'Customer Name', ";
									$guestCount = ", `o`.`guests` AS 'Guest Count'";
								}
								$query = "SELECT `l`.`title` AS 'Location Name', " . $customerName . $invoiceNumber . " `o`.`closed` AS 'Order Date', concat('[money]', format(cast(`o`.`subtotal` AS decimal(30, 5)),2)) AS 'Subtotal', concat('[money]',format(sum( IF(`cc`.`action`='Refund', -1*`cc`.`total_collected`, `cc`.`total_collected`) ),2)) as 'Total', `o`.`order_id` AS 'Order ID' " . $guestCount . " FROM `orders` as `o` LEFT JOIN `med_customers` AS `mc` on `mc`.`id` = SUBSTRING_INDEX(SUBSTRING_INDEX(`o`.`original_id`, '|', 7),'|',-1) INNER JOIN `locations` AS `l` on `o`.`location_id` = `l`.`id` INNER JOIN `cc_transactions` AS `cc` on `cc`.`order_id`=`o`.`order_id` WHERE `o`.`void` = 0 AND `o`.`closed` >= '$start_datetime' AND `o`.`closed` < '$end_datetime' group by `o`.`order_id`";
								$reporting_groups['rests'] = connect_to_fiscal_restaurant($reporting_groups['rests']);
							}
						}
						$_report_results_next = array();
						$arrOfRows = array();
						$gross_value1=0;
						$quantity_sum=$order_count_sum=$gross_sum=$discount_sum=$net_sum=$order_tax_sum=$included_tax_sum=0;
						$flag = 1;
						for( $j = 0; $j < count( $reporting_groups['rests'] ); $j++ ) {
							$company_id = $reporting_groups['rests'][$j];
							if( !connect_to_chain_database( $company_id ) ) {
								continue;
							}
							$tempReportCountry = $this->getReportCountry();
							if( isset($_GET['show_query'])) {
								echo $query . '1111<br />';
							}
							$display_extra="";
							$report_query = rpt_query($query);
							if( !$report_query ) {
								continue;
							}
							$temp_report_results = array();
							while( $report_read = mysqli_fetch_assoc( $report_query ) ) {
								/*LP-11769: Fixed the rounding issue in sale report (Group report)*/
								if ($this->report_name == 'sales' || $this->report_name == 'super_groups' || $this->report_name == 'categories') {
									if(isset($report_read['tgross'])){
										$report_read['gross'] = "[money]{$report_read['tgross']}";
										unset($report_read['tgross']);
									}
									if(isset($report_read['tnet'])){
										$report_read['net'] = "[money]{$report_read['tnet']}";
										unset($report_read['tnet']);
									}
								}
								foreach( $report_read as $k => $v ) {
									if ($k == "Order ID") {
										$order_ids[] = $v;
									}
									if( report_value_is_number( $v ) && substr($v,0,7) != '[money]') {
										$report_read[$k] = "[number]{$v}";
									}
								}
								if($this->report_read['special_code']!="") {
									$report_read = $this->process_extra_code_for_row($vars_for_main_query[$i_index],$report_read,$this->report_read['special_code']);
								}

								$temp_report_results[][] = $report_read;
							}
							if (in_array($fiscalCountryName['value'], array('Paraguay', 'Israel')) && $this->v2report_name == 'FiscalZReconciliation' && in_array(intval(sessvar("admin_access_level")), $levels)) {
								$taxIdFlag = 1;
								if ($fiscalCountryName['value'] == 'Israel') {
									$taxIdFlag = 0;
								}
								$temp_report_results = getTaxProfileNames($order_ids, $temp_report_results, $flag, $reporting_groups['seperate_locations'], $globalTaxes, $taxIdFlag);
							}
							$this->combineReportResults( $this->report_results, $temp_report_results, $reporting_groups['seperate_locations'], $reportCountry, $tempReportCountry, $conversionDate );
						}

						if (in_array($fiscalCountryName['value'], array('Paraguay', 'Israel')) && in_array(intval(sessvar("admin_access_level")), $levels)) {
	                        if ($reporting_groups['seperate_locations'] == 0) {
	                        $loopAllTaxes = array();
		                        foreach ($globalTaxes as $key => $value) {
		                                if (count($value)>0) {
		                                        foreach ($value as $k => $v) {
		                                             $loopAllTaxes[$k] = $v;
		                                        }
		                                }
		                        }
	                        $this->report_results = updateTaxesForAllLocations($loopAllTaxes, $this->report_results);
							}
						}
						if(is_array($vars_for_secondary_query) && count($vars_for_secondary_query) > 0) {// Process additional query (like for previous year)
							$query_next = $this->query_string($vars_for_secondary_query[$i_index]);
							$temp_report_results_next = array();
							$report_query_next = rpt_query($query_next);
							while($report_read_next = mysqli_fetch_assoc($report_query_next))
							{
								foreach( $report_read as $k => $v ) {
									if( report_value_is_number( $v ) && substr($v,0,7) != '[money]') {
										$report_read[$k] = "[number]{$v}";
									}
								}
								if($this->report_read['special_code']!="") {
									$report_read_next = $this->process_extra_code_for_row($vars_for_secondary_query[$i_index],$report_read_next,$this->report_read['special_code']);
								}
								$temp_report_results_next[] = $report_read_next;
							}
							$this->combineReportResults( $_report_results_next, $temp_report_results_next, $reporting_groups['seperate_locations'] , $reportCountry, $tempReportCountry, $conversionDate );
						}
					}
					connect_to_chain_database( admin_info("companyid") );			
				} else {
					$start_vars = $vars_for_main_query[$i_index];
					$start_vars['report_name'] = 'FiscalZReconciliation';
					global $start_date;
					global $end_date;
					global $start_time;
					global $end_time;
					global $filtermode;
					$selector_result = $this->datetime_selector();
					$start_date= $start_vars['start_date'];
					$end_date= $start_vars['end_date'];
					$start_time= $start_vars['start_time'];
					$end_time= $start_vars['end_time'];
					$filtermode = $selector_result['mode'];
					if(isset($start_vars['start_datetime']) && !empty($start_vars['start_datetime'])) {
						$startDateTimeToArray = explode(' ', $start_vars['start_datetime']);
						$start_date = $startDateTimeToArray[0];
						$start_time = $startDateTimeToArray[1];
					}

					if(isset($start_vars['end_datetime']) && !empty($start_vars['end_datetime'])) {
						$endDateTimeToArray = explode(' ', $start_vars['end_datetime']);
						$end_date = $endDateTimeToArray[0];
						$end_time = $endDateTimeToArray[1];
					}
					$query = $this->query_string($vars_for_main_query[$i_index],$orderby); // Process main query
					if ($this->v2report_name == "FiscalZReconciliation") {
    						$fiscal_config_query = ers_query("select * from `config` where setting='fiscal_integration'");
    						$fiscal_config_read = mysqli_fetch_assoc($fiscal_config_query);
    						$fiscal_config_value = json_decode($fiscal_config_read['value']);
							if(isset($start_vars['start_datetime']) && !empty($start_vars['start_datetime'])) {
								$startDateTimeToArray = explode(' ', $start_vars['start_datetime']);
								$start_date = $startDateTimeToArray[0];
								$start_time = $startDateTimeToArray[1];
							}

							if(isset($start_vars['end_datetime']) && !empty($start_vars['end_datetime'])) {
								$endDateTimeToArray = explode(' ', $start_vars['end_datetime']);
								$end_date = $endDateTimeToArray[0];
								$end_time = $endDateTimeToArray[1];
							}
							if($start_date != '' && $end_date != '') {
								if ($filtermode == 'day') {
								//Check If Time Selected & End Time Greater Than Start Time
								if ($start_time != $end_time && $end_time > $start_time) {
								//fiscal_datetime
								$fiscal_datetime = "(DATE(fiscal_reports.datetime) BETWEEN '$start_date' AND '$end_date') AND (TIME(fiscal_reports.datetime) BETWEEN '$start_time' AND '$end_time')";
								} else {
								$fiscal_datetime = "fiscal_reports.datetime >= '$start_datetime' and fiscal_reports.datetime <= '$conversionDate'";
								}
								} else {
								$fiscal_datetime = "fiscal_reports.datetime >= '$start_datetime' and fiscal_reports.datetime <= '$conversionDate'";
								}
							}
    						$country= $fiscal_config_value->country;
    						global $modules;
    						if ($modules->hasModule("reports.fiscal.uruguay")) {
								$query="select fiscal_reports.report_type as 'Report Type', fiscal_reports.first_invoice as 'First Invoice', fiscal_reports.last_invoice as 'Last Invoice', fiscal_reports.report_id as 'Z Report ID', fiscal_reports.fiscal_day as 'Date (YYMMDD)', concat('[money]',format(cast(fiscal_reports.net_total AS decimal(15,5)),2)) as 'Net Total', concat('[money]',format(cast(fiscal_reports.tax  AS decimal(15,5)),2)) as 'Tax', concat('[money]',format(cast( fiscal_reports.total AS decimal(15,5)),2)) as Total, concat('[money]',format(cast( fiscal_reports.refund_net AS decimal(15,5)),2)) as 'Refund Net', concat('[money]',format(cast( fiscal_reports.refund_tax  AS decimal(15,5)),2)) as 'Refunded Tax', concat('[money]',format(cast( fiscal_reports.refund_total AS decimal(15,5)),2)) as 'Refund Total', concat('[money]',format(cast( fiscal_reports.untax_total AS decimal(15,5)),2)) as 'Untaxed Total', concat('[money]',format(cast( fiscal_reports.untax_refund AS decimal(15,5)),2)) as 'Untaxed Refund Total' from `fiscal_reports` where $fiscal_datetime and fiscal_reports.report_id != '' order by fiscal_reports.report_id asc";
							}
							if (in_array($fiscalCountryName['value'], array('Paraguay', 'Israel')) && $this->v2report_name == 'FiscalZReconciliation' && in_array(intval(sessvar("admin_access_level")), $levels)) {
								// Skip invoice number column for Israel, It is not required for Israel fiscal report
								$invoiceNumber = '';
								$customerName = '';
								$guestCount = '';
								if (!in_array($fiscalCountryName['value'], array('Israel'))) {
									$invoiceNumber = "`o`.`invoice_number` AS 'Fiscal Invoice Number', ";
									$customerName = "concat(`mc`.`l_name`,' ',`mc`.`f_name`) AS 'Customer Name', ";
									$guestCount = ", `o`.`guests` AS 'Guest Count'";
								}
								$query = "SELECT `l`.`title` AS 'Location Name', " . $customerName . $invoiceNumber . " `o`.`closed` AS 'Order Date', concat('[money]', format(cast(`o`.`subtotal` AS decimal(30, 5)),2)) AS 'Subtotal', concat('[money]',format(sum( IF(`cc`.`action`='Refund', -1*`cc`.`total_collected`, `cc`.`total_collected`) ),2)) as 'Total', `o`.`order_id` AS 'Order ID' " . $guestCount . " FROM `orders` as `o` LEFT JOIN `med_customers` AS `mc` on `mc`.`id` = SUBSTRING_INDEX(SUBSTRING_INDEX(`o`.`original_id`, '|', 7),'|',-1) INNER JOIN `locations` AS `l` on `o`.`location_id` = `l`.`id` INNER JOIN `cc_transactions` AS `cc` on `cc`.`order_id`=`o`.`order_id` WHERE `o`.`void` = 0 AND `o`.`closed` >= '$start_datetime' AND `o`.`closed` < '$end_datetime' group by `o`.`order_id`";
							}
						}
    					if( isset($_GET['show_query'])){
    						echo "QUERY IS :".$query . '<br />';

						}
						if ($this->prepareQueriesForEmailReport) {
							$this->queries[sessvar('admin_dataname')] = $query;
						} else {
							//This query is triggered when making an AJAX request to refresh a report Version 2
							$report_query = rpt_query($query);
							if(!$report_query) {
								if(get_permission_level()==12) {
									$display_extra .= "<table bgcolor='#eeeeff' style='border:solid 1px #aabbcc' cellpadding=6><tr><td style='color:#880000'>" . ers_sql_error() . "</td></tr></table>";
								}
								if($this->display_query=="1") {
									$report_query_info = rpt_query($query);
									$report_query = $report_query_info['result'];
									$display_extra .= "<table bgcolor='#eeeeff' style='border:solid 1px #aabbcc' cellpadding=6><tr><td>" . $query . "</td></tr></table>";
								}
							} else {
								while($report_read = mysqli_fetch_assoc($report_query)) {
									/*LP-11769: Fixed the rounding issue in sale report*/
									if ($this->report_name == 'sales' || $this->report_name == 'super_groups' || $this->report_name == 'categories') {
										if(isset($report_read['tgross'])){
											$report_read['gross'] = "[money]{$report_read['tgross']}";
											unset($report_read['tgross']);
										}
										if(isset($report_read['net'])){
											$report_read['net'] = "[money]{$report_read['tnet']}";
											unset($report_read['tnet']);
										}
									}
									foreach( $report_read as $k => $v ) {
										if ($k == "Order ID") {
											$order_ids[] = $v;
										}
										if (report_value_is_number( $v ) && substr($v,0,7) != '[money]') {
											if ($k == "First Invoice" || $k == "Last Invoice" || $k == "Z Report ID" || $k == "P. V. No"){
												$report_read[$k] = "{$v}";
											} else {
												$report_read[$k] = "[number]{$v}";
											}
										} else if(report_value_is_date($v)){
											$report_read[$k] = "[date]{$v}";
										}
									}
									if ($this->report_read['special_code']!="") {
										$report_read = $this->process_extra_code_for_row($vars_for_main_query[$i_index],$report_read,$this->report_read['special_code']);
									}

									$temp_report_results[][] = $report_read;
								}

								if (in_array($fiscalCountryName['value'], array('Paraguay', 'Israel')) && $this->v2report_name == 'FiscalZReconciliation' && in_array(intval(sessvar("admin_access_level")), $levels)) {
									$taxIdFlag = 1;
									if ($fiscalCountryName['value'] == 'Israel') {
										$taxIdFlag = 0;
									}
									$temp_report_results = getTaxProfileNames($order_ids, $temp_report_results, '', '', $globalTaxes, $taxIdFlag);
								}

								$this->combineReportResults( $this->report_results, $temp_report_results, $reporting_groups['seperate_locations'], $reportCountry, $tempReportCountry, $conversionDate );
								if(is_array($vars_for_secondary_query) && count($vars_for_secondary_query) > 0) { // Process additional query (like for previous year)
									$query_next = $this->query_string($vars_for_secondary_query[$i_0index]);
									$_report_results_next[0] = array();
									$report_query_next = rpt_query($query_next);
									while( $report_read_next = mysqli_fetch_assoc($report_query_next) ) {
										if( $this->report_read['special_code']!="" ) {
											$report_read_next = $this->process_extra_code_for_row($vars_for_secondary_query[$i_index],$report_read_next,$this->report_read['special_code']);
										}
										$_report_results_next[0][] = $report_read_next;
									}
								}
							}
					}
				}
			}

			if( $this->orderby && !empty( $this->orderby ) ){
				usort($this->report_results[0], array( $this, 'reorder' ) );
			}

	}
	/*Author: Ravi Tiwari on 18th Mar 2019
	* Purpose: Check duplicate category and merge respective data
	*/
	function checkDuplicateCategory(&$resultSet,$result){

		$matchFlag = 0;
		$index = 0;
		foreach($resultSet as $resultdata){
			
			if(trim($resultdata[0]['category'])==trim($result['category'])){

				$matchFlag = 1;
				$temp = array();
				foreach( $result as $k => $v ) {

					if (substr($v,0,8) == '[number]') {
						$value1 = explode("[number]",$v);
						$value1[1] = str_replace(",","",$value1[1]);

						$value2 = explode("[number]",$resultdata[0][$k]);
						$value2[1] = str_replace(",","",$value2[1]);

						$resultdata[0][$k] = '[number]'.($value1[1]+$value2[1]);

					} else if(substr($v,0,7) == '[money]'){
						$value1 = explode("[money]",$v);
						$value1[1] = str_replace(",","",$value1[1]);

						$value2 = explode("[money]",$resultdata[0][$k]);
						$value2[1] = str_replace(",","",$value2[1]);

						$resultdata[0][$k] = '[money]'.($value1[1]+$value2[1]);
					}
				}

			}
			#Duplicate category records found
			if($matchFlag){
				$resultSet[$index] = $resultdata;
				break;
			}
			$index ++;
		}
		return $matchFlag;
	}
		private static function maxArrayDepth( $arr ) {
			$max = 0;
			if( !is_array( $arr ) ) {
				return $max;
			}
			foreach( $arr as $_ => $val ) {
				$max = MAX( $max, 1+area_reports::maxArrayDepth( $val ) );
			}

			return $max;
		}

		public function show_query_table()
		{
			if(!isset($this->report_read))
			{
				return "";
			}

			//Date Selector Portions
			{
				$selector_result = $this->datetime_selector();

				if($this->allow_controls)
					$selector_output = $selector_result['output'];
				else
					$selector_output = "";
				$selector_vars = $selector_result['vars'];
				$selector_vars_next = $selector_result['vars_next'];
				$selector_vars_next2 = $selector_result['vars_next2'];
				$selector_repeat = $selector_result['repeat'];
				$selector_id = $selector_result['id'];
				$selector_mode = $selector_result['mode'];

				if(!$selector_repeat) {
					$selector_vars = array($selector_vars);
				}

				if($selector_mode=="month") $inum = date("m");
				else if($selector_mode=="year") $inum = date("Y");
				else $inum = date("d");
				$icount = count($selector_vars) - 1;
				$use_main_row_title_type = "row_title";
			}

			if($this->multiorder=="current_desc") {
				$istart = $inum - 1;
				$iend = 0;
				$istep = -1;
			}
			if($this->multiorder=="current_desc_cycle") {
				$istart = $inum - 1;
				$iend = 0 - $icount + $inum;
				$istep = -1;
				$use_main_row_title_type = "row_title_details";
			}
			else if($this->multiorder=="current_asc") {
				$istart = $inum - 1;
				$iend = $icount;
				$istep = 1;
			}
			else if($this->multiorder=="desc") {
				$istart = $icount;
				$iend = 0;
				$istep = -1;
			}
			else {
				$istart = 0;
				$iend = $icount;
				$istep = 1;
			}

			$reporting_groups = $this->chain_reporting_groups();
			$reporting_groups_ouput = ($reporting_groups && $reporting_groups['display'])?$reporting_groups['display']:'';
			$table_str = "";
			$graph_str = "";
			$display_extra = "";
			$firstcol = "";
			$firstcol_name = "";
			$export_str = "";

			$idone = 0;
			$irec = 0; // prevent infinite loop
			//multiple report iteration
			for($i=$istart; !$idone; $i+=$istep) {
				$irec++;
				if($irec > 10000 || ($istep==-1 && $i <= $iend) || ($istep==1 && $i >= $iend))
					$idone = true;

				$swap_title_for_secondary = false;
				if($i >= 0) {
					$i_index = $i;
					$vars_for_main_query = $selector_vars;
					$vars_for_secondary_query = $selector_vars_next;
				} else {
					$i_index = $icount + $i + 1;
					$vars_for_main_query = $selector_vars_next;
					$vars_for_secondary_query = $selector_vars_next2;
					$swap_title_for_secondary = true;
				}

				$row_title = "";
				$row_title_next = "";
				if(isset($selector_vars[$i_index][$use_main_row_title_type])) {
					$row_title = $selector_vars[$i_index][$use_main_row_title_type];
				}
				if(isset($selector_vars_next[$i_index]) && isset($selector_vars_next[$i_index]['row_title'])) {
					$row_title_next = $selector_vars_next[$i_index]['row_title'];
				}
				if(isset($selector_vars_next[$i_index]) && isset($selector_vars_next[$i_index]['row_title_details'])) {
					$row_title_next_details = $selector_vars_next[$i_index]['row_title_details'];
				}

				if($swap_title_for_secondary) {
					$row_title = $row_title_next_details;
				}

				// set custom order by
				$orderby_var = "orderby_".$selector_id;
				$orderby = memvar($orderby_var,"");
				$this->orderby = $orderby;
				$this->report_results = array( array() );
				$_report_results_next = array( array() );
				
				//Pulling The Data
				$this->get_report_data($i_index, $orderby, $vars_for_main_query, $vars_for_secondary_query);
				if( empty( $this->report_drawer ) ){
					$this->report_drawer = new area_report_drawer( $this );
				}
				//Draw Portions
				{
					$title_str = "";
					$content_str = "<div id=\"details_view\"></div>";
					$export_str = "";
					$table_start = "";
					$table_end = "";
					if(count( $this->report_results ) == 1 && count($this->report_results[0])==0) {// No records found
						$table_str .= $this->report_drawer->draw_no_data();
					}else if(count( $this->report_results ) == 1 && $this->report_results[0]=='email_report') {// Send Report in Email
						$table_str .= $this->report_drawer->draw_email_send_text();
					} else if(count( $this->report_results ) == 1 && count($this->report_results[0])==1) {// Only one record, show vertical presentation
						$table_str .= $this->report_drawer->draw_single_row( $this->report_results );
					} else {// Multiple records (a regular report)						
						$table_str .= $this->report_drawer->draw_multiple_rows( $this->report_results );
					}
				}
			}
			return $table_str;
		}
		
		//LP-5000,new function
		
		public function getDrillDropDown($reportId){

			$report_query = mlavu_query("select `select` as drill from `reports_v2` where `id`='[1]'",$reportId);
		    if(mysqli_num_rows($report_query))
		    {
				$report_read_data = mysqli_fetch_assoc($report_query);
		    }
		    
			$drillValue = explode('[_sep_]drill_down',$report_read_data['drill']);
		    $testStr = 'drill_down'.$drillValue[1];
			$str = $this->process_query_command("drill_down","concat('[drill_down]',[val])",$testStr);
		    
		    $drill_down = false;
		    $report_read=array();
		    if(strpos($str,"[drill_down]")!==false)
		    {
		        
		        $drill_down = true;
		        $strval = explode("[drill_down]",$str);
		        $str = str_replace("','","",$strval[1]);
		        $strval[1] = str_replace("') as filters","",$str);
		        
		        if(count($strval) > 1)
		        {
		            if(isset($this->fwd_drillvars['title']) && $this->fwd_drillvars['title']!="")
		            {
		                $last_drillvar_subreport = trim($this->drillvars[count($this->drillvars) - 1]['subreport']);
		                if( strlen( $last_drillvar_subreport ) && '@' == $last_drillvar_subreport[0] ){
		                    $this->verify_rj_loaded(true);
		                    $this->rj->verify_aliases_are_loaded();
		                    $alias = report_alias::alias( substr( $last_drillvar_subreport, 1 ) );
		                    $last_drillvar_subreport = $alias->statement();
		                }
		                $set_title_col = $last_drillvar_subreport;
		                $group_col_name = "group_col_" . str_replace(".","_",$set_title_col);
		                if(isset($report_read[$group_col_name]))
		                {
		                    $set_drill_title = $report_read[$group_col_name];
		                    $this->fwd_drillvars['last_subreport'] = $group_col_name;
		                    $this->fwd_drillvars['last_drill_title'] = $set_drill_title;
		                } else if( isset( $report_read['group_col_date_formatted'])){
		                    $set_drill_title = $report_read['group_col_date_formatted'];
		                    $this->fwd_drillvars['last_subreport'] = $group_col_name;
		                    $this->fwd_drillvars['last_drill_title'] = $set_drill_title;
		                }
		            }
		            
		            $strreport_parts = explode(",",$strval[1]);
		            if($this->allow_drilldown(count($strreport_parts)))
		            {
		                
		                $use_firstcol = $this->firstcol_name;
		                if($use_firstcol=="") $use_firstcol = $this->firstcol;
		                if($report_read && isset($report_read[$use_firstcol]))
		                {
		                    $show_firstcol_value = $this->processTimeFunction( $report_read[$use_firstcol] );
		                }
		                else $show_firstcol_value = "";
		                $groupstr = "";
		                $filtervals = "";
		                $set_drill_down_start = "";
		                $set_drill_title = urlencode( $show_firstcol_value);
		                $set_title_col = urlencode( $use_firstcol);
		                $set_filter_cols = urlencode( $groupstr);
		                $set_filter_vals = urlencode( $filtervals);
		                if(isset($this->fwd_drillvars['title']))
		                {
		                    $last_drillvar_subreport = $this->drillvars[count($this->drillvars) - 1]['subreport'];
		                    if( strlen( $last_drillvar_subreport ) && '@' == $last_drillvar_subreport[0] ){
		                        $this->verify_rj_loaded(true);
		                        $this->rj->verify_aliases_are_loaded();
		                        $alias = report_alias::alias( substr( $last_drillvar_subreport, 1 ) );
		                        $last_drillvar_subreport = $alias->statement();
		                    }
		                    $set_title_col = urlencode(trim($last_drillvar_subreport) );
		                    $group_col_name = "group_col_" . str_replace(".","_",$set_title_col);
		                    if(isset($report_read[$group_col_name])) {
		                        $set_drill_title = urlencode( $report_read[$group_col_name] );
		                        $this->fwd_drillvars['last_subreport'] = $group_col_name;
		                        $this->fwd_drillvars['last_drill_title'] = $set_drill_title;
		                    } else if( isset( $report_read['group_col_date_formatted'])){
		                        $set_drill_title = $report_read['group_col_date_formatted'];
		                        $set_drill_title = $this->processTimeFunction( $set_drill_title );
		                        $this->fwd_drillvars['last_subreport'] = 'group_col_date_formatted';
		                        $this->fwd_drillvars['last_drill_title'] = $set_drill_title;
		                    }
		                    
		                    $set_drill_title = urlencode( $this->fwd_drillvars['title'] ) . "[_dsep_]" . $set_drill_title;
		                    $set_drill_down_start = urlencode( $this->fwd_drillvars['subreport'] ) . "[_dsep_]";
		                    $set_title_col = urlencode($this->fwd_drillvars['title_col']) . "[_dsep_]" . $set_title_col;
		                    $set_filter_cols = urlencode( $this->fwd_drillvars['filter_cols'] ) . "[_dsep_]" . $set_filter_cols;
		                    $set_filter_vals = urlencode( $this->fwd_drillvars['filter_vals'] ) . "[_dsep_]" . $set_filter_vals;
		                    $include_drilldown_selector = true;
		                }
		                else $include_drilldown_selector = true;
		                
		                $str = "";
		                
		                if($include_drilldown_selector)
		                {
		                    $sister_links = '';
		                    $sister_select = urlencode(reqvar('sister_select',''));
		                    $sister_where = urlencode(reqvar('sister_where',''));
		                    $sister_groupby = urlencode(reqvar('sister_groupby',''));
		                    
		                    $sister_links .= ($sister_select)?'&sister_select='.$sister_select:'';
		                    $sister_links .= ($sister_where)?'&sister_where='.$sister_where:'';
		                    $sister_links .= ($sister_groupby)?'&sister_groupby='.$sister_groupby:'';
		                    
		                    $multi_report_link = '';
		                    {
		                        $multi_report = reqvar('multi_report','');
		                        $sep_loc = reqvar('sep_loc','');
		                        
		                        if( $multi_report != '' ){
		                            $multi_report_link .= '&multi_report=' . $multi_report . '&';
		                            
		                            if( $sep_loc != '' ) {
		                                $multi_report_link .= '&sep_loc=' . $sep_loc . '&';
		                            }
		                        }
		                    }
		                    
		                    if( $sister_links ){
		                        $sister_links .= '&';
		                    }
		                    
		                    $str .= "<select style='font-size:10px' onchange='[CLICK_URL]'>";
		                    $str .= "<option value=''></option>";
		                    for($z=0; $z<count($strreport_parts); $z++) {
								$strname = $strreport_parts[$z];
		                        $strtitle = $strname;
		                        if( strlen($strname) && '@' === $strname[0] ){//Alias Dereference
		                            $strtitle = substr( $strname, 1 );
		                            $STRpos = strpos($strtitle, "')");
		                            if($STRpos > 0){
		                                $strExplode = explode("')",$strtitle);
		                                $strtitle = $strExplode[0];
		                            } 
		                            $report_read="";
		                            $objAlias = new report_joins($report_read);
		                            $strtitle = $objAlias->getAliasDefs($strtitle);
		                        } else {
		                            $strtitle_parts = explode(".",$strname);
		                            
		                            if(count($strtitle_parts) > 1 && trim(strtolower($strtitle_parts[1]))=="name"){
		                                $strtitle = $strtitle_parts[0];
		                            } else if(count($strtitle_parts) > 1){
		                                $strtitle = $strtitle_parts[1];
		                            } else{
		                                $strtitle = $strtitle_parts[0];
		                            }
		                        }
		                        $strtitle = htmlspecialchars(trim(ucwords(str_replace("_"," ",$strtitle))));
		                        if($strtitle!="" && !in_array($strtitle, $temptitle)) {
		                            // Title = $show_firstcol_value;
		                            $fallowed = true;
		                            for($x=0; $x<count($this->drillvars); $x++) {
		                                if(isset($this->drillvars[$x]['subreport']) && $this->drillvars[$x]['subreport']==$strname){
		                                    $fallowed = false;
		                                    continue;
		                                }
		                            }
		                            if( '@' == $strname[0] ){  //Check to see if alias isn't already in use via the sister reports
		                                $fallowed = $fallowed && !(substr(reqvar('sister_select', sister_report::sister('select')->defaultValue()), 1) == substr($strname, 1));
		                            }
		                            
		                            // below condition to not show filter by order id/device for below actions as per JIRA/LP-121 ticket
		                            $skip_actions_for = array('Clock In Time Edited',
		                                'Clock Out Time Edited',
		                                'Clock Punch Deleted',
		                                'Server Clocked In',
		                                'Server Clocked Out'
		                            );
		                            if(in_array($show_firstcol_value, $skip_actions_for) && in_array($strtitle, array('Order Id', 'Device'))) {
		                                $fallowed = false;
		                            }
		                            if($fallowed) {
		                            	if ($this->report_read['title'] != 'Sales' || (speak($strtitle) != 'Payment Type' || $_REQUEST['sister_select'] == '#super_group')) {
		                            		$str .= "<option value='{$strname}'>".speak("Filter by")." ".speak($strtitle)."</option>";
		                               	}
		                            }
		                            $temptitle[] = $strtitle;
		                        }
		                    }
		                    $str .= "</select>";
		                    
		                }
		            }
		            else
		            {
		                $str = "";
		                if(isset($_GET['test']))
		                {
		                    $str .= "no-drilldown";
		                }
		            }
		        }
		    }
		    return $str;
		}
		//Modify the result for Report Action Log report id = 35
		public function modifyActionLogReports($report_read) {
			//json object is required for offline inventory functionality, So here overwrite json string with new message for report id 35
			foreach ($report_read as $key => $actionValues) {
				if (isset($actionValues['details']) && $actionValues['action'] == 'Inventory Process Order') {
					$report_read[$key]['details'] = 'Reserved Inventory Item';

				}
				if (isset($actionValues['details']) && $actionValues['action'] == 'Inventory Deliver Order') {
					$report_read[$key]['details'] = 'Deliverd Inventory Item';
				}
			}
			return $report_read;
		}

		function unique_multidim_array($array, $lookFor) {
			$tempArray = [] ;
			$numberStr = "[number]";
			$moneyStr = "[money]";
			$savedKeys = [];
			$i = 0;
			foreach($array as $data) {
				foreach($data as $dataArray) {
						$keyName = trim($dataArray[0][$lookFor]);
						if(!isset($savedKeys[$keyName])) {
							$tempArray[$i] = $dataArray;
							$savedKeys[$keyName] = $i;
							$i++;
						} else {
							foreach($dataArray[0] as $dataKey => $dataValue) {
								$isNumber = strpos($dataValue, $numberStr);
								$isMoney = strpos($dataValue, $moneyStr);
								$dataValue = str_replace($moneyStr, '', str_replace($numberStr, '', $dataValue));
								if($dataKey !== $lookFor && is_numeric($dataValue)) {
									$tempValue = $tempArray[$savedKeys[$keyName]][0][$dataKey];
									$tempValue = str_replace($moneyStr, '', str_replace($numberStr, '', $tempValue));
									$strChk = $isNumber !== false ? $numberStr : '';
                                    $strChk= $isMoney !== false ? $moneyStr : '';
									$ref = &$tempArray[$savedKeys[$keyName]][0][$dataKey];
									$ref = $tempValue + $dataValue;
									$ref =$strChk.$ref;
								}
							}
						}
				}
			}
			return [$tempArray];
		}
	
	function paginationQuery($startDatetime, $endDatetime, $loacationDataname = '', $dataname, $isMultiReport) {
		$chainid = sessvar("admin_chain_id");
		$filter = "(dataname='".$dataname."' and dataname!='')";
		$filter = $isMultiReport === 'true' ? "(".$filter." OR (chainid='".$chainid."' and chainid!=''))" : $filter; 
		$queryString="select count(*) from `poslavu_MAIN_db`.`lavu_gift_cards` where lavu_gift_cards.deleted=0 AND lavu_gift_cards.inactive=0 AND lavu_gift_cards.created_datetime >= '".$startDatetime."' and lavu_gift_cards.created_datetime <= '".$endDatetime."' and ".$filter;
		$pages = $this->displayPaginationLink($queryString, $loacationDataname);
		return $pages;
	}

	function getPaginationConfiguration($returnTypeObject = true) {
		$limit = reqvar('limit', false);
		$noOfRowPerPage = reqvar('noOfRowPerPage', false);
		if (empty($limit) || empty($perPageRow)) {
			$data = fetchPaginationConfig(PAGINATION_LAVU_GIFT_AND_LOYALTY);
		} else {
			$data = [
				'limit' => $limit,
				'noOfRowPerPage' => $noOfRowPerPage
			];
		}
		return $returnTypeObject ? (object) $data : (array) $data;
	}

	function calculatePageOffset($limit) {
		$page = reqvar('page', 1);
		$offset = (($page * $limit) - $limit) + 1;
		return $offset > 1 ? $offset : 0;
	}

	function displayPaginationLink($selectQuery, $loacationDataname = '') {
		$activePage = (int) reqvar('page', 1);
		$totalPages = reqvar('totalPages', false);
		$pageConfig = $this->getPaginationConfiguration();
		$limit = $pageConfig->limit;
		$perPageRow = $pageConfig->noOfRowPerPage;
		$offset = 0;
		if(empty($totalPages)) {
			$queryResult = mlavu_query($selectQuery);
			$row = mysqli_fetch_row($queryResult);
			$totalRecords = isset($row[0]) ? $row[0] : 10;
			$totalPages = ceil($totalRecords / $limit);
		}
		
		$pagination = '';
		$separateLocation = (int) reqvar('sep_loc', 0);
		$multiReport = (int) reqvar('multi_report', 0);
		$mode = reqvar('mode', 'day');
		$isSeparateLocationWithMultiPart = $separateLocation === 1 && !in_array($multiReport, ['', 0]);

		if($totalPages > 1) {
			$this->paginationSet = true;
			$pagination .= '<div class="reports-pagination">';
			$list = '<ul class="reports-pagination-ul" data-pagination-length="'.$totalPages.'" data-split-number-count="'.$perPageRow.'">';
			$previousDisableClass = $activePage === 1 ? 'action-disabled' : '';
			$previousRowClass = $activePage <= $perPageRow ? 'action-disabled': '';
			$list .= '<li style="margin-right:10px" class="previous-page input_wrapper next-prev-button '.$previousRowClass.'" onclick="nextPrevious($(this))"><<</li>';
			$list .= '<li class="previous-page-number input_wrapper next-prev-button '.$previousDisableClass.'" onclick="nextPrevious($(this))">&#x25C0;</li>';
			
			$activeSetEnd =  (int) ceil($activePage/$perPageRow) * $perPageRow;
			$activeSetStart = abs($activeSetEnd - $perPageRow) + 1;
			$isNextDisabled = false;
			$isLastActive = false;
			for($i = 1; $i <= $totalPages; $i++) {
				$activeClass = $i === $activePage ? 'active-page': '';
				$isHidden = true;
				$rowClass = 'class="hidden"';
				if ($i >= $activeSetStart && $i <= $activeSetEnd) {
					$rowClass = 'class="input_wrapper '.$activeClass.'"';
					$isHidden = false;
				}
				$onClick = 'click_to_page(\'multi_report='.$multiReport.'&sep_loc='.$separateLocation.'&mode='.$mode.'&page='.$i.'&limit='.$limit.'&noOfRowPerPage='.$perPageRow.'&totalPages='.$totalPages.'\')';
				
				if ($isSeparateLocationWithMultiPart) {
					$onClick = 'loadReportPage($(this),'.$i.',\''.$limit.'\','.$offset.',\''.$loacationDataname.'\')';
				}
				 
				$list .= '<li '.$rowClass.' data-number="'.$i.'" onclick="'.$onClick.'">'.$i.'</li>';
				if ((int) $i === (int) $totalPages && !$isHidden && !$isNextDisabled) {
					$isNextDisabled = true;
					$isLastActive = !empty($activeClass);
				}
			}
			$nextDisableNumberClass = $isLastActive ? 'action-disabled' : '';
			$nextDisableClass = $isNextDisabled ? 'action-disabled': '';
			$list .= '<li class="next-page-number input_wrapper next-prev-button '.$nextDisableNumberClass.'" onclick="nextPrevious($(this))">&#x25B6;</li>';
			$list .= '<li style="margin-left:10px" class="next-page input_wrapper next-prev-button '.$nextDisableClass.'" onclick="nextPrevious($(this))">>></li></ul>';
			$select .= '</select>';
			$pagination .= $list;
			$pagination .= '</div>';
		}

		return $pagination;
	}

	public function getExportToEmailParams() {
		$params = [];
		if (!$this->prepareQueriesForEmailReport) {
			return $params;
		}
		$this->queries[dates]= $this->dates;
		$params['email'] = reqvar('email', '');
		$params['queries'] = $this->queries;
		$params['report_id'] = $this->report_id;
		$params['dataname'] = sessvar('admin_dataname');
		switch($this->report_name) {
			case TYPE_LAVU_GIFT_AND_LOYALITY: 
				$params = array_merge($params, $this->getPaginationConfiguration(false));
			break;
		}
		
		return $params;
	}

	public function processEmailReport() {
		$this->show_query_table();
		$params = $this->getExportToEmailParams();
		$emailReports = new EmailReports();
		if ($emailReports->setReportToProcess($params) !== false) {
			return true;
		} else {
			return false;
		}
	}
}
?>
