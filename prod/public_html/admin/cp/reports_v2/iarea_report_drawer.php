<?php

interface iarea_report_drawer {
	public function draw_multiple_rows( $report_read );
	public function draw_single_row( $report_read );
	public function draw_no_data();
}