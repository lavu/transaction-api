<?php
function labor_report($vars)
{
	$vars['mode'] = $_GET['mode'];
	global $location_info;
	ini_set('memory_limit', '1024M');
	ini_set('max_execution_time', 600);
	if( !empty( $location_info['timezone'] )){
		date_default_timezone_set( $location_info['timezone'] );
	}

			$lvars = array();
			$lvars['min_datetime'] = $vars['start_datetime'];
			$lvars['max_datetime'] = $vars['end_datetime'];
			$final_start_date = '';
			$final_end_date = '';
			$final_start_date = explode(" ", $vars['start_datetime']);
			$final_end_date = explode(" ", $vars['end_datetime']);
			$lvars['labor_start_date'] = $final_start_date[0];
			$lvars['labor_start_time'] = $final_start_date[1];
			$lvars['labor_end_date'] = $final_end_date[0];
			$lvars['labor_end_time'] = $final_end_date[1];
			//Get start_date,start_time,end_date,end_time
			if ($vars['mode'] == '' || $vars['mode'] == 'day') {
				if ($lvars['labor_end_time'] != $lvars['labor_start_time'] && $lvars['labor_end_time'] > $lvars['labor_start_time']) {
				$labor_whereStr = "(DATE(orders.closed) BETWEEN '[labor_start_date]' AND '[labor_end_date]') AND (TIME(orders.closed) BETWEEN '[labor_start_time]' AND '[labor_end_time]')";
				} else {
				$labor_whereStr = "orders.closed >= '[min_datetime]' AND orders.closed < '[max_datetime]'";
				}
			} else {
				$labor_whereStr = "orders.closed >= '[min_datetime]' AND orders.closed < '[max_datetime]'";
			}

			$laborq = "SELECT sum((order_contents.price + order_contents.modify_price + order_contents.forced_modifiers_price)*order_contents.quantity - if( order_contents.quantity > '0',if(order_contents.discount_amount<((order_contents.price + order_contents.modify_price + order_contents.forced_modifiers_price)*order_contents.quantity),order_contents.discount_amount,(order_contents.price + order_contents.modify_price + order_contents.forced_modifiers_price)*order_contents.quantity)+ order_contents.idiscount_amount + order_contents.itax, 0)) as net FROM orders LEFT JOIN order_contents ON orders.order_id  = order_contents.order_id AND
			orders.location_id = order_contents.loc_id WHERE $labor_whereStr AND orders.currency_type = '0' AND orders.void = '0' AND orders.location_id = '1' AND order_contents.item != 'SENDPOINT'";
			$labor_query = lavu_query($laborq, $lvars);
			$net_read = mysqli_fetch_assoc($labor_query);
			$net=$net_read['net'];
			if($net==""){$net="0";}
			$day_start_ts = strtotime($vars['start_datetime']);
			$day_end_ts = strtotime($vars['end_datetime']);

			$datediff = $day_end_ts - $day_start_ts;
			$diff = $datediff / (60 * 60 * 24);

			$hinfo = array(); // For Hours
			for($h=0; $h<24*$diff; $h++)
			{
				$h_ts = $day_start_ts + (60 * 60 * $h);
				$h_index = date("Y-m-d H",$h_ts);
				$hinfo[$h_index] = array("sales_total"=>0,"labor_hours"=>0,"labor_cost"=>0,"labor_info"=>array(),"ingredient_cost"=>0);
			}
			$day_start_datetime_check = date("Y-m-d H:i:s",strtotime($vars['start_datetime']) - (60 * 60 * 24));
			$lvars['min_datetime_check'] = $day_start_datetime_check;
			$hour_info = array();
			$clock_query = lavu_query("select * from `clock_punches` where `time`>='[min_datetime_check]' and `server_time`!='' and `_deleted`!='1' and (`time_out`='' or `time_out`>'[min_datetime]') and `time`<'[max_datetime]' and `location_id`='1' and `server`!='' and `punch_type`='Shift' order by `server_time` asc", $lvars);
			while($clock_read = mysqli_fetch_assoc($clock_query))
			{
				$punch_start_ts = strtotime($clock_read['time']);
				if($clock_read['time_out']=="") $punch_end_ts = time();
				else $punch_end_ts = strtotime($clock_read['time_out']);

				if($punch_end_ts > $punch_start_ts && $punch_end_ts < $punch_start_ts + (60 * 60 * 24 * 7))
				{
					for($t=$punch_start_ts; $t<=$punch_end_ts+(60*60); $t+=(60*60))
					{
						$hour_start_datetime = date("Y-m-d H",$t) . ":00:00";
						$hour_end_datetime = date("Y-m-d H",$t) . ":59:59";
						$hour_start_ts = strtotime($hour_start_datetime);
						$hour_end_ts = strtotime($hour_end_datetime);

						if($hour_start_ts > $punch_end_ts)
						{
							$hour_add = 0;
						}
						else if($hour_end_ts < $punch_start_ts)
						{
							$hour_add = 0;
						}
						else if($punch_start_ts <= $hour_start_ts && $punch_end_ts >= $hour_end_ts)
						{
							$hour_add = 1;
						}
						else if($punch_start_ts <= $hour_start_ts && $punch_end_ts < $hour_end_ts)
						{
							$hour_add = sprintf('%0.2f', ($punch_end_ts - $hour_start_ts) / 60 / 60);
							if($hour_add * 1 < 0)
							{
								echo date("Y-m-d H:i:s",$punch_start_ts) . " - " . date("Y-m-d H:i:s",$punch_end_ts) . "<br>";
							}
						}
						else if($punch_start_ts > $hour_start_ts && $punch_end_ts >= $hour_end_ts)
						{
							$hour_add = sprintf('%0.2f', ($hour_end_ts - $punch_start_ts) / 60 / 60);
						}
						else if($punch_start_ts > $hour_start_ts && $punch_end_ts < $hour_end_ts)
						{
							$hour_add = sprintf('%0.2f', ($punch_end_ts - $punch_start_ts) / 60 / 60);
						}
						else
						{
							$hour_add = 0;
						}
						if ($clock_read['payrate'] == "")
						{
							$cost_query = lavu_query("SELECT * FROM `users` WHERE id = '[server_id]'", $clock_read);
							while ($cost_read = mysqli_fetch_assoc($cost_query))
							{
								$for_min = $cost_read['payrate'];
								$cost_add = $for_min * $hour_add;
							}
						}else{
								
							$for_min = $clock_read['payrate'];
							$cost_add = $for_min * $hour_add;
						}

						$hindex = date("Y-m-d H",$t);
						if(isset($hinfo[$hindex]))
						{
							$hour_info[$clock_read['role_id']]['labor_hours'] += $hour_add * 1;
							$hour_info[$clock_read['role_id']]['labor_cost'] += $cost_add * 1;
						}
					}
				}
			}
			$rows = array();
			foreach ($hour_info as $rid => $rdata) {
			    $title="";
			    if (isset($rid) && $rid!="") {
					$rclass_query = lavu_query("SELECT title FROM `emp_classes` WHERE id = ".$rid);
					$rclass_read = mysqli_fetch_assoc($rclass_query);
					$title = $rclass_read['title'];
				}
				$lab_hrs = sprintf('%0.2f', ($rdata['labor_cost']/$net)*100);
				$sale_lh = sprintf('%0.2f',$net/$rdata['labor_hours']);
				$data['work_area'] = $title;
				$data['reg_hours'] = $rdata['labor_hours'];
				$data['total_pay'] = '[money]'.$rdata['labor_cost'];
				$data['net_sales'] = '[money]'.$net;
				$data['labor %'] = $lab_hrs;
				$data['sales/lh'] = '[money]'.$sale_lh;
				$rows[] = $data;
			}
			$rop_id=0;
			$data=array();
			foreach ($rows as $sum1)
			{
				$data[0][$rop_id][0]['work_area'] = $sum1['work_area'];
				$data[0][$rop_id][0]['reg_hours'] = $sum1['reg_hours'];
				$data[0][$rop_id][0]['total_pay'] = $sum1['total_pay'];
				$data[0][$rop_id][0]['net_sales'] = $sum1['net_sales'];
				$data[0][$rop_id][0]['labor %'] = $sum1['labor %'];
				$data[0][$rop_id][0]['sales/lh'] = $sum1['sales/lh'];
				$rop_id++;
			}

			return $data;
}

?>
