<?php

require_once(__DIR__ .'/InventoryApiReport.php');

$args = array(
	'report_name'     => 'food_and_beverage',
	'report_api_path' => 'Reports/FoodAndBeverage',
	'PHPSESSID'       => 'h1hr0vnjvppl6bhk0vpiojgo83',
	'SERVER_NAME'     => 'localhost',
	'REQUEST_SCHEME'  => 'http',
);

$inventoryApiReport = new InventoryApiReport();
$results[] = $inventoryApiReport->getData($args);

echo print_r($results, true);
