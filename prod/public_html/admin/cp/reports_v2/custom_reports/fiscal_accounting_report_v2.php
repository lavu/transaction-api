<?php
ini_set("memory_limit",'300M');
function fiscal_accounting_report_v2($vars) {
    $allFiscalTypeRefundsTypeA = array("NotaDeCreditoA","TiqueNotaCreditoA");
    $allFiscalTypeRefundsTypeB = array("NotaDeCreditoB", "TiqueNotaCreditoB");
    
	if (strpos ( $vars ['report_name'], "daily" ) !== false)
		$report_group_by = "day";
	else
		$report_group_by = "record";
	
	$queryMode = isset($_REQUEST['mode']) && !empty($_REQUEST['mode']) ? $_REQUEST['mode'] : false;
	$getWhereClause = setWhereClauseForReport($vars, $queryMode);
	$whereClause = $getWhereClause['where_clause'];
	$orderClause = $getWhereClause['order_clause'];
	$fiscal_spans = array ();
	$fiscal_query = lavu_query("select * from `fiscal_reports` where $whereClause");
	while ( $fiscal_read = mysqli_fetch_assoc ( $fiscal_query ) ) {
		$register = $fiscal_read ['register'];
		$first_invoice = $fiscal_read ['first_invoice'];
		$last_invoice = $fiscal_read ['last_invoice'];
		$invoice_report_id = $fiscal_read ['report_id'];
		$report_type = $fiscal_read ['report_type'];
		
		if (! isset ( $fiscal_spans ["RT" . $report_type] ["R" . $register] )) {
			$fiscal_spans ["RT" . $report_type] ["R" . $register] = array ();
		}
		
		if (is_numeric ( $first_invoice ) && is_numeric ( $last_invoice )) {
			$invoice_span = $last_invoice * 1 - $first_invoice * 1;
			if ($invoice_span >= 0 && $invoice_span <= 5000) {
				for ($n = $first_invoice * 1; $n <= $last_invoice * 1; $n++) {
					$fiscal_spans ["RT" . $report_type] ["R" . $register] ["I" . ($n * 1)] = $invoice_report_id;
				}
			}
		}
	}
	$startts = time ();
	// schedule - actual sales time
	// fiscal date - according to fiscal
	// commercial date - according to lavu
	// z location - fiscal zreport id
	// type of fiscal receipt - a/b/nc
	// invoice number - order_id
	// name
	// cuit (local id) - xx-xxxxxxxx-x
	
	// print_r($vars);
	
	$stdate = $vars ['start_datetime'];
	$endate = $vars ['end_datetime'];
	
	$query = "select `locations`.`title` as 'location_name',
						LEFT(orders.closed,10) as fiscal_date,
						concat(`med_customers`.f_name,' ',`med_customers`.l_name) as Client_Name,
                        IF(truncate(' ',`med_customers`.field10) = '', `med_customers`.field10, '') AS CUIT,
						'z_location' as z_location,
                        split_check_details.print_info AS PV_NO,
                        'invoicenumber' as invoicenumber,
                        split_check_details.print_info AS fiscal_type,
                        'VAT_percentage' as VAT_percentage,
						orders.order_id as order_id,
                        split_check_details.print_info as fiscal_txn_id,
						if(count(*) > 0, concat('[money]',format(sum( IF(cc_transactions.action='Refund', -1*cc_transactions.total_collected, cc_transactions.total_collected) ),2)), '[money]0.00') as total_payment,
						`payment_types`.`type` as `group_col_payment_types_type`
						from `orders`
						LEFT JOIN `cc_transactions` on `orders`.`order_id` = `cc_transactions`.`order_id`
						LEFT JOIN `payment_types` ON `cc_transactions`.`pay_type_id` = `payment_types`.`id` AND (`cc_transactions`.`loc_id` = `payment_types`.`loc_id` OR '0' = `payment_types`.`loc_id` )
						LEFT JOIN `locations` on `orders`.`location_id`=`locations`.`id`
						LEFT JOIN `split_check_details` on `orders`.`order_id`=`split_check_details`.`order_id` AND `split_check_details`.`check`=1
						LEFT JOIN `med_customers` ON `med_customers`.id = SUBSTRING_INDEX(SUBSTRING_INDEX(`orders`.original_id, '|', 7),'|',-1)
						WHERE $orderClause
						group by orders.id order by fiscal_date desc;";
	
	$order_id_list = "";
	
	$rows = array ();
	// output_debug_query($query,$vars);
	$report_query = lavu_query ( $query, $vars );
	while ( $report_read = mysqli_fetch_assoc ( $report_query ) ) {
		
		if ($order_id_list != "") {
			$order_id_list .= ",";
		}
		
		$escaped_order_id = str_replace ( urlencode ( "-" ), "-", urlencode ( $report_read ['order_id'] ) );
		$order_id_list .= "'" . $escaped_order_id . "'";
		
		if (isset ( $report_read ['fiscal_type'] )) {
			$fiscal_type_reg = str_replace ( "}{", "},{", $report_read ['fiscal_type'] );
			$fiscal_type_reg = "[" . $fiscal_type_reg . "]";
			$fiscal_type = json_decode ( $fiscal_type_reg, 1 );
			if (count ( $fiscal_type ) != 1) {
				if ($r_order_id != $report_read [order_id]) {
					$r_order_id = $report_read [order_id];
					$fiscalTypeCount = count ( $fiscal_type ) - 1;
				} else {
					$fiscalTypeCount--;
				}
				$report_read ['fiscal_type'] = $fiscal_type [$fiscalTypeCount] [receipt_type];
				$report_read ['fiscal_txn_id'] = $fiscal_type [$fiscalTypeCount] [transaction_id];
				$report_read ['PV_NO'] = $fiscal_type [$fiscalTypeCount] ['PV_Number'];
			} else {
				$report_read ['fiscal_type'] = $fiscal_type [0] [receipt_type];
				$report_read ['fiscal_txn_id'] = $fiscal_type [0] [transaction_id];
				$report_read ['PV_NO'] = $fiscal_type [0] ['PV_Number'];
			}
		} else {
			$report_read ['fiscal_type'] = "";
			$report_read ['fiscal_txn_id'] = "";
			$report_read ['PV_NO'] = "";
		}
		
		if ( in_array($report_read ['fiscal_type'],$allFiscalTypeRefundsTypeA)) {
			if (isset ( $report_read ['total_payment'] )) {
				$totPayment = str_replace ( "[money]", "-", $report_read ['total_payment'] );
				$report_read ['total_payment'] = '[money]' . $totPayment;
			}
		}
		$rows [] = $report_read;
	}
	
	$order_data = array ();
	$categories = array ();
	
	if (! empty ( $order_id_list )) {
		
		$query = "select `orders`.`total` as `order_total`,
												concat('[money]',`orders`.`subtotal` - (`orders`.`discount` + `orders`.`idiscount_amount` + `orders`.`itax`)) as `order_taxable`,
                                                 concat('[money]',if(`orders`.`void`=0,`fiscal_transaction_log`.`refund`,'0.00')) as `order_refund`,
												concat('[money]',`orders`.`tax` + `orders`.`itax`) as `order_tax`,
                                                 concat('[money]',`fiscal_transaction_log`.`total`) as `total`,
                                                 concat('',`fiscal_transaction_log`.`show`) as `show`,   
												`orders`.`register_name` as `register_name`,
												`menu_categories`.`name` as `category_name`,
												`menu_categories`.`id` as `category_id`,
												`order_contents`.`tax_subtotal1` as `cost`,
												`order_contents`.`order_id` as `order_id`,
												`order_contents`.`item_id` as `item_id`,
                                                 `fiscal_transaction_log`.`transaction_id`
													from `order_contents`
												left join `menu_categories` on `order_contents`.`category_id`=`menu_categories`.`id`
												left join `orders` on `order_contents`.`order_id`=`orders`.`order_id`
                                                 left join `fiscal_transaction_log` on ( `orders`.`order_id`=`fiscal_transaction_log`.`order_id` And `fiscal_transaction_log`.`show`=1 )
													where `orders`.`order_id` IN ($order_id_list) AND `order_contents`.`quantity`*1 > 0";
		
		$content_query = lavu_query ( $query );
		while ( $content_read = mysqli_fetch_assoc ( $content_query ) ) {
			$order_id = $content_read ['order_id'];
			$category_id = $content_read ['category_id'];
			$category_name = trim ( $content_read ['category_name'] );
			$cost = $content_read ['cost'];
			if (! isset ( $order_data [$order_id] [$category_id] ))
				$order_data [$order_id] [$category_id] = 0;
			$order_data [$order_id] [$category_id] += $cost;
			$categories [$category_id] = $category_name;
			$order_data [$order_id] ['info'] = $content_read;
			$order_data [$order_id] ['register_name'] = $content_read ['register_name'];
		}
	}
	
	
	// Make sure all orders have information for all categories found
	foreach ( $order_data as $order_id => $order_arr ) {
		foreach ( $categories as $category_id => $category_name ) {
			if (! isset ( $order_data [$order_id] [$category_id] ))
				$order_data [$order_id] [$category_id] = 0;
		}
	}
	
	$processed_order_ids = array ();
	
	for ($i = 0; $i < count ( $rows ); $i++) {
		$order_id = $rows [$i] ['order_id'];
		$fiscalTypeToSearch = $rows [$i] ['fiscal_type'];
		if (in_array($fiscalTypeToSearch, $allFiscalTypeRefundsTypeA) ) {
			$taxable = str_replace ( "[money]", "-", $order_data [$order_id] ['info'] ['order_taxable'] );
			$rows [$i] ['order_taxable'] = '[money]' . $taxable;
			$tax = str_replace ( "[money]", "-", $order_data [$order_id] ['info'] ['order_tax'] );
			$rows [$i] ['order_tax'] = '[money]' . $tax;
		} else {
			$rows [$i] ['order_taxable'] = $order_data [$order_id] ['info'] ['order_taxable'];
			$rows [$i] ['order_tax'] = $order_data [$order_id] ['info'] ['order_tax'];
			$refund = str_replace ( "[money]", "-", $order_data [$order_id] ['info'] ['order_refund'] );
			$rows [$i] ['order_refund'] = '[money]' . $refund;
		}
		$order_arr = $order_data [$order_id];
		$fiscal_txn_id = $rows [$i] ['fiscal_txn_id'];
		$fiscal_type = $rows [$i] ['fiscal_type'];
		
		if (isset ( $order_arr ['register_name'] ) && trim ( $order_arr ['register_name'] ) != "" && trim ( $fiscal_txn_id ) != "") {
			$rows [$i] ['z_location'] = "No ZReport";
			$register_name = trim ( $order_arr ['register_name'] );
			if (isset ( $fiscal_spans ["RT" . $fiscal_type] ["R" . $register_name] ["I" . ($fiscal_txn_id * 1)] )) {
				$rows [$i] ['z_location'] = "#" . $fiscal_spans ["RT" . $fiscal_type] ["R" . $register_name] ["I" . ($fiscal_txn_id * 1)];
			}
		}
		
		$processed_order_ids [] = $order_id;
	}
	
	$pvNumberGrouping = array ();
	$tempInvoiceRange = array();

	
	for ($i = 0; $i < count ( $rows ); $i++) {
		$newrow = array ();
		foreach ( $rows [$i] as $col => $val ) {
			if ($col == "total_payment") {
				$newrow ['order_taxable'] = $rows [$i] ['order_taxable'];
				$newrow ['order_tax'] = $rows [$i] ['order_tax'];
				$newrow ['order_refund'] = $rows [$i] ['order_refund'];
			}
			if ($col != "total_cash" && $col != "total_card") {
				$newrow [$col] = $rows [$i] [$col];
			}
		}
		$pvNumber = (isset ( $newrow ['PV_NO'] ) && $newrow ['PV_NO'] != "") ? $newrow ['PV_NO'] : 'Not Available';
		$newrow ["PV_NO"] = $pvNumber;
		$fiscalReportId = $newrow ['z_location'];
		if (empty ( $newrow ["total_payment"] )) {
			$newrow ["total_payment"] = "[money]0";
		}
		
		if (empty ( $newrow ['order_taxable'] )) {
			$newrow ['order_taxable'] = "[money]0";
		}
		if (empty ( $newrow ['order_tax'] )) {
			$newrow ['order_tax'] = "[money]0";
		}
		if (empty ( $newrow ['order_refund'] )) {
		    $newrow ['order_refund'] = "[money]0";
		}
		if (empty ( $newrow ['Client_Name'] )) {
			$newrow ['Client_Name'] = "Consumidor Final";
		}
		
		if ($newrow ['invoicenumber'] == "invoicenumber") {
		    $newrow ['invoicenumber'] = $newrow ['fiscal_txn_id'];
		}
		
		if ($newrow ['VAT_percentage'] == "VAT_percentage") {
			$newrow ['VAT_percentage'] = "21%";
		}
		$fiscalType = trim ( $newrow ['fiscal_type'] );
		if (in_array ( $fiscalType, array (
				"TiqueFacturaB",
				"ReciboB",
		        "NotaDeCreditoB", 
		        "TiqueNotaCreditoB"
		) )) {
		    if (!isset($pvNumberGrouping [$pvNumber] [$fiscalType] [$fiscalReportId])) {
			    $pvNumberGrouping [$pvNumber] [$fiscalType] [$fiscalReportId] ['location_name'] 	= $newrow ['location_name'];
			    $pvNumberGrouping [$pvNumber] [$fiscalType] [$fiscalReportId] ['fiscal_date'] 	= $newrow ['fiscal_date'];
			    $pvNumberGrouping [$pvNumber] [$fiscalType] [$fiscalReportId] ['Client_Name'] 	= $newrow ['Client_Name'];
			    $pvNumberGrouping [$pvNumber] [$fiscalType] [$fiscalReportId] ['CUIT'] 			= $newrow ['CUIT'];
			    $pvNumberGrouping [$pvNumber] [$fiscalType] [$fiscalReportId] ['z_location'] 		= $newrow ['z_location'];
			    $pvNumberGrouping [$pvNumber] [$fiscalType] [$fiscalReportId] ['PV_NO'] 			= $newrow ['PV_NO'];
			    $pvNumberGrouping [$pvNumber] [$fiscalType] [$fiscalReportId] ['invoicenumber'] 	= $newrow ['invoicenumber'];
			    
			    $order_taxable 	= str_replace ( '[money]', '', $newrow ['order_taxable'] );
			    $order_tax		= str_replace ( '[money]', '', $newrow ['order_tax'] );
			    $order_refund 	= str_replace ( ',', '', str_replace ( '[money]', '', $newrow ['order_refund'] ) );
			    $total_payment 	= str_replace ( ',', '', str_replace ( '[money]', '', $newrow ['total_payment'] ) );
			    
			    if (in_array($fiscalType, $allFiscalTypeRefundsTypeB)) {
			        $order_taxable   = "-".$order_taxable;
			        $order_tax       = "-".$order_tax;
			        $order_refund 	= 0;
			        $total_payment   = "-".$total_payment;
			    }
			    
			    $pvNumberGrouping [$pvNumber] [$fiscalType] [$fiscalReportId] ['order_taxable'] 	= $order_taxable;
			    $pvNumberGrouping [$pvNumber] [$fiscalType] [$fiscalReportId] ['order_tax'] 		= $order_tax;
			    $pvNumberGrouping [$pvNumber] [$fiscalType] [$fiscalReportId] ['order_refund'] 	= $order_refund;
			    $pvNumberGrouping [$pvNumber] [$fiscalType] [$fiscalReportId] ['total_payment'] 	= $total_payment;
			    
			} else {
			    
			    $order_taxable 	= str_replace ( '[money]', '', $newrow ['order_taxable'] );
			    $order_tax		= str_replace ( '[money]', '', $newrow ['order_tax'] );
			    $order_refund 	= str_replace ( ',', '', str_replace ( '[money]', '', $newrow ['order_refund'] ) );
			    $total_payment 	= str_replace ( ',', '', str_replace ( '[money]', '', $newrow ['total_payment'] ) );
			    
			    if (in_array($fiscalType, $allFiscalTypeRefundsTypeB)) {
			        $order_taxable   = "-".$order_taxable;
			        $order_tax       = "-".$order_tax;
			        $order_refund = 0;
			        $total_payment   = "-".$total_payment;
			    }
			    
			    $pvNumberGrouping [$pvNumber] [$fiscalType] [$fiscalReportId] ['order_taxable'] 	= $pvNumberGrouping [$pvNumber][$fiscalType] [$fiscalReportId] ['order_taxable'] + $order_taxable;
			    $pvNumberGrouping [$pvNumber] [$fiscalType] [$fiscalReportId] ['order_tax'] 		= $pvNumberGrouping [$pvNumber][$fiscalType] [$fiscalReportId] ['order_tax'] + $order_tax;
			    $pvNumberGrouping [$pvNumber] [$fiscalType] [$fiscalReportId] ['order_refund'] 		= $pvNumberGrouping [$pvNumber][$fiscalType] [$fiscalReportId] ['order_refund'] + $order_refund;
			    $pvNumberGrouping [$pvNumber] [$fiscalType] [$fiscalReportId] ['total_payment'] 	= $pvNumberGrouping [$pvNumber][$fiscalType] [$fiscalReportId] ['total_payment'] + $total_payment;
			}
			$tempInvoiceRange[$pvNumber.$fiscalType.$fiscalReportId][] = $newrow ['invoicenumber'];
		} else {
			$rows [$i] = $newrow;
		}
	}
	
	$translated_phrases = array ();
	$auto_translate = array (
			"fiscal_date",
			"z_location",
			"invoicenumber",
			"fiscal_type",
			"PV_NO",
			"order_id",
			"fiscal_txn_id",
			"CUIT",
			"order_taxable",
			"order_tax",
	        "order_refund",
			"total_payment" 
	);
	for ($n = 0; $n < count ( $auto_translate ); $n++) {
		$phrase = $auto_translate [$n];
		$human_phrase = trim ( ucwords ( str_replace ( "_", " ", $phrase ) ) );
		$translated_human_phrase = speak ( $human_phrase );
		if ($translated_human_phrase != $human_phrase) {
			$translated_phrase = str_replace ( " ", "_", trim ( str_replace ( ".", "", strtolower ( $translated_human_phrase ) ) ) );
			$translated_phrases [$phrase] = $translated_phrase;
		}
	}
	for ($i = 0; $i < count ( $rows ); $i++) {
		$newrow = array ();
		foreach ( $rows [$i] as $col => $val ) {
			if (isset ( $translated_phrases [$col] ))
				$newrow [$translated_phrases [$col]] = $val;
			else
				$newrow [$col] = $val;
		}
		$rows [$i] = $newrow;
	}
	
	$transalatedFiscalType = translatePhrase('fiscal_type');
	$newrow2 = array ();
	if (count ( $rows ) > 0) {
		for ($i = 0; $i < count ( $rows ); $i++) {
		    if (! in_array ( $rows [$i] [$transalatedFiscalType], array (
					"TiqueFacturaB",
					"ReciboB",
			        "NotaDeCreditoB", 
			        "TiqueNotaCreditoB"
			) )) {
				$newrow2 [$i] = $rows [$i];
			}
		}
		
		if (count ( $newrow ) > 1) {
		    if (count ( $pvNumberGrouping ) > 0) {
		        $reportType = 'FB';
		        foreach ( $pvNumberGrouping as $pvNo => $reportBasedArr ) {
		            foreach ( $reportBasedArr as $fiscalReportType =>  $fiscalReportBasedArr ) {
		                foreach ( $fiscalReportBasedArr as $key => $pvBasedArr) {
		                    $human_phrase4 = trim ( ucwords ( str_replace ( "_", " ", 'order_tax' ) ) );
		                    $translated_human_phrase4 = speak ( $human_phrase4 );
		                    
		                    if ($translated_human_phrase4 != $human_phrase4) {
		                        $translated_phrase4 = str_replace ( " ", "_", trim ( str_replace ( ".", "", strtolower ( $translated_human_phrase4 ) ) ) );
		                        $translated_phrases4 = $translated_phrase4;
		                    } else {
		                        $translated_phrases4 = 'order_tax';
		                    }
		                    switch ( $fiscalReportType ) {
		                        case 'TiqueFacturaB':
		                            $reportType = "FB";
		                            break;
		                        case "ReciboB":
		                            $reportType = "ReciboB";
		                            break;
		                        case "NotaDeCreditoB":
		                        case "TiqueNotaCreditoB":
		                            $reportType = "NCB";
		                            break;
		                    }
		                    $totalPayment = translatePhrase("total_payment");
		                    $orderTaxable = translatePhrase("order_taxable");
		                    $TequeFacturaBArray [] = array (
		                        'location_name' => $pvBasedArr ['location_name'],
		                        'fiscal_date' => $pvBasedArr ['fiscal_date'],
		                        'Client_Name' => 'Consumidor Final',
		                        'CUIT' => '-NA-',
		                        'z_location' => $key,
		                        'PV_NO' => $pvNo,
		                        'invoicenumber' => min($tempInvoiceRange[$pvNo.$fiscalReportType.$key])." - ". max($tempInvoiceRange[$pvNo.$fiscalReportType.$key]),
		                        $transalatedFiscalType => $reportType,
		                        'VAT_percentage' => '21%',
		                        'order_id' => '----------',
		                        'fiscal_txn_id' => '----------',
		                        $orderTaxable => '[money]' . $pvBasedArr ['order_taxable'],
		                        $translated_phrases4 => '[money]' . $pvBasedArr ['order_tax'],
		                        'order_refund' => '[money]' . $pvBasedArr ['order_refund'],
		                        $totalPayment => '[money]' . $pvBasedArr ['total_payment']
		                    );
		                }
		            }
		        }
		    }
		}
	}
	$newrowDisplay = array ();

	$newrow1 = array_merge ( $newrow2, $TequeFacturaBArray );
	foreach ( $newrow1 as $vals ) {
		$human_phrase1 = trim ( ucwords ( str_replace ( "_", " ", "order_id" ) ) );
		$translated_human_phrase1 = speak ( $human_phrase1 );
		if ($translated_human_phrase1 != $human_phrase1) {
			$translated_phrase1 = str_replace ( " ", "_", trim ( str_replace ( ".", "", strtolower ( $translated_human_phrase1 ) ) ) );
			$translated_phrases1 = $translated_phrase1;
		}
		
		unset ( $vals [$translated_phrases1] );
		unset ( $vals ['order_id'] );
		
		if ($vals ['fiscal_type'] == "TiqueFacturaA") {
			$vals ['fiscal_type'] = "FA";
			$vals ['invoicenumber'] = $vals ['fiscal_txn_id'];
		}
		if ($vals ['fiscal_type'] == "NotaDeCreditoA" || $vals ['fiscal_type'] == "TiqueNotaCreditoA") {
		    $vals ['fiscal_type'] = "NCA";
		    $vals ['invoicenumber'] = $vals ['fiscal_txn_id'];
		}
		unset ( $vals ['fiscal_txn_id'] );
		$newrowDisplay [] = $vals;
	}
	
	$counter=0;
	$dataArray = array();
	foreach ( $newrowDisplay as $singleRow ) {
	    $dataArray[0][$counter][0]=$singleRow;
	    $counter++;
	}
	return $dataArray;
}
?>
