<?php

class InventoryApiReport
{
	const INVENTORY_API_ENDPOINT_PATH = '/api_entry_point/inventory/InventoryRequestRouter.php/inventory';  // TO DO : use current SERVER_URL
	const INVENTORY_API_VERSION       = 'v1.0';

	function __construct()
	{
		$this->endpoint_url_base = InventoryApiReport::INVENTORY_API_ENDPOINT_PATH .'/'. InventoryApiReport::INVENTORY_API_VERSION;
		$this->sessionID = session_id();
	}


	/**
	 * Run inventory report by making Inventory API call
	 *
	 * @param  $args Array of args with required key(s): report_api_path
	 *
	 * @return Array of report data
	 */
	function getData($args)
	{
		if (empty($args['report_api_path']))
		{
			error_log(__FILE__ ." was missing required arg: ". json_encode($args));
			return;
		}

		// Set args to default values if they do not have pass-thru values (pass-thru support helps testing and also overrides)

		// Convert V2 report date range field names
		// Need to pass Lavu session cookie through to Inventory API for authentication
		if (empty($args['PHPSESSID'])) $args['PHPSESSID'] = $_COOKIE['PHPSESSID'];

		// Need to know what server on which we will make the API request
		if (empty($args['SERVER_NAME']))    $args['SERVER_NAME']    = empty($_SERVER['SERVER_NAME']) ? 'localhost' : $_SERVER['SERVER_NAME'];
		if (empty($args['REQUEST_SCHEME'])) $args['REQUEST_SCHEME'] = empty($_SERVER['REQUEST_SCHEME']) ? 'http'   : $_SERVER['REQUEST_SCHEME'];

		if (isset($args['start_datetime']) && $args['start_datetime'] != '') {
			$args['start_date'] =  $args['start_datetime'];
		}
		if (isset($args['end_datetime']) && $args['end_datetime'] != '') {
			$args['end_date'] =  $args['end_datetime'];
		}
		// Pass date range args thru

		$dateRange = array('startDate' => $args['start_date'], 'endDate' => $args['end_date'], 'startTime' => $args['start_time'], 'endTime' => $args['end_time'], 'mode' => $args['mode']);

		// The Inventory API expects an array of input request parameters
		$args['payload_json'] = json_encode(array($dateRange));

		// Make the Inventory API call for our report
		$response_json = $this->makeInventoryApiCall($args);

		// Convert JSON to an array
		$response = json_decode($response_json, true);
		$results = array();
		if (isset($response['DataResponse'])) $results = $response['DataResponse'];
		if (isset($results['DataResponse'])) $results = $results['DataResponse'];
		// Re-format data for V2 Reports

		$data = array();
		foreach ($results as $result)
		{
			$dataRow = array();
			foreach ($result as $key => $val)
			{

				$new_key = preg_replace('/([a-z0-9])([A-Z])/', '${1} ${2}', $key);
				$new_key = str_ireplace(' Of ', ' of ', $new_key);

				// Skip ID fields and quantity fields
				if (preg_match('/ID$/', $new_key)){
					continue;
				}
                else if(preg_match('/Qty On Hand$/', $new_key) || preg_match('/Variance$/', $new_key) ){
                    $dataRow[$new_key] = '[number]'.$val; // adding [number] so that we get pleasant to read numbers, not extreme decimals.
                }
                else if(preg_match('/Qty Wasted$/', $new_key)){
                    $dataRow[$new_key] = '[number]'.$val; // adding [number] so that we get pleasant to read numbers, not extreme decimals.
                }
				else if(preg_match('/Qty$/', $new_key) || preg_match('/PU Threshold$/', $new_key) || preg_match('/Qty Used$/', $new_key)){
				    $dataRow[$new_key] = '[number]'.$val; // adding [number] so that we get pleasant to read numbers, not extreme decimals.
                }
                else if(preg_match('/Cost of Goods Sold$/', $new_key)){
                    $dataRow[$new_key] = $val."%"; // adding [number] so that we get pleasant to read numbers, not extreme decimals.
                }
				// Add [money] keyword to financial figures
				else if (preg_match('/[0-9][.][0-9]/', $val)|| preg_match('/SUCost$/', $new_key)||preg_match('/SUExtended Cost$/', $new_key)) {
					$dataRow[$new_key] = '[money]'. $val;
				}
				else {
					$dataRow[$new_key] = $val;
				}
			}
			$data[] = $dataRow;
		}

		// MAYBE : reformat report data array for V2 Cross-Country Reporting (?)
		// [location_names][<loc_id>] = '<location_name>'
		// [location_currencies][<loc_id>] = '<location_currency>'

		return $data;
	}

	/**
	 * Send Inventory API request via cURL
	 *
	 * @param  $args Array of passed-in args with required key(s): report_api_path, SERVER_NAME, REQUEST_SCHEME, PHPSESSID
	 *
	 * @return JSON response string
	 */
	function makeInventoryApiCall($args)
	{
		// Assemble Inventory API endpoint URL
		$endpoint_url = $args['REQUEST_SCHEME'] .'://'. $args['SERVER_NAME'] . $this->endpoint_url_base .'/'. $args['report_api_path'];
		$strCookie = 'PHPSESSID=' . $args['PHPSESSID'] . '; path=/';
		session_write_close();
		// Init curl
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
		curl_setopt($ch, CURLOPT_POSTFIELDS, $args['payload_json']);
		curl_setopt($ch, CURLOPT_URL, $endpoint_url);
		curl_setopt($ch, CURLOPT_HEADER, 0);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 10);
		curl_setopt($ch, CURLOPT_TIMEOUT, 200);
		curl_setopt($ch, CURLOPT_COOKIESESSION,true);
		curl_setopt($ch, CURLOPT_COOKIE, $strCookie);
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
		$json_response = curl_exec($ch);
		curl_close($ch);
		return $json_response;
	}

}