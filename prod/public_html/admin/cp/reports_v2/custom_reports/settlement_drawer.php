<?php
require_once dirname(dirname(__FILE__)) . '/iarea_report_drawer.php';
require_once dirname(dirname(__FILE__)) . '/area_reports.php';
require_once dirname(dirname(__FILE__)) . '/report_functions.php';
require_once dirname(dirname(dirname(__FILE__))) . '/areas/reports/order_details_v2.php';

class settlement_drawer implements iarea_report_drawer {

	protected
		$area_reports,
		$settlement_report;

	public function __construct( $area_reports, $settlement_report ){
		$this->area_reports = $area_reports;
		$this->settlement_report = $settlement_report;
	}

	private function finalize( $table_str, $graph_str ) {
		$selector_result = $this->area_reports->datetime_selector();
		if( $this->area_reports->allow_controls ) {
			$selector_output = $selector_result['output'];
		} else {
			$selector_output = "";
		}

		ob_start();
		require 'templates/settlement_area.php';
		$output = ob_get_contents();
		ob_end_clean();

		$buttons_arr = array();

		if( $this->settlement_report->settlementReportMode() == REPORTS_V2_SETTLEMENT_MODE_OFFLINE_TIP_ENTRY ||
			$this->settlement_report->settlementReportMode() == REPORTS_V2_SETTLEMENT_MODE_OFFLINE_TIP_ENTRY ) {
			$report_mode = getvar('mode');
			$report_id = getvar('report');
			$buttons_arr[] = "<input type='button' onclick='com.lavu.reportsv2.settlement.submitTipAdjustments.call( this, {$report_id}, \"{$report_mode}\", event )' value='Submit Tip Adjustments' />";
			$buttons_arr[] = "<input type='button' onclick='com.lavu.reportsv2.settlement.settleBatch.call( this, {$report_id}, \"{$report_mode}\", event )' value='Settle Batch' />";
		}

		$output = str_replace('<![CDATA[REPORT_BREADCRUMBS]]>', $this->area_reports->breadcrumbs(), $output );
		$output = str_replace('<![CDATA[SETTLEMENT_DATE_SELECTOR_AREA]]>', $selector_output, $output );
		$output = str_replace('<![CDATA[SETTLEMENT_AREA]]>', $table_str, $output );
		$output = str_replace('<![CDATA[SETTLEMENT_SUBMISSION_BUTTONS]]>', implode('<br /><br />', $buttons_arr ), $output );

		return $output;
	}

	private function draw_payment_row( $payment ) {
		$first_four = report_value_is_number( $payment['first_four'] ) ? report_number_to_float( $payment['first_four'] ) : $payment['first_four'];
		$card_desc = report_value_is_number( $payment['card_desc'] ) ? report_number_to_float( $payment['card_desc'] ) : $payment['card_desc'];

		$first_four = str_pad( $first_four, 4, '0', STR_PAD_LEFT);
		$card_desc = str_pad( $card_desc, 4, '0', STR_PAD_LEFT);

		if( $first_four == '0000' ) {
			$first_four = '****';
		}
		$card_pan = str_pad( $first_four, 4, '0', STR_PAD_LEFT) . '********' . str_pad( $card_desc, 4, '0', STR_PAD_LEFT);
		$card_name = $payment['info'];
		$card_type = $payment['card_type'];

		$offline_entry_needs_to_be_submitted = false;
		if( $payment['process_data'] == 'LOCT' &&
			!report_value_is_number( $payment['temp_data'] ) &&
			$payment['temp_data'][0] != '[' ) {
			$offline_entry_needs_to_be_submitted = true;
			$card_details = $this->settlement_report->parseEncodedData( $payment['temp_data'] );
			$card_pan = isset($card_details['pan'])?$card_details['pan']:$card_pan;
			$card_name = isset($card_details['name'])?$card_details['name']:$card_name;
			$card_type = isset($card_details['type'])?$card_details['type']:$card_type;
		}

		$has_been_processed = report_number_to_float( $payment['processed'] ) == 1;

		$entry_classes_extra = array();
		$entry_attributes_extra = array();

		ob_start();
		require 'templates/settlement_payment_entry.php';
		$server_section_entry = ob_get_contents();
		ob_end_clean();

		ob_start();
		require 'templates/faux_card.php';
		$faux_card = ob_get_contents();
		ob_end_clean();

		$faux_card = str_replace('<![CDATA[FAUX_CARD_TYPE]]>', $card_type, $faux_card );
		$faux_card = str_replace('<![CDATA[FAUX_CARD_NAME]]>', $card_name, $faux_card );
		$faux_card = str_replace('<![CDATA[FAUX_CARD_PAN]]>', $card_pan, $faux_card );

		$payment_id = report_number_to_float( $payment['payment_id'] );
		$entry_attributes_extra[] = "payment=\"{$payment_id}\"";

		$auth_code = report_number_to_float( $payment['auth_code'] );
		$process_payment = '';

		$tip_amount = report_money_output( $payment['total_tip'] );
		$error_message = '';
		$error_attributes = array( 'hide' );
		global $locationid;
		global $data_name;
		static $integration_info = null;
		if( $integration_info === null ) {
			$integration_info = get_integration_from_location($locationid, "poslavu_".$data_name."_db");
		}
		$gateway = strtolower($integration_info['gateway']);

		if( $this->settlement_report->settlementReportMode() == REPORTS_V2_SETTLEMENT_MODE_OFFLINE_ORDERS &&
			$offline_entry_needs_to_be_submitted ) {
			$entry_classes_extra[] = 'offline';
			$auth_code = '';
			if( $has_been_processed ) {
				if( report_value_is_number( $payment['temp_data'] ) ) {
					$entry_attributes_extra[] = 'processed="approved"';
				} else {
					$entry_attributes_extra[] = 'processed="declined"';
					if( $payment['temp_data'][0] == '[' ) {
						$json_data = json_decode( $payment['temp_data'], true);
						$error_message = $json_data[0]['reason'];
						$error_attributes = array( );
					}
				}
			} else {
				$entry_attributes_extra[] = 'unprocessed';
				$report_mode = getvar('mode');
				$report_id = getvar('report');
				if( $gateway ==  strtolower( $payment['gateway'] ) ) {
					$process_payment = "<input name='offline_payment_{$payment_id}' type='button' onclick='com.lavu.reportsv2.settlement.processOfflinePayment.call( this, {$report_id}, \"{$report_mode}\", event )' value='Process Payment'/>";
				} else {
					$process_payment = 'Expected ' . $payment['gateway'] . ' Gateway';
				}
			}
		} else if( $this->settlement_report->settlementReportMode() == REPORTS_V2_SETTLEMENT_MODE_OFFLINE_TIP_ENTRY &&
					substr( $payment['process_data'], 0, 4 ) != 'LOCT' &&
					!$has_been_processed ) {
			$tip_amount = "<input name='tip_adjustment_payment_{$payment_id}' class='alignRight' type='text' value='{$tip_amount}' startingvalue='{$tip_amount}' oninput='com.lavu.reportsv2.settlement.updateValue.call( this, event )' />";
		} else if( substr( $payment['process_data'], 0, 4 ) == 'LOCT' ) {
			$auth_code = '';
			if( report_value_is_number( $payment['temp_data'] ) ) {
				$entry_attributes_extra[] = 'processed="approved"';
			} else {
				$entry_attributes_extra[] = 'processed="declined"';
				if( $payment['temp_data'][0] == '[' ) {
					$json_data = json_decode( $payment['temp_data'], true);
					$error_message = $json_data[0]['reason'];
					$error_attributes = array( );
				}
			}
		}

		$server_section_entry = str_replace( '<![CDATA[SETTLEMENT_PAYMENT_CLASSES_EXTRA]]>', implode(' ', $entry_classes_extra), $server_section_entry );
		$server_section_entry = str_replace( '<![CDATA[SETTLEMENT_PAYMENT_ATTRIBUTES_EXTRA]]>', implode(' ', $entry_attributes_extra), $server_section_entry );
		$server_section_entry = str_replace( '<![CDATA[SETTLEMENT_PAYMENT_CARD_DETAILS]]>', $faux_card, $server_section_entry );
		$server_section_entry = str_replace( '<![CDATA[SETTLEMENT_PAYMENT_ORDER_ID]]>', $payment['order_id'], $server_section_entry );
		$server_section_entry = str_replace( '<![CDATA[SETTLEMENT_PAYMENT_PAYMENT_ID]]>', $payment_id, $server_section_entry );
		$server_section_entry = str_replace( '<![CDATA[SETTLEMENT_PAYMENT_PAYMENT_REFERENCE]]>', report_value_is_number( $payment['ref'] ) ? report_number_to_float( $payment['ref'] ) : $payment['ref'], $server_section_entry );
		$server_section_entry = str_replace( '<![CDATA[SETTLEMENT_PAYMENT_TIME]]>', $payment['time'], $server_section_entry );
		$server_section_entry = str_replace( '<![CDATA[SETTLEMENT_PAYMENT_TIME_ISO]]>', date('c',strtotime($payment['time'])), $server_section_entry );
		$server_section_entry = str_replace( '<![CDATA[SETTLEMENT_PAYMENT_AMOUNT]]>', report_money_output( $payment['total_payment'] ), $server_section_entry );
		$server_section_entry = str_replace( '<![CDATA[SETTLEMENT_PAYMENT_TIP_AMOUNT]]>', $tip_amount, $server_section_entry );
		$server_section_entry = str_replace( '<![CDATA[SETTLEMENT_PAYMENT_AUTH_CODE]]>', $auth_code, $server_section_entry );
		$server_section_entry = str_replace( '<![CDATA[SETTLEMENT_PAYMENT_PROCESS_PAYMENT]]>', $process_payment, $server_section_entry );
		$server_section_entry = str_replace( '<![CDATA[SETTLEMENT_PAYMENT_ERROR_MESSAGE]]>', $error_message, $server_section_entry );
		$server_section_entry = str_replace( '<![CDATA[SETTLEMENT_PAYMENT_ERROR_ATTRIBUTES]]>', implode( ' ', $error_attributes ), $server_section_entry );

		return $server_section_entry;
	}

	//Expecting 3 loopable materials
	//
	// Section
	//  -> table
	//    -> row (columns)
	public function draw_multiple_rows( $report_results ) {
		$str = '';

		$server_sections_html = array();
		$total_edittable_count = 0;
		foreach( $report_results as $report_results_row => $server_section ){
			if( !count( $server_section ) ){
				continue;
			}

			$section = strtolower(str_replace(' ', '_', $server_section[0]['order_server'] ));
			$section_title = $server_section[0]['order_server'];
			if( $server_section[0]['process_data'] == 'LOCT' ) {
				$section_title = 'Offline Orders';
			}
			$last_order_id = '';
			$edittable_count = 0;

			$server_section_rows_html = array();
			foreach( $server_section as $payment_row => $payment ){
				if( !count( $payment ) ){
					continue;
				}

				$server_section_rows_html[] = $this->draw_payment_row( $payment );
			}

			$submit_adjustments_section_html = '';
			if( $edittable_count &&
				$this->settlement_report->settlementReportMode() == REPORTS_V2_SETTLEMENT_MODE_OFFLINE_TIP_ENTRY ){
				$submit_adjustments_section_html .= '<input type="submit" value="Submit Tip Changes for '.$section_title.'"/>';
			}

			ob_start();
			require 'templates/settlement_server_area.php';
			$server_section_html = ob_get_contents();
			ob_end_clean();

			$server_section_html = str_replace('<![CDATA[SETTLTEMENT_SERVER_SUBMIT_TIP_CHANGES]]>', $submit_adjustments_section_html, $server_section_html );
			$server_section_html = str_replace('<![CDATA[SETTLEMENT_ORDER_SERVER]]>', $section_title, $server_section_html );
			// $server_section_html = str_replace('<![CDATA[SETTLEMENT_HEADER_AREA]]>', implode("\n",$server_section_header_columns_html), $server_section_html );
			$server_section_html = str_replace('<![CDATA[SETTLEMENT_ROWS]]>', implode("\n",$server_section_rows_html), $server_section_html );

			$server_sections_html[] = $server_section_html;
		}
		if( $total_edittable_count ){
			// $str .= '<input type="submit" value="Settle/Batch"/>';
		}
		return $this->finalize(implode("\n", $server_sections_html), '');
	}

	public function draw_single_row( $report_results ) {
		return $this->draw_multiple_rows( $report_results );
	}

	public function draw_no_data() {
		return $this->finalize('No Payments to consider for Settlement', '');
	}

}