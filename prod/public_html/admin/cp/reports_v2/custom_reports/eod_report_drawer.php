<?php
require_once dirname(dirname(__FILE__)) . '/iarea_report_drawer.php';
require_once dirname(dirname(__FILE__)) . '/area_reports.php';
require_once dirname(dirname(__FILE__)) . '/report_functions.php';

class eod_report_drawer implements iarea_report_drawer {

	protected
		$area_reports;

	public function __construct( $area_reports ){
		$this->area_reports = $area_reports;
	}

	private function finalize( $table_str, $graph_str, $graphlist ) {
		$selector_result = $this->area_reports->datetime_selector();
			if($this->area_reports->allow_controls)
				$selector_output = $selector_result['output'];
			else
				$selector_output = "";
		$str = "";

		// return;
		$str .= $this->area_reports->breadcrumbs();
		$str .= "<div id=\"details_view\"></div>";
		$str .= "<table style='float: left;'><tr><td align='center'>";
		// if($selector_output!="")
			// $str .= "<br><br>";
		$str .= "<table><tr>";
		$str .= "<td align='left' valign='top' style='min-width:500px;'>";
		$str .= $selector_output;
		$str .= $table_str;

		$str .= "</td>";
		if($graph_str != "")
		{
			$str .= "<td>&nbsp;</td>";
			$str .= "<td align='left' valign='top'>";
			$str .= $graph_str;
			$str .= "</td>";
		}
		$str .= "</tr></table>";
		$str .= "</td></tr></table>";

		// $str .= $display_extra;

		if( $graphlist && count( $graphlist ) ){
			foreach( $graphlist as $columnName => $value ){
				$json = json_encode( $value['data'] );
				$str .= "<!--<canvas id=\"c_{$columnName}\" data='{$json}' width=\"500\" height=\"500\" style='width: 500px; height: 500px;' ></canvas>-->";
			}
		}

		return $str;
	}

	//Expecting 3 loopable materials
	//
	// Section
	//  -> table
	//    -> row (columns)
	public function draw_multiple_rows( $report_results ) {
		$report_results = array( $report_results );
		$str = '';
		foreach( $report_results as $report_results_index => $location ){
			$str .= '<table class="eod_table" cellspacing="0">';
			$str .= '<tbody>';
			foreach( $location as $location_index => $category ){
				if( array_key_exists('title',$category) && $category['title'] == 'Extra' ){
					$str .= '<tr class="eod_category_header" category="'.strtolower($category['title']).'"><td colspan="3" style="text-align: center;"><br /><h3>'.speak($category['title']).'</h3></td></tr>';
					$str .= '<td colspan="3"><div style="display: table; margin: 0 auto;">';
					foreach( $category as $category_index => $section ){
						if( $category_index == 'title' && !is_array($section) ){
							continue;
						}

						if( count( $section ) == 0){
							continue;
						}

						$str .= '<div style="float: left; padding: 5px; text-align: center;">';
						$str .= '<table style="border: 1px solid black;">';
						$it = 0;
						foreach( $section as $section_index => $row ){
							if( $it++ === 0 ){
								$str .= '<thead>';

								$str .= '<tr>';
								$keys = array_keys( $row );
								$first_key = $keys[0];
								foreach( $row as $column_name => $value ){
									if( !$this->area_reports->should_show_col( $column_name ) ){
										continue;
									}
									// if( $column_name == $first_key ){
									// 	$str .= '<th class="alignLeft">';
									// } else {
										$str .= '<th>';
									// }
									$str .= speak(show_as_title( $column_name ));
									$str .= '</th>';
								}
								$str .= '</tr>';
								$str .= '</thead>';
								$str .= '<tbody>';
							}
							$str .= '<tr>';
							foreach( $row as $column_name => $value ){
								if( !$this->area_reports->should_show_col( $column_name ) ){
									continue;
								}
								// if( $column_name == $first_key ){
									// $str .= '<td class="alignLeft">';
								// } else {
									$str .= '<td>';
								// }

								$display_value = $this->area_reports->show_reported_value( $value, $report_results,  $column_name);

								if( $column_name == 'open_orders' ){
									$value = report_date_to_number($value);
									$str .= "<span style=\"cursor: pointer;\" onclick=\"show_order_id(&quot;{$value}&quot;, event)\">".$display_value.'</span>';
								} else {
									$str .= $display_value;
								}
								$str .= '</td>';
							}
							$str .= '</tr>';
						}
						$str .= '</tbody>';
						$str .= '</table>';
						$str .= '</div>';
					}
					$str .= '</div></td>';
					continue;
				}
				foreach( $category as $category_index => $section ){
					if( $category_index == 'title' && !is_array($section) ){
						$str .= '<tr class="eod_category_header" category="'.strtolower($category['title']).'"><td colspan="3"><br /><h3>'.speak($section).'</h3></td></tr>';
						continue;
					}
					if( count( $section ) == 0 ){
						continue;
					}

					$it = 0;
					$subtotals = array();
					foreach( $section as $section_index => $row ){
					    foreach( $row as $section_index => $row ){ 
						if( $it++ === 0 ){
							$str .= '<tr class="eod_spacer"><td><br /></td></tr>';
							$row_first_key = $row;
							if(is_array($row)){
								$row_first_key = array_keys( $row );
								$row_second_key = $row_first_key[1];
								$row_third_key = isset($row_first_key[2])?$row_first_key[2]:"";
								$row_first_key = $row_first_key[0];
								$str .= '<tr class="eod_section_header" category="'.strtolower($category['title']).'" section="'.strtolower(str_replace(" ", "_", $row_first_key)).'">';
								foreach( $row as $column_name => $value ){
									if( !$this->area_reports->should_show_col( $column_name ) ){
										continue;
									}
									$recolor='';
									if ( $column_name == $row_third_key || $column_name == $row_first_key  ){
										$recolor = " recolor=\"{$row_first_key}_{$row_third_key}\" ";
									}
									
									if( report_value_is_number( $value ) || report_value_is_money( $value ) ){
										$str .= "<th class=\"alignRight\"{$recolor}><b>" . speak(show_as_title($column_name)) . '</b></th>';
									} else {
										$str .= "<th class=\"alignLeft\"`{$recolor}><b>" . speak(show_as_title($column_name)) . '</b></th>';
									}
								}
							}
							$str .= '</tr>';
						}
						
						if(array_key_exists('title',$category)){
							$str .= '<tr class="eod_data_row" category="'.strtolower($category['title']).'" section="'.strtolower(str_replace(" ", "_", $row_first_key)).'">';
						}
						
						if(is_array($row)){
							foreach( $row as $column_name => $value ){
								if( !$this->area_reports->should_show_col( $column_name ) ){
									continue;
								}
								$recolor = '';
								if ( $row_second_key == 'summary' ){
									$color_title = str_ireplace( " ", "_", strtolower( $row[$row_second_key] ));
									$recolor = " recolor=\"{$color_title}\" ";
								}
								
								if( !isset( $subtotals[$column_name] ) ){
									$subtotals[$column_name] = 0;
								}
								$display_value = $this->area_reports->show_reported_value( $value, $report_results, $column_name );
								if( report_value_is_number( $value ) || report_value_is_money( $value ) ){
									if( report_value_is_number( $value ) ){
										$value = str_replace(",", "", $value );
										$str .= "<td class=\"alignRight\" {$recolor}><div>" . $display_value . '</div></td>';
										$subtotals[$column_name] =  report_float_to_number( report_number_to_float($subtotals[$column_name]) + report_number_to_float($value) );
									} else {
										$str .= "<td class=\"alignRight\" {$recolor}><div>" . $display_value . '</div></td>';
										$total_money = sprintf('%0.4f', (report_number_to_float(report_money_to_number($subtotals[$column_name])) + report_number_to_float(report_money_to_number($value)) ));
										$subtotals[$column_name] = '[money]'. $total_money;
									}
								} else {
									if( $column_name == 'order_id' ){
										$value = report_date_to_number($value);
										$str .= "<td class=\"alignLeft\" {$recolor}><div style=\"cursor: pointer;\" onclick=\"show_order_id(&quot;{$value}&quot;, event)\">".$display_value.'</div></td>';
									} else {
										$str .= "<td class=\"alignLeft\" {$recolor}><div>" . $display_value . '</div></td>';
									}
									$subtotals[$column_name] = '';
								}
							}
						}
						$str .= '</tr>';
					}
				}

					if(count($section)>5){
						$str .= '<tr onclick="toggle_section(this)" section="'.strtolower(str_replace(" ", "_", $row_first_key)).'" style="text-align: center" selected><td colspan="3" style="color: blue; cursor: pointer;">'.speak('Show More').'</td></tr>';
					}
					
					if(array_key_exists('title',$category)){
						$str .= '<tr class="eod_subtotal" category="'.strtolower($category['title']).'" section="'.strtolower(str_replace(" ", "_", $row_first_key)).'">';
					}
					
					$it = 0;
					foreach( $subtotals as $column_name => $value ){
						$recolor='';
						$display_value = $this->area_reports->show_reported_value( $value, $report_results, $column_name );
						if ( $column_name == $row_third_key ){
							$recolor = " recolor=\"{$row_first_key}_{$column_name}\" ";
						}
						$str .= "<td class=\"alignRight\"{$recolor}>" . $display_value . '</td>';
					}
					$str .= '</tr>';
				}
			}
			$str .= '</tbody>';
			$str .= '</table>';
		}
		return $this->finalize($str, '', array());
	}

	public function draw_single_row( $report_results ) {
		return $this->draw_multiple_rows( $report_results );
	}

	public function draw_no_data() {
		return speak('No EOD Data Found');
	}

}