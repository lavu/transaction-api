<?php
/**
 * Created by PhpStorm.
 * User: Chris
 * Date: 11/9/17
 * Time: 2:47 PM
 */

require_once(resource_path()."/mealplan_functions.php");

function mealplan_cashusage($args){
    $args['type'] = 'cash_balance';
    $report_data = getMealPlanReportData($args);
    $counter=0;
    $dataArray = array();
    if(count($report_data) > 0) {
        foreach($report_data as $singleRow) {
            $dataArray[0][$counter][0]=$singleRow;
            $counter++;
        }
    }
    return $dataArray;
}