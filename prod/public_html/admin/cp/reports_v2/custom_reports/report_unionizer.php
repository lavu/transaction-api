<?php
define( 'REPORTS_V2_SELECTION_COLUMN_KEY', 'selection_column' );
define( 'REPORTS_V2_SPECIAL_CODE_KEY', 'special_code' );
define( 'REPORTS_V2_SELECTION_KEY', 'selection' );
define( 'REPORTS_V2_TABLE_KEY', 'table' );
define( 'REPORTS_V2_TITLE_KEY', 'title' );
define( 'REPORTS_V2_NAME_KEY', 'name' );
define( 'REPORTS_V2_ORDERBY_KEY', 'orderby' );
define( 'REPORTS_V2_SELECT_KEY', 'select' );
define( 'REPORTS_V2_GROUPBY_KEY', 'groupby' );
define( 'REPORTS_V2_WHERE_KEY', 'where' );
define( 'REPORTS_V2_JOIN_KEY', 'join' );
define( 'REPORTS_V2_ID_KEY', 'id' );
define( 'REPORTS_V2_ORDER_KEY', '_order' );
define( 'REPORTS_V2_DELETED_KEY', '_deleted' );
define( 'REPORTS_V2_FIELD_SEPERATOR', '[_sep_]' );

define( 'REPORTS_V2_DRILL_DOWN_SEPERATOR', '[_dsep_]' );
define( 'REPORTS_V2_DRILL_DOWN_COLUMN_SEPERATOR', '(col)');

require_once dirname(dirname(__FILE__)).'/black_hole_report_drawer.php';

class report_unionizer {
	private function runReport( $report_details ){
		static $it = 0;
		$ar = new area_reports( array('display_query' => 1) );
		$ar->report_read = $report_details;
		$ar->report_id = $ar->report_read['id'];
		$ar->report_name = $ar->report_read['name'];
		$ar->report_title = $ar->report_read['title'];
		$ar->report_selection = $ar->report_read['selection'];
		$ar->report_selection_column = $ar->report_read['selection_column'];

		$ar->report_drawer = new black_hole_report_drawer();
		$ar->show_query_table();

		if ($it++ === 0 ){
			$this->area_reports->groupby_cols = $ar->groupby_cols;
			$this->area_reports->firstcol = $ar->firstcol;
			$this->area_reports->firstcol_name = $ar->firstcol_name;
		}
		return $ar->report_results;
	}

	private function combineResults( &$result1, $result2, $field_offsets ){
	    foreach( $result2 as $key => $value ){
			$result1[] = $value;
		}
	}

	public function countRP( $item ){
		$result = 1;
		for( $i = 0; $i < count($item); $i++ ){
			if( substr($item[$i], 0,3) != 'RP(' ){
				continue;
			}
			
			$item_str = $item[$i];
			$item_arr = explode(',', $item_str);

			$result = MAX($result, count($item_arr));
		}
		return $result;
	}

	public function parseRP( $item, $it, &$indexes=null ){
		for( $i = 0; $i < count($item); $i++ ){
			if( substr($item[$i], 0,3) != 'RP(' ){
				continue;
			}
			if($indexes !== null && is_array($indexes)){
				$indexes[] = $i;
			}
			$item_str = substr($item[$i], 3);
			$item_str = substr($item_str, 0, strlen($item_str)-1);

			$item_arr = explode(',', $item_str);
			$item[$i] = $item_arr[$it];
		}
		return $item;
	}

	public function parseRPS( $item, $it ){
		for( $i = 0; $i < count($item); $i++ ){
			if( substr($item[$i], 0,4) != 'RPS(' && substr($item[$i], 0,3) != 'RP(' ){
				continue;
			}

			$item_str = substr($item[$i], 4);
			$item_str = substr($item_str, 0, strlen($item_str)-1);

			$item_arr = explode(',', $item_str);
			$item[$i] = $item_arr[$it];
		}
		return $item;
	}

	public function run( $area_reports ){
		$this->area_reports = $area_reports;
		$results = array();

		$multi_report = false;

		$get_filter_cols = isset($_GET['filter_cols'])?$_GET['filter_cols']:'';
		$filter_cols = explode(REPORTS_V2_DRILL_DOWN_SEPERATOR, $get_filter_cols );
		$groupbys = explode(REPORTS_V2_FIELD_SEPERATOR, $area_reports->report_read['groupby']);
		$joins = explode(REPORTS_V2_FIELD_SEPERATOR, $area_reports->report_read['join']);
		// $orderbys = explode(REPORTS_V2_FIELD_SEPERATOR, $area_reports->report_read['orderby']);
		$selects = explode(REPORTS_V2_FIELD_SEPERATOR, $area_reports->report_read['select']);
		$wheres = explode(REPORTS_V2_FIELD_SEPERATOR, $area_reports->report_read['where']);
		$number_of_reports_to_run = $this->countRP($selects);
		
		$area_reports->verify_rj_loaded(true);
		$area_reports->rj->verify_aliases_are_loaded();
		for( $i=0; $i < $number_of_reports_to_run; $i++ ){
			$select_grouping_indexes = array();
			$individual_report_groupbys = $this->parseRP($groupbys, $i);
			$individual_report_joins = $this->parseRP($joins, $i);
			// $individual_report_orderbys = $this->parseRP($orderbys, $i);
			$individual_report_selects = $this->parseRP($selects, $i, $select_grouping_indexes);
			$individual_report_wheres = $this->parseRP($wheres, $i);

			$individual_report_groupbys = $this->parseRPS($individual_report_groupbys, $i);
			$individual_report_joins = $this->parseRPS($individual_report_joins, $i);
			// $individual_report_orderbys = $this->parseRPS($individual_report_orderbys, $i);
			$individual_report_selects = $this->parseRPS($individual_report_selects, $i);
			$individual_report_wheres = $this->parseRPS($individual_report_wheres, $i);
			// echo '<pre style="text-align: left;">' . print_r( $individual_report_groupbys, true ) . '</pre>';

			$tables = $this->parseRP( array( $area_reports->report_read['table'] ), $i );

			if( isset( $_GET['filter_cols'] ) && $i !== 0){
				//Need to Replace the Drillvar for each sub-report with the appropriate tablename/fieldname
				$groupby_stmt = '';
				$temp_groupbys = $individual_report_groupbys;
				foreach( $temp_groupbys as $key => $groupby ){
					if( $groupby_stmt != '' ){
						$groupby_stmt .= ', ';
					}
					if( $groupby[0] == '#' ){//alias
						$alias = report_alias::alias( substr($groupby, 1) );
						$groupby_stmt .= $alias->statement();
					} else {
						$groupby_stmt .= $groupby;
					}
				}

				if( $groupby_stmt != '' ){
					$groupby_stmt = 'group by ' . $groupby_stmt;
				}

				// echo 'MY CALL<br />';
				$groupby_columns = $area_reports->parse_groupby_cols( $groupby_stmt );
				$it = 0;
				$groupby_column_str = implode(REPORTS_V2_DRILL_DOWN_COLUMN_SEPERATOR, $groupby_columns);
				$temp = $filter_cols;
				$temp[0] = $groupby_column_str;
				//Replace initial group by filters with the appropriate ones.

				$_GET['filter_cols'] = implode(REPORTS_V2_DRILL_DOWN_SEPERATOR, $temp);
			}

			$report = $this->runReport( array(
				REPORTS_V2_SELECTION_COLUMN_KEY => $area_reports->report_read['selection_column'],
				REPORTS_V2_SPECIAL_CODE_KEY => '',
				REPORTS_V2_SELECTION_KEY => $area_reports->report_read['selection'],
				REPORTS_V2_TABLE_KEY => $tables[0],
				REPORTS_V2_TITLE_KEY => $area_reports->report_title,
				REPORTS_V2_NAME_KEY => $area_reports->report_name,
				REPORTS_V2_ORDERBY_KEY => $area_reports->orderby,
				REPORTS_V2_SELECT_KEY => implode( REPORTS_V2_FIELD_SEPERATOR, $individual_report_selects),
				REPORTS_V2_GROUPBY_KEY => implode( REPORTS_V2_FIELD_SEPERATOR, $individual_report_groupbys),
				REPORTS_V2_WHERE_KEY => implode( REPORTS_V2_FIELD_SEPERATOR, $individual_report_wheres),
				REPORTS_V2_JOIN_KEY => implode( REPORTS_V2_FIELD_SEPERATOR, $individual_report_joins),
				REPORTS_V2_ID_KEY => 'unionizer-'.$area_reports->report_read['id'].'-'.$i,
				REPORTS_V2_ORDER_KEY => '',
				REPORTS_V2_DELETED_KEY => '',
			));

			if( count( $report ) === 1 ) { // probably not a group report
				$report = $report[0];
				$groupby_cols = $this->area_reports->groupby_cols;
				foreach( $groupby_cols as $key => $groupby_col ){
					$groupby_cols[$key] = 'group_col_'.str_replace('.', '_', $groupby_col );
				}
				foreach( $report as $result_row => $result ){
					$it = 0;
					foreach( $result as $column => $field ){
						if( substr($column, 0, 10) ==  'group_col_' ){
							if( !in_array($column, $groupby_cols) ){
								$group_col = $groupby_cols[$it];
								$report[$result_row][$group_col] = $report[$result_row][$column];
								unset( $report[$result_row][$column] );
							}
							$it++;
						}
					}
				}
				foreach( $report as $key => $value ){
					$this->combineResults( $results, array($value), $select_grouping_indexes );
				}
			} else {
				$multi_report = true;
				// multi report
				$keys = array_merge(array_keys($report), $results);
				$groupby_cols = $this->area_reports->groupby_cols;
				
				foreach( $groupby_cols as $key => $groupby_col ){
					$groupby_cols[$key] = 'group_col_'.str_replace('.', '_', $groupby_col );
				}
				foreach( $keys as $index ) {
					if( !isset($results[$index]) ) {
						$results[$index] = array();
					}
					
					// LP-1691 -- Fixed location_currencies not being persisted by combineResults()
					if( $index === 'location_names' || $index === 'location_currencies' ) {
						foreach( $report[$index] as $key => $value ) {
							if( isset( $results[$index][$key] ) && $results[$index][$key] != $value ) {
								// echo '['.__FILE__.':'.__LINE__.'] LOCATION MISMATCH<br />';
							   
							}
							$results[$index][$key] = $value;
						}
						continue;
					}
					
					foreach( $report[$index] as $result_row => $result ){
						$it = 0;
						foreach( $result[0] as $column => $field ){
							if( substr($column, 0, 10) ==  'group_col_' ){
								if( !in_array($column, $groupby_cols) ){
									$group_col = $groupby_cols[$it];
									$report[$index][$result_row][0][$group_col] = $report[$index][$result_row][0][$column];
									unset( $report[$index][$result_row][0][$column] );
								}
								$it++;
							}
						}
					}
					 foreach( $report[$index] as $key => $value ){
						$this->combineResults( $results[$index], array($value), $select_grouping_indexes );
					} 
					if( count( $results[$index] ) === 0 ){
						unset( $results[$index] );
					}
				}
			}
		}
		
		$selector_result = $area_reports->datetime_selector();
		if($area_reports->allow_controls) {
			$selector_output = $selector_result['output'];
		} else {
			$selector_output = "";
		}
		$selector_id = $selector_result['id'];
		$orderby_var = "orderby_".$selector_id;
		$orderby = memvar($orderby_var,"");
		if( !$multi_report ) {
			//re-sort after combining
			if( !empty($orderby) ){
				$orderby_field = explode(' ', $orderby);
				$orderby_field = $orderby_field[0];
				$this->orderby_field = $orderby_field;
				if( substr($orderby, -4) == 'desc' ){
					uasort( $results, array( $this, 'sortDescending'));
				} else {
					uasort( $results, array( $this, 'sortAscending'));
				}
			}
			return array_values($results);
		}

		//reindex?
		// $newResults = array( 'location_names' => array() );
		// $newIndex = 0;
		// foreach( $results as $index => $value ) {
		// 	if( $index == 'location_names' || count( $value ) === 0 ) {
		// 		continue; // skip
		// 	}

		// 	$newResults[$newIndex] = $value;
		// 	$newResults['location_names'][$newIndex] = $results['location_names'][$index];
		// 	$newIndex++;
		// }
		// $results = $newResults;

		if( !empty($orderby) ){
			$orderby_field = explode(' ', $orderby);
			$orderby_field = $orderby_field[0];
			$this->orderby_field = $orderby_field;
			foreach( $results as $key => $value ) {
				if( $key == 'location_names' ) {
					continue;
				}
				if( substr($orderby, -4) == 'desc' ){
					uasort( $results[$key], array( $this, 'sortDescending'));
				} else {
					uasort( $results[$key], array( $this, 'sortAscending'));
				}

				$results[$key] = array_values( $results[$key] );
			}
		}
		return $results;
	}

	private function sortDescending( $row1, $row2 ){
		if( !is_array($row1) || !is_array($row2) ){
			return 0;
		}
		$orderby_field = $this->orderby_field;

		if( !isset($row1[$orderby_field]) || !isset($row2[$orderby_field]) ){
			return 0;
		}

		$val1 = $row1[$orderby_field];
		$val2 = $row2[$orderby_field];

		if( report_value_is_money( $val1 ) ){
			$val1 = report_money_to_number( $val1 );
			$val2 = report_money_to_number( $val2 );
		} else if( report_value_is_number( $val1 ) ){
			$val1 = report_number_to_float($val1);
			$val2 = report_number_to_float($val2);
		}

		if( $val1 < $val2 ){
			return 1;
		} else if( $val1 > $val2 ){
			return -1;
		}
		return 0;
	}

	private function sortAscending( $row1, $row2 ){
		if( !is_array($row1) || !is_array($row2) ){
			return 0;
		}
		$orderby_field = $this->orderby_field;

		if( !isset($row1[$orderby_field]) || !isset($row2[$orderby_field]) ){
			return 0;
		}

		$val1 = $row1[$orderby_field];
		$val2 = $row2[$orderby_field];

		if( report_value_is_money( $val1 ) ){
			$val1 = report_money_to_number( $val1 );
			$val2 = report_money_to_number( $val2 );
		} else if( report_value_is_number( $val1 ) ){
			$val1 = report_number_to_float($val1);
			$val2 = report_number_to_float($val2);
		}

		if( $val1 < $val2 ){
			return -1;
		} else if( $val1 > $val2 ){
			return 1;
		}
		return 0;
	}
}