<?php
define( 'REPORTS_V2_SELECTION_COLUMN_KEY', 'selection_column' );
define( 'REPORTS_V2_SPECIAL_CODE_KEY', 'special_code' );
define( 'REPORTS_V2_SELECTION_KEY', 'selection' );
define( 'REPORTS_V2_TABLE_KEY', 'table' );
define( 'REPORTS_V2_TITLE_KEY', 'title' );
define( 'REPORTS_V2_NAME_KEY', 'name' );
define( 'REPORTS_V2_ORDERBY_KEY', 'orderby' );
define( 'REPORTS_V2_SELECT_KEY', 'select' );
define( 'REPORTS_V2_GROUPBY_KEY', 'groupby' );
define( 'REPORTS_V2_WHERE_KEY', 'where' );
define( 'REPORTS_V2_JOIN_KEY', 'join' );
define( 'REPORTS_V2_ID_KEY', 'id' );
define( 'REPORTS_V2_ORDER_KEY', '_order' );
define( 'REPORTS_V2_DELETED_KEY', '_deleted' );
define( 'REPORTS_V2_FIELD_SEPERATOR', '[_sep_]' );

require_once dirname(dirname(__FILE__)).'/black_hole_report_drawer.php';
require_once dirname(__FILE__).'/settlement_drawer.php';

define( 'REPORTS_V2_SETTLEMENT_MODE_OFFLINE_ORDERS',		0x01 );
define( 'REPORTS_V2_SETTLEMENT_MODE_OFFLINE_TIP_ENTRY',		0x02 );
define( 'REPORTS_V2_SETTLEMENT_MODE_OFFLINE_SETTLE_BATCH',	0x03 );

class settlement {
	private
		$settlement_report_mode;

	public function settlementReportMode(){
		return $this->settlement_report_mode;
	}

	private function runReport( $report_details ){
		$ar = new area_reports( array('display_query' => 1) );
		$ar->report_read = $report_details;
		$ar->report_id = $ar->report_read['id'];
		$ar->report_name = $ar->report_read['name'];
		$ar->report_title = $ar->report_read['title'];
		$ar->report_selection = $ar->report_read['selection'];
		$ar->report_selection_column = $ar->report_read['selection_column'];

		$ar->report_drawer = new black_hole_report_drawer();
		$ar->show_query_table();
		return $ar->report_results[0];
	}

	public function parseEncodedData( $temp_data ) {
		$card_details = array();
		$temp_data_parts = explode( chr(0x1E), $temp_data );
		$a_matches = array();
		for( $i = 0; $i < count( $temp_data_parts ); $i++ ){
			if( preg_match("/data:([^,;]+)(?:;([^,]+),(.*))/", $temp_data_parts[$i], $a_matches ) ) {
				$mime_type = $a_matches[1];
				$encoding = $a_matches[2];
				$data = $a_matches[3];

				$mime_parts = explode('/', $mime_type );
				$mime_super = $mime_parts[0];
				$mime_sub = isset($mime_parts[1])?$mime_parts[1]:'';

				$supported_encodings = array(
					'lc',
					'base64',
					'hex',
				);

				$encoding_order = array();
				while( $encoding != '' ){
					$before_len = strlen($encoding);
					foreach( $supported_encodings as $encoding_type ) {
						// echo "test: substr( $encoding, 0, strlen( $encoding_type )) == $encoding_type:" . (substr( $encoding, 0, strlen( $encoding_type )) == $encoding_type)*1 . '<br />';;
						if( substr( $encoding, 0, strlen( $encoding_type )) == $encoding_type ){
							$encoding_order[] = $encoding_type;
							$encoding = substr( $encoding, strlen( $encoding_type ) );
							break;
						}
					}
					if( $before_len == strlen( $encoding ) ) {
						break;
						// prevent infinite loop... didn't detect any recognized encodings.
					}
				}

				if( $mime_super == 'lc' ){
					require_once dirname(dirname(dirname(dirname(__FILE__)))) . '/lib/gateway_functions.php';
					$encoding_order = array_reverse( $encoding_order );
					foreach( $encoding_order as $encoding ) {
						switch( $encoding ){
							case 'lc':
								$data = interpret( $data );
								break;
							case 'hex':
								$data = hexToString( $data );
								break;
							case 'base64':
								$data = str_replace('+', '-', $data);
								$data = str_replace('/', '_', $data);
								$data = base64_decode( $data );
								break;
							default:
								//que?
						}
					}
					// echo '['.__FILE__.':'.__LINE__.'] Here:' . ' <pre style="text-align:left;white-space:pre-wrap;max-width:100em;`">' . print_r( $data, true ) . '</pre>';

					$a_card_matches = array();

					$card_track = $data;
					$card_details['msr'] = $card_track;
					$card_track_pieces = str_split($card_track );

					if( substr($mime_sub,0,3) == 'ete' ) {
						$card_details['encrypted'] = '1';
						if( substr($mime_sub,3) != '' ){
							$card_details['encrypted'] = substr($mime_sub,3);
						}
					}

					$masked_tracks = '';
					$started = false;
					for( $j = 0; $j < count( $card_track_pieces ); $j++ ){
						if( $card_track_pieces[$j] == ';' ||
							$card_track_pieces[$j] == '%' ||
							$started ) {
							$started = true;
							if( ord( $card_track_pieces[$j] ) < 0x1F ) {
								break;
							}
							$masked_tracks .= $card_track_pieces[$j];
						}
					}

					if( preg_match("/(%([A-Z\\*])([0-9\\*\\ ]{1,19})\\^([^\\^]{2,})\\^([0-9\\*]{4}|\\^)([0-9\\*]{3}|\\^)([^\\?]+)\\?[0-9\\*]?)?(;([0-9\\*]{1,19})=([\\d\\*]{4}|=)([\\d\\*]{3}|=)([\\d\\*]*)?\\?([\\d\\*]*))?/", $masked_tracks, $a_card_matches ) ) {
						if( $a_card_matches[1] != '' ){ // Have Track 1 Masked
							$card_details['pan'] = $a_card_matches[3];
							$card_details['name'] = $a_card_matches[4];
						}

						if( $a_card_matches[8] != '' ){ // Have Track 2 Masked
							$card_details['pan'] = $a_card_matches[9] != '' ? $a_card_matches[9] : $card_details['pan'];
						}

						$card_pan = $card_details['pan'];
						$card_details['type'] = getCardTypeFromNumber($card_pan);
					}

				} else if( $mime_super == 'image' ) {
					if( in_array('base64', $encoding_order) ){
						$data = str_replace('+', '-', $data);
						$data = str_replace('/', '_', $data);
					}
					if( !isset( $card_details['signatures'] ) ){
						$card_details['signatures'] = array();
					}
					$card_details['signatures'][] = $data;

				}
			} else {
			}
		}
		return $card_details;
	}

	private function processOfflinePayment( $area_reports ){
		$area_reports->report_drawer = new black_hole_report_drawer();
		$result = array();
		$result['status'] = 'failed';
		$offline_payment = isset($_POST['offline_payment'])?$_POST['offline_payment']:null;
		if( $offline_payment !== null && ($query_result = lavu_query( "SELECT * FROM `cc_transactions` WHERE `id` = '[1]' AND `process_data` LIKE 'LOCT%' AND `processed`='0'", $offline_payment )) !== FALSE ){
			$row = null;
			while( $res = mysqli_fetch_assoc($query_result) ){
				$row = $res;
			}

			if( $row !== null ){
				global $locationid;
				global $data_name;

				$attempt_count = 0;
				if( substr( $row['process_data'], 5 ) != '' ){
					$attempt_count = substr( $row['process_data'], 5 ) * 1;
				}
				$card_details = $this->parseEncodedData( $row['temp_data'] );

				if( !isset( $card_details['encrypted'] ) ) {
					// Card is not encrypted, or at least is wasn't reported as encrypted.
					// this means that the encoded data doesn't specify ete, which is why
					// we'd assume unencrypted.  This shouldn't exist in the database. At
					// the same time, we shouldn't process it to prevent the encourangement
					// of it's use.
					$result['reason'] = 'No Encrypted Card Data Found';
					$result['card_details'] = $card_details;
					$result['row'] = $row;
					echo json_encode( $result );
					exit();
				}


				$encrypted = $card_details['encrypted'];
				if( $row['reader'] == 'llsswiper' ) {
					$encrypted = '2';
				}
				require_once dirname(dirname(dirname(dirname(__FILE__)))) . $dev_dir . '/lib/gateway_functions.php';
				if( !in_array(strtolower($row['reader']), 'idynamo', 'udynamo' ) ) {
					// echo $card_details['msr'] . "\n";
					$card_details['msr'] = hex_string( $card_details['msr'] );
					// echo $card_details['msr'] . "\n";
					// exit();
				}

				$integration_info = get_integration_from_location($locationid, "poslavu_".$data_name."_db");
				$process_info = array();
				$process_info['username']			= $integration_info['integration1'];
				$process_info['password']			= $integration_info['integration2'];
				$process_info['integration3']		= $integration_info['integration3'];
				$process_info['integration4']		= $integration_info['integration4'];
				$process_info['integration5']		= $integration_info['integration5'];
				$process_info['integration6']		= $integration_info['integration6'];

				$process_info['card_number']		= $card_details['pan'];
				$process_info['encrypted']			= $encrypted;
				$process_info['mag_data']			= $card_details['msr'];
				$process_info['name_on_card']		= $card_details['name'];

				$process_info['auth_by_id']			= $row['pin_used_id'];
				$process_info['auth_by']			= '';
				$process_info['card_amount']		= $row['total_collected'];
				$process_info['check']				= $row['check'];
				$process_info['device_udid']		= $row['device_udid'];
				$process_info['for_deposit']		= $row['for_deposit'];
				$process_info['ioid']				= $row['ioid'];
				$process_info['is_deposit']			= $row['is_deposit'];
				$process_info['order_id']			= $row['order_id'];
				$process_info['reader']				= $row['reader'];
				$process_info['register']			= $row['register'];
				$process_info['register_name']		= $row['register_name'];
				$process_info['server_id']			= $row['server_id'];
				$process_info['server_name']		= $row['server_name'];
				$process_info['set_pay_type']		= $row['pay_type'];
				$process_info['set_pay_type_id']	= $row['pay_type_id'];
				$process_info['tip_amount']			= $row['tip_amount'];
				$process_info['transtype']			= $row['transtype'];

				$process_info['data_name']			= $data_name;
				$process_info['device_time']		= localize_datetime(date("Y-m-d H:i:s"), $location_info['timezone']);
				$process_info['loc_id']				= $locationid;

				$process_info['item_db_id']			= false;
				$process_info['refund_tip_amount']	= 0;

				$process_info['authcode']			= '';
				$process_info['card_cvn']			= '';
				$process_info['card_present']		= '1';
				$process_info['exp_month']			= '';
				$process_info['exp_year']			= '';
				$process_info['ext_data']			= '';
				$process_info['info']				= '';
				$process_info['itid']				= '';
				$process_info['more_info']			= '';
				$process_info['pnref']				= '';
				$process_info['poslavu_build']		= '20150827-1BE';
				$process_info['poslavu_version']	= '3.0.0';

				$gateway = strtolower($integration_info['gateway']);
				$gateway_function_name = 'process_' . $gateway;
				//let's give it a try

				require_once dirname(dirname(dirname(dirname(__FILE__)))) . '/lib/gateway_settings.php';
				$extra_function_file = dirname(dirname(dirname(dirname(__FILE__)))) . '/lib/gateway_lib/Transaction_Controller.php';

				if (file_exists($extra_function_file)) {
					require_once $extra_function_file;
				}
				global $location_info;
				$transactionController = new Transaction_Controller($gateway, $process_info);
				$transactionController->process_transaction($process_info, $location_info);
				$response = $transactionController->getResponse();
				$response_parts = explode('|', $response );
				$result['pnref'] = $response_parts[2];
				switch( $response_parts[0] ) {
					case '1':
						$success_parts = array(
							'flag',
							'transaction_id',
							'card_desc',
							'response_message',
							'auth_code',
							'card_type',
							'record_no',
							'amount',
							'order_id',
							'ref_data',
							'process_data',
							'new_balanace',
							'bonus_message',
							'first_four',
						);

						for( $i = 1; $i < min( count($success_parts), count( $response_parts )); $i++ ){
							if( $response_parts[$i] == '' ) {
								continue;
							}
							$result[ $success_parts[$i] ] = $response_parts[$i];
							$result['status'] = 'success';
						}

						//Update existing record
						if( lavu_query( "UPDATE `cc_transactions` SET `processed`='1', `temp_data` = '[2]' WHERE `id` = '[1]'", $offline_payment, $result['transaction_id'] ) === FALSE ){
							//failed... dammit
							$result['mysql_failed'] = lavu_dberror();
						}
						break;
					case '0':
					default :
						$failure_parts = array(
							'flag',
							'reason',
							'transaction_id',
							'order_id',
							'???',
							'response_title',
						);
						for( $i = 1; $i < min(count($failure_parts), count( $response_parts )); $i++ ){
							if( $response_parts[$i] == '' ) {
								continue;
							}
							$result[ $failure_parts[$i] ] = $response_parts[$i];
						}

						if( !strstr($result['reason'], 'No response received from gateway.') ) {
							$total_ref_data = array();
							if( $row['temp_data'][0] == '[' ) {
								$total_ref_data = json_decode( $row['ref_data'] );
							}
							$total_ref_data[] = $result;

							// Update existing record to say we attempted it... I doubt we'll
							// actually be able to retry the transaction again.  However, we
							// will see if we can for now. In general, we should just mark the
							// transaction as processed and set the temp_data field to empty.
							if( lavu_query( "UPDATE `cc_transactions` SET `processed`='1', `temp_data` = '[2]' WHERE `id` = '[1]'", $offline_payment, json_encode( $total_ref_data ) ) === FALSE ){
								//Handle the failure
								$result['mysql_failed'] = lavu_dberror();
							}
						}
				}
			} else {
				$result['reason'] = speak('No Records Found');
			}
		} else {
			$result['reason'] = speak('Query Error');
		}

		echo json_encode( $result );
		exit();
	}

	private function processTipAdjustments( $area_reports ) {
		$area_reports->report_drawer = new black_hole_report_drawer();
		global $location_info;
		global $locationid;
		global $data_name;

		$decimal_places = $location_info['disable_decimal'];
		$smallest_money = (1 / pow(10, $decimal_places));

		$integration_info = get_integration_from_location($locationid, "poslavu_".$data_name."_db");
		$process_info = array();
		$process_info['mag_data'] = "";
		$process_info['reader'] = "";
		$process_info['card_number'] = "";
		$process_info['card_cvn'] = "";
		$process_info['name_on_card'] = "";
		$process_info['exp_month'] = "";
		$process_info['exp_year'] = "";
		$process_info['card_amount'] = "";
		$process_info['company_id'] = admin_info('companyid');
		$process_info['loc_id'] = $location_info['id'];
		$process_info['ext_data'] = "";
		$process_info['data_name'] = admin_info('dataname');
		$process_info['register'] = "Back End";
		$process_info['register_name'] = "Back End";
		$process_info['device_udid'] = "";
		$process_info['server_id'] = admin_info('loggedin');
		$process_info['server_name'] = $server_name;
		$process_info['device_time'] = $device_time;
		$process_info['set_pay_type'] = "Card";
		$process_info['set_pay_type_id'] = "2";
		$process_info['for_deposit'] = "";

		$gateway = strtolower($integration_info['gateway']);
		$gateway_function_name = 'process_' . $gateway;
		//let's give it a try

		require_once dirname(dirname(dirname(dirname(__FILE__)))) . '/lib/gateway_settings.php';
		$extra_function_file = dirname(dirname(dirname(dirname(__FILE__)))) . '/lib/gateway_lib/Transaction_Controller.php';
		if (file_exists($extra_function_file)) {
			require_once $extra_function_file;
		}

		$result = array();
		// $result['status'] = 'failed';
		$tip_adjustment_details = json_decode( $_POST['tip_adjustments'], true );
		if ($location_info['integrateCC'] == "1") {
			require_once dirname(dirname(dirname(dirname(__FILE__)))) . $dev_dir . '/lib/gateway_functions.php';
			$payment_ids = array();
			foreach( $tip_adjustment_details as $payment_id => $tip_amount ) {
				$payment_ids[] = $payment_id;
				$tip_adjustment_details[ $payment_id ] = report_money_to_number( $tip_amount )*1;
			}

			$payment_id_str = implode(',', $payment_ids);
			if( ($query = lavu_query( "SELECT * FROM `cc_transactions` WHERE `id` IN ({$payment_id_str})" )) !== FALSE ) {
				global $error_encountered;
				while( $transaction_info = mysqli_fetch_assoc( $query ) ){
					$order_id = $transaction_info['order_id'];
					$payment_id = $transaction_info['id'];
					$apply_tip = $tip_adjustment_details[ $payment_id ];
					$apply_tip =  number_format((float)$apply_tip, $decimal_places, ".", "");

					$error_encountered = false;
					$tip_amount = process_capture_or_tip($location_info, $integration_info, $process_info, $transaction_info, $apply_tip, $order_id, "", false);

					$result[ $payment_id ] = array(
						'status' => $error_encountered ? 'failed' : 'success',
						'tip_amount' => $tip_amount,
					);
				}
			} else {
				foreach( $tip_adjustment_details as $payment_id => $apply_tip ) {
					$result[ $payment_id ] = array(
						'status' => 'failed',
						'reason' => 'Unable to retrieve Transactions',
					);
				}
			}
		} else {
			foreach( $tip_adjustment_details as $payment_id => $apply_tip ) {
				$payment_ids[] = $payment_id;
				if( (lavu_query("UPDATE `cc_transactions` SET `tip_amount` = '[1]' WHERE `id` = '[2]'", $apply_tip, $payment_id )) === FALSE ) {
					$result[ $payment_id ] = array(
						'status' => 'failed',
						'reason' => 'Update Tip Failed',
					);
				} else {
					$result[ $payment_id ] = array(
						'status' => 'success',
						'tip_amount' => $apply_tip,
					);
				}
			}
		}

		echo json_encode( $result );
		exit();
	}

	private function processSettleBatch( $area_reports ) {
		$area_reports->report_drawer = new black_hole_report_drawer();
		global $location_info;
		global $locationid;
		global $data_name;

		$decimal_places = $location_info['disable_decimal'];
		$smallest_money = (1 / pow(10, $decimal_places));

		$integration_info = get_integration_from_location($locationid, "poslavu_".$data_name."_db");
		$process_info = array();
		$process_info['mag_data'] = "";
		$process_info['reader'] = "";
		$process_info['card_number'] = "";
		$process_info['card_cvn'] = "";
		$process_info['name_on_card'] = "";
		$process_info['exp_month'] = "";
		$process_info['exp_year'] = "";
		$process_info['card_amount'] = "";
		$process_info['company_id'] = admin_info('companyid');
		$process_info['loc_id'] = $location_info['id'];
		$process_info['ext_data'] = "";
		$process_info['data_name'] = admin_info('dataname');
		$process_info['register'] = "Back End";
		$process_info['register_name'] = "Back End";
		$process_info['device_udid'] = "";
		$process_info['server_id'] = admin_info('loggedin');
		$process_info['server_name'] = $server_name;
		$process_info['device_time'] = $device_time;
		$process_info['set_pay_type'] = "Card";
		$process_info['set_pay_type_id'] = "2";
		$process_info['for_deposit'] = "";

		$process_info['username'] = $integration_info['integration1'];
		$process_info['password'] = $integration_info['integration2'];
		$process_info['integration3'] = $integration_info['integration3'];
		$process_info['integration4'] = $integration_info['integration4'];
		$process_info['integration5'] = $integration_info['integration5'];
		$process_info['integration6'] = $integration_info['integration6'];

		$gateway = strtolower($integration_info['gateway']);
		$gateway_function_name = 'process_' . $gateway;
		//let's give it a try

		require_once dirname(dirname(dirname(dirname(__FILE__)))) . '/lib/gateway_functions.php';
		require_once dirname(dirname(dirname(dirname(__FILE__)))) . $dev_dir . '/lib/gateway_settings.php';
		$extra_function_file = dirname(dirname(dirname(dirname(__FILE__)))) . $dev_dir . '/lib/gateway_lib/Transaction_Controller.php';
		if (file_exists($extra_function_file)) {
			require_once $extra_function_file;
		}

		$result = array(
			'order_status' => array(),
			'batch_result' => array(),
		);

		$transactionController = new Transaction_Controller($gateway);
		switch( $gateway ) {
			case 'authorize.net':
			case 'bluepay':
			case 'cybersource':
			case 'elavon':
				$forty_five_days_ago = date("Y-m-d H:i:s", time() - 3888000);

				$tip_was_declined = false;
				$get_transactions = lavu_query("SELECT * FROM `cc_transactions` WHERE `loc_id` = '[1]' AND `pay_type` = 'Card' AND `Action` = 'Sale' AND `voided` != '1' AND `processed` != '1' AND `gateway` = 'Authorize.net' AND `datetime` >= '[2]'".$order_by, $location_info['id'], $forty_five_days_ago);
				if (@mysqli_num_rows($get_transactions) > 0) {
					$log_response = "Transactions settled with no errors. ";
					while ($transaction_info = mysqli_fetch_assoc($get_transactions)) {
						usleep(1000);
						$iid = $transaction_info['internal_id'];
						$process_info['order_id'] = $transaction_info['order_id'];
						$process_info['ioid'] = $transaction_info['ioid'];
						$process_info['check'] = $transaction_info['check'];
						$process_info['pnref'] = $transaction_info['transaction_id'];
						if ($transaction_info['auth'] == "1") {
							$process_info['transtype'] = "PreAuthCapture";
							$process_info['card_amount'] = $transaction_info['amount'];
							$process_info['split_tender_id'] = $transaction_info['split_tender_id'];

							$transactionController->process_transaction($process_info, $location_info);
							$response = $transactionController->getResponse();

							$response_parts = explode("|", $response);
							if ($response_parts[0] == '1') {
								$txid = $response_parts[1];
								$update_transaction = lavu_query("UPDATE `cc_transactions` SET `auth` = '0', `transaction_id` = '[1]' WHERE `id` = '[2]'", $txid, $transaction_info['id']);
								if (!empty($iid)) {
									schedule_remote_to_local_sync_if_lls($location_info, $location_info['id'], "run local query", "cc_transactions", "UPDATE `cc_transactions` SET `auth` = '0', `transaction_id` = '".$txid."' WHERE `internal_id` = '".$iid."'");
								}
							}
						} else {
							$response = "1|Approved";
						}

						if (($transaction_info['tip_amount'] > 0) && ($response_parts[0] == "1")) {
							usleep(1000);

							$process_info['transtype'] = "AddTip";
							$process_info['card_amount'] = $transaction_info['tip_amount'];

							$process_info['card_number'] = $card_data[0];

							if ($integration_info['gateway'] == "CyberSource") {
								$card_data = explode("|*|", interpret($transaction_info['temp_data']));
								$process_info['mag_data'] = $card_data[4];
								$process_info['exp_month'] = $card_data[1];
								$process_info['exp_year'] = $card_data[2];
								$process_info['card_cvn'] = $card_data[3];
								$process_info['transaction_db_id'] = $transaction_info['id'];
								$transactionController->process_transaction($process_info, $location_info);
								$response = $transactionController->getResponse();
							} else {
								$card_data = explode("|", interpret($transaction_info['temp_data']));
								$process_info['mag_data'] = "";
								$process_info['exp_month'] = substr($card_data[1], 0, 2);
								$process_info['exp_year'] = substr($card_data[1], -2);
								$process_info['card_cvn'] = $card_data[2];
								$transactionController->process_transaction($process_info, $location_info);
								$response = $transactionController->getResponse();
							}
						}
						$response_parts = explode("|", $response);

						if ($response_parts[0] == "1") {
							$update_transaction = lavu_query("UPDATE `cc_transactions` SET `processed` = '1', `temp_data` = '' WHERE `id` = '[1]'", $transaction_info['id']);
							if (!empty($iid)) {
								schedule_remote_to_local_sync_if_lls($location_info, $location_info['id'], "run local query", "cc_transactions", "UPDATE `cc_transactions` SET `processed` = '1', `temp_data` = '' WHERE `internal_id` = '".$iid."'");
							}

							$result['order_status'][] = array(
								'order_id' => $transaction_info['order_id'],
								'loc_id' => $location_info['id'],
								'status' => 'success',
								'card_tip' => number_format($transaction_info['tip_amount'], $decimal_places, ".", ""),
							);
						} else if (($response_parts[0] != "1") && ($transaction_info['tip_amount'] > 0)) {
							$result['order_status'][] = array(
								'order_id' => $transaction_info['order_id'],
								'loc_id' => $location_info['id'],
								'status' => 'declined',
								'reason' => 'Tip was Declined',
								'card_tip' => number_format($transaction_info['tip_amount'], $decimal_places, ".", ""),
							);

							$tip_was_declined = true;

							$log_file = fopen("/home/poslavu/logs/company/".$process_info['data_name']."/tip_declines.txt", "a");
							fputs($log_file, $transaction_info['order_id']." : ".$process_info['card_amount']."\n");
							fclose($log_file);

						} else {
							$result['order_status'][] = array(
								'order_id' => $transaction_info['order_id'],
								'loc_id' => $location_info['id'],
								'status' => 'failed',
								'reason' => $response_parts[1],
							);
							$log_response = "One or more errors encountered. ";
						}
					}
				} else {

					$result['batch_result'][] = array(
						'status' => 'failed',
						'reason' => 'No Card Transactions to Settle',
					);
				}

				if ($tip_was_declined == true) {

					$result['batch_result'][] = array(
						'status' => 'failed',
						'reason' => 'Some tips were declined. Transactions with declined tips will still be flagged as unprocessed so you may adjust tip amounts until all transactions can be processed successfully.',
					);
				}

				break;
			case 'magensa':
				$tip_was_declined = false;
				$get_transactions = lavu_query("SELECT * FROM `cc_transactions` WHERE `loc_id` = '[id]' AND `pay_type` = 'Card' AND `transtype` = 'Auth' AND `auth` != '2' AND `voided` != '1' AND `got_response` = '1' AND `processed` != '1'", $location_info); // AND `datetime` <= '2013-09-16 03:59:00'
				if (@mysqli_num_rows($get_transactions) > 0) {
					$log_response = "Settled with no errors.";
					$base_total = 0;
					$tip_total = 0;
					$base_total_c = 0;
					$tip_total_c = 0;
					while ($transaction_info = mysqli_fetch_assoc($get_transactions)) {
						usleep(1000);

						$process_info['pnref'] = $transaction_info['preauth_id'];
						$process_info['order_id'] = $transaction_info['order_id'];
						$process_info['check'] = $transaction_info['check'];
						$capture_amount = number_format(($transaction_info['amount'] + $transaction_info['tip_amount']), $decimal_places, ".", "");

						$process_info['transtype'] = "PreAuthCapture";
						$iid = $transaction_info['internal_id'];
						$process_info['card_amount'] = $capture_amount;

						$response = process_magensa($process_info, $location_info);

						$base_total += $transaction_info['amount'];
						$tip_total += $transaction_info['tip_amount'];

						$response_parts = explode("|", $response);
						if ($response_parts[0] == "1") {
							$base_total_c += $transaction_info['amount'];
							$tip_total_c += $transaction_info['tip_amount'];
							$txid = $response_parts[1];
							if( FALSE === lavu_query("UPDATE `cc_transactions` SET `transaction_id` = '[1]', `voided` = '0', `auth` = '0', `processed` = '1', `temp_data` = '' WHERE `id` = '[2]'", $txid, $transaction_info['id']) ) {
								$result['order_status'][] = array(
									'order_id' => $transaction_info['order_id'],
									'loc_id' => $location_info['id'],
									'status' => 'failed',
									'reason' => 'Transaction Posted Successfully but Unable to Update Transaction Record, please contact Lavu Support.',
									'transaction_id' => $txid,
								);
							} else {
								$result['order_status'][] = array(
									'order_id' => $transaction_info['order_id'],
									'loc_id' => $location_info['id'],
									'status' => 'success',
								);
							}
							if (!empty($iid)) {
								schedule_remote_to_local_sync_if_lls($location_info, $location_info['id'], "run local query", "cc_transactions", "UPDATE `cc_transactions` SET `transaction_id` = '".txid."', `auth` = '0', `processed` = '1', `temp_data` = '' WHERE `internal_id` = '".$iid."'");
							}
							$mid .= "<tr><td align='center'><br>Order ID ".$location_info['id'].$transaction_info['order_id']." - Card Tip: ".number_format($transaction_info['tip_amount'], $decimal_places, ".", "")." - Approved</td></tr>";
						} else if (($response_parts[0] != "1") && ($transaction_info['tip_amount'] > 0)) {
							$result['order_status'][] = array(
								'order_id' => $transaction_info['order_id'],
								'loc_id' => $location_info['id'],
								'status' => 'declined',
								'reason' => 'Tip was Declined',
								'card_tip' => number_format($transaction_info['tip_amount'], $decimal_places, ".", ""),
							);
							$tip_was_declined = true;
						} else {
							$result['order_status'][] = array(
								'order_id' => $transaction_info['order_id'],
								'loc_id' => $location_info['id'],
								'status' => 'failed',
								'reason' => $response_parts[1],
							);
						}
					}

					$result['batch_result'] = array(
						'status' => 'success',
						'total_base_amount_confirmed'	=> number_format($base_total_c, $decimal_places, ".", ""),
						'total_base_amount_attempted'	=> number_format($base_total, $decimal_places, ".", ""),
						'total_tip_amount_confirmed'	=> number_format($tip_total_c, $decimal_places, ".", ""),
						'total_tip_amount_attempted'	=> number_format($tip_total, $decimal_places, ".", ""),
						'grand_total_amount_confirmed'	=> number_format(($base_total_c + $tip_total_c), $decimal_places, ".", ""),
						'grand_total_amount_attempted'	=> number_format(($base_total + $tip_total), $decimal_places, ".", ""),
						'batch_total_confirmed'			=> number_format(($base_total_c + $tip_total_c), $decimal_places, ".", ""),
						'batch_total_attempted'			=> number_format(($base_total + $tip_total), $decimal_places, ".", ""),
					);

				} else {
					$result['batch_result'] = array(
						'status' => 'failed',
						'reason' => 'No Card Transactions to Settle',
					);
				}

				if ($tip_was_declined) {
					$result['batch_result']['status'] = 'failed';
					$result['batch_result']['reason'] = 'Some tips were declined. Transactions with declined tips will still be flagged as unprocessed so you may adjust tip amounts until all transactions can be processed successfully.';
				} else {
					$result['batch_result']['status'] = 'failed';
					$result['batch_result']['reason'] = $log_response;
				}
				break;
			case 'heartland' :
			case 'merchantware' :
			case 'mercury' :
			case 'redfin' :
			case 'tgate' :
            case 'paypal':
				$transactionController->process_transaction($process_info, $location_info);
				$response = $transactionController->getResponse();
				$response_parts = explode("|", $response);
				$log_response = isset($response_parts[3])?$response_parts[3]:'';
				$show_response = isset($response_parts[3])?$response_parts[3]:'';

				$batch_no = '';
				$batch_total = 0;
				switch ( $gateway ) {
					case 'heartland' :
						$batch_total = $response_parts[7];
						$batch_no = $response_parts[1];

						$show_response = $response_parts[4];
						break;
					case 'mercury' :
						$batch_total = $response_parts[2];
						$batch_no = $response_parts[4];

						$log_response = $response_parts[1];
						// $till_report_info['details'] = $response_parts[5];
						break;
					default:
						break;
				}

				if ($response_parts[0] == '1') {
					$query_arguments = array(
						'batch_no' => $batch_no,
						'loc_id' => $location_info['id'],
					);
					if( FALSE === lavu_query("UPDATE `cc_transactions` SET `processed` = '1', `batch_no` = '[batch_no]' WHERE `datetime` > DATE_SUB(NOW(), INTERVAL 2 WEEK) AND `processed` != '1' AND `loc_id` = '[loc_id]' AND `mpshc_pid` = '' AND `auth` = '0' AND `voided` != '1'", $query_arguments) ){
						// Update Failed
						$result['batch_result'] = array(
							'status' => 'failed',
							'reason' => 'Batch Succeeded, Transaction Records Update Failed',
						);
					} else {
						// Update Succeeded
						$result['batch_result'] = array(
							'status' => 'success',
						);
					}

				} else {
					$log_response = $response_parts[1];
					$show_response = $response_parts[1];

					if (($gateway == 'tgate') && ($log_response == "No Records To Process")) {
						$result['batch_result'] = array(
							'status' => 'failed',
							'reason' => 'No Records To Process - please note this message will always be returned if you are using a host based processor, and there is no need for you to manually settle batches from the POSLavu back end. Host-based processors include Concord (EFSNet), Global, Paymentech - Tampa, FastCheck/Cyclone, IPay/Intercept, NCN (Rocky Mountain), Echo, Valuetec, Element, Litle, Global East Canada, Givex, Telecheck, and Elavon/Nova.',
						);
					} else {
						$result['batch_result'] = array(
							'status' => 'failed',
							'reason' => $log_response,
						);
					}
				}

				break;
			case 'ipcommerce' :
			case 'merchantpartners' :
			case 'usaepay' :
				$result['batch_result'] = array(
					'status' => 'failed',
					'reason' => 'Merchant Initiated Batching is not Available for this Gateway.',
				);
				break;
			default :
				$result['batch_result'] = array(
					'status' => 'failed',
					'reason' => 'Unrecognized payment gateway ('.$integration_info['gateway'].')...',
				);
				break;
		}

		echo json_encode( $result );
		exit();
	}

	public function run( $area_reports ){
		global $location_info;

		if( isset($_POST['offline_payment']) ){
			$this->processOfflinePayment( $area_reports );
		} else if ( isset( $_POST['tip_adjustments'] ) ) {
			$this->processTipAdjustments( $area_reports );
		} else if ( isset( $_POST['settle_batch'] ) ) {
			$this->processSettleBatch( $area_reports );
		} else {

		}

		switch( $area_reports->output_mode ){
			case 'csv':
			// 	$area_reports->report_drawer = new exporter_eod_report_drawer( $area_reports );
			// 	break;
			default:
				$area_reports->report_drawer = new settlement_drawer( $area_reports, $this );
				break;
		}

		$this->settlement_report_mode = REPORTS_V2_SETTLEMENT_MODE_OFFLINE_TIP_ENTRY; // Default

		$card_info = $this->runReport( array(
			REPORTS_V2_SELECTION_COLUMN_KEY => '',
			REPORTS_V2_SPECIAL_CODE_KEY => '',
			REPORTS_V2_SELECTION_KEY => 'Preset Date Ranges',
			REPORTS_V2_TABLE_KEY => 'orders',
			REPORTS_V2_TITLE_KEY => 'Order Information',
			REPORTS_V2_NAME_KEY => 'order_info',
			REPORTS_V2_ORDERBY_KEY => 'time ASC',
			REPORTS_V2_SELECT_KEY => implode( REPORTS_V2_FIELD_SEPERATOR,
										array(
											'#order_server',
											'#payment_order_id',
											'cc_transactions.process_data',
											'cc_transactions.temp_data',
											'cc_transactions.datetime as time',
											'#payment_id',
											'\'\' as card',
											'cc_transactions.info',
											'cc_transactions.card_type',
											'cc_transactions.card_desc',
											'cc_transactions.first_four',
											'cc_transactions.transaction_id as ref',
											'cc_transactions.auth_code',
											'#sum_amount_collected',
											'#sum_tip_amount',
											'cc_transactions.processed',
											'cc_transactions.gateway',
										)),
			REPORTS_V2_GROUPBY_KEY => '#order_server,#payment_order_id,#payment_id',
			REPORTS_V2_WHERE_KEY => implode( REPORTS_V2_FIELD_SEPERATOR,
										array(
											'#transactions_occurred_with_offline_with_active',
											"orders.location_id = '[locationid]'",
											"(cc_transactions.action = 'Sale' OR cc_transactions.action = 'Auth')",
											"cc_transactions.pay_type_id = '2'",
										)),
			REPORTS_V2_JOIN_KEY => '',
			REPORTS_V2_ID_KEY => 'x',
			REPORTS_V2_ORDER_KEY => '',
			REPORTS_V2_DELETED_KEY => '',
		));

		$unprocessed_count = 0;
		$result = array();
		foreach( $card_info as $row=>$payment_info ){
			if( substr($payment_info['process_data'],0,4) == 'LOCT' ){
				if( !isset($result['offline order']) ){
					$result['offline order'] = array();
				}

				$result['offline order'][] = $payment_info;
				if( report_number_to_float( $payment_info['processed'] ) === 0 ){
					if( substr($payment_info['process_data'],0,4) == 'LOCT' ){
						$card_detail = $this->parseEncodedData( $payment_info['temp_data'] );
						// echo '['.__FILE__.':'.__LINE__.']' . 'Card Detail:<pre style="text-align: left;max-width:100em;">' . print_r( $card_detail, true ) . '</pre>';
					}
					$unprocessed_count++;
				}

				continue;
			} else if( !isset($result[$payment_info['order_server']]) ){
				$result[$payment_info['order_server']] = array();
			}

			$result[$payment_info['order_server']][] = $payment_info;
		}
		if( $unprocessed_count > 0 ) {
			$this->settlement_report_mode = REPORTS_V2_SETTLEMENT_MODE_OFFLINE_ORDERS;
		}

		$keys = array_keys( $result );

		uasort($keys, function( $entry1, $entry2 ){
			if( $entry1 == 'offline order' && $entry2 == 'offline order' ) {
				return 0;
			}
			if( $entry1 == 'offline order' ) {
				return -1;
			}
			if( $entry2 == 'offline order' ) {
				return 1;
			}

			if( $entry1 < $entry2 ) {
				return -1;
			}
			if( $entry1 > $entry2 ) {
				return 1;
			}

			return 0;
		});

		$result2 = array();
		foreach( $keys as $key ) {
			$result2[$key] = $result[$key];
		}

		return array_values($result2);
		echo '<br /><br />';
		echo '['.__FILE__.':'.__LINE__.'] '. '<pre style="text-align: left;">' . print_r( $card_info, true ) . '</pre>';
	}
}
