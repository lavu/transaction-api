<?php
	function fiscal_accounting_report($vars,$flag)
	{
		// LP-1895 this is temporary fix we will remove once have a good solution./
	    if (!(isset($_GET['email']) && $_GET['email'] != '' && isset($_GET['output_mode']) && $_GET['output_mode'] == 'csv'))
	    {
		  ini_set('memory_limit', '1024M');
		  ini_set('max_execution_time', 600);
	    }

		if(strpos($vars['report_name'],"daily")!==false)
			$report_group_by = "day";
		else
			$report_group_by = "record";
		
		$queryMode = isset($_REQUEST['mode']) && !empty($_REQUEST['mode']) ? $_REQUEST['mode'] : false;
		$getWhereClause = setWhereClauseForReport($vars, $queryMode);
		$where_clause = $getWhereClause['where_clause'];
		$order_clause = $getWhereClause['order_clause'];

		$fiscal_spans = array();
		$fiscal_config_query=lavu_query("select * from `config` where setting='fiscal_integration'");
		$fiscal_config_read=mysqli_fetch_assoc($fiscal_config_query);
		$fiscal_config_value = json_decode($fiscal_config_read['value']);
		$country= $fiscal_config_value->country;
		global $modules;
		if ($modules->hasModule("reports.fiscal.uruguay")) {
			$fiscal_invoice_type_query=lavu_query("select * from `fiscal_invoice_type` ");
			while($fiscal_invoice_type_query_read = mysqli_fetch_assoc($fiscal_invoice_type_query))
			{
				$fiscal_invoice_type_id = $fiscal_invoice_type_query_read['fiscal_invoice_type_id'];
				$fiscal_invoice_type[$fiscal_invoice_type_id] = $fiscal_invoice_type_query_read['invoice_label'];
                $fiscal_report_type[$fiscal_invoice_type_id] = $fiscal_invoice_type_query_read['invoice_type'];
			}
			//echo "<pre>";	print_r($fiscal_invoice_type);
			/* $fiscal_query = lavu_query("select * from `fiscal_reports` where `datetime`>='[min_fiscal_datetime]' and `datetime`<='[max_fiscal_datetime]'",$fvars);
			 while($fiscal_read = mysqli_fetch_assoc($fiscal_query))
			 {
			 $register = $fiscal_read['register'];
			 $first_invoice = $fiscal_read['first_invoice'];
			 $last_invoice = $fiscal_read['last_invoice'];
			 $invoice_report_id = $fiscal_read['report_id'];
			 $report_type = $fiscal_read['report_type'];

			 if(!isset($fiscal_spans["RT".$report_type]["R".$register]))
			 {
			 $fiscal_spans["RT".$report_type]["R".$register] = array();
			 }

			 if(is_numeric($first_invoice) && is_numeric($last_invoice))
			 {
			 $invoice_span = $last_invoice * 1 - $first_invoice * 1;
			 if($invoice_span >=0 && $invoice_span <= 5000)
			 {
			 for($n=$first_invoice * 1; $n<=$last_invoice * 1; $n++)
			 {
			 $fiscal_spans["RT".$report_type]["R".$register]["I".($n * 1)] = $invoice_report_id;
			 }
			 }
			 }
			 }
			 $startts = time();
			 */			// schedule					- actual sales time
			// fiscal date				- according to fiscal
			// commercial date			- according to lavu
			// z location				- fiscal zreport id
			// type of fiscal receipt	- a/b/nc
			// invoice number			- order_id
			// name
			// cuit (local id)			- xx-xxxxxxxx-x
			$query = "select LEFT(orders.closed,10) as fiscal_date,
						fiscal_invoice.invoice_type_id AS fiscal_type,
						orders.order_id as order_id,
						fiscal_invoice.ref_data as fiscal_txn_id,
						payment_types.type as payment_type,
						cc_transactions.card_desc as last_four,
						if(count(*) > 0, concat('[money]',format(sum( IF(cc_transactions.action='Refund', -1*cc_transactions.total_collected, cc_transactions.total_collected) ),2)), '[money]0.00') as total_payment,
						`payment_types`.`type` as `group_col_payment_types_type`
						from `orders`
						LEFT JOIN `cc_transactions` on `orders`.`order_id` = `cc_transactions`.`order_id`
						LEFT JOIN `payment_types` ON `cc_transactions`.`pay_type_id` = `payment_types`.`id` AND (`cc_transactions`.`loc_id` = `payment_types`.`loc_id` OR '0' = `payment_types`.`loc_id` )
						LEFT JOIN `locations` on `orders`.`location_id`=`locations`.`id`
						LEFT JOIN `fiscal_invoice` on `orders`.`order_id`=`fiscal_invoice`.`order_id`
						LEFT JOIN `med_customers` ON `med_customers`.id = SUBSTRING_INDEX(SUBSTRING_INDEX(`orders`.original_id, '|o|', 4), '|o|', -1)
						WHERE $order_clause
                        AND orders.void = '0'
						group by orders.id order by fiscal_date desc;";

			$order_id_list = "";

			$rows = array();
			//output_debug_query($query,$vars);
			$report_query = lavu_query($query,$vars);
			while($report_read = mysqli_fetch_assoc($report_query))
			{
				//print_r($report_read);
				if($order_id_list!="") $order_id_list .= ",";
				$escaped_order_id = str_replace(urlencode("-"),"-",urlencode($report_read['order_id']));
				$order_id_list .= "'".$escaped_order_id."'";
                $fiscal_report_order_id=$report_read['order_id'];
                $fiscal_query="select invoice_type_id ,ref_data from fiscal_invoice where order_id='$fiscal_report_order_id' limit 1 ";
                $fiscal_read = lavu_query($fiscal_query);
                $fiscal_data=mysqli_fetch_assoc($fiscal_read);

                if(isset($report_read['fiscal_type'])){
					//$fiscal_type = json_decode($report_read["fiscal_type"]);
					$report_read['fiscal_type'] = $fiscal_invoice_type[$report_read[fiscal_type]];

				}
				else{
					$report_read['fiscal_type'] = "";
				}

				if(isset($report_read['fiscal_txn_id']))
				{
					$fiscalJsonArray = json_decode($report_read['fiscal_txn_id']);
					if(isset($fiscalJsonArray->NUMERO)){
						$report_read['fiscal_txn_id']=$fiscalJsonArray->NUMERO;
					}
				}
				$rows[] = $report_read;
			}

			$order_data = array();
			$categories = array();

			$elapsed = time() - $startts;
			if (!empty($order_id_list))
			{
				$query = "select `orders`.`total` as `order_total`,
				concat('[money]',`orders`.`subtotal`- `orders`.`itax`) as `order_sale`,
				concat('[money]',`orders`.`discount` + `orders`.`idiscount_amount`) as `order_discount`,
				concat('[money]',`orders`.`subtotal` - (`orders`.`discount` + `orders`.`idiscount_amount` + `orders`.`itax`)) as `order_taxable`,
				concat('[money]',`orders`.`tax` + `orders`.`itax`) as `order_tax`,
				concat('[money]',`orders`.`refunded`) as `order_refunded`,
				`orders`.`register_name` as `register_name`,
				`menu_categories`.`name` as `category_name`,
				`menu_categories`.`id` as `category_id`,
				`order_contents`.`tax_subtotal1` as `cost`,
				`order_contents`.`order_id` as `order_id`,
				`order_contents`.`item_id` as `item_id`
				from `order_contents`
				left join `menu_categories` on `order_contents`.`category_id`=`menu_categories`.`id`
				left join `orders` on `order_contents`.`order_id`=`orders`.`order_id`
				where `orders`.`order_id` IN ($order_id_list) AND `order_contents`.`quantity`*1 > 0";
				$content_query = lavu_query($query);
				while($content_read = mysqli_fetch_assoc($content_query))
				{
					$order_id = $content_read['order_id'];
					$category_id = $content_read['category_id'];
					$category_name = trim($content_read['category_name']);
					$cost = $content_read['cost'];
					if(!isset($order_data[$order_id][$category_id]))
						$order_data[$order_id][$category_id] = 0;
						$order_data[$order_id][$category_id] += $cost;
						$categories[$category_id] = $category_name;
						$order_data[$order_id]['info'] = $content_read;
						$order_data[$order_id]['register_name'] = $content_read['register_name'];
				}
			}

			// Make sure all orders have information for all categories found
			foreach($order_data as $order_id => $order_arr)
			{
				foreach($categories as $category_id => $category_name)
				{
					if(!isset($order_data[$order_id][$category_id]))
						$order_data[$order_id][$category_id] = 0;
				}
			}

			$processed_order_ids = array();
			$refund_query="select order_id ,concat('[money]',format(sum( IF(cc_transactions.action='Refund', cc_transactions.total_collected, 0.00) ),2)) as order_refunded from `cc_transactions`  where `order_id` IN ($order_id_list)  group by order_id ";
			$refuned_query = lavu_query($refund_query);
			while($refuned_read = mysqli_fetch_assoc($refuned_query))
			{
				$order_id = $refuned_read['order_id'];
				$refuned_data[$order_id]=$refuned_read['order_refunded'];
			}

            for($i=0; $i<count($rows); $i++)
			{
				$order_id = $rows[$i]['order_id'];
				$already_processed = in_array($order_id, $processed_order_ids);

				$rows[$i]['order_sale'] = $order_data[$order_id]['info']['order_sale'];
				$rows[$i]['order_discount'] = $order_data[$order_id]['info']['order_discount'];
				$rows[$i]['order_taxable'] = $order_data[$order_id]['info']['order_taxable'];
				$rows[$i]['order_tax'] = $order_data[$order_id]['info']['order_tax'];
				$rows[$i]['order_refunded'] = $refuned_data[$order_id];
				foreach($categories as $category_id => $category_name)
				{
					$cost = $order_data[$order_id][$category_id];
					$category_name = $categories[$category_id];

					if ($already_processed)
					{
						$rows[$i][$category_name] = "[money]0";
					}
					else
					{
						if (isset($rows[$i][$category_name])) // i.e., 2 categories are named 'Pinta Patagonia'
						{
							$previous_cost = str_replace("[money]", "", $rows[$i][$category_name]);
							$cost += $previous_cost;
						}
						$rows[$i][$category_name] = "[money]".$cost;
					}
				}
				$order_arr = $order_data[$order_id];
				$fiscal_txn_id = $rows[$i]['fiscal_txn_id'];
				$fiscal_type = $rows[$i]['fiscal_type'];
				if(isset($order_arr['register_name']) && trim($order_arr['register_name'])!="" && trim($fiscal_txn_id)!="")
				{
					//$rows[$i]['z_location'] = "No ZReport";
					$register_name = trim($order_arr['register_name']);
					if(isset($fiscal_spans["RT".$fiscal_type]["R".$register_name]["I".($fiscal_txn_id * 1)]))
					{
						$rows[$i]['z_location'] = "#" . $fiscal_spans["RT".$fiscal_type]["R".$register_name]["I".($fiscal_txn_id * 1)];
					}
				}
				$processed_order_ids[] = $order_id;
			}

			if($report_group_by=="day")
			{
				$ommitcols = array("P. V. No","order_id", "fiscal_txn_id", "cuit_local_id", "payment_type", "last_four", "total_tip");
				$sumcols = array("total_payment", "total_cash", "total_card", "order_sale", "order_discount", "order_taxable", "order_tax","order_refunded");
				foreach($categories as $category_id => $category_name) $sumcols[] = $category_name;

				for($i=0; $i<count($rows); $i++)
				{
					$rows[$i]['total_cash'] = 0;
					$rows[$i]['total_card'] = 0;
					$payment_type = strtolower(trim($rows[$i]['payment_type']));

					if($payment_type=="cash") $rows[$i]['total_cash'] = $rows[$i]['total_payment'];
					else if($payment_type=="card") $rows[$i]['total_card'] = $rows[$i]['total_payment'];
				}
				for($i=0; $i<count($rows); $i++)
				{
					$newrow = array();
					foreach($rows[$i] as $col => $val)
					{
						if($col=="total_payment") // put cash and card before total
						{
							$newrow['order_sale'] = $rows[$i]['order_sale'];
							$newrow['order_discount'] = $rows[$i]['order_discount'];
							$newrow['order_taxable'] = $rows[$i]['order_taxable'];
							$newrow['order_tax'] = $rows[$i]['order_tax'];
							$newrow['order_refunded'] = $rows[$i]['order_refunded'];
						}
						if($col!="total_cash"&&$col!="total_card")
						{
							$newrow[$col] = $rows[$i][$col];
						}
						if($col=="total_payment") // put cash and card before total
						{
							$newrow['total_cash'] = $rows[$i]['total_cash'];
							$newrow['total_card'] = $rows[$i]['total_card'];
						}
					}
					$rows[$i] = $newrow;
				}


				$lastday = "";
				$dayrows = array();
				for($i=0; $i<count($rows); $i++)
				{
					$nextday = $rows[$i]['fiscal_date'];
					if($nextday!=$lastday || $i==0)
					{
						$newrow = array();
						foreach($rows[$i] as $col => $val)
						{
							if(!in_array($col,$ommitcols))
							{
								$newrow[$col] = $val;
							}
						}
						$dayrows[] = $newrow;
					}
					else
					{
						$crow = count($dayrows) - 1;
						foreach($dayrows[$crow] as $col => $val)
						{
							if(in_array($col,$sumcols))
							{
								$prefix = "";
								$current_val = $dayrows[$crow][$col];
								$add_val = $rows[$i][$col];
								if(substr($current_val,0,7)=="[money]"||substr($add_val,0,7)=="[money]") $prefix = "[money]";
								if(substr($current_val,0,8)=="[number]"||substr($add_val,0,8)=="[number]") $prefix = "[number]";
								if($prefix!="")
								{
									$current_val = str_replace($prefix,"",$current_val);
									$add_val = str_replace($prefix,"",$add_val);
								}
								$current_val = $current_val * 1 + $add_val * 1;

								if($prefix!="") $current_val = $prefix . $current_val;
								$dayrows[$crow][$col] = $current_val;
							}
						}
					}
					$lastday = $nextday;
				}
				$rows = $dayrows;
			}
			else
			{
				for($i=0; $i<count($rows); $i++)
				{
					$newrow = array();
					foreach($rows[$i] as $col => $val)
					{
						if($col=="total_payment") // put taxable before total
						{
							$newrow['order_sale'] = $rows[$i]['order_sale'];
							$newrow['order_discount'] = $rows[$i]['order_discount'];
							$newrow['order_taxable'] = $rows[$i]['order_taxable'];
							$newrow['order_tax'] = $rows[$i]['order_tax'];
							$newrow['order_refunded'] = $rows[$i]['order_refunded'];

						}
						if($col!="total_cash"&&$col!="total_card")
						{
							$newrow[$col] = $rows[$i][$col];
						}
					}
					if(empty($newrow["total_payment"])){
						$newrow["total_payment"]="[money]0";
					}
					if(empty($newrow['order_sale'])){
						$newrow['order_sale']="[money]0";
					}
					if(empty($newrow['order_discount'])){
						$newrow['order_discount']="[money]0";
					}
					if(empty($newrow['order_taxable'])){
						$newrow['order_taxable']="[money]0";
					}
					if(empty($newrow['order_tax'])){
						$newrow['order_tax']="[money]0";
					}
					if(empty($newrow['order_refunded'])){
						$newrow['order_refunded']="[money]0";
					}
					$rows[$i] = $newrow;
				}
			}

			$translated_phrases = array();
			$auto_translate = array("schedule","fiscal_date","commercial_date","z_location","fiscal_type","order_id","fiscal_txn_id","cuit_local_id","payment_type","last_four","order_sale","order_discount","order_taxable","order_tax","total_payment");
			for($n=0; $n<count($auto_translate); $n++)
			{
				$phrase = $auto_translate[$n];
				$human_phrase = trim(ucwords(str_replace("_"," ",$phrase)));
				$translated_human_phrase = speak($human_phrase);
				if($translated_human_phrase != $human_phrase)
				{
					$translated_phrase = str_replace(" ","_",trim(str_replace(".","",strtolower($translated_human_phrase))));
					$translated_phrases[$phrase] = $translated_phrase;
				}
			}
			for($i=0; $i<count($rows); $i++)
			{
				$newrow = array();
				foreach($rows[$i] as $col => $val)
				{
					if(isset($translated_phrases[$col])) $newrow[$translated_phrases[$col]] = $val;
					else $newrow[$col] = $val;
				}
				$rows[$i] = $newrow;
			}
			
			$i=0;
			foreach( $rows as $rs){
			    $data[0][$i][0]=$rs;
			    $i++; 
			}
			return $data;
		}
		else {
		$fiscal_query = lavu_query("select * from `fiscal_reports` where $where_clause");
		while($fiscal_read = mysqli_fetch_assoc($fiscal_query))
		{
			$register = $fiscal_read['register'];
			$first_invoice = $fiscal_read['first_invoice'];
			$last_invoice = $fiscal_read['last_invoice'];
			$invoice_report_id = $fiscal_read['report_id'];

			$report_type = $fiscal_read['report_type'];
			
			if(!isset($fiscal_spans["RT".$report_type]["R".$register]))
			{
				$fiscal_spans["RT".$report_type]["R".$register] = array();
			}

			if(is_numeric($first_invoice) && is_numeric($last_invoice))
			{
				$invoice_span = $last_invoice * 1 - $first_invoice * 1;
				if($invoice_span >=0 && $invoice_span <= 5000)
				{
					for($n=$first_invoice * 1; $n<=$last_invoice * 1; $n++)
					{
						$fiscal_spans["RT".$report_type]["R".$register]["I".($n * 1)] = $invoice_report_id;
					}
				}
			}
		}
		$startts = time();
		// schedule					- actual sales time
		// fiscal date				- according to fiscal
		// commercial date			- according to lavu
		// z location				- fiscal zreport id
		// type of fiscal receipt	- a/b/nc
		// invoice number			- order_id
		// name
		// cuit (local id)			- xx-xxxxxxxx-x
		$query = "select `locations`.`title` as 'location_name',
						orders.closed as `schedule`,
						LEFT(orders.closed,10) as fiscal_date,
						LEFT(orders.closed,10) as `commercial_date`,
						'z_location' as z_location,
						split_check_details.print_info AS fiscal_type,
						orders.order_id as order_id,
						split_check_details.print_info as fiscal_txn_id,
                        'pv_number' as  pv_number,
						IF(truncate(' ',`med_customers`.field10) = '', `med_customers`.field10, '') AS cuit_local_id,
						payment_types.type as payment_type,
						cc_transactions.card_desc as last_four,
						if(count(*) > 0, concat('[money]',format(sum( IF(cc_transactions.action='Refund', -1*cc_transactions.total_collected, cc_transactions.total_collected) ),2)), '[money]0.00') as total_payment,
						`payment_types`.`type` as `group_col_payment_types_type`
						from `orders`
						LEFT JOIN `cc_transactions` on `orders`.`order_id` = `cc_transactions`.`order_id` 
						LEFT JOIN `payment_types` ON `cc_transactions`.`pay_type_id` = `payment_types`.`id` AND (`cc_transactions`.`loc_id` = `payment_types`.`loc_id` OR '0' = `payment_types`.`loc_id` )
						LEFT JOIN `locations` on `orders`.`location_id`=`locations`.`id`
						LEFT JOIN `split_check_details` on `orders`.`order_id`=`split_check_details`.`order_id` AND `split_check_details`.`check`=1
						LEFT JOIN `med_customers` ON (`med_customers`.id = SUBSTRING_INDEX(SUBSTRING_INDEX(`orders`.original_id, '|o|', 4), '|o|', -1))
						WHERE $where_clause
						group by orders.id order by fiscal_date desc;";

		$order_id_list = "";
		
		$rows = array();
		//output_debug_query($query,$vars);
		$report_query = lavu_query($query,$vars);
		while($report_read = mysqli_fetch_assoc($report_query))
		{
			if($order_id_list!="") $order_id_list .= ",";
			$escaped_order_id = str_replace(urlencode("-"),"-",urlencode($report_read['order_id']));
			$order_id_list .= "'".$escaped_order_id."'";
			
			if(isset($report_read['fiscal_type'])){
				$fiscal_type_reg= str_replace("}{","},{",$report_read['fiscal_type']);
				$fiscal_type_reg="[".$fiscal_type_reg."]";
				$fiscal_type = json_decode($fiscal_type_reg,1);
				if(count($fiscal_type)!=1){
					if($r_order_id!=$report_read[order_id]){ $r_order_id=$report_read[order_id]; $j=count($fiscal_type)-1; }
					else { $j--; }
					$report_read['fiscal_type'] = $fiscal_type[$j][receipt_type];
				}else {
					$report_read['fiscal_type'] = $fiscal_type[0][receipt_type];
				}
			}
			else{
				$report_read['fiscal_type'] = "";
			}
			$pv_order_id=$report_read['order_id'];
			$fiscal_pv_query="select print_info from split_check_details where order_id='$pv_order_id' order by id DESC limit 1";
			$fiscal_pv_read = lavu_query($fiscal_pv_query);
			$fiscal_pv_data=mysqli_fetch_assoc($fiscal_pv_read);
			$fiscal_PV = json_decode($fiscal_pv_data['print_info']);
			$report_read['pv_number']=$fiscal_PV->PV_Number;

			if(isset($report_read['fiscal_txn_id']))
			{
				$fiscal_txn_txn= str_replace("}{","},{",$report_read['fiscal_txn_id']);
				$fiscal_txn_txn="[".$fiscal_txn_txn."]";
				$fiscalJsonArray = json_decode($fiscal_txn_txn,1);
				if(count($fiscalJsonArray)!=1){
					if($f_order_id!=$report_read[order_id]){ $f_order_id=$report_read[order_id]; $i=count($fiscalJsonArray)-1; }
					else { $i--; }
					$report_read['fiscal_txn_id']=$fiscalJsonArray[$i][transaction_id];
				}else {
					$report_read['fiscal_txn_id']=$fiscalJsonArray[0][transaction_id];
				}
			}
			$rows[] = $report_read;
		}
		
		$order_data = array();
		$categories = array();

		$elapsed = time() - $startts;

		if (!empty($order_id_list))
		{
			$query = "select `orders`.`total` as `order_total`,			
												concat('[money]',if(`orders`.`void`=0,`orders`.`subtotal`- `orders`.`itax`,'0.00')) as `order_sale`,
												concat('[money]',if(`orders`.`void`=0,`fiscal_transaction_log`.`refund`,'0.00')) as `order_refund`,
                                                 concat('[money]',`fiscal_transaction_log`.`void_amount`) as `order_void`,
                                                 concat('[money]',`fiscal_transaction_log`.`total`) as `total`,
                                                 concat('',`fiscal_transaction_log`.`show`) as `show`,
												concat('[money]',if(`orders`.`void`=0,`orders`.`discount` + `orders`.`idiscount_amount`,'0.00')) as `order_discount`,
												concat('[money]',if(`orders`.`void`=0,`orders`.`subtotal` - (`orders`.`discount` + `orders`.`idiscount_amount` + `orders`.`itax`),'0.00')) as `order_taxable`,
												concat('[money]',if(`orders`.`void`=0,`orders`.`tax` + `orders`.`itax`,'0.00')) as `order_tax`,
												`orders`.`register_name` as `register_name`,
												`menu_categories`.`name` as `category_name`,
												`menu_categories`.`id` as `category_id`,
												`order_contents`.`tax_subtotal1` as `cost`,
												`order_contents`.`order_id` as `order_id`,
												`order_contents`.`item_id` as `item_id`,
                                                `orders`.`void` as `void`
													from `order_contents` 
												left join `menu_categories` on `order_contents`.`category_id`=`menu_categories`.`id` 
												left join `orders` on `order_contents`.`order_id`=`orders`.`order_id` 
												left join `fiscal_transaction_log` on ( `orders`.`order_id`=`fiscal_transaction_log`.`order_id` And `fiscal_transaction_log`.`show` = 1 )
													where `orders`.`order_id` IN ($order_id_list) AND `order_contents`.`quantity`*1 > 0 " ;
			$content_query = lavu_query($query);
			while($content_read = mysqli_fetch_assoc($content_query))
			{
				$order_id = $content_read['order_id'];
				$category_id = $content_read['category_id'];
				$category_name = trim($content_read['category_name']);
				$cost = $content_read['cost'];
				if(!isset($order_data[$order_id][$category_id]))
					$order_data[$order_id][$category_id] = 0;
				$order_data[$order_id][$category_id] += $cost;
				$categories[$category_id] = $category_name;
				$order_data[$order_id]['info'] = $content_read;
				$order_data[$order_id]['register_name'] = $content_read['register_name'];
			}
		}
		
		//echo "<pre>";print_r($order_data); echo "</pre>";

		// Make sure all orders have information for all categories found
		foreach($order_data as $order_id => $order_arr)
		{
			foreach($categories as $category_id => $category_name)
			{
				if(!isset($order_data[$order_id][$category_id]))
					$order_data[$order_id][$category_id] = 0;
			}
		}

		$processed_order_ids = array();
		$refund_query="select order_id ,concat('[money]',format(sum( IF(cc_transactions.action='Refund', cc_transactions.total_collected, 0.00) ),2)) as order_refunded from `cc_transactions`  where `order_id` IN ($order_id_list)  group by order_id ";
		$refuned_query = lavu_query($refund_query);
		while($refuned_read = mysqli_fetch_assoc($refuned_query))
		{
		    $order_id = $refuned_read['order_id'];
		    $refuned_data[$order_id]=$refuned_read['order_refunded'];
		}
		for($i=0; $i<count($rows); $i++)
		{
			$order_id = $rows[$i]['order_id'];
			$already_processed = in_array($order_id, $processed_order_ids);

			$rows[$i]['order_sale'] = $order_data[$order_id]['info']['order_sale'];
			$rows[$i]['order_discount'] = $order_data[$order_id]['info']['order_discount'];
			$rows[$i]['order_taxable'] = $order_data[$order_id]['info']['order_taxable'];
			$rows[$i]['order_tax'] = $order_data[$order_id]['info']['order_tax'];
			if( $order_data[$order_id]['info']['void'] == '1' ) { 
			    $refunened_var='[money]0.00'; 
			    $rows[$i]['total_payment']='[money]0.00';  
			}
			else { 
			    $refunened_var=$refuned_data[$order_id];
			    if ( $order_data[$order_id]['info']['show'] == 1 ) {
			        $rows[$i]['total_payment'] = $order_data[$order_id]['info']['total'];
			    }
			}
			$rows[$i]['order_refund'] = $refunened_var;
			$rows[$i]['order_void'] = $order_data[$order_id]['info']['order_void'];
			$rows[$i]['item_void'] = $order_data[$order_id]['info']['item_void'];
			foreach($categories as $category_id => $category_name)
			{
				$cost = $order_data[$order_id][$category_id];
				$category_name = $categories[$category_id];

				if ($already_processed)
				{
					$rows[$i][$category_name] = "[money]0";
				}
				else
				{
					if (isset($rows[$i][$category_name])) // i.e., 2 categories are named 'Pinta Patagonia'
					{
						$previous_cost = str_replace("[money]", "", $rows[$i][$category_name]);
						$cost += $previous_cost;
					}
					$rows[$i][$category_name] = "[money]".$cost;
				}
			}
			$order_arr = $order_data[$order_id];
			$fiscal_txn_id = $rows[$i]['fiscal_txn_id'];
			$fiscal_type = $rows[$i]['fiscal_type'];
			if(isset($order_arr['register_name']) && trim($order_arr['register_name'])!="" && trim($fiscal_txn_id)!="")
			{
				$rows[$i]['z_location'] = "No ZReport";
				$register_name = trim($order_arr['register_name']);
				if(isset($fiscal_spans["RT".$fiscal_type]["R".$register_name]["I".($fiscal_txn_id * 1)]))
				{
					$rows[$i]['z_location'] = "#" . $fiscal_spans["RT".$fiscal_type]["R".$register_name]["I".($fiscal_txn_id * 1)];
				}
			}
			$processed_order_ids[] = $order_id;
		}

		if ($report_group_by=="day")
		{
			$ommitcols = array("P. V. No","order_id", "fiscal_txn_id", "cuit_local_id", "payment_type", "last_four", "total_tip");
			$sumcols = array("total_payment", "total_cash", "total_card", "order_sale", "order_discount", "order_taxable", "order_tax", "order_refund", "order_void");
			foreach($categories as $category_id => $category_name) $sumcols[] = $category_name;

			for($i=0; $i<count($rows); $i++)
			{
				$rows[$i]['total_cash'] = 0;
				$rows[$i]['total_card'] = 0;
				$payment_type = strtolower(trim($rows[$i]['payment_type']));

				if($payment_type=="cash") $rows[$i]['total_cash'] = $rows[$i]['total_payment'];
				else if($payment_type=="card") $rows[$i]['total_card'] = $rows[$i]['total_payment'];
			}
			for($i=0; $i<count($rows); $i++)
			{
				$newrow = array();
				foreach($rows[$i] as $col => $val)
				{
					if($col=="total_payment") // put cash and card before total
					{
						$newrow['order_sale'] = $rows[$i]['order_sale'];
						$newrow['order_discount'] = $rows[$i]['order_discount'];
						$newrow['order_taxable'] = $rows[$i]['order_taxable'];
						$newrow['order_tax'] = $rows[$i]['order_tax'];
						$newrow['order_refund'] = $rows[$i]['order_refund'];
						$newrow['order_void'] = $rows[$i]['order_void'];
					}
					if($col!="total_cash"&&$col!="total_card")
					{
						$newrow[$col] = $rows[$i][$col];
					}
					if($col=="total_payment") // put cash and card before total
					{
						$newrow['total_cash'] = $rows[$i]['total_cash'];
						$newrow['total_card'] = $rows[$i]['total_card'];
					}
				}
				$rows[$i] = $newrow;
			}


			$lastday = "";
			$dayrows = array();
			for($i=0; $i<count($rows); $i++)
			{
				$nextday = $rows[$i]['fiscal_date'];
				if($nextday!=$lastday || $i==0)
				{
					$newrow = array();
					foreach($rows[$i] as $col => $val)
					{
						if(!in_array($col,$ommitcols))
						{
							$newrow[$col] = $val;
						}
					}
					$dayrows[] = $newrow;
				}
				else
				{
					$crow = count($dayrows) - 1;
					foreach($dayrows[$crow] as $col => $val)
					{
						if(in_array($col,$sumcols))
						{
							$prefix = "";
							$current_val = $dayrows[$crow][$col];
							$add_val = $rows[$i][$col];
							if(substr($current_val,0,7)=="[money]"||substr($add_val,0,7)=="[money]") $prefix = "[money]";
							if(substr($current_val,0,8)=="[number]"||substr($add_val,0,8)=="[number]") $prefix = "[number]";
							if($prefix!="") 
							{
								$current_val = str_replace($prefix,"",$current_val);
								$add_val = str_replace($prefix,"",$add_val);
							}
							$current_val = $current_val * 1 + $add_val * 1;

							if($prefix!="") $current_val = $prefix . $current_val;
							$dayrows[$crow][$col] = $current_val;
						}
					}
				}
				$lastday = $nextday;
			}
			$rows = $dayrows;

			//echo "<pre>Consolidate"; print_r($rows); echo "</pre>";
		}
		else
		{
			for($i=0; $i<count($rows); $i++)
			{
				$newrow = array();
				foreach($rows[$i] as $col => $val)
				{
					if($col=="total_payment") // put taxable before total
					{
						$newrow['order_sale'] = $rows[$i]['order_sale'];
						$newrow['order_discount'] = $rows[$i]['order_discount'];
						$newrow['order_taxable'] = $rows[$i]['order_taxable'];
						$newrow['order_tax'] = $rows[$i]['order_tax'];
					}
					if($col!="total_cash"&&$col!="total_card")
					{
						$newrow[$col] = $rows[$i][$col];
					}
				}
				if(empty($newrow["total_payment"])){
					$newrow["total_payment"]="[money]0";
				}
				if(empty($newrow['order_sale'])){
					$newrow['order_sale']="[money]0";
				}
				if(empty($newrow['order_discount'])){
					$newrow['order_discount']="[money]0";
				}
				if(empty($newrow['order_taxable'])){
					$newrow['order_taxable']="[money]0";
				}
				if(empty($newrow['order_tax'])){
					$newrow['order_tax']="[money]0";
				}
				if(empty($newrow['order_refund'])){
					$newrow['order_refund']="[money]0";
				}
				if(empty($newrow['order_void'])){
				    $newrow['order_void']="[money]0";
				}
				if ( empty($newrow['item_void']) ) {
				    $newrow['item_void']="[money]0";
				}
				$rows[$i] = $newrow;
			}
		}

		$translated_phrases = array();
		$auto_translate = array("schedule","fiscal_date","commercial_date","z_location","fiscal_type","order_id","fiscal_txn_id","cuit_local_id","payment_type","last_four","order_sale","order_discount","order_taxable","order_tax","total_payment");
		for($n=0; $n<count($auto_translate); $n++)
		{
			$phrase = $auto_translate[$n];
			$human_phrase = trim(ucwords(str_replace("_"," ",$phrase)));
			$translated_human_phrase = speak($human_phrase);
			if($translated_human_phrase != $human_phrase){
				$translated_phrase = str_replace(" ","_",trim(str_replace(".","",strtolower($translated_human_phrase))));
				$translated_phrases[$phrase] = $translated_phrase;
			}
		}

		for($i=0; $i<count($rows); $i++)
		{
			$newrow = array();
			foreach($rows[$i] as $col => $val)
			{
				if(isset($translated_phrases[$col])) $newrow[$translated_phrases[$col]] = $val;
				else $newrow[$col] = $val;
			}
			$rows[$i] = $newrow;
		}

		$rows_final = $rows;//for data
	   $data=array();
	   $i=0;
	   $human_phrase = trim(ucwords(str_replace("_"," ","schedule")));
	   $translated_schedule = strtolower(speak($human_phrase));

	   $human_phrase_fis = trim(ucwords(str_replace("_"," ","fiscal_date")));
	   $translated_fis = str_replace(" ","_",trim(str_replace(".","",strtolower(speak($human_phrase_fis)))));

	   $human_phrase_com = trim(ucwords(str_replace("_"," ","commercial_date")));
	   $translated_com = str_replace(" ","_",trim(str_replace(".","",strtolower(speak($human_phrase_com)))));

	   $human_phrase_type = trim(ucwords(str_replace("_"," ","fiscal_type")));
	   $translated_type = str_replace(" ","_",trim(str_replace(".","",strtolower(speak($human_phrase_type)))));

	   $human_phrase_order_id = trim(ucwords(str_replace("_"," ","order_id")));
	   $translated_order_id = str_replace(" ","_",trim(str_replace(".","",strtolower(speak($human_phrase_order_id)))));

	   $human_phrase_fiscal_txn_id = trim(ucwords(str_replace("_"," ","fiscal_txn_id")));
	   $translated_fiscal_txn_id = str_replace(" ","_",trim(str_replace(".","",strtolower(speak($human_phrase_fiscal_txn_id)))));

	   $human_phrase_cuit_local_id = trim(ucwords(str_replace("_"," ","cuit_local_id")));
	   $translated_cuit_local_id = str_replace(" ","_",trim(str_replace(".","",strtolower(speak($human_phrase_cuit_local_id)))));


	   $human_phrase_payment_type = trim(ucwords(str_replace("_"," ","payment_type")));
	   $translated_payment_type = str_replace(" ","_",trim(str_replace(".","",strtolower(speak($human_phrase_payment_type)))));


	   $human_phrase_last_four = trim(ucwords(str_replace("_"," ","last_four")));
	   $translated_last_four = str_replace(" ","_",trim(str_replace(".","",strtolower(speak($human_phrase_last_four)))));


	   $human_phrase_order_sale = trim(ucwords(str_replace("_"," ","order_sale")));
	   $translated_order_sale = str_replace(" ","_",trim(str_replace(".","",strtolower(speak($human_phrase_order_sale)))));

	   $human_phrase_order_discount = trim(ucwords(str_replace("_"," ","order_discount")));
	   $translated_order_discount = str_replace(" ","_",trim(str_replace(".","",strtolower(speak($human_phrase_order_discount)))));

	   $human_phrase_order_taxable = trim(ucwords(str_replace("_"," ","order_taxable")));
	   $translated_order_taxable = str_replace(" ","_",trim(str_replace(".","",strtolower(speak($human_phrase_order_taxable)))));

	   $human_phrase_order_taxable = trim(ucwords(str_replace("_"," ","order_tax")));
	   $translated_order_tax = str_replace(" ","_",trim(str_replace(".","",strtolower(speak($human_phrase_order_taxable)))));

	   $human_phrase_total_payment = trim(ucwords(str_replace("_"," ","total_payment")));
	   $translated_total_payment = str_replace(" ","_",trim(str_replace(".","",strtolower(speak($human_phrase_total_payment)))));


	   foreach ($rows_final as $sum1){
	       $data[0][$i][0]['location_name']=$sum1['location_name'];
	       $data[0][$i][0][$translated_schedule]=$sum1[$translated_schedule];
	       $data[0][$i][0][$translated_fis]=$sum1[$translated_fis];
	       $data[0][$i][0][$translated_com]=$sum1[$translated_com];
	       $data[0][$i][0]['z_location']=$sum1['z_location'];
	       $data[0][$i][0][$translated_type]=$sum1[$translated_type];
	       $data[0][$i][0]['pv_number']=$sum1['pv_number'];

	       //sale and collection
	       if($_REQUEST['sister_select']=='fiscal_accounting'){
	           $data[0][$i][0][$translated_order_id]=$sum1[$translated_order_id];
	           $data[0][$i][0][$translated_fiscal_txn_id]=$sum1[$translated_fiscal_txn_id];
	           $data[0][$i][0][$translated_cuit_local_id]=$sum1[$translated_cuit_local_id];
	           $data[0][$i][0][$translated_payment_type]=$sum1[$translated_payment_type];
	           $data[0][$i][0][$translated_last_four]=$sum1[$translated_last_four];
	       }
	       $data[0][$i][0][$translated_order_sale]=$sum1[$translated_order_sale];
	       $data[0][$i][0][$translated_order_discount]=$sum1[$translated_order_discount];
	       $data[0][$i][0][$translated_order_taxable]=$sum1[$translated_order_taxable];
	       $data[0][$i][0][$translated_order_tax]=$sum1[$translated_order_tax];
	       $data[0][$i][0][$translated_total_payment]=$sum1[$translated_total_payment];
	       $data[0][$i][0]['order_refund']=$sum1['order_refund'];
	       $data[0][$i][0]['order_void']=$sum1['order_void'];

	       // CONSOLIDATED
	       if ( $_REQUEST['sister_select']=='fiscal_daily_accounting') {
	           $data[0][$i][0]['total_cash']=$sum1['total_cash'];
	           $data[0][$i][0]['total_card']=$sum1['total_card'];
	       }

	       $data[0][$i][0]['group_col_payment_types_type']=$sum1['group_col_payment_types_type'];
	       $data[0][$i][0]['CERVEZA']=$sum1[CERVEZA];
	       $data[0][$i][0]['PASAPORTE PATAGONIA']=$sum1['PASAPORTE PATAGONIA'];
	       $data[0][$i][0]['SNACKS']=$sum1['SNACKS'];
	       $data[0][$i][0]['PROMO FIESTAS']=$sum1['PROMO FIESTAS'];
	       $data[0][$i][0]['VASO + PINTA']=$sum1['VASO + PINTA'];
	       $data[0][$i][0]['TIENDA']=$sum1['TIENDA'];
	       $data[0][$i][0]['MERCHANDISING']=$sum1['MERCHANDISING'];
	       //$data[0][$i][0]['SIN ALCOHOL']=$sum1['SIN ALCOHOL'];
	       //$data[0][$i][0]['OTRAS BEBIDAS']=$sum1['OTRAS BEBIDAS'];
	       //$data[0][$i][0]['SNACKS']=$sum1['SNACKS'];
	       //$data[0][$i][0]['PASAPORTE PATAGONIA']=$sum1['PASAPORTE PATAGONIA'];
	       $i++;
	   }
	   return $data;
	}
}
?>
