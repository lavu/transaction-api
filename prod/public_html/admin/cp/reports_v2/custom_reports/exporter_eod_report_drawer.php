<?php
require_once dirname(dirname(__FILE__)) . '/iarea_report_drawer.php';
require_once dirname(dirname(__FILE__)) . '/area_reports.php';
require_once dirname(dirname(__FILE__)) . '/report_functions.php';
require_once dirname(dirname(__FILE__)) . '/exporter_report_drawer.php';

class exporter_eod_report_drawer extends exporter_report_drawer {

	protected
		$area_reports;

	public function __construct( $area_reports ){
		$this->area_reports = $area_reports;
	}

	public function output( $export_str ){
		$this->area_reports->output_export_string( strtolower( str_replace(" ", "_", $this->area_reports->reportFullName() ) ).'.csv', $export_str );
	}

	//Expecting 3 loopable materials
	//
	// Section
	//  -> table
	//    -> row (columns)
	public function draw_multiple_rows( $report_results ) {
	    $mainArray = array();
	    foreach($report_results as $topLevelKey => $data ){
	        if ( in_array( $topLevelKey, array( "0", "1", "2" ) ) ) {
	            $newDataArr=array();
	            foreach( $data as $dataKey => $value ){
	                $newDataArr[$dataKey]=$value;
	                if( $topLevelKey == 1 && in_array( $dataKey, array( "0", "2" ) ) && isset( $report_results[$topLevelKey][$dataKey][0] ) ) {
	                    $newDataArr[$dataKey] = $report_results[$topLevelKey][$dataKey][0];
	                }
	                else if( count( $value ) > 0 ) {
	                    foreach( $value as $secondLevelKey => $secondLevelData ){
	                        if(isset($secondLevelData[0])){
	                            $newDataArr[$dataKey][$secondLevelKey] = $secondLevelData[0];
	                        }else{
	                            unset ($newDataArr[$dataKey][$secondLevelKey]);
	                        }
	                    }
	                }
	            }
	            $mainArray[$topLevelKey]=$newDataArr;
	        }
	        else{
	            $mainArray[$topLevelKey] = $report_results[$topLevelKey];
	        }
	    }
	    $report_results=$mainArray;
		$report_results = array( $report_results );
		$str = '';
		foreach( $report_results as $report_results_index => $location ){
			foreach( $location as $location_index => $category ){
				if( $category['title'] == 'Extra' ){
					$str .= $this->escape_value( $category['title'] );
					$str .= $this->getRowDelimeter();
					foreach( $category as $category_index => $section ){
						if( $category_index == 'title' && !is_array($section) ){
							continue;
						}

						if( count( $section ) == 0){
							continue;
						}
						$it = 0;
						foreach( $section as $section_index => $row ){
							if( $it++ === 0 ){
								$keys = array_keys( $row );
								$first_key = $keys[0];
								foreach( $row as $column_name => $value ){
									if( !$this->area_reports->should_show_col( $column_name ) ){
										continue;
									}
									$str .= $this->escape_value( show_as_title( $column_name ) );
									$str .= $this->getColDelimeter();
								}
								$str .= $this->getRowDelimeter();
							}
							foreach( $row as $column_name => $value ){
								if( !$this->area_reports->should_show_col( $column_name ) ){
									continue;
								}
								$display_value = $this->area_reports->show_reported_value( $value );
								$str .= $this->escape_value( $display_value );
								$str .= $this->getColDelimeter();
							}
							$str .= $this->getRowDelimeter();
						}
					}
					continue;
				}
				foreach( $category as $category_index => $section){
        					if( $category_index == 'title' && !is_array($section) ){
        						$str .= $this->escape_value( $category['title'] );
        						$str .= $this->getRowDelimeter();
        						continue;
        					}
        					
        					if( count( $section ) == 0 ){
        						continue;
        					}
        
        					$it = 0;
        					$subtotals = array();
        					foreach( $section as $section_index => $row ){
        						if( $it++ === 0 ){
        							$str .= $this->getRowDelimeter();
        							$row_first_key = array_keys( $row );
        							$row_second_key = $row_first_key[1]; 
        							$row_third_key = $row_first_key[2]; 
        							$row_first_key = $row_first_key[0];
        							foreach( $row as $column_name => $value ){
        								if( !$this->area_reports->should_show_col( $column_name ) ){
        									continue;
        								}
        								$str .= $this->escape_value(show_as_title($column_name));
        								$str .= $this->getColDelimeter();
        							}
        							$str .= $this->getRowDelimeter();
        						}
        
        						foreach( $row as $column_name => $value ){
        							if( !$this->area_reports->should_show_col( $column_name ) ){
        								continue;
        							}
        							$display_value = $this->area_reports->show_reported_value( $value );
        							if( report_value_is_number( $value ) || report_value_is_money( $value ) ){
        								if( report_value_is_number( $value ) ){
        									$str .= $this->escape_value( $display_value );
        									$str .= $this->getColDelimeter();
        									if(!isset($subtotals[$column_name])){
        										$subtotals[$column_name] = 0;
        									}
        									$subtotals[$column_name] +=  $display_value;
        								} else {
        									$str .= $this->escape_value( $display_value );
        									$str .= $this->getColDelimeter();
        									$subtotals[$column_name] = '[money]'.(report_money_to_number(isset($subtotals[$column_name])?$subtotals[$column_name]:'[money]0')*1 + report_money_to_number($value)*1);
        								}
        							} else {
        								$str .= $this->escape_value( $value );
        								$str .= $this->getColDelimeter();
        								
        								$subtotals[$column_name] = '';
        							}
        						}
        						$str .= $this->getRowDelimeter();
        					}
        					$it = 0;
        					foreach( $subtotals as $column_name => $value ){
        						$display_value = $this->area_reports->show_reported_value( $value );
        						$str .= $this->escape_value( $display_value );
        						$str .= $this->getColDelimeter();
        					}
        					$str .= $this->getRowDelimeter();
        				}
				}
		}
		return $this->output($str);
	}

	public function draw_single_row( $report_results ) {
		return $this->draw_multiple_rows( $report_results );
	}

	public function draw_no_data() {
		return 'No EOD Data Found';
	}

}