(function(){
	if( !window.com ){
		window.com = {};
	}

	if( !com.lavu ){
		com.lavu = {};
	}

	if( !com.lavu.reportsv2 ){
		com.lavu.reportsv2 = {};
	}

	if( !com.lavu.reportsv2.settlement ){
		com.lavu.reportsv2.settlement = {};
	}

	(function(){
		function changeCorrespondingCheckboxes( ele ){
			var section = ele.getAttribute('section');
			if( section === null || section === undefined ){
				return;
			}

			if( ele.checked ){
				turnCheckboxesOnWithSection( section );
			} else {
				turnCheckboxesOffWithSection( section );
			}
			console.log( ele );
		}
		com.lavu.reportsv2.settlement.changeCorrespondingCheckboxes = changeCorrespondingCheckboxes;

		function turnCheckboxesOnWithSection( section ){
			var eles = document.querySelectorAll( 'input[type=checkbox][section=' + section +']' );
			for( var i = 0; i < eles.length; i++ ){
				eles[i].checked = true;
			}
		}
		// com.lavu.reportsv2.settlement.turnCheckboxesOnWithSection = turnCheckboxesOnWithSection;

		function turnCheckboxesOffWithSection( section ){
			var eles = document.querySelectorAll( 'input[type=checkbox][section=' + section +']' );
			for( var i = 0; i < eles.length; i++ ){
				eles[i].checked = false;
			}
		}
		// com.lavu.reportsv2.settlement.turnCheckboxesOffWithSection = turnCheckboxesOffWithSection;


		function updateValue( event ) {
			this.setAttribute( 'value', this.value );
			if( this.hasAttribute('startingvalue') && this.getAttribute('startingvalue') != this.value ) {
				this.classList.add('diff');
			} else {
				this.classList.remove('diff');
			}
		}
		com.lavu.reportsv2.settlement.updateValue = updateValue

		function ajaxRequest(url, query, handler){
			var xmlHttpReq = null;
			// Mozilla/Safari
			if (window.XMLHttpRequest) {
				xmlHttpReq = new XMLHttpRequest();
			} else if (window.ActiveXObject) { // IE
				xmlHttpReq = new ActiveXObject("Microsoft.XMLHTTP");
			}
			xmlHttpReq.open('POST', url, true);
			xmlHttpReq.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
			function handleReadStateChange( event ){
				if( xmlHttpReq.readyState == 4 ){
					if( xmlHttpReq.status == 200 ){
						handler( xmlHttpReq.responseText );
					} else {
						console.log( 'Error: ', xmlHttpReq.responseText );
					}
					xmlHttpReq.removeEventListener( 'readystatechange'. handleReadStateChange, false );
				}
			}
			xmlHttpReq.addEventListener('readystatechange', handleReadStateChange, false );
			xmlHttpReq.send(query);
		}

		function processOfflinePayment( report_id, mode, event ) {
			var payment_id = this.name.substr( 16 )*1;
			var button = this;
			button.setAttribute('disabled','');
			var div = document.querySelector('[payment="'+payment_id+'"]');
			function handleProcessOfflinePaymentResult( data ) {
				var result = JSON.parse( data );
				console.log( result );
				button.removeAttribute('disabled','');
				if( result.status == 'success' ) {
					div.removeAttribute('unprocessed');
					div.setAttribute('processed', 'approved');
					button.remove();
				} else if( result.status == 'failed' ) {
					if( result.reason.indexOf('No response received from gateway.') === -1 ){
						div.removeAttribute('unprocessed');
						div.setAttribute('processed', 'declined');
						button.remove();

						var error_message_ele = document.querySelector('[payment="'+payment_id+'"] .errormessage');
						error_message_ele.appendChild( document.createTextNode( result.reason ) );
						error_message_ele.removeAttribute('hide');
					} else {
						div.removeAttribute('unprocessed');
						div.setAttribute('processed', 'declined');
					}
					// alert( result.reason );
				}
			}

			console.log( this, event );
			ajaxRequest('?widget=reports/v2_entry&report=' + report_id + '&mode=' + mode, 'offline_payment=' + payment_id, handleProcessOfflinePaymentResult );
		}
		com.lavu.reportsv2.settlement.processOfflinePayment = processOfflinePayment;

		function handleTipAdjustmentResult( data ) {
			var obj = JSON.parse( data );

			for( var payment_id in obj ) {
				var payment_input = document.querySelector('input[name=tip_adjustment_payment_' + payment_id + ']');
				if( obj[payment_id].status == 'success' ) {
					payment_input.setAttribute( 'startingvalue', payment_input.value );
					payment_input.classList.remove( 'diff' );
				} else {
					payment_input.classList.add( 'failed' );
				}
			}
		}

		function submitTipAdjustments( report_id, mode, event  ) {
			var tip_adjustment_inputs = document.querySelectorAll('input[name^=tip_adjustment_payment_].diff');

			var submission = {};
			for( var i = 0; i < tip_adjustment_inputs.length; i++ ) {
				var payment_id = tip_adjustment_inputs[i].getAttribute('name').substr(23)*1;
				submission[ payment_id ] = tip_adjustment_inputs[i].value;
			}

			var toSubmit = 'tip_adjustments=' + encodeURIComponent( JSON.stringify( submission ) );

			ajaxRequest('?widget=reports/v2_entry&report=' + report_id + '&mode=' + mode, toSubmit, handleTipAdjustmentResult );
		}
		com.lavu.reportsv2.settlement.submitTipAdjustments = submitTipAdjustments;

		function settleBatch( report_id, mode, event ) {
			function handleSettleBatchResult( data ) {
				var obj = JSON.parse( data );
				if( obj.batch_result.status == 'success' ) {
					click_to_page( "mode=" + mode )
				} else if ( obj.batch_result.status == 'failed' ) {
					alert( "Batch Failed: " + obj.batch_result.reason );
				}
				console.log( obj );
			}
			var response = confirm("Proceed with credit card transaction batch settlement only if all tip amounts have been entered and applied and open authorizations have been captured. If you are in the process of adjusting tips for more than one day, you must send all tip adjustments for orders created since the last batch settlement before settling the current credit card batch.");

			if( response ) {
				var toSubmit = 'settle_batch=1';
				ajaxRequest('?widget=reports/v2_entry&report=' + report_id + '&mode=' + mode, toSubmit, handleSettleBatchResult );
			}
		}
		com.lavu.reportsv2.settlement.settleBatch = settleBatch;
	})();
})();