<?php
    function general_ledger($vars) {
		$vars['mode'] = $_GET['mode'];
		$property_query = lavu_query("SELECT `value` FROM `config` WHERE `setting` = 'general_ledger_property_number' AND `location` = '1'");
		$property_data = mysqli_fetch_assoc($property_query);
		$property = $property_data['value'];
		$general_ledger_query = lavu_query("SELECT * FROM `general_ledger_settings` WHERE `_deleted` = '0' ORDER BY `position` ASC");
		if (mysqli_num_rows($general_ledger_query) > 0) {
			if ($vars['mode'] == '' || $vars['mode'] == 'day') {
				//General Ledger Start Date Time,End Date Time
				$general_start = explode(" ", $vars['start_datetime']);
				$general_end = explode(" ", $vars['end_datetime']);
				$general_start_date = $general_start[0];
				$general_end_date = $general_end[0];
				$general_start_time = $general_start[1];
				$general_end_time = $general_end[1];
				//Check If Time Selected & End Time Greater than Start Time
				if ($general_start_time != $general_end_time && $general_end_time > $general_start_time) {
					//CC Where Clause
					$ccWhereClause = "(DATE(cc_transactions.datetime) BETWEEN '$general_start_date' AND '$general_end_date') AND (TIME(cc_transactions.datetime) BETWEEN '$general_start_time' AND '$general_end_time')";
					//Order Where Clause
					$ordersWhereClause = "(DATE(orders.closed) BETWEEN '$general_start_date' AND '$general_end_date') AND (TIME(orders.closed) BETWEEN '$general_start_time' AND '$general_end_time')";
				} else {
					//If Time Not Selected
					$ordersWhereClause = "orders.closed >= '$vars[start_datetime]' AND orders.closed < '$vars[end_datetime]'";
					$ccWhereClause = "cc_transactions.datetime > '$vars[start_datetime]' AND cc_transactions.datetime < '$vars[end_datetime]'";
				}
			} else {
				$ordersWhereClause = "orders.closed >= '$vars[start_datetime]' AND orders.closed < '$vars[end_datetime]'";
				$ccWhereClause = "cc_transactions.datetime > '$vars[start_datetime]' AND cc_transactions.datetime < '$vars[end_datetime]'";
			}
			$payment_details = getPaymentDetails($ccWhereClause);
			$total_tax_amount = getTaxAmount('1', $ordersWhereClause);
			$discount_amount = getDiscount($ordersWhereClause);
			$reconciliation = getReconciliationDetails($total_tax_amount, $discount_amount, $ordersWhereClause, $ccWhereClause);
			$deposit_amount = getDepositAmount($ccWhereClause);
			$value = 0;
			while ($gl_read = mysqli_fetch_assoc($general_ledger_query)) {
				$data = array();
				$amount = '0';
				$line_name = '';
				$transaction_type = "";
				if ($gl_read['transaction_type'] == 'credit') {
					$transaction_type = "-";
				} else {
					$transaction_type = "";
				}
				if ($gl_read['lineitem_name'] != '') {
					$line_name = $gl_read['lineitem_name'];
				}
				if ($gl_read['lineitem_name'] == 'Cash Payments') {
					$amount = $payment_details[cash_amount];
				} else if ($gl_read['lineitem_name'] == 'Credit Card Sales') {
					$amount = $payment_details[card_amount];
				} else if ($gl_read['lineitem_name'] == 'Tips') {
					$amount = $payment_details[tip_amount] + $reconciliation[gratuity];
				} else if ($gl_read['lineitem_name'] == 'Sales Tax') {
					$amount = $total_tax_amount;
				} else if ($gl_read['lineitem_name'] == 'Discounts') {
					$amount = $discount_amount;
				} else if ($gl_read['lineitem_name'] == 'Reconciliation') {
					$amount = $reconciliation[reconciliation];
				} else if ($gl_read['lineitem_name'] == 'Deposits') {
					$amount = $deposit_amount;
				} else {
					if ($gl_read['type'] == 'super_group') {
						$sg_details = getSuperGroupDetails($gl_read['ref_id'], $ordersWhereClause);
						$amount = $sg_details[amount];
						$line_name = $sg_details[name];
					} else {
						$amount = 0;
					}
				}
				$data[0][property] = $property;
				$data[0][account] = $gl_read['account_number'];
				$data[0][name] = $line_name;
				$data[0][amount] = "[money]".$transaction_type.$amount;
				$data[0][description] = $gl_read['description'];
				$general_ledger[$value] = $data;
				$value++;
			}
			$gl_data[] = $general_ledger;
		}
		return $gl_data;
    }
	
	function getPaymentDetails($ccWhereClause) {
		$payment_query = "SELECT concat(cc_transactions.pay_type, concat(' ', cc_transactions.card_type)) AS pay_type,
                       cc_transactions.pay_type AS pay_type_raw,
                       cc_transactions.action,
                       cc_transactions.voided,
                       count(*) AS COUNT,
                       if(count(*) > 0, concat(cast(sum(cc_transactions.tip_amount)AS decimal(30,5))), '0.00') AS tip_amount,
                       if(count(*) > 0, concat(cast(sum(cc_transactions.total_collected)AS decimal(30,5))), '0.00') AS total_collected,
                       `cc_transactions`.`pay_type` AS `group_col_cc_transactions_pay_type`,
                       `cc_transactions`.`card_type` AS `group_col_cc_transactions_card_type`,
                       `cc_transactions`.`action` AS `group_col_cc_transactions_action`,
                       `cc_transactions`.`voided` AS `group_col_cc_transactions_voided`,
                       `cc_transactions`.`pay_type` AS `group_col_cc_transactions_pay_type`,
                       `cc_transactions`.`pay_type` AS `group_col_cc_transactions_pay_type`
                FROM `cc_transactions`
                WHERE $ccWhereClause
                  AND cc_transactions.action != ''
                  AND cc_transactions.action NOT LIKE '%issue%'
                  AND cc_transactions.action NOT LIKE '%reload%'
                  AND cc_transactions.process_data NOT LIKE 'LOCT%'
                  AND cc_transactions.order_id NOT LIKE 'Paid%'
                  AND cc_transactions.process_data NOT LIKE 'LOCT%'
                  AND cc_transactions.currency_type != '1'
                  AND cc_transactions.loc_id = '1'
                  AND cc_transactions.voided='0'
                GROUP BY concat(cc_transactions.pay_type, concat(' ', cc_transactions.card_type)),
                         cc_transactions.action,
                         cc_transactions.voided
                ORDER BY (sum(cc_transactions.total_collected)*1) DESC";
	    $payment_read = lavu_query($payment_query);
	    $cash_amount = 0;
	    $card_amount = 0;
	    $tip_amount = 0;
	    while ($payment_data = mysqli_fetch_assoc($payment_read)) {
	        if ($payment_data['pay_type_raw'] == 'Cash') {
	            if ($payment_data['action'] == 'Sale' || $payment_data['action'] == 'CashTips') {
	                $cash_amount = $cash_amount + $payment_data['total_collected'];
	                $tip_amount = $tip_amount + $payment_data['tip_amount'];
	            }
	            if ($payment_data['action'] == 'Refund') {
	                $cash_amount = $cash_amount - $payment_data['total_collected'];
	                $tip_amount = $tip_amount - $payment_data['tip_amount'];
	            }
	        } else if ($payment_data['pay_type_raw']=='Card' || $payment_data['pay_type_raw'] == 'Paypal') {
	            if ($payment_data['action'] == 'Sale') {
	                $card_amount = $card_amount + $payment_data['total_collected'];
	                $tip_amount = $tip_amount + $payment_data['tip_amount'];
	            }
	            if ($payment_data['action'] == 'Refund') {
	                $card_amount = $card_amount - $payment_data['total_collected'];
	                $tip_amount = $tip_amount - $payment_data['tip_amount'];
	            }
	        }
	    }
	    $payment_details[cash_amount] = $cash_amount;
	    $payment_details[card_amount] = $card_amount;
	    $payment_details[tip_amount] = $tip_amount ;
	    return $payment_details;
	}

	function getTaxAmount($locationid, $ordersWhereClause) {
		$tax_amount = 0;
		$tax_query1 = lavu_query("SELECT if(count(*) > 0, concat( cast(sum(order_contents.tax1) AS decimal(30,2))), '0.00') AS tax FROM `orders`LEFT JOIN `order_contents` ON `orders`.`order_id` = `order_contents`.`order_id` AND `orders`.`location_id` = `order_contents`.`loc_id` WHERE order_contents.tax_exempt = 0 AND order_contents.tax_rate1 != 0 AND $ordersWhereClause AND orders.void = '0' AND order_contents.loc_id = '$locationid' AND order_contents.quantity > '0' GROUP BY order_contents.tax_inclusion,order_contents.tax_name1,order_contents.tax_rate1*1,order_contents.tax_exempt,if( order_contents.tax_exempt = '1', order_contents.exemption_name, '') ORDER BY order_contents.tax_rate1*1 DESC");
	    if (mysqli_num_rows($tax_query1)) {
	        while ($get_total_taxes1 = mysqli_fetch_assoc($tax_query1)) {
	            $tax_amount = $tax_amount + $get_total_taxes1['tax'];
	        }
	    }
	    $tax_query2 = lavu_query("select  if(count(*) > 0, concat(cast(sum(order_contents.tax2) AS decimal(30,2))), '0.00') AS tax FROM `orders`LEFT JOIN `order_contents` ON `orders`.`order_id` = `order_contents`.`order_id` AND `orders`.`location_id` = `order_contents`.`loc_id` WHERE order_contents.tax_exempt = 0 AND order_contents.tax_rate2 != 0 AND $ordersWhereClause AND orders.void = '0' AND order_contents.loc_id = '$locationid' AND order_contents.quantity > '0' GROUP BY order_contents.tax_inclusion,order_contents.tax_name2,order_contents.tax_rate2*1,order_contents.tax_exempt,if( order_contents.tax_exempt = '1', order_contents.exemption_name, '') ORDER BY order_contents.tax_rate2*1 DESC");
	    if (mysqli_num_rows($tax_query2)) {
	        while ($get_total_taxes2 = mysqli_fetch_assoc($tax_query2)) {
	            $tax_amount = $tax_amount + $get_total_taxes2['tax'];
	        }
	    }

	    $tax_query3 = lavu_query("select  if(count(*) > 0, concat(cast(sum(order_contents.tax3)as decimal(30,2))), '0.00') AS tax FROM `orders`LEFT JOIN `order_contents` ON `orders`.`order_id` = `order_contents`.`order_id` AND `orders`.`location_id` = `order_contents`.`loc_id` WHERE order_contents.tax_exempt = 0 AND order_contents.tax_rate3 != 0 AND $ordersWhereClause AND orders.void = '0' AND order_contents.loc_id = 'locationid'and order_contents.quantity > '0' ");
	    if (mysqli_num_rows($tax_query3)) {
	        while ($get_total_taxes3 = mysqli_fetch_assoc($tax_query3)) {
	            $tax_amount = $tax_amount + $get_total_taxes3['tax'];
	        }
	    }

	    $tax_query4 = lavu_query("select if(count(*) > 0, concat(cast(sum(order_contents.tax4)as decimal(30,2))), '0.00') AS tax FROM `orders`LEFT JOIN `order_contents` ON `orders`.`order_id` = `order_contents`.`order_id` AND `orders`.`location_id` = `order_contents`.`loc_id` WHERE order_contents.tax_exempt = 0 AND order_contents.tax_rate4 != 0 AND $ordersWhereClause AND orders.void = '0' AND order_contents.loc_id = '$locationid' AND order_contents.quantity > '0' ");
	    if (mysqli_num_rows($tax_query4)) {
	        while ($get_total_taxes4 = mysqli_fetch_assoc($tax_query4)) {
	            $tax_amount = $tax_amount + $get_total_taxes4['tax'];
	        }
	    }
	    
	    $tax_query5 = lavu_query("select  if(count(*) > 0, concat(cast(sum(order_contents.tax5)as decimal(30,2))), '0.00') AS tax FROM `orders`LEFT JOIN `order_contents` ON `orders`.`order_id` = `order_contents`.`order_id` AND `orders`.`location_id` = `order_contents`.`loc_id` WHERE order_contents.tax_exempt = 0 AND order_contents.tax_rate5 != 0 AND $ordersWhereClause AND orders.void = '0' AND order_contents.loc_id = '$locationid' AND order_contents.quantity > '0' ");
	    if (mysqli_num_rows($tax_query5)) {
	        while ($get_total_taxes5 = mysqli_fetch_assoc($tax_query5)) {
	            $tax_amount = $tax_amount + $get_total_taxes5['tax'];
	        }
	    }
	    return $tax_amount;
	}

	function getDiscount($ordersWhereClause) {
	    $discount_query1 = "SELECT if(count(*) > 0,cast(sum(order_contents.idiscount_amount)AS decimal(30,5)), '0.00') AS discount
                                FROM `orders`
                                LEFT JOIN `order_contents` ON `orders`.`order_id` = `order_contents`.`order_id`
                                AND `orders`.`location_id` = `order_contents`.`loc_id`
                                WHERE $ordersWhereClause
                                  AND orders.currency_type = '0'
                                  AND orders.void = '0'
                                  AND order_contents.idiscount_amount*1 != 0
                                  AND order_contents.loc_id = '1'
                                  AND order_contents.quantity > '0' ";
	    $discount_read1 = mysqli_fetch_assoc(lavu_query($discount_query1));
	    $discount1 = $discount_read1[discount];
	    $discount_query2 = "SELECT if(count(*) > 0, cast(sum(split_check_details.discount)AS decimal(30,5)), '0.00') AS discount
                                FROM `orders`
                                LEFT JOIN `split_check_details` ON `orders`.`order_id` = `split_check_details`.`order_id`
                                AND `orders`.`location_id` = `split_check_details`.`loc_id`
                                WHERE $ordersWhereClause
                                  AND orders.currency_type = '0'
                                  AND orders.void = '0'
                                  AND split_check_details.discount*1 != 0
                                  AND split_check_details.loc_id = '1'";
	    $discount_read2 = mysqli_fetch_assoc(lavu_query($discount_query2));
	    $discount2 = $discount_read2[discount];
	    return $discount1 + $discount2;
	}
	function getSuperGroupDetails($super_id, $ordersWhereClause) {
		$sg_query = "SELECT super_groups.id,
                       super_groups.title AS super_group,
                       if(count(*) > 0, concat(cast(sum((order_contents.price + order_contents.modify_price + order_contents.forced_modifiers_price)*order_contents.quantity - if(order_contents.quantity > '0', order_contents.itax, 0))AS decimal(30,5))), '0.00') AS gross
                FROM `orders`
                LEFT JOIN `order_contents` ON `orders`.`order_id` = `order_contents`.`order_id`
                AND `orders`.`location_id` = `order_contents`.`loc_id`
                LEFT JOIN `menu_items` ON `order_contents`.`item_id` = `menu_items`.`id`
                LEFT JOIN `menu_categories` ON `menu_items`.`category_id` = `menu_categories`.`id`
                LEFT JOIN `super_groups` ON IF (menu_items.super_group_id = '',
                                                menu_categories.super_group_id,
                                                menu_items.super_group_id) = `super_groups`.`id`
                WHERE $ordersWhereClause
                  AND orders.currency_type = '0'
                  AND orders.void = '0'
                  AND order_contents.item != 'SENDPOINT'
                  AND super_groups.id='$super_id'";
        $sg_read = mysqli_fetch_assoc(lavu_query($sg_query));
        $sg_details[amount] = $sg_read[gross];
        $sg_details[name] = $sg_read[super_group];
        return $sg_details;
	}

	function getReconciliationDetails($tax, $discount, $ordersWhereClause, $ccWhereClause) {
		$actual_summary = 0;
		$expected = 0;
		$sale_query = "SELECT if(count(*) > 0, concat(cast(sum(cc_transactions.total_collected)as decimal(30,5))), '0.00') AS total_collected FROM `cc_transactions` WHERE $ccWhereClause AND cc_transactions.action != '' AND cc_transactions.action NOT LIKE '%issue%' AND cc_transactions.action NOT LIKE '%reload%' AND cc_transactions.process_data NOT LIKE 'LOCT%' AND cc_transactions.order_id NOT LIKE 'Paid%' AND cc_transactions.process_data NOT LIKE 'LOCT%' AND cc_transactions.currency_type != '1' AND cc_transactions.loc_id = '1' AND cc_transactions.action != 'Refund' AND cc_transactions.voided = '0'";
		$sale_read = mysqli_fetch_assoc(lavu_query($sale_query));
        $sale = $sale_read[total_collected];
        $refund_query = "SELECT if(count(*) > 0, concat(cast(sum(cc_transactions.total_collected)as decimal(30,5))), '0.00') AS total_collected FROM `cc_transactions` WHERE $ccWhereClause AND cc_transactions.action != '' AND cc_transactions.action NOT LIKE '%issue%' AND cc_transactions.action NOT LIKE '%reload%' AND cc_transactions.process_data NOT LIKE 'LOCT%' AND cc_transactions.order_id NOT LIKE 'Paid%' AND cc_transactions.process_data NOT LIKE 'LOCT%' AND cc_transactions.currency_type != '1' AND cc_transactions.loc_id = '1' AND cc_transactions.action != 'Sale' AND cc_transactions.voided = '0'";
        $refund_read = mysqli_fetch_assoc(lavu_query($refund_query));
        $refund = $refund_read[total_collected];
        $payment_mismatch_query = "SELECT '' AS payment_mismatch, orders.order_id, sum(format((orders.cash_applied + orders.card_paid + orders.gift_certificate + orders.alt_paid) - (orders.total),2)) AS amount FROM `orders` WHERE $ordersWhereClause AND orders.currency_type = '0' AND orders.void = '0' AND format((orders.cash_applied + orders.card_paid + orders.gift_certificate + orders.alt_paid),4) - format(orders.total,2) AND orders.void = '0' AND orders.order_id NOT LIKE 'Paid%' AND orders.location_id = '1'";
        $payment_mismatch_read = mysqli_fetch_assoc(lavu_query($payment_mismatch_query));
        $payment_mismatch = $payment_mismatch_read[amount];
        $actual_summary = $sale-$refund-$payment_mismatch;
        $category_query = "SELECT if(count(*) > 0, concat(cast(sum((order_contents.price + order_contents.modify_price + order_contents.forced_modifiers_price)*order_contents.quantity - if( order_contents.quantity > '0', order_contents.itax, 0) ) AS decimal(30,5))), '0.00') AS gross FROM `orders`LEFT JOIN `order_contents` ON `orders`.`order_id` = `order_contents`.`order_id` AND `orders`.`location_id` = `order_contents`.`loc_id` LEFT JOIN `menu_items` ON `order_contents`.`item_id` = `menu_items`.`id` LEFT JOIN `menu_categories` ON `menu_items`.`category_id` = `menu_categories`.`id` WHERE $ordersWhereClause AND `orders`.`currency_type` = '0' AND `orders`.`void` = '0' AND `order_contents`.`item` != 'SENDPOINT' AND `orders`.`currency_type` != '1' AND `order_contents`.`loc_id` = '1'";
        $category_read = mysqli_fetch_assoc(lavu_query($category_query));
        $category = $category_read[gross];
        $gratuity_query = "SELECT if(count(*) > 0, concat(cast(sum(split_check_details.gratuity)as decimal(30,5))), '0.00') AS gratuity, if(count(*) > 0, concat(cast(sum(split_check_details.rounding_amount) AS decimal(30,5))), '0.00') AS rounding_amount FROM `orders` LEFT JOIN `split_check_details` ON `orders`.`order_id` = `split_check_details`.`order_id` AND `orders`.`location_id` = `split_check_details`.`loc_id` WHERE $ordersWhereClause AND `orders`.`currency_type` = '0' AND `orders`.`void` = '0' AND `orders`.`location_id` = '1' AND `orders`.`order_id` NOT LIKE 'Paid%' ORDER BY (sum(split_check_details.gratuity)*1) ";
        $gratuity_read = mysqli_fetch_assoc(lavu_query($gratuity_query));
        $gratuity = $gratuity_read[gratuity];
        $rounding_amount = $gratuity_read[rounding_amount];
        $expected = $category + $tax + $gratuity - $discount + $rounding_amount;
        $allow_deposit_query = "SELECT concat( cc_transactions.pay_type, concat( ' ', cc_transactions.card_type)) AS pay_type, cc_transactions.action, cc_transactions.voided, count(*) AS count, if(count(*) > 0, format(cast(sum(cc_transactions.tip_amount) AS decimal(30,5)),2), '0.00') AS tip_amount, if(count(*) > 0, format(cast(sum(cc_transactions.total_collected) AS decimal(30,5)),2), '0.00') AS total_collected FROM `cc_transactions` WHERE `cc_transactions`.`for_deposit` = '1' AND $ccWhereClause AND `cc_transactions`.`voided` = '0' AND `cc_transactions`.`action` != 'Void' AND `cc_transactions`.`action` NOT LIKE '%issue%' AND `cc_transactions`.`action` NOT LIKE '%reload%' AND `cc_transactions`.`process_data` NOT LIKE 'LOCT%' AND `cc_transactions`.`action` != '' AND `cc_transactions`.`order_id` NOT LIKE 'Paid%' AND `cc_transactions`.`process_data` NOT LIKE 'LOCT%' AND `cc_transactions`.`loc_id` = '1' ";
        $allow_deposit_read = mysqli_fetch_assoc(lavu_query($allow_deposit_query));
        $allow_deposit = $allow_deposit_read[total_collected];
        $allow_refund_query = "SELECT concat( cc_transactions.pay_type, concat( ' ', cc_transactions.card_type)) AS pay_type, cc_transactions.action, cc_transactions.voided, count(*) AS count, if(count(*) > 0, format(cast(sum(cc_transactions.tip_amount) AS decimal(30,5)),2), '0.00') AS tip_amount, if(count(*) > 0, format(cast(sum(cc_transactions.total_collected) AS decimal(30,5)),2), '0.00') AS total_collected  FROM `cc_transactions` LEFT JOIN `orders` ON `cc_transactions`.`order_id` = `orders`.`order_id` AND `cc_transactions`.`loc_id` = `orders`.`location_id` WHERE `cc_transactions`.`for_deposit` = '1' AND $ordersWhereClause AND orders.currency_type = '0' AND `orders`.`void` = '0' AND `cc_transactions`.`order_id` NOT LIKE 'Paid%' AND `cc_transactions`.`process_data` NOT LIKE 'LOCT%' AND `cc_transactions`.`loc_id` = '1'";
        $allow_refund_read = mysqli_fetch_assoc(lavu_query($allow_refund_query));
        $allow_refund = $allow_refund_read[total_collected];
        $allow_deposit_amount = $allow_deposit - $allow_refund;
        $return[reconciliation] = $actual_summary-$expected - $allow_deposit_amount;
        $return[gratuity] = $gratuity;
        return $return;
	}
	function getDepositAmount($ccWhereClause) {
		$deposit_amount = 0;
		$deposit_query = lavu_query("SELECT if(count(*) > 0, concat('', format(sum( IF(`cc_transactions`.`transtype` = 'deposit_refund', -1*`cc_transactions`.`amount`, `cc_transactions`.`amount`)),2)), '0.00') AS amount FROM `deposit_customer_info` AS `dci`
				JOIN `deposit_information` AS `di` ON (`dci`.`id` = `di`.`customer_id`)
				JOIN `cc_transactions` ON (`cc_transactions`.`order_id` = `di`.`transaction_id` )
				WHERE $ccWhereClause");
		if (mysqli_num_rows($deposit_query)) {
			$deposit_read = mysqli_fetch_assoc($deposit_query);
			$deposit_amount = $deposit_read['amount'];
		}
		return $deposit_amount;
	}
?>
