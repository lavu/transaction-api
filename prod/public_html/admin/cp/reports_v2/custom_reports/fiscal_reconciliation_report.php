<?php
function fiscal_reconciliation_report($vars) {
    $queryMode = isset($_REQUEST['mode']) && !empty($_REQUEST['mode']) ? $_REQUEST['mode'] : false;
    $getWhereClause = setWhereClauseForReport($vars, $queryMode);
    $whereClause = $getWhereClause['where_clause'];
    $allFiscalTypeRefundsTypeA = array("NotaDeCreditoA", "TiqueNotaCreditoA");
    $allFiscalTypeRefundsTypeB = array("NotaDeCreditoB", "TiqueNotaCreditoB");
    $rows = array ();
    $query = "select fiscal_reports.fiscal_day as 'date',
                   locations.title as 'location_name',
                   fiscal_reports.register as 'Register',
                   fiscal_reports.client_name as 'Client',
                   fiscal_reports.CUIT as 'CUIT',
                   fiscal_reports.report_id as 'Number of Z',
                   fiscal_reports.pv_number as 'PV_NO',
                   concat(fiscal_reports.first_invoice, '-', fiscal_reports.last_invoice ) as 'Number Of FC NC',
                   fiscal_reports.report_type as 'Type',
                   'Iva Percentage' as 'Iva Percentage',
                   fiscal_reports.net_total as 'taxable_net_sale',
                   fiscal_reports.tax as 'Taxes',
                   fiscal_reports.total as 'Total Sales',
                   fiscal_reports.refund_net as 'refund_net',
                   fiscal_reports.refund_tax as 'refund_tax',
                   fiscal_reports.refund_total as 'refund_total',
                   'actual_total' as 'actual_total'
            from `fiscal_reports`
            LEFT JOIN `config` ON fiscal_reports.register = config.value2
            LEFT JOIN `locations` ON config.location = locations.id
            where $whereClause AND ( fiscal_reports.net_total > 0 OR fiscal_reports.tax > 0 OR fiscal_reports.total > 0 OR fiscal_reports.refund_total > 0 )
            AND fiscal_reports.report_id != ''
            AND report_type IN ('TiqueFacturaA', 'TiqueFacturaB','ReciboB','TiqueNotaCreditoB','TiqueNotaCreditoA')
            ORDER BY fiscal_reports.report_id asc";
    $content_query = lavu_query ( $query );
    while ( $content_read = mysqli_fetch_assoc ( $content_query ) ) {
        $fiscal_date   = $content_read ['date'];
        $date=explode(" ", $fiscal_date);
        $report_read['date'] = substr($date[0], 4, 2)."-".substr($date[0], 2, 2)."-".substr($date[0], 0, 2);
        $report_read['location_name'] = $content_read ['location_name'];
        $report_read['Register'] = $content_read ['Register'];
        $report_read['Client'] = $content_read ['Client'];
        $report_read['CUIT'] = $content_read ['CUIT'];
        $report_read['Number of Z']    = $content_read ['Number of Z'];
        $report_read['PV_NO']         = $content_read ['PV_NO'];
        $report_read['Number Of FC NC'] = $content_read ['Number Of FC NC'];
        $report_read['Type']   = $content_read ['Type'];
        $report_read['taxable_net_sale'] = $content_read ['taxable_net_sale'];
        $report_read['Iva Percentage'] = $content_read ['Iva Percentage'];
        $report_read['Taxes']     = $content_read ['Taxes'];
        $report_read['Total Sales'] = $content_read ['Total Sales'];
        $report_read['refund_total'] = $content_read ['refund_total'];
        $report_read['refund_net'] = $content_read ['refund_net'];
        $report_read['refund_tax'] = $content_read ['refund_tax'];
        $report_read['actual_total'] = $content_read ['Total Sales'];//-$content_read ['refund_total'];

        if ( in_array( $report_read ['Type'], $allFiscalTypeRefundsTypeA ) || ( in_array( $report_read ['Type'], $allFiscalTypeRefundsTypeB ) ) ) {
            if (isset ( $report_read ['refund_total'] ) && $report_read ['refund_total'] !=0 ) {
                $totPayment = str_replace ( "[money]", "-", $report_read ['refund_total'] );
                $report_read ['actual_total'] = "-".$totPayment;
            }
        }
        
        if ( in_array($report_read ['Type'], $allFiscalTypeRefundsTypeA) || $report_read ['Type'] == "TiqueFacturaA" ) {
            $invoiceNumbers =  explode("-", $report_read['Number Of FC NC']);
            $report_read['Number Of FC NC'] = $invoiceNumbers[0];
        }
        
        $rows [] = $report_read;
    }
    for ( $i = 0; $i < count ( $rows ); $i++ ) {
        $fiscalTypeToSearch = $rows [$i] ['Type'];
        if ( in_array( $fiscalTypeToSearch, $allFiscalTypeRefundsTypeA ) || ( in_array( $fiscalTypeToSearch, $allFiscalTypeRefundsTypeB ) ) ) {
            $taxable = str_replace ( "[money]", "-", $rows [$i] ['refund_net'] );
            $rows [$i] ['taxable_net_sale'] = "-".$taxable;
            $tax = str_replace ( "[money]", "-", $rows [$i] ['refund_tax'] );
            $rows [$i] ['Taxes'] = "-".$tax;
        } else {
            $rows [$i] ['taxable_net_sale'] = $rows [$i] ['taxable_net_sale'];
            $rows [$i] ['Taxes'] = $rows [$i] ['Taxes'];
        }
    }
    $pvNumberGrouping = array ();
    $tempMaxInvoiceRange = array();
    $tempMinInvoiceRange = array();
    $tempReportIdRange = array();
    for ($i = 0; $i < count ( $rows ); $i++) {
        $newrow = array ();
        foreach ( $rows [$i] as $col => $val ) {
            if ($col == "Total Sales") {
                $newrow ['taxable_net_sale'] = $rows [$i] ['taxable_net_sale'];
                $newrow ['Taxes'] = $rows [$i] ['Taxes'];
            }
            if ($col != "total_cash" && $col != "total_card") {
                $newrow [$col] = $rows [$i] [$col];
            }
        }
        $pvNumber = (isset ( $newrow ['PV_NO'] ) && $newrow ['PV_NO'] != "") ? $newrow ['PV_NO'] : 'Not Available';
        $newrow ["PV_NO"] = $pvNumber;
        if (empty ( $newrow ["Total Sales"] ) || $newrow ["Total Sales"] == 0) {
            $newrow ["Total Sales"] = 0;
        }
        if (empty ( $newrow ["actual_total"] ) || $newrow ["actual_total"] == 0) {
            $newrow ["actual_total"] = 0;
        }
        if (empty ( $newrow ["refund_total"] ) || $newrow ["refund_total"] == 0) {
            $newrow ["refund_total"] = 0;
        }
        if (empty ( $newrow ['taxable_net_sale'] ) || $newrow ['taxable_net_sale'] == 0) {
            $newrow ['taxable_net_sale'] = 0;
        }
        if (empty ( $newrow ['Taxes'] ) || $newrow ['Taxes'] == 0) {
            $newrow ['Taxes'] = 0;
        }
        if ($newrow ['Number Of FC NC'] == "") {
            $newrow ['Number Of FC NC'] = "";//$newrow ['fiscal_txn_id'];
        }
        if ($newrow ['Iva Percentage'] == "Iva Percentage") {
            $newrow ['Iva Percentage'] = "21%";
        }
        if ($newrow ['taxable_net_sale'] == 0 && $newrow ['Taxes'] == 0) {
            $newrow ['Iva Percentage'] = '0%';
        }
        $fiscalType = trim ( $newrow ['Type'] );
        $Register = trim ( $newrow ['Register'] );
        if (in_array ( $fiscalType, array (
            "TiqueFacturaB",
            "ReciboB",
            "NotaDeCreditoB",
            "TiqueNotaCreditoB"
        ) )) {
            if (!isset($pvNumberGrouping [$pvNumber] [$fiscalType] [$Register] )) {
                $pvNumberGrouping [$pvNumber] [$fiscalType] [$Register] ['date'] 	= $newrow ['date'];
                $pvNumberGrouping [$pvNumber] [$fiscalType] [$Register] ['location_name'] 	= $newrow ['location_name'];
                $pvNumberGrouping [$pvNumber] [$fiscalType] [$Register] ['Regsiter'] 	= $newrow ['Regsiter'];
                $pvNumberGrouping [$pvNumber] [$fiscalType] [$Register] ['Client'] 	= $newrow ['Client'];
                $pvNumberGrouping [$pvNumber] [$fiscalType] [$Register] ['CUIT'] 	= $newrow ['CUIT'];
                $pvNumberGrouping [$pvNumber] [$fiscalType] [$Register] ['Number of Z'] 		= $newrow ['Number of Z'];
                $pvNumberGrouping [$pvNumber] [$fiscalType] [$Register] ['PV_NO'] 			= $newrow ['PV_NO'];
                $pvNumberGrouping [$pvNumber] [$fiscalType] [$Register] ['Number Of FC NC'] 	= $newrow ['Number Of FC NC'];
                $taxable_net_sale 	= str_replace ( '[money]', '', $newrow ['taxable_net_sale'] );
                $Taxes		= str_replace ( '[money]', '', $newrow ['Taxes'] );
                $Total_Sales 	= str_replace ( ',', '', str_replace ( '[money]', '', $newrow ['Total Sales'] ) );
                $refund_total 	= str_replace ( ',', '', str_replace ( '[money]', '', $newrow ['refund_total'] ) );
                $actual_total 	= str_replace ( ',', '', str_replace ( '[money]', '', $newrow ['actual_total'] ) );
                /*
                if ($taxable_net_sale != 0 && $Taxes != 0 && $Total_Sales != "" ) {
                    if (in_array($fiscalType, $allFiscalTypeRefundsTypeB ) ) {
                        $taxable_net_sale   = "-".$taxable_net_sale;
                        $Taxes       = "-".$Taxes;
                        $Total_Sales   = "-".$Total_Sales;
                    }
                }*/
		        $pvNumberGrouping [$pvNumber] [$fiscalType] [$Register] ['taxable_net_sale'] 	= $taxable_net_sale;
                $pvNumberGrouping [$pvNumber] [$fiscalType] [$Register] ['Taxes'] 		= $Taxes;
                $pvNumberGrouping [$pvNumber] [$fiscalType] [$Register] ['Total Sales'] 	= $Total_Sales;
                $pvNumberGrouping [$pvNumber] [$fiscalType] [$Register] ['refund_total'] 	= $refund_total;
                $pvNumberGrouping [$pvNumber] [$fiscalType] [$Register] ['actual_total'] 	= $actual_total;
            } else {
                $taxable_net_sale 	= str_replace ( '[money]', '', $newrow ['taxable_net_sale'] );
                $Taxes		= str_replace ( '[money]', '', $newrow ['Taxes'] );
                $refund_total 	= str_replace ( ',', '', str_replace ( '[money]', '', $newrow ['refund_total'] ) );
                $Total_Sales 	= str_replace ( ',', '', str_replace ( '[money]', '', $newrow ['Total Sales'] ) );
                $actual_total 	= str_replace ( ',', '', str_replace ( '[money]', '', $newrow ['actual_total'] ) );

                if (in_array($fiscalType, $allFiscalTypeRefundsTypeB ) ) {
                    $taxable_net_sale   = "-".$taxable_net_sale;
                    $Taxes       = "-".$Taxes;
                    $Total_Sales   = "-".$Total_Sales;
                }

                $pvNumberGrouping [$pvNumber] [$fiscalType] [$Register] ['taxable_net_sale'] 	= $pvNumberGrouping [$pvNumber][$fiscalType] [$Register] ['taxable_net_sale'] + $taxable_net_sale;
                $pvNumberGrouping [$pvNumber] [$fiscalType] [$Register] ['Taxes'] 		= $pvNumberGrouping [$pvNumber][$fiscalType] [$Register] ['Taxes'] + $Taxes;
                $pvNumberGrouping [$pvNumber] [$fiscalType] [$Register] ['Total Sales'] 	= $pvNumberGrouping [$pvNumber][$fiscalType] [$Register] ['Total Sales'] + $Total_Sales;
                $pvNumberGrouping [$pvNumber] [$fiscalType] [$Register] ['refund_total'] 	= $pvNumberGrouping [$pvNumber][$fiscalType] [$Register] ['refund_total'] + $refund_total;
                $pvNumberGrouping [$pvNumber] [$fiscalType] [$Register] ['actual_total'] 	= $pvNumberGrouping [$pvNumber][$fiscalType] [$Register] ['actual_total'] + $actual_total;

            }
            $invNumber=explode('-', $newrow ['Number Of FC NC']);
            $firstInvoice =$invNumber[0];
            $lastInvoice =$invNumber[1];
            if ($lastInvoice != "Unavailable" && $lastInvoice != "No Transactions" ) {
                $tempMaxInvoiceRange[$pvNumber.$fiscalType.$Register][] = $lastInvoice;
            }

            if ($firstInvoice != "Unavailable" && $firstInvoice!="No Transactions" ) {
                $tempMinInvoiceRange[$pvNumber.$fiscalType.$Register][] = $firstInvoice;
            }

            $tempReportIdRange[$pvNumber.$fiscalType.$Register][] = $newrow ['Number of Z'];

        } else {
            $rows [$i] = $newrow;
        }
    }
    $translated_phrases = array ();
    $auto_translate = array (
        "date",
        "Register",
        "Client",
        "Number of Z",
        "Number Of FC NC",
        "Type",
        "PV_NO",
        "order_id",
        "fiscal_txn_id",
        "CUIT",
        "taxable_net_sale",
        "Iva Percentage",
        "Taxes",
        "Total Sales",
        "refund_total"
    );


    for ($n = 0; $n < count ( $auto_translate ); $n++) {
        $phrase = $auto_translate [$n];
        $human_phrase = trim ( ucwords ( str_replace ( "_", " ", $phrase ) ) );
        $translated_human_phrase = speak ( $human_phrase );
        if ($translated_human_phrase != $human_phrase) {
            $translated_phrase = str_replace ( " ", "_", trim ( str_replace ( ".", "", strtolower ( $translated_human_phrase ) ) ) );
            $translated_phrases [$phrase] = $translated_phrase;
        }
    }

    for ($i = 0; $i < count ( $rows ); $i++) {
        $newrow = array ();
        foreach ( $rows [$i] as $col => $val ) {
            if (isset ( $translated_phrases [$col] ) ) {
                $newrow [$translated_phrases [$col]] = $val;
            } else {
                    $newrow [$col] = $val;
            }
        }
        $rows [$i] = $newrow;
    }
    $transalatedRegister = translatePhrase('Register');
    $transalatedClient = translatePhrase('Client');
    $transalatedFiscalType = translatePhrase('Type');
    $transalatedNumberZ = translatePhrase('Number of Z');
    $transalatedNumberInvoice = translatePhrase('Number Of FC NC');
    $transalatedIvapercentage = translatePhrase('Iva Percentage');
    $newrow2 = array ();
    if (count ( $rows ) > 0) {
        for ($i = 0; $i < count ( $rows ); $i++) {
            if (! in_array ( $rows [$i] [$transalatedFiscalType], array (
                "TiqueFacturaB",
                "ReciboB",
                "NotaDeCreditoB",
                "TiqueNotaCreditoB"
            ) )) {
                $newrow2 [$i] = $rows [$i];
            }
        }

        if (count ( $newrow ) > 1) {
            if (count ( $pvNumberGrouping ) > 0) {
                $reportType = 'FB';
                foreach ( $pvNumberGrouping as $pvNo => $reportBasedArr ) {
                    foreach ( $reportBasedArr as $fiscalReportType =>  $fiscalTypeBasedArr ) {
                        foreach ( $fiscalTypeBasedArr as $key => $pvBasedArr) {
                            $human_phrase4 = trim ( ucwords ( str_replace ( "_", " ", 'Taxes' ) ) );
                            $translated_human_phrase4 = speak ( $human_phrase4 );

                            if ($translated_human_phrase4 != $human_phrase4) {
                                $translated_phrase4 = str_replace ( " ", "_", trim ( str_replace ( ".", "", strtolower ( $translated_human_phrase4 ) ) ) );
                                $translated_phrases4 = $translated_phrase4;
                            } else {
                                $translated_phrases4 = 'Taxes';
                            }
                            switch ($fiscalReportType) {
                                case 'TiqueFacturaB':
                                    $reportType = "FB";
                                    break;
                                case "ReciboB":
                                    $reportType = "ReciboB";
                                    break;
                                case "NotaDeCreditoB":
                                case "TiqueNotaCreditoB":
                                    $reportType = "NCB";
                                    break;
                            }
                            $minInvoice = "Unavailable";
                            $maxInvoice = "Unavailable";

                            if (count($tempMinInvoiceRange[$pvNo.$fiscalReportType.$key]) > 0 ) {
                                $minInvoice = min($tempMinInvoiceRange[$pvNo.$fiscalReportType.$key]);
                            }

                            if (count($tempMaxInvoiceRange[$pvNo.$fiscalReportType.$key]) > 0 ) {
                                $maxInvoice = max($tempMaxInvoiceRange[$pvNo.$fiscalReportType.$key]);
                            }
                            
                            if($pvBasedArr ['taxable_net_sale']==0 && $pvBasedArr ['Taxes']==0){
                                $ivaPercentage = '0%';
                            }else{
                                $ivaPercentage = '21%';
                            }

                            $totalPayment = translatePhrase("Total Sales");
                            $orderTaxable = translatePhrase("taxable_net_sale");
                            $orderrefund_total = translatePhrase("refund_total");
                            $TequeFacturaBArray [] = array (
                                'date' => $pvBasedArr ['date'],
                                'location_name' => $pvBasedArr ['location_name'],
                                $transalatedRegister => $key,
                                $transalatedClient => 'Consumidor Final',
                                'CUIT' => '-NA-',
                                $transalatedNumberZ => min($tempReportIdRange[$pvNo.$fiscalReportType.$key])." - ". max($tempReportIdRange[$pvNo.$fiscalReportType.$key]),
                                'PV_NO' => $pvBasedArr ['PV_NO'],
                                $transalatedNumberInvoice => $minInvoice." - ". $maxInvoice,
                                $transalatedFiscalType => $reportType,
                                $orderTaxable => '[money]'.$pvBasedArr ['taxable_net_sale'],
                                $transalatedIvapercentage => $ivaPercentage,
                                $translated_phrases4 => '[money]'.$pvBasedArr ['Taxes'],
                                $totalPayment =>  $pvBasedArr ['Total Sales'],
                                $orderrefund_total =>  $pvBasedArr ['refund_total'],
                                'actual_total' => '[money]'.$pvBasedArr ['actual_total']
                            );
                       }
                    }
                }
            }
        }
    }

    $newrowDisplay = array ();
    $newrow1 = array_merge ( $newrow2, $TequeFacturaBArray );
    foreach ( $newrow1 as $vals ) {
        $human_phrase1 = trim ( ucwords ( str_replace ( "_", " ", "order_id" ) ) );
        $translated_human_phrase1 = speak ( $human_phrase1 );
        if ($translated_human_phrase1 != $human_phrase1) {
            $translated_phrase1 = str_replace ( " ", "_", trim ( str_replace ( ".", "", strtolower ( $translated_human_phrase1 ) ) ) );
            $translated_phrases1 = $translated_phrase1;
        }
        unset ( $vals [$translated_phrases1] );
        unset ( $vals ['order_id'] );
        unset ( $vals [$totalPayment] );
        unset ( $vals [$orderrefund_total] );
        unset ( $vals ['refund_net'] );
        unset ( $vals ['refund_tax'] );

        if ($vals [$transalatedFiscalType] == "TiqueFacturaA") {
            $vals [$transalatedFiscalType] = "FA";
            $vals [$transalatedNumberInvoice] = $vals [$transalatedNumberInvoice];
            $vals [$orderTaxable] = '[money]'.$vals [$orderTaxable];
            $vals [$translated_phrases4] = '[money]'.$vals [$translated_phrases4];
            $vals ['actual_total'] = '[money]'.$vals ['actual_total'];
        }
        if ($vals [$transalatedFiscalType] == "NotaDeCreditoA" || $vals [$transalatedFiscalType] == "TiqueNotaCreditoA") {
            $vals [$transalatedFiscalType] = "NCA";
            $vals [$transalatedNumberInvoice] = $vals [$transalatedNumberInvoice];
            $vals [$orderTaxable] = '[money]'.$vals [$orderTaxable];
            $vals [$translated_phrases4] = '[money]'.$vals [$translated_phrases4];
            $vals ['actual_total'] = '[money]'.$vals ['actual_total'];
        }
        $newrowDisplay [] = $vals;
    }

    $counter=0;
    $dataArray = array();
    foreach ($newrowDisplay as $singleRow) {
        $dataArray[0][$counter][0]=$singleRow;
        $counter++;
    }
    return $dataArray;
}
?>
