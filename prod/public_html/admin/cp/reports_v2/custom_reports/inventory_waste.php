<?php

require_once(__DIR__ .'/InventoryApiReport.php');

function inventory_waste($args)
{
	// Main report settings
	$args['report_name']     = 'inventory_waste';
	$args['report_api_path'] = 'Reports/Waste';
	$args['mode'] = $_GET['mode'];
	// Run report
	$inventoryApiReport = new InventoryApiReport();
	$reportData = $inventoryApiReport->getData($args);

	$counter=0;
	$dataArray = array();
	if(count($reportData) > 0) {
	    foreach($reportData as $singleRow) {
	        $dataArray[0][$counter][0]=$singleRow;
	        $counter++;
	    }
	}
	return $dataArray;
	
}
