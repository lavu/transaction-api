<?php
function get_deposit_detail($vars) {
    $vars['mode'] = $_GET['mode'];
	if ($vars['mode'] == '' || $vars['mode'] == 'day') {
        $startDate = explode(" ", $vars['start_datetime']);
        $endDate = explode(" ", $vars['end_datetime']);
        $deposit_start_date = $startDate[0];
        $deposit_start_time = $startDate[1];
        $deposit_end_date = $endDate[0];
        $deposit_end_time = $endDate[1];
        //Check If Time Selected & End Time Greater Than Start Time
        if ($deposit_start_time != $deposit_end_time && $deposit_end_time > $deposit_start_time) {
            $whereDateClause = "(DATE(cc.`datetime`) BETWEEN '$deposit_start_date' AND '$deposit_end_date') AND (TIME(cc.`datetime`) BETWEEN '$deposit_start_time' AND '$deposit_end_time')";
        } else {
            //If Time Not Selected
            $whereDateClause = "cc.`datetime` >='[start_datetime]' AND cc.`datetime` < '[end_datetime]'";
        }
	} else {
        $whereDateClause = "cc.`datetime` >='[start_datetime]' AND cc.`datetime` < '[end_datetime]'";
	}
    $query = "SELECT `di`.`transaction_id` AS deposit_id,
	       `cc`.`card_desc` AS 'card_last_4_digits',
	       `cc`.`server_name` AS 'server',
	       `dci`.`first_name`,
	       `dci`.`last_name`,
	       IF(COUNT(*) > 0, CONCAT('[money]', FORMAT(SUM(IF(`cc`.`transtype` = 'deposit_refund', -1*cc.amount, cc.amount)),2)), '[money]0.00') AS amount,
	       `di`.`deposit_type`,
	       MAX(`cc`.`datetime`) AS 'date'
        FROM `deposit_customer_info` AS dci
        JOIN `deposit_information` AS di ON (dci.id = di.customer_id)
        JOIN `cc_transactions` AS cc ON (cc.order_id = di.transaction_id)
		WHERE $whereDateClause GROUP BY cc.order_id ORDER BY `cc`.`datetime` DESC";
    $dataArray = array();
    $report_query = lavu_query($query, $vars);
    $counter = 0;
    if (mysqli_num_rows($report_query)>0) {
        while ($report_read = mysqli_fetch_assoc($report_query)) {
            $dataArray[0][$counter][0] = $report_read;
            $counter++;
        }
    }
    return $dataArray;
}
?>