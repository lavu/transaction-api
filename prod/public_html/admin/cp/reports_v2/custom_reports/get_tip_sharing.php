<?php
function get_tip_sharing($vars) {
	if (isset($vars['start_datetime']) && $vars['start_datetime']!="") {
		$starDate = explode(" ", $vars['start_datetime']);
		$endDate = explode(" ", $vars['end_datetime']);
		$sDate = $starDate[0];
		$eDate = $endDate[0];
	}
	$sharingType = 'ByEmployee';
	if (isset($_REQUEST['sharingType']) && $_REQUEST['sharingType']!="") {
		$sharingType = $_REQUEST['sharingType'];
	}

	$firstDate = date_create($sDate);
	$secondDate = date_create($eDate);
	$interval = date_diff($firstDate, $secondDate);
	$dayCount = $interval->format('%a');
	$sharingShowType = 'ShowTotals';
	if ($dayCount < 2) {
		$sharingShowType = 'ShowAll';
	}

	if (isset($_REQUEST['sharingShowType']) && $_REQUEST['sharingShowType']!="") {
		$sharingShowType = $_REQUEST['sharingShowType'];
	}

	$getDate = '';
	if ($sharingShowType != 'ShowTotals') {
		$getDate = " `created_date` AS 'date',";
	}

	if ($sharingType == 'EmployeeClass' && $sharingShowType != '') {
		if ($sharingShowType == 'ShowTotals' || $sharingType == 'EmployeeClass') {
			$subQuery = " GROUP BY `emp_class_id`";
		}
		if (isset($vars['start_datetime']) && $vars['start_datetime'] != "") {
			$query = "SELECT `emp_class_name` AS 'employee class', `emp_class_id` AS 'class id', ".$getDate." SUM(`hours_worked`) AS 'Hours', CONCAT('[money]', SUM(`sales`)) AS 'sales', CONCAT('[money]', SUM(`card_tip`)) AS 'card tips', CONCAT('[money]', SUM(`cash_tip`)) AS 'cash tips', CONCAT('[money]', SUM(`total_tip`)) AS 'total tips', CONCAT('[money]', SUM(`tip_out`)) AS 'tip out', CONCAT('[money]',SUM(`tip_in`)) AS 'tip in', CONCAT('[money]', SUM(`tip_due`)) AS 'tips due to employee', CONCAT('[money]', SUM(`tip_pool`)) AS 'tip pool results' FROM `tip_sharing` WHERE (( `created_date` >= '".$sDate."' AND `created_date` < '".$eDate."' AND `time_for_tip_sharing` NOT LIKE 'Weekly%' ) ||  ( `updated_date` >= '".$sDate."' AND `updated_date` < '".$eDate."' AND `time_for_tip_sharing` LIKE 'Weekly%' ) ) $subQuery";
		} else {
			$query = "SELECT `emp_class_name` AS 'employee class', `emp_class_id` AS 'class id', ".$getDate." SUM(`hours_worked`) AS 'Hours', CONCAT('[money]', SUM(`sales`)) AS 'sales', CONCAT('[money]', SUM(`card_tip`)) AS 'card tips', CONCAT('[money]', SUM(`cash_tip`)) AS 'cash tips', CONCAT('[money]', SUM(`total_tip`)) AS 'total tips', CONCAT('[money]', SUM(`tip_out`)) AS 'tip out', CONCAT('[money]', SUM(`tip_in`)) AS 'tip in', CONCAT('[money]', SUM(`tip_due`)) AS 'tips due to employee', CONCAT('[money]', SUM(`tip_pool`)) AS 'tip pool results' FROM `tip_sharing` WHERE (( DATE_FORMAT(`created_date`, '%Y-%m-%d') BETWEEN '".$sDate."' AND '".$eDate."' AND `time_for_tip_sharing` NOT LIKE 'Weekly%' ) || ( DATE_FORMAT(`updated_date`, '%Y-%m-%d') BETWEEN '".$sDate."' AND '".$eDate."' AND `time_for_tip_sharing` LIKE 'Weekly%' ) ) $subQuery";
		}
	} else {

		if ($sharingShowType == 'ShowTotals' ) {
			$subQuery = " GROUP BY `name` ";
		} else {
			$subQuery = " GROUP BY `id` ";
		}

		if (isset($vars['start_datetime']) && $vars['start_datetime'] != "") {
			$query = "SELECT `id`, `server_name` AS 'name', `emp_class_name` AS 'employee class', ".$getDate." `emp_class_id` AS 'class id', SUM(`hours_worked`) AS 'Hours', CONCAT('[money]', SUM(`sales`)) AS 'sales', CONCAT('[money]', SUM(`card_tip`)) AS 'card tips', CONCAT('[money]', SUM(`cash_tip`)) AS 'cash tips', CONCAT('[money]', SUM(`total_tip`)) AS 'total tips', CONCAT('[money]', SUM(`tip_out`)) AS 'tip out', CONCAT('[money]', SUM(`tip_in`)) AS 'tip in', CONCAT('[money]', SUM(`tip_due`)) AS 'tips due to employee', CONCAT('[money]', SUM(`tip_pool`)) AS 'tip pool results' FROM `tip_sharing` WHERE (( `created_date` >= '".$sDate."' AND `created_date` < '".$eDate."' AND `time_for_tip_sharing` NOT LIKE 'Weekly%' ) ||  ( `updated_date` >= '".$sDate."' AND `updated_date` < '".$eDate."' AND `time_for_tip_sharing` LIKE 'Weekly%' ) ) $subQuery";
		} else {
			$query = "SELECT `id`, `server_name` as 'name', `emp_class_name` AS 'employee class', ".$getDate." `emp_class_id` AS 'class id', SUM(`hours_worked`) AS 'Hours', CONCAT('[money]', SUM(`sales`)) AS 'sales', CONCAT('[money]', SUM(`card_tip`)) AS 'card tips', CONCAT('[money]', SUM(`cash_tip` )) AS 'cash tips', CONCAT('[money]',SUM(`total_tip`)) AS 'total tips', CONCAT('[money]', SUM(`tip_out`)) AS 'tip out', CONCAT('[money]', SUM(`tip_in`)) AS 'tip in', CONCAT('[money]', SUM(`tip_due`)) AS 'tips due to employee', CONCAT('[money]', SUM(`tip_pool`)) AS 'tip pool results' FROM `tip_sharing` WHERE (( DATE_FORMAT(`created_date`, '%Y-%m-%d') BETWEEN '".$sDate."' AND '".$eDate."' AND `time_for_tip_sharing` NOT LIKE 'Weekly%' ) || ( DATE_FORMAT(`updated_date`, '%Y-%m-%d') BETWEEN '".$sDate."' AND '".$eDate."' AND `time_for_tip_sharing` LIKE 'Weekly%' ) ) $subQuery";
		}
	}

    $dataArray = array();
    $report_query = lavu_query($query,$vars);
    $counter = 0;
    if (mysqli_num_rows($report_query) > 0 ) {
        //In Tipsharing report, Hours colums need to show in Hours as Decimal format.LP-10486.
        $decimalPlaces = (isset(sessvar('location_info')['disable_decimal'])) ? sessvar('location_info')['disable_decimal'] : 2;
        $decimalChar = (isset(sessvar('location_info')['decimal_char']) && sessvar('location_info')['decimal_char'] != '') ? sessvar('location_info')['decimal_char'] : '.';
        $thousandSep = ($decimalChar == '.') ? ',' : '.';

        while ($report_read = mysqli_fetch_assoc($report_query)) {
            if (isset($report_read['id'])) {
            	unset($report_read['id']);
            }

            $saleVal = substr($report_read['sales'],7);
            $tipoutlVal = substr($report_read['tip out'],7);
            $tipinVal = substr($report_read['tip in'],7);
            $report_read['Hours'] = number_format($report_read['Hours'], $decimalPlaces, $decimalChar, $thousandSep);
            $dataArray[0][$counter][0] = $report_read;
            $counter++;
        }
    }
    return $dataArray;
}
?>