<?php
	// currency_type = '0' Amount
	// currency_type = '1' Points

	define( 'REPORTS_V2_SELECTION_COLUMN_KEY', 'selection_column' );
	define( 'REPORTS_V2_SPECIAL_CODE_KEY', 'special_code' );
	define( 'REPORTS_V2_SELECTION_KEY', 'selection' );
	define( 'REPORTS_V2_TABLE_KEY', 'table' );
	define( 'REPORTS_V2_TITLE_KEY', 'title' );
	define( 'REPORTS_V2_NAME_KEY', 'name' );
	define( 'REPORTS_V2_ORDERBY_KEY', 'orderby' );
	define( 'REPORTS_V2_SELECT_KEY', 'select' );
	define( 'REPORTS_V2_GROUPBY_KEY', 'groupby' );
	define( 'REPORTS_V2_WHERE_KEY', 'where' );
	define( 'REPORTS_V2_JOIN_KEY', 'join' );
	define( 'REPORTS_V2_ID_KEY', 'id' );
	define( 'REPORTS_V2_ORDER_KEY', '_order' );
	define( 'REPORTS_V2_DELETED_KEY', '_deleted' );
	define( 'REPORTS_V2_FIELD_SEPERATOR', '[_sep_]' );

	require_once dirname(dirname(__FILE__)).'/black_hole_report_drawer.php';
	require_once dirname(__FILE__).'/eod_report_drawer.php';
	require_once dirname(__FILE__).'/exporter_eod_report_drawer.php';

	function append_if_not_zero( &$arr, $item, $negate=false ){
		if( report_money_to_number($item['sum']) != 0 ){
			if( $negate ){
				$item['sum'] = report_float_to_money( -sprintf('%0.4f', report_money_to_number($item['sum']) ));
			}
			$arr[] = $item;
		}
	}

	function last_item_compare( $item1, $item2 ){
		$keys = array_keys( $item1 );
		$key = $keys[count($keys)-1];

		$value1 = $item1[$key];
		$value2 = $item2[$key];
		if( report_value_is_money( $value1 ) ){
			$value1 = report_money_to_number( $value1 );
			$value2 = report_money_to_number( $value2 );
		}

		if( $value1 < $value2 ){
			return 1;
		} else if ( $value1 > $value2 ){
			return -1;
		} else {
			return 0;
		}
	}

	function sum_last_item( $section_title, $title, $arr ){
		$result = array(
			$section_title => '',
			'summary' => show_as_title( $title ),
			'sum' => report_float_to_money('0')
		);
		foreach( $arr as $arr_index => $row ){
		    foreach( $row as $arr_index => $row ){
		        $it = 0;
		        foreach( $row as $key => $value ){
		            if( $it++ == 2 ){
						$result['sum'] = sprintf('%0.4f', report_money_to_number( $value ) + report_money_to_number( $result['sum'] ));
						$result['sum'] = report_float_to_money($result['sum']);
		            }
		        }
		    }
		}
		return $result;
	}

	function sum_all_items( $arr ){
		$result = array();
		foreach( $arr as $arr_index => $row ){
			foreach( $row as $rowValue ){
			    foreach( $rowValue as $key => $value ){
					if( ! isset( $result[$key] ) ){
						$result[$key] = 0;
					}
					if( report_value_is_money( $value ) ){
						$total = sprintf('%0.4f', report_money_to_number( $value ) + report_money_to_number( $result[$key] ));
						$result[$key] = report_float_to_money($total);
					} else {
						$result[$key] += report_number_to_float($value);
					}
			    }
			}
		}
		return $result;
	}

	class end_of_day {

		private function runReport( $report_details ){
			$ar = new area_reports( array('display_query' => 1) );
			$ar->report_read = $report_details;
			$ar->report_id = $ar->report_read['id'];
			$ar->report_name = $ar->report_read['name'];
			$ar->report_title = $ar->report_read['title'];
			$ar->report_selection = $ar->report_read['selection'];
			$ar->report_selection_column = $ar->report_read['selection_column'];

			$ar->report_drawer = new black_hole_report_drawer();
			$ar->show_query_table();
			return $ar->report_results[0];
		}

		private function combineResults( &$result1, $result2, $num_eles ){
			foreach( $result1 as $result1_key => $result1_value ){
				$keys = array_keys( $result1_value );
				foreach( $result2 as $result2_key => &$result2_value ){
					$match = true;
					for( $i = 0; $i < min( count($keys), $num_eles ); $i++ ){
						$val1 = trim($result1_value[$keys[$i]]);
						$val2 = trim($result2_value[$keys[$i]]);
						if( report_value_is_number($val1) ){
							$match = $match && (report_number_to_float($val1) == report_number_to_float($val2));  // LP-1590 -- Fixed typo
						} else {
							$match = $match && ($val1 == $val2);
						}
					}

					if( $match ){
						for( $i = min( count($keys), $num_eles ); $i < count( $keys ); $i++ ){
							if( report_value_is_money( $result1_value[$keys[$i]] ) ) {
								$result1[$result1_key][$keys[$i]] = report_float_to_money(report_money_to_number($result1_value[$keys[$i]]) + report_money_to_number($result2_value[$keys[$i]]));
							} else if( report_value_is_number( $result1_value[$keys[$i]] ) ) {
								$result1[$result1_key][$keys[$i]] = report_float_to_number(report_number_to_float($result1[$result1_key][$keys[$i]]) + report_number_to_float($result2_value[$keys[$i]]));
							}
						}
						unset( $result2[$result2_key] );
					}
				}
			}

			foreach( $result2 as $key => $value ){
				$result1[] = $value;
			}
		}
		
		private function fill_in_payment_arrays_using_payment_details( $payments_from_payments, &$sale_payments, &$refunds, &$gift_cards_balance_change, &$voided_sale_payments, &$voided_refunds_payments, &$voided_gift_cards_balance_change, $payment_title = 'deposit', $refund_title='refunded_deposit', $giftcard_title='_as_deposit' ) {
		    foreach( $payments_from_payments as $paymentsFromPaymentsArray ){
		        foreach( $paymentsFromPaymentsArray as $value ){  //payment data
        				if( $value['action'] == 'Void' ){
        				 	continue;
        				}
        				if( $value['action'] == 'Sale' ){
        					if( report_number_to_float($value['voided']) == 1 ){
        						$voided_sale_payments[][] = array(
        								'voided_'.$payment_title => $value['pay_type'],
        								'count' => $value['count'],
        								'amount' => $value['total_collected']
        
        							);
        					} else {
        						$sale_payments[][] = array(
        								$payment_title => $value['pay_type'],
        								'count' => $value['count'],
        								'amount' => $value['total_collected']
        
        							);
        					}
        				} else if( $value['action'] == 'Refund' ) {
        					if( report_number_to_float($value['voided']) == 1 ){
        						$voided_refunds_payments[][] = array(
        								'voided_'.$refund_title => $value['pay_type'],
        								'count' => $value['count'],
        								'amount' => $value['total_collected']
        
        							);
        					} else {
        						$refunds[][] = array(
        								$refund_title => $value['pay_type'],
        								'count' => $value['count'],
        								'amount' => $value['total_collected']
        
        							);
        					}
        				} else { //Gift Card?
        					if( !in_array($value['action'], array( 'ReloadGiftCard', 'IssueGiftCard' ) ) ){
        						continue;
        					}
        
        					if( report_number_to_float($value['voided']) == 1 ){
        						switch( $value['action'] ){
        							case 'ReloadGiftCard':
        								$voided_gift_cards_balance_change[][] = array(
        									'voided_' . $value['pay_type'] . $giftcard_title => 'Reload Card',
        									'count' => $value['count'],
        									'amount' => $value['total_collected']
        								);
        								break;
        							case 'IssueGiftCard':
        								$voided_gift_cards_balance_change[][] = array(
        									'voided_' . $value['pay_type'] . $giftcard_title => 'Issued Card',
        									'count' => $value['count'],
        									'amount' => $value['total_collected']
        								);
        								break;
        							default:
        								break;
        						}
        					} else {
        						switch( $value['action'] ){
        							case 'ReloadGiftCard':
        								$gift_cards_balance_change[][] = array(
        									$value['pay_type'] . $giftcard_title=> 'Reload Card',
        									'count' => $value['count'],
        									'amount' => $value['total_collected']
        								);
        								break;
        							case 'IssueGiftCard':
        								$gift_cards_balance_change[][] = array(
        									$value['pay_type'] . $giftcard_title => 'Issued Card',
        									'count' => $value['count'],
        									'amount' => $value['total_collected']
        								);
        								break;
        							default:
        								break;
        						}
        					}
        				}
			    }
			}
		}

		public function run( $area_reports ){
			global $data_name;
			global $location_info;
			global $modules;

			switch( $area_reports->output_mode ){
				case 'csv':
					$area_reports->report_drawer = new exporter_eod_report_drawer( $area_reports );
					break;
				default:
					$area_reports->report_drawer = new eod_report_drawer( $area_reports );
					break;
			}

			$order_info = $this->runReport( array(
				REPORTS_V2_SELECTION_COLUMN_KEY => '',
				REPORTS_V2_SPECIAL_CODE_KEY => '',
				REPORTS_V2_SELECTION_KEY => 'Preset Date Ranges',
				REPORTS_V2_TABLE_KEY => 'orders',
				REPORTS_V2_TITLE_KEY => 'Order Information',
				REPORTS_V2_NAME_KEY => 'order_info',
				REPORTS_V2_ORDERBY_KEY => 'gross desc',
				REPORTS_V2_SELECT_KEY => implode( REPORTS_V2_FIELD_SEPERATOR,
											array(
												'#count',
												'sum(guests) as guests',
												'sum(orders.no_of_checks) as num_checks',
												'sum_price(orders.gratuity) as gratuity'
											)),	
				REPORTS_V2_GROUPBY_KEY => '',
				REPORTS_V2_WHERE_KEY => implode( REPORTS_V2_FIELD_SEPERATOR,
											array(
												'#orders_placed_active',
												"orders.location_id = '[locationid]'",
												"orders.order_id NOT LIKE 'Paid%'"
											)),
				REPORTS_V2_JOIN_KEY => '',
				REPORTS_V2_ID_KEY => 'x',
				REPORTS_V2_ORDER_KEY => '',
				REPORTS_V2_DELETED_KEY => '',
			));

			$check_info = $this->runReport( array(
				REPORTS_V2_SELECTION_COLUMN_KEY => '',
				REPORTS_V2_SPECIAL_CODE_KEY => '',
				REPORTS_V2_SELECTION_KEY => 'Preset Date Ranges',
				REPORTS_V2_TABLE_KEY => 'orders',
				REPORTS_V2_TITLE_KEY => 'Order Information',
				REPORTS_V2_NAME_KEY => 'split_check_info',
				REPORTS_V2_ORDERBY_KEY => 'gratuity',
				REPORTS_V2_SELECT_KEY => implode( REPORTS_V2_FIELD_SEPERATOR,
											array(
												'#count',
												'sum_price(split_check_details.gratuity) as gratuity',
												'sum_price(split_check_details.rounding_amount) as rounding_amount'
											)),
				REPORTS_V2_GROUPBY_KEY => '',
				REPORTS_V2_WHERE_KEY => implode( REPORTS_V2_FIELD_SEPERATOR,
											array(
												'#orders_placed_active',
												"orders.location_id = '[locationid]'",
												"orders.order_id NOT LIKE 'Paid%'"
											)),
				REPORTS_V2_JOIN_KEY => '',
				REPORTS_V2_ID_KEY => 'x',
				REPORTS_V2_ORDER_KEY => '',
				REPORTS_V2_DELETED_KEY => '',
			));

			$togo_info = $this->runReport( array(
					REPORTS_V2_SELECTION_COLUMN_KEY => '',
					REPORTS_V2_SPECIAL_CODE_KEY => '',
					REPORTS_V2_SELECTION_KEY => 'Preset Date Ranges',
					REPORTS_V2_TABLE_KEY => 'orders',
					REPORTS_V2_TITLE_KEY => 'Order Information',
					REPORTS_V2_NAME_KEY => '',
					REPORTS_V2_ORDERBY_KEY => 'togo_delivery_fees',
					REPORTS_V2_SELECT_KEY => implode( REPORTS_V2_FIELD_SEPERATOR,
							array(
									'#count',
									'sum_price(orders.togo_delivery_fees) as togo_delivery_fees'
							)),
					REPORTS_V2_GROUPBY_KEY => '',
					REPORTS_V2_WHERE_KEY => implode( REPORTS_V2_FIELD_SEPERATOR,
							array(
									'#orders_placed_active',
									"orders.location_id = '[locationid]'",
									"orders.tablename LIKE 'Lavu To Go%'"
							)),
					REPORTS_V2_JOIN_KEY => '',
					REPORTS_V2_ID_KEY => 'x',
					REPORTS_V2_ORDER_KEY => '',
					REPORTS_V2_DELETED_KEY => '',
			));

			$gratuity = array();
			$total_rounding_for_checks = 0;
			if( isset($check_info[0]) && isset($check_info[0][0]['gratuity']) ) {
				$amount = report_money_to_number( $check_info[0][0]['gratuity'] );
				$total_rounding_for_checks += report_money_to_number($check_info[0][0]['rounding_amount']);
				if( $amount * 1 != 0 ){
					$gratuity[][] = array(
						'charge' => show_as_title( isset($location_info['gratuity_label'])?$location_info['gratuity_label']:'gratuity' ),
						' ' => '',
						'amount' => $check_info[0][0]['gratuity']
					);
				}
			}

			if( isset($togo_info[0][0]['togo_delivery_fees']) && $togo_info[0][0]['togo_delivery_fees'] != '') {
				$deliveryFee = report_money_to_number( $togo_info[0][0]['togo_delivery_fees'] );
				if( $deliveryFee * 1 != 0 ) {
					$gratuity[][] = array(
							'charge' => show_as_title('Delivery Fees'),
							' ' => '',
							'amount' => $togo_info[0][0]['togo_delivery_fees']
					);
				}
			}

			$rounding_section = array();
			if( abs($total_rounding_for_checks) != 0.0 ) {
				$rounding_section[][] = array(
					'check_rounding' => '',
					'' => '',
					'amount' => $total_rounding_for_checks );
			}
			$super_groups = $this->runReport( array(
				REPORTS_V2_SELECTION_COLUMN_KEY => '',
				REPORTS_V2_SPECIAL_CODE_KEY => '',
				REPORTS_V2_SELECTION_KEY => 'Preset Date Ranges',
				REPORTS_V2_TABLE_KEY => 'orders',
				REPORTS_V2_TITLE_KEY => 'Super Groups',
				REPORTS_V2_NAME_KEY => 'super_groups',
				REPORTS_V2_ORDERBY_KEY => 'gross desc',
				REPORTS_V2_SELECT_KEY => implode( REPORTS_V2_FIELD_SEPERATOR,
											array(
												'#super_group',
												'#sum_contents_quantity',
												'#gross_contents'
											)),
				REPORTS_V2_GROUPBY_KEY => '#super_group',
				REPORTS_V2_WHERE_KEY => implode( REPORTS_V2_FIELD_SEPERATOR,
											array(
												'#orders_placed_active',
												'#exclude_sendpoints',
												"order_contents.loc_id = '[locationid]'"
											)),
				REPORTS_V2_JOIN_KEY => '',
				REPORTS_V2_ID_KEY => 'x',
				REPORTS_V2_ORDER_KEY => '',
				REPORTS_V2_DELETED_KEY => '',
			));

			$categories = $this->runReport( array(
				REPORTS_V2_SELECTION_COLUMN_KEY => '',
				REPORTS_V2_SPECIAL_CODE_KEY => '',
				REPORTS_V2_SELECTION_KEY => 'Preset Date Ranges',
				REPORTS_V2_TABLE_KEY => 'orders',
				REPORTS_V2_TITLE_KEY => 'Categories',
				REPORTS_V2_NAME_KEY => 'categories',
				REPORTS_V2_ORDERBY_KEY => 'gross desc',
				REPORTS_V2_SELECT_KEY => implode( REPORTS_V2_FIELD_SEPERATOR,
											array(
												'#menu_category_name',
												'#sum_contents_quantity',
												'#gross_contents'
											)),
				REPORTS_V2_GROUPBY_KEY => '#menu_category_name',
				REPORTS_V2_WHERE_KEY => implode( REPORTS_V2_FIELD_SEPERATOR,
											array(
												'#orders_placed_active',
												'#exclude_sendpoints',
												"orders.currency_type != '1'",
												"order_contents.loc_id = '[locationid]'"
											)),
				REPORTS_V2_JOIN_KEY => '',
				REPORTS_V2_ID_KEY => 'x',
				REPORTS_V2_ORDER_KEY => '',
				REPORTS_V2_DELETED_KEY => '',
			));
			if($location_info['gift_revenue_tracked'] == 'when_redeemed'){
			$redeemption_after_payments = $this->runReport( array(
				REPORTS_V2_SELECTION_COLUMN_KEY => '',
				REPORTS_V2_SPECIAL_CODE_KEY => '',
				REPORTS_V2_SELECTION_KEY => 'Preset Date Ranges',
				REPORTS_V2_TABLE_KEY => 'orders',
				REPORTS_V2_TITLE_KEY => 'Orders table',
				REPORTS_V2_NAME_KEY => 'orders_table',
				REPORTS_V2_ORDERBY_KEY => '',
				REPORTS_V2_SELECT_KEY => implode( REPORTS_V2_FIELD_SEPERATOR,
											array(
												"'' as paid_orders_with_gift_cards",
												'orders.order_id',
												"CONCAT('[money]', if(orders.alt_refunded !=0, orders.alt_refunded + orders.alt_paid, orders.alt_paid)) as amount"
											)),
				REPORTS_V2_GROUPBY_KEY => '',
				REPORTS_V2_WHERE_KEY => implode( REPORTS_V2_FIELD_SEPERATOR,
											array(
												'#orders_placed_active',
												"orders.location_id = '[locationid]'",
												"orders.order_id IN (select cc.order_id from cc_transactions cc WHERE cc.transtype IN ('GiftCardSale', 'GiftCardReturn') AND cc.action IN ('Sale', 'Refund') and cc.voided = '0')"
											)),
				REPORTS_V2_JOIN_KEY => '',
				REPORTS_V2_ID_KEY => 'x',
				REPORTS_V2_ORDER_KEY => '',
				REPORTS_V2_DELETED_KEY => '',
			));
			}
			$idiscounts = $this->runReport( array(
				REPORTS_V2_SELECTION_COLUMN_KEY => '',
				REPORTS_V2_SPECIAL_CODE_KEY => '',
				REPORTS_V2_SELECTION_KEY => 'Preset Date Ranges',
				REPORTS_V2_TABLE_KEY => 'orders',
				REPORTS_V2_TITLE_KEY => 'Item Discounts',
				REPORTS_V2_NAME_KEY => 'item_discounts',
				REPORTS_V2_ORDERBY_KEY => 'discount desc',
				REPORTS_V2_SELECT_KEY => implode( REPORTS_V2_FIELD_SEPERATOR,
											array(
												'#idiscount_sh',
												'count(*) as count',
												'#idiscount_amount'
											)),
				REPORTS_V2_GROUPBY_KEY => '#idiscount_sh',
				REPORTS_V2_WHERE_KEY => implode( REPORTS_V2_FIELD_SEPERATOR,
											array(
												'#orders_placed_active',
												'order_contents.idiscount_amount*1 != 0',
												"order_contents.loc_id = '[locationid]'",
												"order_contents.quantity > '0'"
											)),
				REPORTS_V2_JOIN_KEY => '',
				REPORTS_V2_ID_KEY => 'x',
				REPORTS_V2_ORDER_KEY => '',
				REPORTS_V2_DELETED_KEY => '',
			));

			$discounts_info = $this->runReport( array(
				REPORTS_V2_SELECTION_COLUMN_KEY => '',
				REPORTS_V2_SPECIAL_CODE_KEY => '',
				REPORTS_V2_SELECTION_KEY => 'Preset Date Ranges',
				REPORTS_V2_TABLE_KEY => 'orders',
				REPORTS_V2_TITLE_KEY => 'Check Discounts',
				REPORTS_V2_NAME_KEY => 'check_discounts',
				REPORTS_V2_ORDERBY_KEY => 'discount desc',
				REPORTS_V2_SELECT_KEY => implode( REPORTS_V2_FIELD_SEPERATOR,
											array(
												'#discount_sh',
												'count(*) as count',
												'#discount_amount'
											)),
				REPORTS_V2_GROUPBY_KEY => '#discount_sh',
				REPORTS_V2_WHERE_KEY => implode( REPORTS_V2_FIELD_SEPERATOR,
											array(
												'#orders_placed_active',
												'split_check_details.discount*1 != 0',
												"split_check_details.loc_id = '[locationid]'"
											)),
				REPORTS_V2_JOIN_KEY => '',
				REPORTS_V2_ID_KEY => 'x',
				REPORTS_V2_ORDER_KEY => '',
				REPORTS_V2_DELETED_KEY => '',
			));
			
			$temp_discounts_info = array();
			foreach($discounts_info as $tempArray){
			    foreach( $tempArray as $value){
			        $temp_discounts_info[]=$value;
			    }
			}
			$discounts_info = $temp_discounts_info;
			
			$temp_idiscounts = array();
			foreach($idiscounts as $tempArray){
			    foreach( $tempArray as $value){
			        $temp_idiscounts[]=$value;
			    }
			}
			$idiscounts = $temp_idiscounts;
			
			$this->combineResults( $discounts_info, $idiscounts, 1 );
			$discounts_info = array_values( $discounts_info );

			$discounts = array();
			foreach( $discounts_info as $value ){
    				if( report_value_is_money( $value['discount'] ) && report_money_to_number( $value['discount'] ) * 1 == 0 ){
    					continue;
    				}
    				$discounts[][] = array( 'discount' => $value['discount_name'], 'count' => $value['count'], 'amount' => $value['discount'] );
			}

			uasort( $discounts, 'last_item_compare' );
			$discounts = array_values( $discounts );

			$taxes1 = $this->runReport( array(
				REPORTS_V2_SELECTION_COLUMN_KEY => '',
				REPORTS_V2_SPECIAL_CODE_KEY => '',
				REPORTS_V2_SELECTION_KEY => 'Preset Date Ranges',
				REPORTS_V2_TABLE_KEY => 'orders',
				REPORTS_V2_TITLE_KEY => 'Tax Profiles',
				REPORTS_V2_NAME_KEY => 'tax_profiles',
				REPORTS_V2_ORDERBY_KEY => 'tax desc',
				REPORTS_V2_SELECT_KEY => implode( REPORTS_V2_FIELD_SEPERATOR,
											array(
												'order_contents.tax_inclusion',
												'order_contents.tax_name1 as name',
												'order_contents.tax_rate1*1 as tax_rate',
												'order_contents.tax_exempt',
												"if( order_contents.tax_exempt = '1', order_contents.exemption_name, '') as exemption_name",
												'count(*) as count',
												'sum_price(order_contents.tax1) as tax',
												'sum_price(order_contents.tax_subtotal1) as tax_subtotal'
											)),
				REPORTS_V2_GROUPBY_KEY => "order_contents.tax_inclusion,order_contents.tax_name1,order_contents.tax_rate1*1,order_contents.tax_exempt,if( order_contents.tax_exempt = '1', order_contents.exemption_name, '')",
				REPORTS_V2_WHERE_KEY => implode( REPORTS_V2_FIELD_SEPERATOR,
											array(
												'#orders_placed_active',
												"order_contents.loc_id = '[locationid]'",
												"order_contents.quantity > '0'"
											)),
				REPORTS_V2_JOIN_KEY => '',
				REPORTS_V2_ID_KEY => 'x',
				REPORTS_V2_ORDER_KEY => '',
				REPORTS_V2_DELETED_KEY => '',
			));

			$taxes2 = $this->runReport( array(
				REPORTS_V2_SELECTION_COLUMN_KEY => '',
				REPORTS_V2_SPECIAL_CODE_KEY => '',
				REPORTS_V2_SELECTION_KEY => 'Preset Date Ranges',
				REPORTS_V2_TABLE_KEY => 'orders',
				REPORTS_V2_TITLE_KEY => 'Tax Profiles',
				REPORTS_V2_NAME_KEY => 'tax_profiles',
				REPORTS_V2_ORDERBY_KEY => 'tax desc',
				REPORTS_V2_SELECT_KEY => implode( REPORTS_V2_FIELD_SEPERATOR,
											array(
												'order_contents.tax_inclusion',
												'order_contents.tax_name2 as name',
												'order_contents.tax_rate2*1 as tax_rate',
												'order_contents.tax_exempt',
												"if( order_contents.tax_exempt = '1', order_contents.exemption_name, '') as exemption_name",
												'count(*) as count',
												'sum_price(order_contents.tax2) as tax',
												'sum_price(order_contents.tax_subtotal2) as tax_subtotal'
											)),
				REPORTS_V2_GROUPBY_KEY => "order_contents.tax_inclusion,order_contents.tax_name2,order_contents.tax_rate2*1,order_contents.tax_exempt,if( order_contents.tax_exempt = '1', order_contents.exemption_name, '')",
				REPORTS_V2_WHERE_KEY => implode( REPORTS_V2_FIELD_SEPERATOR,
											array(
												'#orders_placed_active',
												"order_contents.loc_id = '[locationid]'",
												"order_contents.quantity > '0'"
											)),
				REPORTS_V2_JOIN_KEY => '',
				REPORTS_V2_ID_KEY => 'x',
				REPORTS_V2_ORDER_KEY => '',
				REPORTS_V2_DELETED_KEY => '',
			));

			$taxes3 = $this->runReport( array(
				REPORTS_V2_SELECTION_COLUMN_KEY => '',
				REPORTS_V2_SPECIAL_CODE_KEY => '',
				REPORTS_V2_SELECTION_KEY => 'Preset Date Ranges',
				REPORTS_V2_TABLE_KEY => 'orders',
				REPORTS_V2_TITLE_KEY => 'Tax Profiles',
				REPORTS_V2_NAME_KEY => 'tax_profiles',
				REPORTS_V2_ORDERBY_KEY => 'tax desc',
				REPORTS_V2_SELECT_KEY => implode( REPORTS_V2_FIELD_SEPERATOR,
											array(
												'order_contents.tax_inclusion',
												'order_contents.tax_name3 as name',
												'order_contents.tax_rate3*1 as tax_rate',
												'order_contents.tax_exempt',
												"if( order_contents.tax_exempt = '1', order_contents.exemption_name, '') as exemption_name",
												'count(*) as count',
												'sum_price(order_contents.tax3) as tax',
												'sum_price(order_contents.tax_subtotal3) as tax_subtotal'
											)),
				REPORTS_V2_GROUPBY_KEY => "order_contents.tax_inclusion,order_contents.tax_name3,order_contents.tax_rate3*1,order_contents.tax_exempt,if( order_contents.tax_exempt = '1', order_contents.exemption_name, '')",
				REPORTS_V2_WHERE_KEY => implode( REPORTS_V2_FIELD_SEPERATOR,
											array(
												'#orders_placed_active',
												"order_contents.loc_id = '[locationid]'",
												"order_contents.quantity > '0'"
											)),
				REPORTS_V2_JOIN_KEY => '',
				REPORTS_V2_ID_KEY => 'x',
				REPORTS_V2_ORDER_KEY => '',
				REPORTS_V2_DELETED_KEY => '',
			));

			$taxes4 = $this->runReport( array(
				REPORTS_V2_SELECTION_COLUMN_KEY => '',
				REPORTS_V2_SPECIAL_CODE_KEY => '',
				REPORTS_V2_SELECTION_KEY => 'Preset Date Ranges',
				REPORTS_V2_TABLE_KEY => 'orders',
				REPORTS_V2_TITLE_KEY => 'Tax Profiles',
				REPORTS_V2_NAME_KEY => 'tax_profiles',
				REPORTS_V2_ORDERBY_KEY => 'tax desc',
				REPORTS_V2_SELECT_KEY => implode( REPORTS_V2_FIELD_SEPERATOR,
											array(
												'order_contents.tax_inclusion',
												'order_contents.tax_name4 as name',
												'order_contents.tax_rate4*1 as tax_rate',
												'order_contents.tax_exempt',
												"if( order_contents.tax_exempt = '1', order_contents.exemption_name, '') as exemption_name",
												'count(*) as count',
												'sum_price(order_contents.tax4) as tax',
												'sum_price(order_contents.tax_subtotal4) as tax_subtotal'
											)),
				REPORTS_V2_GROUPBY_KEY => "order_contents.tax_inclusion,order_contents.tax_name4,order_contents.tax_rate4*1,order_contents.tax_exempt,if( order_contents.tax_exempt = '1', order_contents.exemption_name, '')",
				REPORTS_V2_WHERE_KEY => implode( REPORTS_V2_FIELD_SEPERATOR,
											array(
												'#orders_placed_active',
												"order_contents.loc_id = '[locationid]'",
												"order_contents.quantity > '0'"
											)),
				REPORTS_V2_JOIN_KEY => '',
				REPORTS_V2_ID_KEY => 'x',
				REPORTS_V2_ORDER_KEY => '',
				REPORTS_V2_DELETED_KEY => '',
			));

			$taxes5 = $this->runReport( array(
				REPORTS_V2_SELECTION_COLUMN_KEY => '',
				REPORTS_V2_SPECIAL_CODE_KEY => '',
				REPORTS_V2_SELECTION_KEY => 'Preset Date Ranges',
				REPORTS_V2_TABLE_KEY => 'orders',
				REPORTS_V2_TITLE_KEY => 'Tax Profiles',
				REPORTS_V2_NAME_KEY => 'tax_profiles',
				REPORTS_V2_ORDERBY_KEY => 'tax desc',
				REPORTS_V2_SELECT_KEY => implode( REPORTS_V2_FIELD_SEPERATOR,
											array(
												'order_contents.tax_inclusion',
												'order_contents.tax_name5 as name',
												'order_contents.tax_rate5*1 as tax_rate',
												'order_contents.tax_exempt',
												"if( order_contents.tax_exempt = '1', order_contents.exemption_name, '') as exemption_name",
												'count(*) as count',
												'sum_price(order_contents.tax5) as tax',
												'sum_price(order_contents.tax_subtotal5) as tax_subtotal'
											)),
				REPORTS_V2_GROUPBY_KEY => "order_contents.tax_inclusion,order_contents.tax_name5,order_contents.tax_rate5*1,order_contents.tax_exempt,if( order_contents.tax_exempt = '1', order_contents.exemption_name, '')",
				REPORTS_V2_WHERE_KEY => implode( REPORTS_V2_FIELD_SEPERATOR,
											array(
												'#orders_placed_active',
												"order_contents.loc_id = '[locationid]'",
												"order_contents.quantity > '0'"
											)),
				REPORTS_V2_JOIN_KEY => '',
				REPORTS_V2_ID_KEY => 'x',
				REPORTS_V2_ORDER_KEY => '',
				REPORTS_V2_DELETED_KEY => '',
			));

			$taxes = array();
			foreach( $taxes1 as $taxes1Array ){
			    foreach( $taxes1Array as $value ){
        				$this->combineResults( $taxes, array($value), 5 );
        			}
        		}
			
        		foreach( $taxes2 as $taxes2Array ){
        		    foreach( $taxes2Array as $value ){
				    $this->combineResults( $taxes, array($value), 5 );
        		    }
        		}
        		
        		foreach( $taxes3 as $taxes3Array ){
        			foreach( $taxes3Array as $value ){
        				$this->combineResults( $taxes, array($value), 5 );
        			}
        		}
        		
        		foreach( $taxes4 as $taxes4Array ){
        		    foreach( $taxes4Array as $value ){
        		        $this->combineResults( $taxes, array($value), 5 );
        		    }
        		}
        		
        		foreach( $taxes5 as $taxes5Array ){
        		    foreach( $taxes5Array as $value ){
        		        $this->combineResults( $taxes, array($value), 5 );
        		    }
        		}
        		
			uasort( $taxes, 'last_item_compare' );
			$taxes = array_values( $taxes );

			// echo '<pre style="text-align: left;">' . print_r( $taxes, true ) . '</pre>';

			$included_taxes = array();
			$non_included_taxes = array();
			$exemptions = array();
			$included_taxes_exemptions = array();
			foreach( $taxes as $key => $value ){
    				if( report_number_to_float($value['tax_rate']) == 0 ) {
    					continue;
    				}
    				if( report_number_to_float($value['tax_exempt']) === (float)1 ){
    					$exemptions[][] = array(
    						'exemption' => $value['exemption_name'],
    						'included_tax' => report_number_to_float($value['tax_inclusion']) == 1 ? 'Yes' : 'No',
    						'amount' => $value['tax']
    					);
    					if (report_number_to_float($value['tax_inclusion']) == 1) {
    						$included_taxes_exemptions[][] = array(
    								'exemption' => $value['exemption_name'],
    								'included_tax' => 'Yes',
    								'amount' => $value['tax']
    						);
    					}
    				} 
    				else {
    					if ( report_number_to_float($value['tax_inclusion']) == 1 ) {
    						$included_taxes[][] = array(
    							'included_tax' => $value['name'] . ' ('. (report_number_to_float($value['tax_rate'])*100 ).'%)',
    							'count' => $value['count'],
    							'amount' => $value['tax']
    						);
    					} else if ( report_number_to_float($value['tax_inclusion']) == 0 ) {
    						$non_included_taxes[][] = array(
    								'tax' => $value['name'] . ' ('. (report_number_to_float($value['tax_rate'])*100 ).'%)',
    								'count' => $value['count'],
    								'amount' => $value['tax']
    							);
    					}
    				}
			}

			uasort( $included_taxes, 'last_item_compare' );
			$included_taxes = array_values( $included_taxes );
			uasort( $non_included_taxes, 'last_item_compare' );
			$non_included_taxes = array_values( $non_included_taxes );
			uasort( $exemptions, 'last_item_compare' );
			$exemptions = array_values( $exemptions );

			//If we have enabled record mode "Daily Total". Get daily total tips from orders table order_id like '777%'
			$custom_daily_tip_information_per_server = array();
			$tipsCountStr = " and cc_transactions.tip_amount > 0";
			$cash_tip_information_per_server = array();
			if($location_info['cash_tips_as_daily_total'] == 1){
				$tipsCountStr = "";
				$daily_cash_tips = lavu_query("SELECT `server_id`, `value` as cashtips FROM `cash_data` WHERE `type` = 'server_cash_tips' AND `loc_id` = '[1]' AND `date` = '[2]'", $location_info["id"], date("Y-m-d"));
				if (mysqli_num_rows($daily_cash_tips) > 0) {
					while($row = mysqli_fetch_assoc($daily_cash_tips)){
						$custom_daily_tip_information_per_server[str_replace('[number]', '', $row['server_id'])] = $row['cashtips'];
					}
				}
			}
			else{
				$temp_tip_information_per_server = $this->runReport( array(
						REPORTS_V2_SELECTION_COLUMN_KEY => '',
						REPORTS_V2_SPECIAL_CODE_KEY => '',
						REPORTS_V2_SELECTION_KEY => 'Preset Date Ranges',
						REPORTS_V2_TABLE_KEY => 'orders',
						REPORTS_V2_TITLE_KEY => 'Tips Per Server',
						REPORTS_V2_NAME_KEY => 'tips_per_server',
						REPORTS_V2_ORDERBY_KEY => '',
						REPORTS_V2_SELECT_KEY => implode( REPORTS_V2_FIELD_SEPERATOR,
								array(
										'orders.server',
										'count(*) as tips',
										'sum_price(orders.cash_tip) as cashtips'
								)),
						REPORTS_V2_GROUPBY_KEY => "orders.server_id",
						REPORTS_V2_WHERE_KEY => implode (REPORTS_V2_FIELD_SEPERATOR,
								array(
										'#orders_placed_active', //Datetime void
										"orders.order_id not like '777%'",
										"orders.cash_tip > 0",
										"orders.location_id = '[locationid]'"
								)),
						REPORTS_V2_JOIN_KEY => '',
						REPORTS_V2_ID_KEY => 'x',
						REPORTS_V2_ORDER_KEY => '',
						REPORTS_V2_DELETED_KEY => '',
				));
				foreach ( $temp_tip_information_per_server as $rowIndex ) {
					foreach ( $rowIndex as $row ) {
						$server_id = str_replace('[number]', '', $row['group_col_orders_server_id']);
						$cash_tip_information_per_server[$server_id]['server'] = $row['server'];
						$cash_tip_information_per_server[$server_id]['tips'] = str_replace('[number]', '', $row['tips']);
						$cash_tip_information_per_server[$server_id]['cashtips'] = str_replace('[money]', '', $row['cashtips']);
					}
				}
			}
			
			
			//If pay_type is cash and record mode "Daily Total" then we overwrite cash tips with daily total tips and add card tips
			$tip_information_per_server = $this->runReport( array(
				REPORTS_V2_SELECTION_COLUMN_KEY => '',
				REPORTS_V2_SPECIAL_CODE_KEY => '',
				REPORTS_V2_SELECTION_KEY => 'Preset Date Ranges',
				REPORTS_V2_TABLE_KEY => 'orders',
				REPORTS_V2_TITLE_KEY => 'Tips Per Server',
				REPORTS_V2_NAME_KEY => 'tips_per_server',
				REPORTS_V2_ORDERBY_KEY => '',
				REPORTS_V2_SELECT_KEY => implode( REPORTS_V2_FIELD_SEPERATOR,
											array(
												'orders.server',
												'count(*) as tips',
												'cc_transactions.pay_type',
												'sum_price(cc_transactions.tip_amount) as tip_amount'
											)),
				REPORTS_V2_GROUPBY_KEY => "orders.server_id, cc_transactions.pay_type",
				REPORTS_V2_WHERE_KEY => implode (REPORTS_V2_FIELD_SEPERATOR,
											array(
												'#transactions_occurred_active', //Datetime void
												"cc_transactions.action = 'Sale'",
												"cc_transactions.order_id NOT LIKE '777%'",
												"cc_transactions.process_data NOT LIKE 'LOCT%'",
												"orders.order_id not like 'Paid%'",
												"orders.location_id = '[locationid]'",
												"cc_transactions.tip_amount>0"
												. $tipsCountStr
											)),
				REPORTS_V2_JOIN_KEY => '',
				REPORTS_V2_ID_KEY => 'x',
				REPORTS_V2_ORDER_KEY => '',
				REPORTS_V2_DELETED_KEY => '',
			));
			
			$custom_tip_information_per_server = array();

			//If pay_type is cash and record mode "Daily Total" then we overwrite cash tips with daily total tips and add card tips
			foreach( $tip_information_per_server as $tip_information2 ) {
			    foreach( $tip_information2 as $tip_information ) {
    				    $server_id = str_replace('[number]', '', $tip_information['group_col_orders_server_id']);
                    if ($tip_information['pay_type'] == 'Cash' && $location_info['cash_tips_as_daily_total']) {
                        $tip_information['tip_amount'] = $custom_daily_tip_information_per_server[$server_id];
                    }
			    }
                $custom_tip_information_per_server[$server_id]['server'] = $tip_information['server'];
                if(array_key_exists('tip_amount',$custom_tip_information_per_server[$server_id])){
					$custom_tip_information_per_server[$server_id]['tip_amount'] += str_replace('[money]', "", $tip_information['tip_amount']);
                }else{
					$custom_tip_information_per_server[$server_id]['tip_amount'] = str_replace('[money]', "", $tip_information['tip_amount']);
                }
                
                if(array_key_exists('tips',$custom_tip_information_per_server[$server_id])){
                	$custom_tip_information_per_server[$server_id]['tips'] += str_replace('[number]', "", $tip_information['tips']);
                }else{
                	$custom_tip_information_per_server[$server_id]['tips'] = str_replace('[number]', "", $tip_information['tips']);
                }
                
                $custom_tip_information_per_server[$server_id]['group_col_orders_server_id'] = $tip_information['group_col_orders_server_id'];
            }
            
            if( count($cash_tip_information_per_server) > 0 ){
                	foreach ($cash_tip_information_per_server as $serverId => $cashTipInformation){
                		if(isset($custom_tip_information_per_server[$serverId])){
                			$custom_tip_information_per_server[$serverId]['tip_amount'] += str_replace('[money]', "", $cashTipInformation['cashtips']);
                			$custom_tip_information_per_server[$serverId]['tips'] += str_replace('[number]', "", $cashTipInformation['tips']);
                		}
                		else{
                			$custom_tip_information_per_server[$serverId]['server'] = $cashTipInformation['server'];
                			$custom_tip_information_per_server[$serverId]['tip_amount'] += str_replace('[money]', "", $cashTipInformation['cashtips']);
                			$custom_tip_information_per_server[$serverId]['tips'] += str_replace('[number]', "", $cashTipInformation['tips']);
                			$custom_tip_information_per_server[$serverId]['group_col_orders_server_id'] = $cashTipInformation['group_col_orders_server_id'];
                		}
                	}
            }
            

            $tip_information_per_server = array();
            foreach( $custom_tip_information_per_server as $server_id => $custom_information ){
                // Show only those server who has tips amount. Handled a test case here when server served orders and mode is daily total but 
                // not added daily total tips amount
                	if ($custom_information['tip_amount'] > 0 )
                	{
    	                $tip_information_per_server[$server_id]['server'] = $custom_information['server'];
    	                $tip_information_per_server[$server_id]['tips'] = '[number]'.$custom_information['tips'];
    	                $tip_information_per_server[$server_id]['tip_amount'] = '[money]'.$custom_information['tip_amount'];
    	                $tip_information_per_server[$server_id]['average'] = report_money_output( report_number_to_float($custom_information['tip_amount']/$custom_information['tips']) );
                	}
            }

            //---
			$payments_from_payments = $this->runReport( array(
				REPORTS_V2_SELECTION_COLUMN_KEY => '',
				REPORTS_V2_SPECIAL_CODE_KEY => '',
				REPORTS_V2_SELECTION_KEY => 'Preset Date Ranges',
				REPORTS_V2_TABLE_KEY => 'cc_transactions',
				REPORTS_V2_TITLE_KEY => 'Payment Breakouts',
				REPORTS_V2_NAME_KEY => 'payment_breakouts',
				REPORTS_V2_ORDERBY_KEY => 'total_collected desc',
				REPORTS_V2_SELECT_KEY => implode( REPORTS_V2_FIELD_SEPERATOR,
										array(
											"concat( cc_transactions.pay_type, concat( ' ', cc_transactions.card_type)) as pay_type",
											"cc_transactions.pay_type as pay_type_raw",
											"cc_transactions.pay_type_id as pay_type_id",
											'cc_transactions.action',
											'cc_transactions.voided',
											'count(*) as count',
											'sum_price(cc_transactions.tip_amount) as tip_amount',
											'sum_price(cc_transactions.total_collected) as total_collected'
										)),
				REPORTS_V2_GROUPBY_KEY => "concat( cc_transactions.pay_type, concat( ' ', cc_transactions.card_type)),cc_transactions.action,cc_transactions.voided",
				REPORTS_V2_WHERE_KEY => implode (REPORTS_V2_FIELD_SEPERATOR,
										array(
											'#transactions_occurred_all',
											"cc_transactions.order_id not like 'Paid%'",
											"cc_transactions.process_data NOT LIKE 'LOCT%'",
											"cc_transactions.currency_type != '1'",
											"cc_transactions.loc_id = '[locationid]'"
										)),
				REPORTS_V2_JOIN_KEY => '',
				REPORTS_V2_ID_KEY => 'x',
				REPORTS_V2_ORDER_KEY => '',
				REPORTS_V2_DELETED_KEY => '',
			));

			$payments_batch_status = $this->runReport( array(
				REPORTS_V2_SELECTION_COLUMN_KEY => '',
				REPORTS_V2_SPECIAL_CODE_KEY => '',
				REPORTS_V2_SELECTION_KEY => 'Preset Date Ranges',
				REPORTS_V2_TABLE_KEY => 'cc_transactions',
				REPORTS_V2_TITLE_KEY => 'Payment Batch Status',
				REPORTS_V2_NAME_KEY => 'payment_batch_status',
				REPORTS_V2_ORDERBY_KEY => 'total_collected desc',
				REPORTS_V2_SELECT_KEY => implode( REPORTS_V2_FIELD_SEPERATOR,
											array(
												'cc_transactions.processed',
												'count(*) as count',
												'sum_price(cc_transactions.tip_amount) as tip_amount',
												'sum_price(cc_transactions.total_collected) as total_collected'
											)),
				REPORTS_V2_GROUPBY_KEY => "cc_transactions.processed",
				REPORTS_V2_WHERE_KEY => implode (REPORTS_V2_FIELD_SEPERATOR,
											array(
												'#transactions_occurred_all',
												"(cc_transactions.action = 'Sale' or cc_transactions.action = 'Auth')",
												"cc_transactions.voided='0'",
												"cc_transactions.pay_type_id = '2'", //Credit Card
												"cc_transactions.order_id not like 'Paid%'",
												"cc_transactions.process_data NOT LIKE 'LOCT%'",
												"cc_transactions.loc_id = '[locationid]'"
											)),
				REPORTS_V2_JOIN_KEY => '',
				REPORTS_V2_ID_KEY => 'x',
				REPORTS_V2_ORDER_KEY => '',
				REPORTS_V2_DELETED_KEY => '',
			));

			$payments_from_before = $this->runReport( array(
				REPORTS_V2_SELECTION_COLUMN_KEY => '',
				REPORTS_V2_SPECIAL_CODE_KEY => '',
				REPORTS_V2_SELECTION_KEY => 'Preset Date Ranges',
				REPORTS_V2_TABLE_KEY => 'cc_transactions',
				REPORTS_V2_TITLE_KEY => 'Payment Breakouts',
				REPORTS_V2_NAME_KEY => 'payment_breakouts',
				REPORTS_V2_ORDERBY_KEY => 'total_collected desc',
				REPORTS_V2_SELECT_KEY => implode( REPORTS_V2_FIELD_SEPERATOR,
											array(
												"concat( cc_transactions.pay_type, concat( ' ', cc_transactions.card_type)) as pay_type",
												'cc_transactions.action',
												'cc_transactions.voided',
												'count(*) as count',
												'sum_price(cc_transactions.tip_amount) as tip_amount',
												'sum_price(cc_transactions.total_collected) as total_collected'
											)),
				REPORTS_V2_GROUPBY_KEY => "concat( cc_transactions.pay_type, concat( ' ', cc_transactions.card_type)),cc_transactions.action,cc_transactions.voided",
				REPORTS_V2_WHERE_KEY => implode (REPORTS_V2_FIELD_SEPERATOR,
											array(
												'#orders_placed_active',
												'#transactions_occurred_before',
												"cc_transactions.order_id not like 'Paid%'",
												"cc_transactions.process_data NOT LIKE 'LOCT%'",
												"cc_transactions.loc_id = '[locationid]'"
											)),
				REPORTS_V2_JOIN_KEY => '',
				REPORTS_V2_ID_KEY => 'x',
				REPORTS_V2_ORDER_KEY => '',
				REPORTS_V2_DELETED_KEY => '',
			));

			$payments_from_after = $this->runReport( array(
				REPORTS_V2_SELECTION_COLUMN_KEY => '',
				REPORTS_V2_SPECIAL_CODE_KEY => '',
				REPORTS_V2_SELECTION_KEY => 'Preset Date Ranges',
				REPORTS_V2_TABLE_KEY => 'cc_transactions',
				REPORTS_V2_TITLE_KEY => 'Payment Breakouts',
				REPORTS_V2_NAME_KEY => 'payment_breakouts',
				REPORTS_V2_ORDERBY_KEY => 'total_collected desc',
				REPORTS_V2_SELECT_KEY => implode( REPORTS_V2_FIELD_SEPERATOR,
											array(
												"concat( cc_transactions.pay_type, concat( ' ', cc_transactions.card_type)) as pay_type",
												'cc_transactions.action',
												'cc_transactions.voided',
												'count(*) as count',
												'sum_price(cc_transactions.tip_amount) as tip_amount',
												'sum_price(cc_transactions.total_collected) as total_collected'
											)),
				REPORTS_V2_GROUPBY_KEY => "concat( cc_transactions.pay_type, concat( ' ', cc_transactions.card_type)),cc_transactions.action,cc_transactions.voided",
				REPORTS_V2_WHERE_KEY => implode (REPORTS_V2_FIELD_SEPERATOR,
											array(
												'#orders_placed_active',
												'#transactions_occurred_after',
												"cc_transactions.order_id not like 'Paid%'",
												"cc_transactions.process_data NOT LIKE 'LOCT%'",
												"cc_transactions.loc_id = '[locationid]'"
											)),
				REPORTS_V2_JOIN_KEY => '',
				REPORTS_V2_ID_KEY => 'x',
				REPORTS_V2_ORDER_KEY => '',
				REPORTS_V2_DELETED_KEY => '',
			));

			$payments_from_open = $this->runReport( array(
				REPORTS_V2_SELECTION_COLUMN_KEY => '',
				REPORTS_V2_SPECIAL_CODE_KEY => '',
				REPORTS_V2_SELECTION_KEY => 'Preset Date Ranges',
				REPORTS_V2_TABLE_KEY => 'cc_transactions',
				REPORTS_V2_TITLE_KEY => 'Payment Breakouts',
				REPORTS_V2_NAME_KEY => 'payment_breakouts',
				REPORTS_V2_ORDERBY_KEY => 'total_collected desc',
				REPORTS_V2_SELECT_KEY => implode( REPORTS_V2_FIELD_SEPERATOR,
											array(
												"concat( cc_transactions.pay_type, concat( ' ', cc_transactions.card_type)) as pay_type",
												'cc_transactions.action',
												'cc_transactions.voided',
												'count(*) as count',
												'sum_price(cc_transactions.tip_amount) as tip_amount',
												'sum_price(cc_transactions.total_collected) as total_collected'
											)),
				REPORTS_V2_GROUPBY_KEY => "concat( cc_transactions.pay_type, concat( ' ', cc_transactions.card_type)),cc_transactions.action,cc_transactions.voided",
				REPORTS_V2_WHERE_KEY => implode (REPORTS_V2_FIELD_SEPERATOR,
											array(
												'#orders_placed_open',
												'#transactions_occurred_active',
												"cc_transactions.order_id not like 'Paid%'",
												"cc_transactions.process_data NOT LIKE 'LOCT%'",
												"cc_transactions.loc_id = '[locationid]'"
											)),
				REPORTS_V2_JOIN_KEY => '',
				REPORTS_V2_ID_KEY => 'x',
				REPORTS_V2_ORDER_KEY => '',
				REPORTS_V2_DELETED_KEY => '',
			));

			$payments_from_voided = $this->runReport( array(
				REPORTS_V2_SELECTION_COLUMN_KEY => '',
				REPORTS_V2_SPECIAL_CODE_KEY => '',
				REPORTS_V2_SELECTION_KEY => 'Preset Date Ranges',
				REPORTS_V2_TABLE_KEY => 'cc_transactions',
				REPORTS_V2_TITLE_KEY => 'Payment Breakouts',
				REPORTS_V2_NAME_KEY => 'payment_breakouts',
				REPORTS_V2_ORDERBY_KEY => 'total_collected desc',
				REPORTS_V2_SELECT_KEY => implode( REPORTS_V2_FIELD_SEPERATOR,
											array(
												"concat( cc_transactions.pay_type, concat( ' ', cc_transactions.card_type)) as pay_type",
												'cc_transactions.action',
												'cc_transactions.voided',
												'count(*) as count',
												'sum_price(cc_transactions.tip_amount) as tip_amount',
												'sum_price(cc_transactions.total_collected) as total_collected'
											)),
				REPORTS_V2_GROUPBY_KEY => "concat( cc_transactions.pay_type, concat( ' ', cc_transactions.card_type)),cc_transactions.action,cc_transactions.voided",
				REPORTS_V2_WHERE_KEY => implode (REPORTS_V2_FIELD_SEPERATOR,
											array(
												"orders.void = '1'",
												'#transactions_occurred_active',
												"cc_transactions.order_id not like 'Paid%'",
												"cc_transactions.process_data NOT LIKE 'LOCT%'",
												"cc_transactions.loc_id = '[locationid]'"
											)),
				REPORTS_V2_JOIN_KEY => '',
				REPORTS_V2_ID_KEY => 'x',
				REPORTS_V2_ORDER_KEY => '',
				REPORTS_V2_DELETED_KEY => '',
			));

			$payments_marked_for_deposit = $this->runReport( array(
				REPORTS_V2_SELECTION_COLUMN_KEY => '',
				REPORTS_V2_SPECIAL_CODE_KEY => '',
				REPORTS_V2_SELECTION_KEY => 'Preset Date Ranges',
				REPORTS_V2_TABLE_KEY => 'cc_transactions',
				REPORTS_V2_TITLE_KEY => 'Payment Breakouts',
				REPORTS_V2_NAME_KEY => 'payment_breakouts',
				REPORTS_V2_ORDERBY_KEY => 'total_collected desc',
				REPORTS_V2_SELECT_KEY => implode( REPORTS_V2_FIELD_SEPERATOR,
											array(
												"concat( cc_transactions.pay_type, concat( ' ', cc_transactions.card_type)) as pay_type",
												'cc_transactions.action',
												'cc_transactions.voided',
												'count(*) as count',
												'sum_price(cc_transactions.tip_amount) as tip_amount',
												'sum_price(cc_transactions.total_collected) as total_collected'
											)),
				REPORTS_V2_GROUPBY_KEY => "concat( cc_transactions.pay_type, concat( ' ', cc_transactions.card_type)),cc_transactions.action,cc_transactions.voided",
				REPORTS_V2_WHERE_KEY => implode (REPORTS_V2_FIELD_SEPERATOR,
											array(
												"cc_transactions.for_deposit = '1'",
												'#transactions_occurred_active',
												"cc_transactions.order_id not like 'Paid%'",
												"cc_transactions.process_data NOT LIKE 'LOCT%'",
												"cc_transactions.loc_id = '[locationid]'"
											)),
				REPORTS_V2_JOIN_KEY => '',
				REPORTS_V2_ID_KEY => 'x',
				REPORTS_V2_ORDER_KEY => '',
				REPORTS_V2_DELETED_KEY => '',
			));

			$payments_marked_for_deposit_redeemed = $this->runReport( array(
				REPORTS_V2_SELECTION_COLUMN_KEY => '',
				REPORTS_V2_SPECIAL_CODE_KEY => '',
				REPORTS_V2_SELECTION_KEY => 'Preset Date Ranges',
				REPORTS_V2_TABLE_KEY => 'cc_transactions',
				REPORTS_V2_TITLE_KEY => 'Payment Breakouts',
				REPORTS_V2_NAME_KEY => 'payment_breakouts',
				REPORTS_V2_ORDERBY_KEY => 'total_collected desc',
				REPORTS_V2_SELECT_KEY => implode( REPORTS_V2_FIELD_SEPERATOR,
											array(
												"concat( cc_transactions.pay_type, concat( ' ', cc_transactions.card_type)) as pay_type",
												'cc_transactions.action',
												'cc_transactions.voided',
												'count(*) as count',
												'sum_price(cc_transactions.tip_amount) as tip_amount',
												'sum_price(cc_transactions.total_collected) as total_collected'
											)),
				REPORTS_V2_GROUPBY_KEY => "concat( cc_transactions.pay_type, concat( ' ', cc_transactions.card_type)),cc_transactions.action,cc_transactions.voided",
				REPORTS_V2_WHERE_KEY => implode (REPORTS_V2_FIELD_SEPERATOR,
											array(
												"cc_transactions.for_deposit = '1'",
												'#orders_placed_active',
												"cc_transactions.order_id not like 'Paid%'",
												"cc_transactions.process_data NOT LIKE 'LOCT%'",
												"cc_transactions.loc_id = '[locationid]'"
											)),
				REPORTS_V2_JOIN_KEY => '',
				REPORTS_V2_ID_KEY => 'x',
				REPORTS_V2_ORDER_KEY => '',
				REPORTS_V2_DELETED_KEY => '',
			));

			$payments_for_before = $this->runReport( array(
				REPORTS_V2_SELECTION_COLUMN_KEY => '',
				REPORTS_V2_SPECIAL_CODE_KEY => '',
				REPORTS_V2_SELECTION_KEY => 'Preset Date Ranges',
				REPORTS_V2_TABLE_KEY => 'cc_transactions',
				REPORTS_V2_TITLE_KEY => 'Payment Breakouts',
				REPORTS_V2_NAME_KEY => 'payment_breakouts',
				REPORTS_V2_ORDERBY_KEY => 'total_collected desc',
				REPORTS_V2_SELECT_KEY => implode( REPORTS_V2_FIELD_SEPERATOR,
											array(
												"concat( cc_transactions.pay_type, concat( ' ', cc_transactions.card_type)) as pay_type",
												'cc_transactions.action',
												'cc_transactions.voided',
												'count(*) as count',
												'sum_price(cc_transactions.tip_amount) as tip_amount',
												'sum_price(cc_transactions.total_collected) as total_collected'
											)),
				REPORTS_V2_GROUPBY_KEY => "concat( cc_transactions.pay_type, concat( ' ', cc_transactions.card_type)),cc_transactions.action,cc_transactions.voided",
				REPORTS_V2_WHERE_KEY => implode (REPORTS_V2_FIELD_SEPERATOR,
											array(
												'#orders_placed_before',
												'#transactions_occurred_active',
												"cc_transactions.order_id not like 'Paid%'",
												"cc_transactions.process_data NOT LIKE 'LOCT%'",
												"cc_transactions.loc_id = '[locationid]'"
											)),
				REPORTS_V2_JOIN_KEY => '',
				REPORTS_V2_ID_KEY => 'x',
				REPORTS_V2_ORDER_KEY => '',
				REPORTS_V2_DELETED_KEY => '',
			));

			$payments_for_after = $this->runReport( array(
				REPORTS_V2_SELECTION_COLUMN_KEY => '',
				REPORTS_V2_SPECIAL_CODE_KEY => '',
				REPORTS_V2_SELECTION_KEY => 'Preset Date Ranges',
				REPORTS_V2_TABLE_KEY => 'cc_transactions',
				REPORTS_V2_TITLE_KEY => 'Payment Breakouts',
				REPORTS_V2_NAME_KEY => 'payment_breakouts',
				REPORTS_V2_ORDERBY_KEY => 'total_collected desc',
				REPORTS_V2_SELECT_KEY => implode( REPORTS_V2_FIELD_SEPERATOR,
											array(
												"concat( cc_transactions.pay_type, concat( ' ', cc_transactions.card_type)) as pay_type",
												'cc_transactions.action',
												'cc_transactions.voided',
												'count(*) as count',
												'sum_price(cc_transactions.tip_amount) as tip_amount',
												'sum_price(cc_transactions.total_collected) as total_collected'
											)),
				REPORTS_V2_GROUPBY_KEY => "concat( cc_transactions.pay_type, concat( ' ', cc_transactions.card_type)),cc_transactions.action,cc_transactions.voided",
				REPORTS_V2_WHERE_KEY => implode (REPORTS_V2_FIELD_SEPERATOR,
											array(
												'#orders_placed_after',
												'#transactions_occurred_active',
												"cc_transactions.order_id not like 'Paid%'",
												"cc_transactions.process_data NOT LIKE 'LOCT%'",
												"cc_transactions.loc_id = '[locationid]'"
											)),
				REPORTS_V2_JOIN_KEY => '',
				REPORTS_V2_ID_KEY => 'x',
				REPORTS_V2_ORDER_KEY => '',
				REPORTS_V2_DELETED_KEY => '',
			));

			$order_payment_mismatch_payments = $this->runReport( array(
				REPORTS_V2_SELECTION_COLUMN_KEY => '',
				REPORTS_V2_SPECIAL_CODE_KEY => '',
				REPORTS_V2_SELECTION_KEY => 'Preset Date Ranges',
				REPORTS_V2_TABLE_KEY => 'orders',
				REPORTS_V2_TITLE_KEY => 'Payment mismatch',
				REPORTS_V2_NAME_KEY => 'payment_mismatch',
				REPORTS_V2_ORDERBY_KEY => '',
				REPORTS_V2_SELECT_KEY => implode( REPORTS_V2_FIELD_SEPERATOR,
											array(
												"'' as payment_mismatch",
												'orders.order_id',
												'format((orders.cash_applied + orders.card_paid + orders.gift_certificate + orders.alt_paid) - (orders.total),2) as amount'
											)),
				REPORTS_V2_GROUPBY_KEY => "orders.order_id",
				REPORTS_V2_WHERE_KEY => implode (REPORTS_V2_FIELD_SEPERATOR,
											array(
												'#orders_placed_active',
												"format((orders.cash_applied + orders.card_paid + orders.gift_certificate + orders.alt_paid),4) - format(orders.total,2)",
												"orders.void = '0'",
												"orders.order_id not like 'Paid%'",
												"orders.location_id = '[locationid]'"
											)),
				REPORTS_V2_JOIN_KEY => '',
				REPORTS_V2_ID_KEY => 'x',
				REPORTS_V2_ORDER_KEY => '',
				REPORTS_V2_DELETED_KEY => '',
			));
			foreach( $order_payment_mismatch_payments as $mismatch_row => $mismatch_record ){
			    foreach( $mismatch_record as $row => $record ){
				    $order_payment_mismatch_payments[$mismatch_row][0]['amount'] = report_float_to_money( report_number_to_float($record['amount'])*1 );
			    }
			}
			$payments_paid_in_out = $this->runReport( array(
				REPORTS_V2_SELECTION_COLUMN_KEY => '',
				REPORTS_V2_SPECIAL_CODE_KEY => '',
				REPORTS_V2_SELECTION_KEY => 'Preset Date Ranges',
				REPORTS_V2_TABLE_KEY => 'cc_transactions',
				REPORTS_V2_TITLE_KEY => 'Payment Breakouts',
				REPORTS_V2_NAME_KEY => 'payment_breakouts',
				REPORTS_V2_ORDERBY_KEY => 'total_collected desc',
				REPORTS_V2_SELECT_KEY => implode( REPORTS_V2_FIELD_SEPERATOR,
											array(
												"concat( cc_transactions.pay_type, concat( ' ', cc_transactions.card_type)) as pay_type",
												'cc_transactions.action',
												'cc_transactions.voided',
												'cc_transactions.order_id',
												'count(*) as count',
												'sum_price(cc_transactions.tip_amount) as tip_amount',
												'sum_price(cc_transactions.total_collected) as total_collected'
											)),
				REPORTS_V2_GROUPBY_KEY => "concat( cc_transactions.pay_type, concat( ' ', cc_transactions.card_type)),cc_transactions.action,cc_transactions.voided",
				REPORTS_V2_WHERE_KEY => implode (REPORTS_V2_FIELD_SEPERATOR,
											array(
												'#transactions_occurred_all',
												"cc_transactions.order_id like 'Paid%'",
												"cc_transactions.loc_id = '[locationid]'"
											)),
				REPORTS_V2_JOIN_KEY => '',
				REPORTS_V2_ID_KEY => 'x',
				REPORTS_V2_ORDER_KEY => '',
				REPORTS_V2_DELETED_KEY => '',
			));

			$orders_per_hour_data = $this->runReport( array(
				REPORTS_V2_SELECTION_COLUMN_KEY => '',
				REPORTS_V2_SPECIAL_CODE_KEY => '',
				REPORTS_V2_SELECTION_KEY => 'Preset Date Ranges',
				REPORTS_V2_TABLE_KEY => 'orders',
				REPORTS_V2_TITLE_KEY => 'Order Statistics',
				REPORTS_V2_NAME_KEY => 'order_statistics',
				REPORTS_V2_ORDERBY_KEY => 'guests desc',
				REPORTS_V2_SELECT_KEY => implode( REPORTS_V2_FIELD_SEPERATOR,
											array(
												'#opened_actual_date_hour',
												'#count',
												'#sum_guests',
												'#sum_checks'
											)),
				REPORTS_V2_GROUPBY_KEY => "#opened_actual_date_hour",
				REPORTS_V2_WHERE_KEY => implode (REPORTS_V2_FIELD_SEPERATOR,
											array(
												'#orders_placed_active',
												"orders.location_id = '[locationid]'"
											)),
				REPORTS_V2_JOIN_KEY => '',
				REPORTS_V2_ID_KEY => 'x',
				REPORTS_V2_ORDER_KEY => '',
				REPORTS_V2_DELETED_KEY => '',
			));

			$orders_per_hour = array();
			{
				$orders_per_hour_stats = array();
				foreach( $orders_per_hour_data as $row => $data ){
				    foreach( $data as $row => $data ){
        					$hour = substr( substr( $data['actual_opened_date_and_hour'], -8 ), 0, 5);
        					if (! isset( $orders_per_hour_stats[ $hour ] ) ){
        						$orders_per_hour_stats[ $hour ] = array(
        							'hour' => $hour,
        							'count' => 0,
        							'orders' => 0
        						);
        					}

        					if(!isset($orders_per_hour_stats[ $hour ]['count'])){
        						$orders_per_hour_stats[ $hour ]['count'] = 0;
        					}

        					if(!isset($orders_per_hour_stats[ $hour ]['guests'])){
        						$orders_per_hour_stats[ $hour ]['guests'] = 0;
        					}

        					if(!isset($orders_per_hour_stats[ $hour ]['checks'])){
        						$orders_per_hour_stats[ $hour ]['checks'] = 0;
        					}

        					$orders_per_hour_stats[ $hour ]['count']++;
        					$orders_per_hour_stats[ $hour ]['orders'] += report_number_to_float($data['count']);
        					$orders_per_hour_stats[ $hour ]['guests'] += report_number_to_float($data['guests']);
        					$orders_per_hour_stats[ $hour ]['checks'] += report_number_to_float($data['number_of_checks']);
        				}
        			}
				$keys = array_keys( $orders_per_hour_stats );
				sort( $keys );
				$keys = array_values( $keys );
				for( $i = 0; $i < count( $keys ); $i++ ){
					$count = $orders_per_hour_stats[$keys[$i]]['count'];
					$orders = $orders_per_hour_stats[$keys[$i]]['orders'];
					$guests = $orders_per_hour_stats[$keys[$i]]['guests'];
					$checks = $orders_per_hour_stats[$keys[$i]]['checks'];
					$orders_per_hour[] = array(
						'hour' => $keys[$i],
						'hour_count' => $count,
						'avg_orders' => report_float_to_number( $orders / $count, 2),
						'avg_checks' => report_float_to_number( $checks / $count, 2),
						'avg_guests' => report_float_to_number( $guests / $count, 2)
					);
				}
			}

			$currently_open_orders = $this->runReport( array(
				REPORTS_V2_SELECTION_COLUMN_KEY => '',
				REPORTS_V2_SPECIAL_CODE_KEY => '',
				REPORTS_V2_SELECTION_KEY => 'Preset Date Ranges',
				REPORTS_V2_TABLE_KEY => 'orders',
				REPORTS_V2_TITLE_KEY => 'Order Open',
				REPORTS_V2_NAME_KEY => 'orders_open',
				REPORTS_V2_ORDERBY_KEY => 'total_collected desc',
				REPORTS_V2_SELECT_KEY => implode( REPORTS_V2_FIELD_SEPERATOR,
											array(
												'orders.order_id as open_orders'
											)),
				REPORTS_V2_GROUPBY_KEY => "orders.order_id",
				REPORTS_V2_WHERE_KEY => implode (REPORTS_V2_FIELD_SEPERATOR,
											array(
												'#orders_placed_open',
												'#orders_opened_active',
												"orders.location_id = '[locationid]'"
											)),
				REPORTS_V2_JOIN_KEY => '',
				REPORTS_V2_ID_KEY => 'x',
				REPORTS_V2_ORDER_KEY => '',
				REPORTS_V2_DELETED_KEY => '',
			));

			foreach($currently_open_orders as $d){
			    $currently_open_orders2[]=$d[0];
			}

			$currently_open_orders= $currently_open_orders2;

			$sales_payments_types = array();
			$tip_information = array();
			if ($location_info['gift_revenue_tracked'] == 'when_redeemed'){
			$extra_added_payments = $this->runReport( array(
                                REPORTS_V2_SELECTION_COLUMN_KEY => '',
                                REPORTS_V2_SPECIAL_CODE_KEY => '',
                                REPORTS_V2_SELECTION_KEY => 'Preset Date Ranges',
                                REPORTS_V2_TABLE_KEY => 'cc_transactions',
                                REPORTS_V2_TITLE_KEY => 'Payment Breakouts',
                                REPORTS_V2_NAME_KEY => 'payment_breakouts',
                                REPORTS_V2_ORDERBY_KEY => 'total_collected desc',
                                REPORTS_V2_SELECT_KEY => implode( REPORTS_V2_FIELD_SEPERATOR,
                                                                                array(
                                                                                        "'' as gift_cards_sales",
                                                                                        'count(*) as count',
                                                                                        'sum_price(cc_transactions.total_collected) as amount'
                                                                                )),
                                REPORTS_V2_GROUPBY_KEY => "concat( cc_transactions.pay_type, concat( ' ', cc_transactions.card_type)),cc_transactions.action,cc_transactions.voided",
                                REPORTS_V2_WHERE_KEY => implode (REPORTS_V2_FIELD_SEPERATOR,
                                                                                array(
                                                                                        '#giftcards_transactions_all',
                                                                                        "cc_transactions.order_id not like 'Paid%'",
                                                                                        "cc_transactions.process_data NOT LIKE 'LOCT%'",
                                                                                        "cc_transactions.currency_type != '1'",
                                                                                        "cc_transactions.loc_id = '[locationid]'",
                                                                                        "cc_transactions.action IN ('Issue', 'Reload')",
                                                                                        "cc_transactions.transtype IN ('IssueGiftCard', 'ReloadGiftCard')"
                                                                                )),
                                REPORTS_V2_JOIN_KEY => '',
                                REPORTS_V2_ID_KEY => 'x',
                                REPORTS_V2_ORDER_KEY => '',
                                REPORTS_V2_DELETED_KEY => '',
                        ));
			}
			foreach( $payments_from_payments as $row_index ){
			    foreach($row_index as $row){
			        if( ($row['action'] != 'Sale' && $row['action'] != 'CashTips' && $row['action'] != 'Refund') || report_number_to_float($row['voided']) != 0 ){
			            continue;
			        }
			        if( report_money_to_number( $row['tip_amount'] ) *1 != 0 ){
			            if( !isset( $tip_information[$row['pay_type']] ) ){
							$payType = trim($row['pay_type']);
			                $tip_information[$payType] = array(
			                    'tips' => $payType,
			                    ' ' => '',
			                    'amount' => report_float_to_money( 0 )
			                );
			            }

			            if( $row['action'] == 'Refund' ){
			                $tip_information[$payType]['amount'] = report_float_to_money( report_money_to_number( $tip_information[$payType]['amount'] ) - report_money_to_number( $row['tip_amount'] ) );
			            } else {
			                $tip_information[$payType]['amount'] = report_float_to_money( report_money_to_number( $row['tip_amount'] ) + report_money_to_number( $tip_information[$payType]['amount'] ) );
			            }
					}

			        if( !isset($sales_payments_types[$row['pay_type_raw']]) ){
			            $sales_payments_types[$row['pay_type_raw']] = array(
			                'payment_type' => $row['pay_type_raw'],
			                'count' => report_float_to_number( 0 ),
			                'amount' => report_float_to_money( 0 )
			            );
			        }
			        
			        if( $row['action'] == 'Refund' ){
						$paytypeAmount = sprintf('%0.4f', (report_money_to_number( $sales_payments_types[$row['pay_type_raw']]['amount'] ) - report_money_to_number( $row['total_collected'] ) ) );
						$sales_payments_types[$row['pay_type_raw']]['amount'] = report_float_to_money($paytypeAmount);
			        }
			        else{
						$paytypeAmount = sprintf('%0.4f', (report_money_to_number( $sales_payments_types[$row['pay_type_raw']]['amount'] ) + report_money_to_number( $row['total_collected'] )));
						$sales_payments_types[$row['pay_type_raw']]['amount'] = report_float_to_money($paytypeAmount);
			        }
			        $sales_payments_types[$row['pay_type_raw']]['count'] = report_float_to_number( report_number_to_float( $row['count'] ) + report_number_to_float( $sales_payments_types[$row['pay_type_raw']]['count'] ) );
			    }
			}
			/**
			 * This condition is used to show per-order related cash tips in V2 EOD Report.
			 */
			if ($location_info['cash_tips_as_daily_total'] != 1) {
				if (count($cash_tip_information_per_server) > 0) {
					$payType = 'Cash';
					$tip_information[$payType]['tips'] = $payType;
					$tip_information[$payType][' '] = '';
					$tip_information[$payType]['amount'] = report_float_to_money( 0 );
					foreach ( $cash_tip_information_per_server as $cashTipInformation ) {
						$tip_information[$payType]['amount'] = report_float_to_money(report_number_to_float( $cashTipInformation['cashtips']) + report_money_to_number($tip_information[$payType]['amount']));
					}
				}
			}
			uasort( $tip_information, 'last_item_compare' );
			uasort( $sales_payments_types, 'last_item_compare' );
			$sales_payments_types = array( '0' => $sales_payments_types );
			$tip_information = array_values( $tip_information );
			$tip_information = array( '0' => $tip_information );

			$sale_payments = array();
			$refunds = array();
			$gift_cards_balance_change = array();
			$voided_sale_payments = array();
			$voided_refunds_payments = array();
			$voided_gift_cards_balance_change = array();

			$this->fill_in_payment_arrays_using_payment_details( $payments_from_payments, $sale_payments, $refunds, $gift_cards_balance_change, $voided_sale_payments, $voided_refunds_payments, $voided_gift_cards_balance_change, 'payments', 'refunds', '' );

			$before_sale_payments = array();
			$before_refund_payments = array();
			$before_giftcard_payments = array();
			$before_voided_sale_payments = array();
			$before_voided_refund_payments = array();
			$before_voided_giftcard_payments = array();
			$this->fill_in_payment_arrays_using_payment_details( $payments_from_before, $before_sale_payments, $before_refund_payments, $before_giftcard_payments, $before_voided_sale_payments, $before_voided_refund_payments, $before_voided_giftcard_payments, 'deposits_redeemed', 'previously_refunded_deposits','_for_deposit' );

			$after_sale_payments = array();
			$after_refund_payments = array();
			$after_giftcard_payments = array();
			$after_voided_sale_payments = array();
			$after_voided_refund_payments = array();
			$after_voided_giftcard_payments = array();
			$this->fill_in_payment_arrays_using_payment_details( $payments_from_after, $after_sale_payments, $after_refund_payments, $after_giftcard_payments, $after_voided_sale_payments, $after_voided_refund_payments, $after_voided_giftcard_payments, 'payments_from_the_future', 'refunds_on_payments_in_the_future','_from_the_future' );

			$open_sale_payments = array();
			$open_refund_payments = array();
			$open_giftcard_payments = array();
			$open_voided_sale_payments = array();
			$open_voided_refund_payments = array();
			$open_voided_giftcard_payments = array();
			$this->fill_in_payment_arrays_using_payment_details( $payments_from_open, $open_sale_payments, $open_refund_payments, $open_giftcard_payments, $open_voided_sale_payments, $open_voided_refund_payments, $open_voided_giftcard_payments, 'deposits_not_yet_redeemed', 'refunds_on_deposits_not_yet_redeemed','_not_yet_redeemed' );

			$void_sale_payments = array();
			$void_refund_payments = array();
			$void_giftcard_payments = array();
			$void_voided_sale_payments = array();
			$void_voided_refund_payments = array();
			$void_voided_giftcard_payments = array();
			$this->fill_in_payment_arrays_using_payment_details( $payments_from_voided, $void_sale_payments, $void_refund_payments, $void_giftcard_payments, $void_voided_sale_payments, $void_voided_refund_payments, $void_voided_giftcard_payments, 'payments_on_voided_orders', 'refunds_on_voided_orders','_from_voided_orders' );


			$orders_before_sale_payments = array();
			$orders_before_refund_payments = array();
			$orders_before_giftcard_payments = array();
			$orders_before_voided_sale_payments = array();
			$orders_before_voided_refund_payments = array();
			$orders_before_voided_giftcard_payments = array();
			$this->fill_in_payment_arrays_using_payment_details( $payments_for_before, $orders_before_sale_payments, $orders_before_refund_payments, $orders_before_giftcard_payments, $orders_before_voided_sale_payments, $orders_before_voided_refund_payments, $orders_before_voided_giftcard_payments, 'payment_for_past_orders', 'refunds_on_payment_for_past_orders','_for_past_orders'  );

			$orders_after_sale_payments = array();
			$orders_after_refund_payments = array();
			$orders_after_giftcard_payments = array();
			$orders_after_voided_sale_payments = array();
			$orders_after_voided_refund_payments = array();
			$orders_after_voided_giftcard_payments = array();
			$this->fill_in_payment_arrays_using_payment_details( $payments_for_after, $orders_after_sale_payments, $orders_after_refund_payments, $orders_after_giftcard_payments, $orders_after_voided_sale_payments, $orders_after_voided_refund_payments, $orders_after_voided_giftcard_payments, 'deposits_collected_redeemed_in_future', 'refunds_deposits_collected_redeemed_in_future','_redeemed_in_future' );

			$marked_deposits_collected_sale_payments = array();
			$marked_deposits_collected_refund_payments = array();
			$marked_deposits_collected_giftcard_payments = array();
			$marked_deposits_collected_voided_sale_payments = array();
			$marked_deposits_collected_voided_refund_payments = array();
			$marked_deposits_collected_voided_giftcard_payments = array();
			$this->fill_in_payment_arrays_using_payment_details( $payments_marked_for_deposit, $marked_deposits_collected_sale_payments, $marked_deposits_collected_refund_payments, $marked_deposits_collected_giftcard_payments, $marked_deposits_collected_voided_sale_payments, $marked_deposits_collected_voided_refund_payments, $marked_deposits_collected_voided_giftcard_payments, 'marked_deposits_collected', 'refunds_marked_deposits_collected','_marked_deposits_collected' );

			$marked_deposits_redeemed_sale_payments = array();
			$marked_deposits_redeemed_refund_payments = array();
			$marked_deposits_redeemed_giftcard_payments = array();
			$marked_deposits_redeemed_voided_sale_payments = array();
			$marked_deposits_redeemed_voided_refund_payments = array();
			$marked_deposits_redeemed_voided_giftcard_payments = array();
			$this->fill_in_payment_arrays_using_payment_details( $payments_marked_for_deposit_redeemed, $marked_deposits_redeemed_sale_payments, $marked_deposits_redeemed_refund_payments, $marked_deposits_redeemed_giftcard_payments, $marked_deposits_redeemed_voided_sale_payments, $marked_deposits_redeemed_voided_refund_payments, $marked_deposits_redeemed_voided_giftcard_payments, 'marked_deposits_redeemed', 'refunds_marked_deposits_redeemed','_marked_deposits_redeemed' );


			$paid_in_out = array();
			foreach ($payments_paid_in_out as $paidInPaidOutArray) {
			    foreach($paidInPaidOutArray as  $value){
			        if( report_number_to_float($value['voided']) == 1 ){
			            continue;
			        }
			        
			        if( $value['action'] == 'Sale' ){
			            $paid_in_out[][] = array(
			                'paid_in_or_out' => $value['order_id'],
			                'count' => $value['count'],
			                'amount' => $value['total_collected']
			            );
			        }
			        else if( $value['action'] == 'Refund' ){
			            $paid_in_out[][] = array(
			                'paid_in_or_out' => $value['order_id'],
			                'count' => $value['count'],
			                'amount' => report_float_to_money( -report_money_to_number($value['total_collected']) )
			            );
			        }
			    }
			}

			$deposit_in_out = array();
			if($payments_deposit_in_out[0][0][deposit_count]!='' && $payments_deposit_in_out[0][0][refund_count]!=''){
				$deposit_in_out[][] = array(
						'customer_deposits_and_refunds' => Deposits,
						'count' => $payments_deposit_in_out[0][0][deposit_count],
						'amount' =>$payments_deposit_in_out[0][0][deposit]
				);
				$deposit_in_out[][] = array(
						'customer_deposits_and_refunds' => Refunds,
						'count' => $payments_deposit_in_out[0][0][refund_count],
						'amount' => $payments_deposit_in_out[0][0][refund]
				);
			}

			uasort( $sale_payments, 'last_item_compare' );
			$sale_payments = array_values( $sale_payments );
			uasort( $refunds, 'last_item_compare' );
			$refunds = array_values( $refunds );
			uasort( $gift_cards_balance_change, 'last_item_compare' );
			$gift_cards_balance_change = array_values( $gift_cards_balance_change );
			uasort( $voided_sale_payments, 'last_item_compare' );
			$voided_sale_payments = array_values( $voided_sale_payments );
			uasort( $voided_refunds_payments, 'last_item_compare' );
			$voided_refunds_payments = array_values( $voided_refunds_payments );
			uasort( $voided_gift_cards_balance_change, 'last_item_compare' );
			$voided_gift_cards_balance_change = array_values( $voided_gift_cards_balance_change );
			$expected_summary = array();
			append_if_not_zero( $expected_summary[], sum_last_item( 'expected_take_in_summary', 'category_gross', $categories ) );
			append_if_not_zero( $expected_summary[], sum_last_item( 'expected_take_in_summary', 'charge_amount', $gratuity ) );
			append_if_not_zero( $expected_summary[], sum_last_item( 'expected_take_in_summary', 'discount_amount', $discounts ), true );
			append_if_not_zero( $expected_summary[], sum_last_item( 'expected_take_in_summary', 'tax_amount', $non_included_taxes ) );
			append_if_not_zero( $expected_summary[], sum_last_item( 'expected_take_in_summary', 'included_tax_amount', $included_taxes ) );
			append_if_not_zero( $expected_summary[], sum_last_item( 'expected_take_in_summary', 'tax_exemptions_(_included_tax_)', $included_taxes_exemptions), true );
			append_if_not_zero( $expected_summary[], sum_last_item( 'expected_take_in_summary', 'check_rounding_amount', $rounding_section ) );
			if($location_info['gift_revenue_tracked'] == 'when_redeemed'){
				append_if_not_zero( $expected_summary[], sum_last_item( 'expected_take_in_summary', 'paid_orders_with_gift_cards', $redeemption_after_payments), true);
			}

			$actual_summary = array();
			append_if_not_zero( $actual_summary[], sum_last_item( 'actual_take_in_summary', 'payments_amount', $sale_payments ) );
			append_if_not_zero( $actual_summary[], sum_last_item( 'actual_take_in_summary', 'refunds_amount', $refunds ), true );
			if($location_info['gift_revenue_tracked'] == 'when_redeemed'){
				append_if_not_zero( $actual_summary[], sum_last_item( 'actual_take_in_summary', 'gift_cards_sales',$extra_added_payments), true );
			}
			$giftcard_credit_summary = array();
			$giftcard_past_credit_summary = array();
			if ($location_info['gift_revenue_tracked'] == 'when_redeemed'){
				global $data_name;
				// based on date format setting
				$date_setting = "date_format";
				$location_date_format_query = lavu_query( "SELECT `location`, `setting`, `value` FROM `config` WHERE `setting` = '[1]'",  $date_setting);
				if( !$location_date_format_query || !mysqli_num_rows($location_date_format_query)){
					$date_format = array("value" => 2);
				}else
					$date_format = mysqli_fetch_assoc( $location_date_format_query );

				$giftcards_payments = $this->runReport( array(
						REPORTS_V2_SELECTION_COLUMN_KEY => '',
						REPORTS_V2_SPECIAL_CODE_KEY => '',
						REPORTS_V2_SELECTION_KEY => 'Preset Date Ranges',
						REPORTS_V2_TABLE_KEY => 'cc_transactions',
						REPORTS_V2_TITLE_KEY => 'Gift Payments',
						REPORTS_V2_NAME_KEY => 'gift_payments',
						REPORTS_V2_ORDERBY_KEY => 'total_collected desc',
						REPORTS_V2_SELECT_KEY => implode( REPORTS_V2_FIELD_SEPERATOR,
								array(
										"concat( cc_transactions.pay_type, concat( ' ', cc_transactions.card_type)) as pay_type",
										"cc_transactions.pay_type as pay_type_raw",
										"cc_transactions.pay_type_id as pay_type_id",
										'cc_transactions.action',
										'cc_transactions.voided',
										'count(*) as count',
										'sum_price(cc_transactions.tip_amount) as tip_amount',
										'sum_price(cc_transactions.total_collected) as total_collected'
								)),
						REPORTS_V2_GROUPBY_KEY => "concat( cc_transactions.pay_type, concat( ' ', cc_transactions.card_type)),cc_transactions.action,cc_transactions.voided",
						REPORTS_V2_WHERE_KEY => implode (REPORTS_V2_FIELD_SEPERATOR,
								array(
										'#giftcards_transactions_all',
										"cc_transactions.action IN ('Issue', 'Reload')",
										"cc_transactions.transtype IN ('IssueGiftCard', 'ReloadGiftCard')",
										"cc_transactions.order_id not like 'Paid%'",
										"cc_transactions.process_data NOT LIKE 'LOCT%'",
										"cc_transactions.currency_type != '1'",
										"cc_transactions.loc_id = '[locationid]'"
								)),
						REPORTS_V2_JOIN_KEY => '',
						REPORTS_V2_ID_KEY => 'x',
						REPORTS_V2_ORDER_KEY => '',
						REPORTS_V2_DELETED_KEY => '',
				));
				
				$past_redeemed = $this->runReport( array(
						REPORTS_V2_SELECTION_COLUMN_KEY => '',
						REPORTS_V2_SPECIAL_CODE_KEY => '',
						REPORTS_V2_SELECTION_KEY => 'Preset Date Ranges',
						REPORTS_V2_TABLE_KEY => 'cc_transactions',
						REPORTS_V2_TITLE_KEY => 'Gift Past Payments',
						REPORTS_V2_NAME_KEY => 'gift_past_payments',
						REPORTS_V2_ORDERBY_KEY => 'total_collected desc',
						REPORTS_V2_SELECT_KEY => implode( REPORTS_V2_FIELD_SEPERATOR,
								array(
										"concat( cc_transactions.pay_type, concat( ' ', cc_transactions.card_type)) as pay_type",
										"cc_transactions.pay_type as pay_type_raw",
										"cc_transactions.pay_type_id as pay_type_id",
										'cc_transactions.first_four as first_four',
										'cc_transactions.action',
										'cc_transactions.voided',
										'count(*) as count',
										'sum_price(cc_transactions.tip_amount) as tip_amount',
										'sum_price(cc_transactions.total_collected) as total_collected'
								)),
						REPORTS_V2_GROUPBY_KEY => "concat( cc_transactions.pay_type, concat( ' ', cc_transactions.card_type)),cc_transactions.first_four,cc_transactions.voided",
						REPORTS_V2_WHERE_KEY => implode (REPORTS_V2_FIELD_SEPERATOR,
								array(
										'#giftcards_transactions_all',
										"cc_transactions.transtype LIKE '%GiftCardSale%'",
										"cc_transactions.action LIKE '%Sale%'",
										"cc_transactions.order_id not like 'Paid%'",
										"cc_transactions.process_data NOT LIKE 'LOCT%'",
										"cc_transactions.currency_type != '1'",
										"cc_transactions.loc_id = '[locationid]'",
										"cc_transactions.voided = '0'"
								)),
						REPORTS_V2_JOIN_KEY => '',
						REPORTS_V2_ID_KEY => 'x',
						REPORTS_V2_ORDER_KEY => '',
						REPORTS_V2_DELETED_KEY => '',
				));
				if(empty($past_redeemed)){
				$past_redeemed = $this->runReport( array(
						REPORTS_V2_SELECTION_COLUMN_KEY => '',
						REPORTS_V2_SPECIAL_CODE_KEY => '',
						REPORTS_V2_SELECTION_KEY => 'Preset Date Ranges',
						REPORTS_V2_TABLE_KEY => 'cc_transactions',
						REPORTS_V2_TITLE_KEY => 'Gift Past Payments',
						REPORTS_V2_NAME_KEY => 'gift_past_payments',
						REPORTS_V2_ORDERBY_KEY => 'total_collected desc',
						REPORTS_V2_SELECT_KEY => implode( REPORTS_V2_FIELD_SEPERATOR,
								array(
										"concat( cc_transactions.pay_type, concat( ' ', cc_transactions.card_type)) as pay_type",
										"cc_transactions.pay_type as pay_type_raw",
										"cc_transactions.pay_type_id as pay_type_id",
										'cc_transactions.first_four as first_four',
										'cc_transactions.action',
										'cc_transactions.voided',
										'count(*) as count',
										'sum_price(cc_transactions.tip_amount) as tip_amount',
										'sum_price(cc_transactions.total_collected) as total_collected'
								)),
						REPORTS_V2_GROUPBY_KEY => "concat( cc_transactions.pay_type, concat( ' ', cc_transactions.card_type)),cc_transactions.first_four,cc_transactions.voided",
						REPORTS_V2_WHERE_KEY => implode (REPORTS_V2_FIELD_SEPERATOR,
								array(
										'#giftcards_transactions_all',
										"cc_transactions.card_type LIKE '%Lavu Gift%'",
										"cc_transactions.action LIKE '%Sale%'",
										"cc_transactions.order_id not like 'Paid%'",
										"cc_transactions.process_data NOT LIKE 'LOCT%'",
										"cc_transactions.currency_type != '1'",
										"cc_transactions.loc_id = '[locationid]'",
										"cc_transactions.voided = '0'"
								)),
						REPORTS_V2_JOIN_KEY => '',
						REPORTS_V2_ID_KEY => 'x',
						REPORTS_V2_ORDER_KEY => '',
						REPORTS_V2_DELETED_KEY => '',
				));
				}
				$gift_card_past_redeemed = array();
				foreach( $past_redeemed as $row_index ){
					foreach($row_index as $row){
						$gift_card_past_redeemed[] = str_replace("[money]", "", $row['total_collected']);
					}
				}
				if(!empty($gift_card_past_redeemed)){
					$giftcard_past_credit_summary[][] = array('Gift_Card_Revenue_from_the_past_Redeemed'=> '', ''=>'', 'amount' => report_float_to_money(array_sum($gift_card_past_redeemed)));
				}

				$revised_sum = array();
				$day = memvar('report_day',date("Y-m-d"));
				$revised_giftcards_query = lavu_query("SELECT DATE_FORMAT(`cc_transactions`.`datetime`, '%Y-%m-%d') as `datewise`, sum(`cc_transactions`.`amount`) as `amount` FROM `cc_transactions` WHERE `cc_transactions`.`card_type`= 'Lavu Gift'  AND `cc_transactions`.`transtype` = 'GiftCardSale' AND `cc_transactions`.`card_desc` IN (SELECT `cc`.`card_desc` FROM `cc_transactions` cc WHERE `cc`.`card_type` = 'Lavu Gift' and DATE_FORMAT(`cc`.`datetime`, '%Y-%m-%d') = '".$day."' AND `cc`.`transtype` IN ('IssueGiftCard', 'ReloadGiftCard')  and `cc`.`first_four` = `cc_transactions`.`first_four`) and DATE_FORMAT(`cc_transactions`.`datetime`, '%Y-%m-%d') >= '".$day."' group by `datewise`");
				while($revised_gifts = mysqli_fetch_assoc($revised_giftcards_query)){
					$revisedGiftCards[][str_replace("-", "/", $revised_gifts['datewise'])] = str_replace("[money]", "", $revised_gifts['amount']);
					$revised_sum[] = $revised_gifts['amount'];
				}
				$gift_card_payment_types = array();
				foreach( $giftcards_payments as $row_index ){
					foreach($row_index as $row){
						$payment_types_query = lavu_query("SELECT `id`, `type` FROM `poslavu_".$data_name."_db`.`payment_types` WHERE `type` = '".$row['pay_type_raw']."' AND `_deleted` != 1 AND `special` IN ('gift_card', 'lavu_gift', 'payment_extension:75', 'payment_extension:91')");
						$payment_types_read = mysqli_fetch_assoc($payment_types_query);
						if ($payment_types_read['type'] == $row['pay_type_raw'] && ( $row['action'] == "Issue" || $row['action'] == "Reload" )){
							$remaining_gift_amount = str_replace("[money]", "", $row['total_collected']);
							$gift_card_payment_types[] = $remaining_gift_amount;
						}
					}
				}
				if(!empty($gift_card_payment_types)){
					$giftcard_credit_summary[][] = array('Gift_Card_Revenue_to_be_Redeemed_in_the_Future'=> '', ''=>'', 'amount' => report_float_to_money(-(array_sum($gift_card_payment_types) - array_sum($revised_sum))));
				}
				foreach($revisedGiftCards as $gift_card){
					foreach($gift_card as $key => $val){
						$change_format = explode("/", $key);
						switch ($date_format['value']){
							case 1:$key = $change_format[2]."/".$change_format[1]."/".$change_format[0];
							break;
							case 2:$key = $change_format[1]."/".$change_format[2]."/".$change_format[0];
							break;
							case 3:$key = $change_format[0]."/".$change_format[1]."/".$change_format[2];
							break;
						}
						$revise_redeemed_gift_cards[] = array('Gift_Card_Revenue_Redeemed_on' => '', '' => $key, 'amount' => report_float_to_money(-$val));
					}
				}
				$giftcard_revised_credit_summary[] = $revise_redeemed_gift_cards;
			}

			//Payments From Orders Before
			//Payments From Orders After
			//Payments From Orders Open
			$difference_summary = array();

			append_if_not_zero( $difference_summary[], sum_last_item( 'reconciliation_difference', 'actual_take_in_summary_sum', $actual_summary ) );
			append_if_not_zero( $difference_summary[], sum_last_item( 'reconciliation_difference', 'expected_take_in_summary_sum', $expected_summary ), true );

			$reconciliation_summary = array();
			append_if_not_zero( $reconciliation_summary[], sum_last_item( 'reconciliation_summary', 'reconciliation_difference_sum', $difference_summary ) );
			append_if_not_zero( $reconciliation_summary[], sum_last_item( 'reconciliation_summary', 'payment_mismatch_amount', $order_payment_mismatch_payments ), true );
			append_if_not_zero( $reconciliation_summary[], sum_last_item( 'reconciliation_summary', 'deposits_redeemed_amount', $before_sale_payments ) );
			append_if_not_zero( $reconciliation_summary[], sum_last_item( 'reconciliation_summary', 'previously_refunded_deposits_amount', $before_refund_payments ), true );
			append_if_not_zero( $reconciliation_summary[], sum_last_item( 'reconciliation_summary', 'payments_from_the_future_amount', $after_sale_payments ) );
			append_if_not_zero( $reconciliation_summary[], sum_last_item( 'reconciliation_summary', 'refunds_on_payments_in_the_future_amount', $after_refund_payments ), true );
			append_if_not_zero( $reconciliation_summary[], sum_last_item( 'reconciliation_summary', 'deposits_not_yet_redeemed_amount', $open_sale_payments ), true );
			append_if_not_zero( $reconciliation_summary[], sum_last_item( 'reconciliation_summary', 'refunds_on_deposits_not_yet_redeemed_amount', $open_refund_payments ) );
			append_if_not_zero( $reconciliation_summary[], sum_last_item( 'reconciliation_summary', 'payments_on_voided_orders', $void_sale_payments ), true );
			append_if_not_zero( $reconciliation_summary[], sum_last_item( 'reconciliation_summary', 'refunds_on_voided_orders', $void_refund_payments ) );
			append_if_not_zero( $reconciliation_summary[], sum_last_item( 'reconciliation_summary', 'payment_for_past_orders_amount', $orders_before_sale_payments ), true );
			append_if_not_zero( $reconciliation_summary[], sum_last_item( 'reconciliation_summary', 'refunds_on_payment_for_past_orders_amount', $orders_before_refund_payments ) );
			append_if_not_zero( $reconciliation_summary[], sum_last_item( 'reconciliation_summary', 'deposits_collected_redeemed_in_future_amount', $orders_after_sale_payments ), true );
			append_if_not_zero( $reconciliation_summary[], sum_last_item( 'reconciliation_summary', 'refunds_deposits_collected_redeemed_in_future_amount', $orders_after_refund_payments ) );
			append_if_not_zero( $reconciliation_summary[], sum_last_item( 'reconciliation_summary', 'Gift_Card_Revenue_to_be_Redeemed_in_the_Future', $giftcard_credit_summary), true );
			append_if_not_zero( $reconciliation_summary[], sum_last_item( 'reconciliation_summary', 'Gift_Card_Revenue_from_the_past_Redeemed', $giftcard_past_credit_summary), true );
			append_if_not_zero( $reconciliation_summary[], sum_last_item( 'reconciliation_summary', 'Gift_Card_Revenue_Redeemed_on', $giftcard_revised_credit_summary), true );

			$has_sacoa_cards = $modules->hasModule("extensions.payment.sacoa");
			if($has_sacoa_cards){
				$playcard_credit_summary = array();
				$sacoa_gift_payments = $this->runReport( array(
						REPORTS_V2_SELECTION_COLUMN_KEY => '',
						REPORTS_V2_SPECIAL_CODE_KEY => '',
						REPORTS_V2_SELECTION_KEY => 'Preset Date Ranges',
						REPORTS_V2_TABLE_KEY => 'orders',
						REPORTS_V2_TITLE_KEY => 'Sacoa Gift Redeem',
						REPORTS_V2_NAME_KEY => 'sacoa_gift_redeem',
						REPORTS_V2_ORDERBY_KEY => '',
						REPORTS_V2_SELECT_KEY => implode( REPORTS_V2_FIELD_SEPERATOR,
								array(
										'sum(orders.subtotal) as subtotal'
								)),
						REPORTS_V2_GROUPBY_KEY => "orders.currency_type",
						REPORTS_V2_WHERE_KEY => implode (REPORTS_V2_FIELD_SEPERATOR,
								array(
										"orders.opened >= '[start_datetime]'",
										"orders.closed <= '[end_datetime]'",
										"orders.void = '0'",
										"orders.order_id NOT LIKE '777%'",
										"orders.currency_type = '1'",
										"orders.order_status = 'closed'",
										"orders.location_id = '[locationid]'"
								)),
						REPORTS_V2_JOIN_KEY => '',
						REPORTS_V2_ID_KEY => 'x',
						REPORTS_V2_ORDER_KEY => '',
						REPORTS_V2_DELETED_KEY => '',
				));
				foreach($sacoa_gift_payments as $d){
					$sacoa_gift_payments2[] = $d[0];
				}
				$sacoa_gift_payments = $sacoa_gift_payments2;
				$sacoa_cards_credits = array('sacoa_gift_redemption'=> '', 'summary'=>'Total Points Redeemed', 'sum' => str_replace("[number]", "", $sacoa_gift_payments[0]['subtotal']));
				append_if_not_zero($playcard_credit_summary[], $sacoa_cards_credits);
			}

			$category_totals = sum_all_items($categories);
			$order_info_totals = sum_all_items($order_info);
			$actual_totals = sum_all_items($actual_summary);
			$misc_stats = array();

			if(  report_number_to_float($order_info_totals['guests'])*1 > 0 && report_number_to_float($order_info_totals['num_checks'])*1 > 0 &&  report_number_to_float($order_info_totals['count'])*1 > 0 ){
				$misc_stats = array(
					array(
						'statistic' => 'Average Actual Take In/Guest',
						'average' => report_money_output(report_money_to_number($actual_totals['sum']) / $order_info_totals['guests'])
					),
					array(
						'statistic' => 'Average Actual Take In/Check',
						'average' => report_money_output(report_money_to_number($actual_totals['sum']) / $order_info_totals['num_checks'])
					),
					array(
						'statistic' => 'Average Actual Take In/Order',
						'average' => report_money_output(report_money_to_number($actual_totals['sum']) / $order_info_totals['count'])
					),
					array(
						'statistic' => 'Average Number of Items/Order',
						'average' => report_float_to_number( round( report_number_to_float($category_totals['quantity']) / report_number_to_float($order_info_totals['count']), 2) )
					),
					array(
						'statistic' => 'Average Total/Item',
						'average' => report_money_output(report_money_to_number(report_number_to_float($actual_totals['sum'])) / report_number_to_float($category_totals['quantity']))
					),
					array(
						'statistic' => 'Total Guests',
						'average' => $order_info_totals['guests']
					),
					array(
						'statistic' => 'Total Checks',
						'average' => $order_info_totals['num_checks']
					),
					array(
						'statistic' => 'Total Orders',
						'average' => $order_info_totals['count']
					)
				);
				foreach( $payments_batch_status as $payment_batch_status_rows => $payment_batchs ){
				    foreach( $payment_batchs as $payment_batch_status_row => $payment_batch ){
        					if( report_number_to_float($payment_batch['processed']) == 1 ){
        						$misc_stats[] = array(
        							'statistic' => 'Batched Credit Card Total',
        							'total' => report_money_output( report_money_to_number( $payment_batch['total_collected'] ) + report_money_to_number( $payment_batch['tip_amount'] ) )
        						);
        					} else {
        						$misc_stats[] = array(
        							'statistic' => 'Unbatched Credit Card Total',
        							'total' => report_money_output( report_money_to_number( $payment_batch['total_collected'] ) + report_money_to_number( $payment_batch['tip_amount'] ) )
        						);

        					}
				    }
				}
			}

			return
			array(
				array(
					'title' => 'Expected Take In',
					$super_groups,
					$categories,
					$gratuity,
					$discounts,
					$non_included_taxes,
					$included_taxes,
					$exemptions,
					$rounding_section,
					$redeemption_after_payments,
					$expected_summary
				),
				array(
					'title' => 'Actual Take In',
					$sales_payments_types,
					$sale_payments,
					$tip_information,
					$voided_sale_payments,
					$refunds,
					$voided_refunds_payments,
					$gift_cards_balance_change,
					$voided_gift_cards_balance_change,
					$paid_in_out,
					$deposit_in_out,
					$marked_deposits_collected_sale_payments,
					$marked_deposits_collected_refund_payments,
					$marked_deposits_collected_giftcard_payments,
					$marked_deposits_collected_voided_sale_payments,
					$marked_deposits_collected_voided_refund_payments,
					$marked_deposits_collected_voided_giftcard_payments,
					$marked_deposits_redeemed_sale_payments,
					$marked_deposits_redeemed_refund_payments,
					$marked_deposits_redeemed_giftcard_payments,
					$marked_deposits_redeemed_voided_sale_payments,
					$marked_deposits_redeemed_voided_refund_payments,
					$marked_deposits_redeemed_voided_giftcard_payments,
					$extra_added_payments,
					$actual_summary
				),
				array(
					'title' => 'Reconciliation',
					$difference_summary,
					// $order_payment_mismatch,
					$order_payment_mismatch_payments,
					$before_sale_payments,
					$before_refund_payments,
					$before_giftcard_payments,
					$before_voided_sale_payments,
					$before_voided_refund_payments,
					$before_voided_giftcard_payments,
					$after_sale_payments,
					$after_refund_payments,
					$after_giftcard_payments,
					$after_voided_sale_payments,
					$after_voided_refund_payments,
					$after_voided_giftcard_payments,
					$open_sale_payments,
					$open_refund_payments,
					$open_giftcard_payments,
					$open_voided_sale_payments,
					$open_voided_refund_payments,
					$open_voided_giftcard_payments,
					$void_sale_payments,
					$void_refund_payments,
					$void_giftcard_payments,
					$void_voided_sale_payments,
					$void_voided_refund_payments,
					$void_voided_giftcard_payments,
					$orders_before_sale_payments,
					$orders_before_refund_payments,
					$orders_after_sale_payments,
					$giftcard_credit_summary,
					$giftcard_past_credit_summary,
					$giftcard_revised_credit_summary,
					$reconciliation_summary,
					$playcard_credit_summary
				),
				array(
					'title' => 'Extra',
					$currently_open_orders,
					$orders_per_hour,
					$tip_information_per_server,
					$misc_stats
				)
			);
		}
	}
