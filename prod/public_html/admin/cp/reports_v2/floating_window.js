	var floating_window_parent = false;
	var obj_parent_pos = false;
	var auto_reset_content_when_closed = false;
	var extra_winid = "";
	function open_floating_window(set_title,set_content,set_parent,setwidth,setheight,framename)
	{
		if(!setwidth) setwidth = 480;
		if(!setheight) setheight = 320;
		
		document.getElementById("floating_window_table").style.width = setwidth + "px";
		document.getElementById("floating_window_table").style.height = setheight + "px";
		
		document.getElementById("floating_window_title").innerHTML = set_title;
		document.getElementById("floating_window_content").innerHTML = unescape(set_content);
		document.getElementById("floating_window").style.visibility = "visible";
		
		floating_window_parent = document.getElementById(set_parent);
		obj_parent = get_parent_element("floating_window");
		obj_parent_pos = getObjPosition(obj_parent);
		obj_pos = getElementPosition("floating_window");
						
		width_diff = obj_parent_pos['width'] - obj_pos['width'];
		set_x = width_diff / 2;
		
		document.getElementById("floating_window").style.left = set_x + "px";
		
		if(framename)
		{
			iframe = document.getElementById(framename);
			if (iframe.attachEvent){
				iframe.attachEvent("onload", function(){
					frame_autosize(iframe);
					set_info_location("init");
				});
			} else {
				iframe.onload = function(){
					frame_autosize(iframe);
					set_info_location("init");
				};
			}
		}
		else set_info_location("init");
		//iwinpos = getElementPosition(floating_window_parent.id);
		/*if(opener)
		{
			opener_pos = getObjPosition(opener);
			set_y = (opener_pos['top'] - obj_parent_pos['top']) - obj_pos['height'] / 4;
			document.getElementById("floating_window").style.top = set_y + "px";
		}*/
	}
	function frame_autosize(iframe)
	{
		iframe.width = null;
		iframe.height = null;
		newwidth=iframe.contentWindow.document.body.scrollWidth;
		newheight=iframe.contentWindow.document.body.scrollHeight;
		if(newheight > 400) newheight = 400;
		iframe.width = (newwidth * 1 + 60) + "px";
		iframe.height = (newheight * 1 + 30) + "px";
	}
	
	function open_floating_frame(set_title,set_frame,set_parent,setwidth,setheight)
	{
		if(!setwidth) setwidth = 480;
		if(!setheight) setheight = 320;
		set_frame = set_frame.replace(/\?/ig,"&");
		set_content = "<iframe name='floating_frame' id='floating_frame' style='width:" + setwidth + "px; height:" + setheight + "px;' frameborder='0' src='/cp/index.php?render_frame=" + set_frame + "&rfsep=1'></iframe>";
		open_floating_window(set_title,set_content,set_parent,setwidth,setheight,"floating_frame");
	}
	
	function open_floating_ajax_link(set_title,set_container,set_ajax_link,set_parent,setwidth,setheight)
	{
		var set_content = "<div id='" + set_container + "'>&nbsp;</div>";
		open_floating_window(set_title,set_content,set_parent,setwidth,setheight);
		ajax_register_flexible_container("floating_window_content");
		//ajax_register_flexible_container(set_container);
		ajax_link(set_container,set_ajax_link);
		set_info_location("init");
	}
	
	function close_floating_window()
	{
		document.getElementById("floating_window").style.visibility = "hidden";
		if(auto_reset_content_when_closed) document.getElementById("floating_window_content").innerHTML = " ";
	}
		
	function set_info_location(set_loc_mode)
	{
		if(!set_loc_mode) set_loc_mode = "general";
		
		if(floating_window_exists && floating_window_parent)
		{
			iwinpos = getElementPosition(floating_window_parent.id);
			var dify = (obj_parent_pos['top'] - iwinpos['top']);
			var difx = (obj_parent_pos['left'] - iwinpos['left']);
			var leftx = iwinpos['left'];
			var topy = iwinpos['top'];
			var winWidth = window.innerWidth;
			var widthx = document.getElementById(floating_window_parent.id).offsetWidth;
			var areawidth = document.getElementById("floating_window_table").offsetWidth;
			
			var top = document.body.scrollTop;
			var left = document.body.scrollLeft;
			if(!top) top = document.documentElement.scrollTop;
			if(!left) left = document.documentElement.scrollLeft;
			
		
			if(top > topy)
			{
				gotoy = 0 - dify + (top - topy);
			}
			else
			{
				gotoy = 0 - dify;
			}
						
			
			//gotox = (winWidth / 2) - (areawidth / 2) + left;	
			gotox = 0 - difx + (widthx / 2) - (areawidth / 2) + left;
			
			//document.getElementById("floating_window_title").innerHTML = topy + " " + top + " " + gotoy + " " + document.body.scrollTop;
			//document.getElementById("floating_window_content").innerHTML = "topy: " + topy + "<br>leftx: " + leftx + "<br>widthx: " + widthx + "<br>top: " + top + "<br>gotox: " + gotox + "<br>gotoy: " + gotoy + "<br>areawidth: " + areawidth + "<br>winWidth: " + winWidth + "<br>left: " + left;
			

			//Modified by AMC on August 22, 2013
			//Checks to see if the height of the parent window is less than the height of the floating window
			//There are some mysterious things going on here, but it appears to be work.
			//height (i.e. the clientHeight of the floating window), will be wrong when first loaded, but
			//once you start scrolling it becomes correct. 

			var height = document.getElementById("floating_window").clientHeight;
			var winHeight = window.innerHeight;

			if(document.getElementById("floating_window").style.visibility=="visible")
			{
			     //By comparing window height to the floating window height here, we will ignore the y adjustment
			     // if the winheight is smaller than the height.  However, what I don't understand is how it draws
			     // the floating window at the correct location when it is first loaded?
				 
				 //  Modified by CF on November 23, 2013 
				 //To force it to set the location during initialization, even if the floating window is too large
				 
			     if (winHeight > height || set_loc_mode=="init")
 			     {
					document.getElementById("floating_window").style.top = (gotoy + 20) + "px";
			     }
			     document.getElementById("floating_window").style.left = gotox + "px";
			}
		}
		if(extra_winid!="")
		{
			if(!floating_window_parent)
			{
				floating_window_parent = document.getElementById(extra_winid).parentNode;//document.getElementById("main_area");
				if(!floating_window_parent.id)
					floating_window_parent.id = "floating_window_parent_placeholder";
				//alert("set fwp to main_area - " + floating_window_parent);
			}
			iwinpos = getElementPosition(floating_window_parent.id);
			
			var top = document.body.scrollTop;
			var left = document.body.scrollLeft;
			if(!top) top = document.documentElement.scrollTop;
			if(!left) left = document.documentElement.scrollLeft;
			
			elem = document.getElementById(extra_winid);
			
			//if(top > obj_pos['top'])
			
			//if(top > iwinpos['top'])
				//elem.style.top = ((top - iwinpos['top']) + 20) + "px";
				
			obj_pos = getElementPosition(extra_winid);
			//var diff_pos = (obj_pos['top'] - iwinpos['top']);
			//var min_pos = iwinpos['top'] + diff_pos - 20;
			var rel_pos = document.getElementById(extra_winid).style.top.replace(/px/ig,'');
			var rel_offset = (top - (obj_pos['top'] - rel_pos) + 20);
			if(rel_offset < 0) rel_offset = 0;
					
			elem.style.top = rel_offset + "px";
			/*if(top > min_pos)
			{
				elem.style.top = (top - min_pos) + "px";
			}*/
			
			//document.getElementById('debug_cell').innerHTML = "top: " + obj_pos['top'] + " scroll: " + top + "\nparent top: " + iwinpos['top'] + "\nrelative top: " + (obj_pos['top'] - iwinpos['top']) + "\nrel_pos: " + rel_pos + "\nrel_offset: " + rel_offset;
			//if(obj_pos['top'] < top + 20)
			//	elem.style.top = (top + 20 - iwinpos['top']) + "px";
			
			//if(top > obj_pos['top'])
				//elem.style.top = ((top - obj_pos['top']) + 20) + "px";
			
			//document.getElementById('debug_cell').innerHTML = iwinpos['left'] + " / " + iwinpos['top'] + "\n" + obj_pos['left'] + " / " + obj_pos['top'] + "\n" + top;
			//str = iwinpos['top'] + "/" + iwinpos['left'] + " obj: " + obj_pos['top'] + "/" + obj_pos['left'];
			//alert(str);
			//var dify = (obj_parent_pos['top'] - iwinpos['top']);
			//var difx = (obj_parent_pos['left'] - iwinpos['left']);			
		}
		//document.getElementById("floating_window_title").innerHTML = ("FWE: " + floating_window_exists + " FWP: " + floating_window_parent);
	}
	function register_floating_window(winid)
	{
		extra_winid = winid;
	}
	floating_window_exists = true;
	window.onscroll = set_info_location;
