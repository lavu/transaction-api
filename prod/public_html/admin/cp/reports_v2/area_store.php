<?php
define("INTERNAL_FILE_PREPROCESSOR", "//internal_file:");
class area_store {
	public function __construct(){
		// echo '<pre style="text-align: left;">' . print_r( func_get_args(), true ) . '</pre>';
	}

	public function apply_rules( $vars, $arr, $cmd, $special, $area_reports = null ){
		if( substr($special, 0, strlen( INTERNAL_FILE_PREPROCESSOR ) ) == INTERNAL_FILE_PREPROCESSOR ){
			return $this->internal_file( substr($special, strlen(INTERNAL_FILE_PREPROCESSOR) ), $area_reports );
		}
		return $vars['row'];
	}

	private function internal_file( $file, $area_reports ) {
		require_once dirname(__FILE__) . '/custom_reports/' . str_ireplace("../", "", $file ) . '.php';
		$obj = new $file();
		return $obj->run( $area_reports );
	}
}