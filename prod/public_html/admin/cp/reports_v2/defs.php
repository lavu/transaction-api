<?php
	require_once(dirname(__FILE__) . "/report_alias.php");
	function get_report_join_defs()
	{// FROM => TO
		return <<<REPORT_JOINS
			orders.order_id = order_contents.order_id AND orders.location_id = order_contents.loc_id
			order_contents.item_id = menu_items.id

			menu_items.category_id = menu_categories.id
			menu_categories."IF (menu_items.super_group_id = '', menu_categories.super_group_id, menu_items.super_group_id)" = super_groups.id
			menu_categories.group_id = menu_groups.id

			orders.server_id = users.id
			cash_data.server_id = users.id
			orders.order_id = cc_transactions.order_id AND orders.location_id = cc_transactions.loc_id
			orders.order_id = split_check_details.order_id AND orders.location_id = split_check_details.loc_id
			orders.revenue_center_id = revenue_centers.id
			orders.meal_period_typeid = meal_period_types.id
			orders.togo_status = order_tags.id
			orders.register = config.setting AND 'printer' = config.type AND orders.location_id = config.location AND '0' = config._deleted
            		orders.customer_id = med_customers.id

			cc_transactions.register = config.setting AND 'printer' = config.type AND cc_transactions.loc_id = config.location AND '0' = config._deleted
			cc_transactions.order_id = orders.order_id AND cc_transactions.loc_id = orders.location_id
			cc_transactions.pay_type_id = payment_types.id AND cc_transactions.loc_id = payment_types.loc_id OR '0' = payment_types.loc_id
			cc_transactions.device_udid = devices.UUID AND cc_transactions.loc_id = devices.loc_id
			cc_transactions.loc_id = cash_data.loc_id

			lk_events.id = lk_event_schedules.eventid
			lk_event_schedules.customerid = lk_customers.id
			lk_race_results.eventid = lk_events.id
			lk_race_results.customerid = lk_customers.id
			lk_events.type = lk_event_types.id
			lk_events.trackid = lk_tracks.id
			lk_event_schedules.kartid = lk_karts.id

			ingredients.id = ingredient_usage.ingredientid and ingredients.loc_id = ingredient_usage.loc_id
			ingredient_usage.orderid = order_contents.order_id and ingredient_usage.loc_id = order_contents.loc_id and ingredient_usage.icid = order_contents.icid
			order_contents.order_id = orders.order_id AND order_contents.loc_id = orders.location_id

			action_log.device_udid = devices.UUID AND action_log.loc_id = devices.loc_id
			send_log.device_udid = orders.UUID AND send_log.loc_id = orders.location_id
REPORT_JOINS;
	}

	function get_report_alias_defs()
	{
		global $start_date;
		global $end_date;
		global $start_time;
		global $end_time;
		global $filtermode;
		$cc_transactions_datetime = '';
		$order_closed_datetime = '';
		$action_log_datetime = '';
		$ingredient_usage_datetime = '';
		$changebank_datetime = '';
		$bank_deposit_datetime = '';
		$order_open_datetime = '';
		//Check for Not Empty
		if ($start_date != '' && $end_date != '') {
		//Day Mode
		if ($filtermode == 'day') {
			//If Time Selcted & End Time Greater Than Start Time
			if ($start_time != $end_time && $end_time > $start_time) {
				//cc_transactions_datetime
				$cc_transactions_datetime = "(DATE(cc_transactions.datetime) BETWEEN '$start_date' AND '$end_date') AND (TIME(cc_transactions.datetime) BETWEEN '$start_time' AND '$end_time')";
				//order_closed_datetime
				$order_closed_datetime = "(DATE(orders.closed) BETWEEN '$start_date' AND '$end_date') AND (TIME(orders.closed) BETWEEN '$start_time' AND '$end_time')";
				//action_log_datetime
				$action_log_datetime = "(DATE(action_log.time) BETWEEN '$start_date' AND '$end_date') AND (TIME(action_log.time) BETWEEN '$start_time' AND '$end_time')";
				//ingredient_usage_datetime
				$ingredient_usage_datetime = "(DATE(ingredient_usage.date) BETWEEN '$start_date' AND '$end_date') AND (TIME(ingredient_usage.date) BETWEEN '$start_time' AND '$end_time')";
				//changebank_datetime
				$changebank_datetime = "(DATE(change_bank.created_date) BETWEEN '$start_date' AND '$end_date') AND (TIME(change_bank.created_date) BETWEEN '$start_time' AND '$end_time')";
				//bank_deposit_datetime
				$bank_deposit_datetime = "(DATE(bank_deposit.created_date) BETWEEN '$start_date' AND '$end_date') AND (TIME(bank_deposit.created_date) BETWEEN '$start_time' AND '$end_time')";
				//order_open_datetime orders.opened
				$order_open_datetime = "(DATE(orders.opened) BETWEEN '$start_date' AND '$end_date') AND (TIME(orders.opened) BETWEEN '$start_time' AND '$end_time')";
			} else {
				//If Time Not Selected
				//cc_transactions_datetime
				$cc_transactions_datetime = "cc_transactions.datetime >= '[start_datetime]' and cc_transactions.datetime <= '[end_datetime]'";
				//order_closed_datetime
				$order_closed_datetime = "orders.closed >= '[start_datetime]' AND orders.closed < '[end_datetime]'";
				//action_log_datetime
				$action_log_datetime = "action_log.time >= '[start_datetime]' AND action_log.time <= '[end_datetime]'";
				//ingredient_usage_datetime
				$ingredient_usage_datetime = "ingredient_usage.date >= '[start_datetime]' and ingredient_usage.date < '[end_datetime]'";
				//changebank_datetime change_bank.created_date
				$changebank_datetime = "change_bank.created_date >= '[start_datetime]' and change_bank.created_date <= '[end_datetime]'";
				//bank_deposit_datetime
				$bank_deposit_datetime = "bank_deposit.created_date >= '[start_datetime]' and bank_deposit.created_date <= '[end_datetime]'";
				//order_open_datetime orders.opened
				$order_open_datetime = "orders.opened >= '[start_datetime]' and orders.opened < '[end_datetime]'";
			}
		} else {
				//Month & Year Mode
				//cc_transactions_datetime
				$cc_transactions_datetime = "cc_transactions.datetime >= '[start_datetime]' and cc_transactions.datetime <= '[end_datetime]'";
				//order_closed_datetime
				$order_closed_datetime = "orders.closed >= '[start_datetime]' AND orders.closed < '[end_datetime]'";
				//action_log_datetime
				$action_log_datetime = "action_log.time >= '[start_datetime]' AND action_log.time <= '[end_datetime]'";
				//ingredient_usage_datetime
				$ingredient_usage_datetime = "ingredient_usage.date >= '[start_datetime]' and ingredient_usage.date < '[end_datetime]'";
				//changebank_datetime change_bank.created_date
				$changebank_datetime = "change_bank.created_date >= '[start_datetime]' and change_bank.created_date <= '[end_datetime]'";
				//bank_deposit_datetime
				$bank_deposit_datetime = "bank_deposit.created_date >= '[start_datetime]' and bank_deposit.created_date <= '[end_datetime]'";
				//order_open_datetime orders.opened
				$order_open_datetime = "orders.opened >= '[start_datetime]' and orders.opened < '[end_datetime]'";
		}
		}

		$res = '';
		$res .= <<<ORDER_WHERES
		    where:zreport_dates {
		        fiscal_reports.datetime >= '[start_datetime]' and
		        fiscal_reports.datetime <= '[end_datetime]' and
		        fiscal_reports.report_id != ''}

			where:orders_placed_active { $order_closed_datetime and orders.currency_type = '0' and
												orders.void = '0' as non-voided }

			where:orders_placed_before { orders.closed < '[start_datetime]' and
												orders.closed != '0000-00-00 00:00:00' and
												orders.closed != '' and
												orders.closed != '0' and
												orders.void = '0' }

			where:orders_placed_after { orders.closed >= '[end_datetime]' and
												orders.void = '0' }

			where:orders_placed_void { $order_closed_datetime and
												orders.void = '1' as voided }

			where:orders_placed_open { ( orders.closed = '0000-00-00 00:00:00' or
												orders.closed = '' or
												orders.closed = '0' ) and
												orders.void = '0' }
			where:orders_opened_active { $order_open_datetime and
												orders.void = '0' }

			where:order_contents_order_active { orders.void ='0' and
												order_contents.device_time >= '[start_datetime]' and
												order_contents.device_time < '[start_datetime]' as item_added_time_non-voided}

			where:order_contents_order_void { orders.void ='1' and
												order_contents.device_time >= '[start_datetime]' and
												order_contents.device_time < '[start_datetime]' as item_added_time_voided }

			where:exclude_sendpoints { order_contents.item != 'SENDPOINT' }
		
			where:orders_opened_closed { orders.opened >= '[start_datetime]' and
												orders.closed <= '[end_datetime]' and
												orders.void = '0' and orders.order_id NOT LIKE '777%'
												and orders.closed NOT LIKE '%00%' }

			where:active_cc_transactions { cc_transactions.datetime >= '[start_datetime]'
									and cc_transactions.datetime < '[end_datetime]'
									and cc_transactions.action LIKE "Sale"
									and cc_transactions.pay_type = "Cash"
									and cc_transactions.voided = 0
									and orders.void = '0' as non-voided }

		    where:changeBankReportDates {
		        $changebank_datetime }

			where:depositBankReportDates {
		        $bank_deposit_datetime }

			where:overpaid_orders { ROUND((orders.cash_paid + orders.card_paid + orders.gift_certificate + ( orders.alt_paid - orders.alt_refunded) ),3) > orders.total }
			where:underpaid_orders { ROUND((orders.cash_paid + orders.card_paid	 + orders.gift_certificate + ( orders.alt_paid - orders.alt_refunded)),3) < orders.total }

			where:lavu_gift_active { lavu_gift_cards.created_datetime >= '[start_datetime]'
				and lavu_gift_cards.created_datetime <= '[end_datetime]' }
							
			where:kiosk_orders { orders.order_id LIKE '5-%' }
            
            where:lavu_togo_orders { orders.order_id LIKE '2-%' }

ORDER_WHERES;
		$res .= <<<TRANSACTION_WHERES
			where:giftcards_transactions_all { $cc_transactions_datetime and
												cc_transactions.action != '' }

			where:transactions_occurred_all { $cc_transactions_datetime and
												cc_transactions.action != '' and
												cc_transactions.action NOT LIKE '%issue%' and
												cc_transactions.action NOT LIKE '%reload%' and
												cc_transactions.process_data NOT LIKE 'LOCT%' }

			where:transactions_occurred_after { cc_transactions.datetime >= '[end_datetime]' and
												cc_transactions.action != '' and
												cc_transactions.action NOT LIKE '%issue%' and
												cc_transactions.action NOT LIKE '%reload%' and
												cc_transactions.process_data NOT LIKE 'LOCT%' }

			where:transactions_occurred_before { cc_transactions.datetime < '[start_datetime]' and
												cc_transactions.action != '' and
												cc_transactions.action NOT LIKE '%issue%' and
												cc_transactions.action NOT LIKE '%reload%' and
												cc_transactions.process_data NOT LIKE 'LOCT%' }

			where:transactions_occurred_with_offline_with_active { cc_transactions.datetime >= '[start_datetime]' and
															cc_transactions.datetime <= '[end_datetime]' and
															cc_transactions.voided = '0' and
															cc_transactions.action != 'Void'  and
															cc_transactions.action NOT LIKE '%issue%' and
															cc_transactions.action NOT LIKE '%reload%' and
															cc_transactions.action != '' as non-voided }

			where:transactions_occurred_active { $cc_transactions_datetime and
																	cc_transactions.voided = '0' and
																	cc_transactions.action != 'Void'  and
																	cc_transactions.action NOT LIKE '%issue%' and
																	cc_transactions.action NOT LIKE '%reload%' and
																	cc_transactions.process_data NOT LIKE 'LOCT%' and
																	cc_transactions.action != '' as non-voided }

			where:transactions_occurred_voided { cc_transactions.datetime >= '[start_datetime]' and
																	cc_transactions.datetime <= '[end_datetime]' and
																	cc_transactions.voided = '1' and
																	cc_transactions.action != 'Void'  and
																	cc_transactions.action NOT LIKE '%issue%' and
																	cc_transactions.action NOT LIKE '%reload%' and
																	cc_transactions.process_data NOT LIKE 'LOCT%' and
																	cc_transactions.action != '' as voided }

			where:transactions_reconcilliation_deposits_from_past { orders.closed >= '[start_datetime]' and
																	orders.closed < '[end_datetime]' and
																	orders.void = '0' and
																	cc_transactions.datetime < '[start_datetime]' and
																	cc_transactions.order_id not like 'Paid%'  and
																	cc_transactions.process_data NOT LIKE 'LOCT%' and
																	cc_transactions.action NOT LIKE '%issue%' and
																	cc_transactions.action NOT LIKE '%reload%' and
																	cc_transactions.action != '' as past_transactions }

			where:transactions_reconcilliation_from_future { orders.closed >= '[start_datetime]' and
																	orders.closed < '[end_datetime]' and
																	orders.void = '0' and
																	cc_transactions.datetime >= '[end_datetime]' and
																	cc_transactions.order_id not like 'Paid%'  and
																	cc_transactions.process_data NOT LIKE 'LOCT%' and
																	cc_transactions.action NOT LIKE '%issue%' and
																	cc_transactions.action NOT LIKE '%reload%' and
																	cc_transactions.action != '' as future_transactions }

			where:transactions_reconcilliation_from_open_orders { ( orders.closed = '0000-00-00 00:00:00' or
																	orders.closed = '' or
																	orders.closed = '0' ) and
																	orders.void = '0' and
																	cc_transactions.datetime >= '[start_datetime]' and
																	cc_transactions.datetime <= '[end_datetime]' and
																	cc_transactions.voided = '0' and
																	cc_transactions.order_id not like 'Paid%'  and
																	cc_transactions.process_data NOT LIKE 'LOCT%' and
																	cc_transactions.action NOT LIKE '%issue%' and
																	cc_transactions.action NOT LIKE '%reload%' and
																	cc_transactions.action != '' as open_orders }

			where:transactions_reconcilliation_from_past_on_current { orders.closed < '[start_datetime]' and
																	orders.closed != '0000-00-00 00:00:00' and
																	orders.closed != '' and
																	orders.closed != '0' and
																	orders.void = '0' and
																	cc_transactions.datetime >= '[start_datetime]' and
																	cc_transactions.datetime <= '[end_datetime]' and
																	cc_transactions.voided = '0' and
																	cc_transactions.order_id not like 'Paid%'  and
																	cc_transactions.process_data NOT LIKE 'LOCT%' and
																	cc_transactions.action NOT LIKE '%issue%' and
																	cc_transactions.action NOT LIKE '%reload%' and
																	cc_transactions.action != '' as past_orders }

			where:transactions_reconcilliation_from_future_on_current { orders.closed >= '[end_datetime]' and
																	orders.void = '0' and
																	cc_transactions.datetime >= '[start_datetime]' and
																	cc_transactions.datetime <= '[end_datetime]' and
																	cc_transactions.voided = '0' and
																	cc_transactions.action != '' and
																	cc_transactions.action NOT LIKE '%issue%' and
																	cc_transactions.action NOT LIKE '%reload%' and
																	cc_transactions.process_data NOT LIKE 'LOCT%' and
																	cc_transactions.order_id not like 'Paid%' as future_orders}
TRANSACTION_WHERES;
		$res .= <<<LAVU_KART_WHERES
			where:race_ran { lk_events.ms_start >= '[start_datetime]' and
																	lk_events.ms_start <= '[end_datetime]' }
LAVU_KART_WHERES;
		$res .= <<<ACTION_LOG_WHERES
			where:action_log_active { $action_log_datetime as active}
ACTION_LOG_WHERES;
		$res .= <<<TAXES_WHERES
			where:taxes_exempt_voided { $order_closed_datetime and
									orders.void = '1' and
									order_contents.tax_exempt = '1' as exempt_voided }

			where:taxes_exempt_non_voided { $order_closed_datetime and
									orders.void = '0' and
									order_contents.tax_exempt = '1' as exempt_non-voided }

			where:taxes_non_exempt_voided { $order_closed_datetime and
									orders.void = '1' and
									order_contents.tax_exempt = '0' as non-exempt_voided }

			where:taxes_non_exempt_non_voided { $order_closed_datetime and
									orders.void = '0' and
									order_contents.tax_exempt = '0' as non-exempt_non-voided }
TAXES_WHERES;
		$res .= <<<INGREDIENT_WHERE_FIELDS
			where:ingredient_usage_occurred { $ingredient_usage_datetime and
									ingredient_usage.qty*1 >= 0 as inventory_used }
			where:ingredient_addition_occurred { $ingredient_usage_datetime and
									ingredient_usage.qty*1 < 0 as inventory_gained }
INGREDIENT_WHERE_FIELDS;
		$res .= <<<GENERAL_WHERE_FIELDS
			where:idiscount_has_amount { order_contents.idiscount_amount*1 != 0 and
										order_contents.quantity*1 > 0 }
			where:discount_has_amount { split_check_details.discount*1 != 0 }

			where:tax1_non_zero_amount { order_contents.tax1 > 0 AND `quantity` > 0 }
			where:tax2_non_zero_amount { order_contents.tax2 > 0 AND `quantity` > 0 }
			where:tax3_non_zero_amount { order_contents.tax3 > 0 AND `quantity` > 0 }
			where:tax4_non_zero_amount { order_contents.tax4 > 0 AND `quantity` > 0 }
			where:tax5_non_zero_amount { order_contents.tax5 > 0 AND `quantity` > 0 }
GENERAL_WHERE_FIELDS;

		$res .= <<<CASH_DATA_WHERES
			where:cash_data_active { CONCAT( cash_data.date, ' [day_start_end_time]') >= '[start_datetime]' AND
									CONCAT( cash_data.date, ' [day_start_end_time]') < '[end_datetime]' AND
									cash_data.type = 'server_cash_tips' as non-voided }
CASH_DATA_WHERES;
		$res .= <<<GENERAL_FIELDS
			all:taxes_exemption_status { if( order_contents.tax_exempt = '1', 'Exempt', 'Not Exempt') as tax_exempt }
			all:tax_exemption_reason { if( order_contents.tax_exempt = '1', order_contents.exemption_name, '') as exemption_reason }
			all:taxes_inclusion_status { if( order_contents.tax_inclusion = '1', 'Included', 'Not Included') as tax_inclusion }

			all:menu_item_id { menu_items.id }
			all:item_name { menu_items.name as item_name }

			all:rev_center { revenue_centers.name as revenue_center }
			all:meal_period { meal_period_types.name as meal_period }
			all:super_group { if(super_groups.title != "", super_groups.title, 'No Super Group' ) as super_group }
			all:menu_group { menu_groups.group_name as menu_group }
			all:menu_category_name { menu_categories.name as category }

			all:overpaid_order{ orders.order_id as overpaid_order }
			all:server { orders.server as server }
			all:closer_by_name { orders.cashier as closed_by }
			all:overpaid_amount { sum_price( (orders.cash_paid + orders.card_paid + orders.gift_certificate + ( orders.alt_paid - orders.alt_refunded ) ) - orders.total) as overpaid_amount }
			all:underpaid_amount { sum_price(orders.total - (orders.cash_paid + orders.card_paid + orders.gift_certificate + ( orders.alt_paid - orders.alt_refunded ) ) ) as underpaid_amount }
			all:underpaid_order{ orders.order_id as underpaid_order }

			all:combo_sales { menu_items.name as combo_sales }
			all:order_cashier { concat(orders.cashier, " (id:", orders.cashier_id, ")") as order_cashier }
			all:order_server { concat(orders.server, " (id:", orders.server_id, ")") as order_server }
			all:order_id { orders.order_id as order_id }
			all:kiosk_order { orders.order_id as kiosk_order }
            all:lavu_togo_order { orders.order_id as lavu_togo_order }
			all:device_name { orders.server as device_name }
			all:order_count { if(count(distinct orders.ioid )>count( distinct orders.order_id ),count( distinct orders.ioid ),count( distinct orders.order_id )) } as order_count }

			all:order_reg_group { config.value as register_group }
			all:order_time { orders.order_id as order_time }
			all:closed_by_hour { concat('time(h:ia)',date_format(orders.closed, '%H:00')) as order_closed_hour }
			all:closed_by_day { concat('time(#day)',date_format(DATE_SUB( orders.closed, INTERVAL '[day_start_end_time]' HOUR_MINUTE), '%m-%d')) as order_closed_day }
			all:closed_by_weekday { concat('time(#weekday)',date_format(DATE_SUB( orders.closed, INTERVAL '[day_start_end_time]' HOUR_MINUTE), '%W')) as order_closed_weekday }
			all:closed_by_week_for_year { concat('time(#weekforyear)',lpad(date_format(DATE_SUB( orders.closed, INTERVAL '[day_start_end_time]' HOUR_MINUTE), '%U')+1,2,'0')) as order_closed_week_of_year }
			all:closed_by_month { concat('time(#month)',date_format(DATE_SUB( orders.closed, INTERVAL '[day_start_end_time]' HOUR_MINUTE), '%M')) as order_closed_month }
			all:closed_by_year { concat('time(#year)',date_format(DATE_SUB( orders.closed, INTERVAL '[day_start_end_time]' HOUR_MINUTE), '%Y')) as order_closed_year }
			all:closed_actual_date_hour { concat('time(Y-m-d H:i:s)',date_format(orders.closed, '%Y-%m-%d %H:00:00')) as order_closed_actual_date_and_hour }

			all:item_added_by_hour { concat('time(h:ia)',date_format(order_contents.device_time, '%H:00')) as item_added_hour }
			all:item_added_by_day { concat('time(#day)',date_format(DATE_SUB( order_contents.device_time, INTERVAL '[day_start_end_time]' HOUR_MINUTE), '%m-%d')) as item_added_day }
			all:item_added_by_weekday { concat('time(#weekday)',date_format(DATE_SUB( order_contents.device_time, INTERVAL '[day_start_end_time]' HOUR_MINUTE), '%W')) as item_added_weekday }
			all:item_added_by_week_for_year { concat('time(#weekforyear)',lpad(date_format(DATE_SUB( order_contents.device_time, INTERVAL '[day_start_end_time]' HOUR_MINUTE), '%U')+1,2,'0')) as item_added_week_of_year }
			all:item_added_by_month { concat('time(#month)',date_format(DATE_SUB( order_contents.device_time, INTERVAL '[day_start_end_time]' HOUR_MINUTE), '%M')) as item_added_month }
			all:item_added_by_year { concat('time(#year)',date_format(DATE_SUB( order_contents.device_time, INTERVAL '[day_start_end_time]' HOUR_MINUTE), '%Y')) as item_added_year }
			all:item_added_actual_date_hour { concat('time(Y-m-d H:i:s)',date_format(order_contents.device_time, '%Y-%m-%d %H:00:00')) as item_added_actual_date_and_hour }

			all:opened_actual_date_hour { concat('time(Y-m-d H:i:s)',date_format(orders.opened, '%Y-%m-%d %H:00:00')) as actual_opened_date_and_hour }
				
			all:opened { orders.opened as opened }
			all:closed { orders.closed as closed }
			all:time_elapsed { TRIM(TRAILING '.000000' from TIMEDIFF(orders.closed, orders.opened)) as time_elapsed }

			all:sum_guests { sum_rounded( orders.guests ) as guests }
			all:sum_checks { sum_rounded( orders.no_of_checks ) as number_of_checks }

			all:transaction_payment_type { cc_transactions.pay_type_id }
			all:transaction_cash_drawer { cc_transactions.drawer }
			all:transaction_payment_register { CONCAT(cc_transactions.register_name, CHAR(30), cc_transactions.register) as register }

			all:order_register { CONCAT(config.value2, CHAR(30), config.setting) as register }
			all:card_type { cc_transactions.card_type as card_type }
			all:trans_occur_by_hour { concat('time(h:ia)',date_format(cc_transactions.datetime, '%H:00')) as hour }
			all:trans_occur_by_day { concat('time(#day)',date_format(DATE_SUB( cc_transactions.datetime, INTERVAL '[day_start_end_time]' HOUR_MINUTE), '%m-%d')) as day }
			all:trans_occur_by_weekday { concat('time(#weekday)',date_format(DATE_SUB( cc_transactions.datetime, INTERVAL '[day_start_end_time]' HOUR_MINUTE), '%W')) as weekday }
			all:trans_occur_by_week_of_year { concat('time(#weekforyear)',lpad(date_format(DATE_SUB( cc_transactions.datetime, INTERVAL '[day_start_end_time]' HOUR_MINUTE), '%U')+1,2,'0')) as week_of_year }
			all:trans_occur_by_month { concat('time(#month)',date_format(DATE_SUB( cc_transactions.datetime, INTERVAL '[day_start_end_time]' HOUR_MINUTE), '%M')) as month }
			all:trans_occur_by_year { concat('time(#year)',date_format(DATE_SUB( cc_transactions.datetime, INTERVAL '[day_start_end_time]' HOUR_MINUTE), '%Y')) as year }
			all:trans_occur_actual_date_hour { concat('time(Y-m-d H:i:s)',date_format(cc_transactions.datetime, '%Y-%m-%d %H:00:00')) as actual_date_and_hour }
			all:transaction_action { cc_transactions.action as action }
			all:payment_id { cc_transactions.id as payment_id }
			all:payment_order_id { cc_transactions.order_id as order_id }

			all:action_log_order_id { if( action_log.order_id = '0', '', action_log.order_id) as order_id }
			all:action_action { if( locate(' - ', action_log.action) = 0, action_log.action, substr(action_log.action, 1, locate(' - ', action_log.action))) as action }
			all:action_details { if(action_log.details = '' and locate(' - ', action_log.action) != 0, substr(action_log.action, locate(' - ', action_log.action)+3), action_log.details) as details }

			all:action_device_uuid { CONCAT(devices.name, CHAR(30), devices.uuid) as device }
			all:action_device_udid { CONCAT(devices.name, CHAR(30), devices.uuid) as device }
			all:action_user { action_log.user as user }
			all:action_time { action_log.time as time }

			all:order_tag { if( order_tags.name is NULL, '[default_order_tag]', order_tags.name ) as order_type }

			all:idiscount_type { if(order_contents.idiscount_id = '1', 'Entered Percentage Discount', if( order_contents.idiscount_id = '2', 'Entered Cash Discount', order_contents.idiscount_sh) ) as discount_type }
			all:idiscount_sh { order_contents.idiscount_sh as discount_name }
			all:idiscount_amount_single { order_contents.idiscount_amount as discount }
			all:idiscount_amount { sum_price( order_contents.idiscount_amount ) as discount }
			all:idiscount_reason { order_contents.idiscount_info as discount_reason }
			all:discount_sh { split_check_details.discount_sh as discount_name }
			all:discount_type { if(split_check_details.discount_id = '1', 'Entered Percentage Discount', if( split_check_details.discount_id = '2', 'Entered Cash Discount', split_check_details.discount_sh) ) as discount_type }
			all:discount_amount_single { split_check_details.discount as discount }
			all:discount_amount { sum_price( split_check_details.discount ) as discount }
			all:discount_reason { split_check_details.discount_info }

			all:contents_name { order_contents.item as order_item }
			all:content_options { order_contents.options as content_options }
			all:order_included_tax { sum_price( if( order_contents.quantity != 0 AND order_contents.tax_exempt != '1', `order_contents`.`itax`, 0) ) as included_tax }
			all:order_tax { sum_price( if( order_contents.quantity != 0 AND order_contents.tax_exempt != '1', `order_contents`.`tax1` + `order_contents`.`tax2` + `order_contents`.`tax3` + `order_contents`.`tax4` + `order_contents`.`tax5` - `order_contents`.`itax`, 0 ) ) as order_tax }

			all:tax1_name { order_contents.tax_name1 as tax }
			all:tax2_name { order_contents.tax_name2 as tax }
			all:tax3_name { order_contents.tax_name3 as tax }
			all:tax4_name { order_contents.tax_name4 as tax }
			all:tax5_name { order_contents.tax_name5 as tax }
			all:tax1_rate { concat(format(order_contents.tax_rate1*100,2),'%') as rate }
			all:tax2_rate { concat(format(order_contents.tax_rate2*100,2),'%') as rate }
			all:tax3_rate { concat(format(order_contents.tax_rate3*100,2),'%') as rate }
			all:tax4_rate { concat(format(order_contents.tax_rate4*100,2),'%') as rate }
			all:tax5_rate { concat(format(order_contents.tax_rate5*100,2),'%') as rate }
			all:tax1_subtotal { sum_price(order_contents.tax_subtotal1) as taxed_amount }
			all:tax2_subtotal { sum_price(order_contents.tax_subtotal2) as taxed_amount }
			all:tax3_subtotal { sum_price(order_contents.tax_subtotal3) as taxed_amount }
			all:tax4_subtotal { sum_price(order_contents.tax_subtotal4) as taxed_amount }
			all:tax5_subtotal { sum_price(order_contents.tax_subtotal5) as taxed_amount }
			all:tax1_amount { sum_price(order_contents.tax1) as tax_total }
			all:tax2_amount { sum_price(order_contents.tax2) as tax_total }
			all:tax3_amount { sum_price(order_contents.tax3) as tax_total }
			all:tax4_amount { sum_price(order_contents.tax4) as tax_total }
			all:tax5_amount { sum_price(order_contents.tax5) as tax_total }

			all:track_name { lk_tracks.title as track_name }
			all:event_type { lk_event_types.title as event_type }
			all:race_scheduled { concat(lk_events.date,' ',lpad(((lk_events.time * 1) - MOD(lk_events.time,100)) / 100,2,'0'),':',lpad(MOD(lk_events.time,100),2,'0'),':00') }
			all:races_ran_by_hour { concat('time(h:ia)',date_format(lk_events.ms_start, '%H:00')) as race_hour }
			all:races_by_hour { concat('time(h:ia)',date_format(concat(lk_events.date,' ',lpad(((lk_events.time * 1) - MOD(lk_events.time,100)) / 100,2,'0'),':',lpad(MOD(lk_events.time,100),2,'0'),':00'), '%H:00')) as race_hour }
			all:kart_customer_name { concat(lk_customers.f_name,' ',lk_customers.l_name) as customer_name }
			all:kart_number { concat('kart ',lk_karts.number) as kart_number }

			all:ingredient_cost { ingredients.cost as cost }
			all:ingredient_title { ingredients.title as ingredient_name }
			all:ingredient_quantity { ingredients.qty as unit_quantity }
			all:ingredient_unit { ingredients.unit as unit }
			all:all_ingredients { 1 as all_ingredients }
			all:discount_types { 1 as discount_types }
			all:discount_list { 1 as discount_list }

			all:unique_item_discount { concat(orders.order_id,'I',order_contents.id) }
			all:unique_check_discount { concat(orders.order_id,'C',split_check_details.id) }

			all:ingredient_usage_total_quantity{ ingredients.qty as current_inventory }
			all:ingredient_usage_quantity { sum(ingredient_usage.qty) as usage_quantity }
			all:ingredient_usage_order_id { ingredient_usage.orderid as order_id }

			all:ingredient_usage_par { ingredients.high as par }
			all:ingredient_usage_short { IF(ingredients.qty < ingredients.high, ingredients.high - ingredients.qty, 0) as short }

			all:cash_sales { cc_transactions.datetime as cash_sales }
			all:total_collected { cc_transactions.total_collected as sales_total_collected }
			all:gratuity { orders.gratuity as gratuity }

			all:dateTime { change_bank.created_date as date_time }
			all:changeBankAmount { CONCAT('[money]', change_bank.amount) as change_bank_amount }
			all:firstName { users.f_name as first_name }
			all:lastName { users.l_name as last_name }
			all:userName { users.username as username }

			all:depositDateTime { bank_deposit.created_date as date_time }
			all:depositAmount { CONCAT('[money]', bank_deposit.amount) as entered_deposit_amount }
			all:depositFirstName { users.f_name as first_name }
			all:depositLastName { users.l_name as last_name }
			all:depositUserName { users.username as username }
			all:lavuCalculatedAmount { CONCAT('[money]', bank_deposit.required_deposit) as lavu_calculated_deposit_amount }
            		all:customer_name { CONCAT(med_customers.f_name, ' ', med_customers.l_name) as customer_name }
GENERAL_FIELDS;
		$res .= <<<SELECT_FIELDS
			select:sum_amount_collected { sum_price( IF(cc_transactions.action='Refund', -1*cc_transactions.total_collected, cc_transactions.total_collected) ) as total_payment }
			select:number_of_transactions { COUNT(cc_transactions.id ) as transactions }
			select:amount_collected { cc_transactions.total_collected as payment_amount_collected }
			select:sum_tip_amount { sum_price( IF(cc_transactions.action='Refund', -1*cc_transactions.tip_amount, cc_transactions.tip_amount) ) as total_tip }

			select:cash_tip_number_of_transactions { COUNT(*)*0 as transactions }
			select:cash_tip_amount_collected { COUNT(*)*0 as payment_amount_collected }
			select:sum_cash_tip_amount { sum_price(cash_data.value) as total_tip }
			select:cash_data_server { CONCAT( users.f_name, ' ', users.l_name ) as order_server }

			select:contents_price { order_contents.price as item_price }
			select:contents_quantity { sum(order_contents.quantity) as quantity }
			select:sum_contents_unit_price { sum_price(order_contents.price * order_contents.quantity ) as price }
			select:sum_contents_price { sum_price((order_contents.price + order_contents.modify_price + order_contents.forced_modifiers_price)*order_contents.quantity) as price }
			select:gross_contents { sum_price((order_contents.price + order_contents.modify_price + order_contents.forced_modifiers_price)*order_contents.quantity - if( order_contents.quantity > '0', order_contents.itax, 0) ) as gross }
			select:net_contents { sum_price((order_contents.price + order_contents.modify_price + order_contents.forced_modifiers_price)*order_contents.quantity - if( order_contents.quantity > '0',if(order_contents.discount_amount<((order_contents.price + order_contents.modify_price + order_contents.forced_modifiers_price)*order_contents.quantity),order_contents.discount_amount,(order_contents.price + order_contents.modify_price + order_contents.forced_modifiers_price)*order_contents.quantity)+ order_contents.idiscount_amount + order_contents.itax, 0)) as net }
			select:sum_contents_discount { sum_price(if(order_contents.quantity > 0, order_contents.discount_amount + order_contents.idiscount_amount, 0)) as discount }
			select:sum_contents_quantity { sum(order_contents.quantity) as quantity }

			select:payment_type_name { payment_types.type as payment_type }
			select:cash_drawer { cc_transactions.drawer as cash_drawer }

			select:count { count(*) as count }
			select:order_total { sum_price((order_contents.price + order_contents.modify_price + order_contents.forced_modifiers_price)*order_contents.quantity - if(order_contents.quantity > '0',order_contents.discount_amount + order_contents.idiscount_amount + order_contents.itax, 0) + if(order_contents.quantity != 0 AND order_contents.tax_exempt != '1', `order_contents`.`tax1` + `order_contents`.`tax2` + `order_contents`.`tax3` + `order_contents`.`tax4` + `order_contents`.`tax5` - `order_contents`.`itax`, 0) + if(order_contents.quantity != 0 AND order_contents.tax_exempt != '1', `order_contents`.`itax`, 0)) as total }
SELECT_FIELDS;
		$res .= <<<ORDER_BY_FIELDS
			orderby:sum_tip_amount { cc_transactions.tip_amount }
			orderby:amount_collected { cc_transactions.total_collected }

			orderby:number_of_transactions { }
			orderby:order_id { orders.order_id }

ORDER_BY_FIELDS;

		return $res;

}
