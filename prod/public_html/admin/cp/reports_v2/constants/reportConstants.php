<?php
    // reports type
    define('TYPE_LAVU_GIFT_AND_LOYALITY', 'lavu_gift_and_loyalty');
    define('TYPE_CHANGE_BANK_REPORT', 'changeBankReport');
    define('TYPE_DEPOSIT_BANK_REPORT', 'depositBankReport');
    define('TYPE_FISCAL_Z_RECONCILIATION', 'FiscalZReconciliation');
    // Pagination types
    define('PAGINATION_LAVU_GIFT_AND_LOYALTY', 'lavu_gift_and_loyality_pagination');
    // Reports output modes
    define('OUTPUT_MODE_CSV_TO_EMAIL', 'export-csv-to-email');
    define('OUTPUT_MODE_JSON', 'json');
    define('OUTPUT_MODE_CSV', 'csv');
?>