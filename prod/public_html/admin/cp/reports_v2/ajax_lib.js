ajax_callback_function = "ajax_callback";
extra_callback_function = "";
ajax_render_id = "";

function xmlhttpPost(strURL,querystr) 
{
	var xmlHttpReq = false;
	var self = this;
	// Mozilla/Safari
	if (window.XMLHttpRequest) 
	{
		self.xmlHttpReq = new XMLHttpRequest();
	}
	// IE
	else if (window.ActiveXObject) 
	{
		self.xmlHttpReq = new ActiveXObject("Microsoft.XMLHTTP");
	}
	self.xmlHttpReq.open('POST', strURL, true);
	self.xmlHttpReq.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
	self.xmlHttpReq.onreadystatechange = function() 
	{
		if (self.xmlHttpReq.readyState == 4) 
		{
			process_ajax_callback(self.xmlHttpReq.responseText, strURL);
		}
	}
	self.xmlHttpReq.send(querystr);
}

function make_ajax_call(strURL,querystr,callback) 
{
	var xmlHttpReq = false;
	var self = this;
	// Mozilla/Safari
	if (window.XMLHttpRequest) 
	{
		self.xmlHttpReq = new XMLHttpRequest();
	}
	// IE
	else if (window.ActiveXObject) 
	{
		self.xmlHttpReq = new ActiveXObject("Microsoft.XMLHTTP");
	}
	self.xmlHttpReq.open('POST', strURL, true);
	self.xmlHttpReq.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
	self.xmlHttpReq.onreadystatechange = function() 
	{
		if (self.xmlHttpReq.readyState == 4) 
		{
			eval(callback + "(parse_ajax_response(self.xmlHttpReq.responseText))");
			//process_ajax_callback(self.xmlHttpReq.responseText);
		}
	}
	self.xmlHttpReq.send(querystr);
}

aj_flexible_containers = new Array();
function ajax_register_flexible_container(container_id)
{
	aj_flexible_containers[container_id] = container_id;
}

function parse_ajax_response(str)
{
	var rsp = new Array();
	rsp['success'] = "0";
	var str_parts = str.split("&");
	for(var i=0; i<str_parts.length; i++)
	{
		var var_parts = str_parts[i].split("=");
		if(var_parts.length > 1)
			rsp[var_parts[0]] = var_parts[1];
	}
	return rsp;
}

function process_ajax_callback(str, queryString) {
	var sep = "<!--" + "AJAX" + " " + "RESPONSE" + "-->";
	if(str.indexOf(sep) >= 0)
		start_pos = str.indexOf(sep) + sep.length;
	else
		start_pos = 0;
	eval(ajax_callback_function + "(str.substring(start_pos))");
	
	if (extra_callback_function) {
		if(Array.isArray(extra_callback_function)) {
			try {
				var i = 0;
				var callbackLength = extra_callback_function.length;
				for ( i; i < callbackLength;i++){
					eval(extra_callback_function[i] + '(queryString, str)');
				}
			} catch(error) {
				console.log(error);
			}
		} else {
			eval(extra_callback_function + "(queryString, str)");
		}
	}
}

function get_ajax_response(render_area,post_info)
{
	xmlhttpPost("?widget=reports/v2_entry&render_frame=" + render_area.replace(/\?/ig,"&"),post_info);
	//xmlhttpPost("http://localhost/?render_frame=" + render_area.replace(/\?/ig,"&"),post_info);
}

function find_and_eval_script_tags(estr,script_start,script_end)
{
	var xhtml = estr.split(script_start);
	for(var n=1; n<xhtml.length; n++)
	{
		var xhtml2 = xhtml[n].split(script_end);
		if(xhtml2.length > 1)
		{
			run_xhtml = xhtml2[0];
			eval(run_xhtml);
		}
	}
}

render_ajax_template = "";
function render_ajax_response(str)
{
	var elem = document.getElementById("loading_spinner_div");
	elem.parentNode.removeChild(elem);
	document.getElementById(ajax_render_id).className = "ajax_overlay";
	//document.getElementById(ajax_render_id).style.opacity = 1;
	//document.getElementById(ajax_render_id).style.filter="alpha(opacity=100)";
	
	if(render_ajax_template!="")
		document.getElementById(ajax_render_id).innerHTML = render_ajax_template.replace(/\[str\]/ig,str);
	else
		document.getElementById(ajax_render_id).innerHTML = str;
	
	//-------------- For fli
	var str2 = "";
	for(var flc in aj_flexible_containers)
	{
		if(elementDescendedFrom(ajax_render_id,flc))
		{
			elem_desc = document.getElementById(flc);
			for(var n=0; n<elem_desc.childNodes.length; n++)
			{
				elem_ch = elem_desc.childNodes[n];
				/*if(typeof aj_flexible_containers[elem_ch.id]!="undefined")
				{
					for(var m=0; m<elem_ch.childNodes.length; m++)
					{
						elem_gc = elem_ch.childNodes[m];
						str += "descendent found (" + m + "/" + elem_ch.childNodes.length + ")\nparent:" + elem_ch.id + "\n" + elem_gc + "\n\n";
					//str += "descendent found (" + m + "/" + elem_ch.childNodes.length + ")\nparent:" + elem_ch.id + "\nchild:" + elem_gc.id + "\nwidth:" + elem_gc.offsetWidth + "\nheight:" + elem_gc.offsetHeight + "\n";
					}
				}
				else
				{*/
					//str += "descendent found (" + n + "/" + elem_desc.childNodes.length + ")\nparent:" + elem_desc.id + "\n" + elem_ch + "\n\n";
					str2 += "descendent found (" + n + "/" + elem_desc.childNodes.length + ")\nparent:" + elem_desc.id + "\nchild:" + elem_ch.id + "\nwidth:" + elem_ch.offsetWidth + "\nheight:" + elem_ch.offsetHeight + "\n";
				//}
			}
		}
	}
	//alert(str);
	//if(typeof aj_flexible_containers[ajax_render_id]!="undefined")
		//alert(ajax_render_id + ":" + (typeof aj_flexible_containers[ajax_render_id]));

	/*var x = document.getElementById(ajax_render_id).getElementsByTagName("script");
	if(x.length > 0)
	{
		for(var i=0;i<x.length;i++)  
		{
			alert(x[i].text);
			eval(x[i].text);
		}
	}
	else*/
	{
		find_and_eval_script_tags(str,"<" + "script language=\"javascript\"" + ">","<" + "/script>");
		find_and_eval_script_tags(str,"<" + "script language='javascript'" + ">","<" + "/script>");
	}
}

function ajax_link(render_id,render_area,post_info,render_template,set_extra_callback_function)
{
	if(!post_info) post_info = "";
	if(!render_template) render_template = "";
	if(!set_extra_callback_function) set_extra_callback_function = "";
	render_ajax_template = render_template;
	ajax_callback_function = "render_ajax_response";
	extra_callback_function = set_extra_callback_function;
	ajax_render_id = render_id;
	
	//document.getElementById(render_id).innerHTML = "<table cellspacing=0 cellpadding=24><td>Loading...</td></table>";
	//document.getElementById(render_id).style.border = "solid 2px black";
	
	var elem = document.getElementById(render_id);
	var t = document.createElement('div');
	t.id = "loading_spinner_div";
	var set_top = (elem.offsetHeight / 2) - 6;
	var set_left = (elem.offsetWidth / 2) - 6;
	if(set_top > 40) set_top = 40;
	t.innerHTML = "<div style='position:absolute; top:" + set_top + "px; left:" + set_left + "px;'><img src='/cp/images/loading_transparent.gif' /></div>";
	//t.innerHTML = "<div style='position:absolute' left:120px; top:120px'><table cellpadding=36 style='border:solid 2px #aaaaaa' width='400' height='200'><tr><td bgcolor='#ffffff'>Loading ...</td></tr></table></div>";
	elem.parentNode.insertBefore(t,elem);
	
	document.getElementById(render_id).className = "ajax_overlay_greyed_out";
	//alert(document.getElementById(render_id).className);
	
	//document.getElementById(render_id).style.opacity = .4;
	//document.getElementById(render_id).style.filter="alpha(opacity=40)";
	
	get_ajax_response(render_area,post_info);
	//setTimeout("get_ajax_response('" + render_area + "','" + post_info + "')",1000);
}

function ajax_post(render_id,render_area,form_name)
{
	var post_str = "";
	var elem = document.getElementById(form_name).elements;
	for(var i = 0; i < elem.length; i++)
	{
		if(post_str!="") post_str += "&";
		post_str += elem[i].name + "=" + escape(elem[i].value);
	}
	return ajax_link(render_id,render_area,post_str);
}

// example for ajax callback
//function ajax_callback(str)
//{
//	alert("callback response " + str);
//}
//get_ajax_response("store.calendar?cinfo=test");

// example for ajax link
//ajax_link("container","store.calendar?cinfo=test");

function explain_price_received(rsp) {
	if(rsp['success']=='1') {
		var output = rsp['output'];
		alert(unescape_decode(output));
	}
}

function explain_price(server_host,for_order_id,for_item_id) {
	make_ajax_call("http://" + server_host + "/ajax_query/","cmd=explain_price&order_id=" + for_order_id+ "&explain_id=" + for_item_id,"explain_price_received");
}
