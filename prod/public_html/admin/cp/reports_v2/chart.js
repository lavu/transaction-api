/*!
 * Chart.js
 * http://chartjs.org/
 *
 * Copyright 2013 Nick Downie
 * Released under the MIT license
 * https://github.com/nnnick/Chart.js/blob/master/LICENSE.md
 */

//Define the global Chart Variable as a class.
(function(){
	window.previousDevicePixelRatio = null;
	(function(){
		function Color( r, g, b ){
			this._red = r;
			this._green = g;
			this._blue = b;
		}

		function red(){
			return this._red;
		}

		function green(){
			return this._green;
		}

		function blue(){
			return this._blue;
		}

		function toRGBAStringWithAlpha( alpha ){
			if( alpha === undefined ){
				alpha = 1;
			}
			return "rgba(" + this._red + "," + this._green + "," + this._blue + "," + alpha + ")";
		}

		function toString(){
			return this.toRGBAStringWithAlpha();
		}

		function brightness(){
			return (this._red + this._green + this._blue) / 3;
		}

		function brightnessDistance(){
			return Math.sqrt( (this._red*this._red) + (this._green*this._green) + (this._blue*this._blue) );
		}

		function colorSMult( s ){
			return new Color( s * this._red, s * this._green, s * this._blue );
		}

		function HSVtoRGB2( h, s, v ){
			// while( h > 2 * Math.PI ){
			// 	h -= Math.PI;
			// }

			var ra = 0;
			var rg = 2/3 * Math.PI;
			var rb = 4/3 * Math.PI;
			var c = 1/2;

			// s = [0, 1]
			// 

			var r = ((s*Math.cos( h - ra ))+1)*c*v;
			var g = ((s*Math.cos( h - rg ))+1)*c*v;
			var b = ((s*Math.cos( h - rb ))+1)*c*v;

			return new Color( Math.floor(r*255), Math.floor(g*255), Math.floor(b*255) );

		}

		function HSVtoRGB( h, s, v ){
			var r, g, b;
			var c = v * s;
			var x = c * ( 1 - Math.abs( ((h / 60) % 2 )- 1) );
			var m = v - c;

			if( h < 60 ){
				r = c; g = x; b = 0;
			} else if ( h < 120 ){
				r = x; g = c; b = 0;
			} else if ( h < 180 ){
				r = 0; g = c; b = x;
			} else if ( h < 240 ){
				r = 0; g = x; b = c;
			} else if ( h < 300 ){
				r = x; g = 0; b = c;
			} else {
				r = c; g = 0; b = x;
			}

			return new Color( Math.floor((r+m)*255), Math.floor((g+m)*255), Math.floor((b+m)*255) );
		}

		var previousHues = [];
		var threshold = 10.0;
		function nearPreivousHues( hue ){
			for( var i = 0; i < previousHues.length; i++ ){
				if( Math.abs( hue - previousHues[i] ) < threshold ){
					return true;
				}
			}
			return false;
		}

		function reset(){
			previousHues = [];
		}

		function createColorBetweenThresholds( rng, lower, upper ){

			var hue = rng.randBetween( 0, 360 );
			var ogHue = hue;
			var it = 0;
			while( nearPreivousHues( hue ) && it++ < threshold ) {
				hue = rng.randBetween( 0, 360 );
			}
			previousHues.push( hue );
			var color = Color.HSVtoRGB( hue, 0.55, 0.65 );

			return color;
		}

		window.Color = Color;
		Color.prototype.constructor = Color;
		Color.prototype = {};
		Color.prototype.red = red;
		Color.prototype.green = green;
		Color.prototype.blue = blue;
		Color.prototype.brightness = brightness;
		Color.prototype.brightnessDistance = brightnessDistance;
		Color.prototype.toRGBAStringWithAlpha = toRGBAStringWithAlpha;
		Color.prototype.toString = toString;
		Color.createColorBetweenThresholds = createColorBetweenThresholds;
		Color.HSVtoRGB = HSVtoRGB;
		Color.HSVtoRGB2 = HSVtoRGB2;
		Color.White = new Color( 255, 255, 255 );
		Color.Gray = new Color( 128, 128, 128 );
		Color.Grey = Color.Gray;
		Color.LightGray = new Color( 238, 238, 238 );
		Color.LightGrey = Color.LighGray;
		Color.BrightnessTest = new Color( 204, 204, 204 );
		Color.reset = reset;
	})();

	(function(){
		function RNG( seed ){
			var d = new Date();
			if( seed === undefined ){
				this._seed = 2345678901 + (d.getSeconds() * 0xFFFFFF) + (d.getMinutes() * 0xFFFF);
			} else {
				this._seed = seed;
			}

			this._A = 48271;
			this._M = 2147483647;
			this._Q = this._M / this._A;
			this._R = this._M % this._A;
			this._oneOverM = 1.0 / this._M;
		}

		function next(){
			var hi = this._seed / this._Q;
			var lo = this._seed % this._Q;
			var test = this._A * lo - this._R * hi;
			if(test > 0){
				this._seed = test;
			} else {
				this._seed = test + this._M;
			}
			return (this._seed * this._oneOverM);
		}

		function randBetween(Min, Max){
			return Math.round((Max-Min) * this.next() + Min);
		}

		var rngs = {};
		function rng( seed ){
			if(!seed){
				return null;
			}
			if(!isset( rngs[seed] ) ){
				rngs[seed] = new RNG(seed);
			}
			return rngs[seed];
		}

		window.RNG = RNG;
		RNG.prototype.constructor = RNG;
		RNG.prototype = {};
		RNG.prototype.next = next;
		RNG.prototype.randBetween = randBetween;
		RNG.rng = rng;
	})();

	(function(){
		function NumberFormatter( comma, decimal, precision, monitary_symbol, left_or_right ){
			this._comma = comma ? comma : ',';
			this._decimal = decimal ? decimal : '.';
			this._precision_number = (precision || precision === 0 ) ? precision : 2;
			this._monitary_symbol = monitary_symbol ? monitary_symbol : '$';
			this._symbol_on_left_or_right = left_or_right ? left_or_right : 'left';
		}

		function formatNumber( number ) {
			var roundindPrecision = 1;
			for( var i = 0; i < this._precision_number; i++ ){
				roundindPrecision *= 10;
			}

			var postDecimal = ((Math.round( (number % 1)*roundindPrecision ) / roundindPrecision) + "").split(".");
			postDecimal = postDecimal[1]===undefined?"0":postDecimal[1];
			var preDecimal = Math.floor( number ) + "";

			var postParts = postDecimal.split("");
			var e = new Array( this._precision_number + 1 );
			postDecimal = postParts.join("") + e.join("0");
			var pre = [];
			while( preDecimal.length !== 0 ){
				var m = preDecimal.length % 3;
				if( m === 0 ){
					m = 3;
				}
				pre.push( preDecimal.substr( 0, m ) );
				preDecimal = preDecimal.substr( m );
			}

			return pre.join( this._comma ) + this._decimal + postDecimal.substr( 0, this._precision_number );
		}

		function formatCurrency( number ) {
			var left = this._symbol_on_left_or_right == 'left' ? this._monitary_symbol : '';
			var right = this._symbol_on_left_or_right == 'right' ? this._monitary_symbol : '';
			return left + this.formatNumber( number ) + right;
		}

		var _sharedFormatter = null;
		function sharedFormatter( comma, decimal, precision, monitary_symbol, left_or_right ){
			if( !_sharedFormatter ){
				_sharedFormatter = new NumberFormatter( comma, decimal, precision, monitary_symbol, left_or_right );
			}
			return _sharedFormatter;
		}

		NumberFormatter.prototype = Object.create( Object.prototype );
		NumberFormatter.prototype.constructor = NumberFormatter;
		NumberFormatter.prototype.formatNumber = formatNumber;
		NumberFormatter.prototype.formatCurrency = formatCurrency;
		NumberFormatter.sharedFormatter = sharedFormatter;
		window.NumberFormatter = NumberFormatter;
	})();

	(function(){
		function Chart(context){
			if(!context){
				return;
			}
			this._chart = this;
			this._context = context;
			this._context.canvas.chart = this;
			this._config = this.defaults;
			this._rand = new RNG( 0xaece37 );
			this._magnitude = 20;
			// var chart = this;


			//Easing functions adapted from Robert Penner's easing equations
			//http://www.robertpenner.com/easing/

			//Variables global to the chart
			this._width =  context.canvas.width;
			this._height = context.canvas.height;


			//High pixel density displays - multiply the size of the canvas height/width by the device pixel ratio, then scale.
			// if (window.devicePixelRatio) {
				// this._context.canvas.style.width = this._width + "px";
				// this._context.canvas.style.height = this._height + "px";
			// 	this._context.canvas.height = this._height * window.devicePixelRatio;
			// 	this._context.canvas.width = this._width * window.devicePixelRatio;
			// 	this._context.scale(window.devicePixelRatio, window.devicePixelRatio);
			// }

			var ctx = this._context;
			ctx.canvas.addEventListener('mousemove', this );
			ctx.canvas.addEventListener('touchmove', this );
			ctx.canvas.addEventListener('touchdown', this );
			ctx.canvas.addEventListener('mousedown', this );

		}

		function clear(c){
			c.clearRect(0, 0, this._width, this._height);
		}

		function calculateOffset(val,calculatedScale,scaleHop){
			var outerValue = calculatedScale.steps * calculatedScale.stepValue;
			var adjustedValue = val - calculatedScale.graphMin;
			var scalingFactor = this.CapValue(adjustedValue/outerValue,1,0);
			return (scaleHop*calculatedScale.steps) * scalingFactor;
		}

		function animationLoop(config,drawScale,drawData,ctx){
			var animFrameAmount = (config.animation)? 1/this.CapValue(config.animationSteps,Number.MAX_VALUE,1) : 1;
			easingFunction = this.animationOptions[config.animationEasing];
			percentAnimComplete =(config.animation)? 0 : 1;

			if (typeof drawScale !== "function") drawScale = function(){};

			requestAnimFrame(animLoop.bind(this));

			function animateFrame(){
				if( window.previousDevicePixelRatio != window.devicePixelRatio ) {
					this._context.canvas.height = this._height * window.devicePixelRatio;
					this._context.canvas.width = this._width * window.devicePixelRatio;
					this._context.scale(window.devicePixelRatio, window.devicePixelRatio);
				}

				var easeAdjustedAnimationPercent =(config.animation)? this.CapValue(easingFunction(percentAnimComplete),null,0) : 1;
				this.clear(this._context);
				if(config.scaleOverlay){
					(drawData.bind(this))(easeAdjustedAnimationPercent);
					if( this.drawScale ){
						this.drawScale();
					}
				} else {
					if( this.drawScale ){
						this.drawScale();
					}
					(drawData.bind(this))(easeAdjustedAnimationPercent);
				}
			}

			function animLoop(){
				//We need to check if the animation is incomplete (less than 1), or complete (1).
				percentAnimComplete += animFrameAmount;
				(animateFrame.bind(this))();
					//Stop the loop continuing forever
					if (percentAnimComplete <= 1){
						requestAnimFrame(animLoop.bind(this));
					}
					else{
						if (typeof config.onAnimationComplete == "function") config.onAnimationComplete();
					}

				}

			}

			var magnitude = 20;
		//Declare global functions to be called within this namespace here.
		function calculateScale(drawingHeight,maxSteps,minSteps,maxValue,minValue,labelTemplateString){
			var graphMin,graphMax,graphRange,stepValue,numberOfSteps,valueRange,rangeOrderOfMagnitude,decimalNum;
			valueRange = maxValue - minValue;
			rangeOrderOfMagnitude = calculateOrderOfMagnitude.bind( this )(valueRange);
			graphMin = Math.min(Math.floor(minValue / (1 * Math.pow(this._magnitude, rangeOrderOfMagnitude))) * Math.pow(this._magnitude, rangeOrderOfMagnitude), 0);
			graphMax = Math.ceil(maxValue / (1 * Math.pow(this._magnitude, rangeOrderOfMagnitude))) * Math.pow(this._magnitude, rangeOrderOfMagnitude);
			graphRange = graphMax - graphMin;
			stepValue = Math.pow(this._magnitude, rangeOrderOfMagnitude)*this._magnitude;
			numberOfSteps = Math.round(graphRange / stepValue);

			//Compare number of steps to the max and min for that size graph, and add in half steps if need be.
			while(numberOfSteps < minSteps || numberOfSteps > maxSteps) {
				if (numberOfSteps < minSteps){
					stepValue /= 2;
					numberOfSteps = Math.round(graphRange/stepValue);
				}
				else{
					stepValue *=2;
					numberOfSteps = Math.round(graphRange/stepValue);
				}
			}

			var labels = [];
			populateLabels(labelTemplateString, labels, numberOfSteps, graphMin, stepValue);

			return {
				steps : numberOfSteps,
				stepValue : stepValue,
				graphMin : graphMin,
				labels : labels

			};

			function calculateOrderOfMagnitude(val){
				return Math.floor(Math.log(val) / Math.log(this._magnitude));
			}
		}

		//Populate an array of all the labels by interpolating the string.
		function populateLabels(labelTemplateString, labels, numberOfSteps, graphMin, stepValue) {
			if (labelTemplateString) {
				//Fix floating point errors by setting to fixed the on the same decimal as the stepValue.
				for (var i = 1; i < numberOfSteps + 1; i++) {
					labels.push(tmpl(labelTemplateString, {value: (graphMin + (stepValue * i)).toFixed(getDecimalPlaces(stepValue))}));
				}
			}
		}

		//Max value from array
		function Max( array ){
			return Math.max.apply( Math, array );
		}
		//Min value from array
		function Min( array ){
			return Math.min.apply( Math, array );
		}
		//Default if undefined
		function Default(userDeclared,valueIfFalse){
			if(!userDeclared){
				return valueIfFalse;
			} else {
				return userDeclared;
			}
		}
		//Is a number function
		function isNumber(n) {
			return !isNaN(parseFloat(n)) && isFinite(n);
		}
		//Apply cap a value at a high or low number
		function CapValue(valueToCap, maxValue, minValue){
			if(isNumber(maxValue)) {
				if( valueToCap > maxValue ) {
					return maxValue;
				}
			}
			if(isNumber(minValue)){
				if ( valueToCap < minValue ){
					return minValue;
				}
			}
			return valueToCap;
		}
		function getDecimalPlaces (num){
			var numberOfDecimalPlaces;
			if ((num%1)!==0){
				return num.toString().split(".")[1].length;
			}
			else{
				return 0;
			}
		}

		function mergeChartConfig(defaults,userDefined){
			var returnObj = {};
			var attrname;
			for (attrname in defaults) { returnObj[attrname] = defaults[attrname]; }
				for (attrname in userDefined) { returnObj[attrname] = userDefined[attrname]; }
					return returnObj;
			}

		/**
		 * Transforms the given number x, from the range i - I to the range o - O
		 * @param  {number} i the lower based number in the i - I range.
		 * @param  {number} x the number to transform
		 * @param  {number} I the higher based number in the i - I range.
		 * @param  {number} o the lower based number in the o - O range.
		 * @param  {number} O the higher based number in the o - O range.
		 * @return {number}   the transformed x number
		 */
		 function affineTransform( i, x, I, o, O ){
		 	return (((x - i)/(I - i)) * (O - o)) + o;
		 }

		 function handleEvent( event ) {
		 	var offset = getTotalOffsetForElement( event.target );

		 	var x = 0;
		 	var y = 0;
		 	if( event.clientX !== undefined && event.clientY !== undefined ){
		 		x = event.clientX - offset.x;
		 		y = event.clientY - offset.y;
		 	} else {
		 		x = event.x - offset.x;
		 		y = event.y - offset.y;
		 	}

			// if( window.devicePixelRatio ){
			// 	x *= window.devicePixelRatio;
			// 	y *= window.devicePixelRatio;
			// }

			if( event.touches ){
				x = event.touches[0].clientX - offset.x;
				y = event.touches[0].clientY - offset.y;
			}
			var point = null;
			switch( event.type ){
				case 'mousemove':
				case 'touchmove':
				event.preventDefault();
				event.stopPropagation();
				this.mouseMovedTo( { x: x, y : y } );
				break;
				case 'touchdown':
				case 'mousedown':
				event.preventDefault();
				event.stopPropagation();
				this.mouseActivatedOn( { x: x, y : y } );
				break;
				default:
			}
		}

		function mouseMovedTo( point ){
			// console.log( point );
		}

		function mouseActivatedOn( point ){
			// console.log( point );
		}

		function getTotalOffsetForElement( ele ){
			var x = 0;
			var y = 0;
			while( ele && ele != document.documentElement ){
				x += ele.offsetLeft;
				y += ele.offsetTop;
				ele = ele.offsetParent;
			}

			return { x: x - ( document.documentElement.scrollLeft + document.body.scrollLeft ), y: y - ( document.documentElement.scrollTop + document.body.scrollTop ) };
		}

		window.Chart = Chart;
		Chart.prototype.constructor = Chart;
		Chart.prototype = {};
		Chart.prototype.clear = clear;
		Chart.prototype.calculateScale = calculateScale;
		Chart.prototype.calculateOffset = calculateOffset;
		Chart.prototype.populateLabels = populateLabels;
		Chart.prototype.animationLoop = animationLoop;
		Chart.prototype.Max = Max;
		Chart.prototype.Min = Min;
		Chart.prototype.Default = Default;
		Chart.prototype.isNumber = isNumber;
		Chart.prototype.CapValue = CapValue;
		Chart.prototype.getDecimalPlaces = getDecimalPlaces;
		Chart.prototype.mergeChartConfig = mergeChartConfig;
		Chart.prototype.affineTransform = affineTransform;
		Chart.prototype.tmpl = tmpl;
		Chart.prototype.handleEvent = handleEvent;
		Chart.prototype.mouseMovedTo = mouseMovedTo;
		Chart.prototype.mouseActivatedOn = mouseActivatedOn;
		Chart.prototype.defaults = {
			scaleOverlay : true,
			scaleOverride : false,
			scaleSteps : null,
			scaleStepWidth : null,
			scaleStartValue : null,
			scaleShowLine : true,
			scaleLineColor : "rgba(0,0,0,.1)",
			scaleLineWidth : 1,
			scaleShowLabels : true,
			scaleLabel : "<%=value%>",
			scaleFontFamily : "'Verdana'",
			scaleFontSize : 12,
			scaleFontStyle : "normal",
			scaleFontColor : "#888",
			scaleShowLabelBackdrop : true,
			scaleBackdropColor : "rgba(255,255,255,0.75)",
			scaleBackdropPaddingY : 2,
			scaleBackdropPaddingX : 2,
			scaleGridLineColor : "rgba(0,0,0,.05)",
			scaleGridLineWidth : 1,
			animation : true,
			animationSteps : 100,
			animationEasing : "easeOutBounce",
			animateRotate : true,
			animateScale : false,
			onAnimationComplete : null
		};
		Chart.prototype.animationOptions = {
			linear : function (t){
				return t;
			},
			easeInQuad: function (t) {
				return t*t;
			},
			easeOutQuad: function (t) {
				return -1 *t*(t-2);
			},
			easeInOutQuad: function (t) {
				t /= (1/2);
				if ( t < 1)
					return 1/2*t*t;
				return -1/2 * ((--t)*(t-2) - 1);
			},
			easeInCubic: function (t) {
				return t*t*t;
			},
			easeOutCubic: function (t) {
				return 1*((t=t/1-1)*t*t + 1);
			},
			easeInOutCubic: function (t) {
				t /= (1/2);
				if (t < 1)
					return 1/2*t*t*t;
				return 1/2*((t-=2)*t*t + 2);
			},
			easeInQuart: function (t) {
				return t*t*t*t;
			},
			easeOutQuart: function (t) {
				return -1 * ((t=t/1-1)*t*t*t - 1);
			},
			easeInOutQuart: function (t) {
				t /= (1/2);
				if (t < 1)
					return 1/2*t*t*t*t;
				return -1/2 * ((t-=2)*t*t*t - 2);
			},
			easeInQuint: function (t) {
				return 1*(t/=1)*t*t*t*t;
			},
			easeOutQuint: function (t) {
				return 1*((t=t/1-1)*t*t*t*t + 1);
			},
			easeInOutQuint: function (t) {
				t /= (1/2);
				if (t < 1) return 1/2*t*t*t*t*t;
				return 1/2*((t-=2)*t*t*t*t + 2);
			},
			easeInSine: function (t) {
				return -1 * Math.cos(t/1 * (Math.PI/2)) + 1;
			},
			easeOutSine: function (t) {
				return 1 * Math.sin(t/1 * (Math.PI/2));
			},
			easeInOutSine: function (t) {
				return -1/2 * (Math.cos(Math.PI*t/1) - 1);
			},
			easeInExpo: function (t) {
				return (t===0) ? 1 : 1 * Math.pow(2, 10 * (t/1 - 1));
			},
			easeOutExpo: function (t) {
				return (t===1) ? 1 : 1 * (-Math.pow(2, -10 * t/1) + 1);
			},
			easeInOutExpo: function (t) {
				if (t===0)
					return 0;
				if (t===1)
					return 1;
				t /= (1/2);
				if (t < 1)
					return 1/2 * Math.pow(2, 10 * (t - 1));
				return 1/2 * (-Math.pow(2, -10 * --t) + 2);
			},
			easeInCirc: function (t) {
				if (t>=1)
					return t;
				return -1 * (Math.sqrt(1 - (t/=1)*t) - 1);
			},
			easeOutCirc: function (t) {
				return 1 * Math.sqrt(1 - (t=t/1-1)*t);
			},
			easeInOutCirc: function (t) {
				t /= (1/2);
				if (t < 1)
					return -1/2 * (Math.sqrt(1 - t*t) - 1);
				return 1/2 * (Math.sqrt(1 - (t-=2)*t) + 1);
			},
			easeInElastic: function (t) {
				var s=1.70158;
				var p=0;
				var a=1;
				if (t===0)
					return 0;
				t /= 1;
				if (t===1)
					return 1;
				if (!p)
					p=1*0.3;
				if (a < Math.abs(1)){
					a=1;
					s=(p/4);
				}
				else
					s = p/(2*Math.PI) * Math.asin (1/a);
				return -(a*Math.pow(2,10*(t-=1)) * Math.sin( (t*1-s)*(2*Math.PI)/p ));
			},
			easeOutElastic: function (t) {
				var s=1.70158;
				var p=0;
				var a=1;
				if (t===0)
					return 0;
				t/=1;
				if (t===1)
					return 1;
				if (!p)
					p=1*0.3;
				if (a < Math.abs(1)){
					a=1;
					s=p/4;
				}
				else
					s = p/(2*Math.PI) * Math.asin (1/a);
				return a*Math.pow(2,-10*t) * Math.sin( (t*1-s)*(2*Math.PI)/p ) + 1;
			},
			easeInOutElastic: function (t) {
				var s=1.70158;
				var p=0;
				var a=1;
				if (t===0)
					return 0;
				t /= (1/2);
				if (t===2)
					return 1;
				if (!p)
					p=1*(0.3*1.5);
				if (a < Math.abs(1)){
					a=1;
					s=p/4;
				}
				else
					s = p/(2*Math.PI) * Math.asin (1/a);
				if (t < 1)
					return -0.5*(a*Math.pow(2,10*(t-=1)) * Math.sin( (t*1-s)*(2*Math.PI)/p ));
				return a*Math.pow(2,-10*(t-=1)) * Math.sin( (t*1-s)*(2*Math.PI)/p )*0.5 + 1;
			},
			easeInBack: function (t) {
				var s = 1.70158;
				return 1*(t/=1)*t*((s+1)*t - s);
			},
			easeOutBack: function (t) {
				var s = 1.70158;
				return 1*((t=t/1-1)*t*((s+1)*t + s) + 1);
			},
			easeInOutBack: function (t) {
				var s = 1.70158;
				t /= (1/2);
				if (t < 1)
					return 1/2*(t*t*(((s*=(1.525))+1)*t - s));
				return 1/2*((t-=2)*t*(((s*=(1.525))+1)*t + s) + 2);
			},
			easeInBounce: function (t) {
				return 1 - this.easeOutBounce (1-t);
			},
			easeOutBounce: function (t) {
				if ((t/=1) < (1/2.75)) {
					return 1*(7.5625*t*t);
				} else if (t < (2/2.75)) {
					return 1*(7.5625*(t-=(1.5/2.75))*t + 0.75);
				} else if (t < (2.5/2.75)) {
					return 1*(7.5625*(t-=(2.25/2.75))*t + 0.9375);
				} else {
					return 1*(7.5625*(t-=(2.625/2.75))*t + 0.984375);
				}
			},
			easeInOutBounce: function (t) {
				if (t < 1/2)
					return this.easeInBounce (t*2) * 0.5;
				return this.easeOutBounce (t*2-1) * 0.5 + 1*0.5;
			}
		};
		// shim layer with setTimeout fallback
		var requestAnimFrame = (function(){
			return window.requestAnimationFrame ||
			window.webkitRequestAnimationFrame ||
			window.mozRequestAnimationFrame ||
			window.oRequestAnimationFrame ||
			window.msRequestAnimationFrame ||
			function(callback) {
				window.setTimeout(callback, 1000 / 60);
			};
		})();

		function _createGraph(proto, data, options){
			var config = (options)? mergeChartConfig( proto.defaultConfigs(), options ) : proto.defaultConfigs();
			if( !data ){
				data = this._data;
			} else {
				this._data = data;
			}
			return new proto( data, config, this._context );
		}


		function PolarArea(data,options){
			// var config = (options)? mergeChartConfig(Chart.prototype.PolarArea.defaults,options) : Chart.prototype.PolarArea.defaults;
			// if( !data ){
			// 	data = this._data;
			// } else {
			// 	this._data = data;
			// }
			// return new PolarAreaChart(data,config,this._context);
			return this._createGraph( PolarAreaChart, data, options );
		}

		function Radar(data, options){
			// var config = (options)? mergeChartConfig(Chart.prototype.Radar.defaults,options) : Chart.prototype.Radar.defaults;
			// if( !data ){
			// 	data = this._data;
			// } else {
			// 	this._data = data;
			// }
			// return new RadarChart(data,config, this._context);
			return this._createGraph( RadarChart, data, options );
		}

		function Pie(data,options){
			// var config = (options)? mergeChartConfig(Chart.prototype.Pie.defaults,options) : Chart.prototype.Pie.defaults;
			// if( !data ){
			// 	data = this._data;
			// } else {
			// 	this._data = data;
			// }
			// return new PieChart(data,config, this._context);
			return this._createGraph( PieChart, data, options );
		}

		function Doughnut(data,options){
			// var config = (options) ? mergeChartConfig(Chart.prototype.Doughnut.defaults,options) : Chart.prototype.Doughnut.defaults;
			// if( !data ){
			// 	data = this._data;
			// } else {
			// 	this._data = data;
			// }
			// return new DoughnutChart(data,config,this._context);
			return this._createGraph( DoughnutChart, data, options );
		}

		function Line(data,options){
			// var config = (options) ? mergeChartConfig(Chart.prototype.Line.defaults,options) : Chart.prototype.Line.defaults;
			// if( !data ){
			// 	data = this._data;
			// } else {
			// 	this._data = data;
			// }
			// return new LineChart(data,config,this._context);
			return this._createGraph( LineChart, data, options );
		}

		function Bar(data,options){
			// var config = (options) ? mergeChartConfig(Chart.prototype.Bar.defaults,options) : Chart.prototype.Bar.defaults;
			// if( !data ){
			// 	data = this._data;
			// } else {
			// 	this._data = data;
			// }
			// return new BarChart(data,config,this._context);
			return this._createGraph( BarChart, data, options );
		}

		function HBar(data,options){
			// var config = (options) ? mergeChartConfig(Chart.prototype.HBar.defaults,options) : Chart.prototype.HBar.defaults;
			// if( !data ){
			// 	data = this._data;
			// } else {
			// 	this._data = data;
			// }
			// return new HBarChart(data,config,this._context);
			return this._createGraph( HBarChart, data, options );
		}

		var _defaultConfigs = {
			animation : true,
			animationEasing : "easeOutQuart",
			animationSteps : 60,
			onAnimationComplete : null
		};

		function defaultConfigs(){
			return _defaultConfigs;
		}

		Chart.defaultConfigs = defaultConfigs;
		Chart.prototype._createGraph = _createGraph;
		Chart.prototype.PolarArea = PolarArea;
		Chart.prototype.Radar = Radar;
		Chart.prototype.Doughnut = Doughnut;
		Chart.prototype.Line = Line;
		Chart.prototype.Bar = Bar;
		Chart.prototype.HBar = HBar;
		Chart.prototype.Pie = Pie;
	})();

	(function(){
		function ChartGrid( data, config, ctx ){
			if(!config || !data){
				return;
			}
			Chart.call( this, ctx );
			this._maxSize = undefined;
			this._rotateLabels = undefined;
			this._labelHeight = undefined;
			this._scaleHeight = undefined;

			this._xAxisLength = undefined;
			this._valueHop = undefined;
			this._barWidth = undefined;
			this._yAxisPosX = undefined;
			this._xAxisPosY = undefined;

			this._config = config;
			this._data = data;

			for( var i = 0; i < this._data.datasets.length; i++ ){
				for( var j = 0; j < this._data.datasets[i].data.length; j++ ){
					this._data.datasets[i].data[j] = this._data.datasets[i].data[j] * 1;
				}
				if( this._data.datasets[i].fillColor === undefined && this._data.datasets[i].color === undefined ) {
					this._data.datasets[i].color = Color.createColorBetweenThresholds( this._rand );
				}
			}

			this.calculateDrawingSizes();
			this._valueBounds = this.getValueBounds();
			//Check and set the scale
			this._labelTemplateString = (config.scaleShowLabels)? config.scaleLabel : "";

			if (!config.scaleOverride){

				this._calculatedScale = this.calculateScale(this._scaleHeight,this._valueBounds.maxSteps,this._valueBounds.minSteps,this._valueBounds.maxValue,this._valueBounds.minValue,this._labelTemplateString);
			}
			else {
				this._calculatedScale = {
					steps : config.scaleSteps,
					stepValue : config.scaleStepWidth,
					graphMin : config.scaleStartValue,
					labels : []
				};
				this.populateLabels(this._labelTemplateString, this._calculatedScale.labels,this._calculatedScale.steps,config.scaleStartValue,config.scaleStepWidth);
			}

			this._scaleHop = Math.floor(this._scaleHeight/this._calculatedScale.steps);
			this.calculateXAxisSize();
			this.animationLoop(config,this.drawScale,this.drawGraph,ctx);
		}

		function drawGraph( number ){
			return;
		}

		function calculateDrawingSizes(){
			var width = this._context.canvas.clientWidth;
			var height = this._context.canvas.clientHeight;
			this._maxSize = height;

			//Need to check the X axis first - measure the length of each text metric, and figure out if we need to rotate by 45 degrees.
			this._context.font = this._config.scaleFontStyle + " " + this._config.scaleFontSize+"px " + this._config.scaleFontFamily;
			widestXLabel = 1;
			for (var i=0; i<this._data.labels.length; i++){
				var textLength = this._context.measureText(this._data.labels[i]).width;
				//If the text length is longer - make that equal to longest text!
				widestXLabel = (textLength > widestXLabel)? textLength : widestXLabel;
			}
			if (width/this._data.labels.length < widestXLabel){
				this._rotateLabels  = 45;
				if (width/this._data.labels.length < Math.cos(this._rotateLabels) * widestXLabel){
					this._rotateLabels = 90;
					this._maxSize -= widestXLabel;
				}
				else{
					this._maxSize -= Math.sin(this._rotateLabels) * widestXLabel;
				}
			}
			else{
				this._maxSize -= this._config.scaleFontSize;
			}

			//Add a little padding between the x line and the text
			this._maxSize -= 5;
			this._labelHeight = this._config.scaleFontSize;
			this._maxSize -= this._labelHeight;
			//Set 5 pixels greater than the font size to allow for a little padding from the X axis.
			this._scaleHeight = this._maxSize;
			//Then get the area above we can safely draw on.
		}

		function getValueBounds() {
			var upperValue = Number.MIN_VALUE;
			var lowerValue = Number.MAX_VALUE;
			for (var i=0; i<this._data.datasets.length; i++){
				for (var j=0; j<this._data.datasets[i].data.length; j++){
					if (this._data.datasets[i].data[j] > upperValue){ upperValue = this._data.datasets[i].data[j]; }
					if (this._data.datasets[i].data[j] < lowerValue){ lowerValue = this._data.datasets[i].data[j]; }
				}
			}

			var maxSteps = Math.floor((this._scaleHeight / (this._labelHeight*0.36)));
			var minSteps = Math.floor((this._scaleHeight / this._labelHeight*0.25));

			return {
				maxValue : upperValue,
				minValue : lowerValue,
				maxSteps : maxSteps,
				minSteps : minSteps
			};
		}

		function calculateXAxisSize(){
			var longestText = 1;
			//if we are showing the labels
			if (this._config.scaleShowLabels){
				this._context.font = this._config.scaleFontStyle + " " + (this._config.scaleFontSize)+"px " + this._config.scaleFontFamily;
				for (var i=0; i<this._calculatedScale.labels.length; i++){
					var measuredText = this._context.measureText( NumberFormatter.sharedFormatter().formatCurrency( this._calculatedScale.labels[i] )).width;
					longestText = (measuredText > longestText)? measuredText : longestText;
				}
				//Add a little extra padding from the y axis
				longestText +=10;
			}
			this._xAxisLength = this._width - longestText - widestXLabel;
			var extra = ( this instanceof LineChart ) ? 1 : 0;
			this._valueHop = Math.floor(this._xAxisLength/(this._data.labels.length - extra));
			this._valueHop = isFinite( this._valueHop ) ? this._valueHop : 0.0;
			this._barWidth = (this._valueHop - this._config.scaleGridLineWidth*2 - (this._config.barValueSpacing*2) - (this._config.barDatasetSpacing*this._data.datasets.length-1) - ((this._config.barStrokeWidth/2)*this._data.datasets.length-1))/this._data.datasets.length;

			this._yAxisPosX = this._width-widestXLabel/2-this._xAxisLength;
			this._xAxisPosY = this._scaleHeight + this._config.scaleFontSize/2;
		}

		var _defaultConfigs = {
			// scaleShowLine : true,
			scaleBackdropColor : "rgba(255,255,255,0.75)",
			scaleBackdropPaddingX : 2,
			scaleBackdropPaddingY : 2,
			scaleFontColor : "#888",
			scaleFontFamily : "'Verdana'",
			scaleFontSize : 12,
			scaleFontStyle : "normal",
			scaleGridLineColor : "rgba(0,0,0,.05)",
			scaleGridLineWidth : 1,
			scaleLabel : "<%=value%>",
			scaleLineColor : "rgba(0,0,0,.1)",
			scaleLineWidth : 1,
			scaleOverlay : false,
			scaleOverride : false,
			scaleShowGridLines : true,
			scaleShowLabelBackdrop : true,
			scaleShowLabels : true,
			scaleStartValue : null,
			scaleSteps : null,
			scaleStepWidth : null
		};

		function defaultConfigs(){
			return Chart.prototype.mergeChartConfig( Chart.defaultConfigs(), _defaultConfigs );
		}

		window.ChartGrid = ChartGrid;
		ChartGrid.prototype.constructor = ChartGrid;
		ChartGrid.prototype = Object.create( Chart.prototype );
		ChartGrid.prototype.calculateDrawingSizes = calculateDrawingSizes;
		ChartGrid.prototype.getValueBounds = getValueBounds;
		ChartGrid.prototype.calculateXAxisSize = calculateXAxisSize;
		ChartGrid.prototype.drawGraph = drawGraph;
		ChartGrid.defaultConfigs = defaultConfigs;

	})();

	(function(){
		function PolarAreaChart(data,config,ctx){
			this._maxSize = undefined;
			this._scaleHop = undefined;
			this._calculatedScale = undefined;
			this._labelHeight = undefined;
			this._scaleHeight = undefined;
			this._valueBounds = undefined;
			this._labelTemplateString = undefined;
			this._config = config;
			this._data = data;
			if( data.datasets ){
				this._graphData = data.datasets[0].data;
			} else {
				this._graphData = data;
			}
			var tempData = this._graphData;
			this._rand = new RNG( 0xaece37 );
			// data = this._graphData;
			if( tempData && tempData.length && !isNaN( Number( tempData[0] ) ) ){
				this._graphData = [];
				for( i = 0; i < tempData.length; i++ ){
					this._graphData.push( { value: tempData[i], color: Color.createColorBetweenThresholds( this._rand ) } );
				}
			}
			// data = this._graphData;

			ChartGrid.call( this, data, config, ctx );
			// this.calculateDrawingSizes();

			// this._valueBounds = this.getValueBounds();

			// this._labelTemplateString = (config.scaleShowLabels)? config.scaleLabel : null;

			// //Check and set the scale
			// if (!config.scaleOverride){

			// 	this._calculatedScale = this.calculateScale(this._scaleHeight,this._valueBounds.maxSteps,this._valueBounds.minSteps,this._valueBounds.maxValue,this._valueBounds.minValue,this._labelTemplateString);
			// }
			// else {
			// 	this._calculatedScale = {
			// 		steps : config.scaleSteps,
			// 		stepValue : config.scaleStepWidth,
			// 		graphMin : config.scaleStartValue,
			// 		labels : []
			// 	};
			// 	populateLabels(this._labelTemplateString, this._calculatedScale.labels,this._calculatedScale.steps,config.scaleStartValue,config.scaleStepWidth);
			// }

			// this._scaleHop = this._maxSize/(this._calculatedScale.steps);

			// //Wrap in an animation loop wrapper
			// this.animationLoop(config,drawScale,drawAllSegments,ctx);
		}

		function calculateDrawingSizes(){
			var width = this._context.canvas.clientWidth;
			var height = this._context.canvas.clientHeight;
			this._maxSize = (this.Min([width,height])/2);
			//Remove whatever is larger - the font size or line width.

			this._maxSize -= this.Max([this._config.scaleFontSize*0.5,this._config.scaleLineWidth*0.5]);

			this._labelHeight = this._config.scaleFontSize*2;
			//If we're drawing the backdrop - add the Y padding to the label height and remove from drawing region.
			if (this._config.scaleShowLabelBackdrop){
				this._labelHeight += (2 * this._config.scaleBackdropPaddingY );
				this._maxSize -= this._config.scaleBackdropPaddingY*1.5;
			}

			this._scaleHeight = this._maxSize;
			//If the label height is less than 5, set it to 5 so we don't have lines on top of each other.
			this._labelHeight = this.Default(this._labelHeight,5);
		}

		function drawScale(){

			for (var i=0; i<this._calculatedScale.steps; i++){
				//If the line object is there
				if (this._config.scaleShowLine){
					this._context.beginPath();
					this._context.arc(this._width/2, this._height/2, this._scaleHop * (i + 1), 0, (Math.PI * 2), true);
					this._context.strokeStyle = this._config.scaleLineColor;
					this._context.lineWidth = this._config.scaleLineWidth;
					this._context.stroke();
				}

				if (this._config.scaleShowLabels){
					this._context.textAlign = "center";
					this._context.font = this._config.scaleFontStyle + " " + this._config.scaleFontSize + "px " + this._config.scaleFontFamily;
					var label =  this._calculatedScale.labels[i];
					//If the backdrop object is within the font object
					if (this._config.scaleShowLabelBackdrop){
						var textWidth = this._context.measureText(label).width;
						this._context.fillStyle = this._config.scaleBackdropColor;
						this._context.beginPath();
						this._context.rect(
							Math.round(this._width/2 - textWidth/2 - this._config.scaleBackdropPaddingX),     //X
							Math.round(this._height/2 - (this._scaleHop * (i + 1)) - this._config.scaleFontSize*0.5 - this._config.scaleBackdropPaddingY),//Y
							Math.round(textWidth + (this._config.scaleBackdropPaddingX*2)), //Width
							Math.round(this._config.scaleFontSize + (this._config.scaleBackdropPaddingY*2)) //Height
							);
						this._context.fill();
					}
					this._context.textBaseline = "middle";
					this._context.fillStyle = this._config.scaleFontColor;
					this._context.fillText(label,this._width/2,this._height/2 - (this._scaleHop * (i + 1)));
				}
			}
		}

		function drawGraph( number ){
			this.drawAllSegments( number );
		}

		function drawAllSegments(animationDecimal){

			var startAngle = -Math.PI/2,
			angleStep = (Math.PI*2)/this._graphData.length,
			scaleAnimation = 1,
			rotateAnimation = 1;
			if (this._config.animation) {
				if (this._config.animateScale) {
					scaleAnimation = animationDecimal;
				}
				if (this._config.animateRotate){
					rotateAnimation = animationDecimal;
				}
			}

			for (var i=0; i<this._graphData.length; i++){

				this._context.beginPath();
				this._context.arc(this._width/2,this._height/2,scaleAnimation * this.calculateOffset(this._graphData[i].value,this._calculatedScale,this._scaleHop),startAngle, startAngle + rotateAnimation*angleStep, false);
				this._context.lineTo(this._width/2,this._height/2);
				this._context.closePath();
				if( this._graphData[i].color ){
					if( this._graphData[i].color instanceof Color ){
						this._context.fillStyle = this._graphData[i].color.toString();
					} else {
						this._context.fillStyle = this._graphData[i].color;
					}
				}
				this._context.fill();

				if(this._config.segmentShowStroke){
					this._context.strokeStyle = this._config.segmentStrokeColor;
					this._context.lineWidth = this._config.segmentStrokeWidth;
					this._context.stroke();
				}
				startAngle += rotateAnimation*angleStep;
			}
		}

		function getValueBounds() {
			var upperValue = Number.MIN_VALUE;
			var lowerValue = Number.MAX_VALUE;
			for (var i=0; i<this._graphData.length; i++){
				if (this._graphData[i].value > upperValue) {upperValue = this._graphData[i].value;}
				if (this._graphData[i].value < lowerValue) {lowerValue = this._graphData[i].value;}
			}

			var maxSteps = Math.floor((this._scaleHeight / (this._labelHeight*0.66)));
			var minSteps = Math.floor((this._scaleHeight / this._labelHeight*0.5));

			return {
				maxValue : upperValue,
				minValue : lowerValue,
				maxSteps : maxSteps,
				minSteps : minSteps
			};
		}

		var _defaultConfigs = {
			animateRotate : true,
			animateScale : false,
			scaleOverlay : true,
			segmentShowStroke : true,
			segmentStrokeColor : "#fff",
			segmentStrokeWidth : 2
		};

		function defaultConfigs(){
			return Chart.prototype.mergeChartConfig( ChartGrid.defaultConfigs(), _defaultConfigs );
		}

		window.PolarAreaChart = PolarAreaChart;
		PolarAreaChart.prototype.constructor = PolarAreaChart;
		PolarAreaChart.prototype = Object.create( ChartGrid.prototype );//new Chart();
		PolarAreaChart.prototype.calculateDrawingSizes = calculateDrawingSizes;
		PolarAreaChart.prototype.drawScale = drawScale;
		PolarAreaChart.prototype.drawGraph = drawGraph;
		PolarAreaChart.prototype.drawAllSegments = drawAllSegments;
		PolarAreaChart.prototype.getValueBounds = getValueBounds;
		PolarAreaChart.defaultConfigs = defaultConfigs;
	})();

	(function(){
		function PieChart(data,config,ctx){
			Chart.call( this, ctx );
			this._segmentTotal = 0;
			//In case we have a canvas that is not a square. Minus 5 pixels as padding round the edge.
			this._pieRadius = this.Min([this._height/2,this._width/2]) - 5;
			this._config = config;
			this._data = data;
			this._labels = [];
			if( data.datasets ){
				var graphData = [];
				var graphLabels = [];
				for( var i = 0; i < data.datasets.length; i++ ){
					graphData.push( data.datasets[i].data[0] );
					graphLabels.push( data.datasets[i].label );
				}

				this._graphData = graphData;
				this._labels = graphLabels;
			} else if( data.graphData ) {
				this._graphData = data.graphData;
			} else {
				this._graphData = data;
			}
			data = this._graphData;
			var i;
			if( data && data.length && !isNaN( Number( data[0] ) ) ){
				this._graphData = [];
				for( i = 0; i < data.length; i++ ){
					this._graphData.push( { value: data[i], color: Color.createColorBetweenThresholds( this._rand ), label: this._labels[i] } );
				}
			}
			data = this._graphData;

			for (i=0; i<data.length; i++){
				this._segmentTotal += (data[i].value*1);
			}


			this.animationLoop(config,null,drawPieSegments,ctx);
		}

		function drawPieSegments (animationDecimal){

			var cumulativeAngle = -Math.PI/2,
			scaleAnimation = 1,
			rotateAnimation = 1;
			if (this._config.animation) {
				if (this._config.animateScale) {
					scaleAnimation = animationDecimal;
				}
				if (this._config.animateRotate){
					rotateAnimation = animationDecimal;
				}
			}
			for (var i=0; i<this._graphData.length; i++){
				var segmentAngle = rotateAnimation * ((this._graphData[i].value/this._segmentTotal) * (Math.PI*2));
				this._context.beginPath();
				this._context.arc(this._width/2,this._height/2,scaleAnimation * this._pieRadius,cumulativeAngle,cumulativeAngle + segmentAngle);
				this._context.lineTo(this._width/2,this._height/2);
				this._context.closePath();
				if( this._graphData[i].color ){
					if( this._graphData[i].color instanceof Color ){
						this._context.fillStyle = this._graphData[i].color.toString();
					} else {
						this._context.fillStyle = this._graphData[i].color;
					}
				}
				this._context.fill();

				if(this._config.segmentShowStroke){
					this._context.lineWidth = this._config.segmentStrokeWidth;
					this._context.strokeStyle = this._config.segmentStrokeColor;
					this._context.stroke();
				}
				cumulativeAngle += segmentAngle;
			}
		}

		var _defaultConfigs = {
			animateRotate : true,
			segmentShowStroke : true,
			segmentStrokeColor : "#fff",
			segmentStrokeWidth : 2
		};

		function defaultConfigs(){
			return Chart.prototype.mergeChartConfig( Chart.defaultConfigs(), _defaultConfigs );
		}

		window.PieChart = PieChart;
		PieChart.prototype.constructor = PieChart;
		PieChart.prototype = Object.create( Chart.prototype );
		PieChart.prototype.drawPieSegments = drawPieSegments;
		PieChart.defaultConfigs = defaultConfigs;
	})();

	(function(){
		function DoughnutChart(data,config,ctx){
			Chart.call( this, ctx );
			this._segmentTotal = 0;
			//In case we have a canvas that is not a square. Minus 5 pixels as padding round the edge.
			this._doughnutRadius = this.Min([this._height/2,this._width/2]) - 5;

			this._cutoutRadius = this._doughnutRadius * (config.percentageInnerCutout/100);
			this._data = data;
			this._labels = [];
			if( data.datasets ){
				var graphData = [];
				var graphLabels = [];
				for( var i = 0; i < data.datasets.length; i++ ){
					graphData.push( data.datasets[i].data[0] );
					graphLabels.push( data.datasets[i].label );
				}

				this._graphData = graphData;
				this._labels = graphLabels;
			} else if( data.graphData ) {
				this._graphData = data.graphData;
			} else {
				this._graphData = data;
			}
			data = this._graphData;
			if( data && data.length && !isNaN( Number( data[0] ) ) ){
				this._graphData = [];
				for( i = 0; i < data.length; i++ ){
					this._graphData.push( { value: data[i], color: Color.createColorBetweenThresholds( this._rand ), label: this._labels[i] } );
				}
			}
			data = this._graphData;
			this._config = config;

			for (var i=0; i<data.length; i++){
				this._segmentTotal += (data[i].value*1);
			}


			this.animationLoop(config,null,drawPieSegments,ctx);
			this._currentSegment = null;
		}

		function drawPieSegments (animationDecimal){


			// this._context.scale(dpr, dpr);
			var cumulativeAngle = -Math.PI/2,
			scaleAnimation = 1,
			rotateAnimation = 1;
			if (this._config.animation) {
				if (this._config.animateScale) {
					scaleAnimation = animationDecimal;
				}
				if (this._config.animateRotate){
					rotateAnimation = animationDecimal;
				}
			}
			for (var i=0; i<this._graphData.length; i++){
				var segmentAngle = rotateAnimation * ((this._graphData[i].value/this._segmentTotal) * (Math.PI*2));
				this._context.beginPath();
				this._context.arc(this._width/2,this._height/2,scaleAnimation * this._doughnutRadius,cumulativeAngle,cumulativeAngle + segmentAngle,false);
				this._context.arc(this._width/2,this._height/2,scaleAnimation * this._cutoutRadius,cumulativeAngle + segmentAngle,cumulativeAngle,true);
				this._context.closePath();
				if( this._graphData[i].color ){
					if( this._graphData[i].color instanceof Color ){
						if( i === this._currentSegment ){
							this._context.fillStyle = this._graphData[i].color.toRGBAStringWithAlpha(0.75);
						} else {
							this._context.fillStyle = this._graphData[i].color.toRGBAStringWithAlpha(1.0);
						}
					} else {
						this._context.fillStyle = this._graphData[i].color;
					}
				}
				this._context.fill();

				if(this._config.segmentShowStroke){
					this._context.lineWidth = this._config.segmentStrokeWidth;
					this._context.strokeStyle = this._config.segmentStrokeColor;
					this._context.stroke();
				}
				cumulativeAngle += segmentAngle;
			}

			if( this._currentSegment !== null ){
				var frac = Math.sqrt(2)/2;
				this._labelHeight = this.Default(this._labelHeight,this._config.scaleFontSize);
				var details = this._graphData[this._currentSegment];

				var centerY = this._height / 2;
				var centerX = this._width / 2;

				var whiteColor = 'rgba(255,255,255,0.9)';
				var padding = 10;
				var boxSize = 20;
				var maxWidth = frac * 2 * this._cutoutRadius;
				maxWidth = Math.max( maxWidth, this._context.measureText( details.label ).width );
				var maxHeight = 3*(this._labelHeight + (padding/2));

				var topOfBox = centerY - (this._cutoutRadius*(1 - frac));
				var leftOfBox = centerX - (this._cutoutRadius*frac);

				if( maxWidth > frac * 2 * this._cutoutRadius ) {
					leftOfBox -= (  maxWidth - (frac * 2 * this._cutoutRadius)) /2;
				}

				this._context.font = this._config.scaleFontStyle + " " + this._config.scaleFontSize+"px " + this._config.scaleFontFamily;
				maxWidth = Math.max( maxWidth, this._context.measureText( NumberFormatter.sharedFormatter().formatCurrency( details.value*1 ) ).width + boxSize );

				this._context.fillStyle = 'rgba(0,0,0,0.75)';
				this._context.strokeStyle = whiteColor;

				var l = leftOfBox;
				var t = topOfBox;
				var p = padding;
				var w = maxWidth;
				var h = maxHeight;

				this._context.beginPath();
				this._context.moveTo( l, t - p );
				this._context.arc( l + w, t, p, 1.5 * Math.PI, 0, false );
				this._context.arc( l + w, t + h, p, 0, 0.5 * Math.PI, false );
				this._context.arc( l, t + h, p, 0.5 * Math.PI, Math.PI, false );
				this._context.arc( l, t, p, Math.PI, 1.5 * Math.PI, false );
				this._context.closePath();
				this._context.fill();

				this._context.textAlign = 'left';
				this._context.fillStyle = whiteColor;

				var h = padding;
				this._context.fillText( details.label, leftOfBox, h + topOfBox );
				h += this._labelHeight + padding;

				var boxPadding = 5;

				this._context.fillText( NumberFormatter.sharedFormatter().formatCurrency( details.value*1 ), leftOfBox + boxSize, h + topOfBox );
				this._context.fillStyle = details.color.toString();
				this._context.fillRect( leftOfBox, topOfBox + h - (boxPadding*1.7), boxSize - boxPadding, boxSize - boxPadding );
				h += this._labelHeight + padding;
				this._context.fillStyle = whiteColor;
			}
		}

		var _defaultConfigs = {
			animateRotate : true,
			segmentShowStroke : true,
			segmentStrokeColor : "#fff",
			segmentStrokeWidth : 2,
			percentageInnerCutout : 50,
			scaleFontSize: 12,
			scaleFontStyle: "normal",
			scaleFontFamily : "'Verdana'",
		};

		function defaultConfigs(){
			return Chart.prototype.mergeChartConfig( Chart.defaultConfigs(), _defaultConfigs );
		}

		function mouseMovedTo( point ) {
			var x = point.x;
			var y = point.y;

			var centerX = this._width/2;
			var centerY = this._height/2;

			var _x = x - centerX;
			var _y = y - centerY;
			var a = Math.atan2( _y, _x );
			if( a < -Math.PI/2 ) {
				a += 2 *  Math.PI;
			}

			var innerRS = this._cutoutRadius * this._cutoutRadius;
			var outterRS = this._doughnutRadius * this._doughnutRadius;

			var total_angle = -Math.PI/2;
			var previousSegment = this._currentSegment;
			this._currentSegment = null;
			var rads = (_x*_x) + (_y*_y);
			if( rads  >= innerRS && rads <= outterRS ) {
				for( var i = 0; i < this._graphData.length; i++ ){
					var segmentAngle = ((this._graphData[i].value/this._segmentTotal) * (Math.PI*2));
					if( a >= total_angle && a <= total_angle + segmentAngle ) {
						// i - 1
						this._currentSegment = i;
						break;
					}

					total_angle += segmentAngle;
				}
			}

			if( this._currentSegment !== previousSegment ){
				this.clear( this._context);
				this.drawPieSegments(1);
			}
		}

		window.DoughnutChart = DoughnutChart;
		DoughnutChart.prototype.constructor = DoughnutChart;
		DoughnutChart.prototype = Object.create( Chart.prototype );
		DoughnutChart.prototype.drawPieSegments = drawPieSegments;
		DoughnutChart.prototype.mouseMovedTo = mouseMovedTo;
		DoughnutChart.defaultConfigs = defaultConfigs;
	})();

	(function(){
		function RadarChart(data,config,ctx) {
			this._scaleHop = undefined;
			this._calculatedScale = undefined;
			this._valueBounds = undefined;
			this._labelTemplateString = undefined;
			//If no labels are defined set to an empty array, so referencing length for looping doesn't blow up.
			if (!data.labels)
				data.labels = [];

			ChartGrid.call( this, data, config, ctx );
		}

		function calculateDrawingSizes(){
			var width = this._context.canvas.clientWidth;
			var height = this._context.canvas.clientHeight;
			this._maxSize = (this.Min([width,height])/2);

			this._labelHeight = this._config.scaleFontSize*2;

			var labelLength = 0;
			for (var i=0; i<this._data.labels.length; i++){
				this._context.font = this._config.pointLabelFontStyle + " " + this._config.pointLabelFontSize+"px " + this._config.pointLabelFontFamily;
				var textMeasurement = this._context.measureText(this._data.labels[i]).width;
				if(textMeasurement>labelLength) labelLength = textMeasurement;
			}

			//Figure out whats the largest - the height of the text or the width of what's there, and minus it from the maximum usable size.
			this._maxSize -= this.Max([labelLength,((this._config.pointLabelFontSize/2)*1.5)]);

			this._maxSize -= this._config.pointLabelFontSize;
			this._maxSize = this.CapValue(this._maxSize, null, 0);
			this._scaleHeight = this._maxSize;
			//If the label height is less than 5, set it to 5 so we don't have lines on top of each other.
			this._labelHeight = this.Default(this._labelHeight,5);
		}

		//Radar specific functions.
		function drawAllDataPoints(animationDecimal){

			var rotationDegree = (2*Math.PI)/this._data.datasets[0].data.length;

			this._context.save();
			//translate to the centre of the canvas.
			this._context.translate(this._width/2,this._height/2);

			//We accept multiple data sets for radar charts, so show loop through each set
			for (var i=0; i<this._data.datasets.length; i++){
				this._context.beginPath();

				this._context.moveTo(0,animationDecimal*(-1*this.calculateOffset(this._data.datasets[i].data[0],this._calculatedScale,this._scaleHop)));
				for (var j=1; j<this._data.datasets[i].data.length; j++){
					this._context.rotate(rotationDegree);
					this._context.lineTo(0,animationDecimal*(-1*this.calculateOffset(this._data.datasets[i].data[j],this._calculatedScale,this._scaleHop)));

				}
				this._context.closePath();

				if( this._data.datasets[i].fillColor !== undefined ){
					this._context.fillStyle = this._data.datasets[i].fillColor;
					this._context.strokeStyle = this._data.datasets[i].strokeColor;
				} else {
					var color = this._data.datasets[i].color;
					this._context.fillStyle = color.toRGBAStringWithAlpha( 0.1 );
					this._context.strokeStyle = color.toString();
				}
				this._context.lineWidth = this._config.datasetStrokeWidth;
				this._context.fill();
				this._context.stroke();


				if (this._config.pointDot){
					this._context.fillStyle = this._data.datasets[i].pointColor;
					this._context.strokeStyle = this._data.datasets[i].pointStrokeColor;
					this._context.lineWidth = this._config.pointDotStrokeWidth;
					for (var k=0; k<this._data.datasets[i].data.length; k++){
						this._context.rotate(rotationDegree);
						this._context.beginPath();
						this._context.arc(0,animationDecimal*(-1*this.calculateOffset(this._data.datasets[i].data[k],this._calculatedScale,this._scaleHop)),this._config.pointDotRadius,2*Math.PI,false);
						this._context.fill();
						this._context.stroke();
					}

				}
				this._context.rotate(rotationDegree);

			}
			this._context.restore();


		}
		function drawScale(){

			var rotationDegree = (2*Math.PI)/this._data.datasets[0].data.length;
			this._context.save();
			this._context.translate(this._width / 2, this._height / 2);

			if (this._config.angleShowLineOut){
				this._context.strokeStyle = this._config.angleLineColor;
				this._context.lineWidth = this._config.angleLineWidth;
				for (var h=0; h<this._data.datasets[0].data.length; h++){

					this._context.rotate(rotationDegree);
					this._context.beginPath();
					this._context.moveTo(0,0);
					this._context.lineTo(0,-this._maxSize);
					this._context.stroke();
				}
			}

			for (var i=0; i<this._calculatedScale.steps; i++){
				this._context.beginPath();

				if(this._config.scaleShowLine){
					this._context.strokeStyle = this._config.scaleLineColor;
					this._context.lineWidth = this._config.scaleLineWidth;
					this._context.moveTo(0,-this._scaleHop * (i+1));
					for (var j=0; j<this._data.datasets[0].data.length; j++){
						this._context.rotate(rotationDegree);
						this._context.lineTo(0,-this._scaleHop * (i+1));
					}
					this._context.closePath();
					this._context.stroke();

				}

				if (this._config.scaleShowLabels){
					this._context.textAlign = 'center';
					this._context.font = this._config.scaleFontStyle + " " + this._config.scaleFontSize+"px " + this._config.scaleFontFamily;
					this._context.textBaseline = "middle";

					if (this._config.scaleShowLabelBackdrop){
						var textWidth = this._context.measureText(this._calculatedScale.labels[i]).width;
						this._context.fillStyle = this._config.scaleBackdropColor;
						this._context.beginPath();
						this._context.rect(
							Math.round(- textWidth/2 - this._config.scaleBackdropPaddingX),     //X
							Math.round((-this._scaleHop * (i + 1)) - this._config.scaleFontSize*0.5 - this._config.scaleBackdropPaddingY),//Y
							Math.round(textWidth + (this._config.scaleBackdropPaddingX*2)), //Width
							Math.round(this._config.scaleFontSize + (this._config.scaleBackdropPaddingY*2)) //Height
							);
						this._context.fill();
					}
					this._context.fillStyle = this._config.scaleFontColor;
					this._context.fillText(this._calculatedScale.labels[i],0,-this._scaleHop*(i+1));
				}

			}
			for (var k=0; k<this._data.labels.length; k++){
				this._context.font = this._config.pointLabelFontStyle + " " + this._config.pointLabelFontSize+"px " + this._config.pointLabelFontFamily;
				this._context.fillStyle = this._config.pointLabelFontColor;
				var opposite = Math.sin(rotationDegree*k) * (this._maxSize + this._config.pointLabelFontSize);
				var adjacent = Math.cos(rotationDegree*k) * (this._maxSize + this._config.pointLabelFontSize);

				if(rotationDegree*k == Math.PI || rotationDegree*k === 0){
					this._context.textAlign = "center";
				}
				else if(rotationDegree*k > Math.PI){
					this._context.textAlign = "right";
				}
				else{
					this._context.textAlign = "left";
				}

				this._context.textBaseline = "middle";

				this._context.fillText(this._data.labels[k],opposite,-adjacent);

			}
			this._context.restore();
		}

		function drawGraph( number ){
			this.drawAllDataPoints( number );
		}

		var _defaultConfigs = {
			angleLineColor : "rgba(0,0,0,.1)",
			angleLineWidth : 1,
			angleShowLineOut : true,
			datasetFill : true,
			datasetStroke : true,
			datasetStrokeWidth : 2,
			pointDot : true,
			pointDotRadius : 3,
			pointDotStrokeWidth : 1,
			pointLabelFontColor : "#666",
			pointLabelFontFamily : "'Verdana'",
			pointLabelFontSize : 12,
			pointLabelFontStyle : "normal",
			scaleOverlay : true
		};

		function defaultConfigs(){
			return Chart.prototype.mergeChartConfig( Chart.defaultConfigs(), _defaultConfigs );
		}

		window.RadarChart = RadarChart;
		RadarChart.prototype.constructor = RadarChart;
		RadarChart.prototype = Object.create( ChartGrid.prototype );
		RadarChart.prototype.drawAllDataPoints = drawAllDataPoints;
		RadarChart.prototype.drawScale = drawScale;
		RadarChart.prototype.calculateDrawingSizes = calculateDrawingSizes;
		RadarChart.prototype.drawGraph = drawGraph;
		RadarChart.defaultConfigs = defaultConfigs;
	})();

	(function(){
		function LineChart(data,config,ctx){
			this._scaleHop = undefined;
			this._calculatedScale = undefined;
			this._valueBounds = undefined;
			this._labelTemplateString = undefined;
			this._widestXLabel = undefined;

			this._minimumIndex = null;
			this._maximumIndex = null;
			this._selectionMode = false;
			ChartGrid.call( this, data, config, ctx );
		}

		function drawLines(animPc){
			var allHeights = 0;

			for (var i=0; i<this._data.datasets.length; i++){

				if( this._data.datasets[i].strokeColor !== undefined ){
					this._context.fillStyle = this._data.datasets[i].fillColor;
					this._context.strokeStyle = this._data.datasets[i].strokeColor;
				} else {
					this._context.fillStyle = this._data.datasets[i].color.toRGBAStringWithAlpha( 0.1 );
					this._context.strokeStyle = this._data.datasets[i].color.toRGBAStringWithAlpha( 0.5 );
				}
				this._context.lineWidth = this._config.datasetStrokeWidth;
				this._context.beginPath();
				this._context.moveTo(this._yAxisPosX, this._xAxisPosY - animPc*(this.calculateOffset(this._data.datasets[i].data[0],this._calculatedScale,this._scaleHop)));


				for (var j=0; j<this._data.datasets[i].data.length; j++){
					if (this._config.bezierCurve){
						var yEnd = this.yPos(i,j,animPc);
						var xEnd = this.xPos(j);
						if( this._minimumIndex !== null && j == this._minimumIndex ){
							allHeights += yEnd;
						}
						if( j === 0 ){
							continue;
						}
						var yStart = this.yPos(i,j-1,animPc);
						var xStart = this.xPos(j-0.5);
						var yMid = this.yPos(i,Math.min(j+1,this._data.datasets[i].data.length-1), animPc );
						var xMid = this.xPos( j + 1 );
						this._context.bezierCurveTo(
							xStart,
							yStart,
							xStart,
							yEnd,
							xEnd,
							yEnd
							);
					}
					else{
						this._context.lineTo(this.xPos(j),this.yPos(i,j,animPc));
					}
				}
				this._context.stroke();
				if (this._config.datasetFill){
					var graphMinValue = this._calculatedScale.graphMin;
					var graphMaxValue = graphMinValue + this._calculatedScale.stepValue * this._calculatedScale.steps;
					var zeroAxisPoint = this.affineTransform( graphMinValue, 0, graphMaxValue, this._xAxisPosY, 5 );

					this._context.lineTo(this._yAxisPosX + (this._valueHop*(this._data.datasets[i].data.length-1)),zeroAxisPoint);
					this._context.lineTo(this._yAxisPosX,zeroAxisPoint);

					this._context.closePath();
					this._context.fill();
				}
				else{
					this._context.closePath();
				}
				if(this._config.pointDot){
					// this._context.fillStyle = this._data.datasets[i].pointColor;
					// this._context.strokeStyle = this._data.datasets[i].pointStrokeColor;
					var min = Math.min( this._minimumIndex, this._maximumIndex );
					var max = Math.max( this._minimumIndex, this._maximumIndex );
					this._context.lineWidth = this._config.pointDotStrokeWidth;
					for (var k=0; k<this._data.datasets[i].data.length; k++){
						if( this._data.datasets[i].pointColor !== undefined ){
							this._context.fillStyle = this._data.datasets[i].pointColor;
							this._context.strokeStyle = this._data.datasets[i].pointStrokeColor;
						} else {
							this._context.fillStyle = this._data.datasets[i].color.toRGBAStringWithAlpha( 1 );
							this._context.strokeStyle = Color.White.toString();
						}

						if( (this._selectionMode && k <= max && k >= min) || ( this._minimumIndex !== null && k == this._minimumIndex ) ){
							var a = this._context.strokeStyle;
							this._context.strokeStyle = this._context.fillStyle;
							this._context.fillStyle = a;
						}
						this._context.beginPath();
						this._context.arc(this._yAxisPosX + (this._valueHop *k),this._xAxisPosY - animPc*(this.calculateOffset(this._data.datasets[i].data[k],this._calculatedScale,this._scaleHop)),this._config.pointDotRadius,0,Math.PI*2,true);
						this._context.fill();
						this._context.stroke();
					}
				}
			}

			if( this._minimumIndex !== null ){

				var totals = [];
				var minIndex = this._selectionMode ? Math.min( this._minimumIndex, this._maximumIndex ) : this._minimumIndex;
				var maxIndex = this._selectionMode ? Math.max( this._minimumIndex, this._maximumIndex ) : this._minimumIndex;
				for (var i=0; i<this._data.datasets.length; i++){
					totals[i] = 0;
					for( var j=minIndex; j <= maxIndex; j++ ) {
						totals[i] += this._data.datasets[i].data[j];
					}
				}
				var title = this._data.labels[minIndex];
				if( this._selectionMode && minIndex != maxIndex ){
					title += ' - ' + this._data.labels[maxIndex];
				}


				var whiteColor = 'rgba(255,255,255,0.9)';
				var padding = 10;
				var boxSize = 20;
				var maxWidth = this._context.measureText( title ).width;
				var maxHeight = (totals.length + 2) * (this._labelHeight + 5);

				var total = 0;

				var topOfBox = allHeights / (totals.length + 2);
				for (var i=0; i<totals.length; i++){
					maxWidth = Math.max( maxWidth, this._context.measureText( NumberFormatter.sharedFormatter().formatCurrency( totals[i] ) ).width + boxSize );
					total += totals[i];
				}
				maxWidth = Math.max( maxWidth, this._context.measureText( NumberFormatter.sharedFormatter().formatCurrency( total ) ).width );
				topOfBox -= maxHeight / 2;
				var leftOfBox = this._yAxisPosX + ( this._valueHop * minIndex ) + (2*padding);
				if( leftOfBox - ( maxWidth + (4*padding) ) > this._yAxisPosX ){
					leftOfBox -= maxWidth + (4*padding);
				}

				this._context.fillStyle = 'rgba(0,0,0,0.75)';
				this._context.strokeStyle = whiteColor;

				if( topOfBox + maxHeight > this._xAxisPosY ){
					topOfBox -= (topOfBox + maxHeight - this._xAxisPosY)
				}

				var l = leftOfBox;
				var t = topOfBox;
				var p = padding;
				var w = maxWidth;
				var h = maxHeight;

				this._context.beginPath();
				this._context.moveTo( l, t - p );
				this._context.arc( l + w, t, p, 1.5 * Math.PI, 0, false );
				this._context.arc( l + w, t + h, p, 0, 0.5 * Math.PI, false );
				this._context.arc( l, t + h, p, 0.5 * Math.PI, Math.PI, false );
				this._context.arc( l, t, p, Math.PI, 1.5 * Math.PI, false );
				this._context.closePath();
				this._context.fill();
				// this._context.stroke();
				// this._context.fillRect( leftOfBox - padding, topOfBox - padding, maxWidth + padding + padding, maxHeight + padding + padding );

				this._context.textAlign = 'left';
				this._context.fillStyle = whiteColor;
				// this._context.strokeStyle = 'rgba(255,255,255,0.9)';

				var h = padding;
				this._context.fillText( title, leftOfBox, h + topOfBox );
				h += this._labelHeight + 5;

				var boxPadding = 5;

				for (var i=0; i<totals.length; i++){
					this._context.fillText( NumberFormatter.sharedFormatter().formatCurrency( totals[i] ), leftOfBox + boxSize, h + topOfBox );
					this._context.fillStyle = this._data.datasets[i].color.toString();
					this._context.fillRect( leftOfBox, topOfBox + h - (boxPadding*1.7), boxSize - boxPadding, boxSize - boxPadding );
					h += this._labelHeight + 5;
					this._context.fillStyle = whiteColor;
					// this._data.datasets[i].label;
				}

				this._context.fillText( NumberFormatter.sharedFormatter().formatCurrency( total ), leftOfBox, h + topOfBox );
			}
		}

		function yPos(dataSet,iteration, animPc){
			return this._xAxisPosY - animPc*(this.calculateOffset(this._data.datasets[dataSet].data[iteration],this._calculatedScale,this._scaleHop));
		}
		function xPos(iteration){
			return this._yAxisPosX + (this._valueHop * iteration);
		}
		function drawScale(){
			//X axis line
			this._context.lineWidth = this._config.scaleLineWidth;
			this._context.strokeStyle = this._config.scaleLineColor;
			this._context.beginPath();
			this._context.moveTo(this._width-widestXLabel/2+5,this._xAxisPosY);
			this._context.lineTo(this._width-(widestXLabel/2)-this._xAxisLength-5,this._xAxisPosY);
			this._context.stroke();

			if (this._rotateLabels > 0){
				this._context.save();
				this._context.textAlign = "right";
			} else{ 
				this._context.textAlign = "center";
			}
			this._context.fillStyle = this._config.scaleFontColor;
			for (var i=0; i<this._data.labels.length; i++){
				this._context.save();
				if (this._rotateLabels > 0){
					this._context.translate(this._yAxisPosX + i*this._valueHop,this._xAxisPosY + this._config.scaleFontSize);
					this._context.rotate(-(this._rotateLabels * (Math.PI/180)));
					this._context.fillText(this._data.labels[i], 0,0);
					this._context.restore();
				}

				else{
					this._context.fillText(this._data.labels[i], this._yAxisPosX + i*this._valueHop,this._xAxisPosY + this._config.scaleFontSize+3);
				}

				this._context.beginPath();
				this._context.moveTo(this._yAxisPosX + i * this._valueHop, this._xAxisPosY+3);

				//Check i isnt 0, so we dont go over the Y axis twice.
				if(this._config.scaleShowGridLines && i>0){
					// if( (this._selectionMode && i <= this._maximumIndex && i >= this._minimumIndex) || ( this._minimumIndex !== null && i == this._minimumIndex ) ){
					// 	this._context.lineWidth = this._config.scaleGridLineWidth;
					// 	this._context.strokeStyle = this._config.scaleGridLineColor;
					// 	this._context.lineTo(this._yAxisPosX + i * this._valueHop, 5);
					// }
				}
				else{
					this._context.lineTo(this._yAxisPosX + i * this._valueHop, this._xAxisPosY+3);
				}
				this._context.stroke();
			}

			if( this._selectionMode ){
				this._context.lineWidth = this._config.scaleGridLineWidth;
				this._context.strokeStyle = this._config.scaleGridLineColor;
				this._context.fillStyle = this._config.scaleGridLineColor;

				this._context.beginPath();
				this._context.moveTo( this._yAxisPosX + (this._minimumIndex * this._valueHop), this._xAxisPosY+3);
				this._context.lineTo( this._yAxisPosX + (this._minimumIndex * this._valueHop), 5);
				this._context.lineTo( this._yAxisPosX + (this._maximumIndex * this._valueHop), 5);
				this._context.lineTo( this._yAxisPosX + (this._maximumIndex * this._valueHop), this._xAxisPosY+3);
				// this._context.lineTo( this._yAxisPosX + this._minimumIndex * this._valueHop, this._xAxisPosY+3);

				this._context.closePath();

				this._context.stroke();
				this._context.fill();

			} else if( this._minimumIndex !== null ){
				this._context.beginPath();
				this._context.moveTo(this._yAxisPosX + this._minimumIndex * this._valueHop, this._xAxisPosY+3);

				this._context.lineWidth = this._config.scaleGridLineWidth;
				this._context.strokeStyle = '#ddd';//this._config.scaleGridLineColor;
				this._context.lineTo(this._yAxisPosX + this._minimumIndex * this._valueHop, 5);
				this._context.stroke();
			}

			//Y axis
			this._context.lineWidth = this._config.scaleLineWidth;
			this._context.strokeStyle = this._config.scaleLineColor;
			this._context.beginPath();
			this._context.moveTo(this._yAxisPosX,this._xAxisPosY+5);
			this._context.lineTo(this._yAxisPosX,5);
			this._context.stroke();

			this._context.textAlign = "right";
			this._context.textBaseline = "middle";
			for (var j=0; j<this._calculatedScale.steps; j++){
				this._context.beginPath();
				this._context.moveTo(this._yAxisPosX-3,this._xAxisPosY - ((j+1) * this._scaleHop));
				if (this._config.scaleShowGridLines){
					this._context.lineWidth = this._config.scaleGridLineWidth;
					this._context.strokeStyle = this._config.scaleGridLineColor;
					this._context.lineTo(this._yAxisPosX + this._xAxisLength + 5,this._xAxisPosY - ((j+1) * this._scaleHop));
				}
				else{
					this._context.lineTo(this._yAxisPosX-0.5,this._xAxisPosY - ((j+1) * this._scaleHop));
				}

				this._context.stroke();

				if (this._config.scaleShowLabels){
					this._context.fillStyle = this._config.scaleFontColor;
					this._context.fillText(NumberFormatter.sharedFormatter().formatCurrency( this._calculatedScale.labels[j] ),this._yAxisPosX-8,this._xAxisPosY - ((j+1) * this._scaleHop));
				}
			}
		}

		function drawGraph( number ){
			return this.drawLines( number );
		}

		var _defaultConfigs = {
			bezierCurve : true,
			pointDot : true,
			pointDotRadius : 2,
			pointDotStrokeWidth : 0.5,
			datasetStroke : true,
			datasetStrokeWidth : 2,
			datasetFill : true
		};

		function mouseMovedTo( point ) {
			// console.log( point );
			if( this._context.canvas.chart !== this ){
				return;
			}
			var x = point.x;
			var l = this._yAxisPosX;
			var currentValue =  this._selectionMode ? this._maximumIndex : this._minimumIndex;
			var closeEnough = false;
			for( var i = 0; i < this._data.labels.length; i++ ){
				if( Math.abs( x - l ) < 5 ){
					//close enough to a point
					closeEnough = true;
					if( this._selectionMode ){
						this._maximumIndex = i;
					} else {
						this._minimumIndex = i;
					}
					break;
				}
				l += this._valueHop;
			}

			if( !closeEnough && !this._selectionMode ){
				this._minimumIndex = null;
			}

			if( (currentValue !== this._minimumIndex && !this._selectionMode) || (currentValue !== this._maximumIndex && this._selectionMode) ){
				if( window.previousDevicePixelRatio != window.devicePixelRatio ) {
					this._context.canvas.height = this._height * window.devicePixelRatio;
					this._context.canvas.width = this._width * window.devicePixelRatio;
					this._context.scale(window.devicePixelRatio, window.devicePixelRatio);
				}

				this.clear( this._context );
				this.drawScale();
				this.drawGraph( 1 );
			}
		}

		function mouseActivatedOn( point ){
			if( this._selectionMode ){
				this._selectionMode = false;
				this._maximumIndex = null;
				this.mouseMovedTo( point );
				return;
			}

			this.mouseMovedTo( point );
			if( this._minimumIndex !== null ){
				this._selectionMode = true;
				this._maximumIndex = this._minimumIndex;
			}
		}

		function defaultConfigs(){
			return Chart.prototype.mergeChartConfig( ChartGrid.defaultConfigs(), _defaultConfigs );
		}

		window.LineChart = LineChart;
		LineChart.prototype.constructor = LineChart;
		LineChart.prototype = Object.create( ChartGrid.prototype );
		LineChart.prototype.drawLines = drawLines;
		LineChart.prototype.yPos = yPos;
		LineChart.prototype.xPos = xPos;
		LineChart.prototype.drawScale = drawScale;
		LineChart.prototype.drawGraph = drawGraph;
		LineChart.prototype.mouseMovedTo = mouseMovedTo;
		LineChart.prototype.mouseActivatedOn = mouseActivatedOn;
		LineChart.defaultConfigs = defaultConfigs;
	})();

	(function(){
		function HBarChart( data, config, ctx ){

			ChartGrid.call( this, data, config, ctx );
		}

		function drawScale(){
			// console.log('drawScale', this );
		}

		function drawGraph( number ){
			// console.log('drawGraph', this );
		}

		var _defaultConfigs = {

		};

		function defaultConfigs(){
			return Chart.prototype.mergeChartConfig( ChartGrid.defaultConfigs(), _defaultConfigs );
		}

		window.HBarChart = HBarChart;
		HBarChart.prototype.constructor = HBarChart;
		HBarChart.prototype = Object.create( ChartGrid.prototype );
		HBarChart.prototype.drawScale = drawScale;
		HBarChart.prototype.drawGraph = drawGraph;
		HBarChart.defaultConfigs = defaultConfigs;
	})();

	(function(){
		function BarChart(data,config,ctx){
			this._scaleHop = undefined;
			this._calculatedScale = undefined;
			this._valueBounds = undefined;
			this._labelTemplateString = undefined;
			this._valueHop = undefined;
			this._widestXLabel = undefined;
			ChartGrid.call( this, data, config, ctx );
		}

		function drawBars(animation) {
			// this._context.canvas.width = this._width * window.devicePixelRatio;
			// this._context.scale(window.devicePixelRatio, window.devicePixelRatio);
			this._context.lineWidth = this._config.barStrokeWidth;

			var graphMinValue = this._calculatedScale.graphMin;
			var graphMaxValue = graphMinValue + this._calculatedScale.stepValue * this._calculatedScale.steps;
			var zeroAxisPoint = this.affineTransform( graphMinValue, 0, graphMaxValue, this._xAxisPosY, 5 );
			for (var i=0; i<this._data.datasets.length; i++){
				var color;
				if( this._data.datasets[i].fillColor !== undefined ){
					this._context.fillStyle = this._data.datasets[i].fillColor;
					this._context.strokeStyle = this._data.datasets[i].strokeColor;
				} else if ( this._data.datasets[i].color !== undefined ){
					color = this._data.datasets[i].color;
					this._context.fillStyle = color.toRGBAStringWithAlpha( 0.1 );
					this._context.strokeStyle = color.toString();
				}
				for (var j=0; j<this._data.datasets[i].data.length; j++){
					var barOffset = this._yAxisPosX + this._config.barValueSpacing + this._valueHop*j + this._barWidth*i + this._config.barDatasetSpacing*i + this._config.barStrokeWidth*i;

					this._context.beginPath();
					this._context.moveTo(barOffset, zeroAxisPoint);
					this._context.lineTo(barOffset, this._xAxisPosY - animPc*this.calculateOffset(this._data.datasets[i].data[j],this._calculatedScale,this._scaleHop)+(this._config.barStrokeWidth/2));
					this._context.lineTo(barOffset + this._barWidth, this._xAxisPosY - animPc*this.calculateOffset(this._data.datasets[i].data[j],this._calculatedScale,this._scaleHop)+(this._config.barStrokeWidth/2));
					this._context.lineTo(barOffset + this._barWidth, zeroAxisPoint);
					if(this._config.barShowStroke){
						this._context.stroke();
					}
					this._context.closePath();
					this._context.fill();
				}
			}

		}
		function drawScale(){

			//X axis line
			this._context.lineWidth = this._config.scaleLineWidth;
			this._context.strokeStyle = this._config.scaleLineColor;
			this._context.beginPath();
			this._context.moveTo(this._width-widestXLabel/2+5,this._xAxisPosY);
			this._context.lineTo(this._width-(widestXLabel/2)-this._xAxisLength-5,this._xAxisPosY);
			this._context.stroke();


			if (this._rotateLabels > 0){
				this._context.save();
				this._context.textAlign = "right";
			}
			else{
				this._context.textAlign = "center";
			}
			this._context.fillStyle = this._config.scaleFontColor;
			for (var i=0; i<this._data.labels.length; i++){
				this._context.save();
				if (this._rotateLabels > 0){
					this._context.translate(this._yAxisPosX + i*this._valueHop,this._xAxisPosY + this._config.scaleFontSize);
					this._context.rotate(-(this._rotateLabels * (Math.PI/180)));
					this._context.fillText(this._data.labels[i], 0,0);
					this._context.restore();
				}

				else{
					this._context.fillText(this._data.labels[i], this._yAxisPosX + i*this._valueHop + this._valueHop/2,this._xAxisPosY + this._config.scaleFontSize+3);
				}

				this._context.beginPath();
				this._context.moveTo(this._yAxisPosX + (i+1) * this._valueHop, this._xAxisPosY+3);

				//Check i isnt 0, so we dont go over the Y axis twice.
				this._context.lineWidth = this._config.scaleGridLineWidth;
				this._context.strokeStyle = this._config.scaleGridLineColor;
				this._context.lineTo(this._yAxisPosX + (i+1) * this._valueHop, 5);
				this._context.stroke();
			}

			//Y axis
			this._context.lineWidth = this._config.scaleLineWidth;
			this._context.strokeStyle = this._config.scaleLineColor;
			this._context.beginPath();
			this._context.moveTo(this._yAxisPosX,this._xAxisPosY+5);
			this._context.lineTo(this._yAxisPosX,5);
			this._context.stroke();

			this._context.textAlign = "right";
			this._context.textBaseline = "middle";
			for (var j=0; j<this._calculatedScale.steps; j++){
				this._context.beginPath();
				this._context.moveTo(this._yAxisPosX-3,this._xAxisPosY - ((j+1) * this._scaleHop));
				if (this._config.scaleShowGridLines){
					this._context.lineWidth = this._config.scaleGridLineWidth;
					this._context.strokeStyle = this._config.scaleGridLineColor;
					this._context.lineTo(this._yAxisPosX + this._xAxisLength + 5,this._xAxisPosY - ((j+1) * this._scaleHop));
				}
				else{
					this._context.lineTo(this._yAxisPosX-0.5,this._xAxisPosY - ((j+1) * this._scaleHop));
				}

				this._context.stroke();
				if (this._config.scaleShowLabels){
					this._context.fillText(this._calculatedScale.labels[j],this._yAxisPosX-8,this._xAxisPosY - ((j+1) * this._scaleHop));
				}
			}
		}

		function drawGraph( number ){
			return this.drawBars( number );
		}

		var _defaultConfigs = {
			barShowStroke : true,
			barStrokeWidth : 2,
			barValueSpacing : 5,
			barDatasetSpacing : 1
		};

		function defaultConfigs(){
			return Chart.prototype.mergeChartConfig( ChartGrid.defaultConfigs(), _defaultConfigs );
		}

		window.BarChart = BarChart;
		BarChart.prototype.constructor = BarChart;
		BarChart.prototype = Object.create( ChartGrid.prototype );
		BarChart.prototype.drawBars = drawBars;
		BarChart.prototype.drawScale = drawScale;
		BarChart.prototype.drawGraph = drawGraph;
		BarChart.defaultConfigs = defaultConfigs;
	})();

	//Javascript micro templating by John Resig - source at http://ejohn.org/blog/javascript-micro-templating/
	var cache = {};

	function tmpl(str, data){
		// Figure out if we're getting a template, or if we need to
		// load the template - and be sure to cache the result.
		var fn = !/\W/.test(str) ?
		cache[str] = cache[str] ||
		tmpl(document.getElementById(str).innerHTML) :

			// Generate a reusable function that will serve as a template
			// generator (and which will be cached).
			new Function("obj",
				"var p=[],print=function(){p.push.apply(p,arguments);};" +

			// Introduce the data as local variables using with(){}
			"with(obj){p.push('" +

				// Convert the template into pure JavaScript
				str
				.replace(/[\r\t\n]/g, " ")
				.split("<%").join("\t")
				.replace(/((^|%>)[^\t]*)'/g, "$1\r")
				.replace(/\t=(.*?)%>/g, "',$1,'")
				.split("\t").join("');")
				.split("%>").join("p.push('")
					.split("\r").join("\\'") + "');}return p.join('');");

		// Provide some basic currying to the user
		return data ? fn( data ) : fn;
	}
})();