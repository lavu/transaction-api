<?php

class sister_report_types {
	const
		SELECT = 0,
		WHERE = 1,
		GROUPBY = 3,
		UNKNOWN = 4;
}

class sister_report { 
	private
		$type,
		$options,
		$value,
		$defaultValue;

		private function __construct( $type, $str ){
			$this->options = array();
			$this->type = $type;


			$typeStr = $this->typeToString( $type );
			$overwrite=reqvar("sister_{$typeStr}", '');
			// echo 'overwrite: ' . $overwrite . '<br />';
			preg_match("/sister\('([^'\)]+)'\)/", $str, $a_matches );
			if( count( $a_matches ) ){
				$portions = explode(',', $a_matches[1] );
				$this->defaultValue = $this->value = $portions[0];
				if( $overwrite && in_array($overwrite, $portions ) ){
					$this->value = $overwrite;
				}

				foreach( $portions as $key => $portion ){
					if( !in_array($portion, $this->options) ){
						$this->options[] = $portion;
					}
				}
				
			}
		}

		public function type(){
			return $this->type;
		}

		public function options(){
			return $this->options;
		}

		public function value(){
			return $this->value;
		}

		public function defaultValue(){
			return $this->defaultValue;
		}

		private function typeToString(){
			switch( $this->type ){
				case sister_report_types::SELECT :
					return 'select';
				case sister_report_types::WHERE :
					return 'where';
				case sister_report_types::GROUPBY :
					return 'groupby';
					break;
				default :
					return 'unknown';
			}
		}

		public function toReqVar(){
			$result = 'sister_' . $this->typeToString();
			$result .= urlencode( $this->value() );
		}

		private static $existing = array();

		public static function sister( $type, $str='' ){
			switch ( strtolower($type) ) {
				case 'select':
					$type = sister_report_types::SELECT;
					break;
				case 'where':
					$type = sister_report_types::WHERE;
					break;
				case 'groupby':
					$type = sister_report_types::GROUPBY;
					break;			
				default:
					$type = sister_report_types::UNKNOWN;
					break;
			}
			if( !isset( sister_report::$existing[$type] ) ){
				if( !$str ){
					return new sister_report('','');
				}

				$result = new sister_report( $type, $str );
				sister_report::$existing[$type] = $result;
			}
			return sister_report::$existing[$type];
		}
}