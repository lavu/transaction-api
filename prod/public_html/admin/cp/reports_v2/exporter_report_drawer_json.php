<?php
require_once dirname(__FILE__) . '/iarea_report_drawer.php';
require_once dirname(__FILE__) . '/area_reports.php';
require_once dirname(__FILE__) . '/report_functions.php';

class exporter_report_drawer_json implements iarea_report_drawer {
	protected
		$area_reports;

	public function __construct( $area_reports ){
		$this->area_reports = $area_reports;
	}

	public function draw_multiple_rows( $report_results ) {

		require_once dirname(dirname(__FILE__)).'/resources/json.php';

		return LavuJson::json_encode( $report_results );

		// $result = array();
		// for($m=0;$m<count($report_results); $m++){
		// 	if( isset($report_results['location_names']) && !isset( $report_results['location_names'][$m] )){
		// 		continue;
		// 	}
		// 	if( isset($report_results['location_names']) && count( $report_results ) > 1 ){
		// 		$export_str .= $this->escape_value( $report_results['location_names'][$m] ) . $this->getRowDelimeter();
		// 	}
		// 	if( count( $report_results[$m] ) > 0 ){
		// 		for($n=0; $n<count($report_results[$m]); $n++){
		// 			$rowstr = "";
		// 			$report_read = $report_results[$m][$n];
		// 			if( $n === 0 ){
		// 				foreach($report_read as $key => $val) { // Output a row
		// 					if(strpos($val,"[drill_down]")!==false) {
		// 					} else if($this->area_reports->should_show_col($key)){
		// 						if($rowstr!=""){
		// 							$rowstr .= $this->getColDelimeter();
		// 						}
		// 						$rowstr .= $this->escape_value(show_as_title($key));
		// 					}
		// 				}

		// 				$export_str .= $rowstr;
		// 				$rowstr = '';
		// 			}

		// 			foreach($report_read as $key => $val) { // Output a row
		// 				if(strpos($val,"[drill_down]")!==false) {
		// 				} else if($this->area_reports->should_show_col($key)){

		// 					if( is_numeric( $val ) ){
		// 						if( $val * 1 == 0 ){
		// 							$val = abs( $val * 1 );
		// 						}
		// 					} else if( report_value_is_money( $val ) ) {
		// 						$nval = report_money_to_number( $val );
		// 						if( $nval == 0 ){
		// 							$val = report_money_output( abs( $nval ) );
		// 						}
		// 					}
		// 					if($rowstr!=""){
		// 						$rowstr .= $this->getColDelimeter();
		// 					}
		// 					$new_value = $this->area_reports->show_reported_value($val);
		// 					$rowstr .= $this->escape_value( $new_value );
		// 				}
		// 			}
		// 			if($rowstr!=""){
		// 				if($export_str!=""){
		// 					$export_str .= $this->getRowDelimeter();
		// 				}
		// 				$export_str .= $rowstr;
		// 			}
		// 		}

		// 	} else {
		// 		if( isset( $report_results['location_names'][$m] )){
		// 			$export_str .= $this->escape_value( 'No Records Found' ) . $this->getRowDelimeter();
		// 		}
		// 	}

		// 	$export_str .=  $this->getRowDelimeter() . $this->getRowDelimeter();
		// }
		// $this->output( $export_str );
	}

	public function draw_single_row( $report_results ) {
		return $this->draw_multiple_rows( $report_results );
	}

	public function draw_no_data() {
		$this->output( $this->escape_value( speak("No Records Found") ) );
	}
}