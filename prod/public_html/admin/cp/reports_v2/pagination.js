var targetBody = '.make-scrollable-report';
var urlData = '';
var pageNavigation = {};
var dataName = '';
var previousPageClass = 'previous-page';
var nextPageClass = 'next-page';
var nextPageNumber = 'next-page-number';
var previousPageNumber = 'previous-page-number';
var activePageClass = 'active-page';
var ulClass = 'reports-pagination-ul';
var classHidden = 'hidden';
var nextPreviousDisableClass = 'action-disabled';
var printableClass = 'button.printable';
var exportableClass = 'button.export';
var servingRequest = false;
var noOfRecordsPerPage = 0;
var reportlimit = 0;
var reportOffset = 0;
var ajaxServerDirectory = "./reports_v2/ajax_requests/";
var sort = 'asc';
var sortColumn = 'dataname';
var nextPreviousRows = function (element, type) {
    var parentUl = element.parent().closest('ul');
    var splitNumberCount = parseInt(parentUl.data('split-number-count'));
    var target = parentUl.find('li').not('.' + classHidden).not('.' + nextPageClass).not('.' + nextPageNumber).not('.' + previousPageNumber).not('.' + previousPageClass);
    switch (type) {
        case 'pre':
            var targetElement = target.first();
            var i = targetElement.index() - 1;
            parentUl.find('li').not('.' + nextPageClass).not('.' + previousPageClass).not('.' + nextPageNumber).not('.' + previousPageNumber).addClass(classHidden);
            var till = (i - splitNumberCount);
            for (i; i > till; i--) {
                $(parentUl).find('li:eq(' + i + ')').removeClass(classHidden);
            }
            enableDisablePrevNext(parentUl);
            parentUl.find('li').not('.' + classHidden).not('.' + nextPageClass).not('.' + nextPageNumber).not('.' + previousPageNumber).not('.' + previousPageClass).last().trigger('click');
            break;

        case 'previous-number':
            var targetElement = parentUl.find('.' + activePageClass).prev();
            if (targetElement.hasClass(classHidden)) {
                nextPreviousRows(element, 'pre');
            } else if (targetElement.length) {
                targetElement.trigger('click');
            }
            enableDisablePrevNext(parentUl);
            break;

        case 'next-number':
            var targetElement = parentUl.find('.' + activePageClass).next();
            if (targetElement.hasClass(classHidden)) {
                nextPreviousRows(element, 'next');
            } else if (targetElement.length) {
                targetElement.trigger('click');
            }
            enableDisablePrevNext(parentUl);
            break;

        case 'next':
            var targetElement = target.last();
            var i = targetElement.index() + 1;
            parentUl.find('li').not('.' + nextPageClass).not('.' + previousPageClass).not('.' + nextPageNumber).not('.' + previousPageNumber).addClass(classHidden);
            var till = (i + splitNumberCount);
            for (i; i < till; i++) {
                $(parentUl).find('li:eq(' + i + ')').removeClass(classHidden);
            }
            enableDisablePrevNext(parentUl);
            parentUl.find('li').not('.' + classHidden).not('.' + nextPageClass).not('.' + nextPageNumber).not('.' + previousPageNumber).not('.' + previousPageClass).first().trigger('click');
            break;
    }
}

var nextPrevious = function (element) {
    if(!$(element).hasClass(nextPreviousDisableClass)) {
        if ($(element).hasClass(previousPageClass)) {
            nextPreviousRows(element, 'pre');
        } else if ($(element).hasClass(nextPageClass)) {
            nextPreviousRows(element, 'next');
        } else if ($(element).hasClass(nextPageNumber)) {
            nextPreviousRows(element, 'next-number');
        } else if ($(element).hasClass(previousPageNumber)) {
            nextPreviousRows(element, 'previous-number');
        }
    }
};

var enableDisablePrevNext = function (parentUl) {
    var list = parentUl.find('li').not('.' + nextPageClass).not('.' + previousPageClass).not('.' + nextPageNumber).not('.' + previousPageNumber);
    var activeList = parentUl.find('li').not('.' + nextPageClass).not('.' + previousPageClass).not('.' + nextPageNumber).not('.' + previousPageNumber).not('.' + classHidden);
    var lastChild = list.last();
    var first = list.first();
    if (!lastChild.hasClass(classHidden)) {
        parentUl.find('.' + nextPageClass).addClass(nextPreviousDisableClass);
    } else {
        parentUl.find('.' + nextPageClass).removeClass(nextPreviousDisableClass);
    }

    var activelistFirst = activeList.first();
    if (activelistFirst.hasClass(activePageClass) && first.hasClass(activePageClass)) {
        parentUl.find('.' + previousPageNumber).addClass(nextPreviousDisableClass);
    } else {
        parentUl.find('.' + previousPageNumber).removeClass(nextPreviousDisableClass);
    }

    var activelistLast = activeList.last();
    if (activelistLast.hasClass(activePageClass) && lastChild.hasClass(activePageClass)) {
        parentUl.find('.' + nextPageNumber).addClass(nextPreviousDisableClass);
    } else {
        parentUl.find('.' + nextPageNumber).removeClass(nextPreviousDisableClass);
    }

    if (!first.hasClass(classHidden)) {
        parentUl.find('.' + previousPageClass).addClass(nextPreviousDisableClass);
    } else {
        parentUl.find('.' + previousPageClass).removeClass(nextPreviousDisableClass);
    }
}

var enableDisabeElement = function (target, status) {
    status = status || true;
    var statusClass = "action-disabled";
    if (status) {
        $('.' + target).addClass(statusClass);
    } else {
        $('.' + target).removeClass(statusClass);
    }
}

var loadReportPage = function (element, page, limit, offset, location) {
    var isServing = false;
    if (pageNavigation[location] && pageNavigation[location].servingRequest) {
        isServing = true;
    }
    if (!isServing) {
        reportlimit = parseInt(limit);
        pageNavigation[location]['activePage'] = parseInt(page);
        pageNavigation[location]['reportOffset'] = parseInt(offset);
        var offsetDetails = getOffset(location);
        if (element) {
            $(element).parent().find('li').removeClass('active-page');
            $(element).addClass('active-page');
        }
        loadData(offsetDetails.offset, 'page=' + page + urlData, true, offsetDetails.pageLimit, offsetDetails.pageOffset, location);
    }
}

var getOffset = function (location) {
    var endOffset = (pageNavigation[location].activePage * reportlimit) + 1;
    var pageOffset = (pageNavigation[location].activePage * reportlimit) - reportlimit;
    var pageLimit = reportlimit;
    var offset = ((pageNavigation[location].activePage * reportlimit) - reportlimit + 1) + pageNavigation[location].reportOffset;
    return {
        pageLimit: pageLimit,
        pageOffset: pageOffset,
        offset: offset,
        endOffset: endOffset,
    }
}

var testOffset = function (location) {
    var offsetDetails = getOffset(location);
    if (offsetDetails.offset < offsetDetails.endOffset) {
        return offsetDetails;
    } else {
        return false;
    }
}

function initPagination(url, recolors) {
    urlData = url ? url.replace('?', '&') : '';
    pageNavigation = {};
    if ($(targetBody).length && $('.'+ulClass).length) {
        var configUrl = ajaxServerDirectory + 'fetch_pagination_config.php';
        isPaginationSet = true;
        $.ajax({
            url : configUrl,
            method: 'GET',
            dataType: 'JSON',
            success: function(response) {
                if(response) {
                    var recordLimit = response.limit;
                    noOfRecordsPerPage = recordLimit;
                    reportlimit = recordLimit;
                    reportOffset = recordLimit;
                    $(targetBody).each(function () {
                        var location = $(this).data('location');
                        if (!pageNavigation[location]) {
                            pageNavigation[location] = {};
                        }
                        pageNavigation[location]['activePage'] = 1;
                        pageNavigation[location]['reportOffset'] = reportlimit;
                    });
                }
            }
        });
    }
}

var getPreviousBalance = function (targetTableBody, sequenceNumber) {
    return parseFloat($(targetTableBody).closest('.report_body').parent().find("tfoot tr td:eq(" + sequenceNumber + ")").text().replace(",", ""))
}

var changePreviousBalance = function (targetTableBody, sequenceNumber, data) {
    $(targetTableBody).closest('.report_body').parent().find("tfoot tr td:eq(" + sequenceNumber + ")").html(data);
}

var loadingTable = function (targetBody, status) {
    var loadingRowClass = 'loading-report';
    var loaderRowExsist = $(targetBody).find('.' + loadingRowClass);

    if (status === true) {
        if (!loaderRowExsist.length) {
            var loadingText = "Loading please wait...";
            var imageLoader = "<img src='/cp/images/loading_transparent.gif' />"
            var loaderRow = '<tr class="' + loadingRowClass + '"><td><div class="table-loader"><div class="table-loader-text">' + imageLoader + '</div></div></td></tr>';
            $(targetBody).append(loaderRow);
        }

    } else if (loaderRowExsist && status === false) {
        loaderRowExsist.remove();
    }
}

var flushCounts = function (targetTableBody) {
    changePreviousBalance(targetTableBody, 1, 0);
    changePreviousBalance(targetTableBody, 3, 0);
}

var restTableContents = function(targetTableBody) {
    flushCounts(targetTableBody);
    $(targetTableBody).html('');
}

var loadData = function (offset, mode, replace, pageLimit, pageOffset, locationName) {
    var targetTableBody = targetBody;
    var cleanTableBeforAppend = replace || false;
    if (locationName) {
        targetTableBody = $(targetTableBody + '[data-location="' + locationName + '"]');
    } else {
        targetTableBody = $(targetTableBody);
    }
    var limit = reportlimit;
    var startMonth = $("#month option:selected").html();
    var endYear = $("#year option:selected").html();
    var url = ajaxServerDirectory + 'gift_and_loyality_separate_location.php?' + mode;
    loadingTable($('#report_container'), true);

    var data = {
        limit: limit,
        offset: offset,
        startMonth: startMonth,
        startYear: endYear,
        page: pageNavigation[locationName].activePage,
        pageLimit: pageLimit,
        pageOffset: pageOffset,
        locationName: locationName,
        sort: sort,
        sortColumn: sortColumn,
    };

    pageNavigation[locationName].reportOffset = parseInt(pageNavigation[locationName].reportOffset) + parseInt(reportlimit);
    pageNavigation[locationName]['servingRequest'] = true;
    $.ajax({
        type: "GET",
        url: url,
        data: data,
        success: function (res) {
            pageNavigation[locationName]['servingRequest'] = false;
            try {
                if (res && res !== 'null') {
                    var result = JSON.parse(res);
                    var followColorRowclass = $(targetTableBody).find('tr:last-child').attr('alt') === "" ? false : true;
                    if (result.data && result.data.length) {
                        if (cleanTableBeforAppend) {
                            restTableContents(targetTableBody);
                        }
                        $.each(result.data, function (i) {
                            var tableRow = (function (data, coloredRow) {
                                var tableRow = '';
                                if (data) {
                                    tableRow += "<tr " + (coloredRow ? 'alt' : '') + ">";
                                    tableRow += "<td valign = 'top' class='alignLeft'>" + data['account_number'] + "<<space>></td>";
                                    tableRow += "<td valign = 'top' class='alignRight' recolor='gift_balance'>" + data['gift_balance'] + "<<space>></td>";
                                    tableRow += "<td valign = 'top' class='alignLeft'>" + data['dataname'] + "<<space>></td>";
                                    tableRow += "<td valign = 'top' class='alignRight' recolor='loyalty_points'>" + data['loyalty_points'] + "<<space>></td>";
                                    tableRow += '</tr>';
                                    tableRow = tableRow.replace(/<<space>>/g, '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;');
                                }
                                return tableRow;
                            })(result.data[i], followColorRowclass);
                            followColorRowclass = followColorRowclass === true ? false : true;
                            $(targetTableBody).append(tableRow);
                        });
                        changePreviousBalance(targetTableBody, 1, result.counts.giftCardBalance);
                        changePreviousBalance(targetTableBody, 3, result.counts.loyalityBalance);
                    } else if (cleanTableBeforAppend) {
                        restTableContents(targetTableBody);
                        noRecordFound(targetTableBody);
                    }
                    setTimeout(function(){
                        loadingTable($('#report_container'), false);
                    }, 500);
                }
            } catch (error) {
                if (cleanTableBeforAppend) {
                    restTableContents(targetTableBody);
                    noRecordFound(targetTableBody);
                }
                loadingTable($('#report_container'), false);
            }

        },
        error: function (err) {
            loadingTable(targetTableBody, false);
            alert('Error in loading report. Please try reloading again!');
        }
    });
}

var noRecordFound = function (targetTableBody) {
    $(targetTableBody).append('<tr><td colspan="4"><center>No records found!</center></td></tr>');
}

function sortTable(location, sortColumnName) {
    sortColumnName = sortColumnName || sortColumn;
    if (
        pageNavigation[location] &&
        pageNavigation[location]['headerOrder'] &&
        !pageNavigation[location]['headerOrder'][sortColumnName]
    ) {
        pageNavigation[location]['headerOrder'][sortColumnName] = true;
    } else {
        if (!pageNavigation[location]['headerOrder']) {
            pageNavigation[location]['headerOrder'] = {};
        }
        pageNavigation[location]['headerOrder'][sortColumnName] = false;
    }
    if(pageNavigation[location]['headerOrder'][sortColumnName] === false) {
        sort = "desc";
    } else {
        sort = "asc";
    }
    sortColumn = sortColumnName;
    var activePage = $('table[data-pagination-for="' + location + '"]').find('.active-page');
    if (activePage.length) {
        activePage.trigger('click');
    } else {
        var offsetDetails = getOffset(location);
        loadData(0, 'page=1'+ urlData, true, offsetDetails.pageLimit, offsetDetails.pageOffset, location);
    }
    
}

function isNumber(n) {
    return /^-?[\d.]+(?:e-?\d+)?$/.test(n);
}

// on document ready init lazy loader
try {
    $(document).ready(function () {
        initPagination();
    });
} catch (error) {
    console.log(error);
}