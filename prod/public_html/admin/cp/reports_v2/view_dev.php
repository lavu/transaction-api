<?php
	session_start();

	require_once($_SERVER['DOCUMENT_ROOT']."/cp/resources/core_functions.php");
	require_once(dirname(__FILE__) . "/report_functions.php");
	require_once(dirname(__FILE__) . "/area_reports.php");

	$output_mode = (isset($_GET['output_mode']))?$_GET['output_mode']:"";
	$hstr = "";
	$hstr .= "<script language='javascript' src='ajax_lib.js'></script>";
	$hstr .= "<script language='javascript' src='floating_window.js'></script>";
	$hstr .= "<script type='text/javascript' src='/cp/resources/tigra/tcal.js'></script>";
	$hstr .= "<script type='text/javascript' src='chart.js'></script>";
	$hstr .= "<link rel='stylesheet' type='text/css' href='/cp/resources/tigra/tcal.css' />";
	$hstr .= "<link rel='stylesheet' type='text/css' href='styles/reports_v2.css' />";
	$hstr .= "<style>";
	$hstr .= "body, table { font-family:Verdana,Arial; font-size:10px; } ";
	$hstr .= "</style>";

	$loggedin = (isset($_SESSION['posadmin_loggedin']))?$_SESSION['posadmin_loggedin']:false;
	if($loggedin=="") $loggedin = false;
	$loggedin_fullname = (isset($_SESSION['posadmin_fullname']))?$_SESSION['posadmin_fullname']:false;
	if($loggedin_fullname=="") $loggedin_fullname = $loggedin;
	$loggedin_email = (isset($_SESSION['posadmin_email']))?$_SESSION['posadmin_email']:false;
	if($loggedin_email=="") $loggedin_email = $loggedin;
	$loggedin_access = (isset($_SESSION['posadmin_access']))?$_SESSION['posadmin_access']:false;
	if($loggedin_access=="") $loggedin_access = "";
	$loggedin_lavu_admin = (isset($_SESSION['posadmin_lavu_admin']))?$_SESSION['posadmin_lavu_admin']:0;

	function can_access($area)
	{
		global $loggedin_access;
		if(trim(strtolower($loggedin_access))=="all")
			return true;
		else if(strpos($loggedin_access,$area)!==false)
			return true;
		else
			return false;
	}

	if(1)
	{
		global $data_name;

		if( isset( $_GET['dataname'] ) ){
			$data_name = $_GET['dataname'];
			set_sessvar("admin_dataname",$data_name);
		} else {
			$data_name = sessvar('admin_dataname');
		}

		$report = ( (isset( $_GET['report'] )&&!empty( $_GET['report'] ) ) ? $_GET['report'] : "test_report" );

		if($output_mode=="")
		{
			$ar = new area_reports(array("report"=>$report, 'display_query' => 1 ));
			echo <<<HTML
			{$hstr}
			<style>
				.ajax_overlay_greyed_out {
					opacity:.40;
					filter: alpha(opacity=40);
					-moz-opacity: 0.4;
					width:100%;
				}
				.ajax_overlay {
				}
			</style>
			<table width='100%'>
				<tbody>
					<tr>
						<td width='100%' align='center' valign='top'>
							<div id='report_container'>
								{$ar->show_query_table()}
							</div>
						</td>
					</tr>
				</tbody>
			</table>
			<script type="text/javascript">
			(function(){
				function observationHandler( arg1, arg2 ){
					var win = document.defaultView.parent;

					var skip = true;
					for( var i = 0; i < arg1.length; i++ ){
						skip = skip && (arg1[i].target.tagName != 'CANVAS');
					}

					if( !skip ){
						return;
					}


					if( win['iframe_rescale'] !== undefined ){
						win['iframe_rescale']();
					}
					redrawCanvases();
				}

				var config = { childList: true, attributes: true, characterData: true, subtree: true, attributeOldValue: true, characterDataOldValue: true };
				var ob = new MutationObserver( observationHandler );
				ob.observe( document.getElementById('report_container'), config);

				function redrawCanvases(){
					// var canvases = document.getElementsByTagName( 'canvas' );
					// for( var i = 0; i < canvases.length; i++ ){
					// 	var canvas = canvases[i];
					// 	var ctx = canvas.getContext('2d');
					// 	var chart = new Chart( ctx );
					// 	canvas.chart = chart;
					// 	var data = JSON.parse( canvas.getAttribute('data') );
					// 	chart.Line( data );
					// }
				}
			})();
				
			</script>
HTML;
		}
		else // For Export
		{
			$ar = new area_reports(array("report"=>$report,"output_mode"=>$output_mode));
			$export_str = $ar->show_query_table();
			$ar->output_export_string("exportfile.csv",$export_str);
		}
	}
?>
