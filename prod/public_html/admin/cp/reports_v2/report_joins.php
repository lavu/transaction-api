<?php
	//require_once(dirname(__FILE__) . "/defs.php");
	
	class report_join {
		private
			$fromTable,
			$toTable,
			$fieldMap,
			$conditions;

		private function __construct(){
			$this->fromTable = null;
			$this->toTable = null;
			$this->fieldMap = array();
			$this->conditions = array();
		}

		public function fromTable(){
			return $this->fromTable;
		}

		public function toTable(){
			return $this->toTable;
		}

		public function hasFields(){
			return count($this->fieldMap) > 0;
		}

		/**
		 * Considers the string that should be to the right of the JOIN statement
		 *
		 * @example orders.order_id = order_contents.order_id AND orders.location_id = order_contents.loc_id
		 * @param  string $str the string to parse into a report_join
		 * @return report_join      the report join that should accurately represent this join statement
		 */
		public static function parseJoinStr( $line ){
			preg_match_all("/\s*(`?([\w_@#]+)`?\.`?([\w_@#]+)`?|('[^']+'))\s*=\s*(`?([\w_@#]+)`?\.`?([\w_@#]+)`?|('[^']+'))\s*(\w+)?/", $line, $a_matches );
			$result = null;
			// Put custom query in double quotes like ( menu_categories."IF (menu_items.super_group_id = '', menu_categories.super_group_id, menu_items.super_group_id)" = super_groups.id)
            // If we use custom condition query then get empty array so gain check with new preg match
            if ( empty($a_matches[0]) && !count( $a_matches[0]) ) {
				preg_match_all("/\s*(`?([\w_@#]+)`?\.?([\w_@#]+|\"[^#\"]+\"?)|('[^']+'))\s*=\s*(`?([\w_@#]+)`?\.?([\w_@#]+|\"[^\"]+\"?)|('[^']+'))\s*(\w+)?/", $line, $a_matches );            	
                if ( !empty($a_matches[0]) && count( $a_matches[0]) ) {
                    $matches_count = count( $a_matches[0] );
                    for( $i = 0; $i < $matches_count; $i++ ){
                        $a_matches[4][$i] = (strpos($a_matches[3][$i], '"') !== false) ? trim($a_matches[3][$i], '"')  : '';
                        $a_matches[8][$i] = (strpos($a_matches[7][$i], '"') !== false) ? trim($a_matches[7][$i], '"') : '';

                    }
                }
            }			
			if( $a_matches && count( $a_matches ) ){
				$matches_count = count( $a_matches[0] );
				//1 from
				//2 from field
				//3 to
				//4 to field
				//5 merge condition
				$result = new report_join();
				for( $i = 0; $i < $matches_count; $i++ ){
					$fromTable = $a_matches[2][$i];
					if( !$result->fromTable ){
						$result->fromTable = $fromTable;
					}
					$toTable = $a_matches[6][$i];
					if( !$result->toTable ){
						$result->toTable = $toTable;
					}

					$obj = new stdClass();
					$obj->fromField = $a_matches[3][$i];
					$obj->toField = $a_matches[7][$i];
					$obj->fromConst = $a_matches[4][$i];
					$obj->toConst = $a_matches[8][$i];

					if( ( $fromTable != $result->fromTable && empty( $obj->fromConst ) ) || ( $toTable != $result->toTable && empty( $obj->toConst ) ) ){
						continue; //Get rid of errors/non-matching
					}

					$result->fieldMap[] = $obj;
					$result->conditions[] = $a_matches[9][$i];
				}
			}

			return $result;
		}

		public function __toString(){
			return $this->toString();
		}

		public function toString(){
			$result = '';
			if( !count( $this->fieldMap ) ){
				return $result;
			}

			$result .= "LEFT JOIN `{$this->toTable}` ON ";
			$closeParen = 0;
			for( $i = 0; $i < count( $this->fieldMap ); $i++ ){
				$fromField = $this->fieldMap[$i]->fromField;
				$toField = $this->fieldMap[$i]->toField;
				$condition = ' '.$this->conditions[$i].' ';

				$fromSide = "`{$this->fromTable}`.`{$fromField}`";
				$toSide = "`{$this->toTable}`.`{$toField}`";

				if( !empty( $this->fieldMap[$i]->fromConst ) ){
					$fromSide = $this->fieldMap[$i]->fromConst;
				}

				if( !empty( $this->fieldMap[$i]->toConst ) ){
					$toSide = $this->fieldMap[$i]->toConst;
				}

				$left = $right = '';
				if( $closeParen ){
					$closeParen--;
					$right = ')';
				}


				if( trim(strtolower($condition)) == 'or' ){
					$left = '(';
					$closeParen++;
				}

				$result .= "{$left}{$fromSide} = {$toSide}{$condition}{$right}";
			}

			return $result;
		}
	}


	class report_joins {
		public $report_read;
		public $alias_arrays;
		
		public function __construct($set_report_read)
		{
			$this->report_read = $set_report_read;
			$this->alias_arrays = array();
		}
		
		public function update_report_read($set_report_read)
		{
			$this->report_read = $set_report_read;
		}
		
		public function apply_aliases($type)
		{
			if(isset($this->report_read[$type]))
			{
				$str = $this->report_read[$type]; //Query
				$type_aliases = $this->verify_aliases_are_loaded($type);
				foreach($type_aliases as $akey => $aval) {
					// $str = str_replace("#".$akey,$aval,$str);
					if(class_exists('report_alias_types')) {  
					$type_aliase = report_alias_types::typeByStr( $type );
					$str = str_replace("#{$akey}", $aval->expand( $type_aliase ), $str );
					}
				}
				$this->report_read[$type] = $str;	
			}
		}
		
		public function apply_aliases_to_string($str)
		{
			// $type_aliases = $this->verify_aliases_are_loaded("display");
			$type_aliases = $this->verify_aliases_are_loaded( 'display', false );
			foreach($type_aliases as $akey => $aval) {
				// $str = str_replace("#".$akey,$aval,$str);
				$str = str_replace("#{$akey}", $aval->expand(), $str );
			}
			return $str;
		}
		
		public function get_current_report_read()
		{
			return $this->report_read;
		}
		
		public function get_alias_defs(){
			$str = get_report_alias_defs();
			$str_parts = explode("\n", $str );
			for($i = 0; $i < count( $str_parts ); $i++ ){
				$str_parts[$i] = trim($str_parts[$i] );
			}
			$str = implode(" ", $str_parts );

			preg_match_all('/([^\s:]+):\s*([^\s\{]+)\s*{\s*(.*)\s*}\s*/Um', $str, $a_matches );
			if( count($a_matches) ){
				for($i=0;$i<count($a_matches[0]);$i++){
					$type = report_alias_types::typeByStr( $a_matches[1][$i] );
					$name = $a_matches[2][$i];
					$selection = $a_matches[3][$i];
					$label = '';

					$match_as = stripos( $selection, ' as ' );
					if( $match_as !== FALSE ){
						$label =  trim(substr( $selection, $match_as + 4));
						$selection = trim(substr($selection, 0, $match_as ) );
					}

					report_alias::alias( $name, $type, $selection, $label );
				}
			}
		}
		
		//LP-5558 - changes
		public function getAliasDefs($aliasName){ 
		    $str = get_report_alias_defs();
		    $str_parts = explode("\n", $str );
		    for($i = 0; $i < count( $str_parts ); $i++ ){
		        $str_parts[$i] = trim($str_parts[$i] );
		    }
		    $str = implode(" ", $str_parts );
		    $label = '';
		    preg_match_all('/([^\s:]+):\s*([^\s\{]+)\s*{\s*(.*)\s*}\s*/Um', $str, $a_matches );
		    if( count($a_matches) ){
		        for($i=0;$i<count($a_matches[0]);$i++){
		            $type = report_alias_types::typeByStr( $a_matches[1][$i] );
		            $name = $a_matches[2][$i];
		            $selection = $a_matches[3][$i];
		            if($name == $aliasName){
		                $match_as = stripos( $selection, ' as ' );
		                if( $match_as !== FALSE ){
		                    $label =  trim(substr( $selection, $match_as + 4));
		                    //$selection = trim(substr($selection, 0, $match_as ) );
		                }
		            }
		         }
		    }
		    return $label;
		}

		public function verify_aliases_are_loaded($key="",$include_all=true)
		{
			/*if( $key == 'display' ){
				if(count($this->alias_arrays)==0)
				{
					$this->alias_arrays = $this->get_report_alias_arrays();
				}
				if($key!="" && isset($this->alias_arrays[$key]))
				{
					if($include_all && isset($this->alias_arrays['all']))
						return array_merge($this->alias_arrays[$key],$this->alias_arrays['all']);
					else
						return $this->alias_arrays[$key];
				}
				else if($key!="" && isset($this->alias_arrays['all']))
					return $this->alias_arrays['all'];
				else if($key!="")
					return array();
				else
					return $this->alias_arrays;
			} else {*/
			$ralias_key = "remember_aliases_" . $key;
			if($include_all) $ralias_key .= "_yes"; else $ralias_key . "_no";
			if(isset($_SESSION[$ralias_key]) && isset($_SESSION[$ralias_key . '_ts']) && $_SESSION[$ralias_key . '_ts']==time()) return $_SESSION[$ralias_key];

				$result = array();
				$this->get_alias_defs();
				$alias_names = report_alias::aliasNames();

				$type = report_alias_types::typeByStr( $key );
				if( $include_all ){

				}

				foreach( $alias_names as $alias_name ){
					$alias = report_alias::alias( $alias_name );
					if(empty($key) || $type === $alias->type() || ( $include_all && report_alias_types::ALL === $alias->type() )){
						$result[$alias_name] = $alias;
					}
				}

				$_SESSION[$ralias_key] = $result;
				$_SESSION[$ralias_key . '_ts'] = time();

				return $result;
			// }
		}
		
		public function get_report_alias_arrays()
		{
			$aliases = array();
			$aliases['full_list'] = array();
			
			$astr_parts = explode("}",get_report_alias_defs());
			for($n=0; $n<count($astr_parts); $n++)
			{
				$inner_parts = explode("{",$astr_parts[$n]);
				if(count($inner_parts) > 1)
				{
					$def_name = trim($inner_parts[0]);
					$def_value = trim($inner_parts[1]);
					
					if($def_name!="" && $def_value!="")
					{
						$def_name_parts = explode(":",$def_name);
						if(count($def_name_parts) > 1)
						{
							$def_type = $def_name_parts[0];
							$def_name = $def_name_parts[1];
						}
						else
						{
							$def_type = "all";
						}

						$def_value = str_replace("\n"," ",$def_value);
						$def_value = str_replace("\t"," ",$def_value);
						while(strpos($def_value,"  ")!==false)
						{
							$def_value = str_replace("  "," ",$def_value);
						}
						if(!isset($aliases[$def_type])) $aliases[$def_type] = array();					
						$aliases[$def_type][$def_name] = $def_value;
						$aliases['full_list'][$def_name] = array("type"=>$def_type,"value"=>$def_value);
					}
				}
			}
			return $aliases;
		}
		
		public function get_report_join_arrays()
		{
			$jtables = array();
			$jstr_parts = explode("\n",get_report_join_defs());
			for($n=0; $n<count($jstr_parts); $n++)
			{
				$jstr_parts[$n] = trim( $jstr_parts[$n] );
				if( empty( $jstr_parts[$n] ) ){
					continue;
				}

				// echo $join_obj->toString();
				// echo '<pre style="text-align: left;">' . print_r( $join_obj, true ) . '</pre>';
				// $jline_parts = explode("=",$jstr_parts[$n]);
				// if(count($jline_parts) > 1)
				// {
				// 	$jline_left_parts = explode(".",$jline_parts[0]);
				// 	$jline_right_parts = explode(".",$jline_parts[1]);
				// 	if(count($jline_left_parts) > 1 && count($jline_right_parts) > 1)
				// 	{
				// 		$left_table = trim($jline_left_parts[0]);
				// 		$left_column = trim($jline_left_parts[1]);
				// 		$right_table = trim($jline_right_parts[0]);
				// 		$right_column = trim($jline_right_parts[1]);
						
				// 		if($left_table!="" && $left_column!="" && $right_table!="" && $right_column!="")
				// 		{
				// 			$newval = array("left_table"=>$left_table,"left_column"=>$left_column,"right_table"=>$right_table,"right_column"=>$right_column);
				// 			if(!isset($jtables[$left_table])) $jtables[$left_table] = array();
				// 			if(!isset($jtables[$right_table])) $jtables[$right_table] = array();
				// 			$jtables[$left_table][$right_table] = $newval;
				// 			$jtables[$right_table][$left_table] = $newval;
				// 		}
				// 	}
				// }
				$join_obj = report_join::parseJoinStr( $jstr_parts[$n] );
				if( $join_obj && $join_obj->fromTable() && $join_obj->toTable() && $join_obj->hasFields() ){
					$left_table = $join_obj->fromTable();
					$right_table = $join_obj->toTable();
					if(!isset($jtables[$left_table])) $jtables[$left_table] = array();
					if(!isset($jtables[$right_table])) $jtables[$right_table] = array();
					$jtables[$left_table][$right_table] = $join_obj;
					// $jtables[$right_table][$left_table] = $join_obj;
				}
			}
			return $jtables;
		}
		
		public function find_connection_from_table_to_table(&$jar,$table1,$table2,$stack=array())
		{
			if(count($stack)==0)
			{
				$stack = array($table1);
			}
			else if(count($stack) > 7)
			{
				return false;
			}
			
			if(isset($jar[$table1][$table2]))
			{
				/*for($n=0; $n<count($stack); $n++)
				{
					echo $stack[$n] . " ";
				}
				echo "<br>";*/
				//echo "Connection - $table1 to $table2 " . count($stack). "<br>";
				return array_merge($stack,array($table2));
			}
			else
			{
				//echo "No connection between $table1 and $table2...<br>";
				$min_result = array();
				foreach($jar[$table1] as $step_table => $step_value)
				{
					//echo "trying $step_table...<br>";
					$found_in_stack = false;
					for($m=0; $m<count($stack); $m++)
					{
						if($stack[$m]==$step_table)
							$found_in_stack = true;
					}
					if(!$found_in_stack)
					{
						$result = $this->find_connection_from_table_to_table($jar,$step_table,$table2,array_merge($stack,array($step_table)));
						if($result)
						{
							if(count($min_result)==0 || count($result) < count($min_result))
								$min_result = $result;
						}
					}
				}
				if(count($min_result) > 0)
				{
					return $min_result;
				}
			}
		}
		
		public function create_join_statement_for_tables($tablelist)
		{
			$jar = $this->get_report_join_arrays();
			$tables_joined = array();
			
			// First figure out the path to take when joining these tables (will go through other tables if necessary)
			$all_clists = array();
			for($i=0; $i<count($tablelist); $i++) {
				$step_table = $tablelist[$i];
				if(count($tables_joined) < 1) {
					$tables_joined[$step_table] = 1;
				}
				else {
					foreach($tables_joined as $tjname => $tjvalue) {
						$clist = array();
						if(isset($jar[$tjname][$step_table])) {
							$clist = array($tjname,$step_table);
						}
						else {
							$clist = $this->find_connection_from_table_to_table($jar,$tjname,$step_table);
						}
						
						if(count($clist) > 0) {				
							$all_clists[] = $clist;
						}
					}
				}
			}
			
			// Rearrange lists to do the first step of each list, then the second step of each list, etc. until done
			$step_clists = array();
			for($i=0; $i<count($all_clists); $i++) {
				$clist = $all_clists[$i];
				for($n=1; $n<count($clist); $n++) {
					$table1 = $clist[$n - 1];
					$table2 = $clist[$n];
					
					if(!isset($step_clists[$n])){
						$step_clists[$n] = array();
					}
					$step_clists[$n][] = array($table1,$table2);
				}
			}
			
			// Basically get rid of any duplicate joins and merge the multiple lists into one
			$connections_made = array();
			$final_clist = array();
			foreach($step_clists as $i => $steplist)
			{
				for($n=0; $n<count($steplist); $n++)
				{
					$table1 = $steplist[$n][0];
					$table2 = $steplist[$n][1];
					
					if(!isset($connections_made[$table1 . "-" . $table2]))
					{
						$connections_made[$table1 . "-" . $table2] = 1;
						$final_clist[] = array($table1,$table2);
					}
				}
			}

			// Build the mysql join string
			$str = "";
			for($i=0; $i<count($final_clist); $i++)
			{
				$table1 = $final_clist[$i][0];
				$table2 = $final_clist[$i][1];
				
				$cjar = $jar[$table1][$table2];
				$str .= $cjar->toString();
				
				// $str .= "left join `".$cjar['right_table']."` on ";
				// $str .= "`".$cjar['left_table']."`.`".$cjar['left_column']."`=";
				// $str .= "`".$cjar['right_table']."`.`".$cjar['right_column']."` ";
			}
			
			return $str;
		}
		
		public function extract_tablenames_from_string($str)
		{
			//if(get_current_foldername()=="default_dev") echo "check for tablenames: " . $str .= "<br>";
			$tablenames = array();
			$sep_parts = explode("[_sep_]",$str);
			for($n=0; $n<count($sep_parts); $n++)
			{
				$str_parts = flat_multi_explode($sep_parts[$n]," ","(",")",",");
				for($i=0; $i<count($str_parts); $i++)
				{
					$part = str_replace("`","",$str_parts[$i]);
					if(strpos($part,"."))
					{
						$inner_parts = explode(".",$part);
						$part_left = trim($inner_parts[0]);
						$part_right = trim($inner_parts[1]);
						
						if(strlen($part_left) > 0 && strlen($part_right) > 0 && is_alpha_numeric($part_left) && is_alpha_numeric($part_right))
						{
							if(isset($tablenames[$part_left])) $tablenames[$part_left]++;
							else $tablenames[$part_left] = 1; 
						}
					}
				}
			}
			return $tablenames;
		}
		
		public function extract_tablenames_from_report_column($colname,&$tablelist)
		{
			$new_tablelist = $this->extract_tablenames_from_string($this->report_read[$colname]);
			foreach($new_tablelist as $ntable => $nval)
			{
				if(isset($tablelist[$ntable])) $tablelist[$ntable] += $nval;
				else $tablelist[$ntable] = $nval;
			}
		}
		
		public function auto_join_string()
		{
			$primary_table = $this->report_read['table'];
			$tlist = array($primary_table=>1);
			$this->extract_tablenames_from_report_column("select",$tlist);
			$this->extract_tablenames_from_report_column("where",$tlist);
			$this->extract_tablenames_from_report_column("groupby",$tlist);
			$this->extract_tablenames_from_report_column("orderby",$tlist);
			
			$tlist_arr = array();
			foreach($tlist as $tname => $tval)
			{
				$tlist_arr[] = $tname;
			}
			return $this->create_join_statement_for_tables($tlist_arr);
		}
	}
