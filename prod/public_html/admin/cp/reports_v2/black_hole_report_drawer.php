<?php
require_once dirname(__FILE__) . '/iarea_report_drawer.php';
require_once dirname(__FILE__) . '/area_reports.php';
require_once dirname(__FILE__) . '/report_functions.php';

class black_hole_report_drawer implements iarea_report_drawer {
	public function draw_multiple_rows( $report_results ) {
		return "";
	}

	public function draw_single_row( $report_results ) {
		return "";
	}

	public function draw_no_data() {
		return "";
	}
}