<?php
require_once dirname(__FILE__) . '/iarea_report_drawer.php';
require_once dirname(__FILE__) . '/area_reports.php';
require_once dirname(__FILE__) . '/report_functions.php';

class exporter_report_drawer implements iarea_report_drawer {
	protected
	$area_reports;

	public function __construct( $area_reports ){
		$this->area_reports = $area_reports;
	}

	public function getRowDelimeter(){
		return "\n";
	}

	public function getColDelimeter(){
		return ",";
	}

	public function escape_value( $value ){
		$value = htmlspecialchars_decode( $value );
		$value_parts = str_split( $value );
		foreach( $value_parts as $value_part_key => $value_part ){
			if( in_array( $value_part,
					array(
							"\n",
							"\"",
							"\r",
							"\t",
							" ",
							","
					))
					){
						return '"' .  str_replace("\"", "\\\"", $value ) . '"';
			}
		}
		return $value;
	}

	public function output( $export_str ){
		$this->area_reports->output_export_string( strtolower( str_replace(" ", "_", $this->area_reports->reportFullName() ) ).'.csv', $export_str );
	}

	public function draw_multiple_rows( $report_results ) {
	        $export_str = '';
		//start LP-4062 main ticket,LP-4449
		global $location_info;
		global $data_name;

		$monetary_symbols = array();
		if (isset($report_results['location_currencies'])) {
			$monetary_symbols = $report_results['location_currencies'];
			unset($report_results['location_currencies']);
		}
		//cheeck for ABI Argentina
		$rest_query = mlavu_query("select `chain_name` from `poslavu_MAIN_db`.`restaurants` where `data_name`='[1]'",$data_name);
		if(mysqli_num_rows($rest_query))
		{
			$rest_read = mysqli_fetch_assoc($rest_query);
			$chain_name = $rest_read['chain_name'];
			if(substr($chain_name,0,3)=="ABI")
			{
				// condition argentina
				if ($_REQUEST['report'] !=''){
					$report_title_query = mlavu_query("SELECT `title` from `poslavu_MAIN_db`.`reports_v2` where `id` ='[1]'",$_REQUEST['report']);
					$report_title = mysqli_fetch_assoc($report_title_query);
					if($_REQUEST['sister_select'] !=''){
						$subtitle = $_REQUEST['sister_select'];
						$subtitle =	str_replace("_", " ", ltrim($subtitle, '#'));
						$export_str .= speak('Name of report:').','.$report_title['title'].' ('.$subtitle.')';
					}
					else{
						$export_str .= speak('Name of report:').','.$report_title['title'];
					}
					$export_str .="\n";
					$export_str .=speak('Name of POC').':,'. $location_info['title'];
					$export_str .="\n";
					if ($location_info['cuit_no'] !=''){ // START of $location_info['cuit_no'] //LP-4449
						$export_str .=speak('Cuit No.').':,'.$location_info['cuit_no'];
						$export_str .="\n";
					}// END of $location_info['cuit_no']//LP-4449
					$export_str .=speak('Date').':,'.speak('Year').':'. date(Y).' '.speak('Month').':'.date(F);
					$export_str .="\n";
				}
			}
		}
		$rowDelimeter = $this->getRowDelimeter();
		$colDelimeter = $this->getColDelimeter();
		$totalItemPrice = 0;
		$totalModifyPrice = 0;
		$totalForcedModifiersPrice = 0;
		$totalQuantity = 0;
		//END LP-4062 main ticket,LP-4449
		for($m=0;$m<count($report_results); $m++){
			$monitary_symbol = "";
			if( isset($report_results['location_names']) && !isset( $report_results['location_names'][$m] )){
				continue;
			}
			if( isset($report_results['location_names']) && count( $report_results ) > 1 ){
				$export_str .= $this->escape_value( $report_results['location_names'][$m] ) . $rowDelimeter;
			}
			if (isset($monetary_symbols[$m])) {
				$monitary_symbol = $monetary_symbols[$m];
			}
			if( count( $report_results[$m] ) > 0 ){
				$total_types=array();
				$totals=array();
				for($n=0; $n<count($report_results[$m]); $n++){
					$rowstr = "";
					$report_read = $report_results[$m][$n];
					//Modify Action Log result
					if ($this->area_reports->report_id == 35) {
						$report_read = $this->area_reports->modifyActionLogReports($report_read);
					}
					if( $n === 0 ){
						foreach($report_read as $index => $rowValues) { // Output a row
						    foreach($rowValues as $key => $val) { // Output a row
        							if(strpos($val,"[drill_down]")!==false) {
        							} else if($this->area_reports->should_show_col($key)){
        								if($rowstr!=""){
											$rowstr .= $colDelimeter;
        								}
        								if($key == "Date (AAMMDD)"){
        									$date_setting = "date_format";
        									$location_date_format_query = lavu_query( "SELECT `location`, `setting`, `value` FROM `config` WHERE `setting` = '[1]'",  $date_setting);
        									if( !$location_date_format_query || !mysqli_num_rows($location_date_format_query)){
        										$date_format = array("value" => 2);
        									}
        									else{
        										$date_format = mysqli_fetch_assoc( $location_date_format_query );
        									}
        									switch ($date_format['value']){
        										case 1:$key = "Date (DDMMAA)";
        										break;
        										case 2:$key = "Date (MMDDAA)";
        										break;
        										case 3:$key = "Date (AAMMDD)";
        										break;
        										default:$key = "Date (MMDDAA)";
        									}
										}
										// If country name is Israel, header will change based on selected language
										if (in_array($location_info['business_country'], array('Israel'))) {
											$key = speak($key);
										}
										$rowstr .= $this->escape_value(show_as_title($key));

										// We need to show items details for Israel Ref. LP-10427
										if (in_array($location_info['business_country'], array('Israel')) && $key == 'Order ID') {
											$contentsArr = array('Item', 'Price', 'Quantity', 'Options', 'Modify Price', 'Specials', 'Forced Modifiers Price' );
											foreach ($contentsArr as $column) {
												$rowstr .= $colDelimeter . $this->escape_value(show_as_title(speak($column)));
											}
										}
        							}
						    }
						}
						$export_str .= $rowstr;
						$rowstr = '';
					}
					foreach($report_read as $index => $rowValues) { // Output a row
					    foreach($rowValues as $key => $val) { // Output a row
						if(strpos($val,"[drill_down]")!==false) {
						} else if($this->area_reports->should_show_col($key)){
							if( is_numeric( $val ) ){
								if( $val * 1 == 0 ){
									$val = abs( $val * 1 );
								}
							} else if( report_value_is_money( $val ) ) {
								$nval = report_money_to_number( $val );
								if ($nval === 0) {
									$val = report_money_output( abs( $nval ), $monitary_symbol);
								}
							}
							//start LP-4464
							if($val != "" && ($key == "order_closed_time" || $key == "opened" || $key == "closed" || $key == "time" || $key == "schedule" || $key == "date" || $key  == "Order Date")){
								$date_setting = "date_format";
								$location_date_format_query = lavu_query( "SELECT `location`, `setting`, `value` FROM `config` WHERE `setting` = '[1]'",  $date_setting);
								if( !$location_date_format_query || !mysqli_num_rows($location_date_format_query)){
									$date_format = array("value" => 2);
								}
								else{
									$date_format = mysqli_fetch_assoc( $location_date_format_query );
								}

								$split_date = explode(" ", $val);
								$change_format = explode("-", $split_date[0]);
								switch ($date_format['value']){
									case 1:$val = trim($change_format[2])."-".trim($change_format[1])."-".trim($change_format[0])." ".$split_date[1];
									break;
									case 2:$val = trim($change_format[1])."-".trim($change_format[2])."-".trim($change_format[0])." ".$split_date[1];
									break;
									case 3:$val = trim($change_format[0])."-".trim($change_format[1])."-".trim($change_format[2])." ".$split_date[1];
									break;
									default:$val = trim($change_format[1])."-".trim($change_format[2])."-".trim($change_format[0])." ".$split_date[1];
								}
							}
							if($val != "" && ($key == "actual_date_and_hour" || $key == "order_closed_actual_date_and_hour"	|| $key == "item_added_actual_date_and_hour")){
								$split_date = explode(")", $val);
								$change_date = explode(" ", $split_date[1]);
								$change_format = explode("-", $change_date[0]);
								$formatter = explode("(", $split_date[0]);
								$date_format = explode(" ", $formatter[1]);
								$split_date = explode("-", $date_format[0]);
								$date_setting = "date_format";
								$location_date_format_query = lavu_query( "SELECT `location`, `setting`, `value` FROM `config` WHERE `setting` = '[1]'",  $date_setting);
								if( !$location_date_format_query || !mysqli_num_rows($location_date_format_query)){
									$date_option = array("value" => 2);
								}else
									$date_option = mysqli_fetch_assoc( $location_date_format_query );

									switch ($date_option['value']){
										case 1:
											$val = trim($change_format[2])."-".trim($change_format[1])."-".trim($change_format[0])." ".$change_date[1];
											break;
										case 2:
											$val = trim($change_format[1])."-".trim($change_format[2])."-".trim($change_format[0])." ".$change_date[1];
											break;
										case 3:
											$val = trim($change_format[0])."-".trim($change_format[1])."-".trim($change_format[2])." ".$change_date[1];
											break;
										default:$val = trim($change_format[1])."-".trim($change_format[2])."-".trim($change_format[0])." ".$change_date[1];
									}
							}
							if($val != "" && ($key=="Date" || $key=="Date Requested" || $key=="Date Received")){
								$date_setting = "date_format";
								$location_date_format_query = lavu_query( "SELECT `location`, `setting`, `value` FROM `config` WHERE `setting` = '[1]'",  $date_setting);
								if( !$location_date_format_query || !mysqli_num_rows($location_date_format_query)){
									$date_option = array("value" => 2);
								}else
									$date_option = mysqli_fetch_assoc( $location_date_format_query );

								$split_date = explode(" ", $val);
								$change_format = explode("/", $split_date[0]);
								switch ($date_option['value']){
									case 1:$val = trim($change_format[1])."/".trim($change_format[0])."/".trim($change_format[2])." ".$split_date[1];
									break;
									case 2:$val = trim($change_format[0])."/".trim($change_format[1])."/".trim($change_format[2])." ".$split_date[1];
									break;
									case 3:$val = trim($change_format[2])."/".trim($change_format[0])."/".trim($change_format[1])." ".$split_date[1];
									break;
									default:$val = trim($change_format[1])."/".trim($change_format[2])."/".trim($change_format[0])." ".$split_date[1];
								}
							}
							//END LP-4464
							if($val != "" && ($key == "fiscal_date" || $key == "commercial_date")){
								$date_setting = "date_format";
								$location_date_format_query = lavu_query( "SELECT `location`, `setting`, `value` FROM `config` WHERE `setting` = '[1]'",  $date_setting);
								if( !$location_date_format_query || !mysqli_num_rows($location_date_format_query)){
									$date_format = array("value" => 2);
								}
								else{
									$date_format = mysqli_fetch_assoc( $location_date_format_query );
								}
								$change_format = explode("-", $val);
								switch ($date_format['value']){
									case 1:$val = trim($change_format[2])."-".trim($change_format[1])."-".trim($change_format[0]);
									break;
									case 2:$val = trim($change_format[1])."-".trim($change_format[2])."-".trim($change_format[0]);
									break;
									case 3:$val = trim($change_format[0])."-".trim($change_format[1])."-".trim($change_format[2]);
									break;
									default:$val = trim($change_format[1])."-".trim($change_format[2])."-".trim($change_format[0]);
								}
							}
							if($val != "" && ($key == "Date (DDMMAA)" || $key == "Date (MMDDAA)" || $key == "Date (AAMMDD)")){
								$date_setting = "date_format";
								$location_date_format_query = lavu_query( "SELECT `location`, `setting`, `value` FROM `config` WHERE `setting` = '[1]'",  $date_setting);
								if( !$location_date_format_query || !mysqli_num_rows($location_date_format_query)){
									$date_format = array("value" => 2);
								}
								else{
									$date_format = mysqli_fetch_assoc( $location_date_format_query );
								}
								$split_date = explode(" ", $val);
								$change_format = str_split($split_date[0], 2);
								switch ($date_format['value']){
									case 1:$val = trim($change_format[2])."".trim($change_format[1])."".trim($change_format[0])." ".$split_date[1];
									break;
									case 2:$val = trim($change_format[1])."".trim($change_format[2])."".trim($change_format[0])." ".$split_date[1];
									break;
									case 3:$val = trim($change_format[0])."".trim($change_format[1])."".trim($change_format[2])." ".$split_date[1];
									break;
									default:$val = trim($change_format[1])."".trim($change_format[2])."".trim($change_format[0])." ".$split_date[1];
								}
							}
							
							if($val != "" && ($key == "order_closed_day" || $key == "item_added_day" || $key == "day")){
								// based on date format setting
								$date_setting = "date_format";
								$location_date_format_query = lavu_query( "SELECT `location`, `setting`, `value` FROM `config` WHERE `setting` = '[1]'",  $date_setting);
								if( !$location_date_format_query || !mysqli_num_rows($location_date_format_query)){
									$date_format = array("value" => 2);
								}else{
									$date_format = mysqli_fetch_assoc( $location_date_format_query );
								}
								$split_date = explode(")", $val);
								$change_format = explode("-", $split_date[1]);
								switch ($date_format['value']){
									case 1:$val = trim($change_format[1])."-".trim($change_format[0]);
									break;
									case 2:$val = trim($change_format[0])."-".trim($change_format[1]);
									break;
									case 3:$val = trim($change_format[0])."-".trim($change_format[1]);
									break;
									default:$val = trim($change_format[1])."-".trim($change_format[0]);
								}
							}
							if ($rowstr!="") {
								$rowstr .= $colDelimeter;
							}
							$new_value = $this->area_reports->show_reported_value($val, $report_read, $key, $monitary_symbol);

							if($new_value==""){
								$new_value=" ";
							}
							$new_value = strip_tags($new_value);
							$rowstr .= $this->escape_value( $new_value );
							$contentsStr = '';
							if ($key == "Order ID" && in_array($location_info['business_country'], array('Israel'))) {
								$order_contents_query = lavu_query("SELECT item, concat('[money]', format(cast(`price` AS decimal(30, 5)),2)) AS 'Price', quantity, options, concat('[money]', format(cast(`modify_price` AS decimal(30, 5)),2)) AS 'Modify Price', special,  concat('[money]', format(cast(`forced_modifiers_price` AS decimal(30, 5)),2)) AS 'Forced Modifiers Price' FROM `order_contents` WHERE `order_id` = '[1]' AND `loc_id` = '[2]' AND `item` != 'SENDPOINT'", trim($new_value), $location_info['id']);
								if (mysqli_num_rows($order_contents_query) > 0 ) {
									$contentsRow = '';
									while( $row = mysqli_fetch_assoc( $order_contents_query ) ) {
										$extraCols = '';
										for ($i = 0; $i < 5; $i++) {
											$extraCols .= $colDelimeter;
										}
										$item = ($row['item']) ? $row['item'] : '--';
										$price = report_money_output( $row['Price'], $monitary_symbol );
										$quantity = ($row['quantity']) ? $row['quantity'] : 0;
										$options = ($row['options']) ? $row['options'] : '--';
										$modifyPrice = report_money_output( $row['Modify Price'], $monitary_symbol );
										$special = ($row['special']) ? $row['special'] : '--';
										$forcedModifierPrice = report_money_output( $row['Forced Modifiers Price'], $monitary_symbol );
										$contentsRow .= $rowDelimeter . $extraCols . $item . $colDelimeter . $this->escape_value($price) . $colDelimeter . $quantity . $colDelimeter . $this->escape_value($options) . $colDelimeter . $this->escape_value($modifyPrice) . $colDelimeter . $this->escape_value($special) . $colDelimeter . $this->escape_value($forcedModifierPrice);
										$totalItemPrice += report_money_to_number( $row['Price'], $monitary_symbol )*1;
										$totalModifyPrice += report_money_to_number( $row['Modify Price'], $monitary_symbol )*1;
										$totalForcedModifiersPrice += report_money_to_number( $row['Forced Modifiers Price'], $monitary_symbol )*1;
										$totalQuantity += $quantity;
									}
									if ($contentsRow != '') {
										$contentsRow .= $rowDelimeter;
									}
									$contentsStr .= $contentsRow;
								}
								if ($rowstr!="") {
									// Added 7 new column to display order details
									for ($i = 0; $i < 7; $i++) {
										$rowstr .= $colDelimeter . '--';
									}
								}
							}

							$nval = $new_value;
							if (report_value_is_number($val) && strpos($key,"_id")===false && strpos($key,"zip")===false && strpos($key,"postal")===false && strpos(strtolower($key),"invoice")===false  && strpos(strtolower($key),"date")===false && strpos(strtolower($key),"z report id")===false) {
								$total_type = "numeric";
								$nval = report_number_to_float($val)*1;
							} else if(report_value_is_date($val)){
								$total_type = "date";
								$nval = report_date_to_number( $val );
								if($key == 'order_id'){
									$order_ids_arr[] = $nval;
								}
							} else if ( report_value_is_money( $val , $monitary_symbol )&&  strpos($key,"net_sales")===false ) {

								$total_type = "money";
								$nval = report_money_to_number( $val, $monitary_symbol )*1;
							} else {
								$total_type = "string";
							}
							if(!isset($total_types[$key]) || $total_types[$key]!="string"){
								$total_types[$key] = $total_type;
							}
							if($total_types[$key]=="numeric" || $total_types[$key]=="money"){
								if( $nval == 0 ){
									$nval = abs($nval); // avoid -0.0
								}
								if(!isset($totals[$key])) {
									$totals[$key] = 0;
								}
								$totals[$key] += $nval;

							}
						}}
					}
					if($rowstr!=""){
						if($export_str!=""){
							$export_str .= $rowDelimeter;
						}
						$export_str .= $rowstr . $contentsRow;
					}
				}

				$totals_str = "";
				$totals_count = 0;
				$orders_count = array_count_values($order_ids_arr);
				foreach($total_types as $key => $val)
				{
					if($val=="string") {
						$totals_str .= " ";
					} else {
						$totals_count++;
						if($val=="numeric")
						{
							if($key=="average"){
								$totals_str .= report_money_output($totals[$key] / count($report_results[$m]));
							}else{
								if($flag != 1){
									$totals_str .= report_number_output($totals[$key]);
								}
							}
						}
						else if($val=="money") {
							$totals_str .= $this->escape_value(report_money_output($totals[$key], $monitary_symbol));//"$" . number_format($totals[$key],2);
						}
						$totals_str .= " ";
					}
					if($totals_str!=""){
						if ($key == "Order ID" && in_array($location_info['business_country'], array('Israel'))) {
							$totals_str .= $colDelimeter . $colDelimeter . $this->escape_value(report_money_output(report_float_to_money($totalItemPrice), $monitary_symbol)) . $colDelimeter . $totalQuantity . $colDelimeter . $colDelimeter . $this->escape_value(report_money_output(report_float_to_money($totalModifyPrice), $monitary_symbol)) . $colDelimeter . $colDelimeter . $this->escape_value(report_money_output(report_float_to_money($totalForcedModifiersPrice), $monitary_symbol));
						}
						$totals_str .= $colDelimeter;
					}
				}
				$export_str .= $rowDelimeter;
				
				$report_title_query = mlavu_query("SELECT `title` from `poslavu_MAIN_db`.`reports_v2` where `id` ='[1]'",$_REQUEST['report']);
				$report_title = mysqli_fetch_assoc($report_title_query);
				if ($report_title['title'] != "General Ledger" && $report_title['title'] != "Tip Sharing") {
				$export_str .= $totals_str;
				}
				
			} else {
			    if( isset( $report_results['location_names'][$m] )){
					$export_str .= $this->escape_value( speak('No Records Found') ) . $rowDelimeter;
				}
			}

			$export_str .=  $rowDelimeter;
		}
		$this->output( $export_str );
	}

	public function draw_single_row( $report_results ) {
		return $this->draw_multiple_rows( $report_results );
	}

	public function draw_no_data() {
		$this->output( $this->escape_value( speak("No Records Found") ) );
	}
}