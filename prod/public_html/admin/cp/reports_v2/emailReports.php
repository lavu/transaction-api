<?php
require_once __DIR__.'/constants/reportConstants.php';
class emailReports {
    protected $table = 'proccess_reports_to_email';
    protected $rootDirectory = 'email-reports';
    protected $id;
    protected $token;
    protected $limit;
    protected $noOfRecordProcessed = 0;
    protected $email;
    protected $queries;
    protected $filePath;
    protected $status;
    protected $reportId;
    protected $dataname;
    protected $domain;
    protected $companyName;
    public static $_QUERY_TYPE = 'main';
    public static $_STATUS_DONE = 'completed';
    public static $_STATUS_PROCESSING = 'processing';
    public static $_STATUS_PENDING = 'pending';
    public static $_STATUS_ERROR = 'error';
    public static $allowedReports = [
        TYPE_LAVU_GIFT_AND_LOYALITY
    ];
    private $tableReports = 'reports';
    function __construct($token = false) {
        $this->token = $token;
        if ($this->token) {
            $this->setObject($this->token);
        }
    }
    protected function setObject($token, $data = '') {
        $record = !empty($data) ? $data : $this->getRecordByToken($token);
        if (!empty($record)) {
            foreach ($record as $key => $value) {
                $camlizedKey = $this->camelize($key);
                    if (is_array($value)) {
                        $value = json_encode($value);
                    }
                    $this->$camlizedKey = $value;
            }
        }
    }
    public static function isReportAllowedToEmail($type) {
        return in_array($type, self::$allowedReports);
    }
    public function getRecordByToken($token) {
        $query = 'SELECT * FROM '.$this->table.' where `token` = "[0]" LIMIT 1';
		$queryResult = mlavu_query($query, [$token]);
		return mysqli_fetch_assoc($queryResult);
    }
    public function insert($data = '') {
        $status = false;
        try {
            $columns = '(`report_id`, `queries`, `email`, `limit`, `status`, `no_of_record_processed`, `token`, `dataname`, `domain`)';
            if (!empty($data)) {
                $this->setObject(false, $data);
            }
            if (empty($this->email)) {
                throw new Exception('Required paramater are missing!');
            }
            $values = [
                $this->reportId,
                $this->queries,
                $this->email,
                $this->limit,
                self::$_STATUS_PENDING,
                $this->noOfRecordProcessed,
                $this->token,
                $this->dataname,
                $this->getDomain()
            ];
            $status = mlavu_query("INSERT INTO `proccess_reports_to_email` ".$columns." VALUES ([0], '[1]', '[2]', '[3]', '[4]', '[5]', '[6]', '[7]', '[8]')", $values);
        } catch (Exception $e) {
            $this->logError(__METHOD__."Failed to insert report, ". $e->getMessage());
        }
        return $status ? $this->token : false;
    }
    protected function update($data, $token, $id = false) {
        $updateSet = [];
        foreach ($data as $key => $value) {
            if (is_array($value)) {
                $value = json_encode($value);
            }
            $updateSet[] = $key.'="'.$value.'"';
        }
        if (!empty($updateSet) && ($token !== false || $id != false)) {
            $updateQuery = 'UPDATE `proccess_reports_to_email` SET '.implode(", ", $updateSet).' WHERE ';
            $where = '`token` = "[0]"';
            $filter = $token;
            if ($token === false) {
                $where = '`id` = "[0]"';
                $filter = $id;
            }
            $status = false;
            try {
                $status = mlavu_query($updateQuery.$where, [$filter]);
            } catch (Exception $e) {
                $this->logError(__METHOD__."Failed to update report, ". $e->getMessage());
            }
            return $status ? $token : false;
        } else {
            return false;
        }
    }
    protected function setToken() {
        $this->token = md5($this->queries.uniqid());
    }
    public function getToken() {
        return $this->token;
    }
    public function setReportToProcess($params) {
        if (!isset($params['queries'])) {
            $this->logError('Queries not found!');
            return false;
        } else {
            $status = true;
            $this->setObject(false, $params);
            $this->setToken();
            $status = $this->insert();
            return $status;
        }
    }
    public function camelize($input) {
        if ($input == 'report_id') {
        	return "reportId";
        } else if ($input=='no_of_record_processed') {
        	return  "noOfRecordProcessed";
        } else {
        	return $input;
        }
    }
    protected function logError($msg) {
		error_log(__FILE__ .' Got error: '. $msg);
		error_log('ERROR: '. $msg);
    }
    protected function getCompanyName() {
        $query = "SELECT `company_name` FROM restaurants where data_name = '[0]' LIMIT 1";
        $result = mlavu_query($query, [$this->dataname]);
        $data = mysqli_fetch_assoc($result);
        $companyName = isset($data['company_name']) ? $data['company_name'] : 'Lavu Inc';
        $this->companyName = $companyName;
        return $this->companyName;
    }
    protected function getReportName() {
        $query = "SELECT `title` FROM reports_v2 where id = '[0]' LIMIT 1";
        $result = mlavu_query($query, [$this->reportId]);
        $data = mysqli_fetch_assoc($result);
        return isset($data['title']) ? $data['title'] : 'Lavu Report';
    }
    protected function getDomain() {
        $protocol = stripos($_SERVER['SERVER_PROTOCOL'], 'https') === true ? 'https://' : 'http://';
        return $protocol.$_SERVER['HTTP_HOST'];
    }
    protected function createDownloadLink() {
        return $this->domain.'/cp/download_report.php?token='.$this->token;
    }
    protected function sendMail() {
        $from = ucwords($this->getCompanyName()).' <noreply@poslavu.com>';
        $headers = 'MIME-Version: 1.0' . "\r\n";
        $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
        $headers .= 'From: ' . $from . "\r\n" . 'X-Mailer: PHP/' . phpversion ();
        $reportName = ucwords($this->getReportName());
        $subject = $reportName;
        $emailto = $this->email;
        $emailcontent = "<div><b>Dear user,</b></br>The requested $reportName is ready to download. <br/> <a target='_blank' href='".$this->createDownloadLink()."'>please click here to download</a></div>";
	    mail ( $emailto, $subject, $emailcontent, $headers );
    }
    protected function getTableName() {
        return '`poslavu_'.$this->dataname.'_db`.`'.$this->tableReports.'`';
    }
}