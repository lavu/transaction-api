(function(){
	function observationHandler( arg1, arg2 ){
		var win = document.defaultView.parent;
		var reapplypreviewMode = false;
		var skip = true;
		var added = false;
		for( var i = 0; i < arg1.length; i++ ){
			if( arg1[i].target.id == 'report_container' ){
				reapplypreviewMode = arg1[i].addedNodes.length > 1;
			}
			skip = skip && (arg1[i].target.tagName != 'CANVAS');
			added = added || (arg1[i].addedNodes.length > 0);
			if( arg1[i].target == document.getElementById('details_view') && arg1[i].addedNodes.length > 0 ){
				arg1[i].target.style.display = 'block';
			}
		}

		if( !skip || !added ){
			return;
		}

		if( win['iframe_rescale'] !== undefined ){
			win['iframe_rescale']();
		}


		redrawCanvases();
		recolor();
		showTimeElements();
		if( reapplypreviewMode ){
			previewModeForEOD();
		}
	}

	var config = { childList: true, attributes: true, characterData: true, subtree: true, attributeOldValue: true, characterDataOldValue: true };
	if( window.MutationObserver ){
		var ob = new MutationObserver( observationHandler );
		ob.observe( document.getElementById('report_container'), config);
	}

	function showTimeElements(){
		var timeElements = document.querySelectorAll('time');
		for( var j = 0; j < timeElements.length; j++ ){
			if(!timeElements[j].childNodes.length){
				var d = new Date(timeElements[j].getAttribute('datetime'));
				timeElements[j].appendChild( document.createTextNode( d.toLocaleString() ) );
			}
		}
	}

	function previewModeForEOD(){
		var allHeaders = document.querySelectorAll('tr.eod_section_header');
		for( var i = 0; i < allHeaders.length; i++ ){
			var allRows = document.querySelectorAll("tr.eod_data_row[section=" + allHeaders[i].getAttribute('section') +"]");
			if( allRows.length > 5 ){
				for(var k = 0; k < allRows.length; k++ ){
					//That's us!
					if( k >= 5 ){
						allRows[k].style.display = 'none';
						// var allItems = allRows[k].querySelectorAll('td > div' );
						// for( var j = 0; j < allItems.length; j++ ){
							// allItems[j].style.height = '0';
							// allItems[j].style.opacity = 0;
						// }
						allRows[k].style.opacity = 0;
					} else {
						// var allItems = allRows[k].querySelectorAll('td > div' );
						// for( var j = 0; j < allItems.length; j++ ){
							// allItems[j].style.opacity = (5 - k) / 5;
						// }
						allRows[k].style.opacity = (5 - k) / 5;
					}
				}
			}
		}
	}

	function redrawCanvases(){
		var canvases = document.getElementsByTagName( 'canvas' );
		for( var i = 0; i < canvases.length; i++ ){
			var canvas = canvases[i];
			var ctx = canvas.getContext('2d');
			var chart = new Chart( ctx );
			canvas.chart = chart;
			var data = JSON.parse( canvas.getAttribute('data') );
			// chart.HBar( data );
			chart.Line( data );
		}
	}

	var tableMap = null;
	var colorsMap = null;
	function recolor(){
		var i = 0;
		colorsMap = {};
		tableMap = [];
		var rng = new RNG( 0xaece37 );
		var tableDataList = document.querySelectorAll('.report_table > tbody:not(.pagination-table)');
		if( !tableDataList){
			return;
		}
		
		
		for( var j = 0; j < tableDataList.length; j++ ){
			var tableData = tableDataList[j];
			var readerBody = tableData;
			var scrollableTable = tableData.querySelectorAll('div.scrollable-element table tbody');
			if(scrollableTable && scrollableTable.length) {
				readerBody = scrollableTable[0];
			}
			var tableRow = readerBody.children[0];
			if (tableRow) {
				for( i = 0; i < tableRow.children.length; i++ ){
					if( tableRow.children[i].hasAttribute('recolor') ){
						var colorMapTitle = tableRow.children[i].getAttribute('recolor');
						if( colorsMap[colorMapTitle] === undefined ){
							colorsMap[colorMapTitle] = Color.createColorBetweenThresholds( rng );
							tableMap[i] = colorMapTitle;
						}
					}
				}
			} else {
				return;
			}
			
			if( !tableData.parentNode.querySelector('thead') || ! tableData.parentNode.querySelector('tfoot') ){
				return;
			}

			var tableHeadRow = tableData.parentNode.querySelector('thead').children[0];
			var tableFootRow = tableData.parentNode.querySelector('tfoot').children[0];

			for( i = 0; i < tableMap.length; i++ ){
				if( tableMap[i] === undefined && !colorsMap[tableMap[i]]){
					continue;
				}

				tableHeadRow.children[i].style.color = colorsMap[ tableMap[i] ].toRGBAStringWithAlpha();
				tableFootRow.children[i].style.color = colorsMap[ tableMap[i] ].toRGBAStringWithAlpha();
			}
		}

		var recolors = document.querySelectorAll( '[recolor]' );
		if(colorsMap.length) {
			for( i = 0; i < recolors.length; i++ ){
				if( recolors[i].hasAttribute('entry') ){
					continue;
				}

				var colorMapTitle = recolors[i].getAttribute('recolor');
				var recolortype = recolors[i].getAttribute('recolortype');
				switch( recolortype ){
					case 'bg' : {
						recolors[i].style.backgroundColor = colorsMap[ colorMapTitle ].toRGBAStringWithAlpha();
						break;
					} case 'color' : {
					} default: {
						if( colorsMap[colorMapTitle] === undefined ){
							colorsMap[colorMapTitle] = Color.createColorBetweenThresholds( rng );
							tableMap[i] = colorMapTitle;
						}
						recolors[i].style.color = colorsMap[ colorMapTitle ].toRGBAStringWithAlpha();
						break;
					}
				}
			}	
		}
		
	}

	redrawCanvases();
	recolor();
	showTimeElements();
	previewModeForEOD();
})();

(function(){
	window.onpopstate = function( event ){
		if(event.state){
			ajax_link('report_container',event.state,'','','f_tcalInit');
		} else {
			// window.location.reload();
		}
	};
})();

function getTotalOffsetFromElement( ele1, ele2, x, y ){
	if( ele1 == ele2 ){
		return { x: x, y: y };
	}

	return getTotalOffsetFromElement( ele1.offsetParent, ele2, x + ele1.offsetLeft, y + ele1.offsetTop );
}

//This function is used in LP-3802.
function show_combo_info( combo_id, event ){
	ajax_link('details_view', 'index.php?widget=reports/combo_details_v2&combo_id=' + combo_id, '');
	var ele = document.getElementById('details_view');
	// ele.style.display = 'block';
	var mouseEvent = event;
	var target = mouseEvent.target||mouseEvent.srcElement;
	var coords = getTotalOffsetFromElement(target, document.getElementById('main_content_area'), 0, 0 );
	ele.style.left = coords.x + 'px';
	ele.style.top = coords.y + target.offsetHeight + 10 + 'px';
	// window._detailsViewListener.skipOne = true;
	if(!window.MutationObserver){
		ele.style.display = 'block';
	}
}


function show_order_id( order_id, event ){
	ajax_link('details_view', 'index.php?widget=reports/order_details_v2&order_id=' + order_id, '');
	var ele = document.getElementById('details_view');
	// ele.style.display = 'block';
	var mouseEvent = event;
	var target = mouseEvent.target||mouseEvent.srcElement;
	var coords = getTotalOffsetFromElement(target, document.getElementById('main_content_area'), 0, 0 );
	ele.style.left = coords.x + 'px';
	ele.style.top = coords.y + target.offsetHeight + 10 + 'px';
	// window._detailsViewListener.skipOne = true;
	if(!window.MutationObserver){
		ele.style.display = 'block';
	}
}

function show_device( uuid, event ){
	ajax_link('details_view', 'index.php?widget=reports/device_details&UUID=' + uuid, '');
	var ele = document.getElementById('details_view');
	// ele.style.display = 'block';
	var mouseEvent = event;
	var coords = getTotalOffsetFromElement(mouseEvent.target, document.getElementById('main_content_area'), 0, 0 );
	ele.style.left = coords.x + 'px';
	ele.style.top = coords.y + mouseEvent.target.offsetHeight + 10 + 'px';
	if(!window.MutationObserver){
		ele.style.display = 'block';
	}
	// window._detailsViewListener.skipOne = true;
}

function show_printer( designator, event ){
	ajax_link('details_view', 'index.php?widget=reports/printer_details&designator=' + designator, '');
	var ele = document.getElementById('details_view');
	// ele.style.display = 'block';
	var mouseEvent = event;
	var coords = getTotalOffsetFromElement(mouseEvent.target, document.getElementById('main_content_area'), 0, 0 );
	ele.style.left = coords.x + 'px';
	ele.style.top = coords.y + mouseEvent.target.offsetHeight + 10 + 'px';
	if(!window.MutationObserver){
		ele.style.display = 'block';
	}
	// window._detailsViewListener.skipOne = true;
}

function show_order_id_new_window( order_id ){
	window.open( 'index.php?widget=reports/order_details_v2&order_id=' + order_id, 'order_window', "width=300,height=600,left=" + event.screenX + ",top=" + event.screenY + ",menubar=0,location=0,titlebar=0" );
}

function toggle_section( ele ){
	if( ele.hasAttribute('wait')){
		return;
	}

	Color.reset();

	if( ele.hasAttribute('selected') ){
		ele.removeAttribute('selected');
		$('.eod_data_row[section=' + ele.getAttribute('section') +']').css({'display' : 'table-row'});
		$('.eod_data_row[section=' + ele.getAttribute('section') + ']').animate({opacity:'1'}, 500, 'swing', function(){
			ele.removeAttribute('wait');
		} );
		ele.children[0].textContent = 'Show Less';
	} else {
		ele.setAttribute('selected','');
		$('.eod_data_row[section=' + ele.getAttribute('section') + ']').animate({opacity:'0'},500,'swing',
			function(){
				$('.eod_data_row[section=' + ele.getAttribute('section') +']').css({'display' : 'none'});
				ele.removeAttribute('wait');
			 }
		);
		ele.children[0].textContent = 'Show More';
	}

	ele.setAttribute('wait','');
}

(function(){
	(function(){
		function BlurElementListener( element_id, property ) {
			this._element_id = element_id;
			if( !property ){
				property = 'display';
			}
			this._property = property;
			this.skipOne = false;
			document.body.addEventListener('click', this );
		}

		function isInHeirarchy( ele1, ele2 ){
			if( ele1 == ele2 ){
				return true;
			}

			if( ele1 === null ){
				return false;
			}

			return isInHeirarchy( ele1.parentNode, ele2 );
		}

		function handleEvent( event ){
			var ele1 = document.getElementById(this._element_id);

			if( !this.isInHeirarchy( event.target, ele1 ) ){
				if( this.skipOne ){
					this.skipOne = false;
					return;
				}
				ele1.style[this._property] = '';
			}
		}

		// BlurElementListener = BlurElementListener;
		BlurElementListener.prototype.constructor = BlurElementListener;
		BlurElementListener.prototype = {};
		BlurElementListener.prototype.isInHeirarchy = isInHeirarchy;
		BlurElementListener.prototype.handleEvent = handleEvent;
		window.BlurElementListener = BlurElementListener;
		window._detailsViewListener = new BlurElementListener('details_view');
	})();

	(function(){
		function TCalBlurListener( element_id1, element_id2, element_id3 ){
			window.BlurElementListener.call( this, element_id1 );
			this._element_id2 = element_id2;
			this._element_id3 = element_id3;
		}

		function handleEvent( event ){
			var ele1 = document.getElementById(this._element_id);
			var ele2 = document.getElementById(this._element_id2);
			var ele3 = document.getElementById(this._element_id3);

			if( event.target.id && (event.target.id == 'tcalPrevYear' || event.target.id == 'tcalPrevMonth' || event.target.id == 'tcalNextMonth' || event.target.id == 'tcalNextYear') ){
				return;
			}

			if( !this.isInHeirarchy( event.target, ele1 ) && !this.isInHeirarchy( event.target, ele2 ) && !this.isInHeirarchy( event.target, ele3 ) ){
				f_tcalRemoveClass( ele2, 'tcalActive' );
				f_tcalRemoveClass( ele3, 'tcalActive' );
				ele1.style.visibility = '';
			}
		}

		// TCalBlurListener = TCalBlurListener;
		TCalBlurListener.prototype.constructor = TCalBlurListener;
		TCalBlurListener.prototype = Object.create( window.BlurElementListener.prototype );
		TCalBlurListener.prototype.handleEvent = handleEvent;
		window._detailsViewListener = new TCalBlurListener('tcal', 'day', 'second_day');
	})();
	
})();

function show_report_tab( tabname ) {
	var ele = document.querySelector('tab-area > div[' + tabname + ']');
	var tabArea = document.querySelector('tab-area');

	for( var i = 0; i < tabArea.children.length; i++ ){
		if( !tabArea.children[i].hasAttribute('hide') ){
			tabArea.children[i].setAttribute('hide', '');
		}
	}

	ele.removeAttribute('hide');

	var nav = document.querySelector('nav.order_detail_navigation');
	var link = document.querySelector('nav.order_detail_navigation > div[' + tabname + ']');
	for( var i = 0; i < nav.children.length; i++ ){
		if( nav.children[i].hasAttribute('selected') ){
			nav.children[i].removeAttribute('selected');
		}
	}
	link.setAttribute('selected','');
}