<?php
require_once dirname(dirname(__FILE__)) . '/resources/dimensional_form.php';

	function connect_to_fiscal_restaurant($countries) {
		foreach ($countries as $cid => $company_id) {
			$rest_info_query_result = mrpt_query( "SELECT `data_name`, `id` FROM `poslavu_MAIN_db`.`restaurants` WHERE `id` = '[1]'", $company_id );
			if (!$rest_info_query_result || !mysqli_num_rows($rest_info_query_result)) {
				return false;
			}
			$rest_info = mysqli_fetch_assoc( $rest_info_query_result );
			$rdb = 'poslavu_' . $rest_info['data_name'] . '_db';
			$fiscal_restaurant_query = ers_query("SELECT `value` FROM `".$rdb."`.`config` WHERE `setting` = 'business_country' AND `location` = '[1]'", $company_id);
			$fiscal_restaurant_read = mysqli_fetch_assoc($fiscal_restaurant_query);
			if ($fiscal_restaurant_read['value'] != 'Paraguay' && $fiscal_restaurant_read['value'] != 'Israel') {
				unset($countries[$cid]);
			}
		}
		return array_values($countries);
	}

	function calculate_median($time_arr)
	{
		$count = count($time_arr);
		$midlevel = floor(($count-1)/2);
		sort($time_arr);
		if($count % 2) { 
			$median = $time_arr[$midlevel];
		} else {
			$low = $time_arr[$midlevel];
			$high = $time_arr[$midlevel+1];
			$median = floor(($low + $high)/2);
		}
		return $median;
	}
	
	function report_money_output($n, $monetary_symbol = "")
	{
		if( substr($n, 0,7) == '[money]'){
			return format_currency( str_replace(",", "", substr($n,7))*1, false,  $monetary_symbol );
		}
		return format_currency( $n, false, $monetary_symbol );
		// return "$" . number_format($n,2);
	}

	function report_money_to_number($n, $monetary_symbol="")
	{
        global $location_info;
        if(empty($monetary_symbol)){
            $monetary_symbol = $location_info['monitary_symbol'];
        }
		$v = $n;
		if( !report_value_is_money( $n, $monetary_symbol ) ){
			return $n;
		}
		if( substr($n."", 0,7)=='[money]'){
			$v = unformatMoney($n, $monetary_symbol);
		}
		else if(is_numeric(substr($n, 1, strlen($n)))){
            $v = str_replace(",", "", substr($n, 1, strlen($n)) ) * 1 ;
        }
        else if(!empty($monetary_symbol) && substr($n, 0, strlen($monetary_symbol)) == $monetary_symbol){
			$v = unformatCurrencyForInsert($n, $monetary_symbol);
    	}
		else {
			$v = unFormatCurrencyForInsert($n, $monetary_symbol);
		}
		return $v;
		// return str_replace("$","",str_replace(",","",$n));
	}

	function report_value_is_money($n, $monetary_symbol = "")
	{
		global $location_info;
		if(empty($monetary_symbol)){
			$monetary_symbol = $location_info['monitary_symbol'];
		}
		if( substr($n."", 0,7)=='[money]'){
			return true;
		}
		// LP-1540 -- Fixed sum_all_items() blanking out $order_info_totals in V2 EOD due to [number] values being interpretted as [money]
		if (substr($n.'', 0,8) == '[number]') {
			return false;
		}
		if( !empty($monetary_symbol)&& strstr($n, $monetary_symbol) === FALSE ){
			return false;
		}
		return !empty($monetary_symbol)?report_value_is_number( implode("",explode($monetary_symbol, $n) ) ):report_value_is_number( $n );
	}

	function report_query_money_concat_code( $val ){
		global $location_info;
		//For ERS just use this:
		// return "concat('$',format({$val}),2)), '$0.00')"

		// $left_symbol = ($location_info['left_or_right'] == 'left') ? $location_info['monitary_symbol'] : '';
		// $right_symbol = ($location_info['left_or_right'] == 'left') ? '' : $location_info['monitary_symbol'];
		$no_of_decimal = isset($location_info['disable_decimal'])?$location_info['disable_decimal']:2;

		// return "concat('{$left_symbol}',format({$val},{$no_of_decimal}),'{$right_symbol}')";
		//LP-511---Tax Rate calculating incorrectly across multiple reports in --V1 add cast() function
		return "concat('[money]',format(cast({$val}as decimal(30,5)),{$no_of_decimal}))";
	}

	function report_number_to_float($n){
		$v = $n;
		if( !report_value_is_number( $n ) ){
			return $n;
		}
		if( substr($n."",0,8) == '[number]' ){
			$v = str_replace(",", "", substr($n,8))*1;
		}

		// LP-1590 -- Changed the float conversion method to fix numbers with commas being truncated to the first number before the comma
		return (float) str_replace(',', '', $v);
	}

	// LP-1590 -- Added new function for V2 *_report_drawer.php's draw_multiple_rows() to use when adding [money] values and the result also needs to be a [money] value
	function report_float_to_money( $f ) {
		if ( substr($f.'', 0, 9) == '[money]' ) {
			return $f;
		}

		return '[money]' . $f;  // SOMEDAY : might need to add equivalent of MySQL format(n, 2) here, since that's generally the form in which [money] values are outputted
	}

	function report_float_to_number( $f ){
		if( substr($f."",0,8) == '[number]' ){
			return $f;
		}
		return '[number]' . $f;
	}
	
	function report_date_to_number( $d ){
		$v = $d;
	
		if(preg_match("/\d{4}-\d{2}-\d{2} \d{2}:\d{2}:\d{2}/", $d)){
			$v = str_replace("[date]", " ", $d);
		} else if (preg_match("/\d{2}:\d{2}:\d{2}/", $d)){
			$v = str_replace("[date]", " ", $d);
		} else if (preg_match("/\d{4}-\d{1}/", $d)){
			$v = str_replace("[date]", " ", $d);
		}
	
		return $v;
	}

	function report_value_is_number($n)
	{
		if( substr($n,0,8) == '[number]' ){
			return true;
		}
		$n = str_replace(".", "", str_replace(",", "", $n));
		// $decimals = "0123456789";

		// $str_parts = str_split($n."");
		// foreach( $str_parts as $key => $str_part ){
		// 	if( !in_array($str_part, $decimals)){
		// 		return false;
		// 	}
		// }
		return is_numeric( $n );
	}
	
	function report_value_is_date($n){
		if (preg_match("/\d{4}-\d{2}-\d{2} \d{2}:\d{2}:\d{2}/", $n))
		{
			return true;
		}else if(preg_match("/\d{2}:\d{2}:\d{2}/", $n)){
			return true;
		} else if (preg_match("/\d{4}-\d{1}/", $n)){
			return true;
		} else {
			return false;
		}
		return $n;
	}

	function report_number_output($n){
		if( substr($n, 0, 8) == '[number]'){
			if( fmod(substr($n,8)*1, 1.0) == 0.0 ){
				return substr($n,8)*1;
			}
			return format_currency( substr($n,8)*1, true );
		}
		if( fmod($n*1,1.0) == 0.0 ){
			return $n*1;
		}
		return format_currency( $n*1, true );
	}

	// LP-1426 -- Added new function to strip the correct thousands_char for [money] string
	// which is based on unFormatCurrencyForInsert() in core_functions but that function
	// couldn't be used because it always pulled decimal_separator from locations table
	// and this function will use decimal_separator and thousands_char from report data
	// queries when [money] identifier is present.
	function unformatMoney($monetary_str, $monetary_symbol = '')
	{
		global $location_info;

		if (empty($monetary_symbol)) $monetary_symbol = isset($location_info['monitary_symbol']) ? trim($location_info['monitary_symbol']) : '';
		$decimal_char = isset($location_info['decimal_char']) ? $location_info['decimal_char'] : '.';
		$thousands_char = (isset($location_info['decimal_char']) && $location_info['decimal_char'] == ',') ? '.' : ',';

		// Override location-specific settings for [money] strings and use database currency conventions
		if (substr($monetary_str."", 0,7)=='[money]') {
			$decimal_char = '.';
			$thousands_char = ',';
			$monetary_str = substr($monetary_str, 7);
		}

		// Strip out monetary symbol
		$monetary_str = str_replace($monetary_symbol, '', $monetary_str);

		// Strip out thousands character (to ensure money gets turned into number with format nnnn.nn)
		$monetary_str = str_replace($thousands_char, '', $monetary_str);

		// Change location-specific decimal separator character into decimal point
		$monetary_str = str_replace($decimal_char, '.', $monetary_str);

		$monetary_str_parts = explode('.', $monetary_str);

		$decimal_str = "";
		if (count($monetary_str_parts) > 1 ) {
			$decimal_str = array_pop($monetary_str_parts);
		}

		// This just gets the monetary_str before the decimal point
		$whole_numbers_str = implode($monetary_str_parts, '');

		$unformatted_monetary_str = '';

		// This adds the whole number string or keep $unformatted_monetary_str blank if it's a string with zero
		if (!empty($whole_numbers_str)) $unformatted_monetary_str .= $whole_numbers_str;

		// This adds on the monetary_str after the decimal point or leave the decimal part of $unformatted_monetary_str blank if it's a string with zero
		if (!empty($decimal_str)) $unformatted_monetary_str .= ".". $decimal_str;

		return $unformatted_monetary_str;
	}

	function allow_special_access(){
		if( function_exists("is_lavu_admin") ){
			return is_lavu_admin();
		} else {
			return false;
		}

	}

	function lib_path()
	{
		return "/home/poslavu/public_html/admin/lib/";
	}
	function get_export_url()
	{
		return "?widget=reports/v2_entry";
	}
	function get_current_foldername()
	{
		global $data_name;
		return $data_name;
	}
	function get_permission_level()
	{
		$lvl = admin_info("access_level");
		if(!$lvl) $lvl = 1; // not logged in
		if(isset($_SESSION['posadmin_access']))
		{
			if($_SESSION['posadmin_access']=="all") $lvl = 12;
			else $lvl = 6;
		}
		else if(admin_info("lavu_admin") == 1) $lvl = 12; // superadmin
		return $lvl;
	}
	function flat_multi_explode($str,$var1,$var2="",$var3="",$var4="")
	{
		$str_parts = explode($var1,$str);
		if($var2!="")
		{
			$new_str_parts = array();
			for($i=0; $i<count($str_parts); $i++)
			{
				$inner_parts = flat_multi_explode($str_parts[$i],$var2,$var3,$var4);
				for($n=0; $n<count($inner_parts); $n++)
				{
					$new_str_parts[] = $inner_parts[$n];
				}
			}
			$str_parts = $new_str_parts;
		}
		return $str_parts;
	}
	function is_alpha_numeric($str)
	{
		$is_legal = true;
		for($i=0; $i<strlen($str); $i++)
		{
			$char_is_legal = false;
			$ch = substr($str,$i,1);
			$nch = ord($ch);

			if($nch >= ord("a") && $nch <= ord("z")) $char_is_legal = true;
			else if($nch >= ord("A") && $nch <= ord("Z")) $char_is_legal = true;
			else if($nch >= ord("0") && $nch <= ord("9")) $char_is_legal = true;
			else if($ch=="_" || $ch=="-") $char_is_legal = true;

			if(!$char_is_legal)
			{
				$is_legal = false;
			}
		}
		return $is_legal;
	}

	function add_to_url($str,$urlstr)
	{
		if(strpos($str,"?")===false)
			$str = $str . "?" . $urlstr;
		else
			$str = $str . "&" . $urlstr;
		return $str;
	}
	if(!function_exists("getvar"))
	{
		function getvar($var,$def=false)
		{
			return (isset($_GET[$var]))?$_GET[$var]:$def;
		}
	}
	function memvar($var,$def=false)
	{
		if(isset($_SESSION[$var])) $def = $_SESSION[$var];
		$val = reqvar($var,$def);
		$_SESSION[$var] = $val;
		return $val;
	}

	function basic_datetime_add($dt,$val){
		//As Per LP-11458 TimeZone Issue
		date_default_timezone_set('UTC');

		$dt_parts = explode(' ', $dt );
		$dt_date = $dt_parts[0];
		$dt_time = $dt_parts[1];
		$dt_date_parts = explode('-', $dt_date );
		$dt_time_parts = explode(':', $dt_time );

		$time = mktime($dt_time_parts[0]*1, $dt_time_parts[1]*1, $dt_time_parts[2]*1, $dt_date_parts[1]*1, $dt_date_parts[2]*1, $dt_date_parts[0]*1 ) + $val;

		return date("Y-m-d H:i:s", $time );
	}


	function basic_date_add($dt,$val)
	{
		$dt_parts = explode("-",$dt);
		if(count($dt_parts) > 2)
		{
			return date("Y-m-d",mktime(0,0,0,$dt_parts[1],($dt_parts[2] + $val),$dt_parts[0]));
		}
		else if(count($dt_parts) > 1)
		{
			return date("Y-m",mktime(0,0,0,($dt_parts[1] + $val),1,$dt_parts[0]));
		}
		else if(count($dt_parts)==1)
		{
			return ($dt + $val);
		}
		else return $dt;
	}

	function basic_date_diff($dt1,$dt2)
	{
		if($dt1 > $dt2)
		{
			$dt1_temp = $dt1;
			$dt1 = $dt2;
			$dt2 = $dt1_temp;
		}
		$dt1_parts = explode("-",$dt1);
		$dt1_ts = mktime(0,0,0,$dt1_parts[1],$dt1_parts[2],$dt1_parts[0]);
		$dt2_parts = explode("-",$dt2);
		$dt2_ts = mktime(0,0,0,$dt2_parts[1],$dt2_parts[2],$dt2_parts[0]);
		$diff_days = floor(($dt2_ts - $dt1_ts) / 60 / 60 / 24);
		return $diff_days;
	}

	function basic_date_add_year($dt,$val=1)
	{
		$dt_parts = explode("-",$dt);
		if(count($dt_parts) > 2)
		{
			return date("Y-m-d",mktime(0,0,0,$dt_parts[1],$dt_parts[2],($dt_parts[0] + $val)));
		}
		else if(count($dt_parts) > 1)
		{
			return date("Y-m",mktime(0,0,0,$dt_parts[1],1,($dt_parts[0] + $val)));
		}
		else return $dt;
	}

	function basic_date_subtract_year($dt,$val=1)
	{
		return basic_date_add_year($dt,$val * -1);
	}

	function custom_display_value( $field_name, $field_value ) {
	    if ( in_array($field_name, array('order_id','kiosk_order','lavu_togo_order'))) {
			return '<span style="cursor: pointer;" onclick="show_order_id(&quot;' . $field_value . '&quot;, event)">'.$field_value.'</span>';
		} else if(  in_array($field_name, array('udid','opening_device','closing_device') ) ){
			return '<span style="cursor: pointer;" onclick="show_device(&quot;' . $field_value . '&quot;, event)">'.$field_value.'</span>';
		} else if( $field_name == 'register' ) {
			$value = explode(chr(30), $field_value);
			return '<span style="cursor: pointer;" onclick="show_printer(&quot;' . $value[1] . '&quot;, event)">'.$value[0].'</span>';
		} else if( $field_name == 'device' ){
			$value = explode(chr(30), $field_value);
			return '<span style="cursor: pointer;" onclick="show_device(&quot;' . $value[1] . '&quot;, event)">'.$value[0].'</span>';
		} else if( in_array($field_name, array('payment_type', 'payments')) && in_array(trim($field_value), array('Refund', 'Sale', 'Cash', 'Card', 'Gift Certificate', 'EGC', 'Visa', 'AMEX', 'MasterCard', 'DinersClub', 'enRoute', 'Discover', 'JCB', 'Debit', 'EBT', 'WAX', 'JCB', 'Voyager', 'cp') ) ) {
				return $field_value;
		} else if( in_array($field_name, array('summary', 'paid_in_or_out', 'marked_deposits_collected', 'deposits_not_yet_redeemed', 'statistic', 'tax_inclusion', 'action', 'card_type', 'order_closed_weekday', 'weekday', 'month', 'order_closed_month') ) ) {
			if (preg_match("/^[a-z \/]+$/i", $field_value)) {
				return $field_value;
			} else {
				return $field_value;
			}
		}
		else if( $field_name == 'combo_id'){//added for LP-3802
		    return '<span style="cursor: pointer;" onclick="show_combo_info(&quot;' . $field_value . '&quot;, event)">'.$field_value.'</span>';
		}

		return $field_value;
	}

	function ers_display_date($dt)
	{
		if(is_numeric($dt))
		{
			$ts = $dt;
		}
		else
		{
			$dt_parts = explode("-",$dt);
			if(count($dt_parts) > 2)
			{
				$ts = mktime(0,0,0,$dt_parts[1],$dt_parts[2],$dt_parts[0]);
			}
			else $ts = time();
		}
		return date("m/d/Y",$ts);
	}

	function lzero($t)
	{
		if($t < 10)
			return "0" . ($t * 1);
		else
			return ($t * 1);
	}

	function get_month_name($n,$mtype="F")
	{
		if(strpos($n,"-"))
		{
			$n_parts = explode("-",$n);
			$n = $n_parts[1];
		}
		return date($mtype,mktime(0,0,0,($n * 1),1,date("Y")));
	}

	function show_as_title($str)
	{
		return ucwords(str_replace("_"," ",$str));
	}

	function decode_spelled_url($str)
	{
		$str = str_replace("(question)","?",$str);
		$str = str_replace("(and)","&",$str);
		$str = str_replace("(ampersand)","&",$str);
		$str = str_replace("(equal)","=",$str);
		$str = str_replace("(dot)",".",$str);
		return $str;
	}

	function replaceLocationID( $str, $dataname = ""){
		global $location_info;
		$conn = ConnectionHub::getConn('poslavu');
		$str = str_replace('[default_order_tag]', $conn->escapeString((isset($location_info['dine_in_order_type_name'])?$location_info['dine_in_order_type_name']:'Dine In')), $str );
		if(strpos($str,"[chainid]")!==false)
		{
			if($chainid = sessvar('admin_chain_id')) $str = str_replace('[chainid]', $conn->escapeString($chainid), $str);
			else $str = str_replace("[chainid]","no-chainid-defined",$str);
		}
		if(strpos($str,"[dataname]")!==false  && empty($dataname))
		{
			if($dataname = sessvar('admin_dataname')) $str = str_replace('[dataname]', $conn->escapeString($dataname), $str);
			else $str = str_replace("[dataname]","no-dataname-defined",$str);
		} else {
			$str = str_replace('[dataname]', $dataname, $str);
		}
		return  str_replace( '[locationid]', $conn->escapeString(sessvar("locationid")),  $str );
	}

	/**
	 * Will save favorite as a future entry to be displayed in the reports themselves
	 * @param  string $report could be the report name or the report id, but it should be consistent on saving and loading
	 * @param  string $name   the name of the favorite to be saved, this should be the displayed title of the report
	 * @param  string $link   the link containing the get variables needed to get to this snap shot. More-than-likely the drill forward vars
	 * @return bool         This should return whether the report was successfully saved or not.
	 */
	function saveFavoriteReport( $report, $name, $link ){
		$query_params = array(
			'locationid' => sessvar('locationid'),
			'type' => 'report_v2',
			'setting' => 'favorite',
			'link' => $link,
			'name' => $name,
			'report' => $report
		);
		return lavu_query("INSERT INTO `config` ( `location`, `type`, `setting`, `value`, `value2`, `value3` ) VALUES ( '[locationid]', '[type]', '[setting]', '[link]', '[name]', '[report]' )", $query_params) !== FALSE;
	}

	/**
	 * This function takes a report and should load all favorites that have been saved an associated with the provied $report entry.
	 * @param  string $report the string that represents the stored report's favorites, should be the same thing provided to saveFavoriteReport, should be either an id or the original report name
	 * @return array         an array of saved favorite details
	 */
	function loadFavoriteReports( $report ){
		static $storedResult = array();
		if( !isset( $storedResult[$report] ) ){
			$query_params = array(
				'locationid' => sessvar('locationid'),
				'report' => $report
			);
			$query_result = lavu_query("SELECT `id` as `id`, `value` as `link`, `value2` as `name` FROM `config` WHERE `type` ='report_v2' AND `setting` = 'favorite' AND `location` = '[locationid]' AND `value3` = '[report]'", $query_params);
			if( !$query_result ){
				return array();
			}

			$result = array();
			while( $fetched = mysqli_fetch_assoc($query_result)){
				$result[] = $fetched;
			}
			$storedResult[$report] = $result;
		}
		return $storedResult[$report];
	}

	function deleteFavorite( $id ){
		$query_params = array(
			'id' => $id
		);
		return lavu_query("DELETE FROM `config` WHERE `id` = '[id]' LIMIT 1", $query_params) !== FALSE;
	}

	function replaceStartAndEndTime( $str ){
		$search_term = '[day_start_end_time]';
		$location_info = sessvar("location_info");
		if( !$location_info ){
			return str_replace($search_term, '12:00', $str);
		}

		$day_start_end_time = $location_info ['day_start_end_time']*1;

		$minutes = $day_start_end_time % 100;
		$day_start_end_time -= $minutes;
		$day_start_end_time /= 100;
		$hours = $day_start_end_time % 100;

		return str_replace($search_term, "{$hours}:{$minutes}", $str);
	}

	function adjustStartAndEndTimes( &$vars ){
		$set_start_time = memvar("set_start_time","");
		if( !empty( $set_start_time ) ){
			return;
		}

		if( !count( $vars ) ){
			return;
		}

		if( !$vars['start_datetime'] || !$vars['end_datetime']){
			return;
		}

		$location_info = sessvar("location_info");
		if( !$location_info ){
			return;
		}
		$day_start_end_time = $location_info ['day_start_end_time']*1;

		$minutes = $day_start_end_time % 100;
		$day_start_end_time -= $minutes;
		$day_start_end_time /= 100;
		$hours = $day_start_end_time % 100;

		$seconds = (($hours * 60) + $minutes)*60;


		$set_start_time = memvar("set_start_time","");
		$set_end_time = memvar("set_end_time","");

		if( empty($set_start_time) && empty($set_end_time) ){
			$start_time = basic_datetime_add( $vars['start_datetime'], $seconds );
			$end_time = basic_datetime_add( $vars['end_datetime'], $seconds );
			$vars['start_datetime'] = $start_time;
			$vars['end_datetime'] = $end_time;
		}
	}

	function get_location_name(){
		$result = '';

		if( ($query_result = lavu_query( "SELECT `title` FROM `locations`")) === FALSE ){
			return $result;
		}

		if( !mysqli_num_rows( $query_result ) ){
			return $result;
		}

		$location_name_arr = mysqli_fetch_assoc( $query_result );
		$result = $location_name_arr['title'];

		return $result;
	}

	function connect_to_chain_database( $company_id ) {
		$rest_info_query_result = mlavu_query( "SELECT `data_name`, `id` FROM `poslavu_MAIN_db`.`restaurants` WHERE `id` = '[1]'", $company_id );
		if( !$rest_info_query_result || !mysqli_num_rows($rest_info_query_result)){
			return false;
		}

		$rest_info = mysqli_fetch_assoc( $rest_info_query_result );
		$rdb = 'poslavu_' . $rest_info['data_name'] . '_db';
		lavu_connect_byid($company_id,$rdb);
		return true;
	}

	function get_chain_reporting_group($reportName){
		require_once dirname(dirname(__FILE__)) . '/resources/chain_functions.php';
		static $result = null;
		if( $result ){
			return $result;
		}
		global $data_name;
		global $modules;
		$multi_report = reqvar('multi_report','0');
		$reporting_chain_groups = CHAIN_USER::get_user_reporting_groups();
		// if(!count($reporting_chain_groups)){
		// 	return array( 'display' => '' );
		// }

		$reporting_chain_groups = array_merge( array( array( 'id' => 0, 'contents' => admin_info("companyid"), 'name' => speak('Current Location') ) ), $reporting_chain_groups );
		$reporting_chain_groups = array_values( $reporting_chain_groups );

		$multi_report_index = 0;
		$found = false;
		for( $i = 0; $i < count( $reporting_chain_groups ); $i++ ){
			if( $reporting_chain_groups[$i]['id'] == $multi_report ){
				$found = true;
				$multi_report_index = $i;
				break;
			}
		}

		if(!$found){
			$multi_report = '0';
		}

		$display_text = '';
		
		if ($reportName!="tip_sharing") {
		    if (!empty($reporting_chain_groups)) {
		        $display_text .= "<div class=\"input_wrapper\" style=\"padding: 5px; border: 1px solid #ccc;\"><select onchange=\"click_to_page('[url]&multi_report=' + this.value);\">";
        
        			$arrCnt = count( $reporting_chain_groups );
        
        			for( $i = 0; $i < $arrCnt; $i ++ ) {
        				// $reporting_chain_groups[$i]['restaurant_ids'] = explode(',', $reporting_chain_groups[$i]['contents'] );
        				$selected = '';
        				if( $reporting_chain_groups[$i]['id'] == $multi_report ) {
        					$selected = ' selected ';
        				}
        
        				$display_text .= "<option value='".$reporting_chain_groups[$i]['id']."'{$selected}>".$reporting_chain_groups[$i]['name']."</option>";
        			}
        			$display_text .= '</select></div>';
        		}
		}

		$more_than_one_location = count( $reporting_chain_groups ) > 1;

		$reporting_chain_groups['seperate_locations'] = '0';
		if( $multi_report != '0' ){
			$sep_locations = reqvar('sep_loc', '0' );
			$separation_options = array( speak('Group Locations'), speak('Separate Locations'), speak('Separate (Local Currency)') );

			$display_text .= "<div class=\"input_wrapper\" style=\"padding: 5px; border: 1px solid #ccc;\"><select onchange=\"click_to_page('[url]&multi_report={$multi_report}&sep_loc=' + this.value );\">";
			foreach( $separation_options as $key => $option ){
				$selected = '';
				if( $sep_locations == $key ){
					$selected = ' selected ';
				}
				$display_text .= "<option value=\"{$key}\"{$selected}>{$option}</option>";
			}
			$display_text .= "</select></div>";
			$reporting_chain_groups['seperate_locations'] = $sep_locations;
		}

		if( $more_than_one_location && $modules && $modules->hasModule( 'chains.view' ) ){
			$reporting_chain_groups['display'] = $display_text;
		} else {
			$reporting_chain_groups['display'] = '';
		}
		$reporting_chain_groups['rests'] = $reporting_chain_groups[$multi_report_index]['contents'];
		$reporting_chain_groups['multi_report'] = $multi_report;

		$result = $reporting_chain_groups;
		return $result;
	}
	
	function translatePhrase($needToTranslate) {
	    
	    $originalText = trim ( ucwords ( str_replace ( "_", " ", $needToTranslate ) ) );
	    
	    $translated_human_phrase4 = speak ( $originalText );
	    if ($translated_human_phrase4 != $originalText) {
	        $translated_phrase4 = str_replace ( " ", "_", trim ( str_replace ( ".", "", strtolower ( $translated_human_phrase4 ) ) ) );
	        $translated_phrases4 = $translated_phrase4;
	    }else{
	        $translated_phrases4 = $needToTranslate;
	    }
	    return  $translated_phrases4;
	}

	function respectTimezoneForAllReports($hours = "", $minutes = "") {

		global $location_info;
		$datetime_str = DateTimeForTimeZone($location_info['timezone']);
		$datetime_arr = explode(" ", $datetime_str);

		if ($hours == "" || $minutes == "") {
			$day_start_end_time = $location_info['day_start_end_time']*1;
			$minutes = $day_start_end_time % 100;
			$day_start_end_time -= $minutes;
			$day_start_end_time /= 100;
			$hours = $day_start_end_time % 100;
		}
		$seconds = (($hours * 60) + $minutes)*60;
		$currentTime = $datetime_arr[1];
		$splitTime = explode(":", $currentTime);
		$splitHours = $splitTime[0];
		$splitMinutes = $splitTime[1];
		$presentSec = (($splitHours * 60) + $splitMinutes)*60;
		$status = ($presentSec < $seconds) ? -1 : 0;

		return $status;
	}

	/**
	 * This function is used to prepare tax profiles of order ids.
	 *
	 * @param array $orderIds list of order Ids
	 * @param array $tempReportResults
	 * @param string $flag
	 * @param string $separate chain location/ separate location
	 * @param array $globalTaxes list of global taxes
	 * @param string $taxIdFlag - to confirm Tax Id Numer need to show or not.
	 *
	 * @return array list of tax profile names.
	 */

	function getTaxProfileNames($orderIds, $tempReportResults, $flag = '', $separate, &$globalTaxes = array(), $taxIdFlag = 0) {
		$taxQuery = ers_query("select id, order_id, apply_taxrate, tax1, tax2, tax3, tax4, tax5 from order_contents where order_id in ('".implode("', '", $orderIds)."')");
		$taxDetails = array();
		$orderProfiles = array();
		while ($taxInfo = mysqli_fetch_assoc($taxQuery)) {
			$taxProfilesExploded = explode(";;", $taxInfo['apply_taxrate']);
			$taxNumber = 1;
			if (count($taxProfilesExploded)>0) {
				$taxAmount = 0;
				$taxName ='';
				foreach ($taxProfilesExploded as $tax) {
					$taxValues = explode("::", $tax);
					$taxName = $taxValues[1];
					$taxAmount = $taxInfo['tax'.$taxNumber];
					$orderProfiles[$taxInfo['order_id']][$taxName] = $taxAmount;
					if(!in_array($taxName, $taxDetails) ){
						$taxDetails [$taxName] = '[money]0.00';
					}
					$taxNumber++;
				}
			}
		}

		if ($flag == 1 && $separate == 0) {
			$globalTaxes[] = $taxDetails;
		}

		if ($taxIdFlag) {
			$fiscalTempTaxInfo = getFiscalTempTaxInfo();
		}

		foreach ($tempReportResults as $temp ) {
			foreach ($temp as $tempDetails) {
				$tempResults[] = $tempDetails;
			}
		}
		$temporary = array();
		foreach ($tempResults as $temp) {
			if ($taxIdFlag) {
				$temp = addExtraFiscalColumns($temp, $fiscalTempTaxInfo);
			}
			$firstTemp = array_slice($temp, 0, 6);
			$lastTemp = array_slice($temp, 6);
			if ($flag != 1) {
				$splitOrderId = explode("]", $temp['Order ID']);
				$orderId = $splitOrderId[1];
			} else {
				$orderId = $temp['Order ID'];
			}
			$midTemp = $taxDetails;
			foreach ($orderProfiles[$orderId] as $profile => $value) {
				$midTemp[$profile] = "[money]".$value;
			}
			$temporary[][] = $firstTemp + $midTemp + $lastTemp;
		}
		return $temporary;
	}

	function addExtraFiscalColumns($resultsData, $fiscalTempTaxInfo) {
		$firPos = array_search('Location Name', array_keys($resultsData));
		$secPos = array_search('Order ID', array_keys($resultsData));

		$resArray = array_slice($resultsData, $firPos, 1, true) + array("Tax ID Number" => $fiscalTempTaxInfo['tax_id']) + array_slice($resultsData, $firPos, $secPos, true) + array("Timbrado" => $fiscalTempTaxInfo['text_input']) + array_slice($resultsData, $secPos, 2, true);
		return $resArray;
	}

	function updateTaxesForAllLocations($loopTaxes, $report_results) {
		$tempResults = array();
		foreach ($report_results as $temp) {
			foreach ($temp as $tempDetails) {
				foreach ($tempDetails as $temporary) {
					$tempResults[] = $temporary;
				}
			}
		}
		$temporary = array();
		foreach ($tempResults as $temp) {
			$pos = array_search('Total', array_keys($temp));
			$firstArray = array_slice($temp, 0, $pos);
			$lastArray = array_slice($temp, $pos);
			foreach ($loopTaxes as $key => $value) {
				if (!array_key_exists($key, $temp)) {
					$firstArray[$key] = "[money]".$value;
				}
			}
			$temporary[][] = $firstArray + $lastArray;
		}
		$results[] = $temporary;
		return $results;
	}

	/**
	 * Get pagination limit and number of pages to display
	 * @param string
	 * @return array
	 */
	function fetchPaginationConfig($setting = '') {
		$data = [];
		$query = "select `value` from `main_config` where `setting` = '".$setting."' LIMIT 1";
		$queryResult = mlavu_query($query);
		if (mysqli_num_rows($queryResult) > 0) {
			while ($result = mysqli_fetch_assoc($queryResult)) {
				if (isset($result['value'])) {
					$data = json_decode($result['value']);
				}
			}
		}

		return $data;
	}

	class area_foundation
	{

		public function argval($set_args,$key,$def=false,$memvar_name="")
		{
			if(isset($set_args[$key]))
				return $set_args[$key];
			else if($memvar_name!="" && memvar($memvar_name))
				return memvar($memvar_name);
			else
				return $def;
		}

		public function argval_bool($set_args,$key,$def=false,$memvar_name="")
		{
			$val = substr(strtolower($this->argval($set_args,$key,$def,$memvar_name)),0,1);
			if($val=="1"||$val=="t"||$val=="y") return true;
			else if($val=="0"||$val=="f"||$val=="n") return false;
			else return $def;
		}

		public function setting($setting_name,$def="")
		{
			return system_setting($setting_name,$def);
		}

		public function arg_is_true($arr,$arg,$def=false)
		{
			if(isset($arr[$arg]))
			{
				$pstr = substr(strtolower(trim($arr[$arg])),0,1);
				if($pstr=="1" || $pstr=="y" || $pstr=="t")
					return true;
				else
					return false;
			}
			else return $def;
		}
		public function set_ajax_fwd_vars($varname,$urlname="")
		{
			if(!isset($this->ajax_fwd_vars))
				$this->ajax_fwd_vars = array();
			if(is_array($varname))
				$this->ajax_fwd_vars = array_merge($this->ajax_fwd_vars,$varname);
			else if($urlname!="")
				$this->ajax_fwd_vars = array_merge($this->ajax_fwd_vars,array($varname=>$urlname));
			else
				$this->ajax_fwd_vars = array_merge($this->ajax_fwd_vars,array($varname=>$varname));
		}
	}

	function setWhereClauseForReport($vars, $queryMode) {

		global $location_info;
		$timeZone = "";
		if (isset($location_info['timezone'])) {
			$timeZone = $location_info['timezone'];
		}

		if ($queryMode === 'day') {
			$startDate = $vars['start_date'];
			$endDate = $vars['end_date'];
			$startTime = $vars['start_time'];
			$endTime = $vars['end_time'];
		} else if ($queryMode === 'month' || $queryMode === 'year') {
			$startDate = $vars['start_datetime'];
			$endDate = $vars['end_datetime'];
			$startTime = $vars['start_time'];
			$endTime = $vars['end_time'];
		} else {
			$startDate = gmdate('Y-m-d', strtotime($vars['start_date']." ".$timeZone));
			$endDate = gmdate('Y-m-d', strtotime($vars['end_date']." $timeZone"));
			$startTime = gmdate('H:i:s', strtotime($vars['start_time']." $timeZone"));
			$endTime = gmdate('H:i:s', strtotime($vars['end_time']." $timeZone"));
		}

		$retunrArr = array();
		if ($queryMode === 'day') {
			$retunrArr['where_clause'] = " DATE_FORMAT(`datetime`, '%Y-%m-%d') BETWEEN '$startDate' AND '$endDate' AND DATE_FORMAT(`datetime`, '%H:%i:%s') BETWEEN '$startTime' AND '$endTime' ";
			$retunrArr['order_clause'] = " DATE_FORMAT(`orders`.`closed`, '%Y-%m-%d') BETWEEN '$startDate' AND '$endDate' AND DATE_FORMAT(`orders`.`closed`, '%H:%i:%s') BETWEEN '$startTime' AND '$endTime' ";
		} else {
			$retunrArr['where_clause'] = " DATE_FORMAT(`datetime`, '%Y-%m-%d') BETWEEN '$startDate' AND '$endDate' ";
			$retunrArr['order_clause'] = " DATE_FORMAT(`orders`.`closed`, '%Y-%m-%d') BETWEEN '$startDate' AND '$endDate' ";
		}
		return $retunrArr;
	}

	/**
	* This function is used to get Fiscal Template Tax details.
	*
	* @return array Fiscal Template Tax details.
	**/
	function getFiscalTempTaxInfo() {
		$fiscalTaxInfo = array();
		$fiscalTaxQuery = "SELECT `l_frt`.`value`, `m_frf`.`title`, `m_frtf`.`macros` FROM `poslavu_MAIN_db`.`fiscal_receipt_fields` AS `m_frf` INNER JOIN `poslavu_MAIN_db`.`fiscal_receipt_tpl_fields` AS `m_frtf` ON `m_frf`.`fiscal_receipt_field_id` = `m_frtf`.`fiscal_receipt_field_id` INNER JOIN  `fiscal_receipt_tpl` AS `l_frt` ON `m_frtf`.`fiscal_receipt_tpl_field_id` = `l_frt`.`fiscal_receipt_tpl_field_id` WHERE (`m_frf`.`title` = 'tax_id' || (`m_frtf`.`macros` = 'invoice_code' AND `m_frf`.`title` = 'text_input'))";
		$fiscalTaxReport = ers_query($fiscalTaxQuery);
		while($fiscalTaxReportRead = mysqli_fetch_assoc($fiscalTaxReport)) {
			$fiscalTaxInfo[$fiscalTaxReportRead['title']] = $fiscalTaxReportRead['value'];
		}
		return $fiscalTaxInfo;
	}
