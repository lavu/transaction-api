<?php
	$filePath = dirname(__FILE__);
	require_once $filePath."/../../resources/core_functions.php";
	require_once $filePath ."/../report_functions.php";
	session_start();

	if (sessvar_isset('location_info')) {
		$multiReport = reqvar('multi_report', '');
		$isMultiReport = empty($multiReport) ? false :true;
		$dataname = sessvar("admin_dataname");
		$sepLocation = reqvar('sep_loc', '');
		$isSeperateLocation = empty($sepLocation) ? false : true;
		$chainid = sessvar("admin_chain_id");
		$mode = reqvar('mode', 'day');
		$numberOfRecord = reqvar('recordPerPage', 0);
		$currentActivePage = reqvar('page', 0);
		$limit = reqvar('limit', 0);
		$offset = reqvar('offset', 0);
		$offset = $offset > 1 ? $offset : 0;
		$sort = reqvar('sort', 'asc');
		$sortColumn = reqvar('sortColumn', 'dataname');
		$locationName = reqvar('locationName', '');
		if ($locationName !== '') {
			set_sessvar(strtolower('Order Location By - '. $locationName), $sortColumn.' '.$sort);
		}
		switch ($sortColumn) {
			case 'account_number':
				$sortColumn = "concat(name,' ')";
				break;
			case 'gift_balance':
				$sortColumn = "(sum(points * 1)*1)";
				break;
			case 'account_number':
				$sortColumn = "(sum(balance * 1)*1)";
				break;
			default:
			break;
		}

		switch ($mode) {
			case 'month':
				$startMonth = reqvar('startMonth', '');
				$reportMonth = reqvar('report_month', '');

				$monthYear = !empty($startMonth) ? date('Y-m', strtotime($startMonth)) : date("Y-m");
				$monthYear = !empty($reportMonth) ? $reportMonth : $monthYear;
				$startDate = $monthYear. "-01 00:00:00";
				$endDate = $monthYear. "-31 24:00:00";
				break;

			case 'year':
				$startYear = reqvar('startYear', '');
				$reportYear = reqvar('report_year', '');

				$currentYear = !empty($startYear) ? $startYear : date("Y");
				$year= !empty($reportYear) ? $reportYear : $currentYear;
				$startDate = $year . "-01-01 00:00:00";
				$endDate = $year . "-12-31 24:00:00";
				break;

			case 'day':
				$reportDay = sessvar('report_day', '');
				$reportDaySecond = sessvar('report_day_second', '');
				$startTime = sessvar('set_start_time', '');
				$endTime = sessvar('set_end_time', '');

				$currentStartDate = !empty($reportDay) ? $reportDay : date("Y-m-d");
				$currentEndDate =!empty($reportDaySecond) ? $reportDaySecond : $currentStartDate;
				$currentStartTime = !empty($startTime) ? formatTime($startTime) : "00:00:00";
				$currentEndTime = !empty($endTime) ? formatTime($endTime) : "24:00:00";

				$overrideStartDay = reqvar('report_day', '');
				$overrideReportDaySecond = reqvar('report_day_second', '');
				$overrideStartTime = reqvar('set_start_time', '');
				$overrideEndTime = reqvar('set_end_time', '');

				$startDay = !empty($overrideStartDay) ? $overrideStartDay : $currentStartDate;
				$endDay = !empty($overrideReportDaySecond) ? $overrideReportDaySecond : $currentEndDate;
				$statTime = !empty($overrideStartTime) ? formatTime($overrideStartTime) : $currentStartTime;
				$endTime = !empty($overrideEndTime) ? formatTime($overrideEndTime) : $currentEndTime;
				$startDate = $startDay." ".$statTime;
				$endDate = $endDay." ".$endTime;
				break;
		}

		if ($isSeperateLocation) {
			$locationQuery = "select dataname from restaurant_locations where title = '".$locationName."'";
			$locationDataName = mlavu_query($locationQuery);
			$result = mysqli_fetch_assoc($locationDataName);
			$dataname = $result['dataname'];
		}
		$filter = "(dataname='".$dataname."' and dataname!='')";
		$filter = $isMultiReport && !$isSeperateLocation ? "(".$filter." OR (chainid='".$chainid."' and chainid!=''))" : $filter;
		$querystring = "select concat(name,' ') as account_number, SUM(balance * 1) as gift_balance, dataname, SUM(points * 1) as loyalty_points from `poslavu_MAIN_db`.`lavu_gift_cards` where lavu_gift_cards.deleted = 0 AND lavu_gift_cards.inactive = 0 AND lavu_gift_cards.created_datetime >= '".$startDate."' and lavu_gift_cards.created_datetime <= '".$endDate."' and ".$filter." group by concat(name, ' '),dataname order by ".$sortColumn." ".$sort." limit $offset,$limit";
		$query = mlavu_query($querystring);
		$res = [
			"data" => [],
			"counts" =>[]
		];
		if (mysqli_num_rows($query) > 0) {
			while ($result = mysqli_fetch_assoc($query)) {
				array_push($res["data"], $result);
			}
		}
		$giftBalance = 0;
		$loayalityPoints = 0;
		foreach ($res['data'] as $key => $data) {
			$giftBalance += $data['gift_balance'];
			$loayalityPoints += $data['loyalty_points'];
			$res['data'][$key]['gift_balance'] = report_number_output($data['gift_balance']);
		}

		$res['counts'] = [
			"giftCardBalance" => report_number_output($giftBalance),
			"loyalityBalance" => $loayalityPoints
		];

		$myJSON = json_encode($res);
		echo $myJSON;
	}

	function formatTime($time) {
		return str_pad($time, 2, '0', STR_PAD_LEFT).":00:00";
	}
?>