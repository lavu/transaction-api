<?php
    $filePath = dirname(__FILE__);
    require_once $filePath ."/../../resources/core_functions.php";
    require_once $filePath ."/../report_functions.php";
    require_once $filePath ."/../constants/reportConstants.php";
    echo json_encode(fetchPaginationConfig(PAGINATION_LAVU_GIFT_AND_LOYALTY));
    exit;
?>