<?php
class report_alias_types {
	const JOIN		= 1;//(1<<0); Probably not possible here
	const SELECT	= 2;//(1<<1);
	const WHERE		= 4;//(1<<2);
	const GROUP_BY	= 8;//(1<<3);
	const ORDER_BY	= 16;//(1<<4);
	const DISPLAY	= 32;//(1<<5);
	const ALL		= PHP_INT_MAX;//(JOIN | SELECT | WHERE | GROUP_BY | ORDER_BY);

	public static function typeByStr( $str ){
		switch( $str ){
			case 'join' : return report_alias_types::JOIN;
			case 'select' : return report_alias_types::SELECT;
			case 'where' : return report_alias_types::WHERE;
			case 'groupby' : return report_alias_types::GROUP_BY;
			case 'orderby' : return report_alias_types::ORDER_BY;
			case 'display' : return report_alias_types::DISPLAY;
			case 'all' : return report_alias_types::ALL;
			default : return null;
		}
	}
}

class report_alias {
	private
		$type,
		$name,
		$statement,
		$label;

	private function __construct( $type, $name, $statement, $label='' ){
		$this->type = $type;
		$this->name = $name;
		$this->statement = $statement;
		$this->label = $label;
		if(!$label){
			$this->label = $name;
		}
	}

	public function expand( $type=null ){
		switch( $type ){
			case report_alias_types::SELECT :
				if( !empty($this->label) ){
					return $this->statement . ' as ' . $this->label;
				}
			case report_alias_types::JOIN :
			case report_alias_types::WHERE :
			case report_alias_types::GROUP_BY :
			case report_alias_types::ORDER_BY :
			case report_alias_types::DISPLAY :
			case report_alias_types::ALL :
				return $this->statement;
			default :
				return $this->expand( $this->type );
		}
	}

	public function type(){
		return $this->type;
	}

	public function name(){
		return $this->name;
	}

	public function statement(){
		return $this->statement;
	}

	public function label(){
		return $this->label;
	}

	private static $_aliases = array();
	public static function alias( $name, $type=null, $statement=null, $label=null ){
		if( !isset( self::$_aliases[$name] ) && $type && $statement ){
			self::$_aliases[$name] = new report_alias( $type, $name, $statement, $label );
		}

		return self::$_aliases[$name];
	}

	public static function aliasNames(){
		return array_keys( self::$_aliases );
	}
}