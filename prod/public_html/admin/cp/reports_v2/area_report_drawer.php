<?php
$filePath = dirname(__FILE__);
require_once $filePath .'/iarea_report_drawer.php';
require_once $filePath .'/area_reports.php';
require_once $filePath .'/constants/reportConstants.php';

class area_report_drawer implements iarea_report_drawer
{

    private $area_reports;

    public function __construct($area_reports)
    {
        $this->area_reports = $area_reports;
    }

    private function should_show_col($colname)
    {
        if (substr($colname, 0, 9) == "group_col")
            return false;
        else
            return true;
    }

    public function checkExists($sarray, $array)
    {
        $result = - 1;
        for ($i = 0; $i < sizeof($array); $i ++) {
            if ($array[$i]['order_item'] == $sarray) {
                $result = $i;
                break;
            }
        }
        return $result;
    }

    private function finalize($table_str, $graph_str, $graphlist)
    {
        $reporting_groups = $this->area_reports->chain_reporting_groups();
        $reporting_groups_ouput = ($reporting_groups && $reporting_groups['display']) ? $reporting_groups['display'] : '';
        $selector_result = $this->area_reports->datetime_selector();
        if ($this->area_reports->allow_controls)
            $selector_output = $selector_result['output'];
        else
            $selector_output = "";
        $str = "";

        // return;
        $str .= $this->area_reports->breadcrumbs();
        $str .= "<div id=\"details_view\"></div>";
        $str .= "<table style='float: left;'><tr><td align='center'>";
        // if($selector_output!="")
        // $str .= "<br><br>";
        $str .= "<table><tr>";
        $str .= "<td align='left' valign='top' style='min-width:500px;'>";
        $str .= "<div style=\"display: table; margin: 0 auto;\">{$reporting_groups_ouput}</div>";
        $str .= $selector_output;
        $str .= $table_str;

        $str .= "</td>";
        if ($graph_str != "") {
            $str .= "<td>&nbsp;</td>";
            $str .= "<td align='left' valign='top'>";
            $str .= $graph_str;
            $str .= "</td>";
        }
        $str .= "</tr></table>";
        $str .= "</td></tr></table>";

        // $str .= $display_extra;

        if ($graphlist && count($graphlist)) {
            foreach ($graphlist as $columnName => $value) {
                $json = json_encode($value['data']);
                //$str .= "<!--<canvas id=\"c_{$columnName}\" data='{$json}' width=\"500\" height=\"500\" style='width: 500px; height: 500px;' ></canvas>-->";   // LP-5320 changes, as this line html commented so that complete line is commented
            }
        }

        return $str;
    }

    public function draw_multiple_rows($report_results) {
        global $location_info;
        $multiReport = 0;
        if (isset($_GET['multi_report'])) {
            $multiReport = $_GET['multi_report'];
        }
        $reporting_groups = $this->area_reports->chain_reporting_groups();
        $separate = $reporting_groups['seperate_locations'];
        #This condition added for LP-8662 && LP-10454. Data was coming in 4th level of result array
        $report_info = $this->area_reports->get_report_info_from_getvar();        
        $requestedReport = array('discounts', 'taxes');
        if($separate && $multiReport  && in_array($report_info['name'], $requestedReport)){
            $report_results = $report_results[0];
        }

        $key = array_keys($report_results[0][0][0]);
        $drilldownkey = array_search("order_item", $key);
        $checkContentOptions = in_array("content_options", $key);
        $orderByItem = "";    
        if ( isset($_SESSION['drilldown']) && $_SESSION['drilldown'] == 0 && isset($key[0]) && $key[0] == "order_item") {
            $orderByItem = "order_item";
        }
        else if ( isset($_SESSION['drilldown']) && $_SESSION['drilldown'] == 1 && isset($key[$drilldownkey]) && $key[$drilldownkey] == "order_item") {
            $orderByItem = "order_item";
        }
        if ( $multiReport != 0 && $orderByItem == "order_item" && $separate == 0 && $checkContentOptions == false) {
            $report_results = $this->area_reports->unique_multidim_array($report_results,$orderByItem);
        }
        if ($this->area_reports->report_name === TYPE_LAVU_GIFT_AND_LOYALITY && $separate == 0) {
            $filter = 'account_number';
            $report_results = $this->area_reports->unique_multidim_array($report_results,$filter);
        }
        if (isset($report_results['location_names']) && $report_results['location_names'] != '') {
            ksort($report_results['location_names']);
            $report_results_loc = array_unique($report_results['location_names']);
        }

        $selector_result = $this->area_reports->datetime_selector();
        $selector_id = $selector_result['id'];
        $selector_mode = $selector_result['mode'];
        
        $monetary_symbols = array();
        if (isset($report_results['location_currencies'])) {
            $monetary_symbols = $report_results['location_currencies'];
            unset($report_results['location_currencies']);
        }

        $graphable_cols = array();
        $table_str = '';
        $graph_str = '';
        $table_start = '';
        $title_str = ''; // $this->area_reports->report_title;
        $content_str = '';
        $totalTime = '';
        $time_count = 0;
        $time_values = array();
        $order_ids_arr = array();
        $recentChangeBankTime='';
        
        $orderby_var = "orderby_" . $selector_id;
        $orderby = $this->area_reports->orderby;

        $sister_select = substr($_GET['sister_select'], 1);
        $flag = 0;
        if ("cash_sales" == $sister_select) {
            $flag = 1;
        }

        if ($table_start) {
            $table_start .= '<br /><br />';
        }
        // based on date format setting
        $date_setting = "date_format";
        $location_date_format_query = lavu_query("SELECT `location`, `setting`, `value` FROM `config` WHERE `setting` = '[1]'", $date_setting);

        if (! $location_date_format_query || ! mysqli_num_rows($location_date_format_query)) {
            $date_format = array(
                "value" => 2
            );
        } else {
            $date_format = mysqli_fetch_assoc($location_date_format_query);
        }
     
        $counter = count($report_results);
        for ($m = 0; $m < $counter; $m ++) {
            if (array_key_exists("location_names", $report_results)) {
                $m = key($report_results_loc);
                $counter = key(array_slice($report_results_loc, - 1, 1, TRUE)) + 1;
                if ($m === NULL)
                    continue;
                next($report_results_loc);
            }

            $totals = array();
            $total_types = array();
            $monitary_symbol = "";
            $sumofitems = array();
            if (isset($monetary_symbols[$m])) {
                $monitary_symbol = $monetary_symbols[$m];
            }
            $report_location = '';
            if (count($report_results) > 1) {
                $report_location .= $report_results['location_names'][$m];
                $table_start .= '<div style="display: table; margin: 0 auto; font-size: 14px;"><b>' .$report_results['location_names'][$m] . '</b></div>';
            }
            
            if ($this->area_reports->report_name == TYPE_CHANGE_BANK_REPORT) {
                $table_start .="<table><tr><td colspan=''><b>Final amount in Change Bank</b></td></tr>";
                $table_start .="<tr><td><b>Total Variance over day</b></td></tr></table>";
            }
            $locationOrDataname = empty($report_location) ? trim(sessvar("admin_dataname")) : trim($report_location);
            $isPrintable = (int) reqvar('printable', 0) === 1;
			$isOutputModeSet = reqvar('output_mode', '') === 'csv';
            
            if ($this->area_reports->report_name === TYPE_LAVU_GIFT_AND_LOYALITY && !$isPrintable && !$isOutputModeSet) {
                $isMultiReport = isset($_GET['multi_report']) ? 'true' : 'false';
                $startDate = explode(" ", $selector_result['vars']['start_datetime'])[0];
                $endDate = explode(" ", $selector_result['vars']['end_datetime'])[0];
                $startTime = date("H:i:s", strtotime($startDate));
                $endTime = date("H:i:s", strtotime($endDate)) == "00:00:00" ? "24:00:00" : date("H:i:s", strtotime($endDate));
                $startDateTime = $startDate." ".$startTime;
                $endDateTime = $endDate." ".$endTime;
                $pageLink = $this->area_reports->paginationQuery($startDateTime, $endDateTime, $locationOrDataname, sessvar("admin_dataname"), $isMultiReport);
                $table_start .="<table data-pagination-for='".$locationOrDataname."' cellspacing=\"0\" cellpadding=\"4\" style=\"margin: 0 auto;\"><tbody class='pagination-table'><tr><td align='center'>".$pageLink."</td></tr></tbody></table>";
            }

            $table_start .= "\n<table data-location='".$locationOrDataname."' data-mode='".$selector_mode."' class=\"report_table\" cellspacing=\"0\" cellpadding=\"4\" style=\"margin: 0 auto; \">\n";
            
            $change_count=count($report_results[$m]);
            
            if (count($report_results[$m]) >= 1) {
                if ($this->area_reports->report_name == TYPE_CHANGE_BANK_REPORT) {
                    $firstChangeEntry = explode('[money]', $report_results[$m][0][0]['change_bank_amount']);
                    $lastChangeEntry= explode('[money]', $report_results[$m][($change_count - 1)][0]['change_bank_amount']);
                }

                for ($n = 0; $n < $change_count; $n ++) {

                    $report_read = $report_results[$m][$n];

                    //Modify Action Log result
                    if ($this->area_reports->report_id == 35) {
                    	$report_read = $this->area_reports->modifyActionLogReports($report_read);
                    }

                    if ($title_str == "") {
                        if (! isset($row_title)) {
                            $row_title = '';
                        }
                        if ($row_title != "") {
                            $title_str .= "<tr>\n";
                            $title_str .= "<td colspan='" . count($report_read) . "' bgcolor='#eeeeee' align='left'>&nbsp;&nbsp;$row_title</td>\n";
                            /*
                             * if($row_title_next!="")
                             * {
                             * $content_str .= "<td style='border-bottom:solid 1px black; color:#777777'>$row_title_next</td>";
                             * }
                             */
                            // else $content_str .= "<td style='border-bottom:solid 1px black;'>&nbsp;</td>";
                            $title_str .= "</tr>\n";
                        }
                        
                        $title_str .= "<thead>\n<tr>\n";
                        // Create header for titles
        foreach ($report_read as $keys => $rowValues)
        {
            // Create header for titles
            foreach ($rowValues as $key => $val)
            {
                if ($this->should_show_col($key))
                {
                    $nval = report_money_to_number($val, $monitary_symbol); // str_replace(",","",str_replace("$","",$val)); // add up if it is numeric
                    $title_style = "border-bottom:solid 1px black; cursor:pointer;";
                    $set_orderby = $key;
                    if ($orderby == $key || $orderby == $key . " asc")
                    {
                       $title_style .= " font-weight:bold;";
                       $set_orderby .= " desc";
                     } else if ($orderby == $key . " desc")
                     {
                        $title_style .= " font-weight:bold;";
                     }
                       $class = '';
                       if ((report_value_is_number($val) || substr($val, 0, 7) == "[money]") && strpos($key, "_id") === false && strpos($key, "zip") === false && strpos($key, "postal") === false) {
                       $class .= 'alignRight';
                        } else {
                        $class .= 'alignLeft';
                        }
                        if($key == "Date (YYMMDD)" || $key == "Date (AAMMDD)"){
                        	$key = $this->displayKeyDateFormat($key);
                        }
                        $onClickSort = '';
                        if ($this->area_reports->report_name === TYPE_LAVU_GIFT_AND_LOYALITY && $reporting_groups['multi_report'] != '0' && $reporting_groups['seperate_locations'] != '0') {
                            $onClickSort = 'sortTable("'.$locationOrDataname.'", "'.$key.'")';
                        } else {
                            $queryString = $this->area_reports->innerurl("mode={$selector_mode}&{$orderby_var}={$set_orderby}");
                            if ($this->area_reports->report_name === TYPE_LAVU_GIFT_AND_LOYALITY) {
                                $page = reqvar('page', 1);
                                $queryString .= "&page=".$page;
                            }
                            $onClickSort = "click_to_page(\"".$queryString."\")";
                        }
                        
                        if ($flag != 1) {
                            $title_str .= "<th class='{$class}' valign='bottom' style='{$title_style}' onclick='".$onClickSort."'>";
                            if ($key == "order_time") {
                                $key = "order_id";
                                $title_str .= "<nobr>" . show_as_title($key) . '</nobr>';
                            } else {
                                $title_str .= "<nobr>" . speak(show_as_title($key), FALSE) . '</nobr>';
                            }
                            $title_str .= "</th>\n";
                       }
                        if ($this->area_reports->firstcol == "") {
                               $this->area_reports->firstcol = $key;
                           }
                        if ($this->area_reports->firstcol_name == "" && strpos(show_as_title($key), "Name") !== false) {
                               $this->area_reports->firstcol_name = $key;
                            }
                        }
                    }
               }

                if ($this->area_reports->report_name === TYPE_LAVU_GIFT_AND_LOYALITY) {
                    $name = 'name="'.$locationOrDataname.'"';
                    $title_str .= "</tr>\n</thead>\n<tbody data-location='".$locationOrDataname."' ".$name." class=\"report_body make-scrollable-report\" cellspacing=\"0\" cellpadding=\"4\" style=\"margin: 0 auto;\">\n";
                } else {
                    $title_str .= "</tr>\n</thead>\n<tbody>\n";
                }
           }
                    $alt = '';
                    if ($n % 2) {
                        $alt = ' alt';
                    }
                    $content_str .= "<tr{$alt}>\n";
                    $key_it = 0;
                    if (! is_array($report_read)) {
                        // echo '['.__FILE__.':'.__LINE__.']report_read: ' . var_dump( $report_read, true );
                    } else {
                        foreach ($report_read as $keys => $rowValues) {
                            foreach ($rowValues as $key => $val) {
                                if ($this->should_show_col($key)) {
                                    $recolor = '';
                                    $nval = $val;
                                    if (report_value_is_number($val) && strpos($key, "_id") === false && strpos($key, "zip") === false && strpos($key, "postal") === false && strpos(strtolower($key), "invoice") === false && strpos(strtolower($key), "date") === false && strpos(strtolower($key), "z report id") === false && strpos(strtolower($key), "card_last_4_digits") === false) {
                                        $recolor = ' recolor="' . $key . '" entry="' . $n . '" class="alignRight"';
                                        $total_type = "numeric";
                                        $nval = report_number_to_float($val) * 1;
                                    } else if (report_value_is_date($val)) {
                                        $recolor = ' recolor="' . $key . '" entry="' . $n . '" class="alignLeft"';
                                        $total_type = "date";
                                        $nval = report_date_to_number($val);
                                        if ($key == 'Order ID' && in_array($location_info['business_country'], array('Israel'))) {
                                            $recolor .=  ' onclick="show_order_id(&quot;' . $nval . '&quot;, event)"';
                                        }
                                        if ($key == 'order_id') {
                                            $order_ids_arr[] = $nval;
                                        }
                                    } else if (report_value_is_money($val, $monitary_symbol) && strpos($key, "net_sales") === false) {
                                        $recolor = ' recolor="' . $key . '" entry="' . $n . '" class="alignRight"';
                                        $total_type = "money";
                                        $nval = report_money_to_number($val, $monitary_symbol) * 1;
                                    } else {
                                        $total_type = "string";
                                        $recolor = 'class="alignLeft"';
                                    }

                                    if (! isset($total_types[$key]) || $total_types[$key] != "string") {
                                        $total_types[$key] = $total_type;
                                    }

                                    if ($total_types[$key] == "numeric" || $total_types[$key] == "money") {
                                        if ($nval == 0) {
                                            $nval = abs($nval);
                                        }
                                        $sumofitems[$key] += $nval;
                                        if (! isset($totals[$key])) {
                                            $totals[$key] = 0;
                                        }

                                        if (strpos($report_read[$key], '[number]') === false && strpos($report_read[$key], '[money]') === false) {
                                            global $location_info;
                                            $decimal_char = isset($location_info['decimal_char']) ? trim($location_info['decimal_char']) : '.';
                                            if (strpos($report_read[$key], ".") === false && $decimal_char == ",") {
                                                // LP-1511 -- Only setting monetary symbol if we haven't already set it with a location-specific value
                                                // to keep "SEPARATE (LOCAL CURRENCY)" view with reporting groups from having blank totals for locations
                                                // that have a different monetary symbol than the dataname being viewed in CP. However, this line was
                                                // needed for the "SEPARATE LOCATIONS" view to keep from blank totals rows being reported in that case.
                                                if (empty($monitary_symbol))
                                                    $monitary_symbol = isset($location_info['monitary_symbol']) ? trim($location_info['monitary_symbol']) : '';
                                            }
                                        }
	                                    if ( !in_array($key, array("PV_NO", "cash_drawer","class id" ) ) ) {
                                            $totals[$key] += $nval;
                                        }
                                        if ($flag != 1) {
                                            if (! isset($graphable_cols[$key])) {
                                                $graphable_cols[$key] = array(
                                                    "min" => INF,
                                                    "max" => - INF
                                                );
                                            }

                                            if ($graphable_cols[$key]['max'] < $nval) {
                                                $graphable_cols[$key]['max'] = $nval;
                                            }

                                            if ($graphable_cols[$key]['min'] > $nval) {
                                                $graphable_cols[$key]['min'] = $nval;
                                            }
                                        }
                                    }
                                    if ($total_types[$key] == "date") {
                                        if ($key == "time_elapsed") {
                                            sscanf($nval, "%d:%d:%d", $hours, $minutes, $seconds);
                                            $time_seconds = $hours * 3600 + $minutes * 60 + $seconds;
                                            $time_count ++;
                                            $totalTime += $time_seconds;
                                            $totals[$key] = $totalTime;
                                            $time_values[] = $time_seconds;
                                        } else {
                                            $totals[$key] = " ";
                                        }
                                    }
                                    $supress = '';
                                    if ($this->area_reports->is_field_currently_drilled($key, $key_it)) {
                                        $supress = ' supress';
                                    }
                                    if ($key_it % 2) {
                                        $supress .= ' alt';
                                    }
                                    $nowrap = '';
                                    if ($val != "" && ($key == "order_closed_time" || $key == "opened" || $key == "closed" || $key == "time" || $key == "schedule" || $key == "date_time" || $key  == "Order Date")) {
                                          			$val = $this->displayDateFormat($key, $val);
                                        $recentChangeBankTime=$this->area_reports->show_reported_value($val,$report_read,$key,$monitary_symbol);
                                    }
                                    if ($val != "" && ($key == "actual_date_and_hour" || $key == "order_closed_actual_date_and_hour" || $key == "item_added_actual_date_and_hour"
                                    		|| $key == "Date" || $key == "Date Requested" || $key == "Date Received"
                                    		|| $key == "fiscal_date" || $key == "commercial_date" || $key == "Date (DDMMYY)" || $key == "Date (MMDDYY)" || $key == "Date (YYMMDD)"
                                    		|| $key == "Date (DDMMAA)" || $key == "Date (MMDDAA)" || $key == "Date (AAMMDD)"
						|| $key == "order_closed_day" || $key == "item_added_day" || $key == "day" || $key == "date")) {
                                    			$val = $this->displayDateFormat($key, $val);
                                    }
                                    $display_val = $this->area_reports->show_reported_value($val, $report_read, $key, $monitary_symbol);
                                    $display_val .= "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp";
                                    if (strlen($display_val) < 7 || strpos($display_val, ' ') === FALSE) {
                                        $nowrap = ' nowrap';
                                    }
                                    if (isset($_REQUEST['sharingType']) && $_REQUEST['sharingType']!="") {
                                        if ($sharingType == 'EmployeeClass') {
                                            if ($key_it == 1) {
                                                $classid = trim($display_val);
                                            }
                                        }
                                    }
                                    if ($flag != 1) {
                                        $content_str .= "<td valign='top'{$recolor}{$supress}{$nowrap}>";
                                        if ($key_it == 0) {
                                            if (isset($_REQUEST['sharingType']) && $_REQUEST['sharingType']!="") {
                                                $sharingType = $_REQUEST['sharingType'];
                                            }
                                            if ($sharingType == 'EmployeeClass') {
                                            	if (isset($_REQUEST['printable']) && $_REQUEST['printable'] == 1) {
                                            		$content_str .= "<span style='color: blue;font-size: 12px;font-weight: 400;'>".$display_val."</span>";
                                            	} else {
                                                	$content_str .= "<a class='toggle' href='javascript:void(0);'>".$display_val."</a>";
                                            	}
                                            } else {
                                                $content_str .= $display_val;
                                            }
                                        } else {
                                            $content_str .= $display_val;
                                        }
                                        $content_str .= "</td>\n";
                                    }
                                }
                                $key_it ++;
                            }
                        }
                    }
                    $content_str .= "</tr>";
                    if (isset($_REQUEST['sharingType']) && $_REQUEST['sharingType']!="") {
                        if ($sharingType == 'EmployeeClass') {
                            $selector_result = $this->area_reports->datetime_selector();
                            $startDate = explode(" ", $selector_result['vars']['start_datetime'])[0];
                            $endDate = explode(" ", $selector_result['vars']['end_datetime'])[0];
                            $decimalQuer = lavu_query("select `decimal_char`,`monitary_symbol`,`disable_decimal` from `locations` where `id` = '".sessvar("locationid")."' ");
                            $decimalData = mysqli_fetch_assoc($decimalQuer);
                            $decimalPoint = $decimalData['disable_decimal'];
                            $monitarySymbol = $decimalData['monitary_symbol'];
                            $decimalChar = $decimalData['decimal_char'];

                            $firstDate = date_create($startDate);
                            $secondDate = date_create($endDate);
                            $interval = date_diff($firstDate, $secondDate);
                            $dayCount = $interval->format('%a');
                            $sharingShowType = 'ShowTotals';
                            if($dayCount < 2) {
                            	$sharingShowType = 'ShowAll';
                            }

                            if (isset($_REQUEST['sharingShowType']) && $_REQUEST['sharingShowType']!="") {
                             	$sharingShowType = $_REQUEST['sharingShowType'];
                            }
                            $getDate = '';
                            $groupBy = " GROUP BY `server_name`";
                            if($sharingShowType == 'ShowAll') {
                            	$getDate = " `created_date`,";
                            	$groupBy = " GROUP BY `id`";
                            }
                            $query_users ="select `id`, `server_name`, `emp_class_name`,`emp_class_id`,".$getDate." SUM(`hours_worked`) AS `hours_worked`, SUM(`sales`) AS `sales`, SUM(`card_tip`) AS `card_tip`, SUM(`cash_tip`) AS `cash_tip`, SUM(`total_tip`) AS `total_tip`, SUM(`tip_out`) AS `tip_out`, SUM(`tip_in`) AS `tip_in`, SUM(`tip_due`) AS `tip_due`, SUM(`tip_pool`) AS `tip_pool` from `tip_sharing` where emp_class_id = '".$classid."' AND `created_date` >= '".$startDate."' AND `created_date` < '".$endDate."' $groupBy";

                            $report_query = lavu_query($query_users);
                            if (mysqli_num_rows($report_query) > 0) {
                                $dateValue = '';
                            	while ($report_read = mysqli_fetch_assoc($report_query)) {
                                	if (isset($report_read['created_date'])) {
	                            		$created_date = $report_read['created_date'];
	                                	$dtVal = $this->returnDateFormat($created_date, '-');
	                                	$dateValue = '<td>'.$dtVal.'</td>';
                                	}
                                    $name = $report_read['server_name'];
                                    $emp_class_name = $report_read['emp_class_name'];
                                    $emp_class_id = $report_read['emp_class_id'];
                                    $Hours = $report_read['hours_worked'];
                                    $Hours = str_replace(".", ":", number_format($Hours,2));
                                    $monitoryPos= $location_info['left_or_right'];
                                    $monitoryPosLeft = ($monitoryPos == 'left') ? $monitarySymbol : '';
                                    $monitoryPosRight = ($monitoryPos == 'right') ? $monitarySymbol : '';
                                    $sales = report_money_output($report_read['sales'], $monitarySymbol);
                                    $card_tips = report_money_output($report_read['card_tip'], $monitarySymbol);
                                    $cash_tips = report_money_output($report_read['cash_tip'], $monitarySymbol);
                                    $total_tips = report_money_output($report_read['total_tip'], $monitarySymbol);
                                    $tip_out = report_money_output($report_read['tip_out'], $monitarySymbol);
                                    $tip_in = report_money_output($report_read['tip_in'], $monitarySymbol);
                                    $tip_due = report_money_output($report_read['tip_due'], $monitarySymbol);
                                    $tip_pool = report_money_output($report_read['tip_pool'], $monitarySymbol);
                                    
                                    $content_str .= "<tr style='display:none;' class='show'><td>".$name."</td><td style='text-align: center;'>".$emp_class_id."</td>".$dateValue."<td style='text-align: left;padding-left: 2px;'>".$Hours."</td><td>".$sales."</td><td style='text-align: left;padding-left: 14px;'>".$card_tips."</td><td style='text-align: left;padding-left: 14px;'>".$cash_tips."</td><td style='text-align: left;padding-left: 18px;'>".$total_tips."</td><td>".$tip_out."</td><td>".$tip_in."</td><td style='text-align: center;padding-left: 58px;'>".$tip_due."</td><td style='text-align: center;padding-left: 46px;'>".$tip_pool."</td></tr>\n";
                                }
                            }
                        }
                    }
                }
            } else {
                if (isset($report_results['location_names'][$m])) {
                    $content_str .= "<tr><td style='border:solid 1px black' width='320' height='200' align='center' valign='middle'>";
                    $content_str .= speak("No Records Found");
                    $content_str .= "</td></tr>";
                }
            }
            $totals_str = "";
            $totals_count = 0;
            if ($this->area_reports->report_name!='tip_sharing') {
                $totals_str .= "</tbody>\n<tfoot>\n<tr>\n";
                $orders_count = array_count_values($order_ids_arr);
                foreach ($total_types as $key => $val)
                {
                $textAlign = '';
                if ($val == "numeric" || $val == "money") {
                    $textAlign = ' text-align: right;';
                }
                if ($flag != 1) {
                    $totals_str .= "<td style='border-top:solid 1px black;{$textAlign}'>\n";
                }
                if ($val == "string") {
                    $totals_str .= "&nbsp;";
                } else {
                    $totals_count ++;
                    if ($val == "numeric") {
                        if ($key == "average") {

                            $totals_str .= report_number_output($totals[$key] / count($report_results[$m]));
                        } else {
                            if ($flag != 1) {
                                $totals_str .= report_number_output($totals[$key]);
                            }
                        }
                    } else if ($val == "date") {
                        if ($key == 'time_elapsed') {
                            $hours = floor($totals[$key] / 3600);
                            if ($hours < 10) {
                                $hours = sprintf("%02d", $hours);
                            }
                            $minutes = floor(($totals[$key] / 60) % 60);
                            if ($minutes < 10) {
                                $minutes = sprintf("%02d", $minutes);
                            }
                            $seconds = $totals[$key] % 60;
                            if ($seconds < 10) {
                                $seconds = sprintf("%02d", $seconds);
                            }
                            $totals_str .= "$hours:$minutes:$seconds";
                        }
                        /* if($this->area_reports->report_name == "changeBankReport" && $key == 'date_time'){
                         $totals_str .= "Final amount entered in Change Bank: ";
                         } */
                        if($this->area_reports->report_name == TYPE_DEPOSIT_BANK_REPORT && $key == 'date_time') {
                            $totals_str .= "Final amount deposited into Bank: ";
                        }
                    } else if ($val == "money") {
                        if($this->area_reports->report_name == TYPE_DEPOSIT_BANK_REPORT){
                            $diffLavuCalcAmtAndTotEnteredAmt[] = $totals[$key];
                        }
                        
                        if($this->area_reports->report_name != TYPE_CHANGE_BANK_REPORT && $key != 'change_bank_amount') {
                            $totals_str .= report_money_output($totals[$key], $monitary_symbol);//"$" . number_format($totals[$key],2);
                        }
                    }
                }
                $totals_str .= "</td>\n";
           }
            $totals_str .= "</tr>\n</tfoot>\n";
            } else {
                $totals_count = 0;
                $totals_str .= "</tbody>\n<tfoot>\n<tr>\n";
                $orders_count = array_count_values($order_ids_arr);
                foreach ($total_types as $key => $val)
                {
                    if ($flag != 1) {
                        $totals_str .= "<td style='border-top:solid 1px black;'>\n";
                    }
                        $totals_count ++;
                        $totals_str .= "</td>\n";
                }
                $totals_str .= "</tr>\n</tfoot>\n";
            }
            
            if($this->area_reports->report_name == TYPE_CHANGE_BANK_REPORT){//LP-4186
                $lastChangeBankAmount=$lastChangeEntry;
                $changeBankAmount=report_money_output($lastChangeBankAmount[1], $monitary_symbol);
                $changeBankInfo="Final Amount in Change Bank (".$recentChangeBankTime.") : ".$changeBankAmount;
                $table_start =str_replace("Final amount in Change Bank",$changeBankInfo,$table_start);
                
                $changeBankVariance="Total Variance over day (+/-):  ".report_money_output(($lastChangeEntry[1] - $firstChangeEntry[1]));
                $table_start =str_replace("Total Variance over day",$changeBankVariance,$table_start);
            }
            
            $report_name = $this->area_reports->get_report_info_from_getvar("name");
            if ($totals_count > 0 && strtolower($report_name) != "general_ledger") {
                $table_end .= $totals_str;
            }

            $table_end .= "</table><br /><br />";
            $table_str .= $table_start;
            $table_str .= $title_str;
            $table_str .= $content_str;
            $table_str .= $table_end;

            $table_start = '';
            $title_str = '';
            $content_str = '';
            $table_end = '';
        }
        if ($flag == 1) {
            $selector_result = $this->area_reports->datetime_selector();
            $mode = getvar("mode", "");
            $totalsales_cardtips = "";
            // lp-1062 change for report_v2 for caash sale START
            // lp-2507
            $title_style = "border-bottom:solid 1px black; cursor:pointer;";
            $title_style .= " font-weight:bold;";
            $class = 'alignLeft';
            $table_start .= "\n<table class=\"report_table\" cellspacing=\"0\" cellpadding=\"4\" style=\"margin: 0 auto;\">\n";
            $title_str .= "<thead>\n<tr>\n";
            $title_str .= "<th class='{$class}' valign='bottom' style='{$title_style}'>";
            $title_str .= "<nobr'> Date </nobr>";
            $title_str .= "</th>\n";
            $title_str .= "<th class='{$class}' valign='bottom' style='{$title_style}'>";
            $title_str .= "<nobr> Cash Sales </nobr>";
            $title_str .= "</th>\n";

            $title_str .= "</tr>\n</thead>\n<tbody>\n";
            $table_str .= $table_start;
            $table_str .= $title_str;
            $sales_query = lavu_query("SELECT date(datetime)as saleDate,SUM(total_collected) as sales_total_collected FROM cc_transactions WHERE `action` = 'Sale' and pay_type = 'Cash' and voided = 0 and `datetime` >= '" . explode(" ", $selector_result['vars']['start_datetime'])[0] . "' and `datetime` < '" . explode(" ", $selector_result['vars']['end_datetime'])[0] . "' group by date(datetime)");
            $rowsCount = mysqli_num_rows($sales_query);
            if ($rowsCount > 0) {
                while ($sales_total_collected = mysqli_fetch_assoc($sales_query)) {
                    $saleDate = $sales_total_collected['saleDate'];
                    $card_tip_amount = array();
                    $refund_query = lavu_query("SELECT SUM(total_collected) as refund_total_collected FROM cc_transactions WHERE `action` = 'Refund' and pay_type = 'Cash' and voided = 0 and date(`datetime`) >= '" . $saleDate . "' and date(`datetime`) <= '" . $saleDate . "' group by date(datetime)");
                    $refund_total_collected = mysqli_fetch_assoc($refund_query);
                    if ($sales_total_collected['sales_total_collected'] > $refund_total_collected['refund_total_collected']) {
                        $totalsales_totalrefund = $sales_total_collected['sales_total_collected'] - $refund_total_collected['refund_total_collected'];
                    } else {
                        $totalsales_totalrefund = $refund_total_collected['refund_total_collected'] - $sales_total_collected['sales_total_collected'];
                    }
                    $cardtips_query = lavu_query("SELECT SUM(tip_amount) as tip_amount FROM cc_transactions WHERE `action` = 'Sale' and pay_type = 'Card' and voided = 0 and date(`datetime`) >= '" . $saleDate . "' and date(`datetime`) <= '" . $saleDate . "' group by date(datetime) ");
                    $card_tip_amount = mysqli_fetch_assoc($cardtips_query);
                    $autogratuity_query = lavu_query("SELECT SUM(o.gratuity) as auto_gratuity FROM orders o JOIN cc_transactions cc on o.order_id = cc.order_id WHERE cc.action = 'Sale' and cc.voided = 0 and o.void = 0 and date(cc.datetime) >= '" . $saleDate . "' and date(cc.datetime) <= '" . $saleDate . "' group by date(cc.datetime)");
                    $auto_gratuity = mysqli_fetch_assoc($autogratuity_query);
                    $totalsales_cardtips = ($totalsales_totalrefund) - ($card_tip_amount['tip_amount']);
                    $totalsales_cardtips = $totalsales_cardtips - ($auto_gratuity['auto_gratuity']);
                    $change_format = explode("-", $saleDate);
                    switch ($date_format['value']) {
                        case 1:
                            $saleDate = $change_format[2] . "-" . $change_format[1] . "-" . $change_format[0];
                            break;
                        case 2:
                            $saleDate = $change_format[1] . "-" . $change_format[2] . "-" . $change_format[0];
                            break;
                        case 3:
                            $saleDate = $change_format[0] . "-" . $change_format[1] . "-" . $change_format[2];
                            break;
                        default:
                            $saleDate = $change_format[1] . "-" . $change_format[2] . "-" . $change_format[0];
                    }
                    // lp-1062 change for report_v2 for cash sale end
                    $content_str = "<tr>\n";
                    $content_str .= "<td valign='top'>";
                    $content_str .= "<b>" . $saleDate . "</b>";
                    $content_str .= "</td>\n";
                    $content_str .= "<td valign='top'>";
                    $content_str .= "<b>" . report_money_output($totalsales_cardtips, $monitary_symbol) . "</b>";
                    $content_str .= "</td>\n";
                    $content_str .= "</tr>\n";
                    $table_str .= $content_str;
                }
            } else {
                $content_str .= "<tr>\n";
                $content_str .= "<td valign='top'>";
                $content_str .= "No Record Found";
                $content_str .= "</td>";
                $content_str .= "</tr>\n";
                $table_str .= $content_str;
            }
            $table_end .= "</table><br /><br />";
            $table_str .= $table_end;
            // lp-2507 end
        }
        $showGraph = ! ($reporting_groups && $reporting_groups['multi_report'] != '0' && $reporting_groups['seperate_locations'] != '0');
        // echo '<pre style="text-align: left;">' . print_r( $graphable_cols, true ) . '</pre>';
        // $check_colname = "";
        // Graph columns that are numeric or dollars (they exist in the array graphable_cols)
        if (count($graphable_cols) > 0 && $showGraph) {
            $allow_graphing = true;

            $is_fiscal_report = false;
            $report_name = $this->area_reports->get_report_info_from_getvar("name");
            if (strpos(strtolower($report_name), "fiscal") !== false)
                $is_fiscal_report = true;

            if ($is_fiscal_report || strtolower($report_name)== "general_ledger"){
                $allow_graphing = false;
            }
            
            if ($allow_graphing) {
                $graphlist = array();
                $graphcolors = array(
                    "#5599cc",
                    "#55aa55",
                    "#55aaaa"
                );
                $graphcolor_pos = 0;

            foreach ($graphable_cols as $key => $maxval) {
                    $graphlist[$key] = array(
                        "output" => "",
                        "data" => array(
                            'labels' => array(),
                            'datasets' => array(
                                array(
                                    'data' => array()
                                )
                            )
                        )
                    );
              }
                for ($n = 0; $n < count($report_results[0]); $n ++) {
                    $report_read = $report_results[0][$n];
                    foreach ($report_read as $key => $val) {
                        if ($val != "" && ($key == "actual_date_and_hour" || $key == "order_closed_actual_date_and_hour" || $key == "item_added_actual_date_and_hour")) {
                        	$report_read["group_col_date_formatted"] = $this->displayDateFormat($key, $val);
                        }
                        if ($val != "" && $key == "time") {
                        	$report_read["group_col_action_log_time"] = $this->displayDateFormat($key, $val);
                        }
                    }
                    foreach ($graphable_cols as $key => $minmaxval) {
                        $minval = $minmaxval['min'];
                        $maxval = $minmaxval['max'];

                        if (isset($graphlist[$key]['color'])) {
                            $graph_color = $graphlist[$key]['color'];
                        } else {
                            $graph_color = $graphcolors[$graphcolor_pos];
                            $graphlist[$key]['color'] = $graph_color;
                            $graphcolor_pos ++;
                            if ($graphcolor_pos >= count($graphcolors))
                                $graphcolor_pos = 0;
                        }
                        if ($minval < 0) {
                            $step_size = 360 / ($maxval - $minval);
                            $step_baseline = floor(abs($step_size * $minval));
                            if ($step_baseline > 240) {
                                $step_baseline = 240;
                                $step_size = $step_baseline / abs($minval);
                            }
                        } else {
                            if ($maxval == 0)
                                $step_size = 100;
                            else
                                $step_size = 360 / $maxval;
                            $step_baseline = 2;
                        }
                        $graph_value = 0;

                        if (isset($total_types[$key]) && $total_types[$key] == "money") {

                            $graph_value = report_money_to_number($report_read[0][$key]);

                            global $location_info;

                            $decimal_char = isset($location_info['decimal_char']) ? trim($location_info['decimal_char']) : '.';

                            if (strpos($report_read[0][$key], ".") === false && $decimal_char == ",") {
                                $monitary_symbol = isset($location_info['monitary_symbol']) ? trim($location_info['monitary_symbol']) : '';
                                $graph_value = str_replace($monitary_symbol, '', $report_read[0][$key]);
                                $graph_value = preg_replace('/,([^,]*)$/', '.\1', $graph_value);
                                $graph_value = str_replace(',', '', $graph_value);
                            }

                            // str_replace(",","",str_replace("$","",$report_read[$key])) * 1;
                        } else {
                            $graph_value = report_number_to_float($report_read[0][$key]);
                        }

                        $setwidth = floor($graph_value * $step_size);
                        $inner_chars_allowed = abs($setwidth) / 7;

                        $inner_output = "&nbsp;";
                        $outer_output = "";
                        $use_firstcol = $this->area_reports->firstcol;
                        $graphKey = $use_firstcol;
                        if (isset($this->area_reports->fwd_drillvars['last_subreport']))
                            $use_firstcol = $this->area_reports->fwd_drillvars['last_subreport'];
                        if ($use_firstcol == "")
                            $use_firstcol = $this->area_reports->firstcol;

                        if ($use_firstcol != "") {
                            $graph_colname = $this->area_reports->show_reported_value($report_read[0][$use_firstcol], $report_read, $graphKey);
                            $graph_colname = explode(chr(30), $graph_colname);
                            $graph_colname = trim($graph_colname[0]);
                           
                            /*
                             * LP-5429
                             */
                            if(preg_match("/^([0-9]{4})-([0-9]{2})-([0-9]{2}) ([0-9]{2}):([0-9]{2}):([0-9]{2})$/", strip_tags(trim($graph_colname)))){
                            	$graph_colname = $this->displayDateFormat($key, strip_tags(trim($graph_colname)));
                            }
                            /*
                             *End of LP-5429 
                             */
                            
                            // START discount graph issue fixed
                            if (preg_match('/^[0-9 .]+$/', $graph_colname) || preg_match('/time.*/', trim($graph_colname))) {
                            	$graph_colname = $this->displayDateFormat($key, strip_tags(trim($graph_colname)));
                            }

                            // END discount graph issue fixed
                            if ($setwidth > 360 || $inner_chars_allowed > strlen($graph_colname)) { //LP-5320 $setwidth > 180 change to $setwidth > 360 due to alignment issue
                                $inner_output = "";
                                $inner_output .= "<nobr>";
                                if ($setwidth >= 0)
                                    $inner_output .= "&nbsp;";
                                $inner_output .= substr($graph_colname, 0, $inner_chars_allowed);
                                if ($setwidth < 0)
                                    $inner_output .= "&nbsp;";
                                $inner_output .= "</nobr>";
                            } else {
                                $outer_output = "&nbsp;&nbsp;<font style='font-size:10px'>" . $graph_colname . "</font>";
                            }

                            $graphlist[$key]['data']['labels'][] = str_replace('"', "\\\"", str_replace("'", '&#34;', $graph_colname));
                        }

                        if ($maxval <= 0) {
                            $width_pct = 0;
                        } else {
                            $width_pct = (($graph_value / $maxval) * 0.9) * 100;
                        }

                        $show_graph_value = '';
                        if (isset($total_types[$key]) && $total_types[$key] == "money") {
                            $show_graph_value = report_money_output($graph_value);
                        } else if (isset($total_types[$key]) && $total_types[$key] == "numeric") {
                            $show_graph_value = report_number_output($report_read[0][$key]);
                        }
                        /*
                         * else if( $orderinfotab['name'] == "orderinfo" )
                         * {
                         * $show_graph_value = $orders_count[$graph_colname];
                         * }
                         */
                        $graphlist[$key]['data']['datasets'][0]['data'][] = str_replace('"', '\"', str_replace("'", '&#34;', $graph_value));

                        $gstr = "";
                        $gstr .= "<tr><td align='left'>";
                        $gstr .= "<table cellspacing=0 cellpadding=0 style='width:500px'><tr>"; //LP-5320 changes, style='table-layout: fixed;' replace with style='width:500px'

                        if ($step_baseline > 0) {
                            if ($graph_value < 0) {
                                $gstr .= "<td recolor='{$key}' recolortype='bg' bgcolor='{$graph_color}' style='font-size:10px; height:12px; width:" . abs($setwidth) . "px; color:#ffffff' valign='middle' align='right'>{$inner_output}</td>";
                            } else {
                                $gstr .= "<td style='width:" . $step_baseline . "px; background-color:#eeeeee' align='right'>";
                                $gstr .= "&nbsp;";
                                $gstr .= "</td>";
                            }
                        }
                        if ($setwidth >= 0) {
                            $gstr .= "<td recolor='{$key}' recolortype='bg' bgcolor='{$graph_color}' style='font-size:10px; height:12px; width:{$setwidth}px; color:#ffffff' valign='middle' align='left'>{$inner_output}</td>";
                        }
                        $gstr .= "<td style='width: 5px;'>&nbsp;</td><td recolor='{$key}' recolortype='color' style='color:$graph_color'><b>{$show_graph_value}</b>" . $outer_output . "</td>";
                        $gstr .= "</tr></table>";

                        $graphlist[$key]['output'] .= $gstr;
                    }
                }
                foreach ($graphlist as $key => $val) {
                    $val_output = $val['output'];
                    if (trim($val_output) != "") {
                        $graph_str .= "&nbsp;<br>";
                        $graph_str .= "<table cellspacing=0 cellpadding=2 style='table-layout: fixed;'>";
                        $graph_str .= "<tr><td class='graph_title' style='color:#777777; font-weight:bold; width:440px' align='left'>" . speak(str_replace("_", " ", $key)) . "</td></tr>";
                        $graph_str .= $val_output;
                        $graph_str .= "</table>";
                    }
                }
                if ($graph_str != "") {
                    $graph_str = "<table><tr><td align='left'>$graph_str</td></tr></table>";
                }
            }
        }
        // $test_start_time = microtime(true);
        // $test_end_time = microtime(true);
        // $elapsed = number_format(($test_end_time - $test_start_time),2);
        // echo "ELAPSED: $elapsed<br>";

        if (isset($_GET['printable']) && $_GET['printable'] == "1") {
            return $this->area_reports->show_report_description() . "<br>" . $table_str;
        }
        if ($_REQUEST['sister_select'] == "#order_time") {
            $report_tab = "alt_orderinfo";
            $order_info_tab = mlavu_query("SELECT `name` FROM `poslavu_MAIN_db`.`reports_v2` WHERE `name` = '[1]'", $report_tab);
            $orderinfotab = mysqli_fetch_assoc($order_info_tab);

            $avgseconds = round($totalTime / $time_count, 2);
            $meanseconds = calculate_median($time_values);
            $avg_hours = round($avgseconds / 3600);
            if ($avg_hours < 10) {
                $avg_hours = sprintf("%02d", $avg_hours);
            }
            $avg_minutes = round(($avgseconds / 60) % 60);
            if ($avg_minutes < 10) {
                $avg_minutes = sprintf("%02d", $avg_minutes);
            }
            $avg_seconds = round($avgseconds % 60);
            if ($avg_seconds < 10) {
                $avg_seconds = sprintf("%02d", $avg_seconds);
            }

            $avgtime = "$avg_hours:$avg_minutes:$avg_seconds";
            $mean_hours = round($meanseconds / 3600);
            if ($mean_hours < 10) {
                $mean_hours = sprintf("%02d", $mean_hours);
            }
            $mean_minutes = round(($meanseconds / 60) % 60);
            if ($mean_minutes < 10) {
                $mean_minutes = sprintf("%02d", $mean_minutes);
            }
            $mean_seconds = round($meanseconds % 60);
            if ($mean_seconds < 10) {
                $mean_seconds = sprintf("%02d", $mean_seconds);
            }

            $meantime = "$mean_hours:$mean_minutes:$mean_seconds";
            $graph_str = "<br><br><br><br><br><br><br><br><br>";
            if ($orderinfotab['name'] == "alt_orderinfo") {
                $graph_str .= "<br><br><b style='color: rgb(165, 74, 124);'>Average Time Elapsed per Order: $avgtime</b>";
                $graph_str .= "<br><br><b style='color: rgb(165, 74, 124);'>Median Time Elapsed per Order: $meantime</b>";
            }
		}
		//exit;
		/* if($this->area_reports->report_name == "changeBankReport"){
			$graph_str .= "<br><br><b style='color: rgb(165, 74, 124);'>Total Variance over day (+/-):  ".report_money_output(($lastChangeEntry[1] - $firstChangeEntry[1]))."</b>";
		} */
		if($this->area_reports->report_name == TYPE_DEPOSIT_BANK_REPORT){
			$graph_str .= "<br><br><b style='color: rgb(165, 74, 124);'>Difference between the entered deposit and the Lavu calculated deposit (+/-):  ".report_money_output($diffLavuCalcAmtAndTotEnteredAmt[1] - $diffLavuCalcAmtAndTotEnteredAmt[0])."</b>";
        }
        return $this->finalize($table_str, $graph_str, @$graphlist);
    }

    public function draw_single_row($report_results)
    {
        $table_str = '';
        $table_start = '';
        $content_str = '';
        $row_title = ''; // $this->area_reports->report_title;
        $flag = 0;
        if ("cash_sales" == $sister_select) {
            $flag = 1;
            $selector_result = $this->area_reports->datetime_selector();
            $mode = getvar("mode", "");
            $totalsales_cardtips = "";
            $sales_query = lavu_query("SELECT SUM(total_collected) as sales_total_collected FROM cc_transactions WHERE `action` = 'Sale' and pay_type = 'Cash' and voided = 0 and `datetime` >= '" . explode(" ", $selector_result['vars']['start_datetime'])[0] . "' and `datetime` < '" . explode(" ", $selector_result['vars']['end_datetime'])[0] . "'");
            $sales_total_collected = mysqli_fetch_assoc($sales_query);
            $refund_query = lavu_query("SELECT SUM(total_collected) as refund_total_collected FROM cc_transactions WHERE `action` = 'Refund' and pay_type = 'Cash' and voided = 0 and `datetime` >= '" . explode(" ", $selector_result['vars']['start_datetime'])[0] . "' and `datetime` < '" . explode(" ", $selector_result['vars']['end_datetime'])[0] . "'");
            $refund_total_collected = mysqli_fetch_assoc($refund_query);
            if ($sales_total_collected['sales_total_collected'] > $refund_total_collected['refund_total_collected']) {
                $totalsales_totalrefund = $sales_total_collected['sales_total_collected'] - $refund_total_collected['refund_total_collected'];
            } else {
                $totalsales_totalrefund = $refund_total_collected['refund_total_collected'] - $sales_total_collected['sales_total_collected'];
            }
            $cardtips_query = lavu_query("SELECT SUM(tip_amount) as tip_amount FROM cc_transactions WHERE `action` = 'Sale' and pay_type = 'Card' and voided = 0 and `datetime` >= '" . explode(" ", $selector_result['vars']['start_datetime'])[0] . "' and `datetime` < '" . explode(" ", $selector_result['vars']['end_datetime'])[0] . "'");
            $card_tip_amount = mysqli_fetch_assoc($cardtips_query);
            $autogratuity_query = lavu_query("SELECT SUM(o.gratuity) as auto_gratuity FROM orders o JOIN cc_transactions cc on o.order_id = cc.order_id WHERE cc.action = 'Sale' and cc.voided = 0 and o.void = 0 and cc.datetime >= '" . explode(" ", $selector_result['vars']['start_datetime'])[0] . "' and cc.datetime < '" . explode(" ", $selector_result['vars']['end_datetime'])[0] . "'");

            $auto_gratuity = mysqli_fetch_assoc($autogratuity_query);
            $totalsales_cardtips = ($totalsales_totalrefund) - ($card_tip_amount['tip_amount']);
            $totalsales_cardtips = $totalsales_cardtips - ($auto_gratuity['auto_gratuity']);
        }

        if ($table_start) {
            $table_start .= '<br /><br />';
        }
        $table_start .= "<table cellspacing=0 cellpadding=2>";
        if ($row_title != "") {
            $content_str .= "<tr>";
            $content_str .= "<td colspan='2' style='border-bottom:solid 1px black;' align='left'><nobr>&nbsp;&nbsp;$row_title</nobr></td>";
            if ($row_title_next != "") {
                $content_str .= "<td style='border-bottom:solid 1px black; color:#777777'><nobr>$row_title_next</nobr></td>";
            } else
                $content_str .= "<td style='border-bottom:solid 1px black;'>&nbsp;</td>";
            $content_str .= "</tr>";
        }

        $report_read = $report_results[0][0];
        // echo '<pre style="text-align: left;">' . print_r( $report_read, true ) . '</pre>';
        if ($flag == 1) {
            foreach ($report_read as $key => $rowValues) {
                foreach ($rowValues as $key => $val) {
                    $timestamp_date = explode(" ", $val);
                    $date = explode("[date]", $timestamp_date[0]);
                    $report_read = array();
                    $report_read['Date'] = $date[1];
                    $report_read['Cash Sales'] = report_money_output($totalsales_cardtips, $monitary_symbol);
                    $report_read['Gratuity'] = $auto_gratuity['auto_gratuity'];
                }
            }
        }

        $report_read_next = (isset($_report_results_next[0])) ? $_report_results_next[0] : array();
        // based on date format setting
        $date_setting = "date_format";
        $location_date_format_query = lavu_query("SELECT `location`, `setting`, `value` FROM `config` WHERE `setting` = '[1]'", $date_setting);
        if (! $location_date_format_query || ! mysqli_num_rows($location_date_format_query)) {
            $date_option = array(
                "value" => 2
            );
        } else {
            $date_option = mysqli_fetch_assoc($location_date_format_query);
        }

	    //Modify Action Log result
	    if ($this->area_reports->report_id == 35) {
	        $report_read = $this->area_reports->modifyActionLogReports($report_read);
        }

        foreach ($report_read as $key => $rowValues) {
            foreach ($rowValues as $key => $val) {
				if ($val != "" && ($key == "actual_date_and_hour" || $key == "order_closed_actual_date_and_hour" || $key == "item_added_actual_date_and_hour" || $key == "Date" || $key == "Date Requested" || $key == "Date Received" || $key == "time" || $key == "order_closed_day" || $key == "item_added_day" || $key == "day" || $key == "schedule" || $key == "fiscal_date" || $key == "commercial_date" || $key == "Date (DDMMYY)" || $key == "Date (MMDDYY)" || $key == "Date (YYMMDD)" || $key == "Date (DDMMAA)" || $key == "Date (MMDDAA)" || $key == "Date (AAMMDD)" || $key == "opened" || $key == "closed" || $key  == "Order Date" )) {
               				$val = $this->displayDateFormat($key, $val);
            	}
                if ($this->should_show_col($key)) {
                	if($key == "Date (YYMMDD)" || $key == "Date (AAMMDD)"){
                		$key = $this->displayKeyDateFormat($key);
                	}
                    $content_str .= "<tr>";
                    $content_str .= "<td align='right' valign='top' width='100'><nobr>" . speak(show_as_title($key), FALSE) . "&nbsp;&nbsp;</nobr></td>";
                    if (in_array($key, array('order_id', 'kiosk_order', 'lavu_togo_order'))) {
                        $content_str .= '<td valign="top" width="150"><b><span style="cursor: pointer;" onclick="show_order_id(&quot;' . report_date_to_number($val) . '&quot;, event)">' . $this->area_reports->show_reported_value($val, $report_read, $key) . '</span></b></td>';
                    } else if ($key == 'udid') {
                        $content_str .= '<span style="cursor: pointer;" onclick="show_device(&quot;' . $display_val . '&quot;, event)">' . $display_val . '</span>';
                    } else {
                        $content_str .= "<td valign='top' width='150'><b>" . $this->area_reports->show_reported_value($val, $report_read, $key) . "</b></td>";
                    }
                    if (isset($report_read_next[$key])) { // There is an additional value to show (like for previous year)
                        $next_val = $report_read_next[$key];
                        $content_str .= "<td valign='top' style='color:#999999' width='150'>" . $this->area_reports->show_reported_value($next_val, $report_read, $key) . "</td>";

                        $compare_val = report_money_to_number($val); // str_replace("$","",str_replace(",","",$val));
                        $compare_next_val = report_money_to_number($next_val); // str_replace("$","",str_replace(",","",$next_val));

                        if (report_value_is_number($compare_val) && report_value_is_number($compare_next_val)) {
                            $diff = report_number_to_float($compare_val) - report_number_to_float($compare_next_val);
                            if ($compare_next_val != 0) {
                                $perc = number_format(($diff / report_number_to_float($compare_next_val) * 100), 2);
                                if ($perc > 0)
                                    $perc = "+" . $perc;
                                $content_str .= "<td width='100'>" . $perc . "%</td>";
                            } else {
                                $content_str .= "<td width='100'>&nbsp;</td>";
                            }
                            // $content_str .= "<td width='20%'>" . $diff . " / " . $compare_next_val . "</td>";
                        } else {
                            $content_str .= "<td width='100'>&nbsp;</td>";
                        }
                    } else {
                        $content_str .= "<td width='150'>&nbsp;</td><td width='100'>&nbsp;</td>";
                    }
                    $content_str .= "</tr>";

                    // firstCol wasn't being set on single result display, causing 'empty' drill down values
                    if ($this->area_reports->firstcol == "") {
                        $this->area_reports->firstcol = $key;
                    }

                    if ($this->area_reports->firstcol_name == "" && strpos(show_as_title($key), "Name") !== false) {
                        $this->area_reports->firstcol_name = $key;
                    }
                }
            }
        }
        $title_str = ''; // $this->area_reports->report_title;
        $table_end = "</table>";
        $table_str .= $table_start;
        $table_str .= $title_str;
        $table_str .= $content_str;
        $table_str .= $table_end;
        if (isset($_GET['printable']) && $_GET['printable'] == "1") {
            return $this->area_reports->show_report_description() . "<br>" . $table_str;
        }
        return $this->finalize($table_str, '', null);
    }

    public function draw_no_data()
    {
        $title_str = '';
        $table_str = '';
        $table_start = '';
        if ($table_start) {
            $table_start .= '<br /><br />';
        }
        $table_start .= "<table>";
        $content_str = '';
        $content_str .= "<tr><td style='border:solid 1px black' width='320' height='200' align='center' valign='middle'>";
        $content_str .= speak("No Records Found");
        $content_str .= "</td></tr>";
        $table_end = "</table>";
        $table_str = '';
        $table_str .= $table_start;
        $table_str .= $title_str;
        $table_str .= $content_str;
        $table_str .= $table_end;

        if (isset($_GET['printable']) && $_GET['printable'] == "1") {
            return $this->area_reports->show_report_description() . "<br>" . $table_str;
        }
        return $this->finalize($table_str, '', null);
    }

	public function draw_email_send_text() {
		$title_str = '';//$this->area_reports->report_title;
		$table_str = '';
		$table_start = '';
		if( $table_start ){
			$table_start .= '<br /><br />';
		}
		$table_start .= "<table>";
		$content_str='';
		$content_str .= "<tr><td style='border:solid 1px black;font-size:16px;color:black;' width='320' height='200' align='center' valign='middle'>";
		$content_str .= speak("The requested report exceeds the data return limit. You will be emailed a link to download this report.");
		$content_str .= "</td></tr>";
		$table_end = "</table>";
		$table_str = '';
		$table_str .= $table_start;
		$table_str .= $title_str;
		$table_str .= $content_str;
		$table_str .= $table_end;

		if(isset($_GET['printable']) && $_GET['printable']=="1")
		{
			return $this->area_reports->show_report_description() . "<br>" . $table_str;
		}
		return $this->finalize( $table_str, '', null );
	}

	public function displayDateFormat($format_date_key = "", $format_date_val = ""){
		$setting = $this->getDateFormatSetting();
		if ($format_date_key == "order_closed_time" || $format_date_key == "opened" || $format_date_key == "closed" || $format_date_key == "time" || $format_date_key == "schedule" 
				|| $format_date_key == "date_time" || preg_match('/^([0-9]{4})-([0-9]{2})-([0-9]{2}) ([0-9]{2}):([0-9]{2}):([0-9]{2})$/', $format_date_val) || $format_date_key  == "Order Date") {
			//format : [date]2017-05-17 07:38:50

			$split_date = explode(" ", $format_date_val);
			$format_date = $this->returnDateFormat($split_date[0], "-", $split_date[1]);
			return $format_date;
		}
		if ($format_date_key == "actual_date_and_hour" || $format_date_key == "order_closed_actual_date_and_hour" || $format_date_key == "item_added_actual_date_and_hour" 
				|| strpos($format_date_val, "[date]time") !== false) {
			//format : [date]time(Y-m-d H:i:s)2017-05-17 19:00:00

			$split_date_format = explode(")", $format_date_val);
			$change_date = explode(" ", $split_date_format[1]);
			$change_format = explode("-", $change_date[0]);
			$formatter = explode("(", $split_date_format[0]);
			$change_date_format = explode(" ", $formatter[1]);
			$split_date = explode("-", $change_date_format[0]);
			switch ($setting) {
				case 1:
					$reorder_date = trim($split_date[2]) . "-" . trim($split_date[1]) . "-" . trim($split_date[0]) . " " . $change_date_format[1];
					$format_date = $formatter[0] . "(" . $reorder_date . ")" . trim($change_format[2]) . "-" . trim($change_format[1]) . "-" . trim($change_format[0]) . " " . $change_date[1];
					break;
				case 3:
					$reorder_date = trim($split_date[0]) . "-" . trim($split_date[1]) . "-" . trim($split_date[2]) . " " . $change_date_format[1];
					$format_date = $formatter[0] . "(" . $reorder_date . ")" . trim($change_format[0]) . "-" . trim($change_format[1]) . "-" . trim($change_format[2]) . " " . $change_date[1];
					break;
				case 2:
				default:
					$reorder_date = trim($split_date[1]) . "-" . trim($split_date[2]) . "-" . trim($split_date[0]) . " " . $change_date_format[1];
					$format_date = $formatter[0] . "(" . $reorder_date . ")" . trim($change_format[1]) . "-" . trim($change_format[2]) . "-" . trim($change_format[0]) . " " . $change_date[1];
			}
			return $format_date;
		}
		if ($format_date_val != "" && ($format_date_key == "Date" || $format_date_key == "Date Requested" || $format_date_key == "Date Received")) {
			$split_date = explode(" ", $format_date_val);
			$change_format = explode("/", $split_date[0]);
			switch ($setting) {
				case 1:
					$format_date = trim($change_format[1]) . "/" . trim($change_format[0]) . "/" . trim($change_format[2]) . " " . $split_date[1];
					break;
				case 3:
					$format_date = trim($change_format[2]) . "/" . trim($change_format[0]) . "/" . trim($change_format[1]) . " " . $split_date[1];
					break;
				case 2:
				default:
					$format_date = trim($change_format[0]) . "/" . trim($change_format[1]) . "/" . trim($change_format[2]) . " " . $split_date[1];
			}
			return $format_date;
		}
		if ($format_date_val != "" && ($format_date_key == "fiscal_date" || $format_date_key == "commercial_date" || $format_date_key == "date")) {
			$format_date = $this->returnDateFormat($format_date_val, "-");
			return $format_date;
		}
		if ($format_date_val != "" && ($format_date_key == "Date (DDMMYY)" || $format_date_key == "Date (MMDDYY)" || $format_date_key == "Date (YYMMDD)")) {
			if(strpos($format_date_val, "[date]") !== false){
				$format_val = explode("[date]", trim($format_date_val));
				$split_date = explode(" ", $format_val[1]);
				$format_date = $this->returnDateFormat($split_date[0], " ", $split_date[1]);
				return $format_date;
			}
		}
		if ($format_date_val != "" && ($format_date_key == "Date (DDMMAA)" || $format_date_key == "Date (MMDDAA)" || $format_date_key == "Date (AAMMDD)")) {
			$split_date = explode(" ", $format_date_val);
			$format_date = $this->returnDateFormat($split_date[0], "", $split_date[1]);
			return $format_date;
		}
		if ($format_date_val != "" && ($format_date_key == "order_closed_day" || $format_date_key == "item_added_day" || $format_date_key == "day")) {
			$split_date = explode(")", $format_date_val);
			$format_date = $this->returnMonthDayFormat($split_date[1], "-");
			return $format_date;
		}
		if ($format_date_val != "" && (preg_match('/^[0-9 .]+$/', $format_date_val) || preg_match('/time.*/', trim($format_date_val)))) {
			$split_date_format = explode(")", $format_date_val);
			$change_date = explode(" ", $split_date_format[1]);
			$change_format = explode("-", $change_date[0]);
			$formatter = explode("(", $split_date_format[0]);
			$date_format = explode(" ", $formatter[1]);
			$split_date = explode("-", $date_format[0]);		
			switch ($setting) {
				case 1:
					$format_date = trim($change_format[2]) . "-" . trim($change_format[1]) . "-" . trim($change_format[0]) . " " . $change_date[1];
					break;
				case 3:
					$format_date = trim($change_format[0]) . "-" . trim($change_format[1]) . "-" . trim($change_format[2]) . " " . $change_date[1];
					break;
				case 2:
				default:
					$format_date = trim($change_format[1]) . "-" . trim($change_format[2]) . "-" . trim($change_format[0]) . " " . $change_date[1];
			}
			return $format_date;
		}
	}

	public function displayKeyDateFormat($format_date_key = ""){
		$setting = $this->getDateFormatSetting();
		if($format_date_key == "Date (YYMMDD)"){
			switch ($setting) {
				case 1:
					$format_date = "Date (DDMMYY)";
					break;
				case 3:
					$format_date = "Date (YYMMDD)";
					break;
				case 2:
				default:
					$format_date = "Date (MMDDYY)";
			}
			return $format_date;
		}
		if ($format_date_key == "Date (AAMMDD)") {
			switch ($setting) {
				case 1:
					$format_date = "Date (DDMMAA)";
					break;
				case 3:
					$format_date = "Date (AAMMDD)";
					break;
				case 2:
				default:
					$format_date = "Date (MMDDAA)";
			}
			return $format_date;
		}
	}

	public function getDateFormatSetting(){
		// based on date format setting
		$date_setting = "date_format";
		$location_date_format_query = lavu_query("SELECT `location`, `setting`, `value` FROM `config` WHERE `setting` = '[1]'", $date_setting);		
		if (! $location_date_format_query || ! mysqli_num_rows($location_date_format_query)) {
			$date_format = array(
					"value" => 2
			);
		} else {
			$date_format = mysqli_fetch_assoc($location_date_format_query);
		}
		return $date_format['value'];
	}
	
	public function returnDateFormat($split_date, $operator, $tail_part=""){
		$setting = $this->getDateFormatSetting();
		$change_format = $this->operatorChecking($operator, $split_date);
		switch ($setting) {
			case 1:
				$format_date = trim($change_format[2]) . $operator . trim($change_format[1]) . $operator . trim($change_format[0]);
				break;
			case 3:
				$format_date = trim($change_format[0]) . $operator . trim($change_format[1]) . $operator . trim($change_format[2]);
				break;
			case 2:
			default:
				$format_date = trim($change_format[1]) . $operator . trim($change_format[2]) . $operator . trim($change_format[0]);
		} 
		if($tail_part != ""){
			return $format_date . " " . $tail_part;
		}else{
			return $format_date;
		}
	}

	public function operatorChecking($operator, $split_date){
		if($operator == ""){
			$change_format = str_split($split_date, 2);
		}else{
			$change_format = explode($operator, $split_date);
		}
		
		return $change_format;
	}
	
	public function returnMonthDayFormat($split_date, $operator){
		$setting = $this->getDateFormatSetting();
		$change_format = $this->operatorChecking($operator, $split_date);
		switch ($setting) {
			case 1:
				$format_date = trim($change_format[1]) . $operator . trim($change_format[0]);
				break;
			case 3:
				$format_date = trim($change_format[0]) . $operator . trim($change_format[1]);
				break;
			case 2:
			default:
				$format_date = trim($change_format[0]) . $operator . trim($change_format[1]);
		}
		return $format_date;
	}

}
