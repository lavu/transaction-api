<?php
	$ms = array();

	$ms['decimal_places'] = $location_info['disable_decimal'];
	$ms['decimal_char'] = $location_info['decimal_char'];
	if($ms['decimal_places'] == 0){
		$ms['decimal_char'] = ".";
		$ms['comma_char'] = ",";
	}else{
		$ms['comma_char'] = $ms['decimal_char'] !== '.'?'.':',';
	}
	$ms['monitary_symbol'] = $location_info['monitary_symbol'];
	$ms['left_or_right'] = $location_info['left_or_right'];
	
	function report_format_money($m)
	{
		global $ms;
		$mstr = number_format(str_replace(",","",$m),$ms['decimal_places'],$ms['decimal_char'],$ms['comma_char']);
		if ($ms['left_or_right'] == "right") return $mstr . html_entity_decode($ms['monitary_symbol']."&rlm;");
		else return html_entity_decode($ms['monitary_symbol'] ." &lrm;"). $mstr;
	}

	function parse_report_lavu_query_string($query)
	{
		global $location_info;

		$rep_loc_id = $location_info['id'];
		$se_mins = ($location_info['day_start_end_time']*1)%100;
		$se_hours = (($location_info['day_start_end_time']*1)-$se_mins)/100;
		//$rep_loc_id = "1";
		$status = respectTimezoneForAllReports($se_hours, $se_mins);
		$todaysDateTime = new DateTime('NOW');
		if (isset($location_info['timezone']) && $location_info['timezone'] != '') {	
			$todaysDateTime->setTimezone(new DateTimeZone($location_info['timezone']));
		}
		$businessStartDateTime = clone $todaysDateTime;
		$businessEndDateTime = clone $todaysDateTime;
		$businessStartDateTime->setTime($se_hours, $se_mins, '00');
		$businessEndDateTime->setTime($se_hours, $se_mins, '00');

		if ($status == -1) {
			$rep_start = $businessStartDateTime->sub(new DateInterval('P1D'))->format('Y-m-d H:i:s');
			$rep_end = $businessEndDateTime->format('Y-m-d H:i:s');
		} else {
			$rep_start = $businessStartDateTime->format('Y-m-d H:i:s');
			$rep_end = $businessEndDateTime->add(new DateInterval('P1D'))->format('Y-m-d H:i:s');
		}

		$conn = ConnectionHub::getConn('rest');
		$query = str_replace("[start_datetime]", $conn->escapeString($rep_start), $query);
		$query = str_replace("[end_datetime]", $conn->escapeString($rep_end), $query);
		$query = str_replace("[loc_id]", $conn->escapeString($rep_loc_id), $query);
		
		return $query;
	}
	
	function run_report_quick_sum($col,$query,$repeatn=1)
	{
		$total_value = 0;
		$query = parse_report_lavu_query_string($query);
		
		for($n=0; $n<$repeatn; $n++)
		{
			$run_query = $query;
			$run_query = str_replace("[n]",($n + 1),$run_query);
			
			$set_value = 0;
			$rep_query = lavu_query($run_query);
			if(mysqli_num_rows($rep_query))
			{
				$rep_read = mysqli_fetch_assoc($rep_query);
				$set_value = $rep_read[$col];
				$set_value = str_replace(",","",str_replace("[money]","",$set_value));
			}
			$total_value += $set_value * 1;
			$total_value = sprintf('%0.4f', $total_value);
		}
		
		return report_format_money($total_value);//"$" . number_format(str_replace(",","",$total_value),2);
	}
	
	function report_add_sums($n1,$n2,$n3=0,$n4=0)
	{
		$n1 = report_money_to_number($n1);//str_replace(",","",$n1) * 1;
		$n2 = report_money_to_number($n2);//str_replace(",","",$n2) * 1;
		$n3 = report_money_to_number($n3);//str_replace(",","",$n3) * 1;
		$n4 = report_money_to_number($n4);//str_replace(",","",$n4) * 1;
		return report_format_money($n1 + $n2 + $n3 + $n4);//"$" . number_format($n1 + $n2 + $n3 + $n4,2);
	}
            
	function getSalesForDay()
	{
		$query = "select menu_categories.name as category, sum(order_contents.quantity) as quantity, if(count(distinct orders.ioid )>count( distinct orders.order_id ),count( distinct orders.ioid ),count( distinct orders.order_id )) as order_count, if(count(*) > 0, concat('[money]',format(sum((order_contents.price + order_contents.modify_price + order_contents.forced_modifiers_price)*order_contents.quantity - if( order_contents.quantity > '0', order_contents.itax, 0) ),2)), '[money]0.00') as gross, if(count(*) > 0, concat('[money]',format(sum(if(order_contents.quantity > 0, order_contents.discount_amount + order_contents.idiscount_amount, 0)),2)), '[money]0.00') as discount, if(count(*) > 0, concat('[money]',format(sum((order_contents.price + order_contents.modify_price + order_contents.forced_modifiers_price)*order_contents.quantity - if( order_contents.quantity > '0',order_contents.discount_amount + order_contents.idiscount_amount + order_contents.itax, 0)),2)), '[money]0.00') as net, if(count(*) > 0, concat('[money]',format(sum( if( order_contents.quantity != 0 AND order_contents.tax_exempt != '1', `order_contents`.`tax1` + `order_contents`.`tax2` + `order_contents`.`tax3` + `order_contents`.`tax4` + `order_contents`.`tax5` - `order_contents`.`itax`, 0 ) ),2)), '[money]0.00') as order_tax, if(count(*) > 0, concat('[money]',format(sum( if( order_contents.quantity != 0 AND order_contents.tax_exempt != '1', `order_contents`.`itax`, 0) ),2)), '[money]0.00') as included_tax, `menu_categories`.`name` as `group_col_menu_categories_name` from `orders`LEFT JOIN `order_contents` ON `orders`.`order_id` = `order_contents`.`order_id` AND `orders`.`location_id` = `order_contents`.`loc_id` LEFT JOIN `menu_items` ON `order_contents`.`item_id` = `menu_items`.`id` LEFT JOIN `menu_categories` ON `menu_items`.`category_id` = `menu_categories`.`id` where orders.closed >= '[start_datetime]' and orders.closed < '[end_datetime]' and orders.void = '0' and orders.location_id='[loc_id]' and order_contents.item != 'SENDPOINT' group by `category`";
		$query = parse_report_lavu_query_string($query);
		$sales_query = lavu_query($query);
		
		$method = "datasets";
		$salesarr = array();
		if($method=="datasets")
		{
			$salesarr['datasets'] = array();
			while($sales_read = mysqli_fetch_assoc($sales_query))
			{
				$dataobj = array();
				$dataobj['label'] = $sales_read['category'];
				$dataobj['data'] = array();
				$dataobj['data'][] = str_replace("[money]","",str_replace(",","",$sales_read['gross']));
				$salesarr['datasets'][] = $dataobj;
			}
			if(count($salesarr['datasets']) < 1)
			{
				$dataobj = array();
				$dataobj['label'] = speak("No Sales");
				$dataobj['data'] = array();
				if(isset($_GET['showmenu'])) 
					$dataobj['data'][] = "10.01";
				else $dataobj['data'][] = "0.00000000001";
				$salesarr['datasets'][] = $dataobj;
			}
		}
		else
		{
			while($sales_read = mysqli_fetch_assoc($sales_query))
			{
				$salesarr[] = str_replace("[money]","",str_replace(",","",$sales_read['gross']));
			}
			if(count($salesarr) < 1)
			{
				$salesarr[] = 1;
			}
		}
		
		if(isset($_GET['jsl'])) echo json_encode($salesarr) . "<br>";
		return json_encode($salesarr);
	}

	$total_sales = run_report_quick_sum("gross","select menu_categories.name as category, sum(order_contents.quantity) as quantity, if(count(distinct orders.ioid )>count( distinct orders.order_id ),count( distinct orders.ioid ),count( distinct orders.order_id )) as order_count, if(count(*) > 0, concat('[money]',format(sum((order_contents.price + order_contents.modify_price + order_contents.forced_modifiers_price)*order_contents.quantity - if( order_contents.quantity > '0', order_contents.itax, 0) ),2)), '[money]0.00') as gross, if(count(*) > 0, concat('[money]',format(sum(if(order_contents.quantity > 0, order_contents.discount_amount + order_contents.idiscount_amount, 0)),2)), '[money]0.00') as discount, if(count(*) > 0, concat('[money]',format(sum((order_contents.price + order_contents.modify_price + order_contents.forced_modifiers_price)*order_contents.quantity - if( order_contents.quantity > '0',order_contents.discount_amount + order_contents.idiscount_amount + order_contents.itax, 0)),2)), '[money]0.00') as net, if(count(*) > 0, concat('[money]',format(sum( if( order_contents.quantity != 0 AND order_contents.tax_exempt != '1', `order_contents`.`tax1` + `order_contents`.`tax2` + `order_contents`.`tax3` + `order_contents`.`tax4` + `order_contents`.`tax5` - `order_contents`.`itax`, 0 ) ),2)), '[money]0.00') as order_tax, if(count(*) > 0, concat('[money]',format(sum( if( order_contents.quantity != 0 AND order_contents.tax_exempt != '1', `order_contents`.`itax`, 0) ),2)), '[money]0.00') as included_tax, `menu_categories`.`name` as `group_col_menu_categories_name` from `orders`LEFT JOIN `order_contents` ON `orders`.`order_id` = `order_contents`.`order_id` AND `orders`.`location_id` = `order_contents`.`loc_id` LEFT JOIN `menu_items` ON `order_contents`.`item_id` = `menu_items`.`id` LEFT JOIN `menu_categories` ON `menu_items`.`category_id` = `menu_categories`.`id` where orders.closed >= '[start_datetime]' and orders.closed < '[end_datetime]' and orders.void = '0' and orders.location_id='[loc_id]' and order_contents.item != 'SENDPOINT'");
	
	$total_eod = run_report_quick_sum("net","select menu_categories.name as category, sum(order_contents.quantity) as quantity, if(count(distinct orders.ioid )>count( distinct orders.order_id ),count( distinct orders.ioid ),count( distinct orders.order_id )) as order_count, if(count(*) > 0, concat('[money]',format(sum((order_contents.price + order_contents.modify_price + order_contents.forced_modifiers_price)*order_contents.quantity - if( order_contents.quantity > '0', order_contents.itax, 0) ),2)), '[money]0.00') as gross, if(count(*) > 0, concat('[money]',format(sum(if(order_contents.quantity > 0, order_contents.discount_amount + order_contents.idiscount_amount, 0)),2)), '[money]0.00') as discount, if(count(*) > 0, concat('[money]',format(sum((order_contents.price + order_contents.modify_price + order_contents.forced_modifiers_price)*order_contents.quantity - if( order_contents.quantity > '0',if(order_contents.discount_amount<(order_contents.price + order_contents.modify_price + order_contents.forced_modifiers_price),order_contents.discount_amount,order_contents.price + order_contents.modify_price + order_contents.forced_modifiers_price) + order_contents.idiscount_amount + order_contents.itax, 0)),2)), '[money]0.00') as net , if(count(*) > 0, concat('[money]',format(sum( if( order_contents.quantity != 0 AND order_contents.tax_exempt != '1', `order_contents`.`tax1` + `order_contents`.`tax2` + `order_contents`.`tax3` + `order_contents`.`tax4` + `order_contents`.`tax5` - `order_contents`.`itax`, 0 ) ),2)), '[money]0.00') as order_tax, if(count(*) > 0, concat('[money]',format(sum( if( order_contents.quantity != 0 AND order_contents.tax_exempt != '1', `order_contents`.`itax`, 0) ),2)), '[money]0.00') as included_tax, `menu_categories`.`name` as `group_col_menu_categories_name` from `orders`LEFT JOIN `order_contents` ON `orders`.`order_id` = `order_contents`.`order_id` AND `orders`.`location_id` = `order_contents`.`loc_id` LEFT JOIN `menu_items` ON `order_contents`.`item_id` = `menu_items`.`id` LEFT JOIN `menu_categories` ON `menu_items`.`category_id` = `menu_categories`.`id` where orders.closed >= '[start_datetime]' and orders.closed < '[end_datetime]' and orders.void = '0' and orders.location_id='[loc_id]' and order_contents.item != 'SENDPOINT'");
	
	$total_payments = run_report_quick_sum("total_payment","select payment_types.type as payment_type, COUNT(cc_transactions.id ) as transactions, if(count(*) > 0, concat('',format(sum( IF(cc_transactions.action='Refund', -1*cc_transactions.total_collected, cc_transactions.total_collected) ),2)), '0.00') as total_payment, if(count(*) > 0, concat('',format(sum( IF(cc_transactions.action='Refund', -1*cc_transactions.tip_amount, cc_transactions.tip_amount) ),2)), '0.00') as total_tip, `payment_types`.`type` as `group_col_payment_types_type` from `cc_transactions`LEFT JOIN `payment_types` ON `cc_transactions`.`pay_type_id` = `payment_types`.`id` AND (`cc_transactions`.`loc_id` = `payment_types`.`loc_id` OR '0' = `payment_types`.`loc_id` ) where cc_transactions.datetime >= '[start_datetime]' and cc_transactions.datetime <= '[end_datetime]' and cc_transactions.voided = '0' and cc_transactions.action != 'Void' and cc_transactions.process_data NOT LIKE 'LOCT%' and cc_transactions.action != '' and cc_transactions.action NOT LIKE '%issue%' and cc_transactions.action NOT LIKE '%reload%' and cc_transactions.loc_id='[loc_id]'");
	
	$order_discounts = run_report_quick_sum("discount","select order_contents.idiscount_sh as discount_name, count(*) as count, if(count(*) > 0, concat('[money]',format(sum( order_contents.idiscount_amount ),2)), '[money]0.00') as discount, `order_contents`.`idiscount_sh` as `group_col_order_contents_idiscount_sh` from `orders` LEFT JOIN `order_contents` ON `orders`.`order_id` = `order_contents`.`order_id` AND `orders`.`location_id` = `order_contents`.`loc_id` where orders.closed >= '[start_datetime]' and orders.closed < '[end_datetime]' and orders.void = '0' and orders.location_id='[loc_id]' and order_contents.idiscount_amount*1 > 0");
	
	$split_check_discounts = run_report_quick_sum("discount","select split_check_details.discount_sh as discount_name, count(*) as count, if(count(*) > 0, concat('[money]',format(sum( split_check_details.discount ),2)), '[money]0.00') as discount, `split_check_details`.`discount_sh` as `group_col_split_check_details_discount_sh` from `orders` LEFT JOIN `split_check_details` ON `orders`.`order_id` = `split_check_details`.`order_id` AND `orders`.`location_id` = `split_check_details`.`loc_id` where orders.closed >= '[start_datetime]' and orders.closed < '[end_datetime]' and orders.void = '0' and orders.location_id='[loc_id]' and split_check_details.discount*1 > 0");
	
	if(isset($_GET['tt'])) echo "OD: $order_discounts<br>scd: $split_check_discounts<br>";
	
	$total_discounts = report_add_sums($order_discounts,$split_check_discounts);
	
	$total_taxes = run_report_quick_sum("tax_total","select order_contents.tax_name[n] as tax, if( order_contents.tax_inclusion = '1', 'Included', 'Not Included') as tax_inclusion, count(*) as count, concat(format(order_contents.tax_rate[n]*100,2),'%') as rate, if(count(*) > 0, concat('[money]',format(sum(order_contents.tax_subtotal[n]),2)), '[money]0.00') as taxed_amount, if(count(*) > 0, concat('[money]',format(sum(order_contents.tax[n]),2)), '[money]0.00') as tax_total, `order_contents`.`tax_name[n]` as `group_col_order_contents_tax_name[n]`, `order_contents`.`tax_name[n]` as `group_col_order_contents_tax_name[n]`, `order_contents`.`tax_name[n]` as `group_col_order_contents_tax_name[n]` from `orders`LEFT JOIN `order_contents` ON `orders`.`order_id` = `order_contents`.`order_id` AND `orders`.`location_id` = `order_contents`.`loc_id` where orders.closed >= '[start_datetime]' and orders.closed < '[end_datetime]' and orders.void = '0' and order_contents.tax_exempt = '0' and orders.location_id='[loc_id]' and order_contents.item != 'SENDPOINT' and order_contents.tax[n] > 0 AND `quantity` > 0",5);
	
	//$salesdata = htmlspecialchars(getSalesForYear());
	$salesdata = htmlspecialchars(getSalesForDay());
	$gl_query="select id from poslavu_MAIN_db.reports_v2 where title ='General Ledger'";
	$gl_read=mysqli_fetch_assoc(mlavu_query($gl_query));
	$gl_report_v2_id=$gl_read[id];
	$extra_reports = "";
	$labor_reporting_enabled = true;
	if($labor_reporting_enabled)  //if($data_name=="gold_st_cafe" || date("Y-m-d H:i:s") > "2016-05-03 10:00:00")
	{
		$extra_reports .= '
		<div class="cpdash-reports">
		  <div class="cpdash-title"><img src="cp-images/ICON_CP_EOD.png" width="60" height="60"><p>'.speak("LABOR").'</p></div>
		  <a href="/cp/index.php?mode=home_labor_report" class="rounded-button borderorange">'.speak("VIEW").'</a>
		</div>
		<div class="cpdash-reports">
		  <div class="cpdash-title"><img src="cp-images/ICON_CP_USAGE.png" width="60" height="60"><p>'.speak("INGREDIENT USAGE").'</p></div>
		  <a href="/cp/index.php?mode=reports_reports_v2&report=38" class="rounded-button borderyellow">'.speak("VIEW").'</a>
		</div>
		<hr>
		<div class="cpdash-reports">
		  <div class="cpdash-title"><img src="cp-images/ICON_CP_ACTIONLOG.png" width="60" height="60"><p>'.speak("ACTION LOG").'</p></div>
		  <a href="/cp/index.php?mode=reports_reports_v2&report=35" class="rounded-button borderorange">'.speak("VIEW").'</a>
		</div>  
        <div class="cpdash-reports">
		  <div class="cpdash-title"><img src="cp-images/ICON_CP_GENERAL_LEDGER.svg" style="width: 74px;height: 94px;margin-top: -14px;""><p>'.speak("GENERAL LEDGER").'</p></div>
		  <a href="/cp/index.php?mode=reports_reports_v2&report='.$gl_report_v2_id.'" class="rounded-button borderorange">'.speak("VIEW").'</a>
		</div>  <!-- END .cpdash-reports -->
		';
	}
	else
	{
		$extra_reports .= '
		<div class="cpdash-reports">
		  <div class="cpdash-title"><img src="cp-images/ICON_CP_ACTIONLOG.png" width="60" height="60"><p>'.speak("ACTION LOG").'</p></div>
		  <a href="/cp/index.php?mode=reports_reports_v2&report=35" class="rounded-button borderorange">'.speak("VIEW").'</a>
		</div>
		<div class="cpdash-reports">
		  <div class="cpdash-title"><img src="cp-images/ICON_CP_USAGE.png" width="60" height="60"><p>'.speak("INGREDIENT USAGE").'</p></div>
		  <a href="/cp/index.php?mode=reports_reports_v2&report=38" class="rounded-button borderyellow">'.speak("VIEW").'</a>
		</div>   <!-- END .cpdash-reports -->
		';
	}
	
	$speak="speak";      
	echo <<<HTML

<style type="text/css">
@font-face {
	font-family: 'din';
	src: url(//www.lavu.com/wp-content/themes/lavu.com/fonts/din-webfont.eot);
	src: url(//www.lavu.com/wp-content/themes/lavu.com/fonts/din-webfont.eot?#iefix) format('embedded-opentype'),
	url(//www.lavu.com/wp-content/themes/lavu.com/fonts/din-webfont.woff) format('woff'),
	url(//www.lavu.com/wp-content/themes/lavu.com/fonts/din-webfont.ttf) format('truetype'),
	url(//www.lavu.com/wp-content/themes/lavu.com/fonts/din-webfont.svg#dinregular) format('svg');
	font-weight: normal;
	font-style: normal;
}

@font-face {
    font-family: 'din';
    src: url(//www.lavu.com/wp-content/themes/lavu.com/fonts/din_medium/din_medium-webfont.eot);
    src: url(//www.lavu.com/wp-content/themes/lavu.com/fonts/din_medium/din_medium-webfont.eot?#iefix) format('embedded-opentype'),
    url(//www.lavu.com/wp-content/themes/lavu.com/fonts/din_medium/din_medium-webfont.woff) format('woff'),
    url(//www.lavu.com/wp-content/themes/lavu.com/fonts/din_medium/din_medium-webfont.ttf) format('truetype'),
    url(//www.lavu.com/wp-content/themes/lavu.com/fonts/din_medium/din_medium-webfont.svg#dinbold) format('svg');
    font-weight: bold;
    font-style: normal;
}

@font-face {
    font-family: 'din';
    src: url(//www.lavu.com/wp-content/themes/lavu.com/fonts/din_light-webfont.eot);
    src: url(//www.lavu.com/wp-content/themes/lavu.com/fonts/din_light-webfont.eot?#iefix) format('embedded-opentype'),
    url(//www.lavu.com/wp-content/themes/lavu.com/fonts/din_light-webfont.woff) format('woff'),
    url(//www.lavu.com/wp-content/themes/lavu.com/fonts/din_light-webfont.ttf) format('truetype'),
    url(//www.lavu.com/wp-content/themes/lavu.com/fonts/din_light-webfont.svg#din_lightregular) format('svg');
    font-weight: lighter;
    font-style: normal;
}
body{margin:0;padding:0;background-color:#fff;font-family : 'din', Verdana, Helvetica, sans-serif;font-size:16px;color:#9b9b9b;min-height:100%;}
h1{padding:40px 18% 20px 18%;margin:0;text-align:center;font-family : 'din', Verdana, Helvetica, sans-serif;font-size:24px;color:#8b8b8b;font-weight:lighter;}
p{padding:20px 18% 20px 18%;margin:0;text-align:justify;font-weight:normal;}
hr{display: block;margin-top:0px;margin-bottom:20px;margin-left:auto;margin-right:auto;border0;border-top:1px solid #dddddd;}
.header{width:100%;height:90px;background-color:#2b2b2b;display:block;}
.logo{position:relative;top:50%;left:50%;-webkit-transform: translate(-50%, -50%);-moz-transform: translate(-50%, -50%);-ms-transform: translate(-50%, -50%);-o-transform: translate(-50%, -50%);transform: translate(-50%, -50%);}
.grayblock{width:100%;height:3px;background-color:#bdbdbd;margin:0;padding:0;}
.colorblock{height:100%;width:20px;float:right;}
.main{max-width:1000px;height:100%;background-color:#ffffff;margin:0 auto 0 auto;}
.cpdash-container{width:860px;height:500px;margin:40px 0;}
.cpdash-sales{width:360px;height:100%;float:left;}
.cpdash-salesgraph{margin:40px 0 20px 0;}
.cpdash-reports{width:250px;float:left;margin-bottom:20px;}
.cpdash-title{width:100%;height:60px;float:left;}
.cpdash-title img{float:left;height:60px;width:60px;}
.cpdash-title p{float:left;font-size:16px;color:#9b9b9b;vertical-align:bottom;padding:38px 2px 2px 4px;}
.cpdash-stats{width:100%;height:34px;float:left;font-size:2em;color:#9b9b9b; margin: 10px 0 0 0;}
.cpdash-stats p{float:left;font-size:24px;color:#9b9b9b;vertical-align:bottom;padding:2px 2px 2px 8px;}
.rounded-button{background-color: #fff;border-radius: 5px;display: inline-block;font-size:1.175rem;font-weight: normal;font-family:'din', Verdana, Helvetica, sans-serif;margin: 0;text-transform:uppercase;text-decoration:none;-webkit-box-sizing:border-box;-moz-box-sizing:border-box;box-sizing:border-box;vertical-align:middle;line-height: 26px;padding: 0 40px;margin:10px 0 10px 0;}
.rounded-button:hover{border: 2px solid #9b9b9b;/*background-color: #9b9b9b;*/color:#fff!important;}
.bordergreen{border: 2px solid #aecd37;color:#aecd37!important;}
.borderteal{border: 2px solid #0eae95;color:#0eae95!important;}
.borderblue{border: 2px solid #3382a9;color:#3382a9!important;}
.borderpurple{border: 2px solid #5053a2;color:#5053a2!important;}
.borderorange{border: 2px solid #f68e20;color:#f68e20!important;}
.borderyellow{border: 2px solid #ebcf4a;color:#ebcf4a!important;}
.bargreen{border-left: 10px solid #aecd37;color:#aecd37!important;}
.barteal{border-left: 10px solid #0eae95;color:#0eae95!important;}
.barblue{border-left: 10px solid #3382a9;color:#3382a9!important;}
.barpurple{border-left: 10px solid #5053a2;color:#5053a2!important;}
.barorange{border-left: 10px solid #f68e20;color:#f68e20!important;}
.baryellow{border-left: 10px solid #ebcf4a;color:#ebcf4a!important;}

.bordergreen:hover{border-color: #aecd37!important; background: #aecd37!important;}
.borderteal:hover{border-color: #0eae95!important; background: #0eae95!important;}
.borderblue:hover{border-color: #3382a9!important; background: #3382a9!important;}
.borderpurple:hover{border-color: #5053a2!important; background: #5053a2!important;}
.borderorange:hover{border-color: #f68e20!important; background: #f68e20!important;}
.borderyellow:hover{border-color: #ebcf4a!important; background: #ebcf4a!important;}

.monster{width:66px;height:66px;position:relative;top:0;left:50%;-webkit-transform: translate(-50%, -50%);-moz-transform: translate(-50%, -50%);-ms-transform: translate(-50%, -50%);-o-transform: translate(-50%, -50%);transform: translate(-50%, -50%);display:block;}
.leftt{float:left;}
.right{float:right;}
img.center{display:block;margin-left:auto;margin-right:auto;}
</style>

<div class="main">         
  <div class="cpdash-container" style='text-align:left'>
    <div class="cpdash-sales" style="height:700px">
      <div class="cpdash-title"><img src="cp-images/ICON_CP_SALES.png" width="60" height="60"><p>{$speak("SALES")}</p></div>
      <div class="cpdash-stats bargreen"><p>{$total_sales}</p></div>
      <div class="cpdash-salesgraph">
      
      	<script type="text/javascript" src="/cp/reports_v2/chart.js"></script>
      	<!--<img src="cp-images/SWEET_GRAPH.png" width="322" style="margin-top:10px;">-->
				<div>
					<!--<div id="c_legend" style="display: block; text-align: right; font-size: 0.75em; float: right; max-width: 50%;"></div>-->
					<div style='overflow: hidden; margin: 5px 5px 20px 20px;'>Today's Sales: <b style="font-size: 1.25em; color: #666;"></b></div>
				</div>
				<div style='position:relative;'>
                	&nbsp;
                    <div style='positon:absolute; top:0px; left:0px; z-index:400'>
	                	<canvas id="c_summary" data="{$salesdata}" width="350" height="320" style="width: 350px; height: 320px;"></canvas>
                    </div>
                    <div style='position:absolute; top:370px; left:0px; z-index:800'>
                          <a href="/cp/index.php?mode=reports_reports_v2&report=31" class="rounded-button borderteal">{$speak("VIEW")}</a>
                   </div>
                </div>
			<!-- </div> -->  
      </div>
    </div>   <!-- END .cpdash-sales -->
    
    <div class="cpdash-reports">
      <div class="cpdash-title"><img src="cp-images/ICON_CP_EOD.png" width="60" height="60"><p>{$speak("END OF DAY")}</p></div>
      <div class="cpdash-stats barteal"><p>{$total_eod}</p></div>
      <a href="/cp/index.php?mode=reports_reports_v2&report=33" class="rounded-button borderteal">{$speak("VIEW")}</a>
    </div>
    <div class="cpdash-reports">
      <div class="cpdash-title"><img src="cp-images/ICON_CP_PAYMENTS.png" width="60" height="60"><p>{$speak("PAYMENTS")}</p></div>
      <div class="cpdash-stats barblue"><p>{$total_payments}</p></div>
      <a href="/cp/index.php?mode=reports_reports_v2&report=30" class="rounded-button borderblue">{$speak("VIEW")}</a>
    </div>
    
    <hr>
    
    <div class="cpdash-reports">
      <div class="cpdash-title"><img src="cp-images/ICON_CP_DISCOUNTS.png" width="60" height="60"><p>{$speak("DISCOUNTS")}</p></div>
      <div class="cpdash-stats barpurple"><p>{$total_discounts}</p></div>
      <a href="/cp/index.php?mode=reports_reports_v2&report=37" class="rounded-button borderpurple">{$speak("VIEW")}</a>
    </div>
    <div class="cpdash-reports">
      <div class="cpdash-title"><img src="cp-images/ICON_CP_TAXES.png" width="60" height="60"><p>{$speak("TAXES")}</p></div>
      <div class="cpdash-stats bargreen"><p>{$total_taxes}</p></div>
      <a href="/cp/index.php?mode=reports_reports_v2&report=36" class="rounded-button bordergreen">{$speak("VIEW")}</a>
    </div>
    
    <hr>
        
    {$extra_reports}
  </div>   <!-- END .cpdash-container -->
</div>

<script type="text/javascript">
	NumberFormatter.sharedFormatter( "{$ms['comma_char']}", "{$ms['decimal_char']}", {$ms['decimal_places']}, "{$ms['monitary_symbol']}", "{$ms['left_or_right']}" );
	var canvas = document.getElementById('c_summary');
	// var canvas = canvases[i];
	var ctx = canvas.getContext('2d');
	var chart = new Chart( ctx );
	canvas.chart = chart;
	var data = JSON.parse( canvas.getAttribute('data') );
	//var c = chart.Line( data );
	//var c = chart.Pie( data );
	var c = chart.Doughnut( data );
</script>


HTML;
?>
