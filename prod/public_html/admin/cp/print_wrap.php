<?php

	// takes the place of index.php for iframes in printers.php
	session_start();
	
	require_once(__DIR__."/resources/core_functions.php");
	require_once(__DIR__."/resources/json.php");
	require_once(__DIR__.'/resources/help_box.php');
	
	$pw_mode = (isset($_REQUEST['pw_mode']))?$_REQUEST['pw_mode']:"";

	$rdb = admin_info("database");
	$loggedin_id = admin_info("loggedin");
	$data_name = sessvar("admin_dataname");
	$locationid = sessvar("locationid");
	
	if (isset($_REQUEST['dn_loc'])) {
		$dn_loc = explode(":", $_REQUEST['dn_loc']);
		if ($dn_loc[0]!=$data_name || $dn_loc[1]!=$locationid) {
			error_log("invalid session: ".$pw_mode);
			exit();
		}
	}

	function is_lavu_admin() {
		
		return (admin_info("lavu_admin") == 1);
	}
	
	$in_lavu = true;
	
	lavu_connect_dn($data_name);

	if ($pw_mode == 'kiosk_printers') {
		echo "<html><head><link rel='stylesheet' type='text/css' href='styles/styles.css'><script type='text/javascript' src='scripts/index.js'></script></head><body style='min-width:655px; max-width:655px; padding:0px 0px 0px 0px; margin:0px 0px 0px 0px;'>";
		echo "<center><div id='pw_content_area' style='padding:0px 0px 0px 0px; margin:0px 0px 0px 0px;'>";
	}
	else
	echo "<html><head><link rel='stylesheet' type='text/css' href='styles/styles.css'><script type='text/javascript' src='scripts/index.js'></script></head><body style='min-width:700px; max-width:700px; padding:0px 0px 0px 0px; margin:0px 0px 0px 0px;'>";
	echo "<center><div id='pw_content_area' style='padding:0px 0px 0px 0px; margin:0px 0px 0px 0px;'>";
	if (!empty($pw_mode)) require_once(dirname(__FILE__).'/areas/'.$pw_mode.'.php');
	echo "</div></center></body></html>";

?>
