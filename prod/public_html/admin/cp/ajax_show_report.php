<?php
set_time_limit ( 60 );
session_start ();

ini_set ( 'memory_limit', '120M' );
putenv ( "TZ=America/Chicago" );
header ( 'Content-Type: application/html' );
require_once (__DIR__ . "/resources/core_functions.php");
require_once (__DIR__ . "/resources/action_tracker.php");
require_once ("/home/poslavu/public_html/admin/lib/modules.php");
function checkLoginStatus($loggedin_id) {
	$loggedin_is_distro = false;
	if ($loggedin_id == 1001 && substr ( admin_info ( "username" ), 0, 15 ) == "poslavu_distro_") {
		$dcode_query = mlavu_query ( "select `distro_code` as `distro_code` from `poslavu_MAIN_db`.`restaurants` where `data_name`='[1]'", admin_info ( "dataname" ) );
		if (mysqli_num_rows ( $dcode_query )) {
			$dcode_read = mysqli_fetch_assoc ( $dcode_query );
			if (substr ( admin_info ( "username" ), 15 ) == $dcode_read ['distro_code']) {
				$loggedin_is_distro = true;
			}
		}
	}
	
	if ($loggedin_is_distro) {
	} else if ($loggedin_id == 1000 && strstr ( admin_info ( "username" ), "poslavu_admin" )) {
	} else {
		$confirm_access_query = mlavu_query ( "select * from `poslavu_" . admin_info ( 'root_dataname' ) . "_db`.`users` where `id`='[1]' and `access_level`>='3' and `_deleted`!='1'", $loggedin_id );
		if (mysqli_num_rows ( $confirm_access_query ) < 1) {
			return false;
		}
	}
	return true;
}

// Checking here if check_location_id() does not exist then add it. I am not removing might be this file is using other file as well
if (!function_exists('check_location_id')) {
	function check_location_id($rdb, $locationid) {
		if (( int ) $locationid == 0 && trim ( $rdb ) !== '') {
			$query = mlavu_query ( "SELECT `id` FROM `[1]`.`locations` WHERE `_disabled` = '0' LIMIT 1", $rdb );
			if ($query !== FALSE && mysqli_num_rows ( $query ) > 0) {
				$query_read = mysqli_fetch_assoc ( $query );
				$locationid = $query_read ['id'];
				set_sessvar ( 'locationid', $locationid );
			}
		}
		return $locationid;
	}
}

function getModules() {
	global $location_info;
	global $o_package_container;
	global $current_package;
	
	$dataname = admin_info ( 'dataname' );
	if (strlen ( $dataname ) == 0) {
		return new Module ();
		;
	}
	
	$use_net_path = isset ( $location_info ['use_net_path'] ) ? $location_info ['use_net_path'] : '0';
	$net_path = isset ( $location_info ['net_path'] ) ? $location_info ['net_path'] : '';
	$has_LLS = ! ($use_net_path == "0" || strpos ( $net_path, ".poslavu.com" ) !== false);
	
	if ((! sessvar_isset ( "modules" )) || (sessvar ( "modules_dn" ) != $dataname)) {
		
		$module_query = mlavu_query ( "SELECT `modules`, `package_status`,`id` FROM `poslavu_MAIN_db`.`restaurants` WHERE `data_name`='[1]' LIMIT 1", $dataname );
		$module_result = mysqli_fetch_assoc ( $module_query );
		
		// get the package
		$restaurant_query = mlavu_query ( "SELECT `id` FROM `poslavu_MAIN_db`.`restaurants` WHERE `data_name`='[1]' LIMIT 1", $dataname );
		$restaurant_result = array ();
		if ($restaurant_query && mysqli_num_rows ( $restaurant_query )) {
			$restaurant_result = mysqli_fetch_assoc ( $restaurant_query );
		} else {
			$restaurant_result ['id'] = $module_result ['id'];
		}
		require_once (dirname ( __FILE__ ) . '/../sa_cp/billing/payment_profile_functions.php');
		$package_result = find_package_status ( $restaurant_result ['id'] );
		
		// get the package
		/*
		 * $package_query = mlavu_query("SELECT `package` FROM `poslavu_MAIN_db`.`signups` WHERE `dataname`='[1]' LIMIT 1", $dataname);
		 * if($package_query && mysqli_num_rows($package_query))
		 * {
		 * $package_result = mysqli_fetch_assoc($package_query);
		 * }
		 * else
		 * {
		 * $package_result['package'] = $module_result['package_status'];
		 * }
		 */
		
		$component_package = '';
		if (! empty ( $location_info ['component_package_code'] )) {
			$component_package = $location_info ['component_package_code'];
		}
		set_sessvar ( "modules", $module_result ['modules'] );
		set_sessvar ( "package", $package_result ['package'] );
		set_sessvar ( "isLLS", $has_LLS );
		set_sessvar ( "modules_dn", $dataname );
		set_sessvar ( "componentPackage", $component_package );
	}
	$modules = getActiveModules ( sessvar ( 'modules' ), sessvar ( 'package' ), sessvar ( 'isLLS' ), sessvar ( 'componentPackage' ) );
	
	return $modules;
}
function ajax_show_report($dbname, $maindb, $date_mode, $rurl, $reportid, $special = false, $set_locationid = false, $is_combo_rep = false, $return_query = false, $b_second_part_of_multi_report = FALSE) {
	if ($special == "debug")
		$debug = true;
	else
		$debug = false;
	if (strpos ( $special, "export" ) !== false)
		$export = true;
	else
		$export = false;
	if (substr ( $special, 0, 10 ) == "subreport:") {
		$use_subreport = true;
		$subreport = substr ( $special, 10 );
	} else
		$use_subreport = false;
		
		// report options - controled
	
	require_once (dirname ( __FILE__ ) . '/../cp/areas/reports/graphData.php');
	$b_draw_pretty_graphs = TRUE;
	
	// whether to draw multiple locations as separate reports or as a single report
	if (isset ( $_GET ['sep_loc'] )) {
		$b_separate_locations = ($_GET ['sep_loc'] == '0') ? FALSE : TRUE;
		set_rs_start_val ( 'draw_multi_reports_separate_locations', $b_separate_locations );
	} else {
		$b_separate_locations = rs_getvar ( 'draw_multi_reports_separate_locations', TRUE );
	}
	
	// --------------------------------------------- Multi Location Code -------------------------------------//
	if (isset ( $_GET ['multireport'] )) {
		$showing_multi_report = trim ( $_GET ['multireport'] );
		if (( int ) $showing_multi_report == 0)
			$showing_multi_report = FALSE;
		set_rs_start_val ( 'draw_multi_reports_multireport', $showing_multi_report );
	} else {
		$showing_multi_report = rs_getvar ( 'draw_multi_reports_multireport', FALSE );
	}
	
	// testing/debugging (can be deleted)
	$mpcount = 0;
	foreach ( $_GET as $k => $v )
		if ($k == 'multireport')
			$mpcount ++;
	if (($showing_multi_report == "0" || $showing_multi_report === 0 || $mpcount > 1) && $showing_multi_report !== FALSE)
		error_log ( __FILE__ . ' error with multireport: ' . gettype ( $showing_multi_report ) . ', getvars:' . print_r ( $_GET, TRUE ) );
	if ($showing_multi_report == "0" || $showing_multi_report == 0) {
		$showing_multi_report = FALSE;
		set_rs_start_val ( 'draw_multi_reports_multireport', $showing_multi_report );
	}
	
	global $allow_multi_location;
	$selector_str = "";
	$multi_location_ids = "";
	if ($allow_multi_location) {
		$cr_dataname = $dbname;
		$cr_dataname = substr ( $cr_dataname, 8 );
		$cr_dataname = substr ( $cr_dataname, 0, strlen ( $cr_dataname ) - 3 );
		
		if (isset ( $_GET ['mode'] ) && substr ( $_GET ['mode'], 0, 7 ) == "reports") {
			
			// get the chain reporting groups
			require_once (dirname ( dirname ( __FILE__ ) ) . '/sa_cp/manage_chain_functions.php');
			$cr_groups = CHAIN_USER::get_user_reporting_groups ();
			$a_chain_locations = getChainLocations ( $cr_dataname, FALSE, FALSE );
			
			// check that the currently selected group is one of the groups this user has access to
			// -1 means "all locations for this restaurant"
			if ($showing_multi_report !== FALSE && $showing_multi_report !== '-1') {
				$b_found = FALSE;
				$first_cr_group = NULL;
				foreach ( $cr_groups as $cr_group ) {
					$first_cr_group = $cr_group;
					if (( int ) $cr_group ['id'] == ( int ) $showing_multi_report) {
						$b_found = TRUE;
						break;
					}
				}
				if (! $b_found)
					$showing_multi_report = ($first_cr_group !== NULL) ? $first_cr_group ['id'] : FALSE;
			}
			
			$cr_group_name = "";
			if ((count ( $cr_groups ) > 0 || count ( $a_chain_locations ) > 1)) {
				// find the group name and associated accounts
				foreach ( $cr_groups as $cr_group ) {
					if (( int ) $showing_multi_report == ( int ) $cr_group ['id']) {
						$cr_group_name = $cr_group ['name'];
						$multi_location_ids = $cr_group ['contents'];
					}
				}
				
				// create a getvar string
				$uri = $_SERVER ['QUERY_STRING'];
				$uri = str_ireplace ( array (
						"&multireport=0",
						"&multireport=" . $showing_multi_report 
				), "", $uri );
				
				// create a dropdown select box for selecting the reporting group
				$selector_str .= "<select onchange='if(this.value==\"\") window.location = \"index.php?$uri&multireport=0\"; else window.location = \"index.php?$uri&multireport=\" + this.value'>";
				$selector_str .= "<option value=''>Showing Current Location</option>";
				if (count ( $a_chain_locations ) > 1) {
					$s_selected = ($showing_multi_report == '-1') ? "selected" : "";
					$selector_str .= "<option value='-1' $s_selected>Current Account - All Locations</option>";
				}
				if (count ( $cr_groups ) > 0) {
					foreach ( $cr_groups as $cr_group ) {
						$groupid = $cr_group ['id'];
						$groupname = $cr_group ['name'];
						$s_selected = (( int ) $groupid == ( int ) $showing_multi_report) ? "selected" : "";
						$selector_str .= "<option value='$groupid' $s_selected>$groupname</option>";
					}
				}
				$selector_str .= "</select>";
				
				// create a dropdown with an option to view all locations in one report or
				// in separate reports
				if ($b_draw_multi_locations && $showing_multi_report != FALSE && $showing_multi_report !== '-1') {
					$s_group_selected = ($b_separate_locations) ? "" : "selected";
					$s_separate_selected = ($b_separate_locations) ? "selected" : "";
					$sep_uri = str_replace ( array (
							'&sep_loc=0',
							'&sep_loc=1' 
					), '', $uri );
					$sep_uri = str_replace ( array (
							'sep_loc=0&',
							'sep_loc=1&' 
					), '', $sep_uri );
					$sep_uri = str_replace ( array (
							'sep_loc=0',
							'sep_loc=1' 
					), '', $sep_uri );
					$selector_str .= "<select onchange='window.location = \"index.php?$sep_uri&sep_loc=\"+this.value'>
													<option value='0' $s_group_selected>Group Locations</option>
													<option value='1' $s_separate_selected>Separate Locations</option>
													</select>";
				}
			}
		}
	}
	
	if (strpos ( $date_mode, "|" ))
		$selector_str = "";
		// -------------------------------------------------------------------------------------------------------//
	
	$show_extra_functions = false;
	if (isset ( $_GET ['test48'] ))
		$show_extra_functions = true;
	
	if (! $export && ! $return_query) {
		// createWhiteBox ();
	}
	
	if ($set_locationid)
		$locationid = $set_locationid;
	else if (sessvar_isset ( "locationid" ))
		$locationid = sessvar ( "locationid" );
	else
		$locationid = false;
	
	$date_start = "";
	$date_end = "";
	$time_start = "";
	$time_end = "";
	$setdate = '';
	$set_date_end = '';
	$set_date_start = '';
	$day_duration = '';
	
	$primary_table = "";
	$join_code = "";
	$select_code = "";
	$select_outer_code = "";
	$where_code = "";
	$stitles = array ();
	$sdisplay = array ();
	$smonetary = array ();
	$ssum = array ();
	$ssumop = array ();
	$sgraph = array ();
	$sbranches = array ();
	$salign = array ();
	$scount = 0;
	
	$groupid = "";
	$dateid = "";
	$timeid = "";
	$groupby = "";
	
	$s_header = "";
	$s_body = "";
	$s_footer = "";
	$s_graph = "";
	$output = "";
	$export_output = "";
	$export_row = "\r\n";
	$export_col = "\t";
	if ($special == "export:xls")
		$export_ext = "xls";
	else if ($special == "export:csv") {
		$export_ext = "csv";
		$export_col = ",";
	} else
		$export_ext = "txt";
	
	$branchto = (isset ( $_GET ['branch'] )) ? $_GET ['branch'] : "";
	$branch_title = (isset ( $_GET ['branch_title'] )) ? $_GET ['branch_title'] : "";
	$branch_col = (isset ( $_GET ['branch_col'] )) ? $_GET ['branch_col'] : "";
	$branch_fieldname = "";
	$repeat_foreach = "";
	
	if ($reportid == "select_date")
		$date_selector = true;
	else
		$date_selector = false;
	
	if ($date_selector) {
		$date_select_type = "Day Range";
	} else {
		$report_query = mlavu_query ( "select * from `$maindb`.`reports` where id='$reportid'" );
		if (mysqli_num_rows ( $report_query )) {
			$report_read = mysqli_fetch_assoc ( $report_query );
			$groupid = $report_read ['groupid'];
			$dateid = $report_read ['dateid'];
			$locid = $report_read ['locid'];
			$timeid = $report_read ['timeid'];
			$group_operation = $report_read ['group_operation'];
			$order_operation = $report_read ['order_operation'];
			$orderby = explode ( " ", $report_read ['orderby'] );
			$date_select_type = $report_read ['date_select_type'];
			$report_title = $report_read ['title'];
			$repeat_foreach = $report_read ['repeat_foreach'];
			$extra_functions = $report_read ['extra_functions'];
			$set_unified = $report_read ['unified'];
			
			if (isset ( $_GET ['test'] ))
				echo "date mode: $set_unified <br>";
			if ($set_unified == "No")
				$date_mode = "single";
			
			if ($report_read ['report_type'] == "Combo")
				$date_mode = "combo|" . $date_mode;
			
			if (count ( $orderby ) > 1) {
				$orderby_id = trim ( $orderby [0] );
				$orderby_dir = trim ( $orderby [1] );
			} else {
				$orderby_id = 0;
				$orderby_dir = "asc";
			}
			$orderby = "";
			
			if ($use_subreport) {
				$s_header .= "<b>" . $subreport . "</b>";
			} else {
				$s_header .= "Report: ";
				if ($is_combo_rep)
					$s_header .= "<a href='?mode=reports_id_" . $report_read ['id'] . "'><b class='reportTitle'>" . $report_read ['title'] . "</a>";
				else {
					$s_header .= "<b class='reportTitle'>" . $report_read ['title'];
					if ($report_read ['title'] == 'Credit Card Batches')
						$s_header .= "</b><br/> This report is available for customers processing with Mercury Payment Systems and who manually settle the batch.";
				}
				if ($branch_col != "")
					$s_header .= ": " . $branch_title;
				$s_header .= "</b>";
				
				$s_header .= "&nbsp;" . $selector_str;
			}
			$s_header .= "<br><br>";
		}
	}
	
	$explicit_date_end = false;
	$rep_type = "Regular";
	if (substr ( $date_mode, 0, 5 ) == "auto|") {
		$date_parts = explode ( "|", $date_mode );
		$date_mode = $date_parts [1];
		$date_start = $date_parts [2];
		$time_start = $date_parts [3];
		$date_end = $date_parts [4];
		$time_end = $date_parts [5];
		$date_select_type = "auto";
		$rep_type = "SubReport";
		
		$set_date_start = $date_start;
		$set_date_end = $date_end;
		$set_time_start = $time_start;
		$set_time_end = $time_end;
		// if(isset($_GET['test'])) echo "SETDATE:".$set_date_start . " - " . $set_date_end;
		
		$explicit_date_end = true;
	}
	if (substr ( $date_mode, 0, 6 ) == "combo|") {
		$date_parts = explode ( "|", $date_mode );
		$date_mode = $date_parts [1];
		$rep_type = "Combo";
		$date_select_type = "Day Range";
	}
	// if(isset($_GET['test'])) echo "date mode: $date_mode <br>";
	
	$change_group_from = "";
	$change_group_to = "";
	$change_group_id = "";
	$reporturl = "$rurl&reportid=$reportid&rmode=view";
	// ------------------------------------ date/time selection here -----------------------------------//
	if ($date_select_type == "Year") {
		$setdate = (isset ( $_GET ["setdate"] )) ? $_GET ['setdate'] : date ( "Y" );
		$s_header .= "<select onchange='window.location = \"$reporturl&setdate=\" + this.value'>";
		for($i = date ( "Y" ) - 8; $i < date ( "Y" ) + 8; $i ++) {
			$sv = $i;
			$s_header .= "<option value='$sv'";
			if ($sv == $setdate)
				$s_header .= " selected";
			$s_header .= ">" . date ( "Y", mktime ( 0, 0, 0, 0, 0, $i + 1 ) ) . "</option>";
		}
		$s_header .= "</select>";
		$set_date_start = $setdate . "-01-01";
		$set_date_end = $setdate . "-12-31";
		$set_time_start = "00:00:00";
		$set_time_end = "24:00:00";
	} else if ($date_select_type == "Month") {
		$setdate = (isset ( $_GET ["setdate"] )) ? $_GET ['setdate'] : date ( "Y-m" );
		$s_header .= "<select onchange='window.location = \"$reporturl&setdate=\" + this.value'>";
		for($i = date ( "Y" ) - 2; $i < date ( "Y" ) + 2; $i ++) {
			for($n = 1; $n <= 12; $n ++) {
				if ($n < 10)
					$m = "0" . $n;
				else
					$m = $n;
				$sv = $i . "-" . $m;
				$s_header .= "<option value='$sv'";
				if ($sv == $setdate)
					$s_header .= " selected";
				$s_header .= ">" . date ( "M Y", mktime ( 0, 0, 0, $n + 1, 0, $i ) ) . "</option>";
			}
		}
		$s_header .= "</select>";
		$set_date_start = $setdate . "-01";
		$set_date_end = $setdate . "-31";
		$set_time_start = "00:00:00";
		$set_time_end = "24:00:00";
	} else if ($date_select_type == "Day" || $date_select_type == "Day Range") {
		// $setdate = (isset($_GET["setdate"]))?$_GET['setdate']:date("Y-m-d");
		// $day_duration = (isset($_GET['day_duration']))?$_GET['day_duration']:1;
		// $stime = (isset($_GET['stime']))?$_GET['stime']:0;
		
		if (date ( "H" ) < 7)
			$default_setdate = date ( "Y-m-d", mktime ( 0, 0, 0, date ( "m" ), date ( "d" ) - 1, date ( "Y" ) ) );
		else
			$default_setdate = date ( "Y-m-d" );
		
		$setdate = rs_getvar ( "setdate", $default_setdate );
		$day_duration = rs_getvar ( "day_duration", 1 );
		$stime = rs_getvar ( "stime", 0 );
		
		$s_header .= "<script type=\"text/javascript\" src=\"resources/datepickercontrol.js\"></script>";
		$s_header .= "<link type=\"text/css\" rel=\"stylesheet\" href=\"resources/datepickercontrol.css\">";
		$s_header .= "<script language='javascript'>";
		$s_header .= "DatePickerControl.onSelect = function(inputid) { ";
		$s_header .= "   window.location = '$reporturl&day_duration=$day_duration&stime=$stime&setdate=' + document.getElementById(inputid).value; ";
		$s_header .= "} ";
		$s_header .= "</script>";
		
		$set_date_start = $setdate;
		$date_start_ts = mktime ( 0, 0, 0, substr ( $set_date_start, 5, 2 ), substr ( $set_date_start, 8, 2 ), substr ( $set_date_start, 0, 4 ) );
		
		$s_header .= "<input type=\"hidden\" id=\"DPC_TODAY_TEXT\" value=\"today\">";
		$s_header .= "<input type=\"hidden\" id=\"DPC_BUTTON_TITLE\" value=\"Open calendar...\">";
		$s_header .= "<input type=\"hidden\" id=\"DPC_MONTH_NAMES\" value=\"['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December']\">";
		$s_header .= "<input type=\"hidden\" id=\"DPC_DAY_NAMES\" value=\"['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat']\">";
		$s_header .= "<input type=\"text\" name=\"edit1\" id=\"edit1\" datepicker=\"true\" datepicker_format=\"YYYY-MM-DD\" value=\"" . date ( "m/d/Y", $date_start_ts ) . "\">";
		if ($date_select_type == "Day Range") {
			$s_header .= "<select id=\"toDateSelect\" onchange='window.location = \"$reporturl&setdate=$setdate&stime=$stime&day_duration=\" + this.value'>";
			for($i = 1; $i <= 365; $i ++) {
				$s_header .= "<option value='$i'";
				if ($day_duration == $i)
					$s_header .= " selected";
				$s_header .= ">";
				$s_header .= "to " . date ( "m/d/Y", mktime ( 0, 0, 0, date ( "m", $date_start_ts ), date ( "d", $date_start_ts ) + ($i - 1), date ( "Y", $date_start_ts ) ) ) . " ($i day";
				if ($i > 1)
					$s_header .= "s";
				$s_header .= ")";
				$s_header .= "</option>";
			}
			$s_header .= "</select>";
		} else
			$day_duration = 1;
		
		$set_date_end = date ( "Y-m-d", mktime ( 0, 0, 0, date ( "m", $date_start_ts ), date ( "d", $date_start_ts ) + ($day_duration - 1), date ( "Y", $date_start_ts ) ) );
		$set_time_start = "00:00:00";
		$set_time_end = "24:00:00";
		
		$set_nextdate = date ( "Y-m-d", mktime ( 0, 0, 0, date ( "m", $date_start_ts ), date ( "d", $date_start_ts ) + ($day_duration), date ( "Y", $date_start_ts ) ) );
		$s_header .= "<input type='button' value='Next >>' onclick='window.location = \"$reporturl&setdate=$set_nextdate&stime=$stime&day_duration=$day_duration\"'>";
		
		/*
		 * $s_header .= " End of Day: <select name='stime' onchange='window.location = \"$reporturl&setdate=$setdate&day_duration=$day_duration&stime=\" + this.value'>";
		 * for($t=0; $t<=900; $t+=100)
		 * {
		 * $hour = (($t - ($t % 100)) / 100);
		 * $min = ($t % 100);
		 * if($min < 10) $min = "0" . $min;
		 * if($t==0) $show_time = "12:00am";
		 * else $show_time = $hour . ":" . $min . "am";
		 * $s_header .= "<option value='$t'";
		 * if($stime==$t) $s_header .= " selected";
		 * $s_header .= ">$show_time</option>";
		 * }
		 * $s_header .= "</select>";
		 */
	}
	
	$date_reports_by = "opened";
	$rep_time_query = mlavu_query ( "select * from `$maindb`.`report_parts` where `reportid`='[1]' and `id`='[2]'", $reportid, $dateid );
	if (mysqli_num_rows ( $rep_time_query )) {
		preg_match ( "/poslavu_(.*)_db/", $dbname, $data_name );
		lavu_connect_dn ( $data_name [1], $dbname );
		$rep_time_read = mysqli_fetch_assoc ( $rep_time_query );
		// if(isset($_GET['testing'])) echo "<br>found report open/close: ".$rep_time_read['fieldname']."-".$date_select_type."<br>";
		if (($rep_time_read ['fieldname'] == "opened" || $rep_time_read ['fieldname'] == "closed") && ($date_select_type == "Year" || $date_select_type == "Month" || $date_select_type == "Day" || $date_select_type == "Day Range" || $date_select_type == "auto")) {
			if (isset ( $_GET ['date_reports_by'] )) {
				$cfg_query = lavu_query ( "select * from `[1]`.`config` where `location`='[2]' and `type`='special' and `setting`='report config'", $dbname, $locationid );
				if (mysqli_num_rows ( $cfg_query )) {
					$cfg_read = mysqli_fetch_assoc ( $cfg_query );
					lavu_query ( "update `[1]`.`config` set `value`='[2]' where `id`='[3]'", $dbname, $_GET ['date_reports_by'], $cfg_read ['id'] );
				} else
					lavu_query ( "insert into `[1]`.`config` (`location`,`type`,`setting`,`value`) values ('[2]','special','report config','[3]')", $dbname, $locationid, $_GET ['date_reports_by'] );
			}
			
			$cfg_query = lavu_query ( "select * from `[1]`.`config` where `location`='[2]' and `type`='special' and `setting`='report config'", $dbname, $locationid );
			if (mysqli_num_rows ( $cfg_query )) {
				$cfg_read = mysqli_fetch_assoc ( $cfg_query );
				$date_reports_by = ConnectionHub::getConn ( 'rest' )->escapeString ( $cfg_read ['value'] );
				if (isset ( $_GET ['testing2'] ))
					echo "date reports by: $date_reports_by\n<br />";
			}
			
			if ($date_select_type != "auto") {
				$s_header .= " <select name='date_reports_by' onchange='window.location = \"$reporturl&date_reports_by=\" + this.value'>";
				$s_header .= "<option value='opened'" . ($date_reports_by == "opened" ? " selected" : "") . ">Report using orders OPENED on this date</option>";
				$s_header .= "<option value='closed'" . ($date_reports_by == "closed" ? " selected" : "") . ">Report using orders CLOSED on this date</option>";
				$s_header .= "</select>";
			}
		}
	}
	
	//
	$apply_stime = false;
	if ($locationid) {
		preg_match ( "/poslavu_(.*)_db/", $dbname, $data_name );
		lavu_connect_dn ( $data_name [1], $dbname );
		$location_query = lavu_query ( "select * from `$dbname`.`locations` where `id`='[1]'", $locationid );
		if (mysqli_num_rows ( $location_query )) {
			$location_read = mysqli_fetch_assoc ( $location_query );
			$stime = $location_read ['day_start_end_time'];
			if (is_numeric ( $stime )) {
				$stime_mins = $stime % 100;
				$stime_hours = ($stime - $stime_mins) / 100;
				if ($stime_hours < 10)
					$stime_hours = "0" . $stime_hours;
				if ($stime_mins < 10)
					$stime_mins = "0" . $stime_mins;
				$apply_stime = $stime_hours . ":" . $stime_mins . ":00";
			} else {
				$apply_stime = false;
			}
		}
	}
	
	// wrap it in a "header" tag (for processing later on)
	$s_header = '';
	$output = $s_header;
	
	if ($apply_stime) {
		$add_to_date_end = 1;
		if (($date_mode != "unified") || $explicit_date_end) {
			$add_to_date_end = 0;
		}
		$date_end_ts = mktime ( 0, 0, 0, substr ( $set_date_end, 5, 2 ), substr ( $set_date_end, 8, 2 ), substr ( $set_date_end, 0, 4 ) );
		$set_date_end = date ( "Y-m-d", mktime ( 0, 0, 0, date ( "m", $date_end_ts ), date ( "d", $date_end_ts ) + $add_to_date_end, date ( "Y", $date_end_ts ) ) );
		$set_time_start = $apply_stime;
		$set_time_end = $apply_stime;
	}
	if ($date_selector) {
		echo $s_header;
		$ret = array ();
		$ret ['date_start'] = $set_date_start;
		$ret ['time_start'] = $set_time_start;
		$ret ['date_end'] = $set_date_end;
		$ret ['time_end'] = $set_time_end;
		$ret ['date_reports_by'] = $date_reports_by;
		$ret ['locationid'] = $locationid;
		
		return $ret;
	}
	// if(isset($_GET['test'])) echo "STIME: $apply_stime ";
	
	//
	
	// reports that are actually a compilation of other reports
	// eg: reports --> general --> summary
	if ($rep_type == "Combo") {
		if ($report_read ['title'] == "Balance Summary") {
			$date_start = "$set_date_start $set_time_start";
			$date_end = "$set_date_end $set_time_end";
			
			// if(isset($_GET['testing1'])) echo "PROCESS FIX from $date_start to $date_end<br>";
			// process_fix_all($date_reports_by,$date_start,$date_end,$locationid);
		}
		
		echo $s_header;
		echo "<hr>";
		$rep_count = 0;
		$rep_query = mlavu_query ( "select `report_parts`.`prop1` as `prop1`, `report_parts`.`id` as `id`, `reports`.`title` as `title` from `$maindb`.`report_parts` LEFT JOIN `$maindb`.`reports` ON `report_parts`.`prop1`=`reports`.`id` where `report_parts`.`reportid`='$reportid' and `report_parts`.`type`='report' order by `_order` asc, id asc" );
		while ( $rep_read = mysqli_fetch_assoc ( $rep_query ) ) {
			if ($rep_count > 0)
				echo "<hr>";
			$date_str = $set_date_start . "|" . $set_time_start . "|" . $set_date_end . "|" . $set_time_end;
			$debug_combo_report = false; // (isset($_GET['test']))?true:false;
			$use_report_id = $rep_read ['prop1'];
			
			show_report ( $dbname, $maindb, "auto|" . $date_mode . "|" . $date_str, $rurl, $use_report_id, $debug_combo_report, $locationid, true );
			$rep_count ++;
		}
		return;
		
		// reports that are actually a compilation of other reports in some other weird way
	} else if ($repeat_foreach != "" && ! $use_subreport) {
		echo $s_header;
		echo "<hr>";
		$fe_parts = explode ( ".", $repeat_foreach );
		if (count ( $fe_parts ) > 1) {
			$fe_table = str_replace ( "`", "", $fe_parts [0] );
			$fe_table = str_replace ( "'", "", $fe_table );
			$fe_col = str_replace ( "`", "", $fe_parts [1] );
			$fe_col = str_replace ( "'", "", $fe_col );
			
			// get the row from report_parts containing the `date_table`.`date_column` to match dates to
			$where_code = "";
			$dt_query = mlavu_query ( "select * from `poslavu_MAIN_db`.`report_parts` where `id`='[1]'", $dateid );
			if (mysqli_num_rows ( $dt_query )) {
				$dt_read = mysqli_fetch_assoc ( $dt_query );
				$date_start = "$set_date_start $set_time_start";
				$date_end = "$set_date_end $set_time_end";
				$date_col = "`" . $dt_read ['tablename'] . "`.`" . $dt_read ['fieldname'] . "`";
				$where_code = " where $date_col >= '$date_start' and $date_col <= '$date_end'";
			}
			
			if (substr ( $fe_table, 0, 1 ) == '-') {
				$where_code = '';
				$fe_table = substr ( $fe_table, 1 );
			}
			preg_match ( "/poslavu_(.*)_db/", $dbname, $data_name );
			lavu_connect_dn ( $data_name [1], $dbname );
			$fe_query = lavu_query ( "select DISTINCT(`$fe_col`) as `fe_value` from `$dbname`.`$fe_table`" . $where_code );
			while ( $fe_read = mysqli_fetch_assoc ( $fe_query ) ) {
				$date_str = $set_date_start . "|" . $set_time_start . "|" . $set_date_end . "|" . $set_time_end;
				// echo "dbname: " . $dbname . "<br>maindb: " . $maindb . "<br>date_mode: " . $date_mode . "<br>date_str: " . $date_str . "<br>rurl: " . $rurl . "<br>reportid: " . $reportid . "<br>subreport: " . $fe_read['fe_value'] . "<br>locationid: " . $locationid . "<br>";
				show_report ( $dbname, $maindb, "auto|" . $date_mode . "|" . $date_str, $rurl, $reportid, "subreport:" . $fe_read ['fe_value'], $locationid, false );
			}
		}
		return;
	}
	
	//
	
	// old position of apply_stime and date_selector snippets
	
	//
	
	if ($set_unified == "Yes")
		$date_mode = "unified";
	else if ($set_unified == "No")
		$date_mode = "mixed";
		// Date Start and Date End are the fields that govern the time range that the reports should be pulled from.
	if ($date_mode == "unified") {
		$date_start = "$set_date_start $set_time_start";
		$date_end = "$set_date_end $set_time_end";
	} else {
		$date_start = "$set_date_start";
		$date_end = "$set_date_end";
		$time_start = $set_time_start;
		$time_end = $set_time_end;
	}
	
	// error_log(__FILE__ . " " . __LINE__ . ": Date Start ~" .$date_start."~");
	// error_log(__FILE__ . " " . __LINE__ . ": Date End ~" .$date_end."~");
	// error_log(__FILE__ . " " . __LINE__ . ": Time Start ~" .$time_start."~");
	// error_log(__FILE__ . " " . __LINE__ . ": Time End ~" .$time_end."~");
	
	if (! isset ( $locationid )) {
		/*
		 * if(isset(reqvar("loc_id")) $locationid = reqvar("loc_id");
		 * else if(isset(reqvar("locationid")) $locationid = reqvar("locationid");
		 * else
		 */
		$locationid = 0;
	}
	
	$join_after = array ();
	$tables_joined = array ();
	$rep_query = mlavu_query ( "select * from `$maindb`.`report_parts` where `reportid`='$reportid' order by `_order` asc, `id` asc" );
	$tableInfo = array ();
	$valuesArray = array ();
	$categoriesArray = array ();
	$included_snames = array ();
	// Added by Brian D. and Leif G.
	$specialFieldRows = array ();
	//
	// normal reports (only have one report per report)
	while ( $rep_read = mysqli_fetch_assoc ( $rep_query ) ) {
		
		$set_table = $rep_read ['tablename'];
		$set_field = $rep_read ['fieldname'];
		$set_prop1 = $rep_read ['prop1'];
		$set_prop2 = $rep_read ['prop2'];
		$set_prop3 = $rep_read ['prop3'];
		$set_prop4 = $rep_read ['prop4'];
		$set_type = $rep_read ['type'];
		$set_operation = $rep_read ['operation'];
		$set_opargs = $rep_read ['opargs'];
		$set_filter = $rep_read ['filter'];
		$set_label = $rep_read ['label'];
		$use_graph = $rep_read ['graph'];
		$use_branch = $rep_read ['branch'];
		$use_monetary = $rep_read ['monetary'];
		$use_sumop = $rep_read ['sum'];
		$use_align = $rep_read ['align'];
		$partid = $rep_read ['id'];
		$hide = ($rep_read ['hide'] == "1") ? true : false;
		
		if ($use_branch == "1") {
			$show_label = $set_label;
			if ($show_label == "")
				$show_label = $set_field;
			$sbranches [] = array (
					$set_table,
					$set_field,
					$partid,
					$show_label 
			);
		}
		
		if ($primary_table == "") {
			$primary_table = $set_table;
			$tables_joined [] = $set_table;
		} else
			$tables_joined [] = $primary_table;
		
		if ($set_type == "join") {
			$set_table_joined = false;
			$set_table2_joined = false;
			
			for($x = 0; $x < count ( $tables_joined ); $x ++) {
				if ($tables_joined [$x] == $set_table)
					$set_table_joined = true;
				if ($tables_joined [$x] == $set_prop1)
					$set_table2_joined = true;
			}
			if ($set_table_joined) {
				$set_prop_joined = false;
				for($x = 0; $x < count ( $tables_joined ); $x ++) {
					if ($tables_joined [$x] == $set_prop1)
						$set_prop_joined = true;
				}
				if (! empty ( $set_prop3 ) && ! empty ( $set_prop4 )) {
					$fcode = $set_prop3 . "[DBNAME]." . $set_prop4 . " `$set_prop1`";
				} else if ($set_prop_joined) {
					$fcode = "`$set_prop1`";
				} else {
					$tables_joined [] = $set_prop1;
					$fcode = from_table ( $set_prop1, "[DBNAME]" );
				}
				$join_code .= " LEFT JOIN " . $fcode . " ON `$set_table`.`$set_field` = `$set_prop1`.`$set_prop2`";
			} else if ($set_table2_joined) {
				$tables_joined [] = $set_table;
				$fcode = from_table ( $set_table, "[DBNAME]" );
				$join_code .= " LEFT JOIN " . $fcode . " ON `$set_prop1`.`$set_prop2` = `$set_table`.`$set_field`";
			} else {
				// $tables_joined[] = $set_table;
				$fcode = from_table ( $set_table, "[DBNAME]" );
				$join_after [$set_prop1] = " LEFT JOIN " . $fcode . " ON `$set_prop1`.`$set_prop2` = `$set_table`.`$set_field`";
				/*
				 * if(isset($_GET['test']))
				 * {
				 * echo "<br>join after $set_prop1 - " . $join_after[$set_prop1] . "<br>";
				 * }
				 */
			}
			if (isset ( $join_after [$set_table] ) && $join_after [$set_table] != "") {
				$tables_joined [] = $set_table;
				$join_code .= $join_after [$set_table];
				$join_after [$set_table] = "";
			}
		} else if ($set_type == "select") {
			$sname = "`$set_table`.`$set_field`";
			
			if ($dbname == "poslavu_test_pizza_sho_db")
				error_log ( "sname: " . $sname );
			
			$alias_list [] = array (
					$sname,
					"`field" . $scount . "`" 
			);
			
			$prime_sname = $sname;
			if ($set_filter != "") {
				if ($where_code != "")
					$where_code .= " and ";
				$set_filter = str_replace ( "'", "''", $set_filter );
				$where_code .= ast_replace ( $sname, $set_filter ); // str_replace("*",$sname,$set_filter);
			}
			if ($dateid == $partid) {
				$date_sname = $sname;
				if ($set_field == "opened" || $set_field == "closed") {
					$date_sname = "`$set_table`.`$date_reports_by`";
				}
				
				if ($where_code != "")
					$where_code .= " and ";
				if (isset ( $_GET ['testing'] ))
					echo "date_sname: $date_sname - $date_reports_by<br>";
				$where_code .= "$date_sname >= '$date_start' and $date_sname <= '$date_end'";
			}
			if ($timeid == $partid) {
				if ($where_code != "")
					$where_code .= " and ";
				$where_code .= "$sname >= '$time_start' and $sname <= '$time_end'";
			}
			if ($locid == $partid && $showing_multi_report !== '-1') {
				if ($where_code != "")
					$where_code .= " and ";
				$where_code .= "$sname = '$locationid'";
			}
			if ($set_operation != "") {
				if ($set_operation == "custom") {
					if (substr ( $set_opargs, 0, 12 ) == "[get_fields:") {
						
						// error_log("opargs: ".$set_opargs);
						$gf_parts = explode ( ":", trim ( $set_opargs, "[]" ) );
						// error_log(print_r($get_field_parts, true));
						
						if ($gf_parts [1] == "config") {
							preg_match ( "/poslavu_(.*)_db/", $dbname, $data_name );
							lavu_connect_dn ( $data_name [1], $dbname );
							$gf = lavu_query ( "SELECT `value`, `value2`, `value4`, `type` FROM `$dbname`.`config` WHERE `setting` = '[1]' AND `location` = '[2]' AND (`value10` * 1) > '2' AND `value11` = '[3]' ORDER BY `value10` ASC", $gf_parts [2], $locationid, $reportid );
							$gf_count = mysqli_num_rows ( $gf );
							
							$cnt = 0;
							while ( $gf_info = mysqli_fetch_assoc ( $gf ) ) {
								// error_log(print_r($gf_info, true));
								
								$cnt ++;
								$sname = "`$set_table`.`" . $gf_info ['value2'] . "`";
								
								$hide = false;
								$set_label = ucwords ( str_replace ( "_", " ", $gf_info ['value'] ) );
								$use_monetary = "0";
								$use_sumop = "0";
								$use_graph = "0";
								$use_align = (($gf_info ['value4'] == "number") ? "center" : "left");
								$ssum [] = 0;
								
								if ($cnt < $gf_count) {
									if ($select_code != "")
										$select_code .= ",";
									$select_code .= "`$set_table`.`" . $gf_info ['value2'] . "` AS `field$scount`";
									$scount ++;
									$sdisplay [] = ! $hide;
									$stitles [] = $set_label;
									$smonetary [] = "0";
									$ssumop [] = "0";
									$sgraph [] = "0";
									$salign [] = $use_align;
									$ssum [] = 0;
								}
							}
						}
					} else if ($set_opargs != "")
						$sname = ast_replace ( $sname, $set_opargs ); // str_replace("*",$sname,$set_opargs);
				} else {
					$sname = strtoupper ( $set_operation ) . "(" . $sname;
					if ($set_opargs != "") {
						$set_opargs = str_replace ( "'", "''", $set_opargs );
						$sname .= "," . $set_opargs;
					}
					$sname .= ")";
				}
			}
			
			if ($branchto != "") {
				if ($branchto == $partid) {
					/*
					 * if($group_operation && $group_operation!="")
					 * $groupby = " group by " . ast_replace($prime_sname,$group_operation);//str_replace("*",$prime_sname,$group_operation);
					 * else
					 */
					$groupby = " group by `field$scount` ";
					$change_group_to = count ( $stitles );
					if (isset ( $_GET ['test3'] ))
						echo "CHANGE GROUP TO: $change_group_to<br>";
				} else if ($groupid == $partid) {
					$change_group_from = count ( $stitles );
					if (isset ( $_GET ['test3'] ))
						echo "CHANGE GROUP FROM: $change_group_from<br>";
				}
			} else {
				if ($groupid == $partid) {
					if ($group_operation && $group_operation != "")
						$groupby = " group by " . ast_replace ( $prime_sname, $group_operation ); // str_replace("*",$prime_sname,$group_operation);
					else
						$groupby = " group by `field$scount` ";
					$change_group_id = count ( $stitles );
				}
			}
			
			if ($branch_col == "field" . $scount) {
				$branch_fieldname = $sname;
			}
			
			$sdisplay [] = ! $hide;
			if ($set_label != "")
				$stitles [] = $set_label;
			else
				$stitles [] = $set_field;
			$smonetary [] = $use_monetary;
			$ssumop [] = $use_sumop;
			$sgraph [] = $use_graph;
			$salign [] = $use_align;
			$ssum [] = 0;
			
			if ($select_code != "")
				$select_code .= ",";
			$select_code .= "$sname AS `field$scount`";
			$included_snames [] = $sname;
			
			// -1 is "all locations for this restaurant"
			if (($showing_multi_report && (! $b_draw_multi_locations || ! $b_separate_locations)) && $showing_multi_report !== '-1') {
				if (! isset ( $fields_used ))
					$fields_used = array ();
				if (! isset ( $fields_used [$set_field] )) {
					$select_code .= ",$sname AS `$set_field`";
					$fields_used [$set_field] = 1;
				}
			}
			
			if ($select_outer_code != "")
				$select_outer_code .= ",";
			if (strpos ( strtolower ( $sname ), "sum(" ) !== false || strpos ( strtolower ( $sname ), "count(" ) !== false)
				$select_outer_code .= "sum(`innerquery`.`field$scount`) AS `field$scount`";
			else
				$select_outer_code .= "`innerquery`.`field$scount` AS `field$scount`";
			
			if ($orderby_id == $partid) {
				if ($order_operation && $order_operation != "")
					$orderby = " order by " . ast_replace ( $prime_sname, $order_operation ) . " " . $orderby_dir;
				else
					$orderby = " order by `field$scount` " . $orderby_dir;
			}
			$scount ++;
		}  // Added by Brian D. and Leif G.
else if ($set_type == 'special') {
			$specialFieldRows [] = $rep_read;
		}
		//
	} // End while loop that pulls report parts one at a time.
	foreach ( $specialFieldRows as $currSpecialField ) {
		$stitles [] = $currSpecialField ['label'];
		$scount ++;
	}
	
	if ($repeat_foreach != "" && $use_subreport) {
		$fe_parts = explode ( ".", $repeat_foreach );
		if (count ( $fe_parts ) > 1) {
			$fe_table = str_replace ( "`", "", $fe_parts [0] );
			$fe_table = str_replace ( "'", "", $fe_table );
			$fe_table = str_replace ( "-", "", $fe_table );
			$fe_col = str_replace ( "`", "", $fe_parts [1] );
			$fe_col = str_replace ( "'", "", $fe_col );
			$fe_value = str_replace ( "'", "''", $subreport );
			
			if ($where_code != "")
				$where_code .= " and ";
			$where_code .= "`$fe_table`.`$fe_col` = '$fe_value'";
		}
	}
	if ($branchto != "" && $branch_col != "") {
		$branch_title = str_replace ( "'", "''", $branch_title );
		if ($where_code != "")
			$where_code .= " and ";
		$where_code .= "$branch_fieldname = '$branch_title'";
	}
	if ($where_code != "")
		$where_code = "where " . $where_code;
	
	$process_ag_data = false;
	if (strpos ( $select_code . " " . $where_code, "ag_cash" ) || strpos ( $select_code . " " . $where_code, "ag_card" ))
		$process_ag_data = true;
	if ($process_ag_data) {
		lavu_select_db ( $dbname );
		if (isset ( $_GET ['view_ag_process'] ))
			echo "View AG process:<br>";
		process_ag_data ( $date_reports_by, $date_start, $date_end, $locationid );
	}
	if (isset ( $_GET ['explain'] )) {
		process_explain_mismatches ( $date_reports_by, $date_start, $date_end, $locationid );
	}
	
	// show reports from multiple locations
	// -1 is "all locations for this restaurant"
	if ($showing_multi_report && $showing_multi_report !== '-1') {
		$inner_selects = "";
		$multi_db_list = array (
				$dbname 
		);
		
		if (count ( $multi_location_ids ) > 0) {
			$multi_db_list = array ();
			$multi_ids = $multi_location_ids;
			
			foreach ( $multi_ids as $n => $v ) {
				$mid = trim ( $multi_ids [$n] );
				$mrest_query = mlavu_query ( "select `data_name`,`id` from `poslavu_MAIN_db`.`restaurants` where `id`='[1]'", $mid );
				if (mysqli_num_rows ( $mrest_query )) {
					$mrest_read = mysqli_fetch_assoc ( $mrest_query );
					$multi_db_list [] = "poslavu_" . $mrest_read ['data_name'] . "_db";
				}
			}
			if (count ( $multi_db_list ) < 1)
				$multi_db_list = array (
						$dbname 
				);
			
			$inner_selects = "";
			for($n = 0; $n < count ( $multi_db_list ); $n ++) {
				$multi_dbname = $multi_db_list [$n];
				
				$sel1_code = "select '[DBNAME]' as `cc`, " . $select_code . " from " . from_table ( $primary_table, "[DBNAME]" ) . $join_code . $where_code . $groupby;
				$sel1_code = str_replace ( "[DBNAME]", $multi_dbname, $sel1_code );
				
				if ($inner_selects != "")
					$inner_selects .= " UNION ALL ";
				$inner_selects .= "(" . $sel1_code . ")";
			}
			if ($showing_multi_report) {
				$query_start = "select `innerquery`.`cc` as `cc`, $select_outer_code from (" . $inner_selects . ") AS `innerquery`";
				if (( int ) $reportid == 148) {
					$selectQuery = " select count( distinct concat(`field16`,`field15`)) as totalCount from (" . $inner_selects . ") AS `innerquery`";

					// $pages = getPaginationInfo($selectQuery, $_GET['ipp'], $_GET['page']);
					// an AJAX request.
					$isAjaxRequest = false;
					$noOfRecordsPerPage = $_GET ['ipp'];
					$pageNumber = $_GET ['page'];

					// IF HTTP_X_REQUESTED_WITH is equal to xmlhttprequest
					if (isset ( $_SERVER ['HTTP_X_REQUESTED_WITH'] ) && strcasecmp ( $_SERVER ['HTTP_X_REQUESTED_WITH'], 'xmlhttprequest' ) == 0) {
						// Set $isAjaxRequest to true.
						$isAjaxRequest = true;
					}

					// set the required variables if it is a ajax request
					if ($isAjaxRequest) {
						if (isset ( $_GET ['ipp'] ) && isset ( $_GET ['page'] )) {
							$noOfRecordsPerPage = $noOfRecordsPerPage * $pageNumber;
							$pageNumber = 1;
						}
					}

					$pages = getPaginationInfo ( $selectQuery, $noOfRecordsPerPage, $pageNumber );

					if ($isAjaxRequest) {
						$skipOffsetNumberForShowingRecords = ($_GET ['page'] * $_GET ['ipp']) - $_GET ['ipp'];
					}
				}
			} else {
				$query_start = "select $select_outer_code from (" . $inner_selects . ") AS `innerquery`";
			}
		}
	} else {
		if (( int ) $reportid == 148) {
			$selectQuery = " select count( distinct concat(`order_contents`.`item_id`,`order_contents`.`options`)) as totalCount from " . from_table ( $primary_table, "[DBNAME]" ) . $join_code . $where_code;
			$selectQuery = str_replace ( "[DBNAME]", $dbname, $selectQuery );

			// an AJAX request.
			$isAjaxRequest = false;
			$noOfRecordsPerPage = $_GET ['ipp'];
			$pageNumber = $_GET ['page'];

			// IF HTTP_X_REQUESTED_WITH is equal to xmlhttprequest
			if (isset ( $_SERVER ['HTTP_X_REQUESTED_WITH'] ) && strcasecmp ( $_SERVER ['HTTP_X_REQUESTED_WITH'], 'xmlhttprequest' ) == 0) {
				// Set $isAjaxRequest to true.
				$isAjaxRequest = true;
			}

			// set the required variables if it is a ajax request
			if ($isAjaxRequest) {
				if (isset ( $_GET ['ipp'] ) && isset ( $_GET ['page'] )) {
					$noOfRecordsPerPage = $noOfRecordsPerPage * $pageNumber;
					$pageNumber = 1;
				}
			}

			$pages = getPaginationInfo ( $selectQuery, $noOfRecordsPerPage, $pageNumber );

			if ($isAjaxRequest) {
				$skipOffsetNumberForShowingRecords = ($_GET ['page'] * $_GET ['ipp']) - $_GET ['ipp'];
			}
		}
		$query_start = "select SQL_NO_CACHE $select_code from " . from_table ( $primary_table, "[DBNAME]" ) . $join_code . $where_code;
		$query_start = str_replace ( "[DBNAME]", $dbname, $query_start );
	}
	
	if ($b_draw_multi_locations && $b_separate_locations) {
		$query_start = "select SQL_NO_CACHE $select_code from " . from_table ( $primary_table, "[DBNAME]" ) . $join_code . $where_code;
		$query_start = str_replace ( "[DBNAME]", $dbname, $query_start );
	}
	
	// ---DEBUG---
	
	// $query_start = "select SQL_NO_CACHE $select_code from ".from_table($primary_table).$join_code.$where_code;
	$query_start = str_replace ( "(selected_date)", $setdate, $query_start );
	$set_stime = $stime;
	$stime_mins = $set_stime % 100;
	$stime_hours = ($set_stime - $stime_mins) / 100;
	if ($stime_hours < 10)
		$stime_hours = "0" . $stime_hours;
	if ($stime_mins < 10)
		$stime_mins = "0" . $stime_mins;
	$set_stime = $stime_hours . ":" . $stime_mins . ":00";
	$query_start = str_replace ( "(day_start_end)", $set_stime, $query_start );
	$groupby = str_replace ( "(day_start_end)", $set_stime, $groupby );
	$query_start = str_replace ( "(open_close)", $date_reports_by, $query_start );
	$groupby = str_replace ( "(open_close)", $date_reports_by, $groupby );
	$sdate_parts = explode ( "-", $setdate );
	if (count ( $sdate_parts ) > 2) {
		$nextday = date ( "Y-m-d", mktime ( 0, 0, 0, $sdate_parts [1], $sdate_parts [2] + 1, $sdate_parts [0] ) );
	} else
		$nextday = "";
	$query_start = str_replace ( "(next_day)", $nextday, $query_start );
	
	// Added by leif for special queries
	// error_log(__FILE__ . " " . __LINE__ . "Group By: " . $groupby);
	preg_match ( "~field\\d+~", $groupby, $groupByForSpecialQueries );
	$groupByForSpecialQueries = $groupByForSpecialQueries [0];
	// end of added code.
	
	if (isset ( $_GET ['test2'] ))
		echo "(selected_date) = $setdate<br>(next_day) = $nextday<br>(day_start_end) = $set_stime<br>";
	
	$orderby = '';
	
	if (isset ( $_GET ['order_by_col'] ) && $_GET ['order_by_col'] != '') {
		$orderby = "order by `" . $_GET ['order_by_col'] . "` " . $_GET ['ordermode'] . " ";
		if ($_GET ['ordermode'] == 'asc')
			$_GET ['ordermode'] = 'desc';
		else
			$_GET ['ordermode'] = 'asc';
	}
	
	for($n = 0; $n < count ( $alias_list ); $n ++) {
		if (($showing_multi_report && (! $b_draw_multi_locations || ! $b_separate_locations)) && $showing_multi_report !== '-1') {
			$orderby = str_replace ( $alias_list [$n] [0], $alias_list [$n] [1], $orderby );
			$groupby = str_replace ( $alias_list [$n] [0], $alias_list [$n] [1], $groupby );
		}
	}
	
	if (( int ) $reportid == 148) {
		$query_start .= 'and `order_contents`.`combo_id` != 1 and `order_contents`.`is_combo`=0 '; 
		$query = $query_start . $groupby . $orderby . $pages->limit;
	} else {
		$query = $query_start . $groupby . $orderby;
	}
	
	if ($return_query) {
		for($i = ($scount - 1); $i >= 0; $i --) {
			$query = str_replace ( 'field' . $i, $stitles [$i], $query );
		}
		return $query;
	}
	// ////////////////////////////////////////////////////
	// draw the table header row
	if ($change_group_from == "")
		$change_group_from = 999999;
	if ($debug)
		$s_body .= $query . "<br><br>";
	lavu_select_db ( $dbname );
	
	if (( int ) $reportid == 148 && $pages->items_total > 0) {
		// $s_body .= displayPagination ( $pages );
	}
	$table_class = "";
	// $s_body .= "<table cellpadding=4 {$table_class}>";
	// $s_body .= "<tr>";
	
	foreach ( $specialFieldRows as $currSpecialFieldReportPart ) {
		$stitles [] = $currSpecialFieldReportPart ["label"];
		$sdisplay [] = "1";
	}
	//
	
	for($i = 0; $i < count ( $stitles ); $i ++) {
		if ($sgraph [$i] && $sdisplay [$i]) {
			if (isset ( $_GET ['nialls_test'] ))
				echo print_r ( $sdisplay ['i'], 1 );
			
			array_push ( $categoriesArray, $stitles [$i] );
		}
		if ($sdisplay [$i]) {
			$export_output .= $stitles [$i] . $export_col;
		}
	}
	
	if (trim ( $extra_functions ) != "" && $show_extra_functions) {
		// extra_functions
		require_once (dirname ( __FILE__ ) . "/report_extra_functions.php");
		
		$extra_function_list = explode ( ",", $extra_functions );
		for($x = 0; $x < count ( $extra_function_list ); $x ++) {
			$function_name = trim ( $extra_function_list [$x] );
			
			$s_body .= "<td syle='border-bottom:solid 1px black;' align='center'><b><font>";
			$s_body .= run_extra_report_function ( $function_name, false, false, "title" );
			$s_body .= "</td>";
		}
	}
	// $s_body .= "</tr>";
	$graph_vals = array ();
	
	// must use mlavu query if accessing more than just the current account
	// -1 is "all locations for this restaurant"
	if ($showing_multi_report === FALSE || $showing_multi_report !== '-1')
		$result_query = lavu_query ( $query );
	if ($result_query === FALSE)
		$result_query = mlavu_query ( $query ); // use_mlavu
	
	$specialParts = setBranchingVarsForSpecialReportParts ( $reportid, $currSpecialFieldReportPart, $groupByForSpecialQueries );
	// Build the query rows for normal queries and put them in an array.
	$fullyBuiltQueryResultRowsArray = buildOriginalQueryArray ( $result_query );
	
	$fullyBuiltQueryResultRowsArray = performSpecialQueryPullSpecialResultsAndAppendToOriginalQueryResult ( $fullyBuiltQueryResultRowsArray, $specialFieldRows, $reportid, $date_start, $date_end, $specialParts, $dbname );
	$smonetary [] = "1"; // This will only work for one special query so if you're implementing multiple queries you'll have to deal with changing this.
	$ssumop [] = "1"; // This will only work for one special query so if you're implementing multiple queries you'll have to deal with changing this.
	$salign [] = "center"; // This will only work for one special query so if you're implementing multiple queries you'll have to deal with changing this.
	                       // /End adding stuff...
	if ($result_query !== FALSE && $result_query !== NULL && mysqli_num_rows ( $result_query ) && $skipOffsetNumberForShowingRecords < count ( $fullyBuiltQueryResultRowsArray )) {
		$j = 1;
		foreach ( $fullyBuiltQueryResultRowsArray as $result_read ) {
			$export_output = substr ( $export_output, 0, - 1 ) . $export_row;

			// start of display record
			if ($j > $skipOffsetNumberForShowingRecords) {
				$s_body .= "<tr>";
			}
			for($i = 0; $i < $scount; $i ++) {
				if ($sdisplay [$i]) {
					// get the value of this column
					if ($change_group_from == $i) {
						$show_colval = $result_read ['field' . $change_group_to];
					} else {
						$show_colval = $result_read ['field' . $i];
					}
					$ex_colval = $show_colval;
					
					if ($smonetary [$i] == "1") {
						$ex_colval = display_money ( $show_colval, $location_read, "" );
						$show_colval = display_money ( $show_colval, $location_read );
					} else if (is_numeric ( $show_colval )) {
						$disable_decimal = isset ( $location_read ['disable_decimal'] ) ? $location_read ['disable_decimal'] : 2;
						$show_colval = round ( $show_colval * 1, $disable_decimal );
					}
					
					if ($export_ext == 'csv' && (strstr ( $ex_colval, '"' ) || strstr ( $ex_colval, "\n" ) || strstr ( $ex_colval, "\r" ) || strstr ( $ex_colval, "," ))) {
						$ex_colval = str_ireplace ( '"', "\\\"", $ex_colval );
						$ex_colval = "\"{$ex_colval}\"";
					}
					if ($sgraph [$i]) {
						if ($b_draw_pretty_graphs) {
							$s_tableinfo = str_replace ( array (
									$location_read ['monitary_symbol'],
									' ' 
							), '', $ex_colval );
							$s_tableinfo = str_replace ( array (
									$location_read ['decimal_char'] 
							), '.', $s_tableinfo );
							if (preg_match ( '/-?[0-9]+\.[0-9]+/', $s_tableinfo ) === 1)
								$s_tableinfo = ( float ) $s_tableinfo;
							else if (preg_match ( '/-?[0-9]+/', $s_tableinfo ) === 1)
								$s_tableinfo = ( int ) $s_tableinfo;
						} else {
							$s_tableinfo = str_replace ( array (
									$location_read ['monitary_symbol'],
									' ',
									$location_read ['decimal_char'] 
							), '', $ex_colval );
						}
						array_push ( $tableInfo, $s_tableinfo );
					}
					
					// export code
					$export_output .= $ex_colval . $export_col;
					
					// add it to the body
					if ($j > ( int ) $skipOffsetNumberForShowingRecords) {
						$s_body .= "<td";
						if ((($stitles [$i] == "order_id" || strtolower ( $stitles [$i] ) == "order id")) && ($result_read ['field' . $i] != "") && (substr ( $result_read ['field' . $i], 0, 4 ) != "Paid") && ($result_read ['field' . $i] != "0"))
							$s_body .= " style='padding-left:12px; padding-right:12px; border-bottom:solid 1px #999999; color:#000088;' onclick='showWhiteBox(\"Order ID: " . $result_read ['field' . $i] . "<br><iframe src=\\\"index.php?show_page=reports/order_details&order_id=" . $result_read ['field' . $i] . "\\\" width=\\\"100%\\\" height=\\\"100%\\\" frameborder=\\\"0\\\"></iframe>\")'";
						else
							$s_body .= " style='padding-left:12px; padding-right:12px; border-bottom:solid 1px #999999'";
						$s_body .= " align='" . $salign [$i] . "'>";
						if (($show_colval == "") || ((($stitles [$i] == "order_id") || strtolower ( $stitles [$i] ) == "order id") && (($result_read ['field' . $i] == "") || ($result_read ['field' . $i] == "0"))))
							$s_body .= "&nbsp;";
						else
							$s_body .= $show_colval;
						$s_body .= "</td>";
					}
					
					if ($ssumop [$i] == "1" || $ssumop [$i] == "2") {
						if (strpos ( $result_read ['field' . $i], "$" ) !== false)
							$ssumop [$i] = "2";
						
						$addnum = $result_read ['field' . $i];
						if ($location_read ['decimal_char'] == ",") {
							$addnum = str_replace ( ",", ".", $addnum );
						} else {
							$addnum = str_replace ( ",", "", $addnum );
						}
						$addnum = str_replace ( "$", "", $addnum );
						
						$ssum [$i] = $ssum [$i] * 1 + ($addnum * 1);
						if (isset ( $_GET ['poopies'] )) {
							echo "sum: + " . $result_read ['field' . $i] . " = " . $ssum [$i] . "<br>";
						}
					}
					if ($sgraph [$i] == "1") {
						$graph_vals [] = (str_replace ( "$", "", str_replace ( ",", "", $result_read ['field' . $i] ) ) * 1);
					}
				}
			}
			
			if ($j > ( int ) $skipOffsetNumberForShowingRecords) {
				if (trim ( $extra_functions ) != "" && $show_extra_functions) {
					// extra_functions
					require_once (dirname ( __FILE__ ) . "/report_extra_functions.php");
					
					$extra_function_list = explode ( ",", $extra_functions );
					for($x = 0; $x < count ( $extra_function_list ); $x ++) {
						$function_name = trim ( $extra_function_list [$x] );
						
						$s_body .= "<td style='padding-left:12px; padding-right:12px; border-bottom:solid 1px #999999' align='center'>";
						$s_body .= run_extra_report_function ( $function_name, $result_read, $stitles );
						$s_body .= "</td>";
					}
				}
				
				if ($change_group_id != "") {
					if ($rep_type != "SubReport") {
						for($i = 0; $i < count ( $sbranches ); $i ++) {
							if ($j > ( int ) $skipOffsetNumberForShowingRecords) {
								$s_body .= "<td><a href='$reporturl&setdate=$setdate&stime=$stime&day_duration=$day_duration&branch=" . $sbranches [$i] [2] . "&branch_title=" . urlencode ( $result_read ['field' . $change_group_id] ) . "&branch_col=field" . $change_group_id . "'>(" . $sbranches [$i] [3] . ")</a></td>";
							}
						}
					}
				}
			}

			// end of display record
			if ($j > ( int ) $skipOffsetNumberForShowingRecords) {
				$s_body .= "</tr>";
			}

			$j ++;
		}
		$export_output = substr ( $export_output, 0, - 1 );
		$export_totals = "";

		for($i = 0; $i < $scount; $i ++) {
			if ($sdisplay [$i]) {
				if ($export_totals != "") {
					$export_totals .= $export_col;
				}
				$s_body .= "<td";
				if ($smonetary [$i] == "1" && $ssumop [$i] == "1") {
					$s_body .= " align='" . $salign [$i] . "'><b>" . display_money ( $ssum [$i], $location_read ) . "</b>";
					$temp = display_money ( $ssum [$i], $location_read );
					if ($export_ext == "csv" && strstr ( $temp, "," ))
						$export_totals .= '"' . $temp . '"';
					else
						$export_totals .= $temp;
				} else if ($ssumop [$i] == "1") {
					$s_body .= " align='" . $salign [$i] . "'><b>" . $ssum [$i] . "</b>";
					// $export_totals .= $ssum[$i];
					$temp = $ssum [$i];
					if ($export_ext == "csv" && strstr ( $temp, "," ))
						$export_totals .= '"' . $temp . '"';
					else
						$export_totals .= $temp;
				} else if ($ssumop [$i] == "2") {
					$s_body .= " align='" . $salign [$i] . "'><b>$" . number_format ( $ssum [$i], 2, ".", "," ) . "</b>";
					$temp = number_format ( $ssum [$i], 2, ".", "," );
					if ($export_ext == "csv" && strstr ( $temp, "," ))
						$export_totals .= '"' . $temp . '"';
					else
						$export_totals .= $temp;
				} else {
					$s_body .= ">&nbsp;";
					$export_totals .= " ";
				}
				$s_body .= "</td>";
			}
		}
		$s_body .= "</tr>";
		$export_output .= $export_row . $export_totals;
	} else {
		$s_body .= "<tr>";
		$s_body .= "<td colspan='$scount' align='center'>No Records found</td>";
		$s_body .= "</tr>";
	}
	// $s_body .= "</table>";
	
	if (( int ) $reportid == 148 && $pages->items_total > 0) {
		// $s_body .= displayPagination ( $pages );
	}
	
	$output .= $s_body;
	
	// give the user the option to export this report
	if ($rep_type != "SubReport") {
		
		if (count ( $tableInfo ) > 0 && $b_draw_pretty_graphs) {
			$s_graph .= "<br /><div id='new_graph' style='width:600px;height:300px; margin:0 auto; display:none;'></div>";
			$s_chart_retval = chart ( $categoriesArray, $tableInfo, false );
			$s_graph .= $s_chart_retval;
			$s_graph .= "<br />";
		} else {
			
			if (count ( $graph_vals ) > 0) {
				$graph_val_str = "";
				for($i = 0; $i < count ( $graph_vals ); $i ++) {
					if ($graph_val_str != "")
						$graph_val_str .= ",";
					$graph_val_str .= $graph_vals [$i];
				}
				// $s_footer .= "<img src='resources/show_graph.php?width=480&height=360&data=" . $graph_val_str . "'>";
			}
		}
		
		if ($showing_multi_report)
			$mrcode = "&multireport=" . $showing_multi_report;
		else
			$mrcode = "";
		
		$query_str = $_SERVER ['QUERY_STRING'];
		
		$s_footer .= "<br><br><a href='index.php?{$query_str}&reportid={$reportid}&export_type=txt&widget=reports/export";
		if (isset ( $_GET ['order_by_col'] ))
			$s_footer .= "&order_by_col=" . $_GET ['order_by_col'];
		$s_footer .= "'>Export To Tab Delimited \".txt\" file</a>";
		
		$s_footer .= "<br><br><a href='index.php?{$query_str}&reportid={$reportid}&export_type=xls&widget=reports/export";
		if (isset ( $_GET ['order_by_col'] ))
			$s_footer .= "&order_by_col=" . $_GET ['order_by_col'];
		$s_footer .= "'>Export To Tab Delimited \".xls\" file</a>";
		
		$s_footer .= "<br><br><a href='index.php?{$query_str}&reportid={$reportid}&export_type=csv&widget=reports/export";
		if (isset ( $_GET ['order_by_col'] ))
			$s_footer .= "&order_by_col=" . $_GET ['order_by_col'];
		$s_footer .= "'>Export To Comma Delimited \".csv\" file</a>";
		$s_footer .= "<br><br><br><br>";
	}
	$s_footer = '';
	$output = $output;
	$output .= $s_footer;
	
	$output = json_encode ( array (
			'content' => $output,
			'graph' => $s_graph
	) );
	
	$b_do_the_multi_report_thing = ($showing_multi_report && $showing_multi_report !== '-1' && count ( $multi_db_list ) > 1 && ! $b_second_part_of_multi_report && ($b_draw_multi_locations && $b_separate_locations));
	
	// export the report
	if ($export) {
		export_report ( "report_" . date ( "Y-m-d H:i" ) . "." . $export_ext, $export_output );
	} else {
		if ($b_second_part_of_multi_report || $b_do_the_multi_report_thing) {
			if (! class_exists ( "MAIN_DB_Interface" )) {
			}
			$a_locations = ConnectionHub::getConn ( 'rest' )->getDBAPI ()->getAllInTable ( 'locations', NULL, TRUE, array (
					'databasename' => $dbname,
					'selectclause' => '`title` AS `location_name`' 
			) );
			$s_location_name = $a_locations [0] ['location_name'];
			$s_draw_dataname = (function_exists ( 'is_lavu_admin' ) && is_lavu_admin ()) ? "<font style='font-weight:bold;'>dataname: $cr_dataname</font> (only visible to lavu admins)<br />" : "";
			if (! $b_second_part_of_multi_report)
				echo "<br />$s_header<br /><br /><h2>$s_location_name</h2>$s_draw_dataname<br />$s_body<br />";
			else
				echo "<h2>$s_location_name</h2>$s_draw_dataname<br />$s_body<br />";
		} else {
			echo "" . $output;
		}
	}
	
	// draw the report for other restaurants in the chain reporting group
	// don't draw the header and footer
	if ($b_do_the_multi_report_thing) {
		foreach ( $multi_db_list as $s_database ) {
			if ($dbname == $s_database)
				continue;
				// show_report($dbname,$maindb,$date_mode,$rurl,$reportid,$special=false,$set_locationid=false,$is_combo_rep=false,$return_query=false,$b_second_part_of_multi_report=FALSE);
			ajax_show_report ( $s_database, $maindb, $date_mode, $rurl, $reportid, $special, $set_locationid, $is_combo_rep, $return_query, TRUE );
		}
		echo $s_footer;
	}
}

$lavu_minimum_login_level = 3;

$template_dir = "";
$content_dir = "";
$content_status = "";
$content = "";
$location_gateway = "";
$cpcode = "";
$menu_mode = "";
$locSelector = "";

$location_info = array ();
$package_result = array ();

$in_lavu = true;
$suppress_billing_alert = false;
$loggedin_is_distro = false;
$reload_language_pack = false;
$has_LLS = false;
$use_special_menu = false;
$package_component = NULL;
$in_backend = true;
$first_login = false;

$mode = urlvar ( "mode" );
$submode = urlvar ( "submode", 1 );
$action = urlvar ( "action" );
$parent_mode = urlvar ( "parent_mode" );
$parent_submode = urlvar ( "parent_submode" );

// load global information
$rdb = admin_info ( "database" );
$loggedin_id = admin_info ( "loggedin" );
$data_name = sessvar ( "admin_dataname" );
$locationid = check_location_id ( $rdb, sessvar ( "locationid" ) );

$got_location_id = $locationid;

require_once (resource_path () . "/chain_functions.php");
if (isset ( $_GET ['locationid'] ) && $loggedin_id) {
	verifyCanAccessRestaurantID ( $data_name, admin_info ( 'dataname' ), $_GET ['locationid'] );
	checkSetGroupid ( $data_name, $_GET ['locationid'] ); // updates the group id if the location id has changed
}

if (isset ( $_GET ['locationid'] )) {
	$req_locationid = $_GET ['locationid'];
} else {
	$req_locationid = $locationid;
}

$section = "";
if ($mode != "billing") {
	$section_end_index = strpos ( $mode, '_' );
	$section = substr ( $mode, 0, $section_end_index );
	$mode = substr ( $mode, $section_end_index + 1 );
} else {
	$section = $mode;
}

$reload_language_pack = ($mode == "location_settings" || $mode == "custom_language");

$locSelector = getChainLocationSelector ( $req_locationid, $reload_language_pack, $location_info );

$location_info = sessvar ( "location_info" );
$comPackage = $location_info ['component_package_code'];

$maindb = "poslavu_MAIN_db";

$_SESSION ['last_data_name'] = $data_name;

// ***** reset these in case of chain location change *****
$rdb = admin_info ( "database" );
$loggedin_id = admin_info ( "loggedin" );
$data_name = sessvar ( "admin_dataname" );
$locationid = sessvar ( "locationid" );
$got_location_id = $locationid;

/**
 * *************************** DB CONNECTS *******************************
 */
lavu_auto_login ();
lavu_connect_byid ( admin_info ( "companyid" ), $rdb );
/**
 * ************************** END DB CONNECTS ****************************
 */

require_once (__DIR__ . "/resources/log_process_info.php");
lavu_write_process_info ( array () );

appVersionMinMax ( $locationid, "POSLavu Client", $min_POSLavu_version, $max_POSLavu_version ); // returns minimum and maximum version found in devices table
$modules = getModules ();

$req_locationid = $locationid;
if (isset ( $_GET ['locationid'] )) {
	$req_locationid = $_GET ['locationid'];
	$reload_language_pack = true;
}

if (isset ( $_GET ['logout'] )) {
	admin_logout ();
	header ( 'Location: /cp/index.php' );
	lavu_exit ();
}

if (! admin_loggedin ()) {
	$content_status .= "&nbsp;";
	require ("areas/login.php");
	lavu_exit ();
}

require_once 'objects/backend_user.php';
BackendUser::currentBackendUser ();

if (! checkLoginStatus ( $loggedin_id )) {
	echo "Access Denied";
	admin_logout ();
	exit ();
}

$cpcode = locationSetting ( "component_package_code" );

/**
 * ***************** CHECK FOR FIRST LOCATION LOGIN **********************
 */
if (! isset ( $location_info ['first_location_backend_login'] )) {
	set_sessvar ( "first_login", 1 );
	$arguments = array (
			'location_id' => $locationid,
			'first_location_backend_login' => gmdate ( 'Y-m-d H:i:s' ) 
	);
	$res = lavu_query ( "INSERT INTO `config` ( `location`, `setting`, `type`, `value` ) VALUES ( '[location_id]', 'first_location_backend_login', 'location_config_setting', '[first_location_backend_login]')", $arguments );
	if ($res === FALSE && DEV) {
		echo 'Error Inserting Config Setting: ' . lavu_dberror () . ' <br />';
	}
}
/**
 * *************** END CHECK FOR FIRST LOCATION LOGIN ********************
 */

if (! BackendUser::currentBackendUser ()->isInWhiteList ( $section )) {
	require_once "areas/unauthorized.php";
}

$report_mode = substr ( $mode, 0, 8 ) == 'reports_' ? substr ( $mode, 8 ) : $mode;

if (substr ( $report_mode, 0, 3 ) == "id_") {
	$date_mode = "unified";
	$reportid = ConnectionHub::getConn ( 'poslavu' )->escapeString ( substr ( $report_mode, 3 ) );
	
	$has_required_module = TRUE;
	$required_modules_query = mlavu_query ( "SELECT `required_modules` FROM `$maindb`.`reports` WHERE `id` = '$reportid'" );
	if ($required_modules_query !== FALSE) {
		if (mysqli_num_rows ( $required_modules_query ) > 0) {
			
			$required_modules_read = mysqli_fetch_assoc ( $required_modules_query );
			$required_modules_read = $required_modules_read ['required_modules'];
			$mods = explode ( "&", $required_modules_read );
			for($i = 0; $i < count ( $mods ); $i ++) {
				$mod = $mods [$i];
				if ($mod == NULL || $mod == "")
					continue;
				$has_required_module = FALSE;
				$or_mods = explode ( "|", $mod );
				foreach ( $or_mods as $omod ) {
					if ($modules->hasModule ( $omod )) {
						$has_required_module = TRUE;
						break;
					}
				}
				if (! $has_required_module)
					break;
			}
		}
	}
	
	if ($has_required_module) {
		if ($in_lavu && $reportid) {
			require_once ("/home/poslavu/public_html/admin/sa_cp/show_report.php");
			ajax_show_report ( admin_info ( "database" ), "poslavu_MAIN_db", "unified", "index.php?mode={$section}_{$mode}", $reportid );
		}
	} else {
		echo "You do not have the required settings to view this page.";
	}
} else {
	// / Need to check whta should be done here....
}
