<?php 
require_once(__DIR__."/../cp/resources/session_functions.php");
standarizeSessionDomain();
session_start();
$protocol = 'https://';
$domainName = $_SERVER['HTTP_HOST'];
if ($_SESSION['posadmin_loggedin'] == '') {
	$redirectUrl= $protocol.$domainName.'/cp/index.php?logout=1';
	header("location:".$redirectUrl);
}

?>
<style>
.pull-left {
    float: left;
    margin-right: 10px;
}
 .filters input {
    width: 100px;
}
.filters button {
    width: 100px;
    padding: 4px 4px;
    border-radius: 4px;
    border: 2px solid;

}
</style>
<script src="flexmonster/flexmonster.js"></script>
<link rel="stylesheet" type="text/css" href="month.css" /> 
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
  <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<div>
    <form method="GET">
    <div >
        Filter Data By : 
        <label><input type="radio" name="mode" value="Day" <?php echo ( isset($_GET['mode']) && $_GET['mode'] === "Day") || !isset($_GET['mode'])? 'checked' :''?>/> Day</label> 
    </div>
    <div class="filters"> 
        <p>
            <label>Start Date</label>
            <input type="text" name="startDate" value="<?php echo isset($_GET['startDate'])? $_GET['startDate'] :''?>"/>
            <label>End Date</label>
            <input type="text" name="endDate" value="<?php echo isset($_GET['endDate'])? $_GET['endDate'] :''?>"/>
            <label>DataName</label>
            <input type="text" name="dataname" value="<?php echo isset($_GET['dataname'])? $_GET['dataname'] :''?>"/>
            <Button>Submit</Button>
        </p>
    </div>
</form>
</div>
<div id="pivot-container"></div>

<script>
    $(document).ready(function(){
        $('[name="startDate"], [name="endDate"]').datepicker({
          dateFormat: 'yy-mm-dd',
          changeMonth: true,
          changeYear: true
        });
        });
    var pivot = new Flexmonster({
    container: "pivot-container",
    componentFolder: "flexmonster/",
    toolbar: true,
    report: {
    dataSource: {
      dataSourceType: "ocsv",
      /* URL to the Data Compressor PHP */
      filename: "<?php echo $hostUrl;?>/auditLogTracker/flexmonster/demo_mysql.php?<?php echo $_SERVER['QUERY_STRING'];  ?>"
    },

    "options": {
    "viewType": "grid",
     "grid": {
        "type": "flat",
        "title": "",
        "showFilter": true, 
        "showHeaders": true,
        "fitGridlines": false,
        "showTotals": "off",
        "showGrandTotals": "off",
        "showExtraTotalLabels": false,
        "showHierarchies": true,
        "showHierarchyCaptions": true,
        "showReportFiltersArea": true
        },
      }
    },
    licenseKey:"Z77W-XAA402-0B713X-0Z630W"
});
</script>
