<?php
	
	if (!function_exists('account_loggedin'))
		require_once(dirname(__FILE__).'/../login.php');
	
	require_once($_SERVER['DOCUMENT_ROOT']."/manage/error_config.php");
	class AccountNotes {
		public static function getNoteInfo($id){
		
			if($id!=''){
				$notes_query = mlavu_query("SELECT * FROM `poslavu_MAIN_db`.`restaurant_notes` WHERE `restaurant_id`=[1] ORDER BY `created_on` DESC",$id);
				$result=array();
				if(mysqli_num_rows($notes_query)){
					while( $notes_read = mysqli_fetch_assoc($notes_query) ) {
						array_push($result,$notes_read);
					}
					return $result;	
				}
				else
					return array();
			}
			else 
				return array();
		}

		public static function getNotes($id, $dataname){
			$name= $_SESSION['posadmin_fullname'];
			$notes="
			<div class='scrollableTable'>
				".self::getNotesCore($id, $dataname)."
			</div>
			<button class='buttonClass' id = 'showHideNotes' onclick='$(\"#newNote\").css(\"display\", \"\");$(\"#showHideNotes\").css(\"display\", \"none\");$(\".scrollableTable\").css(\"max-height\", \"200px\");$(\"#noteValue\").val(\"\");'> Create New Note</button><br />".printNextZendeskUpdate($dataname)."
					
			<table id='newNote' style='display:none'>
		
				<tr>
					<th>New Note:</th>
				</tr>
				<tr>
					<td colspan=3> 
						<input type='hidden' name='support_ninja' value='".$name."'> 
						<input type='hidden' name='id' value='".$id."'>
						<textarea cols=60 rows=8 id='noteValue' name='notes_message'>
						</textarea>
					</td>
				</tr>
				<tr>
					<td>
						<center>Ticket ID:
							<input type='text' id= 'ticketIDVal' name='ticket_id'> <input type='submit' value='Save Note' onclick=' saveNote(\"".$name."\", \"".$id."\", $(\"#noteValue\").val(), $(\"#ticketIDVal\").val(),\"".$dataname."\" )' >
						</center>
					</td>
				</tr>
			</table>
			<br />
			
			";
			return $notes;	
		}

		public static function getNotesCore($id) {
			$noteSpec= self::getNoteInfo($id);
			$retval = "
			<table id= 'notes'>

				<tr><th colspan=4 class='areaHeader'>Notes</th></tr>
				
				<tr>
					<td>Notes</td>
					<td>Created Date</td>
					<td>Ticket Number</td>
					<td> Support Ninja</td>
				</tr>
				";
				foreach ($noteSpec as $note){
					$retval.= "<tr class='notesRow'  >
							<td class='message' style='border-top:1px solid black;'>".$note['message']."</td>
							<td style='border-top:1px solid black;'>".$note['created_on']."</td>
							<td style='border-top:1px solid black;'>".$note['ticket_id']."</td>";
					if (strpos($note['support_ninja'], 'zendesk:') === 0) {
						$s_name = 'zendesk';
						$s_id = substr($note['support_ninja'], strlen('zendesk:'));
						$query = mlavu_query("SELECT `name` FROM `poslavu_MAIN_db`.`zendesk_users` WHERE `zendesk_id` = '[zendesk_id]'", array('zendesk_id'=>$s_id));
						if ($query !== FALSE && mysqli_num_rows($query)) {
							$a_zenuser = mysqli_fetch_assoc($query);
							$s_name = $a_zenuser['name'];
						}
						$retval .= "
							<td style='border-top:1px solid black;'><a href='http://support.lavu.com/tickets/".$note['ticket_id']."' style='color:blue;text-decoration:underline;' target='_blank'>{$s_name}</a></td>";
					} else {
						$retval .= "
							<td style='border-top:1px solid black;'>".$note['support_ninja']."</td>";
					}
					$retval.= "
						</tr>";
				}
				$retval.= "
			</table>";
			return $retval;
		}
	}

?>