<?php

	/**
	This is the file for the main tab all we have to do
	is to call this file and itll echo the html for that page
	**/
	require_once("../GetCustomerInfo.php");
	require_once("../login.php");
	require_once($_SERVER['DOCUMENT_ROOT']."/manage/error_config.php");
	
	if( isset($_GET['dataname']) && $_GET['dataname']!=''){
		$dataname= $_GET['dataname'];
		if(strlen($dataname)<20)
			makeMain($dataname);
	}
	else
		echo "No data";

	function makeMain($dataname){
		session_start();
		$o_getCustomerInfo = new GetCustomerInfoClass();
		$info=$o_getCustomerInfo->getCustomerInfo($dataname);
			$syncStatus= $o_getCustomerInfo->getSyncStatus($info['id']);

		if(count($info)!=0) {
			if( $info['disabled']=='D')
				$info['disabled']= "Account Disabled";
			else
				$info['disabled']= "Account Active";
			echo "
			<div id='MainContainer'>
				<table id='main' >
					<tr><th colspan=2>Data Name: ".$info['data_name']."&nbsp;&nbsp; Company Name: ".$info['company_name']." &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;".$info['disabled']."</th></tr>
					<tr>
						<td class='main'><center>".getNotes($o_getCustomerInfo, $info['id'], $info['data_name'])."</center></td>

						<td class='main'><center>".getclientInfo($info)."</center></td>
					</tr>
					<tr>
						<td class='main' colspan=2><center>
						<table><tr><td class='main'>".getClientControl($info['id'], $info['company_code'])."</td>
						</center>
						</td>

					</tr>
				</table>
			</div>
			";
		}
		else{
			echo"<br>No data for this account found. Please try again.<br>";
		}
	}
	function getNotes($o_getCustomerInfo, $id, $dataname){
		$noteSpec= $o_getCustomerInfo->getLavuLiteNoteInfo($id);
		$name= $_SESSION['posadmin_fullname'];
		$notes="
		<div class='scrollableTable'>
		<table id= 'notes'>


				<tr><th colspan=4>Notes</th></tr>

					<tr>
						<td>Notes</td>
						<td>Created Date</td>
						<td>Ticket Number</td>
						<td> Support Ninja</td>
					</tr>
					";
					foreach ($noteSpec as $note){
						$notes.= "<tr class='notesRow'  >
								<td class='message' style='border-top:1px solid black;'>".$note['message']."</td>
								<td style='border-top:1px solid black;'>".$note['created_on']."</td>
								<td style='border-top:1px solid black;'>".$note['ticket_id']."</td>
								<td style='border-top:1px solid black;'>".$note['support_ninja']."</td>
							</tr>";
					}
				$notes.= "
				</table>
				</div>


				<table id='newNote'>

					<tr>
						<th>New Note:</th>
					</tr>
					<tr>
						<td colspan=3>
							<input type='hidden' name='support_ninja' value='".$name."'>
							<input type='hidden' name='id' value='".$id."'>
							<textarea cols=60 rows=8 id='noteValue' name='notes_message'></textarea>
						</td>
					</tr>
					<tr>
						<td>
							<center>Ticket ID:
								<input type='text' id= 'ticketIDVal' name='ticket_id'> <input type='submit' value='Save Note' onclick=' saveLavuLiteNote(\"".$name."\", \"".$id."\", $(\"#noteValue\").val(), $(\"#ticketIDVal\").val(),\"".$dataname."\" )' >
							</center>
						</td>
					</tr>
				</table>
				<br>

		";
		return $notes;
	}
	function getLogo($dataname,$logo){
		$logo="
			<table id='logo'>
				<tr>
					<td>
						<img src='http://admin.poslavu.com/images/".$dataname."/".$logo."' width='150' height='100' >
					</td>
				</tr>
			</table>
		";
		return $logo;
	}
	function getClientControl($id, $companyCode){
		$clientControl="
			<table id='clientControl'>
				<tr><th colspan=3> Client Control </th></tr>
				<tr>

					<td> <button  onclick='window.open(\"http://admin.lavulite.com/manage/misc/MainFunctions/custLogin.php?site=new&mode=login&custID=".$id."&custCode=".$companyCode."\", \"_blank\");  window.focus(); ' >Client Control Panel(New)</button></td>
					<td><button  onclick='window.open(\"http://admin.lavulite.com/manage/misc/MainFunctions/custLogin.php?mode=create_temp&custID=".$id."&custCode=".$companyCode."&username=".$_SESSION['admin_username']."\", \"_blank\");  window.focus(); '>Create Temp Account</a></button></td>
				</tr>
			</table>
		";
		return $clientControl;
	}
	function getClientInfo($info){

		$clientInfo="
			<div style='cursor:pointer; color:blue;' onclick='doRefresh(\"Main\", \"".$info['data_name']."\")'>
				 (refresh page)
				</div>
			<table id='clientInfo'>
			<tr><th colspan=2 style='cursor:pointer' onclick= '
				    if( $(\"#infoChain\").css(\"display\") == \"none\"){

				        $(\"#infoChain\").css(\"display\", \"block\")
				        $(\"#editChain\").css(\"display\", \"none\");

				        $(\"#infoEmail\").css(\"display\", \"block\");
				        $(\"#editEmail\").css(\"display\", \"none\");

				        $(\"#infoPhone\").css(\"display\", \"block\");
				        $(\"#editPhone\").css(\"display\", \"none\");

				        $(\"#infoAddress\").css(\"display\", \"block\");
				        $(\"#editAddress\").css(\"display\", \"none\");

				        $(\"#infoDistro\").css(\"display\", \"block\");
				        $(\"#editDistro\").css(\"display\", \"none\");

				        $(\"#infoName\").css(\"display\", \"block\");
				        $(\"#editName\").css(\"display\", \"none\");

				        $(\"#userSave\").css(\"display\", \"none\");

				    }else{
				        $(\"#infoChain\").css(\"display\", \"none\");
				        $(\"#editChain\").css(\"display\", \"block\");

				        $(\"#infoEmail\").css(\"display\", \"none\");
				        $(\"#editEmail\").css(\"display\", \"block\");

				        $(\"#infoPhone\").css(\"display\", \"none\");
				        $(\"#editPhone\").css(\"display\", \"block\");

				        $(\"#infoAddress\").css(\"display\", \"none\");
				        $(\"#editAddress\").css(\"display\", \"block\");

				        $(\"#infoDistro\").css(\"display\", \"none\");
				        $(\"#editDistro\").css(\"display\", \"block\");

				        $(\"#infoName\").css(\"display\", \"none\");
				        $(\"#editName\").css(\"display\", \"block\");
				        $(\"#userSave\").css(\"display\", \"block\");


				    }    '>Client Info</th></tr>
				<tr>
					<td colspan=2>
						<center><br> <b>LavuLite Account</b><br></center>
					</td>


				</tr>
				<tr >
					<td> ID</td>
					<td style='display:block' id= 'infoid' > ".$info['id']." </td>
				</tr>
				<tr>
					<td> Company Name</td>
					<td> ".$info['company_name']." </td>
				</tr>
				<tr>
					<td background-color='green'> Data Name</td>
					<td> ".$info['data_name']."</td>
				</tr>
				<tr>
					<td> Account Created</td>
					<td> ".$info['created']."</td>
				</tr>
				<tr>
					<td>Last Activity</td>
					<td> ".$info['last_activity']."</td>
				</tr>
				<tr>
					<td>Chain</td>
					<td id= 'infoChain'> ".$info['chain_name']."</td>
					<td id= 'editChain' style='display:none'> <input type= 'text' name='editChainName' value='".$info['chain_name']."'/></td>
				</tr>
				<tr>
					<td>License</td>
					<td> ".$info['package']." </td>
				</tr>

				<tr>
					<td> Status</td>
					<td> ".$info['status']." </td>
				</tr>
				<tr>
					<td> Name</td>
					<td id='infoName'> ".$info['firstname']."  ".$info['lastname']."</td>
					<td id='editName'style='display:none'>
					<input type= 'text' name='editFName' value='".$info['firstname']."' />
					<input type= 'text' name='editLName' value='".$info['lastname']."' />
					</td>
				</tr>
				<tr>
					<td> Email</td>
					<td id='infoEmail' style='display:block'> ".$info['email']." </td>
					<td id='editEmail'style='display:none'> <input type= 'text' name='editEmail' value='".$info['email']."' /></td>
				</tr>
				<tr>
					<td> Phone</td>
					<td id='infoPhone' style='display:block'>  ".$info['phone']." </td>
					<td id='editPhone'style='display:none'> <input type= 'text' name='editPhone' value='".$info['phone']."'/></td>
				</tr>
				<tr>
					<td> Address</td>
					<td id='infoAddress' style='display:block'>  ".$info['address']." </td>
					<td id='editAddress'style='display:none'> <input type= 'text' name='editAddress' value='".$info['address']."'/></td>
				</tr>
				<tr>
					<td> Distro Code</td>
					<td id='infoDistro' style='display:block'>  ".$info['distro_code']." </td>
					<td id='editDistro'style='display:none'> <input type= 'text' name='editDistro' value='".$info['distro_code']."' /></td>
				</tr>";

				if($info['arb_license_start'] > date("Y-m-d")){
				$clientInfo.="
				<tr>
					<td>
					Extend Trial
					</td>
					<td>
						<table bgcolor='#bbbbbb' style='border:solid 1px #888888' cellspacing=1 cellpadding=1>
						<td style='padding-left:6px; padding-right:12px'>
							<input type='button' onclick='ext_days = document.getElementById(\"extend_count\").value;
								if(ext_days==\"\")
									alert(\"Choose how many days to extend first dude!\");
								 else {
								 	if(confirm(\"Are you sure you want to extend by \"+ext_days+\" days?\"))
								 		extendBilling('".$info['id']."',ext_days);
								 }'
								 value='Extend' style='font-size:10px;'>
							<select id='extend_count' style='font-size:10px'>
								<option value=''></option>
								<option value='1'>1</option>
								<option value='2'>2</option>
								<option value='3'>3</option>
								<option value='4'>4</option>
								<option value='5'>5</option>
								<option value='6'>6</option>
								<option value='7'>7</option></select>
						</td>
					</table></td>
				</tr>";
				}
				$clientInfo.="<tr>
					<td colspan=2><center>";
					if($info['disabled']=='Account Disabled')
							$clientInfo.= "<div style='color:red; cursor:pointer' onclick=\"enableOrDisableAccount('".$info['id']."', 'enable', '".$info['data_name']."'); this.setAttribute('onclick', '');\">Enable</div>";
						else
							$clientInfo.="<div style='color:green;cursor:pointer' onclick=\"enableOrDisableAccount('".$info['id']."', 'disable', '".$info['data_name']."'); this.setAttribute('onclick', '');\">Disable</div>";

					$clientInfo.="</center></td>
				</tr>
				<tr colspan=2>
				<td id='userSave' style='display:none'><input type='submit' onclick='saveUserInfo($(\"input[name=editChainName]\").val(),$(\"input[name=editFName]\").val(), $(\"input[name=editLName]\").val(),$(\"input[name=editEmail]\").val(), $(\"input[name=editPhone]\").val(),$(\"input[name=editAddress]\").val(), $(\"input[name=editDistro]\").val(),\"".$info['data_name']."\" )'> </td>
				</tr>

			</table>
		";
		return $clientInfo;
	}
	function getSyncDatabaseStruct($syncStatus, $locID){


		$databaseStruct="
			<table id = 'databaseStruct'>
				<tr>
					<th> Sync Database Structure</th>
				</tr>
				<tr>
					<td>";

						if($syncStatus=="inSync")
							$databaseStruct.="<center>In Sync</center> ";
						else{
							$databaseStruct.="<center><a href='./misc/syncDB.php?mode=sync_database&rowid=".$locID."'>".$syncStatus."</a></center>";

						}
					$databaseStruct.="</td>
				</tr>
			</table>

		";
		return $databaseStruct;
	}
	function getDeviceInfo($o_getCustomerInfo, $id, $dataname){

		$deviceInfo="
			<div class= 'scrollableDevices'>
			<table class='DeviceInfo'>
			<tr><th colspan=11>Device Info</th></tr>
				<tr>
					<th>Name/IP</th>
					<th>Model</th>
					<th>iOS</th>
					<th>App</th>
					<th>Version(Build)</th>
					<th>Location</th>
					<th>Order Prefix</th>
					<th>Identifier</th>
					<th>Last Checkin</th>
					<th>DeviceTime</th>
					<th>Last Reload Settings</th></tr>

				";
			$devices= $o_getCustomerInfo->getDevices($id, $dataname);
			foreach( $devices as $device){
					if ($device['MAC'] != "")
						$display_identifier = $device['MAC'];
					else if ($device['UUID'] != "")
						$display_identifier = $device['UUID'];
					else
						$display_identifier="N/A";

				$deviceInfo.="
					<tr >
						<td>".$device['name']."/".$device['ip_address']."</td>
						<td>".$device['model']."</td>
						<td>".$device['system_version']."</td>
						<td>".$device['app_name']."</td>
						<td>".$device['poslavu_version']."</td>
						<td>".$device['location_name']."</td>
						<td>". str_pad($device['prefix'], 2, "0", STR_PAD_LEFT)."</td>
						<td>".$display_identifier."</td>
						<td>".$device['last_checkin']."</td>
						<td>".$device['device_time']."</td>
						<td id=".$device['ip_address'].">".$device['last_reload']."</td>";

					$deviceInfo.="</tr>";

			}

			$deviceInfo.="</table></div>";
			return $deviceInfo;
	}
	function getLocationInfo($o_getCustomerInfo,$dataname, $id){
		$locations= $o_getCustomerInfo->getLocationInfo($dataname);

		$locationInfo="
			<table id='LocationInfo'>
			<tr><th colspan=12>Location Info</th></tr>
				<tr>
					<th>ID</th>
					<th>Name</th>
					<th>City</th>
					<th>State</th>
					<th>Country</th>
					<th>Phone</th>
					<th>Manager</th>
					<th>Gateway</th>
					<!--<th>Debug Gateway</th>
					<th>LS Ipad Tunnel </th>
					<th>Request Server info</th> -->
					<th>Disable</th>

				</tr>";
				foreach ($locations as $location){

					$locationInfo.="
					<tr>
						<td>".$location['id']."</td>
						<td>".$location['title']."</td>
						<td>".$location['city']."</td>
						<td>".$location['state']."</td>
						<td>".$location['country']."</td>
						<td>".$location['phone']."</td>
						<td>".$location['manager']."</td>
						<td>".$location['gateway']."</td>";
						$enable_disable = "Disable";
						if ($loc_read['_disabled'] == "1") $enable_disable = "Enable";
						$locationInfo.="<td style='padding-left:5px'><input type='button' onclick='window.open(\"misc/MainFunctions/disableLocation.php?dataname=".$dataname."&rowid=".$id."&".$enable_disable."_location=".$location['id']."\")' value=".$enable_disable."></td>
					</tr>
					";
				}
			$locationInfo.="</table>";
		return $locationInfo;
	}
	function getSignupInfo($o_getCustomerInfo, $dataname){
		$signups= $o_getCustomerInfo->getSignupInfo($dataname);

		$getSignupInfo="<center>

		<table id='signupInfo'>
		<tr><th colspan=6>Signup Info</th></tr>

				<tr>
					<th>ID</th>
					<th>Name</th>
					<th>City</th>
					<th>State</th>
					<th>Country</th>
					<th>Phone</th>
				</tr>";
			if(count($signups)>0){
				foreach ($signups as $signup){
					$getSignupInfo.="
						<tr>
							<td>".$signup['id']."</td>
							<td>".$signup['firstname']." ".$signup['lastname']."</td>
							<td>".$signup['city']."</td>
							<td>".$signup['state']."</td>
							<td>".$signup['country']."</td>
							<td>".$signup['phone']."</td>
						</tr>
					";
				}
			}
			$getSignupInfo.="</table></center>";
		return $getSignupInfo;
	}
?>