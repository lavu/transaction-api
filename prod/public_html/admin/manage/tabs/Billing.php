<?php
	require_once($_SERVER['DOCUMENT_ROOT']."/manage/error_config.php");
	require_once("../login.php");
	require_once(dirname(dirname(dirname(__FILE__)))."/cp/resources/lavuquery.php");
	$loggedin= account_loggedin();
	isset($_GET['dn'])?$dn=$_GET['dn']:$dn=false;
	//echo "the dataname is:".$dn;

	if(can_access('technical'))
		$p_area='billing_info';
	else
		$p_area='customer_billing_info';



	if($dn && $dn!=""){
		$get_company_name = mlavu_query("SELECT `id`, `company_name` FROM `poslavu_MAIN_db`.`restaurants` WHERE `data_name` = '[1]'", $dn);
		if($get_company_name !== FALSE && mysqli_num_rows($get_company_name))
		{
			$info = mysqli_fetch_assoc($get_company_name);
			$title = $info['company_name']." ($dn)";
			$restaurantid = $info['id'];
			if (!isset($load_billing_individual_accounts))
				$load_billing_individual_accounts = TRUE;
			require_once(dirname(__FILE__)."/../../sa_cp/billing/procareas/billing_for_individual_accounts.php");
			echo onready_script();
		} else {
			echo " The company \"{$dn}\" can't be found.";
		}
	}
	else
		echo " No dataname Supplied";

	function onready_script() {

		ob_start();
		?>
		<script type='text/javascript'>
			$(document).ready(function() {
				var pwindow = window.parent.window;
				pwindow.resize(0, true);
				var internalScrollTop = parseInt($(window).scrollTop());
				var externalScrollTop = parseInt($(pwindow).scrollTop());
				pwindow.$(pwindow).scrollTop(internalScrollTop + externalScrollTop);
				$(window).scrollTop(0);
			});
		</script>
		<?php
		$s_retval = ob_get_contents();
		ob_end_clean();

		return $s_retval;
	}
