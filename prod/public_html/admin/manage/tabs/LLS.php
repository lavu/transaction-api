<?php
    require_once($_SERVER['DOCUMENT_ROOT']."/manage/error_config.php");
	require_once("/home/poslavu/public_html/admin/cp/resources/lavuquery.php");
	require_once("../login.php");
	$dataname="";
	$rowid="";
	$loc_id="";
	if(isset($_REQUEST['dataname']) && isset($_REQUEST['rowid']) && isset($_REQUEST['locationID'])){
		$dataname=$_REQUEST['dataname'];
		$rowid= $_REQUEST['rowid'];
		$loc_id=$_REQUEST['locationID'];
	}
	else{
		echo "No dataname retrieved. Please try again. ";
		exit;
	}


	$basepath = "n/a";
	$paths = array();

	$locserv_query = mlavu_query("select * from `poslavu_[1]_db`.`config` where `setting`='local_file_structure'", $dataname);

	$pathInfo = mysqli_fetch_assoc (mlavu_query("select `use_net_path`, `net_path` from `poslavu_[1]_db`.`locations`  ", $dataname));

	if( ! mysqli_num_rows($locserv_query) ){

		while($locserv_read = mysqli_fetch_assoc($locserv_query)){
			$loc_id=$locserv_read['location'];
		}
			if( $pathInfo['use_net_path']==0 && strstr($pathInfo['net_path'], 'poslavu.com'))
				echo "This account does not appear to have a local server. ";
			echo "<input type='button' value='Request Local Server Info' style='font-size:10px' onclick='requestLLS(\"".$dataname."\", \"".$loc_id."\")'/>";

	}

	echo "<table id='LLS' width=100%; style='background-color:white'>
	<tr border=1>
		<td>
		<div style='overflow:scroll; height:400px; border-style:solid; border-width:1px;'>
			<table class= 'llsFileStructure' > ";
	while($locserv_read = mysqli_fetch_assoc($locserv_query)){
	$loc_id=$locserv_read['location'];

		echo "
		<tr>
			<td>
				<div style='cursor:pointer; color:blue;' onclick='doRefresh(\"LLS\", \"".$dataname."\")'>
				 (refresh)
				</div>
			</td>
		</tr>

		<tr>
			<td>
				<br><b>Local Server File Structure
			</td>
				<td>
					Location: " . $locserv_read['location'] . "</b>
				</td>
		</tr>
		<tr>
				<td>
					<br>Ip Address:</td><td> " . $locserv_read['value']."
				</td>
		</tr>
		<tr>
				<td>
					<br>Last Read:</td><td> " . $locserv_read['value2']."
				</td>
		</tr>";

		$lastsync_query = mlavu_query("select * from `poslavu_[1]_db`.`config` where `setting`='last sync request'", $dataname);
		if(mysqli_num_rows($lastsync_query))
		{
			$lastsync_read = mysqli_fetch_assoc($lastsync_query);
			//echo "<br>Last Sync: " . $lastsync_read['value'];

			$lastsync_time = $lastsync_read['value'];
			if(strpos($lastsync_time,"/"))
			{
				$lastsync_parts = explode("/",$lastsync_time);
				$lastsync_time = $lastsync_parts[0];
				$lastsync_interval = $lastsync_parts[1];
			}
			else $lastsync_interval = 60;
			//echo "<br>Last Sync TS: " . $lastsync_time;
			//echo "<br>Last Sync Interval: " . $lastsync_interval;
			//$lastsync_ts = create_timestamp_from_datetime($lastsync_read['value']);

			$elapsed_sync = time() - $lastsync_time * 1;
			if($elapsed_sync >= 60)
			{
				if($elapsed_sync >= 60 * 60 * 24)
				{
					$days_ago = floor($elapsed_sync / (60 * 60 * 24));
					$time_ago = $days_ago . " day";
					if($days_ago > 1) $time_ago .= "s";
					$time_ago .= " ago";
				}
				else if($elapsed_sync >= 60 * 60)
				{
					$hours_ago = floor($elapsed_sync / (60 * 60));
					$time_ago = $hours_ago . " hour";
					if($hours_ago > 1) $time_ago .= "s";
					$time_ago .= " ago";
				}
				else
				{
					$minutes_ago = floor($elapsed_sync / 60);
					$time_ago = $minutes_ago . " minute";
					if($minutes_ago > 1) $time_ago .= "s";
					$time_ago .= " ago";
				}
				echo "
				<tr>
					<td>
						<br>Last Time Synced:
					</td>
					<td>
						" . date("Y-m-d h:i:s a",$lastsync_read['value']) . " (".$time_ago.")
					</td>
				</tr>";

			}
			else
			{
				$until_next_sync = ($lastsync_interval - $elapsed_sync);
				if($until_next_sync < 0) $until_next_sync = 0;
				$sync_left_display = "display_next_sync";
				$sync_left_var = "var_next_sync";
				$sync_left_function = "function_next_sync";
				echo "<tr><td colspan=2><table cellspacing=0 cellpadding=0><tr><td style='color:#000088'>Next Sync Expected:&nbsp;</td><td><div style='color:#000088' id='$sync_left_display'><b>" . $until_next_sync . "</b></div></td><td style='color:#000088'>&nbsp;seconds</td></tr></table>";
				echo "</td></tr>";
			}
		}

		if($locserv_read['value3']!="")
		{
			$varset = get_vars_from_str($locserv_read['value3']);

			$remote_dbinfo = get_database_info("poslavu_".$dataname."_db");
			$remote_table_count = $remote_dbinfo['table_count'];
			$remote_field_count = $remote_dbinfo['field_count'];
			$remote_fields = $remote_dbinfo['fields'];

			if(isset($varset['field_sync_count']))
				$db_field_balance = ($varset['field_count'] - $varset['field_sync_count']);
			else $db_field_balance = $varset['field_count'];

			if(isset($varset['table_count']))
			echo "
			<tr>
				<td><br><font color='#008800'>Database Tables:</td>
				<td> <b>" . $varset['table_count'] . " / $remote_table_count</b></font>
				</td>
			</tr>";
			if(isset($varset['field_count']))
			{
				echo "
				<tr>
					<td>
						<br><font color='#008800'>Database Fields:
					</td>
					<td>
						<b>".$db_field_balance."
					</td>
				</tr>";
				echo "<tr> <td colspan=2>";
				if(isset($varset['field_sync_count']) && $varset['field_sync_count'] > 0)
					echo "<center> <font color='#008800'>(+".$varset['field_sync_count'].")</font>";
				echo " / $remote_field_count</b></font></center></td></tr>";

			}
			if(isset($varset['field_sync_count']))
			echo "<tr><td><br><font color='#008800'>Fields used for Syncing:</td><td> <b>" . $varset['field_sync_count'] . "</b></td></font></tr>";
			if(isset($varset['table_name_list']))
			{
				echo "<tr><td colspan=2><br><a style='cursor:pointer' onclick='
					if(document.getElementById(\"local_table_list\").style.display==\"\")
						document.getElementById(\"local_table_list\").style.display = \"none\";
					else
						document.getElementById(\"local_table_list\").style.display = \"\"'>

					<font color='#008800'>List Local Tables</font></a></td></tr>";

				echo "<tr style='display:none' id='local_table_list'><td colspan=2><table width=100%>";
				$table_name_list = explode(",",urldecode($varset['table_name_list']));

				for($n=0; $n<count($table_name_list); $n++)
				{
					$table_name_parts = explode(" - ",$table_name_list[$n]);
					$ltname = $table_name_parts[0];
					if(isset($table_name_parts[1]))
						$local_table_field_count = $table_name_parts[1];
					else $local_table_field_count = 0;
					if(isset($table_name_parts[2]))
						$local_table_field_sync_count = $table_name_parts[2];
					else $local_table_field_sync_count = 0;
					if(isset($remote_fields[$ltname]) && isset($remote_fields[$ltname][0]))
						$remote_table_field_count = count($remote_fields[$ltname][0]);
					else $remote_table_field_count = 0;

					$local_table_field_balance = $local_table_field_count - $local_table_field_sync_count;
					if($local_table_field_balance * 1 != $remote_table_field_count * 1)
						$use_style = "style='font-weight:bold'";
					else
						$use_style = "";

					echo "<tr>";
					echo "<td align='right' $use_style>" . $ltname . "</td>";
					echo "<td $use_style>" . $local_table_field_balance;
					if($local_table_field_sync_count > 0) echo " <font color='#008800'>(+" . $local_table_field_sync_count . ")</font>";
					echo "</td>";
					echo "<td $use_style>" . $remote_table_field_count . "</td>";
					echo "</tr>";
				}
				echo "</td></tr></table> </table></div></td>";
			}
		}


		if(isset($_GET['view_local_query']) && isset($_GET['locid']) && $_GET['locid']==$locserv_read['location'])
		{
			$locid = $_GET['locid'];
			$local_query_options = "";
			$local_query_value = "";

			if(isset($_GET['view_local_query_id']))
				$view_local_query_id = $_GET['view_local_query_id'];
			else
				$view_local_query_id = 0;
			$last_local_query = mlavu_query("select * from `[1]`.`config` where `location`='[2]' and `setting`='local_query_result' order by id desc limit 20","poslavu_" . $dataname . "_db",$_GET['locid']);
			while($last_local_read = mysqli_fetch_assoc($last_local_query))
			{
				if($view_local_query_id==0)
					$view_local_query_id = $last_local_read['id'];
				$local_query_options .= "<option value='".$last_local_read['id']."'";
				if($view_local_query_id==$last_local_read['id'])
				{
					$local_query_value = $last_local_read['value_long'];
					$local_query_options .= " selected";
				}
				$local_query_options .= ">" . $last_local_read['value2'] . " (" . $last_local_read['value4'] . ") " . $last_local_read['value'] . "</option>";
			}

			echo "<br><br>View Queries:<br><br>";
			echo "<select onchange='window.location = \"?mode=manage_customers&submode=$submode&rowid=$rowid&locid=$locid&view_local_query=1&view_local_query_id=\" + this.value'>";
			echo $local_query_options;
			echo "</select>";
			echo "<div style='overflow:auto; width:800px; height:400px; border:solid 1px #888888'>";
			echo $local_query_value;
			echo "</div><br><br>";
		}
		else if(isset($_GET['run_health_check']) && isset($_GET['locid']) && $_GET['locid']==$locserv_read['location'])
		{
			schedule_msync($dataname,$locserv_read['location'],"health check","","");
		}
		else if(isset($_GET['perform_records_check']) && isset($_GET['locid']) && $_GET['locid']==$locserv_read['location'])
		{
			schedule_msync($dataname,$locserv_read['location'],"records_check","","");
		}
		else if(isset($_GET['catchup_syncing']) && isset($_GET['locid']) && $_GET['locid']==$locserv_read['location'])
		{
			function mlavu_schedule_remote_to_local_sync($rdb,$locationid,$sync_setting,$sync_value,$sync_value_long="")
			{
				$exist_query = mlavu_query("select * from `$rdb`.`config` where `setting`='[1]' and `location`='[2]' and `value`='[3]' and `value_long`='[4]'",$sync_setting,$locationid,$sync_value,$sync_value_long);
				if(mysqli_num_rows($exist_query))
				{
				}
				else mlavu_query("insert into `$rdb`.`config` (`setting`,`location`,`value`,`value_long`,`type`) values('[1]','[2]','[3]','[4]','log')",$sync_setting,$locationid,$sync_value,$sync_value_long);
			}

			$set_last_sync = "";
			$year = date("Y");
			$month = date("m");
			$day = date("d");
			$hour = date("H");
			$minute = date("i");
			$second = date("s");

			$set_last_sync = date("Y-m-d H:i:s",mktime($hour + 1,$minute,$second,$month,$day,$year));
			$closed_before = date("Y-m-d H:i:s",mktime($hour,$minute,$second,$month,$day - 2,$year));

			$run_local_query = "update orders set `lastsync`='$set_last_sync', `lastmod`='' where `closed`<'$closed_before' and `closed`!='0000-00-00 00:00:00' and `lastsync` < `lastmod`";
			mlavu_schedule_remote_to_local_sync("poslavu_" . $dataname . "_db",$_GET['locid'],"run local query","config",$run_local_query);
		}
		else if(isset($_GET['run_local_query']) && isset($_GET['locid']) && $_GET['locid']==$locserv_read['location'])
		{
			function mlavu_schedule_remote_to_local_sync($rdb,$locationid,$sync_setting,$sync_value,$sync_value_long="")
			{
				$exist_query = mlavu_query("select * from `$rdb`.`config` where `setting`='[1]' and `location`='[2]' and `value`='[3]' and `value_long`='[4]'",$sync_setting,$locationid,$sync_value,$sync_value_long);
				if(mysqli_num_rows($exist_query))
				{
				}
				else mlavu_query("insert into `$rdb`.`config` (`setting`,`location`,`value`,`value_long`,`type`) values('[1]','[2]','[3]','[4]','log')",$sync_setting,$locationid,$sync_value,$sync_value_long);
			}

			if(isset($_POST['local_query_command']))
			{
				$run_local_query = $_POST['local_query_command'];
				if(strpos(strtolower($run_local_query),"where")===false || (strpos(strtolower($run_local_query),"select")!==false && strpos(strtolower($run_local_query),"limit")===false))
				{
					echo "<br>Query Not Allowed:<br>".$run_local_query."<br>";
				}
				else
				{
					mlavu_schedule_remote_to_local_sync("poslavu_" . $dataname . "_db",$_GET['locid'],"run local query","config",$run_local_query);
				}
			}
			else
			{
				$locid = $_GET['locid'];
				echo "<form name='run_local_query_".$locid."' method='post' action='?mode=manage_customers&submode=$submode&rowid=$rowid&locid=$locid&run_local_query=1'>";
				echo "<br>Run Local Query";
				echo "<script language='javascript' src='resources/textarea_tab.js'></script>";
				echo "<br><textarea name='local_query_command' onkeydown='return catchTab(this,event)' cols='60' rows='8'></textarea>";
				echo "<br><input type='button' value='Submit Query' onclick='if(confirm(\"Are you sure you want to execute this query?\")) document.run_local_query_".$locid.".submit()'>";
				echo "<br>";
			}
		}
		else if(isset($_GET['read_retrieved_file']) && isset($_GET['locid']) && $_GET['locid']==$locserv_read['location'] && isset($_GET['filedate']))
		{
			$local_filedate = $_GET['filedate'];
			$file_query = mlavu_query("select * from `[1]`.`config` where `location`='[2]' and `setting`='retrieved_local_file' and TRIM(`value`)='[3]'","poslavu_".$dataname."_db",$locserv_read['location'],trim($_GET['read_retrieved_file']));
			if(mysqli_num_rows($file_query))
			{
				$file_read = mysqli_fetch_assoc($file_query);

				if(isset($_POST['file_contents']) && isset($_POST['file_name']) && $_POST['file_name']==$_GET['read_retrieved_file'])
				{
					mlavu_query("update `[1]`.`config` set `value_long`='[2]', `value5`='write', `value6`='[3]' where `id`='[4]'","poslavu_".$dataname."_db",$_POST['file_contents'],date("Y-m-d H:i:s") . " - " . $_SERVER['REMOTE_ADDR'],$file_read['id']);
					//echo "update `[1]`.`config` set `value_long`='[2]', `value5`='write', `value6`='[3]' where `id`='[4]'<br>";
				}
				else
				{
					if($file_read['value2'] < $local_filedate)
						$file_readonly = true;
					else $file_readonly = false;

					$locid = $_GET['locid'];
					echo "<form name='update_local_file_".$locid."' method='post' action='?mode=manage_customers&submode=$submode&rowid=$rowid&locid=$locid&filedate=$local_filedate&read_retrieved_file=".$_GET['read_retrieved_file']."'>";
					echo "<br>File Contents: " . $file_read['value'] . " (retrieved / updated " . $file_read['value2'] . " from ".$file_read['value4'].")";
					echo "<script language='javascript' src='resources/textarea_tab.js'></script>";
					echo "<br>Last Updated on Local Server: " . $local_filedate;
					echo "<br><input type='hidden' name='file_name' value='".$_GET['read_retrieved_file']."'>";
					echo "<br><textarea name='file_contents' onkeydown='return catchTab(this,event)' cols='100' rows='30'";
					if($file_readonly) echo " readonly disabled";
					echo ">";
					echo str_replace("<","&lt;",str_replace("&","&amp;",$file_read['value_long']));
					echo "</textarea>";
					if(!$file_readonly)
						echo "<br><input type='button' value='Upload Changes' onclick='if(confirm(\"Are you sure you want to upload your changes?\")) document.update_local_file_".$locid.".submit()'>";
					echo "<br>";
				}
			}

		}
		else if(isset($_GET['_copy_remote_files']) && isset($_GET['_copy_local_files']) && isset($_GET['locid']) && $_GET['locid']==$locserv_read['location'])
		{
			schedule_msync($dataname,$locserv_read['location'],"copy remote file",$_GET['_copy_remote_files'],$_GET['_copy_local_files']);
			//$success = mlavu_query("insert into `poslavu_[1]_db`.`config` (`location`,`setting`,`value`,`value_long`) values ('[2]',
			//'copy remote file','[3]','[4]')",$dataname,$locserv_read['location'],$_GET['_copy_remote_files'],$_GET['_copy_local_files']);
			//if($success) echo "<br><b>Copying Files: </b>" . $_GET['_copy_remote_files'] . " to " . $_GET['_copy_local_files'] . "<br>";
		}
		else if(isset($_GET['update_garage_files']) && $_GET['update_garage_files']==1 && $_GET['locid']==$locserv_read['location'])
		{
			$garage_files = array("kart_addKart.php","kart_addKartWrapper.php","karts_form2.js","karts_formCSS2.css","karts_formInclude.php","kart_info.php","karts_info_wrapper.php","karts_list.php","karts_list_wrapper.php","karts_removeKart.php","karts_submitAddKartToDB.php","karts_submitAddKartToDB.php","karts_updateKart.php","part_functions.php","parts_addPart.php","parts_addPartWrapper.php","part_alphabetizer.php","part_form_select_element2.css","parts_info.php","part_info_wrapper.php","parts_list.php","parts_list_wrapper.php","parts_pit_forms1.js","parts_removePart.php","parts_updatePart3.php");
			for($n=0; $n<count($garage_files); $n++)
			{
				$set_remote_filename = "garage/" . $garage_files[$n];
				$set_local_filename = "../components/lavukart/" . $garage_files[$n];
				schedule_msync($dataname,$locserv_read['location'],"copy remote file",$set_remote_filename,$set_local_filename);
			}
		}
		else if(isset($_GET['remove_local_files']) && isset($_GET['locid']) && $_GET['locid']==$locserv_read['location'])
		{
			schedule_msync($dataname,$locserv_read['location'],"remove local file",$_GET['remove_local_files']);
			//$success = mlavu_query("insert into `poslavu_[1]_db`.`config` (`location`,`setting`,`value`) values ('[2]','remove local file','[3]')",$dataname,$locserv_read['location'],$_GET['remove_local_files']);
			//if($success) echo "<br><b>Removing Files: </b>" . $_GET['remove_local_files'] . "<br>";
		}
		else if(isset($_GET['upload_signature']) && isset($_GET['locid']) && $_GET['locid']==$locserv_read['location'])
		{
			if(isset($_GET['upload_signature_type']) && $_GET['upload_signature_type']=="waiver")
				$upload_type = "upload waiver";
			else
				$upload_type = "upload signature";
			schedule_msync($dataname,$locserv_read['location'],$upload_type,$_GET['upload_signature']);
			//$success = mlavu_query("insert into `poslavu_[1]_db`.`config` (`location`,`setting`,`value`) values ('[2]','remove local file','[3]')",$dataname,$locserv_read['location'],$_GET['remove_local_files']);
			//if($success) echo "<br><b>Removing Files: </b>" . $_GET['remove_local_files'] . "<br>";
		}

		$pending_query = mlavu_query("select * from `poslavu_[1]_db`.`config` where `location`='[2]' and (`setting`='copy remote file' or `setting`='health check' or `setting`='records_check' or `setting`='remove local file' or `setting`='upload waiver' or `setting`='upload signature' or `setting`='read file structure' or `setting`='run local query' or `setting`='open tunnel' or `setting`='sync database' or `setting`='retrieve local file' or (`setting`='retrieved_local_file' and `value5`='write'))", $dataname,$locserv_read['location']);
		if(mysqli_num_rows($pending_query))
		{
			echo "<br><table bgcolor='#8888AA' cellpadding=4>";
			echo "<tr><td bgcolor='#000088' style='color:#ffffff' align='center' colspan='4'>Pending Actions</td></tr>";
			while($pending_read = mysqli_fetch_assoc($pending_query))
			{
				if($pending_read['setting']=="retrieved_local_file" && $pending_read['value5']=="write")
				{
					$show_setting = "update retrieved file";
					$show_value = $pending_read['value'];
				}
				else
				{
					$show_setting = $pending_read['setting'];
					$show_value = $pending_read['value'];
					$show_value_long = $pending_read['value_long'];
				}
				echo "<tr>";
				echo "<td>";
				echo "<a  onclick='removePendingAction(\"$dataname\",\"$rowid\",\"".$locserv_read['location']."\", \"".$pending_read['id']."\")'>(remove)</a>";
				echo "</td>";
				echo "<td bgcolor='#ffffff'>".$show_setting."</td><td bgcolor='#ffffff'>".$show_value."</td><td bgcolor='#ffffff'>".$show_value_long."</td>";
				echo "</tr>";
			}
			echo "</table>";
		}


		echo "
			<td style= 'padding-left:20px;'>
				";
		require_once(dirname(__FILE__) . "/../port_proxy_server/proxy_rpc_plugin.php");//Outputs the iframe where proxy can be controlled by rpc.
		echo "
				<table id='buttonsTable' >
				<tr><th colspan=5> Static Functions </th></tr>
				<tr>
					<td>
						<input type='button' value='Sync Local DB structure' onclick='syncLocalDBStruct(\"".$rowid."\",\"".$locserv_read['location']."\",\"".$dataname."\")'>
					</td>
					<td>
			  			<input type='button' value='Turn on LSVPN Tunnel'  onclick='window.open(\"./misc/LLSFunctions/iPadTunneling.php?notabs=1&rowid=".
			  			$rowid."&locid=".$loc_id."\",\"\",\"width=420,height=320,resizable=yes,toolbar=no\")'>
			  		</td>

			  		<td>
						<input type='button' value='Perform Health Check' onclick='performHealthCheck(\"".$rowid."\",\"".$locserv_read['location']."\",\"".$dataname."\")'>
					</td>
					<td>
						<input type='button' value='Request Sync Version' onclick='$.ajax({ cache:false, type:\"POST\", url:\"/manage/misc/LLSFunctions/getSyncVersion.php\", data:{dn:\"$dataname\",file:\"getSyncVersion\",cmd:\"request_sync_version\"}, async:true, success:function(message){alert(message);} });'>
					</td>
				</tr>

				<tr>
					<td>
						Debug Gateway
						<div id='divloc".$location['id']."'>

							<script type='text/javascript' src='./js/sacpjs.js'></script>
							<script language='javascript'>
								//showSelector(\"loc\", \"".$rowid."\", \"".$location['id']."\", \"\", \"\",this );
							</script></div>

							</td>

					<td>
						<input type='button' value='Request Local Server Info)' style='font-size:10px'
						onclick='requestLLS(\"".$dataname."\", \"".$loc_id."\")'/>
					</td>
					<td>
						<input type='button' class='run_local_query_btn' value='Run Local Query' onclick='local_query_box()'/>
						<div class='run_local_query_class' style='display:none'>
							<textarea class='local_query_val'> </textarea><br>
							<input type='button' value='submit' onclick= 'run_local_query(\"$loc_id\",\"$dataname\")'/>
						</div>
					</td>
				</tr>

				<tr>
					<td>
						Launch Bash Command:
					</td>
					<td colspan=2>
						<input type='input' name='sync_down_bash_input' id='sync_down_bash_input_id' style='width:100%'/>
					</td>
					<td>
						<input type='button' style='margin-left:20px' value='run bash cmd' onclick='run_local_bash(\"$loc_id\",\"$dataname\", document.getElementById(\"sync_down_bash_input_id\").value)';
					</td>
				</tr>
				<tr>";
					require_once(dirname(__FILE__) . '/../port_proxy_server/launch_tunnel_gui_row.php');//The complicated table row is built here.
		echo	"</tr>
			</table>
		</td></tr>";
		echo
			"
				<tr><td colspan=2>
					<table id='updatesTable'>
					<tr><th colspan=4> Updates</th></tr>";

					$updates= explode("|*update*|", file_get_contents("./../misc/LLSFunctions/updates.txt"));
					if(count( $updates)>0){
						$counter=0;
						echo "<tr>";
						foreach($updates as $update){
							if($update!=" "){
								$counter++;
								$line=explode("|", $update);
								echo "<td><input type='submit' value='".$line[0]."' onclick='doUpdate(\"$dataname\",\"$rowid\",\"".$locserv_read['location']."\",\"".$line[1]."\")'></td>";
								if( $counter%4==0){
									echo "</tr><tr>";
								}
							}
						}
						echo "</tr>";
					}
					else{
						echo "<tr><td>No updates to apply</td></tr>";
					}

		echo"		</table>


				</td></tr>
			";




			if(isset($_REQUEST['viewFolders'])){
				if (strpos($locserv_read['value_long'], "ioncube_loader"))
					$lls_encoder = "ioncube";
				else
					$lls_encoder = "zendguard";

				$repo_dir = "lls-".$lls_encoder;

				$paths = array();
				$structure = explode("\n",$locserv_read['value_long']);

				for($i=0; $i<count($structure); $i++)
				{
					$structure_parts = explode("|",$structure[$i]);
					$fname = $structure_parts[0];
					if(count($structure_parts) > 2)
					{
						$fsize = $structure_parts[1];
						$fdate = $structure_parts[2];
					}
					else
					{
						$fsize = "";
						$fdate = "";
					}

					$parent_folder = substr($fname,0,strrpos($fname,"/"));
					$basename = substr($fname,strrpos($fname,"/") + 1);
					$paths[] = array($parent_folder,$fname,$basename,$fsize,$fdate);

					if(($basepath=="n/a" || strlen($parent_folder) < strlen($basepath)) && trim($fname)!="")
					{
						$basepath = $parent_folder;
					}
					//$fname_parts = explode("/",$fname);
					//for($n=1; $n<count($fname_parts); $n++)
					//{
					//	$folder = $fname_parts[$n];
					//	if(!isset($path_part[$folder]))
					//}
				}

				echo show_folders_with_parent("poslavu_".$dataname."_db","?mode=manage_customers&submode=$submode&rowid=$rowid&locid=".$locserv_read['location'],$basepath,$paths);
				echo "<script type=text/javascript>$('#createUpdate').css('display', 'none');$('#viewFolders').css('display', 'none'); </script>";
			}
			else{
				echo "
				<tr>
					<table>
					<tr>
					<td colspan=2  id='viewFolders'>
						<input type='checkbox' onchange='createFileStructure(\"".$dataname."\",\"".$rowid."\", \"".$loc_id."\", true)'> View Folder Structure
					</td>

				";
			}

			if(isset($_REQUEST['createUpdate'])){
				if (strpos($locserv_read['value_long'], "ioncube_loader"))
					$lls_encoder = "ioncube";
				else
					$lls_encoder = "zendguard";

				$repo_dir = "lls-".$lls_encoder;

				$paths = array();
				$structure = explode("\n",$locserv_read['value_long']);

				for($i=0; $i<count($structure); $i++)
				{
					$structure_parts = explode("|",$structure[$i]);
					$fname = $structure_parts[0];
					if(count($structure_parts) > 2)
					{
						$fsize = $structure_parts[1];
						$fdate = $structure_parts[2];
					}
					else
					{
						$fsize = "";
						$fdate = "";
					}

					$parent_folder = substr($fname,0,strrpos($fname,"/"));
					$basename = substr($fname,strrpos($fname,"/") + 1);
					$paths[] = array($parent_folder,$fname,$basename,$fsize,$fdate);

					if(($basepath=="n/a" || strlen($parent_folder) < strlen($basepath)) && trim($fname)!="")
					{
						$basepath = $parent_folder;
					}
					//$fname_parts = explode("/",$fname);
					//for($n=1; $n<count($fname_parts); $n++)
					//{
					//	$folder = $fname_parts[$n];
					//	if(!isset($path_part[$folder]))
					//}
				}
				echo selectFoldersForUpdate("poslavu_".$dataname."_db","?mode=manage_customers&submode=$submode&rowid=$rowid&locid=".$locserv_read['location'],$basepath,$paths);
				echo "<script type=text/javascript> $('#viewFolders').css('display', 'none'); </script>";
				echo "<table> <tr><td><input type='text' id='updateName' placeholder='Update Name'> </td><td><input type='submit' value='Create Update' onclick='createUpdate($(\"#updateName\").val(), \"$dataname\", \"$rowid\",\"".$locserv_read['location']."\")'></td></tr></table>";

			}
			else{
				if( can_access('technical')){
					echo "

						<td colspan=2 id='createUpdate'>
							<input type='checkbox' onchange='createFileStructureForUpdate(\"".$dataname."\",\"".$rowid."\", \"".$loc_id."\", true) '> Select Folders for Update
						</td>
						</tr></table>
					</tr>
					";
				}
			}

		/*****OLD OPTIONS, MIGHT USE LATER

		//echo "<br>Copy Files From Repo:  <input type='text' name='copy_remote_files' id='copy_remote_files'> to local/<input type='text' name='copy_local_files' id='copy_local_files'><input type='button' value='Submit' onclick='window.location = \"?mode=manage_customers&submode=$submode&rowid=$rowid&locid=".$locserv_read['location']."&encoder=".$lls_encoder."&_copy_local_files=\" + document.getElementById(\"copy_local_files\").value + \"&_copy_remote_files=\" + document.getElementById(\"copy_remote_files\").value'>";
		//echo "<br>Remove Files:  lib/<input type='text' name='remove_local_files' id='remove_local_files'><input type='button' value='Submit' onclick='window.location = \"?mode=manage_customers&submode=$submode&rowid=$rowid&locid=".$locserv_read['location']."&remove_local_files=\" + document.getElementById(\"remove_local_files\").value'>";
		//echo "<br>Upload Signature:  <input type='text' name='upload_signature' id='upload_signature'><input type='button' value='Submit' onclick='window.location = \"?mode=manage_customers&submode=$submode&rowid=$rowid&locid=".$locserv_read['location']."&upload_signature=\" + document.getElementById(\"upload_signature\").value + \"&upload_signature_type=\" + document.getElementById(\"upload_signature_type\").value'><select name='upload_signature_type' id='upload_signature_type'><option value='regular'>Regular</option><option value='waiver'>Waiver</option></select>";

		//echo "<br>Update Gateway Scripts for Heartland:  <input type='button' value='Submit' onclick='window.location = \"?mode=manage_customers&submode=$submode&rowid=$rowid&locid=".$locserv_read['location']."&update_gateway_for_heartland=1&encoder=".$lls_encoder."\"'>";
		//echo "<br>Catchup Syncing:  <input type='button' value='Submit' onclick='window.location = \"?mode=manage_customers&submode=$submode&rowid=$rowid&locid=".$locserv_read['location']."&catchup_syncing=1\"'>";

	//update orders set `lastsync`='2012-05-03 18:00:00' where closed<'2012-05-02 00:00:00'
		//echo "<br>Run Local Query: <input type='button' value='Submit' onclick='window.location = \"?mode=manage_customers&submode=$submode&rowid=$rowid&locid=".$locserv_read['location']."&run_local_query=1\"'>";
		//echo "<br>View Local Queries: <input type='button' value='Submit' onclick='window.location = \"?mode=manage_customers&submode=$submode&rowid=$rowid&locid=".$locserv_read['location']."&view_local_query=1\"'>";

		//echo "<br>Perform Records Check: <input type='button' value='Submit' onclick='window.location = \"?mode=manage_customers&submode=$submode&rowid=$rowid&locid=".$locserv_read['location']."&perform_records_check=1\"'>";
		//echo "<br>View Syncing and Records Check Status: <input type='button' value='Submit' onclick='window.open(\"index.php?mode=manage_customers&submode=view_records_check_status&notabs=1&rowid=$rowid&locid=".$loc_read['id']."\",\"\",\"width=800,height=600,resizable=yes,scrolling=yes,scrollbars=yes,toolbar=no\")'>";//window.open(\"?mode=manage_customers&submode=$submode&rowid=$rowid&locid=".$locserv_read['location']."&view_records_check_status=1\",\"\",\"width=800,height=600,resizable=yes,scorllbars=yes,toolbar=no\")'>";
		//echo "<br>Export Remote Database: <input type='button' value='Submit' onclick='if(confirm(\"Are you Sure?\")) window.location = \"?mode=manage_customers&submode=$submode&rowid=$rowid&locid=".$locserv_read['location']."&export_all_remote_data=1\"'>";
		****/
		exit;
	}

	function schedule_msync($dataname,$locationid,$sync_setting,$sync_value="",$sync_value_long="",$sync_value2=""){
		$exist_query = mlavu_query("select * from `poslavu_[1]_db`.`config` where `setting`='[2]' and `location`='[3]' and `value`='[4]' and `value_long`='[5]' and `value2`='[6]'",$dataname,
		$sync_setting,$locationid,$sync_value,$sync_value_long,$sync_value2);
		if(!mysqli_num_rows($exist_query))
			mlavu_query("insert into `poslavu_[1]_db`.`config` (`setting`,`location`,`value`,`value_long`,`value2`,`type`) values('[2]','[3]','[4]','[5]','[6]','log')",
			$dataname,$sync_setting,$locationid,$sync_value,$sync_value_long,$sync_value2);
	}

	function get_vars_from_str($str){
		$varset = array();
		$varsplit = explode("&",$str);
		for($v=0; $v<count($varsplit); $v++)
		{
			$cursplit = explode("=",$varsplit[$v]);
			if(count($cursplit) > 1)
			{
				$varset[trim($cursplit[0])] = $cursplit[1];
			}
		}
		return $varset;
	}
	function get_database_info($dbname){
		$table_list = array();
		$field_list = array();
		$table_count = 0;
		$field_count = 0;
		// creates array
		// dbinfo['tables'] = table_list
		// dbinfo['fields'] = field_list[tablename][0] = array of field names
		//                                         [1][fieldname] = associative array of field info

		$table_query = mlavu_query("SHOW tables from $dbname");
		while($table_read = mysqli_fetch_row($table_query))
		{
			$tablename = $table_read[0];
			$table_count++;

			$table_list[] = $tablename;
			$field_list[$tablename] = array(array(),array());
			$field_query = mlavu_query("SHOW columns from `[1]`.`[2]`",$dbname,$tablename);
			while($field_read = mysqli_fetch_row($field_query))
			{
				$fieldname = $field_read[0];
				$field_count++;

				$field_info = array();
				$field_info['name'] = $fieldname;
				$field_info['type'] = $field_read[1];
				$field_info['null'] = $field_read[2];
				$field_info['key'] = $field_read[3];
				$field_info['default'] = $field_read[4];
				$field_info['extra'] = $field_read[5];

				$field_list[$tablename][0][] = $fieldname;
				$field_list[$tablename][1][$fieldname] = $field_info;
			}
		}

		$dbinfo = array();
		$dbinfo['tables'] = $table_list;
		$dbinfo['fields'] = $field_list;
		$dbinfo['table_count'] = $table_count;
		$dbinfo['field_count'] = $field_count;

		return $dbinfo;
	}
	function show_folders_with_parent($rdb,$url_path,$parent,$paths,$level=0){
		global $rowid;
		global $locserv_read;
		global $repo_dir;
		if($level > 16) return "";

		$varset = get_vars_from_str($url_path);
		if(isset($varset['locid']))
			$locationid = $varset['locid'];
		else $locationid = "";

		$path_level = array();
		for($i=0; $i<count($paths); $i++)
		{
			if($paths[$i][0]==$parent)
			{
				$path_level[] = array_merge($paths[$i],array($i));
			}
		}

		if(count($path_level) < 1)
		{
			return "";
		}
		else
		{
			$str = "";
			for($i=0; $i<count($path_level); $i++)
			{
				$parent_folder = $path_level[$i][0];
				$filename = $path_level[$i][1];
				$basename = $path_level[$i][2];
				$filesize = $path_level[$i][3];
				$filedate = $path_level[$i][4];
				$id = $path_level[$i][5];
				$folder_id = "folder_".$level."_".$id;

				if($filesize=="" && $filedate=="")
				{
					$folder_icon = "icon_folder.jpg";
					$extra_code = " style='cursor:pointer' onclick='current_display = document.getElementById(\"$folder_id\").style.display; if(current_display==\"table-row\") set_display = \"none\"; else set_display = \"table-row\"; document.getElementById(\"$folder_id\").style.display = set_display'";
					$get_subdirs = true;
				}
				else
				{
					$folder_icon = "icon_blank.jpg";
					$extra_code = "";
					$get_subdirs = false;
				}

				$file_ext = "";
				if(strrpos($basename,"."))
					$file_ext = substr($basename,strrpos($basename,".") + 1);

				$str .= "<tr style='display:$set_display'>";
				$str .= "<td style='width:500px; overflow:hidden'>";
				$str .= "  <table cellspacing=0 cellpadding=2><tr>";
				if($level > 0)
					$str .= "<td style='width:".(30 * $level)."px'>&nbsp;</td>";
				$str .= "  <td valign='middle'><img src='/sa_cp/images/$folder_icon' border='0' ".$extra_code."/></td><td valign='middle' ".$extra_code.">" . $basename;
				if($filesize <= 200000 && ($file_ext!="" || strpos($basename,"htaccess")!==false))
				{
					$file_query = mlavu_query("select `value`, `value2`, `value4` from `[1]`.`config` where `location`='[2]' and `setting`='retrieved_local_file' and TRIM(`value`)='[3]'",$rdb,$locationid,trim($filename));
					if(mysqli_num_rows($file_query))
					{
						$file_read = mysqli_fetch_assoc($file_query);
						$str .= " <a href='".$url_path."&filedate=".$filedate."&read_retrieved_file=$filename' title='Updated: ".$file_read['value2']." from ".$file_read['value4']."'><font color='#000088'>(read)</font></a>";
					}
					//$str .= " <a href='".$url_path."&retrieve_local_file=$filename'><font color='#008800'>(retrieve)</font></a>";

					$repo_copy_from = str_replace("/poslavu-local/",$repo_dir."/",$filename);
					$repo_copy_to = str_replace("/poslavu-local/","../",$filename);
					//if(isset($_GET['test1']))
					//$str .= " <a href='?mode=manage_customers&submode=details&rowid=$rowid&locid=".$locserv_read['location']."&_copy_local_files=$repo_copy_to&_copy_remote_files=$repo_copy_from'>(update from repo)</a>";
					//onclick='window.location = \"?mode=manage_customers&submode=$submode&rowid=$rowid&locid=".$locserv_read['location']."&_copy_local_files=\" + document.getElementById(\"copy_local_files\").value + \"&_copy_remote_files=\" + document.getElementById(\"copy_remote_files\").value'
				}
				$str .= "</td>";
				$str .= "  </tr></table>";
				$str .= "</td>";
				$str .= "<td style='width:70px'>" . $filesize . "</td>";
				$str .= "<td style='width:160px'>" . $filedate . "</td>";
				$str .= "</tr>";

				if($get_subdirs)
				{
					$str .= "<tr style='display:none' id='$folder_id'>";
					$str .= "<td colspan='3'>";
					$str .= "<table cellspacing=0 cellpadding=0>";
					$str .= show_folders_with_parent($rdb,$url_path,$filename,$paths,$level+1);
					$str .= "</table>";
					$str .= "</td>";
					$str .= "</tr>";
				}
			}
			if($level==0)
				$str = "<td colspan=2><div style= 'height:400px; overflow:auto; border:solid 1px black;background-color:white;'><table style='height:100%' cellspacing=0 cellpadding=0>" . $str . "</table></div></td>
				";
			return $str;
		}
	}


	function selectFoldersForUpdate($rdb,$url_path,$parent,$paths,$level=0){
		global $rowid;
		global $locserv_read;
		global $repo_dir;

		if($level > 16) return "";

		$varset = get_vars_from_str($url_path);
		if(isset($varset['locid']))
			$locationid = $varset['locid'];
		else $locationid = "";

		$path_level = array();
		for($i=0; $i<count($paths); $i++)
		{
			if($paths[$i][0]==$parent)
			{
				$path_level[] = array_merge($paths[$i],array($i));
			}
		}

		if(count($path_level) < 1)
		{
			return "";
		}
		else
		{
			$str = "";
			for($i=0; $i<count($path_level); $i++)
			{
				$parent_folder = $path_level[$i][0];
				$filename = $path_level[$i][1];
				$basename = $path_level[$i][2];
				$filesize = $path_level[$i][3];
				$filedate = $path_level[$i][4];
				$id = $path_level[$i][5];
				$folder_id = "folder_".$level."_".$id;
				$file_id="file_".$level."_".$id;
				if($filesize=="" && $filedate=="")
				{
					$folder_icon = "icon_folder.jpg";
					$extra_code = " style='cursor:pointer' onclick='
					current_display = document.getElementById(\"$folder_id\").style.display;
					if(current_display==\"table-row\")
						set_display = \"none\";
					else
						set_display = \"table-row\";

					document.getElementById(\"$folder_id\").style.display = set_display'";
					$get_subdirs = true;
				}
				else
				{
					$folder_icon = "icon_blank.jpg";
					$extra_code = "style='cursor:pointer'onclick='
						if( $(\".$file_id\").css(\"background-color\")==\"rgb(176, 226, 255)\" ){
							$(\".$file_id\").css(\"background-color\", \"\");
							toUpdate(\"$filename\", \"remove\");
						}else{
							$(\".$file_id\").css(\"background-color\", \"#B0E2FF\");
							toUpdate(\"$filename\", \"add\");
						}'";

					$get_subdirs = false;
				}

				$file_ext = "";
				if(strrpos($basename,"."))
					$file_ext = substr($basename,strrpos($basename,".") + 1);

				if( $file_id!='')
					$str .= "<tr class=$file_id style='display:$set_display' $extra_code>";
				else
					$str .= "<tr class=$file_id style='display:$set_display'>";

				$str .= "<td style='width:500px; overflow:hidden'>";
				$str .= "  <table cellspacing=0 cellpadding=2><tr>";
				if($level > 0)
					$str .= "<td style='width:".(30 * $level)."px'>&nbsp;</td>";
				if( $file_id!='')
					$str .= "  <td class=$file_id valign='middle'><img src='/sa_cp/images/$folder_icon' border='0' ".$extra_code."/></td><td class=$file_id valign='middle'>" . $basename;
				else
					$str .= "  <td class=$file_id valign='middle'><img src='/sa_cp/images/$folder_icon' border='0' ".$extra_code."/></td><td class=$file_id valign='middle' ".$extra_code.">" . $basename;
				if($filesize <= 200000 && ($file_ext!="" || strpos($basename,"htaccess")!==false))
				{
					$file_query = mlavu_query("select `value`, `value2`, `value4` from `[1]`.`config` where `location`='[2]' and `setting`='retrieved_local_file' and TRIM(`value`)='[3]'",$rdb,$locationid,trim($filename));
					if(mysqli_num_rows($file_query))
					{
						$file_read = mysqli_fetch_assoc($file_query);
						$str .= " <a href='".$url_path."&filedate=".$filedate."&read_retrieved_file=$filename' title='Updated: ".$file_read['value2']." from ".$file_read['value4']."'><font color='#000088'>(read)</font></a>";
					}
					//$str .= " <a href='".$url_path."&retrieve_local_file=$filename'><font color='#008800'>(retrieve)</font></a>";

					$repo_copy_from = str_replace("/poslavu-local/",$repo_dir."/",$filename);
					$repo_copy_to = str_replace("/poslavu-local/","../",$filename);

				}
				$str .= "</td>";
				$str .= "  </tr></table>";
				$str .= "</td>";
				$str .= "<td  style='width:70px'>" . $filesize . "</td>";
				$str .= "<td  style='width:160px'>" . $filedate . "</td>";
				$str .= "</tr>";

				if($get_subdirs)
				{
					$str .= "<tr style='display:none' id='$folder_id'>";
					$str .= "<td colspan='3'>";
					$str .= "<table cellspacing=0 cellpadding=0>";
					$str .= selectFoldersForUpdate($rdb,$url_path,$filename,$paths,$level+1);
					$str .= "</table>";
					$str .= "</td>";
					$str .= "</tr>";
				}
			}
			if($level==0)
				$str = "<td colspan=2><div style= 'height:400px; overflow:auto; border:solid 1px black;background-color:white;'><table style='height:100%' cellspacing=0 cellpadding=0 >" . $str . "</table></div></td>
				";
			return $str;
		}
	}


	?>