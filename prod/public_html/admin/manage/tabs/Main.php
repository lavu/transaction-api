<?php

	/**
	 * This is the file for the main tab all we have to do
	 * is to call this file and itll echo the html for that page
	**/
	require_once($_SERVER['DOCUMENT_ROOT']."/manage/error_config.php");
	require_once("../GetCustomerInfo.php");
	require_once("../login.php");
	
	if( isset($_GET['dataname']) && $_GET['dataname']!=''){

		$dataname= $_GET['dataname'];

		if(strlen($dataname) < 40)
			makeMain($dataname);
		else
			echo "invalid dataname";
	}
	else
		echo "No data name received";

	function makeMain($dataname){

		$o_customerInfo = new GetCustomerInfoClass();

		$info=$o_customerInfo->getCustomerInfo($dataname);
			//$syncStatus= $o_customerInfo->getSyncStatus($info['id']);
			$syncStatus= $o_customerInfo->getSyncStatus($dataname);
		//echo "modules: ".print_r($info['modules'],true);
		if(count($info)!=0) {
			if( $info['disabled']=='D')
				$info['disabled']= "Account Disabled";
			else
				$info['disabled']= "Account Active";
			echo "
			<div id='MainContainer'>
				<table id='main' >
					<tr ><th colspan=2 style='background-color:#aecd37'>Data Name: ".$info['data_name']."&nbsp;&nbsp; Company Name: ".$info['company_name']." &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;".$info['disabled']."</th></tr>
					<tr>
						<td class='main'><center>".getNotes($o_customerInfo, $info['id'], $info['data_name'])."</center></td>

						<td class='main'><center>".getclientInfo($info, $o_customerInfo->getLocationInfo($dataname))."</center></td>
					</tr>
					<tr>
						<td class='main'><center>
						<table><tr><td class='main'>".getClientControl($info['id'], $info['company_code'])."</td>";
						if($syncStatus=='In Sync')
							echo "<td class='main' style='border-color:green; border-width:3'>".getSyncDatabaseStruct($syncStatus, $info['id'], $dataname)."</td></tr></table>";
						else
							echo "<td class='main' style='border-color:red; border-width:3'>".getSyncDatabaseStruct($syncStatus, $info['id'], $dataname)."</td></tr></table>";
						echo"</center>
						</td>
						";
						echo "<td class='main'>".getlogo($info['data_name'], $info['logo_img'])."</td>";


					echo "</tr>

					<tr>
						<td colspan=2 class='main'>".getDeviceInfo($o_customerInfo, $info['id'], $info['data_name'])."</td>
					</tr>
					<tr>
						<td class='main'>".getLocationInfo($o_customerInfo, $info['data_name'], $info['id'])."</td>
						";

					$module_list = $info['modules'];
					{
						lavu_connect_dn($dataname,'poslavu_'.$dataname.'_db');
						$modules_info_result = lavu_query("SELECT * FROM `poslavu_[1]_db`.`config` WHERE `setting` = 'modules' AND `type`='location_config_setting' AND '_deleted'=0 AND `location`!=0", $dataname );
						if(!$modules_info_result){
							error_log( "MySQL error Loading Modules: " . lavu_dberror() );
						} else if( mysqli_num_rows( $modules_info_result ) ){
							$module_info = mysqli_fetch_assoc( $modules_info_result );
							$module_list = $module_info['value'];
							mysqli_free_result( $modules_info_result );
						}
					}

					if(can_access('modules')) {
						//echo "THE MODULES ARE: ". $info['modules'];
						echo "<td class='main' style='text-align:left;'>".getModules($info['data_name'], $module_list, $info['id'])." </td>";
					} else {
						echo "<td class='main'>".getModulesList($info['data_name'], $module_list)."</td>";
					}
					echo "
					</tr>
					<tr>
						<td colspan=2 class='main'>".getSignupInfo($o_customerInfo, $info['data_name'])."</td>
					</tr>

				</table>
			</div>
			";
		}
		else{
			echo"<br>No data for this account found. Please try again.<br>";
		}
	}
	function printNextZendeskUpdate($dataname) {

		// get the time of the next update
		if (!isset($loggedin_id))
			$loggedin_id = TRUE;
		require_once(dirname(__FILE__).'/../../cp/resources/contact_zendesk.php');
		return "<font style='font-size:12px;'>".ZENDESK::next_update_timer_tostr()."</font>";
	}
	function getNotes($o_customerInfo, $id, $dataname) {
		require_once(dirname(__FILE__).'/Notes.php');
		return AccountNotes::getNotes($id, $dataname);
	}
	function getLogo($dataname,$logo){
		if($logo == 'poslavu.png'){
			$fpath = getenv('S3IMAGEURL').'images/poslavu.png';
		}else{
			$fpath = "/images/".$dataname."/".$logo;
		}

		$logo="
			<table id='logo'>
				<tr>
					<td>
						<img src='".$fpath."' style='max-width:200px; max-height:200px' >
					</td>
				</tr>
			</table>
		";
		return $logo;
	}

	function getClientControl($id, $companyCode)
	{
		$inappButton	= (can_access('inapp_management'))?"<td>".buttonInappManagement()."</td>":"";
		$clientControl	= "<table id='clientControl'>
			<tr><th colspan=4 class='areaHeader'> Client Control </th></tr>
			<tr>
				<td> <button class='buttonClass' onclick='window.open(\"misc/MainFunctions/custLogin.php?site=old&mode=login&custID=".$id."&custCode=".$companyCode."\", \"_blank\");  window.focus(); ' >Client Control Panel</button></td>
				<td> <button class='buttonClass' id='createTempAccount'   onclick='getTempAccountInfo(\"create_temp\",\"".$id ."\",\"".$companyCode."\",\"".$_SESSION['admin_username']."\",true) '>Create Temp Account</a></button></td>
				<td> <select class='buttonClass' id='tunnel_select_id'>
				<option value='1'> Always On Tunnel </option>
				<option value='2'> LSVPN Tunnel </option>
				<option value='3'> RSSH Tunnel 1</option>
				<option value='4'> RSSH Tunnel 2</option>
				<option value='5'> RSSH Tunnel 3</option>
				<option value='6'> RSSH Tunnel 4</option>
				<option value='7'> RSSH Tunnel 5</option>
				<option value='8'> RSSH Tunnel 6</option>
				<option value='9'> RSSH Tunnel 7</option>
				<option value='10'> RSSH Tunnel 8</option>
				<option value='11'> RSSH Tunnel 9</option>
				<option value='12'> RSSH Tunnel 10</option>
				<option value='13'> RSSH Tunnel 11</option>
				<option value='14'> RSSH Tunnel 12</option>
				<option value='15'> RSSH Tunnel 13</option>
				<option value='16'> RSSH Tunnel 14</option>
				<option value='17'> RSSH Tunnel 15</option>
				<option value='18'> RSSH Tunnel 16</option>
				<option value='19'> RSSH Tunnel 17</option>
				<option value='20'> RSSH Tunnel 18</option>
				<option value='21'> RSSH Tunnel 19</option>
				<option value='22'> RSSH Tunnel 20</option>
				<option value='23'> RSSH Tunnel 21</option>
				<option value='24'> RSSH Tunnel 22</option>
				<option value='25'> RSSH Tunnel 23</option>
				<option value='26'> RSSH Tunnel 24</option>
				<option value='27'> RSSH Tunnel 25</option>
				<option value='28'> RSSH Tunnel 26</option>
				<option value='29'> RSSH Tunnel 27</option>
				<option value='30'> RSSH Tunnel 28</option>
				<option value='31'> RSSH Tunnel 29</option>
				<option value='32'> RSSH Tunnel 30</option>
				</select> </td>
				".$inappButton."
			</tr>
		</table>";

		return $clientControl;
	}
	function buttonInappManagement() {
		global $dataname;

		// get the data on the restaurant from the database
		require_once(dirname(__FILE__).'/../../cp/resources/core_functions.php');
		$rest_query = mlavu_query("SELECT `id` FROM `poslavu_MAIN_db`.`restaurants` WHERE `data_name`='[dataname]'", array('dataname'=>$dataname));
		lavu_connect_dn($dataname, 'poslavu_'.$dataname.'_db');
		$server_query = lavu_query("SELECT * FROM `poslavu_[dataname]_db`.`users` WHERE `_deleted`='0' && `active`='1' LIMIT 1", array('dataname'=>$dataname));
		$device_query = lavu_query("SELECT * FROM `poslavu_[dataname]_db`.`devices` WHERE `inactive`='0' ORDER BY `id` DESC LIMIT 1", array('dataname'=>$dataname));
		if ($rest_query === FALSE || mysqli_num_rows($rest_query) == 0 || $server_query === FALSE || mysqli_num_rows($server_query) == 0 || $device_query === FALSE || mysqli_num_rows($device_query) == 0)
			return '';
		$rest_read = mysqli_fetch_assoc($rest_query);
		$server_read = mysqli_fetch_assoc($server_query);
		$device_read = mysqli_fetch_assoc($device_query);

		// parse the data from the database
		$id = $rest_read['id'];
		$cc = $dataname.'_key_'.lsecurity_key($id).':'.date('Ymd').'-1';
		$m = '26';
		$server_id = $server_read['id'];
		$server_name = $server_read['username'];
		$mac = $device_read['MAC'];
		$uuid = $device_read['UUID'];

		// parse some other data
		$date = date('Y-m-d');
		$datetime = date('Y-m-d H:i:s');

		// draw the button
		return "<button class='buttonClass' onclick='openInappManagement(\"$dataname\", \"$cc\",\"$m\", this);'>In-App Management</button><div style='display:none;'></div>
		<script type='text/javascript'>
			function openInappManagement(dn, cc, m, button) {
				$.ajax({
					url: '/lib/json_connect.php?loc_id=1&cc='+cc+'&m='+m,
					async: false,
					cache: false,
					type: 'POST',
					data: { cc: cc, m: m, loc_id: 1 },
					success: function(message) {
						var jdiv = $(button).siblings('div');
						jdiv.html('');
						var path = \"/manage/misc/Standalones/inapp_management_frontpage.php?loc_id=1&MAC={$mac}&server_id={$server_id}&UUID={$uuid}&register=receipt&cc={$cc}&lavu_lite=0&access_level=4&available_DOcmds=alert,confirm,open_drawer,print,startListeningForSwipe,stopListeningForSwipe&server_id={$server_id}&username={$server_name}\";
						//jdiv.append('<a href='+path+' target=\"_blank\" style=\"color:#aecd37;\">Open</a>');
						window.open(path, '_blank');
						jdiv.show();
						jdiv.css({ display: 'inline-block' });
					}
				});
			}
		</script>";
	}
	function getClientInfo($info, $locationInfo){
		global $dataname;
		$sessrand = (isset($_SESSION['rand']))?$_SESSION['rand']:"";
		$rand = base64_encode($sessrand);
		$locationCount = count( $locationInfo );
		$componentPackageCode = "";
		if($locationCount == 0) {
			return "No accounts could be found matching that dataname.";
		}
		$color="";
		lavu_connect_dn($dataname,'poslavu_'.$dataname.'_db');
		$modules_info_result = lavu_query("SELECT modules FROM `poslavu_MAIN_db`.`restaurants` WHERE `data_name` = '[1]'", $dataname );
		if(!$modules_info_result){
			error_log( "MySQL error Loading Modules: " . lavu_dberror() );
		} else if( mysqli_num_rows( $modules_info_result ) ){
			$module_info = mysqli_fetch_assoc( $modules_info_result );
			$module_list = $module_info['modules'];
			mysqli_free_result( $modules_info_result );
		}
		$module_lists =explode(",",$module_list);
		if (in_array("extensions.ordering.+posse",$module_lists)||in_array("extensions.ordering.*",$module_lists) || in_array("extensions.*",$module_lists)) {
			$color="#C24227";
		}
		if($locationCount == 1 ){
			//echo "<pre>" . print_r($info, true) . "</pre>";
			$singleLocation = array_values($locationInfo);
			$singleLocation = $singleLocation[0];
			$currentPackage = $singleLocation['component_package_code'];
			$editComponentPackageOptions = "";

			require_once(dirname(dirname(dirname(__FILE__))) . '/sa_cp/tools/convert_product.php' );
			$component_packages = get_product_list();

			if(empty($currentPackage)){

				$editComponentPackageOptions .= "<option value=''>Pick One to Change</option>";
				foreach($component_packages as $component_package){
					if($component_package['available']){
						$editComponentPackageOptions .= "<option value='{$component_package['name']}'>{$component_package['name']}</option>";
					}
				}
			} else {
				foreach( $component_packages as $component_package ) {
					if( $component_package['comcode'] == $currentPackage ) {
						$currentPackage = $component_package['name'];
					}
				}
				$editComponentPackageOptions = "<option value='{$currentPackage}'>{$currentPackage}</option>";
				$editComponentPackageOptions .= "<option value='remove'>(remove)</option>";
			}

			$editComponentPackageCode = <<<HTML
			<td id='editComponentPackage' style='display:none;'>
				<select name='editComponentPackage'>
					{$editComponentPackageOptions}
				</select>
			</td>
HTML;
			$componentPackageCode = <<<HTML
			<tr>
				<td>Comp Package</td>
				<td id='infoComponentPackage'>{$currentPackage}</td>
				{$editComponentPackageCode}
			</tr>
HTML;
		}

		$clientInfo="
			<div style='cursor:pointer; color:blue;' onclick='doRefresh(\"Main\", \"".$info['data_name']."\")'>
				 (refresh page)
				</div>
			<table id='clientInfo'>
			<tr><th colspan=2 style='cursor:pointer;background: $color;' class='areaHeader' onclick= 'toggleClientInfoEditMode()'>Client Info</th></tr>
				<tr >
					<td> ID</td>
					<td style='display:block' id= 'infoid'  > ".$info['id']." </td>
				</tr>
				<tr>
					<td> Company Name</td>
					<td> ".$info['company_name']." </td>
				</tr>
				<tr>
					<td background-color='green'> Data Name</td>
					<td> ".$info['data_name']."</td>
				</tr>
				<tr>
					<td> Account Created</td>
					<td> ".$info['created']."</td>
				</tr>
				<tr>
					<td>Last Activity</td>
					<td> ".$info['last_activity']."</td>
				</tr>
				<tr>
					<td>Chain</td>
					<td id= 'infoChain'> ".$info['chain_name']."</td>
					<td id= 'editChain' style='display:none'> <input type= 'text' name='editChainName' value='".$info['chain_name']."'/></td>
				</tr>
				<tr>
					<td>Num Locations</td>
					<td>{$locationCount}</td>
				</tr>
				{$componentPackageCode}
				<tr>
					<td>License</td>
					<td> ".$info['package']." </td>
				</tr>

				<tr>
					<td> Status</td>
					<td> ".$info['status']." </td>
				</tr>
				<tr>
					<td> Name</td>
					<td id='infoName'> ".$info['firstname']."  ".$info['lastname']."</td>
					<td id='editName'style='display:none'>
					<input type= 'text' name='editFName' value='".$info['firstname']."' />
					<input type= 'text' name='editLName' value='".$info['lastname']."' />
					</td>
				</tr>
				<tr>
					<td> Email</td>
					<td id='infoEmail' style='display:block'> ".$info['email']." </td>
					<td id='editEmail'style='display:none'> <input type= 'text' name='editEmail' value='".$info['email']."' /></td>
				</tr>
				<tr>
					<td> Phone</td>
					<td id='infoPhone' style='display:block'>  ".$info['phone']." </td>
					<td id='editPhone'style='display:none'> <input type= 'text' name='editPhone' value='".$info['phone']."'/></td>
				</tr>
				<tr>
					<td> Address</td>
					<td id='infoAddress' style='display:block'>  ".$info['address']." </td>
					<td id='editAddress'style='display:none'> <input type= 'text' name='editAddress' value='".$info['address']."'/></td>
				</tr>
				<tr>
					<td> Distro Code</td>
					<td id='infoDistro' style='display:block'>  ".$info['distro_code']." </td>
					<td id='editDistro'style='display:none'> <input type= 'text' name='editDistro' value='".$info['distro_code']."' /></td>
				</tr>";

				if($info['arb_license_start'] > date("Y-m-d")){
				$clientInfo.="
				<tr>
					<td>
					Extend Trial
					</td>
					<td>
						<table bgcolor='#bbbbbb' style='border:solid 1px #888888' cellspacing=1 cellpadding=1>
						<td style='padding-left:6px; padding-right:12px'>
							<input type='button' onclick='ext_days = document.getElementById(\"extend_count\").value;
								if(ext_days==\"\")
									alert(\"Choose how many days to extend first dude!\");
								 else {
								 	if(confirm(\"Are you sure you want to extend by \"+ext_days+\" days?\"))
								 		extendBilling('".$info['id']."',ext_days);
								 }'
								 value='Extend' style='font-size:10px;'>
							<select id='extend_count' style='font-size:10px'>
								<option value=''></option>
								<option value='1'>1</option>
								<option value='2'>2</option>
								<option value='3'>3</option>
								<option value='4'>4</option>
								<option value='5'>5</option>
								<option value='6'>6</option>
								<option value='7'>7</option></select>
						</td>
					</table></td>
				</tr>";
				}
				if (can_access('toggle_acct_enabled')) {
					$clientInfo.="<tr>
						<td colspan=2><center>";

						if($info['disabled']=='Account Disabled')
								$clientInfo.= "<div style='color:red; cursor:pointer' onclick=\"enableOrDisableAccount('".$info['id']."', 'enable', '".$info['data_name']."'); this.setAttribute('onclick', '');\">Enable</div>";
							else
								$clientInfo.="<div style='color:green;cursor:pointer' onclick=\"enableOrDisableAccount('".$info['id']."', 'disable', '".$info['data_name']."'); this.setAttribute('onclick', '');\">Disable</div>";

						$clientInfo.="</center></td>
					</tr>";
				}
				$clientInfo .= "
				<tr colspan=2>
				<td id='userSave' style='display:none'><input type='submit' onclick='saveClientInformation()'> </td>
				</tr>

			</table>
			<script type='text/javascript'>
			var chainArray=[";
				$clientInfo.=implode(",",get_chains());

			$clientInfo.= <<<JAVASCRIPT
];
			$('[name=editChainName]').autocomplete({
					source:chainArray,
					change:
						function(event,ui){
							$(this).val((ui.item ? ui.item.id : ''));
						}

			});

			function saveClientInformation(){
				saveUserInfo(
					$("input[name=editChainName]").val(),
					$("input[name=editFName]").val(),
					$("input[name=editLName]").val(),
					$("input[name=editEmail]").val(),
					$("input[name=editPhone]").val(),
					$("input[name=editAddress]").val(),
					$("input[name=editDistro]").val(),
					"{$info['data_name']}"
				);

				var component_package_select = $("select[name=editComponentPackage]");
				if( component_package_select && component_package_select.val() &&  component_package_select.children().length > 1){
					setComponentPackage( component_package_select.val(), "{$info['id']}", '{$rand}' );
				}
			}

			function toggleClientInfoEditMode() {
				if( $("#infoChain").css("display") == "none"){

				        $("#infoChain").css("display", "block")
				        $("#editChain").css("display", "none");

				        $("#infoEmail").css("display", "block");
				        $("#editEmail").css("display", "none");

				        $("#infoPhone").css("display", "block");
				        $("#editPhone").css("display", "none");

				        $("#infoAddress").css("display", "block");
				        $("#editAddress").css("display", "none");

				        $("#infoDistro").css("display", "block");
				        $("#editDistro").css("display", "none");

				        $("#infoName").css("display", "block");
				        $("#editName").css("display", "none");

				        $("#userSave").css("display", "none");

				        if( $("#infoComponentPackage") && $("#editComponentPackage") ) {
				        	$("#infoComponentPackage").css("display", "block");
				        	$("#editComponentPackage").css("display", "none");
				        }

				    }else{
				        $("#infoChain").css("display", "none");
				        $("#editChain").css("display", "block");

				        $("#infoEmail").css("display", "none");
				        $("#editEmail").css("display", "block");

				        $("#infoPhone").css("display", "none");
				        $("#editPhone").css("display", "block");

				        $("#infoAddress").css("display", "none");
				        $("#editAddress").css("display", "block");

				        $("#infoDistro").css("display", "none");
				        $("#editDistro").css("display", "block");

				        $("#infoName").css("display", "none");
				        $("#editName").css("display", "block");
				        $("#userSave").css("display", "block");

				        if( $("#infoComponentPackage") && $("#editComponentPackage") ) {
				        	$("#infoComponentPackage").css("display", "none");
				        	$("#editComponentPackage").css("display", "block");
				        }
				    }
			}


			</script>
JAVASCRIPT;

		$use_net_path = 0;
		$net_path = "https://admin.poslavu.com";
		foreach($locationInfo as $location){
			if($location['_disabled'] != '1' ){
				$use_net_path = $location['use_net_path'];
				$net_path = $location['net_path'];
			}
		}

		if(can_access('technical') && ($use_net_path != '0' && stripos($net_path, "poslavu.com") === FALSE && stripos($net_path, "lavutogo.com") === FALSE )){
			$key = lsecurity_key($info['id']);
			$dataname = $info['dataname'];
			$clientInfo .= <<<HTML
				<a href="https://lsvpn.poslavu.com/local/tools.php?cc={$dataname}_key_{$key}" target="_blank" >(Click for LSVPN Tools)</a>
HTML;
		}

		return $clientInfo;
	}
	function get_chains(){
		$return_arr=array();
		$get_chains = mlavu_query("SELECT `id`, `name` FROM `poslavu_MAIN_db`.`restaurant_chains` ORDER BY `name` ASC");
		while ($chain_info = mysqli_fetch_assoc($get_chains)) {
			$return_arr[]="\"".($chain_info['name'])."\"";
		}
		return $return_arr;
	}
	function getSyncDatabaseStruct($syncStatus, $rowid, $dataname){

		$databaseStruct="
			<table id = 'databaseStruct'>
				<tr>
					<th> Sync Database Structure</th>
				</tr>
				<tr>
					<td>";

						if($syncStatus=="In Sync")
							$databaseStruct.="<center> ".$syncStatus."</center> ";
						else{
							$databaseStruct.="<br>Not available on weekends<br>";
							//$databaseStruct.="<center><button onclick='performSync(\"".$rowid."\", \"".$dataname."\")'>".$syncStatus."</button></center>";

						}
					$databaseStruct.="</td>
				</tr>
			</table>

		";
		return $databaseStruct;
	}
	function getDeviceInfo($o_customerInfo, $id, $dataname){

		$deviceInfo="
			<div class= 'scrollableDevices'>
			<table class='DeviceInfo'>
			<tr><th colspan=11 class='areaHeader'>Device Info</th></tr>
				<tr >
					<th class='smallHeader'>Name/IP</th>
					<th class='smallHeader'>SSID</th>
					<th class='smallHeader'>Model</th>
					<th class='smallHeader'>iOS</th>
					<th class='smallHeader'>App</th>
					<th class='smallHeader'>Version (Build)</th>
					<th class='smallHeader'>Location</th>
					<th class='smallHeader'>Order Prefix</th>
					<th class='smallHeader'>Identifier</th>
					<th class='smallHeader'>Last Checkin</th>
					<th class='smallHeader'>Device Time</th>
					<th class='smallHeader'>Check-in Engine</th>
					<th class='smallHeader'>Last Reload Settings</th></tr>

				";

			$devices= $o_customerInfo->getDevices($id, $dataname);

			foreach( $devices as $device){
					if ($device['MAC'] != "")
						$display_identifier = $device['MAC'];
					else if ($device['UUID'] != "")
						$display_identifier = $device['UUID'];
					else
						$display_identifier="N/A";

				$check_in_engine = ($device['check_in_engine'] == "dataplex")?"DataPlex":"JSON Connect";

				$deviceInfo.="
					<tr style='font-size: smaller;'>
						<td >".$device['name']."/".$device['ip_address']."</td>
						<td>".$device['SSID']."</td>
						<td>".$device['model']."</td>
						<td>".$device['system_version']."</td>
						<td>".$device['app_name']."</td>
						<td>".$device['poslavu_version']."</td>
						<td>".$device['location_name']."</td>
						<td>". str_pad($device['prefix'], 2, "0", STR_PAD_LEFT)."</td>
						<td>".$display_identifier."</td>
						<td>".$device['last_checkin']."</td>
						<td>".$device['device_time']."</td>
						<td>".$check_in_engine."</td>
						<td id=".$device['ip_address'].">".$device['last_reload']."</td>";

					$deviceInfo.="</tr>";

			}

			$deviceInfo.="</table></div>";
			return $deviceInfo;
	}
	function getLocationInfo($o_customerInfo,$dataname, $id){
		$locations= $o_customerInfo->getLocationInfo($dataname);

		$locationInfo="
			<table id='LocationInfo'>
			<tr><th colspan=12 class='areaHeader'>Location Info</th></tr>
				<tr>
					<th class='smallHeader'>ID</th>
					<th class='smallHeader'>Name</th>
					<th class='smallHeader'>City</th>
					<th class='smallHeader'>State</th>
					<th class='smallHeader'>Country</th>
					<th class='smallHeader'>Phone</th>
					<th class='smallHeader'>Manager</th>
					<th class='smallHeader'>Gateway</th>
					<!--<th>Debug Gateway</th>
					<th class='smallHeader'>LS Ipad Tunnel </th>
					<th class='smallHeader'>Request Server info</th> -->
					<th class='smallHeader'>Disable</th>

				</tr>";
				foreach ($locations as $location){

					$locationInfo.="
					<tr style='font-size: smaller;'>
						<td>".$location['id']."</td>
						<td>".$location['title']."</td>
						<td>".$location['city']."</td>
						<td>".$location['state']."</td>
						<td>".$location['country']."</td>
						<td>".$location['phone']."</td>
						<td>".$location['manager']."</td>
						<td>".$location['gateway']."</td>";
						$enable_disable = "Disable";

						if ($location['_disabled'] == "1") $enable_disable = "Enable";
						$locationInfo.="<td style='padding-left:5px'><input type='button' onclick='window.open(\"misc/MainFunctions/disableLocation.php?dataname=".$dataname."&rowid=".$id."&".$enable_disable."_location=".$location['id']."\")' value=".$enable_disable."></td>
					</tr>
					";
				}
			$locationInfo.="</table>";
		return $locationInfo;
	}
	function getSignupInfo($o_customerInfo, $dataname){
		$signups= $o_customerInfo->getSignupInfo($dataname);

		$getSignupInfo="<center>

		<table id='signupInfo'>
		<tr><th colspan=6 class='areaHeader'>Signup Info</th></tr>

				<tr>
					<th>ID</th>
					<th>Name</th>
					<th>City</th>
					<th>State</th>
					<th>Country</th>
					<th>Phone</th>
				</tr>";
			if(count($signups)>0){
				foreach ($signups as $signup){
					$getSignupInfo.="
						<tr>
							<td>".$signup['id']."</td>
							<td>".$signup['firstname']." ".$signup['lastname']."</td>
							<td>".$signup['city']."</td>
							<td>".$signup['state']."</td>
							<td>".$signup['country']."</td>
							<td>".$signup['phone']."</td>
						</tr>
					";
				}
			}
			$getSignupInfo.="</table></center>";
		return $getSignupInfo;
	}
	function getModulesList($dataname, $modules) {
		$retString = "";
		$retString.= "<br />Module List:<br />";
		$retString.= "<div id='module_list' style='width: 250px; overflow: auto;  padding-left:10px;'>";
		$retString.= "<pre>";
		$mods = explode(",", $modules);
		for($i = 0; $i < count($mods); $i++)
		{
			$retString.= $mods[$i] . "\n";
		}
		$retString.= "</pre>";
		$retString.= "</div>";
		return $retString;
	}
	function getModules($dataname, $modules, $id){
		//require_once($_SERVER['DOCUMENT_ROOT'].'/sa_cp/modules.php');
		if(can_access("modules")){

			$retString='';
			require_once($_SERVER['DOCUMENT_ROOT']."/lib/modules.php");

			$module_array = array("*" => "*");
			foreach($available_modules as $key => $mod)
			{
				$sections = explode(".", $mod);
				$acc = "";
				for($i = 0; $i < count($sections); $i++)
				{
					if($acc != "")
						$acc .= ".";
					$acc .= $sections[$i];
					if($i < (count($sections) - 1))
						$module_array[$acc . ".*"] = $acc . ".*";
				}
				$module_array[$mod] = $mod;
			}
			$retString.= getModulesList($dataname, $modules);
			$retString.= "<input id=\"modules\" type=\"hidden\" value=\"" . $modules . "\" style=\"width: 250px;\" onchange=\"updateModuleList($(this).val()));\" />";
			$retString.= "<input type='button' onclick='saveModules(\"".$id."\",\"".$dataname."\")' value='Save Modules'>";
			$retString.= "<div style='width: 400px; height: 100px; overflow: auto;'>";
				foreach($module_array as $mod)
				{
					$retString.= "<div class= 'modList' onclick='enableModule(\"$mod\");'>+</div>";
					$retString.= "<div class= 'modList' onclick='disableModule(\"$mod\");'>-</div>";
					$retString.= "<div class= 'modList' onclick='deleteModule(\"".$mod."\");'>D</div>";
					$retString.= "&nbsp;<label>$mod</label><br />";
				}
			$retString.= "</div>";

			return $retString;
		}
	}

?>
