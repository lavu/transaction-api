<?php


?>
<script type="text/javascript" src="./js/Tabs/distroSearch.js"></script>
<div class='distributor_area'>
	
	<div class='search_container'>
		<input type='text' class='distributor_search' /> <input type='checkbox' class='certified_box' name='is_certified'>is certified
		<span id='add_reseller' onclick="add_reseller()" style='cursor:pointer'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Add Reseller</span>
	</div>
	
	<div class='distro_result_container'>
		<table class='distro_result_table'>
			<tr class='distro_result_tr_header'>
				<th class='distro_result_th' >
					Name
				<th class='distro_result_th'>
					Company
				</th>
				<th class='distro_result_th'>
					Address
				</th>
				<th class='distro_result_th'>
					City
				</th>
				<th class='distro_result_th'> 
					State
				</th>
				<th class='distro_result_th'>
					Country
				</th>
				<th class='distro_result_th'>
					Postal Code
				</th>
				<th class='distro_result_th'>
					Phone
				</th>
				<th class='distro_result_th'>
					Email
				</th>
				<th class='distro_result_th'>
					Website
				</th>
	
				<th class='distro_result_th'>
					Signup Date
				</th>
				<th class='distro_result_th'>
					Certified
				</th>
				<th class='distro_result_th'>
					Edit
				</th>
			</tr>
			
		</table>
		<div class='distro_edit'>
			<form id='distro_edit_form'>
				<table>
					<tr>
						<th colspan="2">
							DISTRO EDIT
						</th>
					</tr>
					<tr>
						<td>First Name</td>
						<td><input type='text' name='f_name' val=''> </td>
						
					</tr>
					<tr>
						<td>Last Name</td>
						<td><input type='text' name='l_name' val=''> </td>
					</tr>
					<tr>
						<td>Company</td>
						<td><input type='text' name='company' val=''> </td>
					</tr>
					<tr>
						<td>Address</td>
						<td><input type='text' name='address' val=''> </td>
					</tr>
					<tr>
						<td>City</td>
						<td><input type='text' name='city' val=''> </td>
					</tr>
					<tr>
						<td>State</td>
						<td><input type='text' name='state' val=''> </td>
					</tr>
					<tr>
						<td>Country</td>
						<td><input type='text' name='country' val=''> </td>
					</tr>
					<tr>
						<td>Postal Code</td>
						<td><input type='text' name='postal_code' val=''> </td>
					</tr>
					<tr>
						<td>Phone</td>
						<td><input type='text' name='phone' val=''> </td>
					</tr>
					<tr>
						<td>Email</td>
						<td><input type='text' name='email' val=''> </td>
					</tr>
					<tr>
						<td>Additional Info</td>
						<td><textarea name='notes' cols='20' rows='10'> </textarea> </td>
					</tr>
					<tr>
						<td>Website</td>
						<td><input type='text' name='website' val=''> </td>
					</tr>
					<tr>
						<td>Is Certified</td>
						<td><input type='text' name='isCertified' val=''> </td>
					</tr>
					<tr>
						<td>Username</td>
						<td><input type='text' name='username' val=''> </td>
					</tr>
					<tr>
						<td>Password</td>
						<td><input type='text' name='password' val=''> </td>
					</tr>
					<tr>
						<td>Commision Percent</td>
						<td><input type='text' name='distro_license_com' val=''> </td>
					</tr>
					<tr><td><input type='hidden' name='id'></td><td> <input type='button' value='Save Distro' onclick='save_distro($("#distro_edit_form"))'/></td></tr>
				</table>
			</form>
		</div> 
		
	</div>
</div> 
