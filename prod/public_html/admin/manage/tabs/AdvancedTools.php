<?php
	require_once($_SERVER['DOCUMENT_ROOT']."/manage/error_config.php");
	require_once("../login.php");

	$lls_list_vars = array();

	if(isset($_POST['fromManage'])){
		buildAdvancedTools();
	}else{
		echo "you do not have access to this page.";
		exit;
	}
	if(!isset($maindb)) {
		$maindb = "poslavu_MAIN_db";
	}

	function buildAdvancedTools(){
		$default=2;
		if( isset($_POST['username']))
			$default= get_default_tab($_POST['username']);

		echo "
		<div id='advancedToolsArea'>
				
			<div id='advancedToolsHeader' style='display: none;'>Advanced Tools </div>
			<div id='advancedToolsTabContainer'>
			<div id='advancedToolsTabScroller'>";
			$buttons_arr= array(//"getMigrateTools"   => array("access"=>'technical','title'=>"Migrate Account"), //Depricated.
								"getMassDatabase"     => array("access"=>array("technical", "database_menu_backup", "transfer_settings"),'title'=>"Mass Database"),
								//"addExtension" 	    => array("access"=>"tecnhical","title"=>"Extensions"),
								"getLavuLiteStats"    => array("access"=>array("company_reporting","satisfaction_survey","customers"),"title"=>"Company Stats"),
								"listLocalServers"    => array("access"=>"customers", "title"=>"Local Server Stats"),
								//"report_builder"	=> array("access"=>"create_reports", "title"=>"Report Creator"),
								/*"viewPaymentHistory" => array("access"=>"customers","title"=>"View Payment History"),*/
								"distributorsTools" => array("access"=>"distributor_tools","title"=>"Distributor Tools"),
								"editModules"		=>array("access"=>"module","title"=>"Edit Modules"),
								//"createAccount"	  	=>array("access"=>"create_account","title"=>"Create Account"),
								"uploadDocs"	 	=>array("access"=>"upload_documents","title"=>"Upload Documents"),
								//"build_website"		=>array("access"=>"website_builder","title"=>"Subsite Builder"),
								"backendMessages"	=>array("access"=>"add_backend_messages","title"=>"Customer Messages"),
								"change_backend_video"	=>array("access"=>"customers","title"=>"Change Backend Video"),
								"activeCustomersReport"    => array("access"=>"customers", "title"=>"Active customers"),
						);

			build_buttons($buttons_arr, $default);
			echo "</div></div>
			<div id= 'advancedToolsContent' style='overflow-y:auto;'></div>
		</div>
		<script type='text/javascript'>
			resize_buttons();
		</script>
		";
	}
	function build_buttons($buttons_arr, $default){
		$num_tabs=1;

		foreach($buttons_arr as $button=>$info){
			$can_access=false;
			if(is_array($info['access']))
				$can_access= can_access_by_array($info['access']);
			else
				$can_access= can_access($info['access']);
			if($can_access){
				if( $num_tabs==$default){

					$selected= "advancedToolsTabSelected";
				}else
					$selected ="";
				$num_tabs++;
					echo"
					<div class='advancedToolsTab $selected' id='".$button."Tab' onclick='getPage(\"function=$button&in_manage=1\",\"/manage/misc/AdvancedToolsFunctions/getPage.php\",\"POST\",\"".$button."Tab\")'>
						{$info['title']}
					</div>";
			}
		}
	}

	function get_default_tab($username){
		require_once("/home/poslavu/public_html/admin/cp/objects/json_decode.php");
		$json= new Json();
		$result= mysqli_fetch_assoc(mlavu_query("SELECT `options` FROM `poslavu_MAIN_db`.`accounts` WHERE `username`='[1]' LIMIT 1", $username));
		$options= $result['options'];
		$obj= $json->decode($options);

		$obj->advanced_tools_default;
		if(isset($obj->advanced_tools_default))
			return $obj->advanced_tools_default+1;
		else
			return 2;
	}
?>
