<?php
	
	function getDocNames(){
		$path= $_SERVER['DOCUMENT_ROOT']."/manage/misc/companyDocs";
		$docs=array();
		if( is_dir($path)){
			if ($handle = opendir($path)) {
			    /* This is the correct way to loop over the directory. */
			    while (false !== ($entry = readdir($handle))) {
			    	
			    	if($entry!='..' &&  $entry!='.'){
			    		
			    		$docs[]=$entry;
			    	}
			    	
			    }
			   
			    closedir($handle);
			    return $docs;
			}else
				return "error has occurred opening the folder";
		}else
			return "Couldn't find the folder"; 
	}
	function getDocsAsTabs(){
	
		$namesArray= getDocNames();
	
		foreach($namesArray as $name){
		
			$nameExploded= explode("___", $name);
			//echo $nameExploded[0];
			$date= date("Y-m-d H:i:s",$nameExploded[0]);
			$returnString.="
			<tr> 
				<td class='compDocsTD docsTab' onclick='openDocument($(this))' name='$name'>{$nameExploded[2]} </td>
				<td class='compDocsTD docsTab' name='$name'>$date</td>
				<td class='compDocsTD docsTab sortType' onclick='sortOnType($(this))'>{$nameExploded[1]}</td>
			</tr>";
		}		
		
		return $returnString;	
	}
?>

<div id='docsTabContainer'>
	<div id='leftCompDocsContent'>
		<table id= 'docs'>
			<thead>
				<tr>
					<th class='compDocsTD' id='compDocName'>
						Name
					</th>
					<th class='compDocsTD' id='compDocDate'>
						Date
					</th>
					<th class='compDocsTD' id='compDocType'>
						Type
					</th>
				</tr>
			</thead>
			
			<tbody class='docsTbody'>
				<?php echo getDocsAsTabs(); ?>
			</tbody>
			<tfoot>
			</tfoot>
		</table>
	</div>
	<div id='compDocsContent' >
		<object data="/manage/misc/companyDocs/AIDoc.pdf" type="application/pdf" width="100%" height="90%"></object>
	</div>
</div>
<script type='text/javascript'>
	$("#docs").ready(function(){
		 var compDocTable= $('#docs');
		 
			$('#compDocName, #compDocDate, #compDocType').each(function(){
	            
	            var th = $(this),
	                thIndex = th.index(),
	                inverse = false;
	           
	            th.click(function(){
	            		                //$('.searchColsSelected').removeClass('searchColsSelected');
	                //th.addClass('searchColsSelected');
	                compDocTable.find('td').filter(function(){
	                    
	                    return $(this).index() === thIndex;
	                    
	                }).sortElements(function(a, b){
	                    
	                    return $.text([a]) > $.text([b]) ?
	                        inverse ? -1 : 1
	                        : inverse ? 1 : -1;
	                    
	                }, function(){
	                    
	                    // parentNode is the element we want to move
	                    return this.parentNode; 
	                    
	                });
	                
	                inverse = !inverse;     
	            });     
	        });
	
	});

</script>
