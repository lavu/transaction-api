<?php
	require_once $_SERVER['DOCUMENT_ROOT'] . "/cp/resources/lavuquery.php";
    $were_updating_images =  array_key_exists( "type", $_POST ) && isset( $_POST['type'] ) && $_POST['type'] === "upload_image";
	if(isset($_POST['id']) && isset($_POST['product'])) {

	    if ( isset( $_POST['val'] ) ) {
	        $val= rawurldecode(trim($_POST['val']));
		    //echo $val;
		    if(is_numeric($_REQUEST['id']) )
			    $field="`id`";
		    else	//if the id is non numeric that means that we are using the handle 
			    $field="`handle`";
		    $query= mlavu_query("SELECT `id` FROM `poslavu_PRODUCTS_db`.`product_components` where $field = '[1]' and `product`='[2]' ", $_REQUEST['id'],$_REQUEST['product']);
		
		    if(mysqli_num_rows($query))
			    mlavu_query("UPDATE `poslavu_PRODUCTS_db`.`product_components` set `value`='[1]' where $field='[2]' and `product`='[3]' limit 1", $val, $_POST['id'], $_POST['product']);
		    else
			    mlavu_query("INSERT INTO `poslavu_PRODUCTS_db`.`product_components` (`value`,$field, `product`) VALUES('[1]','[2]', '[3]') ", $val,$_REQUEST['id'], $_POST['product']);
		    if(mlavu_dberror())
			    echo mlavu_dberror();
		    else
			    echo 'success';
	    } elseif ( $were_updating_images ) {
	        echo "We made it this far";
	    // Added 8/8/2013 by carlosa@poslavu.com
	        // First we need to check if there is an entry for handle with the current product
	        $query1 =  "SELECT `id` from `poslavu_PRODUCTS_db`.`product_components` ";
	        $query1 .= "WHERE `handle`='[1]' AND `product`='[2]' ORDER BY `id` DESC LIMIT 1";
	        $response = mlavu_query( $query1, $_POST["id"], $_POST["product"] ); // poslavu_PRODUCTS_db
	        echo $response;
	        // prepare the new value
	        $val = http_build_query( array(
	                "img" => $_POST["img"],
	                "alt" => $_POST["alt"],
	                "width" => $_POST["width"],
	                "height" => $_POST["height"],
	                "class" => $_POST["class"],
	               "id" => $_POST["html_id"]
	            ) );
	        // update or make a new entry
	        if ( $response ){
	            $query  = "UPDATE `poslavu_PRODUCTS_db`.`product_components` ";
	            $query .= "SET `value`='[1]' WHERE `handle`='[2]' AND `product`='[3]' LIMIT 1";
	            $val = http_build_query( array(
	                "img" => $_POST["img"],
	                "alt" => $_POST["alt"],
	                "width" => $_POST["width"],
	                "height" => $_POST["height"],
	                "class" => $_POST["class"],
	               "id" => $_POST["html_id"]
	            ) );
	            echo mlavu_query( $query, $val, $_POST["id"], $_POST["product"] ); // poslavu_PRODUCTS_db
	            echo $val;
	        } else {
	            echo "Making new entry ";
	            $query  = "INSERT INTO `poslavu_PRODUCTS_db`.`product_components` ";
	            $query .= "( `value`, `handle`, `product` ) ";
	            $query .= "VALUES ( '[1]', '[2]', '[3]' )";
	            mlavu_query( $query, $val, $_POST["id"], $_POST["product"] ); // poslavu_PRODUCTS_db
	        }
	    }
	} else {
		echo "failed: Vars not sent";
	}
?>