<?php
	require_once($_SERVER['DOCUMENT_ROOT']."/cp/resources/lavuquery.php");
	
	if(isset($_POST['id']) && isset($_POST['function'])){
		$_POST['function']($_POST['id']);
	}else if(isset($_POST['function'])){
		
		$function_name= $_POST['function'];
		unset($_POST['function']);
	
		$function_name($_POST);
	
	}else{
		echo "ERROR: info not found";
	}
	function component_order($id){
		$selected_components = get_selected_components($id);
		$start='<div class="order_form_container"><form name="order_form" id="order_form_id"><table> ';
		$files= scandir($_SERVER['DOCUMENT_ROOT']."/manage/subsite_builder/components/html/");
		$selected_list=array();
		foreach($files as $file){
		
			if( $file != '.' && $file != '..'){
			
				if(isset($selected_components->$file))
					$selected_list[$selected_components->$file]="<tr><td><input type='text' value='{$selected_components->$file}' name='$file' class='text_box_resize'/></td><td> $file</td></tr>";
				else	
					$returnVal.="<tr><td><input type='text' value='' name='$file' class='text_box_resize'/></td><td>$file</td></tr>";
			}
		}
		ksort($selected_list);
		echo $start.implode(" ", $selected_list).$returnVal."<tr><td><input type='hidden' name='product_id' value='{$_POST['id']}' /></td>
		<td><input type='button' value='submit' onclick='save_info($(\"#order_form_id\"))'/></td></tr></table></form></div>";
	}
	function niche_email_editor($info){
		$result= mysqli_fetch_assoc(mlavu_query("SELECT * FROM `poslavu_PRODUCTS_db`.`info` WHERE `id`= '[1]'", $info['id']));
		$contents= rawurldecode($result['niche_email']);
		$output= "
		<form id='file_editor_form'> 
			<textarea id='code' name='code'>$contents</textarea>
			<input type='hidden' value='{$info['id']}' name='prod_id' /> <br/> 
			<input value='Save Email' type='button' onclick='save_email(\"{$info['id']}\")'>
			<input value='Preview'   type='button' onclick='preview_email(\"{$info['id']}\")'>
		</form>";
		
		$output.= '<script>
			var mixedMode = {name: "htmlmixed",scriptTypes: [{matches: /\/x-handlebars-template|\/x-mustache/i,mode: null},{matches: /(text|application)\/(x-)?vb(a|script)/i,mode: "vbscriptz"}]};
			var editor = CodeMirror.fromTextArea(document.getElementById("code"), {mode: mixedMode, tabMode: "indent"});
			
		</script>';	
		echo $output;	
	}
	function save_component_order($info){
		
		$product_id=$info['product_id']; 
		unset($info['product_id']);
		
		$vals_array= array();
		
		foreach($info as $key=>$val){
			if($val){
				$page= str_replace("_html", ".html", $key);
				$vals_array[]='"'.$page.'":"'.$val.'"';
			}	
		}
		$insert_val= "{".implode(",", $vals_array)."}";
	
		mlavu_query("UPDATE `poslavu_PRODUCTS_db`.`info` SET `components_used`='[1]' where `id`='[2]' LIMIT 1 ",$insert_val, $product_id);
		if( mlavu_dberror())
			echo mlavu_dberror();
		else
			echo "success";
	}
	function save_email($info){

		mlavu_query("UPDATE `poslavu_PRODUCTS_db`.`info` SET `niche_email`='[1]' WHERE `id`='[2]' LIMIT 1 ",$info['code'],$info['prod_id']);
		if( mlavu_dberror())
			echo mlavu_dberror();
		else
			echo "success";
	}
	
	function get_selected_components($product_id){
		
		require_once($_SERVER['DOCUMENT_ROOT']."/cp/objects/json_decode.php");
		
		$json    = new Json();
		$result  = mysqli_fetch_assoc(mlavu_query("SELECT `components_used` FROM `poslavu_PRODUCTS_db`.`info` WHERE `id`='[1]' ", $product_id));
		$decoded = $json->decode($result['components_used']);

		return $decoded;
	}
	function uploaded_images($prod_id){
		$files= scandir($_SERVER['DOCUMENT_ROOT']."/manage/subsite_builder/images/template_images/niche_layout");
		$return_str= '<div class="img_container"><ul>';
		
		foreach($files as $file){
			if( $file != '.' && $file != '..')
				$return_str.="<li class= 'component_editor_list' onclick='show_image($(this),\"$file\")'>".$file."</li>";
		}
		$return_str.='</ul></div>';
		$return_str .= '<form action="#" method="post" class="img_uploader" ';
		$return_str .= 'onsubmit="return !img_uploader($( \'#fileToUpload\' ),\'plain_img_upload\')">';
		$return_str .= '<input id="fileToUpload" name="fileToUpload" type="file" class="img_uploader_input">';
		$return_str .= '<input type="submit" value="Upload Image" />';
		$return_str .= '</div>';
		
		echo $return_str;
		
	}
	function get_image_list($prod_id){
		
		$files= scandir($_SERVER['DOCUMENT_ROOT']."/manage/subsite_builder/images/template_images/niche_layout");
		
		$counter=0;
		$arr=array();
		foreach($files as $file){
			$arr[]='"'.$counter.'":"'.$file.'"';
			$counter++;
		}
		$str="{".implode(",", $arr)."}";
		
		echo $str;
	
		
	}
	function component_editor($id){
		$html_files= scandir($_SERVER['DOCUMENT_ROOT']."/manage/subsite_builder/components/html");
		$css_files= scandir($_SERVER['DOCUMENT_ROOT']."/manage/subsite_builder/components/css");
		$return_str= '<div class="editor_container" style="margin: 0 auto; height:80%; overflow-y:auto"><div class= "css_list"> <ul>';
		foreach($css_files as $file){
			if( $file != '.' && $file != '..' && !strstr($file, "__deleted"))
				$return_str.="<li class= 'component_editor_list' '> <span onclick='call_php($(this).html(),\"get_component_editor\")'>".$file."</span></li>";
		}
		$return_str.="</ul>
					</div>";
		
		$return_str.= '<div class= "html_list"> <ul>';
		foreach($html_files as $file){
			if( $file != '.' && $file != '..' && !strstr($file, "__deleted"))
				$return_str.="<li class= 'component_editor_list' ><span oncontextmenu='javascript:show_preview(\"$file\");return false;' onclick='call_php($(this).html(), \"get_component_editor\")'>".$file."</span></li>";
		}
		$return_str.="</ul></div></div>
		<div class='global_template_editor' style='padding-left:20px'>
				GLOBAL TEMPLATE EDIT
			<ul style='display:inline-block'>
				<li  class= 'component_editor_list' ><span onclick='call_php($(this).html(), \"get_layout_editor\")'>niche_layout.css</span></li>
			</ul>
			<ul style='display:inline-block'>
				<li class= 'component_editor_list' ><span onclick='call_php($(this).html(), \"get_layout_editor\")'>niche_layout.tpl</span></li>
			</ul>
		</div>
		<div class='component_options_container' style='padding-left:20px'> 
				<input type='text' id='new_comp_name' placeholder='component name' /> 
				<input type='button' value='New Component' onclick='create_new_component($(\"#new_comp_name\").val())' />
				<input type='button' value='delete' class='delete_comp_btn' onclick='delete_component($(this))'/>
				<input type='button' value='reset component' onclick='reset_component_contents($(this))'/> 
			</div>";	
		
		echo $return_str;
	}
	function create_new_component($name){
		
		$counter=0;
		if(!isset($name['name']))
			$name='new_component';
		else
			$name= $name['name'];
			
		while(file_exists($_SERVER['DOCUMENT_ROOT']."/manage/subsite_builder/components/css/$name.css")){
			$counter++;
			$name.=$counter;
		}	
	
		file_put_contents($_SERVER['DOCUMENT_ROOT']."/manage/subsite_builder/components/css/$name.css","");
		file_put_contents($_SERVER['DOCUMENT_ROOT']."/manage/subsite_builder/components/html/$name.html", "");
		component_editor("");
	}
	function get_component_editor($name){
		$dir='';
		$folder='html';
		if(stristr($name, "css"))
			$folder='css';
			
		$dir= $_SERVER['DOCUMENT_ROOT']."/manage/subsite_builder/components/$folder/$name";
		
		$contents= htmlspecialchars(file_get_contents($dir));
		
	
		
		$output= "
		<form id='file_editor_form'> 
			<textarea id='code' name='code'>$contents </textarea>
			<input type='hidden' value='$dir' name='path_to_file' /> <br/> 
			<input value='Save file' type='button' onclick='save_file(\"$dir\")'>
		
		</form>";

		
		$output.= '<script>
			var mixedMode = {name: "htmlmixed",scriptTypes: [{matches: /\/x-handlebars-template|\/x-mustache/i,mode: null},{matches: /(text|application)\/(x-)?vb(a|script)/i,mode: "vbscriptz"}]};
			var editor = CodeMirror.fromTextArea(document.getElementById("code"), {mode: mixedMode, tabMode: "indent"});
			
		</script>';	
		echo $output;	
	}
	function get_layout_editor($name){
		
		$dir= $_SERVER['DOCUMENT_ROOT']."/manage/subsite_builder/$name";
		if(stristr($name, "css"))
			$dir= $_SERVER['DOCUMENT_ROOT']."/manage/subsite_builder/css/$name";
		
		$contents= htmlspecialchars(file_get_contents($dir));
		
		$output= "
		<form id='file_editor_form'> 
			<textarea id='code' name='code'>$contents </textarea>
			<input type='hidden' value='$dir' name='path_to_file' /> <br/> 
			<input value='Save file' type='button' onclick='save_file(\"$dir\")'>
		
		</form>";

		
		$output.= '<script>
			var mixedMode = {name: "htmlmixed",scriptTypes: [{matches: /\/x-handlebars-template|\/x-mustache/i,mode: null},{matches: /(text|application)\/(x-)?vb(a|script)/i,mode: "vbscriptz"}]};
			var editor = CodeMirror.fromTextArea(document.getElementById("code"), {mode: mixedMode, tabMode: "indent"});
			
		</script>';	
		echo $output;	
	}
	function save_component_edit($comp_text){
		
		$dir=rawurldecode($comp_text['dir']); 
		$val=rawurldecode($comp_text['code']);
		addHandles($val);
		
		if( file_put_contents($dir, $val))
			echo "saved";
		else
			echo "Error saving file. Dir is: ". $dir;
	}
	function addHandles($text){
		$editable_areas= get_between($text);
		foreach($editable_areas as $editable){
			$type_and_handle= explode("::", $editable);
			$default_val='';
			
			if( $type_and_handle[0]=='text')
				$default_val='[DEFAULT TEXT]';
			else if( $type_and_handle[0]=='textarea')
				$default_val='[DEFAULT TEXTAREA CONTENT]';
			else if($type_and_handle[0]=='upload_image')
				$default_val='blank_img.png';
			else 
				$default_val='[UNKNOWN EDITOR TYPE]';
			
			$query= mlavu_query("SELECT * FROM `poslavu_PRODUCTS_db`.`product_components` where `handle`='[1]' and `product`=''", $type_and_handle[1]);
			if(mysqli_num_rows($query)==0){
				mlavu_query("INSERT INTO `poslavu_PRODUCTS_db`.`product_components` (`value`, `handle`,`deleted`) VALUES ('[1]','[2]','0')", $default_val, $type_and_handle[1]);
			}else
				mlavu_query("UPDATE `poslavu_PRODUCTS_db`.`product_components` SET `value`='[1]' WHERE `handle`='[2]' limit 1", $default_val, $type_and_handle[1]);
			if (mlavu_dberror())
				echo mlavu_dberror();
			else
				echo"success";
		}
	}
	function get_advanced_info($id){
		$status_array= array("Select a Status", "Testing","Deleted", "Live");
		$info= mysqli_fetch_assoc(mlavu_query("SELECT * FROM `poslavu_PRODUCTS_db`.`info` where `id`='[1]'", $id));	
		
		$return_str="
			<form id='advanced_info_form'> 
				<table style='margin: 0 auto;'>
					<tr> 
						<th colspan=100>Advanced Info </th>
					</tr>
					<tr>
						<td>
							Development Status: 
						</td>
						<td>
							<select name='status'>";
							for($x=0; $x<4; $x++){
								if( $info['status']==$x)
									$return_str.="<option value='$x' selected>{$status_array[$x]}</option>";
								else
									$return_str.="<option value='$x'>{$status_array[$x]}</option>";
							}
			  $return_str.="</select>
						</td>
					</tr>
					<tr>
						<td>
							Domain Name:
						</td>
						<td>
							<input type='text' name= 'domain_name' value='{$info['domain_name']}' placeholder='domain name' />
						</td>
					</tr>
					<tr>
						<td>
							Title:
						</td>
						<td>
							<input type='text' name= 'title' value='{$info['title']}' placeholder='title' />
						</td>
					</tr>
					<tr>
						<td>
							Product Name:
						</td>
						<td>";
				
				if($info['product_name']!='unnanmed_product')
					$return_str .="<input type='text' name= 'product_name' value='{$info['product_name']}' placeholder='product_name' readonly/>";
				else
					$return_str .="<input type='text' name= 'product_name' value='{$info['product_name']}' placeholder='product_name' />";
				$return_str.="
						</td>
					</tr>
					<tr>
						<td>Edit Meta Tags</td><td><textarea name='meta_tags'>{$info['meta_tags']}</textarea></td>
					</tr>
					<tr>
						<td>
							<input type='hidden' name='component_id' value='$id'>
							<input type='button' onclick='submit_advanced_info($(\"#advanced_info_form\"))' value='submit'>
						</td>
					</tr>
				</table>				
			</form>
			<table style='margin:0 auto'>
				<tr><td><input type='button' onclick='publish_product(\"$id\")' value='Publish Product'/></td><td><input type='button' onclick='publish_to_main(\"$id\", \"{$info['product_name']}\")' value='Publish to Main'/></td></tr>
			</table>";
			
		echo $return_str;
		
	}
	function save_advanced_info($form){
		mlavu_query("UPDATE `poslavu_PRODUCTS_db`.`info` SET `status`='[1]', `domain_name`='[2]', `title`='[3]', `product_name`='[4]',`meta_tags`='[6]' where `id`='[5]' LIMIT 1", 
		$form['status'],$form['domain_name'],$form['title'], $form['product_name'], $form['component_id'], $form['meta_tags']);
		
		if (mlavu_dberror())
			echo mlavu_dberror();
		else
			echo 'success';
	}
	function create_new_product(){
		$query= mlavu_query("INSERT INTO `poslavu_PRODUCTS_db`.`info` (`template_file`, `title`, `components_used`, `status`, `domain_name`, `product_name`) VALUES('niche_layout.tpl', '', '', '1', '','unnanmed_product'); ");
		if (mlavu_dberror())
			return "ERROR:".mlavu_dberror();
		else
			return 'success';
	}
	function change_product($prod){
		$result= mysqli_fetch_assoc(mlavu_query("SELECT * FROM `poslavu_PRODUCTS_db`.`info` where `product_name`='[1]' limit 1", $prod));
		echo $result['id'];
	}
	function update_image_selection($vars){
		$product=$vars['product'];
		$val=$vars['picture'];
		$id= $vars['prod_id'];
		//echo "product is: ".$product." picture is: ". $val." id is: ". $id;
		$query=mlavu_query("UPDATE `poslavu_PRODUCTS_db`.`product_components` set `value`='[1]' where `product`='[2]' and `handle`='[3]'", $val, $product, $id);
		if(!ConnectionHub::getConn('poslavu')->affectedRows())
			mlavu_query("INSERT INTO `poslavu_PRODUCTS_db`.`product_components` (`value`,`product`,`handle`) VALUES ('[1]', '[2]','[3]') ", $val, $product, $id);
		
		if (mlavu_dberror())
			echo mlavu_dberror();
		else
			echo "success";
			
	}
	function delete_component($vals){
		$names= explode(".",$vals['text']);
		exec("mv /home/poslavu/public_html/admin/manage/subsite_builder/components/css/".$vals['text']." /home/poslavu/public_html/admin/manage/subsite_builder/components/css/".$vals['text']."__deleted");
		exec("mv /home/poslavu/public_html/admin/manage/subsite_builder/components/html/".$names[0].".html /home/poslavu/public_html/admin/manage/subsite_builder/components/html/".$names[0].".html__deleted");
		echo "rm /home/poslavu/public_html/admin/manage/subsite_builder/components/css/".$vals['text'];
	}
	function reset_component($vals){
		$file_name= explode(".",$vals['text']);
		$to_reset = get_between(file_get_contents("/home/poslavu/public_html/admin/manage/subsite_builder/components/html/".$file_name[0].".html"));
		$prod_name= get_prod_name($vals['prod_id']);
		foreach($to_reset as $handle_and_type){
			$exploded_arr= explode("::", $handle_and_type);
			//echo "handle: ". $exploded_arr[1]. " prod_name: ". $prod_name. " \n";
			$res= mlavu_query("DELETE FROM `poslavu_PRODUCTS_db`.`product_components` WHERE `handle`='[1]' AND `product`='[2]' LIMIT 1", $exploded_arr[1], $prod_name);
			if(mlavu_dberror()){
				echo mlavu_dberror();
			}else
				echo "reset successful";
		}
	}
	function get_prod_name($id){
		$res= mysqli_fetch_assoc(mlavu_query("SELECT `product_name` FROM `poslavu_PRODUCTS_db`.`info` where `id`='[1]' limit 1", $id));
		return $res['product_name'];
	}
	function get_between($contents){
		$sPattern = "/<editable>(.*?)<\/editable>/s";
		preg_match_all($sPattern,$contents,$aMatch);
		return $aMatch[1];
	}
	function img_scroller_editor($id){
		$files= scandir($_SERVER['DOCUMENT_ROOT']."/manage/subsite_builder/images/template_images/niche_layout");
		
		$selected_files= get_selected_images($id);
		//echo print_r($selected_files,true);
				
		echo "
			<div class='img_scroller_editor_container'> 
				<form name='img_scroller_form' id='img_scroller_form_id'>
					<table>
						<tr>
							<th colspan=2>
								Images to be used for this Sub-product's image_scroller
							</th>
						</tr>
						<tr>
							<td>First Image: </td>
							<td>
								<select class='first_img_select' name='first_img'>";
								echo'<option value="">No Image</option>';
								if(isset($selected_files[0]))
									$file= $selected_files[0];
								else
								 	$file='';
								foreach($files as $f_name){
									if($f_name!='.' && $f_name!='..'){
										if($f_name == $file)
											$selected= 'selected';
										else
											$selected='';
										echo "<option value='$f_name' $selected> $f_name</option>";
									}
								}
							
							
								echo "</select>
							</td>
						</tr>
						<tr>
							<td>Second Image: </td>
							<td>
								<select class='second_img_select' name='second_img'>
								";
								echo'<option value="">No Image</option>';
								if(isset($selected_files[1]))
									$file= $selected_files[1];
								else
								 	$file='';
								foreach($files as $f_name){
									if($f_name!='.' && $f_name!='..'){
										if($f_name == $file)
											$selected= 'selected';
										else
											$selected='';
										echo "<option value='$f_name' $selected> $f_name</option>";
									}
								}
							
								echo"
								</select>
							</td>
						</tr>
						<tr>
							<td>Third Image: </td>
							<td>
								<select class='third_img_select' name='third_img'>
								";
								echo'<option value="">No Image</option>';
								if(isset($selected_files[2]))
									$file= $selected_files[2];
								else
								 	$file='';
								foreach($files as $f_name){
									if($f_name!='.' && $f_name!='..'){
										if($f_name == $file)
											$selected= 'selected';
										else
											$selected='';
										echo "<option value='$f_name' $selected> $f_name</option>";
									}
								}
							
								echo"
								</select>
							</td>
						</tr>
						<tr>
							<td>Fourth Image: </td>
							<td>
								<select class='fourth_img_select' name='fourth_img'>";
								echo'<option value="">No Image</option>';
								if(isset($selected_files[3]))
									$file= $selected_files[3];
								else
								 	$file='';
								foreach($files as $f_name){
									if($f_name!='.' && $f_name!='..'){
										if($f_name == $file)
											$selected= 'selected';
										else
											$selected='';
										echo "<option value='$f_name' $selected> $f_name</option>";
									}
								}
							
								echo"
								</select>
							</td>
						</tr>
						<tr>
							<td>Fifth Image: </td>
							<td>
								<select class='fifth_img_select' name='fifth_img'>
								";
								echo'<option value="">No Image</option>';
								if(isset($selected_files[4]))
									$file= $selected_files[4];
								else
								 	$file='';
								foreach($files as $f_name){
									if($f_name!='.' && $f_name!='..'){
										if($f_name == $file)
											$selected= 'selected';
										else
											$selected='';
										echo "<option value='$f_name' $selected> $f_name</option>";
									}
								}
							
								echo"
								</select>
							</td>
						</tr>
						<tr>
							<td><input type='hidden' name='prod_id' value='$id'/></td><td><input type='button' value='submit' onclick='save_img_scroller($(\"#img_scroller_form_id\"))'/></td>
						</tr>
						
					</table>
				</form>			
			</div>
		";
	}
	function get_selected_images($id){
		$prod_name= get_prod_name($id);
		$query=mlavu_query("SELECT * FROM `poslavu_PRODUCTS_db`.`product_components` where `product`='[1]' and `handle`='img_scroller' limit 1", $prod_name);
		if(mysqli_num_rows($query)){
			$vals= mysqli_fetch_assoc($query);
			return explode(",", $vals['value']);
			
		}else
			return null;
	}	
	function save_img_scroller($images){
		$img_list='';
		$id= $images['prod_id'];
		unset($images['prod_id']);
		echo print_r($images);
		foreach( $images  as $key=>$val){
			if($val=='')
				unset($images[$key]);
		}
		$images= implode(",",$images);
		$prod_name= get_prod_name($id);
		$query= mlavu_query("SELECT * FROM `poslavu_PRODUCTS_db`.`product_components` WHERE `product`='[1]' and `handle`='img_scroller'", $prod_name);
		if( mysqli_num_rows($query))
			mlavu_query("UPDATE `poslavu_PRODUCTS_db`.`product_components` SET `value`= '[1]' where `product`='[2]' and `handle`='img_scroller' limit 1", $images, $prod_name);
		else
			mlavu_query("INSERT INTO `poslavu_PRODUCTS_db`.`product_components` (`product`, `value`,`handle`,`deleted`) VALUES ('[1]', '[2]', 'img_scroller', '0')", $prod_name, $images);
		
		if( mlavu_dberror())
			echo mlavu_dberror();
		else
			echo "success";
	}
	function get_preview($file_name){
		$folder='css';
		$file_name=$file_name['file_name'];
		if(stristr($file_name, ".html"))
			$folder="html";
		
		
		$css  = file_get_contents("/home/poslavu/public_html/admin/manage/subsite_builder/components/css/$file_name");
		$html = file_get_contents("/home/poslavu/public_html/admin/manage/subsite_builder/components/html/$file_name");
		require_once("/home/poslavu/public_html/admin/manage/subsite_builder/components/components.php");
		$comp = new comp($html, $css, '');
		echo $comp->get_component();
	}
	
?>
