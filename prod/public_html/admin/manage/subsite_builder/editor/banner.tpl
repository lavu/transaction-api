<style>
	.editor_banner_container{
		position:fixed;
		top:0px;
		left:0px;
		width:100%;
		background-color:#aecd37;
		height:60px;
		z-index:100;
		overflow-y: hidden;
	}
	.editor_banner_product_container{
		
		position:relative;
		width:80%;
		padding-left:10px;
		overflow-x:auto;
		height:100%;
		overflow-y:hidden;

	}
	
	.editor_banner_product{
		float:left;
		border-right:1px solid black;
		color:white;		
		padding-top:20px;
		height:100%;
		padding-right:10px;
		padding-left:10px;
	}
	.editor_banner_product_selected{
		float:left;
		background-color:white;
		border-right:1px solid black;
		color:#aecd37;		
		padding-top:20px;
		height:100%;
		padding-right:10px;
		padding-left:10px;
	}
	.editor_banner_product:hover{
		background-colo:white;
		color:black;
		cursor:pointer;
	}
	.editor_banner_product_settings{
		position: relative;
		float: right;
		border-right: 1px solid black;
		color: white;
		padding-top: 20px;
		height: 100%;
		padding-right: 30px;
		padding-left: 10px;
		top: -60px;
	}
	.editor_banner_product_settings:hover{
		cursor: pointer;
		color : black;
	}
	.editor_banner_product_new{
		position: relative;
		float: right;
		border-right: 1px solid black;
		border-left: 1px solid black;
		color: white;
		padding-top: 20px;
		height: 100%;
		padding-right: 30px;
		padding-left: 10px;
		top: -60px;
	}
	.editor_banner_product_new:hover{
		cursor: pointer;
		color : black;
	}
	
	#detailsEditWindowPositioner{
		display: none;
		position: fixed;
		width: 100%;
		height: 100%;
		z-index: 100;
		margin-left: -7px;
	}
	#detailsEditWindow{
		position: relative;
		z-index: 10;
		top: 20px;
		display: inherit;
		width: 100%;
		height: 643px;
		margin: 0 auto;
		background-color: rgba(0,0,0,0.75);
		border: 1px solid gray;
		font-family: sans-serif,verdana;
		font-weight: 100;
	}
	#closeDetailsBtn{
		text-align: right;
		float: right;
		margin-top: 5px;
		margin-right: 16px;
		background-color: gray;
		width: 11px;
		height: 16px;
		color: white;
		border: 1px solid #aecd37;
	}
	#detailsHeader{
		padding-top: 15px;
		text-align: center;
		font-size: x-large;
		color: white;
	}
	#detailsContent{
		position: relative;
		width: 100%;
		height: 439px;
		background-color: white;
		top: 88px;
		overflow:auto;
	}
	#detailsTabContainer{
		position: inherit;
		width: 100%;
		top: 99px;
		margin: 10px;
		margin-left: 0px;
		overflow:auto;
		background-color:#aecd37;
	}
	.detailsTabScroller{
		width:1700px;
	}
	#closeDetailsBtn:hover{
		color: black;
		cursor: pointer;
	}
	.detailsTab{
		display:inline-block;
		float:left;
		background-color:#aecd37;
		padding-left:30px;
		padding-right:30px;
		text-align:center;
		padding-top:10px;
		padding-bottom:10px;
		color:white;
	}
	.detailsTab:hover{
		background-color:white;
		color:#aecd37;
		
	}
	.detailsTabSelected{
		background-color:white;
		color:#aecd37;
	}
	#info_id{
		display:none;
	}
	.text_box_resize{
		width:20px;
	}
	.html_list{
		position:relative;
		width:45%;
		overflow:auto;
		float:left;
	}
	.css_list{
		position:relative;
		width:45%;
		overflow:auto;
		float:left;
	}
	.component_options_container{
		position:relative;
		top:10px;
	}
	.component_editor_list:hover{
		cursor:pointer;
		background-color:#aecd37;
		color:white;
	}
	.img_container{
		height: 80%;
		overflow-y: auto;
	}
	.img_uploader{
		padding-top:30px;
	}
	.detailsTab{
		color:white;
		background-color:#aecd37;
		cursor:pointer;
	}
	.detailsTab:hover{
		color:#aecd37;
		background-color:white;
		cursor:pointer;
	}
	.details_tab_selected{
		color:#aecd37;
		background-color:white;
		display:inline-block;
		float:left;
		padding-left:30px;
		padding-right:30px;
		text-align:center;
		padding-top:10px;
		padding-bottom:10px;
		
	}

</style>


<link rel="stylesheet" href="/manage/subsite_builder/js/plugins/codemirror/lib/codemirror.css">
<script type="text/javascript">window.jQuery || document.write( '<script type="text/javascript" src="http://code.jquery.com/jquery.min.js"><\/script>' )</script>
<script type="text/javascript" src= '/manage/subsite_builder/js/editor_functions.js'></script>
<script type='text/javascript' src= '/manage/subsite_builder/js/plugins/codemirror/lib/codemirror.js'></script>
<script type="text/javascript" src= '/manage/subsite_builder/js/scripts/ajaxImageUpload.min.js'></script>
<script type='text/javascript' src= '/manage/subsite_builder/js/plugins/codemirror/mode/htmlmixed/htmlmixed.js'></script>
<script src="/manage/subsite_builder/js/plugins/codemirror/mode/xml/xml.js"></script>
<script src="/manage/subsite_builder/js/plugins/codemirror/mode/javascript/javascript.js"></script>
<script src="/manage/subsite_builder/js/plugins/codemirror/mode/css/css.js"></script>
<script src="/manage/subsite_builder/js/plugins/codemirror/mode/vbscript/vbscript.js"></script>



<div class='editor_banner_container'>
	<div class='editor_banner_product_container'><!--PRODUCTS--></div>
	<div class= 'editor_banner_product_settings' onclick='open_editor_settings()'>Settings</div>
	<div class='editor_banner_product_new' onclick='create_new_product()'>New Product</div>
	
</div>

<div id= 'detailsEditWindowPositioner'>
	<div id='info_id' product_id= '<!--PRODUCT_ID-->'> </div>
	<div id='detailsEditWindow'>
		<span id="closeDetailsBtn" onclick="hideDetailsWindow()">X</span>
		<div id="detailsHeader">Edit Product Details </div>
		
		<div id='detailsTabContainer'>
			<div class='detailsTabScroller'>
				<div class='advanced_info detailsTab'    onclick='showDetails($(this), $("#info_id").attr("product_id"))' name = 'advanced_info'>        Advanced Info</div>
				<div class='images detailsTab'           onclick='showDetails($(this), $("#info_id").attr("product_id"))' name = 'show_uploaded_images'> Uploaded Images</div>
				<div class='component_order detailsTab'  onclick='showDetails($(this), $("#info_id").attr("product_id"))' name = 'component_order'>      Component Selecting and Order</div>
				<div class='component_editor detailsTab' onclick='showDetails($(this), $("#info_id").attr("product_id"))' name = 'component_editor'>     Component Editor</div>
				<div class='img_scroller detailsTab'     onclick='showDetails($(this), $("#info_id").attr("product_id"))' name = 'img_scroller_editor'>  Image Scroller Editor</div>
				<div class='niche_email detailsTab'      onclick='showDetails($(this), $("#info_id").attr("product_id"))' name = 'niche_email_editor'>   Niche Email Editor</div>
			</div>
		</div>
		<div id='detailsContent'></div>	
	</div>	
</div>

