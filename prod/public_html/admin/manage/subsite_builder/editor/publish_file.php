<?php
//ini_set('display_errors',1);
set_error_handler('handleError');
// "Main" 
require_once($_SERVER['DOCUMENT_ROOT']."/cp/resources/lavuquery.php");
if(!isset($_POST['prod_id']) ||!isset($_POST['html'])){
    echo "No product id sent.";
    return;
}else{
    $id= $_POST['prod_id'];
    $html= rawurldecode($_POST['html']);
    //echo $html;
    send_data($id, $html);
}

function handleError($errno, $errstr, $errfile, $errline, array $errcontext){
    // error was suppressed with the @-operator
    if (0 === error_reporting()) {
        return false;
    }
    echo "<pre>". $errstr. "<br/></pre>";
    //throw new ErrorException($errstr, 0, $errno, $errfile, $errline);
}

function send_data($id, $html){

    $prod_info    = get_product_info($id);
    if(!$prod_info['domain_name']){
        echo "You have not yet specified a domain name. Please do so then publish";
        exit;
    }else if(!$prod_info['product_name']){
        echo "You have not yet specified a product name. Please do so then publish";
        exit;
    }else if(!$prod_info['status']){
        echo "You have not yet specified a development status. Please do so then publish";
        exit;
    }
    $files_array  = get_files_to_zip_list($html);
    $include_list = get_include_list($html);
    
    try {
       copy_images_over($files_array);

    }
    catch (ErrorException $e) {
        // ...
    }

    copy_includes_over($include_list);
    write_info_file($prod_info);
    $result       = file_put_contents("./temp/index.html", $html);
    $prod_name    = zipFolder($prod_info['product_name']);
    performcURL($prod_name);	
    move_and_delete_zip($prod_name);
    echo "Successfully transferred product. Please check url in a few minutes to see your published site. ";
}

function performcURL($prod_name){
		
		//This part performs the scp
		echo shell_exec("/bin/bash \n sshpass -p 'tnk4zig' scp /home/poslavu/public_html/admin/manage/subsite_builder/editor/temp/$prod_name.zip poslavu@poslavu.com:~/public_html/poslavu.com/niche_products/zip_drop");
		
} 

function move_and_delete_zip($prod_name){
	date_default_timezone_set('America/Chicago');
	exec("cp /home/poslavu/public_html/admin/manage/subsite_builder/editor/temp/$prod_name.zip /home/poslavu/public_html/admin/manage/subsite_builder/editor/zips/".$prod_name."_".date("YmdHis").".zip");
    //error_log("mv /home/poslavu/public_html/admin/manage/subsite_builder/editor/temp/$prod_name.zip /home/poslavu/public_html/admin/manage/subsite_builder/editor/zips/".$prod_name."_".date("YmdHis").".zip");
}

function get_product_info($id){
	$result= mysqli_fetch_assoc(mlavu_query("SELECT * FROM `poslavu_PRODUCTS_db`.`info` where `id`='[1]'", $id));
	return $result;
}
	
//	function process_image( )

/**
 * #get_files_to_zip
 * Function that gets all the images and other stuff and chenges 
 * the url to make it in the published location.
 */
function get_files_to_zip_list(&$html){
    $return_array= array();
	$dom = new domDocument;
	$dom->loadHTML($html);
	$dom->preserveWhiteSpace = false;
	$images = $dom->getElementsByTagName('img');
	foreach ($images as $image) {
		$img_with_path=$image->getAttribute('src');
		if ( strpos( $img_with_path, '&' ) ) {
		    parse_str(  $img_with_path, $img_params );
		    foreach( $img_params as $attr => $val ) {
		        if ( $attr === "img" ) {
		            $image->setAttribute( 'src', './images/$val' );
		        } else {
		            $image->setAttribute( $attr, $val );
		        }
		    }
		} elseif ( strpos( $img_with_path, '||' ) ) {
		    $img_param = explode( '||', $img_with_path );
		    $basename = basename( $img_param[0] );
		    $image->setAttribute( 'src', "./images/$basename" );
		    $image->setAttribute( 'alt', "{$img_param[1]}" );
		} else {
		    $base_name= basename($img_with_path);
		    $image->setAttribute('src',"./images/$base_name");
		}
		$html=$dom->saveHTML();
		if(!in_array($base_name, $return_array))
			$return_array[]=$base_name;
	}
	//This part is lame.. im sorry 
		
	$pieces=explode('<div class="img_scroller_pics">', $html);
	if($pieces){
		if(isset($pieces[1])){
			$content= explode("</div>", $pieces[1]);
			$images=explode(",", $content[0]);
			foreach($images as $img)
				$return_array[]= trim($img);		
		}
	}
	preg_match_all("/url\('?(.*)'?\)/", $html, $matches);
	$matches= $matches[1];
	foreach($matches as $match){
		$match= str_replace("'", "", $match);
		
		if( !in_array(basename($match), $return_array))
			$return_array[]= basename($match);
		//echo "The match is: ".$match. " The base name is: ". basename($match); 
		$html= str_replace($match, "./images/".basename($match), $html);
		
	}
	//END STUPID NESS	
	echo "<pre>".print_r($return_array,true)."</pre>";
	return $return_array; 
}

function get_include_list(&$html){

    $return_array= array();
    $dom = new domDocument;
    $dom->loadHTML($html);
    $dom->preserveWhiteSpace = false;
    //This part is the the javascript part
    $scripts = $dom->getElementsByTagName('script');
    foreach ($scripts as $script) {
        $src= $script->getAttribute('src');	
        if(!strstr($src, "http") && $src!=''){	//if this is coming from somewhere absolute. dont do anything			
            $base_name= basename($src);
            if( $base_name=='index.js')
                $base_name= 'js_index.js';
            
            $script->setAttribute('src',"./includes/$base_name");
                
            
            $html=$dom->saveHTML();
            
            $return_array[]=$base_name;
        }
    }
    //This part is the css part 
    $links = $dom->getElementsByTagName('link');
    foreach ($links as $link) {
        $href= $link->getAttribute('href');	
        $base_name= basename($href);
        $link->setAttribute('href',"./includes/$base_name");
        $html=$dom->saveHTML();
        $return_array[]=$base_name;
        
    }
    
    return $return_array;
}
	
function copy_includes_over($include_list){
    //echo print_r($include_list,true);
    $dest_dir = $_SERVER['DOCUMENT_ROOT']."/manage/subsite_builder/editor/temp/includes/"; 
    foreach($include_list as $include){
        if($include){
            if(strstr($include, ".css")){
                if(file_exists( $_SERVER['DOCUMENT_ROOT']."/manage/subsite_builder/css/".$include))
                    $source_dir = $_SERVER['DOCUMENT_ROOT']."/manage/subsite_builder/css/";
                else{
                    $source_dir = $_SERVER['DOCUMENT_ROOT']."/manage/subsite_builder/components/css/";
                }
            }else
                $source_dir = $_SERVER['DOCUMENT_ROOT']."/manage/subsite_builder/js/";

            if(!copy($source_dir.$include, $dest_dir.$include))
                echo "Failed to copy ". $include;
        }
    }
}
	
function copy_images_over($image_array){
    
    $source_dir = $_SERVER['DOCUMENT_ROOT']."/manage/subsite_builder/images/template_images/niche_layout/";
    $dest_dir   = $_SERVER['DOCUMENT_ROOT']."/manage/subsite_builder/editor/temp/images/"; 
    $replace_arr= array(")", "\""," ");
    foreach($image_array as $image){
        
        $image= str_replace($replace_arr, "", $image);
        $exploded= explode("||",$image);
        if(!copy($source_dir.$exploded[0], $dest_dir.$exploded[0])){
            //DO SOMETHING
            //echo "Failed to copy ". $image;		
        }
    }
}
	
function write_info_file($prod_info){
    
    $write_str= "domain_name:::".$prod_info['domain_name']."{+}prod_name:::".$prod_info['product_name']."{+}status:::".$prod_info['status'];
    $fh= fopen($_SERVER['DOCUMENT_ROOT']."/manage/subsite_builder/editor/temp/prod_info.txt", "w+");
    fwrite($fh, $write_str);
    fclose($fh);		
}
	
function zipFolder($prod_name){
		while(file_exists($_SERVER['DOCUMENT_ROOT']."/manage/subsite_builder/editor/temp/$prod_name.zip")){
			exec("rm -rf ".$_SERVER['DOCUMENT_ROOT']."/manage/subsite_builder/editor/temp/$prod_name.zip");
		}
		echo exec("zip -r -q ".$_SERVER['DOCUMENT_ROOT']."/manage/subsite_builder/editor/temp/$prod_name.zip  ".$_SERVER['DOCUMENT_ROOT']."/manage/subsite_builder/editor/temp");
		return $prod_name;
}

function show_error($error_message){
		
}
	
?>