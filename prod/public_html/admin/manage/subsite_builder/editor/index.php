<?php
	require_once($_SERVER['DOCUMENT_ROOT']."/manage/error_config.php");
	require_once($_SERVER['DOCUMENT_ROOT']."/manage/subsite_builder/product_factory.php");
	require_once($_SERVER['DOCUMENT_ROOT']."/manage/subsite_builder/components/components.php");
		
		
	$templateFile='';
	$product_name= '';
	$contents='';
	
	if(isset($_REQUEST['product_id'])){
		$product_id=$_REQUEST['product_id'];
	}else
		$product_id=1;
	
	$product_info= get_product_info($product_id);
	
	if(!isset($product_info['product_name']))
		$product_name='test_product';
	else
		$product_name= $product_info['product_name'];
	
	if(!isset($product_info['template_file']))
		$templateFile='niche_layout.tpl';
	else
		$templateFile= $product_info['template_file'];


	$contents=file_get_contents($_SERVER['DOCUMENT_ROOT']."/manage/subsite_builder/".$templateFile);
	$html = get_component_html($product_id, $product_name);
	
	$product_obj = new Product($product_id, $contents, $html, true);
	
	if($product_obj -> has_error()){
		echo $product_obj -> get_error();
	}else{
		echo $product_obj -> get_webpage();
	}
	
	function get_component_html($product_id, $product_name){
	
		$htmlDir= dirname(__FILE__)."/../components/html";
		$html_files= scandir($htmlDir);
		
		$cssDir= dirname(__FILE__)."/../components/css";
		$css_files= scandir($cssDir);
		$selected_components= get_selected_components($product_id);
		//echo "The selected components are: ".print_r($selected_components,true);
	
		$componentHTML='';

		$componentHTML_order=array();
		for($x=0; $x < count($html_files); $x++){
			
			if(isset($selected_components->$html_files[$x])){
				
				$ordering=$selected_components->$html_files[$x];
				 	
				$html = file_get_contents($htmlDir."/".$html_files[$x]);
				$css  = file_get_contents($cssDir."/".$css_files[$x]);
				$comp = new comp($html, $css, $product_name);
				$componentHTML=$comp->get_component();
				
				$componentHTML_order[$ordering]= $componentHTML;
			}
		}
		ksort($componentHTML_order);
		$componentHTML= implode(" ", $componentHTML_order);
		
		return $componentHTML;
	
	}
	function get_product_info($id){
		$result= mysqli_fetch_assoc(mlavu_query("SELECT * FROM `poslavu_PRODUCTS_db`.`info` where `id`='[1]' and `deleted` !='1' ", $id));
		return $result;
	}
	function get_selected_components($product_id){
		
		require_once($_SERVER['DOCUMENT_ROOT']."/cp/objects/json_decode.php");
		
		$json    = new Json();
		$result  = mysqli_fetch_assoc(mlavu_query("SELECT `components_used` FROM `poslavu_PRODUCTS_db`.`info` WHERE `id`='[1]' ", $product_id));
		$decoded = $json->decode($result['components_used']);

		return $decoded;
	}	
?>