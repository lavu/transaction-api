<?php

if ( isset ( $_POST ) ) {
    if ( array_key_exists( 'action', $_POST )  && isset( $_POST['action'] ) ) {
        $name = $_POST['component_name'];
        $html_exists = file_exists( "components/html/$name.html" );
        $css_exists = file_exists( "components/css/$name.css" );
        if ( $html_exists && $css_exists ) {
            $html = file_get_contents( "components/html/$name.html" );
            $css = file_get_contents( "components/css/$name.css" );
            $data_json = json_encode( array( "success" => true, "update" => true, "html" => $html, "css" => $css ) );
            $data = gzcompress( $data_json );
            $response = $data;
            echo $response;
            //TODO actually check the time stamp! 
        } else {
            $error = "";
            $error .= ($html_exists)? "" : "HTML file not found\n";
            $error .= ($css_exists)? "" : "CSS file not found\n";
            $response = array( "success" => false, "err_msg" => $error );
            echo json_encode( $response );
        }

    }
}

?>
