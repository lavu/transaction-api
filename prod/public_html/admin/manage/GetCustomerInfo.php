<?php
	//session_start();
	require_once(dirname(__FILE__)."/../cp/resources/lavuquery.php");
	require_once(dirname(__FILE__)."/../cp/resources/core_functions.php");
	require_once(dirname(__FILE__)."/../sa_cp/billing/package_levels_object.php");
	require_once(dirname(__FILE__)."/../sa_cp/billing/payment_profile_functions.php");
	global $o_package_container;
	if (!isset($o_package_container))
		$o_package_container = new package_container();

	class GetCustomerInfoClass {

		/**
		 * Gets information on the account's restuarant and signups rows,
		 *     the package, the disabled status ('E' or 'D'), and the modules
		 * @param  string $s_dataname The dataname for the account
		 * @return array              The information on the account
		 */
		function getCustomerInfo($s_dataname){
			global $o_package_container;

			// get restaurants/signups info
			$query= "SELECT *, `restaurants`.`id` as `id` FROM `poslavu_MAIN_db`.`restaurants` LEFT JOIN `poslavu_MAIN_db`.`signups` ON `poslavu_MAIN_db`.`restaurants`.`data_name` = `poslavu_MAIN_db`.`signups`.`dataname` WHERE `data_name`='[1]'";
			@$s_dataname = ConnectionHub::getConn('poslavu')->escapeString($s_dataname);
			$info = mysqli_fetch_assoc(mlavu_query($query, $s_dataname)); // poslavu_MAIN_db
			if($info['disabled']==0)
				$info['disabled']= "E";
			else
				$info['disabled']= "D";

			// get package
			$status = find_package_status($info['id']);
			$info['package'] = $o_package_container->get_plain_name_by_attribute('level', (int)$status['package']);

			// get modules
			if( $info['lavu_lite_server']==''){
				lavu_connect_dn($s_dataname,'poslavu_'.$s_dataname.'_db');
				$mods= mysqli_fetch_assoc(lavu_query("SELECT `value` FROM `config` WHERE `setting`='modules' "));
				$info['modules']= $mods['value'];
			}

			return $info;
		}

		function getNoteInfo($id) {
			require_once(dirname(__FILE__).'/tabs/Notes.php');
			return AccountNotes::getNoteInfo($id);
		}

		function getLavuLiteNoteInfo($id){
			if($id!=''){
					$url ='admin.lavulite.com/manage/mainServerAccess/getNotes.php';
					$fields_string= 'id='.$id.'&key=984a63690360b061bd63b8ded55538f6';
					$ch = curl_init();

					//set the url, number of POST vars, POST data
					curl_setopt($ch,CURLOPT_URL, $url);
					curl_setopt($ch,CURLOPT_POST, 2);
					curl_setopt($ch,CURLOPT_POSTFIELDS, $fields_string);
					curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
					//execute post
					$result = curl_exec($ch);

					//close connection
					curl_close($ch);
					return lavu_json_decode($result);
			}
			else
				return array();
		}

		function getDevices($id, $dataname){
			lavu_connect_dn($dataname,'poslavu_'.$dataname.'_db');
			$device_query = lavu_query("SELECT `check_in_engine`,`lavu_admin`,`prefix`,`MAC`,`UUID`,`name`,`device_time`,`ip_address`,`model`,`system_version`,`app_name`,`poslavu_version`,`last_checkin`, `UDID`,`loc_id`, `SSID`, `title` AS `location_name` FROM `devices` LEFT JOIN `locations` ON `locations`.`id` = `loc_id` WHERE `company_id`='[1]' AND `inactive` != '1' ORDER BY `last_checkin` desc ", $id);
			if( ! $device_query)
				echo lavu_dberror();
			else if(mysqli_num_rows($device_query)){
					$result=array();
				while($device_read = mysqli_fetch_assoc($device_query)){

					$device_read['last_reload']="<input type='submit' value='Get Time' onclick= \"getReloadTime('".$device_read['MAC']."','".$dataname."', '".$device_read['ip_address']."')\">";
					array_push($result, $device_read);
				}
				return $result;
			}
			else
				return array();
		}
		function getLocationInfo( $dataname){
			$loc_id_lookup = array();
			lavu_connect_dn($dataname,'poslavu_'.$dataname.'_db');
			$loc_query 	   = lavu_query("SELECT * FROM `locations`  ORDER BY `title` ASC", $dataname);
			$license_result = mysqli_fetch_assoc(mlavu_query("SELECT `package` FROM `poslavu_MAIN_db`.`signups` WHERE `dataname`='[1]'", $dataname)); // poslavu_MAIN_db

			if(mysqli_num_rows($loc_query)){
				while($loc_read = mysqli_fetch_assoc($loc_query)){
				 array_push($loc_id_lookup,$loc_read);
				}

				return $loc_id_lookup;
			}
		}
		function getSignupInfo($dataname){
			$signupInfo = array();
			if($dataname=='')
				return $signupInfo;

			$signupQuery = mlavu_query("SELECT * FROM `poslavu_MAIN_db`.`signups` WHERE `poslavu_MAIN_db`.`signups`.`dataname`='[1]'",$dataname); // poslavu_MAIN_db
			if(mysqli_num_rows($signupQuery) !=0){

				while($signupRead = mysqli_fetch_assoc($signupQuery)){
					array_push($signupInfo,$signupRead);
				}
				return $signupInfo;
			}
		}

		function get_database_info($dbname){
			$table_list = array();
			$field_list = array();
			$table_count = 0;
			$field_count = 0;
			// creates array
			// dbinfo['tables'] = table_list
			// dbinfo['fields'] = field_list[tablename][0] = array of field names
			//                                         [1][fieldname] = associative array of field info

			$table_query = mlavu_query("SHOW tables from $dbname");
			while($table_read = mysqli_fetch_row($table_query))
			{
				$tablename = $table_read[0];
				$table_count++;

				$table_list[] = $tablename;
				$field_list[$tablename] = array(array(),array());
				$field_query = mlavu_query("SHOW columns from `[1]`.`[2]`",$dbname,$tablename);

				while($field_read = mysqli_fetch_row($field_query))
				{
					$fieldname = $field_read[0];
					$field_count++;

					$field_info = array();
					$field_info['name'] = $fieldname;
					$field_info['type'] = $field_read[1];
					$field_info['null'] = $field_read[2];
					$field_info['key'] = $field_read[3];
					$field_info['default'] = $field_read[4];
					$field_info['extra'] = $field_read[5];

					$field_list[$tablename][0][] = $fieldname;
					$field_list[$tablename][1][$fieldname] = $field_info;
				}
			}

			$dbinfo = array();
			$dbinfo['tables'] = $table_list;
			$dbinfo['fields'] = $field_list;
			$dbinfo['table_count'] = $table_count;
			$dbinfo['field_count'] = $field_count;

			return $dbinfo;
		}

		function alternate_get_database_info($dbname){



			$returnArray=array();
			$table_query = mlavu_query("SELECT TABLE_NAME as `t_name`  FROM information_schema.tables WHERE table_schema = '[1]'", $dbname);
			//$returnArray['tables']= mysqli_num_rows($table_query);

			$contents= file_get_contents($_SERVER['DOCUMENT_ROOT']."/sa_cp/core_tables.dbt");
			$contents= preg_replace('/[^a-zA-Z0-9_ %\[\]\.\(\)%&-]/s', '', $contents);
			//$contents = explode(" ", $contents);


			$counter=0;

			while($tableArray= mysqli_fetch_assoc($table_query)){

				$tableName=$tableArray['t_name'];

				if( stristr($contents,$tableName)){
					$returnArray['tables']+=1;
					$maiQuery=mysqli_fetch_assoc(mlavu_query("SELECT COUNT(*) AS 'COUNTER' FROM INFORMATION_SCHEMA.COLUMNS WHERE table_schema = '[1]' AND table_name = '[2]'",$dbname,$tableName));
					$counter+=$maiQuery['COUNTER'];
				}

			}
			$returnArray['columns']= $counter;

			return $returnArray;
		}

		// returns the structure of the given database's structure
		// @$dbname: database name (eg poslavu_17edison_db)
		// @$o_cmp_dbstruct: the database structure to compare to (if the table or column isn't set in this table then ignore it)
		// @return: an object/array representing the database's structure, eg
		//     { table1: [column1,column2,...], table2: [column1,column2,...], ... }
		function get_database_structure_as_object($dbname, $o_cmp_dbstruct = NULL) {

			// init some values
			$retval = new stdClass();

			// compare only certain table names
			$contents= file_get_contents(dirname(__FILE__)."/../sa_cp/core_tables.dbt");
			$contents= preg_replace('/[^a-zA-Z0-9_ %\[\]\.\(\)%&-]/s', '', $contents);

			// query for the table names
			$table_query = mlavu_query("SELECT `TABLE_NAME` AS `t_name`  FROM INFORMATION_SCHEMA.TABLES WHERE table_schema = '[database]'", array('database'=>$dbname));
			if ($table_query === FALSE || mysqli_num_rows($table_query) == 0)
				return $retval;
			$a_tnames = array();
			while ($row = mysqli_fetch_assoc($table_query))
				if (($o_cmp_dbstruct === NULL || isset($o_cmp_dbstruct->$row['t_name'])) && (stripos($contents,$row['t_name']) !== FALSE))
					$a_tnames[] = $row['t_name'];

			// order the table names
			sort($a_tnames);

			// create the object structure
			for($i = 0; $i < count($a_tnames); $i++) {
				$s_tname = $a_tnames[$i];
				$a_table = array();

				// query for the column names
				$column_query = mlavu_query("SELECT `COLUMN_NAME` AS `c_name` FROM INFORMATION_SCHEMA.COLUMNS WHERE `table_schema` = '[database]' AND `table_name` = '[table]'", array('database'=>$dbname, 'table'=>$s_tname));
				if ($column_query === FALSE || mysqli_num_rows($column_query) == 0)
					return $retval;
				while ($column_read = mysqli_fetch_assoc($column_query))
					if ($o_cmp_dbstruct === NULL || in_array($column_read['c_name'], $o_cmp_dbstruct->$s_tname))
						$a_table[] = $column_read['c_name'];

				// order the column names
				$retval->$s_tname = $a_table;
				sort($retval->$s_tname);
			}

			return $retval;
		}

		function getSyncStatus($dbname){

			// get json encode
			require_once(dirname(__FILE__)."/../cp/resources/json.php");

			// set some values
			$tablesOff = 0;
			$colsOff = 0;
			$tables = '';
			$columns = '';
			$model_db = (isset($_GET['from']))?$_GET['from']:"DEV";
			$dbt_file = (isset($_GET['dbt']))?$_GET['dbt']:"core_tables.dbt";
			$make_changes = true;

			// get the database structure
			$o_db_struct1 = $this->get_database_structure_as_object('poslavu_'.$model_db.'_db');
			$o_db_struct2 = $this->get_database_structure_as_object('poslavu_'.$dbname.'_db', $o_db_struct1);
			foreach($o_db_struct1 as $t_name=>$a_colnames) {

				// check that the table exists in the destination db
				if (!isset($o_db_struct2->$t_name)) {
					$tablesOff++;
					continue;
				}

				// compare the database structure, make sure all columns exist
				$s_tstruct1 = LavuJson::json_encode($a_colnames);
				$s_tstruct2 = LavuJson::json_encode($o_db_struct2->$t_name);
				if ($s_tstruct1 != $s_tstruct2) {

					// this table is off, calculate how far it is off
					for ($i = 0; $i < count($a_colnames); $i++) {
						$s_colname = $a_colnames[$i];
						if (!in_array($s_colname, $o_db_struct2->$t_name))
							$colsOff++;
					}
				}
			}

			// check how far we are off
			if ($tablesOff > 0 || $colsOff > 0) {
				$tables = "Tables: {$tablesOff}";
				$columns = "Columns: {$colsOff}";
				return "{$tables} {$columns}";
			}
			return "In Sync";

		}


	}
?>
