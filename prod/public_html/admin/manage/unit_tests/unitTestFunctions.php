<?php

global $o_unitTests;
$o_unitTests = new unitTests();

$in_lavu = TRUE;
require_once(dirname(__FILE__)."/../../cp/resources/core_functions.php");

class unitTests {

	function __construct() {
		$this->styleHasBeenDrawn = FALSE;
	}

	/**
	 * Draws a unit test table with a standard format
	 * @param  string $s_title       The name of this unit test.
	 * @param  array  $a_tests       An array of tests, in the form array(array('title'=>string, 'expected'=>string, 'actual'=>string), ...)
	 * @param  string $s_description A description of this unit test (appears between the title and the table)
	 * @return string                The unit test table as a string
	 */
	public function testTableTostr($s_title, $a_tests, $s_description = "") {
		$s_retval = $this->styleTostr();
		ob_start();
		?>
		<div>
			<div class="title"><?= $s_title ?></div>
			<div class="description" style="<?= ($s_description == '' ? 'display:none;' : '') ?>"><?= $s_description ?></div>
			<table class="unitTestTable" cellspacing="0" cellpadding="0">
				<tr><th>Test</th><th>Expected</th><th>Actual</th></tr>
				<?php
				foreach($a_tests as $a_test) {
					echo $this->testTostr($a_test);
				}
				?>
			</table>
		</div>
		<?php
		$s_retval .= ob_get_contents();
		ob_end_clean();
		return $s_retval;
	}

	/**
	 * Draws a single unit test
	 * @param  array  $a_test An array in the form array('title'=>string, 'expected'=>string, 'actual'=>string)
	 * @return string         The table row as a string
	 */
	private function testTostr($a_test) {
		ob_start();
		$b_success = ($a_test['expected'] == $a_test['actual']);
		$s_success = $b_success ? 'success' : 'failure';
		?>
			<tr><td><div class="title <?= $s_success ?>"><?= $a_test['title'] ?></div></td><td><div class="expected <?= $s_success ?>"><?= $a_test['expected'] ?></div></td><td><div class="actual <?= $s_success ?>"><?= $a_test['actual'] ?></div></td></tr>
		<?php
		$s_contents .= ob_get_contents();
		ob_end_clean();
		return $s_contents;
	}

	private function styleTostr() {
		if ($this->styleHasBeenDrawn)
			return "";
		ob_start();
		?>
		<style type="text/css">
			.title {
				font-size: 20px;
				font-weight: bold;
				color: black;
			}
			.description {
				font-size: 12px;
				color: #333;
				width: 700px;
			}
			.unitTestTable {
				min-width: 700px;
				border-left: 1px solid gray;
				border-top: 1px solid gray;
			}
			th {
				background-color: #eee;
				color: #333;
				padding: 5px;
				border-right: 1px solid gray;
				border-bottom: 1px solid gray;
			}
			td {
				background-color: #eee;
				border-right: 1px solid gray;
				border-bottom: 1px solid gray;
			}
			td div {
				background-color: #eee;
				color: black;
				padding: 5px;
			}
			td div.expected {
				min-width: 300px;
				max-width: 500px;
				max-height: 300px;
				overflow-x: auto;
				overflow-y: auto;
			}
			td div.actual {
				min-width: 300px;
				max-width: 500px;
				max-height: 300px;
				overflow-y: auto;
				overflow-x: auto;
			}
			td div.title {
				max-width: 300px;
				overflow-x: auto;
			}
			td div.title.success {
				background-color: green;
			}
			td div.title.failure {
				background-color: red;
			}
		</style>
		<?php
		$this->styleHasBeenDrawn = TRUE;
		$s_contents = ob_get_contents();
		ob_end_clean();
		return $s_contents;
	}
}

?>