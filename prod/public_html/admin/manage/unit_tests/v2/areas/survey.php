<?php

require_once("/home/poslavu/public_html/admin/manage/unit_tests/unitTestFunctions.php");
require_once(str_replace("/manage/unit_tests", "", __FILE__));

foreach(array("get_most_recent_survey_results") as $s_func_name) {
	surveyUnitTests::draw($s_func_name);
}

class surveyUnitTests {

	public static function draw($s_name) {
		global $o_unitTests;
		$a_retval = call_user_func(array("surveyUnitTests", $s_name));
		echo $o_unitTests->testTableTostr($a_retval['title'], $a_retval['tests'], $a_retval['description']);
	}

	public static function get_most_recent_survey_results() {
		$a_datanames = array('el_patio');//, 'load_all_poslavu_restaurants');
		$a_settings = array('satisfaction survey', array('satisfaction survey'), 'lavu satisfaction survey', array('lavu satisfaction survey'), array('satisfaction survey', 'lavu satisfaction survey'));
		$a_ignore_first = array(TRUE, FALSE);
		$a_load_multiple = array(TRUE, FALSE);
		$a_tests = array();

		foreach($a_datanames as $s_dataname) {
			foreach($a_settings as $wt_setting) {
				foreach($a_ignore_first as $b_ignore_first) {
					foreach($a_load_multiple as $b_load_multiple) {

						// get the test name
						$s_name = "Dataname: {$s_dataname}, Setting: ".str_replace("\n", "", print_r($wt_setting,TRUE)).", Ignore First: ".($b_ignore_first ? "TRUE" : "FALSE").", Load Multiple: ".($b_load_multiple ? "TRUE" : "FALSE");
						$s_name = str_replace(", ", ",<br />", $s_name);

						// get the survey strings to search for
						$a_surveys = NULL;
						$a_setting = array();
						$a_wherevars = ($s_dataname === 'load_all_poslavu_restaurants') ? array() : array("dataname"=>$s_dataname);
						if (is_array($wt_setting)) {
							foreach($wt_setting as $k=>$v) {
								$a_wherevars["setting{$k}"] = $v;
								$a_setting[] = "`setting`='[setting{$k}]'";
							}
						} else {
							$a_setting[] = "`setting`='[setting]'";
							$a_wherevars['setting'] = $wt_setting;
						}
						$s_setting = ($s_dataname === 'load_all_poslavu_restaurants') ? "" : "`dataname`='[dataname]' AND ";
						$s_setting .= "(".implode(" OR ", $a_setting).")";

						// search the database
						$a_surveys = ConnectionHub::getConn('poslavu')->getDBAPI()->getAllInTable("survey", $a_wherevars, TRUE, array("whereclause"=>"WHERE {$s_setting}", "orderbyclause"=>"ORDER BY `value` ASC"));

						// process by dataname
						$a_surveys_by_dataname = array();
						foreach($a_surveys as $a_survey) {
							$s_dn = $a_survey['dataname'];
							if (!isset($a_surveys_by_dataname[$s_dn]))
								$a_surveys_by_dataname[$s_dn] = array();
							$a_surveys_by_dataname[$s_dn][] = $a_survey;
						}

						// ignore first
						if ($b_ignore_first) {
							foreach($a_surveys_by_dataname as $k1=>$a_by_dataname) {
								unset($a_surveys_by_dataname[$k1][0]);
							}
						}

						// load multiple
						if (!$b_load_multiple) {
							foreach($a_surveys_by_dataname as $k1=>$a_by_dataname) {
								$survey_submitted = 0;
								foreach($a_by_dataname as $k2=>$v) {
									$survey_submitted++;
									if ($survey_submitted == count($a_by_dataname)) {
										break;
									}
									unset($a_surveys_by_dataname[$k1][$k2]);
								}
							}
						}

						// put datanames back into a single array
						$a_surveys = array();
						foreach($a_surveys_by_dataname as $a_by_dataname) {
							foreach($a_by_dataname as $a_survey) {
								$a_retval = NULL;
								assign_names_to_values($a_survey, $a_retval);
								$a_surveys[] = $a_retval;
							}
						}
						unset($a_surveys_by_dataname);
						unset($a_by_dataname);

						// get the actual
						$a_actual = get_most_recent_survey_results($wt_setting, $s_dataname, $b_ignore_first, $b_load_multiple);

						// create the test
						$a_tests[] = array('title'=>$s_name, 'expected'=>"<pre>".print_r($a_surveys,TRUE)."</pre>", 'actual'=>"<pre>".print_r($a_actual,TRUE)."</pre>");
						unset($a_surveys);
						unset($a_actual);
					}
				}
			}
		}

		$a_retval = array('title'=>__FUNCTION__, 'description'=>'', 'tests'=>$a_tests);
		unset($a_tests);
		return $a_retval;
	}
}

?>
