<?php
	require_once(dirname(__FILE__). '/../cp/areas/white_list.php');

	require_once(__DIR__."/../cp/resources/session_functions.php");

	standarizeSessionDomain();

	if(session_id()==''){
		session_start();
	}

	global $maindb;
	global $loggedin;
	global $loggedin_access;
	global $loggedin_email;
	global $loggedin_fullname;
	global $loggedin_lavu_admin;

	require_once dirname(dirname(__FILE__)) . "/cp/resources/lavuquery.php";
	$maindb = "poslavu_MAIN_db";
	$loggedin = (isset($_SESSION['posadmin_loggedin']))?$_SESSION['posadmin_loggedin']:false;
	if($loggedin=="") $loggedin = false;
	$loggedin_fullname = (isset($_SESSION['posadmin_fullname']))?$_SESSION['posadmin_fullname']:false;
	if($loggedin_fullname=="") $loggedin_fullname = $loggedin;
	$loggedin_email = (isset($_SESSION['posadmin_email']))?$_SESSION['posadmin_email']:false;
	if($loggedin_email=="") $loggedin_email = $loggedin;

	$loggedin_access = (isset($_SESSION['posadmin_access']))? $_SESSION['posadmin_access'] : false;
	if(!$loggedin_access) $loggedin_access = "";
	$loggedin_lavu_admin = (isset($_SESSION['posadmin_lavu_admin']))?$_SESSION['posadmin_lavu_admin']:0;



	require_once(dirname(__FILE__).'/can_access.php');

	function account_loggedin()
	{

		global $loggedin;
		return $loggedin;
	}

	function logout_account($username){
		global $loggedin;
		//Logout in the db
		mlavu_query("update `poslavu_MAIN_db`.`loggedin` set `isLoggedin`='0', `loggedoutat`='".date("Y-m-d H:i:s")."' where `username`='".$username."'");
		//echo $username;
		$_SESSION['posadmin_loggedin'] = false;
		$loggedin = false;

		unset($_COOKIE["poslavu_username"]);
		unset($_COOKIE["poslavu_password"]);
		setcookie("poslavu_username", "", time()-3600);
		setcookie("poslavu_password", "", time()-3600);
	}

	// Force login if the POST request includes the flag `login`
	if(!$loggedin || isset($_POST['login']))
	{

		$maindb = "poslavu_MAIN_db";
		$login_message = "";

		$fwd_page = "";
		$fwd_action = "";
		if(isset($_GET['fwd']))
		{
			$fwd_page = $_GET['fwd'];
			$fwd_action = "?fwd=" . $fwd_page;
		}
		if(isset($_COOKIE['poslavu_username']) && isset($_COOKIE['poslavu_password'])) {
			$_POST['username'] = $_COOKIE['poslavu_username'];
			$_POST['password'] = $_COOKIE['poslavu_password'];
		}

		if(isset($_POST['username']) && isset($_POST['password']))
		{
			function update_cp_login_status($success,$username)
			{
				global $maindb;
				if($success) {
					$set_field = "succeeded";
				} else {
					$set_field = "failed";
					logout_account($username);
				}

				$ipaddress = empty($_SERVER['HTTP_X_FORWARDED_FOR']) ? $_SERVER['REMOTE_ADDR'] : $_SERVER['HTTP_X_FORWARDED_FOR'];  // work-around for load balancer proxy
				$mints = time() - (60 * 15);
				$currentts = time();
				$currentdate = date("Y-m-d H:i:s");

				$li_query = mlavu_query("select * from `$maindb`.`login_log` where `ipaddress`='$ipaddress' and `$set_field`>'0' and `ts`>='$mints'");
				if(mysqli_num_rows($li_query))
				{
					$li_read = mysqli_fetch_assoc($li_query);
					$li_id = $li_read['id'];
					mlavu_query("update `$maindb`.`login_log` set `ts`='$currentts', `date`='$currentdate', `users`=CONCAT(`users`,',[1]'), `$set_field`=`$set_field`+1 where `id`='$li_id'",$username);
				}
				else
				{
					mlavu_query("insert into `$maindb`.`login_log` (`ts`,`date`,`$set_field`,`ipaddress`,`users`) values ('$currentts','$currentdate','1','$ipaddress','[1]')",$username);
				}
			}

			function get_cp_failed_login_count()
			{
				$ipaddress = empty($_SERVER['HTTP_X_FORWARDED_FOR']) ? $_SERVER['REMOTE_ADDR'] : $_SERVER['HTTP_X_FORWARDED_FOR'];  // work-around for load balancer proxy
				if (function_exists("get_office_ip_addresses") && in_array($ipaddress, get_office_ip_addresses()))
					return 0;

				global $maindb;

				$mints = time() - (60 * 15);

				$li_query = mlavu_query("select * from `$maindb`.`login_log` where `ipaddress`='$ipaddress' and `failed`>'0' and `ts`>='$mints'");
				if(mysqli_num_rows($li_query))
				{
					$li_read = mysqli_fetch_assoc($li_query);
					return $li_read[$field];
				}
				else return false;
			}

			$failed_login_attempts = get_cp_failed_login_count("failed");

			$prod_hn = 'prod-hn';
                        if($failed_login_attempts >= 10 && !isset($whiteList[$ipaddress]) && !$isLocalIP) {
				update_cp_login_status(false,$_POST['username']);
				$login_message = "Exceeded Max Login Attempts";
			}
			else
			{
				$account_query = mlavu_query("SELECT * FROM `poslavu_MAIN_db`.`accounts` WHERE `username`='[1]' AND (`password`=PASSWORD('[2]') OR `password`=OLD_PASSWORD('[2]')) AND `lavu_admin`='1'",$_POST['username'],$_POST['password']);
				if(mysqli_num_rows($account_query))
				{
					$account_read = mysqli_fetch_assoc($account_query);
					$loggedin = $account_read['username'];
					$loggedin_fullname = trim($account_read['firstname'] . " " . $account_read['lastname']);
					$loggedin_email = $account_read['email'];
					$loggedin_access = $account_read['access'];
					$loggedin_lavu_admin = $account_read['lavu_admin'];
					$_SESSION['posadmin_loggedin'] = $loggedin;
					$_SESSION['posadmin_fullname'] = $loggedin_fullname;
					$_SESSION['posadmin_email'] = $loggedin_email;
					$_SESSION['posadmin_access'] = $loggedin_access;
					$_SESSION['posadmin_lavu_admin'] = $loggedin_lavu_admin;
					$_SESSION['posadmin_user_id'] = $account_read['id'];
					$_SESSION['posadmin_options'] = $account_read['options'];
					$_SESSION['posadmin_type'] = $account_read['type'];
					$_SESSION['posadmin_default_pipeline_id'] = $account_read['default_pipeline_id'];
					$_SESSION['posadmin_default_stage_id'] = $account_read['default_stage_id'];
					$_SESSION['posadmin_password'] = $_POST['password'];

					update_cp_login_status(true,$_POST['username']);

					if(isset($_POST['stay_logged_in']))
					{
						$autokey = session_id() . rand(1000,9999);
						setcookie("poslavu_sacp_login", $autokey, mktime(0,0,0,date("m")+1,date("d"),date("Y")));
						//mlavu_query("update `poslavu_MAIN_db`.`customer_accounts` set `autokey`=AES_ENCRYPT('[1]','autokey') where `id`='[2]'",$autokey,$custid);
					}

					if($fwd_page=="")
						$fwd_page = "index.php";

					(isset($_GET['mode']))?$_GET['mode']="":"";
					$query = mlavu_query("select * from `".$maindb."`.`loggedin` where `username` = '".$_POST['username']."'");
					if( mysqli_num_rows($query))  //if they have logged in before then we can just update their entry
						mlavu_query("update `".$maindb."`.`loggedin` set `isLoggedin`='1', `loggedinat`='".date("Y-m-d H:i:s")."' where `username`='".$_POST['username']."'");
					else
						mlavu_query("insert into `".$maindb."`.`loggedin` (isLoggedin, username, loggedinat, viewedChats) VALUES ('1' ,'".$_POST['username']."','".date("Y-m-d H:i:s")."','|')");
					$_SESSION['admin_username']= $_POST['username'];
					echo "<script language='javascript'>";
					echo "window.location.replace('$fwd_page'); ";
					echo "</script>";

					setcookie("poslavu_username",$_POST['username'],time() + (10 * 365 * 24 * 60 * 60));
					setcookie("poslavu_password",$_POST['password'],time() + (10 * 365 * 24 * 60 * 60));

					exit();
				}
				else
				{
					update_cp_login_status(false,$_POST['username']);
					$login_message = "Invalid Account or Password";
				}
			}
		}

		echo "<html> <head>	<meta name='viewport' content='width=device-width, initial-scale=1.0'/> </head><body><table cellpadding=16><td style='border: solid 2px #bbbbbb' bgcolor='#f6f6f6'>";
		echo "<form name='login' method='post' action='$fwd_action'>";
		echo "<table>";
		if($login_message!="")
			echo "<tr><td colspan='2' align='center'><b>$login_message</b></td></tr>";

		echo "<tr><td class='form_row_title'>Username</td><td><input type='text' name='username'></td></tr>";
		echo "<tr><td class='form_row_title'>Password</td><td><input type='password' name='password'></td></tr>";
		echo "<tr><td>&nbsp;</td><td><input class='form_submit_button' type='submit' value='Submit'></td></tr>";
		//echo "<tr><td>&nbsp;</td><td>&nbsp;</td></tr>";
		//echo "<tr><td>&nbsp;</td><td><input type='checkbox' name='stay_logged_in'> Stay Logged In</td></tr>";
		echo "</table>";
		echo "</form>";
		echo "</td></table></body>";
		echo "<script language='javascript'>";
		echo "document.login.username.focus(); ";
		echo "</script></html>";
	}

?>
