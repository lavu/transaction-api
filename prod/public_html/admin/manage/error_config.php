<?php
    if (getenv('ENVIRONMENT') == 'development' && !defined('DEV')) {
		define('DEV', 1);
	} else if (!defined('DEV')) {
		define('DEV', 0);
    }
	if( DEV ) {
		ini_set("display_errors", '1');
	} else {
		error_reporting(error_reporting() & ~E_NOTICE & ~E_DEPRECATED & ~E_WARNING);
	}
?>