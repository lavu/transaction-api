<?php

	global $o_emailAddressesMap;
	$o_emailAddressesMap = new EmailAddressesMap();

	/**
	 * Used to create a mapping between email destination addresses and user email addresses.
	 * Vocabulary
	 *     destination email address: A string that gets mapped to users and explicit addresses
	 *     user:                      The username associated with a row from `poslavu_MAIN_db`.`accounts`
	 *     explict email address:     An email address not necessarily connected to a user account
	 */
	class EmailAddressesMap {

		/**
		 * Sends an email to a group of users based on the given email address
		 * The syntax is the same as the php "mail()" command except that the first extra argument isn't a specific email address.
		 * @param  string  $s_address The mapping of addresses to send emails to (see getMapping for the list of addresses)
		 * @param  string  $s_subject The subject line of the email
		 * @param  string  $s_body    The body of the email
		 * @param  string  $s_options Options (eg "From: noreply@poslavu.com")
		 * @return boolean            TRUE if at least one email address is found, FALSE if there are no associated addresses
		 */
		public function sendEmailToAddress($s_address, $s_subject, $s_body, $s_options = "") {
			$s_emails = $this->getEmailsForAddressToString($s_address);

			if ($s_emails !== "") {
				mail($s_emails, $s_subject, $s_body, $s_options);
				return TRUE;
			}
			return FALSE;
		}

		/**
		 * Get all email addresses that a destination address maps to (both from users and explicity defined ones)
		 * @param  string $s_address The destination address
		 * @return string            A string of the addresses (comma delimeted), or empty string ""
		 */
		public function getEmailsForAddressToString($s_address) {
			$a_emails = $this->getEmailsForAddress($s_address);

			if (count($a_emails) == 0) {
				return "";
			} else {
				return implode(",", $a_emails);
			}
		}

		/**
		 * Get all email addresses that a destination address maps to (both from users and explicity defined ones)
		 * @param  string $s_address The destination address
		 * @return array             An array of email addresses, in the form array('address1'=>'address1', 'address2'=>'address2')
		 */
		public function getEmailsForAddress($s_address) {

			$a_usernames = $this->getUsersForAddress($s_address);
			$a_explicit_emails = $this->getExplicitEmailsForAddress($s_address);
			$a_emails = array();
			for($i = 0; $i < count($a_usernames); $i++) {
				$s_email = trim($this->getEmailAddressForUser($a_usernames[$i]));
				if ($s_email !== "")
					$a_emails[$s_email] = $s_email;
			}
			for($i = 0; $i < count($a_explicit_emails); $i++) {
				$s_email = $a_explicit_emails[$i];
				if ($s_email !== "")
					$a_emails[$s_email] = $s_email;
			}

			return $a_emails;
		}

		function _construct() {
			$this->users = NULL;
			$this->mapping = NULL;
		}

		/**
		 * Get the set of destination addresses that are mapped to users and/or explicit_emails
		 * @return array An array of strings
		 */
		public function enumerateEmailAddresses() {

			$o_mapping = $this->getMapping();
			$a_addresses = array();
			foreach($o_mapping as $s_address=>$o_map)
				$a_addresses[] = $s_address;

			return $a_addresses;
		}

		/**
		 * Get all the users that a destionation address maps to.
		 * @param  string $s_address A destination address
		 * @return array             An array of usernames
		 */
		public function getUsersForAddress($s_address) {

			$o_mapping = $this->getMapping();
			if (isset($o_mapping->$s_address))
				return explode(",", $o_mapping->$s_address->users);
			return array();
		}

		/**
		 * Get the explicit emails that a destination address maps to.
		 * @param  string $s_address A destination address
		 * @return array             An array of email explicit email addresses
		 */
		public function getExplicitEmailsForAddress($s_address) {

			$o_mapping = $this->getMapping();
			if (isset($o_mapping->$s_address))
				return explode(",", $o_mapping->$s_address->explicit_emails);
			return array();
		}

		/**
		 * Get the mapping from destination addresses to users and explicit_email
		 * @return object Each index is a destination address, and each value is an object(users->comma seperated list, explicit_emails->comma seperated list)
		 */
		private function getMapping() {

			return (object)array(
				'distro_portal_dev'=>(object)array("users"=>"martin", "explicit_emails"=>""),
				'distro_portal_errors'=>(object)array("users"=>"", "explicit_emails"=>"specialists@poslavu.com"),
				'distro_scripts_errors'=>(object)array("users"=>"martin", "explicit_emails"=>""),
				'upgrade_success_emails'=>(object)array("users"=>"", "explicit_emails"=>"billing@lavu.com,billing@lavu.zendesk.com"),
				'upgrade_failure_emails'=>(object)array("users"=>"tom", "explicit_emails"=>""),
				'account_cancelations'=>(object)array("users"=>"Team Zuora", "explicit_emails"=>"billing@lavu.com,billing@lavu.zendesk.com"),
				'cancel_profile_failures'=>(object)array("users"=>"tom", "explicit_emails"=>""),
				'zendesk_ticket_errors'=>(object)array("users"=>"tschnepp", "explicit_emails"=>""),
				'billing_errors'=>(object)array("users"=>"tom", "explicit_emails"=>"team-billing@lavu.com"),
				'billing_notifications'=>(object)array("users"=>"Team Zuora", "explicit_emails"=>"billing@lavu.com,billing@lavu.zendesk.com"),
				'payment_notifications'=>(object)array("users"=>"tom", "explicit_emails"=>"payments@lavu.com"),
				'security_dev'=>(object)array("users"=>"tom", "explicit_emails"=>""),
				'cp_dev'=>(object)array("users"=>"tom", "explicit_emails"=>""),
				'survey_followups'=>(object)array("users"=>"tom", "explicit_emails"=>"distributors@lavuinc.com"),
				'sales_quote_notifications'=>(object)array("users"=>"tom,s.brown", "explicit_emails"=>"orders@lavu.com"),
				'sales_pbc_rejections'=>(object)array("users"=>"tom", "explicit_emails"=>"ahassley@lavu.com"),
			);

			if ($this->mapping !== NULL)
				return $this->mapping;

			$query = mlavu_query("SELECT * FROM `poslavu_MAIN_db`.`email_mapping`");
			if ($query !== FALSE && mysqli_num_rows($query) > 0) {
				$this->mapping = new stdClass();
				while ($row = mysqli_fetch_object($query)) {
					$address = $row->address;
					$this->mapping->$address = $row;
				}
			}

			return $this->mapping;
		}

		/**
		 * Loads relavant user information from the `accounts` section of the database
		 * @return object Each index is a username, and each value is an object(username->string, email->string)
		 */
		private function getUsers() {

			if ($this->users !== NULL)
				return $this->users;

			$query = mlavu_query("SELECT `username`,`email` FROM `poslavu_MAIN_db`.`accounts`");
			if ($query !== FALSE && mysqli_num_rows($query) > 0) {
				$this->users = new stdClass();
				while ($row = mysqli_fetch_object($query)) {
					$username = $row->username;
					$this->users->$username = $row;
				}
			}

			return $this->users;
		}

		/**
		 * Get the email address associated with a given user
		 * @param  string $s_username The username on the account
		 * @return string             The user account's email address
		 */
		private function getEmailAddressForUser($s_username) {

			$o_users = $this->getUsers();
			if (isset($o_users->$s_username))
				return $o_users->$s_username->email;
			return "";
		}
	}

?>
