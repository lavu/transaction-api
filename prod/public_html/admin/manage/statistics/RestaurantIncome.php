<?php
	require_once($_SERVER['DOCUMENT_ROOT']."/manage/error_config.php");
	require_once(dirname(__FILE__).'/../login.php');
	require_once('/home/poslavu/public_html/admin/manage/misc/AdvancedToolsFunctions/companyReporting.php');
	require_once('/home/poslavu/public_html/admin/cp/resources/lavuquery.php');
	require_once('/home/poslavu/public_html/admin/cp/resources/core_functions.php');
	require_once('/home/poslavu/public_html/admin/cp/objects/json_decode.php');
	
	// get the environment set up
	session_start();
	set_time_limit(60);
	if (!account_loggedin())
		header("Location: http://admin.poslavu.com/manage/");
	
	$a_income_brackets = array(
		array('title'=>'low', 'min'=>10, 'max'=>10000),
		array('title'=>'mid', 'min'=>10001, 'max'=>56000),
		array('title'=>'max', 'min'=>56001, 'max'=>3000000),
	);
	
	class restaurantIncomeStatistics {

		function __construct() {
			$this->dbapi = ConnectionHub::getConn('poslavu')->getDBAPI();
		}

		// get the income of a random restaurant from the list of restaurants (indexed by number)
		// for the given day, and removes the restaurant dataname from the array
		// $s_date: should be the exact date (no time)
		// $i_ignore_below: ignores restaurants with an income below the given value (still removes them from the array if encountered)
		// returns array('dn'=>dataname, 'val'=>total income for the day)
		// returns array('dn'=>'', 'val'=>-1) if there are no restaurants in $a_datanames that meet the requirements
		function getIncomeForDay($s_date, &$a_datanames, $i_ignore_below = 0) {
			$a_badval = array('dn'=>'', 'val'=>-1);
			if (count($a_datanames) == 0)
				return $a_badval;
			
			// get a random dataname and unset the index
			$i_num_datanames = count($a_datanames);
			$i_index = rand(0, $i_num_datanames-1);
			$s_dataname = $a_datanames[$i_index];
			$a_datanames[$i_index] = $a_datanames[$i_num_datanames-1];
			unset($a_datanames[$i_num_datanames-1]);
			$s_database = 'poslavu_'.$s_dataname.'_db';
			
			// get the income for that day
			$s_start = $s_date.' 00:00:00';
			$s_end = $s_date.' 23:59:59';
			lavu_connect_dn($s_database,'poslavu_'.$s_database.'_db');
			$income_query = lavu_query("SELECT SUM(`total`) AS `total` FROM `[database]`.`orders` WHERE `closed` >= '[start]' AND `closed` <= '[end]'",
				array('database'=>$s_database, 'start'=>$s_start, 'end'=>$s_end));
			
			// check that there is a total
			if ($income_query === FALSE) {
				return $this->getIncomeForDay($s_date, $a_datanames, $i_ignore_below);
			}
			if (mysqli_num_rows($income_query) == 0) {
				mysqli_free_result($income_query);
				return $a_badval;
			}
			
			// get the total
			$income_read = mysqli_fetch_assoc($income_query);
			mysqli_free_result($income_query);
			$i_income = 0;
			if (isset($income_read['total']))
				$i_income = (int)$income_read['total'];
			
			// return
			if ($i_income <= $i_ignore_below)
				return $this->getIncomeForDay($s_date, $a_datanames, $i_ignore_below);
			return array('dn'=>$s_dataname, 'val'=>$i_income);
		}
		
		// returns either the combined file pointer or the file pointer for the given date
		private static $combinedFilePointer = NULL;
		private static function getCachedFile($s_date, &$file, $b_use_combined_file = TRUE) {
			
			// try and load the combined file pointer
			if ($b_use_combined_file) {
				if (self::$combinedFilePointer != NULL) {
					$file = self::$combinedFilePointer;
					return;
				}
				self::$combinedFilePointer = fopen(dirname(__FILE__).'/RestaurantIncomes/compiled.txt', 'r');
				if (self::$combinedFilePointer != NULL) {
					$file = self::$combinedFilePointer;
					return;
				}
			}
			
			// try and load the file for the given date
			$filename = dirname(__FILE__).'/RestaurantIncomes/'.$s_date.'_cached.txt';
			if (!file_exists($filename))
				return $a_retval;
			$file = fopen($filename, 'r');
		}
		
		// searches in __FILE__.'_cached.txt' for the given date and JSON decodes the line
		// if the line is found, and it has enough values for $i_sample, it returns the first $i_sample values from the line as 
		// array('flle_found'=>TRUE/FALSE, 'line_found'=>TRUE/FALSE, 'enough_samples'=>TRUE/FALSE, 'number_of_samples'=>integer, 'samples'=>stdClass)
		function getIncomesByDayAndLevelCached($s_date, $i_sample, $b_use_combined_file = TRUE) {
			$s_date = trim($s_date);
			$file = NULL;
			$a_retval = array('file_found'=>FALSE, 'line_found'=>FALSE, 'enough_samples'=>FALSE, 'samples'=>new stdClass());
			
			// try and find the file
			self::getCachedFile($s_date, $file, $b_use_combined_file);
			if ($file) {
				$a_retval['file_found'] = TRUE;
				
				// try and find the line (limit to 1MB per line)
				$s_line_data = fgets($file, 1048576);
				while ($s_line_data !== FALSE) {
					
					// read the line and determine if this is the correct line
			        $o_line_data = Json::unencode($s_line_data);
			        if (trim($o_line_data->date) != $s_date) {				        
						$s_line_data = fgets($file, 1048576);
						continue;
			        }
			        $a_retval['line_found'] = TRUE;
			        
			        // check that there are enough samples
			        if ($o_line_data->max_samples >= $i_sample) {
				        $a_retval['enough_samples'] = TRUE;
				        
				        // get the samples
				        $s_line_samples = fgets($file, 1048576);
				        $o_samples = Json::unencode($s_line_samples);
				        $a_retval['samples'] = $o_samples;
			        }
			        
			        break;
			    }
			}
			
			return $a_retval;
		}
		
		// saves the data to a cached file
		// $o_incomes should be a stdClass object with field for every restaurant $o_income->package_level->array(name = value...)
		function saveIncomesToCache($s_date, $i_sample, $o_incomes) {
			// get the data ready for writing
			$filename = dirname(__FILE__).'/RestaurantIncomes/'.$s_date.'_cached.txt';
			$a_line_data = array(
				'date'=>$s_date,
				'max_samples'=>$i_sample,
			);
			
			// open the file for writing and write the data
			$file = fopen($filename, 'w');
			if ($file) {
				fwrite($file, lavu_json_encode($a_line_data)."\n");
				fwrite($file, lavu_json_encode($o_incomes)."\n");
				fclose($file);
			} else {
				error_log('failed to write file '.$filename);
			}
		}
		
		// returns either NULL or an object (see the retval for getIncomesByDayAndLevel)
		function getCachedData($s_date, $i_sample, $b_use_combined_file = TRUE) {
			$a_cached = $this->getIncomesByDayAndLevelCached($s_date, $i_sample, $b_use_combined_file);
			
			if (!$a_cached['file_found'] || !$a_cached['line_found'] || !$a_cached['enough_samples'])
				return NULL;
			$o_samples = $a_cached['samples'];
			return $o_samples;
		}
		
		// $s_date: a date in the form date('Y-m-d')
		// returns the incomes for <30 Silver, <30 Gold, and <30 Platinum restaurants as
		// array('total'=>number of restaurants, 'silver'=>stdClass('dn'='income',...), 'gold'=>..., 'platinum'=>...)
		// if the parameters are left as defaults, it tries looking in the post_vars for values by the same name
		function getIncomesByDayAndLevel($s_date = NULL, $i_sample = NULL, $b_cache_results = NULL) {
			// get values from postvars (if set)
			$this->getPostvars($s_date, 's_date', '2013-04-09');
			$this->getPostvars($i_sample, 'i_sample', 30);
			$this->getPostvars($b_cache_results, 'b_cache_results', FALSE);
			
			// set up the variables
			$s_start_time = $s_date.' 00:00:00';
			$s_end_time = $s_date.' 23:59:59';
			$o_stats = new statsReporting();
			$a_packages = array('silver'=>array(1,11), 'gold'=>array(2,12), 'platinum'=>array(3,13));
			
			// check for cached data
			if ($b_cache_results) {
				$o_cached = $this->getCachedData($s_date, $i_sample);
				if ($o_cached !== NULL) {
					$this->removeNullObjects($a_packages, $o_cached);
					return $o_cached;
				}
			}
			
			// get the datanames
			$a_active_datanames = $o_stats->getActiveDatanames(FALSE, array('start_date'=>$s_start_time, 'end_date'=>$s_end_time, 'check_deleted'=>FALSE));
			$i_total = count($a_active_datanames);
			$a_where_datanames = array();
			foreach($a_active_datanames as $s_dataname)
				$a_where_datanames[$s_dataname] = $s_dataname;
			$s_in_clause = $this->dbapi->arrayToInClause($a_where_datanames);
			
			// get the restaurants and
			// create the silver/gold/platinum arrays to stick restaurants into
			if ($i_total > 0)
				$a_restaurants_query = $this->dbapi->getAllInTable('signups', NULL, TRUE,
					array('whereclause'=>'WHERE `dataname` '.$s_in_clause, 'selectclause'=>'`dataname`,`package`'));
			$a_restaurants = array();
			foreach($a_packages as $s_packagename=>$a_packagelevels) {
				$a_restaurants[$s_packagename] = array();
				$a_restaurants[$s_packagename.'_chosen'] = array();
			}
			
			// get the package for every restaurant and put them into one of the
			// silver/gold/platinum package arrays
			$a_seen = array();
			if ($i_total > 0) {
				foreach($a_restaurants_query as $a_restaurant) {
					// make sure this signup is unique
					$s_dataname = $a_restaurant['dataname'];
					if (isset($a_seen[$s_dataname]))
						continue;
					$a_seen[$s_dataname] = TRUE;
					
					// assign the package
					$i_package = (int)$a_restaurant['package'];
					foreach($a_packages as $s_packagename=>$a_packagelevels) {
						if (in_array($i_package, $a_packagelevels)) {
							$a_restaurants[$s_packagename][] = $s_dataname;
							break;
						}
					}
				}
			}
			
			// get the data
			foreach($a_packages as $s_packagename=>$a_packagelevels) {
				for($i = 0; $i < $i_sample; $i++) {
					$a_income = $this->getIncomeForDay($s_date, $a_restaurants[$s_packagename]);
					if ($a_income['dn'] == '')
						break;
					$a_restaurants[$s_packagename.'_chosen'][] = $a_income;
				}
			}
			
			// get, save, and return the values
			$a_retval = array();
			foreach($a_packages as $s_packagename=>$a_packagelevels) {
				$a_retval[$s_packagename] = $a_restaurants[$s_packagename.'_chosen'];
				unset($a_restaurants[$s_packagename]);
				unset($a_restaurants[$s_packagename.'_chosen']);
			}
			$a_retval['total'] = $i_total;
			$o_retval = Json::unencode(lavu_json_encode($a_retval));
			$this->removeNullObjects($a_packages, $o_retval);
			$this->saveIncomesToCache($s_date, $i_sample, $o_retval);
			return $o_retval;
		}
		
		// json decode is randomly inserting null objects from the cache, so
		// this function was made to remove them
		function removeNullObjects(&$a_packages, $o_samples) {
			foreach($a_packages as $s_packagename=>$a_packagelevels) {
				$a_new_package = array();
				foreach($o_samples->$s_packagename as $k=>$o_object_data)
					if (gettype($o_object_data) !== 'NULL')
						$a_new_package[] = $o_object_data;
				$o_samples->$s_packagename = $a_new_package;
			}
		}
		
		// if $var is NULL and $_POST[$postname] is set, inserts the post value into $var
		// if $var is NULL and $_POST[$postname] is not set, inserts the $defaultValue into $var
		// returns TRUE if the postvar/defaultvalue are used
		// returns FALSE if $var is unchanged
		function getPostvars(&$var, $postname, $defaultValue) {
			if ($var === NULL) {
				$var = (isset($_POST[$postname])) ? $_POST[$postname] : $defaultValue;
				return TRUE;
			}
			return FALSE;
		}
		
		// if $var is NULL and $_POST[$postname] is set, inserts the post value into $var
		// if $var is NULL and $_POST[$postname] is not set, inserts the $defaultValue into $var
		// returns TRUE if the postvar/defaultvalue are used
		// returns FALSE if $var is unchanged
		function getGetvars(&$var, $postname, $defaultValue) {
			if ($var === NULL) {
				$var = (isset($_GET[$postname])) ? $_GET[$postname] : $defaultValue;
				return TRUE;
			}
			return FALSE;
		}
		
		// a php/javascript medly to load data for all of the days from 2010-05-01 to now
		// if a day is already cached, it skips it to the next day
		function getAllDays() {
			// get the relatives days as times
			$s_current_day = NULL;
			$this->getGetvars($s_current_day, 'i_current_day', '0');
			$s_current_day = (string)((int)$s_current_day);
			$i_current_day = strtotime('2010-05-01 +'.$s_current_day.' days');
			$i_today = strtotime(date('Y-m-d'));
			$s_date = date('Y-m-d', $i_current_day);
			error_log($s_date);
			
			// setup vars
			$i_sample = 20000; // number of restaurants to use as a relative sample
			$s_form = '
				<form id="nextDayForm" action="'.basename(__FILE__).'?get_all_days" method="get">
					<input type="hidden" name="get_all_days" value="1">
					<input type="hidden" name="i_current_day" value="'.((int)$s_current_day+1).'">
				</form>';
			
			// check how many days are left
			// return if <1 days are left
			$i_days_left = ($i_today - $i_current_day) / (60*60*24);
			if ($i_days_left < 1)
				return $s_date."<br /><b>Finished</b>";
			
			// check if there's a cached value
			$o_cached = $this->getCachedData($s_date, $i_sample);
			if ($o_cached !== NULL)
				return $s_date."<br />Cached</b>".$s_form.'
					<script type="text/javascript">
						document.forms[0].submit();
					</script>';
			
			// compute the new value
			$this->getIncomesByDayAndLevel($s_date, $i_sample, TRUE);
			return $s_date."<br />Loading...</b>".$s_form.'
				<script type="text/javascript">
					setTimeout(function() {
						document.forms[0].submit();
					}, 3000);
				</script>';
		}
		
		// combines all of the files from the start date until now into one huge file
		// this is for speed improvements in reading data
		function combineFiles($s_start_time = '', $s_end_time = '', $dir = '', $dest = 'compiled', $suffix = 'cached', $insert_beggining = '', $insert_middle = '', $insert_end = '') {
			// initialize some variables
			$i_start_time = ($s_start_time == '') ? strtotime('2010-05-01') : strtotime($s_start_time);
			$i_end_time = ($s_end_time == '') ? strtotime(date('Y-m-d')) : strtotime($s_end_time);
			$i_load_time = $i_start_time;
			if ($dir !== '' && !strstr($dir, '/')) $dir .= '/';
			$file = fopen(dirname(__FILE__).'/RestaurantIncomes/'.$dir.$dest.'.txt', 'w');
			
			if ($file === FALSE)
				return FALSE;
			
			fwrite($file, $insert_beggining);
			$insert_next = '';
			
			// combine the files
			while ($i_load_time <= $i_end_time) {
				$s_date = date('Y-m-d', $i_load_time);
				$s_filename = dirname(__FILE__).'/RestaurantIncomes/'.$dir.$s_date.'_'.$suffix.'.txt';
				if (file_exists($s_filename)) {
					fwrite($file, $insert_next);
					fwrite($file, file_get_contents($s_filename));
					$insert_next = $insert_middle;
				}
				$i_load_time += 60*60*24;
			}
			
			fwrite($file, $insert_end);
			fclose($file);
		}
		
		function combineFilesByDayByClass() {
			$this->combineFiles('', '', 'IncomeByDayByClass', 'compiled', 'incomeByClass', '[', ',', ']');
		}
		
		function saveJsonToFile(&$a_data, $s_filename) {
			$file = fopen($s_filename, 'w');
			if (!$file)
				return;
			fwrite($file, lavu_json_encode($a_data));
			fclose($file);
		}
		
		function calculateSummariesByDayEachDay() {
			$s_start_date = (isset($_GET['start'])) ? $_GET['start'] : '2010-05-02';
			$i_start_time = strtotime($s_start_date);
			$i_end_time = strtotime(date('Y-m-d'));
			$s_retval = '';
			$s_next_date = date('Y-m-d', $i_start_time+60*60*26);
			
			self::calculateSummariesByDay($i_start_time, $i_start_time);
			
			$s_retval .= $s_start_date.'<br />';
			if ($i_start_time > $i_end_time) {
				$s_retval .= 'Finished. Combining files. Please wait.
					<form id="loadNext" method="get" action="'.basename(__FILE__).'"><input type="hidden" name="combineFilesByDayByClass" value="1"></form>
					<script type="text/javascript">
						setTimeout(function() {
							document.getElementById("loadNext").submit();
						}, 10000);
					</script>';
			} else {
				$s_retval .= 'loading...
					<form id="loadNext" method="get" action="'.basename(__FILE__).'"><input type="hidden" name="start" value="'.$s_next_date.'"><input type="hidden" name="calculateSummariesByDayEachDay" value="1"></form>
					<script type="text/javascript">
						setTimeout(function() {
							document.getElementById("loadNext").submit();
						}, 100);
					</script>';
			}
			
			return $s_retval;
		}
		
		// sums up all low income, mid income, and high income restaurants by day
		function calculateSummariesByDay($i_start_time, $i_end_time) {
			global $a_income_brackets;
			
			// initialize some variables
			$s_directory = dirname(__FILE__).'/RestaurantIncomes/IncomeByDayByClass/';
			$s_base_file_name = '_incomeByClass.txt';
			$i_load_time = $i_start_time;//strtotime('2010-05-01');
			$i_end_time = $i_end_time;//strtotime(date('Y-m-d'));
			
			// compute and store the data
			while ($i_load_time <= $i_end_time) {
				
				// initialize the class data
				$a_classes = $a_income_brackets;
				foreach($a_classes as $k=>$a_class) {
					$a_classes[$k]['sum'] = 0;
					$a_classes[$k]['gained'] = 0;
					$a_classes[$k]['lost'] = 0;
					$a_classes[$k]['total'] = 0;
				}
				
				// get the date
				$s_date = date('Y-m-d', $i_load_time);
				$s_date_prev = date('Y-m-d', $i_load_time-60*60*22);
				$i_load_time += 60*60*26;
				
				// get the data
				$o_data = self::getCachedData($s_date, 6000, FALSE);
				$o_data_prev = self::getCachedData($s_date_prev, 6000, FALSE);
				if ($o_data == NULL)
					continue;
				$curr_rests = array();
				$prev_rests = array();
				
				// add the loaded values to the restaurant sums
				foreach($o_data->silver as $o_restaurant_data)
					$curr_rests[] = $o_restaurant_data;
				foreach($o_data->gold as $o_restaurant_data)
					$curr_rests[] = $o_restaurant_data;
				foreach($o_data->platinum as $o_restaurant_data)
					$curr_rests[] = $o_restaurant_data;
				foreach($o_data_prev->silver as $o_restaurant_data)
					$prev_rests[] = $o_restaurant_data;
				foreach($o_data_prev->gold as $o_restaurant_data)
					$prev_rests[] = $o_restaurant_data;
				foreach($o_data_prev->platinum as $o_restaurant_data)
					$prev_rests[] = $o_restaurant_data;
				
				foreach($curr_rests as $o_restaurant_data) {
					foreach($a_classes as $k=>$a_class) {
						if ($o_restaurant_data->val >= $a_class['min'] && $o_restaurant_data->val <= $a_class['max']) {
							$a_classes[$k]['sum'] += $o_restaurant_data->val;
							$a_classes[$k]['total']++;
						}
					}
				}
				
				// find the difference between yesterday and today
				// use this information to calculate how many restaurants were gained and how many were lost
				foreach($curr_rests as $o_curr_rest) {
					$b_found = FALSE;
					foreach($prev_rests as $o_prev_rest) {
						if ($o_curr_rest->dn == $o_prev_rest->dn) {
							$b_found = TRUE;
							break;
						}
					}
					if (!$b_found)
						foreach ($a_classes as $k=>$a_class)
							if ($o_curr_rest->val >= $a_class['min'] && $o_curr_rest->val <= $a_class['max'])
								$a_classes[$k]['gained']++;
				}
				foreach($prev_rests as $o_prev_rest) {
					$b_found = FALSE;
					foreach($curr_rests as $o_curr_rest) {
						if ($o_curr_rest->dn == $o_prev_rest->dn) {
							$b_found = TRUE;
							break;
						}
					}
					if (!$b_found)
						foreach ($a_classes as $k=>$a_class)
							if ($o_curr_rest->val >= $a_class['min'] && $o_curr_rest->val <= $a_class['max'])
								$a_classes[$k]['lost']++;
				}
				
				// count the total number gained and lost
				$i_rests_gained = 0;
				foreach($a_classes as $a_class)
					$i_rests_gained += $a_class['gained'];
				$i_rests_lost = 0;
				foreach($a_classes as $a_class)
					$i_rests_lost += $a_class['lost'];
				
				$i_num_restaurants = count($o_data->silver) + count($o_data->gold) + count($o_data->platinum);
				$a_classes[] = array('title'=>'numRestaurants', 'total'=>$i_num_restaurants);
				$a_classes[] = array('title'=>'day', 'date'=>$s_date, 'dow'=>date('D', $i_load_time));
				$a_classes[] = array('title'=>'gainedLost', 'gained'=>$i_rests_gained, 'lost'=>$i_rests_lost);
				
				// save the data
				$file = fopen($s_directory.$s_date.$s_base_file_name, 'w');
				fwrite($file, lavu_json_encode($a_classes));
				fclose($file);
			}
		}
		
		// loads the data about total income for all restaurants across all days
		function loadSumIncomeForRestaurants($i_start_time, $i_end_time, $s_cached_filename) {
			$i_load_time = $i_start_time;
			$s_json = '';
			
			if (file_exists($s_cached_filename)) {
				
				// load the data
				$file = fopen($s_cached_filename, 'r');
				$s_json = fgets($file, 1048576);
			} else {
				
				// compute the data
				while ($i_load_time <= $i_end_time) {
					
					// get the date
					$s_date = date('Y-m-d', $i_load_time);
					$i_load_time += 60*60*24;
					
					// get the data
					$o_data = self::getCachedData($s_date, 6000);
					if ($o_data == NULL)
						continue;
					
					// add the loaded values to the restaurant sums
					foreach($o_data->silver as $o_restaurant_data) {
						$s_dataname = $o_restaurant_data->dn;
						if (!isset($a_restaurants[$s_dataname]))
							$a_restaurants[$s_dataname] = array('i', 'd');
						$a_restaurants[$s_dataname]['i'] += $o_restaurant_data->val;
						$a_restaurants[$s_dataname]['d'] += 1;
					}
					foreach($o_data->gold as $o_restaurant_data) {
						$s_dataname = $o_restaurant_data->dn;
						if (!isset($a_restaurants[$s_dataname]))
							$a_restaurants[$s_dataname] = array('i', 'd');
						$a_restaurants[$s_dataname]['i'] += $o_restaurant_data->val;
						$a_restaurants[$s_dataname]['d'] += 1;
					}
					foreach($o_data->platinum as $o_restaurant_data) {
						$s_dataname = $o_restaurant_data->dn;
						if (!isset($a_restaurants[$s_dataname]))
							$a_restaurants[$s_dataname] = array('i', 'd');
						$a_restaurants[$s_dataname]['i'] += $o_restaurant_data->val;
						$a_restaurants[$s_dataname]['d'] += 1;
					}
				}
				
				// save the data
				$s_json = lavu_json_encode($a_restaurants);
				unset($a_restaurants);
				self::saveJsonToFile($s_json, $s_cached_filename);
			}
			
			return $s_json;
		}
		
		// find the incomes for active restaurants from the start of time to the end of time
		function viewRestaurantDistribution() {
			// initialize some variables
			$i_start_time = strtotime('2010-05-01');
			$i_end_time = strtotime(date('Y-m-d'));
			$s_retval = '';
			$s_cached_filename = dirname(__FILE__).'/RestaurantIncomes/sumOfIncomes.txt';
			
			// include jquery and flot scripts
			$s_retval .= '
				<script type="text/javascript" src="/manage/js/jquery/js/jquery-1.9.0.js"></script>
				<script language="javascript" type="text/javascript" src="/newcp/backend/js/jquery/flot/jquery.flot.js"></script>';
			
			// load the sum income for each restaurant across all days
			$s_json = self::loadSumIncomeForRestaurants($i_start_time, $i_end_time, $s_cached_filename);
			
			// print the data in a form that javascript can understand
			$s_retval .= '
				<script type="text/javascript">
					json_data = JSON.parse('.$s_json.');';
			
			// load the javascript to handle the data
			ob_start();
			require(dirname(__FILE__).'/RestaurantIncome.js');
			$s_retval .= ob_get_contents();
			ob_end_clean();
			
			$s_retval .= '
					init = initFunctions.viewRestaurantDistribution;
				</script>
				<div id="plotbox" style="width:800; height:500">&nbsp</div>';
			
			return $s_retval;
		}
		
		function viewFinalStatistics() {
			$s_cached_filename = 'RestaurantIncomes/IncomeByDayByClass/compiled.txt';
			$file = fopen(dirname(__FILE__).'/'.$s_cached_filename, 'r');
			if (file === FALSE) return;
			
			$s_retval = '';
			$s_retval .= '
			<script type="text/javascript" src="/manage/js/jquery/js/jquery-1.9.0.js"></script>
			<script language="javascript" type="text/javascript" src="/newcp/backend/js/jquery/flot/jquery.flot.js"></script>';
			$s_retval .= '
			<script type="text/javascript">
				'.file_get_contents(dirname(__FILE__).'/RestaurantIncome.js').'
				restDataJSON = \''.fgets($file, 1024*1000).'\';
				restData = JSON.parse(restDataJSON);
				init = initFunctions.viewFinalData;
			</script>';
			$s_retval .= '
			<style type="text/css">
				div.box {
					width:800;
					height:500;
				}
			</style>';
			$s_retval .= '
				<div id="plotbox_restaurantsToIncome"><div class="box"></div><div class="label"></div></div>
				<div>Comparison of number of restaurants to income by day</div>
				<div id="plotbox_gainedLost"><div class="box"></div><div class="label"></div></div>
				<div>Comparison of restaurants gained or lost to total number of restaurants by day</div>
				<div id="plotbox_combined"><div class="box"></div><div class="label"></div></div>
				<div>Combined</div>
				<div id="plotbox_interactive"><div class="box"></div><div class="label"></div></div>
				<div>Interactive</div>';
			
			return $s_retval;
		}
		
	}
	
	// check if a command has been posted
	$o_income = new restaurantIncomeStatistics();
	$s_command = '';
	$o_income->getPostvars($s_command, 's_command', '');
	if ($s_command !== '') {
		if (method_exists($o_income, $s_command)) {
			echo $o_income->$s_command();
		} else {
			echo "command not recognized";
		}
	} else {
		// getAllDays, combineFiles, viewRestaurantDistribution, calculateSummariesByDayEachDay, viewSummaryOfRestaurants
		if (isset($_GET['get_all_days'])) {
			echo $o_income->getAllDays();
		} else if (isset($_GET['view_restaurant_distribution'])) {
			echo $o_income->viewRestaurantDistribution();
		} else if (isset($_GET['combine_files'])) {
			echo $o_income->combineFiles();
		} else if (isset($_GET['calculateSummariesByDayEachDay'])) {
			echo $o_income->calculateSummariesByDayEachDay();
		} else if (isset($_GET['combineFilesByDayByClass'])) {
			echo $o_income->combineFilesByDayByClass();
		} else if (isset($_GET['viewFinalStatistics'])) {
			echo $o_income->viewFinalStatistics();
		}
	}
	
?>
