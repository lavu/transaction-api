flattenBy = 10;

function initFunctionsClass () {
	this.viewRestaurantDistribution = function() {
		var restaurants = [];
		var income_data = [];
		var days_data = [];
		var income_adjust = 1000;
		
		$.each(json_data, function(data_name, restaurant) {
			var new_restaurant = { data_name:data_name, income:0, days_with_lavu:0 };
			new_restaurant.income = parseInt(restaurant.i);
			new_restaurant.days_with_lavu = parseInt(restaurant.d);
			if (new_restaurant.income < 3000000 && new_restaurant.days_with_lavu < 2000 && new_restaurant.income >= 10)
				restaurants.push(new_restaurant);
		});
		
		restaurants = $(restaurants).sort(function(a, b) {
			return (a.income < b.income) ? -1 : 1;
		});
		
		$.each(restaurants, function(i, restaurant) {
			income_data.push([i,restaurant.income/income_adjust]);
		});
		
		$.each(restaurants, function(i, restaurant) {
			days_data.push([i,restaurant.days_with_lavu]);
		});
		
		$.plot($("#plotbox"), [
			{ color: 2, data: days_data, lines: { show:false }, points: { show:true, radius:1 } },
			{ color: 1, data: income_data },
		], {legend: { show:true }} );
	}
	
	this.getData_gainedLost = function(restData) {
		var retval = { header: [{title:'gained'},{title:'lost'},{title:'placeholder'},{title:'numRestaurants'}], data: [[],[],[],[]], divisors: [0,0,1,0] };
		var maxGained = 0;
		var maxLost = 0;
		var maxDay = restData.length;
		var maxRestaurants = 0;
		
		for (var i = 1; i < maxDay; i++) {
			var gained = 0;
			var lost = 0;
			var numRestaurants = restData[i][3].total;
			if (restData[i][5] && restData[i][5].title == "gainedLost") {
				var gained = parseInt(restData[i][5].gained) || 0;
				var lost = parseInt(restData[i][5].lost) || 0;
			}
			retval.data[0].push(gained);
			retval.data[1].push(lost);
			retval.data[3].push(numRestaurants);
			maxRestaurants = Math.max(numRestaurants, maxRestaurants);
		}
		
		gained_flattened = this.flatten_data(retval.data[0], flattenBy);
		lost_flattened = this.flatten_data(retval.data[1], flattenBy);
		$.each(gained_flattened, function(k,v) {
			maxGained = Math.max(v, maxGained);
		});
		$.each(lost_flattened, function(k,v) {
			maxLost = Math.max(v, maxLost);
		});
		
		retval.divisors[0] = 1;
		retval.divisors[1] = 1;
		retval.divisors[3] = maxRestaurants / Math.max(maxGained, maxLost);
		
		$.each(retval.data, function(i, datapoints) {
			if (retval.header[i].title != 'placeholder') {
				$.each(datapoints, function(k, v) {
					retval.data[i][k] = v/retval.divisors[i];
				});
			}
		});
		
		return retval;
	}
	
	this.getData_restaurantsToIncome = function(restData) {
		var retval = { header: [{title:'low'},{title:'mid'},{title:'max'},{title:'numRestaurants'}], data: [[],[],[],[]], divisors: [0,0,0,1] };
		
		$.each(restData, function(k,v) {
			var desired_max = parseInt(v[3].total);
			retval.data[3].push(parseInt(v[3].total));
			
			for (var i = 0; i < 3; i++) {
				var sum = parseInt(v[i].sum);
				var total = (v[i].total && parseInt(v[i].total) > 0 ) ? parseInt(v[i].total) : 1;
				var val = sum/total;
				retval.data[i].push(val);
				desired_value = Math.max(desired_max*(i+1), 1);
				retval.divisors[i] += parseInt(val / desired_value);
			}
		});
		
		var count = retval.data[0].length;
		retval.divisors[3] = count;
		$.each(retval.divisors, function(k,v) {
			retval.divisors[k] = v/count;
		});
		$.each(retval.data, function(k,datapoints) {
			$.each(datapoints, function(i,v) {
				retval.data[k][i] = v/retval.divisors[k];
			});
		});
		
		return retval;
	}
	
	this.flatten_data = function(datapoints, range_length) {
		var retval = [];
		
		var range = [-range_length, range_length];
		var length = datapoints.length;
		for (var i = 0; i < length; i++) {
			var bottom = Math.max(0, i+range[0]);
			var top = Math.min(length-1, i+range[1]);
			var count = Math.max(top-bottom,1);
			var sum = 0;
			for (var j = bottom; j <= top; j++) {
				sum += datapoints[j];
			}
			retval.push(sum/count);
		}
		
		return retval;
	}
	
	this.plotData = function(dataset, jplotbox) {
		var plotable = [];
		var object = this;
		$.each(dataset.data, function(index,datapoints) {
			datapoints = object.flatten_data(datapoints, flattenBy);
			$.each(datapoints, function(k,v) {
				datapoints[k] = [k,v];
			});
			plotable.push({
				color: index+1,
				data: datapoints,
				label: dataset.header[index].title+' (x'+parseInt(dataset.divisors[index])+')',
			});
		});
		
		var jlabelbox = jplotbox.children('.label');
		var options = {
			legend: {
				show: true,
				container: jlabelbox,
			}
		}
		
		$.plot(jplotbox.children('.box'), plotable, options);
		
		jlabelbox.height(jlabelbox.children().children().height());
		jlabelbox.children().height(jlabelbox.children().children().height());
	}
	
	this.viewFinalData = function() {
		var data_restaurantsToIncome = initFunctions.getData_restaurantsToIncome(restData);
		initFunctions.plotData(data_restaurantsToIncome, $('#plotbox_restaurantsToIncome'));
		var data_gainedLost = initFunctions.getData_gainedLost(restData);
		initFunctions.plotData(data_gainedLost, $('#plotbox_gainedLost'));
	}
}

initFunctions = new initFunctionsClass();

setTimeout(function() {
	$(document).ready(function() {
		init();
	});
}, 500);