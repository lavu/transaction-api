<?php
	//Author: Ezra Stallings
?>

<!DOCTYPE html>
<meta charset="utf-8">
<body style='background:#111'>
<script src="https://d3js.org/d3.v3.min.js"></script>
<script src="https://d3js.org/topojson.v1.min.js"></script>

<script src="location_data.js"></script>
<script src="datamaps.all.min.js"></script>
<style>

.onoffswitch {
	position: relative; width: 55px;
	-webkit-user-select:none; -moz-user-select:none; -ms-user-select: none;
}
.onoffswitch-checkbox {
	display: none;
}
.onoffswitch-label {
	display: block; overflow: hidden; cursor: pointer;
	height: 20px; padding: 0; line-height: 20px;
	border: 0px solid #FFFFFF; border-radius: 27px;
	background-color: #DDDDDD;
}
.onoffswitch-label:before {
	content: "";
	display: block; width: 27px; margin: -3.5px;
	background: #09FF00;
	position: absolute; top: 0; bottom: 0;
	right: 31px;
	border-radius: 27px;
	box-shadow: 0 6px 12px 0px #757575;
}
.onoffswitch-checkbox:checked + .onoffswitch-label {
	background-color: #DDDDDD;
}
.onoffswitch-checkbox:checked + .onoffswitch-label, .onoffswitch-checkbox:checked + .onoffswitch-label:before {
   border-color: #DDDDDD;
}
.onoffswitch-checkbox:checked + .onoffswitch-label .onoffswitch-inner {
	margin-left: 0;
}
.onoffswitch-checkbox:checked + .onoffswitch-label:before {
	right: 0px; 
	background-color: #09FF00; 
	box-shadow: 3px 6px 18px 0px rgba(0, 0, 0, 0.2);
}
</style>
<div style='display:inline-block'><h2 style='color:white; font-family:Sans-Serif'>Location Heatmap By Country</h2></div>
&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
<div style='display:inline-block'>
	<div style='display:inline-block'><h3 style='color:white; font-family:Sans-Serif'>World Map</h2></div>
	<div style='display:inline-block' class="onoffswitch">
		<input onchange='toggleMapType()' type="checkbox" name="onoffswitch" class="onoffswitch-checkbox" id="myonoffswitch" checked>
		<label class="onoffswitch-label" for="myonoffswitch"></label>
	</div>
	<div style='display:inline-block'><h3 style='color:white; font-family:Sans-Serif'>USA Map</h3></div>
</div>

<div id="container1" style="position: relative; width: 80%;"></div>
<script>
FILLS = {
		defaultFill: '#111',
		"0":         '#111',
		"1":         '#10E',
		"2":         '#20D',
		"3":         '#30C',
		"4":         '#40B',
		"5":         '#50A',
		"6":         '#609',
		"7":         '#708',
		"8":         '#807',
		"9":         '#906',
		"A":         '#A05',
		"B":         '#B04',
		"C":         '#C03',
		"D":         '#D02',
		"E":         '#E01',
		"F":         '#F00',
		"PIN":       'rgba(0,255,0,0.7)'
};

// FILLS = {
// 		defaultFill: '#111',
// 		"0":         '#111',
// 		"1":         '#001',
// 		"2":         '#002',
// 		"3":         '#003',
// 		"4":         '#004',
// 		"5":         '#005',
// 		"6":         '#006',
// 		"7":         '#007',
// 		"8":         '#008',
// 		"9":         '#009',
// 		"A":         '#00A',
// 		"B":         '#00B',
// 		"C":         '#00C',
// 		"D":         '#00D',
// 		"E":         '#00E',
// 		"F":         '#00F',
// 		"PIN":       'rgba(0,255,0,0.7)'
// };

var bins = [
	{cutoff:0, name: "None", fill: "0"},
	{cutoff:1, name: "1", fill: "1"},
	{cutoff:5, name: "2-5", fill: "2"},
	{cutoff:10, name: "6-10", fill: "3"},
	{cutoff:15, name: "11-15", fill: "4"},
	{cutoff:20, name: "16-20", fill: "5"},
	{cutoff:25, name: "21-25", fill: "6"},
	{cutoff:50, name: "26-50", fill: "7"},
	{cutoff:75, name: "51-75", fill: "8"},
	{cutoff:100, name: "76-100", fill: "9"},
	{cutoff:150, name: "101-150", fill: "A"},
	{cutoff:200, name: "151-200", fill: "B"},
	{cutoff:250, name: "201-250", fill: "C"},
	{cutoff:300, name: "251-300", fill: "D"},
	{cutoff:500, name: "301-500", fill: "E"},
	{cutoff:Infinity, name: "500+", fill: "F"}
];

var country_count = {};
var state_count = {};
var world_pins = [];
var RAW_DATA_ROWS = {DATA_NAME:0,IP_ADDRESS:1,CITY:2,REGION:3,COUNTRY_CODE_2:4,LAT:5,LON:6,POSTAL:7};
var pin_radius = 2;
// {name: 'Hot', latitude: 21.32, longitude: 5.32, radius: 10, fillKey: 'gt50'},
RAW_DATA.map(function(d){
	var code3 = COUNTRY_CODE_MAP[d[RAW_DATA_ROWS.COUNTRY_CODE_2]];
	world_pins.push({name: d[RAW_DATA_ROWS.DATA_NAME], latitude: d[RAW_DATA_ROWS.LAT], longitude: d[RAW_DATA_ROWS.LON], radius: pin_radius, fillKey: "PIN"});
	if(country_count[code3] == undefined){
		country_count[code3] = 0;
	}
	if(code3 == "USA"){
		var state_code = STATE_CODE_MAP[d[RAW_DATA_ROWS.REGION]];
		if(state_count[state_code] == undefined){
			state_count[state_code] = 0;
		}
		state_count[state_code]++;
	}
	country_count[code3]++;
});

//Bin by country
var country_bins = {};
for(var i in country_count){
	for(var j = 0; j <= bins.length; j++){
		if(bins[j].cutoff > country_count[i]){
			country_bins[i] = {count: country_count[i], fillKey: bins[j].fill};
			break;
		}
	}
}

//Bin by state
var state_bins = {};
for(var i in state_count){
	for(var j = 0; j < bins.length; j++){
		if(bins[j].cutoff >= state_count[i]){
			state_bins[i] = {count: state_count[i], fillKey: bins[j].fill};
			break;
		}
	}
}

MODE = 'world';
function toggleMapType(){
	MODE = (MODE == 'world') ? 'usa' : 'world';
	document.getElementById('container1').innerHTML = '';

	//basic map config with custom fills, mercator projection
	var map = new Datamap({
		scope: MODE,
		element: document.getElementById('container1'),
		projection: 'mercator',
		height: 700,
		fills: FILLS,

		data: MODE == 'usa' ? state_bins : country_bins,
		geographyConfig: {
			dataUrl: null,
			hideAntarctica: false,
			borderWidth: 1,
			borderColor: '#FDFDFD',
			popupTemplate: function(geography, data) {
				var ct = data && data.count ? data.count : 0;
				return '<div class="hoverinfo"><strong>' + geography.properties.name + ' (' + ct + ')</strong></div>';
			},
			popupOnHover: true,
			highlightOnHover: true,
			highlightFillColor: '#FC8D59',
			highlightBorderColor: 'rgba(250, 15, 160, 0.2)',
			highlightBorderWidth: 2
		},
		bubblesConfig: {
			borderWidth: 0,
			borderColor: '#FFFFFF',
			popupOnHover: true,
			radius: null,
			popupTemplate: function(geography, data) {
				return '<div class="hoverinfo"><strong>' + data.name + '</strong></div>';
			},
			fillOpacity: 0.75,
			animate: true,
			highlightOnHover: true,
			highlightFillColor: '#FC8D59',
			highlightBorderColor: 'rgba(250, 15, 160, 0.2)',
			highlightBorderWidth: 2,
			highlightFillOpacity: 0.85,
			exitDelay: 100
		}
	})

	//bubbles, custom popup on hover template
	map.bubbles(world_pins);
}
toggleMapType();

</script>
</body>