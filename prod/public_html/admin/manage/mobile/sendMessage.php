<?php
	session_start();
	require_once ($_SERVER['DOCUMENT_ROOT']."/cp/resources/core_functions.php");
	
	if(isset($_POST['username']) && isset($_POST['message'])&&  isset($_POST['recipient']) )
		sendMessage($_POST['username'], $_POST['message'], $_POST['recipient']);
	else{
		echo "fail";
		return;
	}	
	
	function sendMessage($username, $message, $recipient){
		mlavu_query("INSERT INTO `poslavu_MAIN_db`.`Chats` (`priority`, `username`, `audience`, `time`, `message`, `viewedBy`) VALUES ('normal', '[1]','[2]','[3]','[4]','[1]') ", $username, $recipient, date('Y-m-d H:i:s'), $message);
		if(mlavu_dberror()){
			echo mlavu_dberror();
			return;
		}
		else
			mailNotification($recipient, $username);
	}	
	function mailNotification($recipient, $username){
		$query=mlavu_query("SELECT `email` FROM `poslavu_MAIN_db`.`accounts` WHERE (`type` LIKE '%[1]%' OR `username`='[1]') AND `username` !='[2]' ", $recipient, $username);
		$recipients=array();
		while($read = mysqli_fetch_assoc($query)){
			if( $read['email'] !='' ){
				mail($read['email'],"New Notification in Manage", $username.", has sent you a message in /manage. To view this message login to admin.poslavu.com/manage and click the my account link on the top right of the page.");
				$recipients[]=$read['email'];
			}
		}
		
		$r=implode(", ",$recipients);
		echo "message(s) sent successfully to: $r.";
	}
	
?>