<?php
	session_start();
	require_once ($_SERVER['DOCUMENT_ROOT']."/cp/resources/core_functions.php");
	
	if(isset($_POST['dataname']) && isset($_POST['tab']) )
		$_POST['tab']($_POST['dataname']);
	else{
		echo "fail";
		return;
	}	
	
	//function openAccount($dataname){
	//	mlavu_query("SELECT * from `poslavu_[1]_db`.`locations` ");
	//}
	function notesTab($dataname){
		$id= mysqli_fetch_assoc(mlavu_query("SELECT `id` FROM `poslavu_MAIN_db`.`restaurants` where `data_name`='[1]'", $dataname));
		$id= $id['id'];
	
		if($_POST['type']=='Lite'){
			$notes= getLiteNotes($id);
			if(count($notes)){
				$returnString='<table id="notesTable"><tr><th>Note</th><th>Ninja</th><th>Date</th></tr>';
					foreach($notes as $notes_read) {
						$returnString.="<tr><td class='noteRow'>".$notes_read['message']."</td><td class='noteRow'>".$notes_read['support_ninja']."</td><td class='noteRow'>".$notes_read['created_on']."</td></tr>";
					}
					echo $returnString."</table>";
			}else
				echo "<table id='notesTable'><tr><td>There are no notes on this account.</td></tr></table>";
		}
		else{
			if($id!=''){
				$notes_query = mlavu_query("SELECT * FROM `poslavu_MAIN_db`.`restaurant_notes` WHERE `restaurant_id`=[1] ORDER BY id DESC",$id);
				$result=array();
				if(mysqli_num_rows($notes_query)){
					$returnString='<table id="notesTable"><tr><th>Note</th><th>Ninja</th><th>Date</th></tr>';
					while( $notes_read = mysqli_fetch_assoc($notes_query) ) {
						$returnString.="<tr><td class='noteRow'>".$notes_read['message']."</td><td class='noteRow'>".$notes_read['support_ninja']."</td><td class='noteRow'>".$notes_read['created_on']."</td></tr>";
					}
					echo $returnString."</table>";
				}
				else
					echo "<table id='notesTable'><tr><td>There are no notes on this account.</td></tr></table>";
			}
			else 
				echo "<table id='notesTable'><tr><td>An error has occurred retrieving notes for this account. Please try again later.</td></tr></table>";
		}
	}
	function clientInfoTab($dataname){
		$info= getCustomerInfo($dataname);
		echo "
			<table id='customerInfoTable'>
				<tr >
					<td class='noteRow'> ID</td>
					<td class='noteRow' id= 'infoid' > ".$info['id']." </td>
				</tr>
				<tr>
					<td class='noteRow'> Company Name</td>
					<td class='noteRow'> ".$info['company_name']." </td>
				</tr>
				<tr>
					<td class='noteRow'> Data Name</td>
					<td class='noteRow'> ".$info['data_name']."</td>
				</tr>
				<tr>
					<td class='noteRow'> Account Created</td>
					<td class='noteRow'> ".$info['created']."</td>
				</tr>
				<tr>
					<td class='noteRow'>Last Activity</td>
					<td class='noteRow'> ".$info['last_activity']."</td>
				</tr>
				<tr>
					<td class='noteRow'>Chain</td>
					<td class='noteRow' id= 'infoChain'> ".$info['chain_name']."</td>
					<td class='noteRow' id= 'editChain' style='display:none'> <input type= 'text' name='editChainName' value='".$info['chain_name']."'/></td>
				</tr>
				<tr>
					<td class='noteRow'>License</td>
					<td class='noteRow'> ".$info['package']." </td>
				</tr>

				<tr>
					<td class='noteRow' > Status</td>
					<td class='noteRow' > ".$info['status']." </td>
				</tr>
				<tr>
					<td class='noteRow'> Name</td>
					<td class='noteRow' id='infoName'> ".$info['firstname']."  ".$info['lastname']."</td>
			
					</td>
				</tr>
				<tr>
					<td class='noteRow'> Email</td>
					<td id='infoEmail' class='noteRow'> ".$info['email']." </td>
				</tr>
				<tr>
					<td class='noteRow'> Phone</td>
					<td class='noteRow' id='infoPhone'>  ".$info['phone']." </td>
				</tr>
				<tr>
					<td class='noteRow'> Address</td>
					<td id='infoAddress' class='noteRow'>  ".$info['address']." </td>
				</tr>
				<tr>
					<td class='noteRow'> Distro Code</td>
					<td id='infoDistro'class='noteRow'>  ".$info['distro_code']." </td>
					
				</tr>
				<tr>
					<td class='noteRow'>Local Server</td>
					<td id='infoDistro'class='noteRow'>  ".$info['hasLLS']." </td>
					
				</tr>
			</table>";
		
	}
	function clientControlTab($dataname){
		$id= mysqli_fetch_assoc(mlavu_query("SELECT `id` FROM `poslavu_MAIN_db`.`restaurants` where `data_name`='[1]'", $dataname));
		$id= $id['id'];
		if( $_POST['type']!='Lite'){
			echo" 			
			<table id='clientControl'>
				<tr>
					<th colspan=3 class='areaHeader'> Client Control </th>
				</tr>
				<tr>
					<td> 
						<button class='submitButton' onclick='window.open(\"/manage/misc/MainFunctions/custLogin.php?site=new&mode=login&custID=".$id."&custCode=".$dataname."\", \"_blank\");  window.focus(); ' >Client Control Panel(V2)
						</button>
					</td>
					<td> 
						<button class='submitButton' onclick='window.open(\"/manage/misc/MainFunctions/custLogin.php?site=old&mode=login&custID=".$id."&custCode=".$dataname."\", \"_blank\");  window.focus(); ' >Client Control Panel(CP)
						</button>
					</td>
					<td>
						<button class='submitButton' id='createTempAccount' onclick='getTempAccountInfo(\"".$id ."\",\"".$dataname."\",\"".$_POST['username']."\") '>Create Temp Account</a>
						</button>
					</td>
				</tr>
			</table>";
		}else{
			echo"
			<table id='clientControl'>
				<tr><th colspan=3> Client Control </th></tr>
				<tr>
					<td> 
						<button class='submitButton' onclick='window.open(\"http://admin.lavulite.com/manage/misc/MainFunctions/custLogin.php?site=new&mode=login&custID=".$id."&custCode=".$dataname."\", \"_blank\");  window.focus(); ' >
							Client Control Panel(Newcp)
						</button>
					</td>
					<td>
						<button  class='submitButton' onclick='window.open(\"http://admin.lavulite.com/manage/misc/MainFunctions/custLogin.php?mode=create_temp&custID=".$id."&custCode=".$dataname."&username=".$_POST['username']."\", \"_blank\");  window.focus(); '>Create Temp Account</a></button>
					</td>
				</tr>
			</table>
		";	
		}
	}
	function deviceInfoTab($dataname){
		$id= mysqli_fetch_assoc(mlavu_query("SELECT `id` FROM `poslavu_MAIN_db`.`restaurants` where `data_name`='[1]'", $dataname));
		$id= $id['id'];
		$device_query = mlavu_query("SELECT `lavu_admin`,`prefix`,`MAC`,`UUID`,`name`,`device_time`,`ip_address`,`model`,`system_version`,`app_name`,`poslavu_version`,`last_checkin`, `UDID`,`loc_id`, `title` AS `location_name` 
		FROM `poslavu_[2]_db`.`devices` LEFT JOIN `poslavu_[2]_db`.`locations` ON `poslavu_[2]_db`.`locations`.`id` = `loc_id` WHERE `company_id`='[1]' AND `inactive` != '1' ORDER BY `last_checkin` desc ", $id, $dataname);
		
		if( mlavu_dberror())
			echo "<table id='notesTable'><tr><td>".mlavu_dberror()."</td></tr></table>";
		else if(mysqli_num_rows($device_query)){
			$returnString='<table id="deviceTable"><tr><th>Name/IP</th><th>Model</th><th>iOS</th><th>Version</th><th>Location</th><th>iOS</th><th>Last Checkin</th><th>Last Reload Settings</th></tr>';
			
			while($device_read = mysqli_fetch_assoc($device_query)){
				
				$device_read['last_reload']="<input type='button' class='submitButton' value='Get Time' onclick= \"getReloadTime('".$device_read['MAC']."','".$dataname."', '".$device_read['ip_address']."')\">";
					$returnString.="<tr style='font-size: smaller;'>
						<td class='deviceRow'>".$device_read['name']."/".$device_read['ip_address']."</td>
						<td class='deviceRow'>".$device_read['model']."</td>
						<td class='deviceRow'>".$device_read['system_version']."</td>
						<td class='deviceRow'>".$device_read['poslavu_version']."</td>
						<td class='deviceRow'>".$device_read['location_name']."</td>
						<td class='deviceRow'>". str_pad($device_read['prefix'], 2, "0", STR_PAD_LEFT)."</td>
						<td class='deviceRow'>".$device_read['last_checkin']."</td>
						<td class='deviceRow' id=".$device_read['ip_address'].">".$device_read['last_reload']."</td>";

			}
			echo $returnString."</table>";
		}
		else 
			echo "<table id='notesTable'><tr><td>There are no devices on this account.</td></tr></table>";
	}

	function getCustomerInfo($search_term){
		
		$info_query1 = "SELECT * FROM `poslavu_MAIN_db`.`restaurants` WHERE `data_name`='[1]'";
		$info_query2 = "SELECT * FROM `poslavu_MAIN_db`.`signups` WHERE `dataname`='[1]'";
		$localServerQuery= mlavu_query("SELECT * FROM `poslavu_MAIN_db`.`local_servers` WHERE `data_name`='[1]'", $search_term);
		
		$e_search_term = ConnectionHub::getConn('poslavu')->escapeString($search_term);
		
		$info1 = (mlavu_query($info_query1, $e_search_term));
		$info2 = (mlavu_query($info_query2, $e_search_term));
		
		$info = mysqli_fetch_assoc($info1);
		
		if(mysqli_num_rows($info2)){
			$info = array_merge(mysqli_fetch_assoc($info2), $info);
		}
		
		if($info['disabled']==0)
			$info['disabled']= "E";
		else
			$info['disabled']= "D";
		
		if($info['package'] == '1')
			$info['package']="Silver";
		else if($info['package'] == '2')
			$info['package']="Gold";
		else if($info['package'] == '3')
			$info['package']="Platinum";
		else
			$info['package']	= "";
		
		if (mysqli_num_rows($localServerQuery))
			$info['hasLLS']='true';
		else
			$info['hasLLS']='false';
		
		//get modules
		$mods= mysqli_fetch_assoc(mlavu_query("SELECT `value` FROM `poslavu_[1]_db`.`config` WHERE `setting`='modules' ", $search_term));
		$info['modules']= $mods['value'];
		
		return $info;
	}

	function getLiteNotes($id){
		if($id!=''){
			$url ='admin.lavulite.com/manage/mainServerAccess/getNotes.php';
			$fields_string= 'id='.$id.'&key=984a63690360b061bd63b8ded55538f6';
			$ch = curl_init();

			//set the url, number of POST vars, POST data
			curl_setopt($ch,CURLOPT_URL, $url);
			curl_setopt($ch,CURLOPT_POST, 2);
			curl_setopt($ch,CURLOPT_POSTFIELDS, $fields_string);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			//execute post
			$result = curl_exec($ch);
			
			//close connection
			curl_close($ch);			
			return json_decode($result);
		}
		else 
			return array();
	}
?>
