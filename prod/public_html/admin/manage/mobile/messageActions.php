<?php
	session_start();
	require_once ($_SERVER['DOCUMENT_ROOT']."/cp/resources/core_functions.php");
	
	if(isset($_POST['username']) && isset($_POST['timestamp'])&&  isset($_POST['action']) )
		$_POST['action']($_POST['username'], $_POST['timestamp']);
	else{
		echo "fail";
		return;
	}	

	function viewMessage($username, $timestamp){
		
		if (!strstr(getViewedList($timestamp), $username)){
			mlavu_query("UPDATE `poslavu_MAIN_db`.`Chats` SET `viewedBy` = CONCAT( `viewedBy`, ',[1]') WHERE `time`='[2]'", $username, $timestamp);
			if (mlavu_dberror())
				echo mlavu_dberror();
			else
				echo 'success';
		}
		else
			echo 'success';
	}
	function getViewedList($timestamp){
		$read= mysqli_fetch_assoc(mlavu_query("SELECT `viewedBy` FROM `poslavu_MAIN_db`.`Chats` WHERE `time`='[1]' ", $timestamp));
		return $read['viewedBy'];
	}

?>