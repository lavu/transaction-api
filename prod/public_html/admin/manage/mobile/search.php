<?php
	require_once ($_SERVER['DOCUMENT_ROOT']."/cp/resources/core_functions.php");
	if(isset($_POST['param']))
		doSearch($_POST['param']);
	else{
		echo "fail";
		return;
	}	
	
	function doSearch($searchTerm){
	
		$query= mlavu_query("SELECT * FROM `poslavu_MAIN_db`.`restaurants` where (`data_name` LIKE '%[1]%' OR `company_name` LIKE '%[1]%' OR `company_code` LIKE '%[1]%' OR `email` LIKE '%[1]%') 
		AND `notes` NOT LIKE '%AUTO-DELETED%' AND `disabled` !='1' ", $searchTerm);
		
		if(mlavu_dberror()){
			echo mlavu_dberror();
			return;
		}
		if (!mysqli_num_rows($query)){
			echo "No companies match this query.";
			return;
		}
		
		$returnString="<table id='searchResults'><tr><td colspan=5> Number of Companies: ".mysqli_num_rows($query)."</td></tr>";
		
		while($read= mysqli_fetch_assoc($query)){
			if( $read['data_name']!=''){
				if( $read['lavu_lite_server']!='')
					$returnString.="<tr class='rowResultLite' onclick=' openAccount(\"".$read['data_name']."\", \"Lite\")'><td class='tdWidth'>".$read['company_name']."</td><td>".$read['data_name']."</td></tr>";
				else
					$returnString.="<tr class='rowResultMain' onclick='openAccount(\"".$read['data_name']."\",\"Main\")'><td class='tdWidth'>".$read['company_name']."</td><td >".$read['data_name']."</td></tr>";
				}
		}
		echo $returnString;
	}

?>