<?php
	require_once($_SERVER['DOCUMENT_ROOT']."/manage/error_config.php");
	require_once ($_SERVER['DOCUMENT_ROOT']."/cp/resources/core_functions.php");
	require_once("../login.php");
	session_start();
	if(!account_loggedin()){
		 header('Location: http://admin.poslavu.com/manage/login.php' ) ;
	} 
	$mode=(isset($_GET['mode']))?$_GET['mode']:"";
			
	/*
		The reason I am doing this in the constructor is because its wayyy easier to just check to see if
		we just logged out here than to do something else :P 	
	*/
	if( $mode=='logout'){
		logout_account($_SESSION['admin_username']);
		header('Location: http://admin.poslavu.com/manage/login.php') ;
	}	
function getDropDown(){
	$returnString='<select id="minsLate"> ';
	
	for($i=5; $i<121; $i+=5){
		$returnString.="<option value='$i'> $i </option>";
	}
	
	$returnString.="</select>";
	return $returnString;
}


?>

<html>
	<head>
		<title>
			Mobile Manage
		</title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0"/>  
		<link rel="stylesheet" type="text/css" href="/manage/css/mobileStyles.css" />		
		
		<script type="text/javascript" src="/manage/js/jquery/js/jquery-1.9.0.js"></script>
		<script type="text/javascript" src="/manage/js/jquery/js/jquery-ui-1.10.0.custom.min.js"></script>
		<script type="text/javascript" src="/manage/js/mobileScripts.js"></script>
	</head>
	
	<body>
		
		<div id='loading'></div>
		<div id='loadingText'>Loading...</div>
		
		<input type='hidden' id='name' value='<?php echo $_SESSION['admin_username']; ?>'> 
		<div id='lateButton' class='buttonNav' onclick='showPage("late", $(this),"200")'> <span class='textPadding'>Going To Be Late</span></div>
		
		<div id='lateArea' >
			<div class='container'>
				<div id='minutesLate'>Minutes Late: <?php echo getDropDown();?> </div>
				<div id='reasonArea'> <textArea id='reason' name=reason cols=30 rows=5 placeholder='Reason You Are Late'></textArea></div>
				<input type='submit' class='submitButton' value='submit' onclick='submitLateNotice()'/>
			</div>
		</div>
		
		<div id='searchButton'  class='buttonNav' onclick='showPage("search", $(this),"400")'><span class='textPadding'>Search Account</span></div>
		
		<div id='searchArea' >
			<div class= 'container'>
				<div id='searchForm'>
					<input type='text' id='search' placeholder="Account Search"/>
					<input type='submit' class='submitButton' value='search' onclick='searchAccount($("#search").val())'/>
				</div>
				<div id='searchContainer'>
				
				</div>
				<div id='accountContainer'>
					<input type='hidden' id='dataname' value='' />
					<input type='hidden' id='type' value='' />
					<div id='tabContainer'>
						<div id='tabContainerScroller'>
							<span id='notesTab'        class='infoTabSelected' onclick='openTab($(this))'>Notes </span>
							<span id='clientInfoTab'   class='infoTab'         onclick='openTab($(this))'>Info  </span>
							<span id='deviceInfoTab'   class='infoTab'         onclick='openTab($(this))'>Device</span>
							<span id='clientControlTab'class='infoTab'         onclick='openTab($(this))'>Client</span>
						</div>
					</div>
					<div id='infoContainer'>
					
					</div>
				</div>
			</div>
		</div>
		<div id='messagesButton'class='buttonNav'  onclick='showPage("messages", $(this),"400")'><span class='textPadding'>Messaging</span></div>
		
		<div id='messagesArea' >
			<div id='messagesContainer'>
				<div id='tabContainer'>
					<div id='tabContainerScroller'>
						<span id='inbox'  class='infoTab' onclick='openMessagesTab($(this))'>Inbox </span>
						<span id='outbox' class='infoTab' onclick='openMessagesTab($(this))'>Outbox </span>
						<span id='compose' class='infoTabSelected' onclick='openMessagesTab($(this))'>Compose </span>
					</div>
				</div>
				<div id='messages'>
					<select id='group' onChange='$("#user").css("display","inline-block");'>
						<option value='lavu'>Lavu</option>
						<option value='support'>Support</option>
						<option value='dev'>Dev</option>
						<option value='user'>User</option>
					</select>	
					<select id='user' style='display:none; color:black;'>
						<option value=''></option>
						<?php
							$query=mlavu_query("SELECT `username`, `email` FROM `poslavu_MAIN_db`.`accounts` WHERE `type` !='terminated' AND `type` !='language' AND `type` != 'other' ORDER BY `username` asc ");
							while($read=mysqli_fetch_assoc($query))
								echo "<option value='".$read['username']."'>".$read['username']."</option>";
						?>
					</select>
					<br>
					<textarea id='message' cols='50' rows='10'></textarea>  
					<br>
					<input type='submit' class='submitButton' value='submit' onclick='createMessage($("#group").val(), $("#user").val(),$("#message").val() )'> 
				</div>
				
			</div>
		</div>
		<div id='authorizeLLSButton' class='buttonNav' onclick='showPage("authorizeLLS", $(this), "400")' <?php if (!can_access('billing_payment_status')) echo "style='display:none;'"; ?>><span class='textPadding'>Authorize LLS</span></div>
		
		<div id='authorizeLLS'>
			<div id='authorizeLLSContainer' style='padding-left:8px;'>
				<input type='textbox' size='20' name='dataname' placeholder='dataname' style='color:black;' /><br />
				<input type='button' value='Authorize' onclick='LLSauthorize(this, true);' class='submitButton' /> <input type='button' value='Deauthorize' onclick='LLSauthorize(this, false);' class='submitButton' /><br />
				<input type='button' value='Quick Stats' onclick='LLSgetQuickStats(this);' class='submitButton' /><br />
				<div class='quickStatsContainer'>&nbsp;</div>
			</div>
		</div>
		<div id='useMain'onclick='window.location="http://admin.poslavu.com/manage/index.php?useMain=true";'><span class='textPadding'>Main Manage</span></div> 
		<div id='logout' onclick='window.location.replace("http://admin.poslavu.com/manage/mobile/index.php?mode=logout");'><span class='textPadding'>Logout</span></div> 
	</body>
</html>