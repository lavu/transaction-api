<?php

	require_once ($_SERVER['DOCUMENT_ROOT']."/cp/resources/core_functions.php");
	require_once(dirname(__FILE__).'/../../sa_cp/billing/payment_profile_functions.php');
	require_once(dirname(__FILE__).'/../../sa_cp/billing/package_levels_object.php');
	require_once("../login.php");
	session_start();
	if(!account_loggedin()){
		 header('Location: http://admin.poslavu.com/manage/login.php' ) ;
	}

	$s_action = trim($_POST['action']);
	$s_dataname = trim($_POST['dn']);

	if ($s_dataname == '') {
		echo 'fail|Please provide a dataname.';
		exit();
	}
	$a_restaurants = ConnectionHub::getConn('poslavu')->getDBAPI()->getAllInTable('restaurants', array('data_name'=>$s_dataname), TRUE);
	if (count($a_restaurants) == 0) {
		echo 'fail|Restaurant not found.';
		exit();
	}
	$a_restaurant = $a_restaurants[0];

	switch($s_action) {
	case 'authorize':
		require_once(dirname(__FILE__)."/../misc/LLSFunctions/lls_authorization.php");
		if ($_POST['authorize'] == '1')
			$b_success = enableLLS($s_dataname, TRUE, TRUE);
		else
			$b_success = disableLLS($s_dataname, TRUE);

		if ($b_success)
			echo 'success|Success';
		else
			echo 'fail|Failed to update database';
		break;
	case 'getQuickStats':
		$a_package = find_package_status($a_restaurant['id']);
		$o_package_container = new package_container();
		echo 'Package: '.$o_package_container->get_printed_name_by_attribute('level', (int)$a_package['package']).'<br />';
		$a_authorizations = ConnectionHub::getConn('poslavu')->getDBAPI()->getAllInTable('authorizations', array('data_name'=>$s_dataname), TRUE);
		if (count($a_authorizations) > 0) {
			if ($a_authorizations[0]['lls_authorized'] == '0')
				echo 'Currently authorization: not authorized<br />';
			else
				echo 'Currently authorization: authorized<br />';
		} else {
			echo 'Currently authorization: not authorized<br />';
		}
		break;
	default:
		echo 'fail|Unknown action "'.$s_action.'".';
	}

?>
