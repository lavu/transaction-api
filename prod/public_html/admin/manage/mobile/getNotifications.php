<?php

	session_start();
	require_once($_SERVER['DOCUMENT_ROOT']."/manage/error_config.php");
	require_once($_SERVER['DOCUMENT_ROOT']."/cp/resources/core_functions.php");
	require_once(dirname(__FILE__)."/../login.php");
	if (!isset($_POST['username']) || !isset($_POST['function']) || !account_loggedin()){
		echo "error";
		return;
	}

	$_POST['function']($_POST['username']);

	function getNotifications($username, $query, $b_sender = FALSE){

		// build a mapping of user ids to names
		$o_ids_to_names = new stdClass();
		$a_accounts = ConnectionHub::getConn('poslavu')->getDBAPI()->getAllInTable('accounts', array('disabled'=>0), TRUE);
		foreach($a_accounts as $a_account)
			$o_ids_to_names->$a_account['id'] = $a_account['firstname'].' '.$a_account['lastname'];

		$retStr='<table id="messageContentsArea">';

		while($query !== FALSE && $notification = mysqli_fetch_assoc($query)){

			$backgroundDiv = ((int)$notification['status'] < 3) ? "notViewed" : "Viewed";
			$onclick = "";
			$urgent = "";

			$s_user_name = ($b_sender) ? "To ".$o_ids_to_names->$notification['receiver'] : "From ".$o_ids_to_names->$notification['sender'];

			$retStr.="
			<tr>
				<td>
					<div class='deleteCheckBox' style='display:none'>
						<input type='checkbox' value='".$notification['id']."'/>
					</div>
				</td>
				<td colspan='2' class='messagesCell {$backgroundDiv}' {$onclick}>
					<div class='messageSender'> ".$s_user_name."</div>
					<div class='messageText ".$urgent." '>".$notification['message']."</div>
					<div class='messageTime'>".$notification['timestamp']."</div>
				</td>
			</tr>";

		}
		echo $retStr."</table>";
	}


	function inbox($username){
		$a_accounts = ConnectionHub::getConn('poslavu')->getDBAPI()->getAllInTable('accounts', array('username'=>$username), TRUE);
		if (count($a_accounts) == 0)
			$a_accounts = array(array('id'=>0));
		$query=mlavu_query("select * from `poslavu_MAIN_db`.`notifications` where `receiver`='[userid]' ORDER BY `timestamp` desc ", array('userid'=>$a_accounts[0]['id']));
		getNotifications($username, $query, FALSE);
	}
	function outbox($username){
		$a_accounts = ConnectionHub::getConn('poslavu')->getDBAPI()->getAllInTable('accounts', array('username'=>$username), TRUE);
		if (count($a_accounts) == 0)
			$a_accounts = array(array('id'=>0));
		$query=mlavu_query("select * from `poslavu_MAIN_db`.`notifications` where `sender`='[userid]' ORDER BY `timestamp` desc", array('userid'=>$a_accounts[0]['id']));
		getNotifications($username, $query, TRUE);
	}

	// todo: update for notifications instead of chats
	function compose($username){
		$retStr="
			<select id='group' onChange='$(\"#user\").css(\"display\",\"inline-block\");'>
				<option value='lavu'>Lavu</option>
				<option value='support'>Support</option>
				<option value='dev'>Dev</option>
				<option value='user' >User</option>
			</select>
			<select id='user' style='display:none; color:black;'>
				<option value=''></option>
			";
				$query=mlavu_query("SELECT `username`, `email` FROM `poslavu_MAIN_db`.`accounts` WHERE `type` !='terminated' AND `type` !='language' AND `type` != 'other' ORDER BY `username` asc ");
				while($read=mysqli_fetch_assoc($query))
					$retStr.= "<option value='".$read['username']."'>".$read['username']."</option>";


			$retStr.="
			</select>
			<br>
			<textarea id='message' cols='50' rows='10'></textarea>
			<br>
			<input type='submit' class='submitButton' value='submit' onclick='createMessage($(\"#group\").val(), $(\"#user\").val(),$(\"#message\").val() )'>
		";


		echo $retStr;
	}

?>
