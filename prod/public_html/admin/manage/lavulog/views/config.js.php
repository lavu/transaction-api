/**
 * @license Copyright (c) 2003-2013, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.html or http://ckeditor.com/license
 */

CKEDITOR.editorConfig = function( config ) {
    // Define changes to default configuration here. For example:
    // config.language = 'fr';
    // config.uiColor = '#AADC6E';
    // this is a test, please do not panic !
    config.filebrowserBrowseUrl =      '<?php echo option('base_path'); ?>/kcfinder/browse.php?type=files';
    config.filebrowserImageBrowseUrl = '<?php echo option('base_path'); ?>/kcfinder/browse.php?type=images';
    config.filebrowserFlashBrowseUrl = '<?php echo option('base_path'); ?>/kcfinder/browse.php?type=flash';
    config.filebrowserUploadUrl =      '<?php echo option('base_path'); ?>/kcfinder/upload.php?type=files';
    config.filebrowserImageUploadUrl = '<?php echo option('base_path'); ?>/kcfinder/upload.php?type=images';
    config.filebrowserFlashUploadUrl = '<?php echo option('base_path'); ?>/kcfinder/upload.php?type=flash';
    config.width = 750;
    config.toolbar = [
        ["Source", "-", "New Page"], 
        ["Cut", "Copy", "Paste", "PasteText", "PasteFromWord", "-", "Undo", "Redo"],
        ["Bold", "Italic", "Underline", "RemoveFormat"],
        ["NumberedList", "BulletedList"],
        ["Link", "Unlink", "Styles", "Image", "Font", "TextColor"]
    ];
};
// 
// config.toolbar = 'Full';
//  
// config.toolbar_Full =
// [
    // { name: 'document', items : [ 'Source','-','Save','NewPage','DocProps','Preview','Print','-','Templates' ] },
    // { name: 'clipboard', items : [ 'Cut','Copy','Paste','PasteText','PasteFromWord','-','Undo','Redo' ] },
    // { name: 'editing', items : [ 'Find','Replace','-','SelectAll','-','SpellChecker', 'Scayt' ] },
    // { name: 'forms', items : [ 'Form', 'Checkbox', 'Radio', 'TextField', 'Textarea', 'Select', 'Button', 'ImageButton', 
        // 'HiddenField' ] },
    // '/',
    // { name: 'basicstyles', items : [ 'Bold','Italic','Underline','Strike','Subscript','Superscript','-','RemoveFormat' ] },
    // { name: 'paragraph', items : [ 'NumberedList','BulletedList','-','Outdent','Indent','-','Blockquote','CreateDiv',
    // '-','JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock','-','BidiLtr','BidiRtl' ] },
    // { name: 'links', items : [ 'Link','Unlink','Anchor' ] },
    // { name: 'insert', items : [ 'Image','Flash','Table','HorizontalRule','Smiley','SpecialChar','PageBreak','Iframe' ] },
    // '/',
    // { name: 'styles', items : [ 'Styles','Format','Font','FontSize' ] },
    // { name: 'colors', items : [ 'TextColor','BGColor' ] },
    // { name: 'tools', items : [ 'Maximize', 'ShowBlocks','-','About' ] }
// ];
//  
// config.toolbar_Basic =
// [
    // ['Bold', 'Italic', '-', 'NumberedList', 'BulletedList', '-', 'Link', 'Unlink','-','About']
// ];