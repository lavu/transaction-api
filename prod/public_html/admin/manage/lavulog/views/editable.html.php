<!doctype html>
<!-- paulirish.com/2008/conditional-stylesheets-vs-css-hacks-answer-neither/ -->
<!--[if lt IE 7]> <html class="no-js ie6 oldie" lang="en"> <![endif]-->
<!--[if IE 7]>    <html class="no-js ie7 oldie" lang="en"> <![endif]-->
<!--[if IE 8]>    <html class="no-js ie8 oldie" lang="en"> <![endif]-->
<!-- Consider adding an manifest.appcache: h5bp.com/d/Offline -->
<!--[if gt IE 8]><!--> <html class="no-js" lang="en"> <!--<![endif]-->
<head>
  <meta charset="utf-8">

  <!-- Use the .htaccess and remove these lines to avoid edge case issues.
       More info: h5bp.com/b/378 -->
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

  <title>Lavu IPV</title>
  <meta name="description" content="">
  <meta name="author" content="">

  <!-- Mobile viewport optimized: j.mp/bplateviewport -->
  <meta name="viewport" content="width=device-width,initial-scale=1">

  <!-- Place favicon.ico and apple-touch-icon.png in the root directory: mathiasbynens.be/notes/touch-icons -->

  <!-- CSS: implied media=all -->
  <!-- CSS concatenated and minified via ant build script-->
  
  <link rel="stylesheet" href="<?php echo option('base_path') . "/css/style.css"; ?>">
  <!-- end CSS-->

  <!-- More ideas for your <head> here: h5bp.com/d/head-Tips -->
      <link rel="shortcut icon" href="favicon.ico" />

  <!-- All JavaScript at the bottom, except for Modernizr / Respond.
       Modernizr enables HTML5 elements & feature detects; Respond is a polyfill for min/max-width CSS3 Media Queries
       For optimal performance, use a custom Modernizr build: www.modernizr.com/download/ -->
  <script src="<?php echo option('base_path'); ?>/js/libs/modernizr-2.0.6.min.js"></script>
</head>

<body>

  <div id="container">
      <form action="<?php echo option('base_path') . "/lib/gateway.php"; ?>" method="post" accept-charset="utf-8">
        <input type="hidden" name="action" value="edit-entry" />
        <input type="hidden" name="old-id" value="<?php echo $old_id; ?>" />
    <header>
        <div id="control-panel">
            <input class="edit-submit" type="submit" value="Submit Changes" />
            <a class="back-button" href="<?php echo option('base_path') . '/features/' . $old_id; ?>">Cancel changes</a>
            <a class="home-button" href="<?php echo option('base_path') . '/' ?>">Go back to main</a>
        </div>
        
        
    </header>
    <div id="modal-bg"><h2 id="panel-label">default</h2></div>
    
    <div id="entry-container" role="main">
        
<h1><label for="name">Name </label><input name="name" type="text" value="<?php echo $name; ?>" /></h1>
<h2><label for="domain">Domain </label> <?php echo $domain; ?> </h2>
<h2>Assigned to : <?php echo $assigned_to; ?></h2>
<h3>Current Status : <?php echo $status; ?></h3>
<h3>Goal to accomplish : </h3>
<textarea id="goal-viewer" name="goal-viewer">
        <?php echo html_entity_decode($goal); ?>
</textarea>
<h3>Specifications : </h3>
<textarea id="specs-viewer" name="specs-viewer">
        <?php echo html_entity_decode($specs); ?>
</textarea>

    </div>
    <footer>
    </footer>
      </form>
  </div> <!--! end of #container -->


  <!-- JavaScript at the bottom for fast page loading -->

  <!-- Grab Google CDN's jQuery, with a protocol relative URL; fall back to local if offline -->
  <script src="//ajax.googleapis.com/ajax/libs/jquery/1.6.2/jquery.min.js"></script>
  <script>window.jQuery || document.write('<script src="../js/libs/jquery-1.6.2.min.js"><\/script>')</script>
  
<script src="<?php echo option("base_path"); ?>/js/libs/ckeditor/ckeditor.js" type="text/javascript" charset="utf-8"></script>
  <!-- scripts concatenated and minified via ant build script-->
<script type="text/javascript" charset="utf-8">

    CKEDITOR.replace('goal-viewer');
    CKEDITOR.replace('specs-viewer');
</script>
  <!-- end scripts-->


  <!-- Prompt IE 6 users to install Chrome Frame. Remove this if you want to support IE 6.
       chromium.org/developers/how-tos/chrome-frame-getting-started -->
  <!--[if lt IE 7 ]>
    <script src="//ajax.googleapis.com/ajax/libs/chrome-frame/1.0.3/CFInstall.min.js"></script>
    <script>window.attachEvent('onload',function(){CFInstall.check({mode:'overlay'})})</script>
  <![endif]-->
  
</body>
</html>
