<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN"
	"http://www.w3.org/TR/html4/strict.dtd">

<!doctype html>

<!-- paulirish.com/2008/conditional-stylesheets-vs-css-hacks-answer-neither/ -->
<!--[if lt IE 7]> <html class="no-js ie6 oldie" lang="en"> <![endif]-->
<!--[if IE 7]>    <html class="no-js ie7 oldie" lang="en"> <![endif]-->
<!--[if IE 8]>    <html class="no-js ie8 oldie" lang="en"> <![endif]-->
<!-- Consider adding an manifest.appcache: h5bp.com/d/Offline -->
<!--[if gt IE 8]><!--> <html class="no-js" lang="en"> <!--<![endif]-->
<head>
  <meta charset="utf-8">

  <!-- Use the .htaccess and remove these lines to avoid edge case issues.
       More info: h5bp.com/b/378 -->
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

  <title>Lavu IPV</title>
  <meta name="description" content="">
  <meta name="author" content="">

  <!-- Mobile viewport optimized: j.mp/bplateviewport -->
  <meta name="viewport" content="width=device-width,initial-scale=1">

  <!-- Place favicon.ico and apple-touch-icon.png in the root directory: mathiasbynens.be/notes/touch-icons -->

  <!-- CSS: implied media=all -->
  <!-- CSS concatenated and minified via ant build script-->
  
  <link rel="stylesheet" href="css/style.css">
  <!-- end CSS-->

  <!-- More ideas for your <head> here: h5bp.com/d/head-Tips -->
      <link rel="shortcut icon" href="favicon.ico" />

  <!-- All JavaScript at the bottom, except for Modernizr / Respond.
       Modernizr enables HTML5 elements & feature detects; Respond is a polyfill for min/max-width CSS3 Media Queries
       For optimal performance, use a custom Modernizr build: www.modernizr.com/download/ -->
  <script src="js/libs/modernizr-2.0.6.min.js"></script>
</head>

<body>

  <div id="container">
    <header>
        <div id="control-panel">
            <button id="make-entry">Make a new Entry</button>
        </div>
        
        
    </header>
    <div id="modal-bg"></div>
    
    <table id="features-table" role="main">
        <tr>
            <td>
                <?php echo $indesign; ?>
                <?php echo $dev; ?>
                <?php echo $ready; ?>
            </td>
            <td>
                <?php echo $inreview; ?>
                <?php echo $testing; ?>
                <?php echo $completed; ?>
            </td>
        </tr>
    </table>
    <footer>
<form action="lib/gateway.php" method="post" accept-charset="utf-8" id="make-entry-form">
            <p><label for="name">Name of the feature : </label>
                <input type="text" name="name" class="required" /></p>
                
            <p><label for="domain">What is the domain of this feature</label>
                <select name="domain">
                    <option value="ios">Front End (iOS)</option>
                    <option value="backend">Backend</option>
                    <option value="web">Web</option>
                    <option value="internal">Internal Tools</option>
                    <option value="other">Other</option>
                </select></p>
            <p>What is it intented to solve?</p>
            
            <textarea name="goal"></textarea>
            
            <p><label for="assigned-to">Assigned To : </label>
                <select name="assigned-to" class="required">
                    <option>----</option>
                    <option value="Theo">Theo</option>
                    <option value="Nialls">Nialls</option>
                    <option value="Brian">Brian</option>
                    <option value="Ben B">Ben B</option>
                    <option value="Carlos">Carlos</option>
                </select>
                </p>
                
            <p><label for="status">Current Status : </label>
                <select name="status" class="required">
                    <option>----</option>
                    <option value="indesign">In design</option>
                    <option value="inreview">In review</option>
                    <option value="dev">In development</option>
                    <option value="testing">Testing</option>
                    <option value="ready">Ready to launch</option>
                    <option value="completed">Completed</option>
                </select></p>
            
            <p>Specifications</p>
            <textarea name="specifications" id="" cols="30" rows="10"></textarea>
             <input type="hidden" name="action" value="new-entry" />
            
          <p><input type="submit" value="Make it rain! &rarr;" /></p>
          
        </form>
    </footer>
  </div> <!--! end of #container -->


  <!-- JavaScript at the bottom for fast page loading -->

  <!-- Grab Google CDN's jQuery, with a protocol relative URL; fall back to local if offline -->
  <script src="//ajax.googleapis.com/ajax/libs/jquery/1.6.2/jquery.min.js"></script>
  <script>window.jQuery || document.write('<script src="js/libs/jquery-1.6.2.min.js"><\/script>')</script>
  <script src="js/libs/validate.js" type="text/javascript" charset="utf-8"></script>
  <script src="js/libs/ckeditor/ckeditor.js" type="text/javascript" charset="utf-8"></script>


  <!-- scripts concatenated and minified via ant build script-->
  <script defer src="js/plugins.js"></script>
  <script defer src="js/script.js"></script>
  <!-- end scripts-->


  <!-- Prompt IE 6 users to install Chrome Frame. Remove this if you want to support IE 6.
       chromium.org/developers/how-tos/chrome-frame-getting-started -->
  <!--[if lt IE 7 ]>
    <script src="//ajax.googleapis.com/ajax/libs/chrome-frame/1.0.3/CFInstall.min.js"></script>
    <script>window.attachEvent('onload',function(){CFInstall.check({mode:'overlay'})})</script>
  <![endif]-->
  
</body>
</html>
