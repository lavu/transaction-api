/* Author: 
 * Alvie @
 * __/\\\__________________/\\\\\\\\\______/\\\________/\\\___/\\\________/\\\_        
 *   _\/\\\________________/\\\\\\\\\\\\\___\/\\\_______\/\\\__\/\\\_______\/\\\_       
 *    _\/\\\_______________/\\\/////////\\\__\//\\\______/\\\___\/\\\_______\/\\\_      
 *     _\/\\\______________\/\\\_______\/\\\___\//\\\____/\\\____\/\\\_______\/\\\_     
 *      _\/\\\______________\/\\\\\\\\\\\\\\\____\//\\\__/\\\_____\/\\\_______\/\\\_    
 *       _\/\\\______________\/\\\/////////\\\_____\//\\\/\\\______\/\\\_______\/\\\_   
 *        _\/\\\______________\/\\\_______\/\\\______\//\\\\\_______\//\\\______/\\\__  
 *         _\/\\\\\\\\\\\\\\\__\/\\\_______\/\\\_______\//\\\_________\///\\\\\\\\\/___ 
 *          _\///////////////___\///________\///_________\///____________\/////////_____
*/

$.fn.serializeObject = function()
{
    var o = {};
    var a = this.serializeArray();
    $.each(a, function() {
        if (o[this.name] !== undefined) {
            if (!o[this.name].push) {
                o[this.name] = [o[this.name]];
            }
            o[this.name].push(this.value || '');
        } else {
            o[this.name] = this.value || '';
        }
    });
    return o;
};



$( function () {
    $( "#make-entry-form" ).validate();
    $( "#make-entry" ).click( function () {
        $( "#modal-bg" ).show();
        $( "#make-entry-form" ).show();
    } );
    $( "#modal-bg" ).click( function () {
        $( "#make-entry-form" ).add( "#modal-bg" ).hide();
    } );
    // Code for the edit functionality
    $( ".entry-odd" ).add( ".entry-even" ).click( function () {
        
    } );
    CKEDITOR.replace( 'goal' );
    CKEDITOR.replace( 'specifications' );
} );   
