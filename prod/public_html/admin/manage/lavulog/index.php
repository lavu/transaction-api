<?php

//ini_set("memory_limit","64M");

require_once "lib/limonade.php";
require 'lib/backend.php';
require 'lib/formatter.php';

option( 'base_uri', option( 'base_path' ) );

dispatch( '/', "main_page" );

dispatch( '/features/*', 'feature_viewer' );

dispatch( '/features/*/edit', 'edit_feature' );

dispatch( '/js/libs/ckeditor/config.js', 'ckeditor_config_file' );

// dispatch( '/kceditor/upload/images/*', 'send_image' );
// 
// dispatch( '/kcfinder/upload/images/*', 'send_image' );


function main_page () {
    $db = new Backend();
   // $indesign = $db->get_features_by( "status", "indesign" );
   // $inreview = $db->get_features_by( "status", "inreview" );
   // $dev      = $db->get_features_by( "status", "dev" );
   // $testing  = $db->get_features_by( "status", "testing" );
   // $ready    = $db->get_features_by( "status", "ready" );
   // $completed= $db->get_features_by( "status", "completed" );
    $entries = array( 
        "indesign" => format_entries_block( $db->get_features_by( "status", "indesign" ), "entry-odd", "In Design", "header-indesign" ),
        "inreview" => format_entries_block( $db->get_features_by( "status", "inreview" ), "entry-even", "In Review", "header-inreview" ),
        "dev"      => format_entries_block( $db->get_features_by( "status", "dev" ), "entry-odd", "In Development", "header-dev" ), 
        "testing"  => format_entries_block( $db->get_features_by( "status", "testing" ), "entry-even", "Testing", "header-testing" ),
        "ready"    => format_entries_block( $db->get_features_by( "status", "completed" ), "entry-odd", "Ready", "header-ready" ),
        "completed"=> format_entries_block( $db->get_features_by( "status", "completed" ), "entry-even", "Completed", "header-completed" )    
    );
    return render( "index.html.php", null, $entries );
}

function feature_viewer () {
    option( 'base_uri', option( 'base_path' ) );
    $db = new Backend();
    $entry = $db->get_entry_by_id( params( 0 ) );
    if ( empty( $entry ) ) {
        return html( "404.html.php" );
    }
    return render( "features.html.php", null, $entry[0] );
}

function edit_feature() {
    option( 'base_uri', option( 'base_path' ) );
    $db = new Backend();
    $entry = $db->get_entry_by_id( params( 0 ) );
    if ( empty( $entry ) ) {
        return html( "404.html.php" );
    }
    $entry = $entry[0];
    $entry["assigned_to"] = format_assigment_list( $entry["assigned_to"] );
    $entry["status"] = format_status_list( $entry["status"] );
    $entry["old_id"] = $entry["domain"] . $entry["id"];
    $entry["domain"] = format_domain_list( $entry["domain"] );
    return render( "editable.html.php", null, $entry );
}


function send_image () {
    return render_file( 'kcfinder/upload/images/' . params(0) );
}

function ckeditor_config_file () {
    return js( 'config.js.php' );
}

run();



?>