<?php 

function format_entries_block ( $entries, $class, $header=null, $header_class=null ) {
    $str_buf = "<div class=\"$class\">";
    $str_buf .= ( is_null( $header ) )? "" : "<h2 class=\"$header_class\">$header</h2>";
    $str_buf .= "<ul>";
    foreach( $entries as $index => $entry ) {
        $goal = substr( strip_tags( $entry["goal"] ), 0, 50) . "...";
        $str_buf .= "<li class=\"{$entry["domain"]}\">";
        $str_buf .= "<a href=\"features/{$entry["domain"]}{$entry["id"]}\" class=\"link-feature\">";
        $str_buf .= "{$entry["name"]} ({$entry["domain"]})</a>";
        $str_buf .= "<p class=\"timestamp\">Last Modified: {$entry["last-modified"]}</p>";
        $str_buf .= "<p class=\"short-goal\">$goal</p></li>";
    }
    return $str_buf . "</ul></div>";
}

function format_assigment_list ( $selected ) {
    $str_buf = '<select name="assigned-to" class="required">';
    $arr = array( "Theo", "Nialls", "Brian", "Ben B", "Carlos" );
    foreach( $arr as $item ) {
        if ( $item == $selected ) {
            $str_buf .= "<option value=\"$item\" selected=\"selected\">$item</option>";
        } else {
            $str_buf .= "<option value=\"$item\">$item</option>";
        }
    }
    return $str_buf . '</select>';
}

function format_status_list( $selected ) {
    $arr = array( 
        array( "value" => "indesign", "text" => "In Design" ),
        array( "value" => "inreview", "text" => "In Review"),
        array( "value" => "dev",      "text" => "In Development" ),
        array( "value" => "testing",  "text" => "Testing" ),
        array( "value" => "ready",    "text" => "Ready To Launch" ),
        array( "value" => "completed","text" => "Completed" )    );
   $str_buf = '<select name="status" class="required">';
   foreach( $arr as $item ) {
       if ( $selected == $item["value"] ) {
           $str_buf .= "<option value=\"{$item["value"]}\" selected=\"selected\">{$item["text"]}</option>";
       } else {
           $str_buf .= "<option value=\"{$item["value"]}\">{$item["text"]}</option>";
       }
   }
   return $str_buf . '</select>';
}

function format_domain_list( $selected ) {
    $arr = array(
        array( "value" => "ios", "text" => "Front End (iOS)" ),
        array( "value" => "backend", "text" => "Backend" ),
        array( "value" => "web" , "text" => "Web"),
        array( "value" => "internal", "text" => "Internal Tools" ),
        array( "value" => "other", "text" => "Other" )
    );
    $str_buf = '<select name="domain">';
    foreach( $arr as $item ) {
        $str_buf .= "<option value=\"{$item["value"]}\"";
        $str_buf .= ( $selected == $item["value"] )? ' selected="selected">' : '>';
        $str_buf .= $item["text"] . "</option>";
    }
    return $str_buf . '</select>';
}
?>