<?php 
/**
 * __/\\\__________________/\\\\\\\\\______/\\\________/\\\___/\\\________/\\\_        
 *   _\/\\\________________/\\\\\\\\\\\\\___\/\\\_______\/\\\__\/\\\_______\/\\\_       
 *    _\/\\\_______________/\\\/////////\\\__\//\\\______/\\\___\/\\\_______\/\\\_      
 *     _\/\\\______________\/\\\_______\/\\\___\//\\\____/\\\____\/\\\_______\/\\\_     
 *      _\/\\\______________\/\\\\\\\\\\\\\\\____\//\\\__/\\\_____\/\\\_______\/\\\_    
 *       _\/\\\______________\/\\\/////////\\\_____\//\\\/\\\______\/\\\_______\/\\\_   
 *        _\/\\\______________\/\\\_______\/\\\______\//\\\\\_______\//\\\______/\\\__  
 *         _\/\\\\\\\\\\\\\\\__\/\\\_______\/\\\_______\//\\\_________\///\\\\\\\\\/___ 
 *          _\///////////////___\///________\///_________\///____________\/////////_____
 */

/***
 *
 * -- table query : 
 * CREATE TABLE `lavulog` (
 *    `name` VARCHAR(200) NOT NULL,
 *    `domain` ENUM( 'ios', 'web', 'backend', 'internal', 'other') NOT NULL,
 *    `assigned-to` VARCHAR(50) NOT NULL,
 *    `status` ENUM( 'indesign', 'inreview', 'dev', 'testing', 'ready', 'completed' ) NOT NULL,
 *    `specifications` MEDIUMTEXT,
 *    `goal` MEDIUMTEXT, 
 *    `id` INT AUTO_INCREMENT NOT NULL,
 *    `last-modified` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
 *    PRIMARY KEY (`domain`, `id`) 
 *  ) ENGINE=MyISAM;
 * 
 *
 */

/**
 * General use functions, used frequently in this class but that are not necessarily class methods
 */
 
 
/**
 * Actual class declaration
 */
 
require_once 'constants.php';
 
class Backend {
    
    private static $db_conn;
    
    function __construct () {
        if ( !self::$db_conn ) {
            self::$db_conn =  new mysqli( DB_SERVER, DB_USER, DB_PASSWORD, DB_NAME ) 
                or die ("Could not establish a connection to the database");
        }
    }
    
/**
 * #get_associative_array
 * Function to retrieve an associative array from a
 * prepared statement
 * 
 */
    function get_associative_array ( $mysqli_stmt ) {
        $mysqli_stmt->store_result();
        $meta = $mysqli_stmt->result_metadata();
        while ( $field = $meta->fetch_field() ) {
            $params[] = &$row[ $field->name ];
        }
        call_user_func_array( array( $mysqli_stmt, 'bind_result' )  , $params );
        $entries = array(); 
        while ( $mysqli_stmt->fetch() ) {
            foreach ( $row as $key => $value ) {
                $r[$key] = $value;
            }
            $entries[] = $r;
        }
        return $entries;
    }
    
/**
 * #get_error
 * shortcut function that returns the error code and message of the last mysqli query.
 * If the query was successful, in theory this should return "::", although this function
 * should not be called when the query is expected to be successful
 * @return String
 * "{error number}::{error message}"
 */
 
    function get_error ( ) {
        $errno  = mysqli_errno( self::$db_conn );
        $error  = mysqli_error( self::$db_conn );
        return array( "success" => false, "error_msg" => "$errno::$error" );
    }
    
/**
 * #make new entry
 * insert a new feature in the lavulog DB
 * @return 
 * boolean::true if all went well
 * Array( success::false, string::error ) if something went wrong
 */
    function make_new_entry ( $name, $domain, $assigned_to, $status, $specs, $goal ) {
        $query = "insert into `lavulog` ";
        $query .= "(`name`, `domain`, `assigned-to`, `status`, `specifications`, `goal`) values ";
        $query .= "(?,?,?,?,?,?)";
        $stmt = self::$db_conn->prepare( $query );
        $input = array( "ssssss", &$name, &$domain, &$assigned_to, &$status, &$specs, &$goal );
        $fun = array ( $stmt, 'bind_param' );
        if ( $stmt && call_user_func_array( $fun  , $input ) && $stmt->execute() ) {
            $stmt->close();
            return true;
        }
        return $this->get_error();   
    }
    
    
    /**
     * #get_all_features
     * Retrieves all the feature entries, regardless of domain
     * @return Array
     * if it has a property called "success" and it is false, it means
     * something went wrong, else it returns the array with all the entries
     */
    
    function get_all_features () {
        $query  = " select `name`, `domain`, `last-modified`, `assigned-to`, "; 
        $query .= "`status`, `specifications`, `goal` from `lavulog`";
        $stmt = self::$db_conn->prepare( $query );
        $entries = array();
        if ( $stmt && $stmt->execute() ) {
            $entries = $this->get_associative_array( $stmt );
            $stmt->close();
            $stmt->free_result();
            return $entries;
        }
        return $this->get_error();
    }
    
    /**
     * #get_features_by
     * Function to selectively retrieve entries
     */
    function get_features_by( $criteria, $parameter ) {
       $query  = "SELECT `name`, `id`, `domain`, `last-modified`, `assigned-to`, ";
       $query .= "`status`, `specifications`, `goal` FROM `lavulog` WHERE ";
       $query .= sprintf( "%s=?", $criteria ) ;
       $stmt = self::$db_conn->prepare( $query );
       $func = array( $stmt, 'bind_param' );
       $params = array( "s", &$parameter );
       if ( $stmt && call_user_func_array( $func, $params) && $stmt->execute() ) {
           $entries = $this->get_associative_array( $stmt );
           $stmt->close();
           $stmt->free_result();
           return $entries;
       }
       return $this->get_error();
    }
    
    /**
     * #get_entry_by_id
     * This function gets only one entry from `lavulog`.
     * Since the primary key is a combination of the domain and the id
     * the argument for this function is not only the id, but the domain
     * too in a single string in the format of:
     * {domain}{id} (no spaces) 
     * Ex:
     * ios5
     * web10
     * backend2
     * @return Array
     * The array containing the feature entry data.
     */
     
     function get_entry_by_id ( $domain_id_string ) {
         preg_match( "/^[a-z]+/", $domain_id_string, $match );
         $domain = $match[0];
         preg_match( "/\d+$/", $domain_id_string, $match );
         $id = $match[0];
         $query  = "SELECT `name`, `id`, `domain`, `last-modified` as 'last_modified', `assigned-to` as 'assigned_to', ";
         $query .= "`status`, `specifications` as 'specs', `goal` FROM `lavulog` WHERE ";
         $query .= "`domain` like ? and `id`=?";
         $stmt = self::$db_conn->prepare( $query );
         if ( $stmt && $stmt->bind_param( 'si', $domain, $id ) && $stmt->execute() ) {
             $entries = $this->get_associative_array( $stmt );
             $stmt->close();
             return $entries;
         } 
         return $this->get_error();
     }
     
     function get_last_id () {
         $stmt = self::$db_conn->prepare( "SELECT LAST_INSERT_ID()" );
         if ( !$stmt ) return $this->get_error();
         $b = $stmt->execute();
         if ( !b ) return $this->get_error();
         $b = $stmt->bind_result( $id );
         if ( !b ) return $this->get_error();
         $b = $stmt->fetch();
         if ( !b ) return $this->get_error();
         $b = $stmt->close();
         if ( !b ) return $this->get_error();
         return $id;
     }
     
     function update_entry ( $name, $domain, $assigned_to, $status, $specs, $goal, $old_id ) {
         preg_match( "/^[a-z]+/", $old_id, $match );
         $old_domain = $match[0];
         preg_match( "/\d+$/", $old_id, $match );
         $old_id_num = $match[0];
         if ( $domain === $old_domain ) {
             $stmt = self::$db_conn->prepare( "UPDATE `lavulog` SET `name`=? , `assigned-to`=? , `status`=? , `specifications`=? , `goal`=? WHERE `id`=? AND `domain` LIKE ?" );
             if ( $stmt ) {
                 if ( $stmt->bind_param( "sssssis", $name, $assigned_to, $status, $specs, $goal, $old_id_num, $old_domain ) ) {
                     if ( $stmt->execute() ) {
                         $stmt->close();
                         return array( "success" => true, "new_id" => $old_id_num );
                     }
                 }
             }
         } else {
             $stmt = self::$db_conn->prepare("DELETE FROM `lavulog` WHERE `domain` LIKE ? AND `id`=?" );
             if ( $stmt && $stmt->bind_param( "si", $old_domain, $old_id_num ) && $stmt->execute() && $stmt->close() ) {
                 if ( $this->make_new_entry( $name, $domain, $assigned_to, $status, $specs, $goal ) === true ) {
                     return array(  "success" => true, "new_id" => $this->get_last_id() );
                 }
             }
         }
         return $this->get_error();
     }
}
 
?>