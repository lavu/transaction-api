<?php

require_once 'backend.php';
require_once 'limonade.php';

$db = null;

if ( isset( $_POST ) &&  ( $_POST['action'] ) ) {
    $action = $_POST['action'];
    $db = new Backend();
    $fn = str_replace( '-', '_', $action );
    if ( function_exists( $fn ) ){
        $fn();
    }
}

function new_entry () {
    $args = array(
        $_POST['name'], 
        $_POST['domain'], 
        $_POST['assigned-to'], 
        $_POST['status'], 
        $_POST['specifications'], 
        $_POST['goal'] 
    );
    global $db;
    $fn = array( $db, 'make_new_entry' );
    $response = call_user_func_array( $fn, $args);
    var_export($response);
    if ( $response === true ) {
        header("Location: ../");
    } else {
        echo "<br />Something went wrong!";
    }
}

function edit_entry() {
    $args = array(
        $_POST['name'],
        $_POST['domain'],
        $_POST['assigned-to'],
        $_POST['status'],
        $_POST['specs-viewer'],
        $_POST['goal-viewer'],
        $_POST['old-id']
    );
    global $db;
    $fn = array( $db, 'update_entry' );
    $response = call_user_func_array( $fn, $args );
    if ( $response["success"] ) {
        header( "Location: " . '../features/' . $_POST['domain'] . $response["new_id"] );
    } else {
        echo "<br />Something went wrong!</br>"  . var_export($response ,true) ;
    }
}

?>