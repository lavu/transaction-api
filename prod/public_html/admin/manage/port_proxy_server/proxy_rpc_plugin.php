<?php
	require_once('/home/poslavu/public_html/admin/lib/jcvs/proxy_ip_table.php');
	$proxy_table = getProxyIPTable();
	$internal = $proxy_table['tunnels_proxy_internal_ip'];
	$external = $proxy_table['tunnels_proxy_external_ip'];
	$is_internal_ip = $_SERVER['REMOTE_ADDR'] == $proxy_table['tunnels_proxy_external_ip'];
	$is_internal_ip_display = $is_internal_ip ? 'INTERNAL' : 'EXTERNAL';
	$proxy_server_access_ip = $_SERVER['REMOTE_ADDR'] == $external ? $internal : $external;
	$remoteIP = $_SERVER['REMOTE_ADDR'];
	$port = $is_internal_ip ? '' : '22080';
	$port = '';
	$htmlString = <<<HTMLSTRINGHEREDOC
		<p>Proxy Server External IP: $external<br>
		   Proxy Server Internal IP: $internal<br>
		   Currently connected to Manage on IP: $remoteIP<br>
		   Access to Proxy is: $is_internal_ip_display<br></p>
		
		<iframe src="https://proxytunnels.poslavu.com:$port/proxy_rpc.php" width='625' height='250' id='proxy_rpc_iframe'></iframe>
		<!--
		<iframe src="http://$proxy_server_access_ip:$port/proxy_rpc.php" width='625' height='250' id='proxy_rpc_iframe'></iframe>
		-->
		<script type='text/javascript'>
		    window.clearTimeout(blah);
			var blah = window.setInterval(function(){var my_proxy_rpc_iframe = document.getElementById('proxy_rpc_iframe'); my_proxy_rpc_iframe.src = my_proxy_rpc_iframe.src; }, 6000);
		</script>
HTMLSTRINGHEREDOC;

	echo $htmlString;
?>