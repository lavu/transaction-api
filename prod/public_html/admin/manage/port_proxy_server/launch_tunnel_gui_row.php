<?php 
	require_once("/home/poslavu/public_html/admin/lib/jcvs/proxy_ip_table.php");
	$proxyIPTable = getProxyIPTable();
	$externalProxyIP = $proxyIPTable['tunnels_proxy_external_ip'];
	$internalProxyIP = $proxyIPTable['tunnels_proxy_internal_ip'];
    $proxyAccessIP   = $_SERVER['REMOTE_ADDR'] == $externalProxyIP ? $internalProxyIP : $externalProxyIP;
    /*
    echo "
    <script type='text/javascript'>
        function launchTunnelPressed(){
            var int_ext_select = document.getElementById('ip_type_select_id');
            var int_ext = int_ext_select.options[int_ext_select.selectedIndex].value;
            var port_select = document.getElementById('port_select_id');
            var port = port_select.options[port_select.selectedIndex].value;
            var tunnel_select = document.getElementById('tunnel_select_4ssh_id');
            
            var tunnel = tunnel_select.options[tunnel_select.selectedIndex].value;
            var ssh_port = int_ext == 'internal' ? '22' : '22020';
            var proxy_ip = int_ext=='internal' ? '$internalProxyIP' : '$externalProxyIP';
            var tunnel_port = '2202'+(''+tunnel);
            alert('Launching ['+int_ext+'] lls tunnel, connecting on port ['+ssh_port+'] to ip ['+proxy_ip+'] connecting tunnel ['+tunnel+'] and forwarding port ['+port+'] to proxy server.  \\n\\nProxy endpoint: [$proxyAccessIP:'+tunnel_port+']');
            
            var bash_command = '/poslavu-local/local/rssh.expect '+ssh_port+' '+tunnel_port+' '+port+' ';
            bash_command += 'proxytunnels '+proxy_ip+' '+'test39';
            //alert(bash_command);
            run_local_bash(\"$loc_id\",\"$dataname\", bash_command);
        }
    </script>
    ";
    */
    echo "
    <script type='text/javascript'>
        function launchTunnelPressed(){
            var int_ext_select = document.getElementById('ip_type_select_id');
            var int_ext = int_ext_select.options[int_ext_select.selectedIndex].value;
            var port_select = document.getElementById('port_select_id');
            var port = port_select.options[port_select.selectedIndex].value;
            var tunnel_select = document.getElementById('tunnel_select_4ssh_id');
            
            
            
            var tunnel = tunnel_select.options[tunnel_select.selectedIndex].value;
            var proxy_ip = tunnel_select.options[tunnel_select.selectedIndex].getAttribute('proxyIPAddress');
            var tunnel_port = tunnel_select.options[tunnel_select.selectedIndex].getAttribute('proxyPortNumber');
            var ssh_port = int_ext == 'internal' ? '22' : '22020';
            //var proxy_ip = int_ext=='internal' ? '$internalProxyIP' : '$externalProxyIP';
            //var tunnel_port = '2202'+(''+tunnel);
            
            
            
            alert('Launching ['+int_ext+'] lls tunnel, connecting on port ['+ssh_port+'] to ip ['+proxy_ip+'] connecting tunnel ['+tunnel+'] and forwarding port ['+port+'] to proxy server.  \\n\\nProxy endpoint: [$proxyAccessIP:'+tunnel_port+']');
            
            var bash_command = '/poslavu-local/local/rssh.expect '+ssh_port+' '+tunnel_port+' '+port+' ';
            bash_command += 'proxytunnels '+proxy_ip+' '+'test39';
            //alert(bash_command);
            run_local_bash(\"$loc_id\",\"$dataname\", bash_command);
        }
    </script>
    ";
    
    function getProxyTunnelsThatAreAllowed(){
	    $currAccountRow = getCurrentAccountsRow();
echo "account row: ".print_r($currAccountRow,1)."<br>";
	    $tunnelResult = mlavu_query("SELECT * FROM `poslavu_MAIN_db`.`tunnels` WHERE `proxy_tunnel_id` > '0'");
	    $rowList = array();
	    while($currTunnelRow = mysqli_fetch_assoc($tunnelResult)){
		    if(isTunnelRowAllowedForCustomer($currTunnelRow,$currAccountRow)){
			    $rowList[] = $currTunnelRow;
		    }
	    }
	    return $rowList;
    }

    function isTunnelRowAllowedForCustomer($tunnelRow, $currAccountRow){
    	static $accessWhiteListArr = null;
    	static $typeWhiteListArr = null;
    	if(!$accessWhiteListArr || !$typeWhiteListArr){
	    	$accessWhiteListArr = explode(",",$currAccountRow['access']);
	    	$typeWhiteListArr = explode(",",$currAccountRow['type']);//Probably only has 1 entry but whatever.
	    }
	    $tunnelRowsAllowedTypesArr  = explode(',',$tunnelRow['allowed_account_types']);
	    $tunnelRowsAllowedAccessArr = explode(',',$tunnelRow['allowed_access_codes']);
	    if( in_array("all",$tunnelRowsAllowedTypesArr) || in_array("all",$tunnelRowsAllowedAccessArr) ){
		    return true;
	    }
	    foreach($accessWhiteListArr as $currAccountsAccessExplodedItem){
		    if (in_array($currAccountsAccessExplodedItem, $tunnelRowsAllowedAccessArr)){
			    return true;
		    }
	    }
	    foreach($typeWhiteListArr as $currAccountsTypeExplodedItem){
		    if (in_array($currAccountsTypeExplodedItem, $tunnelRowsAllowedTypesArr)){
			    return true;
		    }
	    }
	    
	    return false;
    }
    
    function getCurrentAccountsRow(){
	    GLOBAL $loggedin;
	    $accountRowResult = mlavu_query("SELECT * FROM `poslavu_MAIN_db`.`accounts` WHERE `username`='[1]'",$loggedin);
	    if(!$accountRowResult){
		    error_log("Mysql error in ".__FILE__." -bdu- error:".mlavu_dberror());
		    return array();
	    }
	    return mysqli_fetch_assoc($accountRowResult);
    }
    
    function buildTunnelSelectOptions(){
	    $tunnelRows = getProxyTunnelsThatAreAllowed();
	    $strBuilder = '';
	    foreach($tunnelRows as $currTunnelRow){
		    $strBuilder .= "<option proxyIPAddress='"."216.243.114.158"."' proxyPortNumber='".$currTunnelRow['proxy_port']."' value='".$currTunnelRow['proxy_tunnel_id']."'>".$currTunnelRow['title'].'</option>';
	    }
	    return $strBuilder;
    }
?>

	<td>
	    Tunnel &nbsp;&nbsp;
		<select id='ip_type_select_id'>
		    <option value='external'>LLS-External</option>
		    <option value='internal'>LLS-Internal</option>
		</select>
	</td>
	<td>
		<select id='port_select_id'>
			<option value='22'>SSH 22</option>
			<option value='80'>HTTP 80</option>
			<option value='3306'>MySQL 3306</option>
			<option value='5900'>VNC 5900</option>
		</select>
	</td>
	<td>
		<select id='tunnel_select_4ssh_id'>
		<!--
			<option value='1'>Tunnel 1 (internal)</option>
			<option value='2'>Tunnel 2 (internal)</option>
			<option value='3'>Tunnel 3 (internal)</option>
			<option value='4'>Tunnel 4 (internal)</option>
			<option value='5'>Tunnel 5 (internal)</option>
			<option value='6'>Tunnel 6 (internal)</option>
			<option value='7'>Tunnel 7 (internal)</option>
			<option value='8'>Tunnel 8 (external)</option>
			<option value='9'>Tunnel 9 (external)</option>
		-->
		<?php
			echo buildTunnelSelectOptions();
		?>
		</select>
	</td>
	<td>
		<input type='button' value='Launch Tunnel' style='margin-left:20px' onclick="launchTunnelPressed()"/>
	</td>
