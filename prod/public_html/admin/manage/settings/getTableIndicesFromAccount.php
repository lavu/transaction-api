<?php

	require_once($_SERVER['DOCUMENT_ROOT']."/manage/error_config.php");
	require_once('/home/poslavu/public_html/admin/cp/resources/lavuquery.php');
	require_once('/home/poslavu/public_html/admin/cp/resources/json.php');
	if( !empty($_REQUEST['web_request']) ){
		if(empty($_REQUEST['dataname'])){echo 'forgot the dataname'; exit; }
		$tablesMappedToIndices = getAllTablesAndIndicesFromAccount($dataname);
		//We will want to return the JSON representation of it all in a wrapped json request.
		$response = array();
		$response['status'] = 'success';
		$response['index_list'] = $tablesMappedToIndices;
		$responseJSONStr = LavuJson::json_encode($response);
		echo $responseJSONStr;
	}

	function getAllTablesAndIndicesFromAccount($dataname){
		$tables = getAllTablesFromAccount($_REQUEST['dataname']);
		$tablesMappedToIndices = array();
		foreach($tables as $curr){
			$tablesMappedToIndices[$curr] = array();
		}
		//Mutative function loads all indices as an array per each table which is the dict. key.
		loadIndexesIntoTablesArray($tablesMappedToIndices, $_REQUEST['dataname']);
		return $tablesMappedToIndices;
	}

	function getAllTablesFromAccount($dataname){
		$db = "poslavu_".$dataname."_db";
		$result = mlavu_query("SHOW TABLES FROM `[1]`", $db);
		if(!result){ echo "ERROR IN SHOW TABLES QUERY -asekfje- mysql_error:".mlavu_dberror(); exit;}
		$tablesArr = array();
		while($curr = mysqli_fetch_assoc($result)){
			$tablesArr[] = $curr['Tables_in_'.$db];
		}
		return $tablesArr;
	}

	function getAllIndicesForTable($table, $database){
		$result = mlavu_query("SHOW INDEXES FROM `[1]`.`[2]`", $database, $table);
		if(!$result){ echo "ERROR IN SHOW INDICES"; exit; }
		$indexes = array();
		while($curr = mysqli_fetch_assoc($result)){
			$indexes[$curr['Column_name']] = $curr;
		}
		return $indexes;
	}

	function loadIndexesIntoTablesArray(&$tablesByKey, $dataname){
		$db = 'poslavu_'.$dataname.'_db';
		foreach($tablesByKey as $key => $val){
			$tablesByKey[$key] = getAllIndicesForTable($key,$db);
		}
	}

	//Essentially merges all indices between 
	function mergeTableIndiceLists($tablesArr_1, $tablesArr_2){
		$mergedIndicesList = array();
		foreach($tablesArr_1 as $table_name_key => $indexes_arr_val){
			$tablesIndiceArrFromTable2 = isset($tablesArr_2[$table_name_key]) ? $tablesArr_2[$table_name_key] : array();
			$mergedIndicesList[$table_name_key] = array_merge($indexes_arr_val, $tablesIndiceArrFromTable2);
		}
		foreach($tablesArr_2 as $table_name_key => $indexes_arr_val){
			if(empty($tablesArr_1[$table_name_key]))
				$mergedIndicesList[$table_name_key] = $indexes_arr_val;
		}
		return $mergedIndicesList;
	}


?>
