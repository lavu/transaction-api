<?php
	
	class ORDERS_X_DAY_1MONTH {
		
		// returns a string representing the graph
		function graph($s_dataname) {
			
			// check that the dataname is good
			if ($s_dataname == '')
				return 'Dataname is blank.';
			$restaurant_exists = ConnectionHub::getConn('poslavu')->getDBAPI()->getAllInTable('restaurants', array('data_name'=>$s_dataname), TRUE);
			if (count($restaurant_exists) == 0)
				return 'Restaurant "'.$s_dataname.'" doesn\'t exist or has been deleted (it can\'t be found in the restaurants table).';
			
			// get the orders for the last week
			$i_secs_today = strtotime('now')-strtotime('today');
			$i_today = strtotime('today');
			$i_last_month = strtotime('-1 month') - $i_secs_today;
			$s_last_month = date('Y-m-d H:i:s', $i_last_month);
			$a_orders = ConnectionHub::getConn('poslavu')->getDBAPI()->getAllInTable('orders', array('starttime'=>$s_last_month), TRUE,
				array('databasename'=>'poslavu_'.$s_dataname.'_db', 'whereclause'=>"WHERE `closed` >= '[starttime]'", 'selectclause'=>'`closed`'));
			
			// initialize the data array
			$a_closed_data = array();
			$i = 0;
			for ($j = $i_last_month; $j < $i_today; $j = strtotime(date('Y-m-d',$j).' +1 days')) {
				$a_closed_data[$i] = 0;
				$i++;
			}
			
			// translate to data
			foreach($a_orders as $a_order) {
				$i_num_days = $this->date_diff($i_last_month, strtotime( $a_order['closed'] ));
				$a_closed_data[$i_num_days]++;
			}
			$s_closed_data = '{ color: "#aecd37", points: { fillColor: "#aecd37", show: true }, label: "Orders Closed By Day", data: [';
			foreach($a_closed_data as $k=>$v) {
				$s_closed_data .= '[gd('.date('Y, n, j', strtotime($s_last_month.' +'.$k.' days')).'),'.$v.'], ';
			}
			$s_closed_data .= '],}';
			
			// count the number of days
			$i_num_days = $this->date_diff($i_last_month, $i_today)-1;
			
			return "<script type='text/javascript'>
				// from http://www.jqueryflottutorial.com/how-to-make-jquery-flot-time-series-chart.html
				function gd(year, month, day) {
					var now = new Date();
					return (new Date(year, month - 1, day, now.getHours() + 4, now.getMinutes()).getTime());
				}
				dayOfWeek = ['Sun', 'Mon', 'Tue', 'Wed', 'Thr', 'Fri', 'Sat'];
				previousPoint = null, previousLabel = null;
				$.fn.UseTooltip = function () {
					$(this).bind('plothover', function (event, pos, item) {
						if (item) {
							if ((previousLabel != item.series.label) || (previousPoint != item.dataIndex)) {
								previousPoint = item.dataIndex;
								previousLabel = item.series.label;
								$('#tooltip').remove();
								
								var x = item.datapoint[0];
								var y = item.datapoint[1];
								var date = new Date(x);
								var color = item.series.color;
								
								showTooltip(item.pageX, item.pageY, color,
								'<strong>' + item.series.label + '</strong><br>'  +
								(date.getMonth()+1) + '/' + date.getDate() +
								' (' + dayOfWeek[date.getDay()] + ') : <strong>' + y + '</strong>');
							}
						} else {
							$('#tooltip').remove();
							previousPoint = null;
						}
					});
				};
				
				var data = [ $s_closed_data ];
				var options = {
						series: {
							lines: { show: true, },
							points: { show: true, },
						},
						yaxis: {
							show: true,
							axisLabel: 'Orders Closed',
						},
						xaxes: [
							{
								mode: 'time',
								tickFormatter: function (val, axis) {
									var date = new Date(val);
									if (date.getDate() % 2 == 1)
										return (date.getMonth()+1)+'/'+date.getDate();
									return '';
								},
								tickSize: [1, 'day'],
								color: 'black',       
								axisLabel: 'Date',
								axisLabelUseCanvas: true,
								axisLabelFontSizePixels: 12,
								axisLabelFontFamily: 'Verdana, Arial',
								axisLabelPadding: 10
							}
						],
						grid: {
							hoverable: true,
							borderWidth: 3,
							mouseActiveRadius: 50,
							axisMargin: 20
						},
					};
				$.plot($('#graphing_area'), data, options);
				$('#graphing_area').UseTooltip();
			</script>";
		}
		
		// returns the number of days between $i_date_a and $i_date_b, starting with 0
		// if $i_date_b < $i_date_a, returns 0
		private function date_diff($i_date_a, $i_date_b) {
			
			// check the input
			if ($i_date_b < $i_date_a)
				return 0;
			
			// sanitize the input
			$i_date_a = strtotime(date('Y-m-d',(int)$i_date_a));
			$i_date_b = strtotime(date('Y-m-d',(int)$i_date_b));
			
			// get the number of days between
			$i = 0;
			while ($i_date_a < $i_date_b) {
				$i_date_a = strtotime(date('Y-m-d',$i_date_a).' +1 days');
				$i++;
			}
			
			return $i;
		}
	}
	
	$o_php_graph_object = new ORDERS_X_DAY_1MONTH();
	
?>
