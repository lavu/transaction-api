<?php
	
	require_once(dirname(__FILE__).'/../login.php');
	require_once('/home/poslavu/public_html/admin/cp/resources/lavuquery.php');
	require_once('/home/poslavu/public_html/admin/cp/resources/core_functions.php');
	
	// get the environment set up
	session_start();
	if (!account_loggedin())
		header("Location: http://admin.poslavu.com/manage/");
		
	// here's a list of graphs
	$a_all_graphs = array(
		array('display_name'=>'Orders by Time, 1 Week', 'javascript_id'=>'orders_x_time_1week', 'file'=>'orders_x_time_1week', 'accesses'=>array('customers')),
		array('display_name'=>'Orders by Day, 1 Month', 'javascript_id'=>'orders_x_day_1month', 'file'=>'orders_x_day_1month', 'accesses'=>array('customers')),
	);
	
	function main() {
		global $a_all_graphs;
		
		$retval = '';
		$retval .= '<div id="MainContainer" style="background-color:white;">';
		$retval .= '<table id="main" style="background-color:white;"><tr>';
		
		// draw the list of graphs
		$retval .= '
			<td id="list_of_graphs" style="vertical-align:top; border-right:1px solid black"><div style="padding:30px;">
				List of graphs:<br />';
		$i_available_graphs = 0;
		foreach($a_all_graphs as $a_graph) {
			if (can_access_by_array($a_graph['accesses']) == count($a_graph['accesses'])) {
				$retval .= '
					<a onclick="loadGraph(\''.$a_graph['file'].'\');" class="graphs_list">'.$a_graph['display_name'].'</a><br />';
				$i_available_graphs++;
			}
		}
		if ($i_available_graphs == 0) {
			$retval .= 'You have no graphs available to you.';
		}
		$retval .= '
			</div></td>';
		
		// draw the graph area
		$retval .= '
			<td style="overflow:visible;"><div id="graphing_area" style="padding:30px; width:700px; height:550px; overflow:visible;"><font style=\'color:gray;\'>Click on one of the links to the left to get started.</font></div></td>';
		
		$retval .= '</div>';
		$retval .= drawJavascript();
		$retval .= drawStyle();
		
		return $retval;
	}
	
	function drawJavascript() {
		return '
			<script type="text/javascript">
				function loadGraph(filename) {
					$("#graphing_area").html("<font style=\'color:gray;\'>loading...</font>");
					$.ajax({
						url: "./graphs/index.php",
						type: "POST",
						data: { dataname: "'.$_POST['dataname'].'", draw_graph: 1, filename: filename },
						async: true,
						success: function(message) {
							message = JSON.parse(message);
							$("#graphing_area").html("");
							$("#graphing_area").append(message.graph);
							eval(message.javascript);
						},
					});
				}
				
				// from http://www.jqueryflottutorial.com/how-to-make-jquery-flot-time-series-chart.html
				function showTooltip(x, y, color, contents) {
					$("<div id=\'tooltip\'>" + contents + "</div>").css({
						position: "absolute",
						display: "none",
						top: y - 40,
						left: x - 120,
						border: "2px solid " + color,
						padding: "3px",
						"font-size": "9px",
						"border-radius": "5px",
						"background-color": "#fff",
						"font-family": "Verdana, Arial, Helvetica, Tahoma, sans-serif",
						opacity: 0.9
					}).appendTo("body").fadeIn(200);
				}
			</script>
			<script language="javascript" type="text/javascript" src="/newcp/backend/js/jquery/flot/jquery.flot.js"></script>
			<script language="javascript" type="text/javascript" src="/newcp/backend/js/jquery/flot/jquery.flot.time.js"></script>
			<script language="javascript" type="text/javascript" src="/newcp/backend/js/jquery/flot/jquery.flot.axislabels.js"></script>';
	}
	
	function drawStyle() {
		return '
			<style type="text/css">
				a.graphs_list {
					color: #668B8B;
					cursor: pointer;
				}
			</style>';
	}
	
	function drawGraph() {
		global $o_php_graph_object;
		
		$s_dataname = $_POST['dataname'];
		$s_filename = $_POST['filename'];
		
		$a_restaurants = ConnectionHub::getConn('poslavu')->getDBAPI()->getAllInTable('restaurants', array('data_name'=>$s_dataname), TRUE, array('selectclause'=>'`lavu_lite_server`'));
		if (count($a_restaurants) == 0)
			return json_encode(array('graph'=>'This restaurant can\'t be found in the restaurants table.'));
		$a_restaurant = $a_restaurants[0];
		if ($a_restaurant['lavu_lite_server'] != '')
			return json_encode(array('graph'=>'Graphs are not supported for lavu lite restaurants.'));
		
		$a_retval = array('graph'=>'This graph couldn\'t be found.');
		
		if (file_exists(dirname(__FILE__).'/'.$s_filename.'.php')) {
			require_once(dirname(__FILE__).'/'.$s_filename.'.php');
			$a_retval['graph'] = 'The object $o_php_graph_object doesn\'t exist for this graph.';
			if (isset($o_php_graph_object)) {
				$a_retval = array('graph'=>'This "graph" method doesn\'t exist for this graph.');
				if (method_exists($o_php_graph_object, 'graph'))
					$a_retval['graph'] = $o_php_graph_object->graph($s_dataname);
			}
		}
		
		return json_encode($a_retval);
	}
	
	if (!isset($_POST['draw_graph'])) {
		echo main();
	} else {
		echo drawGraph();
	}
	
?>
