<?php
	
	class ORDERS_X_TIME_1WEEK {
		
		// returns a string representing the graph
		function graph($s_dataname) {
			
			// check that the dataname is good
			if ($s_dataname == '')
				return 'Dataname is blank.';
			$restaurant_exists = ConnectionHub::getConn('poslavu')->getDBAPI()->getAllInTable('restaurants', array('data_name'=>$s_dataname), TRUE);
			if (count($restaurant_exists) == 0)
				return 'Restaurant "'.$s_dataname.'" doesn\'t exist or has been deleted (it can\'t be found in the restaurants table).';
			
			// get the orders for the last week
			$i_secs_today = strtotime('now')-strtotime('today');
			$i_last_week = strtotime('-1 week') - $i_secs_today;
			$s_last_week = date('Y-m-d H:i:s', $i_last_week);
			$a_orders = ConnectionHub::getConn('poslavu')->getDBAPI()->getAllInTable('orders', array('starttime'=>$s_last_week), TRUE,
				array('databasename'=>'poslavu_'.$s_dataname.'_db', 'whereclause'=>"WHERE `closed` >= '[starttime]'", 'selectclause'=>'`opened`,`closed`'));
			
			// initialize the data array
			$a_opened_data = array();
			$a_closed_data = array();
			$i = 0;
			for ($j = 0; $j < 24; $j++) {
				$a_opened_data[$i*24+$j] = 0;
				$a_closed_data[$i*24+$j] = 0;
			}
			
			// translate to data
			foreach($a_orders as $a_order) {
				$i_opened_time = floor(   ( strtotime($a_order['opened']) - $i_last_week ) / 3600   );
				$a_opened_data[$i_opened_time%24]++;
				$i_closed_time = floor(   ( strtotime($a_order['closed']) - $i_last_week ) / 3600   );
				$a_closed_data[$i_closed_time%24]++;
			}
			$s_opened_data = '{ color: 1, label: "Orders Opened By Hour", data: [';
			foreach($a_opened_data as $k=>$v) {
				$s_opened_data .= '['.$k.','.$v.'], ';
			}
			$s_opened_data .= '],}';
			$s_closed_data = '{ color: "#aecd37", label: "Orders Closed By Hour", data: [';
			foreach($a_closed_data as $k=>$v) {
				$s_closed_data .= '['.$k.','.$v.'], ';
			}
			$s_closed_data .= '],}';
			
			return "<script type='text/javascript'>
				var data = [ $s_opened_data, $s_closed_data ];
				var options = {
						series: {
							lines: { show: true, },
							points: { show: true, },
						},
						xaxis: {
							show: true,
							min: 0,
							max: 23,
							autoscaleMargin: 1,
							ticks: 23,
							tickSize: 1,
						},
					};
				$.plot($('#graphing_area'), data, options);
			</script>";
		}
	}
	
	$o_php_graph_object = new ORDERS_X_TIME_1WEEK();
	
?>
