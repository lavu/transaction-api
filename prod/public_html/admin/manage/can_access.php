<?php

	require_once(dirname(__FILE__)."/../cp/resources/lavuquery.php");

function add_to_can_access_list($area, $s_filename = '') {

	global $o_can_access_list;

	require_once(dirname(__FILE__)."/../cp/resources/json.php");

	// load from the db if not yet loaded
	if (!isset($o_can_access_list)) {
		$access_query = mlavu_query("SELECT `access` FROM `poslavu_MAIN_db`.`accounts` WHERE `username`='all_accesses'");
		if ($access_query !== FALSE && mysqli_num_rows($access_query) > 0) {
			$access_read = mysqli_fetch_assoc($access_query);
			$o_can_access_list = LavuJson::json_decode($access_read['access'], false);
		}
	}

	// find the associated file
	if ($s_filename == '') {
		$a_backtrace = debug_backtrace();
		$s_filename = $a_backtrace[1]['file'];
	}

	// check if in the can_access array
	if (isset($o_can_access_list->$area) && isset($s_filename, $o_can_access_list->$area->files->$s_filename)) {

		// check if it is time to update this access to reflect that it was accessed within the last week
		$i_time = (int)$o_can_access_list->$area->files->$s_filename->last_access;
		if (strtotime('-1 days') < $i_time)
			return;
	}

	// add to the array
	if (!isset($o_can_access_list->$area)) {
		$o_can_access_list->$area = new stdClass();
		$o_can_access_list->$area->files = new stdClass();
	}
	if (!isset($o_can_access_list->$area->files->$s_filename))
		$o_can_access_list->$area->files->$s_filename = new stdClass();
	$o_can_access_list->$area->files->$s_filename->last_access = strtotime('now');
	$s_can_access = LavuJson::json_encode($o_can_access_list);
	mlavu_query("UPDATE `poslavu_MAIN_db`.`accounts` SET `access`='[access]' WHERE `username`='all_accesses'", array('access'=>$s_can_access));
}

function can_access($area)
{

	global $loggedin_access;

	add_to_can_access_list($area);

	if(trim(strtolower($loggedin_access))=="all")
		return true;
	else if(stristr($loggedin_access,$area)!==false)
		return true;
	else
		return false;
}

// returns the number of matching can_access's
function can_access_by_array($a_access_strings) {
	$i_access_count = 0;
	foreach($a_access_strings as $s_access)
		if (can_access($s_access))
			$i_access_count++;
	return $i_access_count;
}

?>