<?php
	require_once(dirname(__FILE__)."/../../../cp/resources/lavuquery.php");
	require_once(dirname(__FILE__)."/getSyncVersion.php");

	/**
	 * Schedule a set of files to be synced to local server.
	 * @param  string  $dataname     The dataname of the account to sync to
	 * @param  integer $locationid   The location id of the account to sync to
	 * @param  string  $sync_setting 'copy remote file'
	 * @param  array   $array        an array or subarrays, with each subarray in the form array(remote filename, local filename, sync value 2 = "")
	 * @return none                  N/A
	 */
	function schedule_msync_with_array($dataname,$locationid,$sync_setting,$array) {
		$remote_files = array();
		$local_files = array();
		$sync_value2 = array();
		for ($i = 0; $i < count($array); $i++) {
			$remote_files[] = $array[$i][0];
			$local_files[] = $array[$i][1];
			$sync_value2[] = $array[$i][2];
		}
		$remote_files_string = implode(", ",$remote_files);
		$local_files_string = implode(", ",$local_files);
		$sync_value2_string = implode("",$sync_value2);
		schedule_msync($dataname,$locationid,$sync_setting,$remote_files_string,$local_files_string,$sync_value2_string);
	}

	// some example uses:
	// schedule_msync('zukos', 1, "copy remote file", $remote.".php", $local.".php")
	// schedule_msync('DEV', 27,"create_directory","/cp/objects/","");
	// schedule_msync($dataname,$loc_id,"health check","","");
	// schedule_msync($dataname,$loc_id,"open tunnel");
	// schedule_msync($dataname,$loc_id,"read file structure");
	// schedule_msync($dataname,$loc_id,"sync database");
	// schedule_msync($dataname,$loc_id,"records_check","","");
	// schedule_msync($dataname,$loc_id,"remove local file",$_GET['remove_local_files']);
	// schedule_msync($dataname,$loc_id,"retrieve local file",$_GET['retrieve_local_file']);
	// schedule_msync($dataname,$loc_id,"sync_cmd:remote control",$set_remote_control);
	// schedule_msync($dataname,$loc_id,"api sync scheduled");
	// schedule_msync($dataname,$loc_id,"upload signature",$signature_basename);
	// schedule_msync($dataname,$loc_id,"upload waiver",$waiver_sig_name);
	// schedule_msync($dataname,$loc_id,"records_check","extra_command",$cmd_details,$cmd);
	function schedule_msync($dataname,$locationid,$sync_setting,$sync_value="",$sync_value_long="",$sync_value2=""){
		lavu_connect_dn($dataname,'poslavu_'.$dataname.'_db');
		$exist_query = lavu_query("select * from `config` where `setting`='[setting]' and `location`='[location]' and `value`='[value]' and `value_long`='[value_long]' and `value2`='[value2]' AND `type`='log'", array('dataname'=>$dataname, 'setting'=>$sync_setting, 'location'=>$locationid, 'value'=>$sync_value, 'value_long'=>$sync_value_long, 'value2'=>$sync_value2));
		if($exist_query !== FALSE && mysqli_num_rows($exist_query)){
			return "exists already";
		} else {
					$query_string = "insert into `config` (`setting`,`location`,`value`,`value_long`,`value2`,`type`) values('[setting]','[location]','[value]','[value_long]','[value2]','log')";
					$query_vars = array('dataname'=>$dataname, 'setting'=>$sync_setting, 'location'=>$locationid, 'value'=>$sync_value, 'value_long'=>$sync_value_long, 'value2'=>$sync_value2);
					lavu_query($query_string, $query_vars);
					foreach($query_vars as $k=>$v)
						$query_string = str_replace("[{$k}]", $v, $query_string);
					//error_log($query_string);
					return "inserted";
				}
	}

	class LLSFunctionsScheduleMsync {

		// sets the package level on the LLS
		// @$s_dataname: the account to affect
		// @$i_package_level: the package level to set on the local server
		// @return: TRUE on success, FALSE on failure
		public static function setPackage($s_dataname, $i_package_level) {

			$s_databasename = ConnectionHub::getConn('poslavu')->escapeString("poslavu_{$s_dataname}_db");

			// get the encryption key
			$a_authorizations = ConnectionHub::getConn('poslavu')->getDBAPI()->getAllInTable('authorizations', array('data_name'=>$s_dataname), TRUE, array('selectclause'=>'`package_key`'));
			if (count($a_authorizations) == 0) {
				error_log(__FILE__." can't find authorization");
				return FALSE;
			}
			$s_package_key = $a_authorizations[0]['package_key'];

			// encrypt the setting and value
			$crypt_query = mlavu_query("SELECT HEX(AES_ENCRYPT('[dataname]','[package_key]')) AS `setting`, HEX(AES_ENCRYPT('[package]','[package_key]')) AS `value`", array('dataname'=>$s_dataname, 'package_key'=>$s_package_key, 'package'=>$i_package_level)); // poslavu_MAIN_db
			$crypt_read = mysqli_fetch_assoc($crypt_query);
			$s_setting = $crypt_read['setting'];
			$s_value = $crypt_read['value'];
			$s_type = "location_config_setting";

			// get the locations for the account
			$a_locations = ConnectionHub::getConn('poslavu')->getDBAPI()->getAllInTable('locations', NULL, TRUE, array('databasename'=>$s_databasename, 'selectclause'=>'`id`'));

			// schedule the msync
			$a_queries = array();
			$a_queries[] = "DELETE FROM ---database---`config` WHERE `setting`='{$s_setting}' AND `type`='{$s_type}'";
			$a_queries[] = "INSERT INTO ---database---`config` (`setting`,`type`,`value`) VALUES ('{$s_setting}','{$s_type}','{$s_value}')";
			foreach($a_queries as $k=>$s_query) {
				mlavu_query(str_replace("---database---", "`poslavu_{$s_dataname}_db`.", $s_query));
				$a_queries[$k] = str_replace("----database---", "", $s_query);
			}
			for($i = 0; $i < count($a_locations); $i++)
				self::scheduleQueries($s_dataname, $a_locations[$i]['id'], $a_queries);
			return TRUE;
		}

		// schedules a set of queries to be performed on the LLS
		// @$s_dataname: the account to affect
		// @$locationid: location id of the account
		// @$a_queries: the set of queries to perform
		public static function scheduleQueries($s_dataname, $locationid, $a_queries) {
			return schedule_msync($s_dataname, $locationid, "run local query", "", '"'.implode("[_query_]", $a_queries).'"');
		}

		/**
		 * Schedules a table to by synced to the local server
		 * @param  string $s_dataname The dataname of the account that wishes to be synced
		 * @param  string $locationid The location id of the account that wishes to be synced
		 * @param  string $table_name The tablename to be synced down
		 */
		public static function syncTableDown($s_dataname, $locationid, $table_name) {
			return schedule_msync($s_dataname, $locationid, "table updated", $table_name);
		}

		public static function runBashCommandOnLLS($s_dataname, $locationid, $bashCommand) {
error_log("runBashCommandOnLLS reached with dn:$s_dataname locid:$locationid bashcommand:$bashCommand");
			return schedule_msync($s_dataname, $locationid, "exec_command","",$bashCommand);
		}

		/**
		 * Copies a set of files to the LLS
		 * @param  string  $s_dataname   The account to copy files to
		 * @param  integer $i_locationid The id of the account to copy files to
		 * @param  array   $a_files      An array of filenames to copy, eg array("/poslavu-local/lib/jcvs/jc_inc7.php")
		 * @return none                  N/A
		 */
		public static function copyFilesToLLS($s_dataname, $i_locationid, $a_files) {
			$remotedir = self::getRemoteDir($s_dataname,$i_locationid);
			$localdir = self::getLocalDir();
			$using_ioncube = self::usingIoncube($s_dataname,$i_locationid);
			$files_to_copy = array();

			foreach($a_files as $file){
				$remotefile = str_replace('/poslavu-local/',$remotedir,$file);
				$localfile = str_replace('/poslavu-local/',$localdir,$file);
				if (!$using_ioncube)
					schedule_msync($s_dataname,$i_locationid,"copy remote file", $remotefile, $localfile);
				$files_to_copy[] = array($remotefile, $localfile);
			}
			self::updateSync($s_dataname, $i_locationid);
			if ($using_ioncube)
				schedule_msync_with_array($s_dataname,$i_locationid,"copy remote file",$files_to_copy);
		}

		/**
		 * Guarantees that the local server is on the latest sync.php and sync_exec.php versions
		 * @param  string  $s_dataname   The account to update
		 * @param  integer $i_locationid The id of the account to update
		 * @return none                  N/A
		 */
		public static function updateSync($s_dataname, $i_locationid) {
			$b_using_ioncube = self::usingIoncube($s_dataname,$i_locationid);

			if (!$b_using_ioncube)
				return;
			$i_currentversion = self::getMostRecentSyncVersionNumber();
			$i_syncversion = self::getLLSSyncVersionNumber($s_dataname, $i_locationid);
			$remote = "lls-ioncube/local/sync";
			$local = "../local/sync";

			if ($i_syncversion < $i_currentversion) {
				schedule_msync($s_dataname, $i_locationid,"copy remote file",$remote.".php",$local.".php");
				schedule_msync($s_dataname, $i_locationid,"copy remote file",$remote."_exec.php",$local."_exec.php");
			}
		}

		/***********************************************************************
		 *                    P R I V A T E   F U N C T I O N S                *
		 **********************************************************************/

		private static function getLLSSyncVersionNumber($s_dataname, $i_locationid) {
			$i_syncversion = 0;
			$locserv_query = mlavu_query("SELECT SQL_NO_CACHE * FROM `poslavu_[1]_db`.`config` WHERE `setting`='local_sync_version' AND `location`='[2]'", $s_dataname, $i_locationid);
			if ($locserv_query) {
				if (mysqli_num_rows($locserv_query) > 0) {
					$locserv_read = mysqli_fetch_assoc($locserv_query);
					$i_syncversion = $locserv_read['value']*1;
				}
			}

			return $i_syncversion;
		}

		// reads the current version number from the repo/lls-ioncube/sync file
		// returns 0 if the sync version number can't be found
		private static function getMostRecentSyncVersionNumber() {
			$file = fopen('/home/poslavu/public_html/admin/cp/reqserv/repo/lls-ioncube/local/sync.php', 'r');
			if ($file !== FALSE) {
				while (($s_buffer = fgets($file, 1048576)) !== FALSE) {
					$a_matches = array();
					$count = preg_match('/^\s*\$v\s*=\s*[0-9]+\s*;/', $s_buffer, $a_matches);
					if ($count === FALSE)
						break;
					if ($count > 0) {
						$a_numbers = array();
						preg_match('/[0-9]+/', $a_matches[0], $a_numbers);
						return (int)$a_numbers[0];
					}
				}
			}

			return 0;
		}

		private static function getRemoteDir($dataname,$locationid) {
			$lls_encoder = "zendguard";
			if (self::usingIoncube($dataname,$locationid))
				$lls_encoder = "ioncube";
			return "lls-".$lls_encoder."/";
		}

		private static function usingIoncube($dataname,$locationid) {
			$locserv_query = mlavu_query("SELECT * FROM `poslavu_[1]_db`.`config` WHERE `setting`='local_file_structure' AND `location`='[2]'", $dataname, $locationid);
			if (!$locserv_query)
				return TRUE;
			if (mysqli_num_rows($locserv_query) < 1)
				return TRUE;

			$locserv_read = mysqli_fetch_assoc($locserv_query);
			if (strpos($locserv_read['value_long'], "ioncube_loader"))
				return TRUE;
			else
				return FALSE;
		}

		private static function getLocalDir() {
			return "../";
		}
	}

?>
