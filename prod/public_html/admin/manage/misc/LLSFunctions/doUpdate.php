<?php
	require_once("/home/poslavu/public_html/admin/cp/resources/lavuquery.php");
	require_once(dirname(__FILE__)."/schedule_msync.php");
	require_once(dirname(__file__)."/getSyncVersion.php");

	global $dontActuallyDoAnUpdateBoys;
	if (!isset($dontActuallyDoAnUpdateBoys))
		doUpdate_do_indeed_do_update();

	function doUpdate_do_indeed_do_update() {
		if( isset($_POST['dataname']) && isset($_POST['locid']) && isset($_POST['files']) ){
			$dataname = $_POST['dataname'];
			$locationid = $_POST['locid'];
			$files= explode("," , $_POST['files']);

			LLSFunctionsScheduleMsync::copyFilesToLLS($dataname, $locationid, $files);

			echo "Update successfully performed";
			exit;
		}else{
			echo "Update Creation unsuccessful :(";
			exit;
		}
	}

	function using_ioncube($dataname,$locationid) {
		lavu_connect_dn($dataname,'poslavu_'.$dataname.'_db');
		$locserv_query = lavu_query("SELECT * FROM `config` WHERE `setting`='local_file_structure' AND `location`='[2]'", $dataname, $locationid);
		if (!$locserv_query)
			return TRUE;
		if (mysqli_num_rows($locserv_query) < 1)
			return TRUE;

		$locserv_read = mysqli_fetch_assoc($locserv_query);
		if (strpos($locserv_read['value_long'], "ioncube_loader"))
			return TRUE;
		else
			return FALSE;
	}

	function get_remote_dir($dataname,$locationid) {
		$lls_encoder = "zendguard";
		if (using_ioncube($dataname,$locationid))
			$lls_encoder = "ioncube";
		return "lls-".$lls_encoder."/";
	}

	function get_local_dir() {
		return "../";
	}

	function update_sync($s_dataname, $i_locationid) {
		LLSFunctionsScheduleMsync::updateSync($s_dataname, $i_locationid);
	}

	// reads the current version number from the repo/lls-ioncube/sync file
	// returns 0 if the sync version number can't be found
	function get_most_recent_sync_version_number() {
		$file = fopen('/home/poslavu/public_html/admin/cp/reqserv/repo/lls-ioncube/local/sync.php', 'r');
		if ($file !== FALSE) {
			while (($s_buffer = fgets($file, 1048576)) !== FALSE) {
				$a_matches = array();
				$count = preg_match('/^\s*\$v\s*=\s*[0-9]+\s*;/', $s_buffer, $a_matches);
				if ($count === FALSE)
					break;
				if ($count > 0) {
					$a_numbers = array();
					preg_match('/[0-9]+/', $a_matches[0], $a_numbers);
					return (int)$a_numbers[0];
				}
			}
		}

		return 0;
	}

?>
