<?php

    if( isset( $_POST['handling_dn_ajax'] ) )
    {
        if($_POST['jibberish'] != 'afj3jg8sh34jag83h'){
            exit();
        }
        handle_DN_LLSAuthorizationAjaxRequest();
        exit();
    }

    function echoLLSAuthorizationWebCode(){
    ?>
	     <script type='text/javascript'>
	         var timeOutVariable;
	         function keyUpEventHandler(){
	             currTxt = document.getElementById('lls_dn_find_input').value;
	             filteredTxt = currTxt.replace(/[^_0-9a-zA-Z]/, '');
	             document.getElementById('lls_dn_find_input').value = filteredTxt;
	             document.getElementById('response_html_frame').innerHTML = '&nbsp;';
	             window.clearTimeout(timeOutVariable);
    	         timeOutVariable = setTimeout(function(){dn_lls_lookup()}, 300);
	         }
	         function dn_lls_lookup(command) {
		         var CURR_DN_INPUT_TXT = document.getElementById('lls_dn_find_input').value;
		         if(CURR_DN_INPUT_TXT == ''){
		             document.getElementById('response_html_frame').innerHTML = '&nbsp;';
    		         return;
		         }
		         //Now we make ajax call to see if this dataname is valid.
		         var xmlhttp;
		         if (window.XMLHttpRequest){
    		         xmlhttp=new XMLHttpRequest();
		         }else{
    		         xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
		         }
		         //The ajax return function
		         xmlhttp.onreadystatechange=function(){
    		         if(xmlhttp.readyState == 4 && xmlhttp.status == 200){
    		             //alert(xmlhttp.responseText);
    		             //return;
    		             if(xmlhttp.responseText == 'enable_success'){
        		             //alert('Enable Successful');
        		             dn_lls_lookup();
    		             }
    		             else if(xmlhttp.responseText == 'disable_success'){
        		             //alert('Disable Successful');
        		             dn_lls_lookup();
    		             }
    		             else{
        		             var returnData = JSON.parse(xmlhttp.responseText);
        		             document.getElementById('response_html_frame').innerHTML = returnData['html_insert'];
    		             }
    		         }
		         }
		         var url="//admin.poslavu.com/manage/misc/LLSFunctions/lls_authorization.php";
		         var postStr="handling_dn_ajax=1&data_name=" + CURR_DN_INPUT_TXT +
		                          "&cmd=" + command + "&jibberish=" + 'afj3jg8sh34jag83h';
		         xmlhttp.open("POST", url, true);
		         xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
		         xmlhttp.send(postStr);
	         }
	         function authorize_lls(){
    	         dn_lls_lookup('authorize');
	         }
	         function deauthorize_lls(){
    	         dn_lls_lookup('deauthorize');
	         }
	     </script>

	     <table>
	         <tr>
	             <td> <div> <input type='text' id='lls_dn_find_input' value='' onkeyup="if (event.keyCode == 13) dn_lls_lookup(''); else keyUpEventHandler();"> </div> </td>
	         </tr>
	     </table>
	     <div id='response_html_frame'> </div>
    <?php
	}

	function handle_DN_LLSAuthorizationAjaxRequest(){
	    global $_POST;
    	require_once("/home/poslavu/public_html/admin/cp/resources/core_functions.php");


    	//If we are authorizing or deauthorizing
    	if($_POST['cmd'] == 'authorize'){
        	enableLLS($_POST['data_name']);
        	exit();
    	}
    	if($_POST['cmd'] == 'deauthorize'){
            disableLLS($_POST['data_name']);
        	exit();
    	}

    	//Else we are just doing a regular lookup...

    	//The return data agent.
    	$returnJSONArr = array();

    	//<>~-~<>~-~<>~-~<>~-~<>~-~<>~-~<>~-~<>~-~<>~-~<>~-~<>~-~<>~-~<>~-~<>~-~<>~-~<>~-~<>~-~<>~-~<>~-~<>~-~<>~-~<>~-~<>~-~<>~-~<>~-~<>~-~<>
    	//  R E S T A U R A N T    T A B L E
    	//Now we check against the restaurants table to see if first it is a valid data_name.
    	$restaurantTblResult = mlavu_query("SELECT * FROM `poslavu_MAIN_db`.`restaurants` WHERE `data_name`='[1]'", $_POST['data_name']);
    	if(!$restaurantTblResult){
        	echo '{"error":"failed query looking into `restaurants` table"}';
        	exit();
    	}
    	if(mysqli_num_rows($restaurantTblResult) == 0){
        	$returnJSONArr['exists_in_restaurants_table'] = '0';
    	}else{
        	$returnJSONArr['exists_in_restaurants_table'] = '1';
    	}
    	$restArr = mysqli_fetch_assoc($restaurantTblResult);


        //<>~-~<>~-~<>~-~<>~-~<>~-~<>~-~<>~-~<>~-~<>~-~<>~-~<>~-~<>~-~<>~-~<>~-~<>~-~<>~-~<>~-~<>~-~<>~-~<>~-~<>~-~<>~-~<>~-~<>~-~<>~-~<>~-~<>
    	//  L O C A L _ S E R V E R S    T A B L E
    	//Now we see if they are in the list of authorizations
    	$inLSsQuery = "SELECT * FROM `poslavu_MAIN_db`.`authorizations` WHERE `data_name`='[1]'";
    	$inLSsResult = mlavu_query($inLSsQuery, $_POST['data_name']); // poslavu_MAIN_db
    	if(!$inLSsQuery){
        	echo '{"error":"failed query looking into `authorizations` table"}';
        	exit();
    	}
    	if(mysqli_num_rows($inLSsResult) == 0){
        	$returnJSONArr['exists_in_authorizations_table'] = '0';
    	}else{ //TODO MAKE SURE THERE'S NO MORE THAN 1 RESULT, THAT COULD BE A PROBLEM.
    	    $inLSsResultArr = mysqli_fetch_assoc($inLSsResult);
        	$returnJSONArr['exists_in_authorizations_table'] = '1';
        	$returnJSONArr['lls_authorized'] = $inLSsResultArr['lls_authorized'];
    	}


        //<>~-~<>~-~<>~-~<>~-~<>~-~<>~-~<>~-~<>~-~<>~-~<>~-~<>~-~<>~-~<>~-~<>~-~<>~-~<>~-~<>~-~<>~-~<>~-~<>~-~<>~-~<>~-~<>~-~<>~-~<>~-~<>~-~<>
    	//    H T M L    I N J E C T I O N
    	$inRestTable = $returnJSONArr['exists_in_restaurants_table'];
    	$inLSsTable   = $returnJSONArr['exists_in_authorizations_table'];
    	$hasBeenAuth = $returnJSONArr['lls_authorized'];

    	//We allow the enable button if they are in the restaurant table and they are not already enabled.
    	$llsEnableBtnDisabledHTML = $inRestTable && !$hasBeenAuth ? '' : "disabled='true'";
    	//We allow the disable button if they are marked as enabled.
    	$llsDisableBtnDisableHTML =  $hasBeenAuth ? '' : "disabled='true'";

    	$htmlInsert  = '<table border="1">';
    	$htmlInsert .= '<tr>';
    	//$htmlInsert .=     '<td>Exists in restaurants list:';
    	//$htmlInsert .=        $returnJSONArr['exists_in_restaurants_table'] ? ' yes ' : ' no ';
    	//$htmlInsert .=     '</td>';
    	//$htmlInsert .=     '<td>Exists in authorizations list:';
    	//$htmlInsert .=        $returnJSONArr['exists_in_authorizations_table'] ? ' yes ' : ' no ';
    	//$htmlInsert .=     '</td>';
    	$htmlInsert .=     "<td> <div> <button id='authorize_lls_dn_bt'   onclick='authorize_lls()'   $llsEnableBtnDisabledHTML> Authorize LLS </button> </div> </td>";
    	$htmlInsert .=     "<td> <div> <button id='deauthorize_lls_dn_bt' onclick='deauthorize_lls()' $llsDisableBtnDisableHTML> De-Authorize LLS </button> </div> </td>";
    	$htmlInsert .= '</tr>';
    	$htmlInsert .= '</table>';

    	if($inRestTable)
    	   $returnJSONArr['html_insert'] = $htmlInsert;
        else{
           $returnJSONArr['html_insert'] = 'Data Name: [' . $_POST['data_name'] . '] not found';
        }
    	echo lavu_json_encode($returnJSONArr);
	}
    function disableLLS($dataName, $b_no_echo = FALSE){
        //update
        mlavu_query("UPDATE `poslavu_MAIN_db`.`authorizations` SET `lls_authorized`='0' WHERE `data_name`='[1]'", $dataName);
        if (!$b_no_echo)
            echo 'disable_success';
        return TRUE;
    }
    function enableLLS($dataName, $b_no_echo = FALSE, $b_no_exit = FALSE){

        // First see if the row exists
        $selectResult = mlavu_query("SELECT * FROM `poslavu_MAIN_db`.`authorizations` WHERE `data_name`='[1]'", $dataName);
        if($selectResult === FALSE){
            if ($b_no_exit)
                return FALSE;
            else
                exit();
        }

        // insert/update the data into the DB
        if(mysqli_num_rows($selectResult) == 0){
            mlavu_query("INSERT INTO `poslavu_MAIN_db`.`authorizations` (`data_name`, `lls_authorized`, `package_key`) VALUES('[1]','1','benbean')", $dataName);
        }
        if(mysqli_num_rows($selectResult) > 1){
            mlavu_query("UPDATE `poslavu_MAIN_db`.`authorizations` SET `lls_authorized`='1' WHERE `data_name`='[1]'", $dataName);
        }
        if(mysqli_num_rows($selectResult) == 1){
            mlavu_query("UPDATE `poslavu_MAIN_db`.`authorizations` SET `lls_authorized`='1' WHERE `data_name`='[1]'", $dataName);
        }

        // update the package level on the LLS
        require_once(dirname(__FILE__)."/../../../sa_cp/billing/payment_profile_functions.php");
        require_once(dirname(__FILE__)."/schedule_msync.php");
        $a_restaurants = ConnectionHub::getConn('poslavu')->getDBAPI()->getAllInTable('restaurants', array('data_name'=>$dataName), TRUE, array('selectclause'=>'`id`', 'limitclause'=>'LIMIT 1'));
        if (count($a_restaurants) > 0) {
            $a_package_status = find_package_status($a_restaurants[0]['id']);
            $i_package_level = (int)$a_package_status['package'];
            LLSFunctionsScheduleMsync::setPackage($dataName, $i_package_level);
        } else {
            error_log(__FILE__." can't find restaurant \"{$dataName}\"");
        }

        if (!$b_no_echo)
            echo 'enable_success';
        return TRUE;
    }

?>
