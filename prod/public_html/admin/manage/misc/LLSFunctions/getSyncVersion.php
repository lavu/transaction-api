<?php
	function get_lls_sync_version_number($s_dataname, $i_locationid) {
		$i_syncversion = 0;
		$locserv_query = mlavu_query("SELECT SQL_NO_CACHE * FROM `poslavu_[1]_db`.`config` WHERE `setting`='local_sync_version' AND `location`='[2]'", $s_dataname, $i_locationid);
		if ($locserv_query) {
			if (mysqli_num_rows($locserv_query) > 0) {
				$locserv_read = mysqli_fetch_assoc($locserv_query);
				$i_syncversion = $locserv_read['value']*1;
			}
		}
		
		return $i_syncversion;
	}
	
	// $i_currentversion can be passed in as FALSE, in which case the script checks for past variable version numbers
	function update_lls_sync_version_number($s_dataname, $i_locationid, $i_currentversion) {
		if ($i_currentversion === FALSE) {
			$i_sync_version = 0;
			$i_sync_exec_version = 0;
			
			if (isset($_POST['sync_version']))
				$i_sync_version = (int)$_POST['sync_version'];
			if (isset($_POST['sync_exec_version']))
				$i_sync_exec_version = (int)$_POST['sync_exec_version'];
			$i_currentversion = min($i_sync_version, $i_sync_exec_version);
		}
		
		//error_log($s_dataname." current sync version: " .$i_currentversion." (".__file__.")");
	
		//mlavu_query("DELETE FROM `poslavu_[1]_db`.`config` WHERE `setting`='local_sync_version' AND `location`='[2]'", $s_dataname, $i_locationid);
		//mlavu_query("INSERT INTO `poslavu_[1]_db`.`config` (`location`,`setting`,`value`) VALUES ('[2]','local_sync_version','[3]')", $s_dataname, $i_locationid,$i_currentversion);
		$exists_query = mlavu_query("SELECT `setting` FROM `poslavu_[1]_db`.`config` WHERE `setting`='local_sync_version' AND `location`='[2]'", $s_dataname, $i_locationid);
		$exists = FALSE;
		if ($exists_query)
			if (mysqli_num_rows($exists_query))
				$exists = TRUE;
		if (!$exists) {
			mlavu_query("DELETE FROM `poslavu_[1]_db`.`config` WHERE `setting`='local_sync_version' AND `location`='[2]'", $s_dataname, $i_locationid);
			mlavu_query("INSERT INTO `poslavu_[1]_db`.`config` (`location`,`setting`,`value`) VALUES ('[2]','local_sync_version','[3]')", $s_dataname, $i_locationid,$i_currentversion);
		} else {
			mlavu_query("UPDATE `poslavu_[1]_db`.`config` SET `value`='[3]' WHERE `setting`='local_sync_version' AND `location`='[2]'", $s_dataname, $i_locationid,$i_currentversion);
		}
		mlavu_query("UPDATE `poslavu_MAIN_db`.`local_servers` SET `sync_version`='[sync_version]' WHERE `data_name`='[dataname]'",
			array('sync_version'=>$i_currentversion, 'dataname'=>$s_dataname));
	}
	
	function get_and_request_sync_version($s_dataname) {
		mlavu_query("DELETE FROM `[database]`.`config` WHERE `setting`='last sync finished' AND `type`='log'",
						array('database'=>'poslavu_'.$s_dataname.'_db'));
		$query = mlavu_query("SELECT `sync_version` FROM `poslavu_MAIN_db`.`local_servers` WHERE `data_name`='[dataname]'",
			array('dataname'=>$s_dataname));
		if ($query === FALSE || mysqli_num_rows($query) === 0)
			return -1;
		$read = mysqli_fetch_assoc($query);
		return (int)$read['sync_version'];
	}
	
	function get_recorded_server_connection($s_dataname) {
		$query = mlavu_query("SELECT `server` FROM `poslavu_MAIN_db`.`local_servers` WHERE `data_name`='[dataname]'",
			array('dataname'=>$s_dataname));
		if ($query === FALSE || mysqli_num_rows($query) === 0)
			return NULL;
		$read = mysqli_fetch_assoc($query);
		return $read['server'];
	}
	
	if (isset($_POST['cmd']) && $_POST['cmd'] == 'request_sync_version' && isset($_POST['file']) && $_POST['file'] == 'getSyncVersion' && isset($_POST['dn'])) {
		$s_dataname = $_POST['dn'];
		require_once('/home/poslavu/public_html/admin/cp/resources/lavuquery.php');
		
		$i_sync_version = get_and_request_sync_version($s_dataname);
		if ($i_sync_version == -1) {
			echo "no recorded sync version for this dataname\n";
		} else {
			echo "most recent recorded version: \"$i_sync_version\"\n";
		}
		
		$s_server_connect = get_recorded_server_connection($s_dataname);
		if ($s_server_connect === NULL) {
			echo "\nno recorded server connection for this dataname\n";
		} else {
			echo "\nmost recent recorded server: \"$s_server_connect\" \n";
		}
		
		echo "\ncheck again after next sync\n";
	}
?>