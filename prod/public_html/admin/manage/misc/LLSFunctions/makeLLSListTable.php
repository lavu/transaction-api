<?php
	if (!isset($maindb) || $maindb == "")
		$maindb = "poslavu_MAIN_db";
	require_once("/home/poslavu/public_html/admin/cp/resources/lavuquery.php");
	
	function make_table($s_table_tag, $s_title_row, $s_sort_by, $s_return_pattern) {
		global $maindb;
		$i_low_color = 220;
		$i_high_color = 250;
		
		$s_table = $s_table_tag.str_replace("<tr>", "<tr id='lls_list_th_reference_for_widths'>", str_replace("<th style='", "<th style='visibility:hidden;height:1px;line-height:1px;", $s_title_row));
			
		$i = 0;
		$i_color = $i_low_color;
		$a_local_servers_data = array();
		$s_query_string = "SELECT * FROM `$maindb`.`[1]` JOIN `$maindb`.`[2]` ON (`$maindb`.`[1]`.`data_name`=`$maindb`.`[2]`.`data_name`) WHERE `$maindb`.`[2]`.`disabled`='0' ORDER BY `$maindb`.`[2]`.`[3]` ASC";
		$lls_query = mlavu_query($s_query_string, "local_servers", "restaurants", $s_sort_by);
		/* debug stuff: */
		$stuff = array("local_servers", "restaurants", $sort_by);
		for ($j = 1; $j < 4; $j++) {
			$s_query_string = str_replace("[$j]", $stuff[$j-1], $s_query_string);
		}
		//error_log($s_query_string);
		while ($a_lls_read = mysqli_fetch_assoc($lls_query)) {
			$a_lls_read['last_checked_in'] = date("Y-m-d", strtotime($a_lls_read['last_checked_in']));
			$a_lls_read['<tr>'] = "<tr style='background-color:rgb($i_color,$i_color,$i_color);cursor:pointer;' class='lls_list' onmouseover='set_background(this,\"#bbf\");' onmouseout='set_background(this,\"rgb($i_color,$i_color,$i_color)\");' onclick='loading();populateResults(\"".$a_lls_read['data_name']."\");$(\"#company_search_button\").click();doneLoading();'>";
			$a_lls_read["<td>"] = "<td style='padding-left:5px;padding-right:5px;padding-top:3px;padding-bottom:3px;border-top:1px solid rgb(180,180,180);'>";
			
			$s_row = $s_return_pattern;
			foreach($a_lls_read as $k=>$v)
				$s_row = str_replace($k, $v, $s_row);
			$s_table .= $s_row;
			if ($i_color == $i_low_color)
				$i_color = $i_high_color;
			else
				$i_color = $i_low_color;
			$i++;
		}
		
		$s_table .= "</table>";
		return $s_table;
	}
	
	if (isset($_POST['lls_list_table_echo_table'])) {
		$s_table = make_table();
		echo $s_table;
	}
?>