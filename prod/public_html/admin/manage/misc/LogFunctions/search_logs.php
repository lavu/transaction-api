<?php

	ini_set('memory_limit','128M');
	if(!function_exists("resource_path"))
		require_once($_SERVER['DOCUMENT_ROOT']."/cp/resources/core_functions.php");

		

	function fetchLog($base_url, $dn, $vars) {

			$vars .= "&dn=$dn&lc=".lc_encode("cloud_connect:$dn:".date("Ymd"));

			//echo "<br><br>$vars<br><br>";

			$ch = curl_init();
			curl_setopt($ch, CURLOPT_URL, $base_url."/sa_cp/search_cloud_log.php");
			curl_setopt($ch, CURLOPT_POST, 1);
			curl_setopt($ch, CURLOPT_POSTFIELDS, $vars);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			$response = curl_exec($ch);
			curl_close ($ch);

			return $response;
		}

	function displayLogLine($is_line1, $log_line, $server, $datetime, $srch, $lsrch) {

			$br_before = "<br>";
			$br_after = "";
			$log_line = str_replace("<", "&lt;", $log_line);
			$log_line = str_replace("[--CR--]", "<br>", $log_line);
			if (substr($log_line, 0, 19)==$datetime && $server!="{ERROR}") {
				$log_line = $datetime." (<font color='#000066'>".$server."</font>) ".substr($log_line, 20);
				if (!$is_line1) $br_before = "<br><br><br>";
			}
			if ($server == "{ERROR}") {
				$br_before = "";
				$br_after = "<br>";
				$log_line = str_replace("PHP Fatal error:", "<font color='#CC3300'><b>PHP Fatal error:</b></font>", $log_line);
				$log_line = str_replace("PHP Parse error:", "<font color='#CC3300'><b>PHP Parse error:</b></font>", $log_line);
				$log_line = str_replace("PHP Warning:", "<font color='#FF0000'><b>PHP Warning:</b></font>", $log_line);
			}
			$log_line = str_replace($lsrch, "<span style='background-color:#FFFF66'><b>$lsrch</b></span>", $log_line);
			$log_line = str_replace("{SUCCEEDED}", "<font color='#0B2F00'>{SUCCEEDED}</font>", $log_line);
			if (strstr($log_line, "{FAILED}")) {
				$log_line = str_replace("{FAILED}", "<font color='#990000'><b>{FAILED}</b></font>", $log_line);
				$br_after .= "<br>";
			}
			echo $br_before.str_replace($srch, "<span style='background-color:#FFFF66'><b>$srch</b></span>", $log_line).$br_after;
		}

	/*function displayLogLine($is_line1, $log_line, $server, $datetime, $srch, $lsrch) {

		$br_before = "<br>";
		$br_after = "";
		$log_line = str_replace("<", "&lt;", $log_line);
		if (substr($log_line, 0, 19)==$datetime && $server!="{ERROR}") {
			$log_line = $datetime." (<font color='#000066'>".$server."</font>) ".substr($log_line, 20);
			if (!$is_line1) $br_before = "<br><br><br>";
		}
		if ($server == "{ERROR}") {
			$br_before = "";
			$br_after = "<br>";
			$log_line = str_replace("PHP Fatal error:", "<font color='#CC3300'><b>PHP Fatal error:</b></font>", $log_line);
			$log_line = str_replace("PHP Parse error:", "<font color='#CC3300'><b>PHP Parse error:</b></font>", $log_line);
			$log_line = str_replace("PHP Warning:", "<font color='#FF0000'><b>PHP Warning:</b></font>", $log_line);
		}
		$log_line = str_replace($lsrch, "<span style='background-color:#FFFF66'><b>$lsrch</b></span>", $log_line);
		$log_line = str_replace("{SUCCEEDED}", "<font color='#0B2F00'>{SUCCEEDED}</font>", $log_line);
		if (strstr($log_line, "{FAILED}")) {
			$log_line = str_replace("{FAILED}", "<font color='#990000'><b>{FAILED}</b></font>", $log_line);
			$br_after .= "<br>";
		}
		if (substr($log_line, 0, 8)=="Getvars:" || substr($log_line, 0, 9)=="Postvars:") $br_before .= "<br>";
		echo $br_before.str_replace($srch, "<span style='background-color:#FFFF66'><b>$srch</b></span>", $log_line).$br_after;
	}*/

	$rowid = $_POST["rowid"];

	echo "the row id is: ".$rowid;

	$log_type = (isset($_REQUEST['log_type']))?$_REQUEST['log_type']:"Admin Actions";
	$lls = (isset($_REQUEST['search_lls']))?$_REQUEST['search_lls']:"0";
	$lls_checked = "";
	if ($lls == "1") $lls_checked = " checked";
	$date = (isset($_REQUEST['date']))?$_REQUEST['date']:date("Y-m-d");
	$loc_id = (isset($_REQUEST['loc_id']))?$_REQUEST['loc_id']:"0";
	$search_for = (isset($_REQUEST['search_for']))?$_REQUEST['search_for']:"";

	$cust_query = mlavu_query("select * from `poslavu_MAIN_db`.`restaurants` where `id`='[1]'",$rowid);
	if(mysqli_num_rows($cust_query)) {
		$cust_read = mysqli_fetch_assoc($cust_query);

		echo "<style>";
		echo ".label { text-align:right; color:#999999; vertical-align:top; } ";
		echo "</style>";
		//echo "<br><a href='index.php?mode=manage_customers&submode=details&rowid=$rowid'><< Company Details</a><br><br>";

		echo "<table cellpadding=12><td style='border:solid 1px black' valign='top'>";

		echo "<div style='float:left; display:block;'>";
		echo "<table>";
		echo "<tr><td class='label label-info'>Company Name: </td><td width='200'>" . $cust_read['company_name'] . "</td><td class='label label-info'>Data Name: </td><td>" . $cust_read['data_name'] . "</td></tr>";
		echo "<tr><td>&nbsp;</td></tr>";

		//$log_types = array("Admin Actions", "Device Console", "Gateway", "Gateway Comm Vars", "Gateway Debug", "JSON Comm Vars", "MySQL", "Order Sum Corrections");
		$log_types = array("Admin Actions", "Apache Log (Universal)", "Device Console", "Gateway", "Gateway Comm Vars", "Gateway Debug", "JSON Comm Vars", "MySQL");
		$dates = array();
		for ($d = -1; $d < 90; $d++) {
			$dates[] = date("Y-m-d", time() - ($d * 86400));
		}

		$locations = array();
		$loc_id_lookup = array();
		lavu_connect_dn($cust_read['data_name'],'poslavu_'.$cust_read['data_name'].'_db');
		$loc_query = lavu_query("SELECT `id`, `title` FROM `locations` ORDER BY `title` ASC", $cust_read['data_name']);
		if (mysqli_num_rows($loc_query)) {
			while ($loc_read = mysqli_fetch_assoc($loc_query)) {
				$locations[] = array($loc_read['id'], $loc_read['title']);
				$loc_id_lookup[$loc_read['id']] = $loc_read['title'];
			}
		}

		echo "<tr>
			<td colspan='4'>
				<table>
					<tr>
						<td class='label label-info'>Log to search: </td>
						<td>
							<select name='log_type'>";
		foreach ($log_types as $log) {
			$selected = "";
			if ($log == $log_type) {
				$selected = " SELECTED";
			}
			echo "<option value='$log'$selected>$log</option>";
		}
		echo "		</select>
						</td>
						<td class='label label-info'>LLS: </td>
						<td>
							<input name='search_lls' type='checkbox' value='1'$lls_checked>
						</td>
						<td class='label label-info' style='padding-left:10px'>Date: </td>
						<td>
							<select name='date'>";
		$todays_date = false;
		for ($d = 0; $d < count($dates); $d++) {
			$selected = "";
			if ($date == $dates[$d]) {
				$selected = " SELECTED";
				if ($d == 0) $todays_date = true;
			}
			echo "<option value='".$dates[$d]."'$selected>".$dates[$d]."</option>";
		}
		echo "		</select>
						</td>
						</tr><tr>
						<td class='label label-info' style='padding-left:10px'>Location: </td>
						<td>
							<select name='loc_id'>
								<option value='0'>All Locations</option>";
		foreach ($locations as $l) {
			$selected = "";
			if ($loc_id == $l[0]) {
				$selected = " SELECTED";
			}
			echo "<option value='".$l[0]."'$selected>".$l[1]."</option>";
		}
		echo "		</select>
						</td>
						<td class='label label-info' style='padding-left:10px'>Search string: </td>
						<td><input type='text' name='search_for' size='30' value='$search_for'></td>
						<td style='padding-left:10px'><input type='button' value='Search' onclick ='searchLogs($(\"[name=log_type]\").val(), $(\"[name=search_lls]\").prop(\"checked\") , $(\"[name=date]\").val(), $(\"[name=loc_id]\").val(),  $(\"[name=search_for]\").val(), \"$rowid\" )'>
</td>
					</tr>
				</table>
			</td>
		</tr>";
		echo "</table>";


		echo "<span style='font-size: 11px; color:#666666;'>(LLS currently searchable only when LSVPN is turned on)</span>";

		echo "</div>";
		echo "</table>";

		echo "<div style='float:left; padding-left:10px; width:900px;'>";
		echo "<br><br>";

		$servers = array();
		$servers[] = array("WEB1", "http://admin.poslavu.com");
		$servers[] = array("CLOUD1", "http://10.182.103.248");
		$servers[] = array("CLOUD2", "http://10.183.67.38");

		switch ($log_type) {
			case "Apache Log (Universal)" : $type = "error"; break;
			case "Device Console" : $type = "device_console"; break;
			case "Gateway" : $type = "gateway"; break;
			case "Gateway Comm Vars" : $type = "gateway_vars"; break;
			case "Gateway Debug" : $type = "gateway_debug"; break;
			case "JSON Comm Vars" : $type = "json_log"; break;
			case "MySQL" : $type = "mysql"; break;
		}

		if (!empty($_REQUEST['dcf'])) {

			$url = "";
			foreach ($servers as $server) {
				if ($server[0] == $_REQUEST['dcs']) {
					$url = $server[1];
					break;
				}
			}

			if ($url != "") $response = fetchLog($url, $cust_read['data_name'], "t=$type&dcf=".$_REQUEST['dcf']);
			else $response = "Error: unable to determine server URL...";

			echo $response."<br><br>";

		} else if ($log_type == "Admin Actions") {

			if ($lls == "1") echo "Admin Actions log not available for LLS search...<br><br>";
			else {

				$filter = "`server_time` >= '$date 00:00:00' AND `server_time` <= '$date 23:59:59'";
				if ($loc_id != 0) $filter .= " AND `loc_id` = '$loc_id'";
				if ($search_for != "") $filter .= " AND (`action` LIKE '%".$search_for."%' OR `user` LIKE '%".$search_for."%' OR `data` LIKE '%".$search_for."%' OR `detail1` LIKE '%".$search_for."%' OR `detail2` LIKE '%".$search_for."%' OR `detail3` LIKE '%".$search_for."%')";
				lavu_connect_dn( $cust_read['data_name'],'poslavu_'.$cust_read['data_name'].'_db');
				$get_actions = lavu_query("SELECT * FROM `admin_action_log` WHERE $filter ORDER BY `server_time` ASC", $cust_read['data_name']);
				if (@mysqli_num_rows($get_actions) > 0) {

					echo "<br><table cellspacing=0 cellpadding=5 style='border:solid 1px black'>";
					echo "<tr bgcolor='#EDEDED'>";
					echo "<td align='center' valign='bottom'>Server Time</td>";
					echo "<td align='center' valign='bottom'>Local Time</td>";
					echo "<td align='center' valign='bottom'>Location</td>";
					echo "<td align='center' valign='bottom'>Action</td>";
					echo "<td align='center' valign='bottom'>User</td>";
					echo "<td align='center' valign='bottom'>User ID</td>";
					echo "<td align='center' valign='bottom'>IP Address</td>";
					echo "<td align='center' valign='bottom'>Order ID</td>";
					echo "<td align='center' valign='bottom' colspan='4'>Details</td>";
					echo "</tr>";

					while($log_info = mysqli_fetch_assoc($get_actions)) {
						echo "<tr>";
						echo "<td valign='top'>".$log_info['server_time']."</td>";
						echo "<td valign='top'>".$log_info['time']."</td>";
						echo "<td valign='top'>".$loc_id_lookup[$log_info['loc_id']]."</td>";
						echo "<td valign='top'>".str_replace($search_for, "<span style='background-color:#FFFF66'><b>$search_for</b></span>", $log_info['action'])."</td>";
						echo "<td valign='top'>".str_replace($search_for, "<span style='background-color:#FFFF66'><b>$search_for</b></span>", $log_info['user'])."</td>";
						echo "<td valign='top'>".$log_info['user_id']."</td>";
						echo "<td valign='top'>".$log_info['ipaddress']."</td>";
						echo "<td valign='top'>".$log_info['order_id']."</td>";
						echo "<td valign='top'>".str_replace($search_for, "<span style='background-color:#FFFF66'><b>$search_for</b></span>", $log_info['data'])."</td>";
						echo "<td valign='top'>".str_replace($search_for, "<span style='background-color:#FFFF66'><b>$search_for</b></span>", $log_info['detail1'])."</td>";
						echo "<td valign='top'>".str_replace($search_for, "<span style='background-color:#FFFF66'><b>$search_for</b></span>", $log_info['detail2'])."</td>";
						echo "<td valign='top'>".str_replace($search_for, "<span style='background-color:#FFFF66'><b>$search_for</b></span>", $log_info['detail3'])."</td>";
						echo "</tr>";
					}
					echo "</table><br><br>";

				} else {
					echo "<br><br>No log entries found matching your search criteria...<br>";
				}
			}

		} else {

			if ($lls == "1") {

				$url = "http://poslavu.com/lsvpn/local/search_logs.php";
				$vars = "cc=".$cust_read['data_name']."&search=".$search_for."&type=".$type."&date=".$date;

				echo "<iframe src='".$url."?".$vars."' frameborder='1' width='900px' height='1000px'></iframe><br><br>";

			} else {
				$errors = array();
				$results = array();
				foreach ($servers as $server) {
					$response = fetchLog($server[1], $cust_read['data_name'], "t=$type&d=$date&s=$search_for&l=$loc_id");
					if (substr($response, 0, 6) == "Error:"){
						$errors[$server[0]] = $response;
					}else if (($response!=1 && $response!="") || strlen($response) > 1) {
						$results[$server[0]] = explode("\n", $response);
					}
				}

				if (count($results) > 0) {
					$r_keys = array_keys($results);

					if ($log_type == "Apache Log (Universal)") {
						echo "<input type='button' onclick='clearTimeout(autoSubmitTimer); this.style.display = \"none\";' value='Cancel Auto Refresh'><br><br>";

						$criticals = "";

						foreach ($r_keys as $key) {
							echo "<br><br><b>$key</b> - ".$results[$key][0]."<br><br>";
							$line1 = true;
							$has_matches = false;
							for ($i = 1; $i < count($results[$key]); $i++) {
								$log_line = $results[$key][$i];
								if ($log_line!="" && $log_line!=" "){
									$has_matches = true;
								}
								if (strstr($log_line, "PHP Fatal error:") || strstr($log_line, "PHP Parse error:")) {
									if ($criticals != "") $criticals .= ", ";
									$criticals .= $key." line ".($i/2);
								}
								if ($i%2==0 && $log_line!="" && $log_line!=" ") echo "<font style='color:#888888;'><b>[".($i/2)."]</b></font> ";
								displayLogLine($line1, $log_line, "{ERROR}", "", $search_for, "");
								$line1 = false;

							}
							if (!$has_matches){
								echo "No lines match search criteria...<br><br><br>";
							}
						}
						if ($criticals != "") {
							echo "<embed src='sounds/somethingwrong.wav' autostart='true' width='0' height='0' id='alarm' enablejavascript='true'>";
							echo "<script language='javascript'>setTimeout(function() { alert('Critical Errors Detected: $criticals'); }, 1000);</script>";
						}
						echo "<script language='javascript'>autoSubmitTimer = setTimeout(function() { document.form1.submit(); }, 10000);</script>";
					} else if ($log_type == "Device Console") {
						foreach ($r_keys as $key) {
							echo "<br><br><b>$key</b><br><br>";
							for ($i = 0; $i < count($results[$key]); $i++) {
								if ($results[$key][$i] != "") {
									$dc_info = explode("|***|", $results[$key][$i]);
									if (strstr($dc_info[0], "No device console logs found")) echo $dc_info[0]."<br>";
									else if ($dc_info[0] != "") echo "<a style='cursor:pointer; color:#0033CC;' onclick='window.location = \"?mode=manage_customers&submode=$submode&rowid=$rowid&log_type=Device%20Console&date=$date&dcs=".$key."&dcf=".$dc_info[0]."\"'>".$dc_info[0]."</a> - <b>".$dc_info[1]."</b> to <b>".$dc_info[2]."</b> - ".$dc_info[3]."<br>";
								}
							}
						}
					} else if ($log_type == "Gateway Debug") {
						foreach ($r_keys as $key) {
							echo "<br><br><b>$key</b><br><br>";
							for ($i = 0; $i < count($results[$key]); $i++) {
								if (strstr($results[$key][$i], "No gateway debug logs found")) echo $results[$key][$i]."<br>";
								else if ($results[$key][$i] != "") echo "<a style='cursor:pointer; color:#0033CC;' onclick='window.location = \"?mode=manage_customers&submode=$submode&rowid=$rowid&log_type=Gateway%20Debug&date=$date&dcs=".$key."&dcf=".$results[$key][$i]."\"'>".$results[$key][$i]."</a><br>";
							}
						}
					} else {
						$by_the_second = array();
						foreach ($r_keys as $key) {
							$last_datetime = $date." 00:00:00";
							for ($i = 0; $i < count($results[$key]); $i++) {
								$log_line = $results[$key][$i];
								if ($log_line != "") {
									if (substr($log_line, 0, 10) == $date) {
										$datetime = substr($log_line, 0, 19);
										$last_datetime = $datetime;
									} else $datetime = $last_datetime;
									if (!isset($by_the_second[$datetime])) $by_the_second[$datetime] = array();
									$by_the_second[$datetime][] = array($key, $i);
								}
							}
						}
						ksort($by_the_second);
						$line1 = true;
						$seconds = array_keys($by_the_second);
						foreach ($seconds as $second) {
							foreach ($by_the_second[$second] as $ll) {
								$log_line = $results[$ll[0]][$ll[1]];
								displayLogLine($line1, $log_line, $ll[0], $second, $search_for, $search_for_loc);
								$line1 = false;
							}
						}
					}
				} else {
					echo "No log entries found matching search criteria...<br>";
				}
			}
		}
		echo "<br><br><input type='button' onclick='document.form1.submit()' value='Refresh'></a><br><br>";
		echo "</div>";
	}
?>
