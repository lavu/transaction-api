<?php
	if(!function_exists("resource_path"))
		require_once(dirname(__FILE__)."/../cp/resources/core_functions.php");

	
	ini_set('memory_limit','64M');

	$valid_ips = array();
	$valid_ips[] = "192.168.100.226";
	$valid_ips[] = "192.168.101.224";
	$valid_ips[] = "10.182.103.248";
	$valid_ips[] = "10.183.67.38";
	
	//echo "<br><br>IP ".$_SERVER['REMOTE_ADDR']."<br><br>";

	$lc = explode(":", lc_decode($_REQUEST['lc']));
	if ($lc[0]=="cloud_connect" && $lc[1]==$_REQUEST['dn'] && $lc[2]==date("Ymd") && in_array($_SERVER['REMOTE_ADDR'], $valid_ips)) {
	
		$dataname = (isset($_REQUEST['dn']))?$_REQUEST['dn']:false;
		$type = (isset($_REQUEST['t']))?$_REQUEST['t']:false;
		$date = (isset($_REQUEST['d']))?$_REQUEST['d']:false;
		$search_for = (isset($_REQUEST['s']))?$_REQUEST['s']:"";
		$loc_id = (isset($_REQUEST['l']))?$_REQUEST['l']:"0";
		$dcf = (!empty($_REQUEST['dcf']))?$_REQUEST['dcf']:false;

		if ($dataname && $type && ($date || (($type=="device_console" || $type=="gateway_debug") && $dcf))) {

			$path = "/home/poslavu/logs/company/".$dataname;		
			if ($type == "gateway_debug") $path .= "/gateway_debug";
	
			if ($type == "error") {
			
				$lines = file("/home/poslavu/logs/error_log_tail");
				foreach ($lines as $line) {
					if ($search_for=="" || strstr($line, $search_for) || substr($line, 0, 21)=="ERROR WATCH LAST RAN:") echo $line."\n";
				}
				
			} else if ($dcf) {
			
				if (file_exists($path."/".$dcf)) {

					echo "<a style='cursor:pointer; color:#0033CC;' onclick='history.go(-1);'><< Back to list</a><br><br>";

					$log_file = fopen($path."/".$dcf, "r");
					while (($log_line = fgets($log_file, 4096)) !== false) {
						echo nl2br(str_replace("<", "&lt;", $log_line));
					}

					if (!feof($log_file)) echo "Error: unexpected fgets() fail\n";
					fclose($log_file);
					
					echo "<br><br>";
				
				} else {
				
					echo "Error: cannot find ".$dcf."<br><br>";
				}
				
				echo "<a style='cursor:pointer; color:#0033CC;' onclick='history.go(-1);'><< Back to list</a><br><br>";
				
			} else if ($type == "device_console") {
			
				if ($dir = opendir($path)) {
	
					$found_match = false;

					$t = 0;
					$f = 0;
					$file_list = array();
					$file_list['name'] = array();
					$file_list['time'] = array();

					while (false !== ($file = readdir($dir))) {
						if (strstr($file, "device_console") && strstr($file, str_replace("-", "", $date))) {
							$found_match = true;
							$file_list['time'][$t++] = filemtime($path."/".$file);
							$file_list['name'][$f++] = $file;
						}
					}
			
					closedir($dir);
					asort($file_list['time']);
					asort($file_list['name']);
						
					if ($found_match) {
					
						foreach ($file_list['time'] as $key=>$ftime) {
		
							$filename = $file_list['name'][$key];
							//$File = fopen($path."/".$file, "r");
							$File = escapeshellarg($path."/".$filename);
							$header = `head -5 $File`;
							$first_line = `head -7 $File | tail -n 1`;
							$last_line = `tail -n 1 $File`;
							
							$first_time = substr($first_line, 0, 19);
							$last_time = substr($last_line, 0, 19);
							
							echo $filename."|***|".$first_time."|***|".$last_time."|***|".trim(str_replace("\n", " &nbsp;&nbsp; ", $header), " &nbsp;")."\n";
	
							//echo "<a style='cursor:pointer; color:#0033CC;' onclick='window.location = \"?mode=manage_customers&submode=$submode&rowid=$rowid&log_type=$search_log&date=$date&dcf=$filename\"'>".$filename."</a> - <b>$first_time</b> to <b>$last_time</b> - ";
							//echo trim(str_replace("\n", " &nbsp;&nbsp; ", $header), " &nbsp;")."<br>";
						}

					} else echo "No device console logs found for ".$date."...";
	
				} else echo "Error: unable to open directory (".$dataname.")...";
				
			} else if ($type == "gateway_debug") {
				
				if ($dir = opendir($path)) {
		
					$found_match = false;

					$file_list = array();
					
					$search_for_loc = "";
					if ($loc_id != 0) $search_for_loc = $loc_id."_";
					
					$test_list = array();

					while (false !== ($file = readdir($dir))) {
						$test_list[] = $file;
						$test_list[] = array($date, strstr($file, str_replace("-", "", $date)), $loc_id, $search_for_loc, (substr($file, 0, (strlen((string)$loc_id) + 1))==$search_for_loc));
						if (strstr($file, str_replace("-", "", $date)) && ($loc_id=="0" || substr($file, 0, (strlen((string)$loc_id) + 1))==$search_for_loc)) {
							$found_match = true;
							$file_list[] = $file;
						}
					}
					
					sort($file_list);
					closedir($dir);

					if ($found_match) {
				
						for ($f = (count($file_list) - 1); $f >= 0; $f--) {
							echo $file_list[$f]."\n";
						}

					} else echo "No gateway debug logs found for ".$date."...";
	
				} else {
				
					echo "Error: unable to open directory (".$dataname.")...";
					if (!is_dir($path)) mkdir($path, 0755);
				}
					
			} else {
			
				$search_for_loc = "";
				if ($loc_id != 0) $search_for_loc = "LOCATION: ".$loc_id;
				
				$ext = "";
				if ($type == "gateway" || $type == "mysql" || $type == "auto_correct") $ext = ".log";
			
				$path = "/home/poslavu/logs/company/".$dataname."/".$type."_".$date.$ext;
				if (!file_exists($path) && $_SERVER['REMOTE_ADDR']=="192.168.101.224") $path = "/mnt/poslavu-files/logs/company/".$dataname."/".$type."_".$date.$ext;
				
				if (file_exists($path)) {
					
					$temp_string = "";
					
					$log_file = fopen($path, "r");
					while (($log_line = fgets($log_file, 4096)) !== false) {

						if ((($search_for == "") || strstr($log_line, $search_for)) && (($search_for_loc == "") || strstr($log_line, $search_for_loc))) $temp_string .= $log_line;
					
						/*$skip_completion = false;
					
						$pre_temp_string = "";			
						if ($type=="gateway") {
							$pre_temp_string .= $log_line;
							$log_line = fgets($log_file, 4096);						
						}
						
						if ($type=="json_log" && strstr($log_line, "Request from") && strstr($log_line, $date)) {
							$next_line = fgets($log_file, 4096);
							$log_line .= $next_line;
							if (substr($next_line, 0, 8) == "Getvars:") {
								$next_line = fgets($log_file, 4096);
								if (substr($next_line, 0, 9) == "Postvars:") $log_line .= $next_line;
								else $skip_completion = true;
							}
						}
					
						if ((($search_for == "") || strstr($log_line, $search_for)) && (($search_for_loc == "") || strstr($log_line, $search_for_loc))) {
							$temp_string .= $pre_temp_string.$log_line;
							if (!$skip_completion) {
								while (($log_line = fgets($log_file, 4096)) !== false) {
									if ($log_line == "\n") {
										break;
									} else {
										$temp_string .= $log_line;
									}
								}
							}
						}*/
					}
					
					if (!feof($log_file)) echo "Error: unexpected fgets() fail";
					else echo $temp_string;
					
					fclose($log_file);
		
				} else echo "Error: file not found...";
			}
			
		} else echo "Error: incomplete request...";
	
	} else echo "Error: request validation failed...";

?>
