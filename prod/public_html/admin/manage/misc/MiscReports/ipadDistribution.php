<?php

	/******
	
	This is a simple reporting file that pulls the number of ipads people are currently using. 
	
	if they have been active in the last 5 days
	
	******/
	ini_set('error_reporting', 1);
	error_reporting(1);	
	session_start();
	$silver=0;
	$gold=0;
	$platinum=0;
	$noIpad=0;
	$localServers=0;
	require_once("/home/poslavu/public_html/admin/cp/resources/lavuquery.php");
	$restQuery = mlavu_query("SELECT `data_name` FROM `poslavu_MAIN_db`.`restaurants` WHERE DATE_ADD(`last_activity`, INTERVAL 5 DAY) >= NOW()  ");
	while($dataname= mysqli_fetch_assoc($restQuery)){
		
		/*
		$localServerQuery= mlavu_query("SELECT * FROM `poslavu_[1]_db`.`locations` where `net_path` LIKE '%poslavu%' AND `net_path` != ''", $dataname['data_name']) ;
		if(!mysqli_num_rows($localServerQuery)){
			$localServers++;
			continue;
		}
		*/	 

		$rowQuery= mlavu_query("select * from `poslavu_[1]_db`.`devices` where `model` like '%iPad%' AND DATE_ADD(`last_checkin`, INTERVAL 2 DAY) >= NOW() ", $dataname['data_name']);
		$numRows= mysqli_num_rows($rowQuery);
		
		if($numRows==1)
			$silver++;
		else if( $numRows==2)
			$gold++;
		else if( $numRows >=3)
			$platinum++;
		else
			$noIpad++;
			
	}

	echo "The number of restaurants with one iPad: ". $silver."<br/>";
	echo "The number of restaurants with two iPads: ". $gold."<br/>";
	echo "The number of restaurants with three or more iPads: ". $platinum."<br/>";
	echo "The number of restaurants with zero iPads: ". $noIpad."<br/>";
	//echo "the number of local servers are: ". $localServers."<br>";
	


?>