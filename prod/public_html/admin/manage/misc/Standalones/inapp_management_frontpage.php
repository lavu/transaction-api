<?php

require_once(dirname(__FILE__).'/../../login.php');

require_once("/home/poslavu/public_html/admin/cp/resources/lavuquery.php");
require_once("/home/poslavu/public_html/admin/cp/resources/core_functions.php");
require_once("/home/poslavu/public_html/admin/cp/objects/json_decode.php");
require_once("/home/poslavu/public_html/admin/sa_cp/billing/package_levels_object.php");

// get the environment set up
session_start();
$urlPathBase = $_SERVER['SERVER_NAME'] == 'localhost' ? 'http://localhost:'.$_SERVER['SERVER_PORT']."/" : 'https://admin.poslavu.com';
if (!account_loggedin())
	header("Location: $urlPathBase/manage/");

if (isset($_GET['frontent_reports']) && $_GET['frontend_reports'] == '0') {
	unset($_SESSION['frontend_reports']);
}

if ((isset($_GET['frontend_reports']) && $_GET['frontend_reports'] == '1') || (isset($_SESSION['frontend_reports']) && $_SESSION['frontend_reports'] == '1' && isset($_GET['mode']))) {
	$a_getvars = $_GET;
	unset($a_getvars['frontend_reports']);
	echo InnappManagement::frontend_reports($a_getvars);
} else {
	unset($_SESSION['frontend_reports']);
	if (!isset($_GET['dont_draw_inapp_management'])) {
		echo InnappManagement::options_tostring();
		echo InnappManagement::iframe_tostring();
	} else {
		if (isset($_GET['verify_getvars'])) echo InnappManagement::verify_getvars();
	}
}

class InnappManagement {

	// returns the get/post vars as an array(getkey=>array('value'=>getvalue, 'type'=>string/int/float/hidden, 'printed_name'=>name to display to user), ...)
	public static function  get_inapp_vars() {

		if (!function_exists('update_select_settings')) {
			function update_select_settings(&$a_vars, $s_index, &$a_vals, $s_valkey, $s_valprint) {
				if (count($a_vals) > 0) {
					$b_found = FALSE;
					$a_vars[$s_index]['vals'] = array();
					foreach($a_vals as $a_val) {
						$a_vars[$s_index]['vals'][$a_val[$s_valkey]] = $a_val[$s_valprint];
						if ($a_val[$s_valkey] == $a_vars[$s_index]['value'])
							$b_found = TRUE;
					}
					if (!$b_found)
						$a_vars[$s_index]['value'] = $a_vals[0][$s_valkey];
				}
			}
		}

		// set the defaults
		$a_types = array(
			'cc'=>array('type'=>'string', 'value'=>'17edison_key_kf0MbILrMnjbabYzEtVI', 'printed_name'=>'Dataname and Key'),
			'loc_id'=>array('type'=>'select', 'value'=>'1', 'vals'=>array('1'=>'Main'), 'printed_name'=>'Location'),
			'server_id'=>array('type'=>'hidden', 'value'=>'1', 'printed_name'=>'User Id'),
			'username'=>array('type'=>'select', 'value'=>'17edison', 'vals'=>array('17edison'=>'17edison (4)'), 'printed_name'=>'Username'),
			'register'=>array('type'=>'string', 'value'=>'receipt', 'printed_name'=>'Register Type'),
			'register_name'=>array('type'=>'string', 'value'=>'Main', 'printed_name'=>'Register Name'),
			'device_time'=>array('type'=>'string', 'value'=>date('Y-m-d H:i:s'), 'printed_name'=>'Device Time'),
			'date'=>array('type'=>'string', 'value'=>date('Y-m-d H:i:s'), 'printed_name'=>'Date'),
			'poslavu_version'=>array('type'=>'string', 'value'=>'8.8.8', 'printed_name'=>'App Version'),
			'lavu_lite'=>array('type'=>'select', 'value'=>'0', 'vals'=>array('0'=>'No', '1'=>'Yes'), 'printed_name'=>'Is Lavu Lite'),
			'MAC'=>array('type'=>'string', 'value'=>'74:E1:B6:71:9B:EF', 'printed_name'=>'MAC'),
			'UUID'=>array('type'=>'string', 'value'=>'C48D5B28-0EAE-4BE4-9D9A-671FE358F89E', 'printed_name'=>'UUID'),
			'access_level'=>array('type'=>'select', 'value'=>'4', 'vals'=>array('1'=>'1','2'=>'2','3'=>'3','4'=>'4'), 'printed_name'=>'Access Level (1-4)'),
			'available_DOcmds'=>array('type'=>'string', 'value'=>'alert,confirm,open_drawer,print,startListeningForSwipe,stopListeningForSwipe', 'printed_name'=>'available_DOcmds')
		);

		// replace default values with values from the getvars
		$a_vars = array();
		foreach($_GET as $k=>$v) {
			if ($k == 'dev')
				continue;
			if (isset($a_types[$k])) {
				$a_vars[$k] = array_merge($a_types[$k], array('value'=>$v));
			} else {
				$a_vars[$k] = array('value'=>$v, 'type'=>'string', 'printed_name'=>$k);
			}
		}
		foreach($a_types as $k=>$v) {
			if (!isset($a_vars[$k])) {
				$a_vars[$k] = $v;
			}
		}

		// get the dataname
		$s_dataname = substr($a_vars['cc']['value'], 0, strpos($a_vars['cc']['value'], '_key_'));
		$s_databasename = 'poslavu_'.$s_dataname.'_db';

		// set the locations
		$a_locations = ConnectionHub::getConn('poslavu')->getDBAPI()->getAllInTable('locations', array('_disabled'=>0), TRUE, array('databasename'=>$s_databasename, 'selectclause'=>'`id`,`title`'));
		update_select_settings($a_vars, 'loc_id', $a_locations, 'id', 'title');

		// set the users
		$a_users = ConnectionHub::getConn('poslavu')->getDBAPI()->getAllInTable('users', array('_deleted'=>'0', 'active'=>'1'), TRUE, array('databasename'=>$s_databasename, 'selectclause'=>"`username`,CONCAT(`username`,' (',`access_level`,')') AS `print`"));
		update_select_settings($a_vars, 'username', $a_users, 'username', 'print');

		return $a_vars;
	}

	// draw user options that appear at the top of the page
	public static function options_tostring() {

		// get the option vars
		$a_vars = self::get_inapp_vars();

		// get the dataname
		$s_dataname = substr($_GET['cc'], 0, strpos($_GET['cc'], '_key_'));
		$a_restaurants = ConnectionHub::getConn('poslavu')->getDBAPI()->getAllInTable('restaurants', array('data_name'=>$s_dataname), TRUE);
		$s_restaurant_id = $a_restaurants[0]['id'];

		// draw the variables
		$s_vars = "
			<script type='text/javascript'>
				inapp_vars = {";
		$s_comma = "";
		foreach($a_vars as $k=>$v) {
			$k = str_replace(array("'", '"'), '', $k);
			$v['value'] = str_replace(array("'", '"'), '', $v['value']);
			if ($v['type'] == 'string' || $v['type'] == 'select')
				$v['value'] = "'".$v['value']."'";
			$s_vars .= "{$s_comma}
					'{$k}': ".$v['value'];
			$s_comma = ",";
		}
		$dev = (isset($_GET['dev']) ? 'dev/' : '');
		$getvars_dev = (isset($_GET['dev']) ? '&dev' : '');
		$s_vars .= "
				}
				window.getvars = '';
				function verify_getvars(getvars) {
					$.ajax({
						url: './inapp_management_frontpage.php?dont_draw_inapp_management&verify_getvars',
						async: false,
						cache: false,
						method: 'POST',
						data: { getvars: getvars },
						success: function(message) {
							window.getvars = message;
						},
						error: function(a,b,c) {
							alert('error communicating with server');
						}
					});
					return window.getvars;
				}
				function reload_iframe() {
					var getvars = '';
					var jinputs = $.merge(
						$('#inapp_vars_form').find('input'),
						$('#inapp_vars_form').find('select')
					);
					var first = true;
					$.each(jinputs, function(k,v) {
						v = $(v);
						if (first) {
							getvars += '?'+v.attr('name')+'='+v.val();
						} else {
							getvars += '&'+v.attr('name')+'='+v.val();
						}
						first = false;
					});
					getvars = verify_getvars(getvars);
					var frameToLoad = getFrameToLoad();
					if (frameToLoad == 'management_area') {
						load_management_area(getvars);
					} else {
						load_frontend_reports(getvars);
					}
				}
				function getFrameToLoad() {
					var jtabContainer = $('#iframe_tab_container');
					var jselected = jtabContainer.find('.iframe_tab.selected');
					if (jselected.length == 0) {
						jselected = $(jtabContainer.find('.iframe_tab')[0]);
						jselected.addClass('selected');
					}
					return jselected.attr('value');
				}
				function load_management_area(getvars) {
					var source = 'https://admin.poslavu.com/{$dev}lib/inapp_management.php';
					source += getvars;
					console.log(source);
					$('iframe').attr('src', source);
				}
				function load_frontend_reports(getvars) {
					var source = 'https://admin.poslavu.com/manage/misc/Standalones/inapp_management_frontpage.php';
					source += getvars;
					source += '&frontend_reports=1&mode=main_menu{$getvars_dev}';
					console.log(source);
					$('iframe').attr('src', source);
				}
				function selectTab(element) {
					var jtabContainer = $('#iframe_tab_container');
					jtabContainer.find('.iframe_tab').each(function(k,v) {
						$(v).removeClass('selected');
					});
					$(element).addClass('selected');
					reload_iframe();
				}
				setTimeout('reload_iframe();', 1000);
				setTimeout('reload_iframe();', 2000);
			</script>";

		// draw the inputs
		$s_inputs = "
			<form id='inapp_vars_form'>";
		foreach($a_vars as $k=>$v) {
			$s_inputs .= "
				";
			$k = str_replace(array("'", '"'), '', $k);
			$v['value'] = str_replace(array("'", '"'), '', $v['value']);
			if ($v['type'] == 'int') {
				$s_inputs .= $v['printed_name'].": <input type='number' name='{$k}' value='".$v['value']."' /><br />";
			} else if ($v['type'] == 'hidden') {
				$s_inputs .= "<input type='hidden' name='{$k}' value='".$v['value']."' />";
			} else if ($v['type'] == 'select') {
				$s_inputs .= $v['printed_name']." <select name='{$k}'>";
				foreach($v['vals'] as $s_value=>$s_print) {
					if ($s_value == $v['value'])
						$s_inputs .= "<option value='{$s_value}' SELECTED>{$s_print}</option>";
					else
						$s_inputs .= "<option value='{$s_value}'>{$s_print}</option>";
				}
				$s_inputs .= "</select><br />";
			} else {
				$s_inputs .= $v['printed_name'].": <input type='text' name='{$k}' value='".$v['value']."' /><br />";
			}
		}
		$s_unset_link = can_access('all') ? " <a href='/manage/misc/MainFunctions/custLogin.php?site=new&mode=login&custID={$s_restaurant_id}&custCode={$s_dataname}&unset_sessvar=isLLS' target='_blank'>Unset isLLS (opens CP)</a>" : "";
		$s_inputs .= "
				<input type='button' name='view_inapp_management' value='submit' onclick='reload_iframe();' />{$s_unset_link}<br />
			</form>";

		// return the string
		return $s_vars.$s_inputs;
	}

	public static function iframe_tostring() {
		$dev = (isset($_GET['dev']) ? 'dev/' : '');
		$s_iframe_source = str_replace('/manage/misc/Standalones/inapp_management_frontpage.php', 'https://admin.poslavu.com/'.$dev.'lib/inapp_management.php', $_SERVER['REQUEST_URI']);
		$s_retval = "
			<br />
			<style type='text/css'>
				.iframe_container {
					background-color: #eee;
					border: 1px solid #ddd;
					border-radius: 8px;
					padding: 8px;
					display: table;
				}
				.iframe_tab {
					width: 200px;
					float: left;
					cursor: pointer;
					background-color: #666;
					border: 1px solid #333;
					padding: 5px 10px;
					border-radius: 5px 5px 0 0;
					border-bottom: 0;
					height: 17px;
				}
				.iframe_tab:hover {
					background-color: white;
				}
				.iframe_tab.selected {
					background-color: white;
				}
				.iframe_tab span {
					transform: rotateX(-21.5deg);
					-webkit-transform: rotateX(-21.5deg);
				}
			</style>
			<div class='iframe_container'>
				<div id='iframe_tab_container'>
					<div class='iframe_tab' value='management_area' onclick='selectTab(this);'><span>Management Area</span></div>
					<div class='iframe_tab' value='frontend_reports' onclick='selectTab(this);'><span>Frontend Reports</span></div>
				</div>
				<iframe src='' style='width:984px; height:670px; margin:0 auto; background-color: white;'></iframe>
			</div>
			<script type='text/javascript' src='/manage/js/jquery/js/jquery-1.9.0.js'></script>
			<script type='text/javascript'>
				setTimeout('resize_iframe();', 1000);
				function resize_iframe() {
					if (window.resize_iframe_timeout)
						clearTimeout(window.resize_iframe_timeout);
					resize_iframe2();
				}
				function resize_iframe2() {

					var jiframe = $('iframe');
					var iw = parseInt(jiframe.css('width'));
					var ih = parseInt(jiframe.css('height'));
					//console.log(iw+':'+ih);

					window.resize_iframe_timeout = setTimeout(function() {
						resize_iframe2();
					}, 1000);
				}
			</script>";
		return $s_retval;
	}

	public static function verify_getvars() {

		// get the getvars
		$s_getvars = substr($_POST['getvars'], 1);
		$a_getvars = explode('&', $s_getvars);

		// find the user id and change the respective getvar
		foreach($a_getvars as $k=>$v) {
			if (strpos($v, 'cc=') === 0)
				$i_dataname = $k;
			if (strpos($v, 'username=') === 0)
				$i_username = $k;
			if (strpos($v, 'server_id=') === 0)
				$i_server_id = $k;
		}
		$a_search = array('username'=>substr($a_getvars[$i_username],strlen('username=')));
		$s_dataname = substr($a_getvars[$i_dataname], strlen('cc='));
		$s_dataname = substr($s_dataname, 0, strpos($s_dataname, '_key_'));
		$s_databasename = 'poslavu_'.$s_dataname.'_db';
		$a_users = ConnectionHub::getConn('poslavu')->getDBAPI()->getAllInTable('users', $a_search, TRUE, array('databasename'=>$s_databasename));
		$a_getvars[$i_server_id] = 'server_id='.$a_users[0]['id'];

		// build the new getvars
		$s_getvars = '?'.implode('&', $a_getvars);

		// return the new getvars
		return $s_getvars;
	}

	public static function frontend_reports($a_getvars) {
		if (isset($a_getvars['mode'])) {
			$report_page = $a_getvars['mode'];
		} else {
			if (isset($_SESSION['inapp_management_frontpage_report_page'])) {
				$report_page = $_SESSION['inapp_management_frontpage_report_page'];
			} else {
				$report_page = "main_menu";
			}
		}
		$_SESSION['inapp_management_frontpage_report_page'] = $report_page;
		$_SESSION['frontend_reports'] = '1';
		$s_dataname = substr($a_getvars['cc'], 0, strpos($a_getvars['cc'], '_key_'));
		$dev = (isset($a_getvars['dev']) ? 'dev/' : '');

		$s_getvars = "";
		foreach($a_getvars as $k=>$v) {
			if ($k != 'mode') {
				$s_getvars .= "<input type='hidden' name='{$k}' value='{$v}'></input>";
			}
		}

		echo "<style type='text/css'>
			.ipad_select_button {
				background-color: #eee;
				border: none;
				border-radius: 20px;
				width: 200px;
				font-size: 18px;
				padding: 4px;
			}
		</style>";
		if ($report_page == "main_menu") {
			echo "<script type='text/javascript' src='/manage/js/jquery/js/jquery-1.9.0.js'></script>";
			echo "<form method='GET'>";
			echo $s_getvars;
			foreach(array('server_menu', 'register', 'x_report', 'z_report') as $s_new_mode) {
				echo "<input type='button' onclick='$(\"input[name=mode]\").val(\"{$s_new_mode}\"); $(\"form\").submit();' value='{$s_new_mode}' class='ipad_select_button'></input><br />";
			}
			echo "<input type='hidden' name='mode' value=''></input>";
			echo "</form>";
		} else if ($report_page == "server_menu") {
			$a_users = ConnectionHub::getConn('poslavu')->getDBAPI()->getAllInTable("users", array("active"=>"1", "_deleted"=>"0"), TRUE, array("selectclause"=>"`id`,`username`", "databasename"=>"poslavu_".$s_dataname."_db"));
			$a_user_selects = array();
			foreach($a_users as $a_user) {
				$a_user_selects[] = "<input type='button' value='".$a_user["username"]."' onclick='$(\"input[name=report_server_id]\").val(\"".$a_user["id"]."\"); $(\"form\").submit();' class='ipad_select_button'></input><br />";
			}
			echo "<script type='text/javascript' src='/manage/js/jquery/js/jquery-1.9.0.js'></script>";
			echo "<form method='GET' style='margin:0;'>{$s_getvars}<input type='hidden' name='mode' value='server_single'></input>";
			echo "<input type='hidden' name='report_server_id' value=''></input>";
			echo implode("", $a_user_selects);
			echo "</form>";
		} else {
			global $location_info;
			$_GET['debug'] = '1';
			$_POST['debug'] = '1';
			$_REQUEST['debug'] = '1';
			$_GET['m'] = $a_getvars['mode'];
			$_POST['m'] = $a_getvars['mode'];
			$_REQUEST['m'] = $a_getvars['mode'];
			echo "<form method='GET' style='display:inline-block;''><input type='hidden' name='mode' value='main_menu'></input>{$s_getvars}<input type='submit' value='Reports Menu'></form>";
			echo "<form method='GET' style='display:inline-block;'><input type='hidden' name='mode' value='{$report_page}'></input>{$s_getvars}<input type='submit' value='Reload'></form>";
			echo "<div style='float:right; max-width:500px; overflow-x:auto;'>Getvars:<pre style='font-family:\"Courier New\",Courier,monospace;'>\n".print_r($_GET,TRUE)."</pre></div>";
			echo "<div style='border:1px dashed #555; display:table; padding:5px; font-family:\"Courier New\", Courier, monospace; font-weight:bold;'>";
			require_once(dirname(__FILE__)."/../../../{$dev}lib/till_report.php");
			echo "</div>";
		}
	}
}

?>
