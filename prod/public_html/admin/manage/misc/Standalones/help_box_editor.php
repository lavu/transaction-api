<?php
	
	require_once(dirname(__FILE__).'/../../login.php');
	require_once('/home/poslavu/public_html/admin/cp/resources/lavuquery.php');
	require_once('/home/poslavu/public_html/admin/cp/resources/core_functions.php');
	require_once("/home/poslavu/public_html/admin/sa_cp/billing/package_levels_object.php");
	require_once('/home/poslavu/public_html/admin/manage/misc/AdvancedToolsFunctions/companyReporting.php');
	require_once(dirname(__FILE__)."/../../misc/autoTable.php");
	
	echo "
		<script type='text/javascript' src='/manage/js/jquery/js/jquery-1.9.0.js'></script>
		<script type='text/javascript' src='/manage/js/jquery/js/jquery-ui-1.10.0.custom.min.js'></script>
		<script type='text/javascript' src='/cp/scripts/jquery.simulate.js'></script>
		<link rel='stylesheet' href='http://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css' />";
	require_once(dirname(__FILE__).'/../../../cp/resources/help_box.php');
	
	// get the environment set up
	session_start();
	if (!account_loggedin())
		header("Location: http://admin.poslavu.com/manage/");
	
	if (!function_exists('postvar')) {
		function postvar($s_name, $wt_default = FALSE) {
			return isset($_POST[$s_name]) ? $_POST[$s_name] : $wt_default;
		}
	}
	if (!function_exists('getvar')) {
		function gettvar($s_name, $wt_default = FALSE) {
			return isset($_GET[$s_name]) ? $_GET[$s_name] : $wt_default;
		}
	}
	if (!function_exists('requestvar')) {
		function requestvar($s_name, $wt_default = FALSE) {
			return isset($_REQUEST[$s_name]) ? $_REQUEST[$s_name] : $wt_default;
		}
	}
	
	global $i_helptext_id;
	$i_helptext_id = "";
	
	class HELP_BOX_EDITOR {
		public static function edit_text() {
			
			global $i_helptext_id;
			
			// get the data
			$i_helptext_id = requestvar('helptext_id', 0);
			
			// check that the user has correct permissions and the file is editable
			if (!self::can_edit_help_text($i_helptext_id))
				return;
			
			// get the text
			$b_is_deleted = NULL;
			$s_allowed_users = NULL;
			$s_hyperlink_json = NULL;
			$s_text = self::get_helptext_contents($i_helptext_id, $b_is_deleted, $s_allowed_users, $s_hyperlink_json);
			$s_enable = $b_is_deleted ? 'enable' : 'disable';
			
			// create the editing dialog
			echo "
			<h2>Dev</h2>
			<table>
				<tr>
					<td style='vertical-align:top;'>
						<textarea cols='100' rows='40' onkeydown='update_preview();' onkeyup='update_preview();'>{$s_text}</textarea>
					</td>
					<td style='vertical-align:top;'>
						<div><input type='button' value='Insert Title' onclick='insert(this);' /><font style='display:none'>
<div class='h1' style='margin:0;'>Help Box Title</div></font></div>
						<div><input type='button' value='Insert List' onclick='insert(this);' /><font style='display:none;'>
<ol>
	<li>
		<div class='h2'>List Item Title</div>
		<font>
			List item help text.
		</font>
	</li>
</ol></font></div>
						<div><input type='button' value='Insert List Item' onclick='insert(this);' /><font style='display:none;'>
	<li>
		<div class='h2'>List Item Title</div>
		<font>
			List item help text.
		</font>
	</li></font></div>
						<div><input type='button' value='Insert Bullet' onclick='insert(this);' /><font style='display:none;'>&bull;</font></div>
						<div><input type='button' value='Insert Right Arrow' onclick='insert(this);' /><font style='display:none;'>&rarr;</font></div>
						<div style='border-top:1px dashed lightgray; margin:5px 0 5px 0;'></div>
						<div><input type='button' class='application_type' value='Bold Text' onclick='apply_property(this);' /><font style='display:none;' class='start'>(bold)</font><font style='display:none;' class='end'>(boldend)</font></div>
						<div><input type='button' class='application_type' value='Italicize Text' onclick='apply_property(this);' /><font style='display:none;' class='start'>(ital)</font><font style='display:none;' class='end'>(italend)</font></div>
						<div><input type='button' class='application_type' value='Underline Text' onclick='apply_property(this);' /><font style='display:none;' class='start'>(uline)</font><font style='display:none;' class='end'>(ulineend)</font></div>
						<div><input type='button' class='application_type' value='Apply Hyperlink' onclick='apply_property(this); check_for_hyperlinks(); update_preview();' /><font style='display:none;' class='start'>(link)</font><font style='display:none;' class='end'>(linkend)</font><font style='display:none;' class='special_tag_search'>link_check</font></div>
					</td>
					<td style='vertical-align:top;'>
						Hyperlinks:
						<table id='hyperlinks'>
							".self::hyperlink_rows_tostr($s_hyperlink_json)."
						</table>
					</td>
				</tr>
			</table>
			<input type='button' onclick='save_helptext(this, \"dev\");' value='Save Text (dev)' />
			<input type='button' onclick='save_helptext(this, \"live\");' value='Save Text (live)' />
			<input type='button' onclick='save_helptext(this, \"dev\"); save_helptext(this, \"live\");' value='Save Text (both)' />
			<br />
			<input type='button' onclick='disable_helptext(0);' value='Enable Help Text' />
			<input type='button' onclick='disable_helptext(1);' value='Disable Help Text' />
			<br />
			Preview: (auto-updates)<div id='preview'></div>
			<br />";
			if (can_access('edit_help_text_users')) {
				if ($s_allowed_users == '') $s_allowed_users = 'none';
				$a_db_users = ConnectionHub::getConn('poslavu')->getDBAPI()->getAllInTable('accounts', NULL, TRUE, array('selectclause'=>'`username`'));
				if (count($a_db_users) > 0) {
					$a_users = array();
					foreach($a_db_users as $a_user)
						$a_users[] = $a_user['username'];
					sort($a_users);
					echo "<div>Who has access to edit this help text: <input type='text' value='{$s_allowed_users}' name='special_users' /><input type='button' name='add_user' value='+' onclick='add_special_user();' /><input type='button' name='remove_user' value='-' onclick='remove_special_user();' /><input type='hidden' name='special_users_allowed' value='".implode(',',$a_users)."' /><input type='button' onclick='save_helptext_users();' value='Save' /></div>";
				}
			}
		}
		
		// returns the contents for the given helptext id
		// @$i_helptext_id: id of the help text to fetch from `poslavu_MAIN_db`.`helpText`
		// @$b_is_deleted: sets to TRUE if deleted, FALSE otherwise
		// @$s_allowed_users: sets to the string value of specific_user_access
		// @$s_hyperlink_json: sets the string to the hyperlinks json text, if it exists
		public static function get_helptext_contents($i_helptext_id, &$b_is_deleted = NULL, &$s_allowed_users = NULL, &$s_hyperlink_json = NULL) {
			$a_helptexts = ConnectionHub::getConn('poslavu')->getDBAPI()->getAllInTable('helpText', array('id'=>$i_helptext_id), TRUE);
			if (count($a_helptexts) == 0) {
				$b_is_deleted = TRUE;
				$s_allowed_users = '';
				return '';
			}
			$b_is_deleted = ($a_helptexts[0]['_deleted'] == 0 ? FALSE : TRUE);
			$s_allowed_users = $a_helptexts[0]['specific_user_access'];
			$s_hyperlink_json = $a_helptexts[0]['hyperlinks_dev'];
			return $a_helptexts[0]['contents_dev'];
		}
		
		public static function get_preview() {
			
			$i_counter = postvar('counter');
			$i_helptext_id = postvar('helptext_id');
			$s_text = rawurldecode( postvar('new_text') );
			$s_hyperlinks = rawurldecode( postvar('new_hyperlinks') );
			
			echo "<!--message-->";
			echo $i_counter;
			echo "[*update-preview-part*]";
			echo help_mark_tostring($i_helptext_id, $s_text, TRUE, $s_hyperlinks, TRUE);
			echo "<!--message-->";
		}
		
		// check that the user can access these functions
		// @$i_helptext_id: id of the helptext trying to be edited
		// @$s_echo: used to return a failure message if there is one
		// @return: TRUE or FALSE
		public static function can_edit_help_text($i_helptext_id, &$s_echo = NULL) {
			
			// check user access
			if (!can_access('edit_help_text') && !can_access('edit_help_text_users') && !self::user_can_edit($i_helptext_id)) {
				$s_echo = "Incorrect permissions";
				echo $s_echo;
				return FALSE;
			}
			
			return TRUE;
		}
		
		// check if the given user has access to edit this specific help text
		public static function user_can_edit($i_helptext_id) {
			
			// get the help text
			$a_helptexts = ConnectionHub::getConn('poslavu')->getDBAPI()->getAllInTable('helpText', array('id'=>$i_helptext_id), TRUE);
			if (count($a_helptexts) == 0)
				return FALSE;
			
			// check if the user's name is a part of specific_user_access
			global $loggedin;
			$s_users = explode(',', $a_helptexts[0]['specific_user_access']);
			foreach($s_users as $s_user) {
				if ($s_user == $loggedin)
					return TRUE;
			}
			return FALSE;
		}
		
		// save data from the form
		public static function save_helptext() {
			
			// get the data
			$i_helptext_id = (int)postvar('helptext_id', 0);
			$s_value = rawurldecode(postvar('value', ''));
			$s_column = postvar('column', '');
			
			// check user permissions
			$s_failure = '';
			if (!self::can_edit_help_text($i_helptext_id, $s_failure)) {
				echo "<!--message-->{$s_failure}<!--message-->";
				return;
			}
			
			// check that the helptext exists
			$a_helptexts = ConnectionHub::getConn('poslavu')->getDBAPI()->getAllInTable('helpText', array('id'=>$i_helptext_id), TRUE);
			if (count($a_helptexts) == 0) {
				echo "<!--message-->Help text id {$i_helptext_id} can't be found.<!--message-->";
				return;
			}
			
			// check if the helptext value is already what is trying to be set
			if ($a_helptexts[0][$s_column] == $s_value) {
				echo "<!--message-->success<!--message-->";
				return;
			}
			
			// save the new text
			mlavu_query("UPDATE `poslavu_MAIN_db`.`helpText` SET `[column]`='[value]' WHERE `id`='[id]' LIMIT 1",
				array('column'=>$s_column, 'value'=>$s_value, 'id'=>$i_helptext_id));
			if (ConnectionHub::getConn('rest')->affectedRows() > 0)
				echo "<!--message-->success<!--message-->";
			else
				echo "<!--message-->update failed<!--message-->";
		}
		
		public static function get_hyperlink_row_tostr() {
			return '<tr class="--index--"><td class="name">Link --index--: </td><td class="value"><input type="textarea" name="linkvalue" placeholder="http://example.com" --value-- onkeydown="update_preview();" onkeyup="update_preview();" /><input type="hidden" name="linkindex" value="--index--" /></td><td class="commands"><a href="#" onclick="highlight_hyperlink(this);" style="color:blue; text-decoration:none;">find</a> <a href="#" onclick="open_hyperlink(this);" class="openlink">open</a></td></tr>';
		}
		
		public static function get_hyperlink_row_to_javascript() {
			$s_string = str_replace('--value--', '', str_replace('--index--', "'+index+'", self::get_hyperlink_row_tostr()));
			return $s_string;
		}
		
		public static function get_hyperlink_row_to_php($index, $value) {
			return str_replace('--value--', "value='{$value}'", str_replace('--index--', $index, self::get_hyperlink_row_tostr()));
		}
		
		public static function hyperlink_rows_tostr($s_json) {
			require_once(dirname(__FILE__).'/../../../cp/objects/json_decode.php');
			$o_json = JSON::unencode($s_json);
			if (count($o_json) > 0) {
				foreach($o_json as $o_hyperlink) {
					return self::get_hyperlink_row_to_php($o_hyperlink->index, $o_hyperlink->value);
				}
			}
		}
	}
	
	$s_action = postvar('action');
	if ($s_action !== FALSE) {
		$o_help_box_editor = new HELP_BOX_EDITOR();
		if (method_exists($o_help_box_editor, $s_action)) {
			call_user_method($s_action, $o_help_box_editor);
		}
	}
	
?>

<script type="text/javascript">
function get_type_choice(string, possible_types, current_types, remove_types, complete_func) {
	var jgrouping = $("input[name=special_users]").parent();
	var jspecial_box = $("input[name=special_users]");
	var jwindow = $(window);
	var new_types = [];
	var x = 0, y = 0;
	
	// get the types that haven\'t been assigned yet
	for (var i = 0; i < current_types.length; i++) {
		var type = current_types[i];
		if (possible_types.indexOf(type) < 0)
			continue;
		var has_type = false;
		for (var j = 0; j < remove_types.length; j++) {
			if (remove_types[j] == type) {
				has_type = true;
				break;
			}
		}
		if (!has_type) {
			new_types.push(type);
		}
	}
	
	// calculate the position for the new div
	x = jspecial_box.offset().left - jwindow.scrollLeft()-100;
	y = jspecial_box.offset().top - jwindow.scrollTop()-60;
	
	// display the types
	while($("#type_chooser").length > 0)
		$("#type_chooser").remove();
	jgrouping.append("<div id='type_chooser' style='position:fixed; top:"+y+"px; left:"+x+"px; border:1px solid black; background-color:white; padding:15px;'></div>");
	var jdiv = $("#type_chooser");
	var options = "";
	$.each(new_types, function(k,v) {
		options += "<option>"+v+"</option>";
	});
	var appendtext = "<select>"+options+"</select> <input type='button' value='Choose' /> <input type='button' value='Cancel' />"
	if (options == "") {
		appendtext = "There are no more choices available. <input type='button' value='Cancel' />";
	}
	jdiv.append(appendtext);
	jdiv.find("input[value=Cancel]").click(function(){ jdiv.remove(); complete_func(""); });
	if (jdiv.find("input[value=Choose]").length > 0)
		jdiv.find("input[value=Choose]").click(function(){ var val = jdiv.find("select").val(); jdiv.remove(); complete_func(val); });
}

function add_special_user() {
	var types_string = $("input[name=special_users_allowed]").val();
	var jinput = $("input[name=special_users]");
	var types = types_string.split(",");
	var current_types_string = jinput.val();
	var current_types = current_types_string.split(",");
	
	get_type_choice("Choose a type to add...", types, types, current_types, function(type) {
		if (type != "") {
			if (jinput.val() == "none") {
				jinput.val(type);
			} else {
				jinput.val(jinput.val()+","+type);
			}
		}
	});
}

function remove_special_user() {
	var types_string = $("input[name=special_users_allowed]").val();
	var jinput = $("input[name=special_users]");
	var types = types_string.split(",");
	var current_types_string = jinput.val();
	var current_types = current_types_string.split(",");
	
	get_type_choice("Choose a type to add...", types, current_types, [], function(type) {
		if (type != "") {
			if (jinput.val() == type) {
				jinput.val("none");
			} else if (jinput.val().indexOf(","+type) > -1) {
				var split = jinput.val().split(","+type);
				jinput.val(split[0]+split[1]);
			} else if (jinput.val().indexOf(type+",") > -1) {
				var split = jinput.val().split(type+",");
				jinput.val(split[0]+split[1]);
			}
		}
	});
}

// from http://phpjs.org/functions/rawurlencode/
function rawurlencode (str) {
  // http://kevin.vanzonneveld.net
  // +   original by: Brett Zamir (http://brett-zamir.me)
  // +      input by: travc
  // +      input by: Brett Zamir (http://brett-zamir.me)
  // +   bugfixed by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
  // +      input by: Michael Grier
  // +   bugfixed by: Brett Zamir (http://brett-zamir.me)
  // +      input by: Ratheous
  // +      reimplemented by: Brett Zamir (http://brett-zamir.me)
  // +   bugfixed by: Joris
  // +      reimplemented by: Brett Zamir (http://brett-zamir.me)
  // %          note 1: This reflects PHP 5.3/6.0+ behavior
  // %        note 2: Please be aware that this function expects to encode into UTF-8 encoded strings, as found on
  // %        note 2: pages served as UTF-8
  // *     example 1: rawurlencode('Kevin van Zonneveld!');
  // *     returns 1: 'Kevin%20van%20Zonneveld%21'
  // *     example 2: rawurlencode('http://kevin.vanzonneveld.net/');
  // *     returns 2: 'http%3A%2F%2Fkevin.vanzonneveld.net%2F'
  // *     example 3: rawurlencode('http://www.google.nl/search?q=php.js&ie=utf-8&oe=utf-8&aq=t&rls=com.ubuntu:en-US:unofficial&client=firefox-a');
  // *     returns 3: 'http%3A%2F%2Fwww.google.nl%2Fsearch%3Fq%3Dphp.js%26ie%3Dutf-8%26oe%3Dutf-8%26aq%3Dt%26rls%3Dcom.ubuntu%3Aen-US%3Aunofficial%26client%3Dfirefox-a'
  str = (str + '').toString();

  // Tilde should be allowed unescaped in future versions of PHP (as reflected below), but if you want to reflect current
  // PHP behavior, you would need to add \".replace(/~/g, '%7E');\" to the following.
  return encodeURIComponent(str).replace(/!/g, '%21').replace(/'/g, '%27').replace(/\(/g, '%28').
  replace(/\)/g, '%29').replace(/\*/g, '%2A');
}

general_parse_message = function(message) {
	message = message.substring(message.indexOf('<!--message-->')+14);
	message = message.substring(0, message.indexOf('<!--message-->'));
	return message;
}
general_success = function(message) {
	message = general_parse_message(message);
	alert(message);

}
general_failure = function(a,b,c) {
	if (b == 'timeout') {
		alert('Connection timed out.');
	} else {
		alert('Could not contact the server.');
	}
}

function save_helptext_users() {
	var userstring = $('input[name=special_users]').val();
	if (userstring == 'none') userstring = '';
	
	$.ajax({
		async: false,
		cache: false,
		url: './help_box_editor.php',
		method: 'post',
		data: { action: 'save_helptext', helptext_id: '<?= $i_helptext_id ?>', column: 'specific_user_access', value: rawurlencode(userstring) },
		success: general_success,
		error: general_failure,
		timeout: 5000
	});
}

function save_helptext(element, dev) {
	var jtextarea = $(document).find('textarea');
	var new_text = jtextarea.val();
	var column = 'contents_dev';
	
	if (new_text == '' && false) {
		alert('help area cannot be empty');
		return;
	}
	
	if (dev == 'live') {
		column = 'contents';
	}
	
	$.ajax({
		async: true,
		cache: false,
		url: './help_box_editor.php',
		method: 'post',
		data: { action: 'save_helptext', helptext_id: '<?= $i_helptext_id ?>', column: column, value: rawurlencode(new_text) },
		success: function(m) {
			m = general_parse_message(m);
			if (m != 'success') {
				alert('failed to save');
				return;
			}
			save_helptext_hyperlinks(dev);
		},
		error: general_failure,
		timeout: 5000
	});
}

// builds a list of hyperlinks, in the form [{'index':int, 'value':string}, ...]
function get_list_of_hyperlinks() {
	
	// initiate the list
	var hyperlinks = [];
	
	// get all the hyperlinks
	var jhyperlinks = $('#hyperlinks').find('td.value');
	$.each(jhyperlinks, function(k,td) {
		var jtd = $(td);
		var index = jtd.find('input[name=linkindex]').val();
		var value = jtd.find('input[name=linkvalue]').val();
		
		// add the hyperlink to the list
		hyperlinks.push({ index:index, value:value });
	});
	
	return hyperlinks;
}

function save_helptext_hyperlinks(dev) {
	
	var column = 'hyperlinks';
	if (dev == 'dev')
		column += '_dev';
	
	// get the list of hyperlinks
	var hyperlinks = get_list_of_hyperlinks();
	var json_hyperlinks = JSON.stringify(hyperlinks);
	
	// save the hyperlinks
	$.ajax({
		async: true,
		cache: false,
		url: './help_box_editor.php',
		method: 'post',
		data: { action: 'save_helptext', helptext_id: '<?= $i_helptext_id ?>', column: column, value: rawurlencode(json_hyperlinks) },
		success: general_success,
		error: general_failure,
		timeout: 5000
	})
}

function disable_helptext(value) {
	$.ajax({
		async: true,
		cache: false,
		url: './help_box_editor.php',
		method: 'post',
		data: { action: 'save_helptext', helptext_id: '<?= $i_helptext_id ?>', column: '_deleted', value: rawurlencode(value) },
		success: general_success,
		error: general_failure,
		timeout: 5000
	});
}

window.update_preview_counter = 0;
window.update_preview_latest = 0;
function update_preview() {
	window.update_preview_counter++;
	var jtextarea = $(document).find('textarea');
	var new_text = jtextarea.val();
	var new_hyperlinks = JSON.stringify(get_list_of_hyperlinks());
	
	$.ajax({
		async: true,
		cache: false,
		url: './help_box_editor.php',
		method: 'post',
		data: { action: 'get_preview', helptext_id: <?= $i_helptext_id ?>, counter: window.update_preview_counter, new_text: rawurlencode(new_text), new_hyperlinks: rawurlencode(new_hyperlinks) },
		success: function(message) {
			message = message.substring(message.indexOf('<!--message-->')+14);
			message = message.substring(0, message.indexOf('<!--message-->'));
			var parts = message.split('[*update-preview-part*]');
			var counter = parseInt(parts[0]);
			var preview = parts[1];
			if (counter > window.update_preview_latest) {
				window.update_preview_latest = counter;
				
				var jdialog = $('.ui-dialog');
				var display = jdialog.css('display');
				var closebutton = $('.ui-button-text');
				if (closebutton.length > 0)
					closebutton.click();
				while ($('#help_mark_tostring_<?= $i_helptext_id ?>').length > 0)
					$('#help_mark_tostring_<?= $i_helptext_id ?>').remove();
				
				var jpreview = $('#preview')
				jpreview.html('');
				jpreview.append(preview);
			}
		},
		error: function(a,b,c) {
		}
	});
}

String.prototype.splice = function( idx, rem, s ) {
    return (this.slice(0,idx) + s + this.slice(idx + Math.abs(rem)));
};

function insert(element) {
	jfont = $(element).siblings('font');
	var html = jfont.html();
	var jtextarea = $(document).find('textarea');
	var position = jtextarea[0].getCaretPosition();
	var curr_val = jtextarea.val();
	var new_val = curr_val;
	new_val = new_val.splice(position, 0, html);
	jtextarea.val(new_val);
	jtextarea[0].setCaretPosition(position+html.length);
	update_preview();
}

function get_selection_length() {
	var jtextarea = $(document).find('textarea');
	return (jtextarea[0].hasSelection() ? jtextarea[0].getSelectedText().length : 0);
}

function link_check() {
	
	var jtextarea = $('textarea');
	var html = jtextarea.val();
	
	// check for the first 1000 links
	for (var i = 0; i < 1000; i++) {
		var tags = { start: ('(link'+i+')'), end: ('(linkend'+i+')') };
		if (html.indexOf(tags.start) >= 0) {
			if (in_property(tags.start, tags.end)) {
				return tags;
			}
		} else {
			break;
		}
	}
	
	return { start: '(link)', end: '(linkend)' };
}

function get_start_end_tags(element) {
	var jfont_special_tag_search = $(element).siblings('font.special_tag_search');
	if (jfont_special_tag_search.length > 0) {
		var func_name = jfont_special_tag_search.html();
		return eval(func_name+'();');
	} else {
		var jfont_start = $(element).siblings('font.start');
		var jfont_end = $(element).siblings('font.end');
		var start = jfont_start.html();
		var end = jfont_end.html();
		return { start: start, end: end };
	}
}

function apply_property(element) {
	
	// get the start and end tags
	var tags = get_start_end_tags(element);
	var start = tags.start;
	var end = tags.end;
	
	// get the textarea and other vars
	var jtextarea = $(document).find('textarea');
	var position = jtextarea[0].getCaretPosition();
	var end_position = position+(jtextarea[0].hasSelection() ? jtextarea[0].getSelectedText().length : 0);
	var curr_val = jtextarea.val();
	var new_val = curr_val;
	
	// apply
	if (in_property(start,end)) {
		end_position = find_next(curr_val, end, position-end.length+1);
		position = find_prev(curr_val, start, position);
		new_val = new_val.splice(end_position, end.length, '');
		new_val = new_val.splice(position, start.length, '');
		prop_length = -start.length;
	} else {
		new_val = new_val.splice(end_position, 0, end);
		new_val = new_val.splice(position, 0, start);
		prop_length = start.length+end.length;
	}
	jtextarea.val(new_val);
	jtextarea[0].setSelection(position, end_position+prop_length);
	update_preview();
}

function find_next(haystack, needle, startat) {
	startat = Math.max(0, startat);
	haystack = haystack.splice(0, startat, '');
	var splits = haystack.split(needle);
	if (splits.length == 1)
		return -1;
	return startat+splits[0].length;
}

function find_prev(haystack, needle, startat) {
	var splits = haystack.split(needle);
	if (splits.length == 1)
		return -1;
	length = splits[0].length;
	prev_length = 0;
	if (length > startat)
		return -1;
	for (var i = 1; i < splits.length; i++) {
		prev_length = length;
		length += splits[i].length+needle.length;
		if (length > startat)
			return prev_length;
	}
}

function in_property(start_tag,end_tag) {
	var jtextarea = $(document).find('textarea');
	var sel_length = get_selection_length();
	var position = jtextarea[0].getCaretPosition();
	var curr_val = jtextarea.val();
	
	// check if the caret is in the property
	var splits = curr_val.split(start_tag);
	var length = splits[0].length;
	for(var i = 1; i < splits.length; i++) {
		
		// check if we've already passed the caret
		if (length > position)
			break;
		
		// check if caret is inside this start segment
		var seg_start = length;
		var seg_end = seg_start+start_tag.length+splits[i].length;
		var caret_in_seg = ((position > seg_start || (position == seg_start && sel_length > 0)) && position <= seg_end+end_tag.length-1);
		
		// inside a start, check for ends
		if (caret_in_seg) {
			var tempsplits = splits[i].split(end_tag);
			var prop_end = seg_start+tempsplits[0].length+start_tag.length+end_tag.length-1;
			if (position <= prop_end)
				return true;
		}
		
		length = seg_end;
	}
	
	return false;
}

function set_application_on(element, on) {
	var val = element.value;
	var prefix = 'Un-';
	if (val.indexOf(prefix) == 0 && !on) {
		element.value = element.value.splice(0, 3, '');
	}
	if (val.indexOf(prefix) != 0 && on) {
		element.value = element.value.splice(0, 0, prefix);
	}
}

function check_caret_in_application_types() {
	setTimeout('check_caret_in_application_types();', 100);
	
	var application_types = $('input.application_type');
	for(var i = 0; i < application_types.length; i++) {
		
		// get the start and end tags
		var tags = get_start_end_tags(application_types[i]);
		var start = tags.start;
		var end = tags.end;
		
		if (in_property(start,end)) {
			set_application_on(application_types[i], true);
		} else {
			set_application_on(application_types[i], false);
		}
	}
}

// looks for hyperlinks and creates their hyperlink tr's in the table $('#hyperlinks')
function check_for_hyperlinks() {
	
	// get the html
	var jtextarea = $('textarea');
	var html = jtextarea.val();
	var jtable = $('#hyperlinks');
	
	// get the caret position
	var caret = jtextarea[0].getCaretPosition();
	var selected_text = jtextarea[0].getSelectedText();
	
	// look for the first 1000 hyperlinks
	var index = 0;
	var max_iterations = index+10;
	for (var i = 0; i < 1000; i++) {
		var str_search = '(link'+i+')';
		link_pos = html.indexOf(str_search);
		if (link_pos >= 0) {
			
			// update the link for the next index
			if (i != index) {
				html = html.replace('(link'+i+')', '(link'+index+')');
				html = html.replace('(linkend'+i+')', '(linkend'+index+')');
				if (link_pos+'(link'.length <= caret)
					caret += (''+index).length - (''+i).length;
				
				// update the table row, if it exists
				var jtr_old = jtable.find('tr.'+index);
				if (jtr_old.length > 0) jtr_old.remove();
				var jtr = jtable.find('tr.'+i);
				if (jtr.length > 0) {
					jtr.removeClass(''+i);
					jtr.addClass(''+index);
					jtr.children('td.name').html('Link '+index);
					jtr.children('td.value').find('input[name=linkindex]').val(index);
				}
			}
			
			// find the table row, creating one if it doesn't exist
			var jtr = jtable.find('tr.'+index);
			if (jtr.length == 0)
				jtable.append('<?= HELP_BOX_EDITOR::get_hyperlink_row_to_javascript(); ?>');
			
			// update the index position
			max_iterations = index+10;
			index++;
		} else {
			if (index > max_iterations)
				break;
		}
	}
	
	// look now for unassigned hyperlinks
	var jtr_new = null;
	var max_iterations = index+1000;
	while (html.indexOf('(link)') >= 0) {
		link_pos = html.indexOf('(link)');
		
		// update the link name
		if (link_pos+'(link'.length <= caret)
			caret += (''+index).length;
		html = html.replace('(link)', '(link'+index+')');
		html = html.replace('(linkend)', '(linkend'+index+')');
		
		// create a new table row if one doesn't exist
		var jtr = jtable.find('tr.'+index);
		if (jtr.length > 0) {
			jtr_new = jtr;
		} else {
			jtr_new = $('<?= HELP_BOX_EDITOR::get_hyperlink_row_to_javascript(); ?>');
			jtable.append(jtr_new);
		}
		
		// update the index position
		index++;
		
		if (index > max_iterations)
			break;
	}
	
	// remove old table rows
	var b_removed = false;
	for (var i = index; i < index+10; i++) {
		var jtr = jtable.find('tr.'+i);
		if (jtr.length > 0) {
			b_removed = true;
			jtr.remove();
		}
	}
	
	// set the value and the caret position/focus
	jtextarea.val(html);
	jtextarea[0].setCaretPosition(caret);
	if (b_removed !== false) {
		jtextarea[0].setSelection(caret, caret+selected_text.length);
	}
	if (jtr_new) {
		jtr_new.find('input[type=textarea]').focus();
		var jtr_new_index = jtr_new.find('input[name=linkindex]').val();
		var tags = { start: ('(link'+jtr_new_index+')'), end: ('(linkend'+jtr_new_index+')') };
		var pos = { start: (html.indexOf(tags.start)), end: (html.indexOf(tags.end)+tags.end.length) };
		jtextarea[0].setSelection(pos.start, pos.end);
	}
}

function highlight_hyperlink(element) {
	
	// get the start and end tags
	var jtd = $(element).parent().siblings('td.value');
	var index = jtd.find('input[name=linkindex]').val();
	var tags = { start:('(link'+index+')'), end:'(linkend'+index+')' };
	
	// get the position of the tags in the textarea
	var jtextarea = $('textarea');
	var html = jtextarea.val();
	var start = html.indexOf(tags.start);
	var end = html.indexOf(tags.end);
	
	// set the selection
	if (start >= 0 && end >= start) {
		jtextarea[0].setSelection(start, end+tags.end.length);
	} else {
		alert("Can't find this link");
	}
}

function open_hyperlink(element) {
	
	// get the link to open
	var jelement = $(element);
	var jparent = jelement.parent();
	var jtd = jparent.siblings('td.value');
	var value = jtd.find('input[name=linkvalue]').val().trim();
	if (value == "") {
		return false;
	}
	
	// open the link in a new tab
	if (typeof window.winlinks == "undefined") { window.winlinks = []; }
	win = window.open(value, '_blank');
	window.winlinks.push({ window:win, td:jtd[0], closed:false });
	var winlink_index = window.winlinks.length-1;
	
	// create a close link
	while (jparent.children('a.closelink').length > 0)
		jparent.children('a.closelink').remove();
	jparent.append('<a href="#" onclick="$(this).siblings(\'a.openlink\').show(); $(this).remove(); window.winlinks['+winlink_index+'].window.close();" class="closelink">close</a>');
	jelement.hide();
	
	// remove the close link if the window is closed
	if (window.remove_close_links_timeout)
		window.clearTimeout(window.remove_close_links_timeout);
	remove_close_links();
}

function remove_close_links() {
	window.remove_close_links_timeout = setTimeout('remove_close_links();', 500);
	
	var b_are_found = false;
	$.each(window.winlinks, function(k,v) {
		if (v.window && v.td && v.window.closed && !v.closed) {
			$(v.td).parent().find('a.closelink').click();
			v.closed = true;
		}
	});
}

// from http://stackoverflow.com/questions/1796141/properly-bind-javascript-events
function bindEvent(element, type, handler) {
   if(element.addEventListener) {
      element.addEventListener(type, handler, false);
   } else {
      element.attachEvent('on'+type, handler);
   }
}

function check_against_keys(which_keys) {
	
}

window.invalid_keys = {
	hyperlinks: {
		
	}
}

// from http://css-tricks.com/snippets/javascript/support-tabs-in-textareas/
HTMLTextAreaElement.prototype.getCaretPosition = function () { //return the caret position of the textarea
    return this.selectionStart;
};
HTMLTextAreaElement.prototype.setCaretPosition = function (position) { //change the caret position of the textarea
    this.selectionStart = position;
    this.selectionEnd = position;
    this.focus();
};
HTMLTextAreaElement.prototype.hasSelection = function () { //if the textarea has selection then return true
    if (this.selectionStart == this.selectionEnd) {
        return false;
    } else {
        return true;
    }
};
HTMLTextAreaElement.prototype.getSelectedText = function () { //return the selection text
    return this.value.substring(this.selectionStart, this.selectionEnd);
};
HTMLTextAreaElement.prototype.setSelection = function (start, end) { //change the selection area of the textarea
    this.selectionStart = start;
    this.selectionEnd = end;
    this.focus();
};

var textarea = document.getElementsByTagName('textarea')[0];

textarea.onkeydown = function(event) {
	
	var tab = '	';
	
	//support tab on textarea
	if (event.keyCode == 9) { //tab was pressed
	    var newCaretPosition;
	    newCaretPosition = textarea.getCaretPosition() + tab.length;
	    textarea.value = textarea.value.substring(0, textarea.getCaretPosition()) + tab + textarea.value.substring(textarea.getCaretPosition(), textarea.value.length);
	    textarea.setCaretPosition(newCaretPosition);
	    return false;
	}
	if(event.keyCode == 8){ //backspace
	    if (textarea.value.substring(textarea.getCaretPosition() - 4, textarea.getCaretPosition()) == tab) { //it's a tab space
	        var newCaretPosition;
	        newCaretPosition = textarea.getCaretPosition() - 3;
	        textarea.value = textarea.value.substring(0, textarea.getCaretPosition() - 3) + textarea.value.substring(textarea.getCaretPosition(), textarea.value.length);
	        textarea.setCaretPosition(newCaretPosition);
	    }
	}
	if(event.keyCode == 37){ //left arrow
	    var newCaretPosition;
	    if (textarea.value.substring(textarea.getCaretPosition() - 4, textarea.getCaretPosition()) == tab) { //it's a tab space
	        newCaretPosition = textarea.getCaretPosition() - 3;
	        textarea.setCaretPosition(newCaretPosition);
	    }    
	}
	if(event.keyCode == 39){ //right arrow
	    var newCaretPosition;
	    if (textarea.value.substring(textarea.getCaretPosition() + 4, textarea.getCaretPosition()) == tab) { //it's a tab space
	        newCaretPosition = textarea.getCaretPosition() + 3;
	        textarea.setCaretPosition(newCaretPosition);
	    }
	}
}

setTimeout('check_caret_in_application_types(); check_for_hyperlinks();', 1000);
</script>
