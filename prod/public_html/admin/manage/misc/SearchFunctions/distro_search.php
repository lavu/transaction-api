<?php
	session_start();
	require_once($_SERVER['DOCUMENT_ROOT']."/cp/resources/lavuquery.php");
	
	if(isset($_POST['function'])){
		$function= $_POST['function'];
		unset($_POST['function']);
		 echo  $function($_POST);
	}else
		echo "Failed";
	
	function perform_search($params){
		$filter		   = $params['search_param'];
		$use_certified = $params['certified'];

		if($use_certified=="true")
			$get_resellers = mlavu_query("SELECT `id`, CONCAT(`f_name`,' ', `l_name`) AS `name`,`company`,`address`,`city`,`state`,`country`,`postal_code`,`phone`,`email`,`website`,`signup_date`,`isCertified` FROM `poslavu_MAIN_db`.`resellers` WHERE (`f_name` LIKE '%[1]%' OR `l_name` LIKE '%[1]%' OR `company` LIKE '%[1]%' OR `country` LIKE '%[1]%') AND `isCertified`='1' ORDER BY `l_name`, `f_name` ASC LIMIT 100", $filter);
		else
			$get_resellers = mlavu_query("SELECT `id`, CONCAT(`f_name`,' ', `l_name`) AS `name`,`company`,`address`,`city`,`state`,`country`,`postal_code`,`phone`,`email`,`website`,`signup_date`,`isCertified` FROM `poslavu_MAIN_db`.`resellers` WHERE (`f_name` LIKE '%[1]%' OR `l_name` LIKE '%[1]%' OR `company` LIKE '%[1]%' OR `country` LIKE '%[1]%') ORDER BY `l_name`, `f_name` ASC LIMIT 100", $filter);
		
		$ret_str='';
		
		if(mysqli_num_rows($get_resellers) > 0) {
			$even=false;
			
			while($result= mysqli_fetch_assoc($get_resellers)){
			
				if($even)
					$class='distro_result_td_even';
				else
					$class='';
				$even = !$even;										
				$ret_str.="<tr class='distro_result_tr'>";
				foreach($result as $key=>$info){
					if( $key=='website'){
						
						if($info && strstr("http",$info))
							$ret_str.="<td class='distro_result_td distro_result_$key $class'><a href='$info' target='_blank'>link</a></td>";
						else
							$ret_str.="<td class='distro_result_td distro_result_$key $class'><a href='http://$info' target='_blank'>link</a></td>";
					}else if( $key !='id')
						$ret_str.="<td class='distro_result_td distro_result_$key $class'>$info</td>";
				}
				$ret_str.="<td class='distro_result_td distro_result_edit $class' onclick= 'edit_distro(\"{$result['id']}\")'>edit</td>";
				$ret_str.="</tr>";
			}	
		}
		return $ret_str;
	}
	function get_distro_info($params){
		$id= $params['distro_id'];
		$result= mysqli_fetch_assoc(mlavu_query("SELECT * FROM `poslavu_MAIN_db`.`resellers` where `id`='[1]' limit 1", $id));
		unset($result['password']);
		return $result = json_encode($result);
		
	}
	function save_distro($params){
		if( $params['id']=='new_distro'){
			$query_str= "INSERT INTO `poslavu_MAIN_db`.`resellers` (";
			foreach($params as $col=> $val){
				if( $col!='id'){
					
					if($col=='password' && $val!=''){
						$query_str .="`password`,";
						$values_str.="PASSWORD('".$val."'),";
					}else{
						
						$query_str .="`".$col."`,";
						$values_str.="'".$val."',";
					}
				}
			}
			$query_str .="`signup_date`";
			$values_str.="'".date("Y-m-d H:i:s")."'";
			//$query_str=rtrim($query_str, ",");
			//$values_str=rtrim($values_str, ",");
			$query_str.=") VALUES (". $values_str.") ";
			//echo $query_str;
			mlavu_query($query_str);
			if (mlavu_dberror())
				echo mlavu_dberror();
			else
				return "success";
		}else{
			$query_str= "UPDATE `poslavu_MAIN_db`.`resellers` SET ";
			foreach($params as $col=> $val){
				if( $col!='id'){
					if($col=='password' && $val!='')
						$query_str.="`password` =PASSWORD('".$val."')";
					else
						$query_str.="`".$col."` = '".$val."',";
				}
			}
			$query_str=rtrim($query_str, ",");
			$query_str.=" WHERE `id`='{$params['id']}' LIMIT 1";
			mlavu_query($query_str);
			if (mlavu_dberror())
				echo mlavu_dberror();
			else
				return "success";
		}
		
	}	
?>
