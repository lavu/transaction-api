<?php
	/**
	
		this file is the json dumper essentiall that does the query then we get 
		the result back from it to populate the nifty dropdown box 
		
	**/
	set_time_limit(5);
	function sort_scores( $item1, $item2 ){
		if ($item1[3] > $item2[3] ){
			return -1;
		}
		if( $item1[3] < $item2[3] ){
			return 1;
		}
		return 0;
	}

	if(isset($_GET["term"])){
	
		require_once("searchParams.php");
		require_once("/home/poslavu/public_html/admin/cp/resources/lavuquery.php");
		
		$maindb = "poslavu_MAIN_db";
		$t = strtolower( $_GET['term'] );
		$q = $t;
		$t = str_split( $t );
		$q = ConnectionHub::getConn('poslavu')->escapeString($q);


		$chars = str_split( $q );
		$q = implode('%', $chars);
		$q = addslashes($q);
		$pattern = '/';
		for( $i = 0; $i < count( $chars ); $i++ ){
			$patterns .= preg_quote( $chars[$i] );
			if( $i !== count($chars)-1 ){
				$pattern .= '.*';
			}
		}
		$pattern .= '/i';

		if( ($exec = mlavu_query("select `data_name`, `company_name`, `chain_name` from `$maindb`.`restaurants` where (`data_name` like '%{$q}%' or `company_name` like '%{$q}%' or `chain_name` like '%{$q}%') and `notes` NOT LIKE '%AUTO_DELETED%'")) !== FALSE ){
		
			$results = array();
			$entries = array();
			$index = 0;

			while( $row = mysqli_fetch_assoc($exec) ){
				$results[$index] = $row;
				$entries[ $index ] = array();

				if( preg_match($pattern, $row['data_name'] ) !== FALSE ){
					$entries[$index][] = $row['data_name'];
				}

				if( preg_match($pattern, $row['company_name'] ) !== FALSE ){
					$entries[$index][] = $row['company_name'];
				}
				
				if( preg_match($pattern, $row['chain_name'] ) !== FALSE ){
					$entries[$index][] = $row['chain_name'];
				}

				$index++;
			}

			$scores = array();
			//Need to Rank in order of likelyhood
			foreach( $entries as $index => $terms ){
				foreach( $terms as $k => $term ){
					$i = 0;
					$score = 1.0;
					$display_match = '';
					$term = str_split($term);
					for( $j = 0; $j < count( $term ); $j++ ){
						if($i < count($t) && ( strtolower($term[$j]) == strtolower($t[$i]) )){
							$score *= ((count($term) - $i)-abs($j-$i))/(count($term) - $i);
							$i++;

							$display_match .= '<b>'.$term[$j].'</b>';
							continue;
						}

						$display_match .= $term[$j];
					}
					if ($i !== count( $t ) ){
						continue;
					}
					$score *= count($t)/max(count($t),count($term));
					$scores[] = array( $term, $index, $display_match, $score );
				}
			}


			uasort($scores, 'sort_scores');


			// echo '<pre style="text-align: left;">' . print_r( $scores, true ) . '</pre>';

			$search_result = array();
			foreach( $scores as $k => $score ){
				$search_result[] = $results[ $score[1] ]['company_name'] . ' (' . $results[ $score[1] ]['data_name'] . ')';
			}

			echo json_encode($search_result);

			// while ($row = mysqli_fetch_array($exec)) {
			// 	$row['company_name']=str_replace("(", "[", $row['company_name']);
			// 	$row['company_name']=str_replace(")", "]", $row['company_name']);
			
			// 	echo '" '.$row['company_name'].' ('.$row['data_name'].')",';
			// }
			
			exit;
		}
	}
	echo "No search term. please try again.";
?>
