<?php
	/**
		THis file writes the previous searches into the employee specified. the table it writes to is poslavu_MAIN_db.loggedin.  
	**/
	if(!isset($_POST['dataname']) || !isset($_POST['username'])){
		echo "no dataname passed to writePreviousSearches.";
		exit;
	}
	
	require_once(dirname(__FILE__).'/../MyAccount/userOptions.php');
	$o_user_options = new USER_OPTIONS();
	$i_max = $o_user_options->get_option('quickaccount_count');
	if ($i_max === NULL)
		$i_max = '6';
	$i_max = (int)$i_max;
	$i_max--;
	
	require_once("/home/poslavu/public_html/admin/cp/resources/lavuquery.php");
	
	$searchesArray= mysqli_fetch_assoc(mlavu_query("select `lastFiveSearches` from `poslavu_MAIN_db`.`loggedin` where `username`= '[1]'", $_POST['username']));
	$searches=$searchesArray['lastFiveSearches'];
	$resultArray=explode("|", $searches);
	$writeString="";
	if(!in_array($_POST['dataname'], $resultArray)){
	if( count($resultArray)==1)
		$writeString= "|".$_POST['dataname'];
	else if(count($resultArray) <= $i_max){
		$resultArray[]= $_POST['dataname'];
		$writeString=implode("|", array_reverse($resultArray));
	}else{
		array_pop($resultArray);
		$resultArray=array_reverse($resultArray);
		$resultArray[]=$_POST['dataname'];
		$writeString=implode("|",array_reverse($resultArray));
	}
		mlavu_query("update `poslavu_MAIN_db`.`loggedin` set `lastFiveSearches` = '[1]' where `username`='[2]'", $writeString, $_POST['username']);
	}
	echo "successfully updated search history is: ". $writeString." the dataname is: ". $_POST['dataname'];
?>