<?php
	
	require_once(dirname(__FILE__).'/../../login.php');
	require_once('/home/poslavu/public_html/admin/cp/resources/lavuquery.php');
	require_once('/home/poslavu/public_html/admin/cp/resources/core_functions.php');
	
	// get the environment set up
	session_start();
	if (!account_loggedin())
		header("Location: http://admin.poslavu.com/manage/");
	
	if (isset($_POST['username']) && isset($_POST['dataname'])) {
		$s_username = $_POST['username'];
		$s_dataname = $_POST['dataname'];
		
		// get the previous searches
		$a_last_searches_query = mysqli_fetch_assoc(mlavu_query("Select `lastFiveSearches` from `poslavu_MAIN_db`.`loggedin` where `username`='[username]'", 
			array('username'=>$s_username)));
		$a_last_searches = explode('|', $a_last_searches_query['lastFiveSearches']);
		
		// get the position of the search to be rotated
		$i_position = -1;
		foreach($a_last_searches as $k=>$s_last_search_dn) {
			if ($s_last_search_dn == $s_dataname) {
				$i_position = $k;
				break;
			}
		}
		
		// check that the dataname exists
		$b_exists = FALSE;
		$a_exists_query = ConnectionHub::getConn('poslavu')->getDBAPI()->getAllInTable('restaurants', array('data_name'=>$s_dataname), TRUE);
		if (count($a_exists_query) > 0)
			$b_exists = TRUE;
		
		// check that this user has rotation enabled
		$b_option = FALSE;
		require_once(dirname(__FILE__).'/../MyAccount/userOptions.php');
		$o_user_options = new USER_OPTIONS();
		if ($o_user_options->get_option('quickaccount_rotate') === '1')
			$b_option = TRUE;
		
		// rotate the searches
		if ($b_exists && $s_dataname != '' && $s_dataname != NULL && $i_position > -1 && $b_option) {
			$a_new_searches = array($s_dataname);
			foreach($a_last_searches as $k=>$s_last_search_dn) {
				if ($k != $i_position) {
					$a_new_searches[] = $s_last_search_dn;
				}
			}
			$s_new_searches = implode('|', $a_new_searches);
			mlavu_query("UPDATE `poslavu_MAIN_db`.`loggedin` SET `lastFiveSearches`='[new_searches]' WHERE `username`='[username]'",
				array('new_searches'=>$s_new_searches, 'username'=>$s_username));
		}
	}
	
?>
