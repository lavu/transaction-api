<?php

	require_once("/home/poslavu/public_html/admin/cp/resources/lavuquery.php");
	require_once("/home/poslavu/public_html/admin/cp/resources/json.php");
	
	if(isset($_POST['username'])){
		$lastSearches=mysqli_fetch_assoc(mlavu_query("Select `lastFiveSearches` from `poslavu_MAIN_db`.`loggedin` where `username`='[1]'", $_POST['username']));
		$a_lastSearches = explode("|", $lastSearches['lastFiveSearches']);
		foreach($a_lastSearches as $k=>$s_dataname) {
			$a_restaurants = ConnectionHub::getConn('poslavu')->getDBAPI()->getAllInTable('restaurants', array('data_name'=>$s_dataname), TRUE, array('selectclause'=>'`id`'));
			if (count($a_restaurants) == 0) {
				$a_lastSearches[$k] = array('dataname'=>$s_dataname, 'restaurant_id'=>0);
			} else {
				$a_lastSearches[$k] = array('dataname'=>$s_dataname, 'restaurant_id'=>$a_restaurants[0]['id']);
			}
		}
		echo LavuJson::json_encode($a_lastSearches);
	}
?>