<?php
	//ini_set("display_errors",'1');
    /**
     * This is for the results that get displayed when multiple entries want to be displayed
    **/
    require_once(dirname(__FILE__)."/../../../sa_cp/billing/package_levels_object.php");
    global $o_package_container;
    if (!isset($o_package_container))
    	$o_package_container = new package_container();

    if(isset($_GET["choice"])){
   		require_once("/home/poslavu/public_html/admin/cp/resources/lavuquery.php");
		$maindb = "poslavu_MAIN_db";
		//echo $_GET['choice'];
		$search_term = strtolower($_GET["choice"]);
		$search_cols=array("`restaurants`.`company_name`","`restaurants`.`company_code`","`signups`.`company`","`signups`.`firstname`","`signups`.`lastname`","`restaurants`.`data_name`","`restaurants`.`chain_name`");

		$join = " LEFT JOIN `poslavu_MAIN_db`.`signups` ON (`poslavu_MAIN_db`.`restaurants`.`data_name` = `poslavu_MAIN_db`.`signups`.`dataname`)";
		$orderby = "`company_name` ASC";

		if( $search_term!= ''){

			if(is_numeric($search_term) && $_GET['useID']){

				$cust_query = mlavu_query("SELECT `data_name` FROM `poslavu_MAIN_db`.`restaurants` WHERE `id`='[1]' and `notes` NOT LIKE '%AUTO_DELETED%'" , $search_term);
				if(mysqli_num_rows($cust_query)){
					$result=mysqli_fetch_assoc($cust_query);
					echo "
					<script type='text/javascript'>

						populateResults(\"".$result['data_name']."\");
						$('.resultTable').css('display','none');
						$('.floatingTempResult').css('display', 'none');
						$('.resultTable').css('display','none');
						$( '#tabs').css('display', 'block');

						exit();
						</script> ";
				}
				else
					echo "<script type='text/javascript'> alert('No valid company found with id: ". $search_term."');</script>";

				exit;
			}
			else{
				$cond="(";
				//echo $search_term;
				foreach($search_cols as $col){
					$cond.=$col." like '%".ConnectionHub::getConn('poslavu')->escapeString($search_term)."%' or ";
				}

				$cond.=" 1=0)";
			}
		}else
			$cond ="1=1";

		$a_rows = array();

		$cond .= " AND `restaurants`.`data_name` != ''";
		if( isset($_GET["email"]) && $_GET["email"]!='undefined')
	    	$cond.=" and `restaurants`.`email` like '%".$_GET["email"]."%'";
	    if( isset($_GET["lname"]) && $_GET["lname"]!='undefined')
	    	$cond.=" and `signups`.`lastname` like '%".$_GET["lname"]."%'";
	    if( isset($_GET["fname"]) && $_GET["fname"]!='undefined')
	    	$cond.=" and `signups`.`firstname` like '%".$_GET["fname"]."%'";
	    if( isset($_GET["compCode"]) && $_GET["compCode"]!='undefined')
	    	$cond.=" and `restaurants`.`company_code` like '%".$_GET["compCode"]."%'";
		if( isset($_GET["status"])&& $_GET["status"]!='undefined')
	    	$cond.=" and `restaurants`.`license_status` like '%".$_GET["status"]."%'";
	    if( isset($_GET["enabled"])&& $_GET["enables"]!='undefined')
	    	$cond.=" and `restaurants`.`disabled` like '%".$_GET["enabled"]."%'";
	    if( isset($_GET["level"])&& $_GET["level"]!='undefined'){
	    	if($_GET['level']=='Lite') {
	    		$cond.=" AND `restaurants`.`lavu_lite_server` LIKE '%admin.lavulite.com%' ";
	    	} else {
	    		$a_levels = $o_package_container->get_levels_of_similar_licenses(0, $_GET['level']);
	    		$cond.=" AND `signups`.`package` IN ('".implode("','", $a_levels)."')";
	    	}
		}
		if( isset($_GET["city"])&& $_GET["city"]!='undefined')
	    	$cond.=" and `signups`.`city` like '%".$_GET["city"]."%'";
	    if( isset($_GET["state"])&& $_GET["state"]!='undefined')
	    	$cond.=" and `signups`.`state` like '%".$_GET["state"]."%'";
	    if( isset($_GET["zip"])&& $_GET["zip"]!='undefined')
	    	$cond.=" and `signups`.`zip` like '%".$_GET["zip"]."%'";
	    if( isset($_GET["date"])&& $_GET["date"]!='undefined'){
	    	$date = date("Y-m-d", strtotime($_GET['date']));
	    	$cond.=" and `restaurants`.`last_activity` > '".$date."'";
	    }
	    if ( isset($_GET['useip'])) {
	    	$a_rows = array_merge($a_rows, getRowsFromIP($_GET['choice']));
	    }

	    $query_string = "SELECT *, `poslavu_MAIN_db`.`restaurants`.`id` as `id` from `poslavu_MAIN_db`.`restaurants`{$join} WHERE ({$cond}) AND `restaurants`.`notes` NOT LIKE '%AUTO_DELETED%' AND `restaurants`.`company_name` !='' ORDER BY $orderby";
		$cust_query = mlavu_query($query_string);

		//echo "SELECT *, `poslavu_MAIN_db`.`restaurants`.`id` as `id` from `poslavu_MAIN_db`.`restaurants`".$join." where $cond ORDER BY $orderby";
		$res="";

		while($row = mysqli_fetch_assoc($cust_query)){
			$a_rows[] = $row;
		}

		foreach($a_rows as $row) {
			$row['package'] = $o_package_container->get_plain_name_by_attribute('level', $row['package']);
			if ($row['package'] == '')
				$row['package'] = 'none';

			if( $row['disabled']==0)
				$row['disabled']= "<font color='#aecd37'>E</div>";
			else
				$row['disabled']= "<font color='#9D1309'>D</div>";


			if( $row['license_status']=='Paid')
				$row['license_status']='<font color="#488214">Paid</div>';
			else
				$row['license_status']='<font color="#9D1309">'.$row['license_status'].'</div>';
			$res.="
			<tr class= 'rowResult'
			";
			 if( $row['lavu_lite_server']!=='' ) $res.='style="background-color:#6B6B6B; color:white"';

			$res.= ">
				<td class='acompany_name'>".$row['company_name']."</td>
				<td class='acompany_code' '>".$row['company_code']."</td>

				<td class='adisabled'><center>".$row['disabled']."</center></td>
				<td class='alast_activity'>".$row['last_activity']."</td>
				<td class='acreated'>".$row['created']."</td>
				<td class='alicense_status'><center>".$row['license_status']."</center></td>";
				if($row['lavu_lite_server']!=='')
					$res.="<td class='apackage_status'><center>Lite</center></td>";
				else
					$res.="<td class='apackage_status'><center>".$row['package']."</center></td>";

				$res.="<td class='aemail'>".$row['email']."</td>
				<td class='aphone' style='text-align:center'>".preg_replace("/[^0-9,.]/", "", $row['phone'])."</td>
				<td class='afirstname'>".$row['firstname']."</td>
				<td class='alastname'>".$row['lastname']."</td>
				<td class='acity'>".$row['city']."</td>
				<td class='astate'><center>".$row['state']."</center></td>

				<!--<td class='azip'><center>".$row['zip']."</center></td>-->
				<td class='achain_name'>".$row['chain_name']."</td> ";

			if( $row['lavu_lite_server'] ==''){
				$res.="
				<td ><center><a href='misc/MainFunctions/custLogin.php?mode=login&custID=".$row['id']."&custCode=".$row['company_code']."' target='_blank'>CCP</a><center></td>

			</tr>";
			}
			else
				$res.="<td ><center><a href='http://admin.lavulite.com/manage/misc/MainFunctions/custLogin.php?site=new&mode=login&custID=".$row['id']."&custCode=".$row['company_code']."' target='_blank'>CCP</a><center></td>

			</tr>";
		}
		$numRows= mysqli_num_rows($cust_query);
		$res.='
		<script language=text/javascript>
				$(document).ready(function(){
					$(function(){
						var arr = [];

							$("table.resultTable > tbody > tr.rowResult").click(function(){
							  $("#numResultsTable").css("display","none");
							  arr = $(this).find("td").map(function(){
							  	return this.innerHTML;
							  }).get();

							  if(arr[1]!="Company Code "){
							  	 $("#numResultsTable").css("display","none");
							  	loading();
							  	populateResults(arr[1]);
							  }

						});
					});
				});
			$("#numResultsTable").css("display","block");
			$("#numResults").html("Number of Results: '.$numRows.'");
	    </script>
		';
		echo $res."";

    }else{
	    echo "not set";
    }

	function getRowsFromIP($s_ip) {
		global $maindb;
		if (!isset($maindb))
			$maindb = 'poslavu_MAIN_db';
		$a_retval = array();

		$ipquery = mlavu_query("SELECT `users` FROM `[maindb]`.`login_log` WHERE `ipaddress`='[ipaddress]' ORDER BY `date` DESC LIMIT 1",
			array('maindb'=>$maindb, 'ipaddress'=>$s_ip));
		if ($ipquery === FALSE || mysqli_num_rows($ipquery) == 0) {
			error_log('no ips found (db '.$maindb.') (ip '.$s_ip.')');
			return array();
		}
		$a_ip_row = mysqli_fetch_assoc($ipquery);
		$a_users = explode(',', $a_ip_row['users']);

		$a_datanames_searched = array();
		foreach($a_users as $s_user) {
			$dataname_query = mlavu_query("SELECT `dataname` FROM `[maindb]`.`customer_accounts` WHERE `username`='[username]'",
				array('maindb'=>$maindb, 'username'=>$s_user));
			if ($dataname_query === FALSE || mysqli_num_rows($dataname_query) == 0) {
				error_log('no datanames found (username '.$s_user.')');
				continue;
			}

			while ($a_dataname_row = mysqli_fetch_assoc($dataname_query)) {
				$s_dataname = $a_dataname_row['dataname'];
				if (in_array($s_dataname, $a_datanames_searched)) {
					error_log('dataname already searched');
					continue;
				}
				$join = " LEFT JOIN `poslavu_MAIN_db`.`signups` ON `poslavu_MAIN_db`.`restaurants`.`signupid` = `poslavu_MAIN_db`.`signups`.`id`";
				$orderby = "`company_name` ASC";
				$cust_query = mlavu_query("SELECT *, `[maindb]`.`restaurants`.`id` as `id` from `poslavu_MAIN_db`.`restaurants`".$join." WHERE `data_name`='[dataname]' AND `restaurants`.`notes` NOT LIKE '%AUTO_DELETED%' and `restaurants`.`company_name` !='' ORDER BY $orderby",
					array('maindb'=>$maindb, 'dataname'=>$s_dataname));
				if ($cust_query === FALSE || mysqli_num_rows($cust_query) == 0) {
					error_log('no restaurants found');
					continue;
				}
				while($row = mysqli_fetch_assoc($cust_query))
					$a_retval[] = $row;
				$a_datanames_searched[] = $s_dataname;
			}

		}
		return $a_retval;
	}


?>
