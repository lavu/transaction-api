<?php
	session_start();
	require_once($_SERVER['DOCUMENT_ROOT']."/manage/error_config.php");
	require_once("/home/poslavu/public_html/admin/cp/resources/lavuquery.php");
	require_once("../../login.php");
	$username='';
	if(isset($_POST['username'])){
		//echo print_r($_POST,1);
		$username=$_POST['username'];
		//echo "The username is $username";
	}
	else{
		echo "An Error has occurred, please try again. ";
		return;
	}
	
	$retString=" 
	<script type='text/javascript' src='/manage/js/Tabs/myAccount.js'> </script>";
	$retString.="<div id='myAccountHeader'>MyAccount</div>
			<div id='myAccountTabContainer'>
			<span class='myAccountTabs myAccountTabSelected' onclick='get_area(\"myAccountAreaContent\",$(this)) '> Time Off </span>
			<span class='myAccountTabs' onclick='get_area(\"messagesArea\",$(this))'> Messages </span>
			<span class='myAccountTabs' onclick='get_area(\"accountInfo\",$(this))'>Edit Account Info</span>
			<span class='myAccountTabs' onclick='get_area(\"myAccountOptionsContent\",$(this))'>Manage Options</span>";
			if( can_access("company_calendar") )
				$retString.="<span class='myAccountTabs' onclick='get_area(\"companyDaysOff\",$(this))'>Company Days Off</span>";
			if( can_access("edit_user_accesses") )
				$retString.="<span class='myAccountTabs' onclick='get_area(\"editUserAccesses\",$(this))'>Edit User Accesses</span>";
		$retString.="</div>
		<div id='myAccountArea'>
			<div id='myAccountAreaContent'>";

	
	if(can_access('scheduling')){

		$retString.=createRequestSection($username);	
		$retString.=getLateNotices();
		$retString.=getTimeOffRequests()."</div>";
		
	}else{
		$retString.=createRequestSection($username);	
		$retString.=getTimeOffRequests()."</div>";		
	}
	if(can_access("company_calendar")){
		$retString.=getCompanyDaysOff();
	}
	$retString.=getEditUserAccesses();
	
	$accountInfo= getAccountInfo($username);
	
	$retString.="
	<div id='messagesArea' style='display:none' >
			<div id='messagesContainer'>
				<div id='tabContainer'>
					<div id='tabContainerScroller'>
						<span id='inbox'  class='infoTab' onclick='openMessagesTab($(this))'>Inbox </span>
						<span id='outbox' class='infoTab' onclick='openMessagesTab($(this))'>Outbox </span>
						<span id='compose' class='infoTabSelected' onclick='openMessagesTab($(this))'>Compose </span>
					</div>
				</div>
				<div id='messages'>
					<select id='group' onChange='if(\$(\"#group\").val()==\"user\"){\$(\"#user\").css(\"display\",\"inline-block\");}else{\$(\"#user\").css(\"display\",\"none\");\$(\"#user\").val(\"\");} '>
						<option value='lavu'>Lavu</option>
						<option value='support'>Support</option>
						<option value='dev'>Dev</option>
						<option value='user'>User</option>
					</select>	
					<select id='user' style='display:none; color:black;'>
						<option value=''></option>
						";
							$query=mlavu_query("SELECT `username`, `email` FROM `poslavu_MAIN_db`.`accounts` WHERE `type` !='terminated' AND `type` !='language' AND `type` != 'other' ORDER BY `username` asc ");
							while($read=mysqli_fetch_assoc($query))
								$retString.= "<option value='".$read['username']."'>".$read['username']."</option>";
					
				$retString.="</select>
					<br/>
					<textarea id='message' cols='50' rows='10'></textarea>  
					<br/>
					<input type='submit' class='submitButton' value='submit' onclick='createMessage($(\"#group\").val(), $(\"#user\").val(),$(\"#message\").val() )' /> 
				</div>
			</div>
		</div>
		<div id='accountInfo'>
			<div class='infoContainer'>
				<form id='userForm'>
					<table class='infoTable' cellpadding='10'>
						<thead>
							<tr>
								<th colspan=2>
									Edit Your Info
								</th>				
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>
									Phone						
								</td>
								<td>
									<input type='text' name='phone' value='{$accountInfo['phone']}' />
								</td>
							</tr>
							<tr>
								<td> 
									Email 
								</td>
								<td>
									<input type='text' name='email' value='{$accountInfo['email']}' />
								</td>
							</tr>
							<tr> 
								<td> 
									Password
								</td>
								<td>
									<input name='password' type='password' />
								</td>
							</tr>
							<tr>
								<td>
									Re-enter Password
								</td>	
								<td>
									<input name='password2' type='password' />
								</td>
							</tr>
							<tr>
								<td><input type='hidden' name='username' value='$username'></td><td style='text-align:right'><input type=button value='save' onclick='if( $(\"[name=password]\").val() == $(\"[name=password2]\").val() ) saveAccountInfo($(\"#userForm\")); else alert(\"Passwords do not match!\")' ></td>
							</tr>
						</tbody>
						<tfoot></tfoot>
					</table>
				</form>
			</div>				
		</div>
	";
	$advanced_tools_access_array= array(
		"Migrate Account" 	 =>"technical",
		"Extensions"	 	 =>"technical", 
		"Statistics"	     =>"customers",
		"List Local Servers" =>"customers",
		"Distro Tools"	  	 =>array("distros_add_license", "distros_remove_license", "distros_view_licenses"),
		"Edit Modules"	  	 =>"module",
		"Create Account"  	 =>"create_account",
		"Upload Documents"	 =>"upload_documents",
		"Subsite Builder" 	 =>"website_builder",
		"Backend Messages"	 =>"backend_messages");
	$access_array= array();
	$counter=0;
	foreach( $advanced_tools_access_array as $name =>$access){
		if(is_array($access)){
			$allowed=false;
			foreach( $access as $a){
				if(can_access($a)){
					$allowed=true;
					break;
				}	
			}
			if($allowed)
				$access_array[]=array(($counter++),$name);
		}else{
			if(can_access($access))
				$access_array[]=array(($counter++),$name);
		}
	}

	
	
	$a_options = array(
		array('name'=>'quickaccount_count', 'description'=>'Number of quick access account tabs displayed:', 'type'=>'number', 'default'=>'6', 'accesses'=>array('customers')),
		array('name'=>'quickaccount_rotate', 'description'=>'Rotates clicked quick access tabs back to the beggining:', 'type'=>'select', 'options'=>array(array('0','Off'),array('1','On')), 'default'=>'0', 'accesses'=>array('customers')),
		array('name'=>'advanced_tools_default', 'description'=>'Changes which tab loads by default in Advanced Tools', 'type'=>'select', 'options'=>$access_array, 'default'=>'0', 'accesses'=>array('customers')),
		array('name'=>'display_company_name', 'description'=>'Choice of displaying the currently selected account at the top of the page', 'type'=>'select', 'options'=>array(array('top','Display'),array('none',"Don't display")), 'default'=>'none', 'accesses'=>array('customers')),
	);
	$retString .= "
		<div id='myAccountOptionsContent' style='display:none;'>
			<div class='infoContainer' style='width:600px;'>
				".build_options($a_options)."
			</div>
		</div>
	";
	echo $retString;
	function getAccountInfo($username){
		$query=mysqli_fetch_assoc(mlavu_query("select * from `poslavu_MAIN_db`.`accounts` WHERE `username`='[1]'",$username));
		return $query;
	}
	function getCompanyDaysOff(){
	 	$query= mlavu_query("SELECT * FROM `poslavu_MAIN_db`.`employee_holidays` order by `start_date` asc");
	 	
	 	$str="
	 		<div id='companyDaysOff'>
	 			<div class='days_off_container'>
		 			<table style='margin:0 auto' cellpadding='10'>
		 				<tr>
		 					<th class='th_days_off_header' colspan='2'>
		 						Current Listed Days Off
		 					</th>
		 				</tr>
		 				<tr>
		 					<td>Holiday</td>
		 					<td style='text-align:center'>Date</td>
		 				</tr>";
		 				while($result= mysqli_fetch_assoc($query))
		 					if($result['start_date']== $result['end_date'])
		 						$str.="<tr><td>".$result['name']."</td><td>".$result['start_date']."</td>";

		 					else
		 						$str.="<tr><td>".$result['name']."</td><td>".$result['start_date']. " through ". $result['end_date']."</td>";
		 				
		 				$str.=" 		
		 				<tr>
		 					<td colspan='2' onclick='addDayOff($(this))' style='border-top:1px solid black'><div  class='add_days_off_button'> Add Day Off</div>
		 						<div class='add_days_off'>
		 							<form id='mai_form'>
		 								<table>
		 									<tr>
		 										<td colspan='2'>Start and End Dates:
		 											<input type= 'text' class='start_picker' name='start_date'/> 
		 											<input type= 'text' class='end_picker' name='end_date'/>
		 										</td>
		 									</tr>
		 									<tr>
		 										<td>Holiday Name: 
		 											<input type='text' name='holiday_name'>
		 										</td>
		 										<td>
		 											<input type='button' onclick='saveNewHoliday($(\"#mai_form\").serialize())'value='Save Holiday'>
		 										</td>
		 									</tr>
		 								</table>
		 							</form>
		 						</div>
		 					</td>
		 				</tr>
		 			</table>
	 			</div>
	 		</div>";
	 		
	 	return $str; 
	}
	function getEditUserAccesses() {
		
		// check if the user has access
		if( !can_access("edit_user_accesses") )
			return "";
		require_once(dirname(__FILE__).'/../../../cp/resources/json.php');
		
		// get all accesses
		$a_all_accesses = get_all_all_accesses();
		$s_all_accesses = LavuJson::json_encode($a_all_accesses);
		
		// get the user accesses
		$a_user_accesses = get_all_user_accesses($a_all_accesses);
		$s_user_accesses = LavuJson::json_encode($a_user_accesses);
		
		// draw it!
		ob_start();
		?>
		<div id="editUserAccesses" style='display:none'>
			<div class="editor" style="display:table; margin:20px auto; border:1px solid lightgray;">
			</div>
		</div>
		<style type="text/css">
			.editor tr {
				background-color:white;
			}
			.editor tr:hover {
				background-color:lightgray;
			}
			.editor td {
				overflow: hidden;
			}
			.editor .rotate {
				transform: rotate(270deg);
				-ms-transform: rotate(270deg);
				-webkit-transform: rotate(270deg);
				width: 20px;
				position: absolute;
				bottom: 0px;
			}
			.editor .highlighted {
				background-color:lightgray;
			}
			.editor .popup {
				position: fixed;
				padding: 10px;
				background-color: white;
				color: black;
				border: 1px solid black;
			}
		</style>
		<?php
		$s_dom = ob_get_contents();
		ob_end_clean();
		ob_start();
		?>
		<script type="text/javascript">
			if (typeof(Edit) == 'undefined')
				Edit = {};
			Edit.Accesses = {
				allAccesses: <?= $s_all_accesses ?>,
				userAccesses: <?= $s_user_accesses ?>,
				jeditor: $('#editUserAccesses').children('.editor'),
				popupTimer: null,
				draw: function() {
					var table = "<table>";
					var header = "<thead><tr class='header'><td></td>";
					var count = 0;
					$.each(this.allAccesses, function(k,access){
						header += "<td class='"+count+"' style='position:relative; height:250px;' onmouseover='Edit.Accesses.drawFiles("+count+");'><div class='rotate'>"+access.name+"</div><input type='hidden' name='files' value='"+access.files+"' /><input type='hidden' name='name' value='"+access.name+"' /></td>";
						count++;
					});
					header += "<td></td></tr></thead>";
					var body = "<tbody>";
					$.each(this.userAccesses, function(k,user){
						body += "<tr><td class='user'>"+user.fullname+"<input type='hidden' name='fullname' value='"+user.fullname+"' /><input type='hidden' name='username' value='"+user.username+"' /></td>";
						var count = 0;
						$.each(user.access, function(k,access){
							body += "<td class='"+count+"' onmouseover='Edit.Accesses.drawStatus(this, "+count+"); Edit.Accesses.highlightAccess("+count+");' onmouseout='Edit.Accesses.unhighlightAccess("+count+");'><input type='checkbox' "+(access.isset ? "checked" : "")+" /></td>";
							count++;
						});
						body += "<td><input type='button' onclick='Edit.Accesses.saveUser(this);' value='Save'></td></tr>";
					});
					body += "</tbody>";
					table += header + body + "</table>";
					this.jeditor.html("");
					this.jeditor.append(table);
				},
				highlightAccess: function(which) {
					$.each(this.jeditor.find("."+which), function(k,v) {
						v = $(v);
						if (!v.hasClass('highlighted'))
							v.addClass('highlighted');
					});
				},
				unhighlightAccess: function(which) {
					this.jeditor.find("."+which).removeClass('highlighted');
				},
				drawFiles: function(which) {
					var jelement = $(this.jeditor.find('.'+which)[1]);
					var filenames = this.getAccess(which).files.replace(/(, )/g, ',<br />');
					if (filenames == '')
						filenames = 'N/A';
					else
						filenames = 'Files:<br />'+filenames;
					this.drawPopup(jelement, filenames);
				},
				drawStatus: function(element, which) {
					var jelement = $(element);
					var fullname = this.getUser(element).fullname;
					var accessname = this.getAccess(which).name;
					var text = accessname+",<br />"+fullname;
					this.drawPopup(jelement, text);
				},
				drawPopup: function(jelement, text) {
					var left = parseInt(jelement.offset().left)+20;
					var top = parseInt(jelement.offset().top)+20 - document.documentElement.scrollTop - document.body.scrollTop;
					var popup = "<div class='popup' style='left:"+left+"px; top:"+top+"px;' onmouseover='$(this).remove();'>"+text+"</div>";
					var jpopup = $(popup);
					this.jeditor.find(".popup").remove();
					this.jeditor.append(jpopup);
					if (this.popupTimer !== null)
						clearTimeout(this.popupTimer);
					this.popupTimer = setTimeout(function(){ jpopup.hide(200); }, 1500);
				},
				getUser: function(element) {
					var jelement = $(element);
					while (jelement[0].nodeName.toLowerCase() !== 'td')
						jelement = jelement.parent();
					var juser = jelement.siblings('.user');
					var retval = {};
					$.each(juser.find("input"), function(k,input){
						var jinput = $(input);
						retval[jinput.attr('name')] = jinput.val();
					});
					return retval;
				},
				getAccess: function(which) {
					var jth = this.jeditor.find('tr.header').find('.'+which);
					var retval = {};
					$.each(jth.find("input"), function(k,input){
						var jinput = $(input);
						retval[jinput.attr('name')] = jinput.val();
					});
					return retval;
				}
			};
			Edit.Accesses.draw();
		</script>
		<?php
		$s_javascript = ob_get_contents();
		ob_end_clean();
		
		// clean the output
		$s_dom = str_replace(array("\n", "\r", "\n\r"), "\n", $s_dom);
		$s_javascript = str_replace(array("\n", "\r", "\n\r"), "\n", $s_javascript);
		
		return $s_dom.$s_javascript;
	}
	function getLateNotices(){
		$toffRequestQuery=mlavu_query("select * from `poslavu_MAIN_db`.`employeeTimeOff` where `minsLate` is NOT NULL AND DATE_SUB(CURDATE(),INTERVAL 7 DAY) <=DATE(`submitDate`)  order by `endDate` asc ", date("Y-m-d"));

		$requests="
		<div id='late_notices'>
			<table class='LateNotices' cellpadding='8'>
				<thead>
					<tr>
						<th id='late_notice_username' style='cursor:pointer;'>Name</th>
						<th id='late_notice_submit_date' style='cursor:pointer'>Submit Date</th>
						<th id='late_notice_mins_late' style='cursor:pointer'>Minutes Late</th>
						<th id='late_notice_message' style='cursor:pointer'>Message</th>
						
					</tr>
				</thead>
				<tbody>";

		
		while( $result= mysqli_fetch_assoc($toffRequestQuery)){
		$name_result= mysqli_fetch_assoc(mlavu_query("SELECT `firstname`, `lastname` FROM `poslavu_MAIN_db`.`accounts` WHERE `username` LIKE '[1]'", $result['username']));
			$requests.="
					<tr>
						<td class='tdUnderline'>".$name_result['firstname']." ".$name_result['lastname']."</td>
						<td class='tdUnderline'>".$result['submitDate']."</td>
						<td class='tdUnderline'>".$result['minsLate']."</td>
						<td class='tdUnderline'>".$result['message']."</td>
					</tr>";
		}
		$requests.="</tbody></table></div>";
			return $requests;
		
	}
	function createRequestSection($username){
		
		$retString="
		<div class='floater'>
			<table id='accountOptions' cellpadding='10'> 
				<tr rowspan=2 >
					<td>
						Going to Be Late: <input type='checkbox' id='late' onchange='switchInput($(this))'/>
						<span class='timeOffType'> 
							Start: <input type= 'text' class='datepickerStart' />
							End: <input type= 'text' class='datepickerEnd' /> 
						</span>
						<span class='lateType' style='display:none'> 
						";
							$retString.='<select id="minsLate"> ';
							for($i=5; $i<121; $i+=5){
								$retString.="<option value='$i'> $i </option>";
							}
		
							$retString.="</select>";
						$retString.="</span>
	
						
						<!--Request Coverage <input type='checkbox' id='cover'>--></td>
				</tr>
				<tr>
				
					<td colspan=4>
						<textArea id='timeOffReason' name=reason placeholder='Reason for time off'></textArea>
					</td>
				</tr>
				<tr>
					
					<td>
						<input style='float:left' type=submit onclick=\"if($('#late').is(':checked')){submitLateNotice( $('#timeOffReason').val(), $('#minsLate').val(),'".$username."');}else{ sendTimeOffRequest($('.datepickerStart').val(),$('.datepickerEnd').val(),$('#timeOffReason').val(), '".$username."');}\">
					</td>
				</tr>
			</table>
		</div>";
		
		return $retString;
	}
	function createSchedulingSection($username){
		$retString ="
		<div id='dialog' title='Scheduling Request'  >
			<p>Confrim or Deny this request</p>
		</div>
		
		<table id='accountOptions' style='background-color:rgb(212,212,212);'> 
			<tr>
				<th colspan=2>Scheduling</th>
			</tr>
			<tr>
				<td>Current Requests </td>
				<td>".getTimeOffRequests()."</td>
			</tr>
		</table>";
		
		return $retString;
	}
	function getTimeOffRequests(){
		
		$requests="
		<div class='floatRight'>
			<table class='timeOffRequests' cellpadding='5'>
				<thead>
					<tr>
						<th id='time_off_username' style='cursor:pointer'>Name</th>
						<th id='time_off_request_date' style='cursor:pointer'>Request Date</th>
						<th id='time_off_start_date' style='cursor:pointer'>Start Date</th>
						<th id='time_off_end_date' style='cursor:pointer'>End Date</th>
						<th id='time_off_message' style='cursor:pointer'>Message</th>
						<th id='time_off_approval_status' style='cursor:pointer'>Approval Status</th>
					</tr>
				</thead>";
				//$toffRequestQuery=mlavu_query("select * from `poslavu_MAIN_db`.`employeeTimeOff` WHERE DATE(`endDate`) >= '[1]' ", date("Y-m-d"));
		if(can_access('scheduling'))
			$toffRequestQuery=mlavu_query("select * from `poslavu_MAIN_db`.`employeeTimeOff` where `minsLate` is NULL AND `cancelled` is NULL AND `approved`='pending' order by `endDate` asc ", date("Y-m-d"));
		else
			$toffRequestQuery=mlavu_query("select * from `poslavu_MAIN_db`.`employeeTimeOff` where `username`= '[1]' AND DATE(`endDate`) >= '[2]' AND `cancelled` is NULL ", $_POST['username'], date("Y-m-d"));
		
		
		while( $result= mysqli_fetch_assoc($toffRequestQuery)){
			$name_result= mysqli_fetch_assoc(mlavu_query("SELECT `firstname`, `lastname` FROM `poslavu_MAIN_db`.`accounts` WHERE `username` LIKE '[1]'", $result['username']));
			$requests.="
					<tr>
						<td class='tdUnderline'>".$name_result['firstname']." ".$name_result['lastname']."</td>
						<td class='tdUnderline'>".$result['submitDate']."</td>
						<td class='tdUnderline'>".$result['startDate']."</td>
						<td class='tdUnderline'>".$result['endDate']."</td>
						<td class='tdUnderline'>".$result['message']."</td>";
			if(can_access('scheduling')){
				if(stristr($result['username'], $_POST['username']) && strtotime($result['startDate']) > strtotime(date("Y-m-d"))){
					$requests.="<td style='cursor:pointer;' class='tdUnderline opener' onclick='$(\"#dialog\").html(\"Do You Want to Cancel this request?\"); $( \"#dialog\" ).dialog(\"option\", \"buttons\", [ { text: \"Yes\", click: function() { $( this ).dialog(\"close\");cancel_request(\"".$_POST['username']."\", \"".$result['ID']."\"); } } ] );
 $(\"#dialog\").dialog(\"open\"); '>".$result['approved']."</td></tr>";
				}else{
					$requests.="<td style='cursor:pointer;' class='opener tdUnderline' onclick=\"reset_dialog();clickedUsername='".$result['username']."'; clickedID='".$result['ID']."'; $('#dialog').dialog('open');\">".$result['approved']." By ".$result['approvedBy']."</td></tr>";
				}
			}else{
				if($result['username'] == $_POST['username'])
					$requests.="<td class='tdUnderline' onclick=\" $('#dialog').html('Do You Want to Cancel this request?');$('#dialog').dialog( 'option', 'buttons', [ { text: 'Yes', click: function() { $( this ).dialog( 'close' );cancel_request(\"".$_POST['username']."\", \"".$result['ID']."\");  } } ] );$('#dialog').dialog(\"open\"); \">".$result['approved']."</td></tr>";
				else
					$requests.="<td class='tdUnderline' >".$result['approved']."</td></tr>";
			}
		}
		$requests.="</table></div>";
		
		if(can_access('scheduling')){
			$requests.= "<div id='daysOffDialog' title='Scheduling Days'>Paid Time Off</div>";
			$requests.= "<div id='previousDaysOff' width='595px' title='Add Past Days Off'><p>Start: <input type= 'text' id='pastDaysOffStart' class='datepickerStart' />End: <input type= 'text' id='pastDaysOffEnd' class='datepickerEnd' />  </p></div>";
			$requests.= "<div id='dialog' title='ApproveTimeOff'><p></p></div>";
			$requests.= "
			<div class='floatLeft'>
			<table  class='allUserData' border='1'>
				<thead>
					<tr>
						<th id='name' style='cursor:pointer'>Name</th>
						<th id='numDays' style='cursor:pointer'>Paid Time Off</th>
						<th id='numLateDays' style='cursor:pointer'>Days Late</th>
					</tr>
				</thead>";
			
			
			$query= mlavu_query("select `username`, `firstname`, `lastname`, `type` from `poslavu_MAIN_db`.`accounts` where `lavu_admin`='1' AND `username`!= 'andy' AND `username`!='corey' AND `username` != 'richard' and `type` !='terminated'  AND `type` !='other' AND `type` !='langauge' order by `firstname` ");
			while($read= mysqli_fetch_assoc($query)){
				$requests.= "<tr>";
			
				if(isManager($_POST['username'])){
					
					$requests.="<td style='cursor:pointer' onclick= \"$('#previousDaysOff').dialog( 'option', 'minWidth', 595 ); username='".$read['username']."'; $( '#previousDaysOff' ).dialog( 'open' ); \" >".$read['firstname']." ".$read['lastname']."</td>";

				}else{	 
					$requests.="<td>".$read['firstname']." ".$read['lastname']."</td>";
				
				}
				$timeOffQuery = mlavu_query("select * from `poslavu_MAIN_db`.`employeeTimeOff` where `username` LIKE '[1]' and (`Approved` LIKE '%Approved%' OR `minsLate` IS NOT NULL) ",$read['username']);
				$holidays     = mlavu_query("select * from `poslavu_MAIN_db`.`employee_holidays`");
				$holidays_arr = array();
				
				while($result= mysqli_fetch_assoc($holidays)){
					$holidays_arr=array_merge($holidays_arr,createDateRangeArray($result['start_date'], $result['end_date']));
				}
				
				if(mysqli_num_rows($timeOffQuery)){
				
					$numDaysOff = 0;	
					$dates	    = '';
					$days	    = 0;
					$daysLate   = 0;
					$dates_off="<table> <tr><th colspan=5> Approved Requests</th></tr><tr><td class=td_under_border>Start Date</td><td class=td_under_border>End Date</td><td class=td_under_border>Request Date</td><td class=td_under_border>Message</td><td class=td_under_border>Approval Status</td></tr>";
					$dates_late="<table> <tr><th colspan=5>Days Late</th></tr><tr><td class=td_under_border>Date late</td><td class=td_under_border>Message</td><td class=td_under_border>Minutes Late</td></tr>";
					while($timeOffRead= mysqli_fetch_assoc($timeOffQuery)){
	
						$start = strtotime($timeOffRead['startDate']);
						$end   = strtotime($timeOffRead['endDate']);
						if(!$timeOffRead['minsLate']){
							$dates_off.="<tr><td>{$timeOffRead['startDate']}</td><td>{$timeOffRead['endDate']}</td><td>{$timeOffRead['submitDate']}</td><td>".
							str_replace("'", "",$timeOffRead['message'])."</td><td>{$timeOffRead['approved']} by {$timeOffRead['approvedBy']}</td></tr>";
						}else{
							$dates_late.="<tr><td>{$timeOffRead['submitDate']}</td><td>".str_replace("'", "",$timeOffRead['message'])."</td><td>{$timeOffRead['minsLate']}</td></tr>";
						}
						
						while ($start <= $end) {
						    if ((date('N', $start) <= 5) && !stristr($read['type'],'support')  ) {
						        $current = date('Y-m-d', $start);
						        if(!in_array($current, $holidays_arr)){
						        	if($timeOffRead['minsLate'])
						        		$daysLate++;
						        	else
						        		$days++;
						        	
						        }
						    }else if( stristr($read['type'],'support')){
							    $current = date('Y-m-d', $start);
						        if(!in_array($current, $holidays_arr)){
						        	if($timeOffRead['minsLate'])
						        		$daysLate++;
						        	else
						        		$days++;

						        }
						    }
						    $start += 86400;
						}
											
					}
					
					$pending_query = mlavu_query("select * from `poslavu_MAIN_db`.`employeeTimeOff` where `username` LIKE '[1]' and `Approved` LIKE '%pending%' AND `minsLate` is NULL ",$read['username']);
					$dates_off.="<tr><th colspan=5> Pending Requests </th></tr>";
					while($pending= mysqli_fetch_assoc($pending_query)){
						$dates_off.="<tr><td>{$pending['startDate']}</td><td>{$pending['endDate']}</td><td>{$pending['submitDate']}</td><td>".str_replace("'", "",$timeOffRead['message'])."</td><td>{$pending['approved']}</td></tr>";
					}
					$dates_off.="</table>";
					$dates_late.="</table>";
					$requests.="<td style='cursor:pointer'  onclick= \"$('#daysOffDialog' ).html('".$dates_off."');$( '#daysOffDialog' ).dialog( 'open' );\" ><span style='padding-right:10px; padding-left:10px; text-align:center'>".$days."</span></td>
					<td style='cursor:pointer'  onclick= \"$('#daysOffDialog' ).html('".$dates_late."');$( '#daysOffDialog' ).dialog( 'open' );\"><span style='padding-right:10px; padding-left:10px; text-align:center'>".$daysLate." </span> </td>";
					

				}
				else{
					$requests.= "<td> <span style='padding-right:10px; padding-left:10px; text-align:center'>0</span></td>
					<td><span style='padding-right:10px; padding-left:10px; text-align:center'>0</span></td>";
				}
			}
						
			$requests.= "</table></div>";
		}
		return $requests;
	}
	function isManager($name){
		$type=mysqli_fetch_assoc(mlavu_query("SELECT `type` from `poslavu_MAIN_db`.`accounts` WHERE `username`='[1]'", $name));
		if(strstr($type['type'],'management'))
			return true;
		else
			return false;
	}
	function createDateRangeArray($strDateFrom,$strDateTo){
	    // takes two dates formatted as YYYY-MM-DD and creates an
	    // inclusive array of the dates between the from and to dates.
	
	    // could test validity of dates here but I'm already doing
	    // that in the main script
		if( $strDateFrom== $strDateTo)
			return array($strDateTo);
	
	    $aryRange=array();
	
	    $iDateFrom=mktime(1,0,0,substr($strDateFrom,5,2),     substr($strDateFrom,8,2),substr($strDateFrom,0,4));
	    $iDateTo=mktime(1,0,0,substr($strDateTo,5,2),     substr($strDateTo,8,2),substr($strDateTo,0,4));
	
	    if ($iDateTo>=$iDateFrom)
	    {
	        array_push($aryRange,date('Y-m-d',$iDateFrom)); // first entry
	        while ($iDateFrom<$iDateTo)
	        {
	            $iDateFrom+=86400; // add 24 hours
	            array_push($aryRange,date('Y-m-d',$iDateFrom));
	        }
	    }
	    return $aryRange;
	}	
	function build_options($a_form_options) {
		require_once(dirname(__FILE__).'/userOptions.php');
		$o_user_options = new USER_OPTIONS();
		
		$s_retval = '';
		$i_option_id = 0;
		foreach($a_form_options as $a_option) {
			if (count($a_option['accesses']) != can_access_by_array($a_option['accesses']))
				continue;
			
			$s_value = $o_user_options->get_option($a_option['name']);
			if ($s_value === NULL) $s_value = $a_option['default'];
			$s_id = 'account_option_'.$i_option_id;
			$i_option_id++;
			
			$s_retval .= '<font style="font-weight:bold;">'.$a_option['name'].'</font> '.$a_option['description'].'<br />';
			switch($a_option['type']) {
				case 'number':
				case 'text':
					$s_retval .= '
						<input id="'.$s_id.'" type="'.$a_option['type'].'" name="'.$a_option['name'].'" value="'.$s_value.'" />';
					break;
				case 'select':
					$s_retval .= '
						<select name="'.$a_option['name'].'" id="'.$s_id.'">';
					foreach($a_option['options'] as $a_select_option) {
						if ($a_select_option[0] == $s_value) {
							$s_retval .= '
							<option value="'.$a_select_option[0].'" selected>'.$a_select_option[1].'</option>';
						} else {
							$s_retval .= '
							<option value="'.$a_select_option[0].'">'.$a_select_option[1].'</option>';
						}
					}
					$s_retval .= '
						</select>';
					break;
				case 'radio':
					$s_retval .= '<div id="'.$s_id.'">';
					foreach($a_option['options'] as $a_radio_option) {
						$checked = ($a_radio_option[0] == $s_value) ? 'CHECKED' : '';
						$s_retval .= '<input type="radio" name="'.$a_option['name'].'" value="'.$a_radio_option[0].'" '.$checked.' />'.$a_radio_option[1].'<br />';
					}
			}
			$s_retval .= '<input type="button" value="Set" onclick="set_account_option(\''.$s_id.'\');" /><br /><br />';
		}
		
		$s_retval .= '
			<script type="text/javascript">
				function set_account_option(id) {
					var jelement = $("#"+id);
					if (jelement.length == 0)
						return;
					var value = jelement.val();
					var name = jelement.attr("name");
					
					$.ajax({
						url: "./misc/MyAccount/userOptions.php",
						type: "POST",
						async: true,
						data: { name: name, value: value, set_option: 1 },
						success: function(message) {
							lavuLog(message);
						}
					});
				}
				
			</script>';
		
		return $s_retval;
	}
	
	function get_all_all_accesses() {
		$a_users = ConnectionHub::getConn('poslavu')->getDBAPI()->getAllInTable('accounts', array('username'=>'all_accesses'), TRUE);
		$s_all_accesses = $a_users[0]['access'];
		$o_accesses = LavuJson::json_decode($s_all_accesses, FALSE);
		$a_all_accesses = array('all'=>array('name'=>'all', 'files'=>''));
		$i_cutoff_time = strtotime("-15 days");
		foreach($o_accesses as $area=>$o_properties) {
			
			// check if the access was attempted to be accessed in the last 15 days
			$a_files = array();
			$b_recent = FALSE;
			foreach($o_properties->files as $filename=>$o_fileproperties) {
				$a_files[] = str_replace("'", '', $filename);
				if ((int)$o_fileproperties->last_access > $i_cutoff_time)
					$b_recent = TRUE;
			}
			if (!$b_recent)
				continue;
			
			// add the access to the list of all accesses
			$a_all_accesses[$area] = array('name'=>$area, 'files'=>implode(", ", $a_files));
		}
		
		return $a_all_accesses;
	}
	
	function get_all_user_accesses($a_all_accesses) {
		
		$a_users = ConnectionHub::getConn('poslavu')->getDBAPI()->getAllInTable('accounts', NULL, TRUE, array('whereclause'=>"WHERE `lavu_admin`='1' AND `disabled`='0' AND `type` NOT IN ('terminated','special')"));
		$a_user_accesses = array();
		foreach($a_users as $a_user) {
			
			// get a list of user accesses
			$a_has_access = explode(',', $a_user['access']);
			foreach($a_has_access as $k=>$v)
				$a_has_access[$k] = trim($v);
			
			// build of list of accesses that the user has and does not have
			$a_accesses = array();
			foreach($a_all_accesses as $a_access) {
				$s_name = $a_access['name'];
				$b_isset = in_array($s_name, $a_has_access);
				$a_accesses[$s_name] = array('name'=>$s_name, 'isset'=>$b_isset);
			}
			
			// add to the user's accesses
			$a_user_accesses[$a_user['username']] = array('username'=>$a_user['username'], 'fullname'=>$a_user['firstname'].' '.$a_user['lastname'], 'access'=>$a_accesses);
		}
		
		return $a_user_accesses;
	}
?>