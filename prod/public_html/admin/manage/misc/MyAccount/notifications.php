<?php
    require_once($_SERVER['DOCUMENT_ROOT']."/manage/error_config.php");
	require_once($_SERVER['DOCUMENT_ROOT']."/manage/misc/Standalones/json_encode.php");
	require_once($_SERVER['DOCUMENT_ROOT']."/cp/resources/lavuquery.php");
	$counter=0;
	/*
		Message statuses:
			Unviewed = 1;
			viewed   = 2;
			deleted  = 3;
	*/

	if (isset($_POST['function'])){
		if(function_exists($_POST['function'])){
			$fun = $_POST['function'];
			unset($_POST['function']);
			$fun($_POST);
			exit;
		}else{
			echo '{"error":" Function does not exist"}';
		}
	}else{
		echo '{"error":" no function sent"}';
	}

	function remove_notification($args){
		if(isset($args['userID'])){
			$uid		= $args['userID'];
			$message_id = $args['message_id'];

			mlavu_query("UPDATE `poslavu_MAIN_db`.`notifications` SET `status`='3' WHERE `receiver`='[1]' AND `id`='[2]'",$uid, $message_id);
			if (mlavu_dberror()){
				echo '{"error":"'. mlavu_dberror().'"}';
			}else{
				sleep(2);
				echo '{"status":"success"}';
			}
		}else{
			echo '{"error":"UserID not sent"}';
			return;
		}
	}
	function add_notification($args){

		if(isset($args['userID']) && isset($args['notification']) ){

			if(!is_numeric($args['userID'])){
				$list= get_user_list($args['userID']);
				foreach($list as $id){
					$args['userID']= $id;
					mlavu_query("INSERT INTO `notifications` (`message`, `sender`, `receiver`, `level`, `category`, `title`, `status`) VALUES ('[notification]', '[userID]', '[sender]', '[level]', '[category]', '[title]','1')", $args); // poslavu_MAIN_db
				}
			}else{
				mlavu_query("INSERT INTO `notifications` (`message`, `sender`, `receiver`, `level`, `category`, `title`, `status`) VALUES ('[notification]', '[sender]', '[userID]', '[level]', '[category]', '[title]', '1')", $args); // poslavu_MAIN_db
			}

			if (mlavu_dberror()){
				echo '{"error":"'. mlavu_dberror().'"}';
			}else{
				echo '{"status":"success"}';
			}
		}else{
			echo '{"error":"UserID not sent"}';
			return;
		}
	}
	function get_user_list($group){
		if( $group=='all'){
			$query = mlavu_query("SELECT `id` FROM `poslavu_MAIN_db`.`accounts` WHERE `lavu_admin`='1' AND `disabled`!='1' AND `type`!='terminated' ");
		}else {
			$query = mlavu_query("SELECT `id` FROM `poslavu_MAIN_db`.`accounts` WHERE `lavu_admin`='1' AND `disabled`!='1' AND `type`='[1]'", $group);
		}
		$return_arr= array();
		while($res= mysqli_fetch_assoc($query))
			$return_arr[]= $res;

		return $return_arr;
	}
	function get_updates($args){

		global $counter;
		$uid= $args['userID'];

		if(isset($uid)){
			if(isset($args['initialize']))
				$query= mlavu_query("SELECT * FROM `poslavu_MAIN_db`.`notifications` WHERE `receiver`='[1]' AND `status` = '1' OR `status`='2' ", $uid);	//1 is unviewd. 2. is viewd. 3 is dismissed
			else
				$query= mlavu_query("SELECT * FROM `poslavu_MAIN_db`.`notifications` WHERE `receiver`='[1]' AND `status` = '1' ", $uid);	//1 is unviewd. 2. is viewd. 3 is dismissed

			if(mysqli_num_rows($query)){
				$return_arr					 = array();
				$return_arr['notifications'] = array();

				$display_query= mlavu_query("SELECT * FROM `poslavu_MAIN_db`.`notifications` WHERE `receiver`='[1]' AND (`status` = '1' OR `status`='2') ", $uid);
				while($res = mysqli_fetch_assoc($display_query)){
					$return_arr['notifications'][] = $res;
					mlavu_query("UPDATE `poslavu_MAIN_db`.`notifications` SET `status` = '2' WHERE `id` = '[1]' ", $res['id']);
				}
				//error_log(json_encode($return_arr));
				echo json_encode($return_arr);
				return;
			}else{
				if(isset($args['initialize'])){
					echo '{"notifications":""}';
					return;
				}
				sleep(10);
				$counter++;
				if($counter > 15)
					echo '{"notifications":""}';
				else
					get_updates($args);
			}
		}else{
			echo '{"error":"UserID not sent"}';
			return;
		}
	}
?>