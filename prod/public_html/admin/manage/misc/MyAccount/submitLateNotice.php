<?php
	
	require_once ($_SERVER['DOCUMENT_ROOT']."/cp/resources/core_functions.php");
	
	if( isset($_POST['name']) && isset($_POST['minutes'])&&  isset($_POST['reason'])){
		mlavu_query("INSERT INTO `poslavu_MAIN_db`.`employeeTimeOff` (`username`,`startDate`,`endDate`,`approved`,`message`,`submitDate`,`minsLate`) VALUES ('[1]','[2]','[2]','[3]','[4]','[2]','[5]') ", 
		$_POST['name'], date("Y-m-d"),"Late Notice",$_POST['reason'],$_POST['minutes']);
		
		if (mlavu_dberror()){
			echo mlavu_dberror();
		}else{
			
			sendemail($_POST['name'],$_POST['reason']);
			echo "success";
		}
	}	
	else
		echo "no data sent"; 

	function sendemail($username, $reason) {
		$managerIdsEmailed = array();

		$user_query = mlavu_query("SELECT * FROM `poslavu_MAIN_db`.`accounts` WHERE `username`='[1]'", $username);
		if ( $user_query === false ) {
			error_log(__FILE__ ." had query error for username={$username} reason={$reason}: ". mlavu_dberror());
			return $managerIdsEmailed;
		}

		$user = mysqli_fetch_assoc($user_query);
		if ( empty($user['type']) ) {
			error_log(__FILE__ ." had blank `accounts`.`type` for username={$username} reason={$reason}: ". mlavu_dberror());
			return $managerIdsEmailed;
		}

		// Managers need approval from management_all, everyone else needs approval from their dept manager(s)
		$approving_dept = (strpos($user['type'], 'management_') !== false) ? 'all' : $user['type'];

		$managers_query = mlavu_query("SELECT * FROM `poslavu_MAIN_db`.`accounts` WHERE `type` LIKE 'management%_[type]%' AND `username`!='[username]'", array('type' => $approving_dept, 'username' => $username));
		if ( $managers_query === false ) {
			error_log(__FILE__ ." had query error for username={$username} reason={$reason}: ". mlavu_dberror());
			return $managerIdsEmailed;
		}

		while ( $manager = mysqli_fetch_assoc($managers_query) ) {
			//if (DEV) {
			//	$override_email = 'tom@lavu.com';
			//	error_log(__FILE__ ." DEV: overriding manager email ({$manager['email']}) with {$override_email}");
			//	$manager['email'] = $override_email;
			//}
			mail($manager['email'], "Late Notice for: {$username}", "{$username} is going to be late today because: {$reason}. You can view this message in manage.");
			$managerIdsEmailed[] = $manager['id'];
		}

		return $managerIdsEmailed;
	}
?>
