<?php
	
	session_start();
	require_once("/home/poslavu/public_html/admin/cp/resources/lavuquery.php");
	require_once($_SERVER['DOCUMENT_ROOT']."/manage/misc/Standalones/json_encode.php");
	if(!isset($_POST['username']) || $_POST['username']==''){
		echo "An Error Has Occurred. Please try again.";
		return;
	}
	if(isset($_POST['manager_username'])){
		if(empty($_POST['endDate']) ||empty( $_POST['startDate'])|| (strtotime($_POST['endDate']) < strtotime($_POST['startDate']) )){
			echo "Please enter a valid time off request! start date: ". $_POST['startDate']." endDate: ". $_POST['endDate'];
			return;
		}
	}else{
		if(empty($_POST['endDate']) || empty( $_POST['startDate'])|| (strtotime($_POST['endDate']) < strtotime((date('Y-m-d')))) || strtotime($_POST['endDate']) < strtotime($_POST['startDate']) ){
			echo "Please enter a valid time off request. ";
			return;
		}
	}
	
	$message= "User: ".$_POST['username']. "\r\nHas Requested ".$_POST['startDate']." through ".$_POST['endDate']." off.\r\n\r\nMessage: \r\n\r\n".
	$_POST['message']."\r\n\r\nPlease notify this user of the status of their request.\r\n\r\nThank you.";
	$subject= "Time Off Request from: ".$_POST['username'];
	
	$manager_ids= sendemail($_POST['username'],$subject, $message);
	
	$formattedStartDate= date("Y-m-d H:i:s", strtotime(str_replace('-', '/', $_POST['startDate'])));
	$formattedEndDate= date("Y-m-d H:i:s", strtotime(str_replace('-', '/', $_POST['endDate'])));
	
	//$id= mysqli_num_rows((mlavu_query("select * from `poslavu_MAIN_db`.`employeeTimeOff`"))) + 1;
	if( strpos($_POST['message'],'manager past time insert') !==false){
		mlavu_query("INSERT INTO `poslavu_MAIN_db`.`employeeTimeOff` (`username`, `startDate`, `endDate`, `approved`, `message`, `submitDate`,`approvedBy`) VALUES ('[1]','[2]','[3]','Approved! On : ".date('Y-m-d')."','[4]','[5]','[6]')", 
		$_POST['username'],$formattedStartDate,$formattedEndDate,$_POST['message'],  date('Y-m-d'), trim($_POST['manager_username']));	
	}else{
		mlavu_query("INSERT INTO `poslavu_MAIN_db`.`employeeTimeOff` (username, startDate, endDate, approved, message, submitDate) VALUES ('[1]','[2]','[3]','pending','[4]','[5]')",
		$_POST['username'],$formattedStartDate,$formattedEndDate,$_POST['message'],  date('Y-m-d'));
	}

	//echo "request sent.";
	echo json_encode($manager_ids);
	function sendemail($username, $subject, $reason) {
		$managerIdsEmailed = array();

		$user_query = mlavu_query("SELECT * FROM `poslavu_MAIN_db`.`accounts` WHERE `username`='[1]'", $username);
		if ( $user_query === false ) {
			error_log(__FILE__ ." had query error for username={$username} subject={$subject} reason={$reason}: ". mlavu_dberror());
			return $managerIdsEmailed;
		}

		$user = mysqli_fetch_assoc($user_query);
		if ( empty($user['type']) ) {
			error_log(__FILE__ ." had blank `accounts`.`type` for username={$username} subject={$subject} reason={$reason}: ". mlavu_dberror());
			return $managerIdsEmailed;
		}

		// Managers need approval from management_all, everyone else needs approval from their dept manager(s)
		$approving_dept = (strpos($user['dept'], 'management_') !== false) ? 'all' : $user['type'];

		$managers_query = mlavu_query("SELECT * FROM `poslavu_MAIN_db`.`accounts` WHERE `type` LIKE 'management%_[type]%' AND `username`!='[username]'", array('type' => $approving_dept, 'username' => $username));
		if ( $managers_query === false ) {
			error_log(__FILE__ ." had query error for username={$username} subject={$subject} reason={$reason}: ". mlavu_dberror());
			return $managerIdsEmailed;
		}

		while ( $manager = mysqli_fetch_assoc($managers_query) ) {
			//if (DEV) {
			//	$override_email = 'tom@lavu.com';
			//	error_log(__FILE__ ." DEV: overriding manager email ({$manager['email']}) with {$override_email}");
			//	$manager['email'] = $override_email;
			//}
			mail($manager['email'], $subject, $reason);
			$managerIdsEmailed[] = $manager['id'];
		}

		return $managerIdsEmailed;
	}
	function isManager($name){
		$type=mysqli_fetch_assoc(mlavu_query("SELECT `type` from `poslavu_MAIN_db`.`accounts` WHERE `username`='[1]'", $name));
		if(strstr($type['type'],'management'))
			return true;
		else
			return false;
	}

?>
