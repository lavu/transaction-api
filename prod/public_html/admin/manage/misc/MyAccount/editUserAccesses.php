<?php
	
	require_once(dirname(__FILE__).'/../../login.php');
	if (!function_exists('account_loggedin') || !account_loggedin())
		exit();
	
	if (isset($_POST['action']) && $_POST['action'] == 'saveUserAccesses' && isset($_POST['username']) && $_POST['username'] !== '' && isset($_POST['accesses']) && $_POST['accesses'] !== '') {
		if (!can_access('edit_user_accesses')) {
			echo "You don't have permission to change user accesses.";
			exit();
		}
		require_once(dirname(__FILE__).'/../../../cp/resources/lavuquery.php');
		require_once(dirname(__FILE__).'/../../../cp/resources/json.php');
		$s_username = $_POST['username'];
		$a_accesses = LavuJson::json_decode($_POST['accesses']);
		$a_access = '';
		foreach($a_accesses as $k=>$a_single_access)
			$a_access[] = $a_single_access['name'];
		$s_access = implode(',', $a_access);
		mlavu_query("UPDATE `poslavu_MAIN_db`.`accounts` SET `access`='[access]' WHERE `username`='[username]'", array('access'=>$s_access, 'username'=>$s_username));
		if (ConnectionHub::getConn('poslavu')->affectedRows() > 0)
			echo "success";
		else
			echo "failed";
		exit();
	}
	
	function editUserAccessesTostr() {
		require_once(dirname(__FILE__).'/../../../cp/resources/json.php');
		
		// get all accesses
		$a_all_accesses = get_all_all_accesses();
		
		// get the user accesses
		$a_user_accesses = get_all_user_accesses($a_all_accesses);
		$a_user_accesses = get_all_user_accesses($a_all_accesses);
		
		// stringify accesses
		$s_all_accesses = LavuJson::json_encode($a_all_accesses);
		$s_user_accesses = LavuJson::json_encode($a_user_accesses);
		
		// draw it!
		ob_start();
		?>
		<div id="editUserAccesses" style='display:none'>
			<div class="editor" style="display:table; margin:20px auto; border:1px solid lightgray;">
			</div>
		</div>
		<style type="text/css">
			.editor tr {
				background-color:white;
			}
			.editor tr:hover {
				background-color:lightgray;
			}
			.editor tr.header:hover{
				background-color:white;
			}
			.editor td {
				overflow: visible;
			}
			.editor .rotate {
				transform: rotate(314deg);
				-ms-transform: rotate(314deg);
				-webkit-transform: rotate(314deg);
				width: 20px;
				position: absolute;
				bottom: 0px;
			}
			.editor .highlighted {
				background-color:lightgray;
			}
			.editor .popup {
				position: fixed;
				padding: 10px;
				background-color: white;
				color: black;
				border: 1px solid black;
			}
			.editor .sortable {
				cursor: pointer;
			}
		</style>
		<?php
		$s_dom = ob_get_contents();
		ob_end_clean();
		ob_start();
		?>
		<script type="text/javascript">
			if (typeof(Edit) == 'undefined')
				Edit = {};
			Edit.Accesses = {
				allAccesses: <?= $s_all_accesses ?>,
				userAccesses: <?= $s_user_accesses ?>,
				jeditor: $('#editUserAccesses').children('.editor'),
				popupTimer: null,
				init: function() {
					var all = [];
					$.each(this.allAccesses, function(k,v) {
						all.push(v);
					});
					this.allAccesses = all;
					var user = [];
					$.each(this.userAccesses, function(k,v) {
						var access = [];
						$.each(v.access, function(k2,v2){
							access.push(v2);
						});
						v.access = access;
						user.push(v);
					});
					this.userAccesses = user;
				},
				draw: function() {
					
					// draw the access names
					var header = "<thead><tr class='header'><td onclick='Edit.Accesses.sort();' class='sortable'></td>";
					var count = 0;
					var all_index = 0;
					$.each(this.allAccesses, function(k,access){
						var checkbox_class = (access.name == "all" ? " all" : "");
						all_index = (access.name == "all" ? count : all_index);
						header += "<td class='"+count+" sortable' style='position:relative; height:250px;' onmouseover='Edit.Accesses.drawFiles("+count+");' onclick='Edit.Accesses.sortByAccess("+count+");'><div class='rotate'>"+access.name+"</div><input type='hidden' name='files' value='"+access.files+"' /><input type='hidden' name='name' value='"+access.name+"' /></td>";
						count++;
					});
					header += "<td></td></tr></thead>";
					
					// draw the body of the table
					var body = "<tbody>";
					$.each(this.userAccesses, function(k,user){
						
						// draw the user names
						body += "<tr><td class='user sortable' onclick='Edit.Accesses.sortByUser(this);'>"+user.fullname+"<input type='hidden' name='fullname' value='"+user.fullname+"' /><input type='hidden' name='username' value='"+user.username+"' /><input type='hidden' name='userid' value='"+user.userid+"' /></td>";
						var count = 0;
						
						// draw the access checkboxes
						var has_all = user.access[all_index].isset;
						$.each(user.access, function(k,access){
							var checkbox_class = (access.name == "all" ? " all" : "");
							var checkbox_checked = (access.isset ? "checked" : "");
							var checkbox_disabled = (has_all && access.name != "all" ? "DISABLED" : "");
							var checkbox_special_action = (access.name == "all" ? "Edit.Accesses.check_all(this);" : "");
							body += "<td class='"+count+"' onmouseover='Edit.Accesses.drawStatus(this, "+count+");' ><input type='checkbox' "+checkbox_checked+" class='"+checkbox_class+"' onchange='Edit.Accesses.updateUserAccesses(this, "+count+");"+checkbox_special_action+"' "+checkbox_disabled+" /></td>";
							count++;
						});
						
						// draw the save buttons
						body += "<td><input type='button' onclick='Edit.Accesses.saveUser(this);' value='Save'></td></tr>";
					});
					body += "</tbody>";
					
					// draw the table
					var table = "<table>";
					table += header + body + "</table>";
					this.jeditor.html("");
					this.jeditor.append(table);
				},
				check_all: function(checkbox) {
					var user = this.getUser(checkbox);
					if (checkbox.checked) {
						$.each(this.userAccesses, function(k,v){
							if (v.username == user.username) {
								$.each(Edit.Accesses.userAccesses[k].access, function(k2,v2){
									if (v2.name != "all")
										Edit.Accesses.userAccesses[k].access[k2].isset = false;
								});
							}
						});
					}
					this.draw();
				},
				updateUserAccesses: function(element, which) {
					var user = this.getUser(element);
					var access = this.getAccess(which);
					$.each(this.userAccesses, function(k,v){
						if (v.username == user.username) {
							Edit.Accesses.userAccesses[k].access[which].isset = element.checked;
						}
					});
				},
				saveUser: function(element) {
					var user = this.getUser(element);
					var accesses = {};
					$.each(this.userAccesses, function(k,v){
						if (v.username == user.username) {
							$.each(v.access, function(k2,v2){
								if (v2.isset)
									accesses[v2.name] = v2;
							});
						}
					});
					$.ajax({
						url: '/manage/misc/MyAccount/editUserAccesses.php',
						method: 'POST',
						async: true,
						cache: false,
						data: { action: 'saveUserAccesses', username: user.username, accesses: JSON.stringify(accesses) },
						success: function(m) {
							alert('Save accesses for '+user.username+': '+m);
							Lavu.Notifications.add_notification(user.userid, Lavu.currentUser.getUserID(), 1, "Your Manage access has been changed. Completely log out of Manage and log back in to see your new permissions.");
						},
						error: function(a,b,c) {
							alert('Unable to contact server to save accesses for '+user.username+'.');
						}
					});
				},
				sort: function() {
					this.allAccesses.sort(function(a,b) {
						return (a.name < b.name ? -1 : 1);
					});
					$.each(this.userAccesses, function(k,v) {
						Edit.Accesses.userAccesses[k].access.sort(function(a,b){
							return (a.name < b.name ? -1 : 1);
						});
					});
					this.userAccesses.sort(function(a,b){
						return (a.fullname < b.fullname ? -1 : 1);
					});
					this.draw();
				},
				sortByUser: function(element) {
					var user = this.getUser(element);
					var accessIndex = {};
					$.each(this.userAccesses, function(k,v) {
						if (v.username == user.username) {
							Edit.Accesses.userAccesses[k].access.sort(function(a,b){
								return (a.isset < b.isset ? 1 : (a.isset > b.isset ? -1 : (a.name < b.name ? -1 : 1)));
							});
							$.each(Edit.Accesses.userAccesses[k].access, function(k2,v2){
								accessIndex[v2.name] = k2;
							});
						}
					});
					this.allAccesses.sort(function(a,b) {
						return (accessIndex[a.name] < accessIndex[b.name] ? -1 : 1);
					});
					$.each(this.userAccesses, function(k,v) {
						Edit.Accesses.userAccesses[k].access.sort(function(a,b){
							return (accessIndex[a.name] < accessIndex[b.name] ? -1 : 1);
						});
					});
					this.draw();
				},
				sortByAccess: function(which) {
					var access = this.getAccess(which);
					this.userAccesses.sort(function(a,b) {
						return (a.access[which].isset < b.access[which].isset ? 1 : (a.access[which].isset > b.access[which].isset ? -1 : (a.fullname < b.fullname ? -1 : 1)));
					});
					this.draw();
				},
				highlightAccess: function(which) {
					$.each(this.jeditor.find("."+which), function(k,v) {
						v = $(v);
						if (!v.hasClass('highlighted'))
							v.addClass('highlighted');
					});
				},
				unhighlightAccess: function(which) {
					this.jeditor.find("."+which).removeClass('highlighted');
				},
				drawFiles: function(which) {
					var jelement = $(this.jeditor.find('.'+which)[1]);
					var filenames = this.getAccess(which).files.replace(/(, )/g, ',<br />');
					if (filenames == '')
						filenames = 'N/A';
					else
						filenames = 'Files:<br />'+filenames;
					this.drawPopup(jelement, filenames);
				},
				drawStatus: function(element, which) {
					var jelement = $(element);
					var fullname = this.getUser(element).fullname;
					var accessname = this.getAccess(which).name;
					var text = accessname+",<br />"+fullname;
					this.drawPopup(jelement, text);
				},
				hideStatus: function() {
					this.jeditor.find(".popup").remove();
				},
				drawPopup: function(jelement, text) {
					var left = parseInt(jelement.offset().left)+20;
					var top = parseInt(jelement.offset().top)+20 - document.documentElement.scrollTop - document.body.scrollTop;
					var popup = "<div class='popup' style='left:"+left+"px; top:"+top+"px;' onmouseover='$(this).remove();'>"+text+"</div>";
					var jpopup = $(popup);
					this.jeditor.find(".popup").remove();
					this.jeditor.append(jpopup);
					if (this.popupTimer !== null)
						clearTimeout(this.popupTimer);
					this.popupTimer = setTimeout(function(){ jpopup.hide(200); }, 5000);
				},
				getUser: function(element) {
					var jelement = $(element);
					while (jelement[0].nodeName.toLowerCase() !== 'tr')
						jelement = jelement.parent();
					var juser = jelement.children('.user');
					var retval = {};
					$.each(juser.find("input"), function(k,input){
						var jinput = $(input);
						retval[jinput.attr('name')] = jinput.val();
					});
					return retval;
				},
				getAccess: function(which) {
					var jth = this.jeditor.find('tr.header').find('.'+which);
					var retval = {};
					$.each(jth.find("input"), function(k,input){
						var jinput = $(input);
						retval[jinput.attr('name')] = jinput.val();
					});
					return retval;
				}
			};
			Edit.Accesses.init();
			Edit.Accesses.sort();
		</script>
		<?php
		$s_javascript = ob_get_contents();
		ob_end_clean();
		
		// clean the output
		$s_dom = str_replace(array("\n", "\r", "\n\r"), "\n", $s_dom);
		$s_javascript = str_replace(array("\n", "\r", "\n\r"), "\n", $s_javascript);
		
		return $s_dom.$s_javascript;
	}
	
	// returns an array of all access strings
	// @return: an array(access_name=>array('name'=>access_name, 'files'=>associated file names), ...)
	function get_all_all_accesses() {
		$a_users = ConnectionHub::getConn('poslavu')->getDBAPI()->getAllInTable('accounts', array('username'=>'all_accesses'), TRUE);
		$s_all_accesses = $a_users[0]['access'];
		$o_accesses = LavuJson::json_decode($s_all_accesses, FALSE);
		$a_all_accesses = array('all'=>array('name'=>'all', 'files'=>''));
		$i_cutoff_time = strtotime("-15 days");
		foreach($o_accesses as $area=>$o_properties) {
			
			// ignore "all" access
			if ($area == "all")
				continue;
			
			// check if the access was attempted to be accessed in the last 15 days
			$a_files = array();
			$b_recent = FALSE;
			foreach($o_properties->files as $filename=>$o_fileproperties) {
				$a_files[] = str_replace("'", '', $filename);
				if ((int)$o_fileproperties->last_access > $i_cutoff_time)
					$b_recent = TRUE;
			}
			if (!$b_recent)
				continue;
			
			// add the access to the list of all accesses
			$a_all_accesses[$area] = array('name'=>$area, 'files'=>implode(", ", $a_files));
		}
		
		return $a_all_accesses;
	}
	
	// returns an array of all access strings for all users
	// @return: an array(
	//     username=>array('username'=>username, 'fullname'=>full user name, 'access'=>array(
	//         access_name=>array('name'=>access_name, 'isset'=>bool),
	//         ...
	//     ),
	//     ...
	// )
	function get_all_user_accesses(&$a_all_accesses) {
		
		$a_users = ConnectionHub::getConn('poslavu')->getDBAPI()->getAllInTable('accounts', NULL, TRUE, array('whereclause'=>"WHERE `lavu_admin`='1' AND `disabled`='0' AND `type` NOT IN ('terminated','special')"));
		$a_user_accesses = array();
		foreach($a_users as $a_user) {
			
			// get a list of user accesses
			$a_has_access = explode(',', $a_user['access']);
			foreach($a_has_access as $k=>$v)
				$a_has_access[$k] = trim($v);
			
			// build of list of accesses that the user has and does not have
			$a_accesses = array();
			foreach($a_all_accesses as $a_access) {
				$s_name = $a_access['name'];
				$b_isset = in_array($s_name, $a_has_access);
				$a_accesses[$s_name] = array('name'=>$s_name, 'isset'=>$b_isset);
			}
			
			// build a list of accesses the user has that all doesn't
			foreach($a_has_access as $s_user_access)
				if (!isset($a_all_accesses[$s_user_access]))
					$a_all_accesses[$s_user_access] = array('name'=>$s_user_access, 'files'=>'found in user '.$a_user['username']);
			
			// add to the user's accesses
			$a_user_accesses[$a_user['username']] = array('username'=>$a_user['username'], 'fullname'=>$a_user['firstname'].' '.$a_user['lastname'], 'userid'=>$a_user['id'], 'access'=>$a_accesses);
		}
		
		return $a_user_accesses;
	}
	
?>
