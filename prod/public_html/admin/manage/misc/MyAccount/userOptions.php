<?php
	
	require_once(dirname(__FILE__).'/../../login.php');
	require_once('/home/poslavu/public_html/admin/cp/resources/lavuquery.php');
	require_once('/home/poslavu/public_html/admin/cp/resources/core_functions.php');
	require_once("/home/poslavu/public_html/admin/cp/objects/json_decode.php");
	
	// get the environment set up
	session_start();
	if (!account_loggedin())
		header("Location: http://admin.poslavu.com/manage/");
	
	class USER_OPTIONS {
		private $s_username = '';
		private $a_options = NULL;
		
		public function __construct() {
			$this->s_username = account_loggedin();
			$this->a_options = $this->load_options($this->s_username);
			$this->set_defaults();
		}
		
		private function set_defaults() {
			$a_defaults = array(
				'display_company_name'=>'top',
			);
			
			foreach($a_defaults as $s_setting=>$s_default) {
				if (!isset($this->a_options[$s_setting]))
					$this->a_options[$s_setting] = $s_default;
			}
		}
		
		private function load_options($s_username) {
			$a_options = ConnectionHub::getConn('poslavu')->getDBAPI()->getAllInTable('accounts', array('username'=>$s_username), TRUE,
				array('selectclause'=>'`options`'));
			$s_options = $a_options[0]['options'];
			$a_options = Json::unencode($s_options);
			if (is_object($a_options) && $a_options !== NULL)
				$a_options = (array)$a_options;
			if (!is_array($a_options))
				$a_options = array();
			return $a_options;
		}
		
		// returns the value of the option, or NULL if the option isn't set
		public function get_option($s_option_name) {
			foreach($this->a_options as $k=>$v) {
				if ($k == $s_option_name)
					return $v;
			}
			return NULL;
		}
		
		// returns 0 if the option was already set, or 1 if it was added
		public function set_option($s_option_name, $s_value) {
			$b_found = FALSE;
			foreach($this->a_options as $k=>$v) {
				if ($k == $s_option_name) {
					$b_found = TRUE;
				}
			}
			$this->a_options[$s_option_name] = $s_value;
			
			$s_json = json_encode($this->a_options);
			$s_query_string = "UPDATE `poslavu_MAIN_db`.`accounts` SET `options`='[options]' WHERE `username`='[username]'";
			$a_query_vars = array('options'=>$s_json, 'username'=>$this->s_username);
			mlavu_query($s_query_string, $a_query_vars);
			
			if ($b_found)
				return 0;
			return 1;
		}
	}
	
	if (isset($_POST['set_option']) && isset($_POST['name']) && isset($_POST['value']) && $_POST['set_option'] == '1') {
		$o_user_options = new USER_OPTIONS();
		echo $o_user_options->set_option($_POST['name'], $_POST['value']);
	}
	
?>