<?php
	require_once($_SERVER['DOCUMENT_ROOT']."/cp/objects/json_decode.php");
	require_once($_SERVER['DOCUMENT_ROOT']."/cp/resources/core_functions.php");
	
	if(isset($_POST['id']) && isset($_POST['function']) && isset($_POST['tab']))
		if(function_exists($_POST['function']))
			$_POST['function']($_POST['id'], $_POST['val'], $_POST['tab'], $_POST['name_editor_id']);	//id is the id of the integrator
		else
			echo "fail";
	else
		echo "fail";

	function comms($id, $value='', $tab, $name_editor_id){
		$returnString="";
		$returnString.='
		<table class="detailsList">
			<tr>
				<td>
					<select id="fieldSelect" name_editor_id="'.$name_editor_id.'" onchange=\'openComms("'.$id.'", this, "'.$tab.'")\' >
			';
			if($value=='')
				$returnString.='<option value="" selected>Select a field</option>';
			else
				$returnString.='<option value="">Select a field</option>';
			
			if($value=='main')
				$returnString.='<option value="main" selected>MAIN</option>';
			else
				$returnString.='<option value="main" >MAIN</option>';
			if($value=='user')
				$returnString.='<option value="user" selected>USER</option>';
			else
				$returnString.='<option value="user">USER</option>';
			if($value=='request')
				$returnString.='<option value="request" selected>REQUEST</option>';
			else
				$returnString.='<option value="request">REQUEST</option>';
			if($value=='consts')
				$returnString.='<option value="consts" selected>CONSTS</option>';
			else
				$returnString.='<option value="consts" >CONSTS</option>';
				
				$returnString.='
				</select>
			</td>
			<td>';
				
		$Json= new Json();
		$json= mysqli_fetch_assoc(mlavu_query("SELECT `[1]` FROM `poslavu_MAIN_db`.`extensions` where `id`='[2]'", $tab, $id));
		$communications= $Json->decode($json[$tab]);	
	
		if($value=='user'){
			
			$returnString.='<select class="detailsSelect" onchange="displayTableRowsForComms(\''.$id.'\', $(this).val(), $(this), \"'.$name_editor_id.'\" )">';
			$tablesQuery= mlavu_query("SHOW TABLES FROM `poslavu_lavu_inc_db`");			
			$locCounter=0;$userCounter=0;
			if( isset($communications->$value->locations->fields))
					foreach($communications->$value->locations->fields as $numSelected)
						$locCounter++;
			if( isset($communications->$value->users->fields))
					foreach($communications->$value->users->fields as $numSelected)
						$userCounter++;
			
			$returnString.="
					<option> Select a table </option>
					<option value='locations'>Locations($locCounter)</option>
					<option value='users'>Users($userCounter)</option>";
			
			/*while($table = mysqli_fetch_assoc($tablesQuery)){		// i dont think we will ever need to send anything to the integrator other than stuff from users and locations... will revisit if needed
				$t= $table['Tables_in_poslavu_lavu_inc_db'];
				$counter=0;
				
				
				if($counter)
					$returnString.="<option value='$t'>$t ($counter)</option>";
				else
					$returnString.="<option value='$t'>$t </option>";
			}*/
			
		}else if($value=='main'){
			$counter=0;
			if(isset($communications->$value->restaurants->fields)){
				foreach($communications->$value->restaurants->fields as $numSelected)
					$counter++;
			}
			$returnString.='
				<select class="detailsSelect" onchange="displayTableRowsForComms(\''.$id.'\', $(this).val(), $(this), \''.$name_editor_id.'\'  )">
					<option>Select a table</option>
					';
				if($counter)
					$returnString.='<option value="restaurants">Restaurants ('.$counter.')</option>';	
				else
					$returnString.='<option value="restaurants">Restaurants </option>';	
				$returnString.='</select>';
		}
		else if($value=='request'){
			$returnString.="
			<select class='detailsSelect' id='request' onchange=\"displayTableRowsForComms('$id', $(this).val(), $(this), \"'.$name_editor_id.'\" )\">
				<option>Select an input type</option>
				<option value='text'> Text Box</option>
				<option value='password'>Password Field</option>
				<option value='select'>Select Box</option>
				<option value='radio'> Radio button</option>
			</select>";
		}else if($value=='consts'){
		
		}
		else{
			$returnString.='<option value=""></option>';
		}		
		$returnString.="</td></tr></table><div class='rowsContent'></div>";
	
		echo $returnString;
	}
	function dbExtensionFetch($id, $column){
		$result=mysqli_fetch_assoc(mlavu_query("SELECT `[1]` FROM `poslavu_MAIN_db`.`extensions` where `id`='[2]'", $column,$id));
		return $result[$column];
	}
	function dbFetch($table, $columns, $db){
		
		if($db=='MAIN')
			$query= mlavu_query(); // poslavu_MAIN_db
		else
			$query=lavu_query();
		
		$result= mysqli_fetch_assoc($query);
	}
		//Object structure:
		/*
		
		{
		    "MAIN": [
		        {
		            "tableName": "restaurants",
		            "fields": [
		                {
		                    "facingName": "dbname",
		                    "fieldName": "dataname",
		                    "extras": ""
		                }
		            ]
		        },
		        {
		            "tableName": "signups",
		            "fields": [
		                {
		                    "facingName": "dbname",
		                    "fieldName": "dataname",
		                    "extras": ""
		                }
		            ]
		        }
		    ],
		    "USER": [
		        {
		            "tableName": "locations",
		            "fields": [
		                {
		                    "facingName": "token",
		                    "fieldName": "api_token",
		                    "extras": {
		                        "needsDecode": "api_key"
		                    }
		                }
		            ]
		        }
		    ],
		    "REQUEST": [
		        {
		            "setting": "name",
		            "value": "value",
		            "integratorName": "name",
		            "input type": "text"
		        },
		        {}
		    ],
		    "CONSTS": [
		        {
		            "facingName": "name",
		            "value": "const"
		        }
		    ]
		}		
		*/
	

?>