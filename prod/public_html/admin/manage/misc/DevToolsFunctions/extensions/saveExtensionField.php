<?php
	require_once($_SERVER['DOCUMENT_ROOT']."/cp/objects/json_decode.php");
	require_once($_SERVER['DOCUMENT_ROOT']."/cp/resources/core_functions.php");
	if(isset($_POST['column']) && isset($_POST['value']) && isset($_POST['id'])){
				
			if($_POST['column']=='usage' && isset($_POST['field']))	{
				$json      = new Json();
				$valArray  = mysqli_fetch_assoc(mlavu_query("SELECT `[1]` FROM `poslavu_MAIN_db`.`extensions` where `id`='[2]'", $_POST['column'], $_POST['id']));
				
				$decoded   = $json->decode($valArray[$_POST['column']]);
				if($decoded){
					if($_POST['field']!='usage'){
						$var= $_POST['field']*1;
						if( isset($decoded->features[$var])){
			
							$decoded->features[$var]=  $_POST['value'];
							
						}else if($_POST['value']!='Add Feature'){
							$decoded->features[]= $_POST['value'];
							
						}
						if($_POST['value']==''){
						
							unset($decoded->features[$_POST['field']]);
						}
					}
					$_POST['value']=json_encode($decoded);
				}else{
					if( $_POST['field']=='usage'){
						$_POST['value']='{"usage":"'.$_POST['value'].'","features":[]}';
					}else{
						$_POST['value']='{"usage":"","features":["'.$_POST['value'].'"]}';

					}
				}
				
			}else if($_POST['column']=='features' && isset($_POST['field'])){
				$json      = new Json();
				$valArray  = mysqli_fetch_assoc(mlavu_query("SELECT `[1]` FROM `poslavu_MAIN_db`.`extensions` where `id`='[2]'", $_POST['column'], $_POST['id']));
				
				$decoded   = $json->decode($valArray[$_POST['column']]);
				if( isset($decoded[$_POST['field']]))
					$decoded[$_POST['field']]= $_POST['value'];
				else if($_POST['value']!='Add Feature')
					$decoded[]= $_POST['value'];
				
				if($_POST['value']==''){
					unset($decoded[$_POST['field']]);
				}
				
				$_POST['value']=json_encode($decoded);
				
			}
			/*else if($_POST['column']=='logo'){
				//do some stuffs;
			}else{
				echo "invalid column name.";
				exit();
			}*/
			
			mlavu_query("UPDATE `poslavu_MAIN_db`.`extensions` SET `[1]`='[2]' WHERE `id` ='[3]' ",$_POST['column'],$_POST['value'],$_POST['id'] );
			if(mlavu_dberror())
				echo mlavu_dberror();
			else
				echo 'success';
	}else
		echo "fail";
?>