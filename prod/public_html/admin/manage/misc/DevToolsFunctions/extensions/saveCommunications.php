<?php
	require_once($_SERVER['DOCUMENT_ROOT']."/cp/resources/core_functions.php");
	require_once($_SERVER['DOCUMENT_ROOT']."/cp/objects/json_decode.php");
	
	if( isset($_POST['table']) && isset($_POST['column'])  && isset($_POST['givenName']) && isset($_POST['field'])&& isset($_POST['commArea']) && isset($_POST['isIdentifier']) && isset($_POST['integratorID'])){
			saveData($_POST['table'],$_POST['column'], $_POST['givenName'],$_POST['field'], $_POST['commArea'],$_POST['isIdentifier'], $_POST['integratorID']);
	}
	else
		echo"Data missing.";
	
	function saveData($table, $column, $givenName, $field, $commArea,$isIdentifier, $integratorID){
	
		$JSON= new Json();
		$integratorInfo=mysqli_fetch_assoc(mlavu_query("SELECT * FROM `poslavu_MAIN_db`.`extensions` where `id`='[1]'", $integratorID));

		if(array_key_exists($commArea, $integratorInfo)){
			//echo "Field: ". $field. "\ntable: ". $table."\ncolumn: ".$column."\ncommArea: ".$commArea."\ngivenName: ".$givenName."\nintegratorID: ". $integratorID."\n\n";
			
			if($integratorInfo[$commArea]!=null) // if we already  have stuff in there for that type of communication we need to modify, not write over. 
				$communicationObj=$JSON->decode($integratorInfo[$commArea]);							
			else		// we have no communication data of this type saved currently
				$communicationObj=new stdClass();
				
			if(!isset($communicationObj->$field))  // if the field is already in this object that means we need to modify it 
				$communicationObj->$field= new stdClass();	
			
			if(!isset($communicationObj->$field->$table)) // if we dont have the table then make it. 
				$communicationObj->$field->$table= new stdClass();	
			
			if(!isset($communicationObj->$field->$table->fields)) // if we dont have the fields entry then make it.
				$communicationObj->$field->$table->fields=new stdClass();
		
			if(!isset($communicationObj->$field->$table->fields->$column)){ //if we have fields then add to it. and the column isnt already in here
				
				$tempObj= new stdClass();
				if($givenName=='')
					$tempObj->facingName=$column;
				else
					$tempObj->facingName=$givenName;
				if( $isIdentifier)
					$tempObj->isIdentifier= $isIdentifier;
				$tempObj->extras='';
				
				if($isIdentifier){  //if we are selecting this field as the identifier, then we need to unset the previous identifier if it exisits. 
					foreach($communicationObj->$field->$table->fields as $col){
						if(isset($col->isIdentifier))
							unset($col->isIdentifier);
					}
				}
				
				$communicationObj->$field->$table->fields->$column=$tempObj;
				
			}else{ //we are editing this column so update this means remove it.
				unset($communicationObj->$field->$table->fields->$column);
			} 
			echo print_r($communicationObj,true);
			
			$encodedObj= json_encode($communicationObj);

			mlavu_query("UPDATE `poslavu_MAIN_db`.`extensions` SET `[1]`= '[2]' WHERE `id`='[3]' ", $commArea, $encodedObj, $integratorID);
			if(mlavu_dberror())
				echo mlavu_dberror();
			else
				echo "success";
			
		}else
			echo "Error: no communication of type:". $commArea;
	}

?>
