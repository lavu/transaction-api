<?php
	require_once($_SERVER['DOCUMENT_ROOT']."/cp/objects/json_decode.php");
	require_once($_SERVER['DOCUMENT_ROOT']."/cp/resources/core_functions.php");
	
	if(isset($_POST['id']) && isset($_POST['function']))
		if(function_exists($_POST['function'])){
			if(isset($_POST['val']))
				$_POST['function']($_POST['id'],$_POST['val']);		
			else
				$_POST['function']($_POST['id']);	
		}else
			echo "error";
			
	function getLinks($id){
		$returnString='<table class="detailsList">';
		$json= new Json();
		$decoded= $json->decode(getField($id, 'link'));
		if($decoded){
			foreach($decoded as $key=>$link){
				$returnString.='<tr> <td><span class="linkText">'.$key.'</span></td><td><input type"text" value="'.$link.'" onblur="saveDetails(\''.$id.'\',$(this).val(),\'updateLinks\', \''.$key.'\')" style="width:300px"/></td></tr>';
			}
		}
		$returnString.="<tr><td>State</td><td><select onchange='saveDetails(\"".$id."\", $(this).val(),\"updateActiveState\",\"\" )'>";
		
		$active= getField($id, '_active');
		$activeStates= array("", 'Active','Development','Disabled', 'Deleted');
		for($x=1; $x<5; $x++){
			if($x==$active)
				$returnString.="<option value='$x' selected >".$activeStates[$x]."</option>";
			else
				$returnString.="<option value='$x'>".$activeStates[$x]."</option>";
		}
		$returnString.='</select></td></tr>
		';
		$returnString.="</table>";
		
		echo $returnString; 
	}		
	
	function getCommunications($id, $val=false){
		if(!$val)
			$val = 1;
		$signupsComms = getField($id, 'signup_communication');
		$loginComms   = getField($id, 'login_communication');
		$disableComms = getField($id, 'disable_communication');
		$Json= new Json();
		//$returnString='<table class="detailsList"><tr><td>'.print_r($Json->decode($signupsComms),true)."</td></tr><tr><td>".print_r($Json->decode($loginComms),true)."</td></tr><tr><td>".print_r($Json->decode($disableComms),true)."</td></tr></table>";
		$returnString="
			<div class= tabContainer > 
				<div id='tabTabContainer'>
					<span id='signup_communication' onclick='openComms(\"".$id."\", this)' name_editor_id = '".$val."'>Signup Comms </span> 
					<span id='login_communication' onclick='openComms(\"".$id."\", this)'  name_editor_id = '".$val."'>Login Comms </span> 
					<span id='disable_communication' onclick='openComms(\"".$id."\", this)'name_editor_id = '".$val."' >Disable Comms</span> 
				</div>
			</div>
			<div id='commsContainer'></div> ";
	
		echo $returnString;
		
		
	}
	function getPermissions($id){
			
			$permissionsOptionsArray= array("read","write", "read/write");
			$returnString='<table class="detailsList"><tr><td><select class="detailsSelect" onchange="displayTableRows(\''.$id.'\', $(this).val(), $(this) )">';
			$tablesQuery= mlavu_query("SHOW TABLES FROM `poslavu_lavu_inc_db`");			
			$returnString.="<option> Select a table </option>";
			while($table = mysqli_fetch_assoc($tablesQuery)){
				$t= $table['Tables_in_poslavu_lavu_inc_db'];
				$returnString.="<option value='$t'>$t</option>";
			}
			$returnString.="</select></td><td><select class='permissionSelect' \">";
			$x=1;
			foreach($permissionsOptionsArray as $permissions)
				$returnString.=	"<option value=".$x++.">$permissions</option>";
			$returnString.="</td><td><input type='submit' onclick='addPermission($(this), \"$id\")' ></td></tr><tr><td colspan=4><div class='rowsContent'></div></td></tr></table>";
			
			$permissions = getField($id, 'permissions');
			$Json= new Json();
			  
			echo $returnString;
	
	}
	function getTableFields($id, $table){
		$Json= new Json();
		$returnString='';
		if( $table=='Select a table'){
			echo "";
			return;
		}
		$columnsQuery= mlavu_query("SHOW COLUMNS FROM `poslavu_lavu_inc_db`.`[1]` ", $table);
		$permissions= mysqli_fetch_assoc(mlavu_query("SELECT `permissions` from `poslavu_MAIN_db`.`extensions` where `id`='[1]'", $id));
		$perms= $Json->decode($permissions['permissions']);
		$returnString.="<table><tr>";
		$counter=0;
		
		//echo "The perms are: ".print_r($perms,true);
		while($column = mysqli_fetch_assoc($columnsQuery)){
		
			
			if(($counter++ % 1)==0)
				$returnString.="</tr><tr>";
				
			if(isset($perms->$table->$column['Field'])){
			
				if($perms->$table->$column['Field']=='1')
					$backgroundColor='#1f4bcf';
				else if($perms->$table->$column['Field']=='2')
					$backgroundColor='#cf1f1f';
				else if($perms->$table->$column['Field']=='3')
					$backgroundColor='#8c1fcf';
			
				$returnString.="<td onclick= \"addToList('".$column['Field']."', this)\" permSelected= '".$perms->$table->$column['Field']."' class='fieldRow' style='background-color:".$backgroundColor."; color:white'> ".$column['Field']."</td>";
			}
			else
				$returnString.="<td onclick= \"addToList('".$column['Field']."', this)\" class='fieldRow'> ".$column['Field']."</td>";
			
			
		}	
		$returnString.="</table>";

		echo $returnString;
	}

	
	function getTableFieldsForComms($id, $table){	//called from the js file in function: displayTableRowsForComms
		$name_editor_id= $_POST['name_editor_id'];
		$Json= new Json();
		$field= $_POST['field'];
		$comm_type=$_POST['comm_type'];
		
		$returnString='';
		if( $table=='Select a table' ||$table==''){
			echo "";
			return;
		}
	
		if($table=='restaurants' /*|| $table=='signups'*/)
			$columnsQuery= mlavu_query("SHOW COLUMNS FROM `poslavu_MAIN_db`.`[1]`", $table);
		else
			$columnsQuery= mlavu_query("SHOW COLUMNS FROM `poslavu_lavu_inc_db`.`[1]` ", $table);
		
		$permissions= mysqli_fetch_assoc(mlavu_query("SELECT `permissions` from `poslavu_MAIN_db`.`extensions` where `id`='[1]'", $id));
		$perms= $Json->decode($permissions['permissions']);
		$returnString.="<table><tr>";
		$counter=0;

		$json= mysqli_fetch_assoc( mlavu_query("SELECT `[1]` FROM `poslavu_MAIN_db`.`extensions` where `id`='[2]'", $comm_type, $id));
		
		$communications= $Json->decode($json[$comm_type]);
		while($column = mysqli_fetch_assoc($columnsQuery)){
		
			if(($counter++ % 2)==0)
				$returnString.="</tr><tr>";
		
		if(isset($communications->$field->$table->fields->$column['Field']))		
			$returnString.="<td onclick= \"openNameEditor('".$column['Field']."','".$table."','".$id."', this, $name_editor_id)\" class='fieldRow selectedFieldRow'> ".$column['Field']." (".$communications->$field->$table->fields->$column['Field']->facingName.")</td>";
		else
			$returnString.="<td onclick= \"openNameEditor('".$column['Field']."','".$table."','".$id."', this, $name_editor_id)\" class='fieldRow'> ".$column['Field']."</td>";	
			
		}	
		$returnString.="</table>";

		echo $returnString;
	}
	function getField($id, $column){
		$result=mysqli_fetch_assoc(mlavu_query("SELECT `[1]` FROM `poslavu_MAIN_db`.`extensions` WHERE `id`='[2]'", $column, $id));
		if (mlavu_dberror()){
			echo "error";
			exit();
		}else
			return $result[$column];
	}
	
	function getInputFieldOptions($id, $table){	//called from the js file in function: displayTableRowsForComms
		
		$Json= new Json();
		$commType= $_POST['comm_type'];
		$result= mysqli_fetch_assoc(mlavu_query("SELECT `[1]` FROM `poslavu_MAIN_db`.`extensions` where `id`='[2]'", $commType, $id));
	
		$decoded= $Json->decode($result[$commType]);
		//echo "decoded is:".print_r($decoded,true);
		$returnString.="<div class='requestContainer'> <form id='saveConfigSettingOption'><table> ";		
		
		$optionsArray=array("none"=>"<option value='none'>None </option>","min8Chars"=>"<option value='min8Chars'>min of 8 characters</option>","ensureEmail"=>"<option value='ensureEmail'>validate as email</option>");
	
		if(isset($decoded->request)){
			foreach($decoded->request as $request){
				if($request->type==$table){
					$returnString.="
					<tr>
						<td>Setting: </td>
						<td><input type='text'  placeholder='Setting name' name='setting_name' value='".$request->name."'/></td>
					</tr>";
				}
			}
		}
			
		if( $table=='select'){
			if(isset($decoded->request)){
				foreach($decoded->request as $request){
				
					if($request->type==$table){
						
						$exploded=explode("||",$request->options);
						
						$counter=1;
						foreach($exploded as $opts){
							if($opts){
								$returnString.="
								<tr>
									<td>Option:</td>
									<td>
										<input type='text' class='selectBoxOption' value='$opts' name='opt$counter' place='$counter' placeholder='option' onchange='$(this).attr(\"changed\",\"true\")' 
										onblur='makeNextOption($(this)); $(this).attr(\"changed\",\"false\");'>
										
									</td>
								</tr>";
							}
						$counter++;
						}
					}
				}
			}
			$returnString.="
				<tr>
					<td>Setting: </td>
					<td><input type='text'  placeholder='Setting name' name='setting_name'/></td>
				</tr>
				<tr>
					<td>Option:</td>
					<td>
						<input type='text' class='selectBoxOption' name='opt1' place='1' placeholder='option' onchange='$(this).attr(\"changed\",\"true\")' 
						onblur='makeNextOption($(this)); $(this).attr(\"changed\",\"false\");'>
					</td>
				</tr>
				<tr>
				
					<td>
						<input type='hidden' name='integratorID' value='$id'>
						<input type='button' id='submitConfigSetting' onclick='saveConfigSetting($(\"#saveConfigSettingOption\"))' value='save'>
					</td>
				</tr>
			";
		}else{
			
			if(isset($decoded->request)){
				
				foreach($decoded->request as $request){
					if($request->type==$table){

						$returnString.="
							
							<tr>
								<td>Extra Params: </td>
								<td>
									<select name='extra_params'>";
								foreach($optionsArray as $key=>$content){
									if($key==$request->extras){
										$exploded=explode("<option", $content);
										
										$returnString.="<option selected".$exploded[1];
									}else
										$returnString.=$content;
								}
								$returnString.="
									</select>
								</td>
								<input type='hidden' name='integratorID' value='$id'>
							</tr>";
					}
						
				}
			}	
	
			$returnString.="
				<tr>
					<td>Setting: </td>
					<td><input type='text'  placeholder='Setting name' name='setting_name'/></td>
				</tr>
				<tr>
					<td>Extra Params: </td>
					<td><select name='extra_params'><option value='none'>None </option><option value='min8Chars'>min of 8 characters</option><option value='ensureEmail'>validate as email</option></select></td>
					<input type='hidden' name='integratorID' value='$id'>
				</tr>
				<tr>
					<td>
						<input type='button' id='submitConfigSetting' onclick='saveConfigSetting($(\"#saveConfigSettingOption\"))' value='save'>
					</td>
				</tr>";
		
		}
		$returnString.="</table></form></div>";
		echo $returnString;
		
	}
	
?>	