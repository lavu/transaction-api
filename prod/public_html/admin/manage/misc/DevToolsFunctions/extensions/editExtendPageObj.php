<?php	
	require_once($_SERVER['DOCUMENT_ROOT']."/manage/error_config.php");
	require_once($_SERVER['DOCUMENT_ROOT']."/cp/objects/json_decode.php");
	require_once($_SERVER['DOCUMENT_ROOT']."/cp/objects/extendObject.php");
	class EditExtendPage extends extendPage{
		
		private $extensions = null;
		private $extendPage = null;
		private $json		=null;
		function __construct(){
			$this->json= new Json();
			$this->extendPage= new extendPage(true);
			//echo print_r($this->extendPage,true);
			$this->setExtensions();
		}
		public function getExtensions(){
			return $this->extensions;
		}
		private function setExtensions(){
			$this->extensions= $this->extendPage->getPages();
		}
		public function editUsage($usage, $id){
			$returnString='';
			$counter=0;
			if($usage){
				$decoded= $this->json->decode($usage);
				
				$returnString= "<span class='usage' onclick='openEditor($(this),\"usage\", \"".$id."\",\"usage\")'> ".$decoded->usage."</span> <span style= 'color:#aecd37'>: </span>";
				$counter=0;
				
				foreach($decoded->features as $feature){
					$returnString.= "<span class='usage' onclick='openEditor($(this),\"usage\", \"".$id."\",".$counter++.")'>".$feature."</span><span style= 'color:#aecd37'>|</span> ";
				}
				$returnString.= "<span class='usage' onclick='openEditor($(this),\"usage\", \"".$id."\",".$counter++.")'>Add Feature</span><span style= 'color:#aecd37'>|</span> ";
			}else{
				$returnString.= "<span class='usage' onclick='openEditor($(this),\"usage\", \"".$id."\",\"usage\")'>Add Usage</span> <span style= 'color:#aecd37'>: </span>";
				$returnString.= "<span class='usage' onclick='openEditor($(this),\"usage\", \"".$id."\",".$counter++.")'>Add Feature</span><span style= 'color:#aecd37'>|</span> ";
			}
			return $returnString;
		}  
		public function editTitle($title, $id){
			if($title)
				return "<span class='title' onclick='openEditor($(this),\"title\", \"".$id."\")'> ".$title."</span> ";
			else
				return "<span class='title' onclick='openEditor($(this),\"title\", \"".$id."\")'> Your Title Here</span> ";
		}
		public function editTypeImage($title, $id){
			if($title)
				return "<span class='typeImg' onclick='openEditor($(this),\"typeImg\", \"".$id."\")'> <img src= 'http://admin.poslavu.com/cp/images/".$title.".png'></span> ";
			else
				return "<span class='typeImg' onclick='openEditor($(this),\"typeImg\", \"".$id."\")'> <img src= 'http://admin.poslavu.com/cp/images/computerToIOS.png'/ ></span> ";
		}
		public function editFeatures($features, $id){
			if($features){
				$decoded= $this->json->decode($features);
				
				$returnString='<ul>';
				$counter=0;
				foreach($decoded as $feat){
					$returnString.="<li style='margin-bottom:5px;'  class='features' onclick= \"openEditor( $(this), 'features','".$id."','".$counter++."')\" >".$feat."</li>";	
				}
				$returnString.="<li style='margin-bottom:5px;'  class='features' onclick= \"openEditor( $(this), 'features','".$id."')\" >Add Feature</li>";	
				$returnString.="</ul>";
			}else
				$returnString.="<ul><li style='margin-bottom:5px;'  class='features' onclick= \"openEditor( $(this), 'features','".$id."')\" >Add Feature</li></ul>";
			return $returnString;
		}
		public function editLogo($logo, $id){
			if($logo)
				return "<img class='logo'src= 'http://admin.poslavu.com/manage/misc/AdvancedToolsFunctions/extensionLogos/". $logo."' onclick=\"openEditor($(this),'logo','".$id."')\" width='240px' height='60px' >";
			else
				return "<img class='logo'src= 'http://admin.poslavu.com/manage/misc/AdvancedToolsFunctions/extensionLogos/logo_blank.jpg' onclick=\"openEditor($(this),'logo','".$id."')\" width='240px' height='60px' >";
		}
		public function editLink($id, $num){
			return "<img src= 'http://admin.poslavu.com/images/arrowcircle.png' onclick='openDetailsEditWindow(\"".$id."\", \"".$num."\")'>";
		}
	}
?>
