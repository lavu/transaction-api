<?php
	require_once($_SERVER['DOCUMENT_ROOT']."/cp/resources/core_functions.php");
	require_once($_SERVER['DOCUMENT_ROOT']."/cp/objects/json_decode.php");

	if(isset($_POST['setting_name'])){
		saveData($_POST);
	}else
		echo "post vars missing. ";

	function saveData($postVars){
			$json= new Json();
		
		$name= $postVars['setting_name'];
		unset($postvars['setting_name']);
		
		$type= $postVars['type'];
		unset($postVars['type']);
		
		$commArea= $postVars['commArea'];
		unset($postvars['commArea']);
		
		$integratorID= $postVars['integratorID'];
		unset($postVars['integratorID']);
		
		$extraParams= $postVars['extra_params'];
		unset($postVars['extra_params']);		
	
		$options='';
		foreach($postVars as $key=>$post){
			if(strstr($key,"opt") && $post!='')
				$options.=$post."||";
		}
		$jsonObj= mysqli_fetch_assoc(mlavu_query("SELECT `[1]` FROM `poslavu_MAIN_db`.`extensions` WHERE `id`='[2]'", $commArea, $integratorID));
		
		$decoded= $json->decode($jsonObj[$commArea]);
		
		if(!isset($decoded->request))
			$decoded->request= array();
		
		
		$settingObj= new stdClass();
		$settingObj->type=$type;
		$settingObj->name= $name;
		
		if($options)
			$settingObj->options= $options;
		if($extraParams)
			$settingObj->extras=$extraParams;
		
		$decoded->request[]=$settingObj;		
		$encoded= json_encode($decoded);

		mlavu_query("UPDATE `poslavu_MAIN_db`.`extensions` SET `[1]`='[2]' WHERE `id`='[3]'", $commArea,$encoded, $integratorID);
		
		if(mlavu_dberror())
			echo mlavu_dberror();
		else
			echo "success";
	}

 ?>