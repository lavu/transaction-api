<?php
	require_once($_SERVER['DOCUMENT_ROOT']."/cp/objects/json_decode.php");
	require_once($_SERVER['DOCUMENT_ROOT']."/cp/resources/core_functions.php");
	
	if(isset($_POST['id']) && isset($_POST['function']) && isset($_POST['val']))
		if(function_exists($_POST['function']))
			$_POST['function']($_POST['id'], $_POST['val'], $_POST['field']);	

	function updateLinks($id, $val, $field){
		$Json= new Json();
		$links= mysqli_fetch_assoc(mlavu_query("SELECT `link` FROM `poslavu_MAIN_db`.`extensions` where `id`='[1]'", $id));
		$decoded= $Json->decode($links['link']);
		
		if($decoded){
			
			$decoded->$field= $val;
			$encoded=json_encode($decoded);	
			mlavu_query("UPDATE `poslavu_MAIN_db`.`extensions` SET `link`='[1]' where `id`='[2]'",$encoded, $id);
			if (mlavu_dberror())
				echo mlavu_dberror();
			else
				echo 'success';
		}else
			echo "fail";
		
		
	}
	function updateActiveState($id, $val, $field){
		mlavu_query("UPDATE `poslavu_MAIN_db`.`extensions` SET `_active`='[1]' where `id`='[2]'",$val, $id);
		if (mlavu_dberror())
			echo mlavu_dberror();
		else
			echo 'success';
	}
	function updatePermissionsList($id, $val, $field){
		
		//val comes in as json
		
		$Json = new Json();
		$result= mysqli_fetch_assoc(mlavu_query("SELECT `permissions` FROM `poslavu_MAIN_db`.`extensions` where `id`='[2]'", $field, $id));
		
		$permissions=$Json->decode($result['permissions']);
		$val= $Json->decode($val);
		if(!$permissions){
			echo "no permissions";
			$permissions=$Json->decode("{}");	
		}
		if($val){
			$permissions->$field= $val;
			echo print_r($permissions);		
			$encoded= json_encode($permissions);
			mlavu_query("UPDATE `poslavu_MAIN_db`.`extensions` SET `permissions`='[1]' where `id`='[2]'",$encoded, $id);
			if (mlavu_dberror())
				echo mlavu_dberror();
			else
				echo "success";
		}else{
			echo "Value sent is not valid JSON.";
			return;
		}
	
	}
?>