<?php
	require_once($_SERVER['DOCUMENT_ROOT']."/cp/resources/lavuquery.php");
	require_once($_SERVER['DOCUMENT_ROOT']."/cp/resources/json.php");
	require_once($_SERVER['DOCUMENT_ROOT']."/manage/error_config.php");
	if ( isset($_POST['function'])){

		if (function_exists($_POST['function'])){
			$fun_name= $_POST['function'];
			unset($_POST['function']);
			$fun_name($_POST);
		}
		else
			echo "error:no function of name ". $_POST['function'];
	}else
		echo "error: no function name given.";
	function get_extension_data(){
		require_once($_SERVER['DOCUMENT_ROOT']."/manage/misc/DevToolsFunctions/extensions/editExtendPageObj.php");
		$EditExtendPage= new EditExtendPage();
		$extensions = $EditExtendPage->getExtensions();
		$counter    = 0;
		foreach($extensions as $extension){
			echo "
				<div id='detailsEditWindowPositioner'>
					<div id='detailsEditWindow'>
						<span id='closeExtensionDetailsBtn' onclick='hideDetailsWindow()'>X</span>
						<div id='detailsHeader'>Edit Extension Details </div>

						<div id='detailsTabsContainer'>
							<div id='links'>Links</div>
							<div id='permissions'>Permissions</div>
							<div id='communications'>Communications</div>
						</div>
						<div id='detailsContent'></div>
					</div>
				</div>
				<!--<div id='nameEditor' style='display:none'>-->
				<div id='nameEditor".$counter."'>
					<span id='integratorID' ></span>
					<span id='commNameTable'> </span>
					
					<span id='commNameTitle'></span>
						<br/>
					<input type='text' id='commsName' placeholder='name for integrator' />

				</div>
				<div class='extension'>
					<div class= 'extensionUsage'>".    $EditExtendPage->editUsage($extension->getUsage(), $extension->getID())."</div>
					<div class= 'extensionLogo'>".     $EditExtendPage->editLogo($extension->getLogo(), $extension->getID()).         "</div>
					<div class= 'extensionTitle'>".    $EditExtendPage->editTitle($extension->getTitle(), $extension->getID()). "</div>
					<div class= 'extensionFeatures'>". $EditExtendPage->editFeatures($extension->getFeatures(),$extension->getID()). "</div>
					<div class= 'extensionTypeImage'>".$EditExtendPage->editTypeImage($extension->getTypeImage(),$extension->getID()).    "</div>
					<div class= 'extensionLink'>".	   $EditExtendPage->editLink($extension->getID(), $counter)."</div>
				</div>";
			$counter++;
		}
		echo "<div class='extension'>
				<div class= 'extensionUsage'>".    $EditExtendPage->editUsage(null,null)."</div>
				<div class= 'extensionLogo'>".     $EditExtendPage->editLogo(null,null). "</div>
				<div class= 'extensionTitle'>".    $EditExtendPage->editTitle(null,null). "</div>
				<div class= 'extensionFeatures'>". $EditExtendPage->editFeatures(null,null). "</div>
				<div class= 'extensionTypeImage'>".$EditExtendPage->editTypeImage(null,null).    "</div>
				<div class= 'extensionLink'>".	   $EditExtendPage->editLink(null,null)."</div>
			</div>
	";
	}
?>