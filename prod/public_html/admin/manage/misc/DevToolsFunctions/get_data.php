<?php
	require_once($_SERVER['DOCUMENT_ROOT']."/manage/error_config.php");
	require_once($_SERVER['DOCUMENT_ROOT']."/cp/resources/lavuquery.php");
	require_once($_SERVER['DOCUMENT_ROOT']."/cp/resources/json.php");
	require_once($_SERVER['DOCUMENT_ROOT']."/cp/resources/core_functions.php");
	date_default_timezone_set("America/Denver");
	if (isset($_POST['function'])) {
		if (function_exists($_POST['function'])) {
			$fun_name= $_POST['function'];
			unset($_POST['function']);
			echo $fun_name($_POST);
		} else {
			echo "error:no function of name ". $_POST['function'];
		}
	} else {
		echo "error: no function name given.";
	}

	/**
	 * @deprecated Method get_projects() is deprecated.
	 */
	function get_projects() {
		trigger_error("Called deprecated function get_projects()", E_USER_DEPRECATED);
	}

	/**
	 * @deprecated Method get_current_working_project() is deprecated.
	 */
	function get_current_working_project() {
		trigger_error("Called deprecated function get_current_working_project()", E_USER_DEPRECATED);
	}

	/**
	 * @deprecated Method get_past_projects() is deprecated.
	 */
	function get_past_projects() {
		trigger_error('Warning: Called deprecated function #get_past_projects()', E_USER_DEPRECATED);
	}

	/**
	 * @deprecated Method start_work() is deprecated.
	 */
	function start_work() {
		trigger_error('Warning: Called deprecated function #start_work()', E_USER_DEPRECATED);
	}

	/**
	 * @deprecated Method save_new_project() is deprecated.
	 */
	function save_new_project() {
		trigger_error('Warning: Called deprecated function #save_new_project()', E_USER_DEPRECATED);
	}

	/**
	 * @deprecated Method end_work() is deprecated.
	 */
	function end_work() {
		trigger_error('Warning: Called deprecated function #end_work()', E_USER_DEPRECATED);
	}

	function edit_modules(){
	 	require_once($_SERVER['DOCUMENT_ROOT']."/cp/objects/json_decode.php");
	 	$json = new JSON();
	 	//echo "<script type='text/javascript' src = '/manage/misc/AdvancedToolsFunctions/moduleEditing/module_editing.js'>< /script>";
	 	require_once $_SERVER['DOCUMENT_ROOT']. "/manage/misc/AdvancedToolsFunctions/getEditModules.php";
	}
	function create_account(){

			$model_list = array();
			$model_list['Bar/Lounge'] = array("model"=>"defaultbar",	"title"=>"Bar/Lounge");
			$model_list['Coffee Shop'] = array("model"=>"default_coffee",	"title"=>"Coffee Shop");
			$model_list['Diner'] = array("model"=>"apple_default",	"title"=>"Diner");
			$model_list['Original Sushi'] = array("model"=>"sushi_town",	"title"=>"Original Sushi");
			$model_list['Pizza Shop'] = array("model"=>"default_pizza_",	"title"=>"Pizza Shop");
			$model_list['Restaurant'] = array("model"=>"default_restau",	"title"=>"Restaurant");
			$model_list['Laser Tag'] = array("model"=>"ersdemo",	"title"=>"Laser Tag",	"product"=>"lasertag");
			$model_list['Lavu Give'] = array("model"=>"lavu_tithe",	"title"=>"Lavu Give",	"product"=>"tithe");
			$model_list['Lavu Hospitality'] = array("model"=>"hotel_lavu",	"title"=>"Lavu Hospitality",	"product"=>"hotel");
			$model_list['Lavu Kart (P2R)'] = array("model"=>"p2r_syracuse",	"title"=>"Lavu Kart (P2R)",	"product"=>"lavukart");

			ksort($model_list);

			//../lib/create_demo_account.php?m=2' method='POST'>
			require_once(__DIR__.'/../../../cp/resources/chain_functions.php');
			$countryCodes = getCountryListOption();
			
				echo "
			<form id='createAccountForm'>
				<table cellspacing='0' cellpadding='5'>
					<tr>
						<td align='right'>Package:</td>
						<td align='left'>
							<input type='radio' name='product' id='package_lavu' value='Lavu POS' checked><label for='package_lavu'>Lavu</label>
							<input type='radio' name='product' id='package_lavuse' value='LavuSE'><label for='package_lavuse'>SE</label>
						</td>
					</tr>
					<tr>
						<td align='right'>Terminals:</td>
						<td align='left'><input type='number' name='terminals' size='2' value='1'></td>
					</tr>
					<tr><td align='right'>Default Menu:</td><td align='left'><select name='default_menu'>";

					foreach ($model_list as $key => $val) {
						$model_dataname = $val['model'];
						$model_title = $val['title'];
						$selected = ($key == 'Restaurant') ? 'selected' : '';
						echo "<option value='$model_dataname' {$selected}>$model_title</option>";
					}


				echo "		</select></td></tr>
						<tr>
							<td align='right'>Account Type:</td>
							<td align='left'>
								<input type='radio' name='account_type' value='Client' checked> Client
								<input type='radio' name='account_type' value='Reseller'> Reseller
								<input type='radio' name='account_type' value='Demo'> Demo
								<input type='radio' name='account_type' value='Test'> Test
								<input type='radio' name='account_type' value='Dev'> Dev
							</td>
						</tr>
						<tr>
							<td>Custom Type</td>
							<td><input type='text' name='override_default_menu' value=''></td>
						</tr>
						<tr><td align='right'>Company Name:</td><td align='left'><input type='text' name='company' size='30' /></td></tr>
						<tr><td align='right'>Email:</td><td align='left'><input type='text' name='email' size='30' /></td></tr>
						<tr><td align='right'>Phone:</td><td align='left'><input type='text' name='phone' size='30' /></td></tr>
						<tr><td align='right'>First Name:</td><td align='left'><input type='text' name='firstname' size='30' /></td></tr>
						<tr><td align='right'>Last Name:</td><td align='left'><input type='text' name='lastname' size='30' /></td></tr>
						<tr><td align='right'>Address:</td><td align='left'><input type='text' name='address' size='30' /></td></tr>
						<tr><td align='right'>* Country:</td><td align='left'>
						<select id='country' name='country'><option value=''>Select Country</option>";
						echo $countryCodes;
						echo "</select>
						</td></tr>
						<tr><td align='right'>City:</td><td align='left'><input type='text' name='city' size='30' /></td></tr>
						<tr><td align='right'>State:</td><td align='left'><input type='text' name='state' size='30' /></td></tr>
						<tr><td align='right'>Zip:</td><td align='left'><input type='text' name='zip' size='30' /></td></tr>
						<tr><td align='center' colspan='2'><input type='button' value='Create Account' onclick='perform_create_account(this)'/></td></tr>
					</table>
				";
	}
	function build_website(){
		$query= mlavu_query("SELECT * FROM `poslavu_PRODUCTS_db`.`info` where `deleted` !='1'");
		echo "
		<div class= 'advanced_tools_container'>

		<table class='advanced_tools_table_positioner'>
			<tr><th class= 'advanced_tools_th' colspan='8'> Subsite List </th></tr>
			<tr><td>" . speak("ID") . "</td><td> " . speak("Name") . " </td><td>" . speak("Domain Name") . "</td><td>" . speak("Status") . "</td><td>" . speak("View Site") . "</td><td>" . speak("Edit Site") . "</td><td>" . speak("Delete") ."</td></tr>";
			$odd= false;
			while($result= mysqli_fetch_assoc($query)){
				$odd= !$odd;
				if(!$odd)
					$class='advanced_tools_td';
				else
					$class='advanced_tools_td_odd';

				echo "<tr><td class='$class'>{$result['id']}</td><td class='$class'>{$result['product_name']}</td><td class='$class'>{$result['domain_name']}</td><td class='$class'> {$result['status']}</td>
				<td class='$class'><a href='http://admin.poslavu.com/manage/subsite_builder/?product_id={$result['id']}'>View</a></td>
				<td class='$class'><a href='http://admin.poslavu.com/manage/subsite_builder/editor/?product_id={$result['id']}'>Edit</a></td>
				<td class='$class'><a class='delete_component' style='cursor:pointer' onclick='delete_product(\"{$result['id']}\")'>Delete</a></td></tr>";
			}

		echo "</table></div></div>";
	}
	function save_edited_message($args){
		$s_time = $args['start_time'];
		$e_time = $args['end_time'];
		$id     = $args['proj_id'];
		if(!strtotime($s_time) || !strtotime($e_time)|| !$id)
			return '{"status":"error", "message":"start or end time or ID not found."}';
		else{
			$diff = (strtotime($e_time) - strtotime($s_time));
			//echo "the first diff is: ". $diff;
			$diff= ($diff / 3600 % 24).":".($diff / 60 % 60) .":".$diff % 60;
			//echo "the diff is: ".$diff. "  args are: ". print_r($args,1);
			mlavu_query("UPDATE `poslavu_MAIN_db`.`employee_work_tracking` SET `start_time`='[1]', `end_time`='[2]', `total`='[3]' where `id`= '[4]'",$s_time, $e_time, $diff, $id);
			return '{"status":"success", "message":"updated"}';
		}
	}
?>