<?php
	require_once($_SERVER['DOCUMENT_ROOT']."/manage/GetCustomerInfo.php");
	require_once($_SERVER['DOCUMENT_ROOT']."/manage/login.php");
		$o_customer_info = new GetCustomerInfoClass();
	$component_package = (isset($_GET['comps']))?$_GET['comps']:false;
	$model_db = (isset($_GET['from']))?$_GET['from']:"DEV";
	$dbt_file = (isset($_GET['dbt']))?$_GET['dbt']:"core_tables.dbt";
	$make_changes = true;
	$rowid='';
	if(isset($_REQUEST['rowid']))
		$rowid=$_REQUEST['rowid'];
	else{
		echo "No Row id sent. ";
		return;
	}
		//echo "model db: $model_db <br>component package: $component_package<br>dbt file: $dbt_file<br><br>";

	$dev_db_info = $o_customer_info->get_database_info('poslavu_'.$model_db.'_db');
	$dev_tables = $dev_db_info['tables'];
	$dev_fields = $dev_db_info['fields'];

	//$fp = fopen($_SERVER['DOCUMENT_ROOT']."/sa_cp/".$dbt_file,"r");
	//$coredata = fread($fp, filesize($dbt_file));
	//fclose($fp);

	$coredata=file_get_contents($_SERVER['DOCUMENT_ROOT']."/sa_cp/".$dbt_file);

	$core_tables = array();
	$coredata = explode("}",$coredata);
	for($i=0; $i<count($coredata); $i++)
	{
		$corerow = explode("{",$coredata[$i]);
		$corerow = trim($corerow[0]);
		if($corerow!="")
		{
			$core_tables[] = $corerow;
		}
	}

	$log = "";
	$company_query = mlavu_query("select * from `poslavu_MAIN_db`.`restaurants` WHERE `id` = '[1]'", $rowid);
	$company_read = mysqli_fetch_assoc($company_query);
	$company_db_name = "poslavu_".$company_read['data_name']."_db";
	$company_db_info = $o_customer_info->get_database_info($company_db_name);
	$company_tables = $company_db_info['tables'];
	$company_fields = $company_db_info['fields'];

	$check_company = false;
	if($component_package) {
		$comp_query = mlavu_query("select * from `$company_db_name`.`locations` where `component_package`='".str_replace("'","''",$component_package)."'");
		if(mysqli_num_rows($comp_query))
			$check_company = true;
	}
	else $check_company = true;

	if($check_company)
	{
		for($i=0; $i<count($dev_tables); $i++)
		{
			$tablename = $dev_tables[$i];
			$process_table = false;
			for($n=0; $n<count($core_tables); $n++)
			{
				if($core_tables[$n]==$tablename)
					$process_table = true;
			}

			if($process_table)
			{
				if(isset($_GET['archived']))
				{
					if($tablename=="orders" || $tablename=="cc_transactions" || $tablename=="order_contents")
					{
						$dev_fields["archived_".$tablename] = $dev_fields[$tablename];
						$tablename = "archived_".$tablename;
					}
				}
				//echo "<br>" . $tablename;
				if(!isset($company_fields[$tablename])) // create new table
				{
					$fieldlist = $dev_fields[$tablename][0];
					$fields = $dev_fields[$tablename][1];

					//CREATE TABLE `poslavu_DEV_db`.`test_table` (
					//`t1` VARCHAR( 255 ) NOT NULL ,
					//`t2` INT NOT NULL ,
					//`id` INT NOT NULL AUTO_INCREMENT PRIMARY KEY
					//) ENGINE = MYISAM

					$tcode = "";
					for($n=0; $n<count($fieldlist); $n++)
					{
						$fieldname = $fieldlist[$n];
						$fieldtype = $fields[$fieldname]['type'];
						$fieldnull = $fields[$fieldname]['null'];
						$fieldkey = $fields[$fieldname]['key'];
						if(trim($fieldnull)!="") $notnull = true; else $notnull = false;
						if(trim($fieldkey)!="") $usekey = true; else $usekey = false;

						if($tcode!="") $tcode .= ", ";
						$tcode .= "`$fieldname` $fieldtype";
						if($notnull) $tcode .= " NOT NULL";
						if($usekey) $tcode .= " AUTO_INCREMENT PRIMARY KEY";
					}
					$query = "CREATE TABLE `$company_db_name`.`$tablename` ($tcode)";
					if($make_changes)
					{
						$str = "<br><font color='blue'>Executing:</font> $query";
						$success = mlavu_query($query);
						if($success) $str .= " <b><font color='green'>(Success)</font></b>";
						else $str .= " <b><font color='red'>(Failed)</font></b>";
						$str .= "<br>";
						echo $str;
						$log .= $str;
					}
					else echo "<br><br>" . $query;
				}
				else
				{
					$fieldlist = $dev_fields[$tablename][0];
					$fields = $dev_fields[$tablename][1];

					for($n=0; $n<count($fieldlist); $n++)
					{
						$fieldname = $fieldlist[$n];
						$fieldtype = $fields[$fieldname]['type'];
						if(!isset($company_fields[$tablename][1][$fieldname])) // create new column
						{
							$query = "ALTER TABLE `$company_db_name`.`$tablename` ADD `$fieldname` $fieldtype NOT NULL";
							if($make_changes)
							{
								$str = "<br><font color='blue'>Executing:</font> $query";
								$success = mlavu_query($query);
								if($success) $str .= " <b><font color='green'>(Success)</font></b>";
								else $str .= " <b><font color='red'>(Failed)</font></b>";
								$str .= "<br>";
								echo $str;
								$log .= $str;
							}
							else echo "<br><br>" . $query;
						}
						else
						{
							// check column type
							$company_fieldtype = $company_fields[$tablename][1][$fieldname]['type'];
							if($fieldtype!=$company_fieldtype)
							{
								$query = "ALTER TABLE `$company_db_name`.`$tablename` CHANGE `$fieldname` `$fieldname` $fieldtype NOT NULL";

								if($make_changes)
								{
									$str = "<br><font color='blue'>Executing:</font> $query";
									$success = mlavu_query($query);
									if($success) $str .= " <b><font color='green'>(Success)</font></b>";
									else $str .= " <b><font color='red'>(Failed)</font></b>";
									$str .= "<br>";
									echo $str;
									$log .= $str;
								}
								else echo "<br><br>" . $query;
							}
						}
					}
				}
			}
		}
	}

	mlavu_query("insert into `poslavu_MAIN_db`.`updatelog` (`type`,`date`,`details`,`account`) values ('single database sync (".mlavu_query_encode($company_read['company_name']).")','".date("Y-m-d H:i:s")."','".str_replace("'","''",$log)."','".str_replace("'","''",account_loggedin())."')");

	//echo "<br><br><br><a href='index.php?mode=manage_customers&submode=details&rowid=$rowid'><< Company Details</a><br><br>";


?>
