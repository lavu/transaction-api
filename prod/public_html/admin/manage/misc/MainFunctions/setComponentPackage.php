<?php
session_start();
if(!isset($_POST['rest_id']) || !isset($_POST['comp_pack']) || !isset($_POST['rand'])){
	echo "A requried credential was not set.";
	exit();
}

if( empty($_POST['rest_id']) || empty($_POST['comp_pack']) ) {
	echo "Attempting to set empty information.";
	exit();
}

$rand = $_POST['rand'];
$rand = base64_decode(urldecode($rand));

if($rand != $_SESSION['rand']){
	echo "Unauthorized Access Attempted.  This incident will be reported.";
	exit();
}

require_once("/home/poslavu/public_html/admin/cp/resources/lavuquery.php");
require_once("/home/poslavu/public_html/admin/sa_cp/tools/convert_product.php");

echo "Submission Validated.\n";

update_account_product($_POST['rest_id'],$_POST['comp_pack']);