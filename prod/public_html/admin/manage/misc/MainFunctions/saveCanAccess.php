<?php
	
	require_once(dirname(__FILE__).'/../../login.php');
	if (!account_loggedin())
		exit();
	if (isset($_POST['action']) && isset($_POST['access']) && $_POST['action'] == 'add_can_access')
		add_to_can_access_list($_POST['access'], 'javascript');
	
?>