<?php
	if (!isset($_SESSION)) {
		session_start();
	}
	require_once($_SERVER['DOCUMENT_ROOT']."/cp/resources/core_functions.php");
	require_once($_SERVER['DOCUMENT_ROOT']."/cp/areas/extensions/extensions_functions.php");
	if(isset($_POST['rowID'])&& isset($_POST['updateTo']) &&  isset($_POST['dataname'])){
		//echo print_r($_POST,1);
		updateMods($_POST['rowID'],rawurldecode($_POST['updateTo']), $_POST['dataname']);
	}
	
	else{
		echo "no such luck";
		exit();
	}
	function updateMods($rowid, $update_modules_to, $dataname){
		$update_modules_to = str_ireplace("unddotund", ".", $update_modules_to);

		$modules_str = $update_modules_to;
		$modules_arr = explode(",", $modules_str);
		$restaurants_modules_query = mlavu_query("SELECT `modules` FROM `poslavu_MAIN_db`.`restaurants` WHERE `id`='[1]' AND `data_name`='[2]'", $rowid, $dataname);
		$rest_modules = mysqli_fetch_assoc($restaurants_modules_query);
		$rest_modules_str = $rest_modules['modules'];
		$rest_modules_arr = explode(",", $rest_modules_str);
		$result_array = array_diff($modules_arr, $rest_modules_arr);

		foreach ($result_array as $val) {
			if (strpos($val, '+') !== false) {
				$val = str_replace('+', '', $val);
				$enable = 1;
			} else {
				$val = str_replace('-', '', $val);
				$enable = 0;
			}
			$ext_title_query = mlavu_query("SELECT `title` FROM `poslavu_MAIN_db`.`extensions` WHERE `module`='[1]'", $val);
			$ext_title_read = mysqli_fetch_assoc($ext_title_query);
			adminActionLogExtensionChange($ext_title_read['title'], $enable, $dataname);
		}

		mlavu_query("UPDATE `poslavu_MAIN_db`.`restaurants` SET `modules`='[1]' WHERE `id`='[2]' AND `data_name`='[3]'", $update_modules_to, $rowid, $dataname);
		
		// save modules info in config table so it's available to LLS
		$get_location_ids = mlavu_query("SELECT `id` FROM `poslavu_[1]_db`.`locations`", $dataname);
	
		while ($linfo = mysqli_fetch_assoc($get_location_ids)) {
		
			$check_config = mlavu_query("SELECT `id` FROM `poslavu_[2]_db`.`config` WHERE `location` = '[1]' AND `type` = 'location_config_setting' AND `setting` = 'modules'", $linfo['id'], $dataname);
		
			if (mysqli_num_rows($check_config) > 0) {
				$cinfo = mysqli_fetch_assoc($check_config);
				$update_config = mlavu_query("UPDATE `poslavu_[3]_db`.`config` SET `value` = '[1]' WHERE `id` = '[2]'", $update_modules_to, $cinfo['id'], $dataname);
			}else{
				$insert_config = mlavu_query("INSERT INTO `poslavu_[3]_db`.`config` (`location`, `type`, `setting`, `value`) VALUES ('[1]', 'location_config_setting', 'modules', '[2]')", 
				$linfo['id'], $update_modules_to,$dataname);
			}
		}
		if( mlavu_dberror())
			echo mlavu_dberror();
		else
			echo "success";
	}

?>