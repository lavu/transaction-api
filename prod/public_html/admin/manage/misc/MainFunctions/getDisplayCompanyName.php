<?php
	
	require_once(dirname(__FILE__).'/../../login.php');
	require_once('/home/poslavu/public_html/admin/cp/resources/lavuquery.php');
	require_once('/home/poslavu/public_html/admin/cp/resources/core_functions.php');
	require_once("/home/poslavu/public_html/admin/cp/objects/json_decode.php");
	
	// get the environment set up
	session_start();
	if (!account_loggedin())
		header("Location: http://admin.poslavu.com/manage/");
	
	$dn = $_POST['dn'];
	$val = $_POST['val'];
	
	if ($val == 'dn') {
		echo $dn;
	} else {
		$a_restaurants = ConnectionHub::getConn('poslavu')->getDBAPI()->getAllInTable('restaurants', array('data_name'=>$dn), TRUE,
			array('selectclause'=>'`company_name`'));
		if (count($a_restaurants) > 0)
			echo $a_restaurants[0]['company_name'];
		else
			echo $dn;
	}
	
?>