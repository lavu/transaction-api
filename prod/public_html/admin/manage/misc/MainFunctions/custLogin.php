<?php
	require_once("/home/poslavu/public_html/admin/cp/resources/core_functions.php");
	require_once("/home/poslavu/public_html/admin/cp/resources/lavuquery.php");
	require_once("/home/poslavu/public_html/admin/cp/resources/session_functions.php");
	// log into customer accounts via super admin
	ini_set('error_reporting', 1);
	error_reporting(1);
	session_start();
	if(isset($_GET['mode'])){
		//Edited out by Brian D.  Now passing which 'tunnel' we wish to connect to.
		//execute_cplogin($_GET['username'],$_GET['custID'],$_GET['custCode'],$_GET['mode']);
		$tunnelNumber = empty($_GET['tunnel_number']) ? 1 : $_GET['tunnel_number'];
		execute_cplogin($_GET['username'],$_GET['custID'],$_GET['custCode'],$_GET['mode'], $tunnelNumber);
	}else{
		echo "nothing to do here.";
		exit;
	}
	function execute_cplogin($loggedin,$cplogin,$ccode,$loginmode,$tunnel_number = 1) //{}
	{
		global $data_name;
		global $lavu_query_should_log;

		$lavu_query_should_log = true;

		//$cust_query = mlavu_query("select * from `poslavu_MAIN_db`.`restaurants` where `id`='[1]' and `company_code`='[2]'",$cplogin,$ccode);
		$cust_query = mlavu_query("select * from `poslavu_MAIN_db`.`restaurants` where `data_name`='[1]'",$ccode);
		if(mysqli_num_rows($cust_query))
		{

			$cust_read = mysqli_fetch_assoc($cust_query);

			if( $loginmode=="check_temp"){
				//echo print_r($loggedin,1);
				$username_info = get_temp_username($loggedin);
				if($username_info['user_exists'] && ($ccode==$username_info['old_dataname'])){
					lavu_connect_dn($username_info['old_dataname'],"poslavu_".$username_info['old_dataname']."_db");
					$query= lavu_query("SELECT * FROM `users` WHERE `username`='{$username_info['use_username']}'");
					if( mysqli_num_rows($query)){
						$result= mysqli_fetch_assoc($query);
						echo "Previous user found:<br>";
						echo "username: " .$username_info['use_username'];
						echo "<br>password & PIN: " . $result['PIN'];
						return;
					}else{
						echo "no_user";
						return;
					}
				}else{
					echo "no_user";
					return;
				}
			}
			else if($loginmode=="create_temp")
			{
				$username_info = get_temp_username($loggedin);

				$use_username = $username_info['use_username'];
				$user_exists  = $username_info['user_exists'];
				$old_dataname = $username_info['old_dataname'];

				$vars = array();
				$vars['username'] = $use_username;
				$vars['password'] = rand(1111,9999);
				$vars['dataname'] = $ccode;
				$vars['restaurant'] = $cplogin;
				$vars['type'] = "lavuadmin";
				$vars['userid'] = $user_exists;

				$vars['f_name'] = "LavuAdmin";
				$vars['l_name'] = $use_username;
				$vars['email'] = "info@poslavu.com";
				$vars['access_level'] = 4;
				$vars['PIN'] = "8228";
				$vars['quick_serve'] = 0;

				$new_db = "poslavu_" . $ccode . "_db";
				if($user_exists){
						mlavu_query("update `poslavu_MAIN_db`.`customer_accounts` set `username`='[username]' ,`dataname`='[dataname]',`restaurant`='[restaurant]',`type`='[type]' where `id`='[userid]'",$vars); // poslavu_MAIN_db
				}
				else{
					mlavu_query("insert into `poslavu_MAIN_db`.`customer_accounts` (`username`,`dataname`,`restaurant`,`type`) values ('[username]','[dataname]','[restaurant]','[type]')",$vars);// poslavu_MAIN_db
				}

				$use_pin = "";
				$try_pin = rand(11111,99999);
				while($use_pin=="")
				{
					lavu_connect_dn($ccode,$new_db);
					$pin_query = lavu_query("select * from `$new_db`.`users` where `PIN`='[1]'",$try_pin);
					if(mysqli_num_rows($pin_query))
					{
						$try_pin = rand(11111,99999);
					}
					else
					{
						$use_pin = $try_pin;
					}
				}
				$vars['password'] = $use_pin;
				$vars['PIN'] = $use_pin;

				$is_LLS = false;
				lavu_connect_dn($ccode,$new_db);
				$get_loc_info = lavu_query("SELECT `id`, `use_net_path`, `net_path` FROM `locations` LIMIT 1");
				if (mysqli_num_rows($get_loc_info) > 0) {
					$location_info = mysqli_fetch_assoc($get_loc_info);
					if ((int)$location_info['use_net_path']>0 && !strstr($location_info['net_path'], "poslavu.com")) {
						$is_LLS = true;
					}
				}

				$data_name = $vars['dataname'];

				$update_id = false;
				$check_existing = lavu_query("SELECT `id` FROM `users` WHERE `username` = '[username]'", $vars);
				if ($check_existing!==FALSE && mysqli_num_rows($check_existing)) {
					$info = mysqli_fetch_assoc($check_existing);
					$update_id = $info['id'];
				}

				$key_val_pairs = array(
					"company_code"	=> "'[dataname]'",
					"username"		=> "'[username]'",
					"f_name"		=> "'[f_name]'",
					"l_name"		=> "'[l_name]'",
					"password"		=> "PASSWORD('[password]')",
					"email"			=> "'[email]'",
					"access_level"	=> "'[access_level]'",
					"PIN"			=> "'[PIN]'",
					"quick_serve"	=> "'[quick_serve]'",
					"lavu_admin"	=> "'1'",
					"active"		=> "'1'"
				);

				if ($is_LLS) {
					$key_val_pairs['_use_tunnel'] = "'[_use_tunnel]'";
					//Edited out by Brian D. Which tunnel is now variable.
					//$vars['_use_tunnel'] = '1';
					$vars['_use_tunnel'] = $tunnel_number;
				}

				if ($update_id) {

					$q_update = "";
					foreach ($key_val_pairs as $key => $val) {
						if ($q_update != "") {
							$q_update .= ", ";
						}
						$q_update .= "`".$key."` = ".$val;
					}
					$vars['id'] = $update_id;

					lavu_query("UPDATE `users` SET ".$q_update." WHERE `id` = '[id]'", $vars);

				} else {

					$q_fields = "";
					$q_values = "";
					foreach ($key_val_pairs as $key => $val) {
						if ($q_fields != "") {
							$q_fields .= ", ";
							$q_values .= ", ";
						}
						$q_fields .= "`".$key."`";
						$q_values .= $val;
					}

					lavu_query("INSERT INTO `users` (".$q_fields.") VALUES (".$q_values.")", $vars);
				}

				if ($is_LLS) {
					$exist_query = lavu_query("SELECT * FROM `config` WHERE `setting`='[1]' AND `location`='[2]' AND `value`='[3]' AND `value_long`='[4]'", "table updated", $location_info['id'], "users", "");
					if (mysqli_num_rows($exist_query) == 0) {
						lavu_query("INSERT INTO `config` (`setting`, `location`, `value`, `value_long`, `type`) VALUES ('[1]', '[2]', '[3]', '[4]', 'log')", "table updated", $location_info['id'], "users", "");
					}
				}

				echo "username: " . $vars['username'];
				echo "<br>password & PIN: " . $vars['password'];
			}else if($loginmode=="login"){
				$dataname = $cust_read['company_code'];
				$userid = 1000;
				$username = "poslavu_admin";
				$full_username = "POSLavu Admin";
				$email = "info@poslavu.com";
				$companyid = $cust_read['id'];
				// get more customer information
				// including: location_id
				$location_id = "0";
				$a_locations = ConnectionHub::getConn('poslavu')->getDBAPI()->getAllInTable('locations', array('_disabled'=>0), TRUE, array('databasename'=>'poslavu_'.$dataname.'_db', 'selectclause'=>'`id`', 'limitclause'=>'LIMIT 1'));
				if (count($a_locations) > 0) {
					$a_location = $a_locations[0];
					$location_id = $a_location['id'];
				}

				// set admin variables
				$result= admin_login($dataname, $userid, $username, $full_username, $email, $companyid, "", $cust_read['dev'], "4", "", "", $location_id, "1", $cust_read['chain_id']);

				// set session variables
				set_sessvar('admin_dataname', $dataname);
				set_sessvar('locationid', $location_id);

				// get tack-on getvars
				$a_getvars = array();
				if (isset($_GET['unset_sessvar'])) $a_getvars[] = 'unset_sessvar='.$_GET['unset_sessvar'];
				$s_getvars = '';
				if (count($a_getvars) > 0)
					$s_getvars = '?'.implode('&', $a_getvars);

				// rmode = "redirect" mode, basically the mode querystring var in our CP redir URL - since "mode" was already a used var name for this page
				if (isset($_REQUEST['rmode']))
					$s_getvars .= (strpos($s_getvars, '?') === false) ? '?mode='. $_REQUEST['rmode'] : '&mode='. $_REQUEST['rmode'];

				// Jumps you into whatever stage of the account cancellation prompt you want to go to - useful since the Cancel button was removed in CP (which is awful)
				if (isset($_REQUEST['cancel_step']))
					$s_getvars .= (strpos($s_getvars, '?') === false) ? '?cancel_step='. $_REQUEST['cancel_step'] : '&cancel_step='. $_REQUEST['cancel_step'];

				$cpURL = "/cp/index.php{$s_getvars}";

				echo "<script language='javascript'>";

				// Users with an already openned session will need to log in again to store their 
				// pasword as a session variable
				if (!isset($_SESSION['posadmin_password'])) {
					echo "alert('Please log out from your current manage session in order to enable single sign-on')</script>";
					exit();
				}

				if (!isset($_GET['skipHubql']) || $_GET['skipHubql'] != 'true') {
						$data_string = json_encode([
							'username' => $_SESSION['posadmin_loggedin'],
							'password' => $_SESSION['posadmin_password'],
							'dataname' => $dataname,
							'skipPHP' => false,
						]);
	
						$ch = curl_init($_SERVER['HUBQL_URL']."/loginAdmin");
						curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
						curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
						curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
						curl_setopt($ch, CURLOPT_HTTPHEADER, [
							'Content-Type: application/json',
							'Content-Length: ' . strlen($data_string),
						]);
						$result = curl_exec($ch);
						curl_close($ch);
						$data = json_decode($result);

						if ($data->status === 200) {
							$cpDomain = getStandardDomain();
	
							echo '
								var expires = new Date(new Date().getTime() + '.$_SERVER['CP3_SESSION_EXPIRE_DAYS'].' * 24 * 60 * 60 * 1000);
								var cookie = "LAVU_TOKEN='.$data->token.';expires=" + expires.toGMTString() + ";path=/;domain='.$cpDomain.'";
								document.cookie = cookie;
							';
						} else if ($data->status === 401) {
							echo "alert('".$data->message."'); window.close(); </script>";
							exit();
						}
					}

				if (isNewControlPanelActive()) {
					$cpURL = $_SERVER['CP3_URL']."/home";
				}

				echo "window.location.replace('".$cpURL."'); ";
				echo "</script>";
			}
			exit();
		}

	}
	function get_temp_username($try_username)
	{
		$start_try = $try_username;
		$use_username = "";
		$old_dataname = "";
		$user_exists = false;
		$trys = 0;
		while($use_username=="")
		{
			$act_query = mlavu_query("select `id`, `type`, `dataname` from `poslavu_MAIN_db`.`customer_accounts` where `username`='[1]'",$try_username); // poslavu_MAIN_db
			if(mysqli_num_rows($act_query))
			{
				$act_read = mysqli_fetch_assoc($act_query);
				if($act_read['type']=="lavuadmin")
				{
					$use_username = $try_username;
					$old_dataname = $act_read['dataname'];
					$user_exists = $act_read['id'];

				}
				else
				{
					$trys++;
					$try_username = $start_try . $trys;
				}
			}
			else $use_username = $try_username;
		}
		$return_info = array();
		$return_info['use_username'] = $use_username;
		$return_info['user_exists']  = $user_exists;
		$return_info['old_dataname'] = $old_dataname;
		return $return_info;
	}


?>
