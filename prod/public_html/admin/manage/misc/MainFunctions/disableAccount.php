<?php

	require_once(dirname(__FILE__).'/../../login.php');
	require_once('/home/poslavu/public_html/admin/cp/resources/lavuquery.php');
	require_once('/home/poslavu/public_html/admin/cp/resources/core_functions.php');
	require_once("/home/poslavu/public_html/admin/sa_cp/billing/package_levels_object.php");

	// get the environment set up
	session_start();
	if (!account_loggedin())
		header("Location: http://admin.poslavu.com/manage/");

	//------------------ Enable / Disable Account --------------//
	//$account_disabled = $cust_read['disabled'];
	if (isset($_POST['enableOrDisable'])) {
		$b_enable_disable = FALSE;
		switch($_POST['enableOrDisable']) {
		case 'disable':
			mlavu_query("update `poslavu_MAIN_db`.`restaurants` set `disabled`='1' where id='[1]'",$_POST['id']);
			$b_enable_disable = TRUE;
			$s_action = 'account disabled through manage';
			break;
		case 'enable':
			mlavu_query("update `poslavu_MAIN_db`.`restaurants` set `disabled`='0' where id='[1]'",$_POST['id']);
			mlavu_query("update `poslavu_MAIN_db`.`payment_status` set `canceled`='', `canceled_arbs`='', `cancel_date`='', `cancel_info`='', `cancel_reason`='', `cancel_notes`='' where restaurantid='[1]'",$_POST['id']);
			$b_enable_disable = TRUE;
			$s_action = 'account enabled through manage';
			break;
		}

		if ($b_enable_disable) {
			$a_restaurants = ConnectionHub::getConn('poslavu')->getDBAPI()->getAllInTable('restaurants', array('id'=>$_POST['id']), TRUE);
			$a_restaurant = $a_restaurants[0];
			$a_ed_insert_vars = array('action'=>$s_action, 'datetime'=>date('Y-m-d H:i:s'), 'dataname'=>$a_restaurant['data_name'], 'restaurantid'=>$a_restaurant['id'], 'username'=>$_SESSION['admin_username'], 'fullname'=>$_SESSION['admin_fullname'], 'ipaddress'=>$_SERVER['REMOTE_ADDR'], 'details'=>$_POST['details']);
			$s_insert_clause = ConnectionHub::getConn('poslavu')->getDBAPI()->arrayToInsertClause($a_ed_insert_vars);
			mlavu_query("INSERT INTO `poslavu_MAIN_db`.`billing_log` $s_insert_clause", $a_ed_insert_vars);
		}

		echo 'success';
	} else {
		echo 'failed: please refresh manage';
	}
?>
