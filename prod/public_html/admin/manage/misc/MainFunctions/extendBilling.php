<?php

	if(!isset($_POST['id']) || !isset($_POST['numDays'])){
		echo "An Error occured please try again. Or tell Nialls he will fix it for you. ";
		exit;
	}
		$signupid=$_POST['id'];
		$add_days=$_POST['numDays'];
		$signup_query = mlavu_query("select * from `poslavu_MAIN_db`.`signups` where id='[1]'",$signupid);
		if(mysqli_num_rows($signup_query))
		{
			$signup_read = mysqli_fetch_assoc($signup_query);
			
			function add_days_to_date($sdate,$days)
			{
				$sdate_parts = explode("-",$sdate);
				if(count($sdate_parts) > 2)
				{			
					return date("Y-m-d",mktime(0,0,0,$sdate_parts[1],$sdate_parts[2] + $days,$sdate_parts[0]));
				}
				else return false;
			}
			
			$arb_license_id = $signup_read['arb_license_id'];
			$arb_hosting_id = $signup_read['arb_hosting_id'];
			$arb_license_newdate = add_days_to_date($signup_read['arb_license_start'],$add_days);
			$arb_hosting_newdate = add_days_to_date($signup_read['arb_hosting_start'],$add_days);
			
			$extend_arbs = array();
			$extend_arbs[] = array($arb_license_id,$arb_license_newdate,"arb_license_start");
			$extend_arbs[] = array($arb_hosting_id,$arb_hosting_newdate,"arb_hosting_start");
			
			$success = false;
			for($i=0; $i<count($extend_arbs); $i++)
			{
				$arbid = $extend_arbs[$i][0];
				$edate = $extend_arbs[$i][1];
				$set_field = $extend_arbs[$i][2];
				
				$vars['refid'] = 16;
				$vars['subscribeid'] = $arbid;
				$vars['start_date'] = $edate;
				//echo $vars['subscribeid'] . "<br>" . $vars['start_date'] . "<br>";
				$extend_info = manage_recurring_billing("update_start",$vars);
				if($extend_info['success'])
				{
					mlavu_query("update `poslavu_MAIN_db`.`signups` set `$set_field`='[1]' where `id`='[2]'",$edate,$signupid);
					$success = true;
				}
				else 
				{
					$success = false;
					//echo "Failed: " . $extend_info['error'] . "<br>";
				}
				//echo "<br>response: " . str_replace("<","&lt;",$extend_info['response']) . "<br>";
			}
			if($success)
			{
				$signup_query = mlavu_query("select * from `poslavu_MAIN_db`.`signups` where id='[1]'",$signupid);
				if(mysqli_num_rows($signup_query))
				{
					return mysqli_fetch_assoc($signup_query);
				}
			}
		}
		return false;

?>
