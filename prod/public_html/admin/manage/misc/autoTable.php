<?php

	$auto_build_table_index = 1;
	function auto_build_table($a_headers, $a_data_set, $s_tr_onclick='', $s_tablename='') {
		global $auto_build_table_index;
		$s_table = '';
		$s_mouseover = 'onmouseover="$(this).addClass(\'mouseOver\');"';
		$s_mouseout = 'onmouseout="$(this).removeClass(\'mouseOver\');"';
		$s_mouse = $s_mouseover.' '.$s_mouseout;
		
		if (!isset($auto_build_table_index))
			$auto_build_table_index = 1;
		if ($s_tablename == '')
			$s_tablename = $auto_build_table_index;
		
		$s_table .= '<table class="autoTableContainer"><tr><td>';
		
		//draw the static header
		$s_table .= '<table class="autoTable" id="auto_table_static_'.$s_tablename.'"><tr>';
		foreach($a_headers as $k=>$s_header)
			$s_table .= '<th class="autoTable" onclick="sort_table_by_header($(\'#'.$s_tablename.'_'.$k.'\'));" '.$s_mouse.'>'.$s_header.'</th>';
		$s_table .= '</tr></table>';
		
		$s_table .= '</td></tr><tr><td><div id="customerData" class="autoTable">';
		
		// draw the inner table
		$s_table .= '<table class="autoTable" id="auto_table_hidden_'.$s_tablename.'">';
			$s_table .= '<thead>';
			// draw the inner table header
			$s_table .= '<tr style="visibility:hidden;height:1px;">';
			foreach($a_headers as $k=>$s_header)
				$s_table .= '<th class="autoTable" style="height:1px;line-height:1px;padding-top:0px;padding-bottom:0px;" id="'.$s_tablename.'_'.$k.'">'.$s_header.'</th>';
			$s_table .= '</tr>';
			$s_table .= '<thead>';
			$s_table .= '<tbody id="table-body">';
			// draw the data
			foreach($a_data_set as $a_data) {
				$s_table .= '<tr class="autoTable" '.$s_mouse.' onclick="'.$s_tr_onclick.'">';
				foreach($a_data as $k=>$s_column)
					$s_table .= '<td class="autoTable">'.$s_column.'</td>';
				$s_table .= '</tr>';
			}

		$s_table .= '</tbody>';
		$s_table .= '</table>';
		$s_table .= '<div class="lastrow" style="height: 50px;width: 100%;visibility:show;"></div></div></td></tr></table>';

		$s_script = '<script type="text/javascript">auto_table_adjust_table_widths("'.$s_tablename.'");$(window).resize(function(){auto_table_adjust_table_widths("'.$s_tablename.'");});</script>';
		
		$auto_build_table_index++;
		return $s_table.$s_script;
	}

	/**
	* This function is used to get lazyload data in table view.
	*
	* @param array $body.
	*
	* @return string $resultData.
	*/
	function buildLazyloadTableData($body) {
		$resultData = '';
		$mouseover = 'onmouseover="$(this).addClass(\'mouseOver\');"';
		$mouseout = 'onmouseout="$(this).removeClass(\'mouseOver\');"';
		$hover = $mouseover.' '.$mouseout;
		foreach ($body as $data) {
			$resultData .= '<tr class="autoTable" '.$hover.'>';
			foreach ($data as $column) {
				$resultData .= '<td >'.$column.'</td>';
			}
			$resultData .= '</tr>';
		}
		return $resultData;
	}
?>