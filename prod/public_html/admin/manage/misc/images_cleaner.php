<?php
require_once(__DIR__.'/../../cp/resources/lavuquery.php');
session_start();
if($_SESSION['posadmin_lavu_admin'] != 1){
	echo "Not logged in at poslavu admin";
	exit;
}
echo '<pre>';
$dataname = $_REQUEST['DATANAMETOCLEAR'];
if(false == validateDataname($dataname)){
	echo "Dataname Failed Validation";
	exit;
}
ConnectionHub::getConn('rest')->selectDB($dataname);
databaseCleanup();
$imageWhitelist = getImageWhitelist();
$imageFiles = globImages($dataname);
$toDelete = array_diff($imageFiles,$imageWhitelist);
deleteImages($dataname,$toDelete);
#echo '<pre>'."To Delete\n".print_r(array_diff($imageFiles,$imageWhitelist),1).'</pre>';
#echo '<pre>'."In Use\n".print_r($imageWhitelist,1).'</pre>';
#echo '<pre>'."Files\n".print_r($imageFiles,1).'</pre>';

echo 'Finished';

function getImageWhitelist(){
	$imagesInUse = 'SELECT DISTINCT(`image`) FROM `menu_items` WHERE `image` != "" ORDER BY `image`';
	$result = lavu_query($imagesInUse);
	$toReturn = array();
	while($temp = mysqli_fetch_assoc($result) ){
		$toReturn[] = $temp['image'];
	}
	sort($toReturn);
	return $toReturn;
}

function databaseCleanup(){
	$unusedImages='UPDATE `menu_items` 
		SET 
			`image` = ""
		WHERE 
			`image` != "" 
		AND
			(
				`_deleted` = 1
			OR
				`category_id` IN (
					SELECT `id` FROM `menu_categories` WHERE `_deleted` = 1 OR `group_id` IN (
						SELECT `id` FROM `menu_groups` WHERE `_deleted` = 1
		)));';
	return lavu_query($unusedImages);
}

function globImages($dataname){
	$toReturn = array();
	$workingDir = __DIR__.'/../../images/'.$dataname.'/items/';
	$imgDir = scandir($workingDir);
	foreach ($imgDir as $value) {
		if(($value != '') && ($value != '.') && ($value != '..') && is_file($workingDir.$value) ){
			if(!in_array($value, $toReturn)){
				$toReturn[] = $value;
			}
		}
	}
	$imgDir = scandir($workingDir.'full/');
	foreach ($imgDir as $value) {
		if(($value != '') && ($value != '.') && ($value != '..') && is_file($workingDir.$value) ){
			if(!in_array($value, $toReturn)){
				$toReturn[] = $value;
			}
		}
	}
	$imgDir = scandir($workingDir.'main/');
	foreach ($imgDir as $value) {
		if(($value != '') && ($value != '.') && ($value != '..') && is_file($workingDir.$value) ){
			if(!in_array($value, $toReturn)){
				$toReturn[] = $value;
			}
		}
	}
	sort($toReturn);
	return $toReturn;
}

function deleteImages($dataname,$filenames){
	$workingDir = __DIR__.'/../../images/'.$dataname.'/items/';
	foreach ($filenames as $value) {
		unlink($workingDir.$value);
		unlink($workingDir.'main/'.$value);
		unlink($workingDir.'full/'.$value);
		echo "Delete : {$value}\n";
	}
	return true;
}

function validateDataname($input,$fullInfo = false){
	if(!is_string($input)){return false;}
	if(strlen($input) < 1){return false;}
	$test = preg_replace("/[^\da-zA-Z\_]/", "", $input);
	if(0 != strcmp($test,$input)){return false;}
	$result = mlavu_query("SELECT `id`,`data_name`,`dev` FROM `poslavu_MAIN_db`.`restaurants` WHERE `data_name` = '{$input}'");
	while($resultRows = mysqli_fetch_assoc($result ) ){
		if($fullInfo){
			return $resultRows;
		}else{
			return $resultRows['id'];
		}
	}
	return false;
}
