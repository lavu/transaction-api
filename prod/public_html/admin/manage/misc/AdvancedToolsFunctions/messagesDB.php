<?php
	/******************************************************************
	Contains all the methods that handle the DB queries.
	*******************************************************************/
	class messagesDB{
		/******************************************************************
		Handles connection to the DB trough the custom inhouse DB libraries

		Params:		---			---

		Returns: 	---
		*******************************************************************/
		function __construct(){

			if (!isset($_SESSION)) {
				session_start();
			}

			//echo dirname(__FILE__).'<br/><br/>';
			//require_once($_SERVER['DOCUMENT_ROOT']."/cp/resources/core_functions.php");
			require_once(dirname(__FILE__)."/../../../cp/resources/core_functions.php");

			$maindb = "poslavu_MAIN_db";
			$data_name = sessvar("admin_dataname");
			$in_lavu = true;
			lavu_auto_login();

			$rdb = admin_info("database");
			lavu_connect_byid(admin_info("companyid"),$rdb);
		}

		function updateEntry($title, $type, $content,$username, $limiting_query,$mod_limit, $id, $removal_date, $min_access_level="1"){
				$arr= array("title"=>$title,"type"=>$type,"content"=>$content,"username"=>$username,"limiting_query"=>$limiting_query,"mod_limit"=>$mod_limit,"id"=>$id, "removal_date"=> $removal_date,"min_access_level"=>$min_access_level);
				$query = "UPDATE `poslavu_MAIN_db`.`messages` set `title`='[title]', `type`='[type]', `content`='[content]',`last_edited_by`='[username]', `limiting_query`='[limiting_query]',`limiting_module`='[mod_limit]', `removal_date`='[removal_date]', `min_access_level`='[min_access_level]' WHERE id='[id]'";
				$result = mlavu_query($query, $arr);// poslavu_MAIN_db
				return $result;

		}//updateEntry()

		function insertNewMessage($title, $type, $content, $username, $limiting_query, $mod_limit, $removal_date, $min_access_level="1"){
				$arr= array("title"=>$title,"type"=>$type,"content"=>$content,"username"=>$username,"limiting_query"=>$limiting_query,"mod_limit"=>$mod_limit,"created_date"=>date("Y-m-d H:i:s"),"removal_date"=> $removal_date,"min_access_level"=>$min_access_level);
				$query = "INSERT INTO poslavu_MAIN_db.messages (`title`, `type`, `content`,`last_edited_by`, `limiting_query`,`limiting_module`, `created_date`,`removal_date`,`min_access_level`) VALUES ('[title]','[type]','[content]', '[username]','[limiting_query]','[mod_limit]', '[created_date]','[removal_date]','[min_access_level]')";
				$res = mlavu_query($query,$arr);// poslavu_MAIN_db
				return $res;
		}//insertNewMessage()

		function getMessageInfo($id){
			$query = "SELECT * FROM poslavu_MAIN_db.messages WHERE id='".$id."'";//
			$result = mlavu_query($query);// poslavu_MAIN_db

			return $result;
		}//queryDB()

		function saveNewMessage($title, $type, $content){
			return mlavu_query("insert into poslavu_MAIN_db.messages (`title`, `type`, `content`,`timestamp`) values ('[1]','[2]'.'[3]', NOW())", $title, $type,$content);
		}

		function getMessage($id){
			$result = array();

			$query = mlavu_query('select * from poslavu_MAIN_db.messages where id='.$id);

			while ($read = mysqli_fetch_assoc($query)) {
				$result[] = $read;
			}
			return $result;
		}

		function getMesagges(){

			$result = array();

			$query = mlavu_query('select * from poslavu_MAIN_db.messages order by `created_date` desc');
			while ($read = mysqli_fetch_assoc($query)) {
				$result[] = $read;
			}

			return $result;
		}

		function deleteMessage($id){
			$result= mysqli_fetch_assoc(mlavu_query("SELECT * FROM `poslavu_MAIN_db`.`messages` WHERE `id`='[1]'", $id));
			$val= '';
			if( $result['_deleted']==1)
				$val=2;
			else if($result['_deleted']==2)
				$val=0;
			else if($result['_deleted']==0)
				$val=1;

			$query = mlavu_query("Update `poslavu_MAIN_db`.`messages` SET `_deleted`='[2]' where `id`= '[1]' ", $id, $val);

			if(mlavu_dberror())
				return mlavu_dberror();
			else
				return "success";

		}


	}
?>