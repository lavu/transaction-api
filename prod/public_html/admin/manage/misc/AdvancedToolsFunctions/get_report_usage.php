<?php
	require_once($_SERVER['DOCUMENT_ROOT']."/cp/resources/core_functions.php");
	if(isset($_POST['start_date']) && isset($_POST['end_date'])){
		if( isset($_POST['data_name']))
			get_report_usage_between($_POST['start_date'], $_POST['end_date'], $_POST['data_name']);
		else
			get_report_usage_between($_POST['start_date'], $_POST['end_date']);
	}
	
	function get_report_usage_between($start, $end, $dataname=false){
		$query='';
		$start= date("Y-m-d", strtotime($start));
		$end= date("Y-m-d", strtotime($end));
		if($dataname){
			$query=mlavu_query("SELECT * FROM `poslavu_MAIN_db`.`customer_backend_usage` where (`date` BETWEEN '[1]' AND '[2]') AND `dataname`='[3]'", $start,$end, $dataname);
		}else{
			$query=mlavu_query("SELECT * FROM `poslavu_MAIN_db`.`customer_backend_usage` where `date` BETWEEN '[1]' AND '[2]'",$start, $end);
		}
		
		$report_arr= array();
		while($result= mysqli_fetch_assoc($query)){
			if(array_key_exists($result['link_clicked'], $report_arr))
				$report_arr[$result['link_clicked']]+=$result['num_clicks'];
			else
				$report_arr[$result['link_clicked']]=$result['num_clicks'];
		}
		arsort($report_arr);
		$html='';
		foreach($report_arr as $report=>$clicks){
			$html .="<tr><td>". $report."</td><td>". $clicks."</td></tr>";	
		}
		echo $html;
	}
?>