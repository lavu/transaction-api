<?php

	global $o_emailAddressesMap;
	require_once($_SERVER['DOCUMENT_ROOT']."/manage/error_config.php");
	require_once(dirname(__FILE__)."/../../../cp/resources/lavuquery.php");
	require_once(dirname(__FILE__)."/../../../cp/resources/json.php");
	require_once(dirname(__FILE__)."/../../../sa_cp/billing/package_levels_object.php");
	require_once(dirname(__FILE__)."/../../../sa_cp/billing/payment_profile_functions.php");
	require_once(dirname(__FILE__)."/../../../manage/globals/email_addresses.php");
	require_once(dirname(__FILE__)."/../../login.php");
	require_once(dirname(__FILE__)."/../autoTable.php");
	require_once(dirname(__FILE__)."/distro_tools/custom_purchases.php");
	if (!isset($o_package_container))
		$o_package_container = new package_container();
	if (!isset($maindb))
		$maindb = "poslavu_MAIN_db";
	if(session_id()==''){
		session_start();
	}

	function drawAllDistributorTools() {
		$s_retval = '';
		$s_retval .= drawScripts();
		$s_retval .= drawStyle();
		$s_retval .= drawGiveRemoveLicenses();
		$s_retval .= drawLicenseQuantityManager();
		$s_retval .= drawDistroPointsManager();
		$s_retval .= drawSpecialLicenses();
		$s_retval .= drawDistroDeviceManagement();
		$s_retval .= drawDistroCommissionManagement();
		$s_retval .= drawDistroCustomPurchases();
		$s_retval .= drawDistroCreatedAccounts();
		return $s_retval;
	}

	function get_array_of_distros_for_license_management($include_deactivated=FALSE, $extra_fields='') {
		global $maindb;

		$s_where_clause = 'WHERE `username`!=""';
		if (!$include_deactivated) $s_where_clause .=  "AND `access`!='deactivated'";
		$distros_query = mlavu_query("SELECT `username`,`f_name`,`l_name`,`company`$extra_fields FROM `[maindb]`.`resellers` $s_where_clause",
			array("maindb"=>$maindb));
		if (!$distros_query) {
			error_log("bad query: get_array_of_distros_for_license_management");
			return array();
		}
		if (mysqli_num_rows($distros_query) == 0) {
			error_log("no distros: get_array_of_distros_for_license_management");
			return array();
		}

		$a_retval = array();
		while ($distro_read = mysqli_fetch_assoc($distros_query))
			$a_retval[] = $distro_read;
		mysqli_free_result($distros_query);

		return $a_retval;
	}

	function get_array_of_distros_for_generic_list() {
		global $maindb;

		require_once(dirname(__FILE__).'/../../../cp/objects/json_decode.php');
		$o_json = new Json();
		$a_distros = get_array_of_distros_for_license_management(FALSE, ',`email`,`website`,`signup_date`,`special_json`');

		$distro_connects_query = mlavu_query("SELECT `most_recent_ts`,`resellername` FROM `[maindb]`.`reseller_connects` ORDER BY `most_recent_ts` DESC",
			array('maindb'=>$maindb));
		if ($distro_connects_query !== FALSE && mysqli_num_rows($distro_connects_query) > 0) {
			$a_distro_connects = array();
			while ($row = mysqli_fetch_assoc($distro_connects_query))
				if (!isset($a_distro_connects[$row['resellername']]))
					$a_distro_connects[$row['resellername']] = $row['most_recent_ts'];
			mysqli_free_result($distro_connects_query);
			foreach($a_distros as $k=>$a_distro) {
				$a_distros[$k]['most_recent_ts'] = (isset($a_distro_connects[$a_distro['username']])) ? $a_distro_connects[$a_distro['username']] : '';
			}
		} else {
			foreach($a_distros as $k=>$a_distro)
				$a_distros[$k]['most_recent_ts'] = '&nbsp;';
		}

		foreach($a_distros as $k=>$a_distro) {
			$o_specials = $o_json->decode($a_distro['special_json']);
			unset($a_distros[$k]['special_json']);
			$a_distros[$k]['mercuryAcceptsList'] = ($o_specials->mercuryAcceptsList == '1') ? 'Yes' : 'No';
		}

		return $a_distros;
	}

	function drawSpecialLicenses() {
		if (!can_access('mod_distributor_special_licenses'))
			return '';

		return '
			<div class="manage_grouping_header">
				Special License Discounts
			</div>
			<div class="manage_grouping" id="distributor_special_licenses">
				First select a distributor from the list above...
			</div>
		';
	}

	function drawDistroDeviceManagement() {
		if (!can_access('mod_distributor_device_management'))
			return '';

		return '
			<div class="manage_grouping_header">
				Manage Distributor Devices
			</div>
			<div class="manage_grouping" id="manage_distributor_devices">
				First select a distributor from the list above...
			</div>
		';
	}

	function drawDistroCommissionManagement() {
		if (!can_access('mod_distributor_commission_management'))
			return '';

		return '
			<div class="manage_grouping_header">
				Manage Distributor Commission
			</div>
			<div class="manage_grouping" id="manage_distributor_commission">
				First select a distributor from the list above...
			</div>
		';
	}

	/**
	 * Draws the distributor tool to view created accounts
	 * @return string: an html string, including the manage grouping header and body
	 */
	function drawDistroCreatedAccounts() {

		if (!can_access('distro_search'))
			return "";

		// initialize some values
		$s_retval = "";

		// draw the header
		$s_retval .= "
		<div class='manage_grouping_header'>
			View Accounts Created By Distributors
		</div>";

		// draw the body
		$s_retval .= "
		<div class='manage_grouping distro_created_accounts'>
			First select a distributor from the list above.
		</div>";

		return $s_retval;
	}

	/**
	 * Draws the distributor tool to manage custom purchases
	 * @return string: an html string, including the manage grouping header and body
	 */
	function drawDistroCustomPurchases() {

		global $o_distroToolsCustomPurchases;
		if (!$o_distroToolsCustomPurchases->check_permission())
			return "";

		// initialize some values
		$s_retval = "";

		// draw the header
		$s_retval .= "
		<div class='manage_grouping_header'>
			Manage Distributor Buy-Ins and Custom Purchases
		</div>";

		// draw the body
		$s_retval .= "
		<div class='manage_grouping distro_custom_purchases'>
			First select a distributor from the list above.
		</div>";

		return $s_retval;
	}

	// used to output raw html to view a special license's history
	function drawDistroSpecialLicenseHistory($s_distro, $s_special_lic_index) {

		// get the distributor
		require_once(dirname(__FILE__).'/../../../distro/beta/distro_user.php');
		if (!isset($_SESSION['distro_user_'.$s_distro]) || ($user = unserialize($_SESSION['distro_user_'.$s_distro])) === FALSE || TRUE) {
			$user = new distro_user();
			$user->set_leads_accounts($s_distro, '1');
			if ($user->set_all_info($s_distro) === FALSE)
				return 'print error[*note*]The distributor "'.$s_distro.'" doesn\'t exist.';
			$_SESSION['distro_user_'.$s_distro] = serialize($_SESSION['distro_user_'.$s_distro]);
		}

		// get the special license
		$o_special_licenses = $user->get_special_licenses(-9999, FALSE);
		if (!isset($o_special_licenses->$s_special_lic_index))
			return 'The special license can\'t be found.';
		$o_special_license = $o_special_licenses->$s_special_lic_index;

		// draw its history
		return "<pre style='text-align:left;'>".print_r($o_special_license->history,TRUE)."</pre>";
	}

	function drawLicenseQuantityManager() {
		$s_retval = '
			<div class="manage_grouping_header">
				Distributor Standing by Licenses Sold
			</div>
			<div class="manage_grouping">
				Select a range for the number of licenses sold in the last 90 days:
				<form>
					<input type="button" onclick="
						$(\'#drawLicenseQuantityManagerForm\').find(\'[name=min_licenses]\').val(0);
						$(\'#drawLicenseQuantityManagerForm\').find(\'[name=max_licenses]\').val(2);
						$(\'#drawLicenseQuantityManagerForm\').find(\'[type=button]\').click();
						" value="Certified - Probation" />
					<input type="button" onclick="
						$(\'#drawLicenseQuantityManagerForm\').find(\'[name=min_licenses]\').val(3);
						$(\'#drawLicenseQuantityManagerForm\').find(\'[name=max_licenses]\').val(5);
						$(\'#drawLicenseQuantityManagerForm\').find(\'[type=button]\').click();
						" value="Certified" />
					<input type="button" onclick="
						$(\'#drawLicenseQuantityManagerForm\').find(\'[name=min_licenses]\').val(6);
						$(\'#drawLicenseQuantityManagerForm\').find(\'[name=max_licenses]\').val(8);
						$(\'#drawLicenseQuantityManagerForm\').find(\'[type=button]\').click();
						" value="Preferred" />
					<input type="button" onclick="
						$(\'#drawLicenseQuantityManagerForm\').find(\'[name=min_licenses]\').val(9);
						$(\'#drawLicenseQuantityManagerForm\').find(\'[name=max_licenses]\').val(10000);
						$(\'#drawLicenseQuantityManagerForm\').find(\'[type=button]\').click();
						" value="Super" />
				</form>
				<form id="drawLicenseQuantityManagerForm">
					<input type="hidden" name="command" value="drawLicenseQuantityManager_drawDistros">
					Minimum Licenses: <select name="min_licenses">';
		for($i = 0; $i < 21; $i++) $s_retval .= '<option>'.$i.'</option>';
		$s_retval .= '</select>
					Maximum Licenses: <select name="max_licenses">';
		for($i = 0; $i < 91; $i++) $s_retval .= '<option>'.$i.'</option>';
		$s_retval .= '<option>10000</option></select>
					From the past: <select name="previous_days">';
		for($i = 0; $i < 365; $i+=15)
			if ($i == 90)
				$s_retval .= '<option value="'.$i.'" selected>'.$i.' days</option>';
			else
				$s_retval .= '<option value="'.$i.'">'.$i.' days</option>';
		for($i = 400; $i < 5100; $i+=100)
			$s_retval .= '<option value="'.$i.'">'.$i.' days</option>';
		$s_retval .= '</select>
					<input type="button" value="Search" onclick="send_ajax_call_from_form(\'/manage/misc/AdvancedToolsFunctions/manageDistributorLicenses.php\',\'drawLicenseQuantityManagerForm\');" />
					<label class="errors">&nbsp;</label>
				</form>
				<div id="drawLicenseQuantityManagerTableContainer">&nbsp</div>
				<div id="drawLicensesQuantityManagerLicensesContainer">&nbsp</div>
			</div>';

		return $s_retval;
	}

	function drawLicenseQuantityManager_drawDistros($s_minimum, $s_maximum) {
		global $maindb;
		$i_min = (int)$s_minimum;
		$i_max = (int)$s_maximum;
		$i_prev_days = (int)$_POST['previous_days'];

		// get the licenses sold per distro
		$prev_date = date('Y-m-d H:i:s', strtotime('-'.$i_prev_days.' days'));
		require_once(dirname(__FILE__)."/../../../distro/distro_data_functions.php");
		$a_distro_quantities = get_distributor_licenses_sold_since($prev_date, $i_min, $i_max);
		if (count($a_distro_quantities) == 0)
			return '';

		// load all the distro data
		$a_distro_in = array();
		foreach($a_distro_quantities as $a)
			$a_distro_in[$a['resellername']] = $a['resellername'];
		$s_in_clause = ConnectionHub::getConn('poslavu')->getDBAPI()->arrayToInClause($a_distro_in);
		$a_distros = ConnectionHub::getConn('poslavu')->getDBAPI()->getAllInTable('resellers', NULL, TRUE, array('whereclause'=>'WHERE `username` '.$s_in_clause.' AND `access` != \'deactivated\''));
		$a_last_connect = ConnectionHub::getConn('poslavu')->getDBAPI()->getAllInTable('reseller_connects', NULL, TRUE, array('whereclause'=>'WHERE `resellername` '.$s_in_clause.' GROUP BY `resellername` ORDER BY `most_recent_ts` DESC'));

		// do something useful with that data (like get the data that I want in the array that I want)
		$a_data = array();
		foreach($a_distros as $a_distro) {
			$s_distro_quantity = 0;
			foreach($a_distro_quantities as $a_distro_quantity)
				if ($a_distro_quantity['resellername'] == $a_distro['username']) {
					$s_distro_quantity = $a_distro_quantity['count'];
					break;
				}
			$s_distro_last_connect = '';
			foreach($a_last_connect as $a_connection)
				if ($a_connection['resellername'] == $a_distro['username'] && $a_connection['most_recent_ts'] != '0000-00-00 00:00:00') {
					$s_distro_last_connect = date('Y-m-d H:i', strtotime($a_connection['most_recent_ts']));
					break;
				}
			$s_signup_date = '';
			if ($a_distro['signup_date'] != '')
				$s_signup_date = date('Y-m-d', strtotime($a_distro['signup_date']));
			$a_data[] = array($a_distro['f_name'], $a_distro['l_name'], $a_distro['username'], $a_distro['company'], $a_distro['email'], $s_distro_quantity, $s_distro_last_connect, $s_signup_date);
		}

		// draw the table
		$a_headers = array('First Name', 'Last Name', 'Username', 'Company Name', 'Email', 'Applied Last '.$i_prev_days, 'Last Connect', 'Signup');
		$s_retval = 'set item html[*note*]#drawLicenseQuantityManagerTableContainer[*findby*]';
		$s_retval .= auto_build_table($a_headers, $a_data, "apply_class_to_single_element($(this),'LicenseQuantityManager_distroSelected');loadQuantityManagerLicenses($(this));", 'LicenseQuantityManagerTable');
		return $s_retval;
	}

	function drawLicenseQuantityManager_drawLicenses($s_distrocode) {
		global $maindb;
		global $o_package_container;
		$i_prev_days = (int)$_POST['previous_days'];

		// load all the distro and restaurant data
		$prev_date = date('Y-m-d H:i:s', strtotime('-'.$i_prev_days.' days'));
		$a_distro_lsold = ConnectionHub::getConn('poslavu')->getDBAPI()->getAllInTable('reseller_licenses', array('prev_date'=>$prev_date, 'distro_code'=>$s_distrocode), TRUE, array('whereclause'=>"WHERE `applied` > '[prev_date]' AND `resellername`='[distro_code]'"));
		$a_restaurantid_in = array();
		foreach($a_distro_lsold as $a_license)
			$a_restaurantid_in[$a_license['restaurantid']] = $a_license['restaurantid'];
		$s_in_clause = ConnectionHub::getConn('poslavu')->getDBAPI()->arrayToInClause($a_restaurantid_in);
		$a_restaurants = ConnectionHub::getConn('poslavu')->getDBAPI()->getAllInTable('restaurants', NULL, TRUE, array('whereclause'=>"WHERE `id` $s_in_clause"));

		// build the data
		$a_data = array();
		foreach($a_restaurants as $a_restaurant) {
			$s_created_date = date('Y-m-d', strtotime($a_restaurant['created']));
			$s_last_activity_date = date('Y-m-d', strtotime($a_restaurant['last_activity']));
			$s_license_applied = '';
			foreach($a_distro_lsold as $a_license)
				if ($a_license['restaurantid'] == $a_restaurant['id']) {
					$s_license_applied = date('Y-m-d', strtotime($a_license['applied']));
				}
			$s_disabled = '<div style="color:#aecd37;">E</div>';
			if ($a_restaurant['disabled'] != '0')
				$s_disabled = '<div style="color:#9D1309;">D</div>';
			$s_opencp_button = '<button class="buttonClass" onclick=\'window.open("misc/MainFunctions/custLogin.php?site=old&mode=login&custID='.$a_restaurant['id'].'&custCode='.$a_restaurant['data_name'].'", "_blank");  window.focus();\'>Login</button>';
			$a_package = find_package_status($a_restaurant['id']);
			$s_package = $o_package_container->get_printed_name_by_attribute('level', $a_package['package']);
			$a_data[] = array($a_restaurant['company_name'], $a_restaurant['data_name'], $s_created_date, $s_license_applied, $s_disabled, $s_last_activity_date, $a_restaurant['license_status'], $s_package, $s_opencp_button);
		}

		// draw the table
		$a_headers = array('Company Name', 'Company Code', 'Created', 'License Applied', 'Disabled', 'Last Activity', 'Status', 'Level', 'CP Access');
		$s_retval = '<div style="color:black;margin-top:20px;">
			<font style="font-weight:bold;">Licenses:</font><br />';
		$s_retval .= auto_build_table($a_headers, $a_data, "apply_class_to_single_element($(this),'LicenseQuantityManager_licenseSelected');", 'LicenseQuantityManagerTable_Licenses');
		$s_retval .= '</div>';
		return $s_retval;
	}

	function drawDistroPointsManager() {
		return '
			<div class="manage_grouping_header">
				Add/Remove Distributor Points:
			</div>
			<div id="distributor_add_remove_points" class="manage_grouping">First select a distributor from the list above.</div>';
	}

	function drawGiveRemoveLicenses() {
		global $o_package_container;
		if (!isset($o_package_container))
			$o_package_container = new package_container();

		/*$a_distro_data = get_array_of_distros_for_license_management();
		$a_distro_headers = array("Username", "First Name", "Last Name", "Company Name");
		$s_distro_table = auto_build_table($a_distro_headers, $a_distro_data, "apply_class_to_single_element($(this),'distro_selected');get_list_of_distributor_licenses_to_remove($(this));");*/

		$a_give_license_data_names = $o_package_container->get_highest_base_package_names();
		$a_give_license_data = array();
		foreach($a_give_license_data_names as $v) {
			$a_give_license_data[] = array($v);
			if (strpos($v, 'Platinum') !== false) $a_give_license_data[] = array('Platinum2');  // Manually adding Platinum2, per Gibbs 2014-12-19.
		}
		$a_give_license_headers = array("License Name");
		$s_give_license_table = auto_build_table($a_give_license_headers, $a_give_license_data, "apply_class_to_single_element($(this),'license_selected');");

		$a_distros_generic_information_data = get_array_of_distros_for_generic_list();
		$a_distros_generic_information_headers = array("Username", "First Name", "Last Name", "Company Name", "Email", "Website", "Signup", "Last Connect", "Mercury");
		$s_distros_generic_information = auto_build_table($a_distros_generic_information_headers, $a_distros_generic_information_data, "apply_class_to_single_element($(this),'distro_selected');get_list_of_distributor_licenses_to_remove($(this));");

		$s_retval = '
		<div class="manage_grouping_header">
			Choose a distributor
		</div>
		<div class="manage_grouping">
			All active distributors<br />
			'.$s_distros_generic_information.'
		</div>';
		if (can_access('distros_add_license') || can_access('distros_remove_license'))
			$s_retval .= '
		<div class="manage_grouping_header">
			Add/Remove Licenses
		</div>
		<div class="manage_grouping">
			<table><tr>
';
		if (can_access('distros_add_license'))
			$s_retval .= '
			<td>Choose a license to give</td>';
		if (can_access('distros_remove_license'))
			$s_retval .= '
			<td>Choose an unassigned license to remove</td>';
		if (can_access('distros_add_license') || can_access('distros_remove_license'))
			$s_retval .= '
		</tr>
		<tr>';
		if (can_access('distros_add_license'))
			$s_retval .= '
			<td style="padding-left:20px;vertical-align:top;">
				<div style="max-height:100px;overflow-y:hidden;">'.$s_give_license_table.'</div><br />
				<div>Give license to distributor:</div><br />
				<div><input type="button" onclick="crossbrowser_stopPropagation(event);give_new_license_to_distributor();" value="Apply License" /></div><br />
				<div><font id="give_new_license_label"></font></div>
			</td>';
		if (can_access('distros_remove_license'))
			$s_retval .= '
			<td style="padding-left:20px;vertical-align:top;">
				<div id="list_of_distro_licenses_container">&nbsp;</div><br />
				<div><font id="remove_license_label"></font></div>
			</td>';
		if (can_access('distros_add_license') || can_access('distros_remove_license'))
			$s_retval .= '
			</tr></table>
		</div>';
		$s_retval .= '
		<div class="manage_grouping_header">
			All distributor licenses
		</div>
		<div class="manage_grouping">
			<div id="all_distributor_licenses_container">First select a distributor from the list above.</div>
		</div>
		<div class="manage_grouping_header">
			Distributor Leads Statistics:
		</div>
		<div id="distributor_leads_statistics" class="manage_grouping">
			First select a distributor from the list above.
		</div>';

		return $s_retval;
	}

	function drawStyle() {
		$css = file_get_contents(dirname(__FILE__)."/../../css/autoTable.css");
		return "
		<style type='text/css'>
			tr.autoTable.distro_selected {
				background-color: #333;
				color: #fff;
			}
			tr.autoTable.license_selected {
				background-color: #333;
				color: #fff;
			}
			tr.autoTable.distro_all_licenses_selected {
				background-color: #333;
				color: #fff;
			}
			tr.autoTable.generic_distro_selected {
				background-color: #333;
				color: #fff;
			}
			tr.autoTable.LicenseQuantityManager_distroSelected {
				background-color: #333;
				color: #fff;
			}
			tr.autoTable.LicenseQuantityManager_licenseSelected {
				background-color: #333;
				color: #fff;
			}
			table.table_of_distro_special_licenses td {
				border-left:1px solid #ccc;
				border-right:1px solid #ccc;
				color:black;
				padding:0 5px;
			}
		</style>";
	}

	function manage_distributor_devices($s_distro) {

		// get the user
		require_once(dirname(__FILE__)."/../../../distro/beta/distro_user.php");
		if (!isset($_SESSION['distro_user_'.$s_distro]) || ($user = unserialize($_SESSION['distro_user_'.$s_distro])) === FALSE || TRUE) {
			$user = new distro_user();
			$user->set_leads_accounts($s_distro, '1');
			if ($user->set_all_info($s_distro) === FALSE)
				return 'print error[*note*]The distributor "'.$s_distro.'" doesn\'t exist.';
			$_SESSION['distro_user_'.$s_distro] = serialize($_SESSION['distro_user_'.$s_distro]);
		}

		// get some other data and
		// check if number of devices should be set
		$s_retval = "";
		$i_num_devices = (int)$_POST['num_devices'];
		if ($i_num_devices >= 0) {
			$a_values = array();
			foreach($_POST as $k=>$v)
				if (is_numeric($k) && (int)$k > 0)
					$a_values[] = $v;
			$user->num_allowed_admin_devices($i_num_devices);
			$user->allowed_admin_devices($a_values);
			return '{"success":true,"success_msg":""}';
		}
		$i_num_devices = $user->num_allowed_admin_devices();

		// draw the form to edit the number of devices
		$s_retval .= "
		<script type='text/javascript' src='/manage/js/lavugui.js'></script>
		<script type='text/javascript' src='/manage/js/lavuform.js'></script>
		<div class='edit_distributor registered_devices' style='padding:19px;'>
			<legend>Registered Devices
				<div style='display:inline-table; width:200px; height:20px; text-align:right; color:blue; text-decoration:underline;'>
					<span class='edit_icon'>Edit</span>
					<span class='done_icon' style='display:none;'>Done</span>
				</div>
			</legend>
			<input type='hidden' class='draw_info_form_submit_url' name='' value='/manage/misc/AdvancedToolsFunctions/manageDistributorLicenses.php' />
			<input type='hidden' class='draw_info_form_name' name='' value='registered_devices' />
			<fieldset class='my_info_fieldset draw_info_form_noedit' style='margin:5px 0 0 0; padding:10px;'>
				User: ".$user->get_info_attr("username")."<br /><br />
				Registered devices: ".$user->draw_allowed_admin_devices()."<br />
				<div>
					<input type='hidden' name='type' value='number' />
					<input type='hidden' name='index' value='1000' />
					<input type='hidden' name='name' value='num_devices' />
					Number of allowed devices: <font class='value'>{$i_num_devices}</font>
				</div>
				<div style='display:none;'>
					<input type='hidden' name='type' value='textbox' />
					<input type='hidden' name='style' value='display:none;' />
					<input type='hidden' name='index' value='1001' />
					<input type='hidden' name='name' value='distributor' />
					<font style='display:none;' class='value'>{$s_distro}</font>
				</div>
				<div style='display:none;'>
					<input type='hidden' name='type' value='textbox' />
					<input type='hidden' name='style' value='display:none;' />
					<input type='hidden' name='index' value='1002' />
					<input type='hidden' name='name' value='command' />
					<font style='display:none;' class='value'>manage_distributor_devices</font>
				</div>
				<font>
					<input type='hidden' name='attributes' value='{--class--:--submit--}' />
					<input type='hidden' name='type' value='button' />
					<input type='hidden' name='index' value='1004' />
					<font style='display:none;' class='value'>Submit</font>
				</font>
			</fieldset>
			<fieldset class='my_info_fieldset draw_info_form_edit' style='margin:5px 0 0 0; padding:0; display:none;'>
			</fieldset>
		</div>
		<script type='text/javascript'>
			$(document).ready(function() { setTimeout(function() {
				Lavu.Form.draw_info_form($('.edit_distributor.registered_devices'));
			}, 300); });
		</script>";

		return $s_retval;
	}

	function view_distributor_created_accounts($s_distro) {

		global $loggedin;

		// check accesses
		if (!can_access('distro_search'))
			return "You don't have permission to view these statistics.";

		// load the distributor's created accounts
		$a_created_accounts_db = ConnectionHub::getConn('poslavu')->getDBAPI()->getAllInTable("reseller_history", array('reseller_name'=>$s_distro, 'action'=>'created new account'), TRUE);
		$a_created_accounts = array();
		foreach($a_created_accounts_db as $a_created_account) {
			$a_details_parts = explode("\n", $a_created_account['details']);
			$s_dataname = "";
			foreach($a_details_parts as $s_detail_part) {
				$a_detail_part = explode(" ", trim($s_detail_part));
				if ($a_detail_part[0] == "dataname:") {
					$s_dataname = $a_detail_part[1];
					break;
				}
			}
			$a_created_accounts[] = array(
				'reseller_name'=>$a_created_account['reseller_name'],
				'datetime'=>$a_created_account['datetime'],
				'dataname'=>$s_dataname,
				'details'=>"<a onclick='alert(\"".str_replace(array("'", '"'),"",$s_dataname)."\");'>details</a>"
			);
		}

		// view the distributor's created accounts
		return auto_build_table(array('reseller_name','datetime','dataname','details'), $a_created_accounts);
	}

	function manage_distributor_commission($s_distro) {

		global $loggedin;

		// get the user
		require_once(dirname(__FILE__)."/../../../distro/beta/distro_user.php");
		require_once(dirname(__FILE__)."/../../../distro/distro_data_functions.php");
		if (!isset($_SESSION['distro_user_'.$s_distro]) || ($user = unserialize($_SESSION['distro_user_'.$s_distro])) === FALSE || TRUE) {
			$user = new distro_user();
			$user->set_leads_accounts($s_distro, '1');
			if ($user->set_all_info($s_distro) === FALSE)
				return 'print error[*note*]The distributor "'.$s_distro.'" doesn\'t exist.';
			$_SESSION['distro_user_'.$s_distro] = serialize($_SESSION['distro_user_'.$s_distro]);
		}

		// get some other data and
		// check if number of devices should be set
		$s_retval = "";
		$i_is_custom_commission = (int)$_POST['custom_commission'];
		$i_commission = (int)$_POST['commission'];
		$s_reason_for_changing = $_POST['reason_for_changing'];
		if ($s_reason_for_changing == "Reason For Changing")
			$s_reason_for_changing = "";
		$i_old_commission = get_distro_commision($a_distro, $user->get_info_attr('username'));
		$i_old_is_custom_commission = $user->use_custom_commission();
		if ($i_commission >= 0) {
			$id = $user->get_info_attr('id');
			$username = $user->get_info_attr('username');

			// check for and save commission
			if ($i_commission !== $i_old_commission) {
				$user->update_commission($i_commission, $loggedin, $s_reason_for_changing);
			}

			// check for and save use_custom_commission
			if ($i_is_custom_commission !== $i_old_is_custom_commission) {
				$i_is_custom_commission = $user->use_custom_commission($i_is_custom_commission, $loggedin, $s_reason_for_changing);
			}

			return '{"success":true,"success_msg":""}';
		}
		$a_distro = array();
		$i_is_custom_commission = $i_old_is_custom_commission;
		$i_commission = $i_old_commission;

		// get the previous commissions
		$s_previous_commissions = $user->previous_commissions_toString(TRUE);
		if (strlen($s_previous_commissions) > 0)
			$s_previous_commissions = "<br />Previous Commissions: <br />{$s_previous_commissions}";

		// draw the form to edit the number of devices
		$s_retval .= "
		<script type='text/javascript' src='/manage/js/lavugui.js'></script>
		<script type='text/javascript' src='/manage/js/lavuform.js'></script>
		<div class='edit_distributor distro_commission' style='padding:19px;'>
			<legend>Registered Devices
				<div style='display:inline-table; width:200px; height:20px; text-align:right; color:blue; text-decoration:underline;'>
					<span class='edit_icon'>Edit</span>
					<span class='done_icon' style='display:none;'>Done</span>
				</div>
			</legend>
			<input type='hidden' class='draw_info_form_submit_url' name='' value='/manage/misc/AdvancedToolsFunctions/manageDistributorLicenses.php' />
			<input type='hidden' class='draw_info_form_name' name='' value='distro_commission' />
			<fieldset class='my_info_fieldset draw_info_form_noedit' style='margin:5px 0 0 0; padding:10px;'>
				User: ".$user->get_info_attr("username")."<br /><br />
				<div>
					<input type='hidden' name='type' value='number' />
					<input type='hidden' name='index' value='1000' />
					<input type='hidden' name='name' value='commission' />
					Current Commission: <font class='value'>{$i_commission}</font>%
				</div>
				<div>
					<input type='hidden' name='type' value='checkbox' />
					<input type='hidden' name='index' value='1001' />
					<input type='hidden' name='name' value='custom_commission' />
					Use Custom Commission: <font class='value'>{$i_is_custom_commission}</font>
				</div>
				(non-custom commissions update every night)<br />
				<div style='display:none;'>
					<input type='hidden' name='type' value='textbox' />
					<input type='hidden' name='style' value='display:none;' />
					<input type='hidden' name='index' value='1002' />
					<input type='hidden' name='name' value='distributor' />
					<font style='display:none;' class='value'>{$s_distro}</font>
				</div>
				<div style='display:none;'>
					<input type='hidden' name='type' value='textbox' />
					<input type='hidden' name='style' value='display:none;' />
					<input type='hidden' name='index' value='1003' />
					<input type='hidden' name='name' value='command' />
					<font style='display:none;' class='value'>manage_distributor_commission</font>
				</div>
				<div>
					<input type='hidden' name='type' value='textbox' />
					<input type='hidden' name='index' value='1004' />
					<input type='hidden' name='name' value='reason_for_changing' />
					<font style='display:none;' class='value'>Reason For Changing</font>
				</div>
				<font>
					<input type='hidden' name='attributes' value='{--class--:--submit--}' />
					<input type='hidden' name='type' value='button' />
					<input type='hidden' name='index' value='1005' />
					<font style='display:none;' class='value'>Submit</font>
				</font>
				{$s_previous_commissions}
			</fieldset>
			<fieldset class='my_info_fieldset draw_info_form_edit' style='margin:5px 0 0 0; padding:0; display:none;'>
			</fieldset>
		</div>
		<script type='text/javascript'>
			$(document).ready(function() { setTimeout(function() {
				Lavu.Form.draw_info_form($('.edit_distributor.distro_commission'));
			}, 300); });
		</script>";

		return $s_retval;
	}

	function drawScripts() {
		$s_retval = "";
		ob_start();
		?>
		<script type="text/javascript" src="/manage/js/collapsables.js"></script>
		<script type="text/javascript">
			function apply_class_to_single_element(jelement, classname) {
				while ($("."+classname).length > 0)
					$("."+classname).removeClass(classname);
				jelement.addClass(classname);
			}

			give_new_license_to_distributor_timer = new Date().getTime();
			function give_new_license_to_distributor() {
				var timediff = (new Date().getTime()) - give_new_license_to_distributor_timer;
				if (timediff < 2)
					return;
				give_new_license_to_distributor_timer = new Date().getTime();
				var jdistro_row = $(".distro_selected");
				if (jdistro_row.length == 0)
					return;
				var distributor = $(jdistro_row.find("td")[0]).text();
				var jlicense_row = $(".license_selected");
				if (jlicense_row.length == 0)
					return;
				var reason = "";
				while (reason == "") { reason = prompt("Please provide a reason for giving this license to the distributor:"); }
				if (reason == null)
					return;
				var license = $(jlicense_row.find("td")[0]).text();
				var url = "/manage/misc/AdvancedToolsFunctions/manageDistributorLicenses.php";
				var data = {command: "give_new_license_to_distributor", distributor: distributor, license: license, user: "<?= $_SESSION['admin_username'] ?>", reason: reason};
				jlicense_row.removeClass("license_selected");
				send_ajax_call_return_to_label($("#give_new_license_label"), url, data, true, function(a){jdistro_row.click();});
			}

			function loadQuantityManagerLicenses(jrow) {
				var s_distrocode = $(jrow.children("td")[2]).text();
				var url = "/manage/misc/AdvancedToolsFunctions/manageDistributorLicenses.php";
				var data = {command: "drawLicenseQuantityManager_drawLicenses", distributor: s_distrocode};
				send_ajax_call_return_to_label($("#drawLicensesQuantityManagerLicensesContainer"), url, data, true, null);
			}

			function get_list_of_distributor_licenses(jdistro_row) {
				var s_distro_code = $(jdistro_row.find("td")[0]).text();
				var url = "/manage/misc/AdvancedToolsFunctions/manageDistributorLicenses.php";
				var data = {command: "get_list_of_distributor_licenses", distributor: s_distro_code};
				var jcontainer = $("#all_distributor_licenses_container");
				send_ajax_call_return_to_label(jcontainer, url, data, true, function(a){$("#all_distributor_licenses_container").css({ color: "#000" });$("#all_distributor_licenses_container").html(a);});
			}

			function get_distributor_leads_statistics(jdistro_row) {
				var s_distro_code = $(jdistro_row.find("td")[0]).text();
				var url = "/manage/misc/AdvancedToolsFunctions/manageDistributorLicenses.php";
				var data = {command: "get_distributor_leads_statistics", distributor: s_distro_code};
				var jcontainer = $("#distributor_leads_statistics");
				send_ajax_call_return_to_label(jcontainer, url, data, true, function(a){$("#distributor_leads_statistics").css({ color: "#000" });$("#distributor_leads_statistics").html(a);});
			}

			function get_distributor_special_licenses(jdistro_row) {
				var s_distro_code = $(jdistro_row.find("td")[0]).text();
				var url = "/manage/misc/AdvancedToolsFunctions/manageDistributorLicenses.php";
				var data = {command: "get_distributor_special_licenses", distributor: s_distro_code};
				var jcontainer = $("#distributor_special_licenses");
				send_ajax_call_return_to_label(jcontainer, url, data, true, function(a){$("#distributor_special_licenses").html(a);});
			}

			function show_distributor_add_remove_points(jdistro_row) {
				var s_distro_code = $(jdistro_row.find("td")[0]).text();
				var url = "/manage/misc/AdvancedToolsFunctions/manageDistributorLicenses.php";
				var data = {command: "get_distributor_add_remove_points", distributor: s_distro_code};
				var jcontainer = $("#distributor_add_remove_points");
				send_ajax_call_return_to_label(jcontainer, url, data, true, function(a){$("#distributor_add_remove_points").css({ color: "#000" });$("#distributor_add_remove_points").html(a);});
			}

			function manage_distributor_devices(jdistro_row, i_num_devices) {
				if (typeof(i_num_devices) === 'undefined')
					i_num_devices = -1;
				var s_distro_code = $(jdistro_row.find("td")[0]).text();
				var url = "/manage/misc/AdvancedToolsFunctions/manageDistributorLicenses.php";
				var data = { command: "manage_distributor_devices", distributor: s_distro_code, num_devices: i_num_devices };
				var jcontainer = $("#manage_distributor_devices");
				send_ajax_call_return_to_label(jcontainer, url, data, true, function(a){$("#manage_distributor_devices").html(a)});
			}

			function manage_distributor_commission(jdistro_row, i_commission) {
				if (typeof(i_commission) === 'undefined')
					i_commission = -1;
				var s_distro_code = $(jdistro_row.find("td")[0]).text();
				var url = "/manage/misc/AdvancedToolsFunctions/manageDistributorLicenses.php";
				var data = { command: "manage_distributor_commission", distributor: s_distro_code, commission: i_commission };
				var jcontainer = $("#manage_distributor_commission");
				send_ajax_call_return_to_label(jcontainer, url, data, true, function(a){$("#manage_distributor_commission").html(a)});
			}

			function manage_distributor_custom_purchases(jdistro_row) {
				var s_distro_code = $(jdistro_row.find("td")[0]).text();
				var url = "/manage/misc/AdvancedToolsFunctions/manageDistributorLicenses.php";
				var data = { command: "manage_distributor_custom_purchases", distributor: s_distro_code };
				var jcontainer = $(".manage_grouping.distro_custom_purchases");
				send_ajax_call_return_to_label(jcontainer, url, data, true, function(a){$(".manage_grouping.distro_custom_purchases").html(a)});
			}

			function view_distributor_created_accounts(jdistro_row) {
				var s_distro_code = $(jdistro_row.find("td")[0]).text();
				var url = "/manage/misc/AdvancedToolsFunctions/manageDistributorLicenses.php";
				var data = { command: "view_distributor_created_accounts", distributor: s_distro_code };
				var jcontainer = $(".manage_grouping.distro_created_accounts");
				send_ajax_call_return_to_label(jcontainer, url, data, true, function(a){$(".manage_grouping.distro_created_accounts").html(a)});
			}

			function get_list_of_distributor_licenses_to_remove(jdistro_row) {
				var s_distro_code = $(jdistro_row.find("td")[0]).text();
				var url = "/manage/misc/AdvancedToolsFunctions/manageDistributorLicenses.php";
				var data = {command: "get_list_of_distributor_licenses_to_remove", distributor: s_distro_code};
				var jcontainer = $("#list_of_distro_licenses_container");
				get_list_of_distributor_licenses(jdistro_row);
				get_distributor_leads_statistics(jdistro_row);
				get_distributor_special_licenses(jdistro_row);
				show_distributor_add_remove_points(jdistro_row);
				manage_distributor_devices(jdistro_row);
				manage_distributor_commission(jdistro_row);
				manage_distributor_custom_purchases(jdistro_row);
				view_distributor_created_accounts(jdistro_row);
				send_ajax_call_return_to_label(jcontainer, url, data, false, function(a){$("#list_of_distro_licenses_container").html(a);});
				jcontainer.css({ color: "#000" });
			}

			function remove_distro_license(jdistro_license_row) {
				var a_tds = jdistro_license_row.find("td");
				lavuLog(a_tds);
				var type = $(a_tds[0]).text();
				var purchased = $(a_tds[1]).text();
				var notes = $(a_tds[2]).text();
				var jdistro_row = $("tr.autoTable.distro_selected");
				lavuLog(jdistro_row);
				if (jdistro_row.length == 0)
					return;
				var distributor = $(jdistro_row.find("td")[0]).text();
				lavuLog(distributor);
				var remove_reason = prompt("Please enter a reason for removing this license from the distributor");
				if (remove_reason === null)
					return;
				var url = "/manage/misc/AdvancedToolsFunctions/manageDistributorLicenses.php";
				var data = {command: "remove_license_from_distributor", type: type, purchased: purchased, distributor: distributor, notes: notes, remove_reason: remove_reason, user: "<?= $_SESSION['admin_username'] ?>"};
				if (send_ajax_call_return_to_label($("#remove_license_label"), url, data)) {
					jdistro_row.click();
				}
			}

			function send_ajax_call_from_form(php_file_name, form_id) {
				var jform = $("#"+form_id);
				var a_inputs = jform.find("input");
				var a_selects = jform.find("select");
				var a_textareas = jform.find("textarea");
				var inputs = $.merge($.merge(a_inputs, a_selects), a_textareas);

				var posts = {};
				var full_posts = [];
				for(var i = 0; i < inputs.length; i++) {
					var jinput = $(inputs[i])
					var name = jinput.prop("name");
					var value = jinput.val();
					if (jinput.attr("type") && jinput.attr("type") == "checkbox")
						value = (jinput[0].checked) ? 1 : 0;
					full_posts.push([name, value]);
					posts[name] = value;
				}

				jerrors_label = $(jform.find("label.errors"));
				set_html_and_fade_in(jerrors_label, "", "<font style='color:gray;'>Please wait...</font>");
				var commands_array = retval_to_commands(send_ajax_call(php_file_name, posts));
				set_html_and_fade_in(jerrors_label, "", "&nbsp;");
				interpret_common_ajax_commands(commands_array);
				for (var i = 0; i < commands_array.length; i++) {
					var command = commands_array[i][0];
					var note = commands_array[i][1];
					if (command == "print error") {
						set_html_and_fade_in($("#"+form_id).find("label.errors"), "", "<font style='color:red;'>"+note+"</font>");
					} else if (command == "print success") {
						set_html_and_fade_in($("#"+form_id).find("label.errors"), "", "<font style='color:black;font-weight:normal;'>"+note+"</font>");
					} else if (command == "set item html") {
						var parts = note.split("[*findby*]");
						var html = parts[1];
						var findby = parts[0];
						kill_children($(findby));
						$(findby).html("");
						$(findby).stop(true, true);
						$(findby).css({ opacity: 0 });
						$(findby).append(html);
						$(findby).animate({ opacity: 1 }, 500);
					} else if (command == "load page with post") {
						var posts_string = "";
						for (var i = 0; i < full_posts.length; i++)
							posts_string += '<input type="hidden" name="'+full_posts[i][0]+'" value="'+full_posts[i][1]+'" />';
						var id_string = get_unique_id();
						var create_string = '<form method="POST" action="'+note+'" id="'+id_string+'">'+posts_string+'</form>';
						$(create_string).appendTo("body");
						$("#"+id_string).submit();
					} else if (command == "clear field") {
						jform.find("input[name="+note+"]").val("");
					}
				}
			}

			function retval_to_commands(retval) {
				var commands_list = retval.split("[*command*]");
				var commands_array = [];
				for (var i = 0; i < commands_list.length; i++) {
					var command = commands_list[i].split("[*note*]");
					commands_array.push(command);
				}
				return commands_array;
			}

			function interpret_common_ajax_commands(commands_array) {
				for (var i = 0; i < commands_array.length; i++) {
					var command = commands_array[i][0];
					var note = commands_array[i][1];
					if (command == "load page") {
						window.location = note;
					} else if (command == "alert") {
						alert(note);
					}
				}
			}

			function set_html_and_fade_in(jparent_object, parent_id, html) {
				if (jparent_object === null)
					jparent_object = $("#"+parent_id);
				kill_children(jparent_object);
				jparent_object.html("");
				jparent_object.append(html);
				jparent_object.stop(true,true);
				jparent_object.children().css({opacity:0});
				jparent_object.children().animate({opacity:1},300);
			}

			function kill_children(jobject) {
				var children = jobject.children();
				for(var i = 0; i < children.length; i++)
					$(children[i]).remove();
			}

			function send_ajax_call(php_file_name, posts) {
				a_message = send_async_ajax_call(php_file_name, posts, false);
				return a_message;
			}

			function send_async_ajax_call(php_file_name, posts, async) {
				var send_ajax_call_retval = "";
				$.ajax({
					url: php_file_name,
					async: async,
					cache: false,
					data: posts,
					type: "POST",
					timeout: 10000,
					success: function(data) {
						send_ajax_call_retval = data;
					},
					error: function(xhr, ajaxOptions, thrownError) {
						if (parseInt(xhr.status) == 0 && thrownError) {
							if ((thrownError+"").indexOf("NETWORK_ERR") > -1) {
								send_ajax_call_retval = "network error encountered";
								return;
							}
						}
						alert("Error sending request: ("+xhr.status+") "+thrownError);
						send_ajax_call_retval = "error";
					}
				});
				return send_ajax_call_retval;
			}

			send_ajax_call_return_to_label_retval = false;
			function send_ajax_call_return_to_label(jlabel, url, data, async, afterfunction) {
				if (async == null) async = false;

				jlabel.css({ color: "#aaa", opacity: 0 });
				jlabel.html("Processing...");
				jlabel.animate({ opacity: 1 }, 500);
				$.ajax({
					url: url,
					type: "POST",
					async: async,
					cache: false,
					data: data,
					success: function(value) {
						var a_parts = value.split("|");
						var success = a_parts[0];
						var note = a_parts[1];
						if (value.indexOf("|") == -1)
							note = value;
						if (success == "success") {
							jlabel.css({ color: "#000", opacity: 0 });
							send_ajax_call_return_to_label_retval = true;
						} else {
							jlabel.css({ color: "#00f", opacity: 0});
							send_ajax_call_return_to_label_retval = false;
						}
						jlabel.html(note);
						jlabel.animate({ opacity: 1 }, 500);
						if (afterfunction != null)
							afterfunction(value);
					},
				});
				return send_ajax_call_return_to_label_retval;
			}

			// from http://forums.mozillazine.org/viewtopic.php?f=38&t=549571
			function crossbrowser_stopPropagation(e){
				if (!e) var e = window.event;
				e.cancelBubble = true; //Setting the cancelBubble property in browsers that don’t support it doesn’t hurt. The browser shrugs and creates the property. Of course it doesn’t actually cancel the bubbling, but the assignment itself is safe.
				if (e.stopPropagation) e.stopPropagation();
			}

			function set_special_discount_by_select(jselect) {
				var a_discounts = { "6 - 10 locations (10%)":10, "11 - 20 locations (15%)":15, "21 or more locations (20%)":20 };
				var val = jselect.val();
				var jcustom = jselect.siblings("input[name=discount_percentage]");
				lavuLog(val);
				if (val == "Custom") {
					jcustom.stop(true,true);
					jcustom.show(200);
				} else {
					jcustom.val(a_discounts[val]);
					jcustom.stop(true,true);
					jcustom.hide(200);
				}
			}

			function get_type_choice(string, possible_types, current_types, remove_types, complete_func, grouping_selector, destination_selector) {
				var jgrouping = $(grouping_selector);
				var jspecial_box = $(destination_selector);
				var jwindow = $(window);
				var new_types = [];
				var x = 0, y = 0;

				// get the types that haven\'t been assigned yet
				for (var i = 0; i < current_types.length; i++) {
					var type = current_types[i];
					if (possible_types.indexOf(type) < 0)
						continue;
					var has_type = false;
					for (var j = 0; j < remove_types.length; j++) {
						if (remove_types[j] == type) {
							has_type = true;
							break;
						}
					}
					if (!has_type) {
						new_types.push(type);
					}
				}

				// calculate the position for the new div
				x = jspecial_box.offset().left - jwindow.scrollLeft()-100;
				y = jspecial_box.offset().top - jwindow.scrollTop()-60;

				// display the types
				while($("#type_chooser").length > 0)
					$("#type_chooser").remove();
				jgrouping.append("<div id='type_chooser' style='position:fixed; top:"+y+"px; left:"+x+"px; border:1px solid black; background-color:white; padding:15px;'></div>");
				var jdiv = $("#type_chooser");
				var options = "";
				$.each(new_types, function(k,v) {
					options += "<option>"+v+"</option>";
				});
				var appendtext = "<select>"+options+"</select> <input type='button' value='Choose' /> <input type='button' value='Cancel' />"
				if (options == "") {
					appendtext = "There are no more choices available. <input type='button' value='Cancel' />";
				}
				jdiv.append(appendtext);
				jdiv.find("input[value=Cancel]").click(function(){ jdiv.remove(); complete_func(""); });
				if (jdiv.find("input[value=Choose]").length > 0)
					jdiv.find("input[value=Choose]").click(function(){ var val = jdiv.find("select").val(); jdiv.remove(); complete_func(val); });
			}

			/**
			 * Creates a prompt to add another value to the selection
			 * @param string source_selector The string used by jquery to select the dom element with the options
			 * @param string dest_selector The string used by jquery to select the dom element with the selected values
			 * @param string descriptor The general name for each value
			 * @param string grouping_selector The string used by jquery to select where to add the new dom elements
			 * @param string destination_selector The string used by jquery to select where to place the new dom elements
			 * @return null
			 */
			function create_add_prompt(source_selector, dest_selector, descriptor, grouping_selector, destination_selector) {
				var types_string = $(source_selector).val();
				var jinput = $(dest_selector);
				var types = types_string.split(",");
				var current_types_string = jinput.val();
				var current_types = current_types_string.split(",");

				get_type_choice("Choose a "+descriptor+" to add...", types, types, current_types, function(type) {
					if (type != "") {
						if (jinput.val() == "none") {
							jinput.val(type);
						} else {
							jinput.val(jinput.val()+","+type);
						}
					}
				}, grouping_selector, destination_selector);
			}

			/**
			 * Creates a prompt to remove one of the values from the selection
			 * @param string source_selector The string used by jquery to select the dom element with the options
			 * @param string dest_selector The string used by jquery to select the dom element with the selected values
			 * @param string descriptor The general name for each value
			 * @param string grouping_selector The string used by jquery to select where to add the new dom elements
			 * @param string destination_selector The string used by jquery to select where to place the new dom elements
			 * @return null
			 */
			function create_remove_prompt(source_selector, dest_selector, descriptor, grouping_selector, destination_selector) {
				var types_string = $(source_selector).val();
				var jinput = $(dest_selector);
				var types = types_string.split(",");
				var current_types_string = jinput.val();
				var current_types = current_types_string.split(",");

				get_type_choice("Choose a "+descriptor+" to add...", types, current_types, [], function(type) {
					if (type != "") {
						if (jinput.val() == type) {
							jinput.val("none");
						} else if (jinput.val().indexOf(","+type) > -1) {
							var split = jinput.val().split(","+type);
							jinput.val(split[0]+split[1]);
						} else if (jinput.val().indexOf(type+",") > -1) {
							var split = jinput.val().split(type+",");
							jinput.val(split[0]+split[1]);
						}
					}
				}, grouping_selector, destination_selector);
			}

			// for creating special distributor licensing
			function add_special_license_type() {
				create_add_prompt("input[name=special_license_types_allowed]", "input[name=special_license_types]", "type", "#distributor_special_licenses", "input[name=special_license_types]");
			}
			function remove_special_license_type() {
				create_remove_prompt("input[name=special_license_types_allowed]", "input[name=special_license_types]", "type", "#distributor_special_licenses", "input[name=special_license_types]");
			}

			// for creating special distributor purchases
			function add_create_custom_purchase_distro() {
				create_add_prompt("input[name=values_create_custom_purchase_usernames]", "input[name=create_custom_purchase_distros]", "distributor", ".manage_grouping.distro_custom_purchases", "input[name=create_custom_purchase_distros]");
			}
			function remove_create_custom_purchase_distro() {
				create_remove_prompt("input[name=values_values_create_custom_purchase_usernames]", "input[name=create_custom_purchase_distros]", "distributor", ".manage_grouping.distro_custom_purchases", "input[name=create_custom_purchase_distros]");
			}
			function add_create_custom_purchase_distro_all() {
				$("input[name=create_custom_purchase_distros]").val($("input[name=values_create_custom_purchase_usernames]").val());
			}

			setTimeout(function() {
				create_collapsables();
				collapse_collapsables();
				manage_grouping_collapse(0);
			}, 500);
		</script>
		<?php
		$s_retval .= ob_get_contents();
		ob_end_clean();

		return $s_retval;
	}

	// @$s_admin_username username of the admin logged in to manage
	function give_new_license_to_distributor($s_distro, $s_license, $s_admin_username) {
		global $maindb;
		global $o_package_container;
		global $o_emailAddressesMap;

		if (!isset($maindb))
			$maindb = "poslavu_MAIN_db";
		if (!isset($o_package_container))
			$o_package_container = new package_container();
		$s_reason = (isset($_POST['reason'])) ? $_POST['reason'] : '';
		if ($s_reason == '')
			return 'fail|Provide a reason';

		require_once(dirname(__FILE__)."/../../../distro/beta/distro_data_functions.php");
		$a_distro = get_distro_from_array_distrocode_dataname(array(), $s_distro);
		if (count($a_distro) == 0)
			return 'fail|Could not find distributor';

		$i_license_value = $o_package_container->get_value_by_attribute('name', $s_license);
		if ($i_license_value == 0)
			return 'fail|Invalid package (package has no value)';
		mlavu_query("INSERT INTO `[maindb]`.`reseller_licenses` (`type`,`resellerid`,`resellername`,`cost`,`value`,`purchased`,`notes`) VALUES ('[type]','[resellerid]','[resellername]','[cost]','[value]','[purchased]','[notes]')",
			array('maindb'=>$maindb, 'type'=>$s_license, 'resellerid'=>$a_distro['id'], 'resellername'=>$s_distro, 'cost'=>0, 'value'=>$i_license_value, 'purchased'=>date("Y-m-d H:m:i"), 'notes'=>"license given by ".$s_admin_username." ($s_reason)"));
		if (ConnectionHub::getConn('poslavu')->affectedRows() == 0)
			return 'fail|Failed to update mysql database';
		$i_license_id = mlavu_insert_id();

		// check for other licenses that were recently granted
		$duplicate_license_query = mlavu_query("SELECT `id` FROM `[maindb]`.`reseller_licenses` WHERE `purchased` > '[one_minute_ago]' AND `cost`='0' AND `resellername`='[resellername]' AND `type`='[type]' AND `id`!='[license_id]' AND `notes` LIKE 'license given by%'",
			array("maindb"=>$maindb, "one_minute_ago"=>date("Y-m-d H:m:i", time()-60), "resellername"=>$s_distro, "type"=>$s_license, "license_id"=>$i_license_id));
		if ($duplicate_license_query === FALSE)
			return 'fail|bad query to check for duplicates';
		$a_duplicate_ids = array();
		if (mysqli_num_rows($duplicate_license_query) > 0)
			while ($duplicate_license_read = mysqli_fetch_assoc($duplicate_license_query))
				$a_duplicate_ids[] = $duplicate_license_read['id'];
		if (count($a_duplicate_ids) > 0) {
			$message = count($a_duplicate_ids).' duplicate licenses possibly created in the last minute (ids '.implode(',', $a_duplicate_ids).')';
			$o_emailAddressesMap->sendEmailToAddress("billing_errors", "Duplicate licenses created in ".__FILE__, $message, "FROM: noreply@poslavu.com");
			return 'success|'.$message;
		}

		return 'success|Successfully gave '.$s_license.' license to '.$s_distro.' (license id '.mlavu_insert_id().')';
	}

	// get the html that represents the specials licenses a distributor has, and
	// provides a way to assign them
	function get_distributor_special_licenses($s_distro) {

		// get the globals
		global $maindb;
		$s_retval = '';

		// get the distributor
		require_once(dirname(__FILE__).'/../../../distro/beta/distro_user.php');
		if (!isset($_SESSION['distro_user_'.$s_distro]) || ($user = unserialize($_SESSION['distro_user_'.$s_distro])) === FALSE || TRUE) {
			$user = new distro_user();
			$user->set_leads_accounts($s_distro, '1');
			if ($user->set_all_info($s_distro) === FALSE)
				return 'print error[*note*]The distributor "'.$s_distro.'" doesn\'t exist.';
			$_SESSION['distro_user_'.$s_distro] = serialize($_SESSION['distro_user_'.$s_distro]);
		}

		// check for modifications
		$s_prepend_text = '';
		$s_append_text = '';
		if (isset($_POST['command']) && $_POST['command'] == 'add_distributor_special_licenses') {
			$s_prepend_text = 'set item html[*note*]#distributor_special_licenses[*findby*]';
			$s_append_text = add_distributor_special_licenses($user);
			if ($s_append_text !== 'print success[*note*]success')
				$s_prepend_text = '';
			$s_append_text = '[*command*]'.$s_append_text;
		}
		if (isset($_POST['command']) && $_POST['command'] == 'modify_distributor_special_licenses') {
			$s_prepend_text = 'set item html[*note*]#distributor_special_licenses[*findby*]';
			$s_append_text = modify_distributor_special_licenses($user);
			if ($s_append_text !== 'print success[*note*]success')
				$s_prepend_text = '';
			$s_append_text = '[*command*]'.$s_append_text;
		}

		// check user access
		if (!can_access('mod_distributor_special_licenses'))
			return $s_retval.'print error[*note*]You don\'t have access to this function.';

		// get the environment set up
		session_start();
		if (!account_loggedin())
			header("Location: http://admin.poslavu.com/manage/");
		require_once(dirname(__FILE__)."/../../../cp/objects/json_decode.php");
		require_once(dirname(__FILE__)."/../../../sa_cp/billing/package_levels_object.php");
		$o_package_container = new package_container();

		// find the distributor
		$a_distributors = ConnectionHub::getConn('poslavu')->getDBAPI()->getAllInTable('resellers', array('username'=>$s_distro), TRUE, array('selectclause'=>'`special_json`'));
		if (count($a_distributors) == 0)
			return $s_retval.'print error[*note*]The distributor "'.$s_distro.'" can\'t be found.';
		$o_json = Json::unencode($a_distributors[0]['special_json']);

		// get the current specials licenses
		$a_license_types = array_merge($o_package_container->get_not_lavulite_names(), $o_package_container->get_upgrades_names());
		$i_count = 0;
		$o_special_licenses = $user->get_special_licenses(-9999, FALSE, $i_count);

		//object('notes'=>notes, 'prefix'=>dataname prefix, 'quantity'=>int, 'discount_percentage'=>int, 'lic_types'=>array(), 'deleted'=>1/0, 'history'=>object('created'=>object(),'updated'=>object()))
		// draw a way to add special licenses
		$s_retval = $s_prepend_text.'
			<div>
				Enable/Disable special licenses:
			</div>';
		if ($i_count > 0) {
			$s_retval .= '
				<table class="table_of_distro_special_licenses">
					<tr>
						<td>Notes</td><td>Dataname Prefix</td><td>Original Quantity</td><td>Purchased</td><td>Remaining</td><td>Discount Percentage</td><td>Applicable License Types</td><td>Disabled</td><td>History</td><td>Special Discount Comments</td><td>Remove</td>
					</tr>
				';
			foreach($o_special_licenses as $i_special_license_index=>$o_special_license) {
				$s_disabled_enabled = ($o_special_license->deleted == '0' ? 'disable' : 'enable');
				$s_retval .= '
					<tr>
						<td>'.$o_special_license->notes.'</td><td>'.$o_special_license->prefix.'</td><td>'.$o_special_license->original_quantity.'</td><td>'.($o_special_license->original_quantity-$o_special_license->quantity).'</td><td>'.$o_special_license->quantity.'</td><td>'.$o_special_license->discount_percentage.'</td><td>'.implode(', ',$o_special_license->lic_types).'</td><td>'.($o_special_license->deleted == '0' ? 'No' : 'Yes').'</td>
							<td><a target="_blank" href="'.$_SERVER['REQUEST_URI'].'?distro='.$s_distro.'&view_special_license_history='.$i_special_license_index.'">open</a></td>
							<td>'.(isset($o_special_license->discount_comment)  ? '<a onclick="alert(\''.str_replace(array('"',"'"), '', $o_special_license->discount_comment).'\');">view</a>' : 'n/a').'</td>
						</td><td>
							<form id="remove_distributor_special_licenses_form_'.$i_special_license_index.'" style="color:black;">
								<input type="button" onclick="send_ajax_call_from_form(\'/manage/misc/AdvancedToolsFunctions/manageDistributorLicenses.php\', \'remove_distributor_special_licenses_form_'.$i_special_license_index.'\');" value="'.ucfirst($s_disabled_enabled).'" />
								<input type="hidden" name="command" value="modify_distributor_special_licenses" />
								<input type="hidden" name="subcommand" value="'.$s_disabled_enabled.'" />
								<input type="hidden" name="distributor" value="'.$s_distro.'" />
								<input type="hidden" name="special_license_index" value="'.$i_special_license_index.'" />
								<br />
								<label class="errors" />
							</form>
						</td>
					</tr>';
			}
			$s_retval .= '
				</table>';
		} else {
			$s_retval .= '<font style="color:black;">This distributor has no special licenses.</font>';
		}
		$s_retval .= '
			<div style="margin-top:20px;">
				Assign special licenses:
			</div>
			<form id="add_distributor_special_licenses_form" style="color:black;">
				Notes <input type="text" name="notes" placeholder="Notes" />
				Chain Dataname Prefix <input type="text" name="dataname_prefix" placeholder="Eg. pain_du_monde" />
				Quantity <input type="number" name="quantity" value="0" />
				Discount Percentage <select name="discount_type" onchange="set_special_discount_by_select($(this))"><option>6 - 10 locations (10%)</option><option>11 - 20 locations (15%)</option><option>21 or more locations (20%)</option><option>Custom</option></select><input type="number" name="discount_percentage" value="10" style="display:none;" />
				Applicable License Types <input type="text" value="none" name="special_license_types" /><input type="button" name="add_lic_type" value="+" onclick="add_special_license_type();" /><input type="button" name="sub_lic_type" value="-" onclick="remove_special_license_type();" />
				<input type="button" onclick="var comment = prompt(\'Please enter a reason for the custom amount.\'); if (comment != \'\') { $(\'#add_distributor_special_licenses_form\').find(\'input[name=discount_comment]\').val(comment); send_ajax_call_from_form(\'/manage/misc/AdvancedToolsFunctions/manageDistributorLicenses.php\', \'add_distributor_special_licenses_form\'); }" value="Submit" />
				<input type="hidden" name="special_license_types_allowed" value="'.implode(',',$a_license_types).'" />
				<input type="hidden" name="command" value="add_distributor_special_licenses" />
				<input type="hidden" name="distributor" value="'.$s_distro.'" />
				<input type="hidden" name="discount_comment" value="" />
				<br />
				<label class="errors" />
			</form>
			'.$s_append_text;

		return $s_retval;
	}

	function get_list_of_distributor_licenses_to_remove($s_distro) {
		global $maindb;
		if (!isset($maindb))
			$maindb = "poslavu_MAIN_db";

		$distro_license_query = mlavu_query("SELECT `type`,`purchased`,`notes` FROM `[maindb]`.`reseller_licenses` WHERE `restaurantid`='' AND `applied`='' AND `resellername`='[resellername]'",
			array("maindb"=>$maindb, "resellername"=>$s_distro));
		if (!$distro_license_query)
			return 'fail|bad query made to database';
		if (mysqli_num_rows($distro_license_query) == 0)
			return 'The distributor '.$s_distro.' has no unassigned licenses.';
		$a_distro_licenses = array();
		while($distro_license_read = mysqli_fetch_assoc($distro_license_query))
			$a_distro_licenses[] = $distro_license_read;
		$a_headers = array("Type", "Purchased", "Notes");
		return auto_build_table($a_headers, $a_distro_licenses, "apply_class_to_single_element($(this),'license_selected');remove_distro_license($(this));", "distro_licenses");
	}

	// $a_all_stats is either an array or a string
	// if a string, return the string
	function draw_single_distributor_leads_statistics($a_all_stats) {
		if (is_string($a_all_stats))
			return $a_all_stats;
		$i_count = (int)($a_all_stats['total']);

		// print the results
		$s_retval = '';
		$s_retval .= '
			<table>
				<tr>';
		foreach($a_all_stats as $k=>$v)
			$s_retval .= '
					<th>'.ucwords(implode(' ', explode('_', $k))).'</th>
					<th>%</th>';
		$s_retval .= '
				</tr>
				<tr>';
		foreach($a_all_stats as $k=>$v)
			$s_retval .= '
					<td style="background-color:#bbb">'.$v.'</td>
					<td style="background-color:#ddd">'.(int)($v/$i_count*100).'%</td>';
		$s_retval .= '
				</tr>
			</table>';

		return $s_retval;
	}

	function get_distributor_add_remove_points($s_distro) {
		require_once(dirname(__FILE__).'/../../login.php');
		if (!function_exists('can_access') || !can_access('add_remove_distro_points'))
			return 'print error|You don\'t have the proper access for this action.';

		if (isset($_POST['action']) && $_POST['action'] == 'add_remove_points') {
			$s_distro = $_POST['distributor'];
		}

		// get the distributor
		if (trim($s_distro) == '')
			return 'Bad distributor selected';
		$a_distributors = ConnectionHub::getConn('poslavu')->getDBAPI()->getAllInTable('resellers', array('username'=>$s_distro), TRUE);
		if (count($a_distributors) == 0)
			return 'Bad distributor selected';
		$a_distributor = $a_distributors[0];

		// add/remove distro points
		$s_old_distro_points = '';
		$s_command = '';
		if (isset($_POST['action']) && $_POST['action'] == 'add_remove_points') {
			global $loggedin;
			$s_details = $_POST['details'];
			$i_points = (int)$_POST['points'];
			$s_dataname = $_POST['dataname'];

			$s_old_distro_points = "Old distributor points: ".(int)$a_distributor['credits']."<br />";
			$a_distributor['credits'] += $i_points;

			$a_insert_vars = array('datetime'=>date('Y-m-d'), 'resellerid'=>$a_distributor['id'], 'resellername'=>$s_distro, 'dataname'=>$s_dataname, 'ipaddress'=>$_SERVER['REMOTE_ADDR'], 'action'=>'custom add distro points', 'details'=>$s_details, 'credits_applied'=>$i_points, 'username'=>$loggedin);
			$s_insert_clause = ConnectionHub::getConn('poslavu')->getDBAPI()->arrayToInsertClause($a_insert_vars);
			mlavu_query("INSERT INTO `poslavu_MAIN_db`.`reseller_billing_log` $s_insert_clause", $a_insert_vars);
			mlavu_query("UPDATE `poslavu_MAIN_db`.`resellers` SET `credits`='[credits]' WHERE `id`='[id]'", $a_distributor);

			$s_command = 'set item html[*note*]#distributor_add_remove_points[*findby*]';
		}

		// return the shindig
		return $s_command."
			<script type='text/javascript'>
				function add_remove_points() {
					var details = prompt('Please enter some details describing why the distributor is gaining/losing these points.');
					if (details) {
						$('#add_remove_points_add_form').find('[name=details]').val(details);
						send_ajax_call_from_form('/manage/misc/AdvancedToolsFunctions/manageDistributorLicenses.php', 'add_remove_points_add_form');
					}
				}
			</script>
			Distributor: ".$s_distro."<br />
			".$s_old_distro_points."
			Current distributor points: ".(int)$a_distributor['credits']."<br />
			<form id='add_remove_points_add_form'>
				<input type='hidden' name='distributor' value='".$s_distro."' />
				<input type='hidden' name='command' value='get_distributor_add_remove_points' />
				<input type='hidden' name='action' value='add_remove_points' />
				<input type='hidden' name='details' value='' />
				Add/Remove Points: <input type='textbox' size='30' placeholder='Example: 200 or -200' name='points'> <input type='textbox' size='30' placeholder='restaurant dataname' name='dataname' /> <input type='button' value='Submit' onclick='add_remove_points();' /><br />
				<label class='errors'>&nbsp;</label>
			</form>
		";
	}

	function get_distributor_leads_statistics($s_distro) {
		global $maindb;

		// get all leads that this distributor has and/or declined
		$reseller_leads_query_string = "SELECT * FROM `[maindb]`.`reseller_leads` WHERE `distro_code`='[distro_code]' OR `declined_by`='[distro_code]' OR `declined_by` LIKE '%|[distro_code]' OR `declined_by` LIKE '[distro_code]|%'";
		$reseller_leads_query_vars = array("maindb"=>$maindb, "distro_code"=>$s_distro);
		$reseller_leads_query = mlavu_query($reseller_leads_query_string, $reseller_leads_query_vars); // poslavu_MAIN_db
		if ($reseller_leads_query === FALSE)
			return 'bad query ('.__FILE__.')';
		if (mysqli_num_rows($reseller_leads_query) == 0)
			return 'The distributor '.$s_distro.' has no leads associated with them.';
		$a_leads = array();
		while ($row = mysqli_fetch_assoc($reseller_leads_query))
			$a_leads[] = $row;

		// select only wanted information
		$i_count = count($a_leads);
		$a_count = array('total'=>$i_count);
		$a_date_stats = array('accepted'=>0, 'contacted'=>0, 'made_demo_account'=>0, 'license_applied'=>0);
		$a_distro_stats = array('distro_code'=>0, 'declined_by'=>0);
		foreach($a_leads as $a_lead) {
			foreach($a_date_stats as $k=>$v)
				if ($a_lead[$k] !== '0000-00-00 00:00:00')
					$a_date_stats[$k]++;
			foreach($a_distro_stats as $k=>$v) {
				$a_distros = explode('|', $a_lead[$k]);
				if (in_array($s_distro, $a_distros))
					$a_distro_stats[$k]++;
			}
		}
		$a_all_stats = array_merge($a_count, $a_distro_stats, $a_date_stats);

		// use nice words
		$a_replace_with = array('distro_code'=>'assigned_to_distro', 'declined_by'=>'declined');
		foreach($a_replace_with as $k=>$v) {
			if (isset($a_all_stats[$k])) {
				$a_all_stats[$v] = $a_all_stats[$k];
				unset($a_all_stats[$k]);
			}
		}

		return $a_all_stats;
	}

	function get_list_of_distributor_licenses($s_distro) {
		global $maindb;
		if (!isset($maindb))
			$maindb = "poslavu_MAIN_db";

		$distro_license_query = mlavu_query("select * from `[maindb]`.`reseller_licenses` LEFT JOIN ((SELECT `id` as `rest_id`,`data_name` FROM `[maindb]`.`restaurants`)) `dnr` ON (`reseller_licenses`.`restaurantid` = `rest_id`) WHERE `resellername`='[resellername]' ORDER BY `id` DESC",
			array("maindb"=>$maindb, "resellername"=>$s_distro));
		if (!$distro_license_query)
			return 'fail|bad query made to database';
		if (mysqli_num_rows($distro_license_query) == 0)
			return 'The distributor '.$s_distro.' has no licenses.';
		$a_distro_licenses = array();
		while($distro_license_read = mysqli_fetch_assoc($distro_license_query)) {
			unset($distro_license_read['restaurantid']);
			$a_distro_licenses[] = $distro_license_read;
		}
		$a_headers = array();
		foreach($a_distro_licenses[0] as $k=>$v)
			$a_headers[] = ucwords($k);
		$a_left_align = array('<div style="text-align:left;color:inherit;">', '</div>');
		$a_right_align = array('<div style="text-align:right;color:inherit;">', '</div>');
		foreach($a_distro_licenses as $k=>$a_distro_license) {
			$a_distro_licenses[$k]['notes'] = $a_left_align[0].str_replace('|', '<br />', $a_distro_license['notes']).$a_left_align[1];
			$a_distro_licenses[$k]['rest_id'] = $a_right_align[0].$a_distro_license['rest_id'].$a_right_align[1];
			$a_distro_licenses[$k]['data_name'] = $a_left_align[0].$a_distro_license['data_name'].$a_left_align[1];
		}
		return auto_build_table($a_headers, $a_distro_licenses, "apply_class_to_single_element($(this),'distro_all_licenses_selected');", "all_distro_licenses");
	}

	function remove_license_from_distributor($s_distro, $s_type, $s_purchased, $s_notes, $s_remove_reason, $s_admin_username) {
		global $maindb;
		if (!isset($maindb))
			$maindb = "poslavu_MAIN_db";

		$license_query = mlavu_query("SELECT `id` FROM `[maindb]`.`reseller_licenses` WHERE `resellername`='[resellername]' AND `type`='[type]' AND `purchased`='[purchased]' AND `notes`='[notes]'",
			array("maindb"=>$maindb, "resellername"=>$s_distro, "type"=>$s_type, "purchased"=>$s_purchased, "notes"=>$s_notes));
		if ($license_query === FALSE)
			return 'fail|bad query';
		if (mysqli_num_rows($license_query) == 0)
			return 'fail|Could not find license. Maybe try reloading?';
		$license_read = mysqli_fetch_assoc($license_query);
		$i_license_id = (int)$license_read['id'];
		$s_revoked_message = 'revoked from distributor by '.$s_admin_username.' ('.$s_remove_reason.')';
		if (trim($s_notes) != '')
			$s_notes .= '|'.$s_revoked_message;
		else
			$s_notes = $s_revoked_message;
		mlavu_query("UPDATE `[maindb]`.`reseller_licenses` SET `restaurantid`='-1',`applied`='[date]',`notes`='[notes]' WHERE `id`='[id]' LIMIT 1",
			array("maindb"=>$maindb, "id"=>$i_license_id, "date"=>date("Y-m-d H:m:i"), "notes"=>$s_notes));
		if (ConnectionHub::getConn('poslavu')->affectedRows() == 0)
			return 'fail|Failed to update the database';
		return 'success|Successfully removed license from distributor.';
	}

	// modifies a user's distributor special licensing
	// @return: TRUE, or a string on error
	function modify_distributor_special_licenses($user) {

		global $loggedin;

		// get the post vars
		$a_required = array('subcommand', 'distributor', 'special_license_index');
		foreach($a_required as $s_required)
			if (!isset($_POST[$s_required]))
				return 'print error[*note*]Please set the value '.$s_required.'.';
			else
				$_POST[$s_required] = trim($_POST[$s_required]);
		$s_subcommand = $_POST['subcommand'];
		$s_distro = $_POST['distributor'];
		$s_index = $_POST['special_license_index'];

		// add the types to the distributor
		$a_mods = array();
		$o_special_licenses = $user->get_special_licenses(-9999, FALSE);
		if ($s_subcommand == 'disable') {
			if (!isset($o_special_licenses->$s_index))
				return 'print error[*note*]Special Licenses not found';
			$o_special_licenses->$s_index->deleted = '1';
			$a_mods = array('action'=>'disabled', 'username'=>$loggedin, 'usertype'=>LAVUADMIN);
		} else if ($s_subcommand == 'enable') {
			if (!isset($o_special_licenses->$s_index))
				return 'print error[*note*]Special Licenses not found';
			$o_special_licenses->$s_index->deleted = '0';
			$a_mods = array('action'=>'enabled', 'username'=>$loggedin, 'usertype'=>LAVUADMIN);
		}

		// check for modifications
		if (count($a_mods) > 0) {
			$s_datetime = date('Y-m-d H:i:s');
			$o_special_licenses->$s_index->history->updated->$s_datetime = $a_mods;
			$user->save_special_licenses($o_special_licenses);
		}
		return 'print success[*note*]success';
	}

	// adds special licensing opportunities to a distributor's account
	// @return: TRUE, or a string on error
	function add_distributor_special_licenses($user) {

		global $loggedin;

		// get the post vars
		$a_required = array('notes', 'dataname_prefix', 'quantity', 'discount_percentage', 'special_license_types_allowed', 'special_license_types', 'distributor');
		foreach($a_required as $s_required)
			if (!isset($_POST[$s_required]))
				return 'print error[*note*]Please set the value '.$s_required.'.';
			else
				$_POST[$s_required] = trim($_POST[$s_required]);
		$s_notes = $_POST['notes'];
		$s_dataname_prefix = $_POST['dataname_prefix'];
		$s_quantity = $_POST['quantity'];
		$s_discount_percentage = $_POST['discount_percentage'];
		$s_discount_type = $_POST['discount_type'];
		$s_discount_comment = $_POST['discount_comment'];
		$s_lic_types_allowed = $_POST['special_license_types_allowed'];
		$s_lic_types = $_POST['special_license_types'];
		$s_distro = $_POST['distributor'];

		// check that the fields are not empty
		foreach($a_required as $s_required)
			if ($_POST[$s_required] == '')
				return 'print error[*note*]"'.ucfirst($s_required).'" can\'t be empty.';
		if ((int)$s_quantity == 0)
			return 'print error[*note*]"Quantity" must be greater than 0.';
		if ((int)$s_discount_percentage == 0)
			return 'print error[*note*]"Discount Percentage" must be greater than 0.';
		if ((int)$s_discount_percentage >= 100)
			return 'print error[*note*]"Discount Percentage" must be less than 100.';
		if ($s_discount_type == 'Custom' && $s_discount_comment == '')
			return 'print error[*note*]You must provide a comment if the discount percentage is a custom amount.';

		// get the license types and check that they are valid
		$a_lic_types = explode(',', $s_lic_types);
		$a_lic_types_allowed = explode(',', $s_lic_types_allowed);
		foreach($a_lic_types as $s_lic_type)
			if (!in_array($s_lic_type, $a_lic_types_allowed))
				return 'print error[*note*]The license type "'.$s_lic_type.'" is not recognized.';

		// add the types to the distributor
		$user->create_special_licenses($s_notes, $s_dataname_prefix, (int)$s_quantity, (int)$s_discount_percentage, $s_discount_type, $s_discount_comment, $a_lic_types, $loggedin, LAVUADMIN);
		return 'print success[*note*]success';
	}

	function evaluate_commands() {
		function manage_distro_get_post_var($s_name) {
			return (isset($_POST[$s_name])) ? $_POST[$s_name] : '';
		}

		global $o_distroToolsCustomPurchases;

		$s_command = manage_distro_get_post_var('command');
		$s_distro = manage_distro_get_post_var('distributor');
		$s_license = manage_distro_get_post_var('license');
		$s_admin_username = manage_distro_get_post_var('user');
		$s_type = manage_distro_get_post_var('type');
		$s_purchased = manage_distro_get_post_var('purchased');
		$s_notes = manage_distro_get_post_var('notes');
		$s_remove_reason = manage_distro_get_post_var('remove_reason');
		$s_minimum = manage_distro_get_post_var('min_licenses');
		$s_maximum = manage_distro_get_post_var('max_licenses');

		if ($s_command == 'give_new_license_to_distributor')
			return give_new_license_to_distributor($s_distro, $s_license, $s_admin_username);
		else if ($s_command == 'get_list_of_distributor_licenses_to_remove')
			return get_list_of_distributor_licenses_to_remove($s_distro);
		else if ($s_command == 'get_list_of_distributor_licenses')
			return get_list_of_distributor_licenses($s_distro);
		else if ($s_command == 'get_distributor_leads_statistics')
			return draw_single_distributor_leads_statistics(get_distributor_leads_statistics($s_distro));
		else if ($s_command == 'remove_license_from_distributor')
			return remove_license_from_distributor($s_distro, $s_type, $s_purchased, $s_notes, $s_remove_reason, $s_admin_username);
		else if ($s_command == 'drawLicenseQuantityManager_drawDistros')
			return drawLicenseQuantityManager_drawDistros($s_minimum, $s_maximum);
		else if ($s_command == 'drawLicenseQuantityManager_drawLicenses')
			return drawLicenseQuantityManager_drawLicenses($s_distro);
		else if ($s_command == 'get_distributor_add_remove_points')
			return get_distributor_add_remove_points($s_distro);
		else if ($s_command == 'get_distributor_special_licenses' || $s_command == 'add_distributor_special_licenses' || $s_command == 'modify_distributor_special_licenses')
			return get_distributor_special_licenses($s_distro);
		else if ($s_command == 'manage_distributor_devices')
			return manage_distributor_devices($s_distro);
		else if ($s_command == 'manage_distributor_commission')
			return manage_distributor_commission($s_distro);
		else if ($s_command == 'manage_distributor_custom_purchases')
			return $o_distroToolsCustomPurchases->manage($s_distro);
		else if ($s_command == 'view_distributor_created_accounts')
			return view_distributor_created_accounts($s_distro);
		else if ($s_command == 'manage_distributor_custom_purchase_ajax')
			return $o_distroToolsCustomPurchases->manage_ajax($s_distro);
		return 'fail|bad command';
	}

	if (isset($_POST['command'])) {
		echo evaluate_commands();
	}

	if (isset($_GET['view_special_license_history']) && isset($_GET['distro'])) {
		echo drawDistroSpecialLicenseHistory($_GET['distro'], $_GET['view_special_license_history']);
	}

?>
