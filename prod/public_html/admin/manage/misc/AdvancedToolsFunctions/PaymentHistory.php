<?php
    require_once($_SERVER['DOCUMENT_ROOT']."/manage/error_config.php");
	require_once($_SERVER['DOCUMENT_ROOT'].'/cp/resources/core_functions.php');
	$sum_capture = 0;
	$sum_refund = 0;
	$sum_declined = 0;
	$daily_totals = array();
	
	$mindate = "3000-01-01";
	$maxdate = "1000-01-01";
	
	function parse_payment_response($response)
	{
		$vars = array();
		$response = explode("\n",$response);
		for($i=0; $i<count($response); $i++)
		{
			$line = explode("=",$response[$i]);
			if(count($line) > 1)
			{
				$vars[trim($line[0])] = trim($line[1]);
			}
		}
		return $vars;
	}
	

	function add_daily_total($daily_totals,$day,$amount)
	{
		if(isset($daily_totals[$day]))
			$daily_totals[$day] += ($amount * 1);
		else
			$daily_totals[$day] = 0 + ($amount * 1);
		return $daily_totals;
	}
	
	if(isset($_GET['wayback']))
		$min_date = date("Y-m-d",mktime(0,0,0,date("m"),date("d") - 360,date("Y")));
	else
		$min_date = date("Y-m-d",mktime(0,0,0,date("m"),date("d") - 60,date("Y")));
	
	$pr_query = mlavu_query("select * from `poslavu_MAIN_db`.`payment_responses` where `date`>='[1]' order by `date` desc, `time` desc",$min_date);
		echo "<style>";
	echo ".style_capture { ";
	echo "  color: #ffffff; ";
	echo "} ";
	echo ".style_general { ";
	echo "  color: #000000; ";
	echo "} ";
	echo "</style>";
	echo "<table cellpadding=3>";
	while($pr_read = mysqli_fetch_assoc($pr_query))
	{
		if($mindate > $pr_read['date']) $mindate = $pr_read['date'];
		if($maxdate < $pr_read['date']) $maxdate = $pr_read['date'];
		
		$dt = $pr_read['date'];
		if(isset($daily_totals[$dt]))
		{
			$daily_totals[$dt] += $day_total;
		}
		
		//echo "$mindate > " . $pr_read['date'] . "<br>";
		
		$pay_info = parse_payment_response($pr_read['response']);
		if($pay_info['x_type']=="auth_capture") 
		{
			if($pay_info['x_response_code']=="1")
			{
				$sum_capture += ($pay_info['x_amount'] * 1);
				$daily_totals = add_daily_total($daily_totals,$dt,($pay_info['x_amount'] * 1));
				$bgcolor = "#338833";
				
				if(isset($_GET['t4']))
				{
					if(isset($pay_info['x_subscription_id']) && $pay_info['x_subscription_id']!="")
					{
						$signup_query = mlavu_query("select * from `poslavu_MAIN_db`.`signups` where `arb_license_id`='[1]'",$pay_info['x_subscription_id']);
						if(mysqli_num_rows($signup_query))
						{
							$signup_read = mysqli_fetch_assoc($signup_query);
							//echo "<tr><td colspan='4' bgcolor='#dddddd'>signup: " . $signup_read['company'] . "</td></tr>";
							$rest_query = mlavu_query("select * from `poslavu_MAIN_db`.`restaurants` where `data_name`='[1]'",$signup_read['dataname']);
							if(mysqli_num_rows($rest_query))
							{
								$rest_read = mysqli_fetch_assoc($rest_query);
								//echo "<tr><td colspan='4' bgcolor='#dddddd'>restaurant: " . $rest_read['company_name'] . "</td></tr>";
								$rest_id = $rest_read['id'];
								if($rest_read['license_status']=="")
								{
									$refund_query = mlavu_query("select * from `poslavu_MAIN_db`.`payment_responses` where `response` LIKE '%[1]%' and `response` LIKE '%x_type = credit%'",$pay_info['x_account_number']);
									if(mysqli_num_rows($refund_query))
									{
										//echo "payment found with refund - " . $pay_info['x_first_name'] . "<br>";
									}
									else
									{
										mlavu_query("update `poslavu_MAIN_db`.`restaurants` set `license_status`='Paid' where `id`='[1]'",$rest_id);
										echo "payment updated - " . $rest_read['company_name'] . "<br>";
									}
								}
								if($rest_read['package_status']=="")
								{
									$set_status = "";
									if($pay_info['x_amount']=="895.00")
										$set_status = "Silver";
									else if($pay_info['x_amount']=="1495.00")
										$set_status = "Gold";
									else if($pay_info['x_amount']=="3495.00")
										$set_status = "Platinum";
									mlavu_query("update `poslavu_MAIN_db`.`restaurants` set `package_status`='[1]' where `id`='[2]'",$set_status,$rest_id);
									echo "status updated - " . $rest_read['company_name'] . " - " . $set_status . "<br>";
								}
							}
						}
					}
				}
			}
			else
			{
				$sum_decline += ($pay_info['x_amount'] * 1);
				$bgcolor = "#883333";
			}
			$class = "style_capture";
		}
		else if($pay_info['x_type']=="credit")
		{
			$sum_refund += ($pay_info['x_amount'] * 1);
			$daily_totals = add_daily_total($daily_totals,$dt,($pay_info['x_amount'] * -1));
			$bgcolor = "#333388";
			$class = "style_capture";
		} 
		else 
		{
			$bgcolor = "#ffffff";
			$class = "style_general";
		}
		echo "<tr bgcolor='$bgcolor'>";
		echo "<td>";
		
		$mrestid = $pr_read['match_restaurantid'];
		if(is_numeric($mrestid))
		{
			$result=mysqli_fetch_assoc(mlavu_query("select `data_name` from `poslavu_MAIN_db`.`restaurants` where `id`= '[1]'", $mrestid));
			
			echo "<a href='http://admin.poslavu.com/manage/index.php?goToAccount=".$result['data_name']."' target='_blank' style='color:#aabbff'><b>(manage account)</b></a>";
		}
		else if($mrestid!="")
		{
			echo "<font color='#ffffff'>" . $mrestid . "</font>";
		}
		else echo "&nbsp;";
		
		echo "</td>";
		echo "<td class='$class'>" . $pr_read['date'] . "</td>";
		echo "<td class='$class'>" . $pr_read['time'] . "</td>";
		echo "<td class='$class'>" . $pay_info['x_company'] . "</td>";
		echo "<td class='$class'>" . trim($pay_info['x_first_name'] . " " . $pay_info['x_last_name']) . "</td>";
		echo "<td class='$class'>";
		if($pay_info['x_type']=="auth_capture" && $pay_info['x_response_code']=="1")
			echo "<b>" . $pay_info['x_amount'] . "</b>";
		else
			echo $pay_info['x_amount'];
		echo "</td>";
		//echo "<td>" . $pay_info['x_response_code'] . "</td>";
		echo "<td class='$class'>" . $pay_info['x_response_reason_text'] . "</td>";
		echo "<td class='$class'>" . $pay_info['x_type'] . "</td>";
		echo "<td class='$class'><a style='cursor:pointer' onclick='if(document.getElementById(\"details_".$pr_read['id']."\").style.display == \"table-row\") document.getElementById(\"details_".$pr_read['id']."\").style.display = \"none\"; else document.getElementById(\"details_".$pr_read['id']."\").style.display = \"table-row\"'>(details)</a></td>";
		echo "</tr>";
		echo "<tr>";
		echo "<td colspan='20' id='details_".$pr_read['id']."' style='display:none'>";
		echo trim(str_replace("x_","<br>x_",$pr_read['response']),"<br>");
		echo "</td>";
		echo "</tr>";
	}
	echo "</table>";
	
	$min_date_parts = explode("-",$mindate);
	$min_date_ts = mktime(0,0,0,$min_date_parts[1],$min_date_parts[2],$min_date_parts[0]);
	$max_date_parts = explode("-",$maxdate);
	$max_date_ts = mktime(0,0,0,$max_date_parts[1],$max_date_parts[2],$max_date_parts[0]);
	$date_diff = ($max_date_ts - $min_date_ts) / 24 / 60 / 60;
	
	if(isset($_GET['sum']))
	{
		echo "<br><br>Days: $date_diff ($mindate - $maxdate)";
		$sum_total = $sum_capture - $sum_refund;
		echo "<br>Total Capture: ".number_format($sum_capture,2,".",",");
		echo "<br>Total Refunds: ".number_format($sum_refund,2,".",",");
		echo "<br>total Declined: ".number_format($sum_decline,2,".",",");
		echo "<br>Sum (Capture - Refunds): ".number_format($sum_total,2,".",",");
		echo "<br>Sum Per Day: " . number_format($sum_total / $date_diff,2,".",",");
		
		echo "<br>";
		echo "<table>";
		$check_date = $maxdate;
		$last_check_date = $check_date;
		$week_counter = 0;
		$week_total = 0;
		$all_weeks = 0;
		$week_end = "";
		while($check_date >= $mindate && $mindate > "2000-01-01")
		{
			$last_check_date = $check_date;
			if($week_counter==0) $week_end = $check_date;
			$week_counter++;
			
			if(isset($daily_totals[$check_date]))
				$week_total += $daily_totals[$check_date] * 1;
			
			if($week_counter > 6) 
			{
				echo "<tr><td>" . $last_check_date . " to " . $week_end . ": </td><td align='right'>$" . number_format($week_total,2,".",",") . "</td><td align='right'>$" . number_format($week_total / $week_counter,2,".",",") . "</td></tr>";
				$all_weeks += $week_total * 1;
				$week_counter = 0;
				$week_total = 0;
			}
			$dparts = explode("-",$check_date);
			$check_date_ts = mktime(0,0,0,$dparts[1],$dparts[2]-1,$dparts[0]);
			$check_date = date("Y-m-d",$check_date_ts);
		}
		if($week_total != 0) 
		{
			echo "<tr><td>" . $last_check_date . " to " . $week_end . ": </td><td align='right'>$" . number_format($week_total,2,".",",") . "</td><td align='right'>$" . number_format($week_total / $week_counter,2,".",",") . "</td></tr>";
			$all_weeks += $week_total * 1;
		}
		echo "</table>";
		//echo "<br>All Weeks: " . number_format($all_weeks,2,".",",");
		/*foreach($daily_totals as $key => $val)
		{
			echo "<br>" . $key . ": " . $val;
		}*/
	}
	
?>