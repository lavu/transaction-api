<script type="text/javascript" src="misc/AdvancedToolsFunctions/moduleEditing/module_editing.js"></script>
<style type="text/css">
	tr[selected]{
		background-color: green;
		color: white;
	}

	tr{
		cursor: pointer;
	}
</style>
<?php
require_once($_SERVER['DOCUMENT_ROOT']."/manage/error_config.php");
require_once $_SERVER['DOCUMENT_ROOT'] . '/sa_cp/billing/package_levels_object.php';
if (!isset($o_package_container))
	$o_package_container = new package_container();

class ModuleEditor
{
	private
	$a_availableModules,
	$a_remainingModules,
	$a_assignedModules,
	$modulePackageName;

	public function __construct($modulePackageName, $a_availableModules, $a_assignedModules){
		$this->a_availableModules = $a_availableModules;
		$this->a_assignedModules = $a_assignedModules;
		$this->a_remainingModules = $a_availableModules;
		$this->modulePackageName = $modulePackageName;

		foreach($a_assignedModules as $module => $mod){
			unset($this->a_remainingModules[$module]);
		}
	}

	public function __tostring(){

	}

	public function toHTML(){
		$str = "";
		$str .= "<h3>" . $this->modulePackageName . "</h3>";
		$str .= "
				<div style=\"position: relative;\">
					<div style=\"display: inline-table; position: relative; text-align: left;\">
						<h4>Remaining Modules</h4>
						[REMAINING MODULES]
					</div>
					<div style=\"display: inline-table; position: relative;\">
						<br />
						<br />
						<button>>+</button><br />
						<button>>-</button><br />
						<button>&lt;&lt;</button><br />
					</div>
					<div style=\"display: inline-table; position: relative; text-align: left;\">
						<h4>Assigned Modules</h4>
						[ASSIGNED MODULES]
					</div>
				</div>
				";
		$module_html = array();
		$module_html['ASSIGNED MODULES'] = "";
		$module_html['REMAINING MODULES'] = "";
		foreach($this->a_assignedModules as $module){
			$module_html['ASSIGNED MODULES'] .= "<div>$module</div>";
		}
		foreach($this->a_remainingModules as $module){
			$module_html['REMAINING MODULES'] .= "<div>$module</div>";
		}

		foreach($module_html as $tag => $html){
			$str = str_ireplace('[' . $tag . ']', $html, $str);
		}

		return $str;
	}
}

$extra_modules = array(
	'Available',
	'Core',
	'Dev',
	'Lavu Local Server'
);

$edit_module_list = array();
$packages = $o_package_container->get_baselevel_packages(1,13);

foreach($packages as $package){
	$module_name = $package->get_plain_name() . ' Modules';
	$edit_module_list[$module_name] = $module_name;
}

foreach($extra_modules as $modname){
	$module_name = $modname . ' Modules';
	$edit_module_list[$module_name] = $module_name;
}
unset($edit_module_list['Lite Modules']);

$options = "";
sort($edit_module_list);
foreach($edit_module_list as $module){
	$options .= "<option value=\"$module\">$module</option>\n";
}

echo "<div style=\"width: 100%; height: 100%; overflow: auto; text-align: center;\">";
echo "Select which Modules file to Edit:
	<select id=\"modulefile\" onchange=\"module_editor.save(); loadModuleEditor('module_area');\">
		<option value=\"\">Main</option>
		<option selected value=\"dev\">Dev</option>
	</select><br />
	Select which Modules to Edit: <select id=\"module_package\" onchange=\"module_editor.save(); loadModuleEditor('module_area');\">
		$options
	</select><br />";
echo "<button onclick=\"copyModulesOver();\">Copy to Live</button><br />";
echo "<button onclick=\"module_editor.save()\">Save</button><br />";
$str = file_get_contents( $_SERVER['DOCUMENT_ROOT'] . '/dev/lib/modules/modules_list.txt' );
$str = str_ireplace("\n", "", $str);
echo "
	<div id=\"module_area\" style=\"width: 100%;\"></div>
	";

//echo $str;
$obj = $json->decode($str);

$a_modules = array();
$packages = $o_package_container->get_baselevel_packages(1,13);

foreach($packages as $k=>$package){
	$module_name = $package->get_plain_name() . ' Modules';
	if(isset($obj->$module_name))
		$a_modules[$module_name] = $obj->$module_name;
	else
		unset($packages[$k]);
}

foreach($extra_modules as $modname){
	$module_name = $modname . ' Modules';
	$a_modules[$module_name] = $obj->$module_name;
}
unset($a_modules['Lite Modules']);

$editors = array();
foreach($a_modules as $module => $mod){
	if($module == 'Available Modules')
		continue;
	$editors[] = new ModuleEditor($module, $a_modules['Available Modules'], $mod);
}

//foreach($editors as $editor){
//	echo ($editor->toHTML());
//	echo "<br />";
//}

echo "</div>";

?>
<script type=\"text/javascript\">
	loadModuleEditor("module_area");
	//if(!(window.mozInnerScreenX == null)) //basically, if is firefox
	//	window.history.replaceState();
	//doneLoading();
</script>