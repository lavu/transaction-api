<?php
	
	class serverStatus {
		
		// see the help text (bellow) for a description
		public static function drawOutdatedLocationConfigSettings() {
			// about this function
			$s_retval = '<b>About:</b><br /><br />
				New location settings are stored in the config table of restaurants instead of being added as a column to `location_settings`.<br ><br />
				When a new feature comes out, a new row is added to the config table for default restaurants but sometimes this row is forgotten about. Well...<br /><br />
				THIS is to help us remember what hasn\'t been added by comparing the dev config table to the other default restaurant tables and printing out those location_config_setting settings that are missing.<br /><br />';
			
			// get the databases to compare
			$a_datanames = array('source'=>'DEV', 'dest1'=>'default_coffee', 'dest2'=>'default_other_', 'dest3'=>'default_pizza_', 'dest4'=>'default_restau', 'dest5'=>'default_retail', 'dest6'=>'default_tab_la');
			
			// get the setting names for all 'location_config_setting' for the two databases
			$a_configs = array();
			foreach($a_datanames as $s_dataname) {
				$s_database = 'poslavu_'.$s_dataname.'_db';
				$a_config_settings = ConnectionHub::getConn('poslavu')->getDBAPI()->getAllInTable('config', array('type'=>'location_config_setting'), TRUE,
					array('databasename'=>$s_database, 'selectclause'=>'setting', 'orderbyclause'=>'ORDER BY `setting`'));
				$a_location_settings = array();
				foreach($a_config_settings as $a_config_setting)
					if (!in_array($a_config_setting['setting'], $a_location_settings))
						$a_location_settings[] = $a_config_setting['setting'];
				$a_configs[] = $a_location_settings;
			}
			
			// compare the setting names for all 'location_config_setting' config settings
			$a_shared = array();
			foreach($a_configs as $a_settings) {
				foreach($a_settings as $s_setting) {
					if (!isset($a_shared[$s_setting]))
						$a_shared[$s_setting] = 1;
					else
						$a_shared[$s_setting]++;
				}
			}
			
			$a_out_of_date = array();
			foreach($a_shared as $s_setting=>$i_count) {
				if ($i_count < count($a_datanames))
					$a_out_of_date[] = $s_setting;
			}
			
			// print the settings
			$s_retval .= '<b>There are '.count($a_out_of_date).' settings out of date:</b><br /><br />';
			if (count($a_out_of_date) > 0) {
				foreach($a_out_of_date as $s_setting)
					$s_retval .= $s_setting.'<br />';
			}
			
			return $s_retval;
		}
		
	}
	
?>