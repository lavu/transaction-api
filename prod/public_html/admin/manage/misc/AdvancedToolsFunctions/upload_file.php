<?php
	$extension = end(explode(".", basename($_FILES["file"]["name"])));
	if ((($_FILES["file"]["type"] == "application/pdf"))){
		if ($_FILES["file"]["error"] > 0){
			echo "Return Code: " . $_FILES["file"]["error"] . "<br>";
		}else{
			$basename = basename($_FILES["file"]["name"]);
			if (file_exists("upload/" . $basename)){
				echo $basename . " already exists. ";
			}else{
				$val=move_uploaded_file($_FILES["file"]["tmp_name"],$_SERVER['DOCUMENT_ROOT']."/manage/misc/companyDocs/".strtotime(date("Y-m-d H:i:s"))."___".$_POST['fileType']."___".$basename );
				if($val){
					echo "<script type='text/javascript'>history.back();return false;</script>";
				}else{
					echo "Failed move uploaded file. tmp name is: ". $_FILES["file"]["tmp_name"]. " File name is: ". $basename;
				}
			}
		}
	}else{
		echo "Invalid file. Type is: ".$_FILES["file"]["type"];
	}
?>