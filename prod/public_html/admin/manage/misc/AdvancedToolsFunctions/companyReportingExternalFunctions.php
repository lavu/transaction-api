<?php
	
	global $o_statsReportingExternal;
	$o_statsReportingExternal = new statsReportingExternal;
	
	class statsReportingExternal {
		function getListOfUpgradedAccounts(&$a_upgrades = NULL) {
			// to be used throughout this function
			$a_upgrades = array(
				'distro'=>array('display_name'=>'Distributor Upgrades', 'upgrades'=>NULL),
				'user'=>array('display_name'=>'User Upgrades', 'upgrades'=>NULL),
				'lavu'=>array('display_name'=>'Lavu Admin Upgrades', 'upgrades'=>NULL),
			);
			
			// get a list of distributor and user upgrades
			$a_upgrades['distro']['upgrades'] = ConnectionHub::getConn('poslavu')->getDBAPI()->getAllInTable('billing_log', NULL, TRUE,
				array('selectclause'=>"`dataname`,`restaurantid`,TRIM(LEADING 'upgraded from ' FROM SUBSTRING_INDEX(`details`,' to ',1)) AS `from`, TRIM(LEADING ' to ' FROM TRIM(LEADING SUBSTRING_INDEX(`details`,' to ',1) FROM `details`)) AS `to`", 'whereclause'=>"WHERE `username` LIKE 'distro:%' AND `action` LIKE 'package was changed to %' AND `details` LIKE 'upgraded from % to %'", 'groupbyclause'=>'GROUP BY `dataname`,`from`,`to`'));
			$a_upgrades['user']['upgrades'] = ConnectionHub::getConn('poslavu')->getDBAPI()->getAllInTable('billing_log', NULL, TRUE,
				array('selectclause'=>"`id`,`details`,`dataname`,`restaurantid`,TRIM(LEADING 'upgraded from ' FROM SUBSTRING_INDEX(`details`,' to ',1)) AS `from`, SUBSTRING_INDEX(TRIM(LEADING ' to ' FROM TRIM(LEADING SUBSTRING_INDEX(`details`,' to ',1) FROM `details`)),',',1) AS `to`, SUBSTRING_INDEX(SUBSTRING_INDEX(`details`,',',2),' distro commission ',-1) AS `commission`, SUBSTRING_INDEX(SUBSTRING_INDEX(`details`,',',3),' distributor ',-1) AS `distro_code`", 'whereclause'=>"WHERE `username` LIKE 'restaurant:%' AND `action` LIKE 'package was changed to %' AND `details` LIKE 'upgraded from % to %'", 'groupbyclause'=>'GROUP BY `dataname`,`from`,`to`'));
			
			// get a list of accounts upgraded by a lavu admin
			$a_distro_user_datanames = array();
			foreach($a_upgrades as $s_name=>$a_upgrade_cat) {
				if ($s_name != 'distro' && $s_name != 'user')
					continue;
				foreach($a_upgrade_cat['upgrades'] as $a_upgrade) {
					$a_distro_user_datanames[$a_upgrade['dataname']] = $a_upgrade['dataname'];
				}
			}
			$s_inclause = ConnectionHub::getConn('poslavu')->getDBAPI()->arrayToInClause($a_distro_user_datanames);
			$a_upgrades['lavu']['upgrades'] = ConnectionHub::getConn('poslavu')->getDBAPI()->getAllInTable('billing_log', NULL, TRUE,
				array('selectclause'=>'`dataname`,`restaurantid`', 'whereclause'=>"WHERE (`action` LIKE '%upgrad%' OR `details` LIKE '%upgrad%') AND `dataname` NOT ".$s_inclause, 'groupbyclause'=>'GROUP BY `dataname`,`details`'));
		}
	}

?>