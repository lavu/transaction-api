<?php
	$dataname = $_REQUEST['dn'];
	$conversionType = $_REQUEST['ct'];
	$locationid = $_REQUEST['locid'];
	if( empty($dataname) || empty($conversionType) || empty($locationid) ){
		echo 'Missing dataname, conversion type, or location_id (dn, ct, locid)';
		exit;
	}

	require_once('/home/poslavu/public_html/admin/cp/resources/core_functions.php');
	require_once('/home/poslavu/public_html/admin/manage/misc/LLSFunctions/schedule_msync.php');

	lavu_connect_dn($dataname,'poslavu_'.$dataname.'_db');
	echo 'Performing Conversion-'.$conversionType.'<br>';

	convertBetweenPizzaAndBuildYourOwn($dataname, $locationid, $conversionType);

	function convertBetweenPizzaAndBuildYourOwn($dataname, $locid, $type){
		global $dataname;
		if($type == 'to_pizza'){
			echo updateOrInsertConfigField('build_your_own_background_image_empty',   '') . '<br>';
			echo updateOrInsertConfigField('build_your_own_disable_default_heavyness','') . '<br>';
			echo updateOrInsertConfigField('build_your_own_disable_portions',         '') . '<br>';
		}
		else if($type == 'to_general'){
			echo updateOrInsertConfigField('build_your_own_background_image_empty',   'true') . '<br>';
			echo updateOrInsertConfigField('build_your_own_disable_default_heavyness','true') . '<br>';
			echo updateOrInsertConfigField('build_your_own_disable_portions',         'true') . '<br>';
		}
		//public static function syncTableDown($s_dataname, $locationid, $table_name)
		LLSFunctionsScheduleMsync::syncTableDown($dataname, $locid, 'config');
	}

	function updateOrInsertConfigField($setting, $value){
		$conf_row = getConfigField($setting);
		$result;
		$insertOrUpdateStr = '';
		if($conf_row === false){
			$insertOrUpdateStr = "insert";
			$result = lavu_query("INSERT INTO `config` (`setting`, `value`) VALUES ('[1]','[2]')", $setting, $value);
		}else{
			$insertOrUpdateStr = "update";
			$result = lavu_query("UPDATE `config` SET `value`='[1]' WHERE `setting`='[2]'", $value, $setting);
		}
		if(!$result) {
			error_log("mysql error in ".__FILE__." -aefeisj- lavu_dberror:" . lavu_dberror());
			return false;
		}
		else{
			return $insertOrUpdateStr;
		}
	}

	function getConfigField($setting){
		$query = "SELECT * FROM `config` WHERE `setting`='[1]'";
		$result = lavu_query($query, $setting);
		if(!$result) { error_log("mysql error in ".__FILE__." -ae3w8wh4- error:" . lavu_dberror() ); }
		if(mysqli_num_rows($result) == 0) { return false; }
		else{
			return mysqli_fetch_assoc($result);
		}
	}
?>
