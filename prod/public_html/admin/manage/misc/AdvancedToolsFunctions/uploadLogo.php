<?php
	$error = "";
	$msg = "";
	if(!empty($_FILES[$fileElementName]['error'])){
		switch($_FILES[$fileElementName]['error']){

			case '1':
				$error = 'The uploaded file exceeds the upload_max_filesize directive in php.ini';
				break;
			case '2':
				$error = 'The uploaded file exceeds the MAX_FILE_SIZE directive that was specified in the HTML form';
				break;
			case '3':
				$error = 'The uploaded file was only partially uploaded';
				break;
			case '4':
				$error = 'No file was uploaded.';
				break;
			case '6':
				$error = 'Missing a temporary folder';
				break;
			case '7':
				$error = 'Failed to write file to disk';
				break;
			case '8':
				$error = 'File upload stopped by extension';
				break;
			case '999':
			default:
				$error = 'No error code avaiable';
		}
	}elseif(empty($_FILES['fileToUpload']['tmp_name']) || $_FILES['fileToUpload']['tmp_name'] == 'none'){
		$error = 'No file was uploaded..';
	}else {
		session_start();
		require_once($_SERVER['DOCUMENT_ROOT']."/cp/resources/core_functions.php");
		require_once("/home/poslavu/private_html/rs_upload.php");
		$dirToMove = $_SERVER['DOCUMENT_ROOT'].'/manage/misc/AdvancedToolsFunctions/extensionLogos/';
		$picName = $_FILES['fileToUpload']['name'];	
		
		$picNameParts = explode(".",$picName);
		if(count($picNameParts) > 1){  
			
			$baseNameExt = strtolower($picNameParts[count($picNameParts)-1]);
			
			$allowedExt = array("gif","jpg","jpe","jpeg","png");

			$isAllowed = false;
			for($x=0; $x<count($allowedExt); $x++){
				if($allowedExt[$x]==$baseNameExt)
					$isAllowed = true;
			}
			
			
			if($isAllowed){
				if(file_exists($dirToMove.$picName)) 
				$picName = $picNameParts[0].date("YmdHis").$picNameParts[1];
				
				$uploadFile = $dirToMove . $picName;				
				$fileMoved = rs_move_uploaded_file($_FILES['fileToUpload']['tmp_name'], $uploadFile);
				
				if(isset($_REQUEST['receiptLogo']) && $_REQUEST['receiptLogo'] && $_REQUEST['receiptLogo']!='false'){
				
					rs_convert_receipt_logo($uploadFile);				
				}
				mlavu_query("UPDATE `poslavu_MAIN_db`.`extensions` SET `logo`='[1]' WHERE `id`='[2]' LIMIT 1", $picName,$_REQUEST['id']);
				if($fileMoved){
					 $msg .= $picName;
				}
			
			}
		}
	}	
		
	echo '{"error":"'.$error. '", "msg": "'.$msg.'", "dbName":"'.$dbName.'","isReceiptLogo":"'.$_REQUEST['receiptLogo'].'"}';
?>