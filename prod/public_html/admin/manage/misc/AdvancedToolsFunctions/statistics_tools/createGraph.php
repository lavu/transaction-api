<?php
	require_once(dirname(__FILE__)."/../../../../cp/resources/json.php");

	class statisticsGraph {

		function __construct() {
			$this->graphCount = $this->sessvar('statisticsGraph_graphCount', 0);
		}
		private function sessvar($var, $default, $a_array = NULL) {
			if ($a_array === NULL) {
				if (isset($_SESSION[$var])) return $_SESSION[$var];
			} else {
				if (isset($a_array[$var])) return $a_array[$var];
			}
			return $default;
		}

		/**
		 * creates the code necessary for generating graphs
		 * @param  string $s_json_data  the data to be displayed in the graph, as an array of arrays of values
		 *     eg: [{'name1':value,'name2':value},{'name1':value,'name2':value},...]
		 * @param  array  $a_other_vars other parameters that can be passed, including
		 *     'defaultx': the initial value to use for the x-axis, defaults to the first value
		 *     'defaulty': the initial value to use for the y-axis, defaults to the first value
		 *     'chooseSplitGraph': allows the user to choose how to split the graph, default TRUE
		 *     'splitGraphOn': choose which parameter to split the data into multiple graph, default none (empty string)
		 *     'keyDisplayNames': a mapping of key names to display names
		 * @return array                the strings used in the html, including:
		 *     'open_button': the html to view the graph
		 *     'graph_code': the code of the graph, including all the javascript
		 */
		public function getGraphCode($s_json_data, $a_other_vars = NULL) {

			//get some values
			$i_graph = $this->getNextGraphCount();
			$s_graph_id = "statistics_graph_{$i_graph}";
			$s_xaxis = self::sessvar("defaultx", "", $a_other_vars);
			$s_yaxis = self::sessvar("defaulty", "", $a_other_vars);
			$b_chooseSplitGraph = (self::sessvar("chooseSplitGraph", "TRUE", $a_other_vars) == "TRUE") ? TRUE : FALSE;
			$s_splitGraphOn = self::sessvar("splitGraphOn", "", $a_other_vars);
			$s_keyDisplayNames = LavuJson::json_encode(self::sessvar("keyDisplayNames", array(), $a_other_vars));

			// generate the open button code
			$s_open_button_code = "<input type='button' onclick='StatGraphs.openGraph(\"{$s_graph_id}\");' value='View Graph' />";

			// generate the javascript
			ob_start();
			?>
			<script language="javascript" type="text/javascript" src="/newcp/backend/js/jquery/flot/jquery.flot.js"></script>
			<script language="javascript" type="text/javascript" src="/newcp/backend/js/jquery/flot/jquery.flot.pie.js"></script>
			<script type="text/javascript">
				if (typeof(StatGraphs) == 'undefined' || true) {
					StatGraphs = {
						graphData: {},
						getvals: {},
						interpretDataRanking: {},
						interpretDataReverseRanking: {},
						previousPoint: {},
						previousLabel: {},

						/*******************************************************
						 * Open and close a graph
						 ******************************************************/
						openGraph: function(graph_id) {

							// initiate some of the data
							var data = this.graphData[graph_id].data;
							var meta = this.graphData[graph_id].meta;
							var keys = []; for(var key in data[0]) { keys.push(key); }
							if (meta.xaxis == '') meta.xaxis = keys[0];
							if (meta.yaxis == '') meta.yaxis = keys[0];
							this.previousPoint[graph_id] = null;
							this.previousLabel[graph_id] = null;

							// open and draw the graph
							var jcontainer = this.getContainer(graph_id);
							var jblockScreen = jcontainer.siblings('.blockScreen');
							this.drawTools(graph_id);
							jcontainer.show();
							jblockScreen.show();
							this.plotGraph(graph_id);
						},
						closeGraph: function(graph_id) {
							var jcontainer = this.getContainer(graph_id);
							var jblockScreen = jcontainer.siblings('.blockScreen');
							jcontainer.hide();
							jblockScreen.hide();
						},

						/*******************************************************
						 * Data shortcut functions
						 ******************************************************/
						getKeyDisplayNames: function(graph_id, keys) {
							var meta = this.graphData[graph_id].meta;
							var retval = {};
							for(var i = 0; i < keys.length; i++) {
								var key = keys[i];
								if (meta.keyDisplayNames[key]) {
									retval[key] = meta.keyDisplayNames[key];
								} else {
									retval[key] = key;
								}
							}
							return retval;
						},
						setAxis: function(axis, graph_id) {

							// set the axis
							var meta = this.graphData[graph_id].meta;
							var axisDiv = this.getAxisDiv(graph_id);
							var jselect = axisDiv.find('select.'+axis);
							var axisVal = jselect.val();
							meta[axis] = axisVal;

							// draw the graph
							this.plotGraph(graph_id);
						},
						setDrawAverage: function(graph_id) {

							// set the draw average boolean
							var meta = this.graphData[graph_id].meta;
							var axisDiv = this.getAxisDiv(graph_id);
							var javerage = axisDiv.find('input.average');
							meta.drawAverage = javerage[0].checked;

							// draw the graph
							this.plotGraph(graph_id);
						},

						/*******************************************************
						 * Draw the tools/Plot a graph
						 ******************************************************/
						drawTools: function(graph_id) {

							// get some data on the graph
							var meta = this.graphData[graph_id].meta;
							var data = this.graphData[graph_id].data;
							var keys = []; for(var key in data[0]) { keys.push(key); }
							var keyDisplayNames = this.getKeyDisplayNames(graph_id, keys);
							var axisDiv = this.getAxisDiv(graph_id);
							var chooseSplitGraph = meta.chooseSplitGraph;

							// get the options
							var splitGraphOnOptions = "<option></option>";
							var xoptions = "";
							var yoptions = "";
							for(var i = 0; i < keys.length; i++) {
								var key = keys[i];
								var splitselected = (key == meta.splitGraphOn) ? "SELECTED" : "";
								var xselected = (key == meta.xaxis) ? "SELECTED" : "";
								var yselected = (key == meta.yaxis) ? "SELECTED" : "";
								splitGraphOnOptions += '<option value="'+key+'" '+splitselected+'>'+keyDisplayNames[key]+'</option>';
								xoptions += '<option value="'+key+'" '+xselected+'>'+keyDisplayNames[key]+'</option>';
								yoptions += '<option value="'+key+'" '+yselected+'>'+keyDisplayNames[key]+'</option>';
							}

							// draw the axis choosers
							axisDiv.html('');
							var select = '';
							var options = [
								{draw:chooseSplitGraph, name:'Split On', class:'splitGraphOn', options:splitGraphOnOptions, function:'StatGraphs.setSplitGraphOnOption(\''+graph_id+'\');'},
								{draw:true, name:'X Axis', class:'xaxis', options:xoptions, function:'StatGraphs.setAxis(\'xaxis\',\''+graph_id+'\');'},
								{draw:true, name:'Y Axis', class:'yaxis', options:yoptions, function:'StatGraphs.setAxis(\'yaxis\',\''+graph_id+'\');'}];
							for(var i = 0; i < options.length; i++) {
								if (!options[i].draw)
									continue;
								select += options[i].name+': <select class="'+options[i].class+'" onchange="'+options[i].function+'">'+options[i].options+'</select>';
								select += '<br />';
							}

							// draw the average checkbox
							var checked = (meta.drawAverage ? "CHECKED" : "");
							var avgCheckbox = 'Draw Average: <input class="average" type="checkbox" onchange="StatGraphs.setDrawAverage(\''+graph_id+'\');" '+checked+' /><br />';

							axisDiv.append(select+avgCheckbox);
						},
						drawSplitGraphChoices: function(graph_id) {

							// get some data on the graph
							var meta = this.graphData[graph_id].meta;
							var splitGraphIndex = meta.splitGraphIndex;

							// get the values to draw
							var drawVals = [];
							for (var key in splitGraphIndex) {
								var drawval = '<input type="checkbox" onchange="StatGraphs.setDrawGraph(\''+graph_id+'\',\''+key+'\');" '+checked+' /><font onmouseover="StatGraphs.setDrawLines(\''+graph_id+'\',\''+key+'\');">'+key+'</font><br />';
								drawVals.push(drawval);
							}
						},
						getGraphData: function(graph_id, data, meta) {
							var alldata = [];
							var splitGraphIndex = {};

							// split the data into multiple graphs
							var graphDatas = [];
							if (meta.splitGraphOn == "") {
								graphDatas = {0:data};
							} else {
								for (var i = 0; i < data.length; i++) {
									var key = data[i][meta.splitGraphOn];
									if (!graphDatas[key] && typeof(graphDatas[key]) == 'undefined')
										graphDatas[key] = [];
									graphDatas[key].push(data[i]);
								}
							}

							// get the graph data for each graph
							var graphs = [];
							for (var key in graphDatas) {
								var gdata = graphDatas[key];
								var gdataPoints = [];
								this.interpretData(graph_id, null);
								for(var i = 0; i < gdata.length; i++) {
									var xdata = this.interpretData(graph_id, gdata[i][meta.xaxis], 'xaxis');
									var ydata = this.interpretData(graph_id, gdata[i][meta.yaxis], 'yaxis');
									var gdataPoint = [ xdata, ydata ];
									gdataPoints.push(gdataPoint);
									alldata.push(gdataPoint);
								}
								if (typeof(meta.drawLines[key]) != 'undefined')
									graphs.push({data:gdataPoints, lines:{show:true}});
								else
									graphs.push({data:gdataPoints});
								splitGraphIndex[key] = graphs.length-1;
							}

							return {graphs:graphs, alldata:alldata, splitGraphIndex:splitGraphIndex};
						},
						plotGraph: function(graph_id) {

							// get some data on the graph
							var meta = this.graphData[graph_id].meta;
							var data = this.graphData[graph_id].data;
							var keys = []; for(var key in data) { keys.push(key); }
							var graph = this.getGraph(graph_id);
							var graphs = {};
							var alldata = [];

							// sort the data
							data.sort(function(a,b) {
								return a[meta.xaxis] < b[meta.xaxis];
							});

							// get the data to graph
							var graphData = this.getGraphData(graph_id, data, meta);
							graphs = graphData.graphs;
							alldata = graphData.alldata;
							meta.splitGraphIndex = graphData.splitGraphIndex;

							// draw the average
							if (meta.drawAverage) {

								// get the average
								alldata.sort(function(a,b) {
									return (a[0] - b[0]);
								});
								var avg = {};
								var gavg = [];
								var part = 1;
								var divisions = Math.min(30, alldata.length/2);
								var min = alldata[0][0];
								var max = alldata[alldata.length-1][0];
								var partsize = (max-min)/divisions;
								for (var i = 0; i < alldata.length; i++) {
									if (alldata[i][0] > (min+partsize*part)) {
										part++;
									}
									if (!avg[part])
										avg[part] = {count:0, total:0};
									avg[part].total += alldata[i][1];
									avg[part].count++;
								}
								for (var i = 1; i <= part; i++) {
									gavg.push([ (i*partsize+min), (avg[i].total/avg[i].count) ]);
								}

								// add the average graph
								graphs.push({data:gavg, lines:{show:true}});
							}

							// get the options for graphing
							var options = {
								series: {
									lines: { show:false },
									points: { show:true }
								},
								xaxis: {
									tickFormatter: function(key) {
										return StatGraphs.reverseInterpretData(graph_id, key, 'xaxis');
									}
								},
								yaxis: {
									tickFormatter: function(key) {
										return StatGraphs.reverseInterpretData(graph_id, key, 'yaxis');
									}
								},
								grid: {
									hoverable: true
								}
							};

							$.plot(graph, graphs, options);
							graph.bind("plothover", function (event, pos, item) {
								if (item) {
									StatGraphs.showHoverText(graph_id, event, pos, item);
								} else {
									StatGraphs.removeTooltip(graph_id);
								}
							});
						},
						interpretData: function(graph_id, dataPoint, axis) {

							// reset the ranking system
							if (dataPoint === null ||
							    typeof(this.interpretDataRanking[graph_id]) == 'undefined' ||
							    typeof(this.interpretDataReverseRanking[graph_id]) == 'undefined') {
								this.interpretDataRanking[graph_id] = {xaxis:[], yaxis:[]};
								this.interpretDataReverseRanking[graph_id] = {xaxis:{}, yaxis:{}};
							}
							if (dataPoint === null)
								return 0;
							var ranking = this.interpretDataRanking[graph_id][axis];
							var reverseRanking = this.interpretDataReverseRanking[graph_id][axis];

							// it's a boolean, return 0 or 1
							if (dataPoint === false || dataPoint === true) {
								var retval = (dataPoint ? 1 : 0);
								reverseRanking[retval] = dataPoint;
								return retval;
							}

							// it's a number, return the number
							if (isNumber(dataPoint)) {
								var retval = (dataPoint.indexOf('.') >= 0 || dataPoint.indexOf(',') >= 0) ? parseFloat(dataPoint) : parseInt(dataPoint);
								reverseRanking[retval] = dataPoint;
								return retval;
							}

							// it's a date, convert it to a unix date
							var trimmed = dataPoint.trim();
							var dateMatch = trimmed.match(/[0-9]{4}-[0-9]{2}-[0-9]{2}/);
							var fullMatch = trimmed.match(/[0-9]{4}-[0-9]{2}-[0-9]{2} [0-9]{2}:[0-9]{2}:[0-9]{2}/);
							if (dateMatch !== null && dateMatch.length > 0 && (dateMatch[0] == trimmed || fullMatch[0] == trimmed)) {
								var dateParts = dateMatch[0].split('-');
								var date = null;
								if (dateMatch == trimmed) {
									date = new Date(dateParts[0], parseInt(dateParts[1])-1, dateParts[2], 0, 0, 0, 0);
								} else {
									timeParts = fullMatch[0].split(' ')[1].split(':');
									date = new Date(dateParts[0], parseInt(dateParts[1])-1, dateParts[2], timeParts[0], timeParts[1], timeParts[2], 0);
								}
								retval = date.getTime();
								reverseRanking[retval] = dataPoint;
								return retval;
							}

							// it's a string, rank it
							if (ranking.indexOf(dataPoint) < 0)
								ranking.push(dataPoint);
							var retval = ranking.indexOf(dataPoint);
							reverseRanking[retval] = dataPoint;
							return retval;
						},
						reverseInterpretData: function(graph_id, dataPoint, axis) {
							if (typeof(this.interpretDataReverseRanking[graph_id][axis][dataPoint]) != 'undefined')
								return this.interpretDataReverseRanking[graph_id][axis][dataPoint];

							if (isNumber(dataPoint) && parseFloat(dataPoint) > 1200000000000) {
								var date = new Date(parseInt(dataPoint));
								var year = date.getFullYear();
								var month = ('00'+(date.getMonth()+1));
								var day = ('00'+date.getDate());
								month = month.substr(month.length-2);
								day = day.substr(day.length-2);
								return year+'-'+month+'-'+day;
							}
							return dataPoint;
						},

						/*******************************************************
						 * Get parts of the graph dom element shortcuts
						 ******************************************************/
						getGetvals: function(graph_id) {
							if (typeof(this.getvals[graph_id]) == 'undefined')
								this.getvals[graph_id] = {};
							return this.getvals[graph_id];
						},
						getContainer: function(graph_id) {
							var getvals = this.getGetvals(graph_id);
							if (typeof(getvals.container) == 'undefined')
								getvals.container = $("#"+graph_id);
							return getvals.container;
						},
						getGraph: function(graph_id) {
							var jcontainer = this.getContainer(graph_id);
							var getvals = this.getGetvals(graph_id);
							if (typeof(getvals.graph) == 'undefined')
								getvals.graph = jcontainer.find(".graph")
							return getvals.graph;
						},
						getAxisDiv: function(graph_id) {
							var jcontainer = this.getContainer(graph_id);
							var getvals = this.getGetvals(graph_id);
							if (typeof(getvals.axisDiv) == 'undefined')
								getvals.axisDiv = jcontainer.find(".axis_choice")
							return getvals.axisDiv;
						},
						getSplitOnDiv: function(graph_id) {
							var jcontainer = this.getContainer(graph_id);
							var getvals = this.getGetvals(graph_id);
							if (typeof(getvals.splitOnDiv) == 'undefined')
								getvals.splitOnDiv = jcontainer.find(".split_on_div");
							return getvals.splitOnDiv;
						},

						/*******************************************************
						 * plot hover functions
						 ******************************************************/
						// from http://www.jqueryflottutorial.com/how-to-make-jquery-flot-time-series-chart.html
						showTooltip: function(x, y, color, contents) {
							$("<div id='tooltip'>" + contents + "</div>").css({
								position: "absolute",
								display: "none",
								top: y - 40,
								left: x - 20,
								border: "2px solid " + color,
								padding: "3px",
								"font-size": "9px",
								"border-radius": "5px",
								"background-color": "#fff",
								"font-family": "Verdana, Arial, Helvetica, Tahoma, sans-serif",
								opacity: 1,
								"z-index": 1,
							}).appendTo("body").fadeIn(200);
						},
						showHoverText: function(graph_id, event, pos, item) {
							function addCommas(nStr)
							{
								nStr += "";
								x = nStr.split(".");
								x1 = x[0];
								x2 = x.length > 1 ? "." + x[1] : "";
								var rgx = /(\d+)(\d{3})/;
								while (rgx.test(x1)) {
									x1 = x1.replace(rgx, "$1" + "," + "$2");
								}
								return x1 + x2;
							}
							var previousPoint = this.previousPoint[graph_id];
							var previousLabel = this.previousLabel[graph_id];
							if ((previousLabel != item.series.label) || (previousPoint != item.dataIndex)) {
								previousPoint = item.dataIndex;
								previousLabel = item.series.label;
								$("#tooltip").remove();

								var x = item.datapoint[0];
								var y = item.datapoint[1];
								var color = item.series.color;

								this.showTooltip(item.pageX, item.pageY, color,
								"<strong>" + this.reverseInterpretData(graph_id, x, 'xaxis') + "</strong><br>"  +
								addCommas(y));
							}
						},
						removeTooltip: function(graph_id) {
							var previousPoint = this.previousPoint[graph_id];
							$("#tooltip").remove();
							previousPoint = null;
						}
					};
				}

				StatGraphs.graphData["<?= $s_graph_id ?>"] = {meta: {xaxis:"<?= $s_xaxis ?>", yaxis:"<?= $s_yaxis ?>", chooseSplitGraph:"<?= ($b_chooseSplitGraph?'true':'false') ?>", splitGraphOn:"<?= $s_splitGraphOn ?>", keyDisplayNames:<?= $s_keyDisplayNames ?>, drawAverage:true, drawLines:{}}, data:<?= $s_json_data ?>};
			</script>
			<?php
			$s_graph_javascript = ob_get_contents();
			ob_end_clean();

			// generate the dom
			ob_start();
			?>
			<div class="blockScreen" style="display:none; position:fixed; top:0; left:0; width:100%; height:100%; background-color:rgba(0,0,0,0.3);">&nbsp;</div>
			<div style="display:none; position:fixed; width:calc(100% - 100px); height:calc(100% - 100px); background-color:white; border:1px solid black; top:50px; left:50px; z-index:1;" id="<?= $s_graph_id ?>">
				<div class="graph" style="position:absolute; left:210px; top:10px; width:calc(100% - 220px); height:calc(100% - 20px); border:1px solid lightgray;">
				</div>
				<div style="position:absolute; left:10px; top:10px; width:200px; height:calc(100% - 20px);">
					<div class="axis_choice" style="position:absolute; top:0px; left:0px; width:100%; height:90px;">
					</div>
					<div class="split_on_div" style="position:absolute; top:100px; width:100%; height:calc(100% - 100px); overflow:auto;">
					</div>
				</div>
				<div class="close_button" style="position:absolute; top:-15px; right:-15px; width:30px; height:30px; border-radius:40px; border:1px solid black; background-color:#aecd37; cursor:pointer;" onclick="StatGraphs.closeGraph('<?= $s_graph_id ?>');" onmouseover="$(this).css({'background-color':'white'});" onmouseout="$(this).css({'background-color':'#aecd37'});">
					<div style="display:table; margin:0 auto; position:relative; top:8px;">X</div>
				</div>
			</div>
			<?php
			$s_graph_dom = ob_get_contents();
			ob_end_clean();

			$s_graph_code = $s_graph_javascript.$s_graph_dom;

			return array('open_button'=>$s_open_button_code, 'graph_code'=>$s_graph_code);
		}

		/**
		 * Get the id number of the next graph to be created
		 * and then update the graph counter.
		 * @return int The id number of the next graph to be created
		 */
		private function getNextGraphCount() {
			$this->graphCount++;
			$_SESSION['statisticsGraph_graphCount'] = $this->graphCount;
			return ($this->graphCount-1);
		}
	}

	global $o_statisticsGraph;
	$o_statisticsGraph = new statisticsGraph();

?>