<?php
	require_once($_SERVER['DOCUMENT_ROOT']."/manage/error_config.php");
	require_once("messagesDB.php");
	require_once(dirname(__FILE__)."/../../../cp/resources/core_functions.php");
	if( isset($_POST['function'])){
		
		if (function_exists($_POST['function'])){
			if( strlen($_POST['function']) > 1){
				$function= $_POST['function'];
				unset($_POST['function']);
				
				$function($_POST);
			
			}else	
				$_POST['function']();
		}else
			echo "error:no function of name ". $_POST['function'];
	}else
		echo "error: no function name given.";

	function new_message($vars){
		if( isset($vars['title']) && isset($vars['type']) && isset($vars['content'])){
			$db->saveNewMessage($vars['title'], $vars['type'], $vars['content']);
		}
		else	
			echo "FAILED, Data not sent";

	}
	function save_video($vars){
		$vid = $vars['value'];
		if( mysqli_num_rows( mlavu_query("SELECT * FROM `poslavu_MAIN_db`.`messages` where `type`= '6'")))
			mlavu_query("UPDATE `poslavu_MAIN_db`.`messages` SET `content` ='[1]' where `type`='6' LIMIT 1", $vid);
		else
			mlavu_query("INSERT INTO `poslavu_MAIN_db`.`messages` (`content`,`type`) VALUES ('[1]','6') ", $vid);
	}
	function delete_message($vars){
		if( isset($vars['id'])){
			
			$db = new messagesDB();
			$id = $vars["id"];
			$result= $db->deleteMessage($id);
			if($result=='success')
				echo "success";
			else
				echo "Failed Deleting Message. id: ".$id;
			
		}else
			echo "FAILED: no id sent";
	}
	function update_message($vars){
		if( isset($vars['title']) && isset($vars['type']) && isset($vars['content']) && isset($vars['id'])){
			
			$db->updateEntry($vars['title'], $vars['type'], $vars['content'], $vars['id']);
		}
		else	
			echo "FAILED, Data not sent";

	}
	function edit_message($vars){
	
		if( isset($vars['id'])){
			$db = new messagesDB();
			$id = $vars["id"];
			$result= $db->getMessage($id);
			$result[0]['content']= rawurlencode($result[0]['content']);
			//echo print_r($result,1);
			echo lavu_json_encode($result);
		}else
			echo "FAILED: no id sent";
	}
	function save_edited_message($vars){
	
		$db = new messagesDB();
		$mod_limit='';
		//echo print_r($vars,1);
		if(!empty($vars['limiting_module']) && !empty($vars['module_use']) && !empty($vars['mod_combiner'])){
			//echo "limiting_mod=". $vars['limiting_module'];
			$arr= explode(",", urldecode($vars['limiting_module']));
			$mod_limit= '{"module_use":'.$vars['module_use'].',"mod_combiner":"'.$vars['mod_combiner'].'","limiting_module":'.lavu_json_encode($arr).'}';
		}else{
			$mod_limit='';
		}
		
		$echo_this = "FAILED";

		if (isset($vars['removal_date']) && !empty($vars['removal_date']))
			$vars['removal_date']= date("Y-m-d",strtotime(rawurldecode($vars['removal_date'])));
		else
			$vars['removal_date']='';
		
		if ($vars['title']=='') {
			$echo_this = "Your Message needs a title";
		} else if ($vars['type']=='') {
			$echo_this = "Your Message needs a type";
		} else if (rawurldecode($vars['content'])=='') {
			$echo_this = "Your Message needs content";
		} else if ($vars['id']=='') {
			if ($db->insertNewMessage($vars['title'],$vars['type'],rawurldecode($vars['content']),$vars['username'],rawurldecode($vars['limiting_query']),$mod_limit, $vars['removal_date'], $vars['limiting_access_level'])) {
				$vars['id'] = mlavu_insert_id();
				$echo_this = "success";
			}
		} else {
			if ($db->updateEntry($vars['title'],$vars['type'],rawurldecode($vars['content']),$vars['username'],rawurldecode($vars['limiting_query']),$mod_limit, $vars['id'], $vars['removal_date'], $vars['limiting_access_level']))
				$echo_this = "success";
		}
		
		ob_start();
		echo $echo_this;
		$size = ob_get_length();
		header("Content-Length: $size");
		header('Connection: close');
		ob_end_flush();
		ob_flush();
		flush();
				
		if ($echo_this == "success") {
			if ($vars['type'] == "7") {
				require_once(dirname(__FILE__)."/front_end_messages.php");
				process_front_end_message($vars);
			}
		}
	}

?>