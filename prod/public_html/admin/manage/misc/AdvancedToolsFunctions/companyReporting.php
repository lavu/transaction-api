<?php
	
	//if(!function_exists("resource_path"))
	//	require_once($_SERVER['DOCUMENT_ROOT'].'/cp/resources/core_functions.php');
	
	require_once($_SERVER['DOCUMENT_ROOT']."/manage/error_config.php");
	require_once('/home/poslavu/public_html/admin/cp/resources/json.php');
	
	require_once(dirname(__FILE__).'/companyReportingExternalFunctions.php');
	
	if (!isset($maindb))
		$maindb = "poslavu_MAIN_db";
	if (!function_exists("is_dev")) {
		function is_dev(){
			if(strpos($_SERVER['REQUEST_URI'],"v2/")!==false)
				return true;
			else
				return false;
		}
	}	

	ob_start();
	require_once(dirname(__FILE__).'/../../login.php');
	
	$str_dumpvar = ob_get_contents();
	ob_end_clean();

	if (!account_loggedin() && (!function_exists('admin_loggedin') || !admin_loggedin()))
		header('Location: /manage/');
	
	class statsReporting{

		/* GLOBALS */

		private $lavuLiteStats='';
		private $lavuStats='';
		private $geographicStats='';
		private $deviceStats='';
		private $unusedAccounts='';
		private $activeDatanames = array();
		private $totalSoldLicenses = array();
		/*END GLOBALS */

		function __construct(){
		}
		//array('title'=>"ABDA", 'id'=>'activeBillingDisAccount', 'functionName'=>'drawAbdaAccounts', 'access'=>array('company_reporting', 'customers', 'view_referrals'))
		private function getStatsCategories() {
			return array(
				array('title'=>"Lavu Lite Stats", 'functionName'=>'drawLavuLiteStats', 'container'=>'pre', 'access'=>array('company_reporting')),
				array('title'=>"Geographic Stats", 'functionName'=>'drawGeographicStats', 'container'=>'pre', 'access'=>array('company_reporting')),
				array('title'=>"Active Restaurants", 'functionName'=>'drawActiveDatanames', 'access'=>array('company_reporting')),
				array('title'=>"Device Stats", 'functionName'=>'drawDeviceStatistics', 'access'=>array('company_reporting')),
				array('title'=>"Report Usage", 'functionName'=>'drawReportUsage', 'access'=>array('company_reporting')),
				array('title'=>"Satisfaction Survey Results", 'id'=>'SatisfactionSurveyDiv', 'functionName'=>'drawSatisfactionSurveyResults', 'access'=>array('company_reporting', 'satisfaction_survey')),
				array('title'=>"Leads Status", 'id'=>'leadStatusContainer', 'functionName'=>'drawLeadsStatus', 'access'=>array('company_reporting')),
				array('title'=>"Account Alerts", 'id'=>'alertAccountsByNoteCountContainer', 'functionName'=>'drawAlertAccountsByNoteCount', 'access'=>array('company_reporting', 'customers')),
				array('title'=>"Location Settings", 'id'=>'outdatedLocationConfigSettings', 'functionName'=>'drawOutdatedLocationConfigSettings', 'access'=>array('company_reporting', 'customers')),
				array('title'=>"Upgraded Accounts", 'id'=>'listOfUpgradedAccounts', 'functionName'=>'drawListOfUpgradedAccounts', 'access'=>array('company_reporting', 'customers')),
				array('title'=>"Stacked Accounts", 'id'=>'listOfStackedAccounts', 'functionName'=>'drawListOfStackedAccounts', 'access'=>array('company_reporting', 'customers', 'technical')),
				array('title'=>"Multiple ARBs", 'id'=>'listOfMultipleActiveARBs', 'functionName'=>'drawActiveBillingProfiles_allAccounts_multipleProfiles', 'access'=>array('company_reporting', 'customers', 'technical')),
				array('title'=>"Referrals", 'id'=>'simpleReferralStats', 'functionName'=>'drawSimpleReferralStats', 'access'=>array('company_reporting', 'customers', 'view_referrals')),
				array('title'=>"Customer Breakdown", 'id'=>'custBreakdownStats', 'functionName'=>'drawCustBreakdown', 'access'=>array('company_reporting', 'customers', 'view_referrals')),
				array('title'=>"Canceled Reasons", 'id'=>'custCancelStats', 'functionName'=>'drawCancelReasons', 'access'=>array('company_reporting', 'customers', 'view_referrals'))
			);
		}

		private function getCategoryInputs($a_category) {
			$input = (isset($a_category['input'])) ? $a_category['input'] : '<input type="button" onclick="getStatistics(this, \''.$a_category['functionName'].'\');" value="Load" />';
			if ($a_category['title'] == 'Satisfaction Survey Results')
				$input .= '<input type="button" onclick="getStatistics(this, \''.$a_category['functionName'].'_byDistro\');" value="View By Distributor" /><br />
				<input type="text" placeholder="start date (optional)" name="start" class="date_selector survey start">
				<input type="text" placeholder="end date (optional)" name="end" class="date_selector survey end">
				<script type="text/javascript">
					$(".survey.date_selector").datepicker();
				</script>';
			return $input;
		}
		function drawCustBreakdown(){
			$contents= LavuJson::json_decode(file_get_contents($_SERVER['DOCUMENT_ROOT']."/manage/misc/AdvancedToolsFunctions/cust_breakdown_info.txt"));

			if($contents['date'] != date("Y-m-d", time())){
				$contents = $this->get_cust_breakdown($contents['totals']);

				file_put_contents($_SERVER['DOCUMENT_ROOT']."/manage/misc/AdvancedToolsFunctions/cust_breakdown_info.txt", LavuJson::json_encode($contents));
			}
			//echo print_r($contents['totals'],1);
			//$contents   = $contents['info'];
			$return_str = "
			<table>
				<tr>
					<th colspan =4 style='text-align:center'> Stats Today </th>
				</tr>
				<tr>
					<td>Level</td>
					<td>Average orders </td>
					<td>Num Zeroes</td>
					<td>Non-Zeroes</td>
				</tr>
				<tr>
					<td style='text-align:center'>Silver</td>
					<td style='text-align:center'>".$contents['totals']['Silver']['average']."</td>
					<td style='text-align:center'>".$contents['totals']['Silver']['zeroes']."</td>
					<td style='text-align:center'>".$contents['totals']['Silver']['non_zeroes']."</td>
				</tr>
				<tr>
					<td style='text-align:center'>Gold</td>
					<td style='text-align:center'>".$contents['totals']['Gold']['average']."</td>
					<td style='text-align:center'>".$contents['totals']['Gold']['zeroes']."</td>
					<td style='text-align:center'>".$contents['totals']['Gold']['non_zeroes']."</td>
				</tr>
				<tr>
					<td style='text-align:center'>Platinum</td>
					<td style='text-align:center'>".$contents['totals']['Platinum']['average']."</td>
					<td style='text-align:center'>".$contents['totals']['Platinum']['zeroes']."</td>
					<td style='text-align:center'>".$contents['totals']['Platinum']['non_zeroes']."</td>

				</tr>
			</table>";
			/*
			$return_str .= "
			<table>
				<tr>
					<th colspan =4 style='text-align:center'> Stats Overtime (2014-01-03) </th>
				</tr>
				<tr>
					<td>Level</td>
					<td>Average orders </td>
					<td>Num Zeroes</td>
					<td>Non-Zeroes</td>
				</tr>
				<tr>
					<td style='text-align:center'>Silver</td>
					<td style='text-align:center'>".$contents['totals']['Silver']['average']."</td>
					<td style='text-align:center'>".$contents['totals']['Silver']['zeroes']."</td>
					<td style='text-align:center'>".$contents['totals']['Silver']['non_zeroes']."</td>
				</tr>
				<tr>
					<td style='text-align:center'>Gold</td>
					<td style='text-align:center'>".$contents['totals']['Gold']['average']."</td>
					<td style='text-align:center'>".$contents['totals']['Gold']['zeroes']."</td>
					<td style='text-align:center'>".$contents['totals']['Gold']['non_zeroes']."</td>
				</tr>
				<tr>
					<td style='text-align:center'>Platinum</td>
					<td style='text-align:center'>".$contents['totals']['Platinum']['average']."</td>
					<td style='text-align:center'>".$contents['totals']['Platinum']['zeroes']."</td>
					<td style='text-align:center'>".$contents['totals']['Platinum']['non_zeroes']."</td>

				</tr>
			</table>";
			*/
			echo $return_str;
		}
		function get_cust_breakdown($previous_avg){
			$a_active_datanames = $this->getActiveDatanames(FALSE, array("with_package_level"=>1), TRUE);
			$display 	 = array("Silver"=>array(), "Gold"=>array(), "Platinum"=>array());
			$info_array  = array("date"=> date("Y-m-d"));

			$silver_orders   = 0;
			$gold_orders     = 0;
			$platinum_orders = 0;

			$silver_count   = 0;
			$gold_count     = 0;
			$platinum_count = 0;

			$silver_zeroes   = 0;
			$gold_zeroes     = 0;
			$platinum_zeroes = 0;

			foreach($a_active_datanames as $info){

				$dn    = $info['data_name'];
				$level = $info['package_level'];
				$res   = mysqli_fetch_assoc(mlavu_query("select count(*) as `poslavu_[2]_db`.`num_orders` from `orders` where `opened` >= '[1]' ", date("Y-m-d")." 00:00:00",$dn));

				if(strlen($level) == 2)
					$level = substr($level,-1);

				if($level == 1){//put it in the silver category
					$display['Silver'][$dn]=$res['num_orders'];
					if($res['num_orders']*1 == 0)
						$silver_zeroes++;
					else{
						$silver_count++;
						$silver_orders += $res['num_orders'];
					}

				}else if($level == 2){//put it in the gold category
					$display['Gold'][$dn]=$res['num_orders'];
					if($res['num_orders']*1 == 0)
						$gold_zeroes++;
					else{
						$gold_orders += $res['num_orders'];
						$gold_count++;
					}
				}else if($level == 3){//put it in the platinum category
					$display['Platinum'][$dn]=$res['num_orders'];
					if($res['num_orders']*1 == 0)
						$platinum_zeroes++;
					else{
						$platinum_orders += $res['num_orders'];
						$platinum_count++;
					}
				}
			}
			//echo $platinum_orders ."  ".$platinum_count;
            $averages['Silver']=array("average"    => $silver_orders/$silver_count,
                                      "zeroes"     => $silver_zeroes,
                                      "non_zeroes" => $silver_count
                                      );
            $averages['Gold']=array("average"    => $gold_orders/$gold_count,
                                    "zeroes"     => $gold_zeroes,
                                    "non_zeroes" => $gold_count
                               );
            $averages['Platinum']=array("average"    => $platinum_orders/$platinum_count,
                                        "zeroes"     => $platinum_zeroes,
                                        "non_zeroes" => $platinum_count
                                      );
			$averages['days_counted']=date("Y-m-d", (date("Y-m-d") - date("Y-m-d",$previous_avg['date'])));
            $info_array["info"]   = $display;
            $info_array["totals"] = $averages;
			return $info_array;
		}
		function displayData(){
			$a_stats_categories = $this->getStatsCategories();

			// create the stats based off of the above array
			$a_headers = array();
			foreach($a_stats_categories as $k=>$a_category) {
				$b_can_access = FALSE;
				if (isset($a_category['access']))
					if (can_access_by_array($a_category['access']) == count($a_category['access']))
						$b_can_access = TRUE;
				$a_stats_categories[$k]['can_access']=$b_can_access;
				if ($b_can_access)
					$a_headers[] = array('title'=>$a_category['title'], 'stat'=>$a_stats_categories[$k]);
			}

			// split the statistics into multiple rows so that it all fits on one line
			$i_max_stats_per_line = 8;
			$a_headers_by_line = array();
			$i_count = 0;
			$i_line_index = 0;
			foreach($a_headers as $a_header) {
				if ($i_count == $i_max_stats_per_line) {
					$i_line_index++;
					$i_count = 0;
				}
				$a_headers_by_line[$i_line_index][] = $a_header;
				$i_count++;
			}

			echo "
					<table id='displayLavuData' style='border:none 0 clear; height:200px; background-color:white; padding:20px;'>";

			// draw the lines (maximum $i_max_stats_per_line statistics per line)
			foreach($a_headers_by_line as $a_headers) {

				// start the line
				echo  "
						<tr>";

				// draw the headers
				foreach($a_headers as $index=>$a_header) {
					$style = '';
					if ($index == 0)
						$style .= 'border-left:1px solid black;';
					if ($index == count($a_headers)-1)
						$style .= 'border-right:1px solid black;';
					echo "
							<th style='padding:10px;border-top:1px solid black;$style'>
								".$a_header['title']."
							</th>";
				}

				// draw the statistics
				echo "
						</tr>
						<tr>";
				foreach($a_headers as $index=>$a_header) {
					$style = '';
					if ($index == 0)
						$style .= 'border-left:1px solid black;';
					if ($index == count($a_headers)-1)
						$style .= 'border-right:1px solid black;';
					$a_category = $a_header['stat'];
					if (!$a_category['can_access'])
						continue;
					$i_row_height = 250;
					$container = (isset($a_category['container'])) ? $a_category['container'] : 'div';
					$id = (isset($a_category['id'])) ? $a_category['id'] : '';
					$input = $this->getCategoryInputs($a_category);
					echo '
							<td style="padding:10px;border-bottom:1px solid black;'.$style.'">
								<'.$container.' style="height: '.$i_row_height.'px; overflow-y:auto;" id="'.$id.'">'.$input.'</'.$container.'>
							</td>';
				}

				// end the line
				echo "
						</tr>";
			}

			echo "
					</table>";

			/*echo "
					</tr>";
			if (in_array("Lavu Lite Stats", $a_headers)) {
				echo "
					<tr>
						<th>
							Unused LavuLite Accounts
						</th>
					</tr>
					<tr>
						<td>
							".$this->unusedAccounts."
						</td>
					</tr>
				</table>";
			}*/

			echo $this->drawJavascripts();
		}

		// draws the given statistics in the standard list format, with links to each category and quick stats on the categories
		// @$s_id: id of the statistic
		// @$a_headers: an array of each category, in the format:
		//      array('plain_name'=>javascript compliant name, 'printed_name'=>name shown to the user, 'quick_stat'=>optional, a quick stat on the category (eg, a number), 'body'=>the string of the body of the statistic)
		// @return a string to be printed
		function drawStatsListFormat($s_id, &$a_headers) {

			// some commonly used base variables
			$s_basename = $s_id.'_';
			$s_scroll_top = '<a onclick="elementScrollTop($(\'#'.$s_basename.'top\'), $(\'#'.$s_id.'\'));" style="cursor:pointer;">Back to top</a>';

			// draw the menu
			$s_retval = '';
			$s_retval .= '<a id="'.$s_basename.'top" style="visibility:hidden;">&nbsp;</a><br />';
			foreach($a_headers as $a_header) {
				$s_plain_name = $a_header['plain_name'];
				$s_printed_name = $a_header['printed_name'];
				$s_quick_stat = $a_header['quick_stat'];
				$s_retval .= '
					<a onclick="elementScrollTop($(\'#'.$s_basename.$s_plain_name.'\'), $(\'#'.$s_id.'\'));" style="cursor:pointer;">'.$s_printed_name.($s_quick_stat != '' ? " ({$s_quick_stat})" : '').'</a><br />';
			}
			$s_retval .= '<br />';

			// draw each category
			foreach($a_headers as $a_header) {
				$s_plain_name = $a_header['plain_name'];
				$s_printed_name = $a_header['printed_name'];
				$s_quick_stat = $a_header['quick_stat'];
				$s_retval .= '
					<font style="font-weight:bold;" id="'.$s_basename.$s_plain_name.'">'.$s_printed_name.($s_quick_stat != '' ? " ({$s_quick_stat})" : '').'</font><br />
					'.$s_scroll_top.'<br />
					<br />';
				$s_retval .= $a_header['body'];
				$s_retval .= '
					<br />';
			}

			return $s_retval;
		}

		function getListOfStackedAccounts() {

			// get the stacked accounts from `locations` of individual accounts
			$a_active_datanames = $this->getActiveDatanames(FALSE, NULL, TRUE);
			$s_inclause = ConnectionHub::getConn('poslavu')->getDBAPI()->arrayToInClause($a_active_datanames, TRUE);
			$a_stacked_accounts = array();
			foreach($a_active_datanames as $s_dataname) {
				$a_locations = ConnectionHub::getConn('poslavu')->getDBAPI()->getAllInTable('locations', array('_disabled'=>'0'), TRUE, array('databasename'=>'poslavu_'.$s_dataname.'_db', 'selectclause'=>'id'));
				if (count($a_locations) > 1) {
					$a_stacked_accounts[] = array('dataname'=>$s_dataname, 'count'=>count($a_locations));
				}
			}

			return $a_stacked_accounts;
		}
		function drawReportUsage(){

			$query= mlavu_query("SELECT * FROM `poslavu_MAIN_db`.`customer_backend_usage` "); // poslavu_MAIN_db
			$report_arr= array();
			while($result= mysqli_fetch_assoc($query)){
				if(array_key_exists($result['link_clicked'], $report_arr))
					$report_arr[$result['link_clicked']]+=$result['num_clicks'];
				else
					$report_arr[$result['link_clicked']]=$result['num_clicks'];
			}
			$html="
			<form class='report_usage_form'>
				<table>
				<tr>
					<td><input type='text' placeholder='start date' class='start_date_selector' name='start_date' style='display:inline-block'></td>
					<td><input type='text' placeholder='end date' class='end_date_selector' name='end_date' style='display:inline-block'></td>
				</tr>
				<tr>
					<td><input type='text' placeholder='dataname (optional)' name='data_name'></td><td><input type='button' onclick='get_dates($(\".report_usage_form\"))' value='submit'> </td>
				</tr>
			</form>
			<script type='text/javascript'>
				$('.start_date_selector').datepicker();
				$('.end_date_selector').  datepicker();
			</script>
			<table><tr class='report_usage_table'><th colspan= 2> Report Usage Stats </th></tr>";
			arsort($report_arr);
			foreach($report_arr as $report=>$clicks){
				$html .="<tr><td>". $report."</td><td>". $clicks."</td></tr>";
			}
			$html.="</table>";
			return $html;
		}
		function drawListOfStackedAccounts() {
			$a_stacked_accounts = $this->getListOfStackedAccounts();

			// draw the menu
			$s_retval = '';
			$s_retval .= 'Total: '.count($a_stacked_accounts).'<br /><br />';

			// draw each stacked account
			foreach($a_stacked_accounts as $a_account) {
				$s_link = '<a href="/manage/index.php?goToAccount='.$a_account['dataname'].'" target="_blank" style="cursor:pointer;">ua_cp</a>';
				$s_retval .= '
					'.$a_account['dataname'].' ('.$a_account['count'].') '.$s_link.'<br />';
			}

			return $s_retval;
		}

		function drawGeographicStats() {
			if ($this->geographicStats == '')
				$this->geographicStats = $this->getGeographicStats();
			return $this->geographicStats;
		}

		function getListOfUpgradedAccounts(&$a_upgrades = NULL) {
			global $o_statsReportingExternal;
			return $o_statsReportingExternal->getListOfUpgradedAccounts($a_upgrades);
		}

		function drawSimpleReferralStats() {

			// get the stats on emails
			$a_emails_by_referrer = ConnectionHub::getConn('poslavu')->getDBAPI()->getAllInTable('referrals_sent', array('id'=>'38'), TRUE, array('selectclause'=>'`from_dataname` AS `dataname`,COUNT(`id`) AS `count`,GROUP_CONCAT(`to_email` SEPARATOR \', \') AS `to_emails`', 'whereclause'=>"WHERE `id`>'[id]'", 'groupbyclause'=>'GROUP BY `from_dataname`', 'orderbyclause'=>'ORDER BY `count` DESC,`dataname` ASC'));
			$a_emails_by_referree = ConnectionHub::getConn('poslavu')->getDBAPI()->getAllInTable('referrals_sent', array('id'=>'38'), TRUE, array('selectclause'=>'COUNT(`id`) AS `count`', 'whereclause'=>"WHERE `id`>'[id]'", 'orderbyclause'=>'ORDER BY `count` DESC'));

			// get the stats on who has visited the referrals page
			$a_viewed_referrals_page = ConnectionHub::getConn('poslavu')->getDBAPI()->getAllInTable('restaurant_locations', NULL, TRUE, array('whereclause'=>"WHERE `referral_code` != ''", 'selectclause'=>'`dataname`,`referral_code`', 'groupbyclause'=>'GROUP BY `dataname`'));

			// get the stats on who has signed up with a referral code
			$a_referral_codes = array();
			foreach($a_viewed_referrals_page as $a_viewed) {
				$a_referral_codes[$a_viewed['referral_code']] = $a_viewed['dataname'];
				$a_referral_codes['used:'.$a_viewed['referral_code']] = $a_viewed['dataname'];
			}
			$s_inclause = ConnectionHub::getConn('poslavu')->getDBAPI()->arrayToInClause($a_referral_codes);
			$a_signups = ConnectionHub::getConn('poslavu')->getDBAPI()->getAllInTable('signups', $a_referral_codes, TRUE, array('selectclause'=>'`dataname`, `signup_timestamp`,`promo`', 'whereclause'=>"WHERE `promo` $s_inclause", 'orderbyclause'=>'ORDER BY `signup_timestamp` DESC'));

			// -- drawing the stats --
			$a_headers = array();

			// draw the signups
			$s_body = '';
			foreach($a_signups as $a_signup)
				$s_body .= $a_signup['dataname'].' ('.$a_referral_codes[$a_signup['promo']].', '.$a_signup['signup_timestamp'].')<br />';
			$a_headers[] = array('plain_name'=>'signups', 'printed_name'=>'Signups', 'quick_stat'=>count($a_signups), 'body'=>$s_body);

			// draw the emails sent
			$s_body = '';
			foreach($a_emails_by_referrer as $a_emailer)
				$s_body .= $a_emailer['dataname'].' <a onclick="alert(\''.$a_emailer['to_emails'].'\');" style="cursor:pointer;">'.$a_emailer['count'].'</a><br />';
			$a_headers[] = array('plain_name'=>'emails_sent', 'printed_name'=>'Emails Sent', 'quick_stat'=>$a_emails_by_referree[0]['count'], 'body'=>$s_body);

			// draw who has visited the page
			$s_body = '';
			foreach($a_viewed_referrals_page as $a_viewed)
				$s_body .= $a_viewed['dataname'].' - '.$a_viewed['referral_code'].'<br />';
			$a_headers[] = array('plain_name'=>'viewed_referrals_page', 'printed_name'=>'Viewed Referrals Page', 'quick_stat'=>count($a_viewed_referrals_page), 'body'=>$s_body);

			// draw the standard list format
			$s_retval = $this->drawStatsListFormat('simpleReferralStats', $a_headers);

			return $s_retval;
		}

		function drawListOfUpgradedAccounts() {

			// get the data
			$this->getListOfUpgradedAccounts($a_upgrades);

			// prepare each category
			$a_headers = array();
			foreach($a_upgrades as $s_name=>$a_upgrade_cat) {
				$s_body = '';
				foreach($a_upgrade_cat['upgrades'] as $a_upgrade) {
					$s_link = '<a href="/manage/index.php?goToAccount='.$a_upgrade['dataname'].'" target="_blank" style="cursor:pointer;">ua_cp</a>';
					$s_body .= '
						'.$a_upgrade['dataname'].' '.$s_link.'<br />';
				}
				$a_headers[] = array('plain_name'=>$s_name, 'printed_name'=>$a_upgrade_cat['display_name'], 'quick_stat'=>count($a_upgrade_cat['upgrades']), 'body'=>$s_body);
			}

			// draw the standard list format
			$s_retval = $this->drawStatsListFormat('listOfUpgradedAccounts', $a_headers);

			return $s_retval;
		}

		function drawJavascripts() {
			echo '
				<script type="text/javascript">
					function getStatistics(javascriptObject, functionName) {
						var jcontainer = $(javascriptObject).parent();
						var data = { command: "get_statistics", function_name: functionName, company_reporting: "do_stuff" };

						// try to find date selectors and pass on their data
						var jselectors = jcontainer.find(".date_selector");
						$.each(jselectors, function(k,v) {
							data[v.name] = v.value;
						});

						loading();
						$.ajax({
							type: "POST",
							async: true,
							cache: false,
							url: "/manage/misc/AdvancedToolsFunctions/companyReporting.php",
							data: data,
							success: function(message) {
								jcontainer.html(message);
								setTimeout(function() {
									if (parseInt(jcontainer.css("width")) < 200)
										jcontainer.animate({ "width": 200 }, 500);
									if (parseInt(jcontainer.css("width")) > 600)
										jcontainer.animate({ "width": 600 }, 500);
								}, 200);
								doneLoading();
							}
						});
					}

					function elementScrollTop(jelement, jcontainer) {
						jcontainer.scrollTop(0);
						jcontainer.scrollTop(
							jelement.position().top -
							jcontainer.position().top
						);
					}

					function satisfactionSurveyScrollTop(jelement) {
						return elementScrollTop(jelement, $("#SatisfactionSurveyDiv"));
					}
					function showCancelReasons(id){
						if($(id).is(":hidden")){
							$(".monthly_breakdown_reasons").hide();
							$(id).show();
						}else{
							$(".monthly_breakdown_reasons").hide();
						}
					}
				</script>
				';
		}

		function drawActiveBillingProfiles_allAccounts_multipleProfiles() {

			// get the billing profiles and remove those that have count < 2
			$a_billing_profiles = $this->getActiveBillingProfiles_allAccounts(array('hosting'));
			foreach($a_billing_profiles as $k=>$a_billing_profile)
				if ((int)$a_billing_profile['count'] < 2)
					unset($a_billing_profiles[$k]);

			// check that there are still billing profiles to print
			if (count($a_billing_profiles) == 0)
				return 'There are no accounts with multiple active billing profiles.';

			// make the quickinfo
			$s_quickinfo = '';
			$s_quickinfo .= 'Affected Accounts: '.count($a_billing_profiles);

			// make the table
			
			require_once(dirname(__FILE__).'/../autoTable.php');
			$s_script = '<script type="text/javascript">
				function load_company_search_from_dataname() {
					lavuLog(s_global_dataname);
					populateResults(s_global_dataname, function(){
						$("#company_search_button").click();
						doneLoading();
					});
				}
			</script>';
			$s_onclick = "
						s_global_dataname=$( $(this).find('td')[0] ).text();
						load_company_search_from_dataname();
					";
			$a_headers = array('Dataname', 'ARB Count', 'Type');
			$s_table = auto_build_table($a_headers, $a_billing_profiles, $s_onclick, 'allAccounts_multipleProfiles_table');

			return $s_script.$s_quickinfo.$s_table;
		}

		function drawSatisfactionSurveyResults_byDistro() {
			return $this->drawSatisfactionSurveyResults(TRUE);
		}

		function drawSatisfactionSurveyResults($b_orderby_distro = FALSE) {

			// get/set some initial values
			$a_json_data = array();
			$s_retval = '';
			$i = 0;
			$a_date_keys = array();
			$a_distro_keys = array();
			$a_distro_counts = array(0,0,0,0,0,0,0,0,0,0);
			$s_extra_query_code = "";

			// create the arrays into which the results will go
			$a_integer_lists = array('lavu_satisfaction'=>array(), 'distributor_interaction'=>array(), 'distro_satisfaction'=>array());
			$a_string_lists = array('poslavu_feedback'=>array());
			foreach($a_integer_lists as $k=>$a_list)
				for ($i = 1; $i < 6; $i++)
					$a_integer_lists[$k][$i.''] = array();
			foreach($a_string_lists as $k=>$a_list)
				for ($i = 1; $i < 6; $i++)
					$a_string_lists[$k][$i.''] = array();
			//echo "here";	
			require_once('/home/poslavu/public_html/admin/areas/survey.php');
			//echo "here 2";
			//require_once('/home/poslavu/public_html/admin/resources/json.php');

			// check for date selectors
			if (isset($_POST['start'])) {
				$a_date = explode("/", $_POST['start']);
				$s_date = $a_date[2]."-".$a_date[0]."-".$a_date[1]." 00:00:00";
				$s_extra_query_code .= " AND `value` > '".$s_date."'";
			}
			if (isset($_POST['end'])) {
				$a_date = explode("/", $_POST['end']);
				$s_date = $a_date[2]."-".$a_date[0]."-".$a_date[1]." 23:59:59";
				$s_extra_query_code .= " AND `value` < '".$s_date."'";
			}

			// check that there are any survey results
			$s_extra_query_code .= " AND `dataname` NOT IN ('17edison','test_pizza_sho','tigay_restaura') AND `dataname` NOT LIKE '%test%' ";
			$a_multi_survey_results = get_most_recent_survey_results(array('satisfaction survey','lavu satisfaction survey'), 'load_all_poslavu_restaurants', FALSE, TRUE, $s_extra_query_code);

			// process each result
			for ($i = 0; $i < count($a_multi_survey_results); $i++) {

				// get data on the survey results
				$a_survey_results = $a_multi_survey_results[$i];
				$s_date = $a_survey_results['date'];
				$s_distro_code = $a_survey_results['distro_code'];
				$s_dataname = $a_survey_results['dataname'];
				$i_date = strtotime($s_date);
				$a_date_keys[] = $i_date;
				$a_distro_keys[$s_distro_code] = $s_distro_code;
				$o_json_data_part = NULL;

				// add the data to the data lists
				foreach($a_integer_lists as $k=>&$v) {
					if (in_array($a_survey_results[$k], array('1','2','3','4','5'))) {
						$v[$a_survey_results[$k]][$i_date] = array('distro'=>$s_distro_code, 'draw'=>"<tr><td>".$s_dataname."</td><td>".$s_date."</td><td>".$s_distro_code."</td></tr>");
						if ($k == 'lavu_satisfaction' && trim($s_distro_code) != "") $a_distro_counts[$a_survey_results[$k]]++;
						$o_json_data_part = (object)array('sat'=>$a_survey_results[$k], 'date'=>$s_date, 'dn'=>$s_dataname, 'distro'=>$s_distro_code);
					}
				}
				if ($a_survey_results['poslavu_feedback'] !== '')
					$a_string_lists['poslavu_feedback'][$a_survey_results['lavu_satisfaction']][$i_date] = array('dataname'=>$s_dataname, 'feedback'=>"(".$s_date.") ".$a_survey_results['poslavu_feedback']);

				// store the results in the json_data object
				if ($o_json_data_part !== NULL)
					$a_json_data[] = $o_json_data_part;
			}

			// sort and store the data into a json object
			rsort($a_date_keys);
			sort($a_distro_keys);
			//echo "heyaaaaaaa";
			//require_once('/home/poslavu/public_html/admin/resources/json.php');
			$s_json_data = LavuJson::json_encode($a_json_data);

			// provide the interface to load survey results
			require_once(dirname(__FILE__)."/statistics_tools/createGraph.php");
			$a_graph = $o_statisticsGraph->getGraphCode($s_json_data, array("defaultx"=>"date", "defaulty"=>"sat", "splitGraphOn"=>"distro", "keyDisplayNames"=>array('sat'=>'Satisfaction', 'date'=>'Date', 'dn'=>'Dataname', 'distro'=>'Distributor')));
			$s_inputs = "";
			$s_inputs .= $a_graph['open_button'].$a_graph['graph_code'];
			$s_inputs .= $this->getCategoryInputs(array('functionName'=>'drawSatisfactionSurveyResults', 'title'=>'Satisfaction Survey Results'));
			$s_retval .= $s_inputs."<br />";

			// provide a key
			$s_retval .= '
				<a style="cursor:pointer;" onclick="satisfactionSurveyScrollTop($(\'#lavu_satisfaction\'));">lavu_satisfaction</a><a id="satisfactionSurveyTop">&nbsp;</a><br />
				<a style="cursor:pointer;" onclick="satisfactionSurveyScrollTop($(\'#distributor_interaction\'));">distributor_interaction</a><br />
				<a style="cursor:pointer;" onclick="satisfactionSurveyScrollTop($(\'#distro_satisfaction\'));">distro_satisfaction</a><br />
				<a style="cursor:pointer;" onclick="satisfactionSurveyScrollTop($(\'#poslavu_feedback\'));">poslavu_feedback</a><br />
				<br />';

			// display the integer data
			foreach($a_integer_lists as $k=>&$v) {
				$i_total = 0;
				$i_grand_sum = 0;
				for ($i = 1; $i <= 5; $i++) {
					$i_total += count($v[(string)$i]);
					$i_grand_sum += count($v[(string)$i])*$i;
				}
				$i_total = max($i_total, 1);
				$s_retval .= '
					<a style="color:black;font-weight:bold;" id="'.$k.'">'.$k.' ('.number_format($i_grand_sum/$i_total, 1).' avg)</a><br />';
				for($i = 1; $i <= 5; $i++) {
					$count = max(count($v[(string)$i]), 1);
					$s_percent_distros = ($k == 'lavu_satisfaction') ? ', '.number_format($a_distro_counts[$i]*100.0/$count. 1).'% have distros' : '';
					$s_retval .= '
						<a style="cursor:pointer;" onclick="satisfactionSurveyScrollTop($(\'#'.$k.$i.'\'));">Value '.$i.'</a>: '.$count.' ('.(int)($count/$i_total*100).'%'.$s_percent_distros.')<br />';
				}
				$s_retval .= '
					<a style="cursor:pointer;" onclick="satisfactionSurveyScrollTop($(\'#satisfactionSurveyTop\'));">Back to top</a><br />
					<br />';
				for($i = 1; $i <= 5; $i++) {
					$s_retval .= '
						<a style="color:black;font-weight:bold;" id="'.$k.$i.'">Value  '.$i.'</a>:<br />
						<a style="cursor:pointer;" onclick="satisfactionSurveyScrollTop($(\'#'.$k.'\'));">'.$k.'</a><br />
						<br /><table>';
					if ($b_orderby_distro) {
						foreach($a_distro_keys as $s_distro) {
							foreach($a_date_keys as $i_date) {
								if (isset($v[(string)$i][$i_date]['draw']) && $v[(string)$i][$i_date]['distro'] == $s_distro)
									$s_retval .= '
										'.$v[(string)$i][$i_date]['draw'];
							}
						}
					} else {
						foreach($a_date_keys as $i_date)
							if (isset($v[(string)$i][$i_date]['draw']))
								$s_retval .= '
									'.$v[(string)$i][$i_date]['draw'];
					}
					$s_retval .= '
						</table><br />';
				}
			}

			// display the feedback
			foreach($a_string_lists as $k=>&$v) {
				$s_retval .= '
					<a style="color:black;font-weight:bold;" id="'.$k.'">'.$k.'</a>:<br />';
				for($i = 1; $i <= 5; $i++)
					$s_retval .= '
						<a style="cursor:pointer;" onclick="satisfactionSurveyScrollTop($(\'#'.$k.$i.'\'));">Value '.$i.'</a><br />';
				$s_retval .= '
					<a style="cursor:pointer;" onclick="satisfactionSurveyScrollTop($(\'#satisfactionSurveyTop\'));">Back to top</a><br />
					<br />';
				for($i = 1; $i <= 5; $i++) {
					$s_retval .= '
						<a style="color:black;font-weight:bold;" id="'.$k.$i.'">Value  '.$i.'</a>:<br />
						<a style="cursor:pointer;" onclick="satisfactionSurveyScrollTop($(\'#'.$k.'\'));">'.$k.'</a><br />
						<br />';
					foreach($a_date_keys as $i_date) {
						if (!isset($v[(string)$i][$i_date]))
							continue;
						$a_feedback = $v[(string)$i][$i_date];
						$s_retval .= '
							<font style="font-weight:bold;">'.$a_feedback['dataname'].'</font>: '.$a_feedback['feedback'].'<br />';
					}
					$s_retval .= '
						<br />';
				}
			}

			return $s_retval;
		}

		function getArrayVal($s_needle, &$a_haystack, $wt_default = '') {
			return (isset($a_haystack[$s_needle])) ? $a_haystack[$s_needle] : $wt_default;
		}

		// options for $a_extra_vars:
		// * start_date (only includes restaurants signed up before this datetime)
		// * end_date (only includes restaurants with last activity after this datetime)
		//
		function getActiveDatanames($b_include_lavulite = FALSE, $a_extra_vars = NULL, $b_new_way = FALSE) {

			$s_options = ($b_include_lavulite ? "TRUE\n" : "FALSE\n").print_r($a_extra_vars,TRUE).($b_new_way ? "TRUE\n" : "FALSE\n");
			if (isset($this->activeDatanames[$s_options]))
				return $this->activeDatanames[$s_options];
			global $maindb;

			// set some default values
			$lavu_lite_string = ($b_include_lavulite) ? '' : "AND `restaurants`.`lavu_lite_server` = ''";
			$b_check_deleted = TRUE;
			$b_check_hosting_status = TRUE;
			$b_check_license_status = TRUE;
			$b_check_by_state = FALSE;
			$b_state_to_check = FALSE;

			$b_check_by_region = FALSE;
			$b_region_to_check = FALSE;

			$b_check_by_lic_level = FALSE;
			$b_lic_level_to_check = FALSE;
			$b_include_testing_demo = FALSE;
			$b_created_time_filter = FALSE;
			$b_check_by_distro_level = FALSE;
			$b_include_portal_fields = FALSE;

			// start_date and end_date are meant to be used to select a slice from the history of Lavu,
			// and determine if an account was created before start_date, and survived past end_date
			$s_start_date = date('Y-m-d H:i:s', strtotime("+1 day"));
			$s_end_date = '2010-05-01 00:00:00';

			if (is_array($a_extra_vars) && count($a_extra_vars) > 0) {
				$s_start_date = $this->getArrayVal('start_date', $a_extra_vars, $s_start_date);
				$s_end_date = $this->getArrayVal('end_date', $a_extra_vars, $s_end_date);
				$b_created_time_filter = $this->getArrayVal('created_filter', $a_extra_vars, $b_created_time_filter);
				$s_created_start = $this->getArrayVal('created_start', $a_extra_vars, $b_created_time_filter);
				$s_created_end = $this->getArrayVal('created_end', $a_extra_vars, $b_created_time_filter);
				//
				$b_check_deleted = (bool)$this->getArrayVal('check_deleted', $a_extra_vars, $b_check_deleted);
				//
				if($b_check_deleted){
					error_log("MMMMMM-----checking active with disabled:: check deleted:: ".$b_check_deleted);
				}
				$b_check_hosting_status = (bool)$this->getArrayVal('check_hosting_status', $a_extra_vars, $b_check_hosting_status);
				$b_check_license_status = (bool)$this->getArrayVal('check_license_status', $a_extra_vars, $b_check_license_status);

				$b_check_by_state = (bool)$this->getArrayVal('check_by_state', $a_extra_vars, $b_check_by_state);
				$b_state_to_check = $this->getArrayVal('state_to_check', $a_extra_vars, $b_state_to_check);

				$b_check_by_region = (bool)$this->getArrayVal('check_by_region', $a_extra_vars, $b_check_by_region);
				$b_region_to_check = $this->getArrayVal('region_to_check', $a_extra_vars, $b_region_to_check);
				$b_with_package_level= $this->getArrayVal('with_package_level',$a_extra_vars, $b_with_package_level);

				$b_check_by_lic_level = (bool)$this->getArrayVal('check_by_lic_level', $a_extra_vars, $b_check_by_lic_level);
				$b_lic_level_to_check = $this->getArrayVal('lic_level_to_check', $a_extra_vars, $b_lic_level_to_check);
				$b_include_testing_demo = (bool)$this->getArrayVal('include_testing_demo', $a_extra_vars, $b_include_testing_demo);
				$b_check_by_distro_level = (bool)$this->getArrayVal('check_by_distro_level', $a_extra_vars, $b_check_by_distro_level);
				$s_distro_level_to_check = $this->getArrayVal('distro_level_to_check', $a_extra_vars, $b_check_by_distro_level);

				$b_include_portal_fields = (bool)$this->getArrayVal('portal_report_fields', $a_extra_vars, $b_check_by_distro_level);

			}
			//echo "with package level is: ". $this->getArrayVal('with_package_level',$a_extra_vars, $b_with_package_level);
			// check for deleted in the notes and disabled equal to 0
			$s_disabled = $b_check_deleted ? "AND `restaurants`.`disabled`='0'" : '';
			$s_deleted = $b_check_deleted ? "AND `restaurants`.`notes` NOT LIKE '%DELETED%'" : '';
			$s_hosting_status = $b_check_hosting_status ? "(`arb_hosting_status` = 'active' OR `arb_hosting_status` = '')" : "";
			$s_license_status = $b_check_license_status ? "(`arb_license_status` = 'active' OR `arb_license_status` = '')" : "";
			$s_by_state = $b_check_by_state ? "AND `signups`.`state`='$b_state_to_check'" :"";
			$s_by_region= $b_check_by_region ? "AND `signups`.`state` {$b_region_to_check}" :"";
			$s_by_lic_level = $b_check_by_lic_level ? "AND (`signups`.`package`='$b_lic_level_to_check' OR `signups`.`package`='1$b_lic_level_to_check' OR `signups`.`package`='2$b_lic_level_to_check')" :"";
			$s_created_time_filter = $b_created_time_filter ? "WHERE `restaurants`.`created`> '$s_created_start' AND `restaurants`.`created`< '$s_created_end'" : "WHERE `restaurants`.`created`<'[start_date]'";
			$s_show_package = $b_with_package_level ? true:false;
			$s_testing_demo = (!$b_include_testing_demo) ? "AND `restaurants`.`test`='0' AND `restaurants`.`hosting_status` NOT LIKE '%DEV%' AND `restaurants`.`license_status` NOT LIKE '%DEV%' AND `restaurants`.`package_status` NOT LIKE '%DEV%' AND `restaurants`.`demo` = '0' AND `restaurants`.`dev` = '0' AND `restaurants`.`company_name` NOT LIKE '%test%'" : "";
			$s_testing_demo_package_and_trial = (!$b_include_testing_demo) ? "AND ((`signups`.`package` != '' AND `signups`.`package` != 'none') OR `payment_status`.`trial_end`='')" : "";
			if($b_check_by_distro_level){
				$field_array = array();
				$field_array['reseller_db'] = '`poslavu_MAIN_db`.`resellers`';
				$field_array['restaurants_db'] = '`poslavu_MAIN_db`.`restaurants`';

				if($s_distro_level_to_check == 'UK')
				{
					//$field_array['country'] = "United Kingdom' or `country`= 'Great Britain'";
					$q_distro_level_string = "select `username` from [reseller_db] where `country` = 'United Kingdom' or `country`= 'Great Britain'";
				}else{
					$field_array['d_level'] = $s_distro_level_to_check;
					$field_array['country'] = 'United States';
					$q_distro_level_string = "select `username` from [reseller_db] where `access`='[d_level]' and `country` = [country]";
				}
				$distro_query = mlavu_query($q_distro_level_string, $field_array); // poslavu_MAIN_db

				//error_log('active UK biscuit eaters:: '.ConnectionHub::getConn('poslavu')->getDBAPI()->drawQuery($q_distro_level_string,$field_array));
				$a_distro_usernames = array();
				$a_distro_usernames_ds = array();
				while($row = mysqli_fetch_assoc($distro_query))
				{
					$a_distro_usernames[$row['username']] = $row['username'];
					$a_distro_usernames_ds["DS_".$row['username']] = "DS_".$row['username'];
				}
				$s_distro_usernames = ConnectionHub::getConn('poslavu')->getDBAPI()->arrayToInClause($a_distro_usernames);

				$q_string_rest_id = "select id from [restaurants_db] where `distro_code` {$s_distro_usernames}";
				$q_result_rest_id = mlavu_query($q_string_rest_id, $field_array); // poslavu_MAIN_db
				while($row = mysqli_fetch_assoc($q_result_rest_id)){
					$a_distro_usernames_ds[$row['id']] = $row['id'];
				}
				$s_distro_usernames_ds = ConnectionHub::getConn('poslavu')->getDBAPI()->arrayToInClause($a_distro_usernames_ds);
				$s_distro_level_to_check = " AND `match_restaurantid` {$s_distro_usernames_ds}";
				//error_log("active clause for distro level = ".$s_distro_level_to_check);
			}else
			{
				$s_distro_level_to_check = "";

			}
			// do a query to find active restaurants (mostly accurate)
			// checks created time, last activity time, disabled flag, 'deleted' in notes, test flag, 'dev' in hosting status,
			// 'dev' in license status, 'dev' in package status, demo flag, dev flag, 'test' in company name, and lavu lite server
			$a_query_vars = array("maindb"=>$maindb, 'start_date'=>$s_start_date, 'end_date'=>$s_end_date);
			$s_restaurants = "$s_created_time_filter AND `data_name` != '' $lavu_lite_string $s_disabled $s_deleted $s_testing_demo";
			$s_signups = "$s_by_state $s_by_lic_level $s_by_region";
			//error_log("signups in clause".$s_signups);
			$s_payment_status = "(`payment_status`.`license_applied` !='' AND `payment_status`.`license_applied` !='payment_link')";
			$s_payment_responses = "(`payment_responses`.`x_response_code`='1')";
			//if($b_include_portal_fields){
			//	$s_query_string ="SELECT DISTINCT `restaurants`.`data_name` , `restaurants`.`distro_code` , `signups`.`state`  FROM `poslavu_MAIN_db`.`payment_responses` LEFT JOIN `poslavu_MAIN_db`.`restaurants` ON `payment_responses`.`match_restaurantid`=`restaurants`.`id` LEFT JOIN `poslavu_MAIN_db`.`signups` ON `restaurants`.`data_name`=`signups`.`dataname` LEFT JOIN `poslavu_MAIN_db`.`payment_status` ON `signups`.`dataname`=`payment_status`.`dataname` $s_restaurants AND ($s_payment_status OR $s_payment_responses) $s_distro_level_to_check $s_signups $s_testing_demo_package_and_trial";
			//}else{
			$get_pack='';
			if($s_show_package)
				$get_pack = ",`signups`.`package`";
			$s_query_string ="SELECT DISTINCT `restaurants`.`data_name` $get_pack FROM `poslavu_MAIN_db`.`payment_responses` LEFT JOIN `poslavu_MAIN_db`.`restaurants` ON `payment_responses`.`match_restaurantid`=`restaurants`.`id` LEFT JOIN `poslavu_MAIN_db`.`signups` ON `restaurants`.`data_name`=`signups`.`dataname` LEFT JOIN `poslavu_MAIN_db`.`payment_status` ON `signups`.`dataname`=`payment_status`.`dataname` $s_restaurants AND ($s_payment_status OR $s_payment_responses) $s_distro_level_to_check $s_signups $s_testing_demo_package_and_trial";
			//}

			$active_restaurants_query = mlavu_query($s_query_string, $a_query_vars); // poslavu_MAIN_db
			// check that the query is good
			if ($active_restaurants_query === FALSE) {
				error_log("bad query (manage/misc/advancedtoolsfuncitons/companyreporting.php), getActiveDatanames");
				file_put_contents(dirname(__FILE__)."/stuff.txt", ConnectionHub::getConn('poslavu')->getDBAPI()->drawQuery($s_query_string,$a_query_vars));
				return array();
			}
			//if($b_check_deleted)
			error_log("active reports query:::  ".ConnectionHub::getConn('poslavu')->getDBAPI()->drawQuery($s_query_string,$a_query_vars));
			if (mysqli_num_rows($active_restaurants_query) == 0)
				return array();

			// collect the data in a succinct manner and free the mysql query
			$a_retval = array();
			//echo $s_query_string. "\n\n";
			while($active_restaurants_read = mysqli_fetch_assoc($active_restaurants_query))
				if (!in_array($active_restaurants_read['data_name'], $a_retval)){
					if($s_show_package)
						$a_retval[] = array("data_name"=>$active_restaurants_read['data_name'], "package_level"=>$active_restaurants_read['package']);
					else
						$a_retval[] = $active_restaurants_read['data_name'];
				}
			mysqli_free_result($active_restaurants_query);
			if (!isset($this->activeDatanames))
				$this->activeDatanames = array();
			$this->activeDatanames[$s_options] = $a_retval;
			return $a_retval;
		}// end getActiveDatanames()


		//total sold licenses
		//
		function getTotalLicenses($b_include_lavulite = FALSE, $a_extra_vars = NULL){
			if (count($this->totalSoldLicenses) > 0)
				return $this->totalSoldLicenses;

			global $maindb;

			// set some default values
			//$lavu_lite_string = ($b_include_lavulite) ? '' : "AND `lavu_lite_server` = ''";
			$b_check_by_state = FALSE;
			$b_check_by_region = FALSE;
			$b_check_by_country = FALSE;
			$b_state_to_check = FALSE;
			$b_region_to_check = FALSE;
			$b_country_to_check = FALSE;
			$b_check_by_lic_level = FALSE;
			$b_lic_level_to_check = FALSE;
			$b_created_time_filter = FALSE;
			$b_check_only_distro = FALSE;
			$b_check_by_distro_level = FALSE;

			// start_date and end_date are meant to be used to select a slice from the history of Lavu,
			// and determine if an account was created before start_date, and survived past end_date
			$s_start_date = date('Y-m-d H:i:s', strtotime("+1 day"));
			$s_end_date = '2010-05-01 00:00:00';

			// Created start/end are for active accounts that were started between the created start/end dates
			//
			//$s_created_start = '2010-05-01 00:00:00';
			//$s_created_end = date('Y-m-d H:i:s', strtotime("+1 day"));
			//$b_created_start = FALSE;
			//$s_created_end = FALSE;

			if (is_array($a_extra_vars) && count($a_extra_vars) > 0) {
				$s_start_date = $this->getArrayVal('start_date', $a_extra_vars, $s_start_date);
				$s_end_date = $this->getArrayVal('end_date', $a_extra_vars, $s_end_date);
					$b_created_time_filter = $this->getArrayVal('created_filter', $a_extra_vars, $b_created_time_filter);
					$s_created_start = $this->getArrayVal('created_start', $a_extra_vars, $b_created_time_filter);
					$s_created_end = $this->getArrayVal('created_end', $a_extra_vars, $b_created_time_filter);
					//$s_no_created_filter = $this->getArrayVal('created_start', $a_extra_vars, $b_created_time_filter);
					//error_log("companyReporting totalLicense time check:: filter:: ".$b_created_time_filter.", s:: ".$s_created_start.", e:: ".$s_created_end);
				$b_check_by_state = (bool)$this->getArrayVal('check_by_state', $a_extra_vars, $b_check_by_state);
				$b_state_to_check = $this->getArrayVal('state_to_check', $a_extra_vars, $b_state_to_check);

				$b_check_by_region = (bool)$this->getArrayVal('check_by_region', $a_extra_vars, $b_check_by_region);
				$s_region_to_check = $this->getArrayVal('region_to_check', $a_extra_vars, $b_region_to_check);

				$b_check_by_country = (bool)$this->getArrayVal('check_by_state', $a_extra_vars, $b_check_by_state);
				$s_country_to_check = $this->getArrayVal('state_to_check', $a_extra_vars, $b_state_to_check);

                //error_log("s_region_to_check:: ".$s_region_to_check);
				$b_check_only_distro = (bool)$this->getArrayVal('check_only_distro', $a_extra_vars, $b_check_only_distro);
				$b_check_by_lic_level = (bool)$this->getArrayVal('check_by_lic_level', $a_extra_vars, $b_check_by_lic_level);

				$b_lic_level_to_check = $this->getArrayVal('lic_level_to_check', $a_extra_vars, $b_lic_level_to_check);

				$b_check_by_distro_level = (bool)$this->getArrayVal('check_by_distro_level', $a_extra_vars, $b_check_by_distro_level);
				$s_distro_level_to_check = $this->getArrayVal('distro_level_to_check', $a_extra_vars, $b_check_by_distro_level);
			}
			//by distributor level
			if($b_check_by_distro_level)
			{
				$field_array = array();
				$field_array['reseller_db'] = '`poslavu_MAIN_db`.`resellers`';
				$field_array['restaurants_db'] = '`poslavu_MAIN_db`.`restaurants`';
				

				if($s_distro_level_to_check == 'UK')
				{
					//$field_array['country'] = "United Kingdom' or `country`= 'Great Britain'";
					$q_distro_level_string = "select `username` from [reseller_db] where `country` = 'United Kingdom' or `country`= 'Great Britain'";
				}else{
					$field_array['d_level'] = $s_distro_level_to_check;
					$field_array['country'] = 'United States';
					$q_distro_level_string = "select `username` from [reseller_db] where `access`='[d_level]' and `country` = [country]";
				}

				
				$distro_query = mlavu_query($q_distro_level_string, $field_array); // poslavu_MAIN_db

				//error_log('license by distributor type _query:: '.ConnectionHub::getConn('poslavu')->getDBAPI()->drawQuery($q_distro_level_string,$field_array));
				$a_distro_usernames = array();
				$a_distro_usernames_ds = array();
				while($row = mysqli_fetch_assoc($distro_query))
				{
					$a_distro_usernames[$row['username']] = $row['username'];
					$a_distro_usernames_ds["DS_".$row['username']] = "DS_".$row['username'];
				}
				$s_distro_usernames = ConnectionHub::getConn('poslavu')->getDBAPI()->arrayToInClause($a_distro_usernames);

				$q_string_rest_id = "select id from [restaurants_db] where `distro_code` {$s_distro_usernames}";
				$q_result_rest_id = mlavu_query($q_string_rest_id, $field_array); // poslavu_MAIN_db
				while($row = mysqli_fetch_assoc($q_result_rest_id)){
					$a_distro_usernames_ds[$row['id']] = $row['id'];
				}
				$s_distro_usernames_ds = ConnectionHub::getConn('poslavu')->getDBAPI()->arrayToInClause($a_distro_usernames_ds);
				$s_distro_level_to_check = " AND `match_restaurantid` {$s_distro_usernames_ds}";
				//error_log("clause for type = ".$s_distro_level_to_check);
			}else
			{
				$s_distro_level_to_check = "";

			}
			// check for deleted in the notes and disabled equal to 0
			$s_disabled = $b_check_deleted ? "AND `disabled`='0'" : '';
			$s_deleted = $b_check_deleted ? "AND `notes` NOT LIKE '%DELETED%'" : '';
			$s_hosting_status = $b_check_hosting_status ? "(`arb_hosting_status` = 'active' OR `arb_hosting_status` = '')" : "";
			$s_license_status = $b_check_license_status ? "(`arb_license_status` = 'active' OR `arb_license_status` = '')" : "";
			$s_by_state = $b_check_by_state ? "AND `x_state`='$b_state_to_check'" :"";
			$s_by_region = $b_check_by_region ? "AND `x_state` {$s_region_to_check}" :"";

			$s_by_country = $b_check_by_country ? $s_country_to_check :"";

			$s_lic_level = $b_check_by_lic_level ? "AND $b_lic_level_to_check" :"and `x_amount` > 300 and `x_amount` < 3000";

			$s_created_time_filter = $b_created_time_filter ? " AND `datetime`> '$s_created_start' AND `datetime`< '$s_created_end'":" AND `datetime`<'[end_date]'";
			$s_distro_licenses_only = $b_check_only_distro ? "and LEFT(`match_restaurantid`,2)='DS'" :"";
			//and LEFT(`match_restaurantid`,2)='DS'
			//$s_no_created_time_filter = $b_created_time_filter ? "":"WHERE `created`<'[start_date]' AND `last_activity`>'[end_date]";

			//$s_no_created_filter = '`created`<'[start_date]' AND `last_activity`>'[end_date]';

			/*if($b_check_by_region){
				error_log('martin-- '.$s_by_region);
			}*/
			//error_log("s_by_lic_level::  ".$s_by_lic_level);

			//$s_query_string ="SELECT `restaurants`.`data_name` FROM ((SELECT * FROM `poslavu_MAIN_db`.`restaurants` $s_created_time_filter AND `data_name` != '' $s_disabled $s_deleted $s_testing_demo)) as `restaurants` LEFT JOIN ((SELECT `state`,`package`,`restaurantid`,`dataname` FROM `poslavu_MAIN_db`.`signups` ORDER BY `id` DESC)) `sdb` ON (`restaurants`.`data_name`=`sdb`.`dataname`) LEFT JOIN ((select `dataname`, `restaurantid`,`license_applied` from `poslavu_MAIN_db`.`payment_status`)) `sdb2` on (`sdb`.`dataname`=`sdb2`.`dataname`)LEFT JOIN((select distinct `match_restaurantid` as `match_id`, `x_response_code`, `x_amount` from `poslavu_MAIN_db`.`payment_responses`))`sdb3` on (`restaurants`.`id`=`sdb3`.`match_id`)  where ((`license_applied` !='' and `license_applied` !='payment_link') or (`sdb3`.`x_response_code`='1')) $s_by_state $s_by_lic_level ";

			$s_query_string = "select `id`, `match_restaurantid`, `x_state`, `datetime`, `x_type`,`x_amount`, `x_description` from `poslavu_MAIN_db`.`payment_responses` where `x_response_code`='1' and `match_restaurantid` != 'DS_martin' and `match_restaurantid` != 'x' $s_distro_level_to_check $s_by_state $s_by_region $s_created_time_filter $s_lic_level $s_distro_licenses_only order by id desc";


			//include distro code 'martin'
			//$s_query_string = "select `id`, `match_restaurantid`, `x_state`, `datetime`, `x_type`,`x_amount` from `poslavu_MAIN_db`.`payment_responses` where `match_restaurantid` != 'x' and $s_created_time_filter $s_lic_level $s_distro_licenses_only order by id desc";







			$a_query_vars = array("maindb"=>$maindb, 'start_date'=>$s_end_date, 'end_date'=>$s_start_date);
			$active_restaurants_query = mlavu_query($s_query_string, $a_query_vars); // poslavu_MAIN_db
			//if(!$b_check_by_region)
			//{
			error_log(mysqli_num_rows($active_restaurants_query).', total license query:: '.ConnectionHub::getConn('poslavu')->getDBAPI()->drawQuery($s_query_string,$a_query_vars));
			//}


			/*
			if($b_created_time_filter){
				error_log('created filter-- '.ConnectionHub::getConn('poslavu')->getDBAPI()->drawQuery($s_query_string,$a_query_vars));
			}else{
				error_log('no created filter-- '.ConnectionHub::getConn('poslavu')->getDBAPI()->drawQuery($s_query_string,$a_query_vars));
			}
			*/

			// check that the query is good
			if ($active_restaurants_query === FALSE) {
				error_log("bad query (manage/misc/advancedtoolsfuncitons/companyreporting.php), getActiveDatanames");
				return array();
			}
			if (mysqli_num_rows($active_restaurants_query) == 0)
				return array();

			// collect the data in a succinct manner and free the mysql query
			$a_retval = array();
			while($active_restaurants_read = mysqli_fetch_assoc($active_restaurants_query)){
				$match_restaurantid = $active_restaurants_read['id'];
				//if (!in_array($match_restaurantid, $a_retval))
				$a_retval[$match_restaurantid]['match_restaurantid'] = $active_restaurants_read['match_restaurantid'];
				$a_retval[$match_restaurantid]['x_state'] = $active_restaurants_read['x_state'];
				$a_retval[$match_restaurantid]['x_type'] = $active_restaurants_read['x_type'];
				$a_retval[$match_restaurantid]['x_amount'] = $active_restaurants_read['x_amount'];
				$a_retval[$match_restaurantid]['datetime'] = $active_restaurants_read['datetime'];
				//$a_retval['query_check']['query'] = ConnectionHub::getConn('poslavu')->getDBAPI()->drawQuery($s_query_string,$a_query_vars);
			}
			mysqli_free_result($active_restaurants_query);
			$this->totalSoldLicenses = $a_retval;
			return $this->totalSoldLicenses;

		}// end getTotalLicenses

		function drawAlertAccountsByNoteCount() {
			global $maindb;

			$a_active_datanames = $this->getActiveDatanames();
			if (count($a_active_datanames) == 0)
				return 'there were no active accounts found';

			$possible_problems_query = mlavu_query("SELECT `data_name`,`created_on`,COUNT(`id`) AS `count`,`account_created` FROM ((SELECT * FROM `[maindb]`.`restaurant_notes` WHERE `created_on` > '[three_months_ago]' ORDER BY `created_on` DESC)) `ndb` LEFT JOIN ((SELECT `id` AS `r_id`,`data_name`,`created` AS `account_created` FROM `[maindb]`.`restaurants`)) `rdb` ON (`ndb`.`restaurant_id` = `rdb`.`r_id`) GROUP BY `restaurant_id`",
				array('maindb'=>$maindb, 'three_months_ago'=>date('-3 months'))); // poslavu_MAIN_db
			if ($possible_problems_query === FALSE) {
				error_log('bad restaurant notes query in '.__FILE__);
				return 'bad query';
			}
			if (mysqli_num_rows($possible_problems_query) == 0)
				return 'no problem restaurants were found';
			$a_problem_retaurants = array();
			while($row = mysqli_fetch_assoc($possible_problems_query))
				if ($row['count'] >= 3 && in_array($row['data_name'],$a_active_datanames))
					$a_problem_restaurants[] = $row;
			mysqli_free_result($possible_problems_query);

			require_once(dirname(__FILE__).'/../autoTable.php');
			$s_script = '<script type="text/javascript">
				function load_company_search_from_dataname() {
					lavuLog(s_global_dataname);
					populateResults(s_global_dataname, function(){
						$("#company_search_button").click();
						doneLoading();
					});
				}
			</script>';
			$s_onclick = "
						s_global_dataname=$( $(this).find('td')[0] ).text();
						load_company_search_from_dataname();
						setTimeout('load_company_search_from_lls_list();', 200);
					";
			$a_headers = array('Dataname', 'Last Note Time', 'Note Count', 'Account Created');
			$s_table = auto_build_table($a_headers, $a_problem_restaurants, $s_onclick, 'possibleProblemRestaurantsTable');

			return $s_script.$s_table;
		}

		function getLLSDatanames() {
			$i_today = strtotime(date('Y-m-d'));
			$i_yesterday = $i_today-1;
			$a_checked_in_today = ConnectionHub::getConn('poslavu')->getDBAPI()->getAllInTable('local_servers', array('yesterday'=>$i_yesterday), TRUE,
				array('selectclause'=>'`data_name`', 'whereclause'=>"WHERE `last_checked_in`>='[yesterday]'"));
			$a_retval = array();
			if (count($a_checked_in_today) > 0)
				foreach($a_checked_in_today as $a_row)
					$a_retval[$a_row['data_name']] = $a_row['data_name'];
			return $a_retval;
		}

		function drawActiveDatanames() {
			$a_active_datanames = $this->getActiveDatanames(FALSE, NULL, TRUE);
			$a_lls_datanames = $this->getLLSDatanames();
			$s_retval = '<font style="font-weight:bold;">Total: '.count($a_active_datanames).'</font><br /><br />';
			$s_retval .= '<font style="font-weight:bold;">Bolded</font><font> datanames have an active local server (their LLS checked in today)</font><br /><br />';
			foreach($a_active_datanames as $s_dataname)
				if (!isset($a_lls_datanames[$s_dataname]))
					$s_retval .= $s_dataname.'<br />';
				else
					$s_retval .= '<font style="font-weight:bold;">'.$s_dataname.'</font><br />';
			return $s_retval;
		}

		function drawDeviceStatistics() {
			$o_device_stats = new device_statistics_object($this->getActiveDatanames());
			return $o_device_stats->draw_stats();
		}

		function getLavuLiteStats(){
			if ($this->lavuLiteStats != '')
				return $this->lavuLiteStats;
			$key= md5('lavu');
			$url='http://admin.lavulite.com/mainconnect/manageConnect/getLavuLiteInfo.php';
			$this->lavuLiteStats = $this->performCURL($url, array('fromManage'=>$key));
			return $this->lavuLiteStats;
		}

		function drawLavuLiteStats() {
			return $this->getLavuLiteStats();
		}

		function drawLeadsStatusSingleDate($a_params) {
			global $maindb;

			// these are the values allowable in $a_params
			$containerId = isset($a_params['leadStatusContainerID']) ? $a_params['leadStatusContainerID'] : '';
			$a_dates_ids = $a_params['a_dates_ids'] ? $a_params['a_dates_ids'] : NULL;
			$starttime = (int)$a_params['starttime'] ? $a_params['starttime'] : 0;
			$href_id = $a_params['href_id'] ? $a_params['href_id'] : '';

			// static styling strings
			$s_anchor_style = 'color:black;font-weight:bold;';
			$s_href_link_style = 'cursor:pointer;';
			$s_scroll_top = '<a onclick="elementScrollTop($(\'#'.$containerId.'top\'),$(\'#'.$containerId.'\'));" style="'.$s_href_link_style.'">Back to top</a>';

			// these are the statistics that will be shown
			$a_time_stats = array('accepted','contacted','made_demo_account','license_applied');
			$s_time_select_clause = '`'.implode('`,`', $a_time_stats).'`';
			$a_time_counts = array();
			foreach($a_time_stats as $s_time_name)
				$a_time_counts[$s_time_name] = 0;

			// get the data
			// looks for all leads created or assigned since $starttime
			$leads_status_query_string = "SELECT [select_clause],`distro_code` FROM `[maindb]`.`reseller_leads` WHERE (`assigned_time`='0000-00-00 00:00:00' AND `created`>'[startdate]') OR (`assigned_time`>'[startdate]' AND `created`='0000-00-00 00:00:00') OR (`accepted`>'[startdate]' AND `assigned_time`='0000-00-00 00:00:00' AND `created`='0000-00-00 00:00:00')";
			$leads_status_query_vars = array('maindb'=>$maindb, 'startdate'=>date('Y-m-d H:i:s', $starttime), 'select_clause'=>$s_time_select_clause);
			$leads_status_query = mlavu_query($leads_status_query_string, $leads_status_query_vars); // poslavu_MAIN_db
			if ($leads_status_query === FALSE)
				return '
						<a id="'.$href_id.'" style="'.$s_anchor_style.'">bad leads query: '.__FILE__.'</a><br />
						'.$s_scroll_top.'<br />';
			if (mysqli_num_rows($leads_status_query) == 0)
				return '
						<a id="'.$href_id.'" style="'.$s_anchor_style.'">no leads found for the given date</a><br />
						'.$s_scroll_top.'<br />';

			// analyze the data
			$i_total = 0;
			$i_assigned = 0;
			while ($a_lead = mysqli_fetch_assoc($leads_status_query)) {
				$i_total++;
				if ($a_lead['distro_code'] !== '')
					$i_assigned++;
				foreach($a_time_stats as $s_time_name)
					if ($a_lead[$s_time_name] != '0000-00-00 00:00:00')
						$a_time_counts[$s_time_name]++;
			}

			// print the data for total leads and those assigned
			$s_retval = '';
			$s_retval .= '
						<a id="'.$href_id.'" style="'.$s_anchor_style.'">'.ucwords(implode(' ', explode('_', $href_id))).'</a><br />
						'.$s_scroll_top.'<br />
						<br />
						Total: '.$i_total.'<br />
						Assigned: '.$i_assigned.' ('.(int)($i_assigned/max($i_total,1)*100).'%)<br />';

			// print the other time related data
			foreach($a_time_counts as $s_time_name=>$i_time_count) {
				$s_name = ucwords(implode(' ', explode('_', $s_time_name)));
				$s_retval .= '
						'.$s_name.': '.$i_time_count.' ('.(int)($i_time_count/max($i_total,1)*100).'%)<br />';
			}

			return $s_retval;
		}

		// @$starttime should be an integer in the form of time()
		// returns a string representing the leads status for all leads created after $starttime
		function drawLeadsStatus($a_params) {
			$containerId = isset($a_params['leadStatusContainerID']) ? $a_params['leadStatusContainerID'] : '';
			$a_dates_ids = $a_params['a_dates_ids'] ? $a_params['a_dates_ids'] : NULL;
			$starttime = (int)$a_params['starttime'] ? $a_params['starttime'] : 0;
			$s_href_link_style = 'cursor:pointer;';

			// recursively call for multiple dates
			if ($starttime == 0 || is_array($a_dates_ids)) {
				if (!is_array($a_dates_ids))
					$a_dates_ids = array(
						array('date'=>strtotime('-7 days'), 'id'=>'one_week_ago'),
						array('date'=>strtotime('-14 days'), 'id'=>'two_weeks_ago'),
						array('date'=>strtotime('-30 days'), 'id'=>'one_month_ago')
					);
				$s_retval = '
						<a id=\''.$containerId.'top\' style=\'visibility:hidden;\'>&nbsp;</a><br />';
				foreach($a_dates_ids as $a_date_id)
					$s_retval .= '
						<a onclick="elementScrollTop($(\'#'.$a_date_id['id'].'\'), $(\'#'.$containerId.'\'));" style="'.$s_href_link_style.'">'.ucwords(implode(' ', explode('_', $a_date_id['id']))).'</a><br />';
				$s_retval .= '
						<br />';
				foreach($a_dates_ids as $a_date_id)
					$s_retval .= '
						'.$this->drawLeadsStatusSingleDate(array('leadStatusContainerID'=>$containerId, 'starttime'=>$a_date_id['date'], 'href_id'=>$a_date_id['id'])).'
						<br />';
				return $s_retval;
			}

			return $this->drawLeadsStatusSingleDate($a_params);
		}

		function drawOutdatedLocationConfigSettings() {
			require_once(dirname(__FILE__).'/company_reporting_server_status.php');
			return serverStatus::drawOutdatedLocationConfigSettings();
		}

		//Active in Billing_profile table, Disabled in restaurants table
		function drawAbdaAccounts(){
			$a_abda_accounts = $this->getAbdaAccounts();
			$s_retval = '<font style="font-weight:bold;">Total: '.count($a_abda_accounts).'</font><br /><br />';
			$s_retval .= '<font> Active datanames in billing_profiles table that are disabled in restaurants table.</font><br /><br />';
			foreach($a_abda_accounts as $key=>$dname){
				//error_log("companyReporting::: ".$key.", ".$dname['data_name']);
				$s_retval .= $dname['data_name'].'<br />';
			}
			return $s_retval;
		}
		function drawCancelReasons(){
			$this->getCancelReasons();
		}//drawCancelReasons
		function getLavuStats(){

		}

		function getGeographicStats(){
			require_once($_SERVER['DOCUMENT_ROOT']."/newcp/backend/sandbox/geoLocationInfo/geoLocator.php");
			$info= getInfo();
			return $info;
		}


		function forceLockoutCheck(){

		}

		function getOldAccounts(){
			$key= md5('lavu');
			$url="http://admin.lavulite.com/mainconnect/manageConnect/showUnusedAccounts.php";
			$result= performCURL($url, array("fromManage"=>$key));
			return $result;
		}
		function getAbdaAccounts(){
			global $maindb;
			$a_query_vars = array();
			$a_query_vars['db_billing_profiles'] = '`poslavu_MAIN_db`.`billing_profiles`';
			$a_query_vars['db_restaurants'] = '`poslavu_MAIN_db`.`restaurants`';
			//$s_query_string ="SELECT DISTINCT `restaurants`.`data_name` $get_pack FROM `poslavu_MAIN_db`.`payment_responses` LEFT JOIN `poslavu_MAIN_db`.`restaurants` ON `payment_responses`.`match_restaurantid`=`restaurants`.`id` LEFT JOIN `poslavu_MAIN_db`.`signups` ON `restaurants`.`data_name`=`signups`.`dataname` LEFT JOIN `poslavu_MAIN_db`.`payment_status` ON `signups`.`dataname`=`payment_status`.`dataname` $s_restaurants AND ($s_payment_status OR $s_payment_responses) $s_distro_level_to_check $s_signups $s_testing_demo_package_and_trial";
			$s_query_string = "select [db_billing_profiles].`dataname` FROM [db_billing_profiles] LEFT JOIN [db_restaurants] ON [db_billing_profiles].`dataname`=[db_restaurants].`data_name` where [db_billing_profiles].`active`='1' and ([db_restaurants].`disabled`='1' or [db_restaurants].`disabled`='2' or [db_restaurants].`disabled`='3') and [db_restaurants].`data_name`!='' and [db_billing_profiles].`last_pay_date`>'2014-01-01'";
			$s_q_check = ConnectionHub::getConn('poslavu')->getDBAPI()->drawQuery($s_query_string,$a_query_vars);
			//error_log("manage/misc/AdvancedToolsFunctions/companyReporting.php--line 1342:: ".$s_q_check);

			$q_abda_results = mlavu_query($s_query_string, $a_query_vars);
			$a_return_val = array();
			while($abda_result = mysqli_fetch_assoc($q_abda_results)){
				$a_return_val[] = array("data_name"=>$abda_result['dataname']);
			}
			return $a_return_val;
		}//getAbdaAccounts
		function getCancelReasons(){
			global $maindb;
			$a_query_vars = array();
			$a_query_vars['db_payment_status'] = '`poslavu_MAIN_db`.`payment_status`';
			$a_query_vars['db_restaurants'] = '`poslavu_MAIN_db`.`restaurants`';
			//$s_query_string ="SELECT DISTINCT `restaurants`.`data_name` $get_pack FROM `poslavu_MAIN_db`.`payment_response` LEFT JOIN `poslavu_MAIN_db`.`restaurants` ON `payment_responses`.`match_restaurantid`=`restaurants`.`id` LEFT JOIN `poslavu_MAIN_db`.`signups` ON `restaurants`.`data_name`=`signups`.`dataname` LEFT JOIN `poslavu_MAIN_db`.`payment_status` ON `signups`.`dataname`=`payment_status`.`dataname` $s_restaurants AND ($s_payment_status OR $s_payment_responses) $s_distro_level_to_check $s_signups $s_testing_demo_package_and_trial";
			$s_query_string = "select [db_payment_status].`cancel_reason`,[db_payment_status].`cancel_notes`,[db_restaurants].`created` ,[db_payment_status].`cancel_date`,[db_payment_status].`dataname` FROM [db_payment_status] LEFT JOIN [db_restaurants] ON [db_payment_status].`dataname`=[db_restaurants].`data_name` where [db_payment_status].`canceled`='1' and DATE([db_restaurants].`created`+ INTERVAL 1 MONTH) < DATE([db_payment_status].`cancel_date`)";
			//select `cancel_reason`,`cancel_notes`,`created` ,`cancel_date`,`dataname`, FROM `poslavu_MAIN_db`.`payment_status` left join `poslavu_MAIN_db`.`restaurants` on `payment_status`.`dataname` = `restaurants`.`data_name` where `poslavu_MAIN_db`.`payment_status`.`canceled`='1' and DATE(`restaurants`.`created`+ INTERVAL 1 MONTH) < DATE(`payment_status`.`cancel_date`)
			$s_q_check = ConnectionHub::getConn('poslavu')->getDBAPI()->drawQuery($s_query_string,$a_query_vars);
			//error_log("manage/misc/AdvancedToolsFunctions/companyReporting.php--line 1374:: ".$s_q_check);

			$q_cancel_results = mlavu_query($s_query_string, $a_query_vars);
			$a_return_val = array();
			$a_reason_count = array();
			$a_reason_monthly = array();
			while($cancel_result = mysqli_fetch_assoc($q_cancel_results)){
				$no_space_key = str_replace(' ','_', $cancel_result['cancel_reason']);
				$reason_key = str_replace("'",'', $no_space_key);

				//
				//total
				$a_reason_count[$reason_key]++;
				$a_reason_count['total']++;
				$cancel_result['cancel_reason'] = $reason_key;
				//$a_return_val[$cancel_result['dataname']] = $cancel_result;//array("data_name"=>$abda_result['dataname']);

				//
				//by month
				$date_key = substr($cancel_result['cancel_date'], 0,7);
				$a_reason_monthly[$date_key][$cancel_result['dataname']] = $cancel_result;
				
			}
			//print_r($a_reason_count);
			//return $a_return_val;
			$s_display = '';
			$s_total_display = '';
			$i_total = $a_reason_count['total'];
			foreach($a_reason_count as $k=>$v){
				if($k == ''){
					$k = "NONE GIVEN";
				}else if($k == 'total'){
					$s_total_display .= '<div >TOTAL: '.$v.'  (100%)</div><div style="height:10px;"></div>';
				}else{
					$i_percent = round(($v/$i_total)*100, 0);
					$s_display .= '<div>'.$k.': '.$v.'  ('.$i_percent.'%)</div><div style="height:10px;"></div>';
				}
				
			}
			$s_date = date("Y-m");
			$top_display = $s_date.'<br />'.$s_total_display.$s_display;
			krsort($a_reason_monthly);
			$s_monthy_display_string = '';
			$a_monthly_type = array();
			foreach($a_reason_monthly as $km=>$vm){
				//echo '<div style="font-weight:bold;font-size:16px;cursor:pointer;" onclick="$(\'.monthly_breakdown_reasons\').hide();$(\'#'.$km.count($vm).'\').toggle();">'.$km .': <span style="color:red;">'.count($vm).'</span></div><div class="monthly_breakdown_reasons" style="display:none;" id="'.$km.count($vm).'">';
				echo '<div style="font-weight:bold;font-size:16px;cursor:pointer;" onclick="showCancelReasons(\'#'.$km.count($vm).'\');">'.$km .': <span style="color:red;">'.count($vm).'</span></div><div class="monthly_breakdown_reasons" style="display:none;" id="'.$km.count($vm).'">';
				//$s_monthy_display_string .= $km.': TOTAL: '.count($vm).'<br ><br />';
				foreach($vm as $km2=>$vm2){
					echo '<div style="font-weight:bold;">Dataname:: '.$vm2['dataname'].'</div>Created:: '.$vm2['created'].'<br />Reason::'.$vm2['cancel_reason'].'<br />Cancel Note:: '.$vm2['cancel_notes'].'<br />';
					$a_monthly_type[$vm2['cancel_reason']]++;
					$a_monthly_type['total_montl']++;
				}//monthly stats
				echo '</div>';
				foreach($a_monthly_type as $ktype =>$kval){
					//$s_montly_display_string .= 
					//echo $ktype.' : '.$kval.'( '.round(($kval/count($vm))*100, 0).'%)<br ><br >';
				}//


			}//monthly
			//echo $s_montly_display_string;//$top_display;
		}

		// gets a list of active billing profiles for all accounts
		// @$a_types: if not null, it looks for types LIKE those in $a_types
		// @return: an array('dataname'=>dataname, 'count'=>count, types=>GROUP_CONCAT(DISTINCT `type`))
		function getActiveBillingProfiles_allAccounts($a_types = NULL) {

			// get the types clause
			$s_types_clause = '';
			if ($a_types !== NULL && count($a_types) > 0) {
				foreach($a_types as $s_type) {
					$s_types_clause .= " AND `type` LIKE '".ConnectionHub::getConn('poslavu')->escapeString($s_type)."' ";
				}
			}

			// get the rows
			$a_retval = ConnectionHub::getConn('poslavu')->getDBAPI()->getAllInTable('billing_profiles', array('arb_status'=>'active', 'active'=>'1'), TRUE,
				array('selectclause'=>'`dataname`,COUNT(`id`) AS `count`,GROUP_CONCAT(DISTINCT `type`) AS `types`', 'whereclause'=>"WHERE `arb_status`='[arb_status]' AND `active`='[active]' $s_types_clause",
				'groupbyclause'=>'GROUP BY `dataname`', 'orderbyclause'=>'ORDER BY `count` DESC'));

			return $a_retval;
		}

		function performCURL($url, $paramArray, $returnTransfer=true){

			$stringified= '';
			$first=true;
			$counter=0;

			foreach ($paramArray as $key=> $content){

				($first)?$first=false:'&';
				$stringified.=urlencode($key)."=".urlencode($content);
				$counter++;

			}
			//echo $stringified;
			$ch = curl_init();
			//set the url, number of POST vars, POST data
			curl_setopt($ch,CURLOPT_URL, $url);
			curl_setopt($ch,CURLOPT_POST,$counter);
			curl_setopt($ch,CURLOPT_POSTFIELDS, $stringified);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, $returnTransfer);
			//execute post
			$result = curl_exec($ch);

			//close connection
			curl_close($ch);

			return $result;
		}


	}

	class device_statistics_object {
		private $a_statistics = array();
		private $a_datanames = array();

		function __construct($a_datanames) {
			$this->a_datanames = $a_datanames;
			$this->a_statistics = $this->build_statistics(array(1,2,3,4,5,6,7,8,9,10,11), array(1,2,3,4,5,6,7,8,9,10,11), -1);
		}

		// helper function of build_statistics
		function combine_results($s_subtractfrom, $s_addto, $a_count) {
			if (isset($a_count[$s_subtractfrom])) {
				$i_total_count = 0;
				if (isset($a_count[$s_addto]))
					$i_total_count = $a_count[$s_addto];
				$i_total_count += $a_count[$s_subtractfrom];
				$a_count[$s_addto] = $i_total_count;
				unset($a_count[$s_subtractfrom]);
			}
			return $a_count;
		}

		// searches through the last $days_to_search days for the given numbers of iPads and iPods used
		// only searches through the first $i_max_datanames_to_search datanames in $this->a_datanames
		//     searches all datanames if $i_max_datanames_to_search == -1
		function build_statistics($a_max_ipads, $a_max_ipods, $i_max_datanames_to_search) {
			$days_to_search = 1;
			$a_count_ipads = array();
			$a_count_ipods = array();
			$mintime = date("Y-m-d H:i:s", strtotime($days_to_search." days ago"));

			$i_max = $i_max_datanames_to_search;
			foreach($this->a_datanames as $s_dataname) {
				if (trim($s_dataname) == '')
					continue;
				$a_count = $this->get_device_counts($s_dataname, $mintime);
				$a_count = $this->combine_results('ipod touch', 'ipod', $a_count);
				$a_count = $this->combine_results('iphone', 'ipod', $a_count);
				$a_count = $this->combine_results('ipad simulator', 'ipad', $a_count);
				foreach($a_count as $k=>$v)
					if (!in_array($k, array('ipad','ipod')))
						error_log($k);
				//error_log(print_r($a_count, TRUE));
				if (isset($a_count['ipad']))
					if (isset($a_count_ipads[$a_count['ipad']]))
						$a_count_ipads[$a_count['ipad']] .= '|'.$s_dataname;
					else
						$a_count_ipads[$a_count['ipad']] = $s_dataname;
				if (isset($a_count['ipod']))
					if (isset($a_count_ipods[$a_count['ipod']]))
						$a_count_ipods[$a_count['ipod']] .= '|'.$s_dataname;
					else
						$a_count_ipods[$a_count['ipod']] = $s_dataname;
				$i_max--;
				if ($i_max == 0)
					break;
			}

			$a_statistics = array();

			$i_ipod_count = 0;

			// ipods
			for($i = 0; $i < count($a_max_ipods); $i++) {
				$i_min_ipods = $a_max_ipods[$i];
				if ($i == count($a_max_ipods)-1) {
					$i_max_ipods = 999;
				} else if ($i == 0) {
					$i_max_ipods = $i_min_ipods;
				} else {
					$i_max_ipods = $i_min_ipods;
					$i_min_ipods = $a_max_ipods[$i-1]+1;
				}
				$s_index_name = $i_min_ipods.' to '.$i_max_ipods.' iPods';
				$a_statistics[$s_index_name] = array();
				for ($j = $i_min_ipods; $j <= $i_max_ipods; $j++) {
					if (!isset($a_count_ipods[$j]))
						continue;
					$i_ipod_count += count(explode('|', $a_count_ipods[$j]))*$j;
					$a_statistics[$s_index_name] = array_merge($a_statistics[$s_index_name], explode('|', $a_count_ipods[$j]));
				}
			}

			$i_ipad_count = 0;

			// ipads
			for($i = 0; $i < count($a_max_ipads); $i++) {
				$i_min_ipads = $a_max_ipads[$i];
				if ($i == count($a_max_ipads)-1) {
					$i_max_ipads = 999;
				} else if ($i == 0) {
					$i_max_ipads = $i_min_ipads;
				} else {
					$i_max_ipads = $i_min_ipads;
					$i_min_ipads = $a_max_ipads[$i-1]+1;
				}
				$s_index_name = $i_min_ipads.' to '.$i_max_ipads.' iPads';
				$a_statistics[$s_index_name] = array();
				for ($j = $i_min_ipads; $j <= $i_max_ipads; $j++) {
					if (!isset($a_count_ipads[$j]))
						continue;
					$i_ipad_count += count(explode('|', $a_count_ipads[$j]))*$j;
					$a_statistics[$s_index_name] = array_merge($a_statistics[$s_index_name], explode('|', $a_count_ipads[$j]));
				}
			}

			//error_log("ipods: ".$i_ipod_count);
			//error_log("ipads: ".$i_ipad_count);

			return $a_statistics;
		}

		// returns an array ('ipod'=>number of active ipods, 'ipad'=>number of active ipads)
		// where any number of the keys can be set or not
		function get_device_counts($s_dataname, $mintime) {
			$device_query = mlavu_query("SELECT `model`,`MAC` FROM `poslavu_[1]_db`.`devices`",$s_dataname);
			if ($device_query === FALSE)
				return array();
			if (mysqli_num_rows($device_query) == 0)
				return array();
			$a_devices = array();
			while ($device_read = mysqli_fetch_assoc($device_query))
				$a_devices[] = $device_read;
			unset($device_read);
			mysqli_free_result($device_query);

			// find the distinct models
			$a_device_models = array();
			foreach($a_devices as $a_device) {
				$a_device['model'] = strtolower($a_device['model']);
				$s_model = $a_device['model'];
				if (!strstr($a_device['MAC'], ':'))
					continue;
				if (!isset($a_device_models[$s_model]))
					$a_device_models[$s_model] = array();
				$a_device_models[$s_model][] = ConnectionHub::getConn('poslavu')->escapeString($a_device['MAC']);
			}

			$a_retval = array();
			foreach($a_device_models as $k=>$a_mac_addresses) {
				$last_mod_devices = "('".implode("','",$a_mac_addresses)."')";
				$s_query_string = "SELECT `id` FROM `[database]`.`orders` WHERE `closed`>'[mintime]' AND `last_mod_device` IN ".$last_mod_devices." GROUP BY `last_mod_device`";
				$a_query_vars = array("database"=>"poslavu_".$s_dataname."_db", "mintime"=>$mintime);
				$orders_query = mlavu_query($s_query_string, $a_query_vars);
				if ($orders_query === FALSE) {
					error_log("bad query (manage/misc/advancedtoolsfunctions/companyreporting.php), get_device_count");
					error_log(mlavu_dberror());
					continue;
				}
				$a_retval[$k] = mysqli_num_rows($orders_query);
			}
			return $a_retval;
		}

		function draw_stats() {
			$s_retval = '';

			foreach($this->a_statistics as $k=>$a_datanames) {
				$s_retval .= "
					<div class='manage_grouping_header' style='cursor:pointer;'>
						<div class='collapsable'>></div>{$k}: (<b>".count($a_datanames)."</b>)
					</div>
					<div class='manage_grouping' style='display: block;'>";
				foreach($a_datanames as $s_dataname)
					$s_retval .= $s_dataname.'<br />';
				$s_retval .= '</div>';
			}

			$s_retval .= "
				<script type='text/javascript'>setTimeout(function(){ create_collapsables(); collapse_collapsables(); },500);</script>
			~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~";

			return $s_retval;
		}
	}


	function companyReportingMain() {
		function getPostVar($s_name, $s_default = '') {
			return (isset($_POST[$s_name]) ? $_POST[$s_name] : $s_default);
		}

		$command = getPostVar('command');
		$functionName = getPostVar('function_name');

		$a_params = array(
			'leadStatusContainerID' => getPostVar('leadStatusContainerID', 'leadStatusContainer'),
		);

		if ($command == 'get_statistics') {
			$statsReporting = new statsReporting();
			if (method_exists($statsReporting, $functionName)) {
				return call_user_func(array($statsReporting, $functionName), $a_params);
			} else {
				return 'Unrecognized function: '. $functionName;
			}
		} else {
			return 'Unrecognized command';
		}
	}

	if (isset($_POST['company_reporting']) && $_POST['company_reporting'] == 'do_stuff') {
		echo companyReportingMain();
	}
?>
