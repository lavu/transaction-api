<?php
	require_once($_SERVER['DOCUMENT_ROOT']."/manage/error_config.php");
	require_once($_SERVER['DOCUMENT_ROOT']."/cp/resources/lavuquery.php");
	require_once($_SERVER['DOCUMENT_ROOT']."/cp/resources/json.php");

	if(isset($_POST['function'])){
		$fun = $_POST['function'];
		unset($_POST['function']);
		echo $fun($_POST);
	}else{
		echo '{"error":"No Function."}';
	}
	
	function get_table_names($args){
		$query	    = mlavu_query("SHOW TABLES FROM `poslavu_DEV_db`");
		$return_arr = array();
		while($res= mysqli_fetch_assoc($query)){
			if($res['Tables_in_poslavu_DEV_db']!= 'users')
				$return_arr[] = $res;
		}
		return LavuJson::json_encode($return_arr);
	}
	function transfer_settings($args){
		//echo print_r($args,1);
		$dn_start_exists = mysqli_num_rows(mlavu_query("SELECT SCHEMA_NAME FROM INFORMATION_SCHEMA.SCHEMATA WHERE SCHEMA_NAME = '[1]'", "poslavu_".$args['dn_start']."_db"));
		$dn_end_exists   = mysqli_num_rows(mlavu_query("SELECT SCHEMA_NAME FROM INFORMATION_SCHEMA.SCHEMATA WHERE SCHEMA_NAME = '[1]'", "poslavu_".$args['dn_end']."_db"));
		if($dn_end_exists && $dn_start_exists){
			if(isset($args['selected_tables'])&&!empty($args['selected_tables'])){
				$tables= LavuJson::json_decode($args['selected_tables']);
				lavu_connect_dn($args['dn_end'],"poslavu_".$args['dn_end']."_db");
				foreach($tables as $table){
					backup_table($table, $args['dn_end']);
					lavu_connect_dn($args['dn_start'],true);
					$result 			= lavu_query("SELECT * FROM `[1]`", $table);
					$num_fields 		= mysqli_num_fields($result);	
					$drop_table_query   = "DROP TABLE `[1]`";
					$create_table_query = mysqli_fetch_row(lavu_query("SHOW CREATE TABLE `[1]`",$table));
					
					lavu_query($drop_table_query,$args['dn_end'], $table);
					
					lavu_query($create_table_query[1]);
					for ($i = 0; $i < $num_fields; $i++) 
					{
						while($row = mysqli_fetch_row($result))
						{
							if($table == 'locations' || $table == 'users'){
								
							}else{
								//$insert_string= 'INSERT INTO `poslavu_[1]_db`.`'.$table.'`VALUES(';
								$insert_string= 'INSERT INTO `'.$table.'() `VALUES(';
								for($j=0; $j<$num_fields; $j++){
									$row[$j] = addslashes($row[$j]);
									$row[$j] = ereg_replace("\n","\\n",$row[$j]);
									if (isset($row[$j])){ 
										$insert_string.= '"'.$row[$j].'"' ; 
									}else{ 
										$insert_string.= '""'; 
									}
									if ($j<($num_fields-1)) { 
										$insert_string.= ','; 
									}
								}
								$insert_string.= ");";
								//echo "The insert_string: ".$insert_string;
								lavu_query($insert_string);
							}
						}
					}
				}
			}else{
				return '{"error":"No Tables Selected"}';
			}
		}else{
			return '{"error":"Start or end dataname is invalid"}';
		}
	}
	function backup_table($table, $dn){
		lavu_connect_dn($dn, "poslavu_".$dn."_db");
		$result = lavu_query("SELECT * FROM `[1]`", $table);
		$num_fields = mysqli_num_fields($result);
		
		$return.= 'DROP TABLE '.$table.';';
		$row2 = mysqli_fetch_row(lavu_query("SHOW CREATE TABLE `[1]`",$table));
		$return.= "\n\n".$row2[1].";\n\n";
		
		for ($i = 0; $i < $num_fields; $i++) 
		{
			while($row = mysqli_fetch_row($result))
			{
				$return.= 'INSERT INTO '.$table.' VALUES(';
				for($j=0; $j<$num_fields; $j++) 
				{
					$row[$j] = addslashes($row[$j]);
					$row[$j] = ereg_replace("\n","\\n",$row[$j]);
					if (isset($row[$j])) { $return.= '"'.$row[$j].'"' ; } else { $return.= '""'; }
					if ($j<($num_fields-1)) { $return.= ','; }
				}
				$return.= ");\n";
			}
		}
		$return.="\n\n\n";
		mkdir($_SERVER['DOCUMENT_ROOT']."/manage/misc/db_backups/".$dn);
		file_put_contents($_SERVER['DOCUMENT_ROOT']."/manage/misc/db_backups/".$table.".sql", $return);
		//$handle = fopen("/manage/misc/db_backups/".$dn.'_'.$table.".sql",'w+');
		//fwrite($handle,$return);
		//fclose($handle);
	}

?>