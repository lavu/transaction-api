
<?php
	require_once($_SERVER['DOCUMENT_ROOT']."/manage/error_config.php");
	session_start();
	require_once("../../login.php");
	require_once($_SERVER['DOCUMENT_ROOT']."/cp/objects/json_decode.php");
	require_once(dirname(__FILE__)."/../autoTable.php");
	require_once(dirname(__FILE__)."/../../../cp/resources/core_functions.php");

	if (isset($in_manage) || isset($_POST['in_manage'])) {
		require_once($_SERVER['DOCUMENT_ROOT']."/manage/misc/LLSFunctions/makeLLSListTable.php");
		require_once($_SERVER['DOCUMENT_ROOT']."/manage/misc/AdvancedToolsFunctions/extensions/editExtendPageObj.php");
		if ( isset($_POST['function'])){

			if (function_exists($_POST['function']))
				$_POST['function']();
			else
				echo "error:no function of name ". $_POST['function'];
		}else
			echo "error: no function name given.";
	}

	/*This condition is used to check exportData for Active customers*/
	if (reqvar('exportData') == 'exportCSV' ) {
		$exportData = exportTocsv($_REQUEST);
		echo $exportData;
		exit;
	} else if (reqvar('action') == 'search') {/*This condition is used to check search data for Active customers*/
		$searchInfo = getActiveCustomerInfo($_POST);
		echo $searchInfo;
		exit;
	} else if (reqvar('action') == 'lazyLoad') {/*This condition is used to check search data for Active customers  */
		$activeCustomerInfo = getCustomerInfoByLazyLoad($_POST);
		echo $activeCustomerInfo;
		exit;
	}

	function getMigrateTools(){
		/* Depricated.
		echo"<table id='migrateAccountTable'>
		<tr><td>New DB Name:</td><td> <input type='text' id='newName'></td></tr>
		</tr><td>Old Location ID:</td><td> <input type='text' id='locationID'></td></tr>
		<tr><td>Old DataName</td><td><input type='text' id='oldDataname'></td></tr>
		<tr><td><input type='button' value= 'submit' onclick=\"doMigrate($('#newName').val(),$('#locationID').val(),$('#menuID').val(),$('#oldDataname').val()) \"> </td></tr>
		</table>
		</div>";
		*/
	}
	function change_backend_video(){
		echo"<table id='change_backend_video'>
		<tr><td>Backend Video </td><td> <input type='text' id='backend_vid'></td></tr>
		<tr><td><input type='button' value= 'submit' onclick=\"change_vid($('#backend_vid').val()) \"> </td></tr>
		</table>
		</div>";
	}
	function listLocalServers(){

		echo "<br><a style='color:white' href= '/manage/misc/AdvancedToolsFunctions/PaymentHistory.php?mode=payment_history' target='_blank'> View Payment History </a><br><br><br>";
		echo get_local_server_list()."<br><br><br>";

	}
	function report_builder(){
		echo "<iframe src='http://admin.poslavu.com/manage/misc/AdvancedToolsFunctions/report_builder.php' height='100%' width='100%'></iframe>";
	}
	function getLavuLiteStats(){

		require_once($_SERVER['DOCUMENT_ROOT']."/manage/misc/AdvancedToolsFunctions/companyReporting.php");
		$statsReporting= new statsReporting();
		echo $statsReporting->displayData();

	}
	function viewPaymentHistory(){
		echo "<br><a style='color:white' href= '/manage/misc/AdvancedToolsFunctions/PaymentHistory.php?mode=payment_history' target='_blank'> View Payment History </a><br><br><br>";
	}
	function addExtension(){

	}
	function distributorsTools() {
		require_once(dirname(__FILE__)."/manageDistributorLicenses.php");
		echo drawAllDistributorTools();
		echo "<br /><br /><br />";
		//echo drawRemoveLicenses()."<br /><br /><br />";
	}
	function backendMessages(){
		require_once("./messagesDB.php");
		$db = new messagesDB();
		$return_str = "<script type='text/javascript' src= './misc/AdvancedToolsFunctions/rich_text_editor/jquery.wysiwyg.js'></script>
		<link rel='stylesheet' type='text/css' href='./misc/AdvancedToolsFunctions/rich_text_editor/jquery.wysiwyg.css'>
		<div class='advanced_tools_container'>
			<center>
				<table class='advanced_tools_table_positioner' cellpadding='4'>
					<tr>
						<th colspan='6'>
							Customer Back/Front End Messages
						</th>
					</tr>
					<tr>
						<td class='advanced_tools_td'>Date Created</td><td class='advanced_tools_td'>Type</td><td class='advanced_tools_td'>Title</td><td class='advanced_tools_td'><td class='advanced_tools_td'>Status</td>
					</tr>";
		$messages = $db->getMesagges();
		$odd=false;
		foreach($messages as $message){
			$id        = $message['id'];
			$timestamp = $message['timestamp'];
			$type      = $message['type'];
			$access_lvl= $message['min_access_level'];
			$title     = $message['title'];
			$content   = $message['content'];
			$limiting_q= $message['limiting_query'];
			$limiting_m= $message['limiting_module'];
			//echo "CONTENT:". $content;
			if(strlen($title)>50){
				$title = substr($title, 0, 50).'...';
			}//if
			if(strlen($content)>50){
				$content = substr($content, 0, 50).'...';
			}//if
			$odd= !$odd;
			if(!$odd)
				$class='advanced_tools_td';
			else
				$class='advanced_tools_td_odd';
				
			switch ($type) {
				case '1': $type = "Mail"; break;
				case '2': $type = "Cog"; break;
				case '3': $type = "Ambulance"; break;
				case '4': $type = "Bug"; break;
				case '5': $type = "Emergency"; break;
				case '6': $type = "Video"; break;
				case '7': $type = "Front End"; break;
				default: break;
			}
			
			if ($access_lvl == "0") $access_lvl = "1";
			$access_lvl += "+";

			$return_str .= "<tr>
				<td class='$class'>$timestamp</td>
				<td class='$class'>$type</td>
				<td class='$class'>$title</td>
				<td class='$class'><div class='edit_message' onclick='edit_message(\"$id\")'>Edit</div></td>
				<td class='$class'>
					<div class='delete_message' onclick='delete_message(\"$id\")'>";
			
			//0= active, 1=deleted, 2= pending
			if($message['_deleted']==1)
				$return_str.="Un-Delete";
			else if($message['_deleted']==2)
				$return_str.="Pending";
			else
				$return_str.="Delete";
			
			$return_str .= "</div>
				</td>
			</tr>";
		}

		$return_str .= "<tr><td colspan='5' align='center'><br><table width='150px'><tr><td><div class='new_message_btn' onclick='new_message();' style='text-align:center'>New Message</div></td></tr></table><br><br></td></tr>
				</table>
				<div id='message_editor'>
					<center>
						<form name='message_form'>			
							<table>
								<tr>
									<td>
										<input type='hidden' name='function' class='message_action'/>
										<input type='hidden' name='id' class='message_id'/>
										<input type='hidden' name='username' class='message_username'/>
										Title: <input type='text' name='title' class='message_title'/>
									</td>
									<td>Type: <select name='type' id='message_type_selector' class='message_type'>";
									
		$typeArray 	= array('0'=>' -- select ---','3'=>'Ambulance','4'=>'Bug','2'=>'Cog','5'=>'Emergency','7'=>'Front End','1'=>'Mail');
		foreach($typeArray as $value=>$toEcho){
			$return_str .= '<option value="'.$value.'">'.$toEcho.'</option>';
		}

		$return_str .= "</select>
									</td>
									<td>
										<div class='emergency_date_container'>
											Removal Date:<input type='text' class= 'emergency_end_date' name='removal_date' />
											<script type='text/javascript'>
												function put_picker(){
													$('.emergency_end_date').datepicker();
												}
												put_picker();
											</script>
										</div>
									</td>
								</tr>
								<tr>
									<td colspan='100'>
										<textarea rows='10' cols='50' name='content' class='message_content'></textarea>
									</td>
								</tr>
								<tr>
									<td colspan='100' style='display:none'>
										Minimum access level: <select name='limiting_access_level' class='limiting_access_level'>
											<option value='1'>1</option>
											<option value='2'>2</option>
											<option value='3'>3</option>
											<option value='4'>4</option>
										</select>
									</td>
								</tr>
								<tr>
									<td colspan='3'>
										Use Limiters?<input type='checkbox' class='limiters_use' value='1' onclick=\"if($('.limiters').css('display')=='none')$('.limiters').css('display','block'); else $('.limiters').css('display','none');\">
										<div class='limiters' style='display:none'>
											<input type='text' style='width:370px' name='limiting_query' class='limiting_query' placeholder='Limiting query (\"For developers only\")'/>
											<br>
											<div style='float:left' >
												<input type='radio' name='module_use' value='1'>Has</input>
												<input type='radio' name='module_use' value='0'>Doesn't Have</input>
												<br>
												<input type='radio' name='mod_combiner' value='and'>And</input>
												<input type='radio' name='mod_combiner' value='or'>Or</input>
											</div>
											<div class='check_box_container' style='overflow-y:auto; height:100px;width:200px;float:right;'>";

		$live_file = $_SERVER['DOCUMENT_ROOT'] . '/lib/modules/modules_list.txt';
		$json	  = new Json();
		$contents = $json->decode(file_get_contents($live_file));
		$var 	  ='Available Modules';
		foreach($contents->$var as $val){
			$return_str.="<input type='checkbox' class='lim_mod' name='limiting_module' value='$val'> $val </br>";
		}
		
		$return_str .= "</div>
										</div>
									</td>
								</tr>
								<tr>
									<td>
										<input type='button' value='Submit' onclick='if (document.getElementById(\"message_type_selector\").value == \"0\") alert(\"Please select message type...\"); else save_message($(\"form\"));'/>
									</td>
								</tr>
							</table>
						</form>
					</center>
				</div>
			</center>
		</div><br><br><br><br>";

		echo $return_str;
	}
	function get_local_server_list() {
		global $maindb;

		$s_retval = '';
		$s_retval .= '
		<script type=\'text/javascript\'>
			load_company_search_from_lls_list_dataname = \'\';
			function load_company_search_from_lls_list() {
				lavuLog(load_company_search_from_lls_list_dataname);
				populateResults(load_company_search_from_lls_list_dataname, function(){
					$(\'#company_search_button\').click();
					doneLoading();
				});
			}
		</script>';

		require_once(dirname(__FILE__)."/../autoTable.php");
		require_once("/home/poslavu/public_html/admin/cp/resources/lavuquery.php");
		$lls_query = mlavu_query("SELECT
			`company_name` AS `company name`,
			`data_name` AS `dataname`,
			`encryption_type` AS `server type`,
			`last_checked_in` AS `last checked in`,
			`last_ip_address` AS `last ip address`,
			`version_number` AS `app version`
			FROM ((SELECT * FROM `[maindb]`.`local_servers`)) `llsdb`
			LEFT JOIN ((SELECT `company_name`,`data_name` AS `rdb dataname`,`disabled`,`version_number` FROM `[maindb]`.`restaurants`)) `rdb`
			ON (`llsdb`.`data_name`=`rdb`.`rdb dataname`)
			WHERE `rdb`.`disabled`='0'
			ORDER BY `rdb`.`company_name` ASC",
			array("maindb"=>$maindb));
		if ($lls_query !== FALSE) {
			if (mysqli_num_rows($lls_query) > 0) {
				$a_lls_entries = array();
				while ($lls_read = mysqli_fetch_assoc($lls_query)) {
					$lls_read['last checked in'] = date("Y-m-d H:i:s", (int)$lls_read['last checked in']);
					$a_lls_entries[] = $lls_read;
				}
				$a_headers = array();
				foreach($a_lls_entries[0] as $k=>$v)
					$a_headers[] = ucwords($k);
				$s_retval .= auto_build_table($a_headers, $a_lls_entries,
					'$(this).addClass(\'lls_selected\');
						//loading();
						load_company_search_from_lls_list_dataname=$( $(this).find(\'td\')[1] ).text();
						load_company_search_from_lls_list();
						setTimeout(\'load_company_search_from_lls_list();\', 200);
					', 'lls_table');
				$s_retval .= '<script type=\'text/javascript\'>
						auto_table_adjust_table_widths(\'lls_selected\');
					</script>';
			}
		}
		mysqli_free_result($lls_query);

		return $s_retval;
	}
	function getInputForJsonfield($obj, $key){
		$returnString='';
		if( $key == 'usage'){
			$returnString.="usage: ".$obj->usage."<br>";
			$returnString.="<ol>";
			foreach($obj->features as $feature){
				$returnString.="<li>".$feature."</li>";
			}
			$returnString.="</ol>";
			$returnString.="<div class='addFeature' onclick='addFeature()'>Add Feature</div>";

		}else if($key=='link'){
			$returnString.='<ul> <li> Site Link: '. $obj->siteLink."</li> <li> Login Link: ".$obj->loginLink."</li></ul>";
		}else if($key=='features'){
			$returnString.="<ol>";
			foreach($obj as $feat){
				$returnString.="<li>".$feat."</li>";
			}
			$returnString.="</ol>";
			$returnString.="<div class='addFeature' onclick='addFeature()'>Add Feature</div>";

		}else
			return print_r($obj,true);
		return $returnString;
	}
	function editModules(){
		/**
		 *
		 */
		 $json = new JSON();
		 require_once dirname( __FILE__ ) . "/getEditModules.php";

	}
	function createAccount(){
		
		$model_list = array();
		$model_list['Bar/Lounge'] = array("model"=>"defaultbar",	"title"=>"Bar/Lounge");
		$model_list['Coffee Shop'] = array("model"=>"default_coffee",	"title"=>"Coffee Shop");
		$model_list['Diner'] = array("model"=>"apple_default",	"title"=>"Diner");
		$model_list['Original Sushi'] = array("model"=>"sushi_town",	"title"=>"Original Sushi");
		$model_list['Pizza Shop'] = array("model"=>"default_pizza_",	"title"=>"Pizza Shop");
		$model_list['Restaurant'] = array("model"=>"default_restau",	"title"=>"Restaurant");
		
		if(can_access("technical"))
		{
			$model_list['Laser Tag'] = array("model"=>"ersdemo",	"title"=>"Laser Tag",	"product"=>"lasertag");
			$model_list['Lavu Give'] = array("model"=>"lavu_tithe",	"title"=>"Lavu Give",	"product"=>"tithe");
			$model_list['Lavu Hospitality'] = array("model"=>"hotel_lavu",	"title"=>"Lavu Hospitality",	"product"=>"hotel");
			$model_list['Lavu Kart (P2R)'] = array("model"=>"p2r_syracuse",	"title"=>"Lavu Kart (P2R)",	"product"=>"lavukart");
		}
		
		ksort($model_list);
			
			//../lib/create_demo_account.php?m=2' method='POST'>
			//<form action='index.php?mode=create_account' method='POST'>
			echo "
			<form id='createAccountForm'>
				<table cellspacing='0' cellpadding='5'>
					<tr><td align='right'>Model:</td><td align='left'><select name='default_menu'>";
					
					foreach ($model_list as $key => $val) {				
						$model_dataname = $val['model'];
						$model_title = $val['title'];
						echo "<option value='$model_dataname'>$model_title</option>";
					}

			echo "</select></td></tr>
					<tr>
						<td align='right'>Account Type:</td>
						<td align='left'>
							<input type='radio' name='account_type' value='Client' checked> Client
							<input type='radio' name='account_type' value='Reseller'> Reseller
							<input type='radio' name='account_type' value='Demo'> Demo
							<input type='radio' name='account_type' value='Test'> Test
							<input type='radio' name='account_type' value='Dev'> Dev
						</td>
					</tr>
					<tr><td align='right'>Company Name:</td><td align='left'><input type='text' name='company' size='30' /></td></tr>
					<tr><td align='right'>Email:</td><td align='left'><input type='text' name='email' size='30' /></td></tr>
					<tr><td align='right'>Phone:</td><td align='left'><input type='text' name='phone' size='30' /></td></tr>
					<tr><td align='right'>First Name:</td><td align='left'><input type='text' name='firstname' size='30' /></td></tr>
					<tr><td align='right'>Last Name:</td><td align='left'><input type='text' name='lastname' size='30' /></td></tr>
					<tr><td align='right'>Address:</td><td align='left'><input type='text' name='address' size='30' /></td></tr>
					<tr><td align='right'>City:</td><td align='left'><input type='text' name='city' size='30' /></td></tr>
					<tr><td align='right'>State:</td><td align='left'><input type='text' name='state' size='30' /></td></tr>
					<tr><td align='right'>Zip:</td><td align='left'><input type='text' name='zip' size='30' /></td></tr>
					<tr><td align='center' colspan='2'><input type='button' value='Create Account' onclick='doCreateAccount($(\"#createAccountForm\") )' /></td></tr>
				</table>
			</form>";


	}

	function uploadDocs(){

		echo "Currently uploaded files: ";
		$files= scandir($_SERVER['DOCUMENT_ROOT']. "/manage/misc/companyDocs/");
		echo "<ul>";
		foreach($files as $file){
			if( $file != "." && $file !=".."){
				$exploded= explode("___", $file);
				if(isset($exploded[1]))
					echo "<li><span class='uploaded_document' onclick='open_delete_box(\"{$file}\")'> {$exploded[2]}</span> Type: {$exploded[1]}</li>";
				else
					echo "<li> {$exploded[1]}</li>";
			}
		}
		echo "</ul>";

		echo '<form action="/manage/misc/AdvancedToolsFunctions/upload_file.php" method="post" enctype="multipart/form-data">
				<label for="file">Filename:</label>

				<input type="file" name="file" id="file" />
				<input type="text" name="fileType" id="fileType" placeholder="Document Type"/>
				<br/>
				<input type="submit" name="submit" value="Submit">
			  </form>';
	}
	function build_website(){
		$query= mlavu_query("SELECT * FROM `poslavu_PRODUCTS_db`.`info` where `deleted` !='1'");
		echo "
		<div class= 'advanced_tools_container'>

		<table class='advanced_tools_table_positioner'>
			<tr><th class= 'advanced_tools_th' colspan='8'> Subsite List </th></tr>
			<tr><td>ID</td><td> Name </td><td>Domain Name</td><td>Status</td><td>View Site</td><td>Edit Site</td><td>Delete</td></tr>";
			$odd= false;
			while($result= mysqli_fetch_assoc($query)){
				$odd= !$odd;
				if(!$odd)
					$class='advanced_tools_td';
				else
					$class='advanced_tools_td_odd';

				echo "<tr><td class='$class'>{$result['id']}</td><td class='$class'>{$result['product_name']}</td><td class='$class'>{$result['domain_name']}</td><td class='$class'> {$result['status']}</td>
				<td class='$class'><a href='http://admin.poslavu.com/manage/subsite_builder/?product_id={$result['id']}'>View</a></td>
				<td class='$class'><a href='http://admin.poslavu.com/manage/subsite_builder/editor/?product_id={$result['id']}'>Edit</a></td>
				<td class='$class'><a class='delete_component' style='cursor:pointer' onclick='delete_product(\"{$result['id']}\")'>Delete</a></td></tr>";
			}

		echo "</table></div></div>";
	}
	function settingsTransfer(){
		require_once("./transfer_settings.php");

	}
		/*
		$json= new Json();

		$returnString='<table id="extensionTable"><tr>';
		$columnQuery=mlavu_query("SELECT COLUMN_NAME FROM INFORMATION_SCHEMA.COLUMNS WHERE table_name = 'extensions'AND table_schema = 'poslavu_MAIN_db'");
		$extensionQuery= mlavu_query("SELECT * FROM `poslavu_MAIN_db`.`extensions`");
		while($col =mysqli_fetch_assoc($columnQuery)){
			$returnString.="<th >".$col['COLUMN_NAME']."</th>";
		}

		$returnString.="</tr>";
		while($col =mysqli_fetch_assoc($extensionQuery)){
			$returnString.="<tr>";
			foreach($col as $key =>$content){
				$decoded=$json->decode($content);
				if(!$decoded){
					if($key!='id')
						$returnString.="<td class='".$key."' onclick='openEditor( $(this), \"".$key."\")'>".$content."</td>";
					else
						$returnString.="<td class='".$key."' >".$content."</td>";

				}else
					$returnString.="<td class='".$key."'>".getInputForJsonfield($decoded, $key)."</td>";

			}$returnString.="</tr>";
		}

		$returnString.='<tr style="border-top:1px solid black;"><td><div id="addExtension" onclick="addExtensionRow()">Add Extension</div><td></td></tr>

		</table>';


		echo $returnString;
		*/

	/**
	* This function is used for Active customer Report Page.
	*
	* @return string $dataUsageReport.
	*/
 	function activeCustomersReport() {
 		$oldTimezone = date_default_timezone_get();
		$serverTimezone = "America/Denver";
		date_default_timezone_set($serverTimezone);
		$toDate = date('Y-m-d', strtotime('today - 1 day'));
		$fromDate = date('Y-m-d', strtotime('today - 30 days'));
		$dataUsageReport = "<script type='text/javascript' src= './js/Tabs/advancedTools.js'></script>
		<div style='width:100%'>
			<div style='width:100%;text-align:center;font-weight: bold;font-size: 20px;'><span>Active Customers(".$fromDate." To ".$toDate.")</span></div>
			<div class='search' style='display:inline-block;left:20px;width:50%'>
				<input type='text' id='searchInput' placeholder='Dataname Search' name='search' style='height:18px;z-index:100;margin-right:2px;' onkeypress='return searchKeyPress(event);'/><input type='hidden' id='iteration' name='iteration' value='0'><input type='hidden' id='customerCount' name='customerCount' value='0'><input id='searchDataname' type='button' value='Search' onclick='getSelectedActiveCustInfo()'><span style='padding:10px'> <input id='showAllActiveData' type='button' value='Show All' onclick='getSelectedActiveCustInfo(\"ShowAll\")'></span>
				 <span id='loadStatus' class='displayNone'>Loading...</span>
			</div>
			<div style='float:right;'><input id='exporttocsv' type='button' value='Export to CSV' onclick='getDataToExport()'></div>
		</div>";
		date_default_timezone_set($oldTimezone);
		$dataUsageReport .= getActiveCustomerInfo();
		echo $dataUsageReport;
	}

	/**
	* This function is used to get Active customer Report Data in last 30days.
	*
	* @param string $searchInfo
	*
	* @return array $dataUsageReport.
	*/
	function customerData($searchInfo) {
		$searchQry = '';
		$iteration = (isset($searchInfo['iteration'])) ? $searchInfo['iteration'] : 0;
		$limit = (isset($searchInfo['limit'])) ? $searchInfo['limit'] : 100;
		$startIndex = 0;
		if ($iteration != 0) {
			$startIndex = ($iteration * $limit);
		}

		$searchData = (isset($searchInfo['searchData'])) ? $searchInfo['searchData'] : '';
		$searchString = strip_tags($searchData);
		if ($searchString != '') {
			$searchQry = ' WHERE `data_name` like "%'.$searchString.'%"';
		}
		$subQuery = " LIMIT $startIndex, $limit";
		$resultQry = "SELECT * FROM `poslavu_MAIN_db`.`active_customer_details` $searchQry ORDER BY `data_name` ASC";
		$finalQry = $resultQry.$subQuery;
		if (isset($searchInfo['exportData']) && $searchInfo['exportData'] != '') {
			$finalQry = $resultQry;
		}

		$custInfoQuery = mrpt_query($finalQry);
		$customerInfo = array();
		if (mysqli_num_rows($custInfoQuery) > 0) {
			while ($resultData = mysqli_fetch_assoc($custInfoQuery)) {
				$activeCustomerInfo = array();
				$customerData = json_decode($resultData['data'], true);
				$activeCustomerInfo['Dataname'] = $resultData['data_name'];
				$activeCustomerInfo['Gateway'] = $customerData['gateway'];
				$activeCustomerInfo['US/Intl'] = $customerData['business_country'];
				$activeCustomerInfo['Language Pack'] = $customerData['use_language_pack'];
				$activeCustomerInfo['Start Date(EST)'] = date('Y-m-d', strtotime($customerData['start_date']));
				$activeCustomerInfo['Default Service Type'] = $customerData['default_service_type'];
				$activeCustomerInfo['Number of Users'] = $customerData['active_user_count'];
				$activeCustomerInfo['Terminal Count'] = $customerData['terminal_count'];
				$activeCustomerInfo['Ipod Count'] = $customerData['ipod_count'];
				$activeCustomerInfo['Premium KDS Features'] = $customerData['premium_kds_features'];
				$activeCustomerInfo['Monthly SaaS'] = $customerData['zuora_payment'];
				$activeCustomerInfo['Average order amount(last 3 months)'] = $customerData['average_order_amount'];
				$activeCustomerInfo['%CC Sales a month'] = $customerData['cc_sales_ratio'];
				$activeCustomerInfo['Chain'] = $customerData['chain_status'];
				$activeCustomerInfo['CP'] = $customerData['cp_statis'];
				$activeCustomerInfo['LTG'] = $customerData['ltg_status'];
				$activeCustomerInfo['Kiosk'] = $customerData['kiosk_status'];
				$activeCustomerInfo['Inventory 1'] = $customerData['inventory1_status'];
				$activeCustomerInfo['Inventory 2'] = $customerData['inventory2_status'];
				$activeCustomerInfo['Scheduling'] = $customerData['scheduling_status'];
				$activeCustomerInfo['Timecards '] = $customerData['timecards_status'];
				$activeCustomerInfo['Combos'] = $customerData['combo_status'];
				$activeCustomerInfo['Pizza'] = $customerData['pizza_status'];
				$activeCustomerInfo['Lavu Loyalty'] = $customerData['lavu_loyalty_status'];
				$activeCustomerInfo['LPL (Pepper)'] = $customerData['lpl_status'];
				$activeCustomerInfo['Lavu Gift'] = $customerData['lavu_gift_status'];

				$customerInfo[] = $activeCustomerInfo;
			}
		}

		return $customerInfo;
	}

	/**
	* This function is used to prepare Active Customer table view and check replica DB is connected or not.
	*
	* @param string $searchData
	*
	* @return string $dataUsageReport.
	*/
	function getActiveCustomerInfo($searchData = array()) {
		$dataUsageReport = '<div id="activeCustomerData">';

		$obj = new DBConnector();
		$replicaConn = $obj->getLink('readreplica');
		if (empty($replicaConn)) {
		$dataUsageReport .= '<script type="text/javascript">$("#exporttocsv").prop("disabled", true);$("#exporttocsv").css("opacity", "0.5");</script><div class="noOrders" style="color:#FF0000;font-size: 12px;"> The reports application is currently unavailable due to maintenance. <br> Please check back later</div>';
		} else {
			$customerInfo = customerData($searchData);
			if (empty($customerInfo)) {
				$dataUsageReport .= '<script type="text/javascript">$("#exporttocsv").prop("disabled", true);$("#exporttocsv").css("opacity", "0.5");</script><div class="noOrders">No Records Found</div>';
			} else {
				$customerInfoHeader = array_keys($customerInfo['0']);
				$customerCount = count($customerInfo);
				$dataUsageReport .= auto_build_table($customerInfoHeader, $customerInfo);
				$dataUsageReport .= '<script type="text/javascript">$("#exporttocsv").prop("disabled", false);$("#exporttocsv").css("opacity", "1");
					$("#advancedToolsContent").css("height", "600px");
					$("#activeCustomerData #customerData").css("height", "440px");
					$("#customerCount").val('.$customerCount.');
					$(".lastrow").hover(function () {
						const customerCount = $("#customerCount").val();
						loadActiveCustomerInfo(customerCount);
					});
				</script>';
			}
		}

		$dataUsageReport .= '</div>';

		return $dataUsageReport;
	}

	/**
	* This function is used to export active customer report in CSV .
	*
	* @param string $searchData.
	*
	* @return csvfile download to csv.
	*/
	function exportTocsv($searchData = array()) {
		ob_end_clean();
		$filename = "ActiveCustomerInfo-" .date('YmdHis'). ".csv";

		header("Content-Type: text/csv; charset=UTF-8");
		header("Content-Disposition: attachment; filename=\"$filename\"");
		header("Content-Transfer-Encoding: binary");
		header('Accept-Ranges: bytes');
		header("Cache-control: private");
		header('Pragma: private');
		header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");

		$customerInfo = customerData($searchData);
		if (isset($customerInfo['0'])) {
			$BOM = "\xEF\xBB\xBF"; // UTF-8 BOM
	        $file = fopen('php://output', 'w');
			fwrite($file, $BOM);
	        fputcsv($file, array_keys($customerInfo['0']));
	        foreach ($customerInfo as $values) {
	            fputcsv($file, $values);
	        }
	        fclose($file);
		}
		ob_flush();
    }

    /**
    * This function is used to get lazyload data.
    *
    * @param array $data
    *
    * @return string $resultData.
    */
	function getCustomerInfoByLazyLoad($data = array()) {
		$customerInfo = customerData($data);
		$customerCount = count($customerInfo);
		$resultData = '<script type="text/javascript">$("#customerCount").val('.$customerCount.');</script>';
		if (!empty($customerInfo)) {
			$resultData = buildLazyloadTableData($customerInfo);
		}

		return $resultData;
	}
