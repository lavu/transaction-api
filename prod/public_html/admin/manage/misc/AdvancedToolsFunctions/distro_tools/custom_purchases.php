<?php
	require_once(dirname(__FILE__)."/../../../../cp/objects/historyObject.php");
	require_once(dirname(__FILE__)."/../../../../sa_cp/billing/package_levels_object.php");
	require_once(dirname(__FILE__)."/../../../../manage/globals/email_addresses.php");

	global $o_package_levels;
	$o_package_levels = new package_container();

	class ManageDistroToolsCustomPurchases {

		/**
		 * Sets the permissions to TRUE if can_access("distros_custom_purchases")
		 */
		function __construct() {
			global $loggedin;
			$tempLoggedIn = $loggedin;

			if (!function_exists('account_loggedin'))
				require_once(dirname(__FILE__)."/../../../login_noecho.php");
			if (function_exists('can_access')) {
				if (can_access("distros_custom_purchases"))
					$this->set_permission(TRUE);
			} else {
				$this->set_permission(FALSE);
			}

			if (trim($tempLoggedIn) !== "")
				$loggedin = $tempLoggedIn;
		}

		/**
		 * determine if the user has permission to access the distributor's custom purchases
		 * @return boolean TRUE if the user has permission, FALSE otherwise
		 */
		public function check_permission() {
			if (!$this->has_permission)
				return FALSE;
			return TRUE;
		}

		/**
		 * set the permissions for this user
		 * @param boolean $b_has_permission TRUE if the user should have permission, FALSE otherwise
		 */
		public function set_permission($b_has_permission) {
			$this->has_permission = $b_has_permission;
		}

		/**
		 * Describes available distributor custom purchase types, as an array of arrays
		 * @return array: in the form:
		 *     'type'=>array(
		 *         'name'=>array(
		 *             'absolute_vars'=>array(
		 *                 values and descriptions that are always the same
		 *             ),
		 *             'relative_vars'=>array(
		 *                 values and descriptions that change based on other variables
		 *             )
		 *         )
		 *     )
		 */
		public function get_types() {
			if (!$this->check_permission()) return array();

			global $o_package_levels;

			$a_retval = array(
				'Buy-In'=>array(
					'preferred'=>array(
						'absolute_vars'=>array(
							'name'=>'Preferred',
							'amount'=>4500,
							'apply_commission'=>0
						),
						'relative_vars'=>array(
							'expiration'=>date("Y-m-d H:i:s",strtotime("+2 weeks"))
						)
					),
					'premier'=>array(
						'absolute_vars'=>array(
							'name'=>'Premier',
							'amount'=>9000,
							'apply_commission'=>0
						),
						'relative_vars'=>array(
							'expiration'=>date("Y-m-d H:i:s",strtotime("+2 weeks"))
						)
					)
				),
				'Custom Payment'=>array(
					'reseller_conference'=>array(
						'absolute_vars'=>array(
							'name'=>'Reseller Conference',
							'amount'=>500,
							'apply_commission'=>0
						),
						'relative_vars'=>array(
							'expiration'=>((strtotime("now") < strtotime("January 10")) ? date("Y-m-d H:i:s",strtotime("January 10")) : date("Y-m-d H:i:s",strtotime("January 10 +1 year")))
						)
					)
				)
			);

			$a_retval['License'] = array(
				'Lavu Pro (Platinum2)'=>array(
					'absolute_vars'=>array(
						'name'=>'Lavu Pro (Platinum2)',
						'amount'=>2495.00,
						'apply_commission'=>1
					),
					'relative_vars'=>array(
						'expiration'=>"2013-11-11 00:00:00"
					)
				),
				'Lavu Pro Upgrade (Gold2 to Platinum2)'=>array(
					'absolute_vars'=>array(
						'name'=>'Lavu Pro Upgrade (Gold2 to Platinum2)',
						'amount'=>1000.00,
						'apply_commission'=>1
					),
					'relative_vars'=>array(
						'expiration'=>"2013-11-11 00:00:00"
					)
				),
				'Lavu Pro Upgrade (Silver2 to Platinum2)'=>array(
					'absolute_vars'=>array(
						'name'=>'Lavu Pro Upgrade (Silver2 to Platinum2)',
						'amount'=>1000.00,
						'apply_commission'=>1
					),
					'relative_vars'=>array(
						'expiration'=>"2013-11-11 00:00:00"
					)
				)
			);

			return $a_retval;
		}

		/**
		 * Used in Manage->Advanced Tools Functions->Distributor Tools to create or update custom purchases.
		 * @param  String	$s_distro The username of the currently selected distributor
		 * @return String	          A string command for send_ajax_call_from_form
		 */
		public function manage_ajax($s_distro) {
			if (!$this->check_permission()) return "print error[*note*]You are missing the required permissions.";
			if (!isset($_POST['mode']))
				return "print error[*note*]Mode not found.";

			// check if the user is logged in
			require_once(dirname(__FILE__)."/../../../login.php");
			if (!function_exists('account_loggedin') || !account_loggedin())
				return "print error[*note*]You must be logged in to manage to use this tool.";

			// get the vars to create/update with
			$a_create_vars = array();
			$a_distro_names = array();
			$s_details = "";
			foreach($_POST as $k=>$v) {
				if (strpos($k, $_POST['mode']."_custom_purchase_") === 0) {
					$s_name = str_replace($_POST['mode']."_custom_purchase_","",$k);
					if ($v == "" && ($s_name != "purchased_date" || $_POST['mode'] != "update"))
						return "print error[*note*]Please provide a value for {$s_name}.";
					if ($s_name == "distros") {
						$a_distro_names = explode(",", $v);
					} else if ($s_name == "details") {
						$s_details = $v;
					} else if ($s_name == "usernames") {
					} else {
						$a_create_vars[$s_name] = $v;
					}
				}
			}

			switch($_POST['mode']) {
			case "create":

				// check the distributor names
				$s_in_clause = ConnectionHub::getConn('poslavu')->getDBAPI()->arrayToInClause($a_distro_names, TRUE);
				$distro_query = mlavu_query("SELECT GROUP_CONCAT(`username`) AS `usernames` FROM `poslavu_MAIN_db`.`resellers` WHERE `access` != '' AND `access` != 'deactivated' AND `username` != '' AND `username` {$s_in_clause}");
				if ($distro_query === FALSE || mysqli_num_rows($distro_query) == 0)
					return "print error[*note*]mysql error";
				$distro_read = mysqli_fetch_assoc($distro_query);
				$a_distros = explode(",", $distro_read['usernames']);
				$a_unrecognized_names = array();
				foreach($a_distro_names as $s_name)
					if (!in_array($s_name, $a_distros))
						$a_unrecognized_names[] = $s_name;
				if (count($a_unrecognized_names) > 0)
					return "print error[*note*]The following reseller names are unrecognized: ".implode(", ", $a_unrecognized_names);

				// check the expiration time and the amount
				if (strtotime($a_create_vars['expiration']) < strtotime('now') && $a_create_vars['expiration'] !== "")
					return "print error[*note*]The expiration time must be in the future.";
				if ((int)$a_create_vars['amount'] <= 0 || (float)(int)$a_create_vars['amount'] != (float)$a_create_vars['amount'])
					return "print error[*note*]The amount must be a positive whole number.";

				// create the custom payment
				$s_created_date = date("Y-m-d H:i:s");
				$o_history = HistoryObject::create_standard_history("created", $s_details);
				$s_history = $o_history->toString();
				foreach($a_distro_names as $s_name) {
					$a_insert_vars = array("type"=>$a_create_vars['type'], "name"=>$a_create_vars['name'], "username"=>$s_name, "created"=>$s_created_date, "expiration"=>$a_create_vars['expiration'], "purchased_date"=>"", "amount"=>$a_create_vars['amount'], "_deleted"=>"0", "json_history"=>$s_history, "apply_commission"=>$a_create_vars['apply_commission']);
					$s_insert_string = ConnectionHub::getConn('poslavu')->getDBAPI()->arrayToInsertClause($a_insert_vars);
					mlavu_query("INSERT INTO `poslavu_MAIN_db`.`reseller_custom_purchases` {$s_insert_string}", $a_insert_vars);
				}

				break;
			case "update":

				// set some values
				$a_update_vars = $a_create_vars;
				$s_id = $a_update_vars['id'];
				$a_custom_purchases = ConnectionHub::getConn('poslavu')->getDBAPI()->getAllInTable('reseller_custom_purchases', array('id'=>$s_id), TRUE);

				// check that the custom purchase exists
				if (count($a_custom_purchases) == 0)
					return "print error[*note*]Could not find that custom purchase.";

				// find the changed values
				$a_changed = array();
				foreach($a_update_vars as $k=>$v) {
					if ($a_custom_purchases[0][$k] != $v) {
						$a_changed[] = array($k=>$v);
					}
				}
				if (count($a_changed) == 0)
					return "print error[*note*]Nothing has been changed.";
				$s_change_string = " (changes: ".LavuJson::json_encode($a_changed).")";

				// update the history
				$o_history = new HistoryObject($a_custom_purchases[0]['json_history']);
				$a_history_update = array('action'=>'updated', 'details'=>$s_details.$s_change_string);
				$o_history->updateHistory($a_history_update);
				$a_update_vars['json_history'] = $o_history->toString();

				// update the database
				unset($a_update_vars['id']);
				$s_update_clause = ConnectionHub::getConn('poslavu')->getDBAPI()->arrayToUpdateClause($a_update_vars);
				mlavu_query("UPDATE `poslavu_MAIN_db`.`reseller_custom_purchases` {$s_update_clause} WHERE `id`='[id]'", array_merge($a_update_vars, array('id'=>$s_id)));
			}

			return "set item html[*note*].manage_grouping.distro_custom_purchases[*findby*]".$this->manage($s_distro);
		}

		/**
		 * Retrieves the custom purchases associated with the given distributor
		 * @param  String	$s_distro        The username of the distributor
		 * @param  boolean	$b_check_deleted If TRUE, ignores any custom purchases marked as deleted
		 * @return Array	                 The puchases in the form array("available"=>array(rows from db), "expired"=>array(...), "purchased"=>array(...))
		 */
		public function get_purchases($s_distro, $b_check_deleted = FALSE, $b_print_query = FALSE) {
			if (!$this->check_permission()) return array('available'=>array(), 'expired'=>array(), 'purchased'=>array());

			$s_deleted = $b_check_deleted ? "AND `_deleted`='0'" : "";
			$a_purchase_db = array(
				"available"=>ConnectionHub::getConn('poslavu')->getDBAPI()->getAllInTable('reseller_custom_purchases', array('username'=>$s_distro, 'expiration'=>date("Y-m-d H:i:s")), TRUE, array('whereclause'=>"WHERE `username`='[username]' AND `purchased_date`='' {$s_deleted} AND `expiration` > '[expiration]'", "print_query"=>$b_print_query)),
				"expired"=>ConnectionHub::getConn('poslavu')->getDBAPI()->getAllInTable('reseller_custom_purchases', array('username'=>$s_distro, 'expiration'=>date("Y-m-d H:i:s")), TRUE, array('whereclause'=>"WHERE `username`='[username]' AND `purchased_date`='' {$s_deleted} AND `expiration` < '[expiration]'", "print_query"=>$b_print_query)),
				"purchased"=>ConnectionHub::getConn('poslavu')->getDBAPI()->getAllInTable('reseller_custom_purchases', array('username'=>$s_distro, 'expiration'=>date("Y-m-d H:i:s")), TRUE, array('whereclause'=>"WHERE `username`='[username]' AND `purchased_date` != '' {$s_deleted}", "print_query"=>$b_print_query))
			);
			return $a_purchase_db;
		}

		/**
		 * Build one row to be displayed in Manage->Advanced Tools->Distributor Tools
		 * @param  Array	$a_db_rows      The rows directly from `reseller_custom_purchases` to be drawn
		 * @param  Boolean	$b_disabled     Whether to draw these rows with disabled or enabled gui elements
		 * @param  Array	$a_type_options The options to be displayed for custom purchase types (eg "<option>value<option>")
		 * @param  Array	$a_name_options The options to be displayed for custom purchase names (eg "<option>value<option>")
		 * @param  String	$s_id           The id used for the group, eg "available" or "purchased"
		 * @param  String	$s_distro       The distributor's name that is currently selected
		 * @return String	                The html for several rows
		 */
		public function build_mod($a_db_rows, $b_disabled, $a_type_options, $a_name_options, $s_id, $s_distro) {
			if (!$this->check_permission()) return "You don't have the required permissions.";

			// get some values
			$s_retval = "";
			$s_disabled = ($b_disabled) ? " DISABLED" : "";
			$s_id = "distro_custom_purchases_update_form_$s_id";
			$i = 0;
			$a_apply_commission_options = array("<option>No</option><option>Yes</option>");

			// check if there are any rows
			if (count($a_db_rows) == 0) {
				return "<font style='color:#aaa;'>None</font>";
			}

			// draw the table head
			ob_start();
			?>
			<table style="border:0px none;" cellspacing="0px">
				<tr>
					<td> Type: </td><td> Name: </td><td> Created: </td><td> Expiration: </td><td> Purchased Date: </td><td> Amount: </td><td> Apply Commission: </td><td> Deleted: </td><td> History: </td><td> Update: </td>
				</tr>
			<?php
			$s_retval .= ob_get_contents();
			ob_end_clean();

			// draw the rows
			foreach($a_db_rows as $a_db_row) {

				// set some values
				$id = $s_id.'_'.$i;
				$i++;

				// find the selected option
				$s_type = $a_db_row['type'];
				$s_name = $a_db_row['name'];
				$s_apply_commission_selection = ((string)$a_db_row['apply_commission'] == "1") ? "Yes" : "No";
				$s_type_options = str_replace(">{$s_type}<", " SELECTED>{$s_type}<", implode("", $a_type_options));
				$s_name_options = str_replace(">{$s_name}<", " SELECTED>{$s_name}<", implode("", $a_name_options));
				$s_apply_commission_options = str_replace(">{$s_apply_commission_selection}<", " SELECTED>{$s_apply_commission_selection}<", implode("", $a_apply_commission_options));

				// draw the row
				ob_start();
				?>
				<tr id="<?= $id ?>">
					<td><select name="update_custom_purchase_type" <?= $s_disabled ?>><?= $s_type_options ?></select></td>
					<td><select name="update_custom_purchase_name" <?= $s_disabled ?>><?= $s_name_options ?></select></td>
					<td><input type="text" name="update_custom_purchase_created" value="<?= $a_db_row['created'] ?>" DISABLED /></td>
					<td><input type="text" name="update_custom_purchase_expiration" value="<?= $a_db_row['expiration'] ?>" <?= $s_disabled ?> /></td>
					<td><input type="text" name="update_custom_purchase_purchased_date" value="<?= $a_db_row['purchased_date'] ?>" DISABLED /></td>
					<td><input type="text" name="update_custom_purchase_amount" value="<?= $a_db_row['amount'] ?>" <?= $s_disabled ?> /></td>
					<td><select name="update_custom_purchase_apply_commission" <?= $s_disabled ?>><?= $s_apply_commission_options ?></select></td>
					<td><input type="checkbox" name="update_custom_purchase__deleted" <?= ((int)$a_db_row['_deleted'] ? "CHECKED" : "") ?> <?= $s_disabled ?> /></td>
					<td><span>
						<a onclick='Lavu.Gui.drawHistory($("#<?= $id ?>")[0], $(this).siblings().val());' <?= $s_disabled ?>>Show</a>
						<input type="hidden" value='<?= str_replace("'", "", $a_db_row['json_history']) ?>' />
					</span></td>
					<td><font <?= ($b_disabled ? 'style="display:none;"' : "")?>>
							<input type="button" value="Update" onclick="Lavu.Manage.DistroCustomPurchases.Update.submit('<?= $id ?>');" />
						</font>
						<label class="errors"></label>
						<input type="hidden" name="update_custom_purchase_id" value="<?= $a_db_row['id'] ?>" />
						<input type="hidden" name="update_custom_purchase_details" value="" />
						<input type="hidden" name="command" value="manage_distributor_custom_purchase_ajax" />
						<input type="hidden" name="distributor" value="<?= $s_distro ?>" />
						<input type="hidden" name="mode" value="update" />
					</td>
				</tr>
				<?php
				$s_retval .= ob_get_contents();
				ob_end_clean();
			}

			// draw the table foot
			ob_start();
			?>
			</table>
			<?php
			$s_retval .= ob_get_contents();
			ob_end_clean();

			return $s_retval;
		}

		/**
		 * Provides a form for creating and/or updating distributor custom purchases.
		 * The selected distributor is used as the default distributor to create custom purchases for.
		 * @param  string $s_distro the username of the distributor
		 * @return string           the raw html/javascript to use
		 */
		public function manage($s_distro) {
			if (!$this->check_permission()) return "You don't have the required permissions.";

			// get some values
			$a_distro_usernames = ConnectionHub::getConn('poslavu')->getDBAPI()->getAllInTable('resellers', NULL, TRUE, array('selectclause'=>'`username`', 'whereclause'=>"WHERE `access` != '' AND `access` != 'deactivated' AND `username` != ''", 'collapse_arrays'=>TRUE));
			sort($a_distro_usernames);
			$a_purchase_types = $this->get_types();
			$s_purchase_types = LavuJson::json_encode($a_purchase_types);
			$a_purchase_db = $this->get_purchases($s_distro);

			// get the type options and name options strings
			$a_type_options = array("<option></option>");
			$a_name_options = array("<option></option>");
			foreach($a_purchase_types as $s_type=>$a_type) {
				$a_type_options[] = "<option>{$s_type}</option>";
				foreach($a_type as $s_name=>$a_name) {
					$a_name_options[] = "<option>{$s_name}</option>";
				}
			}

			// get the list of custome purchases strings
			$a_purchase_string = array(
				"available"=>$this->build_mod($a_purchase_db['available'], FALSE, $a_type_options, $a_name_options, "available", $s_distro),
				"purchased"=>$this->build_mod($a_purchase_db['purchased'], TRUE, $a_type_options, $a_name_options, "purchased", $s_distro),
				"expired"=>$this->build_mod($a_purchase_db['expired'], FALSE, $a_type_options, $a_name_options, "expired", $s_distro),
			);

			// add_special_license_type
			// remove_special_license_type
			// draw the form
			ob_start();
			?>
			<div id='distro_custom_purchases_create_form'>
				Create new custom purchase: <label style="display:inline;" class="errors"></label>
				<div>
					Affected Distributors: <input type="text" value="<?= $s_distro ?>" name="create_custom_purchase_distros" /><input type="button" name="add_distro" value="+" onclick="add_create_custom_purchase_distro();" /><input type="button" name="remove_distro" value="-" onclick="remove_create_custom_purchase_distro();" /><input type="button" name="add_all_distros" value="all" onclick="add_create_custom_purchase_distro_all();" /><input type="hidden" value="<?php echo implode(',', $a_distro_usernames); ?>" name="values_create_custom_purchase_usernames" />
					Purchase Type: <select onchange="Lavu.Manage.DistroCustomPurchases.Create.selectType(this);" name="create_custom_purchase_type"><?= implode("", $a_type_options) ?></select>
					<font class="secondary name" style="display:none;">Name: <select name="create_custom_purchase_name" onchange="Lavu.Manage.DistroCustomPurchases.Create.selectName(this);"></select></font>
					<font class="secondary expiration tertiary" style="display:none;">Expiration: <input type="text" name="create_custom_purchase_expiration" placeholder="<?= date('Y-m-d H:i:s') ?>" /></font>
					<font class="secondary amount tertiary" style="display:none;">Amount: <input type="text" name="create_custom_purchase_amount" placeholder="4500" /></font>
					<font class="secondary apply_commission tertiary" style="display:none;">Apply Commission: <select name="create_custom_purchase_apply_commission"><option value="0">No</option><option value="1">Yes</option></select></font>
					<font class="secondary submit" style="display:none;"><input type="button" value="Submit" onclick="Lavu.Manage.DistroCustomPurchases.Create.submit();" /></font>
					<input type="hidden" name="create_custom_purchase_created" value="<?= date('Y-m-d H:i:s') ?>" />
					<input type="hidden" name="distributor" value="<?= $s_distro ?>" />
					<input type="hidden" name="create_custom_purchase_details" value="" />
					<input type="hidden" name="command" value="manage_distributor_custom_purchase_ajax" />
					<input type="hidden" name="mode" value="create" />
				</div><br />
			</div>
			<div id='distro_custom_purchases_available_form'>
				Available:
				<div>
					<?= $a_purchase_string['available'] ?>
				</div><br />
			</div>
			<div id='distro_custom_purchases_purchased_form'>
				Purchased:
				<div>
					<?= $a_purchase_string['purchased'] ?>
				</div><br />
			</div>
			<div id='distro_custom_purchases_expired_form'>
				Expired (and not purchased):
				<div>
					<?= $a_purchase_string['expired'] ?>
				</div><br />
			</div>
			<?php
			$s_form = ob_get_contents();
			ob_end_clean();

			// draw the complementary javascript
			ob_start();
			?>
			<script type='text/javascript'>
				if (!Lavu.Manage)
					Lavu.Manage = {};
				Lavu.Manage.DistroCustomPurchases = {
					distributor: '<?= $s_distro ?>',
					purchase_types: <?= $s_purchase_types ?>,
					Create: {
						type: "",
						selectType: function (element) {
							var jselect = $(element);
							var type = jselect.val();
							var inputs = this.getInputs(jselect);
							if (type == "" || this.type != "") {
								$.each(inputs, function(k,v) {v.jfont.hide();});
								if (type == "") {
									this.type = type;
									return;
								}
							}
							var purchase_types = Lavu.Manage.DistroCustomPurchases.purchase_types;
							kill_children(inputs.name.jinput);
							inputs.name.jinput.append($("<option></option>"));
							$.each(purchase_types[type], function(k,v){
								inputs.name.jinput.append($("<option value='"+k+"'>"+v.absolute_vars.name+"</option>"));
							});
							inputs.name.jfont.show();
							this.type = type;
						},
						selectName: function (element) {
							var jselect = $(element);
							var name = jselect.val();
							var type = this.type;
							var inputs = this.getInputs(jselect.parent());
							var purchase_types = Lavu.Manage.DistroCustomPurchases.purchase_types;
							var values = {};
							if (name !== "") {
								$.each(purchase_types[type][name], function(k,v){
									$.each(v, function(k2,v2) {
										values[k2] = v2;
									});
								});
							}
							$.each(inputs, function(k,v) {
								if (name == "") {
									v.jfont.hide();
								} else {
									if (v.jfont.hasClass('tertiary')) {
										v.jinput.val(values[v.name]);
									}
									v.jfont.show();
								}
							});
						},
						getInputs: function (jelement) {
							var fonts = jelement.siblings("font.secondary");
							var retval = {};
							for(var i = 0; i < fonts.length; i++) {
								var font = fonts[i];
								var name = font.className.split(/\s+/)[1];
								var jfont = $(font);
								var jinput = jfont.find("input");
								if (jinput.length == 0)
									jinput = jfont.find("select");
								retval[name] = {jfont: jfont, name: name, jinput: jinput};
							}
							return retval;
						},
						submit: function() {
							var jerror = $("#distro_custom_purchases_create_form").find("label.errors");
							jerror.css({color:"#aaa"});
							jerror.html("");
							jerror.append("Processing...");
							$("#distro_custom_purchases_create_form").find("input[name=create_custom_purchase_details]").val(prompt("Please provide some details explaining the purpose of this custom purchase."));
							send_ajax_call_from_form("/manage/misc/AdvancedToolsFunctions/manageDistributorLicenses.php", "distro_custom_purchases_create_form");
						}
					},
					Update: {
						submit: function(id) {
							var jerror = $("#"+id).find("label.errors");
							jerror.css({color:"#aaa"});
							jerror.html("");
							jerror.append("Processing...");
							$("#"+id).find("input[name=update_custom_purchase_details]").val(prompt("Please provide some details explaining the purpose of this update."));
							send_ajax_call_from_form("/manage/misc/AdvancedToolsFunctions/manageDistributorLicenses.php", id);
						}
					}
				};
			</script>
			<?php
			$s_javascript = ob_get_contents();
			ob_end_clean();

			return $s_form.$s_javascript;
		}

		/**
		 * Sets the status of a license to purchased by giving it a purchased date.
		 * Only succeeds if the license is not expired, not purchased, and not deleted, and the distro can be found (for buy-ins).
		 * @param  Int		$i_custom_purchase_id Id of the custom purchase.
		 * @param  String	$s_purchase_details   The details to add to the history about why the purchase happened.
		 * @param  Int		$i_now                The time of the purchase (can be -1 for now).
		 * @param  String   $s_cost               The amount that was paid for the custom purchase, as a string
		 * @return boolean	                      TRUE on success, FALSE on failure (sets $this->error_msg)
		 */
		public function make_purchase($i_custom_purchase_id, $s_purchase_details, $i_now = -1, $s_cost = "") {
			if (!$this->check_permission()) {
				$this->error_msg = "You don't have the right permissions.";
				return FALSE;
			}

			// get some values
			if ($i_now == -1)
				$i_now = strtotime("now");
			$a_where_vars = array('id'=>$i_custom_purchase_id);

			// check that the row exists, that it isn't purchased or deleted,
			// and that it hasn't expired
			$a_custom_purchases = ConnectionHub::getConn('poslavu')->getDBAPI()->getAllInTable('reseller_custom_purchases', $a_where_vars, TRUE);
			if (count($a_custom_purchases) == 0) {
				$this->error_msg = "Can't find the custom purchase.";
				return FALSE;
			}
			if ($a_custom_purchases[0]['purchased'] != "") {
				$this->error_msg = "The ".$a_custom_purchases['type']." has already been purchased.";
				return FALSE;
			}
			if ($a_custom_purchases[0]['_deleted'] == '1') {
				$this->error_msg = "The ".$a_custom_purchases['type']." has been removed.";
			}
			$s_expiration = $a_custom_purchases[0]['expiration'];
			if ($s_expiration != "" && strtotime($s_expiration) < $i_now) {
				$this->error_msg = "The ".$a_custom_purchases['type']." has expired.";
				return FALSE;
			}

			// check if the type is a buy-in. If so, award points to the distributor.
			$a_distros = ConnectionHub::getConn('poslavu')->getDBAPI()->getAllInTable('resellers', array('username'=>$a_custom_purchases[0]['username']), TRUE);
			if (count($a_distros) == 0) {
				$this->error_msg = "The distributor ".$a_custom_purchases[0]['username']." can't be found.";
				return FALSE;
			}
			if (strtolower($a_custom_purchases[0]['type']) == 'buy-in') {
				$i_credits = (int)$a_distros[0]['credits'];
				$i_credits_new = $i_credits + (int)$a_custom_purchases[0]['amount'];
				mlavu_query("UPDATE `poslavu_MAIN_db`.`resellers` SET `credits`='{$i_credits_new}' WHERE `username`='[username]'", $a_custom_purchases[0]);
				$a_insert_vars = array(
					'datetime'=>date("Y-m-d H:i:s", $i_now),
					'resellerid'=>$a_distros[0]['id'],
					'resellername'=>$a_distros[0]['username'],
					'ipaddress'=>$_SERVER['REMOTE_ADDR'],
					'action'=>'buy-in add distro points',
					'details'=>'custom purchase #'.$a_custom_purchases[0]['id'],
					'credits_applied'=>$a_custom_purchases[0]['amount'],
					'username'=>$a_distros[0]['username']
				);
				$s_insert_string = ConnectionHub::getConn('poslavu')->getDBAPI()->arrayToInsertClause($a_insert_vars);
				mlavu_query("INSERT INTO `poslavu_MAIN_db`.`reseller_billing_log` {$s_insert_string}", $a_insert_vars);
			}

			// check if the type is a license. If so, give a license to the distributor
			if (strtolower($a_custom_purchases[0]['type']) == 'license') {
				$this->apply_license_for_purchase($a_custom_purchases[0], $i_now, $s_cost);
			}

			// set the purchase date and return true
			$s_date = date("Y-m-d H:i:s", $i_now);
			$o_history = new HistoryObject($a_custom_purchases[0]['json_history']);
			$a_history_update = array('action'=>'purchased', 'details'=>$s_purchase_details);
			$o_history->updateHistory($a_history_update);
			$s_history = $o_history->toString();
			$a_update_vars = array('purchased_date'=>$s_date, 'json_history'=>$s_history);
			$s_update_clause = ConnectionHub::getConn('poslavu')->getDBAPI()->arrayToUpdateClause($a_update_vars);
			mlavu_query("UPDATE `poslavu_MAIN_db`.`reseller_custom_purchases` {$s_update_clause} WHERE `id`='[id]'", array_merge($a_update_vars, $a_where_vars));

			// mail the distributor managers in charge
			mail("bryan.p@poslavu.com, kalynn@poslavu.com", "A distributor just made a custom purchase", "The distributor ".$a_distros[0]['username']." made a custom purchase of ".$a_custom_purchases[0]['type'].": ".$a_custom_purchases[0]['name']." for $".$a_custom_purchases[0]['amount']." (id# ".$a_custom_purchases[0]['id'].")", "From: noreply@poslavu.com");

			return TRUE;
		}

		/**
		 * Helper function for "make_purchase()"
		 * @param  array   $a_custom_purchase Information loaded from the database about the custom purchase.
		 * @param  integer $i_now             The time that the custom purchased happend at.
		 * @param  string  $s_cost            The amount that the distro paid for the custom purchase.
		 * @return none                       N/A
		 */
		private function apply_license_for_purchase($a_custom_purchase, $i_now, $s_cost) {
			global $o_emailAddressesMap;
			global $o_package_levels;

			// get the email to create if something fails
			$s_distro_name = $a_custom_purchase['username'];
			$s_to = $o_emailAddressesMap->getEmailsForAddressToString('distro_portal_errors');
			$s_subject = "Custom Purchase: License, Error No. ";
			$s_body = "The distributor ".$s_distro_name." just bought custom purchase #".$a_custom_purchase['id']."\nThe purchase was attempted at ".date("Y-m-d H:i:s", $i_now).".\nThe type is License but the license cannot be applied because the name \"".$a_custom_purchase['name']."\" isn't recognized. Please contact development to get hte problem sorted out and apply a license to the distributor's account.";
			$s_extra = "From: noreply@poslavu.com";

			// get the license name
			$a_license_name_parts = explode("(", $a_custom_purchase['name']);
			if (count($a_license_name_parts) < 2) {
				mail($s_to, $s_subject."1", $s_body, $s_extra);
				return;
			}
			$a_license_name_parts = explode(")", $a_license_name_parts[1]);
			if (count($a_license_name_parts) < 2) {
				mail($s_to, $s_subject."2", $s_body, $s_extra);
				return;
			}
			$s_license_name = ucfirst(strtolower(trim($a_license_name_parts[0])));
			$s_license_name = str_replace(array("Gold2 to platinum2", "Silver2 to platinum2"), array("Gold2 to Platinum2", "Silver2 to Platinum2"), $s_license_name);

			// check if the license is valid
			if ($o_package_levels->get_package_status($s_license_name) == "none" && $s_license_name !== "Gold2 to Platinum2" && $s_license_name !== "Silver2 to Platinum2") {
				error_log($s_license_name);
				mail($s_to, $s_subject."3", $s_body, $s_extra);
				return;
			}

			// get the distributor's id
			$query = mlavu_query("SELECT `id` FROM `poslavu_MAIN_db`.`resellers` WHERE `username`='[username]'", array('username'=>$s_distro_name));
			if ($query === FALSE || mysqli_num_rows($query) == 0) {
				mail($s_to, $s_subject."4", $s_body, $s_extra);
				return;
			}
			$query_read = mysqli_fetch_object($query);
			$s_distro_id = $query_read->id;

			// create the license
			mlavu_query("INSERT INTO `poslavu_MAIN_db`.`reseller_licenses` (`type`,`resellerid`,`resellername`,`cost`,`value`,`purchased`,`notes`) VALUES ('[type]','[resellerid]','[resellername]','[cost]','[value]','[purchased]','[notes]')", array('type'=>$s_license_name, 'resellerid'=>$s_distro_id, 'resellername'=>$s_distro_name, 'cost'=>$s_cost, 'value'=>$a_custom_purchase['amount'], 'purchased'=>date("Y-m-d H:m:i", $i_now), 'notes'=>"license created as custom purchase in manage (custom purchase #".$a_custom_purchase['id'].")"));
		}

	}

	global $o_distroToolsCustomPurchases;
	$o_distroToolsCustomPurchases = new ManageDistroToolsCustomPurchases();

?>
