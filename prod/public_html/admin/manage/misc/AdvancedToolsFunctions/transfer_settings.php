<!--<script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>-->
<div class='settings_transfer_container'>
	<!--<form id='settings_transfer_form'>-->
		<div class='disclamer'> Please select the tables you <b>do not</b> want to transfer</div>
		<input type='text' name='dataname_start' placeholder='Start Dataname'>
		<input type='text' name='dataname_end' placeholder='End Dataname'>
		<div class='multi_selector'>
			
		</div> 
		<input type='button' value='submit' onclick='transfer_settings()'/>
	<!--</form>-->
</div>

<script type='text/javascript'>
	function initialize(){
		get_table_names();
	}
	function build_selector(){
		var selector = document.getElementsByClassName("multi_selector")[0];
		var t_names  = get_table_names();
		
	}
	function get_table_names(){
		var names     = mai_do_ajax("/manage/misc/AdvancedToolsFunctions/transfer_settings_functions.php","function=get_table_names","POST", false);
		var decoded   = JSON.parse(names);
		var container = document.getElementsByClassName("multi_selector")[0];
		
		for(var i = 0; i < decoded.length; i++){		 
			var inner = document.createElement("div");
				inner.addEventListener("click", table_unselected);
				inner.innerHTML = decoded[i]['Tables_in_poslavu_DEV_db'];
				inner.className = "table_selected";
				container.appendChild(inner);
		}
	}
	function table_selected(elem){
		elem.target.style.backgroundColor = 'white';
		elem. target.className = 'table_selected';
		event.target.removeEventListener("click", table_selected);
		event.target.addEventListener("click",    table_unselected);
	}
	function table_unselected(elem){
		elem. target.style.backgroundColor='#aecd37';
		
		elem.target.className = '';
		event.target.removeEventListener("click", table_unselected);
		event.target.addEventListener("click",    table_selected);
	
	}
	function mai_do_ajax(ajax_url, urlString, type, async_setting){

		var return_val= '';
		$.ajax({
			url: ajax_url,
			type: type,
			data:urlString,
			async: async_setting,
			success: function (string){
				if(async_setting){
					return_val = string; 
				}else{
					return_val = string;	
				}
			}
		});
		return return_val;
	}
	function transfer_settings(){
		var dn_start		= document.querySelector("[name=dataname_start]").value;
		var dn_end		    = document.querySelector("[name=dataname_end]").value;
		var elements 		= document.getElementsByClassName('table_selected');
		var selected_tables = new Array();
		//alert(dn_start);
		//alert(dn_end);
		for( var i = 0; i < elements.length; i++){
			selected_tables.push(elements[i].innerHTML);	
		}
		var stringified = encodeURIComponent(JSON.stringify(selected_tables));
		mai_do_ajax("/manage/misc/AdvancedToolsFunctions/transfer_settings_functions.php","function=transfer_settings&dn_start="+dn_start+"&dn_end="+dn_end+"&selected_tables="+stringified,"POST", false)
	}
	initialize();
</script>
<style>
</style>

<?php


?>