<?php
	require_once($_SERVER['DOCUMENT_ROOT']."/manage/error_config.php");
	session_start();
	if( isset( $_REQUEST['render_frame'] ) ){
		$mode = $_REQUEST['mode'];
		$report = $_REQUEST['report'];
		$widget = $_REQUEST['widget'];
		header('Location: ' . "/cp/index.php?mode={$mode}&widget={$widget}&report={$report}" );
		die;
	}
	echo '<script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>';
	echo '<link rel="stylesheet" type="text/css" href="/cp/styles/styles.css">';

	require_once($_SERVER['DOCUMENT_ROOT']."/cp/resources/json.php");
	if(!function_exists("resource_path"))
		require_once($_SERVER['DOCUMENT_ROOT']."/cp/resources/core_functions.php");
	
	require_once($_SERVER['DOCUMENT_ROOT']."/cp/resources/help_box.php");
	

	$loggedin = (isset($_SESSION['posadmin_loggedin']))?$_SESSION['posadmin_loggedin']:false;
	if($loggedin=="") $loggedin = false;
	$loggedin_fullname = (isset($_SESSION['posadmin_fullname']))?$_SESSION['posadmin_fullname']:false;
	if($loggedin_fullname=="") $loggedin_fullname = $loggedin;
	$loggedin_email = (isset($_SESSION['posadmin_email']))?$_SESSION['posadmin_email']:false;
	if($loggedin_email=="") $loggedin_email = $loggedin;
	$loggedin_access = (isset($_SESSION['posadmin_access']))?$_SESSION['posadmin_access']:false;
	if($loggedin_access=="") $loggedin_access = "";
	$loggedin_lavu_admin = (isset($_SESSION['posadmin_lavu_admin']))?$_SESSION['posadmin_lavu_admin']:0;

	function can_access($area)
	{
		global $loggedin_access;
		if(trim(strtolower($loggedin_access))=="all")
			return true;
		else if(strpos($loggedin_access,$area)!==false)
			return true;
		else
			return false;
	}

	if($loggedin && can_access("reports"))
	{
		global $data_name;

		$row_str = '';
		if( isset($_GET['rowid'] ) ){
			$row_str = '&rowid='.$_GET['rowid'];
		}
		$forward_to = "/manage/misc/AdvancedToolsFunctions/report_builder.php?rmode=jiggy{$row_str}";
		$tablename = "reports_v2";
		$in_lavu=1;
		$using_manage= 1;
		$fields = array();

		//$fields[] = array("ID","id","readonly","","list:yes");
		//$fields[] = array("Active","active","bool","","list:yes,defaultvalue:1");
		//$fields[] = array("First Name","f_name","text","size:30","list:yes");
		//$fields[] = array("Last Name","l_name","text","size:30","list:yes");
		//$fields[] = array("Chain Id","chainid","text","size:30","list:yes");
		//$fields[] = array("Contents","contents","text","size:60","list:yes");
		$fields[] = array("Report Name (Identifier)","name","text","size:30","list:yes");
		$fields[] = array("Report Title","title","text","size:30","list:yes");
		$fields[] = array("Main Table","table","text","size:30","list:yes");
		$fields[] = array("Join","join","multi_text","type:text,width:40","list:yes");


		$fields[] = array("Where","where","multi_text","type:text,width:40");
		$fields[] = array("Group By","groupby","multi_text","type:text,width:40");
		$fields[] = array("Select","select","multi_text","type:text,width:40");
		$fields[] = array("Order By","orderby","multi_text","type:text,width:40");
		$fields[] = array("Selection","selection","text");
		$fields[] = array("Selection Column","selection_column","text");
		$fields[] = array("Special Code","special_code","textarea");

		$fields[] = array(speak("Submit"),"submit","submit");

		ob_start();
		require_once($_SERVER['DOCUMENT_ROOT']."/cp/resources/browse.php");
		$form_output = ob_get_contents();
		ob_end_clean();

		// echo '<pre style="text-align: left;">' . print_r( get_defined_vars(), true ) . '</pre>';

		$form_output = str_replace("'images/","'/cp/images/",$form_output);
		$form_output = str_replace("\"images/","\"/cp/images/",$form_output);
		$form_output = "
		<script type=\"text/javascript\" src=\"https://ajax.googleapis.com/ajax/libs/jquery/1.6.2/jquery.min.js\"></script>
		<script type=\"text/javascript\" src=\"https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.14/jquery-ui.min.js\"></script>
		<style style=\"text/css\" >
			ul {
				list-style-image: URL('/manage/misc/AdvancedToolsFunctions/extensionLogos/hairy_small.png'); list-style-image-width:25px;
			}
		</style>
		" . $form_output;
		//echo "<textarea rows='40' cols='120'>" . str_replace("<","&lt;",$form_output) . "</textarea>";
		if( isset($_GET['rowid']) && !empty($_GET['rowid'] ) ){
			echo <<<HTML
			<script type="text/javascript">
			function iframe_rescale( ele ){
				// ele.style.height = ele.contentWindow.document.body.scrollHeight + "px";
				// ele.style.width = ele.contentWindow.document.body.scrollWidth + "px";
			}
			</script>
			<script language='javascript' src='/cp/reports_v2/ajax_lib.js'></script>
			<script language='javascript' src='/cp/reports_v2/floating_window.js'></script>
			<script type='text/javascript' src='/cp/resources/tigra/tcal.js'></script>
			<script type='text/javascript' src='/cp/reports_v2/chart.js'></script>
			<link rel='stylesheet' type='text/css' href='/cp/resources/tigra/tcal.css' />
			<link rel='stylesheet' type='text/css' href='/cp/styles/reports_v2.css' />
			<table>
				<tbody>
					<tr>
						<td valign="top" >{$form_output}</td>
						<td valign="top" >
HTML;
			$_GET['report'] =  $row_read['name'];//$_GET['rowid'];	
			require_once '/home/poslavu/public_html/admin/cp/areas/reports/v2_entry.php';

			echo <<<HTML
						</td>
					</tr>
				</tbody>
			</table>
			<script type='text/javascript' src='/cp/reports_v2/reports_v2.js'></script>
HTML;
		} else {
			echo $form_output;
		}
	}
