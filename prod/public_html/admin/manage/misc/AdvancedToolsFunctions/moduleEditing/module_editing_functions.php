<?php
	$devpath = $_POST['filev'];
	$mode = $_POST['mode'];
	
	if($mode && $mode == "copy"){
		$dev_file = $_SERVER['DOCUMENT_ROOT'] . '/dev/lib/modules/modules_list.txt';
		$live_file = $_SERVER['DOCUMENT_ROOT'] . '/lib/modules/modules_list.txt';
		
		file_put_contents($live_file, file_get_contents($dev_file));
		exit();
	}
	
	if( (!$devpath && $devpath != "") || !$mode){
		echo "Not Enough Arguments Specified";
		exit();
	}
	
	$path = $_SERVER['DOCUMENT_ROOT'] . '/' . $devpath . '/lib/modules/modules_list.txt';
	
	if($mode == "read"){
		$str = file_get_contents($path);
		if($str === FALSE){
			echo "File Read Error";
			exit();
		}
		$str = str_ireplace("\n", "", $str);
		$str = str_ireplace("\t", "", $str);
		echo $str;
	}
	else if($mode == "write"){
		$data = $_POST['data'];
		if(!$data){
			echo "No Data Specified";
			exit();
		}
		file_put_contents($path, $data);
		exit();
	}
	else{
		echo "Mode Not Found";
		exit();
	}