/**
 * Module Editor Javascript class
 * This is meant to allow for the swapping back and forth of modules.
 */
var ModuleEditor = function(json, moduleType, assignedDiv){
	/**
	 * PRIVATE FUNCTION(S)
	 **/
	var arrayCopy = function(arr){
		var result = [];
		var i = 0;
		arr.forEach(function(element, index, array){
			result[i++] = element;
		});
		
		return result;
	};
	
	/**
	 * PRIVATE VARIABLES
	 */
	var _moduleOBJ = JSON.parse(json); //Overall state of saved modules
	var _availableModules = arrayCopy(_moduleOBJ['Available Modules']);
	var _assignedModules = arrayCopy(_moduleOBJ[moduleType]);
	var _remainingModules = arrayCopy(_availableModules);
		_assignedModules.forEach(function(element, index, array){
			var ele = element.replace(/-/g,"");
			for(var i = 0; i < _remainingModules.length; i++){
				if(_remainingModules[i] == ele){
					_remainingModules.splice(i,1);
				}
			}
		});
	var _moduleType = moduleType; //Should be the name (Ex: Silver2 Modules)
	var _assignedDiv = assignedDiv;
	
	// Returns a copy of avialableModules... Ensures no editting to the available list
	this.getAvailableModules = function(){
		return arrayCopy(_availableModules);
	};
	
	/**
	 * Returns a javascript array of elements attributes as being assigned;.
	 * Returns a copy, preventing direct editting.
	 *
	 * @returns an Array containing string representations of assigned Modules
	 **/
	this.getAssignedModules = function(){
		return arrayCopy(_assignedModules);
	};
	
	/**
	 * Returns a javascript array of elements attributes that are considerred remaining.
	 * Returns a copy, preventing direct editting.  Combining Remaining with Assigned should
	 * yield avaialble modules.
	 *
	 * @returns an Array containing string representations of unassigned Modules
	 **/
	this.getRemainingModules = function(){
		return arrayCopy(_assignedModules);
	};
	
	/**
	 * Prints out the specified array as a series of Table Rows
	 **/
	this.toTableRows = function(arr){
		var result = "";
		arr.forEach(function(element, index, array){
			result += "<tr id=\"" + element.replace(/-/g,"") + "\" onclick=\"toggleSelect(this);\"><td>" + element + "</td></tr>";
		});
		
		return result;
	};
	
	/**
	 * Returns the html to represent the module_editor as interactive
	 **/
	this.toTable = function(){
		var contain = function(inner, left){
				var style = "";
				if(left){
					style = "text-align: left;";
				}
				return "<div style=\"display: inline-table; position: relative;" + style +  " \">" + inner + "</div>";
			};
	
		if(_moduleType == "Available Modules"){
			var availableTable = "<table><thead><th><b>Available Modules</b></th></thead><tbody id=\"availableModules\">" +this.toTableRows(_assignedModules) + "</tbody></table>";
			
			var addAnotherModuleText = "<br /><button onclick=\"module_editor.deleteAvailableSelections();\" style=\"width: 100%;\">Delete Selected</button><input id=\"module_placeholder\"type=\"text\" placeholder=\"New Module\" onblur=\"module_editor.addAvailableModule();\" style=\"width: 100%;\" />";
			
			return contain(availableTable + addAnotherModuleText, true);
			
		}
		else{
			var assignedTable = "<table><thead><th><b>Assigned Modules</b></th></thead><tbody id=\"assignedModules\">" + this.toTableRows(_assignedModules) + "</tbody></table>";
			var remainingTable = "<table><thead><th><b>Remaining Modules</b></th></thead><tbody id=\"remainingModules\">" + this.toTableRows(_remainingModules) + "</tbody></table>";
			var buttons = "<br /><br />\n" +
						  "<button onclick=\"module_editor.processAdditions(false);\">>>+</button><br />\n" + 
						  "<button onclick=\"module_editor.processAdditions(true);\">>>-</button><br />\n" +
						  "<button onclick=\"module_editor.processRemovals();\">&lt;&lt;&lt;</button><br />\n";
			return contain(remainingTable, true) + contain(buttons, false) + contain(assignedTable, true);
		}
	};
	
	/**
	 * Adds the module specified within the #module_placeholder textbox to the
	 * <b>_assignedModules</b> array.
	 **/
	this.addAvailableModule = function(){
		var modulename = $('#module_placeholder').val();
		modulename = modulename.replace(/[\-+*]/g, "");
		if(modulename){
			$('#module_placeholder').val('')
			var availableTable = document.getElementById('availableModules');
			for(var i = 0; i < availableTable.childNodes.length; i++){
				if(availableTable.childNodes[i].childNodes[0].innerHTML == modulename){
					return;
				}
			}
			
			var tr = document.createElement('tr');
			var td = document.createElement('td');
			tr.appendChild(td);
			tr.id=modulename;
			tr.onclick=function(){toggleSelect(this);};
			td.innerHTML = modulename;
			availableTable.appendChild(tr);
			_assignedModules.push(modulename);
			_assignedModules = _assignedModules.sort();
		}
	};
	
	/**
	 * Deletes the modules selected within the availableModules table from the 
	 * availableModules Table, and the <b>_assignedModules</b> array.
	 **/
	this.deleteAvailableSelections = function(){
		$('#availableModules').children('tr[selected]').each(function(){
			var id = this.id;
			var index = _availableModules.indexOf(id);
			if(index == -1){
				return;
			}
			
			_assignedModules.splice(index, 1);
			$(this).remove();
		});
	};
	
	/**
	 * This function will seek out the provided <b>module</b>, provided that it exists in
	 * _remainingModules, and move it to _assignedModules with a negative tag if <b>negate</b>
	 * is true.
	 **/
	this.assignModule = function(module, negate){
		var modulename = module.replace(/-/g,"");
		if(negate){
			var arr = module.split(".");
			var length = arr.length;
			arr[length-1] = "-" + arr[length-1];
			
			modulename = arr.join(".");
		}
		
		var index = _remainingModules.indexOf(module);
		if(index !== -1){
			_assignedModules.push(modulename);
			_remainingModules.splice(index, 1);
			
			_assignedModules = _assignedModules.sort()
			_remainingModules = _remainingModules.sort();
			return true;
		}	
		return false;
	};
	
	/**
	 * This function removes the specified <b>module</b> from the available modules. 
	 **/
	this.removeModule = function(module){
		var modulename = module.replace(/-/g, "");
		var modulerow = document.getElementById(modulename);
		modulerow.childNodes[0].innerHTML = modulename;
		
		var index = _assignedModules.indexOf(module);
		if(index === -1){
			index = _assignedModules.indexOf("-")
		}
		if(index !== -1){
			_remainingModules.push(modulename);
			_assignedModules.splice(index, 1);
			
			_remainingModules = _remainingModules.sort();
			_assignedModules = _assignedModules.sort();
			return true;
		}
		return false;
	};
	
	/**
	 * The update function redraws the table.  This should be done only if neccessary, but ultimately
	 * at least once in order to draw the table in the specified <b>_assignedDiv</b>;
	 **/
	this.update = function(){
		var table = this.toTable();
		$('#' + _assignedDiv).html(table);
	};
	
	/**
	 * This looks through all selected results within the <b>remainingModules</b> table, and will move them
	 * to the <b>assignedModules</b> table, as well as update the corresponding <b>_remainingModules</b> and
	 * <b>_assignedModules</b> arrays.
	 **/
	this.processAdditions = function(negate){
		$('#remainingModules').children('tr[selected]').each(function(){
			var id = this.id;
			this.removeAttribute('selected');
			module_editor.assignModule(id, negate);
			var assignedTable = document.getElementById('assignedModules');
			assignedTable.appendChild(this);
			var modName = this.childNodes[0].innerHTML;
			if(negate){
				//Add the '-'
				modName = modName.replace(/-/g, "");
				var packages = modName.split(".");
				packages[packages.length - 1] = "-" + packages[packages.length - 1];
				modName = packages.join(".");
				this.childNodes[0].innerHTML = modName;
			}
			
		});
	};
	
	/**
	 * This looks through all selected results within the <b>assignedModules</b> table, and will moves them to
	 * the <b>remainingModules</b> table.  It will also update the corresponding <b>_remainingModules</b> and
	 * <b>_assignedModules</b> arrays.
	 **/
	this.processRemovals = function(){
		if(_moduleType == "Available Modules"){
			$('#availableModules').children('tr[selected]').each(function(){
				module_editor.removeModule(this.firstChild.innerText);
				this.remove();
			});
		}
		else{
			$('#assignedModules').children('tr[selected]').each(function(){
				var id = this.id;
				this.removeAttribute('selected');
				module_editor.removeModule(this.firstChild.innerText);
				var remainingTable = document.getElementById('remainingModules');
				remainingTable.appendChild(this);			
			});
		}
	};
	
	/**
	 * Compares the assigned modules vs the original object, to see if anything is different.
	 *
	 * @returns true if changes are present, false otherwise.
	 **/
	this.hasChanges = function(){
		var focus = _moduleOBJ[_moduleType];
		
		if (_assignedModules.length != focus.length){
			return true;
		}
		
		var result = false;
		_assignedModules.forEach(function(element, index, array){
			if(focus.indexOf(element) === -1){
				result = true;
			}
		});
		
		return result;
	};
	
	/**
	 *	Saves the changes in the modules, provided they exist.
	 **/
	this.save = function(){
		if(!this.hasChanges()){
			return;
		}
		_moduleOBJ[_moduleType] = _assignedModules;		
		
		if (window.XMLHttpRequest){// code for IE7+, Firefox, Chrome, Opera, Safari
		  xmlhttp=new XMLHttpRequest();
		}
		else{// code for IE6, IE5
		  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
		}
		xmlhttp.onreadystatechange=function(){
		  if (xmlhttp.readyState==4 && xmlhttp.status==200){
		    	var json = xmlhttp.response;
		    	module_editor = new ModuleEditor(json, moduleType, divID);
		    	module_editor.update();
		    }
		}
		
		var initialVars="mode=write&filev=" + $('#modulefile').val() + "&data=" + JSON.stringify(_moduleOBJ);
		xmlhttp.open("POST","misc/AdvancedToolsFunctions/moduleEditing/module_editing_functions.php",true);
		xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
		//xmlhttp.responseType="json";
		xmlhttp.send(initialVars);
		
		return JSON.stringify(_moduleOBJ);
	};
}
ModuleEditor.prototype = new Object();
ModuleEditor.prototype.constructor = ModuleEditor;

function toggleSelect(item){
	if(item.getAttribute('selected') == ""){
		item.removeAttribute('selected');
		//item.style.backgroundColor = "";
	}
	else{
		item.setAttribute('selected', '');
		//item.style.backgroundColor = "green";
	}
}

function loadModuleEditor(divID){
	var moduleType = $('#module_package').val();
	
	if (window.XMLHttpRequest){// code for IE7+, Firefox, Chrome, Opera, Safari
	  xmlhttp=new XMLHttpRequest();
	}
	else{// code for IE6, IE5
	  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
	}
	xmlhttp.onreadystatechange=function(){
	  if (xmlhttp.readyState==4 && xmlhttp.status==200){
	    	var json = xmlhttp.response;
	    	module_editor = new ModuleEditor(json, moduleType, divID);
	    	module_editor.update();
	    }
	}
	
	var initialVars="mode=read&filev=" + $('#modulefile').val();
	xmlhttp.open("POST","misc/AdvancedToolsFunctions/moduleEditing/module_editing_functions.php",true);
	xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
	xmlhttp.send(initialVars);
}

function copyModulesOver(){
	if (window.XMLHttpRequest){// code for IE7+, Firefox, Chrome, Opera, Safari
	  xmlhttp=new XMLHttpRequest();
	}
	else{// code for IE6, IE5
	  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
	}
	xmlhttp.onreadystatechange=function(){
	  if (xmlhttp.readyState==4 && xmlhttp.status==200){
	    	var json = xmlhttp.response;
	    	if(json == ""){
		    	alert('successfully copied over');
	    	}
	    }
	}
	
	var initialVars="mode=copy";
	xmlhttp.open("POST","misc/AdvancedToolsFunctions/moduleEditing/module_editing_functions.php",true);
	xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
	xmlhttp.send(initialVars);
}