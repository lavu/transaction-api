<?php
ini_set("displau_errors",1);
if(isset($_POST['company']) && isset($_POST['email'])){

	function confirm_restaurant_shard($data_name) {
		$str = "";
		$letter = substr($data_name,0,1);
		if(ord($letter) >= ord("a") && ord($letter) <= ord("z"))
			$letter = $letter;
		else
			$letter = "OTHER";
		$tablename = "rest_" . $letter;

		$mainrest_query = mlavu_query("select * from `poslavu_MAINREST_db`.`$tablename` where `data_name`='[1]'",$data_name);
		if(mysqli_num_rows($mainrest_query))
		{
			$str .= $letter . ": " . $data_name . " exists";
		}
		else
		{
			$str .= $letter . ": " . $data_name . " inserting";

			$rest_query = mlavu_query("select `data_access`,`id`,`last_activity`,`jkey`,`data_name` from `poslavu_MAIN_db`.`restaurants` where `data_name`='[1]'",$data_name);
			if(mysqli_num_rows($rest_query))
			{
				$rest_read = mysqli_fetch_assoc($rest_query);

				$success = mlavu_query("insert into `poslavu_MAINREST_db`.`$tablename` (`data_access`,`last_activity`,`jkey`,`data_name`,`companyid`) values ('[data_access]','[last_activity]','[jkey]','[data_name]','[id]')",$rest_read);
				if($success) $str .= " <font color='green'>success</font>";
				else $str .= " <font color='red'>failed</font>";
			}
		}
		return $str;
	}

	$company = $_POST['company'];
	$email = $_POST['email'];
	$phone = $_POST['phone'];
	$firstname = $_POST['firstname'];
	$lastname = $_POST['lastname'];
	$address = $_POST['address'];
	$city = $_POST['city'];
	$state = $_POST['state'];
	$zip = $_POST['zip'];
	$countryInfo = explode('_', $_POST['country']);
	$country = $countryInfo[0];
	$countryCode = $countryInfo[1];
	$default_menu = $_POST['default_menu'];
	$override_default_menu = (isset($_POST['override_default_menu']))?$_POST['override_default_menu']:"";
	$account_type = $_POST['account_type'];
	$set_com_product = $_POST['set_com_product'];
	$package = $_POST['product'];
	$terminals = $_POST['terminals'];
	require_once("/home/poslavu/public_html/admin/cp/resources/core_functions.php");
	$maindb = "poslavu_MAIN_db";

	if($override_default_menu!="")
	{
		$exist_query = mlavu_query("select `data_name` from `poslavu_MAIN_db`.`restaurants` where `data_name`='[1]' and `notes` NOT LIKE '%AUTO-DELETED%' and `disabled`!=1 and `disabled`!=2 and `disabled`!=3 and `lavu_lite_server`=''",$override_default_menu);
		if(mysqli_num_rows($exist_query))
		{
			$exist_read = mysqli_fetch_assoc($exist_query);
			$default_menu = $exist_read['data_name'];
		}
		else
		{
			echo "Dataname not found: $override_default_menu<br>";
			exit();
		}
	}

	$package_info = array();
	if (!empty($package))
	{
		$package_info['package'] = $package;
	}

	if (!empty($terminals))
	{
		$package_info['ipads'] = $terminals;
		$package_info['ipods'] = $terminals * 5;
	}

	require_once("/home/poslavu/public_html/register/create_account.php");
	$result = create_new_poslavu_account($company,$email,$phone,$firstname,$lastname,$address,$city,$state,$zip,$default_menu,"",$account_type,$set_com_product,$package_info,$countryCode);
	$success = $result[0];
	if($success)
	{
		$username = $result[1];
		$password = $result[2];
		$dataname = $result[3];
		$company_id = $result[4];

		echo "<br>username: $username";
		echo "<br>password: $password";
		echo "<br>dataname: $dataname";

		if($default_menu=="hotel_lavu" || $default_menu=="default_hotel" || $default_menu=="hotel_default")
		{
			$edataname = $dataname;
			$edataname_valid = false;
			$rcount = 2;
			while(!$edataname_valid && $rcount < 20)
			{
				$rsp_available = get_folder_availability_from_server($edataname);
				if($rsp_available)
				{
					$edataname_valid = true;
				}
				else
				{
					$edataname = $dataname . $rcount;
				}
				$rcount++;
			}
			
			if($edataname_valid)
			{
				$create_token = e2_api_create_new_password(20);
				$create_key = e2_api_create_new_password(20);

				$new_rdb = "poslavu_".$dataname."_db";
				mlavu_query("update `[1]`.`locations` set `api_token`=AES_ENCRYPT('$create_token','lavutokenapikey')",$new_rdb);
				mlavu_query("update `[1]`.`locations` set `api_key`=AES_ENCRYPT('$create_key','lavuapikey')",$new_rdb);

				$settings = array();
				$settings['Control Panel Product'] = "Hotel";
				$settings['Poslavu Dataname'] = $dataname;
				$settings['Poslavu Api Key'] = $create_key;
				$settings['Poslavu Api Token'] = $create_token;
				$site_created = process_v2_create_site($edataname,$password,$company,"hotel",$settings);
				if($site_created)
				{
					echo "Integrated Site Created!<br>";
					$ersnetpath = "https://" . $edataname . ".ershost.com";
					$exist_query = mlavu_query("select * from `[1]`.`config` where `setting`='ers_netpath'",$new_rdb);
					if(mysqli_num_rows($exist_query))
					{
						mlavu_query("update `[1]`.`config` set `value`='[2]' where `setting`='ers_netpath'",$new_rdb,$ersnetpath);
					}
					else
					{
						mlavu_query("insert into `[1]`.`config` (`setting`,`location`,`value`,`type`) values ('[2]','[3]','[4]','[5]')",$new_rdb,"ers_netpath",1,$ersnetpath,"location_config_setting");
					}
					mlavu_query("update `poslavu_MAIN_db`.`restaurants` set `dev`='1' where `data_name`='[1]' limit 1",$dataname);
				}
				else
				{
					echo "failed to create integrated site...<br>";
				}
			}
			else
			{
				echo "Error: Could not create Integrated Account";
				exit();
			}
		}
		
		$credvars['username'] = $username;
		$credvars['dataname'] = $dataname;
		$credvars['password'] = $password;
		$credvars['company'] = $company;
		$credvars['email'] = $email;
		$credvars['phone'] = $phone;
		$credvars['firstname'] = $firstname;
		$credvars['lastname'] = $lastname;
		$credvars['address'] = $address;
		$credvars['city'] = $city;
		$credvars['state'] = $state;
		$credvars['zip'] = $zip;
		$credvars['country'] = $country;
		$credvars['default_menu'] = $default_menu;
		mlavu_query("insert into `poslavu_MAIN_db`.`signups` (`username`, `dataname`, `password`, `company`, `email`, `phone`, `firstname`, `lastname`, `address`, `city`, `state`, `zip`, `country`, `status`, `default_menu`) values ('[username]', '[dataname]', AES_ENCRYPT('[password]', 'password'), '[company]', '[email]', '[phone]', '[firstname]', '[lastname]', '[address]', '[city]', '[state]', '[zip]', '[country]', 'manual', '[default_menu]')",$credvars);
		confirm_restaurant_shard($dataname);
		//LP-7274
		//mlavu_query("insert into `poslavu_MAIN_db`.`signups` (`username`, `dataname`, `password`, `company`, `email`, `phone`, `firstname`, `lastname`, `address`, `city`, `state`, `zip`, `country`, `status`, `default_menu`) values ('[username]', '[dataname]', AES_ENCRYPT('[password]', 'password'), '[company]', '[email]', '[phone]', '[firstname]', '[lastname]', '[address]', '[city]', '[state]', '[zip]', '[country]', 'manual', '[default_menu]')",$credvars);
		mlavu_query("INSERT INTO poslavu_".$dataname."_db.`config` ( `location`, `setting`, `type`, `value`, `value2` ) VALUES ( '[1]', 'business_country', 'location_config_setting', '[2]', '[3]')", $company_id, $country, $countryCode);
	}
	else 
		echo "Failed to create account";
}
?>