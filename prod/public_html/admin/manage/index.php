<?php
	require_once($_SERVER['DOCUMENT_ROOT']."/manage/error_config.php");
	require_once("login.php");
	require_once ($_SERVER['DOCUMENT_ROOT']."/cp/resources/core_functions.php");

	//require_once("misc/ChatFunctions/getChatsFromDB.php");
	if( session_id() == '' ){
		session_start();
	}
	//mt_getmaxrand() == 2147483647

	$_SESSION['rand']="";
	for($i = 0; $i < 3; $i++){
		$_SESSION['rand'] .= rand(0, mt_getrandmax());
		//base64_encode($_SESSION['rand']);
	}

	if(account_loggedin()) // logged in
	{
		include './misc/Mobile_Detect.php';
		$detect = new Mobile_Detect();
		if ($detect->isMobile() && !isset($_GET['useMain'])) {
			 header( 'Location: http://admin.poslavu.com/manage/mobile' ) ;

		}
		echo'
		<!doctype html>
		<!--[if lt IE 8]>     <html class="no-js ie7 oldie" lang="en"><![endif]-->
		<!--[if IE 8]>        <html class="no-js ie8 oldie" lang="en"><![endif]-->
		<!--[if IE 9]>        <html class="no-js ie9 oldie" lang="en"><![endif]-->
		<!--[if gt IE 9]><!--><html class="no-js" lang="en"><!--<![endif]-->
			<head>
			<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
				<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
				<script type="text/javascript" src="./js/jquery/js/jquery-1.9.0.js"></script>
				<script type="text/javascript" src="./js/jquery/js/jquery-ui-1.10.0.custom.min.js"></script>
				<script type="text/javascript" src="./js/jquery/js/jquery-confirm.min.js"></script>
				<script type="text/javascript" src="./js/jquery/jquery.dataTables.min.js"></script>
				<script type="text/javascript" src="/manage/js/ajaxFileUpload.min.js"></script>
				<script type="text/javascript" src="./js/index.js"></script>
				<script type="text/javascript" src="./js/pdfobject.js"></script>
				<script type="text/javascript" src="./js/Tabs/LLS.js"></script>
				<script type="text/javascript" src="./js/upload.js"></script>
				<!--<script type="text/javascript" src="./js/easterEgg.js"></script>-->
				<script type="text/javascript" src="./js/Tabs/myAccount.js"></script>
			
				<script type="text/javascript" src="./js/lavuutil.js"></script>
				<script type="text/javascript" src="./js/lavumvc.js"></script>
				<script type="text/javascript" src="./js/lavubase.js"></script>
				<script type="text/javascript" src="/manage/js/Tabs/customelements.js"></script>
				<script type="text/javascript" src="./js/Tabs/advancedTools.js"></script>
				<script type="text/javascript" src="misc/AdvancedToolsFunctions/moduleEditing/module_editing.js"></script>
				<script type="text/javascript" src="/cp/scripts/select2-4.0.3.min.js"></script>
				<link href="/cp/css/select2-4.0.3.min.css" rel="stylesheet" />
				<link rel="stylesheet" type="text/css" href="./js/jquery/css/custom-theme/jquery-ui-1.10.0.custom.css" />
				<link rel="stylesheet" type="text/css" href="./css/style.css" />
				<link rel="stylesheet" type="text/css" href="./css/autoTable.css" />

				<!--
				<script type="text/javascript" src="./js/jquery/js/jquery-ui-1.8.23.custom.min.js"></script>
				<script type="text/javascript" src="./js/jquery/js/jquery-1.8.0.min.js"></script>
				<link rel="stylesheet" type="text/css" href="./js/jquery/css/ui-lightness/jquery-ui-1.8.23.custom-1.css" />
				-->
				<script type="text/javascript">
					(function(){
						Lavu.currentUser = new Lavu.User.UserAccount( "' . $_SESSION['posadmin_user_id'] . '","' . $_SESSION['admin_username'] . '", "' . $_SESSION['posadmin_fullname'] . '", "' . $_SESSION['posadmin_email'] . '", "' . $_SESSION['posadmin_access'] . '", " ' . $_SESSION['posadmin_lavu_admin'] . '", \''. $_SESSION['posadmin_options'] .'\', \''. $_SESSION['posadmin_type'] .'\', "' . $_SESSION['posadmin_default_pipeline_id'] .'", "' . $_SESSION['posadmin_default_stage_id'] . '");
						Lavu.User.UserManager.addUser( Lavu.currentUser );
					})();
				</script>

			</head>
			<title>ua_cp</title>
			<body class="bodClass">
		';

		$Manage= new Manage();
	}

	class Manage{

		private static $dataname;
		private static $resturantID;
		private static $name;
		private static $o_userOptions;
		public function __construct(){

			$mode=(isset($_GET['mode']))?$_GET['mode']:"";

			/*
				The reason I am doing this in the constructor is because its wayyy easier to just check to see if
				we just logged out here than to do something else :P
			*/
			if( $mode=='logout'){
				logout_account($_SESSION['admin_username']);
				header('Location: login.php') ;
			}
			else{

					$this->createBanner();
					$this->getUserOptions();
					$this->createNotificationBar();
					$this->createMyAccountSection();
					$this->createAdvancedToolsSection();
					$this->createDistributorSearchSection();
					$this->createCompanyDocumentsSection();
					$this->createDevToolsSection();
					$this->createSupportToolsSection();
					if(can_access("customers"))
						$this->createBody();

					$this->makeExpiring();

					if(isset($_GET['goToAccount'])){

						$dataname= $_GET['goToAccount'];
						if($dataname !=''){
							if($this->checkDatanameValidity($dataname)){
								echo "<script type='text/javascript'> populateResults('$dataname');</script>";
								$_GET['goToAccount']='';
							}
							else
								echo "<script type='text/javascript'>alert('Invalid Dataname please try again')</script>";
						}
					}
			}
		}
		private function getUserOptions() {
			require_once(dirname(__FILE__).'/misc/MyAccount/userOptions.php');
			Manage::$o_userOptions = new USER_OPTIONS();
		}
		private function createNotificationBar(){

		}
		private function checkDatanameValidity($dataname){

			if( mysqli_num_rows(mlavu_query("select * from `poslavu_MAIN_db`.`restaurants` where `data_name`='[1]'",$dataname)))
				return true;
			else
				return false;
		}
		private function createBanner(){
			//$username= (explode( $_SESSION['posadmin_email'],'@'))[0];
			$username= $_SESSION['admin_username'];
			$fullname= $_SESSION['posadmin_fullname'];
			//$username='';
			Manage::$name = $username;
			if($username==''){
				echo "<script type='text/javascript'> window.location.replace('http://admin.poslavu.com/manage/index.php?mode=logout');</script>";
			}

			echo "
			<div id='loading'></div>
			<div id='loadingText'>Loading...
				<br><input type=button style='border:0; background-color:#aecd37; color:white; cursor:pointer' value='cancel load' onclick='window.location.reload()'>
			</div>
			<div class='header'>

							<notification-tray-element id='notifications'></notification-tray-element>
			";

			$this->createButtons();
			echo "
				<nav id='accountNavigation' >
					<div onclick ='goToAccount(\"{$username}\");' style='cursor:pointer; text-align:left;' >
						<div class='account_icon'></div><div id= 'displayAccountInfo' style='display:none;'>
							{$username}
						</div>
					<div class='adminfullname'>{$fullname}</div>
				</div>
					<div onclick='window.location=\"index.php?mode=logout\"'><div class='logout_icon'></div></div>
				</nav>
			</div>
			";
		}
		private function createLargeCompanyDisplay() {
			if (is_object(Manage::$o_userOptions)) {
				$s_display_name = Manage::$o_userOptions->get_option('display_company_name');
				?>
					<div id="display_company_name" style="font-size:20px; top:6px; right:6px; position:absolute; <?php if ($s_display_name != 'top') echo ' display:none;'; ?>">
						<div style="display:inline-block; color:black; margin-left:20px;" onclick="SelectText($(this).find('.company_name'))">
							<div style="background-color:lightgray; display:inline-table; height:24px; padding:3px;">cn</div><div style="background-color:white; display:inline-block; height:24px; padding:3px; display:inline-table; overflow:hidden;" class="company_name">I am a company name</div>
						</div>
						<div style="display:inline-block; color:black; margin-left:20px;" onclick="SelectText($(this).find('.data_name'))">
							<div style="background-color:lightgray; display:inline-table; height:24px; padding:3px;">dn</div><div style="background-color:white; display:inline-block; height:24px; padding:3px; display:inline-table; overflow:hidden;" class="data_name">I am a dataname</div>
						</div>
					</div>
				<?php
			} else {
				error_log('could not load user options for '.Manage::$name);
			}
		}
		private function createButtons(){
			echo '<nav id="page_button_container">';
			echo '<div><div id ="company_search_button"><div>Company Search</div></div></div>';
			if(can_access('distro_search'))
				echo '<div><div id ="distributor_search_button"><div>Distributor Search</div></div></div>';

			if(can_access('customers'))
				echo '<div><div id ="advanced_tools"><div>Advanced Tools</div></div></div>';
			echo '<div><div id="dev_tools"><div>Dev Tools</div></div></div>';
			echo '<div><div id="support_tools"><div>Support Tools</div></div></div>';
			echo '<div><div id="company_docs_button"><div>Documents</div></div></div>';


			echo '</nav>';
		}
		private function createCompanyDocumentsSection(){
			echo "<div id='companyDocs' style='display:none'> </div>";
		}
		private function createDevToolsSection(){
			echo "<div id='dev_tools_section' style='display:none'> </div>";
		}
		private function createSupportToolsSection(){
			echo "<div id='support_tools_section' style='display:none'> </div>";
		}
		
		private function createAdvancedToolsSection(){
			echo " <div id= 'advancedToolsSection' style='display:none'> </div>";
		}
		private function createMyAccountSection(){
			echo "<div id='myAccountSection' style='display:none'></div>";
		}
		private function createDistributorSearchSection(){
			echo "<div id='distroSearchSection' style='display:none'></div>";
		}

		private function createBody(){
			?>
			<canvas id = 'c' style='margin: 0;z-index: 1000;top: 120px;left: -72px;display:none'> </canvas>
			<div id='messageBox' title='Messages'><p id='alertMessage'></p></div>
			<div class="center">
				<table id="searchTable">
					<tr>
						<td>
							<div class="search" style="display:inline-block;">
								<input type="text" id="searchInput" placeholder="Company Search" name="search" style="height:24px; width:394px;font-size:20px;z-index:100;" onfocus="setTimeout(function(){SelectText($('#searchInput'));}, 50)" />
							</div>
						</td>
					</tr>
					<tr>
						<td>
							<table>
								<tr>
									<td>
										<div class="previousSearches"></div>
									</td>
								</tr>
							</table>
						</td>
					</tr>
					<tr>
						<td>
							<div class="advancedSearch">
								use advanced search: <input type="checkbox" id="useAdvancedSearch"
								onchange='
									if( $("#advancedSearchSettings").css("display")=="none")
										$("#advancedSearchSettings").css("display","");
									else
										$("#advancedSearchSettings").css("display", "none");
								'>
								Use restaurant ID<input type='checkbox' id='useID' />
							</div>
						</td>
					</tr>
					<tr id="advancedSearchSettings" style="display:none;">

						<td >
							<table id="searchOptions">

								<tr>
									<td style= "border-bottom:1px solid black; ">
										 <small>Include First Name: <input type=checkbox id="useFirstName" style="float:right;" onchange =
										 "if($('#useFname').css('display')=='none'){
										 	$('#useFname').css('display', '');
										 }else{
										 	$('#useFname').css('display', 'none');
										 }"
										 ></small>
										<br> <input type="text" id="useFname" style="display:none" class="advancedSearch">
									</td>
									<td style= "border-bottom:1px solid black; ">
										 <small>
										 Use Level: <input type=checkbox id="useLevel" style="float:right;"
										 onchange =
										 		"if($('#levelDiv').css('display')=='none'){
										 			$('#levelDiv').css('display', '');

										 		}else{
										 			$('#levelDiv').css('display', 'none');

										 		}"
										 >
										 <div id= "levelDiv" style="display:none" class="advancedSearch">
											<table>
												<tr>
													<td>
														<input type="radio" class="radioChange" name="level" value="Silver"><small>Silver
													</td>
													<td>
														<input type="radio" class="radioChange" name="level" value="Gold"><small>Gold
													</td>
												</tr>

												<tr>
													<td>
														<input type="radio" class="radioChange" name="level" value="Platinum"><small>Platinum
													</td>
													<td>
														<input type="radio" class="radioChange" name="level" value="Dev"><small>Dev
													</td>
												</tr>
												<tr>
													<td>
														<input type= 'radio' class="radioChange" name= 'level' value="Lite"> <small>Lite</small>
													</td>
												</tr>
											</table>
										</div>
										 </small>
									</td>
									<td style= "border-bottom:1px solid black; ">
										 <small>Filter By City: <input type=checkbox id="filterCity" style="float:right;"onchange =
										 "if($('#useCity').css('display')=='none'){
										 	$('#useCity').css('display', '');
										 }else{
										 	$('#useCity').css('display', 'none');
										 }"

										 >
										 </small>
										 	<br> <input type="text" id="useCity" style="display:none" class="advancedSearch">
									</td>
								</tr>
								<tr>
									<td style= "border-bottom:1px solid black; ">
										 <small>Include Last name: <input type=checkbox id="useLastName" style="float:right;" onchange =
										 "if($('#useLname').css('display')=='none'){
										 	$('#useLname').css('display', '');
										 }else{
										 	$('#useLname').css('display', 'none');
										 }"
										 ></small>
										<br> <input type="text" id="useLname" style="display:none" class="advancedSearch">

										 </small>
									</td>
									<td style= "border-bottom:1px solid black; ">
										 <small>Use Last Activity: <input type=checkbox id="useLastActivity" style="float:right;" onchange =
										 "if($('#useDate').css('display')=='none'){
										 	$('#useDate').css('display', '');
										 }else{
										 	$('#useDate').css('display', 'none');
										 }"
										 ></small>
										<br> <input type="text" id="useDate" style="display:none" class="advancedSearch">
									</td>
									<td style= "border-bottom:1px solid black; ">
										 <small>Filter By State: <input type=checkbox id="filterState"
										 onchange =
										 "if($('#useState').css('display')=='none'){
										 	$('#useState').css('display', '');
										 }else{
										 	$('#useState').css('display', 'none');
										 }"

										 >
										 </small>
										 	<br> <input type="text" id="useState" style="display:none" class="advancedSearch">
									</td>
								</tr>
								<tr >
									<td style= "border-bottom:1px solid black; ">
										 <small>Include company code: <input type=checkbox id="useCompanyCode" style="float:right;" onchange =
										 "if($('#useCcode').css('display')=='none'){
										 	$('#useCcode').css('display', '');
										 }else{
										 	$('#useCcode').css('display', 'none');
										 }"
										 ></small>
										<br> <input type="text" id="useCcode" style="display:none" class="advancedSearch">

										 </small>

									</td>
									<td style= "border-bottom:1px solid black;">
										 <small>Use Enabled or Disabled: <input type=checkbox id="useEnabledorDisabled"
										 onchange =
										 		"if($('#enabledDiv').css('display')=='none'){
										 			$('#enabledDiv').css('display', '');

										 		}else{
										 			$('#enabledDiv').css('display', 'none');

										 		}"
										 >
										 <div id= "enabledDiv" style="display:none">
											<input type="radio" name="enabled" class="radioChange" value="1">Disabled
											<input type="radio" name="enabled" class="radioChange" value="0"> Enabled
										</div>
										</small>
									</td>
									<td style= "border-bottom:1px solid black; ">
										 <small>Filter By Zip: <input type=checkbox id="filterZip" style="float:right;"
										 onchange =
										 "if($('#useZip').css('display')=='none'){
										 	$('#useZip').css('display', '');
										 }else{
										 	$('#useZip').css('display', 'none');
										 }"
										 ></small>
										<br> <input type="text" id="useZip" style="display:none" class="advancedSearch">

										 </small>

									</td>
								</tr>
								<tr>
									<td style= "border-bottom:1px solid black; ">
										 <small>Include Email: <input type=checkbox id="emailCheck" style="float:right;" onchange =
										 "if($('#useEmail').css('display')=='none'){
										 	$('#useEmail').css('display', '');
										 }else{
										 	$('#useEmail').css('display', 'none');
										 }"
										 ></small>
										<br> <input type="text" id="useEmail" class="advancedSearch" style="display:none">

										 </small>
									</td>
									<td style= "border-bottom:1px solid black; ">
										 <small>
										 	Use Status:
										 	<input type=checkbox id="useStatus" style="float:right;" onchange =
										 		"if($('#status').css('display')=='none'){
										 			$('#status').css('display', '');

										 		}else{
										 			$('#status').css('display', 'none');

										 		}"
										 	>

										<br>
										<div id= "status" style="display:none">
											<input type="radio" name="stat" class="radioChange" value="Paid">Paid
											<input type="radio" name="stat" class="radioChange" value="Due"> Due
										</div>
										 </small>
									</td>
									<td style= "border-bottom:1px solid black; ">
									</td>
								</tr>
								<tr>
									<td style= "border-bottom:1px solid black; ">
										 <small>Include IP Address: <input type=checkbox id="useIPAddress" style="float:right;" /></small>
									</td>
									<td style= "border-bottom:1px solid black; ">
									</td>
									<td style= "border-bottom:1px solid black; ">
									</td>
								</tr>
							</table>

						</td>
					</tr>
				</table>
				<input type='hidden' id='dataname' value=''>
				<div class="floatingTempResult"></div>
				<div id= 'results'>
					<table id='numResultsTable'style='display:none'>
					<tr>
						<td id='numResults' ></td>
					</tr>
					</table>
					<div id='resultHolder' style=' overflow: auto;'>
						<table class="resultTable" >
						<thead>
							<tr id="tableHeader">
								<th class='searchCols' id='company_name'>Company Name </th>
								<th class='searchCols' id='company_code'>Company Code </th>
								<th class='searchCols' id='disabled'>Disabled</th>
								<th class='searchCols' id='last_activity'>Last Activity</th>
								<th class='searchCols' id='created'>Created</th>
								<th class='searchCols' id='license_status'>Status</th>
								<th	class='searchCols' id='package_status'>Level</th>
								<th class='searchCols' id='email'>Email</th>
								<th class='searchCols' id='phone'>Phone</th>
								<th	class='searchCols' id='firstname'>First Name</th>
								<th	class='searchCols' id='lastname'>Last Name</th>
								<th	class='searchCols' id='city'>City</th>
								<th	class='searchCols' id='state'>State</th>
								<th class='searchCols' id='chain'>Chain</th>
								<!--<th	class='searchCols' id='zip'>Zip</td>
								<th width="4%"  class='searchCols' id='chain_name'>Chain</td>-->
								<th	style='text-align:center;background-color:#aecd47;color:gray;height:30px;'>CP</th>
							</tr>
						</thead>
						<tbody>
						</tbody>
						 <tfoot>
							<tr>
							  <td></td>
							</tr>
						  </tfoot>
						</table>
					</div>
				</div>
			<div id='tabs'>
				<ul <?php if (can_access("manage_dev_area")) { echo 'oncontextmenu="return switchManageMainTabsV2CP(event);"'; } ?>>
					<li><a href='#Main' id='mainTab' onclick='loadMain($("#dataname").val())'>Main</a></li>
					<li><a href='#LLS' id='LLSTab' onclick='loadLLS($("#dataname").val())'>LLS</a></li>
					<li><a href='#Billing' id= 'billingTab' onclick='loadBilling($("#dataname").val()); resize();'>Billing</a></li>
					<li><a href='#Logs' id= 'logsTab' onclick='loadLogs($("#dataname").val())'>Logs</a></li>
					<li><a href='#Graphs' id='graphsTab' onclick='loadGraphs($("#dataname").val())'>Graphs</a></li>



				</ul>
			<?php
			//<!--<li><a href='#Passbook' id='passbookTab' onclick='loadPassbook($("#dataname").val())'>Passbook</a></li> ->
			$this->makeMain();
			$this->makeBilling();
			$this->makeLLS();
			$this->makeLogs();
			$this->makeGraphs();
			//$this->makePassbook();
			$this->createLargeCompanyDisplay();
			?>
			</div>


			<?php
			$this -> makeResultSection();

		}

		private function makeMain(){
			echo "<div id ='Main' class='company_search_tab'>";
			echo "</div>";
		}
		private function makeBilling(){
			echo "<div id ='Billing' class='company_search_tab'>";
			echo "<iframe style= 'border-width:0px;' id='billingContent' src=''></iframe>";
			echo "</div>";
		}
		private function makeLLS(){
			echo "<div id ='LLS' class='company_search_tab'>";
			echo "</div>";
		}
		private function makeLogs(){
			echo "<div id ='Logs' class='company_search_tab'>";
			echo "</div>";
		}
		private function makeGraphs(){
			echo "<div id ='Graphs' class='company_search_tab'>";
			echo "</div>";
		}
		private function makePassbook(){
    		echo "<div id ='Passbook' class='company_search_tab'>";
			echo "</div>";
		}
		private function makeResultSection(){
			echo "
			<script type= text/javascript>
				resultSection();
			</script>";
		}
		private function createChat(){

			?>
			<div class='lavuChat' >

					<table width='100%' >
						<tr><th id ='chatHeader' colspan=2 onclick='
										if( $("#chatID").css("display")=="none"){
											$(".center").css("left","19%");
											$(".center").css("width","80%");
											$("#searchTable").css("left","19%");
											$("#chatArea").css("display", "");
											$(".chatBody").css("display", "");
											$("#chatID").css("display", "");
											$(".audience").css("display", "");
											$(".priority").css("display", "");
											$(".sendPost").css("display", "");
											$(".markViewed").css("display", "");
											$("#chatBody").css("display", "");
											$(".currentUsers").css("display", "");
											$(".lavuChat").css("height", "95%");
											$(".lavuChat").css("position","fixed");
											$("#advancedToolsSection").css("left","19%");
											$("#advancedToolsSection").css("width","900px");

										}else{
											$(".center").css("left","0");
											$(".center").css("width","100%");
											$("#searchTable").css("left","0");
											$("#chatID").css("display", "none");
											$(".chatBody").css("display", "none");
											$("#chatArea").css("display", "none");
											$(".audience").css("display", "none");
											$(".priority").css("display", "none");
											$(".sendPost").css("display", "none");
											$(".markViewed").css("display", "none");
											$(".currentUsers").css("display", "none");
											$(".lavuChat").css("height","30px");
											$(".lavuChat").css("position","absolute");
											$("#advancedToolsSection").css("left","5%");
											$("#advancedToolsSection").css("width","1100px");
										}
									'>
										Lavu Chat
									</th>
								</tr>
					</table>
					<table class='chatBody' style='width:100%; overflow-x:auto;display:none'>
						<tr>
							<td>

								<tr class='audience'>
									<td colspan=2>
										<input type='radio' name='group3' value='Development' title='Development Monkeys' > <small>Monkeys</small>
										<input type='radio' name='group3' value='Support' title='Support Ninjas'><small>Ninjas</small>
										<input type='radio' name='group3' value='Lavu' checked title='Lavu INC'><small>Lavu</small><br>
									</td>
								</tr>
								<tr class='priority'>
									<td>
										<small>Priority:</small>
										<input type='checkbox' id= 'usePriority'
											onchange= '
												if( $("#priority").css("display")=="none")
													$("#priority").css("display", "");

												else{
													$("#priority").css("display", "none");
													$("#normalPriority").prop("checked",true);
												}
										'>
										<div id='priority'  style= 'display:none'>
											<input type='radio' name='group1' id='normalPriority' value='normal' checked> <small>normal</small>
											<input type='radio' name='group1' id='urgentPriority' value='urgent'><small> urgent</small>
										</div>
									</td>
								</tr>
								<tr class='sendPost'>
									<td>
										<input type ='text' autocomplete='off' id='newPost' placeholder='New Message' onkeydown='if (event.keyCode == 13) $(\"#chatSubmitter\").click(); ' >
									</td>

									<td style='position:inherit;padding-bottom:9px' colspan=2>
										<input type='hidden' id='username' value="<?php echo $this->name; ?>">
										<input type='submit' id='chatSubmitter' value='send'
											onclick='savePost($("#newPost").val(),$("#username").val(), $("[name=group1]:checked").val(), $("[name=group3]:checked").val());' >

									</td>
								</tr>
							</td>
						</tr>
						<tr class='chatArea'>
							<td colspan='2'>
								<div style="height: 400px; overflow-y:auto">
									<table id='chatID' style='width:100%; display:none'>
										<?php echo $this->getChats(); ?>
									</table>
									<div id='newMessagesExist' style='display:none'> </div>
								</div>
							</td>
						</tr>
						<tr class='markViewed'>
							<td colspan='2' style='text-align:center'>
								<!-- <input type='button' value='mark all as viewed' onclick= "">-->
								<input id='deleteButton' type='button' value='Delete Messages' onclick="
									if($('.deleteCheckBox').css('display')=='block'){

										var allVals = [];
										 $('.deleteCheckBox :checked').each(function() {
										 	allVals.push($(this).val());
										 });
										 if(allVals.length > 0){
										 	performDeletion(allVals, $('#username').val(), $('[name=group3]:checked').val());
										 }
										$('.deleteCheckBox').css('display','none');
										$('#deleteButton').val('Delete Messages');

									}
									else{
										$('.deleteCheckBox').css('display','block');
										$('#deleteButton').val('Perform Deletion');
									}
									">
							</td>
						</tr>
						<tr class='currentUsers'>
							<td>
								<table id='usersLoggedIn'>
									<tr>
										<td>
											<b><small>Users Logged In</b></small>
										</td>
									</tr>
										<?php echo $this->getLoggedInUsers();?>
								</table>
							</td>
						</tr>
					</table>

			</div>


			</div>
		<?php
		}
		private function getChats(){
			echo "<script type=text/javascript>
				getChatsFromDB($('input[name=group3]:checked').val(), '".$this->name."');
			</script>";
		}
		private function getLoggedInUsers(){
		    echo "<script type=text/javascript>
				getCurrentlyLoggedInUsers();
			</script>";
		}
		private function makeExpiring(){
			?>
			<div id="expiring">
				<div>
					<table>
						<tr>
							<td id="expiringText" colspan="2">
								<span id="expiringTime"></span> <br/> <br/>
								Your session is about to expire in <span id="minutesLeft"></span>. <br/><br/>
								Press OK to stay logged in, otherwise press Cancel.
								<br/>
								<br/>
							</td>
						</tr>
						<tr>
							<td id="firstExpiringButton" class="expiringButton">
								<button type="button" onclick="modalWindowButton('OK')"> OK </button>
							</td>
							<td id="secondExpiringButton" class="expiringButton">
								<button type="button" onclick="modalWindowButton('Cancel')"> Cancel </button>
							<td>
						</tr>
					</table>
				</div>
			</div>

		<?php
		}

	}
?>
