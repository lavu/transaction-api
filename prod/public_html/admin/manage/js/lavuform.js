// for creating generic forms to be updated and saved via ajax
// the form should contain all inputs and their types, values, and attributes
// every input MUST have an index
// the form should contain the url to post to and the name of the form
// here's an example form:
/*
*	<div class="edit_account_info registered_devices" style="padding:19px;">
*		<legend>Registered Devices
*			<span class="edit_icon">'.$edit_icon.'</span>
*			<span class="done_icon">'.$done_icon.'</span>
*		</legend>
*		<input type="hidden" class="draw_info_form_submit_url" name="" value="/distro/beta/exec/distro_devices.php?action=register_devices" />
*		<input type="hidden" class="draw_info_form_name" name="" value="registered_devices" />
*		<fieldset class="my_info_fieldset draw_info_form_noedit" style="margin:5px 0 0 0; padding:0;">
*			<table>
*				<tr>
*					<td class="edit_name">Device 1: </td>
*					<td>
*						<input type="hidden" name="type" value="textbox" />
*						<input type="hidden" name="index" value="1" />
*						<input type="hidden" name="name" value="1" />
*						<font class="value">00000000-0000</font>
*					</td>
*				</tr>
*			</table>
*			<font>
*				<input type="hidden" name="attributes" value="{--class--:--submit--}" />
*				<input type="hidden" name="type" value="button" />
*				<input type="hidden" name="index" value="0" />
*				<font style="display:none;" class="value">Submit</font>
*			</font>
*		</fieldset>
*		<fieldset class="my_info_fieldset draw_info_form_edit" style="margin:5px 0 0 0; padding:0; display:none;">
*		</fieldset>
*		<script type="text/javascript">
*			$(document).ready(function() { setTimeout(function() {
*				Lavu.Form.draw_info_form($(".edit_account_info.registered_devices"));
*			}, 300); });
*		</script>
*	</div>
*/

if (typeof(Lavu) == "undefined")
	Lavu = {};
if (typeof(Lavu.Form) == "undefined")
	Lavu.Form = {};

// draws the edit area for an info area
Lavu.Form.draw_info_form = function(jelement) {

	// set up the edit area
	// copy the noedit area into the edit area
	var jnoedit = jelement.find(".draw_info_form_noedit");
	var jedit = jelement.find(".draw_info_form_edit");
	var jsubmiturl = jelement.find(".draw_info_form_submit_url");
	var formname = jelement.find(".draw_info_form_name").val();
	jedit.html("");
	jedit.append(jnoedit.html());
	var values = jedit.find(".value");

	// transform each edit value into an edit field
	$.each(values, function(k,v) {

		// collect data on the edit field
		var jvalue = $(v);
		var attributes = { type:'textarea', draw:true, disabled:false, possible_values:[], identifier_class:'', name:'', style:'', attributes:'' };
		attributes.value = jvalue.text();
		$.each(jvalue.siblings("input"), function(i,input){
			if (input.name == "attributes") {
				input.value = input.value.replace(/--/g, '"');
				attributes[input.name] = JSON.parse(input.value);
			} else {
				attributes[input.name] = input.value;
			}
		});

		// determine the type
		if (typeof(attributes.type) == 'undefined')
			attributes.type = 'textarea';

		// draw the input
		jvalue.hide();
		attributes.identifier_class = 'lavu_form_edit_element_'+formname+"_"+attributes.index;
		jvalue.parent().append(Lavu.Gui.toString(attributes));
	});

	// bind the edit/done buttons
	jelement.find('.edit_icon').click(function(){
		jedit.show();
		jnoedit.hide();
		jelement.find('.edit_icon').hide();
		jelement.find('.done_icon').show();
	});
	jelement.find('.done_icon').click(function(){
		jedit.hide();
		jnoedit.show();
		jelement.find('.edit_icon').show();
		jelement.find('.done_icon').hide();
	});

	// bind the submit button
	jelement.find(".submit").click(function(){
		Lavu.Form.submit_info_form(jelement, jsubmiturl.val());
	});
};

// helper function to draw_info_form
// submits the results and populates them back into the noedit area
Lavu.Form.submit_info_form = function(jelement, url) {

	// get some values
	var jnoedit = jelement.find(".draw_info_form_noedit");
	var jedit = jelement.find(".draw_info_form_edit");
	var formname = jelement.find(".draw_info_form_name").val();

	// compile the data
	var data = {};
	var copyinfo = [];
	var indexes = jedit.find("input[name=index]");
	$.each(indexes, function(k,v) {
		var index = v.value;
		var name = Lavu.Gui.getName("lavu_form_edit_element_"+formname+"_"+index);
		var value = Lavu.Gui.getValue("lavu_form_edit_element_"+formname+"_"+index);
		data[name] = value;
		copyinfo.push({ index:index, value:value });
	});

	// try to submit the data
	$.ajax({
		url: url,
		cache: false,
		async: false,
		type: 'POST',
		timeout: 5000,
		data: data,
		success: function(m) {
			var json = JSON.parse(m);
			if (json.success) {

				// the data was communicated successfully
				// push the data back into the noedit area
				$.each(copyinfo, function(k,v) {
					var jindex = jnoedit.find("input[name=index][value="+v.index+"]");
					var jvalue = jindex.siblings(".value");
					jvalue.text(v.value);
				});

				// close the edit form
				jedit.hide();
				jnoedit.show();
				jelement.find('.edit_icon').show();
				jelement.find('.done_icon').hide();
			} else {

				// tell the user they failed
				alert(json.error_msg);
			}
		},
		error: function(a,b,c) {
			alert("There was an error communicating with the server. Please try again.");
		}
	});
};