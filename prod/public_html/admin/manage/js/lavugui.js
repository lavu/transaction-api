if (typeof(Lavu) == "undefined")
	Lavu = {};
if (typeof(Lavu.Gui) == "undefined")
	Lavu.Gui = {};

Lavu.Gui.getElement = function(identifier_class) {
	return $(".lavu_gui."+identifier_class)[0];
};
Lavu.Gui.getInput = function(jelement) {
	var input = jelement.find("input");
	if (input.length > 0)
		return input;
	input = jelement.find("select");
	if (input.length > 0)
		return input;
	input = jelement.find("textbox");
	return input;
};
Lavu.Gui.getValue = function(identifier_class) {
	var element = this.getElement(identifier_class);
	var jelement = $(element);
	var jinput = this.getInput(jelement);
	var value = "";
	if (jelement.hasClass('type_checkbox')) {
		value = jinput[0].checked ? 1 : 0;
	} else {
		value = jinput.val();
	}
	return value;
};
Lavu.Gui.getName = function(identifier_class) {
	var element = this.getElement(identifier_class);
	var jelement = $(element);
	var jinput = this.getInput(jelement);
	return jinput.attr("name");
};
Lavu.Gui.parseBool = function(value) {
	return (typeof(value) == 'boolean' ? value : (typeof(value) == 'number' ? (value !== 0) : (value != '0')));
};
Lavu.Gui.setValue = function(element, value) {
	var jelement = $(element);
	var jinput = this.getInput(jelement);
	if (jelement.hasClass('type_checkbox')) {
		value = this.parseBool(value);
		jinput[0].checked = value;
		return;
	}
	jinput.val(value);
};

// turns an input object into an input element
// @a_draw_parts: should contain the following fields:
//		(wt)value, (string)type, (bool)draw, (bool)disabled, (array)possible_values, (string)identifier_class, (string)name
//	can contain the following fields:
//		(string)style, (string)attributes
// @return: the html of the input element
Lavu.Gui.toString = function(a_draw_parts) {
	var retval = "";
	var p = a_draw_parts;
	var properties = "";
	var domtype = "";
	var extra_style = "";
	var contents = "";
	var attributes = "";

	// check that there is a value
	if (typeof(p.value) == "undefined") {
		if (p.type == "textbox" || p.type == "button" || p.type == "text" || p.type == "select" || p.type == "history")
			p.value = "";
		else if (p.type == "number")
			p.value = 0;
		else if (p.type == "checkbox")
			p.value = "0";
	}

	// check whether to draw this and get other values
	if (!p.draw)
		return "";
	if (p.disabled)
		properties += " DISABLED";
	if (p.style)
		extra_style += p.style;

	// get the dom type
	if (p.type == "textbox" || p.type == "number" || p.type == "checkbox" || p.type == "button")
		domtype = "input";
	else if (p.type == "text")
		domtype = "font";
	else if (p.type == "select")
		domtype = "select";
	else if (p.type == "history")
		domtype = "a";

	// get extra attributes
	if (typeof(p.attributes) != "undefined") {
		$.each(p.attributes, function(k, v) {
			if (attributes.length > 0)
				attributes += " ";
			attributes += k+"='"+v+"'";
		});
		attributes = " "+attributes+" ";
	}

	// get the contents
	if (p.type == "textbox" || p.type == "number" || p.type == "button") {
		properties += " value='"+p.value+"' type='"+p.type+"'";
	} else if (p.type == "text") {
		contents += p.value+"<input type='hidden' value='"+p.value+"' />";
		attributes = "";
	} else if (p.type == "checkbox") {
		properties += " value='"+p.value+"' type='"+p.type+"'"+(this.parseBool(p.value) ? " CHECKED" : "");
	} else if (p.type == "select") {
		$.each(p.possible_values, function(k,v) {
			var selected = (v.value == p.value) ? " SELECTED" : "";
			contents += "<option value='"+v.value+"'"+selected+">"+v.printed+"</option>";
		});
	} else if (p.type == "history") {
		properties += " onclick='Lavu.Gui.drawHistory($(this).parent()[0], "+p.value+");'";
		extra_style += "color:blue; text-decoration:underline; cursor:default;";
		contents += "show";
	}

	// create the element
	retval += "<div style='display:inline-table;' class='lavu_gui "+p.identifier_class+" type_"+p.type+"'><"+domtype+" name='"+p.name+"' style='"+extra_style+"'"+properties+attributes+">"+contents+"</"+domtype+"></div>";

	return retval;
};

// draws the history in the json_string
// draws it to the screen
Lavu.Gui.drawHistory = function(append_element, json_string) {
	var jelement = $(append_element);
	var w = 400;
	var win = { window: window, width: $(window).width(), height: $(window).height(), scroll: $(window).scrollTop() };
	var left = win.width/2-w/2;
	var top = 30;
	var h = 500;

	var json = JSON.parse(json_string);
	var contents = Lavu.Gui.printObject(json, "\t");

	while ($("#lavu_gui_history_box").length > 0)
		$("#lavu_gui_history_box").remove();
	jelement.append("<div id='lavu_gui_history_box' style='font-family:monospace; position:fixed; width:"+w+"px; height:"+h+"px; top:"+top+"px; left:"+left+"px; background-color:white; border:1px solid black; border-radius:0; padding:10px; overflow:auto; color:#444; display:none; cursor:pointer;' onclick='$(this).remove();'><pre>"+contents+"</pre></div>");
	$("#lavu_gui_history_box").show(300);

	// scroll inside of manage
	var mewindow = window;
	var parentWindow = window.parent.window;
	if (parentWindow !== mewindow) {
		var scrollfunc = function() {
			var jHistoryBox = mewindow.$("#lavu_gui_history_box");
			if (jHistoryBox.length <= 0)
				return;
			var parentScroll = Math.max(0, parseInt(parentWindow.$(parentWindow).scrollTop(),10)-158);
			jHistoryBox.css("top", ((top+mewindow.$(mewindow).scrollTop()+parentScroll)+"px"));
		};
		setTimeout(scrollfunc, 400);
		scrollfunc();
	}
};

// translate a javascript object as a jquery object
// return the jquery object as a string
Lavu.Gui.printObject = function(object, line_prefix) {

	var open_brace = "{";
	var close_brace = "}";
	if (typeof(object) == "array") {
		open_brace = "[";
		close_brace = "]";
	}

	var retval = open_brace;
	var comma = "";
	$.each(object, function(k,v) {
		var type = typeof(v);
		var key = k;
		if (typeof(k) == "string")
			key = "\""+k+"\"";
		retval += comma+"\n"+line_prefix+key+": ";
		if (type == "undefined" || type == "null")
			retval += type;
		else if (type == "number")
			retval += v;
		else if (type == "boolean")
			retval += (v ? "true" : "false");
		else if (type == "array" || type == "object")
			retval += Lavu.Gui.printObject(v, line_prefix+"\t");
		else if (type == "string")
			retval += "\""+v+"\"";
		comma = ",";
	});
	retval += "\n"+line_prefix.replace("\t","")+close_brace;
	return retval;
};

// find the parent dom object of the given element
// @jelement: the jquery element to start at
// @dom_type: the dom type to search for (eg tr)
// @return: the jquery dom object being searched for, or null if the searched for dom_type wasn't found
Lavu.Gui.getParentDOM = function(jelement, dom_type) {
	if (jelement.is(dom_type))
		return jelement;
	if (jelement[0] == window)
		return null;
	return this.getParentDOM(jelement.parent(), dom_type);
};

// finds all inputs nested in the given element and returns their name/value pairs
// @jelement: the element to search in
// @return: an array[{name:string,value:string},...]
Lavu.Gui.getInputsNamesValues = function(jelement) {
	var inputs = jelement.find("input");
	$.merge(inputs, jelement.find("select"));
	$.merge(inputs, jelement.find("textarea"));

	var retval = [];
	$.each(inputs, function(k, input) {
		var jinput = $(input);
		var name = jinput.attr('name');
		var value = jinput.val();
		if (jinput.attr('type') == 'checkbox')
			value = jinput[0].checked ? '1' : '0';
		if (typeof(name) !== 'undefined' && typeof(value) !== 'undefined') {
			retval.push({name:name, value:value});
		}
	});

	return retval;
};