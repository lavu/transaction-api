//Base Lavu
(function(){
	if( ! window.Lavu ) {
		window.Lavu = {};
	}

	/**
	 * Returns the Appropriate AJAX Obect
	 *
	 * @returns an XMLHttpRequest object, if it can, or an ActiveXObject equivelent.
	 **/
	function getAjaxObject() {
		if( window.XMLHttpRequest) {
			return new XMLHttpRequest();
		} else {
			return new ActiveXObject("Microsoft.XMLHTTP");
		}
	}

	/**
	 * A Conviencience function that allows for the quick sending/submitting of POST
	 * requests to the url specified.
	 *
	 * @param url: the url to connect to.  It should be relative to the current site,
	 *			since all others would result in an error otherwise.  You don't have
	 *			to submit the full url path.
	 *
	 * @param variables: The variables you'd like to submit to the POST request,
	 *			recommended to be set as an object with fields mapping to values.
	 *			Will also accept a pre-processed URI encoded string, or an array.
	 *
	 * @param callback: The function/object that the request should return the result to.
	 *			Since the AJAX request is asyncronus, this is neccessary.  Please note that
	 *			the function passed will be stored within the AJAX request object, so you
	 *			can get to it by calling this, unless it is an object.
	 *				Ex: this.status, this.responseText.
	 *				Ex: event.target.status, event.target.responseText
	 *
	 *	@returns the AJAXRequest Object that was created in the process.
	 *
	 **/
	function sendPOST( url, variables, callback ) {
		var ajaxRequest = Lavu.getAjaxObject();

		for( var field in ajaxRequest ){
			if(field.indexOf('on') === 0 ){
				var eventtype = field.substr(2);
				ajaxRequest.addEventListener( eventtype, callback );
			}
		}
		ajaxRequest.open("POST",url,true);

		var vars = "";
		switch( typeof variables ) {
			case "string" :
				vars = variables;
				break;
			case "object" :
				for( var key in variables ) {
					if(vars) {
						vars += "&";
					}
					vars += encodeURIComponent(key) + "=" + encodeURIComponent(variables[key]);
				}
				break;
			case "array" :
				for( var i = 0; i < variables.length; i++ ){
					if(vars) {
						vars += "&";
					}

					vars += i + "=" + encodeURIComponent( variables[i] );
				}
				break;
			default : break;
		}
		ajaxRequest.setRequestHeader("Content-type","application/x-www-form-urlencoded");
		ajaxRequest.send( vars );
		return ajaxRequest;
	}
	Lavu.getAjaxObject = getAjaxObject;
	Lavu.sendPOST = sendPOST;

	/**
	 *
	 **/
	function decimalToHex( num ) {
		var reference = [ '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f' ];

		var result = '';
		while( num > 0 ){
			var index = num % 16;
			result += reference[index];
			num = num - index;
			num = num / 16;
		}

		return result;
	}

	function hexToDecimal( hex ) {
		var reference = [ '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f' ];
		var result = 0;

		while(hex.length){
			var hex_dec = hex.substr(0, 1);
			hex = hex.substr(1);

			var num = reference.indexOf( hex_dec );
			result = result * 16;
			result = result + num;
		}

		return result;
	}

	Lavu.decimalToHex = decimalToHex;
	Lavu.hexToDecimal = hexToDecimal;

	function affineTransform( i, x, I, o, O ){
		return (((x - i)/(I-i))*(O-o))+o;
	}
	Lavu.affineTransform = affineTransform;

	(function(){
		function RunLoop( delay ){
			this._updatables = new Lavu.util.Set();
			this._loopHandle = null;
			this._delay = 300;
			if( delay ){
				this._delay = delay;
			}
		}

		function reset(){
			this._updatables = new Lavu.util.Set();
		}

		function add( updatable ){
			if( !updatable ){
				return false;
			}

			if(!(updatable instanceof Lavu.util.IUpdatable) ){
				return false;
			}

			return this._updatables.add( updatable );
		}

		function remove( updatable ){
			if( !updatable ){
				return false;
			}

			if(!(updatable instanceof Lavu.util.IUpdatable) ){
				return false;
			}

			return this._updatables.remove( updatable );
		}

		function startLoop(){
			if(!this._loopHandle){
				this._loopHandle = window.setInterval( this.execute.bind( this ), this._delay );
			}
		}

		function stopLoop(){
			if(this._loopHandle){
				clearInterval(this._loopHandle);
				this._loopHandle = null;
			}
		}

		function execute(){
			for( var i = 0; i < this._updatables.length; i++ ){
				this._updatables[i].update();
			}
		}

		var _drawLoop = null;
		function drawLoop(){
			if( !_drawLoop ){
				_drawLoop = new RunLoop( 1000/30 );
			}
			return _drawLoop;
		}

		var _updateLoop = null;
		function updateLoop(){
			if( !_updateLoop ){
				_updateLoop = new RunLoop( 1000/60 );
			}
			return _updateLoop;
		}
		var _mainLoop = null;
		function mainLoop(){
			if( !_mainLoop ){
				_mainLoop = new RunLoop( 1000/60 );
			}
			return _mainLoop;
		}

		Lavu.RunLoop = RunLoop;
		RunLoop.prototype = {};
		RunLoop.prototype.constructor = RunLoop;
		RunLoop.prototype.reset = reset;
		RunLoop.prototype.add = add;
		RunLoop.prototype.remove = remove;
		RunLoop.prototype.startLoop = startLoop;
		RunLoop.prototype.stopLoop = stopLoop;
		RunLoop.prototype.execute = execute;
		RunLoop.drawLoop = drawLoop;
		RunLoop.updateLoop = updateLoop;
		RunLoop.mainLoop = mainLoop;
	})();

	(function(){
		function ClassList( element ) {
			if(! element ){
				return null;
			}

			if(! (element instanceof HTMLElement )){
				return null;
			}

			if( element.classList ){
				return null;
			}
			Array.call( this );
			element.classList = this;
			this._element = element;
		}

		function add( classStr ) {
			if(! classStr ){
				return false;
			}

			if( ((typeof classStr) != "string" ) ){
				return false;
			}
			this._rebuildClassName();

			if( this.indexOf( classStr ) >= 0 ){
				return false;
			}

			this.push( classStr );

			this._updateClassName();
			return true;
		}

		function remove( classStr ) {
			if(! classStr ){
				return false;
			}

			if( ((typeof classStr) != "string" ) ){
				return false;
			}
			this._rebuildClassName();

			if( this.indexOf( classStr ) < 0 ){
				return false;
			}

			this.push( classStr );
			this._updateClassName();
			return true;
		}

		function toggle( classStr ){
			if( !this.add( classStr ) ){
				this.remove( classStr );
			}
		}

		function toClassName() {
			var result = "";
			for(var i = 0; i < this.length; i++ ){
				if( result ){
					result += " ";
				}

				result += this[i];
			}
			return result;
		}

		function _rebuildClassName() {
			if( this._element.className == this.toClassName() ){
				return;
			}

			while( this.length ){
				this.pop();
			}

			var classes = this._element.className.split(" ");
			for( var i = 0; i < classes.length; i++ ){
				this.push( classes[i] );
			}
		}

		function _updateClassName() {
			this._element.className = this.toClassName();
		}

		Lavu.ClassList = ClassList;
		ClassList.prototype = [];
		ClassList.prototype.constructor = ClassList;
		ClassList.prototype.add = add;
		ClassList.prototype.remove = remove;
		ClassList.prototype.toggle = toggle;
		ClassList.prototype.toClassName = toClassName;
		ClassList.prototype._rebuildClassName = _rebuildClassName;
		ClassList.prototype._rebuildClassName = _rebuildClassName;
		ClassList.prototype._updateClassName = _updateClassName;

	})();

	function getWithLeadingZeros( str, n ){
		var result = str + '';
		while(result.length < n ){
			result = '0' + result;
		}

		return result;
	}
	Lavu.getWithLeadingZeros = getWithLeadingZeros;

	function getHumanReadableDateSinceNow( date ){
		var now = new Date();
		var date_diff = now.valueOf() - date.valueOf();
		var date_display_str = '';

		var milliseconds = date_diff % 1000;
		date_diff -= milliseconds;
		date_diff /= 1000;
		var seconds = (date_diff < 0) ? 0 : date_diff % 60;
		date_diff -= seconds;
		date_diff /= 60;
		var minutes = (date_diff < 0) ? 0 : date_diff % 60;
		date_diff -= minutes;
		date_diff /= 60;
		var hours = (date_diff < 0) ? 0 : date_diff % 24;
		date_diff -= hours;
		date_diff /= 24;
		var days = (date_diff < 0) ? 0 : date_diff % 30;
		date_diff -= days;
		date_diff /= 30;
		var months = (date_diff < 0) ? 0 : date_diff % 12;
		date_diff -= months;
		date_diff /= 12;
		var years = date_diff;


		if( years > 0 ){
			date_display_str = years + (( years == 1 ) ? ' Year Ago' : ' Years Ago');
		} else if ( months > 0 ) {
			date_display_str = months + (( months == 1 ) ? ' Month Ago' : ' Months Ago');
		} else if ( days > 0 ) {
			date_display_str = days + (( days == 1) ? ' Day Ago' : ' Days Ago');
		} else if ( hours > 0  ) {
			date_display_str = hours + (( hours == 1 ) ? ' Hour Ago' : ' Hours Ago');
		} else if ( minutes > 0  ) {
			date_display_str = minutes + (( minutes == 1 ) ? ' Minute Ago' : ' Minutes Ago');
		} else if ( seconds >= 0  ) {
			date_display_str = ' Just Now';
		}

		return date_display_str;
	}
	Lavu.getHumanReadableDateSinceNow = getHumanReadableDateSinceNow;

	function getFontColorForBGColor( bgColor ){
		var r = bgColor.substring(1, 3);
		var g = bgColor.substring(3, 5);
		var b = bgColor.substring(5, 7);

		r = Lavu.hexToDecimal( r );
		g = Lavu.hexToDecimal( g );
		b = Lavu.hexToDecimal( b );

		var value = (r + g + b) / 3;

		if( value < 150 ){
			return '#ffffff';
		}
		return null;
	}

	Lavu.getFontColorForBGColor = getFontColorForBGColor;


	(function(){
		function ManagedItem(){
			Lavu.mvc.Model.call( this );
		}

		function updateItemWithItem( item ){
			//Please Implement This
			return false;
		}

		function equals( item ){
			//Override this with custom equality function
			return item == this;
		}

		function getDisplayValue(){
			return 'ManagedItem';
		}

		function getValue(){
			return this;
		}

		function compare( item1, item2 ){
			if(item1.getDisplayValue() < item2.getDisplayValue()){
				return -1;
			}

			if(item1.getDisplayValue() > item2.getDisplayValue()){
				return 1;
			}

			return 0;
		}

		function shouldFilter(){
			return false;
		}

		Lavu.ManagedItem = ManagedItem;
		ManagedItem.prototype = Object.create( Lavu.mvc.Model.prototype );
		ManagedItem.prototype.constructor = ManagedItem;
		ManagedItem.prototype.updateItemWithItem = updateItemWithItem;
		ManagedItem.prototype.equals = equals;
		ManagedItem.prototype.getDisplayValue = getDisplayValue;
		ManagedItem.prototype.getValue = getValue;
		ManagedItem.prototype.compare = compare;
		ManagedItem.prototype.shouldFilter = shouldFilter;
	})();

	(function(){
		function Manager(){
			Lavu.mvc.Model.call( this );
			this._items = [];

			this._observable = new Lavu.util.IObservable();
		}

		function addItem( item ) {
			if(! item ){
				return false;
			}

			if(! (item instanceof Lavu.mvc.Model)){
				return false;
			}

			if( ! (item instanceof Lavu.ManagedItem )){
				return false;
			}

			for(var i = 0; i < this._items.length; i++ ){
				if( this._items[i].equals( item ) ){
					this._items[i].updateItemWithItem( item );
					this.update();
					return true;
				}
			}

			this._items.push( item );

			this._items.sort( item.compare );
			this.update();
			return true;
		}

		function getItemForItem( item ){
			if(! item ){
				return null;
			}

			if(! (item instanceof Lavu.mvc.Model)){
				return null;
			}

			if(! (item instanceof Lavu.ManagedItem)){
				return null;
			}

			for( var i = 0; i < this._items.length; i++ ){
				if( this._items[i].equals( item ) ){
					return this._items[i];
				}
			}

			return null;
		}

		function removeItem( item ){
			if(! item ){
				return false;
			}

			if(! (item instanceof Lavu.mvc.Model)){
				return false;
			}

			if(! (item instanceof Lavu.ManagedItem)){
				return false;
			}

			for( var i = 0; i < this._items.length; i++ ){
				if( this._items[i].equals( item ) ){
					this._items.splice(i, 1 );
					this.update();
					return true;
				}
			}

			return false;
		}

		function getItems(){
			var result = [];
			for( var i = 0; i < this._items.length; i++ ){
				if(! this._items[i].shouldFilter() ){
					result.push( this._items[i] );
				}
			}

			return result;
		}

		function reset(){
			this._items = [];
			this._observable = new Lavu.util.IObservable();
		}

		function update(){
			this._observable.update( this );
		}

		function addObserver( observer ){
			observer.notify( this );
			this._observable.addObserver( observer );
		}

		function removeObserver( observer ){
			this._observable.removeObserver( observer );
		}

		Lavu.Manager = Manager;
		Manager.prototype = Object.create( Lavu.mvc.Model.prototype );
		Manager.prototype.constructor = Manager;
		Manager.prototype.addItem = addItem;
		Manager.prototype.getItemForItem = getItemForItem;
		Manager.prototype.removeItem = removeItem;
		Manager.prototype.getItems = getItems;
		Manager.prototype.reset = reset;
		Manager.prototype.update = update;
		Manager.prototype.addObserver = addObserver;
		Manager.prototype.removeObserver = removeObserver;
	})();

	(function(){
		if(! window.Lavu.User ){
			window.Lavu.User = {};
		}
		function LavuManageUsersModel() {
			Lavu.Manager.call( this );
		}

		function getUsers( ){
			return this.getItems();
		}

		function addUser( user ){
			if(! (user instanceof Lavu.User.UserAccount )) {
				return false;
			}

			return this.addItem( user );
		}

		function getUserForUser( user ){
			return this.getItemForItem( user );
		}

		function removeUser( user ){
			if(! (user instanceof Lavu.User.UserAccount )){
				return false;
			}

			return thie.removeItem( user );
		}

		function getUserByID( user_id ) {
			if(! user_id ){
				return null;
			}

			var user = Lavu.User.UserAccount.createUser( { 'id' : user_id } );
			return this.getUserForUser( user );
		}

		function getUserByUserName( username ) {
			if(! username ){
				return null;
			}

			var user = Lavu.User.UserAccount.createUser( { 'username' : username } );
			return this.getUserForUser( user );
		}

		var _userManager;
		function userManager(){
			if( !_userManager ){
				_userManager = new Lavu.User.LavuManageUsersModel();
			}
			return _userManager;
		}

		Lavu.User.LavuManageUsersModel = LavuManageUsersModel;
		LavuManageUsersModel.prototype = Object.create( Lavu.Manager.prototype );
		LavuManageUsersModel.prototype.constructor = LavuManageUsersModel;
		LavuManageUsersModel.prototype.getUsers = getUsers;
		LavuManageUsersModel.prototype.addUser = addUser;
		LavuManageUsersModel.prototype.getUserForUser = getUserForUser;
		LavuManageUsersModel.prototype.removeUser = removeUser;
		LavuManageUsersModel.prototype.getUserByID = getUserByID;
		LavuManageUsersModel.prototype.getUserByUserName = getUserByUserName;
		LavuManageUsersModel.userManager = userManager;

		Lavu.User.UserManager = Lavu.User.LavuManageUsersModel.userManager();
	})();

	(function(){
		if(! window.Lavu.User ) {
			window.Lavu.User = {};
		}
		function UserAccount( id, username, fullname, email, access, lavuAdmin, options, type, pipeline_id, stage_id ) {
			Lavu.ManagedItem.call( this );
			this._userID = new Lavu.util.ObservableField( this, 'id', id*1 );
			this._fullname = new Lavu.util.ObservableField( this, 'fullname', fullname );
			this._email = new Lavu.util.ObservableField( this, 'email', email );
			this._access = new Lavu.util.ObservableField( this, 'access', access );
			this._lavuAdmin = new Lavu.util.ObservableField( this, 'lavuAdmin', lavuAdmin*1 );
			this._username = new Lavu.util.ObservableField( this, 'username', (username?username.toLowerCase():'') );
			this._options = new Lavu.util.ObservableField( this, 'options', ((typeof options == 'string' && options !== '')?JSON.parse( options ):{}) );
			this._type = new Lavu.util.ObservableField( this, 'type', type );
			this._default_pipeline_id = new Lavu.util.ObservableField( this, 'default_pipeline_id', pipeline_id*1 );
			this._default_stage_id = new Lavu.util.ObservableField( this, 'default_stage_id', stage_id*1 );
			this._userObserver = new Lavu.util.IObservable();
			this._sorting_function = new Lavu.util.ObservableField( this, 'sorting_function', 1 );
		}

		function getFullName() {
			return this._fullname.fieldValue();
		}

		function addFullNameObserver( observer ){
			return this._fullname.addObserver( observer );
		}

		function removeFullNameObserver( observer ){
			return this._fullname.removeObserver( observer );
		}

		function getEmail() {
			return this._email.fieldValue();
		}

		function addEmailObserver( observer ){
			return this._email.addObserver( observer );
		}

		function removeEmailObserver( observer ){
			return this._email.removeObserver( observer );
		}

		function getAccess() {
			return this._access.fieldValue();
		}

		function addAccessObserver( observer ){
			return this._access.addObserver( observer );
		}

		function removeAccessObserver( observer ){
			return this._access.removeObserver( observer );
		}

		function getLavuAdmin() {
			return this._lavuAdmin.fieldValue();
		}

		function addLavuAdminObserver( observer ){
			return this._lavuAdmin.addObserver( observer );
		}

		function removeLavuAdminObserver( observer ){
			return this._lavuAdmin.removeObserver( observer );
		}

		function getUsername() {
			return this._username.fieldValue();
		}

		function addUserNameObserver( observer ){
			return this._username.addObserver( observer );
		}

		function removeUserNameObserver( observer ){
			return this._username.removeObserver( observer );
		}

		function getUserID() {
			return this._userID.fieldValue();
		}

		function addUserIDObserver( observer ){
			return this._userID.addObserver( observer );
		}

		function removeUserIDObserver( observer ){
			return this._userID.removeObserver( observer );
		}

		function getType(){
			return this._type.fieldValue();
		}

		function addTypeObserver( observer ){
			return this._type.addObserver( observer );
		}

		function removeTypeObserver( observer ){
			return this._type.removeObserver( observer );
		}

		function getDefaultPipelineID(){
			return this._default_pipeline_id.fieldValue();
		}

		function addDefaultPipelineIDObserver( observer ){
			return this._default_pipeline_id.addObserver( observer );
		}

		function removeDefaultPipelineIDObserver( observer ){
			return this._default_pipeline_id.removeObserver( observer );
		}

		function getDefaultStageID(){
			return this._default_stage_id.fieldValue();
		}

		function addDefaultStageIDObserver( observer ){
			return this._default_stage_id.addObserver( observer );
		}

		function removeDefaultStageIDObserver( observer ){
			return this._default_stage_id.removeObserver( observer );
		}

		function setSortingFunction( value ){
			this._sorting_function.setFieldValue( value );
		}

		function getSortingFunction(){
			return this._sorting_function.fieldValue();
		}

		function addSortingFunctionObserver( observer ){
			return this._sorting_function.addObserver( observer );
		}

		function removeSortingFunctionObserver( observer ){
			return this._sorting_function.removeObserver( observer );
		}

		function getOptions() {
			return this._options.fieldValue();
		}

		function addOptionsObserver( observer ){
			return this._options.addObserver( observer );
		}

		function removeOptionsObserver( observer ){
			return this._options.removeObserver( observer );
		}

		function canAccess( area ) {
			checkAccesses( area );

			if( this.getAccess() == "all" ) {
				return true;
			}

			if( this.getAccess().indexOf( area ) >= 0 ){
				return true;
			}
			return false;
		}

		function checkAccesses( area ) {
			if (!window.sentAjaxAccesses){
				window.sentAjaxAccesses = [];
			}
			if (window.sentAjaxAccesses[area] === true){
				return;
			}
			sendPOST( '/manage/misc/MainFunctions/saveCanAccess.php', { action: 'add_can_access', access: area }, function(){} );
			window.sentAjaxAccesses[area] = true;
		}

		function updateItemWithItem( item ){
			if(! item ){
				return false;
			}

			if(! (item instanceof Lavu.mvc.Model )){
				return false;
			}

			if(! (item instanceof Lavu.User.UserAccount )){
				return false;
			}

			this._userID.setFieldValue( item.getUserID() );
			this._fullname.setFieldValue( item.getFullName() );
			this._email.setFieldValue( item.getEmail() );
			this._access.setFieldValue( item.getAccess() );
			this._lavuAdmin.setFieldValue( item.getLavuAdmin() );
			this._username.setFieldValue( item.getUsername() );
			this._options.setFieldValue( item.getOptions() );
			this._userObserver.update( this );
			return true;
		}

		function equals( item ){
			if( !item ){
				return false;
			}

			if(! (item instanceof Lavu.User.UserAccount )){
				return false;
			}

			return item.getUserID() == this.getUserID() || item.getUsername() == this.getUsername();
		}

		function getDisplayValue(){
			return this.getFullName();
		}

		function getValue(){
			return this.getUserID();
		}

		function updateUserWithUser( user ){
			return this.updateItemWithItem( user );
		}

		function shouldFilter(){
			return this.getType() == 'terminated' || this.getLavuAdmin() === 0;
		}

		function isDeleted(){
			return false;
		}

		function createUser( user_obj ) {
			if(! user_obj ){
				return null;
			}

			if(! (user_obj instanceof Object )){
				return null;
			}

			var result = new Lavu.User.UserAccount(user_obj.id, user_obj.username, user_obj.firstname + " " + user_obj.lastname, user_obj.email, user_obj.access, user_obj.lavu_admin, user_obj.options, user_obj.type, user_obj.default_pipeline_id, user_obj.default_stage_id);
			// result._disabled = user_obj.disabled == '1';
			return result;
		}

		function addUserObserver( observer ){
			return this._userObserver.addObserver( observer );
		}

		function removeUserObserver( observer ){
			return this._userObserver.removeObserver( observer );
		}

		Lavu.User.UserAccount = UserAccount;
		UserAccount.prototype = Object.create( Lavu.ManagedItem.prototype );
		UserAccount.prototype.constructor = UserAccount;
		
		UserAccount.prototype.addAccessObserver = addAccessObserver;
		UserAccount.prototype.addDefaultPipelineIDObserver = addDefaultPipelineIDObserver;
		UserAccount.prototype.addDefaultStageIDObserver = addDefaultStageIDObserver;
		UserAccount.prototype.addEmailObserver = addEmailObserver;
		UserAccount.prototype.addFullNameObserver = addFullNameObserver;
		UserAccount.prototype.addLavuAdminObserver = addLavuAdminObserver;
		UserAccount.prototype.addOptionsObserver = addOptionsObserver;
		UserAccount.prototype.addSortingFunctionObserver = addSortingFunctionObserver;
		UserAccount.prototype.addTypeObserver = addTypeObserver;
		UserAccount.prototype.addUserIDObserver = addUserIDObserver;
		UserAccount.prototype.addUserNameObserver = addUserNameObserver;
		UserAccount.prototype.addUserObserver = addUserObserver;
		UserAccount.prototype.canAccess = canAccess;
		UserAccount.prototype.equals = equals;
		UserAccount.prototype.getAccess = getAccess;
		UserAccount.prototype.getDefaultPipelineID = getDefaultPipelineID;
		UserAccount.prototype.getDefaultStageID = getDefaultStageID;
		UserAccount.prototype.getDisplayValue = getDisplayValue;
		UserAccount.prototype.getEmail = getEmail;
		UserAccount.prototype.getFullName = getFullName;
		UserAccount.prototype.getLavuAdmin = getLavuAdmin;
		UserAccount.prototype.getOptions = getOptions;
		UserAccount.prototype.getSortingFunction = getSortingFunction;
		UserAccount.prototype.getType = getType;
		UserAccount.prototype.getUserID = getUserID;
		UserAccount.prototype.getUsername = getUsername;
		UserAccount.prototype.getValue = getValue;
		UserAccount.prototype.isDeleted = isDeleted;
		UserAccount.prototype.removeAccessObserver = removeAccessObserver;
		UserAccount.prototype.removeDefaultPipelineIDObserver = removeDefaultPipelineIDObserver;
		UserAccount.prototype.removeDefaultStageIDObserver = removeDefaultStageIDObserver;
		UserAccount.prototype.removeEmailObserver = removeEmailObserver;
		UserAccount.prototype.removeFullNameObserver = removeFullNameObserver;
		UserAccount.prototype.removeLavuAdminObserver = removeLavuAdminObserver;
		UserAccount.prototype.removeOptionsObserver = removeOptionsObserver;
		UserAccount.prototype.removeSortingFunctionObserver = removeSortingFunctionObserver;
		UserAccount.prototype.removeTypeObserver = removeTypeObserver;
		UserAccount.prototype.removeUserIDObserver = removeUserIDObserver;
		UserAccount.prototype.removeUserNameObserver = removeUserNameObserver;
		UserAccount.prototype.removeUserObserver = removeUserObserver;
		UserAccount.prototype.setSortingFunction = setSortingFunction;
		UserAccount.prototype.shouldFilter = shouldFilter;
		UserAccount.prototype.updateItemWithItem = updateItemWithItem;

		UserAccount.createUser = createUser;
	})();

	( function() {
		function ByteSize( size_in_bytes ){
			if(! size_in_bytes ){
				return null;
			}

			this._size_in_bytes = size_in_bytes;
		}

		function getSizeInBytes() {
			return this._size_in_bytes;
		}

		function returnBytesByDenominationPower( bytes, power ){
			return bytes / (Math.pow( 1024, power ));
		}

		function getSizeInKiloBytes() {
			return returnBytesByDenominationPower( this._size_in_bytes, 1 );
		}

		function getSizeInMegaBytes() {
			return returnBytesByDenominationPower( this._size_in_bytes, 2 );
		}

		function getSizeInGigaBytes() {
			return returnBytesByDenominationPower( this._size_in_bytes, 3 );
		}

		function getSizeInTeraBytes() {
			return returnBytesByDenominationPower( this._size_in_bytes, 4 );
		}

		function getSizeInPetaBytes() {
			return returnBytesByDenominationPower( this._size_in_bytes, 5 );
		}

		function getSizeInExaBytes() {
			return returnBytesByDenominationPower( this._size_in_bytes, 6 );
		}

		function getSizeInZettaBytes() {
			return returnBytesByDenominationPower( this._size_in_bytes, 7 );
		}

		function getSizeInYottaBytes() {
			return returnBytesByDenominationPower( this._size_in_bytes, 8 );
		}

		Lavu.ByteSize = ByteSize;
		ByteSize.prototype = {};
		ByteSize.prototype.constructor = ByteSize;
		ByteSize.prototype.getSizeInBytes = getSizeInBytes;
		ByteSize.prototype.getSizeInKiloBytes = getSizeInKiloBytes;
		ByteSize.prototype.getSizeInMegaBytes = getSizeInMegaBytes;
		ByteSize.prototype.getSizeInGigaBytes = getSizeInGigaBytes;
		ByteSize.prototype.getSizeInTeraBytes = getSizeInTeraBytes;
		ByteSize.prototype.getSizeInPetaBytes = getSizeInPetaBytes;
		ByteSize.prototype.getSizeInExaBytes = getSizeInExaBytes;
		ByteSize.prototype.getSizeInZettaBytes = getSizeInZettaBytes;
		ByteSize.prototype.getSizeInYottaBytes = getSizeInYottaBytes;
	})();

})();
//Lavu.views
(function(){
	Lavu.views = {};

	//Lavu.views.HTMLElementView
	(function(){
		function _ensureClassList( element ) {
			if(! element ){
				return false;
			}

			if(! (element instanceof HTMLElement) ){
				return false;
			}

			if( element.classList ){
				return false;
			}

			element.classList = new Lavu.ClassList( element );
			return true;
		}

		/**
		 * An MVC View, represents a 'view' that is able to display information when provided a Lavu.mvc.Model.
		 * The Lavu.mvc.View is represented as an object and creates an HTML Element for which it is
		 * responsible for. In this case, the default HTML Element is a div.  A Lavu.mvc.View can also have
		 * a parent and children. It is only responsible for itself however, and is not requires to inform
		 * it's children of any updates.
		 **/
		function HTMLElementView() {
			Lavu.mvc.View.call( this );
			this._element = document.createElement('div');
			this._element.view = this;
			_ensureClassList( this._element );
			this._parentView = null;
			this._displayStyle = null;
			this._childrenViews = [];
		}

		/**
		 * Convenience method for retrieving the HTML Element that this view is responsible for.
		 *
		 * @returns an HTMLElement that is the 'frame' for this Lavu.mvc.View.
		 **/
		function getElement() {
			return this._element;
		}

		/**
		 * This method will attempt to assign the parent of the Lavu.mvc.View to the provided Lavu.mvc.View.
		 * This should only be called internally as it doesn't do anything other than a reference assignment.
		 * There is no 'cleanup' beyond the assignment.  You should not use this function to add a Lavu.mvc.View
		 *  to another Lavu.mvc.View.
		 *
		 * @param parentView: the Lavu.mvc.View that is to be the parentView.
		 *
		 * @returns: true if assignment is successful, false otherwise.  This will not succeed if the provided
		 *			parentView Is not a Lavu.mvc.View.
		 **/
		function setParent( parentView ){
			if(! parentView ) {
				this._parentView = null;
				return true;
			}

			if(! parentView instanceof Lavu.mvc.View ) {
				return false;
			}

			this._parentView = parentView;
			return true;
		}

		/**
		 * This method returns the currently assignment parentView.
		 *
		 * @returns: the Lavu.mvc.View that is to be this Lavu.mvc.View's parent.
		 **/
		function getParent() {
			return this._parentView;
		}

		/**
		 * This method attempts to take a Lavu.mvc.View and adds it to this Lavu.mvc.View as a child.  In
		 * doing so successfully this will also change the parent of the provided Lavu.mvc.View to itself,
		 * as well as setting up the HTML Element heirarchy that is needed automatically by appending the
		 * HTML Element to it's own HTML Element.
		 *
		 * @param view: the Lavu.mvc.View to add as a child of this Lavu.mvc.View.
		 *
		 * @returns: true upon success, false otherwise.  Please note that this will not succeed if view
		 * is not a Lavu.mvc.View, or if the provided Lavu.mvc.View is already a child of this Lavu.mvc.View.
		 **/
		function addSubView( view ) {
			if(! view ) {
				return false;
			}

			if(! view instanceof Lavu.mvc.View ){
				return false;
			}

			if( this._childrenViews.indexOf( view ) >= 0 ) {
				return false;
			}

			if( view.getParent() ) {
				view.getParent().removeSubView( view );
			}

			view.setParent( this );
			this._childrenViews.push( view );
			this._element.appendChild( view.getElement() );
			this._hidden = false;
			return true;
		}

		/**
		 * This method attempts to remove the provided Lavu.mvc.View from itself, and does the associated
		 * cleanup by removing the Heirarchy connection between the HTML Element of the two Lavu.mvc.Views.
		 * It will also set the provided Lavu.mvc.View's parent to null to signify that it no longer has a
		 * Parent.
		 *
		 * @param view: the Lavu.mvc.View to remove from this Lavu.mvc.View.
		 *
		 * @returns: true if the removal of the Lavu.mvc.View was successful, false otherwise.  It will not
		 *			remove the view if it is not a Lavu.mvc.View, or if this Lavu.mvc.View does not contain the
		 *			provided Lavu.mvc.View.
		 **/
		function removeSubView( view ) {
			if(! view ){
				return false;
			}

			if(! view instanceof Lavu.mvc.View ) {
				return false;
			}

			var index = this._childrenViews.indexOf( view );
			if( index < 0 ){
				return false;
			}

			this._childrenViews.splice( index, 1 );
			view.getElement().parentNode.removeChild( view.getElement() );
			view.setParent( null );
			return true;
		}

		/**
		 * This method will replace this Lavu.mvc.View's current HTML Element with the specified HTML Element.
		 * Use this if you don't want the Lavu.mvc.View's HTML Element to be a div, or if you want to associate
		 * the Lavu.mvc.View with an already existing element.
		 *
		 * @param element: the HTML Element to bind this Lavu.mvc.View with.
		 *
		 * @returns true if successful, false otherwise.  Will not success if no element is specified, or if it
		 *			is not an HTML Element.
		 **/
		function setHTMLElement( element ) {
			if(! element ) {
				return false;
			}

			if(! (element instanceof HTMLElement) ){
				return false;
			}

			this._displayStyle = null;
			this._element = element;
			element.view = this;
			_ensureClassList( this._element );

			return true;
		}

		/**
		 * This method will hide this Lavu.mvc.View's HTML Element by setting it's style.display to none.
		 * Before it does this, it stores whatever it's current style.display value is, in order to
		 * restore it with the same value;
		 *
		 * @returns void;
		 **/
		function hide() {
			if( this._element.style.display != 'none' ) {
				this._displayStyle = this._element.style.display;
				this._element.style.display = 'none';
				this._hidden = true;
			}
		}

		/**
		 * This method will 'show' this Lavu.mvc.View's HTML Element by setting it's style.display to
		 * whatever it was when it was hidden.  This will only attempt to restore it if the HTML
		 * Elements's style.display is none
		 **/
		function show() {
			if( this._element.style.display == 'none' ) {
				this._element.style.display = this._displayStyle;
				this._hidden = false;
			}
		}

		function getChildrenViews(){
			return this._childrenViews.slice( 0, this._childrenViews.length );
		}

		function killAllChildren(){
			var result = true;
			while( this._childrenViews.length ){
				result = result && this._childrenViews[0].killAllChildren();
				result = result && this.removeSubView( this._childrenViews[0] );
			}

			return result;
		}

		function __getAbsolutePosition( element ){
			var left = 0;
			var top = 0;

			while( element ) {
				left += element.offsetLeft;
				top += element.offsetTop;
				element = element.offsetParent;
			}

			return { left : left, top  : top };
		}

		function getAbsolutePosition(){
			var width = this.getElement().offsetWidth;
			var height = this.getElement().offsetHeight;

			var positions = __getAbsolutePosition( this.getElement() );
			return { left : positions.left, top : positions.top, width : width, height : height };
		}

		/**
		 * This method will return the current 'Hidden' state of this Lavu.mvc.View.  If you have called.
		 * .hide() on this Lavu.mvc.View this will return true, and false if you haven't, or if you have
		 * called .show() afterwards.
		 *
		 * @returns true if this Lavu.mvc.View is currently Hidden, false otherwise.
		 **/
		function isHidden(){
			return this._hidden;
		}

		/* Object setup and Inheritence */
		Lavu.views.HTMLElementView = HTMLElementView;
		HTMLElementView.prototype = Object.create( Lavu.mvc.View.prototype );
		HTMLElementView.prototype.constructor = HTMLElementView;
		HTMLElementView.prototype.getElement = getElement;
		HTMLElementView.prototype.setParent = setParent;
		HTMLElementView.prototype.getParent = getParent;
		HTMLElementView.prototype.addSubView = addSubView;
		HTMLElementView.prototype.removeSubView = removeSubView;
		HTMLElementView.prototype.hide = hide;
		HTMLElementView.prototype.show = show;
		HTMLElementView.prototype.setHTMLElement = setHTMLElement;
		HTMLElementView.prototype.getChildrenViews = getChildrenViews;
		HTMLElementView.prototype.killAllChildren = killAllChildren;
		HTMLElementView.prototype.getAbsolutePosition = getAbsolutePosition;
		HTMLElementView.prototype.isHidden = isHidden;
	})();

	//Lavu.views.ClassView
	(function(){
		function ClassView( classes ) {
			Lavu.views.HTMLElementView.call( this );
			if(! classes ) {
				return null;
			}

			if(! classes instanceof Array ){
				return null;
			}

			Lavu.views.HTMLElementView.call( this );
			for( var i = 0; i < classes.length; i++ ){
				this._element.classList.add( classes[i] );
			}
		}

		function draw( model ){
			return true;
		}

		Lavu.views.ClassView = ClassView;
		ClassView.prototype = Object.create( Lavu.views.HTMLElementView.prototype );
		ClassView.prototype.constructor = ClassView;
		ClassView.prototype.draw = draw;
	})();

	//Lavu.views.CustomStyleView
	(function(){
		//Customized Style View
		function CustomStyleView( style ) {
			Lavu.views.HTMLElementView.call( this );
			if(! style ) {
				return null;
			}

			if(! style instanceof Object ){
				return null;
			}
			Lavu.views.HTMLElementView.call( this );
			for( var key in style ) {
				if( this._element.style[key] !== null ) {
					this._element.style[key] = style[key];
				}
			}
		}

		function draw( model ) {
			return true;
		}

		Lavu.views.CustomStyleView = CustomStyleView;
		CustomStyleView.prototype = Object.create( Lavu.views.HTMLElementView.prototype );
		CustomStyleView.prototype.constructor = CustomStyleView;
		CustomStyleView.prototype.draw = draw;
	})();

	//Lavu.views.TextArea
	(function(){
		function TextArea( classes ){
			Lavu.views.ClassView.call( this, classes );
			this.setHTMLElement( document.createElement('textarea'));
			for( var i = 0; i < classes.length; i++ ){
				this._element.classList.add( classes[i] );
			}

			this.getElement().addEventListener( 'keydown', this );
			this.getElement().addEventListener( 'keypress', this );
			this.getElement().addEventListener( 'input', this );
			this._regex = null;
			this._length = null;
			this._value = new Lavu.util.ObservableField( this, 'value', '' );
		}

		function setRegex( regex ){
			if(! regex ){
				return false;
			}

			if(! regex instanceof RegExp ){
				return false;
			}

			this._regex = regex;
			return true;
		}

		function setLength( length ){
			if(! length ){
				this._length = null;
				return true;
			}

			if( isNaN( Number( length ) ) ){
				return false;
			}

			this._length = Number( length );
			return true;
		}

		function handleEvent( event ) {
			if( event.type == 'keydown' || event.type == 'keypress') {
				if( [ 8, 46 ].indexOf( event.keyCode ) === -1 ) {
					if( this._length && event.target.value.length >= this._length ) {
						event.preventDefault();
					}

					if( event.keyIdentifier ){
						var code = String.fromCharCode( '0x' + event.keyIdentifier.replace(/[\+uU]/ig,'') );
						if( this._regex && !code.match( this._regex ) ) {
							event.preventDefault();
						}
					} else {
						if( this._regex && ! this._regex.test( String.fromCharCode( event.keyCode ) ) ) {
							event.preventDefault();
						}
					}
				}
			} else if( event.type == 'input' ) {
				if( this._length && event.target.value.length > this._length ) {
					event.target.value = event.target.value.substring( 0, this._length );
				}

				if( this._regex ){
					event.target.value = event.target.value.match( this._regex ).join("");
				}

				this._value.setFieldValue( event.target.value );
			}
		}

		function addValueObserver( observer ){
			return this._value.addObserver( observer );
		}

		function removeValueObserver( observer ){
			return this._value.removeObserver( observer );
		}

		function draw( model ){
			return true;
		}

		function setValue( value ){
			this.getElement().value = value;
			this._value.setFieldValue( value );
		}

		Lavu.views.TextArea = TextArea;
		TextArea.prototype = Object.create( Lavu.views.ClassView.prototype );
		TextArea.prototype.constructor = TextArea;
		TextArea.prototype.draw = draw;
		TextArea.prototype.setRegex = setRegex;
		TextArea.prototype.setLength = setLength;
		TextArea.prototype.handleEvent = handleEvent;
		TextArea.prototype.addValueObserver = addValueObserver;
		TextArea.prototype.removeValueObserver = removeValueObserver;
		TextArea.prototype.setValue = setValue;
	})();
})();

//Lavu.sendPOST()