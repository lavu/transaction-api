// get each header and grouping
function get_header_grouping_pairs() {
	var headers = $(".manage_grouping_header");
	var groupings = $(".manage_grouping");
	var pairs = {};
	for (var i = 0; i < headers.length; i++) {
		if (groupings.length > i) {
			pairs[i] = {header:headers[i],grouping:groupings[i]};
		}
	}
	return pairs;
}

// make each grouping collapsable, by its header
function create_collapsables() {

	// get each header and grouping
	var pairs = get_header_grouping_pairs();

	// create collapsables for groupings that don\'t have them
	$.each(pairs, function(k, pair) {
		var jheader = $(pair.header);
		if (jheader.find(".collapsable").length === 0) {
			jheader.html("<div class=\'collapsable\'>></div>"+jheader.html());
			jheader.css({ cursor: "pointer" });
		}
	});

	$.each(pairs, function(k,pair) {
		$(pair.header).unbind('click');
		$(pair.header).click(function() {
			manage_grouping_collapse(k);
		});
	});
}

// collapse or uncollapse a grouping based on its header and index
function manage_grouping_collapse(index) {

	// get the header/grouping pair
	var pairs = get_header_grouping_pairs();
	if (typeof(pairs[index]) == "undefined")
		return false;
	var pair = pairs[index];
	jheader = $(pair.header);
	jgrouping = $(pair.grouping);

	// uncollapse the grouping
	if (jheader.hasClass("collapsed")) {
		jheader.removeClass("collapsed");
		jgrouping.stop(true, true);
		jgrouping.show(100);
	}

	// collapse the grouping
	else {
		jheader.addClass("collapsed");
		jgrouping.stop(true, true);
		jgrouping.hide(100);
	}
}

function collapse_collapsables() {

	// get the header/grouping pairs
	var pairs = get_header_grouping_pairs();

	$.each(pairs, function(k, v) {
		if (!$(v.header).hasClass("collapsed"))
			manage_grouping_collapse(k);
	});
}