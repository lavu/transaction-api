(function(){
	(function(){
		function Initialize(){
			this.run();
			Object.call( this );
		}

		function run(){
			// alert( 'test' );
			if( !document.registerElement ){

			}
		}

		window.Initialize = Initialize;
		Initialize.prototype.constructor = Initialize;
		Initialize.prototype = {};
		Initialize.prototype.run = run;
	})();

	(function(){
		function CustomElementLoader(){
			this._registeredElements = {};
			this.ogce = document.createElement;
		}

		var _customElementLoader;
		function loader(){
			if( !_customElementLoader ){
				_customElementLoader = new CustomElementLoader();
			}
			return _customElementLoader;
		}

		function createElement(){
			var node = this.ogce.apply(document,arguments);
			var nodeName = node.tagName.toLowerCase();

			if( this._registeredElements[ nodeName ] !== undefined && !(node instanceof this._registeredElements[ nodeName ]) ){
				if( node.setPrototypeOf ){
					node.setPrototypeOf( this._registeredElements[ nodeName ].prototype );
				} else {
					node.__proto__ = this._registeredElements[ nodeName ].prototype;
				}

				if( node.createdCallback && "function" == typeof node.createdCallback ){
					node.createdCallback();
				}
			}

			return node;
		}

		function lookForCustomNodes( node ){
			if( !node.tagName ){
				return;
			}
			var nodeName = node.tagName.toLowerCase();
			if( 'template' == node.tagName.toLowerCase() ){
				return;
			}
			if( this._registeredElements[ nodeName ] !== undefined && !(node instanceof this._registeredElements[ nodeName ]) ){
				//This has been previously registered
				if( node.setPrototypeOf ){
					node.setPrototypeOf( this._registeredElements[ nodeName ].prototype );
				} else {
					node.__proto__ = this._registeredElements[ nodeName ].prototype;
				}
				// this._registeredElements[ nodeName ].call( node );

				if( node.createdCallback && "function" == typeof node.createdCallback ){
					node.createdCallback();
				}
			}

			if( node.attachedCallback && "function" == typeof node.attachedCallback ){
				node.attachedCallback();
			}

			for( var i = 0; i < node.childNodes.length; i++ ){
				this.lookForCustomNodes( node.childNodes[i] );
			}
		}

		function mutationObservation( mutations ){
			// console.log( mutations );
			for( var i = 0; i < mutations.length; i++ ){
				var mutationRecord = mutations[i];
				for( var j = 0; j < mutationRecord.addedNodes.length; j++ ){
					var addedNode = mutationRecord.addedNodes[j];
					this.lookForCustomNodes( addedNode );
				}

				for( var j = 0; j < mutationRecord.removedNodes.length; j++ ){
					var removedNode = mutationRecord.removedNodes[j];
					if( !(removedNode instanceof HTMLElement) ){
						continue;
					}

					if( removedNode.detachedCallback && "function" == typeof removedNode.detachedCallback ){
						removedNode.detachedCallback();
					}
				}

				if( mutationRecord.type == 'attributes' ){
					var tar = mutationRecord.target;
					if( tar.attributeChangedCallback && "function" == typeof tar.attributeChangedCallback ){
						tar.attributeChangedCallback( mutationRecord.attributeName, mutationRecord.oldValue, tar.getAttribute( mutationRecord.attributeName ) );
					}
				}
			}
		}

		function registerElement( tagname, obj ){

			var lowerTagName = tagname.toLowerCase();
			var replacedTagName = tagname.replace(/-/g, '_' );

			if( this._registeredElements[lowerTagName] ){
				var err = new Error("Failed to execute 'registerElement' on 'Document': Registration failed for type '"+tagname+"'. A type with that name is already registered.");
				err.code = 11;
				throw err;
			}

			this._registeredElements[lowerTagName] = function(){};
			this._registeredElements[lowerTagName].prototype = Object.create( obj.prototype );

			return this._registeredElements[lowerTagName];
		}

		window.CustomElementLoader = CustomElementLoader;
		CustomElementLoader.prototype.constructor = CustomElementLoader;
		CustomElementLoader.prototype = Object.create( Object.prototype );
		CustomElementLoader.prototype.lookForCustomNodes = lookForCustomNodes;
		CustomElementLoader.prototype.registerElement = registerElement;
		CustomElementLoader.prototype.createElement = createElement;
		CustomElementLoader.prototype.mutationObservation = mutationObservation;
		CustomElementLoader.loader = loader;


		if( !document.registerElement ){
			var observer = new MutationObserver( CustomElementLoader.loader().mutationObservation.bind( CustomElementLoader.loader() ) );
			observer.observe( document.documentElement, {
				childList: true,
				attributes: true,
				characterData: true,
				subtree: true,
				attributeOldValue: true,
				characterDataOldValue: true
			});

			document.registerElement = CustomElementLoader.loader().registerElement.bind( CustomElementLoader.loader() );
			document.createElement =  CustomElementLoader.loader().createElement.bind( CustomElementLoader.loader() );
		}
	})();

	(function(){
		function CustomElement(){
			var templates = document.getElementsByTagName( 'template' );
			var template;
			for( var i = 0; i < templates.length; i++ ){
				if( templates[i].id.toLowerCase() == this.tagName.toLowerCase() ){
					template = templates[i];
				}
			}
			if( template ){
				if( template.content ){
					this.appendChild( document.importNode( template.content, true ));
				} else {
					for( var j = 0; j < template.childNodes.length; j++ ){
						this.appendChild( document.importNode( template.childNodes[j], true ) );
					}
				}
			}
		}

		window.CustomElement = CustomElement;
		CustomElement.prototype = Object.create( HTMLElement.prototype );
		CustomElement.prototype.constructor = CustomElement;
	})();

	(function(){
		var tabIndex = 1;
		function CustomFocusableElement(){
			CustomElement.call( this );
			if( !this.hasAttribute( 'tabindex' ) ){
				this.setAttribute( 'tabindex', tabIndex++ );
			}
		}

		window.CustomFocusableElement = CustomFocusableElement;
		CustomFocusableElement.prototype = Object.create( CustomElement.prototype );
		CustomFocusableElement.prototype.constructor = CustomFocusableElement;
	})();
})();