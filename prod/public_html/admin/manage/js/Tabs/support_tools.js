var URL = "/manage/misc/SupportToolsFunctions/support_functions.php";
var menu_items = {
	
	time_tracker:   {title:"Restore User", id:"restore_user",    click_fun : show_restore_user}, 
	extensions:     {title:"Restore Menu", id:"restore_menu",    click_fun : show_restore_menu}

}
function get_projects(){

}
function get_support_items(){

}
function support_tools_initialize(){
	build_menu();

}
function build_menu(){
	var menu_container = document.getElementsByClassName("support_tools_menu")[0];
	//console.log(menu_container);
	for( var item in menu_items){
		//console.log(item);
		var men_item = document.createElement("div");
			
			men_item.innerHTML  = menu_items[item].title;
			men_item.id 		= menu_items[item].id;
			men_item.className  = "support_tools_menu_item";
			men_item.addEventListener("click", menu_items[item].click_fun);
			menu_container.appendChild(men_item);
	}
}
function show_restore_user(){
	var container = document.getElementsByClassName("support_tools_content")[0];
	var html = "Dataname: <input type='text' id='dataname_input'/><br>";
	html += "Username: <input type='text' id='username_input'/><br>";
	html += "<input type='button' onclick='submitRestoreUser()' value='Submit'/>";

	container.innerHTML = html;
}
function show_restore_menu(){
	var container 	 	  = document.getElementsByClassName("support_tools_content")[0];
	container.innerHTML = '';
	
}

function submitRestoreUser(){
	var dataname = $('#dataname_input')[0].value;
	var username = $('#username_input')[0].value;
	console.log("restoring " + dataname + " user " + username);
	var data  = performAJAX(URL, "function=restore_user&dataname=" + dataname + "&username=" + username, false, "POST");
	alert(data);
}

function saveDetails(id, val, functionName, field){
	urlString='id='+id+"&function="+functionName+"&val="+val+"&field="+field;
	$.ajax({
		url: "/manage/misc/AdvancedToolsFunctions/extensions/saveExtensionDetails.php",
		type: "POST",
		data:urlString,
		async:false,
		success: function (string){
			if(string=='success'){
				if(functionName=='updateActiveState' && val=='4') //this means deleted. therefore, we need to put the filter over it.
					alert("Deleted!");
			}
			else
				alert(string);
		}
	});
}
function hideDetailsWindow(){
	$("#detailsEditWindowPositioner").fadeOut('slow',function(){
	});
}
function addPermission(element, id){
	table =	$('.detailsSelect').val();
	permList = savePermList();

	console.log("permission:"+permList+" table:"+table);

	urlString ='id='+id+"&function=updatePermissionsList&val="+permList+"&field="+table;
	$.ajax({
		url: "/manage/misc/AdvancedToolsFunctions/extensions/saveExtensionDetails.php",
		type: "POST",
		data:urlString,
		async:false,
		success: function (string){
			if(string=='success'){
				alert("Saved");
			}
			else
				alert(string);
		}
	});
}
function displayTableRows(id, tableName, element){
	if($(".rowsContent").html()){
		savePermList();
	}
	urlString='id='+id+"&function=getTableFields&val="+tableName+"&field";
	$.ajax({
		url: "/manage/misc/AdvancedToolsFunctions/extensions/getExtensionDetails.php",
		type: "POST",
		data:urlString,
		async:false,
		success: function (string){
			if(string!=='fail'){

				$(".rowsContent").html(string);
			}
			else
				alert("could not retrieve rows");
		}
	});
}
function displayTableRowsForComms(id, tableName, element, counter){

	if( element && element.attr('id')=='request')
		urlString='id='+id+"&function=getInputFieldOptions&val="+tableName+"&field="+$("#fieldSelect").val()+"&comm_type="+$('.commsSelected').attr('id')+"&name_editor_id="+counter ;
	else
		urlString='id='+id+"&function=getTableFieldsForComms&val="+tableName+"&field="+$("#fieldSelect").val()+"&comm_type="+$('.commsSelected').attr('id')+"&name_editor_id="+counter ;

	$.ajax({
		url: "/manage/misc/DevToolsFunctions/extensions/getExtensionDetails.php",
		type: "POST",
		data:urlString,
		async:false,
		success: function (string){
			if(string!=='fail'){

				$(".rowsContent").html(string);
			}
			else
				alert("could not retrieve rows");
		}
	});
}
function makeNextOption(elem){
	var input=$("#submitConfigSetting").parent().parent().prev().children().next().children();
	var place= elem.attr('place')*1+1;
	if(elem.val()!=='' && elem.attr('changed')=='true' && input.val()!=='')
		elem.parent().parent().after("</tr><tr><td>Option:</td><td><input type='text' place='"+place+"' name='opt"+place+"' class='selectBoxOption' placeholder='option' onchange='$(this).attr(\"changed\",\"true\")' onblur='makeNextOption($(this))'></td></tr>");
}
function openNameEditor(title,table, id, elem, name_editor_id){
	if($(elem).hasClass('selectedFieldRow')){
		$("#commsName").replaceWith('Do you want to remove this communication?');
		$(".identifierSelector").html('');
	}
	$("#nameEditor"+name_editor_id).dialog( 'open' );
	$('#commNameTitle').html(title +"");
	$("#commNameTable").html(table+"");
	$("#integratorID").html(id);
}
function addToList(name, element){

	var permission= $('.permissionSelect').val();

	if( $(element).attr('permSelected') == permission ){

		$(element).removeAttr('permSelected');
		$(element).css('background-color','');
		$(element).css('color','black');

	}else{

		$(element).attr('permSelected', permission);
		if(permission==1)
			$(element).css('background-color','#1f4bcf');
		else if(permission==2)
			$(element).css('background-color','#cf1f1f');
		else if(permission==3)
			$(element).css('background-color','#8c1fcf');

		$(element).css('color','white');
	}
}
function addToCommsList(table, title, name, isIdentifier, integratorID, counter){
	field= $('#fieldSelect').val();
	commArea= $('.commsSelected').attr('id');
	//saveComms(table, title, name, field, commArea, integratorID );
	if(isIdentifier)
		isIdentifier=1;
	else
		isIdentifier=0;
	urlString='table='+table.trim()+"&givenName="+name+"&column="+title.trim()+"&field="+field+"&commArea="+commArea+"&isIdentifier="+isIdentifier+"&integratorID="+integratorID;
	alert(urlString);
	$.ajax({
		url: "/manage/misc/AdvancedToolsFunctions/extensions/saveCommunications.php",
		type: "POST",
		data:urlString,
		async:false,
		success: function (string){
			displayTableRowsForComms(integratorID, table, '', counter);
		}
	});

	}
	function openComms(id, elem, tabName){
	if(tabName)
		urlString='id='+id+"&function=comms&val="+$(elem).val()+"&tab="+tabName+"&name_editor_id="+$(elem).attr("name_editor_id");
	else
		urlString='id='+id+"&function=comms&val="+$(elem).val()+"&tab="+$(elem).attr('id')+"&name_editor_id="+$(elem).attr("name_editor_id");

	$.ajax({
		url: "/manage/misc/DevToolsFunctions/extensions/getCommsDetails.php",
		type: "POST",
		data:urlString,
		async:false,
		success: function (string){
			if(string!=='fail'){
				$(".commsSelected").css("background-color","#aecd37");
				$(".commsSelected").css("color","white");
				$(".commsSelected").removeClass('commsSelected');
				if(tabName){
					$("#"+tabName).attr('class','commsSelected');
					$("#"+tabName).css("background-color","white");
					$("#"+tabName).css("color","#aecd37");
				}else{
					$(elem).attr('class','commsSelected');
					$(elem).css("background-color","white");
					$(elem).css("color","#aecd37");
				}
				$("#commsContainer").html(string);
			}
			else
				alert("could not retrieve rows");
		}
	});
}
function saveConfigSetting(form){
	$.ajax({
		url: "/manage/misc/AdvancedToolsFunctions/extensions/saveUserRequests.php",
		type: "POST",
		data:form.serialize()+"&type="+$(".detailsSelect").val()+"&commArea="+$(".commsSelected").attr('id'),
		async:false,
		success: function (string){
			if(string!=='fail'){
				alert(string);
			}else
				alert("could not retrieve rows");
		}
	});
}
function report_creator(){
	var container = document.getElementsByClassName("support_tools_content")[0];
		container.innerHTML = '';
	var frame = document.createElement("iframe");
		frame.setAttribute("src","http://admin.poslavu.com/manage/misc/AdvancedToolsFunctions/report_builder.php");
		frame.style.height ="100%";
		frame.style.width = "100%";
	
	container.appendChild(frame);
	//<iframe src='http://admin.poslavu.com/manage/misc/AdvancedToolsFunctions/report_builder.php' height='100%' width='100%'></iframe>
}
function edit_modules(){
	var container = document.getElementsByClassName("support_tools_content")[0];
		container.innerHTML = '';
	var data  = performAJAX(URL, "function=edit_modules",false, "POST");
	container.innerHTML = data;
}
function create_account(){
	var container = document.getElementsByClassName("support_tools_content")[0];
		container.innerHTML = '';
	var data  = performAJAX(URL, "function=create_account",false, "POST");
	container.innerHTML = data;
}
function doCreateAccount(form){

	$.ajax({
		url: "/manage/misc/AdvancedToolsFunctions/create_account.php",
		type: "POST",
		data:form.serialize(),
		async:false,
		success: function (string){
			if(string!=='fail'){
				alert(string);
			}else
				alert("could not retrieve rows");
		}
	});
}
function perform_create_account(elem){
	var form= document.getElementById("createAccountForm");
	var post_str = $(form).serialize();
	alert(post_str);
	res = performAJAX("/manage/misc/AdvancedToolsFunctions/create_account.php", post_str,false,"POST");
	alert(res);
}
function subsite_builder(){
	var container = document.getElementsByClassName("support_tools_content")[0];
		container.innerHTML = '';
	var data  = performAJAX(URL, "function=build_website",false, "POST");
	container.innerHTML = data;
}
function performAJAX(URL, urlString,asyncSetting,type ){
	var returnVal='';
	loading();
	$.ajax({
		url: URL,
		type: type,
		data:urlString,
		async:asyncSetting,
		success: function (string){
				try{
					returnVal = JSON.parse(string);
					if( returnVal.result=="error")
						alert(returnVal.message);
				}catch(err){
					returnVal = string;
					//alert(string);
					return;
				}
			}
	});
	doneLoading();
	return returnVal;
}
/*
window.onload = function(){
	support_tools_initialize();
}*/