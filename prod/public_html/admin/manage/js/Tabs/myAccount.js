$(function() {
	$('.datepickerStart').datepicker();
	$('.datepickerEnd').datepicker();
	$('.start_picker').datepicker();
	$('.end_picker').datepicker();
    // increase the default animation speed to exaggerate the effect

	var result='';
	var clickedUsername='';
	var clickedID= '';
 

 	
	$( '#dialog' ).dialog({
    	autoOpen: false,
    	show: 'blind',
    	hide: 'explode',
    	buttons: {
        'Approve': function() {
     		approveOrNot(clickedID ,1,clickedUsername, get_username() );
            $( this ).dialog( 'close' ); 
        },
        'Deny': function() {
        	
        	approveOrNot(clickedID,0,clickedUsername, get_username() );
            $( this ).dialog( 'close' );
           
        }
    }
});

 	
$( '#daysOffDialog' ).dialog({
	autoOpen: false,
	show: 'blind',
	hide: 'explode',
	width:'700px',
	buttons: {
        'Ok': function() {
            $( this ).dialog( 'close' ); 
        }
    }
});
 	
	$( '#previousDaysOff' ).dialog({
    	autoOpen: false,
    	show: 'blind',
    	hide: 'explode',
    	buttons: {
        'Ok': function() {
        	var note= "manager past time insert: "+ $("#past_days_off_note").val();
        		
          sendTimeOffRequest($('#pastDaysOffStart').val(),$('#pastDaysOffEnd').val(),note, username, get_username());
          $( this ).dialog( 'close' ); 
         
          goToAccount($('#loggedIn').attr('name'));
        }
    }
});



$( '#previousDaysOff').parent().css('width','595px');
});

$(document).ready(function(){
	
	$('table.allUserData').scrollbarTable();
	$('table.timeOffRequests').scrollbarTable();
	$('table.LateNotices').scrollbarTable();
	
	var table= $('.allUserData').last();
$('#position, #name, #numDays').each(function(){
    
    var th = $(this),
        thIndex = th.index(),
        inverse = false;
    
    th.click(function(){
        
        table.find('td').filter(function(){
            
            return $(this).index() === thIndex;
            
        }).sortElements(function(a, b){
            
            return $.text([a]) > $.text([b]) ?
                inverse ? -1 : 1
                : inverse ? 1 : -1;
            
        }, function(){
            
            // parentNode is the element we want to move
            return this.parentNode; 
            
        });
        
        inverse = !inverse;     
    });     
});	
 var table2= $('.timeOffRequests').last();
$('#time_off_username, #time_off_request_date, #time_off_start_date,#time_off_end_date, #time_off_message, #time_off_approval_status').each(function(){
    
    var th = $(this),
        thIndex = th.index(),
        inverse = false;
    
    th.click(function(){
        
        table2.find('td').filter(function(){
            
            return $(this).index() === thIndex;
            
        }).sortElements(function(a, b){
            
            return $.text([a]) > $.text([b]) ?
                inverse ? -1 : 1
                : inverse ? 1 : -1;
            
        }, function(){
            
            // parentNode is the element we want to move
            return this.parentNode; 
            
        });
        
        inverse = !inverse;     
    });     
});
var table3= $('.LateNotices').last();
$('#late_notice_username, #late_notice_submit_date, #late_notice_mins_late,#late_notice_message').each(function(){
    
    var th = $(this),
        thIndex = th.index(),
        inverse = false;
    
    th.click(function(){
 		  
        table3.find('td').filter(function(){
            
            return $(this).index() === thIndex;
            
        }).sortElements(function(a, b){
            
            return $.text([a]).toLowerCase() > $.text([b]).toLowerCase() ?
                inverse ? -1 : 1
                : inverse ? 1 : -1;
            
        }, function(){
            
            // parentNode is the element we want to move
            return this.parentNode; 
            
        });
        
        inverse = !inverse;     
    });     
});
});


function reset_dialog(){
	$( '#dialog' ).dialog({
	    	autoOpen: false,
	    	show: 'blind',
	    	hide: 'explode',
	    	buttons: {
	        'Approve': function() {
	     		approveOrNot(clickedID ,1,clickedUsername,get_username() );
	            $( this ).dialog( 'close' ); 
	        },
	        'Deny': function() {
	        	
	        	approveOrNot(clickedID,0,clickedUsername, get_username() );
	            $( this ).dialog( 'close' );
	           
	        }
	    }
	});
	$('#dialog').html("Approve or Deny?");
}
function submitLateNotice(reason, minutes, name){
	reason = reason || $("#reason").val();
	minutes = minutes || $("#minsLate").val();
	name = name || $("#name").val();
	//alert("reason:  "+ reason+"  mins:  "+ minutes);
	var urlString="reason="+reason+"&minutes="+minutes+"&name="+name;
	loading();
	$.ajax({
		url: "/manage/misc/MyAccount/submitLateNotice.php",
		type: "post",
		data:urlString,
		async:false,
		success:
			function (string){
				if(string=='success') {
					doneLoading();
					alert("Notice Submitted");
					window.location.reload();
				}else
					alert(string);
			}
	});

}
function sendTimeOffRequest(startDate, endDate, message, username, manager_name){
	var urlString = "startDate="+startDate+"&endDate="+endDate+"&message="+message+"&username="+username+"&manager_username="+manager_name;
	var result = performAJAX( "/manage/misc/MyAccount/sendTimeOffRequest.php",urlString,false,"POST");
	var obj = jQuery.parseJSON(result);
	var note = Lavu.currentUser.getFullName()+" has added a timeoff request. You can approve or deny this request in your My Account section.";
	//console.log(obj);
	//console.log(note);
	loading();
	$(".datepickerStart").val("");
	$(".datepickerEnd").val("");
	$("#timeOffReason").val("");
	alert("Time off request sent");
	doneLoading();
	window.location.reload();
}
function cancel_request(username, id){
	var result=performAJAX("/manage/misc/MyAccount/cancel_request.php","username="+username+"&id="+id,false,"POST");
	alert(result);
}
function approveOrNot(id,result, clickedUsername, manager_username){
	var result= performAJAX("/manage/misc/MyAccount/approveOrDeny.php","id="+id+"&result="+result+"&manager="+manager_username+"&username="+clickedUsername,false,"POST");
	alert(result);
}
function performAJAX(URL, urlString,asyncSetting,type ){
	var returnVal='';
    loading();
     $.ajax({
		url: URL,
		type: type,
		data:urlString,
		async:asyncSetting,
		success: 
			function (string){
				returnVal= string; 
			
			}
	});	
	doneLoading();
   return returnVal; 
}
function saveAccountInfo(form){
	performAJAX("/manage/misc/MyAccount/saveAccountInfo.php",form.serialize(),false,"POST");
}
function switchInput(){
	
	if($(".lateType").css("display")=='inline-block'){
		$(".lateType").css("display","none");	
		$(".timeOffType").css('display','inline-block');
	}else{
		$(".lateType").css("display","inline-block");	
		$(".timeOffType").css('display','none');
	}
	
}
function saveNewHoliday(form){
	performAJAX("/manage/misc/MyAccount/saveNewHoliday.php",form,false,"POST");
}
function addDayOff(elem){
	$(".add_days_off").css("display",'block');
}
function get_username(){
	
	return $("#displayAccountInfo").html();
}
