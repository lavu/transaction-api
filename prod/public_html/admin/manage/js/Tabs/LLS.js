function syncLocalDBStruct(rowID, locID, dataname){
	var url= "dataname="+dataname+"&locid="+locID;
	//alert( url);
	$.ajax({
		url: "./misc/LLSFunctions/syncLocalDB.php",
		type: "post",
		data:url,
		success:
			function (string){
					alert(string);
					updatePage(dataname, rowID, locID);
			}
	});
}
function removePendingAction(dataname, rowid, locationID,actionid){
	var url= "dataname="+dataname+"&actionid="+actionid;
	$.ajax({
		url: "./misc/LLSFunctions/removeAction.php",
		type: "post",
		data:url,
		success:
			function (string){
				alert(string);
				updatePage(dataname, rowid, locationID);
			}
	});
}
function performHealthCheck(rowID, locID, dataname){
	var url= "dataname="+dataname+"&locid="+locID;
	//alert( url);
	$.ajax({
		url: "./misc/LLSFunctions/healthCheck.php",
		type: "post",
		data:url,
		success:
			function (string){
					alert(string);
					updatePage(dataname, rowID, locID);
			}
	});
}
function createFileStructure(dataname, rowid, locationID, viewFolders){
	var urlString="dataname="+dataname+"&rowid="+rowid+"&viewFolders="+viewFolders+"&locationID="+locationID;
	//alert( url);
	$.ajax({
		url: "./tabs/LLS.php",
		type: "post",
		data:urlString,
		success:
			function (string){

				$("#LLS").html(string);
			}
		});
}
function createFileStructureForUpdate(dataname, rowid, locationID, viewFolders){
	var urlString="dataname="+dataname+"&rowid="+rowid+"&createUpdate="+viewFolders+"&locationID="+locationID;
	$.ajax({
		url: "./tabs/LLS.php",
		type: "post",
		data:urlString,
		success:
			function (string){
				//alert("updating");
				$("#LLS").html(string);
			}
		});
}


function updatePage(dataname, rowid, locationID, viewFolders){

	var url="dataname="+dataname+"&rowid="+rowid+"&locationID="+locationID+"&viewFolders="+viewFolders;

	$.ajax({
		url: "./tabs/LLS.php",
		type: "post",
		data:url,
		success:
			function (string){
				//alert("updating");
				$("#LLS").html(string);
			}
		});
}

var updates= new Array();

function toUpdate(fileName, operation){
	if (!Array.prototype.indexOf){
		Array.prototype.indexOf = function(elt /*, from*/){
	    	var len = this.length >>> 0;
	    	var from = Number(arguments[1]) || 0;
	    	from = (from < 0)?
	    		Math.ceil(from)
	    		:
	    		Math.floor(from);
	    	if (from < 0)
	    		from += len;
	    	for (; from < len; from++){
		    	if (from in this && this[from] === elt)
		    		return from;
		    }
		    return -1;
		};
	}
	if(operation=='add')
		updates.push(fileName);
	else{
		updates.splice(updates.indexOf(fileName), 1);
	}

	//alert("the number of files to update is: "+ updates.length);
}
function createUpdate(updateName, dataname, rowid, locid ){
	if( updates.length==0){
		alert("Please select files to update.");
		return;
	}
	var files= updates.join();
	var data= "files="+files+"&updateName="+updateName;
	$.ajax({
		url: "./misc/LLSFunctions/createUpdate.php",
		type: "post",
		data: data,
		success:
			function (string){
				alert(string);
				updatePage(dataname, rowid);
			}
	});
}
function doUpdate(dataname, rowid, locid, files ){
	var data="&dataname="+dataname+"&locid="+locid+"&files="+files;
	$.ajax({
		url: "./misc/LLSFunctions/doUpdate.php",
		type: "post",
		data: data,
		success:
			function (string){
				alert(string);
				updatePage(dataname, rowid);
			}
	});
}
function local_query_box(){
	if($(".run_local_query_class").css("display")=="block"){
		$(".run_local_query_btn").val("Run Local Query");
		$(".run_local_query_class").hide();
	}else{
		$(".run_local_query_btn").val("Hide Local Query Box");
		$(".run_local_query_class").show();
	}
}
function run_local_query(loc_id, data_name, p_local_query){

	if(confirm("Are you sure you want to execute this query?")){
		var local_query;
		if(!p_local_query)
			local_query= $(".local_query_val").val();
		else{
			local_query = p_local_query;
		}
		var data='local_query_command='+encodeURIComponent(local_query)+"&locid="+loc_id+"&data_name="+data_name;
		$.ajax({
			url: "./misc/LLSFunctions/run_local_query.php",
			type: "POST",
			data: data,
			success:
				function (string){
					$(".run_local_query_class").html(string);

				}
		});

	}
}

function run_local_bash(loc_id, data_name, bash_command){
	if(!confirm("Are you sure you want to execute this bash command on next sync? Command:[" + bash_command + "]")){
		return;
	}
	var data='bash_command='+encodeURIComponent(bash_command)+"&locid="+loc_id+"&data_name="+data_name;
		$.ajax({
			url: "./misc/LLSFunctions/run_local_bash.php",
			type: "POST",
			data: data,
			success:
				function (string){
					$(".run_local_query_class").html(string);
				}
		});
}
