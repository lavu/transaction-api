var URL = "/manage/misc/DevToolsFunctions/get_data.php";
var menu_items = {
	extensions:     {title:"Extensions",       id:"extensions",      click_fun : show_extensions},
	report_creator: {title:"Report Creator",   id:"report_creator",  click_fun : report_creator}, 
	edit_modules:   {title:"Edit Modules",     id:"edit_modules",    click_fun : edit_modules},
	create_account: {title:"Create Account",   id:"create_account",  click_fun : create_account},
	subsite_builder:{title:"Subsite Builder",  id:"subsite_builder", click_fun : subsite_builder}

}
function get_projects(){

}
function get_dev_items(){

}
function dev_tools_initialize(){
	build_menu();

}
function build_menu(){
	var menu_container = document.getElementsByClassName("dev_tools_menu")[0];
	//console.log(menu_container);
	for( var item in menu_items){
		//console.log(item);
		var men_item = document.createElement("div");
			
			men_item.innerHTML  = menu_items[item].title;
			men_item.id 		= menu_items[item].id;
			men_item.className  = "dev_tools_menu_item";
			men_item.addEventListener("click", menu_items[item].click_fun);
			menu_container.appendChild(men_item);
	}
}
function show_extensions(){
	var container 	 	  = document.getElementsByClassName("dev_tools_content")[0];
	container.innerHTML = '';
	var extensions_html = performAJAX("/manage/misc/DevToolsFunctions/extension_data.php","function=get_extension_data", false, "POST");
	container.innerHTML = extensions_html;
	var counter =1;

	while ($( '#nameEditor'+counter ).length != 0){

		$(  '#nameEditor'+counter ).dialog({			//setting up the name editor for comms in extensions
			autoOpen: false,
			show: 'blind',
			hide: 'explode',
			buttons: {
				'Ok': function() {
					addToCommsList($('#commNameTable').html(),$('#commNameTitle').html(), $('#commsName').val(),$('#useAsIdentifier').val(), $('#integratorID').html(), counter);
					$( '#nameEditor'+counter).html(" <span id='integratorID' style='display:none'></span><span id='commNameTable'> </span> --> <span id='commNameTitle'></span> <br/> <input type='text' id='commsName' placeholder='name for integrator' />");
					$( this ).dialog( 'close' );
				}
			}
		});
		counter++;
	}
}
function openEditor(field, key, id, dbField){

	if(!id){
		if(globalNewId)
			id=globalNewId;
		else{
			id= createRowAndGetID();
			globalNewId= id;
		}
	}
	var val=$(field).html();
	var fieldClass= $(field).attr('class');

	var editor='';

	if( key=='logo'){
		var logoName=  $(field).attr('src');
		editor= '<input id="fileToUpload" name="fileToUpload" onchange="fileToUploadFieldChanged(this, \''+id+'\')" type="file" class="settingsInputlogo_img"><select id="dropDownSelector" onchange="changeLogo(this, \''+id+'\' )"></select>';
		$(field).replaceWith(editor);
		getImgDropDown(logoName);
	}else{
		editor= getEditor(fieldClass, key, val);
		$(field).html(editor);
	}
	$("#extensionFieldEditor").focus();
	$(field).removeAttr('onclick');
	$("#extensionFieldEditor").blur(function(){
		//alert('blur');
		var fieldClass= $(this).attr('class');
		var val= $(this).val();
		var urlString='column='+fieldClass+"&value="+val+"&id="+id+"&field="+dbField;

		$.ajax({
			url: "/manage/misc/AdvancedToolsFunctions/extensions/saveExtensionField.php",
			type: "POST",
			data:urlString,
			async:false,
			success: function (string){
				if(string==='success'){

					if(fieldClass=='typeImg')
						$("#extensionFieldEditor").parent().html("<img src= 'http://admin.poslavu.com/cp/images/"+val+".png' />");
					else if(fieldClass==='features' ){
						$("#extensionFieldEditor").parent().html(val);
						field.after('<li style="margin-bottom:5px;" class="features" onclick="openEditor($(this),\'features\',\''+id+'\',\'undefined\')">Add Feature</li>');
					}else if( fieldClass==='usage'){
						$("#extensionFieldEditor").parent().html(val+"|");
						field.after('<span class="usage" onclick="openEditor($(this),\'usage\', \''+id+'\',\''+(dbField+1)+'\')">Add Feature</span>');
					}else
						$("#extensionFieldEditor").parent().html(val);

					$("#extensionFieldEditor").attr('id','');
					$(field).attr('onclick',"openEditor($(this),'"+key+"','"+id+"','"+dbField+"')");
				}
				else
					alert('changes not saved. Please notify a developer. Error:'+ string);
			}
		});

	});
}
function getEditor(fieldClass, key, val){
	if(key === 'title'){
		return "<input type='text' class='"+fieldClass+"' id='extensionFieldEditor' style='width:200px'value='"+val+"' />";
	}else if(key ==='typeImg'){
		return '<select class="'+fieldClass+'" id="extensionFieldEditor" > <option value="cloudToComputer">cloud To Computer</option><option value="cloudToIOS"> Cloud to IOS</option><option value="computerToIOS">ComputerToIOS </select>';
	}else if(key==='usage'){
		return "<input type='text' class='"+fieldClass+"' id='extensionFieldEditor' style='width:70px;'value='"+val+"' />";
	}else if(key=='features'){
		return "<input type='text' class='"+fieldClass+"' id='extensionFieldEditor' style='width:200px'value='"+val+"' />";
	}
		//return '<input type="file" class="'+fieldClass+'" id="extensionFieldEditor" name="file" >';
}
function changeLogo(element, id){
	var val= $(element).val();

	var urlString="column=logo&value="+val+"&id="+id;

	$.ajax({
		url: "/manage/misc/AdvancedToolsFunctions/extensions/saveExtensionField.php",
		type: "POST",
		data:urlString,
		async:false,
		success: function (string){
			if(string==='success'){
				$(element).parent().html('<img class="logo" src="http://admin.poslavu.com/manage/misc/AdvancedToolsFunctions/extensionLogos/'+val+'" onclick="openEditor($(this),\'logo\',\'84667\')" width="240px" height="60px">');
			}
			else
				alert('changes not saved. Please notify a developer. Error:'+ string);
		}
	});
}
function getImgDropDown(imageName){

    $.post("/manage/misc/AdvancedToolsFunctions/extensions/getLogosAsDropdown.php",
	{
		posted: "TRUE",
		currentImage: imageName
	},
	function(data){
		$('#dropDownSelector').html(data);
	});
}
function fileToUploadFieldChanged(element, idIN){

    $.ajaxFileUpload({
		url:'/manage/misc/AdvancedToolsFunctions/uploadLogo.php',
		secureuri:false,
		fileElementId:'fileToUpload',
		dataType: 'json',
		data:{name:'login', id:idIN },
		success: function (data, status){

			if(typeof(data.error) != 'undefined'){
				if(data.error !== ''){
					alert(data.error);
				//success
                }else{
					//element.replaceWith("<img src='http://admin.poslavu.com/cp/images/"+data.msg+"'/>");
					console.log(data);
					console.log('success');
					$("#fileToUpload").replaceWith("<img src='http://admin.poslavu.com/manage/misc/AdvancedToolsFunctions/extensionLogos/"+data.msg+"' width='240px' height='60px'/ >");
					console.log(idIN);

				}
			}
		},
		error: function (data, status, e){
			alert(e);
        }
	});
}
function createRowAndGetID(){
	urlString='';
	var returnVal;
	$.ajax({
		url: "/manage/misc/AdvancedToolsFunctions/extensions/createExtensionRow.php",
		type: "POST",
		data:urlString,
		async:false,
		success: function (string){
			if(string!=='fail'){
				returnVal=string;
			}
			else
				alert('changes not saved. Please notify a developer. Error:'+ string);
		}
	});
	return returnVal;
}
function openDetailsEditWindow(id, counter){ //id is the id of the integrator
	$("#detailsEditWindowPositioner").css("display","block");
	$("#links").attr('onclick',"showDetails('"+id+"', 'getLinks', 'links')");
	$("#permissions").attr('onclick',"showDetails('"+id+"', 'getPermissions', 'permissions')");
	$("#communications").attr('onclick',"showDetails('"+id+"', 'getCommunications', 'communications', '"+counter+"')");
	showDetails(id, 'getLinks','links');
}
function showDetails(id, functionName, elementID, counter){
	urlString='id='+id+"&function="+functionName+"&val="+counter;
	$.ajax({
		url: "/manage/misc/DevToolsFunctions/extensions/getExtensionDetails.php",
		type: "POST",
		data:urlString,
		async:false,
		success: function (string){
			if(string!=='error'){
				$("#detailsContent").html(string);
				$("#"+elementID).parent().find('.detailsTabSelected').css({"background-color":"#aecd37", "color":"white"});
				$("#"+elementID).parent().find('.detailsTabSelected').removeAttr('class');
				$("#"+elementID).attr("class",'detailsTabSelected');
				$("#"+elementID).css({"background-color":"white", "color":"#aecd37"});
			}
			else
				alert('Could not retrieve information.');
		}
	});
}
function saveDetails(id, val, functionName, field){
	urlString='id='+id+"&function="+functionName+"&val="+val+"&field="+field;
	$.ajax({
		url: "/manage/misc/AdvancedToolsFunctions/extensions/saveExtensionDetails.php",
		type: "POST",
		data:urlString,
		async:false,
		success: function (string){
			if(string=='success'){
				if(functionName=='updateActiveState' && val=='4') //this means deleted. therefore, we need to put the filter over it.
					alert("Deleted!");
			}
			else
				alert(string);
		}
	});
}
function hideDetailsWindow(){
	$("#detailsEditWindowPositioner").fadeOut('slow',function(){
	});
}
function addPermission(element, id){
	table =	$('.detailsSelect').val();
	permList = savePermList();

	console.log("permission:"+permList+" table:"+table);

	urlString ='id='+id+"&function=updatePermissionsList&val="+permList+"&field="+table;
	$.ajax({
		url: "/manage/misc/AdvancedToolsFunctions/extensions/saveExtensionDetails.php",
		type: "POST",
		data:urlString,
		async:false,
		success: function (string){
			if(string=='success'){
				alert("Saved");
			}
			else
				alert(string);
		}
	});
}
function displayTableRows(id, tableName, element){
	if($(".rowsContent").html()){
		savePermList();
	}
	urlString='id='+id+"&function=getTableFields&val="+tableName+"&field";
	$.ajax({
		url: "/manage/misc/AdvancedToolsFunctions/extensions/getExtensionDetails.php",
		type: "POST",
		data:urlString,
		async:false,
		success: function (string){
			if(string!=='fail'){

				$(".rowsContent").html(string);
			}
			else
				alert("could not retrieve rows");
		}
	});
}
function displayTableRowsForComms(id, tableName, element, counter){

	if( element && element.attr('id')=='request')
		urlString='id='+id+"&function=getInputFieldOptions&val="+tableName+"&field="+$("#fieldSelect").val()+"&comm_type="+$('.commsSelected').attr('id')+"&name_editor_id="+counter ;
	else
		urlString='id='+id+"&function=getTableFieldsForComms&val="+tableName+"&field="+$("#fieldSelect").val()+"&comm_type="+$('.commsSelected').attr('id')+"&name_editor_id="+counter ;

	$.ajax({
		url: "/manage/misc/DevToolsFunctions/extensions/getExtensionDetails.php",
		type: "POST",
		data:urlString,
		async:false,
		success: function (string){
			if(string!=='fail'){

				$(".rowsContent").html(string);
			}
			else
				alert("could not retrieve rows");
		}
	});
}
function makeNextOption(elem){
	var input=$("#submitConfigSetting").parent().parent().prev().children().next().children();
	var place= elem.attr('place')*1+1;
	if(elem.val()!=='' && elem.attr('changed')=='true' && input.val()!=='')
		elem.parent().parent().after("</tr><tr><td>Option:</td><td><input type='text' place='"+place+"' name='opt"+place+"' class='selectBoxOption' placeholder='option' onchange='$(this).attr(\"changed\",\"true\")' onblur='makeNextOption($(this))'></td></tr>");
}
function openNameEditor(title,table, id, elem, name_editor_id){
	if($(elem).hasClass('selectedFieldRow')){
		$("#commsName").replaceWith('Do you want to remove this communication?');
		$(".identifierSelector").html('');
	}
	$("#nameEditor"+name_editor_id).dialog( 'open' );
	$('#commNameTitle').html(title +"");
	$("#commNameTable").html(table+"");
	$("#integratorID").html(id);
}
function addToList(name, element){

	var permission= $('.permissionSelect').val();

	if( $(element).attr('permSelected') == permission ){

		$(element).removeAttr('permSelected');
		$(element).css('background-color','');
		$(element).css('color','black');

	}else{

		$(element).attr('permSelected', permission);
		if(permission==1)
			$(element).css('background-color','#1f4bcf');
		else if(permission==2)
			$(element).css('background-color','#cf1f1f');
		else if(permission==3)
			$(element).css('background-color','#8c1fcf');

		$(element).css('color','white');
	}
}
function addToCommsList(table, title, name, isIdentifier, integratorID, counter){
	field= $('#fieldSelect').val();
	commArea= $('.commsSelected').attr('id');
	//saveComms(table, title, name, field, commArea, integratorID );
	if(isIdentifier)
		isIdentifier=1;
	else
		isIdentifier=0;
	urlString='table='+table.trim()+"&givenName="+name+"&column="+title.trim()+"&field="+field+"&commArea="+commArea+"&isIdentifier="+isIdentifier+"&integratorID="+integratorID;
	alert(urlString);
	$.ajax({
		url: "/manage/misc/AdvancedToolsFunctions/extensions/saveCommunications.php",
		type: "POST",
		data:urlString,
		async:false,
		success: function (string){
			displayTableRowsForComms(integratorID, table, '', counter);
		}
	});

	}
	function openComms(id, elem, tabName){
	if(tabName)
		urlString='id='+id+"&function=comms&val="+$(elem).val()+"&tab="+tabName+"&name_editor_id="+$(elem).attr("name_editor_id");
	else
		urlString='id='+id+"&function=comms&val="+$(elem).val()+"&tab="+$(elem).attr('id')+"&name_editor_id="+$(elem).attr("name_editor_id");

	$.ajax({
		url: "/manage/misc/DevToolsFunctions/extensions/getCommsDetails.php",
		type: "POST",
		data:urlString,
		async:false,
		success: function (string){
			if(string!=='fail'){
				$(".commsSelected").css("background-color","#aecd37");
				$(".commsSelected").css("color","white");
				$(".commsSelected").removeClass('commsSelected');
				if(tabName){
					$("#"+tabName).attr('class','commsSelected');
					$("#"+tabName).css("background-color","white");
					$("#"+tabName).css("color","#aecd37");
				}else{
					$(elem).attr('class','commsSelected');
					$(elem).css("background-color","white");
					$(elem).css("color","#aecd37");
				}
				$("#commsContainer").html(string);
			}
			else
				alert("could not retrieve rows");
		}
	});
}
function saveConfigSetting(form){
	$.ajax({
		url: "/manage/misc/AdvancedToolsFunctions/extensions/saveUserRequests.php",
		type: "POST",
		data:form.serialize()+"&type="+$(".detailsSelect").val()+"&commArea="+$(".commsSelected").attr('id'),
		async:false,
		success: function (string){
			if(string!=='fail'){
				alert(string);
			}else
				alert("could not retrieve rows");
		}
	});
}
function report_creator(){
	var container = document.getElementsByClassName("dev_tools_content")[0];
		container.innerHTML = '';
	var frame = document.createElement("iframe");
		frame.setAttribute("src","misc/AdvancedToolsFunctions/report_builder.php");
		frame.style.height ="100%";
		frame.style.width = "100%";
	
	container.appendChild(frame);
	//<iframe src='http://admin.poslavu.com/manage/misc/AdvancedToolsFunctions/report_builder.php' height='100%' width='100%'></iframe>
}
function edit_modules(){
	var container = document.getElementsByClassName("dev_tools_content")[0];
		container.innerHTML = '';
	var data  = performAJAX(URL, "function=edit_modules",false, "POST");
	container.innerHTML = data;
}
function create_account(){
	var container = document.getElementsByClassName("dev_tools_content")[0];
		container.innerHTML = '';
	var data  = performAJAX(URL, "function=create_account",false, "POST");
	container.innerHTML = data;
	$('select[name^=\'country\']').select2();
	$('.select2-container').css({'width':'18.5em', 'backgroud-color':'#fff', 'border':'1px solid #ccc', 'font-size':'13px'});
	$('.select2-container--default .select2-selection--single').attr('style', 'max-width:18.5em !important');
}
function doCreateAccount(form){

	$.ajax({
		url: "/manage/misc/AdvancedToolsFunctions/create_account.php",
		type: "POST",
		data:form.serialize(),
		async:false,
		success: function (string){
			if(string!=='fail'){
				alert(string);
			}else
				alert("could not retrieve rows");
		}
	});
}
function perform_create_account(elem){
	var form= document.getElementById("createAccountForm");
	//var inps = table.getElementsByTagName("input");
	//var post_arr = [];
	/*
	for( var i =0; i<inps.length; i++){
		post_arr.push(inps[i].getAttribute("name")+"="+inps[i].value);
	}*/
	var country= $('#country').val();
	if (country == '') {
		alert('Please select country name');
		return false;
	}
	var post_str = $(form).serialize();
	alert(post_str);
	res = performAJAX("/manage/misc/AdvancedToolsFunctions/create_account.php", post_str,false,"POST");
	alert(res);
}
function subsite_builder(){
	var container = document.getElementsByClassName("dev_tools_content")[0];
		container.innerHTML = '';
	var data  = performAJAX(URL, "function=build_website",false, "POST");
	container.innerHTML = data;
}
function performAJAX(URL, urlString,asyncSetting,type ){
	var returnVal='';
	loading();
	$.ajax({
		url: URL,
		type: type,
		data:urlString,
		async:asyncSetting,
		success: function (string){
				try{
					returnVal = JSON.parse(string);
					if( returnVal.result=="error")
						alert(returnVal.message);
				}catch(err){
					returnVal = string;
					//alert(string);
					return;
				}
			}
	});
	doneLoading();
	return returnVal;
}
/*
window.onload = function(){
	dev_tools_initialize();
}*/
