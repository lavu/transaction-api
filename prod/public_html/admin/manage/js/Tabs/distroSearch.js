$(document).ready(function(){
	$('.distributor_search').keydown(function(key_code){
		if(key_code.which==13){
			//alert($('.distributor_search').val());
			perform_search($('.distributor_search').val(), $(".certified_box").prop('checked'));
		}
	});
});
function perform_search(search_val,certified){
	
	var result= performAJAX("./misc/SearchFunctions/distro_search.php","function=perform_search&search_param="+search_val+"&certified="+certified, false, "POST");
	if(result=='Failed')
		alert("query failed");
	else{
		$(".distro_result_tr").remove();
		$(".distro_result_tr_header").after(result);
	}		
}

function edit_distro(distro_id){
	
	var result= performAJAX("./misc/SearchFunctions/distro_search.php","function=get_distro_info&distro_id="+distro_id, false, "POST");
	var obj= JSON.parse(result);
	for( key in obj){
	
		if($("[name="+key+"]").length){
			if( key=='notes')
				$("[name="+key+"]").html(obj[key]);			
			else
				$("[name="+key+"]").val(obj[key]);
		}	
	}
	$(".distro_edit").dialog({
	
		height:'500',
		width:'400',
	});
	$(".distro_edit").show();
}
function save_distro(form){
	var data= form.serialize();
	var result= performAJAX("./misc/SearchFunctions/distro_search.php","function=save_distro&"+data, false, "POST");
	if( result=='success')
		alert('saved');
	else{
		console.log(result);
		alert("Save Failed");
	}
		
}
function add_reseller(){
	document.getElementById("distro_edit_form").reset();
	$("[name=id]").val("new_distro");
	$(".distro_edit").dialog({
	
		height:'500',
		width:'400',
	});
	$(".distro_edit").show();

}
function performAJAX(URL, urlString,asyncSetting,type ){
	var returnVal='';
    loading();
     $.ajax({
		url: URL,
		type: type,
		data:urlString,
		async:asyncSetting,
		success: 
			function (string){
				returnVal= string; 
			}
	});	
	doneLoading();
   return returnVal; 
}
