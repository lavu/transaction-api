var globalNewId='';
function getPage(urlString, URL, type, id){

	loading();
	$.ajax({
		url: URL,
		type: type,
		data:urlString,
		async:true,
		success: function (string){

			$("#advancedToolsContent").html(string);
			$(".advancedToolsTabSelected").removeClass("advancedToolsTabSelected");
			$("#"+id).addClass("advancedToolsTabSelected");

			doneLoading();
		}
	});
}
function set_lls_th_width() {
	var tr_header = $("#lls_list_th_row");
	var tr_reference = $("#lls_list_th_reference_for_widths");
	var th_children = tr_reference.find("th");
	var th_all = $("th");

	for (var i = 0; i < th_children.length; i++) {
		var th_child = $(th_children[i]);
		var i_width = th_child.width();
		var s_html = th_child.html();
		for (var j = 0; j < th_all.length; j++) {
			var th = $(th_all[j]);
			if (th.html() == s_html) {
				th.css({ width: i_width });
			}
		}
	}
}
function delete_product(prod_id){
	var urlString= "prod_id="+prod_id;
	$.ajax({
		url: "/manage/misc/AdvancedToolsFunctions/delete_product.php",
		type: "POST",
		data:urlString,
		async:false,
		success: function (string){
			alert(string);
			$("#build_website").click();
		}
	});

}
function resize_buttons(){
	var width=0;
	var counter=0;

		$(".advancedToolsTab").each(function(){
			counter++;
			width+= (parseInt($(this).css("width").replace(/px/,""),10))+50;
		});
		$("#advancedToolsTabScroller").css("width", width+"px");

}

function savePermList(){
	var permissions='{';
	$('td[permSelected]').each(function(){
		permissions+='"'+$(this).html().trim()+'":"'+$(this).attr('permSelected')+'",';
	});
	permissions= permissions.substring(0,(permissions.length-1));
	permissions+="}";

	return permissions;
}

function doCreateAccount(form){

	$.ajax({
		url: "/manage/misc/AdvancedToolsFunctions/create_account.php",
		type: "POST",
		data:form.serialize(),
		async:false,
		success: function (string){
			if(string!=='fail'){
				alert(string);
			}else
				alert("could not retrieve rows");
		}
	});
}
function set_background(tag, color) {
	$(tag).css({"background-color": color});
}
setTimeout("set_lls_th_width();", 300);
function new_message(){
	$('.message_title').val("");
	$('.message_content').val("");
	$(".message_type").val("");
	$(".message_action").val("save_edited_message");
	$(".message_id").val("");
	$(".message_username").val($("#loggedIn").attr("name"));
	$("#message_editor").show();
}
function save_message(form){
	form[0][5].value= encodeURIComponent(form[0][5].value);

	if (form[0].type.value == "7") encodeURIComponent(form[0][6].value);
	else form[0][6].value = encodeURIComponent("<div style='padding-left:5px; padding-right:5px;'>"+form[0][6].value+"</div>");
	
	var serial='';
	var string = '';
	if($('.limiters').css('display')=='block'){
		var checkedVals = $('.lim_mod:checkbox:checked').map(function() {
			return this.value;
		}).get();
		var checked=checkedVals.join("%2C");
		string= form.serialize();
		serial= string.split("limiting_module")[0]+"limiting_module="+checked;
	}else{
		string= form.serialize();
		serial= string.split("module_use")[0];
	}
	console.log(serial);
	$.ajax({
		url: "/manage/misc/AdvancedToolsFunctions/messages.php",
		type: "POST",
		data:serial,
		async:false,
		success: function (string){
			if(string=='success'){
				alert('Message saved!');
				form[0][5].value= decodeURIComponent(form[0][5].value);
				form[0][6].value= decodeURIComponent(form[0][6].value);
			}else
				alert(string);
		}
	});
}
function edit_message(id){
	$.ajax({
		url: "/manage/misc/AdvancedToolsFunctions/messages.php",
		type: "POST",
		data:"function=edit_message&id="+id,
		async:false,
		success: function (string){
			if(string!=='fail'){
				//console.log(string);
				
				var obj=JSON.parse(string);
				
				var lim_mod= '';
				if(obj[0].limiting_module.length)
					lim_mod= JSON.parse(obj[0].limiting_module);
				else{
					lim_mod=JSON.parse('{"module_use":"", "mod_combiner":""}');
				}
				$('.message_title').   val(obj[0].title);
				$(".message_content").initialContent= decodeURIComponent(obj[0].content);
				$('.message_content')[0]. innerHTML =decodeURIComponent(obj[0].content);
				if (obj[0].type != "7") $('.message_content').wysiwyg(); // don't need rich text editor for front end alerts
				$(".message_type").    val(obj[0].type);
				$(".message_action").  val("save_edited_message");
				$(".message_id").      val(id);
				$(".limiting_access_level").val(obj[0].min_access_level);
				$(".limiting_query").  val(obj[0].limiting_query);
				$(".message_username").val(Lavu.currentUser._username);
				$('input[name=module_use][value='+lim_mod.module_use+']').attr('checked', 'checked');
				$('input[name=mod_combiner][value='+lim_mod.mod_combiner+']').attr('checked', 'checked');
				$('.emergency_end_date'). val(decodeURIComponent(obj[0].removal_date));
				if(lim_mod.limiting_module){
					for( var i=0; i<lim_mod.limiting_module.length; i++){
						$('input[name=limiting_module][value="'+lim_mod.limiting_module[i]+'"]').attr('checked', 'checked');
					}
				}
				$("#message_editor").  show();

			}else
				alert("could not retrieve rows");
		}
	});
}
function delete_message(id){
	$.ajax({
		url: "/manage/misc/AdvancedToolsFunctions/messages.php",
		type: "POST",
		data:"function=delete_message&id="+id,
		async:false,
		success: function (string){
			if(string!=='fail'){
				$("#backendMessagesTab").click();

			}else
				alert("could not retrieve rows");
		}
	});
}

if (!String.prototype.trim) {
	String.prototype.trim = function() {
		return this.replace(/^\s+|\s+$/g,'');
	};
}

function get_dates(form){
	var serialized= form.serialize();
	$.ajax({
		url: "/manage/misc/AdvancedToolsFunctions/get_report_usage.php",
		type: "POST",
		data:serialized,
		async:false,
		success: function (string){
			if(string!=='fail'){
				$(".report_usage_table").nextAll().remove();
				$(".report_usage_table").after(string);

			}else
				alert("could not retrieve rows");
		}
	});
}

/**
* This function is used to call Active customer info for all customers/selected customers.
*
* @param string showAll
*
* @return string show Active customer info.
*/
function getSelectedActiveCustInfo(showAll = '') {
	var searchData = $('#searchInput').val();
	$('#iteration').val(0);
	if(showAll != '') {
		$('#searchInput').val('');
		searchData = '';
	}

    $('#loadStatus').removeClass('displayNone');
    document.getElementById("loading").style.display='block';
	$.ajax({
		url: '/manage/misc/AdvancedToolsFunctions/getPage.php',
		type: 'POST',
		data: {'action': 'search', 'searchData':searchData},
		async:true,
		success: function (string) {
			document.getElementById("loading").style.display='none';
			$('#loadStatus').addClass('displayNone');
			$('#activeCustomerData').html(string);
		}
	});
}

/**
* This function is used to search data when trigger enter in text box.
*/
function searchKeyPress(e) {
    e = e || window.event;
    if (e.keyCode == 13) {
		getSelectedActiveCustInfo();
        return false;
    }
    return true;
}

/**
* This function is used to export Active customer data in CSV format.
*
* @return csv show Active customer info.
*/
function getDataToExport() {
	const searchData = $('#searchInput').val();
	pageUrl = '/manage/misc/AdvancedToolsFunctions/getPage.php?exportData=exportCSV&searchData='+searchData;
	window.location = pageUrl;
}

/**
* This function is used to display load image.
*/
function loading() {
	document.getElementById("loading").style.display='block';
	document.getElementById("loadingText").style.display='block';
}

/**
* This function is used to hide load image.
*/
function doneLoading() {
	document.getElementById("loading").style.display='none';
	document.getElementById("loadingText").style.display='none';
}

/**
* This funcction is used for lazy load.
*/
function loadActiveCustomerInfo(customerCount) {
	const limit = 100;
	if (customerCount < limit) {
		return false;
	}
	loading();
	const searchData = $('#searchInput').val();
	const iteration = Number($('#iteration').val()) + 1;
	$('#iteration').val(iteration);
	$.ajax({
		url: '/manage/misc/AdvancedToolsFunctions/getPage.php',
		type: 'POST',
		data: {'action': 'lazyLoad', 'searchData':searchData, 'iteration':iteration, 'limit':limit},
		async: true,
		success: function (string) {
			$("div #activeCustomerData #table-body").append(string);
			doneLoading();
		}
	});
}