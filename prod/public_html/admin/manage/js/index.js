
	(function ($) {	//sticky header
		$.fn.scrollbarTable = function (i) {
			var o = {};
			if (typeof (i) == 'number') o.height = i;
			else if (typeof (i) == 'object') o = i;
			else if (typeof (i) == 'undefined') o = {
				height: 400
			};
			return this.each(function () {
				var $t = $(this);
				var w = $t.width();
				$t.width(w - function (width) {
					var parent, child;
					if (width === undefined) {
						parent = $('<div style="width:50px;height:50px;overflow:auto"><div style="height:50px;"></div></div>').appendTo('body');
						child = parent.children();
						width = child.innerWidth() - child.height(99).innerWidth();

						parent.remove();
					}
					// alert(width);
					return width;
				}());

				var cols = [];
				var tableCols = [];
				$t.find('thead th,thead td').each(function () {
					cols.push($(this).width());
				});
				$t.find('tr:eq(1) th,thead td').each(function () {
					tableCols.push($(this).width());
				});
				var $firstRow = $t.clone();
				$firstRow.find('tbody').remove();
				$t.find('thead').remove();
				$t.before($firstRow);
				$firstRow.find('thead th,thead td').each(function (i) {
					$(this).attr('width', cols[i]);
				});
				$t.find('tr:first th,tr:first td').each(function (i) {
					$(this).attr('width', tableCols[i]);
				});
				var $wrap = $('<div>');

				$wrap.css({
					width: w,
					height: o.height,
					overflow: 'auto'

				});
				$t.wrap($wrap);
				//$("table.allUserData").first().css("z-index","2");
			});
		};
	}(jQuery));


	$(function() {
		window.onpopstate= function(){
			if(window.history.state)
		performPopState(window.history.state);

		};

		$( "#tabs" ).tabs();
		$( "#useDate" ).datepicker();
		$( "#useDate" ).datepicker({dateFormat: "yyyy-mm-dd", showOn: ''});

		/*
		setInterval(function(){
			if($(".deleteCheckBox").css('display')=='none' ){
				getChatsFromDB($("input[name=group3]:checked").val(), $("#loggedIn").attr("name"));
				updateLoggedInUsers();
			}
			//getChats($("input[name=group3]:checked").val());
		},5000);
		setInterval(function(){
			if( document.getElementsByName("newMessage").length >0){
			//alert("whooaa"+document.getElementsByName("newMessage"));
				if( document.title==='New Chat!')
					document.title= 'ua_cp';
				else
					document.title= 'New Chat!';
			}
			else
				document.title='ua_cp';
		}, 2000);
		*/
		/*
		$("input[name=group3]").change(function () {
			//alert($("input[name=group3]:checked").val());
			getChatsFromDB($("input[name=group3]:checked").val(), $("#loggedIn").attr("name"));

			//getChats($("input[name=group3]:checked").val());
		});
		*/
		//timer((15*60)*1000, 10, 5);

		$( '#messageBox' ).dialog({
			autoOpen: false,
			show: 'blind',
			hide: 'explode',
			buttons: {
					'Ok': function() {
						$( this ).dialog( 'close' );
					}
			}
		});

		//$(window).resize( function() { resize_advanced_tools(); });
		setTimeout(function() { resize_advanced_tools(); }, 500);

	});

	function resize_advanced_tools(content, parent) {
		if (typeof(content) == 'undefined') content = 'advancedToolsContent';
		if (typeof(parent) == 'undefined') parent = 'advancedToolsHeader';

		var parentWidth = $("#"+parent).width();
		var paddingLeft = parseInt($("#"+content).css("padding-left"), 10);
		var paddingRight = parseInt($("#"+content).css("padding-right"), 10);

		//$("#"+content).width(parentWidth - paddingLeft - paddingRight);
	}

	function lls_sort(object) {
		var jheader = $(object);
		if (jheader.parent().prop("id") == "lls_list_th_reference_for_widths")
			return;
		var index = 0;
		var a_headers = jheader.parent().children();
		for (var i = 0; i < a_headers.length; i++) {
			if (a_headers[i] == jheader.get(0)) {
				index = i;
				break;
			}
		}
		sort_table_by_header($("#lls_list_th_reference_for_widths").children()[index]);
	}

	function lls_adjust_table_widths() {
		var a_hidden_headers = $("#lls_list_th_reference_for_widths").children();
		var a_seen_headers = $("#lls_list_th_row").parent().children();
		for (var i = 0; i < a_hidden_headers.length; i++) {
			$(a_seen_headers[i]).width($(a_hidden_headers[i]).width());
		}
	}

	function auto_table_adjust_table_widths(i_table_index) {
		var a_hidden_headers = $("#auto_table_hidden_"+i_table_index).find("th");
		var a_seen_headers = $("#auto_table_static_"+i_table_index).find("th");
		for (var i = 0; i < a_hidden_headers.length; i++) {
			$(a_seen_headers[i]).width($(a_hidden_headers[i]).width());
		}
	}

	function sort_table_by_header(header) {
		var jheader = $(header);
		//lavuLog(jheader);
		var jheader_parent = jheader.parent().parent();
		var a_all_rows = jheader_parent.children();
		var jheader_row = $(a_all_rows[0]);
		var a_rows = [];
		var i = 0;
		for(i = 1; i < a_all_rows.length; i++)
			a_rows.push(a_all_rows[i]);
		var a_headers = jheader_row.children();
		var column_index = 0;
		for(i = 0; i < a_headers.length; i++) {
			if ($(a_headers[i]).html() == jheader.html()) {
				column_index = i;
				break;
			}
		}
		// assign ascending/descending
		var ascending = jheader.hasClass("sort_asc");
		a_headers = jheader.siblings();
		a_headers.push(jheader.get(0));
		for(i = 0; i < a_headers.length; i++) {
			a_headers.removeClass("sort_asc");
			a_headers.removeClass("sort_desc");
		}
		if (ascending)
			jheader.addClass("sort_desc");
		else
			jheader.addClass("sort_asc");
		ascending = jheader.hasClass("sort_asc");
		// sort
		var rows_sorted = a_rows.sort(function(a,b) {
			var a_col = $($(a).children()[column_index]).html().toLowerCase();
			var b_col = $($(b).children()[column_index]).html().toLowerCase();
			if (jQuery.isNumeric(a_col) && jQuery.isNumeric(b_col)) {
				a_col = parseInt(a_col, 10);
				b_col = parseInt(b_col, 10);
			}
			if (ascending)
				return a_col > b_col ? 1 : -1;
			else
				return a_col > b_col ? -1 : 1;
		});
		// preserve old content
		var header_row = jheader_row.get(0);
		for(i = 0; i < rows_sorted.length; i++) {
			rows_sorted[i] = $(rows_sorted[i]).get(0);
		}
		// remove old content
		for(i = 0; i < a_all_rows.length; i++) {
			$(a_all_rows[i]).remove();
		}
		// add new content
		jheader_parent.append(header_row);
		for(i = 0; i < rows_sorted.length; i++) {
			jheader_parent.append(rows_sorted[i]);
		}
	}

	function performPopState(state){
		var stateArray = state.split("::");
		if(stateArray[0]=='search')
			performSearch(stateArray[1]);
		else if(stateArray[0]=='showAccount')
			populateResults(stateArray[1]);
		else if(stateArray[0]=='goToAccount'){
			goToAccount(stateArray[1]);
		}else if(stateArray[0]=='button'){
			$("#"+stateArray[1]).click();
		}
	}

	modal = false;
	function timer(time, tries, triesLeft){
		//check if logged in
		usrname='';
		if( $("#loggedIn").attr("name") ==='')
				window.location = '/manage/index.php?mode=logout';
		else
			username=$("#loggedIn").attr("name");
		$.post("misc/ChatFunctions/checkIn.php", {uname:username}, function(data){
			isLoggedIn =  eval('('+data+')');

			//This code checks to see if you are still logged in to the page and not logged in somewhere else by
			// looking at the javascript of the page and checking if you still have a username. in this case we want to immediately redirect the user
			//and log them out.

			if( $("#loggedIn").attr("name") ==='')
				window.location = '/manage/index.php?mode=logout';

			if(modal){
				triesLeft = tries;
				modal = false;
			}

			if((!isLoggedIn || triesLeft<1)&(!modal)){
				window.location = '/manage/index.php?mode=logout';

			}else if(triesLeft==1){
			//alert(triesLeft);
				currentTime = new Date();

				month	= leadingZeroDate(currentTime.getMonth() + 1);
				day		= leadingZeroDate(currentTime.getDate());
				year	= leadingZeroDate(currentTime.getFullYear());

				hours	= leadingZeroDate(currentTime.getHours());
				minutes = leadingZeroDate(currentTime.getMinutes());
				seconds = leadingZeroDate(currentTime.getSeconds());

				dateString = '('+month+'/'+day+'/'+year+' - '+hours+':'+minutes+':'+seconds+')';

				minutes = time/60/1000;

				minutesString = '';
				if(minutes<1){
					minutesString = ' less than 1 minute';
				}else{
					minutesString = parseInt(minutes, 10)+' minutes';
				}

				$('#expiring').css('display', 'block');
				$('#expiringTime').html(dateString);
				$('#minutesLeft').html(minutesString);
			}

			//console.log(isLoggedIn);
			//console.log(triesLeft);
		});

		setTimeout(function(){
			timer(time, tries, triesLeft-1);

		}, time);

	}

	function leadingZeroDate(string){
		return ('0'+string).slice(-2);
	}
	function updateLoggedInUsers(){
		var username= getUsername();
		var urlString= "username="+username;

		$.ajax({
			url: "misc/ChatFunctions/checkIn.php",
			type: "POST",
			data:urlString,
			success: function (string){
				getCurrentlyLoggedInUsers();
			}
		});

	}
	function modalWindowButton(button){
		if(button=='OK'){
			modal = true;
		}else{
			modal = false;
		}

		$('#expiring').css('display', 'none');
	}

	function loading(){
		if ($(".currentButton").attr("id") != "company_search_button")
			return;
		$("#loading").css('height','100%');
		$("#loadng").css('width', '100%');
		$("#loading").css("display","block");
		$("#loadingText").css("display","block");
	}
	function doneLoading(){

		$("#loading").css("display","none");
		$("#loadingText").css("display","none");
	}
	function goToAccount(username, date){
		//alert(username);
		if(!date){
			var d = new Date();
			date = d.getFullYear();
		}

		do_button_click("#myAccountSection", "");
		var urlString= "username="+username+"&date="+date;
		//alert(username);
		if(username=='Nialls' && false){
			$.ajax({
				url: "misc/MyAccount/myAccount_new.php",
				type: "POST",
				data:urlString,
				async:false,
				success: function (string){

					$("#myAccountSection").html(string);
					if(getInternetExplorerVersion() ==-1){
						window.history.replaceState("goToAccount::"+username, 'ua_cp');
					}
				}
			});
		}else{
			$.ajax({
				url: "misc/MyAccount/myAccount.php",
				type: "POST",
				data:urlString,
				async:false,
				success: function (string){

					$("#myAccountSection").html(string);
					if(getInternetExplorerVersion() ==-1){
						window.history.replaceState("goToAccount::"+username, 'ua_cp');
					}
				}
			});
		}
	}

	/*
		Gets called when a user clicks on "request Local Server Info "
	*/
	function requestLLS(dataname, locationID){
		var urlString='data_name='+dataname+"&locid="+locationID;
		$.ajax({

			url: "misc/LLSFunctions/requestLLSInfo.php",
			type: "get",
			data:urlString,
			success: function (string){
				alert("requesting local server info, please check back to see if the msync completes. ");
				loadLLS(dataname);
			}

		});
	}

	/*
		gets called when a user clicks on a chat so that we can
		update the list of chats that a user has looked at.
	*/
	function hasLooked(timeStamp, username, audience){
		var urlString='timeStamp='+timeStamp+"&username="+username;
		//alert("clicked!");
		$.ajax({

			url: "misc/ChatFunctions/hasChecked.php",
			type: "get",
			data:urlString,
			success: function (string){
				//alert("success!");
				getChatsFromDB(audience, username);
				getCurrentlyLoggedInUsers();
				$("#chatID").removeClass("newMessage");
			}

		});
	}

	function getPreviousSearches(){
		var username= getUsername();
		var urlString= 'username='+username;
		$.ajax({

			url: "misc/SearchFunctions/getPreviousSearches.php",
			type: "POST",
			data:urlString,
			async:false,
			success: function (string){
				var previousSearches= JSON.parse(string);
				var returnString='';

				for(var i=0; i<previousSearches.length; i++){
					if(previousSearches[i].dataname.length > 0) {
						var oncontextmenu = (Lavu.currentUser.getOptions() && Lavu.currentUser.getOptions().quickaccount_buttons && Lavu.currentUser.getOptions().quickaccount_buttons.length > 0) ? "oncontextmenu=\"showQuickaccountLinks(this, "+previousSearches[i].restaurant_id+", '"+previousSearches[i].dataname+"', 20);return false;\"" : "";
						returnString+="<td class='previousSearchesLinks' onclick=\"populateResults('"+previousSearches[i].dataname+"');rotateSearches('"+previousSearches[i].dataname+"');\" "+oncontextmenu+" onmouseout=\"hideQuickaccountLinks(10);\" onmouseover=\"clearTimeout(window.quickaccountLinksHideTimer); showQuickaccountLinksCheckOpen(this, "+previousSearches[i].restaurant_id+", '"+previousSearches[i].dataname+"');\"><div>"+previousSearches[i].dataname+"</div></td>";
					}
				}

				$('.previousSearches').html(returnString);
			}
		});
	}

	function showQuickaccountLinksCheckOpen(element, restaurantid, dataname) {
		if (window.quickaccountLinksDataname && window.quickaccountLinksDataname != dataname && window.quickaccountLinksDataname !== "")
			showQuickaccountLinks(element, restaurantid, dataname, 0);
	}

	function showQuickaccountLinks(element, restaurantid, dataname, timeout) {
		if (window.quickaccountLinksDataname == dataname)
			return;

		window.quickaccountLinksTimer = setTimeout(function() {
			hideQuickaccountLinks(0);
			setTimeout(function() {
				var jelement = $(element);
				var append = "";
				var userOptions = (Lavu.currentUser.getOptions() && Lavu.currentUser.getOptions().quickaccount_buttons) ? Lavu.currentUser.getOptions().quickaccount_buttons.split(',') : [];
				var width = userOptions.length*110+10;
				var left = Math.min(jelement.offset().left, $(window).width()-width);
				var top = jelement.offset().top+parseInt(jelement.height(), 10)+15;
				append += "<div id='quickaccountLinksDiv' style='left:"+left+"px; top:"+top+"px; width:"+width+"px;' onmouseover='clearTimeout(window.quickaccountLinksHideTimer);' oncontextmenu=''>";
				append += "	<div style='height:20px; width:20px; margin:0 0 0 20px; transform:rotate(45deg); -ms-transform:rotate(45deg); -webkit-transform:rotate(45deg); background-color:lightgray;'></div>";
				append += "	<div style='height:40px; width:"+width+"px; margin:0 auto; box-sizing:border-box; -moz-box-sizing:border-box; padding:10px; position:relative; top:-12px; border-top:0px solid transparent; background-color:lightgray;'>";
				$.each(userOptions, function(k,v) {
					if (v.indexOf("tab_") === 0 && v.substr(4)) {
						var name = v.substr(4);
						append += "		<a href='index.php#"+name+"' style='border:0px solid transparent;'><input type='button' style='width:100px; height:20px; text-align:center;' value='"+name+"' onclick='populateResults(\""+dataname+"\", null, \""+name+"\"); rotateSearches(\""+dataname+"\"); event.preventDefault(); return false;' /></a>";
					} else if (v == 'dev') {
						append += "		<input type='button' style='width:100px; height:20px; text-align:center;' value='Open Dev' onclick='window.open(\"misc/MainFunctions/custLogin.php?site=new&mode=login&custID="+restaurantid+"&custCode="+dataname+"\", \"_blank\");  window.focus(); event.preventDefault(); return false;' />";
					} else if (v == 'live') {
						append += "		<input type='button' style='width:100px; height:20px; text-align:center;' value='Open Live' onclick='window.open(\"misc/MainFunctions/custLogin.php?site=old&mode=login&custID="+restaurantid+"&custCode="+dataname+"\", \"_blank\");  window.focus(); event.preventDefault(); return false;' />";
					}
				});
				append += "	</div>";
				append += "</div>";
				jelement.append(append);
				$("#quickaccountLinksDiv").show(150);
				window.quickaccountLinksDataname = dataname;
			}, 10);
		}, timeout);
	}

	function hideQuickaccountLinks(timeout) {
		var dataname = window.quickaccountLinksDataname;

		clearTimeout(window.quickaccountLinksHideTimer);
		window.quickaccountLinksHideTimer = setTimeout(function() {
			if (window.quickaccountLinksDataname !== dataname)
				return;
			clearTimeout(window.quickaccountLinksTimer);
			$("#quickaccountLinksDiv").stop(true,true);
			$("#quickaccountLinksDiv").remove();
			window.quickaccountLinksDataname = "";
		}, timeout);
	}

	function rotateSearches(dataname) {
		var username= getUsername();
		$.ajax({
			url: "misc/SearchFunctions/rotatePreviousSearches.php",
			type: "POST",
			data: { 'dataname': dataname, 'username': username },
			async: true,
			success: function(message) {
				getPreviousSearches();
			},
		});
	}

	function setComponentPackage( package, restid, rand ) {
		var urlString='rest_id=' + restid + '&comp_pack=' + package + '&rand=' + encodeURIComponent( rand );
		alert(urlString);
		$.ajax({

			url: "misc/MainFunctions/setComponentPackage.php",
			type: "post",
			data: urlString,
			success: function (string){
				alert( string );
			}

		});
	}

	function saveUserInfo(chainname, firstname, lastname,email,phone, address, distro, dataname){
		var urlString='chainname='+chainname+'&firstname='+firstname+'&lastname='+lastname+'&email='+email+'&phone='+phone+'&address='+address+'&distro='+distro+'&dataname='+dataname;
		alert(urlString);
		$.ajax({

			url: "misc/MainFunctions/saveUserInfo.php",
			type: "post",
			data:urlString,
			success: function (string){
				doRefresh("Main",dataname);
			}

		});
	}
	selected=false;


	$(function() {
		$.ui.autocomplete.prototype._renderItem = function( ul, item){
			var term = this.term.split(' ').join('|');
			var re = new RegExp("(" + term + ")", "gi") ;
			var t = item.label.replace(re,"<b>$1</b>");
			return $( "<li></li>" )
			.data( "item.autocomplete", item )
				.append( "<a>" + t + "</a>" )
				.appendTo( ul );
			};
			$("#searchInput").autocomplete({
			source:  "misc/SearchFunctions/search.php",
			minLength: 2,
			async:false,
			focus: function( event, ui) {
				var val = 0;
				if(typeof(ui.item.value) !=='undefined')
					val= ui.item.value.split(" (")[1].split(")")[0];
				else
					return;
				//lavuLog( val);
				$.ajax({
					url: "misc/SearchFunctions/searchTempResult.php",
					data:{ action:val},
					type: "post",
					success: function (string){
						$( "#tabs").css("display", "none");
						$("#tableHeader").css("display","");
						$("#results").css("display","block");
						$(".floatingTempResult").html("");

						if(document.getElementById("tempResult")){
							$(".rowResult").replaceWith(string);
							$(".floatingTempResult").css("display", "");

						}else{
							$(".floatingTempResult").css("display", "block");
							$(".floatingTempResult").html(string);
						}
					}
				});
			},
			select: function(event, ui){
				$("#results").css("display","none");
				$(".floatingTempResult").css("display", "none");
				event.keyCode= 0;
				$(".rowResult").css("display", "");
				var val;

				if( typeof $("#searchInput").val() === "undefined")
					val= $("#searchInput").val().split(" (")[1].split(")")[0];
				else
					val= $(".floatingTempResult").find('.cCode').html();

				selected=true;
				populateResults(val);

			}

		});
	});

	$(document).ready(function(){

		getPreviousSearches();

		if(window.mozInnerScreenX !== null) //basically, if is firefox
			if( window.history.state)
				performPopState(window.history.state);

		$('table.resultTable').scrollbarTable();// Sticky headers
				//var table= $('.resultTable');
		var table= $('.resultTable').last();

		$('#company_code, #company_name, #disabled, #last_activity,#created, #license_status, #package_status, #email, #phone, #firstname, #lastname, #city, #state, #zip, #chain_name').each(function(){

			var th = $(this),
				thIndex = th.index(),
				inverse = false;

			th.click(function(){
				$('.searchColsSelected').removeClass('searchColsSelected');
				th.addClass('searchColsSelected');
				table.find('td').filter(function(){

					return $(this).index() === thIndex;

				}).sortElements(function(a, b){

					return $.text([a]) > $.text([b]) ?
						inverse ? -1 : 1
						: inverse ? 1 : -1;

				}, function(){

					// parentNode is the element we want to move
					return this.parentNode;

				});

				inverse = !inverse;
			});
		});

		//easter egg :)
		var konamiCode=[38,38,40,40,37,39,37,39,66,65];
		var counter=0;
		$(document).bind("keyup",function(e){

			if(counter==2 && e.keyCode==38){}

			else if(konamiCode[counter]==e.keyCode)
				counter++;
			else
				counter=0;

			if(counter==konamiCode.length){

				//$("#searchTable").after("<img id='ryu' style='position:relative;left:10px; top:103px'src='/manage/images/streetfighter.gif'>");

				$("#c").css("display",'block');
				/*
				setTimeout(function(){
					$("#ryu").remove();
				}, 12500);
				*/
			}




		});




		$("#searchInput").focus();
		$(".floatingTempResult").focusout(function(){
			$(".floatingTempResult").css("display", "none");
			$(".ui-autocomplete").css("display", "none");
		});
		$("#searchInput").mousedown(function(){
			$('#searchInput').autocomplete('enable');
		});

		$("#searchInput").bind("keyup",function(e){
			if(e.keyCode==13){


				if( $("#searchInput").val().length > 1){
					var urlString= getSearchURLString();
				if(getInternetExplorerVersion() ==-1){
					window.history.replaceState("search::"+urlString, "index.php");
				}
				performSearch(urlString);
				if(!selected)
					$("#results").css("display","block");
				$("#searchInput").autocomplete("disable");
				$(".floatingTempResult").css("display", "none");
				$(".ui-autocomplete").css("display", "none");

				}else{
					//alert('search must be at least 2 characters long');
				}

			}else if(e.keyCode==8){

				if(getInternetExplorerVersion() ==-1)
					window.history.replaceState("clear::",'index.php');

				$('.searchColsSelected').removeClass('searchColsSelected');
				$("#numResultsTable").css("display", "none");
				$(".floatingTempResult").css("display", "none");
				$(".rowResult").css("display","none");
				$("#tabs").css("display","none");
				$("#tableHeader").css("display","");
				$(".resultHolder").css("display","");
				$("#result").css("display","");
				$("#searchInput").autocomplete("enable");
			}

		});

		$(".advancedSearch").bind("keyup",function(e){
			if(e.keyCode==13){
				var urlString= getSearchURLString();
				if(getInternetExplorerVersion() ==-1)
					window.history.replaceState("search::"+urlString, 'index.php');
				performAdvancedSearch(urlString);
			}

		});
		$(".radioChange").bind("click",function(e){
			//alert("whooaa");
			var urlString= getSearchURLString();
			if(getInternetExplorerVersion() ==-1)
					window.history.replaceState("search::"+urlString,'index.php');
			performAdvancedSearch(urlString);
		});


		/**
			This controls the buttons to switch pages.
		**/
		//$("#distributor_search_button").button();
		//$("#company_search_button").	button();
		//$("#company_docs_button").		button();
		//$("#advanced_tools").			button();
		//$("#bug_tracker_button").		button();

		$("#company_search_button").addClass('currentButton');
		$("#company_docs_button").click(function(){
			var contents=performAJAX("/manage/tabs/companyDocuments.php", "",false,"POST");
			do_button_click("#companyDocs", "#company_docs_button");
			$("#companyDocs").html(contents);
		});
		$("#company_search_button").click(function(){

			if($("#main").html())
				do_button_click("#searchTable, #tabs, .center", "#company_search_button");
			else
				do_button_click("#results, .center,#searchTable", "#company_search_button");

		});

		$("#distributor_search_button").click(function(){

			do_button_click("#distroSearchSection", "#distributor_search_button");
			$("#distroSearchSection").html(performAJAX("tabs/Distro_Search.php", "",false,"POST" ));

		});
		$("#advanced_tools").click(function(){

			do_button_click("#advancedToolsSection", "#advanced_tools");

			var urlString="fromManage=1&username="+getUsername();
			loading();
			$.ajax({
				url: "./tabs/AdvancedTools.php",
				type: "POST",
				data:urlString,
				async:false,
				success: function (string){
					$("#advancedToolsSection").html(string);
					$(".advancedToolsTabSelected").click();

					//resize_advanced_tools();
					doneLoading();
				}
			});
		});
		$("#dev_tools").click(function(){

			do_button_click("#dev_tools_section", "#dev_tools");

			var urlString="fromManage=1&username="+getUsername();
			loading();
			$.ajax({
				url: "./tabs/Developer_tools.php",
				type: "POST",
				data:urlString,
				async:false,
				success: function (string){
					$("#dev_tools_section").html(string);
					//$(".advancedToolsTabSelected").click();
					doneLoading();
				}
			});
		});
		$("#support_tools").click(function(){

			do_button_click("#support_tools_section", "#support_tools");

			var urlString="fromManage=1&username="+getUsername();
			loading();
			$.ajax({
				url: "./tabs/Support_tools.php",
				type: "POST",
				data:urlString,
				async:false,
				success: function (string){
					$("#support_tools_section").html(string);
					//$(".advancedToolsTabSelected").click();
					doneLoading();
				}
			});
		});
	});

	function do_button_click(show_area, btn_id){
		if(btn_id) {
			$('nav#page_button_container > div[selected]').removeAttr('selected');
			$(btn_id)[0].parentNode.setAttribute('selected','');
		}


		var btn_without_hash= btn_id.split("#");
		if(getInternetExplorerVersion() ==-1)
				window.history.replaceState("button::"+btn_without_hash[1] ,'index.php');

		$('.currentButton').		removeClass('currentButton');
		$(btn_id).					addClass('currentButton');
		$("#searchTable").			css('display','none');
		$("#results").				css('display','none');
		$(".center").				css('display','none');
		$("#myAccountSection").		css('display','none');
		$("#tabs").					css('display','none');
		$("#advancedToolsSection").	css('display','none');
		$("#companyDocs").			css('display','none');
		$("#distroSearchSection").	css('display','none');
		$("#dev_tools_section").	css('display','none');
		$("#support_tools_section").css('display','none');
		$(show_area).				css('display','block');
		if(show_area.indexOf('#searchTable') >= 0 ){
			$('#searchTable').css('display','inline-block');
		}
	}

	function resultSection(){
		$( '#tabs' ).tabs();
		$( '#tabs').css('display', 'none');
	}
	function performAdvancedSearch(urlString){
		loading();
		setTimeout(function(){
		$.ajax({
				url: "misc/SearchFunctions/searchMultiResult.php",
				type: "get",
				data:urlString,
				async:false,
				success: function (string){
					doneLoading();
					$('.rowResult').remove();
					$("#tableHeader").after(string);
					$(".floatingTempResult").css("display", "none");
					$(".ui-autocomplete").css("display", "none");
				}
		});
	},10);

	}

	function performSearch(urlString){
		loading();
		setTimeout(function(){
			$.ajax({

				url: "misc/SearchFunctions/searchMultiResult.php",
				type: "get",
				data:urlString,
				async: false,
				success: function (string){
					doneLoading();

					$('.rowResult').remove();
					//resizeHeader();

					$('.resultTable').find('tbody').html(string);

					$(".floatingTempResult").css("display", "none");

					if($('#tabs').css('display')==='block' && !selected){
						$('#tabs').css('display','none');
						$("#results").css("display","block");
						$(".ui-autocomplete").css("display", "none");
					}
					else
						selected=false;
				}
			});
		},10);
	}
	function resizeHeader(){

		var header= "<table id='stickyHeader'><tr>";

					var maiHtml='';
					$(".resultTable > thead ").children().children().each(
						function(){
							$(this).css('width', $(this).width());
							maiHtml=$(this).parent().html();

						}
					);

					header+=maiHtml;
					header+= "</tr></table>";
					if($("#stickyHeader").length)
						$("#stickyHeader").replaceWith(header);

					else
						$("#numResultsTable").after(header);
						$(".center").css("height", $(window).height()-$(".center").offset().top );
						//$("#results").css("height", ($(window).height()-$("#results").offset().top)-20);
	}

	displayLargeCompanyName_previousDn = '';
	function displayLargeCompanyName(dataname, dn_cn) {
		if (!dataname)
			dataname = displayLargeCompanyName_previousDn;
		displayLargeCompanyName_previousDn = dataname;

		if (typeof(dn_cn) == 'undefined') {
			displayLargeCompanyName(dataname, 'dn');
			displayLargeCompanyName(dataname, 'cn');
			return;
		}

		var display_company_name_div = $("#display_company_name");
		if (display_company_name_div.length > 0) {
			var jdisplay = '';
			if (dn_cn == 'dn')
				jdisplay = display_company_name_div.find(".data_name");
			else
				jdisplay = display_company_name_div.find(".company_name");
			$.ajax({
				cache: false,
				async: true,
				url: '/manage/misc/MainFunctions/getDisplayCompanyName.php',
				type: 'post',
				data: { dn: dataname, val: dn_cn },
				success: function(message) {
					jdisplay.html('');
					jdisplay.append(message);
					adjustLargeCompanyNameWidth();
				},
				error: function(a,b,c) {
					jdisplay.html('');
					jdisplay.append(dataname);
				},
			});
		}
	}

	// given a way for jquery to select an element, selects the contents of that element
	function SelectText(element) {
		var doc = document, text = $(element)[0], range, selection;
		if (doc.body.createTextRange) {
			range = document.body.createTextRange();
			range.moveToElementText(text);
			range.select();
		} else if (window.getSelection) {
			selection = window.getSelection();
			range = document.createRange();
			range.selectNodeContents(text);
			selection.removeAllRanges();
			selection.addRange(range);
			if ($(text).is("input"))
				$(text).select();
		}
	}

	function adjustLargeCompanyNameWidth() {
		/*var dcn = $('#display_company_name');
		var w = 0;
		dcn.css({ position:'absolute', width:'' });
		$.each(dcn.children(), function(k,v){
			w += parseInt($(v).width());
		});
		dcn.width(w+8);*/
	}

	function populateResults(dataname, postFunction, loadTab){

		loading();
		setTimeout(function(){  //This is so that loading actually comes up :P ha ..... GAAAYYYYYY (gayseal.jpg)

			if(getInternetExplorerVersion() ==-1)
				window.history.replaceState("showAccount::"+dataname, 'ua_cp');

			displayLargeCompanyName(dataname);

			$("#numResultsTable").css("display", "none");
			$("#results").css("display","none");
			$(".floatingTempResult").css("display", "none");

			$( "#tabs").css("display", "block");
			$("#dataname").val(dataname);
			var username = getUsername();
			if(username === '' || username === false){
				alert("you are not logged in. Please login to perform searches. ");
				window.location.replace('http://admin.poslavu.com/manage/index.php?mode=logout');
			}

			addToPreviousSearches(dataname, username);

			// find the tab to load (Main, LLS, Billing, etc...)
			var currentTab = "loadundefined";
			if (loadTab)
				currentTab = "load"+loadTab;
			else
				currentTab= "load"+getCurrentTab();

			// load the tab
			if(currentTab && currentTab !== 'loadundefined') {
				currentTab=currentTab+"('"+dataname+"')";
				eval(currentTab);
			}else {
				loadMain(dataname);
			}

			if (postFunction !== null && typeof(postFunction) !== 'undefined')
				postFunction();
		},10);
	}

	function getCurrentTab(){
		if (window.lastCompanySearchTabID)
			return window.lastCompanySearchTabID;
		return $('.ui-state-active').children().html();
	}

	/**This function records a list of previous searches. it maintains 5 previous searches. **/

	function addToPreviousSearches(dataname, username){
		var completed=false;
		var urlString="dataname="+dataname+"&username="+username;
		$.ajax({
			url: "./misc/SearchFunctions/writePreviousSearches.php",
			type: "post",
			data:urlString,
			async:false,
			success: function (string){
					getPreviousSearches();
					completed=true;
				}
		});
		return completed;
	}


	function savePost(post, name, priority, audience){
		var urlString= "post="+post+"&name="+name+"&priority="+priority+"&audience="+audience;
		if(post ===''){
			alert("Please enter a message.");
			return;
		}
		if( name===''){
			alert("You do not have a username, this means your session variable was destroyed. Please logout, login and try again. ");
			return;
		}

		$("#chatSubmitter").attr("disabled","disabled");
		$.ajax({
			url: "./misc/ChatFunctions/writePostToDB.php",
			type: "post",
			data:urlString,
			async:false,
			success: function (string){

					$("#newPost").val("");
					getChatsFromDB(audience, name);
					getCurrentlyLoggedInUsers();
					$("#chatSubmitter").removeAttr("disabled");
				}
		});
	}

	function getChatsFromDB(audience, username){
		var urlString= "audience="+audience+"&username="+username;
		$.ajax({
			url: "./misc/ChatFunctions/getChatsFromDB.php",
			type: "post",
			async:false,
			data:urlString,
			success: function (string){
					$("#chatID").html(string);
				}
		});
	}

	function getUsername(){
		return Lavu.currentUser.getUsername();
	}
	function enableOrDisableAccount(id, enableOrDisable, dataname){
		var details = prompt('Please enter a reason for '+enableOrDisable+'ing this account.');
		if (details && details !== '') {
			$.ajax({
				async:false,
				url: "./misc/MainFunctions/disableAccount.php",
				type: "post",
				data: { enableOrDisable: enableOrDisable, id: id, details: details },
				success: function (string){
						populateResults(dataname);
					}
			});
		}
	}
	function doRefresh(page, dataname){
		if(page=='LLS'){
			loadLLS(dataname);
		}
		else {
			populateResults(dataname);
		}

	}
	function saveNote(name, id, note, ticketID, dataname){
		var urlString="support_ninja="+name+"&ticket_id="+ticketID+"&notes_message="+note+"&id="+id;

		$.ajax({
			async:false,
			url: "./misc/MainFunctions/saveNote.php",
			type: "post",
			data:urlString,
			success: function (string){
					alert("Note Written");
					populateResults(dataname);
				}
		});
	}
	function saveLavuLiteNote(name, id, note, ticketID, dataname){
		var urlString="support_ninja="+name+"&ticket_id="+ticketID+"&notes_message="+note+"&id="+id;

		$.ajax({
			async:false,
			url: "http://admin.lavulite.com/manage/mainServerAccess/saveNote.php",
			type: "post",
			data:urlString,
			success: function (string){
					//alert(string);
					populateResults(dataname);

				}
		});
	}

	function getSearchURLString(){
		var choice= $("#searchInput").val();
		choice= choice.split("(")[0].trim();
		//alert(choice);
		var urlString= "choice="+choice;
		if(isNumber(choice)){
			if( $("#useID").prop('checked')){
				urlString+="&useID=true";
				choice = pad(choice);
				$("#searchInput").val(choice);
			}
		}
		//alert("urlSTring: "+urlString);
		if( $("#useAdvancedSearch").prop('checked')){
			if($("#emailCheck").prop('checked'))urlString+="&email="+$("#useEmail").val();
			if( $("#useCompanyCode").prop('checked'))urlString+="&compCode="+$("#useCcode").val();
			if( $("#useLastName").prop('checked'))urlString+="&lname="+$("#useLname").val();
			if( $("#useFirstName").prop('checked'))urlString+="&fname="+$("#useFname").val();
			if( $("#useStatus").prop('checked'))urlString+="&status="+$('input:radio[name=stat]:checked').val();
			if( $("#useEnabledorDisabled").prop('checked'))urlString+="&enabled="+$('input:radio[name=enabled]:checked').val();
			if( $("#useLevel").prop('checked'))urlString+="&level="+$('input:radio[name=level]:checked').val();
			if( $("#filterCity").prop('checked'))urlString+="&city="+$("#useCity").val();
			if( $("#filterState").prop('checked'))urlString+="&state="+$("#useState").val();
			if( $("#filterZip").prop('checked'))urlString+="&zip="+$("#useZip").val();
			if( $("#useLastActivity").prop('checked'))urlString+="&date="+$("#useDate").val();
			if( $("#useIPAddress").prop('checked'))urlString+="&useip=1";
		}
		//alert(urlString);
		return urlString;
	}
	function change_vid(new_vid){
		$.ajax({
			url: './misc/AdvancedToolsFunctions/messages.php',
			type: 'POST',
			data: "function=save_video&value="+new_vid,
			async: true,
			success: function(message) {
				alert("saved");
			},
			error: function() {
				doneLoading();
			},
		});
	}

	function loadGraphs(dataname) {
		loading();
		highlightTabWhenClicked('Graphs');

		$.ajax({
			url: './graphs/index.php',
			type: 'POST',
			data: { dataname: dataname },
			async: true,
			success: function(message) {
				$("#Graphs").html(message);
				load_company_search_tab("Graphs");
				doneLoading();
			},
			error: function() {
				doneLoading();
			},
		});
	}

	function load_company_search_tab(tabid) {
		window.lastCompanySearchTabID = tabid;
		var tabs = $(".company_search_tab");
		$.each(tabs, function(k,v) {
			$(v).hide();
		});
		$("#"+tabid).show();
	}

	function highlightTabWhenClicked(tabname) {
		var jas = $($("#tabs").find("ul")[0]).find("a[href^=#]");
		var jlis = {};
		$.each(jas, function(k,v){
			jlis[$(v).attr('href').substr(1)] = $(v).parent();
		});
		$.each(jlis, function(k,v) {
			if (k == tabname) {
				v.addClass('ui-tabs-active');
				v.addClass('ui-state-active');
			} else {
				v.removeClass('ui-tabs-active');
				v.removeClass('ui-state-active');
			}
		});
	}

	function loadMain(dataname){
		loading();
		var urlString="dataname="+dataname;

		var url="./tabs/Main.php";
		var isLite= checkIfLavuLite(dataname);
		highlightTabWhenClicked('Main');

		if(isLite==1)
			url= './tabs/Main_Lite.php';
		$.ajax({
			url: url,
			type: "get",
			data:urlString,
			async:true,
			success: function (string){

					if( string==='')
						string="Compnay Does Not Exist.  ";

					$("#Main").html(string);
					getTempAccountInfo("check_temp", 2268, dataname, getUsername());
					load_company_search_tab("Main");
					doneLoading();

				},
			error: function() {
				doneLoading();
			},
		});
	}
	function getReloadTime(MAC, dataname, ip){
		if(MAC=='N/A')
			MAC='';
		var urlString= "MAC="+MAC+"&dataname="+dataname;
		$.ajax({
			url: "./misc/MainFunctions/getReloadTime.php",
			type: "post",
			data:urlString,
			async:false,
			success: function (string){
					//alert(ip);
					document.getElementById(ip).innerHTML=string;
				}
		});
	}

	function loadBilling(dataname){

		loading();
		highlightTabWhenClicked('Billing');

		var result = '';

		$('#billingContent').attr('src','./tabs/Billing.php?dn='+dataname+'&origin=ua_cp');
		$('#billingContent').html(result);
		$('#billingContent').css('height','900px');
		load_company_search_tab("Billing");
		resize();

		//loadLogs(dataname);
	}
	function loadLogs(dataname){
		loading();
		var urlString="dataname="+dataname;
		highlightTabWhenClicked('Logs');

		if(checkIfLavuLite(dataname)==1)
			urlString+="&filename=Logs_Lite";
		else
			urlString+="&filename=Logs";

		$.ajax({
				url: "./misc/LogFunctions/getLogInfo.php",
				type: "get",
				data:urlString,
				async:true,
				success: function (string){
						$("#Logs").html(string);
						load_company_search_tab("Logs");
						doneLoading();
						//window.history.pushState($("#tabs").html(), 'ua_cp');
					}
		});



	}
	function resize(count, dont_show_loading){
		if (!count)
			count = 0;
		count = parseInt(count, 10);
		if (count === 0) {
			if (!dont_show_loading) loading();
			$("#billingContent").css("height","auto");
			setTimeout('resize(1);', 1000);
			setTimeout('resize(2);', 2000);
			setTimeout('resize(3);', 3000);
		}

		setTimeout(function(){
			var height='';
			var width='';
			if(getInternetExplorerVersion() ==-1){
				height = $("#billingContent").contents().height();
				//width  = $("#billingContent").contents().width();
				width = $("#Billing").width() - parseInt($("#Billing").css('padding-left'), 10)*2;
			}else{
				height= document.getElementById('billingContent').contentWindow.document.body.offsetHeight;
				width  ='900px';
				height+=60;
			}

			//alert(height +" and "+width);
			if( height===60 || width ===''){
				$("#billing").click();
				height = '900px';
				width  ='900px';
				//alert("in the if statement"+ height);
			}
			$("#billingContent").css("height",height);
			$("#billingContent").css("width",width);
			$("#Billing").css("height",height+50);
			if (count === 0 && !dont_show_loading)
				doneLoading();
		},1000);

	}

	window.check_refresh_time_timeout_counter = 0;
	function loadLLS(dataname){
		var urlString= "dataname="+dataname;
		highlightTabWhenClicked('LLS');

		$.ajax({
			url: "./misc/SearchFunctions/getRowID.php",
			type: "post",
			data:urlString,
			async:false,
			success: function (string){
					loading();
					var url="dataname="+dataname+"&rowid="+string;

					$.ajax({
						url: "./tabs/LLS.php",
						type: "post",
						data:url,
						async:true,
						success: function (string){

								$("#LLS").html(string);
								if($("div[id^='display_next_sync']").html()){
									var next_sync_time= parseInt($("div[id^='display_next_sync']").html().match(/<b>(.*?)<\/b>/)[1],10);
									window.check_refresh_time_timeout_counter++;
									check_refresh_time(next_sync_time, dataname, check_refresh_time_timeout_counter);
								}
								doneLoading();
							}
						});
				}
		});
		//loadBilling(dataname);
	}
	function check_refresh_time(next_sync_time, dataname, timeout_counter){
		//console.log(dataname);
		if (window.check_refresh_time_timeout) {
			clearTimeout(window.check_refresh_time_timeout);
		}
		if (window.check_refresh_time_timeout_counter != timeout_counter) {
			return;
		}

		window.check_refresh_time_timeout = setTimeout(function(){
			next_sync_time =  parseInt($("div[id^='display_next_sync']").html().match(/<b>(.*?)<\/b>/)[1],10);
			if( next_sync_time < 0) {
				doRefresh("LLS", dataname);
			}
			check_refresh_time(next_sync_time,dataname, timeout_counter);
			$("#display_next_sync").html("<b>"+(next_sync_time-1)+"</b>");
		}, 1000);
	}
	/*
	Lavu log is a super simple logging function and all it does is
	make sure that if you are on IE you do not get console.log outputs.
	The reason for this is be cause we found that if the console is not open
	in IE than it will freeze the browser if you try to write to the console. so
	with this in place we can output to any browser that isnt IE.
	-ncc 7/25/12

	*/

	function lavuLog(output){
		if( navigator.appName != 'Microsoft Internet Explorer')
			console.log(output);
		}

	function getCurrentlyLoggedInUsers(){
		var urlString= "";
		$.ajax({
			url: "./misc/ChatFunctions/usersLoggedIn.php",
			type: "post",
			data:urlString,
			success: function (string){
						$("#usersLoggedIn").html(string);
				}
		});

	}
	function isNumber(n) {
		return !isNaN(parseFloat(n)) && isFinite(n);
	}
	function pad(number) {
			var str = '' + number;
		while (str.length < 5) {
			str = '0' + str;
		}

		return str;
	}
	function extendBilling(id, numDays){
			var urlString= "id="+id+"&numDays="+numDays;
		$.ajax({
			url: "./misc/MainFunctions/extendBilling.php",
			type: "post",
			data:urlString,
			async:false,
			success: function (string){
					alert("billing extended for: "+numDays);
				}
		});
	}

	function checkIfLavuLite(dataname){

		var urlString='dataname='+dataname;
		var returnVal=0;
		$.ajax({
			url: "./misc/SearchFunctions/checkIfLavuLite.php",
			type: "post",
			data:urlString,
			async:false,
			success: function (string){
					returnVal= string;
				}
		});
		return returnVal;
	}
	function searchLogs(log_type, search_lls, date, loc_id, search_for, rowid, isLite){

		var urlString= "log_type="+log_type+"&search_lls="+search_lls+"&date="+date+"&loc_id="+1+"&search_for="+search_for+"&rowid="+rowid;
		var Maiurl="./misc/LogFunctions/search_logs.php";
		if(isLite == 1)
			Maiurl = "http://admin.lavulite.com/manage/mainServerAccess/getLogsFromSearch.php";

		//alert(Maiurl);

		$.ajax({
			url: Maiurl,
			type: "post",
			data:urlString,
			async:false,
			success: function (string){
					$("#Logs").html(string);
				}
		});
		//return returnVal;
	}

	/*
		Yes I realize that this function needs to be implemented! im sorry!
	*/
	function markAllAsViewed(){

	}

	function performSync(rowid, dataname){
		var urlString= "rowid="+rowid;
		var Maiurl="./misc/MainFunctions/syncDB.php";

		$.ajax({
			url: Maiurl,
			type: "post",
			data:urlString,
			async:false,
			success: function (string){

					$("#alertMessage").html(string);
					$("#messageBox").dialog( 'open' );

					populateResults(dataname);
				}
		});

	}
	function performDeletion(timesToDelete, username, audience){

		var maiJson= JSON.stringify(timesToDelete);
		var urlString= "json="+maiJson+"&username="+username;
		var Maiurl="./misc/ChatFunctions/deleteChats.php";

		$.ajax({
			url: Maiurl,
			type: "post",
			data:urlString,
			async:false,
			success: function (string){
					$("#alertMessage").html(string);
					$("#messageBox").dialog( 'open' );
					getChatsFromDB(audience, username);
				}
		});

	}
	//Made tunnel number variable (brian d.)
	function getTempAccountInfo(mode, custID, custCode, username, fromBtnPress){

		fromBtnPress = fromBtnPress ? true : false;
		var tunnel_number = 1;
		if(fromBtnPress){
			var tunnelSelector = document.getElementById('tunnel_select_id');
			tunnel_number = tunnelSelector.options[tunnelSelector.selectedIndex].value;
		}
		var urlString="mode="+mode+"&custID="+custID+"&custCode="+custCode+"&username="+username+"&tunnel_number="+tunnel_number;
		//alert(urlString);
		var URL= "./misc/MainFunctions/custLogin.php";
		var val=performAJAX(URL, urlString,false,"GET" );
		//For Tunnel 1
		if(val!='no_user'){
			$("#createTempAccount").html(val);
			$("#createTempAccount").attr("disabled", "disabled");
			$("#createTempAccount_2").attr("disabled", "disabled");
		}
		//For Tunnel 2
		if(val!='no_user' && tunnel_number == 2){
			$("#createTempAccount_2").html(val);
			$("#createTempAccount_2").attr("disabled", "disabled");
			$("#createTempAccount").attr("disabled", "disabled");
		}
	}

	function createMessage(group, user, message){
		var status='';
		if(user)
			status=performAJAX("/manage/mobile/sendMessage.php", "recipient="+user+"&message="+message+"&username="+getUsername(),false,'POST');
		else
			status=performAJAX("/manage/mobile/sendMessage.php", "recipient="+group+"&message="+message+"&username="+getUsername(),false,'POST');

		$('#message').val('');
		$('#user').val('');
		$('#group').val('');

		alert(status);
		doneLoading();
	}

	function messageAction(action, time, elem){
		var status=performAJAX("/manage/mobile/messageActions.php", "timestamp="+time+"&action="+action+"&username="+getUsername(),false,'POST');

		if( status=='success'){
			elem.removeClass('notViewed');
			elem.addClass('viewed');
		}
		else
			alert(status);
		doneLoading();
	}
	function get_area(area, clicked_elem){

		$("#myAccountArea").children().each(function(){
			var id= $(this).attr("id");
			if( id== area)
				$("#"+id).show();
			else
				$(this).hide();
		});
		$(".tab_selected").removeClass("tab_selected");
		$('.myAccountTabSelected').removeClass('myAccountTabSelected');
		clicked_elem.addClass('myAccountTabSelected');
	}


	function saveModules(rowID, dataname){
		var module_str = $("#modules").val();
			module_str = module_str.replace(/\./ig,'unddotund');
		var urlString= 'rowID='+rowID+"&updateTo="+encodeURIComponent( module_str )+"&dataname="+dataname;
		var result=performAJAX("./misc/MainFunctions/updateModules.php",urlString,false,'POST');
		if(result==='success'){
			alert('Module List Saved!');
		}else
			alert('An error has occurred and modules have not been saved. Please try again. Error: '+ result);

	}

	function performAJAX(URL, urlString,asyncSetting,type ){
		var returnVal='';
		loading();
		$.ajax({
			url: URL,
			type: type,
			data:urlString,
			async:asyncSetting,
			success: function (string){
					returnVal= string;

				}
		});
		doneLoading();
		return returnVal;
	}
	function modifyModule(str, symbol)
	{
		var parts = str.split(".");
		var result = "";
		for(var i = 0; i < parts.length; i++)
		{
			if(i > 0)
				result += ".";
			if(i === parts.length-1)
				result += symbol;
			result += parts[i];
		}

		return result;
	}

	function addModule(str, symbol)
	{
		deleteModule(str, symbol);
		var mods = $("#modules").val().split(",");
		mods.push(modifyModule(str, symbol));
		mods.sort();
		var result = "";
		for(var i = 0; i < mods.length; i++)
		{
			if(result !== "")
				result += ",";
			result += mods[i];
		}
		$("#modules").val(result);

		updateModuleList($("#modules").val());
	}

	function enableModule(str)
	{
		message = "You want to enable '" + str + "' module. Are you sure you want to proceed";
		symbol = "+";
		enableDisableModule(message, symbol, str)
	}

	function disableModule(str)
	{
		message = "You want to disable '" + str + "' module. Are you sure you want to proceed";
		symbol = "-";
		enableDisableModule(message, symbol, str)
	}

	function enableDisableModule(message, symbol, str) {
	    $('<div></div>').appendTo('body')
	    .html('<div><h4>'+message+'?</h4></div>')
	    .dialog({
	        modal: true, title: 'WARNING', zIndex: 10000, autoOpen: true,
	        width: '500', resizable: false,
	        buttons: {
	            Yes: function () {
			if (symbol == 'D') {
				deleteModule(str, symbol);
			} else {
				addModule(str, symbol);
			}
	                $(this).dialog('close');
	            },
	            No: function () {
	                $(this).dialog('close');
	            }
	        },
	        close: function (event, ui) {
	            $(this).remove();
	        }
	    });
	}

	function deleteModule(str, symbol = '')
	{
		if (symbol == '') {
			message = "You want to delete '" + str + "' module. Are you sure you want to proceed";
			symbol = "D";
			enableDisableModule(message, symbol, str)
		} else {
			var mods = $("#modules").val().split(",");
			var result = "";
			var str1 = str.replace("-","").replace("\\\+","");

			if(mods[0] === "")
				return;

			if (str1.charAt(str1.length - 1) === "*") {
				str1 = str1.substr(0, str1.length-1) + "\\\*";
			}

			for (var i = 0; i < mods.length; i++) {
				var enabled = true;
				var reg = new RegExp("^" + str1);
				var orig = mods[i];
				var mod = mods[i].replace("-","").replace("\+","");

				if (mods[i].match("-")) {
					enabled = false;
					//mods[i] = mods[i].replace("-","");

				} else if (mods[i].match("\\\+")) {
					enabled = true;
					//mods[i] = mods[i].replace("\\+","");
				}

				if (!(reg.exec(mod) &&  reg.exec(mod)[0] === mod)) {
					if (result !== "") {
						result += ",";
					}
					result += orig;
				}
			}
			$("#modules").val(result);
			updateModuleList($("#modules").val());
		}
	}

	function updateModuleList(str)
	{
		var mods = str;
		var result = "<pre>";
		result += str.replace(/,/g,"\n");
		result += "</pre>";

		$("#module_list").html(result);
	}
	function catchUpload(){
		var file = document.getElementById('uploader').files[0]; //Files[0] = 1st file
		var reader = new FileReader();
		reader.readAsText(file, 'UTF-8');
		reader.onload = shipOff;
		//reader.onloadstart = ...
		//reader.onprogress = ... <-- Allows you to update a progress bar.
		//reader.onabort = ...
		//reader.onerror = ...
		//reader.onloadend = ...
	}
	function shipOff(event) {
		var result = event.target.result;
		var fileName = document.getElementById('uploader').files[0].name; //Should be 'picture.jpg'
		$.post('/manage/misc/AdvancedToolsFunctions/uploader.php', {
			data: result,
			name: fileName
		}, continueSubmission);

	}

	function getInternetExplorerVersion()
	// Returns the version of Internet Explorer or a -1
	// (indicating the use of another browser).
	{
		var rv = -1; // Return value assumes failure.
		if (navigator.appName == 'Microsoft Internet Explorer')
		{
		var ua = navigator.userAgent;
		var re  = new RegExp("MSIE ([0-9]{1,}[\.0-9]{0,})");
		if (re.exec(ua) !== null)
			rv = parseFloat( RegExp.$1 );
		}
		return rv;
	}
	function checkVersion()
	{
		var msg = "You're not using Internet Explorer.";
		var ver = getInternetExplorerVersion();

		if ( ver > -1 )
		{
			if ( ver >= 8.0 )
				msg = "You're using a recent copy of Internet Explorer.";
			else
				msg = "You should upgrade your copy of Internet Explorer.";
		}
		alert( msg );
	}

	function getDataname() {
		var dataname = $.trim($('#dataname').val());
		return dataname;
	}

	function openNotificationsTab(elem){
		var tab= $(elem).attr('id');
		var dataname= $('#dataname').val();
		var type= $('#type').val();
		var username= getUsername();
		var urlString="function="+tab+"&username="+username;

		if(username === '')
			window.location.replace('https://admin.poslavu.com/manage/login.php');

		loading();
		$.ajax({

			url: "/manage/mobile/getNotifications.php",
			type: "POST",
			data:urlString,
			async: false,
			success: function (string){
				if(string!=='error'){
					$(".infoTabSelected").removeClass('infoTabSelected').addClass('infoTab');
					elem.addClass('infoTabSelected').removeClass('infoTab');

					$("#notificationsManageArea").find(".notificationsArea").html(string);

				}else
					$("#accountContainer").html("An error has occurred. Please try again. ");
				doneLoading();
			}
		});
	}
	function openDocument(elem){
		$("#compDocsContent").html('<object data="/manage/misc/companyDocs/'+elem.attr('name')+'" type="application/pdf" width="100%" height="90%"></object>');
		$('.docsTabSelected').removeClass('docsTabSelected');
		$("#compDocsContent").css("height","100%");
		elem.addClass('docsTabSelected');
	}

	function sortOnType(elem){
		var table= $("#docs").val();
		var sortOnVal= elem.html();
		var pullRows="";
		$(".sortType").each(function(){
			if($(this).html()==sortOnVal){

				pullRows+="<tr>"+$(this).parent().html()+"</tr>";

				$(this).parent().remove();
			}
		});

		$('.docsTbody').prepend(pullRows);


	}
	function delete_file(name){
		var result= performAJAX("/manage/misc/AdvancedToolsFunctions/delete_file.php", "name="+name,false,"POST");

	}
	function open_delete_box(name){
		$( '#messageBox' ).dialog({
			autoOpen: false,
			show: 'blind',
			hide: 'explode',
			buttons: {
					'Ok': function() {
						delete_file(name);
						$( this ).dialog( 'close' );
						$("#uploadDocsTab").click();
					}
			}
		});
		$("#alertMessage").css("width","100%");
		$("#alertMessage").html("Are you sure you want to delete: "+ name+ "?");
		$("#messageBox").dialog( 'open' );
	}

	window.start_zendesk_counter_timeout = 0;
	window.start_zendesk_counter_counter = (new Date()).getTime();
	function start_zendesk_counter(setValue) {
		window.start_zendesk_counter_counter++;
		counter = window.start_zendesk_counter_counter;
		zendesk_countdown(counter);
		var jsecs = $("input[name=seconds_until_zendesk_update]");
		if (jsecs.length > 0) {
			var secs = parseInt(jsecs.val(), 10);
			setTimeout(function() {
				zendesk_countdown(counter,secs-1*60*5);
			}, 1*60*5*1000);
			setTimeout(function() {
				zendesk_countdown(counter,secs-2*60*5);
			}, 2*60*5*1000);
			setTimeout(function() {
				zendesk_countdown(counter,secs-3*60*5);
			}, 3*60*5*1000);
			setTimeout(function() {
				zendesk_countdown(counter,secs-4*60*5);
			}, 4*60*5*1000);
		}
	}

	function zendesk_countdown(count, setValue) {
		if (count !== window.start_zendesk_counter_counter)
			return;
		clearTimeout(window.start_zendesk_counter_timeout);

		window.start_zendesk_counter_timeout = setTimeout(function() {
			zendesk_countdown(count);
		}, 1000);

		var jsecs = $("input[name=seconds_until_zendesk_update]");
		if (jsecs.length === 0)
			return;
		if (setValue) {
			jsecs.val(setValue);
		}
		var secs = parseInt(jsecs.val(), 10);
		var jdisplay = jsecs.siblings('div');
		var secstring = "00"+Math.abs(parseInt(secs%60, 10));
		secstring = secstring.substr(secstring.length-2,2);
		jdisplay.text("Time until next update from ZenDesk: "+parseInt(secs/60, 10)+":"+secstring);
		jsecs.val(secs-1);

		// get the timer from zendesk, again
		if (secs <= 0 && secs % 10 === 0) {
			$.ajax({
				url: '/cp/resources/contact_zendesk.php',
				async: true,
				cache: false,
				data: { get_next_update_timer: 1 },
				type: 'POST',
				success: function(m) {
					var jparent = jsecs.parent();
					jparent.html('');
					jparent.append(m);
				}
			});
		}
	}

	function switchManageMainTabsV2CP(event) {
		if (Lavu.currentUser.manageMainTabsV2CP) {
			Lavu.currentUser.manageMainTabsV2CP = false;
			$("#tabs").removeClass("v2");
		} else {
			Lavu.currentUser.manageMainTabsV2CP = true;
			$("#tabs").addClass("v2");
		}
		if (event.stopPropogation) {
			event.stopPropogation();
		} else {
			event.cancelBubbling = true;
		}
		return false;
	}
