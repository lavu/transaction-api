
$(document).ready(function(){
	$("#group").change(function (){
		if( $('#group').val()=='user'){
			$("#user").css('display','inline-block');
		}else{
			$("#user").css('display','none');
			$("#user").val('');
		}
	});
	
});

function LLSauthorize(childElement, authorize) {
	var jparent = $(childElement).parent();
	var dataname = jparent.children('[name=dataname]').val();
	$.ajax({
		async: true,
		cache: true,
		url: 'authorizeLLS.php',
		type: 'post',
		data: { dn: dataname, action: 'authorize', authorize: (authorize ? 1 : 0) },
		success: function(message) {
			alert(message);
			LLSgetQuickStats(childElement);
		},
		error: function(a,b,c) {
			alert('failed to communicate with server');
		}
	});
}
function LLSgetQuickStats(childElement) {
	var jparent = $(childElement).parent();
	var dataname = jparent.children('[name=dataname]').val();
	var jquickstatsContainer = jparent.children('.quickStatsContainer');
	$.ajax({
		async: true,
		cache: true,
		url: 'authorizeLLS.php',
		type: 'post',
		data: { dn: dataname, action: 'getQuickStats' },
		success: function(message) {
			jquickstatsContainer.html('');
			jquickstatsContainer.append(message);
		},
		error: function(a,b,c) {
			alert('failed to communicate with server');
		}
	});
}
function showPage(functionName, elem,height){
	
	id= "#"+elem.next().attr('id');
	
	if( $(".open")[0]){
		$(".open").animate({
		    //opacity: 0.25,
		    height: '-=400'
		  	// height: 'toggle'
		  }, 200, function() {
			  var tempID="#"+$(".open").attr('id');
			  $(tempID).removeAttr('class');
			  $(tempID).css("display","none");
			  
			  if(id!==tempID){
				  doOpen(id,height);
			  }
		});
	}else if(!$(id).hasClass('open')){
		doOpen(id, height);
	}
}
function doOpen(id,height){
	$(id).css("display","block");
	$(id).attr('class','open');
	
	$(id).animate({
	    height: '+='+height
	  }, 500, function() {
		  $(id).css({'display':'block', 'margin-top':'-1px'});
		  
	});
}
function searchAccount(searchString){
	var urlString='param='+searchString;
	loading();
		$.ajax({
		
			url: "/manage/mobile/search.php",
			type: "POST",
			data:urlString,
			async: false,
			success: function (string){
				$("#accountContainer").css('display',"none");
				$("#searchContainer").css('display',"inline-block");
				if(string!=='fail')
					$("#searchContainer").html(string); 
				else
					$("#searchContainer").html("An error has occurred. Please try again. ");
				doneLoading();
			}
	});
}
function openAccount(dataname, type){
	var tab='notesTab';
	
	var urlString='dataname='+dataname+"&tab="+tab+"&type="+type;
	loading();
	$.ajax({
	
		url: "/manage/mobile/openAccount.php",
		type: "POST",
		data:urlString,
		async: false,
		success: function (string){
			$("#deviceInfoTab").css('display','none');
			$("#accountContainer").css('display',"inline-block");
			$("#searchContainer").css('display',"none");
			if(string!=='fail'){
				 $('#dataname').val(dataname);
				 $('#type').val(type);
				 $(".infoTabSelected").removeClass('infoTabSelected').addClass('infoTab');
				 $('#notesTab').addClass('infoTabSelected').removeClass('infoTab');
				 $("#infoContainer").html(string); 
			}else
				$("#accountContainer").html("An error has occurred. Please try again. ");
			doneLoading();
		}
	});
}
function openTab(elem){
	var tab= $(elem).attr('id');
	var dataname= $('#dataname').val();
	var type= $('#type').val();
	var username= $('#name').val();
	var urlString='dataname='+dataname+"&tab="+tab+"&username="+username+"&type="+type;
	if(username='')
		window.location.replace('https://admin.poslavu.com/manage/login.php');
	loading();
	$.ajax({
	
		url: "/manage/mobile/openAccount.php",
		type: "POST",
		data:urlString,
		async: false,
		success: function (string){
			if(string!=='fail'){
				$(".infoTabSelected").removeClass('infoTabSelected').addClass('infoTab');
				elem.addClass('infoTabSelected').removeClass('infoTab');
				
				$("#infoContainer").html(string); 
			
			}else
				$("#accountContainer").html("An error has occurred. Please try again. ");
			doneLoading();
		}
	});
}
function getReloadTime(MAC, dataname, ip){
	if(MAC=='N/A')
		MAC='';
	var urlString= "MAC="+MAC+"&dataname="+dataname;
	$.ajax({
		url: "/manage/misc/MainFunctions/getReloadTime.php",
		type: "post",
		data:urlString,
		async:false,
		success: 
			function (string){
			
				document.getElementById(ip).innerHTML=string;
			}
	});	
}
function openMessagesTab(elem){
	var tab= $(elem).attr('id');
	var dataname= $('#dataname').val();
	var type= $('#type').val();
	var username= $('#name').val();
	var urlString="function="+tab+"&username="+username;
	
	if(username='')
		window.location.replace('https://admin.poslavu.com/manage/login.php');
	
	loading();
	$.ajax({
	
		url: "/manage/mobile/getMessages.php",
		type: "POST",
		data:urlString,
		async: false,
		success: function (string){
			if(string!=='error'){
				$(".infoTabSelected").removeClass('infoTabSelected').addClass('infoTab');
				elem.addClass('infoTabSelected').removeClass('infoTab');
				
				$("#messages").html(string); 
			
			}else
				$("#accountContainer").html("An error has occurred. Please try again. ");
			doneLoading();
		}
	});
}

function getTempAccountInfo(custID, custCode, username){
    mode='create_temp';
    var urlString="mode="+mode+"&custID="+custID+"&custCode="+custCode+"&username="+username;
    var URL= "/manage/misc/MainFunctions/custLogin.php";
    
    var val=performAJAX(URL, urlString,false,"GET" );
    $("#createTempAccount").html(val);
    $("#createTempAccount").attr("disabled", "disabled");
}
function submitLateNotice(){
	var reason=$("#reason").val();
	var minutes=$("#minsLate").val();
	var name= $("#name").val();
	//alert("reason:  "+ reason+"  mins:  "+ minutes);
	var urlString="reason="+reason+"&minutes="+minutes+"&name="+name; 
	loading();
	$.ajax({
		url: "../misc/MyAccount/submitLateNotice.php",
		type: "post",
		data:urlString,
		async:false,
		success: 
			function (string){
				if(string=='success') {
					doneLoading();
					alert("Notice Submitted");
					window.location.reload();	
				}else
					alert(string);
			}
	});	

}

function createMessage(group, user, message){
	var status='';
	if(user)
		status=performAJAX("/manage/mobile/sendMessage.php", "recipient="+user+"&message="+message+"&username="+$("#name").val(),false,'POST');
	else
		status=performAJAX("/manage/mobile/sendMessage.php", "recipient="+group+"&message="+message+"&username="+$("#name").val(),false,'POST');
	
	$('#message').val('');
	$('#user').val('');
	$('#group').val('');
	alert(status);
		
}

function messageAction(action, time, elem){
	status=performAJAX("/manage/mobile/messageActions.php", "timestamp="+time+"&action="+action+"&username="+$("#name").val(),false,'POST');
	
	if( status=='success'){
		elem.removeClass('notViewed');
		elem.addClass('viewed');
	}
	else
		alert(status);
}
function performAJAX(URL, urlString,asyncSetting,type ){
	var returnVal='';
    
     $.ajax({
		url: URL,
		type: type,
		data:urlString,
		async:asyncSetting,
		success: 
			function (string){
				returnVal= string; 
			}
	});	
   return returnVal; 
}
function loading(){
	$("#loading").css('height','100%');
	$("#loadng").css('width', '100%');
	$("#loading").css("display","block");
	$("#loadingText").css("display","block");
}
function doneLoading(){
	
	$("#loading").css("display","none");
	$("#loadingText").css("display","none");
}
