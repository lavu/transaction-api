(function(){
	if(!window.Lavu){
		window.Lavu = {};
	}

	if(!Lavu.util){
		Lavu.util = {};
	}

	(function(){
		function IEventHandler(){}
		function handleEvent( event ){}

		Lavu.util.IEventHandler = IEventHandler;
		IEventHandler.prototype = {};
		IEventHandler.prototype.handleEvent = handleEvent;
		IEventHandler.prototype.constructor = IEventHandler;
	})();

	(function(){
		function IEquivilence(){
			Object.call( this );
		}

		function equals( item ){
			return this === item;
		}

		Lavu.util.IEquivilence = IEquivilence;
		IEquivilence.prototype = {};
		IEquivilence.prototype.constructor = IEquivilence;
		IEquivilence.prototype.equals = equals;
	})();

	(function(){
		function IStringable(){
			Lavu.util.IEquivilence.call( this );
		}

		function _getFunctionType( structure, level, fwdStr ){
			return this.constructor.name;
		}

		function toString(){
			return '[Object ' + this._getFunctionType( Lavu )[0] + ']';
		}

		Lavu.util.IStringable = IStringable;
		IStringable.prototype = Object.create( Lavu.util.IEquivilence.prototype );
		IStringable.prototype.constructor = IStringable;
		IStringable.prototype._getFunctionType = _getFunctionType;
		IStringable.prototype.toString = toString;
	})();

	(function(){
		function Obj(){
			Lavu.util.IStringable.call( this );
		}

		function implements( proto ){
			var obj = Object.create( proto );
			for( var attrib in obj ){
				if( obj[attrib] && typeof obj[attrib] == 'function' ){
					if( this[attrib] === undefined || typeof this[attrib] != 'function' || this[attrib].length !== obj[attrib].length ){
						return false;
					}
				}
			}

			return true;
		}

		Lavu.util.Object = Obj;
		Obj.prototype = Object.create( Lavu.util.IStringable.prototype );
		Obj.prototype.constructor = Obj;
		Obj.prototype.implements = implements;
	})();

	(function(){
		function IUpdatable(){
			Lavu.util.Object.call( this );
		}

		function update(){
			return;
		}

		Lavu.util.IUpdatable = IUpdatable;
		IUpdatable.prototype = Object.create(  Lavu.util.Object.prototype );
		IUpdatable.prototype.constructor = IUpdatable;
		IUpdatable.prototype.update = update;
	})();

	(function(){
		function IObservable(){
			Lavu.util.IUpdatable.call( this );
			this._observers = new Lavu.util.Set();
		}

		function addObserver( observer ){
			observer.notify( this );
			return this._observers.add( observer );
		}

		function removeObserver( observer ){
			return this._observers.remove( observer );
		}

		function update( notifier ){
			if( notifier === undefined ){
				notifier = this;
			}
			var observers = this._observers.toArray();
			for( var i = 0; i < observers.length; i++ ){
				if( observers[i].shouldRemove() ){
					this._observers.remove( observers[i] );
					continue;
				}

				this._observers[i].notify( notifier );
			}
		}

		function reset(){
			this._observers = new Lavu.util.Set();
		}

		Lavu.util.IObservable = IObservable;
		IObservable.prototype = Object.create( Lavu.util.IUpdatable.prototype );
		IObservable.prototype.constructor = IObservable;
		IObservable.prototype.addObserver = addObserver;
		IObservable.prototype.removeObserver = removeObserver;
		IObservable.prototype.update = update;
		IObservable.prototype.reset = reset;
	})();

	(function(){
		function ObservableField( ownerObject, fieldName, fieldValue ){
			Lavu.util.IObservable.call( this );
			this._owner = ownerObject;
			this._fieldName = fieldName;
			this._fieldValue = fieldValue;
		}

		function owner(){
			return this._owner;
		}

		function fieldName(){
			return this._fieldName;
		}

		function fieldValue(){
			return this._fieldValue;
		}

		function setFieldValue( value ){
			this._fieldValue = value;
			this.update( this );
		}

		Lavu.util.ObservableField = ObservableField;
		ObservableField.prototype = Object.create( Lavu.util.IObservable.prototype );
		ObservableField.prototype.constructor = ObservableField;
		ObservableField.prototype.owner = owner;
		ObservableField.prototype.fieldName = fieldName;
		ObservableField.prototype.fieldValue = fieldValue;
		ObservableField.prototype.setFieldValue = setFieldValue;
	})();

	(function(){
		function IObserver(){
			Lavu.util.IUpdatable.call( this );
			this._observables = new Lavu.util.Set();
		}

		function addObservable( observable ){
			return this._observables.add( observable );
		}

		function removeObservable( observable ){
			return this._observables.remove( observable );
		}

		function shouldRemove(){
			return false;
		}

		function reset(){
			this._observables = new Lavu.util.Set();
		}

		function notify( notifier ){}

		Lavu.util.IObserver = IObserver;
		IObserver.prototype = Object.create( Lavu.util.IUpdatable.prototype );
		IObserver.prototype.constructor = IObserver;
		IObserver.prototype.addObservable = addObservable;
		IObserver.prototype.removeObservable = removeObservable;
		IObserver.prototype.shouldRemove = shouldRemove;
		IObserver.prototype.reset = reset;
		IObserver.prototype.notify = notify;
	})();

	(function(){
		function Set(){
			Array.call( this );
		}

		function add( item ){
			if(!item){
				return false;
			}
			if(!(item instanceof Lavu.util.Object ) ){
				return false;
			}

			if( this.indexOf( item ) === -1 ){
				return this.length < this.push( item );
			}
		}

		function remove( item ){
			if(!item){
				return false;
			}
			if(!(item instanceof Lavu.util.Object ) ){
				return false;
			}

			var index = this.indexOf( item );
			if( index !== -1 ){
				return this.splice( index, 1 )[0].equals( item );
			}
		}

		function indexOf( item ){
			if(!item){
				return -1;
			}
			if(!(item instanceof Lavu.util.Object ) ){
				return -1;
			}

			for( var i = 0; i < this.length; i++ ){
				if( item.equals( this[i] ) ){
					return i;
				}
			}
			return -1;
		}

		function toArray(){
			return this;
			// return this.slice(0, this.length);
		}

		Lavu.util.Set = Set;
		Set.prototype = [];
		Set.prototype.constructor = Set;
		Set.prototype.add = add;
		Set.prototype.remove = remove;
		Set.prototype.indexOf = indexOf;
		Set.prototype.toArray = toArray;
	})();
})();