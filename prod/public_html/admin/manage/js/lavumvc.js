//mvc
(function(){
	if( ! window.Lavu ) {
		window.Lavu = {};
	}

	if( ! window.Lavu.mvc ) {
		window.Lavu.mvc = {};
	}

	(function(){
		/**
		 * An MVC Model, represents the base-line representation of any data based object.  Anything
		 * wishing to employ the MVC framework should extend Lavu.mvc.Model instead of Object.
		 *
		 * This class contains base representations of the Model, including defining functions for
		 * running a Lavu.mvc.Command that has been submitted to it, as well as the ability to Store
		 * Lavu.mvc.Views or remove them.
		 *
		 * If you extend Lavu.mvc.Model, any time your interal data changes, you are required to call
		 * updateViews(), so that your views will be updated appropriately.
		 **/
		function Model() {
			Lavu.util.IUpdatable.call( this );
		}

		/**
		 * This method iterates through all of the Lavu.mvc.Model's Lavu.mvc.Views and informs them to
		 * update by passing them a reference to itself.  Utilizing that information, the Lavu.mvc.View
		 * will be able to update its representation accordingly.
		 *
		 * @returns: void.
		 **/
		function update() {}

		/**
		 * The run method will attempt to run the Lavu.mvc.Command that has been provided by providing the
		 * command a reference to itself.
		 *
		 * @param command: The Lavu.mvc.Command to call run on.
		 *
		 * @param returns: true if the Lavu.mvc.Command was successfully run, false otherwise.  This will
		 *				not run the command if it is not a Lavu.mvc.Command.
		 **/
		function run( command ) {
			if(! command ) {
				return false;
			}

			if(! command instanceof Lavu.mvc.Command ) {
				return false;
			}

			command.run( this );
			return true;
		}

		/* Object Setup and Inheritence  */
		Lavu.mvc.Model = Model;
		Model.prototype = Object.create( Lavu.util.IUpdatable.prototype );
		Model.prototype.constructor = Model;
		Model.prototype.update = update;
		Model.prototype.run = run;
	})();

	(function(){

		//__allViews = [];

		//function mainDrawLoop(){
		//	for( var i = 0; i < __allViews.length; i++ ){
		//		__allViews[i].update();
		//	}
		//}

		//var __mainLoop = window.setInterval( mainDrawLoop, 300 );

		/**
		 * An MVC View, represents a 'view' that is able to display information when provided a Lavu.mvc.Model.
		 * The Lavu.mvc.View is represented as an object and creates an HTML Element for which it is
		 * responsible for. In this case, the default HTML Element is a div.  A Lavu.mvc.View can also have
		 * a parent and children. It is only responsible for itself however, and is not requires to inform
		 * it's children of any updates.
		 **/
		function View() {
			Lavu.util.IObserver.call( this );
			//__allViews.push( this );
			this._notifiers = new Lavu.util.Set();
			Lavu.RunLoop.drawLoop().add( this );
			Lavu.RunLoop.drawLoop().startLoop();
		}

		function destruct(){
			return delete this._notifiers;
		}

		function notify( model ){
			if(!model){
				return false;
			}

			if(! (model instanceof Lavu.util.IObservable||model instanceof Lavu.mvc.Model) ){
				return false;
			}

			return this._notifiers.add( model );
		}

		function update(){
			while( this._notifiers.length ){
				this.draw( this._notifiers[0] );
				this._notifiers.splice(0,1);
			}
		}

		/**
		 * This method is requires by anything extending Lavu.mvc.View.  By default, in order to ensure
		 * it's implementation this function returns false to signify that it wasn't able to draw.
		 * In general, it should draw the information it's representing accordingly based on what
		 * is presented within the Lavu.mvc.Model that it has been handed.
		 *
		 * @param model: The Lavu.mvc.Model that is to be provided in order to update this Lavu.mvc.View.
		 *
		 * @returns: true if updated successfully, false otherwise.
		 **/
		function draw( model ) {
			return false;
		}

		/* Object setup and Inheritence */
		Lavu.mvc.View = View;
		View.prototype = Object.create( Lavu.util.IObserver.prototype );
		View.prototype.constructor = View;
		View.prototype.update = update;
		View.prototype.draw = draw;
		View.prototype.notify = notify;
	})();

	(function(){
		/**
		 * TODO: COMMENTS NEED UPDATING
		 *
		 * An mvc Controller.  The Lavu.mvc.Controller is spawned with an initial Model.
		 * It primarily passes Commands to the provided Model, and is responsible for overall inital setup
		 * of most things.
		 **/
		function Controller( model ) {
			Lavu.util.IObserver.call( this );
			this._models = new Lavu.util.Set();
			this._views = new Lavu.util.Set();
			this._controllers = new Lavu.util.Set();

			if( model){
				this._models.add( model );
			}
		}

		/**
		 * TODO: COMMENTS NEED UPDATING
		 *
		 * The runCommand method will pass the provided Lavu.mvc.Command to the primary Model.
		 *
		 * @param command: the Lavu.mvc.Command command to pass to the stored Lavu.mvc.Model.
		 * @returns: void;
		 **/
		function runCommand( command ) {
			/*var target = command.getTarget();
			if( !target ) {
				target = Lavu.mvc.Model;
			}

			var model = this.getModelByType( target );
			if( model ) {
				model.run( command );
			}

			return;*/

			if( !command ){
				return false;
			}

			if( !command instanceof Lavu.mvc.Command ){
				return false;
			}
			var result = true;
			var i;
			for( i = 0; i < this._controllers.length; i++ ){
				result = this._controllers[i].runCommand( command ) && result;
			}

			for( i = 0; i < this._models.length; i++ ){
				if(! command.filter( this._models[i] )){
					result = this._models[i].run( command ) && result;
				}
			}
			return result;
		}

		function getModelByType( type ) {
			if( ! type ) {
				return null;
			}

			for( var i = 0; i < this._models.length; i++ ){
				if( this._models[i] instanceof type ){
					return this._models[i];
				}
			}

			return null;
		}

		function addModel( model ) {
			if(! model ){
				return false;
			}

			if(! model instanceof Lavu.mvc.Model ){
				return false;
			}
			return this._models.add( model );
		}

		function removeModel( model ) {
			if(! model ){
				return false;
			}

			if(! model instanceof Lavu.mvc.Model ){
				return false;
			}
			return this._models.remove( model );
		}

		function addView( view ){
			if(! view ){
				return false;
			}

			if(! view instanceof Lavu.mvc.View ){
				return false;
			}

			return this._views.add( view );
		}

		function removeView( view ){
			if(! view ){
				return false;
			}

			if(! view instanceof Lavu.mvc.View ){
				return false;
			}
			return this._views.remove( view );
		}

		function addController( controller ){
			if(! controller ){
				return false;
			}

			if(! controller instanceof Lavu.mvc.controller ){
				return false;
			}
			return this._controllers.add( controller );
		}

		function removeController( controller ){
			if(! controller ){
				return false;
			}

			if(! controller instanceof Lavu.mvc.controller ){
				return false;
			}
			return this._controllers.remove( controller );
		}

		function reset(){
			this._models = new Lavu.util.Set();
			this._views = new Lavu.util.Set();
			this._controllers = new Lavu.util.Set();
		}

		function drawLoop(){
			for( var i = 0; i < this._views.length; i++ ){
				this._views[i].update();
			}
		}

		/* Object setup and Inheritence */
		Lavu.mvc.Controller = Controller;
		Controller.prototype = Object.create( Lavu.util.IObserver.prototype );
		Controller.prototype.constructor = Controller;
		Controller.prototype.runCommand = runCommand;
		Controller.prototype.getModelByType = getModelByType;
		Controller.prototype.addModel = addModel;
		Controller.prototype.removeModel = removeModel;
		Controller.prototype.addController = addController;
		Controller.prototype.removeController = removeController;
		Controller.prototype.addView = addView;
		Controller.prototype.removeView = removeView;
		Controller.prototype.drawLoop = drawLoop;
		Controller.prototype.reset = reset;
	})();

	(function(){
		/**
		 * An mvc Command is a Lavu.mvc.Command that promsises to be extended and implement functionality for
		 * run method.
		 **/
		function Command( ) {
			Lavu.util.Object.call( this );
			this._target = null;
		}

		/**
		 * The run method will perform the actions neccessary on the provided Lavu.mvc.Model.  Please ensure
		 * that the provided Object is Not null, and is a Lavu.mvc.Model before performing any tasks on the
		 * Lavu.mvc.Model to avoid errors.
		 *
		 * @param model: The Lavu.mvc.Model to manipulate with the Lavu.mvc.Command.
		 *
		 * @returns: true upon successfully running, false otherwise.
		 **/
		function run( model ) {
			return false;
		}

		/**
		 * Returns the current Target for this Lavu.mvc.Command.  By default this is null, and it's up to the
		 * extender to modify this.  The point of the target is to specify a class type so that the Lavu.mvc.Model
		 * or Lavu.mvc.Controller can either choose to accept/discard this Lavu.mvc.Command, or so they can redirect
		 * the Lavu.mvc.Command to an appropriate recipient.
		 *
		 * @returns the currently set target of this Lavu.mvc.Command
		 **/
		function getTarget() {
			return this._target;
		}

		/**
		 * Will set the current target of this Lavu.mvc.Command to whatever is specified. This does not filter the
		 * input and will even accept null.
		 *
		 * @returns void
		 **/
		function setTarget( target ) {
			this._target = target;
		}

		function filter( obj ){
			if ( this._target ){
				return !(obj instanceof this._target);
			}
			return false;
		}


		/* Object setup and Inheritence */
		Lavu.mvc.Command = Command;
		Command.prototype = Object.create( Lavu.util.Object.prototype );
		Command.prototype.constructor = Command;
		Command.prototype.run = run;
		Command.prototype.getTarget = getTarget;
		Command.prototype.setTarget = setTarget;
		Command.prototype.filter = filter;
	})();

})();