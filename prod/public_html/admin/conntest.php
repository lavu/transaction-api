<?php
	if( isset($_REQUEST['ping']) ){
		echo 1;
		exit();
	}

	if( isset($_REQUEST['post']) ){
		$str = '';
		for($i = 0; $i < 100; $i++ ){
			$str .= $_REQUEST['post'] . "\r\n";
		}
		echo $str;
		exit();
	}

	define('TEMP_FILE', '/tmp/testwrite.txt' );
	define('FILE_LENGTH', 2048 );

	if( isset($_REQUEST['write']) || isset($_REQUEST['read'])){
		$start = mstime();
		$file_handle = fopen( TEMP_FILE, 'w');
		for( $i = 0; $i < FILE_LENGTH; $i++ ){
			fwrite( $file_handle, rand(0, 255));
		}
		fclose( $file_handle );

		if( isset($_REQUEST['read']) ){
			$start = mstime();
			$file_handle = fopen( TEMP_FILE, 'r' );
			$read_len = 1;
			$i=0;
			for( $i = 0; $i < FILE_LENGTH; $i += $read_len ){
				if(fread( $file_handle, $read_len ) === FALSE ){
					//Error
					echo 'Error: ';
					break;
				}
			}

			fclose( $file_handle );
		}


		unlink( TEMP_FILE );

		$end = mstime();

		echo $end - $start;
		exit();
	}

	function mstime(){
		return (int)(microtime(true)*1000);
	}


?><!DOCTYPE html>
<html>
	<head>
		<script type="text/javascript">
			(function(){
				function AJAXRequest( url, params, requestType, async ){
					this._request = null;
					if (window.XMLHttpRequest){
						this._request = new XMLHttpRequest();		
					} else { 
						this._request = new ActiveXObject("Microsoft.XMLHTTP");
					}

					this._time_of_end = null;
					this._time_of_connect = null;
					this._time_of_request_recieved = null;
					this._time_of_request_processed = null;
					this._time_of_request_finished = null;

					this._time_to_process = null;
					this._time_to_recieve = null;
					this._time_to_connect = null;
					this._time_to_finish = null;

					this._duration_of_connect = null;
					this._duration_of_recieved = null;
					this._duration_of_processed = null;
					this._duration_of_finished = null;
					this._duration_of_total = null;

					this._request.addEventListener( 'readystatechange', this );
					this._time_of_start = new Date();
					this._request.open( requestType, url, async );
					this._request.setRequestHeader("Content-type","application/x-www-form-urlencoded");
					this._request.send( params );
				}

				function handleEvent( event ){
					switch( this._request.readyState ) {
						case 0:
							this._time_of_start = new Date();
							break;
						case 1:
							this._time_of_connect = new Date();
							this._time_to_connect = this._time_of_connect.valueOf() - this._time_of_start.valueOf();
							break;
						case 2:
							this._time_of_request_recieved = new Date();
							this._time_to_recieve = this._time_of_request_recieved.valueOf() - this._time_of_start.valueOf();
							this._duration_of_connect = this._time_of_request_recieved.valueOf() - this._time_of_connect.valueOf();
							break;
						case 3:
							this._time_of_request_processed = new Date();
							this._time_to_process = this._time_of_request_processed.valueOf() - this._time_of_start.valueOf();
							this._duration_of_recieved = this._time_of_request_processed.valueOf() - this._time_of_request_recieved.valueOf();
							break;
						case 4:
							this._time_of_request_finished = new Date();
							this._time_to_finish = this._time_of_request_finished.valueOf() - this._time_of_start.valueOf();
							this._duration_of_processed = this._time_of_request_finished.valueOf() - this._time_of_request_processed.valueOf();
							break;
						default:
					}

					if( this._request.readyState == 4 ){
						this._time_of_end = new Date();
						this._duration_of_finished = this._time_of_end.valueOf() - this._time_of_request_finished.valueOf();
						this._duration_of_total = this._time_of_end.valueOf() - this._time_of_start.valueOf();
						if( this._request.status == 200 ){
							this.onSuccess( this._request.responseText )
						} else {
							this.onFailure( this._request.status, this._request.responseText );
						}
					}
				}

				function onSuccess( responseText ){
					console.log( responseText );
				}

				function onFailure( status, responseText ){
					console.error( status, responseText );
				}

				function timeOfStart(){
					return this._time_of_start;
				}

				function timeOfEnd(){
					return this._time_of_end;
				}

				function timeOfConnect(){
					return this._time_of_connect;
				}

				function timeOfRequestRecieved(){
					return this._time_of_request_recieved;
				}

				function timeOfRequestProcessed(){
					return this._time_of_request_processed;
				}

				function timeOfRequestFinished(){
					return this._time_of_request_finished;
				}

				function timeToConnect(){
					return this._time_to_connect;
				}

				function timeToRecieve(){
					return this._time_to_recieve;
				}

				function timeToProcess(){
					return this._time_to_process;
				}

				function timeToFinish(){
					return this._time_to_finish;
				}

				function durationOfConnect(){
					return this._duration_of_connect;
				}

				function durationOfRecieved(){
					return this._duration_of_recieved;
				}

				function durationOfProcessed(){
					return this._duration_of_processed;
				}

				function durationOfFinished(){
					return this._duration_of_finished;
				}

				function durationOfTotal(){
					return this._duration_of_total;
				}

				window.AJAXRequest = AJAXRequest;
				AJAXRequest.prototype = {};
				AJAXRequest.prototype.constructor = AJAXRequest;
				AJAXRequest.prototype.handleEvent = handleEvent;
				AJAXRequest.prototype.onSuccess = onSuccess;
				AJAXRequest.prototype.onFailure = onFailure;
				AJAXRequest.prototype.timeOfStart = timeOfStart;
				AJAXRequest.prototype.timeOfEnd = timeOfEnd;
				AJAXRequest.prototype.timeOfConnect = timeOfConnect;
				AJAXRequest.prototype.timeOfRequestRecieved = timeOfRequestRecieved;
				AJAXRequest.prototype.timeOfRequestProcessed = timeOfRequestProcessed;
				AJAXRequest.prototype.timeOfRequestFinished = timeOfRequestFinished;
				AJAXRequest.prototype.timeToConnect = timeToConnect;
				AJAXRequest.prototype.timeToRecieve = timeToRecieve;
				AJAXRequest.prototype.timeToProcess = timeToProcess;
				AJAXRequest.prototype.timeToFinish = timeToFinish;
				AJAXRequest.prototype.durationOfConnect = durationOfConnect;
				AJAXRequest.prototype.durationOfRecieved = durationOfRecieved;
				AJAXRequest.prototype.durationOfProcessed = durationOfProcessed;
				AJAXRequest.prototype.durationOfFinished = durationOfFinished;
				AJAXRequest.prototype.durationOfTotal = durationOfTotal;

			})();

			(function(){
				function AjaxRequestDetailDrawer( host, params, reqType, async ){
					AJAXRequest.call( this, host, params, reqType, async );
				}

				function createDivWithAttribtutes(){
					var div = document.createElement('div');
					div.style.position = 'absolute';
					div.style.margin = '0';
					div.style.padding = '0';
					div.style.display = 'block';
					div.style.height = '15px';
					return div;
				}

				function outputDetails( ele ){
					ele.style.position = 'relative';

					var div = document.createElement('div');
					ele.appendChild( div );
					div.style.boxShadow = 'rgba(0,0,0,0.25) 0px 1px 5px 1px';
					div.style.padding = '5px';
					div.style.borderRadius =  '3px';
					div.style.position = 'absolute';
					div.style.top = '15px';
					div.style.display = 'none';
					div.style.background = 'rgba(255,255,255,0.9)';
					div.style.zIndex = 1;


					ele.onmouseover = function( event ){
						div.style.display = 'block';
						div.style.left = 10 + 'px';
					}

					ele.onmouseout = function(event ){
						div.style.display = 'none';
						div.style.left = 10 + 'px';
					}

					var scale = 0.25;

					var totalTime = createDivWithAttribtutes();
					div.appendChild( totalTime );
					totalTime.setAttribute('title','Total Process Time: ' + (this.durationOfTotal()) + 'ms' );
					totalTime.style.width = (this.durationOfTotal())*scale + 'px';
					totalTime.style.border = '1px solid rgba(0,0,0,0.25)';

					var details = document.createElement('div');
					details.style.marginTop = '15px';

					var connectDetails = document.createElement('div');
					var receivedDetails = document.createElement('div');
					var processedDetails = document.createElement('div');
					var finishedDetails = document.createElement('div');

					details.appendChild( connectDetails );
					details.appendChild( receivedDetails );
					details.appendChild( processedDetails );
					details.appendChild( finishedDetails );

					var l = this.timeOfConnect() - this.timeOfStart().valueOf();
					var connectBlock = createDivWithAttribtutes();
					totalTime.appendChild( connectBlock );
					connectBlock.setAttribute('title', 'Time of Connect: ' + l + 'ms, duration: ' + ( this.durationOfConnect() ) + 'ms' );
					connectDetails.appendChild( document.createTextNode( 'Time of Connect: ' + l + 'ms, duration: ' + ( this.durationOfConnect() ) + 'ms' ) );
					connectBlock.style.left = l*scale + 'px';
					connectBlock.style.width = this.durationOfConnect()*scale + 'px';
					connectBlock.style.background = 'green';
					connectDetails.style.color = 'green';

					var l = this.timeOfRequestRecieved() - this.timeOfStart().valueOf();
					var recievedBlock = createDivWithAttribtutes();
					totalTime.appendChild( recievedBlock );
					recievedBlock.setAttribute('title', 'Time of Request Recieved: ' + l + 'ms, duration: ' + ( this.durationOfRecieved() ) + 'ms' );
					receivedDetails.appendChild( document.createTextNode( 'Time of Request Recieved: ' + l + 'ms, duration: ' + ( this.durationOfRecieved() ) + 'ms' ) );
					recievedBlock.style.left = l*scale + 'px';
					recievedBlock.style.width = this.durationOfRecieved()*scale + 'px';
					recievedBlock.style.background = 'orange';
					receivedDetails.style.color = 'orange';

					var l = this.timeOfRequestProcessed() - this.timeOfStart().valueOf();
					var processedBlock = createDivWithAttribtutes();
					totalTime.appendChild( processedBlock );
					processedBlock.setAttribute('title', 'Time of Request Processed: ' + l + 'ms, duration: ' + ( this.durationOfProcessed() ) + 'ms' );
					processedDetails.appendChild( document.createTextNode( 'Time of Request Processed: ' + l + 'ms, duration: ' + ( this.durationOfProcessed() ) + 'ms' ) );
					processedBlock.style.left = l*scale + 'px';
					processedBlock.style.width = this.durationOfProcessed()*scale + 'px';
					processedBlock.style.background = 'red';
					processedDetails.style.color = 'red';

					var l = this.timeOfRequestFinished() - this.timeOfStart().valueOf();
					var finishedBlock = createDivWithAttribtutes();
					totalTime.appendChild( finishedBlock );
					finishedBlock.setAttribute('title', 'Time of Request Finish: ' + l + 'ms, duration: ' + ( this.durationOfFinished() ) + 'ms' );
					finishedDetails.appendChild( document.createTextNode( 'Time of Request Finish: ' + l + 'ms, duration: ' + ( this.durationOfFinished() ) + 'ms' ) );
					finishedBlock.style.left = l*scale + 'px';
					finishedBlock.style.width = this.durationOfFinished()*scale + 'px';
					finishedBlock.style.background = 'gray';
					finishedDetails.style.color = 'gray';

					div.appendChild( details );

				}

				window.AjaxRequestDetailDrawer = AjaxRequestDetailDrawer;
				AjaxRequestDetailDrawer.prototype = Object.create( AJAXRequest.prototype );
				AjaxRequestDetailDrawer.prototype.outputDetails = outputDetails;
			})();

			var pingSuccessCount = 0;
			var pingTotalCount = 0;
			var pingRunTimes = [];

			(function(){
				function PingTest(){
					this._pingIndex = pingTotalCount++;
					this._startTime = new Date();
					AjaxRequestDetailDrawer.call( this, window.location.href, 'ping=1','POST', true );
				}

				function onSuccess( responseText ){
					this._endTime = new Date();
					if( responseText == '1' ){
						pingSuccessCount++;
					}

					this.wrapUp( '<span style="color: green;">Succeeded</span>' );
				}

				function onFailure( status, responseText ){
					this._endTime = new Date();
					this.wrapUp( '<span style="color: red;">Failed</span>' );
				}

				function wrapUp( message ){
					var runTime = this._endTime.valueOf() - this._startTime.valueOf();
					pingRunTimes.push( runTime );

					var ele = document.getElementById('ping_body');
					var div = document.createElement('div');

					ele.appendChild( div );

					var extra = '';
					if( this._pingIndex > 0 ){
						var diff =  pingRunTimes[this._pingIndex] - pingRunTimes[this._pingIndex-1]; 
						if( diff < 0 ){
							extra = '<span style="color: green; font-size: 0.8em;">'+String.fromCharCode(0x25BC)+Math.abs(diff)+'ms</span>';
						} else if( diff > 0 ){
							extra = '<span style="color: red; font-size: 0.8em;">'+String.fromCharCode(0x25B2)+diff+'ms</span>';
						} else {
							extra = '<span style="color: #ccc; font-size: 0.8em;">'+diff+'ms</span>';
						}

						extra = ' (' + extra + ')';
					}

					var timing_details = '<span style="font-size: 0.8em;">' + this.durationOfConnect() + 'ms, ' + this.durationOfRecieved() + 'ms, ' + this.durationOfProcessed() + 'ms, ' + this.durationOfFinished() + 'ms' + '</span>';

					div.innerHTML = 'Ping ' + (this._pingIndex + 1) + ' ' + message + ' After ' + (runTime) + 'ms' + extra + ' ' + timing_details;

					this.outputDetails( div );

					var avgElement = document.getElementById('ping_avg');
					if( !avgElement ){
						avgElement = document.createElement('div');
						avgElement.setAttribute('id','ping_avg');
						avgElement.id = 'ping_avg';
					}
					avgElement.remove();

					var sum = 0;
					for( var i = 0; i < pingRunTimes.length; i++ ){
						sum += pingRunTimes[i];
					}
					var avg = sum / pingRunTimes.length;
					avgElement.innerHTML = '<i>Average Run Time: <b>' + Math.round(avg,2) + '</b>ms</i> ' + pingSuccessCount + '/' + pingTotalCount + ' Ran Successfully';
					ele.appendChild( avgElement );

					if( postTotalCount < 10 ){
						new PostTest();
					} else {

					}
				}

				window.PingTest = PingTest;
				PingTest.prototype = Object.create( AjaxRequestDetailDrawer.prototype );
				PingTest.prototype.onSuccess = onSuccess;
				PingTest.prototype.onFailure = onFailure;
				PingTest.prototype.wrapUp = wrapUp;
			})();

			var postSuccessCount = 0;
			var postTotalCount = 0;
			var postRunTimes = [];

			(function(){
				function PostTest(){
					this._postIndex = postTotalCount++;
					this._startTime = new Date();
					this._payload = '';
					for( var i = 0; i < 4096; i++ ){
						this._payload += String.fromCharCode( Math.floor(Math.random()*26)+('a'.charCodeAt(0)) )
					}

					AjaxRequestDetailDrawer.call( this, window.location.href, 'post=' + this._payload,'POST', true );
				}

				function onSuccess( responseText ){
					this._endTime = new Date();
					var data = responseText.substr( 0, responseText.indexOf("\r\n") );
					if( data == this._payload ){
						postSuccessCount++;
					}

					this.wrapUp( '<span style="color: green;">Succeeded</span>' );
				}

				function onFailure( status, responseText ){
					this._endTime = new Date();
					this.wrapUp( '<span style="color: red;">Failed</span>' );
				}

				function wrapUp( message ){
					var runTime = this._endTime.valueOf() - this._startTime.valueOf();
					postRunTimes.push( runTime );

					var ele = document.getElementById('post_body');
					var div = document.createElement('div');

					ele.appendChild( div );

					var extra = '';
					if( this._postIndex > 0 ){
						var diff =  postRunTimes[this._postIndex] - postRunTimes[this._postIndex-1]; 
						if( diff < 0 ){
							extra = '<span style="color: green; font-size: 0.8em;">'+String.fromCharCode(0x25BC)+Math.abs(diff)+'ms</span>';
						} else if( diff > 0 ){
							extra = '<span style="color: red; font-size: 0.8em;">'+String.fromCharCode(0x25B2)+diff+'ms</span>';
						} else {
							extra = '<span style="color: #ccc; font-size: 0.8em;">'+diff+'ms</span>';
						}

						extra = ' (' + extra + ')';
					}

					var timing_details = '<span style="font-size: 0.8em;">' + this.durationOfConnect() + 'ms, ' + this.durationOfRecieved() + 'ms, ' + this.durationOfProcessed() + 'ms, ' + this.durationOfFinished() + 'ms' + '</span>';

					div.innerHTML = 'Post ' + (this._postIndex + 1) + ' ' + message + ' After ' + (runTime) + 'ms' + extra + ' ' + timing_details;
					this.outputDetails( div );

					var avgElement = document.getElementById('post_avg');
					if( !avgElement ){
						avgElement = document.createElement('div');
						avgElement.setAttribute('id','post_avg');
						avgElement.id = 'post_avg';
					}
					avgElement.remove();

					var sum = 0;
					for( var i = 0; i < postRunTimes.length; i++ ){
						sum += postRunTimes[i];
					}
					var avg = sum / postRunTimes.length;
					avgElement.innerHTML = '<i>Average Run Time: <b>' + Math.round(avg,2) + '</b>ms</i> ' + postSuccessCount + '/' + postTotalCount + ' Ran Successfully';
					ele.appendChild( avgElement );

					if( writeTotalCount < 10 ){
						new WriteTest();
					} else {
						// new PostTest();
					}
				}

				window.PostTest = PostTest;
				PostTest.prototype = Object.create( AjaxRequestDetailDrawer.prototype );
				PostTest.prototype.onSuccess = onSuccess;
				PostTest.prototype.onFailure = onFailure;
				PostTest.prototype.wrapUp = wrapUp;
			})();

			var writeSuccessCount = 0;
			var writeTotalCount = 0;
			var writeRunTimes = [];

			(function(){
				function WriteTest(){
					this._writeIndex = writeTotalCount++;
					this._startTime = new Date();
					this._processTime = undefined;
					this._payload = '';
					for( var i = 0; i < 4096; i++ ){
						this._payload += String.fromCharCode( Math.floor(Math.random()*26)+('a'.charCodeAt(0)) )
					}

					AjaxRequestDetailDrawer.call( this, window.location.href, 'write=' + this._payload,'POST', true );
				}

				function onSuccess( responseText ){
					writeSuccessCount++;
					this._endTime = new Date();
					this._processTime = responseText * 1;

					this.wrapUp( '<span style="color: green;">Succeeded</span>' );
				}

				function onFailure( status, responseText ){
					this._endTime = new Date();
					this.wrapUp( '<span style="color: red;">Failed</span>' );
				}

				function wrapUp( message ){
					var runTime = this._processTime === undefined ? this._endTime.valueOf() - this._startTime.valueOf():this._processTime;
					writeRunTimes.push( runTime );

					var ele = document.getElementById('write_body');
					var div = document.createElement('div');

					ele.appendChild( div );

					var extra = '';
					if( this._writeIndex > 0 ){
						var diff =  writeRunTimes[this._writeIndex] - writeRunTimes[this._writeIndex-1]; 
						if( diff < 0 ){
							extra = '<span style="color: green; font-size: 0.8em;">'+String.fromCharCode(0x25BC)+Math.abs(diff)+'ms</span>';
						} else if( diff > 0 ){
							extra = '<span style="color: red; font-size: 0.8em;">'+String.fromCharCode(0x25B2)+diff+'ms</span>';
						} else {
							extra = '<span style="color: #ccc; font-size: 0.8em;">'+diff+'ms</span>';
						}

						extra = ' (' + extra + ')';
					}
					var timing_details = '<span style="font-size: 0.8em;">' + this.durationOfConnect() + 'ms, ' + this.durationOfRecieved() + 'ms, ' + this.durationOfProcessed() + 'ms, ' + this.durationOfFinished() + 'ms' + '</span>';
					div.innerHTML = 'Write ' + (this._writeIndex + 1) + ' ' + message + ' After ' + (runTime) + 'ms' + extra + ' ' + timing_details;
					this.outputDetails( div );

					var avgElement = document.getElementById('write_avg');
					if( !avgElement ){
						avgElement = document.createElement('div');
						avgElement.setAttribute('id','write_avg');
						avgElement.id = 'write_avg';
					}
					avgElement.remove();

					var sum = 0;
					for( var i = 0; i < writeRunTimes.length; i++ ){
						sum += writeRunTimes[i];
					}
					var avg = sum / writeRunTimes.length;
					avgElement.innerHTML = '<i>Average Run Time: <b>' + Math.round(avg,2) + '</b>ms</i> ' + writeSuccessCount + '/' + writeTotalCount + ' Ran Successfully';
					ele.appendChild( avgElement );

					if( readTotalCount < 10 ){
						new ReadTest();
					} else {
						// new writeTest();
					}
				}

				window.WriteTest = WriteTest;
				WriteTest.prototype = Object.create( AjaxRequestDetailDrawer.prototype );
				WriteTest.prototype.onSuccess = onSuccess;
				WriteTest.prototype.onFailure = onFailure;
				WriteTest.prototype.wrapUp = wrapUp;
			})();

			var readSuccessCount = 0;
			var readTotalCount = 0;
			var readRunTimes = [];

			(function(){
				function ReadTest(){
					this._readIndex = readTotalCount++;
					this._startTime = new Date();
					this._processTime = undefined;
					this._payload = '';
					for( var i = 0; i < 4096; i++ ){
						this._payload += String.fromCharCode( Math.floor(Math.random()*26)+('a'.charCodeAt(0)) )
					}

					AjaxRequestDetailDrawer.call( this, window.location.href, 'read=' + this._payload,'POST', true );
				}

				function onSuccess( responseText ){
					readSuccessCount++;
					this._endTime = new Date();
					this._processTime = responseText * 1;

					this.wrapUp( '<span style="color: green;">Succeeded</span>' );
				}

				function onFailure( status, responseText ){
					this._endTime = new Date();
					this.wrapUp( '<span style="color: red;">Failed</span>' );
				}

				function wrapUp( message ){
					var runTime = this._processTime === undefined ? this._endTime.valueOf() - this._startTime.valueOf():this._processTime;
					readRunTimes.push( runTime );

					var ele = document.getElementById('read_body');
					var div = document.createElement('div');

					ele.appendChild( div );

					var extra = '';
					if( this._readIndex > 0 ){
						var diff =  readRunTimes[this._readIndex] - readRunTimes[this._readIndex-1]; 
						if( diff < 0 ){
							extra = '<span style="color: green; font-size: 0.8em;">'+String.fromCharCode(0x25BC)+Math.abs(diff)+'ms</span>';
						} else if( diff > 0 ){
							extra = '<span style="color: red; font-size: 0.8em;">'+String.fromCharCode(0x25B2)+diff+'ms</span>';
						} else {
							extra = '<span style="color: #ccc; font-size: 0.8em;">'+diff+'ms</span>';
						}

						extra = ' (' + extra + ')';
					}
					var timing_details = '<span style="font-size: 0.8em;">' + this.durationOfConnect() + 'ms ' + this.durationOfRecieved() + 'ms, ' + this.durationOfProcessed() + 'ms, ' + this.durationOfFinished() + 'ms' + '</span>';
					div.innerHTML = 'Read ' + (this._readIndex + 1) + ' ' + message + ' After ' + (runTime) + 'ms' + extra + ' ' + timing_details;
					this.outputDetails( div );

					var avgElement = document.getElementById('read_avg');
					if( !avgElement ){
						avgElement = document.createElement('div');
						avgElement.setAttribute('id','read_avg');
						avgElement.id = 'read_avg';
					}
					avgElement.remove();

					var sum = 0;
					for( var i = 0; i < readRunTimes.length; i++ ){
						sum += readRunTimes[i];
					}
					var avg = sum / readRunTimes.length;
					avgElement.innerHTML = '<i>Average Run Time: <b>' + Math.round(avg,2) + '</b>ms</i> ' + readSuccessCount + '/' + readTotalCount + ' Ran Successfully';
					ele.appendChild( avgElement );

					if( pingTotalCount < 10 ){
						new PingTest();
					} else {
						// new readTest();
					}
				}

				window.ReadTest = ReadTest;
				ReadTest.prototype = Object.create( AjaxRequestDetailDrawer.prototype );
				ReadTest.prototype.onSuccess = onSuccess;
				ReadTest.prototype.onFailure = onFailure;
				ReadTest.prototype.wrapUp = wrapUp;
			})();
		</script>
	</head>
	<body>
		<div id="ping_body" style="display: inline-table; padding: 5px;"><h3>Ping Test</h3></div>
		<div id="post_body" style="display: inline-table; padding: 5px;"><h3>Post Test</h3></div>
		<div id="write_body" style="display: inline-table; padding: 5px;"><h3>Write Test</h3></div>
		<div id="read_body" style="display: inline-table; padding: 5px;"><h3>Read Test</h3></div>
		<p>The result's timing shows how long each request took, whether they succeeded, and averages the successes.  The numbers to the right, indicate the timings of particular events: Time to Connect, Time to Recieve, Time to Process, Time to Finish</p>
		<p><b>Ping Test</b>: This is a simple test which hits the Server, at this url, with a variable named ping set to 1.  The server, upon success, returns just the string 1.  This is an extremely light-weight request and should occur pretty quickly.</p>  
		<p><b>Post Test</b>: This test hits the Server, at this url, with a variable named post set to a randomly generated 4kb string.  The server, upon success, the ent string 100 times appended with carriage return and a newline between each.  The resulting data sent back should be 409600 bytes, or ~400 Kilobytes worth of data</p>
		<p><b>Write Test</b>: This is a simple test designed to test overall server stability/performance by simply writing to a file in /tmp.  It writes 2046 randomly generated bytes, and records how long it takes to write the file.  The sever returns the amount of time it took to do the write only, and this is what is displayed.</p>
		<p><b>Read Test</b>: This is a simple test designed to help narrow down whether the results of the Write test are Server Load based, or just an IO block in general.  This test does the same things a Write initially.  After it creates the temporary file, it then opens it and reads from it, and times how long that takes.</p>
		<!--<div id="post_body" style="display: inline-table;"><h3>Memory Test</h3></div>-->
		<script type="text/javascript">
			(function (){
				new PingTest();
				// for( var i = 0; i < 10; i++ ){
				// 	new PingTest();
				// 	new PostTest();
				// }
			})();
		</script>
	</body>
</html>