
function initialize(){
	populate_user_info();
	initialize_listeners();
	build_add_referral_form();
	build_past_referrals();
	build_purchased_referrals();
	document.getElementsByClassName("opt_btn")[0].click();
}
function populate_user_info(){
	var container = document.getElementsByClassName("my_account")[0];
	var inputs = container.getElementsByClassName("user_info_holder");

	for(var i=0; i<inputs.length; i++){
		var val = inputs[i].getAttribute("name");
		if(val=='id')
			inputs[i].value =user_info[val];
		else	
			inputs[i].innerHTML =user_info[val];

	}
}
function initialize_listeners(){

	var edit_btn= document.getElementsByClassName("edit_info")[0];
		edit_btn.addEventListener("click",edit_info);
	
	var sub= document.getElementsByClassName("submit_ref")[0];
		sub.addEventListener("click",submit_ref);
	var out= document.getElementsByClassName("logout")[0];
		out.addEventListener("click",logout);
	var account= document.getElementsByClassName("my_account_btn")[0];
		account.addEventListener("click",open_my_account);

	var btns = document.getElementsByClassName("opt_btn");
	for(var i=0; i < btns.length; i++){
		btns[i].addEventListener("click", change_tab);
	}
}
function change_tab(evt){

	if(document.getElementsByClassName("opt_btn_selected").length)	{
		document.getElementsByClassName(document.getElementsByClassName("opt_btn_selected")[0].getAttribute("name"))[0].style.display='none';
		var selected_tab = document.getElementsByClassName("opt_btn_selected")[0].className = "opt_btn";
		
	}
	
	evt.target.className+=" opt_btn_selected";
	document.getElementsByClassName(evt.target.getAttribute("name"))[0].style.display='block';

}
function open_my_account(){
	var acc= document.getElementsByClassName("my_account")[0];
	if(acc.style.display=='none')
		acc.style.display="inline-block";
	else
		acc.style.display="none";
}
function logout(){
	var res = perform_ajax("./referrer_data.php","function=logout",false,"POST");
	location.reload();
}
function edit_info(){
	var container = document.getElementsByClassName("my_account")[0];
	var inputs = container.getElementsByClassName("user_info_holder ");
	for(var i=0; i<inputs.length; i++){
		if(inputs[i].getAttribute("name") !='id' && inputs[i].getAttribute("name") !='lavu_tag'){
			var edit_field = document.createElement("input");
				edit_field.setAttribute("type","text");	
				edit_field.setAttribute("name", inputs[i].getAttribute("name"));
				edit_field.value = inputs[i].innerHTML;
				edit_field.className = 'long_inputs';
			inputs[i].innerHTML ="";
			inputs[i].appendChild(edit_field);
		}
	}
	var cont = document.getElementsByClassName("edit_user_options")[0];
	cont.innerHTML = '';
	var submit_btn = document.createElement("input");
		submit_btn.setAttribute('type',"button");
		submit_btn.value = "Save Info";
		submit_btn.addEventListener("click",save_user_info);
	
	var cancel_btn =document.createElement("input");
		cancel_btn.setAttribute('type',"button");
		cancel_btn.value = "Cancel";
		cancel_btn.addEventListener("click",close_user_info_editor);

	cont.appendChild(submit_btn);
	cont.appendChild(cancel_btn);
	
}
function build_add_referral_form(){
	var tbod = document.getElementsByClassName("add_referral_form")[0];
	var form = 
		[
			{name:"Primary Contact *", type:"text", class_name:"req_info",db_val:"primary_contact", validation: not_null},
			{name:"Company Name *",    type:"text", class_name:"req_info",db_val:"company_name", validation: not_null},
			{name:"Phone Number",      type:"text", class_name:"opt_info",db_val:"phone_num"},
			{name:"Secondary Phone",   type:"text", class_name:"opt_info",db_val:"secondary_phone_num"},
			{name:"Email", 			   type:"text", class_name:"opt_info",db_val:"email"},
			{name:"Alt Email",		   type:"text", class_name:"opt_info",db_val:"alt_email"},
			{name:"Address 1",		   type:"text", class_name:"opt_info",db_val:"address_1"},
			{name:"Address 2",		   type:"text", class_name:"opt_info",db_val:"address_2"},
			{name:"City", 			   type:"text", class_name:"opt_info",db_val:"city"},
			{name:"State", 			   type:"text", class_name:"opt_info",db_val:"state"},
			{name:"Zip", 			   type:"text", class_name:"opt_info",db_val:"zip"},
			{name:"Country", 		   type:"text", class_name:"opt_info",db_val:"country"},
			{name:"Website", 		   type:"text", class_name:"opt_info",db_val:"website"},
			{name:"Notes", 			   type:"textarea", class_name:"opt_info", db_val:"notes"},
			{name:"Email Blurb", 	   type:"textarea", class_name:"opt_info", db_val:"blurb"},
			{name:"referree_id", 	   type:"hidden",   class_name:"req_info",db_val:"referree_id", default_val:user_info.id},
		];
	var built = build_form(form);
	tbod.appendChild(built);
}
function submit_ref(){

	var	stringified = "";
	var required = document.getElementsByClassName("req_info");
	var opt 	 = document.getElementsByClassName("opt_info");
	var fields_arr = [];
	for(var i = 0; i < required.length; i++){
		var val    = required[i].children[0].value;
		var db_val = required[i].children[0].getAttribute("db_val");
		if(val == ''){
			alert("Empty Required Field");
		}else{
			fields_arr.push(db_val+"="+val);
		}
	}
	for(var j = 0; j < opt.length; j++){
		var val    = opt[j].children[0].value;
		var db_val = opt[j].children[0].getAttribute("db_val");
		if(val != ''){
			fields_arr.push(db_val+"="+val);
		}
	}
	stringified = fields_arr.join("&");
	var res = perform_ajax("./referrer_data.php","function=save_referral&"+stringified,false,"POST");
	if(res.status=='error'){
		alert("Could not edit. "+ res.message);
	}else{
		alert(res.message);
		location.reload();
	}
}
function not_null(){
	return true;
}
function save_user_info(){
	var container = document.getElementsByClassName("my_account")[0];
	var inputs = container.getElementsByTagName("input");
	var serialized = new Array();
	for(var i=0; i<inputs.length; i++){
		if(inputs[i].getAttribute('type') != 'button' && inputs[i].getAttribute("name") !='lavu_tag'){
			serialized[i]=inputs[i].getAttribute('name')+"="+inputs[i].value; 
		}
	}
	var str = serialized.join("&");
	var res = perform_ajax("./referrer_data.php","function=update_referrer&"+str,false,"POST");
	if(res.status=='error'){
		alert("Could not edit. "+ res.message);
	}
	close_user_info_editor();
}
function close_user_info_editor(){
	var container = document.getElementsByClassName("my_account")[0];
	var inputs = container.getElementsByClassName("user_info_holder");
	for(var i=0; i<inputs.length; i++){
		inputs[i].innerHTML =inputs[i].children[0].value;
	}
	var cont = document.getElementsByClassName("edit_user_options")[0];
	cont.innerHTML = '';
	var edit_btn = document.createElement("input");
		edit_btn.setAttribute('type',"button");
		edit_btn.value = "Edit User Info";
		edit_btn.addEventListener("click",edit_info);
	cont.appendChild(edit_btn);
}
function build_purchased_referrals(){
	var res = perform_ajax("./referrer_data.php","function=get_purchased_referrals&referree_id="+user_info.id,false,"POST");
	var container = document.getElementsByClassName("purchased_referrals")[0];

	var tr = document.createElement("tr");
	for(var col in res[0]){
		var th = document.createElement("th");
			th.innerHTML = col;
			tr.appendChild(th);
	}
	container.appendChild(tr);

	for( var i = 0; i < res.length; i++){
		var tr = document.createElement("tr");
		for(var col in res[i]){
			if(col == 'other_info'){
				var parsed = JSON.parse(res[i][col]);
				var table  = document.createElement("table");
					table.className = 'other_info_table';
				var div    = document.createElement("div");
					div.className ='inner_table_container';
				for( var key in parsed){
					var tr2 = document.createElement("tr");
					var td2 = document.createElement("td");
						td2.className = 'inner_table_td';
						td2.innerHTML = key;

					var td3 = document.createElement("td");
						td3.innerHTML = parsed[key];
						td3.className = 'inner_table_td other_info_val';
						td3.setAttribute("other_name", key);

					tr2.appendChild(td2);
					tr2.appendChild(td3);
					table.appendChild(tr2);
				}
				div.appendChild(table);
				var s_td = document.createElement("td");
					s_td.className = 'outer_table_td';
					s_td.setAttribute("db_col", col);
					s_td.appendChild(div);
					tr.appendChild(s_td);

			}else{
				var s_td = document.createElement("td");
					s_td.className = 'outer_table_td';
					s_td.innerHTML = res[i][col];
					s_td.setAttribute("db_col", col);
				tr.appendChild(s_td);
			}
		}
		
		container.appendChild(tr);
	}
}
function edit_past_referral(evt){
	var elems = evt.target.parentNode.parentNode.getElementsByClassName("outer_table_td");
	for(var i=0; i < elems.length; i++){
		if(elems[i].getAttribute("db_col")== 'primary_contact' || elems[i].getAttribute("db_col")=='company_name'){
			var val = elems[i].innerHTML;
			var inp = document.createElement("input");
				inp.setAttribute("type","text");
				inp.setAttribute("db_val",elems[i].getAttribute("db_col"));
				inp.value=val;
			elems[i].innerHTML ='';
			elems[i].appendChild(inp);
		}else if(elems[i].getAttribute("db_col")=='other_info'){
			var inner_table =elems[i].getElementsByClassName("other_info_val");
			var form = [
				{name:"Phone Number",      type:"text", class_name:"opt_info",db_val:"phone_num"},
				{name:"Secondary Phone",   type:"text", class_name:"opt_info",db_val:"secondary_phone_num"},
				{name:"Email", 			   type:"text", class_name:"opt_info",db_val:"email"},
				{name:"Alt Email",		   type:"text", class_name:"opt_info",db_val:"alt_email"},
				{name:"Address 1",		   type:"text", class_name:"opt_info",db_val:"address_1"},
				{name:"Address 2",		   type:"text", class_name:"opt_info",db_val:"address_2"},
				{name:"City", 			   type:"text", class_name:"opt_info",db_val:"city"},
				{name:"State", 			   type:"text", class_name:"opt_info",db_val:"state"},
				{name:"Zip", 			   type:"text", class_name:"opt_info",db_val:"zip"},
				{name:"Country", 		   type:"text", class_name:"opt_info",db_val:"country"},
				{name:"Website", 		   type:"text", class_name:"opt_info",db_val:"website"},
				{name:"Notes", 			   type:"textarea", class_name:"opt_info", db_val:"notes"},
				{name:"Email Blurb",	   type:"textarea", class_name:"opt_info", db_val:"blurb"}
				/*{name:"referree_id", 	   type:"hidden",   class_name:"req_info",db_val:"referree_id", default_val:user_info.id  }*/
			];
			var col_names = [];
			var vals 	  = [];
			for(var j = 0; j < inner_table.length; j++){
				col_names.push(inner_table[j].getAttribute("other_name"));
				vals.push(inner_table[j].innerHTML);
			}
			for( var j = 0; j < form.length; j++ ){
				var index = col_names.indexOf(form[j].db_val)
				if( index != -1){
					form[j].default_val = vals[index];
				}
			}
			var built = build_form(form);
			elems[i].innerHTML ='';
			elems[i].appendChild(built);
		}else if(elems[i].getAttribute("db_col")=='btn'){
			var save_btn = document.createElement("input");
				save_btn.setAttribute("type","button");
				save_btn.addEventListener("click",save_edited_referral);
				save_btn.setAttribute("value","Update Referral");
			var cancel_btn = document.createElement("input");
				cancel_btn.setAttribute("type","button");
				cancel_btn.addEventListener("click",cancel_edited_referral);
				cancel_btn.setAttribute("value","Cancel");
			elems[i].innerHTML ='';
			elems[i].appendChild(save_btn);
			elems[i].appendChild(cancel_btn);
		}
	}
}
function build_past_referrals(){
	var res = perform_ajax("./referrer_data.php","function=get_past_referrals&referree_id="+user_info.id,false,"POST");
	var container = document.getElementsByClassName("past_referrals")[0];

	var tr = document.createElement("tr");
	for(var col in res[0]){
		var th = document.createElement("th");
			th.innerHTML = col;
			tr.appendChild(th);
	}
	container.appendChild(tr);


	for( var i = 0; i < res.length; i++){
		var tr = document.createElement("tr");
		for(var col in res[i]){
			if(col == 'other_info'){
				var parsed = JSON.parse(res[i][col]);
				var table  = document.createElement("table");
					table.className = 'other_info_table';
				var div    = document.createElement("div");
					div.className ='inner_table_container';
				for( var key in parsed){
					var tr2 = document.createElement("tr");
					var td2 = document.createElement("td");
						td2.className = 'inner_table_td';
						td2.innerHTML = key;

					var td3 = document.createElement("td");
						td3.innerHTML = parsed[key];
						td3.className = 'inner_table_td other_info_val';
						td3.setAttribute("other_name", key);

					tr2.appendChild(td2);
					tr2.appendChild(td3);
					table.appendChild(tr2);
				}
				div.appendChild(table);
				var s_td = document.createElement("td");
					s_td.className = 'outer_table_td';
					s_td.setAttribute("db_col", col);
					s_td.appendChild(div);
					tr.appendChild(s_td);

			}else{
				var s_td = document.createElement("td");
					s_td.className = 'outer_table_td';
					s_td.innerHTML = res[i][col];
					s_td.setAttribute("db_col", col);
				tr.appendChild(s_td);
			}
		}
		
		var td = document.createElement("td");
			td.className = 'outer_table_td';
			td.setAttribute("db_col","btn");
		var btn= document.createElement("input");
			btn.setAttribute("type","button");
			btn.setAttribute("value","Edit Referral");
			btn.className = 'long_inputs';
			btn.addEventListener("click",edit_past_referral); 
		td.appendChild(btn);
		tr.appendChild(td);
		
		container.appendChild(tr);
	}
}
function edit_past_referral(evt){
	var elems = evt.target.parentNode.parentNode.getElementsByClassName("outer_table_td");
	for(var i=0; i < elems.length; i++){
		if(elems[i].getAttribute("db_col")== 'primary_contact' || elems[i].getAttribute("db_col")=='company_name'){
			var val = elems[i].innerHTML;
			var inp = document.createElement("input");
				inp.setAttribute("type","text");
				inp.setAttribute("db_val",elems[i].getAttribute("db_col"));
				inp.value=val;
			elems[i].innerHTML ='';
			elems[i].appendChild(inp);
		}else if(elems[i].getAttribute("db_col")=='other_info'){
			var inner_table =elems[i].getElementsByClassName("other_info_val");
			var form = [
				{name:"Phone Number",      type:"text", class_name:"opt_info",db_val:"phone_num"},
				{name:"Secondary Phone",   type:"text", class_name:"opt_info",db_val:"secondary_phone_num"},
				{name:"Email", 			   type:"text", class_name:"opt_info",db_val:"email"},
				{name:"Alt Email",		   type:"text", class_name:"opt_info",db_val:"alt_email"},
				{name:"Address 1",		   type:"text", class_name:"opt_info",db_val:"address_1"},
				{name:"Address 2",		   type:"text", class_name:"opt_info",db_val:"address_2"},
				{name:"City", 			   type:"text", class_name:"opt_info",db_val:"city"},
				{name:"State", 			   type:"text", class_name:"opt_info",db_val:"state"},
				{name:"Zip", 			   type:"text", class_name:"opt_info",db_val:"zip"},
				{name:"Country", 		   type:"text", class_name:"opt_info",db_val:"country"},
				{name:"Website", 		   type:"text", class_name:"opt_info",db_val:"website"},
				{name:"Notes", 			   type:"textarea", class_name:"opt_info", db_val:"notes"},
				{name:"Email Blurb",	   type:"textarea", class_name:"opt_info", db_val:"blurb"}
				/*{name:"referree_id", 	   type:"hidden",   class_name:"req_info",db_val:"referree_id", default_val:user_info.id  }*/
			];
			var col_names = [];
			var vals 	  = [];
			for(var j = 0; j < inner_table.length; j++){
				col_names.push(inner_table[j].getAttribute("other_name"));
				vals.push(inner_table[j].innerHTML);
			}
			for( var j = 0; j < form.length; j++ ){
				var index = col_names.indexOf(form[j].db_val)
				if( index != -1){
					form[j].default_val = vals[index];
				}
			}
			var built = build_form(form);
			elems[i].innerHTML ='';
			elems[i].appendChild(built);
		}else if(elems[i].getAttribute("db_col")=='btn'){
			var save_btn = document.createElement("input");
				save_btn.setAttribute("type","button");
				save_btn.addEventListener("click",save_edited_referral);
				save_btn.setAttribute("value","Update Referral");
			var cancel_btn = document.createElement("input");
				cancel_btn.setAttribute("type","button");
				cancel_btn.addEventListener("click",cancel_edited_referral);
				cancel_btn.setAttribute("value","Cancel");
			elems[i].innerHTML ='';
			elems[i].appendChild(save_btn);
			elems[i].appendChild(cancel_btn);
		}

	}
}
function save_edited_referral(evt){
	var pars= evt.target.parentNode.parentNode;
	cancel_edited_referral(evt);
	var outer = pars.getElementsByClassName("outer_table_td");
	var inner = pars.getElementsByClassName("inner_table_td");
	
	var serialized = new Array();
	for( var i =0; i< outer.length; i++){
		if(outer[i].getAttribute('db_col') !='other_info' && outer[i].getAttribute('db_col') !='referral_date' && outer[i].getAttribute('db_col') !='btn' && outer[i].getAttribute('db_col') !='contacted' )
		serialized.push(outer[i].getAttribute('db_col')+"="+encodeURIComponent(outer[i].innerHTML)); 
	}
	for(var i=0; i<inner.length; i++){	
		serialized.push(inner[i].getAttribute('other_name')+"="+encodeURIComponent(inner[i].innerHTML)); 
	}

	var str = serialized.join("&");
	console.log(str);
	var res = perform_ajax("./referrer_data.php","function=update_referral&"+str,false,"POST");
	if(res.status=='error'){
		alert("Could not edit. "+ res.message);
	}	
}
function cancel_edited_referral(evt){
	var inps = evt.target.parentNode.parentNode.getElementsByTagName("input");
	var first =true;
	while(inps.length > 2 ){

		if(inps[0].getAttribute('type') != 'button'){
			if( inps[0].getAttribute("db_val")!='primary_contact' && inps[0].getAttribute("db_val")!='company_name'){
				if(first){
					inps[0].parentNode.parentNode.parentNode.className = 'inner_table_container';
					first= false;
				}
				inps[0].parentNode.className ="inner_table_td other_info_val";
				inps[0].parentNode.setAttribute("other_name",inps[0].getAttribute('db_val'));
			}
			var val = inps[0].value;
			
			inps[0].parentNode.innerHTML = val;
		}
	}
	var t_area = evt.target.parentNode.parentNode.getElementsByTagName("textarea");
		t_area[0].parentNode.className ="inner_table_td other_info_val";
		t_area[0].parentNode.setAttribute("other_name",t_area[0].getAttribute('db_val'));
		t_area[0].parentNode.innerHTML = t_area[0].value;
	
	var btn= document.createElement("input");
		btn.setAttribute("type","button");
		btn.setAttribute("value","Edit Referral");
		btn.className = 'long_inputs';
		btn.addEventListener("click",edit_past_referral); 
	var btn_container = inps[0].parentNode;
		btn_container.innerHTML= '';
		btn_container.appendChild(btn);
}
function build_form(form){
	var cont = document.createElement("div");
	for(var i=0; i < form.length; i++){
		var tr = document.createElement("tr");
		//var td = document.createElement("td");
		//if(form[i].type != 'hidden')
		//	td.innerHTML = form[i].name;
		var inner_td = document.createElement("td");
			inner_td.className= "new_referral_info "+ form[i].class_name;
		//tr.appendChild(td);
		
		if(form[i].type == 'text'){
			var inp = document.createElement("input");
				inp.setAttribute("type","text");
				inp.setAttribute("db_val", form[i].db_val);
				inp.setAttribute("placeholder",form[i].name);
				inp.className  = 'long_inputs';
			if(form[i].validation){
				inp.addEventListener("click", form[i].validation);	
			}
			if(form[i].default_val){
				inp.value = form[i].default_val;
			}			
			inner_td.appendChild(inp);
		}else if(form[i].type == 'textarea'){
			var inp = document.createElement("textarea");
				inp.setAttribute("db_val", form[i].db_val)
				inp.className ='long_inputs';
				inp.setAttribute("placeholder",form[i].name);
			if(form[i].default_val){
				inp.value = form[i].default_val;
			}			
			inner_td.appendChild(inp);
		}else if(form[i].type == 'hidden'){
			var inp = document.createElement("input");
				inp.setAttribute("type","hidden");
				inp.setAttribute("db_val", form[i].db_val);
				if(form[i].default_val){
					inp.value = form[i].default_val;
				}
			inner_td.appendChild(inp);
		}
		tr.appendChild(inner_td);
		cont.appendChild(tr);
	}
	return cont;
}

function perform_ajax(URL, urlString,asyncSetting,type ){
	var returnVal='';
	$.ajax({
		url: URL,
		type: type,
		data:urlString,
		async:asyncSetting,
		success: function (string){
			try{
				returnVal = JSON.parse(string);
			}catch(err){
				console.log(err);
				returnVal = string;
			}
		}
	});
	return returnVal;
}
window.onload= initialize;
