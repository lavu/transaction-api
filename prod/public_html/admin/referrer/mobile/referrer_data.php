<?php
	ini_set("display_errors",1);
	require_once($_SERVER['DOCUMENT_ROOT']."/cp/resources/json.php");
	if ( isset($_POST['function'])){
		if (function_exists($_POST['function'])){
			require_once($_SERVER['DOCUMENT_ROOT']."/cp/resources/lavuquery.php");
			$fun_name = $_POST['function'];	
			unset($_POST['function']);
			echo $fun_name($_POST);
		}else
			echo "error:no function of name ". $_POST['function'];
	}else
		echo "error: no function name given.";

	function check_unused_email($args){		
		if(isset($args['email'])){
			$query = mlavu_query("SELECT * FROM `poslavu_REFERRER_db`.`referrers` where `email`='[1]' AND `_deleted`='0' limit 1", $args['email']);
			if(mysqli_num_rows($query)){
				return '{"status":"error", "message":"Email already registered"}';
			}else{
				return '{"status":"success", "message": "unused email"}';
			}
		}else{
			return '{"status":"error", "message":"no email sent"}';
		}
	}
	function check_unused_lavutag($args){
		if(isset($args['tag'])){
			$query = mlavu_query("SELECT * FROM `poslavu_REFERRER_db`.`referrers` where `lavu_tag`='[1]' AND `_deleted`='0' limit 1", $args['tag']);
			if(mysqli_num_rows($query))
				return '{"status":"error", "message": "lavu tag already registered"}';
			else
				return '{"status":"success", "message": "unused tag"}';
		}else{
			return '{"status":"error", "message": "no tag sent"}';
		}
	}
	function update_referral($args){
		$arr = array();
		$arr['id']	  			= $args['id'];
		$arr['referree_id']	  	= $args['referree_id'];
		$arr['primary_contact'] = $args['primary_contact'];
		$arr['company_name']	= $args['company_name'];
		unset($args['id']);
		unset($args['referree_id']);
		unset($args['primary_contact']);
		unset($args['company_name']);
		$arr['other_info']= LavuJson::json_encode($args);
		$query_str="UPDATE `poslavu_REFERRER_db`.`referrals` SET `primary_contact` ='[primary_contact]', `company_name`='[company_name]', `other_info`='[other_info]' where `referree_id`='[referree_id]' and `id`='[id]'";
		mlavu_query($query_str, $arr);
		if( mlavu_dberror())
			return '{"status":"error","message":"'.mlavu_dberror().'"}';
		else
			return '{"status":"success", "message":"success"}';
	}
	function save_referrer($args){
		$unused_tag_str   = check_unused_lavutag(array("tag"=>$args['lavu_tag']));
		$unused_email_str = check_unused_email(array("email"=>$args['email']));
		
		$unused_lavu_tag = LavuJson::json_decode($unused_tag_str);
		$unused_email    = LavuJson::json_decode($unused_email_str);

		if( ($args['password'] == $args['password2']) && $args['password']!='' && $unused_lavu_tag['status']=='success' && $unused_email['status']=='success'){
			mlavu_query("INSERT INTO `poslavu_REFERRER_db`.`referrers` (`lavu_tag`, `email`,`first_name`, `last_name`,`phone`, `password`, `_deleted`) VALUES ('[1]','[2]','[3]','[4]','[5]',PASSWORD('[6]'),'0')",$args['lavu_tag'],$args['email'],$args['f_name'], $args['l_name'], $args['phone'],$args['password']);
			if( mlavu_dberror())
				return '{"status":"error","message":"'.mlavu_dberror().'"}';
			else
				return '{"status":"success", "message":"success"}';
		}else if( $unused_lavu_tag['status'] == 'error'){
			return $unused_tag_str;
		}else if($unused_email['status'] == 'error'){
			return $unused_email_str;
		}else 
			return '{"status":"error","message":"uncaught gayness"}';
	}
	function update_referrer($args){
		$q  = mlavu_query("SELECT * FROM `poslavu_REFERRER_db`.`referrers` WHERE `email`='[1]' and `id`='[2]' ", $args['email'], $args['id']);
		$q2 = mlavu_query("SELECT * FROM `poslavu_REFERRER_db`.`referrers` WHERE `email`='[1]'", $args['email']);
		if(!mysqli_num_rows($q) && mysqli_num_rows($q2)){
			return '{"status":"error","message":"Used Email"}';
		}else{
			$query_str='UPDATE `poslavu_REFERRER_db`.`referrers` SET ';
			$update_arr= array();
			foreach($args as $key=>$value){
				if($value!='' && $key !='id'){
					$update_arr[] = "`$key`='[$key]'";
				}
			}
			$query_str.= implode(",", $update_arr). "WHERE `id`='[id]' limit 1";
			mlavu_query($query_str, $args);

		}
	}
	function login($args){
		$un = $args['username'];
		$pw = $args['password'];
		$query = mlavu_query("SELECT * FROM `poslavu_REFERRER_db`.`referrers` WHERE (`lavu_tag`='[1]' AND `password` = PASSWORD('[2]') ) OR (`email`='[1]' AND `password` = PASSWORD('[2]')) limit 1", $un, $pw);
		if(mysqli_num_rows($query)){
		$query = mlavu_query("SELECT * FROM `poslavu_REFERRER_db`.`logged_in` WHERE `username` = '[1]' AND `valid`='1'", $un);
			if(mysqli_num_rows($query)){
				if(isset($_COOKIE['poslavu_referrer_login'])){
					$token = $_COOKIE['poslavu_referrer_login'];
					$query2 = mlavu_query("SELECT * FROM `poslavu_REFERRER_db`.`logged_in` WHERE `username` = '[1]' AND `token`='[2]' AND `valid`='1'", $un, $token);
					if( mysqli_num_rows($query2))
						return '{"status":"success","message":"prev_login"}';
					else{
						return perform_login($un);
						//return '{"status":"error","message":"invalid token"}';
					}
				}else
					return perform_login($un);
			}else
				return perform_login($un);
			
		}else
			return '{"status":"error", "message":"Username/Password combination invalid."}';
	}
	function perform_login($un){
	
		$token = generate_token();
		$query = mlavu_query("INSERT INTO `poslavu_REFERRER_db`.`logged_in` (`username`, `token`,`valid`) VALUES ('[1]', '[2]', '1')", $un, $token);
		if(mlavu_dberror()){
			return '{"status":"error","message":"'.mlavu_dberror().'"}';
		}else{
			setcookie("poslavu_referrer_login",$token, time()+3600);
			return '{"status":"success","message":"logged_in"}';
		}
	}
	function save_referral($args){
		$arr = array();
		$arr['referree_id']	  	= $args['referree_id'];
		$arr['primary_contact'] = $args['primary_contact'];
		$arr['company_name']	= $args['company_name'];
		unset($args['referree_id']);
		unset($args['primary_contact']);
		unset($args['company_name']);
		$arr['other_info']= LavuJson::json_encode($args);

		$query = "INSERT INTO `poslavu_REFERRER_db`.`referrals` (`referree_id`,`primary_contact`,`company_name`,`other_info`,`referral_date`,`contacted`) VALUES ('[referree_id]','[primary_contact]','[company_name]','[other_info]',NOW(),'0')";
		//echo print_r($query,1);
		//echo print_r($arr,1);
		$q = mlavu_query($query, $arr);
		if(mlavu_dberror()){
			return '{"status":"error","message":"'.mlavu_dberror().'"}';
		}else
			return '{"status":"success","message":"Referral Created"}';
	}
	function get_past_referrals($args){
		$query = mlavu_query("SELECT `primary_contact`, `company_name`,`other_info`,`referral_date` FROM `poslavu_REFERRER_db`.`referrals` WHERE `referree_id`='[1]'", $args['referree_id']);
		$ret_arr = array();
		while($res = mysqli_fetch_assoc($query)){
			$ret_arr[] = $res;
		}
		return LavuJson::json_encode($ret_arr);

	}
		function get_purchased_referrals($args){
		$query = mlavu_query("SELECT `primary_contact`, `company_name`,`other_info`,`referral_date` FROM `poslavu_REFERRER_db`.`referrals` WHERE `referree_id`='[1]' and `contacted`='1'", $args['referree_id']);
		$ret_arr = array();
		while($res = mysqli_fetch_assoc($query)){
			$ret_arr[] = $res;
		}
		return LavuJson::json_encode($ret_arr);

	}
	function generate_token(){
		$characters = 'abcdefghijklmnopqrstuvwxyz0123456789';
		$string = '';
		$random_string_length= 7;
		for ($i = 0; $i < $random_string_length; $i++) {
			$string .= $characters[rand(0, strlen($characters) - 1)];
		}
		return $string;
	}
	function logout($args){
		$token = $_COOKIE['poslavu_referrer_login'];
		//echo print_r($token,1);
		$query = mlavu_query("SELECT * FROM `poslavu_REFERRER_db`.`logged_in` where `token` ='[1]' and `valid`='1'", $token);
		if(mysqli_num_rows($query)){
			mlavu_query("UPDATE `poslavu_REFERRER_db`.`logged_in` set `valid`='0' where `token`='[1]'", $token);
		}else{
			return '{"status":"error","message":"No token found"}';
		}
		if(mlavu_dberror()){
			return '{"status":"error","message":"'.mlavu_dberror().'"}';
		}else
			return '{"status":"success","message":"Referral Created"}';
	}

?>