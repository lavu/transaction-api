<?php
	
	ini_set("display_errors",1);
	require_once($_SERVER['DOCUMENT_ROOT']."/cp/resources/lavuquery.php");
	function check_login(){
		if(isset($_COOKIE['poslavu_referrer_login'])){
			$token = $_COOKIE['poslavu_referrer_login'];
			$query = mlavu_query("SELECT * FROM `poslavu_REFERRER_db`.`logged_in` where `token` ='[1]' and `valid`='1'", $token);
			if(mysqli_num_rows($query))
				header("Location:https://admin.poslavu.com/referrer/mobile/index.php");
		}
	}
	check_login();
?>

<script type ='text/javascript' src= './login.js'></script>
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
<link rel="stylesheet" type="text/css" href="./login.css">
<div class = 'referrer_sign_in'>
	<form name ='sign_in_form'>
		<table class = 'sign_in_table'>
			<tr>
				<th colspan='2' style='font-family:sans-serif; font-size:70pt; font-weight:normal; color:white'>
					Lavu Referrer Sign in
				</th>
			</tr>
			<tr>
				<td  class='input_container'>
					<input type ='text' name ='username' placeholder='LavuTag/Email'/>
				</td>
			</tr>
			<tr>
				<td class='input_container'><input type='password' name='password' placeholder='Password'/> </td>
			</tr>
			<tr><td class='error_div' colspan ='2'></td></tr>
			<tr>
			
				<td colspan=2><input type='button' style='float:right' value = 'Login' onclick='referrer_login()'/></td>
			</tr>
			<tr>
				<td style='text-align:center' colspan ='2'>
					<div class ='lavu_referrer_signup_btn' onclick='show_signup_form()'>Sign up to be a Lavu referrer.</div>
				</td>
			</tr>
		</table>
	</form>
</div>

<div class ='referrer_sign_up_container'>
	<form name ='sign_up_form'>
		<table class = 'sign_up_table'>
			<tr>
				<th colspan='2'  style='font-family:sans-serif; font-size:70pt; font-weight:normal;color:white;'>
					Lavu Referrer Sign up
				</th>
			</tr>
			<tr>

				<td>
					<input type ='text' name ='f_name' onblur = 'not_null(this)' placeholder='First Name' valid ='0'/>
				</td>
			</tr><tr>
				<td>
					<input type ='text' name ='l_name' onblur = 'not_null(this)' valid ='0' placeholder='Last Name'/>
				</td>
			</tr>
			<tr>
				<td>
					<input type ='text' name ='phone' onblur = 'validate_phone(this)' valid ='0' placeholder='Phone Number' />
				</td>
			</tr>
			<tr>

				<td>
					<input type ='text' name ='lavu_tag' onblur = 'validate_tag(this)' valid ='0' placeholder='Lavu Tag (Username)' />
				</td>
			</tr>
			<tr>
				<td>
					<input type ='text' name ='email' onblur = 'validate_email(this)' valid ='0' placeholder='Email'/>
				</td>
			</tr>
			<tr>
				<td><input type='password' name='password' class='pass1' onblur = 'validate_password(this)' valid ='0' placeholder='Password'/> </td>
			</tr>
			<tr>
				<td><input type='password' name='password2' class='pass2'onblur = 'validate_password(this)' valid ='0' placeholder='Re-enter Passwor'> </td>
			</tr>
			<tr>
			
				<td colspan=2><input type='button' value='Submit' id='submit_signup' style='float:right' onclick ='referrer_signup()' disabled/></td>
			</tr>
			<tr>
				<td style='text-align:center' colspan ='2'>
					<div class ='lavu_referrer_signup_btn' onclick='show_signup_form()'>Back to login.</div>
				</td>
			</tr>
		</table>
	</form>
</div>