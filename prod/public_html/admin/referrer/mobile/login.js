var URL = './referrer_data.php';
function test(){
	alert('success');
}
function show_signup_form(){
	if(	$(".referrer_sign_in").css("display")=='block'){
		$(".referrer_sign_in").fadeOut("slow", function(){
			$(".referrer_sign_up_container").fadeIn("slow", function(){});
		});
	}else{
		$(".referrer_sign_up_container").fadeOut("slow", function(){
			$(".referrer_sign_in").fadeIn("slow", function(){});
		});
	}
}
function referrer_login(){
	var table= document.getElementsByClassName('sign_in_table')[0];
	var vals = new Array();
	var input = table.getElementsByTagName( 'input' ); 
    for ( var z = 0; z < input.length; z++ ) { 
        if( input[z].getAttribute("type")!='button')
        	vals[z] = input[z].getAttribute("name")+"="+input[z].value;
        	//vals[z]=  {name: input[z].getAttribute("name"), value:input[z].value};
    } 
    var stringified = "function=login&"+vals.join("&");
   	var res = perform_ajax(URL,stringified, false, "POST");
   	if(res.status =='success')
   		window.location = "https://admin.poslavu.com/referrer/index.php";
   	else
   		document.getElementsByClassName("error_div")[0].innerHTML = res.message;
}
function referrer_signup(){
	var table = document.getElementsByClassName('sign_up_table')[0];
	var vals  = new Array();
	var input = table.getElementsByTagName( 'input' ); 
    for ( var z = 0; z < input.length; z++ ) { 
       if( input[z].getAttribute("type")!='button')
       		vals[z] = input[z].getAttribute("name")+"="+input[z].value;
       		//vals[z] =  {name: input[z].getAttribute("name"), value:input[z].value};
    }
    var stringified = "function=save_referrer&"+vals.join("&");
   	var res = perform_ajax(URL,stringified, false, "POST");
   	if(res.status =='success')
   		alert("Account Created");
   	else
   		alert(res.message);
}
function not_null(elem){
    if(elem.value.match(/^[A-Za-z]+$/)){
    	remove_error(elem);
    }else{
    	create_error(elem, 'Field is invalid');
    }
}
function validate_email(elem){
 	var val = elem.value;
 	var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    if(re.test(val)){
    	var res= perform_ajax(URL, "function=check_unused_email&email="+val, false, "POST");
		if(res.status=='success'){
			remove_error(elem);
		}else{
			create_error(elem,"Email already registered");
		}
    }else{
    	create_error(elem,"Invalid Email");
    }
}
function validate_phone(elem){

	var	val = elem.value.replace(/[^0-9]/g, '');
	if(val.length != 10) 
		create_error(elem,"Phone number may only contain <br> numbers, and must be 10 digits");
	else
		remove_error(elem);
}
function validate_tag(elem){
	var val = elem.value;
	if(/[a-zA-Z0-9]/.test(val)){
    	var res= perform_ajax(URL, "function=check_unused_lavutag&tag="+val, false, "POST");
		if(res.status=='success'){
			remove_error(elem);
		}else{
			create_error(elem, "LavuTag already taken.");
		}
	}else{
		create_error(elem,"LavuTag may only contain <br> letters and numbers");
	}
}
function validate_password(elem){
	var parent_node = elem.parentNode;
	var val = elem.value;
	if(val && val.length > 5){
		if(elem.getAttribute('name')=='password2'){
			var p1 = document.getElementsByClassName("pass1")[0];
			
			if( p1.value == val){
				remove_error(p1);
				remove_error(elem);
			}else
				create_error(elem,"Passwords do not match");
		}else{
			var p2 = document.getElementsByClassName("pass2")[0];
			if(p2.value && p2.value.length && p2.value == val ){
				remove_error(p2);
				remove_error(elem);
			}else{
				if( p2.value.length != 0)
					create_error(elem, "Passwords do not match.");
			}
		}
	}else{
		create_error(elem, "Password must be greater than 6 or equal to 6 characters");
	}
}
function remove_error(elem){
	var parent_node = elem.parentNode;
	if(parent_node.getElementsByClassName("error_div").length)
    	parent_node.removeChild(parent_node.getElementsByClassName("error_div")[0]);
    elem.setAttribute("valid","1");
    var inputs = document.getElementsByTagName("input");
   	var valid =true;
    for( var i = 0; i < inputs.length; i++ ){
    	if(inputs[i].getAttribute("valid") == 0)
    		valid = false;
    }
    if(valid)
    	document.getElementById("submit_signup").removeAttribute("disabled");
}
function create_error(elem, message){
	if(elem.parentNode.getElementsByClassName("error_div").length){
		var error_div=elem.parentNode.getElementsByClassName("error_div")[0];
			error_div.innerHTML =message;
			elem.setAttribute("valid","0"); 
	}else{
		var error_div = document.createElement("div");
			error_div.className ='error_div';
			error_div.innerHTML = message;
		elem.parentNode.appendChild(error_div);
		elem.setAttribute("valid","0"); 
	}
}
function perform_ajax(URL, urlString,asyncSetting,type ){
	var returnVal='';
	$.ajax({
		url: URL,
		type: type,
		data:urlString,
		async:asyncSetting,
		success: function (string){
			try{
				returnVal = JSON.parse(string);
			}catch(err){
				console.log(err);
				returnVal = string;
			}
		}
	});
	return returnVal;
}

