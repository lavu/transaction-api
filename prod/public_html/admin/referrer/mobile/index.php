<?php
	ini_set("display_errors",1);
	require_once($_SERVER['DOCUMENT_ROOT']."/cp/resources/lavuquery.php");
	require_once($_SERVER['DOCUMENT_ROOT']."/cp/resources/json.php");
	$user_info = array();
	function check_login(){
		if(isset($_COOKIE['poslavu_referrer_login'])){
			$token = $_COOKIE['poslavu_referrer_login'];
			$query = mlavu_query("SELECT * FROM `poslavu_REFERRER_db`.`logged_in` where `token` ='[1]' and `valid`='1'", $token);
			if(!mysqli_num_rows($query)){
				header("Location:https://admin.poslavu.com/referrer/mobile/login.php");
			}
			else{
				$res = mysqli_fetch_assoc($query);
				get_user_info($res['username']);
			}
		}else
			header("Location:https://admin.poslavu.com/referrer/mobile/login.php");
	}
	function get_user_info($username){
		global $user_info;
		$user_info = mysqli_fetch_assoc(mlavu_query("SELECT `id`, `first_name`,`last_name`, `lavu_tag`, `phone`,`email` FROM `poslavu_REFERRER_db`.`referrers` where `lavu_tag`='[1]' limit 1", $username));
		//build_js_user_obj($user_info);
	}
	function build_js_user_obj(){
		global $user_info;
		return LavuJson::json_encode($user_info);
	}
	check_login();
?>
<html>
	<head>
		<title>Lavu Referrers</title>
		<script type='text/javascript'> 
			var user_info = <?php echo build_js_user_obj(); ?>
		</script>
		<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
		<script type='text/javascript' src ='index.js'> </script>
		<link rel="stylesheet" type="text/css" href="./index.css">
	</head>
	<body>
		<div class ='header'>
			Lavu Referral Program
			<!--<span class ='logout'>Logout</span>
			<span class ='my_account_btn'>My Account</span>	-->	
		</div>
		<div class ='body'>
			<div class = 'new_referral_container'>
				<div class ='opt_btn' name='add_referral'>New Referral</div>
				<div class = 'tab_container add_referral'>
				<table>
					<thead><!--
						<tr>
							<th colspan =2>Create New Referral</th>
						</tr> -->
					</thead>
					<tbody class = 'add_referral_form'></tbody>
					<tfoot>
						<tr>
							<td class='edit_user_options'>
								<input type='button' value= 'Submit Referral' class= 'submit_ref long_inputs'/> 
							
							</td>
						</tr>
						<tr>
							<td>
								* Required Field
							</td>
						</tr>
					</tfoot>
				</table>
			</div>
			</div>
			<div class = 'pending_referrals_container'>
				<div class ='opt_btn' name='past_referrals_container'>Pending Referrals</div>
				<div class = 'tab_container past_referrals_container'>
					<table>
						<thead>
						</thead>
						<tbody class = 'past_referrals'>
							
						</tbody>
						<tfoot>
							<!--<tr>
								<td class='edit_user_options'>
									<input type='button' value= 'Submit Referral' class= 'submit_ref'/> 
								
								</td>
							</tr>-->
							
						</tfoot>
					</table>
				</div>
			</div>
			<div class ='purchased_referrals_container_container'>
				<div class ='opt_btn' name='purchased_referrals_container'>Purchased Referrals</div>
				<div class = 'tab_container purchased_referrals_container'>
					<table style='width:100%'>
						<thead>
						</thead>
						<tbody class = 'purchased_referrals'></tbody>
						<tfoot>
						</tfoot>
					</table>
				</div>
			</div>
			<div class='my_account_btn_container'>
				<span class ='opt_btn my_account_btn' name = 'my_account'>My Account</span>	
				<div class = 'tab_container my_account' style='display:none'>
					<table style='width:100%'>
						<thead>

						</thead>
						<tbody>
							<tr>
								<td class = 'user_info_holder long_inputs' name ='first_name' placeholder='First Name'></td>
							</tr>
							<tr>
								<td class= 'user_info_holder long_inputs' name ='last_name' placeholder='LastName'></td>
							</tr>
							<tr>
								<td class= 'user_info_holder long_inputs' name ='phone' placeholder='Phone Number'></td>
							</tr>
							<tr>
								<td class= 'user_info_holder long_inputs' name ='email' placeholder='Email'></td>
							</tr>
							<tr>
								<td class= 'user_info_holder long_inputs' name = 'lavu_tag'></td>
								<input type='hidden' class='user_info_holder' name = 'id'/>
							</tr>
						</tbody>
						<tfoot>
							<tr>
								<td class='edit_user_options'>
									<input type='button' value= 'Edit User Info' class= 'edit_info long_inputs'/> 
								</td>
							</tr>
						</tfoot>
					</table>
				</div>
			</div>
			<div class ='logout_btn_contianer'>
				<span class ='opt_btn logout' name ='logout'>Logout</span>
			</div>
		</div>
	</body>
</html>