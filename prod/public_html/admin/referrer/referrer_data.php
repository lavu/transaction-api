<?php
	ini_set("display_errors",1);
	require_once($_SERVER['DOCUMENT_ROOT']."/cp/resources/json.php");
	if ( isset($_POST['function'])){
		$allowed_functions = array("check_unused_email","check_unused_lavutag","update_referral","save_referrer","update_referrer","login","perform_login","save_referral","get_past_referrals","get_referral","get_purchased_referral","get_purchased_referrals","generate_token","forgot_password","update_password","logout","get_states","get_countries");
		if(in_array($_POST['function'],$allowed_functions))
		{
			if (function_exists($_POST['function'])){
				require_once($_SERVER['DOCUMENT_ROOT']."/cp/resources/lavuquery.php");
				$fun_name = $_POST['function'];	
				unset($_POST['function']);
				echo $fun_name($_POST);
			}else
				echo "error:no function of name ". $_POST['function'];
		}else
			echo "error:function not allowed " . $_POST['function'];
	}else
		echo "error: no function name given.";

	function check_unused_email($args){
		if(isset($args['email'])){
			$query = mlavu_query("SELECT * FROM `poslavu_REFERRER_db`.`referrers` where `email`='[1]' AND `_deleted`='0' limit 1", $args['email']);
			if(mysqli_num_rows($query)){
				return '{"status":"error", "message":"Email already registered"}';
			}else{
				return '{"status":"success", "message": "unused email"}';
			}
		}else{
			return '{"status":"error", "message":"no email sent"}';
		}
	}
	function check_unused_lavutag($args){
		if(isset($args['tag'])){
			$query = mlavu_query("SELECT * FROM `poslavu_REFERRER_db`.`referrers` where `lavu_tag`='[1]' AND `_deleted`='0' limit 1", $args['tag']);
			if(mysqli_num_rows($query))
				return '{"status":"error", "message": "lavu tag already registered"}';
			else
				return '{"status":"success", "message": "unused tag"}';
		}else{
			return '{"status":"error", "message": "no tag sent"}';
		}
	}
	function update_referral($args){
		$required = LavuJson::json_decode(urldecode($args['required_fields']));
		$required['other_info']=urldecode($args['opt_fields']);

		$update_arr = array();
		foreach( $required as $col=>$val){
			if($col!='referral_id' )
			$update_arr[]= "`".$col."` = '[".$col."]'";
		}
		$query = "UPDATE `poslavu_REFERRER_db`.`referrals` SET ".implode(",", $update_arr)." WHERE  `id`='[referral_id]'";
		$q = mlavu_query($query, $required);
		if(mlavu_dberror()){
			return '{"status":"error","message":"'.mlavu_dberror().'"}';
		}else
			return '{"status":"success","message":"Referral Created"}';
	}
	function save_referrer($args){
		$unused_tag_str   = check_unused_lavutag(array("tag"=>$args['lavu_tag']));
		$unused_email_str = check_unused_email(array("email"=>$args['email']));
		
		$unused_lavu_tag = LavuJson::json_decode($unused_tag_str);
		$unused_email    = LavuJson::json_decode($unused_email_str);

		if( ($args['password'] == $args['password2']) && $args['password']!='' && $unused_lavu_tag['status']=='success' && $unused_email['status']=='success'){
			mlavu_query("INSERT INTO `poslavu_REFERRER_db`.`referrers` (`lavu_tag`, `email`,`first_name`, `last_name`,`phone`, `password`, `_deleted`) VALUES ('[1]','[2]','[3]','[4]','[5]',PASSWORD('[6]'),'0')",$args['lavu_tag'],$args['email'],$args['f_name'], $args['l_name'], $args['phone'],$args['password']);
			if( mlavu_dberror())
				return '{"status":"error","message":"'.mlavu_dberror().'"}';
			else
				return '{"status":"success", "message":"success"}';
		}else if( $unused_lavu_tag['status'] == 'error'){
			return $unused_tag_str;
		}else if($unused_email['status'] == 'error'){
			return $unused_email_str;
		}else 
			return '{"status":"error","message":"uncaught gayness"}';
	}
	function update_referrer($args){
		$q  = mlavu_query("SELECT * FROM `poslavu_REFERRER_db`.`referrers` WHERE `email`='[1]' and `id`='[2]' ", $args['email'], $args['id']);
		$q2 = mlavu_query("SELECT * FROM `poslavu_REFERRER_db`.`referrers` WHERE `email`='[1]'", $args['email']);
		if(!mysqli_num_rows($q) && mysqli_num_rows($q2)){
			return '{"status":"error","message":"Used Email"}';
		}else{
			$query_str='UPDATE `poslavu_REFERRER_db`.`referrers` SET ';
			$update_arr= array();
			foreach($args as $key=>$value){
				if($value!='' && $key !='id'){
					$update_arr[] = "`$key`='[$key]'";
				}
			}
			$query_str.= implode(",", $update_arr). "WHERE `id`='[id]' limit 1";
			mlavu_query($query_str, $args);

		}
	}
	function login($args){
		$un = $args['username'];
		$pw = $args['password'];
		$query = mlavu_query("SELECT * FROM `poslavu_REFERRER_db`.`referrers` WHERE (`lavu_tag`='[1]' AND `password` = PASSWORD('[2]') ) OR (`email`='[1]' AND `password` = PASSWORD('[2]')) limit 1", $un, $pw);
		if(mysqli_num_rows($query)){
		$query = mlavu_query("SELECT * FROM `poslavu_REFERRER_db`.`logged_in` WHERE `username` = '[1]' AND `valid`='1'", $un);
			if(mysqli_num_rows($query)){
				if(isset($_COOKIE['poslavu_referrer_login'])){
					$token = $_COOKIE['poslavu_referrer_login'];
					$query2 = mlavu_query("SELECT * FROM `poslavu_REFERRER_db`.`logged_in` WHERE `username` = '[1]' AND `token`='[2]' AND `valid`='1'", $un, $token);
					if( mysqli_num_rows($query2))
						return '{"status":"success","message":"prev_login"}';
					else{
						return perform_login($un);
						//return '{"status":"error","message":"invalid token"}';
					}
				}else
					return perform_login($un);
			}else
				return perform_login($un);
			
		}else
			return '{"status":"error", "message":"Username/Password combination invalid."}';
	}
	function perform_login($un){
	
		$token = generate_token();
		$query = mlavu_query("INSERT INTO `poslavu_REFERRER_db`.`logged_in` (`username`, `token`,`valid`) VALUES ('[1]', '[2]', '1')", $un, $token);
		if(mlavu_dberror()){
			return '{"status":"error","message":"'.mlavu_dberror().'"}';
		}else{
			setcookie("poslavu_referrer_login",$token, time()+3600);
			return '{"status":"success","message":"logged_in"}';
		}
	}
	function save_referral($args){
		$required = LavuJson::json_decode(urldecode($args['required_fields']));
		$required['other_info']=urldecode($args['opt_fields']);

		$cols_arr = array();
		$vals_arr = array();
		foreach( $required as $col=>$val){
			$cols_arr[]="`".$col."`";
			$vals_arr[]="'[".$col."]'";
		}
		$query = "INSERT INTO `poslavu_REFERRER_db`.`referrals` (".implode(",", $cols_arr)." ,`referral_date`,`contacted`) VALUES (".implode(",",$vals_arr).",NOW(),'0')";
		$q = mlavu_query($query, $required);
		if(mlavu_dberror()){
			return '{"status":"error","message":"'.mlavu_dberror().'"}';
		}else
			return '{"status":"success","message":"Referral Created"}';
	}
	function get_past_referrals($args){
		$query = mlavu_query("SELECT `id`,concat (`f_name`,' ',`l_name`) as 'name', `company_name`,`email`, `phone` as 'phone_number',`referral_date` as 'date' FROM `poslavu_REFERRER_db`.`referrals` WHERE `referree_id`='[1]' and `contacted`='0'", $args['referree_id']);
		$ret_arr = array();
		while($res = mysqli_fetch_assoc($query)){
			$ret_arr[] = $res;
		}
		return LavuJson::json_encode($ret_arr);

	}
	function get_referral($args){
		$res = mysqli_fetch_assoc(mlavu_query("SELECT `id`,`f_name`,`l_name` , `company_name`,`email`, `phone`,`other_info` FROM `poslavu_REFERRER_db`.`referrals` WHERE `referree_id`='[1]' and `id`='[2]' LIMIT 1", $args['referree_id'], $args['id']));

		return LavuJson::json_encode($res);
	}
	function get_purchased_referral($args){
		$res = mysqli_fetch_assoc(mlavu_query("SELECT `id`,`f_name`,`l_name` , `company_name`,`email`, `phone`,`other_info` FROM `poslavu_REFERRER_db`.`referrals` WHERE `referree_id`='[1]' and `id`='[2]' and `contacted`='1' LIMIT 1", $args['referree_id'], $args['id']));
		return LavuJson::json_encode($res);
	}
	function get_purchased_referrals($args){
		$query = mlavu_query("SELECT `id`,concat (`f_name`,' ',`l_name`) as 'name', `company_name`,`email`, `phone` as 'phone_number',`referral_date` as 'date' FROM `poslavu_REFERRER_db`.`referrals` WHERE `referree_id`='[1]' and `contacted`='1'", $args['referree_id']);
		$ret_arr = array();
		while($res = mysqli_fetch_assoc($query)){
			$ret_arr[] = $res;
		}
		return LavuJson::json_encode($ret_arr);
	}
	function generate_token($num_chars =false){
		$characters = 'abcdefghijklmnopqrstuvwxyz0123456789';
		$string = '';
		if( $num_chars)
			$random_string_length=$num_chars;
		else
			$random_string_length= 7;

		for ($i = 0; $i < $random_string_length; $i++) {
			$string .= $characters[rand(0, strlen($characters) - 1)];
		}
		return $string;
	}
	function forgot_password($args){
		
		$email= $args["email"];
		$token = generate_token(15);
		if( mysqli_num_rows(mlavu_query("SELECT * FROM `poslavu_REFERRER_db`.`referrers` WHERE `email`='[1]'", $email))){
			mlavu_query("INSERT INTO `poslavu_REFERRER_db`.`forgot_password` (`token`, `valid`,`email`,`used`) VALUES ('[1]','1', '[2]','0') ", $token ,$email);
			mail($email, "Lavu Referrer Password", "Thank you for contacting the Lavu Referrer program to reset your password. Click the link to reset your password http://admin.poslavu.com/referrer/forgot_password.php?token=$token");
			return '{"status":"success","message":"Email Sent"}';
		}else{
			return '{"status":"error","message":"Invalid Email Address"}';
		}
	}
	function update_password($args){
		$pass  = $args['pass'];
		$token = $args['token'];
		$query = mlavu_query("SELECT * FROM `poslavu_REFERRER_db`.`forgot_password` WHERE `token`='[1]' AND `used`='0'", $token);
		if(mysqli_num_rows($query)){
			$res = mysqli_fetch_assoc($query);
			mlavu_query("UPDATE `poslavu_REFERRER_db`.`referrers` set `password`=PASSWORD('[1]') where `email`='[2]'",$pass, $res['email']);
			mlavu_query("UPDATE `poslavu_REFERRER_db`.`forgot_password` set `used`='1' where `token`='[1]'", $token);
		}else{
			return '{"status":"error","message":"Token already used"}';
		}
	}
	function logout($args){
		$token = $_COOKIE['poslavu_referrer_login'];
		//echo print_r($token,1);
		$query = mlavu_query("SELECT * FROM `poslavu_REFERRER_db`.`logged_in` where `token` ='[1]' and `valid`='1'", $token);
		if(mysqli_num_rows($query)){
			mlavu_query("UPDATE `poslavu_REFERRER_db`.`logged_in` set `valid`='0' where `token`='[1]'", $token);
		}else{
			return '{"status":"error","message":"No token found"}';
		}
		if(mlavu_dberror()){
			return '{"status":"error","message":"'.mlavu_dberror().'"}';
		}else
			return '{"status":"success","message":"Referral Created"}';
	}
	function get_states(){
		return '[{"name":"Alabama","code":"AL"},{"name":"Alaska","code":"AK"},{"name":"AmericanSamoa","code":"AS"},{"name":"Arizona","code":"AZ"},{"name":"Arkansas","code":"AR"},{"name":"California","code":"CA"},{"name":"Colorado","code":"CO"},{"name":"Connecticut","code":"CT"},{"name":"Delaware","code":"DE"},{"name":"DistrictOfColumbia","code":"DC"},{"name":"FederatedStatesOfMicronesia","code":"FM"},{"name":"Florida","code":"FL"},{"name":"Georgia","code":"GA"},{"name":"Guam","code":"GU"},{"name":"Hawaii","code":"HI"},{"name":"Idaho","code":"ID"},{"name":"Illinois","code":"IL"},{"name":"Indiana","code":"IN"},{"name":"Iowa","code":"IA"},{"name":"Kansas","code":"KS"},{"name":"Kentucky","code":"KY"},{"name":"Louisiana","code":"LA"},{"name":"Maine","code":"ME"},{"name":"MarshallIslands","code":"MH"},{"name":"Maryland","code":"MD"},{"name":"Massachusetts","code":"MA"},{"name":"Michigan","code":"MI"},{"name":"Minnesota","code":"MN"},{"name":"Mississippi","code":"MS"},{"name":"Missouri","code":"MO"},{"name":"Montana","code":"MT"},{"name":"Nebraska","code":"NE"},{"name":"Nevada","code":"NV"},{"name":"NewHampshire","code":"NH"},{"name":"NewJersey","code":"NJ"},{"name":"NewMexico","code":"NM"},{"name":"NewYork","code":"NY"},{"name":"NorthCarolina","code":"NC"},{"name":"NorthDakota","code":"ND"},{"name":"NorthernMarianaIslands","code":"MP"},{"name":"Ohio","code":"OH"},{"name":"Oklahoma","code":"OK"},{"name":"Oregon","code":"OR"},{"name":"Palau","code":"PW"},{"name":"Pennsylvania","code":"PA"},{"name":"PuertoRico","code":"PR"},{"name":"RhodeIsland","code":"RI"},{"name":"SouthCarolina","code":"SC"},{"name":"SouthDakota","code":"SD"},{"name":"Tennessee","code":"TN"},{"name":"Texas","code":"TX"},{"name":"Utah","code":"UT"},{"name":"Vermont","code":"VT"},{"name":"VirginIslands","code":"VI"},{"name":"Virginia","code":"VA"},{"name":"Washington","code":"WA"},{"name":"WestVirginia","code":"WV"},{"name":"Wisconsin","code":"WI"},{"name":"Wyoming","code":"WY"}]';
	}
	function get_countries(){
		return '[ {"name": "Afghanistan", "code": "AF"}, {"name": "Åland Islands", "code": "AX"}, {"name": "Albania", "code": "AL"}, {"name": "Algeria", "code": "DZ"}, {"name": "American Samoa", "code": "AS"}, {"name": "AndorrA", "code": "AD"}, {"name": "Angola", "code": "AO"}, {"name": "Anguilla", "code": "AI"}, {"name": "Antarctica", "code": "AQ"}, {"name": "Antigua and Barbuda", "code": "AG"}, {"name": "Argentina", "code": "AR"}, {"name": "Armenia", "code": "AM"}, {"name": "Aruba", "code": "AW"}, {"name": "Australia", "code": "AU"}, {"name": "Austria", "code": "AT"}, {"name": "Azerbaijan", "code": "AZ"}, {"name": "Bahamas", "code": "BS"}, {"name": "Bahrain", "code": "BH"}, {"name": "Bangladesh", "code": "BD"}, {"name": "Barbados", "code": "BB"}, {"name": "Belarus", "code": "BY"}, {"name": "Belgium", "code": "BE"}, {"name": "Belize", "code": "BZ"}, {"name": "Benin", "code": "BJ"}, {"name": "Bermuda", "code": "BM"}, {"name": "Bhutan", "code": "BT"}, {"name": "Bolivia", "code": "BO"}, {"name": "Bosnia and Herzegovina", "code": "BA"}, {"name": "Botswana", "code": "BW"}, {"name": "Bouvet Island", "code": "BV"}, {"name": "Brazil", "code": "BR"}, {"name": "British Indian Ocean Territory", "code": "IO"}, {"name": "Brunei Darussalam", "code": "BN"}, {"name": "Bulgaria", "code": "BG"}, {"name": "Burkina Faso", "code": "BF"}, {"name": "Burundi", "code": "BI"}, {"name": "Cambodia", "code": "KH"}, {"name": "Cameroon", "code": "CM"}, {"name": "Canada", "code": "CA"}, {"name": "Cape Verde", "code": "CV"}, {"name": "Cayman Islands", "code": "KY"}, {"name": "Central African Republic", "code": "CF"}, {"name": "Chad", "code": "TD"}, {"name": "Chile", "code": "CL"}, {"name": "China", "code": "CN"}, {"name": "Christmas Island", "code": "CX"}, {"name": "Cocos (Keeling) Islands", "code": "CC"}, {"name": "Colombia", "code": "CO"}, {"name": "Comoros", "code": "KM"}, {"name": "Congo", "code": "CG"}, {"name": "Congo, The Democratic Republic of the", "code": "CD"}, {"name": "Cook Islands", "code": "CK"}, {"name": "Costa Rica", "code": "CR"}, {"name": "Cote D\"Ivoire", "code": "CI"}, {"name": "Croatia", "code": "HR"}, {"name": "Cuba", "code": "CU"}, {"name": "Cyprus", "code": "CY"}, {"name": "Czech Republic", "code": "CZ"}, {"name": "Denmark", "code": "DK"}, {"name": "Djibouti", "code": "DJ"}, {"name": "Dominica", "code": "DM"}, {"name": "Dominican Republic", "code": "DO"}, {"name": "Ecuador", "code": "EC"}, {"name": "Egypt", "code": "EG"}, {"name": "El Salvador", "code": "SV"}, {"name": "Equatorial Guinea", "code": "GQ"}, {"name": "Eritrea", "code": "ER"}, {"name": "Estonia", "code": "EE"}, {"name": "Ethiopia", "code": "ET"}, {"name": "Falkland Islands (Malvinas)", "code": "FK"}, {"name": "Faroe Islands", "code": "FO"}, {"name": "Fiji", "code": "FJ"}, {"name": "Finland", "code": "FI"}, {"name": "France", "code": "FR"}, {"name": "French Guiana", "code": "GF"}, {"name": "French Polynesia", "code": "PF"}, {"name": "French Southern Territories", "code": "TF"}, {"name": "Gabon", "code": "GA"}, {"name": "Gambia", "code": "GM"}, {"name": "Georgia", "code": "GE"}, {"name": "Germany", "code": "DE"}, {"name": "Ghana", "code": "GH"}, {"name": "Gibraltar", "code": "GI"}, {"name": "Greece", "code": "GR"}, {"name": "Greenland", "code": "GL"}, {"name": "Grenada", "code": "GD"}, {"name": "Guadeloupe", "code": "GP"}, {"name": "Guam", "code": "GU"}, {"name": "Guatemala", "code": "GT"}, {"name": "Guernsey", "code": "GG"}, {"name": "Guinea", "code": "GN"}, {"name": "Guinea-Bissau", "code": "GW"}, {"name": "Guyana", "code": "GY"}, {"name": "Haiti", "code": "HT"}, {"name": "Heard Island and Mcdonald Islands", "code": "HM"}, {"name": "Holy See (Vatican City State)", "code": "VA"}, {"name": "Honduras", "code": "HN"}, {"name": "Hong Kong", "code": "HK"}, {"name": "Hungary", "code": "HU"}, {"name": "Iceland", "code": "IS"}, {"name": "India", "code": "IN"}, {"name": "Indonesia", "code": "ID"}, {"name": "Iran, Islamic Republic Of", "code": "IR"}, {"name": "Iraq", "code": "IQ"}, {"name": "Ireland", "code": "IE"}, {"name": "Isle of Man", "code": "IM"}, {"name": "Israel", "code": "IL"}, {"name": "Italy", "code": "IT"}, {"name": "Jamaica", "code": "JM"}, {"name": "Japan", "code": "JP"}, {"name": "Jersey", "code": "JE"}, {"name": "Jordan", "code": "JO"}, {"name": "Kazakhstan", "code": "KZ"}, {"name": "Kenya", "code": "KE"}, {"name": "Kiribati", "code": "KI"}, {"name": "Korea, Democratic People\"S Republic of", "code": "KP"}, {"name": "Korea, Republic of", "code": "KR"}, {"name": "Kuwait", "code": "KW"}, {"name": "Kyrgyzstan", "code": "KG"}, {"name": "Lao People\"S Democratic Republic", "code": "LA"}, {"name": "Latvia", "code": "LV"}, {"name": "Lebanon", "code": "LB"}, {"name": "Lesotho", "code": "LS"}, {"name": "Liberia", "code": "LR"}, {"name": "Libyan Arab Jamahiriya", "code": "LY"}, {"name": "Liechtenstein", "code": "LI"}, {"name": "Lithuania", "code": "LT"}, {"name": "Luxembourg", "code": "LU"}, {"name": "Macao", "code": "MO"}, {"name": "Macedonia, The Former Yugoslav Republic of", "code": "MK"}, {"name": "Madagascar", "code": "MG"}, {"name": "Malawi", "code": "MW"}, {"name": "Malaysia", "code": "MY"}, {"name": "Maldives", "code": "MV"}, {"name": "Mali", "code": "ML"}, {"name": "Malta", "code": "MT"}, {"name": "Marshall Islands", "code": "MH"}, {"name": "Martinique", "code": "MQ"}, {"name": "Mauritania", "code": "MR"}, {"name": "Mauritius", "code": "MU"}, {"name": "Mayotte", "code": "YT"}, {"name": "Mexico", "code": "MX"}, {"name": "Micronesia, Federated States of", "code": "FM"}, {"name": "Moldova, Republic of", "code": "MD"}, {"name": "Monaco", "code": "MC"}, {"name": "Mongolia", "code": "MN"}, {"name": "Montserrat", "code": "MS"}, {"name": "Morocco", "code": "MA"}, {"name": "Mozambique", "code": "MZ"}, {"name": "Myanmar", "code": "MM"}, {"name": "Namibia", "code": "NA"}, {"name": "Nauru", "code": "NR"}, {"name": "Nepal", "code": "NP"}, {"name": "Netherlands", "code": "NL"}, {"name": "Netherlands Antilles", "code": "AN"}, {"name": "New Caledonia", "code": "NC"}, {"name": "New Zealand", "code": "NZ"}, {"name": "Nicaragua", "code": "NI"}, {"name": "Niger", "code": "NE"}, {"name": "Nigeria", "code": "NG"}, {"name": "Niue", "code": "NU"}, {"name": "Norfolk Island", "code": "NF"}, {"name": "Northern Mariana Islands", "code": "MP"}, {"name": "Norway", "code": "NO"}, {"name": "Oman", "code": "OM"}, {"name": "Pakistan", "code": "PK"}, {"name": "Palau", "code": "PW"}, {"name": "Palestinian Territory, Occupied", "code": "PS"}, {"name": "Panama", "code": "PA"}, {"name": "Papua New Guinea", "code": "PG"}, {"name": "Paraguay", "code": "PY"}, {"name": "Peru", "code": "PE"}, {"name": "Philippines", "code": "PH"}, {"name": "Pitcairn", "code": "PN"}, {"name": "Poland", "code": "PL"}, {"name": "Portugal", "code": "PT"}, {"name": "Puerto Rico", "code": "PR"}, {"name": "Qatar", "code": "QA"}, {"name": "Reunion", "code": "RE"}, {"name": "Romania", "code": "RO"}, {"name": "Russian Federation", "code": "RU"}, {"name": "RWANDA", "code": "RW"}, {"name": "Saint Helena", "code": "SH"}, {"name": "Saint Kitts and Nevis", "code": "KN"}, {"name": "Saint Lucia", "code": "LC"}, {"name": "Saint Pierre and Miquelon", "code": "PM"}, {"name": "Saint Vincent and the Grenadines", "code": "VC"}, {"name": "Samoa", "code": "WS"}, {"name": "San Marino", "code": "SM"}, {"name": "Sao Tome and Principe", "code": "ST"}, {"name": "Saudi Arabia", "code": "SA"}, {"name": "Senegal", "code": "SN"}, {"name": "Serbia and Montenegro", "code": "CS"}, {"name": "Seychelles", "code": "SC"}, {"name": "Sierra Leone", "code": "SL"}, {"name": "Singapore", "code": "SG"}, {"name": "Slovakia", "code": "SK"}, {"name": "Slovenia", "code": "SI"}, {"name": "Solomon Islands", "code": "SB"}, {"name": "Somalia", "code": "SO"}, {"name": "South Africa", "code": "ZA"}, {"name": "South Georgia and the South Sandwich Islands", "code": "GS"}, {"name": "Spain", "code": "ES"}, {"name": "Sri Lanka", "code": "LK"}, {"name": "Sudan", "code": "SD"}, {"name": "Suriname", "code": "SR"}, {"name": "Svalbard and Jan Mayen", "code": "SJ"}, {"name": "Swaziland", "code": "SZ"}, {"name": "Sweden", "code": "SE"}, {"name": "Switzerland", "code": "CH"}, {"name": "Syrian Arab Republic", "code": "SY"}, {"name": "Taiwan, Province of China", "code": "TW"}, {"name": "Tajikistan", "code": "TJ"}, {"name": "Tanzania, United Republic of", "code": "TZ"}, {"name": "Thailand", "code": "TH"}, {"name": "Timor-Leste", "code": "TL"}, {"name": "Togo", "code": "TG"}, {"name": "Tokelau", "code": "TK"}, {"name": "Tonga", "code": "TO"}, {"name": "Trinidad and Tobago", "code": "TT"}, {"name": "Tunisia", "code": "TN"}, {"name": "Turkey", "code": "TR"}, {"name": "Turkmenistan", "code": "TM"}, {"name": "Turks and Caicos Islands", "code": "TC"}, {"name": "Tuvalu", "code": "TV"}, {"name": "Uganda", "code": "UG"}, {"name": "Ukraine", "code": "UA"}, {"name": "United Arab Emirates", "code": "AE"}, {"name": "United Kingdom", "code": "GB"}, {"name": "United States", "code": "US", "default":"true"}, {"name": "United States Minor Outlying Islands", "code": "UM"}, {"name": "Uruguay", "code": "UY"}, {"name": "Uzbekistan", "code": "UZ"}, {"name": "Vanuatu", "code": "VU"}, {"name": "Venezuela", "code": "VE"}, {"name": "Viet Nam", "code": "VN"}, {"name": "Virgin Islands, British", "code": "VG"}, {"name": "Virgin Islands, U.S.", "code": "VI"}, {"name": "Wallis and Futuna", "code": "WF"}, {"name": "Western Sahara", "code": "EH"}, {"name": "Yemen", "code": "YE"}, {"name": "Zambia", "code": "ZM"}, {"name": "Zimbabwe", "code": "ZW"} ]';
	}
?>