<?php
	ini_set("display_errors",1);
	require_once($_SERVER['DOCUMENT_ROOT']."/cp/resources/json.php");
	require_once($_SERVER['DOCUMENT_ROOT']."/cp/resources/lavuquery.php");
	function check_token(){
		if(!mysqli_num_rows(mlavu_query("SELECT * FROM `poslavu_REFERRER_db`.`forgot_password` where `token`='[1]' and `valid`='1'",$_GET['token']))){
			echo 'invalid token';
			exit();
			//header("http://admin.poslavu.com/referrer/login.php");
		}else{
			mlavu_query("UPDATE `poslavu_REFERRER_db`.`forgot_password` SET `valid`='0' WHERE `token`='[1]'", $_GET['token']);
			echo"<script>var token = '".$_GET['token']."';</script>";
		}
	}
	check_token();
?>
<html>
	<head>
		<title>Forgot lavu Referrer Password</title>
		<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
		<script type='text/javascript'>

			function update_password(){
				
				var p1 = document.getElementsByClassName("pass1")[0].value;
				var p2 = document.getElementsByClassName("pass2")[0].value;
				perform_ajax("./referrer_data.php","function=update_password&pass="+p1+"&token="+token, false, "POST");

			}
			function validate_password(elem){
				var parent_node = elem.parentNode;
				var val = elem.value;
				var p1 = document.getElementsByClassName("pass1")[0];
				var p2 = document.getElementsByClassName("pass2")[0];

				if(val && val.length > 5){
					if(elem.getAttribute('name')=='password2'){
						var p1 = document.getElementsByClassName("pass1")[0];
						
						if( p1.value == val){
							remove_error(p1);
							remove_error(elem);
						}else{
							create_error(elem,"Passwords do not match");
						}
					}else{
						var p2 = document.getElementsByClassName("pass2")[0];
						if(p2.value && p2.value.length && p2.value == val ){
							remove_error(p2);
							remove_error(elem);
						}else{
							if( p2.value.length != 0)
								create_error(elem, "Passwords do not match.");
						}
					}
				}else{
					if(elem.value.length > 0)
						create_error(elem, "Password must be greater than 6 or equal to 6 characters");
				}
			}
			function perform_ajax(URL, urlString,asyncSetting,type ){
				var returnVal='';
				$.ajax({
					url: URL,
					type: type,
					data:urlString,
					async:asyncSetting,
					success: function (string){
						try{
							returnVal = JSON.parse(string);
						}catch(err){
							console.log(err);
							returnVal = string;
						}
					}
				});
				return returnVal;
			}
			function create_error(elem, message){
				if(elem.parentNode.getElementsByClassName("error_div").length){
					var error_div=elem.parentNode.getElementsByClassName("error_div")[0];
						error_div.innerHTML =message;
						elem.setAttribute("valid","0"); 
				}else{
					var error_div = document.createElement("div");
						error_div.className ='error_div';
						error_div.innerHTML = message;
					elem.parentNode.appendChild(error_div);
					elem.setAttribute("valid","0"); 
				}
			}
			function remove_error(elem){
				var parent_node = elem.parentNode;
				if(parent_node.getElementsByClassName("error_div").length)
			    	parent_node.removeChild(parent_node.getElementsByClassName("error_div")[0]);
			    elem.setAttribute("valid","1");
			    var inputs = document.getElementsByTagName("input");
			   	var valid =true;
			    for( var i = 0; i < inputs.length; i++ ){
			    	if(inputs[i].getAttribute("valid") == 0)
			    		valid = false;
			    }
			    if(valid)
			    	document.getElementById("submit_signup").removeAttribute("disabled");
			}
		</script>
	</head>

	<body>
		<div class ='new_password_container'>
			<table>
				<tr>
					<th>Please enter a new password. </th>
				</tr>
				<tr>
					<td><input type='password' placeholder='Password' class= 'pass1' onblur= 'validate_password(this)'/></td>
				</tr>
				<tr>
					<td><input type='password' placeholder='Re-Password' class='pass2' onblur= 'validate_password(this)'/></td>
				</tr>
				<tr>
					<td><input type='button' value='submit' onclick = 'update_password(this)'></td>
				</tr>
			</table>
		</div>
	</body>
</html>