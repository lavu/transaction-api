<?php
	ini_set("display_errors",1);
	require_once($_SERVER['DOCUMENT_ROOT']."/cp/resources/lavuquery.php");
	require_once($_SERVER['DOCUMENT_ROOT']."/cp/resources/json.php");
	$user_info = array();
	function check_mobile(){
		$useragent=$_SERVER['HTTP_USER_AGENT'];
		if(preg_match('/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i',$useragent)||preg_match('/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i',substr($useragent,0,4)))
			return true;
		else
			return false;
	}
	function check_login(){
		if(isset($_COOKIE['poslavu_referrer_login'])){
			$token = $_COOKIE['poslavu_referrer_login'];
			$query = mlavu_query("SELECT * FROM `poslavu_REFERRER_db`.`logged_in` where `token` ='[1]' and `valid`='1'", $token);
			if(!mysqli_num_rows($query)){
				if( check_mobile())
					header("Location:https://admin.poslavu.com/referrer/mobile/login.php");
				else
					header("Location:https://admin.poslavu.com/referrer/login.php");	
			}
			else{
				$res = mysqli_fetch_assoc($query);
				get_user_info($res['username']);
			}
		}else{
			if(check_mobile())
				header("Location:https://admin.poslavu.com/referrer/mobile/login.php");
			else
				header("Location:https://admin.poslavu.com/referrer/login.php");	
		}
	}
	function get_user_info($username){
		global $user_info;
		$user_info = mysqli_fetch_assoc(mlavu_query("SELECT `id`, `first_name`,`last_name`, `lavu_tag`, `phone`,`email` FROM `poslavu_REFERRER_db`.`referrers` where `lavu_tag`='[1]' OR `email`='[1]'  limit 1", $username));
		//build_js_user_obj($user_info);
	}
	function build_js_user_obj(){
		global $user_info;
		return LavuJson::json_encode($user_info);
	}
	check_mobile();
	check_login();
?>
<html>
	<head>
		<title>Lavu Referrers</title>
		<script type='text/javascript'> 
			var user_info = <?php echo build_js_user_obj(); ?>
		</script>
		<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
		<script type='text/javascript' src ='index.js'> </script>
		<link rel="stylesheet" type="text/css" href="./index.css">
	</head>
	<body>
		<div class ='header'>
			<div class='header_stuff_container'>
				<div class='logo_img'></div>
				<span class='header_text'>REFERRAL PROGRAM</span>
				<span class ='logout'>Logout</span>
				<span class ='my_account_btn'>My Account</span>		
			</div>
		</div>
		<div class = 'tab_container my_account'>
			
			<table class = 'my_account_table'>
				<thead>
					<tr>
						<th colspan =2 style = 'font-weight:300; font-size:larger; padding-top:20px; padding-bottom:20px;'>User Info <div class='close_my_account' onclick = 'open_my_account()'>X </div></th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td class='title_holder'>First Name</td>
						<td class = 'user_info_holder ' name ='first_name'></td>
					</tr>
					<tr>
						<td class='title_holder'>Last Name</td>
						<td class= 'user_info_holder' name ='last_name'></td>
					</tr>
					<tr>
						<td class='title_holder'>Phone Number</td>
						<td class= 'user_info_holder' name ='phone'></td>
					</tr>
					<tr>
						<td class='title_holder'>Email</td>
						<td class= 'user_info_holder' name ='email'></td>
					</tr>
					<tr>
						<td class='title_holder'>LavuTag</td>
						<td class= 'user_info_holder' name = 'lavu_tag'></td>
						<input type='hidden' class='user_info_holder' name = 'id'/>
					</tr>
				</tbody>
				<tfoot>
					<tr>
						<td class='edit_user_options'>
							<input type='button' value= 'Edit User Info' class= 'edit_info' style = 'margin-bottom: 20px;'/> 
						</td>
					</tr>
				</tfoot>
			</table>
		</div>
		<div class ='btn_container'>
			<div class ='opt_btn opt_btn_left' name='add_referral'>NEW REFERRAL</div>
			<div class ='opt_btn' name='past_referrals_container'>PENDING REFERRALS</div>
			<div class ='opt_btn opt_btn_right' name='purchased_referrals_container'>PURCHASED REFERRALS</div>
		</div>
		<div class ='body'>
			<div class = 'tab_container add_referral'>
				<table style='margin:0 auto; width:100%'>
					<thead>
						<tr>
							<th style='font-size:15pt; font-weight:100; font-family:arial, sans-serif;padding-bottom:20px;padding-top:20px;'>CREATE NEW REFERRAL</th>
							<th style='font-size:7pt; font-weight:100; font-family:arial, sans-serif;'>* REQUIRED FIELD</th>
						</tr>
					</thead>
					<tbody class = 'add_referral_form'></tbody>
					<tfoot>
						<tr>
							<td class='edit_user_options'>
								<input type='button' value= 'Submit Referral' class= 'submit_ref'/> 
							
							</td>
						</tr>
					</tfoot>
				</table>
			</div>
			<div class = 'tab_container past_referrals_container'>
				<table style='width:100%;border-collapse: collapse;'>
					<thead>
						<tr>
							<th colspan =7 style='font-size:15pt; font-weight:100; font-family:arial, sans-serif; text-align:left; padding-bottom:20px;padding-top:20px;' >PENDING REFERRALS</th>
						</tr>
					</thead>
					<tbody class = 'past_referrals'>
						
					</tbody>
					<tfoot>
						<!--<tr>
							<td class='edit_user_options'>
								<input type='button' value= 'Submit Referral' class= 'submit_ref'/> 
							
							</td>
						</tr>-->
						
					</tfoot>
				</table>
			</div>
			<div class = 'tab_container purchased_referrals_container'>
				<table style='width:100%'>
					<thead>
						<tr>
							<th colspan =7 style='font-size:15pt; font-weight:100; font-family:arial, sans-serif; text-align:left; padding-bottom:20px;padding-top:20px;' >PURCHASED REFERRALS</th>
						</tr>
					</thead>
					<tbody class = 'purchased_referrals'>
						
					</tbody>
					<tfoot>
						<!--<tr>
							<td class='edit_user_options'>
								<input type='button' value= 'Submit Referral' class= 'submit_ref'/> 
							
							</td>
						</tr>-->
						
					</tfoot>
				</table>
			</div>
		</div>
		<div class='overlay_container'>
			<div class='overlay'>
				<div class='close_overlay' onclick = 'close_overlay()'>X</div>
				<table class='overlay_table'>
					<thead >
						<tr>
							<th class='overlay_th'>

							</th>
						</tr>
					</thead>
					<tbody class='overlay_tbody'>

					</tbody>
					<tfoot class='overlay_tfoot'>
					</tfoot>
				</table>
			</div>
		</div>
	</body>
</html>