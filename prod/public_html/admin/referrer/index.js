var countries =null;
var states =null;

function initialize(){
	populate_user_info();
	get_additional_info();
	initialize_listeners();
	build_add_referral_form();
	build_past_referrals();
	build_purchased_referrals();

	document.getElementsByClassName("opt_btn")[0].click();
}
function populate_user_info(){
	var container = document.getElementsByClassName("my_account")[0];
	var inputs = container.getElementsByClassName("user_info_holder");

	for(var i=0; i<inputs.length; i++){
		var val = inputs[i].getAttribute("name");
		if(val=='id')
			inputs[i].value =user_info[val];
		else	
			inputs[i].innerHTML =user_info[val];

	}
}
function initialize_listeners(){

	var edit_btn= document.getElementsByClassName("edit_info")[0];
		edit_btn.addEventListener("click",edit_info);
	
	var sub= document.getElementsByClassName("submit_ref")[0];
		sub.addEventListener("click",submit_ref);
	var out= document.getElementsByClassName("logout")[0];
		out.addEventListener("click",logout);
	var account= document.getElementsByClassName("my_account_btn")[0];
		account.addEventListener("click",open_my_account);

	//var body_click = document.getElementsByTagName("body")[0];
	//	body_click.addEventListener("click",close_select_overlays);

	var btns = document.getElementsByClassName("opt_btn");
	for(var i=0; i < btns.length; i++){
		btns[i].addEventListener("click", change_tab);
	}
}
function close_select_overlays(evt){
	var overlay = document.getElementsByClassName("select_overlay");
	for(var i =0; i < overlay.length; i++){
		if(overlay[i].style.display == 'block'){
			overlay[i].style.display ='none';
		}
	}
}
function get_additional_info(){
	countries = perform_ajax("./referrer_data.php","function=get_countries",false,"POST");
	states    = perform_ajax("./referrer_data.php","function=get_states",false,"POST");
}
function change_tab(evt){

	if(document.getElementsByClassName("opt_btn_selected").length)	{
		var selected_tab = document.getElementsByClassName("opt_btn_selected")[0];
		document.getElementsByClassName(selected_tab.getAttribute("name"))[0].style.display='none';
		selected_tab.className= selected_tab.className.replace( /(?:^|\s)opt_btn_selected(?!\S)/ , '' );

	}	
	evt.target.className+=" opt_btn_selected";
	document.getElementsByClassName(evt.target.getAttribute("name"))[0].style.display='block';
}
function open_my_account(){
	var acc= document.getElementsByClassName("my_account")[0];
	if(acc.style.display=='block')
		acc.style.display="none";
	else
		acc.style.display="block";
}
function logout(){
	var res = perform_ajax("./referrer_data.php","function=logout",false,"POST");
	location.reload();
}
function edit_info(){
	var container = document.getElementsByClassName("my_account")[0];
	var inputs = container.getElementsByClassName("user_info_holder ");
	for(var i=0; i<inputs.length; i++){
		if(inputs[i].getAttribute("name") !='id' && inputs[i].getAttribute("name") !='lavu_tag'){
			var edit_field = document.createElement("input");
				edit_field.setAttribute("type","text");	
				edit_field.setAttribute("name", inputs[i].getAttribute("name"));
				edit_field.value = inputs[i].innerHTML;
			inputs[i].innerHTML ="";
			inputs[i].appendChild(edit_field);
		}
	}
	var cont = document.getElementsByClassName("edit_user_options")[0];
	cont.innerHTML = '';
	var submit_btn = document.createElement("input");
		submit_btn.setAttribute('type',"button");
		submit_btn.value = "Save Info";
		submit_btn.addEventListener("click",save_user_info);
	
	var cancel_btn =document.createElement("input");
		cancel_btn.setAttribute('type',"button");
		cancel_btn.value = "Cancel";
		cancel_btn.addEventListener("click",close_user_info_editor);

	cont.appendChild(submit_btn);
	cont.appendChild(cancel_btn);
	
}
function build_add_referral_form(){
	var tbod = document.getElementsByClassName("add_referral_form")[0];
	var form = 
		[
			{name:"First Name *",   type:"text", class_name:"req_info",db_val:"f_name", validation: not_null},
			{name:"Last Name *",    type:"text", class_name:"req_info",db_val:"l_name", validation: not_null},
			{name:"Company Name *", type:"text", class_name:"req_info",db_val:"company_name", validation: not_null},
			{name:"Website", 	    type:"text", class_name:"opt_info",db_val:"website" },
			{name:"Phone Number *", type:"text", class_name:"req_info",db_val:"phone", validation:not_null, add_more:true},
			{name:"Email *", 		type:"text", class_name:"req_info",db_val:"email", add_more:true},
			{name:"Address Line 1",	type:"text", class_name:"opt_info",db_val:"address_1"},
			{name:"Address Line 2",	type:"text", class_name:"opt_info",db_val:"address_2"},
			{name:"City", 			type:"text", class_name:"opt_info",db_val:"city"},
			{name:"Country", 		type:"select", class_name:"opt_info",db_val:"country", options:countries},
			{name:"Zip", 			type:"text", class_name:"opt_info",db_val:"zip"},
			{name:"State", 			validation:show_hide_states,type:"select", class_name:"opt_info",db_val:"state", options:states},
			{name:"Notes", 			   type:"textarea", class_name:"opt_info", db_val:"notes"},
			{name:"Email Blurb", 	   type:"textarea", class_name:"opt_info", db_val:"blurb"},
			{name:"referree_id", 	   type:"hidden",   class_name:"req_info",db_val:"referree_id", default_val:user_info.id},
		];
	var built = build_form(form,tbod);
}
function submit_ref(){

	var	required_stringified = "";
	var opt_stringified 	 = "";

	var required = document.getElementsByClassName("req_info");
	var opt 	 = document.getElementsByClassName("opt_info");
	var fields_arr = [];
	for(var i = 0; i < required.length; i++){
		var val    = required[i].value;
		var db_val = required[i].getAttribute("db_val");
		if(val == ''){
			alert("Empty Required Field");
			return;
		}else{
			val = val.replace(/\r?\n/g,'');
			val = val.replace(/["']/g, "-");
			fields_arr.push('"'+db_val+'":"'+val+'"');
		}
	}
	required_stringified = "required_fields="+encodeURIComponent("{"+ fields_arr.join(",")+"}");
	var fields_arr = [];
	for(var j = 0; j < opt.length; j++){
		var val = opt[j].value;
		var db_val = opt[j].getAttribute("db_val");
		if(val != ''){
			fields_arr.push('"'+db_val+'":"'+val+'"');
		}
	}
	opt_stringified = "opt_fields="+encodeURIComponent("{"+ fields_arr.join(",")+"}");
	var stringified = required_stringified+"&"+opt_stringified;
	var res = perform_ajax("./referrer_data.php","function=save_referral&"+stringified,false,"POST");
	var reserr = perform_ajax("../distro/beta/exec/new_lead.php","function=save_referral&"+stringified,false,"POST");
	if(res.status=='error'){
		alert("Could not edit. "+ res.message);
	}else{
		alert(res.message);
		//location.reload();
	}
	if(reserr.status=='error'){
		alert("Could not edit. "+ reserr.message);
	}else{
		alert(reserr.message);
		//location.reload();
	}
}
function not_null(event){
	if( event.target.value.length)
		return true;
	else
		return false;
}
function save_user_info(){
	var container = document.getElementsByClassName("my_account")[0];
	var inputs = container.getElementsByTagName("input");
	var serialized = new Array();
	for(var i=0; i<inputs.length; i++){
		if(inputs[i].getAttribute('type') != 'button' && inputs[i].getAttribute("name") !='lavu_tag'){
			serialized[i]=inputs[i].getAttribute('name')+"="+inputs[i].value; 
		}
	}
	var str = serialized.join("&");
	var res = perform_ajax("./referrer_data.php","function=update_referrer&"+str,false,"POST");
	if(res.status=='error'){
		alert("Could not edit. "+ res.message);
	}
	close_user_info_editor();
}
function close_user_info_editor(){
	var container = document.getElementsByClassName("my_account")[0];
	var inputs = container.getElementsByClassName("user_info_holder");
	for(var i=0; i<inputs.length; i++){
		inputs[i].innerHTML =inputs[i].children[0].value;
	}
	var cont = document.getElementsByClassName("edit_user_options")[0];
	cont.innerHTML = '';
	var edit_btn = document.createElement("input");
		edit_btn.setAttribute('type',"button");
		edit_btn.value = "Edit User Info";
		edit_btn.addEventListener("click",edit_info);
	cont.appendChild(edit_btn);
}
function build_purchased_referrals(){
	var res = perform_ajax("./referrer_data.php","function=get_purchased_referrals&referree_id="+user_info.id,false,"POST");
	var container = document.getElementsByClassName("purchased_referrals")[0];

	var tr = document.createElement("tr");

	for(var col in res[0]){
		var th = document.createElement("th");
			th.className='header_class';
			th.innerHTML = col.replace(/_/g, " ");;
			tr.appendChild(th);
	}
	container.appendChild(tr);

	for( var i = 0; i < res.length; i++){
		var tr = document.createElement("tr");
		for(var col in res[i]){
			if(col == 'other_info'){
				var parsed = JSON.parse(res[i][col]);
				var table  = document.createElement("table");
					table.className = 'other_info_table';
				var div    = document.createElement("div");
					div.className ='inner_table_container';
				for( var key in parsed){
					var tr2 = document.createElement("tr");
					var td2 = document.createElement("td");
						td2.className = 'inner_table_td';
						td2.innerHTML = key;

					var td3 = document.createElement("td");
						td3.innerHTML = parsed[key];
						td3.className = 'inner_table_td other_info_val';
						td3.setAttribute("other_name", key);

					tr2.appendChild(td2);
					tr2.appendChild(td3);
					table.appendChild(tr2);
				}
				div.appendChild(table);
				var s_td = document.createElement("td");
					s_td.className = 'outer_table_td';
					s_td.setAttribute("db_col", col);
					s_td.appendChild(div);
					tr.appendChild(s_td);

			}else{
				var s_td = document.createElement("td");
					s_td.className = 'outer_table_td';
					s_td.innerHTML = res[i][col];
					s_td.setAttribute("db_col", col);
				tr.appendChild(s_td);
			}
		}
		
		var td = document.createElement("td");
			td.className = 'outer_table_td';
			td.setAttribute("db_col","btn");
		var btn= document.createElement("div");
			btn.className = 'edit_ref_btn';
			btn.setAttribute("ref_id",res[i].id);
			btn.innerHTML ='VIEW';

			btn.addEventListener("click",view_past_referral); 
		td.appendChild(btn);
		tr.appendChild(td);
		
		container.appendChild(tr);
	}
}
function view_past_referral(evt){
	var res = perform_ajax("./referrer_data.php","function=get_purchased_referral&referree_id="+user_info.id+"&id="+evt.target.getAttribute('ref_id'),false,"POST");
	res['other_info']= JSON.parse(res.other_info);

	var overlay = document.getElementsByClassName("overlay_tbody")[0];
	var form = 
		[
			{name:"First Name *",   type:"text", 	 class_name:"req_info", db_val:"f_name", 	   validation: not_null,default_val:res.f_name, disabled:true},
			{name:"Last Name *",    type:"text", 	 class_name:"req_info", db_val:"l_name", 	   validation: not_null,default_val:res.l_name, disabled:true},
			{name:"Company Name *", type:"text", 	 class_name:"req_info", db_val:"company_name", validation: not_null,default_val:res.company_name, disabled:true},
			{name:"Website", 	    type:"text", 	 class_name:"opt_info", db_val:"website",	   default_val:res['other_info'].website , disabled:true},
			{name:"Phone Number *", type:"text", 	 class_name:"req_info", db_val:"phone",    	   validation:not_null, disabled:true,default_val:res.phone, additional_fields: [{db_val: "phone_2",val:res['other_info'].phone_2},{db_val: "phone_3", val:res['other_info'].phone_3}]},
			{name:"Email *", 		type:"text", 	 class_name:"req_info", db_val:"email",    	   validation:not_null, disabled:true, default_val:res.email,additional_fields: [{db_val: "email_2", val:res['other_info'].email_2},{db_val: "email_3",val:res['other_info'].email_3}]},
			{name:"Address Line 1",	type:"text", 	 class_name:"opt_info", db_val:"address_1",	   default_val:res['other_info'].address_1, disabled:true},
			{name:"Address Line 2",	type:"text", 	 class_name:"opt_info", db_val:"address_2",	   default_val:res['other_info'].address_2, disabled:true},
			{name:"City", 			type:"text", 	 class_name:"opt_info", db_val:"city",		   default_val:res['other_info'].city, disabled:true},
			{name:"Country", 		type:"select",   class_name:"opt_info", db_val:"country", 	   options:countries,default_val:res['other_info'].country, disabled:true},
			{name:"Zip", 			type:"text",     class_name:"opt_info", db_val:"zip",   	   default_val:res['other_info'].zip, disabled:true},
			{name:"State", 			type:"select",   class_name:"opt_info", db_val:"state", 	   validation:show_hide_states,options:states, default_val:res['other_info'].state, disabled:true},
			{name:"Notes", 			type:"textarea", class_name:"opt_info", db_val:"notes",		   default_val:res['other_info'].notes, disabled:true},
			{name:"Email Blurb", 	type:"textarea", class_name:"opt_info", db_val:"blurb",	  	   default_val:res['other_info'].blurb, disabled:true},
			{name:"referree_id", 	type:"hidden",   class_name:"req_info", db_val:"referree_id",  default_val:user_info.id},
			{name:"referral_id", 	type:"hidden",   class_name:"req_info", db_val:"referral_id",  default_val:evt.target.getAttribute('ref_id')}
		];
	build_form(form,overlay);
	var tr = document.createElement("tr");
	var td = document.createElement("td");
	var cancel = document.createElement("input");
		cancel.setAttribute('type','button');
		cancel.value ='Cancel';
		cancel.className='submit_ref';
		cancel.addEventListener("click",close_overlay);
	td.appendChild(cancel);
	tr.appendChild(td);
	overlay.appendChild(tr);
	document.getElementsByClassName("overlay_th")[0].innerHTML ='VIEW PURCHASED REFERRALS';
	document.getElementsByClassName("overlay_container")[0].style.display='block';
}
function edit_past_referral(evt){
	var res = perform_ajax("./referrer_data.php","function=get_referral&referree_id="+user_info.id+"&id="+evt.target.getAttribute('ref_id'),false,"POST");
	res['other_info']= JSON.parse(res.other_info);

	var overlay = document.getElementsByClassName("overlay_tbody")[0];
	var form = 
		[
			{name:"First Name *",   type:"text", 	 class_name:"req_info", db_val:"f_name", 	   validation: not_null,default_val:res.f_name},
			{name:"Last Name *",    type:"text", 	 class_name:"req_info", db_val:"l_name", 	   validation: not_null,default_val:res.l_name},
			{name:"Company Name *", type:"text", 	 class_name:"req_info", db_val:"company_name", validation: not_null,default_val:res.company_name},
			{name:"Website", 	    type:"text", 	 class_name:"opt_info", db_val:"website",	   default_val:res['other_info'].website },
			{name:"Phone Number *", type:"text", 	 class_name:"req_info", db_val:"phone",    	   validation:not_null, add_more:true,default_val:res.phone, additional_fields: [{db_val: "phone_2",val:res['other_info'].phone_2},{db_val: "phone_3", val:res['other_info'].phone_3}]},
			{name:"Email *", 		type:"text", 	 class_name:"req_info", db_val:"email",    	   validation:not_null, add_more:true, default_val:res.email,additional_fields: [{db_val: "email_2", val:res['other_info'].email_2},{db_val: "email_3",val:res['other_info'].email_3}]},
			{name:"Address Line 1",	type:"text", 	 class_name:"opt_info", db_val:"address_1",	   default_val:res['other_info'].address_1},
			{name:"Address Line 2",	type:"text", 	 class_name:"opt_info", db_val:"address_2",	   default_val:res['other_info'].address_2},
			{name:"City", 			type:"text", 	 class_name:"opt_info", db_val:"city",		   default_val:res['other_info'].city},
			{name:"Country", 		type:"select",   class_name:"opt_info", db_val:"country", 	   options:countries,default_val:res['other_info'].country},
			{name:"Zip", 			type:"text",     class_name:"opt_info", db_val:"zip",   	   default_val:res['other_info'].zip},
			{name:"State", 			type:"select",   class_name:"opt_info", db_val:"state", 	   validation:show_hide_states,options:states, defaulthmmm_val:res['other_info'].state},
			{name:"Notes", 			type:"textarea", class_name:"opt_info", db_val:"notes",		   default_val:res['other_info'].notes},
			{name:"Email Blurb", 	type:"textarea", class_name:"opt_info", db_val:"blurb",	  	   default_val:res['other_info'].blurb},
			{name:"referree_id", 	type:"hidden",   class_name:"req_info", db_val:"referree_id",  default_val:user_info.id},
			{name:"referral_id", 	type:"hidden",   class_name:"req_info", db_val:"referral_id",  default_val:evt.target.getAttribute('ref_id')}
		];
	build_form(form,overlay);
	var tr = document.createElement("tr");
	var td = document.createElement("td");
	var save = document.createElement("input");
		save.setAttribute('type','button');
		save.value ='Save Referral';
		save.className='submit_ref';
		save.addEventListener("click",save_edited_referral);
	var cancel = document.createElement("input");
		cancel.setAttribute('type','button');
		cancel.value ='Cancel';
		cancel.className='submit_ref';
		cancel.addEventListener("click",close_overlay);
	td.appendChild(save);
	tr.appendChild(td);
	var td = document.createElement("td");
	td.appendChild(cancel);
	tr.appendChild(td);
	overlay.appendChild(tr);
	document.getElementsByClassName("overlay_th")[0].innerHTML ='EDIT PAST REFERRAL';
	document.getElementsByClassName("overlay_container")[0].style.display='block';

}
function build_past_referrals(){
	var res = perform_ajax("./referrer_data.php","function=get_past_referrals&referree_id="+user_info.id,false,"POST");
	var container = document.getElementsByClassName("past_referrals")[0];

	var tr = document.createElement("tr");

	for(var col in res[0]){
		if(col!='id'){
			var th = document.createElement("th");
			th.className='header_class';
			th.innerHTML = col.replace(/_/g, " ");;
			tr.appendChild(th);
		}
	}
	container.appendChild(tr);
	for( var i = 0; i < res.length; i++){
		var tr = document.createElement("tr");
		for(var col in res[i]){
			if(col!='id'){
				var s_td = document.createElement("td");
					s_td.className = 'outer_table_td';
					s_td.innerHTML = res[i][col];
					s_td.setAttribute("db_col", col);
				tr.appendChild(s_td);
			}
		}
		var td = document.createElement("td");
			td.className = 'outer_table_td';
			td.setAttribute("db_col","btn");
		var btn= document.createElement("div");
			btn.className = 'edit_ref_btn';
			btn.innerHTML ='EDIT';
			btn.setAttribute("value","Edit Referral");
			btn.setAttribute("ref_id",res[i].id);
			btn.addEventListener("click",edit_past_referral); 
		td.appendChild(btn);
		tr.appendChild(td);
		container.appendChild(tr);
	}
}
function save_edited_referral(evt){
	var	required_stringified = "";
	var opt_stringified 	 = "";
	var scope =evt.target.parentNode.parentNode.parentNode;
	var required = scope.getElementsByClassName("req_info");
	var opt 	 = scope.getElementsByClassName("opt_info");
	var fields_arr = [];
	for(var i = 0; i < required.length; i++){
		var val    = required[i].value;
		var db_val = required[i].getAttribute("db_val");
		if(val == ''){
			alert("Empty Required Field");
			return;
		}else{
			fields_arr.push('"'+db_val+'":"'+val+'"');
		}
	}
	required_stringified = "required_fields="+encodeURIComponent("{"+ fields_arr.join(",")+"}");
	var fields_arr = [];
	for(var j = 0; j < opt.length; j++){
		var val = opt[j].value;
		var db_val = opt[j].getAttribute("db_val");
		if(val != ''){
			fields_arr.push('"'+db_val+'":"'+val+'"');
		}
	}
	opt_stringified = "opt_fields="+encodeURIComponent("{"+ fields_arr.join(",")+"}");
	var stringified = required_stringified+"&"+opt_stringified;
	var res = perform_ajax("./referrer_data.php","function=update_referral&"+stringified,false,"POST");
	if(res.status=='error'){
		alert("Could not edit. "+ res.message);
	}else{
		alert(res.message);
		location.reload();
	}
}
function cancel_edited_referral(evt){
	var inps = evt.target.parentNode.parentNode.getElementsByTagName("input");
	var first =true;
	while(inps.length > 2 ){

		if(inps[0].getAttribute('type') != 'button'){
			if( inps[0].getAttribute("db_val")!='primary_contact' && inps[0].getAttribute("db_val")!='company_name'){
				if(first){
					inps[0].parentNode.parentNode.parentNode.className = 'inner_table_container';
					first= false;
				}
				inps[0].parentNode.className ="inner_table_td other_info_val";
				inps[0].parentNode.setAttribute("other_name",inps[0].getAttribute('db_val'));
			}
			var val = inps[0].value;
			
			inps[0].parentNode.innerHTML = val;
		}
	}
	var t_area = evt.target.parentNode.parentNode.getElementsByTagName("textarea");
		t_area[0].parentNode.className ="inner_table_td other_info_val";
		t_area[0].parentNode.setAttribute("other_name",t_area[0].getAttribute('db_val'));
		t_area[0].parentNode.innerHTML = t_area[0].value;
	
	var btn= document.createElement("input");
		btn.setAttribute("type","button");
		btn.setAttribute("value","Edit Referral");
		btn.addEventListener("click",edit_past_referral); 
	var btn_container = inps[0].parentNode;
		btn_container.innerHTML= '';
		btn_container.appendChild(btn);
}
function build_form(form, pa){
	for(var i=0; i < form.length; i++){
		if( i%2 ==0)
			var tr = document.createElement("tr");
		var inner_td = document.createElement("td");
			inner_td.className= "referral_info ";

		if(form[i].type == 'text'){
			var inp = document.createElement("input");
				inp.setAttribute("type","text");
				inp.className = 'editor_input ' + form[i].class_name;
				inp.setAttribute("placeholder",form[i].name);
				inp.setAttribute("db_val", form[i].db_val)
			if(form[i].validation){
				inp.addEventListener("click", form[i].validation);
			}
			if(form[i].default_val){
				inp.value = form[i].default_val;
			}
			if(form[i].add_more){
				var add_more_btn = document.createElement("div");
					add_more_btn.addEventListener("click", add_more);
					add_more_btn.className = 'add_more_btn';
					add_more_btn.innerHTML ='+';
				inner_td.appendChild(add_more_btn);
			}	
			if( form[i].disabled)	
				inp.setAttribute("disabled","1");	
			inner_td.appendChild(inp);
			if(form[i].additional_fields){
				
				for(var j = 0; j < form[i].additional_fields.length; j++){
					if( form[i].additional_fields[j].val ){
						var br = document.createElement("br");
						inner_td.appendChild(br);
						var add_inp = document.createElement("input");
							add_inp.setAttribute('type', "text");
							add_inp.className = 'editor_input opt_info';
							add_inp.setAttribute("placeholder",form[i].name+" "+ (j+2) );
							add_inp.setAttribute("db_val", form[i].additional_fields[j].db_val)
							add_inp.value = form[i].additional_fields[j].val;
						if(form[i].add_more){
							var remove_field_btn = document.createElement("div");
								remove_field_btn.addEventListener("click", remove_field);
								remove_field_btn.className = 'remove_field_btn';
								remove_field_btn.innerHTML ='-';
							inner_td.appendChild(remove_field_btn);
						}
						if( form[i].disabled)	
							add_inp.setAttribute("disabled","1");	
						inner_td.appendChild(add_inp);
					}
				}
			}
		}else if(form[i].type == 'textarea'){
			var inp = document.createElement("textarea");
				inp.setAttribute("db_val", form[i].db_val)
				inp.setAttribute("placeholder",form[i].name);
				inp.style.margin= "0px 0px 10px";
				inp.style.height="104px"; 
				inp.style.width= "449px;";
				inp.style.paddingTop= "17px";
				inp.className='editor_input '+ form[i].class_name;
				inp.style.resize="none";
			if(form[i].default_val){
				inp.value = form[i].default_val;
			}			

			if( form[i].disabled)	
				inp.setAttribute("disabled","1");
			inner_td.appendChild(inp);
		}else if(form[i].type == 'hidden'){
			var inp = document.createElement("input");
				inp.setAttribute("type","hidden");
				inp.setAttribute("db_val", form[i].db_val);
				inp.className = form[i].class_name;
				if(form[i].default_val){
					inp.value = form[i].default_val;
				}
			inner_td.appendChild(inp);
		}else if(form[i].type=='select'){
			var inp = document.createElement("input");
				inp.setAttribute("type","text");
				inp.className = 'editor_input '+ form[i].class_name;
				inp.setAttribute("placeholder",form[i].name);
				inp.setAttribute("db_val", form[i].db_val)
			if(form[i].options){
				var options = form[i].options;
				var opt_div = document.createElement("div");
					opt_div.className='select_overlay';
					var opt_table = document.createElement("table");
						opt_table.className='select_table';
				
				for( var j = 0; j < options.length; j++){
					var opt_tr = document.createElement("tr");
					var opt_td = document.createElement("td");
						opt_td.className ='select_td';
						opt_td.innerHTML = options[j].name;
						opt_td.addEventListener("click",select_option);
					if(options[j].default){
						opt_td.className+=" selected_option";
						inp.value=options[j].name; 
					}
					if(form[i].default_val && options[j].name == form[i].default_val){
					
						opt_td.className+=" selected_option";
						inp.value = form[i].default_val;
						
					}
					opt_tr.appendChild(opt_td);
					opt_table.appendChild(opt_tr);
				}
				opt_div.appendChild(opt_table);
				if(form[i].validation)
					inp.addEventListener("click", form[i].validation);
				else
					inp.addEventListener("click", open_custom_drop_down);
				
				
				inner_td.appendChild(opt_div);
			}
			if( form[i].disabled)	
				inp.setAttribute("disabled","1");
			inner_td.appendChild(inp);
		}
		tr.appendChild(inner_td);
		pa.appendChild(tr);
	}
}
function open_custom_drop_down(event){
	var overlays = document.getElementsByClassName("select_overlay");
	for(var i= 0; i < overlays.length; i++){
		overlays[i].style.display='none';
	}
	event.target.parentNode.getElementsByClassName("select_overlay")[0].style.display='block';
}
function select_option(event){
	var val = event.target.innerHTML;
	event.target.parentNode.parentNode.parentNode.style.display='none';
	event.target.parentNode.parentNode.parentNode.parentNode.getElementsByTagName("input")[0].value = val;
}	
function add_more(event){
	var elem =  event.target.parentNode.children[1];
	if( event.target.parentNode.getElementsByTagName("input").length <= 2){
		var cloned = $(elem).clone();
		cloned[0].setAttribute('placeholder', cloned[0].getAttribute('placeholder')+ " "+ (event.target.parentNode.getElementsByTagName("input").length +1 ) ) ;
		cloned[0].setAttribute('db_val', cloned[0].getAttribute('db_val')+ "_"+ (event.target.parentNode.getElementsByTagName("input").length +1 ) );
		cloned[0].className =  cloned[0].className.replace( /(?:^|\s)req_info(?!\S)/ , ' opt_info');
		var br = document.createElement("br");
		event.target.parentNode.appendChild(br);
		var remove_field_btn = document.createElement("div");
			remove_field_btn.addEventListener("click", remove_field);
			remove_field_btn.className = 'remove_field_btn';
			remove_field_btn.innerHTML ='-';
		event.target.parentNode.appendChild(remove_field_btn);
		event.target.parentNode.appendChild(cloned[0]);
	
	}	
}
function show_hide_states(event){
	if(document.getElementsByClassName("select_overlay")[0].nextElementSibling.value == "United States"){
		open_custom_drop_down(event)
	}
}
function remove_field(event){
	var inp = event.target.nextElementSibling;
	inp.parentElement.removeChild(inp);
	var br = event.target.previousElementSibling;
	br.parentNode.removeChild(br);
	event.target.parentElement.removeChild(event.target);
}
function close_overlay(){
	document.getElementsByClassName("overlay_container")[0].style.display="none";
	document.getElementsByClassName("overlay_tbody")[0].innerHTML = '';
}
function perform_ajax(URL, urlString,asyncSetting,type ){
	var returnVal='';
	//console.log('urlString='+urlString);
	$.ajax({
		url: URL,
		type: type,
		data:urlString,
		async:asyncSetting
	}).done(function(msg){
		try{
				returnVal = JSON.parse(msg);
			}catch(err){
				console.log(err);
				returnVal = string;
			}
	}).fail(function(){
		returnVal = JSON.parse('{"status":"error","message":"ajax fail"}');
	});
	/*
	$.ajax({
		url: URL,
		type: type,
		data:urlString,
		async:asyncSetting,
		success: function (string){
			try{
				returnVal = JSON.parse(string);
			}catch(err){
				console.log(err);
				returnVal = string;
			}
		}
	});
	*/
	return returnVal;
}
window.onload= initialize;
