<?php
	
	ini_set("display_errors",1);
	require_once($_SERVER['DOCUMENT_ROOT']."/cp/resources/lavuquery.php");
	function check_mobile(){
		$useragent=$_SERVER['HTTP_USER_AGENT'];
		if(preg_match('/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i',$useragent)||preg_match('/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i',substr($useragent,0,4)))
			return true;
		else
			return false;
	}
	function check_login(){
		if(isset($_COOKIE['poslavu_referrer_login'])){
			$token = $_COOKIE['poslavu_referrer_login'];
			$query = mlavu_query("SELECT * FROM `poslavu_REFERRER_db`.`logged_in` where `token` ='[1]' and `valid`='1'", $token);
			if(mysqli_num_rows($query)){
				header("Location:https://admin.poslavu.com/referrer/index.php");
			}
		}
	}
	if(!check_mobile())
		check_login();
	else
		header("Location:https://admin.poslavu.com/referrer/mobile/login.php");

?>

<script type ='text/javascript' src= './login.js'></script>
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
<link rel="stylesheet" type="text/css" href="./login.css">
<body>
	<div class='outer_container'>
		<div class= 'lavu_logo'></div>
		<div class='main_container'>
			<div class = 'referrer_sign_in'>
				<div class='info_container'>
					<div class='info_container_title'>REFERRALS PROGRAM</div>
					<div class="info_container_subtitle">Help a business owner find Lavu iPad POS and YOU GET REWARDS!</div>
					<div class="info_container_shortlist">
						<div><span>1 - </span>Create your account</div>
						<div><span>2 - </span>Send referrals to business owners</div>
						<div><span>3 - </span>Get rewards when they sign up for Lavu!</div>
					</div>
				</div>
				<div class='logo'></div>
				<form name ='sign_in_form' class='sign_in_form'>
					<table class = 'sign_in_table'>
						<tr>
							<td>
								<input type ='text' class ='login_input' name ='username' placeholder='Lavu Tag/Email Address'/>
							</td>
						</tr>
						<tr>
							<td><input type='password' class ='login_input' name='password' placeholder='Password'/> </td>
						</tr>
						<tr><td class='error_div'></td></tr>
						<tr>
							<td> <div class='forgot_pass' onclick='show_forgot_pass_overlay()'>Forgot Password?</div><input type='button' class='login_btn' value = 'SIGN IN' onclick='referrer_login()'/> </td>
						</tr>

					</table>
				</form>

			</div>
			<div class ='signup_btn_container'> 
				<span class='crayon'></span>  
				<div class ='lavu_referrer_signup_btn' onclick='show_signup_form()'><span style='color:#aecd37'>Sign Up</span> to become a Lavu referrer.</div>
			</div>
			<div class ='referrer_sign_up_container'>
				<form name ='sign_up_form'>
					<table class = 'sign_up_table'>
						<tr>
							<td>
								<input type ='text' class ='login_input' name ='f_name' onblur = 'not_null(this)' placeholder='First Name' valid ='0'/>
							</td>
						</tr><tr>

							<td>
								<input type ='text'class ='login_input' name ='l_name' onblur = 'not_null(this)'  placeholder='Last Name' valid ='0' />
							</td>
						</tr>
						<tr>
							<td>
								<input type ='text'class ='login_input' name ='phone' onblur = 'validate_phone(this)' placeholder='Phone Number' valid ='0' />
							</td>
						</tr>
						<tr>
							<td>
								<input type ='text'class ='login_input' name ='lavu_tag' onblur = 'validate_tag(this)'  placeholder='Lavu Tag'valid ='0' />
							</td>
						</tr>
						<tr>

							<td>
								<input type ='text' class ='login_input' name ='email' onblur = 'validate_email(this)' placeholder='Email' valid ='0'/>
							</td>
						</tr>
						<tr>
							<td>
								<input type='password' class ='pass_input' name='password' class='pass1' onblur = 'validate_password(this)'  placeholder='Password' valid ='0'/> 
								<input type='password' class ='pass_input' name='password2' class='pass2'onblur = 'validate_password(this)'  placeholder='Confirm Password' valid ='0'>
							</td>
						</tr>
						
						<tr>
							<td> <input type='button' class='sign_up_btn' value ='Sign Up and Sign In' id='submit_signup' style='float:right' onclick ='referrer_signup()' disabled/></td>
						</tr>
						<tr>
							<td style='text-align:center' colspan ='2'>
								<div class ='lavu_referrer_signup_btn' onclick='show_signup_form()'>Back to login.</div>
							</td>
						</tr>
					</table>
				</form>
			</div>
			<div class='forgot_pass_overlay_container'>
				<div class='forgot_pass_overlay'>
					<span class = 'close_overlay_btn' onclick='hide_forgot_pass_overlay()' >X</span>
					<table class = 'forgot_pass_table' > 
						<!--<tr>
							<td>Forgot Password</td>
						</tr>-->
							<td><input type='text' class='forgot_pass_action' placeholder='Email Address'/></td>
						<tr>
						</tr>
							<td><input type='button' class='submit_forgot_pass_btn' value ='Submit' onclick='submit_forgot_pass()'/></td>
						<tr>
						</tr>
					</table>
				</div>
			</div>
		</div>
	</div>
</body>

