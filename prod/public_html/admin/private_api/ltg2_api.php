<?php
error_reporting(E_ALL);
ini_set('display_errors', 1);
require_once(__DIR__."/../lib/info_inc.php");
require_once(__DIR__."/../lib/jcvs/jc_functions.php");

if(empty($GLOBALS['company_code']) ){return;}

if($_REQUEST['mode'] == 'loc_info'){
        $menu_id = "";
        $temp = loadLocations($_REQUEST['loc_id'], $data_name, $menu_id);
        echo json_encode($temp[0]);
        return;
}else{
        $result = lavu_query('SELECT `value` FROM `config` WHERE `setting` = "menu_last_updated" AND `location` = "[1]" ORDER BY `value` DESC LIMIT 1;',$_REQUEST['loc_id']);
        if (mysqli_num_rows($result) > 0) {
                $temp = mysqli_fetch_assoc($result);
                echo '{"menu_last_updated":"'.strtotime($temp['value']).'"}';
                return;
        }else{
                echo '{"menu_last_updated":""}';
        }
}