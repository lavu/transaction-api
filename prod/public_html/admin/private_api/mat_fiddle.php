<?php
error_reporting(E_ALL);
ini_set('display_errors', 1);
require_once(__DIR__."/../cp/resources/lavuquery.php");
echo "<pre>";
echo "-_-\n";
return;

$result = mlavu_query('SHOW TABLES IN `poslavu_DEV_db`;');
$MainTables = array();
while($temp = mysqli_fetch_array($result)){
	$MainTables[] = $temp[0];
}
//print_r($MainTables);

$TableDescriptions = array();
foreach ($MainTables as $TableName) {
	$result = mlavu_query('SHOW COLUMNS IN `poslavu_DEV_db`.`'.$TableName.'`;');
	$columns = array();
	while($temp = mysqli_fetch_array($result,MYSQL_ASSOC)){
		$columns[$temp['Field']] = $temp['Type'];
	}
	$TableDescription[$TableName] = $columns;
}

$company_query = mlavu_query("SELECT `data_name` FROM `poslavu_MAIN_db`.`restaurants` WHERE `data_name`!='' AND `data_name`!='poslavu_DEV_db' AND `notes` NOT LIKE '%AUTO-DELETED%' AND `lavu_lite_server`='' ORDER BY `data_name` DESC");
$datanames = array();
while($temp = mysqli_fetch_array($company_query)){
	if(!empty($temp)){
		$datanames[] = $temp[0];
	}
}

checkDataBase('poslavu_vivante_db');

#foreach ($datanames as $dn) {
#	checkDataname($dn);
#}
// table
//  columnname
//   type

//checkDataBase('poslavu_matt_lavu_db');

function checkDataname($dataname){
	return checkDataBase('poslavu_'.$dataname.'_db');
}

function checkDataBase($database_name){
	global $TableDescription;
	$checkTables = array();
	$result = mlavu_query('SHOW TABLES IN `'.$database_name.'`;');
	$dbTables = array();
	while($temp = mysqli_fetch_array($result)){
		if(array_key_exists($temp[0],$TableDescription)){
			$dbTables[] = $temp[0];
		}
	}

	$dbTableDescriptions = array();
	foreach ($dbTables as $TableName) {
		$result = mlavu_query('SHOW COLUMNS IN `'.$database_name.'`.`'.$TableName.'`;');
		$columns = array();
		while($temp = mysqli_fetch_array($result,MYSQL_ASSOC)){
			$columns[$temp['Field']] = $temp['Type'];
		}
		$dbTableDescriptions[$TableName] = $columns;
		$checkTables[$TableName] = $TableDescription[$TableName];
	}
	
	$return = arrayRecursiveDiff($checkTables, $dbTableDescriptions);
	if(!empty($return)){
		echo "Bad: $database_name\n";
		//print_r($dbTableDescriptions);
		createAlterStatements($database_name,$return,$dbTableDescriptions);
		//print_r($return);
	}else{
		echo "Good: $database_name\n";
	}
	return $return;
}

function createAlterStatements($database_name,$diff,$dbTableDescriptions){
	foreach ($diff as $table => $fields) {
		$flag1 = false;
		$flag2 = false;
		$alterString  = "ALTER TABLE `$database_name`.`$table` ADD (";
		$alter2String = "ALTER TABLE `$database_name`.`$table` ";
		foreach ($fields as $field => $type) {
			if(empty($dbTableDescriptions[$table][$field]) ){
				$flag1 = true;
				$alterString .= "`$field` $type NOT NULL ,";
			}else{
				$flag2 = true;
				$alter2String .= "CHANGE `$field` `$field` $type NOT NULL ,";
			}
		}
		$alterString = rtrim($alterString,  ",");
		$alter2String= rtrim($alter2String, ",");
		$alterString  .= ");\n";
		$alter2String .= ";\n";
		if($flag1){
			echo $alterString;
		}
		if($flag2){
			echo $alter2String;
		}
	}
}

function arrayRecursiveDiff($aArray1, $aArray2) {
	$aReturn = array();
	foreach ($aArray1 as $mKey => $mValue) {
		if (array_key_exists($mKey, $aArray2)) {
			if (is_array($mValue)) {
				$aRecursiveDiff = arrayRecursiveDiff($mValue, $aArray2[$mKey]);
				if (count($aRecursiveDiff)) { $aReturn[$mKey] = $aRecursiveDiff; }
			} else {
				if ($mValue != $aArray2[$mKey]) {
					$aReturn[$mKey] = $mValue;
				}
			}
		} else {
			$aReturn[$mKey] = $mValue;
		}
	}
	return $aReturn;
}

echo "</pre>";
