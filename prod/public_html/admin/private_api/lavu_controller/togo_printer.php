<?php
require_once("../../cp/resources/core_functions.php");
require_once("../../manage/misc/Standalones/json_encode.php");
error_reporting(E_ALL);


if($_REQUEST['code'] != 'TESTING'){echo "error1";return;}
if(empty($_REQUEST['DATA_NAME'])){echo "error2";return;}
if(empty($_REQUEST['DB_NAME'])){echo "error3";return;}


if($_REQUEST['cmd'] = 'GET'){
	togoPrint();
}

if($_REQUEST['cmd'] = 'SET'){
	if(empty($_REQUEST['orderID'])){return;}
	togoPrinted($_REQUEST['orderID']);
}

function togoPrinted($myOrder)
{
	lavu_connect_dn($_REQUEST['DATA_NAME'],$_REQUEST['DB_NAME']);
	$myQuery = "UPDATE `orders` SET `send_status` = \"0\" WHERE `order_id` = \"$myOrder\" ";
	$queryReturn = lavu_query($myQuery);
	//error_log($myQuery);
	echo "1";
	return;
}

function togoPrint()
{
	lavu_connect_dn($_REQUEST['DATA_NAME'],$_REQUEST['DB_NAME']);
	$myQuery = "SELECT `order_id`,`togo_time`,`togo_name` FROM `orders`
					WHERE (`togo_status` = 1 AND `send_status` != 0)
					AND
					(
						`closed` = 0
						OR
						(`reopened_datetime` != 0 AND `reclosed_datetime` = 0)
					)
					ORDER BY `togo_time` ASC
					LIMIT 1;
				";
	$queryReturn = lavu_query($myQuery);
	$oneResult = mysqli_fetch_assoc($queryReturn);
	if(empty($oneResult)){echo "{}";return;}
	$myOrder = new TogoPrintOrder();
	$myOrder->orderID = $oneResult['order_id'];
	$myOrder->togoTime = $oneResult['togo_time'];
	$myOrder->togoTimeRaw = strtotime($oneResult['togo_time']);
	$myOrder->currentTimeRaw = time();
	if(!timeCheck($myOrder->togoTimeRaw,$myOrder->togoTimeRaw)){echo "{}";return;}
	$myOrder->name = $oneResult['togo_name'];
	$myOrder->fontStyle = getFontStyle();
	$myOrder->printJobArray = array();
	ProcessOrder($myOrder);
	echo json_encode($myOrder);
}

function timeCheck($togoTime,$currentTime)
{
	$timeDiff = $togoTime - $currentTime;
	if($timeDiff <= 0){
		return true;
	}
	$myQuery = "select `value` from `config` where `setting` = \"auto_print_minutes\"";
	$queryReturn = lavu_query($myQuery);
	$oneResult = mysqli_fetch_assoc($queryReturn);
	if(empty($oneResult)){return false;}
	if(empty($oneResult['value'])){return false;}
	if(is_numeric($oneResult['value'])){return false;}
	$delayTime = $oneResult['value'] * 60;
	if($timeDiff <= $delayTime)
	{
		return true;
	}
	return false;
}

function processOrder($myOrder)
{ //Processes an order and prints tickets if needed
	$itemArray = getOrderItems($myOrder->orderID);
	if (empty($itemArray)) {
		return TRUE;
	}
	$printerQueues = array();

	foreach ($itemArray as $item) {
		if ($item['printer'] < 1000) { //Print Groups are over 1000
			$printerNumber = $item['printer'];
			if ('1' == $printerNumber) { //
				$printerName = trim('kitchen');
			} else {
				$printerName = trim('kitchen' . $printerNumber);
			}
			if (!isset($printerQueues[$printerName])) {
				$printerQueues[$printerName] = new PrinterQueue();
				$printerQueues[$printerName]->printerID = $printerName;
				$printerQueues[$printerName]->printerInfo  = getPrinterType($printerName);
				$printerQueues[$printerName]->items = array();
			}
			$printerQueues[$printerName]->items[] = $item;
		} else { //Print Groups
			$printerQueues = queueForGroup($item, $printerQueues);
			//error_log("PrinterGroup Not Implimented");
		}
	}
	$myOrder->printJobArray = $printerQueues;
}

function queueForGroup($item, $printerQueues)
{ //takes an item printing to a printer group and sends it to the printer who are part of the group
	$groupID = $item['printer'] - 1000;
	$myQuery = "SELECT `value`,`value2`,`value3`,`value4`,`value5`,`value6`
					 FROM `config` WHERE `id` = \"$groupID\"
					";
	$myQuery = trim($myQuery);
	$queryReturn = lavu_query($myQuery);
	$resultArray = mysqli_fetch_assoc($queryReturn);
	foreach ($resultArray as $printerName) {
		if ('0' != $printerName) {
			if (!isset($printerQueues[$printerName])) {
				$printerQueues[$printerName] = new printerQueue();
				$printerQueues[$printerName]->printerID = $printerName;
				$printerQueues[$printerName]->printerInfo  = getPrinterType($printerName);
				$printerQueues[$printerName]->items = array();
			}
			$printerQueues[$printerName]->items[] = $item;
		}
	}
	return $printerQueues;
}

function getOrderItems($order_id)
{ //takes an order and returns an array of all of the items attached to that order
	$myQuery = "
		SELECT `quantity`,`item`,`options`,`special`,`notes`,`printer`
		FROM `order_contents`
		WHERE `order_id` = \"$order_id\"";
	$myQuery = trim($myQuery);
	$queryReturn = lavu_query($myQuery);
	$resultArray = array();
	while ($resultArray[] = mysqli_fetch_assoc($queryReturn) ){}
	return $resultArray;
}

function getPrinterType($printerID)
{
	$myQuery = "
		SELECT `value6`,`value3`,`value5`
		FROM `config`
		WHERE `type` = \"printer\"
		AND `setting` = \"$printerID\"
		";
	$myQuery = trim($myQuery);
	$queryReturn = lavu_query($myQuery);
	$resultArray = mysqli_fetch_assoc($queryReturn);
	$myReturnArray = array();
	$myReturnArray['type'] = $resultArray['value6'];
	$myReturnArray['ip'] = $resultArray['value3'];
	$myReturnArray['port'] = $resultArray['value5'];
	return $myReturnArray;
}

function getFontStyle()
{ //Returns the font style and red modifiers settings in an array (index 0 and 1 respectively)
	$myQuery = 'SELECT `kitchen_ticket_font_size`,`modifiers_in_red` FROM `locations` WHERE `id` = 1';
	$queryReturn = lavu_query($myQuery);
	$resultArray = array();
	$resultArray = mysqli_fetch_assoc($queryReturn);
	if(empty($resultArray))
	{
		$resultArray['kitchen_ticket_font_size'] = 0;
		$resultArray['modifiers_in_red'] = 0;
	}
	return $resultArray;
}

// @codingStandardsIgnoreStart
class TogoPrintOrder
{
	public $orderID;
	public $name;
	public $togoTime;
	public $printJobArray;
	public $fontStyle;
}

class PrinterQueue
{ //Convience Class for handling printer queues
	public $printerID;
	public $printerInfo;
	public $items;
}
// @codingStandardsIgnoreEnd

?>