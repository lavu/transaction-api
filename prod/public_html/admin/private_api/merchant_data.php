<?php
	//ini_set("display_errors","1");
    //For mlavu_query().
    require_once('/home/poslavu/public_html/admin/cp/resources/core_functions.php');
	
	function get_secret_message_hash($s_input, $s_time_mod='') {
		$s_current_minutes = date("i", "0 minutes");
		$s_currtime = date("Y-m-d H:i", "0 minutes");
		if ($s_time_mod != '') {
			$s_current_minutes = date("i", $s_time_mod);
			$s_currtime = date("Y-m-d H:i", $s_time_mod);
		}
		$i_one_minute_increment = (int)$s_current_minutes;// - ((int)$s_current_minutes) % 5;
		$s_last_one_minutes = str_replace(":".$s_current_minutes, ":".$i_one_minute_increment, $s_currtime).":00";
		$i_last_one_minutes = strtotime($s_last_one_minutes);
		return (md5($s_input.$i_last_one_minutes."super secret lavu hash key"));
	}
	
	function check_secret_message_hash($s_input, $s_hash) {
		for ($i = -1; $i < 2; $i++) {
			if ($s_hash == get_secret_message_hash($s_input, $i." minutes"))
				return TRUE;
		}
		return FALSE;
	}
	
	function get_requested_information($a_datanames, $a_request_information) {
		$badval = array();
		$badval[] = array();
		foreach($a_request_information as $k=>$v)
			$a_request_information[$k] = ConnectionHub::GetConn('poslavu')->escapeString($v);
		$s_columns = '`'.implode('`,`', $a_request_information).'`';
		$a_dataname_indices = array();
		for ($i = 0; $i < count($a_datanames); $i++)
			$a_dataname_indices[] = $i;
		$s_dataname_where_clause = "`data_name`='[".implode("]' OR `data_name`='[", $a_dataname_indices)."]'";
		$data_query_string = "SELECT $s_columns FROM `poslavu_MAIN_db`.`restaurants` WHERE $s_dataname_where_clause";
		$data_query = mlavu_query($data_query_string, $a_datanames);
		if ($data_query) {
			$retval = array();
			while ($data_read = mysqli_fetch_assoc($data_query)) {
				$retval_index = count($retval);
				$retval[$retval_index] = array();
				foreach($data_read as $k=>$v) {
					$retval[$retval_index][] = $v;
				}
			}
			return $retval;
		}
		return $badval;
	}
	
	// get the datanames and hash
	$s_hash = $_POST['hash'];
	$a_datanames = array();
	for ($i = 0; $i < 100000; $i++) {
		if (!isset($_POST['dn'.$i]))
			break;
		$a_datanames[] = $_POST['dn'.$i];
	}
	$string_to_encrypt = implode('|', $a_datanames);
	// check that the hash is good (trusted source)
	$s_hash = get_secret_message_hash($string_to_encrypt);
	$trusted_source = FALSE;
	if (check_secret_message_hash($string_to_encrypt, $s_hash))
		$trusted_source = TRUE;
	if ($trusted_source) {
		$a_request_information = array('data_name', 'created', 'last_activity');
		$a_requested_information = get_requested_information($a_datanames, $a_request_information);
		foreach($a_requested_information as $k=>$v) {
			$a_requested_information[$k] = implode('|', $v);
		}
		echo implode('|||', $a_requested_information);
	}
?>
