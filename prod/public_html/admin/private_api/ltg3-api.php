<?php
error_reporting(E_ALL);
require_once(__DIR__.'/../cp/resources/lavuquery.php');
ini_set('display_errors','1');
if(empty($_REQUEST['dataname'])){
	echo 'E1';
	return;
}

if(empty($_REQUEST['command'])){
	echo 'E2';
	return;
}

if($_REQUEST['command'] == 'DatanameCheck'){
	if(false === ValidateDataname($_REQUEST['dataname'])){
		echo 'bad';
		return;
	}else{
		echo 'good';
		return;
	}
}


if($_REQUEST['command'] == 'DatanamesCheck'){
	$goodDNs = array();
	$badDNs  = array();
	$datanames = json_decode($_REQUEST['dataname']);
	foreach ($datanames as $dn) {
		if(false === ValidateDataname($dn)){
			$badDNs[] = $dn;
		}else{
			$goodDNs[] = $dn;
		}
	}
	$toReturn = array();
	$toReturn['good'] = $goodDNs;
	$toReturn['bad']  = $badDNs;
	echo json_encode($toReturn,JSON_PRETTY_PRINT);
	return;
}

function ValidateDataname($input,$fullInfo = false){
	if(!is_string($input)){return false;}
	if(strlen($input) < 1){return false;}
	$test = preg_replace("/[^\da-zA-Z\_]/", "", $input);
	if(0 != strcmp($test,$input)){return false;}
	$result = mlavu_query("SELECT `id`,`data_name`,`dev` FROM `poslavu_MAIN_db`.`restaurants` WHERE `data_name` = '{$input}' AND `disabled` = 0");
	while($resultRows = mysqli_fetch_assoc($result ) ){
		if($fullInfo){
			return $resultRows;
		}else{
			return $resultRows['id'];
		}
	}
	return false;
}

function GetAllLTG_asJSON($dataname,$locationID){
	if(defined('JSON_PRETTY_PRINT')){
		return json_encode(GetAllLTG($dataname,$locationID),JSON_PRETTY_PRINT);
	}else{
		return json_encode(GetAllLTG($dataname,$locationID) );
	}
}

function GetAllLTG_asPRINTR($dataname,$locationID){
	return print_r(GetAllLTG($dataname,$locationID),1);
}

function GetAllLTG($dataname,$locationID){
	$toReturn = array();
	lavu_connect_dn($dataname,"poslavu_".$dataname."_db");

	#Locations Table
	$query = "SELECT * FROM `locations` WHERE `id` = {$locationID};";
	$tempQuery = lavu_query($query);
	if (mysqli_num_rows($tempQuery) < 1) {
		error_log(__FILE__." No Location? : {$dataname} : {$locationID}");
		return false;
	}
	$toReturn['locations'] = mysqli_fetch_assoc($tempQuery);

	#Config Table (Smaller)
	$query = "SELECT `setting`,`value` FROM `config`  WHERE `location` = '{$locationID}';";
	$tempQuery = lavu_query($query);
	$temp = array();
	while($temp2 = mysqli_fetch_assoc($tempQuery)){
		$temp[] = $temp2;
	}
	$toReturn['config'] = $temp;

	$query = "SELECT * FROM `tax_rates`      WHERE `_deleted` = 0 AND `loc_id` = {$locationID};";
	$tempQuery = lavu_query($query);
	$temp = array();
	while($temp2 = mysqli_fetch_assoc($tempQuery)){
		$temp[] = $temp2;
	}
	$toReturn['tax_rates'] = $temp;

	$query = "SELECT * FROM `tax_profiles`   WHERE `_deleted` = 0 AND `loc_id` = {$locationID};";
	$tempQuery = lavu_query($query);
	$temp = array();
	while($temp2 = mysqli_fetch_assoc($tempQuery)){
		$temp[] = $temp2;
	}
	$toReturn['tax_profiles'] = $temp;

	$query = "SELECT * FROM `tax_exemptions` WHERE `_deleted` = 0 AND `loc_id` = {$locationID}";
	$tempQuery = lavu_query($query);
	$temp = array();
	while($temp2 = mysqli_fetch_assoc($tempQuery)){
		$temp[] = $temp2;
	}
	$toReturn['tax_exemptions'] = $temp;

	$menuID = $toReturn['locations']['menu_id'];
	if(!is_numeric($menuID)){
		error_log(__FILE__.print_r($toReturn['locations'],1));
		error_log(__FILE__." No valid Menu ID ? : {$dataname} : {$locationID}");
		return false;
	}

	$query = "SELECT * FROM `modifiers` WHERE `_deleted` = 0 AND `category` IN (SELECT `id` FROM `modifier_categories` WHERE `_deleted` = 0 )";
	$tempQuery = lavu_query($query);
	$temp = array();
	while($temp2 = mysqli_fetch_assoc($tempQuery)){
		$temp[] = $temp2;
	}
	$toReturn['modifiers'] = $temp;

	$query = "SELECT * FROM `modifier_categories` WHERE `_deleted` = 0 AND  `menu_id` = {$menu_id}";
	$tempQuery = lavu_query($query);
	$temp = array();
	while($temp2 = mysqli_fetch_assoc($tempQuery)){
		$temp[] = $temp2;
	}
	$toReturn['modifier_categories'] = $temp;

	$query = "SELECT * FROM `forced_modifiers` WHERE `_deleted` = 0 AND `list_id` IN (SELECT `id` FROM `forced_modifier_lists` WHERE `_deleted` = 0 )";
	$tempQuery = lavu_query($query);
	$temp = array();
	while($temp2 = mysqli_fetch_assoc($tempQuery)){
		$temp[] = $temp2;
	}
	$toReturn['forced_modifiers'] = $temp;

	$query = "SELECT * FROM `forced_modifier_groups` WHERE `_deleted` = 0";
	$tempQuery = lavu_query($query);
	$temp = array();
	while($temp2 = mysqli_fetch_assoc($tempQuery)){
		$temp[] = $temp2;
	}
	$toReturn['forced_modifier_groups'] = $temp;

	$query = "SELECT * FROM `forced_modifier_lists` WHERE `_deleted` = 0";
	$tempQuery = lavu_query($query);
	$temp = array();
	while($temp2 = mysqli_fetch_assoc($tempQuery)){
		$temp[] = $temp2;
	}
	$toReturn['forced_modifier_lists'] = $temp;

	$query = "SELECT * FROM `menu_items` WHERE `_deleted` = 0 AND `category_id` IN (SELECT `id` FROM `menu_categories` WHERE `_deleted` = 0 AND `group_id` IN (SELECT `id` FROM `menu_groups` WHERE `_deleted` = 0  AND `menu_id` = {$menuID}))";
	$tempQuery = lavu_query($query);
	$temp = array();
	while($temp2 = mysqli_fetch_assoc($tempQuery)){
		$temp[] = $temp2;
	}
	$toReturn['menu_items'] = $temp;

	$query = "SELECT * FROM `menu_categories` WHERE `_deleted` = 0 AND `group_id` IN (SELECT `id` FROM `menu_groups` WHERE `_deleted` = 0 AND `menu_id` = {$menuID})";
	$tempQuery = lavu_query($query);
	$temp = array();
	while($temp2 = mysqli_fetch_assoc($tempQuery)){
		$temp[] = $temp2;
	}
	$toReturn['menu_categories'] = $temp;

	$query = "SELECT * FROM `menu_groups` WHERE `_deleted` = 0 AND `menu_id` = {$menuID}";
	$tempQuery = lavu_query($query);
	$temp = array();
	while($temp2 = mysqli_fetch_assoc($tempQuery)){
		$temp[] = $temp2;
	}
	$toReturn['menu_groups'] = $temp;

	$query = "SELECT * FROM `menus` WHERE `id` = {$menuID}";
	$tempQuery = lavu_query($query);
	$temp = array();
	while($temp2 = mysqli_fetch_assoc($tempQuery)){
		$temp[] = $temp2;
	}
	$toReturn['menus'] = $temp;
	return $toReturn;
}
