<?php
    
    if( sha1( $_POST['data_name'] . "MrBeanyBaby") != $_POST['security_hash'] ){
        
        echo '{"status":"failure","reason":"not_permitted"}';
        exit();
    }

    require_once("/home/poslavu/public_html/admin/cp/resources/core_functions.php");
    $dba = 'poslavu_' . $_POST['data_name'] . '_db';
    if($_POST['info_request'] == 'zip_code'){
        $result = mlavu_query("SELECT * FROM `[1]`.`locations` ORDER BY `id` ASC LIMIT 1", $dba);
        if(!$result){
            echo '{"status":"failure","reason":"internal_error"}';
            exit();
        }
        if(mysqli_num_rows($result)){
            $row = mysqli_fetch_assoc($result);
            echo '{"status":"success","zip_code":"'.$row['zip'].'"}';
        }
        else{
            exit();
        }
    }
?>