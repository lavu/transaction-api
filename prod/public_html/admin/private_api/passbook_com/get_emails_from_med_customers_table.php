<?php

	
	// M E D _ C U S T O M E R S...........................................................----------------------------------------
	//Now we get all the appropriate med_customers.
	writeToLog("Now searching for med_customer emails.................................");
	
	$medCustomerGlobalList = array();//Array of arrays.  Each inner array has dataname as key.

	//We have the data_name list saved from before so we just iterate through it.
	foreach($dataNamesList as $currDataname){
	
    	$databaseName = 'poslavu_' . $currDataname . '_db';
    	$leftTimeMarkerForQuery = getLastEmailSearchTimeFromMedCustomersFromConfig($databaseName);
    	$emailColumnInMedCustomers = getConfigRowThatContains_GetCustomerInfo_LIKE_EMAIL($databaseName);
    	
        if($emailColumnInMedCustomers && $emailColumnInMedCustomers != ''){
            $newLeftMarkerDate = $leftTimeMarkerForQuery;//Used as out variable for following query
            $listOfEmailsFromDataname = queryMedCustomersTableForEmails($databaseName, 
                                                $leftTimeMarkerForQuery, $emailColumnInMedCustomers, $newLeftMarkerDate); 
            $medCustomerGlobalList[$currDataname] = $listOfEmailsFromDataname;
            insertNewMedCustomersSearchLeftMarkerValueIntoConfig($newLeftMarkerDate, $databaseName);
        }
	}
	
	$med_customer_table_post = json_encode($medCustomerGlobalList);
	/*
	if($isMonitoring){
    	writeToMonitoringLogMedCustomersTable();
	}
	*/
	
	
	
	
	
	function getLastEmailSearchTimeFromMedCustomersFromConfig($siteDatabaseName){
    
        //mktime(h,i,s,m,d,Y) returns the number of seconds since Jan 1, 1970 at 00:00:00.
        //Back one week.
	    $min_time = date("Y-m-d H:i:s",mktime(0,0,0,date("m"),date("d")-7,date("Y")));	
    
        //We pull the most recent email associated date from the config table.
		$lastTimeQuery = "SELECT `id`,`value2` FROM `[1]`.`config` WHERE `setting`='last_med_customers_table_email_mining' " .
		                 "ORDER BY `id` DESC LIMIT 1";
		$lastTimeResult = mlavu_query($lastTimeQuery, $siteDatabaseName);
		if(!$lastTimeResult){
    		error_log("Mysql error in 'get_emails_from_med_customers_table.php' trying to get: last_med_customers_table_email_mining");
    		return false;
		}
		$lastTimeReadFromConfig;
		if(mysqli_num_rows($lastTimeResult)){
		    $rowFromDB = mysqli_fetch_assoc($lastTimeResult);
    		$lastTimeReadFromConfig = $rowFromDB['value2'];
		}
		$final_min_time;//The min time that will be used in the orders table mining query.
		$final_min_time = !isset($lastTimeReadFromConfig) || $lastTimeReadFromConfig == '' ? $min_time : $lastTimeReadFromConfig;
		$final_min_time = $final_min_time < $min_time ? $min_time : $final_min_time;
		return $final_min_time;
    }
    
    function getConfigRowThatContains_GetCustomerInfo_LIKE_EMAIL($databaseName){
        $selectQuery = "SELECT `id`,`value2` FROM `[1]`.`config` WHERE `setting`='GetCustomerInfo' AND `value` LIKE '%email%'";
        $result = mlavu_query($selectQuery, $databaseName);
        if(!$result){
            error_log("Error in sql trying to get email GetCustomerInfo column in get_emails_from_med_customers_table.php, error: " . mlavu_dberror());
            return false;
        }
        
        $configRow = mysqli_num_rows($result) ? mysqli_fetch_assoc($result) : false;
        return $configRow ? $configRow['value2'] : false;
    }
    
    function queryMedCustomersTableForEmails($databaseName, $leftMarkerTimeInclusive, $columnNameInMedTable, &$outNewLeftMarkerMinTime){
        global $EMAIL_QUERY_LIMIT;

        $thisDatanamesEmailListToSend = array();//Array to send

        $med_c_emails_Query  = "SELECT `id`,`[1]`,`date_created` FROM `[2]`.`med_customers` WHERE " .
                               "`date_created` > '[3]' ORDER BY `date_created` ASC LIMIT [4]";
        $med_c_emails_Result = mlavu_query($med_c_emails_Query, $columnNameInMedTable, $databaseName, $leftMarkerTimeInclusive, $EMAIL_QUERY_LIMIT);
        if(!$med_c_emails_Result){
            error_log("SQL Syntax error in get_emails_from_med_customers_table.php 01");
            return false;
        }
        if(mysqli_num_rows($med_c_emails_Result)){
            while($currRow = mysqli_fetch_assoc($med_c_emails_Result)){
                $email = $currRow[$columnNameInMedTable];
                $dateCreated = $currRow['date_created'];
                $outNewLeftMarkerMinTime = ($dateCreated > $outNewLeftMarkerMinTime) ? $dateCreated : $outNewLeftMarkerMinTime;
                //Here we validate the email before going into the list.
                if(eregi("^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,3})$", $email)){
                    $thisDatanamesEmailListToSend[] = $email;
                }
                else{
                    //invalid email, we don't care for now.
                }
            }   
        }
        return $thisDatanamesEmailListToSend;
    }
    
    function insertNewMedCustomersSearchLeftMarkerValueIntoConfig($newLeftMarkerMinTime, $databaseName){
        //We have select to see if 'last_orders_table_email_mining' xists, then update else insert.
		$existsQuery = "SELECT `id` FROM `[1]`.`config` WHERE `setting`='last_med_customers_table_email_mining'";
		$existsResult = mlavu_query($existsQuery, $databaseName);
		if(!$existsResult){
    		error_log("sql error in 'get_emails_from_med_customers_table.php in insertNewLeftMarker function... 01 error: " . mlavu_dberror());
    		return false;
        }
		if(mysqli_num_rows($existsResult)){
    		//Exists, so we update the config table with the new 'last_orders_table_email_mining' entry.
    		$updateQuery = "UPDATE `[1]`.`config` SET `value2`='[2]' WHERE `setting`='last_med_customers_table_email_mining'";
    		$result = mlavu_query($updateQuery, $databaseName, $newLeftMarkerMinTime);
    		if(!$result){
        		error_log("sql error in 'get_emails_from_med_customers_table.php in insertNewLeftMarker function... 02 error: " . mlavu_dberror());
    		}
		}else{
    		//Doesn't exist so we do an insert on the 'last_orders_table_email_mining' entry.
    		$insertQuery  = "INSERT INTO `[1]`.`config` (`setting`, `value2`) VALUES('[2]','[3]')";
    		$result = mlavu_query($insertQuery, $databaseName, 'last_med_customers_table_email_mining', $newLeftMarkerMinTime);
    		if(!$result){
        		error_log("sql error in 'get_emails_from_med_customers_table.php in insertNewLeftMarker function... 03 error: " . mlavu_dberror());
    		}
		}
    }
    
    function writeToMonitoringLogMedCustomersTable(){
        $logString  = "\n...............................................................";
    	$logString .= "Hourly `med_customers` table email mining report...\n";
    	$logString .= "Count of datanames with new med_customer emails: " . count($medCustomerGlobalList) . "\n";
    	foreach($medCustomerGlobalList as $key => $val){
        	$logString .= "Dataname: [$key] has [" . count($val) ."] added emails in the med_customers table:";
        	if(count($val) == 1)
        	   $logString .= "[" . $val[0] . "]\n";
        	else if(count($val) == 2)
        	   $logString .= "[" . $val[0] . "," . $val[1] . "]\n";
            else if(count($val) == 3)
               $logString .= "[" . $val[0] . "," . $val[1] . "," . $val[2] . "]\n";
            else if(count($val) > 3)
               $logString .= "[" . $val[0] . "," . $val[1] . ", ...," . $val[count($val)-1] . "\n";
    	}
    	writeToLog($logString);
    }
?>
