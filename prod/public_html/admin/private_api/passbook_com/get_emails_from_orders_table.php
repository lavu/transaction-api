<?php

    writeToLog("\n\n");
    writeToLog("<>~-~<>~-~<>~-~<>~-~<>~-~<>~-~<>~-~<>~-~<>~-~<>~-~<>~-~<>~-~<>~-~<>~-~<>~-~<>~-~<>~-~<>~-~<>~-~<>~-~<>");
	writeToLog("Top of get_emails_from_orders_table.php script called at: " . date('Y-m-d H:i:s') );


	//The top level object containing all the `emails` from all of the `orders` tables.
	$ordersTableGlobal = array();//Array of arrays, where dataname is the key
	
	
	foreach($dataNamesList as $currDataName)
	{
    	$site_db = "poslavu_" . $currDataName . "_db";
		
		$final_min_time = getLastEmailsFromOrdersTimeFromConfig($site_db);
		$newLeftMarkerMinTime; //Used as an out variable.
		$currDataNameOrderEmails = queryOrdersTableForEmailsAfterTime($final_min_time, $site_db, $newLeftMarkerMinTime);
		insertNewOrdersSearchLeftMarkerValueIntoConfig($newLeftMarkerMinTime, $site_db);
		$ordersTableGlobal[$currDataName] = $currDataNameOrderEmails;
    	insertNewOrdersSearchLeftMarkerValueIntoConfig($newLeftMarkerMinTime, $site_db);
    	$order_table_post = json_encode($ordersTableGlobal);
    	/*
    	if($isMonitoring){
        	writeToMonitoringLogOrdersTable();
    	}
    	*/
	}
	
	
	function getLastEmailsFromOrdersTimeFromConfig($siteDatabaseName){
    
        //mktime(h,i,s,m,d,Y) returns the number of seconds since Jan 1, 1970 at 00:00:00.
        //Back one week.
	    $min_time = date("Y-m-d H:i:s",mktime(0,0,0,date("m"),date("d")-7,date("Y")));	
    
        //We pull the most recent email associated date from the config table.
		$lastTimeQuery = "SELECT `id`,`value2` FROM `[1]`.`config` WHERE `setting`='last_orders_table_email_mining' " .
		                 "ORDER BY `id` DESC LIMIT 1";
		$lastTimeResult = mlavu_query($lastTimeQuery, $siteDatabaseName);
		if(!$lastTimeResult){
    		error_log('Error: failed query trying to get the `last_orders_table_email_mining` value from the config of:' . 
    		  $site_db . '.  Error msg: ' . mlavu_dberror());
    		return false;
		}
		$lastTimeReadFromConfig;
		if(mysqli_num_rows($lastTimeResult)){
		    $rowFromDB = mysqli_fetch_assoc($lastTimeResult);
    		$lastTimeReadFromConfig = $rowFromDB['value2'];
		}
		$final_min_time;//The min time (left marker condition) that will be used in the orders table mining query.
		if(empty($lastTimeReadFromConfig)){
    		$final_min_time = $min_time;
		} else{
		    //We'll only read as far back as a week.
    		$final_min_time = ($lastTimeReadFromConfig > $min_time) ? $lastTimeReadFromConfig : $min_time;
		}
		return $final_min_time;
    }
    
    function queryOrdersTableForEmailsAfterTime($timeLeftLimitExclusive, $site_db, &$outNewLeftLimit){
        global $EMAIL_QUERY_LIMIT;
        //We query for all the emails in the `orders` table that were entered after/equal the $timeLeftLimitExclusive.
		$order_query_str  = "SELECT `email`,`closed` FROM `[1]`.`orders` WHERE `closed`>'[2]' " . 
                            "AND LENGTH(`email`) > 4 ORDER BY `closed` ASC LIMIT [3]";
		$order_query = mlavu_query($order_query_str, $site_db, $timeLeftLimitExclusive, $EMAIL_QUERY_LIMIT);
		if(!$order_query){
    		error_log('error in get_emails_and_send... .php, in query getting emails from orders table');
    		return false;
		}
		$emailRowsFromOrdersTable = array();
		while($order_read = mysqli_fetch_assoc($order_query)){
    		$emailRowsFromOrdersTable[] = $order_read;
		}
		
		$newConfigMinTime = $timeLeftLimitExclusive;//We default this value, but will be set to the latest in the `orders` table.
		
		//The array of emails to be sent for this dataname.
		$currDataNameOrderEmails = array();
		foreach($emailRowsFromOrdersTable as $currOrderRow)
		{
		   //We validate the time `closed` and the `email`.
		   //If validated we put the email into the list for this dataname.
		   //Then we update the newConfigMinTime to be stored in the merchant's config.
		   $currClosedTime = $currOrderRow['closed'];
		   $email = $currOrderRow['email'];
		   
		   $emailValid = eregi("^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,3})$", $email) ? true : false;

		   if( $emailValid ){
    		   $currDataNameOrderEmails[] = $currOrderRow['email'];//All emails in the orders per this dataname.
    		   $newConfigMinTime = ($currOrderRow['closed'] > $newConfigMinTime) ? $currOrderRow['closed'] : $newConfigMinTime;
		   }
		}
		$outNewLeftLimit = $newConfigMinTime;

		return $currDataNameOrderEmails;
    }
    
    function insertNewOrdersSearchLeftMarkerValueIntoConfig($newLeftMarkerMinTime, $databaseName){
        //We have select to see if 'last_orders_table_email_mining' xists, then update else insert.
		$existsQuery = "SELECT `id` FROM `[1]`.`config` WHERE `setting`='last_orders_table_email_mining'";
		$existsResult = mlavu_query($existsQuery, $databaseName);
		if(!$existsResult){
    		error_log("sql error in 'get_emails_from_orders_table.php in insertNewLeftMarker function... 01 error: " . mlavu_dberror());
    		return false;
        }
		if(mysqli_num_rows($existsResult)){
echo 'its found updating to: ' . $newLeftMarkerMinTime;
    		//Exists, so we update the config table with the new 'last_orders_table_email_mining' entry.
    		$updateQuery = "UPDATE `[1]`.`config` SET `value2`='[2]' WHERE `setting`='last_orders_table_email_mining'";
    		$result = mlavu_query($updateQuery, $databaseName, $newLeftMarkerMinTime);
    		if(!$result){
        		error_log("sql error in 'get_emails_from_orders_table.php in insertNewLeftMarker function... 02 error: " . mlavu_dberror());
    		}
		}else{
echo 'not found doing insert';
    		//Doesn't exist so we do an insert on the 'last_orders_table_email_mining' entry.
    		$insertQuery  = "INSERT INTO `[1]`.`config` (`setting`, `value2`) VALUES('[2]','[3]')";
    		$result = mlavu_query($insertQuery, $databaseName, 'last_orders_table_email_mining', $newLeftMarkerMinTime);
    		if(!$result){
        		error_log("sql error in 'get_emails_from_orders_table.php in insertNewLeftMarker function... 03 error: " . mlavu_dberror());
    		}
		}
    }
    
    function writeToMonitoringLogOrdersTable(){
        $logString = "\n\n\n\n...............................................................";
    	$logString = "Hourly `orders` table email mining report...\n";
    	foreach($ordersTableGlobal as $key => $val){
        	$logString .= "Dataname: [$key] has [" . count($val) ."] added emails in the orders table:";
        	if(count($val) == 1)
        	   $logString .= "[" . $val[0] . "]\n";
        	else if(count($val) == 2)
        	   $logString .= "[" . $val[0] . "," . $val[1] . "]\n";
            else if(count($val) == 3)
               $logString .= "[" . $val[0] . "," . $val[1] . "," . $val[2] . "]\n";
            else if(count($val) > 3)
               $logString .= "[" . $val[0] . "," . $val[1] . ", ...," . $val[count($val)-1] . "\n";
    	}
    	writeToLog($logString);
    }
?>
