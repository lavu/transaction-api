<?php

    ini_set('display_errors', 1);
    
    //If not manually ran either from browser or from bash script.
    if( empty($_GET['manual']) && (!isset($argv[1]) || $argv[1] != 'manual') ){
        exit();
    }


    //exit();
    set_time_limit(300);

    $EMAIL_QUERY_LIMIT = 1000;

	require_once("/home/poslavu/public_html/admin/cp/resources/core_functions.php");
	
	$isMonitoring = 1; //Whether to print out reports to the log.
	$stepPrint = 1;
	
	function writeToLog($str){
	    $logPath = dirname(__FILE__) . '/send_2_global_email_set.log';
    	$fp = fopen($logPath, 'a');
    	fwrite($fp, $str . "\n");
    	fclose($fp);
    	chmod($logPath, 777);
    }
    writeToLog("About to perform query for all new emails across `med_customers` and `orders` tables.");
    
    $getAllDataNamesQuery = "SELECT `data_name` FROM `poslavu_MAIN_db`.`restaurants` " .
                            "WHERE (`disabled`='0' OR `disabled`='') AND `notes` NOT LIKE '%AUTO-DELETED%' AND `data_Name`<>'' " .
                            "AND `data_name` IN (SELECT `dataname` FROM `poslavu_MAIN_db`.`restaurant_locations` WHERE `component_package_code`='')";
                            
    $allDataNamesSQLResult = mlavu_query($getAllDataNamesQuery);
    $dataNamesList = array();
    while($currRow = mysqli_fetch_assoc($allDataNamesSQLResult) ){
        $dataNamesList[] = $currRow['data_name'];
    }
    
    
    //The following two scripts actually pull the data from everyone in datanames list.----------------------------
	require_once(dirname(__FILE__) . '/get_emails_from_orders_table.php'); //Populates $order_table_post
	require_once(dirname(__FILE__) . '/get_emails_from_med_customers_table.php'); //Populates $med_customer_table_post
	//-----------------------------------------------------------------------------


	//We pass the information to the couponing server.
	//For now we loop through and do a post for each dataname.
	$url = 'https://www.lavutogo.com/v1/Passbook/_CrossServerScripts/receiveEmailList.php';
	$postString = 'jibberish=ajKf3lLs8h9lFjg0s8F4lhjsa198DmvDAmg9gs94sohkvs9s' . 
	   '&order_table_json='  . urlencode($order_table_post) . 
	   '&med_customer_json=' . urlencode($med_customer_table_post);
	$ch = curl_init($url);
	curl_setopt($ch, CURLOPT_POSTFIELDS, $postString);
	curl_setopt($ch, CURLOPT_POST, 1);
    $returnVal = curl_exec($ch);
    curl_close($ch);
    
    //We want to write the output from the curl to the log.
    if(isset($returnVal) && $returnVal != ''){
        writeToLog("CURL performed, url:[" . $url . "], value:[".$returnVal."]");
        writeToLog("-----------------------------------------------------------");
        writeToLog("\n\n\n\n\n\n\n\n\n\n\n");
    }
?>