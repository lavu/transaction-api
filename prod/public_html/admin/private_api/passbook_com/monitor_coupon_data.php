<?php
    
    
    
    //require_once('/home/poslavu/public_html/admin/objects/)
    require_once(dirname(__FILE__) . '/../../cp/objects/json_decode.php');
    
    $thisWebpath = $_SERVER['SERVER_NAME'] . substr(__FILE__, strlen($_SERVER['DOCUMENT_ROOT']) );

    //echo $thisWebpath; 
    //exit();

    if(!empty($_POST['is_ajax_call'])){
        handleAjaxRequest();
    }else{
        echo getHTMLInsert();
    }
    

    
    function getHTMLInsert(){
        global $thisWebpath;
?>
        <input type='input' id='dataname_input_field'></input>
        <input type='button' onclick='makeCallToGetCouponingData()'></input> <br>
        <div id='passbook_monitoring_display_div'></div>
        <script type='text/javascript'>

        function makeCallToGetCouponingData(){
        
            var dataname = document.getElementById('dataname_input_field').value;
            var xmlhttp;
            if (window.XMLHttpRequest){
                xmlhttp=new XMLHttpRequest();
            }else{
                xmlhttp=new ActiveXObject("microsoft.XMLHTTP");
            }
            
            xmlhttp.onreadystatechange=function(){
                //alert('Ready state: ' + xmlhttp.readyState + '  ' + 'Status:' + xmlhttp.status );
                if(xmlhttp.readyState == 4 && xmlhttp.status == 200){
                
                    var canvas = document.getElementById('passbook_monitoring_display_div');
                    canvas.innerHTML = xmlhttp.responseText;
                    //alert("Response: " + xmlhttp.responseText);
                }
            }
            
            var url='<?="https://".$thisWebpath?>';
            var params = "is_ajax_call=1&data_name=" + dataname;
            xmlhttp.open("POST", url, true);
            xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
            xmlhttp.send(params);
            
        }
        </script>
<?php
    }
    

    
    
    function getCouponingDataForMerchant($dataname){
        $postArr = array();
        $postArr['security_hash'] = sha1($dataname . "BAKED_BEANS_AND_SALTED_HASH");
        $postArr['data_name'] = $dataname;
        $url = "https://www.lavutogo.com/v1/Passbook/_CrossServerScripts/cross_server_monitoring.php";
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_POST, 1);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $postArr);
        $returnContent = curl_exec($curl);
        curl_close($curl);
        //return $returnContent;
        $returnContentArr = Json::unencode($returnContent);
        return $returnContentArr;
    }
    
    function handleAjaxRequest(){
        $dataArr = getCouponingDataForMerchant($_POST['data_name']);
        $dataArr = (array)$dataArr;
        
        $merchantInfo = $dataArr['merchant_info'];
        $couponClasses = $dataArr['coupon_classes'];
        
        $returnHTMLData = '';
        $returnHTMLData .= createMerchantInfoHTML($merchantInfo);
        $returnHTMLData .= createCouponClassesHTML($couponClasses);
        
        echo $returnHTMLData;
    }
    
    function createMerchantInfoHTML($merchantInfo){
        $merchantInfo = (array)$merchantInfo;
        
        $htmlString = "Merchant Info <br>";
        $htmlString .= "<table border='1'> <tr>";
        //We first build the top labels
        foreach($merchantInfo as $key => $val){
            $htmlString .= "<td><div>$key</td></div>";
        }
        $htmlString .= "</tr><tr>";
        foreach($merchantInfo as $key => $val){
            $htmlString .= "<td><div>$val</td></div>";
        }
        $htmlString .= "</tr>";
        $htmlString .= "</table>";
        $htmlString .= "<br><br><br>";
        return $htmlString;
    }
    
    function createCouponClassesHTML($couponClasses){
        $couponClasses = (array)$couponClasses;
        $htmlString  = "Coupon Classes";
        if(count($couponClasses) == 0){
            return "NO COUPON CLASSES";
        }
        $htmlString .= "<table border='1'> <tr>";
        $arrForKeys = $couponClasses[0];
        //The headers
        foreach($arrForKeys as $key => $val){
            $htmlString .= "<td><div>$key</div></td>";
        }
        
        //The values
        foreach($couponClasses as $currCouponClass){
            $currCouponClass = (array)$currCouponClass;
            $htmlString .= "<tr>";
            foreach($currCouponClass as $field){
                $htmlString .= "<td><div>$field</div></td>";
            }
            $htmlString .= "</tr>";
        }
        $htmlString .= "</table>";
        return $htmlString;
    }
    
    
    
    
    
    
    /*
    //Now we format the data that will be returned and inserted into a div as innerHTML.
        $returnHTMLData = "";
        $returnHTMLData .= "Coupon Data For: " . $_POST['data_name'] . "<br><br>";
        $returnHTMLData .= "<table border='1'>";
        //For each table to report
        /*
        foreach($dataArr as $dataItemKey => $dataItemVal){
            $returnHTMLData .= "<tr><div>$dataItemKey";
            if(is_array($dataItemVal) || is_object($dataItemVal)){
                $dataItemVal = (array)$dataItemVal;
                $returnHTMLData .= "<table border='1'>";
                //Need to loop through and collect all the keys as they are the headers
                $returnHTMLData .= "<tr>";
                foreach($dataItemVal as $dataItemSpecKey => $dataItemSpecVal){
                    $returnHTMLData .= "<td>$dataItemSpecKey</td>";    
                }
                $returnHTMLData .= "</tr>";
                //Now the actual rows.
                foreach($dataItemVal as $dataItemSpecKey => $dataItemSpecVal){
                    $dataItemSpecVal = (array)$dataItemSpecVal;
                    //$returnHTMLData .= "<tr>";
                    $returnHTMLData .= "<tr>";
                    foreach($dataItemSpecVal as $key => $item)
                        $returnHTMLData .= "<td><div>".$key."</div></td>";
                    $returnHTMLData .= "</tr>";
                    foreach($dataItemSpecVal as $item)
                        $returnHTMLData .= "<td><div>".$item."</div></td>"; 
                    //$returnHTMLData .= "</tr>";
                }
                $returnHTMLData .= "</table>";
                $returnHTMLData .= "<br><br><br>";
            }else{
                $returnHTMLData .= $dataItemVal;
            }
            $returnHTMLData .= "</div></tr>";
        }
        
    */
?>
