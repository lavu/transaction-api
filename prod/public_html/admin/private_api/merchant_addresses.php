<?php
    //Originally written by Brian D.
    //Purpose of this script is to communicate out a list of all known merchants
    //and their respective address.  This is so the we may keep a merchant geolocation list
    //active on the lavutogo server.  Essentially, we are going to use the 
    //lavutogo database to hold a table of all datanames with additional indices by lat/lon
    //to be used by peripheral apps.
    
    //For mlavu_query().
    require_once('/home/poslavu/public_html/admin/cp/resources/core_functions.php');
    
    //We need password to access this script, and should only be an ssl post, to 
    //keep keyword protected.
    if($_POST['jibberish'] != 'aj8jLjf3kKhLFk91037HfjalUHKkgjAGDHF836fk00DK3dk23kKf5242'){
    	exit();
    }
    
    //We expect a 'left' parameter, meaning we will return all datanames greater than this value,
    // this is so we procure the list iteratively and not bash the sql server nor general network.
    //On first call the left marker will be '', which is less than all other strings.
    if( !isset($_REQUEST['left_marker']) ){
        exit();
    }
    $leftMarker = $_REQUEST['left_marker'];
    //echo 'left marker: ' . $leftMarker;
    
    //Query to get all datanames and their formatted address, including devs.
    $dataNamesQuery = "SELECT `data_name` FROM `poslavu_MAIN_db`.`restaurants` WHERE `data_name` > '[1]' LIMIT 300";
    
    $dataNamesResult = mlavu_query($dataNamesQuery, $leftMarker);
    
    $allDataNamesArr = array();
    while($currAssoc = mysqli_fetch_assoc($dataNamesResult)){
        $allDataNamesArr[] = $currAssoc['data_name'];
    }
    //echo implode("<br>", $allDataNamesArr);
    
    
    //We'll put the output in JSON format
    $globalJSONObject = '{';
    
    
    //Now for every dataname we need to query its conf to get the formatted address.
    foreach($allDataNamesArr as $currDataName){
        $query = "SELECT `value_long` FROM `[1]`.`config` WHERE `setting`='locationData'";
        $result = mlavu_query($query, 'poslavu_' . $currDataName . '_db');
        if(mysqli_num_rows($result)){
            $resultArr = mysqli_fetch_assoc($result);
            $formattedAddress = $resultArr['value_long'];
            if(isset($formattedAddress) && $formattedAddress && $formattedAddress != ''){
                $globalJSONObject .= '"' . $currDataName . '":' . $formattedAddress . ',';
            }
        }
    }
    
    
    $globalJSONObject[strlen($globalJSONObject)-1] = '}';
    
    echo $globalJSONObject;
    
?>