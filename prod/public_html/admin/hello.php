<?php

// Do not instrument this high throughput transaction that is skewing the application average response time
if (extension_loaded('newrelic') && function_exists('newrelic_ignore_transaction')) { // Ensure PHP agent is available
    newrelic_ignore_transaction();
}
if(isset($_REQUEST['json']) && $_REQUEST['json']){
    $response = array("message"=>"UP","payload"=>array("extra"=>array("code"=>200)));
    echo json_encode($response);
}else{
    echo "Hello"; // IMPORTANT: Do not delete!
}
?>