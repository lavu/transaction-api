<?php
// This page is used to enable notice and warning error on local development environment
if (getenv('ENVIRONMENT') == 'LocalDev') {
    ini_set("display_errors", '1');
    error_reporting(E_ALL);
} else {
    error_reporting(E_ALL & ~E_NOTICE & ~E_DEPRECATED & ~E_WARNING);
}