<?php
header("Content-Type:application/json");
define("BEARER", "ee667341b471150f5c314f6fe554ba88");
$serviceName = $_GET['servicename'];
if (!empty($serviceName))
{
	require_once ("/home/poslavu/private_html/scripts/monitorServices.php");
	$object = new monitorServices();
	$reflector = new \ReflectionObject($object);
	$validate = 0;
	$headers = getAuthorizationHeader(); 
	if (!empty($headers)) {
		if (preg_match('/Bearer\s(\S+)/', $headers, $matches)) {
			$headers = $matches[1];
		}
	}
	if ( $headers === BEARER) {
		$validate = 1;
	}
	//echo $headers;exit;
	if ($validate) {
		if ($serviceName == 'lavutogostatus' && isset($_GET['dataname']) && !empty($_GET['dataname'])) {
			$funcName = 'get_'.$serviceName.'bydn';
		} else {
			$funcName = 'get_'.$serviceName;
		}
		try {
			$method = $reflector->getMethod($funcName);
			$method->setAccessible(true);
			if (isset($_GET['dataname']) && !empty($_GET['dataname'])) {
				$getData = $method->invoke($object, $_GET['dataname']);
			} else {
				$getData = $method->invoke($object);
			}
			$status = ($getData['status'] == 'up') ? 200 : 503; 
			response($status, $getData['data']);
		} catch(exception $exp) {
			$errMsg = str_replace(array('Method', 'get_'), array('Service', ''), $exp->getMessage());
			response(503, $errMsg);
		}
	}
	else {
		response(503, "Invalid Request");
	}
}
else {
	response(400, "Invalid Request", null);
}

function response($status, $data)
{
	header("HTTP/1.1 ".$status);

	$response['status'] = $status;
	$response['data'] = $data;

	$json_response = json_encode($response);
	echo $json_response;
}

function getAuthorizationHeader(){
	$headers = null;
	if (isset($_SERVER['Authorization'])) {
		$headers = trim($_SERVER["Authorization"]);
	}
	else if (isset($_SERVER['HTTP_AUTHORIZATION'])) { //Nginx or fast CGI
		$headers = trim($_SERVER["HTTP_AUTHORIZATION"]);
	} elseif (function_exists('apache_request_headers')) {
		$requestHeaders = apache_request_headers();
		// Server-side fix for bug in old Android versions (a nice side-effect of this fix means we don't care about capitalization for Authorization)
		$requestHeaders = array_combine(array_map('ucwords', array_keys($requestHeaders)), array_values($requestHeaders));
		//print_r($requestHeaders);
		if (isset($requestHeaders['Authorization'])) {
			$headers = trim($requestHeaders['Authorization']);
		}
	}
	return $headers;
}

function getBearerToken() {
	$headers = $this->getAuthorizationHeader();
	// HEADER: Get the access token from the header
	if (!empty($headers)) {
		if (preg_match('/Bearer\s(\S+)/', $headers, $matches)) {
			return $matches[1];
		}
	}
	return null;
}