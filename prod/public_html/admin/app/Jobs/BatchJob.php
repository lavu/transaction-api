<?php
namespace App\Jobs;

use App\Controller\LavuPayController;
use Symfony\Component\HttpFoundation\Request;
use \MarshallingArrays;
use \Lavu\Queue\Queue;
use \Lavu\Support\Recurrence;

require_once(dirname(__FILE__) . "/../Http/Controllers/LavuPayController.php");
require_once(dirname(__FILE__) . "/../../vendor/autoload.php");
require_once(dirname(__FILE__) . "/../../cp/resources/lavuquery_base.php");
require_once(dirname(__FILE__) . "/../../api_private/Marshalling/MarshallingArrays.php");
require_once(dirname(__FILE__) . "/../../lib/Lavu/Queue/Queue.php");
require_once(dirname(__FILE__) . "/../../lib/Lavu/Support/Recurrence.php");

class BatchJob
{
    public $dataname;
    public $locationId;
    public $jobRruleRaw;
    public $container;
    public $companyInfo;
    public $locationInfo;
    public $rruleSet;
    private $datanameRrule;



    public function __construct($container, $dataname = null, $locationId = null, $jobRruleRaw = null)
    {
        $this->container   = $container;
        $this->dataname    = $dataname;
        $this->locationId  = $locationId;
        $this->jobRruleRaw = $jobRruleRaw;
    }

    /**
     * Suppress StaticAccess warning
     *
     * @SuppressWarnings(PHPMD.StaticAccess)
     */

    public function fire($job, $data)
    {
        $this->dataname    = $data['dataname'];
        $this->locationId  = $data['locationId'];
        $this->jobRruleRaw = $data['jobRruleRaw'];
        error_log("Working Batch Job for " . $this->dataname . " and location " . $this->locationId . " scheduled at " . $this->jobRruleRaw);
        if ($this->shouldRunJob()) {
            $this->companyInfo  = $this->getCompanyInfo($this->dataname);
            $this->locationInfo = $this->getLocationInfo($this->locationId);
            LavuPayController::route($this->request(), $this->companyInfo, $this->locationInfo);
            $this->scheduleNext();
        }
        $job->delete();
    }

    /**
     * Suppress StaticAccess warning
     *
     * @SuppressWarnings(PHPMD.StaticAccess)
     */

    public function request()
    {
        // Mock request
        $request = Request::createFromGlobals();
        $request->attributes->set('action', 'settle');
        $request->attributes->set('loc_id', $this->locationId);
        return $request;
    }

    /*
        TODO:
        Move this to a location and company that inherids from eloquent model
     */
    public function getCompanyInfo($dataname)
    {
        $get_company_info = mlavu_query("SELECT `id`, `data_name`, `company_name`, `company_code`, `logo_img`, `receipt_logo_img`, `dev`, `demo`, `test`, `disabled`, `chain_id`, `lavu_lite_server`, `email` FROM `poslavu_MAIN_db`.`restaurants` WHERE `data_name` = '[1]'", $dataname);
        if (mysqli_num_rows($get_company_info) > 0) {
            $company_info = mysqli_fetch_assoc($get_company_info);
            if ($company_info['disabled']=="1" || $company_info['disabled'] == "2" || $company_info['disabled'] == "3") {
                lavu_echo('{"json_status":"AccountDisabled"}');
            }
        } else {
            lavu_echo('{"json_status":"NoRestaurants"}');
        }
        return $company_info;
    }

    /**
     * Suppress ShortVariable warning
     * Getting location info with a param id is perfectly valid
     *
     * @SuppressWarnings(PHPMD.ShortVariable)
     */
    public function getLocationInfo($id){
        // these globals are required for lavu query to properly resolve the correct shard
        global $data_name;
        global $locId;
        $data_name = $this->dataname;
        $locId     = $id;

		$locationsResult = lavu_query("SELECT * FROM `locations` WHERE `id`='[1]'", $locId );
		if ($locationsResult && mysqli_num_rows($locationsResult)) {
			return mysqli_fetch_assoc($locationsResult);
		} else {
			return false;
		}
    }

    /**
     * Check to see if the recurrence has changed in the DB since this job was created
     *
     * @return boolean - true if the recurrence has changed since this job was created
     *
     * Suppress StaticAccess warning
     *
     * @SuppressWarnings(PHPMD.StaticAccess)
     */
    public function isJobStale()
    {
        // is the jobs $rrule the same as the $rrule in the db
        return !( $this->jobRruleRaw === $this->getRecurrenceSettings() );
    }

    /**
     * We only want to fire the job if the time too run (this is calculated based on the recurrence setting) is up to date
     * If the recurrence setting is not accurate on the job then the job is stale and should not be run.
     *
     * @return boolean
     */
    public function shouldRunJob()
    {
        return !$this->isJobStale();
    }

    /**
     * Suppress StaticAccess warning
     *
     * @SuppressWarnings(PHPMD.StaticAccess)
     */
    public function getRruleSet()
    {
        if (!$this->rruleSet) {
            $rrule          = $this->getRecurrenceSettings();
            $this->rruleSet = $rrule ? Recurrence::generateRruleSet($rrule) : false;
        }
        return $this->rruleSet;
    }

    /**
     * Suppress StaticAccess warning
     *
     * @SuppressWarnings(PHPMD.StaticAccess)
     */
    public function getNextOccurrence()
    {
        $rrule = $this->getRecurrenceSettings();
        return $rrule ? Recurrence::getNextOccurrence($rrule) : false;
    }

    public function scheduleNext()
    {
        $next  = $this->getNextOccurrence();
        $queue = new Queue();
        $data  = new BatchJob(null, $this->dataname, $this->locationId, $this->jobRruleRaw);
        error_log("Scheduling Next Batch Job to run at " . $this->jobRruleRaw);
        $queue->later($next, 'BatchJob', $data);
    }

    /**
     *
     * Query the Config table for the recurrence settings
     * TODO: update this to use the LavuModel querying capability.
     *
     * @return string rrule
     *
     * Suppress StaticAccess warning
     *
     * @SuppressWarnings(PHPMD.StaticAccess)
     */
    public function getRecurrenceSettings()
    {
        $query            = "SELECT `setting`, `value` FROM `config` where `setting` in ('[1]', '[2]')";
        $autoBatchEnabled = 'lavupay_auto_batch_enabled';
        $batchRrule       = 'lavupay_batch_rrule';

        if (!$this->datanameRrule) {
            // these globals are required for lavu query to properly resolve the correct shard
            global $data_name;
            global $locId;
            $data_name       = $this->dataname;
            $locId           = $this->locationId;
            $results         = array();
            $locationsResult = lavu_query($query, $autoBatchEnabled, $batchRrule );

            if ($locationsResult && mysqli_num_rows($locationsResult)) {
                while ($row = MarshallingArrays::resultToArray($locationsResult)) {
                    $results = array_merge($results, $row);
                }
                $autoBatchValue = $results[0]['value'];
                $rruleString    = $results[1]['value'];
                if ($this->usesAutoBatch($autoBatchValue) && $rruleString) {
                    $this->datanameRrule =  $rruleString;
                } else {
                    $this->datanameRrule = null;
                }
            } else {
                $this->datanameRrule = null;
            }
        }
        return $this->datanameRrule;
    }

    private function usesAutoBatch($value){
        return $value === '1';
    }
}
