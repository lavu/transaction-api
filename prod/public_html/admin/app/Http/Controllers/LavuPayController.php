<?php
namespace App\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Lavu\Payments\CreditCardTransaction;
use PaymentGateways\CardConnect\CardInput\ManualEntry;
use PaymentGateways\CardConnect\Payment;
use PaymentGateways\CardConnect\TenantAccount;

require_once(dirname(__FILE__) . "/../../../vendor/autoload.php");
require_once(dirname(__FILE__) . "/../../../lib/Lavu/Payments/CreditCardTransaction.php");
require_once(dirname(__FILE__) . "/../../../lib/gateway_lib/cardconnect/constants.php");
require_once(dirname(__FILE__) . "/../../../lib/gateway_lib/cardconnect/tenant_account.php");
require_once(dirname(__FILE__) . "/../../../lib/gateway_lib/cardconnect/card_input/manual_entry.php");
require_once(dirname(__FILE__) . "/../../../lib/gateway_lib/cardconnect/payment.php");

class LavuPayController
{
    const GATEWAY_NAME = 'LavuPay';
    public $tenantAccount;

    public function tenantAccount() {
        if (!$this->tenantAccount) {
            $this->tenantAccount = new TenantAccount($this->dataname, $this->locationId);
        }
        return $this->tenantAccount;
    }

    public function gateway(){
        $account = $this->tenantAccount();
        $cardConnectGateway = $account->cardConnectGateway();
        return array(
            "type" => \PaymentGateways\CardConnect\CARDCONNECT_GATEWAY_NAME,
            "data" => array(
                "mid"   => $cardConnectGateway->gatewayAccountId,
                "auth"  => $cardConnectGateway->gatewayAuthToken
            )
        );
    }
    /**
     * Captures all _unadjusted/uncaptured_ transactions
     *
     * @param array         $companyInfo
     * @param array         $locationInfo
     * @param int|string    $locationId
     *
     * @return Symfony\Component\HttpFoundation\JsonResponse
     */
    public function captureAll(array $companyInfo, array $locationInfo, $locationId=1)
    {
        $uncaptured = CreditCardTransaction::uncaptured($locationId)
                        ->get(['cc_transactions.*']);

        // any uncaptured transactions?
        if ($uncaptured->count() > 0) {
            // _adjust_ each uncaptured payment
            $uncaptured->each(function($item) use ($companyInfo, $locationInfo) {
                $tmp = $item->toArray();
                $tmp['pnref'] = $item->preauth_id;
                $tmp['data_name'] = $companyInfo['data_name'];
                $tmp['card_amount'] = isset($tmp['card_amount']) ? $tmp['card_amount'] : '0';
                (new Payment(new ManualEntry($tmp, $locationInfo, $this->gateway())))->adjust();

                // update item to indicate it's been captured
                $item->auth = '0';
                $item->last_mod_ts = time();
                $item->save();
            });
        }

        $response = array(
            'transactions' => array(
                'captured' => $uncaptured->count()
            )
        );

        return new JsonResponse($response);
    }

    /**
     * Captures all _unadjusted/uncaptured_ transactions and
     * requests Payment API (PAPI) to close batch.
     *
     * @param array         $companyInfo
     * @param array         $locationInfo
     * @param int|string    $locationId
     *
     * @return Symfony\Component\HttpFoundation\JsonResponse
     */
    public function settle(array $companyInfo, array $locationInfo, $locationId=1)
    {
        $transactions   = CreditCardTransaction::between()
                            ->where('cc_transactions.gateway', '=', self::GATEWAY_NAME)
                            ->where('cc_transactions.processed', '=', '0');

        // any transactions pending processing?
        if ($transactions->count() > 0) {
            // capture all uncaptured/pending transactions
            $this->captureAll($companyInfo, $locationInfo, $locationId);

            // issue batch command
            $pmt =  new Payment(
                        new ManualEntry(
                            array(
                                'data_name' => $companyInfo['data_name'],
                                'loc_id' => $locationId
                            ),
                            [],
                            $this->gateway()
                        )
                    );
            $batchResponse = $pmt->batch();
            if ($batchResponse && $batchResponse->status === Response::HTTP_ACCEPTED) {
                // construct response before making changes
                $response = array(
                    'batch' => array(
                        'chargetotal' => $transactions->sum('cc_transactions.amount'),
                        // not implemented
                        'refundtotal' => 0,
                        'transactions' => array(
                            'count' => $transactions->count()
                        )
                    )
                );

                // once the payload is created, update records
                $transactions->update([
                    'cc_transactions.processed' => 1,
                    'cc_transactions.last_mod_ts' => time()
                ]);
            } else {
                $message = \PaymentGateways\CardConnect\UNKNOWN_ERROR;
                $responseArray = explode('|', $batchResponse);
                if ($responseArray) {
                    $message = $responseArray[1];
                }

                $response = array(
                    'error' => $message
                );
            }
        } else {
            $response = array(
                'error' => \PaymentGateways\CardConnect\NOTHING_TO_BATCH_ERROR
            );
        }

        return new JsonResponse($response);
    }

    /**
     * Returns the number of credit card transactions
     * that are still _uncaptured_.
     *
     * @param int|string $locationId
     *
     * @return Symfony\Component\HttpFoundation\JsonResponse
     */
    public function getUncapturedCount($locationId=1)
    {
        try {
            $payload = array(
                'count' => CreditCardTransaction::uncaptured($locationId)
                    ->where('cc_transactions.gateway', '=', self::GATEWAY_NAME)
                    ->count()
            );

            // close connections if necessary
			mlavu_close_db();
			lavu_close_db();

			return new JsonResponse($payload);
		} catch (Exception $err) {
			error_log($err);
			return new JsonResponse(array('error' => $err->getMessage()));
		}
    }

    /**
     * Returns information on transactions to be _batched_
     *
     * @param int|string $locationId
     *
     * @return Symfony\Component\HttpFoundation\JsonResponse
     */
    public function getBatchDetails($locationId=1)
    {
        try {
            $uncaptured = CreditCardTransaction::uncaptured($locationId)
                            ->where('cc_transactions.gateway', '=', self::GATEWAY_NAME);
            $transactions = CreditCardTransaction::between()
                            ->where('cc_transactions.gateway', '=', self::GATEWAY_NAME)
                            ->where('cc_transactions.processed', '=', '0');
            $payload = array(
                'batch' => array(
                    'transactions' => array(
                        'unadjusted' => array(
                            'count' => $uncaptured->count()
                        ),
                        'count' => $transactions->count()
                    ),
                )
            );

            // close connections if necessary
			mlavu_close_db();
			lavu_close_db();

			return new JsonResponse($payload);
		} catch (Exception $err) {
			error_log($err);
			return new JsonResponse(array('error' => $err->getMessage()));
		}
    }

    /**
     * Handles an inbound request to LavuPayController,
     * this is a workaround because the app doesn't have
     * classic _Routing_.
     *
     * Should be removed if Routing is ever implemented.
     *
     * @param Symfony\Component\HttpFoundation\Request $request
     * @param array $companyInfo
     * @param array $locationInfo
     *
     * @return Symfony\Component\HttpFoundation\JsonResponse
     */
    public static function route(Request $request, array $companyInfo, array $locationInfo)
    {
        $response;
        $locationId = $request->get('loc_id');
        switch ($request->get('action')) {
            case 'details':
                $response = (new static)->getBatchDetails($locationId);
                break;
            case 'settle':
                $response = (new static)->settle($companyInfo, $locationInfo, $locationId);
                break;
            case 'uncaptured-count':
                $response = (new static)->getUncapturedCount($locationId);
                break;
            default:
                $response = new JsonResponse(
                    array(
                        "error" => Response::$statusTexts[Response::HTTP_NOT_FOUND]
                    ),
                    Response::HTTP_NOT_FOUND
                );
        }

        $respondsToHtml = in_array($request->getMimeType('html'), $request->getAcceptableContentTypes());

        if (isset($response) && !$respondsToHtml) {
            $response->send();
        } else {
            return $response;
        }
    }
}