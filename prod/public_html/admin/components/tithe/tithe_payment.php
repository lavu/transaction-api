<?php

?>

	<style type='text/css'>
	
		.submit_keyed_in {
			color:#FFFFFF;
			font-size:30px;
			text-shadow: 1px 1px 0px rgba(0, 0, 0, 0.5);
			width:436px;
			height:71px;
			background:url("images/btn_436x71.png");
			border:hidden;
		}
		
		.input_long {
			width:304px;
			height:45px;
			background:url("images/bg_input_field.png");
			background-repeat:no-repeat;
			background-position:center;
			border:hidden;
			text-indent:8px;
			font-size:24px;
			padding-top:7px;
		}
		
		.input_short {
			width:100px;
			height:45px;
			background:url("images/bg_input_field_sm.png");
			background-repeat:no-repeat;
			background-position:center;
			border:hidden;
			text-indent:8px;
			font-size:24px;
			padding-top:7px;
		}	
		
	</style>
	
	<script type="text/javascript">
	
		function startListening() {
		
			window.location = '_DO:cmd=startListeningForSwipe';
		}
		
		function stopListening() {
		
			window.location = '_DO:cmd=stopListeningForSwipe';
		}
		
		function handleCardSwipe(swipeString, dataString, reader, encrypted){
			alert( reader + ": " + swipeString );
			return 1;
		}
		
		function openInAppNumberPad(type, max_digits, title, field_id) {
		
			window.location = "_DO:cmd=number_pad&type=" + type +"&max_digits=" + max_digits + "&title=" + encodeURIComponent(title) +"&c_name=tithe&send_cmd=inputNumberFromApp(%27" + field_id + "%27,%27ENTERED_NUMBER%27)";
		}
		
		function inputNumberFromApp(field_id, value) {
		
			document.getElementById(field_id).value = value;
		}
		
	</script>
		
	<div style='position:relative; left:0px; top:0px; width:1024px; height:180px; background-color:#4e4e4e; opacity:0.3;'></div>
	
	<div style='position:relative; left:50px; top:-140px; width:200px; height:50px; background-color:#000099; z-index:10000'>
	
	
	</div>

	<div style='position:relative; left:412px; top:-210px; width:590px; height:50px; text-align:right;'>
		<span class='page_title'>Name of Church</span><br><br>
		<span class='page_subtitle'>Make Payment</span>
	</div>
	
	<center><span style='position:relative; left:0px; top:30px; width:1024px; height:50px; color:#FFFFFF; font-size:48px; text-shadow: 1px 1px 0px rgba(0, 0, 0, 0.5)'>Please swipe your card</span></center>
	
	<div style='position:relative; left:0px; top:75px; width:1024px; height:200px;'>
		<center>
			<form onsubmit='alert("Gots to post this card infos...");'>
			
				<input class='submit_keyed_in' type='submit' value='SUBMIT KEYED IN INFO'><br><br><br>
				<input id='input_cn' class='input_long' type='tel' placeholder='CARD NUMBER' value='' READONLY onclick='openInAppNumberPad("multi_digit", "16", "Card Number", "input_cn");'>
				<input id='input_exp' class='input_short' type='tel' placeholder='EXP' value='' READONLY onclick='openInAppNumberPad("multi_digit", "16", "Expiration Date", "input_exp");'>
				<input id='input_cvn' class='input_short' type='tel' placeholder='CVN' value='' READONLY onclick='openInAppNumberPad("multi_digit", "16", "Verification Number", "input_cvn");'>
			
			</form>
		</center>
	</div>