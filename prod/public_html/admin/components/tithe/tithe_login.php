
	<div style='position:absolute; left:50px; top:25px; width:250px; height:25px; vertical-align:middle; display:block;' ontouchstart='showPage("tithe_welcome");'>
		<img src='images/arrow_back.png' width='12px' height='20px'> &nbsp;
		<span class='back_button_label'>GO BACK</span>
	</div>
	
	<div style='position:relative; left:412px; top:25px; width:580px; height:25px; text-align:right; vertical-align:middle; display:block;' ontouchstart='showPage("tithe_create_account");'>
		<span class='back_button_label'>CREATE ACCOUNT</span>
	</div>

	<center><span class='page_title' style='position:absolute; left:0px; top:70px; width:1024px; height:70px;'><?php echo $location_name; ?></span></center>
	
<?php 

	if(empty($_REQUEST['cc'])){
		echo 'Set the needed information for setting account';
		exit;
	}

	$security_hash = sha1($dataname . 'garbonzo');
	$loginAjaxLocation = "tithe_login.php";

	if(empty($_REQUEST['is_ajax'])){
		require_once(dirname(__FILE__).'/views/login.html.php');
	}
	
	$tmp_buffer = !empty( $_REQUEST['is_ajax'] ) && !empty( $_REQUEST['cmd'] )? ob_get_clean():"";
	//Handling the post back types.
	if(!empty($_REQUEST['is_ajax'])){
		if(empty($_REQUEST['security_hash'])){
			echo "Missing security hash exiting";
			exit;
		}
		if($_REQUEST['security_hash'] != $security_hash){
			echo "Security hash is incorrect. you passed:" . $_REQUEST['security_hash'] . ' vs ' . $security_hash;
			exit;
		}
		if($_REQUEST['cmd'] == 'handle_create'){
			handleCreateAjax();
		}
		else if($_REQUEST['cmd'] == 'handle_login'){
			handleLoginAjax();
		}
	}

	function handleCreateAjax(){
		//Validate fields
		if(empty($_REQUEST['user_name']) || empty($_REQUEST['user_password']) || 
		   empty($_REQUEST['f_name'])    || empty($_REQUEST['l_name']) ){
			 echo '{"status":"failed", "fail_reason":"missing_params"}';
			 exit;
		}

		$rowOrFalse = getRowForUserName($_REQUEST['user_name']);
		if($rowOrFalse){
			$title = "Email Address Already in Use";
			$message = "Please try another email or tap 'LOG IN' to access the existing account.";
			$suggest_username = "";
			if (strpos($_REQUEST['user_name'], "@") === false) {
				$title = "Username Already in Use";
				$message = "Please try again with a different username.";
				$suggest_username = getNextAvailableUserNameFromRoot($_REQUEST['user_name']);
			}
			echo '{"status":"failed", "fail_reason":"username_already_exists", "title":"'.$title.'", "message":"'.$message.'", "suggest_username":"'.$suggest_username.'"}';
			exit;
		}
		
		$usrnm = $_REQUEST['user_name'];

		$insertArr = array('user_name' => $usrnm,
						   'user_password_hash' => sha1($_REQUEST['user_password'].'garbonzo'),
						   'f_name' => $_REQUEST['f_name'],
						   'l_name' => $_REQUEST['l_name'],
							 'locationid' => persistantReqVar("loc_id"),
							 'date_created' => $_REQUEST['device_time'],
							 'last_activity' => $_REQUEST['device_time'],
							 'email' => (strpos($usrnm, "@"))?$usrnm:"");
		$newUsersID = insertNewUser($insertArr);
		if(!empty($newUsersID)){
			logIt("Account Created", $newUsersID);
			echo '{"status":"success", "new_user_id":"'.$newUsersID.'"}';
			exit;
		}
		else{
			echo '{"status":"failed", "fail_reason":"unknown_problem_inserting_user", "message":"Error adding user"}';
		}
	}

	function handleLoginAjax(){
		$rowOrFalse = getRowForUserName($_REQUEST['user_name']);
		if(empty($rowOrFalse)){
			echo '{"status":"failed", "fail_reason":"user_name_does_not_exist", "message":"Username does not exist."}';
			exit;
		}
		else if(!empty($rowOrFalse['id'])){
			$usersID = $rowOrFalse['id'];
			$passwordHash = $rowOrFalse['user_password_hash'];
			if($passwordHash == sha1($_REQUEST['user_password'].'garbonzo')){
				logIt("Logged In at Kiosk", $usersID);
				echo '{"status":"success","user_id":"'.$usersID.'"}';
			}
			else{
				echo '{"status":"failed","fail_reason":"password_hash_doesnt_match","message":"Incorrect Password"}';
			}
			
			exit;
		}
		else{
			echo '{"status":"failed","fail_reason":"unknown","message":"There was an unknown error while logging in user"}';
			exit;
		}
	}

	function getRowForUserName($username){
		global $dataname;
		$queryResult = lavu_query("SELECT * FROM `med_customers` WHERE `user_name`='[1]'", $username);
		if(!$queryResult){ error_log("MYSQL ERROR IN tithe_login.php dn:$dataname error: " . lavu_dberror()); }
		if(mysqli_num_rows($queryResult)){
			return mysqli_fetch_assoc($queryResult);
		}else{
			return false;
		}
	}
	
	function getNextAvailableUserNameFromRoot($username) {
		global $dataname;
		$suggest_username = "";
		$queryResult = lavu_query("SELECT `user_name` FROM `med_customers` WHERE `user_name` LIKE '[1]%'", $username);
		if (mysqli_num_rows($queryResult)) {
			$un_list = array();
			while ($info = mysqli_fetch_assoc($queryResult)) {
				$un_list[] = $info['user_name'];
			}
			error_log("un_list: ".print_r($un_list, true));
			$append = 1;
			while (in_array($username.$append, $un_list)) {
				$append++;
			}
			$suggest_username = $username.$append;
		}
	
		return $suggest_username;	
	}
	
	function insertNewUser($insertArrayKeysMatchDB){
		global $dataname;
		$insertArr = $insertArrayKeysMatchDB;
		$queryStr  = "INSERT INTO `med_customers` (";
		foreach($insertArrayKeysMatchDB as $key => $val){
			$queryStr .= '`'.$key.'`,';
		}
		$queryStr  = substr($queryStr, 0, -1);//Remove last comma.
		$queryStr .= ") VALUES (";
		foreach($insertArrayKeysMatchDB as $key => $val){
			$queryStr .= "'[$key]',";
		}
		$queryStr  = substr($queryStr, 0, -1);//Remove last comma.
		$queryStr .= ')';
		$queryResult = lavu_query($queryStr, $insertArrayKeysMatchDB);
		if(!$queryResult){ 
			error_log("MYSQL error in tith_login for dn:$dataname mysql error:".lavu_dberror());
			echo lavu_dberror();
		}

		$id = ConnectionHub::getConn('rest')->insertID();

		return $id;
	}

	function logIt($action, $customer_id) {
	
		$device_time = $_REQUEST['device_time'];
	
		$q_fields = "";
		$q_values = "";
		$vars = array();
		$vars['action'] = $action;
		$vars['loc_id'] = persistantReqVar("loc_id");
		$vars['order_id'] = "";
		$vars['customer_id'] = $customer_id;
		$vars['restaurant_time'] = $device_time;
		$vars['device_time'] = $device_time;
		$keys = array_keys($vars);
		foreach ($keys as $key) {
			if ($q_fields != "") { $q_fields .= ", "; $q_values .= ", "; }
			$q_fields .= "`$key`";
			$q_values .= "'[$key]'";
		}
		$log_this = lavu_query("INSERT INTO `med_action_log` ($q_fields) VALUES ($q_values)", $vars);
		
		$update_customer = lavu_query("UPDATE `med_customers` SET `last_activity` = '[1]' WHERE `id` = '[2]'", $device_time, $customer_id);
	}

?>
