<?php

	function persistantReqVar($key) {
	
		$val = "";
		if (isset($_REQUEST[$key])) $val = $_REQUEST[$key];
		else if (isset($_SESSION["ltvar_".$key])) $val = $_SESSION["ltvar_".$key];
		if ($val != "") $_SESSION["ltvar_".$key] = $val;
	
		return $val;
	}
	
	$appData = "";
	$persistantKeys = array("UUID", "MAC", "app_name", "version", "build", "cc", "dn", "loc_id", "server_id", "server_name", "register", "register_name");
	foreach ($persistantKeys as $key) {
		if ($appData != "") $appData .= ", ";
		$appData .= $key.": '".persistantReqVar($key)."'";
	}

?>