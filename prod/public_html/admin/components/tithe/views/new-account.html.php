
	<script language='javascript'>
	
		function prepareTitheCreateAccount() {
		
			document.getElementById("f_name").value = "";
			document.getElementById("l_name").value = "";
			document.getElementById("user_name").value = "";
			document.getElementById("user_password").value = "";
			
			window.setTimeout(function(){document.getElementById("f_name").focus()}, 550);
			
			startTimeoutTimer(300000);
		}

		function trySuggestedUsername(username) {
		
			document.getElementById("user_name").value = username;
			
			window.setTimeout(function(){submitNewAccountForm()}, 10);
			
			return 1;
		}
		
		function submitNewAccountForm() {
		
			submit_login_ajax(document.forms["new-account-form"]);
		}

	</script>
	
	<div style='position:absolute; left:212px; top:190px; width:600px; height:400px; background-image:url("images/bg_panel_600x400.png")'>
		<center>
			<form action='' method='POST'  accept-charset='utf-8' id='new-account-form'>
				<input type='hidden' name='cmd' value='handle_create' />
				<table width='600px' height='400px'>
					<tr>
						<td align='center' valign='middle'>
							<table>
								<tr>
									<td align='center'>
										<table cellspacing='6' cellpadding='0'>
											<tr><td align='center'><input class='input_long' type='text' autocapitalize='on' placeholder='FIRST NAME' name='f_name' value='' data-required id='f_name'/><span class='span_required_field'></span></td></tr>
											<tr><td align='center'><input class='input_long' type='text' autocapitalize='on' placeholder='LAST NAME' name='l_name' data-required value='' id='l_name'/><span class='span_required_field'></span></td></tr>
											<tr><td align='center'><input class='input_long' type='email' placeholder='EMAIL' name='user_name' data-required value='' id='user_name'/><span class='span_required_field'></span></td></tr>
											<tr><td align='center'><input class='input_long' type='password' placeholder='PASSWORD' name='user_password' data-required value='' id='user_password'/><span class='span_required_field'></span></td></tr>
										</table>
									</td>
								</tr>
								<tr><td align='center' style='padding-top:14px;'><input class='btn_submit_login' type='submit' value='Continue &rarr;' id='new-submit' /></td></tr>
							</table>
						</td>
					</tr>
				</table>
			</form>
		</center>
	</div>