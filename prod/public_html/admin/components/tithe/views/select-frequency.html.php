	
	<style type="text/css" media="screen">
	
		.btn_frequency {
			color:#FFFFFF;
			font-family:Arial, Helvetica, sans-serif;
			font-size:48px;
			text-shadow: 1px 1px 0px rgba(0, 0, 0, 0.5);
			width:500px;
			height:100px;
			background:url("images/btn_500x100.png");
			border:hidden;
		}
	
	</style>
	
	<table width='1024px' height='490px' cellspacing='0' cellpadding='0'>
		<tr>
			<td align='center' width='1024px' height='490px'>
				<table>
					<tr><td align='center' class='page_subtitle' style='padding:6px 10px 8px 10px;'>Please select gift frequency<td></tr>
					<tr>
						<td align='center'>
							<table cellspacing='16' cellpadding='0'>
								<tr>
									<td><button class='btn_ministry2' type='button' ontouchstart='frequencySelected("One Time");'>One Time</button></td>
									<td><button class='btn_ministry2' type='button' ontouchstart='frequencySelected("Monthly");'>Monthly</button></td>
								</tr>
								<tr>
									<td><button class='btn_ministry2' type='button' ontouchstart='frequencySelected("Bi-Weekly");'>Bi-Weekly</button></td>
									<td><button class='btn_ministry2' type='button' ontouchstart='frequencySelected("Weekly");'>Weekly</button></td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
	  
	<script language='javascript'>
	
		function prepareTitheFrequency() {
		
			startTimeoutTimer(60000);
		}
	
		function frequencySelected(frequency) {
			
			User_Dynamic_Data.tithe_frequency = frequency;
			showPage('tithe_swipe');
		}
	
	</script>
		
<?php

	/*<div style='position:absolute; left:20px; top:100px; width:100px; height:20px;'>
		<button type='button' ontouchstart='frequencySelected("Daily");'>Daily</button>
	</div>
	
	<tr><td><button class='btn_frequency' type='button' ontouchstart='frequencySelected("One Time");'>One Time</button></td></tr>
	<tr><td><button class='btn_frequency' type='button' ontouchstart='frequencySelected("Monthly");'>Monthly</button></td></tr>
	<tr><td><button class='btn_frequency' type='button' ontouchstart='frequencySelected("Weekly");'>Weekly</button></td></tr>*/
	
?>