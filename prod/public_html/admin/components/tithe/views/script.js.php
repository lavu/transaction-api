(function(){
	function AJAXRequest( url, requestType, params, async ){
		if( arguments.length === 0 ){
			return;
		}
		if( !requestType ){
			requestType = '';
		}

		if( !async ){
			async = false;
		} else {
			async = true;
		}

		switch( requestType.toUpperCase().trim() ){
			case 'POST' :
			case 'PATCH' :
			case 'PUT' :
				break;
			case 'GET' :
			default :
				requestType = 'GET';
				break;
		}

		if( ! params ){
			params = '';
		} else if( params instanceof Object ){
			obj = params;
			arr = [];
			for( var k in obj ){
				arr.push( encodeURIComponent( k ) + '=' + encodeURIComponent( obj[k] ) );
			}

			params = arr.join('&');
		} else if('string' != typeof params ){
			params = '';
		}

		this._request = new XMLHttpRequest();
		this._request.open( requestType, url, async );
		this._request.addEventListener('readystatechange', this, false );
		if( requestType == 'POST' ){
			this._request.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
		}

		this._request.send( params );
	}

	function handleEvent( event ){
		if( event.type != 'readystatechange' ){
			return;
		}

		if( this._request.readyState === 4  ){
			this._request.removeEventListener( 'readystatechange', this, false );
			if( this._request.status === 200 ){
				this.success( this._request.responseText );
			} else {
				this.failure( this._request.status, this._request.responseText );
			}
		}
	}

	function success( responseText ){
		alert( responseText );
	}

	function failure( status, responseText ){
		alert( status );
		alert( responseText );
	}

	window.AJAXRequest = AJAXRequest;
	AJAXRequest.prototype = {};
	AJAXRequest.prototype.constructor = AJAXRequest;
	AJAXRequest.prototype.handleEvent = handleEvent;
	AJAXRequest.prototype.success = success;
	AJAXRequest.prototype.failure = failure;
})();

function simpleEmailValidation(email) {
	if (email == "") return false;
	var atpos=email.indexOf("@");
	var dotpos=email.lastIndexOf(".");
	if (atpos<1 || dotpos<atpos+2 || dotpos+2>=email.length) return false;
	return true;
}

function check_values( formname, submit_handler ) {
	return function ( e ) {
		e.preventDefault();
		var form = document.forms[formname];
		var good_to_go = true;
		var inputs = form.querySelectorAll( "input[data-required]" );
		var errors = form.querySelectorAll( "input[data-required] + span" );
		for ( var i = 0; i < inputs.length; i++ ) {
			var empty = inputs[i].value === "";
			good_to_go = good_to_go && !empty;
			errors[i].innerHTML = empty ? "This field is required" :  "";
			if (formname=="new-account-form" && inputs[i].name=="user_name" && inputs[i].value.indexOf("@")>=0) {
				good_to_go = simpleEmailValidation(inputs[i].value);
				errors[i].innerHTML = simpleEmailValidation(inputs[i].value) ? "" :  "Please use valid email";
			}
		}
		if ( good_to_go ) {
			highlightLoginSubmit(formname);
			window.setTimeout(function(){submit_handler( form )}, 20);
		}
		// make sure the form doesn't get commited
		return false;
	};
}

function ObjectLength( object ) {
	var length = 0;
	for( var key in object ) {
		if( object.hasOwnProperty(key) ) {
			++length;
		}
	}
	return length;
};

(function(){
	function LoginAJAXRequest( params ){
		
		this._params = params;

		var getParams = [];
		for( var k in params ){
			getParams.push( encodeURIComponent( k ) + '=' + encodeURIComponent( params[k] ) );
		}

		var getStr = getParams.join('&');

		AJAXRequest.call( this, '<?php echo $loginAjaxLocation; ?>?', 'POST', params, true );
	}

	function success( response ){
	
		var obj = JSON.parse( response );
		if( obj.status == 'success' ){
			go_to_from_amounts = "tithe_frequency";
			if( this._params.cmd == 'handle_create' ){
				fillDataArraysWithCallback(loadDataIntoObjects,this._params.cc,this._params.cc,obj.new_user_id,this._params.security_hash);
				showPage("tithe_ministries");
			} else {
				fillDataArraysWithCallback(loadDataIntoObjects,this._params.cc,this._params.cc,obj.user_id,this._params.security_hash);
				showPage("tithe_manage_account");
			}
		} else {
			var alert_or_confirm = "alert";
			var show_title = "Login Failed";
			var show_message = "Please try again.";
			
			if( this._params.cmd == 'handle_create' ){
				show_title = "Account Creation Failed";
				if (obj.status == 'failed') {
					if (obj.fail_reason == 'unknown_problem_inserting_user') {
						show_message = "A database error seems to have occurred.";
					} else if (obj.fail_reason == 'username_already_exists') {
						show_title = obj.title;
						show_message = obj.message;
						if (obj.suggest_username != '') {
							alert_or_confirm = "confirm";
							show_message = "However, " + obj.suggest_username + " is available. Would you like to use " + obj.suggest_username + "?";
						}
					}
				}
			} else {
				if (obj.status == 'failed') {
					if (obj.fail_reason == 'user_name_does_not_exist') {
						show_title = "Unknown Account";
						show_message = "Please try again or tap 'CREATE ACCOUNT' to start a new account.";
					} else if (obj.fail_reason == 'password_hash_doesnt_match') {
						show_title = "Incorrect Password";
					}
				}
			}
			unhighlightLoginSubmit();
			if (alert_or_confirm == "alert") componentAlert(show_title, show_message);
			else if (alert_or_confirm == "confirm") componentConfirm(show_title, show_message, 'trySuggestedUsername("' + obj.suggest_username +'");', '');
		}
	}

	function failure( status, response ){
		
		componentAlert("Connection Failed", "Please try again.");
	}

	window.LoginAJAXRequest = LoginAJAXRequest;
	LoginAJAXRequest.prototype.constructor = LoginAJAXRequest;
	LoginAJAXRequest.prototype = Object.create( AJAXRequest.prototype );
	LoginAJAXRequest.prototype.success = success;
	LoginAJAXRequest.prototype.failure = failure;
})();

function submit_login_ajax( form ) {

	var data = {};
	var inputs = form.querySelectorAll( "input" );
	for ( var i = 0; i < inputs.length; i++ ){
		if ( inputs[i].type != "submit" ) { // ignore the submit button
			// data.push(inputs[i].name + "=" + encodeURIComponent( inputs[i].value ) );
			data[inputs[i].name] = inputs[i].value;
		}
	}
	data.security_hash = security_hash;
	data.is_ajax = 1;
	data.cc = "<?php echo $_REQUEST['cc']?>";
	data.device_time = getCurrentDateTime();
	
	// data.push( 'security_hash=' + encodeURIComponent( security_hash ) );
	// data.push( 'is_ajax=1' );
	// data.push( "cc=<?php echo $_REQUEST['cc']?>" );

	new LoginAJAXRequest( data );
	// data.push( 'is_ajax=' + encodeURIComponent( security_hash ) );
	// data.push( "cc=<?php echo $_REQUEST['cc']?>" );
	/*var cc = "<?php echo $_REQUEST['cc']?>";
	
	//data_string = data_string.join("&") + "&is_ajax=1&security_hash=" + security_hash + "&cc=<?php echo $_REQUEST['cc']?>";
	//var xmlhttp = new XMLHttpRequest();
	//xmlhttp.open( "GET", "<?php echo $loginAjaxLocation; ?>?" + data_string, true );
	//xmlhttp.onreadystatechange = function () {
	//	if ( xmlhttp.readyState == 4 && xmlhttp.status == 200 ) {
	//		var response = JSON.parse( xmlhttp.responseText );
	//		if ( response.status === "success" ) {
	//			//fillDataArraysWithCallback(loadDataIntoObjects,$cc,$data_name,$member_id,$security_hash);
	//			fillDataArraysWithCallback(loadDataIntoObjects,cc,cc,response.user_id,security_hash);
	//			showPage("tithe_ministries");
	//		}
	//	}
	//};
	//xmlhttp.send();*/
}

function submit_new_account_ajax ( form ){
	var data = {};
	// var data_string = [];
	var inputs = form.querySelectorAll( "input" );
	for ( var i = 0; i < inputs.length; i++ ){
		if ( inputs[i].type != "submit" ) { // ignore the submit button
			// data_string.push(inputs[i].name + "=" + encodeURIComponent( inputs[i].value ) );
			data[ inputs[i].name ] = inputs[i].value;
		}
	}
	// var cc = "<?php echo $_REQUEST['cc']?>";
	// data_string = data_string.join("&") + "&is_ajax=1&security_hash=" + security_hash + "&cc=<?php echo $_REQUEST['cc']?>";
	data.is_ajax = 1;
	data.security_hash = security_hash;
	data.cc = "<?php echo $_REQUEST['cc'];?>";
	data.device_time = getCurrentDateTime();
	// var xmlhttp = new XMLHttpRequest();
	// xmlhttp.open( "GET", "<?php echo $loginAjaxLocation; ?>?" + data_string, true );
	// xmlhttp.onreadystatechange = function () {
	//	if ( xmlhttp.readyState == 4 && xmlhttp.status == 200 ) {
	//		var response = JSON.parse( xmlhttp.responseText );
	//		if ( response.status === "success" ) {
	//			//fillDataArraysWithCallback(loadDataIntoObjects,$cc,$data_name,$member_id,$security_hash
	//			fillDataArraysWithCallback(loadDataIntoObjects,cc,cc,response.new_user_id,security_hash);
	//			showPage("tithe_ministries");
	//		}
	//	}
	//};
	// xmlhttp.send();
	new LoginAJAXRequest( data );
}

function build_ministry_list() {
	//hide the other forms
	document.getElementById("login-form").style.display ="none";
	document.getElementById("new-account-form").style.display ="none";
	var ministries = document.createElement("div");
	ministries.id = "ministries";
}

function show_new_account_form ( e ){
	document.getElementById("login-form").style.display ="none";
	document.getElementById("new-account-form").style.display ="table";
}

function show_login_form ( e ){
	document.getElementById("login-form").style.display ="table";
	document.getElementById("new-account-form").style.display ="none";
}

window.addEventListener( "load" , function () {
	document.forms["login-form"].addEventListener( "submit", check_values( "login-form", submit_login_ajax ), false );
	document.forms["new-account-form"].addEventListener( "submit", check_values( "new-account-form", submit_login_ajax ), false );
	// document.getElementById("new-account").addEventListener( "click", show_new_account_form ,false );
	// document.getElementById("go-back").addEventListener( "click", show_login_form ,false );
}, false );