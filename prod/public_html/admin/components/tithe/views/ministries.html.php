
	<style type='text/css'>
	
		.btn_ministry1 {
				color:#FFFFFF;
				font-family:Arial, Helvetica, sans-serif;
				font-size:48px;
				text-shadow: 1px 1px 0px rgba(0, 0, 0, 0.5);
				width:656px;
				height:130px;
				background:url("images/btn_656x130.png");
				border:hidden;
		}

		.btn_ministry2 {
				color:#FFFFFF;
				font-family:Arial, Helvetica, sans-serif;
				font-size:48px;
				text-shadow: 1px 1px 0px rgba(0, 0, 0, 0.5);
				width:451px;
				height:200px;
				background:url("images/btn_451x200.png");
				border:hidden;
		}

		.btn_ministry3 {
				color:#FFFFFF;
				font-family:Arial, Helvetica, sans-serif;
				font-size:46px;
				text-shadow: 1px 1px 0px rgba(0, 0, 0, 0.5);
				width:451px;
				height:135px;
				background:url("images/btn_451x135.png");
				border:hidden;
		}

		.btn_ministry4 {
				color:#FFFFFF;
				font-family:Arial, Helvetica, sans-serif;
				font-size:38px;
				text-shadow: 1px 1px 0px rgba(0, 0, 0, 0.5);
				width:451px;
				height:98px;
				background:url("images/btn_451x98.png");
				border:hidden;
		}

		.btn_ministry5 {
				color:#FFFFFF;
				font-family:Arial, Helvetica, sans-serif;
				font-size:32px;
				text-shadow: 1px 1px 0px rgba(0, 0, 0, 0.5);
				width:451px;
				height:77px;
				background:url("images/btn_451x77.png");
				border:hidden;
		}
	
	</style>

	<table width='1024px' height='556px' cellspacing='0' cellpadding='0'>
		<tr>
			<td align='center' width='1024px' height='556px'>
				<table>
					<tr><td align='center' class='page_subtitle' style='padding:6px 10px 8px 10px;'>Please select a ministry<td></tr>
					<tr><td id='ministry_button_container' align='center'></td></tr>
				</table>
			</td>
		</tr>
	</table>

<script type="text/javascript">

	function prepareTitheMinistries() {
	
		var str = "<table cellspacing='16' cellpadding='0'><tbody>";
		var button_class = "btn_ministry1";
		var cols = 1;
		
		var ministries = Ministry.ministry_list;
		var active_ministries = [];
		for (m = 0; m < ministries.length; m++) {
			var ministry = ministries[m];
			if (ministry._deleted==0 && ministry.active==1) active_ministries.push(ministry);
		}
		
		var mCnt = active_ministries.length;
		if (mCnt > 3) {
			cols = 2;
			if (mCnt == 4) button_class = "btn_ministry2";
			else if (mCnt <= 6) button_class = "btn_ministry3";
			else if (mCnt <= 8) button_class = "btn_ministry4";
			else button_class = "btn_ministry5";
		}
		
		for (m = 0; m < Math.min(10, mCnt); m++) {
			var ministry = active_ministries[m];
			if (!(m % cols)) {
				if (m) str += "</tr>";
				str += "<tr>";
			}
			colspan = ((m == (mCnt - 1)) && ((m % 2) == 0) && (mCnt > 3))?" colspan='2'":"";
			str += "<td" + colspan + " style='text-align: center;'> <button class='" + button_class + " MinistryButton' type='button' data-ministry-id='" + ministry.id + "' ontouchstart='selectMinistry(this);'>" + ministry.name + "</button></td>";
		}
		
		str += "</tr></tbody></table>";	
		
		document.getElementById("ministry_button_container").innerHTML = str;
		
		document.activeElement.blur();
		
		startTimeoutTimer(60000);
	}

	/*(function(){
		var select_ministry =  function ( e ) {
			var min_id = this.getAttribute( "data-ministry-id" );
			ministry_id = min_id;
			if ( !window.User_Dynamic_Data ) {
				window.User_Dynamic_Data = {};
				window.User_Dynamic_Data = window.window.User_Dynamic_Data;
			}
			window.User_Dynamic_Data.ministry_id = ministry_id;
			window.User_Dynamic_Data.ministry_name = e.currentTarget.innerHTML;
			showPage("tithe_amounts");
			e.preventDefault();
			return false;
		};
		var buttons = document.querySelectorAll( ".MinistryButton" );
		for ( var i = 0; i < buttons.length; i++ ){
			buttons[i].addEventListener( "touchstart", select_ministry ,false );
			buttons[i].addEventListener( "click", select_ministry ,false );
		}
	}() )*/
	
	function selectMinistry(btn) {
	
		var min_id = btn.getAttribute("data-ministry-id");
		ministry_id = min_id;
		if ( !window.User_Dynamic_Data ) {
			window.User_Dynamic_Data = {};
			window.User_Dynamic_Data = window.window.User_Dynamic_Data;
		}
		window.User_Dynamic_Data.ministry_id = ministry_id;
		window.User_Dynamic_Data.ministry_name = btn.innerHTML;
		showPage("tithe_amounts");
	}

</script>
