
	<style type="text/css" media="screen">
	
		.btn_amount {
			color:#FFFFFF;
			font-family:Arial, Helvetica, sans-serif;
			font-size:48px;
			text-shadow: 1px 1px 0px rgba(0, 0, 0, 0.5);
			width:200px;
			height:100px;
			background:url("images/btn_200x100.png");
			border:hidden;
		}
			
		.btn_custom_amount {
			color:#FFFFFF;
			font-family:Arial, Helvetica, sans-serif;
			font-size:48px;
			text-shadow: 1px 1px 0px rgba(0, 0, 0, 0.5);
			width:500px;
			height:100px;
			background:url("images/btn_500x100.png");
			border:hidden;
		}
		
		.span_fee_explain {
			color:#FFFFFF;
			font-family:Arial, Helvetica, sans-serif;
			font-size:20px;
			text-shadow: 1px 1px 0px rgba(0, 0, 0, 0.5);
		}
		
		.btn_include_fee {
			width:63px;
			height:57px;
			background:url("images/checkbox_off.png");
			border:hidden;
		}
		
	</style>
	
	<table width='1024px' height='520px' cellspacing='0' cellpadding='0'>
		<tr>
			<td align='center' width='1024px' height='520px'>
				<table>
					<tr><td align='center' class='page_subtitle' style='padding:0px 10px 12px 10px;'>Please select offering amount<td></tr>
					<tr>
						<td align='center'>
							<table cellspacing='25' cellpadding='0'>
								<tr id='tithe_amounts_row'></tr>
								<tr><td id='tithe_custom' colspan='3' align='center'></td></tr>
							</table>
						</td>
					</tr>
					<tr><td style='padding:4px 4px 4px 4px;'>&nbsp;</td></tr>
					<tr>
						<td align='center' valign='middle' style='width:851px; height:75px; background-image:url("images/bg_panel_851x75.png"); padding: 0px 0px 0px 0px;'>
							<table>
								<tr>
									<td valign='middle'><span class='span_fee_explain'>Please note that the church pays a 3.25% processing fee. Check this box<br>if you'd like to pay the processing fee, thereby adjusting your total offering.</span></td>
									<td valign='middle'> &nbsp;<button id='include_fee_toggle' class='btn_include_fee' ontouchstart='toggleIncludeProcessingFee();'></button></td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
	  
	<script language='javascript'>

		var auto_include_processing_fee = false;

		document.getElementById("tithe_custom").innerHTML = "<button class='btn_custom_amount' type='button' ontouchstart='openCustomAmountNumberPad();'>Custom Amount</button>";
	
		function tryParseJSON(jsonString) {
			try {
					var o = JSON.parse(jsonString);
					if (o && typeof o === "object" && o !== null) {
							return o;
					}
			}
			catch (e) { }
	
			return false;
		};

		function prepareTitheAmounts() {
	
			var titheAmountsObject = tryParseJSON(Ministry.tithe_amounts.value);
			if (!titheAmountsObject) titheAmountsObject = JSON.parse('[{"tithe_amt":"1","status":"1"},{"tithe_amt":"5","status":"1"},{"tithe_amt":"10","status":"1"}]');
		
			var str = "";			
			var btnCount = 0;
			for (t = 0; t < titheAmountsObject.length; t++) {
				if (titheAmountsObject[t].status == 1) {
					btnCount++;
					var amt = titheAmountsObject[t].tithe_amt;
					str += "<td><button class='btn_amount' type='button' ontouchstart='amountSelected(\"" + amt + "\");'>$" + amt + "</button></td>";
				}
			}
			
			document.getElementById("tithe_amounts_row").innerHTML = str;
			document.getElementById("tithe_custom").setAttribute("colspan", btnCount);
			
			updateInlcudeFeeToggleBackground();
			startTimeoutTimer(60000);
		}
	
		function amountSelected(amount) {
		
			var fee_rate = 0.0325;
			if (auto_include_processing_fee) amount = (amount / (1 - fee_rate));
		
			User_Dynamic_Data.tithe_amount = amount;
			showPage(go_to_from_amounts);
		}
		
		function openCustomAmountNumberPad() {
		
			clearTimeouts();
			window.location = "_DO:cmd=number_pad&type=money&max_digits=15&title=" + encodeURIComponent("Custom Amount") + "&c_name=tithe&send_cmd=amountSelected(%27ENTERED_NUMBER%27);";
		}
		
		function toggleIncludeProcessingFee() {
		
			clearTimeouts();
			startTimeoutTimer(60000);
		
			auto_include_processing_fee = !auto_include_processing_fee;
			updateInlcudeFeeToggleBackground();
		}
		
		function updateInlcudeFeeToggleBackground() {
		
			var toggle_img = auto_include_processing_fee?"checkbox_on.png":"checkbox_off.png";
			document.getElementById("include_fee_toggle").style.backgroundImage = "url('images/" + toggle_img + "')";
		}
	
	</script>