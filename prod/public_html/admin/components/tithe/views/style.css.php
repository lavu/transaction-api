body{
    font-family:sans-serif;
}
#login-form{
	display:table;
	margin:30px auto; 
	border:4px solid #dcdcdc;
	-webkit-border-radius:10px;
	-moz-border-radius:10px;
    border-radius:10px;
    padding:20px;
}

#login-form p {
	display:table-row;
	color:#2b2b2b;
}

#login-form a{
    text-decoration:none;
    color:#CCCCCC;
    display:table-cell;
}

#login-form input, #login-form label, #login-form span{
	display:table-cell;
	font-family:0.9em;
}

#new-account-form{
	display:table;
	margin:30px auto; 
	border:4px solid #dcdcdc;
	-webkit-border-radius:10px;
	-moz-border-radius:10px;
    border-radius:10px;
    padding:20px;
}

#new-account-form p {
	display:table-row;
	color:#2b2b2b;
}

#new-account-form a{
    text-decoration:none;
    color:#CCCCCC;
    display:table-cell;
}

#new-account-form input, #new-account-form label{
	display:table-cell;
	font-family:0.9em;
}

#new-account-form p input[data-required] + span{
	display:inline-block;
	min-width:100px;
	color:red;
	font-size:0.75em;
	text-transform:uppercase;
	padding-left:20px;
}

#go-back, #new-account{
	cursor:pointer;
}

.wrapper{
	max-width:768px;
	margin:20px auto; 
}