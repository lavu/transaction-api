	
	<style type='text/css'>

		.btn_submit_login {
				color:#FFFFFF;
				font-family:Arial, Helvetica, sans-serif;
				font-size:20px;
				text-shadow: 1px 1px 0px rgba(0, 0, 0, 0.5);
				width:200px;
				height:40px;
				background:url("images/btn_200x40.png");
				border:hidden;
		}
		
		.span_required_field {
				color:#FFFFCC;
				font-family:Arial, Helvetica, sans-serif;
				font-size:19px;
				text-shadow: 1px 1px 0px rgba(0, 0, 0, 0.5);
		}
		
		.span_forgot_password {
				color:#FFFF99;
				font-family:Arial, Helvetica, sans-serif;
				font-size:18px;
				text-shadow: 1px 1px 0px rgba(0, 0, 0, 0.5);
		}
		
	</style>

	<script language='javascript'>
	
		function prepareTitheLogin() {
		
			document.getElementById("uname").value = "";
			document.getElementById("pwd").value = "";
			unhighlightLoginSubmit();
		
			window.setTimeout(function(){document.getElementById("uname").focus()}, 550);
		}
		
		function highlightLoginSubmit(formname) {
		
			var btnId = (formname == "login-form")?"login-submit":"new-submit";
			document.getElementById(btnId).style.backgroundImage = "url('images/btn_200x40_hl.png')";		
		}
		
		function unhighlightLoginSubmit() {

			document.getElementById("login-submit").style.backgroundImage = "url('images/btn_200x40.png')";		
			document.getElementById("new-submit").style.backgroundImage = "url('images/btn_200x40.png')";		
		}
		
		function askToResetPassword() {
		
			var email = document.getElementById("uname").value;
			if (simpleEmailValidation(email)) componentConfirm("Forgotten Password", "Would you like a password reset email sent to you at " + email + "?", "sendPasswordResetEmail();", "");
			else document.getElementById("uname_span").innerHTML = (email == "")?"Please enter your email":"Please enter valid email";
		}
		
		(function(){
			function PasswordResetEmailAJAXRequest( params ){
				AJAXRequest.call( this, 'lib/password_reset_email.php', 'POST', params, true );
			}

			function success( response ){

				var title = "Unknown Response";
				var message = "An error occurred while trying to contact server. Please try again.";
				var respJSON = tryParseJSON(response);
				if (respJSON) {	
					if (typeof respJSON.title != 'undefined') title = respJSON.title;
					if (typeof respJSON.message != 'undefined') message = respJSON.message;
				}
				
				componentAlert(title, message);
				window.setTimeout(function(){hideAppActivityShade()}, 200);
			}

			function failure( status, response ){

				componentAlert("Connection Failed", "An error occurred while trying to contact server. Please try again.");
				window.setTimeout(function(){hideAppActivityShade()}, 200);
			}

			window.PasswordResetEmailAJAXRequest = PasswordResetEmailAJAXRequest;
			PasswordResetEmailAJAXRequest.prototype.constructor = PasswordResetEmailAJAXRequest;
			PasswordResetEmailAJAXRequest.prototype = Object.create(AJAXRequest.prototype);
			PasswordResetEmailAJAXRequest.prototype.success = success;
			PasswordResetEmailAJAXRequest.prototype.failure = failure;
		})();
		
		function sendPasswordResetEmail() {
		
			showAppActivityShade();
			document.activeElement.blur();

			window.setTimeout(function(){
				
				var postVars = {};
				postVars.UUID = window.AppData.UUID;
				postVars.MAC = window.AppData.MAC;
				postVars.app = window.AppData.app_name;
				postVars.version = window.AppData.version;
				postVars.build = window.AppData.build;
				postVars.server_id = window.AppData.server_id;
				postVars.server_name = window.AppData.server_name;
				postVars.register = window.AppData.register;
				postVars.register_name = window.AppData.register_name;
				postVars.cc = window.AppData.cc;
				postVars.data_name = window.AppData.dn;
				postVars.loc_id = window.AppData.loc_id;
				postVars.email = document.getElementById("uname").value;
				
				new PasswordResetEmailAJAXRequest( postVars );	
			
			}, 50);
			
			return 1;		
		}
			
	</script>
	
	<div style='position:absolute; left:212px; top:260px; width:600px; height:240px; background-image:url("images/bg_panel_600x240.png")'>
		<center>
			<form action='login_submit' method='GET'  accept-charset='utf-8' id='login-form'>
				<input type='hidden' name='cmd' value='handle_login' />
				<table width='600px' height='240px'>
					<tr>
						<td align='center' valign='middle'>
							<table>
								<tr>
									<td align='center'>
										<table cellspacing='6' cellpadding='0'>
											<tr><td align='center'><input class='input_long' type='email' placeholder='EMAIL' name='user_name' value='' data-required id='uname'/><span class='span_required_field' id='uname_span'></span></td></tr>
											<tr><td align='center'><input class='input_long' type='password' placeholder='PASSWORD' name='user_password' data-required value='' id='pwd'/><span class='span_required_field'></span></td></tr>
										</table>
									</td>
								</tr>
								<tr>
									<td align='center' style='padding-top:14px;'>
										<span class='span_forgot_password' ontouchstart='askToResetPassword();'>Forgot Password?</span> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
										<input class='btn_submit_login' type='submit' value='Continue &rarr;' id='login-submit' />
									</td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
			</form>
		</center>
	</div>