
	<style type='text/css'>
	
		.btn_new_gift {
				color:#FFFFFF;
				font-family:Arial, Helvetica, sans-serif;
				font-size:28px;
				text-shadow: 1px 1px 0px rgba(0, 0, 0, 0.5);
				width:320px;
				height:49px;
				background:url("images/btn_320x49.png");
				border:hidden;
		}
	
		.btn_manage_tab {
				color:#DDDDDD;
				font-family:Arial, Helvetica, sans-serif;
				font-size:18px;
				text-shadow: 1px 1px 0px rgba(0, 0, 0, 0.5);
				width:210px;
				height:40px;
				background:url("images/btn_manage_tab.png");
				border:hidden;
		}
		
		.manage_message {
				color:#FFFFFF;
				font-family:Arial, Helvetica, sans-serif;
				font-size:32px;
				text-shadow: 1px 1px 0px rgba(0, 0, 0, 0.7);
		}
		
		.mng_tbl_header {
				color:#EEEEEE;
				font-family:Arial, Helvetica, sans-serif;
				font-size:17px;
				text-shadow: 1px 1px 0px rgba(0, 0, 0, 0.7);
				padding: 0px 3px 3px 3px;
				border-bottom:2px solid #999999;
		}
		
		.mng_tbl_cell {
				color:#FFFFFF;
				font-family:Arial, Helvetica, sans-serif;
				font-size:18px;
				text-shadow: 1px 1px 0px rgba(0, 0, 0, 0.4);
				padding: 4px 2px 2px 2px;
				border-top:1px solid #888888;		
		}
		
		.btn_mng_prev_next {
				color:#FFFFFF;
				font-family:Arial, Helvetica, sans-serif;
				font-size:14px;
				text-shadow: 1px 1px 0px rgba(0, 0, 0, 0.4);
				width:150px;
				height:30px;
				background:url("images/btn_150x30.png");
				border:hidden;
		}
		
	</style>

	<script type="text/javascript">

		var action_log_page = 1;

		function prepareTitheManageAccount() {
		
			go_back_from_ministries = "tithe_manage_account";
		
			hideManageActivityShade();
			drawActiveRecurringContent();
			drawAccountHistoryContent();
		
			switchManageTab("active_recurring");
			
			startTimeoutTimer(180000);
		}
		
		function commonDate(datetime) {
		
			if (datetime=="0000-00-00 00:00:00" || datetime=="0000-00-00" || datetime=="") return "-";
			
			var dt = datetime.split(" ");
			var d = dt[0].split("-");
			
			return d[1].replace(/^0+/, '') + "/" + d[2].replace(/^0+/, '') + "/" + d[0];
		}

		function commonTime(datetime) {
		
			var dt = datetime.split(" ");
			var t = dt[1].split(":");
			
			var h = t[0].replace(/^0+/, '');
			var m = t[1];			
			var ampm = "am";
			if (h > 12) {
				h -= 12;
				ampm = "pm";
			} else if (h == 12) ampm = "pm";
			else if (h == 0) h = 12;
			
			return h + ":" + m + " " + ampm;
		}
		
		function displayMoney(amt) {
		
			return parseFloat(amt).toFixed(decimal_places);
		}
		
		function ministryNameFromId(m_id) {
			
			var m_name = m_id;			
			var ministries = Ministry.ministry_list;
			for (m = 0; m < ministries.length; m++) {
				var ministry = ministries[m];
				if (ministry.id == m_id) {
					m_name = ministry.name;
					break;
				}
			}
			
			return m_name;
		}
			
		function drawActiveRecurringContent() {
		
			var str = "<table width='912px' cellspacing='0' cellpadding='2'>";	
			if (ObjectLength(window.ARB) > 0) {
			
				str += "<tr>";
				str += "<td class='mng_tbl_header'>Started</td>";
				str += "<td class='mng_tbl_header'>Order ID</td>";
				str += "<td class='mng_tbl_header'>Ministry</td>";
				str += "<td class='mng_tbl_header'>Amount</td>";
				str += "<td class='mng_tbl_header'>Frequency</td>";
				str += "<td class='mng_tbl_header'>Card</td>";
				str += "<td class='mng_tbl_header'>Next Offering</td>";
				str += "<td class='mng_tbl_header'> &nbsp;&nbsp;&nbsp; </td>";
				str += "<td class='mng_tbl_header'></td>";
				str += "</tr>";
			
				var keys = [];
				for (var k in window.ARB) keys.push(k);
				
				for (i = 0; i < keys.length; i++) {
					var arb = window.ARB[keys[i]];
					
					var ministry_name = arb.name;					
					var name_parts = ministry_name.split(" ");
					if (name_parts.length == 2) {
						if (!isNaN(name_parts[0]) && !isNaN(name_parts[1])) {
							ministry_name = ministryNameFromId(name_parts[1]);
						}
					}
					
					var card_info = arb.cc_card_type;
					if (card_info != "") card_info += " ";
					card_info += arb.cc_num_last4;
					if (card_info != "") card_info += " ";
					card_info += arb.cc_exp_date;
					
					str += "<tr id='arb_row_" + arb.id +"'>";
					str += "<td class='mng_tbl_cell'>" + commonDate(arb.createtime) + "</td>";
					str += "<td class='mng_tbl_cell'>" + arb.order_id + "</td>";
					str += "<td class='mng_tbl_cell'>" + ministry_name + "</td>";
					str += "<td class='mng_tbl_cell' align='right'>$" + displayMoney(arb.recurring_amount) + "</td>";
					str += "<td class='mng_tbl_cell'>" + arb.bill_frequency + "</td>";
					str += "<td class='mng_tbl_cell'>" + card_info + "</td>";
					str += "<td class='mng_tbl_cell'>" + commonDate(arb.next_bill_date) + "</td>";
					str += "<td class='mng_tbl_cell'></td>";
					str += "<td class='mng_tbl_cell'><img src='images/icon_edit.png' width='30px' height='30px' ontouchstart='editARB(" + arb.id + ")'></td>";
					str += "</tr>";
				}
				
			} else str += "<tr><td height='350px' align='center' valign='middle' class='manage_message'>You currenly have no active recurring gifts</td></tr>";
			str += "</table>";
			
			document.getElementById("active_recurring_div").innerHTML = str;
		}
		
		function drawAccountHistoryContent() {
		
			var str = "<table width='912px' cellspacing='0' cellpadding='2'>";	
			if (ObjectLength(window.ActionLog) > 0) {

				var keys = [];
				for (var k in window.ActionLog) keys.push(k);
				
				str += "<tr>";
				str += "<td class='mng_tbl_header'>Action</td>";
				str += "<td class='mng_tbl_header'>Date</td>";
				str += "<td class='mng_tbl_header'>Time</td>";
				str += "<td class='mng_tbl_header'>Order ID</td>";
				str += "</tr>";
				
				for (i = (keys.length - 1); i >= Math.max(0, (keys.length - 10)); i--) {
					var log_line = window.ActionLog[keys[i]];
				
					str += "<tr>";
					str += "<td class='mng_tbl_cell' style='height:30px'>" + log_line.action + "</td>";
					str += "<td class='mng_tbl_cell' style='height:30px'>" + commonDate(log_line.device_time) + "</td>";
					str += "<td class='mng_tbl_cell' style='height:30px'>" + commonTime(log_line.device_time) + "</td>";
					str += "<td class='mng_tbl_cell' style='height:30px'>" + log_line.order_id + "</td>";
					str += "</tr>";
				}
				
				if (action_log_page>1 || keys.length>10) {
					str += "<tr><td colspan='4' style='padding:6px 2px 2px 2px;'>";
					str += "<table width='100%'><tr><td> &nbsp; </td><td align='left'>";
					if (action_log_page > 1) str += "<button class='btn_mng_prev_next' ontouchstart='loadAccountHistory(\"" + (action_log_page*1 - 1) + "\");'>&larr; PREVIOUS</button>";
					str += "</td><td align='right'>";
					if (keys.length > 10) str += "<button class='btn_mng_prev_next' ontouchstart='loadAccountHistory(\"" + (action_log_page*1 + 1) + "\");'>NEXT &rarr;</button>";
					str += "</td><td> &nbsp; </td></tr></table>";
					str += "</td></tr>";
				}
				
			} else 	str += "<tr><td height='350px' align='center' valign='middle' class='manage_message'>No log entries were found</td></tr>";
			str += "</table>";
			
			document.getElementById("account_history_div").innerHTML = str;
		}
		
		function switchManageTab(toTab) {
		
			var manage_tabs = ["active_recurring", "account_history"];

			var hiddenTab = "";
			var shownTab = "";
			
			for (var i = 0; i < manage_tabs.length; i++) {
				var btn_id = manage_tabs[i];
				var div_id = btn_id + "_div";
				var displayIs = document.getElementById(div_id).style.display;
				var displayTo = (toTab == btn_id)?"block":"none";
				if (displayIs != displayTo) {					
					document.getElementById(div_id).style.display = (toTab == btn_id)?"block":"none";
					if (displayTo == "block") {
						document.getElementById(btn_id).style.color = "#FFFFFF";
						document.getElementById(btn_id).style.backgroundImage = "url('images/btn_manage_tab_hl.png')";					
						shownPage = btn_id;
					} else if (displayTo == "none") {
						document.getElementById(btn_id).style.color = "#DDDDDD";
						document.getElementById(btn_id).style.backgroundImage = "url('images/btn_manage_tab.png')";
						hiddenPage = btn_id;
					}
				}
			}
			
			//if (hiddenTab != "") doOnHide(hiddenPage);
			//if (shownTab != "") doOnShow(shownPage);
			
		}
		
		function editARB(arb_id) {
		
			clearTimeouts();
			
			cancelARB(arb_id);
			
			//componentActionSheet("Edit Recurring Offering", "Change Credit Card|Cancel Recurring Offering", "editARBcallback(\"" + arb_id + "\", \"[CHOICE_INDEX]\", \"[CHOICE]\")");
		}
		
		function editARBcallback(arb_id, choice_index, choice) {
		
			var ci = (choice_index * 1);
		
			switch (choice_index * 1) {		
				case 0:
					componentAlert("Currently Unavailable", "");
					startTimeoutTimer(180000);
					break;
				case 1:
					cancelARB(arb_id);
					break;
				default:
					startTimeoutTimer(180000);
					break;
			}
			
			return 1;
		}
			
		function cancelARB(arb_id) {
		
			var arb = window.ARB[arb_id];
			
			var name_parts = arb.name.split(" ");
			var m_id = name_parts[1];
			
			componentConfirm("Cancel Recurring Offering", "Do you want to cancel your " + arb.bill_frequency + " offering of $" + displayMoney(arb.recurring_amount) + " to " + ministryNameFromId(m_id) + "?", "modifyARB(\"" + arb_id + "\", \"cancel\", \"\")", "startTimeoutTimer(180000)");
		}
		
		(function(){
			function ModifyARBAJAXRequest( params ){
				AJAXRequest.call( this, 'lib/modify_arb.php', 'POST', params, true );
			}

			function success( response ){
			
				var status = "";
				var title = "Unknown Response";
				var message = "An error occurred while trying to contact server. Please try again.";
				var respJSON = tryParseJSON(response);
				if (respJSON) {	
					if (typeof respJSON.status != 'undefined') {
						status = respJSON.status;
						if (status == "success") {
							refreshARBlist();
							loadAccountHistory(1);
						}
					}
					if (typeof respJSON.title != 'undefined') title = respJSON.title;
					if (typeof respJSON.message != 'undefined') message = respJSON.message;
				}
				
				componentAlert(title, message);
				if (status != "success") hideManageActivityShade();
				startTimeoutTimer(180000);
			}

			function failure( status, response ){

				componentAlert("Connection Failed", "An error occurred while trying to modify your recurring gift. Please try again.");
				hideManageActivityShade();
				startTimeoutTimer(180000);
			}

			window.ModifyARBAJAXRequest = ModifyARBAJAXRequest;
			ModifyARBAJAXRequest.prototype.constructor = ModifyARBAJAXRequest;
			ModifyARBAJAXRequest.prototype = Object.create(AJAXRequest.prototype);
			ModifyARBAJAXRequest.prototype.success = success;
			ModifyARBAJAXRequest.prototype.failure = failure;
		})();
		
		var modARBinfo = { id: 0, action: "", new_value: "" };
				
		function modifyARB(arb_id, action, new_value) {
		
			showManageActivityShade();
			
			modARBinfo = { id: arb_id, action: action, new_value: new_value };
		
			window.setTimeout(function(){
			
				var postVars = {};
				postVars.UUID = window.AppData.UUID;
				postVars.MAC = window.AppData.MAC;
				postVars.app = window.AppData.app_name;
				postVars.version = window.AppData.version;
				postVars.build = window.AppData.build;
				postVars.server_id = window.AppData.server_id;
				postVars.server_name = window.AppData.server_name;
				postVars.register = window.AppData.register;
				postVars.register_name = window.AppData.register_name;
				postVars.cc = window.AppData.cc;
				postVars.data_name = window.AppData.dn;
				postVars.loc_id = window.AppData.loc_id;
				postVars.member_id = Member.id;
				postVars.device_time = getCurrentDateTime();
				postVars.arb_id = arb_id;
				postVars.action = action;
				postVars.new_value = new_value;	
						
				new ModifyARBAJAXRequest( postVars );	
				
			}, 50);
			
			return 1;
		}
		
		function refreshARBlist() {
	
			if (modARBinfo.action == "cancel") {		
				var element = document.getElementById("arb_row_" + modARBinfo.id);
				element.parentNode.removeChild(element);
			}
		
			modARBinfo = { id: 0, action: "", new_value: "" };
		}
		
		(function(){
			function loadActionLogAJAXRequest( params ){
				AJAXRequest.call( this, 'lib/load_med_action_log.php', 'POST', params, true );
			}

			function success( response ){
			
				hideManageActivityShade();
				
				var status = "";
				var title = "Unknown Response";
				var message = "An error occurred while trying to contact server. Please try again.";
				var respJSON = tryParseJSON(response);
				if (respJSON) {	
					if (typeof respJSON.status != 'undefined') {
						status = respJSON.status;
						if (status == "success") {
							if (typeof respJSON.log_lines != 'undefined') {
								if (ObjectLength(respJSON.log_lines) > 0) {
									window.ActionLog = respJSON.log_lines;
									action_log_page = respJSON.page;
									drawAccountHistoryContent();							
								} else title = "Empty Response";
							} else title = "Empty Response";
						}
					}
					if (typeof respJSON.title != 'undefined') {
						if (respJSON.title != "") title = respJSON.title;
					}
					if (typeof respJSON.message != 'undefined') {
						if (respJSON.message != "") message = respJSON.message;
					}
				}
				
				if (status != "success") componentAlert(title, message);
				startTimeoutTimer(180000);
			}

			function failure( status, response ){

				hideManageActivityShade();
				componentAlert("Connection Failed", "An error occurred while trying to load account history. Please try again.");
				startTimeoutTimer(180000);
			}

			window.loadActionLogAJAXRequest = loadActionLogAJAXRequest;
			loadActionLogAJAXRequest.prototype.constructor = loadActionLogAJAXRequest;
			loadActionLogAJAXRequest.prototype = Object.create(AJAXRequest.prototype);
			loadActionLogAJAXRequest.prototype.success = success;
			loadActionLogAJAXRequest.prototype.failure = failure;
		})();
		
		function loadAccountHistory(page) {
		
			clearTimeouts();
			showManageActivityShade();
			
			window.setTimeout(function(){
			
				var postVars = {};
				postVars.UUID = window.AppData.UUID;
				postVars.MAC = window.AppData.MAC;
				postVars.app = window.AppData.app_name;
				postVars.version = window.AppData.version;
				postVars.build = window.AppData.build;
				postVars.server_id = window.AppData.server_id;
				postVars.server_name = window.AppData.server_name;
				postVars.register = window.AppData.register;
				postVars.register_name = window.AppData.register_name;
				postVars.cc = window.AppData.cc;
				postVars.data_name = window.AppData.dn;
				postVars.loc_id = window.AppData.loc_id;
				postVars.member_id = Member.id;
				postVars.page = page;
						
				new loadActionLogAJAXRequest( postVars );	
				
			}, 50);			
		}
		
		function showManageActivityShade() {
		
			document.getElementById("mng_panel_activity_shade").style.display = "block";
		}
		
		function hideManageActivityShade() {
		
			document.getElementById("mng_panel_activity_shade").style.display = "none";
		}
	
	</script>

	<div style='position:absolute; left:50px; top:25px; width:250px; height:25px; vertical-align:middle; display:block;' ontouchstart='showPage("tithe_welcome");'>
		<img src='images/arrow_back.png' width='12px' height='20px' /> &nbsp;
		<span class='back_button_label'>GO BACK</span>
	</div>

	<div style='position:relative; left:412px; top:25px; width:580px; height:25px; text-align:right; vertical-align:middle; display:block;' ontouchstart='showPage("tithe_welcome");'>
		<span class='back_button_label'>LOG OUT</span> &nbsp;
		<img src='images/icon_x.png' width='19px' height='19px'>
	</div>	
	
	<center>
		<span class='page_title' style='position:absolute; left:0px; top:70px; width:1024px; height:70px;'><?php echo $location_name; ?></span>
	</center>
	
	<button class='btn_new_gift' style='position:absolute; left:624px; top:162px;' ontouchstart='showPage("tithe_ministries");'>New Offering</button>
	
	<button class='btn_manage_tab' id='active_recurring' style='position:absolute; left:92px; top:184px;' ontouchstart='switchManageTab("active_recurring");'>Recurring Offerings</button>
	<button class='btn_manage_tab' id='account_history' style='position:absolute; left:302px; top:184px;' ontouchstart='switchManageTab("account_history");'>Account History</button>
	
	<div style='position:absolute; left:36px; top:220px; width:952px; height:473px; background-image:url("images/bg_panel_952x473.png");'>
		<div id='active_recurring_div' style='position:relative; left:20px; top:20px; width:912px; height:433px; display:none; overflow:scroll;'></div>		
		<div id='account_history_div' style='position:relative; left:20px; top:20px; width:912px; height:433px; display:none;'></div>		
	</div>
	
	<div id='mng_panel_activity_shade' style='position:absolute; left:36px; top:220px; width:952px; height:473px; background-image:url("images/bg_mng_panel_activity_shade.png"); display:none;'>
		<table width='952px' height='473px'>
			<tr>
				<td align='center' valign='middle'>
					<table width='50px' height='50px' cellspacing='0' cellpadding='0'>
						<tr><td align='center' valign='middle' style='background-image:url("images/bg_activity_indicator.png"); opacity:0.8'><img src='images/activity_indicator.gif' width='32px' height='32px'></td></tr>
					</table><br><br><br><br>
				</td>
			</tr>
		</table>
	</div>