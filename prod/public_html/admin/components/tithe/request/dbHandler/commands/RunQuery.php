<?php
require_once __MVC_ROOT__ . '/Command.php';
require_once dirname(dirname(__FILE__)) . '/models/DatabaseModel.php';

class QueryResultType {
	const ASSOCIATIVE_ARRAY = 0;
	const INDEXED_ARRAY = 1;
	const STDCLASS_OBJ = 2;
}

class RunQuery extends Command {
	private
	$query,
	$types,
	$parameters,
	$result,
	$mysqli_stmt,
	$targetDatabase;

	public function __construct( $query, $parameters ){
		parent::__construct();
		$this->query = $query;
		$this->types = '';
		if($parameters && is_array( $parameters ) ){
			foreach( $parameters as $param ){
				if( is_numeric( $param ) ){
					if( is_integer( $param*1 ) ){
						$this->types .= 'i';
					} else if ( is_double( $param*1 ) || is_float( $param*1 ) ){
						$this->types .= 'd';
					}
				} else {
					$this->types .= 's';
				}
			}
		}

		$this->parameters = $parameters;
		$this->result = null;
		$this->targetDatabase = null;
		$this->mysqli_stmt = null;
	}

	public function __destruct(){
		unset( $this->query );
		unset( $this->types );
		unset( $this->parameters );
		if( $this->result ){
			$this->result->free_result();
			unset( $this->result );
		}
		if( $this->mysqli_stmt ){
			$this->mysqli_stmt->close();
		}
	}

	public function setTargetDatabase( $database ){
		if(!$database){
			return;
		}

		if( ! is_string( $database ) ){
			return;
		}

		$this->targetDatabase = $database;
	}

	public function fetchRow( $type ){
		if( null === $this->mysqli_stmt ){

			if( null === $this->result ){
				return null;
			}

			switch( $type ){
				case QueryResultType::ASSOCIATIVE_ARRAY: {
					return $this->result->fetch_array( MYSQLI_ASSOC );
				} case QueryResultType::INDEXED_ARRAY: {
					return $this->result->fetch_array( MYSQLI_NUM  );
				} case QueryResultType::STDCLASS_OBJ: {
					return (object) $this->result->fetch_array( MYSQLI_ASSOC );
				} default : {
					return null;
				}
			}
		}

		$result_array = array_fill(0, count( $this->result->fetch_fields() ), '' );
		$refs = array();
		foreach( $result_array as $key => $value ){
			$refs[] = & $result_array[$key];
		}

		call_user_func_array( array( $this->mysqli_stmt, 'bind_result' ),  $refs ) ;
		if( !($fetch_result = $this->mysqli_stmt->fetch()) ){
			if($fetch_result === FALSE ){
				ErrorLogView::error( $this->mysqli_stmt->error );
			}
			//No more rows otherwise
			return null;
		}
		switch( $type ){
			case QueryResultType::INDEXED_ARRAY: {
				return $result_array;				
			} case QueryResultType::ASSOCIATIVE_ARRAY: {
				$fields = $this->result->fetch_fields();
				$result = array();
				foreach( $result_array as $key => $value ){
					$result[ $fields[$key]->name ] = $value;
				}
				return $result;
			} case QueryResultType::STDCLASS_OBJ: {
				$fields = $this->result->fetch_fields();
				$result = array();
				foreach( $result_array as $key => $value ){
					$result[ $fields[$key]->name ] = $value;
				}
				return (object)$result;
			} default : {
				return null;
			}
		}
	}

	public function filter( $model ){
		if( !($model instanceof Model) ){
			return true;
		}

		if( !($model instanceof DatabaseModel) ){
			return true;
		}

		if( !($this->targetDatabase) ){
			return false;
		}

		if( $this->targetDatabase == $model->database() ){
			return false;
		}

		return true;
	}

	private function toReferenceArray( &$arr ){
		$resultArr = array();
		foreach( $arr as $key => &$val ){
			$resultArr[] = &$val;
		}

		return $resultArr;

	}

	public function run( $model ){
		if( !$model ){
			return false;
		}
		if( !($model instanceof DatabaseModel) ){
			return false;
		}
		$mysqli_stmt = $model->mysqli()->prepare( $this->query );
		if( !$mysqli_stmt ){
			ErrorLogView::error( $model->mysqli()->error );
			return false;
		}
		if( $this->types != ''){
			$referenceArr = $this->toReferenceArray( $this->parameters );
			$arguments = array_merge( array( $this->types ), $referenceArr );

			if( !call_user_func_array( array( $mysqli_stmt, 'bind_param' ), $arguments ) ){
				ErrorLogView::error( $mysqli_stmt->error );
				unset( $mysqli_stmt );
				return false;
			}
		}
		if( !$mysqli_stmt->execute() ){
			ErrorLogView::error( $mysqli_stmt->error );
			$mysqli_stmt->close();
			return false;
		}

		if( method_exists( $mysqli_stmt, 'get_result' ) ){
			$mysqli_result = $mysqli_stmt->get_result();
			if( !$mysqli_result ){
				ErrorLogView::error( $mysqli_stmt->error );
				$mysqli_stmt->close();
				return false;
			}
			$this->result = $mysqli_result;
			// $mysqli_stmt->close();
			return true;
		}

		$mysqli_stmt->store_result();
		$this->mysqli_stmt = $mysqli_stmt;
		$mysqli_result = $mysqli_stmt->result_metadata();
		if( !$mysqli_result ){
			ErrorLogView::error( $mysqli_stmt->error );
			$mysqli_stmt->close();
			return false;
		}
		$this->result = $mysqli_result;
                //$this->result = $mysqli_result->fetch_All( MYSQLI_ASSOC );

                //$mysqli_stmt->free_result();
                //$mysqli_stmt->close();

                //RequestController::controller()->runCommand( new $this->commandClass( $this->result ) );
		return true;
	}
}