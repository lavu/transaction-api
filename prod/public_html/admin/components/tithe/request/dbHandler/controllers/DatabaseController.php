<?php
require_once __MVC_ROOT__ . '/Controller.php';

class DatabaseController extends Controller {
	public function __construct(){
		parent::__construct();
	}

	public function __destruct(){
		parent::__destruct();
	}

	public static function controller(){
		static $controller;
		if( !$controller ){
			$controller = new DatabaseController();
		}

		return $controller;
	}
}