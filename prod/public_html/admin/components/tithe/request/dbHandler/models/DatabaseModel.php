<?php
require_once __MVC_ROOT__ . '/Model.php';
require_once __MVC_ROOT__ . '/views/ErrorLogView.php';

class DatabaseModel extends Model {
	private $mysqli,
	$database,
	$host;

	public function __construct( $host, $username, $password, $database ){
		parent::__construct();
		$this->mysqli = new mysqli( $host, $username, $password, $database );
		$this->host = $host;
		$this->database = $database;
	}

	public function __destruct(){
		if( $this->mysqli ){
			$this->mysqli->close();
		}
		parent::__destruct();
	}

	public function mysqli() {
		return $this->mysqli;
	}

	public function host(){
		return $this->host;
	}

	public function database(){
		return $this->database;
	}
}