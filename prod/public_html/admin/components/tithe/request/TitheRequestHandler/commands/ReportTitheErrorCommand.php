<?php
require_once __MVC_ROOT__ . '/Command.php';
require_once dirname( dirname(__FILE__) ) . '/models/TitheErrorModel.php';

class ReportTitheErrorCommand extends Command {
private $error;
private $view;

/**
 * The constructor initializes and a TitheErrorModel and a view to report the Error to.
 * @param string $viewType The String Representation of the View that this error is being created with.
 * @param int $code     The integer based code representation of the error.
 * @param string $message  A string representation of the error to inform the user.
 */
public function __construct( $viewType, $code, $message ){
	$this->error = new TitheErrorModel( $code, $message );
	switch( $viewType ) {
		case 'json' :{
			require_once __MVC_ROOT__ . '/views/JSONView.php';
			$this->view = new JSONView();
			break;
		} case 'xml' : {
			require_once __MVC_ROOT__ . '/views/XMLView.php';
			$this->view = new XMLView();
			break;
		} case 'csv' : {
			require_once __MVC_ROOT__ . '/views/CSVView.php';
			$this->view = new CSVView();
			break;
		} case 'html' : {
			require_once __MVC_ROOT__ . '/views/HTMLView.php';
			$this->view = new HTMLView();
		} default : {
			break;
		}
	}
}

public function __destruct(){
	unset( $this->error );
	unset( $this->view );
}

public function filter( $obj ){
	return false;
}

public function run( $model ){
	if( !$model ){
		return false;
	}

	if( !($model instanceof Model) ){
		return false;
	}

	if( $this->view ){
		$this->error->addView( $this->view );
		$this->view->update();
	}
}
}