<?php
require_once __MVC_ROOT__ . '/Command.php';
require_once __TYRO_ROOT__ . '/models/TyroRequestModel.php';
require_once __TYRO_ROOT__ . '/controllers/TitheRequestController.php';
require_once __ENTRY_ROOT__ . '/dbHandler/models/DatabaseModel.php';
require_once __ENTRY_ROOT__ . '/dbHandler/controllers/DatabaseController.php';

require_once __MVC_ROOT__ . '/views/ErrorLogView.php';

define( '__SETTINGS_FILE__', __DOCUMENT_ROOT__ . '/local/settings.txt' );
define( 'CONNECT_TO_LOCALDATABASE_COMMAND_MAX_LINE_READ_LENGTH', 128 );

class ConnectToLocalDatabaseCommand extends Command {
	private
		$handle;
	public function __construct() {
		parent::__construct();
		$this->handle = @fopen( __SETTINGS_FILE__, 'r' );
	}

	public function __destruct() {
		parent::__destruct();
		fclose( $this->handle );
	}

	public function filter( $obj ){
		if( $obj instanceof TyroRequestModel ){
			return false;
		}

		if( $obj instanceof Controller ){
			return false;
		}

		return true;
	}

	public function run( Model $model ){
		if( !$this->handle ){
			return false;
		}

		$username = null;
		$password = null;
		$database = null;

		TitheRequestController::controller()->addController( DatabaseController::controller() );
		while( ($contents = fgets( $this->handle, CONNECT_TO_LOCALDATABASE_COMMAND_MAX_LINE_READ_LENGTH )) !== false ){
			$mapping = array_map( trim, explode( '=', $contents ));
			if( $mapping[0] == 'username' ){
				$username = $mapping[1];
			} else if ( $mapping[0] == 'password' ){
				$password = $mapping[1];
			} else if ( $mapping[0] == 'database' ){
				$database = $mapping[1];
			}
		}

		if( !$username || !$password || !$database ){
			return false;
		}

		if(!DatabaseController::controller()->addModel( new DatabaseModel( 'localhost', $username, $password, $database) )){
			return false;
		}
		return true;
	}
}