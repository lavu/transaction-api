<?php
require_once __MVC_ROOT__ . '/Model.php';
require_once __MVC_ROOT__ . '/views/JSONView.php';
require_once __MVC_ROOT__ . '/views/XMLView.php';
require_once __MVC_ROOT__ . '/views/CSVView.php';
require_once __MVC_ROOT__ . '/views/HTMLView.php';

define( 'TITHE_REQUEST_ERROR_COMMAND_NOT_SPECIFIED', 506 );
define( 'TITHE_REQUEST_ERROR_COMMAND_NOT_FOUND', 507 );
define( 'TITHE_REQUEST_ERROR_COMMAND_MISSING_ARGUMENTS', 508 );

define( 'TITHE_REQUEST_ERROR_VIEW_NOT_SPECIFIED', 516 );
define( 'TITHE_REQUEST_ERROR_VIEW_NOT_FOUND', 517 );

class TitheErrorModel extends Model implements IJSONable, IXMLable, ICSVable, IHTMLable {
	private
	$code,
	$message;
	public function __construct( $code, $message ){
		parent::__construct();
		$this->code = $code;
		$this->message = $message;
	}

	public function __destruct(){
		unset( $this->code );
		unset( $this->message );
		parent::__destruct();
	}

	public function code(){
		return $this->code;
	}

	public function message(){
		return $this->message;
	}

	/// IJSONable
	public function toJSON(){
		$error = new stdClass();
		$error->code = $this->code();
		$error->message = $this->message();

		if( function_exists('json_encode') ){
			return json_encode( $error );
		} else if ( file_exists( __DOCUMENT_ROOT__ . '/cp/resources/json.php' ) ){
			require_once __DOCUMENT_ROOT__ . '/cp/resources/json.php';
			return LavuJson::json_encode( $error );
		} else {
			return print_r( $error, true );
		}
	}

	/// IXMLable
	public function toXML(){
		$error = new stdClass();
		$error->code = $this->code();
		$error->message = $this->message();

		$obj = new stdClass();
		$obj->error = $error;

		return XMLView::toXML( $obj );
	}

	/// ICSVable
	public function toCSV(){
		$error = array();
		$error['code'] = $this->code();
		$error['message'] = $this->message();

		return CSVView::toCSV( array( $error ) );
	}

	public function toHTML( $html ){
		$result = $html;
		$result .= '<h3>' . $this->message . '</h3>';
		return $result;
	}
}