<?php
	define('__DOCUMENT_ROOT__', $_SERVER['DOCUMENT_ROOT'] );
	define('__TITHE_ROOT__', dirname(__FILE__) . '/TitheRequestHandler');
	define('__MVC_ROOT__', dirname(__FILE__) . '/mvc' );
	define('__ENTRY_ROOT__', dirname(__FILE__) );
	define('__VIEW__', (isset($_GET['view'])&&!(empty($_GET['view']))) ? $_GET['view'] : 'json' );
	ini_set('display_errors', '1');

	require_once __MVC_ROOT__ . '/views/ErrorLogView.php';
	require_once __TITHE_ROOT__ . '/controllers/TitheRequestController.php';
	require_once __TITHE_ROOT__ . '/models/TitheRequestModel.php';
	TitheRequestController::controller()->addModel( new TitheRequestModel() );

	if(!isset($_GET['command'] )){
		require_once __TITHE_ROOT__ . '/commands/ReportTitheErrorCommand.php';
		TitheRequestController::controller()->runCommand( new ReportTyroErrorCommand( __VIEW__, TITHE_REQUEST_ERROR_COMMAND_NOT_SPECIFIED, 'The Command was Not Specified' ) );
		return;
	}
	$command = str_ireplace('-', '_', $_GET['command']);
	define( '__COMMAND__', $command );
	unset( $_GET['command'] ); //Clean up the arguments a bit

	$command_path = dirname(__FILE__) . '/commands/' . __COMMAND__ . '.php';
	if(!file_exists( $command_path ) ){
		require_once __TITHE_ROOT__ . '/commands/ReportTitheErrorCommand.php';
		ErrorLogView::debug( $command_path );
		TitheRequestController::controller()->runCommand( new ReportTitheErrorCommand( __VIEW__, TITHE_REQUEST_ERROR_COMMAND_NOT_FOUND, 'The Specified Command was not Found' ) );
		return;
	}

	require_once __ENTRY_ROOT__ . '/dbHandler/commands/ConnectToDatabase.php';
	if(! TitheRequestController::controller()->runCommand( new ConnectToDatabase() ) ){
		require_once __TITHE_ROOT__ . '/commands/ReportTitheErrorCommand.php';
		ErrorLogView::debug( $command_path );
		TitheRequestController::controller()->runCommand( new ReportTitheErrorCommand( __VIEW__, 938, 'Unable To Connect To Local Database' ) );
		return;
	}

	require_once $command_path;
	$arguments = $_GET['arguments'];
	$arguments = explode('/', $arguments);
	$command = new ReflectionClass( __COMMAND__ );
	ob_start();
	TitheRequestController::controller()->runCommand( $command->newInstanceArgs( $arguments ) );
	$output = ob_get_contents();
	ob_end_clean();

	// header( 'x-Tithe-mac: '. strtoupper(hash_hmac('sha1', $output, 'passphrase' )));
	echo $output;
	unset( $command );