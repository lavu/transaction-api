<?php
require_once dirname(__FILE__) . '/Model.php';

class Command {
	public function __construct(){ }
	public function __destruct(){ }

	/**
	 * The filter method is designed to provide a Command a way to specify whether is should be run on a specified Model, or handed to any particular Controller.  It is a simple way to trickle the Command down to perform, or not perform, on multiple Controllers/Models.
	 * @param  stdClass $obj The Object that is to be considered when attempting to filter.
	 * @return BOOL      true if the Command should not consider this item, or false if it should.
	 */
	public function filter( $obj ){
		return false;
	}
	/**
	 * The run method will contain the implementation for the command itself.  Please note that the command is provided a Model for it to modify or reference as is needed.  Please ensure that this is the correct Model that you are expecting, otherwise unexpected behavior may ensue.
	 * @param  Model $model the Model that this Command is being performed on.
	 * @return BOOL        true upon success false otherwise.  Default implementation returns false to indicate non-implementation.
	 */
	public function run( $model ){
		return false;
	}
}