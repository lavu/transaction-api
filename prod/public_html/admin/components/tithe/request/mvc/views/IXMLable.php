<?php
/**
 * Holds functions for formating Models into XML representations
 */
interface IXMLable {
	/**
	 * The toXML method should translate the class into a serialized XML representation.
	 * @return string an XML representation of the data contained within the model.
	 */
	public function toXML();
}