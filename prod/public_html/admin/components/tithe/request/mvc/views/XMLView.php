<?php
require_once dirname( dirname(__FILE__) ) . '/View.php';
require_once  dirname(__FILE__)  . '/IXMLable.php';

class XMLView extends View {
	public function __construct(){
		parent::__construct();
	}

	public function __destruct(){
		parent::__destruct();
	}

	private static function rtoXML( $obj ){
		$result = '';
		if( is_array( $obj ) || is_object( $obj ) ){
			$arr = (array) $obj;
			foreach( $arr as $key => $val ){
				if( is_numeric( $key ) ){
					$result .= "<row>" . XMLView::rtoXML( $val ) . "</row>";
				} else {
					$result .= "<{$key}>" . XMLView::rtoXML( $val ) . "</{$key}>";
				}
			}
		} else {
			$result = htmlspecialchars( $obj );
		}

		return $result;
	}

	public static function toXML( $obj ){
		$result = '<?xml version="1.0"?>' . "\n";
		$result .= '<results>' . XMLView::rtoXML( $obj ) . '</results>';
		return $result;
	}

	public function draw( $model ){
		if( !$model ){
			return false;
		}

		if( !($model instanceof Model) ){
			return false;
		}

		if( !($model instanceof IXMLable) ){
			return false;
		}

		header('Content-Type: text/xml');
		echo $model->toXML();
		return true;
	}
}