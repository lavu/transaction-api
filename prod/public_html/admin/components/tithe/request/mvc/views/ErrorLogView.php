<?php
require_once dirname(dirname(__FILE__)) . '/View.php';

class LogTypes {
        const error = 0;
        const warn = 1;
        const debug = 2;
}

class ErrorLogObject {
        private
        $type,
        $message,
        $file,
        $line,
        $callingClass,
        $callingFunction;

        public function setType( $type ){
                $this->type = $type;
        }

        public function type(){
                return $this->type;
        }

        public function setMessage( $message ){
                $this->message = $message;
        }

        public function message(){
                return $this->message;
        }

        public function setFile( $file ){
                $this->file = $file;
        }

        public function file(){
                return $this->file;
        }

        public function setLine( $line ){
                $this->line = $line;
        }

        public function line(){
                return $this->line;
        }

        public function setCallingClass( $callingClass ){
                $this->callingClass = $callingClass;
        }

        public function callingClass(){
                return $this->callingClass;
        }

        public function setCallingFunction( $callingFunction ){
                $this->callingFunction = $callingFunction;
        }

        public function callingFunction(){
                return $this->callingFunction;
        }
}

interface IErrorLogable {
        /**
         * This method allows for the object to represent itself as an error-loggable object
         * @return ErrorLogObject The object that holds the information to be output
         */
        public function toErrorLog();
}

class ErrorLogView extends View {

        public function __construct(){
                parent::__construct();
        }

        private static function log( $str ) {
                $backtrace = debug_backtrace();

                //calling details
                $file = $backtrace[1]['file'];
                $line = $backtrace[1]['line'];
                $logtype = $backtrace[1]['function'];

                $calling_class = $backtrace[2]['class'];
                $calling_function = $backtrace[2]['function'];
                echo "[{$file}][{$logtype}] {$calling_class}@{$calling_function}:{$line} {$str}<br />";
                error_log( "[{$file}][{$logtype}] {$calling_class}@{$calling_function}:{$line} {$str}" );
        }

        public static function error( $str ){
                ErrorLogView::log( $str );
        }

        public static function warn( $str ){
                ErrorLogView::log( $str );
        }

        public static function debug( $str ){
                ErrorLogView::log( $str );
        }

        public function draw( $model ){
                if( !$model ){
                        return false;
                }

                if( !($model instanceof Model) ){
                        return false;
                }

                if( !($model instanceof IErrorLogable)){
                        return false;
                }


                $errorLogObj = $model->toErrorLog();
                $typeStr;

                switch( $errorLogObj ){
                        case LogTypes::error : {
                                $typeStr = 'error';
                                break;
                        } case LogTypes::debug : {
                                $typeStr = 'debug';
                                break;
                        } case LogTypes::warn : {
                                $typeStr = 'warn';
                                berak;
                        } default : {
                                $typeStr = 'unknown';
                                break;
                        }
                }
                error_log("[{$errorLogObj->file()}][{$typeStr}] {$errorLogObj->callingClass()}@{$errorLogObj->callingFunction()}:{$$errorLogObj->line()} {$errorLogObj->message()}" );
                return true;
        }
}