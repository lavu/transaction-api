<?php
require_once dirname(dirname(__FILE__)) . '/View.php';
require_once dirname(__FILE__) . '/IHTMLable.php';

/**
 * The HTMLView, when subscribed to a Model that implements IHTMLable, will 'render' itself by calling the toHTML on the Model, when it is set to draw.
 */
class HTMLView extends View {

        public function __construct(){
                parent::__construct();
        }

        /**
         * Given a Model that implements IHTMLable, this method will render the Model by calling toHTML on it, and echoing the result.
         * @param  IHTMLable $model the Model to render.
         * @return BOOL        This method returns true if successfully drawn, false otherwise.
         */
        public function draw( $model ){
                if( !$model ){
                        return false;
                }

                if( !($model instanceof Model) ){
                        return false;
                }

                if( !($model instanceof IHTMLable)){
                        return false;
                }

                header('Content-Type: text/html');
                echo "<!DOCTYPE html>\n";

                $html = '<html><head><body>';
                $html = $model->toHTML( $html );
                $html .= '</body></html>';
                echo $html;
                return true;
        }
}