<?php
/**
 * Holds functions for formating Models into JSON representations
 */
interface IJSONable {
	/**
	 * The toJSON method should translate the class into a serialized JSON representation.
	 * @return string a JSON representation of the data contained within the model.
	 */
	public function toJSON();
}