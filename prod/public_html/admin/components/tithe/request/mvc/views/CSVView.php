<?php
require_once dirname( dirname(__FILE__) ) . '/View.php';
require_once  dirname(__FILE__)  . '/ICSVable.php';

class CSVView extends View {
	public function __construct(){
		parent::__construct();
	}

	public function __destruct(){
		parent::__destruct();
	}

	public static function escapeValue( $value ){
		$str_value = $value.'';
		if( strpos($str_value, ',') !== FALSE || strpos($str_value, '"') !== FALSE ){
			return str_replace('"', '\"', $value);
		}
		return $value;
	}

	public static function toCSV( $data ){
		//At the moment assumed to be an array of arrays;
		$result = '';
		$callable = array( 'CSVView', 'escapeValue' );
		for( $i = 0; $i < count( $data ); $i++ ){
			if( $i === 0 ){
				$keys = array_keys( $data[$i] );
				$result .= implode(',', array_map( $callable, $keys) ) . "\n";
			}
			$values = array_values( $data[$i] );
			$result .= implode(',', array_map( $callable, $values) ) . "\n";
		}

		return $result;
	}

	public function draw( $model ){
		if( !$model ){
			return false;
		}

		if( !($model instanceof Model) ){
			return false;
		}

		if( !($model instanceof ICSVable) ){
			return false;
		}

		header('Content-Type: text/csv');
		echo $model->toCSV();
		return true;
	}
}