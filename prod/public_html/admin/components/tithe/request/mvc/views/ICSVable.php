<?php
/**
 * Holds functions for formating Models into CSV representations
 */
interface ICSVable {
	/**
	 * The toCSV method should translate the class into a serialized CSV representation.
	 * @return string an CSV representation of the data contained within the model.
	 */
	public function toCSV();
}