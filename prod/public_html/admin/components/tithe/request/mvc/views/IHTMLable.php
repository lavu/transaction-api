<?php
/**
 * The IHTMLable interface ensures that anything that implements IHTMLable will contain a method that converts the intneral data structure to a HTML format.
 */
interface IHTMLable {
        /**
         * The toHTML method should convert the internal state of the Model that implements it to a String representation of the HTML format.
         * @param DOMDocument $dom The pre-created DOMDocument for HTML, complete with pre-populated html, head, and body tags.
         * @return string a HTML style representation of the internal state of this Model.
         */
        public function toHTML( $html );
}