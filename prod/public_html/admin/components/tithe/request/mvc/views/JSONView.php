<?php
require_once dirname( dirname(__FILE__) ) . '/View.php';
require_once  dirname(__FILE__)  . '/IJSONable.php';

class JSONView extends View {
	public function __construct(){
		parent::__construct();
	}

	public function __destruct(){
		parent::__destruct();
	}

	public function draw( $model ){
		if( !$model ){
			return false;
		}

		if( !($model instanceof Model) ){
			return false;
		}

		if( !($model instanceof IJSONable) ){
			return false;
		}

		header('Content-Type: text/json');
		echo $model->toJSON();
		return true;
	}
}