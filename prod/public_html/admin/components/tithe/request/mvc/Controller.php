<?php
require_once dirname(__FILE__) . '/Model.php';
require_once dirname(__FILE__) . '/Command.php';

class Controller {
	private
	$models,
	$controllers;

	public function __construct(){
		$this->models = array();
		$this->controllers = array();
	}

	public function __destruct(){
		unset( $this->models );
		unset( $this->controllers );
	}

	public function addModel( $model ){
		if( !$model ) {
			return false;
		}

		if( !($model instanceof Model ) ){
			return false;
		}

		if( in_array($model, $this->models, true) ) {
			return false;
		}

		array_push($this->models, $model);
		return true;
	}

	public function removeModel( $model ){
		if( !$model ) {
			return false;
		}

		if( !($model instanceof Model ) ){
			return false;
		}

		$index = -1;

		foreach( $this->models as $idx => $v ){
			if($v === $model ){
				$index = $idx;
				break;
			}
		}

		if($index == -1){
			return false;
		}
		array_splice($this->models, $index, 1);
		return true;
	}

	public function addController( $controller ){
		if( !$controller ) {
			return false;
		}

		if( !($controller instanceof Controller ) ){
			return false;
		}

		if( in_array($controller, $this->controllers) ) {
			return false;
		}

		array_push($this->controllers, $controller);
		return true;
	}

	public function removeController( $controller ){
		if( !$controller ) {
			return false;
		}

		if( !($controller instanceof Controller ) ){
			return false;
		}

		$index = -1;

		foreach( $this->controllers as $idx => $v ){
			if($v === $controller ){
				$index = $idx;
				break;
			}
		}

		if($index == -1){
			return false;
		}
		array_splice($this->controllers, $index, 1);
		return true;
	}

	public function runCommand( $command ){
		if( !$command ){
			return false;
		}

		if(! ($command instanceof Command ) ){
			return false;
		}
		$result = true;
		$count = 0;
		foreach( $this->controllers as $index => $controller ){
			$result = $controller->runCommand( $command ) && $result;
		}

		foreach( $this->models as $index => $model ){
			if( ! $command->filter( $model ) ){
				$result = $model->run( $command ) && $result;
				// $count++;
			}
		}
		return $result;
	}
}