<?php
require_once dirname(__FILE__) . '/View.php';
require_once dirname(__FILE__) . '/Command.php';

/**
 * The Model class is a basic model implementation.  It defines basic behavior for models, and allows for the models to 'subscribe' and 'unsubscribe' via the addView and removeView methods respectively.  Any data member that can be rendered (drawn) should extend the Model class by default.
 */
class Model {
	private $views;

	/**
	 * Initializes the Model with any needed data members.  Any class extending needs to be sure to call the parent constructor.
	 */
	public function __construct(){
		$this->views = array();
	}

	/**
	 * The destructor does cleanup of the Model, any class that extends this needs to call the parent destructor.
	 */
	public function __destruct(){
		unset( $this->views );
	}

	/**
	 * The addView method allows for a view to subscribe to this model for updates.  It will be pushed onto an array of views that is interally maintained.
	 * @param View $view the view that wishes to subscribe to this Model.
	 * @return BOOL      true upon being added to the list of views, false otherwise.
	 */
	public function addView( $view ){
		if( !$view ) {
			return false;
		}

		if( !($view instanceof View ) ){
			return false;
		}

		if( in_array($view, $this->views) ) {
			return false;
		}

		array_push($this->views, $view);
		$view->notify( $this );
		return true;
	}

	/**
	 * The removeView method allows for a view to unsubscribe to this Model for updates.  It will attempt to remove the View from the internally represented views list.
	 * @param  View $view The View to unsubscribe from this Model.
	 * @return BOOL       true upon successfully unsubscribing, false otherwise.
	 */
	public function removeView( $view ){
		if( !$view ) {
			return false;
		}

		if( !($view instanceof View ) ){
			return false;
		}

		$index = -1;

		foreach( $this->views as $idx => $v ){
			if($v === $view ){
				$index = $idx;
				break;
			}
		}

		if($index == -1){
			return false;
		}
		array_splice($this->views, $index, 1);
		return true;
	}

	/**
	 * The notifyViews method iterates through the internally stored views and calls notify on them passing itself as a reference.  Any class that extends Model should call this upon successful call of a Setter.
	 * @return void there is no return value for this method.
	 */
	public function notifyViews(){
		foreach( $this->views as $index => $view ){
			$view->notify( $this );
		}
	}

	/**
	 * The run method attempts to run the provided Command.
	 * @param  Command $command The command to run on this Model
	 * @return BOOL          true upon successful Command execution, false otherwise.
	 */
	public function run( $command ){
		if( !$command ){
			return false;
		}

		if(! ($command instanceof Command) ){
			return false;
		}

		return $command->run( $this );
	}
}