<?php
require_once dirname(dirname(__FILE__)) . '/Model.php';
require_once dirname(dirname(__FILE__)) . '/views/HTMLView.php';
require_once dirname(dirname(__FILE__)) . '/views/JSONView.php';
require_once dirname(dirname(__FILE__)) . '/views/XMLView.php';

class DocumentationModel extends Model implements IHTMLable, IJSONable, IXMLable {

        private
        $classCommentDoc,
        $methodCommentDocs;

        public function __construct(){
                parent::__construct();
                $this->methodCommentDocs = array();
        }

        public function __destruct(){
                unset($this->methodCommentDocs);
                parent::__destruct();
        }

        public function setClassCommentDoc( $classCommentDoc ){
                $this->classCommentDoc = $classCommentDoc;
        }

        public function addMethodCommentDoc( $methodCommentDoc ){
                $this->methodCommentDocs[] = $methodCommentDoc;
        }

        private function parseReferences( $str, $node ){
                preg_match_all("/([^\s\.]+)[\s\.]?/", $str, $a_matches );
                $a_matches = $a_matches[0];
                for( $i = 0; $i < count($a_matches); $i++ ){
                        $potentialClassName = substr( $a_matches[$i], 0, strlen($a_matches[$i]) - 1);
                        if( class_exists( $potentialClassName ) ){
                                $node .= "<a href='{$potentialClassName}.html'>{$potentialClassName}</a>";
                                $node .=  substr( $a_matches[$i], -1 ) ;
                        } else {
                                $node .= $a_matches[$i];
                        }
                }
                return $node;
        }

        private function outputCommentDoc( $commentDoc, $node ){
                //echo '<pre>' . print_r($commentDoc, true) . '</pre>';
                if( $commentDoc->depricated() ){
                        $node .= '<p>DEPRICATED</p>';
                }

                $reflectionClass = new ReflectionClass( $commentDoc->className() );

                //$method = $commentDoc->method();
                if( '' == ($commentDoc->method()) ){
                        $node .= "<legend><h1>{$commentDoc->className()}";
                        $parentClass = $reflectionClass->getParentClass();
                        if($parentClass){
                                $node .= ' extends ';
                                $node .= "<a href='{$parentClass->getName()}.html'>{$parentClass->getName()}</a>";
                        }

                        $interfaces = $reflectionClass->getInterfaceNames();
                        if( count($interfaces) ){
                                $node .= ' implements ';
                                for($i = 0; $i < count($interfaces); $i++ ){
                                        $node .= "<a href='{$interfaces[$i]}.html'>{$interfaces[$i]}</a>";
                                        if($i < count($interfaces)-1 ){
                                                $node .= ', ';
                                        }
                                }
                        }
                        $node .= '</h1></legend>';
                } else {
                        $node .= "<a name='{$commentDoc->className()}::{$commentDoc->method()}'></a>";
                        $node .= "<legend><h3>{$commentDoc->className()}::{$commentDoc->method()}( ";
                        if( NULL !== $commentDoc->params() ){
                                $params = $commentDoc->params();
                                $count = count($params);
                                foreach( $params as $param ){
                                        if( class_exists( $param->type() ) ){
                                                $node .= "<a href='{$param->type()}.html'>{$param->type()}</a>";
                                        } else {
                                                $node .= $param->type();
                                        }
                                        $node .= ' ' . $param->name();
                                        if( --$count ){
                                                $node .= ', ';
                                        }
                                }
                        }
                        $node .= ' )';
                        $node .= '</h3></legend>';
                }

                if( NULL !== $commentDoc->version() ){
                        $node .= "<b>Version</b>: {$commentDoc->version()}<br />";
                }

                if( NULL !== $commentDoc->author() ){
                        $node .= "<b>Author</b>: {$commentDoc->author()}<br />";
                }

                if( NULL !== $commentDoc->params() ){
                        $params = $commentDoc->params();
                        foreach( $params as $param ){
                                $node .= "<b>Parameter</b>: ";
                                if( class_exists( $param->type() ) ){
                                        $node .= "<a href='{$param->type()}.html'>{$param->type()}</a>";
                                } else {
                                        $node .= $param->type();
                                }
                                $node .= " {$param->name()} ";
                                $node = $this->parseReferences( $param->doc(), $node );
                                $node .= '<br />';
                        }
                }

                if( NULL !== $commentDoc->returns() ){
                        $returns = $commentDoc->returns();
                        $node .= "<b>Returns</b>: ";
                        if( class_exists( $returns->type() ) ){
                                $node .= "<a href='{$returns->type()}.html'>{$returns->type()}</a>";
                        } else {
                                $node .= $returns->type();
                        }
                        $node .= ' ';
                        $node = $this->parseReferences( $returns->doc(), $node );
                        $node .= '<br />';
                }

                if( NULL !== $commentDoc->todo() ){
                        $todo = $commentDoc->todo();
                        foreach( $todo as $t ){
                                $node .= "<b>TODO</b>: ";
                                $node = $this->parseReferences( $t->doc(), $node );
                                $node .= '<br />';
                        }
                }

                if( NULL !== $commentDoc->doc() ){
                        $paragraphTexts = explode("\n", $commentDoc->doc() );
                        foreach( $paragraphTexts as $text ){
                                $node .= '<p>';
                                $node = $this->parseReferences( $text, $node );
                                $node .= '</p>';
                        }
                }

                return $node;
        }

        public function toHTML( $html ){
                $result = $html;

                $result .= '<fieldset>';

                $result = $this->outputCommentDoc( $this->classCommentDoc, $result );
                {
                        // Time to output the Legend
                        $result .= '<ul>';
                        foreach( $this->methodCommentDocs as $methodCommentDoc ){
                                $result .= "<li><a href='#{$methodCommentDoc->className()}::{$methodCommentDoc->method()}'>{$methodCommentDoc->className()}::{$methodCommentDoc->method()}</a></li>";
                        }
                        $result .= '</ul>';
                }

                foreach( $this->methodCommentDocs as $methodCommentDocs ){
                        $result .= '<fieldset>';
                        $result = $this->outputCommentDoc( $methodCommentDocs, $result );
                        $result .= '</fieldset>';
                }
                $result .= '</fieldset>';
                return $result;
        }

        public function toJSON(){
                $result = new stdClass();
                $result->classDocumentation = $this->classCommentDoc;
                $result->methodDocumentation = $this->methodCommentDocs;

                return json_encode( $result );
        }

        public function toXML(){
                $result = new stdClass();
                $result->classDocumentation = $this->classCommentDoc;
                $result->methodDocumentation = $this->methodCommentDocs;

                return XMLView::toXML( $result );
        }
}