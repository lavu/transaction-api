<?php
require_once dirname( dirname(__FILE__) ) . '/Model.php';
require_once dirname( dirname(__FILE__) ) . '/views/XMLView.php';
require_once dirname( dirname(__FILE__) ) . '/views/JSONView.php';
require_once dirname( dirname(__FILE__) ) . '/views/CSVView.php';

class GeneralData extends Model implements IXMLable, IJSONable, ICSVable {
	private $data;
	public function __construct( $data ){
		parent::__construct();
		$this->data = $data;
	}

	public function __destruct(){
		unset( $this->data );
		parent::__destruct();
	}

	private function __toXML( $data, $node ){
		if( is_array( $data ) ){
			foreach( $data as $key => $value ){
				$row;
				if( is_numeric( $key ) ){
					$row = new DOMElement('row');
				} else {
					$row = new DOMElement( $key );
				}
				$node->appendChild( $row );
				$this->__toXML( $value, $row );
			}
		} else if( is_object( $data ) ){
			foreach( $data as $member => $value ){
				$row = new DOMElement( $key );
				$node->appendChild( $row );
				$this->__toXML( $value, $row );
			}
		} else {
			$node->appendChild( new DOMTextNode( $data ) );
		}
	}

	public function toXML(){
		return XMLView::toXML( $this->data );
	}

	public function toJSON(){
		if( function_exists('json_encode') ){
			return json_encode( $this->data );
		} else if ( file_exists( __DOCUMENT_ROOT__ . '/cp/resources/json.php' ) ) {
			require_once __DOCUMENT_ROOT__ . '/cp/resources/json.php';
			return LavuJson::json_encode( $this->data );
		}
	}

	public function toCSV(){
		//At the moment assumed to be an array of arrays;
		return CSVView::toCSV( $this->data );
	}
}