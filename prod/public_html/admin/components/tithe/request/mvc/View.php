<?php
	require_once dirname(__FILE__) . '/Model.php';

	class View{
		private $notifiers;
		public function __construct(){
			$this->notifiers = array();
		}

		public function __destruct(){
			unset( $this->notifiers );
		}

		public function notify( $notifier ){
			if( !$notifier ){
				return false;
			}

			if( !($notifier instanceof Model ) ){
				return false;
			}

			if( in_array( $notifier, $this->notifiers ) ) {
				return false;
			}

			array_push( $this->notifiers, $notifier );
			return true;
		}

		public function update(){
			while( count( $this->notifiers ) ){
				$notifier = array_pop( $this->notifiers );
				$this->draw( $notifier );
			}
		}

		public function draw( $model ){
			return false;
		}
	}