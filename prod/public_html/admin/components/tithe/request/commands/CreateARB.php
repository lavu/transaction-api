<?php
require_once __MVC_ROOT__ . '/Command.php';

class CreateARB extends Command {
	private
		$fileHandle;

	public function __construct(){
		parent::__construct();
		$this->fileHandle = fopen( 'php://input', 'r' );
	}

	public function __destruct(){
		parent::__destruct();
		fclose( $this->fileHandle );
	}

	public function filter( Model $model ){
		if( $model instanceof TitheRequestModel ){
			return false;
		}
		return true;
	}

	public function run( Model $model ){
		if( !$model ){
			return false;
		}

		if( !($model instanceof TitheRequestModel) ){
			return false;
		}

		while( $contents = fread( $this->fileHandle, 1024 ) ){
			echo $contents;
		}

		// $abpInsertVars = array(
		// 	'createtime' => 'now()',
		// 	'updatetime' => 'now()',
		// 	'customer_id' => $args['customerId'],
		// 	'order_id' => $args['orderId'],
		// 	'location_id' => $args['locationId'],
		// 	'type' => 'billing_profile',
		// 	'name' => $this->getLocationName( $args['locationId'] ) .' '. $args['profileName'],
		// 	'status' => 'active',\
		// 	'recurring_amount' => $args['recurringAmount'],
		// 	'invoice_template' => $this->generateInvoiceTemplateXml( $args['orderId'] ),
		// 	'start_date' => date("Y-m-d H:i:s"),
		// 	'end_date' => date("Y-m-d H:i:s", strtotime('+ 1 year')),
		// 	'bill_frequency' => 'monthly',
		// 	'bill_event_offset' => '1',
		// 	'cc_ref_num' => $ccRefNum,
		// 	'cc_card_type' => $ccCardType,
		// 	'cc_num_last4' => $ccNumLast4,
		// 	'cc_exp_date' => $ccExpDate
		// );
		
		// $abpInsertSql = "insert into `med_billing` (`createtime`, `updatetime`, `customer_id`, `order_id`, `location_id`, `type`, `name`, `status`, `recurring_amount`, `invoice_template`, `start_date`, `end_date`, `bill_frequency`, `bill_event_offset`, `cc_ref_num`, `cc_card_type`, `cc_num_last4`, `cc_exp_date`) values ([createtime], [updatetime], '[customer_id]', '[order_id]', '[location_id]', '[type]', '[name]', '[status]', '[recurring_amount]', '[invoice_template]', [start_date], [end_date], '[bill_frequency]', [bill_event_offset], '[cc_ref_num]', '[cc_card_type]', '[cc_num_last4]', '[cc_exp_date]')";

		$abpInsertOk = lavu_query( $abpInsertSql, $abpInsertVars );
		$name = $this->getLocationName( $args['locationId'] ) .' '. $args['profileName'];
		$template = $this->generateInvoiceTemplateXml( $args['orderId'] );
		$startDate = date('Y-m-d H:i:s');
		$endDate = date("Y-m-d H:i:s", strtotime('+ 1 year'));
		$bill_frequency = 'monthly';

		TitheRequestController::controller()->runCommand( new RunQuery( "INSERT INTO `med_billing` (`createtime`, `updatetime`, `customer_id`, `order_id`, `location_id`, `type`, `name`, `status`, recurring_amount`, `invoice_template`, `start_date`, `end_date`, `bill_frequency`, `bill_event_offset`, `cc_ref_num`, `cc_card_type`, `cc_num_last4`, `cc_exp_date` ) VALUE ( now(), now(), ?, ?, ?, 'billing_profile', ?, 'active', ?, ?, ?, ?, ?, 1, ?, ?, ?, ? )",
			array(
				&$args['customerId'],
				&$args['orderId'],
				&$args['locationId'],
				&$name,
				&$args['recurringAmount'],
				&$template,
				&$startDate,
				&$endDate,
				&$bill_frequency,
				&$ccRefNum,
				&$ccCardType,
				&$ccNumLast4,
				&$ccExpDate
				) ) );


		return true;
	}
}