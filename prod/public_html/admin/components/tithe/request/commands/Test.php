<?php

require_once __MVC_ROOT__ .'/Command.php';

class Test extends Command {

	public function __construct(){
		parent::__destruct();
	}

	public function __destruct(){
		parent::__destruct();
	}

	public function filter( $obj ){
		return false;
	}

	public function run( $model ){
		session_start();
		// ErrorLogView::debug( print_r( $_SESSION, true ) );
		
		$view = null;
		switch( __VIEW__ ) {
			case 'json' :{
				require_once __MVC_ROOT__ .'/views/JSONView.php';
				$view = new JSONView();
				break;
			} case 'xml' : {
				require_once __MVC_ROOT__ .'/views/XMLView.php';
				$view = new XMLView();
				break;
			} case 'html' : {
				require_once __MVC_ROOT__ .'/views/HTMLView.php';
				$view = new HTMLView();
				break;
			} case 'csv' : {
				require_once __MVC_ROOT__ .'/views/CSVView.php';
				$view = new CSVView();
				break;
			}
		}

		require_once __DOCUMENT_ROOT__ . '/cp/resources/core_functions.php';
		require_once __ENTRY_ROOT__ . '/dbHandler/controllers/DatabaseController.php';
		require_once __ENTRY_ROOT__ . '/dbHandler/models/DatabaseModel.php';
		require_once '/home/poslavu/private_html/myinfo.php';

		$company_info = sessvar('company_info');
		$dataname = $company_info['data_name'];
		$data_access = $company_info['data_access'];
		$company_id = $company_info['id'];
		define( '__POSLAVU_CUSTOMER_DATABASE__', 'poslavu_'.$dataname.'_db' );

		TitheRequestController::controller()->addController( DatabaseController::controller() );
		$db_main_rest_connection = new DatabaseModel( my_poslavu_host(), my_poslavu_username(), my_poslavu_password(), 'poslavu_MAINREST_db' );
		DatabaseController::controller()->addModel( $db_main_rest_connection );
		
		$letter = substr($dataname,0,1);
		if(ord($letter) >= ord("a") && ord($letter) <= ord("z")){
			$letter = $letter;
		}else{
			$letter = "OTHER";
		}
		$tablename = "rest_" . $letter;

		require_once __ENTRY_ROOT__ . '/dbHandler/commands/RunQuery.php';
		$query = new RunQuery( "SELECT `companyid`,`data_name`, AES_DECRYPT(`data_access`,'lavupass') AS `data_access` FROM `{$tablename}` where `data_name`=?", array( &$dataname ) );

		if( !(TitheRequestController::controller()->runCommand( $query )) ){
			return false;
		}

		$dataaccess_information = $query->fetchRow( QueryResultType::ASSOCIATIVE_ARRAY );
		$db_connection = new DatabaseModel( my_poslavu_host( $dataname ), 'usr_pl_' . $company_id, $dataaccess_information['data_access'], 'poslavu_'.$dataname.'_db' );
		if( !DatabaseController::controller()->addModel( $db_connection ) ){
			ErrorLogView::debug( print_r( $db_connection, true ) . "\n" );
		}

		$query = new RunQuery("SELECT * FROM `locations` LIMIT 1", array( ) );
		$query->setTargetDatabase( 'poslavu_'.$dataname.'_db' );

		if( !(TitheRequestController::controller()->runCommand( $query )) ){
			ErrorLogView::debug('here');
			return false;
		}
		// ErrorLogView::debug( print_r( , true ) );
		require_once __MVC_ROOT__ . '/models/GeneralData.php';
		$model = new GeneralData( $query->fetchRow( QueryResultType::ASSOCIATIVE_ARRAY ) );
		$model->addView( $view );
		$view->update();

		return true;
	}
}