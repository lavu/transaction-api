<?php
require_once __MVC_ROOT__ . '/Command.php';
require_once __MVC_ROOT__ . '/Model.php';
require_once __MVC_ROOT__ . '/views/ErrorLogView.php';

require_once __MVC_ROOT__ . '/models/DocumentationModel.php';

/**
 * The DocDoc Class is a class that's meant to help with the parsing of Comment Text Blocks.  It is the base of all Comment Blocks as every comment block have a Documentation section aside from the tags.  In general, this is just a class that houses the Textual information of Comment Documentation, that is otherwise not parsed into something else.
 * @example /** Comment Block Text
 * @todo Improve this!
 * @version 1.0
 * @author Theodore Schnepper
 */
class DocDoc {
    public $doc;
        /**
         * Retrieves the stored textual representation of a Comment Block.  There is no other information here, everything is just represented as a string.
         * @return string The Textual Representation of the Comment Block
         */
        public function doc(){
            return $this->doc;
        }

        /**
         * The setDoc method takes the given string, and stores it as the Textual Representation of the Comment Block
         * @param string $doc The Documentation Section of the Comment Block.
         */
        protected function setDoc( $doc ){
            $this->doc = $doc;
        }

        /**
         * The parse function, takes a given string, and will parse out the documentation section.  In this case, since this is the end point, if nothing has been parsed as anything else, it will be stored in here.  Everything passed into here is stored into a new DocDoc object, and returned.
         * @param  string $str The string to parse.
         * @return DocDoc      A container to hold the parsed string.
         */
        public static function parse( $str ){
            $result = new DocDoc();
            $result->doc = $str;
            return $result;
        }
    }

/**
 * The TypeDoc Class expands on the basic implementation of DocDoc, and adds to it the ability to parse out and store a variable's Type.  This will be used specifically when indicated by certain tags.
 * @example @return
 * @version 1.0
 * @author Theodore Schnepper
 */
class TypeDoc extends DocDoc {
    public $type;
        /**
         * The type method returns a string representation of the type of the value that the TypeDoc is representing
         * @return string The type of the value this TypeDoc is representing
         */
        public function type(){
            return $this->type;
        }

        /**
         * The setType method sets the type of the TypeDoc representation to the type provided.
         * @param string $type The type to store as this type.
         */
        protected function setType( $type ){
            $this->type = $type;
        }

        /**
         * The parse method is a static method that takes a string and parses the result into a type and documentation explaining what the value is.  The parsing algorithmn assumes that the first grouping of non-space characters denotes the type, and everything following that is attributed to the Documentation itself.
         * @example Type Doc for the rest.
         * @param  string $str The string to be parsed.
         * @return TypeDoc      A TypeDoc holding information conerning Documentation, and the Type of the value this TypeDoc is referencing
         */
        public static function parse( $str ){
            $result = new TypeDoc();
            preg_match( "/(\S+)\s+(.*)/" , $str, $a_matches );
            if( $a_matches && count($a_matches) ){
                $result->setType( $a_matches[1] );
                $result->setDoc( $a_matches[2] ) ;
            }
            return $result;
        }
    }

/**
 * The ParamDoc Class expands on the implementation of TypeDoc, and adds to it the ability to parse out and store a variable's name.  This will be used when referring to a named variable, rather than a returned variable.
 */
class ParamDoc extends TypeDoc{
    public        $name;

        /**
         * The name method returns the currently stored name of this referenced variable.
         * @return string The name method returns the currently stored name of this referenced variable.
         */
        public function name(){
            return $this->name;
        }

        /**
         * The setName method sets the name of the intenal representation of the name.
         * @param string $name The string to store as the name of this internally represented variable name.
         */
        protected function setName( $name ){
            $this->name = $name;
        }

        public static function parse( $str ){
            $result = new ParamDoc();
            preg_match( "/(\S+)\s+(\S+)\s+(.*)/" , $str, $a_matches );
            if( $a_matches && count($a_matches) ){
                $result->setType( $a_matches[1] );
                $result->setName( $a_matches[2] );
                $result->setDoc( $a_matches[3] );
            }
            return $result;
        }
    }

    class CommentDoc extends DocDoc {
        public
        $params,
        $returns,
        $version,
        $author,
        $depricated,
        $todo,
        $className,
        $method;

        public function __construct(){
            $this->params = array();
            $this->todo = array();
            $this->returns = null;
            $this->version = null;
            $this->author = null;
            $this->depricated = false;
            $this->className = '';
            $this->method = '';
        }

        public function params() {
            return $this->params;
        }

        public function returns(){
            return $this->returns;
        }

        public function version(){
            return $this->version;
        }

        public function author(){
            return $this->author;
        }

        public function depricated(){
            return $this->depricated;
        }

        public function todo(){
            return $this->todo;
        }

        public function className(){
            return $this->className;
        }

        public function setClassName( $className ){
            $this->className = $className;
        }

        public function method(){
            return $this->method;
        }

        public function setMethod( $method ){
            $this->method = $method;
        }

        private function parseDoc( $str ){
            $comment_parts = split("\n", $str);
            $result = '';
            $a_matches;

            static $keywords = array(
                'author',
                'version',
                'param',
                'return',
                'todo',
                'depricated'
                );

            foreach( $comment_parts as $comment_part ) {

                $comment_part = preg_replace('/^\s*\/\*+\s*/', '', $comment_part);
                $comment_part = preg_replace('/^\s*\*\/\s*/', '', $comment_part);
                $comment_part = preg_replace('/^\s*\*+\s*/', '', $comment_part);

                preg_match("/^@(\w+)\s(.*)$/", $comment_part, $a_matches);
                if($a_matches && count($a_matches) && in_array($a_matches[1], $keywords)){
                                //echo '<pre>' . print_r($a_matches, true) . '</pre>';
                    switch( $a_matches[1] ){
                        case 'author' : {
                            $this->author = $a_matches[2];
                            break;
                        } case 'version' : {
                            $this->version = $a_matches[2];
                            break;
                        } case 'depricated' : {
                            $this->depricated = true;
                            break;
                        } case 'param' : {
                            if(! $this->params ){
                                $this->params = array();
                            }
                            $this->params[] = ParamDoc::parse( $a_matches[2] );
                            break;
                        } case 'todo' : {
                            if(! $this->todo ){
                                $this->todo = array();
                            }
                            $this->todo[] = DocDoc::parse( $a_matches[2] );
                            break;
                        } case 'return' : {
                            $this->returns = TypeDoc::parse( $a_matches[2] );
                            break;
                        } default: {

                        }
                    }
                        } else { //Must be a comment?
                            $this->setDoc( "{$this->doc()}\n{$comment_part}" );

                        }
                    }

                    return $result;
                }

                public static function parse( $str ){
                    $result = new CommentDoc();
                    $result->parseDoc( $str );
                    return $result;
                }
            }


            define('DOCUMENTATION_ERROR_CLASS_NOT_FOUND', 545);

/**
 * The Documentation Class is a Command that automatically encapsulates the Documentation Framework.  The Documentation class utilizes the Reflection framework in order to help it extract the Commentblocks above Classes and Methods.
 * @version 1.0
 * @author Theodore Schnepper
 */
class Documentation extends Command {
    private
    $class;

        /**
         * The Constructor for Documentation calls the parent constructor for Command.  It takes a Class Name as an argument, and will store it for use when it needs to run the command itself.
         * @param string $args The arguments string for a Command.
         * @todo Have Requestd automatically break up the arguments instead of passing the burden to the Command to help make it much more flexible without repeating parsing code.
         */
        public function __construct( $className ){
            parent::__construct();
            $this->class = str_ireplace('-', '_', $className);
        }

        /**
         * The destructor tears down the stored Class Name and then calls the destructor of the parent Class.
         */
        public function __destruct(){
            unset($this->class);
            parent::__destruct();
        }

        private function lookForClassFile( $className, $path = __ENTRY_ROOT__ ){
            $result = false;
            $directory = opendir( $path );
            while( false !== ($filename = readdir( $directory )) ){
                if($filename == '.' || $filename == '..'){
                    continue;
                }

                if( is_dir( "{$path}/{$filename}" ) ){
                    $result = $result || $this->lookForClassFile( $className, $path . '/' . $filename );
                } else if( is_file( "{$path}/{$filename}" ) ){
                    $classNameFile = "{$className}.php";
                    if( $filename == $classNameFile ){
                        require_once "{$path}/{$filename}";
                        return true;
                    }
                }
            }

            return $result;
        }

        /**
         * This Command runs on a RequestModel.
         * The Documentation Command first looks for the given className in the current namespace of class definitions.  If it does not find the declared class there, it will then look for a File that shares the same name as the Class in order to require it, and attempt to resolve the dependency issue.  Upon successfully finding the Class, this Command will create a new DocumentationModel and pass it a generated CommentDoc.
         * @param  Model $model Should only run on a RequestModel
         * @return BOOL        true if the Command was successful, false otherwise.
         */
        public function run( $model ){
            if( !$model ){
                return false;
            }

            if( !($model instanceof TyroRequestModel ) ){
                return false;
            }

            if( !class_exists( $this->class ) && !$this->lookForClassFile( $this->class ) && !$this->lookForClassFile( $this->class, dirname(__DIR__) ) ) {
                $view = __VIEW__;
                require_once dirname(__DIR__) . '/requestHandler/commands/ReportErrorCommand.php';
                RequestController::controller()->runCommand( new ReportErrorCommand( $view, DOCUMENTATION_ERROR_CLASS_NOT_FOUND, 'Unable to Find the Specified Class for Documentation.') );
                return false;
            }

            $documentationModel = new DocumentationModel();

            $reflectionClass = new ReflectionClass( $this->class );

            $methods = $reflectionClass->getMethods();
            $classCommentDoc = CommentDoc::parse( $reflectionClass->getDocComment() );
            $classCommentDoc->setClassName( $reflectionClass->getName() );

            $documentationModel->setClassCommentDoc( $classCommentDoc );

            foreach( $methods as $method ){
                $reflectionMethod = new ReflectionMethod( $method->class, $method->name );
                if($reflectionMethod->isPrivate() || $reflectionMethod->isProtected() ){
                    continue;
                }

                $methodCommentDoc = CommentDoc::parse( $reflectionMethod->getDocComment() );
                $methodCommentDoc->setClassName( $method->class );
                $methodCommentDoc->setMethod( $method->name );
                $documentationModel->addMethodCommentDoc( $methodCommentDoc );
            }

            $view = null;
            switch( __VIEW__ ){
                case 'json' : {
                    $view = new JSONView();
                    break;
                } case 'xml' : {
                    $view = new XMLView();
                    break;
                } case 'html' : {
                } default : {
                    $view = new HTMLView();
                    break;
                }
            }

            $documentationModel->addView( $view );
            $view->update();
            return true;
        }
    }