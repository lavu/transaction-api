<?php

	require_once __MVC_ROOT__ .'/Command.php';

	class GetStaticData extends Command{
		public function __construct(){
			parent::__destruct();
		}

		public function __destruct(){
			parent::__destruct();
		}

		public function filter( $obj ){
			if( $obj instanceof TitheRequestModel ){
				return false;
			}
			return true;
		}

		private function createFailResponse( $message ){
			return new GeneralData(
				array(
					'status' => 'failed',
					'fail_reason' => $message
					)
				);
		}

		private function createResponse( $status, $data ){
			return new GeneralData(
				array_merge( array('status' => $status ), $data )
				);
		}

		public function run( $model ){
			require_once __MVC_ROOT__ . '/models/GeneralData.php';
			$view = null;
			switch( __VIEW__ ){
				case 'csv':
					require_once __MVC_ROOT__ . '/views/CSVView.php';
					$view = new CSVview();
					break;
				case 'html':
					require_once __MVC_ROOT__ . '/views/HTMLView.php';
					$view = new HTMLView();
					break;
				case 'xml':
					require_once __MVC_ROOT__ . '/views/XMLView.php';
					$view = new XMLView();
					break;
				case 'json':
				default:
					require_once __MVC_ROOT__ . '/views/JSONView.php';
					$view = new JSONView();
					break;
			}

			//We validate the ajax request.
			if(empty($_REQUEST['cc'])){
				$generalData = $this->createFailResponse( 'did not pass cc' );
				$generalData->addView( $view );
				$view->update();
				return false;
			}
			if(empty($_REQUEST['data_name'])){
				$generalData = $this->createFailResponse( 'did not pass dataname' );
				$generalData->addView( $view );
				$view->update();
				return false;
			}
			if($_REQUEST['member_id'] == ""){ // not usin empty() so 0 can be used for guest login
				$generalData = $this->createFailResponse( 'did not pass member id' );
				$generalData->addView( $view );
				$view->update();
				return false;
			}
			if(empty($_REQUEST['security_hash'])){
				$generalData = $this->createFailResponse( 'did not pass security_hash' );
				$generalData->addView( $view );
				$view->update();
				return false;
			}
			$security_hash = sha1($_REQUEST['data_name'].'garbonzo');
			if( $_REQUEST['security_hash'] != $security_hash ){
				$generalData = $this->createFailResponse( 'security hash does not match' );
				$generalData->addView( $view );
				$view->update();
				return false;
			}

			//To hook in mysql we import 'info_inc.php' which connects to the mysql_link if $_REQUEST['cc'] is set.
			//  commands->request->tithe->components->dev|admin
			// require_once('../../../../lib/info_inc.php');
			// require_once('../../../../resources/json.php');
			//HERE WE ARE PULLING DOWN ALL STATIC DATA WE CAN AT ONE TIME TO THE JS.
			//If there's an option to ignore $ministryStaticData if already set in the javascript.
			$ministryStaticData = array();
			if(empty($_REQUEST['ignore_ministry_data'])){
				$ministryStaticData = $this->getMinistryStaticDataArr();
			}
			$m_id = $_REQUEST['member_id'];
			$memberStaticData = $this->getMemberStaticDataArr($m_id);
			$ARBStaticData = $this->getARBStaticDataArr($m_id);
			$memberActionLog = $this->getMemberActionLogArr($m_id, 1);
			$GlobalDataArr = array(
				"member" => $memberStaticData,
				"ministry" => $ministryStaticData,
				"ARB" => $ARBStaticData,
				"ActionLog" => $memberActionLog
			);

			$generalData = $this->createResponse( 'success', $GlobalDataArr );
			$generalData->addView( $view );
			$view->update();

			// $globalDataArrJSON = json_encode($GlobalDataArr);
			// echo $globalDataArrJSON;
			return true;
		}

		private function getMemberStaticDataArr($med_customers_id){
			{
				if ($med_customers_id == "0") {
					return array("f_name"=>"Guest","l_name"=>"","id"=>"0","user_name"=>"");
				} else {			
					$query = new RunQuery( "SELECT * FROM `med_customers` WHERE `id`=?", array( &$med_customers_id ) );
					if( TitheRequestController::controller()->runCommand( $query ) ){
						return $query->fetchRow( QueryResultType::ASSOCIATIVE_ARRAY );
					}
					return false;
				}
			}
		}

		private function getMinistryStaticDataArr(){
			//grab in config, setting=lavu_tithe_id, gives a menu id , select * from menu_items where menu_id = lavu_tithe_id... that gives all ministries.
			//custom tithe ammounts from config table, lavu)tithe_amts gives  example in utterly_delicious.

			//   Tithe Amounts as stored in the config.
			$tithe_amt_arr = array();
			{
				$query = new RunQuery( "SELECT * FROM `config` WHERE `setting`='lavu_tithe_amts'", array() );
				if( TitheRequestController::controller()->runCommand( $query ) ){
					$tithe_amt_arr = $query->fetchRow( QueryResultType::ASSOCIATIVE_ARRAY );
				}
			}

			//   The Ministries as stored in the menu_items, by the config field lavu_tithe_id (which equates to the the menu_id of those to include.)
			$lavu_tithe_id = null;
			{
				$query = new RunQuery( "SELECT * FROM `config` WHERE `setting`='lavu_tithe_id'", array() );
				if( TitheRequestController::controller()->runCommand( $query ) ){
					$queryResults = $query->fetchRow( QueryResultType::ASSOCIATIVE_ARRAY );
					$lavu_tithe_id = $queryResults['value'];
				}
			}

			//   Now we pull all the ministries from the menu_items table where the menu id equals to the lavu_tithe_id.
			$ministriesAsStoredInMenuItems = array();
			{
				$query = new RunQuery( "SELECT `id`, `menu_id`, `name`, `active`, `_deleted` FROM `menu_items` WHERE `menu_id`=? ORDER BY `_order`", array( &$lavu_tithe_id ) );
				if( TitheRequestController::controller()->runCommand( $query ) ){
					while( $currRow = $query->fetchRow( QueryResultType::ASSOCIATIVE_ARRAY ) ){
						$ministriesAsStoredInMenuItems[] = $currRow;
					}
				}
			}
			return array("tithe_amounts"=>$tithe_amt_arr, "ministry_list" => $ministriesAsStoredInMenuItems);
		}

		//med_billing
		private function getARBStaticDataArr($med_customers_id){
			$arrayOfRowsPulled = array();
			{
				$query = new RunQuery( "SELECT * FROM `med_billing` WHERE `customer_id`=? AND `status`='active' ORDER BY `id` ASC", array( &$med_customers_id) );
				if( TitheRequestController::controller()->runCommand( $query ) ){
					while( $currRow = $query->fetchRow( QueryResultType::ASSOCIATIVE_ARRAY ) ){
						$arrayOfRowsPulled[$currRow['id']] = $currRow;
					}
				}
			}
			return $arrayOfRowsPulled;
		}

		//med_action_log
		private function getMemberActionLogArr($med_customers_id, $page){
			$arrayOfRowsPulled = array();
			{
				$query = new RunQuery( "SELECT * FROM `med_action_log` WHERE `customer_id`=? ORDER BY `id` DESC LIMIT ".(($page - 1) * 10).", 11", array( &$med_customers_id) );
				if( TitheRequestController::controller()->runCommand( $query ) ){
					while( $currRow = $query->fetchRow( QueryResultType::ASSOCIATIVE_ARRAY ) ){
						$arrayOfRowsPulled[$currRow['id']] = $currRow;
					}
				}
			}
			
			return $arrayOfRowsPulled;
		}
	}
