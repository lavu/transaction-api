
	<style type='text/css'>

		.td_gift_info_label {
			color:#EEEEEE;
			font-family:Arial, Helvetica, sans-serif;
			font-size:25px;
			text-shadow: 1px 1px 0px rgba(0, 0, 0, 0.5);
		}
		
		.td_gift_info {
			color:#FFFFFF;
			font-family:Arial, Helvetica, sans-serif;
			font-size:26px;
			text-shadow: 1px 1px 0px rgba(0, 0, 0, 0.5);
		}

		.td_gift_info_sm {
			color:#FFFFFF;
			font-family:Arial, Helvetica, sans-serif;
			font-size:18px;
			text-shadow: 1px 1px 0px rgba(0, 0, 0, 0.5);
		}
	
	</style>

	<script type="text/javascript">
	
		var last_order_id = "";
	
		function prepareTitheSwipe() {
		
			if (window.Member) document.getElementById("swipe_giver_name").innerHTML = Member.f_name + " " + Member.l_name;
			if (window.User_Dynamic_Data) {
				document.getElementById("swipe_ministry").innerHTML = User_Dynamic_Data.ministry_name;
				document.getElementById("swipe_amount").innerHTML = "$" + displayMoney(User_Dynamic_Data.tithe_amount);
				document.getElementById("swipe_frequency").innerHTML = User_Dynamic_Data.tithe_frequency;
			}
			
			startListening();
			
			startTimeoutTimer(60000);
		}
	
		function startListening() {
		
			window.location = '_DO:cmd=startListeningForSwipe';
		}
		
		function stopListening() {
		
			window.location = '_DO:cmd=stopListeningForSwipe';
		}
		
		String.prototype.paddingLeft = function (paddingValue) {
			return String(paddingValue + this).slice(-paddingValue.length);
		};
		
		function getCurrentDateTime() {
		
			var d = new Date();
			
			var year = d.getFullYear();
			var month = (d.getMonth() + 1);
			var day = d.getDate();
			var hour = d.getHours();
			var minute = d.getMinutes();
			var second = d.getSeconds();
			
			month = month.toString().paddingLeft("00");
			day = day.toString().paddingLeft("00");
			hour = hour.toString().paddingLeft("00");
			minute = minute.toString().paddingLeft("00");
			second = second.toString().paddingLeft("00");
		
			return year + "-" + month + "-" + day + " " + hour + ":" + minute + ":" + second;
		}
		
		(function(){
			function ProcessGatewayAJAXRequest( params ){
				AJAXRequest.call( this, '../../lib/gateway.php', 'POST', params, true );
			}

			function success( response ){
			
				var result = response.split("|");
				
				if (result[0] == "1") {
												
					doCommands = "_DO:cmd=hideActivityShade";
					
					var arbStatus = tryParseJSON(result[(result.length - 1)]);
					if (arbStatus) {
						if (typeof arbStatus.addon_info != 'undefined') {
							if (arbStatus.addon_info.status == 'error') {
								var title = "Recurring Setup Error";
								var message = "Your initial gift transaction was approved, but an error occurred while trying to schedule future transactions: " + arbStatus.addon_info.message;
								doCommands += "_DO:cmd=alert&title=" + encodeURIComponent(title) + "&message=" + encodeURIComponent(message);
							}
						}
					}
						
					window.setTimeout(function(){executeDoCommands(doCommands)}, 300);				
					
					last_order_id = result[8];
					showPage("tithe_thank_you");
	
				} else {

					var title = (result.length > 5)?result[5]:"Declined";
					var message = (result.length > 1)?result[1]:"";
					
					if (result.length > 3) User_Dynamic_Data.order_id = result[3];
				
					componentAlert(title, message);
					window.setTimeout(function(){resetForCardInput()}, 200);
				}
			}

			function failure( status, response ){

				componentAlert("Connection Failed", "An error occurred while trying to process transaction. Please try again.");
				window.setTimeout(function(){resetForCardInput()}, 200);
			}

			window.ProcessGatewayAJAXRequest = ProcessGatewayAJAXRequest;
			ProcessGatewayAJAXRequest.prototype.constructor = ProcessGatewayAJAXRequest;
			ProcessGatewayAJAXRequest.prototype = Object.create(AJAXRequest.prototype);
			ProcessGatewayAJAXRequest.prototype.success = success;
			ProcessGatewayAJAXRequest.prototype.failure = failure;
		})();
		
		function handleCardSwipe( swipeString, hexString, reader, encrypted ){
						
			showAppActivityShade();

			window.setTimeout(function(){processTransaction(swipeString, hexString, reader, encrypted, "", "", "")}, 50);
			
			return 1;
		}
		
		function processTransaction(swipeString, hexString, reader, encrypted, card_number, card_expiration, cvn) {
		
			var mag_data = (encrypted == "1")?hexString:swipeString;
		
			var postVars = {};
			postVars.UUID = window.AppData.UUID;
			postVars.MAC = window.AppData.MAC;
			postVars.app = window.AppData.app_name;
			postVars.version = window.AppData.version;
			postVars.build = window.AppData.build;
			postVars.server_id = window.AppData.server_id;
			postVars.server_name = window.AppData.server_name;
			postVars.register = window.AppData.register;
			postVars.register_name = window.AppData.register_name;
			postVars.cc = window.AppData.cc;
			postVars.data_name = window.AppData.dn;
			postVars.loc_id = window.AppData.loc_id;
			postVars.order_id = User_Dynamic_Data.order_id;
			postVars.check = 1;
			if (mag_data == "") {
				postVars.card_number = card_number;
				postVars.card_expiration = card_expiration;
				postVars.CVN = cvn;
			} else {			
				postVars.reader = reader;
				postVars.encrypted = encrypted;
				postVars.mag_data = (encrypted == "1")?hexString:swipeString;
			}
			postVars.card_amount = displayMoney(User_Dynamic_Data.tithe_amount);
			postVars.transtype = "Sale";
			postVars.device_time = getCurrentDateTime();
			postVars.pay_type = "Card";
			postVars.pay_type_id = 2;
			postVars.for_deposit = 0;
			postVars.is_deposit = 0;
			postVars.ministry_id = User_Dynamic_Data.ministry_id;
			postVars.ministry_name = User_Dynamic_Data.ministry_name;
			postVars.frequency = User_Dynamic_Data.tithe_frequency;
			postVars.member_id = Member.id;
			postVars.member_full_name = Member.f_name + ' ' + Member.l_name;
					
			new ProcessGatewayAJAXRequest( postVars );
		}
		
		function shouldListenForSwipe() {
		
			return (document.getElementById("tithe_swipe").style.display == "block")?1:0;
		}
	
		function resetForCardInput() {
		
			var cmdString = "_DO:cmd=hideActivityShade";
			
			if (shouldListenForSwipe() == 1) {
				// reset swiper
				cmdString += "_DO:cmd=stopListeningForSwipe_DO:cmd=startListeningForSwipe";
			}
			
			window.location = cmdString;
		}
		
		function executeDoCommands(doCommands) {
		
			window.location = doCommands;
			
			window.setTimeout(function(){hideAppActivityShade()}, 250);
		}
								
	</script>
		
	<div style='position:absolute; left:50px; top:25px; width:250px; height:25px; vertical-align:middle; display:block;' ontouchstart='showPage(go_back_from_swipe);'>
		<img src='images/arrow_back.png' width='12px' height='20px' /> &nbsp;
		<span class='back_button_label'>GO BACK</span>
	</div>

	<div style='position:relative; left:412px; top:25px; width:580px; height:25px; text-align:right; vertical-align:middle; display:block;' ontouchstart='showPage("tithe_welcome");'>
		<span class='back_button_label'>LOG OUT</span> &nbsp;
		<img src='images/icon_x.png' width='19px' height='19px'>
	</div>	
	
	<center>
		<span class='page_title' style='position:absolute; left:0px; top:70px; width:1024px; height:70px;'><?php echo $location_name; ?></span>
	</center>
	
	<center>
		<span style='position:absolute; left:0px; top:210px; width:930px; height:50px; color:#FFFFFF; font-size:48px; text-shadow: 1px 1px 0px rgba(0, 0, 0, 0.5)'>Please swipe your card</span>
	</center>
	
	<img style='position:absolute; left:735px; top:205px; width:78px; height:62px;' src='images/icon_card_swipe.png'>
	
	<div style='position:absolute; left:212px; top:320px; width:600px; height:240px; background-image:url("images/bg_panel_600x240.png")'>
		<center>
			<table width='600px' height='240px'>
				<tbody>
					<tr>
						<td align='center' valign='middle'>
							<table>
								<tbody>
									<tr>
										<td align='center'>
											<table cellspacing='6' cellpadding='0'>
												<tbody>
													<tr>
														<td class='td_gift_info_label' align='right'>Giver: &nbsp;</td>
														<td id='swipe_giver_name' class='td_gift_info' align='left'></td>
													</tr>
													<tr>
														<td class='td_gift_info_label' align='right'>Ministry: &nbsp;</td>
														<td id='swipe_ministry' class='td_gift_info'  align='left'></td>
													</tr>
													<tr>
														<td class='td_gift_info_label' align='right'>Amount: &nbsp;</td>
														<td id='swipe_amount' class='td_gift_info'  align='left'></td>
													</tr>
													<tr>
														<td class='td_gift_info_label' align='right'>Frequency: &nbsp;</td>
														<td id='swipe_frequency' class='td_gift_info'  align='left'></td>
													</tr>
												</tbody>
											</table>
										</td>
									</tr>
									<tr>
										<td class='td_gift_info_sm' align='center' style='padding-top:14px;'>Press 'GO BACK' to make changes to your gift</td>
									</tr>
								</tbody>
							</table>
						</td>
					</tr>
				</tbody>
			</table>
		</center>
	</div>
	
	<div style='position:absolute; left:412px; top:630px; width:580px; height:25px; text-align:right; vertical-align:middle; display:block;' ontouchstart='showPage("tithe_keyin");'>
		<span class='back_button_label'>KEY IN CARD INFO</span>
	</div>