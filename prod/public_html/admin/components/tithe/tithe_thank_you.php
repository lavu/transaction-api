
	<script language='javascript'>
	
		var email_member_id = "";
		var email_email = "";
		var email_success_alert = false;
	
		function prepareTitheThankYou() {
		
			email_member_id = Member.id;
			email_success_alert = false;
			if (simpleEmailValidation(Member.user_name)) {
				email_email = Member.user_name;
				emailReceipt(email_member_id, last_order_id, email_email);
			}	else componentConfirm("Would you like a receipt emailed to you?", "", "askForEmailAddress()", "startTimeoutTimer(15000)");
		}
		
		function askForEmailAddress() {
		
			componentUserInput("Please enter your email address:", "email", "checkEnteredEmailAddress(\"[ENTERED_STRING]\")");
		}
		
		function checkEnteredEmailAddress(entered_email) {
		
			if (entered_email == "") componentConfirm("No Email Address Entered", "Would you like to try again?", "askForEmailAddress()", "startTimeoutTimer(15000)");
			else if (simpleEmailValidation(entered_email)) {
				email_success_alert = true;
				email_email = entered_email;
				emailReceipt(email_member_id, last_order_id, email_email);
			} else componentConfirm("Invalid Email Address", "Would you like to try again?", "askForEmailAddress()", "startTimeoutTimer(15000)");
			
			return 1;
		}
		
		(function(){
			function emailReceiptAJAXRequest( params ){
				AJAXRequest.call( this, 'lib/email_gift_receipt.php', 'POST', params, true );
			}

			function success( response ){
			
				if (email_success_alert) componentAlert("Email Successfully Sent", "");
				startTimeoutTimer(15000);
			}

			function failure( status, response ){

				componentConfirm("Connection Failed", "An error occurred while trying to email receipt. Do you want to try again?", "retryEmailReceipt()", "startTimeoutTimer(15000)");
			}

			window.emailReceiptAJAXRequest = emailReceiptAJAXRequest;
			emailReceiptAJAXRequest.prototype.constructor = emailReceiptAJAXRequest;
			emailReceiptAJAXRequest.prototype = Object.create(AJAXRequest.prototype);
			emailReceiptAJAXRequest.prototype.success = success;
			emailReceiptAJAXRequest.prototype.failure = failure;
		})();
		
		function emailReceipt(member_id, order_id, email) {
				
			var postVars = {};
			postVars.UUID = window.AppData.UUID;
			postVars.MAC = window.AppData.MAC;
			postVars.app = window.AppData.app_name;
			postVars.version = window.AppData.version;
			postVars.build = window.AppData.build;
			postVars.server_id = window.AppData.server_id;
			postVars.server_name = window.AppData.server_name;
			postVars.register = window.AppData.register;
			postVars.register_name = window.AppData.register_name;
			postVars.cc = window.AppData.cc;
			postVars.data_name = window.AppData.dn;
			postVars.loc_id = window.AppData.loc_id;
			postVars.member_id = member_id;
			postVars.order_id = order_id;
			postVars.email = email;			
					
			new emailReceiptAJAXRequest( postVars );
		}
		
		function retryEmailReceipt() {
		
			email_success_alert = true;
			emailReceipt(email_member_id, last_order_id, email_email);
		}
	
	</script>

	<center><span class='page_title' style='position:absolute; left:0px; top:70px; width:1024px; height:70px;'><?php echo $location_name; ?></span></center>
	
	<center><span style='position:absolute; left:0px; top:260px; width:1024px; height:70px; color:#FFFFFF; font-size:61px; text-shadow: 1px 1px 0px rgba(0, 0, 0, 0.5)'>Thank you for your contribution</span></center>
	
	<div style='position:absolute; left:0px; top:400px; width:1024px; height:200px;'>
		<center><input class='submit_keyed_in' type='submit' value='DONE' ontouchstart='showPage("tithe_welcome");'><br><br><br></center>
	</div>