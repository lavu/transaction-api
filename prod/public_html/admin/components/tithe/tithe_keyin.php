
	<style type='text/css'>
	
		.submit_keyed_in {
			color:#FFFFFF;
			font-family:Arial, Helvetica, sans-serif;
			font-size:30px;
			text-shadow: 1px 1px 0px rgba(0, 0, 0, 0.5);
			width:436px;
			height:71px;
			background:url("images/btn_436x71.png");
			border:hidden;
		}
		
		.input_long {
			width:304px;
			height:45px;
			background:url("images/bg_input_field.png");
			background-repeat:no-repeat;
			background-position:center;
			border:hidden;
			text-indent:8px;
			font-family:Arial, Helvetica, sans-serif;
			font-size:20px;
			padding-top:7px;
		}
		
		.input_short {
			width:100px;
			height:45px;
			background:url("images/bg_input_field_sm.png");
			background-repeat:no-repeat;
			background-position:center;
			border:hidden;
			text-indent:8px;
			font-family:Arial, Helvetica, sans-serif;
			font-size:20px;
			padding-top:7px;
		}
		
		.select_exp {
			color:#A9A9A9;
			width:132px;
			height:49px;
			background:url("images/bg_input_field_exp.png");
			background-repeat:no-repeat;
			background-position:center;
			border:hidden;
			text-indent:8px;
			font-family:Arial, Helvetica, sans-serif;
			font-size:20px;
			padding-top:3px;
		}
		
	</style>
	
	<script type="text/javascript">
		
		function prepareTitheKeyIn() {
		
			document.getElementById("input_cn").value = "";
			
			var expM = document.getElementById("input_exp_month");
			expM.value = "";
			expM.style.color = '#A9A9A9';
	
			var expY = document.getElementById("input_exp_year");
			expY.value = "";
			expY.style.color = '#A9A9A9';
		
			document.getElementById("input_cvn").value = "";
			
			startTimeoutTimer(180000);
		}
		
		function openInAppNumberPad(type, max_digits, title, field_id) {
		
			window.location = "_DO:cmd=number_pad&type=" + type +"&max_digits=" + max_digits + "&title=" + encodeURIComponent(title) +"&c_name=tithe&send_cmd=inputNumberFromApp(%27" + field_id + "%27,%27ENTERED_NUMBER%27)";
		}
		
		function inputNumberFromApp(field_id, value) {
		
			document.getElementById(field_id).value = value;
		}
		
		function changeTextColor(field) {
				
			if (field.value == '') field.style.color = '#A9A9A9';
			else field.style.color = '#000000';
		}
		
		function checkKeyedInFields() {
		
			if (document.getElementById('input_cn').value.length < 13) {
				componentAlert("Invalid Card Number", "Please enter a valid card number to proceed.");
			} else if (document.getElementById('input_exp_month').value == '') {
				componentAlert("No Expiration Month", "Please select an expiration month to proceed.");
			} else if (document.getElementById('input_exp_year').value == '') {
				componentAlert("No Expiration Year", "Please select an expiration year to proceed.");
			} else if (document.getElementById('input_cvn').value.length < 3) {
				componentAlert("Invalid CVN", "Please enter a valid card verification number to proceed. You can find it on the back of your card.");
			} else submitKeyedInInfo();
		}
		
		function submitKeyedInInfo() {
		
			showAppActivityShade();
			
			var card_number = document.getElementById("input_cn").value;
			var card_expiration = "" + document.getElementById("input_exp_month").value + document.getElementById("input_exp_year").value;
			var cvn = document.getElementById("input_cvn").value;

			window.setTimeout(function(){processTransaction("", "", "", "", card_number, card_expiration, cvn)}, 10);
		}
		
	</script>
		
	<div style='position:absolute; left:50px; top:25px; width:250px; height:25px; vertical-align:middle; display:block;' ontouchstart='showPage(go_back_from_swipe);'>
		<img src='images/arrow_back.png' width='12px' height='20px'> &nbsp;
		<span class='back_button_label'>GO BACK</span>
	</div>

	<div style='position:relative; left:412px; top:25px; width:580px; height:25px; text-align:right; vertical-align:middle; display:block;' ontouchstart='showPage("tithe_welcome");'>
		<span class='back_button_label'>LOG OUT</span> &nbsp;
		<img src='images/icon_x.png' width='19px' height='19px'>
	</div>	
	
	<center><span class='page_title' style='position:absolute; left:0px; top:70px; width:1024px; height:70px;'><?php echo $location_name; ?></span></center>
	
	<div style='position:absolute; left:0px; top:270px; width:1024px; height:200px;'>
		<center>
			<form onsubmit='return false;'>
			
				<input id='input_cn' class='input_long' type='tel' placeholder='CARD NUMBER' value='' READONLY ontouchstart='openInAppNumberPad("cc_number", "16", "Card Number", "input_cn");'> &nbsp;
				<select id='input_exp_month' class='select_exp' onchange='changeTextColor(this);'>
					<option value=''>MONTH</option>
					<option value='01'>01</option>
					<option value='02'>02</option>
					<option value='03'>03</option>
					<option value='04'>04</option>
					<option value='05'>05</option>
					<option value='06'>06</option>
					<option value='07'>07</option>
					<option value='08'>08</option>
					<option value='09'>09</option>
					<option value='10'>10</option>
					<option value='11'>11</option>
					<option value='12'>12</option>
				</select> &nbsp;
				<select id='input_exp_year' class='select_exp' onchange='changeTextColor(this);'>
					<option value=''>YEAR</option>
					<?php
						$year = date("Y");
						for ($i = 0; $i < 10; $i++) {
							echo "<option value='".substr($year, -2)."'>".$year."</option>";
							$year++;
						}
					?>
				</select> &nbsp;
				<input id='input_cvn' class='input_short' type='tel' placeholder='CVN' value='' READONLY ontouchstart='openInAppNumberPad("multi_digit", "4", "Verification Number", "input_cvn");'>
				<br><br><br>
				<input class='submit_keyed_in' type='button' value='SUBMIT KEYED IN INFO' ontouchstart='checkKeyedInFields();'><br><br><br>
			
			</form>
		</center>
	</div>
		
	<div style='position:absolute; left:412px; top:630px; width:580px; height:25px; text-align:right; vertical-align:middle; display:block;' ontouchstart='showPage("tithe_swipe");'>
		<span class='back_button_label'>SWIPE CARD</span>
	</div>
	
	
<?php
	
/*

.input_med {
			width:130px;
			height:45px;
			background:url("images/bg_input_field_med.png");
			background-repeat:no-repeat;
			background-position:center;
			border:hidden;
			text-indent:8px;
			font-family:Arial, Helvetica, sans-serif;
			font-size:20px;
			padding-top:7px;
		}
		
	<input id='input_exp' class='input_med' type='month' min='<?php echo date("Y-m"); ?>' max='<?php echo date("Y-m", (time() + 315360000)); ?>' placeholder='EXP'> &nbsp;
				
*/
	
?>