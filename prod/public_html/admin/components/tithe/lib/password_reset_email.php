<?php

	session_start();
	
	ini_set('display_errors',0);
	
	require_once(dirname(__FILE__)."/../../../lib/info_inc.php");

	function validEmail($email) {
	
		return preg_match("/^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*$/i", $email);
	}
	
	$status = "error";
	$title = "Connection Authentication Error";
	$message = "";
	$email = $_REQUEST['email'];
	
	if (lsecurity_id($company_code_key) == $company_info['id']) {
	
		//$title = "Password Reset Unavailable";
		//$message = "Please try again later: ".$email;
	
		$check_customer = lavu_query("SELECT `id`, `f_name` FROM `med_customers` WHERE `user_name` = '[1]'", $email);
		if ($check_customer) {
			$c_rows = mysqli_num_rows($check_customer);
			if ($c_rows > 0) {			
				if ($c_rows > 1) {
					$title = "Multiple Accounts";
					$message = "More than one account is associated with this email address. Password must be reset manually.";
				} else {
					$customer_info = mysqli_fetch_assoc($check_customer);
					
					if (validEmail($email)) {
					
						$clear_tokens = lavu_query("DELETE FROM `config` WHERE `location` = '[1]' AND `type` = 'password_reset_token' AND `setting` = 'customer' AND `value` = '[2]'", $location_info['id'], $customer_info['id']);
						$token = generateToken(50);
						$store_token = lavu_query("INSERT INTO `config` (`location`, `type`, `setting`, `value`, `value2`, `value3`) VALUES ('[1]', 'password_reset_token', 'customer', '[2]', '[3]', '".time()."')", $location_info['id'], $customer_info['id'], $token);
						if ($store_token) {						
						
							$uri = "https://admin.poslavu.com/pwr.php?p=lg&d=".lc_encode($data_name)."&t=".$token;
						
							$subject = "Lavu Give Password Reset Request";
								
							$email_body = "<html><head></head><body>";
							$email_body .= "Hello ".$customer_info['f_name'].",<br /><br />";
							$email_body .= "We have received a request to reset the password for your Lavu Give account. To continue, please follow the link below:<br /><br />";
							$email_body .= "<a href='$uri'>$uri</a><br /><br />";
							$email_body .= "If you have received this email in error, or you do not wish to change your password, please disregard this email.<br /><br />";
							$email_body .= "Best Wishes,<br />";
							$email_body .= "Lavu";
							$email_body .= "</body></html>";
						
							$headers = "Content-type: text/html; charset=iso-8859-1"."\r\n";
							$headers .= "From: ".$location_info['title']." Lavu Give <noreply@lavugive.com>"."\r\n";
													
							if ($_SERVER['SERVER_ADDR'] == '192.168.101.224') { // web1 (admin.poslavu.com)
								mail(trim($email), $subject, $email_body, $headers);
							} else {
								$vars = "email_address=$email&subject=".str_replace("&", "[AMPERSAND]", $subject)."&body=".str_replace("&", "[AMPERSAND]", $email_body)."&headers=".str_replace("&", "[AMPERSAND]", $headers);
								$ch = curl_init();
								curl_setopt($ch, CURLOPT_URL, "https://admin.poslavu.com/lib/email_receipt.php");
								curl_setopt($ch, CURLOPT_POST, 1);
								curl_setopt($ch, CURLOPT_POSTFIELDS, $vars);
								curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
								$response = curl_exec($ch);
								curl_close ($ch);
							}
							
							$status = "success";
							$title = "Email Sent";
							$message = "A password reset email has been sent to you at ".$email.".";
							
						} else {						
							$title = "Database Error";
							$message = "Failed to store token.";
						}
					}	else $title = "Invalid Email Address";		
				}
			} else $title = "Account Not Found";
		} else {
			$title = "Database Error";
			$message = "Unable to locate your account. Please try again.";
		}
	}
	
	echo '{"status":"'.$status.'","title":"'.$title.'","message":"'.$message.'"}';
	lavu_exit();

?>