<?php

	session_start();
	
	ini_set('display_errors',0);
	
	require_once(dirname(__FILE__)."/../../../lib/info_inc.php");

	$status = "error";
	$title = "Connection Authentication Error";
	$message = "";
	$page = $_REQUEST['page'];
	$log_lines = "{}";
	
	if (lsecurity_id($company_code_key) == $company_info['id']) {
	
		$get_action_log = lavu_query("SELECT * FROM `med_action_log` WHERE `customer_id` = '[1]' ORDER BY `id` DESC LIMIT ".(($page - 1) * 10).", 11", $_REQUEST['member_id']);
		if ($get_action_log) {
			if (mysqli_num_rows($get_action_log) > 0) {
				$status = "success";
				$title = "";
				$message = "";
				$rows = array();
				while ($info = mysqli_fetch_assoc($get_action_log)) {
					$rows[$info['id']] = $info;
				}
				$log_lines = json_encode($rows);
			} else $title = "No more records found.";
		} else {
			$title = "Load Failed";
			$message = "An error occurred while trying to load account history. Please try again.";
		}
	}
	
	echo '{"status":"'.$status.'","title":"'.$title.'","message":"'.$message.'","page":"'.$page.'","log_lines":'.$log_lines.'}';
	lavu_exit();

?>