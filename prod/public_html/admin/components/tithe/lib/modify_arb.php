<?php

	session_start();
	
	ini_set('display_errors',0);
	
	require_once(dirname(__FILE__)."/../../../lib/info_inc.php");

	$status = "error";
	$title = "Connection Authentication Error";
	$message = "";
	
	if (lsecurity_id($company_code_key) == $company_info['id']) {
	
		$action = $_REQUEST['action'];
		$arb_id = $_REQUEST['arb_id'];
	
		if ($action == "cancel") {
		
			$success = false;
			$get_arb = lavu_query("SELECT `order_id`, `status`, `recurring_amount`, `name`, `bill_frequency` FROM `med_billing` WHERE `id` = '[1]'", $arb_id);
			if ($get_arb) {
				$success = true;
				if (mysqli_num_rows($get_arb) > 0) {
					$arb_info = mysqli_fetch_assoc($get_arb);
					if ($arb_info['status'] == "active") {
						$update_arb = lavu_query("UPDATE `med_billing` SET `status` = 'cancelled' WHERE `id` = '[1]'", $arb_id);
						if ($update_arb) {
							$status = "success";
							$title = "Cancellation Successful";
							$message = "";
							$name_parts = explode(" ", $arb_info['name']);
							$ministry = getFieldInfo($name_parts[1], "id", "menu_items", "name");
							$action = "Cancelled Recurring Offering: $".number_format((float)$arb_info['recurring_amount'], $location_info['disable_decimal'], ".", "")." ".$arb_info['bill_frequency']." to ".$ministry;
							actionLogIt($action, $_REQUEST['member_id'], $arb_info['order_id']);
						} else $success = false;
					} else {
						$title = "Already Inactive";
						$message = "Your recurring gift is already marked as ".$arb_info['status'].".";
					}				
				} else {
					$title = "Record Not Found";
					$message = "Unable to locate the database record for your recurring gift. (ID: ".$arb_id.")";
				}
			}
			
			if (!$success) {
				$title = "Cancellation Failed";
				$message = "An error occurred while trying to cancel your recurring offering. (ID: ".$arb_id.")";
			}
		
		} else {
		
			$title = "Unrecognized Command";
			$message = $action;
		}
		
	}
	
	echo '{"status":"'.$status.'","title":"'.$title.'","message":"'.$message.'"}';
	lavu_exit();

	function actionLogIt($action, $customer_id, $order_id) {
	
		$q_fields = "";
		$q_values = "";
		$vars = array();
		$vars['action'] = $action;
		$vars['loc_id'] = $_REQUEST['loc_id'];
		$vars['order_id'] = $order_id;
		$vars['customer_id'] = $customer_id;
		$vars['restaurant_time'] = $_REQUEST['device_time'];
		$vars['device_time'] = $_REQUEST['device_time'];
		$keys = array_keys($vars);
		foreach ($keys as $key) {
			if ($q_fields != "") { $q_fields .= ", "; $q_values .= ", "; }
			$q_fields .= "`$key`";
			$q_values .= "'[$key]'";
		}
		$log_this = lavu_query("INSERT INTO `med_action_log` ($q_fields) VALUES ($q_values)", $vars);
	}

?>