<?php

require_once( dirname(__FILE__)."/../../fitness/RecurringBilling.php" );

class LavuGiveArb extends RecurringBilling
{
	var $test = false;
	var $debug = true;

	// Overrides

	function runCreditCardTxn( $orderId, $invoice, $billingProfile, &$ccTxnResults ) {

		$success = parent::runCreditCardTxn( $orderId, $invoice, $billingProfile, $ccTxnResults );

		if ($success) {

			$customer_id = $billingProfile['customer_id'];
			$data_name = $this->dataname;
			$loc_id = $billingProfile['location_id'];
			$local_datetime = $this->localDateTime($data_name, $loc_id);

			$q_fields = "";
			$q_values = "";
			$vars = array();
			$vars['action'] = "$".number_format((float)$billingProfile['recurring_amount'], 2, ".", "")." recurring offering made to ".$billingProfile['name'];
			$vars['loc_id'] = $loc_id;
			$vars['order_id'] = $orderId;
			$vars['customer_id'] = $customer_id;
			$vars['restaurant_time'] = $local_datetime;
			$vars['device_time'] = $local_datetime;
			$keys = array_keys($vars);
			foreach ($keys as $key) {
				if ($q_fields != "") { $q_fields .= ", "; $q_values .= ", "; }
				$q_fields .= "`$key`";
				$q_values .= "'[$key]'";
			}
			$log_this = lavu_query("INSERT INTO `med_action_log` ($q_fields) VALUES ($q_values)", $vars);

			$check_customer_email = lavu_query("SELECT `email` FROM `med_customers` WHERE `id` = '[1]'", $customer_id);
			if ($check_customer_email === FALSE) die(__CLASS__ . " - check_customer_email query failed: ".lavu_dberror());
			if (mysqli_num_rows($check_customer_email) > 0) {
				$info = mysqli_fetch_assoc($check_customer_email);
				if (!empty($info['email'])) {
				
					$tx_info = array();
					$tx_info[] = $billingProfile['cc_card_type'];
					$tx_info[] = $billingProfile['cc_num_last4'];
					$tx_info[] = ""; // cc_transactions.info or name on card
					$tx_info[] = $ccTxnResults->refnum;
					$tx_info[] = $ccTxnResults->authcode;
					$tx_info[] = ""; // cc_transactions.record_no

					$vars = array();
					$vars['cc'] = $data_name."_key_".lsecurity_key_by_dn($data_name);
					$vars['dn'] = $data_name;
					$vars['loc_id'] = $loc_id;
					$vars['order_id'] = $orderId;
					$vars['member_id'] = $customer_id;
					$vars['email'] = $info['email'];
					$vars['tx_info'] = implode("|o|", $tx_info);

					$varstring = "";
					foreach ($vars as $key => $val) {
						if (!empty($varstring)) $varstring .= "&";
						$varstring .= $key."=".rawurlencode($val);
					}

					error_log("LavuGiveArb varstring: ".$varstring);  //debug

					$ch = curl_init();
					$curl_url = ($this->test) ? "https://admin.poslavu.com/dev/components/tithe/lib/email_gift_receipt.php" : "https://admin.poslavu.com/components/tithe/lib/email_gift_receipt.php";
					error_log( "LavuGiveArb curl_url={$curl_url}");  //debug
					curl_setopt($ch, CURLOPT_URL, $curl_url);
					curl_setopt($ch, CURLOPT_POST, 1);
					curl_setopt($ch, CURLOPT_POSTFIELDS, $varstring);
					curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
					$response = curl_exec($ch);
					error_log( print_r($response, 1) );  //debug
					curl_close ($ch);

				}
			}

		} else {

			// log decline in action log?
		}

		return $success;
	}
}

?>

