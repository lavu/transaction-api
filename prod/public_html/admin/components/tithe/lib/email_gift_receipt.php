<?php

	session_start();
	
	ini_set('display_errors',0);
	
	require_once(dirname(__FILE__)."/../../../lib/info_inc.php");
	
	function validEmail($email) {
	
		return preg_match("/^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*$/i", $email);
	}

	function txInfoString($info, &$tx_str) {
	
		if ($tx_str != "") $tx_str .= "\n\n";
		$tx_str .= $info['card_type']." ".$info['card_desc']."\n";
		if ($info['info'] != "") $tx_str .= $info['info']."\n";
		$tx_str .= "Reference #: ".$info['transaction_id']."\nAuth Code: ".$info['auth_code'];
		if ($info['record_no'] != "") $tx_str .= "\nRecord #: ".$info['record_no'];
	}
	
	$status = "error";
	$title = "Connection Authentication Error";
	$message = "";
	
	if ((lsecurity_id($company_code_key) == $company_info['id']) && !empty($company_info['id'])) {
	
		$customer_id = $_REQUEST['member_id'];
		$loc_id = $_REQUEST['loc_id'];
		$order_id = $_REQUEST['order_id'];
		$email = $_REQUEST['email'];
		
		if (validEmail($email)) {
	
			$customer_name = "";
			$get_customer_info = lavu_query("SELECT `f_name`, `email` FROM `med_customers` WHERE `id` = '[1]'", $customer_id);
			if (mysqli_num_rows($get_customer_info) > 0) {
				$customer_info = mysqli_fetch_assoc($get_customer_info);
				if ($customer_info['f_name'] != "") $customer_name = " ".$customer_info['f_name'];
				if ($customer_info['email'] == "") $update_customer = lavu_query("UPDATE `med_customers` SET `email` = '[1]' WHERE `id` = '[2]'", $email, $customer_id);
			}

			$get_order_contents = lavu_query("SELECT * FROM `order_contents` WHERE `loc_id` = '[1]' AND `order_id` = '[2]'", $loc_id, $order_id);
			if (mysqli_num_rows($get_order_contents) > 0) {
			
				$total = 0;
				$content_str = "";
				while ($info = mysqli_fetch_assoc($get_order_contents)) {
					if ($content_str != "") $content_str .= "\n";
					$total += $info['price'];
					$content_str .= "$".number_format((float)$info['price'], $location_info['disable_decimal'], ".", ",")." to ".$info['item'];
				}
				
				$tx_str = "";						
				if (!empty($_REQUEST['tx_info'])) {
				
					$txs_info = explode("|*|", $_REQUEST['tx_info']);
					foreach ($txs_info as $i1) {
						$i2 = explode("|o|", $i1);
						$info = array("card_type"=>$i2[0], "card_desc"=>$i2[1], "info"=>$i2[2], "transaction_id"=>$i2[3], "auth_code"=>$i2[4], "record_no"=>$i2[5]);
						txInfoString($info, $tx_str);
					}
							
				} else {
				
					$get_transactions = lavu_query("SELECT `card_type`, `card_desc`, `info`, `transaction_id`, `auth_code`, `record_no` FROM `cc_transactions` WHERE `loc_id` = '[1]' AND `order_id` = '[2]'", $loc_id, $order_id);
					if (mysqli_num_rows($get_transactions) > 0) {
						while ($info = mysqli_fetch_assoc($get_transactions)) {
							txInfoString($info, $tx_str);
						}						
					}
				}
				
				if (!empty($tx_str)) {
			
					$subject = "Your Offering Receipt";
					
					$email_body = "Hello".$customer_name.",\n\n";
					$email_body .= "Thank you so very much for your generous offering of $".number_format($total, $location_info['disable_decimal'], ".", ",").". This email confirms your offering as a donation to ".$location_info['title'].", as detailed below.\n\n";
					$email_body .= $content_str."\n".$tx_str."\n\n";
					$email_body .= "Have a great day and God Bless!\n\n";
					$email_body .= $location_info['manager']."\n";
					$email_body .= $location_info['title']."\n";
					$email_body .= $location_info['address']."\n";
					$email_body .= $location_info['city'].", ".$location_info['state']." ".$location_info['zip']."\n";
					$email_body .= $location_info['phone']."\n";
					
					$headers = "Content-type: text/plain; charset=iso-8859-1"."\r\n";
					$headers .= "From: ".$location_info['title']." Lavu Give <noreply@lavugive.com>"."\r\n";
											
					if ($_SERVER['SERVER_ADDR'] == '192.168.101.224') { // web1 (admin.poslavu.com)
						mail(trim($email), $subject, $email_body, $headers);
					} else {
						$vars = "email_address=$email&subject=".str_replace("&", "[AMPERSAND]", $subject)."&body=".str_replace("&", "[AMPERSAND]", $email_body)."&headers=".str_replace("&", "[AMPERSAND]", $headers);
						$ch = curl_init();
						curl_setopt($ch, CURLOPT_URL, "https://admin.poslavu.com/lib/email_receipt.php");
						curl_setopt($ch, CURLOPT_POST, 1);
						curl_setopt($ch, CURLOPT_POSTFIELDS, $vars);
						curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
						$response = curl_exec($ch);
						curl_close ($ch);
					}
										
					$update_order = lavu_query("UPDATE `orders` SET `email` = '[1]' WHERE `location_id` = '[2]' AND `order_id` = '[3]'", $email, $loc_id, $order_id);
					
					$status = "success";
					$title = "";
				
				} else $title = "Missing Transaction Record";
		
			} else $title = "Missing Order Contents";
			
		} else $title = "Invalid Email Address";
	}
	
	echo '{"status":"'.$status.'","title":"'.$title.'","message":"'.$message.'"}';
	lavu_exit();

?>