<?php
	
	ini_set('display_errors', '1');
	
	session_start();

	require_once(dirname(__FILE__).'/app_data.php');
	
	global $dataname;
	$dataname = persistantReqVar("cc");
	require_once(dirname(__FILE__).'/../../lib/info_inc.php');
	$dataname = explode('_key_', $dataname);
	$dataname = $dataname[0];
	$security_hash = sha1($dataname . 'garbonzo');
	
	if ( !empty( $_REQUEST['is_ajax'] ) && !empty( $_REQUEST['cmd'] ) ) {
		ob_start();
	}
	
	//echo "<script type='text/javascript'> var security_hash = '$security_hash'/script>";
	//echo '<pre style="text-align: left;">' . print_r( $_SESSION, true ) . '</pre>';
	require_once(dirname(__FILE__).'/tithe_data.php');
	$getDataAjaxAndSetCodeJS = getGetDataJSAjaxAutoCallsAndFillsObjects();
	// echo "<script type='text/javascript'>" . $getDataAjaxAndSetCodeJS . "</script>";
	$location_name = $location_info['title'];
	
?><!DOCTYPE html>
<html>
	<head>	
		<style type='text/css'>
			
			body {
				font-family:sans-serif;
				background-color: transparent;
				margin: 0 0 0 0;
				padding: 0 0 0 0;
				-webkit-tap-highlight-color:rgba(0, 0, 0, 0);
				-webkit-user-select: none;
			}
			
			body:after {
				content:
					url("images/btn_welcome.png")
					url("images/btn_656x120.png")
					url("images/btn_656x120_hl.png")
					url("images/btn_200x40.png")
					url("images/btn_200x40_hl.png")
					url("images/bg_panel_600x240.png")
					url("images/bg_panel_600x400.png")
					url("images/bg_panel_952x473.png")
					url("images/btn_320x49.png")
					url("images/btn_manage_tab.png")
					url("images/btn_manage_tab_hl.png")
					url("images/icon_edit.png")
					url("images/btn_150x30.png")
					url("images/bg_mng_panel_activity_shade.png")
					url("images/btn_656x130.png")
					url("images/btn_451x200.png")
					url("images/btn_451x135.png")
					url("images/btn_451x98.png")
					url("images/btn_451x77.png")
					url("images/btn_200x100.png")
					url("images/btn_500x100.png")
					url("images/checkbox_off.png")
					url("images/checkbox_on.png")
					url("images/bg_panel_851x75.png")			
					url("images/icon_card_swipe.png")
					url("images/bg_input_field.png")
					url("images/bg_input_field_exp.png")
					url("images/bg_input_field_sm.png")
					url("images/btn_436x71.png");
				display: none;
			}
			
			.container_div {
				position:absolute;
				left:0px;
				top:0px;
				width:1024px;
				height:706px;
			}
			
			.page_title {
				color:#FFFFFF;
				font-family:Arial, Helvetica, sans-serif;
				font-size:54px;
				text-shadow: 1px 1px 0px rgba(0, 0, 0, 0.7);
			}

			.page_subtitle {
				color:#FFFFFF;
				font-family:Arial, Helvetica, sans-serif;
				font-size:40px;
				text-shadow: 1px 1px 0px rgba(0, 0, 0, 0.7);
			}
			
			.back_button_label {
				color:#FFFFFF;
				font-size:24px;
			}
			
		</style>
		
		<script language='javascript'>
			<?php
				echo $getDataAjaxAndSetCodeJS;
			?>
			var security_hash = '<?php echo $security_hash; ?>';		
			var timeouts = [];
			
			window.AppData = { <?php echo $appData; ?> };
			
			var decimal_places = '<?php echo $location_info['disable_decimal']; ?>';
			var go_back_from_ministries = "tithe_welcome";
			var go_to_from_amounts = "tithe_frequency";
			var go_back_from_swipe = "tithe_frequency";
			
			function hardReload() {
			
				window.location = "_DO:cmd=load_url&c_name=tithe&f_name=tithe/tithe.php";
			}
		
			function preloadImage(url) {
    		
				var img = new Image();
    		img.src = url;
			}
		
			function showPage(page) {
			
				var pages = ["tithe_welcome", "tithe_login", "tithe_create_account", "tithe_manage_account", "tithe_ministries", "tithe_amounts", "tithe_frequency", "tithe_swipe", "tithe_keyin", "tithe_thank_you"];
				
				var hiddenPage = "";
				var shownPage = "";
				
				for (var i = 0; i < pages.length; i++) {
					var pid = pages[i];
					var displayIs = document.getElementById(pid).style.display;
					var displayTo = (page == pid)?"block":"none";
					if (displayIs != displayTo) {					
						document.getElementById(pid).style.display = (page == pid)?"block":"none";
						if (displayTo == "block") shownPage = pid;
						if (displayTo == "none") hiddenPage = pid;
					}
				}
				
				if (hiddenPage != "") doOnHide(hiddenPage);
				if (shownPage != "") doOnShow(shownPage);
			}
			
			function doOnShow(pid) {
			
				switch (pid) {
					case "tithe_welcome": prepareTitheWelcome(); break;
					case "tithe_login": prepareTitheLogin(); break;
					case "tithe_create_account": prepareTitheCreateAccount(); break;
					case "tithe_manage_account": prepareTitheManageAccount(); break;
					case "tithe_ministries": prepareTitheMinistries(); break;
					case "tithe_amounts": prepareTitheAmounts(); break;
					case "tithe_frequency": prepareTitheFrequency(); break;
					case "tithe_swipe": prepareTitheSwipe(); break;
					case "tithe_keyin": prepareTitheKeyIn(); break;
					case "tithe_thank_you": prepareTitheThankYou(); break;
					default: break;
				}
			}
			
			function doOnHide(pid) {

				if (pid == "tithe_create_account") go_back_from_ministries = "tithe_welcome";
				if (pid == "tithe_swipe") stopListening();
				
				clearTimeouts();
			}
			
			function componentAlert(title, message) {
			
				window.location = "_DO:cmd=alert&title=" + encodeURIComponent(title) + "&message=" + encodeURIComponent(message);
			}

			function componentConfirm(title, message, callback, callback_on_no) {
			
				window.location = "_DO:cmd=confirm&title=" + encodeURIComponent(title) + "&message=" + encodeURIComponent(message) + "&c_name=tithe&js_cmd=" + encodeURIComponent(callback) + "&no_js=" + encodeURIComponent(callback_on_no);
			}
			
			function componentActionSheet(title, choices, callback) {

				window.location = "_DO:cmd=actionSheet&title=" + encodeURIComponent(title) + "&choices=" + encodeURIComponent(choices) + "&c_name=tithe&js=" + encodeURIComponent(callback);			
			}
			
			function componentUserInput(title, type, callback) {
			
				window.location = "_DO:cmd=userInputAlert&title=" + encodeURIComponent(title) + "&type=" + type + "&c_name=tithe&js=" + encodeURIComponent(callback);
			}
			
			function showAppActivityShade() {
				
				window.location = "_DO:cmd=showActivityShade";
			}

			function hideAppActivityShade() {
				
				window.location = "_DO:cmd=hideActivityShade";
			}
			
			function startTimeoutTimer(ms) {
			
				timeouts.push(setTimeout(function(){showPage("tithe_welcome")}, ms));
			}
			
			function clearTimeouts() {
			
				for (i = 0; i < timeouts.length; i++) {
					clearTimeout(timeouts[i]);
				}
				timeouts = [];
			}
		
			<?php require_once(dirname(__FILE__).'/views/script.js.php'); ?>

			// alert( window.navigator.userAgent );
		
		</script>
		
	</head>
	<body>

		<div style='position:absolute; left:-30px; top:0px; width:1084px; height:150px; background-color:#000000; opacity:0.5;'></div>

		<div id='tithe_welcome' class='container_div' style='display:block'>
			<?php require_once dirname(__FILE__).'/tithe_welcome.php'; ?>
		</div>

		<div id='tithe_login' class='container_div' style='display:none'>
			<?php require_once dirname(__FILE__).'/tithe_login.php'; ?>
		</div>

		<div id='tithe_create_account' class='container_div' style='display:none'>
			<?php require_once dirname(__FILE__).'/tithe_create_account.php'; ?>
		</div>

		<div id='tithe_manage_account' class='container_div' style='display:none'>
			<?php require_once dirname(__FILE__).'/tithe_manage_account.php'; ?>
		</div>

		<div id='tithe_ministries' class='container_div' style='display:none'>
			<?php require_once dirname(__FILE__).'/tithe_ministries.php'; ?>
		</div>
	
		<div id='tithe_amounts' class='container_div' style='display:none'>
			<?php require_once dirname(__FILE__).'/tithe_amounts.php'; ?>
		</div>

		<div id='tithe_frequency' class='container_div' style='display:none'>
			<?php require_once dirname(__FILE__).'/tithe_frequency.php'; ?>
		</div>
	
		<div id='tithe_swipe' class='container_div' style='display:none'>
			<?php require_once dirname(__FILE__).'/tithe_swipe.php'; ?>
		</div>

		<div id='tithe_keyin' class='container_div' style='display:none'>
			<?php require_once dirname(__FILE__).'/tithe_keyin.php'; ?>
		</div>

		<div id='tithe_thank_you' class='container_div' style='display:none'>
			<?php require_once dirname(__FILE__).'/tithe_thank_you.php'; ?>
		</div>

		<?php if (isset($_REQUEST['debug'])) { ?>
			<div style='position:absolute; left:10px; top:665px; width:1024px; height:25px;'>
				 <button ontouchstart=showPage("tithe_welcome");> Welcome </button>
				 <button ontouchstart=showPage("tithe_login");> Login </button>
				 <button ontouchstart=showPage("tithe_create_account");> Create Account </button>
				 <button ontouchstart=showPage("tithe_ministries");> Ministries </button>
				 <button ontouchstart=showPage("tithe_amounts");> Amounts </button>
				 <button ontouchstart=showPage("tithe_frequency");> Frequency </button>
				 <button ontouchstart=showPage("tithe_swipe");> Swipe </button>
				 <button ontouchstart=showPage("tithe_keyin");> Key In </button>
				 <button ontouchstart=showPage("tithe_thank_you");> Thank You </button>
				 <button ontouchstart=hardReload();>Reload</button>
			</div>
		<?php } ?>

	</body>
</html>

<?php
	
	/*echo <<<
	HTML
<!DOCTYPE html>
<html>
	<head>
		<style type='text/css'>
			body {
				background-color: transparent;
				margin: 0 0 0 0;
				padding: 0 0 0 0;
				-webkit-tap-highlight-color:rgba(0,0,0,0);
			}		
		</style>
	</head>
	<body>
		<script type="text/javascript">
			
			//var security_hash = '{$security_hash}';
			
			text
		</script>	
	</body>
</html>
HTML;*/
