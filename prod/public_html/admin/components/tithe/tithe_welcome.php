
	<style type='text/css'>
	
		.btn_welcome {
			color:#FFFFFF;
			font-family:Arial, Helvetica, sans-serif;
			font-size:48px;
			text-shadow: 1px 1px 0px rgba(0, 0, 0, 0.5);
			width:656px;
			height:155px;
			background:url("images/btn_welcome.png");
			border:hidden;
		}
		
		.btn_welcome2 {
			color:#FFFFFF;
			font-family:Arial, Helvetica, sans-serif;
			font-size:48px;
			text-shadow: 1px 1px 0px rgba(0, 0, 0, 0.5);
			width:656px;
			height:120px;
			background:url("images/btn_656x120.png");
			border:hidden;
		}
		
	</style>
	
	<script language='javascript'>
	
		function prepareTitheWelcome() {
			
			hideAppActivityShade();
			
			document.getElementById("guest_button").style.backgroundImage = "url('images/btn_656x120.png')";
		}
	
		function guestLogin(btn) {
		
			btn.style.backgroundImage = "url('images/btn_656x120_hl.png')";
			
			window.setTimeout(function(){guestLoginExec()}, 20);
		}
			
		function guestLoginExec() {
		
			var ccdn = "<?php echo $_REQUEST['cc']?>";
			fillDataArraysWithCallback(loadDataIntoObjects, ccdn, ccdn, "0", security_hash);

			go_back_from_ministries = "tithe_welcome";
			go_to_from_amounts = "tithe_swipe";
			User_Dynamic_Data.tithe_frequency = "One Time";
			go_back_from_swipe = "tithe_amounts";

			showPage("tithe_ministries");		
		}
		
	</script>
	
	<center>
		<span class='page_subtitle' style='position:absolute; left:0px; top:10px; width:1024px; height:70px;'>Welcome to</span><br><br>
		<span class='page_title' style='position:absolute; left:0px; top:70px; width:1024px; height:70px;'><?php echo $location_name; ?></span>
	</center>
		
	<div style='position:absolute; left:0px; top:150px; width:1024px; height:556px;'>
		<center>
			<table height='556px'>
				<tr>
					<td align='center' valign='middle'>
						<button class='btn_welcome2' ontouchstart='showPage("tithe_create_account");'>NEW ACCOUNT</button>
						<br><br><br>
						<button class='btn_welcome2' ontouchstart='showPage("tithe_login");'>EXISTING ACCOUNT</button>
						<br><br><br>
						<button id='guest_button' class='btn_welcome2' ontouchstart='guestLogin(this);'>GUEST</button>
					</td>
				</tr>
			</table>
		</center>
	</div>