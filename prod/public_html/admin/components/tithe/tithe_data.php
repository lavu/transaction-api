<?php

    function getGetDataJSAjaxAutoCallsAndFillsObjects(){

    	$jsCode = <<<JAVASCRIPT

					//All Data will be contained in window.Global_Data
    				//  and all objects will be attached to the window object.
    				//Global_Data = {};
					//Ministry = {};
					//Member = {};
					//ARB = {};
					//User_Dynamic_Data = {};
					
					function fillDataArraysWithCallback(callbackFunction, cc, data_name, member_id, security_hash){
						var xmlhttp;
						if (window.XMLHttpRequest) {// code for IE7+, Firefox, Chrome, Opera, Safari
							xmlhttp=new XMLHttpRequest();
						} else {// code for IE6, IE5
							xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
						}
						xmlhttp.onreadystatechange=function(){
							if (xmlhttp.readyState==4 && xmlhttp.status==200){
								callbackFunction(xmlhttp.responseText);
							}
						}
						xmlhttp.open("POST", 'request/GetStaticData.php', false );
						xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
						xmlhttp.send("cc="+cc+"&data_name="+data_name+"&member_id="+member_id+"&security_hash="+security_hash);
					}
					function loadDataIntoObjects(serverResponseJSON){
 //alert('JSON DATA DOWNLOADED FROM SERVER:' + serverResponseJSON);
						window.Global_Data = JSON.parse(serverResponseJSON);
						window.Ministry = window.Global_Data.ministry;
						window.Member = window.Global_Data.member;
						window.ARB = window.Global_Data.ARB;
						window.ActionLog = window.Global_Data.ActionLog;
						window.User_Dynamic_Data = {};
						window.User_Dynamic_Data.order_id = 0;
						action_log_page = 1;
						auto_include_processing_fee = false;
					}
					/*
					//fillDataArraysWithCallback(loadDataIntoObjects,$cc,$data_name,$member_id,$security_hash);
					*/
JAVASCRIPT;

		return $jsCode;
    }

?>