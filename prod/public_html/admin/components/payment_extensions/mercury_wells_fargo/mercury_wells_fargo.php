<?php

	$forward_cc = $_REQUEST['cc'];
	require_once(dirname(__FILE__)."/../../../lib/info_inc.php");
	$take_tip = ($location_info['allow_signature_tip'] >= 1)?"1":"0";

?><!DOCTYPE html>
<html>

	<head>	
		<style type='text/css'>
			
			body {
				font-family:sans-serif;
				background-color: transparent;
				margin: 0 0 0 0;
				padding: 0 0 0 0;
				-webkit-tap-highlight-color:rgba(0, 0, 0, 0);
				-webkit-user-select: none;
			}
			
			body:after {
				content:
					url("images/popupblue_upper_left.png")
					url("images/popupblue_upper.png")
					url("images/popupblue_upper_right.png")
					url("images/popupblue_left.png")
					url("images/popupblue_right.png")
					url("images/popupblue_lower_left.png")
					url("images/popupblue_lower.png")
					url("images/popupblue_lower_right.png")
					url("images/btn_wow_167x37.png")
					url("images/btn_wow_167x37_hl.png")
				display: none;
			}
			
			.btn_light {
				color:#7D7D7D;
				font-family:Arial, Helvetica, sans-serif;
				font-size:13px;
				width:167px;
				height:37px;
				background:url("images/btn_wow_167x37.png");
				border:hidden;
			}
			
			.message {
				color:#888888;
				font-family:Arial, Helvetica, sans-serif;
				font-size:16px;
			}
	
		</style>
		
		<script src="../webextjs.js"></script>
		<script language='javascript'>
		
			var company_id = "<?php echo $_REQUEST['company_id']; ?>";
			var loc_id = "<?php echo $_REQUEST['loc_id']; ?>";
			var device_prefix = "<?php echo $_REQUEST['device_prefix']; ?>";
			var internal_id = "";
			var tx_info = {};
					
			function startQRscan() {

				setStatusMessage("<td align='center'>Waiting for scan...</td>");
					
				window.location = '_DO:cmd=startQRscan';
			}
			
			function cancelQRscan() {
			
				window.location = '_DO:cmd=cancelQRscan';
			}
			
			function handleQRscan(qr_data, qr_reader) {
			
				setStatusMessage("<td align='center'>Processing...</td>");

				window.setTimeout(function(){processTransaction(decodeURIComponent(qr_data), qr_reader)}, 250);

				showAppActivityShade();
						
				return 1;
			}
			
			function QRScanCancelledByUser() {
			
				setStatusMessage("<td align='center'>Cancelling...</td>");
				window.setTimeout(function(){cancel()}, 100);
			
				return 1;
			}
			
			(function(){
				function ProcessGatewayAJAXRequest( params ){
					AJAXRequest.call( this, '../../../lib/gateway.php', 'POST', params, true );
				}
	
				function success( response ){
				
					var result = response.split("|");
					
					if (result[0] == "1") {
					
						setStatusMessage("<td align='center'><b>Approved</b></td>");
						
						tx_info.transaction_id = result[1];
						tx_info.card_desc = result[2];
						tx_info.auth_code = result[4];
						tx_info.card_type = result[5];
						tx_info.amount = result[7];
						tx_info.total_collected = result[7];
						tx_info.tip_amount = "0";
						tx_info.record_no = result[6];
						tx_info.ref_data = result[9];
						tx_info.process_data = result[10];
						tx_info.internal_id = internal_id;
						tx_info.transtype = "Sale";
						tx_info.action = "Sale";
						tx_info.change = "0";	
						tx_info.pay_type = "Card";
						tx_info.pay_type_id = "2";
						tx_info.got_response = "1";
						tx_info.reader = "zbar";
						tx_info.info = "";
						tx_info.take_signature = "0";
						tx_info.take_tip = "<?php echo $take_tip; ?>";

						addTransaction(tx_info);												
		
					} else {
	
						var title = (result.length > 5)?result[5]:"Declined";
						var message = (result.length > 1)?result[1]:"";

						setStatusMessage("<td align='center'><b>" + title + "</b><br>" + message + "</td>");
						hideAppActivityShade();

					}
				}
	
				function failure( status, response ){
					setStatusMessage("<td align='center'>Connection failed.<br>Please try again.</td>");
					hideAppActivityShade();
				}
	
				window.ProcessGatewayAJAXRequest = ProcessGatewayAJAXRequest;
				ProcessGatewayAJAXRequest.prototype.constructor = ProcessGatewayAJAXRequest;
				ProcessGatewayAJAXRequest.prototype = Object.create(AJAXRequest.prototype);
				ProcessGatewayAJAXRequest.prototype.success = success;
				ProcessGatewayAJAXRequest.prototype.failure = failure;
			})();

			function processTransaction(data, reader) {
			
				internal_id = newInternalID();
		
				var postVars = {};
				postVars.UUID = "<?php echo $_REQUEST['UUID']; ?>";
				postVars.MAC = "<?php echo $_REQUEST['MAC']; ?>";
				postVars.app = "<?php echo $_REQUEST['app']; ?>";
				postVars.version = "<?php echo $_REQUEST['version']; ?>";
				postVars.build = "<?php echo $_REQUEST['build']; ?>";
				postVars.server_id = "<?php echo $_REQUEST['server_id']; ?>";
				postVars.server_name = "<?php echo $_REQUEST['server_name']; ?>";
				postVars.register = "<?php echo $_REQUEST['register']; ?>";
				postVars.register_name = "<?php echo $_REQUEST['register_name']; ?>";
				postVars.cc = "<?php echo $forward_cc; ?>";
				postVars.data_name = "<?php echo $_REQUEST['dn']; ?>";
				postVars.loc_id = loc_id;
				postVars.order_id = "<?php echo $_REQUEST['order_id']; ?>";
				postVars.check = "<?php echo $_REQUEST['check']; ?>";
				postVars.reader = reader;
				postVars.encrypted = "0";
				postVars.mag_data = data;
				postVars.card_amount = "<?php echo $_REQUEST['amount']; ?>";
				postVars.transtype = "Sale";
				postVars.device_time = getCurrentDateTime();
				postVars.pay_type = "Card";
				postVars.pay_type_id = "2";
				postVars.for_deposit = "<?php echo $_REQUEST['for_deposit']; ?>";
				postVars.is_deposit = "<?php echo $_REQUEST['is_deposit']; ?>";
				postVars.ioid = "<?php echo $_REQUEST['ioid']; ?>";
				postVars.internal_id = internal_id;
				postVars.offline_mode = "<?php echo $_REQUEST['offline_mode']; ?>";
					
				new ProcessGatewayAJAXRequest(postVars);
			}
										
			function addTransactionFailed() {
			
				setStatusMessage("<td align='center'>addTransactionFailed</td>");
				
				return 1;
			}
			
			function setStatusMessage(to_html) {
				
				document.getElementById("status_message_row").innerHTML = to_html;
			}
							
		</script>
		
	</head>

	<body onload='startQRscan();'>
	
		<div style='position:absolute; left:0px; top:0px; width:318px; height:260px; opacity:0.97;'>
				
				<img style='position:absolute; left:0px; top:0px; width:17px; height:16px;' src='images/popupblue_upper_left.png'>
				<img style='position:absolute; left:17px; top:0px; width:280px; height:16px;' src='images/popupblue_upper.png'>
				<img style='position:absolute; left:297px; top:0px; width:21px; height:16px;' src='images/popupblue_upper_right.png'>
				<img style='position:absolute; left:0px; top:16px; width:17px; height:212px;' src='images/popupblue_left.png'>
				<img style='position:absolute; left:297px; top:16px; width:21px; height:212px;' src='images/popupblue_right.png'>
				<img style='position:absolute; left:0px; top:228px; width:17px; height:22px;' src='images/popupblue_lower_left.png'>
				<img style='position:absolute; left:17px; top:228px; width:280px; height:22px;' src='images/popupblue_lower.png'>
				<img style='position:absolute; left:297px; top:228px; width:21px; height:22px;' src='images/popupblue_lower_right.png'>
				
				<div style='position:absolute; left:17px; top:16px; width:280px; height:212px; background-color:#EDEFF2;'>

					<div id='view1' style='position:absolute; left:0px; top:0px; width:280px; height:212px; display:block;'>
						<center>
							<table cellpadding='6'>
								<tr>
									<td align='center' height='80px'>
										<table cellpadding='2'>
											<tr id='status_message_row' class='message'><td align='center'>Ready to begin...</td></tr>
										</table>
									</td>
								</tr>
								<tr><td align='center'><button class='btn_light' ontouchstart='startQRscan();'><b>Start QR Scan</b></button></td></tr>
								<tr><td align='center'><button class='btn_light' ontouchstart='cancel();'><b>Cancel</b></button></td></tr>
							</table>
						</center>
					</div>
					
				</div>
				
		</div>

	</body>
</html>