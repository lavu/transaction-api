<?php
/**
 * Created by Code.
 * User: Hayat
 * Date: 04/26/2019
 * Time: 8:23 PM
 */

/* 
 * This class is used for handling Papi Payment Endpoint 
 */

require_once(__DIR__ .'/../../../../inc/papi/jwtPapi.php');
class PapiPaymentHandler {
    private $papiURL;
    private $accessToken;
    function __construct($dataname, $locationId)
    {
        $args['dataname'] = $dataname;
		$args['locationid'] = $locationId;
        $jwtPapiObj = new jwtPapi($args);
		$papiPaypalAuthToken = $jwtPapiObj->generatePapiAuthToken();
        $this->papiURL = getenv('PAPI_API');
        $this->accessToken = $papiPaypalAuthToken;
        
    }

    function makePapiPayment($fields, $apiEndPoint) {
        $papiURL = $this->papiURL.$apiEndPoint;
        $pch = curl_init();
        curl_setopt($pch, CURLOPT_URL, $papiURL);
        curl_setopt($pch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($pch, CURLOPT_FOLLOWLOCATION, FALSE);
        curl_setopt($pch, CURLOPT_HTTPHEADER, array('Content-Type: application/json', 'Accept-Language: en_US', 'Authorization: ' . $this->accessToken));
        if ($fields) {
            curl_setopt($pch, CURLOPT_POSTFIELDS, $fields);
        }
        $curlResponse = curl_exec($pch);
        curl_close($pch);
        $responseArr = json_decode($curlResponse, true);
        $response = array();
        if (isset($responseArr['status']) && $responseArr['status'] == 'A') {
            $response['state'] = 'completed';
        }
        if (isset($responseArr['captureId']) && $responseArr['captureId'] != '') {
            $response['transactionNumber'] = $responseArr['captureId'];
        }
        /*
        Commented, might be need in future
        if (isset($responseArr['_id']) && $responseArr['_id'] != '') {
            $response['id'] = $responseArr['_id'];
        }
        */
        if (isset($responseArr['message']) && $responseArr['message'] != '') {
            $data = explode(' - ', $responseArr['data']);
            $response['responseCode'] = $data[0];
            $msgData = json_decode($data[1], true);
            $name = '';
            $message = '';
            if (!empty($msgData)) {
                $name = ' - ' . $msgData['name'];
                $message = ' - ' .  $msgData['message'];
                $response['errorType'] = $msgData['name'];
            }
            $response['message'] = $responseArr['message'] . $name . $message;
        }
        return json_encode($response);
    }
}