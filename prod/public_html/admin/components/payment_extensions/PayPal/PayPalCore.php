<?php
/**
 * PayPal Core hosts all core functionality for making API calls to PayPal.
 * User: Chris
 * Date: 2/4/16
 * Time: 2:16 PM
 */
require_once("PayPalTokenHelper.php");
require_once(__DIR__."/../../../lib/gateway_lib/hosted_checkout/util.php");
require_once(dirname(__FILE__) . "/../../../cp/resources/core_functions.php");

populateEnvVars();

class PayPalCore
{
    /*
     * Public Properties
     * Token Service
     * API Credentials
     * Token Scopes for oAuth token retrieval
     */
    public $tokenHelper;
    public $lavu_client_id;
    public $lavu_secret;
    public $lavu_merchantID;
    public $scopes;
    public $returnURL;
    public $username;
    public $password;
    public $signature;
    public $logoURL;
    
    /*
     * Private properties:
     * PayPalEndpointURl = root URL for PayPal Endpoint
     * attributes, business, pph and payments are all pieces of the scopes list for token requests
     */
    public $PayPalEndpointURL;
    private $attributes = "https://uri.paypal.com/services/paypalattributes";
    private $business = 'https://uri.paypal.com/services/paypalattributes/business';
    private $pph = 'https://uri.paypal.com/services/paypalhere';
    private $payments = 'https://api.paypal.com/v1/payments/';
    public $PayPalOnboardingURL;
    /*
     * Core Constructor:
     * Sets the Lavu Identifiers for API calls
     * mode is an indicator of dev environment or live.
     */
    function __construct($mode = "live", $tokenHelper = null){
        if(lavuDeveloperStatus()){
            $mode = "sandbox";
        }
        if($tokenHelper == null) $tokenHelper = new PayPalTokenHelper();
        $this->tokenHelper = $tokenHelper;

        $this->scopes = 'openid '.$this->attributes.' address profile email '.$this->business.' '.$this->pph;
        $this->lavu_client_id = $_ENV['PAYPAL_APP_ID'];
        $this->lavu_secret = $_ENV['PAYPAL_SECRET'];
        $this->lavu_merchantID = $_ENV['PAYPAL_MERCHANT_ID'];
        $this->logoURL = "https://www.paypalobjects.com/webstatic/partners/lavu_logo.png";
        $this->returnURL = "https" . "://$_SERVER[HTTP_HOST]" . "/components/payment_extensions/PayPal/ReturnFromPayPal.php";
        $this->PayPalEndpointURL = $_ENV['PAYPAL_URL'];
        $this->PayPalOnboardingURL = $_ENV['PAYPAL_ONBOARDING_URL'];
        
        $this->setAPICredentials();
    }

    private function setAPICredentials(){
        $apiCredentials = $this->tokenHelper->getAPICredentials();
       // error_log("api creds: " . print_r($apiCredentials,1));
        foreach($apiCredentials as $credential){
         //   error_log(print_r($credential, 1));
            if(strpos($credential["dataname"],"UN") !== false){
                $this->username = $credential["token"];
            }
            else if(strpos($credential["dataname"],"PW") !== false){
                $this->password = $credential["token"];
            }
            else if(strpos($credential["dataname"],"SIG") !== false){
                $this->signature = $credential["token"];
            }
        }
        //error_log("Associative Array: " . $this->username . ", " . $this->password . ", " . $this->token);
    }

    /*
     * $endPointURL - URL of PayPal service endpoint
     * $fields - POST fields for API Call to service endpoint
     */
	public function apidebug($rsp){
		$fstr = "";
		$fstr .= "\n---------------PPH:" . date("Y-m-d H:i:s") . "-------------------\n";
		$fstr .= $rsp;
		$fname = "/home/poslavu/logs/oauthdebug.txt";
		$fp = fopen($fname,"a");
		fwrite($fp,$rsp);
		fclose($fp);
	}
    public function PayPalAPICall($endPointURL, $fields){
        $ch = curl_init();
        $username = $this->lavu_client_id.":".$this->lavu_secret;
        //echo("<br>username: ".$username."<br>");
		$this->apidebug("\n-------------PayPalAPICall-------------\nusername: $username\nendpoint: ".$this->PayPalEndpointURL."$endPointURL\n");
        curl_setopt($ch, CURLOPT_URL, $this->PayPalEndpointURL . $endPointURL);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, TRUE);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
        curl_setopt($ch, CURLOPT_USERPWD,$username);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
        curl_setopt($ch, CURLOPT_SSLVERSION, 6);

        // LP-5679 - Changes to fix refresh token issue
        curl_setopt($ch, CURLOPT_HEADER, false);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('content-type: application/x-www-form-urlencoded'));

        $responseArray = curl_exec($ch);
		$this->apidebug("\nsending: $fields\n\nresponse: $responseArray\n----------------------------\n");
        $error = curl_error($ch);
        if($error){
            error_log("PayPal Core cURL error:". $error);
        }

        return $responseArray;
    }

    public function getLocationInfo($dataname, $loc_id){
        $rdb = "poslavu_".$dataname."_db";
        $result = mlavu_query("SELECT manager, address, city, state, zip, phone, email FROM `".$rdb."`.`locations` WHERE id = ".$loc_id);
        if(mysqli_num_rows($result) > 0){
            return mysqli_fetch_assoc($result);
        }else {
            return false;
        }

    }


    /*
     * Dataname, location ID to be provided by caller
     * Generates the form required for PayPalHere onboarding.
     */
    public function PayPalHereOnboardingForm($dataname, $loc_id, $echo = true){
        $locationInfo = $this->getLocationInfo($dataname, $loc_id);
        if(isset($locationInfo["manager"])) {
            $name = explode(" ", $locationInfo["manager"]);
            $firstname = isset($name[0])?$name[0]:"";
            $lastname = isset($name[1])?$name[1]:"";
        }

        $buttonText = '<table cellspacing=\'3\' align="center" width="350">
            <tr><td>
                <p>Use PayPal’s Chip Card Reader to accept payments with magnetic stripe, chip card, contactless or Apple Pay on your iPad or iPhone.</p>
                <p>Click below to create a PayPal Merchant account or link your existing PayPal Merchant account with your Lavu account.</p>
                <p>After linking your PayPal and Lavu accounts, PayPal will automatically be added to your Payment Methods.</p>
                <p><b>Only available for accounts in the United States.</b></p>
                <form method="post" id="pphsignup_form" name="pphsignup_form" action="'.$this->PayPalOnboardingURL.'signup-pph">
                        <input type="hidden" name="partnertype" value="SDK">
                        <input type="hidden" name="partner_id" value="Lavu_POS_PPHSDK">
                        <input type="hidden" name="partner_name" value="LavuPOS">
                        <input type="hidden" name="returnurl" value="'.$this->returnURL.'?dataname='.$dataname.'">
                        <input type="hidden" name="logourl" value="'.$this->logoURL.'">
                        <input type="hidden" name="swiper" value="n">
                        <input type="hidden" name="first_name" value="'.$firstname.'">
                        <input type="hidden" name="last_name" value="'.$lastname .'">';

        if(isset($locationInfo["email"])){
            $buttonText .= '<input type="hidden" name="email" value="'.$locationInfo["email"].'">';
        }
        if(isset($locationInfo["title"])){
            $buttonText .= '<input type="hidden" name="business_name" value="'.$locationInfo["title"].'">';
        }
        if(isset($locationInfo["phone"])){
            $buttonText .= '<input type="hidden" name="business_phone" value="'.$locationInfo["phone"].'">';
        }
        if(isset($locationInfo["address"])){
            $buttonText .= '<input type="hidden" name="business_address1" value="'.$locationInfo["address"].'">';
        }
        if(isset($locationInfo["city"])){
            $buttonText .= '<input type="hidden" name="business_city" value="'.$locationInfo["city"].'">';
        }
        if(isset($locationInfo["state"])){
            $buttonText .= '<input type="hidden" name="business_state" value="'.$locationInfo["state"].'">';
        }
        if(isset($locationInfo["zip"])){
            $buttonText .= '<input type="hidden" name="business_zip" value="'.$locationInfo["zip"].'">';
        }

        $buttonText .=' <button type="submit" class="paypal-button paypal-blue">Get Started</button>
                </form>
            </td></tr>
        </table>';

        if($echo){
            echo($buttonText);
        }
        else return $buttonText;

    }

    //returns the text for the Express Checkout onboarding button
    public function PayPalExpressCheckoutOnboarding($dataname, $echo = true){

        $buttonText = '
        <div dir="ltr" style="text-align: left;" trbidi="on">
        <script>(function(d, s, id){
         var js, ref = d.getElementsByTagName(s)[0];
         if (!d.getElementById(id)){
         js = d.createElement(s); js.id = id; js.async = true;
         js.src = "'. $this->PayPalOnboardingURL .
                    'webapps/merchantboarding/js/lib/lightbox/partner.js";
         ref.parentNode.insertBefore(js, ref);
         }
         }(document, "script", "paypal-js"));
        </script>
        <p>Your Lavu account is ready to accept payments using PayPal.<p>
        <p>PayPal has been added to your Payment Methods.</p>
        <p>To enable PayPal for Lavu To Go, click below. </p>
        <br>
        <a data-paypal-button="true" style="font-size:14px;border:none;padding:5px 50px;border-radius:4px;box-shadow:0 8px #8b8b8b;" class="paypal-blue" href= "' .
            $this->PayPalOnboardingURL.'webapps/merchantboarding/webflow/externalpartnerflow?integrationType=T'.
                    '&subIntegrationType=S'.
                    '&partnerId='. $this->lavu_merchantID .
                    '&productIntentID=addipmt&returnToPartnerUrl=' .$this->returnURL.'?dataname='.$dataname.
                    '&displayMode=lightbox'.
                    '&receiveCredentials=TRUE'.
                    '&showPermissions=TRUE'.
                    '&permissionNeeded=EXPRESS_CHECKOUT'.
                    '&merchantId=DataNameProbably" target="PPFrame">Enable PayPal Express Checkout</a>
        </div>
        ';
        if($echo){
            echo($buttonText);
        }
        else return $buttonText;
    }


}
