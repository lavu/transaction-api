<?php
require_once("PayPalCore.php");
//getExpressCheckoutButton();
function getExpressCheckoutButton($PayPalCore = null, $echo = true){
    if($PayPalCore == null){
        $PayPalCore = new PayPalCore();
    }
    $buttonText = '
<div dir="ltr" style="text-align: left;" trbidi="on">
<script>(function(d, s, id){
 var js, ref = d.getElementsByTagName(s)[0];
 if (!d.getElementById(id)){
 js = d.createElement(s); js.id = id; js.async = true;
 js.src = "'. $PayPalCore->PayPalOnboardingURL .
        'webapps/merchantboarding/js/lib/lightbox/partner.js";
 ref.parentNode.insertBefore(js, ref);
 }
 }(document, "script", "paypal-js"));
</script>
<p>Your Lavu account is ready to accept payments using PayPal.<p>
<p>PayPal has been added to your Payment Methods.</p>
<p>To enable PayPal for Lavu To Go, click below. </p>
<br>
<a data-paypal-button="true" style="font-size:14px;border:none;padding:5px 50px;border-radius:4px;box-shadow:0 8px #8b8b8b;" class="paypal-blue" href= "' .
        $PayPalCore->PayPalOnboardingURL.'webapps/merchantboarding/webflow/externalpartnerflow?integrationType=T'.
        '&subIntegrationType=S'.
        '&partnerId='. $PayPalCore->lavu_merchantID .
        '&productIntentID=addipmt&returnToPartnerUrl=' .$PayPalCore->returnURL.
        '&displayMode=lightbox'.
        '&receiveCredentials=TRUE'.
        '&showPermissions=TRUE'.
        '&permissionNeeded=EXPRESS_CHECKOUT'.
        '&merchantId=DataNameProbably" target="PPFrame">Enable PayPal Express Checkout</a>
</div>
';
    if($echo){
        echo($buttonText);
    }
    return $buttonText;
}