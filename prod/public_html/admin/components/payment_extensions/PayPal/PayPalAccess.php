<?php
$sessionId="";
if (isset($_REQUEST['PHPSESSID'])) {
    $sessionId = trim($_REQUEST['PHPSESSID']);
}
if ($sessionId!="") {
    session_id($sessionId);
} 
session_start();
require_once(dirname(__FILE__)."/../../../lib/info_inc.php");
include("PayPalCore.php");
$PayPalCore = new PayPalCore();
$dataname = "";
if ( isset($_REQUEST['dataname'] ) && trim( $_REQUEST['dataname'] ) !="" ) {
    $dataname = $_REQUEST['dataname'];
}
else if( isset($_SESSION['last_data_name'] ) && trim( $_SESSION['last_data_name'] ) !="" ) {
    $dataname = $_SESSION['last_data_name'];            
}

if ( $dataname === "" ) {
    echo("Access Error: No Valid Dataname");
    error_log("Access Error: No Valid Dataname");
    exit(0);
}
$refresh_token = $PayPalCore->tokenHelper->getRefreshToken($dataname, "refresh");
echo $PayPalCore->tokenHelper->getAccessToken($refresh_token, $dataname);