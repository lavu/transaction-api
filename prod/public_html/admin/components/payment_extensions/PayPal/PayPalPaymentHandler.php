<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

require_once("PayPalCore.php");
class PayPalPaymentHandler{

    private $PayPalCore;
	private $refresh_token;
	private $access_token_json;
	private $access_token;

    function __construct($dataname)
    {
        $this->PayPalCore = new PayPalCore();
        $this->refresh_token = $this->PayPalCore->tokenHelper->getRefreshToken($dataname, "refresh");
        $this->access_token_json = $this->PayPalCore->tokenHelper->getAccessToken($this->refresh_token, $dataname, $this->PayPalCore);
		$access_tokenArray = json_decode($this->access_token_json, true);
		$this->access_token = $access_tokenArray['access_token'];

		//$this->writeDebugStrOauth("refresh token: " . $this->refresh_token . "\naccess token json: " . $this->access_token_json . "\n");
    }

    function findAccessToken(){
		return $this->access_token;
       /*     $access_tokenJSON = getAccessToken();
			return $access_tokenJSON;
            $access_tokenArray = json_decode($access_tokenJSON, true);
            $access_token = $access_tokenArray['access_token'];
            if($access_token === null || $access_token === ""){
                error_log("access token could not be retrieved.");
            }
            return $access_token;*/
    }

    function makePPHPayment($fields, $curlURL, $requestType){
        $access_token = $this->findAccessToken();       
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $curlURL);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $requestType);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, FALSE);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json', 'Accept-Language: en_US', 'Authorization: Bearer '.$access_token));
        if($fields){
            curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
        }
        $response = curl_exec($ch);
        // error_log("CURL Error(s): " . curl_error ( $ch ));
        // error_log("Raw response: ".$response);
        // error_log("Curl info HTTP Response Code: ". curl_getinfo($ch, CURLINFO_HTTP_CODE));
        return $response;
    }
    
	function writeDebugStrOauth($rsp){
		$fstr = "";
		$fstr .= "\n---------------PPH:" . date("Y-m-d H:i:s") . "-------------------\n";
		$fstr .= $rsp;
		$fname = "/home/poslavu/logs/oauthdebug.txt";
		$fp = fopen($fname,"a");
		fwrite($fp,$rsp);
		fclose($fp);
	}
	function writeDebugStr($dstr){
		$fstr = "";
		$fstr .= "\n---------------PPH:" . date("Y-m-d H:i:s") . "-------------------\n";
		$fstr .= $dstr;
		$fname = "/home/poslavu/logs/paypal.txt";
		$fp = fopen($fname,"a");
		fwrite($fp,$dstr);
		fclose($fp);
	}

    function makePayment($fields,$curlURL){
        //error_log("You are using the makePayment method in " . __FILE__ . ". This method is intended for the PayPal REST API. If your intent is to work with the PayPalHere API, please use the makePPHPayment(fields) function instead.");
        $access_token = $this->findAccessToken();
        //$redirect_url = "http://devel.lavu.com/components/payment_extensions/PayPal/ReturnFromPayPal.php";
        $ch = curl_init();
        //echo("<br>username: ".$username."<br>");
        //$curlURL = "https://api.paypal.com/v1/payments/payment";
		if($curlURL=="")
			return "";//$curlURL = "https://api.paypal.com/retail/merchant/v1/pay";

		//$this->writeDebugStr("\n\nFound Access Token: " . $access_token . "\nPosting To $curlURL:\n".$fields);

        //error_log("PayPal Request to: ".$curlURL." with fields: ".$fields);
        curl_setopt($ch, CURLOPT_URL, $curlURL);
        if(lavuDeveloperStatus()){
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false); //no SSL VERIFICATION for sandbox.paypal
        }
        curl_setopt($ch, CURLOPT_ENCODING, "");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, FALSE);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json', 'Accept: application/json', 'Accept-Language: en_US', 'Authorization: Bearer '.$access_token));
        // curl_setopt($ch, CURLOPT_USERPWD,$username);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
        $response = curl_exec($ch);
        //error_log("Raw response: " . $response);
        // error_log("Response: " . json_decode($response));
        return $response;
    }

    /**
     * This function is used to hit paypal endpoint with GET and POST methos
     * @param string $url PayPal endpoint
     * @param string $data json string of payload data
     * @param string $method API method default GET
     *
     * @return jso $response json string
     */
    function callPayPalAPI($url, $data = '', $method = 'GET') {
        $access_token = $this->findAccessToken();
        $ch = curl_init();
		if ($url == "") {
			return "";
        }
        if ($method == 'GET') {
            curl_setopt($ch, CURLOPT_HTTPGET, 1);
        } else if ($method == 'POST') {
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
            if ($data) {
                curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
            }
        }
        curl_setopt($ch, CURLOPT_URL, $url);
        if(lavuDeveloperStatus()){
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false); //no SSL VERIFICATION for sandbox.paypal
        }
        curl_setopt($ch, CURLOPT_ENCODING, "");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, FALSE);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json', 'Accept: application/json', 'Accept-Language: en_US', 'Authorization: Bearer '.$access_token));
        $response = curl_exec($ch);
        return $response;
    }

}