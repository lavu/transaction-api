<?php

/*
 * This file is meant to contain all methods relevant for inserting or retrieving tokens from the Lavu Database
 */
require_once(dirname(__FILE__)."/../../../lib/info_inc.php");
class PayPalTokenHelper
{
    //Pulls the refresh token for the currently active dataname from the auth_tokens table
    //To get tokens for express checkout, TYPE must be 'merchantID'
    function getRefreshToken($dataname, $tokenType) {
        if ( isset( $dataname ) && trim( $dataname ) != "" ) {
            $RefreshTokenQuery = "SELECT `token`
                                  FROM `auth_tokens`
                                  WHERE `dataname`='" . $dataname . "'
                                        AND `token_type` = '" . $tokenType ."'
                                        AND `extension_id`=(SELECT `id` from `extensions` where `title` = 'PayPal' AND `_deleted`=0 LIMIT 1)
                                        AND `_deleted`=0";

            $refreshTokenResult = mlavu_query($RefreshTokenQuery);

            if ( mysqli_num_rows($refreshTokenResult) > 0 ) {
                $refresh_token_array = mysqli_fetch_assoc($refreshTokenResult);
                if (!isset($refresh_token_array['token']) || !$refresh_token_array) {
                    error_log('Token not set for dataname => '.$dataname);
                    return array('Error' => 'Token not set!');
                }
                return $refresh_token_array['token'];
            }
            else {
                error_log('Token information not found for dataname => '.$dataname);
                return array('Error' => 'Token information not found!');
            }
        }
        error_log('Incorrect dataname provided for dataname => '.$dataname);
        return array('Error' => 'Incorrect dataname provided!');
    }

    //Inserts a newly retrived refresh token into the database
    function insertRefreshToken($dataname, $token, $type)
    {
        $dataname = trim($dataname);
        $token = trim($token);
        $type = trim($type);
        if ( $dataname!="" && $token!="" && $type!="" ) {
            $tokenExists = $this->getRefreshToken($dataname, $type);
            if (is_array($tokenExists) || $tokenExists === null){
                $ExtensionIDQuery = "SELECT `id` FROM extensions WHERE `title` = 'PayPal'";
                $InsertTokenQuery = "INSERT INTO auth_tokens set dataname='" . $dataname . "',token='" . $token . "',extension_id = (" . $ExtensionIDQuery . ")"
                    . ",token_type='".$type."'";
                    $InsertResult = mlavu_query($InsertTokenQuery);
                    return $InsertResult;
            } else {
                return $this->updateToken($dataname, $token, $type);
            }
        }
        else {
            return array('Error' => 'Incorrect dataname OR token OR type provided!');
        }
    }


    private function updateToken($dataname, $token, $type)
    {
        $dataname = trim($dataname);
        $token = trim($token);
        $type = trim($type);
        if ( $dataname!="" && $token!="" && $type!="" ) {
            $ExtensionIDQuery = "SELECT `id` FROM extensions WHERE `title` = 'PayPal'";
            $updateQuery = "UPDATE auth_tokens SET token = '" . $token . "' WHERE dataname = '" . $dataname . "' and token_type = '".$type."' and extension_id = (" . $ExtensionIDQuery . ")";
            $updateResult = mlavu_query($updateQuery);
            return $updateResult;
        }
        else {
            return array('Error' => 'Incorrect dataname OR token OR type provided!');
        }
    }

    public function getAPICredentials(){
        $ExtensionIDQuery = "SELECT `id` FROM extensions WHERE `title` = 'PayPal'";
        $APICredentialQuery = "SELECT dataname, token FROM `auth_tokens` WHERE `token_type` = 'API' and extension_id = (".$ExtensionIDQuery.")";
        $APICredentialResult = mlavu_query($APICredentialQuery);
        $credentialArray = array();
        while($row = mysqli_fetch_assoc($APICredentialResult)){
            $dataName = trim($row['dataname']);
            $token = trim($row['token']);
            if ( $dataName != "" && $token != "" ) {
                $credentialArray[] = $row;
            }
        }
        return $credentialArray;
    }

    //Makes the call to PayPal services for a new Access Token for the given Refresh Token
    public function getAccessToken($refresh_token, $dataname, $usePayPalCore=false) {
        global $PayPalCore;
        $time = gettimeofday();
        $accessToken = $dataname."_PayPalAccessToken";
        $tokenExpireTime = $dataname."_PayPalAccessTokenExpiration";
        $PayPalPartnerID = "Lavu_POS_PPHSDK";
        $refreshURL = "https" . "://$_SERVER[HTTP_HOST]" . "/components/payment_extensions/PayPal/PayPalAccess.php";
        if($usePayPalCore)
            $PayPalCore = $usePayPalCore;
        else if($PayPalCore === null){
            $PayPalCore = new PayPalCore();
        }
        /***
         * Adding Check for the Token in the session to prevent extraneous calls to the PayPal Token Service
         */
        if($this->checkExistingToken($accessToken, $tokenExpireTime)){
            //adding a default time of 28800 for expiration time sent to the app to account for miscalculation in app.  Will fix issue in app at a later time and revert this line to properly send the ACTUAL remaining TTL.
            $returnArray = Array('access_token'=> $_SESSION[$accessToken], 'expires_in'=> '28800', 'refresh_url'=>$refreshURL, 'referrer_code'=>$PayPalPartnerID);
            if(lavuDeveloperStatus()){
                $returnArray['sandbox'] = 1;
            }
            return  json_encode($returnArray);

        }

        $fields = "grant_type=refresh_token&refresh_token=". $refresh_token;
        //$response = json_decode($PayPalCore->PayPalAPICall("webapps/auth/protocol/openidconnect/v1/tokenservice", $fields), true);
        $response = json_decode($PayPalCore->PayPalAPICall("v1/identity/openidconnect/tokenservice", $fields), true);

        /***
         *Add the token and the token expiration time to the Session
         */
        if(isset($response['access_token'])){
            $_SESSION[$accessToken] = $response['access_token'];
            if(isset($response['expires_in'])){
                $_SESSION[$tokenExpireTime] = $time['sec'] + $response['expires_in'];
            }
        }
        $returnArray = Array('access_token'=> $response["access_token"], 'expires_in'=> $response["expires_in"], 'refresh_url'=>$refreshURL, 'referrer_code'=>$PayPalPartnerID);
        if(lavuDeveloperStatus()){
            $returnArray['sandbox'] = 1;
        }

        return json_encode($returnArray);
    }

    /***
     * Checks for a token in the session and returns true if the token exists and is valid.
     */
    public function checkExistingToken($accessToken, $tokenExpireTime) {
        $time = gettimeofday();
        if(isset($_SESSION[$accessToken]) && isset($_SESSION[$tokenExpireTime])){
            if(empty($_SESSION[$accessToken]) || empty($_SESSION[$tokenExpireTime])){
               return false;
           }

           if($time['sec'] < $_SESSION[$tokenExpireTime]){
               return true;
           }
       }

       return false;
    }
}
