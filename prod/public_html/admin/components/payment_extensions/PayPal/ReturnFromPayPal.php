<?php
/**
 * This page is designed to handle all return requests from PayPal.  May need to be adjusted as needed to handle 
 * additional web traffic.  All PayPal requests that require a return_url MUST reference this file, see wiki.lavu/PayPal_Here
 * for more information.
*/
require_once(dirname(__FILE__)."/../../../cp/areas/extensions/extensions_requests.php");
require_once("PayPalCore.php");

//require_once("PayPalExpressCheckout.php");
$PayPalCore = new PayPalCore();
$authorize_url = "signin/authorize";  // LP-5679 - Changes to fix refresh token issue
//$token_url = "webapps/auth/protocol/openidconnect/v1/tokenservice";
$token_url = "v1/identity/openidconnect/tokenservice";
$denied_URL = "https://admin.poslavu.com/components/payment_extensions/PayPal/paypal-error.html";
$lavuerror_URL = "https" . "://$_SERVER[HTTP_HOST]" . "/components/payment_extensions/PayPal/lavu-error.html";
$finalURL = "https" . "://$_SERVER[HTTP_HOST]" . "/components/payment_extensions/PayPal/final.html";
$cp3_suffix = "-is_cp3";

session_start();

if(isset($_GET["status"]) && isset($_GET["subcode"])){
    onboarding_complete();
}        
else if(isset($_GET["code"])){            
    authorization_init();
}
else if(isset($_GET["merchantIdInPayPal"])){
    ExpressCheckoutReturn();
}
else{
    // Gets the 'state' request argument
    $dataname = getCurrentDataname();

    if (isCP3($dataname)) {
        $lavuerror_URL = $_SERVER["CP3_URL"]."/extensions/featured/paypal?status=" . $_GET["error"];
    }

    header("Location: ". $lavuerror_URL);
}

/**
 * Given two strings, return true if the second string is the suffix of the first string.
 */
function endsWith($string, $endString) 
{ 
	$len = strlen($endString); 
	if ($len == 0) { 
		return true; 
	} 
	return (substr($string, -$len) === $endString); 
}

function isCP3($datanameValue) {
    global $cp3_suffix;
    return strlen($datanameValue) > 0 && endsWith($datanameValue, $cp3_suffix);
}

function getCurrentDataname(){
    if(isset($_REQUEST['dataname'])){
        return $_REQUEST['dataname'];
    }
    else if(isset($_REQUEST['state'])){
        return $_REQUEST['state'];
    }
    else{
        return ""; //debug dataname
    }
}

function authorization_init(){
    global $token_url;
    global $PayPalCore;
    global $lavuerror_URL;
    global $finalURL;
    global $cp3_suffix;

    // Gets the 'state' request argument
    $stateDataname = getCurrentDataname();

    if (isCP3($stateDataname)) {
        $finalURL = $_SERVER["CP3_URL"]."/extensions/featured/paypal?status=success";
        $dataname = str_replace($cp3_suffix, "", $stateDataname);
    } else {
        $dataname = $stateDataname;
    }

    $return_url = "https" . "://$_SERVER[HTTP_HOST]" . parse_url($_SERVER["REQUEST_URI"], PHP_URL_PATH);

    $fields = 'grant_type=authorization_code&'.
        'code='.$_GET["code"].'&'.
        'redirect_uri=' . $return_url;
    $result = $PayPalCore->PayPalAPICall($token_url, $fields);
    $resultArray = json_decode($result, true);
    if(isset($resultArray["refresh_token"]) && $resultArray["refresh_token"] !== '' && $resultArray["refresh_token"] !== null) {
        $refresh_token = $resultArray['refresh_token'];
        error_log("PayPal Onboarding, Refresh token: ".$refresh_token." dataname: ".$dataname);
        $previousToken = $PayPalCore->tokenHelper->getRefreshToken($dataname, "refresh");
        $result = $PayPalCore->tokenHelper->insertRefreshToken($dataname, $refresh_token, "refresh");
        if($result === "Error: No valid token"){
            error_log("PayPal Onboarding error, failed to insert token ".$refresh_token." for user ".$dataname);
        }
        $PayPalIDResult = mlavu_query("SELECT `id` FROM extensions WHERE `title` = 'PayPal'");
        $ppIDResultArray = mysqli_fetch_assoc($PayPalIDResult);
        $PayPalID = $ppIDResultArray["id"];
        $resp = "";
        enableDisablePayPal($PayPalID, $resp, true);
        //Redirect to "end of onboarding" Splash page
        if(!is_array($previousToken)){
            $email = email_sales();
        }
        if(!$email){
            error_log("Failed to send email to sales team for ".$dataname);
        }

        header('Location:'.$finalURL);
    }
    else{
        if (isCP3($stateDataname)) {
            $lavuerror_URL = $_SERVER["CP3_URL"]."/extensions/featured/paypal?status=" . $resultArray['error'];
        }

        header("Location: ". $lavuerror_URL);
    }

}

function onboarding_complete(){
    global $PayPalCore;
    global $authorize_url;
    global $denied_URL;
    global $cp3_suffix;

    $is_cp3 = isset($_REQUEST['isCP3']);

    $dataname = getCurrentDataname();
    if ($is_cp3) {
        $dataname = $dataname . $cp3_suffix;
    }

    $status = $_GET["status"];
    $subcode = $_GET["subcode"];
    error_log("Beginning the PayPal Authorization Process");
    if(($status === "success" && $subcode === "approved")||
        ($status === "pending" && $subcode === "manualreview")){
        $return_url = "https" . "://$_SERVER[HTTP_HOST]" . parse_url($_SERVER["REQUEST_URI"], PHP_URL_PATH);
        header('Location: '.$PayPalCore->PayPalOnboardingURL . $authorize_url.'?scope='.$PayPalCore->scopes.'&response_type=code&redirect_uri='.$return_url.'&state='.$dataname.'&client_id='.$PayPalCore->lavu_client_id);
        return;
    }
    else if ($status === "failure" && ($status === "declined" || $status === "error")){
        if ($is_cp3) {
            $denied_URL = $_SERVER["CP3_URL"]."/extensions/featured/paypal?status=" . $status;
        }
        header('Location: ' . $denied_URL);
    }
}

//notify sales after a user onboards
function email_sales(){
    global $PayPalCore;
    global $cp3_suffix;
    $dataname = getCurrentDataname();

    if (isCP3($dataname)) {
        $dataname = str_replace($cp3_suffix, "", $dataname);
    }

    $loc_id = 1;
    if(isset($_SESSION['locationid'])){
        $loc_id = $_SESSION["locationid"];
    }
    // error_log("Location ID: ".$loc_id." for dataname ".$dataname);
    $location_info = $PayPalCore->getLocationInfo($dataname, $loc_id);
    $salesMessage = array();
    $salesMessage['dataname'] = $dataname;
    $salesMessage['location ID'] = $loc_id;

    if($location_info !== false){
        $salesMessage['location info'] = $location_info;
    }
    // error_log("Location settings for paypal signup ".print_r($salesMessage, true));
    $messageText = json_encode($salesMessage, JSON_PRETTY_PRINT);
    // error_log("JSON Location settings for PayPal Signup: ". $messageText);
    $result = mail("sales@lavu.com", "PayPal Signup", 'Available Merchant Information for new PayPal customer: '. $messageText);
    return $result;
}


function onboarding_error(){
    echo("there was an error with the request<br>");
    echo(printr($_GET));
}

function ExpressCheckoutReturn(){
    global $PayPalCore;
    global $finalURL;
    global $cp3_suffix;
    $merchantID = $_GET["merchantIDInPayPal"];
    $dataname = getCurrentDataname();

    if (isCP3($dataname)) {
        $dataname = str_replace($cp3_suffix, "", $dataname);
        $finalURL = $_SERVER["CP3_URL"]."/extensions/featured/paypal?status=success";
    }

    if(isset($_GET["permissionsGranted"])){
        if($_GET["permissionGranted"] === 'TRUE'){
            $PayPalCore->tokenHelper->insertRefreshToken($dataname, $merchantID, "merchantID");
        }
    }            
    header('Location:' . $finalURL);        
}
