
function showAppActivityShade()
{
	window.location = "_DO:cmd=showActivityShade";
}

function hideAppActivityShade()
{
	window.location = "_DO:cmd=hideActivityShade";
}

function componentAlert(title, message)
{
	window.location = "_DO:cmd=alert&title=" + encodeURIComponent(title) + "&message=" + encodeURIComponent(message);
}

function tryParseJSON(jsonString)
{
	try
	{
		var o = JSON.parse(jsonString);
		if (o && typeof o === "object" && o !== null)
		{
			return o;
		}
	}
	catch (e) { }

	return false;
};

String.prototype.paddingLeft = function (paddingValue)
{
	return String(paddingValue + this).slice(-paddingValue.length);
};

function getCurrentDateTime(joined)
{
	var d = new Date();

	var year = d.getFullYear();
	var month = (d.getMonth() + 1);
	var day = d.getDate();
	var hour = d.getHours();
	var minute = d.getMinutes();
	var second = d.getSeconds();

	month = month.toString().paddingLeft("00");
	day = day.toString().paddingLeft("00");
	hour = hour.toString().paddingLeft("00");
	minute = minute.toString().paddingLeft("00");
	second = second.toString().paddingLeft("00");

	if (joined)
		return year + "" + month + "" + day + "" + hour + "" + minute + "" + second;
	else
		return year + "-" + month + "-" + day + " " + hour + ":" + minute + ":" + second;
}

function newInternalID()
{
	var rnum = Math.floor((Math.random() * 10000) + 1);

	return company_id + loc_id + device_prefix + "-" + rnum + "-" + getCurrentDateTime(true);
}

(function()
{
	function AJAXRequest(url, requestType, params, async)
	{
		if (arguments.length === 0)
		{
			return;
		}

		if (!requestType)
		{
			requestType = '';
		}

		if (!async)
		{
			async = false;
		}
		else
		{
			async = true;
		}

		switch (requestType.toUpperCase().trim())
		{
			case 'POST':
			case 'PATCH':
			case 'PUT':
				break;
			case 'GET':
			default:
				requestType = 'GET';
				break;
		}

		if (!params)
		{
			params = '';
		}
		else if (params instanceof Object)
		{
			obj = params;
			arr = [];
			for (var k in obj)
			{
				arr.push(encodeURIComponent( k ) + '=' + encodeURIComponent(obj[k]));
			}

			params = arr.join('&');
		}
		else if ('string' != typeof params)
		{
			params = '';
		}

		this._request = new XMLHttpRequest();
		this._request.open(requestType, url, async);
		this._request.addEventListener('readystatechange', this, false);
		if (requestType == 'POST')
		{
			this._request.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
		}

		this._request.send(params);
	}

	function handleEvent(event)
	{
		if (event.type != 'readystatechange')
		{
			return;
		}

		if (this._request.readyState === 4)
		{
			this._request.removeEventListener('readystatechange', this, false);
			if (this._request.status === 200)
			{
				this.success(this._request.responseText);
			}
			else
			{
				this.failure(this._request.status, this._request.responseText);
			}
		}
	}

	function success(responseText)
	{
		alert( responseText );
	}

	function failure(status, responseText)
	{
		alert(status);
		alert(responseText);
	}

	window.AJAXRequest = AJAXRequest;
	AJAXRequest.prototype = {};
	AJAXRequest.prototype.constructor = AJAXRequest;
	AJAXRequest.prototype.handleEvent = handleEvent;
	AJAXRequest.prototype.success = success;
	AJAXRequest.prototype.failure = failure;
})();

function addTransaction(send_info)
{
	window.location = '_DO:cmd=hideActivityShade_DO:cmd=addTransaction&tx_info=' + encodeURIComponent(JSON.stringify(send_info));
}

function updateTransaction(send_info)
{
	window.location = '_DO:cmd=hideActivityShade_DO:cmd=updateTransaction&tx_info=' + encodeURIComponent(JSON.stringify(send_info));
}

function gotCardBalance(send_info)
{
	window.location = '_DO:cmd=hideActivityShade_DO:cmd=gotCardBalance&bal_info=' + encodeURIComponent(JSON.stringify(send_info));
}

function gotSwipeResult(send_info)
{
	window.location = '_DO:cmd=hideActivityShade_DO:cmd=gotSwipeResult&info=' + encodeURIComponent(JSON.stringify(send_info));
}

function transactionCaptured(send_info)
{
	window.location = '_DO:cmd=hideActivityShade_DO:cmd=transactionCaptured&tx_info=' + encodeURIComponent(JSON.stringify(send_info));
}

function cancel()
{
	window.location = '_DO:cmd=close_overlay';
}

function socketWrite(str, ip, port)
{
	window.location = "_DO:cmd=TCPSocket&ip=" + ip + "&port=" + port + "&subcmd=write&data=" + toHex(str);
}

function toHex(str)
{
	var hex = "";
	for	(var i = 0; i < str.length; i++)
	{
		hex += "" + str.charCodeAt(i).toString(16).paddingLeft("00");
	}

	return hex;
}

function fromHex(hexx)
{
	var hex = hexx.toString();
	var str = "";
	for (var i = 0; i < hex.length; i += 2)
	{
		str += String.fromCharCode(parseInt(hex.substr(i, 2), 16));
	}

	return str;
}

/*function testSetSetting()
{
	window.location = "_DO:cmd=set_termset&keys=testing3&vals=" + toHex("yahyah!");
}

function didSetTermSet(keys, results)
{
	var k = keys.split("|_|");
	var r = results.split("|_|");

	var str = "";
	for (var i = 0; i < k.length; i++)
	{
		if (i > 0) str += ", ";
		str += k[i] + " (" + r[i] + ")";
	}

	alert(str);

	return 1;
}

function testGetSetting()
{
	window.location = "_DO:cmd=get_termset&keys=testing1|_|testing2|_|testing3";
}

function didGetTermSet(keys, vals)
{
	var k = keys.split("|_|");
	var v = vals.split("|_|");

	var str = "";
	for (var i = 0; i < k.length; i++)
	{
		if (i > 0) str += ", ";
		str += k[i] + " = " + fromHex(v[i]);
	}

	alert(str);

	return 1;
}*/