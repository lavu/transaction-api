<?php
	session_start();
	$forward_cc = $_REQUEST['cc'];
	require_once(dirname(__FILE__)."/../../../lib/info_inc.php");
	require_once(dirname(__FILE__)."/../../../lib/gateway_settings.php");

	$is_ipad = ($_REQUEST['is_ipad'] == "1");
	
	$pmv_adjX = 0;
	$sclcv_width = 318;
	$isciv_height = 460;
	$isci_title_text = "PayPal";
	$isci_title_width = 108;
	$isci_confirm_spacer = 5;
	$isci_confirm_title_font_size = 20;
	$isci_confirm_title_height = 18;
	$isci_confirm_img_height = 170;
	if ($is_ipad) {
		$pmv_adjX = 180;
		$sclcv_width = 679;
		$isciv_height = 540;
		$isci_title_text = "PayPal Customers";
		$isci_title_width = ($sclcv_width - 38 - 272);
		$isci_confirm_spacer = 30;
		$isci_confirm_title_font_size = 24;
		$isci_confirm_title_height = 25;
		$isci_confirm_img_height = 200;
	}
	
	$tx_method = "";
	$mobwal_json = getAvailableMobileWallets($data_name, $location_info);
	if (!empty($mobwal_json)) {
		require_once(resource_path()."/json.php");
		$mobile_wallets = LavuJson::json_decode($mobwal_json);
		if (isset($mobile_wallets['mobileWallets'])) {
			$input_types = array();
			foreach ($mobile_wallets['mobileWallets'] as $mobwal) {
				if (isset($mobwal['MobileWalletType'])) {
					if ($mobwal['MobileWalletType'] == "PayPal") {
						if (isset($mobwal['MobileWalletInputType'])) $input_types[] = $mobwal['MobileWalletInputType'];
					}
				}
			}
			if (in_array("PIN", $input_types) && in_array("FOTO", $input_types)) $tx_method = "2";
			else if (in_array("FOTO", $input_types)) $tx_method = "1";
			else if (in_array("PIN", $input_types)) $tx_method = "0"; 
		}	
	}
	
	if ($tx_method == "") $tx_method = (isset($location_info['mercury_paypal_transaction_method']))?$location_info['mercury_paypal_transaction_method']:0;
	$pmv_display = "none";
	$sclcv_display = "block";
	$isciv_display = "none";
	if ($tx_method == "1") {
		$sclcv_display = "none";
		$isciv_display = "block";
	} else if ($tx_method == "2") {
		$pmv_display = "block";
		$sclcv_display = "none";
	}
	
	//error_log("mercury_paypal request: ".print_r($_REQUEST, true));
	
	function getAvailableMobileWallets($data_name, $location_info) {
	
		global $mercury_server_url;
	
		$db_name = "poslavu_".$data_name."_db";
		$integration_info = get_integration_from_location($location_info['id'], $db_name);
			
		$webservice = "/ws/ws.asmx";
		
		$send_data = "<?xml version=\"1.0\" encoding=\"utf-8\"?>"
			."<soap:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\">\n"
			."<soap:Body>\n"
			."<CreditTransaction xmlns=\"http://www.mercurypay.com\">\n"
			."<tran>\n"
			."&lt;TStream&gt;\n"
			."&lt;Admin&gt;\n"
			."&lt;MerchantID&gt;".$integration_info['integration1']."&lt;/MerchantID&gt;\n"
			."&lt;TranType&gt;Mercury&lt;/TranType&gt;\n"
			."&lt;TranCode&gt;MerchantConfig&lt;/TranCode&gt;\n"
			."&lt;Memo&gt;POS Lavu&lt;/Memo&gt;\n"
			."&lt;/Admin&gt;\n"
			."&lt;/TStream&gt;\n"
			."</tran>\n"
			."<pw>".$integration_info['integration2']."</pw>\n"
			."</CreditTransaction>\n"
			."</soap:Body>\n"
			."</soap:Envelope>\n";

		$header = array("POST ".$webservice."?WSDL HTTP/1.1", "Host: ".$mercury_server_url, "Content-Type: text/xml; charset=utf-8", "SOAPAction: \"http://www.mercurypay.com/CreditTransaction\"");

		$curl_start = microtime(true);

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, "https://".$mercury_server_url.$webservice);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
		curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_TIMEOUT, 10);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $send_data);
		$response = curl_exec($ch);
		$http_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
		curl_close($ch);
				
		$curl_end = microtime(true);
		$curl_time = round((($curl_end - $curl_start) * 1000), 2) . "ms";
	
		$start_tag = "<CreditTransactionResult>";
		$end_tag = "</CreditTransactionResult>";

		$start_pos = (strpos($response, $start_tag) + strlen($start_tag));
		$end_pos = strpos($response, $end_tag);

		$curl_result = htmlspecialchars_decode(substr($response, $start_pos, ((strlen($response) - $start_pos)) - (strlen($response) - $end_pos)));
		
		$xml = simplexml_load_string($curl_result);

		return base64_decode($xml->MerchantConfig);
	}
	

?><!DOCTYPE html>
<html>

	<head>
		<style type='text/css'>
			
			body {
				font-family:sans-serif;
				background-color: transparent;
				margin: 0 0 0 0;
				padding: 0 0 0 0;
				-webkit-tap-highlight-color:rgba(0, 0, 0, 0);
				-webkit-user-select: none;
			}
			
			body:after {
				content:
					url("images/popupblue_upper_left.png")
					url("images/popupblue_upper.png")
					url("images/popupblue_upper_right.png")
					url("images/popupblue_left.png")
					url("images/popupblue_right.png")
					url("images/popupblue_lower_left.png")
					url("images/popupblue_lower.png")
					url("images/popupblue_lower_right.png")
					url("images/btn_wow_167x37.png")
					url("images/btn_wow_167x37_hl.png")
					url("images/paypal-logo.png")
					url("images/cn_input_field_bg.png")
					url("images/btn_wow_87x52.png")
					url("images/btn_wow_green_tint_87x52.png")
					url("images/btn_wow_red_tint_87x52.png")
					url("images/btn_cc_back.png")
					url("images/btn_wow_113x37.png")
					url("images/btn_wow_32x32.png")
					url("images/btn_wow_98x32.png")
					url("images/btn_back_arrow.png")
					url("images/reload_icon.png")
					url("images/x_icon.png")
					url("images/panel_div1.png")
					url("images/pp_activity_indicator.gif")
					url("images/mercury_paypal_profile_placeholder_image.png");
				display: none;
			}
			
			.btn_light {
				color:#7D7D7D;
				font-family:Arial, Helvetica, sans-serif;
				font-size:13px;
				width:167px;
				height:37px;
				background:url("images/btn_wow_167x37.png");
				border:hidden;
				padding-top:2px;
			}
	
			.btn_light2 {
				color:#7D7D7D;
				font-family:Arial, Helvetica, sans-serif;
				font-size:13px;
				width:113px;
				height:37px;
				background:url("images/btn_wow_113x37.png");
				border:hidden;
				padding-top:2px;
			}

			.btn_light3 {
				color:#7D7D7D;
				font-family:Arial, Helvetica, sans-serif;
				font-size:12px;
				width:98px;
				height:32px;
				background:url("images/btn_wow_98x32.png");
				border:hidden;
				padding-top:2px;
				font-weight:bold;
			}

			.btn_back_arrow {
				color:#7D7D7D;
				width:37px;
				height:37px;
				background:url("images/btn_back_arrow.png");
				border:hidden;
				padding-top:2px;
			}
		
			.title {
				color:#777777;
				font-family:Arial, Helvetica, sans-serif;
				font-size:18px;
			}
			
			.isci_title {
				color:#999999;
				font-family:Arial, Helvetica, sans-serif;
				font-size:26px;
			}

			.ctal {
				color:#404040;
				font-family:Arial, Helvetica, sans-serif;
				font-size:32px;
			}

			.bptl {
				color:#737373;
				font-family:Arial, Helvetica, sans-serif;
				font-size:22px;
			}
			
			.input_long {
				width:260px;
				height:40px;
				background:url("images/cn_input_field_bg.png");
				background-repeat:no-repeat;
				background-position:center;
				border:hidden;
				text-align:center;
				font-family:Arial, Helvetica, sans-serif;
				font-size:18px;
				padding-top:7px;
			}
			
			.btn_number_pad {
				color:#7D7D7D;
				font-family:Arial, Helvetica, sans-serif;
				font-size:26px;
				font-weight:bold;
				text-align:center;
				width:87px;
				height:52px;
				background-image:url("images/btn_wow_87x52.png");
				background-repeat:no-repeat;
				background-position:center;
				border:hidden;
			}

			.btn_number_pad_red {
				color:#7D7D7D;
				font-family:Arial, Helvetica, sans-serif;
				font-size:26px;
				font-weight:bold;
				text-align:center;
				width:87px;
				height:52px;
				background-image:url("images/btn_wow_red_tint_87x52.png");
				background-repeat:no-repeat;
				background-position:center;
				border:hidden;
			}

			.btn_number_pad_green {
				color:#7D7D7D;
				font-family:Arial, Helvetica, sans-serif;
				font-size:26px;
				font-weight:bold;
				text-align:center;
				width:87px;
				height:52px;
				background:url("images/btn_wow_green_tint_87x52.png");
				background-repeat:no-repeat;
				background-position:center;
				border:hidden;
			}
			
			.tab_name1 {
				color:#7f8094;
				font-family:Arial, Helvetica, sans-serif;
				font-size:16px;
				font-weight:bold;
			}

			.tab_created1 {
				color:#999999;
				font-family:Arial, Helvetica, sans-serif;
				font-size:13px;
			}
			
			.tab_pay_confirm_title {
				color:#333333;
				font-family:Arial, Helvetica, sans-serif;
				font-size:<?php echo $isci_confirm_title_font_size; ?>px;
				font-weight:bold;
			}
			
		</style>
		
		<script src="../webextjs.js"></script>
		<script language='javascript'>
	
			var company_id = "<?php echo $_REQUEST['company_id']; ?>";
			var loc_id = "<?php echo $_REQUEST['loc_id']; ?>";
			var device_prefix = "<?php echo $_REQUEST['device_prefix']; ?>";	
			var internal_id = "";
			var decimal_places = <?php echo $location_info['disable_decimal']; ?>;
			var communication_mode = "";
			var tx_info = {};
			var smallest_money = (1 / Math.pow(10, decimal_places)).toFixed(decimal_places);

			function displayMoney(amt) {
			
				return parseFloat(amt).toFixed(decimal_places);
			}

			function displayMoneyWithSymbol(amt) {
			
				var monetary_symbol = "<?php echo $location_info['monitary_symbol']; ?>";
				if (monetary_symbol == "") monetary_symbol = "$";
				var left_or_right = "<?php echo $location_info['left_or_right']; ?>";
				if (left_or_right == "") left_or_right = "left";
				
				var rtn_str = "";
				if (left_or_right == "left") rtn_str += monetary_symbol + " ";
				rtn_str += displayMoney(amt) + "";
				if (left_or_right == "right") rtn_str += " " + monetary_symbol;
				
				return rtn_str;
			}

			function pickMethod() {

				document.getElementById("pick_method_view").style.display = "block";
				document.getElementById("shortcode_longcode_view").style.display = "none";			
				document.getElementById("in_store_check_in_view").style.display = "none";			
			}
					
			var is_ipad = ("<?php echo $_REQUEST['is_ipad']; ?>" == "1");
			
			function processTransaction() {
			
				internal_id = newInternalID();

				var postVars = {};
				postVars.UUID = "<?php echo $_REQUEST['UUID']; ?>";
				postVars.MAC = "<?php echo $_REQUEST['MAC']; ?>";
				postVars.app = "<?php echo $_REQUEST['app']; ?>";
				postVars.version = "<?php echo $_REQUEST['version']; ?>";
				postVars.build = "<?php echo $_REQUEST['build']; ?>";
				postVars.server_id = "<?php echo $_REQUEST['server_id']; ?>";
				postVars.server_name = "<?php echo $_REQUEST['server_name']; ?>";
				postVars.register = "<?php echo $_REQUEST['register']; ?>";
				postVars.register_name = "<?php echo $_REQUEST['register_name']; ?>";
				postVars.cc = "<?php echo $forward_cc; ?>";
				postVars.data_name = "<?php echo $_REQUEST['dn']; ?>";
				postVars.loc_id = loc_id;
				postVars.order_id = "<?php echo $_REQUEST['order_id']; ?>";
				postVars.check = "<?php echo $_REQUEST['check']; ?>";
				postVars.card_amount = "<?php echo $_REQUEST['amount']; ?>";
				postVars.device_time = getCurrentDateTime();
				postVars.pay_type = "<?php echo $_REQUEST['pay_type']; ?>";
				postVars.pay_type_id = "<?php echo $_REQUEST['pay_type_id']; ?>";
				postVars.for_deposit = "<?php echo $_REQUEST['for_deposit']; ?>";
				postVars.is_deposit = "<?php echo $_REQUEST['is_deposit']; ?>";
				postVars.ioid = "<?php echo $_REQUEST['ioid']; ?>";
				postVars.internal_id = internal_id;
				postVars.offline_mode = "<?php echo $_REQUEST['offline_mode']; ?>";
				
				if (communication_mode == "submittingPaymentCode") {
					postVars.transtype = "PayPalSubmitCode";
					postVars.more_info = code_entry_method + "|*|" + sclc_code;
					postVars.tip_amount = displayMoney(sclc_tip);
					postVars.reader = "mercurypaypal_code";
				} else if (communication_mode == "retrievingTabs") {
					postVars.transtype = "PayPalGetTabs";
					postVars.reader = "mercurypaypal_tabs";
				} else if (communication_mode == "creatingInvoice") {
					postVars.transtype = "PayPalCreateInvoice";
					postVars.more_info = itemAndGratuityInfo(); // encodeURIComponent ??
					postVars.reader = "mercurypaypal_tabs";
				} else if (communication_mode == "confirmingPayment") {
					postVars.transtype = "PayPalPayment";
					postVars.tip_amount = active_gratuity_amount;
					postVars.pnref = active_tab_id;
					postVars.authcode = active_invoice_id;
					postVars.more_info = '{"customer_id":"' + active_customer_id + '","currency_code":"' + active_currency_code + '"}'; // encodeURIComponent ??
					postVars.reader = "mercurypaypal_tabs";
				}
					
				new ProcessGatewayAJAXRequest(postVars);
			}
			
			(function(){
				function ProcessGatewayAJAXRequest( params ){
					AJAXRequest.call( this, '../../../lib/gateway.php', 'POST', params, true, "" );
				}
	
				function success( response ){
					hideAppActivityShade();
					handleResponse(response);
				}
				
				function failure( status, response ){				
					hideAppActivityShade();
	
					if (company_id == "6214") alert("failed connection: " + status);
	
					handleResponse("");
				}
	
				window.ProcessGatewayAJAXRequest = ProcessGatewayAJAXRequest;
				ProcessGatewayAJAXRequest.prototype.constructor = ProcessGatewayAJAXRequest;
				ProcessGatewayAJAXRequest.prototype = Object.create(AJAXRequest.prototype);
				ProcessGatewayAJAXRequest.prototype.success = success;
				ProcessGatewayAJAXRequest.prototype.failure = failure;
			})();
			
			function handleResponse(response) {
				
				var approved = false;
				var status = "";
				var title = "Connection Failed";
				var message = "Unable to process payment code at this time.";
				if (communication_mode == "retrievingTabs") message = "Unrecognized response received from server.";
				else if (communication_mode == "creatingInvoice") {
					
					message = "Unable to create invoice.";
					if (company_id == "6214") alert(response);
				
				
				} else if (communication_mode == "confirmingPayment") message = "Unable to confirm payment.";
			
				var respJSON = tryParseJSON(response);
				if (respJSON) {	
					if (typeof respJSON.json_status != 'undefined') {
						status = respJSON.json_status;
						
						if (communication_mode == "submittingPaymentCode") {
						
							if (status == "success") {
						
								approved = true;
								title = "Approved";
								message = "Transaction complete.";
								
								var currency = respJSON.mpp_info;
								if (currency == "") currency = "USD";
								
								tx_info = {};
								tx_info.amount = "<?php echo $_REQUEST['amount']; ?>";
								tx_info.total_collected = "<?php echo $_REQUEST['amount']; ?>";
								tx_info.tip_amount = active_gratuity_amount;
								tx_info.change = displayMoney("0");
								tx_info.pay_type = "<?php echo $_REQUEST['pay_type']; ?>";
								tx_info.pay_type_id = "<?php echo $_REQUEST['pay_type_id']; ?>";
								tx_info.transtype = "Sale";
								tx_info.action = "Sale";
								tx_info.internal_id = internal_id;
								tx_info.got_response = "1";
								tx_info.transaction_id = respJSON.transaction_id;
								tx_info.auth_code = active_invoice_id;
								tx_info.record_no = active_customer_id;
								tx_info.ref_data = active_tab_id;
								tx_info.process_data = "MercuryPayPal";
								tx_info.card_type = "";
								tx_info.first_four = "";
								tx_info.last_four = "";
								tx_info.currency_pptxid = currency;
								tx_info.info = currency;
								tx_info.reader = "mercurypaypal_code";
						
							} else if (status == "invalid") {
									
									title = "Invalid Code";
									message = "The payment code that was sent was invalid. The payment code value was incorrect or improperly formatted.";
									
							} else if (status == "used") {
									
									title = "Used Code";
									message = "The payment code has already been used.";
									
							} else if (status == "expired") {
									
									title = "Expired Code";
									message = "The payment code was not used within a reasonable amount of time and has expired.";
							}
						
						} else if (communication_mode == "retrievingTabs") {
						
							//alert("respJSON: " + JSON.stringify(respJSON));
							
							if (status == "success") {
							
								if (typeof respJSON.tabs != 'undefined') {
									known_tabs = respJSON.tabs;
									buildTabList();
								} else {
									status = "no_tabs";
									message = "There are no PayPal customers right now.";
								}
							
							} else if (status == "no_tabs") {
							
									message = "There are no PayPal customers right now.";
							}
							
						} else if (communication_mode == "creatingInvoice") {
						
							//alert("respJSON: " + JSON.stringify(respJSON));
							
							if (status == "success") {
							
								var invoice_id = (typeof respJSON.invoice_id != 'undefined')?respJSON.invoice_id:"";
								var currency_code = (typeof respJSON.currency_code != 'undefined')?respJSON.currency_code:"";
								if (invoice_id.length == 0) {
									status = "invalid";
									title = "Invalid Invoice ID";
									message = "Unable to create invoice.";
								} else {
									active_invoice_id = invoice_id;
									active_currency_code = currency_code;
									askToConfirmTabPayment();
								}
							
							} else title = "Unrecognized Response";

						} else if (communication_mode == "confirmingPayment") {
						
							//alert("respJSON: " + JSON.stringify(respJSON));
							
							if (status == "success") {
															
								tx_info = {};
								tx_info.amount = "<?php echo $_REQUEST['amount']; ?>";
								tx_info.total_collected = "<?php echo $_REQUEST['amount']; ?>";
								tx_info.tip_amount = active_gratuity_amount;
								tx_info.change = displayMoney("0");
								tx_info.pay_type = "<?php echo $_REQUEST['pay_type']; ?>";
								tx_info.pay_type_id = "<?php echo $_REQUEST['pay_type_id']; ?>";
								tx_info.transtype = "Sale";
								tx_info.action = "Sale";
								tx_info.internal_id = internal_id;
								tx_info.got_response = "1";
								tx_info.transaction_id = respJSON.transaction_id;
								tx_info.auth_code = respJSON.auth_code;
								tx_info.record_no = respJSON.record_no;
								tx_info.ref_data = respJSON.ref_data;
								tx_info.process_data = respJSON.process_data;
								tx_info.card_type = respJSON.card_type;
								tx_info.first_four = respJSON.first_four;
								tx_info.last_four = respJSON.last_four;
								tx_info.card_desc = respJSON.last_four;
								tx_info.currency_pptxid = active_currency_code;
								tx_info.info = active_currency_code;
								tx_info.reader = "mercurypaypal_code"; //"mercurypaypal_tabs"; // temporarily spoof the code type in order to allow refunds to be properly processed in 2.3.11-
								
								var message = displayMoneyWithSymbol(parseFloat(tx_info.amount) + parseFloat(tx_info.tip_amount)) + " charged to " + active_customer_name + " using PayPal";															
								
								var remaining = "<?php echo $_REQUEST['remaining_amount']; ?>";
								var diff = (parseFloat(remaining) - parseFloat(tx_info.amount));
								
								if ((diff >= (smallest_money * 0.75)) ||	("<?php echo $location_info['ask4email_at_checkout']; ?>"=="0" && "<?php echo $location_info['bypass_checkout_message'] ?>"=="0")) {
									
									showTabPaymentApproved(message);
									tx_info.additional_info = "";
								
								} else {
								
									var tip_message = (parseFloat(tx_info.tip_amount) > (smallest_money * 0.75))?" (Includes " + displayMoneyWithSymbol(parseFloat(tx_info.tip_amount)) + " tip)":"";								
									tx_info.additional_info = message + "" + tip_message;
								
									window.setTimeout(function(){addTransaction(tx_info)}, 200);
								}
							
							} else title = "Unrecognized Response";
						}
							
						if (status == "error") {
								
							if (typeof respJSON.title != 'undefined') {
								if (respJSON.title != "") title = respJSON.title;
							}
							if (typeof respJSON.message != 'undefined') {
								if (respJSON.message != "") message = respJSON.message;
							}
						}
					}
				}
				
				if (communication_mode == "submittingPaymentCode") {
							
					document.getElementById("sclc_response_title").innerHTML = title;
					document.getElementById("sclc_response_message").innerHTML = message;
					document.getElementById("sclc_failed_buttons").style.display = approved?"none":"block";
					document.getElementById("sclc_succeeded_button").style.display = approved?"block":"none";
					document.getElementById("sclc_inner1").style.display = "none";
					document.getElementById("sclc_inner2").style.display = "none";
					document.getElementById("sclc_inner3").style.display = "block";
				
				} else if (communication_mode == "retrievingTabs") {
				
					document.getElementById("isci_activity_shade").style.display = "none";	
					if (status != "success") noTabs(message);

				} else if (communication_mode == "creatingInvoice") {
				
					document.getElementById("isci_activity_shade").style.display = "none";
					if (selected_tab_id != "") {
						if (typeof document.getElementById("cell_" + selected_tab_id) != 'undefined') document.getElementById("cell_" + selected_tab_id).style.backgroundColor = "#FFFFFF";
					}
					if (status != "success") componentAlert(title, message);

				} else if (communication_mode == "confirmingPayment") {

					document.getElementById("isci_activity_shade").style.display = "none";
					if (status != "success") componentAlert(title, message);
				}
			}
			
		// * * * * * * * * * * * * * * * * * * * * shortcode/longcode/qr code * * * * * * * * * * * * * * * * * * * * * //
			
			var sclc_code = "";
			var sclc_tip = "0";
			var get_shortcode_tips = ("<?php echo $location_info['mercury_paypal_get_shortcode_tips']; ?>" == "1");
			var getting_short_code_tip = false;
			var code_entry_method = "";
			
			function shortcodeLongcode() {
			
				sclc_code = "";
				sclc_tip = "0";
				shortCodeInput();

				document.getElementById("pick_method_view").style.display = "none";
				document.getElementById("shortcode_longcode_view").style.display = "block";
			}
			
			function shortCodeInput() {

				getting_short_code_tip = false;
				document.getElementById("label1").innerHTML = "Enter payment code:";
				document.getElementById("sclc_input").value = sclc_code;
				document.getElementById("sclc_input").placeholder = "4-8 digit code";			
				document.getElementById("qr_code_button").style.display = "block";
				document.getElementById("sclc_inner1").style.display = "block";
				document.getElementById("sclc_inner2").style.display = "none";
				document.getElementById("sclc_inner3").style.display = "none";
			}

			function startQRscan() {

				window.location = '_DO:cmd=startQRscan';
			}
			
			function handleQRscan(qr_data, qr_reader) {
			
				sclc_code = qr_data;
				code_entry_method = "2dBarCode";
						
				if (get_shortcode_tips) getShortcodeTip();
				else confirmCode();
					
				return 1;
			}
			
			function numberPadButtonPressed(btn) {
									
				var input_field = document.getElementById("sclc_input");
				var input_text = input_field.value;
				var len = input_text.length;

				if (getting_short_code_tip) {
				
					var temp_str = input_text.replace(".", "");
					var temp_amt = 0;
					if (temp_str.length > 0) temp_amt = parseFloat(temp_str);
					if (temp_amt < 1) {
						temp_str = "";
						len = 0;
					} else len = temp_str.length;
				
					if (btn == "go") {
						sclc_tip = input_text;
						confirmCode();
					} else if (btn == "back") {
						if (len > 0) {
							temp_str = temp_str.substring(0, (len - 1));
							temp_amt = parseFloat(temp_str);
							if (temp_amt > 0) input_field.value = (temp_amt / Math.pow(10, decimal_places)).toFixed(decimal_places);
							else input_field.value = "";
						} else shortCodeInput();
					} else {
						temp_str = temp_str + "" + btn;
						input_field.value = (parseFloat(temp_str) / Math.pow(10, decimal_places)).toFixed(decimal_places);
					}
				
				} else {
								
					if (btn == "go") {
						if (len >= 4) {
							sclc_code = input_text;
							code_entry_method = "Keyed";
							if (get_shortcode_tips) getShortcodeTip();
							else confirmCode();
						} else componentAlert("Invalid Code", "Payment code must be at least 4 digits long.");
					} else if (btn == "back") {					
						if (len > 0) input_field.value = input_text.substring(0, (len - 1));
						else if ("<?php echo $tx_method; ?>" == "2") pickMethod();
						else cancel();
					} else input_field.value = input_text + "" + btn;
				}
			}
			
			function getShortcodeTip() {
			
				var display_tip = "";
				if (sclc_tip == "") sclc_tip = "0";
				if (parseFloat(sclc_tip) > 0) display_tip = displayMoney(sclc_tip);
	
				getting_short_code_tip = true;
				document.getElementById("label1").innerHTML = "Enter tip:";
				document.getElementById("sclc_input").value = display_tip;
				document.getElementById("sclc_input").placeholder = "0.00";
				document.getElementById("qr_code_button").style.display = "none";
				document.getElementById("sclc_inner1").style.display = "block";
				document.getElementById("sclc_inner2").style.display = "none";
				document.getElementById("sclc_inner3").style.display = "none";
			}
			
			function confirmCode() {
			
				if (sclc_tip == "") sclc_tip = "0";
				var base_amount = "<?php echo $_REQUEST['amount']; ?>";
				var total = (parseFloat(base_amount) + parseFloat(sclc_tip));
				var basePlusTip = "";
				if (get_shortcode_tips && (parseFloat(sclc_tip) > 0)) {
					basePlusTip = "( " + displayMoney(base_amount) + " + " + displayMoney(sclc_tip) + " tip )";
				}
				
				document.getElementById("confirm_total_title_label").innerHTML = "Confirm Total:";	
				document.getElementById("confirm_total_amount_label").innerHTML = displayMoneyWithSymbol(total);	
				document.getElementById("base_plus_tip_label").innerHTML = basePlusTip;	
				document.getElementById("sclc_inner1").style.display = "none";
				document.getElementById("sclc_inner2").style.display = "block";
				document.getElementById("sclc_inner3").style.display = "none";
			}
			
			function cancelConfirmCode() {
			
				if (get_shortcode_tips) getShortcodeTip();
				else shortCodeInput();
			}
			
			function submitCode() {
			
				communication_mode = "submittingPaymentCode";
				document.getElementById("confirm_total_title_label").innerHTML = "Processing...";	
				showAppActivityShade();
				
				window.setTimeout(function(){processTransaction()}, 200);
			}
						
		// * * * * * * * * * * * * * * * * * * * * * in-store check-in * * * * * * * * * * * * * * * * * * * * * //
			
			var known_tabs = tryParseJSON('<?php echo $_REQUEST['known_tabs']; ?>');
			var invoice_info = tryParseJSON('<?php echo $_REQUEST['invoice_info']; ?>');
			var tab_sort_type = "create_date";
			var prevent_select_tab = false;
			var selected_tab_id = "";
			var active_invoice_id = "";
			var active_tab_id = "";
			var active_customer_id = "";
			var active_customer_name = "";
			var active_currency_code = "";
			var active_gratuity_amount = "";
			
			function inStoreCheckIn() {
			
				document.getElementById("pick_method_view").style.display = "none";
				document.getElementById("in_store_check_in_view").style.display = "block";
				
				var should_load_tabs = true;
				if (known_tabs) {
					if (Object.keys(known_tabs).length > 0) {
						buildTabList();
						should_load_tabs = false;
					}
				}
				
				if (should_load_tabs) {
					document.getElementById("isci_no_tabs").style.display = "none";
					loadTabs();
				}
			}
			
			function loadTabs() {
			
				communication_mode = "retrievingTabs";
				document.getElementById("isci_activity_shade").style.display = "block";			
			
				window.setTimeout(function(){processTransaction()}, 200);
			}
			
			function toggleTabSortType(btn) {
			
				var new_sort_type = (tab_sort_type == "create_date")?"alphabetical":"create_date";			
				btn.innerHTML = (new_sort_type == "create_date")?"Sort by Name":"Sort by Check-in";				
				tab_sort_type = new_sort_type;
				
				buildTabList();
			}
			
			function sortByKey(array, key) {
				return array.sort(function(a, b) {
					var x = a[key]; var y = b[key];
					return ((x < y) ? -1 : ((x > y) ? 1 : 0));
				});
			}
			
			function sortTabs() {
			
				var sort_indexes = {};
				var sort_keys = [];
				
				var indexes = [];
				for (var key in known_tabs) indexes.push(key);
				
				for (i = 0; i < indexes.length; i++) {

					var this_tab = known_tabs[indexes[i]];
					
					var name = (typeof this_tab.customer_name != 'undefined')?this_tab.customer_name:""; 
					var create_date = (typeof this_tab.create_date != 'undefined')?this_tab.create_date:""; 
					var customer_id = (typeof this_tab.customer_id != 'undefined')?this_tab.customer_id:"";
					if (customer_id.length < 4) customer_id = (Math.floor(Math.random() * 9999) + 1).toString().paddingLeft("0000");
					else customer_id = customer_id.substring(0, 4);
					
					var sort_key = (tab_sort_type == "create_date")?create_date + " " + name + " " + customer_id:name + " " + create_date + " " + customer_id;
					
					sort_indexes[sort_key] = i;
					sort_keys.push(sort_key);
				}

				var sorted_tabs = [];
				sort_keys.sort();
				if (tab_sort_type == "create_date") sort_keys.reverse();
				for (var key in sort_keys) {
					var i = sort_indexes[sort_keys[key]];
					sorted_tabs.push(known_tabs[indexes[i]]);
				}
				
				known_tabs = sorted_tabs;				
			}
			
			function buildTabList() {
			
				if (Object.keys(known_tabs).length > 0) {
				
					sortTabs();
				
					document.getElementById("isci_tab_list").style.display = "block";
					document.getElementById("isci_no_tabs").style.display = "none";
					document.getElementById("isci_confirm_payment").style.display = "none";
					document.getElementById("isci_activity_shade").style.display = "none";
									
					var str = "<table cellspacing='1' cellpadding='2'>";
			
					var keys = [];
					for (var k in known_tabs) keys.push(k);
					
					last_row_closed = false;
					for (i = 0; i < keys.length; i++) {
					
						var this_tab = known_tabs[keys[i]];
					
						if ((i % 4)==0 || !is_ipad) str += "<tr>";
						
						var tab_id = this_tab.id + "_" + this_tab.customer_id;					
						
						var img_url = this_tab.photo_url;
						if (img_url == "") img_url = "images/mercury_paypal_profile_placeholder_image.png";
						
						var profile_img = new Image();
						profile_img.src = img_url;
											
						var war = (144 / profile_img.width);
						var har = (125 / profile_img.height);
						
						var dim_str = "width:144px;";
						if (har < war) dim_str = "height:125px;";
						
						var create_date_time = this_tab.create_date;
						var check_in_message = "Unknown check-in time";
						if (create_date_time.length >= 19) {
							var utc_offset = 0;
							if (create_date_time.length >= 22) utc_offset = parseInt(create_date_time.substring(19, 22));
							create_date_time = create_date_time.substring(0, 19);
							var cdt = create_date_time.split("T");
							var dt = cdt[0].split("-");
							var tm = cdt[1].split(":");
							var local_date = new Date();
							var offset = (local_date.getTimezoneOffset() + (utc_offset * 60));
							var date = new Date(dt[0], dt[1], dt[2], tm[0], (tm[1] - offset), tm[2], 0);
							
							var hour = date.getHours();
							var minute = date.getMinutes();
							var ampm = "am";

							if ((hour * 1) >= 12) {
								ampm = "pm";
								if ((hour * 1) > 12) hour -= 12;
							}						
							minute = minute.toString().paddingLeft("00");
							
							check_in_message = "Checked in at " + hour + ":" + minute + " " + ampm;
						}
						
						if (is_ipad) {
						
							str += "<td id='cell_" + tab_id + "' align='center' style='width:160px; height:190px; background-color:#FFFFFF;' ontouchmove='preventSelectTab();' ontouchend='createInvoice(\"" + tab_id + "\");'>";
							str += "<img src='" + img_url + "' onerror='imgError(this);' style='" + dim_str + "'><br>";
							str += "<span class='tab_name1'>" + this_tab.customer_name + "</span><br>";
							str += "<span class='tab_created1'>" + check_in_message + "</span>";
							str += "</td>";

						} else {

							str += "<td id='cell_" + tab_id + "' align='center' style='width:318px; height:135px; background-color:#FFFFFF;' ontouchmove='preventSelectTab();' ontouchend='createInvoice(\"" + tab_id + "\");'>";
							str += "<table cellspacing='0' cellpadding='3'><tr>";
							str += "<td><img src='" + img_url + "' onerror='imgError(this);' style='" + dim_str + "'></td>";						
							str += "<td align='center'><span class='tab_name1'>" + this_tab.customer_name + "</span><br>";
							str += "<span class='tab_created1'>" + check_in_message + "</span></td>";
							str += "</tr></table>";
							str += "</td>";
						}
						
						if ((i % 4)==3 || !is_ipad) {
							str += "</tr>";
							last_row_closed = true;
						} else last_row_closed = false;
					}
					if (!last_row_closed) str += "</tr>";
						
					str += "</table>";
					
					//alert(str);
					
					document.getElementById("isci_tab_list").innerHTML = str;
								
				}	else {
				
					loadTabs();
				}
			}
			
			function imgError(image) {
				image.onerror = "";
				image.src = "images/mercury_paypal_profile_placeholder_image.png";
				image.style.width = "107px";
				image.style.height = "125px";
				return true;
			}

			function imgError2(image) {
				image.onerror = "";
				image.src = "images/mercury_paypal_profile_placeholder_image.png";
				image.style.width = is_ipad?"171px":"145px";
				image.style.height = is_ipad?"200px":"170px";
				return true;
			}
			
			function noTabs(message) {
			
				document.getElementById("isci_no_tabs_message").innerHTML = message;
				document.getElementById("isci_no_tabs").style.display = "block";
				document.getElementById("isci_tab_list").style.display = "none";
				document.getElementById("isci_confirm_payment").style.display = "none";	
				document.getElementById("isci_activity_shade").style.display = "none";
			}
						
			function preventSelectTab() {

				prevent_select_tab = true;
			}

			function createInvoice(tab_id) {

				if (!prevent_select_tab) {

					selected_tab_id = tab_id;
						
					if (selected_tab_id != "") {
						if (typeof document.getElementById("cell_" + selected_tab_id) != 'undefined') {
							document.getElementById("cell_" + selected_tab_id).style.backgroundColor = "#FFFFFF";
						}
					}
				
					if (typeof document.getElementById("cell_" + tab_id) != 'undefined') {
						document.getElementById("cell_" + tab_id).style.backgroundColor = "#EAFFDB";
						
						selected_tab_id = tab_id;
						communication_mode = "creatingInvoice";
						document.getElementById("isci_activity_shade").style.display = "block";			
				
						window.setTimeout(function(){processTransaction()}, 200);
					}
				}
				
				prevent_select_tab = false;
			}
		
			function itemAndGratuityInfo() {
			
				var auto_grat = (typeof invoice_info.autoGratuityAmount != 'undefined')?invoice_info.autoGratuityAmount:"0";
				var check_total = (typeof invoice_info.checkTotal != 'undefined')?invoice_info.checkTotal:"0";
				var items = (typeof invoice_info.items != 'undefined')?invoice_info.items:"";

				var grat = "0";
				var keys = [];
				for (var k in known_tabs) keys.push(k);
				for (i = 0; i < keys.length; i++) {
					var this_tab = known_tabs[keys[i]];
					var tab_id = this_tab.id + "_" + this_tab.customer_id;
					if (tab_id == selected_tab_id) {
						if (typeof this_tab.gratuity != 'undefined') {
							var gtype = this_tab.gratuity.type;
							if (typeof gtype != 'undefined') {
								if (gtype == "AMOUNT") {
									var amt = this_tab.gratuity.amount;
									if (typeof amt != 'undefined') grat = amt;
								} else if (gtype == "PERCENTAGE") {
									var prcntg = this_tab.gratuity.percentage;
									if (typeof prcntg != 'undefined') grat = (parseFloat("<?php echo $_REQUEST['amount']; ?>") * (parseFloat(prcntg) / 100));
								}
							}
						}
						break;						
					}
				}
				
				var info = {};
				info.autoGratuityAmount = auto_grat;
				info.checkTotal = check_total;
				info.items = items;
				info.gratuity = displayMoney(grat);
				
				active_gratuity_amount = displayMoney(grat);
				
				return JSON.stringify(info);
			}
			
			function askToConfirmTabPayment() {
		
				var found_tab = false;
				var keys = [];
				for (var k in known_tabs) keys.push(k);
				for (i = 0; i < keys.length; i++) {
					var this_tab = known_tabs[keys[i]];
					var tab_id = this_tab.id + "_" + this_tab.customer_id;
					if (tab_id == selected_tab_id) {
						found_tab = true;
						
						active_tab_id = this_tab.id;
						active_customer_id = this_tab.customer_id;
						
						var f_grat = parseFloat(active_gratuity_amount);
						var confirm_amount = displayMoneyWithSymbol(parseFloat("<?php echo $_REQUEST['amount']; ?>") + f_grat);
						active_customer_name = (typeof this_tab.customer_name != 'undefined')?this_tab.customer_name:"";
						var confirm_tip = (f_grat > (smallest_money * 0.75))?"(Includes " + displayMoneyWithSymbol(f_grat) + " tip)":"";				
						var img_url = this_tab.photo_url;
						if (img_url == "") img_url = "images/mercury_paypal_profile_placeholder_image.png";
					
						document.getElementById("isci_confirm_pay_title").innerHTML = "Charge " + confirm_amount + " to " + active_customer_name + " using PayPal?";
						document.getElementById("isci_confirm_pay_tip").innerHTML = confirm_tip;
						document.getElementById("isci_confirm_pay_img").src = img_url;
					
						document.getElementById("isci_no_tabs").style.display = "none";
						document.getElementById("isci_tab_list").style.display = "none";
						document.getElementById("isci_confirm_payment").style.display = "block";	
						document.getElementById("isci_activity_shade").style.display = "none";	
						document.getElementById("isci_reload_button").style.display = "none";
						document.getElementById("isci_sort_button").style.display = "none";

						break;
					}
				}
			
				if (!found_tab) componentAlert("Unable to locate tab record.", "Error");				
			}
			
			function backToTabList() {
			
				active_invoice_id = "";
				active_currency_code = "";
			
				document.getElementById("isci_no_tabs").style.display = "none";
				document.getElementById("isci_tab_list").style.display = "block";
				document.getElementById("isci_confirm_payment").style.display = "none";
				document.getElementById("isci_activity_shade").style.display = "none";
				document.getElementById("isci_reload_button").style.display = "block";
				document.getElementById("isci_sort_button").style.display = "block";
			}
			
			function confirmPayment() {
			
				communication_mode = "confirmingPayment";
				document.getElementById("isci_activity_shade").style.display = "block";			
			
				window.setTimeout(function(){processTransaction()}, 200);
			}
			
			function showTabPaymentApproved(message) {
			
				document.getElementById("isci_approved_message").innerHTML = message;
				document.getElementById("isci_confirm_payment").style.display = "none";
				document.getElementById("isci_approved").style.display = "block";
				document.getElementById("isci_activity_shade").style.display = "none";
				document.getElementById("isci_reload_button").style.display = "none";
				document.getElementById("isci_sort_button").style.display = "none";
				document.getElementById("isci_cancel_button").style.display = "none";
			}
			
			function doOnLoad() {
			
				if ("<?php echo $tx_method; ?>" == "1") inStoreCheckIn();
			}
					
		</script>
		
	</head>
	
	<body onload='doOnLoad();'>	
	
		<div id='pick_method_view' style='position:absolute; left:<?php echo $pmv_adjX;?>px; top:90px; width:318px; height:260px; opacity:0.97; display:<?php echo $pmv_display; ?>;'>
			
			<img style='position:absolute; left:0px; top:0px; width:17px; height:16px;' src='images/popupblue_upper_left.png'>
			<img style='position:absolute; left:17px; top:0px; width:280px; height:16px;' src='images/popupblue_upper.png'>
			<img style='position:absolute; left:297px; top:0px; width:21px; height:16px;' src='images/popupblue_upper_right.png'>
			<img style='position:absolute; left:0px; top:16px; width:17px; height:212px;' src='images/popupblue_left.png'>
			<img style='position:absolute; left:297px; top:16px; width:21px; height:212px;' src='images/popupblue_right.png'>
			<img style='position:absolute; left:0px; top:228px; width:17px; height:22px;' src='images/popupblue_lower_left.png'>
			<img style='position:absolute; left:17px; top:228px; width:280px; height:22px;' src='images/popupblue_lower.png'>
			<img style='position:absolute; left:297px; top:228px; width:21px; height:22px;' src='images/popupblue_lower_right.png'>
			
			<div style='position:absolute; left:17px; top:16px; width:280px; height:212px; background-color:#EDEFF2; display:block;'>

				<div id='view1' style='position:absolute; left:0px; top:0px; width:280px; height:212px; display:block;'>
					<center>
						<table cellpadding='5'>
							<tr>
								<td align='center' height='38px'>
									<table cellpadding='2'>
										<tr id='status_message_row' class='title'><td align='center'>Select PayPal Method</td></tr>
									</table>
								</td>
							</tr>
							<tr><td align='center'><button class='btn_light' ontouchstart='shortcodeLongcode();'><b>Shortcode/Longcode</b></button></td></tr>
							<tr><td align='center'><button class='btn_light' ontouchstart='inStoreCheckIn();'><b>In-Store Check-in</b></button></td></tr>
							<tr><td align='center'><button class='btn_light' ontouchstart='cancel();'><b>Cancel</b></button></td></tr>
						</table>
					</center>
				</div>
				
			</div>
			
		</div>
		
		<div id='shortcode_longcode_view' style='position:absolute; left:<?php echo $pmv_adjX;?>px; top:0px; width:318px; height:460px; opacity:0.97; display:<?php echo $sclcv_display; ?>;'>
		
			<img style='position:absolute; left:0px; top:0px; width:17px; height:16px;' src='images/popupblue_upper_left.png'>
			<img style='position:absolute; left:17px; top:0px; width:280px; height:16px;' src='images/popupblue_upper.png'>
			<img style='position:absolute; left:297px; top:0px; width:21px; height:16px;' src='images/popupblue_upper_right.png'>
			<img style='position:absolute; left:0px; top:16px; width:17px; height:412px;' src='images/popupblue_left.png'>
			<img style='position:absolute; left:297px; top:16px; width:21px; height:412px;' src='images/popupblue_right.png'>
			<img style='position:absolute; left:0px; top:428px; width:17px; height:22px;' src='images/popupblue_lower_left.png'>
			<img style='position:absolute; left:17px; top:428px; width:280px; height:22px;' src='images/popupblue_lower.png'>
			<img style='position:absolute; left:297px; top:428px; width:21px; height:22px;' src='images/popupblue_lower_right.png'>

			<div style='position:absolute; left:17px; top:16px; width:280px; height:412px; background-color:#EDEFF2; display:block;'>

				<img style='position:absolute; left:78px; top:9px; width:120px; height:36px;' src='images/paypal-logo.png'>

				<div id='sclc_inner1' style='position:absolute; left:0px; top:45px; width:280px; height:367px; display:block;'>

					<div style='position:absolute; left:10px; top:0px; width:260px; height:40px;'>
						<table><tr><td align='center' valign='middle' width='260px' height='40px'><span id='label1' class='title'>Enter payment code:</span></td></tr></table>
					</div>
					
					<div style='position:absolute; left:0px; top:40px; width:280px; height:40px;'>
						<center><input id='sclc_input' class='input_long' type='tel' placeholder='4-8 digit code' value='' READONLY></center>
					</div>
					
					<div id='number_pad' style='position:absolute; left:1px; top:91px; width:278px; height:250px;'>
						<center>
							<table cellspacing='2' cellpadding='0'>
								<tr>
									<td class='btn_number_pad' ontouchstart='numberPadButtonPressed("7"); return false;'>7</td>
									<td class='btn_number_pad' ontouchstart='numberPadButtonPressed("8"); return false;'>8</td>
									<td class='btn_number_pad' ontouchstart='numberPadButtonPressed("9"); return false;'>9</td>
								</tr>
								<tr>
									<td class='btn_number_pad' ontouchstart='numberPadButtonPressed("4"); return false;'>4</td>
									<td class='btn_number_pad' ontouchstart='numberPadButtonPressed("5"); return false;'>5</td>
									<td class='btn_number_pad' ontouchstart='numberPadButtonPressed("6"); return false;'>6</td>
								</tr>
								<tr>
									<td class='btn_number_pad' ontouchstart='numberPadButtonPressed("1"); return false;'>1</td>
									<td class='btn_number_pad' ontouchstart='numberPadButtonPressed("2"); return false;'>2</td>
									<td class='btn_number_pad' ontouchstart='numberPadButtonPressed("3"); return false;'>3</td>
								</tr>
								<tr>
									<td class='btn_number_pad_red' ontouchstart='numberPadButtonPressed("back"); return false;'><img style='padding:3px 0px 0px 0px;' src='images/btn_cc_back.png'></td>
									<td class='btn_number_pad' ontouchstart='numberPadButtonPressed("0"); return false;'>0</td>
									<td class='btn_number_pad_green' ontouchstart='numberPadButtonPressed("go"); return false;'>Go</td>
								</tr>
								<tr><td colspan='3' align='center' style='padding:6px 0px 0px 0px; display:none;'><button id='qr_code_button' class='btn_light' ontouchstart='startQRscan();'><b>Scan QR Code</b></button></td></tr>
							</table>
						</center>
					</div>
					
				</div>

				<div id='sclc_inner2' style='position:absolute; left:0px; top:45px; width:280px; height:367px; display:none;'>
				
					<div style='position:absolute; left:10px; top:40px; width:260px; height:45px;'>
						<table><tr><td align='center' valign='middle' width='260px' height='45px'><span id='confirm_total_title_label' class='title'>Confirm Total:</span></td></tr></table>
					</div>

					<div style='position:absolute; left:10px; top:115px; width:260px; height:45px;'>
						<table><tr><td align='center' valign='middle' width='260px' height='45px'><span id='confirm_total_amount_label' class='ctal'></span></td></tr></table>
					</div>

					<div style='position:absolute; left:10px; top:160px; width:260px; height:35px;'>
						<table><tr><td align='center' valign='middle' width='260px' height='35px'><span id='base_plus_tip_label' class='bptl'></span></td></tr></table>
					</div>

					<div style='position:absolute; left:0px; top:258px; width:280px; height:37px;'>
						<table cellspacing='0' cellpadding='0'>
							<tr>
								<td align='center' width='280px' height='37px'>
									<button class='btn_light2' ontouchstart='cancelConfirmCode();'><b>Cancel</b></button> &nbsp;&nbsp;
									<button class='btn_light2' ontouchstart='submitCode();'><b>Submit</b></button>
								</td>
							</tr>
						</table>
					</div>
				
				</div>

				<div id='sclc_inner3' style='position:absolute; left:0px; top:45px; width:280px; height:367px; display:none;'>
				
					<div style='position:absolute; left:10px; top:25px; width:260px; height:45px;'>
						<table><tr><td align='center' valign='middle' width='260px' height='45px'><span id='sclc_response_title' class='ctal' style='font-size:25px'>Error</span></td></tr></table>
					</div>

					<div style='position:absolute; left:10px; top:70px; width:260px; height:175px;'>
						<table><tr><td align='center' valign='middle' width='260px' height='180px'><span id='sclc_response_message' class='bptl'>This is a test error...</span></td></tr></table>
					</div>

					<div style='position:absolute; left:0px; top:258px; width:280px; height:37px;'>
						<table cellspacing='0' cellpadding='0'>
							<tr>
								<td align='center' width='280px' height='37px'>
									<div id='sclc_failed_buttons'>
										<button class='btn_light2' ontouchstart='cancel();'><b>Cancel</b></button> &nbsp;&nbsp;
										<button class='btn_light2' ontouchstart='shortCodeInput();'><b>Try Again</b></button>
									</div>
									<div id='sclc_succeeded_button'><button class='btn_light2' ontouchstart='addTransaction(tx_info);'><b>Continue</b></button> </div>
								</td>
							</tr>
						</table>
					</div>
				
				</div>
								
			</div>

		</div>

		<div id='in_store_check_in_view' style='position:absolute; left:0px; top:0px; width:<?php echo $sclcv_width;?>px; height:<?php echo $isciv_height; ?>px; opacity:0.97; display:<?php echo $isciv_display; ?>;'>
		
			<img style='position:absolute; left:0px; top:0px; width:17px; height:16px;' src='images/popupblue_upper_left.png'>
			<img style='position:absolute; left:17px; top:0px; width:<?php echo ($sclcv_width - 38);?>px; height:16px;' src='images/popupblue_upper.png'>
			<img style='position:absolute; left:<?php echo ($sclcv_width - 21);?>px; top:0px; width:21px; height:16px;' src='images/popupblue_upper_right.png'>
			<img style='position:absolute; left:0px; top:16px; width:17px; height:<?php echo ($isciv_height - 38); ?>px;' src='images/popupblue_left.png'>
			<img style='position:absolute; left:<?php echo ($sclcv_width - 21);?>px; top:16px; width:21px; height:<?php echo ($isciv_height - 38); ?>px;' src='images/popupblue_right.png'>
			<img style='position:absolute; left:0px; top:<?php echo ($isciv_height - 22); ?>px; width:17px; height:22px;' src='images/popupblue_lower_left.png'>
			<img style='position:absolute; left:17px; top:<?php echo ($isciv_height - 22); ?>px; width:<?php echo ($sclcv_width - 38);?>px; height:22px;' src='images/popupblue_lower.png'>
			<img style='position:absolute; left:<?php echo ($sclcv_width - 21);?>px; top:<?php echo ($isciv_height - 22); ?>px; width:21px; height:22px;' src='images/popupblue_lower_right.png'>

			<div style='position:absolute; left:17px; top:16px; width:<?php echo ($sclcv_width - 38); ?>px; height:<?php echo ($isciv_height - 38); ?>px; background-color:#EDEFF2; display:block;'>

				<div id='isci_reload_button' style='position:absolute; left:2px; top:2px; width:32px; height:32px;'>
					<div style='position:absolute; left:0px top:0px; width:32px; height:32px; background-image:url("images/btn_wow_32x32.png");' ontouchstart='loadTabs(); return false;'><img src='images/reload_icon.png' width='32px' height='32px'></div>
				</div>
				
				<button id='isci_sort_button' class='btn_light3' style='position:absolute; left:36px; top:2px;' ontouchstart='toggleTabSortType(this);'>Sort by Name</button>
				
				<div style='position:absolute; left:136px; top:2px; width:<?php echo $isci_title_width; ?>px; height:32px;'>
					<center>
						<span class='isci_title'><?php echo $isci_title_text; ?></span>
					</center>
				</div>
			
				<div id='isci_cancel_button' style='position:absolute; left:<?php echo ($sclcv_width - 38 - 34);?>px; top:2px; width:32px; height:32px;'>
					<div style='position:absolute; left:0px top:0px; width:32px; height:32px; background-image:url("images/btn_wow_32x32.png");' ontouchstart='cancel(); return false;'><img src='images/x_icon.png' width='32px' height='32px'></div>
				</div>
				
				<img style='position:absolute; left:1px; top:36px; width:<?php echo ($sclcv_width - 40); ?>px; height:1px;' src='images/panel_div1.png'>
				
				<div id='isci_tab_list' style='position:absolute; left:1px; top:38px; width:<?php echo ($sclcv_width - 40); ?>px; height:<?php echo ($isciv_height - 38 - 39); ?>px; overflow:scroll;'></div>

				<div id='isci_no_tabs' style='position:absolute; left:1px; top:38px; width:<?php echo ($sclcv_width - 40); ?>px; height:<?php echo ($isciv_height - 38 - 39); ?>px;'>
					<table cellspacing='0' cellpadding='3' width='<?php echo ($sclcv_width - 40); ?>px' height='<?php echo ($isciv_height - 38 - 59); ?>px'>
						<tr><td height='50%' align='center' valign='bottom' style='padding: 3px 15px 3px 15px;'><span id='isci_no_tabs_message' class='title'>There are no PayPal customers right now.</span><br><br></td></tr>
						<tr><td height='50%' align='center' valign='top'><button class='btn_light2' ontouchstart='loadTabs();'><b>Retry</b></button></td></tr>
					</table>
				</div>
				
				<div id='isci_confirm_payment' style='position:absolute; left:1px; top:38px; width:<?php echo ($sclcv_width - 40); ?>px; height:<?php echo ($isciv_height - 38 - 39); ?>px; display:none; overflow:scroll;'>
					<table cellspacing='0' cellpadding='3' width='<?php echo ($sclcv_width - 40); ?>px' height='<?php echo ($isciv_height - 38 - 59); ?>px'>
						<tr><td height='<?php echo $isci_confirm_spacer; ?>px'> </td></tr>
						<tr><td height='<?php echo $isci_confirm_title_height; ?>px' align='center' valign='bottom'><span id='isci_confirm_pay_title' class='tab_pay_confirm_title'></span><br></td></tr>
						<tr><td height='30px' align='center' valign='top'><span id='isci_confirm_pay_tip' class='title'></span><br></td></tr>
						<tr><td align='center' valign='top'><img id='isci_confirm_pay_img' src='images/mercury_paypal_profile_placeholder_image.png' style='height:<?php echo $isci_confirm_img_height; ?>px' onerror='imgError2(this);'></td></tr>
						<tr><td height='70px' align='center'><button class='btn_back_arrow' ontouchstart='backToTabList();'>&nbsp;</button> &nbsp;&nbsp;&nbsp; <button class='btn_light' ontouchstart='confirmPayment();'><b>Confirm Payment</b></button></td></tr>
						<tr><td> </td></tr>
					</table>
				</div>

				<div id='isci_approved' style='position:absolute; left:1px; top:38px; width:<?php echo ($sclcv_width - 40); ?>px; height:<?php echo ($isciv_height - 38 - 39); ?>px; display:none'>
					<table cellspacing='0' cellpadding='3' width='<?php echo ($sclcv_width - 40); ?>px' height='<?php echo ($isciv_height - 38 - 59); ?>px'>
						<tr><td height='35%' align='center' valign='bottom'><span class='tab_pay_confirm_title'>Approved</span><br><br></td></tr>
						<tr><td height='15%' align='center' valign='top'><span id='isci_approved_message' class='title'></span><br><br></td></tr>
						<tr><td height='50%' align='center' valign='top'><button class='btn_light2' ontouchstart='addTransaction(tx_info);'><b>Continue</b></button></td></tr>
					</table>
				</div>
				
				<div id='isci_activity_shade' style='position:absolute; left:1px; top:38px; width:<?php echo ($sclcv_width - 40); ?>px; height:<?php echo ($isciv_height - 38 - 39); ?>px; background-color:#DDDDDD; opacity:0.4; z-index:100; display:none;'>
					<table width='<?php echo ($sclcv_width - 40); ?>px' height='<?php echo ($isciv_height - 38 - 39); ?>px'>
						<tr><td align='center' valign='middle'><img src='images/pp_activity_indicator.gif' width='32px' height='32px'><br><br></td></tr>
					</table>
				</div>				
							
			</div>
			
		</div>
			
	</body>

</html>