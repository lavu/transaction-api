<?php 
	ob_start();
	// update payment_types set `special`='payment_extension:91' where `special`='payment_extension:75'
	// update payment_types set `special`='payment_extension:92' where `special`='payment_extension:77'
	// update `forced_modifiers` set `extra5`='0x0x0x0|103x154x818x460' where `title`='add_mod' and `extra2`='lavugift'
?>
     
<!DOCTYPE html>
<html>

	<head>

		<link rel='stylesheet' href='lavugift.css' type='text/css' charset='utf-8'>

		<style type='text/css'>

			.inner_div1
			{
				position: absolute;
				left: 0px;
				top: 0px;
				width: 780px;
				height:412px;
				display: none;
			}

			.inner_div2
			{
				position: absolute;
				left: 0px;
				top: 0px;
				width: 780px;
				height: 367px;
				display: none;
			}

		</style>

		<script src="../webextjs.js"></script>
        <?php //require_once(dirname(__FILE__) . "/templatescript.php"); ?>
	</head>

	<body onload='initialize(); startListening();'>
        <div id='enter_card_number' class='inner_div2'>
            <br><br>
            <?php 
                $keyboard_title = "Enter Card Id/Number";
                $keyboard_action = "	window.location = \"?submit_id=\" + keyboard_value + \"&viewmode=submit_\" + viewmode; ";

				echo "<input type='text' name='enter_card_id' value='' size='42' style='font-size:18px; color:#777777' placeholder='$keyboard_title' />";
				echo "<input type='button' value='Submit >>' onclick='keyboard_value = document.getElementById(\"enter_card_id\").value; $keyboard_action' />";
            ?>
        </div>
        <div class='content_container'>
            <?php $lg_template_top = ob_get_contents(); ob_end_clean(); ob_start(); ?>
        </div>
	</body>

</html>
<?php
	$lg_template_bottom = ob_get_contents();
	ob_end_clean();

	$lg_templates = array();
	$lg_templates['payment-main'] = "
		<table cellpadding=12>
			<tr>
				<td align='left' style='font-size:20px; color:#aaaaaa; width:480px'>
					<br>
					Lavu Gift Card Payment:&nbsp;&nbsp;
					<b>[display_amount]</b><br><br>
				</td>
			</tr>
		</table>";
	$lg_templates['payment-start'] = "
		<br><br>
		<br><br>
		<table style='min-height:320px;' cellspacing=0>
			<tr>
				<td style='width:80px;'>&nbsp;</td>
				<td align='center' valign='middle' style='width:618px; min-height:320px;'>
					<table cellspacing=0 cellpadding=6 width='480' style='border:solid 1px #dddddd'>";
	$lg_templates['payment-end'] = "
					</table>
				</td>
			</tr>
		</table>";
	$lg_templates['payment-2col'] = "<tr><td align='right'>[key]</td><td>[value]</td></tr>";
	$lg_templates['payment-title'] ="<tr><td colspan='2' align='center' bgcolor='#dddddd'>[value]</td></tr>";
	$lg_templates['payment-1col'] = "<tr><td colspan='2' align='center'>[value]</td></tr>";
	$lg_templates['payment-usecard'] = "
		<tr><td colspan='2' align='center'>
			<input class='btn_light_long' type='button' style='font-size:20px' value='Use Card' onclick='window.location = [link]' ontouchstart='window.location = [link]' />
		</td><tr>";
	$lg_templates['payment-tryanother'] = "
		<tr><td colspan='2' align='center'>
			<input class='btn_light_long' type='button' style='font-size:20px' value='Try Another' onclick='window.location = [link]' ontouchstart='window.location = [link]' />
		</td><tr>";

	$lg_templates['loyalty-main'] = "
		<table cellpadding=12>
			<tr>
				<td align='left' style='font-size:20px; color:#aaaaaa; width:480px'>
					<br>Lavu Loyalty<br>
				</td>
			</tr>
		</table>
	";

	$lg_templates['loyalty-start'] = "
		<br><br>
		<br><br>
		<table style='min-height:320px;' cellspacing=0>
			<tr>
				<td style='width:80px;'>&nbsp;</td>
				<td align='center' valign='middle' style='width:618px; min-height:320px;'>
					<table cellspacing=0 cellpadding=6 style='border:solid 1px #dddddd; width:400px'>
						<tr><td align='center'>";

	$lg_templates['loyalty-end'] = "
						</td></tr>
					</table>
				</td>
			</tr>
		</table>";
?>