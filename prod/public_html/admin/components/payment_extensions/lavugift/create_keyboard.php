<style>
	body, table {
		-webkit-user-select: none;
		-khtml-user-select: none;
		-moz-user-select: none;
		-o-user-select: none;
		-ms-user-select: none;
		user-select: none;
	}
</style>

<script type='text/javascript'>
	keyval = "";
	lastkeystr = "";
	lastk = "";
	visible_keyboard = 0;
	keyboard_count = 2;
	locked_key = "";
	ukt_started = false;
	capslock_on = false;
	function key_select(keystr,k,kobj)
	{
		if(locked_key == keystr) return;
		locked_key = keystr;
		if(ukt_started) clearTimeout(ukt);
		ukt = setTimeout("unlock_key()",100);
		ukt_started = true;
		
		var repeatspeed = 250;
				
		if(keystr=="C")
		{
			switch_capslock(kobj);
		}
		else if(keystr=="<")
		{
			keyval = keyval.substring(0,keyval.length - 1);
			repeatspeed = 100;
		}
		else if(keystr=="*")
		{
			switch_visible_keyboard();
			return;
		}
		else
		{
			if(capslock_on && keystr.length==1)
			{
				keystr = keystr.toUpperCase();
			}
			keyval = keyval + keystr;
		}
		document.getElementById('customername').value = keyval + "_";//strfirstcaps(keyval) + "_";
		lastkeystr = keystr;
		lastk = k;
		setTimeout("key_hold()",repeatspeed);
	}
	function unlock_key()
	{
		locked_key = "";
	}
	function switch_capslock(capselem)
	{
		capslock_on = !capslock_on;
		
		var elemlist = document.getElementsByTagName("*");
		for(var e = 0; e < elemlist.length; e++)
		{
			var elem = elemlist[e];
			if(elem.hasAttribute("lowercaseval") && elem.hasAttribute("uppercaseval"))
			{
				if(capslock_on) setkey = elem.getAttribute("uppercaseval");
				else setkey = elem.getAttribute("lowercaseval");
				elem.innerHTML = setkey;
			}
		}
	}
	function switch_visible_keyboard()
	{
		visible_keyboard++;
		if(visible_keyboard >= keyboard_count) visible_keyboard = 0;
		for(n=0; n<keyboard_count; n++)
		{
			if(visible_keyboard==n) set_display = "block";
			else set_display = "none";
			document.getElementById("container_" + n).style.display = set_display;
		}
	}
	function key_up()
	{
		lastkeystr = "";
		lastk = "";
	}
	function key_hold()
	{
		if(lastkeystr!="" && lastk!="")
		{
			key_select(lastkeystr,lastk);
		}
	}
	function strfirstcaps(str) {
		var str_parts = str.split(' ');
		var newstr = "";
		for(var n=0; n<str_parts.length; n++)
		{
			strpart = str_parts[n];
			if(n >0) newstr += " ";
			newstr += strpart.charAt(0).toUpperCase() + strpart.slice(1);
		}
    	return newstr;
	}
	function customer_name_submit()
	{
		var emailToSendToApp = "test@test.com";
		var customer_name = keyval;//strfirstcaps(keyval);
		var customer_id = 0;
		var jsonInfo = "";
		
		<?php echo str_replace("keyboard_value","customer_name",$keyboard_action); ?>
		//window.location = "_DO:cmd=close_overlay&set_info=" + customer_name + "|o|" + customer_id + "|o|" + encodeURIComponent( jsonInfo );		
	}
</script>

<table width='100%'><tr><td width='100%' align='center' valign='top' ontouchend='key_up()'>
	<br>
	<table><tr><td>
   	 <input type='text' id='customername' style='font-size:24px' size='24' placeholder='<?php echo $keyboard_title?>' readonly />
    </td><td>
	    <div style='font-size:24px; color:#97a8a9; background-color:#eeeeee; border:solid 1px #777777; border-radius:6px; padding:4px; padding-left:20px; padding-right:20px; width:140px' ontouchstart='customer_name_submit()'>Continue</div>
    </td></tr></table>
    
    <br>
    <?php
		$kbdata = array();
		$kbdata[] = array("keys"=>"qwertyuiop<-asdfghjkl@-Czxcvbnm.SS-* M","switch"=>"123");
		$kbdata[] = array("keys"=>"1234567890<-/:;()$&@-.,?!'\"-* M","switch"=>"abc");
	
		for($kb=0; $kb<count($kbdata); $kb++)
		{
			$keystr = $kbdata[$kb]['keys'];
			$switchstr = $kbdata[$kb]['switch'];
			
			$cstyle = "display:none; ";
			if($kb==0)
			{
				$cstyle = "display:block; ";
			}
			
			echo "<div style='$cstyle' id='container_$kb'>";
			echo "<table><tr>";
			for($k=0; $k<strlen($keystr); $k++)
			{
				$key = substr($keystr,$k,1);
				if($key=="-") // newline
				{
					echo "</tr></table><table><tr>";
				}
				else if($key=="S") // spacer
				{
					echo "<td style='width:60px'>&nbsp;</td>";
				}
				else
				{
					$showkey = $key;
					$extrastyle = "";
					$has_uppercase = false;
					
					if($key=="<") // delete
					{
						$extrastyle = "width:80px;";
						$showkey = "del";
					}
					else if($key==" ") // space
					{
						$extrastyle = "width:240px";
						$showkey = "&nbsp;";
					}
					else if($key=="*") // switch keyboard
					{
						$extrastyle = "width:80px;";
						$showkey = $switchstr;
					}
					else if($key=="C") // caps
					{
						$showkey = "&#8679;"; //8593 // Up Arrow
					}
					else if($key=="M") // .com
					{
						$extrastyle = "width:100px;";
						$key = ".com";
						$showkey = ".com";
					}
					else
					{
						$has_uppercase = true;
					}
					
					if($key=="C") // capslock button;
					{
						$keylower = $showkey;
						$keyupper = "<b>&#8593;</b>";
					}
					else if($has_uppercase)
					{
						$keylower = strtolower($showkey);
						$keyupper = strtoupper($showkey);
					}
					else
					{
						$keylower = $showkey;
						$keyupper = $showkey;
					}
					
					echo "<td id='key_$key' lowercaseval='$keylower' uppercaseval='$keyupper' width='60' height='60' align='center' valign='middle' style='background-color:#aaaabb; opacity:.8; border:solid 1px #777777; $extrastyle' ontouchstart='key_select(\"$key\",\"$k\",this); return false' onselectstart='return false'>$showkey</td>";
				}
			}
			echo "</tr></table>";
			echo "</div>";
		}
	?>
</td></tr></table>
