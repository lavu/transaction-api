<?php

session_start();

ini_set("display_errors", 0);

require_once(__DIR__."/../../../lib/NewRelicHelper.php");

$NewRelic = new NewRelicHelper();
$NewRelic->trackRequestStart();
$NewRelic->track("client_ip", getClientIPServer());
require_once(__DIR__."/LavuGiftProcessor.php");

$lavuGiftProcessor = new LavuGiftProcessor();
$lavuGiftProcessor->processTransactionRequest($_REQUEST);

lavu_exit(); // calls NewRelicHelper's trackRequestEnd()