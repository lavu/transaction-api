<?php

//error_log("lavugift: ".json_encode($_REQUEST));

session_start();

require_once(__DIR__."/../../../lib/info_inc.php");
require_once(__DIR__."/../extfunctions.php");

if (empty($data_name))
{
	$data_name = remember_reqvar("dataname");
}

if (isset($_SESSION['remember_reqvar_location_info']))
{
	$location_info = $_SESSION['remember_reqvar_location_info'];
}

$loc_id				= $location_info['id'];
$loyalty_print		= locationSetting("loyalty_gift_auto_print", FALSE);
$expiration_setting = locationSetting("loyalty_gift_expiration", "");
$db_name			= "poslavu_".$data_name."_db";
$integration_info	= get_integration_from_location($loc_id, $db_name);

$server_list = array();
if (reqvar("action") == "Admin")
{
	$get_server_list = lavu_query("SELECT `id`, `f_name`, `l_name` FROM `users` WHERE `active` = '1' AND `_deleted` = '0'");
	if (mysqli_num_rows($get_server_list) > 0)
	{
		while ($s_info = mysqli_fetch_assoc($get_server_list))
		{
			$server_list[str_pad($s_info['id'], 6, "0", STR_PAD_LEFT)] = $s_info['f_name']." ".$s_info['l_name'];
		}
	}
}

$using_new_version_gift = FALSE;
$pt_query = lavu_query("SELECT * FROM `payment_types` WHERE `special` = 'payment_extension:91' AND `loc_id` = '[1]' AND `_deleted` = '0'", $loc_id);
if (mysqli_num_rows($pt_query))
{
	$using_new_version_gift = TRUE;
}

$use_view_width = 280;

if ($using_new_version_gift)
{
	$use_view_width = 780;

	if(isset($ll_mode) && $ll_mode=="visitor")
	{
		require_once(__DIR__."/templatevs.php");
	}
	else
	{
		require_once(__DIR__."/templatekb.php");
	}
}
else
{
	require_once(__DIR__."/templatelg.php");
}

// Output Top of Template
echo $lg_template_top;

if(!isset($data_name) || $data_name=="")
{
	echo "<table cellpadding=12>";
	echo "<tr>";
	echo "<td align='center' style='font-size:14px; color:#aaaaaa; width:280px'>";
	echo "<br><br><br>";
	echo "Error: Timeout<br>This usually occurs after leaving the gift card screen open for longer than 20 minutes.  Please close the gift card window and try again";
	echo "</td></tr></table>";
}
else
{	
	if (strpos($viewmode, "loyalty") !== FALSE)
	{
		require_once(__DIR__."/views_loyalty.php");
	}
	else
	{
		require_once(__DIR__."/views_gift.php");
	}
}

// Output Bottom of Template
echo $lg_template_bottom;

function verify_amount_is_int($amount)
{
	if (strpos($amount, ",") !== FALSE)
	{
		$amount = str_replace(",", ".", $amount);
	}

	return $amount;
}

function get_expiration_date($datetime)
{
	global $expiration_setting;

	// Never expires
	if (empty($expiration_setting))
	{
		return "";
	}

	$duration;
	$expdate = DateTime::createFromFormat("Y-m-d H:i:s", $datetime);
	$expiration_split = explode(":", $expiration_setting);

	if (count($expiration_split) == 1)
	{
		$initial	= explode(".", $expiration_split[0]);
		$type		= $initial[0];
		$val		= $initial[1];
		$duration	= new DateInterval("P".$val.strtoupper($type));
	}
	else
	{
		//TODO: handle these cases down the road.
	}
	$expdate->add($duration);

	return $expdate->format("Y-m-d");
}

function ext_display_points($points)
{
	$str = $points." point";
	if ((int)$points != 1)
	{
		$str .= "s";
	}

	return $str;
}

function ext_save_state($key, $set_state, $set_info="")
{
	global $data_name;

	if (is_array($set_info))
	{ 
		$set_info = ext_encode_array($set_info);
	}

	$vars = array(
		'chainid'	=> "",
		'dataname'	=> $data_name,
		'info'		=> $set_info,
		'keyname'	=> $key,
		'state'		=> $set_state
	);

	$key_query = mlavu_query("SELECT `id` FROM `poslavu_MAIN_db`.`lavu_gift_status` WHERE `keyname` = '[keyname]' AND `dataname` = '[dataname]'", $vars);
	if (mysqli_num_rows($key_query))
	{
		$key_read = mysqli_fetch_assoc($key_query);
		$vars['id'] = $key_read['id'];
		mlavu_query("UPDATE `poslavu_MAIN_db`.`lavu_gift_status` SET `state` = '[state]', `info` = '[info]' WHERE `id` = '[id]'", $vars);
	}
	else
	{
		mlavu_query("INSERT INTO `poslavu_MAIN_db`.`lavu_gift_status` (`dataname`, `keyname`, `state`, `info`) values ('[dataname]', '[keyname]', '[state]', '[info]')", $vars);
	}
}

function ext_get_saved_state($key, $def="", $col="state")
{
	global $data_name;

	$vars = array(
		'chainid'	=> "",
		'column'	=> $col,
		'dataname'	=> $data_name,
		'keyname'	=> $key
	);

	$key_query = mlavu_query("SELECT `[column]` FROM `poslavu_MAIN_db`.`lavu_gift_status` WHERE `keyname` = '[keyname]' AND `dataname` = '[dataname]'", $vars);
	if (mysqli_num_rows($key_query))
	{
		$key_read = mysqli_fetch_assoc($key_query);
		return $key_read[$col];
	}

	return $def;
}

function ext_get_saved_state_info($key, $info_key, $def="")
{
	$val_str = ext_get_saved_state($key, "", "info");
	if ($val_str != "")
	{
		$val_arr = ext_decode_string($val_str);
		if (isset($val_arr[$info_key]))
		{
			return $val_arr[$info_key];
		}
	}

	return $def;
}

function ext_decode_string($str)
{
	$arr = array();
	$str_parts = explode("&", $str);
	for($n = 0; $n < count($str_parts); $n++)
	{
		$inner_parts = explode("=", $str_parts[$n]);
		if (count($inner_parts) == 2)
		{
			$inner_key = urldecode(trim($inner_parts[0]));
			$inner_val = urldecode(trim($inner_parts[1]));
			$arr[$inner_key] = $inner_val;
		}
	}

	return $arr;
}

function ext_encode_array($arr)
{
	$str = "";
	foreach($arr as $arr_key => $arr_val)
	{
		if ($str!="")
		{ 
			$str .= "&";
		}
		$str .= urlencode($arr_key)."=".urlencode($arr_val);
	}
	return $str;
}