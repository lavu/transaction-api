<?php

require_once(__DIR__."/../../../lib/info_inc.php");
require_once(__DIR__."/../../../lib/NewRelicHelper.php");
require_once(__DIR__."/../../../lib/gateway_functions.php");
require_once('/home/poslavu/public_html/admin/sa_cp/manage_chain_functions.php');

class LavuGiftProcessor
{
	private $debug;
	private $lgp_debug;
	private $req_id;
	private $NewRelic;

	private $dataname;
	private $loc_id;
	private $chain_id;
	private $timezone;

	private $request;
	private $app_name;
	private $app_version;
	private $app_build;
	private $device_id;

	private $transtype;
	private $gift_or_loyalty;
	private $amount;
	private $card_number;

	private $order_id;
	private $ioid;
	private $icid;
	private $item_id;
	private $check;
	private $server_id;
	private $server_name;

	private $card_info;
	private $history_id;
	private $orig_internal_id;

	public function __construct()
	{
		global $NewRelic;

		$this->debug		= FALSE;
		$this->lgp_debug	= FALSE;
		$this->req_id		= urlvar("YIG");
		$this->NewRelic		= $NewRelic;

		$this->order_id		= "";
		$this->ioid			= "";
		$this->icid			= "";
		$this->item_id		= "";
		$this->card_number	= "";
	}

	public function __destruct()
	{
	}

	public function processTransactionRequest($request)
	{
		//$this->testApproval();
		//return;
		$this->request			= $request;
		$this->app_name			= arrayValueForKey($request, "app_name");
		$this->app_version		= arrayValueForKey($request, "app_version");
		$this->app_build		= arrayValueForKey($request, "app_build");
		$this->device_id		= arrayValueForKey($request, "UUID");

		$this->transtype		= arrayValueForKey($request, "transtype");
		$this->gift_or_loyalty	= (strstr($this->transtype, "Gift"))?"Gift":"Loyalty";
		$this->amount			= arrayValueForKey($request, "card_amount");

		$this->order_id			= arrayValueForKey($request, "order_id");
		$this->ioid				= arrayValueForKey($request, "ioid");
		$this->icid				= arrayValueForKey($request, "icid");
		$this->item_id			= arrayValueForKey($request, "item_id");
		$this->check			= arrayValueForKey($request, "check");
		$this->server_id		= arrayValueForKey($request, "server_id");
		$this->server_name		= arrayValueForKey($request, "server_full_name");

		$this->orig_internal_id	= arrayValueForKey($request, "orig_internal_id");
		$this->dataname			= arrayValueForKey($request, "cc");

		if (!empty($this->dataname)) {
			$this->NewRelic->track("dataname", $this->dataname);
		}

		if (!$this->validateConnection())
		{
			return;
		}

		$this->lgpDebug("request: ".json_encode($request));

		switch ($this->transtype)
		{
			case "IssueGiftCard":
			case "IssueLoyaltyCard":

				$this->processIssue();
				break;

			case "ReloadGiftCard":
			case "ReloadLoyaltyCard":

				$this->processReload();
				break;

			case "GiftCardBulkActivate":

				$this->processBulkActivation();
				break;

			case "VoidGiftCardIssueOrReload":
			case "VoidLoyaltyCardIssueOrReload":

				$this->processIssueOrReloadVoid();
				break;

			case "DeactivateGiftCard":
			case "DeactivateLoyaltyCard":

				$this->processDeactivation();
				break;

			case "GiftCardBalance":
			case "LoyaltyCardBalance":

				$this->processBalanceCheck();
				break;

			case "GiftCardSale":
			case "LoyaltyCardSale":

				$this->processSale();
				break;

			case "GiftCardReturn":
			case "LoyaltyCardReturn":

				$this->processReturn();
				break;

			case "GiftCardVoid":
			case "LoyaltyCardVoid":

				$this->processSaleOrReturnVoid();
				break;

			default:

				$this->unknownTranstype();
				break;
		}
	}

// - - - - - - - - - - - - - - - - Validations - - - - - - - - - - - - - - - - //

	private function validateConnection()
	{
		global $data_name;
		global $loc_id;

		$this->dataname = $data_name;
		$this->loc_id	= $loc_id;
		$this->chain_id = $this->getChainID();
		$this->timezone = locationSetting("timezone");

		return TRUE; // temporary

		// if not valid...

		$response = $this->formattedDecline("Invalid Connection", "");

		$this->echoResponse($response);
	}

	private function validateCardData()
	{
		if (!empty($this->card_number))
		{
			return TRUE;
		}

		$req_card_number	= $this->request['card_number'];
		$card_number		= interpret(substr($req_card_number, 1));

		if (empty($card_number))
		{
			// check mag data

			$mag_data		= arrayValueForKey($this->request, "mag_data");
			$card_reader	= arrayValueForKey($this->request, "reader");
			$encrypted		= arrayValueForKey($this->request, "encrypted", "0");

			if ($card_reader == "llsswiper")
			{
				$encrypted = "2";
			}

			$parsedCardInfo = parseCardSwipe($mag_data, strtolower($card_reader), $encrypted);

			$card_number = $parsedCardInfo['number'];
		}

		if (empty($card_number))
		{
			$response = $this->formattedDecline("Missing or Invalid Card Number", "");

			$this->echoResponse($response);

			return;
		}

		$this->card_number = $card_number;

		return TRUE;
	}

	private function validateHistoryID()
	{
		$this->history_id = arrayValueForKey($this->request, "pnref");
		if (empty($this->history_id))
		{
			$response = $this->formattedDecline("Missing History ID", "");

			$this->echoResponse($response);

			return FALSE;
		}

		if ($this->history_id == "NOHIST")
		{
			$response = $this->formattedDecline("Invalid History ID", "");

			$this->echoResponse($response);

			return FALSE;
		}

		return TRUE;
	}

	private function validateAmount()
	{
		if ((float)$this->amount == 0)
		{
			$response = $this->formattedDecline("Invalid Amount", "");

			$this->echoResponse($response);

			return FALSE;
		}

		return TRUE;
	}

// - - - - - - - - - - - - - - - - Processing - - - - - - - - - - - - - - - - //

	private function processIssue()
	{
		if (!$this->validateCardData())
		{
			return;
		}

		if (!$this->validateAmount())
		{
			return;
		}
		#To stop creating duplicate giftcard while issuing gift card
		if ($this->getCardInfo() == "Deactivated") {
			return;
		}
		
		if (empty($this->card_info))
		{
			$this->createCardRecord();
		}
		else
		{
			$this->transtype = str_replace("Issue", "Reload", $this->transtype);

			$this->processReload();
		}
	}

	private function processReload()
	{
		if (!$this->validateCardData())
		{
			return;
		}

		if (!$this->validateAmount())
		{
			return;
		}

		#To stop creating duplicate giftcard while reloading gift card
		if ($this->getCardInfo() == "Deactivated") {
			return;
		}
		if (empty($this->card_info))
		{
			$this->transtype = str_replace("Reload", "Issue", $this->transtype);

			$this->createCardRecord();

			return;
		}

		$add_value_info = $this->addValueToCard(TRUE, "Loaded");

		$history_id = $add_value_info['history_id'];
		$new_amount = $add_value_info['new_amount'];

		$response = $this->formattedApproval($history_id, "Approval", $this->amount, $new_amount, "", time());

		$this->echoResponse($response);
	}

	private function processBulkActivation()
	{

	}

	private function processIssueOrReloadVoid()
	{
		if (!$this->validateHistoryID())
		{
			return;
		}

		$history_info = $this->getHistoryInfo();
		if (empty($history_info))
		{
			return;
		}

		$issue_or_reload = "";

		switch ($history_info['action'])
		{
			case "Created":

				$issue_or_reload = "Issue";
				break;

			case "Loaded":

				$issue_or_reload = "Reload";
				break;

			default:
				break;
		}

		if (empty($issue_or_reload))
		{
			$this->getHistoryInfoFailed("transtype_mismatch");

			return;
		}

		$this->getCardInfo($history_info['card_id']);
		if (empty($this->card_info))
		{
			return;
		}

		$amount_key = $this->isGift()?"balance_amount":"point_amount";
		$this->amount = (float)$history_info[$amount_key];

		$balance = (float)$this->card_info[$this->isGift()?'balance':'points'];
		if ($balance < $this->amount)
		{
			$borp = $this->isGift()?"balance":"points";

			$response = $this->formattedDecline("Unable to Void ".$issue_or_reload, "The card does not have the sufficient ".$borp." to allow this ".$issue_or_reload." to be voided.");

			$this->echoResponse($response);

			return;
		}

		$subtract_value_info = $this->subtractValueFromCard("Voided ".$issue_or_reload, $history_info['id']);

		$history_id		= $subtract_value_info['history_id'];
		$approval		= $subtract_value_info['approval'];
		$minus_amount	= $subtract_value_info['minus_amount'];
		$new_amount		= $subtract_value_info['new_amount'];

		$update_orginal_history = mlavu_query("UPDATE `lavu_gift_history` SET `status` = 'voided' WHERE `id` = '[id]'", $history_info);
		if (!$update_orginal_history)
		{
			$this->lgpDebug(__FUNCTION__." - query failed - ".mysqli_error());
		}

		$response = $this->formattedApproval($history_id, $approval, $minus_amount, $new_amount, "", time());

		$this->echoResponse($response);
	}

	private function processDeactivation()
	{
		if (!$this->validateCardData())
		{
			return;
		}

		$this->getCardInfo();
		if (empty($this->card_info))
		{
			return;
		}

		$card_id = $this->card_info['id'];

		$update_card_record = mlavu_query("UPDATE `lavu_gift_cards` SET `inactive` = '1' WHERE `id` = '[1]'", $card_id);
		if (!$update_card_record)
		{
			$response = $this->formattedDecline("Deactivation Failed", "Database error.");

			$this->echoResponse($response);

			return;
		}

		$utc_datetime = UTCDateTime(TRUE);
		$local_datetime = DateTimeForTimeZone($this->timezone);

		$hist_vars = array(
			'action'			=> "Deactivated",
			'balance_amount'	=> "",
			'card_id'			=> $card_id,
			'datetime'			=> $local_datetime,
			'item_id_list'		=> "",
			'new_balance'		=> "",
			'new_points'		=> "",
			'point_amount'		=> "",
			'previous_balance'	=> "",
			'previous_points'	=> "",
			'utc_time'			=> $utc_datetime
		);

		$this->order_id	= "";
		$this->ioid		= "";

		$history_id	= $this->writeToHistory($hist_vars);

		$log_action = "Lavu ".$this->gift_or_loyalty." Card Deactivated";

		$log_details = "Last Four: ".$this->cardLastFour();

		$this->writeToActionLog($log_action, $log_details, $local_datetime, $utc_datetime);

		$response = $this->formattedApproval($history_id, "Approval", "");

		$this->echoResponse($response);
	}

	private function processBalanceCheck()
	{
		if (!$this->validateCardData())
		{
			return;
		}

		$this->getCardInfo();
		if (empty($this->card_info))
		{
			return;
		}

		$balance = $this->card_info[$this->isGift()?'balance':'points'];

		$response = $this->formattedApproval($this->card_info['id'], "Approval", $balance, $balance);
		if ($this->transtype == 'GiftCardBalance') {
			$ext_status = toGetExtensionStatus('extensions.payment.lavu.+loyalty', $this->dataname);
			$response= explode("|", $response);
			if ($ext_status == 'enable') {
				$response[7] = ($this->card_info[points] == '[1]') ? '0' : $this->card_info[points]; // Observed that in DB '[1]' got insert instead of 0. So here we are checking if points is [1] then replace it to 0.
			} else {
				$response[7] = '';
			}
			$response = implode("|", $response);
		}

		$this->echoResponse($response);
	}

	private function processSale()
	{
		if (!$this->validateCardData())
		{
			return;
		}

		if (!$this->validateAmount())
		{
			return;
		}

		$this->getCardInfo();
		if (empty($this->card_info))
		{
			return;
		}

		$balance = (float)$this->card_info[$this->isGift()?'balance':'points'];
		if ($balance <= 0)
		{
			$message = $this->isGift()?"This card currently has no value.":"This card currently has no points.";
			$response = $this->formattedDecline("Declined", $message);

			$this->echoResponse($response);

			return;
		}

		$subtract_value_info = $this->subtractValueFromCard("Used");

		$history_id		= $subtract_value_info['history_id'];
		$approval		= $subtract_value_info['approval'];
		$minus_amount	= $subtract_value_info['minus_amount'];
		$new_amount		= $subtract_value_info['new_amount'];

		$response = $this->formattedApproval($history_id, $approval, $minus_amount, $new_amount, "", time());

		$this->echoResponse($response);
	}

	private function processReturn()
	{
		if (!$this->validateHistoryID())
		{
			return;
		}

		if (!$this->validateAmount())
		{
			return;
		}

		$history_info = $this->getHistoryInfo();
		if (empty($history_info))
		{
			return;
		}

		$history_status		= $history_info['status'];
		$previous_refund	= 0;

		if ($history_status == "voided")
		{
			$response = $this->formattedDecline("Unable to Refund", "This transaction has already been voided.");

			$this->echoResponse($response);

			return;
		}
		else if (substr($history_status, 0, 9) == "refunded:")
		{
			$refund_info		= explode(":", $history_status);
			$previous_refund	= (float)$refund_info[1];
			$amount_key			= $this->isGift()?"balance_amount":"point_amount";
			$full_amount		= (float)$history_info[$amount_key];

			if (($previous_refund + (float)$this->amount) > $full_amount)
			{
				$response = $this->formattedDecline("Refund Not Allowed", "Amount entered exceeds the remaining refundable amount for this transaction.");

				$this->echoResponse($response);

				return;
			}
		}

		$this->getCardInfo($history_info['card_id']);
		if (empty($this->card_info))
		{
			return;
		}

		$new_status = "refunded:".($previous_refund + (float)$this->amount);

		$update_orginal_history = mlavu_query("UPDATE `poslavu_MAIN_db`.`lavu_gift_history` SET `status` = '[1]' WHERE `id` = '[2]'", $new_status, $this->history_id);
		if (!$update_orginal_history)
		{
			$this->lgpDebug(__FUNCTION__." - query failed - ".mysqli_error());
		}

		$add_value_info = $this->addValueToCard(FALSE, "Refunded", $this->history_id);

		$history_id = $add_value_info['history_id'];
		$new_amount = $add_value_info['new_amount'];

		$response = $this->formattedApproval($history_id, "Approval", $this->amount, $new_amount, "", time());

		$this->echoResponse($response);
	}

	private function processSaleOrReturnVoid()
	{
		if (!$this->validateHistoryID())
		{
			return;
		}

		$history_info = $this->getHistoryInfo();
		if (empty($history_info))
		{
			return;
		}

		$history_action = $history_info['action'];
		$history_status = $history_info['status'];

		if ($history_status=="voided" || substr($history_status, 0, 9)=="refunded:")
		{
			$message = ($history_status == "voided")?"This transaction has already been voided.":"A refund has already been processed for this transaction.";

			$response = $this->formattedDecline("Unable to Void", $message);

			$this->echoResponse($response);

			return;
		}

		$this->getCardInfo($history_info['card_id']);
		if (empty($this->card_info))
		{
			return;
		}

		$balance = (float)$this->card_info[$this->isGift()?'balance':'points'];
		if (($balance < (float)$this->amount) && $history_action=="Refunded")
		{
			$borp = $this->isGift()?"balance":"points";

			$response = $this->formattedDecline("Unable to Void", "The card does not have the sufficient ".$borp." to allow this refund to be voided.");

			$this->echoResponse($response);

			return;
		}

		$update_orginal_history = mlavu_query("UPDATE `poslavu_MAIN_db`.`lavu_gift_history` SET `status` = 'voided' WHERE `id` = '[1]'", $this->history_id);
		if (!$update_orginal_history)
		{
			$this->lgpDebug(__FUNCTION__." - query failed - ".mysqli_error());
		}

		if ($history_action == "Refunded")
		{
			$value_change_info = $this->subtractValueFromCard("Refund Voided", $this->history_id);
		}
		else
		{
			$value_change_info = $this->addValueToCard(FALSE, "Payment Voided", $this->history_id);
		}

		$history_id = $value_change_info['history_id'];
		$new_amount = $value_change_info['new_amount'];

		$response = $this->formattedApproval($history_id, "Approval", $this->amount, $new_amount, "", time());

		$this->echoResponse($response);
	}

	private function unknownTranstype()
	{
		$response = $this->formattedDecline("Invalid Transtype", "Unable to process this type of transaction: ".$this->transtype);

		$this->echoResponse($response);
	}

	private function getCardInfo($card_id=FALSE)
	{
		if (!empty($this->card_info))
		{
			return $this->card_info;
		}

		$primary_filter = $this->card_number;
		$where_clause = "`name` = '[1]' AND `deleted` = 0 ";

		if ($card_id){
			$primary_filter = $card_id;
			$where_clause = "`id` = '[1]'";
		}else{
			if ( !empty($this->chain_id)) {
				#load all dataname existing in chain
				$chainedDataname = array();
				$chainedRestaurants =get_restaurants_by_chain($this->chain_id );
				foreach($chainedRestaurants as $restaurants) {
					$chainedDataname[] = $restaurants['data_name'];
				}
				$where_clause .= " AND (`dataname`IN('".implode("','", $chainedDataname)."')  OR `chainid` = '[3]'";
			}else {
				$where_clause .= " AND (`dataname` = '[2]'";
			}
			$where_clause .= ")";
		}

		$get_card_info = mlavu_query("SELECT `balance`, `expires_ts`, `id`, `inactive`, `name`, `points` FROM `poslavu_MAIN_db`.`lavu_gift_cards` WHERE ".$where_clause, $primary_filter, $this->dataname, $this->chain_id);
		if (!$get_card_info)
		{
			$this->lgpDebug(__FUNCTION__." - query failed - ".mysqli_error());

			if (substr($this->transtype, 0, 5) != "Issue")
			{
				$this->getCardInfoFailed($card_id, "query_failed");
			}

			return NULL;
		}

		if (mysqli_num_rows($get_card_info) == 0)
		{
			$this->lgpDebug(__FUNCTION__." - no record");

			if (substr($this->transtype, 0, 5)!="Issue" && substr($this->transtype, 0, 6)!="Reload")
			{
				$this->getCardInfoFailed($card_id, "no_record");
			}

			return NULL;
		}

		$card_info = mysqli_fetch_assoc($get_card_info);

		if ($card_info['inactive'] == "1")
		{
			$this->getCardInfoFailed($card_id, "inactive", $card_info);

			return "Deactivated";
		}

		// check expiration and such?

		$this->card_info = $card_info;

		if ($card_id)
		{
			$this->card_number = $card_info['name'];
		}
	}

	private function getCardInfoFailed($card_id, $error, $card_info=FALSE)
	{
		$card_identifier = "number ".$this->card_number;
		if ($card_id)
		{
			$card_identifier = "id ".$card_id;
		}

		$title		= "Card Error";
		$message	= "Unable to retrieve record for card ".$card_identifier.".";

		switch ($error)
		{
			case "no_record":

				$title = "Card Not Found";
				break;

			case "inactive":

				$title = "Card is Deactivated";
				$message = $this->cardDeactivatedInfo($card_info);
				break;

			default:
				break;
		}

		$response = $this->formattedDecline($title, $message);

		$this->echoResponse($response);
	}

	private function cardDeactivatedInfo($card_info)
	{
		$card_number = $card_info['name'];

		if (empty($this->timezone))
		{
			return $card_number;
		}

		$get_history_info = mlavu_query("SELECT `utc_time` FROM `lavu_gift_history` WHERE `action` = 'Deactivated' AND `card_id` = '[1]' ORDER BY `id`", $card_info['id']);
		if (!$get_history_info || (mysqli_num_rows($get_history_info) <= 0))
		{
			return $card_number;
		}

		$history_info = mysqli_fetch_assoc($get_history_info);
		if (empty($history_info['utc_time']))
		{
			return $card_number;
		}

		$unix_ts = strtotime($history_info['utc_time']);
		$local_datetime = DateTimeForTimeZone($this->timezone, $unix_ts);

		$datetime_parts = explode(" ", $local_datetime);
		$local_date = $datetime_parts[0];
		$local_time = $datetime_parts[1];

		return "Card number ".$card_number." was deactivated on ".$local_date." at ".$local_time.".";
	}

	private function createCardRecord()
	{
		$balance = $this->isGift()?(float)$this->amount:"0";
		$points = $this->isGift()?"0":(int)$this->amount;

		$created_ts = time();
		$created_datetime = DateTimeForTimeZone($this->timezone, $create_ts);

		$expires_ts	= $this->generate_expiration_ts($created_ts);
		$expires	= ($expires_ts > 0)?DateTimeForTimeZone($this->timezone, $expires_ts):"";

		$vars = array(
			'balance'			=> $balance,
			'chainid'			=> $this->chain_id,
			'created_datetime'	=> $created_datetime,
			'created_ts'		=> $created_ts,
			'dataname'			=> $this->dataname,
			'expires'			=> $expires,
			'expires_ts'		=> $expires_ts,
			'name'				=> $this->card_number,
			'points'			=> $points
		);

		$fields = "";
		$values = "";

		buildInsertFieldsAndValues($vars, $fields, $values);

		$success = mlavu_query("INSERT INTO `poslavu_MAIN_db`.`lavu_gift_cards` (".$fields.") VALUES (".$values.")", $vars);
		if (!$success)
		{
			$response = $this->formattedDecline("Card Error", "Unable to create record for card number ".$this->card_number.".");

			$this->echoResponse($response);

			return;
		}

		$utc_datetime = UTCDateTime(TRUE, $created_ts);

		$hist_vars = array(
			'action'			=> "Created",
			'balance_amount'	=> $balance,
			'card_id'			=> mlavu_insert_id(),
			'datetime'			=> $created_datetime,
			'item_id_list'		=> $this->item_id,
			'new_balance'		=> $balance,
			'new_points'		=> $points,
			'point_amount'		=> $points,
			'previous_balance'	=> "0",
			'previous_points'	=> "0",
			'utc_time'			=> $utc_datetime
		);

		$history_id	= $this->writeToHistory($hist_vars);

		$log_action = "Lavu ".$this->gift_or_loyalty." Card Issued";

		$log_details = "Last Four: ".$this->cardLastFour();
		$log_details .= " - Amount: ".$this->formattedAmount($this->amount);
		if (!empty($this->check))
		{
			$log_details = "Check ".$this->check." - ".$log_details;
		}

		$this->writeToActionLog($log_action, $log_details, $created_datetime, $utc_datetime);

		$response = $this->formattedApproval($history_id, "Approval", $this->amount, $this->amount, "", time());

		$this->echoResponse($response);
	}

	private function addValueToCard($reset_expiration, $history_action, $ref_history_id="")
	{
		$now_ts	= time();
		$vars	= array();

		if ($reset_expiration)
		{
			$expires_ts	= $this->generate_expiration_ts($now_ts);
			$expires	= ($expires_ts > 0)?DateTimeForTimeZone($this->timezone, $expires_ts):"";

			$vars['expires']	= $expires;
			$vars['expires_ts']	= $expires_ts;
		}

		$prev_balance	= (float)$this->card_info['balance'];
		$new_balance	= $prev_balance;
		$add_balance	= 0;

		$prev_points	= (int)$this->card_info['points'];
		$new_points		= $prev_points;
		$add_points		= 0;

		$new_amount		= 0;

		if ($this->isGift())
		{
			$add_balance = (float)$this->amount;
			$new_balance += $add_balance;
			$new_amount = $new_balance;

			$vars['balance'] = $new_balance;
		}
		else
		{
			$add_points = (int)$this->amount;
			$new_points += $add_points;
			$new_amount = $new_points;

			$vars['points'] = $new_points;
		}

		$update = "";

		buildUpdate($vars, $update);

		$vars['id'] = $this->card_info['id'];

		$success = mlavu_query("UPDATE `poslavu_MAIN_db`.`lavu_gift_cards` SET ".$update." WHERE `id` = '[id]'", $vars);
		if (!$success)
		{
			$response = $this->formattedDecline("Card Error", "Unable to update record for card number ".$this->card_number.".");

			$this->echoResponse($response);

			return;
		}

		$local_datetime = DateTimeForTimeZone($this->timezone, $now_ts);
		$utc_datetime = UTCDateTime(TRUE, $now_ts);

		$hist_vars = array(
			'action'			=> $history_action,
			'balance_amount'	=> $add_balance,
			'card_id'			=> $this->card_info['id'],
			'datetime'			=> $local_datetime,
			'item_id_list'		=> $this->item_id,
			'new_balance'		=> $new_balance,
			'new_points'		=> $new_points,
			'point_amount'		=> $add_points,
			'previous_balance'	=> $prev_balance,
			'previous_points'	=> $prev_points,
			'ref_history_id'	=> $ref_history_id,
			'utc_time'			=> $utc_datetime
		);

		$history_id	= $this->writeToHistory($hist_vars);

		$log_action =  "Lavu ".$this->gift_or_loyalty." Card Reloaded";

		if ($history_action != "Loaded")
		{
			$log_action = str_replace("Reloaded", $history_action, $log_action);
		}

		$log_details = "Last Four: ".$this->cardLastFour();
		$log_details .= " - Amount: ".$this->formattedAmount($this->amount);
		$log_details .= " - New Balance: ".$this->formattedAmount($new_amount);
		if (!empty($this->check))
		{
			$log_details = "Check ".$this->check." - ".$log_details;
		}

		$this->writeToActionLog($log_action, $log_details, $local_datetime, $utc_datetime);

		return array(
			'history_id' => $history_id,
			'new_amount' => $new_amount
		);
	}

	private function subtractValueFromCard($history_action, $ref_history_id="")
	{
		$prev_balance	= (float)$this->card_info['balance'];
		$new_balance	= $prev_balance;
		$minus_balance	= 0;

		$prev_points	= (int)$this->card_info['points'];
		$new_points		= $prev_points;
		$minus_points	= 0;

		$minus_amount	= 0;
		$new_amount		= 0;

		$vars = array();

		if ($this->isGift())
		{
			$minus_balance = min((float)$this->amount, $prev_balance);
			$minus_amount = $minus_balance;
			$new_balance -= $minus_balance;
			$new_amount = $new_balance;

			$vars['balance'] = $new_balance;
		}
		else
		{
			$minus_points = min((int)$this->amount, $prev_points);
			$minus_amount = $minus_points;
			$new_points -= $minus_points;
			$new_amount = $new_points;

			$vars['points'] = $new_points;
		}

		$update = "";

		buildUpdate($vars, $update);

		$vars['id'] = $this->card_info['id'];

		$success = mlavu_query("UPDATE `poslavu_MAIN_db`.`lavu_gift_cards` SET ".$update." WHERE `id` = '[id]'", $vars);
		if (!$success)
		{
			$response = $this->formattedDecline("Card Error", "Unable to update record for card number ".$this->card_number.".");

			$this->echoResponse($response);

			return;
		}

		$local_datetime = DateTimeForTimeZone($this->timezone, $now_ts);
		$utc_datetime = UTCDateTime(TRUE, $now_ts);

		$hist_vars = array(
			'action'			=> $history_action,
			'balance_amount'	=> $minus_balance,
			'card_id'			=> $this->card_info['id'],
			'datetime'			=> $local_datetime,
			'item_id_list'		=> "", // Should potentially have the app post this in the Issue/Reload Request
			'new_balance'		=> $new_balance,
			'new_points'		=> $new_points,
			'point_amount'		=> $minus_points,
			'previous_balance'	=> $prev_balance,
			'previous_points'	=> $prev_points,
			'ref_history_id'	=> $ref_history_id,
			'utc_time'			=> $utc_datetime
		);

		$history_id	= $this->writeToHistory($hist_vars);

		$log_action = ($history_action == "Used")?"Payment Applied":$history_action;

		$log_details = "";

		if ($history_action=="Voided Issue" || $history_action=="Voided Reload")
		{
			$issue_or_reload = ($history_action == "Voided Issue")?"Issue":"Reload";
			$log_action = "Lavu ".$this->gift_or_loyalty." Card ".$issue_or_reload." Voided";
		}
		else
		{
			$log_details = "Lavu ".$this->gift_or_loyalty. " - ";
		}

		$log_details .= "Last Four: ".$this->cardLastFour();
		$log_details .= " - Amount: ".$this->formattedAmount($minus_amount);
		$log_details .= " - New Balance: ".$this->formattedAmount($new_amount);
		if (!empty($this->check))
		{
			$log_details = "Check ".$this->check." - ".$log_details;
		}

		$this->writeToActionLog($log_action, $log_details, $local_datetime, $utc_datetime);

		return array(
			'approval'		=> ((float)$minus_amount < (float)$this->amount)?"Partial Approval":"Approval",
			'history_id'	=> $history_id,
			'minus_amount'	=> $minus_amount,
			'new_amount'	=> $new_amount
		);
	}

// - - - - - - - - - - - - - - - - History and Logging - - - - - - - - - - - - - - - - //

	private function getHistoryInfo()
	{
		$get_history_info = mlavu_query("SELECT `action`, `balance_amount`, `card_id`, `chainid`, `dataname`, `id`, `point_amount`, `status` FROM `poslavu_MAIN_db`.`lavu_gift_history` WHERE `id` = '[1]'", $this->history_id);
		if (!$get_history_info)
		{
			$this->lgpDebug(__FUNCTION__." - query failed - ".mysqli_error());

			$this->getHistoryInfoFailed("query_failed");

			return NULL;
		}

		if (mysqli_num_rows($get_history_info) == 0)
		{
			$this->lgpDebug(__FUNCTION__." - no record - ".$this->history_id);

			$this->getHistoryInfoFailed("no_record");

			return NULL;
		}

		$history_info = mysqli_fetch_assoc($get_history_info);

		if ($history_info['chainid']!=$this->chain_id && $history_info['dataname']!=$this->dataname)
		{
			$this->getHistoryInfoFailed("location_mismatch");

			return NULL;
		}

		if ($history_info['status'] == "voided")
		{
			$this->lgpDebug(__FUNCTION__." - voided - ".$this->history_id);

			$this->getHistoryInfoFailed("voided");

			return NULL;
		}

		return $history_info;
	}

	private function getHistoryInfoFailed($error)
	{
		$title = "History Error";
		$message = "Unable to retrieve record for history ID ".$this->history_id.".";

		switch ($error)
		{
			case "location_mismatch":

				$message = "The history ID provided doesn't match any records for this merchant account.";
				break;

			case "no_record":

				$title = "History Record Not Found";
				break;

			case "transtype_mismatch":

				$message = "The history ID provided points to an unexpected transaction type.";
				break;

			case "voided":

				$message = "This transaction has been previously voided.";
				break;

			default:
				break;
		}

		$response = $this->formattedDecline($title, $message);

		$this->echoResponse($response);
	}

	private function writeToHistory($hist_vars)
	{
		$vars = array_merge( $hist_vars, array(
			'chainid'			=> $this->chain_id,
			'dataname'			=> $this->dataname,
			'gift_or_loyalty'	=> $this->gift_or_loyalty,
			'icid'				=> $this->icid,
			'ioid'				=> $this->ioid,
			'name'				=> $this->card_number,
			'order_id'			=> $this->order_id,
			'server_id'			=> $this->server_id,
			'server_name'		=> $this->server_name
		));

		$fields = "";
		$values = "";

		buildInsertFieldsAndValues($vars, $fields, $values);

		$success = mlavu_query("INSERT INTO `poslavu_MAIN_db`.`lavu_gift_history` (".$fields.") VALUES (".$values.")", $vars);
		if (!$success)
		{
			$this->lgpDebug(__FUNCTION__." - query failed - ".mysqli_error());

			return "NOHIST";
		}

		$history_id = mlavu_insert_id();

		if (empty($history_id))
		{
			return "NOHIST";
		}

		return $history_id;
	}

	private function writeToActionLog($action, $details, $local_datetime, $utc_datetime)
	{
		$host = $_SERVER['HTTP_HOST'];

		$details_short = array();
		if (!empty($this->app_name))
		{
			$details_short[] = $this->app_name;
		}
		if (!empty($this->app_version))
		{
			$details_short[] = $this->app_version;
		}
		if (!empty($this->app_build))
		{
			$details_short[] = "(".$this->app_build.")";
		}
		if (!empty($host))
		{
			$details_short[] = "via ".$host;
		}

		$vars = array(
			'action'		=> $action,
			'check'			=> $this->check,
			'details'		=> $details,
			'details_short'	=> implode(" ", $details_short),
			'device_udid'	=> $this->device_id,
			'ialid'			=> newInternalID(),
			'ioid'			=> $this->ioid,
			'loc_id'		=> $this->loc_id,
			'order_id'		=> (string)$this->order_id,
			'server_time'	=> $utc_datetime,
			'time'			=> $local_datetime,
			'user'			=> $this->server_name,
			'user_id'		=> $this->server_id
		);

		if ($vars['order_id'] == "0")
		{
			$vars['order_id'] = "";
		}

		$fields = "";
		$values = "";

		buildInsertFieldsAndValues($vars, $fields, $values);

		$success = lavu_query("INSERT INTO `action_log` (".$fields.") VALUES (".$values.")", $vars);
		if (!$success)
		{
			$this->lgpDebug(__FUNCTION__." - query failed - ".mysqli_error());
		}
	}

// - - - - - - - - - - - - - - - - Response - - - - - - - - - - - - - - - - //

	private function testApproval()
	{
		$transtype	= $_REQUEST['transtype'];
		$amount		= (strstr($transtype, "Balance"))?"88":$REQUEST['amount'];

		$this->gift_or_loyalty	= strstr($transtype, "Gift")?"Gift":"Loyalty";
		$this->order_id			= $_REQUEST['order_id'];
		$this->card_number		= "12345288";

		$response = $this->formattedApproval("HISTID", "Approval", $amount, "8800", "TESTMSG2", time());

		$this->echoResponse($response);
	}

	/**
	 * Generate pipe-delimited response string to communicate the details related to an approved
	 * transaction request for interpretation by the Lavu POS app.
	 *
	 * @param string $history_id Row ID of the associated history record in `poslavu_MAIN_db`.`lavu_gift_history` destined to be recorded in a transaction record's `transaction_id` field.
	 * @param string $msg1 Approval message, such as "Approval" or "Partial Approval".
	 * @param string $card_type "Lavu Gift" or "Lavu Loyalty".
	 * @param string $amount Approved amount for Issue, Reload, Sale, Refund, and Void transactions.
	 * @param string $new_balance (optional) Current balance of a given gift or loyalty card balance checks, or the new balance of a card following an Issue, Reload, Sale, Refund, or Void.
	 * @param string $msg2 (optional) Additional message that can be added to a gift or loyalty item on an order following an Issue or Reload transaction.
	 * @param string $last_mod_ts (optional) Unix timestamp denoting the second at which a given transaction occurred. This value will be used to populated the `last_mod_ts` field of the associated order and transaction records and is key in ensuring proper syncing, especially as it pertains to secondary transactions such as Voids.
	 */
	private function formattedApproval($history_id, $msg1, $amount, $new_balance="", $msg2="", $last_mod_ts="")
	{
		$rtn = array(
			"1",
			$history_id,
			$this->cardLastFour(),
			$msg1,
			"", // This element contains the auth_code for integrated card transactions and is not relevent for Lavu Gift and Loyalty.
			"Lavu ".$this->gift_or_loyalty,
			"", // This element contains the record_no for integrated card transactions and is not relevent for Lavu Gift and Loyalty.
			$amount,
			$this->order_id,
			"", // This element contains the ref_data for integrated card transactions and is not relevent for Lavu Gift and Loyalty.
			$this->isGift()?"lavu_gift":"lavu_loyatly", // 10
			$new_balance,
			$msg2,
			$this->cardFirstFour(),
			$last_mod_ts
		);

		return implode("|", $rtn);
	}

	/**
	 * Generate pipe-delimited response string to communicate the details related to a declined
	 * transaction request for interpretation by the Lavu POS app.
	 *
	 * @param string $title Text that will appear as the title of the alert shown to the user.
	 * @param string $msg Text that will appear in the body of the alert shown to the user.
	 * @param string $history_id (optional) Row ID of the associated history record in `poslavu_MAIN_db`.`lavu_gift_history`.
	 * @param string $msg2 (optional) Additional message that may be used to facilitate development and testing.
	 */
	private function formattedDecline($title, $msg, $history_id="", $msg2="")
	{
		$rtn = array(
			"0",
			$msg,
			$history_id,
			$this->order_id,
			$msg2,
			$title
		);

		return implode("|", $rtn);
	}

	/**
	 * Echo the result of a given transaction request back to the Lavu POS app. Should be
	 * a pipe-delimited response generated by formattedApproval() or formattedDecline().
	 *
	 * @param string $response Text response to send back to the app.
	 */
	public function echoResponse($response)
	{
		$this->lgpDebug("response: ".$response);

		echo $response;
	}

// - - - - - - - - - - - - - - - - Utility - - - - - - - - - - - - - - - - //

	private function getChainID()
	{
		$account_query = mlavu_query("SELECT `chain_id` FROM `poslavu_MAIN_db`.`restaurants` WHERE `data_name` = '[1]'", $this->dataname);
		if (!$account_query)
		{
			return "";
		}

		if (mysqli_num_rows($account_query) == 0)
		{
			return "";
		}

		$account_info = mysqli_fetch_assoc($account_query);

		return $account_info['chain_id'];
	}

	private function cardFirstFour()
	{
		return substr($this->card_number, 0, 4);
	}

	private function cardLastFour()
	{
		return substr($this->card_number, -4);
	}

	private function isGift()
	{
		return ($this->gift_or_loyalty == "Gift");
	}

	private function formattedAmount($amount)
	{
		if ($this->isGift())
		{
			$decimal_places	= (int)locationSetting("disable_decimal", "2");
			$decimal_char	= locationSetting("decimal_char", ".");
			$thousands_char	= ($decimal_char == ".")?",":".";
			$symbol			= locationSetting("monitary_symbol", "");
			$left_or_right	= locationSetting("left_or_right", "left");

			$formatted = number_format((float)str_replace(",", "", $amount), $decimal_places, $decimal_char, $thousands_char);

			if (empty($symbol))
			{
				return $formatted;
			}
			else if ($left_or_right == "right")
			{
				return trim($formatted." ".$symbol);
			}
			else
			{
				return trim($symbol." ".$formatted);
			}
		}
		else
		{
			return (int)$amount." points";
		}
	}

	private function generate_expiration_ts($created_ts)
	{
		$expiration_setting = locationSetting("loyalty_gift_expiration", "");

		// Never expires
		if (empty($expiration_setting))
		{
			return 0;
		}

		$exp_set_parts = explode(".", $expiration_setting);
		if (count($exp_set_parts) < 2)
		{
			return 0;
		}

		$interval_type	= $exp_set_parts[0];
		$interval_value	= (int)$exp_set_parts[1];
		$exp_interval	= 0;
		$one_day		= (60 * 60 * 24);

		switch ($interval_type)
		{
			case "d":

				$exp_interval = ($one_day * $interval_value);
				break;

			case "y":

				$exp_interval = ($one_day * 365 * $interval_value);
				// What if a leap year falls within the upcoming time interval?
				break;

			default:
				break;
		}

		// Only apply exp_interval if not less than 30 days out
		if ($exp_interval < ($one_day * 30))
		{
			return 0;
		}

		return ($created_ts + $exp_interval);
	}

	/**
	 * Print logging message if debug member is set to TRUE.
	 *
	 * @param string $msg Message to log.
	 */
	private function logDebug($msg)
	{
		if ($this->debug)
		{
			error_log($msg);
		}
	}

	/**
	 * Print logging message if debug member or lgp_debug member is set to TRUE.
	 *
	 * @param string $msg Message to log.
	 */
	private function lgpDebug($msg)
	{
		if ($this->lgp_debug || $this->debug)
		{
			error_log("LGP - ".$this->dataname." - req_id: ".$this->req_id." - ".$msg);
		}
	}
}