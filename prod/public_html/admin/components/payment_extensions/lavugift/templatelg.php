<?php
	ob_start();
	global $using_new_version_gift;
?><!DOCTYPE html>
<html>

	<head>

		<link rel='stylesheet' href='lavugift.css' type='text/css' charset='utf-8'>

		<style type='text/css'>

			.inner_div1
			{
				position: absolute;
				left: 0px;
				top: 0px;
				width: 280px;
				height:412px;
				display: none;
			}

			.inner_div2
			{
				position: absolute;
				left: 0px;
				top: 45px;
				width: 280px;
				height: 367px;
				display: none;
			}

		</style>

   		<script src="../webextjs.js"></script>
        <?php require_once(__DIR__."/templatescript.php"); ?>
	</head>

	<body onload='initialize(); startListening();'>

		<div style='position:absolute; left:0px; top:0px; width:318px; height:460px; opacity:0.99;'>

			<img style='position:absolute; left:0px; top:0px; width:17px; height:16px;' src='images/popup_upper_left.png'>
			<img style='position:absolute; left:17px; top:0px; width:280px; height:16px;' src='images/popup_upper.png'>
			<img style='position:absolute; left:297px; top:0px; width:21px; height:16px;' src='images/popup_upper_right.png'>
			<img style='position:absolute; left:0px; top:16px; width:17px; height:412px;' src='images/popup_left.png'>
			<img style='position:absolute; left:297px; top:16px; width:21px; height:412px;' src='images/popup_right.png'>
			<img style='position:absolute; left:0px; top:428px; width:17px; height:22px;' src='images/popup_lower_left.png'>
			<img style='position:absolute; left:17px; top:428px; width:280px; height:22px;' src='images/popup_lower.png'>
			<img style='position:absolute; left:297px; top:428px; width:21px; height:22px;' src='images/popup_lower_right.png'>

			<div style='position:absolute; left:17px; top:16px; width:280px; height:412px; background-color:#F9F9F9;'>

				<div id='enter_card_number' class='inner_div2'>

					<table cellspacing='1' cellpadding='3' width='280px'>
						<tr><td class='msg_txt1' align='center'>&nbsp;</td></tr>
					</table>

					<div style='position:absolute; left:0px; top:45px; width:280px; height:40px;'>
						<center><input id='card_number_input' class='input_long' type='tel' value='' READONLY></center>
					</div>

					<div id='number_pad' style='position:absolute; left:0px; top:95px; width:278px; height:250px;'>
						<center>
							<table cellspacing='2' cellpadding='0'>
								<tr>
									<td width='1px'></td>
									<td class='btn_number_pad' ontouchstart='numberPadButtonPressed("7"); return false;'>7</td>
									<td class='btn_number_pad' ontouchstart='numberPadButtonPressed("8"); return false;'>8</td>
									<td class='btn_number_pad' ontouchstart='numberPadButtonPressed("9"); return false;'>9</td>
								</tr>
								<tr>
									<td width='1px'></td>
									<td class='btn_number_pad' ontouchstart='numberPadButtonPressed("4"); return false;'>4</td>
									<td class='btn_number_pad' ontouchstart='numberPadButtonPressed("5"); return false;'>5</td>
									<td class='btn_number_pad' ontouchstart='numberPadButtonPressed("6"); return false;'>6</td>
								</tr>
								<tr>
									<td width='1px'></td>
									<td class='btn_number_pad' ontouchstart='numberPadButtonPressed("1"); return false;'>1</td>
									<td class='btn_number_pad' ontouchstart='numberPadButtonPressed("2"); return false;'>2</td>
									<td class='btn_number_pad' ontouchstart='numberPadButtonPressed("3"); return false;'>3</td>
								</tr>
								<tr>
									<td colspan='4'>
										<table cellspacing='0' cellpadding='0'>
											<tr>
												<td width='1px'></td>
												<td class='btn_number_pad_wide' ontouchstart='numberPadButtonPressed("back"); return false;' style='padding-right: 3px;'><img style='padding:3px 0px 0px 0px;' src='images/btn_cc_back.png'></td>
												<td class='btn_number_pad_wide' ontouchstart='numberPadButtonPressed("0"); return false;'>0</td>
											</tr>
										</table>
									</td>
								</tr>
								<tr>
									<td colspan='4' align='center' style='padding:6px 0px 0px 0px;'>
										<button class='btn_light' ontouchstart='submitCardNumber();'><b>Submit</b></button>
									</td>
								</tr>
							</table>
						</center>
					</div>

				</div>

				<div id='response_panel' class='inner_div2'>
					<center>
						<table style='width:280px; height:367px;'>
							<tr>
								<td align='center' valign='bottom' style='height:137px; padding: 6px 8px 16px 8px;'>
									<span id='response_title' class='msg_txt2'></span><br><br><span id='response_text' class='msg_txt1'></span>
								</td>
							</tr>
							<tr><td align='center' valign='top' style='height:230px; padding-top:6px;'><button class='btn_light' ontouchstart='initiateAction();'><b>Retry</b></button></td></tr>
						</table>
					</center>
				</div>
		<?php
			if(isset($_GET['order_info'])){
					$top_header='-20px';
			} else {
					$top_header='5px';
			}
		?>
                <div style='position:absolute; left:0px; top:<?php echo $top_header; ?>;'>
<?php

$lg_template_top = ob_get_contents();
ob_end_clean();

ob_start();

?>
                </div>

				<div id='activity_shade' class='inner_div1' style='display:none; background-color:#FFFFFF; opacity:0.9;'>
					<table style='width:280px; height:412px;'>
						<tr><td align='center' valign='bottom' style='height:181px; padding: 6px 8px 6px 8px;'><span id='activity_message' class='msg_txt1'>Initializing...</span></td></tr>
						<tr><td align='center' valign='top' style='height:231px; padding-top:6px;'><img src='images/moneris_activity.gif' width='32px' height='32px'/></td></tr>
					</table>
				</div>

				<!--<img style='position:absolute; left:100px; top:5px; width:80px;' src='images/moneris_logo.gif'/>-->

				<div style='position:absolute; left:245px; top:3px; width:32px; height:32px;'>
					<div style='position:absolute; left:0px top:0px; width:32px; height:32px; background-image:url("images/btn_wow_32x32.png");' ontouchstart='closeOverlay(); return false;'>
						<img src='images/x_icon.png' width='32px' height='32px'>
					</div>
				</div>

			</div>

		</div>

	</body>

</html>
<?php 

$lg_template_bottom = ob_get_contents();
ob_end_clean();

?>