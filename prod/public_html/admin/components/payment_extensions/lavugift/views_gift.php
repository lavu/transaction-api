<?php

if (!isset($viewmode))
{
	exit();
}

// Gift
// Viewmodes:
//		modifier - entry point when purchasing a gift card
//		submit_modifier - when purchasing a gift card and a gift number has been entered
// 		payment - when using gift card to purchase
//		submit_payment - when using a gift card to puchase and a gift number has been entered
global $loyalty_print;
$chain_id = "";
$account_query = mlavu_query("SELECT * FROM `poslavu_MAIN_db`.`restaurants` WHERE `data_name`='[1]'", $data_name);
if(mysqli_num_rows($account_query) > 0)
{
	$account_read = mysqli_fetch_assoc($account_query);
	$chain_id = $account_read['chain_id'];
	$restaurant_id = $account_read['id'];
}

if ($viewmode == "modifier")
{
	$item_id		= save_reqvar("item_id");
	$item_price		= verify_amount_is_int(save_reqvar("item_price"));
	$quantity		= save_reqvar("quantity");
	$detail1		= save_reqvar("detail1");
	$server_id		= save_reqvar("server_id");
	$server_name	= save_reqvar("server_name");
	$do_cmds		= save_reqvar("available_DOcmds");
	$app_version	= save_reqvar("version");

	$_SESSION['remember_reqvar_dataname']		= $data_name;
	$_SESSION['remember_reqvar_location_info']	= $location_info;

	echo "<table cellpadding=12>";
	echo "<tr>";
	echo "<td align='center' style='font-size:20px; color:#aaaaaa; width:".$use_view_width."px'>";
	echo "<br>";
	echo "Load Lavu Gift Card:<br>";
	echo "<b>".ext_display_money_with_symbol($item_price)."</b><br>";
	//echo "<input type='number' name='load_gift_name' style='font-size:20px; color:#777777'>";
	echo "</td>";
	echo "</tr>";
	echo "</table>";
}
else if ($viewmode == "submit_modifier")
{
	if (!isset($_GET['submit_id']))
	{
		exit();
	}

	$gift_id		= $_GET['submit_id'];
	$gift_dbid		= "";
	$submit_action	= (isset($_GET['submit_action']))?$_GET['submit_action']:"";
	$gift_amount	= verify_amount_is_int(remember_reqvar("item_price"));
	$item_id		= remember_reqvar("item_id");
	$server_id		= remember_reqvar("server_id");
	$server_name	= remember_reqvar("server_name");
	$datetime		= DateTimeForTimeZone(locationSetting("timezone"));
	$utc_time		= UTCDateTime(TRUE);
	$expdate		= get_expiration_date($datetime);

	$gift_amount		= ((str_replace("$", "", str_replace(",", "", $gift_amount))) * 1);
	$show_gift_amount	= ext_display_money_with_symbol($gift_amount);
	$showarr			= array( "" );

	$hist_vars = array(
		'chainid'		=> $chain_id,
		'dataname'		=> $data_name,
		'datetime'		=> $datetime,
		'name'			=> $gift_id,
		'server_id'		=> $server_id,
		'server_name'	=> $server_name,
		'utc_time'		=> $utc_time
	);

	if (strlen($chain_id) > 1)
	{
		#load all dataname existing in chain
		require_once('/home/poslavu/public_html/admin/sa_cp/manage_chain_functions.php');
		$chainedDataname = array();
		$chainedRestaurants =get_restaurants_by_chain($chain_id );	
		foreach($chainedRestaurants as $restaurants) {
			$chainedDataname[] = $restaurants['data_name'];
		}
		$card_query = mlavu_query("SELECT * FROM `poslavu_MAIN_db`.`lavu_gift_cards` WHERE `deleted`=0 AND `inactive` = 0 AND `name`='[1]' AND (`dataname` IN ('".implode("','", $chainedDataname)."') OR `chainid`='[2]')", $gift_id, $chain_id);
	}
	else
	{
		$card_query = mlavu_query("SELECT * FROM `poslavu_MAIN_db`.`lavu_gift_cards` WHERE `deleted`=0 AND `inactive` = 0 AND `name`='[1]' AND `dataname`='[2]'", $gift_id, $data_name);
	}

	if (mysqli_num_rows($card_query) > 0)
	{
		$card_read = mysqli_fetch_assoc($card_query);

		$current_balance		= $card_read['balance'];
		$current_points			= $card_read['points'];
		$new_balance			= (($current_balance * 1) + ($gift_amount * 1));
		$show_current_balance	= ext_display_money_with_symbol($current_balance);
		$show_new_balance		= ext_display_money_with_symbol($new_balance);
		$gift_dbid				= $card_read['id'];
		$gift_expires			= $card_read['expires'];

		if (empty($gift_expires))
		{
			$expired = false;
		}
		else
		{
			$expired = ((strtotime($gift_expires) - strtotime($datetime)) < 0);
		}

		if ($expired)
		{
			echo "<script type='text/javascript'>";
			echo "  setTimeout(function(){";
			echo "    componentAlert('Card Expired','Continue to re-register card as new'); ";
			echo "  }, 10);";
			echo "</script>";
			$showarr[] = "*Load New Card";
			$showarr[] = "";
			$showarr[] = "";
			$showarr[] = "[CN]".$gift_id;
			$showarr[] = "Amount: $show_gift_amount";
		}
		else
		{
			$showarr[] = "*Existing Card";
			$showarr[] = "";
			$showarr[] = "";
			$showarr[] = "[CN]".$gift_id;
			$showarr[] = "Current Amount: $show_current_balance";
			$showarr[] = "Amount to Load: $show_gift_amount";
			$showarr[] = "New Balance: $show_new_balance";

		}

		$card_action = "update";
	}
	else
	{
		$showarr[] = "*Load New Card";
		$showarr[] = "";
		$showarr[] = "";
		$showarr[] = "[CN]".$gift_id;
		$showarr[] = "Amount: ".$show_gift_amount;

		$card_action = "new";
	}
	$showarr[] = "";
	$showarr[] = "";
	$showarr[] = "load card";
	//$showarr[] = "";
	//$showarr[] = "*".$server_name;
	//$showarr[] = "Server ID: #".$server_id;

	if ($submit_action=="load_card")
	{
		$record_transtype = "IssueGiftCard";
		$success = false;

		if ($card_action == "new")
		{
			$new_balance = $gift_amount;
			$show_new_balance = ext_display_money_with_symbol($new_balance);

			$vars = array(
				'balance'			=> $new_balance,
				'chainid'			=> $chain_id,
				'created_datetime'	=> $datetime,
				'dataname'			=> $data_name,
				'expires'			=> $expdate,
				'name'				=> $gift_id,
				'points'			=> '0'
			);

			$success = mlavu_query("INSERT INTO `poslavu_MAIN_db`.`lavu_gift_cards` (`chainid`, `dataname`, `name`, `balance`, `expires`, `created_datetime`, `points`) VALUES ('[chainid]', '[dataname]', '[name]', '[balance]', '[expires]', '[created_datetime]', '[points]')", $vars);

			$gift_dbid = mlavu_insert_id();

			$hist_vars = array_merge( $hist_vars, array(
				'action'			=> "Created",
				'balance_amount'	=> $gift_amount,
				'card_id'			=> $gift_dbid,
				'item_id_list'		=> $item_id,
				'new_balance'		=> $new_balance,
				'new_points'		=> 0,
				'point_amount'		=> 0,
				'previous_balance'	=> 0,
				'previous_points'	=> 0,
			));
		}
		else if ($card_action == "update")
		{
			$record_transtype = "ReloadGiftCard";

			$success = mlavu_query("UPDATE `poslavu_MAIN_db`.`lavu_gift_cards` SET `balance` = '[1]', `expires` = '[2]' WHERE `id` = '[3]'", $new_balance, $expdate, $gift_dbid);

			$hist_vars = array_merge( $hist_vars, array(
				'action'			=> "Loaded",
				'balance_amount'	=> $gift_amount,
				'card_id'			=> $gift_dbid,
				'item_id_list'		=> $item_id,
				'new_balance'		=> $new_balance,
				'new_points'		=> $current_points,
				'point_amount'		=> 0,
				'previous_balance'	=> $current_balance,
				'previous_points'	=> $current_points,
			));
		}

		if ($success)
		{
			$hist_success = mlavu_query("INSERT INTO `poslavu_MAIN_db`.`lavu_gift_history` (`action`, `balance_amount`, `card_id`, `chainid`, `dataname`, `datetime`, `item_id_list`, `name`, `new_balance`, `new_points`, `point_amount`, `previous_balance`, `previous_points`, `server_id`, `server_name`, `utc_time`) VALUES ('[action]', '[balance_amount]', '[card_id]', '[chainid]', '[dataname]', '[datetime]', '[item_id_list]', '[name]', '[new_balance]', '[new_points]', '[point_amount]', '[previous_balance]', '[previous_points]', '[server_id]', '[server_name]', '[utc_time]')", $hist_vars);

			$hist_id = mlavu_insert_id();
			
			$print_datetime = str_replace(":", "[c", date("m/d/Y H:i:s", strtotime($datetime)));

			$mod_title = $gift_id."\nNew Balance: ".$show_new_balance;

			$item_details = array(
				'hidden1' => "lavu_gift",
				'hidden2' => $gift_id.":".$gift_dbid.":".$hist_id,
				'hidden3' => $record_transtype,
				'hidden4' => $gift_amount,
				'hidden5' => "lavu_gift",
				'hidden6' => "pending"
			);
			$created_datetime = date('Y-m-d H:i:s');
			$response = array(
				//  0 - success flag ("1")
				//  1 - [transaction_id]
				//  2 - [card_desc] (last_four)
				//  3 - response message
				//  4 - [auth_code]
				//  5 - [card_type]
				//  6 - [record_no]
				//  7 - [amount]
				//  8 - [order_id]
				//  9 - [ref_data]
				// 10 - [process_data]
				// 11 - new balance
				// 12 - bonus message
				// 13 - [first_four]
				// 14 - [last_mod_ts]
				'flag' => '1',
				'txnid' => $hist_id,
				'card_desc' => substr($gift_id, -4, 4),
				'message' => '',
				'auth_code' => '',
				'card_type' => 'Lavu Gift',
				'record_no' => '',
				'amount' => $gift_amount,
				'order_id' => '',
				'ref_data' => '',
				'process_data' => 'lavu_gift',
				'new_balance' => $new_balance,
				'tickets' => '',
				'first_four' => substr($gift_id, 0, 4),
				'last_mod_ts' => strtotime($created_datetime),
				'transtype' => $record_transtype
			);

			//echo "Success!";
			echo "<script type='text/javascript'>";
			echo "setTimeout(function(){";
			echo "window.location = '_DO:cmd=add_mod&mod_title=".rawurlencode($mod_title)."&item_details=".rawurlencode(json_encode($item_details))."&response=".rawurlencode(json_encode($response))."'; ";
			echo "}, 250);";

			if ($loyalty_print)
			{
				echo "setTimeout(function(){";
				echo "window.location = '_DO:cmd=print&print_string=active:--------------------------::Lavu Gift Card Loaded::Date: ".$print_datetime."::# ".$gift_id."::Amount* + ".$show_gift_amount." *     *::Balance* ".$show_new_balance." *     *::--------------------------::::: : ';";
				echo "}, 100);";
			}

			echo "</script>";
		}
		else
		{
			echo "<table><tr><td width='".$use_view_width."'><br><br><br><br>Error:<br>Unable to Write To the Database</td></tr></table>";
		}
	}
	else
	{
		echo "<br><br>";
		echo "<table cellspacing=0 cellpadding=6 width='".$use_view_width."'>";

		$left_column_width = (($use_view_width / 2) + 20);
		$two_column_mode = FALSE;

		for ($n = 0; $n < count($showarr); $n++)
		{
			$showarr_parts = explode(":", $showarr[$n], 2);
			if (count($showarr_parts) == 2)
			{
				$showarr_key = $showarr_parts[0];
				$showarr_val = $showarr_parts[1];
				$show_type = "2 column";
			}
			else
			{
				$show_arr_key = "";
				$showarr_val = $showarr_parts[0];
				if ($showarr_val == "load card")
				{
					$show_type = $showarr_val;
				}
				else if (substr($showarr_val, 0, 1) == "*")
				{
					$showarr_val = substr($showarr_val, 1);
					$show_type = "title";
				}
				else if (substr($showarr_val, 0, 4) == "[CN]")
				{
					$showarr_val = substr($showarr_val, 4);
					$show_type = "card number";
				}
				else
				{
					$show_type = "1 column";
				}
			}

			if ($show_type == "2 column")
			{
				if (!$two_column_mode)
				{
					$two_column_mode = true;

					echo "<tr><td colspan='2' align='center'><table cellspacing=0 cellpadding=4>";
				}

				echo "<tr><td class='gift_panel_text' align='right'>".$showarr_key.":</td><td class='gift_panel_text'>".$showarr_val."</td></tr>";
			}
			else
			{
				if ($two_column_mode)
				{
					$two_column_mode = false;

					echo "</table></td></tr>";
				}

				if ($show_type == "title")
				{
					echo "<tr><td class='gift_panel_title' colspan='2' align='center'>$showarr_val</td></tr>";
				}
				else if ($show_type == "1 column")
				{
					echo "<tr><td class='gift_panel_text' colspan='2' align='center'>".$showarr_val."</td></tr>";
				}
				else if ($show_type == "card number")
				{
					echo "<tr><td class='gift_panel_card_number' colspan='2' align='center'>".$showarr_val."</td></tr>";
				}
				else if ($show_type == "load card")
				{
					echo "<tr><td colspan='2' align='center' height='50px'>";
					echo "<input class='btn_light_long' type='button' style='font-size:20px' value='Load Card' onclick='loadCard(\"".$gift_id."\");' />";
					//echo "<input type='button' style='font-size:20px; color:#999999' value='Clear Card' onclick='if(confirm(\"Are you sure you want to clear this card's balance to zero?\")) window.location = \"?submit_id=$gift_id&submit_action=clear_card&viewmode=\" + viewmode;' />";
					echo "</td><tr>";
				}
			}
		}

		echo "</table>";
	}
}
else if ($viewmode == "payment")
{
	list($register, $offline_mode, $version, $server_id, $cc, $company_id, $server_name, $register_name, $remaining_amount, $order_id, $pay_type, $pay_type_id, $amount, $action, $customer_name, $customer_id) = setPaymentDetails();
	$_SESSION['remember_reqvar_dataname'] = $data_name;
	$_SESSION['remember_reqvar_location_info'] = $location_info;

	$vstr = "";
	$vstr .= "<table cellpadding=12>";
	$vstr .= "<tr>";
	$vstr .= "<td align='center' style='font-size:20px; color:#aaaaaa; width:".$use_view_width."px'>";
	$vstr .= ($pay_type == "MEAL PLAN") ? "Meal Plan<br>" : "<br>Lavu Gift Card Payment:<br>";
	$vstr .= ($pay_type == "MEAL PLAN") ? "<input type='radio' name='card_action' id='card_action_rfid' value='RFID' onclick='enableRfid()' checked><label for='card_action_rfid'>RFID</label> <input type='radio' name='card_action' id='card_action_swipe' onclick='enableSwipe()' value='swipe'><label for='card_action_swipe'>Swipe</label><br>" : "";
	$vstr .= "<b>[display_amount]</b><br><br>";
	//$vstr .= "<input type='number' name='load_gift_name' style='font-size:20px; color:#777777'>";
	$vstr .= "</td>";
	$vstr .= "</tr>";
	$vstr .= "</table>";
	if(isset($lg_templates) && isset($lg_templates['payment-main'])) $vstr = $lg_templates['payment-main'];
	$vstr = str_replace("[display_amount]",ext_display_money_with_symbol($amount),$vstr);
	echo $vstr;

	//echo "Amount: $amount<br>Server Id: $server_id<br>";
	/*$col = 1;
	echo "<table><tr>";
	foreach($_REQUEST as $rkey => $rval)
	{
		if($col>2) { echo "</tr><tr>"; $col = 1; }
		echo "<td style='font-size:9px'>$rkey = $rval</td>";
		$col++;
	}
	echo "</tr></table>";*/
	//foreach($_REQUEST as $pkey => $pval) echo $pkey . " = " . $pval . "<br>";
}
else if($viewmode=="submit_payment")
{
	// Using this code only for scan lavucard feature. We need to this to maintain below details in session
	if (isset($_GET['scan_lavucard'])) {
		list($register, $offline_mode, $version, $server_id, $cc, $company_id, $server_name, $register_name, $remaining_amount, $order_id, $pay_type, $pay_type_id, $amount, $action, $customer_name, $customer_id) = setPaymentDetails();
		$_SESSION['remember_reqvar_dataname'] = $data_name;
		$_SESSION['remember_reqvar_location_info'] = $location_info;
	}
	if(isset($_GET['submit_id']))
	{
		$gift_id = $_GET['submit_id'];
		$gift_dbid = "";
		$submit_action = (isset($_GET['submit_action']))?$_GET['submit_action']:"";
		$gift_amount = verify_amount_is_int(remember_reqvar("amount"));
		$order_id = remember_reqvar("order_id");
		$server_id = remember_reqvar("server_id");
		$server_name = remember_reqvar("server_name");
		$pay_type = remember_reqvar("pay_type");
		$pay_type_id = remember_reqvar("pay_type_id");
		$card_action = remember_reqvar("card_action");
		$datetime = DateTimeForTimeZone(locationSetting("timezone"));
		$expdate = get_expiration_date($datetime);
		$customer_name = reqvar("customer_name");
		$customer_id = reqvar("customer_id");

		$gift_amount = (str_replace("$","",str_replace(",","",$gift_amount))) * 1;
		$show_gift_amount = ext_display_money_with_symbol($gift_amount);
		$showarr = array();



		if(strlen($chain_id) > 1) {
			$card_query = mlavu_query("SELECT * FROM `poslavu_MAIN_db`.`lavu_gift_cards` WHERE `deleted`=0 AND  `inactive` = 0 AND `name`='[1]' AND (`dataname`='[2]' OR `chainid`='[3]')", $gift_id, $data_name, $chain_id);
		}
		else {
			$card_query = mlavu_query("SELECT * FROM `poslavu_MAIN_db`.`lavu_gift_cards` WHERE `deleted`=0 AND `inactive` = 0 AND `name`='[1]' AND `dataname`='[2]'", $gift_id, $data_name);
		}

		if(mysqli_num_rows($card_query) > 0 && $pay_type !== "MEAL PLAN" && $card_action !== 'use meal plan')
		{
			$card_read = mysqli_fetch_assoc($card_query);
			$current_balance = $card_read['balance'];
			if($current_balance == 0) {
				echo "<script language='javascript'> window.location = '_DO:cmd=close_overlay_DO:cmd=alert&title=Declined&message=This card has insufficient balance.';</script>";
				exit();
			}
			if($gift_amount * 1 > $card_read['balance'] * 1)
			{
				$gift_amount = $card_read['balance'] * 1;
				$show_gift_amount = ext_display_money_with_symbol($gift_amount);
			}
			$new_balance = $card_read['balance'] * 1 - $gift_amount * 1;
			$show_current_balance = ext_display_money_with_symbol($current_balance);
			$show_new_balance = ext_display_money_with_symbol($new_balance);
			$gift_dbid = $card_read['id'];
			$current_points = $card_read['points'];

			$showarr[] = "*Existing Card";
			$showarr[] = $gift_id;
			$showarr[] = "Current Amount: $show_current_balance";
			$showarr[] = "Amount To Use: $show_gift_amount";
			$showarr[] = "New Balance: $show_new_balance";

			$card_action = "update";
			$showarr[] = "use card";
			$showarr[] = "*".$server_name;
			$showarr[] = "Server Id: #$server_id";

			$_SESSION['remember_reqvar_card_action'] = $card_action;
		}
		else if($pay_type === "MEAL PLAN") {
			require_once(resource_path() . "/mealplan_functions.php");
			$mealPlanUser = getUserByRFID($gift_id, $restaurant_id);
			if ($mealPlanUser === false) {
				$showarr[] = "Meal Plan User Not Found";
				$showarr[] = $gift_id;
				$card_action = "not found";
				$showarr[] = "try another";
			} else {
				$current_balance = getStipendBalanceForUser($mealPlanUser);
				$new_balance = $current_balance * 1 - $gift_amount * 1;
				if ($new_balance < 0) {
					$new_balance = 0;
					$remaining_amount = $gift_amount * 1 - $current_balance * 1;
				}
				$customer_name = $mealPlanUser['f_name'] . " " . $mealPlanUser['l_name'];
				$customer_id = $mealPlanUser['id'];
				$customer_group = getMealPlanGroupInfo($mealPlanUser['MealPlanGroupID']);
				$showarr[] = $customer_name;
				$showarr[] = "Meal Plan Tier: " . $customer_group['name'];



				$showarr[] = "*Existing Card";
				$showarr[] = $gift_id;
				$showarr[] = "Current Amount: " . ext_display_money_with_symbol($current_balance);
				$showarr[] = "Amount To Use: " . $show_gift_amount;
				$showarr[] = "New Balance: " . ext_display_money_with_symbol($new_balance);

				$card_action = "use meal plan";
				$showarr[] = "use card";
				$showarr[] = "*" . $server_name;
				$showarr[] = "Server Id: #$server_id";

			}

			$_SESSION['remember_reqvar_card_action'] = $card_action;
		}
		else if($submit_action != "use_card")
		{
			$showarr[] = "Card Not Found";
			$showarr[] = $gift_id;
			$card_action = "not found";
			$showarr[] = "try another";
		}

		if($submit_action=="use_card")
		{
			$success = false;

			if ($card_action == "new")
			{

			}
			else if ($card_action == "update") {

				$success = mlavu_query("UPDATE `poslavu_MAIN_db`.`lavu_gift_cards` SET `balance`='[1]' WHERE `id`='[2]'", strval($new_balance), $gift_dbid);
			}
			else if ($card_action == "use meal plan") {
				require_once(resource_path()."/mealplan_functions.php");
				$payment_info = array('amount'=>$gift_amount, 'rfid'=>$gift_id, 'restaurantid'=>$restaurant_id, 'use_stipend_only' => true);
				$result = useMealPlan($payment_info);
				$success = $result['success'];
			}

			if ($success) {

				$hist_vars = array(
					'action' => "Gift Used",
					'balance_amount' => $gift_amount,
					'card_id' => $gift_dbid,
					'chainid' => $chain_id,
					'dataname' => $data_name,
					'datetime' => $datetime,
					'item_id_list' => $item_id,
					'name' => $gift_id,
					'new_balance' => $new_balance,
					'new_points' => $current_points,
					'order_id' => $order_id,
					'point_amount' => 0,
					'previous_balance' => $current_balance,
					'previous_points' => $current_points,
					'server_id' => $server_id,
					'server_name' => $server_name
				);
				if ($card_action == "use meal plan") {
					$mealPlanUser = getUserByRFID($gift_id, $restaurant_id);
					$transaction_info = array();
					$transaction_info['userID'] = $mealPlanUser['id'];
					$transaction_info['groupID'] = $mealPlanUser['MealPlanGroupID'];
					$transaction_info['amount'] = $gift_amount;
					$transaction_info['type'] = "stipend";
					$transaction_info['datetime'] = $mealPlanUser['last_update'];
					$transaction_info['location_id'] = $restaurant_id;
					insertMealPlanActionLog($transaction_info);
        				$hist_success = mlavu_query("INSERT INTO `poslavu_MAIN_db`.`lavu_gift_history` (`chainid`, `dataname`, `name`, `card_id`, `action`, `server_id`, `server_name`, `previous_points`, 
        				`previous_balance`, `point_amount`, `balance_amount`, `new_points`, `new_balance`, `item_id_list`, `datetime`,`order_id`) VALUES ('[chainid]', '[dataname]', '[name]', 
        				'[card_id]', '[action]', '[server_id]', '[server_name]', '[previous_points]', '[previous_balance]', '[point_amount]', '[balance_amount]', '[new_points]', 
        				'[new_balance]', '[item_id_list]', '[datetime]', '[order_id]')", $hist_vars);
				}
				else {
					$hist_success = mlavu_query("INSERT INTO `poslavu_MAIN_db`.`lavu_gift_history` (`chainid`, `dataname`, `name`, `card_id`, `action`, `server_id`, `server_name`, `previous_points`, `previous_balance`, `point_amount`, `balance_amount`, `new_points`, `new_balance`, `item_id_list`, `datetime`,`order_id`) VALUES ('[chainid]', '[dataname]', '[name]', '[card_id]', '[action]', '[server_id]', '[server_name]', '[previous_points]', '[previous_balance]', '[point_amount]', '[balance_amount]', '[new_points]', '[new_balance]', '[item_id_list]', '[datetime]', '[order_id]')", $hist_vars);
				}
				$pt_query = lavu_query("SELECT `special` FROM `payment_types` WHERE `id` = '[1]' AND `loc_id` = '[2]' AND `_deleted` = '0'", $pay_type_id, $loc_id);
				$pt_read = mysqli_fetch_assoc($pt_query);
				$process_data = $pt_read['special'];
				$hist_id = mlavu_insert_id();
				$card_desc = substr($gift_id, -4, 4);
				$first_four = substr($gift_id, 0, 4);
        			echo "<script type='text/javascript'>";
        			echo "
        			tx_info = {};
        			tx_info.amount = \"$gift_amount\";
        			tx_info.total_collected = tx_info.amount;
        			tx_info.tip_amount = \"0\";
        			tx_info.change = \"0\";
        			tx_info.datetime = \"$datetime\";
                    tx_info.first_four = \"$first_four\";
                    tx_info.card_desc = \"$card_desc\";
        			tx_info.exp = \"\";
        			tx_info.auth_code = \"\";
                    tx_info.transaction_id = \"$hist_id\";
        			tx_info.preauth_id = \"\";
        			tx_info.record_no = \"\";
        			tx_info.swipe_grade = \"\"; // card entry method
					tx_info.card_type = \"Lavu Gift\";
        			tx_info.pay_type = \"$pay_type\";
        			tx_info.pay_type_id = \"$pay_type_id\";
        			tx_info.action = \"Sale\";
                    tx_info.transtype = \"GiftCardSale\";
        			tx_info.got_response = \"1\";
        			tx_info.reader = \"\";
        			tx_info.gateway = \"\";
        			tx_info.for_deposit = \"\";
        			tx_info.is_deposit = \"\";
        			tx_info.internal_id = newInternalID();
                    tx_info.process_data = \"$process_data\";
        			tx_info.take_signature = \"0\";
        			tx_info.take_tip = \"0\";
        			tx_info.server_id = \"$server_id\";
        			tx_info.server = \"$server_name\";
        			tx_info.void_notes = \"\";
        			tx_info.refund_notes = \"\";
        			";
				$print_datetime = str_replace(":", "[c", date("m/d/Y H:i:s", strtotime($datetime)));

				echo "window.setTimeout(function(){addTransaction(tx_info)}, 1000); ";
				if ($loyalty_print) {
					echo "setTimeout(function(){";
					echo "window.location = '_DO:cmd=print&print_string=active:--------------------------::Lavu Gift Card Used::Date: " . $print_datetime . "::# " . $gift_id . "::Amount* - " . $show_gift_amount . " *     *::Balance* " . $show_new_balance . " *     *::--------------------------::::: : ';";
					echo "}, 100);";
				}
				echo "</script>";
			}

			else
			{
				echo "<table><tr><td width='".$use_view_width."'><br><br><br><br>Error:<br>Unable to Write To the Database</td></tr></table>";
			}
		}
		else if ($submit_action == "use_discount")
		{
			echo "<script>window.setTimeout(function(){ window.location = '_DO:cmd=set_loyalty&discount=check|p|.1|Meal Plan Discount||1|_DO:cmd=close_overlay'; }, 1000);</script>";
		}
		else
		{
			$vstr_start = "";
			$vstr_start .= "<br><br>";
			$vstr_start .= "<table cellspacing=0 cellpadding=6 width='".$use_view_width."'>";

			$vstr_end = "";
			$vstr_end .= "</table>";

			$vstr_2col = "";
			$vstr_2col .= "<tr><td align='right'>[key]</td><td>[value]</td></tr>";

			$vstr_title = "";
			$vstr_title .= "<tr><td colspan='2' align='center' bgcolor='#dddddd'>[value]</td></tr>";

			$vstr_1col = "";
			$vstr_1col .= "<tr><td colspan='2' align='center'>[value]</td></tr>";

			$button_text = ($pay_type == "MEAL PLAN" && floatval($current_balance) == 0) ? "Use Discount" : "Use Card";
			$vstr_usecard = "";
			$vstr_usecard .= "<tr><td colspan='2' align='center'>";
			$vstr_usecard .= "<input class='btn_light_long' type='button' style='font-size:20px' value='{$button_text}' onclick='window.location = [link]' />";
			$vstr_usecard .= "</td><tr>";

			$vstr_tryanother = "";
			$vstr_tryanother .= "<tr><td colspan='2' align='center'>";
			$vstr_tryanother .= "<input type='button' style='font-size:20px' value='Try Another' onclick='window.location = [link]' />";
			$vstr_tryanother .= "</td><tr>";

			if(isset($lg_templates) && isset($lg_templates['payment-start'])) $vstr_start = $lg_templates['payment-start'];
			if(isset($lg_templates) && isset($lg_templates['payment-end'])) $vstr_end = $lg_templates['payment-end'];
			if(isset($lg_templates) && isset($lg_templates['payment-2col'])) $vstr_2col = $lg_templates['payment-2col'];
			if(isset($lg_templates) && isset($lg_templates['payment-title'])) $vstr_title = $lg_templates['payment-title'];
			if(isset($lg_templates) && isset($lg_templates['payment-1col'])) $vstr_1col = $lg_templates['payment-1col'];
			if(isset($lg_templates) && isset($lg_templates['payment-usecard'])) $vstr_usecard = $lg_templates['payment-usecard'];
			if(isset($lg_templates) && isset($lg_templates['payment-tryanother'])) $vstr_tryanother = $lg_templates['payment-tryanother'];

			echo $vstr_start;
			for($n=0; $n<count($showarr); $n++)
			{
				$showarr_parts = explode(":",$showarr[$n],2);
				if(count($showarr_parts)==2)
				{
					$showarr_key = $showarr_parts[0];
					$showarr_val = $showarr_parts[1];
					$show_type = "2 column";
				}
				else
				{
					$show_arr_key = "";
					$showarr_val = $showarr_parts[0];
					if($showarr_val=="use card" || $showarr_val=="try another")
					{
						$show_type = $showarr_val;
					}
					else if(substr($showarr_val,0,1)=="*")
					{
						$showarr_val = substr($showarr_val,1);
						$show_type = "title";
					}
					else
					{
						$show_type = "1 column";
					}
				}

				if($show_type=="2 column")
				{
					$vstr = $vstr_2col;
					$vstr = str_replace("[key]",$showarr_key,$vstr);
					$vstr = str_replace("[value]",$showarr_val,$vstr);
					//echo "<tr><td align='right'>$showarr_key</td><td>$showarr_val</td></tr>";
					echo $vstr;
				}
				else if($show_type=="title")
				{
					$vstr = $vstr_title;
					$vstr = str_replace("[value]",$showarr_val,$vstr);
					//echo "<tr><td colspan='2' align='center' bgcolor='#dddddd'>$showarr_val</td></tr>";
					echo $vstr;
				}
				else if($show_type=="1 column")
				{
					$vstr = $vstr_1col;
					$vstr = str_replace("[value]",$showarr_val,$vstr);
					//echo "<tr><td colspan='2' align='center'>$showarr_val</td></tr>";
					echo $vstr;
				}
				else if($show_type=="use card")
				{
					$vstr = $vstr_usecard;
					$vstr = str_replace("[giftid]",$gift_id,$vstr);
					$vstr = str_replace("[link]","\"?submit_id=$gift_id&submit_action=use_card&viewmode=\" + viewmode + \"&customer_id={$customer_id}&customer_name=\"+ encodeURIComponent(\"{$customer_name}\");",$vstr);

					if ($pay_type == "MEAL PLAN" && floatval($current_balance) == 0)
					{
						$vstr = str_replace("&submit_action=use_card&", "&submit_action=use_discount&", $vstr);
					}

					echo $vstr;

					//$vstr_usecard = "";
					//$vstr_usecard .= "<tr><td colspan='2' align='center'>";
					//$vstr_usecard .= "<input class='btn_light_long' type='button' style='font-size:20px' value='Use Card' onclick='window.location = \"?submit_id=$gift_id&submit_action=use_card&viewmode=\" + viewmode;' />";
					////$vstr_usecard .= "<input type='button' style='font-size:20px; color:#999999' value='Clear Card' onclick='if(confirm(\"Are you sure you want to clear this card's balance to zero?\")) window.location = \"?submit_id=$gift_id&submit_action=clear_card&viewmode=\" + viewmode;' />";
					//$vstr_usecard .= "</td><tr>";
				}
				else if($show_type=="try another")
				{
					$gift_amount = verify_amount_is_int(remember_reqvar("amount"));
					$order_id = remember_reqvar("order_id");
					$server_id = remember_reqvar("server_id");
					$server_name = remember_reqvar("server_name");
					$pay_type = remember_reqvar("pay_type");
					$pay_type_id = remember_reqvar("pay_type_id");

					$vstr = $vstr_tryanother;
					$vstr = str_replace("[link]","\"?viewmode=payment&amount=$gift_amount&order_id=$order_id&pay_type=".urlencode($pay_type)."&pay_type_id=$pay_type_id&server_id=$server_id&server_name=".urlencode($server_name)."\"",$vstr);
					echo $vstr;

					//echo "<tr><td colspan='2' align='center'>";
					//echo "<input type='button' style='font-size:20px' value='Try Another' onclick='window.location = \"?viewmode=payment&amount=$gift_amount&order_id=$order_id&pay_type=".urlencode($pay_type)."&pay_type_id=$pay_type_id&server_id=$server_id&server_name=".urlencode($server_name)."\";' />";
					//echo "</td><tr>";
				}
			}
			//echo "</table>";
			echo $vstr_end;
		}
	}
}
else if(strpos($viewmode, "scan_giftcard_payment") !== FALSE)
{
	if(isset($_GET['submit_id']))
	{
		$submit_id = $_GET['submit_id'];
		$gift_dbid = "";
		$gift_amount = verify_amount_is_int(remember_reqvar("amount"));
		$order_id = remember_reqvar("order_id");
		$server_id = remember_reqvar("server_id");
		$server_name = remember_reqvar("server_name");
		$pay_type = remember_reqvar("pay_type");
		$pay_type_id = remember_reqvar("pay_type_id");
		$datetime = DateTimeForTimeZone(locationSetting("timezone"));
		$data_name = reqvar('dataname');
		$expdate = get_expiration_date($datetime);
		$submit_action = reqvar('submit_action');
		$awstr = 'remember_reqvar_loyalty_for_order_' . $order_id;
		$giftCardStatus = ext_get_saved_state($awstr);
		if ($giftCardStatus == "awarded" || $giftCardStatus == "discounted") {
			if($submit_action != "adjust_points" && $submit_action != "undo_discount" && $submit_action != "apply_discount") {
				$submit_action = $giftCardStatus;
			}
		} else if ($giftCardStatus == "") {
			ext_save_state($awstr, $submit_id);
		}

		$gift_amount = (str_replace("$","",str_replace(",","",$gift_amount))) * 1;
		$show_gift_amount = ext_display_money_with_symbol($gift_amount);
		$showarr = array();
		if(strlen($chain_id) > 1) {
			require_once('/home/poslavu/public_html/admin/sa_cp/manage_chain_functions.php');
			$chainedDataname = array();
			$chainedRestaurants =get_restaurants_by_chain($chain_id );	#get all chained restaurants
			foreach($chainedRestaurants as $restaurants) {
				$chainedDataname[] = $restaurants['data_name'];
			}

			$card_query = mlavu_query("SELECT * FROM `poslavu_MAIN_db`.`lavu_gift_cards` WHERE `deleted`=0 AND `inactive` = 0 AND `name`='[1]' AND (`dataname` IN('".implode("','", $chainedDataname)."') OR `chainid`='[2]')", $submit_id, $chain_id);
		}
		else {
			$card_query = mlavu_query("SELECT * FROM `poslavu_MAIN_db`.`lavu_gift_cards` WHERE `deleted`=0 AND `inactive` = 0 AND `name`='[1]' AND `dataname`='[2]'", $submit_id, $data_name);
		}
		if(mysqli_num_rows($card_query) > 0)
		{
			$card_read = mysqli_fetch_assoc($card_query);
			$current_balance = $card_read['balance'];
			$card_dbid = $card_read['id'];
			if($gift_amount * 1 > $card_read['balance'] * 1)
			{
				$gift_amount = $card_read['balance'] * 1;
				$show_gift_amount = ext_display_money_with_symbol($gift_amount);
			}
			$new_balance = $card_read['balance'] * 1 - $gift_amount * 1;
			$show_current_balance = ext_display_money_with_symbol($current_balance);
			$show_new_balance = ext_display_money_with_symbol($new_balance);
			$gift_dbid = $card_read['id'];
			$points_balance = $card_read['points'] * 1;
			$gift_balance = $card_read['balance'];

			$showarr[] = "*Existing Card";
			$showarr[] = $submit_id;
			$showarr[] = "Current Balance: $show_current_balance";
			$showarr[] = "Amount To Use: $show_gift_amount";
			$showarr[] = "Points: $points_balance";
		}
		else
		{
			$showarr[] = "Card Not Found";
			$showarr[] = $submit_id;
			$card_dbid = "new";
			$points_balance = "0";
			$gift_balance = "0";
			$showarr[] = "try another";
		}
	}
?>
	<script type='text/javascript'>
	
	var discount = "";
	
	function acceptOrderInfo(order_info_json)
	{
		if (document.getElementById('no_discrepancies') == null)
		{
			return 1;
		}
		
		var decoded_json = decodeURIComponent(order_info_json);
		var items = JSON.parse(decoded_json).contents;
		var db_discrepancy_detected = false;
		for (var i = 0; i < items.length; i++)
		{
			var quantity = items[i]['quantity'];
			var item = items[i]['item'];
			var failure = true;
			for (var j = 0; j < db_items.length; j++)
			{
				var q2 = db_items[j]['qty'];
				var i2 = db_items[j]['name'];
				if (q2 == quantity && i2 == item)
				{
					failure=false;
				}
			}
			
			if (failure)
			{
				db_discrepancy_detected = true;
			}
			
		}

		if (db_discrepancy_detected)
		{
			setTimeout(function()
			{
				window.location = '?submit_id=<?php echo $submit_id; ?>&viewmode=' + viewmode;
			}, 500);
		}
		else
		{
			setTimeout(function()
			{
				document.getElementById('no_discrepancies').style.display = '';
				document.getElementById('discrepancies').style.display = 'none';
			}, 100);
		}
		
		return 1;
	}
	
	window.onload = function()
	{
		window.location = '_DO:cmd=get_active_order_info&js=acceptOrderInfo(\'[ORDER_INFO]\');'
	}
	</script>
<?php
	$item_id_list = "";
	$total_award = 0;
	$js_items = "";
	$content_query = lavu_query("SELECT * FROM `order_contents` WHERE `order_id`='[1]'",$order_id);
	$print_award_string = "Award Breakdown[c::";
	while($content_read = mysqli_fetch_assoc($content_query))
	{
		$item_name = $content_read['item'];
		$item_id = $content_read['item_id'];
		$item_qty = $content_read['quantity'];
		$item_unit_price = $content_read['price'];
		$subtotal_with_mods = $content_read['subtotal_with_mods'];
		$discount_amount = $content_read['discount_amount'];
		$total_after_discount = $content_read['after_discount'];
		$tax_amount = $content_read['tax_amount'];
		$total_with_tax = $content_read['total_with_tax'];
		$total_before_tax = $total_with_tax * 1 - $tax_amount * 1;
		$per_unit_cost = $total_before_tax / $item_qty * 1;
		
		$js_items .= "{'name':'".str_replace("'", "\'", $item_name)."', 'qty':".$item_qty."},";
		
		$item_query = lavu_query("SELECT * FROM `menu_items` WHERE `id`='[1]'",$item_id);
		if(mysqli_num_rows($item_query))
		{
			$item_read = mysqli_fetch_assoc($item_query);
			$special2 = strtolower($item_read['hidden_value3']); // special details 2
			$award_parts = explode("award ",$special2);
			if(count($award_parts) > 1)
			{
				$point_parts = explode(" point",$award_parts[1]);
				if(count($point_parts) > 1)
				{
					$point_str = trim($point_parts[0]);
					
					if(is_numeric($point_str))
					{
						$min_cost = 0;
						$min_cost_parts = explode("minimum cost ",$point_parts[1]);
						if(count($min_cost_parts) > 1)
						{
							$min_cost_str = trim(str_replace("of ","",$min_cost_parts[1]));
							if(is_numeric($min_cost_str))
							{
								$min_cost = $min_cost_str * 1;
							}
						}
						
						if($per_unit_cost * 1 >= $min_cost * 1)
						{
							//echo $item_name . " - per unit: $per_unit_cost - award points: " . $point_str . " min cost: $min_cost<br>";
							if($item_id_list!="") $item_id_list .= ", ";
							$item_id_list .= $item_id . " x " . $item_qty;
							$total_award += $point_str * $item_qty;
							$print_award_string .= $item_name . " X " . $item_qty . "*" . ($point_str * $item_qty) . ":";
						}
					}
				}
			}
		}
		//echo $item_name . " - " . $total_before_tax . "<br>";
		//foreach($content_read as $key => $val) echo $key . " = " . $val . "<br>";
	}
	
	echo "<script type='text/javascript'>";
	echo "db_items = [".$js_items."]";
	echo "</script>";

	echo "<table><tr><td align='center' style='font-size:20px; color:#aaaaaa; width:".$use_view_width."px'>";
	if($submit_action=="awarded")
	{
		$awstr = 'remember_reqvar_loyalty_for_order_' . $order_id;

			$saved_state_info = ext_get_saved_state_info($awstr,"message");
			$card_id = ext_get_saved_state_info($awstr, "name");
			$card_font_size = strlen($card_id) < 15 ? 20 : 15;
			if($saved_state_info!="")
			{
				echo "<h4>Status:</h4>";
				echo "<p style='text-align:right'><b style='float:left'>Awarded: </b>";
				echo $saved_state_info;
				echo "<br/><b style='float:left'>Card</b><span style='font-size:".$card_font_size."px'>";
				echo $card_id;
				echo "</span></p>";
				$saved_state_awarded = ext_get_saved_state_info($awstr,"awarded");
				
				if($total_award * 1 != $saved_state_awarded * 1)
				{
					$btn_points_title = "";
					if($total_award * 1 > $saved_state_awarded)
					{
						echo "<h4>Notice:</h4>";
						echo "More Points are Available<br>";
						echo "<p style='text-align:right'><b style='float:left'>Points Available: </b>";
						echo ext_display_points($total_award - $saved_state_awarded) . "</p>";
						$btn_points_title = "Add Points";
					}
					else if($total_award * 1 < $saved_state_awarded)
					{
						echo "<h4>Notice:</h4>";
						echo "Awarded Too Many Points!<br>";
						echo "<p style='text-align:right'><b style='float:left'>Points Over: </b>";
						echo ext_display_points($saved_state_awarded - $total_award) . "</p>";
						$btn_points_title = "Adjust Points";
					}
					
					$add_points = $total_award - $saved_state_awarded;
					echo "<br><input class='btn_light_long' type='button' style='font-size:20px' value='$btn_points_title' onclick='window.location = \"?submit_id=$submit_id&submit_action=adjust_points&add_points=$add_points&dataname=$data_name&viewmode=\" + viewmode;' /><br>";
				}
				
			}
	}
	elseif($submit_action=="award_points" || $submit_action=="adjust_points")
	{
		$add_points = (isset($_GET['add_points']))?$_GET['add_points']:$total_award;
		$awstr = 'remember_reqvar_loyalty_for_order_' . $order_id;
		$saved_state_awarded = ext_get_saved_state_info($awstr,"awarded");
		$saved_state_name = ext_get_saved_state_info($awstr,"name");
		if(!is_numeric($saved_state_awarded)) $saved_state_awarded = 0;
		if($saved_state_name!="" && $submit_id=="awarded")
		{
			$submit_id = $saved_state_name;
		}
		
		$track_total_award = $total_award;
		if($add_points==($total_award - $saved_state_awarded) || $add_points==($saved_state_awarded - $total_award))
		{
			$total_award = $add_points;
		}
		
		$new_balance = $points_balance + $total_award;
		$show_new_balance = ext_display_points($new_balance);
		$sign = ($total_award > 0) ? "+ " : "- ";
		update_scan_data($data_name, $chain_id, $submit_id, $datetime, $card_dbid, $new_balance, $total_award, "", $print_award_string, $order_id, $item_id_list);
		$show_total_award = ext_display_points($track_total_award);
		
		ext_save_state('remember_reqvar_loyalty_for_order_' . $order_id,"awarded",array("message"=>"$show_total_award","last_awarded"=>$total_award,"awarded"=>$track_total_award,"name"=>$submit_id));
		
		echo "<script type='text/javascript'>";
		echo "window.location = '_DO:cmd=close_overlay';";
		echo "</script>";
		exit();
	}
	else if($submit_action == "discounted") {
			$awstr = 'remember_reqvar_loyalty_for_order_' . $order_id;
			//if(isset($_SESSION[$awstr]))
			//{
			//	echo $_SESSION[$awstr];
				//}
				$saved_state_info = ext_get_saved_state_info($awstr,"message");
				$points_balance = ext_get_saved_state_info($awstr, "points_balance");
				$card_id = ext_get_saved_state_info($awstr, "name");
		
				if($saved_state_info!="")
				{
					echo "<br><br/>";
					echo "<b>Applied Reward:</b><br/>";
					// echo $submit_id;
					echo $saved_state_info;
					echo "<br/><br/><b>Card #:</b><br/>";
					echo $card_id;
					// $saved_state_discount_info = ext_get_saved_state_info($awstr,"discount_info");
					echo "<br/><br/><br/><input class='btn_light_super_long' type='button' style='font-size:20px' value='Undo & Refund Points' onclick='window.location = \"?submit_id=$submit_id&dataname=$data_name&order_id=$order_id&submit_action=undo_discount&card_dbid=$card_dbid&points_balance=$points_balance&reward_cost=$reward_cost&viewmode=\" + viewmode;' /><br>";
					//echo "<br>Awarded: " . $saved_state_awarded . "<br>";
				}
	}else if($submit_action == "undo_discount") {
			$awstr = 'remember_reqvar_loyalty_for_order_' . $order_id;
			$reward_cost = ext_get_saved_state_info($awstr, "reward_cost");
			$card_dbid = ext_get_saved_state_info($awstr, "card_dbid");
			$new_balance = ext_get_saved_state_info($awstr, "old_balance");
			$old_balance = ext_get_saved_state_info($awstr, "new_balance"); //old is new and new is old.
			$card_id = ext_get_saved_state_info($awstr, "submit_id");

			update_scan_data($data_name, $chain_id, $card_id, $datetime, $card_dbid, $new_balance, "+ ".($new_balance - $old_balance), "Undo Discount", "", $order_id, $item_id_list);
			ext_save_state($awstr, $card_id);
			echo "Please Wait";
			echo "<script type='text/javascript'>";
		
			echo "window.location = '_DO:cmd=set_loyalty&discount=check|" . $reward_code . "|0|none|0|0 _DO:cmd=close_overlay';";
			echo "setTimeout(function(){window.location = \"?submit_id=$card_id&submit_action=''&viewmode=\" + viewmode}, 200);";
			echo "</script>";
	}else if($submit_action == "apply_discount") {
		/* 		https://bugtest-uno.poslavu.com/components/payment_extensions/lavugift/lavugift.php?
		 submit_id=987654321&submit_action=apply_discount&reward_id=6&reward_label=&reward_use_or_unlock=2&reward_calc=1&reward_code=d&reward_cost=0&viewmode=scan_giftcard_payment */
		if (isset($_GET['reward_id']))
		{
			$reward_id = $_GET['reward_id'];
			$reward_label = $_GET['reward_label'];
			$reward_use_or_unlock = $_GET['reward_use_or_unlock'];
			$reward_cost = $_GET['reward_cost'];
			$reward_code = $_GET['reward_code'];
			$reward_calc = $_GET['reward_calc'];
			$order_info  = urldecode($_GET['order_info']);
			$order_info  = stripslashes($order_info);
			$order_info  = json_decode($order_info, 1);
			$dataname    = reqvar('dataname', $data_name);
			$pos_dataname = "`poslavu_".$dataname."_db`";
			$loyalty_query = mlavu_query("SELECT `level` FROM $pos_dataname.`discount_types` WHERE `id`='[1]'",$reward_id);
			$loyalty_read = mysqli_fetch_array($loyalty_query);
			if ($loyalty_read['level'] != 'checks' && !isset($_GET['item_icid'])) {
				if(!isset($_GET['order_info'])) {
					$url = $_SERVER['REQUEST_URI'];
					$url = $url."&order_id=".$order_id;
					$send_url = str_replace('?', '#', $url);
					echo "<script type='text/javascript'>";
					echo "window.location = '_DO:cmd=get_active_check_details&url=".$send_url."';";
					echo "</script>";
				} else {
					$available_discounts_html = "";
					$available_discounts_html = "<div id='discrepancies' style='display:'><h4>Select the item you would like to discount</h4></div>";
					$available_discounts_html = $available_discounts_html . "<div style='overflow: auto;height: 315px;'>";
					foreach ($order_info[contents] as $order_data) {
						$icid = $order_data[icid];
						$onclick = "window.location = \"?submit_id=$submit_id&submit_action=apply_discount&reward_id=$reward_id&reward_label=$reward_label&reward_use_or_unlock=$reward_use_or_unlock&reward_calc=$reward_calc&reward_code=$reward_code&reward_cost=$reward_cost&order_id=$order_id&viewmode=submit_loyalty&dataname=$dataname&item_icid=$icid\"";
						$available_discounts_html = $available_discounts_html . "<div style='padding:6px;' onclick='$onclick'> ".urldecode($order_data[item])." </div>";
					}
					if ($loyalty_read['level'] == '') {
						$onclick = "window.location = \"?submit_id=$submit_id&submit_action=apply_discount&reward_id=$reward_id&reward_label=$reward_label&reward_use_or_unlock=$reward_use_or_unlock&reward_calc=$reward_calc&reward_code=$reward_code&reward_cost=$reward_cost&order_id=$order_id&viewmode=submit_loyalty&dataname=$dataname&item_icid=checks\"";
						$available_discounts_html = $available_discounts_html . "<div style='padding:6px;' onclick='$onclick' > Apply Discount to Check </div>";
					}
					$available_discounts_html = $available_discounts_html . "</div><br/>";
					echo $available_discounts_html;exit();
				}
			}

			$awstr = 'remember_reqvar_loyalty_for_order_' . $order_id;
			$saved_state_name = ext_get_saved_state_info($awstr,"name");
			if ($saved_state_name != "" && $submit_id == "awarded") {
				$submit_id = $saved_state_name;
			}
			if ($loyalty_read['level'] == 'checks' || isset($_GET['item_icid'])) {
				$new_balance = $points_balance - $reward_cost;
				update_scan_data($data_name, $chain_id, $submit_id, $datetime, $card_dbid, $new_balance, "- ".$reward_cost, 'Applied Discount[c*'.$reward_label.':',"", $order_id, $item_id_list);
				ext_save_state('remember_reqvar_loyalty_for_order_' . $order_id, "discounted",array("message"=>"$reward_label", "last_awarded"=>$total_award, "discount_info"=>$reward_label, "name"=>$submit_id, "card_dbid"=>$card_dbid, "reward_cost"=>$reward_cost, "old_balance"=>$points_balance, "new_balance"=>$new_balance, "submit_id"=>$submit_id));
			}
		}
	}
	if ($submit_action !='awarded' && $submit_action != 'discounted') {
	$vstr_start = "";
	$vstr_start .= "<table cellspacing=0 cellpadding=6 width='".$use_view_width."' style='margin-top:35px;'>";

	$vstr_end = "";
	$vstr_end .= "</table>";

	$vstr_2col = "";
	$vstr_2col .= "<tr><td align='right'>[key]</td><td>[value]</td></tr>";

	$vstr_title = "";
	$vstr_title .= "<tr><td colspan='2' align='center' bgcolor='#dddddd'>[value]</td></tr>";

	$vstr_1col = "";
	$vstr_1col .= "<tr><td colspan='2' align='center'>[value]</td></tr>";

	$vstr_usecard = "";
	$vstr_usecard .= "<tr><td colspan='2' align='center'>";
	$vstr_usecard .= "<input class='btn_light_long' type='button' style='font-size:20px' value='Use Card' onclick='window.location = [link]' />";
	$vstr_usecard .= "</td><tr>";

	$vstr_tryanother = "";
	$vstr_tryanother .= "<tr><td colspan='2' align='center'>";
	$vstr_tryanother .= "<input type='button' style='font-size:20px' value='Try Another' onclick='window.location = [link]' />";
	$vstr_tryanother .= "</td><tr>";

	if(isset($lg_templates) && isset($lg_templates['payment-start'])) $vstr_start = $lg_templates['payment-start'];
	if(isset($lg_templates) && isset($lg_templates['payment-end'])) $vstr_end = $lg_templates['payment-end'];
	if(isset($lg_templates) && isset($lg_templates['payment-2col'])) $vstr_2col = $lg_templates['payment-2col'];
	if(isset($lg_templates) && isset($lg_templates['payment-title'])) $vstr_title = $lg_templates['payment-title'];
	if(isset($lg_templates) && isset($lg_templates['payment-1col'])) $vstr_1col = $lg_templates['payment-1col'];
	if(isset($lg_templates) && isset($lg_templates['payment-usecard'])) $vstr_usecard = $lg_templates['payment-usecard'];
	if(isset($lg_templates) && isset($lg_templates['payment-tryanother'])) $vstr_tryanother = $lg_templates['payment-tryanother'];

	echo $vstr_start;
	for($n=0; $n<count($showarr); $n++)
	{
		$showarr_parts = explode(":",$showarr[$n],2);
		if(count($showarr_parts)==2)
		{
			$showarr_key = $showarr_parts[0];
			$showarr_val = $showarr_parts[1];
			$show_type = "2 column";
		}
		else
		{
			$show_arr_key = "";
			$showarr_val = $showarr_parts[0];
			if($showarr_val=="use card" || $showarr_val=="try another")
			{
				$show_type = $showarr_val;
			}
			else if(substr($showarr_val,0,1)=="*")
			{
				$showarr_val = substr($showarr_val,1);
				$show_type = "title";
			}
			else
			{
				$show_type = "1 column";
			}
		}

		if($show_type=="2 column")
		{
			$vstr = $vstr_2col;
			$vstr = str_replace("[key]",$showarr_key,$vstr);
			$vstr = str_replace("[value]",$showarr_val,$vstr);
			echo $vstr;
		}
		else if($show_type=="title")
		{
			$vstr = $vstr_title;
			$vstr = str_replace("[value]",$showarr_val,$vstr);
			echo $vstr;
		}
		else if($show_type=="1 column")
		{
			$vstr = $vstr_1col;
			$vstr = str_replace("[value]",$showarr_val,$vstr);
			echo $vstr;
		}
		else if($show_type=="use card")
		{
			$vstr = $vstr_usecard;
			$vstr = str_replace("[giftid]",$submit_id,$vstr);
			$vstr = str_replace("[link]","\"?submit_id=$submit_id&submit_action=use_card&viewmode=\" + viewmode;",$vstr);
			echo $vstr;
		}
		else if($show_type=="try another")
		{
			$gift_amount = verify_amount_is_int(remember_reqvar("amount"));
			$order_id = remember_reqvar("order_id");
			$server_id = remember_reqvar("server_id");
			$server_name = remember_reqvar("server_name");
			$pay_type = remember_reqvar("pay_type");
			$pay_type_id = remember_reqvar("pay_type_id");

			$vstr = $vstr_tryanother;
			$vstr = str_replace("[link]","\"?viewmode=payment&amount=$gift_amount&order_id=$order_id&pay_type=".urlencode($pay_type)."&pay_type_id=$pay_type_id&server_id=$server_id&server_name=".urlencode($server_name)."\"",$vstr);
			echo $vstr;
		}
	}
	echo $vstr_end;

	$reward_query = lavu_query("SELECT * FROM `discount_types` WHERE `special`='lavu_loyalty' AND `_deleted`='0' AND `loyalty_cost`<='[1]' AND `min_access`<='[2]'", $points_balance, 4);
	$available_discounts_html = "";
	$onchange = "window.location = \"?submit_id=$submit_id&dataname=$data_name&order_id=$order_id&submit_action=apply_discount\" + this.value + \"&viewmode=\" + viewmode";

	$disableSelect = (mysqli_num_rows($reward_query) > 0) ? "" : "disabled style='opacity:0.4'";
	$available_discounts_html = $available_discounts_html . "<select $disableSelect class='btn_light' id='discount_select' onchange='$onchange'><option value=''>Select Discount</option>";
	if(mysqli_num_rows($reward_query) > 0) {
		$discounts_opacity = "opacity:1.0;";
		while ($row = mysqli_fetch_array($reward_query, MYSQL_ASSOC)) {
			$reward_id = $row['id'];
			$reward_label = $row['label'];
			$reward_calc = $row['calc'];
			$reward_code = $row['code'];
			$reward_use_or_unlock = $row['loyalty_use_or_unlock'];
			$reward_cost = $row['loyalty_cost'];
			$unlock_data='';
			if($reward_use_or_unlock=='2'){ $unlock_data = 'unlock'; }

			if($reward_code == 'p') {
				$tmp = floatval($reward_calc) * 100;
				$reward_text = $tmp . "%";
			}
			else
				$reward_text = ext_display_money_with_symbol($reward_calc);
				$reward_text = $reward_label . ": " . $reward_text . " off ".$unlock_data." at " . $reward_cost . " points";
				if($reward_use_or_unlock=='2'){ $reward_cost = '0'; }
				$available_discounts_html = $available_discounts_html . "<option value='&reward_id=" . $reward_id ."&reward_label=". $reward_label ."&reward_use_or_unlock=". $reward_use_or_unlock ."&reward_calc=". $reward_calc ."&reward_code=". $reward_code ."&reward_cost=". $reward_cost ."'>" . $reward_text . "</option>";
		}
	}
	$available_discounts_html = $available_discounts_html . "</select><br/>";
	echo "<hr>";
	$discounts_opacity = "opacity:0.4;";
	$awards_opacity = ($total_award > 0) ? "opacity:1.0;" : "opacity:0.4;";
	echo "<table cellpadding='8' style='text-align:center; border-collapse:collapse;'><tr><td style='border-right:1px solid'>";
	echo "<b style='".$discounts_opacity."'>Discounts:</b><p style='".$discounts_opacity."'>" . mysqli_num_rows($reward_query) . "</p>";
	echo "</td><td >";
	echo "<b  style='".$awards_opacity."'>Award: </b><p style='".$awards_opacity."'>$total_award</p>";
	echo "</td></tr><tr><td style='border-right:1px solid'>";
	echo $available_discounts_html;
	echo "</td><td >";
	$disabled = ($total_award > 0) ? "" : "disabled";
	echo "<input class='btn_light' type='button' $disabled value='Award Points' onclick='window.location = \"?submit_id=$submit_id&order_id=$order_id&dataname=$data_name&submit_action=award_points&add_points=$total_award&viewmode=\" + viewmode;' /><br>";
	}
	echo "</td></tr></table>";
}
function update_scan_data($data_name, $chain_id, $submit_id, $datetime, $card_dbid, $new_balance, $change_amount, $print_before, $print_extra, $order_id, $item_id_list)
{
	global $server_id;
	global $server_name;
	global $gift_balance;
	global $points_balance;
	global $loyalty_print;

	$expdate = get_expiration_date($datetime);

	$print_extra = str_replace('?', '', $print_extra);
	$print_extra = str_replace('&', '', $print_extra);

	$hist_vars = array();
	$hist_vars['chainid'] = $chain_id;
	$hist_vars['dataname'] = $data_name;
	$hist_vars['name'] = $submit_id;
	$hist_vars['server_id'] = $server_id;
	$hist_vars['server_name'] = $server_name;
	$hist_vars['datetime'] = $datetime;

	if($card_dbid=="new")
	{

		$vars = array();
		$vars['chainid'] = $chain_id;
		$vars['dataname'] = $data_name;
		$vars['name'] = $submit_id;
		$vars['points'] = $new_balance;
		$vars['expires'] = $expdate;
		$vars['created_datetime'] = $datetime;
		$vars['balance'] = '0';
		$success = mlavu_query("INSERT INTO `poslavu_MAIN_db`.`lavu_gift_cards` (`chainid`,`dataname`,`name`,`points`,`expires`,`created_datetime`,`balance`) VALUES ('[chainid]','[dataname]','[name]','[points]','[expires]','[created_datetime]','[balance]')",$vars);

		$hist_vars['card_id'] = mlavu_insert_id();
		$hist_vars['action'] = "Created";
		$hist_vars['previous_points'] = 0;
		$hist_vars['previous_balance'] = 0;
		$hist_vars['point_amount'] = $change_amount;
		$hist_vars['balance_amount'] = 0;
		$hist_vars['new_points'] = $new_balance;
		$hist_vars['new_balance'] = 0;
		$hist_vars['item_id_list'] = $item_id_list;
		$hist_vars['order_id'] = $order_id;

	}
	else
	{
		$success = mlavu_query("UPDATE `poslavu_MAIN_db`.`lavu_gift_cards` SET `points`='[1]',`expires`='[2]' WHERE `id`='[3]'", $new_balance, $expdate, $card_dbid);

		$hist_vars['card_id'] = $card_dbid;
		$hist_vars['action'] = "Loyalty";
		$hist_vars['previous_points'] = $points_balance;
		$hist_vars['previous_balance'] = $gift_balance;
		$hist_vars['point_amount'] = $change_amount;
		$hist_vars['balance_amount'] = 0;
		$hist_vars['new_points'] = $new_balance;
		$hist_vars['new_balance'] = $gift_balance;
		$hist_vars['item_id_list'] = $item_id_list;
		$hist_vars['order_id'] = $order_id;
	}
	if($success) {
		$hist_success = mlavu_query("INSERT INTO `poslavu_MAIN_db`.`lavu_gift_history` (`chainid`, `dataname`, `name`, `card_id`, `action`, `server_id`, `server_name`, `previous_points`, `previous_balance`, `point_amount`, `balance_amount`, `new_points`, `new_balance`, `item_id_list`, `datetime`) VALUES ('[chainid]', '[dataname]', '[name]', '[card_id]', '[action]', '[server_id]', '[server_name]', '[previous_points]', '[previous_balance]', '[point_amount]', '[balance_amount]', '[new_points]', '[new_balance]', '[item_id_list]', '[datetime]')", $hist_vars);
	}
/* 	$print_datetime = str_replace(":", "[c", date("m/d/Y H:i:s", strtotime($datetime)));
	if($loyalty_print){
		echo "<script type='text/javascript'>";
		echo "setTimeout(function(){";
		echo "window.location = '_DO:cmd=print&print_string=active:--------------------------::Lavu Loyalty Card::Date: $print_datetime::# $submit_id:$print_before:Points* $change_amount *     *::Balance* $new_balance *     *:: $print_extra ::--------------------------::::: : ';";
		echo "}, 100);";
		echo "</script>";
	} */
}

/**
* This function is used to set payment details to session and return in array.
*
* @return array $paymentInfo.
*/
function setPaymentDetails() {
	//Todo: if any one want to add new value, please add at end.
	$paymentInfo = array (
		save_reqvar("register"),
		save_reqvar("offline_mode"),
		save_reqvar("version"),
		save_reqvar("server_id"),
		save_reqvar("cc"),
		save_reqvar("company_id"),
		save_reqvar("server_name"),
		save_reqvar("register_name"),
		save_reqvar("remaining_amount"),
		save_reqvar("order_id"),
		save_reqvar("pay_type"),
		save_reqvar("pay_type_id"),
		verify_amount_is_int(save_reqvar("amount")),
		save_reqvar("action"),
		reqvar("customer_name"),
		reqvar("customer_id")
	);
	return $paymentInfo;
}