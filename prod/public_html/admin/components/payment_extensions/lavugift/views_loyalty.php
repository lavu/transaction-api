<?php

	function update_point_data($data_name, $chain_id, $submit_id, $datetime, $card_dbid, $new_balance, $change_amount, $print_before, $print_extra)
	{
		global $server_id;
		global $server_name;
		global $gift_balance;
		global $points_balance;
		global $loyalty_print;

		$expdate = get_expiration_date($datetime);

		$print_extra = str_replace('?', '', $print_extra);
		$print_extra = str_replace('&', '', $print_extra);

		$hist_vars = array();
		$hist_vars['chainid'] = $chain_id;
		$hist_vars['dataname'] = $data_name;
		$hist_vars['name'] = $submit_id;
		$hist_vars['server_id'] = $server_id;
		$hist_vars['server_name'] = $server_name;
		$hist_vars['datetime'] = $datetime;

		if($card_dbid=="new")
		{

			$vars = array();
			$vars['chainid'] = $chain_id;
			$vars['dataname'] = $data_name;
			$vars['name'] = $submit_id;
			$vars['points'] = $new_balance;
			$vars['expires'] = $expdate;
			$vars['created_datetime'] = $datetime;
			$vars['balance'] = '0';
			$success = mlavu_query("INSERT INTO `poslavu_MAIN_db`.`lavu_gift_cards` (`chainid`,`dataname`,`name`,`points`,`expires`,`created_datetime`,`balance`) VALUES ('[chainid]','[dataname]','[name]','[points]','[expires]','[created_datetime]','[balance]')",$vars);

			$hist_vars['card_id'] = mlavu_insert_id();
			$hist_vars['action'] = "Created";
			$hist_vars['previous_points'] = 0;
			$hist_vars['previous_balance'] = 0;
			$hist_vars['point_amount'] = $change_amount;
			$hist_vars['balance_amount'] = 0;
			$hist_vars['new_points'] = $new_balance;
			$hist_vars['new_balance'] = 0;
			$hist_vars['item_id_list'] = $item_id_list;
			$hist_vars['order_id'] = $order_id;

		}
		else
		{
			$success = mlavu_query("UPDATE `poslavu_MAIN_db`.`lavu_gift_cards` SET `points`='[1]',`expires`='[2]' WHERE `id`='[3]'", $new_balance, $expdate, $card_dbid);

			$hist_vars['card_id'] = $card_dbid;
			$hist_vars['action'] = "Loyalty";
			$hist_vars['previous_points'] = $points_balance;
			$hist_vars['previous_balance'] = $gift_balance;
			$hist_vars['point_amount'] = $change_amount;
			$hist_vars['balance_amount'] = 0;
			$hist_vars['new_points'] = $new_balance;
			$hist_vars['new_balance'] = $gift_balance;
			$hist_vars['item_id_list'] = $item_id_list;
			$hist_vars['order_id'] = $order_id;
		}
		if($success) {
			$hist_success = mlavu_query("INSERT INTO `poslavu_MAIN_db`.`lavu_gift_history` (`chainid`, `dataname`, `name`, `card_id`, `action`, `server_id`, `server_name`, `previous_points`, `previous_balance`, `point_amount`, `balance_amount`, `new_points`, `new_balance`, `item_id_list`, `datetime`) VALUES ('[chainid]', '[dataname]', '[name]', '[card_id]', '[action]', '[server_id]', '[server_name]', '[previous_points]', '[previous_balance]', '[point_amount]', '[balance_amount]', '[new_points]', '[new_balance]', '[item_id_list]', '[datetime]')", $hist_vars);
		}
		$print_datetime = str_replace(":", "[c", date("m/d/Y H:i:s", strtotime($datetime)));
		if($loyalty_print){
			echo "<script type='text/javascript'>";
			echo "setTimeout(function(){";
			echo "window.location = '_DO:cmd=print&print_string=active:--------------------------::Lavu Loyalty Card::Date: $print_datetime::# $submit_id:$print_before:Points* $change_amount *     *::Balance* $new_balance *     *:: $print_extra ::--------------------------::::: : ';";
			echo "}, 100);";
			echo "</script>";
		}
	}

	function init_card_data()
	{

	}
	if(!isset($viewmode))
	{
		exit();
	}

	// Loyalty
	// Viewmodes:
	//		loyalty - Entry Point for Loyalty
	//		submit_loyalty - After a Loyalty number has been entered

	$chain_id = "";
	$account_query = mlavu_query("SELECT * FROM `poslavu_MAIN_db`.`restaurants` WHERE `data_name`='[1]'", $data_name);
	if(mysqli_num_rows($account_query))
	{
		$account_read = mysqli_fetch_assoc($account_query);
		$chain_id = $account_read['chain_id'];
	}

	// echo date("Y-m-d H:i:s")."<br>";
	if($viewmode=="loyalty")
	{
		$register = save_reqvar("register");
		$offline_mode = save_reqvar("offline_mode");
		$version = save_reqvar("version");
		$server_id = save_reqvar("server_id");
		$cc = save_reqvar("cc"); // dataname
		$company_id = save_reqvar("company_id");
		$server_name = save_reqvar("server_name");
		$register_name = save_reqvar("register_name");
		$remaining_amount = save_reqvar("remaining_amount");
		$order_id = save_reqvar("order_id");
		$pay_type = save_reqvar("pay_type");
		$pay_type_id = save_reqvar("pay_type_id");
		$amount = verify_amount_is_int(save_reqvar("amount"));
		$action = save_reqvar("action");
		$_SESSION['remember_reqvar_dataname'] = $data_name;
		$_SESSION['remember_reqvar_location_info'] = $location_info;
		if(!isset($order_id)) {
			$order_id =$_GET['order_id'];
		}
		
		$rolstr = 'remember_reqvar_loyalty_for_order_' . $order_id;
		if(isset($_GET['reset']))
		{
			ext_save_state($rolstr,"");
			//unset($_SESSION[$rolstr]);
		}
		$saved_state = ext_get_saved_state($rolstr);
		//if(isset($_SESSION[$rolstr]))
		if($saved_state!="")
		{
			echo "<script type='text/javascript'>";
			//echo "window.location = \"?viewmode=submit_loyalty&submit_id=".$_SESSION[$rolstr]."\"; ";
			echo "window.location = \"?viewmode=submit_loyalty&submit_id=$saved_state\"; ";
			echo "</script>";
			exit();
		}

		$vstr = "";
		$vstr .= "<table cellpadding=12>";
		$vstr .= "<tr>";
		$vstr .= "<td align='center' style='font-size:20px; color:#aaaaaa; width:".$use_view_width."px'>";
		$vstr .= "<br>";
		$vstr .= "Lavu Loyalty:<br>";
		//$vstr .= "<b>" . ext_display_money_with_symbol($item_price) . "</b><br><br>";
		//$vstr .= "<input type='number' name='load_gift_name' style='font-size:20px; color:#777777'>";
		//$vstr .= "<br><br><input type='button' style='font-size:20px; color:#77aa77' value='Apply Discount' onclick='window.location = \"?viewmode=submit_loyalty&submit_id=1\"' />";

		$vstr .= "</td>";
		$vstr .= "</tr>";
		$vstr .= "</table>";
		if(isset($lg_templates) && isset($lg_templates['loyalty-main'])) $vstr = $lg_templates['loyalty-main'];

		echo $vstr;
	}
	else if($viewmode=="submit_loyalty")
	{
		if(isset($_GET['submit_id']))
		{
			$submit_id = $_GET['submit_id'];
			$keyed_amount = verify_amount_is_int(remember_reqvar("amount"));
			$order_id = remember_reqvar("order_id");
			$server_id = remember_reqvar("server_id");
			$server_name = remember_reqvar("server_name");
			$pay_type = remember_reqvar("pay_type");
			$pay_type_id = remember_reqvar("pay_type_id");
			$datetime = DateTimeForTimeZone(locationSetting("timezone"));
			$submit_action = (isset($_GET['submit_action'])) ? $_GET['submit_action'] : "";
			if($submit_id == "awarded" || $submit_id == "discounted") 
			{
				if($submit_action!="adjust_points" && $submit_action!="undo_discount")
					$submit_action = $submit_id;
			}
			else
			{
				//$_SESSION['remember_reqvar_loyalty_for_order_' . $order_id] = $submit_id;
				ext_save_state('remember_reqvar_loyalty_for_order_' . $order_id,$submit_id);
			}

			//_DO:cmd=set_loyalty&discount=type|calc_type|calc|label|redeem_id|type_id
			//type :  name
			//calc_type :  p for percent, d for dollar amount
			//calc 	:  value of discount (.50 = 50%, 2.5 = $2.50)
			//label :  discount label that appears on order screen / receipt
			//redeem_id :  reference number for integrated discounts
			//type_id :  id from discount types table if you choose to reference to a location defined discount type
?>
<style type='text/css'>
	table, td
	{
		.b: 1px solid black;
	}
</style>
<script type='text/javascript'>

	var discount = "";

	function closeLoyaltyWindow()
	{
		window.location = '_DO:cmd=close_overlay'; 
	}

	function award_points()
	{
		window.location = '?submit_id=<?php echo $submit_id; ?>&submit_action=award_points&add_points=$total_award&viewmode=' + viewmode;
	}

	function acceptOrderInfo(order_info_json)
	{
		if (document.getElementById('no_discrepancies') == null)
		{
			return 1;
		}

		var decoded_json = decodeURIComponent(order_info_json);
		var items = JSON.parse(decoded_json).contents;
		var db_discrepancy_detected = false;
		for (var i = 0; i < items.length; i++)
		{
			var quantity = items[i]['quantity'];
			var item = items[i]['item'];
			var failure = true;
			for (var j = 0; j < db_items.length; j++)
			{
				var q2 = db_items[j]['qty'];
				var i2 = db_items[j]['name'];
				if (q2 == quantity && i2 == item)
				{
					failure=false;
				}
			}

			if (failure)
			{
				db_discrepancy_detected = true;
			}
		}

		// document.getElementById('no_discrepancies').style.display = 'none';
		// document.getElementById('discrepancies').style.display = '';
		if (db_discrepancy_detected)
		{
			setTimeout(function()
			{
				window.location = '?submit_id=<?php echo $submit_id; ?>&viewmode=' + viewmode;
			}, 500);
		}
		else
		{
			setTimeout(function()
			{
				document.getElementById('no_discrepancies').style.display = '';
				document.getElementById('discrepancies').style.display = 'none';
			}, 100);
		}

		return 1;
	}

	window.onload = function()
	{
		window.location = '_DO:cmd=get_active_order_info&js=acceptOrderInfo(\'[ORDER_INFO]\');'
	}
</script>
<?php
			// echo var_dump($_GET);

			$item_id_list = "";
			$total_award = 0;
			$js_items = "";
			$content_query = lavu_query("SELECT * FROM `order_contents` WHERE `order_id`='[1]'",$order_id);
			$print_award_string = "Award Breakdown[c::";
			while($content_read = mysqli_fetch_assoc($content_query))
			{
				$item_name = $content_read['item'];
				$item_id = $content_read['item_id'];
				$item_qty = $content_read['quantity'];
				$item_unit_price = $content_read['price'];
				$subtotal_with_mods = $content_read['subtotal_with_mods'];
				$discount_amount = $content_read['discount_amount'];
				$total_after_discount = $content_read['after_discount'];
				$tax_amount = $content_read['tax_amount'];
				$total_with_tax = $content_read['total_with_tax'];
				$total_before_tax = $total_with_tax * 1 - $tax_amount * 1;
				$per_unit_cost = $total_before_tax / $item_qty * 1;

				$js_items .= "{'name':'".str_replace("'", "\'", $item_name)."', 'qty':".$item_qty."},";

				$item_query = lavu_query("SELECT * FROM `menu_items` WHERE `id`='[1]'",$item_id);
				if(mysqli_num_rows($item_query))
				{
					$item_read = mysqli_fetch_assoc($item_query);
					$special2 = strtolower($item_read['hidden_value3']); // special details 2
					$award_parts = explode("award ",$special2);
					if(count($award_parts) > 1)
					{
						$point_parts = explode(" point",$award_parts[1]);
						if(count($point_parts) > 1)
						{
							$point_str = trim($point_parts[0]);

							if(is_numeric($point_str))
							{
								$min_cost = 0;
								$min_cost_parts = explode("minimum cost ",$point_parts[1]);
								if(count($min_cost_parts) > 1)
								{
									$min_cost_str = trim(str_replace("of ","",$min_cost_parts[1]));
									if(is_numeric($min_cost_str))
									{
										$min_cost = $min_cost_str * 1;
									}
								}

								if($per_unit_cost * 1 >= $min_cost * 1)
								{
									//echo $item_name . " - per unit: $per_unit_cost - award points: " . $point_str . " min cost: $min_cost<br>";
									if($item_id_list!="") $item_id_list .= ", ";
									$item_id_list .= $item_id . " x " . $item_qty;
									$total_award += $point_str * $item_qty;
									$print_award_string .= $item_name . " X " . $item_qty . "*" . ($point_str * $item_qty) . ":";
								}
							}
						}
					}
				}
				//echo $item_name . " - " . $total_before_tax . "<br>";
				//foreach($content_read as $key => $val) echo $key . " = " . $val . "<br>";
			}

			echo "<script type='text/javascript'>";
			echo "db_items = [".$js_items."]";
			echo "</script>";

			$vstr = "";
			$vstr .= "<table cellpadding=12>";
			$vstr .= "<tr>";
			$vstr .= "<td align='center' style='font-size:20px; color:#aaaaaa; width:".$use_view_width."px'>";
			if(isset($lg_templates) && isset($lg_templates['loyalty-start'])) $vstr = $lg_templates['loyalty-start'];
			echo $vstr;

			//echo "<br><br>processing...<br><br>";
			//echo $submit_id;
			$status_str = "";
			$card_id = ($submit_id != "awarded" && $submit_id != "discounted") ? $submit_id : ext_get_saved_state_info('remember_reqvar_loyalty_for_order_'.$order_id, "name");
			if(strlen($chain_id) > 1) {
				#load all dataname existing in chain
				require_once('/home/poslavu/public_html/admin/sa_cp/manage_chain_functions.php');
				$chainedDataname = array();
				$chainedRestaurants =get_restaurants_by_chain($chain_id );	#get all chained restaurants
				foreach($chainedRestaurants as $restaurants) {
					$chainedDataname[] = $restaurants['data_name'];
				}

				$card_query = mlavu_query("SELECT * FROM `poslavu_MAIN_db`.`lavu_gift_cards` WHERE `deleted` = 0 AND `inactive` = 0 AND `name`='[1]' AND (`dataname` IN('".implode("','", $chainedDataname)."') OR `chainid`='[2]')", $card_id, $chain_id);
			}
			else {
				$card_query = mlavu_query("SELECT * FROM `poslavu_MAIN_db`.`lavu_gift_cards` WHERE `deleted` = 0 AND `inactive` = 0 AND `name`='[1]' AND `dataname`='[2]'", $card_id, $data_name);
			}
			if(mysqli_num_rows($card_query))
			{
				$card_read = mysqli_fetch_assoc($card_query);
				$card_dbid = $card_read['id'];
				$gift_expires = $card_read['expires'];
				if(empty($gift_expires)){
					$expired = false;
				}
				else{
					$expired = (strtotime($gift_expires) - strtotime($datetime)) < 0;
				}
				if($expired){
					echo "<script type='text/javascript'>";
					echo "  setTimeout(function(){";
					echo "    componentAlert('Card Expired','Continue to re-register card as new'); ";
					echo "  }, 10);";
					echo "</script>";
					$gift_balance = "0";
					$points_balance = "0";
				}
				else {
					$points_balance = $card_read['points'] * 1;
					$gift_balance = $card_read['balance'];
				}
			}
			else{
				$card_dbid = "new";
				$points_balance = "0";
				$gift_balance = "0";
			}
			if($submit_action=="awarded")
			{
				//$awstr = 'remember_reqvar_description_for_order_' . $order_id;
				$awstr = 'remember_reqvar_loyalty_for_order_' . $order_id;
				//if(isset($_SESSION[$awstr]))
				//{
				//	echo $_SESSION[$awstr];
				//}
				$saved_state_info = ext_get_saved_state_info($awstr,"message");
				$card_id = ext_get_saved_state_info($awstr, "name");
				$card_font_size = strlen($card_id) < 15 ? 20 : 15;
				if($saved_state_info!="")
				{
					echo "<h4>Status:</h4>";
					echo "<p style='text-align:right'><b style='float:left'>Awarded: </b>";
					echo $saved_state_info;
					echo "<br/><b style='float:left'>Card</b><span style='font-size:".$card_font_size."px'>";
					echo $card_id;
					echo "</span></p>";
					$saved_state_awarded = ext_get_saved_state_info($awstr,"awarded");

					if($total_award * 1 != $saved_state_awarded * 1)
					{
						$btn_points_title = "";
						if($total_award * 1 > $saved_state_awarded)
						{
							echo "<h4>Notice:</h4>";
							echo "More Points are Available<br>";
							echo "<p style='text-align:right'><b style='float:left'>Points Available: </b>";
							echo ext_display_points($total_award - $saved_state_awarded) . "</p>";
							$btn_points_title = "Add Points";
						}
						else if($total_award * 1 < $saved_state_awarded)
						{
							echo "<h4>Notice:</h4>";
							echo "Awarded Too Many Points!<br>";
							echo "<p style='text-align:right'><b style='float:left'>Points Over: </b>";
							echo ext_display_points($saved_state_awarded - $total_award) . "</p>";
							$btn_points_title = "Adjust Points";
						}

						$add_points = $total_award - $saved_state_awarded;
						echo "<br><input class='btn_light_long' type='button' style='font-size:20px' value='$btn_points_title' onclick='window.location = \"?submit_id=$submit_id&submit_action=adjust_points&add_points=$add_points&viewmode=\" + viewmode;' /><br>";
					}

					//echo "<br>Awarded: " . $saved_state_awarded . "<br>";
				}
				//if(isset($_SESSION[$awstr])) echo "<br><br>" . $_SESSION[$awstr]['state'] . "<br>" . $_SESSION[$awstr]['info'];
			}
			else if($submit_action=="award_points" || $submit_action=="adjust_points")
			{
				$add_points = (isset($_GET['add_points']))?$_GET['add_points']:$total_award;
				$awstr = 'remember_reqvar_loyalty_for_order_' . $order_id;
				$saved_state_awarded = ext_get_saved_state_info($awstr,"awarded");
				$saved_state_name = ext_get_saved_state_info($awstr,"name");
				if(!is_numeric($saved_state_awarded)) $saved_state_awarded = 0;
				if($saved_state_name!="" && $submit_id=="awarded")
				{
					$submit_id = $saved_state_name;
				}
				/*echo $awstr . "<br>";
				echo $saved_state_awarded . "<br>";
				echo $saved_state_name . "<br>";
				echo $submit_id . "<br>";
				exit();*/

				$track_total_award = $total_award;
				if($add_points==($total_award - $saved_state_awarded) || $add_points==($saved_state_awarded - $total_award))
				{
					$total_award = $add_points;
				}

				$new_balance = $points_balance + $total_award;
				$show_new_balance = ext_display_points($new_balance);
				$sign = ($total_award > 0) ? "+ " : "- ";
				update_point_data($data_name, $chain_id, $submit_id, $datetime, $card_dbid, $new_balance, $total_award, "", $print_award_string);
				$show_total_award = ext_display_points($track_total_award);

				//$_SESSION['remember_reqvar_loyalty_for_order_' . $order_id] = "awarded";
				//$_SESSION['remember_reqvar_description_for_order_' . $order_id] = "Awarded $total_award to $submit_id";
				ext_save_state('remember_reqvar_loyalty_for_order_' . $order_id,"awarded",array("message"=>"$show_total_award","last_awarded"=>$total_award,"awarded"=>$track_total_award,"name"=>$submit_id));

				echo "<script type='text/javascript'>";
				echo "window.location = '_DO:cmd=close_overlay';";
				echo "</script>";
				exit();
			}
			else if($submit_action=="discounted") {
				$awstr = 'remember_reqvar_loyalty_for_order_' . $order_id;
				//if(isset($_SESSION[$awstr]))
				//{
				//	echo $_SESSION[$awstr];
				//}
				$saved_state_info = ext_get_saved_state_info($awstr,"message");
				$points_balance = ext_get_saved_state_info($awstr, "points_balance");
				$card_id = ext_get_saved_state_info($awstr, "name");

				if($saved_state_info!="")
				{
					echo "<br><br/>";
					echo "<b>Applied Reward:</b><br/>";
					// echo $submit_id;
					echo $saved_state_info;
					echo "<br/><br/><b>Card #:</b><br/>";
					echo $card_id;
					// $saved_state_discount_info = ext_get_saved_state_info($awstr,"discount_info");
					echo "<br/><br/><br/><input class='btn_light_super_long' type='button' style='font-size:20px' value='Undo & Refund Points' onclick='window.location = \"?submit_id=$submit_id&submit_action=undo_discount&card_dbid=$card_dbid&points_balance=$points_balance&reward_cost=$reward_cost&viewmode=\" + viewmode;' /><br>";
					//echo "<br>Awarded: " . $saved_state_awarded . "<br>";
				}
			}
			else if($submit_action == "undo_discount")
			{
				$awstr = 'remember_reqvar_loyalty_for_order_' . $order_id;
				$reward_cost = ext_get_saved_state_info($awstr, "reward_cost");
				$card_dbid = ext_get_saved_state_info($awstr, "card_dbid");
				$new_balance = ext_get_saved_state_info($awstr, "old_balance");
				$old_balance = ext_get_saved_state_info($awstr, "new_balance"); //old is new and new is old.
				$card_id = ext_get_saved_state_info($awstr, "submit_id");
				// echo $card_id; 
				update_point_data($data_name, $chain_id, $card_id, $datetime, $card_dbid, $new_balance, "+ ".($new_balance - $old_balance), "Undo Discount", "");
				ext_save_state($awstr,"");
				echo "Please Wait";
				echo "<script type='text/javascript'>";

				echo "window.location = '_DO:cmd=set_loyalty&discount=check|" . $reward_code . "|0|none|0|0 _DO:cmd=close_overlay';";
				echo "setTimeout(function(){window.location = \"?submit_id=$card_id&submit_action=''&viewmode=\" + viewmode}, 200);";
				echo "</script>";
			}
			else if($submit_action == "apply_discount")
			{
				if (isset($_GET['reward_id']))
				{
					$reward_id = $_GET['reward_id'];
					$reward_label = $_GET['reward_label'];
					$reward_use_or_unlock = $_GET['reward_use_or_unlock'];
					$reward_cost = $_GET['reward_cost'];
					$reward_code = $_GET['reward_code'];
					$reward_calc = $_GET['reward_calc'];
					$order_info = urldecode($_GET['order_info']);
					$order_info = stripslashes($order_info);
					$order_info = json_decode($order_info, 1);
					if(isset($data_name)) {
						$dataname = $_GET['dataname'];
					}
					$pos_dataname="`poslavu_".$dataname."_db`";
					$loyalty_query = mlavu_query("SELECT `level` FROM $pos_dataname.`discount_types` WHERE `id`='[1]'",$reward_id);
					$loyalty_read = mysqli_fetch_array($loyalty_query);
					if ($loyalty_read['level'] != 'checks' && !isset($_GET['item_icid'])) {
						if(!isset($_GET['order_info'])) {
							$url = $_SERVER['REQUEST_URI'];
							$url = $url."&order_id=".$order_id;
							$send_url=str_replace('?','#',$url);
							echo "<script type='text/javascript'>";
							echo "window.location = '_DO:cmd=get_active_check_details&url=".$send_url."';";
							echo "</script>";
						} else {
							$available_discounts_html = "";
							$available_discounts_html = "<div id='discrepancies' style='display:'><h4>Select the item you would like to discount</h4></div>";
							$available_discounts_html = $available_discounts_html . "<div style='overflow: auto;height: 315px;'>";
							foreach ($order_info[contents] as $order_data) {
								$icid=$order_data[icid];
								$url = $url."&order_id=".$order_id;
								$onclick = "window.location = \"?submit_id=$submit_id&submit_action=apply_discount&reward_id=$reward_id&reward_label=$reward_label&reward_use_or_unlock=$reward_use_or_unlock&reward_calc=$reward_calc&reward_code=$reward_code&reward_cost=$reward_cost&order_id=$order_id&viewmode=submit_loyalty&dataname=$dataname&item_icid=$icid\"";
								$available_discounts_html = $available_discounts_html . "<div style='padding:6px;' onclick='$onclick'> ".urldecode($order_data[item])." </div>";
							}
							if ($loyalty_read['level'] == '') {
								$onclick = "window.location = \"?submit_id=$submit_id&submit_action=apply_discount&reward_id=$reward_id&reward_label=$reward_label&reward_use_or_unlock=$reward_use_or_unlock&reward_calc=$reward_calc&reward_code=$reward_code&reward_cost=$reward_cost&order_id=$order_id&viewmode=submit_loyalty&dataname=$dataname&item_icid=checks\"";
								$available_discounts_html = $available_discounts_html . "<div style='padding:6px;' onclick='$onclick' > Apply Discount to Check </div>";
							}
							$available_discounts_html = $available_discounts_html . "</div><br/>";
							echo $available_discounts_html;exit();
						}
					}
						
					$awstr = 'remember_reqvar_loyalty_for_order_' . $order_id;
					$saved_state_name = ext_get_saved_state_info($awstr,"name");
					if($saved_state_name!="" && $submit_id=="awarded")
					{
						$submit_id = $saved_state_name;
					}

					if ($loyalty_read['level'] == 'checks' || isset($_GET['item_icid'])) {
						$new_balance = $points_balance - $reward_cost;
						update_point_data($data_name, $chain_id, $submit_id, $datetime, $card_dbid, $new_balance, "- ".$reward_cost, 'Applied Discount[c*'.$reward_label.':',"");
						ext_save_state('remember_reqvar_loyalty_for_order_' . $order_id,"discounted",array("message"=>"$reward_label","last_awarded"=>$total_award,"discount_info"=>$reward_label,"name"=>$submit_id, "card_dbid"=>$card_dbid, "reward_cost"=>$reward_cost, "old_balance"=>$points_balance, "new_balance"=>$new_balance, "submit_id"=>$submit_id));
					}



					echo "<script type='text/javascript'>";
					echo "setTimeout(function(){";
					if ($loyalty_read['level'] == 'checks' || $_GET['item_icid'] == 'checks') {
						echo "window.location = '_DO:cmd=set_loyalty&discount=check|" . $reward_code . "|" . $reward_calc . "|" . $reward_label . "|0|" . $reward_id . "_DO:cmd=close_overlay';";
					} else {
						echo "window.location = '_DO:cmd=set_loyalty&discount=item|" . $reward_code . "|" . $reward_calc . "|" . $reward_label . "|0|" . $reward_id . "||".$_GET['item_icid']."_DO:cmd=close_overlay';";
					}
					echo "}, 250);";
				//	echo "setTimeout(function(){window.location = \"?submit_id=$submit_id&submit_action=discounted&viewmode=\" + viewmode}, 1000);";
					echo "</script>";
					exit();
				}

			}
			else if($submit_action=="")
			{
				echo "<div id='discrepancies' style='display:'><h4>Please Wait...</h4></div>";
				echo "<div id='no_discrepancies' style='display:none'>";
				if($card_dbid != 'new')
				{
					$status_str .= "<br/><b>Existing Loyalty Card:</b><br/><br/>";
					$status_str .= "<input class='btn_light_super_long' type='button' style='font-size:20px' value='$submit_id' onclick='window.location = \"?viewmode=loyalty&reset=1&amount=$keyed_amount&order_id=$order_id&pay_type=".urlencode($pay_type)."&pay_type_id=$pay_type_id&server_id=$server_id&server_name=".urlencode($server_name)."\";' />";

				}
				else
				{
					$status_str .= "<br/><b>New Loyalty Card:</b><br/><br/>";
					$status_str .= "<input class='btn_light_super_long' type='button' style='font-size:20px' value='$submit_id' onclick='window.location = \"?viewmode=loyalty&reset=1&amount=$keyed_amount&order_id=$order_id&pay_type=".urlencode($pay_type)."&pay_type_id=$pay_type_id&server_id=$server_id&server_name=".urlencode($server_name)."\";' />";
				}
				$status_str .= "<br/><br/><b>Balance: </b>" . ext_display_points($points_balance);
				if($submit_action=="")
				{
					echo $status_str;
					echo "<br/><br/><hr>";
				}

				$reward_query = lavu_query("SELECT * FROM `discount_types` WHERE `special`='lavu_loyalty' AND `_deleted`='0' AND `loyalty_cost`<='[1]' AND `min_access`<='[2]'", $points_balance, 4);
				$available_discounts_html = "";
				$onchange = "window.location = \"?submit_id=$submit_id&submit_action=apply_discount\" + this.value + \"&viewmode=\" + viewmode";
				$disableSelect = (mysqli_num_rows($reward_query) > 0) ? "" : "disabled style='opacity:0.4'";
				$discounts_opacity = "opacity:0.4;";
		    	$available_discounts_html = $available_discounts_html . "<select $disableSelect class='btn_light' id='discount_select' onchange='$onchange'><option value=''>Select Discount</option>";
				if(mysqli_num_rows($reward_query) > 0) {
					$discounts_opacity = "opacity:1.0;";
					while ($row = mysqli_fetch_array($reward_query, MYSQL_ASSOC)) {
					    $reward_id = $row['id'];
					    $reward_label = $row['label'];
					    $reward_calc = $row['calc'];
					    $reward_code = $row['code'];
					    $reward_use_or_unlock = $row['loyalty_use_or_unlock'];
					    $reward_cost = $row['loyalty_cost'];
					    $unlock_data='';
					    if($reward_use_or_unlock=='2'){ $unlock_data = 'unlock'; }
					    
					    if($reward_code == 'p') {
					    	$tmp = floatval($reward_calc) * 100;
					    	$reward_text = $tmp . "%";
					    }
					    else 
					    	$reward_text = ext_display_money_with_symbol($reward_calc);
						$reward_text = $reward_label . ": " . $reward_text . " off ".$unlock_data." at " . $reward_cost . " points";
						if($reward_use_or_unlock=='2'){ $reward_cost = '0'; }

						// $available_discounts_html = $available_discounts_html . "<option value='_DO:cmd=set_loyalty&discount=check|" . $reward_code . "|" . $reward_calc . "|" . $reward_label . "|0|" . $reward_id . "'>" . $reward_text . "</option>";
					    
					    $available_discounts_html = $available_discounts_html . "<option value='&reward_id=" . $reward_id ."&reward_label=". $reward_label ."&reward_use_or_unlock=". $reward_use_or_unlock ."&reward_calc=". $reward_calc ."&reward_code=". $reward_code ."&reward_cost=". $reward_cost ."'>" . $reward_text . "</option>";

					    // printf("%s: %s off for %s points<br/>", $reward_label, $reward_text, $reward_cost);
					    // printf('%s: balance', $points_balance);
					}
					// $available_discounts_html = $available_discounts_html . "<br><input type='button' style='font-size:20px' value='Apply Discount' onclick='window.location = \"?submit_id=$submit_id&submit_action=apply_discount&r\" + reward_data + \"&viewmode=\" + viewmode;' /><br>";
				}
				$awards_opacity = ($total_award > 0) ? "opacity:1.0;" : "opacity:0.4;";
				$available_discounts_html = $available_discounts_html . "</select><br/>";
				echo "<br/>";
				echo "<table cellpadding='8' style='text-align:center; border-collapse:collapse;'><tr><td style='border-right:1px solid'>";
				echo "<b style='".$discounts_opacity."'>Discounts:</b><p style='".$discounts_opacity."'>" . mysqli_num_rows($reward_query) . "</p>";
				echo "</td><td >";
				echo "<b  style='".$awards_opacity."'>Award: </b><p style='".$awards_opacity."'>$total_award</p>";
				echo "</td></tr><tr><td style='border-right:1px solid'>";
				echo $available_discounts_html;
				echo "</td><td >";
				// if($total_award > 0)
				$disabled = ($total_award > 0) ? "" : "disabled";
				echo "<input class='btn_light' type='button' $disabled value='Award Points' onclick='award_points()' /><br>";
				echo "</div>";
				echo "</td></tr></table>";

			}

			echo "</td></tr></table>";

		}
	}
?>