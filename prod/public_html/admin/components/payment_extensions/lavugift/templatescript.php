<script language='javascript'>
<?php
	if(isset($_REQUEST['available_DOcmds']))
	{
		$available_DOcmds = $_REQUEST['available_DOcmds'];
	}

	if(isset($_REQUEST['lavu_loyalty']))
	{
		$viewmode = "loyalty";
	}
	else if(isset($_POST['item_id']))
	{
		$viewmode = "modifier";
	}
	else if(isset($_GET['viewmode']))
	{
		$viewmode = $_GET['viewmode'];
	}
	else
	{
		$viewmode = "payment";
	}
	echo "viewmode = '".$viewmode."';\n";
	echo "available_DOcmds = '".$available_DOcmds."';\n";
?>

	var company_id = "<?php echo reqvar('company_id'); ?>";
	var loc_id = "<?php echo reqvar('loc_id'); ?>";
	var device_prefix = "<?php echo reqvar('device_prefix'); ?>";

	var location_name = "<?php echo htmlspecialchars(strtoupper($location_info['title'])); ?>";
	var location_address = "<?php echo htmlspecialchars(strtoupper($location_info['address'])); ?>";
	var location_city = "<?php echo htmlspecialchars(strtoupper($location_info['city'])); ?>";
	var location_state = "<?php echo htmlspecialchars(strtoupper($location_info['state'])); ?>";
	var location_zip = "<?php echo htmlspecialchars(strtoupper($location_info['zip'])); ?>";

	var device_failed = 1;
	var comm_mode = "";
	var hold_comm_mode = "";
	var cFS = String.fromCharCode(28);
	var cEOT = String.fromCharCode(04);

	var confirm_mode = "";

	var merchant_id = "<?php echo $integration_info['integration1']; ?>";
	var terminal_ip = "";

	var decimal_places = "<?php echo htmlspecialchars($location_info['disable_decimal']); ?>";

	var action = "<?php echo reqvar('action'); ?>";
	var admin_type = "<?php echo reqvar('admin_type'); ?>";
	var amount = "<?php echo reqvar('amount'); ?>";
	var order_id = "<?php echo reqvar('order_id'); ?>";

	var short_order_id = order_id.replace("-", "");
	var soi_len = short_order_id.length;
	if (soi_len > 7) short_order_id = short_order_id.substring((soi_len - 7), soi_len);

	var internal_id = "";
	var for_deposit = "<?php echo reqvar('for_deposit'); ?>";
	var	is_deposit = "<?php echo reqvar('is_deposit'); ?>";
	var tx_info = {};

	var server_id = "<?php echo reqvar('server_id'); ?>";
	var server_name = "<?php echo reqvar('server_name'); ?>";
	var server_list = tryParseJSON('<?php echo lavu_json_encode($server_list); ?>');
	var register = "<?php echo reqvar('register'); ?>";

	var orig_action = "<?php echo reqvar('orig_action'); ?>";
	var orig_internal_id = "<?php echo reqvar('orig_internal_id'); ?>";
	var orig_authcode = "<?php echo reqvar('orig_authcode'); ?>";
	var void_notes = "<?php echo reqvar('void_notes'); ?>";
	var refund_notes = "<?php echo reqvar('refund_notes'); ?>";

	var report_info = {};
	var report_clerk_id = "";

	var tip_entry_mode = "";
	var tip_entry_mode_setto = "";

	var month_num_to_name = [];
	month_num_to_name[0] = "";
	month_num_to_name[1] = "JAN";
	month_num_to_name[2] = "FEB";
	month_num_to_name[3] = "MAR";
	month_num_to_name[4] = "APR";
	month_num_to_name[5] = "MAY";
	month_num_to_name[6] = "JUN";
	month_num_to_name[7] = "JUL";
	month_num_to_name[8] = "AUG";
	month_num_to_name[9] = "SEP";
	month_num_to_name[10] = "OCT";
	month_num_to_name[11] = "NOV";
	month_num_to_name[12] = "DEC";

	var using_new_version_gift = "<?php echo $using_new_version_gift ?>";
	var using_rfid = "<?php echo (reqvar("pay_type") == "MEAL PLAN") ?>";
	var pay_type = "<?php echo reqvar("pay_type") ?>";

	function respEl(resp, i) {

		var el = "";

		if (typeof resp != 'undefined') {
			if (resp instanceof Array) {
				if (resp.length > i) el = resp[i];
			}
		}

		return el;
	}

	function convertAmount(amt) {

		var AMT = parseInt(amt);
		var dp = parseInt(decimal_places);
		var cAmt = (dp > 0)?(AMT / Math.pow(10, dp)):AMT;

		return parseFloat(cAmt).toFixed(dp);
	}

	function displayMoney(amt) {

		return parseFloat(amt).toFixed(decimal_places);
	}

	function displayMoneyWithSymbol(amt) {

		var monetary_symbol = "<?php echo htmlspecialchars($location_info['monitary_symbol']); ?>";
		if (monetary_symbol == "") monetary_symbol = "$";
		var left_or_right = "<?php echo htmlspecialchars($location_info['left_or_right']); ?>";
		if (left_or_right == "") left_or_right = "left";

		var rtn_str = "";
		if (left_or_right == "left") rtn_str += monetary_symbol + " ";
		rtn_str += displayMoney(amt) + "";
		if (left_or_right == "right") rtn_str += " " + monetary_symbol;

		return rtn_str;
	}

	function enableRfid()
	{
		window.setTimeout( function() { window.location = '_DO:cmd=stopListeningForSwipe_DO:cmd=startListeningForKEI' }, 500 );
	}

	function enableSwipe()
	{
		window.setTimeout( function() { window.location = '_DO:cmd=stopListeningForKEI_DO:cmd=startListeningForSwipe' }, 500 );
	}

	function startListening()
	{
		if (available_DOcmds.indexOf('startListeningForSwipe') != -1)
		{
			if (using_rfid)
			{
				// LP-3878 - Had to stop listening for RFID and Card Swipes at the same time because NYU's smart printer could only do one at a time
				//window.setTimeout( function() { window.location = '_DO:cmd=startListeningForKEI_DO:cmd=startListeningForSwipe' }, 500 );
				window.setTimeout( function() { window.location = '_DO:cmd=startListeningForKEI' }, 500 );
			}
			else
			{
				window.location = '_DO:cmd=startListeningForSwipe';
			}
		}
		else
		{
			// Printing errors here causes them to appear too often...
		}
	}

	String.prototype.trimLeft = function(charlist) {
		if (charlist === undefined)
			charlist = "\s";

		return this.replace(new RegExp("^[" + charlist + "]+"), "");
	}

	String.prototype.trimRight = function(charlist) {
		if (charlist === undefined)
			charlist = "\s";

		return this.replace(new RegExp("[" + charlist + "]+$"), "");
	}

	String.prototype.trim = function(charlist) {
		return this.trimLeft(charlist).trimRight(charlist);
	}

	var swiped = false;
	function handleCardSwipe(swipeString, hexString, reader, encrypted)
	{
		if (swiped)
		{
			return;
		}

		swiped = true;

		var cardNumber	= "";
		var cardRead	= false;

		// alert(available_DOcmds);
		//componentAlert("Card swiped: swipeString=" + swipeString + ", hexString=" + hexString + ", reader=" + reader + ", encrypted=" + encrypted, "");
		//alert(swipeString);
		// componentAlert("reader= " + reader);

		if (encrypted == 0)
		{
			swipeString = decodeURIComponent(swipeString);

			if (swipeString.length > 0)
			{
				var tracks = swipeString.split("?");
				for (var i = 0; i < tracks.length; i++)
				{
					var trk = tracks[i].replace(/[^a-z;=\d]+/ig,'');
					if (trk.substring(0, 1) == ";")
					{
						var track2 = trk.split("=");
						cardNumber = track2[0].trim("; ");
						if (cardNumber.length > 0)
						{
							break;
						}
					}
				}
			}
		}
		else
		{
			switch (reader)
			{
				case "idynamo":

					swipeString = decodeURIComponent(swipeString);

					var tracks = swipeString.split("?");

					for (var i = 0; i < tracks.length; i++)
					{
						if (tracks[i].substring(0, 1) == ";")
						{
							cardNumber = tracks[i].split(';')[1].split('=')[0];
						}
					}

					break;

				case "llsswiper":

					if (hexString.length > 0)
					{
						var actual_swipe = fromHex(hexString);
						if (actual_swipe.length == 1714)
						{
							cardRead = true;
							cardNumber = parseCardNumberFromHeartlandE3Swipe(actual_swipe);
						}
						else
						{
							var tracks		= actual_swipe.split("?");
							var track_parts	= tracks[0].split(";");

							cardNumber	= track_parts[1];
							cardRead	= (cardNumber.length > 0);
						}
					}

					if (!cardRead && (swipeString.length > 0))
					{
						var tracks = swipeString.split("?");

						for (var i = 0; i < tracks.length; i++)
						{
							var trk = tracks[i].replace(/[^a-z;=\d]+/ig,'');

							if (trk.substring(0, 1) == ";")
							{
								var track2 = trk.split("=");

								cardNumber = track2[0].trim("; ");
								if (cardNumber.length > 0)
								{
									break;
								}
							}
						}
					}

					break;

				default:
					break;
			}
		}

		if (isNumber(cardNumber))
		{
			window.setTimeout(
				function()
				{
					var input_field = document.getElementById("card_number_input");

					input_field.value = cardNumber;
					submitCardNumber();
				},
				50
			);
		}
		else
		{
			swiped = false;
		}

		return 1;
	}

	function handleKEI(cardNumber)
	{
		if (swiped)
		{
			return;
		}

		swiped = true;

		if (isNumber(cardNumber))
		{
			window.setTimeout(
				function()
				{
					if (using_new_version_gift)
					{
						keyval = cardNumber;
						customer_name_submit();
					}
					else
					{
						var input_field = document.getElementById("card_number_input");
						input_field.value = cardNumber;
						submitCardNumber();
					}

					/// Investigate whether we need to stop listening to KEI input here to prevent memory leak in POS app
					///window.location = '_DO:cmd=stopListeningForKEI';
				},
				50
			);
		}

		return 1;
	}


	function parseCardNumberFromHeartlandE3Swipe(swipe_string)
	{
		var cardNumber = "";

		var masked_track_1_length = parseInt(toHex(swipe_string.substring(492, 493)), 16);
		var masked_track_2_length = parseInt(toHex(swipe_string.substring(493, 494)), 16);

		if (masked_track_1_length != 0)
		{
			var masked_track_1 = swipe_string.substring(495, (495 + masked_track_1_length));

			cardNumber = cardNumberFromMaskedTrack(masked_track_1, /^%([A-Z])([0-9]{1,21})\^([^\^]{2,26})\^([0-9]{4}|\^)([0-9]{3}|\^)([^\?]+)\?$/, 2);
		}

		if (masked_track_2_length!=0 && cardNumber.length==0)
		{
			var masked_track_2 = swipe_string.substring(655, (655 + masked_track_2_length));

			cardNumber = cardNumberFromMaskedTrack(masked_track_2, /;([0-9\*]{1,21})=([\d\*]{4}|=)([\d\*]{3}|=)([\d\*]*)?\?/, 1);

			if (cardNumber.length == 0)
			{
				cardNumber = cardNumberFromMaskedTrack(masked_track_2, /;([0-9\*]{1,21})\?/, 1);
			}
		}

		return cardNumber;
	}

	function cardNumberFromMaskedTrack(masked_track, regex, index)
	{
		if (masked_track == null)
		{
			return "";
		}

		var matches = masked_track.match(regex);
		if (matches == null)
		{
			return "";
		}

		if (matches.length <= index)
		{
			return "";
		}

		var cn = matches[index];
		if (typeof cn != 'string')
		{
			return "";
		}

		return cn;
	}

	function isNumber(n)
	{
		return !isNaN(parseFloat(n)) && isFinite(n);
	}

	function componentAlert(title, message)
	{
		window.location = "_DO:cmd=alert&title=" + encodeURIComponent(title) + "&message=" + encodeURIComponent(message);
	}

	function fromHex(hex)
	{
		var str = "";

		for (var i = 0; i < hex.length; i += 2)
		{
			str += String.fromCharCode(parseInt(hex.substr(i, 2), 16));
		}

		return str;
	}

	function closeOverlay()
	{
		var use_cmd = (viewmode=="modifier" || viewmode=="submit_modifier")?"cancel":"close_overlay";

		// Set customer if available for Meal Plan
		var customer_name = "<?php echo reqvar("customer_name") ?>";
		var customer_id = "<?php echo reqvar("customer_id") ?>";
		if (customer_name && customer_id)
		{
			use_cmd += "&set_info="+ encodeURIComponent(customer_name) +"|o|"+ encodeURIComponent(customer_id) +"|o|{}";
		}

		window.location = "_DO:cmd=" + use_cmd;
	}

	function showActivityShade(message)
	{
		document.getElementById('activity_message').innerHTML = message;
		document.getElementById('activity_shade').style.display = "block";
	}

	function hideActivityShade()
	{
		document.getElementById('activity_shade').style.display = "none";
	}

	function hideAllBut(but)
	{
		var divs = [ "enter_card_number", "response_panel" ];
		for (var d = 0; d < divs.length; d++)
		{
			var did = divs[d];
			if (document.getElementById(did))
			{
				document.getElementById(did).style.display = (did == but)?"block":"none";
			}
		}
	}

	function initialize()
	{
		if (viewmode=="modifier" || viewmode=="loyalty" || viewmode=="payment")
		{
			hideActivityShade();
			hideAllBut("enter_card_number");
			document.getElementById('card_number_input').value = "";
		}

		if (viewmode=="modifier" || viewmode=="submit_modifier")
		{
			window.setTimeout(
				function()
				{
					window.location = "_DO:cmd=set_scrollable&c_name=lavugift&set_to=0";
				},
				50
			);
		}
	}

	function numberPadButtonPressed(btn)
	{
		var input_field = document.getElementById("card_number_input");
		var input_text = input_field.value;
		var len = input_text.length;

		if (btn == "back")
		{
			if (len > 0)
			{
				var i = (len - 1);
				var last_char = input_text.charAt(i);
				input_field.value = input_text.substring(0, i);
			}
		}
		else if (len < 26)
		{
			 input_field.value = input_text + "" + btn;
		}
	}

	function submitCardNumber()
	{
		var input_field = document.getElementById("card_number_input");
		var card_number = input_field.value;

		if (card_number.length == 0)
		{
			componentAlert("Invalid Card Number", "Please try again.");
			return;
		}

		showActivityShade("Checking card number...");

		window.setTimeout(function(){ submitCardNumberExec(); }, 50);
	}

	function submitCardNumberExec()
	{
		var input_field = document.getElementById("card_number_input");
		var card_number = input_field.value;

		if (card_number.length > 0)
		{
			window.location = "?submit_id=" + card_number + "&viewmode=submit_" + viewmode + "&pay_type="+ encodeURIComponent(pay_type);
		}
	}

	function loadCard(card_number)
	{
		showActivityShade("Loading card...");

		window.setTimeout(function(){ loadCardExec(card_number); }, 50);
	}

	function loadCardExec(card_number)
	{
		window.location = "?submit_id=" + card_number + "&submit_action=load_card&viewmode=" + viewmode;
	}

	function showApproved(message) {

		hideActivityShade();
		document.getElementById("approved_message").innerHTML = message;
		hideAllBut("approved_panel");
	}

	function transactionFailed(title, message) {

		hideActivityShade();
		document.getElementById("response_title").innerHTML = title;
		document.getElementById("response_text").innerHTML = message;
		hideAllBut("response_panel");
	}

</script>
