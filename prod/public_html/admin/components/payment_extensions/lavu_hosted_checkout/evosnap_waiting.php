<?php

	error_reporting(E_ALL);

	require_once(__DIR__."/../../../lib/gateway_lib/hosted_checkout/util.php");
	require_once(__DIR__."/../../../lib/gateway_lib/hosted_checkout/evosnap_util.php");

	if ($debug_evosnap) error_log("evosnap_waiting: ".json_encode($_REQUEST));

	if (!empty($_REQUEST['order_identifier']))
	{
		ClientCallback();
	}
	else if (!empty($_REQUEST['apikey']))
	{
		EvoCallback();
	}
	else
	{
		error_log(__FILE__." ".json_encode($_REQUEST));
	}

	function ClientCallback()
	{
		global $debug_evosnap;

		$merchant_order_id = reqvar("order_identifier", "");

		$session_data = getLocationProcessingInfo($merchant_order_id);
		if ($session_data === false)
		{
        	return;
		}

		$dataname		= $session_data['dataname'];
		$location_id	= $session_data['location_id'];
		$order_id		= $session_data['order_id'];

		$elapsed = 0;

		if (isset($session_data['EVO_RESPONSE']))
		{
			if ($debug_evosnap) error_log("evosnap_waiting got EVO_RESPONSE");

			applyEvoTransaction($dataname, $location_id, $order_id, $session_data);

			echo "Payment logged!";

			return;
		}
		else if (isset($session_data['callback_start_ts']))
		{
			$elapsed = (time() - $session_data['callback_start_ts']);

			if ($debug_evosnap) error_log("evosnap_waiting checking callback_start_ts - elapsed: ".$elapsed);

			if ($elapsed > 30)
			{
				echo "Your transaction has timed out.";
				return;
			}
		}
		else
		{
			$now_ts = time();

			if ($debug_evosnap) error_log("evosnap_waiting setting callback_start_ts: ".$now_ts);

			$session_data['callback_start_ts'] = $now_ts;
		    UpdateSessionData($dataname, $merchant_order_id, $session_data);
		}

		echo "<pre>Waiting for gateway response: ".$elapsed ."</pre>";
		echo "<script>setTimeout(function(){window.location.reload(1);}, 100);</script>";
	}

	function EvoCallback()
	{
		global $debug_evosnap;

		if ($debug_evosnap) error_log(__FILE__." hit EvoCallback with request:" .json_encode($_REQUEST));

		$merchant_order_id = reqvar("merchant_order_id", "");

		$session_data = getLocationProcessingInfo($merchant_order_id);
		if ($session_data === false)
		{
			return;
		}

		$dataname = $session_data['dataname'];
	
		if (reqvar("message_type", "") != "CHECKOUT_COMPLETED")
		{
			return;
		}
	
		$evoResponse = array(
			'objects'			=> $_REQUEST['objects'],
			'txn_id'				=> reqvar("txn_id", ""),
			'merchant_order_id'	=> reqvar("merchant_order_id", "")
		);

		$session_data['EVO_RESPONSE'] = $evoResponse;

		if ($debug_evosnap) error_log("evosnap_waiting EVO_RESPONSE: ".json_encode($session_data));

		UpdateSessionData($dataname, $merchant_order_id, $session_data);

		return;	
	}

	function applyEvoTransaction($dataname, $location_id, $order_id, $session_data)
	{
		$responseJSON = json_decode($session_data['EVO_RESPONSE']['objects'], 1);

		$txn_id = isset($session_data['EVO_RESPONSE']['txn_id'])?$session_data['EVO_RESPONSE']['txn_id']:"";
		$txn_id = empty($txn_id)?"":$txn_id;

		$merchant_order_id = isset($session_data['EVO_RESPONSE']['merchant_order_id'])?$session_data['EVO_RESPONSE']['merchant_order_id']:"";
		$merchant_order_id = empty($merchant_order_id)?"":$merchant_order_id;

		$tx_info = $responseJSON['order']['transactions'][0];

		$exp = "";
		$exp_parts = explode("-", $tx_info['acct_exp']);
		if (count($exp_parts) >= 2)
		{
			$exp = $exp_parts[1]."/".substr($exp_parts[0], -2);
		}

		$total_paid = $session_data['total'];
		
		$transaction = array(
			'action'				=> "Sale",
			'amount'				=> $total_paid,
			'auth_code'			=> $tx_info['approval_code'],
			'card_desc'			=> substr($tx_info['acct_num'], -4),
			'card_type'			=> empty($tx_info['acct_type'])?"":$tx_info['acct_type'],
			'change'				=> "0",
			'check'				=> $session_data['check'],
			'currency_code'		=> $tx_info['currency_code'],
			'device_udid'		=> $session_data['device_udid'],
			'exp'				=> $exp,
			'first_four'			=> substr($tx_info['acct_num'], 0, 4),
			'gateway'			=> "EVOSnap",
			'got_response'		=> "1",
			'info'				=> strtoupper($tx_info['acct_name']),
			'internal_id'		=> shortInternalID($location_id),
			'loc_id'				=> (string)$location_id,
			'pay_type'			=> "Card",
			'pay_type_id'		=> "2",
			'preauth_id'			=> $txn_id,
			'reader'				=> "evosnap_hosted",
			'ref_data'			=> $merchant_order_id,
			'tip_amount'			=> "0",
			'total_collected'	=> $total_paid,
			'transaction_id'	=> $txn_id,
			'transtype'			=> "Sale"
		);

		// use session IsLTG or other means to determine whether we're in LTG or not
		$is_lavu_togo = !empty($session_data['IsLTG']);

		if ($is_lavu_togo)
		{
			$transaction['check']			= "1";
			$transaction['server_name']		= "LavuToGo";
			$transaction['register']			= "LavuToGo";
			$transaction['register_name']	= "LavuToGo";
		}
		else
		{
			$transaction['register']			= $session_data['register'];
			$transaction['register_name']	= $session_data['register_name'];
			$transaction['server_id']		= $session_data['server_id'];
			$transaction['server_name']		= $session_data['server_name'];
		}

		$order_info = GetOrderInfo($dataname, $order_id, $location_id);
		if (empty($order_info))
		{
			error_log("evosnap_waiting ".__FUNCTION__." failed to GetOrderInfo");
		}

		$ioid = $order_info['ioid'];

		$order_info = saveTransaction($transaction, $dataname, $ioid, $location_id, $order_id);

		if ($is_lavu_togo)
		{
			prepareOrderConfirmation($dataname, $order_info);

			UnvoidOrder($dataname, $total_paid, $ioid);

			header("Location: ".$session_data['return_url'], true, 303);
		}
		else
		{
			$transaction['take_tip'] = "0";

			applyTransactionInApp($transaction);
		}
	}
?>