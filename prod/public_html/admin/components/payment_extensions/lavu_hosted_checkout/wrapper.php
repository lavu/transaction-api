<!DOCTYPE html>
<html>
	<head>
		<style type='text/css'>
			body
			{
				font-family:sans-serif;
				background-color: transparent;
				margin: 0 0 0 0;
				padding: 0 0 0 0;
				-webkit-tap-highlight-color:rgba(0, 0, 0, 0);
				-webkit-user-select: none;
			}
			#outerBox
			{
				width:99%;
				height:400px;
				opacity:1;
				border:2px #aecd37 solid;
				border-radius: 15px;
				background-color:#f0f0ee;
			}
			#innerBox
			{
				margin:15px;
				width:auto;
				height:95%;
				background-color:#F7F8F0;
			}
			#closeButton
			{
				margin:2px;
				float:right;
				width:32px;
				height:32px;
				background-image:url("../moneris/images/btn_wow_32x32.png");
			}
			#closeButton img
			{
				width:32px;
				height:32px;
			}
		</style>
		<script language='javascript'>

			function closeIt()
			{
				window.location = "_DO:cmd=close_overlay";
			}

			function pageDidLoad()
			{
				var outer_box = document.getElementById('outerBox');
				if (outer_box)
				{
					outer_box.style.height = (document.body.scrollHeight - 5) + "px";
				}

				var top_divider = document.getElementById('topDivider');
				var inner_box = document.getElementById('innerBox');
				if (top_divider && inner_box)
				{
					top_divider.style.width = (inner_box.offsetWidth - 2) + "px";
				}
			}

		</script>
	</head>
	<body onload='pageDidLoad()'>
		<div id='outerBox'>
			<div id='innerBox'>
				<div id='closeButton' ontouchstart='closeIt();'>
					<img src='../moneris/images/x_icon.png'>
				</div>
			</div>
			<img id='topDivider' style='position:absolute; left:18px; top:54px; width:0px; height:1px;' src='../images/panel_div1.png'>
		</div>
	</body>
</html>