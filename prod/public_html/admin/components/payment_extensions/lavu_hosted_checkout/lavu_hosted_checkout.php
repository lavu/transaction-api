<?php

$debugging = print_r($_REQUEST,1);
session_start();
session_unset();
require_once(dirname(__FILE__)."/../../../lib/info_inc.php");
if(!isset($company_info['id'])){
	error_log(__FILE__." 0|Connection Authentication Error");
	printPage("0|Connection Authentication Error ".date(),'""');
	return;
}
if (lsecurity_id($company_code_key) != $company_info['id']) {
	error_log(__FILE__." 0|Connection Authentication Error");
	printPage("0|Connection Authentication Error ".date(),'""');
	return;
}
$_SESSION['REQUEST'] = $_REQUEST;
$_SESSION['IsLTG'] = false;
$dataname = $_REQUEST['dn'];
$rest_id  = $_REQUEST['company_id'];
$order_id = $_REQUEST['order_id'];
$loc_id   = $_REQUEST['loc_id'];
$ioid     = $_REQUEST['ioid'];
$amount   = $_REQUEST['remaining_amount'];
#Lavu App Info
$lavu_version  = $_REQUEST['version'];
$lavu_register = $_REQUEST['register'];
$lavu_app      = $_REQUEST['app'];
$transtype = (isset($_REQUEST['transtype']))?$_REQUEST['transtype']:"Sale";
//$filler = '<br>';
$buttons = getPaymentTypes();
#$debugging = print_r($_SERVER,1);
//$debugging = print_r($_REQUEST,1);
#$debugging = print_r($_SESSION,1);
//$filler = "<pre>".$debugging."</pre>";
printPage($filler,$buttons);

function getPaymentTypes(){
	$toReturn = array();
	$queryString = 'SELECT `value` FROM `config` WHERE `setting` = "alternate_payment_types" AND location = "'.$_REQUEST['loc_id'].'" ORDER BY `value9`';
	error_log($queryString);
	$result = lavu_query($queryString);
	$paymentTypeNames = array();
	while($temp = mysqli_fetch_assoc($result )){
		$paymentTypeNames[] =  $temp['value'];
	}
	if(empty($paymentTypeNames)){
		error_log(__FILE__." No alternate_payment_types?");
	}
	foreach ($paymentTypeNames as $paymentTypeName) {
		switch ($paymentTypeName) {
			case 'free':
				$pt = array();
				$pt['Name'] = 'Free';
				$pt['URL']  = "_DO:cmd=hideActivityShade_DO:cmd=addTransaction&tx_info=".fakeTransaction();
				$pt['Message'] = '';
				$toReturn[] = $pt;
				break;
			case 'paypal':
				$pt = array();
				$pt['Name'] = 'PayPal';
				$pt['URL']  = "paypal.php?".htmlspecialchars(SID);
				$pt['Image']  = "images/paypal.png";
				$pt['Message'] = 'Redirecting to PayPal';
				$toReturn[] = $pt;
				break;
			case 'gocoin':
				$pt = array();
				$pt['Name'] = 'GoCoin';
				$pt['URL']  = "gocoin.php?".htmlspecialchars(SID);
				$pt['Image']  = "images/gocoin.png";
				$pt['Message'] = 'Redirecting to GoCoin';
				$toReturn[] = $pt;
				break;
			case 'quickpay.dk':
				$pt = array();
				$pt['Name'] = 'QuickpayDK';
				$pt['URL']  = "quickpay.php?".htmlspecialchars(SID);
				$pt['Image']  = "images/quickpay.png";
				$pt['Message'] = 'Redirecting to Quickpay Secure Payment Gateway';
				$toReturn[] = $pt;
				break;
			default:
				# code...
				break;
		}
	}
	return json_encode($toReturn);
}

function printPage($filler,$buttons){
	$checkoutHTML = file_get_contents(__DIR__.'/checkout_template.html');
	$checkoutHTML = str_replace('[{sessionID}]', session_id(), $checkoutHTML);
	$checkoutHTML = str_replace('[{filler}]', $filler, $checkoutHTML);
	$checkoutHTML = str_replace('[{Buttons}]', $buttons, $checkoutHTML);
	echo "$checkoutHTML";
	return;
}

function fakeTransaction(){
	$toReturn = array();
	$toReturn["amount"] = $_REQUEST['amount'];
	$toReturn["tip_amount"] = '0';
	$toReturn["total_collected"] = $_REQUEST['remaining_amount'];
	$toReturn["pay_type"] = 'Free';
	$toReturn["pay_type_id"] = $_REQUEST['pay_type_id'];
	$toReturn["transtype"] = 'Sale';
	$toReturn["action"] = 'Sale';
	$toReturn["change"] = '0';
	$toReturn["internal_id"] = $_REQUEST['ioid'];
	$toReturn["got_response"] = '1';
	return urlencode(json_encode($toReturn)); 
}
