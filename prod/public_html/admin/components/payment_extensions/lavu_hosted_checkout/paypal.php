<?php
session_start();
error_reporting(E_ALL);
if(!isset($_SESSION['REQUEST'])){
	return;
}
require_once(__DIR__.'/../../../lib/gateway_lib/hosted_checkout/paypal_util.php');
$dataname = $_SESSION['REQUEST']['dn'];
$order_id = $_SESSION['REQUEST']['order_id'];
$location_id = $_SESSION['REQUEST']['loc_id'];
ConnectionHub::getConn('rest')->selectDN($dataname);
$order_info = GetOrderInfo($dataname,$order_id, $location_id);
$order_info['total'] = $_SESSION['REQUEST']['remaining_amount'];
$integration_data = getAltIntegrationData('paypal',$location_id);
paypalCreatePayment($dataname,$order_id, $location_id,$order_info,$integration_data);
