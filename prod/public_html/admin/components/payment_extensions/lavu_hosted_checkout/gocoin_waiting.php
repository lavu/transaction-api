<?php
session_start();
error_reporting(E_ALL);
if(!isset($_SESSION['REQUEST'])){
	return;
}
require_once(__DIR__.'/../../../lib/gateway_lib/hosted_checkout/util.php');
require_once(__DIR__.'/../../../lib/gateway_lib/hosted_checkout/gocoin_util.php');

if(!array_key_exists('GC_RESPONSE', $_SESSION) ){
	if(!array_key_exists('WAIT',$_SESSION) 
		){
		$_SESSION['WAIT'] = 0;
	}
	$_SESSION['WAIT']++;
	echo "<pre>Waited:".$_SESSION['WAIT']."</pre>";
	echo "<pre>Session:".session_id()."</pre>";

	if($_SESSION['WAIT'] != 0){
		echo "<script>setTimeout(function(){window.location.reload(1);}, 1000);</script>";
		return;
	}
}

echo "<pre>Waited:".$_SESSION['WAIT']."</pre>";
echo "<pre>Session:".session_id()."</pre>";
echo "<pre>".print_r($_SESSION,1)."</pre>";
gcAppLogPayment($_SESSION['REQUEST']['dn'],$_SESSION['REQUEST']['order_id'],$_SESSION['REQUEST']['loc_id']);

function gcAppLogPayment($dataname,$order_id, $location_id){
	$data = $_SESSION['GC_RESPONSE'];

	$payment = array();
	$payment['internal_id'] = 'GoCoin'.$data['payload']['id'];
	$payment['transaction_id'] = $data['payload']['id'];
	$payment['card_type'] = $data['payload']['price_currency'];
	$payment['first_four'] = '0000';
	$payment['card_desc'] = $data['payload']['price_currency'];
	$payment['gateway'] = 'GoCoin';
	$payment['loc_id'] = $location_id;
	$payment["pay_type_id"] = $_SESSION['REQUEST']['pay_type_id'];
	$payment["transtype"] = 'Sale';
	$payment["action"] = 'Sale';
	$payment['total_collected'] = $_SESSION['REQUEST']['remaining_amount'];
	$payment["amount"] = $payment['total_collected'];
	$payment["tip_amount"] = '0';
	$payment["got_response"] = '1';
	$payment["change"] = '0';
	$payment["pay_type"] = 'GoCoin';
	$payment['mpshc_pid'] = $data['payload']['id'];
	echo "<pre>".print_r($data,1)."</pre>";
	echo "<pre>".print_r($payment,1)."</pre>";
	echo "<script>setTimeout(function(){window.location.reload(1);}, 1000);</script>";
	//return;
	printJavascriptRedirect("_DO:cmd=addTransaction&tx_info=".urlencode(json_encode($payment) ));
	session_destroy();
	return;
}
