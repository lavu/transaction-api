<?php

	if (!isset($company_info))
	{
		require_once(__DIR__."/../../../lib/info_inc.php");
	}

	if (!isset($company_info['id']))
	{
		error_log(__FILE__." 0|Connection Authentication Error");
		printPage("0|Connection Authentication Error ".date(),'""');

		return;
	}

	if (lsecurity_id($company_code_key) != $company_info['id'])
	{
		error_log(__FILE__." 0|Connection Authentication Error");
		printPage("0|Connection Authentication Error ".date(),'""');

		return;
	}

	require_once(__DIR__.'/../../../lib/gateway_lib/hosted_checkout/evosnap_util.php');

	$dataname		= reqvar("dn", "");
	$order_id		= reqvar("order_id", "");
	$location_id	= reqvar("loc_id", "");
	$amount			= reqvar("amount", "");

	$integration_data	= GetGatewayInfo($dataname, $location_id);
	$order_info			= GetOrderInfo($dataname, $order_id, $location_id);
	$order_info['total']	= $amount;

	evoPayment($dataname, $order_id, $location_id, $order_info, $integration_data, $location_info);
?>