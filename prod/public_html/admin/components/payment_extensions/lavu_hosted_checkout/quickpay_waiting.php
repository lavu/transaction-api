<?php
session_start();
error_reporting(E_ALL);
if(!isset($_SESSION['REQUEST'])){
	return;
}
require_once(__DIR__.'/../../../lib/gateway_lib/hosted_checkout/util.php');
require_once(__DIR__.'/../../../lib/gateway_lib/quickpay_dk/quickpay_dk.php');

if(!isset($_SESSION['QP_RESPONSE'])){
	if(!isset($_SESSION['WAIT'])){
		$_SESSION['WAIT'] = 0;
	}
	$_SESSION['WAIT']++;
	echo "<pre>Waited:".$_SESSION['WAIT']."</pre>";
	echo "<pre>Session:".session_id()."</pre>";

	if($_SESSION['WAIT'] != 0){
		echo "<script>setTimeout(function(){window.location.reload(1);}, 1000);</script>";
		return;
	}
}

echo "<pre>Waited:".$_SESSION['WAIT']."</pre>";
echo "<pre>Session:".session_id()."</pre>";
echo "<pre>".print_r($_SESSION,1)."</pre>";
qpAppLogPayment($_SESSION['REQUEST']['dn'],$_SESSION['REQUEST']['order_id'],$_SESSION['REQUEST']['loc_id']);

function qpAppLogPayment($dataname,$order_id, $location_id){
	$payment = array();
	$payment['internal_id'] = 'quickpay'.$_SESSION['QP_RESPONSE']['transaction'];
	$payment['transaction_id'] = $_SESSION['QP_RESPONSE']['transaction'];
	$payment['auth_code'] = $_SESSION['QP_RESPONSE']['qpstat'];
	$payment['card_type'] = $_SESSION['QP_RESPONSE']['cardtype']; //Optional-preferred
	$payment['first_four'] = substr($_SESSION['QP_RESPONSE']['cardnumber'],4); //Optional-preferred
	$payment['card_desc'] = substr($_SESSION['QP_RESPONSE']['cardnumber'],-4); //Optional-preferred
	$payment['gateway'] = 'Quickpay';
	$payment['loc_id'] = $location_id;
	$payment["pay_type_id"] = $_SESSION['REQUEST']['pay_type_id'];
	$payment["transtype"] = 'Sale';
	$payment["action"] = 'Sale';
	$payment['total_collected'] = $_SESSION['REQUEST']['remaining_amount'];
	$payment["amount"] = $payment['total_collected'];
	$payment["tip_amount"] = '0';
	$payment["got_response"] = '1';
	$payment["change"] = '0';
	$payment["pay_type"] = $payment['card_type'];
	printJavascriptRedirect("_DO:cmd=addTransaction&tx_info=".urlencode(json_encode($payment) ));
	session_destroy();
	return;
}
