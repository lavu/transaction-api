<?php
	function ext_display_money($amt) {
		global $location_info;
		
		$decimal_places = $location_info['disable_decimal'];
		if(!is_numeric($decimal_places)) $decimal_places = 2;
		
		return number_format($amt,$decimal_places);
	}

	function ext_display_money_with_symbol($amt) {
		global $location_info;
		$amt = str_replace("$","",str_replace(",","",$amt));
		
		$monetary_symbol = $location_info['monitary_symbol'];
		if ($monetary_symbol == "") $monetary_symbol = "$";
		$left_or_right = $location_info['left_or_right'];
		if ($left_or_right == "") $left_or_right = "left";
		
		$rtn_str = "";
		if ($left_or_right == "left") $rtn_str .= $monetary_symbol;
		$str_number = ext_display_money($amt);
		if(isset($location_info['decimal_char']) && $location_info['decimal_char']!="." && $location_info['decimal_char']!="")
		{
			$str_number = str_replace(".",$location_info['decimal_char'],str_replace(",","",$str_number));
		}
		$rtn_str .= $str_number . "";
		if ($left_or_right == "right") $rtn_str .= $monetary_symbol;
		
		return $rtn_str;
	}
	
	function save_reqvar($key) {
		$set_reqvar = reqvar($key);
		$_SESSION['remember_reqvar_' . $key] = $set_reqvar;
		return $set_reqvar;
	}
	
	function remember_reqvar($key) {
		if(isset($_POST[$key])) return $_POST[$key];
		else if(isset($_GET[$key])) return $_GET[$key];
		else if(isset($_SESSION['remember_reqvar_' . $key])) return $_SESSION['remember_reqvar_' . $key];
		else return reqvar($key);
	}
?>
