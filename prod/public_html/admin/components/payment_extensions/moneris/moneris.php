<?php

	session_start();
	require_once(dirname(__FILE__)."/../../../lib/info_inc.php");

	$db_name = "poslavu_".$data_name."_db";
	$integration_info = get_integration_from_location($location_info['id'], $db_name);
	
	/*$server_list = array();
	if (reqvar("action") == "Admin") {
		$get_server_list = lavu_query("SELECT `id`, `f_name`, `l_name` FROM `users` WHERE `active` = '1' AND `_deleted` = '0'");
		if (mysqli_num_rows($get_server_list) > 0) {
			while ($s_info = mysqli_fetch_assoc($get_server_list)) {
				$server_list[str_pad($s_info['id'], 6, "0", STR_PAD_LEFT)] = $s_info['f_name']." ".$s_info['l_name'];	
			}
		}
	}*/

?><!DOCTYPE html>
<html>

	<head>	
		<style type='text/css'>

			body {
				font-family:sans-serif;
				background-color: transparent;
				margin: 0 0 0 0;
				padding: 0 0 0 0;
				-webkit-tap-highlight-color:rgba(0, 0, 0, 0);
				-webkit-user-select: none;
			}

			body:after {
				content:
					url("images/popup_upper_left.png")
					url("images/popup_upper.png")
					url("images/popup_upper_right.png")
					url("images/popup_left.png")
					url("images/popup_right.png")
					url("images/popup_lower_left.png")
					url("images/popup_lower.png")
					url("images/popup_lower_right.png")
					url("images/cn_input_field_bg.png")
					url("images/btn_wow_32x32.png")
					url("images/btn_wow_87x52.png")
					url("images/btn_wow_132x52.png")
					url("images/btn_wow_113x37.png")
					url("images/btn_cc_back.png")
					url("images/x_icon.png")
					url("images/moneris_activity.gif");
				display: none;
			}
			
			.inner_div1 {
				position: absolute;
				left: 0px;
				top: 0px;
				width: 280px;
				height:412px;
				display: none;
			}

			.inner_div2 {
				position: absolute;
				left: 0px;
				top: 45px;
				width: 280px;
				height: 367px;
				display: none;
			}
			
			.msg_txt1 {
				color:#888888;
				font-family:Arial, Helvetica, sans-serif;
				font-size:17px;
			}

			.msg_txt2 {
				color:#666666;
				font-family:Arial, Helvetica, sans-serif;
				font-size:20px;
			}

			.msg_txt3 {
				color:#666666;
				font-family:Arial, Helvetica, sans-serif;
				font-size:22px;
			}

			.msg_txt4 {
				color:#999999;
				font-family:Arial, Helvetica, sans-serif;
				font-size:15px;
			}

			.input_long {
				width:260px;
				height:40px;
				background:url("images/cn_input_field_bg.png");
				background-repeat:no-repeat;
				background-position:center;
				border:hidden;
				text-align:center;
				font-family:Arial, Helvetica, sans-serif;
				font-size:18px;
				padding-top:7px;
			}
			
			.btn_number_pad {
				color:#7D7D7D;
				font-family:Arial, Helvetica, sans-serif;
				font-size:26px;
				font-weight:bold;
				text-align:center;
				width:87px;
				height:52px;
				background-image:url("images/btn_wow_87x52.png");
				background-repeat:no-repeat;
				background-position:center;
				border:hidden;
			}

			.btn_number_pad_wide {
				color:#7D7D7D;
				font-family:Arial, Helvetica, sans-serif;
				font-size:26px;
				font-weight:bold;
				text-align:center;
				width:132px;
				height:52px;
				background-image:url("images/btn_wow_132x52.png");
				background-repeat:no-repeat;
				background-position:center;
				border:hidden;
			}
			
			.btn_light {
				color:#7D7D7D;
				font-family:Arial, Helvetica, sans-serif;
				font-size:13px;
				width:113px;
				height:37px;
				background:url("images/btn_wow_113x37.png");
				border:hidden;
				padding-top:2px;
			}
			
			.btn_light_long {
				color:#7D7D7D;
				font-family:Arial, Helvetica, sans-serif;
				font-size:13px;
				width:167px;
				height:37px;
				background:url("images/btn_wow_167x37.png");
				border:hidden;
				padding-top:2px;
			}
			
			.report_title {
				color:#333333;
				font-family:Arial, Helvetica, sans-serif;
				font-size:22px;
			}
			
			.report_field {
				color:#222222;
				font-family:Arial, Helvetica, sans-serif;
				font-size:14px;			
			}
			
			.report_value {
				color:#2C2C2C;
				font-family:Arial, Helvetica, sans-serif;
				font-size:15px;
			}

		</style>

		<script src="../webextjs.js"></script>
		<script language='javascript'>

			//var useTestDate = "";
			
			var company_id = "<?php echo reqvar("company_id"); ?>";
			var loc_id = "<?php echo reqvar("loc_id"); ?>";
			var device_prefix = "<?php echo reqvar("device_prefix"); ?>";
			
			var location_name = "<?php echo strtoupper($location_info['title']); ?>";
			var location_address = "<?php echo strtoupper($location_info['address']); ?>";
			var location_city = "<?php echo strtoupper($location_info['city']); ?>";
			var location_state = "<?php echo strtoupper($location_info['state']); ?>";
			var location_zip = "<?php echo strtoupper($location_info['zip']); ?>";
						
			var device_failed = 1;
			var comm_mode = "";
			var hold_comm_mode = "";
			var cFS = String.fromCharCode(28);
			var cSTX = String.fromCharCode(02);
			var cEOT = String.fromCharCode(04);
			var cACK = String.fromCharCode(06);
			
			var confirm_mode = "";

			var host_instance_id = "";
			var integration_id = "<?php echo $integration_info['integration2']; ?>";		
			var secure_pass_thru = (integration_id == "USA001");
			var use_socket_port = secure_pass_thru?9599:1; // 1 for CANADA, 9599 for US
			var ACK_received = false;
			
			var merchant_id = "<?php echo $integration_info['integration1']; ?>";
			var terminal_ip = "";
			var terminal_id = "";
			var terminal_initialized = "0";
			
			var decimal_places = "<?php echo $location_info['disable_decimal']; ?>";
		
			var action = "<?php echo reqvar("action"); ?>";
			var admin_type = "<?php echo reqvar("admin_type"); ?>";
			var amount = "<?php echo reqvar("amount"); ?>";
			var order_id = "<?php echo reqvar("order_id"); ?>";
			
			var short_order_id = order_id.replace("-", "");
			var soi_len = short_order_id.length;
			if (soi_len > 7) short_order_id = short_order_id.substring((soi_len - 7), soi_len);
			
			var internal_id = "";
			var for_deposit = "<?php echo reqvar("for_deposit"); ?>";
			var	is_deposit = "<?php echo reqvar("is_deposit"); ?>";
			var tx_info = {};

			var server_id = "<?php echo reqvar("server_id"); ?>";
			var server_name = "<?php echo reqvar("server_name"); ?>";
			//var server_list = tryParseJSON('<...?...php echo lavu_json_encode($server_list); ...?...>');
			var register = "<?php echo reqvar("register"); ?>";
			 
			var orig_action = "<?php echo reqvar("orig_action"); ?>";
			var orig_internal_id = "<?php echo reqvar("orig_internal_id"); ?>";
			var orig_authcode = "<?php echo reqvar("orig_authcode"); ?>";
			var void_notes = "<?php echo reqvar("void_notes"); ?>";
			var void_tip = "<?php echo reqvar("void_tip"); ?>";
			var refund_notes = "<?php echo reqvar("refund_notes"); ?>";
			var refund_tip = "<?php echo reqvar("refund_tip"); ?>";
			
			var report_info = {};
			var report_clerk_id = "";
			
			var tip_entry_mode = "";
			var tip_entry_mode_setto = "";
			
			var month_num_to_name = [];
			month_num_to_name[0] = "";
			month_num_to_name[1] = "JAN";
			month_num_to_name[2] = "FEB";
			month_num_to_name[3] = "MAR";
			month_num_to_name[4] = "APR";
			month_num_to_name[5] = "MAY";
			month_num_to_name[6] = "JUN";
			month_num_to_name[7] = "JUL";
			month_num_to_name[8] = "AUG";
			month_num_to_name[9] = "SEP";
			month_num_to_name[10] = "OCT";
			month_num_to_name[11] = "NOV";
			month_num_to_name[12] = "DEC";
			
			var awaiting_socket_response = false;
			var waiting_for_echo_data = "";
			
			function refreshHostInstanceID(action) {
				
				var uuid = "<?php echo reqvar("UUID", ""); ?>";
				var uuid_parts = uuid.split("-");
				
				host_instance_id = uuid_parts[0] + "_" + action + "_" + getCurrentDateTime(true);
			}

			function TCPSocketResponse(data, status) {
				
				if (awaiting_socket_response) {
					
					if (status == 1) {
						
						device_failed = 0;
						
						if (secure_pass_thru) processSecurePassThru(data, status);
						else processECRresponse(data, status);
						
					} else {
					
						awaiting_socket_response = false;
						device_failed = 1;
						
						switch (comm_mode) {
						
							case "purchase":
							case "void_purchase":
							case "refund":
							case "void_refund":
							case "last_ped_response":
		
								device_failed = (status==2 || status==4)?2:1;
								break;
								
							case "close":
							case "initialize":
							case "add_clerk":
							case "logon":
							case "logoff":				
							case "clerk_subtotals": 
							case "deposit_totals": 
							case "close_batch":
							case "tip_entry_mode":
							default:
							
								device_failed = 1;
								break;
						}
						
						var error_message = fromHex(data);
						if (error_message == "Disconnected") error_message += "<br><br>Please check connection<br>and try again.";
						error_message += "<br><br>IP " + terminal_ip + "<br>Terminal ID " + terminal_id;
		
						transactionFailed("Device Error", error_message);								
					}
				}
				
				return 1;
			}				
			
			function processECRresponse(data, status) {

				awaiting_socket_response = false;
					
				var str = fromHex(data).replace(cEOT, "");
				var response = str.split(cFS);
				var co_co = response[0];
				var b24_co = "";

				switch (comm_mode) {
					
					case "close":
					
						handleCloseResponse(co_co);
						break;
						
					case "initialize":
					
						if (response.length > 5) b24_co = response[5];

						if (co_co == "099") {
							//componentAlert("Initialization Successful!", "");
							window.setTimeout(function(){setInitializedFlag("1")}, 250);
							window.setTimeout(function(){checkTerminalClerkID()}, 500);
						} else {
							terminal_id = "";
							enterIP();
							var error_message = (status == 1)?errorMessageForCodes(co_co, b24_co):str;
							componentAlert("Initialization Failed", error_message);								
						}
					
						break;
						
					case "add_clerk":
					
						if (response.length > 3) b24_co = response[3];

						if (co_co == "099") window.setTimeout(function(){POSPADlogon(server_id)}, 250);
						else {
							var error_message = (status == 1)?errorMessageForCodes(co_co, b24_co):str;
							transactionFailed("Clerk Logon Failed", error_message);
						}
				
						break;
						
					case "logon":
					
						if (response.length > 4) b24_co = response[4];
					
						if (co_co == "099") {
							window.setTimeout(function(){setTerminalClerkID(server_id)}, 250);
							window.setTimeout(function(){initiateAction()}, 500);
						} else {
							var error_message = (status == 1)?errorMessageForCodes(co_co, b24_co):str;
							transactionFailed("Clerk Logon Failed", error_message);
						}
						
						break;
						
					case "logoff":

						if (co_co == "099") {
							window.setTimeout(function(){setTerminalClerkID("")}, 250);
							window.setTimeout(function(){POSPADlogon(server_id)}, 500);
						} else {
							var error_message = (status == 1)?errorMessageForCodes(co_co, b24_co):str;
							transactionFailed("Clerk Logoff Failed", error_message);
						}
					
						break;
						
					case "purchase":
					case "void_purchase":
					case "refund":
					case "void_refund":

						//alert("response: " + JSON.stringify(response));
						
						if (co_co=="099" || co_co=="051" || co_co=="052" || co_co=="304"|| co_co=="307" || co_co=="607" || co_co=="608" || co_co=="609" || co_co=="616" || co_co=="640") {

							var approval_number = respEl(response, 8);
							var approved = (approval_number.length > 0);

							var message = "Approved!";
							var action = "Sale";
							var transtype = "Sale";
							if (comm_mode=="void_purchase" || comm_mode=="void_refund") {
								message = "Transaction Voided!";
								action = "Void";
								transtype = "Void";	
							} else if (comm_mode == "refund") {
								action = "Refund";
								transtype = "Return";	
							}

							var acct = respEl(response, 4);
							var exp_date = respEl(response, 5);
							
							var amountF = parseFloat(amount);
							var rtn_amt = parseFloat(convertAmount(respEl(response, 3)));
							var approved_amount = isNaN(rtn_amt)?amountF:rtn_amt;
							var tip_amount = 0;
							if (approved_amount > amountF) {
								tip_amount = approved_amount - amountF;
								approved_amount = amountF;	
							}

							var date = new Date();
							var year = date.getFullYear();
							var month = (date.getMonth() + 1) + "";
							var day = date.getDate() + "";
							var hour = date.getHours() + "";
							var minute = date.getMinutes() + "";
							var second = date.getSeconds() + "";
							var datetime1 = year + "-" + month.paddingLeft("00") + "-" + day.paddingLeft("00") + " " + hour.paddingLeft("00") + ":" + minute.paddingLeft("00") + ":" + second.paddingLeft("00");
							//var datetime2 = day.paddingLeft("00") + " " + month_num_to_name[parseInt(month)] + " " + year + " " + hour.paddingLeft("00") + ":" + minute.paddingLeft("00") + ":" + second.paddingLeft("00");
							
							var host_datetime = respEl(response, 2);
							if (host_datetime.length >= 12) {
								year = host_datetime.substring(0, 2);
								month = host_datetime.substring(2, 4);
								day = host_datetime.substring(4, 6);
								hour = host_datetime.substring(6, 8);
								minute = host_datetime.substring(8, 10);
								second = host_datetime.substring(10, 12);
								host_datetime = day + " " + month_num_to_name[parseInt(month)] + " 20" + year + " " + hour + ":" + minute + ":" + second;
							}								
							
							tx_info = {};
							tx_info.amount = approved_amount + "";
							tx_info.total_collected = tx_info.amount;
							tx_info.tip_amount = tip_amount + "";
							tx_info.change = "0";
							tx_info.datetime = datetime1;
							tx_info.first_four = acct.substring(0, 4);
							tx_info.card_desc = acct.substring((acct.length - 4), acct.length);
							tx_info.exp = exp_date.substring(2, 4) + "/" + exp_date.substring(0, 2);
							tx_info.auth_code = respEl(response, 8);
							tx_info.transaction_id = respEl(response, 9);
							tx_info.preauth_id = tx_info.transaction_id;
							tx_info.record_no = respEl(response, 11);
							tx_info.swipe_grade = respEl(response, 12); // card entry method
							tx_info.card_type = respEl(response, secure_pass_thru?17:14);
							tx_info.pay_type = "Card";
							tx_info.pay_type_id = "2";
							tx_info.action = action;
							tx_info.transtype = transtype;
							tx_info.got_response = "1";
							tx_info.reader = "ipp320";
							tx_info.gateway = "Moneris";
							tx_info.for_deposit = for_deposit;
							tx_info.is_deposit = is_deposit;
							tx_info.internal_id = newInternalID();
							tx_info.process_data = "payment_extension:49";
							tx_info.take_signature = "0";
							tx_info.take_tip = "0";
							if (comm_mode=="void_purchase" || comm_mode=="void_refund") {
								tx_info.orig_action = orig_action;
								tx_info.orig_internal_id = orig_internal_id;
								tx_info.server_id = server_id;
								tx_info.server_name = server_name;
								tx_info.void_notes = void_notes;
							} else if (comm_mode == "refund") {
								tx_info.refund_notes = refund_notes;
							}
							
							var masked_acct = "";
							for (var m = 0; m < (acct.length - 4); m++) {
								masked_acct += "*";	
							}
							masked_acct += tx_info.card_desc;								
						
							var ref_data = {};
							ref_data.t_type = comm_mode;
							ref_data.co_co = co_co;
							ref_data.a_type = respEl(response, 13);
							ref_data.cn = masked_acct;
							ref_data.datetime = host_datetime;
							if (response.length >= 18) {
								var app_l = respEl(response, secure_pass_thru?15:18);
								if (app_l.substring(0, 4) != "Lavu") {
									ref_data.app_l = app_l;
									ref_data.app_n = respEl(response, secure_pass_thru?16:19);
									ref_data.aid = respEl(response, secure_pass_thru?14:17);
									ref_data.tvr = respEl(response, secure_pass_thru?21:23);
									if (ref_data.tvr.length <= 0) ref_data.tvr = respEl(response, secure_pass_thru?19:21);
									ref_data.tsi = respEl(response, secure_pass_thru?23:24);
								}
							}
							ref_data.iso = respEl(response, 6);
							ref_data.b24 = respEl(response, 7);
							ref_data.cvm = respEl(response, secure_pass_thru?22:15);
							ref_data.lang = respEl(response, 10);
	
							//alert(JSON.stringify(ref_data));
							tx_info.ref_data = JSON.stringify(ref_data);
							
							printReceipt(tx_info, ref_data, approved);
							
							var trnslt = (ref_data.lang=="4" || ref_data.lang=="6");
							if (approved) {
								showApproved(translate(message, trnslt));
								if (co_co == "052") window.setTimeout(function(){setInitializedFlag("0")}, 250);
								window.setTimeout(function(){addTransaction(tx_info)}, 1000);
							} else if (co_co=="051" || co_co=="307" || co_co=="607" || co_co=="608" || co_co=="609" || co_co=="616" || co_co=="640") {
								var useMessage = verbalizeCode("condition", co_co);
								if (co_co=="051" && ref_data.iso=="65" && tx_info.card_type=="MASTERCARD") useMessage = "Please retry with a different entry method, such as swipe or insert.";
								transactionFailed("Declined", useMessage);
							} else {
								transactionFailed("Declined", verbalizeCode("base24", ref_data.b24));
							}
														
						} else if (co_co == "401") {
							
							// print cancellation receipt for debit refund?
							
							transactionFailed("Transaction Cancelled", "");
								
						} else transactionFailed("Error", verbalizeCode("condition", co_co));
				
						break;
						
					case "last_ped_response":
					
						alert("response: " + JSON.stringify(response));
					
						break;
						
					case "clerk_subtotals": 
					case "deposit_totals": 
					
						//alert("response: " + JSON.stringify(response));
					
						if (response.length > 3) b24_co = response[3];

						if (co_co == "099") {
							
							var date = new Date();
							var year = date.getFullYear();
							var month = (date.getMonth() + 1) + "";
							var day = date.getDate() + "";
							var hour = date.getHours() + "";
							var minute = date.getMinutes() + "";
							var second = date.getSeconds() + "";
							var datetime = year + "-" + month.paddingLeft("00") + "-" + day.paddingLeft("00") + " " + hour.paddingLeft("00") + ":" + minute.paddingLeft("00") + ":" + second.paddingLeft("00");
							
							var report_title = (comm_mode == "clerk_subtotals")?"Clerk Subtotals":"Deposit Totals";
							
							report_info = [];
							report_info.push({"title": report_title});
							report_info.push({"value": ""});
							if (comm_mode == "clerk_subtotals") {
								report_info.push({"field1": "Clerk", "value": server_name});
								report_info.push({"field1": "Clerk ID", "value": report_clerk_id});
							}
							report_info.push({"field1": "Date/Time", "value": datetime});
							report_info.push({"new_table": "", "border": true});
							report_info.push({"field2": "Purchase Count", "value": parseInt(response[6])});
							report_info.push({"field2": "Purchase Amount", "value": displayMoneyWithSymbol(convertAmount(response[7])), "border": true});
							report_info.push({"field2": "Refund Count", "value": parseInt(response[8])});
							report_info.push({"field2": "Refund Amount", "value": displayMoneyWithSymbol(convertAmount(response[9])), "border": true});
							report_info.push({"field2": "Void Count", "value": parseInt(response[10])});
							report_info.push({"field2": "Void Amount", "value": displayMoneyWithSymbol(convertAmount(response[11])), "border": true});
							report_info.push({"field2": "Payment Count", "value": parseInt(response[12])});
							report_info.push({"field2": "Payment Amount", "value": displayMoneyWithSymbol(convertAmount(response[13])), "border": true});
							report_info.push({"field2": "Payment Void Count", "value": parseInt(response[14])});
							report_info.push({"field2": "Payment Void Amount", "value": displayMoneyWithSymbol(convertAmount(response[15])), "border": true});
							report_info.push({"field2": "Credit Application Count", "value": parseInt(response[16])});
							
							//alert("report: " + JSON.stringify(report_info));
							
							displayReport();
						
						} else {
						
							var error_message = (status == 1)?errorMessageForCodes(co_co, b24_co):str;
							transactionFailed("Inquiry Failed", error_message);
						}
						
						break;

					case "close_batch":
					
						//alert("response: " + JSON.stringify(response));
					
						if (response.length > 3) b24_co = response[3];

						if (co_co=="006" || co_co=="007") {
								
							var date = new Date();
							var year = date.getFullYear();
							var month = (date.getMonth() + 1) + "";
							var day = date.getDate() + "";
							var hour = date.getHours() + "";
							var minute = date.getMinutes() + "";
							var second = date.getSeconds() + "";
							var datetime = year + "-" + month.paddingLeft("00") + "-" + day.paddingLeft("00") + " " + hour.paddingLeft("00") + ":" + minute.paddingLeft("00") + ":" + second.paddingLeft("00");
							
							var report_title = (comm_mode == "clerk_subtotals")?"Clerk Subtotals":"Deposit Totals";
							
							report_info = [];
							report_info.push({"title": "Close Batch"});
							report_info.push({"value": ""});
							//report_info.push({"field1": "Clerk", "value": server_name});
							//report_info.push({"field1": "Clerk ID", "value": report_clerk_id});
							report_info.push({"field1": "Date/Time", "value": datetime});
							report_info.push({"field2": "Batch Number", "value": parseInt(response[6]), "border": true});
							report_info.push({"new_table": "", "border": true});
							report_info.push({"field2": "Purchase Count", "value": parseInt(response[7])});							
							report_info.push({"field2": "Purchase Amount", "value": displayMoneyWithSymbol(convertAmount(response[8])), "border": true});
							report_info.push({"field2": "Refund Count", "value": parseInt(response[9])});
							report_info.push({"field2": "Refund Amount", "value": displayMoneyWithSymbol(convertAmount(response[10])), "border": true});
							report_info.push({"field2": "Void Count", "value": parseInt(response[11])});
							report_info.push({"field2": "Void Amount", "value": displayMoneyWithSymbol(convertAmount(response[12])), "border": true});
							report_info.push({"field2": "Payment Count", "value": parseInt(response[13])});
							report_info.push({"field2": "Payment Amount", "value": displayMoneyWithSymbol(convertAmount(response[14])), "border": true});
							report_info.push({"field2": "Payment Void Count", "value": parseInt(response[15])});
							report_info.push({"field2": "Payment Void Amount", "value": displayMoneyWithSymbol(convertAmount(response[16])), "border": true});
							report_info.push({"field2": "Credit Application Count", "value": parseInt(response[17])});
							
							//alert("report: " + JSON.stringify(report_info));
							
							displayReport();
						
						} else {
						
							var error_message = (status == 1)?errorMessageForCodes(co_co, b24_co):str;
							transactionFailed("Inquiry Failed", error_message);
						}
					
						break;
						
					case "tip_entry_mode":
					
						if (co_co == "099") {

							tip_entry_mode = tip_entry_mode_setto;
							tipEntrySettings();
							window.location = "_DO:cmd=set_termset&keys=tip_entry_mode&vals=" + toHex(tip_entry_mode) + "&callback=didSetTipEntryMode";

						} else {
						
							var error_message = (status == 1)?errorMessageForCodes(co_co, ""):str;
							transactionFailed("Configuration Failed", error_message);
						}
						
						break;
						
					default:
						break;	
				}
			}
			
			var ignore_phoenix_response = false;
			
			function processSecurePassThru(data, status) {
				
				var str = fromHex(data).replace(cEOT, "");
								
				if (ACK_received) {
				
					POSPADack();
				
					if (str == cACK) alert("Unexpected ACK");
					else {
				
						var response = str.split(cFS);
						var co_co = response[0];

						switch (co_co) {
	
							case "001": 
							
								ACK_received = false;						
								window.setTimeout(function(){socketWrite("99" + cEOT, terminal_ip, use_socket_port)}, 250);
								break;
							
							case "098":
							
								handleCloseResponse("098");
								break;
								
							case "006":
							case "007":
							case "051":
							case "052":
							case "099":
							case "304":
							case "307":
							case "607":
							case "608":
							case "609":
							case "616":
							case "640":
							
								window.setTimeout(function(){processECRresponse(data, status)}, 250);
								break;
								
							case "401":
							
								transactionFailed("Transaction Cancelled", "");
								break;
								
							case "100":
							case "103":
							case "250":
							case "301":
							case "303":
							case "400":
							case "784":
							
								transactionFailed("POSPAD Error", verbalizeCode("condition", co_co));
								break;
								
							default:
						
								if (co_co.substring(0, 3) == "310") ignore_phoenix_response = true;
						
								new PostToPhoenix({ "data": data, "dn": "<?php echo $data_name; ?>", "UUID": "<?php echo reqvar("UUID", ""); ?>", "iid": host_instance_id, "debug": "<?php echo $location_info['gateway_debug']; ?>" });
								break;
						}
					}	
		
				} else ACK_received = (str == cACK);	
			}
			
			function handleCloseResponse(co_co) {
			
				if (co_co == "098") close2();
				else {
					comm_mode = hold_comm_mode;
					alert("Close error: " + verbalizeCode("condition", co_co));
				}
			}
			
			// US urls
			// test - https://ipgtus.moneris.com (69.46.113.17)
			// production - https://ipgus.moneris.com (69.46.113.18)

			(function(){
				function PostToPhoenix( params ){
					AJAXRequest.call( this, 'phoenix_forward.php', 'POST', params, true, "" );
				}
	
				function success( response ){
					handlePhoenixResponse(response);
				}
				
				function failure( status, response ){
					handlePhoenixResponse("");
				}
	
				window.PostToPhoenix = PostToPhoenix;
				PostToPhoenix.prototype.constructor = PostToPhoenix;
				PostToPhoenix.prototype = Object.create(AJAXRequest.prototype);
				PostToPhoenix.prototype.success = success;
				PostToPhoenix.prototype.failure = failure;
			})();
			
			function handlePhoenixResponse(response) {
				
				if (ignore_phoenix_response) ignore_phoenix_response = false;
				else {
									
					var status = 0;
					var title = "Host Error";
					var message = "Missing or empty response.";
					
					var respJSON = tryParseJSON(response);
					if (respJSON) {	
						if (typeof respJSON.status != 'undefined') {
							status = parseInt(respJSON.status);
							
							if (status == 1) {
								
								if (typeof respJSON.data != 'undefined') {
		
									var str = fromHex(respJSON.data);
									if (str.length > 0) {
										
										var lrc = (typeof respJSON.lrc != 'undefined')?parseInt(respJSON.lrc):0;
								
										ACK_received = false;
										socketWrite(cSTX + str + String.fromCharCode(lrc) + cEOT, terminal_ip, use_socket_port);
										
									} else status = 0;							
									
								} else status = 0;
								
							} else if (typeof respJSON.message != 'undefined') message = respJSON.message
						}
					}
					
					if (status == 0) transactionFailed(title, message);
				}
			}
						
			function respEl(resp, i) {
				
				var el = "";
			
				if (typeof resp != 'undefined') {
					if (resp instanceof Array) {
						if (resp.length > i) el = resp[i];	
					}
				}
				
				return el;
			}
			
			function convertAmount(amt) {
				
				var AMT = parseInt(amt);
				var dp = parseInt(decimal_places);
				var cAmt = (dp > 0)?(AMT / Math.pow(10, dp)):AMT;
				
				return parseFloat(cAmt).toFixed(dp);
			}
			
			function displayMoney(amt) {
			
				return parseFloat(amt).toFixed(decimal_places);
			}

			function displayMoneyWithSymbol(amt) {
			
				var monetary_symbol = "<?php echo $location_info['monitary_symbol']; ?>";
				if (monetary_symbol == "") monetary_symbol = "$";
				var left_or_right = "<?php echo $location_info['left_or_right']; ?>";
				if (left_or_right == "") left_or_right = "left";
				
				var rtn_str = "";
				if (left_or_right == "left") rtn_str += monetary_symbol + " ";
				rtn_str += displayMoney(amt) + "";
				if (left_or_right == "right") rtn_str += " " + monetary_symbol;
				
				return rtn_str;
			}

// * * * * * * * * * * POSPAD Transactions * * * * * * * * * * //

			function POSPADack() {
				
				socketWrite(cACK, terminal_ip, use_socket_port);
			}

			function POSPADinitialize() {
			
				refreshHostInstanceID("INIT");
			
				comm_mode = "initialize";
				awaiting_socket_response = true;
			
				var fields = [];
				fields.push("90");			// Transaction Code
				fields.push(terminal_id);	// ECR ID
				fields.push(merchant_id);	// Merchant ID
				fields.push("");				// Initialization Phone Number
				fields.push("");				// Card Acceptor Terminal ID
				
				var str = fields.join(cFS) + cEOT;
				
				ACK_received = false;
				socketWrite(str, terminal_ip, use_socket_port);
			}
			
			function POSPADaddClerk(clerk_id) {
			
				refreshHostInstanceID("ADD_CLERK");
			
				comm_mode = "add_clerk";
				awaiting_socket_response = true;
					
				var fields = [];
				fields.push("80");									// Transaction code
				fields.push("");										// Card Acceptor Terminal ID
				fields.push("+" + clerk_id.paddingLeft("000000"));	// Clerk ID

				var str = fields.join(cFS) + cEOT;
				
				ACK_received = false;
				socketWrite(str, terminal_ip, use_socket_port);
			}

			function POSPADlogon(clerk_id) {
			
				refreshHostInstanceID("LOGON");
			
				comm_mode = "logon";
				awaiting_socket_response = true;
					
				var fields = [];
				fields.push("50");							// Transaction Code
				fields.push(clerk_id.paddingLeft("000000"));	// Clerk ID
				fields.push("");								// Card Acceptor Terminal ID

				var str = fields.join(cFS) + cEOT;
				
				ACK_received = false;
				socketWrite(str, terminal_ip, use_socket_port);
			}
			
			function POSPADlogoff() {
			
				refreshHostInstanceID("LOGOFF");
			
				comm_mode = "logoff";
				awaiting_socket_response = true;
					
				var fields = [];
				fields.push("51");	// Transaction Code
				fields.push("");		// Card Acceptor Terminal ID

				var str = fields.join(cFS) + cEOT;
				
				ACK_received = false;
				socketWrite(str, terminal_ip, use_socket_port);
			}
			
			function POSPADpurchase() {
				
				refreshHostInstanceID("PURCHASE");
				
				comm_mode = "purchase";
				awaiting_socket_response = true;
				waiting_for_echo_data = "Lavu" + getCurrentDateTime(true).substring(2, 14);

				var amt = amount.replace(".", "");
				amt = amt.replace(",", "");
			
				var fields = [];
				fields.push("00");					// Transaction Code
				fields.push(amt);					// Amount
				fields.push("A");					// Customer Track 2 (Tender Type for SPT)
				fields.push("");						// Original Approval Number
				fields.push("");						// Card Acceptor Terminal ID
				fields.push("");						// Invoice Number
				fields.push("");						// Promo Code
				fields.push("");						// Cash Back Allowed Indicator
				if (secure_pass_thru) {
					fields.push("");					// Surcharge Amount - Debit
					fields.push("");					// Surcharge Amount - Credit
				}
				fields.push(waiting_for_echo_data);	// Echo Data
				fields.push("");						// MOTO E-Commerce Flag
				
				var str = fields.join(cFS) + cEOT;
				
				ACK_received = false;
				socketWrite(str, terminal_ip, use_socket_port);
			}

			function POSPADvoidPurchase() {
				
				refreshHostInstanceID("VOID_PURCHASE");
				
				comm_mode = "void_purchase";
				awaiting_socket_response = true;
				waiting_for_echo_data = "Lavu" + getCurrentDateTime(true).substring(2, 14);
				
				var amt = amount.replace(".", "");
				amt = amt.replace(",", "");
			
				var fields = [];
				fields.push("11");						// Transaction Code
				fields.push(amt);						// Amount
				fields.push(secure_pass_thru?"":"A");	// Customer Track 2
				fields.push(orig_authcode);				// Original Approval Number
				fields.push("");							// Card Acceptor Terminal ID
				fields.push("");							// Invoice Number
				fields.push("");							// Promo Code
				fields.push(waiting_for_echo_data);		// Echo Data
				fields.push("");							// MOTO E-Commerce Flag
				
				var str = fields.join(cFS) + cEOT;
				
				ACK_received = false;
				socketWrite(str, terminal_ip, use_socket_port);
			}

			function POSPADrefund() {
				
				refreshHostInstanceID("REFUND");
				
				comm_mode = "refund";
				awaiting_socket_response = true;
				waiting_for_echo_data = "Lavu" + getCurrentDateTime(true).substring(2, 14);
				
				var total_refund_amount = amount;
				if (refund_tip.length > 0) {
					var rfnd_tp = parseFloat(refund_tip);
					if (rfnd_tp > 0) {
						var tra = (parseFloat(amount) + rfnd_tp);
						total_refund_amount = tra.toFixed(decimal_places);
					}
				}
				
				var amt = total_refund_amount.replace(".", "");
				amt = amt.replace(",", "");
			
				var fields = [];
				fields.push("04");					// Transaction Code
				fields.push(amt);					// Amount
				fields.push("A");					// Customer Track 2
				fields.push("");						// Original Approval Number
				fields.push("");						// Card Acceptor Terminal ID
				fields.push("");						// Invoice Number
				fields.push("");						// Promo Code
				fields.push(waiting_for_echo_data);	// Echo Data
				fields.push("");						// MOTO E-Commerce Flag
				
				//alert(JSON.stringify(fields));
				
				var str = fields.join(cFS) + cEOT;
				
				ACK_received = false;
				socketWrite(str, terminal_ip, use_socket_port);
			}

			function POSPADvoidRefund() {
				
				refreshHostInstanceID("VOID_REFUND");
				
				comm_mode = "void_refund";
				awaiting_socket_response = true;
				waiting_for_echo_data = "Lavu" + getCurrentDateTime(true).substring(2, 14);
				
				var total_void_amount = amount;
				if (void_tip.length > 0) {
					var vd_tp = parseFloat(void_tip);
					if (vd_tp > 0) {
						var tva = (parseFloat(amount) + vd_tp);
						total_void_amount = tva.toFixed(decimal_places);
					}
				}
				
				var amt = total_void_amount.replace(".", "");
				amt = amt.replace(",", "");
			
				var fields = [];
				fields.push("12");					// Transaction Code
				fields.push(amt);					// Amount
				fields.push("");						// Customer Track 2
				fields.push(orig_authcode);			// Original Approval Number
				fields.push("");						// Card Acceptor Terminal ID
				fields.push("");						// Invoice Number
				fields.push("");						// Promo Code
				fields.push(waiting_for_echo_data);	// Echo Data
				fields.push("");						// MOTO E-Commerce Flag
				
				var str = fields.join(cFS) + cEOT;
				
				ACK_received = false;
				socketWrite(str, terminal_ip, use_socket_port);
			}
			
			function POSPADresendLastResponse() {
			
				refreshHostInstanceID("RESEND_LAST");
			
				comm_mode = "last_ped_response";
				
				var fields = [];
				fields.push("79");
				
				var str = fields.join(cFS) + cEOT;
				
				ACK_received = false;
				socketWrite(str, terminal_ip, use_socket_port);	
			}

			function POSPADclerkSubtotals(clerk_id) {
			
				refreshHostInstanceID("CLERK_SUBTOTALS");
			
				comm_mode = "clerk_subtotals";
				awaiting_socket_response = true;
				report_clerk_id = clerk_id.paddingLeft("000000");
				
				var fields = [];
				fields.push("64");					// Transaction Code
				fields.push("");						// Card Acceptor Terminal ID
				fields.push("+" + report_clerk_id);	// Clerk ID
				fields.push("0");					// Clear Totals Flag

				var str = fields.join(cFS) + cEOT;
				
				ACK_received = false;
				socketWrite(str, terminal_ip, use_socket_port);
			}

			function POSPADdepositTotals() {
			
				refreshHostInstanceID("DEPOSIT_TOTALS");
			
				comm_mode = "deposit_totals";
				awaiting_socket_response = true;
				report_clerk_id = server_id.paddingLeft("000000");
				
				var fields = [];
				fields.push("67");	// Transaction Code
				fields.push("");		// Card Acceptor Terminal ID

				var str = fields.join(cFS) + cEOT;
				
				ACK_received = false;
				socketWrite(str, terminal_ip, use_socket_port);
			}
			
			function POSPADcloseBatch() {
			
				refreshHostInstanceID("CLOSE_BATCH");
			
				comm_mode = "close_batch";
				awaiting_socket_response = true;
				report_clerk_id = server_id.paddingLeft("000000");
				
				var fields = [];
				fields.push("65");	// Transaction Code
				fields.push("");		// Card Acceptor Terminal ID
				fields.push("C");	// Totals/Close Batch Indicator
				
				var str = fields.join(cFS) + cEOT;
				
				ACK_received = false;
				socketWrite(str, terminal_ip, use_socket_port);
			}
			
			function POSPADtipEntryMode(setto) {
				
				comm_mode = "tip_entry_mode";
				awaiting_socket_response = true;
				tip_entry_mode_setto = setto;
				
				var fields = [];
				fields.push("87");	// Transaction Code
				fields.push(setto);	// Confiuration Setting
				
				var str = fields.join(cFS) + cEOT;
				
				ACK_received = false;
				socketWrite(str, terminal_ip, use_socket_port);
			}
						
// * * * * * * * * * * Error Codes * * * * * * * * * * //

			function errorMessageForCodes(co_co, b24_co) {
				
				var message = "Error: " + verbalizeCode("condition", co_co);
				if (b24_co != "") error_message += " - " + verbalizeCode("base24", b24_co);
				
				return message;
			}

			function verbalizeCode(type, code) {
			
				var message = code;
			
				if (type == "condition") {
					
					switch (code) {
					
						case "051": message = "Please try again"; break;
						case "100": message = "Invalid transaction code"; break;
						case "103": message = "Invalid tender type"; break;
						case "111": message = "Invalid Merchant ID"; break;
						case "112": message = "Missing Merchant ID"; break;
						case "113": message = "Invalid ECR ID"; break;
						case "114": message = "Missing ECR ID"; break;
						case "115": message = "Invalid Clerk ID"; break;
						case "117": message = "Missing Clerk ID"; break;
						case "150": message = "Request not allowed"; break;
						case "152": message = "Refund limit exceeded"; break;
						case "163": message = "Transaction in progress"; break;
						case "200": message = "Initialization Failed"; break;
						case "201": message = "Mismatched PED ID"; break;
						case "206": message = "Invalid software version number"; break;
						case "250": message = (comm_mode == "initialize")?"Initialization failed":"No response from host"; break;					
						case "301": message = "Data block is missing LRC"; break;
						case "303": message = "Data block is missing STX"; break;
						case "304": message = "MAC failure. Please try again."; break;
						case "307": message = "Transaction not completed (307)"; break;
						case "400": message = "Exceeded time to respond"; break;
						case "607": message = "Card was removed prior to completion of the transaction."; break;
						case "608": message = "Declined by card."; break;
						case "609": message = "Transaction not completed (609)"; break;
						case "616": message = "Transaction not completed (616)"; break;
						case "640": message = "Transaction not completed (640)"; break;
						case "784": message = "Invalid surcharge amount"; break;
					
						default: break;
					}
					
				} else if (type == "base24") {

					switch (code) {
					
						case "052": message = "PIN Retries Exceeded"; break;
						case "065": message = "Exceeds Correction Limit"; break;
						case "076": message = "Insufficient Funds"; break;
						case "105": message = "Invalid card"; break;
						case "113": message = "Cannot process. Please retry."; break;
						case "201": message = "Incorrect PIN"; break;
						case "426": message = "Declined"; break;
						case "476": message = "Invalid transaction (476)"; break;
						case "477": message = "Invalid card number"; break;
						case "478": message = "Declined 41 - Hold Card Call"; break;
						case "482": message = "Expired Card"; break;
						case "483": message = "Refer call to issuer"; break;
						case "802": message = "Invalid Clerk ID"; break;
						case "855": message = "Invalid amount"; break;
						case "882": message = "Initialization aborted"; break;
						case "898": message = "Re-Try Edit Error 898"; break;
						case "960": message = "Mismatched Merchant ID"; break;
						case "961": message = "Mismatched PED ID"; break;
						case "965": message = "Invalid software version number"; break;
					
						default: break;
					}
				}
							
				return message;
			}

// * * * * * * * * * * Translations * * * * * * * * * * * * * * //

			function translate(wop, doit) {
				
				var rtn = wop;
				if (doit) {
					switch (wop) {
						case "ACCT":									rtn = "COMPTE"; break;
						case "AMOUNT":								rtn = "MONTANT"; break;
						case "Approved!":							rtn = "Approuvee!"; break;
						case "APPROVED - THANK YOU":				rtn = "APPROUVEE - MERCI"; break;
						case "AUTH #":								rtn = "# AUTOR"; break;
						case "CARD NUMBER":							rtn = "NUMERO CARTE"; break;
						case "CARD REMOVED":						rtn = "CARTE RETIREE"; break;
						case " CHEQUING":							rtn = " CHEQUE"; break;
						case "CHIP CARD KEYED":						rtn = "CARTE A PUCE TAPEE"; break;
						case "CHIP CARD MALFUNCTION":				rtn = "DEFAILLANCE CARTE A PUCE"; break;
						case "CHIP CARD SWIPED":					rtn = "CARTE A PUCE GLISSEE"; break;
						case "CUSTOMER COPY":						rtn = "COPIE DU CLIENT"; break;
						case "DATE/TIME":							rtn = "DATE/HEURE"; break;
						case "DECLINED BY CARD":					rtn = "REFUSEE PAR CARTE"; break;
						case " DEFAULT":							rtn = " DEFAUT"; break;
						case "I agree to pay the above total amount according to the card issuer agreement":
							rtn = "Je accepte de payer le montant total ci-dessus conformement a l'accord de l'emetteur de la carte";
							break;
						case "Merchant":							rtn = "Detaillant"; break;
						case "MERCHANT COPY":						rtn = "COPIE DU DETAILLANT"; break;
						case "PURCHASE":							rtn = "ACHAT"; break;
						case "PURCHASE CORRECTION":					rtn = "CORRECTION D'ACHAT"; break;
						case "REFERENCE #":							rtn = "# REFERENCE"; break;
						case "REFUND":								rtn = "REMISE D'ACHAT"; break;
						case "REFUND CORRECTION":					rtn = "CORRECTION DE REMISE"; break;
						case "Retain this copy for your records":	rtn = "Conserver cette copie pour vos dossiers"; break;
						case " SAVINGS":							rtn = " EPARGNE"; break;
						case "TIP":									rtn = "POURBOIRE"; break;
						case "TRANSACTION NOT APPROVED":			rtn = "OPERATION REFUSEE"; break;
						case "TRANSACTION NOT COMPLETED":			rtn = "OPERATION NON COMPLETEE"; break;
						case "VERIFIED BY PIN":						rtn = "VERIFIEE PAR NIP"; break;
						default: break;	
					}
				}
				
				return rtn;
			}

// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //
			
			function close1() {
			
				if (device_failed == 0) {

					hold_comm_mode = comm_mode;
					awaiting_socket_response = true;
					comm_mode = "close";
					ACK_received = false;	
					socketWrite("97" + cEOT, terminal_ip, use_socket_port);
					
				} else if (device_failed == 2) {
					
					componentAlert("Unable to Close", "Awaiting response from iPP320 terminal.");
	
				} else {
				
					awaiting_socket_response = false;	
					close2();
				}
			}
			
			function close2() {
			
				window.location = "_DO:cmd=close_overlay";	
			}
			
			function showActivityShade(message) {
				
				document.getElementById('activity_message').innerHTML = message;
				document.getElementById('activity_shade').style.display = "block";
			}
			
			function hideActivityShade() {

				document.getElementById('activity_shade').style.display = "none";				
			}
	
			function hideAllBut(but) {
				
				if (but=="transaction_panel" && terminal_id.length>0) document.getElementById("terminal_id_label").innerHTML = "Terminal ID: " + terminal_id;
				
				if (but == "admin1") buildAdminMenu();
				
				var divs = ["enter_ip", "ip_help", "enter_terminal_id", "transaction_panel", "admin1", "tip_entry_settings", "approved_panel", "response_panel", "confirm_panel", "report_panel"];
				for (var d = 0; d < divs.length; d++) {
					var did = divs[d];
					document.getElementById(did).style.display = (did == but)?"block":"none";	
				}
			}
		
// * * * * * * * * * * IP and Intialization * * * * * * * * * * //			
			
			function checkInitialization() {

				window.location = "_DO:cmd=get_termset&keys=terminal_ip|_|terminal_id|_|terminal_initialized|_|terminal_clerk_id|_|tip_entry_mode&callback=currentInitialization";
			}

			function currentInitialization(keys, vals) {
				
				var k = keys.split("|_|");
				var v = vals.split("|_|");

				var ip = "";
				var id = "";
				for (var i = 0; i < k.length; i++) {
					switch (k[i]) {
						case "terminal_ip": ip = fromHex(v[i]); break;
						case "terminal_id": id = fromHex(v[i]); break;
						case "terminal_initialized": terminal_initialized = fromHex(v[i]); break;
						case "terminal_clerk_id": terminal_clerk_id = fromHex(v[i]); break;
						case "tip_entry_mode": tip_entry_mode = fromHex(v[i]); break;
						default: break;	
					}
				}
				
				if (validateIP(ip)) {
					terminal_ip = ip;
					if (id.length > 0) {
						terminal_id = id;
						checkTerminalClerkID();
					} else enterTerminalID();
				} else {
					terminal_initialized = "0";
					enterIP();
				}
			
				return 1;
			}
			
			function validateIP(ip) {
				
				var valid = true;
				var ip_parts = ip.split(".");
				if (ip_parts.length < 4) valid = false;
				else {
					for (var i = 0; i < ip_parts.length; i++) {
						var p = ip_parts[i];
						var l = p.length;
						if (l<1 || l>3 || parseInt(p)>255 || ((i==0 || i==3) && parseInt(p)==0)) {
							valid = false;
							break;
						}
					}
				}

				return valid;
			}
			
			var ip_dot_count = 0;
			var ip_dot_digits = 0;
			
			function enterIP() {
				
				hideActivityShade();
				hideAllBut("enter_ip");
				document.getElementById('ip_input').value = "";
				
				ip_dot_count = 0;
				ip_dot_digits = 0;
			}
			
			function enterIPButtonPressed(btn) {
									
				var input_field = document.getElementById("ip_input");
				var input_text = input_field.value;
				var len = input_text.length;
				
				if (btn == "back") {					
					if (len > 0) {
						var i = (len - 1);
						var last_char = input_text.charAt(i);
						input_field.value = input_text.substring(0, i);
						if (last_char == ".") {
							ip_dot_count--;
							ip_dot_digits = 0;
							for (var ii = (i - 1); ii >= 0; ii--) {
								if (input_text.charAt(ii) == ".") break;
								else ip_dot_digits++;
							}
						} else ip_dot_digits--;
					}
				} else if (btn == ".") {
					if (ip_dot_digits>0 && ip_dot_count<3) {
						input_field.value = input_text + ".";
						ip_dot_digits = 0;
						ip_dot_count++;
					}
				} else if (ip_dot_digits < 3) {
					input_field.value = input_text + "" + btn;
					ip_dot_digits++;
				}
			}
			
			function submitIP() {
			
				var input_field = document.getElementById("ip_input");
				var setIP = input_field.value;
				
				if (validateIP(setIP)) {
					terminal_ip = setIP;
					window.location = "_DO:cmd=set_termset&keys=terminal_ip&vals=" + toHex(setIP) + "&callback=didSetIP";
				} else {
					input_field.value = "";
					ip_dot_count = 0;
					ip_dot_digits = 0;
					componentAlert("Invalid IP Address", "Please try again. Press 'Help' if you continue to have trouble.");
				}
			}
			
			function didSetIP(keys, vals) {
				
				enterTerminalID();
				
				return 1;
			}
			
			function enterTerminalID() {
				
				hideActivityShade();
				hideAllBut("enter_terminal_id");
				document.getElementById('terminal_id_input').value = "";
			}
			
			function terminalIDButtonPressed(btn) {
				
				var input_field = document.getElementById("terminal_id_input");
				var input_text = input_field.value;
				var len = input_text.length;
				
				if (btn == "back") {					
					if (len > 0) {
						var i = (len - 1);
						var last_char = input_text.charAt(i);
						input_field.value = input_text.substring(0, i);
					}
				} else if (len < 8) input_field.value = input_text + "" + btn;
			}
			
			function submitTerminalID() {

				var input_field = document.getElementById("terminal_id_input");
				var setTerminalID = input_field.value;
				
				if (setTerminalID.length > 0) {
					terminal_id = setTerminalID;
					window.location = "_DO:cmd=set_termset&keys=terminal_id&vals=" + toHex(setTerminalID) + "&callback=didSetTerminalID";
				} else componentAlert("Invalid Terminal ID", "Please try again.");		
			}
			
			function didSetTerminalID(keys, vals) {
				
				initialize();
				
				return 1;
			}
			
			function initialize() {
				
				active_clerk_id = "0";
				showActivityShade("Initializing terminal<br>at " + terminal_ip + "...<br><br>(Terminal ID " + terminal_id + ")");
				window.setTimeout(function(){POSPADinitialize()}, 100);
			}
			
			function setInitializedFlag(setto) {
		
				terminal_initialized = setto;
				window.location = "_DO:cmd=set_termset&keys=terminal_initialized|_|tip_entry_mode&vals=" + toHex(setto) + "|_|" + toHex("TF") + "&callback=didSetInitializationFlag";
			}
			
			function didSetInitializationFlag(keys, results) { return 1; }
			
			function didSetTipEntryMode(keys, results) { return 1; }
			
// * * * * * * * * * * Clerk Logon / Logoff * * * * * * * * * * //

			var terminal_clerk_id = "";

			function checkTerminalClerkID() {
			
				device_failed = 0;
				
				if (terminal_initialized != "1") initialize();
				else if (terminal_clerk_id=="" || terminal_clerk_id!=server_id) {
					if (secure_pass_thru) {
						window.setTimeout(function(){setTerminalClerkID(server_id)}, 250);
						window.setTimeout(function(){initiateAction()}, 500);
					} else {					
						showActivityShade("Logon Clerk " + server_id + "...");
						window.setTimeout(function(){POSPADaddClerk(server_id)}, 250);
					}
				} else initiateAction();
			}
			
			function setTerminalClerkID(clerk_id) {
			
				terminal_clerk_id = clerk_id;
				window.location = "_DO:cmd=set_termset&keys=terminal_clerk_id&vals=" + toHex(clerk_id) + "&callback=didSetTerminalClerkID";
			}
			
			function didSetTerminalClerkID(keys, results) { return 1; }

// * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

			//var rsps_index = 0;
			//var rsps = [];
			//rsps.push(["refund","3039391c30341c3135303430393133313435331c373030301c3531303838382a2a2a2a2a2a343131301c313730321c30301c3030301c3130333731311c303031303033303036301c331c36313030303834341c531c301c1c1c1c44454249541c1c1c1c1c501c1c1c50201c1c4c61767531353034303931333134343704"]);

			function initiateAction() {
				
				if (device_failed == 2) {
					
					showActivityShade("");
					window.setTimeout(function(){POSPADresendLastResponse()}, 250);
					
				} else {
				
					device_failed = 0;
					
					if (terminal_initialized != "1") initialize();
					else if (terminal_clerk_id=="" || terminal_clerk_id!=server_id) checkTerminalClerkID();
					
					//else window.setTimeout(function(){reprocessResponse()}, 250);
					
					else {
				
						switch (action) {
						
							case "Sale":
								
								hideActivityShade();
								document.getElementById("tx_title").innerHTML = "Ready for purchase...";
								document.getElementById("tx_message").innerHTML = "Please follow the instructions on your iPP320 terminal.";
								hideAllBut("transaction_panel");
								POSPADpurchase();
								
								break;
								
							case "Void":
							
								hideActivityShade();
								document.getElementById("tx_title").innerHTML = "Ready to perform void...";
								document.getElementById("tx_message").innerHTML = "Please follow the instructions on your iPP320 terminal.";
								hideAllBut("transaction_panel");
								if (orig_action == "Refund") POSPADvoidRefund();
								else POSPADvoidPurchase();
							
								break;
								
							case "Refund":
							
								hideActivityShade();
								document.getElementById("tx_title").innerHTML = "Ready to perform refund...";
								document.getElementById("tx_message").innerHTML = "Please follow the instructions on your iPP320 terminal.";
								hideAllBut("transaction_panel");
								POSPADrefund();
								
								break;
								
							case "Admin": {
								
								switch (admin_type) {
									
									case "server":
									
										hideAllBut("");
										showActivityShade("Loading totals for<br>" + server_name + "<br>(Clerk " + server_id.paddingLeft("000000") + ")<br>");
										window.setTimeout(function(){POSPADclerkSubtotals(server_id)}, 250);
									
										break;
										
									case "manager":
	
										hideActivityShade();
										hideAllBut("admin1");
			
										break;
										
									default:
									
										// display error - invalid admin type?
									
										break;
									
								}
		
								break;
							}
								
							default:
							
								componentAlert("Component Error", "Unrecognized action: " + action);
								window.setTimeout(function(){close1()}, 250);
							
								break;
						}	
					}
				}
			}
			
			function reprocessResponse() {

				awaiting_socket_response = true;

				var rsp = rsps[rsps_index];
				comm_mode = rsp[0];
					
				TCPSocketResponse(rsp[1], "1");
				
				rsps_index++;
				if (rsps_index < rsps.length) window.setTimeout(function(){reprocessResponse()}, 2000);
			}
			
			function showApproved(message) {
				
				hideActivityShade();
				document.getElementById("approved_message").innerHTML = message;
				hideAllBut("approved_panel");
			}
			
			function transactionFailed(title, message) {
				
				hideActivityShade();
				document.getElementById("response_title").innerHTML = title;
				document.getElementById("response_text").innerHTML = message;
				hideAllBut("response_panel");
				
				document.getElementById("response_panel_reconfigure").style.display = (title == "Device Error")?"inline":"none";
			}
			
			function confirmYes() {
				
				switch (confirm_mode) {
				
					case "close_batch":
					
						showActivityShade("Closing current batch<br>");
						window.setTimeout(function(){POSPADcloseBatch()}, 250);
						
						break;
						
					case "reset_totals":
					
						alert("Temporarily unavailable");
						break;
						
					case "reconfigure":
					
						window.location = "_DO:cmd=set_termset&keys=terminal_ip|_|terminal_id|_|terminal_initialized&vals=|_||_|0&callback=didReconfigure";					
						break;
						
					default:
						break;	
				}
			}
			
			function confirmNo() {

				switch (confirm_mode) {
				
					case "close_batch":
					case "reset_totals":
					case "reconfigure":
					default:				
						hideAllBut("admin1");
						break;	
				}
			}

// * * * * * * * * * * Reports & Receipts * * * * * * * * * * //

			function printReceipt(tx_info, ref_data, approved) {
			
				var type = "PURCHASE";
				switch (ref_data.t_type) {
					case "void_purchase": type = "PURCHASE CORRECTION"; break;
					case "refund": type = "REFUND"; break;
					case "void_refund": type = "REFUND CORRECTION"; break;
					default: break;
				}
				
				var use_register = (register == "")?"receipt":register;
				str = "";
				
				var acct_type = "";
				var display_card_type = tx_info.card_type;
				var display_acct_type = "";
				//if (tx_info.card_type=="INTERAC" || tx_info.card_type=="FLASH") {
					switch (ref_data.a_type) {
						case "0":
							if (display_card_type != "DEBIT") {
								acct_type = "default";
								display_card_type = "FLASH";
								display_acct_type = " DEFAULT";
							}
							break;
						case "1": 
							acct_type = "checking";
							display_acct_type = " CHEQUING";
							break;
						case "2":
							acct_type = "savings";
							display_acct_type = " SAVINGS";
							break;
						default: 
							acct_type = "credit";
							break;
					}
				//}
				
				var display_swipe_grade = tx_info.swipe_grade;
				var chip_card_malfunction = false;
				if (display_swipe_grade == "Q") {
					display_swipe_grade = "C";
					chip_card_malfunction = true;
				}
			
				for (var r = 0; r <= 1; r++) {
					
					var merchant_copy = (r==0);
					var trnslt = ((merchant_copy && (ref_data.lang=="4" || ref_data.lang=="6")) || (r==1 && (ref_data.lang=="4" || ref_data.lang=="5")));
					
					str += (merchant_copy?"active":use_register) + ":";
					str += encodeToPrint(location_name) + ":";
					str += encodeToPrint(location_address) + ":";
					str += encodeToPrint(location_city + ", " + location_state + " " + location_zip) + ":";
					str += " :";
					str += "TYPE[c " + translate(type, trnslt) + ":";
					str += " :";
					str += translate("ACCT", trnslt) + "[c " + display_card_type + translate(display_acct_type, trnslt) + ":";
					str += translate("AMOUNT", trnslt) + "[c*" + displayMoneyWithSymbol(tx_info.total_collected) + ":";
					if (parseFloat(tx_info.tip_amount) > 0) {
						str += translate("TIP", trnslt) + "[c*" + displayMoneyWithSymbol(tx_info.tip_amount) + ":";
						var withTip = (parseFloat(tx_info.total_collected) + parseFloat(tx_info.tip_amount)) + "";
						str += "TOTAL[c*" + displayMoneyWithSymbol(withTip) + ":";
					}
					str += " : :";
					str += translate("CARD NUMBER", trnslt) + "[c " + encodeToPrint(ref_data.cn) + ":";
					str += translate("DATE/TIME", trnslt) + "[c " + encodeToPrint(ref_data.datetime) + ":";
					str += translate("REFERENCE #", trnslt) + "[c " + terminal_id + tx_info.transaction_id + " " + display_swipe_grade + ":";
					if (approved) str += translate("AUTH #", trnslt) + "[c " + tx_info.auth_code + ":";
					str += " :";
					if (typeof ref_data.app_l != 'undefined') {
						var app_name_or_label = ((ref_data.app_n.length > 0)?ref_data.app_n:ref_data.app_l);
						if (app_name_or_label.length > 0) {
							str += app_name_or_label + ":";
							str += ref_data.aid + ":";
							str += ref_data.tvr + ":";
							str += ref_data.tsi + ":";
							str += " :";
						}
					}
					var vbp = (ref_data.cvm=="P" || ref_data.cvm=="B");
					switch (tx_info.swipe_grade) {
						case "F":
							str += translate("CHIP CARD SWIPED", trnslt) + ":";
							if (!vbp) str += " :";
							break;
						case "G": 
							str += translate("CHIP CARD KEYED", trnslt) + ":";
							if (!vbp) str += " :";
							break;
						default:
							break;	
					}
					if (merchant_copy) {
						if (chip_card_malfunction) str += translate("CHIP CARD MALFUNCTION", trnslt) + ": :";					
						if (vbp) str += translate("VERIFIED BY PIN", trnslt) + ": :";	
					}
					str += " :";
					if (approved) {
						str += " %20 %20 %20 %20" + ref_data.iso + " " + translate("APPROVED - THANK YOU", trnslt) + " " + ref_data.b24 + ":";
						str += " :";
						
						var print_signature_line = false;
						var merchant_signature = false;
						if (ref_data.cvm=="S" || ref_data.cvm=="B") {							
							switch (tx_info.card_type.toUpperCase().charAt(0)) {
								case "V": // Visa
									print_signature_line = merchant_copy;
									break;
								case "A": // Amex
								case "J": // JCB
								case "D": // Discover
								case "M": // MasterCard
								default: // Private Labels, etc...
									if (type=="PURCHASE" || type=="REFUND CORRECTION") print_signature_line = merchant_copy;
									else if (type=="PURCHASE CORRECTION" || type=="REFUND") {
										print_signature_line = !merchant_copy;
										merchant_signature = true;
									}
									break;
							}
						}
						
						if (print_signature_line) {
							str += " :___________________________:";
							str += "SIGNATURE";
							if (merchant_signature) str += " (" + translate("Merchant", trnslt) + ")";
							str += " : :";
						} // || acct_type=="credit")
						if (merchant_copy && type=="PURCHASE" && print_signature_line) str += translate("I agree to pay the above total amount according to the card issuer agreement", trnslt) + ": :";
						str += " :";
						if (ref_data.cvm == "N") str += " %20 %20 %20 %20 NO SIGNATURE TRANSACTION: : :";
					} else {
						switch (ref_data.co_co) {
							case "051":
							case "307":
								str += " %20 %20 " + ref_data.iso + " " + translate("TRANSACTION NOT COMPLETED", trnslt) + " " + ref_data.b24 + ":";
								break;
							case "607":
								str += translate("CARD REMOVED", trnslt) + " - 991: :";
							case "608":
							case "609":
							case "616":
							case "640":
								if (ref_data.co_co == "608") str += translate("DECLINED BY CARD", trnslt) + " - 990: :";
								str += " %20 %20 %20 %20 " + translate("TRANSACTION NOT COMPLETED", trnslt) + ":";
								break;
							default:
								str += " %20 %20 " + ref_data.iso + " " + translate("TRANSACTION NOT APPROVED", trnslt) + " " + ref_data.b24 + ":";
								break;
						}
						str += " :";
					}
					if (!merchant_copy && acct_type=="credit") {
						str += " %20 %20 %20 %20 %20 %20 -- IMPORTANT --:";
						if (!trnslt) str += " %20 %20";
						str += " " + translate("Retain this copy for your records", trnslt) + ":";
						str += " :";
					}
					str += " :";
					str += " %20 %20 %20 %20 %20 [s[s[s " + translate((merchant_copy?"MERCHANT COPY":"CUSTOMER COPY"), trnslt) + " [s[s[s:";
					str += " :";
					if (merchant_copy) str += "[new_print_job]:";
				}
			
				document.location = "_DO:cmd=print&order_id=" + order_id + "&print_string=" + encodeURIComponent(str);
			}
			
			function displayReport() {
				
				document.getElementById("admin_menu_button").style.display = (admin_type == "manager")?"block":"none";
				
				hideActivityShade();
				hideAllBut("report_panel");
				
				var keys = [];
				for (var k in report_info) keys.push(k);
				
				var str = "<table cellspacing='0' cellpadding='0'><tr><td align='center'><table width='274px' cellspacing='0' cellpadding='3'>";
				
				for (i = 0; i < keys.length; i++) {
					
					var report_line = report_info[keys[i]];
					var border = (typeof report_line.border != 'undefined');
					var border_code = border?" style='border-bottom:1px solid #EEEEEE'":"";
					
					if (typeof report_line.title != 'undefined') {
				
						document.getElementById("report_title_cell").innerHTML = "<span class='report_title'>" + report_line.title + "</span>";
						
					} else if (typeof report_line.new_table != 'undefined') {
				
						str += "</table></td></tr><tr><td align='center'";
						if (border) str += " style='border-top:1px solid #AAAAAA'";
						str += "><table width='274px' cellspacing='0' cellpadding='3'>";
				
					} else if (typeof report_line.field1!='undefined' && typeof report_line.value!='undefined') {

						str += "<tr><td class='report_field' align='left'" + border_code + ">" + report_line.field1 + ":</td><td class='report_value' align='right'" + border_code + ">" + report_line.value + "</td></tr>";
	
					} else if (typeof report_line.field2!='undefined' && typeof report_line.value!='undefined') {

						str += "<tr><td class='report_field' align='left'" + border_code + ">" + report_line.field2 + ":</td><td class='report_value' align='right'" + border_code + ">" + report_line.value + "</td></tr>";
						
					} else if (typeof report_line.field != 'undefined') {

						str += "<tr><td class='report_field' colspan='2' align='right'" + border_code + ">" + report_line.field + "</td></tr>";

					} else if (typeof report_line.value != 'undefined') {

						str += "<tr><td class='report_value' colspan='2' align='left'" + border_code + ">" + report_line.value + "</td></tr>";						
					}
				}
				
				str += "</table></td></tr></table>";
				
				document.getElementById("inner_report_panel").innerHTML = str;
			}
			
			function printReport() {
			
				var str = "active:";
	
				var keys = [];
				for (var k in report_info) keys.push(k);
							
				for (i = 0; i < keys.length; i++) {
					
					var report_line = report_info[keys[i]];
					var border = (typeof report_line.border != 'undefined');
				
					if (typeof report_line.title != 'undefined') {

						str += encodeToPrint(report_line.title) + ": :";
						
					} else if (typeof report_line.new_table != 'undefined') {
				
						if (border) str += "---------------------------: :";
				
					} else if (typeof report_line.field1!='undefined' && typeof report_line.value!='undefined') {

						str += encodeToPrint(report_line.field1) + "[c ";
						str += encodeToPrint(report_line.value) + ":";
						if (border) str += " :";

					} else if (typeof report_line.field2!='undefined' && typeof report_line.value!='undefined') {

						str += encodeToPrint(report_line.field2) + "[c*";
						str += report_line.value + ":";
						if (border) str += " :";
										
					} else if (typeof report_line.field != 'undefined') {

						str += encodeToPrint(report_line.field) + ":";
						if (border) str += " :";

					} else if (typeof report_line.value != 'undefined') {

						str += " *" + encodeToPrint(report_line.value) + ":";
						if (border) str += " :";
					}
				}
				str += " :---------------------------: : :";
				
				document.location = "_DO:cmd=print&order_id=" + order_id + "&print_string=" + encodeURIComponent(str);
			}
			
			function encodeToPrint(str) {
				
				return str.replace(/:/g, "[c").replace(/\*/g, "[s");
			}

// * * * * * * * * * * Admin Menu * * * * * * * * * * //
			
			function buildAdminMenu() {
				
				var str = "<table style='width:280px; height:367px;'>";
				str += "<tr><td class='msg_txt2' align='center' valign='bottom' style='height:40px; padding: 6px 8px 12px 8px;'>Admin Menu</td></tr>";
				if (!secure_pass_thru) {
					str += "<tr><td class='msg_txt1' align='center' valign='top' style='height:47px; padding-top:6px;'><button class='btn_light_long' ontouchstart='getDepositTotals();'><b>Deposit Totals</b></button></td></tr>";
				}
				str += "<tr><td class='msg_txt1' align='center' valign='top' style='height:47px; padding-top:6px;'><button class='btn_light_long' ontouchstart='confirmCloseBatch();'><b>Close Batch</b></button></td></tr>";
				if (!secure_pass_thru) {
					str += "<tr><td class='msg_txt1' align='center' valign='top' style='height:47px; padding-top:6px;'><button class='btn_light_long' ontouchstart='confirmResetTotals();'><b>Reset Totals</b></button></td></tr>";
				}
				str += "<tr><td class='msg_txt1' align='center' valign='top' style='height:47px; padding-top:6px;'><button class='btn_light_long' ontouchstart='tipEntrySettings();'><b>Tip Settings</b></button></td></tr>";
				str += "<tr><td class='msg_txt1' align='center' valign='top' style='height:134px; padding-top:6px;'><button class='btn_light_long' ontouchstart='confirmReconfigure();'><b>Reconfigure</b></button></td></tr>";
				str += "</table>";
	
				document.getElementById("inner_admin1").innerHTML = str;
			}
			
			function getDepositTotals() {
				
				showActivityShade("Loading totals<br>");
				window.setTimeout(function(){POSPADdepositTotals()}, 250);
			}
			
			function confirmCloseBatch() {
			
				hideActivityShade();
				document.getElementById("confirm_title").innerHTML = "Close Batch";
				document.getElementById("confirm_text").innerHTML = "Are you sure you want to close the current batch?";
				confirm_mode = "close_batch";
				hideAllBut("confirm_panel");	
			}
			
			function confirmResetTotals() {
			
				hideActivityShade();
				document.getElementById("confirm_title").innerHTML = "Reset Totals";
				document.getElementById("confirm_text").innerHTML = "Are you sure you want to reset all totals?";
				confirm_mode = "reset_totals";
				hideAllBut("confirm_panel");	
			}
			
			function tipEntrySettings() {
			
				hideActivityShade();

				var current_setting = "Disabled";
				switch (tip_entry_mode) {
					case "TN": current_setting = "Debit Only"; break;
					case "TB": current_setting = "Debit and Credit"; break;
					default: break;
				}

				document.getElementById("tip_entry_current_setting").innerHTML = "Current setting:<br><b>" + current_setting + "</b>";
				hideAllBut("tip_entry_settings");					
			}
			
			function setTipEntry(setto) {
			
				showActivityShade("Setting tip entry mode<br>");	
				window.setTimeout(function(){POSPADtipEntryMode(setto)}, 250);
			}

			function confirmReconfigure() {
			
				hideActivityShade();
				document.getElementById("confirm_title").innerHTML = "Reconfigure Terminal";
				document.getElementById("confirm_text").innerHTML = "Are you sure you want to reconfigure this terminal?";
				confirm_mode = "reconfigure";
				hideAllBut("confirm_panel");	
			}
			
			function didReconfigure() {
			
				terminal_ip = "";
				terminal_id = "";
				terminal_initialized = "0";
				enterIP();
				
				return 1;
			}
			
// * * * * * * * * * * Admin Menu * * * * * * * * * * //
	
			var ip_help_page = 1;
		
			function previousIPhelp() {
			
				IPhelp(ip_help_page - 1);
			}
			
			function nextIPhelp() {

				IPhelp(ip_help_page + 1);				
			}

			function IPhelp(page) {
				
				ip_help_page = page;
				var ttl = document.getElementById("ip_help_title");
				var msg = document.getElementById("ip_help_message");
				
				switch (ip_help_page) {
				
					case 1:
					
						hideAllBut("ip_help");
						ttl.innerHTML = "iPP320 Terminal<br>Configuration";
						msg.innerHTML = "To enter Configuration mode, first hold the yellow <b>CORR</b> key and press the <b>.,#*</b> key to power cycle the terminal.<br><br>At the Version Information screen (U-####-########), press the red <b>CANC</b> key and then press the green <b>OK</b> key.'";
						break;
						
					case 2:
					
						ttl.innerHTML = "Terminal Language";
						msg.innerHTML = "You will know that you have entered Configuration mode once the LANGUAGE/LANGUE screen is displayed.<br><br>Press <b>F1</b> or <b>F4</b> to choose your language.";
						break;

					case 3:
					
						ttl.innerHTML = "Contactless Reader";
						msg.innerHTML = "The CONTACTLESS READER screen prompts to enable or disable the Embedded Contactless Reader.<br><br>Press <b>F1</b> or <b>F4</b> to make your selection.";
						break;
									
					case 4:
					
						ttl.innerHTML = "Integration Mode";
						msg.innerHTML = "The MODE screen prompts to select the Integration Mode.<br><br>Press the <b>F1</b> key to scroll down to PCI. Press the green <b>OK</b> key to make your selection.";
						break;

					case 5:
					
						ttl.innerHTML = "Ethernet IP Type";
						msg.innerHTML = "Press <b>F1</b> to select STATIC and proceed to the next step.";
						break;

					case 6:
					
						ttl.innerHTML = "Terminal IP Address";
						msg.innerHTML = "If already configured, an IP address will be displayed<br>(such as 10.0.2.25). If not, enter an IP address using the <b>.,#*</b> key for the period delimiter.<br><br>Make sure the IP address is valid for your network configuration and take note of it. Press the green <b>OK</b> key when finished.";
						break;

					case 7:
					
						ttl.innerHTML = "Subnet Mask";
						msg.innerHTML = "Please refer to you network router configuration for the proper Subnet Mask.<br><br>Enter the Subnet Mask using the numeric keypad and press the green <b>OK</b> key when finished.";
						break;

					case 8:
					
						ttl.innerHTML = "Default Gateway";
						msg.innerHTML = "Please refer to you network router configuration for the proper Default Gateway.<br><br>Enter the Default Gateway using the numeric keypad and press the green <b>OK</b> key when finished.";
						break;

					case 9:
					
						//msg.innerHTML = "The MODE screen prompts to select the Integration Mode.<br><br>Press the <b>F1</b> key to scroll down to PCI. Press the green <b>OK</b> key to make your selection.";

						ttl.innerHTML = "IP Host Setup 1";
						msg.innerHTML = "Use the <b>F1</b>/<b>F4</b> keys to scroll up/down to PUBLIC(2048) and press the green <b>OK</b> key to make your selection.";
						break;

					case 10:
					
						ttl.innerHTML = "IP Host Setup 2";
						msg.innerHTML = "Use the <b>F1</b>/<b>F4</b> keys to scroll up/down to Public and press the green <b>OK</b> key to make your selection.";
						break;

					case 11:
					
						ttl.innerHTML = "Primary DNS Address";
						msg.innerHTML = "Please refer to you network router configuration for the proper Primary DNS Address.<br><br>Enter the Primary DNS Address using the numeric keypad and press the green <b>OK</b> key when finished.";
						break;

					case 12:
					
						ttl.innerHTML = "Secondary DNS Address";
						msg.innerHTML = "Please refer to you network router configuration for the proper Secondary DNS Address.<br><br>Enter the Secondary DNS Address using the numeric keypad and press the green <b>OK</b> key when finished.";
						break;

					case 13:
					
						ttl.innerHTML = "IP Configuration<br>Retry Period";
						msg.innerHTML = "Enter the IP Configuration Retry Period (in seconds) using the numeric keypad. The default is 10 seconds.<br><br>Press the green <b>OK</b> key when finished.";
						break;

					case 14:
					
						ttl.innerHTML = "Host Connection<br>Timeout Period";
						msg.innerHTML = "Enter the Host Connection Timeout period (in seconds) using the numeric keypad. The default is 20 seconds.<br><br>Press the green <b>OK</b> key when finished.";
						break;

					case 15:
					
						ttl.innerHTML = "Remove Card Beep";
						msg.innerHTML = "Press <b>F1</b> or <b>F4</b> to select whether or not you want the terminal to beep upon card removal.";
						break;

					case 16:
					
						ttl.innerHTML = "Cashback";
						msg.innerHTML = "POS Lavu does not support the Cashback feature.<br><br>Press <b>F4</b> for NO to disable Cashback.";
						break;

					case 17:
					
						ttl.innerHTML = "Surcharge";
						msg.innerHTML = "Use the <b>F1</b>/<b>F4</b> keys to scroll up/down and press the green <b>OK</b> key to make your selection.";
						break;

					case 18:
					
						ttl.innerHTML = "Status Report";
						msg.innerHTML = "POS Lavu does not support the Status Report feature.<br><br>Press <b>F4</b> for NO to disable Status Report.";
						break;
						
					case 19:
					
						ttl.innerHTML = "ERNEX";
						msg.innerHTML = "POS Lavu does not support ERNEX.<br><br>Press <b>F4</b> to DISABLE ERNEX.";

					case 20:
					
						ttl.innerHTML = "Terminal ID";
						msg.innerHTML = "If the terminal has been previously configured you will see a screen displaying the Terminal ID. This is the ID number of the ECR or device the terminal is connected to.<br><br>Press the green <b>OK</b> key to continue.";
						break;
						
					case 21:
					
						ttl.innerHTML = "PED Serial Number";
						msg.innerHTML = "The screen showing your terminal's Serial Number marks the end of the configuration process.<br><br>Press the green <b>OK</b> key to complete the configuration.";
						break;
						
					case 22:
					
						ttl.innerHTML = "Configuration Complete";
						msg.innerHTML = "Once the configuration is complete, the parameters are saved, and the device will begin communicating with the host. You will see several \"Sending/Receiving\" messages.<br><br>When the terminal displays the WELCOME/BONJOUR screen, enter the terminal IP address into POS Lavu to begin intialization.";
						break;

					case 0:
					default:
							
						hideAllBut("enter_ip");
						break;	
				}
				
			}

		</script>

	</head>

	<body onload='checkInitialization();'>
	
		<div style='position:absolute; left:0px; top:0px; width:318px; height:460px; opacity:0.98;'>
				
			<img style='position:absolute; left:0px; top:0px; width:17px; height:16px;' src='images/popup_upper_left.png'>
			<img style='position:absolute; left:17px; top:0px; width:280px; height:16px;' src='images/popup_upper.png'>
			<img style='position:absolute; left:297px; top:0px; width:21px; height:16px;' src='images/popup_upper_right.png'>
			<img style='position:absolute; left:0px; top:16px; width:17px; height:412px;' src='images/popup_left.png'>
			<img style='position:absolute; left:297px; top:16px; width:21px; height:412px;' src='images/popup_right.png'>
			<img style='position:absolute; left:0px; top:428px; width:17px; height:22px;' src='images/popup_lower_left.png'>
			<img style='position:absolute; left:17px; top:428px; width:280px; height:22px;' src='images/popup_lower.png'>
			<img style='position:absolute; left:297px; top:428px; width:21px; height:22px;' src='images/popup_lower_right.png'>				

			<div style='position:absolute; left:17px; top:16px; width:280px; height:412px; background-color:#F9F9F9;'>
			
				<div id='enter_ip' class='inner_div2'>
				
					<table cellspacing='1' cellpadding='3' width='280px'>
						<tr><td class='msg_txt1' align='center'>Please enter your terminal's<br>IP address...</td></tr>
					</table>
				
					<div style='position:absolute; left:0px; top:45px; width:280px; height:40px;'>
						<center><input id='ip_input' class='input_long' type='tel' value='' READONLY></center>
					</div>

					<div id='number_pad' style='position:absolute; left:1px; top:95px; width:278px; height:250px;'>
						<center>
							<table cellspacing='2' cellpadding='0'>
								<tr>
									<td class='btn_number_pad' ontouchstart='enterIPButtonPressed("7"); return false;'>7</td>
									<td class='btn_number_pad' ontouchstart='enterIPButtonPressed("8"); return false;'>8</td>
									<td class='btn_number_pad' ontouchstart='enterIPButtonPressed("9"); return false;'>9</td>
								</tr>
								<tr>
									<td class='btn_number_pad' ontouchstart='enterIPButtonPressed("4"); return false;'>4</td>
									<td class='btn_number_pad' ontouchstart='enterIPButtonPressed("5"); return false;'>5</td>
									<td class='btn_number_pad' ontouchstart='enterIPButtonPressed("6"); return false;'>6</td>
								</tr>
								<tr>
									<td class='btn_number_pad' ontouchstart='enterIPButtonPressed("1"); return false;'>1</td>
									<td class='btn_number_pad' ontouchstart='enterIPButtonPressed("2"); return false;'>2</td>
									<td class='btn_number_pad' ontouchstart='enterIPButtonPressed("3"); return false;'>3</td>
								</tr>
								<tr>
									<td class='btn_number_pad' ontouchstart='enterIPButtonPressed("back"); return false;'><img style='padding:3px 0px 0px 0px;' src='images/btn_cc_back.png'></td>
									<td class='btn_number_pad' ontouchstart='enterIPButtonPressed("0"); return false;'>0</td>
									<td class='btn_number_pad' ontouchstart='enterIPButtonPressed("."); return false;'>.</td>
								</tr>
								<tr>
									<td colspan='3' align='center' style='padding:6px 0px 0px 0px;'>
										<button class='btn_light' ontouchstart='IPhelp(1);'><b>Help</b></button> &nbsp;&nbsp; <button class='btn_light' ontouchstart='submitIP();'><b>Submit</b></button>
									</td>
								</tr>
							</table>
						</center>
					</div>
					
				</div>
				
				<div id='enter_terminal_id' class='inner_div2'>
				
					<table cellspacing='1' cellpadding='3' width='280px'>
						<tr><td class='msg_txt1' align='center'>Please enter your terminal's<br>Moneris Terminal ID...</td></tr>
					</table>
				
					<div style='position:absolute; left:0px; top:45px; width:280px; height:40px;'>
						<center><input id='terminal_id_input' class='input_long' type='tel' value='' READONLY></center>
					</div>

					<div id='number_pad' style='position:absolute; left:0px; top:95px; width:278px; height:250px;'>
						<center>
							<table cellspacing='2' cellpadding='0'>
								<tr>
									<td width='1px'></td>
									<td class='btn_number_pad' ontouchstart='terminalIDButtonPressed("7"); return false;'>7</td>
									<td class='btn_number_pad' ontouchstart='terminalIDButtonPressed("8"); return false;'>8</td>
									<td class='btn_number_pad' ontouchstart='terminalIDButtonPressed("9"); return false;'>9</td>
								</tr>
								<tr>
									<td width='1px'></td>
									<td class='btn_number_pad' ontouchstart='terminalIDButtonPressed("4"); return false;'>4</td>
									<td class='btn_number_pad' ontouchstart='terminalIDButtonPressed("5"); return false;'>5</td>
									<td class='btn_number_pad' ontouchstart='terminalIDButtonPressed("6"); return false;'>6</td>
								</tr>
								<tr>
									<td width='1px'></td>
									<td class='btn_number_pad' ontouchstart='terminalIDButtonPressed("1"); return false;'>1</td>
									<td class='btn_number_pad' ontouchstart='terminalIDButtonPressed("2"); return false;'>2</td>
									<td class='btn_number_pad' ontouchstart='terminalIDButtonPressed("3"); return false;'>3</td>
								</tr>
								<tr>
									<td colspan='4'>
										<table cellspacing='0' cellpadding='0'>
											<tr>
												<td width='1px'></td>
												<td class='btn_number_pad_wide' ontouchstart='terminalIDButtonPressed("back"); return false;' style='padding-right: 3px;'><img style='padding:3px 0px 0px 0px;' src='images/btn_cc_back.png'></td>
												<td class='btn_number_pad_wide' ontouchstart='terminalIDButtonPressed("0"); return false;'>0</td>
											</tr>
										</table>
									</td>
								</tr>
								<tr>
									<td colspan='4' align='center' style='padding:6px 0px 0px 0px;'>
										<button class='btn_light' ontouchstart='submitTerminalID();'><b>Submit</b></button>
									</td>
								</tr>
							</table>
						</center>
					</div>
					
				</div>
				
				<div id='ip_help' class='inner_div2'>
					<center>
						<table style='width:280px; height:285px;' cellpadding='4'>
							<tr><td id='ip_help_title' class='msg_txt2' align='center' valign='bottom' style='height:55px; padding: 6px 8px 12px 8px;'></td></tr>
							<tr><td id='ip_help_message' class='msg_txt1' align='left' valign='top' style='height:230px; padding-top:6px;'></td></tr>
						</table>
						<button id='ip_help_prev' class='btn_light' ontouchstart='previousIPhelp();'><b>&lt; Previous</b></button> &nbsp;&nbsp; <button id='ip_help_next' class='btn_light' ontouchstart='nextIPhelp();'><b>Next &gt;</b></button>
					</center>
				</div>

				<div id='transaction_panel' class='inner_div2'>
					<center>
						<table style='width:280px; height:367px;'>
							<tr><td id='tx_title' class='msg_txt2' align='center' valign='bottom' style='height:117px; padding: 6px 8px 12px 8px;'></td></tr>
							<tr><td id='tx_message' class='msg_txt1' align='center' valign='top' style='height:220px; padding-top:6px;'></td></tr>
							<tr><td id='terminal_id_label' class='msg_txt4' align='center' valign='bottom' style='height:30px; padding-bottom:3px;'></td></tr>
						</table>
					</center>
				</div>

				<div id='admin1' class='inner_div2'>
					<center>
						<table cellspacing='0' cellpadding='0'>
							<tr><td id='inner_admin1' align='center' valign='middle'></td></tr>
						</table>
					</center>
				</div>

				<div id='tip_entry_settings' class='inner_div2'>
					<center>
						<table style='width:280px; height:367px;'>
							<tr><td class='msg_txt2' align='center' valign='bottom' style='height:37px; padding: 6px 8px 8px 8px;'>Tip Entry Settings</td></tr>
							<tr><td class='msg_txt1' id='tip_entry_current_setting' align='center' valign='top' style='height:55px; padding-top:6px;'></td></tr>
							<tr><td class='msg_txt1' align='center' valign='top' style='height:45px; padding-top:6px;'><button class='btn_light_long' ontouchstart='setTipEntry("TF");'><b>Disabled</b></button></td></tr>
							<tr><td class='msg_txt1' align='center' valign='top' style='height:45px; padding-top:6px;'><button class='btn_light_long' ontouchstart='setTipEntry("TN");'><b>Debit Only</b></button></td></tr>
							<tr><td class='msg_txt1' align='center' valign='top' style='height:45px; padding-top:6px;'><button class='btn_light_long' ontouchstart='setTipEntry("TB");'><b>Debit and Credit</b></button></td></tr>
							<tr><td class='msg_txt1' align='center' valign='top' style='height:140px; padding-top:6px;'><button class='btn_light_long' ontouchstart='hideAllBut("admin1");'><b>Menu</b></button></td></tr>
						</table>
					</center>
				</div>
				
				<div id='approved_panel' class='inner_div2'>
					<center>
						<table style='width:280px; height:330px;'>
							<tr><td id='approved_message' class='msg_txt3' align='center' valign='middle' style='padding: 6px 8px 6px 8px;'>Approved!</td></tr>
						</table>
					</center>
				</div>

				<div id='response_panel' class='inner_div2'>
					<center>
						<table style='width:280px; height:367px;'>
							<tr>
								<td align='center' valign='bottom' style='height:137px; padding: 6px 8px 16px 8px;'>
									<span id='response_title' class='msg_txt2'></span><br><br><span id='response_text' class='msg_txt1'></span>
								</td>
							</tr>
							<tr id='response_panel_reconfigure'><td align='center' valign='middle' style='width:278px; height:50px'><button class='btn_light_long' ontouchstart='confirmReconfigure();'><b>Reconfigure</b></button></td></tr>
								<tr><td align='center' valign='top' style='height:230px; padding-top:6px;'><button class='btn_light' ontouchstart='initiateAction();'><b>Retry</b></button></td></tr>
					</table>
					</center>
				</div>

				<div id='confirm_panel' class='inner_div2'>
					<center>
						<table style='width:280px; height:367px;'>
							<tr>
								<td align='center' valign='bottom' style='height:137px; padding: 6px 8px 16px 8px;'>
									<span id='confirm_title' class='msg_txt2'></span><br><br><span id='confirm_text' class='msg_txt1'></span>
								</td>
							</tr>
							<tr><td align='center' valign='top' style='height:230px; padding-top:6px;'><button class='btn_light' ontouchstart='confirmYes();'><b>Yes</b></button>&nbsp;&nbsp;&nbsp;<button class='btn_light' ontouchstart='confirmNo();'><b>No</b></button></td></tr>
						</table>
					</center>
				</div>

				<div id='report_panel' class='inner_div2'>
					<center>
						<table cellspacing='0' cellpadding='0' style='width:280px; height:365px;'>
							<tr><td id='report_title_cell' align='center' style='height:30px; border-bottom:solid 2px #AAAAAA;'></td></tr>
							<tr><td style='height:287px;'><div id='inner_report_panel' style='height:287px; overflow:scroll'></div></td></tr>
							<tr>
								<td align='center' valign='middle' style='height:48px; border-top:solid 2px #AAAAAA;'>
									<table>
										<tr>
											<td id='admin_menu_button' style='display:none;'><button class='btn_light' ontouchstart='hideAllBut("admin1");'><b>Menu</b></button> &nbsp;</td>
											<td><button class='btn_light' ontouchstart='printReport();'><b>Print</b></button></td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
					</center>
				</div>
								
				<div id='activity_shade' class='inner_div1' style='display:block; background-color:#FFFFFF; opacity:0.95;'>
					<table style='width:280px; height:412px;'>
						<tr><td align='center' valign='bottom' style='height:181px; padding: 6px 8px 6px 8px;'><span id='activity_message' class='msg_txt1'>Initializing...</span></td></tr>
						<tr><td align='center' valign='top' style='height:231px; padding-top:6px;'><img src='images/moneris_activity.gif' width='32px' height='32px'/></td></tr>
					</table>
				</div>
				
				<img style='position:absolute; left:100px; top:5px; width:80px;' src='images/moneris_logo.gif'/>

				<div style='position:absolute; left:245px; top:3px; width:32px; height:32px;'>
					<div style='position:absolute; left:0px top:0px; width:32px; height:32px; background-image:url("images/btn_wow_32x32.png");' ontouchstart='close1(); return false;'>
						<img src='images/x_icon.png' width='32px' height='32px'>
					</div>
				</div>

			</div>
				
		</div>

	</body>

</html>