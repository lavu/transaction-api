<?php

	error_log("phoenix_forward: ".print_r($_REQUEST, true));

	function hexToString($hex) {
		
		$string = "";
    	for ($i=0; $i < (strlen($hex) - 1); $i+=2) {
        	$string .= chr(hexdec($hex[$i].$hex[($i + 1)]));
    	}
   
	  	return $string;
	}

	function hex_string($string) {
	
		$return_string = "";
	
		for ($c = 0; $c < strlen($string); $c++) {
			$return_string .= str_pad(dechex(ord($string[$c])), 2, "0", STR_PAD_LEFT);
		}
		
		return $return_string;
	}
	
	function logForDebugging($debug_log) {
		
		$iid = (isset($_REQUEST['iid']))?$_REQUEST['iid']:"";
		if (empty($iid)) {
			return;
		}
		
		$uuid = (isset($_REQUEST['UUID']))?$_REQUEST['UUID']:"";
		$data_name = (isset($_REQUEST['dn']))?$_REQUEST['dn']:"";
		
		$log_string = date("Y-m-d H:i:s", time())." - MONERIS";
		if (strlen($uuid) > 0) $log_string .= " - DEVICE ID: ".$uuid;
		$log_string .= "\n\n";
		$log_string .= implode("\n", $debug_log);
		$log_string .= "\n\n\n";
		
		$log_path = "/home/poslavu/logs/company/".$data_name."/gateway_debug";
		$log_filename = $iid;
		
		if (!is_dir($log_path)) mkdir($log_path, 0777, true);
	
		$log_file = fopen($log_path."/".$log_filename, "a");
		fputs($log_file, $log_string);
		fclose($log_file);
	}
	
	$debug = (isset($_REQUEST['debug']))?($_REQUEST['debug'] == "1"):false;

	$out_data = (isset($_REQUEST['data']))?hexToString($_REQUEST['data']):"";
	$out_data_parts = explode(chr(3), substr($out_data, 4));
	$out_data = $out_data_parts[0];
	if (empty($out_data) || strlen($out_data)<16) {
		if ($debug) {
			logForDebugging(array("Invalid data received from extension - ".hex_string($out_data)));
		}
		echo '{"status":"0","message":"No data to send"}';
		exit();
	}
	
	$debug_log = array();

	if ($debug) $debug_log[] = "out_data (hex): ".hex_string($out_data);

	$len = strlen($out_data);
	$host_header = pack("n", $len);
	$url = "tls://".((strstr(__FILE__, '/dev/components/'))?"ipgtus":"ipg1").".moneris.com";
	$port = 443;

	if ($debug) {
		$debug_log[] = "host_header (hex): ".hex_string($host_header);
		$debug_log[] = "fsockopen - url: ".$url." - port: ".$port;
	}

	$errno = 0;
	$errstr = "";
	$fp = fsockopen($url, $port, $errno, $errstr, 30);
	if (!$fp) {
		if ($debug) {
			$debug_log[] = "!fp: ".$errstr." (".$errno.") - ".print_r(error_get_last(), true);
			logForDebugging($debug_log);
		}
		echo '{"status":"0","message":"'.$errstr.' ('.$errno.')"}';
		exit();	
	}
	
	$bytes = fwrite($fp, $host_header.$out_data);
	$in_data = fread($fp, 1024);
    fclose($fp);

	if ($debug) {
		$debug_log[] = "bytes written: ".$bytes;	
		$debug_log[] = "in_data (hex): ".hex_string($in_data);
		if (empty($in_data)) {
			$debug_log[] = "read error: ".print_r(error_get_last(), true);
		}
	}
	
	$checksum = 0;
	$resp_data = substr($in_data, 2);
    foreach (str_split($resp_data, 1) as $c) {
		$checksum ^= ord($c);
	}
	
	if ($debug) {
		$debug_log[] = "forward back to ipp320: ".hex_string($resp_data);
		$debug_log[] = "checksum: ".$checksum;
		logForDebugging($debug_log);
	}
	
	$resp_json = '{"status":"1","data":"'.hex_string($resp_data).'","lrc":"'.$checksum.'"}';
	
	echo $resp_json;
	exit();
?>