<?php

session_start();

$dataname = $_REQUEST['dn'];
$order_id = $_REQUEST['order_id'];
$check = $_REQUEST['check'];
$check_total = (double) $_REQUEST['check_total'];
$check_balance = (double) $_REQUEST['remaining_amount'];
$pay_type_label = (stristr(strtolower($_REQUEST['pay_type']), 'points') !== false) ? 'Points' : 'App';

// TO DO If no SESSION'd original_id, make them re-select user (?)

?>
<html>
<style>

@font-face{
	font-family:DINreg;
	src:url(/components/order_addons/pepper/fonts/DINMedium.39d8205e.ttf)
}

@font-face{
	font-family:DINlight;
	src:url(/components/order_addons/pepper/fonts/DINLight.70d15931.ttf)
}

@font-face{
	font-family:DINbold;
	src:url(/components/order_addons/pepper/fonts/DINBold.75c6da4d.ttf)
}

body {
	width: 318px;
	height: 460px;
	font-family: DINlight;
	background-color: rgba(0, 0, 0, 0);
}

section {
	width: 300;
	height: 400;
	background-color: rgba(255, 255, 255, 0.9);
	 -webkit-border-radius: 10px;
	padding:0px;
}

h1 {
	position: absolute;
	top: 30px;
	left: 30px;
	width: 200px;
	font-family: DINlight;
	color: grey;
	text-transform: uppercase;
	font-size: 18px;
	padding: 0px;
	margin: 0px;
}

hr {
	width: 50px;
}

form {
	margin: 0 auto;
}

button {
	-webkit-border-radius: 0px;
	-webkit-appearance: button;
	border-top: 1px solid #cffab3;
	padding: 0 20px;
	border: 0 none;
	font-size: 12px;
	line-height: 20px;
}

.activeButton {
	color: white;
	background-color: #aecd37;
	border-top: 1px solid #cffab3;
	box-shadow: 0px 1px 3px #888;
}

.activeButtonBlue {
	color: white;
	background-color: #3a9bcc;
	border-top: 1px solid white;
	box-shadow: 0px 1px 3px #888;
}

.inactiveButton {
	color: #444;
	background-color: #ccc;
	box-shadow: 0px 1px 3px #888;
}

.info_group {
	position: absolute;
	left: 60px;
}
.info_label {
	color: grey;
	display: block;
	white-space: nowrap;
}

#closeButton {
	position: absolute;
	top: 60;
	right: 20;
	padding: 0;
	margin: 0;
	font-size: 25px;
	line-height: 30px;
	color: #666666;
	background-color: #fafafa;
	-webkit-border-radius: 2px;
	width: 30px;
	z-index: 20001;
	right: 5%;
	top: 20px;
	text-align: center;
}

#close_button:after {
	content: "x";
	color: gray;
	border: 1px solid #aaa;
	border-radius: 2px;
	width: 30px;
	height: 30px;
	line-height: 30px;
	position: fixed;
	z-index: 20001;
	right: 5%;
	top: 20px;
	text-align: center;
}

#payButton {
	position: absolute;
	top: 0px;
	left: 0px;
		width: 150px;
		height: 40px;
	display: inline-block;
	margin-top: 10px;

	font-size: 14px;
}

#changeAmountButton {
	position: absolute;
	top: 0px;
	left: 160px;
	width: 60px;
	height: 40px;
	padding: 0 10px;
	display: inline-block;
	margin-top: 10px;
	line-height: 10px;
}

#app_pay {
	display: none;
}

#payment_info {
	top: 80px;
}

#payment_buttons {
	top: 240px;
}

#after_payment {
	top: 280px;
}

#message_area {
	top: 340px;
}

#message {
	text-align: center;
	width: 200px;
}

#appBalance {
	position: absolute;
	top: 0px;
	left: -30px;
	width: 120px;
		padding-top: 10px;
	text-align: right;
		font-size: 40pt;
		line-height: 40pt;
		text-align: center;
		font-weight: normal;
		font-color: grey;
}

#appBalanceArrow {
	position: absolute;
	top: 25px;
	left: 100px;
	white-space: nowrap;
}

#newAppBalance {
		position: absolute;
		top: 0px;
	left: 125px;
	right: 245px;
		padding-top: 10px;
	text-align: right;
		font-size: 40pt;
		line-height: 40pt;
		text-align: center;
		font-weight: normal;
		color: grey;
}

#checkBalance {
		position: absolute;
		top: 80px;
		left: -30px;
	width: 120px;
		padding-top: 10px 0;
		text-align: right;
		font-size: 40pt;
		line-height: 40pt;
		text-align: center;
		font-weight: normal;
		font-color: grey;
}

#checkBalanceArrow {
	position: absolute;
	top: 95px;
	left: 100px;
	white-space: nowrap;
}

#newCheckBalance {
	position: absolute;
	top: 70px;
	left: 125px;
	width: 120px;
		padding-top: 10px;
	text-align: right;
		font-size: 40pt;
		line-height: 40pt;
		text-align: center;
		font-weight: normal;
		color: grey;
}


#checkBalanceLabel {
	position: absolute;
	top: 60px;
	left: -30px;
}

#newCheckBalanceLabel {
		position: absolute;
		top: 120px;
		left: -30px;
}

#appBalanceLabel {
	position: absolute;
	top: -10px;
	left: -30px;
}

#newAppBalanceLabel {
	position: absolute;
	top: 0px;
	left: 120px;
}

</style>
<body>

<section>

<button id="closeButton" ontouchstart="closeWebview()">X</button>

<h1>Loyalty <?= htmlspecialchars($pay_type_label) ?> Pay</h1>

<div class="info_group" id="payment_info">
	<div id="appBalanceLabel" class="info_label"><?= htmlspecialchars($pay_type_label) ?> Balance</div>
	<div id="appBalance"><img src="/cp/images/loading_transparent.gif"></div><div id="appBalanceArrow">-></div><div id="newAppBalance"></div>

	<div id="checkBalanceLabel" class="info_label">Amount Due</div>
	<div id="checkBalance"></div><div id="checkBalanceArrow">-></div><div id="newCheckBalance"></div>
</div>

<div class="info_group" id="payment_buttons">
	<button id="payButton" class="inactiveButton" ontouchstart="createPayment(userId, order_id, app_payment)" disabled>Pay</button>
	<button id="changeAmountButton" class="inactiveButton" ontouchstart="changePayAmount()">change amount</button>
</div>

<div class="info_group" id="message_area">
	<div id="message"></div>
</div>

</section>

<script src="/components/payment_extensions/webextjs.js"></script>
<script>

var dataname = "<?= htmlspecialchars($dataname) ?>";
var order_id = "<?= htmlspecialchars($order_id) ?>";
var check = "<?= htmlspecialchars($check) ?>";
var original_id = "";
var componentLayoutId = "";
var posUserLabel = "";
var userFullName = "";
var extraInfo = "";

var locationId = "";  // Ajax responder will populate this if blank
var awardId = "";
var userId = "";
var orderId = "";
var user = {};
var orderValues = {};

var payment = null;  // Pepper Payment
var pay_type = "<?= htmlspecialchars(strtolower($_REQUEST['pay_type'])) ?>";
var pay_type_denorm = "<?= htmlspecialchars(strtolower($pay_type_label)) ?>";
var payingWithPoints = (pay_type_denorm == "points");  // TO DO : change this to check by payment_types.payment_extension instead of pay_type label

var order_total = parseFloat("<?= htmlspecialchars($_REQUEST['amount']) ?>");
var check_total = parseFloat("<?= htmlspecialchars($_REQUEST['check_total']) ?>");
var check_balance = parseFloat("<?= htmlspecialchars($_REQUEST['remaining_amount']) ?>");
var app_balance = 0.00;
var app_payment = 0.00;
var new_app_balance = 0.00;
var new_check_balance = 0.00;
var active_currency_code = "USD";  // TO DO : get real value (from config_table?)

var company_id = "<?= rawurlencode($_REQUEST['company_id']) ?>";
var loc_id = "<?= rawurlencode( $_REQUEST['loc_id']) ?>";
var device_prefix = "<?= rawurlencode( $_REQUEST['device_prefix']) ?>";
var internal_id = "";
var decimal_places = <?= empty($location_info['disable_decimal']) ? 2 : rawurlencode($location_info['disable_decimal']) ?>;
var communication_mode = "";
var tx_info = {};
var smallest_money = (1 / Math.pow(10, decimal_places)).toFixed(decimal_places);
var register = "<?= rawurlencode( $_REQUEST['register']) ?>";

function displayMoney(amt) {

	return parseFloat(amt).toFixed(decimal_places);
}

function displayMoneyWithSymbol(amt) {

	var monetary_symbol = "<?php echo $location_info['monitary_symbol']; ?>";
	if (monetary_symbol == "") monetary_symbol = "$";
	var left_or_right = "<?php echo $location_info['left_or_right']; ?>";
	if (left_or_right == "") left_or_right = "left";

	var rtn_str = "";
	if (left_or_right == "left") rtn_str += monetary_symbol + " ";
	rtn_str += displayMoney(amt) + "";
	if (left_or_right == "right") rtn_str += " " + monetary_symbol;

	return rtn_str;
}


function closeWebview() {
	// If we paid anything, we'll have created the Pepper Order so we need to update the orders.original_id with the orderId, if it doesn't already have it (somehow)
	var set_info = "";
	if (orderId && extraInfo && extraInfo.loyaltyInfo && !extraInfo.loyaltyInfo.orderId) {
		extraInfo.loyaltyInfo.orderId = orderId;
		set_info = "&set_info="+ encodeURIComponent(userFullName) +"|o|"+ encodeURIComponent(userId) +"|o|"+ encodeURIComponent(JSON.stringify(extraInfo));
	}

	window.location = '_DO:cmd=close_overlay' + set_info;
}

function computeAmounts(override_app_payment) {
	// Don't let the user pay more than their app balance or what they owe on their check
	app_payment = Math.min(check_balance, app_balance);

	// Allow the user to specify their app_payment amount - unless it is more than they can actually pay
	if (override_app_payment != null) {
		app_payment = Math.min(app_payment, override_app_payment);
	}

	new_app_balance = app_balance - app_payment;
	new_check_balance = check_balance - app_payment;

	// Don't let values go negative
	new_app_balance = Math.max(new_app_balance, 0);
	new_check_balance = Math.max(new_check_balance, 0);

	changePayAmountOnButton(app_payment);
	document.getElementById("checkBalance").innerHTML = displayMoney(check_total);
	document.getElementById("appBalance").innerHTML = displayMoney(app_balance);
	document.getElementById("newAppBalance").innerHTML = displayMoney(new_app_balance);
	document.getElementById("newCheckBalance").innerHTML = displayMoney(new_check_balance);
}

function getPaymentForPointsPay(paymentAmount) {
	var payment = {};
	payment.totalPayment = paymentAmount;
	payment.message      = "App payment successful";
	payment.tipAmount    = "";
	payment._id          = "";  // Pepper Transactions aren't created for points anymore - but payment._id is used later

	return payment;
}

function getUserForBalance(userId) {
	var xhr = new XMLHttpRequest();
	xhr.onreadystatechange = function () {
		if (this.readyState == 4 && this.status == 200) {
			user = JSON.parse(this.responseText);
			app_balance = user.balance;
			computeAmounts(null);
			if (app_balance > 0.00) {
				// Disable pay button
				var payButton = document.getElementById("payButton");
				payButton.disabled = false;
				payButton.className = "activeButton";

				var changeAmountButton = document.getElementById("changeAmountButton");
				changeAmountButton.disabled = false;
			}
		}
	}
	xhr.onErrorListener = function(errorMsg) {
		console.error(errorMsg);
	}
	xhr.open("GET", "/components/order_addons/pepper/webview.php?action=user&dataname="+ encodeURIComponent(dataname) +"&locationId="+ encodeURIComponent(locationId) +"&userId="+ encodeURIComponent(userId), true);
	xhr.withCredentials = true;
	xhr.send();
}

function getUserRewardsForPoints(userId) {
	var xhr = new XMLHttpRequest();
	xhr.onreadystatechange = function () {
		if (this.readyState == 4 && this.status == 200) {
			var rewards = JSON.parse(this.responseText);

			for (var i = 0; i < rewards.length; i++) {
				var reward = rewards[i];

				// Set points from awards rather than deprecated user.points
				if (reward.template == 'PSEUDO_CURRENCY') {
					// Divide points by 100 to display points like currency
					app_balance = (reward.points.available / 100);
					awardId = reward._id;
					break;
				}
			}

			computeAmounts(null);
			if (app_balance > 0.00) {
				// Disable pay button
				var payButton = document.getElementById("payButton");
				payButton.disabled = false;
				payButton.className = "activeButton";

				var changeAmountButton = document.getElementById("changeAmountButton");
				changeAmountButton.disabled = false;
			}
		}
	}
	xhr.onErrorListener = function(errorMsg) {
		console.error(errorMsg);
	}
	xhr.open("GET", "/components/order_addons/pepper/webview.php?action=rewards&dataname="+ encodeURIComponent(dataname) +"&locationId="+ encodeURIComponent(locationId) +"&userId="+ encodeURIComponent(userId), true);  // TO DO : see if locationId is really required to pass here
	xhr.withCredentials = true;
	xhr.send();
}

function createPayment(userId, order_id, paymentAmount) {
	if (app_payment <= 0.00 || app_balance <= 0.00) {
		return false;
	}

	// We will have to create the Pepper Order to attach Transactions to it.
	// We don't have access to the tip or tax from this webview (and because
	// the order probably hasn't been committed to the db yet), so just set
	// tip and tax to 0.00, for now.  They will get updated when the order closes.
	var tip = "0.00";
	var tax = "0.00";
	var action = (payingWithPoints) ? "points_pay" : "app_pay";

	var xhr = new XMLHttpRequest();
	xhr.onreadystatechange = function () {
		if (this.readyState == 4 && this.status == 200) {
			var result = JSON.parse(this.responseText);
			if (result.success) {
				// In order to create the Pepper Payment, we would've had to create the Pepper Order so record the new orderId in the POS
				if (result.orderId) {
					orderId = result.orderId;
					updateOriginalIdInPos();  // does not seem to work on checkout screen
				}

				// Get newly created Pepper Payment and apply it to webview, POS (after a slight deplay to give previous _DO:cmd a chance to finish)
				payment = result.transactions.shift();

				// We will need to create a synthetic payment if they're paying with points
				if (!payment && payingWithPoints) {
					payment = getPaymentForPointsPay(paymentAmount);
				}

				window.setTimeout(function() { applyPayment(order_id, orderId, userId, payment) }, 700);
			}
			else {
				// Display error message
				document.getElementById("message").innerHTML = result.error;

				// TO DO : show close button or re-enable Pay button (so they can try again) (?)
			}
		}
	}
	xhr.onErrorListener = function(errorMsg) {
		console.error(errorMsg);
	}
	xhr.open("GET", "/components/order_addons/pepper/webview.php?action=pay&dataname="+ encodeURIComponent(dataname) +"&loc_id="+ encodeURIComponent(loc_id) +"&locationId="+ encodeURIComponent(locationId) +"&awardId="+ encodeURIComponent(awardId) +"&userId="+ encodeURIComponent(userId) +"&orderId="+ encodeURIComponent(orderId) +"&order_id="+ encodeURIComponent(order_id) +"&check="+ encodeURIComponent(check) +"&pay_type="+ encodeURIComponent(pay_type) +"&amount="+ encodeURIComponent(paymentAmount)  +"&total="+ encodeURIComponent(order_total) +"&tip="+ encodeURIComponent(tip) +"&tax="+ encodeURIComponent(tax), true);
	xhr.withCredentials = true;
	xhr.send();

	// Disable pay button
	var payButton = document.getElementById("payButton");
	payButton.disabled = true;
	payButton.className = "inactiveButton";

	var changeAmountButton = document.getElementById("changeAmountButton");
	changeAmountButton.disabled = false;

	// Show loading spinner
	var message = document.getElementById("message");
	var loadingSpinner = document.createElement("img");
	loadingSpinner.src = "/cp/images/loading_transparent.gif";
	message.innerHTML = "";
	message.appendChild(loadingSpinner);

	return true;
}

function applyPayment(order_id, orderId, userId, payment) {
	if (!payment) {
		console.error("No payment found on successful payment creation");  // TO DO : is this ok or should it be changed to console.log() (?)
		return;
	}

	// Update POS, webview with new payment
	applyPaymentToPos(order_id, orderId, userId, payment);
	applyPaymentToWebview(userId, payment);
}

function applyPaymentToPos(order_id, orderId, userId, payment) {
	var tx_info = {};
	tx_info.order_id = order_id;
	tx_info.amount = payment.totalPayment.toString();  // the app wants a string, otherwise it reports 0.00 in the UI
	tx_info.total_collected = payment.totalPayment.toString();  // the app wants a string, otherwise it reports 0.00 in the UI
	tx_info.tip_amount = payment.tipAmount.toString();  // the app wants a string, otherwise it reports 0.00 in the UI
	tx_info.change = displayMoney("0");
	tx_info.pay_type = "<?= $_REQUEST['pay_type'] ?>";
	tx_info.pay_type_id = "<?= $_REQUEST['pay_type_id'] ?>";
	tx_info.transtype = "Sale";
	tx_info.action = "Sale";
	tx_info.internal_id = newInternalID();
	tx_info.got_response = "1";
	tx_info.transaction_id = payment._id;
	tx_info.auth_code = orderId;  // active_invoice_id
	tx_info.record_no = userId;   // active_customer_id
	tx_info.ref_data = "";
	tx_info.process_data = "";
	tx_info.card_type = "";
	tx_info.first_four = "";
	tx_info.card_desc = "";
	//tx_info.currency_pptxid = active_currency_code;
	//tx_info.info = active_currency_code;
	//tx_info.reader = "mercurypaypal_code"; //"mercurypaypal_tabs"; // temporarily spoof the code type in order to allow refunds to be properly processed in 2.3.11-

	addTransaction(tx_info);
}

function applyPaymentToWebview(userId, payment) {

	if (payment.message == null) payment.message = "App payment successful";
	if (payment.totalPayment == null) payment.totalPayment = 0.00;

	// Update app balance on screen
	document.getElementById("payButton").disabled = true;

	// Show payment success|failure message
	document.getElementById("message").innerHTML = payment.message;

	// Update new balance
	new_app_balance = app_balance - payment.totalPayment;

	// TO DO : hide new amounts (?)

	// TO DO : update old amounts (?)
}

function updateOriginalIdInPos() {
	// Update POS value for original_id with orderId
	if (orderId && extraInfo && extraInfo.loyaltyInfo && !extraInfo.loyaltyInfo.orderId) {
		extraInfo.loyaltyInfo.orderId = orderId;
		original_id = componentLayoutId +"|o|"+ posUserLabel +"|o|"+ userFullName +"|o|"+ userId +"|o|"+ JSON.stringify(extraInfo);

		//window.location = "_DO:cmd=setOrderValue&key=original_id&value="+ encodeURIComponent(original_id);
		window.location = "_DO:cmd=setOrderValue&key=original_id&value="+ encodeURIComponent(original_id) +"&ioid="+ encodeURIComponent("<?= $_REQUEST['ioid'] ?>");
	}
}

function changePayAmount() {
	window.location = "_DO:cmd=number_pad&type=money&js=computeAmounts(parseFloat('ENTERED_NUMBER'))&max_digits=5&c_name="+ encodeURI("Lavu Loyalty (Pepper Powered)") +"&title="+ encodeURI("Change Amount to Pay");
	//window.location = "_DO:cmd=input_number&for_form=app_pay&for_field=amount&title="+ encodeURI("Change Amount to Pay");
}

function changePayAmountOnButton(new_amount) {
	document.getElementById("payButton").innerHTML = "Pay "+ displayMoney(new_amount);
}

// Define for app to communicate number pad values (in case it's somehow not set by /lib/webview_special_functions.php)
if (typeof window.setFieldValue === 'undefined') {
	window.setFieldValue = function (form_name, field_id, value) {
		if (field_id == "amount") {
			app_payment = parseFloat(value);
			computeAmounts(app_payment);
		}
		else {
			document.getElementById(field_id).value = value;
		}
	}
}

// Gets values for each POS order object field specified as input arguments
// Note: _DO cmd requires callback function in js parameter or the default handler ("gotOrderValue(values)") to be defined.  Callbacks must return 1.
function getOrderValuesFromPos() {
	if (arguments.length > 0) {
		// Convert arguments object to array (since it has length property but no other Array methods)
		var fields = Array.prototype.slice.call(arguments);

		window.location = "_DO:cmd=getOrderValue&fields="+ fields.join(",") +"&js=processOrderValues('[ORDER_VALUE]')";
	}
}

function processOrderValues(encodedOrderValuesJson) {
	var orderValuesJson = decodeURIComponent(encodedOrderValuesJson);
	orderValues = JSON.parse(orderValuesJson);

	original_id = orderValues.original_id.split("|o|");
	componentLayoutId = original_id[0];
	posUserLabel = original_id[1];
	userFullName = original_id[2];
	userId = original_id[3];
	extraInfo = original_id[4];
	extraInfo = (extraInfo) ? JSON.parse(extraInfo) : {loyaltyInfo:{orderId:""}};

	if (userId && payingWithPoints) {
		getUserRewardsForPoints(userId);
	}
	else if (userId && !payingWithPoints) {
		getUserForBalance(userId);
	}
	else {
		// Show no user selected error message
		document.getElementById("payment_info").style.display = "none";
		document.getElementById("payment_buttons").style.display = "none";
		document.getElementById("message").innerHTML = "Loyalty User must first be selected";
	}

	// POS requires this call to return 1 otherwise an error message is thrown
	return 1;
}

document.addEventListener("DOMContentLoaded", function() {
	getOrderValuesFromPos("order_id", "original_id");

	/*
	var onTouchEvent = "ontouchstart";  //onclick for local dev

	document.getElementById("closeButton").addEventListener(onTouchEvent, function() {
		closeWebview();
	}, false);

	document.getElementById("changeAmountButton").addEventListener(onTouchEvent, function() {
		changePayAmount();
	}, false);

	document.getElementById("payButton").addEventListener(onTouchEvent, function() {
	createPayment(userId, order_id, app_payment);
	}, false);*/
}, false);

</script>
</body>
</html>
