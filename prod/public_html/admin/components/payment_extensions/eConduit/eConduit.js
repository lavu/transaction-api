function checkTerminalId()
{
	window.location = "_DO:cmd=get_termset&keys=terminal_id&callback=currentTerminalID";
}

function currentTerminalID(keys, vals)
{
	var k = keys.split("|_|");
	var v = vals.split("|_|");

	var tid = "";
	for (var i = 0; i < k.length; i++)
	{
    	switch (k[i])
   		 {
       		 case "terminal_id": tid = fromHex(v[i]); break;
        	 default: break;
		 }
	}
	if (tid.length > 0)
	{
		// alert("subtype "+subtype);
	    terminal_id = tid;

		performNextTransactionStep();
	}
	else
	{
	    presentAvailableTerminals();
	}

	return 1;
}

function presentAvailableTerminals()
{
	var terminalButtons = "<div style='position:absolute; left:292px; top:3px; width:32px; height:32px;'>";
	terminalButtons += "<div style='position:absolute; left:0px top:0px; width:32px; height:32px; background-image:url(\"../images/btn_wow_32x32.png\");' ontouchstart='cancel(); return false;'>";
	terminalButtons += "<img src='../images/x_icon.png' width='32px' height='32px'></div></div>";

	terminalButtons += "<div style = 'position:relative; width:300px; height:100%; margin: 0 auto;'>";
	terminalButtons += "<table width='100%' height='100%'>";
	terminalButtons += "<tr><td class='msg_txt1' height='60px' align='center' valign='middle'>Please select terminal</td></tr>";
	terminalButtons += "<tr><td height='100%' align='center' valign='middle'>";
	terminalButtons += "<table cellspacing = '15' cellpadding = '0'>";

	var terminalLists = tryParseJSON(availableTerminalsJson);
	if (terminalLists)
	{
		for (t=0; t < terminalLists.length; t++)
		{
			var terminal = terminalLists[t];
			if (terminal.length >= 2)
			{
				var tID = terminal[0];
				var tName = terminal[1];
				var useClass = (tID == terminal_id)?"terminal_button_hl":"terminal_button";

				terminalButtons += "<tr> <td class = '"+useClass+"' ontouchstart='useTerminalID(\""+tID+"\"); return false;'>"+tName+"</td></tr>";
			}
		}
	}
	else
	{
		terminalButtons += "<tr><td align = 'center' valign = 'middle'>No Terminals available</td></tr>";
	}

	terminalButtons += "</table></td></tr></table></div>";
	var mainContentDiv = document.getElementById('main_content_div');
	mainContentDiv.innerHTML = terminalButtons;
}

function useTerminalID(tID)
{
    terminal_id = tID;
    window.location = "_DO:cmd=set_termset&keys=terminal_id&vals=" + toHex(tID) + "&callback=didSetTerminalID";
}

function performNextTransactionStep()
{
	if (subtype == "Admin")
	{
		displayAdminMenu();
	}
	// LP-2404 - LP-3759 - not ready
	//else if (subtype=="PreAuthCaptureForTab" && previous_card_token.length>0)
	//{
	//	performRunTransaction2ForAuth2();
	//}
	else
	{
		loadTransactioniFrame();
	}
}

function didSetTerminalID(keys, vals)
{
	performNextTransactionStep();

	return 1;
}

function displayAdminMenu()
{
	presentAvailableTerminals();
}

function performRunTransaction2ForAuth2()
{
	// prepare view to show activity indicator
	
	var request = {};
	request.command			= "auth";
	request.key				= api_key;
	request.password		= api_password;
	request.amount			= base_amount;
	request.refID			= auth2_ref_id;
	request.terminalId		= terminal_id;
	request.invoiceNumber	= invoice_number;
	request.merchantId		= "";
	request.token			= previous_card_token;
	request.expDate			= orig_exp;
	
	/*arr = [];
	for (var k in request)
	{
		arr.push(k + "=" + encodeURIComponent(request[k]));
	}

	params = arr.join("&");

	alert("params: " + params);*/

	runTransactionTwo(request);
}

(function(){
	function runTransactionTwo(params)
	{
		AJAXRequest.call( this, 'runTransaction2.php', 'POST', params, true, "" );
	}

	function success( response ){
		//handleRT2Response(response);
	}

	function failure( status, response ){
		//handleRT2Response("");
	}

	window.runTransactionTwo = runTransactionTwo;
	runTransactionTwo.prototype.constructor = runTransactionTwo;
	runTransactionTwo.prototype = Object.create(AJAXRequest.prototype);
	//runTransactionTwo.prototype.success = success;
	//runTransactionTwo.prototype.failure = failure;
})();

function handleRT2Response(response)
{
	window.timeout(function(){alert("response: " + response)},100);
}
