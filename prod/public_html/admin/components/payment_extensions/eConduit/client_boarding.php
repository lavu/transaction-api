<!DOCTYPE html>
<html>
<head>
	<style type='text/css'>
		body
		{
			background-color: transparent;
			margin: 0 0 0 0;
			padding: 0 0 0 0;
		}
	</style>
</head>
<?php

    session_start();
    ini_set("display_errors","1");
    if (!empty($_SERVER['DOCUMENT_ROOT'])) {
        require_once( $_SERVER['DOCUMENT_ROOT'] . "/cp/resources/core_functions.php");
    }
    else{
        header('Location: /cp/index.php');
    }

    $locationid = sessvar('locationid');

    $admin_database = sessvar('admin_database');
    $data_name = sessvar('admin_dataname');

    $admin_companyid = sessvar('admin_companyid');
    $admin_company_name = sessvar('admin_company_name');
    $location_info[] = sessvar('location_info');


    /*if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest' 
    && $_SERVER['REQUEST_METHOD'] == 'POST' ) {*/


    if ( !empty($_SERVER['REQUEST_METHOD'])  && !empty($_POST) && $_SERVER['REQUEST_METHOD'] === 'POST') {

        $_terminalID    = $_POST['TerminalID'];
        $_apiKey        = $_POST['ApiKey'];
        $_apiPassword   = $_POST['ApiPassword'];

        $integration_extract = array();

        $enableIntegrateCardProcessing = '';
        if ($_apiKey && $_apiPassword) {
            // We get eConduit API Key and password after onBoarding success then enable Integrate credit card processing
            // and select eConduit gateway
            $enableIntegrateCardProcessing = ', `integrateCC` = "1", `gateway` = "eConduit"';
        }

        $updateQuery = "UPDATE `locations` 
            SET `integration9`= AES_ENCRYPT('$_apiKey','" . integration_keystr() ."'),
                `integration10`= AES_ENCRYPT('$_apiPassword','" . integration_keystr() ."') " . $enableIntegrateCardProcessing . "
                 where `id`=".$locationid;
        lavu_query($updateQuery);

        $terminal_name="Terminal-".$_terminalID;

        lavu_query("INSERT INTO `location_terminals` ( `location_id`,`terminal_name`, `terminal_id` ) VALUES ( '[1]', '[2]', '[3]')", $locationid, $terminal_name, $_terminalID);

        exit();
    }

    $client_boarding_url = "https://econduitapp.com/setup/activateTerminal.aspx?";
    $client_data = array();

    // Get Client Boarding Data from config table
    $sql = "SELECT setting, value
                FROM  `config` 
                WHERE  `type` =  'location_config_setting'
                AND  `setting` LIKE  'business_%'
                AND location ='[1]' "  ;

	$config_query = lavu_query($sql,$admin_companyid);


while($config_read = mysqli_fetch_assoc($config_query)){
       $client_data[$config_read['setting']] = $config_read['value'];
    }

    $client_data1 = array(
        'BusinessName' => $admin_company_name,
        'ContactName' => $client_data['business_primary_contact'],
        'Address' => $client_data['business_address'],
        'City' => $client_data['business_city'],
        'State' => $client_data['business_state'],
        'ZipCode' => $client_data['business_zip'],
        'Email' => $client_data['business_email'],
        'Phone' => $client_data['business_phone'],
        'ResellerName' => $client_data['business_reseller_name'],
        'ReferenceID' =>$client_data['business_reseller_id'],
        //'ReferenceID' => '12345678',
        //'ResellerName' => 'LavuPOS',
);

    $query_string = http_build_query($client_data1, '', '&amp;');
    $client_boarding_url .= $query_string ;
?>

<div id="icloseframe" style='position: absolute;left: 90.5%;top: 2.5%;width: 32px;height: 32px;z-index: 999;float: right;'>
<div style='position: absolute;left: 0px top:0px;width: 32px;height: 32px;background-image: url("../images/btn_wow_32x32.png");top: 21px;float: right;' ontouchstart='removeIFrame(); return false;'>
<img src='../images/x_icon.png' width='32px' height='32px'></div></div>

<iframe frameborder="0"  style="overflow:hidden; position: absolute; top: -30px;" scrolling="no" id="_client_boarding_iframe" width="580" height="660" src="<?php echo $client_boarding_url;?>"></iframe>

<script>

var LocationID = "<?php echo $locationid; ?>";
var CompanyID  = "<?php echo $admin_companyid; ?>";

function removeIFrame() {
    window.location = '_DO:cmd=close_overlay';
}

function listener(event) {
    var jsonResponse = JSON.parse(event.data);
    window.setTimeout(function() {
        addBoardingResponse(jsonResponse)
    }, 100);
}

if (window.addEventListener) {
    addEventListener("message", listener, false)
} else {
    attachEvent("onmessage", listener)
}

function addBoardingResponse(jsonResponse) {

    var TerminalID = jsonResponse['TerminalID'];
    var ApiKey = jsonResponse['ApiKey'];
    var ApiPassword = jsonResponse['ApiPassword'];

    var http = new XMLHttpRequest();
    var params = "TerminalID=" + TerminalID + "&ApiKey=" + ApiKey + "&ApiPassword=" + ApiPassword;
    http.open("POST", document.URL, true);
    //Send the proper header information along with the request
    http.setRequestHeader("Content-type", "application/x-www-form-urlencoded");

    http.onreadystatechange = function() { //Call a function when the state changes.
        if (http.readyState == 4 && http.status == 200) {
          // console.log(http.responseText); 
        }
    }
    http.send(params);
}
</script>
</html>