<?php
require_once(dirname(__FILE__)."/../../comconnect.php");
ini_set('display_errors', 1);

$subtype = $_REQUEST['subtype'];

$availableTerminalsJson = str_replace("'", "\'",loadAvailableTerminals());

$integrationData = getIntegrationData($_REQUEST['loc_id']);
$eConduitAPIKey = $integrationData['data9'];
$eConduitAPIPassword = $integrationData['data10'];

createMainContentDiv();
loadJavascriptVariables();
requireExtJSFunctions();
handleTransactionLoadIFrame($subtype);

function loadAvailableTerminals()
{
	//Check Config for onBoarded terminals
	$getTerminalList = lavu_query("SELECT `terminal_name`, `terminal_id` from `location_terminals` WHERE `location_id` = '[1]' ORDER BY `terminal_name` ASC", $_REQUEST['loc_id']);
	if (!$getTerminalList  || mysqli_num_rows($getTerminalList) == 0)
	{
		return null;
	}
	$terminal_list = array();
	while ($terminal = mysqli_fetch_assoc($getTerminalList))
	{
		if ($terminal['terminal_name'] != "" && $terminal['terminal_id'] != "")
		{
			$terminal_list[] = array($terminal['terminal_id'], $terminal['terminal_name']);
		}

	}
	return json_encode($terminal_list);
}

function createMainContentDiv(){
	echo "<html>
  			<head>
  			   <script src= 'eConduit.js'> </script>
		       <style type='text/css'>
				body{
		               font-family:sans-serif;
		               background-color: transparent;
		               margin: 0 0 0 0;
		               padding: 0 0 0 0;
		               -webkit-tap-highlight-color:rgba(0, 0, 0, 0);
		               -webkit-user-select: none;

					}
				body:after {
        	        content:
            	        		url('../images/btn_wow_32x32.png')
                	   		    url('../images/btn_wow_231x67.png')
                	   		    url('../images/btn_wow_231x67_hl.png')
                    			url('../images/x_icon.png');
                    			display:none;
            			   }
            	.terminal_button {
					color:#7D7D7D;
					font-family:Arial, Helvetica, sans-serif;
					font-size:15px;
					font-weight:bold;
					text-align:center;
					width:231px;
					height:67px;
					background-image:url('../images/btn_wow_231x67.png');
					background-repeat:no-repeat;
					background-position:center;
					border:hidden;

				}
				.msg_txt1 {
	                color:#888888 ;
	                font-family:Arial, Helvetica, sans-serif;
	                font-size:17px;
	            }
	            .terminal_button_hl {
					color:#8c9c62;
					font-family:Arial, Helvetica, sans-serif;
					font-size:15px;
					font-weight:bold;
					text-align:center;
					width:231px;
					height:67px;
					background-image:url('../images/btn_wow_231x67_hl.png');
					background-repeat:no-repeat;
					background-position:center;
					border:hidden;
				}

			</style>
	</head>
	<body onload = 'checkTerminalId();'>
		<div id='main_content_div' style='width:100%; height:100%; padding:0px; margin:0px; background-color:#ffffff; margin:0px auto;'></div>
	</body>
	</html>";
}

function loadJavascriptVariables()
{
	global $availableTerminalsJson;
	global $subtype;

    // LP-2404 - For secondary auth from token triggered for PreAuthCaptureForTab
	global $eConduitAPIKey;
	global $eConduitAPIPassword;

	$previous_card_token	= reqvar("orig_record_no", "");
	$orig_exp				= reqvar("orig_exp", "");
	$auth2_ref_id			= eConduitInternalID();
	$invoice_number			= str_replace("-", "", reqvar("order_id", ""));

	// LP-3759 - As of Lavu POS 3.8.3, signature and tip behavior is determined in native iOS
	$app_version = reqvar("app_version", "");
	$skip_signature_tip_designation = (appVersionCompareValue($app_version) >= 30803)?"true":"false";

	$javascriptVars = <<<JSVARS_HDOC
		<script>
			// Location Info
			var company_id						= '{$_REQUEST['company_id']}';
			var loc_id							= '{$_REQUEST['loc_id']}';
			var device_prefix					= '{$_REQUEST['device_prefix']}';
			var terminal_id						= "";
			var availableTerminalsJson			= '{$availableTerminalsJson}';
			var skip_signature_tip_designation	= {$skip_signature_tip_designation};
			var subtype							= '{$subtype}';

			// For secondary auth from token triggered for PreAuthCaptureForTab
			var api_key				= '{$eConduitAPIKey}';
			var api_password		= '{$eConduitAPIPassword}';
			var previous_card_token	= 'testing123'; //{$previous_card_token}';
			var orig_exp			= '{$orig_exp}';
			var auth2_ref_id		= '{$auth2_ref_id}';
			var invoice_number		= '{$invoice_number}';
			// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
	</script>
JSVARS_HDOC;

	echo $javascriptVars;
}

function handleTransactionLoadIFrame($subtype)
{
	$pay_type = "Card";
	$pay_type_id = "2";

	$req_pay_type = reqvar("pay_type", "");
	$req_pay_type_id = reqvar("pay_type_id", "");

	$process_data = "payment_extension:82";

	global $eConduitAPIKey, $eConduitAPIPassword;
	//error_log("inside handleTransactionTranstype with subtype:".$subtype);

	$decimal_places = locationSetting("disable_decimal", "2");

	// talked to econduit about requirement to remove decimal
	// they said their system can handle decimals
	$amount = (float)reqvar("amount", 0);
	$tip_amount = (float)reqvar("tip_amount", 0);

	$action = "";
	$command = "";
	$transtype = "";
    $refId = reqvar("orig_transaction_id", "");
    $eConduitRefId = reqvar("transaction_id", "");

	switch ($subtype)
	{
		case "Sale":

			$action		= "Sale";
			$command	= "sale";
			$transtype	= "Sale";
			//$refId		= eConduitInternalID();
			$refId		= $eConduitRefId;
			break;

		case "Adjustment":

			$command	= "tipadjust";
			$transtype	= "Adjustment";

			break;

		case "AuthForTab":

			$action		= "Sale";
			$command	= "auth";
			$transtype	= "AuthForTab";
			$refId		= eConduitInternalID();
			break;

		case "PreAuthCaptureForTab":

			$action		= "Sale";
			$command	= "capture";
			$transtype	= "PreAuthCaptureForTab";

			break;

		case "Void":

			$action		= "Void";
			$command	= "void";
			$transtype	= "Void";
			break;

		case "Refund":

			$action		= "Refund";
			$command	= "refund";
			$transtype	= "Return";
			$tip_amount = (float)reqvar("refund_tip", 0);

			break;

		case "GiftBalance":

			$command = "GiftBalance";
			break;

		case "GiftSale":

			$action			= "Sale";
			$command		= "GiftSale";
			$transtype		= "GiftCardSale";
			$pay_type		= $req_pay_type;
			$pay_type_id	= $req_pay_type_id;
			$process_data	= "gift_extension:82";
			$refId			= eConduitInternalID();
			break;

		case "GiftVoid":

			$action			= "Void";
			$command		= "GiftVoid";
			$transtype		= "GiftCardVoid";
			$pay_type		= $req_pay_type;
			$pay_type_id	= $req_pay_type_id;
			$process_data	= "gift_extension:82";
			break;

		case "GiftRefund":

			$action			= "Refund";
			$command		= "GiftRefund";
			$transtype		= "GiftCardReturn";
			$pay_type		= $req_pay_type;
			$pay_type_id	= $req_pay_type_id;
			$process_data	= "gift_extension:82";
			break;

		case "IssueGiftCard":

			$action			= "IssueGiftCard";
			$command		= "GiftActivate";
			$transtype		= "IssueGiftCard";
			$pay_type		= $req_pay_type;
			$pay_type_id	= $req_pay_type_id;
			$process_data	= "gift_extension:82";
			$refId			= eConduitInternalID();
			break;

		case "ReloadGiftCard":

			$action			= "ReloadGiftCard";
			$command		= "GiftAddValue";
			$transtype		= "ReloadGiftCard";
			$pay_type		= $req_pay_type;
			$pay_type_id	= $req_pay_type_id;
			$process_data	= "gift_extension:82";
			$refId			= eConduitInternalID();
			break;

		case "GetSwipe":

			$command = "getswipe";
			break;

		default:
			break;
	}

	$iFrameURL  = "https://econduitapp.com/terminal.aspx?";
	$iFrameURL .= "cmd=".$command."&";
	$iFrameURL .= "APIKey=".$eConduitAPIKey."&";
	$iFrameURL .= "APIPassword=".$eConduitAPIPassword."&";
	if ($subtype == "GetSwipe")
	{
		$iFrameURL .= "&prompt=Please%20swipe%20card";
	}
	else
	{
		$iFrameURL .= "Amount=".number_format(($amount + $tip_amount), 2)."&";
		$iFrameURL .= "refid=".$refId;
	}
error_log("iFrameURL: ".$iFrameURL);
	$handleTransactionLoadIFrameStr = <<<ECONDUIT_VOID_HTML
	<script>

		var transtype	= "{$transtype}";
		var command		= "{$command}";
		var base_amount	= "{$amount}";
		var tip_amount	= "{$tip_amount}";
		var refId	= "{$refId}";
		var subtype = "{$subtype}";

		function listener(event)
		{
			var jsonResponse = JSON.parse(event.data);

			var ResultCode				= validStringFromObject(jsonResponse, "ResultCode");
			var Message					= validStringFromObject(jsonResponse, "Message");
			var Last4					= validStringFromObject(jsonResponse, "Last4");
			var GiftCardBalanceString	= validStringFromObject(jsonResponse, "GiftCardBalanceString");
			var ResultFinal             = validStringFromObject(jsonResponse, "ResultFinal");

			if (command == "getswipe")
			{
				var track1 = validStringFromObject(jsonResponse, "Track1");
				var track2 = validStringFromObject(jsonResponse, "Track2");

				swipe_result = {};
				swipe_result.result		= ResultCode;
				swipe_result.message	= Message;
				swipe_result.track1		= extractCardNumberFromTrack(track1, 1);
				swipe_result.track2		= extractCardNumberFromTrack(track2, 2);

				window.setTimeout(function(){gotSwipeResult(swipe_result)}, 500);
				return;
			}

			// Check for credit card errors/invalidities etc. report and handle.
			if (ResultCode != "Approved")
			{
				var title = encodeURIComponent(ResultCode);
				var message = encodeURIComponent(Message);
				window.location = "_DO:cmd=alert&title=" + title + "&refId=" + refId + "&result_code=" + ResultCode + "&result_final=" + ResultFinal + "&subtype=" + subtype + "&message=" + message + "_DO:cmd=close_overlay";
				return;
			}

			if ((command == "GiftBalance") || (command == "LoyaltyBalance"))
			{
				balance_info = {};
				balance_info.balance	= GiftCardBalanceString;
				balance_info.last_four	= Last4;
				balance_info.print_chit = "0";

				window.setTimeout(function(){gotCardBalance(balance_info)}, 1000);

				return;
			}

			var RefID		= validStringFromObject(jsonResponse, "RefID");
			var Amount		= validStringFromObject(jsonResponse, "Amount");
			var TipAmount	= "0";
			var NameOnCard	= validStringFromObject(jsonResponse, "Name");

			if ((command == "sale") || (command == "tipadjust") || (command == "capture"))
			{
				var f_amount = parseFloat(Amount);
				var f_base_amount = parseFloat(base_amount);
				if (f_amount > f_base_amount)
				{
					Amount = f_base_amount.toString()
					var f_tip_amount = (f_amount - f_base_amount);
					TipAmount = f_tip_amount.toFixed({$decimal_places});
				}
			}
			else if (command == "refund")
			{
				var f_amount = (parseFloat(Amount) - parseFloat(tip_amount));
				Amount = f_amount.toString();
				TipAmount = tip_amount;
			}

			transaction_result = {};
			transaction_result.ref_data			= terminal_id;
			transaction_result.process_data		= "{$process_data}";
			transaction_result.transaction_id 	= RefID;
			transaction_result.internal_id		= newInternalID();
			transaction_result.tip_amount		= TipAmount;

			if (transtype == "Adjustment")
			{
			}
			else if (transtype == "PreAuthCaptureForTab")
			{
				transaction_result.processed		= "1";
			}
			else
			{
				transaction_result.amount			= Amount;
				transaction_result.total_collected	= Amount;
				transaction_result.datetime			= getCurrentDateTime(false);
				transaction_result.card_desc		= Last4; // last 4 digits of card number
				transaction_result.auth_code		= validStringFromObject(jsonResponse, "AuthCode");
				transaction_result.preauth_id		= RefID;
				transaction_result.pay_type			= "{$pay_type}";
				transaction_result.pay_type_id		= "{$pay_type_id}";
				transaction_result.action			= "{$action}"; // Lavu specific (refund void etc. check dbs. giftcard also)
				transaction_result.transtype		= transtype; // Gateway specific or Void or Refund etc. Basically the command
				transaction_result.got_response		= "1";
				transaction_result.reader			= "eConduit";
				transaction_result.gateway			= "eConduit";
				transaction_result.for_deposit		= "{$_REQUEST['for_deposit']}";
				transaction_result.is_deposit		= "{$_REQUEST['is_deposit']}";
				if (!skip_signature_tip_designation)
				{
					transaction_result.take_signature	= "0";
					transaction_result.take_tip			= "0";
				}
				transaction_result.change			= "0";
				transaction_result.record_no		= validStringFromObject(jsonResponse, "CardToken");
				transaction_result.card_type		= validStringFromObject(jsonResponse, "CardType");
				transaction_result.exp				= validStringFromObject(jsonResponse, "ExpDate");

				transaction_result.temp_data	= JSON.stringify(jsonResponse);

				var info = "";
				if (NameOnCard.length > 0)
				{
					var nameArray = NameOnCard.split("/");
					if (nameArray.length > 1)
					{
						info = nameArray[1];
					}
					var lastName = nameArray[0];
					if (lastName.length > 0)
					{
						if (info.length > 0)
						{
							info += " ";
						}
					}
					info += lastName;
				}
				transaction_result.info = info;
			}

			if (transtype == "AuthForTab")
			{
				transaction_result.auth = "2";
			}

			if (transtype == "Void" || transtype == "Return" || transtype == "GiftCardVoid" || transtype == "GiftCardReturn")
			{
				transaction_result.orig_action		= "{$_REQUEST['orig_action']}";
				transaction_result.orig_internal_id	= "{$_REQUEST['orig_internal_id']}";
				transaction_result.server_id		= "{$_REQUEST['server_id']}";
				transaction_result.server_name		= "{$_REQUEST['server_name']}";
				if (transtype == "Void" || transtype == "GiftCardVoid")
				{
					transaction_result.void_notes	= "{$_REQUEST['void_notes']}";
				}
				if (transtype == "Return" || transtype == "GiftCardReturn")
				{
					transaction_result.refund_notes	= "{$_REQUEST['refund_notes']}";
				}
			}

			if (transtype == "GiftCardSale" || transtype == "IssueGiftCard" || transtype == "ReloadGiftCard")
			{
				transaction_result.more_info = GiftCardBalanceString;
			}

			switch (transtype)
			{
				case "Adjustment":

					window.setTimeout(function(){updateTransaction(transaction_result)}, 1000);
					break;

				case "PreAuthCaptureForTab":

					window.setTimeout(function(){transactionCaptured(transaction_result)}, 1000);
					break;

				default:

					window.setTimeout(function(){addTransaction(transaction_result)}, 1000);
					break;
			}
		}

		if (window.addEventListener)
		{
		  	addEventListener("message", listener, false);
		}
		else
		{
		  	attachEvent("onmessage", listener);
		}

		function loadTransactioniFrame()
		{
			var mainContentDiv = document.getElementById('main_content_div');
			var iFrameURL = "{$iFrameURL}" + "&terminalID=" + terminal_id;
			var iFrame = document.createElement('iframe');
			iFrame.style.display	= "div";
			iFrame.style.height		= "100%";
			iFrame.style.width		= "100%";
			iFrame.style.background	= "#ffffff";
			iFrame.frameborder		= "0";
			iFrame.style.margin		= "0 auto";
			iFrame.style.border		= "0 none";
			iFrame.src = iFrameURL;
			mainContentDiv.appendChild(iFrame);
		}
		
		function removeIFrame() { window.location = '_DO:cmd=close_overlay'; }

		function validStringFromObject(obj, key)
		{
			if (typeof obj !== "object")
			{
				return "";
			}

			if (typeof key !== "string")
			{
				return "";
			}

			var string = obj[key];

			if (typeof string === "undefined")
			{
				return "";
			}

			if (string == null)
			{
				return "";
			}

			return string.toString();
		}

		function extractCardNumberFromTrack(track_data, track_num)
		{
			var card_number = track_data;

			switch (track_num)
			{
				case 1:

					break;

				case 2:

					var cn_parts = card_number.split("=");
					card_number = cn_parts[0];

					card_number = card_number.replace(/[;\?]/g, "");

					break;

				default:
					break;
			}

			if (typeof card_number !== "string")
			{
				return "";
			}

			return card_number;
		}

	</script>
ECONDUIT_VOID_HTML;
	echo $handleTransactionLoadIFrameStr;
}


//Query methods and others
function getIntegrationData($loc_id){
	$integrationKeyStr = integration_keystr();
	$queryStr = "SELECT AES_DECRYPT(`integration9`,'[1]') as data9, ".
					   "AES_DECRYPT(`integration10`,'[1]') as data10 ".
					   "FROM `locations` WHERE `id` = '[2]'";
	$result = lavu_query($queryStr, $integrationKeyStr, $loc_id);
	if(!$result){ error_log('Mysql error in '.__FILE__.' -sirur- error:'.mysql_error()); };
	$row = mysqli_fetch_assoc($result);
	return $row;
}

function makeDebugDiv($msg){
	echo "<div style='background-color:#EEE;'> <pre>".$msg."</pre> </div>";
}

function requireExtJSFunctions(){
	echo "<script>";
	require_once(dirname(__FILE__).'/../webextjs.js');
	echo "</script>";
}

function eConduitInternalID() // limited to 12 characters
{
	global $company_read; // comconnect.php's answer to info_inc.php's company_info

	// LP-3371 - Cara/Rich - this wasn't random enough and was causing tip adjust issues (and maybe other issues)
	// Removing the company id characters and replacing with a timestamp that will hopefully be more unique
	//$cid = str_pad((string)substr($company_read['id'], -2), 2, "0", STR_PAD_LEFT);
	$timestamp = date("His");
	$randomString = str_pad((string)rand(1, 9999), 4, "0", STR_PAD_LEFT);

	return date("ymd").$timestamp.$randomString;
}

//$subtype: Sale,Void
/*
 _REQUEST:[Array\n(\n
 [YIG] => 504\n
 [order_id] => 1026-20\n
 [available_DOcmds] => load_url,set_scrollable,send_js,alert,confirm,close_overlay,printer_setup,go_to_browser,input_number,number_pad,set_loyalty,startListeningForSwipe,stopListeningForSwipe,startListeningForKEI,stopListeningForKEI,get_active_order_info,get_checks_info,set_mercury_loyalty,set_loyalty,setOrderValue,update_signature,startQRscan,cancelQRscan,addTransaction,showActivityShade,hideActivityShade,print,TCPSocket,set_termset,get_termset\n
 [pos_view] => order\n
 [dn] => demo_brian_d\n
 [UUID] => 9D6E48F1-A1CC-4379-B8F0-6CCB3DE02B94\n
 [usejcinc] => 8\n
 [app_name] => Lavu POS\n
 [type] => web_extension\n
 [version] => 3.0.2\n
 [server_id] => 1\n
 [MAC] => 02:00:00:00:00:00\n
 [cc] => demo_brian_d\n
 [check] => 1\n
 [device_time] => 2015-07-27 12:31:02\n
 [subtype] => Void\n
 [build] => 20150721A\n
 [checkout] => 0\n
 [ioid] => 4077126-3004-20150727122757\n
 [device_prefix] => 26\n
 [lavu_admin] => 0\n
 [is_ipad] => 1\n
 [app_version] => 3.0.2\n
 [company_id] => 4077\n
 [lavulite] => 0\n
 [server_name] => Brian Dennedy\n
 [loc_id] => 1\n
 [app_build] => 20150721A\n
 [register] => receipt\n
 [app] => Lavu POS\n
 [register_name] => receipt\n
 [void_tip] => 0.00\n
 [orig_internal_id] => 4077126-9373-20150727122822\n
 [amount] => 1.59\n
 [action] => Void\n
 [pay_type_id] => 2\n
 [pay_type] => Card\n
 [orig_authcode] => \n
 [remaining_amount] => \n
 [orig_transaction_id] => 4077126-9373-20150727122822\n
 [void_notes] => \n
 [for_deposit] => 0\n
 [orig_action] => Sale\n
 [is_deposit] => 0\n)\n]

 VOID RETURN
 "__type":"eConduit.Core.ApiManagment.TerminalResponse",
 "TerminalID":1346,
 "ResultCode":"Approved",
 "AuthCode":"",
 "TransType":"void",
 "Amount":1.92,
 "CardType":"Visa",
 "Last4":"6781",
 "Name":"",
 "CashBack":0,
 "Message":"Approved",
 "RefID":"4077126-6067-20150727135856",
 "receiptTerminal":null,
 "CardToken":"",
 "GiftCardBalance":0,
 "ProcessorExtraData1":null,
 "ProcessorExtraData2":null

 SALE RETURN
 "__type":"eConduit.Core.ApiManagment.TerminalResponse",
 "TerminalID":1346,
 "ResultCode":"Approved",
 "AuthCode":"999999",
 "TransType":"Sale",
 "Amount":1.92,
 "CardType":"MasterCard",
 "Last4" (or "Last 4"?):"6781",
 "Name":"TESTMPS",
 "CashBack":0,
 "Message":"Approved",
 "RefID":"test0003",
 "receiptTerminal":null,
 "CardToken":"aLqqER2tbTQdhywEtk9FynRMLgy1mPKXF6NSlaZmlciEgUQECIQHMBR",
 "GiftCardBalance":0,
 "ProcessorExtraData1":null,
 "ProcessorExtraData2":null

 web1.poslavu.com/components/payment_extensions/eConduit/eConduit.php?YIG=8042&app=Lavu+POS&register=receipt&register_name=receipt&dn=demo_brian_d&UUID=9A660F29-1B7B-4D0A-9DE7-E8E5708851B8&usejcinc=8&app_name=Lavu+POS&type=web_extension&version=3.0.2&server_id=1&MAC=02%3A00%3A00%3A00%3A00%3A00&cc=demo_brian_d_key_Xsdw6WtJvftOAL39P4XG%3A20150624A&check=1&device_time=2015-07-24+12%3A05%3A19&subtype=Sale&build=20150624A&checkout=1&ioid=4077122-5432-20150724104351&device_prefix=22&lavu_admin=0&is_ipad=1&company_id=4077&app_version=3.0.2&lavulite=0&server_name=Brian+Dennedy&loc_id=1&app_build=20150624A&order_id=1022-11&available_DOcmds=load_url%2Cset_scrollable%2Csend_js%2Calert%2Cconfirm%2Cclose_overlay%2Cprinter_setup%2Cgo_to_browser%2Cinput_number%2Cnumber_pad%2Cset_loyalty%2CstartListeningForSwipe%2CstopListeningForSwipe%2CstartListeningForKEI%2CstopListeningForKEI%2Cget_active_order_info%2Cget_checks_info%2Cset_mercury_loyalty%2Cset_loyalty%2CsetOrderValue%2Cupdate_signature%2CstartQRscan%2CcancelQRscan%2CaddTransaction%2CshowActivityShade%2ChideActivityShade%2Cprint%2CTCPSocket%2Cset_termset%2Cget_termset&pos_view=order&for_deposit=0&pay_type=eConduit&is_deposit=0&amount=1.92&remaining_amount=1.92&action=Sale&pay_type_id=10
 */
?>