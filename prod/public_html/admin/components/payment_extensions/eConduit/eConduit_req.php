<?php
require_once(__DIR__."/../../../lib/info_inc.php");

// get API key and password through an ajax call from eConduitTipAdjust.js
function getIntegrationData($loc_id)
{
	$integrationKeyStr = integration_keystr();
	$queryStr = "SELECT AES_DECRYPT(`integration9`,'[1]') as apiKey, ".
						"AES_DECRYPT(`integration10`,'[1]') as apiPassword ".
						"FROM `locations` WHERE `id` = '[2]'";
	$result = lavu_query($queryStr, $integrationKeyStr, $loc_id);
	if (!$result)
	{
		error_log('Mysql error in '.__FILE__.' -sirur- error:'.mysqli_error());
	}

	$row = mysqli_fetch_assoc($result);
	if (empty($row['apiKey'])||is_null($row['apiKey']))
	{
		$row['apiKey'] = "";
		$row['apiPassword'] = "";
	}

	return $row;
}

$loc_id		= reqvar("loc_id", "");
$mode		= reqvar("mode", "");
$command	= reqvar("command", "");

$lavu_query_should_log = TRUE;

switch ($mode)
{
	case "load_credentials":

		$integrationData = getIntegrationData($loc_id);
		// pass back the values
		$response = array(
			'credentials'	=> $integrationData,
			'mode'			=> $mode
		);

		echo json_encode($response);

		break;

	case "update_transaction_records":

		$update_for_capture = "";
		if ($command == "capture")
		{
			$update_for_capture = "`auth` = '0', `processed` = '1', ";

			$new_ref_id = reqvar("new_ref_id", "");
			if (!empty($new_ref_id))
			{
				$update_for_capture .= "`transaction_id` = '[3]', ";
			}
		}

		// values to save
		$currTipAmount = $_REQUEST['tipAmount'];
		$updateResult = lavu_query("UPDATE `cc_transactions` SET ".$update_for_capture."`tip_amount` = '[1]', `last_mod_ts` = '[2]' WHERE `id` = '[4]'", $currTipAmount, time(), $new_ref_id, $_REQUEST['rowId']);
		if (!$updateResult)
		{
			error_log("sql query error in ".__FILE__." query: update cc transactions. error:".mysqli_error());
			return;
		}
		// transactions were updated successfully
		// get cc_transaction tip amounts so determine totalTipAmount for this order
		$ioid = $_REQUEST['ioid'];

		// check voided column - if its 1, the whole transaction was voided
		$allTipAmountsForOrder = lavu_query("SELECT `tip_amount`, `action` FROM `cc_transactions` WHERE `ioid` = '[1]' AND `voided` = 0", $ioid);
		$totalTipAmount = 0.00;
		while ($currRow = mysqli_fetch_assoc($allTipAmountsForOrder))
		{
			$action = $currRow['action'];
			// if action == void, don't do anything
			if ($action == "Sale")
			{
				// add
				$totalTipAmount += floatval($currRow['tip_amount']);
			}
			else if ($action == "Refund")
			{
				// subtract
				$totalTipAmount -= floatval($currRow['tip_amount']);
			}
		}
		$totalTipAmount = number_format($totalTipAmount, 2);

		// orders
		lavu_query("UPDATE `orders` SET `card_gratuity` = '[1]', `last_modified` = '[2]', `last_mod_device` = 'MANAGE TIPS', `last_mod_ts` = '[3]', `pushed_ts` = '[3]' WHERE `ioid` = '[4]' AND `location_id` = '[5]'", $totalTipAmount, $_REQUEST['currentLocalTime'], time(), $_REQUEST['ioid'], $loc_id);

		$response = array(
			'mode'			=> $_REQUEST['mode'],
			'tip_amount'	=> $currTipAmount
		);

		echo json_encode($response);

		break;

	case "update_transaction_errors":
		// values to save
		$extraInfo = 'Time-'.time() . '|' . reqvar("message", "");
		$updateResult = lavu_query("UPDATE `cc_transactions` SET `extra_info` = concat(`extra_info`, '||', '[1]'), `last_mod_ts` = '[2]' WHERE `id` = '[3]'", $extraInfo, time(), $_REQUEST['rowId']);
		if (!$updateResult)
		{
			error_log("sql query error in ".__FILE__." query: update cc transactions. error:".mysqli_error());
			return;
		}
		$response = array(
			'mode'		=> $mode,
			'message'	=> $extraInfo
		);
		echo json_encode($response);
		break;

	default:

		break;
}