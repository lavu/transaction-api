var APICredentials = {};
var extraArgs = {};
var retryAjaxCall = 0;

function submitEconduitTipAdjustOrCapture()
{
	var overlayDiv = document.getElementById('eConduitOverlay');
	overlayDiv.style.display = "block";

	// find total with tip adjustment
	var adjustedTotal = Number(extraArgs.transaction_amount) + Number(extraArgs.tipAmount);
	var adjustedTotalSubmit = Number(adjustedTotal).toFixed(2);

	// load the iframe
	var iFrameURL = "https://econduitapp.com/terminal.aspx?";
	iFrameURL += "cmd=" + extraArgs.command + "&";
	iFrameURL += "APIKey=" + APICredentials.apiKey + "&";
	iFrameURL += "APIPassword=" + APICredentials.apiPwd + "&";
	iFrameURL += "Amount=" + adjustedTotalSubmit + "&";
	iFrameURL += "terminalID=" + extraArgs.terminalId + "&";
	iFrameURL += "refid=" + extraArgs.refId;

	loadTransactioniFrame(iFrameURL);
}

// general ajax request code - duplicated from other areas
(function()
{
	function ajaxRequest(params)
	{
		AJAXRequest.call(this, "/components/payment_extensions/eConduit/eConduit_req.php", "POST", params, true);
	}

	function success(response)
	{
		ajaxResponse(response);
	}

	function failure(status, response)
	{
		ajaxFailure(status, response);
	}

	window.ajaxRequest = ajaxRequest;
	ajaxRequest.prototype.constructor = ajaxRequest;
	ajaxRequest.prototype = Object.create(AJAXRequest.prototype);
	ajaxRequest.prototype.success = success;
	ajaxRequest.prototype.failure = failure;
})();

// create iFrame to run tip ajust process
function loadTransactioniFrame(iFrameURL)
{
	var overlayDiv = document.getElementById('eConduitOverlay');
	overlayDiv.innerHTML = "";

	var iFrame = document.createElement('iframe');
	iFrame.style.display		= "div";
	iFrame.style.height			= "100%";
	iFrame.style.width			= "100%";
	iFrame.style.background		= "#ffffff";
	iFrame.frameborder			= "0";
	iFrame.style.margin			= "0 auto";
	iFrame.style.border			= "0 none";
	iFrame.src = iFrameURL;
	overlayDiv.appendChild(iFrame);

	function listener(event)
	{
		// close overlay
		hideActivityShade();

		var jsonResponse	= JSON.parse(event.data);
		var resultCode		= validStringFromObject(jsonResponse, "ResultCode");
		var resultFinal		= jsonResponse["ResultFinal"];
		var resultMessage	= validStringFromObject(jsonResponse, "Message");
		var result			= encodeURIComponent(resultCode);
		var message			= encodeURIComponent(resultMessage);
		var newRefID		= validStringFromObject(jsonResponse, "RefID");

		// send new ajax request to update tip adjust
		if (resultCode == "Approved")
		{
			// transaction approved, save to db

			var postVars = {};
			postVars.mode				= "update_transaction_records";
			postVars.ioid				= extraArgs.order_ioid;
			postVars.rowId				= extraArgs.rowId;
			postVars.trasactionAmount	= extraArgs.transaction_amount;
			postVars.tipAmount			= extraArgs.tipAmount;
			postVars.loc_id				= extraArgs.activeLocationId;
			postVars.cc					= extraArgs.activeDataName;
			postVars.currentLocalTime	= extraArgs.currentLocalTime;
			postVars.command			= extraArgs.command;
			postVars.new_ref_id			= newRefID

			// to assist with future troubleshooting
			postVars.event_data = encodeURIComponent(event.data);

			new ajaxRequest(postVars);
		} else if (resultMessage != '' && resultCode != "Approved") {
			var postVars 				= {};
			postVars.mode				= "update_transaction_errors";
			postVars.cc					= extraArgs.activeDataName;
			postVars.rowId				= extraArgs.rowId;
			postVars.message			= 'ResultCode-' + resultCode +'|Message-' + resultMessage;
			postVars.event_data			= encodeURIComponent(event.data);
			new ajaxRequest(postVars);
			componentAlert("Error", resultMessage);
		}
		else
		{
			// there was an error
			componentAlert("Error", "The tip adjust did not go through. If you continue to have this issue, please contact Lavu Customer Support.");
		}
	}

	if (window.addEventListener)
	{
		addEventListener("message", listener, false);
	}
	else
	{
		attachEvent("onmessage", listener);
	}

}

// run ajax call to get account credentials
function loadEconduitTipAdjustOrCapture(sender, field_id, value, terminalId, refId, rowId, transactionAmount, ioid, currentLocalTime, activeLocationId, activeDataName, oldTipValue, command)
{
	// sender.value = end of day reports (cp)
	// value = manage tips (app)
	var tipValue = sender.value || value;
	tipValue = Number(tipValue).toFixed(2);

	// check that tip adjust value has actually changed before launching the iFrame
	if ((tipValue === oldTipValue) && (command == "tipadjust"))
	{
		return;
	}

	// set globals for use later
	// extraArgs.fieldId = field_id;
	extraArgs.command				= command;
	extraArgs.tipAmount				= tipValue;
	extraArgs.terminalId			= terminalId;
	extraArgs.refId					= refId;
	extraArgs.rowId					= rowId;
	extraArgs.transaction_amount	= transactionAmount;
	extraArgs.order_ioid			= ioid;
	extraArgs.currentLocalTime		= currentLocalTime;
	extraArgs.activeLocationId		= activeLocationId;
	extraArgs.activeDataName		= activeDataName;

	new ajaxRequest({ "mode": "load_credentials", "loc_id": activeLocationId, "cc": activeDataName });
}

function ajaxResponse(response)
{
	// ajax call should always come back with at least the mode
	var parsedResponse = tryParseJSON(response);
	var credentials = JSON.stringify(parsedResponse);
	// only set credentials if that's the call that came back
	if (parsedResponse.mode == "load_credentials")
	{
		var key = parsedResponse.credentials["apiKey"];
		var pwd = parsedResponse.credentials["apiPassword"];

		// retry at least 3 times if invalid response
		// check for both key and password
		if (!key || !pwd)
		{
			if (key!='' && pwd!='')
			{
				if (retryAjaxCall < 3)
				{
					// key or password not returned, try again
					loadEconduitCredentials();
					retryAjaxCall++;
				}
				else
				{
					// could not get credentials
					componentAlert("loading credentials", "Unable to get valid credentials");
				}
			}
		}
		else
		{
			// received valid data
			APICredentials.apiKey = key;
			APICredentials.apiPwd = pwd;
			// we have credentials so process iframe tip adjust
			submitEconduitTipAdjustOrCapture();
		}
	}

	// the tip adjust went through successfully
	if (parsedResponse.mode == "update_transaction_records")
	{
		if (parsedResponse.tip_amount)
		{
			// refresh
			refreshPage();
		}
	}
}

function ajaxFailure(status, response)
{
	componentAlert("failed connection", status);
}

// standard utility functions that live elsewhere also
function componentAlert(title, message)
{
	if (isEmbeddedWebView)
	{
		window.location = "_DO:cmd=alert&title=" + encodeURIComponent(title) + "&message=" + encodeURIComponent(message);
	}
	else
	{
		alert(title + " - " + message);
		refreshPage();
	}
}

function tryParseJSON(jsonString)
{
	try
	{
		var o = JSON.parse(jsonString);
		if (o && typeof o === "object" && o !== null)
		{
			return o;
		}
	}
	catch (e) { }

	return false;
};

// close the overlay window
function hideActivityShade()
{
	document.getElementById('eConduitOverlay').style.display = "none";
}

function validStringFromObject(obj, key)
{
	if (typeof obj !== "object")
	{
		return "";
	}

	if (typeof key !== "string")
	{
		return "";
	}

	var string = obj[key];

	if (typeof string === "undefined")
	{
		return "";
	}

	if (string == null)
	{
		return "";
	}

	return string.toString();
}