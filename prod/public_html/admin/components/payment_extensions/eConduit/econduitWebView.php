<?php
//validate DN & Key
require_once(dirname(__FILE__)."/../../comconnect.php");
ob_start();
//load webview_special_functions
require_once("../../../lib/webview_special_functions.php");
$webspefun = ob_get_contents();
ob_end_clean();
//Current Time
global $location_info;
$current_time = date("Y-m-d H:i:s", time());
$tz           = $location_info['timezone'];
if (!empty($tz)) {
    $current_time = localize_datetime(date("Y-m-d H:i:s"), $tz);
}
//ioid
$ioID = reqvar("ioid");
//Tip Amount
$tipAmount = reqvar("tipAmount");
//TerminalID
$terminalId = reqvar("terminalId");
//Refid
$refID = reqvar("refId");
//Rowid
$rowID = reqvar("rowId");
//TransactionAmount
$transAmt = reqvar("transAmt");
//ActiveLocationID
$locationId = reqvar("locId");
//DataName
$dataName = reqvar("dataName");
//Auth
$auth = reqvar("auth");
//Command
$command = ($auth == "0") ? "tipadjust" : "capture";
//oldtipValue
$oldTip = reqvar("oldTipAmount");
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
      <head>
            <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
            <title>Manage Tips</title>
            <style>
                  body { font-family:Verdana, Arial, Helvetica, sans-serif; font-size:13px; background-color: transparent; margin-left: 0px; margin-top: 0px; margin-right: 0px; margin-bottom: 0px; }
                  .cct_labels { color:#999999; font-size:11px; }
                  .cct_info { color:#666666; font-size:11px; }
                  .error_text { font-family:Verdana, Arial, Helvetica, sans-serif; font-size:16px; color:#CC0000; }
                  .info_text { font-family:Verdana, Arial, Helvetica, sans-serif; font-size:11px; }
                  .paging_text { font-family:Verdana, Arial, Helvetica, sans-serif; font-size:19px; padding:3px 15px 3px 15px; }
            .mt_panel_top { width:984px; height:30px; }
            .mt_panel_mid { width:984px; }
            .mt_panel_bottom { width:984px; height:30px; }
                  .small_text { font-family:Verdana, Arial, Helvetica, sans-serif; font-size:12px; color:#333333; }
                  .tbot { border-top:solid 2px #777777; text-align:right; font-weight:bold; }
                  .title_text { font-family:Verdana, Arial, Helvetica, sans-serif; font-size:17px; color:#000000; }
                  .ttop { border-bottom:solid 2px #777777; text-align:center; font-weight:bold; }
                  .btn_light_long {
                        color:#7D7D7D;
                        font-family:Arial, Helvetica, sans-serif;
                        font-size:13px;
                        width:200px;
                        height:37px;
                        background:url("images/btn_wow_200x37.png");
                        border:hidden;
                        padding-top:2px;
                  }
                  .iFrameOverlay {
                        display:none;
                        position:fixed;
                        top:0;
                        left:0;
                        width:100%;
                        height:100%;
                        background-color:#EFEFEF;
                        opacity:0.99;
                  }
            </style>
            <script src="../../../cp/scripts/ajax_prototype.js"></script>
            <script src="../../payment_extensions/eConduit/econduitNewTipAdjust.js"></script>
            <?php echo $webspefun; ?>

      </head>

      <body onload="loadTipAdjustorCaptureiFrame('<?php echo $tipAmount; ?>', '<?php echo $terminalId; ?>', '<?php echo $refID; ?>', '<?php echo $rowID; ?>', '<?php echo $transAmt; ?>', '<?php echo $ioID; ?>', '<?php echo $current_time; ?>', '<?php echo $locationId; ?>', '<?php echo $dataName; ?>', '<?php echo $oldTip; ?>', '<?php echo $command; ?>')">
                  <div id='eConduitNewTipUiIframe' class='iFrameOverlay'>
                        <button class='btn_light_long' ontouchstart='hideActivityShade();'>
                              <b>Close Overlay</b>
                        </button>
                  </div>
      </body>
</html>