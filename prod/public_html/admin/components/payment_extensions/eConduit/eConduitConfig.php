<?php
require_once(dirname(__FILE__)."/../../comconnect.php");
/*
 * Function to get eConduit Credentials.
 */
function geteConduitIntegration($loc_id){
	$integrationKeyStr = integration_keystr();
	$queryStr = "SELECT AES_DECRYPT(`integration9`,'[1]') as data9, ".
			"AES_DECRYPT(`integration10`,'[1]') as data10 ".
			"FROM `locations` WHERE `id` = '[2]'";
	$result = lavu_query($queryStr, $integrationKeyStr, $loc_id);
	if(!$result){
		error_log('Mysql error in '.__FILE__.' - error:'.mysql_error()); 
	}
	$row = mysqli_fetch_assoc($result);
	return $row;
}