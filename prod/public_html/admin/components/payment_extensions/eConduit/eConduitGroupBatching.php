<?php
require_once(dirname(__FILE__)."/../../comconnect.php");

echo $styles = <<<STYLES
<style>
  .tbl-qa {
   	width: 90%;
   	font-size: 1em;	 
   	border: solid 2px #aaaaaa;	   	
   }

  .tbl-qa th.table-header {
   	padding: 5px;
   	text-align: left;
   	padding: 10px;
   	color: white;
   	background : #98B624 ;
   }

   .tbl-qa td.table-row  {
   	padding: 10px;
   	background-color: #FDFDFD;
   	display: table-cell;
     border: #aaaaaa 1px solid;	   	   	
   }

   .ajax-action-button  img{	
   	color: #09F;
   	margin: 10px 0px;
   	cursor: pointer;
   	display: inline-block;
   	padding: 10px 20px;
   }

   .button {
	background: #98B624;
	border-radius: 0px;
	color: white;
   }
</style>
STYLES;

$admin_database = sessvar('admin_database');
$data_name = sessvar('admin_dataname');
$admin_companyid = sessvar('admin_companyid');
$admin_company_name = sessvar('admin_company_name');
$location_info[] = sessvar('location_info');
$location_id = '';
if (!empty($location_info)) {
$location_id = $location_info['id'];
}

if (empty($location_id)){
header('Location: /cp/index.php');
exit;
}


$_request_mode = $_REQUEST["mode"];
$_request_action = $_REQUEST["action"];
$_request_method = $_SERVER["REQUEST_METHOD"];
$pack_id = reqvar("pack_id", "");
$active_language_pack = getWebViewLanguagepack($pack_id, $location_info['id']);

function heredoc($param) {
    // return after language translation whatever has been passed to us
    return speak($param);
}
$heredoc = 'heredoc';

echo $htmlHead = <<<HTMLHEAD


        <div style="position:absolute; left:5px; top:3px; width:32px; height:32px;">
        <div style="position:absolute; left:0px top:0px; width:32px; height:32px; background-image:url('../images/btn_wow_32x32.png');"><a href="javascript:close();" ><img src="../images/x_icon.png" width="32px" height="32px"></a></div></div>

<br/>
<br/>
		
		

	<table class="tbl-qa">
		<thead>
			<tr>
				<th class="table-header">{$heredoc("Terminal Name")}</th>
				<th class="table-header">{$heredoc("Terminal ID")}</th>
				<th class="table-header">{$heredoc("Actions")}</th>
			</tr>
		</thead>
		<tbody id="table-body">	
HTMLHEAD;

		   if( !empty($_request_action)  &&  $_request_action == 'delete' && $_request_method === 'POST' ) {

		        $id = $_REQUEST["id"];    

		        $delete_query = "DELETE FROM  location_terminals  WHERE id = '[1]' ";	        
		        lavu_query($delete_query, $id);
		        echo  mysqli_affected_rows();
		    }
    

	
	$integrationKeyStr = integration_keystr();
	$queryStr = " SELECT AES_DECRYPT(`integration9`,'[1]') as data9, AES_DECRYPT(`integration10`,'[1]') as data10 FROM `locations` WHERE `id` = '[2]' and _disabled=0 ";
	$result = lavu_query($queryStr, $integrationKeyStr, $location_id);
	if(!$result){ error_log('Mysql error in '.__FILE__.' -sirur- error:'.mysql_error()); };
	$integration_data = mysqli_fetch_assoc($result);	
	
    $terminal_query = lavu_query("SELECT * FROM location_terminals  WHERE location_id = '[1]' ", $location_id);
    while($terminal_reads =  mysqli_fetch_assoc($terminal_query)){
        $arrTerminals[] = $terminal_reads;
    }

	$default_setdate = date("Y-m-d");
	$batch_date = (isset($_REQUEST['date'])) ? $_REQUEST['date'] : $default_setdate;
	$start_datetime = date('Y-m-d', strtotime ( '-7 days', strtotime($batch_date)  ) ) . " 00:00:00";
	$end_datetime = $batch_date. " 23:59:59";
	$terminalTransactions = array();
	$totalTransactionCount = 0;
	$order_query = lavu_query("SELECT `cc`.`ref_data`, count(*) as transactions FROM `orders` as `or` JOIN `cc_transactions` as `cc` on `or`.`order_id` = `cc`.`order_id` WHERE `or`.`order_id` NOT LIKE 'Paid%' AND `or`.`order_id` NOT LIKE '777%' AND `or`.`closed` >= '$start_datetime' AND `or`.`closed` <= '$end_datetime' AND `cc`.`gateway` = 'eConduit' AND `cc`.pay_type_id = 2 AND `cc`.auth = 1 AND `or`.`void` = '0'  AND `or`.`location_id` = '" . $location_id . "' GROUP BY `cc`.`ref_data`");
	while($transactionsDetails =  mysqli_fetch_assoc($order_query)){
		$terminalTransactions[$transactionsDetails['ref_data']] = $transactionsDetails['transactions'];
		$totalTransactionCount += $transactionsDetails['transactions'];
	}

	if(!empty($arrTerminals)) {
        foreach($arrTerminals as $k=>$v) { ?>
          <tr class="table-row" id="table-row-<?php echo $arrTerminals[$k]["id"]; ?>">
                <td style="width:50%"><?php echo $arrTerminals[$k]["terminal_name"]; ?></td>
                <td><?php echo $arrTerminals[$k]["terminal_id"]; ?></td>
				<td><a href="javascript:unpairTerminal('<?php echo $arrTerminals[$k]["terminal_id"]; ?>',<?php echo $arrTerminals[$k]["id"]; ?>);" ><?php echo  speak("Unpair");?></a>&nbsp;|
		<?php
			if (isset($terminalTransactions[$arrTerminals[$k]["terminal_id"]]) && $terminalTransactions[$arrTerminals[$k]["terminal_id"]] > 0) {?>
			&nbsp;<a href="#" style="cursor: pointer" onclick="window.open('/lib/batchSettlement.php?terminal_id=<?php echo $arrTerminals[$k]["terminal_id"];?>', '_parent');" ><?php echo speak("Batch");?></a></td>
		<?php } else { ?>
				&nbsp;<a href="javascript:performBatch('<?php echo $arrTerminals[$k]["terminal_id"]; ?>');" ><?php echo speak("Batch");?></a></td>
		<?php } ?>
          </tr>
<?php
      }
}
    else {
?>
        <tr class="table-row">
                <td colspan=3 ><?php echo speak("There are no Terminals.") ?></td>
        </tr>

<?php
        }
        echo $htmlFoot = <<<HTMLFOOT
                </tbody>
        </table>
HTMLFOOT;
if(!empty($arrTerminals)) {
	if ($totalTransactionCount > 0) {
?>
<div class="ajax-action-button" id="add-more" onclick="window.open('/lib/batchSettlement.php', '_parent');" >
		<a href="javascript:void(0);" class="addNewBtn"><?php echo speak("Group Batch") ?></a>
		</div>
<?php
	} else {
?>
	<div class="ajax-action-button" id="add-more" onclick="performGroupBatch();">
	<a href="javascript:void(0);" class="addNewBtn"><?php echo speak("Perform Group Batch") ?></a>
	</div>
<?php
	}
}
?>



<script type="text/javascript" src="/manage/js/jquery/js/jquery-1.9.0.js"></script>

<script language="javascript">

	function performBatch(terminalId){
		var key = '<?php echo $integration_data["data9"]; ?>';
		var password = '<?php echo $integration_data["data10"]; ?>';
		var targetURL = 'https://econduitapp.com/services/api.asmx/closeBatch?key='+key+'&password='+password+'&terminalId='+terminalId+'&refId=111&merchantId=123456';
		$.get( targetURL, function( response ) {
			if(response.MessageCredit){
				alert("Terminal ID: "+response.TerminalID+" - "+response.MessageCredit);
			}
		});
	}



	function unpairTerminal(terminalId,id)
	{
		
        var key = '<?php echo $integration_data["data9"]; ?>';
        var password = '<?php echo $integration_data["data10"]; ?>';
        if(confirm("Are you sure you want to unpair this terminal?")) {  
        	var targetURL = 'https://econduitapp.com/services/api.asmx/unPairTerminal?key='+key+'&password='+password+'&terminalId='+terminalId;
        	$.get( targetURL, function( response ) {
               
                if(response.Status=="Success"){
                    //-------------delete on UnPair-----------
                	$.ajax({
                    	url: document.URL,
                    	type: "POST",
                    	data:'id='+id +'&action=delete',
                    	success: function(data){
                    		location.reload(true);
                    	}
                	});
                    //-------------delete on UnPair-----------
      			}
                else
                {
                	alert("Failed to Unpair");
                }
             });
		}
	}


	function performGroupBatch(){
        var key = '<?php echo $integration_data["data9"]; ?>';
        var password = '<?php echo $integration_data["data10"]; ?>';
        var all_terminals=<?php echo json_encode($arrTerminals, JSON_PRETTY_PRINT); ?>;
        var message='';
        for (i = 0; i < all_terminals.length; i++)
        {
               var targetURL = 'https://econduitapp.com/services/api.asmx/closeBatch?key='+key+'&password='+password+'&terminalId='+all_terminals[i]["terminal_id"]+'&refId=111&merchantId=123456';
                $.ajax({
                        url:targetURL,
                        type: "GET",
                        success: function( response ) {
                        if(response.MessageCredit){
                                message=message.concat("Terminal ID: "+response.TerminalID+" - "+response.MessageCredit+"\n");
                         }},
                        async: false
                });
        }
        alert(message);
	}       

	function close()
	{
        window.location = "_DO:cmd=close_overlay";
	}



</script>