<?php
require_once(dirname(__FILE__)."/eConduitConfig.php");

function geteConduitAPIURL(){
	return "https://econduitapp.com/services/api.asmx";
}
$datetime = date("Y-m-d H:i:s");
$request = $_REQUEST;
function geteConduitRefID(){
	$timestamp = date("His");
	$randomString = str_pad((string)rand(1, 9999), 4, "0", STR_PAD_LEFT);
	return date("ymd").$timestamp.$randomString;
}

function refIDbyOrderId($orderId)
{
	$return_arr = array();
	$query = lavu_query("select * from `lip_order_transaction` where `order_id` = '$orderId' order by id desc limit 1");
	if(mysqli_num_rows($query)>0)
	{
		$result = mysqli_fetch_assoc($query);
		$return_arr = array("transaction_id" => $result['transaction_id'], "createdDate" => date('mdY',strtotime($result['created_date'])));
	}
	return $return_arr;
}

function doeConduitTransaction($terminal_id,$transactionCommand,$amount,$loc_id,$orderId,$refId=""){
	$datetime = date("Y-m-d H:i:s");
	$configVars = geteConduitIntegration($loc_id);
	$eConduitAPI = geteConduitAPIURL();
	$eConduitRefId = geteConduitRefID();
	$transParam = array();
	$transParam['key'] = $configVars['data9'];
	$transParam['password'] = $configVars['data10'];
	$transParam['terminalId'] = $terminal_id;
	$transactionURL = "";
	switch($transactionCommand){
		case "sale":
			$status = checkeConduitTransactionStatus($terminal_id,$orderId,$loc_id);
			$status_arr = json_decode($status,true);
			if(is_array($status_arr) && count($status_arr)>0)
			{
				if($status_arr['ResultCode'] == 'Approved')
				{
					return $status;
				}
				else
				{
					$amount = $status_arr['Amount'];
					$eConduitRefId = $status_arr['RefID'];
				}
			}
				
			$transactionURL = "/runTransaction?";
			$transParam['command'] = 'sale';
			$transParam['amount'] = $amount;
			$transParam['refID'] = $eConduitRefId;
			$transactionURL.=http_build_query($transParam);
			lavu_query("insert into lip_order_transaction (`order_id`,`check`,`transaction_id`,`created_date`) values('$orderId','1','".$eConduitRefId."','$datetime')");
		break;
		case "refund":
			$transactionURL = "/runTransaction?";
			$transParam['command'] = 'refund';
			$transParam['amount'] = $amount;
			$transParam['refID'] = $refId;
			$transactionURL.=http_build_query($transParam);
			break;
	}
	$endURL = $eConduitAPI.$transactionURL; 
	$curlCall = curl_init();
	curl_setopt($curlCall, CURLOPT_URL,            $endURL);
	curl_setopt($curlCall, CURLOPT_CONNECTTIMEOUT, 0);
	curl_setopt($curlCall, CURLOPT_TIMEOUT,        200);
	curl_setopt($curlCall, CURLOPT_RETURNTRANSFER, true );
	$result = curl_exec($curlCall);
	
	if ($result === FALSE) {
	    $error = array( "cURLError" => curl_error( $curlCall ) );
		$result = json_encode($error);
	}
	return $result;
}

function checkeConduitTransactionStatus($terminal_id,$orderId,$loc_id){
	$configVars = geteConduitIntegration($loc_id);
	$eConduitAPI = geteConduitAPIURL();
	$eConduitRefId = refIDbyOrderId($orderId);
	if(is_array($eConduitRefId) && count($eConduitRefId)>0)
	{
		$refId = $eConduitRefId['transaction_id'];
		$transDate = $eConduitRefId['createdDate'];
	}
	else {
		return 0;
	}
	$transParam = array();
	$transParam['key'] = $configVars['data9'];
	$transParam['password'] = $configVars['data10'];
	$transParam['terminalId'] = $terminal_id;
	$transactionURL = "/checkStatus?";
	$transParam['command'] = 'sale';
	$transParam['refID'] = $refId;
	$transParam['date'] = $transDate;
	$transactionURL.=http_build_query($transParam);
	$endURL = $eConduitAPI.$transactionURL;
	$curlCall = curl_init();
	curl_setopt($curlCall, CURLOPT_URL,            $endURL);
	curl_setopt($curlCall, CURLOPT_CONNECTTIMEOUT, 0);
	curl_setopt($curlCall, CURLOPT_TIMEOUT,        200);
	curl_setopt($curlCall, CURLOPT_RETURNTRANSFER, true );
	$result = curl_exec($curlCall);

	if ($result === FALSE) {
		$error = array( "cURLError" => curl_error( $curlCall ) );
		$result = json_encode($error);
	}
	return $result;
}

if (isset($request['transactionCommand']) && $request['transactionCommand']!="") {
    $ref_id = '';
    if ($request['transactionCommand']=='refund') {
        $ref_id = $request['transaction_id'];
    }
    $response = doeConduitTransaction($request['terminal_id'],$request['transactionCommand'],$request['card_amount'],$request['loc_id'],$request['order_id'],$ref_id);
    $response = json_decode($response,true);
    if ($response['ResultCode'] == 'Approved') {
        $parameters = array();
        $parameters['pay_status']=1;
        $parameters['order_id']=$request['order_id'];
        if ($request['type']=='deposit') {
            $parameters['transaction_status']=0;
            $parameters['pay_type'] = $type = $request['type'];
            $parameters['refund_date'] = "";
        }
        else {
            $parameters['transaction_status']=1;
            $parameters['pay_type'] = $type = 'deposit_refund';
            $parameters['refund_date'] = $datetime;
            lavu_query("update `cc_transactions` set `transaction_id`='',`refunded` = 1 where `order_id` = '".$request['order_id']."'");
        }
        
        $server_query = lavu_query("select CONCAT_WS(' ',f_name,l_name) as server_name from users where id='".$request['server_id']."'");
        $server_data = mysqli_fetch_assoc($server_query);
        $server_name = $server_data['server_name'];
        
        $cc_data = array(
                            'order_id' => $request['order_id'],
                            'amount' => $request['card_amount'],
                            'refunded' => 0,
                            'loc_id' => $request['loc_id'],
                            'datetime' => $datetime,
                            'pay_type' => $request['mode'],
                            'total_collected' => $request['card_amount'],
                            'transtype' => $type,
                            'action' => $type,
                            'server_time' => $datetime,
                            'transaction_id' => $response['RefID'],
                            'auth_code' => $response['AuthCode'],
                            'card_type' => $response['CardType'],
                            'card_desc' => $response['Last4'],
                            'gateway' => 'eConduit',
                            'register' => $request['register'],
                            'got_response' => '1',
                            'ref_data' => $response['TerminalID'],
                            'process_data' => 'payment_extension:82',
                            'preauth_id' => $response['RefID'],
                            'register_name' => $request['register_name'],
                            'pay_type_id' => '2',
                            'info' => $response['Name'],
                            'reader' => 'eConduit',
                            'server_name' => $server_name,
                            'check' => '1'
        );
        
        lavu_query("INSERT INTO `cc_transactions` ( `order_id`,
                                                    `amount`,
                                                    `refunded`,
                                                    `loc_id`,
                                                    `datetime`,
                                                    `pay_type`,
                                                    `total_collected`,
                                                    `transtype`,
                                                    `action`,
                                                    `server_time`,
                                                    `transaction_id`,
                                                    `auth_code`,
                                                    `card_type`,
                                                    `card_desc`,
                                                    `gateway`,
                                                    `register`,
                                                    `got_response`,
                                                    `ref_data`,
                                                    `process_data`,
                                                    `preauth_id`,
                                                    `register_name`,
                                                    `pay_type_id`,
                                                    `reader`,
                                                    `info`,
                                                    `server_name`,
                                                    `check`) 
                                            values( '[order_id]',
                                                    '[amount]',
                                                    '[refunded]',
                                                    '[loc_id]',
                                                    '[datetime]',
                                                    '[pay_type]',
                                                    '[total_collected]',
                                                    '[transtype]',
                                                    '[action]',
                                                    '[server_time]',
                                                    '[transaction_id]',
                                                    '[auth_code]',
                                                    '[card_type]',
                                                    '[card_desc]',
                                                    '[gateway]',
                                                    '[register]',
                                                    '[got_response]',
                                                    '[ref_data]',
                                                    '[process_data]',
                                                    '[preauth_id]',
                                                    '[register_name]',
                                                    '[pay_type_id]',
                                                    '[reader]',
                                                    '[info]',
                                                    '[server_name]',
                                                    '[check]')",
                                            $cc_data);
        
        lavu_query("UPDATE deposit_information set `pay_status`=[pay_status], 
                                                   `transaction_status`=[transaction_status],
                                                   `refund_date`='[refund_date]',
                                                   `pay_type`='[pay_type]'
                                               where transaction_id='[order_id]'",
                                            $parameters);
        
        $returnArr = "1|success";
        echo $returnArr;
    }
    else {
        if ($request['type']=='deposit') {
            lavu_query("DELETE FROM deposit_information WHERE `transaction_id`='[order_id]'", $request);
            lavu_query("DELETE FROM lip_order_transaction WHERE `order_id`='[order_id]'", $request);
        }
        $returnArr = "0|{$response['Message']}";
        echo $returnArr;
    }
    exit(0);
}
?>