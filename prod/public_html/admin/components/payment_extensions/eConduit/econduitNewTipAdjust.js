var APICredentials = {};
var extraArgs = {};
var retryAjaxCall = 0;

/**
* To hide iFrame
*
* @param
* @return
*/
function hideActivityShade() {
  document.getElementById('eConduitNewTipUiIframe').style.display = 'none';
}

/**
 * To validate object from string
 *
 * @param {*} obj - Object
 * @param {*} key - Key from object
 *
 * @return {*} - Any type
 */
function validStringFromObject(obj, key) {
  if (typeof obj === 'object' && typeof key === 'string') {
    return obj[key] ? obj[key].toString() : '';
  }
  return '';
}

/**
 * To show alert on success
 *
 * @param {string} title - title of alert
 * @param {string} message - content of alert
 *
 * @return {string} cmd
 * @return {string} title
 * @return {string} message
 */
function componentAlert(title, message) {
  if (isEmbeddedWebView) {
    window.location =
      '_DO:cmd=newTipUiAlert&title=' +
      encodeURIComponent(title) +
      '&message=' +
      encodeURIComponent(message);
  } else {
    alert(title + ' - ' + message);
    window.location =
      '_DO:cmd=refreshNewTipUi&title=updatedTransaction&message=success';
  }
}

/**
 * To load iframe
 *
 * @param {string} iFrameURL - An url which load into iframe
 *
 * @return {*} display alert according to result
 */
function loadTransactioniFrame(iFrameURL) {
  var overlayDiv = document.getElementById('eConduitNewTipUiIframe');
  overlayDiv.innerHTML = '';

  var iFrame = document.createElement('iframe');
  iFrame.style.display = 'div';
  iFrame.style.height = '100%';
  iFrame.style.width = '100%';
  iFrame.style.background = '#ffffff';
  iFrame.frameborder = '0';
  iFrame.style.margin = '0 auto';
  iFrame.style.border = '0 none';
  iFrame.src = iFrameURL;
  overlayDiv.appendChild(iFrame);

  /**
   * A listener which parse result got from iframe
   *
   * @param {*} event - object of data according to current execution event handler
   *
   * @return {*} display alert according to result
   */
  function listener(event) {
    // close overlay
    hideActivityShade();

    var jsonResponse = JSON.parse(event.data);
    var resultCode = validStringFromObject(jsonResponse, 'ResultCode');
    var resultFinal = jsonResponse['ResultFinal'];
    var resultMessage = validStringFromObject(jsonResponse, 'Message');
    var result = encodeURIComponent(resultCode);
    var message = encodeURIComponent(resultMessage);
    var newRefID = validStringFromObject(jsonResponse, 'RefID');

    // send new ajax request to update tip adjust
    if (resultCode == 'Approved') {
      // transaction approved, save to db

      var postVars = {};
      postVars.mode = 'update_transaction_records';
      postVars.ioid = extraArgs.order_ioid;
      postVars.rowId = extraArgs.rowId;
      postVars.trasactionAmount = extraArgs.transaction_amount;
      postVars.tipAmount = extraArgs.tipAmount;
      postVars.loc_id = extraArgs.activeLocationId;
      postVars.cc = extraArgs.activeDataName;
      postVars.currentLocalTime = extraArgs.currentLocalTime;
      postVars.command = extraArgs.command;
      postVars.new_ref_id = newRefID;

      // to assist with future troubleshooting
      postVars.event_data = encodeURIComponent(event.data);

      new ajaxRequest(postVars);
    } else if (resultMessage != '' && resultCode != 'Approved') {
      var postVars = {};
      postVars.mode = 'update_transaction_errors';
      postVars.cc = extraArgs.activeDataName;
      postVars.rowId = extraArgs.rowId;
      postVars.message =
        'ResultCode-' + resultCode + '|Message-' + resultMessage;
      postVars.event_data = encodeURIComponent(event.data);
      new ajaxRequest(postVars);
      componentAlert('Error', resultMessage);
    } else {
      // there was an error
      componentAlert(
        'Error',
        'The tip adjust did not go through. If you continue to have this issue, please contact Lavu Customer Support.'
      );
    }
  }

  if (window.addEventListener) {
    addEventListener('message', listener, false);
  } else {
    attachEvent('onmessage', listener);
  }
}

/**
 * To submit econduit tip
 *
 * @param
 *
 * @return alert will display according to result in @function - loadTransactioniFrame
 */
function submitEconduitTipAdjustOrCapture() {
  var overlayDiv = document.getElementById('eConduitNewTipUiIframe');
  overlayDiv.style.display = 'block';

  // find total with tip adjustment
  var adjustedTotal =
    Number(extraArgs.transaction_amount) + Number(extraArgs.tipAmount);
  var adjustedTotalSubmit = Number(adjustedTotal).toFixed(2);

  // load the iframe
  var iFrameURL = 'https://econduitapp.com/terminal.aspx?';
  iFrameURL += 'cmd=' + extraArgs.command + '&';
  iFrameURL += 'APIKey=' + APICredentials.apiKey + '&';
  iFrameURL += 'APIPassword=' + APICredentials.apiPwd + '&';
  iFrameURL += 'Amount=' + adjustedTotalSubmit + '&';
  iFrameURL += 'terminalID=' + extraArgs.terminalId + '&';
  iFrameURL += 'refid=' + extraArgs.refId;

  loadTransactioniFrame(iFrameURL);
}

/**
 * To parse json
 *
 * @param {Object} jsonString - Response got from @function - ajaxResponse
 *
 * @return {*} alert will display according to result in @function - loadTransactioniFrame
 */
function tryParseJSON(jsonString) {
  try {
    var o = JSON.parse(jsonString);
    if (o && typeof o === 'object' && o !== null) {
      return o;
    }
  } catch (e) {}

  return false;
}

 /**
 * if successfully got response once iFrame is load
 *
 * @param {Object} response - response got after iFrame loaded
 *
 * @return {*} according to result it will be notified to frontend using alert
 */
function ajaxResponse(response) {
  // ajax call should always come back with at least the mode
  var parsedResponse = tryParseJSON(response);
  var credentials = JSON.stringify(parsedResponse);
  // only set credentials if that's the call that came back
  if (parsedResponse.mode == 'load_credentials') {
    var key = parsedResponse.credentials['apiKey'];
    var pwd = parsedResponse.credentials['apiPassword'];

    // retry at least 3 times if invalid response
    // check for both key and password
    if (!key || !pwd) {
      if (key != '' && pwd != '') {
        if (retryAjaxCall < 3) {
          // key or password not returned, try again
          loadEconduitCredentials();
          retryAjaxCall++;
        } else {
          // could not get credentials
          componentAlert(
            'loading credentials',
            'Unable to get valid credentials'
          );
        }
      }
    } else {
      // received valid data
      APICredentials.apiKey = key;
      APICredentials.apiPwd = pwd;
      // we have credentials so process iframe tip adjust
      submitEconduitTipAdjustOrCapture();
    }
  }

  // the tip adjust went through successfully
  if (parsedResponse.mode == 'update_transaction_records') {
    if (parsedResponse.tip_amount) {
      // refresh
      window.location =
        '_DO:cmd=refreshNewTipUi&title=updatedTransaction&message=success';
    }
  }
}

/**
 * It will initiate request for internal call like validate credentials, update records
 *
 * @param {*} params - It contains
 *
 * @return {*} params - mode, command, loc_id, rowId, trasactionAmount.
 */
(function() {
  function ajaxRequest(params) {
    AJAXRequest.call(
      this,
      '/components/payment_extensions/eConduit/eConduit_req.php',
      'POST',
      params,
      true
    );
  }

  /**
  * If sucessfully got response once iFrame loaded.
  *
  * @param {Object} response - response from iFrame.
  *
  * @return - it will initiate @function - ajaxResponse.
  */
  function success(response) {
    ajaxResponse(response);
  }

  /**
  * If failed to got response once iFrame loaded.
  *
  * @param {Object} response - response from iFrame.
  *
  * @return - it will initiate @function - ajaxFailure.
  */
  function failure(status, response) {
    ajaxFailure(status, response);
  }

  window.ajaxRequest = ajaxRequest;
  ajaxRequest.prototype.constructor = ajaxRequest;
  ajaxRequest.prototype = Object.create(AJAXRequest.prototype);
  ajaxRequest.prototype.success = success;
  ajaxRequest.prototype.failure = failure;
})();

/**
* Initiate first call from frontend
*
* @param {number} tipvalue - tip amount
* @param {string} terminalId - terminal id
* @param {string} refId - transaction id
* @param {string} rowId - rowid
* @param {number} transactionAmount - transaction amount
* @param {string} ioid - ioid
* @param {string} currentLocalTime - current local time
* @param {number} activeLocationId - location id
* @param {string} activeDataName - data name
* @param {number} oldTipValue - old tip amount
*
* @return {*} it will initiate internal api call and after that load iframe and at the end transction got settled.
*/
function loadTipAdjustorCaptureiFrame(
  tipvalue,
  terminalId,
  refId,
  rowId,
  transactionAmount,
  ioid,
  currentLocalTime,
  activeLocationId,
  activeDataName,
  oldTipValue,
  command
) {
  //tipValue
  var tipValue = Number(tipvalue).toFixed(2);
  // check that tip adjust value has actually changed before launching the iFrame
  if (tipValue === oldTipValue && command == 'tipadjust') {
    return;
  }
  //set values
  extraArgs.command = command;
  extraArgs.tipAmount = tipValue;
  extraArgs.terminalId = terminalId;
  extraArgs.refId = refId;
  extraArgs.rowId = rowId;
  extraArgs.transaction_amount = transactionAmount;
  extraArgs.order_ioid = ioid;
  extraArgs.currentLocalTime = currentLocalTime;
  extraArgs.activeLocationId = activeLocationId;
  extraArgs.activeDataName = activeDataName;

  new ajaxRequest({
    mode: 'load_credentials',
    loc_id: activeLocationId,
    cc: activeDataName
  });
}

function ajaxFailure(status, response) {
  componentAlert('failed connection', status);
}
