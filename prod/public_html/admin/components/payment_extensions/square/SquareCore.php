<?php
/**
 * Created by PhpStorm.
 * User: hayat
 * Date: 8/22/17
 * Time: 11:57 AM
 */

require_once("SquareTokenHelper.php"); 
//include_once("SquarePaymentHandler.php");
require_once(__DIR__."/../../../lib/gateway_lib/hosted_checkout/util.php");
class SquareCore
{
	/*
	 * Public Properties
	 * Token Service
	 * API Credentials
	 * Token Scopes for oAuth token retrieval
	 */
	public $tokenHelper;


	/*
	 * Private properties:
	 * SquareEndpointURl = root URL for Square Endpoint
	 * attributes, business, pph and payments are all pieces of the scopes list for token requests
	 */
	public $SquareEndpointURL;

	/*
	 * Core Constructor:
	 * Sets the Lavu Identifiers for API calls
	 * mode is an indicator of dev environment or live.
	 */
	public function __construct($tokenHelper = null, $dataname = ''){
		if ($tokenHelper == null) {
			$tokenHelper = new SquareTokenHelper($dataname);
		}
		$this->tokenHelper = $tokenHelper;
	}

	/*
	 * $endPointURL - URL of Square service endpoint
	 * $fields - POST fields for API Call to service endpoint
	 */
	public function apidebug($rsp){
		$fstr = "";
		$fstr .= "\n---------------PPH:" . date("Y-m-d H:i:s") . "-------------------\n";
		$fstr .= $rsp;
		$fname = "/home/poslavu/logs/oauthdebug.txt";
		$file = fopen($fname,"a");
		fwrite($file,$rsp);
		fclose($file);
	}

	/*
	 * Dataname, location ID to be provided by caller
	 * Generates the form required for SquareHere onboarding.
	 */
	public function squareHereOnboardingForm($apiCredentials, $dataname) {
		//print_r($apiCredentials);
		// $squareUrl = (empty($apiCredentials)) ? SQUARE_SIGNUP_PAGE : SQUARE_SIGNIN_PAGE;
		$squareOAuthUrl = (!empty($apiCredentials)) ? SQUARE_OAUTH_HOST :"";
		$scope = '&scope=PAYMENTS_WRITE PAYMENTS_READ MERCHANT_PROFILE_READ';
		$oauthUrl = $squareOAuthUrl."oauth2/authorize?client_id=".$apiCredentials['applicationID'].$scope."&state=".md5($dataname)."&session=false";
		$locationdata =  $this->tokenHelper->getCredentialsByDataname($dataname);
		if ($locationdata['extra_info'] != '') {
			$extra_info = json_decode($locationdata['extra_info'], true);
			$email = $extra_info['email'];
		}
		//print_r($locationdata);
		$buttonText = '<table cellspacing="3" align="center" width="350">';
		$buttonText .= '<tbody><tr><td>';
		if ($locationdata['applicationID'] == '')
		{
			$buttonText .= '
				<p>Use Square’s Chip Card Reader to accept payments with magnetic stripe, chip card, contactless or Apple Pay on your iPad or iPhone.</p>
				<p>Click below to create a Square Merchant account or link your existing Square Merchant account with your Lavu account.</p>
				<p>After linking your Square and Lavu accounts, Square will automatically be added to your Payment Methods.</p>
				<p><b>Only available for accounts in the United States.</b></p>';
			$buttonText .='<button type="button" class="square-button square-blue" onClick="openInNewTab(\''.$oauthUrl.'\')">Get Started</button>';
		}
		else {
			//$locationdata =  $this->tokenHelper->getCredentialsByDataname($dataname);
			if ($locationdata['tokenID'] != '')
			{
				$buttonText .= ($email !='') ? '<div style="font-size:15px;padding:5px;" >Square Account: <span id="squareLocName">'.$email.'</span></div>' : '';
				if ($locationdata['locationID']!='') {
					$buttonText .= '<div style="font-size:15px;padding:5px;" >Square Location: <span id="squareLocName">' . $locationdata['location_name'] . '</span></div>';
				}
			}
			if ($locationdata['tokenID']!='') {
				$buttonText .= '<div style="float:left;padding:5px;"><a onClick="reLinkSquareLocations();" style="cursor:pointer;">Re-link Square location</a></div>';
				$buttonText .= '<div style="float:right;padding:5px;"><a onClick="disconnectSquareLocations();" style="cursor:pointer;">Disconnect Square Account</a></div>';
			}
		}

		$buttonText .= '</td></tr></tbody></table>';
		$buttonText .= '
		<div id = "divpopupmessage" style="position: fixed; z-index: 999; height: 100%; width: 100%; top: 0; left:0; display:none;">
			<div align="center" id="messagepopup" style="border: 1px solid #888; border-radius: 5px; width: 7vw; box-shadow: 0 4px 8px 0 rgba(0,0,0,1), 0 6px 20px 0 rgba(0,0,0,1); width: 400px; height:200px; margin:auto; margin-top:250px; background-color: white">
				<div style="padding: 10px 16px; background-color: #AECD37; border-top-left-radius: 5px; border-top-right-radius: 5px; text-align: center;">
						<span id="modal_close" class="modal_close" style="margin-top:-10px; color:#FFF; float:right; font-size:28px; font-weight:bold; cursor:pointer" onClick="closeMessagePopup()">×</span>
						<h1 style="font-size: 1em; font-weight: bold"><span id="modal_header_text_0">Message</span></h1>
				</div>
				<div style="padding:50px; font-size:30px;" id="message_content">
							
				</div>
				<div id="finishBtn">
					<input type="button" id="closeBtn" onClick="closeMessagePopup()" value="Close"></input>		
				</div>
			</div>
		</div>
	   <div id = "divBackground" style="position: fixed; z-index: 1; height: 100%; width: 100%; top: 0; left:0; display:none">
			<div align="center" id="squareOnBoardingForm" style="border: 1px solid #888; border-radius: 5px; width: 7vw; box-shadow: 0 4px 8px 0 rgba(0,0,0,1), 0 6px 20px 0 rgba(0,0,0,1); width: 500px; height:330px; margin:auto; margin-top:200px; background-color: white">
				 <div style="padding: 10px 16px; background-color: #AECD37; border-top-left-radius: 5px; border-top-right-radius: 5px; text-align: center;">
					<span id="modal_close" class="modal_close" style="margin-top:-10px; color:#FFF; float:right; font-size:28px; font-weight:bold; cursor:pointer" onClick="closeSquareOnBoardingForm()">×</span>
						<h style="font-size: 1em; font-weight: bold"><span id="modal_header_text_0">Square Locations</span></h>
				</div>
				<div id="info_div" style="display:none;padding:10px;height:0px;margin-top:5px;">&nbsp;</div>
				<div id="locationsDiv">
						<form method="post" id="square_form" name="square_form" action="">
							<input type="hidden" id="locationsdata"/>
							<div>
							<table width="95%" border="0" cellspacing="10" cellpadding="0" style="margin-top:20px">
								<tr>
									<td>Location Name</td>
									<td></td>
									<td id="select_loc_id">
										<select style="height:20px; font-size:14px; width:300px" id="location_id" name="location_id">
											<option value="">---Select Square Location---</option>
										</select>
									</td>
								</tr>
								<tr><td colspan="3" align="center"><button type="button" id="saveSquareCredential" class="square-button square-blue" onClick="updateSquareLocation()">Save</button></td></tr>
							</table>
							</div>
				
						</form>
				</div>
			</div>
		</div>
	   ';
		return $buttonText;
	}

	//returns the text for the Express Checkout onboarding button
	public function squareExpressCheckoutOnboarding($dataname, $echo = true){

		$buttonText = '
		<div dir="ltr" style="text-align: left;" trbidi="on">
		<script>(function(d, s, id){
		 var js, ref = d.getElementsByTagName(s)[0];
		 if (!d.getElementById(id)) {
		 js = d.createElement(s); js.id = id; js.async = true;
		 js.src = "'. $this->SquareEndpointURL .
			'webapps/merchantboarding/js/lib/lightbox/partner.js";
		 ref.parentNode.insertBefore(js, ref);
		 }
		 }(document, "script", "square-js"));
		</script>
		<p>Your Lavu account is ready to accept payments using Square.<p>
		<p>Square has been added to your Payment Methods.</p>
		<p>To enable Square for Lavu To Go, click below. </p>
		<br>
		<a data-square-button="true" style="font-size:14px;border:none;padding:5px 50px;border-radius:4px;box-shadow:0 8px #8b8b8b;" class="square-blue" href= "' .
			$this->SquareEndpointURL.'webapps/merchantboarding/webflow/externalpartnerflow?integrationType=T'.
			'&subIntegrationType=S'.
			'&partnerId='. $this->lavu_merchantID .
			'&productIntentID=addipmt&returnToPartnerUrl=' .$this->returnURL.'?dataname='.$dataname.
			'&displayMode=lightbox'.
			'&receiveCredentials=TRUE'.
			'&showPermissions=TRUE'.
			'&permissionNeeded=EXPRESS_CHECKOUT'.
			'&merchantId=DataNameProbably" target="PPFrame">Enable Square Express Checkout</a>
		</div>
		';
		if($echo){
			echo($buttonText);
		}
		else return $buttonText;
	}
	public function getLocationsForDataname($data_name)
	{
		include_once("SquarePaymentHandler.php");
		$square_credential = $this->tokenHelper->getCredentialsByDataname($data_name);
		//print_r($square_credential);
		$SquarePaymentHandler = new SquarePaymentHandler($data_name, $square_credential['tokenID']);

		$sqr_loc = $SquarePaymentHandler->getAllSquareLocation();
		//print_r($sqr_loc);
		$locArr = $this->tokenHelper->getLocationsByMerchant($square_credential['applicationID']);
		//print_r($locArr);
		$newarr = array();
		if (!empty($sqr_loc['locations'])) {
			foreach ($sqr_loc['locations'] as $key => $locations) {
				if (count($locArr) > 0) {
					$newkey = in_array($locations['id'], $locArr);
					if ($newkey) {
						//array_splice($sqr_loc['locations'], $newkey, 1);
						unset($sqr_loc['locations'][$key]);

					}
					array_values($sqr_loc['locations']);
				}

			}
			foreach ($sqr_loc['locations'] as $key => $locations) {
				$location['id'] = $locations['id'];
				$location['name'] = $locations['name'];
				$newarr['locations'][] = $location;
			}
		}
		 return $newarr;
	}

}
