<?php
/**
 * Created by PhpStorm.
 * User: hayat
 * Date: 8/29/17
 * Time: 4:25 PM
 */

require_once(dirname(__FILE__)."/../../../lib/info_inc.php");
include("SquareCore.php");
include("SquarePaymentHandler.php");

$dataname		= reqvar("dataname");
$mode			= reqvar("mode", "");
$session_id		= reqvar("PHPSESSID", "");
session_start($session_id);

if($dataname === ""){
	echo json_encode(array("error" => "Access Error: No Valid Dataname"));
}

if ($mode == 'getToken') {
	$SquareCore = new SquareCore('', $dataname);
	echo json_encode($SquareCore->tokenHelper->getAPICredentials($dataname, true));
} else if ($mode == 'generateToken') {
	$SquarePaymentHandler = new SquarePaymentHandler($dataname);
	echo json_encode($SquarePaymentHandler->updateAccessToken());
} else if ($mode == 'revokeToken') {
	$SquarePaymentHandler = new SquarePaymentHandler($dataname);
	echo json_encode($SquarePaymentHandler->revokeSquareToken());
} else if ($mode == 'getTransactionDetails') {
	$transactionid		= reqvar("transactionid", "");
	$userid				= reqvar("userid", "");
	$SquarePaymentHandler = new SquarePaymentHandler($dataname);
	$transDetails = $SquarePaymentHandler->getTransactionDetails($transactionid, $userid);
	echo json_encode($transDetails);
} else if($mode == 'getDepositTransactionDetails') {
	$transactionid      = reqvar("transactionid", "");
	$userid             = reqvar("userid", "");
	$SquarePaymentHandler = new SquarePaymentHandler($dataname);
	$transDetails = $SquarePaymentHandler->retrieveTransactionDetails($transactionid);
	echo json_encode($transDetails);
} else {
	echo json_encode(array("error" => "Unknown mode"));
}