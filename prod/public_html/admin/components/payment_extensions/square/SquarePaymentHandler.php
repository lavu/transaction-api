<?php
/**
 * Created by PhpStorm.
 * User: hayat
 * Date: 8/22/17
 * Time: 12:02 PM
 */


/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

require_once("SquareTokenHelper.php");
class SquarePaymentHandler{

	public $tokenHelper;
	private $api_credential;
	private $application_id;
	private $access_token;
	private $secret_id;
	private $location_id;
	private $extension_id;
	private $data_name;
	private $SquareEndpointURL = "https://connect.squareup.com/v2/";
	private $SquareV1EndpointURL = "https://connect.squareup.com/v1/";
	private $SquareClientEndpointURL = "https://connect.squareup.com/";
	private $cardTenderTypes;

	function __construct($data_name, $access_token = '', $location_id = '')
	{
		$this->tokenHelper = new SquareTokenHelper($data_name);
		$this->api_credential = $this->tokenHelper->getAPICredentials($data_name);
		$this->application_id = $this->api_credential['applicationID'];
		$this->access_token = ($access_token != '') ? $access_token : $this->api_credential['tokenID'];
		$this->secret_id = $this->api_credential['secretID'];
		$locationId = ($location_id != '') ? $location_id : $this->api_credential['locationID'];
		$this->location_id = $locationId;
		$this->extension_id = $this->api_credential['extensionID'];
		$this->data_name = $data_name;
		$this->cardTenderTypes = $this->tokenHelper->cardTenderTypes;

		//$this->writeDebugStrOauth("refresh token: " . $this->refresh_token . "\naccess token json: " . $this->access_token_json . "\n");
	}

	function getAccessToken(){
		return $this->access_token;
	}

	function getApplicationID(){
		return $this->application_id;
	}

	function getSecretID(){
		return $this->secret_id;
	}

	function getLocationID(){
		return $this->location_id;
	}

	function getApiEndPoint(){
		return $this->SquareEndpointURL;
	}

	function getV1ApiEndPoint(){
		return $this->SquareV1EndpointURL;
	}

	function getApiEndPointWithLocation(){
		return $this->SquareEndpointURL.'locations/'.$this->location_id;
	}

	function getV1ApiEndPointWithLocation(){
		return $this->SquareV1EndpointURL.$this->location_id;
	}

	function getApiCredential(){
		return $this->api_credential;
	}

	function getExtensionId(){
		return $this->extension_id;
	}

	function setErr($tag, $replace = ''){
		$get=getErrMessage($tag,$replace);
		$err['message']=$get['body'];
		$err['title']=$get['title'];
		return $err;
	}


	function writeDebugStrOauth($rsp){
		$fstr = "";
		$fstr .= "\n---------------PPH:" . date("Y-m-d H:i:s") . "-------------------\n";
		$fstr .= $rsp;
		$fname = "/home/poslavu/logs/oauthdebug.txt";
		$file = fopen($fname,"a");
		fwrite($file,$rsp);
		fclose($file);
	}
	function writeDebugStr($dstr){
		$fstr = "";
		$fstr .= "\n---------------PPH:" . date("Y-m-d H:i:s") . "-------------------\n";
		$fstr .= $dstr;
		$fname = "/home/poslavu/logs/square.txt";
		$file = fopen($fname,"a");
		fwrite($$file,$dstr);
		fclose($file);
	}

	function getAllSquareLocation(){
		$curlURL = $this->SquareEndpointURL.'locations';
		$response = self::makeApiCall('', $curlURL, 'GET');
		$rspvars = json_decode($response, true);
		$locationsId = array();
		$locationResponse = array();
		if (!isset($rspvars['errors'])) {
			foreach ($rspvars['locations'] as $key => $locations) {
				if ($locations['status'] == 'ACTIVE' && in_array('CREDIT_CARD_PROCESSING', $locations['capabilities'])) {
					$locationsId[$key]['id'] = $locations['id'];
					$locationsId[$key]['name'] = $locations['name'];
				}
			}
			$locationResponse['locations'] = $locationsId;
		} else {
			$locationResponse['error'] = $rspvars['errors'][0]['detail'];
		}
		return $locationResponse;
	}

	function getMerchantDetails(){
		$curlURL = $this->SquareV1EndpointURL.'me';
		$response = self::makeApiCall('', $curlURL, 'GET');
		$rspvars = json_decode($response, true);
		$merchantDetailsResponse = array();
		if (!isset($rspvars['message'])) {
			$merchantDetailsResponse['name'] = $rspvars['name'];
			$merchantDetailsResponse['email'] = $rspvars['email'];
			$merchantDetailsResponse['country_code'] = $rspvars['country_code'];
			$merchantDetailsResponse['language_code'] = $rspvars['language_code'];
			$merchantDetailsResponse['business_type'] = $rspvars['business_type'];
			$merchantDetailsResponse['account_type'] = $rspvars['account_type'];
			$merchantDetailsResponse['currency_code'] = $rspvars['currency_code'];
		} else {
			$merchantDetailsResponse['error'] = $rspvars['message'];
		}
		return $merchantDetailsResponse;
	}

	function renewSquareToken($token = '') {
		$token = ($token != '') ? $token : $this->getAccessToken();
		$curlURL = $this->SquareClientEndpointURL.'oauth2/clients/'.$this->getApplicationID().'/access-token/renew';
		$fields = '{' .
			'"access_token": "' .$token . '"
		}';
		$response = self::makeApiCall($fields, $curlURL, 'POST', true);
		$rspvars = json_decode($response, true);
		$tokenResponse = array();
		if (!isset($rspvars['type']) && isset($rspvars['access_token'])) {
			$tokenResponse['access_token'] = isset($rspvars['access_token']) ? $rspvars['access_token'] : '';
		} else {
			$tokenResponse['error'] = isset($rspvars['errors']) ? $rspvars['errors'][0]['detail'] : $rspvars['message'];
		}
		return $tokenResponse;
	}

	function revokeSquareToken() {
		$curlURL = $this->SquareClientEndpointURL.'oauth2/revoke';
		$fields = '{' .
			'"access_token": "' . $this->getAccessToken() . '",
			"client_id": "' . $this->getApplicationID() . '"
		}';
		$response = self::makeApiCall($fields, $curlURL, 'POST', true);
		$rspvars = json_decode($response, true);
		$tokenResponse = array();
		if (isset($rspvars['success']) && $rspvars['success'] == 1) {
			$tokenResponse = $rspvars;
		} else if ((isset($rspvars['errors']) && $rspvars['errors'][0]['code'] = 'ACCESS_TOKEN_REVOKED') || (isset($rspvars['message']) && $rspvars['type'] == 'not_found')) {
			// If we get 'ACCESS_TOKEN_REVOKED' error from square api, Means token has been revoked from square
			// side. So make soft delete this account from lavu side.
			$rspvars['success'] = 1;
			$tokenResponse = $rspvars;
		}
		else {
			$tokenResponse['error'] = isset($rspvars['errors']) ? $rspvars['errors'][0]['detail'] : $rspvars['message'];
		}
		return $tokenResponse;
	}

	function updateAccessToken()
	{
		$tokenResponse = self::renewSquareToken();
		if (!isset($tokenResponse['error'])) {
			$this->tokenHelper->updateAccessToken($this->data_name, $tokenResponse['access_token'], $this->extension_id);
		}
		return $tokenResponse;
	}

	/**
	 * Get the transaction details of given transaction id.
	 * @param string $txnid
	 * @return array
	 */
	function retrieveTransactionDetails($txnid)
	{
		if($txnid != '') {
			$transResponse = self::retrieveV1TransactionDetails($txnid);
			if (isset($transResponse['error']) || $transResponse['transaction'] == '') {
				$transResponse = self::retrieveV2TransactionDetails($txnid);
			}
		} else {
		//$err=$this->setErr('SQUARE_ERR_2',array("val1"=>$txnid));
		$eMessage = speak('Square transaction id not found');
		$transResponse['error'] = $eMessage;
		}
		return $transResponse;
	}

	/**
	 * Get the transaction details of given transaction id from V1 endpoint.
	 * @param string $txnid
	 * @return array
	 */
	function retrieveV1TransactionDetails($txnid)
	{
		$retrieveTransaction = '/payments/' . $txnid;
		$fields = '';
		$curlurl = $this->getV1ApiEndPointWithLocation() . $retrieveTransaction;
		$response = self::makeApiCall($fields, $curlurl, 'GET');
		$rspvars = json_decode($response, true);
		$transResponse = array();
		if (!empty($rspvars)) {
			if (isset($rspvars['type'])) {
				if ($rspvars['type'] == "oauth.expired") {
					$tokenResponse = self::updateAccessToken();
					if (!isset($tokenResponse['error'])) {
						if ($tokenResponse['access_token'] != '') {
							$this->access_token = $tokenResponse['access_token'];
							return self::retrieveV1TransactionDetails($txnid);
						}
					}
				} else if ($rspvars['type'] == "not_found") {
					$transResponse['error'] = isset($rspvars['message']) ? $rspvars['message'] : 'Not getting any response from square';
				}
			} else {
				$transResponse['transaction'] = (!empty($rspvars)) ? $rspvars : '';
			}
		} else {
			error_log("Error:- Square API is not getting response of transaction id '" . $txnid . "' for location '" . $this->data_name . "'");
			$transResponse['error'] = 'Not getting any response from square';
		}
		return $transResponse;
	}

	/**
	 * Get the transaction details of given transaction id from V2 endpoint.
	 * @param string $txnid
	 * @return array
	 */
	function retrieveV2TransactionDetails($txnid)
	{
		$retrieveTransaction = '/transactions/' . $txnid;
		$fields = '';
		$curlurl = $this->getApiEndPointWithLocation() . $retrieveTransaction;
		$response = self::makeApiCall($fields, $curlurl, 'GET');
		$rspvars = json_decode($response, true);
		$transResponse = array();
		if (!empty($rspvars)) {
			if (isset($rspvars['errors']) ) {
				if ($rspvars['errors'][0]['code'] == "ACCESS_TOKEN_EXPIRED") {
					$tokenResponse = self::updateAccessToken();
					if (!isset($tokenResponse['error'])) {
						if ($tokenResponse['access_token'] != '') {
							$this->access_token = $tokenResponse['access_token'];
							return self::retrieveV2TransactionDetails($txnid);
						}
					}
				} else if ($rspvars['errors'][0]['code'] == "NOT_FOUND") {
					$transResponse['error'] = isset($rspvars['errors'][0]['detail']) ? $rspvars['errors'][0]['detail'] : 'Not getting any response from square';
				}
			} else {
				$transResponse['transaction'] = (!empty($rspvars['transaction'])) ? $rspvars['transaction'] : '';
			}
		} else {
			error_log("Error:- Square API is not getting response of transaction id '" . $txnid . "' for location '" . $this->data_name . "'");
			$transResponse['error'] = 'Not getting any response from square';
		}
		return $transResponse;
	}

	/**
	 * This method is used to send reponse to iOS in required format
	 * @param $txnid
	 * @return string
	 */
	function getTransactionDetails($txnid, $userid)
	{
		//$msg = $this->setErr('SQUARE_MSG_1');

		for ($i = 0; $i < 3; $i++) {
			$transactionResponse = self::retrieveV2TransactionDetails($txnid);
			if ((isset($transactionResponse['error']) || empty($transactionResponse)) && $i < 3) {
				continue;
			}
			break;
		}

		if(!isset($transactionResponse['error'])) {
			// Removed below code because App using order id from their local so no need to hit V1 square endpoint
			$orderID = '';
			$tenders = $transactionResponse['transaction']['tenders'];
			$rf_id = self::getLocationID();//Storing locatiopn id in rf_id column
			$separator = '';
			$transResponse = '';
			$conversionDetails = $this->tokenHelper->getSquareConversionRate();
			$currencyCode = $transactionResponse['transaction']['tenders'][0]['amount_money']['currency'];
			$currencyCode = ($currencyCode != '') ? $currencyCode : 'USD';
			$conversionRate = (isset($conversionDetails[$currencyCode])) ? $conversionDetails[$currencyCode] : 100;
			foreach($tenders as $tender) {
				$split_tender_id = $tender['id'];
				$card_brand = '';
				$last_4 = '';
				if (isset($this->cardTenderTypes['tender_type'][$tender['type']])) {
					$card_brand = (isset($tender['card_brand'])) ? $tender['card_brand'] : $tender['card_details']['card']['card_brand'];
					$last_4 = (isset($tender['pan_suffix'])) ? $tender['pan_suffix'] : $tender['card_details']['card']['last_4'];
				}
				$totalMoney = $tender['amount_money']['amount'];
				$process_data = '';
				$tipAmount = 0;
				if(isset($tender['tip_money']['amount']) && $tender['tip_money']['amount'] > 0)
				{
					$tipAmount = $tender['tip_money']['amount'];
				}
				$amount = ($totalMoney - $tipAmount) / $conversionRate;
				$tipAmount = $tipAmount / $conversionRate;
				//  0 - success flag ("1")
				//  1 - [transaction_id]
				//  2 - [card_desc] (last_four)
				//  3 - response message
				//  4 - [auth_code]
				//  5 - [card_type]
				//  6 - [record_no]
				//  7 - [amount]
				//  8 - [order_id]
				//  9 - [ref_data]
				// 10 - [process_data]
				// 11 - new balance
				// 12 - bonus message
				// 13 - [first_four]
				// 14 - [last_mod_ts]
				// 15 - [tip_amount]
				// 16 - [split_tender_id]
				// 17 - [rf_id] Storing location ID
				// 18 - [pay_type] Storing Tender Type
				// 19 - [pay_type_id] storing pay type id like 1,2,3
				// 20 - [transtype] like Sale, Return, GiftCardReturn, GiftCardSale
			   $transResponse .= $separator . '1|' . $txnid . '|' . $last_4 . '|Square Response|' . $txnid . '|' . $card_brand . '|'.$split_tender_id.'|' . $amount . '|' . $orderID . '|||' . $process_data . '|||' . time().'|'.$tipAmount.'|'.$split_tender_id.'|'.$rf_id.'|'.$this->cardTenderTypes['tender_type'][$tender['type']].'|'.$this->cardTenderTypes['tender_id'][$tender['type']].'|'.$this->cardTenderTypes['sale_trans'][$tender['type']];
			   $separator = '###';
			}
		} else {
			if ($txnid != '') {
				$location_id = self::getLocationID();
				$datetime = date('Y-m-d H:i:s');
				$reqinfo = array('data_name'=>$this->data_name, 'gateway'=>'square', 'transaction_id'=>$txnid, 'location_id'=>$location_id, 'user_id'=>$userid, 'status'=>'pending', 'created_date'=>$datetime);
				$this->tokenHelper->insertSyncRequest($reqinfo);
				$transactionResponse['error'] = 'You are done with transaction, Unfortunately we are not getting transaction details at this time. It will be reflected by reconciliation.';
				$transResponse = '0|'.$transactionResponse['error'].'|'.$txnid.'||'.$transactionResponse['error'];
			} else {
				$transResponse = '0|'.$transactionResponse['error'].'|'.$txnid.'||'.$transactionResponse['error'];
			}
		}
		return $transResponse;
	}

	function makeApiCall($fields, $squareURL, $method = 'GET', $client = false){
		$access_token = ($client) ? 'Client '.$this->getSecretID() : 'Bearer '.$this->getAccessToken();
		$ch = curl_init();
		if($squareURL=="")
			return "";

		switch ($method)
		{
			case "POST":
				curl_setopt($ch, CURLOPT_POST, 1);

				if ($fields)
					curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
				break;
			case "PUT":
				curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'PUT');
				if ($fields)
					curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
				break;
			case "GET":
				curl_setopt($ch, CURLOPT_HTTPGET, 1);
				break;
			case "DELETE":
				curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'DELETE');
				curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
				break;
			default:
				if ($fields)
					$squareURL = sprintf("%s?%s", $squareURL, http_build_query($fields));
		}
		curl_setopt($ch, CURLOPT_URL, $squareURL);
		if(lavuDeveloperStatus()){
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		}
		curl_setopt($ch, CURLOPT_ENCODING, "");
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, FALSE);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json', 'Accept: application/json', 'Accept-Language: en_US', 'Authorization: '.$access_token));
		$response = curl_exec($ch);
		curl_close($ch);
		return $response;
	}
	/*
	 * This method will get the transactions list from Square V2 endpoint based on dates and location
	 * params 1,2 are start date and end date, 3 is square cursor to get the next page records, 4 is location id
	 */
	function getTransactionswithInPeriod($starttime, $endtime, $cursor = '', $locationid = '')
	{
		if ($starttime != '' && $endtime!='') {
			$sdate = date("Y-m-d\TH:i:s.000\Z", strtotime($starttime));
			$edate = date("Y-m-d\TH:i:s.000\Z", strtotime($endtime));
			$locationid = ($locationid) ?  $locationid : self::getLocationID();
			$qrystr = '?begin_time='.$sdate.'&end_time='.$edate;
			if($cursor != '') {
				$qrystr .='&cursor='.$cursor;
			}
			$retrieveTransactions = '/transactions' . $qrystr;
			$fields = '';
			$curlurl = $this->SquareEndpointURL.'locations/'.$locationid. $retrieveTransactions;
			$response = self::makeApiCall($fields, $curlurl, 'GET');
			$rspvars = json_decode($response, true);
			if (!isset($rspvars['errors'])) {
				$transResponse['transactions'] = (!empty($rspvars)) ? $rspvars['transactions'] : '';
			} else {
				$transResponse['error'] = isset($rspvars['errors']) ? $rspvars['errors'][0]['detail'] : 'Not getting any response from square';
			}
		} else {
			$transResponse['error'] = "Dates are not found.";
		}
		return $transResponse;
	}
	/*
	 * This method will get the transactions list from Square V2 endpoint based on dates and location
	 * params 1,2 are start date and end date, 3 is location id
	 */
	function getTransactionswithInPeriodFromV1Api($starttime,$endtime,$location)
	{
		$sdate = date("Y-m-d\TH:i:s.000\Z", strtotime($starttime));
		$edate = date("Y-m-d\TH:i:s.000\Z", strtotime($endtime));
		if($sdate != '' && $edate!='') {
			$qrystr = '?begin_time='.$edate.'&end_time='.$sdate.'&limit=200';

				$retrieveTransactions = '/payments' . $qrystr;
				$fields = '';
				$curlurl = $this->SquareV1EndpointURL. $location. $retrieveTransactions;
				$response = self::makeApiCall($fields, $curlurl, 'GET');
				$rspvars = json_decode($response, true);
				if (!isset($rspvars['message']) && !isset($rspvars['type'])) {
					$transResponse['transaction'] = (!empty($rspvars)) ? $rspvars : '';
				}
				else {
					$transResponse['error'] = isset($rspvars['message']) ? $rspvars['message'] : 'Not getting any response from square';
				}
		} else {
			$transResponse['error'] = "Dates are not found.";
		}
		return $transResponse;
	}

	/**
	 * Function is used to enable payment update webhook
	 * @param $location_id Square selected location id
	 * @return array|mixed
	 */
	function enableWebhook($location_id) {
		//curl -X PUT -H “Authorization: Bearer PERSONAL_ACCESS_TOKEN” -H “Content-Type: application/json” -d “[\”PAYMENT_UPDATED\”]” https://connect.squareup.com/v1/me/webhooks
		$curlURL = $this->getV1ApiEndPoint().$location_id.'/webhooks';
		$fields = '["PAYMENT_UPDATED"]';
		$response = self::makeApiCall($fields, $curlURL, 'PUT', false);
		$rspvars = json_decode($response, true);
		$webhookResponse = array();
		if (isset($rspvars['0']) && $rspvars['0'] == 'PAYMENT_UPDATED') {
			$webhookResponse = $rspvars;
		}
		else {
			$webhookResponse['error'] = 'Webhook notification has not been updated';
		}
		return $webhookResponse;
	}

	/**
	 * This method will parse square transactions list and captured into gateway_sync_requesr table
	 * @param $transactions transactions in json format
	 * @return 1|0
	 */
	function capturedTransactionsToReconcile($transactions, $data_name, $user_id)
	{
		if (!isset($transactions['error'])) {
			if (!empty($transactions)) {
				$datetime = date('Y-m-d H:i:s');
				foreach ($transactions['transactions'] as $details) {
					$location_id = $details['location_id'];
					$transaction_id = $details['tenders'][0]['transaction_id'];
					$reqinfo = array('data_name' => $data_name, 'gateway' => 'square', 'transaction_id' => $transaction_id, 'location_id' => $location_id, 'user_id' => $user_id, 'status' => 'pending', 'created_date' => $datetime);
					$this->tokenHelper->insertSyncRequest($reqinfo);
				}
			}
			return 'done';
		}
	}
}
