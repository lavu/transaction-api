<?php
/**
 * Created by PhpStorm.
 * User: hayat
 * Date: 8/22/17
 * Time: 11:58 AM
 */

define("SQUARE_SIGNUP_PAGE", 'https://squareup.com/signup');
define("SQUARE_SIGNIN_PAGE", 'https://squareup.com/login');
define("SQUARE_OAUTH_HOST", 'https://connect.squareup.com/');
/*
 * This file is meant to contain all methods relevant for inserting or retrieving tokens from the Lavu Database
 */
require_once(dirname(__FILE__)."/../../../lib/info_inc.php");
require_once(dirname(__FILE__)."/../../../lib/jcvs/Extensions.php");

// Populate required environment variables
populateEnvVars();

class SquareTokenHelper
{
	public $extensionObj;

	public $squareAppId;
	public $squareSecret;
	public $squareAccessToken;
	public $squareLocation;

	public $cardTenderTypes = array('tender_type' => array('CARD' => 'Card', 'SQUARE_GIFT_CARD' => 'Square Gift'), 'tender_id' => array('CARD' => '2', 'SQUARE_GIFT_CARD' => '2'), 'sale_trans' => array('CARD' => 'Sale', 'SQUARE_GIFT_CARD' => 'GiftCardSale'), 'return_trans' => array('CARD' => 'Return', 'SQUARE_GIFT_CARD' => 'GiftCardReturn'));

	public function __construct($data_name = ''){
		if ($data_name) {
			$this->setEnvironment($data_name);
		}
	}

	/**
	* LP-6386: This is to set dataname and secretkey which will be used later to query gateway_auth_token
	* @params $dataname String
	* @return Array
	*/
	public function setEnvironment($data_name = '') {
		// Loads Lavu's Square information
		$this->squareAppId = $_ENV['SQUARE_APP_ID'];
		$this->squareSecret = $_ENV['SQUARE_SECRET'];
		$this->squareAccessToken = $_ENV['SQUARE_ACCESS_TOKEN'];
		$this->squareLocation = $_ENV['SQUARE_LOCATION_ID'];
	}

	//Pulls the refresh for the currently active dataname from the auth_tokens table
	function getAPICredentials($dataname, $delete = false)
	{
		// Fetches client's credentials information
		$extensionId = $this->getExtensionID();
		$squareCredentialsQuery = "SELECT dataname, merchant_id, token, location_id FROM gateway_auth_tokens WHERE `dataname`= '" . $dataname . "' and `extension_id`='" . $extensionId . "'". $deleteStr;

		$squareCredentialsResult = mlavu_query($squareCredentialsQuery);
		$row = mysqli_fetch_assoc($squareCredentialsResult);

		$square_token_array = array();
		// Lavu's information
		$square_token_array['secretID'] = $this->squareSecret;
		$square_token_array['applicationID'] = $this->squareAppId;

		// Client's information
		$square_token_array['tokenID'] =  $row['token'];
		$square_token_array['locationID'] =  $row['location_id'];

		// Extension ID
		$square_token_array['extensionID'] = $extensionId;

		return $square_token_array;
	}

	function getOAuthCredentials($delete = false)
	{
		$square_token_array = array();
		$square_token_array['secretID'] = $this->squareSecret;
		$square_token_array['applicationID'] = $this->squareAppId;
		$square_token_array['tokenID'] = $this->squareAccessToken;
		$square_token_array['locationID'] = $this->squareLocation;
		$square_token_array['extensionID'] = $this->getExtensionID();

		return $square_token_array;
	}

	public function getCredentialsByDataname($dataname)
	{
		$extensionId = $this->getExtensionID();
		$deleteStr =  " AND `_deleted` = 0";
		$squareCredentialsQuery = "SELECT extension_id, merchant_id, token, location_name, location_id, extra_info FROM gateway_auth_tokens WHERE `dataname`='" . $dataname . "' and `extension_id`='" . $extensionId . "'". $deleteStr;

		$squareCredentialsResult = mlavu_query($squareCredentialsQuery);

		$credentials = mysqli_fetch_assoc($squareCredentialsResult);
		$square_token_array['applicationID'] = $credentials['merchant_id'];
		$square_token_array['tokenID'] = $credentials['token'];
		$square_token_array['locationID'] = $credentials['location_id'];
		$square_token_array['location_name'] = $credentials['location_name'];
		$square_token_array['extensionID'] = $credentials['extension_id'];
		$square_token_array['extra_info'] = $credentials['extra_info'];
		return $square_token_array;
	}

	function getLocationsByMerchant($token)
	{
		$squarelocationsQuery = "SELECT location_id FROM gateway_auth_tokens WHERE merchant_id='".$token."' AND `_deleted` = 0";

		$squarelocationsResult = mlavu_query($squarelocationsQuery);
		$square_locations_array = array();
		while($row = mysqli_fetch_assoc($squarelocationsResult)){
			 $square_locations_array[] =  $row['location_id'];
		}
		return $square_locations_array;
	}

	function getExtensionID()
	{
		$squareExtQuery = "SELECT `id` from extensions where `title` = 'Square'";
		$squareExtResult = mlavu_query($squareExtQuery);
		$row = mysqli_fetch_assoc($squareExtResult);
		return $row['id'];
	}

	function insertGateWayToken($dataname)
	{
		$extensionId = $this->getExtensionID();
		$rows = $this->getTokenInfoByDataName($dataname, $extensionId);
		if(count($rows)<=0)
		{
			$insertResult = mlavu_query("INSERT INTO gateway_auth_tokens (`dataname`, `extension_id`) VALUES ('[1]', '[2]')", $dataname, $extensionId);
			return $insertResult;
		}
		else
		{
			$updateTokenQuery = "UPDATE gateway_auth_tokens set token='',merchant_id='',token_expiration_date='',permission_granted='',token_generated_date='',location_id='',location_name='',_deleted=0 where id='".$rows['id']."'";
			$updateTokenres = mlavu_query($updateTokenQuery);
			return $updateTokenres;
		}
	}

	public function getTokenInfoByDataName($data_name)
	{
		$extensionId = $this->getExtensionID();
		$authTokenQry = "select id,location_id from gateway_auth_tokens where dataname='".$data_name."' and extension_id='".$extensionId."' order by id desc limit 1";
		$authTokenres = mlavu_query($authTokenQry);
		return mysqli_fetch_assoc($authTokenres);
	}

	public function updateSquareLocation($dataname, $squareCredentialArr)
	{
		$extensionId = $this->getExtensionID();
		$updateTokenQuery = "UPDATE gateway_auth_tokens set location_name='[1]', location_id='[2]'  where dataname = '[3]' and extension_id='[4]' AND `_deleted` = 0";
		return mlavu_query($updateTokenQuery,$squareCredentialArr['locationName'],$squareCredentialArr['locationID'],$dataname,$extensionId);
	}

	public function removeSquareLocation($dataname)
	{
		$squareMerchantQuery = "SELECT `merchant_id` FROM `gateway_auth_tokens` WHERE `dataname` = '".$dataname."' AND `_deleted` = 0";
		$squareMerchantResult = mlavu_query($squareMerchantQuery);
		$merchantResult = mysqli_fetch_assoc($squareMerchantResult);
		$merchant_id = $merchantResult['merchant_id'];
		$updateTokenQuery = "UPDATE `gateway_auth_tokens` SET `location_id` = '', `location_name` = '', `permission_granted` = 0, `_deleted` = 1 WHERE `merchant_id` = '".$merchant_id."'";
		mlavu_query($updateTokenQuery);
	}
	
	public function updateOrInsertOAuthToken($dataname,$oauthrespArr){

		$extensionId = $this->getExtensionID();
		$rows = $this->getTokenInfoByDataName($dataname);
		$curdate = date('Y-m-d H:i:s');
		if(count($rows)>0)
		{
			$updateTokenQuery = "UPDATE gateway_auth_tokens set merchant_id='[1]',token='[2]',token_expiration_date='[3]',permission_granted=1,token_generated_date='[4]', extra_info= '[5]' where dataname = '[6]' and extension_id='".$extensionId."' AND `_deleted` = 0";
			mlavu_query($updateTokenQuery,$oauthrespArr['merchant_id'],$oauthrespArr['access_token'],$oauthrespArr['expires_at'],$curdate,$oauthrespArr['account_details'],$dataname);
		}
		else {
			$InsertTokenQuery = "INSERT INTO gateway_auth_tokens (dataname,extension_id,merchant_id,token,token_expiration_date,permission_granted,token_generated_date,extra_info) value('[1]',".$extensionId.",'[2]','[3]','[4]','1','[5]', '[6]')";
			mlavu_query($InsertTokenQuery,$dataname,$oauthrespArr['merchant_id'],$oauthrespArr['access_token'],$oauthrespArr['expires_at'],$curdate, $oauthrespArr['account_details']);
		}
		return $rows['location_id'];
	}

   /* private function insertCredential($dataname, $token, $type, $extentionId)
	{
		$insertResult = mlavu_query("INSERT INTO auth_tokens (`dataname`, `extension_id`, `token`, `token_type`, `_deleted`) VALUES ('[1]', '[2]', '[3]', '[4]', 1)", $dataname, $extentionId, $token, $type);
		return $insertResult;
	}*/

  /*  private function updateCredential($dataname, $token, $type, $extentionId)
	{
		$updateResult = mlavu_query("UPDATE auth_tokens SET token = '[1]'  WHERE dataname = '[2]' and token_type = '[3]' and extension_id = '[4]'", $token, $dataname, $type, $extentionId);
		return $updateResult;
	}*/

	public function updateAccessToken($dataname, $accessToken, $extensionId)
	{
		$currentDate = gmdate("Y-m-d H:i:s");
		$expireDate = gmdate("Y-m-d\TH:i:s\Z", strtotime('30 days ', time()));
		if ($dataname != '' && $accessToken != '' && $extensionId != '') {
			$updateResult = mlavu_query("UPDATE `gateway_auth_tokens` SET `token` = '[1]', `token_expiration_date` = '[2]', `token_generated_date` = '[3]' WHERE dataname = '[4]' AND extension_id = '[5]' AND _deleted = 0", $accessToken, $expireDate, $currentDate, $dataname, $extensionId);
			return $updateResult;

		}
	}

	/*
	 * Used to get all token for a given time period
	 *
	 */
	public function getTokensForRenewal($day) {
		$extensionId = $this->getExtensionID();
		$tokenQry = "SELECT `dataname`, `extension_id`, `token` FROM gateway_auth_tokens WHERE dataname NOT IN ('POSLAVU_SQUARE_CREDENTIALS', 'POSLAVU_QA_SQUARE_CREDENTIALS') AND extension_id='[1]' AND token_generated_date  < (NOW() - INTERVAL ".$day." DAY) AND _deleted = 0";
		$tokens = mlavu_query($tokenQry, $extensionId);
		while($row = mysqli_fetch_assoc($tokens)){
			$renewalTokens[] =  $row;
		}
		return $renewalTokens;
	}

	//get transactions with in the time period
	public function getSquareTransactionsByDatePeriod($data_name, $beginTime, $endTime)
	{
		$check_transactions = lavu_query("SELECT * FROM `cc_transactions` WHERE  (lower(gateway) = 'square' or lower(pay_type)='square') and transtype='Sale' and datetime between '[1]' and '[2]' order by datetime desc",$beginTime,$endTime);
		$tx_info = array();
		if (mysqli_num_rows($check_transactions) > 0) {
			while($row = mysqli_fetch_assoc($check_transactions)){
				$tx_info[] =$row;
			}
		}
		return $tx_info;
	}

	//get transactions with given transaction id
	public function getSquareTransactionsByTransId($transaction_id)
	{
		$check_transactions = lavu_query("SELECT * FROM `cc_transactions` WHERE  (lower(gateway) = 'square' or lower(pay_type)='square') and transtype='Sale' and transaction_id = '[1]'", $transaction_id);
		//print_r($check_transactions);
		$tx_info = array();
		if (mysqli_num_rows($check_transactions) > 0) {
			while($row = mysqli_fetch_assoc($check_transactions)){
				$tx_info[] =$row;
			}
		}
		return $tx_info;
	}
	public function checkTenderIdExist($tenderid, $refundid)
	{
		$check_transactions = lavu_query("SELECT  `transaction_id`, amount FROM `cc_transactions` WHERE  gateway = 'square' and split_tender_id='[1]' and transtype in ('Return', 'GiftCardReturn') and record_no='[2]'", $tenderid, $refundid);
		if (mysqli_num_rows($check_transactions) > 0) {
			$data = mysqli_fetch_assoc($check_transactions);
			return $data['amount']; //If exist return amount
		}
		else {
			return 0;//return 0 if does not exist
		}
	}

	/**
	 * @param $orderId
	 * @param $check
	 * @return transaction details as array
	 */
	public function getSquareSaleTransaction($orderId, $check)
	{
		global $data_name;
		$ldb = "`poslavu_".$data_name."_db`.";
		$check_transactions = lavu_query("SELECT  * FROM " . $ldb . "`cc_transactions` WHERE  `order_id` = '[1]' and `check` = '[2]' and `gateway` = 'square' and `transtype` in ('Sale', 'GiftCardSale') and `action` = 'Sale'", $orderId, $check);
		$transactionInfo = array();
		while($row = mysqli_fetch_assoc($check_transactions)){
			$transactionInfo[] =  $row;
		}
		return $transactionInfo;
	}

	public function getEnvironmentSetting($dataname) {
		global $data_name;
		$data_name = $dataname;
		$query = lavu_query("SELECT `setting`, `value` FROM `config` WHERE `setting` = 'enable_square_qa_mode'");
		$squareDebugResult = mysqli_fetch_assoc($query);
		$location_info[$squareDebugResult['setting']] = $squareDebugResult['value'];
		return $location_info;
	}

	public function syncTransaction($transaction_vars, $data_name)
	{
		global $data_name;
		require_once(dirname(__FILE__)."/../../../lib/AuditLog.php");
		$audit = new AuditLog();
		$ldb = "`poslavu_".$data_name."_db`.";
		if (isset($transaction_vars['transtype']) && ( $transaction_vars['transtype'] == 'Return' || $transaction_vars['transtype'] == 'GiftCardReturn') ) {
			$q_fields = "";
			$q_values = "";
			$keys = array_keys($transaction_vars);
			foreach ($keys as $key) {
				if ($q_fields != "") { $q_fields .= ", "; $q_values .= ", "; }
				$q_fields .= "`$key`";
				$q_values .= "'[$key]'";
			}

			lavu_query("INSERT INTO " . $ldb . "`cc_transactions` ($q_fields) VALUES ($q_values)", $transaction_vars);

			/**** audit log orders****/
			$paramsArray = array('id'=>$transaction_vars['order_id'], 'id_type'=>'order_id', 'table_name'=>'orders', 'user_id'=>'system', 'updated_for'=>$transaction_vars['extra_info']);
			$audit->doAudit($data_name,$paramsArray);
			/*******/
			$updateStr = (in_array($transaction_vars['pay_type'], $this->cardTenderTypes['tender_type'])) ? "card_gratuity = card_gratuity - '[tip_amount]', card_paid = card_paid - '[amount]', `refunded_cc` = refunded_cc + '[amount]'" : "cash_paid = cash_paid - '[amount]', cash_applied = cash_applied - '[amount]', `refunded` = refunded + '[amount]'";
			lavu_query("UPDATE " . $ldb . "`orders` SET ".$updateStr.", `last_mod_ts` = '[last_mod_ts]', `pushed_ts` = '[last_mod_ts]' WHERE `order_id` = '[order_id]'", $transaction_vars);

		} else {
			/****dump row to audit log cc trans****/
			$paramsArray = array('id'=>$transaction_vars['transaction_id'],'id_type'=>'transaction_id','table_name'=>'cc_transactions','user_id'=>'system','updated_for'=>$transaction_vars['extra_info']);
			$wherecondition = "id='".$transaction_vars['id']."'";
			$audit->doAudit($data_name,$paramsArray,$wherecondition);
			/*******/
			$updatestr = ($transaction_vars['tip_amount'] > 0) ? "tip_amount = '[tip_amount]'," : "";

			lavu_query("UPDATE " . $ldb . "`cc_transactions` SET ".$updatestr." amount = '[amount]', card_desc = '[card_desc]', card_type = '[card_type]', pay_type = '[pay_type]', transtype = '[transtype]', total_collected = '[total_collected]', datetime = '[datetime]', extra_info = '[extra_info]', `got_response` = '1', `last_mod_ts` = '[last_mod_ts]', `transaction_id` = '[transaction_id]', `auth_code` = '[auth_code]', `preauth_id` = '[preauth_id]', `split_tender_id` = '[split_tender_id]' WHERE `id` = '[id]'", $transaction_vars);

			$checksquery = lavu_query("select sum(amount) as amount, sum(tip_amount) as tip_amount from " . $ldb . "`cc_transactions` WHERE `order_id` = '[order_id]' and transtype='Sale'", $transaction_vars);
			$checksdata = mysqli_fetch_assoc($checksquery);
			$transaction_vars['amount'] = $checksdata['amount'];
			$transaction_vars['tip_amount'] = $checksdata['tip_amount'];
			/****dump row to audit log orders****/
			$paramsArray = array('id'=>$transaction_vars['order_id'],'id_type'=>'order_id','table_name'=>'orders','user_id'=>'system','updated_for'=>$transaction_vars['extra_info']);
			$audit->doAudit($data_name,$paramsArray);
			/*******/

			$updateStr = (in_array($transaction_vars['pay_type'], $this->cardTenderTypes['tender_type'])) ? "card_paid = '[amount]', card_gratuity = '[tip_amount]',": "cash_paid = '[amount]', cash_tip = '[tip_amount]',";

			lavu_query("UPDATE " . $ldb . "`orders` SET ".$updateStr." `last_mod_ts` = '[last_mod_ts]', `pushed_ts` = '[last_mod_ts]' WHERE `order_id` = '[order_id]'", $transaction_vars);
		}
		return 1;
	}

	public function updateOfflineTransaction($txnid,$id)
	{
		lavu_query("UPDATE `cc_transactions` SET transaction_id='[1]',auth_code='[1]' WHERE `id` = '[2]'", $txnid,$id);
	}

	public function insertSquareData($data)
	{
		if($data!="" && count($data)>0){
			$curdate = gmdate("Y-m-d H:i:s");
			//error_log("Data-271--".print_r($data,true));
			 foreach ($data as $value)
			 {
				lavu_query("INSERT INTO `gateway_transactions_temp` (`transaction_id`,`info`,`created_date`) VALUES ('[1]','[2]','[3]')",$value['id'], json_encode($value),$curdate);
			 }
		}
	}
	public function removeSquareTransactionsFromTemp()
	{
		lavu_query("delete FROM gateway_transactions_temp");
	}
	public function getSquareTransactionsFromDb()
	{
		$transQry = "SELECT `transaction_id`, `info`, `created_date` FROM gateway_transactions_temp";
		$transdata = lavu_query($transQry);
		$transactions = array();
		while($row = mysqli_fetch_assoc($transdata)){

			$transactions[] =  $row;
		}
		return $transactions;
	}
   /* function getTransactionDetailsByTxnIdandType($txnid,$transtype){
		$transactions = lavu_query("SELECT * FROM `cc_transactions` WHERE  transtype='[1]' and transaction_id='[2]'order by datetime desc",$transtype,$txnid);
		$tx_info = array();
		if (mysqli_num_rows($transactions) > 0) {
			$tx_info = mysqli_fetch_assoc($transactions);
		}
		return $tx_info;
	}*/

	/**
	 * @param $merchant_id location merchant id provide by square
	 * @param $location_id square location id for given merchant id
	 * @return array|null
	 */
	public function getLocationName($merchant_id, $location_id) {
		$locationResult = array();
		if ($merchant_id != '' && $location_id != '') {
			$squareQuery = "SELECT `dataname`, `token` FROM `gateway_auth_tokens` WHERE `merchant_id` = '".$merchant_id."' AND `location_id` = '".$location_id."' AND `_deleted` = 0";
			$squareResult = mlavu_query($squareQuery);
			$locationResult = mysqli_fetch_assoc($squareResult);
		}
	   return $locationResult;
   }

	public function insertSyncRequest($reqinfo)
	{
		if ($reqinfo['transaction_id'] != '' && $reqinfo['location_id'] != '' && $reqinfo['data_name'] != '') {
			mlavu_query("INSERT INTO `gateway_sync_request` (`data_name`,`gateway`,`transaction_id`,`location_id`,`user_id`,`status`,`created_date`) VALUES ('[data_name]','[gateway]','[transaction_id]','[location_id]','[user_id]','[status]','[created_date]')", $reqinfo);

		} else {
			error_log("Missing Location Id or Transaction Id or Location Name");
		}
	}

	/**
	 * @param $squaredata square api response
	 * @param $data_name location name
	 * @param bool $reconcile true - reconcile from sync table, false - reconcile from webhook
	 */
	public function processTransaction($v1squaredata, $v2squaredata, $dataname, $reconcile = true) {
		global $data_name;
		if(!empty($v2squaredata)) {
			$data = array();
			$conversionRate = 1;
			if (isset($v1squaredata['itemizations'][0]['notes'])) {
				$notes = explode(",", $v1squaredata['itemizations'][0]['notes']);
				$orderArr = explode('#', $notes[0]);
				$data['order_id'] = trim($orderArr[1]);
				$data['check'] = $notes[1];
				$mode = $notes[2];
				$currencyCode = $v1squaredata['itemizations'][0]['total_money']['currency_code'];
				$currencyCode = ($currencyCode != '' )? $currencyCode : "USD";
				$conversionDetails = $this->getSquareConversionRate();
				$conversionRate = (isset($conversionDetails[$currencyCode])) ? $conversionDetails[$currencyCode] : 100;
			}

			$data_name = $dataname;
			$location_id = $v2squaredata['location_id'];
			$transaction_id = $v2squaredata['id'];
			$data['transaction_id'] = $transaction_id;
			$tenders = $v2squaredata['tenders'];
			$transactionCount = count($tenders);
			$successCount = 0;
			if ($transactionCount > 0) {
				if ($mode == 'offline' && !$reconcile) {
					$datetime = date('Y-m-d H:i:s');
					$reqinfo = array('data_name' => $data_name, 'gateway' => 'square', 'transaction_id' => $transaction_id, 'location_id' => $location_id, 'user_id' => 0, 'status' => 'pending', 'created_date' => $datetime);
					$this->insertSyncRequest($reqinfo);
					return 'pending';
				} else {
					if ($data['order_id'] == '' && $data['check'] == '') {
						error_log($data_name . ">>>> Order id and check notes is not found in trnsaction details of transaction id " . $transaction_id);
						return 'failed';
					}

					$res_transactions = $this->getSquareSaleTransaction($data['order_id'], $data['check']);

					if (empty($res_transactions) && $mode != 'offline') {
						error_log($data_name . ">>>> No records found in cc_transaction table for transaction id " . $transaction_id);
						return 'failed';
					}
					$row = 0;
					foreach ($res_transactions as $transactionDetails) {
						//If api does not give response of sale transaction that time split_tender_id do not get insert in DB, then
						// we will use transaction id
						if ($transactionDetails['split_tender_id'] != '') {
							$existOrderDetails[$transactionDetails['split_tender_id']]['amount'] = ($transactionDetails['amount'] + $transactionDetails['tip_amount']) * $conversionRate;
							$existOrderDetails[$transactionDetails['split_tender_id']]['sale_amount'] = $transactionDetails['amount'] * $conversionRate;
							$existOrderDetails[$transactionDetails['split_tender_id']]['tip_amount'] = $transactionDetails['tip_amount'] * $conversionRate;
							$existOrderDetails[$transactionDetails['split_tender_id']]['card_desc'] = $transactionDetails['card_desc'];
							$existOrderDetails[$transactionDetails['split_tender_id']]['card_type'] = $transactionDetails['card_type'];
							$existOrderDetails[$transactionDetails['split_tender_id']]['pay_type'] = $transactionDetails['pay_type'];
							if ($mode == 'offline') {
								$existOrderDetails[$row][$transactionDetails['transaction_id']]['tid'] = $transactionDetails['id'];
							} else {
								$existOrderDetails[$transactionDetails['split_tender_id']]['tid'] = $transactionDetails['id'];
							}
						} else {
							$existOrderDetails[$row][$transactionDetails['transaction_id']]['amount'] = ($transactionDetails['amount'] + $transactionDetails['tip_amount']) * $conversionRate;
							$existOrderDetails[$row][$transactionDetails['transaction_id']]['sale_amount'] = $transactionDetails['amount'] * $conversionRate;
							$existOrderDetails[$row][$transactionDetails['transaction_id']]['tip_amount'] = $transactionDetails['tip_amount'] * $conversionRate;
							$existOrderDetails[$row][$transactionDetails['transaction_id']]['card_desc'] = $transactionDetails['card_desc'];
							$existOrderDetails[$row][$transactionDetails['transaction_id']]['card_type'] = $transactionDetails['card_type'];
							$existOrderDetails[$row][$transactionDetails['transaction_id']]['pay_type'] = $transactionDetails['pay_type'];
							$existOrderDetails[$row][$transactionDetails['transaction_id']]['tid'] = $transactionDetails['id'];
						}
						$row++;
					}
					$ioid = $res_transactions[0]['ioid'];
					$register = $res_transactions[0]['register'];
					$register_name = $res_transactions[0]['register_name'];
					$server_name = $res_transactions[0]['server_name'];
					$server_id = $res_transactions[0]['server_id'];
					$offlineFlag = false;
					if (empty($res_transactions)) {
						return 'pending';
					} else {
						if (preg_match("/\bctid-\b/i", $res_transactions[0]['transaction_id'])) {
							$offlineFlag = true;
							$clientId = $res_transactions[0]['transaction_id'];
						}
					}
					$loop = 0;
					foreach ($tenders as $tender) {
						$totalMoney = $tender['amount_money']['amount'];
						$tipAmount = 0;
						if (isset($tender['tip_money']['amount']) && $tender['tip_money']['amount'] > 0) {
							$tipAmount = $tender['tip_money']['amount'];
						}
						$amount = ($totalMoney - $tipAmount) / $conversionRate;
						$tipAmount = $tipAmount / $conversionRate;
						$totalCollected = $amount;
						$squareTenderId = $tender['id'];
						// First we check trend id array data amount if not found then use transaction id array data amount
						$existAmount = ($existOrderDetails[$squareTenderId]['amount'] != '') ? $existOrderDetails[$squareTenderId]['amount'] : $existOrderDetails[$loop][$transaction_id]['amount'];
						$existAmount = ($offlineFlag) ? 0 : round($existAmount);
						if ($totalMoney > $existAmount || $offlineFlag) {
							$card_brand = '';
							$last_4 = '';
							if (isset($this->cardTenderTypes['tender_type'][$tender['type']])) {
								$record['pay_type'] = $this->cardTenderTypes['tender_type'][$tender['type']];
								$pay_type_id = $this->cardTenderTypes['tender_id'][$tender['type']];
								$transtype = $this->cardTenderTypes['sale_trans'][$tender['type']];
								$card_brand = $tender['card_details']['card']['card_brand'];
								$last_4 = $tender['card_details']['card']['last_4'];
							}
							else {
								$record['pay_type'] = 'Cash';
								$pay_type_id = 1;
								$transtype = 'Sale';
							}
							$record['transaction_id'] = $data['transaction_id'];
							$record['auth_code'] = $data['transaction_id'];
							$record['preauth_id'] = $data['transaction_id'];
							$record['extra_info'] = ($reconcile) ? 'square-script-reconcile' : 'square-webhook-reconcile';
							// First we check trend id array data id if not found then use transaction id array data id
							$id = ($existOrderDetails[$squareTenderId]['tid'] != '') ? $existOrderDetails[$squareTenderId]['tid'] : $existOrderDetails[$loop][$transaction_id]['tid'];
							$record['id'] = ($offlineFlag) ? $existOrderDetails[$loop][$clientId]['tid'] : $id;
							$record['transtype'] = $transtype;
							$record['card_desc'] = $last_4;
							$record['card_type'] = $card_brand;
							$record['order_id'] = $data['order_id'];
							$record['check'] = $data['check'];
							$record['split_tender_id'] = $squareTenderId;
							$record['amount'] = $amount;
							$record['tip_amount'] = $tipAmount;
							$record['pay_type_id'] = $pay_type_id;
							$record['got_response'] = 1;
							$record['info'] = 'Read by Square Webhook';
							$record['loc_id'] = 1;
							$record['total_collected'] = $totalCollected;
							$record['record_no'] = $squareTenderId;
							$record['datetime'] = date('Y-m-d H:i:s', strtotime($tender['created_at']));
							$record['last_mod_ts'] = time();
							$record['server_time'] = UTCDateTime(TRUE);
							if ($this->syncTransaction($record, $data_name)) {
								$successCount ++;
							}
						}
						if ($totalMoney == $existAmount) {
							error_log($data_name . ">>>> Nothing to Update for Sale Exist amount " . $totalMoney . "==========New amount " . $existAmount);
							$successCount ++;
						}
						$loop++;
					}
				}
			}

			// Process for refund transaction
			if(isset($v2squaredata['refunds']) && count($v2squaredata['refunds']) > 0)
			{
				if ($offlineFlag) {
					$res_transactions = $this->getSquareSaleTransaction($data['order_id'], $data['check']);
					foreach ($res_transactions as $transactionDetails) {
						$existOrderDetails[$transactionDetails['split_tender_id']]['amount'] = ($transactionDetails['amount'] + $transactionDetails['tip_amount']) * $conversionRate;
						$existOrderDetails[$transactionDetails['split_tender_id']]['sale_amount'] = $transactionDetails['amount'] * $conversionRate;
						$existOrderDetails[$transactionDetails['split_tender_id']]['tip_amount'] = $transactionDetails['tip_amount'] * $conversionRate;
						$existOrderDetails[$transactionDetails['split_tender_id']]['card_desc'] = $transactionDetails['card_desc'];
						$existOrderDetails[$transactionDetails['split_tender_id']]['card_type'] = $transactionDetails['card_type'];
						$existOrderDetails[$transactionDetails['split_tender_id']]['pay_type'] = $transactionDetails['pay_type'];
					}
				}
				$refunds =  $v2squaredata['refunds'];
				$refundedAmount = 0;
				foreach($refunds as $refund) {
					$squareTenderId = $refund['tender_id'];
					$refundid = $refund['id'];
					$createdAt = gmdate('Y-m-d H:i:s', strtotime($refund['created_at']));
					$exist = $this->checkTenderIdExist($squareTenderId, $refundid); // 1=exists 0=not exist
					$refundedAmount = $refundedAmount + ($exist * $conversionRate);
					if ($exist == 0) {
						if (isset($this->cardTenderTypes['tender_type'][$tender['type']])) {
							$record['pay_type'] = $this->cardTenderTypes['tender_type'][$tender['type']];
							$transtype = $this->cardTenderTypes['return_trans'][$tender['type']];
							$pay_type_id = $this->cardTenderTypes['tender_id'][$tender['type']];
						}
						else {
							$data['pay_type'] = 'Cash';
							$pay_type_id = 1;
							$transtype = 'Return';
						}
						$refundamount = $refund['amount_money']['amount'];
						$saleAmount = $existOrderDetails[$squareTenderId]['sale_amount'];
						$tipAmount = $existOrderDetails[$squareTenderId]['tip_amount'];
						$remainingRefund = $saleAmount - $refundedAmount;
						$refundTipAmount = 0;
						if ($remainingRefund >= $refundamount) {
							$refundamount = $refundamount / $conversionRate;
						} else {
							$refundTipAmount = ($refundamount >= $tipAmount) ? $tipAmount : $refundamount;
							$refundTipAmount = $refundTipAmount / $conversionRate;
							$refundamount = ($refundamount > $tipAmount) ? ($refundamount - $tipAmount) : 0;
							$refundamount = $refundamount / $conversionRate;
						}
						$data['id'] = '';
						$data['card_desc'] = $existOrderDetails[$squareTenderId]['card_desc'];
						$data['card_type'] = $existOrderDetails[$squareTenderId]['card_type'];
						$data['pay_type'] = $existOrderDetails[$squareTenderId]['pay_type'];
						$data['split_tender_id'] = $squareTenderId;
						$data['amount'] = $refundamount;
						$data['tip_amount'] = $refundTipAmount;
						$data['total_collected'] = $refundamount;
						$data['refunded']=1;
						$data['loc_id'] = 1;
						$data['action'] = 'Refund';
						$data['rf_id'] = $location_id;
						$data['pay_type_id'] = $pay_type_id;
						$data['gateway'] = 'Square';
						$data['register'] = $register;
						$data['register_name'] = $register_name;
						$data['server_name'] = $server_name;
						$data['server_id'] = $server_id;
						$data['got_response'] = 1;
						$data['info'] = 'Read by Square Webhook';
						$data['reader'] = ($reconcile) ? 'Script' : 'Webhook';
						$data['transtype'] = $transtype;
						$data['datetime'] = $createdAt;
						$data['last_mod_ts'] = time();
						$data['server_time']= UTCDateTime(TRUE);
						$data['refund_pnref'] = $refundid;
						$data['refund_notes'] = $refund['reason'];
						$data['record_no'] = $refundid;
						$data['ioid'] = $ioid;
						$data['extra_info'] = ($reconcile) ? 'square-script-reconcile' : 'square-webhook-reconcile';
						$data['auth_code'] = $data['transaction_id'];
						$data['preauth_id'] = $data['transaction_id'];
						if ($this->syncTransaction($data, $data_name)) {
							$successCount ++;
						}
					}
				}
			}
			if ($successCount > 0) {
				return 'done';
			}
		} else {
			return 'pending';
		}
	}

	/**
	 * This method is used to get lowest denomination conversion rate with country code
	 * @return array
	 */
	function getSquareConversionRate() {
		//TODO:- We will be removed and make it dynamic
		return array('JPY' => 1);
	}
}


