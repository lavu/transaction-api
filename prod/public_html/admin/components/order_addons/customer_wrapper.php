<?php
	require_once(dirname(__FILE__) . "/../comconnect.php"); 
	require_once(dirname(__FILE__) . "/customer_functions.php");
	$tab_name = reqvar("tab_name");
	$parent_mode = reqvar("parent_mode");
	$parent_mode_vars = reqvar("parent_mode_vars");
?>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<title>Customer Search Overlay Wrapper</title>

		<style type="text/css">
<!--


body, div {
    -webkit-touch-callout: none;
    -webkit-user-select: none;
}

body {
	background-color: transparent;
	margin-left: 0px;
	margin-top: 0px;
	margin-right: 0px;
	margin-bottom: 0px;
	-webkit-tap-highlight-color:rgba(0,0,0,0);
}

.style4 {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 14px;
	font-weight: bold;
	color: #8896AD;
}
.style6 {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 24px;
	font-weight: bold;
	color: #8896AD;
}
.style8 {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 15px;
	font-weight: bold;
	color: #07142a;
}
-->
		</style>
		
	
<?php

	$bg = "images/win_overlay_2btn.png";

	$extra_vars .= "&parent_mode=".$parent_mode . "&parent_mode_vars=".$parent_mode_vars;

	$cm = reqvar("cm"); // customer modifer??
	if ($cm) {
		$cancel_cmd = "cancel";
		$extra_vars .= "&cm=1";
	} else {
		$cancel_cmd = "close_overlay";
	}
	
?>	
		
<script type="text/javascript">
	function doSearch(){
	   
	    if(nav_performing_block_buttons){
    	    return;
	    }
	    blur_from_searchbox();
	    //alert('doing search');
	    setLoadingHidden(false);
		setval = document.getElementById("search_text_id").value; 
		nav_performing_block_buttons = true;
        //setTimeout(function(){  nav_performing_block_buttons = false;}, 2000);
        //setTimeout(function(){  unlightAllButtons()}, 3000);
		if(setval!="SEARCH" && setval!="") {
		    window.location = "_DO:cmd=send_js&c_name=customer&js=setBlockNavigations(true);";
		    setTimeout(function(){
    		              run_com_command("customer","mode=search<?=$extra_vars?>&searchterm=" + setval + "&abc=123&mode=do_search");
    		           }, 200);
			
		}
		
		return false;
	}
	function run_com_command(comname,vars) {
		window.location = "_DO:cmd=load_url&c_name=" + comname + "&f_name=order_addons/" + comname + ".php?" + vars;
	}
	function run_cmd(str) {
		alert('wrapper cmd: ' + str);
	}

	function side_bar_letter_pressed(index){
	    //Where index 0 is A, index 1 is B etc.
	    //alert("You pushed: " + String.fromCharCode('A'.charCodeAt(0) + index) + " telling other script...");
	    //window.location = "_DO:cmd=send_js&c_name=" + comname + "&js=run_cmd('" + varstr + "')";
	    window.location = "_DO:cmd=send_js&c_name=customer&js=letter_key_pressed_in_wrapper('" + String.fromCharCode('A'.charCodeAt(0) + index) + "');";
	}
	 
    function set_side_bar_letters_visible(isVisible){
        var msg = isVisible ? 'true' : 'false';
        var letters_side_table = document.getElementById('letters_table');
        if(isVisible){
            letters_side_table.style.display = 'block';
        }else{
            letters_side_table.style.display = 'none';
        }
    }
    function exit_overlay(){
        
        window.location = "_DO:cmd=<?php echo $cancel_cmd; ?>";
    }
    //"close_overlay"
    //margin-left: -6px;
    
    function set_display_searchCustomersCreateACustomer_image(isDisplayed){
        var searchOrCreateDiv = document.getElementById('searchOrCreateCustomerBanner_div');
        if(isDisplayed){
            searchOrCreateDiv.style.display = 'block';
        }else{
            searchOrCreateDiv.style.display = 'none';
        }
    }
    function go_to_add_customer(){
        nav_performing_block_buttons = true;
        //window.location = "_DO:cmd=send_js&c_name=customer&js=go_to_add_customer_nav();";
				window.location = "_DO:cmd=send_js&c_name=customer&js=plus_button_navigate();";
    }
    
    function blur_from_searchbox(){
        var searchInputField = document.getElementById('search_text_id');
        searchInputField.blur();
    }
    
    function preload_images(){
    	preload_image = new Image();
    	preload_image.src = 'images/close_X.png';
    	preload_image.src = 'images/close_X_lit.png';
    	preload_image.src = 'images/grey_plus.png';
    	preload_image.src = 'images/grey_plus_lit.png';
    	preload_image.src = 'images/search_icon.png';
    	preload_image.src = 'images/search_icon_lit.png';
    	
    	//HERE Switch all the images on the components then back again immediately.
    	
	}
	var is_blocking_button_touches;
	function disableButtons(){
    	is_blocking_button_touches = true;
	}
	function enableButtons(){
    	is_blocking_button_touches = false;
	}
	
	function lightAllButtons(){
        var closeButton = document.getElementById('close_button_id');
        //closeButton.style.backgroundImage = "url(images/close_X_lit.png)";
        closeButton.src = 'images/close_X_lit.png';
        
        var searchButton = document.getElementById('search_button_id');
        //searchButton.style.backgroundImage = "url(images/search_icon_lit.png)";
        searchButton.src = 'images/search_icon_lit.png';
        
        var addButton = document.getElementById('add_customer_button_id');
        //addButton.style.backgroundImage = "url(images/grey_plus_lit.png)";
        addButton.src = 'images/grey_plus_lit.png';
	
        //setTimeout(function(){  nav_performing_block_buttons = false;}, 2000);
	}
	function unlightAllButtons(){
        var closeButton = document.getElementById('close_button_id');
        //closeButton.style.backgroundImage = "url(images/close_X.png)";
        closeButton.src = 'images/close_X.png';
        
        var searchButton = document.getElementById('search_button_id');
        //searchButton.style.backgroundImage = "url(images/search_icon.png)";
        searchButton.src = 'images/search_icon.png';
        
        var addButton = document.getElementById('add_customer_button_id');
        //addButton.style.backgroundImage = "url(images/grey_plus.png)";
        addButton.src = 'images/grey_plus.png';
	}
	
	function unlockButtonsDelayed(){
	    setTimeout(function(){ unlightAllButtons();}, 200);
    	setTimeout(function(){ nav_performing_block_buttons = false;}, 200);
	}
	
	var is_loading_label_hidden = true;
	var is_loading_timer_interval;
	var loading_label_tick_count = 0;
	function tickLoadingLabel(){
    	var myMod = loading_label_tick_count++ % 4;
    	var loadingLabelParagraph = document.getElementById('loading_paragraph_id');
    	var dots;
    	if(myMod == 0){
        	dots = '.';
    	}else if(myMod == 1){
        	dots = '..';
    	}else if(myMod == 2){
        	dots = '...';
    	}else if(myMod == 3){
        	dots = '';
    	}
    	loadingLabelParagraph.innerHTML = 'loading' + dots;
	}
	function setLoadingHidden(isHidden){
	    loading_label_tick_count = 0;
	    if(is_loading_label_hidden === isHidden){ return; }
    	is_loading_label_hidden = isHidden;
    	if(!is_loading_label_hidden){
        	is_loading_timer_interval = this.setInterval(function(){tickLoadingLabel();}, 400);
    	}else{
    	    var loadingLabelParagraph = document.getElementById('loading_paragraph_id');
        	window.clearInterval(is_loading_timer_interval);
        	loadingLabelParagraph.innerHTML = '&nbsp;';
    	}
	}
	
	
</script>
	</head>
	<body>
       <div tabindex="1" id='customer_overlay_wrapper_div' class=non_selectable style='width:450px;height:700px;border-radius:30px; background-color:#EEE;'>
            <div id='customer_search_background_div' class=non_selectable style='background-color: rgba(255,0,0,0); padding: 10px 0px 0px 0px;width:80%;margin: 0 auto;'>   
                <form name="submittingForm" method="post" action="" onSubmit="blur_from_searchbox(); return doSearch();">
                    <table style='border: 0px solid black; margin-top: -10px;' cellspacing="3px">
                        <td>
                            <!- CLOSE BUTTON ->
                            <!--
                            <input id='close_button_id' type='button' style='width:22px; height:22px; background: url(images/close_X.png) no-repeat; background-size:100%;border: 0px solid gray; margin-left:-28px; margin-top: -18px' />
                            -->
                            <?php
                                $elemStr = "<img id='close_button_id' />";
                                echo create_padded_touch_area($elemStr, 'close_button_div_id', -32, -3, 35, 35);
                            ?>
                        </td>
                        <td>
                            <!-  SEARCH TEXT INPUTs WRAPPING IMAGE DIV  ->
                            <div id='search_text_wrapping_div_id' style='background: url(images/search_box.png) no-repeat; background-size:90%; height:50px; width:365px; border: 0px solid gray; margin-top: 20px'>
                                <table>
                                    <tr>
                                        <td>
                                            <!-  SEARCH TEXT INPUT  ->
                                            <input id='search_text_id' name='search_input_field' type='text' placeholder='<?php echo speak("customer search"); ?>' onSubmit='doSearch();'  style='width:260px; height:23px; margin-top: -5px; margin-left: 0px; font-size:16px; background-color: rgba(0,0,0,0.0); border:0px'/>
                                        </td>
                                        <td valign='top' align='left'>
                                            <!-  DO SEARCH BUTTON  (magnifying glass) ->
                                            &nbsp;
                                            <?php
                                            $elemStr = "<img src='images/search_icon.png' id='search_button_id' style='margin-top:2px;'/>";
                                            echo create_padded_touch_area($elemStr, 'search_button_div_id', 24, -21, 35, 35);
                                            ?>
                                        </td>
                                        <td>
                                            <!- ADD CUSTOMER BUTTON ->
                                            <?php
                                            $elemStr = "<img id='add_customer_button_id' />";
                                            echo create_padded_touch_area($elemStr, 'add_customer_button_div_id',62,-11,35,35);
                                            ?>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </td>
                    </table>
                </form>    
            </div>
            

            <!- Search Customers / Create A Customer Banner (display is toggled with js function: set_display_searchCustomersCreateACustomer_image(isDisplayed); ) ->
            
            <!--
            <div id='searchOrCreateCustomerBanner_div' style='width:100%; height:106px; background: url(images/searchorcreate.png) no-repeat; background-size:100%; background-color:#AFD;'></div>
            -->
            
            <div style='background-color: rgba(0,0,255,0); height:550px; width:415px; display:inline-block;'></div>

        <!--
            //  element.addEventListener('TouchMove', func | EventListener );
            //  
            //
            //
        -->
            <div id='letters_table_wrapper' style='margin-top: -13px; margin-left: 1px; background-color: rgba(0,255,255,0); height:575px; width:29px; display:inline-block;' ontouchmove='handleTouch(event);'>
                <table style='margin-left:2px;' id='letters_table'>
                <tbody>
                <?php
                    //ontouchmove, ontouchstart, ontouchend, (ontouchcancel)
                     $j=-1;
                     for($i = ord('A'); $i <= ord('Z'); $i++){
                        $j++; $q = strlen($j) == 1 ? '0'.$j : $j;
                        echo "<tr><td id='letter_td_$j'><div style='font-size:14px;text-align:center;margin-left:3px;' >".chr($i)."</div></td></tr>"; 
                     }
                ?>
                </tbody>
                </table>
            </div>
            
            <!-- The bottom 'loading...' label-->
            <div style="text-align:center; margin-top:-42px">
                <div style='width:100px;margin:0 auto;'>
                    <div style='margin-left:-15px'>
                        <p id='loading_paragraph_id' style='text-align:left; font-weight:bold; font-size:32px; color:#DDD; font-family:"Verdana" text-align:left'>
                            
                        </p>
                    </div>
                </div>
            </div>
       </div>

	</body>
	
    <script type='text/javascript'>

    
        function style_button_press_state(elem, isPressed){
            if(   (elem.id == 'search_button_id' || elem.id == 'search_button_div_id')&& document.getElementById("search_text_id").value == ''){
                return;
            } 
            //alert(elem.id);
            var imageName;
            var elementToChange = document.getElementById( elem.id.replace("_div", "") );
            if(elementToChange.id == 'close_button_id'){        imageName = isPressed ? 'images/close_X_lit.png'     : 'images/close_X.png'; }
            if(elementToChange.id == 'search_button_id'){       imageName = isPressed ? 'images/search_icon_lit.png' : 'images/search_icon.png'; }
            if(elementToChange.id == 'add_customer_button_id'){ imageName = isPressed ? 'images/grey_plus_lit.png'   : 'images/grey_plus.png'; }
            if(isPressed == false){
                //imageName = "url(" + imageName + ")";
                //elem.style.backgroundImage = imageName;
                elementToChange.src = imageName;
            }else{
                //imageName = "url(" + imageName + ")";
                //elem.style.backgroundImage = imageName;
                elementToChange.src = imageName;
            }
        }
        
        var nav_performing_block_buttons = false;
        function style_button_press_start(event){

            //Handle the actual event
            if(!nav_performing_block_buttons){
                //nav_performing_block_buttons = true;
                style_button_press_state(this, true);
                
                if(this.id == 'add_customer_button_id'   || this.id == 'add_customer_button_div_id'){
                    go_to_add_customer();
                }
                else if( this.id == 'close_button_id'    || this.id == 'close_button_div_id'){
                    exit_overlay();
                }
                else if( (this.id == 'search_button_id'  || this.id == 'search_button_div_id') && document.getElementById("search_text_id").value != '' ){
                    doSearch();
                }
            }
        }
        function style_button_press_end(event){
            style_button_press_state(this, false); 
        }
        //Button handlers
        var closeButton = document.getElementById('close_button_id');
        closeButton.addEventListener('touchstart', style_button_press_start);
        closeButton.addEventListener('touchmove' , style_button_press_start);
        var searchButton = document.getElementById('search_button_id');
        searchButton.addEventListener('touchstart', style_button_press_start);
        searchButton.addEventListener('touchmove' , style_button_press_start);
        var addButton = document.getElementById('add_customer_button_id');
        addButton.addEventListener('touchstart', style_button_press_start);
        addButton.addEventListener('touchmove' , style_button_press_start);
        //Button padding div handlers
        var closeButtonDiv = document.getElementById('close_button_div_id');
        closeButtonDiv.addEventListener('touchstart', style_button_press_start);
        closeButtonDiv.addEventListener('touchmove' , style_button_press_start);
        var searchButtonDiv = document.getElementById('search_button_div_id');
        searchButtonDiv.addEventListener('touchstart', style_button_press_start);
        searchButtonDiv.addEventListener('touchmove' , style_button_press_start);
        var addButtonDiv = document.getElementById('add_customer_button_div_id');
        addButtonDiv.addEventListener('touchstart', style_button_press_start);
        addButtonDiv.addEventListener('touchmove' , style_button_press_start);
        
    </script>

	
	
	<script type='text/javascript'>
	   //var letters_side_table = document.getElementById('letters_table');
	   //letters_side_table.style.display = 'none';
	   set_side_bar_letters_visible(false);
	   
	   function handle_letter_table_wrapper_touch(event){
    	   var touches = event.touches;
    	   var touch = touches[0];
    	   var y = touch.clientY - this.offsetTop;
    	   y /= this.offsetHeight;
    	   var letter = Math.min(Math.round(y * 26), 25);
    	   side_bar_letter_pressed(letter) ;
    	   return false;
	   }
	   var lettersWrapper = document.getElementById('letters_table_wrapper');
	   lettersWrapper.addEventListener('touchmove',  handle_letter_table_wrapper_touch);
	   lettersWrapper.addEventListener('touchstart', handle_letter_table_wrapper_touch);
	   lettersWrapper.addEventListener('touchend',   handle_letter_table_wrapper_touch);
	   
	   preload_images();
	   setTimeout(function(){
	       lightAllButtons();
	       setTimeout(function(){
    	       unlightAllButtons();
	       },1);
	   }
	   , 20);
    </script>
</html>
<?php
    checkAutoSearchFocusAndSetJS();
