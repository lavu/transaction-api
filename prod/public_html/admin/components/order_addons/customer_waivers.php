<?php

	//Predefined variables.
	$customer_id;//Defined in mode=waiver of customer.php
	$customer_row;
	$mostCurrentValidWaiverRow;//Set in customer_functions--setGlobalVariablesForWhetherUsingWaivers(...) called at top of customer.php
	$locationRow;

	//Globals.
	//Not checking the inode anymore, we check the date signature
	$signature_img_path = dirname(__FILE__).'/../lavukart/companies/'.$dataname.'/signatures/signature_'.$customer_id.'.jpg';
	$signature_exists = file_exists($signature_img_path);

	//Whether to allow or not the signing image.
	echo "<script type='text/javascript'>var enable_signature_do_command = true;</script>";

	//We get the waiver version that the user signed, which is probably just $mostCurrentValidWaiverRow.
	$waiverExpiration = $customer_row['waiver_expiration'];// e.g. 2014-03-26 00:00:00|waiver-id:4;vers:3;
	$waiverExpirationParts = explode('|', $waiverExpiration);
	$signatureExpirationDate = count($waiverExpirationParts) > 1 ? $waiverExpirationParts[0] : false;
	preg_match("/^.*vers:([0-9]*).*$/", $waiverExpiration, $captures);

	$signedWaiverVersion = count($captures) > 1 ? $captures[1] : false;

	//localize_datetime($timestamp, $to_tz)
	$localizedDateTime = localize_datetime(date('Y-m-d H:i:s'), $locationRow['timezone']);
	//echo "LOCALIZED DATE TIME:" . $localizedDateTime;
	$waiverHasExpiredAndExists = $localizedDateTime > $signatureExpirationDate && $signature_exists;

	$field16 = $customer_row['field16'];
	//signatureExpirationParts has a |vers... after it only if signed from within the app.
	$isWaiverElectronic = !empty($field16) && substr($field16,0,1) == "e" && count($waiverExpirationParts) < 2;
	$substrStart = 0;
	$electricSignatureValue = "";
	if($isWaiverElectronic){
		$substrStart = strpos($field16, "-") + 1;
		$electricSignatureValue = substr($field16, $substrStart);
	}
	$regExpirationInfo = getRegistrationExpirationInfo();
//echo "LOC_DATETIME: ".$localizedDateTime."<br>";
//echo "SignExpDate:".$signatureExpirationDate."<br>";
//echo "Sig exists:".$signature_exists."<br>";
//echo "Loc gt expDate:". ($localizedDateTime > $signatureExpirationDate)."<br>";
	// GLOBALS NEEDED IN EACH OF THE FOLLOWING 4 FUNCTIONS:
	$mostCurrentValidWaiverRow;
	$signedWaiverVersion;
	$waiverExpiration;


	// There are 4 possibilities of display:
	// 1.) waiver_expiration is empty --func getDNECaseHTML()
	//     --display most recent version of waiver, enable_signature
	//
	// 2.) waiver_expiration has expired -- func getHasExpiredCaseHTML()
	//	   --display notice that the signature has expired, display most recent version of waiver, enable_signature
	//
	// 3.) not expired, no version info or _deleted=1 on the lk_waiver --func getNoVersionInfoOrDeletedCaseHTML()
	//	   --Display message:(not found, should resign, etc.) display most recent version of waiver, enable_signature
	//
	// 4.) version-exists-&-_deleted=0, not-expired, version is not the most recent --func getNotMostRecentVersionCaseHTML()
	//	   --Display message:(Signature is up to date, but newer waiver exists... etc.) display signed version of signature, disable_signature
	//
	// 5.) version-exists-&-_deleted=0, not-expired, version signed is the most recent --func getIsValidIsRecentCaseHTML()
	//     --Display message:(Signaure is up to date, this is the newest waiver... etc.) display newest version, enable_signature.

	//If signed waiver version equals the same as the person signed then we display that waiver.
	$displayWaiverData = "No Waiver Found.";
	$newerWaiverExists = false;//If newer waiver exists we don't allow resignature of older waiver.
	if(empty($regExpirationInfo)){
		$displayWaiverData = getTopMessageHTML("Please set up your companies registration expiration info.", "#A44");
	}else if($signatureExpirationDate === false){
		$displayWaiverData = getDNECaseHTML(); // Case 1.)
	}else if($localizedDateTime > $signatureExpirationDate){
		$displayWaiverData = getHasExpiredCaseHTML(); // Case 2.)
	}else{//We have a signature expiration that hasn't expired.
		$q = "SELECT * FROM `lk_waivers` WHERE `version`='[1]' AND `_deleted`='0' ORDER BY `id` DESC LIMIT 1";
		$result = lavu_query($q, $signedWaiverVersion);
		if(!$result){ error_log("Mysql error in ".__FILE__." -h4a- lavu_dberror:".lavu_dberror()); }
		else if(mysqli_num_rows($result)){
			$lk_waiver_row = mysqli_fetch_assoc($result);
			if($signedWaiverVersion == $mostCurrentValidWaiverRow['version']){
				$displayWaiverData = getIsValidAndMostRecentCaseHTML(); //Case 5.)
			}else{
				$displayWaiverData = getNotMostRecentVersionCaseHTML($lk_waiver_row); //Case 4.)
			}
		}else{
			$displayWaiverData = getNoVersionInfoOrDeletedCaseHTML(); // Case 3.)
		}
	}


	//GUI
	echo "<div style='width:390px; background-color:FFF; margin-left:25px; '> ";
    echo "<div style='padding:12px'>";
    //The raw data that is stored in the db.
	echo $displayWaiverData;
	echo "</div>";
	
	if($isWaiverElectronic){
			echo "<div id='signature_div' style='border:1px #aaF solid; width:390px; height:100px; position:relative;'>".
					 "<span style='position:absolute; left:15px; top:25px; font-family:Verdana; width:90%;
					 		 font-size:28px; color:#555; overflow:auto; overflow-x:scroll;'>E.Sign:$electricSignatureValue</span>";
			echo "</div><br>";
	}
	else{
		if($signature_exists){
			$borderCSS = $waiverHasExpiredAndExists ? "border:1px #Faa solid;" : "border:1px #7e7 solid";
			$urlPathToSig = '../lavukart/companies/'.$dataname.'/signatures/signature_'.$customer_id.'.jpg';
			echo "<br><div id='signature_div' style=' width:388px; height:71px; background-image: url($urlPathToSig); background-size:contain;
						background-repeat: no-repeat; $borderCSS'>";
			echo "</div><br>";
		}else{
			echo "<div id='signature_div' style='border:1px #ccc solid; width:390px; height:100px; position:relative;'>".
					 "<span style='position:absolute; left:45px; top:20px; font-family:Verdana;
					 		 font-size:38px; color:#AAA;'>Enter Signature</span>";
			echo "</div><br>";
		}
	}

	
	$doCommandForSignature = "\"_DO:cmd=update_signature&refresh_comp=customer&customer_id=$customer_id&minor_or_adult=adult?updated=1\";";
	


	//Regular signature (not electronic)
	echo "<script type='text/javascript'>".
				 "function emptySignatureTouched(elem){".
				 	//"alert('ABOUT TO DO _DO');".//-----------------------------------------------------
				 	"if(enable_signature_do_command){ ".
				 	    "window.location=$doCommandForSignature".
				 	"}".
				 	"else{".
				 		"alert('Please press waiver icon at top of page for signing of newest waiver.');".
				 	"}".
				 "}".
				 "var needSignatureDiv = document.getElementById('signature_div');".
				 //"signature_div.addEventListener('touchstart',emptySignatureTouched);".
				 "signature_div.addEventListener('click',emptySignatureTouched);".
		     "</script>";
	echo "</div>";
	


	function sanitizeWaiverStr($waiverString){
		$waiverEscaped = $waiverString;
		$waiverEscaped = str_replace("\\", "\\",     $waiverEscaped);
		$waiverEscaped = str_replace("&",  "&amp;",  $waiverEscaped);
		$waiverEscaped = str_replace("'",  "&#39;",  $waiverEscaped);
		$waiverEscaped = str_replace("\"", "&quot;", $waiverEscaped);
		$waiverEscaped = str_replace("<",  "&lt;",   $waiverEscaped);
		$waiverEscaped = str_replace(">",  "&gt;",   $waiverEscaped);
		$waiverEscaped = str_replace("\n", "<br>",   $waiverEscaped);
		return $waiverEscaped;
	}

	// There are 4 possibilities of display:
	// 1.) waiver_expiration is empty --func getDNEHeader()
	//     --display most recent version of waiver, enable_signature
	//
	// 2.) waiver_expiration has expired -- func getHasExpiredCaseHTML()
	//	   --display notice that the signature has expired, display most recent version of waiver, enable_signature
	//
	// 3.) not expired, no version info or _deleted=1 on the lk_waiver --func getNoVersionInfoOrDeletedCaseHTML()
	//	   --Display message:(not found, should resign, etc.) display most recent version of waiver, enable_signature
	//
	// 4.) version-exists-&-_deleted=0, not-expired, version is not the most recent --func getNotMostRecentVersionCaseHTML()
	//	   --Display message:(Signature is up to date, but newer waiver exists... etc.) display signed version of signature, disable_signature
	//
	// 5.) version-exists-&-_deleted=0, not-expired, version signed is the most recent --func getIsValidIsRecentCaseHTML()
	//     --Display message:(Signaure is up to date, this is the newest waiver... etc.) display newest version, enable_signature.
	
	// There are 4 possibilities of display:
	// 1.) waiver_expiration is empty --func getDNEHeader()
	//     --display most recent version of waiver, enable_signature
	function getDNECaseHTML(){
		global $mostCurrentValidWaiverRow, $signedWaiverVersion, $waiverExpiration;
		$displayHTML = sanitizeWaiverStr($mostCurrentValidWaiverRow['adult']);
		return $displayHTML;
	}

	// 2.) waiver_expiration has expired -- func getHasExpiredHeader()
	//	   --display notice that the signature has expired, display most recent version of waiver, enable_signature
	function getHasExpiredCaseHTML(){
		global $mostCurrentValidWaiverRow, $signedWaiverVersion, $waiverExpiration;
		$displayHTML  = getTopMessageHTML("The current signature has expired.", "#A44");
		$displayHTML .= sanitizeWaiverStr($mostCurrentValidWaiverRow['adult']);
		return $displayHTML;
	}

	// 3.) not expired, no version info or _deleted=1 on the lk_waiver --func getNoVersionInfoOrDeletedHeader()
	//	   --Display message:(not found, should resign, etc.) display most recent version of waiver, enable_signature
	function getNoVersionInfoOrDeletedCaseHTML(){
		global $mostCurrentValidWaiverRow, $signedWaiverVersion, $waiverExpiration;
		$displayHTML  = getTopMessageHTML("The waiver version that this customer has signed no longer exists.", "#A44");
		$displayHTML .= sanitizeWaiverStr($mostCurrentValidWaiverRow['adult']);
		return $displayHTML;
	}

	// 4.) version-exists-&-_deleted=0, not-expired, version is not the most recent --func getNotMostRecentVersionHeader()
	//	   --Display message:(Signature is up to date, but newer waiver exists... etc.) display signed version of signature, disable_signature
	function getNotMostRecentVersionCaseHTML(&$lk_waiver_row){
		global $mostCurrentValidWaiverRow, $signedWaiverVersion, $waiverExpiration;

		$displayWaiverData  = "<script type='text/javascript'>enable_signature_do_command = false;</script>";

/*
		//We display a button on top that says that the waiver is not the newest, and we disable the signature press until newer waiver is displayed.
		$displayWaiverData .= "<span style='colof:#F00'>Warning: The waiver version signed is an older waiver version, click the button below to display ".
									"the newer version for signing</span>";
*/
		$displayWaiverData .= getTopMessageHTML("There is a newer waiver version available, click icon to show.", "aaa", true);

		//Waiver info.
		$newestWaiverEscaped = str_replace("\\","\\\\", sanitizeWaiverStr($mostCurrentValidWaiverRow['adult']) );//Need to escape inline javascript.
		$waiverWhichWasSignedEscaped = sanitizeWaiverStr($lk_waiver_row['adult']);
		echo "<script type='text/javascript'> waiverData = '$newestWaiverEscaped';</script>";


		$displayWaiverData .= "<div id='display_waiver_data_div_id' style=''>".$waiverWhichWasSignedEscaped."</div>";
		return $displayWaiverData;
	}

	// 5.) version-exists-&-_deleted=0, not-expired, version signed is the most recent --func getIsValidIsRecentHeader()
	//     --Display message:(Signaure is up to date, this is the newest waiver... etc.) display newest version, enable_signature.
	function getIsValidAndMostRecentCaseHTML(){
		global $mostCurrentValidWaiverRow, $signedWaiverVersion, $waiverExpiration;
		$displayHTML  = getTopMessageHTML("The provided signature is current.", "#4A4");
		$waiverEscaped = sanitizeWaiverStr($mostCurrentValidWaiverRow['adult']);
		$displayHTML .= "<div id='display_waiver_data_div_id' style=''>".$waiverEscaped."</div>";
		return $displayHTML;
	}

	function getTopMessageHTML($message, $color, $showNewestVersionOption = false){
		$htmlBuilder = "";

		$button = "<input id='blah' type='image' src='images/doc.png' pushedimg='images/doc_dark.png' ".
		             "style='position:absolute;'".
		             "onclick='var innerDiv = document.getElementById(\"display_waiver_data_div_id\"); ".
							         "innerDiv.innerHTML=waiverData; ".
							         "enable_signature_do_command = true;".
							         "document.getElementById(\"tpmsg_div_id\").hidden = true; '/>";
		$button = $showNewestVersionOption ? $button : "";
		$border = "";//border:1px solid black;

		$width  = $showNewestVersionOption ? "80%" : "99%";
		$height = $showNewestVersionOption ? "65px" : "50px";
		$htmlBuilder .= "<div id='tpmsg_div_id' style='width:100%; height:$height; border:1px solid #ccc; color:$color; position:relative;'>".
					"<div style='$border height:100%; width:0%; display:inline-block; vertical-align:middle;'></div>".
						"<div style='$border width:$width; display:inline-block; horizontal-align:center; vertical-align:middle; font-size:16px'>".
							"$message".
						"</div>";
		if($showNewestVersionOption)
			$htmlBuilder .= $button;

		$htmlBuilder .= "</div><br>";

		return $htmlBuilder;
	}

	function getRegistrationExpirationInfo(){
		$result = lavu_query("SELECT * FROM `config` WHERE `setting`='registration_expiration_info'");
		if(!$result){ error_log("mysql error in ".__FILE__." lavu_dberror:".lavu_dberror()); return false; }
		if(mysqli_num_rows($result) == 0){
			return false;
		}
		else{
			$row = mysqli_fetch_assoc($result);
			$value = $row['value'];
			$valueParts = explode(':', $value);
			return array('time_unit' => $valueParts[0], 'quantity' => $valueParts[1]);
		}
	}

?>