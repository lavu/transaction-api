
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<title>Untitled Document</title>

		<style type="text/css">
<!--

body {
	background-color: transparent;
	margin-left: 0px;
	margin-top: 0px;
	margin-right: 0px;
	margin-bottom: 0px;
	-webkit-tap-highlight-color:rgba(0,0,0,0);
}

.style4 {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 14px;
	font-weight: bold;
	color: #8896AD;
}
.style6 {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 24px;
	font-weight: bold;
	color: #8896AD;
}
.style8 {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 15px;
	font-weight: bold;
	color: #07142a;
}
.creditnumber {
	font-family: Verdana, Geneva, sans-serif;
	font-size: 13px;
	font-weight: bold;
	color: #3d4f6d;
}
.nametext {
	font-family: Verdana, Geneva, sans-serif;
	font-size: 13px;
	font-weight: bold;
}
.subtitles {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 16px;
	font-weight: bold;
	color: #5A75A0;
}
.subtitles_dark {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 16px;
	font-weight: bold;
	color: #2f3e55;
}
.full_name_css_class {
    font-family: Verdana, Arial, Helvetica, sans-serif;
    font-size: 28px;
    font-weight: bold;
    color: #555;
    margin-left: 25px;
    width: 390px;
    /*background-color:#FFF;*/
    text-align: center;
    
    word-wrap:break-word;
}
.info_text1 {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 16px;
	color: #233756;
}
.info_text2 {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 15px;
	color: #5A75A0;
}
.info_text3 {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 15px;
	color: #233756;
}
-->
		</style>
	</head>

<?php 
	require_once(dirname(__FILE__) . "/../comconnect.php"); 
	
	error_log("customer_history: ".print_r($_REQUEST, true));
	
if(reqvar("customer_id")) $customer_id = reqvar("customer_id");
$racer_id = $customer_id;
	//echo "<span style=\"color:#fff;\">harharha customer_id: $customer_id</span><br/>";

	
	function reformat_date($date,$type) {
		$datetime_array = explode(" ", $date);
		$date_array = explode("-", $datetime_array[0]);
		$time_array = explode(":", $datetime_array[1]);
	
		if ($type == 1) {
			return date("g:i:s A \o\\n F jS, Y", mktime((int)$time_array[0], (int)$time_array[1], (int)$time_array[2], (int)$date_array[1], (int)$date_array[2], (int)$date_array[0]));
		} else if ($type == 2) {
			return date("Y-m-d g:i A", mktime((int)$time_array[0], (int)$time_array[1], (int)$time_array[2], (int)$date_array[1], (int)$date_array[2], (int)$date_array[0]));
		}
	}
	
	function get_field_info($table, $get_field, $known_field, $known_value) {	
		$get_info = lavu_query("SELECT `[1]` FROM `[2]` WHERE `[3]` = '[4]'", $get_field, $table, $known_field, $known_value);
		if (mysqli_num_rows($get_info) > 0) {
			$extract_info = mysqli_fetch_array($get_info);
			return $extract_info[$get_field];
		} else {
			return "";
		}
	}

	$creation_date = "";
	$confirmed_by = "Unconfirmed";
	$firstName = '';
	$lastName  = '';
	$get_customer_info = lavu_query("SELECT * FROM `med_customers` WHERE `id` = '[1]'", $customer_id);
	if (mysqli_num_rows($get_customer_info) > 0) {
		$extract_c = mysqli_fetch_array($get_customer_info);
		$creation_date = reformat_date($extract_c['date_created'],1);
		if ($extract_c['confirmed_by_attendant'] == "") {
			$confirmed_by = "Speedsheet Transfer";
		} else if ($extract_c['confirmed_by_attendant'] != "0") {
			$confirmed_by = get_field_info("users", "f_name", "id", $extract_c['confirmed_by_attendant'])." ".get_field_info("users", "l_name", "id", $extract_c['confirmed_by_attendant']);
		}
		$firstName = $extract_c['f_name'];
		$lastName  = $extract_c['l_name'];
	}
	
	$pos_history = "";
	
	$total_credits_purchased = 0;
	$check_pos_history = lavu_query("SELECT `locations`.`title` as pos_location, `med_action_log`.`action` as action, `med_action_log`.`restaurant_time` as time, `med_action_log`.`order_id` as order_id, `med_action_log`.`credits` as credits, `med_action_log`.`user_id` as user_id FROM `med_action_log` LEFT JOIN `locations` ON `locations`.`id` = `med_action_log`.`loc_id` WHERE `customer_id` = '[1]' ORDER BY `med_action_log`.`restaurant_time` DESC", $customer_id);
	
	$usr_pos_arr = array();
	if(mysqli_num_rows($check_pos_history) > 0){
    	while($currRow = mysqli_fetch_assoc($check_pos_history)){
        	$usr_pos_arr[] = $currRow;
    	}
	}
	
	$pos_history .= "<div style='margin-left:25px; font-size:12px;'>";
	
	if( count($usr_pos_arr) ){
        foreach($usr_pos_arr as $currAction){
    	    $attendant = get_field_info("users", "f_name", "id", $currAction['user_id'])." ".get_field_info("users", "l_name", "id", $currAction['user_id']);
    	    $pos_history .= "<br>";
            $pos_history .= "Location: " . $currAction['pos_location'] . "<br>";
            $pos_history .= "Date-Time: " . $currAction['time'] . "<br>";
            $pos_history .= "Attendant: " . $attendant . "<br>";
            $pos_history .= "Order #: " . $currAction['order_id'] . "<br>";
            $pos_history .= $currAction['action'] . "<br>";
        }
	}else{
        $pos_history .= "This person has no POS transactions on record.";
	}

	
	$pos_history .= "</div>";

?>


	<body>
	
	    <br>
	    <div class="full_name_css_class"><?=$firstName?> - <?=$lastName?></div>
		<br>
		<table width="350" cellspacing="0" cellpadding="0" style='margin-left:50px;'>
			<tr>
				<td align="center">
					<table width="350" border="0" cellspacing="0" cellpadding="0">
						<tr><td>&nbsp;</td></tr>
						
						<?php
						
							if ($extract_c['membership'] != "") {
								$today = date("Y-m-d", time());
								if ($today > $extract_c['membership_expiration']) {
									$expires = "Expired ".$extract_c['membership_expiration'];
								} else if ($today == $extract_c['membership_expiration']) {
									$expires = "Today";
								} else {
									$expires = $extract_c['membership_expiration'];
								}
								echo "<tr>
									<td align='center'>
										<table cellspacing='3' cellpadding='3'>
											<tr>
												<td align='left' valign='middle'><span class='info_text3'><b>".$extract_c['membership']." - </b></span></td>
												<td align='right' valign='middle'><span class='info_text2'><b>Expires:</b></span></td>
												<td align='left' valign='middle'><span class='info_text3'><b>".$expires."</b></span></td>
											</tr>
										</table>
									</td>
								</tr>";
							}
						?>
						
						<tr>
							<td align="left">
								<table cellspacing="0" cellpadding="1">
									<tr>
										<td colspan='2' align="left" valign="middle"><span class="subtitles">Account Created:</span></td>
								    </tr>
								    <tr>
										<td colspan='2' align="left" valign="middle"><span class="info_text1"><?php echo $creation_date; ?></span></td>
									</tr>
								</table>
							</td>
						</tr>
						<tr><td>&nbsp;</td></tr>
						<tr id="POS_row">
							<td align="center">
								<table width="350" cellspacing="0" cellpadding="0">
									<tr>
										<td align="center">
											<table width="350" cellspacing="0" cellpadding="0">
    											<tr>
													<td align="left"><span class="subtitles_dark">POS Transactions</span></td>
												</tr>
												
											</table>
										</td>
									</tr>
									<tr><td><hr color="#5A75A0"></td></tr>
									<tr>
										<td>
											<table cellspacing="0" cellpadding="0">
											
											
												<?php echo $pos_history; ?>
											</table>
										</td>
									</tr>
								</table>
							</td>
						</tr>
						<tr id="races_row" style="display:none;">
							<td align="center">
								<table width="350" cellspacing="0" cellpadding="0">
									<tr>
										<td align="center">
											<table width="350" cellspacing="0" cellpadding="0">
												<tr>
													<td align="left" onClick="document.getElementById('races_row').style.display = 'none'; document.getElementById('POS_row').style.display = 'inline';"><span class="subtitles">&nbsp;&nbsp;POS Transactions</span></td>
													<td align="right"><span class="subtitles_dark">Races&nbsp;&nbsp;</span></td>
												</tr>
											</table>
										</td>
									</tr>
									<tr><td><hr color="#5A75A0"></td></tr>
									<tr>
										<td align="center">
											<table cellspacing="0" cellpadding="0">
												<?php echo $race_history; ?>
											</table>
										</td>
									</tr>
								</table>
							</td>
						</tr>
						<tr><td>&nbsp;</td></tr>
					</table>
				</td>
			</tr>
	  </table>	
	</body>
</html>
