<?php
	header('Content-Type: text/html; charset=utf8');
    session_start();

	require_once(dirname(__FILE__) . "/../comconnect.php");
	require_once(dirname(__FILE__) . "/customer_functions.php");
	$dataname = $company_code;//From comconnect.
	$cm = reqvar("cm");//Whether called from a custom modifier.
	$parent_mode = reqvar("parent_mode");
	$parent_mode_vars = reqvar("parent_mode_vars");
	$parent_mode_assoc = array();
	$parent_mode_var_string = "";
	if($parent_mode_vars!="")
	{
		$parent_mode_var_arr = explode("(and)",$parent_mode_vars);
		for($n=0; $n<count($parent_mode_var_arr); $n++)
		{
			$inner_arr = explode("(eq)",$parent_mode_var_arr[$n]);
			if(count($inner_arr) > 1)
			{
				$parent_mode_assoc[$inner_arr[0]] = $inner_arr[1];
				$parent_mode_var_string .= $inner_arr[0] . "=" . $inner_arr[1] . "&";
			}
		}
	}

	if(isset($_REQUEST['saved_scroll_position']))
	{
		$_SESSION['saved_scroll_position'] = $_REQUEST['saved_scroll_position'];
	}

	$extra_vars = "&parent_mode=".$parent_mode . "&parent_mode_vars=".$parent_mode_vars;
	if ($cm) $extra_vars .= "&cm=".$cm;
	$mode = reqvar("mode");
//error_log($dataname . " is in mode: " . $_REQUEST['mode']);


	$jsonFieldTitleTranslationJSONDictionary = getJSONFieldTranslateDictionary();//Returns json const object.
	//error_log("jsonDict:".print_r($jsonFieldTitleTranslationJSONDictionary,1));
?>

<script type='text/javascript'>

	var customer_fields_translations = <?php echo $jsonFieldTitleTranslationJSONDictionary; ?>;



    //alert('dev');
    blocking_navigations = true;
    function setBlockNavigations(isBlocking){
        //alert('Now setting to block navigations');
        //alert('blocking navigations: ' + isBlocking);
        blocking_navigations = isBlocking;
    }

	function handleCardSwipe(swipeString, hexString, reader, encrypted) {

		alert(swipeString + " ... " + hexString + " ... " + reader + " ... " + encrypted);
	}

	function run_cmd(cvars) {
		cvars = cvars.replace(/\(and\)/ig,'&')
		window.location = "?" + cvars;
	}

	function btn_press() {
		window.location = "_DO:cmd=send_js&c_name=customer&js=run_cmd(232)";
		//window.location = "_DO:cmd=send_js&c_name=" + comname + "&js=run_cmd(1234)";
	}

	var delivery_option_on = false;
	function customer_selected(customer_id,customer_name,jsonInfo,parent_offset,customer_email){

	    //To prevent -999 error we need to make sure that the
	    //last url call has finished.
	    //if(history_edit_touches_blocked){
    	//    return;
	    //}

	    if(typeof jsonInfo === 'string'){
	    	jsonInfo = JSON.parse(jsonInfo);
	    }

		var printInfoArr = jsonInfo['print_info'];
		for (var i = 0; i < printInfoArr.length; i++){
			var currPrintItem = printInfoArr[i];
			var currPrintItemTitle = currPrintItem['field'];
			var fieldDisplayTitle = customer_fields_translations[currPrintItemTitle] ? customer_fields_translations[currPrintItemTitle] : currPrintItemTitle;
			currPrintItem['field'] = fieldDisplayTitle;
		}

		if(typeof jsonInfo !== 'string'){
			jsonInfo = JSON.stringify(jsonInfo);
		}

        if(!delivery_option_on){
			var hasScrollPosition = arguments.length > 3 && typeof arguments[3] == "string" && arguments[3].match(/[0-9]*/);
			if(!hasScrollPosition) set_scroll_pos = 0; else set_scroll_pos = arguments[3];
            set_active_customer(customer_id,customer_name,jsonInfo,set_scroll_pos,customer_email);
        }else{
            var hasScrollPosition = arguments.length > 3 && typeof arguments[3] == "string" && arguments[3].match(/[0-9]*/);
            var scrollPosition = hasScrollPosition ? "<input id='saved_scroll_position' name='saved_scroll_position' value='" + arguments[3] + "' type='hidden'/>" : '';
            var addedHTML = "<form id='move_to_delivery_options_form_id' action='' method='POST'>" +
                                        scrollPosition +
                                        "<input id='customer_id'   name='customer_id'   value='"+customer_id+"'     type='hidden'/>" +
                                        "<input id='customer_name' name='customer_name' value='"+customer_name+"'   type='hidden'/>" +
                                        "<input id='customer_email' name='customer_email' value='"+customer_email+"'   type='hidden'/>" +
                                        "<input id='jsonInfo'      name='jsonInfo'      value='"+jsonInfo+"'        type='hidden'/>" +
                                        "<input id='mode'          name='mode'          value='delivery_options'    type='hidden'/>" +
                                    "</form>";
            //alert('Adding: ' + addedHTML);

            window.document.body.innerHTML += addedHTML;
            var theForm = document.getElementById('move_to_delivery_options_form_id');
            //alert(theForm);
            theForm.submit();
        }
	}

	function set_active_customer(customer_id,customer_name,jsonInfo,save_scroll_position,customer_email)
	{

		//Do cmd
		customer_name = customer_name.replace(/&/g,"%26");

		if(!save_scroll_position) save_scroll_position = 0;
		//customer_name = "Mr Poopie";
		//customer_id = 2345;
		//alert("Argument[2] is the config printing info: " + arguments[2]);
		/*
		  //value12
		  {
		      "print_info": [ {"field":"First Name", "print":"k|r|b", "info":"Brian"}, {"field":"Last Name", "print":"k|r|b", "info":"Dennedy"}, ...,]

		  }
		*/
		//json_decode
		//  _DO:cmd=set_order_value&key=email&value=
		//alert(jsonInfo);
	    var jsonInfoObj = JSON.parse(jsonInfo);
	   	var printInfo = jsonInfoObj.print_info;//We are drilling down to email of client.
	   	var emailToSendToApp = '';
			if (typeof customer_email != 'undefined') {
				if (customer_email.length > 0) emailToSendToApp = customer_email;
			}
	   	for(var i = 0; i < printInfo.length; i++){
	   		var currObj = printInfo[i];
	   		if(currObj['field'] == 'Email'){
	   			emailToSendToApp = currObj['info'];
	   			//alert( 'Setting email:'+emailToSendToApp );
	   		}
	   	}
		<?php
			if($parent_mode=="guests")//Hotel tab
			{
				//echo "alert(\"Chose \" + customer_name); window.location = \"_DO:cmd=close_overlay\"; ";
				//echo "var sendstr = customer_name + \"|o|\" + customer_id + \"|o|\" + encodeURIComponent( jsonInfo ); ";
				//echo "window.location = '_DO:cmd=send_js&c_name=guests&js=guest_selected(\"' + customer_id + '\")'; ";
				$mode = $_REQUEST['mode'];
				$searchterm = $_REQUEST['searchterm'];
				$group_order_id = (isset($parent_mode_assoc['group_order_id']))?$parent_mode_assoc['group_order_id']:"";
				echo "window.location = '_DO:cmd=send_js&c_name=guests&js=guest_selected(\"' + customer_id + '\",\"$group_order_id\")_DO:cmd=load_url&c_name=customer&f_name=order_addons/customer.php?parent_mode=$parent_mode&parent_mode_vars=$parent_mode_vars&mode=$mode&searchterm=$searchterm&saved_scroll_position=' + save_scroll_position; ";
			}
			else
			{
				if ($cm) { //Fitness ? (Maybe fitness and other things?)

					echo "window.location = \"_DO:cmd=add_mod&mod_title=MEMBER: \" + customer_name + \" (#\" + customer_id + \")&mod_price=0\";";

				} else {
					echo "window.location = \"_DO:cmd=setOrderValue&key=email&value=\"+encodeURIComponent(emailToSendToApp)+\"_DO:cmd=close_overlay&set_info=\" + customer_name + \"|o|\" + customer_id + \"|o|\" + encodeURIComponent( jsonInfo );";
					// + \"|o| (k,r,b)\" _DO:cm\";
				}
			}
		?>
	}

	function letter_key_pressed_in_wrapper(letter){
	   //alert('Yo!  It worked!!!  You pressed: ' + letter);
	   var letter_div_row = document.getElementById('tr_div_' + letter);
	   var totalOffset = getAbsoluteOffset(letter_div_row);
	   document.body.scrollTop = totalOffset;
	}

	function getAbsoluteOffset(elem){
	// offsetTop is the scalar
	// offsetParent is the element from which it is measured.
	   var totalOffset = elem.offsetTop;
	   while(elem = elem.offsetParent){
	       totalOffset += elem.offsetTop;
	   }
	   return totalOffset;
	}

	//function go_to_add_customer_nav(){
    //	window.location = "?mode=''<?php echo $extra_vars;?>";
	//}

	function plus_button_navigate(){
		//alert('plus_button_navigate');
		//var curr_nav_mode = "<?php echo $mode; ?>";
		var inAddCustomer = <?php echo $_REQUEST['add_customer_mode'] == '1' ? 'true' : 'false'; ?>;
		//alert('Curr Nav Mode:' + curr_nav_mode + '  inAddCustomer:' + inAddCustomer);
		//alert('inAddCustomer:' + inAddCustomer + '  showsMostRecentUsers:'+showsMostRecentUsers);
		if(showsMostRecentUsers && inAddCustomer){
			//alert("Going to recent customers");
			window.location = "?mode=<?php echo $extra_vars;?>";
		}else{
			//alert("Going to add customer");
			window.location = "?add_customer_mode=1&mode=<?php echo $extra_vars;?>";
		}
	}

	function jump_to_saved_person_by_id(saved_person_id){
    	var persons_div = document.getElementById(saved_person_id);
    	var totalOffset = getAbsoluteOffset(persons_div);
    	document.body.scrollTop = totalOffset - 25;
	}

	function preload_images(){
    	preload_image = new Image();
    	preload_image.src = 'images/edit_cust_info.png';
    	preload_image.src = 'images/edit_customer.png';
    	preload_image.src = 'images/edit_customer_lit.png';
    	preload_image.src = 'images/green_arrow.png';
    	preload_image.src = 'images/green_arrow_lit.png';
    	preload_image.src = 'images/history_banner.png';
    	preload_image.src = 'images/view_history.png';
    	preload_image.src = 'images/view_history_lit.png';
    	preload_image.src = 'images/back_arrow.png';
    	preload_image.src = 'images/back_arrow_lit.png';
    	preload_image.src = 'images/deliver.png';
    	preload_image.src = 'images/deliver_lit.png';
    	preload_image.src = 'images/pickup.png';
    	preload_image.src = 'images/pickup_lit.png';
    	//INSERTED BY Brian D.
    	preload_image.src = 'images/doc.png';
    	preload_image.src = 'images/sig_blue.png';
    	preload_image.src = 'images/sig_green.png';
    	preload_image.src = 'images/doc_dark.png';
    	preload_image.src = 'images/sig_blue_dark.png';
    	preload_image.src = 'images/sig_green_dark.png';
    	preload_image.src = 'images/sig_red_dark.png';
    	//END INSERT
	}

</script>


<style>

    body, div, img {
        -webkit-touch-callout: none;
        -webkit-user-select: none;
    }
	body, table {
		font-family:Verdana,Arial;
		font-size:12px;
		color:#333;
	}
	.instructions {
		color:#258;
		font-weight:bold;
		font-size:14px;
	}
	.instructions_or {
		color:#667;
		font-weight:bold;
		font-size:20px;
	}
	.new_customer_header {
		background-color:#258;
		color:#fff;
		font-weight:bold;
		font-size:14px;
	}
	.new_customer_container {
		background-color:#dde;
		font-size:14px;
		width:320px;
	}
	input {
		font-size:14px;
	}

</style>



<body style='margin: 0px; border: 2px gray;'>



<?php

    //We need the locations row to get the localized datetime.
    $locationRowResult = lavu_query("SELECT * FROM `locations`");
    $locationRow;
    if(!$locationRowResult){ error_log("MYSQL Error trying to get location row in components/order_addons/customer.php"); }
    if(mysqli_num_rows($locationRowResult)){
        $locationRow = mysqli_fetch_assoc($locationRowResult);
    }else{
        error_log("Error in customer.php, location row query had no results for dataname: " . $company_read['data_name']);
        $locationRow = array();
    }

    //INSERTED BY Brian D.  For waiver stuff, we set the global variable of whether the option is available.
    setGlobalVariablesForWhetherUsingWaivers($locationRow);
    //END INSERT
    //INSERTED BY Brian D. 2014,04,29...  sets global: $showsMostRecentUsers
    setGlobalVariablesForWhetherShowRecentUsers();
    //
    $deliveryOptionsRow = false;
    $delivery_options_result = lavu_query("SELECT * FROM `config` WHERE `setting`='delivery_options'");
    if(!$delivery_options_result){
        error_log("MYSQL dun messed up in customer.php");
    }
    else if(mysqli_num_rows($delivery_options_result)){
        $deliveryOptionsRow = mysqli_fetch_assoc($delivery_options_result);
        $parts = explode('|o|', $deliveryOptionsRow['value']);
        $usingDeliveryPane = ($parts[4] == 'true') ? 'true' : 'false';
        echo "<script type='text/javascript'>delivery_option_on=$usingDeliveryPane;</script>";
    }else{
        echo "<script type='text/javascript'>delivery_option_on=false;</script>";
    }

    //$deliversQuery = "SELECT * FROM `config` WHERE `setting`= "

    $previousSearchTerm = urlencode( $_GET['previousSearchTerm'] );
    $saved_position_id  = urlencode( $_GET['saved_position_id'] );

	


	$customer_id = reqvar('customer_id');
	$searchterm = reqvar("searchterm");

	/*echo "<script type='text/javascript'> alert('MODE: $mode'); </script>";*/
	
	//INSERTED BY Brian D. 2014-04-29.
	//We now have an option for starting out in the 'search result area' where we pull the different
	//clients ordered by most recent activity.
	$addCustomer = $_REQUEST['add_customer_mode'];
	//echo "<script type='text/javascript'> alert('hi'); </script>";
	//error_log("mode:$mode showsMostRecentUsers:$showsMostRecentUsers add_customer:$addCustomer");
	//echo "<script type='text/javascript'> alert('mode:$mode showsMostRecentUsers:$showsMostRecentUsers add_customer_mode:$addCustomer'); </script>";
	if( $mode == '' && $showsMostRecentUsers && empty($_REQUEST['add_customer_mode']) && empty($_REQUEST['med_posted']) ){// Note $showMostRecentUsers set in customer_functions as global.
		$totalOffset;
		if( !empty($_GET['saved_position_id']) ){
    		$totalOffset = $_SESSION['saved_scroll_position'];
		}

		/*$medCustomerList .= "<script type='text/javascript'> alert('Offset Will Be: $totalOffset'); </script>";*/
		$medCustomerList = draw_med_customer_list($searchterm, false, $totalOffset);

		?>
	    <div style='z-index:400; position:fixed; left:-5px;  top:-50px; width:425px; height:50px; -webkit-box-shadow: 0px 0px 4px 4px rgba(128, 128, 128, 0.5);'></div>
	    <div style='z-index:400; position:fixed; left:-5px;  top:555px; width:425px; height:50px; -webkit-box-shadow: 5px 5px 7px 7px rgba(128, 128, 128, 0.5);'></div>
	    <div style='z-index:400; position:fixed; right: 0px; top:0px;   width: 1px; height: 555px; background-color: rgb(192,192,192);'></div>
		<?php

		//Load the letter sidebar in the other component.
		if(isset($_REQUEST['saved_scroll_position']))
		{
    		$totalOffset = $_SESSION['saved_scroll_position'];
			if(is_numeric($totalOffset))
			{
				$medCustomerList .= "<script type='text/javascript'>document.body.scrollTop = $totalOffset</script>";
			}
		}
		$medCustomerList .= "<script type='text/javascript'> window.location = " .
		          "'_DO:cmd=send_js&c_name=customer_wrapper&js=set_side_bar_letters_visible(true);unlockButtonsDelayed();setLoadingHidden(true);' </script>";
        echo $medCustomerList;
	}
	//End 'recent activity' add on.

	else if( $mode != 'delivery_options' && ($searchterm!="" || $mode == 'do_search') )
	{
		$totalOffset;
		if( !empty($_GET['saved_position_id']) ){
    		$totalOffset = $_SESSION['saved_scroll_position'];
		}

		/*$medCustomerList .= "<script type='text/javascript'> alert('Offset Will Be: $totalOffset'); </script>";*/
		$medCustomerList = draw_med_customer_list($searchterm, false, $totalOffset);

		?>
	    <div style='z-index:400; position:fixed; left:-5px;  top:-50px; width:425px; height:50px; -webkit-box-shadow: 0px 0px 4px 4px rgba(128, 128, 128, 0.5);'></div>
	    <div style='z-index:400; position:fixed; left:-5px;  top:555px; width:425px; height:50px; -webkit-box-shadow: 5px 5px 7px 7px rgba(128, 128, 128, 0.5);'></div>
	    <div style='z-index:400; position:fixed; right: 0px; top:0px;   width: 1px; height: 555px; background-color: rgb(192,192,192);'></div>
		<?php

		//Load the letter sidebar in the other component.
		if(isset($_REQUEST['saved_scroll_position']))
		{
    		$totalOffset = $_SESSION['saved_scroll_position'];
			if(is_numeric($totalOffset))
			{
				$medCustomerList .= "<script type='text/javascript'>document.body.scrollTop = $totalOffset</script>";
			}
		}
		$medCustomerList .= "<script type='text/javascript'> window.location = " .
		          "'_DO:cmd=send_js&c_name=customer_wrapper&js=set_side_bar_letters_visible(true);unlockButtonsDelayed();setLoadingHidden(true);' </script>";
        echo $medCustomerList;
	}
	else if($mode == 'update_customer'){//																U P D A T E   C U S T O M E R

	    $_SESSION['saved_scroll_position'] = $_GET['saved_scroll_position'];

//error_log("Saved Scroll Pos: " . $_SESSION['saved_scroll_position']);
	    echo "<div style='width:390px; height:60px; background-color:#DAA; background: url(images/edit_cust_info.png) no-repeat; background-size:100%;margin-left:25px'> " .
	               create_padded_touch_area(
	               "<input id='update_customer_back_input_id' type='button' ".
	               "style='background:URL(images/back_arrow.png) no-repeat; background-size:100%; width:25px; height:25px;" .
	                      "background-position:center; border:0px;' />",'update_customer_back_input_div_id', 0, 0, 50, 50) .
	         "</div>";
	    insertButtonGraphicsJavascript();
		echo "<div style='margin-left:25px'>";
		echo draw_med_customer_form(false, $customer_id);
		echo "</div>";
		echo "<script type='text/javascript'> window.location = " .
		      "'_DO:cmd=send_js&c_name=customer_wrapper&js=set_side_bar_letters_visible(false);setLoadingHidden(true);' </script>";
	}
	else if($mode == 'history'){
    	$_SESSION['saved_scroll_position'] = $_GET['saved_scroll_position'];
	    echo "<div id='customerHistoryBanner_div' style='width:390px; height:60px; background-color:#DAA; background: url(images/history_banner.png) no-repeat; background-size:100%; margin-left:25px'>" .
	    create_padded_touch_area(
	               "<input id='view_history_back_input_id' type='button' ".
	               "style='background:URL(images/back_arrow.png) no-repeat; background-size:100%; width:25px; height:25px; " .
	                      "background-position:center; border:0px;' />" ,'view_history_back_input_div_id', 0, 0, 50, 50) .
	         "</div>";
	    insertButtonGraphicsJavascript();
    	require_once(dirname(__FILE__) . '/customer_history.php');
    	echo "<script type='text/javascript'> window.location = " .
		      "'_DO:cmd=send_js&c_name=customer_wrapper&js=set_side_bar_letters_visible(false);unlockButtonsDelayed();setLoadingHidden(true);' </script>";
	}//INSERTED BY Brian D.
	else if($mode == 'waiver'){//																- - - W A I V E R S

		echo "<script type='text/javascript'> dontUseCache = true; </script>";
		//If it is the waiver area, we need to load the customer information.
		$customer_id = $_GET['customer_id'];
		$customer_row = array();
		if(!empty($customer_id)){
			$result = lavu_query("SELECT * FROM `med_customers` WHERE `id`='[1]'", $customer_id);
			if(!$result){
				error_log("Mysql error in ".__FILE__." -k55q- error: ".lavu_dberror());
			}
			if(mysqli_num_rows($result)){
				$customer_row = mysqli_fetch_assoc($result);
			}
		}

		$_SESSION['saved_scroll_position'] = $_GET['saved_scroll_position'];
		echo "<div id='banner' style='width:390px; height:60px; background-color:#DAA; background: url(images/banner_sheet_waiver.png) no-repeat; background-size:100%;margin-left:25px'> " .
	               create_padded_touch_area(
	               "<input id='update_customer_back_input_id' type='button' ".
	               "style='background:URL(images/back_arrow.png) no-repeat; background-size:100%; width:25px; height:25px;" .
	                      "background-position:center; border:0px;' />",'update_customer_back_input_div_id', 0, 0, 50, 50) .
	               "<div style='width:300px; white-space:nowrap; margin-left:85px; font:22px Verdana; margin-top:-6px; overflow:scroll;'>".
	               		"Waiver:".$customer_row['f_name'] . " " . $customer_row['l_name'].
	               "</div>".
	         "</div>";
	    insertButtonGraphicsJavascript();
	    require_once(dirname(__FILE__).'/customer_waivers.php');
		echo "<script type='text/javascript'> window.location = ".
		      "'_DO:cmd=send_js&c_name=customer_wrapper&js=set_side_bar_letters_visible(false);unlockButtonsDelayed();setLoadingHidden(true);' </script>";
	}//END INSERT
	else if($mode == 'delivery_options'){//																D E L I V E R Y    O P T I O N S
	    $customer_id = $_POST['customer_id'];

	    $scrollPos = $_SESSION['saved_scroll_position'] = $_POST['saved_scroll_position'];
    	echo "<div id='delivery_options_banner_div_id' ".
    	                "style='width:390px; height:60px; background-color:#DAA; background: url(images/banner.png) no-repeat; ".
    	                "background-size:100%;margin-left:25px;'> " .
    	                create_padded_touch_area(
    	                "<input id='delivery_options_back_input_id' type='button'  " .
	                      "style='background:URL(images/back_arrow.png) no-repeat; background-size:100%; width:25px; height:25px;" .
	                      "background-position:center; border:0px;' />",'delivery_options_back_input_div_id',0,0,50,50) . //Note the posting of the persons name on the banner happens in /delivery_options.php

	                      "<div style='margin-left:90px; font-size:26px; margin-top:-8px; overflow:scroll;'>" .
	                           "<span id='delivery_name_set_first' style='color:#555'>  </span>" .
	                           "<span id='delivery_name_set_last' style='font-family:helvetica; color:#111; margin-left: 10px;'> </span>" .
	                      "</div>".
	         "</div>";
	    insertButtonGraphicsJavascript();
//echo "<script type='text/javascript'> alert('Customer ID: $customer_id'); </script>";
    	require_once(dirname(__FILE__) . '/delivery_options.php');
        echo "<script type='text/javascript'> window.location = " .
		      "'_DO:cmd=send_js&c_name=customer_wrapper&js=set_side_bar_letters_visible(false);unlockButtonsDelayed();setLoadingHidden(true);' </script>";
	}
	else{
	    //echo "<div id='searchOrCreateCustomerBanner_div' style='width:390px; height:106px; background: url(images/searchorcreate.png) no-repeat; background-size:100%;margin-left:25px'></div>";
		echo getSearchOrCreateBannerDiv();
		echo "<div style='margin-left:25px'>";
		insertButtonGraphicsJavascript();
		echo draw_med_customer_form();
		echo "</div>";
		echo "<script type='text/javascript'> preload_images(); </script>";
		echo "<script type='text/javascript'> window.location = " .
		      "'_DO:cmd=send_js&c_name=customer_wrapper&js=set_side_bar_letters_visible(false);unlockButtonsDelayed();setLoadingHidden(true);' </script>";
	}
	echo "<script type='text/javascript'> blocking_navigations = false; </script> ";

	function getSearchOrCreateBannerDiv(){
		echo "<div style='display:inline-block; position:relative;'>";
		echo   "<div style='color:#BBB; width:100%; display:inline-block; position:relative; font:18px Helvetica;'>";
		echo     "<div style='background-color:white;position:relative;width:390px;height:80px;left:28px;'>";
		echo       "<div style='background-color:white;color:gray;border:1px #DDD solid;position:relative;width:390px;height:40px;text-align:center;display:inline-block;'>";//SEARCH CUSTOMERS div
		echo         "<div style='width:0px;height:100%;vertical-align:middle;background-color:gray;display:inline-block;'></div>";//Zero width div for middle height alignment
		echo         "<div style='display:inline-block;background-color:white;'>". speak("SEARCH CUSTOMERS")."</div>";
		echo       "</div>";
		echo       "<div style='background-color:white;color:gray;border:1px #DDD solid;position:relative;width:390px;height:40px;text-align:center;display:inline-block;'>";//CREATE A CUSTOMER
		echo         "<div style='width:0px;height:100%;vertical-align:middle;background-color:gray;display:inline-block;'></div>";//Zero width div for middle height alignment
		echo         "<div style='background-color:white;display:inline-block;margin-top:10px'>".speak("CREATE A CUSTOMER")."</div>";
		echo       "</div>";
		echo     "</div>";
		echo   "</div>";
		echo   "<div style='display:inline-block;position:absolute;left:96px;top:10px;color:white;background-color:#aecd37;font:20px Helvetica Bold;border-radius:10px;width:20px;height:20px'>";// ^ symbol div
		echo     "<div style='display:inline-block;margin-left:4px;margin-top:0px;'>^</div>";
		echo   "</div>";
		echo   "<div style='display:inline-block;position:absolute;left:96px;top:53px;color:white;background-color:#aecd37;font:20px Helvetica Bold;border-radius:10px;width:20px;height:20px'>";// + symbol div
		echo     "<div style='display:inline-block;margin-left:4px;margin-top:-4px;'>+</div>";
		echo   "</div>";
		echo   "<div style='display:inline-block;position:absolute;left:222px;top:32px;background-color:#BBB;font:10px Helvetica Bold;border-radius:10px;width:20px;height:20px;color:white;font:Helvetica'>";// OR middle div
		echo     "<div style='display:inline-block;margin-left:2px;margin-top:3px;'>OR</div>";
		echo   "</div>";
		echo "</div>";
		echo "<br><br>";
	}

	function insertButtonGraphicsJavascript(){
	   global $previousSearchTerm;
	   global $saved_position_id;
    ?>
        <script type='text/javascript'>

            function style_button_press_state(elem, isPressed){
                var imageName;
                var elementToChange = this;
                if(elem.id == 'update_customer_back_input_id'){ imageName = isPressed ? "'images/back_arrow_lit.png'" : "'images/back_arrow.png'";}
                if(elem.id == 'view_history_back_input_id'){    imageName = isPressed ? "'images/back_arrow_lit.png'" : "'images/back_arrow.png'";}
                if(elem.id == 'delivery_options_back_input_id'){imageName = isPressed ? "'images/back_arrow_lit.png'" : "'images/back_arrow.png'";}
                imageName = "url(" + imageName + ")";
                elem.style.backgroundImage = imageName;
            }

            is_back_navigating = false;
            function style_button_press_start(event){
                setBlockNavigations(true);
                elem = document.getElementById( this.id.replace("_div","") );
                style_button_press_state(elem, true);
                if(!is_back_navigating && (elem.id == 'update_customer_back_input_id' || elem.id == 'view_history_back_input_id') ){
                    is_back_navigating = true;
                    var useCacheInline = (typeof dontUseCache !== 'undefined' && dontUseCache == true) ? '0' : '1';
                    var url_back = "?searchterm=" + encodeURIComponent( '<?=$previousSearchTerm?>' ) +
                                        "&saved_position_id=" + '<?=$saved_position_id?>' + '&returning_to_search='+useCacheInline+'&mode=do_search';
                    var fullURL = '_DO:cmd=send_js&c_name=customer_wrapper&js=' + 'setLoadingHidden(false);';
                    dontUseCache = false;
                    window.location = fullURL;
                    setTimeout(function(){ window.location = url_back; }, 100);
                }
                else if(!is_back_navigating && elem.id == 'delivery_options_back_input_id'){
                    /*
                        is_back_navigating = true;
                        var url_back = "?searchterm=" + encodeURIComponent( '<?=$previousSearchTerm?>' ) + "&saved_position_id=" + '<?=$saved_position_id?>' + '&returning_to_search=1';
                        var fullURL = '_DO:cmd=send_js&c_name=customer_wrapper&js=' + 'setLoadingHidden(false);';
                        window.location = fullURL;
                        setTimeout(function(){ window.location = url_back; }, 100);
                    */
                    is_back_navigating = true;
                    var fullDoURL = '_DO:cmd=send_js&c_name=customer_wrapper&js=setLoadingHidden(false);';
                    var url_back_to_search = "?searchterm="+encodeURIComponent('<?=$previousSearchTerm?>')+
                                                "&saved_position_id=1234&returning_to_search=1&mode=do_search";
                    window.location = fullDoURL;
                    setTimeout(function(){ window.location = url_back_to_search; }, 100);
                }
            }

            function reloadWithMode(mode){
                window.location= '?mode=';
            }

            function style_button_press_end(event){
                style_button_press_state(this, false);
            }

            //The actual button
            var backFromHistArrowBtn = document.getElementById('view_history_back_input_id');
            if(backFromHistArrowBtn){
                backFromHistArrowBtn.addEventListener('touchstart', style_button_press_start);
                backFromHistArrowBtn.addEventListener('touchmove' , style_button_press_start);
            }
            var backFromEditCustBtn  = document.getElementById('update_customer_back_input_id');
            if(backFromEditCustBtn){
                backFromEditCustBtn.addEventListener('touchstart', style_button_press_start);
                backFromEditCustBtn.addEventListener('touchmove' , style_button_press_start);
            }
            var backFromDeliverBtn = document.getElementById('delivery_options_back_input_id');
            if(backFromDeliverBtn){
                backFromDeliverBtn.addEventListener('touchstart', style_button_press_start);
                backFromDeliverBtn.addEventListener('touchmove' , style_button_press_start);
            }

            //The padded div (for extended touch area).
            var backFromHistArrowBtnDiv = document.getElementById('view_history_back_input_div_id');
            if(backFromHistArrowBtnDiv){
                backFromHistArrowBtnDiv.addEventListener('touchstart', style_button_press_start);
                backFromHistArrowBtnDiv.addEventListener('touchmove' , style_button_press_start);
            }
            var backFromEditCustBtnDiv  = document.getElementById('update_customer_back_input_div_id');
            if(backFromEditCustBtnDiv){
                backFromEditCustBtnDiv.addEventListener('touchstart', style_button_press_start);
                backFromEditCustBtnDiv.addEventListener('touchmove' , style_button_press_start);
            }
            var backFromDeliverBtnDiv  = document.getElementById('delivery_options_back_input_div_id');
            if(backFromDeliverBtnDiv){
                backFromDeliverBtnDiv.addEventListener('touchstart', style_button_press_start);
                backFromDeliverBtnDiv.addEventListener('touchmove' , style_button_press_start);
            }
        </script>
    <?php
	}
?>
</body>