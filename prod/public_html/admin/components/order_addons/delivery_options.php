<?php
    
    
    
    main();
    $nowTime;//Of the form 'h:i a' (e.g. 2:30 pm)
    function main(){
        global $customer_id, $nowTime;
        require_once(dirname(__FILE__) . '/javascript_dates.php');
        //The config tables setting=GetCustomerInfo
        $customerRows = getCustomerInfoRows();
        //The config tables setting=GetCustomerInfo, array of form: 
        // array( 'first_name' => $firstName, 
        //                'last_name' => $lastName, 
        //                'street_address' => $streetAddress, 
        //                'zip' => $zip,
        //                'state' => $state,
        //                'country' => $country   );
        
        // Where the variables as values are the entire rows from the config table.
        $addressFields = getAddressFields($customerRows);
        
        //The med_customer row we are looking at, (Our Current Customer).
        $customerInfoRow = getCustomerRow($customer_id);
        
        //The address fields joined to the actual customer.
        //    'first_name' => James 
        //    'last_name'  => Buttomsworth
        //     ...
        //    'country'    => "United States"
        $finalAddressArr = getCustomerInfoForConfigGetCustomerInfoRows($customerInfoRow, $addressFields);
        
        //Stored in the config table, what quick options of delivery time to display.
        // numeric array of array('time_amount'=>  , time_unit => )'s
        $quickTimeOptions = getQuickTimeOptions();
        
        //0-7 days added to their localized date-time.
        $thisWeekDates = getDatesForFollowingWeek();
        
        $width = 390;
        
        $nowTime = setNowTimeJavascriptVar_now_time();
        setJavascriptTimeIntervalSelect($quickTimeOptions);
        echo "<script type='text/javascript'> var now_time = '$nowTime'; </script>";
        
//echo print_r($customerInfoRow,1);
        updateNameOnBanner($customerInfoRow['f_name'], $customerInfoRow['l_name']);
        generateUseSavedAddressJavascriptVars($finalAddressArr);
        //Building the gui top down.
        echo "<div style='margin-top:8px;'>"; //Content Container.
        generateQuickTimeButtonsStrip($quickTimeOptions, $width, 100);
        generateFoldDownTimeSelectDiv($thisWeekDates, $quickTimeOptions[3]);
        generateDeliveryDisplayTimeDiv();
        generateAddressHandlingDiv($finalAddressArr, $width, 5);
        echo "</div>"; //End Content Container.
        generateDoNotDeliverOptionStrip();
        generateDeliverno_deliverOptionsJavaScriptHandlers();

        //The button touch event handlers.
        echoJavascriptHandlers();
        //The javascript for showing/hiding the alternate address fields--function showAltAddressFields(toShow)
        generateHideShowAltFieldsJS();
        
        
        echo "<script type='text/javascript'> showAltAddressFields(false); setSelectedTimeBasedOnIndex(0); 
                toggleAddressBlocks('checkbox_saved_address_id'); </script>";
    }
    
    function getDatesForFollowingWeek(){
        
        //$timezone = $locationRow['timezone']
        $localizedDateTime = localize_datetime(date('Y-m-d H:i:s'), $locationRow['timezone']);
        $nowTime = strtotime($localizedDateTime);
        $thisWeek = array();
        
        for($i = 0; $i < 8; $i++){
            $currDayTime = strtotime("+ $i day", $nowTime);
            $currDayDate = date('Y-m-d H:s:i', $currDayTime);
            $dayOfWeekLong  = date('l', $currDayTime);
            $dayOfWeekShort = date('D', $currDayTime);
            $thisWeek[] = array("full_date" => $currDayDate, 'day_short' => $dayOfWeekShort, 'day_long' => $dayOfWeekLong);    
        }
        
        return $thisWeek;
    }
    
    function updateNameOnBanner($firstName, $lastName){
    ?>
        <script type='text/javascript'>
            firstName = document.getElementById('delivery_name_set_first');
            firstName.innerHTML = '<?=$firstName?>';
            lastName  = document.getElementById('delivery_name_set_last');
            lastName.innerHTML = '<?=$lastName?>';
        </script>
        
    <?php
    }
    
    //Quicktime buttons area--------------------------------
    function generateQuickTimeButtonsStrip($quickTimeOptions, $width, $height){
      $numberOfButtons = count($quickTimeOptions) + 1;// + custom time.
      $size = $width/5 - 2;
      
      ?>
      <style type='text/css'>
        .amountTDJustMinutes{
            height:60%;
            text-align:center;
            font-family:arial;
            /*font-size:46px;*/
            font-size:34px;
            margin-top:-10px;
            color:#454545;
        }
        .unitTDJustMinutes{
            height:40%;
            text-align:center;
            font-size:17px;
            /*font-size:22px;*/
            margin-top:-20px;
            color:#454545;
        }
        .amountTDIntegerHour{
            height:60%;
            text-align:center;
            font-family:arial;
            /*font-size:46px;*/
            font-size:34px;
            margin-top:-10px;
            color:#454545;
        }
        .unitTD_HOUR_singular{
            height:40%;
            text-align:center;
            /*font-size:22px;*/
            font-size:17px;
            margin-top:-20px;
            color:#454545;
        }
        .amountTDHourAndMinutes{
            height:60%;
            text-align:center;
            font-family:arial;
            /*font-size:39px;*/
            font-size:30px;
            /*margin-top:-13px;*/
            margin-top:-2;
            margin-left:-3px;
            color:#454545;
            vertical-align: bottom;
        }
        .unitTD_HOURS_plural{
            height:40%;
            text-align:center;
            /*font-size:19px;*/
            font-size:15px;
            margin-top:-19px;
            color:#303030;
        }
 
        
        .quickTimeButtonDivUnselected{
            width:77px; 
            height:77px;
            border:1px solid #ccc;/*#aaa;*/
            border-radius: 17px;
            background-color: #DFDFDF;/*F2F2F2*/
        }
        
        .quickTimeButtonDivSelected{
            width:77px;
            height:77px;
            border:1px solid #666;
            border-radius: 17px;
            background-color: #FFF;
        }

        .selectTimeButtonDivUnselected{
            width:138;
            height:75;
            border-radius: 17px;
            border: 1px solid #ccc;
            background-color: #DFDFDF;
        }
        .selectTimeButtonDivSelected{
            width:138;
            height:75;
            border-radius: 17px;
            border: 1px solid #666;
            background-color: #FFFFFF;
        }
        .selectOnly{
            height:100%;
            text-align:center;
            /*font-size:32px;*/
            font-size:28px;
            margin-top:31px;
            /*color:#404040;*/
            border: 0 solid white;
            background-color: rgba(0,0,0,0.0);
        }
        .selectTimeInnerTableDims{
            width:138;
            height:75;
        }
      </style>
      
      <!-- We need to store the quick time options for when selected. Index: ['conf_orig']  -->
      <input id='quick_time_option_0' type='hidden' value='<?=$quickTimeOptions[0]['exact_time']?>' />
      <input id='quick_time_option_1' type='hidden' value='<?=$quickTimeOptions[1]['exact_time']?>' />
      <input id='quick_time_option_2' type='hidden' value='<?=$quickTimeOptions[2]['exact_time']?>' />
      
      <div style='margin-left:25px; width:390px;'>
        <table cellspacing="2">
          <tr>
            <!-- How the array was created: 'amount_css_class' => 'amountTDIntegerHour', 'unit_css_class' => 'unitTD_HOUR_singular'); -->
            
            <?php
            /*
            <!-- TIME SELECT 1 --> 
            <td><div id='time_select_btn_1_id' style='width:77px; height:77px; background:url(images/time_box_lit.png) no-repeat; background-size:100% 100%;'>
                <table style='width:100%;height:100%;'>
                    <tr><td><div class='<?=$quickTimeOptions[0]['amount_css_class']?>'> <?=$quickTimeOptions[0]['time_amount']?> </div></td></tr>
                    <tr><td><div class='<?=$quickTimeOptions[0]['unit_css_class']?>'>   <?=$quickTimeOptions[0]['time_unit']?> </div></td></tr>
                </table>
            </div></td>
            
            <!-- TIME SELECT 2 --> 
            <td><div id='time_select_btn_2_id' style='width:77px; height:77px; background:url(images/time.png) no-repeat; background-size:100% 100%;'>
                <table style='width:100%;height:100%;'>
                    <tr><td><div class='<?=$quickTimeOptions[1]['amount_css_class']?>'><?=$quickTimeOptions[1]['time_amount']?> </div></td></tr>
                    <tr><td><div class='<?=$quickTimeOptions[1]['unit_css_class']?>'><?=$quickTimeOptions[1]['time_unit']?> </div></td></tr>                
                </table>
            </div></td>
            
            <!-- TIME SELECT 3 --> 
            <td><div id='time_select_btn_3_id' style='width:77px; height:77px; background:url(images/time.png) no-repeat; background-size:100% 100%;'>
                <table style='width:100%;height:100%;'>
                    <tr><td><div class='<?=$quickTimeOptions[2]['amount_css_class']?>'><?=$quickTimeOptions[2]['time_amount']?> </div></td></tr>
                    <tr><td><div class='<?=$quickTimeOptions[2]['unit_css_class']?>'><?=$quickTimeOptions[2]['time_unit']?> </div></td></tr>
                </table>
            </div></td>
            
            <!-- Special Time Select -->
            <td colspan='2'><div id='time_select_variable_btn_id' style='width:140px; height:77px; background:url(images/select.png) no-repeat; background-size:100% 100%;'>
                <table id='time_select_table' style='width:100%;height:100%;'>
                  <!--
                    <tr><td><div class='amountTDJustMinutes'><?=$quickTimeOptions[0]['time_amount']?> </div></td></tr>
                    <tr><td><div class='unitTDJustMinutes'><?=$quickTimeOptions[0]['time_unit']?> </div></td></tr> -->
                    <tr><td><div class='selectOnly'>SELECT</div></td></tr>
                </table>
            </div></td>
            */ ?>
            
            
            <!-- TIME SELECT 1 --> 
            <td><div id='time_select_btn_1_id' class='quickTimeButtonDivSelected'>
                <table style='width:100%;height:100%;'>
                    <tr><td><div class='<?=$quickTimeOptions[0]['amount_css_class']?>'> <?=$quickTimeOptions[0]['time_amount']?> </div></td></tr>
                    <tr><td><div class='<?=$quickTimeOptions[0]['unit_css_class']?>'>   <?=$quickTimeOptions[0]['time_unit']?> </div></td></tr>
                </table>
            </div></td>
            
            <!-- TIME SELECT 2 --> 
            <td><div id='time_select_btn_2_id' class='quickTimeButtonDivUnselected'>
                <table style='width:100%;height:100%;'>
                    <tr><td><div class='<?=$quickTimeOptions[1]['amount_css_class']?>'><?=$quickTimeOptions[1]['time_amount']?> </div></td></tr>
                    <tr><td><div class='<?=$quickTimeOptions[1]['unit_css_class']?>'><?=$quickTimeOptions[1]['time_unit']?> </div></td></tr>                
                </table>
            </div></td>
            
            <!-- TIME SELECT 3 --> 
            <td><div id='time_select_btn_3_id' class='quickTimeButtonDivUnselected'>
                <table style='width:100%;height:100%;'>
                    <tr><td><div class='<?=$quickTimeOptions[2]['amount_css_class']?>'><?=$quickTimeOptions[2]['time_amount']?> </div></td></tr>
                    <tr><td><div class='<?=$quickTimeOptions[2]['unit_css_class']?>'><?=$quickTimeOptions[2]['time_unit']?> </div></td></tr>
                </table>
            </div></td>
            
            <!-- Special Time Select -->
            <td colspan='2'><div id='time_select_variable_btn_id' class='selectTimeButtonDivUnselected'>
                <table id='time_select_table' class='selectTimeInnerTableDims'>
                
                    <tr><td><div class='selectOnly'>SELECT</div></td></tr>
                
                </table>
               
            </div></td>
          </tr>
        </table>
      </div>

    <?
      
    }
    function generateJavascriptFunctionsForSelectButton(){
    ?>
    <script type='text/javascript'> 
        function setDisplayTextOnSelectButton(asSelectBOOL, amount, units){
            var timeSelectTable = document.getElementById('time_select_table');
            if(asSelectBOOL){
                
            }else{
                var inner_html = "";
                inner_html += "<tr><td><div class='amountTDJustMinutes'>"+amount+"</div></td></tr>";
                inner_html += "<tr><td><div class='unitTDJustMinutes'>"+units+"</div></td></tr>";
            }
        }
    </script>
    <?php
    }

    
    function generateFoldDownTimeSelectDiv($daysToSelectFromArr, $dayDivisionSegmentLength){
    ?> 
        <style type='text/css'>
            .foldedUserSelectTimeOptions{
                /*background-color:#ADD;*/
                height:0px;
                transition:height 1s;
                -webkit-transition:height 0.3s; /*safari*/
                margin-left: 28px;
                overflow: hidden;
            }
            .unfoldedUserSelectedTimeOpions{
                transition:height 1s;
                -webkit-transition:height 0.3s; /*safari*/
                /*background-color:#AAD;*/
                height:60px;
                margin-left: 28px;
            }
            .leftDaySelectFolded{
                -webkit-transition:opacity 0.3s ease-in 0.3s; /*safari*/
                opacity: 0.0;
                font-size:28px;
                width:185px;
                color:#555566;
            }
            .rightTimeSelectFolded{
                -webkit-transition:opacity 0.3s ease-in 0.3s; /*safari*/
                opacity: 0.0;
                font-size:28px;
                width:175px;
                color:#555566;
            }
            .leftDaySelectUnfolded{
                -webkit-transition:opacity 0.3s ease-in 0.3s; 
                opacity: 1.0; 
                font-size:28px;
                width:185px;
                color:#555566;
            }
            .rightTimeSelectUnfolded{
                -webkit-transition:opacity 0.3s ease-in 0.3s; 
                opacity: 1.0;
                font-size:28px;
                width:175px;
                color:#555566;
            }
        </style>
        <script type='text/javascript'>
            
            function specialTimeButtonClicked(){
                var expandingDiv = document.getElementById('time_select_folding_div_id');
                if(expandingDiv.className == 'foldedUserSelectTimeOptions'){
                    expandingDiv.className = 'unfoldedUserSelectedTimeOpions';
                }
                var daySelect  = document.getElementById('day_select_id');
                var timeSelect = document.getElementById('time_select_id');
                daySelect.className  = 'leftDaySelectUnfolded';
                timeSelect.className = 'rightTimeSelectUnfolded'; 
                setFULLYMDHIDateFromSelectValues(false);
                //
                delivery_display_div.innerHTML = '&nbsp;';
            }
            function closeSpecialTimeDiv(){
                var expandingDiv = document.getElementById('time_select_folding_div_id');
                expandingDiv.className = 'foldedUserSelectTimeOptions';
                var daySelect  = document.getElementById('day_select_id');
                var timeSelect = document.getElementById('time_select_id');
                daySelect.className  = 'leftDaySelectFolded';
                timeSelect.className = 'rightTimeSelectFolded';
                //
                delivery_display_div.style.display = 'block';
            }
            
            var lastSelectedTimeOptionValue;//Used only for when regenerating times, we default to this last saved value.
            function setSelectedIndexOfHourChooserBasedOnLastSelectedValue(){
                var timeSelect = document.getElementById('time_select_id');
                for(i=0;i<timeSelect.options.length;i++){
                    if(timeSelect.options[i].value == lastSelectedTimeOptionValue){
                        timeSelect.selectedIndex = i;
                        break;
                    }
                }
            }
            
            function refreshTimeOptionsToTodaysOptions(){
                var timeSelect = document.getElementById('time_select_id');
                var timeSelect_length = timeSelect.options.length;
                for(i = timeSelect_length - 1; i >= 0; i--){
                    timeSelect.remove(i);
                }
                var timeOptions = generateDateTimeGivenInterval(time_select_interval);
                var military_now_time = convertFromMeridianToMilitary(now_time);
                for(i = 0; i < timeOptions.length; i++){
                    if(convertFromMeridianToMilitary(timeOptions[i]) <= military_now_time){
                        continue;
                    }
                    //now_time
                    var now_time_military = convertFromMeridianToMilitary(now_time);
                    var option_time_military = convertFromMeridianToMilitary(timeOptions[i]);
                    var timeDifference = timeOneMinusTimeTwo(option_time_military, now_time_military);
                    var option = document.createElement("option");
                    option.style.whiteSpace = "pre";
                    option.value = timeOptions[i];
                    option.text  = "<?php echo chr(20) . chr(32)?>" +timeOptions[i] 
                                + '<?php echo chr(20) . chr(32) . chr(20) . chr(32) . chr(20) . chr(32)?>' + 
                                convertFromColonNotationToHoursMinsDisplayStr(timeDifference['hour'] + ':' + 
                                timeDifference['min']);
                    timeSelect.add(option);
                }
                setSelectedIndexOfHourChooserBasedOnLastSelectedValue();
            }
            function refreshTimeOptionsToOtherDayOptions(){
                //alert('start');
                
                var timeSelect = document.getElementById('time_select_id');
                var timeSelect_length = timeSelect.options.length;
                //alert('Length: ' + timeSelect_length);
                for(i = timeSelect_length - 1; i >= 0; i--){
                    timeSelect.remove(i);
                }
                var timeOptions = generateDateTimeGivenInterval(time_select_interval);
                var military_now_time = convertFromMeridianToMilitary(now_time);
                for(i = 0; i < timeOptions.length; i++){
                    var option = document.createElement("option");
                    option.text = timeOptions[i];
                    option.value = timeOptions[i];
                    timeSelect.add(option);
                }
                setSelectedIndexOfHourChooserBasedOnLastSelectedValue();
                //alert(timeOptions);
                //alert('finished');
            }
            function setFULLYMDHIDateFromSelectValues(dispOnDiv){
                var daySelect  = document.getElementById('day_select_id');
                var timeSelect = document.getElementById('time_select_id');
                //yourSelect.options[yourSelect.selectedIndex].value
                var dayValue  = daySelect.options[daySelect.selectedIndex].value;
                var timeValue = timeSelect.options[timeSelect.selectedIndex].value;
                timeValue = convertFromMeridianToMilitary(timeValue);
                timeValue += ":00";
                selected_delivery_time = dayValue + ' ' + timeValue;
                //alert('HERE WE SET THE selected_delivery_time: '+ "\n" + selected_delivery_time);
                
                delivery_display_div.dateTime = selected_delivery_time;
                if(dispOnDiv){
                    delivery_display_div.translateDateTimeToDisplay();
                }
                
                //delivery_display_div.innerHTML = delivery_display_div.dateTime;
                
                //deliveryDisplayDiv.innerHTML = 
                
            }
            
            function handleDayChangeInSelect(selectElem){
                var timeSelectID = document.getElementById('time_select_id');
                lastSelectedTimeOptionValue = timeSelectID.options[timeSelectID.selectedIndex].value;
                if(selectElem.selectedIndex == 0){
                    refreshTimeOptionsToTodaysOptions(selectElem);
                }
                else{
                    refreshTimeOptionsToOtherDayOptions(selectElem);
                }
                setFULLYMDHIDateFromSelectValues(false);
            }
            function handleHourChangeInSelect(selectElem){
                lastSelectedTimeOptionValue = selectElem.options[selectElem.selectedIndex].value;
                setFULLYMDHIDateFromSelectValues(false);
            }
            function timeOneMinusTimeTwo(timeOne, timeTwo){
                var timeOnePMOffset = timeOne.match(/^.*pm.*$/) ? 12 : 0;
                var timeTwoPMOffset = timeTwo.match(/^.*pm.*$/) ? 12 : 0;
                timeOne = timeOne.replace(/[ A-Za-z]/g, '');
                timeTwo = timeTwo.replace(/[ A-Za-z]/g, '');
                timeOneParts = timeOne.split(':');
                timeTwoParts = timeTwo.split(':');
                var timeOneHours   = timeOneParts[0]*1 + timeOnePMOffset*1;
                var timeOneMinutes = timeOneParts[1]*1;
                var timeTwoHours   = timeTwoParts[0]*1 + timeTwoPMOffset*1;
                var timeTwoMinutes = timeTwoParts[1]*1;
                var timeOneTotalSeconds = timeOneHours * 3600 + timeOneMinutes * 60;
                var timeTwoTotalSeconds = timeTwoHours * 3600 + timeTwoMinutes * 60;
                var differenceBySeconds = timeOneTotalSeconds - timeTwoTotalSeconds;
                var signMultiplier = differenceBySeconds < 0 ? -1 : 1;
                differenceBySeconds = Math.abs(differenceBySeconds);
                var hourDiff = Math.floor(differenceBySeconds/3600);
                var minDiff = (differenceBySeconds % 3600)/60;
                return {'hour':hourDiff,'min':minDiff,'sign':signMultiplier};
            }
    
        </script>
        <div id='time_select_folding_div_id' class='foldedUserSelectTimeOptions'>
            <table style='margin-left:10px;margin-top:5px;'>
                <tr>
                    <td>
                        <?php echo generateDaySelectForHiddenDiv($daysToSelectFromArr); ?>
                    </td>
                    <td>
                        <?php echo generateTimeSelectForHiddenDivAndJavascript($dayDivisionSegmentLength); ?>
                    </td>
                </tr>
            </table>
        </div>
        

    <?php    
    }
    function generateDaySelectForHiddenDiv($daysToSelectFromArr){
        $htmlStr = '';
        $htmlStr .= "<select id='day_select_id' class='leftDaySelectFolded' onchange='handleDayChangeInSelect(this)'>";
        $selected = "SELECTED";
        foreach($daysToSelectFromArr as $currDayOptionArr){
            $dayShort = $currDayOptionArr['day_short'];
            $fullDate = $currDayOptionArr['full_date'];
            $fullDateWOTime = substr($fullDate,0,10);
            $shortDateMonth = substr($fullDate,5,2);
            $shortDateDay   = substr($fullDate,8,2);
            $htmlStr .= "<option value='$fullDateWOTime' $selected>$dayShort $shortDateMonth/$shortDateDay</option>";
            $selected = '';
        }
        $htmlStr .= "</select>";
        return $htmlStr;
    }
    function generateTimeSelectForHiddenDivAndJavascript($segmentLength){
        global $javascriptValidTimesArray;
        $htmlStr = '';
        
        /*
        $addMinutes = 0;
        $addHours = 0;
        if(strpos($segmentLength, ":") !== false){
            $parts = explode(':', $segmentLength);
            $addHours = $parts[0];
        }else{
            $addMinutes = $segmentLength;
        }
        

        $dummyDate = '1970-01-01 00:00:00';
        $dummyTime = strtotime($dummyDate);
        
        $timesArr = array();
        while(true){
            $currHourMinute = date('h:i a', $dummyTime);
            $dummyTime = strtotime("+ $addMinutes minute", $dummyTime);
            $dummyTime = strtotime("+ $addHours hour", $dummyTime);
            $timesArr[] = $currHourMinute;
            if(date('d',$dummyTime) != '01'){
                break;
            }
        }
        
        $javascriptValidTimesArray = array();
        $htmlStr .= "<select id='time_select_id' class='rightTimeSelectFolded'>";
        foreach($timesArr as $currTime){
            $htmlStr .= "<option value=''>$currTime</option>";
        }
        $htmlStr .= "</select>";
        */
        $htmlStr .= "<select id='time_select_id' class='rightTimeSelectFolded' onchange='handleHourChangeInSelect(this);'>";
        $htmlStr .= "</select>";
        $htmlStr .= "<script type='text/javascript'> ".
                        "refreshTimeOptionsToTodaysOptions();" . 
                    "</script>";
        return $htmlStr;
    }
    
    function generateDeliveryDisplayTimeDiv(){
    
        echo "<div id='delivery_display_id' ".
                "style='font-size:16px;background-color:rgba(0,0,0,0.0);width:380;height:30;margin-left:28px;text-align:center;margin-top:-6px'></div>";
        echo "<script type='text/javascript'> ".
                    "var delivery_display_div = document.getElementById('delivery_display_id'); ".
                    "delivery_display_div.translateDateTimeToDisplay = function(){ " .
                        "var dayTime = this.dateTime;" . 
                        "var dayTimeParts = dayTime.split(' ');" .
                        "var dayPartsStr  = dayTimeParts[0];" . 
                        "var timePartsStr = dayTimeParts[1];" .
                        "var dayParts  = dayPartsStr.split('-');"  .
                        "var timeParts = timePartsStr.split(':');" . 
                        "var displayTime = timeParts[0]+':'+timeParts[1];".
                        "displayTime = convertFromMilitaryToMeridian(displayTime);".
                        "var displayDate = dayParts[1] + '/' + dayParts[2];" .
                        "this.innerHTML = '<p> Delivery Time Set For: ' + displayDate + ' ' + displayTime + '</p>';" .
                    "}".
             "</script>";
         
    }
    
    function generateAddressHandlingDiv($addressArr, $width, $height){
/*
    // $addressArr ==
    array( 'first_name' => $firstName, 
                'last_name' => $lastName, 
                'street_address' => $streetAddress, 
                'zip' => $zip,
                'city' => $city,
                'state' => $state,
                'country' => $country   );
    //
    
*/
    //echo print_r($addressArr, 1);
    ?>
    

      <script type='text/javascript'>
        var currentlySelectedCheckboxID = '';

        function toggleAddressBlocks(checkboxID){
            currentlySelectedCheckboxID = checkboxID;
            var savedAddressCheckbox = document.getElementById('checkbox_saved_address_id');
            var newAddressCheckbox   = document.getElementById('checkbox_alternative_address_id');
            var savedAddressInfoDiv  = document.getElementById('saved_address_div_id');
            var alterAddressInfoDiv  = document.getElementById('alter_address_div_id');
            if(checkboxID == 'checkbox_saved_address_id'){
                savedAddressCheckbox.checked = true;
                newAddressCheckbox.checked = false;
                showAltAddressFields(false);
                alterAddressInfoDiv.style.border = "none";
                savedAddressInfoDiv.style.border = "1px solid #666";
                savedAddressInfoDiv.style.borderRadius = "10";
            }
            else if(checkboxID == 'checkbox_alternative_address_id'){
                savedAddressCheckbox.checked = false;
                newAddressCheckbox.checked = true;
                showAltAddressFields(true);
                savedAddressInfoDiv.style.border = "none";
                alterAddressInfoDiv.style.border = "1px solid #666";
                alterAddressInfoDiv.style.borderRadius = "10";
            }
        }
      </script>
      <div id='address_outer_wrapper_div_id' style='margin-top:10px'>
        <table border='0' style='width:<?=$width?>px; height:<?=$height?>px; margin-left:25px' >
          <tr>
            <td style='width:50%;'>
              <!-- Saved Address Div -->
              <div id='saved_address_div_id' style='width:100%; height:100%'>
                <table border='0'>
                  <tr>
                    <td>
                      <input id='checkbox_saved_address_id' type='checkbox' onchange="toggleAddressBlocks(this.id);"/>
                    </td>
                    <td>
                      Use Saved Address:
                    </td>
                  </tr>
                  <!-- Saved Address Body -->
                  <tr>
                    <td colspan='2'><div style='height:173px;width:175px'> <?php echo generateUseSavedAddressBody($addressArr); ?></div></td>
                  </tr>
                  <!-- New 'Saved' deliver button -->
                  <tr>
                    <td colspan='2' align=center><img id='deliver_img_saved_id' src='images/deliver.png' onclick='exitFromDeliveryOptions(true)'
                         style='width:110px;height:26px;'/></td>
                  </tr>
                </table>
              </div>
            </td>
            <td style='width:50%;'>
              <!-- Alter Address Div -->
              <div id='alter_address_div_id' style='border:1px width:100%; height:100%'>
                <table border='0'>
                  <tr>
                    <td>
                      <input id='checkbox_alternative_address_id' type='checkbox' onchange="toggleAddressBlocks(this.id);"/>
                    </td>
                    <td>
                      Use Alternate Address:
                    </td>
                  </tr>
                  <!-- Alternate Body Address -->
                  <tr>
                    <td colspan='2'><div style='height:173px;width:175px'> <?php echo generateUseAlternateAddressBody(); ?> </div></td>
                  </tr>
                  <!-- New 'Alter' deliver button -->
                  <tr>
                    <td colspan='2' align=center><img id='deliver_img_alter_id' src='images/deliver.png' onclick='exitFromDeliveryOptions(true)'
                        style='width:110px;height:26px;'/></td>
                  </tr>
                </table>    
              </div>
            </td>
          </tr>
        </table>
      <div>
      <script type='text/javascript'>         
        //Initialize the checkbox selection to the saved addresss.
        var deliverBtnSaved = document.getElementById('deliver_img_saved_id');
        var deliverBtnAlter = document.getElementById('deliver_img_alter_id');
        function handleDeliverBtnSavedTouchStart(){deliverBtnSaved.src = 'images/deliver_lit.png';}
        function handleDeliverBtnSavedTouchEnd(){  deliverBtnSaved.src = 'images/deliver.png';}
        function handleDeliverBtnAlterTouchStart(){deliverBtnAlter.src = 'images/deliver_lit.png';}
        function handleDeliverBtnAlterTouchEnd(){  deliverBtnAlter.src = 'images/deliver.png';}
        deliverBtnSaved.addEventListener( 'touchstart', handleDeliverBtnSavedTouchStart);
        deliverBtnSaved.addEventListener( 'touchend',   handleDeliverBtnSavedTouchEnd);
        deliverBtnAlter.addEventListener( 'touchstart', handleDeliverBtnAlterTouchStart);
        deliverBtnAlter.addEventListener( 'touchend',   handleDeliverBtnAlterTouchEnd);      
        
      </script>
    <?php
    }
    function generateUseSavedAddressJavascriptVars($addressFields){
    ?>
        <script type='text/javascript'>
            var sav_1 = "<?= !empty($addressFields['street_address']) ? $addressFields['street_address'] : ''?>";
            var sav_2 = "<?= !empty($addressFields['city'])  ? $addressFields['city'] : ''?>";
            var sav_3 = "<?= !empty($addressFields['state']) ? $addressFields['state'] : ''?>";
            var sav_4 = "<?= !empty($addressFields['zip']) ? $addressFields['zip'] : ''?>";
            var sav_5 = "<?= !empty($addressFields['country']) ? $addressFields['country'] : ''?>";
        </script>
    <?php
    }
    function generateUseSavedAddressBody($addressFields){
        
        $htmlStr  = "";
        $htmlStr .= "<table>";
        $htmlStr .= !empty($addressFields['street_address']) ? 
                        "<tr><td>".$addressFields['street_address']."</td></tr>" : "<tr><td>".''."</td></tr>";
        $htmlStr .= !empty($addressFields['city']) ? 
                        "<tr><td>".$addressFields['city']."</td></tr>" : "<tr><td>".""."</td></tr>";
        $htmlStr .= !empty($addressFields['state']) ? 
                        "<tr><td>".$addressFields['state']."</td></tr>" : "<tr><td>" . ""."</td></tr>";
        $htmlStr .= !empty($addressFields['zip']) ? 
                        "<tr><td>".$addressFields['zip']."</td></tr>" : "<tr><td>". ""."</td></tr>";
        $htmlStr .= !empty($addressFields['country']) ? 
                        "<tr><td>".$addressFields['country']."</td></tr>" : "<tr><td>" . ""."</td></tr>";
        $htmlStr .= "</table>";
        return $htmlStr;
    }
    function generateUseAlternateAddressBody(){
        $inputStyle = "style='width:170px;background-color:rgba(255,255,255,0.5);'";
        $htmlStr  = "";
        $htmlStr .= "<table>";
        $htmlStr .=   "<tr><td><input id='alt_1' type='text' $inputStyle placeholder='Street Address' /></td></tr>";
        $htmlStr .=   "<tr><td><input id='alt_2' type='text' $inputStyle placeholder='City' /></td></tr>";
        $htmlStr .=   "<tr><td><input id='alt_3' type='text' $inputStyle placeholder='State Code' /></td></tr>";
        $htmlStr .=   "<tr><td><input id='alt_4' type='text' $inputStyle placeholder='Zip Code' /></td></tr>";
        $htmlStr .=   "<tr><td><input id='alt_5' type='text' $inputStyle placeholder='Country' /></td></tr>";
        $htmlStr .= "</table>";
        return $htmlStr;
    }
    function generateHideShowAltFieldsJS(){
    ?>
        <script type='text/javascript'>
        function showAltAddressFields(toShow){
        
            var savedDeliverBtn = document.getElementById('deliver_img_saved_id');
            var alterDeliverBtn = document.getElementById('deliver_img_alter_id');
            var streetAddressInput = document.getElementById('alt_1');
            var cityInput = document.getElementById('alt_2');
            var stateInput = document.getElementById('alt_3');
            var zipcodeInput = document.getElementById('alt_4');
            var countryInput = document.getElementById('alt_5');
            if(toShow){
                savedDeliverBtn.style.display = 'none';
                alterDeliverBtn.style.display = 'block';
                streetAddressInput.style.display = 'block';
                cityInput.style.display = 'block';
                stateInput.style.display = 'block';
                zipcodeInput.style.display = 'block';
                countryInput.style.display = 'block';
            }else{
                savedDeliverBtn.style.display = 'block';
                alterDeliverBtn.style.display = 'none';
                streetAddressInput.style.display = 'none';
                cityInput.style.display = 'none';
                stateInput.style.display = 'none';
                zipcodeInput.style.display = 'none';
                countryInput.style.display = 'none';
            }
        }
        </script>
    <?php
    }
    
    function generateDoNotDeliverOptionStrip(){
    ?>
            <!--
            <div style='background-color:#AAD; margin-left:28px; margin-top:25px; width:384px; position:absolute; top: 475px; left: 0px; z-index: 400;'>
            -->
            
            <div style='/*background-color:#AAD*/;  position:absolute; left:70px; top: 505px; z-index: 400; margin:0 auto;'>
                <table border='0' cellspacing='5'>
                    <tr>
                    <!--
                        <td style='/*background-color:#DAA;*/'> <img id='deliver_img_id' src='images/deliver.png' onclick='exitFromDeliveryOptions()'/> </td>
                        -->
                        <td align=center style='/*background-color:#ADA;*/'> <img id='no_deliver_img_id' src='images/no_deliver.png'  onclick='do_not_deliver()'/> </td>
                    </tr>
                </table>
            </div>
            <script type='text/javascript'> 
                
                var no_deliverBtn  = document.getElementById('no_deliver_img_id');
                
                function handleno_deliverBtnTouchStart(){ no_deliverBtn.src  = 'images/no_deliver_lit.png';}
                function handleno_deliverBtnTouchEnd(){   no_deliverBtn.src  = 'images/no_deliver.png';}
                no_deliverBtn.addEventListener(  'touchstart', handleno_deliverBtnTouchStart);
                no_deliverBtn.addEventListener(  'touchend',   handleno_deliverBtnTouchEnd);
                
                
                function do_not_deliver(){exitFromDeliveryOptions(false);}
            </script>

    <?php
    }
    function generateDeliverno_deliverOptionsJavaScriptHandlers(){
    /*
            name='customer_id'
            name='customer_name'
            name='jsonInfo' 
    */
    $customerID = $_POST['customer_id'];
//error_log("POST:" . print_r($_POST,1));
    $customer_name = $_POST['customer_name'];
    $customer_email = $_POST['customer_email'];
    $jsonInfo = str_replace("'", "", $_POST['jsonInfo']);
    $medCustomerInfoRow = getCustomerRow($customerID);
    ?>


   
        <script type='text/javascript'>
        /*
            var sav_1 = <?= !empty($addressFields['street_address']) ? $addressFields['street_address'] : ''?>;
            var sav_2 = <?= !empty($addressFields['city'])  ? $addressFields['city'] : ''?>;
            var sav_3 = <?= !empty($addressFields['state']) ? $addressFields['state'] : ''?>;
            var sav_4 = <?= !empty($addressFields['zip']) ? $addressFields['zip'] : ''?>;
            var sav_5 = <?= !empty($addressFields['country']) ? $addressFields['country'] : ''?>;
            
            var savedAddressCheckbox = document.getElementById('checkbox_saved_address_id');
            var newAddressCheckbox   = document.getElementById('checkbox_alternative_address_id');
        */
            
            function exitFromDeliveryOptions(delivering){
                //alert('deliver_pressed');
                var customerID = "<?php echo $customerID; ?>";
                //alert('customerID: ' + customerID);
                var customer_name = "<?php echo $customer_name; ?>";
                //alert('customer_name: ' + customer_name);
                var customer_email = "<?php echo $customer_email; ?>";
																
                var jsonInfoStrIn = '<?php echo $jsonInfo; ?>';
                //alert('jsonInfoStrIn: ' + jsonInfoStrIn);
                
                var customerFN = "<?php echo $medCustomerInfoRow['f_name']; ?>";
                var customerLN = "<?php echo $medCustomerInfoRow['l_name']; ?>";
                var customerPhone = "<?php echo $medCustomerInfoRow['field2']; ?>";
                
                var delivery_object = {};
                delivery_object.First_Name = customerFN;
                delivery_object.Last_Name = customerLN;
                delivery_object.Phone_Number = customerPhone;
                
                //alert('first name:' + delivery_object.First_Name + '  last name:' + delivery_object.Last_Name);
                //alert('customer id:' + customerID + " customerName:" + customer_name + " jsonInfoStrIn:" + jsonInfoStrIn);
                if(!delivering){
                    set_active_customer(customerID, customer_name, jsonInfoStrIn, 0, customer_email);
                    return;
                }
                
                var jsonInfo = JSON.parse(jsonInfoStrIn);
                //alert("jsonInfo: " + jsonInfo);
                
                
                delivery_object.is_delivering = '1';
                delivery_object.delivery_time = selected_delivery_time;
                delivery_object.customer_ID = customerID;
                
                //alert('delivery_time: ' + selected_delivery_time);
                //alert("Selected checkbox id: " + currentlySelectedCheckboxID);
                
                if( currentlySelectedCheckboxID == 'checkbox_saved_address_id'){
                    delivery_object.address = {};
                    delivery_object.address.street  = sav_1;
                    delivery_object.address.city    = sav_2;
                    delivery_object.address.state   = sav_3;
                    delivery_object.address.zip     = sav_4;
                    delivery_object.address.country = sav_5;
                    
                    delivery_object.Street_Address = sav_1;
                    delivery_object.City = sav_2;
                    delivery_object.State = sav_3;
                    delivery_object.Zip_Code = sav_4;
                }else{
                    delivery_object.address = {};
                    delivery_object.address.street  = document.getElementById('alt_1').value;
                    delivery_object.address.city    = document.getElementById('alt_2').value;
                    delivery_object.address.state   = document.getElementById('alt_3').value;
                    delivery_object.address.zip     = document.getElementById('alt_4').value;
                    delivery_object.address.country = document.getElementById('alt_5').value;
                    
                    delivery_object.Street_Address = document.getElementById('alt_1').value;
                    delivery_object.City = document.getElementById('alt_2').value;
                    delivery_object.State = document.getElementById('alt_3').value;
                    delivery_object.Zip_Code = document.getElementById('alt_4').value;
                }
                
                if(currentlySelectedCheckboxID != 'checkbox_saved_address_id'){
                    var missingFields = [];
                    if(delivery_object.address.street == ''){
                        missingFields.push('Street Address');
                    }
                    if(delivery_object.address.city == ''){
                        missingFields.push('City');
                    }
                    if(delivery_object.address.state == ''){
                        missingFields.push('State');
                    }
                    if(delivery_object.address.zip == ''){
                        missingFields.push('Zip Code');
                    }
                    if(missingFields.length > 0){
                        var errorString = "Please enter the needed information for the required fields: ";
                        errorString += missingFields.join(', ');
                        alert(errorString);
                        return;
                    }
                }
                jsonInfo.deliveryInfo = delivery_object;
                //alert('lastMarker');
                var jsonInfoStrOut = JSON.stringify(jsonInfo);
                //alert('final json: ' + jsonInfoStrOut);
                set_active_customer(customerID, customer_name, jsonInfoStrOut, 0, customer_email);
                
            }
        </script>
    <?php
    }
    
    
    
    ////  DATA ORGINIZATION FUNCTIONS BELOW....
    function getCustomerInfoForConfigGetCustomerInfoRows($med_customer_row, $configCustomerInfoRows){
        $returnArr = array();
        foreach($configCustomerInfoRows as $key => $confRow){
            $returnArr[$key] = $med_customer_row[$confRow['value2']];
        }
        return $returnArr;
    }
    
    function getCustomerRow($customer_id){
        $result = lavu_query("SELECT * FROM `med_customers` WHERE `id`='[1]'", $customer_id);
        if(!$result){
            error_log("Mysql statement sucks in order_addons/delivery_options.php, lavu_dberror: " . lavu_dberror());
            return false;
        }
        if(mysqli_num_rows($result)){
            return mysqli_fetch_assoc($result);
        }else{
            return false;
        }
    }
    
    function getAddressFields($configCustomerRows){
        $addressFields = array();
        $firstName = GetCustomerInfoRowWithValueLike($configCustomerRows, 'First_Name');
        $lastName = GetCustomerInfoRowWithValueLike($configCustomerRows, 'Last_Name');
        $streetAddress = GetCustomerInfoRowWithValueLike($configCustomerRows, 'address', 'email');
        $zip = GetCustomerInfoRowWithValueLike($configCustomerRows, 'zip');
        $zip = $zip ? $zip : GetCustomerInfoRowWithValueLike($configCustomerRows , 'postal');
        $city = GetCustomerInfoRowWithValueLike($configCustomerRows, 'city');
        $state = GetCustomerInfoRowWithValueLike($configCustomerRows, 'state');
        $country = GetCustomerInfoRowWithValueLike($configCustomerRows, 'country');
        return array( 'first_name' => $firstName, 
                        'last_name' => $lastName, 
                        'street_address' => $streetAddress, 
                        'zip' => $zip,
                        'city' => $city,
                        'state' => $state,
                        'country' => $country   );
    }
    function GetCustomerInfoRowWithValueLike($configCustomerInfoRows, $value, $negator = false){
        $negator = $negator ? strtolower($negator) : false;
        $value   = strtolower($value);
        foreach($configCustomerInfoRows as $row){
            $currRowValue = strtolower($row['value']);
            if($negator){
                if( strpos($currRowValue, $value) !== false && strpos($currRowValue, $negator) === false){ return $row; }
            }else{
                if( strpos($currRowValue, $value) !== false ){ return $row; }
            }
        }
        return false;
    }

    function GetCustomerInfoRows(){
        $result = lavu_query("SELECT * FROM `config` WHERE `setting`='GetCustomerInfo'");
        $rows = array();
        if(!$result){
            error_log("Failed query in order_addons/delivery_options.php, lavu_dberror: " . lavu_dberror() );  
            return $rows;        
        }
        else{
            while($currRow = mysqli_fetch_assoc($result)){
                $rows[] = $currRow;
            }
        }
        return $rows;
    }
    
    function getQuickTimeOptions($useDefaults = false){
        $result = lavu_query("SELECT * FROM `config` WHERE `setting`='delivery_options'");
        if(!$result){
            error_log("Failed query in order_addons/delivery_options.php, lavu_dberror: " . lavu_dberror() );
            return array();
        }
        if(mysqli_num_rows($result) && !$useDefaults){
            $row = mysqli_fetch_assoc($result);
            
            $quickTimeButtonValsStr = $row['value'];
            $quickTimeButtonValsArr = explode("|o|", $quickTimeButtonValsStr);
            $pairsArr = array();
            
            //foreach($quickTimeButtonValsArr as $val){
            for($i = 0; $i < 3; $i++){
                $val = $quickTimeButtonValsArr[$i];
                if(strpos($val, ':') !== false){
                    //We check for integer hours.
                    $parts = explode(":", $val);
                    if($parts[1] == '00'){
                        if($parts[0] == '1'){
                            //e.g. 1 HOUR
                            $pairsArr[] = array('time_amount' => $parts[0], 'time_unit' => 'HOUR', 
                                'amount_css_class' => 'amountTDIntegerHour', 'unit_css_class' => 'unitTD_HOUR_singular', 
                                'conf_orig' => $val, 'exact_time' => relTimeSelectedToExactTime($val));
                        }else{
                            //e.g. 2 HOURS, 3 HOURS etc.
                            $pairsArr[] = array('time_amount' => $parts[0], 'time_unit' => 'HOURS', 
                                'amount_css_class' => 'amountTDIntegerHour', 'unit_css_class' => 'unitTD_HOURS_plural', 
                                'conf_orig' => $val, 'exact_time' => relTimeSelectedToExactTime($val));
                        }
                    }else{
                        //e.g. 1:30 HOURS, 2:15 HOURS
                        $pairsArr[] = array('time_amount' => $val, 'time_unit' => 'HOURS', 
                            'amount_css_class' => 'amountTDHourAndMinutes', 'unit_css_class' => 'unitTD_HOURS_plural', 
                            'conf_orig' => $val, 'exact_time' => relTimeSelectedToExactTime($val));
                    }
                }else{
                    //e.g. 45 MINS, 15 MINS.
                    $pairsArr[] = array('time_amount' => $val, 'time_unit' => 'MINS', 
                        'amount_css_class' => 'amountTDJustMinutes', 'unit_css_class' => 'unitTDJustMinutes', 
                        'conf_orig' => $val, 'exact_time' => relTimeSelectedToExactTime($val));
                }
            }
            $pairsArr[] = $quickTimeButtonValsArr[3];
            return $pairsArr;
        }else{
            $pairsArr = array();
            $pairsArr[] = array('time_amount' => "15",   'time_unit' => 'MINS');
            $pairsArr[] = array('time_amount' => "30",   'time_unit' => 'MINS');
            $pairsArr[] = array('time_amount' => "1:00", 'time_unit' => 'HOUR');
            $pairsArr[] = "1:00";
            return $pairsArr;
        }
    }
    function setJavascriptTimeIntervalSelect($quickTimeOptions){
        echo "<script type='text/javascript'> var time_select_interval = '".$quickTimeOptions[3]."'</script>";
    }
    function relTimeSelectedToExactTime($relativeTime){
        global $locationRow;
        $locationsTimezone = $locationRow['timezone'];
        $localizedDateTimeNow = localize_datetime(date('Y-m-d H:i:s'), $locationsTimezone);
        //echo "<script type='text/javascript'> alert('Localized Date-Time is: $localizedDateTime'); </script>";
        $hours = 0;
        $minutes = 0;
        if(strpos($relativeTime, ':') !== false){
            $parts = explode(':',$relativeTime);
            $hours = $parts[0];
            $minutes = $parts[1];
        }
        else{
            $minutes = $relativeTime;
        }
        $locNowTime = strtotime($localizedDateTimeNow);
        $deliverTime = strtotime("+ $hours hour + $minutes minute", $locNowTime);
        $deliverDateTimeStr = date('Y-m-d H:i:s', $deliverTime);

        return $deliverDateTimeStr;
    }
    
    function setNowTimeJavascriptVar_now_time(){
        global $locationRow;
        $locationsTimezone = $locationRow['timezone'];
        $localizedDateTimeNow = localize_datetime(date('Y-m-d H:i:s'), $locationsTimezone);
        $locNowTime = strtotime($localizedDateTimeNow);
        $now = date('h:i a', $locNowTime);
 
        return $now;
    }
    
    function echoJavascriptHandlers(){
    ?>
        <script type='text/javascript'>
            
                
            var selected_delivery_time;
            function setSelectedTimeBasedOnIndex(index){
                hiddenElem = document.getElementById('quick_time_option_' + index);
                selected_delivery_time = hiddenElem.value;
                /*
                alert('setting delivery time to that from button index: ' + index + "\n" +
                      "time: " + selected_delivery_time);
                */
                delivery_display_div.dateTime  = selected_delivery_time;
                delivery_display_div.translateDateTimeToDisplay();
                
                //delivery_display_div.innerHTML = delivery_display_div.dateTime;
            }
            
            var time_btn_index_selected;
            function handleTimeSelectTouchStart(event){
                turnOffAllTimeSelectButtons();
                if(this.id == 'time_select_btn_1_id'){
                    //this.style.background = "url('images/time_box_lit.png')";
                    this.className = "quickTimeButtonDivSelected";
                    time_btn_index_selected = 0;
                    closeSpecialTimeDiv();
                    setSelectedTimeBasedOnIndex(0);     
                }else if(this.id == 'time_select_btn_2_id'){
                    //this.style.background = "url('images/time_box_lit.png')";
                    this.className = "quickTimeButtonDivSelected";
                    time_btn_index_selected = 1;
                    closeSpecialTimeDiv();
                    setSelectedTimeBasedOnIndex(1);
                }else if(this.id == 'time_select_btn_3_id'){
                    //this.style.background = "url('images/time_box_lit.png')";
                    this.className = "quickTimeButtonDivSelected";
                    time_btn_index_selected = 2;
                    closeSpecialTimeDiv();
                    setSelectedTimeBasedOnIndex(2);
                }else if(this.id == 'time_select_variable_btn_id'){
                    //this.style.background = "url('images/select_lit.png')";
                    this.className = "selectTimeButtonDivSelected";
                    time_btn_index_selected = 3;
                    specialTimeButtonClicked();
                }
            }

            function turnOffAllTimeSelectButtons(){
                /*
                var timeButtonOne = document.getElementById('time_select_btn_1_id');
                var timeButtonTwo = document.getElementById('time_select_btn_2_id');
                var timeButtonThree = document.getElementById('time_select_btn_3_id');
                var timeButtonSelect = document.getElementById('time_select_variable_btn_id');
                timeButtonOne.style.background = "url('images/time.png')";
                timeButtonTwo.style.background = "url('images/time.png')";
                timeButtonThree.style.background = "url('images/time.png')";
                timeButtonSelect.style.background = "url('images/select.png')";
                */
                /////quickTimeButtonDivSelected
                /////quickTimeButtonDivUnselected
                var timeButtonOne    =  document.getElementById('time_select_btn_1_id');
                var timeButtonTwo    =  document.getElementById('time_select_btn_2_id');
                var timeButtonThree  =  document.getElementById('time_select_btn_3_id');
                var timeButtonSelect =  document.getElementById('time_select_variable_btn_id');
                timeButtonOne.className    =  "quickTimeButtonDivUnselected";
                timeButtonTwo.className    =  "quickTimeButtonDivUnselected";
                timeButtonThree.className  =  "quickTimeButtonDivUnselected";
                timeButtonSelect.className =  "selectTimeButtonDivUnselected";
            }

            //Registerting the time button event handlers.
            var timeButtonOne = document.getElementById('time_select_btn_1_id');
            var timeButtonTwo = document.getElementById('time_select_btn_2_id');
            var timeButtonThree = document.getElementById('time_select_btn_3_id');
            var timeButtonSelect = document.getElementById('time_select_variable_btn_id');
            timeButtonOne.addEventListener('touchstart',handleTimeSelectTouchStart);
            timeButtonTwo.addEventListener('touchstart',handleTimeSelectTouchStart);
            timeButtonThree.addEventListener('touchstart',handleTimeSelectTouchStart);
            timeButtonSelect.addEventListener('touchstart',handleTimeSelectTouchStart);
        
            //Registering the Deliver/no_deliver buttons
            
        </script>
    <?php
    }
    
    function createHiddenUserSpecifiedTimeDiv(){
        $htmlString = '';
        
        $htmlString = <<<DIVSTUFF
        <div style=''>
            
        </div>
DIVSTUFF;
        
        return $htmlString;
    }
    

    
?>