<?php

require_once('/home/poslavu/public_html/admin/cp/resources/core_functions.php');
require_once('/home/poslavu/public_html/inc/loyalty/pepper/PepperIntegration.php');  // TO DO : need to create methods for actions in this test harness so we don't have to call API directly
require_once('/home/poslavu/public_html/inc/loyalty/pepper/PepperApi.php');

// Only show page for Lavu Admins
session_start();
if (empty($_SESSION['posadmin_lavu_admin']))
{
	echo 'Unauthorized access';
	exit;
}

$pepperTest = new PepperTest();
$pepperTest->routeRequest($_REQUEST);

?>
<html>
<head>
<title>Pepper Test Page</title>
<style>
body {
	font-family: Arial;
	margin: 20px;
	background: #dfffdf url(/components/order_addons/pepper/images/pepper-logo.png);
}
#all_users figure img {
	-webkit-border-radius: 10px;  /* rounded corners */
}
#all_users figure {
	display: inline-block;
	white-space: nowrap;
	width: 81px;
	-webkit-margin-before: 0px;
	-webkit-margin-after: 20px;
	-webkit-margin-start: 0px;
	-webkit-margin-end: 36px;

	position:relative;
}
#all_users figure:nth-of-type(4n) {  /* fun css that removes the right margin from every 4th figure element */
	-webkit-margin-before: 0px;
	-webkit-margin-after: 20px;
	-webkit-margin-start: 0px;
	-webkit-margin-end: 36px;

	position:relative;
}
#all_users figcaption {
	/*width: 81px;*/
	word-wrap: break-word;
}

#user_details {
}

#user_details_left {
	display: inline-block;
	vertical-align: top;
	width:182px;
}

#user_details_right {
	display: inline-block;
	vertical-align: top;
}

#user_details_left figure {
	display: inline-block;
	white-space: nowrap;
	width: 182px;
	-webkit-margin-before: 0px;
	-webkit-margin-after: 20px;
	-webkit-margin-start: 0px;
	-webkit-margin-end: 36px;

	position:relative;
}

#user_details_left figure img {
	-webkit-border-radius: 10px;  /* rounded corners */
	display: inline-block;
}

.user_details_control_row {
	display: block;
	padding: 2px;
}

#user_awards_area {
	display: inline-block;
	vertical-align: top;
	margin:0px 20px;
}

#user_awards_area h2 {
	margin-top: 10px;
}

#user_awards_area ul {
	padding: 0px;
	list-style-type: none;
}

#amount {
	width: 40px;
}

.user_stats,
.user_details_stats
{
	font-style: italic;
}

.user_status_icon
{
	position:absolute;
	top:0;
	right:4;
	z-index:100;
	margin:2px;
	margin-left:20px;
	filter: invert(100%);
	font-size:18pt;
}

#user_details_status_icon
{
	position:absolute;
	top:0;
	right:20;
	z-index:100;
	margin:2px;
	margin-left:20px;
	filter: invert(100%);
	font-size:42pt;
}

.nodisplay {
	display:none;
}

.user_details_labels {
	font-weight: bold;
}

.user_action_button {
	width: 110px;
	height: 30px;
	line-height: 26px;
	font-size: 12pt;
	text-align: left;
}

@-webkit-keyframes bounce {
	20% {
		-webkit-transform: scale(.1);
	}
	40% {
		-webkit-transform: scale(1.6);
	}
	70% {
		-webkit-transform: scale(1.0);
	}
}

.changed_checkin_status {
	-webkit-animation: bounce 1.5s;
}

</style>
<script>

var locationId = "<?= $pepperTest->locationId ?>";
var dataname = "<?= $pepperTest->dataname ?>";

function loadUserAwards(userId, awards) {
	var userAwardsUl = document.getElementById("user_awards");

	// Remove all child <li> elements
	while (userAwardsUl.hasChildNodes()) {
		userAwardsUl.removeChild(userAwardsUl.lastChild);
	}

	if (!awards) awards = ["(None)"];

	// Loop through award and create elements for it
	for (var i = 0; i < awards.length; i++) {
		var newListItem = document.createElement("li");
		newListItem.innerHTML = awards[i].title +" ["+ awards[i].type +"]";
		userAwardsUl.appendChild(newListItem);
	}
}

function loadOpenOrders(userId, openOrderIds) {
	var orderIdDropdown = document.getElementById("order_id");

	// Remove all child <option> elements
	while (orderIdDropdown.hasChildNodes()) {
		orderIdDropdown.removeChild(orderIdDropdown.lastChild);
	}

	if (!openOrderIds) openOrderIds = ["(None)"];

	// Loop through award and create elements for it
	for (var i = 0; i < openOrderIds.length; i++) {
		var newOption = document.createElement("option");
		newOption.innerHTML = openOrderIds[i].order_id;
		newOption.value = openOrderIds[i].order_id +"^"+ openOrderIds[i].total;
		orderIdDropdown.appendChild(newOption);
	}
}

function populateAmount(selectedOrderOption) {
	var vals = selectedOrderOption.value.split('^', 2);
	var total = (vals[1] != null) ? vals[1] : '';
	document.getElementById("amount").value = total;
	document.getElementById("action_pay").disabled = false;
}

var userFields = ["status_icon", "pic", "name", "stats", "createddate", "balance", "ccinfo", "pushnotify", "subscribed", "userid"];
function loadUserDetails(userId) {
	for (var i = 0; i < userFields.length; i++) {
		var fieldName = userFields[i];
		var sourceElement = document.getElementById("user_"+ fieldName +"_"+ userId);
		var targetElement = document.getElementById("user_details_"+ fieldName);
		if (fieldName == "pic") targetElement.src = sourceElement.src;
		else targetElement.innerHTML = sourceElement.innerHTML;
	}

	// Re-enable buttons
	document.getElementById("action_checkin").disabled = false;
	document.getElementById("action_pay").disabled = true;

	// List user's awards
	var awardsFormData = new FormData();
	awardsFormData.append("action", "awards");
	awardsFormData.append("userId", userId);
	awardsFormData.append("locationId", locationId);

	fetch("/components/order_addons/pepper/test_harness.php", {method: "POST", body: awardsFormData, credentials: 'include'})
		.then((response) => response.json())
		.then(function (results) {
			let awards  = results.awards;
			loadUserAwards(userId, awards);
		});

	var openOrdersFormData = new FormData();
	openOrdersFormData.append("action", "open_order_ids");
	openOrdersFormData.append("dataname", dataname);

	fetch("/components/order_addons/pepper/test_harness.php", {method: "POST", body: openOrdersFormData, credentials: 'include'})
		.then((response) => response.json())
		.then(function (results) {
			let open_order_ids = results;
			loadOpenOrders(userId, open_order_ids);
		});
}

function doUserCheckin(userId) {
	formData = new FormData();
	formData.append("action", "checkin");
	formData.append("userId", userId);
	formData.append("locationId", locationId);
	formData.append("type", "MANUAL");
	//formData.append("trigger", "BEACON");  // Pepper said this could be left out for MANUAL check-ins

	var loadingImg = document.createElement("img");
	loadingImg.setAttribute("src", "/cp/images/loading_transparent.gif");

	var user_details_status_icon = document.getElementById("user_details_status_icon");
	user_details_status_icon.innerHTML = "";
	user_details_status_icon.appendChild(loadingImg);
	user_details_status_icon.classList.remove("changed_checkin_status");

	var user_status_icon = document.getElementById("user_status_icon_"+ userId);
	user_status_icon.classList.remove("changed_checkin_status");

	fetch("/components/order_addons/pepper/test_harness.php", {method:"POST", body:formData, credentials: 'include'}).then( function (response) {
		console.log(response);

		// Change big user pic to show as checked-in
		user_details_status_icon.innerHTML = "✔️";
		user_details_status_icon.classList.add("changed_checkin_status");

		// Change small user pic to show checked-in
		user_status_icon.innerHTML = "✔️";
		user_status_icon.classList.add("changed_checkin_status");
	} );
}

function flipCss(elementId) {
	document.getElementById(elementId).style.className = (document.getElementById(elementId).style.className == "flipped") ? "" : "flipped";
	return false;
}
</script>
</head>
<body>

<h1>Pepper Test Page</h1>

<h2>LocationId <?= $pepperTest->locationId ?></h2>

<div id="user_details">

	<div id="user_details_left">
		<figure>
			<figcaption id="user_details_status_icon"></figcaption>
			<img id="user_details_pic" src="/components/order_addons/pepper/images/no_profile_pic.png" width="162" height="162" border="0" />
			<figcaption id="user_details_name" ></figcaption>
			<figcaption id="user_details_stats" class="user_details_stats"></figcaption>
		</figure>
	</div>

	<div id="user_details_right">
		<div class="user_details_control_row">
			<input type="button" name="action" id="action_checkin" class="user_action_button" value="✔️  Check-in" onclick="doUserCheckin(document.getElementById('user_details_userid').innerHTML)" disabled>
		</div>
		<div class="user_details_control_row">
			<input type="button" name="action" id="action_pay" class="user_action_button" value="💳  Pay" disabled>
			<select name="order_id" id="order_id" onChange="populateAmount(this)"></select>
			$<input type"text" name="amount" id="amount" value="">
		</div>
		<div class="user_details_control_row">
			<span class="user_details_labels">UserId</span> <span id="user_details_userid">12345</span>
		</div>
		<div class="user_details_control_row">
			<span class="user_details_labels">Status</span> <span id="user_details_state">ACTIVE</span>
		</div>
		<div class="user_details_control_row">
			<span class="user_details_labels">Account Created</span> <span id="user_details_createddate">12/19/2001</span>
		</div>
		<div class="user_details_control_row">
			<span class="user_details_labels">Card</span> <span id="user_details_ccinfo">MC xxxx6288</span>
		</div>
		<div class="user_details_control_row">
			<span class="user_details_labels">Balance</span> <span id="user_details_balance">387</span>
		</div>
		<div class="user_details_control_row">
			<span class="user_details_labels">Push Notifications?</span> <span id="user_details_pushnotify">Y</span>
		</div>
		<div class="user_details_control_row">
			<span class="user_details_labels">Subscribed?</span> <span id="user_details_subscribed">N</span>
		</div>
	</div>

	<div id="user_awards_area">
		<h2>Awards</h2>
		<ul id="user_awards">
			<li>(None)</li>
		</ul>
	</div>

</div>

<div id="all_users">
	<h2>Loyalty Users</h2>
	<?= $pepperTest->renderUserPics() ?>
</div>

<form>

</form>



</body>
</html>

<?php

class PepperTest
{
	private $api;

	private $checkins;
	private $users;
	private $awards;

	public $dataname;
	public $locationId;

	function __construct()
	{
		$this->api = new PepperApi();
		$this->dataname = 'hooli';
		$this->locationId = '58da1293c910c60a37c363ad';  // was '58bdd1052aebd96de8b3b32e';
	}

	public function routeRequest($vars)
	{
		$action = isset($vars['action']) ? strtolower($vars['action']) : '';

		switch ($action)
		{
			// Ajax request responders

			case 'checkin':
				$this->performCheckIn($vars);
				exit;
				break;

			case 'awards':
				echo json_encode($this->getAwards($vars));
				exit;
				break;

			case 'open_order_ids':
				echo json_encode($this->getOpenOrderIDs($vars));
				exit;
				break;

			case 'pay':
				$this->performPay($vars);
				break;


			// Landing Page

			default:
				$this->getCheckins();
				$this->getUsers();
				break;
		}
	}

	/**
	 * "Create User Checkin" API call
	 *
	 * POST /users/{userId}/checkins
	 *
	 */
	private function performCheckIn($vars)
	{
		$response = '';

		try
		{
			$response = $this->api->createCheckin($vars);
		}
		catch (Exception $e)
		{
			error_log($e->getMessage());
			echo json_encode(array('error' => true, 'error_msg' => $e->getMessage()));
			return;
		}

		echo json_encode($response);
	}

	/**
	 * "Create Transaction" API call
	 *
	 * POST /users/{userId}/transactions
	 *
	 */
	public function performPay($vars)
	{
		// TO DO
	}

	/**
	 * "Users Awards" API call
	 *
	 * GET /locations/{locationId}/checkins
	 *
	 */
	public function getAwards($vars)
	{
		$response = $this->api->getAwards($vars);
		//error_log("getAwards API response=". json_encode($response));  //debug

		// Save checkins to member with userId as key, so we can easily test for checked-in users
		$awards = isset($response['awards']) ? $response['awards'] : array();
		foreach ( $awards as $award )
		{
			$this->awards[$award['userId']] = $award;
		}

		return $response;
	}

	/**
	 * "Checked-In Users" API call
	 *
	 * GET /locations/{locationId}/checkins
	 *
	 */
	public function getCheckins($vars)
	{
		$response = $this->api->getCheckins();

		// Save checkins to member with userId as key, so we can easily test for checked-in users
		$checkins = isset($response['checkins']) ? $response['checkins'] : array();
		foreach ( $checkins as $checkin )
		{
			$this->checkins[$checkin['userId']] = $checkin;
		}

		return $response;
	}

	/**
	 * Queries database of passed-in dataname for all open order_id's
	 *
	 */
	public function getOpenOrderIDs($vars)
	{
		$open_orders = $this->queryDatabase('open_order_ids', $vars);

		$open_order_ids = array();
		while ( $order = mysqli_fetch_assoc($open_orders) )
		{
			$open_order_ids[] = $order;
		}

		return $open_order_ids;
	}

	/**
	 * "Checked-In Users" API call
	 *
	 * GET /locations/{locationId}/checkins
	 *
	 */
	public function getUsers($vars)
	{
		$response = $this->api->getUsers();
		$this->users = isset($response['users']) ? $response['users'] : array();

		//error_log("users=". json_encode($this->users));  //debug
	}

	/**
	 * "Checked-In Users" html table rendering
	 *
	 */
	public function renderCheckinsTableRows($vars)
	{
		if (empty($this->checkins))
		{
			$this->api->getCheckins();
		}

		$column_names = null;
		$renderedTableHeaderRow = false;

		echo "<table>\n";
		foreach ( $this->checkins as $i => $checkin )
		{
			if ($column_names === null) $column_names = array_keys($checkin);
			if (!$renderedTableHeaderRow) $renderedTableHeaderRow = $this->renderTableHeaderRow($column_names);
			$this->renderTableRow($checkin, $column_names);
		}
		echo "</table>\n";

	}

	/**
	 * "Checked-In Users" API call
	 *
	 * GET /locations/{locationId}/checkins
	 *
	 */
	public function renderUserTableRows($vars)
	{
		if (empty($this->users))
		{
			$this->api->getUsers();
		}

		$column_names = array('fullName', 'created', 'hasProfilePhoto', 'hasPaymentCard', 'balance', 'points', 'subscribed');
		$renderedTableHeaderRow = false;

		echo "<table>\n";
		foreach ( $this->users as $i => $user )
		{
			if ($column_names === null) $column_names = array_keys($user);
			if (!$renderedTableHeaderRow) $renderedTableHeaderRow = $this->renderTableHeaderRow($column_names);
			$this->renderTableRow($user, $column_names);
		}
		echo "</table>\n";

	}

	public function renderTableHeaderRow($column_names)
	{
		echo "<tr>";
		for ($i = 0; $i < count($column_names); $i++)
		{
			$column_name = $column_names[$i];
			echo "<td>". $column_name ."</td>\n";
		}
		echo "</tr>";

		return true;
	}

	public function renderTableRow($row, $column_names)
	{
		echo "<tr>";
		for ($i = 0; $i < count($column_names); $i++)
		{
			$column_name = $column_names[$i];
			$column_value = is_array($row[$column_name]) ? '[...]' : $row[$column_name];
			echo "<td>". $column_value ."</td>\n";
		}
		echo "</tr>";
	}

	public function renderUserPics()
	{
		if (empty($this->users))
		{
			$this->api->getUsers();
		}

		echo "<div id='userpics'>\n";
		foreach ( $this->users as $i => $user )
		{
			$this->renderUserPicFigure($user);
		}
		echo "</div>\n";
	}

	public function renderCheckinPics()
	{
		if (empty($this->checkins))
		{
			$this->api->getCheckins();
		}

		echo "<div id='userpics'>\n";
		foreach ( $this->checkins as $i => $user )
		{
			$this->renderUserPicFigure($user);
		}
		echo "</div>\n";
	}

	public function renderUserPicFigure($user)
	{
		$pic = empty($user['hasProfilePhoto']) ? '/components/order_addons/pepper/images/no_profile_pic.png' : "http://api.pepperhq.com/users/{$user['id']}/avatar.jpg";  // TO DO : replace random profile pic with missing image
		$created = date('m/d/Y', strtotime(substr($user['created'], 0, 10)));  // TO DO : convert YYYY-mm-dd => mm/dd/YYYY
		$unixtimestamp = time();

		$subscribed = empty($user['subscribed']) ? 'N' : 'Y';
		$push_notify = empty($user['isRegisteredForPushNotifications']) ? 'N' : 'Y';
		$ccinfo = empty($user['card']) ? '' : $user['card']['type'] .' xxxx'. $user['card']['last4'];
		$checkin_emoji = empty($this->checkins[$user['id']]) ? "—" : "✔️";

		echo <<<HTML
		<figure id="{$user['id']}" onClick="loadUserDetails('{$user['id']}')">
			<figcaption class="user_status_icon" id="user_status_icon_{$user['id']}">{$checkin_emoji}</figcaption>
			<img id="user_pic_{$user['id']}" src="{$pic}" width="81" height="81" border="0" />
			<figcaption id="user_name_{$user['id']}">{$user['fullName']}</figcaption>
			<figcaption class="user_stats" id="user_stats_{$user['id']}">{$user['points']} pts</figcaption>
			<figcaption class="nodisplay" id="user_createddate_{$user['id']}">{$created}</figcaption>
			<figcaption class="nodisplay" id="user_createddate_{$user['id']}">{$user['state']}</figcaption>
			<figcaption class="nodisplay" id="user_balance_{$user['id']}">{$user['balance']}</figcaption>
			<figcaption class="nodisplay" id="user_ccinfo_{$user['id']}">{$ccinfo}</figcaption>
			<figcaption class="nodisplay" id="user_pushnotify_{$user['id']}">{$push_notify}</figcaption>
			<figcaption class="nodisplay" id="user_subscribed_{$user['id']}">{$subscribed}</figcaption>
			<figcaption class="nodisplay" id="user_userid_{$user['id']}">{$user['id']}</figcaption>
		</figure>
HTML;
	}

	public function getRandomProfilePic()
	{
		$pics = array(
			'https://www.lavu.com/sites/default/files/2017-02/Andy Lim.jpg',
			'https://www.lavu.com/sites/default/files/2017-02/Corey Fiala.jpg',
			'https://www.lavu.com/sites/default/files/2017-02/Gerone Conyers.jpg',
			'https://www.lavu.com/sites/default/files/2017-02/Pamela Moore.jpg',
			'https://www.lavu.com/sites/default/files/2017-02/Robbie 2_0.jpg',
			'https://www.lavu.com/sites/default/files/2017-02/Robbie 2_1.jpg',
			'https://www.lavu.com/sites/default/files/2017-02/Robbie 2_2.jpg',
			'https://www.lavu.com/sites/default/files/2017-02/Robbie 2_3.jpg',
			'https://www.lavu.com/sites/default/files/2017-02/Robbie 2.jpg',
			'https://www.lavu.com/sites/default/files/2017-02/Robbie Knutson-Ratto.jpg',
			'https://www.lavu.com/sites/default/files/2017-02/Steve Brown.jpg',
			'/components/order_addons/pepper/images/adam_robinson.jpg',
			'/components/order_addons/pepper/images/andrew_hawkins.jpeg',
			'/components/order_addons/pepper/images/charles_hall.jpeg',
			'/components/order_addons/pepper/images/simon_kelton.jpeg',
			'/components/order_addons/pepper/images/stuart_hall.jpg',
		);

		return $pics[rand(0, count($pics) - 1)];
	}

	/**
	 * Handles all database queries and connections
	 *
	 * @param  $query_name String identifying query to run
	 * @param  $args Array of input arguments, if needed for query
	 *
	 * @return mixed Returns mysql resource when $return_single_row is false, array when $return_single_row is true, bool for inserts, updates, failures
	 */
	private function queryDatabase($query_name, $args = array())
	{
		$return_single_row = true;
		$return_all_rows_array = false;
		$die_on_zero_rows  = true;
		$die_on_db_errors  = true;
		$update_or_insert  = false;

		switch ( $query_name )
		{
			case 'open_order_ids':
				$sql = "SELECT `order_id`, `total` FROM `poslavu_[dataname]_db`.`orders` WHERE `closed` = '0000-00-00 00:00:00' ORDER BY `order_id` DESC";
				$die_on_zero_rows = false;
				$return_single_row = false;
				break;

			default:
				die(__METHOD__ ." got unrecognized query {$query_name} for dataname {$this->dataname}\n");
				break;
		}

		$result = mlavu_query($sql, $args);

		if ( $result === false && $die_on_db_errors )
		{
			throw new Exception("Database error - {$query_name} query got: ". mysqli_error() ."\n");
		}
		else if ( $result !== false && $return_all_rows_array )
		{
			$all_rows = array();
			while ( $row = mysqli_fetch_assoc($result) )
			{
				$all_rows[] = $row;
			}
			return $all_rows;
		}
		else if ( $update_or_insert )
		{
			return $result;
		}
		else if ( mysqli_num_rows($result) == 0 && $die_on_zero_rows )
		{
			error_log("No row for '{$query_name}' query for {$this->dataname}: sql={$sql} args=". json_encode($args));
			throw new Exception("Database error - No row for '{$query_name}' query for {$this->dataname}: sql={$sql} args=". json_encode($args));
		}

		return ($return_single_row) ? mysqli_fetch_assoc($result) : $result;
	}

}
