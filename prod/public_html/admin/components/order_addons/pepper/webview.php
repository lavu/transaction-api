<?php

session_start();

require_once('/home/poslavu/public_html/admin/cp/resources/core_functions.php');
require_once('/home/poslavu/public_html/inc/loyalty/pepper/PepperIntegration.php');  // TO DO : need to create methods for actions in this test harness so we don't have to call API directly
require_once('/home/poslavu/public_html/inc/loyalty/pepper/PepperApi.php');
try {
	$pepperWebview = new PepperWebview($_REQUEST);
}
catch(Exception $e) {
	errorView();
}
$pepperWebview->routeRequest($_REQUEST);
?>
<html>
<head>
<title><?php speak("Lavu Loyalty");?></title>
<style>
@font-face{
	font-family:DINreg;
	src:url(fonts/DINMedium.39d8205e.ttf)
}
@font-face{
	font-family:DINlight;
	src:url(fonts/DINLight.70d15931.ttf)
}
@font-face{
	font-family:DINbold;
	src:url(fonts/DINBold.75c6da4d.ttf)
}
body {
	margin: 0px;
	padding: 0px;
	font-family: DINlight;
	background-color: rgba(0, 0, 0, 0);
}
section {
	width: 817px;
	height: 460px;
	background-color: rgba(255, 255, 255, 0.9);
	-webkit-border-radius: 10px;
	padding:0px;
}
h1 {
	position: absolute;
	top: 20px;
	left: 20px;
	width: 200px;
	font-family: DINlight;
	color: grey;
	text-transform: uppercase;
	font-size: 18px;
	padding: 0px;
	margin: 0px;
}
h2 {
	position: absolute;
	top: 40px;
	left: 20px;
	font-weight: normal;
	font-size: 1em;
	padding: 0px;
	margin-bottom: 10px;
}
.center {
	text-align: center;
}
.user_pic {
	width: 81px;
	height: 81px;
}
.visible_pic {
	opacity: 1;
}

.opaque_pic {
	opacity: .3;
}
#all_users {
	position: relative;
	padding-left: 20px;
}

#all_users figure img {
	-webkit-border-radius: 10px;  /* rounded corners */
	position: relative;
}
#all_users figure {
	display: inline-block;
	white-space: nowrap;
	width: 81px;
	-webkit-margin-before: 0px;
	-webkit-margin-after: 20px;
	-webkit-margin-start: 0px;
	-webkit-margin-end: 36px;

	position:relative;
}
#all_users figure:nth-of-type(4n) {  /* fun css that removes the right margin from every 4th figure element */
	-webkit-margin-before: 0px;
	-webkit-margin-after: 20px;
	-webkit-margin-start: 0px;
	-webkit-margin-end: 36px;

	position:relative;
}
#checkin_pics {
	position: absolute;
	top: 90px;
	left: 30px;
}

#user_details {
	position: relative;
}

#user_details figure {
	/*display: block;*/
}

.hiddenRight {
	-webkit-transition: all .75s ease;
	left: 1000;
}

.visibleRight {
	-webkit-transition: all .75s ease;
	left: 0;
}

.hiddenLeft {
	-webkit-transition: all .75s ease;
	left: -1000;
}

.visibleLeft {
	-webkit-transition: all .75s ease;
	left: 20;
}

button {
	width: 200px;
	height: 40px;
	-webkit-border-radius: 0px;
	-webkit-appearance: button;
	border-top: 1px solid #cffab3;
	padding: 0 20px;
	border: 0 none;
	font-size: 11px;
	line-height: 20px;
	white-space: nowrap;
}

.activeButton {
	color: white;
	background-color: #aecd37;
	border-top: 1px solid #cffab3;
	box-shadow: 0px 1px 3px #888;
}

.activeButtonBlue {
	color: white;
	background-color: #3a9bcc;
	border-top: 1px solid white;
	box-shadow: 0px 1px 3px #888;
}

.inactiveButton {
	color: #444;
	background-color: #ccc;
	box-shadow: 0px 1px 3px #888;
}

.hiddenButton {
	display: none;
	box-shadow: 0px 1px 3px #888;
}

#user_details figure {
	position: absolute;
	top: 30px;
	width:182px;
}

#user_details_pic {
	-webkit-border-radius: 10px;  /* rounded corners */
}

#user_details_name {
	position: absolute;
	top: 20px;
	font-size: 12pt;
	line-height: 20px;
	text-align: center;
	white-space: nowrap;
}

#user_details_back_arrow {
	position: absolute;
	top: 20px;
	left: -10px;
}

#checkin_user {
	position: absolute;
	top: 215px;
	left: 25px;
}

#select_user {
	position: absolute;
	top: 260px;
	left: 25px;
}

#import_points {
	position: absolute;
	top: 305px;
	left: 25px;
}

#import_points_msg {
	position: absolute;
	top: 350px;
	left: 25px;
	width: 200px;
}

#stats {
	position: absolute;
	top: 60px;
	left: 450px;
	display: inline-block;
	vertical-align: top;
	margin-top: 0px;
}

#app_stats_header {
	position: absolute;
	top: 40px;
	left: 220px;
}

#app_stats {
	position: absolute;
	top: 60px;
	left: 220px;
}

#balance_stats {
	position: absolute;
	top: 450px;
	left: 30px;
}

.centered {
	text-align: center;
}

.stats_column {
	display: inline-block;
	vertical-align: top;
	margin-right: 20px;
}

.user_details_control_row {
	color: #666666;
	display: block;
	padding: 2px;
	font-size: 12pt;
	line-height: 20px;
}

.user_details_control_cell {
	color: #666666;
	display: inline-block;
	padding: 2px;
	font-size: 12pt;
	line-height: 20px;
	margin-right: 20px;
}

#user_details_balance,
#user_details_points,
#user_details_stamps {
	padding-top: 10px;
	font-size: 40pt;
	line-height: 40pt;
	text-align: center;
}

#user_details_stamps_next_button {
	margin-bottom: 10px;
}

#loyalty_program_header {
	position: absolute;
	top: 40px;
	left: 450px;
}

#user_rewards {
	padding: 0px;
	margin: 0px;
	list-style-type: none;
}

#user_rewards li {
	padding: 2px;
	padding-left: 0px;
	margin: 0px;
}

#rewards_label {
	display: block;
	padding-top: 10px;
	color: grey;
	text-transform: uppercase;
	font-size: 16px;
}

#balance_label {
	position: absolute;
	top:  0;
	left: 200;
}

#search {
	position: absolute;
	top: 15px;
	left: 200px;
	color: #cccccc;
	text-align: center;
	-webkit-border-radius: 8px;
	margin-left: 2em;
}

#search_clear_button {
	position: absolute;
	top: 14px;
	left: 355px;
	padding: 0;
	margin: 0;
	font-size: 25px;
	line-height: 30px;
}
/*#search_clear_button:after {
	content: "✖️";
	color: gray;
	width: 30px;
	height: 30px;
	line-height: 30px;
	z-index: 20001;
}*/

#refresh {
	position: absolute;
	top: 14px;
	left: 455px;
	padding: 0;
	margin: 0;
	font-size: 12px;
	line-height: 30px;
}

#refresh_button {
	top: 14px;
	left: 445px;
		width: 13px;
	height: 16px;
}

#close_button {
	display: block;
	padding: 0;
	margin: 0;
	font-size: 25px;
	line-height: 30px;
	color: #666666;
	background-color: #fafafa;
	-webkit-border-radius: 2px;
	width: 30px;
	position: fixed;
	z-index: 20001;
	right: 5%;
	top: 20px;
	text-align: center;
}
#close_button:after {
	content: "x";
	color: gray;
	border: 1px solid #aaa;
	border-radius: 2px;
	width: 30px;
	height: 30px;
	line-height: 30px;
	position: fixed;
	z-index: 20001;
	right: 5%;
	top: 20px;
	text-align: center;
}


#amount {
	width: 40px;
}

.user_stats,
.user_details_stats
{
	font-style: italic;
}

.user_fullname {
	position:absolute;
	top: 85px;
	font-size: .9em;
	line-height: 20px;
	text-align: center;
	width: 100px;
	white-space: nowrap;
	overflow: hidden;
	text-overflow: ellipsis;
}

.user_status_icon
{
	position:absolute;
	top:50;
	right:4;
	z-index:100;
	margin:2px;
	margin-left:20px;
	filter: invert(90%);
	font-size:14pt;
}

#user_details_status_icon
{
	position:absolute;
	top:90;
	right:20;
	z-index:100;
	margin:2px;
	margin-left:20px;
	filter: invert(100%);
	font-size:42pt;
}

#user_details_rewards {
	position: absolute;
	top: 233px;
	left: 250px;
}

.nodisplay {
	display:none;
}

.user_details_labels {
	color: black;
	font-weight: bold;
	display: block;
}

.user_action_button {
	width: 110px;
	height: 30px;
	line-height: 26px;
	font-size: 12pt;
	text-align: left;
}

@-webkit-keyframes bounce {
	20% {
		-webkit-transform: scale(.1);
	}
	40% {
		-webkit-transform: scale(1.6);
	}
	70% {
		-webkit-transform: scale(1.0);
	}
}

.changed_checkin_status {
	-webkit-animation: bounce 1.5s;
}

@-webkit-keyframes shrink_then_regrow {
	20% {
		-webkit-transform: scale(.1);
	}
	40% {
		-webkit-transform: scale(.6)
	}
	70% {
		-webkit-transform: scale(1.0);
	}
}

.changed_user_points {
	-webkit-animation: shrink_then_regrow 1.5s;
}

.selected_userpic {
	box-sizing: border-box;  /* prevents subtle wrapping */
	border: 2px solid green;
}

.rotating {
	-webkit-animation:rotation 2s linear infinite;
	-moz-animation:rotation 2s linear infinite;
	animation:rotation 2s linear infinite;
}

.rotatable {
	/*-webkit-transform: rotate(360deg);*/
}

@-webkit-keyframes rotation {
	100% {
		-webkit-transform: rotate(360deg);
	}
}

</style>
</head>
<body>
<script>

// Init

var tenantId = "<?= $pepperWebview->tenantId ?>";
var locationId = "<?= $pepperWebview->locationId ?>";
var dataname = "<?= $pepperWebview->dataname ?>";
var order_id = "<?= $pepperWebview->order_id ?>";
var loc_id = "<?= $pepperWebview->loc_id ?>";
var pointsAwardId = "";

var stampCardRewards = [];
var stampCardRewardIdShown = '';

var userData = {};
var orderInfo = {};
var orderValues = {};
var extraInfo = {};
var checkedinUsers = {};
var itemLevelDiscountsUsed = {};
var selectedUserId = "";
var lockSelectedUser = false;
var orderIsClosed = false;
var startOnUserDetails = false;
var showLoyaltyPoints = ("<?= $pepperWebview->pepper_config['pepper_loyalty_points'] ?>" == "1");
var showLoyaltyStamps = ("<?= $pepperWebview->pepper_config['pepper_loyalty_stamps'] ?>" == "1");
var showImportLegacyPoints = ("<?= $pepperWebview->pepper_config['pepper_import_points'] ?>" == "1");

// Strings

var emptySearchFieldStr = "🔍  user search";

// Functions

function showRewardsLoadingIndicator() {
	var userRewardsUl = document.getElementById("user_rewards");

	// Remove all child <li> elements
	while (userRewardsUl.hasChildNodes()) {
		userRewardsUl.removeChild(userRewardsUl.lastChild);
	}

	// Loading spinner
	var loadingImg = document.createElement("img");
	loadingImg.setAttribute("src", "/cp/images/loading_transparent.gif");

	var newListItem = document.createElement("li");
	newListItem.appendChild(loadingImg);
	userRewardsUl.appendChild(newListItem);
}

function showImportResults(results) {
	var import_points_msg = document.getElementById("import_points_msg");

	// Display error otherwise display points awarded
	if (results.message != null) {
		import_points_msg.innerHTML = results.message;
	}
	// Display points imported
	else {
		import_points_msg.innerHTML = "<br>"+ results.state +" "+ results.number +" points";

		// Update points element on page -- TO DO : add css flair?
		var user_details_points = document.getElementById("user_details_points");
		//user_details_points.remove("changed_user_points");
		user_details_points.innerHTML = Number.parseInt(user_details_points.innerHTML) + results.number; 
		//user_details_points.add("changed_user_points");  // This just made the whole number disappear

		// Update points hidden element for user
		var user_points = document.getElementById("user_points_"+ results.userId);
		user_points.innerHTML = Number.parseInt(user_points.innerHTML) + results.number;
	}
}

function showNextStampCard() {
	// Get the next stamp card reward in the stampCardRewards array but prevent array index overflow by wrapping around to 0
	var indexToShow = 0;
	for (var i = 0; i < stampCardRewards.length; i++) {
		if (stampCardRewardIdShown == stampCardRewards[i]._id && (i + 1) < stampCardRewards.length) {
			indexToShow = i + 1;
		}
	}

	var reward = stampCardRewards[indexToShow];
	if (reward != null) {
		// Get stamps and stamps label from awards (rather than deprecated points field on user object)
		var userDetailsStamps = document.getElementById("user_details_stamps");
		var userDetailsStampsLabel = document.getElementById("user_details_stamps_label");
		if (userDetailsStamps) userDetailsStamps.innerHTML = reward.points.available +"/"+ reward.points.redemption;
		if (userDetailsStampsLabel) userDetailsStampsLabel.innerHTML = reward.title;

		stampCardRewardIdShown = reward._id;
	}
}

function getStackedLabel(reward, defaultStackedLabel, numRewardsAvailable) {
	var stackedLabel = "";
	if (!defaultStackedLabel) defaultStackedLabel = "";

	for (var i = 0; i < reward.visualisation.templates.length; i++) {
		var template = reward.visualisation.templates[i];
		if (template.name == "stackedLabel") {
			stackedLabel = template.value;
			stackedLabel = stackedLabel.replace(/[{][{]available[}][}]/, numRewardsAvailable);
			stackedLabel = stackedLabel.replace(/[{][{]current[}][}]/, reward.points.available % reward.points.redemption);

			// The first {{s}} is for stamp cards
			var s = (numRewardsAvailable == 1) ? "" : "s";
			stackedLabel = stackedLabel.replace(/[{][{]s[}][}]/, s);

			// The second {{s}} is for points
			s = (reward.points.available == 1) ? "" : "s";
			stackedLabel = stackedLabel.replace(/[{][{]s[}][}]/, s);
		}
	}

	if (stackedLabel == "") stackedLabel = defaultStackedLabel;

	return stackedLabel;
}

var rewardElementId = null;
function loadUserRewards(userId, rewards) {
	var userRewardsUl = document.getElementById("user_rewards");

	// Remove all child <li> elements
	while (userRewardsUl.hasChildNodes()) {
		userRewardsUl.removeChild(userRewardsUl.lastChild);
	}

	if (!rewards || rewards.length == 0) {
		var newListItem = document.createElement("li");
		newListItem.innerHTML = "(None)";
		userRewardsUl.appendChild(newListItem);
		return;
	}

	// Reset list of stamp cards awards because we may be viewing a different user
	stampCardRewards = [];

	// Loop through rewards and create button elements for them
	for (var i = 0; i < rewards.length; i++) {
		var reward = rewards[i];

		var isPoints = (reward.template == 'PSEUDO_CURRENCY');
		var isStamps = (reward.template == 'STAMP_CARD');
		var numFullStampCard = (isStamps && reward.points) ? Math.floor(reward.points.available / reward.points.redemption) : "";
		var hasFullStampCard = (isStamps && reward.points && (numFullStampCard >= 1));

		if (isPoints) {
			// Get points and points label from awards (rather than deprecated points field on user object)
			var userPoints = document.getElementById("user_points_"+ userId);
			var userDetailsPoints = document.getElementById("user_details_points");
			var userDetailsPointsLabel = document.getElementById("user_details_points_label");
			if (userPoints) userPoints.innerHTML = reward.points.available;
			if (userDetailsPoints) userDetailsPoints.innerHTML = reward.points.available;
			if (userDetailsPointsLabel) userDetailsPointsLabel.innerHTML = reward.title;
			pointsAwardId = reward._id;

			// Don't create button for psueo-currency points
			continue;
		}
		else if (isStamps) {
			// Get stamps and stamps label from awards (rather than deprecated points field on user object)
			var userStamps = document.getElementById("user_stamps_"+ userId);
			var userDetailsStamps = document.getElementById("user_details_stamps");
			var userDetailsStampsLabel = document.getElementById("user_details_stamps_label");
			if (userStamps) userStamps.innerHTML = reward.points.available;
			if (userDetailsStamps) userDetailsStamps.innerHTML = reward.points.available +"/"+ reward.points.redemption;
			if (userDetailsStampsLabel) userDetailsStampsLabel.innerHTML = reward.title; 

			// Save stamp card rewards so we can flip through them with the arrow button if there are multiple
			stampCardRewards.push(reward);
			stampCardRewardIdShown = reward._id;

			// Show arrow button if multiple stamp cards
			if (stampCardRewards.length > 1) {
				var userDetailsStampsNextButton = document.getElementById("user_details_stamps_next_button");
				userDetailsStampsNextButton.classList.remove('nodisplay');
			}

			// Do not incomplete stamp cards in rewards list
			if (!hasFullStampCard) {
				continue;
			}
		}

		var rewardNumAvailable = (isStamps) ? numFullStampCard : reward.points.available;
		var rewardButtonTitle = (rewardNumAvailable == 1 && reward.title.slice(-1) == 's') ? reward.title.slice(0, -1) : reward.title;  // Make singular
		/*if (isStamps) {  // Testing out using stackedLabel
			rewardNumAvailable = "";
			rewardButtonTitle = getStackedLabel(reward, rewardButtonTitle, numFullStampCard);
		}*/

		var rewardButton = document.createElement("button");
		rewardButton.id = "reward_"+ reward._id;
		rewardButton.className = "inactiveButton";
		rewardButton.innerHTML = rewardNumAvailable +" "+ rewardButtonTitle;

		var rewardAttributeTitle = (isStamps) ? reward.points.redemption +" "+ reward.title : reward.title;

		// Work-around for typos in app code where it expects "item" or "check" even though the correct DB value is "items" or "checks"
		var fixedDiscountType = reward.lavuDiscountType.level;
		if (fixedDiscountType == 'items')  fixedDiscountType = 'item';
		if (fixedDiscountType == 'checks') fixedDiscountType = 'check';

		// Custom HTML attributes containing discount_types info
		rewardButton.setAttribute("type", fixedDiscountType);
		rewardButton.setAttribute("calc_type", reward.lavuDiscountType.code);
		rewardButton.setAttribute("calc", reward.lavuDiscountType.calc);
		rewardButton.setAttribute("label", rewardAttributeTitle);
		rewardButton.setAttribute("redeem_id", reward._id);
		rewardButton.setAttribute("token", reward.token);  // Set on Pepper side - must match discount_types.token
		rewardButton.setAttribute("type_id", reward.lavuDiscountType.id);  // Really discount_types.id but POS expects type_id
		rewardButton.setAttribute("designator", "more info");
		rewardButton.setAttribute("limit_to", reward.lavuDiscountType.limit_to);

		// Do not let reward button be used if it's redeemed automatically or if we don't have the necessary discount_types row info
		var missingDiscountFields = (reward.lavuDiscountType.length == 0 || reward.lavuDiscountType.level == null || reward.lavuDiscountType.code == null || reward.lavuDiscountType.calc == null);
		var isDiscountAlreadyUsed = (itemLevelDiscountsUsed[reward._id] !== undefined);
		if (isPoints || missingDiscountFields || isDiscountAlreadyUsed) {
			rewardButton.disabled = true;
		}
		// Otherwise if reward button is for a usable reward then attach the callback that makes it clickable
		else
		{
			rewardButton.ontouchstart = function () {
				/*var hasEligibleMenuItem = true;
				var limit_to = this.getAttribute("limit_to");
				if (limit_to) {
					var limitTo = limit_to.split(",");
					if (limitTo && limitTo.length) {
						hasEligibleMenuItem = false;
						for (orderContentsRow of orderInfo.contents) {
							hasEligibleMenuItem = limitTo.includes(orderContentsRow.id);
						}
						if (!hasEligibleMenuItem) {
							// Display error
							window.location = "_DO:cmd=alert&title="+ encodeURIComponent("Cannot Use Loyalty Reward") +"&message="+ encodeURIComponent("No eligible items found");
							return false;
						}
					}
				}*/

				// Deselect reward if already selected
				if (rewardElementId == this.id && !orderIsClosed) {
					this.className  = "inactiveButton";
					rewardElementId = "";
				}
				// Select this reward (if they don't already have one selected)
				else if (!rewardElementId) {
					// Deselect any other reward currently selected
					if (rewardElementId) document.getElementById(rewardElementId).className = "inactiveButton";

					// Set this reward as the current active reward
					this.className  = "activeButtonBlue";
					rewardElementId = this.id;
				}
			};
		}

		var newListItem = document.createElement("li");
		newListItem.appendChild(rewardButton);

		userRewardsUl.appendChild(newListItem);
	}
}

function getRewards(userId) {
	var xhr = new XMLHttpRequest();
	xhr.onreadystatechange = function () {
		if (this.readyState == 4 && this.status == 200) {
			var response = JSON.parse(this.responseText);
			var rewards = response;
			loadUserRewards(userId, rewards);
		}
	}
	xhr.onErrorListener = function(errorMsg) {
		console.error(errorMsg);
	}
	xhr.open("GET", "/components/order_addons/pepper/webview.php?action=rewards&dataname="+ encodeURIComponent(dataname) +"&locationId="+ encodeURIComponent(locationId) +"&userId="+ encodeURIComponent(userId), true);  // TO DO : see if locationId is really required to pass here
	xhr.withCredentials = true;
	xhr.send();
}

function loadUserDetails(userId) {
	// Show user's rewards -- start call first and affect other elements while we wait
	showRewardsLoadingIndicator();
	getRewards(userId);

	// Hide All Users
	var all_users = document.getElementById("all_users");
	all_users.className = 'hiddenRight';

	// Show User Details
	var user_details = document.getElementById("user_details");
	user_details.className = 'visibleLeft';

	// Glue all hidden user_*_<userUd> divs to user_details_control_row spans
	document.getElementById("user_details_userId").innerHTML = userId;
	document.getElementById("user_details_status_icon").innerHTML = document.getElementById("user_status_icon_"+ userId).innerHTML;
	document.getElementById("user_details_createddate").innerHTML = document.getElementById("user_createddate_"+ userId).innerHTML;
	document.getElementById("user_details_status").innerHTML = (document.getElementById("user_status_"+ userId).innerHTML == "ACTIVE") ? "✔️" : "🚫";
	document.getElementById("user_details_status").innerHTML += " "+ document.getElementById("user_status_"+ userId).innerHTML;
	///document.getElementById("user_details_points").innerHTML = document.getElementById("user_points_"+ userId).innerHTML;  // We now let getRewards() set points
	document.getElementById("user_details_balance").innerHTML = parseFloat(document.getElementById("user_balance_"+ userId).innerHTML).toFixed(2);
	document.getElementById("user_details_ccinfo").innerHTML = (document.getElementById("user_ccinfo_"+ userId).innerHTML == "No card on file") ? "🚫" : "✔️";
	document.getElementById("user_details_ccinfo").innerHTML += " "+ document.getElementById("user_ccinfo_"+ userId).innerHTML;
	document.getElementById("user_details_pushnotify").innerHTML = (document.getElementById("user_pushnotify_"+ userId).innerHTML == "Y") ? "✔️" : "🚫";
	document.getElementById("user_details_pushnotify").innerHTML += " Notifications";
	document.getElementById("user_details_name").innerHTML = document.getElementById("user_name_"+ userId).innerHTML;
	document.getElementById("user_details_pic").src = document.getElementById("user_pic_"+ userId).src;
	document.getElementById("user_details_favoriteproduct").innerHTML = document.getElementById("user_favoriteproduct_"+ userId).innerHTML;

	// Set states of User Details buttons
	document.getElementById("checkin_user").className = (checkedinUsers[userId]) ? "nodisplay" : "activeButton";
	document.getElementById("select_user").className  = (checkedinUsers[userId] && userId != selectedUserId && !lockSelectedUser) ? "activeButton" : "inactiveButton";
}

function loadUserPics(users, isCheckin) {
	var checkin_pics = document.getElementById("checkin_pics");

	// Clear out any checkin pics we may be showing
	var i = 0;  // infinite loop insurance
	while (checkin_pics.firstChild && i++ < 100) {
		checkin_pics.removeChild(checkin_pics.firstChild);
	}

	// Create figcaption & friends for each search user
	for (var i = 0; i < users.length; i++) {
		var user = users[i];

		// For checkins, we don't have the user profile picture, so we have to make an ajax call to get it
		if (isCheckin) {
			checkedinUsers[user.userId] = true;
			getUserForCheckin(user.userId);
		}

		// Create and append figure element
		var userPic = createOrUpdateUserPic(user, isCheckin);
		checkin_pics.appendChild(userPic);
	}

	// Display no results message to user
	if (users.length == 0) {
		var noResultsMsg = document.createElement("h3");
		noResultsMsg.innerHTML = (hasSearched) ? "No search results found" : "No check-ins found";
		checkin_pics.appendChild(noResultsMsg);
	}
}

function createOrUpdateUserPic(user, isCheckin) {
	var userId = (user.userId) ? user.userId : user.id;

	var userPic = document.getElementById(userId);
	if (userPic == null) {
		userPic = document.createElement("figure");
		userPic.id = userId;
		userPic.ontouchstart = function () { loadUserDetails(userId) };
	}

	var checkinIcon = document.getElementById("user_status_icon_"+ userId);
	if (checkinIcon == null) {
		checkinIcon = document.createElement("figcaption");
		checkinIcon.id = "user_status_icon_"+ userId;
		checkinIcon.className = "user_status_icon";
		userPic.appendChild(checkinIcon);
	}
	checkinIcon.innerHTML = (isCheckin || checkedinUsers[userId]) ? "✔️" : "";

	var img = document.getElementById("user_pic_"+ userId);
	if (img == null) {
		img = document.createElement("img");
		img.id = "user_pic_"+ userId;
		img.className = "user_pic";
		img.src = "/components/order_addons/pepper/images/no_profile_pic.png";
		userPic.appendChild(img);
	}
	if (user.hasProfilePhoto) img.src = "http://api.pepperhq.com/users/"+ userId +"/avatar.jpg";
	if (userId == selectedUserId) img.classList.add("selected_userpic");

	var fullName = document.getElementById("user_name_"+ userId);
	if (fullName == null) {
		fullName = document.createElement("figcaption");
		fullName.id = "user_name_"+ userId;
		fullName.className = "user_fullname";
		userPic.appendChild(fullName);
	}
	fullName.innerHTML = (user.userFullName != null) ? user.userFullName : user.fullName;

	var created = document.getElementById("user_createddate_"+ userId);
	if (created == null) {
		created = document.createElement("figcaption");
		created.id = "user_createddate_"+ userId;
		created.className = "nodisplay";
		userPic.appendChild(created);
	}
	var createdDate = new Date(user.created);
	created.innerHTML = createdDate.toLocaleDateString("en-US");

	var status = document.getElementById("user_status_"+ userId);
	if (status == null) {
		status = document.createElement("figcaption");
		status.id = "user_status_"+ userId;
		status.className = "nodisplay";
		userPic.appendChild(status);
	}
	status.innerHTML = user.state;

	var points = document.getElementById("user_points_"+ userId);
	if (points == null) {
		var points = document.createElement("figcaption");
		points.id = "user_points_"+ userId;
		points.className = "nodisplay";
		userPic.appendChild(points);
	}
	points.innerHTML = 0;  // user.points is deprecated; must now get points out of awards array, which is fetched when view a user

	var balance = document.getElementById("user_balance_"+ userId);
	if (balance == null) {
		balance = document.createElement("figcaption");
		balance.id = "user_balance_"+ userId;
		balance.className = "nodisplay";
		userPic.appendChild(balance);
	}
	balance.innerHTML = user.balance;

	var ccinfo = document.getElementById("user_ccinfo_"+ userId);
	if (ccinfo == null) {
		ccinfo = document.createElement("figcaption");
		ccinfo.id = "user_ccinfo_"+ userId;
		ccinfo.className = "nodisplay";
		userPic.appendChild(ccinfo);
	}
	ccinfo.innerHTML = (user.card) ? user.card.type +" xxxx"+ user.card.last4 : "";

	var pushnotify = document.getElementById("user_pushnotify_"+ userId);
	if (pushnotify == null) {
		pushnotify = document.createElement("figcaption");
		pushnotify.id = "user_pushnotify_"+ userId;
		pushnotify.className = "nodisplay";
		userPic.appendChild(pushnotify);
	}
	pushnotify.innerHTML = user.isRegisteredForPushNotifications;

	var subscribed = document.getElementById("user_subscribed_"+ userId);
	if (subscribed == null) {
		subscribed = document.createElement("figcaption");
		subscribed.id = "user_subscribed_"+ userId;
		subscribed.className = "nodisplay";
		userPic.appendChild(subscribed);
	}
	subscribed.innerHTML = user.subscribed;

	var userid = document.getElementById("user_userid_"+ userId);
	if (userid == null) {
		userid = document.createElement("figcaption");
		userid.id = "user_userid_"+ userId;
		userid.className = "nodisplay";
		userPic.appendChild(userid);
	}
	userid.innerHTML = userId;

	var favoriteproduct = document.getElementById("user_favoriteproduct_"+ userId);
	if (favoriteproduct == null) {
		favoriteproduct = document.createElement("figcaption");
		favoriteproduct.id = "user_favoriteproduct_"+ userId;
		favoriteproduct.className = "nodisplay";
		userPic.appendChild(favoriteproduct);
	}
	favoriteproduct.innerHTML = (user.favouriteProducts) ? user.favouriteProducts.map(fp => fp.title).join(", ") : "(None)";

	return userPic;
}

function populateAmount(selectedOrderOption) {
	var vals = selectedOrderOption.value.split('^', 2);
	var total = (vals[1] != null) ? vals[1] : '';
	document.getElementById("amount").value = total;
	document.getElementById("action_pay").disabled = false;
}

function flipCss(elementId) {
	document.getElementById(elementId).style.className = (document.getElementById(elementId).style.className == "flipped") ? "" : "flipped";
	return false;
}

function closeWebview() {
	window.location = '_DO:cmd=close_overlay&c_name=customer&f_name=customer';  // seems like we shouldn't set things if we're just closing
}

function closeUserDetails() {
	// Hide User Details
	var user_details = document.getElementById("user_details");
	user_details.className = 'hiddenLeft';

	// Show All Users
	var all_users = document.getElementById("all_users");
	all_users.className = 'visibleRight';
}

function updateUserCheckin(userId) {
	// Update checkedinUsers array
	checkedinUsers[userId] = true;

	// Put check mark on User Pic
	var checkinIcon = document.getElementById("user_status_icon_"+ userId);
	checkinIcon.innerHTML = "✔️";
	checkinIcon.classList.add("changed_checkin_status");

	// See if our userId is being displayed on User Details screen
	var userDetailsUserId = document.getElementById("user_details_userId");
	if (userDetailsUserId.innerHTML == userId) {

		// Put check mark emoji on + change css class for transition
		var userDetailsCheckinIcon = document.getElementById("user_details_status_icon");
		userDetailsCheckinIcon.innerHTML = "✔️";
		userDetailsCheckinIcon.classList.add("changed_checkin_status");

		// Update states of User Details buttons
		document.getElementById("select_user").className  = "activeButton";
		document.getElementById("checkin_user").className = "inactiveButton";
		document.getElementById("checkin_user").disabled = true;
	}

	// TO DO : remove loading spinner (?)
}


function checkinUser(userId) {
	var xhr = new XMLHttpRequest();
	xhr.onreadystatechange = function () {
		if (this.readyState == 4 && this.status == 200) {
			//console.log("checkinUser response="+ this.responseText);  //debug
			var checkin = JSON.parse(this.responseText);
			updateUserCheckin(checkin.userId);
		}
	}
	xhr.onErrorListener = function(errorMsg) {
		console.error(errorMsg);
	}
	xhr.open("GET", "/components/order_addons/pepper/webview.php?action=checkin&dataname="+ encodeURIComponent(dataname) +"&locationId="+ encodeURIComponent(locationId) +"&userId="+ encodeURIComponent(userId) +"&type=MANUAL", true);
	xhr.withCredentials = true;
	xhr.send();

	// spinner on button?
}


function selectUser(userId, fullName) {
	if (lockSelectedUser) {
		return;
	}

	/*// Loading spinner
	var loadingImg = document.createElement("img");
	loadingImg.setAttribute("src", "/cp/images/loading_transparent.gif");*/

	// Chain _DO:cmd's together so they subsequent calls don't cancel the previous one
	var doCmds = "";

	// If the previously selected user is different than the new one, then clear the discounts
	if (selectedUserId && userId != selectedUserId) {
		doCmds += "_DO:cmd=resetDiscounts&type=check_and_item";

		// Unhighlight previously selected user's user pic
		var previouslySelectedUserPic = document.getElementById("user_pic_"+ selectedUserId);
		if (previouslySelectedUserPic) {
			previouslySelectedUserPic.classList.remove("selected_userpic");
		}
	}

	selectedUserId = userId;

	// Animated check mark on user pic
	var user_status_icon = document.getElementById("user_status_icon_"+ userId);
	user_status_icon.classList.remove("changed_checkin_status");
	user_status_icon.innerHTML = "✔️";
	user_status_icon.classList.add("changed_checkin_status");

	// Highlight user pic from checkins list
	var selectedUserPic = document.getElementById("user_pic_"+ userId);
	if (selectedUserPic) selectedUserPic.classList.add("selected_userpic");

	// Associate loyalty user with order
	// customer = mercury_loyalty|common name|account number
	///var doCmdForLoyaltyUserAndDiscount = "_DO:cmd=set_loyalty&customer=pepper_loyalty|"+ encodeURIComponent(fullName) +"|"+ encodeURIComponent(userId);
	doCmds += "_DO:cmd=set_loyalty&customer=pepper_loyalty|"+ encodeURIComponent(fullName) +"|"+ encodeURIComponent(userId);

	// Use Selected Reward
	// discount = type|calc_type|calc|label|redeem_id|type_id|designator
	if (rewardElementId != null) {
		//console.log("setting discount using rewardElementId="+ rewardElementId);  //debug
		var rewardButton = document.getElementById(rewardElementId);
		doCmds += "&limit_to="+ rewardButton.getAttribute("limit_to") +"&discount="+ rewardButton.getAttribute("type") +"|"+ rewardButton.getAttribute("calc_type") +"|"+ rewardButton.getAttribute("calc") +"|"+ rewardButton.getAttribute("label") +"|"+ rewardButton.getAttribute("redeem_id") +"|"+ rewardButton.getAttribute("type_id") +"|"+ rewardButton.getAttribute("designator");
	}

	// Set loyalty user select and discount
	///window.setTimeout( function() { window.location = doCmdForLoyaltyUserAndDiscount; }, 400 );
	///console.log(doCmdForLoyaltyUserAndDiscount);  //debug

	/*// Ajax call to save userId to order_id mapping in session - in case they use the APP to pay
	var xhr = new XMLHttpRequest();
	xhr.onreadystatechange = function () {
		if (this.readyState == 4 && this.status == 200) {
			var response = JSON.parse(this.responseText);
			if (response.success) {
				//var orderInfoJson = '{}';
				var orderInfoJson = '{}';
				window.location = "_DO:cmd=close_overlay&set_info="+ encodeURIComponent(fullName) +"|o|"+ encodeURIComponent(userId) +"|o|"+ encodeURIComponent(orderInfoJson);
				window.setTimeout(closeWebview, 500);  //debug (?)
			}
			else {
				console.error("Could not save original_id to session");
			}
		}
	}
	xhr.onErrorListener = function(errorMsg) {
		console.error(errorMsg);
	}
	xhr.open("GET", "/components/order_addons/pepper/webview.php?action=save&dataname="+ encodeURIComponent(dataname) +"&locationId="+ encodeURIComponent(locationId) +"&order_id="+ encodeURIComponent(order_id) +"&userId="+ encodeURIComponent(userId), true);
	xhr.withCredentials = true;
	xhr.send();*/

	//var orderInfoJson = '{}';
	var orderInfoJson = '{"loyaltyInfo":{"orderId":""}}';
	window.location = doCmds + "_DO:cmd=close_overlay&set_info="+ encodeURIComponent(fullName) +"|o|"+ encodeURIComponent(userId) +"|o|"+ encodeURIComponent(orderInfoJson);
}

var importPointsUserId = "";
function importPoints(userId) {
	importPointsUserId = userId;

	/// TO DO : open a pop-up another way because the "create_overlay" _DO:cmd isn't available on our screen
	/// Open another overlay for swipe screen
	///window.location = "_DO:cmd=create_overlay&c_name=import&f_name=payment_extensions/pepper/webview.php&dims=353,671,154,614&scroll=0";

	window.location = "_DO:cmd=startListeningForSwipe";
	//window.location = "_DO:cmd=startListeningForKEI";  // TO DO : replace this line with the line below for simulator testing (since swiper doesn't work)
	document.getElementById("import_points_msg").innerHTML = "Swipe card";
}

function handleKEI(rfidCardData) {
	// Display card data for debugging
	var import_points_msg  = document.getElementById("import_points_msg");
	import_points_msg.innerHTML += "<br>"+ rfidCardData;

	// Ajax call to transfer points, if record for legacy rewards card found
	importPointsForUser(importPointsUserId, pointsAwardId, rfidCardData);

	// Stop listening for KEI, since we no longer need it (they can just hit the import button again, if so)
	window.setTimeout( function() { window.location = "_DO:cmd=stopListeningForKEI"  }, 50 );

	return 1;
}

var swiped = false;
var swipeString = "";
function handleCardSwipe(swipeStringEncoded, hexString, reader, encrypted)
{

	/*if (swiped)
	{
		return;
	}

	swiped = true;
	var cardNumber	= "";
	var cardRead	= false;

	/*if (encrypted == 0)
	{
		swipeString = decodeURIComponent(swipeStringEncoded);

		if (swipeString.length > 0)
		{
			var tracks = swipeString.split("?");
			for (var i = 0; i < tracks.length; i++)
			{
				var trk = tracks[i].replace(/[^a-z;=\d]+/ig,'');
				if (trk.substring(0, 1) == ";")
				{
					var track2 = trk.split("=");
					cardNumber = track2[0].trim("; ");
					if (cardNumber.length > 0)
					{
						break;
					}
				}
			}
		}
	}*/

		if (encrypted == 0)
		{
			swipeString = decodeURIComponent(swipeStringEncoded);

			if (swipeString.length > 0)
			{
				var tracks = swipeString.split("?");
				for (var i = 0; i < tracks.length; i++)
				{
					var trk = tracks[i].replace(/[^a-z;=\d]+/ig,'');
					if (trk.substring(0, 1) == ";")
					{
						var track2 = trk.split("=");
						cardNumber = track2[0].trim("; ");
						if (cardNumber.length > 0)
						{
							break;
						}
					}
				}
			}
		}
		else
		{
			switch (reader)
			{
				case "idynamo":

					swipeString = decodeURIComponent(swipeStringEncoded);

					var tracks = swipeString.split("?");

					for (var i = 0; i < tracks.length; i++)
					{
						if (tracks[i].substring(0, 1) == ";")
						{
							cardNumber = tracks[i].split(';')[1].split('=')[0];
						}
					}

					break;

				/*case "llsswiper":

					if (hexString.length > 0)
					{
						var actual_swipe = fromHex(hexString);
						if (actual_swipe.length == 1714)
						{
							cardRead = true;
							cardNumber = parseCardNumberFromHeartlandE3Swipe(actual_swipe);
						}
						else
						{
							var tracks		= actual_swipe.split("?");
							var track_parts	= tracks[0].split(";");

							cardNumber	= track_parts[1];
							cardRead	= (cardNumber.length > 0);
						}
					}

					if (!cardRead && (swipeString.length > 0))
					{
						var tracks = swipeString.split("?");

						for (var i = 0; i < tracks.length; i++)
						{
							var trk = tracks[i].replace(/[^a-z;=\d]+/ig,'');

							if (trk.substring(0, 1) == ";")
							{
								var track2 = trk.split("=");

								cardNumber = track2[0].trim("; ");
								if (cardNumber.length > 0)
								{
									break;
								}
							}
						}
					}

					break;*/

				default:
					break;
			}
		}


	if (cardNumber != "")
	{
		window.setTimeout( function() {
			var import_points_msg  = document.getElementById("import_points_msg");
			import_points_msg.innerHTML += "<br>"+ cardNumber;

			// Ajax call to transfer points, if record for legacy rewards card found
			importPointsForUser(importPointsUserId, pointsAwardId, cardNumber);
		}, 50 );
	}
	else
	{
		swiped = false;
	}

	swiped = false;
	return 1;
}

function goBackFromUserDetails() {
	rewardElementId = null;
	closeUserDetails();

	// Get checkins if we started on User Details screen
	if (startOnUserDetails) {
		// Clear out any checkin pics we may be showing
		var i = 0;  // infinite loop insurance
		while (checkin_pics.firstChild && i++ < 100) {
			checkin_pics.removeChild(checkin_pics.firstChild);
		}

		// Show loading spinner
		var loadingSpinner = document.createElement("img");
		loadingSpinner.src = "/cp/images/loading_transparent.gif";
		checkin_pics.appendChild(loadingSpinner);


		getCheckins();
	}
}

var hasSearched = false;
var searchField = null;
var search_clear_button = null;

function searchClearButton(state) {
	if (search_clear_button == null) search_clear_button = document.getElementById("search_clear_button");
	search_clear_button.className = (state == 'show') ? "" : "nodisplay";
}

function searchOnFocus() {
	if (searchField == null) searchField = document.getElementById("search");

	if (searchField.value == emptySearchFieldStr) {
		// Blank default search text so user can enter search text
		searchField.style.color = "black";
		searchField.value = "";
	}
	else {
		searchClearButton("show");
	}
}

function searchOnBlur() {

	if (searchField.value == "") {
		// If no search text was entered, restore default search text
		searchField.style.color = "#cccccc";
		searchField.value = emptySearchFieldStr;

		searchClearButton("hide");
	}
	else if (searchField.value != "" && searchField.value != emptySearchFieldStr) {
		// Show search clear button
		search_clear_button.className = "";
	}
}

function searchOnInput() {
	if (searchField == null) searchField = document.getElementById("search");

	if (searchField.value == "") {
		searchClearButton("hide");
	}
	else {
		searchClearButton("show");
	}
}

function searchCleared() {
	window.location = "_DO:cmd=resignKeyboard";
	if (searchField == null) searchField = document.getElementById("search");

	// Blank out the search field again
	searchField.value = "";
	searchOnBlur();

	// Hide search clear button
	searchClearButton("hide");

	// If user has performed search, the user_pics will be the seach results, so
	// we will need to reset them to being the checkins - which we will get fresh
	if (hasSearched) {
		hasSearched = false;
		getCheckins();
	}

	// Clear out any checkin pics we may be showing
	var i = 0;  // infinite loop insurance
	while (checkin_pics.firstChild && i++ < 100) {
		checkin_pics.removeChild(checkin_pics.firstChild);
	}

	// Show loading spinner
	var loadingSpinner = document.createElement("img");
	loadingSpinner.src = "/cp/images/loading_transparent.gif";
	checkin_pics.appendChild(loadingSpinner);
}

function search(searchTerm) {
	var xhr = new XMLHttpRequest();
	xhr.onreadystatechange = function () {
		if (this.readyState == 4 && this.status == 200) {
			var users = JSON.parse(this.responseText);
			loadUserPics(users, false);
		}
	}
	xhr.onErrorListener = function(errorMsg) {
		console.error(errorMsg);
	}
	xhr.open("GET", "/components/order_addons/pepper/webview.php?action=search&dataname="+ encodeURIComponent(dataname) +"&locationId="+ encodeURIComponent(locationId) +"&name="+ encodeURIComponent(searchTerm), true);
	xhr.withCredentials = true;
	xhr.send();
	hasSearched = true;

	var checkin_pics = document.getElementById("checkin_pics");

	// Clear out any checkin pics we may be showing
	var i = 0;  // infinite loop insurance
	while (checkin_pics.firstChild && i++ < 100) {
		checkin_pics.removeChild(checkin_pics.firstChild);
	}

	// Show loading spinner
	var loadingSpinner = document.createElement("img");
	loadingSpinner.src = "/cp/images/loading_transparent.gif";
	checkin_pics.appendChild(loadingSpinner);
}

var refreshButtonRotated = false;
function animateRefreshButton() {
	if (refreshButtonRotated) {
		document.getElementById("refresh_button").classList.remove("rotating");
	} else {
		document.getElementById("refresh_button").classList.add("rotating");
	}
	refreshButtonRotated = !refreshButtonRotated;
}

var refreshStarted = false;
var refreshAnimation = null;
function refresh() {
	if (!refreshStarted) {
		startedRefresh = true;
		document.getElementById("refresh_button").classList.add("rotating");
		//refreshAnimation = window.setInterval(animateRefreshButton, 500);

		getCheckins();

		//startedRefresh = false;
		//window.clearInterval(refreshAnimation);
	}
}

function importPointsForUser(userId, awardId, legacyRewardsCardNumber) {
		var xhr = new XMLHttpRequest();
		xhr.onreadystatechange = function () {
				if (this.readyState == 4 && this.status == 200) {
					// Display points import results
					var results = JSON.parse(this.responseText);
					showImportResults(results);
				}
		}
		xhr.onErrorListener = function(errorMsg) {
				console.error(errorMsg);
		}
		xhr.open("GET", "/components/order_addons/pepper/webview.php?action=import&dataname="+ encodeURIComponent(dataname) +"&loc_id="+ encodeURIComponent(loc_id) +"&awardId="+ encodeURIComponent(awardId) +"&userId="+ encodeURIComponent(userId) +"&card_number="+ encodeURIComponent(legacyRewardsCardNumber), true);
		xhr.withCredentials = true;
		xhr.send();
		hasSearched = true;

		var import_points_msg = document.getElementById("import_points_msg");

		// Show loading spinner
		var loadingSpinner = document.createElement("img");
		loadingSpinner.src = "/cp/images/loading_transparent.gif";
		import_points_msg.appendChild(loadingSpinner);

}

function getCheckins() {
	var xhr = new XMLHttpRequest();
	xhr.onreadystatechange = function () {
		if (this.readyState == 4 && this.status == 200) {
			var checkins = JSON.parse(this.responseText);
			// Sort checkins by most recent checkin first
			checkins.sort( function (a, b) {
				return new Date(b.updated) - new Date(a.updated);
			});

			loadUserPics(checkins, true);

			startedRefresh = false;
			document.getElementById("refresh_button").classList.remove("rotating");
		}
	}
	xhr.onErrorListener = function(errorMsg) {
		console.error(errorMsg);
	}
	xhr.open("GET", "/components/order_addons/pepper/webview.php?action=checkins&dataname="+ encodeURIComponent(dataname) +"&locationId="+ encodeURIComponent(locationId), true);
	xhr.withCredentials = true;
	xhr.send();
}

function getUserForCheckin(userId) {
	var xhr = new XMLHttpRequest();
	xhr.onreadystatechange = function () {
		if (this.readyState == 4 && this.status == 200) {
			var user = JSON.parse(this.responseText);
			createOrUpdateUserPic(user, true);
			// Update User Details screen if they were viewing our user already
			if (document.getElementById("user_details_userId").innerHTML == user.id && !startOnUserDetails) loadUserDetails(user.id);
		}
	}
	xhr.onErrorListener = function(errorMsg) {
		console.error(errorMsg);
	}
	xhr.open("GET", "/components/order_addons/pepper/webview.php?action=user&dataname="+ encodeURIComponent(dataname) +"&locationId="+ encodeURIComponent(locationId) +"&userId="+ encodeURIComponent(userId), true);
	xhr.withCredentials = true;
	xhr.send();
}

function getUserToDisplayUserDetails(userId) {
	var xhr = new XMLHttpRequest();
	xhr.onreadystatechange = function () {
		if (this.readyState == 4 && this.status == 200) {
			var user = JSON.parse(this.responseText);

			// Display user details (which requires we first have a user pic)
			loadUserPics([user]);
			loadUserDetails(user.id);
		}
	}
	xhr.onErrorListener = function(errorMsg) {
		console.error(errorMsg);
	}
	xhr.open("GET", "/components/order_addons/pepper/webview.php?action=user&dataname="+ encodeURIComponent(dataname) +"&locationId="+ encodeURIComponent(locationId) +"&userId="+ encodeURIComponent(userId), true);
	xhr.withCredentials = true;
	xhr.send();
}

// LP-7377 - Build a list of the item-level discounts already used in the event that the order already has discounts
function populateItemLevelDiscountsUsed() {
	for (var i = 0; i < orderInfo.contents.length; i++) {
		var orderContentItem = orderInfo.contents[i];
		if (orderContentItem.idiscount_info === "") {
			continue;
		}
		var iDiscountInfoParts = orderContentItem.idiscount_info.split("|");
		var discountId = iDiscountInfoParts[2];
		if (discountId) {
			itemLevelDiscountsUsed[discountId] = orderContentItem.icid;
		}
	}
}

// Gets values for each POS order object field specified as input arguments
// Note: _DO cmd requires callback function in js parameter or the default handler ("gotOrderValue(values)") to be defined.  Callbacks must return 1.
function getOrderValuesFromPos() {
		if (arguments.length > 0) {
				// Convert arguments object to array (since it has length property but no other Array methods)
				var fields = Array.prototype.slice.call(arguments);

				window.location = "_DO:cmd=getOrderValue&fields="+ fields.join(",") +"&js=processOrderValues('[ORDER_VALUE]')";
		}
}

function getActiveOrderInfoFromPos() {
	window.location = "_DO:cmd=get_active_order_info&discounts=1&js=processOrderInfo('[ORDER_INFO]')";
}

function processOrderInfo(encodedOrderInfo) {
	// LP-7377 - Non-blank idiscount_sh values will be strings with unencoded % characters like "1 Free Coffee Discount (50%)" so replace them with the URL encoding for % (%25)
	var encodedOrderInfoWithoutPercents = encodedOrderInfo.replace(/[%][)]/g, '%25)');

	var orderInfoJson = decodeURIComponent(encodedOrderInfoWithoutPercents);
	orderInfo = JSON.parse(orderInfoJson);

	populateItemLevelDiscountsUsed();

	// Had to put this in here becasue calling _DO:cmd and ajax call simultaneously in the start slowed the loading down noticeably
	getOrderValuesFromPos("order_id", "original_id", "order_status");

	// POS requires this call to return 1 otherwise an error message is thrown
	return 1;
}

function processOrderValues(encodedOrderValuesJson) {
	var orderValuesJson = decodeURIComponent(encodedOrderValuesJson);
	orderValues = JSON.parse(orderValuesJson);

	// Prevent the Pepper user from being changed if the order has ever been closed (meaning even if order_status "reopened")
	orderIsClosed = (orderValues.order_status != 'open');

	// Get any existing info from order.original_id
	original_id    = orderValues.original_id.split("|o|");
	posUserLabel   = original_id[1];
	userFullName   = original_id[2];
	selectedUserId = original_id[3];
	extraInfoJson  = original_id[4];  // {"loyaltyInfo":{"orderId":"a1b2c3"}}
	extraInfo      = (extraInfoJson) ? JSON.parse(extraInfoJson) : {};

	// Load User Details for user selected in a previous webview session or via Pre-Order
	if (selectedUserId) {
		startOnUserDetails = true;

		// Show User Details screen
		var all_users = document.getElementById("all_users");
		var user_details = document.getElementById("user_details");
		all_users.className = 'hiddenRight';
		user_details.className = 'visibleLeft';

		// Display at least the user's full name, since we have it, while the rest of their info is retrieved
		var userDetailsFullName = document.getElementById("user_details_name");
		if (userDetailsFullName) userDetailsFullName.innerHTML = userFullName;

		// If we already have an orderId then the user has already been associated
		// with the order on the Pepper side, so do not allow the user to be changed
		if (extraInfo.loyaltyInfo.orderId || orderIsClosed) {
			lockSelectedUser = true;
		}

		// Get user details so we can display it.  We don't just get checkins because Pre-Order user may not be checked in.
		getUserToDisplayUserDetails(selectedUserId);
	}
	// Display all checkins
	else {
		// Had to put this in here becasue calling _DO:cmd and ajax call simultaneously in the start slowed the loading down noticeably
		getCheckins();
	}

	// POS requires this call to return 1 otherwise an error message is thrown
	return 1;
}

</script>


<section>

<div id="close_button" ontouchstart="closeWebview()"></div>

<div id="all_users" class="visibleRight">
	<h1>Loyalty Check-ins</h1>
	<h2>Select user</h2>
	<input type="text" id="search" value="" onfocus="searchOnFocus()" onblur="searchOnBlur()" oninput="searchOnInput()">
	<div id="search_clear_button" class="nodisplay" ontouchstart="searchCleared()">✖️</div>
	<div id="refresh" ontouchstart="refresh()">
		<img id="refresh_button" src="/components/order_addons/images/icon_refresh2x.png"> Refresh Check-ins
	</div>
	<div id="checkin_pics">
		<img src="/cp/images/loading_transparent.gif">
	</div>
</div>

<div id="user_details" class="hiddenLeft">
	<h1 id="user_details_name">User Details</h1>
	<img id="user_details_back_arrow" src="/components/order_addons/images/back_arrow.png" ontouchstart="goBackFromUserDetails()">

	<button id="checkin_user" class="inactiveButton" ontouchstart="checkinUser(document.getElementById('user_details_userId').innerHTML)">CHECK-IN USER</button>
	<button id="select_user" class="activeButton" ontouchstart="selectUser(document.getElementById('user_details_userId').innerHTML, document.getElementById('user_details_name').innerHTML)">SELECT USER</button>
	<button id="import_points" class="hiddenButton" ontouchstart="importPoints(document.getElementById('user_details_userId').innerHTML, document.getElementById('user_details_name').innerHTML)">IMPORT POINTS</button>
	<div id="import_points_msg"></div>

	<h1 id="loyalty_program_header">LOYALTY PROGRAM</h1>

	<div id="user_details_rewards">
		<span id="rewards_label">Available Rewards</span>
		<ul id="user_rewards">
			<li>(None)</li>
		</ul>
	</div>

	<h1 id="app_stats_header">LOYALTY APP</h1>

	<div id="app_stats">
		<div class="stats_column">
			<div class="user_details_control_row">
				<span class="user_details_labels">App Balance</span> <span id="user_details_balance">0</span>
			</div>
			<div class="user_details_control_row nodisplay">
				<span class="user_details_labels">UserId</span> <span id="user_details_userId"></span>
			</div>
			<div class="user_details_control_row">
				<span id="user_details_status">UNKNOWN STATUS</span>
			</div>
			<div class="user_details_control_row">
				<span id="user_details_ccinfo">None</span>
			</div>
			<div class="user_details_control_row">
				<span id="user_details_pushnotify">N</span>
			</div>
		</div>
	</div>

	<div id="stats">
		<div class="stats_column">
			<div class="user_details_control_cell nodisplay" id="user_details_loyalty_points_label">
				<span class="user_details_labels" id="user_details_points_label">Points</span> <span id="user_details_points">0</span>
			</div>
			<div class="user_details_control_cell nodisplay" id="user_details_loyalty_stamps_label">
				<span class="user_details_labels" id="user_details_stamps_label">Stamps</span> <span id="user_details_stamps">0</span>
				<img id="user_details_stamps_next_button" src="/components/order_addons/images/next_arrow.png" class="nodisplay" ontouchstart="showNextStampCard()">
			</div>
			<div class="user_details_control_row">
				<span class="user_details_labels">Member Since</span> <span id="user_details_createddate">--/--/----</span>
			</div>
			<div class="user_details_control_row">
				<span class="user_details_labels">Favorite Product</span> <span id="user_details_favoriteproduct">Unknown</span>
			</div>
		</div>
	</div>

	<figure id="user_details_figure">
		<figcaption id="user_details_status_icon"></figcaption>
		<img id="user_details_pic" src="/components/order_addons/pepper/images/no_profile_pic.png" width="162" height="162" border="0" />
		<figcaption id="user_details_name_old"></figcaption>
		<figcaption id="user_details_stats" class="user_details_stats"></figcaption>
	</figure>

</div>


</section>

<script>
var searchField = document.getElementById("search");
// Set default text in search field
searchField.value = emptySearchFieldStr;
// Search when enter key is hit
searchField.addEventListener("keydown", function (event) {
	if (event.keyCode == 13) {
		search(this.value);
	}
});

// Get user checkins
document.addEventListener('DOMContentLoaded', function() {
	if (showLoyaltyPoints) document.getElementById("user_details_loyalty_points_label").classList.remove("nodisplay");
	if (showLoyaltyStamps) document.getElementById("user_details_loyalty_stamps_label").classList.remove("nodisplay");
	if (showImportLegacyPoints) document.getElementById("import_points").className = "activeButton";
	getActiveOrderInfoFromPos();
}, false);

// Prevent elastic scrolling on everything - except things with the css class "scrollable" to scroll
document.body.addEventListener('touchmove',function(e){
	e.preventDefault();
});

</script>

</body>
</html>

<?php

class PepperWebview
{
	private $api;

	private $checkins;
	private $users;
	private $rewards;

	public $dataname;
	public $loc_id;
	public $order_id;
	public $chain_id;
	//public $check;

	public $pepper_config;

	public $tenantId;
	public $locationId;

	public $referer = 'webview';

	function __construct($vars)
	{
		if (!empty($vars['dn']))
		{
			if (empty($vars['dataname'])) $vars['dataname'] = $vars['dn'];
			unset($vars['dn']);  // So this arg doesn't trickle down into Pepper API payloads
		}

		if (empty($vars['dataname']))
		{
			// The app passes webviews company_id but not dataname
			$restaurant = $this->queryDatabase('restaurant_by_company_id', $vars);
			$this->chain_id = $restaurant['chain_id'];
			$vars['dataname'] = $restaurant['data_name'];
		}

		// LP-5542 - Need restaurant row to get chain_id for import record query
		if (!empty($vars['action']) && $vars['action'] == 'import')
		{
			$restaurant = $this->queryDatabase('restaurant_by_dataname', $vars);
			$this->chain_id = $restaurant['chain_id'];
		}

		$this->getPepperConfig($vars);

		$this->dataname = $vars['dataname'];

		if (empty($vars['tenantId']))
		{
			$vars['tenantId'] = $this->pepper_config['pepper_tenantid'];
		}

		if (empty($vars['locationId']))
		{
			$vars['locationId'] = $this->pepper_config['pepper_locationid'];;
		}

		$this->tenantId = $vars['tenantId'];
		$this->locationId = $vars['locationId'];
		$this->order_id = $vars['order_id'];
		$this->check  = isset($vars['check']) ? $vars['check'] : '1';
		$this->loc_id = isset($vars['loc_id']) ? $vars['loc_id'] : '1';

		$vars['loc_id'] = $this->loc_id;  // TO DO : fix this hack

		$vars['referer'] = $this->referer;

		$this->api = new PepperApi($vars);
	}

	public function routeRequest($vars)
	{
		$action = isset($vars['action']) ? strtolower($vars['action']) : '';

		switch ($action)
		{
			// Ajax request responders

			case 'checkin':
				$this->performCheckIn($vars);
				exit;
				break;

			case 'checkins':
				$this->getCheckins();
				echo json_encode($this->checkins);
				exit;
				break;

			case 'open_order_ids':
				echo json_encode($this->getOpenOrderIDs($vars));
				exit;
				break;

			case 'import':
				$results_json = json_encode($this->performImportPoints($vars));
				echo $results_json;
				exit;
				break;

			case 'pay':
				$result = $this->performPay($vars);
				echo json_encode($result);
				exit;
				break;

			/*case 'app_pay':
				$result = $this->performAppPay($vars);
				echo json_encode($result);
				exit;
				break;

			case 'points_pay':
				$result = $this->performPointsPay($vars);
				echo json_encode($result);
				exit;
				break;*/

			case 'rewards':
				$this->getRewards($vars);
				echo json_encode($this->rewards);
				exit;
				break;

			case 'save':
				echo json_encode($this->setUserIdInSession($_REQUEST));
				exit;
				break;

			case 'search':
				$response = $this->api->getUsers($vars);
				$users = isset($response['users']) ? $response['users'] : array();
				echo json_encode($users);
				exit;
				break;

			case 'user':
				$user = $this->getUser($vars);
				echo json_encode($user);
				exit;
				break;

			case 'users':
				$this->getCheckins();
				$this->getUsers();
				break;

			// Landing Page

			default:
			case 'checkins':
				$this->getCheckins();
				break;
		}
	}

	/**
	 * "Create User Checkin" API call
	 *
	 * POST /users/{userId}/checkins
	 *
	 */
	private function performCheckIn($vars)
	{
		$response = '';

		if (empty($vars['locationId'])) $vars['locationId'] = $this->locationId;

		try
		{
			$response = $this->api->createCheckin($vars);
		}
		catch (Exception $e)
		{
			error_log($e->getMessage());
			echo json_encode(array('error' => true, 'error_msg' => $e->getMessage()));
			return;
		}

		echo json_encode($response);
	}

	/**
	 * Ajax responder method for paying with Pepper app from checkout webview
	 *
	 */
	private function performPay($vars)
	{
		$results = array();

		if (empty($vars['dataname']) || empty($vars['order_id']) || empty($vars['userId']) || empty($vars['amount']) || empty($vars['pay_type']) )
		{
			$results['message'] = "Missing required paramters for app pay";
			error_log($response['message'] .": ". json_encode($vars));
			return $results;
		}

		if (empty($vars['locationId']))
		{
			if ($this->pepper_config === null) $this->getPepperConfig($vars);
			$vars['locationId'] = $this->locationId;
		}

		if (empty($vars['registerId']))
		{
			if ($this->pepper_config === null) $this->getPepperConfig($vars);
			$vars['registerId'] = $this->pepper_config['pepper_registerid'];
		}

		if (empty($vars['orderId']))
		{
			unset($vars['orderId']);
		}

		$vars['scenario'] = 'PAYATPOS';
		$vars['shortCode'] = $vars['order_id'];
		$vars['state'] = 'OPEN';
		$vars['totalPayment'] = (double) $vars['amount'];
		$vars['totalPrice'] = (double) $vars['total'];
		$vars['totalTip'] = (double) $vars['tip'];
		$vars['totalTax'] = (double) $vars['tax'];
		$vars['value'] = (double) $vars['total'];
		$vars['extract-data'] = false;  // Do not query db for order data (since it probably won't be committed to db yet)
		$vars['update-order'] = false;  // Do not finalize and close the Pepper order - let our close orders process handle this (in case they need to add another payment type)

		$payingWithPoints = (stristr(strtolower($vars['pay_type']), 'points') !== false);

		// Points pay - no transactions created but we need to add points awardId to redemptions array
		if (!empty($vars['awardId']) && $payingWithPoints)
		{
			$vars['redemptions'] = array(array(
				'awardId'  => $vars['awardId'],
				'token'    => '',
				'quantity' => ($vars['amount'] * 100),
				'value'    => $vars['amount'],
			));
			unset($vars['awardId']);
			$vars['transactions'] = array();
		}
		// App pay - create transactions so Pepper app deducts from top-up balance
		else
		{
			// Create Pepper Transaction entity manually so we can skip querying the database (since our cc_transactions rows won't exist yet)
			$vars['transactions'] = array( array(
				'locationId'    => $vars['locationId'],
				'registerId'    => $vars['registerId'],
				'userId'        => $vars['userId'],
				'shortCode'     => $vars['order_id'],
				'paymentType'   => 'APP',  // uses app balance
				'subTotal'      => (double) $vars['total'] - (double) $vars['tax'],
				'totalPrice'    => (double) $vars['total'],
				'totalTax'      => (double) $vars['tax'],
				'totalPayment'  => (double) $vars['amount'],
				'totalTip'      => (double) $vars['tip'],
			));
		}

		try
		{
			// Send Order to Pepper
			$results = PepperIntegration::sendOrder($vars);

			// Update orders.original_id if orderId created and save results in split_check_details
			$updated_loyalty_info = $this->queryDatabase('split_check_details-loyalty_info-update', array_merge($vars, $results));
		}
		catch (Exception $e)
		{
			error_log($e->getMessage());
			echo json_encode(array('error' => true, 'error_msg' => $e->getMessage()));
			return;
		}

		return $results;
	}

	/**
	 * Note: Unused but kept for posterity
	 * Ajax responder method for paying with Pepper app from checkout webview
	 *
	 */
	private function performAppPay($vars)
	{
		$results = array();

		if (empty($vars['dataname']) || empty($vars['order_id']) || empty($vars['userId']) || empty($vars['amount']) || empty($vars['pay_type']) )
		{
			$results['message'] = "Missing required paramters for app pay";
			error_log($response['message'] .": ". json_encode($vars));
			return $results;
		}

		if (empty($vars['locationId']))
		{
			if ($this->pepper_config === null) $this->getPepperConfig($vars);
			$vars['locationId'] = $this->locationId;
		}

		if (empty($vars['registerId']))
		{
			if ($this->pepper_config === null) $this->getPepperConfig($vars);
			$vars['registerId'] = $this->pepper_config['pepper_registerid'];
		}

		if (empty($vars['orderId']))
		{
			unset($vars['orderId']);
		}

		$vars['scenario'] = 'PAYATPOS';
		$vars['shortCode'] = $vars['order_id'];
		$vars['state'] = 'OPEN';
		$vars['totalPayment'] = (double) $vars['amount'];
		$vars['totalPrice'] = (double) $vars['total'];
		$vars['totalTip'] = (double) $vars['tip'];
		$vars['totalTax'] = (double) $vars['tax'];
		$vars['value'] = (double) $vars['total'];
		$vars['extract-data'] = false;  // Do not query db for order data (since it probably won't be committed to db yet)
		$vars['update-order'] = false;  // Do not finalize and close the Pepper order - let our close orders process handle this (in case they need to add another payment type)

		// Create Pepper Transaction entity manually so we can skip querying the database (since our cc_transactions rows won't exist yet)
		$vars['transactions'] = array( array(
			'locationId'    => $vars['locationId'],
			'registerId'    => $vars['registerId'],
			'userId'        => $vars['userId'],
			'shortCode'     => $vars['order_id'],
			'paymentType'   => strtoupper($vars['pay_type']),  // CASH|CARD|POS|APP (Careful: APP charges card on file!)
			'loyaltyBurned' => ((strtoupper($vars['pay_type']) == 'POINTS') ? (double) $vars['amount'] : 0),
			'subTotal'      => (double) $vars['total'] - (double) $vars['tax'],
			'totalPrice'    => (double) $vars['total'],
			'totalTax'      => (double) $vars['tax'],
			'totalPayment'  => (double) $vars['amount'],
			'totalTip'      => (double) $vars['tip'],
		));

		try
		{
			// Send Order to Pepper
			$results = PepperIntegration::sendOrder($vars);

			// Update orders.original_id if orderId created and save results in split_check_details
			$updated_loyalty_info = $this->queryDatabase('split_check_details-loyalty_info-update', array_merge($vars, $results));
		}
		catch (Exception $e)
		{
			error_log($e->getMessage());
			echo json_encode(array('error' => true, 'error_msg' => $e->getMessage()));
			return;
		}

		return $results;
	}

	/**
	 * Note: Unused but kept for posterity
	 * Ajax responder method for paying with Pepper points from checkout webview
	 *
	 */
	private function performPointsPay($vars)
	{
		$results = array();

		if (empty($vars['dataname']) || empty($vars['order_id']) || empty($vars['check']) || empty($vars['userId']) || empty($vars['awardId']) || empty($vars['amount']) || empty($vars['pay_type']) )
		{
			$results['message'] = "Missing required paramters for points pay";
			error_log($results['message'] .": ". json_encode($vars));
			return $results;
		}

		if (empty($vars['orderId']))
		{
			unset($vars['orderId']);
		}

		// Points pay webview displays points like money so we have to adjust the amount
		$vars['valueToRedeem'] = (double) $vars['amount'] * 100;

		try
		{
			// Send Order to Pepper
			$results = PepperIntegration::redeemPoints($vars);

			// Skipping updating orders.original_id because we aren't creating a Pepper order yet so no orderId is available yet
			//$updated_loyalty_info = $this->queryDatabase('split_check_details-loyalty_info-update', array_merge($vars, $results));
		}
		catch (Exception $e)
		{
			error_log($e->getMessage());
			echo json_encode(array('error' => true, 'error_msg' => $e->getMessage()));
			return;
		}

		return $results;
	}

	/**
	 * Call Pepper's "Create Credit" API call to import a customer's previous points balance
	 */
	function performImportPoints($vars)
	{
		$results = array();

		// Look for legacy card by card number to get the number of points to transfer
		$vars['chain_id'] = $this->chain_id;
		$legacy_loyalty = $this->queryDatabase('pepper_legacy_loyalty_by_card_number', $vars);

		if ( empty($legacy_loyalty) )
		{
			$results['message'] = "Could not find legacy loyalty card";
		}
		else if ( !empty($legacy_loyalty['import_date']) )
		{
			$results['message'] = "Points from legacy loyalty card already imported";
		}
		else
		{
			// Make pepper api call
			$vars['request_json'] = json_encode(array(
				'description' => 'Imported points from legacy loyalty',
				'scheme'      => 'POINT_PERK',
				'awardId'     => $vars['awardId'],
				'number'      => intval($legacy_loyalty['points']) * 100,
			));

			$results = $this->api->createCredits($vars);

			// On success, update med_customers to show imported
			if (isset($results['state']) && isset($results['number']) && $results['state'] == 'APPLIED' && $results['number'] > 0)
			{
				$vars['import_date'] = gmdate('Y-m-d H:i:s');
				$update_ok = $this->queryDatabase('pepper_legacy_loyalty_update', $vars);

				if ( $update_ok )
				{
					// TO DO : create action_log event
				}
			}
		}

		return $results;
	}

	/**
	 * Call Pepper's "Get Awards" API function (note that we're calling them "Rewards" but Pepper calls them "Awards")
	 *
	 */
	public function getRewards($vars)
	{
		$discount_types = array();
		$discount_types_results = $this->queryDatabase('discount_types', $vars);
		while ($discount_type = mysqli_fetch_assoc($discount_types_results))
		{
			// Only process discount_types with a token value
			if (empty($discount_type['token'])) continue;

			// Save discount_types with token as key, so we can easily get the disount_type that corresponds to a Pepper Award.token
			$discount_types[$discount_type['token']] = $discount_type;
		}

		$response = $this->api->getAwards($vars);

		// Save discount_types with token as key, so we can easily get the disount_type that corresponds to a Pepper Award.token
		$rewards = isset($response['awards']) ? $response['awards'] : array();
		foreach ( $rewards as $reward )
		{
			$reward['id'] = $reward['_id'];
			$token = $reward['token'];
			$reward['lavuDiscountType'] = empty($discount_types[$token]) ? array() : $discount_types[$token];
			$this->rewards[] = $reward;
		}


		return $response;
	}

	/**
	 * "Checked-In Users" API call
	 *
	 * GET /locations/{locationId}/checkins
	 *
	 */
	public function getCheckins($vars)
	{
		$response = $this->api->getCheckinsForLocation();

		// Save checkins to member with userId as key, so we can easily test for checked-in users
		$this->checkins = array();
		$checkins = isset($response['checkins']) ? $response['checkins'] : array();
		foreach ( $checkins as $checkin )
		{
			$checkin['fullName'] = $checkin['userFullName'];
			$checkin['balance'] = number_format($checkin['balance'], 2);
			//$this->checkins[$checkin['userId']] = $checkin;
			$this->checkins[] = $checkin;
		}

		return $response;
	}

	/**
	 * Queries database of passed-in dataname for all open order_id's
	 *
	 */
	public function getOpenOrderIDs($vars)
	{
		$open_orders = $this->queryDatabase('open_order_ids', $vars);

		$open_order_ids = array();
		while ( $order = mysqli_fetch_assoc($open_orders) )
		{
			$open_order_ids[] = $order;
		}

		return $open_order_ids;
	}

	/**
	 * "Get User" API call
	 *
	 * GET /user/{userId}
	 *
	 */
	public function getUser($vars)
	{
		$user = $this->api->getUser($vars);

		$this->users[] = $user;

		return $user;
	}

	/**
	 * "Get Users" API call
	 *
	 * GET /users
	 *
	 */
	public function getUsers($vars)
	{
		$response = $this->api->getUsers();

		$this->users = array();
		foreach ( $response['users'] as $user )
		{
			$this->users[] = $user;
		}

	}

	/**
	 * "Checked-In Users" html table rendering
	 *
	 */
	public function renderCheckinsTableRows($vars)
	{
		if (empty($this->checkins))
		{
			$this->api->getCheckins();
		}

		$column_names = null;
		$renderedTableHeaderRow = false;

		echo "<table>\n";
		foreach ( $this->checkins as $i => $checkin )
		{
			if ($column_names === null) $column_names = array_keys($checkin);
			if (!$renderedTableHeaderRow) $renderedTableHeaderRow = $this->renderTableHeaderRow($column_names);
			$this->renderTableRow($checkin, $column_names);
		}
		echo "</table>\n";

	}

	/**
	 * "Checked-In Users" API call
	 *
	 * GET /locations/{locationId}/checkins
	 *
	 */
	public function renderUserTableRows($vars)
	{
		if (empty($this->users))
		{
			$this->getUsers();
		}

		$column_names = array('fullName', 'created', 'hasProfilePhoto', 'hasPaymentCard', 'balance', 'points', 'subscribed');
		$renderedTableHeaderRow = false;

		echo "<table>\n";
		foreach ( $this->users as $i => $user )
		{
			if ($column_names === null) $column_names = array_keys($user);
			if (!$renderedTableHeaderRow) $renderedTableHeaderRow = $this->renderTableHeaderRow($column_names);
			$this->renderTableRow($user, $column_names);
		}
		echo "</table>\n";

	}

	public function renderTableHeaderRow($column_names)
	{
		echo "<tr>";
		for ($i = 0; $i < count($column_names); $i++)
		{
			$column_name = $column_names[$i];
			echo "<td>". $column_name ."</td>\n";
		}
		echo "</tr>";

		return true;
	}

	public function renderTableRow($row, $column_names)
	{
		echo "<tr>";
		for ($i = 0; $i < count($column_names); $i++)
		{
			$column_name = $column_names[$i];
			$column_value = is_array($row[$column_name]) ? '[...]' : $row[$column_name];
			echo "<td>". $column_value ."</td>\n";
		}
		echo "</tr>";
	}

	public function renderUserPics()
	{
		if (empty($this->users))
		{
			$this->api->getUsers();
		}

		echo "<div id='userpics'>\n";
		foreach ( $this->users as $i => $user )
		{
			$this->renderUserPicFigure($user);
		}
		echo "</div>\n";
	}

	public function renderCheckinPics()
	{
		if (empty($this->checkins))
		{
			$this->api->getCheckins();
		}

		echo "<div id='userpics'>\n";

		if (count($this->checkins))
		{
			foreach ( $this->checkins as $i => $checkin )
			{
				$user = $this->api->getUser($checkin);
				$user['checkin'] = $checkin['created'];

				// Map favouriteProducts (note British spelling from API) to comma-delimited string "favoriteProduct" (note American, singular spelling)
				if (is_array($user['favouriteProducts']) && count($user['favouriteProducts']) > 0 )
				{
					// We combine the array into a single "favoriteProduct" key (note the American spelling)
					foreach ($user['favouriteProducts'] as $favoriteProduct)
					{
						$user['favoriteProduct'] .= empty($user['favoriteProduct']) ? $favoriteProduct['title'] : ', '. $favoriteProduct['title'];
					}
				}

				$this->renderUserPicFigure($user);
			}
		}

		echo "</div>\n";
	}

	public function renderUserPicFigure($user)
	{
		$pic = empty($user['hasProfilePhoto']) ? '/components/order_addons/pepper/images/no_profile_pic.png' : "http://api.pepperhq.com/users/{$user['id']}/avatar.jpg";  // TO DO : replace random profile pic with missing image
		$created = date('n/j/Y', strtotime($user['created']));  // was n/j/Y g:i:s a
		$unixtimestamp = time();

		$subscribed = empty($user['subscribed']) ? 'N' : 'Y';
		$push_notify = empty($user['isRegisteredForPushNotifications']) ? 'N' : 'Y';
		$ccinfo = empty($user['card']) ? 'No card on file' : $user['card']['type'] .' xxxx'. $user['card']['last4'];
		//$checkin_emoji = empty($this->checkins[$user['id']]) ? "—" : "✔️";
		$checkin_emoji = '';  // TO DO : make this check a session var or something

		$user_id = htmlspecialchars($user['id']);
		$user_fullname = htmlspecialchars($user['fullName']);


		$favoriteproduct = empty($user['favoriteProduct']) ? '(None)' : $user['favoriteProduct'];

		echo <<<HTML
		<figure id="{$user_id}" ontouchstart="loadUserDetails('{$user_id}')">
			<figcaption class="user_status_icon" id="user_status_icon_{$user_id}">{$checkin_emoji}</figcaption>
			<img id="user_pic_{$user_id}" src="{$pic}" width="81" height="81" border="0" />
			<figcaption class="center" id="user_name_{$user_id}">{$user_fullname}</figcaption>
			<figcaption class="nodisplay" id="user_stats_{$user_id}">{$created}</figcaption>
			<figcaption class="nodisplay" id="user_createddate_{$user_id}">{$created}</figcaption>
			<figcaption class="nodisplay" id="user_status_{$user_id}">{$user['state']}</figcaption>
			<figcaption class="nodisplay" id="user_points_{$user_id}"></figcaption>
			<figcaption class="nodisplay" id="user_stamps_{$user_id}"></figcaption>
			<figcaption class="nodisplay" id="user_balance_{$user_id}">{$user['balance']}</figcaption>
			<figcaption class="nodisplay" id="user_ccinfo_{$user_id}">{$ccinfo}</figcaption>
			<figcaption class="nodisplay" id="user_pushnotify_{$user_id}">{$push_notify}</figcaption>
			<figcaption class="nodisplay" id="user_subscribed_{$user_id}">{$subscribed}</figcaption>
			<figcaption class="nodisplay" id="user_userid_{$user_id}">{$user_id}</figcaption>
			<figcaption class="nodisplay" id="user_favoriteproduct_{$user_id}">{$favoriteproduct}</figcaption>
		</figure>
HTML;
	}

	public function getRandomProfilePic()
	{
		$pics = array(
			'/components/order_addons/pepper/images/adam_robinson.jpg',
			'/components/order_addons/pepper/images/andrew_hawkins.jpeg',
			'/components/order_addons/pepper/images/charles_hall.jpeg',
			'/components/order_addons/pepper/images/simon_kelton.jpeg',
			'/components/order_addons/pepper/images/stuart_hall.jpg',
		);

		return $pics[rand(0, count($pics) - 1)];
	}

	private function getPepperConfig($vars)
	{
		$pepper_config_rows = $this->queryDatabase('pepper_config', $vars);

		foreach ($pepper_config_rows as $pepper_config_row)
		{
			$setting_name = $pepper_config_row['setting'];
			$setting_value = $pepper_config_row['value'];
			$this->pepper_config[$setting_name] = $setting_value;
		}

		return $pepper_config_rows;
	}

	private function setUserIdInSession($vars)
	{
		$order_id = $vars['order_id'];

		// Update the SESSION'd original_ids array
		$pepperInfo = array('locationId' => $vars['locationId'], 'userId' => $vars['userId'], 'rewardId' => $vars['rewardId']);
		$_SESSION['pepperInfo'][$order_id] = $pepperInfo;


		return array('success' => true);
	}

	/**
	 * Handles all database queries and connections
	 *
	 * @param  $query_name String identifying query to run
	 * @param  $args Array of input arguments, if needed for query
	 *
	 * @return mixed Returns mysql resource when $return_single_row is false, array when $return_single_row is true, bool for inserts, updates, failures
	 */
	private function queryDatabase($query_name, $args = array())
	{
		$return_single_row = true;
		$return_all_rows_array = false;
		$die_on_zero_rows  = true;
		$die_on_db_errors  = true;
		$update_or_insert  = false;
		$query_chain_data  = false;

		switch ( $query_name )
		{
			case 'pepper_config':
				$sql = "SELECT * FROM `poslavu_[dataname]_db`.`config` WHERE `type` = 'location_config_setting' AND `setting` LIKE 'pepper_%' AND `location` = '[loc_id]' AND `_deleted` = 0";
				$die_on_zero_rows  = true;
				$return_all_rows_array = true;
				break;

			case 'pepper_locationid':
				$sql = "SELECT * FROM `poslavu_[dataname]_db`.`config` WHERE `type` = 'location_config_setting' AND `setting` = 'pepper_locationid' AND `_deleted` = 0";
				$die_on_zero_rows  = true;
				$return_single_row = true;
				break;

			case 'discount_types':
				$sql = "SELECT * FROM `poslavu_[dataname]_db`.`discount_types` WHERE `_deleted` = 0";
				$die_on_zero_rows = false;
				$return_single_row = false;
				break;

			case 'pepper_legacy_loyalty_by_card_number':
				$sql = "SELECT * FROM `poslavu_CHAIN_DATA_db`.`pepper_legacy_loyalty` WHERE `chain_id`='[chain_id]' AND `card_number`='[card_number]' AND `_deleted` = 0";
				$die_on_zero_rows = false;
				$return_single_row = true;
				$query_chain_data = true;
				break;

			case 'pepper_legacy_loyalty_update':
				$sql = "UPDATE `poslavu_CHAIN_DATA_db`.`pepper_legacy_loyalty` SET `import_date` = '[import_date]' WHERE `chain_id`='[chain_id]' AND `card_number` = '[card_number]' AND `import_date` = '' AND `_deleted` = 0";
				$update_or_insert = true;
				$die_on_zero_rows = true;
				$query_chain_data = true;
				break;

			case 'open_order_ids':
				$sql = "SELECT `order_id`, `total` FROM `poslavu_[dataname]_db`.`orders` WHERE `closed` = '0000-00-00 00:00:00' ORDER BY `order_id` DESC";
				$die_on_zero_rows = false;
				$return_single_row = false;
				break;

			case 'restaurant_by_company_id':
				$sql = "SELECT `id`, `data_name`, `company_name`, `distro_code`, `chain_id`, `modules`, `created`, `email`, `last_activity`, `dev`, `demo`, `disabled`, `test`, `reseller` FROM `poslavu_MAIN_db`.`restaurants` WHERE `id` = '[company_id]'";
				$die_on_zero_rows = true;
				$return_single_row = true;
				break;

			case 'restaurant_by_dataname':
				$sql = "SELECT `id`, `data_name`, `company_name`, `distro_code`, `chain_id`, `modules`, `created`, `email`, `last_activity`, `dev`, `demo`, `disabled`, `test`, `reseller` FROM `poslavu_MAIN_db`.`restaurants` WHERE `data_name` = '[dataname]'";
				$die_on_zero_rows = true;
				$return_single_row = true;
				break;

			case 'orders-original_id-update':
				$sql = "UPDATE `poslavu_[dataname]_db`.`orders` SET `original_id` = REPLACE(`original_id`, '\"orderId\":\"\"', '\"orderId\":\"[orderId]\"') WHERE `order_id` = '[order_id]'";
				$update_or_insert = true;
				break;

			case 'split_check_details-loyalty_info-update':
				$sql = "UPDATE `poslavu_[dataname]_db`.`split_check_details` SET `loyalty_info` = 'pepper_loyalty|[createdOrder]|[createdTransaction]|[updatedOrder]|[orderId]|[transactionId]|[loyaltyEarned]|[loyaltyBurned]' WHERE `order_id` = '[order_id]' AND `check` = '[check]' AND `loyalty_info` = ''";
				$update_or_insert = true;
				break;

			default:
				die(__METHOD__ ." got unrecognized query {$query_name} for dataname {$this->dataname}\n");
				break;
		}

		if ($query_chain_data)
		{
			$result = clavuQuery($sql, $args);
		}
		else
		{
			$result = mlavu_query($sql, $args);
		}

		if ( $result === false && $die_on_db_errors )
		{
			throw new Exception("Database error - {$query_name} query got: ". mysqli_error() ."\n");
		}
		else if ( $result !== false && $return_all_rows_array )
		{
			$all_rows = array();
			while ( $row = mysqli_fetch_assoc($result) )
			{
				$all_rows[] = $row;
			}
			return $all_rows;
		}
		else if ( $update_or_insert )
		{
			return $result;
		}
		else if ( mysqli_num_rows($result) == 0 && $die_on_zero_rows )
		{
			error_log("No row for '{$query_name}' query for {$this->dataname}: sql={$sql} args=". json_encode($args));
			throw new Exception("Database error - No row for '{$query_name}' query for {$this->dataname}: sql={$sql} args=". json_encode($args));
		}

		return ($return_single_row) ? mysqli_fetch_assoc($result) : $result;
	}

}
function errorView() { ?>
<style>
section {
        width: 817px;
        height: 460px;
        background-color: rgba(255, 255, 255, 0.9);
}
h1 {
        color: grey;
        text-transform: uppercase;
}
#all_users {
        position: relative;
        padding-left: 20px;
}
#close_button {
        display: block;
        padding: 0;
        margin: 0;
        font-size: 25px;
        line-height: 30px;
        color: #666666;
        background-color: #fafafa;
        -webkit-border-radius: 2px;
        width: 30px;
        position: fixed;
        z-index: 20001;
        right: 5%;
        top: 20px;
        text-align: center;
}
#close_button:after {
        content: "x";
        color: gray;
        border: 1px solid #aaa;
        border-radius: 2px;
        width: 30px;
        height: 30px;
        line-height: 30px;
        position: fixed;
        z-index: 20001;
        right: 5%;
        top: 20px;
        text-align: center;
}
.visibleRight {
        -webkit-transition: all .75s ease;
        left: 0;
}
</style>
	  <section>
        <div id="close_button" ontouchstart="window.location = '_DO:cmd=close_overlay&c_name=customer&f_name=customer'"></div>
        <div id="all_users" class="visibleRight">
	        <h1>Loyalty Check-ins</h1> 
	        <div style='font-size:50px;color:#ff0000' ><b>Oops!</b></div></br>
	        <div style='font-size:25px'>  <b> Something went wrong and we couldn't process your request.<b></div></br>
	        <div><b>Please add valid Pepper API credentials in CP Extensions page</b></div>
        </div>
<?php exit;
} ?>
