<?php
	function create_selection_options($str)
	{
		$vars = array();
		$slist = explode(",",$str);
		for($i=0; $i<count($slist); $i++)
		{
			$slist_parts = explode(":",$slist[$i]);
			$fieldname = trim($slist_parts[0]);
			if(count($slist_parts) > 1)
			{
				$fieldtitle = trim($slist_parts[1]);
			}
			else
			{
				$fieldtitle = trim($fieldname);
			}
			$fieldtitle = ucfirst(str_replace("_"," ",$fieldtitle));

			$vars[] = array($fieldname,$fieldtitle);
		}
		return $vars;
	}

	function create_date_selector($elementName,$value,$field_title,$GetCustomerInfo_config_value6_required)
	{
		$field_name = $elementName;
		$field_value = $value;
		$field_value_escaped = str_replace("\"","&quot;",$field_value);
		$select_style = "font-size:14px";
		$field_props = 0;
		$field_props2 = -100;

		if(strpos(strtolower($field_title),"birth")!==false)
		{
			$birthday_change_code = "birthdate_selected(document.getElementById(\"$field_name\").value); ";
		}
		else
		{
			$birthday_change_code = "";
		}

		$select_changecode = " onchange='setval = document.getElementById(\"year_$field_name\").value + \"-\" + document.getElementById(\"month_$field_name\").value + \"-\" + document.getElementById(\"day_$field_name\").value; if(setval.length >= 10) {document.getElementById(\"$field_name\").value = setval; $birthday_change_code}'";
		$valign = "middle";
		$element_height = 46;
		$element_bg = "images/tab_bg2.png";

		$form_element = "";

		$birthdate_parts = explode("-",$field_value);
		if(count($birthdate_parts) > 2)
		{
			$bd_year = $birthdate_parts[0];
			$bd_month = $birthdate_parts[1];
			$bd_day = $birthdate_parts[2];
		}
		else
		{
			$bd_year = "";
			$bd_month = "";
			$bd_day = "";
		}
		$form_element .= "<table><tr><td>";
		$form_element .= "<input type='hidden' name='$field_name' id='$field_name' value=\"$field_value_escaped\" required='$GetCustomerInfo_config_value6_required'>";

		$form_element .= "<select name='month_$field_name' style='$select_style' id='month_$field_name' $select_changecode>";
		$form_element .= "<option value=''></option>";
		for($month=1; $month <= 12; $month++)
		{
			$month_ts = mktime(0,0,0,$month,1,2000);
			$selected = ($month==$bd_month)?" selected":"";
			$form_element .= "<option value='".date("m",$month_ts)."' $selected>".date("M",$month_ts)."</option>";
		}
		$form_element .= "</select>";
		$form_element .= "</td><td>/</td><td>";
		$form_element .= "<select name='day_$field_name' style='$select_style' id='day_$field_name' $select_changecode>";
		$form_element .= "<option value=''></option>";
		for($day=1; $day <= 31; $day++)
		{
			if($day < 10) $day_leading_zero = "0" . ($day * 1);
			else $day_leading_zero = $day;
			$selected = ($day==$bd_day)?" selected":"";
			$form_element .= "<option value='$day_leading_zero' $selected>$day</option>";
		}
		$form_element .= "</select>";
		$form_element .= "</td><td>/</td><td>";
		$form_element .= "<select name='year_$field_name' style='$select_style' id='year_$field_name' $select_changecode>";
		$form_element .= "<option value=''></option>";
		for($year=date("Y") + $field_props; $year >= date("Y") + $field_props2; $year--)
		{
			$selected = ($year==$bd_year)?" selected":"";
			$form_element .= "<option value='$year' $selected>$year</option>";
		}
		$form_element .= "</select>";
		$form_element .= "</td></tr></table>";

		return $form_element;
	}

	function v1_create_date_selector($elementName,$value="", $fieldIsRequired)
	{
		$field_name = $elementName;
		$field_value = $value;
		$field_value_escaped = str_replace("\"","&quot;",$field_value);
		$select_style = "font-size:14px";
		$field_props = 0;
		$field_props2 = -100;

		if(strpos(strtolower($field_title),"birth")!==false)
		{
			$birthday_change_code = "birthdate_selected(document.getElementById(\"$field_name\").value); ";
		}
		else
		{
			$birthday_change_code = "";
		}

		$select_changecode = " onchange='document.getElementById(\"$field_name\").value = document.getElementById(\"year_$field_name\").value + \"-\" + document.getElementById(\"month_$field_name\").value + \"-\" + document.getElementById(\"day_$field_name\").value; document.getElementById(\"update_btn\").style.display = \"inline\";'";


		$valign = "middle";
		$element_height = 46;
		$element_bg = "images/tab_bg2.png";

		$form_element = "";

		$birthdate_parts = explode("-",$field_value);
		if($birthdate_parts > 2)
		{
			$bd_year = $birthdate_parts[0];
			$bd_month = $birthdate_parts[1];
			$bd_day = $birthdate_parts[2];
		}
		else
		{
			$bd_year = "";
			$bd_month = "";
			$bd_day = "";
		}
		$style = " style='font-size:10px; height:24px;";
		$form_element .= "<table style='display:inline;'><tr><td>";

		//$element_height = 92;
		//$form_element .= "<input type='text' name='$field_name' id='$field_name' value=\"$field_value_escaped\">";

		$form_element .= "<input type='hidden' name='$field_name' id='$field_name' value=\"$field_value_escaped\" required='$fieldIsRequired' custom_type='date'/>";

		$form_element .= "<select $style name='month_$field_name' style='$select_style' id='month_$field_name' $select_changecode>";
		$form_element .= "<option value=''></option>";
		for($month=1; $month <= 12; $month++)
		{
			$month_ts = mktime(0,0,0,$month,1,2000);
			$selected = ($month==$bd_month*1)?" selected":"";
			$form_element .= "<option value='".date("m",$month_ts)."' $selected>".date("M",$month_ts)."</option>";
		}
		$form_element .= "</select>";
		$form_element .= "</td><td>/</td><td>";
		$form_element .= "<select $style name='day_$field_name' style='$select_style' id='day_$field_name' $select_changecode>";
		$form_element .= "<option value=''></option>";
		for($day=1; $day <= 31; $day++)
		{
			if($day < 10) $day_leading_zero = "0" . ($day * 1);
			else $day_leading_zero = $day;
			$selected = ($day==$bd_day*1)?" selected":"";
			$form_element .= "<option value='$day_leading_zero' $selected>$day</option>";
		}
		$form_element .= "</select>";
		$form_element .= "</td><td>/</td><td>";
		$form_element .= "<select $style name='year_$field_name' style='$select_style' id='year_$field_name' $select_changecode>";
		$form_element .= "<option value=''></option>";
		for($year=date("Y") + $field_props; $year >= date("Y") + $field_props2; $year--)
		{
			$selected = ($year==$bd_year*1)?" selected":"";
			$form_element .= "<option value='$year' $selected>$year</option>";
		}
		$form_element .= "</select>";
		$form_element .= "</td></tr></table>";

		return $form_element;
	}

	function get_state_list()
	{
		/*
		$state_list = "AL:Alabama,
		AK:Alaska,
		AZ:Arizona,
		AR:Arkansas,
		AK:Alaska,
		AZ:Arizona,
		AR:Arkansas,
		CA:California,
		CO:Colorado,
		CT:Connecticut,
		DE:Delaware,
		DC:District Of Columbia,
		FL:Florida,
		GA:Georgia,
		HI:Hawaii,
		ID:Idaho,
		IL:Illinois,
		IN:Indiana,
		IA:Iowa,
		KS:Kansas,
		KY:Kentucky,
		LA:Louisiana,
		ME:Maine,
		MD:Maryland,
		MA:Massachusetts,
		MI:Michigan,
		MN:Minnesota,
		MS:Mississippi,
		MO:Missouri,
		MT:Montana,
		NE:Nebraska,
		NV:Nevada,
		NH:New Hampshire,
		NJ:New Jersey,
		NM:New Mexico,
		NY:New York,
		NC:North Carolina,
		ND:North Dakota,
		OH:Ohio,
		OK:Oklahoma,
		OR:Oregon,
		PA:Pennsylvania,
		RI:Rhode Island,
		SC:South Carolina,
		SD:South Dakota,
		TN:Tennessee,
		TX:Texas,
		UT:Utah,
		VT:Vermont,
		VA:Virginia,
		WA:Washington,
		WV:West Virginia,
		WI:Wisconsin,
		WY:Wyoming";
		return create_selection_options($state_list);
		*/
		$result = lavu_query("SELECT * FROM `states`");
		if(!$result){ error_log("mysql error in ".__FILE__." error:".lavu_dberror()); }
		$statesArr = array();
		//Filter out deleted.
		while($currRow = mysqli_fetch_assoc($result)){
			if(!empty($currRow['_deleted'])){ continue; }
			$statesArr[] = $currRow;
		}

		//The string versions of the states, exploded.
		$finalStatesArr = array();
		foreach($statesArr as $currStateRow){
			$finalStatesArr[ $currStateRow['state_name'] ] = array($currStateRow['state'], $currStateRow['state_name']);
		}

		ksort($finalStatesArr);

		return array_values($finalStatesArr);
	}

	function pl_row_value($row,$key,$def="")
	{
		if(isset($row) && is_array($row) && isset($row[$key])) return $row[$key];
		else return $def;
	}

	$fname_field = "";
	$lname_field = "";
	$med_customer_fields;
	function get_med_customer_fields()
	{
	    global $med_customer_fields;
		global $fname_field;
		global $lname_field;

		static $fields = null;
		if(!empty($fields) && is_array($fields)){
			return $fields;
		}

		$defaultFieldRows = get_med_customer_field_defaults();
		$fields = array();
		$field_query = lavu_query("select * from `config` where `setting`='GetCustomerInfo' and `_deleted`!='1'");
		$med_customer_fields = array();
		while($field_read = mysqli_fetch_assoc($field_query))
		{
			$row = $field_read;
			$med_customer_fields[$row['value']] = $row;
			//echo str_replace("\n","<Br>",str_replace("<","&lt;",$row)) . "<br><br>";

			$fieldtype = pl_row_value($row,"type");
			$fieldtype = empty($fieldtype) ? 'input' : $fieldtype;
			$fieldname = pl_row_value($row,"value");
			$colname = pl_row_value($row,"value2");
			$props = pl_row_value($row,"value4");
			$required = pl_row_value($row,"value6");
			$ordering = pl_row_value($row,"value10");
			$original_row = $row;

			$fieldtitle = ucfirst(str_replace("_"," ",$fieldname));

			if($colname=="f_name") $fname_field = $fieldtitle;
			if($colname=="l_name") $lname_field = $fieldtitle;

			//$newfield = array("type"=>$fieldtype,"name"=>$fieldname,"column"=>$colname,"props"=>$props,"required"=>$required,"order"=>$ordering,"title"=>$fieldtitle);
			$newfield = array("type"=>$fieldtype,"name"=>$fieldname,"column"=>$colname,"props"=>$props,"required"=>$required,"order"=>$ordering,"title"=>$fieldtitle, "original_row"=>$row);

			//Check if has default.
			if($colname == 'field5'){//If is 'State'
				$newfield['default'] = $defaultFieldRows['customer_settings_default_state']['value'];
			}
			else if($colname == 'field4'){//If is 'City'
				$newfield['default'] = $defaultFieldRows['customer_settings_default_city']['value'];
			}


			$fieldplaced = false;
			$newfields = array();
			for($n=0; $n<count($fields); $n++)
			{
				if(!$fieldplaced && $fields[$n]['order'] > $ordering)
				{
					$newfields[] = $newfield;
					$fieldplaced = true;
				}
				$newfields[] = $fields[$n];
			}
			if(!$fieldplaced)
				$newfields[] = $newfield;
			$fields = $newfields;
		}

		return $fields;
	}
	function get_med_customer_field_defaults(){
		$result = lavu_query("SELECT * FROM `config` WHERE `setting` LIKE 'customer_settings_default%'");
		if(!$result){ error_log("MySQL error in ".__FILE__." error: ".lavu_dberror()); }
		$defaultRows = array();
		while($currRow = mysqli_fetch_assoc($result)){
			$defaultRows[$currRow['setting']] = $currRow;
		}
		return $defaultRows;
	}

	function perform_med_customer_recent_activity_search(){
		$queryStr = "SELECT * FROM `med_customers` ORDER BY `last_activity` DESC LIMIT 20";
		$result = lavu_query($queryStr);
		if(!$result){
			error_log("mysql error in ".__FILE__." error: ".lavu_dberror());
			return false;
		}
		$conf_med_c_fields = empty($lfields) ? get_med_customer_fields() : $lfields;


    	$conf_med_c_titles = array();
    	foreach($conf_med_c_fields as $field_index => $field){
    	   $conf_med_c_titles[$field['title']] = $field['column'];
    	}

		$med_customer_rows = array();
    	while($curr = mysqli_fetch_assoc($result)){
        	$med_customer_rows[] = $curr;
    	}

    	$other_cols = getOtherColumns();
    	$final_client_rows = array(); $j = 0;
    	foreach($med_customer_rows as $row){
			translateRawMedCustomerRowIntoClientRow($row, $final_client_rows, $conf_med_c_titles, $j, $other_cols, $med_customer_rows);
		}
		return $final_client_rows;
	}

	$lastSearchTerm;
	function perform_med_customer_search($searchterm="", $lfields=false){
        global $lastSearchTerm;
        $lastSearchTerm = $searchterm;

        $tablename = "med_customers";

    	//array of db rows with cols: type, name, column, props, required, order, title
    	$conf_med_c_fields = empty($lfields) ? get_med_customer_fields() : $lfields;

    	//return str_replace("\n", "<br>", print_r($conf_med_c_fields,1));

    	//We build the master search query, and the title list.
    	$conf_med_c_titles = array();
    	$search_query_conditions = "concat(`f_name`,' ',`l_name`) LIKE '%[searchterm]%' OR ";
    	foreach($conf_med_c_fields as $field_index => $field){
    	   $conf_med_c_titles[$field['title']] = $field['column'];
    	   $search_clause = "`".ConnectionHub::getConn('rest')->escapeString($field['column'])."` LIKE '%[searchterm]%'";
    	   $search_query_conditions .= $field_index == 0 ? $search_clause : ' OR ' . $search_clause;
    	}

    	$queryStr = "SELECT * FROM `[tablename]` ";
    	if(!empty($searchterm)){
        	$queryStr .= "WHERE $search_query_conditions ORDER BY `l_name` ASC, `f_name` ASC LIMIT 40";
    	}else{
        	$queryStr .= "ORDER BY `id` DESC LIMIT 40";
    	}

    	$result = lavu_query($queryStr, array('tablename' => $tablename, 'searchterm' => $searchterm));

    	$med_customer_rows = array();
    	while($curr = mysqli_fetch_assoc($result)){
        	$med_customer_rows[] = $curr;
    	}

    	//return str_replace("\n", "<br>", print_r($med_customer_rows, 1));

    	//$other_cols = array("last_activity","rfid");
    	$other_cols = getOtherColumns();
    	$final_client_rows = array(); $j = 0;
    	foreach($med_customer_rows as $row){
    		//We now also have function 'perform_med_customer_recent_activity_search' which does same thing. Considering how
    		//balls complicated this is, we better encapsulate it.
			translateRawMedCustomerRowIntoClientRow($row, $final_client_rows, $conf_med_c_titles, $j, $other_cols, $med_customer_rows);
	    	/*
	    	$final_client_rows[$j++] = array();
    		foreach($conf_med_c_titles as $title => $column){
        		$final_client_rows[$j-1][$title] = $med_customer_rows[$j-1][$column];
    		}
    		for($n=0; $n<count($other_cols); $n++)
    		{
    			$colname = $other_cols[$n];
    			$final_client_rows[$j-1][$colname] = (isset($med_customer_rows[$j-1][$colname]))?$med_customer_rows[$j-1][$colname]:"";
    		}
    		$final_client_rows[$j-1]['id'] = $med_customer_rows[$j-1]['id'];
    		$final_client_rows[$j-1]['field16'] = $med_customer_rows[$j-1]['field16'];
    		$final_client_rows[$j-1]['waiver_expiration'] = $med_customer_rows[$j-1]['waiver_expiration'];
    		*/
    	}

    	return $final_client_rows; //Becomes Customers, then Customer
	}
	function getOtherColumns(){
		return array("last_activity","rfid");
	}
	function translateRawMedCustomerRowIntoClientRow(&$row, &$final_client_rows, &$conf_med_c_titles, &$j, &$other_cols, &$med_customer_rows){
	    $final_client_rows[$j++] = array();
    	foreach($conf_med_c_titles as $title => $column){
        	$final_client_rows[$j-1][$title] = $med_customer_rows[$j-1][$column];
    	}
    	for($n=0; $n<count($other_cols); $n++)
    	{
    		$colname = $other_cols[$n];
    		if(empty($med_customer_rows[$j-1][$colname])){ //Don't display rfid if not being used.
    			continue;
    		}
    		$final_client_rows[$j-1][$colname] = (isset($med_customer_rows[$j-1][$colname]))?$med_customer_rows[$j-1][$colname]:"";
    	}
    	$final_client_rows[$j-1]['id'] = $med_customer_rows[$j-1]['id'];
    	$final_client_rows[$j-1]['field16'] = $med_customer_rows[$j-1]['field16'];
    	$final_client_rows[$j-1]['waiver_expiration'] = $med_customer_rows[$j-1]['waiver_expiration'];
    	$final_client_rows[$j-1]['raw_med_customer_row'] =  $med_customer_rows[$j-1]; //
	}

	function getCustomerById($id, $lfields = false){
    	$result = lavu_query("SELECT * FROM `med_customers` WHERE `id`='[1]'", $id);
    	if(!$result || mysqli_num_rows($result) == 0){
        	return false;
    	}
    	$med_customer_row = mysqli_fetch_assoc($result);
    	$conf_med_c_fields = empty($lfields) ? get_med_customer_fields() : $lfields;

    	$conf_med_c_titles = array();
    	foreach($conf_med_c_fields as $field_index => $field){
    	   $conf_med_c_titles[$field['title']] = $field['column'];
    	}

    	$returnCustomer;
    	foreach($conf_med_c_titles as $title => $column){
        	$returnCustomer[$title] = $med_customer_row[$column];
    	}
    	$returnCustomer['id'] = $id;
    	$returnCustomer['field16'] = $med_customer_row['field16'];
    	return $returnCustomer;
	}







	function get_printing_options_json_array($customer){
	   global $med_customer_fields;
          /*// whether to print is at: value12 of each GetCustomerInfo config.
    	  {
    	      "print_info": [ {"field":"First Name", "fields_db_name":"First_Name", print":"k|r|b", "info":"Brian"}, {"field":"Last Name", "print":"k|r|b", "info":"Dennedy"}, ...,]

    	  }
          */
//error_log('med_customer_fields count: ' . count($med_customer_fields, 1) );
        $med_customer_fields_disp_as_key = array();
        foreach($med_customer_fields as $val){
            $newKey = str_replace("_", " ", $val['value']);
            $med_customer_fields_disp_as_key[$newKey] = $val;
        }
        $printValues = array();
        foreach($customer as $custKey => $custVal){
            $medFieldName = str_replace("_", " ", $custKey);
            if (isset($med_customer_fields_disp_as_key[$medFieldName])) {
            	$printValues[$medFieldName] = array( $med_customer_fields_disp_as_key[$medFieldName]['value12'], $med_customer_fields_disp_as_key[$medFieldName]['value']);
            } else{
            	$printValues[$medFieldName] = array("", $custKey);
		        $customer[$medFieldName] = $custVal;
            }
        }

        //Now we build the array of dicts
        $printInfoArr = array();
        foreach($printValues as $key => $val){
					$p = $val[0];
					if ($p=="k" || $p=="r" || $p=="b") {
            $printInfoArr[] = array("field"=>$key, "fields_db_name" => $val[1], "print"=>$val[0], "info"=>$customer[$key]);
            //echo $key . " = " . $val[1] . " = " . $val[0] . " = " . str_replace('"',"",$customer[$key]) . "<br>";
					}
        }
        //exit();
//error_log("Print Info Array: " . print_r($printInfoArr,1));

        $jsonValue = json_encode($printInfoArr);

//error_log("JSON VALUE: " . $jsonValue);
        return $jsonValue;
	}

	function render_med_customer_to_html($customer, $js_id, $searchterm=''){
    	global $lastSearchTerm;
		global $fname_field;
		global $lname_field;


		if($fname_field=="") $fname_field = "First Name";
		if($lname_field=="") $lname_field = "Last Name";

        $whetherToBoldSearch = false;
        $whetherToBoldSearchResult = lavu_query("SELECT `setting`, `value` FROM `config` WHERE `setting`='bold_search_findings'");
        if($whetherToBoldSearchResult && mysqli_num_rows($whetherToBoldSearchResult)){
        	$row = mysqli_fetch_assoc($whetherToBoldSearchResult);
        	$whetherToBoldSearch = !empty($row['value']);
        }

        $returnHTML = "";
        //$returnHTML .= "<div style='background-color:#FFF; width:100%'>";
        $customer_id = $customer['id'];
        unset($customer['id']);//Need to hide the id from being rendered.
        $field16 = $customer['field16'];
        unset($customer['field16']);
        $customer_full_name = trim($customer[$fname_field] . " " . $customer[$lname_field]);
				
				$customer_email = (isset($customer['Email']))?$customer['Email']:"";

		/*if($lastSearchTerm == "test")
		{
			foreach($customer as $ckey => $cval)
			{
				echo $ckey . " = " . $cval . " = " . $customer[$ckey] . "<br>";
			}
			echo $fname_field . "<br>" . $lname_field . "<br>" . $customer[$fname_field] . "<br>" . $customer[$lname_field] . "<br>";
			echo $customer_full_name . "<br>";
		}*/

        $printOptionsArrJSON = get_printing_options_json_array($customer);

        $jsonInfo  = '{';
        $jsonInfo .=     '"print_info":'.$printOptionsArrJSON;
        $jsonInfo .= '}';

        //onclick='customer_selected(\"$customer_id\",\"$customer_full_name\"
        //Empty Row
        //$returnHTML .= "<tr><td colspan='2'><div>&nbsp;</div></td></tr>";

        //First Name
        $fnTitle = key($customer);
        $firstName = current($customer);
        next($customer);

        //Last Name
        $lnTitle = key($customer);
        $lastName = current($customer);
        next($customer);

        //We trim the first/last name down to 9 characters
        if(strlen($firstName) >= 16){
            $firstName = substr($firstName, 0, 13) . "...";
        }
        if(strlen($lastName) >= 16){
            $lastName = substr($lastName, 0, 13) . "...";
        }

        $firstColWidth  = '175px';
        $secondColWidth = '175px';
        $thirdColWidth  = '70px';
        $spansThree = '420px';
        $spansTwo = str_replace('px', '', $secondColWidth) + str_replace('px', '', $thirdColWidth);
        $fnlnStyle =  'max-width:178px; word-wrap: break-word;';
        $tdStyle = 'max-width:178px; word-wrap: break-word; vertical-align:top;';
        //$secondColStyle = 'padding-right:15px;';
        $thirdColStyle = 'right:0px;';
        //The larger First Name/Last Name row.

        //Retard mistake, only the inner '"'s should be changed.
        //$jsonInfo    = str_replace('"', '\"', $jsonInfo);
        $jsonInfo    = str_replace(":-.-:", "", $jsonInfo);
        $currEditID = "edit_" . $customer_id . "_" . $js_id;
        $currHistID = "history_" . $customer_id . "_" . $js_id;
        $currSelectCustomerID = "select_" .$customer_id . "_" . $js_id;

        //INSERTED BY Brian D.
        global $usesSignatureAndWaivers, $dateTimeLoc;
        $currWaiverImgID;
        if($usesSignatureAndWaivers){
        	$currWaiverImgID = "waiver_" . $customer_id . "_" . $js_id;
        	//We check if the customer signature exists
        	global $company_code;//From comconnect
        	//echo "<script type='text/javascript'> alert('Dataname:$company_code');</script>";
        	
        	/* //Not using file access, we get this from the customers row.
        	$signaturePath = dirname(__FILE__) . '/../../lavukart/companies/'.$company_code.'/signatures/signature_'.$customer_id.".jpg";
        	$signatureExists = file_exists($signaturePath);
			*/
		
        	//Now we check if the signature has expired, etc.
        	$waiverTouchIconLoc = '';
        	$waiverTouchIconLoc_pushed = '';
        	$electronicSig = $field16;

        	if(empty($customer['waiver_expiration'])){//There's no expiration
        		$waiverTouchIconLoc = 'images/doc.png';
        		$waiverTouchIconLoc_pushed = 'images/doc_dark.png';
        	}
        	else if($dateTimeLoc > $customer['waiver_expiration']){//The signature has expired
        		$waiverTouchIconLoc = 'images/sig_red.png';
        		$waiverTouchIconLoc_pushed = 'images/sig_red_dark.png';
        	}
        	else if( !empty($electronicSig) && substr($electronicSig, 0, 1) == 'e' && strpos($customer['waiver_expiration'],"|")===false){
        		$waiverTouchIconLoc = 'images/sig_blue.png';
        		$waiverTouchIconLoc_pushed = 'images/sig_blue_dark.png';
        	}
        	else{
        		$waiverTouchIconLoc = 'images/sig_green.png';
        		$waiverTouchIconLoc_pushed = 'images/sig_green_dark.png';
        	}
        	//$dateTimeLoc, $epochTimeLoc;
        }
        //END INSERT

        //LP-   whetherToBoldSearch
        $firstNameLastNameTDs = '';

        if($whetherToBoldSearch){
        	$width = str_replace("px", "", $firstColWidth) + str_replace("px","",$secondColWidth);
        	$firstNameLastName = stringFuzzySearchHighlight($searchterm, $firstName.' '.$lastName);
        	$firstNameLastNameTDs ="<td colspan='2' style='width:$firstColWidth;$fnlnStyle'>".
		                                "<div id='person_row_div_$js_id' style='float:right; color:#111; font-size:17px;'>".
		                                    "$firstNameLastName</div></td>";
        }else{
        	$firstNameLastNameTDs ="<td style='width:$firstColWidth;$fnlnStyle'>".
		                                "<div id='person_row_div_$js_id' style='float:right; color:#111; font-size:17px;'>".
		                                    "$firstName</div></td>".
		                           "<td style='width:$secondColWidth;$fnlnStyle'>".
		                                "<div style='color:#000color:#111;font-weight:bold;font-size:16px;'>&nbsp;".
		                                    "$lastName</div></td>";
        }

    /*$returnHTML .= "<script type='text/javascript'> alert('$currSelectCustomerID'); </script>";*/
        $returnHTML .= "<tr id='fn_ln_row_$js_id' style='background-color:#fff;'>".
                           $firstNameLastNameTDs.
                           "<td style='width:$thirdColWidth;$tdStyle $thirdColStyle'>".
                             "<div style='max-height:56px; height:56px;'>" .
                                 "<div style='margin-top:-10px'>" .
                                     "<div id='$currSelectCustomerID' ".
                                        "style='background-size:100%; width:50px; height:50px; background-position: center center;".
                                            "background-image:url(images/green_arrow.png); margin-left:8px; margin-top:16px;' ".
                                        "customer_full_name='$customer_full_name' ".
                                        "customer_id='$customer_id' " .
                                        "customer_email='$customer_email' " .
                                        "jsonInfo='$jsonInfo' >".
                                     "</div>".
                                 "</div>".

                                 create_padded_touch_area(
                                    "<input id='$currEditID' type='image' ".
                                           "src='images/edit_customer.png' style='".
                                                  "border:0px;' />", $currEditID."_div", "-2px", "14px", "62px", "30px") .
                                 create_padded_touch_area(
                                    "<input id='$currHistID' type='image' " .
                                            "src='images/view_history.png' style='".
                                                   "border:0px;'/>", $currHistID."_div", "-2px", "29px", "62px", "30px");
                                 //END INSERT
        $returnHTML .=
                             "</div>" .
                          "</td>".
                       "</tr>";

        //We add some bottom buffering so images don't go over to next entry if there's only a few fields showing.
        $returnHTML .= "<tr style='background-color:#EEE;max-height:5px;'>".
                            "<td style='max-height:5px;'><div style='height:5px;'></div></td>".
                            "<td style='max-height:5px;'><div style='height:5px;'></td>".
                            "<td style='max-height:5px;'><div style='height:5px;'></td>".
                        "</tr>";


        //Iterating through the other Cust. Fields.
        //We have to configure where the 'edit' and 'history' buttons go based on how many rows there are.
        $cnt = count($customer) - 3;
        $d = 0;


        //There should be at least first and last name,  We need to handle the cases of:
        // count($customer) - 2 == 0, 1, 2, 3, 4, & 5
        $additionalFieldCount = $cnt;
        for($i = $additionalFieldCount; $i <= 6; $i++){
            $customer['__'.$i] = '';
        }


        //$editRowEnd
        $init = 0;
        foreach($customer as $column => $val){
            if($init++ < 2){ continue; }
            if($column == 'waiver_expiration'){ continue; }
            if($column == 'raw_med_customer_row') {continue; }
            //In case we need blank rows
            if(preg_match("/^__.*$/", $column)){
                $column = $val = '';
            }

            $backgroundImagePath;
            $forwardingPath;
            if(isset($editRowStart) && $d == $editRowStart){$backgroundImagePath = 'images/edit_customer.png';
                                    $forwardingPath = "?mode=update_customer&customer_id=$customer_id&previousSearchTerm=" .
                                                            urlencode($lastSearchTerm) . "&saved_position_id=person_row_div_$js_id";}
            if(isset($histRowStart) && $d == $histRowStart){$backgroundImagePath = 'images/view_history.png';
                                    $forwardingPath = "?mode=history&customer_id=$customer_id&previousSearchTerm=" .
                                                            urlencode($lastSearchTerm) . "&saved_position_id=person_row_div_$js_id";}


            if(!empty($searchterm) && $whetherToBoldSearch){
            	$val = stringFuzzySearchHighlight($searchterm, $val);
            }
            if(empty($secondColStyle)){

            }
            $secondColStyle = empty($secondColStyle) ? '' : $secondColStyle;
            $returnHTML .= "<tr style='background-color:#EEE;'>".
                "<td style='width:$firstColWidth; $tdStyle'><div style='float:right;color:#888;font-size:11px;'> ".speak($column)." </div></td>" .//Edited by Brian D. added translation.
                //"<td style='width:$spansTwo;$tdStyle' colspan='2'><div style='font-size:11px;'>&nbsp;$val </div></td>".
                "<td style='width:$secondColWidth;$tdStyle $secondColStyle'><div style='font-size:11px;'>&nbsp;$val </div></td>".
                "<td style='width:$thirdColWidth; $tdStyle $thirdColStyle'><div style='font-size:11px;'>&nbsp; </div></td>".
                "</tr>";

            $d++;

        }

        //Empty Row
        $returnHTML .= "<tr style='background-color:#EEE;'><td colspan='3'><div>&nbsp;</div></td></tr>";


        //$returnHTML .= "</div>";

        return $returnHTML;
	}

/*
	function stringFuzzyIndexes(term, subject){
		var termLowerCase = term.toLowerCase();
		var subjectLowerCase = subject.toLowerCase();
		var indicesArr = [];
		var s = 0;
		for(var t = 0; t < termLowerCase.length; t++){
			for(true; s < subjectLowerCase.length; s++){
				var match = termLowerCase[t] == subjectLowerCase[s];
				if(match){ 
					indicesArr.push(s);
					break;
				}
			}
		}
		return indicesArr.length == termLowerCase.length ? indicesArr : false;
	}
*/
	function stringFuzzySearchHighlight($searchTerm, $subject){
		$indices = stringFuzzyIndexesByKey($searchTerm, $subject);
		$newHTML = boldAtIndices($subject, $indices);
		return $newHTML;
	}

	function stringFuzzyIndexesByKey($searchTerm, $subject){
		$searchTermLowerCase = strtolower($searchTerm);
		$subjectLowerCase = strtolower($subject);
		$indicesArr = array();
		$s = 0;
		for($t = 0; $t < strlen($searchTermLowerCase); $t++){
			for(true; $s < strlen($subjectLowerCase); $s++){
				$isCharMatch = ($searchTermLowerCase[$t].'a') == ($subjectLowerCase[$s].'a');
				if($isCharMatch){
					$indicesArr[$s++] = 1;
					break;
				}
			}
		}
		return count($indicesArr) == strlen($searchTermLowerCase) ? $indicesArr : false;
	}

	function boldAtIndices($htmlString, $boldCharIndices){
		$strBuilder = '';
		$isInBold = false;
		for($i = 0; $i < strlen($htmlString); $i++){
			if(!$isInBold){
				if( !empty($boldCharIndices[$i]) ){
					$isInBold = true;
					$strBuilder .= '<b>'.$htmlString[$i];
				}else{
					$strBuilder .= $htmlString[$i];
				}
			}else{
				if( !empty($boldCharIndices[$i]) ){
					$strBuilder .= $htmlString[$i];
				}else{
					$isInBold = false;
					$strBuilder .= '</b>'.$htmlString[$i];
				}
			}
		}
		if($isInBold){
			$strBuilder .= '</b>';
		}
		return $strBuilder;
	}

	function create_padded_touch_area($htmlElemStr, $divID, $left, $top, $width, $height){
    	//return $htmlElemStr;

    	// "<div style='position:absolute; left:-5px; top:-10px; width:$width; height:$height; background-color:rgba(0,255,0,0.2);'></div>" .

    	return "<div style='position:relative;'>&nbsp;<div id='$divID' style='position:absolute; z-index:200; left:$left; top:$top; width:$width; height:$height; background-color:rgba(0,255,0,0.0);'><table width='100%' height='100%'><tr><td valign='middle' align='center' width='100%' height='100%'>" .$htmlElemStr ."</td></tr></table></div></div>";

	}


	function render_letter_division_row($letter){
    	$returnHTML = "<tr style='background-color:#CCC;'><td colspan='3'><div id='tr_div_$letter' style='height:31px; font-size:24px; color:#ffffff;'>&nbsp; $letter</div></td></tr>";
    	return $returnHTML;
	}



	function render_med_customer_list_to_html($customers, $bodyScrollTop, $searchterm=''){
        $returnHTML = '';

        if(count($customers) == 0){
            $returnHTML = get_no_search_results_html();
            return $returnHTML;
        }

        $returnHTML .= "<table border='0' cellspacing='0' style='/*width:420px; table-layout:fixed;*/'>";
        $globalLastNameLetter = '';


        for($i = 0; $i < count($customers); $i++){
            $currLetter = strtoupper( $customers[$i]['Last Name'][0] );
            if($currLetter != $globalLastNameLetter){
                $returnHTML .= render_letter_division_row($currLetter);
                $globalLastNameLetter = $currLetter;
            }
            $returnHTML .= render_med_customer_to_html($customers[$i], $i, $searchterm);
        }



        $returnHTML .= "</table>";
        return $returnHTML;
	}



	function get_no_search_results_html(){
    	return "<p style='font-size:16px'><br>&nbsp;&nbsp;NO SEARCH RESULTS FOUND </p>";

	}

	function sort_by_last_name($customers){
    	//We just flatten the array by last name and append an incrementor to ensure uniqueness
    	//and that if there's 2 people with the same last name it the first isn't lost. etc.
    	$i = 1;
    	$sortingArr = array();
    	foreach($customers as $currCustomer){
        	$sortingArr[ strtolower($currCustomer['Last Name']) . $i++] = $currCustomer;
    	}
    	ksort($sortingArr);
    	$returnArr = array();
    	foreach($sortingArr as $currCustomer){
        	$returnArr[] = $currCustomer;
    	}
    	return $returnArr;
	}

	function groupCurrentDataNameFirstIfApplicable($customers){

		global $data_name;

		$query = "SELECT `setting`,`value` FROM `config` WHERE `setting`='group_by_dataname_field22' LIMIT 1";
		$group_by_dataname_field22Result = lavu_query($query);
		if(!$group_by_dataname_field22Result){
			error_log("SQL error -h3yeeg- in ".__FILE__);
		}
		if(mysqli_num_rows($group_by_dataname_field22Result) == 0){
			return $customers;
		}
		$configRow = mysqli_fetch_assoc($group_by_dataname_field22Result);
		if(empty($configRow['value'])){
			return $customers;
		}

		$datanameTrimmedLowercase = trim(strtolower($data_name));
		$currChar = count($customers) ? strtolower($customers[0]['Last Name'][0]) : '';
		$groupedByField22Customers = array();
		$currCustomerAtDatanameArr = array();
		$currCustomerDoesNotArr    = array();
		for($i = 0; $i < count($customers); $i++){
			$currCustomerRow = $customers[$i];
			$field22TrimmedLowerCase = trim(strtolower($currCustomerRow['raw_med_customer_row']['field22']));

			if(strtolower($currCustomerRow['Last Name'][0]) != $currChar){
				$currChar = strtolower($currCustomerRow['Last Name'][0]);
				$groupedByField22Customers = array_values($groupedByField22Customers);
				$currCustomerAtDatanameArr = array_values($currCustomerAtDatanameArr);
				$currCustomerDoesNotArr    = array_values($currCustomerDoesNotArr);
				$groupedByField22Customers = array_merge($groupedByField22Customers,$currCustomerAtDatanameArr,$currCustomerDoesNotArr);
				$currCustomerAtDatanameArr = array();
				$currCustomerDoesNotArr    = array();
			}

			if($datanameTrimmedLowercase == $field22TrimmedLowerCase){
				$currCustomerAtDatanameArr[] = $currCustomerRow;
			}else{
				$currCustomerDoesNotArr[] = $currCustomerRow;
			}

			if($i == count($customers) - 1){
				$groupedByField22Customers = array_values($groupedByField22Customers);
				$currCustomerAtDatanameArr = array_values($currCustomerAtDatanameArr);
				$currCustomerDoesNotArr    = array_values($currCustomerDoesNotArr);
				$groupedByField22Customers = array_merge($groupedByField22Customers,$currCustomerAtDatanameArr,$currCustomerDoesNotArr);
			}
		}

		return $groupedByField22Customers;
	}

	function draw_med_customer_list($searchterm="",$lfields=false, $scrollTop=0){

		//echo "<script> alert('You searched for:{$searchterm}'); </script>";
		global $showsMostRecentUsers;
		$isDisplayingUsersByRecentActivity = ($showsMostRecentUsers && $searchterm == '');

	    $returning = $_GET['returning_to_search'];
	    $returnHTML = "";
	    if(empty($_GET['returning_to_search']) || empty($_SESSION['saved_search_data'])){
    	   $med_customer_fields_c = get_med_customer_fields();
    	   $customersArr = array();
    	   if($isDisplayingUsersByRecentActivity){
    	   	//error_log("Doing recent activity med_customer pop");
    	   		$customersArr = perform_med_customer_recent_activity_search();
    	   		$customersArr = sort_by_last_name($customersArr);
    	   }else{
    	   		$customersArr = perform_med_customer_search($searchterm, $med_customer_fields_c);
    	   		$customersArr = sort_by_last_name($customersArr);
    	   }


		   $customersArr = groupCurrentDataNameFirstIfApplicable($customersArr);


    	   
    	   $returnHTML  .= render_med_customer_list_to_html($customersArr, $scrollTop, $searchterm);
    	   ob_start();
    	   addJavascriptImageHandlerForDynamicImages();
    	   addActionListenersToTheDynamicButtons($customersArr);
    	   $returnHTML .= ob_get_clean();
    	   $_SESSION['saved_search_data'] = $returnHTML;

	    }
	    else{
	        $returnHTML   .= "<script type='text/javascript'>  ";
	        $returnHTML   .= "function myOnLoad(){ document.getElementById('all_container').style.display = 'block'; document.body.scrollTop = $scrollTop - 15; }";
	        $returnHTML   .= "</script>";
    	    $returnHTML   .=  "<div style='display:none' id='all_container'>" . $_SESSION['saved_search_data'] . "</div>";
    	    $returnHTML   .= "<script type='text/javascript'> window.onload = myOnLoad(); </script>";
	    }
    	return $returnHTML;

	}

	function addJavascriptImageHandlerForDynamicImages(){
	   global $lastSearchTerm;
	   //How the window.location is created:
	   //$forwardingPath = "?mode=update_customer&customer_id=$customer_id&previousSearchTerm=" .
       //  urlencode($lastSearchTerm) . "&saved_position_id=person_row_div_$js_id";}
       ?>
        <script type='text/javascript'>

            var lastSearchTerm = '<?php echo $lastSearchTerm; ?>';

            function getParentingRow(elem){
                while(true){
                    //alert("Current tagName: " + elem.tagName);
                    if(!elem){
                        return elem;
                    }
                    else if(elem.tagName == 'TR'){
                        return elem;
                    }
                    elem = elem.parentElement;
                }
            }

            function rewindTillFN_LN_ROW(RowElem){
                //alert('rewinding: ' + RowElem);
                while(true){
                    if(!RowElem){
                        return false;
                    }
                    else if(RowElem.id.match(/^fn_ln_row_.*$/)){
                        //alert('match found');
                        return RowElem
                    }
                    RowElem = RowElem.previousSibling;
                }
            }
            function getFNLNRowOfInput(Elem){
                var rowElem = getParentingRow(Elem);
                //alert('marker2');
                var FNLNRow = rewindTillFN_LN_ROW(rowElem);
                //alert('marker3');
                return FNLNRow;
            }
            function getScrollTopOfFNLNRowToInput(inputElem){
                //alert('marker1');
                var fnlnRow = getFNLNRowOfInput(inputElem);
                //alert('marker4');
                var scrollTop = getAbsoluteOffset(fnlnRow);
                //alert('Scroll Top: ' + scrollTop);
                return scrollTop;
            }

            //We need to block multiple touches and only allow the first until the page loads
            history_edit_touches_blocked = false;
            function style_d_button_press_start(event){

                if(history_edit_touches_blocked){return;}

                //alert('ID: ' + this.id);
                var elems_id = this.id.replace("_div","");
                //alert('New ID: ' + elems_id);
                var buttonElem = document.getElementById(elems_id);

                //alert('actual button elem: ' + buttonElem);
                parts = buttonElem.id.split("_");

                //function getAbsoluteOffset(elem)
                //alert('hi');
                //alert("Element: " + this.id + "\n");

                //var actualButtonElem = document.getElementById(this.id.replace("_div", ""));
                //var elems_parent_offset = getScrollTopOfFNLNRowToInput( actualButtonElem );

                var rowElement = document.getElementById('fn_ln_row_' + parts[2]);
                var elems_parent_offset = getScrollTopOfFNLNRowToInput( rowElement );

                //alert("elems_parent_offset: " + elems_parent_offset);
                if( elems_id.match( /^edit_.*$/ ) ){
                    //Edit
                    //this.style.backgroundImage = "url('images/edit_customer_lit.png')";
                    buttonElem.src = 'images/edit_customer_lit.png';
                    history_edit_touches_blocked = true;
                    var forwardingPath = "?mode=update_customer&customer_id=" + parts[1] +
                                         "&previousSearchTerm=" + encodeURIComponent(lastSearchTerm) +
                                         "&saved_position_id=person_row_div_" + parts[2] +
                                         "&saved_scroll_position=" + elems_parent_offset;
                    //alert("Forwarding Path: " + forwardingPath);
                    window.location = forwardingPath;
                }
                else if( elems_id.match( /^history_.*$/ ) ){
                    //History
                    //this.style.backgroundImage = "url('images/view_history_lit.png')";
                    buttonElem.src = 'images/view_history_lit.png';
                    history_edit_touches_blocked = true;
                    var forwardingPath = "?mode=history&customer_id=" + parts[1] +
                                         "&previousSearchTerm=" + encodeURIComponent(lastSearchTerm) +
                                         "&saved_position_id=person_row_div_" + parts[2] +
                                         "&saved_scroll_position=" + elems_parent_offset;
                    //alert("Forwarding Path: " + forwardingPath);
                    window.location = forwardingPath;
                }
                else if( elems_id.match( /^select_.*$/ ) ){
    //alert('select pressed');
                    //Selecting the customer.
                    //$currSelectCustomerID = "select_" .$customer_id . "_" . $js_id;
                    ///green_arrow.png); margin-left:8px; margin-top:16px' " .
                    //           "id='$currSelectCustomerId' customer_id='$customer_id' ".
                    //           "customer_full_name='$customer_full_name' jsonInfo='$jsonInfo'
                    history_edit_touches_blocked = true;
                    var fullName   = this.getAttribute('customer_full_name');
                    var customerID = this.getAttribute('customer_id');
										var customerEmail = this.getAttribute('customer_email');
                    var jsonInfo   = this.getAttribute('jsonInfo');

                    //!!!Strangly when selecting a customer the json will have the '"' quot chars escaped and this brakes it.
                    // so easiest solution is to just remove the escapes.
                    buttonElem.style.backgroundImage = "url('images/green_arrow_lit.png')";
                    setTimeout(function(){ customer_selected(customerID, fullName, jsonInfo, ''+elems_parent_offset, customerEmail); }, 100);

                } //INSERTED BY Brian D.
                else if( elems_id.match( /^waiver_.*$/ )){
                	history_edit_touches_blocked = true;
                	var pushedImageLoc = buttonElem.getAttribute('pushedimg');
                	buttonElem.src = pushedImageLoc;//doc_dark.png
                	//buttonElem.src = 'images/view_history_lit.png';
                	//buttonElem.style.backgroundImage = "url('images/doc_dark.png');";
                	//var curr_waiver_button_id = 'waiver_".$currCustomerID."_".$i."';
                	
                	var forwardingPath = "?mode=waiver&customer_id=" + parts[1] +
                                         "&previousSearchTerm=" + encodeURIComponent(lastSearchTerm) +
                                         "&saved_position_id=person_row_div_" + parts[2] +
                                         "&saved_scroll_position=" + elems_parent_offset;
                    
                    //alert("Waiver touched");
                	window.location = forwardingPath;

                } //END INSERT
            }
        </script>
    <?php
	}

	function addActionListenersToTheDynamicButtons($customersArr){
    	$i = 0;
    	?>
    	<script type='text/javascript'>
            var curr_edit_button, curr_edit_button_id;
            var curr_hist_button, curr_hist_button_id;
            var curr_select_button;
            //id=  'edit_" . $customerid . "_" . $js_id'

    	<?php
    	//$currSelectCustomerID = "select_" .$customer_id . "_" . $js_id;
    	foreach($customersArr as $currCustomer){
        ?>
            curr_edit_button_id = 'edit_<?php echo $currCustomer['id']; ?>_<?php echo $i; ?>';
            curr_edit_button = document.getElementById(curr_edit_button_id);
            if(curr_edit_button){
                curr_edit_button.addEventListener('touchstart', style_d_button_press_start);
                curr_edit_button.addEventListener('touchmove',  style_d_button_press_start);
            }

            curr_hist_button_id = 'history_<?php echo $currCustomer['id']; ?>_<?php echo $i; ?>';
            curr_hist_button = document.getElementById(curr_hist_button_id);
            if(curr_hist_button){
                curr_hist_button.addEventListener('touchstart', style_d_button_press_start);
                curr_hist_button.addEventListener('touchmove',  style_d_button_press_start);
            }

            curr_select_button_id = 'select_<?php echo $currCustomer['id']; ?>_<?php echo $i; ?>';
    //alert('adding event listener to:' + curr_select_button_id);
            curr_select_button = document.getElementById(curr_select_button_id);
            if(curr_select_button){
                curr_select_button.addEventListener('touchstart', style_d_button_press_start);
                curr_select_button.addEventListener('touchmove',  style_d_button_press_start);
            }

            curr_edit_button_div_id = curr_edit_button_id + '_div';
            curr_edit_button_div = document.getElementById(curr_edit_button_div_id);
            if(curr_edit_button_div){
                curr_edit_button_div.addEventListener('touchstart', style_d_button_press_start);
                curr_edit_button_div.addEventListener('touchmove',  style_d_button_press_start);
            }

            curr_hist_button_div_id = curr_hist_button_id + '_div';
            curr_hist_button_div = document.getElementById(curr_hist_button_div_id);
            if(curr_hist_button_div){
                curr_hist_button_div.addEventListener('touchstart', style_d_button_press_start);
                curr_hist_button_div.addEventListener('touchmove',  style_d_button_press_start);
            }
        <?php
        	//INSERTED BY Brian D.
        	//This is only for when the customers are using signature and waivers
        	global $usesSignatureAndWaivers;
	        	if($usesSignatureAndWaivers){
	        		$date = date('Y-m-d');
	        		$sign_term = 'dont_know';
	        		//All of the waiver buttons will go to the same function so we just output the hanlder here.
					echo $jsHeredoc;
		        	////$currWaiverImgID = "waiver_" . $customer_id . "_" . $js_id;
		        	$currCustomerID = $currCustomer['id'];

		        	echo "
		        		var curr_waiver_button_id = 'waiver_".$currCustomerID."_".$i."';
		        		var curr_waiver_button = document.getElementById(curr_waiver_button_id);
			            if(curr_waiver_button){
			                curr_waiver_button.addEventListener('touchstart', style_d_button_press_start);
			                curr_waiver_button.addEventListener('touchmove',  style_d_button_press_start);
			            }
		        	";
        	}
        	//END INSERT.
            $i++;
    	}
    	?>
    	</script>
    	<?php
	}






	if(!isset($extra_vars)) $extra_vars = "";

	function draw_med_customer_form($lfields=false,$rowid=0,$formname="medcustomer_form"){
	    global $loc_id;
	    global $previousSearchTerm, $saved_position_id;
	    drawExpandingRequiredNotificationDiv();
		$tablename = "med_customers";

		//
		if($rowid == 0 && !empty($_REQUEST['med_customer_id']) ){
			$rowid = $_REQUEST['med_customer_id'];
		}
		//
		$content = "";
		if(!$lfields)
		{
			$lfields = get_med_customer_fields();
		}
//error_log("FIELDS: ".print_r($lfields,1));
		$row_read = array();
		if($rowid!=0)
		{
			$row_query = lavu_query("select * from `[tablename]` where `id`='[id]'",array("tablename"=>$tablename,"id"=>$rowid));
			if(mysqli_num_rows($row_query))
			{
				$row_read = mysqli_fetch_assoc($row_query);
            }
        }

        $defaultEntries = getDefaultEntriesArr();
		$insert_cols = "";
		$insert_vals = "";
		$update_code = "";
		$query_vars = array();

		$guardian_fields = array();

		global $extra_vars;
		$formurlvars = "?form_posting=1" . $extra_vars;
		$content .="<form name='$formname' method='post' action='$formurlvars' onkeypress='if (event.keyCode == 13){ submitCustomerInfo() }'>";
		$content .= "<input type='hidden' name='med_posted' id='med_posted' value='1'>";
		//
		$content .="<input type='hidden' name='med_customer_id' value='$rowid'/>";
		//
		$content .="<table id='medcustomer_table' width='390px' border='0'>";

		//Forcing in the other arrays.
		$_POST['date_created'] = date('Y-m-d H:i:s');
		$_POST['locationid'] = $loc_id;
		$_POST['last_activity'] = date('Y-m-d H:i:s');

		$lfields[] = array("column" => 'date_created',  'type' => 'hidden');
		$lfields[] = array("column" => 'locationid',    'type' => 'hidden');
		$lfields[] = array("column" => 'last_activity', 'type' => 'hidden');
		//////

		$astrixGreen = 'rgba(0,100,0,0.4)';
		$astrixRed   = 'rgba(200,0,0,1)';

		////
		$otherRequiredFieldsByID = array();
		////
		foreach($lfields as $field_index => $field)
		{

			$fieldIsRequired = $field['required'] ? '1' : '0';
			$reqAstrix1 = " <span style='color:$astrixGreen;font-size:12px' id='";
			$reqAstrix2 = $fieldIsRequired ? "'>*</span>" : "'></span>";

			$requiredAstrix = $reqAstrix1 . '[name]_astrix_id' . $reqAstrix2;
			$field_name = $field['column'];
			$row_value = (isset($row_read[$field_name]))?$row_read[$field_name]:"";
			$field_value = postvar($field_name,$row_value);
			$field_form_value = str_replace("\"","&quot;",$field_value);
			$field_row_name = "row_" . $field['name'];

			$field_title = ucfirst(str_replace("_"," ",$field['name']));
			$set_display = "table-row";
			$check_if_requires_guardian = false;
			if(strpos(strtolower($field_title),"parent")!==false || strpos(strtolower($field_title),"guardian")!==false)
			{
				$guardian_fields[] = array("name"=>$field_row_name,"id"=>$field_name);
				$set_display = "none";
				$check_if_requires_guardian = true;
			}

			$leftTDStyle  = "";
			$rightTDStyle = "";
			$inputStyle = 'width:85%';

			$content .="<tr style='display:$set_display' id='$field_row_name'>";
			$content .="<td style='$leftTDStyle' valign='middle' align='right'>" . speak($field_title) . "</td>";
			$content .="<td style='$rightTDStyle' valign='middle'>";
			//$content .=$field['type'] . "-" . $field['props'] . "<br>";

			if($field['type']=="input")
			{
				$value = isset($field['default']) && empty($field_form_value) && empty($rowid) ? $field['default'] : $field_form_value;

				$code = "<input type='text' style='$inputStyle' name='[name]' id='[name]' value=\"$value\" required='$fieldIsRequired'>$requiredAstrix";
			}
			//Added if hidden so we can update the other columns.
			else if($field['type']=="hidden"){
    			$code = "<input type='hidden' name='[name]' id='[name]' value=\"$field_form_value\" required='$fieldIsRequired'>$requiredAstrix";
			}
			//
			else if($field['type']=="checkbox")
			{
				$code = "";
				$code .= "<input type='hidden' name='[name]' id='[name]' value=''>";
				$code .= "<input type='checkbox' onclick='if(this.checked) setvalue = \"Yes\"; else setvalue=\"No\"; document.getElementById(\"[name]\").value = setvalue' name='[name]_checkbox' id='[name]_checkbox' required='$fieldIsRequired'>$requiredAstrix";
			}
			else if($field['type']=="radio")
			{
				$code = "";
				$code .= "<input type='hidden' name='[name]' id='[name]' value='' required='$fieldIsRequired'>$requiredAstrix";
				$code .= "<br><input type='radio' onclick='document.getElementById(\"[name]\").value = \"Yes\"' name='[name]_radio' id='[name]_radio'> Yes";
				$code .= "<input type='radio' onclick='document.getElementById(\"[name]\").value = \"No\"' name='[name]_radio' id='[name]_radio'> No";
			}
			else if($field['type']=="list_menu")
			{
				if($field['props']=="useTool") $options = get_state_list();
				else $options = create_selection_options($field['props']);
				// $field_form_value
				$code  = "";

				//$code .= "DEFAULT VALUE: ".$field['default'];
				$code .= "<select name='[name]' id='[name]' required='$fieldIsRequired'>";
				$code .= "<option value=''></option>";
				$defaultValue = isset($field['default']) ? $field['default'] : "";
				for($i=0; $i<count($options); $i++)
				{
					$option_name = $options[$i][0];
					$option_title = $options[$i][1];
					//Adding default
					$selectStr = ($option_title == $defaultValue && empty($field_form_value) && empty($rowid)) ? "selected" : "";
					//
					if($option_name == $field_form_value){
    					$code .= "<option value=\"$option_name\" selected>$option_title</option>";
					}else{
    					$code .= "<option value=\"$option_name\" $selectStr>$option_title</option>";
					}

				}
				$code .= "</select>$requiredAstrix";
				if($fieldIsRequired){
    				$otherRequiredFieldsByID[] = $field_name;
				}
			}
			else if($field['type']=="date")
			{

			    $reqAstrix1 = " <span style='color:$astrixGreen;font-size:12px;top:-10px;position:relative;' id='";
			    $reqAstrix2 = $fieldIsRequired ? "'>*</span>" : "'></span>";
			    $requiredAstrix = $reqAstrix1 . '[name]_astrix_id' . $reqAstrix2;

			    ////$code = create_date_selector("[name]","") . $requiredAstrix;
				//$code = create_date_selector("[name]",$field_form_value, $fieldIsRequired) . $requiredAstrix;
	//error_log(print_r($field,1));
	//error_log("value6:" . $field['value6']);
				$isRequiredStr = $field['original_row']['value6'];// == 'requiredField' ? true : false;
	//error_log("is required: " . $isRequiredStr);
				$code = "<table cellspacing=0 cellpadding=0><tr><td valign='middle'>" . create_date_selector("[name]",$field_form_value, $field_title, $isRequiredStr) . "</td><td valign='middle'>" . $requiredAstrix . "</td></tr></table>";
				if($fieldIsRequired){
    				$otherRequiredFieldsByID[] = $field_name;
				}
			}


			$code = str_replace("[name]",$field_name,$code);
			$content .=$code;

			$content .="</td>";
			$content .="</tr>";

			$escaped_field_name = ConnectionHub::getConn('rest')->escapeString($field_name);
			if($insert_cols!="") $insert_cols .= ",";
			if($insert_vals!="") $insert_vals .= ",";
			//if($update_code!="") $update_code .= ",";
			$insert_cols .= "`".$escaped_field_name."`";
			$insert_vals .= "'[".$escaped_field_name."]'";
			
			if(isset($_REQUEST['med_customer_id']) && $_REQUEST['med_customer_id']!="" && $escaped_field_name!='date_created'){
				if($update_code!="") $update_code .= ",";
			    $update_code .= "`".$escaped_field_name."`='[".$escaped_field_name."]'";
			}
			$query_vars[$field_name] = $field_value;

			//$content .="<br>" . $val['name'] . " - " . $val['type'] . " - " . $val['order'] . " - " . $val['props'];


		}
		////
		$newArr = array();
		foreach($otherRequiredFieldsByID as $curr)
		  $newArr[] = "'" . $curr . "'";
        $otherRequiredFieldsByID = $newArr;

		$otherRequiredFieldsIDJSONArr = '[' . implode(',',$otherRequiredFieldsByID) . ']';
		////

		$eighteen_years_ago = date("Y-m-d",mktime(0,0,0,date("m"),date("d"),date("Y") - 18));
		$js_code = "";
		$js_code .= "requires_guardian_to_submit = false; ";
		$js_code .= "js_guardian_fields = new Array(); ";
		$js_code .= "function birthdate_selected(bdate) { ";
		$js_code .= "	if(bdate > '$eighteen_years_ago') {";
		$js_code .= "		set_display = 'table-row'; ";
		$js_code .= "		requires_guardian_to_submit = true; ";
		$js_code .= "	} else { ";
		$js_code .= "		set_display = 'none'; ";
		$js_code .= "		requires_guardian_to_submit = false; ";
		$js_code .= "	} ";
		for($n=0; $n<count($guardian_fields); $n++)
		{
			$gfield = $guardian_fields[$n]['name'];
			$gfield_id = $guardian_fields[$n]['id'];

			$js_code .= "	document.getElementById('$gfield').style.display = set_display; ";
			$js_code .= "	js_guardian_fields['$gfield_id'] = set_display; ";
			//$js_code .= "	document.".$formname.".".$gfield_input.".style.display = set_display; ";
		}
		$js_code .= "} ";

		$content .="<script type='text/javascript'>".
				$js_code .
		////
		      //"alert(\"other required fields by ID: $otherRequiredFieldsIDJSONArr\");".
		      "var otherRequiredFieldsIDArr = $otherRequiredFieldsIDJSONArr;".

		              "function isValidDate(dateStr){ ".
		                  "if(typeof dateStr != 'string'){return false;}".
		                  "return dateStr.match(/^[0-9][0-9][0-9][0-9]-[0-9][0-9]-[0-9][0-9]$/) ? true : false;".
		              "}".
                      
                      "function dateComplexFieldOnlyPartiallySet(elemsArray){".

                      		  "var dayFound = false;".
		                      "var monthFound = false;".
		                      "var yearFound = false;".
		                      "var totalSetDateParts = 0;".
		                      "for(i=0;i<elemsArray.length;i++){".
	                      		  "if(elemsArray[i].id && elemsArray[i].id.match(/^month_.*$/) ){".
	                                  "monthFound = true;".
	                                  "if(elemsArray[i].value != ''){ totalSetDateParts += 1; }".
	                                  "continue;".
	                              "}".
	                              "if(elemsArray[i].id && elemsArray[i].id.match(/^day_.*$/) ){".
	                              	  "dayFound = true;".
	                                  "if(elemsArray[i].value != ''){ totalSetDateParts += 1; }".
	                                  "continue;".
	                              "}".
	                              "if(elemsArray[i].id && elemsArray[i].id.match(/^year_.*$/) ){".
	                                  "yearFound = true;".
	                                  "if(elemsArray[i].value != ''){ totalSetDateParts += 1; }".
	                                  "continue;".
	                              "}".
                              "}".

                              //"alert('total set date parts:' + totalSetDateParts);".
                              "return totalSetDateParts == 1 || totalSetDateParts == 2; ".

                      "}".
                      
		              "function submitCustomerInfo(){".

        		          "var otherRequiredFields = [];".
        		          "for(i = 0; i < otherRequiredFieldsIDArr.length; i++){".
        		              "var currID = otherRequiredFieldsIDArr[i];".
        		              "var requiredElem = document.getElementById(currID);".
        		              "otherRequiredFields.push(requiredElem);".
        		          "}".

		                  "var inputCollection = document.getElementsByTagName('input');".
		                  "var selectsArr = document.getElementsByTagName('select');".
		                  "var elemsArray = otherRequiredFields.slice(0);".

		                  "for(i=0; i < inputCollection.length; ++i){ elemsArray.push( inputCollection[i] ); }".
		                  "for(i=0; i < selectsArr.length; i++){ elemsArray.push( selectsArr[i] ); }".

		                  "var missingCount = 0;".


		                  "var isOnlyPartialFilledOutDate = dateComplexFieldOnlyPartiallySet(elemsArray);".
		                  //"alert('isOnlyPartialFilledOutDate: ' + isOnlyPartialFilledOutDate);".
		                  "for( i = 0; i < elemsArray.length; i++){ ".

                              "if(!elemsArray[i].id){".
                                  "continue;".
                              "}".

                              "if(elemsArray[i].id && elemsArray[i].id.match(/^month_.*$/) ){".
                                  "continue;".
                              "}".

                              "if(elemsArray[i].id && elemsArray[i].id.match(/^day_.*$/) ){".
                                  "continue;".
                              "}".

                              "if(elemsArray[i].id && elemsArray[i].id.match(/^year_.*$/) ){".
                                  "continue;".
                              "}".


                              "var isNotFNLN = elemsArray[i].id != 'f_name' && elemsArray[i].id != 'l_name';".
                              "if(isNotFNLN && !elemsArray[i].id.match(/field[1-9]/)){".
                                  "continue;".
                              "}".


		                      "var req = elemsArray[i].getAttribute('required'); ".
		                      "var val = elemsArray[i].value;".
		                      "var custom = elemsArray[i].getAttribute('custom_type');".
		                      //Remove " or \
                              //'if( val.match(/\\\\/) ) { alert("Please no use of \\\\ in input fields."); return; }'.
                              //"if( val.match(/\\\"/) ) { alert('Please no use of \\\" in input fields.'); return; }".

                              "val = val.replace(/'/g, '');".
                              "elemsArray[i].value = val;".
                              "val = val.replace(/\"/g, '');".
                              "elemsArray[i].value = val;".

		                      "var isIncompleteDate = custom ? true : false; ".
		                      "isIncompleteDate = isIncompleteDate && custom == 'date';".
		                      "isIncompleteDate = isIncompleteDate && !isValidDate(elemsArray[i].value);".

                    		  "var hasVal = (val != '0' && val != '' && val !== false && val != null) ? true : false;".
                    		  "var isReq  = (req != '0' && req != '' && req !== false && req != null) ? true : false;".

							  "if(typeof js_guardian_fields[elemsArray[i].name] != 'undefined' && js_guardian_fields[elemsArray[i].name]=='none') isReq = false; ".
                    		  "var incDate = isIncompleteDate ? true : false;".
                    		  "var isMedPosted = (elemsArray[i].id == 'med_posted');".


                    		  "if(isMedPosted){ ".
                    		      "isReq = false; ".
                    		  "}".
		                      "var ast_id, astrixSpan;".
		                      //"alert('elems id:' + elemsArray[i].id + ' isOnlyPartialFilledOutDate:' + isOnlyPartialFilledOutDate);".  //<-------HERE WE NEED TO MAKE SURE RED * SHOWS UP ONLY ON 1/2 FILLED OUT DATE.
		                      //"alert('elems id:' + elemsArray[i].id + ' isOnlyPartialFilledOutDate:' + isOnlyPartialFilledOutDate + ' isReq:' + isReq + ' hasVal:' + hasVal + ' req:' + req);".
		                      "if( isReq && (!hasVal || (isOnlyPartialFilledOutDate && elemsArray[i].id == 'field7')) ){".//isIncompleteDate) ){ ".
		                          "missingCount++;".
		                          "ast_id = elemsArray[i].id +  '_astrix_id';".
		                          "astrixSpan = document.getElementById(ast_id);".
		                          "astrixSpan.innerHTML = '*';".
		                          "astrixSpan.style.color = '$astrixRed';".
		                      "}".
		                      "else if( isOnlyPartialFilledOutDate && elemsArray[i].id == 'field7' ){".
		                      	  "missingCount++;".
		                          "ast_id = elemsArray[i].id +  '_astrix_id';".
		                          "astrixSpan = document.getElementById(ast_id);".
		                          "astrixSpan.innerHTML = '*';".
		                          "astrixSpan.style.color = '$astrixRed';".
		                      "}".
		                      "else if(isReq && hasVal){ ".
		                          "ast_id = elemsArray[i].id + '_astrix_id';".
		                          "astrixSpan = document.getElementById(ast_id);".
		                          "astrixSpan.style.color = '$astrixGreen';".
		                      "}".
		                      "else{".

		                      "}".
		                      //"alert(elemsArray[i].id + 'end');".
		                  "}".

		                  "if(missingCount == 0){".
//"alert('wants to submit here');".
		                          "document.".$formname.".submit();".
		                  "}else{".
		                      "expReqDiv.expand();".
		                      "setTimeout(function(){expReqDiv.vanish();}, 3000); ".
		                      "setTimeout(function(){expReqDiv.close();},  4000); ".
		                  "}".
		              "}".
		           "</script>";
		$content .="<tr><td></td><td><input type='button' value='Submit' onclick='submitCustomerInfo()' /></td></tr>";
		$content .="</table>";
		$content .="</form>";

		if(isset($_POST['med_posted']))
		{
			//
			//med_customer_id
			//
			$query_vars['tablename'] = $tablename;
			$query_vars['id'] = $rowid;


			$row_updated = 0;
			if($rowid > 0)
			{
				$update_query = "update `[tablename]` set $update_code where `id`='[id]'";
				$success = lavu_query($update_query,$query_vars);
				if($success)
				{
					$row_updated = $rowid;
					$msg = "updated row $rowid";
				}
				else $msg = "failed to update row $rowid";
			}
			else
			{
				$insert_query = "insert into `[tablename]` ($insert_cols) values ($insert_vals)";
				$success = lavu_query($insert_query,$query_vars);
				if($success)
				{
					$row_updated = lavu_insert_id();
					//$msg = "inserted new row: $row_updated";
				}
				else $msg = "failed to insert new row";
			}

			if($row_updated)
			{

				$row_query = lavu_query("select * from `[tablename]` where `id`='[id]'",array("tablename"=>$tablename,"id"=>$row_updated));
				if(mysqli_num_rows($row_query))
				{
					$row_read = mysqli_fetch_assoc($row_query);
					$customer_id = $row_read['id'];

					if(reqvar("mode") == 'update_customer'){
					   $prevSearchTerm = $_GET['previousSearchTerm'];
    					return "<script type='text/javascript'> window.location=\"?searchterm=" . $previousSearchTerm . "&saved_position_id=".$saved_position_id."\"; </script>";
					}
					else{
    					//---getting print data
    					$customerArr = getCustomerById($customer_id);

							$customer_email = (isset($customerArr['Email']))?$customerArr['Email']:"";

    					$rpstr = "(--quot--)";
    					foreach($row_read as $ckey => $cval)
    					{
        				    $row_read[$ckey] = str_replace('"',$rpstr,$cval);
        				    $row_read[$ckey] = str_replace("\\","",$row_read[$ckey]);
    					}
                        foreach($customerArr as $ckey => $cval)
                        {
                            $customerArr[$ckey] = str_replace('"',  $rpstr, $cval);
                            $customerArr[$ckey] = str_replace("\\", "", $customerArr[$ckey] );
                        }

    //echo("CUSTOMER 2:    " . print_r($customerArr,1));exit();
                        unset($customerArr['id']);
    					$printJSONArr = get_printing_options_json_array($customerArr);

    				    $jsonInfo  = '{';
    				    $jsonInfo .=     '"print_info":'.$printJSONArr;
    				    $jsonInfo .= '}';


    				    $jsonInfo = str_replace('"', '\"', $jsonInfo);
    				    $jsonInfo = str_replace($rpstr,"\\\"",$jsonInfo);

    					//----------------------
    					$customer_name = trim($row_read['f_name'] . " " . $row_read['l_name']);
    					$customer_name = str_replace($rpstr,"\\\"",$customer_name);
    					$content = "<b><font style='color:#008800'>$msg</font></b><br>";

						/*if($_SERVER['REMOTE_ADDR']=="74.95.18.90")
						{
							global $parent_mode;
							global $parent_mode_vars;
							$content .= "---Debugging---<br>";
							$content .= "PMode: $parent_mode<br>";
							$content .= "PMode Vars: $parent_mode_vars<br>";
							$content .= "customer_selected(\"$customer_id\",\"$customer_name\",\"$jsonInfo\"); ";
							echo $content;
							exit();
						}*/
    					$content .= "<script language='javascript'>";
    					$content .= "customer_selected(\"$customer_id\",\"$customer_name\",\"$jsonInfo\",\"0\",\"$customer_email\"); ";
    					$content .= "</script>";
					}
				}
				else
				{
					$content = "<b><font style='color:#880000'>Error: Unable to insert/update customer</font></b><br>" . $content;
				}


			}
			else
			{
				$content = "<b><font style='color:#880000'>$msg</font></b><br>" . $content;
			}
		}
		//$content .= "<textarea rows='60' cols='120'>$lfields</textarea>";

		//foreach($order_read as $key => $val) echo $key . " = " . $val . "<br>";
		return $content;
	}
	function drawExpandingRequiredNotificationDiv(){
	/*
	   transition:height 1s;
       -webkit-transition:height 0.3s;
       -webkit-transition:opacity 0.3s ease-in 0.3s;
	*/

    ?>
        <div id='expandingRequiredNotificationDiv'></div>
        <script type='text/javascript'>

            var expReqDiv;
            var expReqDiv = document.getElementById('expandingRequiredNotificationDiv');
            expReqDiv.expand = function(){
                this.innerHTML = "Please Fill In Required Fields. *";
                this.style.opacity = '1';
                //this.style.backgroundColor = '#ADA';
                this.style.height = '15px';
                this.style.width = '100%';
                this.style.color = 'rgba(0,0,0,0)';
                this.style.textAlign = 'center';
                this.style.setProperty("-webkit-transition", "opacity 0.3s ease_in 0.3s");
                this.style.setProperty("-webkit-transition", "height 0.3s ease-in 0.3s");
                setTimeout(function(){expReqDiv.style.color = 'rgba(200,0,0,1.0)';}, 666);
            }
            expReqDiv.vanish = function(){
                this.style.opacity = '0';
                this.style.color = 'rgba(200,0,0,1)';
                this.style.setProperty("-webkit-transition", "opacity 0.3s");
                //expReqDiv.close();
            }
            expReqDiv.close  = function(){
                this.style.height = '0px';
                this.style.setProperty("-webkit-transition", "height 0.3s");
                this.style.width = '100%';
            }

        </script>
    <?php
	}
	//INSERTED BY Brian D.
	//Called just about immediately in customer.php
	function setGlobalVariablesForWhetherUsingWaivers($location_row){
		global $usesSignatureAndWaivers, $dateTimeLoc, $epochTimeLoc, $currentlyUsedWaiverRow, $mostCurrentValidWaiverRow, $data_name;
		$usesSignatureAndWaivers = false;

		//Setting global time variables
		$timezone = $location_row['timezone'];
		$serverDateTime = date('Y-m-d H:i:s');
		$dateTimeLoc = localize_datetime(date('Y-m-d H:i:s'), $timezone);
		$epochTimeLoc = strtotime($dateTimeLoc);

		//We first see if the lk_waiver table exists and that there's something in it.
		$tableExistResult = lavu_query("SELECT `table_name` FROM information_schema.tables WHERE `table_schema` = 'poslavu_" . $data_name . "_db' and `table_name` = 'lk_waivers'");
		if(mysqli_num_rows($tableExistResult)) {
			$containsAWaiverResult = lavu_query("SELECT * FROM `lk_waivers` WHERE `_deleted`='0' ORDER BY `id` DESC LIMIT 1");
			if(mysqli_num_rows($containsAWaiverResult)){
				$usesSignatureAndWaivers = true;
			}else{
				return;
			}
			$currentlyUsedWaiverRow = mysqli_fetch_assoc($containsAWaiverResult);
			$mostCurrentValidWaiverRow = $currentlyUsedWaiverRow;
		}
	}
	//END INSERT
	
	//Inserted by Brian D. 2014-04-29
	//We are now allowing for the option that the most recent users be displayed first on first launch.
	function setGlobalVariablesForWhetherShowRecentUsers(){
		global $showsMostRecentUsers;
		$result = lavu_query("SELECT * FROM `config` WHERE `setting`='customers_shows_most_recent'");
		if(!$result){
			error_log("MYSQL error in ".__FILE__." mysql error:".lavu_dberror());
			$showsMostRecentUsers = false;
			return;
		}
		else if(mysqli_num_rows($result) == 0){
			$showsMostRecentUsers = false;
		}
		else{
			$row = mysqli_fetch_assoc($result);
			if(strtolower($row['value']) != 'false' && $row['value'] != '0'){
				$showsMostRecentUsers = true;
			}
		}
		//We also add it to js land
		$jsShowsMostRecentUsers = $showsMostRecentUsers ? 'true' : 'false';
		echo "<script type='text/javascript'> var showsMostRecentUsers = $jsShowsMostRecentUsers; </script>";
		//echo "<script type='text/javascript'> alert('setting showsMostRecentUsers:'+showsMostRecentUsers); </script>";
	}

	function getDefaultEntriesArr(){
		$settingsArr = array('customer_settings_default_state' => '', 'customer_settings_default_city' => '');
		/*
		    $defaultState = getConfigBySetting('customer_settings_default_state');
        	$defaultCity = getConfigBySetting('customer_settings_default_city');
		*/
		$result = lavu_query("SELECT `setting`, `value` FROM `config` WHERE `setting` in ('customer_settings_default_city','customer_settings_default_state')");
		while($curr = mysqli_fetch_assoc($result)){
			$settingsArr[$curr['setting']] = $curr['value'];
		}
		return array('state' => $settingsArr['customer_settings_default_state'], 'city' => $settingsArr['customer_settings_default_city']);
	}

	function getJSONFieldTranslateDictionary(){ //This is used to translate the print_info fields from within the `original_id` field.
		$medCustomerFields = get_med_customer_fields();
		//error_log('medCustomerFields: '.print_r($medCustomerFields,1) );
		$fieldList = array();
		foreach($medCustomerFields as $currField){
			$fieldList[] = $currField['title'];
		}

		$jsString  = '';
		$jsString .= "{";
		$i = 0;
		foreach($fieldList as $currField){
			$currFieldTranslation = speak($currField);
			$jsString .= "'$currField':'" . str_replace("'", "", $currFieldTranslation) . "'";
			if($i < count($fieldList) - 1){ $jsString .= ','; }
			$i++;
		}
		$jsString .= "};";
		return $jsString;
	}

	function checkAutoSearchFocusAndSetJS(){
		$result = lavu_query("SELECT `setting`,`value` FROM `config` WHERE `setting`='auto_focus_customer_search'");
		if($result && mysqli_num_rows($result)){
			$row = mysqli_fetch_assoc($result);
			if(!empty($row['value'])){
				echo "<script>document.getElementById('search_text_id').focus();</script>";
			}
		}
	}
