<script type='text/javascript'>
    
    /*
    function timeOneMinusTimeTwo(timeOne, timeTwo){
        var timeOnePMOffset = timeOne.match(/^.*pm.*$/) ? 12 : 0;
        var timeTwoPMOffset = timeTwo.match(/^.*pm.*$/) ? 12 : 0;
        timeOne = timeOne.replace(/[ A-Za-z]/g, '');
        timeTwo = timeTwo.replace(/[ A-Za-z]/g, '');
        timeOneParts = timeOne.split(':');
        timeTwoParts = timeTwo.split(':');
        var timeOneHours   = timeOneParts[0]*1 + timeOnePMOffset*1;
        var timeOneMinutes = timeOneParts[1]*1;
        var timeTwoHours   = timeTwoParts[0]*1 + timeTwoPMOffset*1;
        var timeTwoMinutes = timeTwoParts[1]*1;
        var timeOneTotalSeconds = timeOneHours * 3600 + timeOneMinutes * 60;
        var timeTwoTotalSeconds = timeTwoHours * 3600 + timeTwoMinutes * 60;
        var differenceBySeconds = timeOneTotalSeconds - timeTwoTotalSeconds;
        var signMultiplier = differenceBySeconds < 0 ? -1 : 1;
        differenceBySeconds = Math.abs(differenceBySeconds);
        var hourDiff = Math.floor(differenceBySeconds/3600);
        var minDiff = (differenceBySeconds % 3600)/60;
        return {'hour':hourDiff,'min':minDiff,'sign':signMultiplier}
    }

    function timeOneMinusTimeTwo(timeOne, timeTwo){
        var timeOnePMOffset = timeOne.match(/^.*pm.*$/) ? 12 : 0;
        var timeTwoPMOffset = timeTwo.match(/^.*pm.*$/) ? 12 : 0;
        timeOne = timeOne.replace(/[ A-Za-z]/g, '');
        timeTwo = timeTwo.replace(/[ A-Za-z]/g, '');
        timeOneParts = timeOne.split(':');
        timeTwoParts = timeTwo.split(':');
        var timeOneHours   = timeOneParts[0]*1 + timeOnePMOffset*1;
        var timeOneMinutes = timeOneParts[1]*1;
        var timeTwoHours   = timeTwoParts[0]*1 + timeTwoPMOffset*1;
        var timeTwoMinutes = timeTwoParts[1]*1;
        var timeOneTotalSeconds = timeOneHours * 3600 + timeOneMinutes * 60;
        var timeTwoTotalSeconds = timeTwoHours * 3600 + timeTwoMinutes * 60;
        var differenceBySeconds = timeOneTotalSeconds - timeTwoTotalSeconds;
        var signMultiplier = differenceBySeconds < 0 ? -1 : 1;
        differenceBySeconds = Math.abs(differenceBySeconds);
        var hourDiff = Math.floor(differenceBySeconds/3600);
        var minDiff = (differenceBySeconds % 3600)/60;
        return {'hour':hourDiff,'min':minDiff,'sign':signMultiplier}
    }
    */
    function generateDateTimeGivenInterval(interval){
        var intervalInMinutes;
        var numberOfDivisions;
        if(interval.match(/^[0-9]*:.*$/)){
            var intervalParts = interval.split(':');
            var intervalHours = intervalParts[0];
            intervalInMinutes = intervalHours * 60;
            numberOfSubdivisions = 24/intervalHours;
        }else{
            intervalInMinutes = interval * 1;
            numberOfSubdivisions = 60/intervalInMinutes * 24;
        }
        var totalMinutes = 0;
        var timeOptionsArr = [];
        for( i = 0; i < numberOfSubdivisions; i++){
            var hourPart = Math.floor(totalMinutes/60);
            hourPart = (''+hourPart).length < 2 ? '0' + hourPart : hourPart;
            var minutePart  = totalMinutes % 60;
            minutePart = (''+minutePart).length < 2 ? '0' + minutePart : minutePart;
            var militaryTime = hourPart + ':' + minutePart;
            
            var finalTime = convertFromMilitaryToMeridian(militaryTime);
            timeOptionsArr.push(finalTime);
            totalMinutes += intervalInMinutes;
        }
        return timeOptionsArr;
    }
    function convertFromMilitaryToMeridian(timePassed){
        var timePassedParts = timePassed.split(':');
        var hourPart;
        if(timePassedParts[0]*1 == 0){
            return '12:' + timePassedParts[1] + ' am';
        }
        else if(timePassedParts[0]*1 < 12){
            return timePassedParts[0]*1 + ':' + timePassedParts[1] + ' am';
        }
        else if(timePassedParts[0]*1 == 12){
            return '12:' + timePassedParts[1] + ' pm';
        }
        else{
            var hourReturn = timePassedParts[0];
            hourReturn -= 12;
            return hourReturn + ':' + timePassedParts[1] + ' pm';
        }
    }
    function convertFromMeridianToMilitary(timePassed){
        var hoursAdded = 0;
        if(timePassed.match(/^.*pm.*$/) || timePassed.match(/^.*PM.*$/)){
            hoursAdded = 12;
        }
        timePassed = timePassed.replace(/[ a-zA-Z]/g, "");
        parts=timePassed.split(':');
        var hourPart = parts[0];
        hourPart = hourPart == '12' ? 0 : hourPart*1;
        hourPart += hoursAdded;
        hourPart = '' + hourPart;
        hourPart = hourPart.length < 2 ? '0' + hourPart : hourPart;
        return hourPart + ':' + parts[1];
    }
    function convertFromColonNotationToHoursMinsDisplayStr(time){
        timeParts = time.split(':');
        timeHourPart = timeParts[0]*1;
        timeMinutePart = timeParts[1]*1;
        var displayStr = '';
        //hour part
        if(timeHourPart == 0){
            displayStr += '';
        }
        else if(timeHourPart == 1){
            displayStr += '1 hr';
        }
        else{
            displayStr += timeHourPart + ' hrs';
        }
        displayStr += timeMinutePart != '' && timeHourPart != '' ? ' ' : '';
        //minute part
        if(timeMinutePart == 0){
            displayStr += '';
        }
        else if(timeMinutePart == 1){
            displayStr += '1 min';
        }
        else{
            displayStr += timeMinutePart + ' mins';
        }
        return '(' + displayStr + ')';
    }
</script>
