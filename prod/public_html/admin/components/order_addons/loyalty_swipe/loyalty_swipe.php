<!DOCTYPE html>
<html>
	<head>	
		<style type='text/css'>
			
			body {
				font-family:sans-serif;
				background-color: transparent;
				margin: 0 0 0 0;
				padding: 0 0 0 0;
				-webkit-tap-highlight-color:rgba(0, 0, 0, 0);
				-webkit-user-select: none;
			}
			
			body:after {
				content:
					url("images/bg_panel_310x160.png")
					url("images/btn_wow_113x37.png")
					url("images/bg_input_field_250x44.png");
				display: none;
			}
			
			.container_div {
				position:absolute;
				left:18px;
				top:17px;
				width:270px;
				height:120px;
			}
			
			.message_text {
				color:#555555;
				font-family:Arial, Helvetica, sans-serif;
				font-size:22px;
			}

			.btn_wow {
				color:#7d7d7d;
				font-family:Arial, Helvetica, sans-serif;
				font-size:17px;
				width:113px;
				height:37px;
				background:url("images/btn_wow_113x37.png");
				border:hidden;
			}
			
			.input_field {
				width:250px;
				height:44px;
				background:url("images/bg_input_field_250x44.png");
				background-repeat:no-repeat;
				background-position:center;
				border:hidden;
				text-indent:8px;
				font-family:Arial, Helvetica, sans-serif;
				font-size:20px;
				padding-top:7px;
			}
			
		</style>
		
		<script language='javascript'>
			
			function showPage(page) {
			
				var pages = ["swipe_view", "keyin_view"];
				
				var hiddenPage = "";
				var shownPage = "";
				
				for (var i = 0; i < pages.length; i++) {
					var pid = pages[i];
					var displayIs = document.getElementById(pid).style.display;
					var displayTo = (page == pid)?"block":"none";
					if (displayIs != displayTo) {					
						document.getElementById(pid).style.display = (page == pid)?"block":"none";
						if (displayTo == "block") shownPage = pid;
						if (displayTo == "none") hiddenPage = pid;
					}
				}
				
				if (shownPage != "") doOnShow(shownPage);
			}
			
			function doOnShow(pid) {
			
				switch (pid) {
					case "keyin_view": window.setTimeout(function(){focusOnInputField()}, 500); break;
					default: break;
				}
			}
			
			function focusOnInputField() {
			
				document.getElementById("input_cn").focus();
			}
			
			function startListening() {
			
				window.location = '_DO:cmd=startListeningForSwipe';
			}
			
			String.prototype.trimLeft = function(charlist) {
  			if (charlist === undefined)
    			charlist = "\s";
 
 				return this.replace(new RegExp("^[" + charlist + "]+"), "");
			};
			
			String.prototype.trimRight = function(charlist) {
  			if (charlist === undefined)
   				charlist = "\s";
 	
				return this.replace(new RegExp("[" + charlist + "]+$"), "");
			};
			
			String.prototype.trim = function(charlist) {
  				return this.trimLeft(charlist).trimRight(charlist);
			};

			function handleCardSwipe(swipeString, hexString, reader, encrypted) {
						
				var cardNumber = "";
				
				if (hexString.length > 0) {
					if (reader == "llsswiper") {
						var actual_swipe = fromHex(hexString);
						var tracks = actual_swipe.split("?");
						var track_parts = tracks[0].split(";");
						cardNumber = track_parts[1];
					}
				}
				
				if (cardNumber.length==0 && swipeString.length>0) {
				
					var tracks = swipeString.split("?");
					for (var i = 0; i < tracks.length; i++) {
						var trk = tracks[i].replace(/[^a-z;=\d]+/ig,'');
						if (trk.substring(0, 1) == ";") {
							var track2 = trk.split("=");
							cardNumber = track2[0].trim("; ");
							if (cardNumber.length > 0) break;
						}
					}
				}

				window.setTimeout(function(){useNumber(cardNumber)}, 50);
			
				return 1;
			}
			
			function fromHex(hex) {
				
				var str = "";
				for (var i = 0; i < hex.length; i += 2) {
					str += String.fromCharCode(parseInt(hex.substr(i, 2), 16));
				}
			
				return str;
			}
			
			function submitKeyedInNumber() {
			
				var inputField = document.getElementById("input_cn");
				var entered_number = inputField.value;
				
				if (entered_number.length < 1) componentAlert("Please try again.", "Invalid Card Number");
				else {
					inputField.blur();			
					useNumber(entered_number);
				}				
			}
			
			function componentAlert(title, message) {
			
				window.location = "_DO:cmd=alert&title=" + encodeURIComponent(title) + "&message=" + encodeURIComponent(message);
			}
			
			function useNumber(num) {
			
				window.location = '_DO:cmd=close_overlay&set_info=' + num + '|o|0';
			}

			function cancel() {

				document.getElementById("input_cn").blur();
				window.location = '_DO:cmd=close_overlay';
			}			
		
		</script>
	
	</head>
	
	<body onLoad='startListening();'>

		<div style='position:absolute; left:0px; top:0px; width:310px; height:160px; background-image:url("images/bg_panel_310x160.png"); opacity:0.97;'></div>

		<div id='swipe_view' class='container_div' style='display:block'>
			
			<center>
				<span class='message_text' style='position:absolute; left:5px; top:20px; width:260px; height:30px;'>Please Swipe Card</span>
			</center>
	
			<div style='position:absolute; left:0px; top:46px; width:270px; height:80px;'>
				<center>
					<table height='80px'>
						<tr>
							<td align='center' valign='middle'><button class='btn_wow' ontouchstart='cancel();'>Cancel</button></td>
							<td>&nbsp;</td>
							<td align='center' valign='middle'><button class='btn_wow' ontouchstart='showPage("keyin_view");'>Key In</button></td>
						</tr>
					</table>
				</center>
			</div>
	
		</div>

		<div id='keyin_view' class='container_div' style='display:none'>
		
			<div style='position:absolute; left:0px; top:5px; width:270px; height:44px;'>
				<center>
					<form onsubmit='submitKeyedInNumber(); return false;'>
						<input id='input_cn' class='input_field' type='tel' placeholder='CARD NUMBER' value=''>						
					</form>
				</center>
			</div>

			<div style='position:absolute; left:0px; top:46px; width:270px; height:80px;'>
				<center>
					<table height='80px'>
						<tr>
							<td align='center' valign='middle'><button class='btn_wow' ontouchstart='cancel();'>Cancel</button></td>
							<td>&nbsp;</td>
							<td align='center' valign='middle'><button class='btn_wow' ontouchstart='submitKeyedInNumber();'>Submit</button></td>
						</tr>
					</table>
				</center>
			</div>

		</div>
		
	</body>
</html>		