<?php 

require_once(dirname(__FILE__) . "/../comconnect.php");
//require_once(dirname(__FILE__) . "/../order_addons/customer_functions.php");
require_once(dirname(__FILE__) . '/RecurringBilling.php' );

class MembersSigninsSite {
    // Site vars
    var $page;
    var $title;
    var $msg;

    // App vars posted to every component.
    var $app;

    // Location info hash
    var $location_info;

    // Page content vars
    var $html;
    var $css;
    var $js;

	// Customer Billing
	var $billingUtil;
	var $billingProfile;

    // Constructor
    public function __construct() {
		setlocale(LC_MONETARY, "en_US");

        // Site vars
        $this->page = (isset($_REQUEST["page"])) ? strtolower( $_REQUEST["page"] ) : "";

        // Standard vars posted to every component from the app.
        $this->app["comp_name"] = (isset($_REQUEST["comp_name"])) ? $_REQUEST["comp_name"] : "";
        $this->app["server_id"] = (isset($_REQUEST["server_id"])) ? $_REQUEST["server_id"] : "";
        $this->app["server_name"] = (isset($_REQUEST["server_name"])) ? $_REQUEST["server_name"] : "";
        $this->app["loc_id"] = (isset($_REQUEST["loc_id"])) ? $_REQUEST["loc_id"] : "";
        $this->app["tab_name"] = (isset($_REQUEST["tab_name"])) ? $_REQUEST["tab_name"] : "";
        $this->app["cc"] = (isset($_REQUEST["cc"])) ? $_REQUEST["cc"] : '';
        $this->app["dn"] = (isset($_REQUEST["dn"])) ? $_REQUEST["dn"] : "";

        // Location info, once we have the dataname and location ID.
        if ( isset( $_SESSION['timezone'] ) ) {
        	date_default_timezone_set( $_SESSION['timezone'] );
        }
        else if ( empty( $_SESSION['timezone'] ) && isset( $this->app["dn"] ) && isset( $this->app["loc_id"] )  ) {
        	$this->location_info = get_location_info( $this->app["dn"], $this->app["loc_id"] );  // From core_functions.php

	        // Default timezone, based on CP settings.
        	if ( !empty( $this->location_info['timezone'] ) ) {
	        	date_default_timezone_set( $this->location_info['timezone'] );
	        	$_SESSION['timezone'] = $this->location_info['timezone'];
        	}
        }

        // Fitness component-specific vars
		$this->billingUtil = new RecurringBilling();
		$this->customer_id = (isset($_REQUEST['customer_id'])) ? $_REQUEST['customer_id'] : '';
		$this->invoice_id = (isset($_REQUEST['invoice_id'])) ? $_REQUEST['invoice_id'] : '';
		$this->med_posted = (isset($_REQUEST['med_posted'])) ? $_REQUEST['med_posted'] : "";

		$this->overridePage();

		if ( !empty($this->customer_id) ) {
			$this->billingProfile = $this->billingUtil->getBillingProfileForCustomerId( $this->customer_id );
			$this->setWaiverSignedFlag();
		}
    }

    public function appVarsQuerystring() {
        $querystring = "";
        foreach ($this->app as $key => $val) {
        	if ( $key != 'cc' ) {  // Viewing page=invoice kept breaking until cc was suppressed
        		$querystring .= "&" . urlencode($key) ."=". urlencode($val);
        	}
        }
        return $querystring;
    }

	public function insertMedActionLog( $customerId, $action, $orderId ) {
		// Set device_time based on the location's time zone information.
		$device_time = date("Y-m-d H:i:s");

		// Insert sign-in - we will already have the user's info if rfid was passed in.
		$query_vars = array(
			"action" => $action,
			"customer_id" => $customerId,
			"order_id" => $orderId,
			"restaurant_time" => date("Y-m-d H:i:s"),
			"user_id" => $this->app["server_id"],
			"loc_id" => $this->app["loc_id"],
			"device_time" => $device_time
		);
		$insert_query = "insert into `med_action_log` (`action`, `customer_id`, `order_id`, `restaurant_time`, `user_id`, `loc_id`, `device_time`) values ('[action]', '[customer_id]', '[order_id]', '[restaurant_time]', '[user_id]', '[loc_id]', '[device_time]')";
		$success = lavu_query($insert_query,$query_vars);
		if ($success) {
			$row_updated = lavu_insert_id();
			//$msg = "inserted new row: $row_updated";
			$this->msg = "inserted new row: $row_updated";
		}
		else {
			//$msg = "failed to insert new row";
			$this->msg = "failed to insert new row";
		}

		return $success;
	}

	private function overridePage()
	{
		///error_log( "In overridePage() with page={$site->page} customer_id={$site->customer_id} med_posted={$site->med_posted} SESSION[customer_id]={$_SESSION['customer_id']}" );  //debug

		// When newuser page posts a new user to create, we actually send it back through
		// the newuser routing branch again to re-run draw_med_customer_form() (by way
		// of its wrapper method) because that will process the new user insert.
		if ($this->page == '' && $this->med_posted == '1' && empty($this->customer_id) && empty($_SESSION['customer_id'])) {
			$this->page = "viewnewuser";
		}
		// Run the wrapper function htmlForEditUserInfo() to update the user's information by calling
		// htmlForEditUserInfo() (but not saving the returned html, since we're going to display the
		// user's information) and then re-pulling the user's *updated* data.
		else if ($this->page == '' && $this->med_posted == '1' && empty($this->customer_id) && !empty($_SESSION['customer_id'])) {
			$this->customer_id = $_SESSION['customer_id'];
			$_SESSION['customer_id'] = '';

			// Send people who just submitted editted user info back to the view user page
			// so they can see their newly updated info.
			$this->page = 'saveuser';
		}
		// When we are editing a user's info but haven't posted the data back
		// to the draw_med_customer_form() form yet, we have to save the customer_id
		// in the session so we can pull it out when/if they submit the form and go
		// to update the user's info.
		else if ($this->page == 'edituser' && $this->med_posted == '') {
			$_SESSION['customer_id'] = $this->customer_id;
		}

	}

	public function fetchMemberInfo($searchParams) {
		$row_query = '';
		$row_read = '';

		$this->userFound = 0;

		if (isset($searchParams['id'])) {
			$row_query = lavu_query("select * from `med_customers` where `_deleted`='0' and `id`='[id]'", array("id" => $searchParams['id']));
		}
		else {
			$row_query = lavu_query("select * from `med_customers` where `_deleted`='0' and `rfid`='[rfid]'", array("rfid" => $searchParams['rfid']));
		}

		if ($row_query !== FALSE && mysqli_num_rows($row_query)) {
			$this->userFound = 1;

		    // Create js Member object using data from database.
			$row_read = mysqli_fetch_assoc($row_query);

			// We may override some already-set values but we have to make sure all of these are set.
			$this->customer_id = $row_read["id"];
			$this->rfid = $row_read["rfid"];

			$this->setWaiverSignedFlag();

			// Load their billing profile, which we'll need to fill out the memberInfo array.
			$this->billingProfile = $this->billingUtil->getBillingProfileForCustomerId( $this->customer_id );

			// Escape any double-quotes contained in the member data so the javascript doesn't break.
			$this->memberInfo["FirstName"] = str_replace('"', '\"', $row_read["f_name"]);
			$this->memberInfo["LastName"] = str_replace('"', '\"', $row_read["l_name"]);
			$this->memberInfo["Email"] = str_replace('"', '\"', $row_read["field1"]);
			$this->memberInfo["Address"] = str_replace('"', '\"', $row_read["field3"]);
			$this->memberInfo["City"] = str_replace('"', '\"', $row_read["field4"]);
			$this->memberInfo["State"] = str_replace('"', '\"', $row_read["field5"]);
			$this->memberInfo["ZipCode"] = str_replace('"', '\"', $row_read["field6"]);
			$this->memberInfo["HomePhone"] = str_replace('"', '\"', $row_read["field2"]);
			$this->memberInfo["WorkPhone"] = str_replace('"', '\"', $row_read["field10"]);
			$this->memberInfo["CellPhone"] = str_replace('"', '\"', $row_read["field11"]);
			$this->memberInfo["DOB"] = str_replace('"', '\"', $row_read["field7"]);
			$this->memberInfo["Company"] = str_replace('"', '\"', $row_read["field8"]);
			$this->memberInfo["HowHeardAboutUs"] = str_replace('"', '\"', $row_read["field9"]);
			$this->memberInfo["ProfilePicture"] = (empty($row_read["profile_picture"])) ? 'http://'. $_SERVER['HTTP_HOST'] .'/components/fitness/images/default_profile_pic/SIZE/default_profile_pic.png' : str_replace('"', '\"', $row_read["profile_picture"]);
			$this->memberInfo['Membership'] = isset($this->billingProfile['name']) ? $this->billingProfile['name'] : '-';
			$this->memberInfo['MemberSinceDate'] = date('n/j/Y', strtotime($row_read['date_created']));
			$this->memberInfo['MembershipStartDate'] = isset($this->billingProfile) ? date('n/j/Y', strtotime($this->billingProfile['start_date'])) : '-';
			$this->memberInfo['MembershipEndDate'] = (isset($this->billingProfile) && $this->billingProfile['end_date'] != '0000-00-00') ? date('n/j/Y', strtotime($this->billingProfile['end_date'])) : '-';
			$this->memberInfo["MembershipOutstandingBalance"] = isset( $this->billingProfile ) ? money_format( '%.2n', floatval( $this->billingUtil->getUnpaidAmountForCustomerId( $this->customer_id ) ) ) : '-';
			$this->memberInfo["MembershipExpirationDate"] = (isset( $this->billingProfile ) && isset($this->billingProfile['end_date']) && $this->billingProfile['end_date'] != '0000-00-00 00:00:00') ? date("n/j/Y", strtotime( $this->billingProfile['end_date'] ) ) : '-';
			$this->memberInfo["CreditCardExpirationDate"] = isset( $this->billingProfile ) ? $this->billingProfile['cc_exp_date'] : '-';

			// Add this on the jpg's "querystring" to generate unique requests to prevent caching
			$unixtimestamp = ($this->memberInfo["ProfilePicture"] != '') ? '?' . time() : '';

			// Set up keys for the different image sizes that are automatically captured of each profile picture.
			$this->memberInfo["ProfilePictureThumbnail"] = str_replace('SIZE/', '', $this->memberInfo["ProfilePicture"]);
			$this->memberInfo["ProfilePictureMain"] = str_replace('SIZE/', 'main/', $this->memberInfo["ProfilePicture"] . $unixtimestamp);
			$this->memberInfo["ProfilePictureFull"] = str_replace('SIZE/', 'full/', $this->memberInfo["ProfilePicture"]);
	    }

	    return $row_read;
	}

    public function setWaiverSignedFlag() {
	// Check if waiver and/or contract has been signed by checking to see if signature image file exists for their customer ID.
	$this->waiver_signed = file_exists( dirname(__FILE__) . "/../lavukart/companies/{$this->app['dn']}/signatures/signature_{$this->customer_id}.jpg" );
	$this->contract_signed = file_exists( dirname(__FILE__) . "/../lavukart/companies/{$this->app['dn']}/signatures/signature_contract_{$this->customer_id}.jpg" );

	// Check if contracts are supported by this Lavu Fit account by checking to see if the `lk_waivers`.`type` column exists.
	$contract_query = lavu_query("SHOW COLUMNS FROM `lk_waivers` LIKE 'type'");
	$this->contract_supported = ($contract_query !== false && mysqli_num_rows($contract_query));
    }

    public function htmlForNewMember() {

		// Let the med_customer function rendering its new user edit fields for the member fields section.
		$memberInfoFields = draw_med_customer_form(false, 0);

		$html = <<<HTML

		    <div class="invoice">

				<div id="closeButton" class="closeButton">X</div>
				<script>new FastButton( document.getElementById("closeButton"), function() { loadLastLoadedMemberSignupsPage() } );</script>

		    	<h3>Create New Member</h3>

				<div id="memberInfoFields">

					{$memberInfoFields}

				</div>

		    </div>

HTML;

		return $html;

	}

	public function htmlForDeletedMemberProfile($msg) {

	    // Render html for displaying information about the attempt to delete the member profile.
	    $html .= <<<HTML

		    <div class="invoice">
				<div id="closeButton" class="closeButton">X</div>
				<script>
					new FastButton( document.getElementById("closeButton"), function() { loadLastLoadedMemberSignupsPage() } );
					run_com_command("members","hideMedCustomerRow("+ $site->customer_id +")");
				</script>

		    	<h3>Member Profile Deleted</h3>

				<div id="memberInfoFields">

					{$msg}

				</div>
		    </div>
HTML;

		return $html;
	}

    public function htmlForMemberProfile() {
		$memberInfoFields = '';

		// Let the med_customer function save the edited member info before we look up the customer data.  But we don't use its outputted html.
		if ( $this->page == 'saveuser' ) {
			draw_med_customer_form(false, $this->customer_id);
		}
		// Let the med_customer function insert a new member before we look up the ID and customer data.  We don't use its outputted html here, either.
		else if ( $this->page == 'viewnewuser' ) {
			$insertOutput = draw_med_customer_form(false, 0);
			$this->customer_id = $this->fetchLastInsertedUserId();
		}

	    // Look up member info.
		$memberRow = $this->fetchMemberInfo( array('id' => $this->customer_id) );

		if ( $this->page == 'edituser' ) {
			$memberInfoFields = draw_med_customer_form(false, $this->customer_id);
		}
		// We drawing the member fields using other med_customers functions to get the list of fields.
		else {
			$memberInfoFields .= '';
			foreach ( get_med_customer_fields() as $fieldIndex => $fieldInfo )
			{
				$fieldName = $fieldInfo['title'];
				$fieldValue = $memberRow[$fieldInfo['column']];

				if ( $fieldName == 'DOB' && !empty($fieldValue) ) {
					$dobDateFields = explode( '-', $fieldValue );
					$fieldValue = $dobDateFields[1] .'/'. $dobDateFields[2] .'/'. $dobDateFields[0];
				}


				$memberInfoFields .= <<<HTML
					<tr><td class="memberFieldLabel">{$fieldName}</td><td>{$fieldValue}</td></tr>

HTML;
			}
			$memberInfoFields = "<table border='0'>$memberInfoFields</table>";
		}

		if ( $this->userFound ) {
			$memberFirstName = htmlspecialchars($this->memberInfo['FirstName']);
			$memberLastName = htmlspecialchars($this->memberInfo['LastName']);
			$memberName = $memberFirstName .' '. $memberLastName;

			// Render html for displaying the member profile.
			$html .= <<<HTML

		    <div class="invoice">

				<div id="closeButton" class="closeButton" style="display:none">X</div>
				<script>setTimeout(function() {
					document.getElementById('closeButton').style.display = 'block';
					document.getElementById('delay_linkuser').style.display = 'block';
					document.getElementById('manualSignInButton').style.display = 'block';
					new FastButton( document.getElementById("closeButton"), function() { loadLastLoadedMemberSignupsPage() } ); }, 300);
				</script>

		    	<h3>{$memberName}</h3>

		    	<div id="memberProfileTabs">
					<ul class="tabs">
						<li id="profileTab" class="selected">Profile</li>
						<li id="billingTab" ontouchstart="loadMemberBilling('{$this->customer_id}')">Billing</li>
						<li id="historyTab" ontouchstart="loadMemberHistory('{$this->customer_id}')">History</li>
					</ul>
				</div>

				<div id="memberProfile">

					<img id="memberPicture" width="163" height="163" src="{$this->memberInfo["ProfilePictureMain"]}" border="0" />

					<div class="memberProfileLabel" style="top:0px;  left:175px">Membership</div>
					<div class="memberProfileStat"  style="top:15px; left:175px; width:400px">{$this->memberInfo['Membership']}</div>


					<div class="memberProfileLabel" style="top:35px; left:175px">Start Date</div>
					<div class="memberProfileStat"  style="top:50px; left:175px">{$this->memberInfo['MembershipStartDate']}</div>

					<div class="memberProfileLabel" style="top:35px; left:290px">End Date</div>
					<div class="memberProfileStat"  style="top:50px; left:290px">{$this->memberInfo['MembershipEndDate']}</div>


					<div class="memberProfileLabel" style="top:70px; left:175px">Member Since</div>
					<div class="memberProfileStat"  style="top:85px; left:175px">{$this->memberInfo['MemberSinceDate']}</div>

					<div class="memberProfileLabel" style="top:70px; left:290px">Member ID</div>
					<div class="memberProfileStat"  style="top:85px; left:290px">{$this->customer_id}</div>


<!--
					<div class="memberProfileLabel" style="top:70px; left:175px">Balance Due</div>
					<div class="memberProfileStat"  style="top:85px; left:175px">{$this->memberInfo['MembershipOutstandingBalance']}</div>

					<div class="memberProfileLabel" style="top:70px; left:290px">Credit Card Expires</div>
					<div class="memberProfileStat"  style="top:85px; left:290px">{$this->memberInfo['CreditCardExpirationDate']}</div>
-->

					<div class="memberProfileLabel" style="top:105px; left:175px">Badge ID</div>
					<div class="memberProfileStat"  style="top:120px; left:175px; width:400px">{$this->rfid}</div>

<!--
					<div class="memberProfileLabel" style="top:105px; left:290px">Badge ID</div>
					<div class="memberProfileStat"  style="top:120px; left:290px">{$this->rfid}</div>
-->

					<div style="position:absolute; top:140px; left:270px; display:none" id="delay_linkuser">
			    		<form onsubmit="return confirm('Are you sure you want to set this as the Member Account for {$this->app['server_name']}?')">
						<input type="hidden" name="page" value="linkuser" />
						<input type="hidden" name="customer_id" value="{$this->customer_id}" />
						<input type="hidden" name="billingProfileId" value="{$billingProfile['id']}" />
			    			<input type="hidden" name="server_id" value="{$this->app['server_id']}" />
						<input type="submit" id="arbLinkMemberToUser" class="arbButton" style="width:165px" value="Link to Employee" />
					</form>
			    		<form onsubmit="return confirm('Are you sure you want to delete this Member Profile?')">
						<input type="hidden" name="customer_id" value="{$this->customer_id}" />
						<input type="hidden" name="billingProfileId" value="{$billingProfile['id']}" />
			    			<input type="hidden" name="server_id" value="{$this->app['server_id']}" />
			    			<input type="hidden" name="saved_position_id" value="{$_REQUEST['saved_position_id']}" />
						<input type="hidden" name="FirstName" value="{$memberFirstName}" />
						<input type="hidden" name="LastName" value="{$memberLastName}" />
						<input type="submit" id="deleteProfile" class="arbButton" name="page" value="Delete" />
			    		</form>
					</div>

					<div id="profilePictureToolbar" class="hidden">
						<input type="button" id="savePictureButton" name="savePicture" value="Save Picture" />
						<input type="button" id="flipPictureButton" name="flipPicture" value="Flip Picture" />
						<script>
							new FastButton( document.getElementById("savePictureButton"), function() { saveProfilePicture() } );
							new FastButton( document.getElementById("flipPictureButton"), function() { flipProfilePicture() } );
						</script>
					</div>

					<div id="memberProfileToolbar">
						<img id="camera_icon" height="35" src="images/camera_icon.png"  border="0" ontouchstart="takePicture()" />
						<img id="badge_icon"  height="35" src="images/badge_icon.png"   border="0" ontouchstart="launchSetCardOverlay('{$this->customer_id}')" />
						<img id="waiver_icon" height="35" src="images/waiver_icon2.png" border="0" ontouchstart="launchWaiver()" />
						<img id="cart_icon"   height="35" src="images/cart_icon.png"    border="0" ontouchstart="jumpToOrder(null)" />
						<img id="edit_icon"   height="35" src="images/edit_icon.png"    border="0" ontouchstart="loadMemberEditFields('{$this->customer_id}', '{$this->page}')" />

						<script>
							//new FastButton( document.getElementById("camera_icon"), function() { takePicture() } );
							//new FastButton( document.getElementById("badge_icon"), function() { launchSetCardOverlay("{$this->customer_id}") } );
							//new FastButton( document.getElementById("waiver_icon"), function() { launchWaiver() } );
							//new FastButton( document.getElementById("cart_icon"), function() { jumpToOrder(null) } );
							//new FastButton( document.getElementById("edit_icon"), function() { loadMemberEditFields("{$this->customer_id}", "{$this->page}") } );
						</script>

						<input type="button" border="1" id="manualSignInButton" value="Manual Sign-in" onclick="newManualSignIn('{$this->customer_id}')" />
					</div>

				</div>

				<div id="memberInfoFields">

					{$memberInfoFields}

				</div>

		    </div>

HTML;

		}
		else {
			// Render html for displaying that no member profile could be found.
			$html .= <<<HTML

		    <div class="invoice">

				<div id="closeButton" class="closeButton">X</div>
				<script>new FastButton( document.getElementById("closeButton"), function() { loadLastLoadedMemberSignupsPage() } );</script>

		    	<h3>Member Not Found</h3>

		    </div>
HTML;
		}


		return $html;
	}

    public function htmlForMemberBilling() {
    	$html = '';

    	$allBillingProfilesForCustomer = $this->billingUtil->getAllBillingProfilesForCustomer( $this->customer_id );

    	foreach ( $allBillingProfilesForCustomer as $billingProfile ) {

			$arbStatus = ucfirst( $billingProfile['status'] );
	    	$arbCreateDate = date( 'n/j/Y g:i a', strtotime( $billingProfile['createtime'] ) );
	    	$arbButtonAction = ($billingProfile['status'] == 'active') ? 'cancel' : 'reactivate';
	    	$arbCancelButton = ($billingProfile['status'] == 'active') ? 'displayed' : 'hidden';
	    	$arbReactivateButton = ($billingProfile['status'] == 'active') ? 'hidden' : 'displayed';
	    	$newInvoiceDayOfMonth = ($billingProfile['bill_event_offset'] == '1') ? '15' : '1';

		    $html .= <<<HTML

		    <div class="invoice">

				<div id="closeButton" class="closeButton">X</div>
				<script>new FastButton( document.getElementById("closeButton"), function() { loadLastLoadedMemberSignupsPage(); } );</script>

		    	<h3>Billing Profile</h3>

		    	<div id="memberProfileTabs">
					<ul class="tabs">
						<li id="profileTab" ontouchstart="loadMemberProfile('{$this->customer_id}')">Profile</li>
						<li id="billingTab" class="selected">Billing</li>
						<li id="historyTab" ontouchstart="loadMemberHistory('{$this->customer_id}')">History</li>
					</ul>
				</div>


		    	<table border="0">
		    	<tr>
			    	<td><em>Status:</em> {$arbStatus}</td>
			    	<td><em>CC Type:</em> {$billingProfile['cc_card_type']}</td>
			    	<td valign="top">
			    		<form onsubmit="return confirm('Are you sure you want to {$arbButtonAction} billing for this customer?')">
						<input type="hidden" name="page" value="{$arbButtonAction}" />
						<input type="hidden" name="customer_id" value="{$this->customer_id}" />
						<input type="hidden" name="billingProfileId" value="{$billingProfile['id']}" />
						<input type="submit" id="arbCancelButton" class="arbButton {$arbCancelButton}" value="X Cancel Billing" />
						<input type="submit" id="arbReactivateButton" class="arbButton {$arbReactivateButton}" value="+ Reactivate Billing" />
						</form>
			    	</td>
		    	</tr>
		    	<tr>
			    	<td><em>Created:</em> {$arbCreateDate}</td>
			    	<td><em>CC Last 4:</em> {$billingProfile['cc_num_last4']}</td>
			    	<td valign="top">
<!--
			    		<form onsubmit="return confirm('Are you sure you want to set this as the Member Account for {$this->app['server_name']}?')">
						<input type="hidden" name="page" value="linkuser" />
						<input type="hidden" name="customer_id" value="{$this->customer_id}" />
						<input type="hidden" name="billingProfileId" value="{$billingProfile['id']}" />
			    		<input type="hidden" name="server_id" class="hidden" value="{$this->app['server_id']}" />
						<input type="submit" id="arbLinkMemberToUser" class="arbButton" value="Link to Employee" />
						</form>
-->
			    		<form onsubmit="return confirm('Are you sure you want to change the Invoice Day of Month to {$newInvoiceDayOfMonth} for this customer?')">
						<input type="hidden" name="page" value="invoicedayofmonth" />
						<input type="hidden" name="customer_id" value="{$this->customer_id}" />
						<input type="hidden" name="billingProfileId" value="{$billingProfile['id']}" />
			    		<input type="hidden" name="newInvoiceDayOfMonth" class="hidden" value="{$newInvoiceDayOfMonth}" />
						<input type="submit" id="arbChangeInvoiceDayOfMonthButton" class="arbButton" value="Change Invoice Day of Month to {$newInvoiceDayOfMonth}" />
						</form>
			    	</td>
		    	</tr>
		    	<tr>
			    	<td valign="top"><em>Invoice Day of the Month:</em> {$billingProfile['bill_event_offset']}</td>
			    	<td valign="top"><em>CC Expir Date:</em> {$billingProfile['cc_exp_date']}</td>
			    	<td valign="top">
			    	</td>
		    	</tr>
		    	</table>

		    </div>

		    <h2>Invoices</h2>
HTML;

			$invoices = $this->billingUtil->getAllInvoicesForBillingProfile( $billingProfile, 'desc' );

			if ( count( $invoices ) == 0 ) {
				$html .= <<<HTML

				<p class="gray">(None)</p>
HTML;
			}

			$i = 1;
			$lastInvoiceIndex = count($invoices);
			$invoiceDivsToMake = array();
	    	foreach ( $invoices as $invoice ) {
	    		$invoiceNumber = $invoice['order_id'];
	   			$invoiceDate = date("M d, Y", strtotime( $invoice['opened'] ) );
	   			$invoiceTotal = '$'. number_format( $invoice['total'], 2 );
	   			$invoiceDueAmount = '$'. number_format( $this->billingUtil->getUnpaidAmountOnInvoice( $invoice ), 2 );
	   			$elementId = $invoiceNumber .'-'. $i;
		    	$html .= <<<HTML

		<figure>
			<img id="{$elementId}" src="images/doc.png" width="64" height="64" border="0" />
			<figcaption>{$invoiceDate}<br>Total: {$invoiceTotal}<br>Due: {$invoiceDueAmount}</figcaption>
		</figure>
<!--	<script>new FastButton( document.getElementById("{$elementId}"), function() { loadInvoiceInfo('{$this->customer_id}', '{$invoiceNumber}') } );</script>-->
		<script>new FastButton( document.getElementById("{$elementId}"), function() { hideshowInvoice('invoice-{$elementId}') } );</script>

HTML;

				// We batch up the invoice numbers for the row we're rendering until we get to the last one of the row
				// (the 4th one) or the last one of the series, then we write out a series of hidden divs that will be
				// displayed via javascript hide/show technology.
				$invoiceDivsToMake[$invoiceNumber] = $elementId;
				if ( ($i % 4 === 0) || ($i == $lastInvoiceIndex) ) {
					foreach ( $invoiceDivsToMake as $invoiceNumberForDivToMake => $iconElementId ) {
						$html .= $this->htmlForViewingInvoiceInline($invoiceNumberForDivToMake, $iconElementId);
					}
					$invoiceDivsToMake = array();
				}

				$i++;
	    	}
		}

    	if ( count( $allBillingProfilesForCustomer ) == 0 ) {
			$html = <<<HTML

		    <div class="invoice">

				<div id="closeButton" class="closeButton">X</div>
				<script>new FastButton( document.getElementById("closeButton"), function() { loadRecentSignIns() } );</script>

		    	<h3>Billing Profile</h3>

				<h1>(None)</h1>

				</div>

		    </div>

HTML;

		}


    	return $html;
    }

    public function htmlForMemberHistory() {
    	$html = '';

   	    $html .= <<<HTML

   	    	<div class="invoice">

			<div id="closeButton" class="closeButton">X</div>
			<script>new FastButton( document.getElementById("closeButton"), function() { loadLastLoadedMemberSignupsPage() } );</script>

	    	<div id="memberProfileTabs">
				<ul class="tabs">
					<li id="profileTab" ontouchstart="loadMemberProfile('{$this->customer_id}')">Profile</li>
					<li id="billingTab" ontouchstart="loadMemberBilling('{$this->customer_id}')">Billing</li>
					<li id="historyTab" class="selected">History</li>
				</ul>
			</div>

   	    	<h3>Member History</h3>

			<table border="0" class="invoiceItems">

HTML;

   	// Iterate over history events
	$memberHistoryQuery = lavu_query("select lower(date_format( str_to_date(restaurant_time, '%Y-%m-%d %H:%i:%s'), '%c/%e/%Y %l:%i %p')) as event_time, action, user_id from `med_action_log` where `customer_id` = '{$this->customer_id}' order by restaurant_time desc");
	$memberHistoryQueryNumRows = mysqli_num_rows($memberHistoryQuery);

	if ($memberHistoryQueryNumRows > 0) {

		// Build a hash of all of the employee names using their ID as key.
		$employeeName = array();
		$employeeNameQuery = lavu_query("select id, f_name, l_name from `users`");
		if ($employeeNameQuery === FALSE) die( "Couldn't get employee name from users table:". lavu_dberror() );
		while ( $row = mysqli_fetch_assoc($employeeNameQuery) ) {
			$employeeName[$row['id']] = $row['f_name'] .' '. $row['l_name'];
		}

		$html .= <<<HTML

	    	<tr class="header">
		    	<td align="left"><em>Date</em></td>
		    	<td align="left"><em>Event</em></td>
		    	<td align="left"><em>Employee</em></td>
		    </tr>

HTML;

		while ($row = mysqli_fetch_assoc($memberHistoryQuery)) {

			$eventDate = $row['event_time'];
			$eventName = $row['action'];
			$employeeName = (!empty($row['user_id']) && isset($employeeName[$row['user_id']])) ? $employeeName[$row['user_id']] : '';

	   	    $html .= <<<HTML

			<tr>
				<td align="left" style="font-size:9px" nowrap>{$eventDate}</td>
				<td align="left" style="font-size:10px"  nowrap>{$eventName}</td>
				<td align="left" style="font-size:9px" nowrap>{$employeeName}</td>
			</tr>

HTML;
		}

	}

   	    $html .= <<<HTML

			</tr>
			</table>

			</div>

HTML;


    	return $html;
	}

    public function htmlForViewingInvoice($orderId) {
    	$html = '';

    	$invoice = $this->billingUtil->getInvoice( $orderId );
 		$invoiceDate = date("n/j/Y", strtotime( $invoice['opened'] ) );
    	$invoiceTotal = '$'. number_format( $invoice['total'], 2 );
    	$invoiceDueAmount = '$'. number_format( $this->billingUtil->getUnpaidAmountOnInvoice( $invoice ), 2 );
    	$cartIconDisplayCss = ( !empty($invoice['closed']) ) ? 'none' : 'block';

   	    $html .= <<<HTML

   	    	<div class="invoice">

			<div id="closeButton" class="closeButton">X</div>
			<script>new FastButton( document.getElementById("closeButton"), function() { loadMemberBilling('{$this->customer_id}'); } );</script>

   	    	<h3>Invoice #{$orderId}</h3>

	    	<table border="0" cellpadding="10" class="summaryBlock">
	    	<tr>
		    	<td><em>Date:</em> {$invoiceDate}</td>
		    	<td><em>Total</em> {$invoiceTotal}</td>
		    	<td><em>Due</em> {$invoiceDueAmount}</td>
		    	<td style="display:{$cartIconDisplayCss}"><img id="cart_icon" height="35" src="images/cart_icon.png" border="0" ontouchstart="jumpToOrder('{$orderId}')" /></td>
		    </tr>
		    </table>

			<table border="0" class="invoiceItems">

HTML;

   	// Iterate over invoice items
	$orderContentsQuery = lavu_query("select * from `order_contents` where `order_id` = '{$orderId}' order by id");
	$orderContentsQueryNumRows = mysqli_num_rows($orderContentsQuery);

	if ($orderContentsQueryNumRows > 0) {
		$html .= <<<HTML

			<tr class="header"><td>Item</td><td>Price</td><td>Quantity</td><td>Subtotal</td><td>Tax %</td><td>Tax</td><td>Total</td></tr>

HTML;
	}

	while ($row = mysqli_fetch_assoc($orderContentsQuery)) {

		$subtotal = number_format( $row['subtotal'], 2 );
		$taxAmount = number_format( $row['tax_amount'], 2 );
		$totalWithTax = number_format( $row['total_with_tax'], 2 );

   	    $html .= <<<HTML

			<tr><td>{$row['item']}</td><td>{$row['price']}</td><td>{$row['quantity']}</td><td>{$subtotal}</td><td>{$row['tax_rate1']}</td><td>{$taxAmount}</td><td>{$totalWithTax}</td></tr>

HTML;
	}

   	    $html .= <<<HTML

			</tr>
			</table>

			</div>

HTML;
    	return $html;
    }

    public function htmlForViewingInvoiceInline($orderId, $iconElementId) {
    	$html = '';

    	$invoice = $this->billingUtil->getInvoice( $orderId );
 		$invoiceDate = date("n/j/Y", strtotime( $invoice['opened'] ) );
    	$invoiceTotal = '$'. number_format( $invoice['total'], 2 );
    	$invoiceDueAmount = '$'. number_format( $this->billingUtil->getUnpaidAmountOnInvoice( $invoice ), 2 );
    	$cartIconDisplayCss = ( !empty($invoice['closed']) ) ? 'none' : 'block';

   	    $html .= <<<HTML

   	    	<div class="invoice hidden" id="invoice-{$iconElementId}">

			<div id="closeButton-{$iconElementId}" class="closeButton">X</div>
			<script>new FastButton( document.getElementById("closeButton-{$iconElementId}"), function() { hideshowInvoice('invoice-{$iconElementId}') } );</script>

   	    	<h3>Invoice #{$orderId}</h3>

	    	<table border="0" cellpadding="10" class="summaryBlock">
	    	<tr>
		    	<td><em>Date:</em> {$invoiceDate}</td>
		    	<td><em>Total</em> {$invoiceTotal}</td>
		    	<td><em>Due</em> {$invoiceDueAmount}</td>
		    	<td style="display:{$cartIconDisplayCss}"><img id="cart_icon" height="35" src="images/cart_icon.png" border="0" ontouchstart="jumpToOrder('{$orderId}')" /></td>
		    </tr>
		    </table>

			<table border="0" class="invoiceItems">

HTML;

   	// Iterate over invoice items
	$orderContentsQuery = lavu_query("select * from `order_contents` where `order_id` = '{$orderId}' order by id");
	$orderContentsQueryNumRows = mysqli_num_rows($orderContentsQuery);

	if ($orderContentsQueryNumRows > 0) {
		$html .= <<<HTML

			<tr class="header"><td>Item</td><td>Price</td><td>Quantity</td><td>Subtotal</td><td>Tax %</td><td>Tax</td><td>Total</td></tr>

HTML;
	}

	while ($row = mysqli_fetch_assoc($orderContentsQuery)) {

		$subtotal = number_format( $row['subtotal'], 2 );
		$taxAmount = number_format( $row['tax_amount'], 2 );
		$totalWithTax = number_format( $row['total_with_tax'], 2 );

   	    $html .= <<<HTML

			<tr><td>{$row['item']}</td><td>{$row['price']}</td><td>{$row['quantity']}</td><td>{$subtotal}</td><td>{$row['tax_rate1']}</td><td>{$taxAmount}</td><td>{$totalWithTax}</td></tr>

HTML;
	}

   	    $html .= <<<HTML

			</tr>
			</table>

			</div>

HTML;
    	return $html;
    }

    public function htmlForRecentMemberSignins() {
    	$sectionHtml = "";

		$signins_query = lavu_query("select * from `med_action_log` where `order_id` in('customer_signin', 'member_signin', 'fitness_signin') and `restaurant_time` >= date_sub(current_timestamp, interval 1 day) order by `restaurant_time` desc limit 50");

		$numUsersFound = mysqli_num_rows($signins_query);
		if ($numUsersFound == 0) {
			return <<<HTML
	<h1>(None in the last 24 hours)</h1>
HTML;
		}

		$i = 0;
		while($signins_row = mysqli_fetch_assoc($signins_query)) {

			$user_query = lavu_query("select * from `med_customers` where `_deleted`='0' and `id`='[id]'", array("id" => $signins_row["customer_id"]));
			if (mysqli_num_rows($user_query)) {
				$user_row = mysqli_fetch_assoc($user_query);

				$id = isset($user_row['id']) ? $user_row['id'] : "";
				$name = $user_row['f_name'] .' '. $user_row['l_name'];  // assuming x_name fields contain values since they're required fields
				$pic = isset($user_row['profile_picture']) ? str_replace('SIZE/', '', $user_row['profile_picture']) : "";  // TO DO : default picture here
				$rfid = isset($user_row['rfid']) ? $user_row['rfid'] : "";  // TO DO : default rfid (?)
				$time = $datetime = date('g:i a', strtotime($signins_row['restaurant_time']));
				$unixtimestamp = ($pic != '') ? '?' . time() : '';  // Add this on the jpg's "querystring" to generate unique requests to prevent caching
				$elementId = 'memberpic-'. $id .'_'. $i++;

   	    		$sectionHtml .= <<<HTML
	<figure>
		<img id="{$elementId}" src="{$pic}{$unixtimestamp}" width="81" height="81" border="0" />
		<figcaption>{$name} <em>{$time}</em></figcaption>
	</figure>
	<script>new FastButton( document.getElementById("{$elementId}"), function() { loadMemberProfile("{$id}") } );</script>

HTML;
			}
		}

    	return $sectionHtml;
    }

    public function htmlForRecentMemberSignups()
    {
    	$html = "";

		$signupsQueryResults = lavu_query("select b.id as billingProfileId, c.id as customerId, f_name, l_name, rfid, profile_picture, b.createtime as signupTime from med_billing b left join med_customers c on b.customer_id = c.id where c.id is not null and c._deleted = '0' order by b.id desc limit 50");
		$signupsQueryNumRows = mysqli_num_rows($signupsQueryResults);

		if ($signupsQueryNumRows == 0) {
			$html = <<<HTML

	<h1>(None)</h1> 

HTML;
		}
		else {

			$i = 0;
			while ($row = mysqli_fetch_assoc($signupsQueryResults)) {
				$id = isset($row['customerId']) ? $row['customerId'] : '';
				$name = $row['f_name'] .' '. $row['l_name'];  // assuming x_name fields contain values since they're required fields
				$pic = (empty($row['profile_picture'])) ? 'http://'. $_SERVER['HTTP_HOST'] .'/components/fitness/images/default_profile_pic/default_profile_pic.png' : str_replace('SIZE/', '', $row['profile_picture']);
				$rfid = isset($row['rfid']) ? $row['rfid'] : "";  // TO DO : default rfid (?)
				$time = $datetime = date('n/j/y g:i a', strtotime($row['signupTime']));
				$unixtimestamp = ($pic != '') ? '?' . time() : '';  // Add this on the jpg's "querystring" to generate unique requests to prevent caching
				$elementId = 'memberpic-'. $id .'_'. $i++;

		    	$html .= <<<HTML

		<figure>
			<img id="{$elementId}" src="{$pic}{$unixtimestamp}" width="81" height="81" border="0"/>
			<figcaption>{$name} <em>{$time}</em></figcaption>
		</figure>
		<script>new FastButton( document.getElementById("{$elementId}"), function() { loadMemberProfile("{$id}") } );</script>

HTML;
			}

		}

    	return $html;
    }

    public function htmlForMembershipExpirationDate()
    {
    	$html = '';

		$MembershipExpirationQueryResults = lavu_query("select b.id as billingProfileId, c.id as customerId, f_name, l_name, rfid, profile_picture, b.createtime as signupTime, end_date, if(current_date > end_date, 'Expired', '') as expired from med_billing b left join med_customers c on b.customer_id = c.id where c.id is not null and c._deleted = '0' and end_date != '' and current_date > date_sub( end_date, interval 30 day ) order by expired, end_date, b.id");
		$MembershipExpirationQueryNumRows = mysqli_num_rows($MembershipExpirationQueryResults);

		if ($MembershipExpirationQueryNumRows == 0) {
			$html = <<<HTML

	<h1>(None)</h1> 

HTML;
		}
		else {

			$i = 0;
			while ($row = mysqli_fetch_assoc($MembershipExpirationQueryResults)) {
				$id = isset($row['customerId']) ? $row['customerId'] : '';
				$name = $row['f_name'] .' '. $row['l_name'];  // assuming x_name fields contain values since they're required fields
				$pic = (empty($row['profile_picture'])) ? 'http://'. $_SERVER['HTTP_HOST'] .'/components/fitness/images/default_profile_pic/default_profile_pic.png' : str_replace('SIZE/', '', $row['profile_picture']);
				$rfid = isset($row['rfid']) ? $row['rfid'] : "";  // TO DO : default rfid (?)
				$time = $datetime = date('n/j/y', strtotime($row['end_date']));
				$unixtimestamp = ($pic != '') ? '?' . time() : '';  // Add this on the jpg's "querystring" to generate unique requests to prevent caching
				$elementId = 'memberpic-'. $id .'-'. $i++;
				$expired = empty($row['expired']) ? '' : $row['expired'];

		    	$html .= <<<HTML

		<figure>
			<img id="{$elementId}" src="{$pic}{$unixtimestamp}" width="81" height="81" border="0" />
			<figcaption>{$name} <em>{$time} {$expired}</em></figcaption>
		</figure>
		<script>new FastButton( document.getElementById("{$elementId}"), function() { loadMemberProfile('{$id}'); } );</script>
HTML;
			}

		}

    	return $html;
    }

    public function htmlForCreditCardExpirationDate()
    {
    	$html = '';

		$queryResults = lavu_query("select b.id as billingProfileId, c.id as customerId, f_name, l_name, rfid, profile_picture, str_to_date( concat( cc_exp_date, '/01'), '%m/%y/%d' ) as ccExpirationDate from med_billing b left join med_customers c on b.customer_id = c.id where c.id is not null and c._deleted = '0' and current_date > date_sub( str_to_date( concat( cc_exp_date, '/01'), '%m/%y/%d' ), interval 90 day) order by end_date desc");
		$queryNumRows = mysqli_num_rows($queryResults);

		if ($queryNumRows == 0) {
			$html = <<<HTML

	<h1>(None)</h1>

HTML;
		}
		else {

			$i = 0;
			while ($row = mysqli_fetch_assoc($queryResults)) {
				$id = isset($row['customerId']) ? $row['customerId'] : '';
				$name = $row['f_name'] .' '. $row['l_name'];  // assuming x_name fields contain values since they're required fields
				$pic = (empty($row['profile_picture'])) ? 'http://'. $_SERVER['HTTP_HOST'] .'/components/fitness/images/default_profile_pic/default_profile_pic.png' : str_replace('SIZE/', '', $row['profile_picture']);
				$rfid = isset($row['rfid']) ? $row['rfid'] : "";  // TO DO : default rfid (?)
				$time = $datetime = date('n/y', strtotime($row['ccExpirationDate']));
				$unixtimestamp = ($pic != '') ? '?' . time() : '';  // Add this on the jpg's "querystring" to generate unique requests to prevent caching
				$elementId = 'memberpic-'. $id .'-'. $i++;

		    	$html .= <<<HTML

		<figure>
			<img id="{$elementId}" src="{$pic}{$unixtimestamp}" width="81" height="81" border="0" />
			<figcaption>{$name} <em>{$time}</em></figcaption>
		</figure>
		<script>new FastButton( document.getElementById("{$elementId}"), function() { loadMemberProfile('{$id}'); } );</script>
HTML;
			}

		}

    	return $html;
    }

    public function htmlForMembershipOutstandingBalances()
    {
    	$html = '';

    	// Go through each unpaid invoice and find its member.  One member might have multiple unpaid invoices so we
    	// accumulate all of their unpaid invoices in the $usersWithUnpaidInvoices hash with their customerId as the key.

    	$usersWithUnpaidInvoices = array();
    	$unpaidInvoices = $this->billingUtil->getAllUnpaidInvoicesId();
    	foreach ( $unpaidInvoices as $invoice ) {
    		//error_log( "order_id={$invoice['order_id']} originalOrderId={$invoice['originalOrderId']} unpaidAmount={$invoice['unpaidAmount']}" );
    		// Get the user and billing profile using the original order ID
			$querySql = "select b.id as billingProfileId, c.id as customerId, f_name, l_name, rfid, profile_picture, b.createtime as signupTime from med_billing b left join med_customers c on b.customer_id = c.id where c.id is not null and c._deleted = '0' and b.order_id = '{$invoice['originalOrderId']}' order by b.id desc";
			$queryResults = lavu_query($querySql);
			$queryNumRows = mysqli_num_rows($queryResults);
			if ( $queryNumRows > 0) {
				while ( $row = mysqli_fetch_assoc($queryResults) ) {
					$customerId = $row['customerId'];
					if ( !isset( $usersWithUnpaidInvoices[$customerId] ) ) {
						$usersWithUnpaidInvoices[ $customerId ] = array(
							'unpaidAmount' => floatval( $invoice['unpaidAmount'] ),
							'id' => $customerId,
							'name' => $row['f_name'] .' '. $row['l_name'],  // assuming x_name fields contain values since they're required fields
							'pic' => (empty($row['profile_picture'])) ? 'http://'. $_SERVER['HTTP_HOST'] .'/components/fitness/images/default_profile_pic/default_profile_pic.png' : str_replace('SIZE/', '', $row['profile_picture']),
							'rfid' => isset($row['rfid']) ? $row['rfid'] : ""  // TO DO : default rfid (?)
						);
					}
					else {
						$usersWithUnpaidInvoices[$customerId]['unpaidAmount'] += floatval( $invoice['unpaidAmount'] );
					}
				}
			}
    	}

    	$i = 0;
    	foreach ( $usersWithUnpaidInvoices as $user ) {
    			$id = $user['id'];
    			$pic = $user['pic'];
    			$name = $user['name'];
				$elementId = 'memberpic-'. $id .'-'. $i++;
    			$outstandingBalance = money_format( '%.2n', $user['unpaidAmount'] );

		    	$html .= <<<HTML

		<figure>
			<img id="{$elementId}" src="{$pic}" width="81" height="81" border="0" />
			<figcaption>{$name} <em>{$outstandingBalance}</em></figcaption>
		</figure>
		<script>new FastButton( document.getElementById("{$elementId}"), function() { loadMemberProfile('{$id}'); } );</script>

HTML;

    	}

    	if ( count( $usersWithUnpaidInvoices ) == 0 ) {
			$html = <<<HTML

	<h1>(None)</h1> 

HTML;
		}

		return $html;
    }

	function deleteProfile( $customerId )
	{
    	// Blank out the link from the users table to this med_customer ID, if there happens to be one.
		$unlinkSuccess = lavu_query("UPDATE `users` SET `customer_id` = '' WHERE `customer_id` = '[customer_id]'", array('customer_id' => $customerId));
		if ($unlinkSuccess === FALSE) error_log( "failed to unassociate users with customer id {$customerId} during profile deletion: ". lavu_dberror() );

    	// Delete the member profile by setting the _delete flag rather than actually deleting the row in the database.
		$success = lavu_query("UPDATE `med_customers` SET `_deleted` = '1', `rfid` = '' WHERE `_deleted` = '0' AND `id` = '[customer_id]' LIMIT 1", array('customer_id' => $customerId));
		if ($success === FALSE) die( "failed to delete customer id {$customerId}: ". lavu_dberror() );

		return $success;
	}

    public function linkMemberToUser( $customerId, $serverId )
    {
    	// Blank out any other users row that already have that same customer_id.
		$success = lavu_query("update `users` set customer_id = '' where customer_id = [customer_id]", array('customer_id' => $customerId));
		if ($success === FALSE) die( "failed to unassociate users with customer id {$customerId}" );

    	// Associate the specified server with the specified med_customers account.
		$success = lavu_query("update `users` set customer_id = [customer_id] where id = [server_id]", array('customer_id' => $customerId, 'server_id' => $serverId));
		if ($success === FALSE) die( "failed to associate member {$customerId} with user {$serverId}" );
	}

	public function fetchLastInsertedUserId() {
		$newestId = null;
		$query_results = lavu_query("select max(id) from `med_customers`");  // TO DO : see if there is a one-arg version of this
		if (mysqli_num_rows($query_results)) {
		    // Create js Member object using data from database.
			$row = mysqli_fetch_row($query_results);
			$newestId = $row[0];
		}
		return $newestId;
	}

	public function uploadUserPicture($options) {
        
		// Check options input array.
		$rotate = (!empty($options['rotate']) && $options['rotate'] == 'rotate(180deg)') ? '180' : '';
		if($rotate!="") $rotateSwitch = " -rotate " . $rotate;
		else $rotateSwitch = "";
        
		// TO DO : check for established convention for getting web root dirs
		$imageFileDirBase = '/home/poslavu/public_html/admin/images/'. $this->app['dn'];
		$sectionDir = "members";
		$imageSizeDir = "full";

		$pictureDirs = array($imageFileDirBase, $sectionDir, $imageSizeDir);
		$picturePathSoFar = "";
		foreach ($pictureDirs as $currentDir) {
			$picturePathSoFar .= $currentDir . DIRECTORY_SEPARATOR;
			if ( !file_exists($picturePathSoFar) ) {
				if ( !mkdir($picturePathSoFar, 0775, true) ) {
					die("Couldn't create directory: $picturePathSoFar");
				}
			}
		}

		$imageFileName = $this->customer_id ."_profile_pic.jpg";
		array_push($pictureDirs, $imageFileName);

		$imageFilePath = implode(DIRECTORY_SEPARATOR, $pictureDirs);
		//$decodedImageData = base64_decode($_REQUEST['newMemberPicture']);  /** before ajax pic upload **/
		$picture_content = str_replace(" ", "+", $_REQUEST['newMemberPicture']);
		$decodedImageData = base64_decode($picture_content);
		$imageData = imagecreatefromstring($decodedImageData);
		$savedImage = imagejpeg($imageData, $imageFilePath);
		imagedestroy($imageData);

		//if ( !empty($rotate) ) {
		//	exec("convert '{$imageFilePath}' -rotate {$rotate} '{$imageFilePath}'");
		//}


		// Update picture in database.

		$profilePictureUrl = str_replace('/home/poslavu/public_html/admin/', 'http://'. $_SERVER['HTTP_HOST'] .'/', $imageFilePath);
		$profilePictureUrl = str_replace($imageSizeDir, 'SIZE', $profilePictureUrl);
		$success = lavu_query("update `med_customers` set `profile_picture` = '[profile_picture]' where `id` = '[id]'", array('profile_picture' => $profilePictureUrl, 'id' => $this->customer_id));
		if (!$success)
		{
			die( "failed to update profile picture for customer id {$this->customer_id}" );
		}


		// Create resized picture for "main" version.

        $mainImageSizeDir = "main";
        $mainImageFilePath = str_replace($imageSizeDir . DIRECTORY_SEPARATOR, $mainImageSizeDir . DIRECTORY_SEPARATOR, $imageFilePath);
        $mainImageWidth = "163";
        $mainImageHeight = "163";

		$mainImageDir = dirname( $mainImageFilePath );
		if ( !file_exists( $mainImageDir ) ) {
			if ( !mkdir($mainImageDir, 0775, true) ) {
				die("Couldn't create directory: $mainImageFilePath");
			}
		}

		//$convertcmd = "convert '$imageFilePath' $rotateSwitch -thumbnail '".$mainImageWidth."x".$mainImageHeight.">' '$mainImageFilePath'";
		$convertcmd = 'convert '.lavuShellEscapeArg($imageFilePath).' '.lavuShellEscapeArg($rotateSwitch).' -thumbnail '.lavuShellEscapeArg($mainImageWidth.'x'.$mainImageHeight.'>').' '.lavuShellEscapeArg($mainImageFilePath);
		//exec($convertcmd);
		lavuExec($convertcmd);
		chmod($mainImageFilePath,0775);

//error_log("---------Value for Rotate: $rotate\n");
//error_log("convert '{$mainImageFilePath}' -rotate {$rotate} '{$mainImageFilePath}'\n");
//		if ( !empty($rotate) ) {
//			exec("convert '{$mainImageFilePath}' -rotate {$rotate} '{$mainImageFilePath}'");
//		}



        // Create resized picture for "thumbnail" version.

        $thumbnailImageSizeDir = "";
        $thumbnailFilePath = str_replace($imageSizeDir . DIRECTORY_SEPARATOR, $thumbnailImageSizeDir . DIRECTORY_SEPARATOR, $imageFilePath);
        $thumbnailImageWidth = "81";
        $thumbnailImageHeight = "81";

        // The directory the thumbnails go in already exists so no need to check.

		//$convertcmd = "convert '$imageFilePath' $rotateSwitch -thumbnail '".$thumbnailImageWidth."x".$thumbnailImageHeight.">' '$thumbnailFilePath'";
		//exec($convertcmd);
		$convertcmd = 'convert '.lavuShellEscapeArg($imageFilePath).' '.lavuShellEscapeArg($rotateSwitch).' -thumbnail '.lavuShellEscapeArg($thumbnailImageWidth.'x'.$thumbnailImageHeight.'>').' '.lavuShellEscapeArg($thumbnailFilePath);
		lavuExec($convertcmd);
		chmod($thumbnailFilePath,0775);

		//if ( !empty($rotate) ) {
		//	exec("convert '{$thumbnailFilePath}' -rotate {$rotate} '{$thumbnailFilePath}'");
		//}


	}

}

/* Site Logic */

$site = new MembersSigninsSite();

$site->css['arbCancelButton'] = ($site->billingProfile['status'] == 'active') ? 'displayed' : 'hidden';
$site->css['arbReactivateButton'] = ($site->billingProfile['status'] == 'cancelled') ? 'displayed' : 'hidden';
$site->css['arbButtonAction'] = ($site->billingProfile['status'] == 'active') ? 'cancel' : 'reactivate';
$site->html['newInvoiceDayOfMonth'] = ($site->billingProfile['bill_event_offset'] == "1") ? "15" : "1";

//error_log( "DEBUG: page={$site->page} customer_id={$site->customer_id} med_posted={$site->med_posted} SESSION[customer_id]={$_SESSION['customer_id']} waiver_signed={$site->waiver_signed} expired={$site->userExpired} saved_position_id=". $_REQUEST['saved_position_id'] );  //debug

// Routing Logic

if ( $site->page == 'memberbilling' ) {
	$site->html['ReloadMemberSignins'] = $site->htmlForMemberBilling();
}
else if ( $site->page == 'memberhistory' ) {
	$site->html['ReloadMemberSignins'] = $site->htmlForMemberHistory();
}
else if ( $site->page == 'memberprofile' ) {
	$site->html['ReloadMemberSignins'] = $site->htmlForMemberProfile();
}
else if ( $site->page == 'edituser' || $site->page == 'saveuser' ) {
	$site->html["ReloadMemberSignins"] = $site->htmlForMemberProfile();

	$site->insertMedActionLog( $lookupIdToUse, 'Member Info Edited', 'customer_edited' );
}
else if ( $site->page == 'newuser' ) {
	$_SESSION['customer_id'] = '';
	$site->html["ReloadMemberSignins"] = $site->htmlForNewMember();
}
else if ( $site->page == 'viewnewuser' ) {
	$site->html["ReloadMemberSignins"] = $site->htmlForMemberProfile();

	$site->insertMedActionLog( $site->customer_id, 'Member Created', 'customer_created' );
}
else if ( $site->page == 'invoice' ) {
	$site->html["ReloadMemberSignins"] = $site->htmlForViewingInvoice($site->invoice_id);
}
else if ( $site->page == 'recentsignups' ) {
	$site->html["ReloadMemberSignins"] = $site->htmlForRecentMemberSignups();
}
else if ( $site->page == 'expirations' ) {
	$site->html["ReloadMemberSignins"] = $site->htmlForMembershipExpirationDate($site->invoice_id);
}
else if ( $site->page == 'ccexpirations' ) {
	$site->html["ReloadMemberSignins"] = $site->htmlForCreditCardExpirationDate($site->invoice_id);
}
else if ( $site->page == 'outstandingbalances' ) {
	$site->html["ReloadMemberSignins"] = $site->htmlForMembershipOutstandingBalances();
}
else if ( $site->page == 'cancel' ) {
	// Cancel profile and then re-pull billing profile to get the updated status.
	$site->billingUtil->cancelProfile( $_REQUEST['billingProfileId'] );
	$site->billingProfile = $site->billingUtil->getBillingProfileForCustomerId( $site->customer_id );

	$site->css['arbCancelButton'] = 'hidden';
	$site->css['arbReactivateButton'] = 'displayed';
	$site->css['arbButtonAction'] = 'reactivate';

	$site->html["ReloadMemberSignins"] = $site->htmlForMemberBilling();

	$site->insertMedActionLog($site->customer_id, 'Billing Profile Cancelled', 'arb_cancelled');
}
else if ( $site->page == 'reactivate' ) {
	// Reinstate profile and then re-pull billing profile to get the updated status.
	$site->billingUtil->reactivateProfile( $_REQUEST['billingProfileId'] );
	$site->billingProfile = $site->billingUtil->getBillingProfileForCustomerId( $site->customer_id );

	$site->css['arbCancelButton'] = 'displayed';
	$site->css['arbReactivateButton'] = 'hidden';
	$site->css['arbButtonAction'] = 'cancel';

	$site->html["ReloadMemberSignins"] = $site->htmlForMemberBilling();

	$site->insertMedActionLog($site->customer_id, 'Billing Profile Reactivated', 'arb_reactivated');
}
else if ( $site->page == 'invoicedayofmonth' ) {
	// Reinstate profile and then re-pull billing profile to get the updated status.
	$site->billingUtil->updateProfile( $_REQUEST['billingProfileId'], array( 'bill_event_offset' => $_REQUEST['newInvoiceDayOfMonth'] ) );

	$site->html['newInvoiceDayOfMonth'] = ($_REQUEST['newInvoiceDayOfMonth'] == "1") ? "15" : "1";

	$site->html["ReloadMemberSignins"] = $site->htmlForMemberBilling();

	$site->insertMedActionLog($site->customer_id, 'Billing Profile Invoice Day of Month Changed', 'arb_changedinvoicedayofmonth');
}
else if ( $site->page == 'linkuser' ) {
	$site->html["ReloadMemberSignins"] = $site->htmlForMemberProfile();

	$site->linkMemberToUser( $site->customer_id, $_REQUEST['server_id'] );

	$site->insertMedActionLog($site->customer_id, "Linked Employee #{$_REQUEST['server_id']} to Member #{$site->customer_id}", 'arb_linkedmembertouser');
}
else if ( $site->page == 'delete' ) {
	// Bug #2746 -- add support for deleting member profiles.

	//error_log( "DEBUG: In delete section with customer_id={$site->customer_id} billingProfile[status]={$site->billingProfile['status']}" );  //debug

	$msg = '';
	if ( isset($site->billingProfile['status']) && $site->billingProfile['status'] == 'active' ) {
		$msg = "Can't delete Member Profile because it has an active Billing Profile.  Cancel all active Billing Profiles first.";
	}
	else if ( $site->deleteProfile( $site->customer_id ) ) {
		$msg = "Member Profile #{$site->customer_id} for ". htmlspecialchars($_REQUEST['FirstName'] ." ". $_REQUEST['LastName']) ." was deleted.";
	}

	$site->html["ReloadMemberSignins"] = $site->htmlForDeletedMemberProfile( $msg );
	
	$site->insertMedActionLog($site->customer_id, "Deleted Member #{$site->customer_id}", 'customer_deleted');
}
else if ( $site->page == 'uploadpic' ) {
	ini_set('memory_limit', '64M');
	$rotate = (isset($_REQUEST['rotatepic'])) ? $_REQUEST['rotatepic'] : '';
	$site->uploadUserPicture( array( 'rotate' => $rotate ) );
	$site->insertMedActionLog($site->customer_id, 'New Member Profile Pic Uploaded', 'customer_profile_pic');

/* We shouldn't have to do this stuff anymore since page=uploadpic is loaded by ajax, so this branch never renders the page.
	// Need to re-fetch user data to get the latest profile picture.
	$site->fetchMemberInfo(array("id" => $site->customer_id));

	$site->title = "Member Info";
	$site->memberInfo["SignInTime"] = '';
	$site->css["ProfilePicture"] = "displayed";
	$site->html['memberInfoSection'] = $site->htmlForMemberProfile();

	// Reload the Recent Member Sign-ins component.
	///$site->js["run"] .= $site->jsForReloadingSignins();
	$site->doCmds['reload_signins'] = 1;
*/
}
else {
	// Load the Recent Member Sign-ins component.
	$site->html["ReloadMemberSignins"] = $site->htmlForRecentMemberSignins();
}


/* Site html */

echo <<<HTML

<!DOCTYPE html>
<html>
<head>
<meta http-equiv="cache-control" content="no-cache" />
<style>
html, body {
    height: 100%;
    /*overflow-x: hidden;  /* uncommenting this makes scrolling choppy */
}body {
	-webkit-tap-highlight-color: rgba(0, 0, 0, 0);
	-webkit-user-select: none;
}

body, div, td {
	font-family: Verdana, Arial;
	font-size: 12px;
}
h1 {
	font-family: Verdana, Arial;
	font-weight: normal;
	font-size: 12px;
	text-align: center;
	color: #bbbbbb;
}
img {
	-webkit-border-radius: 10px;  /* rounded corners */
}
em {  /* for the sign-in timestamps */
	font-style: normal;  /* un-italicize it */
	font-size: .8em;
	color: gray;
	display: block;  /* make it line break like a <br> tag - it also kinda snuggles up under the figcaption better */
}
figure {
	display: inline-block;
	white-space: nowrap;
	width: 81px;
	-webkit-margin-before: 0px;
	-webkit-margin-after: 20px;
	-webkit-margin-start: 0px;
	-webkit-margin-end: 36px;
}
figure:nth-of-type(4n) {  /* fun css that removes the right margin from every 4th figure element */
	-webkit-margin-end: 0px;
}
figcaption {
	/*width: 81px;*/
	word-wrap: break-word;
}
h3 {
	margin: 0px;
	margin-bottom: 10px;
	color: #aecd37;
	display: inline-block;
}
em {
	font-weight: normal;
	color: gray;
}
.hidden {
	display: none;
}
.displayed {
	display: block;
}
.profile {
	overflow: none;
}
.invoice {
	/*width: 300px;*/
	position: relative;
	padding: 10px;
	-webkit-border-radius: 10px;  /* rounded corners */
	background-color: white;
	/*overflow-x: hidden;  /* prevents the member profile from scrolling left and right */
}
.invoiceItems td {
	font-size: 11px;
	text-align: center;
	padding: 3px;
}
.invoiceItems tr.header td {
	font-weight: bold;
}
.invoice h2,
.invoice h3 {
	text-align: center;
}
.invoice .closeButton {
	position: absolute;
	top: 0px;
	right: -5px;
	/*right: -20px;  helps position the X nicely in the corner */
	font-size: 20pt;
	-webkit-appearance: none;  /* crucial for removing mobile safari button styles */
	border: none;
	padding: 10px;     /* make the button easier to tap by expanding its dimensions */
	padding-top: 0px;
	color: #ccc;
}
.summaryBlock {
	text-align:center;
}
.gray {
	color: gray;
}
.arbButton {
	font-size: 12pt;
	width: 175px;
	-webkit-appearance: none;  /* crucial for removing mobile safari button styles */
	/*border: none;*/
	color: red;
}
.memberFieldLabel {
	text-align: right;
	color: #ccc;
}
.tabs {
	text-align: center;
	list-style: none;
	margin: 0px;
	margin-top: 5px;
	margin-left: 85px;
	padding: 0px;
	line-height: 22px;
}
.tabs li {
	position: relative;
	display: inline-block;
	margin: 0px 0px;
	padding: 0px 10px;
	border: 1px solid #aaa;
	background: #ececec;

	background-color: white;
	border-color: #ddd;
	color: gray;
	/*background-color: white;
	border-color: #aecd37;
	color: #aaa;*/
	-webkit-border-radius: 10px;  /* rounded corners */
	-webkit-appearance: none;  /* crucial for removing mobile safari button styles */

}
.tabs li.selected {
	background-color: #aecd37;
	border-color: #ddd;
	color: white;
}
.tabs :after {
	position: absolute;
	content: "";
	width: 100%;
	bottom: 0px;
	left: 0px;
	border: bottom: 1px solid #aaa;
	z-index: 1;
}
#memberProfileTabs {
	position: absolute;
	margin: 0px;
	padding: 0px;
	top: 0px;
	left: 100px;
}
#recentSigninsSection {
	width: 100%;
	overflow-x: hidden;
}
#arbCancelButton {
	color: red;
}
#arbReactivateButton {
	color: #aecd37;
}
#arbChangeInvoiceDayOfMonthButton {
	font-size: 12pt;
	color: #aecd37;
	width: 175px;
	white-space: normal;
}
#profileInfo td {
	font-size: 10px;
}
.profileLine {
	font-size: 10px;
}
#memberDetails {
	text-align: right;
	display: inline-block;
	vertical-align: top;
	padding: 10px;
	font-size: 9px;
}
#memberDetails dt {
	text-align: left;
	color: gray;
}
#memberDetails {
	text-align: right;
	display: inline-block;
	vertical-align: top;
	padding: 10px;
	font-size: 9px;
}
#memberDetails dt {
	text-align: left;
	color: gray;
}
#memberProfile {
	position: relative;
}
#memberProfile div {
	/*font-size: 10px;*/
}
.memberProfileLabel {
	position: absolute;
	width: 100px;
	color: gray;
	margin-top: 0px;
	font-size: 9px;
}
.memberProfileStat {
	position: absolute;
	width: 110px;
	margin-top: 0px;
	font-size: 10px;
}
#memberStats {
	position: relative;

}
#profilePictureToolbar {
	position: relative;
}
#memberProfileToolbar {
	position: relative;
	margin-top: 10px;
}
#memberInfoFields table {
	margin: 0px auto;  /* css way to center tables */
}
#savePictureButton,
#flipPictureButton,
#manualSignInButton {
	width: 80%;
	height: 50px;
	margin-bottom: 12px;
	background-color: white;
	border-color: #ddd;
	color: #aecd37;
	/*background-color: white;
	border-color: #aecd37;
	color: #aaa;*/
	font-size: 12pt;
	-webkit-border-radius: 10px;  /* rounded corners */
	-webkit-appearance: none;  /* crucial for removing mobile safari button styles */
	text-transform: uppercase;
}
#savePictureButton {
	display: inline-block;
	width: auto;
	margin-top: 5px;
	margin-bottom: 0px;
	margin-left: 0px;
}
#flipPictureButton {
	display: inline-block;
	width: auto;
	margin-top: 5px;
	margin-bottom: 0px;
	margin-left: 10px;
}
#manualSignInButton {
	position: absolute;
	width: auto;
	height: 35px;  /* to match button icon images */
	left: 273px;
}
#arbLinkMemberToUser {
	background-color: white;
	border-color: #ddd;
	color: #aecd37;
	width: 90px;
	height: 20px;
	font-size: 13px;
}
#deleteProfile {
	background-color: white;
	border-color: #ddd;
	color: #aecd37;
	height: 20px;
	font-size: 13px;

	position: relative;
	width: auto;
	top:-20px;
	left:-90px;
	height: 20px;
}
#shoppingCartButton {
	background-color: white;
	border-color: #ddd;
	color: #aecd37;
	width: 50px;
	height: 20px;
	font-size: 13px;
}

</style>
<script src="js/google.fastbutton.js"></script>
</head>

<body>

<div id="recentSigninsSection">

	{$site->html['ReloadMemberSignins']}

</div>

<script>

// Comment out refresh() during development to allow for refreshing page by simplying clicking on the Members tab.
// Note: the refresh() function also seems to get called when the PIN lock-out screen happens.

function refresh() {
	return 1;
}

function run_com_command(comname,vars) {
	window.location = "_DO:cmd=load_url&c_name=" + comname + "&f_name=fitness/" + comname + ".php?" + vars;
}

function newSignIn(badgeId) {
    //run_com_command("members", "page=signinuser&rfid="+ encodeURIComponent(badgeId) +"{$site->appVarsQuerystring()}");
    run_com_command("members", "page=signinuser&rfid="+ encodeURIComponent(badgeId));
}

function newManualSignIn(customerId) {
    //run_com_command("members", "page=signinuser&rfid="+ encodeURIComponent(badgeId) +"{$site->appVarsQuerystring()}");
    run_com_command("members", "page=manualsigninuser&customer_id="+ encodeURIComponent(customerId));
}

function loadMemberProfile(customerId) {
	//window.location = "_DO:cmd=load_url&c_name=members&f_name=fitness/members.php?page=viewuser&customer_id="+ encodeURIComponent(id);
	//window.location = '_DO:cmd=send_js&c_name=members_wrapper&js=loadMemberProfile("'+ customerId +'");';
	setTimeout( function() { window.location = '_DO:cmd=send_js&c_name=members_wrapper&js=loadMemberProfile("'+ customerId +'");' }, 310 );
	return 1;
}

function loadMemberBilling(customerId) {
	if (customerId != "") {
	    window.location = '_DO:cmd=send_js&c_name=members_wrapper&js=loadMemberBilling("'+ encodeURIComponent(customerId) +'");';
	    //window.location = "members_signins.php?page=billing&customer_id=" + encodeURIComponent(customerId);
	}
}

function loadMemberHistory(customerId) {
	if (customerId != "") {
	    window.location = '_DO:cmd=send_js&c_name=members_wrapper&js=loadMemberHistory("'+ encodeURIComponent(customerId) +'");';
	    //window.location = "members_signins.php?page=billing&customer_id=" + encodeURIComponent(customerId);
	}
}

function loadMemberEditFields(badgeId, currentPage) {
	if (currentPage == 'edituser') {
		window.location = "members_signins.php?page=memberprofile&customer_id="+ encodeURIComponent("{$site->customer_id}") +"{$site->appVarsQuerystring()}";
	} else {
		window.location = "members_signins.php?page=edituser&customer_id="+ encodeURIComponent("{$site->customer_id}") +"{$site->appVarsQuerystring()}";
	}
}

function loadRecentSignIns() {
	window.location = '_DO:cmd=send_js&c_name=members_wrapper&js=loadRecentMemberSignins();';
    //window.location = "members_signins.php";
}

function loadInvoiceInfo(customerId, orderId) {
	if (customerId != "" && orderId != "") {
	    //window.location = '_DO:cmd=send_js&c_name=members_wrapper&js=loadInvoiceInfo("'+ encodeURIComponent(customerId) +'", "'+ encodeURIComponent(orderId) +'");';
	    window.location = "members_signins.php?page=invoice&customer_id=" + encodeURIComponent(customerId) +"&invoice_id="+ encodeURIComponent(orderId) +'{$site->appVarsQuerystring()}';
	}
}

function loadLastLoadedMemberSignupsPage() {
	//window.location = '_DO:cmd=send_js&c_name=members_wrapper&js=reloadLastLoadedMemberSignupsPage();';
	setTimeout( function() { window.location = '_DO:cmd=send_js&c_name=members_wrapper&js=reloadLastLoadedMemberSignupsPage();' }, 350 );
}

function launchWaiver() {
	// Show the popup for signing the waiver, since no signature image has been detected.
	if ( "{$site->waiver_signed}" == "" ) {
		window.location = '_DO:cmd=create_overlay&f_name=medical/customer_history_overlay_wrapper.php;fitness/members_waiver.php&c_name=customer_history_overlay_wrapper;members_waiver&dims=250,100,574,549;266,158,542,480&scroll=0;1?customer_id={$site->customer_id}&refresh_comp=members_signins&title_start=Waiver: (name) (sign)';
	}
	else if ( "{$site->contract_signed}" == "" && "{$site->contract_supported}" != "" ) {
		window.location = '_DO:cmd=create_overlay&f_name=medical/customer_history_overlay_wrapper.php;fitness/members_contract.php&c_name=customer_history_overlay_wrapper;members_contract&dims=250,100,574,549;266,158,542,480&scroll=0;1?customer_id={$site->customer_id}&refresh_comp=members_signins&title_start=Contract: (name) (sign)';
	}
	// Show the popup for emailing the waiver, since the waiver has been signed.
	else {
		//window.location = '_DO:cmd=create_overlay&f_name=medical/customer_history_overlay_wrapper.php;fitness/members_emailwaiver.php&c_name=customer_history_overlay_wrapper;members_emailwaiver&dims=250,100,574,549;266,158,542,480&scroll=0;1?customer_id={$site->customer_id}&arb_id={$site->billingProfile['id']}&refresh_comp=members_signins&email={$site->memberInfo['Email']}&dn={$site->app['dn']}&loc_id={$site->app['loc_id']}&server_id={$site->app['server_id']}&title_start=Signed Waiver: (name)';
		window.location = '_DO:cmd=create_overlay&f_name=ers/floating_wrapper.php;fitness/members_emailwaiver.php&c_name=customer_history_overlay_wrapper;members_emailwaiver&dims=250,100,574,549;266,158,542,480&scroll=0;1?customer_id={$site->customer_id}&arb_id={$site->billingProfile['id']}&refresh_comp=members_signins&email={$site->memberInfo['Email']}&dn={$site->app['dn']}&loc_id={$site->app['loc_id']}&server_id={$site->app['server_id']}&contract_supported={$site->contract_supported}&contract_signed={$site->contract_signed}';
	}
}

function takePicture() {
	if ("{$site->customer_id}" != "") {
		window.location = "_DO:cmd=takePicture";
	}
}

function processPicture(pictureData) {
	// Show the new profile picture
	var memberPicture = document.getElementById("memberPicture");
	memberPicture.src = "data:image/jpeg;base64,"+ pictureData;

	var profilePictureToolbar = document.getElementById("profilePictureToolbar");
	profilePictureToolbar.className = "displayed";

	return 1;
}

/* Ajax Profile Picture Stuff -- BEGIN */

function flipProfilePicture() {
	var memberPicture = document.getElementById("memberPicture");
	memberPicture.style.webkitTransform = (memberPicture.style.webkitTransform == "rotate(180deg)") ? "" : "rotate(180deg)";
}

function saveProfilePicture() {
	var memberPicture = document.getElementById("memberPicture");
	make_ajax_call(window.location.href.split('?')[0], "page=uploadpic&dn={$site->app['dn']}&customer_id={$site->customer_id}&rotatepic="+ escape(memberPicture.style.webkitTransform) +"&newMemberPicture="+ escape( document.getElementById("memberPicture").src.substring(23) ), "profilePictureUploaded");
}

function profilePictureUploaded() {
	var profilePictureToolbar = document.getElementById("profilePictureToolbar");
	profilePictureToolbar.className = "hidden";

//		// Reload member_signins, in case profile picture appears there.
//		window.location = '_DO:cmd=send_js&c_name=members_wrapper&js=reloadLastLoadedMemberSignupsPage();';
}

function make_ajax_call(strURL,querystr,callback)
{
	var xmlHttpReq = false;
	var self = this;
	// Mozilla/Safari
	if (window.XMLHttpRequest)
	{
		self.xmlHttpReq = new XMLHttpRequest();
	}
	// IE
	else if (window.ActiveXObject)
	{
		self.xmlHttpReq = new ActiveXObject("Microsoft.XMLHTTP");
	}
	self.xmlHttpReq.open('POST', strURL, true);
	self.xmlHttpReq.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
	self.xmlHttpReq.onreadystatechange = function()
	{
		if (self.xmlHttpReq.readyState == 4)
		{
			eval(callback + "(parse_ajax_response(self.xmlHttpReq.responseText))");
			//process_ajax_callback(self.xmlHttpReq.responseText);
		}
	}
	self.xmlHttpReq.send(querystr);
}

aj_flexible_containers = new Array();
function ajax_register_flexible_container(container_id)
{
	aj_flexible_containers[container_id] = container_id;
}

function parse_ajax_response(str)
{
	var rsp = new Array();
	rsp['success'] = "0";
	var str_parts = str.split("&");
	for(var i=0; i<str_parts.length; i++)
	{
		var var_parts = str_parts[i].split("=");
		if(var_parts.length > 1)
			rsp[var_parts[0]] = var_parts[1];
	}
	return rsp;
}


/* Ajax Profile Picture Stuff -- END */

function jumpToOrder(orderId) {
	if (orderId == null) orderId = "new";
	window.location = "_DO:cmd=jump_to_order&order_id="+ encodeURIComponent(orderId) +"&auto_fill="+ encodeURIComponent("original_id=19|o|Member|o|{$site->memberInfo['FirstName']} {$site->memberInfo['LastName']}|o|{$site->customer_id}");
}

	function launchSetCardOverlay(id) {
		if ("{$site->customer_id}" != "") {
			window.location = "_DO:cmd=create_overlay&f_name=ers/floating_wrapper.php;fitness/members_card.php&c_name=floating_wrapper;members_card&dims=340,220,394,349;360,285,360,269&scroll=0;1?customer_id="+ id;
		}
	}

function hideshowInvoice(elementId) {
	var invoice = document.getElementById(elementId);
	if (invoice == null) return;
	invoice.style.display = (invoice.style.display != "block") ? "block" : "none";
}

</script>

</body>
</html>

HTML;

?>
