<?php 

require_once(dirname(__FILE__) . "/../comconnect.php");
//require_once(dirname(__FILE__) . "/../order_addons/customer_functions.php");
require_once('RecurringBilling.php' );

//error_log( "DEBUG: id={$_REQUEST['id']} customer_id={$_REQUEST['customer_id']} session={$_SESSION['customer_id']} mode={$_REQUEST['mode']}" );  /** debug **/

class MembersSite {
	// Site vars
	var $page;
	var $title;
	var $msg;

    // App vars posted to every component.
    var $app;

    // Location info hash
    var $location_info;

	// Page content vars
	var $html;
	var $css;
	var $js;

	// Fitness component-specific vars
	var $rfid;
	var $customer_id;

	// Customer component-specific vars
	var $med_posted;
	var $memberInfo;

	// Customer Billing
	var $billingUtil;
	var $billingProfile;

	public function __construct() {
		setlocale(LC_MONETARY, "en_US");

		// Site vars
		$this->page = (isset($_REQUEST["page"])) ? $_REQUEST["page"] : "";

        // Standard vars posted to every component from the app.
        $this->app["comp_name"] = (isset($_REQUEST["comp_name"])) ? $_REQUEST["comp_name"] : "";
        $this->app["server_id"] = (isset($_REQUEST["server_id"])) ? $_REQUEST["server_id"] : "";
        $this->app["server_name"] = (isset($_REQUEST["server_name"])) ? $_REQUEST["server_name"] : "";
        $this->app["loc_id"] = (isset($_REQUEST["loc_id"])) ? $_REQUEST["loc_id"] : "";
        $this->app["tab_name"] = (isset($_REQUEST["tab_name"])) ? $_REQUEST["tab_name"] : "";
        $this->app["cc"] = (isset($_REQUEST["cc"])) ? $_REQUEST["cc"] : "";
        $this->app["dn"] = (isset($_REQUEST["dn"])) ? $_REQUEST["dn"] : "";

        //error_log( "DEBUG: member got dataname=". $this->app['dn'] ." loc_id=". $this->app['loc_id'] );  /** debug **/

        // A collection of multiple doCmds to chain together and run in one shot
        // (since running subsequent doCmds may cause the previous one to be skipped).
        $this->doCmds = array();

        // Location info, once we have the dataname and location ID.
        if ( isset( $_SESSION['timezone'] ) ) {
        	date_default_timezone_set( $_SESSION['timezone'] );
        }
        else if ( empty( $_SESSION['timezone'] ) && isset( $this->app["dn"] ) && isset( $this->app["loc_id"] )  ) {
        	$this->location_info = get_location_info( $this->app["dn"], $this->app["loc_id"] );  // From core_functions.php

	        // Default timezone, based on CP settings.
        	if ( !empty( $this->location_info['timezone'] ) ) {
	        	date_default_timezone_set( $this->location_info['timezone'] );
	        	$_SESSION['timezone'] = $this->location_info['timezone'];
        	}
        }

        //error_log( "SESSION[timezone]=". $_SESSION['timezone'] );  /** debug **/

		// Fitness component-specific vars
		$this->billingUtil = new RecurringBilling();
		$this->rfid = (isset($_REQUEST["rfid"])) ? $_REQUEST["rfid"] : "";
		$this->customer_id = (isset($_REQUEST["customer_id"])) ? $_REQUEST["customer_id"] : "";
		$this->userFound = 0;
		$this->userExpired = false;

		if ($this->rfid != "") {
			// Strip the newline that may have been tacked on to the end from the KEI.
			$this->rfid = str_replace("\n", "", $this->rfid);
			$this->fetchMemberInfo(array("rfid" => $this->rfid));
		}
		else if ($this->customer_id != "") {
			$this->fetchMemberInfo(array("id" => $this->customer_id));
		}

		// Customer component-specific vars
		$this->med_posted = (isset($_REQUEST['med_posted'])) ? $_REQUEST['med_posted'] : "";
		if (isset($_REQUEST['mode'])) {
			switch ( $_REQUEST['mode'] ) {
				case 'update_customer':
					$this->page = 'edituser';
					break;
				case 'history':
					$this->page = 'history';
					break;
			}
		}


		if ($this->page == "" && $this->med_posted == "1" && empty($this->customer_id) && empty($_SESSION['customer_id'])) {

			//error_log( 'DEBUG: Calling htmlForEditUserInfo() in constructor blank page newuser branch customer_id='. $this->customer_id .' SESSION[customer_id]='. $_SESSION['customer_id'] );  /** debug **/

			// When newuser page posts a new user to create, we actually send it back through
			// the newuser routing branch again to re-run draw_med_customer_form() (by way
			// of its wrapper method) because that will process the new user insert.
			$this->page = "newuser";
		}
		else if ($this->page == "" && $this->med_posted == "1" && empty($this->customer_id) && !empty($_SESSION['customer_id'])) {

			//error_log( 'DEBUG: Calling htmlForEditUserInfo() in constructor blank page edituser branch customer_id='. $this->customer_id .' SESSION[customer_id]='. $_SESSION['customer_id'] );  /** debug **/

			// Run the wrapper function htmlForEditUserInfo() to update the user's information by calling
			// htmlForEditUserInfo() (but not saving the returned html, since we're going to display the
			// user's information) and then re-pulling the user's *updated* data.
			$this->customer_id = (empty($this->customer_id)) ? $_SESSION['customer_id'] : $this->customer_id;
			$_SESSION['customer_id'] = '';

			$this->htmlForEditUserInfo($this->customer_id);
			$this->fetchMemberInfo(array("id" => $this->customer_id));

			// Send people who just submitted editted user info back to the view user page
			// so they can see their newly updated info.
			$this->page = "viewuser";

			// TO DO : Reload the Recent Member Sign-ins in case any of their info has been changed
			///$this->js["run"] .= $this->jsForReloadingSignins();
			$this->doCmds['reload_signins'] = 1;
		}
		else if ($this->page == "edituser" && $this->med_posted == "") {
			// When we are editing a user's info but haven't posted the data back
			// to the draw_med_customer_form() form yet, we have to save the customer_id
			// in the session so we can pull it out when/if they submit the form and go
			// to update the user's info.
			//error_log( "DEBUG: in constructor page=edituser branch saving customer_id {$this->customer_id} to session" );  /** debug **/
			$_SESSION['customer_id'] = $this->customer_id;
		}

		// HTML content vars
		$this->html['memberInfoSection'] = "";
	}

    public function appVarsQuerystring() {
        $querystring = "";
        foreach ($this->app as $key => $val) {
        	if ( $key != 'cc' ) {  // Viewing page=invoice kept breaking until cc was suppressed
        		$querystring .= "&" . urlencode($key) ."=". urlencode($val);
        	}
        }
        return $querystring;
    }

    public function appVarsHiddenInputs() {
        $hiddenInputs = "";
        foreach ($this->app as $key => $val) {
        	if ( $key != 'cc' ) {  // Viewing page=invoice kept breaking until cc was suppressed
        		$hiddenInputs .= <<<HTML
        		<input type="hidden" name="{$key}" value="{$val}" />
HTML;
        	}
        }
        return $hiddenInputs;
    }

	public function runDoCmds() {
		$doCmdsJs = '';

		if ( !empty( $this->doCmds ) ) {
			if ( $this->doCmds['reload_signins'] ) {
				//$doCmdsJs .= "_DO:cmd=send_js&c_name=members_signins&js=window.location.reload(true);";
				$doCmdsJs .= "_DO:cmd=send_js&c_name=members_wrapper&js=reloadLastLoadedMemberSignupsPage();";
			}
			if ( $this->doCmds['reset_searchbuttons'] ) {
				$doCmdsJs .= "_DO:cmd=send_js&c_name=members_wrapper&js=document.getElementById('add_customer_button_id').src='images/grey_plus.png';";
			}
			$doCmdsJs = "window.location = \"{$doCmdsJs}\";";
		}

		//error_log( "doCmdsJs={$doCmdsJs}" );  /** debug **/

		return $doCmdsJs;
	}

	public function htmlForEditUserInfo($id) {
		//error_log( 'DEBUG: htmlForEditUserInfo('. $id . ')' );  /** debug **/
		$id = ($id == '') ? '0' : $id;
		$html = draw_med_customer_form(false, $id);
		return $this->replaceMedCustomerEditAndHistoryUrls( $html );  // From order_addons/customers.php
	}

	public function htmlForViewUserInfo() {

		return <<<HTML
HTML;
/*		<table align="right" cellspacing="4">
		<tr>
			<td align="right" class="gray">First Name</td>
			<td width="124" ontouchstart="editMemberInfo('{$this->rfid}')">{$this->memberInfo["FirstName"]}</td>
		</tr>
		<tr>
			<td align="right" class="gray">Last Name</td>
			<td width="124" ontouchstart="editMemberInfo('{$this->rfid}')">{$this->memberInfo["LastName"]}</td>
		</tr>
		<tr>
			<td align="right" class="gray">Email</td>
			<td width="124" ontouchstart="editMemberInfo('{$this->rfid}')">{$this->memberInfo["Email"]}</td>
		</tr>
		<tr>
			<td align="right" class="gray">Phone Number</td>
			<td width="124" ontouchstart="editMemberInfo('{$this->rfid}')">{$this->memberInfo["HomeNumber"]}</td>
		</tr>
		<tr>
			<td align="right" class="gray">Zip Code</td>
			<td width="124" ontouchstart="editMemberInfo('{$this->rfid}')">{$this->memberInfo["ZipCode"]}</td>
		</tr>
		<tr>
			<td align="right" class="gray">How did you hear about us?</td>
			<td width="124" ontouchstart="editMemberInfo('{$this->rfid}')">{$this->memberInfo["HowHeardAboutUs"]}</td>
		</tr>
		</tr>
		</table>*/
	}

	public function htmlForUserHistory() {
		$html = "";

		/* Taken from customer.php */
		//$_SESSION['saved_scroll_position'] = $_GET['saved_scroll_position'];
	    $html .= "<div id='customerHistoryBanner_div' style='width:390px; height:60px; background-color:#DAA; background: url(images/history_banner.png) no-repeat; background-size:100%; margin-left:25px'>" .
	    create_padded_touch_area(
	               "<input id='view_history_back_input_id' type='button' ".
	               "style='background:URL(images/back_arrow.png) no-repeat; background-size:100%; width:25px; height:25px; " .
	                      "background-position:center; border:0px;' />" ,'view_history_back_input_div_id', 0, 0, 50, 50) . 
	         "</div>";
	    //insertButtonGraphicsJavascript();
       	require_once(dirname(__FILE__) . '/../order_addons/customer_history.php');
		//$html .= "<script type='text/javascript'> window.location = " . 
		//      "'_DO:cmd=send_js&c_name=customer_wrapper&js=set_side_bar_letters_visible(false);unlockButtonsDelayed();setLoadingHidden(true);' </script>";

		return $html;
	}

	public function jsForReloadingSignins() {
		return <<<JS

		reloadMembersSignins();

JS;
	}

	public function jsForResettingSearchButtons() {
		return <<<JS

		resetSearchButtons();

JS;

	}

	public function fetchMemberInfo($searchParams) {
		$row_query = "";
		if (isset($searchParams['id'])) {
			$row_query = lavu_query("select * from `med_customers` where `id`='[id]'", array("id" => $searchParams['id']));
		}
		else {
			$row_query = lavu_query("select * from `med_customers` where `rfid`='[rfid]'", array("rfid" => $searchParams['rfid']));
		}

		if (mysqli_num_rows($row_query)) {
			$this->userFound = 1;

		    // Create js Member object using data from database.
			$row_read = mysqli_fetch_assoc($row_query);

			// We may override some already-set values but we have to make sure all of these are set.
			$this->customer_id = $row_read["id"];
			$this->rfid = $row_read["rfid"];

			// Check if waiver has been signed by checking to see if signature file exists for their customer ID.
			$this->waiver = file_exists( dirname(__FILE__) . "/../lavukart/companies/{$this->app['dn']}/signatures/signature_{$this->customer_id}.jpg" );

			// Load their billing profile, which we'll need to fill out the memberInfo array.
			$this->billingProfile = $this->billingUtil->getBillingProfileForCustomerId( $this->customer_id );

			// Escape any double-quotes contained in the member data so the javascript doesn't break.
			$this->memberInfo["FirstName"] = str_replace('"', '\"', $row_read["f_name"]);
			$this->memberInfo["LastName"] = str_replace('"', '\"', $row_read["l_name"]);
			$this->memberInfo["Email"] = str_replace('"', '\"', $row_read["field1"]);
			$this->memberInfo["Address"] = str_replace('"', '\"', $row_read["field3"]);
			$this->memberInfo["City"] = str_replace('"', '\"', $row_read["field4"]);
			$this->memberInfo["State"] = str_replace('"', '\"', $row_read["field5"]);
			$this->memberInfo["ZipCode"] = str_replace('"', '\"', $row_read["field6"]);
			$this->memberInfo["HomePhone"] = str_replace('"', '\"', $row_read["field2"]);
			$this->memberInfo["WorkPhone"] = str_replace('"', '\"', $row_read["field10"]);
			$this->memberInfo["CellPhone"] = str_replace('"', '\"', $row_read["field11"]);
			$this->memberInfo["DOB"] = str_replace('"', '\"', $row_read["field7"]);
			$this->memberInfo["Company"] = str_replace('"', '\"', $row_read["field8"]);
			$this->memberInfo["HowHeardAboutUs"] = str_replace('"', '\"', $row_read["field9"]);
			$this->memberInfo["ProfilePicture"] = str_replace('"', '\"', $row_read["profile_picture"]);
			$this->memberInfo['Membership'] = $this->billingProfile['name'];
			$this->memberInfo["MembershipStartDate"] = date("m/d/Y", strtotime($row_read["date_created"]));
			$this->memberInfo["MembershipOutstandingBalance"] = isset( $this->billingProfile ) ? money_format( '%.2n', floatval( $this->billingUtil->getUnpaidAmountForCustomerId( $this->customer_id ) ) ) : '-';
			$this->memberInfo["MembershipExpirationDate"] = (isset( $this->billingProfile ) && isset($this->billingProfile['end_date']) && $this->billingProfile['end_date'] != '0000-00-00 00:00:00') ? date("m/d/Y", strtotime( $this->billingProfile['end_date'] ) ) : '-';
			$this->memberInfo["CreditCardExpirationDate"] = isset( $this->billingProfile ) ? $this->billingProfile['cc_exp_date'] : '-';

			// Add this on the jpg's "querystring" to generate unique requests to prevent caching
			$unixtimestamp = ($this->memberInfo["ProfilePicture"] != '') ? '?' . time() : '';

			// Set up keys for the different image sizes that are automatically captured of each profile picture.
			$this->memberInfo["ProfilePictureThumbnail"] = str_replace('SIZE/', '', $this->memberInfo["ProfilePicture"]);
			$this->memberInfo["ProfilePictureMain"] = str_replace('SIZE/', 'main/', $this->memberInfo["ProfilePicture"] . $unixtimestamp);
			$this->memberInfo["ProfilePictureFull"] = str_replace('SIZE/', 'full/', $this->memberInfo["ProfilePicture"]);
			///error_log( "ProfilePictureMain=". $this->memberInfo["ProfilePictureMain"] );  // debug

			// Commenting this out because it wasn't correct - we need to get the item id and then query the menu_items table for the image column.
			// Build a link to the menu item image of what their membership is otherwise default to a standard logo.
			//$menuItemImg = dirname(__FILE__) . "/../../images/{$this->app['dn']}/items/{$this->memberInfo['Membership']}.jpg";
			//$this->memberInfo['MembershipImg'] = (file_exists($menuItemImg)) ? $menuItemImg : '';

			// Check for expired memeberships because we would need to block any signin attempts.
			$this->userExpired = ( isset($this->billingProfile) && (strtotime('now') >= strtotime($this->billingProfile['end_date'])) );
	    }
	}

	public function insertMedActionLog( $customerId, $action, $orderId ) {
		// Set device_time based on the location's time zone information.
		$device_time = date("Y-m-d H:i:s");

		// Insert sign-in - we will already have the user's info if rfid was passed in.
		$query_vars = array(
			"action" => $action,
			"customer_id" => $customerId,
			"order_id" => $orderId,
			"restaurant_time" => date("Y-m-d H:i:s"),
			"user_id" => $this->app["server_id"],
			"loc_id" => $this->app["loc_id"],
			"device_time" => $device_time
		);
		$insert_query = "insert into `med_action_log` (`action`, `customer_id`, `order_id`, `restaurant_time`, `user_id`, `loc_id`, `device_time`) values ('[action]', '[customer_id]', '[order_id]', '[restaurant_time]', '[user_id]', '[loc_id]', '[device_time]')";
		$success = lavu_query($insert_query,$query_vars);
		if ($success) {
			$row_updated = lavu_insert_id();
			//$msg = "inserted new row: $row_updated";
			$this->msg = "inserted new row: $row_updated";
		}
		else {
			//$msg = "failed to insert new row";
			$this->msg = "failed to insert new row";
		}

		return $success;
	}

	public function fetchLastInsertedUserId() {
		$newestId = null;
		$query_results = lavu_query("select max(id) from `med_customers`");  // TO DO : see if there is a one-arg version of this
		if (mysqli_num_rows($query_results)) {
		    // Create js Member object using data from database.
			$row = mysqli_fetch_row($query_results);
			$newestId = $row[0];
		}
		return $newestId;
	}

	public function uploadUserPicture() {
		// TO DO : check for established convention for getting web root dirs
		$imageFileDirBase = '/home/poslavu/public_html/admin/images/lavu_fitness';
		$sectionDir = "members";
		$imageSizeDir = "full";

		$pictureDirs = array($imageFileDirBase, $sectionDir, $imageSizeDir);
		$picturePathSoFar = "";
		foreach ($pictureDirs as $currentDir) {
			$picturePathSoFar .= $currentDir . DIRECTORY_SEPARATOR;
			if ( !file_exists($picturePathSoFar) ) {
				if ( !mkdir($picturePathSoFar, 0775, true) ) {
					die("Couldn't create directory: $picturePathSoFar");
				}
			}
		}

		$imageFileName = $this->customer_id ."_profile_pic.jpg";
		array_push($pictureDirs, $imageFileName);

		$imageFilePath = implode(DIRECTORY_SEPARATOR, $pictureDirs);
		//$decodedImageData = base64_decode($_REQUEST['newMemberPicture']);  /** before ajax pic upload **/
		$picture_content = str_replace(" ", "+", $_REQUEST['newMemberPicture']);
		$decodedImageData = base64_decode($picture_content);
		$imageData = imagecreatefromstring($decodedImageData);
		$savedImage = imagejpeg($imageData, $imageFilePath);
		imagedestroy($imageData);

		// Update picture in database.

		$profilePictureUrl = str_replace('/home/poslavu/public_html/admin/', 'http://admin.poslavu.com/', $imageFilePath);
		$profilePictureUrl = str_replace($imageSizeDir, 'SIZE', $profilePictureUrl);
		$success = lavu_query("update `med_customers` set `profile_picture` = '[profile_picture]' where `id` = '[id]'", array('profile_picture' => $profilePictureUrl, 'id' => $this->customer_id));
		if (!$success)
		{
			die( "failed to update profile picture for customer id {$this->customer_id}" );
		}

		// Create resized picture for "main" version.

        $mainImageSizeDir = "main";
        $mainImageFilePath = str_replace($imageSizeDir . DIRECTORY_SEPARATOR, $mainImageSizeDir . DIRECTORY_SEPARATOR, $imageFilePath);
        $mainImageWidth = "163";
        $mainImageHeight = "163";

		$mainImageDir = dirname( $mainImageFilePath );
		if ( !file_exists( $mainImageDir ) ) {
			if ( !mkdir($mainImageDir, 0775, true) ) {
				die("Couldn't create directory: $mainImageFilePath");
			}
		}

		//$convertcmd = "convert '$imageFilePath' -thumbnail '".$mainImageWidth."x".$mainImageHeight.">' '$mainImageFilePath'";
		$convertcmd = "convert ".lavuShellEscapeArg($imageFilePath)." -thumbnail ".lavuShellEscapeArg($mainImageWidth.'x'.$mainImageHeight)." > ".lavuShellEscapeArg($mainImageFilePath);
		//exec($convertcmd);
		lavuExec($convertcmd);
		chmod($mainImageFilePath,0775);


        // Create resized picture for "thumbnail" version.

        $thumbnailImageSizeDir = "";
        $thumbnailFilePath = str_replace($imageSizeDir . DIRECTORY_SEPARATOR, $thumbnailImageSizeDir . DIRECTORY_SEPARATOR, $imageFilePath);
        $thumbnailImageWidth = "81";
        $thumbnailImageHeight = "81";

        // The directory the thumbnails go in already exists so no need to check.

		//$convertcmd = "convert '$imageFilePath' -thumbnail '".$thumbnailImageWidth."x".$thumbnailImageHeight.">' '$thumbnailFilePath'";
		$convertcmd = "convert ".lavuShellEscapeArg($imageFilePath)." -thubnail ".lavuShellEscapeArg($thumbnailImageWidth."x".$thumbnailImageHeight)." > ".lavuShellEscapeArg($thumbnailFilePath);
		//exec($convertcmd);
		lavuExec($convertcmd);
		chmod($thumbnailFilePath,0775);

	}

	/**
	* The outputted html for the med_customer customer search results can't be overriden with javascript so
	* we just have to search and replace the links (which get fed to window.location, conveniently) with DO cmds.
	*/

	public function replaceMedCustomerEditAndHistoryUrls( $html ) {
		// ?mode=update_customer&customer_id=$customer_id
		// ?mode=history&customer_id=$customer_id
		$editButtonSearchString = '?mode=update_customer&customer_id='. $this->customer_id .'&';
		$historyButtonSearchString = '?mode=history&customer_id='. $this->customer_id .'&';

		//error_log( "DEBUG: in replaceMedCustomerEditAndHistoryUrls()" );  //debug
		//error_log( "DEBUG: foundEditButtons=". strpos( $html, 'update_customer' ) );  //debug
		//error_log( "DEBUG: editButtonSearchString=". $editButtonSearchString );  //debug
		//error_log( "DEBUG: saved_position_id=". $_REQUEST['saved_position_id'] );

		$html = str_replace( $editButtonSearchString, "_DO:cmd=load_url&c_name=members_signins&f_name=fitness/members_signins.php?page=edituser&customer_id=". $this->customer_id .$this->appVarsQuerystring(), $html );
		$html = str_replace( $historyButtonSearchString, "_DO:cmd=load_url&c_name=members_signins&f_name=fitness/members_signins.php?page=memberhistory&customer_id=". $this->customer_id .$this->appVarsQuerystring(), $html );

		return $html;
	}
}


/* Site Logic */

$site = new MembersSite();

$site->html['showHomeButtons'] = false;
$site->css['badge_icon'] = ($site->rfid != "") ? "opaque" : "";
$site->css['waiver'] = ($site->waiver) ? "opaque" : "";
$site->css['billing'] = ($site->billingProfile) ? '' : 'opaque';
$site->html['rfid'] = ($site->rfid == "") ? "(None)" : $site->rfid;

$site->js['lockScrolling'] = <<<JS

	// Prevents the webview from scrolling.
	document.addEventListener("touchmove", touchMove, false);
	function touchMove(event) {
    	event.preventDefault();
	}

JS;

// Routing Logic

if ($site->page == "manualsigninuser") {
	$site->insertMedActionLog( $site->customer_id, 'Member Sign-in', 'customer_signin' );

	$site->title = "New Member Sign-in";
	$site->memberInfo["SignInTime"] = date('g:i a', time());
	$site->html['memberInfoSection'] = $site->htmlForViewUserInfo();

	$site->doCmds['reload_signins'] = 1;  // TO DO : only reload signins if displaying the Recent Member Sign-ins
}
else if ($site->page == "signinuser" && !$site->userFound) {
	$site->title = "Unassigned Badge";
	$site->css["ProfilePicture"] = "visible";
}
else if ($site->page == "signinuser" && $site->userFound && $site->userExpired) {
	$site->insertMedActionLog( $site->customer_id, 'Expired Sign-in', 'expired_signin' );

	$site->title = "Membership Expired";
	$site->css["ProfilePicture"] = "visible";
	$site->html['memberInfoSection'] = $site->htmlForViewUserInfo();
}
else if ($site->page == "signinuser" && $site->userFound) {
	$site->insertMedActionLog( $site->customer_id, 'Member Sign-in', 'customer_signin' );

	$site->title = "New Member Sign-in";
	$site->memberInfo["SignInTime"] = date('g:i a', time());
	$site->html['memberInfoSection'] = $site->htmlForViewUserInfo();

	// Reload the Recent Member Sign-ins component.
	///$site->js["run"] .= $site->jsForReloadingSignins();
	$site->doCmds['reload_signins'] = 1;  // TO DO : only reload signins if displaying the Recent Member Sign-ins
}
else if ($site->page == "viewuser") {
	$site->title = "Member Info";
	$site->memberInfo["SignInTime"] = '';
	$site->css["ProfilePicture"] = "displayed";
	$site->html['memberInfoSection'] = $site->htmlForViewUserInfo();
	///$site->js["run"] .= $site->jsForResettingSearchButtons();
	$site->doCmds['reset_searchbuttons'] = 1;
}
else if ($site->page == "edituser") {
	$site->title = "Edit Member Info";
	$site->css["ProfilePicture"] = "displayed";
	//error_log( 'DEBUG: Calling htmlForEditUserInfo() in page=edituser' . " lookupIdToUse={$lookupIdToUse}" );  /** debug **/
	$site->html['memberInfoSection'] = $site->htmlForEditUserInfo($site->customer_id);

	if ( $site->med_posted == "1" ) {  // may not need to check this since it might be implied with page=edituser
		$site->insertMedActionLog( $lookupIdToUse, 'Member Info Edited', 'fitness_edituser' );
	}
}
else if ($site->page == "newuser") {
	//error_log( 'DEBUG: Calling htmlForEditUserInfo() in page=newuser' . " lookupIdToUse={$lookupIdToUse} customer_id={$site->customer_id}" );  /** debug **/

	$site->title = "Create New Member";
	$site->css["ProfilePicture"] = "displayed";
	$site->html['memberInfoSection'] = $site->htmlForEditUserInfo( $site->customer_id );

	// med_posted indicates a new user has been created.  Calling draw_med_customer_form() by
	// way of its wrapper $site->htmlForEditUserInfo() will perform the insert and then we
	// need to duplicate the viewuser block after fetching the database ID so we can view the
	// newly inserted user's info.
	if ($site->med_posted == "1") {
		$site->customer_id = ($site->customer_id) ? $site->customer_id : $site->fetchLastInsertedUserId();  // don't fetch the last inserted ID for an updated user
		$site->fetchMemberInfo(array("id" => $site->customer_id));

		$site->insertMedActionLog( $site->customer_id, 'Member Created', 'fitness_newuser' );

		$site->title = "Member Info";
		$site->memberInfo["SignInTime"] = '';
		$site->css["ProfilePicture"] = "displayed";
		$site->html['memberInfoSection'] = $site->htmlForViewUserInfo();
	}
}
else if ($site->page == "uploadpic") {
	//error_log( "DEBUG: In page=uploadpic" );
	$site->uploadUserPicture();
	$site->insertMedActionLog($site->customer_id, 'New Member Profile Pic Uploaded', 'fitness_profile_pic');

	// Need to re-fetch user data to get the latest profile picture.
	$site->fetchMemberInfo(array("id" => $site->customer_id));

	$site->title = "Member Info";
	$site->memberInfo["SignInTime"] = '';
	$site->css["ProfilePicture"] = "displayed";
	$site->html['memberInfoSection'] = $site->htmlForViewUserInfo();

	// Reload the Recent Member Sign-ins component.
	///$site->js["run"] .= $site->jsForReloadingSignins();
	$site->doCmds['reload_signins'] = 1;
}
else if ($site->page == "history") {
	$site->title = "";
	$site->css["ProfilePicture"] = "hidden";
	$site->html['memberInfoSection'] = $site->htmlForUserHistory($site->customer_id);
}
else {
	$site->title = "";
	$site->css["ProfilePicture"] = "hidden";
	$site->html['memberInfoSection'] = '';
	$site->html['showHomeButtons'] = true;

	///$site->js["run"] .= $site->jsForResettingSearchButtons();
	$site->doCmds['reload_signins'] = 1;
}

/* From /components/order_addons/customer.php */

$mode = reqvar("mode");
$customer_id = reqvar('customer_id');
$searchterm = reqvar("searchterm");

//echo "<script type='text/javascript'> alert('MODE: $mode'); </script>";
if( $mode != 'delivery_options' && ($mode == 'do_search' || $searchterm!="") )
{
	$totalOffset;
	if( !empty($_GET['saved_position_id']) ){
   		$totalOffset = $_SESSION['saved_scroll_position'];
	}

	$site->js['lockScrolling'] = '';
	$site->html['memberSearchResults'] = draw_med_customer_list($searchterm, false, $totalOffset);
	$site->html['memberSearchResults'] = $site->replaceMedCustomerEditAndHistoryUrls( $site->html['memberSearchResults'] );
	$site->html['showHomeButtons'] = false;

	// TO DO : make this work
	//Load the letter sidebar in the other component.
	///$site->html['memberInfoSection'] .= "<script type='text/javascript'> window.location = '_DO:cmd=send_js&c_name=members_wrapper&js=set_side_bar_letters_visible(true);unlockButtonsDelayed();setLoadingHidden(true);' </script>";
}



/* Site html */

echo <<<HTML

<!DOCTYPE html>
<html>
<head>
<meta http-equiv="cache-control" content="no-cache" />
<style>
body, div, td {
	font-family: Verdana, Arial;
	font-size: 12px;
}
img {
	-webkit-border-radius: 10px;  /* rounded corners */
}
dd, dt {
	margin: 5px;
}
#homeButtons {
	width: 330px;
	padding-top: 0px;
	position: relative;  /* to allow the X button to be absolutely positioned within this div */
	text-align: center;
}
#homeButtons input, #savePictureButton {
	width: 80%;
	height: 50px;
	margin-bottom: 12px;
	background-color: white;
	border-color: #ddd;
	color: #aecd37;
	/*background-color: white;
	border-color: #aecd37;
	color: #aaa;*/
	font-size: 12pt;
	-webkit-border-radius: 10px;  /* rounded corners */
	-webkit-appearance: none;  /* crucial for removing mobile safari button styles */
	text-transform: uppercase;
}
#homeButtons input.litup {
	width: 80%;
	height: 50px;
	margin-bottom: 12px;
	background-color: #aecd37;
	border-color: #ddd;
	color: white;
	/*background-color: white;
	border-color: #aecd37;
	color: #aaa;*/
	font-size: 12pt;
	-webkit-border-radius: 10px;  /* rounded corners */
	-webkit-appearance: none;  /* crucial for removing mobile safari button styles */
	text-transform: uppercase;
}
#memberInfo {
	width: 300px;
	padding: 10px;
	-webkit-border-radius: 10px;  /* rounded corners */
	background-color: white;
	position: relative;  /* to allow the X button to be absolutely positioned within this div */
}
#memberInfo h3 {
	margin: 0px;
	margin-bottom: 10px;
	color: #aecd37;
	display: inline-block;
}
#memberInfo em {
	font-style: normal;  /* un-italicize it */
	font-weight: normal;  /* un-bold it (from figcaption style) */
	font-size: .8em;
	color: gray;
}
#memberInfo figure {
	margin: 0px;
	display: inline-block;
	vertical-align: top;
}
#memberInfo figcaption {
	margin-left: 5px;  /* slight indention so the name looks better with the rounded corners */
	margin-bottom: 5px;
	font-weight: bold;
}
/*#memberInfoCloseButton {*/
.closeButton {
	position: absolute;  /* position the X button in the upper right had corner */
	top: 0px;  /* position the X button in the upper right had corner */
	/*right: -20px;  /* helps position the X nicely in the corner */
	right: -5px;  /* helps position the X nicely in the corner */
	font-size: 20pt;
	-webkit-appearance: none;  /* crucial for removing mobile safari button styles */
	border: none;
	padding: 10px;     /* make the button easier to tap by expanding its dimensions */
	padding-top: 0px;
	color: #ccc;
}
#memberStats {
	display: inline-block;
	vertical-align: top;
}
#memberAccountInfo {
	display: inline-block;
	vertical-align: top;
	padding: 10px;
	font-size: 10px;
}
#memberAccountInfo h4 {
	font-size: 12px;
	display: inline;
}
#memberDetails {
	text-align: right;
	display: inline-block;
	vertical-align: top;
	padding: 10px;
	font-size: 9px;
}
#memberDetails dt {
	text-align: left;
	color: gray;
}
#editCustomerInfo form table {
	/*width: 100%;*/
}
#editCustomerInfo form table input {
	/*width: 100%;*/
}
#medcustomer_table {
	width: auto;
}
.static-container {  /* Helps remove the absolute position from the parent container. */
	position: static;
}
.hidden {
	display: none;
}
.displayed {
	display: block;
}
.greenBg {
	background-color: #aecd37;
}
.gray {
	color: gray;
}
.alert {
	color: red;
}
.opaque {
	opacity: .1;
}
</style>
</head>
<body>

HTML;

if ($site->html['showHomeButtons']) {

	echo <<<HTML

<div id="homeButtons">
	<input type="button" id="recentMemberSignins" value="Recent Member Sign-ins" ontouchstart="toggleReportButtonLitness(this); loadRecentMemberSignins()" /><br>
	<input type="button" id="recentMemberSignups" value="Recent Member Sign-ups" ontouchstart="toggleReportButtonLitness(this); loadRecentMemberSignups()" /><br>
	<input type="button" id="expiringMemberships" value="Expiring Memberships" ontouchstart="toggleReportButtonLitness(this); loadMembershipExpirationDate()" /><br>
	<input type="button" id="expiringCreditCards" value="Expiring Credit Cards" ontouchstart="toggleReportButtonLitness(this); loadCreditCardExpirationDate()" /><br>
	<input type="button" id="OustandingBalances"  value="Oustanding Balances" ontouchstart="toggleReportButtonLitness(this); loadMembershipOutstandingBalances()" /><br>
</div>

HTML;

}

if ( $site->page == "signinuser" && !$site->userFound ) {

	echo <<<HTML

<div id="memberInfo" class="{$site->css['ProfilePicture']}">

	<div id="memberInfoCloseButton" class="closeButton" ontouchstart="viewMemberStartPage()">X</div>

	<h3>{$site->title}</h3>

	<div class="static-container">

		<div id="memberStats" class="{$site->css['ProfilePicture']}">

			To assign this badge:
			<ol class="gray">
				<li style="margin-left:0px">Create a new member <br>
					<strong>-OR-</strong><br>
					View an existing member</li>
				<li  style="margin-left:0px; margin-top:10px">Tap the badge icon shown on the Member Profile page.<br>
				<img height="35" src="images/badge_icon.png" border="0" style="margin-top:10px" /></li>
			</ol>

		</div>

	</div>

</div>

HTML;

}
else if ($site->page != "") {

	echo <<<HTML

<div id="memberInfo" class="{$site->css['ProfilePicture']}">

	<h3>{$site->title}</h3>

	<div class="static-container">

		<div id="memberStats" class="{$site->css['ProfilePicture']}">

			<figure>
				<img id="memberPicture" width="163" height="163" src="{$site->memberInfo["ProfilePictureMain"]}" border="0" ontouchstart="loadMemberProfile('{$site->customer_id}')" />
				<!--<figcaption class="memberCaption">{$site->memberInfo["FirstName"]} {$site->memberInfo["LastName"]} <em>{$site->memberInfo['Membership']}</em></figcaption>-->
			</figure>

		</div>

		<div id="memberAccountInfo">
			<h4>{$site->memberInfo['FirstName']} {$site->memberInfo['LastName']}</h4>
			<p>{$site->memberInfo['Membership']}</p>
		</div>

		<dl id="memberDetails">

HTML;
	if ( $site->page == "signinuser" && $site->userFound && $site->userExpired ) {
		echo <<<HTML
                        <dt>Member Since</dt>
                          <dd id="membershipStartDate">{$site->memberInfo["MembershipStartDate"]}</dd>
                        <dt>Balance Due</dt>
                          <dd id="membershipOutstandingBalance">{$site->memberInfo["MembershipOutstandingBalance"]}</dd>
			<dt>Membership Expired</dt>
			  <dd id="membershipExpirationDate" class="alert">{$site->memberInfo["MembershipExpirationDate"]}</dd>
			<!--<dt>Credit Card Expires</dt>
			  <dd id="CreditCardExpirationDate">{$site->memberInfo["CreditCardExpirationDate"]}</dd>-->
HTML;
	}
	if ( $site->page == "viewuser" || ($site->page == "signinuser" && $site->userFound) || $site->page == "newuser" || $site->page == "uploadpic" || $site->page == "edituser" ) {
/*		echo <<<HTML

			<dt>Member Since</dt>
			  <dd id="membershipStartDate">{$site->memberInfo["MembershipStartDate"]}</dd>
			<dt>Balance Due</dt>
			  <dd id="membershipOutstandingBalance">{$site->memberInfo["MembershipOutstandingBalance"]}</dd>
			<dt>Membership Expires</dt>
			  <dd id="membershipExpirationDate">{$site->memberInfo["MembershipExpirationDate"]}</dd>
			<dt>Credit Card Expires</dt>
			  <dd id="CreditCardExpirationDate">{$site->memberInfo["CreditCardExpirationDate"]}</dd>


HTML;*/
	}
	if ( $site->page == "viewuser" || $site->page == "signinuser" || $site->page == "newuser" || $site->page == "uploadpic" || $site->page == "edituser" ) {

		echo <<<HTML

		</dl>


HTML;
	}



	if ($site->page == "viewuser" || $site->page == "signinuser" || $site->page == "manualsigninuser" || $site->page == "newuser" || $site->page == "uploadpic") {
		echo <<<HTML

		<div id="memberInfoCloseButton" class="closeButton" ontouchstart="viewMemberStartPage()">X</div>

HTML;
	}
	else if ($site->page == "edituser" && $site->rfid != "") {
		echo <<<HTML

		<div id="memberInfoCloseButton" class="closeButton" ontouchstart="viewMemberInfo('{$site->rfid}')">X</div>

HTML;
	}
	// This is to make the close button work when we're in edit mode with a person who doesn't have an rfid assigned yet.
	else if ($site->page == "edituser" && $site->customer_id != "") {
		echo <<<HTML

		<div id="memberInfoCloseButton" class="closeButton" ontouchstart="viewMemberInfoById('{$site->customer_id}')">X</div>

HTML;
	}

	$sign_term = "sign";

	echo <<<HTML

		<div id="editMemberInfoSection" style="display: inline-block">

			<div id="editCustomerInfo">
				{$site->html['memberInfoSection']}
	    	</div>

			<input type="button" name="savePicture" id="savePictureButton" value="Save Picture" class="hidden" onclick="uploadProfilePicture()" /><br>
<!--
			<img id="waiver_icon"  style="margin-right:15px; -webkit-border-radius:0px" height="45" src="images/waiver_icon.png" border="0" class="{$site->css['waiver']}" ontouchstart="launchWaiver()"/>
			<img id="badge_icon"   style="margin-right:15px; -webkit-border-radius:0px" height="45" src="images/badge_icon.png" border="0" class="{$site->css['badge_icon']}" ontouchstart="launchSetCardOverlay('{$site->customer_id}')"/>
			<img id="billing_icon" style="margin-right:15px; -webkit-border-radius:0px" height="41" src="images/billing_icon.png" border="0" id="billing" class="{$site->css['billing']}" ontouchstart="loadMemberBilling('{$site->customer_id}')"/>
-->
		</div>

	</div>

</div>

<div id="memberSearchResults" style="margin:0; padding:0">
{$site->html['memberSearchResults']}
</div>

HTML;



}


echo <<<HTML

<script>

	{$site->js['lockScrolling']}

	listeningForKEI = false;
	function changeKEIstatus() {
		//alert("changeKEIstatus="+stopListeningForKEI);  /** debug **/
		if (listeningForKEI) {
			listeningForKEI = false;
			document.getElementById('test_kei_button').value = 'Start Listening';
			startListeningForKEI();
		} else {
			listeningForKEI = true;
			document.getElementById('test_kei_button').value = 'Stop Listening';
			window.location = "_DO:cmd=startListeningForKEI";
			 stopListeningForKEI();
		}
	}

	function viewMemberStartPage() {
		// "Deactivate" the + button.
		//window.location = "_DO:cmd=send_js&c_name=members_wrapper&js=document.getElementById('add_customer_button_id').src='images/grey_plus.png';";

		// Load the starting (blank) members screen.
		///setTimeout( function() { window.location = "members.php"; }, 0);
		window.location = "members.php?1=1" + "{$site->appVarsQuerystring()}";
	}

	function editMemberInfo(badgeId) {
		window.location = "members.php?page=edituser&customer_id="+ encodeURIComponent("{$site->customer_id}") +"{$site->appVarsQuerystring()}";
/*		if ( badgeId != "" ) {
			window.location = "members.php?page=edituser&rfid="+ encodeURIComponent(badgeId) +"{$site->appVarsQuerystring()}";
		} else {
			window.location = "members.php?page=edituser&customer_id="+ encodeURIComponent("{$site->customer_id}") +"{$site->appVarsQuerystring()}";
		}
*/	}

	function viewMemberInfo(badgeId) {
		window.location = "members.php?page=viewuser&rfid="+ encodeURIComponent(badgeId) +"{$site->appVarsQuerystring()}";
	}

	function viewMemberInfoById(id) {
		window.location = "members.php?page=viewuser&customer_id="+ encodeURIComponent(id) +"{$site->appVarsQuerystring()}";
	}

	function reloadMembersSignins() {
		///window.location = "_DO:cmd=load_url&c_name=members_signins&f_name=fitness/members_signins.php";
		window.location = "_DO:cmd=send_js&c_name=members_signins&js=window.location.reload(true);" + "_DO:cmd=send_js&c_name=members_wrapper&js=document.getElementById('add_customer_button_id').src='images/grey_plus.png';";
		//window.location = "_DO:cmd=send_js&c_name=members_signins&js=alert('refresh!');";
	}

	function resetSearchButtons() {
		//window.location = "_DO:cmd=send_js&c_name=members_wrapper&js=document.getElementById('add_customer_button_id').src='images/grey_plus.png';";
	}

	function customer_selected(customer_id, customer_name, jsonInfo, elems_parent_offset, customerEmail) {
		history_edit_touches_blocked = false;
		//window.location = "members.php?page=viewuser&customer_id="+ encodeURIComponent(customer_id) + "{$site->appVarsQuerystring()}";
		loadMemberProfile(customer_id);
	}

	function takePicture() {
		if ("{$site->customer_id}" != "") {
			window.location = "_DO:cmd=takePicture";
		}
	}

	function processPicture(pictureData) {
		// Show the new profile picture
		var memberPicture = document.getElementById("memberPicture");
		memberPicture.src = "data:image/jpeg;base64,"+ pictureData;

		var savePictureButton = document.getElementById("savePictureButton");
		savePictureButton.className = "displayed";

		return 1;
	}

	function readyPictureForUpload() {
		// Save the picture to a hidden form input so we can save it.
		var newMemberPicture = document.getElementById("newMemberPicture");
		newMemberPicture.value = document.getElementById("memberPicture").src.substring(23);

		var savePictureButton = document.getElementById("savePictureButton");
		savePictureButton.className = "hidden";

		return true;
	}

	/* Ajax Profile Picture Stuff -- BEGIN */

	function uploadProfilePicture() {
		make_ajax_call(window.location.href.split('?')[0], "page=uploadpic&customer_id={$site->customer_id}&newMemberPicture="+ escape( document.getElementById("memberPicture").src.substring(23) ), "profilePictureUploaded");
	}

	function profilePictureUploaded() {
		var savePictureButton = document.getElementById("savePictureButton");
		savePictureButton.className = "hidden";

		// Reload member_signins, in case profile picture appears there.
		window.location = '_DO:cmd=send_js&c_name=members_wrapper&js=reloadLastLoadedMemberSignupsPage();';
	}

	function make_ajax_call(strURL,querystr,callback) 
	{
		var xmlHttpReq = false;
		var self = this;
		// Mozilla/Safari
		if (window.XMLHttpRequest) 
		{
			self.xmlHttpReq = new XMLHttpRequest();
		}
		// IE
		else if (window.ActiveXObject) 
		{
			self.xmlHttpReq = new ActiveXObject("Microsoft.XMLHTTP");
		}
		self.xmlHttpReq.open('POST', strURL, true);
		self.xmlHttpReq.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
		self.xmlHttpReq.onreadystatechange = function() 
		{
			if (self.xmlHttpReq.readyState == 4) 
			{
				eval(callback + "(parse_ajax_response(self.xmlHttpReq.responseText))");
				//process_ajax_callback(self.xmlHttpReq.responseText);
			}
		}
		self.xmlHttpReq.send(querystr);
	}

	aj_flexible_containers = new Array();
	function ajax_register_flexible_container(container_id)
	{
		aj_flexible_containers[container_id] = container_id;
	}

	function parse_ajax_response(str)
	{
		var rsp = new Array();
		rsp['success'] = "0";
		var str_parts = str.split("&");
		for(var i=0; i<str_parts.length; i++)
		{
			var var_parts = str_parts[i].split("=");
			if(var_parts.length > 1)
				rsp[var_parts[0]] = var_parts[1];
		}
		return rsp;
	}


	/* Ajax Profile Picture Stuff -- END */

	function launchSetCardOverlay(id) {
		if ("{$site->customer_id}" != "") {
			window.location = "_DO:cmd=create_overlay&f_name=ers/floating_wrapper.php;fitness/members_card.php&c_name=floating_wrapper;members_card&dims=340,220,394,349;360,285,360,269&scroll=0;1?customer_id="+ id;
		}
	}

	function launchWaiver() {
		// Show the popup for signing the waiver, since no signature image has been detected.
		if ( "{$site->waiver}" == "" ) {
			window.location = '_DO:cmd=create_overlay&f_name=medical/customer_history_overlay_wrapper.php;fitness/members_waiver.php&c_name=customer_history_overlay_wrapper;members_waiver&dims=250,100,574,549;266,158,542,480&scroll=0;1?customer_id={$site->customer_id}&refresh_comp=members&title_start=Waiver: (name) ({$sign_term})';
		}
		// Show the popup for emailing the waiver, since the waiver has been signed.
		else {
			window.location = '_DO:cmd=create_overlay&f_name=medical/customer_history_overlay_wrapper.php;fitness/members_emailwaiver.php&c_name=customer_history_overlay_wrapper;members_emailwaiver&dims=250,100,574,549;266,158,542,480&scroll=0;1?customer_id={$site->customer_id}&arb_id={$site->billingProfile['id']}&refresh_comp=members&email={$site->memberInfo['Email']}&loc_id={$site->app['loc_id']}&server_id={$site->app['server_id']}&title_start=Signed Waiver: (name)';
		}
	}

	function loadMemberProfile(id) {
		if (id != "") {
		    window.location = '_DO:cmd=send_js&c_name=members_wrapper&js=loadMemberProfile("'+ id +'");';
		}
	}

	function loadMemberBilling(id) {
		if (id != "") {
		    window.location = '_DO:cmd=send_js&c_name=members_wrapper&js=loadMemberBilling("'+ id +'");';
		}
	}

	function loadRecentMemberSignins() {
	    window.location = '_DO:cmd=send_js&c_name=members_wrapper&js=loadRecentMemberSignins();';
	}

	function loadRecentMemberSignups() {
	    window.location = '_DO:cmd=send_js&c_name=members_wrapper&js=loadRecentMemberSignups();';
	}

	function loadMembershipExpirationDate() {
	    window.location = '_DO:cmd=send_js&c_name=members_wrapper&js=loadMembershipExpirationDate();';
	}

	function loadCreditCardExpirationDate() {
	    window.location = '_DO:cmd=send_js&c_name=members_wrapper&js=loadCreditCardExpirationDate();';
	}

	function loadMembershipOutstandingBalances() {
	    window.location = '_DO:cmd=send_js&c_name=members_wrapper&js=loadMembershipOutstandingBalances();';
	}

	var currentLitButtonId;
	function toggleReportButtonLitness(button) {
		// Return right away if the buttons aren't currently being displayed on the page.
		if (button == null) {
			return;
		}

		// Don't change anything if the button tapped is already lit up.
		if (currentLitButtonId == button.id) {
			return;
		}

		// Make the tapped button lit up
		button.className = (button.className == 'litup') ? '' : 'litup';

		// Stop the previously tapped button from being lit, if there is one.
		if (currentLitButtonId) {
			document.getElementById(currentLitButtonId).className = '';
		}

		// Register this button as the currently lit button in our global variable.
		currentLitButtonId = button.id;
	}
	toggleReportButtonLitness(document.getElementById("recentMemberSignins"));

	// Comment out refresh() during development to allow for refreshing page by simplying clicking on the Members tab.
	// Note: the refresh() function also seems to get called when the PIN lock-out screen happens.

	function refresh() {
		return 1;
	}

	function showBadgeIconAsSet(rfid) {
		var badge_icon = document.getElementById("badge_icon");
		if (badge_icon) {
			badge_icon.className = "opaque";
		}
	}

	function getAbsoluteOffset(elem){
	// offsetTop is the scalar
	// offsetParent is the element from which it is measured.
	   var totalOffset = elem.offsetTop;
	   while(elem = elem.offsetParent){
	       totalOffset += elem.offsetTop;
	   }
	   return totalOffset;
	}

	function preload_images(){
    	preload_image = new Image();
    	//preload_image.src = 'images/edit_cust_info.png';
    	//preload_image.src = 'images/edit_customer.png';
    	//preload_image.src = 'images/edit_customer_lit.png';
    	preload_image.src = 'images/green_arrow.png';
    	preload_image.src = 'images/green_arrow_lit.png';
    	//preload_image.src = 'images/history_banner.png';
    	//preload_image.src = 'images/view_history.png';
    	//preload_image.src = 'images/view_history_lit.png';
    	//preload_image.src = 'images/back_arrow.png';
    	//preload_image.src = 'images/back_arrow_lit.png';
    	//preload_image.src = 'images/deliver.png';
    	//preload_image.src = 'images/deliver_lit.png';
    	//preload_image.src = 'images/pickup.png';
    	//preload_image.src = 'images/pickup_lit.png';
	}
	preload_images();

	// Hide the med_customer edit and history buttons in the search results list,
	// since we can't override them to load in the customers_signins section.
	function hideMedCustomerEditAndHistoryButtons() {
		var memberSearchResults = document.getElementById('memberSearchResults');
		if (!memberSearchResults) return;

		var inputElements = memberSearchResults.getElementsByTagName('input');

		for ( var i = 0; i < inputElements.length; i++ ) {
			if ( inputElements[i].getAttribute('type').toLowerCase() == 'image' && (inputElements[i].id.indexOf('edit_') > -1 || inputElements[i].id.indexOf('history_') > -1) ) {
				inputElements[i].style.display = 'none';
			}
		}
	}
	hideMedCustomerEditAndHistoryButtons();

	{$site->runDoCmds()}

	var br = "\\n";
	//alert( "page={$site->page}"+ br +"userFound={$site->userFound}"+ br +"rfid={$site->rfid}"+ br +"customer_id={$site->customer_id}"+ br +"mode={$mode}"+ br +"med_posted={$site->med_posted}"+ br +"cssProfilePicture={$site->css['ProfilePicture']}"+ br +"querystring="+ window.location.search );  /** debug **/

</script>

</body>
</html>

HTML;

?>
