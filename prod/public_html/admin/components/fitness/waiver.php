<?php

/* Init */

require_once('FitnessWaiver.php' );


/** Run **/

$waiver = new FitnessWaiver( array(
	'dataname' => $_REQUEST['dataname'],
	'customerId' => $_REQUEST['customerId'],
	'arbId' => $_REQUEST['arbId'],
	'locId' => $_REQUEST['locId'],
	'employeeId' => $_REQUEST['employeeId']
) );


/**
 *
 * CRITICAL: **Never** call outputPdf() from this file!  It will result in an infinite loop and crash the server.
 *
 */

echo $waiver->outputHtml();
