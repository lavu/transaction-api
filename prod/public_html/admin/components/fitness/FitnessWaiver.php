<?php

require_once(dirname(__FILE__) . "/../../cp/resources/core_functions.php");
require_once('RecurringBilling.php' );

// Having this uncommented breaks other datanames.  But I think it was needed for the test pages, so those might be broken.  However, it can be added to broken pages.
//lavu_connect_dn( 'lavu_fitness', 'poslavu_lavu_fitness_db' );

/* Real Talk */

class FitnessWaiver
{
	var $dataname;
	var $customerId;
	var $arbId;
	var $locId;
	var $employeeId;    // aka server_id

	var $member;
	var $location;
	var $server;

	var $arbUtil;
	var $arbProfile;

	var $waiverDbRow;
	var $termsCol1;
	var $termsCol2;

	var $xml;
	var $html;

	public function __construct( $args )
	{
		$this->processRequiredArgs( $args );

		error_log( __CLASS__ ." has dataname={$this->dataname}" );
		lavu_connect_dn($this->dataname, "poslavu_{$this->dataname}_db");

		$this->obtainMemberInfo();
		$this->obtainBillingProfile();

		// TO DO : assert that the Customer ID is really associated with the Customer ID(s) on the Billing Profile.

		// TO DO : check to see if the waiver already exists and, if so, just pull it display it (?)
	}

	private function processRequiredArgs($args)
	{
		$requiredArgs = array('dataname', 'customerId', 'arbId', 'locId', 'employeeId');
		$requiredArgsPresent = '';

		foreach ( $requiredArgs as $requiredArg ) {
			if ( empty( $args[$requiredArg] ) ) {
				die( __CLASS__ . " missing required argument {$requiredArg} - only have {$requiredArgsPresent}:". lavu_dberror() );
			}

			// Develop the string showing the key and its value that will be printed in our die() statement above if we're missing any required vars.
			$requiredArgsPresent .= "{$requiredArg}={$args[$requiredArg]} ";

			// Set the required argument to its corresponding member variable.
			$this->{$requiredArg} = $args[$requiredArg];
		}
	}

	private function obtainMemberInfo()
	{
		// Pull customer data from med_customer table using passed-in customerId.
		$memberQuery = lavu_query( "select * from `med_customers` where `id`='[id]'", array( 'id' => $this->customerId ) );
		if ( $memberQuery === FALSE ) die( __FILE__ . " couldn't fetch member info for customerId={$this->customerId}:". lavu_dberror() );
		$this->member = mysqli_fetch_assoc( $memberQuery );

		// Map the generic field names to intuitive field names.  And just store the values back into the same hash,
		// since there won't be any conflicts.
		$this->member['email'] = $this->member['field1'];
		$this->member['home_phone'] = $this->member['field2'];
		$this->member['work_phone'] = $this->member['field10'];
		$this->member['cell_phone'] = $this->member['field11'];
		$this->member['address'] = $this->member['field3'];
		$this->member['city'] = $this->member['field4'];
		$this->member['state'] = $this->member['field5'];
		$this->member['zip'] = $this->member['field6'];
		$this->member['dob'] = $this->member['field7'];
		$this->member['company'] = $this->member['field8'];
		$this->member['howheard'] = $this->member['field9'];
		$this->member['company'] = $this->member['field8'];
		$this->member['ssn'] = '--';  // we don't collect this but it was on the original sityodtong membership so I'm just hardcoding here as a placeholder.

		// Develop the link path to the signature images, which live in the lavukart component subdirectory.
		// Notice that we go down in the directory tree and then up into the appropriate dir so this link
		// should work whether in dev or live (and, presumably, local server).
		$this->member['signature'] = "../lavukart/companies/{$this->dataname}/signatures/signature_{$this->customerId}.jpg";
	}

	private function obtainBillingProfile()
	{
		// Pull arb data using passed-in arbId (aka Billing Profile ID).
		$this->arbUtil = new RecurringBilling();
		$this->arbProfile = $this->arbUtil->getBillingProfileById( $this->arbId );
	}

	private function obtainWaiverDbRow()
	{
		$this->termsCol1 = '';
		$this->termsCol2 = '';

		// Get the same waiver text that the app uses from lk_waiver.
		$waiverQuery = lavu_query( "SELECT * FROM `lk_waivers` WHERE `loc_id` = '[loc_id]' ORDER BY `datetime` desc, `id` DESC LIMIT 1", array('loc_id' => $this->locId) );
		if ($waiverQuery === FALSE) die( __FILE__ . "Couldn't get newest waiver for loc_id={$this->locId}:". lavu_dberror() );

		if ( mysqli_num_rows( $waiverQuery ) ) {
			$this->waiverDbRow = mysqli_fetch_assoc( $waiverQuery );

			// Split on "\nT", which is the 2nd paragraph of the 6th T & C paragraph - the mid-way point of the text.
			$tc = explode( "\nT", $this->waiverDbRow['adult'] );

			// Remove the "TERMS AND CONDITIONS" header from 1st half, since it's already in the html.
			$this->termsCol1 = str_replace( "TERMS AND CONDITIONS:\n\n", '', $tc[0] );
			$this->termsCol1 = nl2br( $this->termsCol1 );

			// Restore the 'T' to 2nd half that we stripped out on the explode.
			$this->termsCol2 = 'T' . $tc[1];
			$this->termsCol2 = nl2br( $this->termsCol2 );
		}
		else {
			die( "No Waiver on file for loc_id={$this->locId}" );
		}
	}

	private function obtainLocationInfo()
	{
		// Pull customer data from med_customer table using passed-in customerId.
		$locationQuery = lavu_query( "select * from `locations` where `id`='[id]'", array( 'id' => $this->locId ) );
		if ( $locationQuery === FALSE ) die( __FILE__ . " couldn't fetch location info for locId={$this->locId}:". lavu_dberror() );
		$this->location = mysqli_fetch_assoc( $locationQuery );
	}

	private function obtainEmployeeInfo()
	{
		// Pull server user data from users table using passed-in employeeId (aka server_id).
		$userQuery = lavu_query( "select * from `users` where `id`='[id]'", array( 'id' => $this->employeeId ) );
		if ( $userQuery === FALSE ) die( __FILE__ . " couldn't fetch users info for id={$this->employeeId}:". lavu_dberror() );
		$this->server = mysqli_fetch_assoc( $userQuery );
	}

	private function processSiblingArbs( &$arb )
	{
		// Query for all sibling ARBs (ARBs with the same order number) and create an array key 'memberX' in $arb for each one.
		$siblingArbsQuery = lavu_query( "select c.`f_name` as f_name, c.`l_name` as l_name, b.* from `med_billing` b, `med_customers` c where b.`customer_id` = c.`id` and `order_id`='[order_id]' order by id", array( 'order_id' => $this->arbProfile['order_id'] ) );
		if ( $siblingArbsQuery === FALSE ) die( __FILE__ . " error with sibling ARB query for order_id={$this->arbProfile['order_id']}" );
		$i = 1;
		while ( $siblingArbs = mysqli_fetch_assoc( $siblingArbsQuery ) ) {
			$memberName = $siblingArbs['f_name'] .' '. $siblingArbs['l_name'];
			$arb["memberName{$i}"] = $memberName;
			$arb['siblingArbXml'] .= <<<XML

		<member order="{$i}">{$memberName}</member>

XML;
			$i++;
		}
	}

	public function generateWaiverXml()
	{
		$this->obtainWaiverDbRow();
		$this->obtainLocationInfo();
		$this->obtainEmployeeInfo();

		// Map the arb fields to user-dipslayable versions of the values.
		$arb = array(
			'id' => $this->arbProfile['id'],
			'version' => $this->waiverDbRow['version'],
			'createTime' => date( 'Y-m-d h:i:s a' ),
			'memberName' => $this->member['f_name'] .' '. $this->member['l_name'],
			'memberId' => $this->customerId,
			'memberSignatureImagePath' => $this->member['signature'],
			'memberSignedDate' => date( 'n/j/Y' ),
			'employeeName' => $this->location['title'],  //$this->server['f_name'] .' '. $this->server['l_name'],
			'employeeId' => $this->employeeId,
			'employeeSignatureImagePath' => "../lavukart/companies/{$this->dataname}/signatures/signature_{$this->server['customer_id']}.jpg",
			'employeeSignedDate' => date( 'n/j/Y' ),

			'schoolName' => $this->location['title'],
			'schoolAddress' => $this->location['address'],
			'schoolCity' => $this->location['city'],
			'schoolCityStateComma' => (empty($this->location['city']) || empty($this->location['state']) ? '' : ', '),
			'schoolState' => $this->location['state'],
			'schoolZip' => $this->location['zip'],
			'schoolPhone' => $this->location['phone'],
			'schoolPhone2' => '',
			'upgradeCode' => '0-002363A',

			'buyerName' => $this->member['f_name'] .' '. $this->member['l_name'],
			'buyerAddress' => $this->member['address'],
			'buyerCity' => $this->member['city'],
			'buyerCityStateComma' => (empty($this->member['city']) || empty($this->member['state']) ? '' : ', '),
			'buyerState' => $this->member['state'],
			'buyerZip' => $this->member['zip'],
			'buyerPhone' => $this->member['home_phone'],
			'buyerSsn' => $this->member['ssn'],

			'cashPrice' => '$0.00',
			'downPayment' => money_format( '%.2n', $this->calculateDownPayment() ),
			'serviceChange' => '$0.00',
			'totalBilled' => money_format( '%.2n', $this->calculateBilledAmount() ),
			'paymentAmount' => '$0.00',
			'billingFrequency' => ucfirst( $this->arbProfile['bill_frequency'] ),
			'firstPaymentDate' => date( 'n/j/Y', strtotime( $this->arbProfile['first_bill_date'] ) ),
			'membershipBegins' => date( 'n/j/Y', strtotime( $this->arbProfile['start_date'] ) ),
			'membershipExpires' => date( 'n/j/Y', strtotime( $this->arbProfile['end_date'] ) ),
		);

		$this->processSiblingArbs( $arb );

		$this->xml = <<<XML

<waiver>
	<waiverInfo>
		<arbId>{$arb['id']}</arbId>
		<createTime>{$arb['createTime']}</createTime>
		<memberName>{$arb['memberName']}</memberName>
		<memberId>{$arb['customerId']}</memberId>
		<memberSignatureImagePath>{$arb['memberSignatureImagePath']}</memberSignatureImagePath>
		<memberSignedDate>{$arb['memberSignedDate']}</memberSignedDate>
		<employeeName>{$arb['employeeName']}</employeeName>
		<employeeId>{$arb['employeeId']}</employeeId>
		<employeeSignatureImagePath>{$arb['employeeSignatureImagePath']}</employeeSignatureImagePath>
		<employeeSignedDate>{$arb['memberSignedDate']}</employeeSignedDate>
	</waiverInfo>
	<schoolInfo>
		<name>{$arb['schoolName']}</name>
		<address>{$arb['schoolAddress']}</address>
		<city>{$arb['schoolCity']}</city>
		<cityStateComma>{$arb['schoolCityStateComma']}</cityStateComma>
		<state>{$arb['schoolState']}</state>
		<zip>{$arb['schoolZip']}</zip>
		<phone>{$arb['schoolPhone']}</phone>
		<phone2>{$arb['schoolPhone2']}</phone2>
		<upgradeCode>{$arb['upgradeCode']}</upgradeCode>
	</schoolInfo>
	<buyerInfo>
		<name>{$arb['buyerName']}</name>
		<address>{$arb['buyerAddress']}</address>
		<city>{$arb['buyerCity']}</city>
		<cityStateComma>{$arb['buyerCityStateComma']}</cityStateComma>
		<state>{$arb['buyerState']}</state>
		<zip>{$arb['buyerZip']}</zip>
		<phone>{$arb['buyerPhone']}</phone>
		<ssn>{$arb['buyerSsn']}</ssn>
	</buyerInfo>
	<members>{$arb['siblingArbXml']}
	</members>
	<agreementTerms>
		<cashPrice>{$arb['cashPrice']}</cashPrice>
		<downPayment>{$arb['downPayment']}</downPayment>
		<serviceChange>{$arb['serviceChange']}</serviceChange>
		<totalBilled>{$arb['totalBilled']}</totalBilled>
		<paymentAmount>{$arb['paymentAmount']}</paymentAmount>
		<billingFrequency>{$arb['billingFrequency']}</billingFrequency>
		<firstPaymentDate>{$arb['firstPaymentDate']}</firstPaymentDate>
		<membershipBegins>{$arb['membershipBegins']}</membershipBegins>
		<membershipExpires>{$arb['membershipExpires']}</membershipExpires>
	</agreementTerms>
</waiver>

XML;

		//error_log( "xml={$this->xml}" );  /** debug **/

		return $this->xml;
	}

	public function storeWaiverXml()
	{
		if ( empty( $this->xml ) ) {
			$this->generateWaiverXml();
		}

		// Save the newly generated waiver template XML to the ARB table.
		$waiverXmlUpdate = lavu_query( "update `med_billing` set waiver_template = '[waiver_template]' where id = '[id]'", array( 'id' => $this->arbId, 'waiver_template' => $this->xml ) );

		if ( $waiverXmlUpdate === FALSE ) {
			$this->errorLog( "Failed to save waiver template for arbId={$this->arbId}:". lavu_dberror() );
			return false;
		}

		return true;
	}

	private function calculateFirstBillDate()  // TO DO : see if this is really needed since med_billing ARBs now have the column first_bill_date.
	{
		// Calculate the first bill date.  We do this by starting out with the arb start date and replacing the day of month
		// with the bill_event_offset.  Then we see if that date would've been before the arb start date (which means that invoice
		// day of month would've happened before the member even signed up) and, if so, pushing the bill date forward to the next month.
		$startDateComponents = getdate( strtotime( $this->arbProfile['start_date'] ) );
		$arbStartDate = strtotime( date( 'Y-m-d', strtotime( $this->arbProfile['start_date'] ) ) );
		$firstBillDate = mktime( 0, 0, 0, intval( $startDateComponents['mon'] ), intval( $this->arbProfile['bill_event_offset'] ), intval( $startDateComponents['year'] ) );
		if ( $firstBillDate < $arbStartDate ) {
			$firstBillDate = mktime( 0, 0, 0, intval( $startDateComponents['mon'] + 1 ), intval( $this->arbProfile['bill_event_offset'] ), intval( $startDateComponents['year'] ) );
		}

		return $firstBillDate;
	}

	private function calculateDownPayment()
	{
		// Calculate the down payment amount by getting the original Order ID and adding up all of the down payment amounts on membership.
		$downPayment = 0.00;
		$orderContentsQuery = lavu_query( "select * from `order_contents` where `order_id`='[order_id]'", array( 'order_id' => $this->arbProfile['order_id'] ) );
		if ( $orderContentsQuery === FALSE ) die( __FILE__ . " couldn't fetch order_contents for order_id={$this->arbProfile['order_id']}:". lavu_dberror() );
		while ( $orderContents = mysqli_fetch_assoc( $orderContentsQuery ) ) {
			if ( strpos( $orderContents['item'], 'Membership' ) !== FALSE  ) {
				$downPayment += floatval( $orderContents['price'] );
			}
		}

		return $downPayment;
	}

	private function calculateBilledAmount()
	{
		// Calculate the billed amount
		$billedAmount = 0.00;
		$orderQuery = lavu_query( "select * from `orders` where `order_id`='[order_id]'", array( 'order_id' => $this->arbProfile['order_id'] ) );
		if ( $orderQuery === FALSE ) die( __FILE__ . " couldn't fetch order_contents for order_id={$this->arbProfile['order_id']}" );
		while ( $order = mysqli_fetch_assoc( $orderQuery ) ) {
			if ( strpos( $order['item'], 'Membership' ) !== FALSE  ) {
				$billedAmount = floatval( $order['total'] ) - (floatval( $order['cash_paid'] ) + floatvl( $order['card_paid'] ));
			}
		}

		return $billedAmount;
	}

	public function outputHtml()
	{
		if ( empty( $this->xml ) ) {
			$this->generateWaiverXml();
		}

		// Parse XML and populate HTML using XML values
		$xmlObj = simplexml_load_string( $this->xml );

		/* Web Page */
		$this->html = <<<HTML

<html>
<head>
<style>
.page-break	{
	page-break-after: always;
}
.clear {
	clear: both;
}
body, td, p {
	font-family: arial, helvetica;
	font-size: 10pt;
}
h1 {
	font-size: 48pt;
	-webkit-transform: rotate(-90deg);  /* turns the text sideways */
	position: absolute;
	top: 375px;
	left: -300px;
}
h2 {
	font-size: 11pt;
	font-weight: bold;
	margin-top: 20px;
	margin-bottom: 10px;
}
#middleColumn {
	position: absolute;
	left: 150px;
}
#rightColumn {
	position: absolute;
	left: 700px;
	top: 25px;
	width: 286px;
	height: 910px;
	background-image: url('images/waiver/rightColumn.jpg');
	background-repeat: no-repeat;
	background-size: 286px 910px;
}
#rightColumn h2 {
	text-align: center;
	margin-top: 25px;
	margin-bottom: 25px;
}
#rightColumn table {
	margin-left: 15px;
}
#latefee {
	width: 200px;
	position: absolute;
	top: 380px;
	margin-left: 50px;
	font-size: 9pt;
	font-weight: bold;
	text-align: center;
}
#coupon {
	position: absolute;
	top: 480px;
	width: 300px;
	margin-left: 20px;
	font-weight: bold;
}
#coupon p {
	font-size: 10pt;
	margin-left: 30px;
}

#agreement {
	position: absolute;
	left: 150px;
	top: 935px;
	width: 800px;
}

#agreement hr {
	height: 1px;
	color: black;
	background-color: black;
	border: none;
	width: 90%;
}

#employeeName {
	position: absolute;
	left: 150px;
	top: 1150px;
}
#memberName {
	position: absolute;
	left: 150px;
	top: 1225px;
}

#employeeSig {
	position: absolute;
	left: 350px;
	top: 1100px;
	z-index: -1;
}
#memberSig {
	position: absolute;
	left: 350px;
	top: 1165px;
	z-index: -1;
}

#employeeSigLine {
	border-top: 1px solid black;
	position: absolute;
	left: 350px;
	top: 1165px;
	width: 480px;
}
#memberSigLine {
	border-top: 1px solid black;
	position: absolute;
	left: 350px;
	top: 1240px;
	width: 480px;
}

#employeeDateLine {
	border-top: 1px solid black;
	position: absolute;
	left: 850px;
	top: 1165px;
	width: 60px;
}
#memberDateLine {
	border-top: 1px solid black;
	position: absolute;
	left: 850px;
	top: 1240px;
	width: 60px;
}

#employeeDate {
	position: absolute;
	left: 854px;
	top: 1145px;
	font-size: 8pt;
}
#memberDate {
	position: absolute;
	left: 854px;
	top: 1220px;
	font-size: 8pt;
}

#employeeDateLabel {
	position: absolute;
	left: 850px;
	top: 1175px;
}
#memberDateLabel {
	position: absolute;
	left: 850px;
	top: 1250px;
}

#tc {
	top: 0px;
}

#tcTable h2 {
	margin: 0px;
	font-size: 10pt;
	font-weight: bold;
}

#tcTable p {
	font-size: 10pt;
}


</style>
</head>
<body>

HTML;

		$this->html .= <<<HTML

<div style="position:relative; width:1000px; height:1300px; overflow:none">

<h1>Membership Agreement</h1>

<div id="middleColumn">

<table width="460" border="0" cellpadding="2" cellspacing="0">
<tr>
	<td colspan="3"><h2>School Information</h2></td>
</tr>
<tr>
	<td width="200">{$xmlObj->schoolInfo->name}</td>
	<td align="left" ><strong>Upgrade Code</strong></td>
	<td align="right">{$xmlObj->schoolInfo->upgradeCode}</td>
</tr>
<tr>
	<td width="200">{$xmlObj->schoolInfo->address}</td>
	<td align="left" ><strong>Phone</strong></td>
	<td align="right">{$xmlObj->schoolInfo->phone}</td>
</tr>
<tr>
	<td width="200">{$xmlObj->schoolInfo->city}{$xmlObj->schoolInfo->cityStateComma}{$xmlObj->schoolInfo->state} {$xmlObj->schoolInfo->zip}</td>
	<td align="left"></td>
	<td align="right">{$xmlObj->schoolInfo->phone2}</td>
</tr>
<tr>
	<td colspan="3"><h2>Buyer Information</h2></td>
</tr>
<tr>
	<td width="200">{$xmlObj->buyerInfo->name}</td>
	<td align="left" ><strong>Phone</strong></td>
	<td align="right">{$xmlObj->buyerInfo->phone}</td>
</tr>
<tr>
	<td width="200">{$xmlObj->buyerInfo->address}</td>
	<td align="left"></td>
	<td align="right"></td>
</tr>
<tr>
	<td width="200">{$xmlObj->buyerInfo->city}{$xmlObj->buyerInfo->cityStateComma}{$xmlObj->buyerInfo->state} {$xmlObj->buyerInfo->zip}</td>
	<td align="left"><strong>SSN</strong></td>
	<td align="right">{$xmlObj->buyerInfo->ssn}</td>
</tr>
<tr>
	<td colspan="3"><h2>Members</h2></td>
</tr>

HTML;

		foreach ( $xmlObj->members->member as $member ) {

			$this->html .= <<< HTML

<tr>
	<td width="200">{$member}</td>
	<td align="left"></td>
	<td align="right"></td>
</tr>

HTML;

		}

		$this->html .=<<< HTML

<tr>
	<td colspan="3"><h2>Buyers Right To Cancel</h2></td>
</tr>
<tr>
	<td colspan="3"><p>If you wish to cancel this agreement, you may cancel by delivering or mailing by
	certified mail, return receipt requested, written notice to the School. The notice
	must say that you do not wish to be bound by the agreement and must be
	delivered or mailed before 12 midnight of the third business day after you sign and
	receive a copy of this agreement. The notice must be delivered or mailed to the
	School at the address shown. If you cancel, any downpayment or initial fee may
	not be refundable and the School may be entitled to a portion of the total
	agreement price. If the school goes out of business or refuses to give you a
	refund, there may be a bond or letter of credit under which you are entitled to
	collect. Member Solutions Inc. (MSI) will not be responsible for any refunds.
	Enforcement of the Health Club Act is by the Attorney General of this State or the
	district attorney of the county in which the School is located. If your rights are
	violated, you may contact the State Bureau of Consumer Protection or your district
	attorney.</p></td>
<tr>
	<td colspan="3"><h2>Payment Terms</h2></td>
</tr>
<tr>
	<td colspan="3"><p>The Buyer hereby agrees to make scheduled payments to MSI by or before each
	consecutive due date according to the terms of this agreement. A service fee of
	$25.00 is payable to MSI if any item is presented for payment and rejected for any
	reason. MSI reserves the sole right to modify any payment due date. If the billing
	method should change, Buyer continues to abide by the terms of this agreement.</p></td>
</tr>
</table>

</div>

<div id="rightColumn">

<h2>Agreement Terms</h2>

<table border="0" style="font-weight:bold">
<tr>
<td align="left">Cash Price</td>
<td align="right">{$xmlObj->agreementTerms->cashPrice}</td>
</tr>
<tr>
<td align="left">Downpayment</td>
<td align="right">{$xmlObj->agreementTerms->downPayment}</td>
</tr>
<tr>
<td align="left">Service Charge</td>
<td align="right">{$xmlObj->agreementTerms->serviceChange}</td>
</tr>
<tr>
<td align="left">Total Billed</td>
<td align="right">{$xmlObj->agreementTerms->totalBilled}</td>
</tr>
<tr>
<td align="left">Payment Amount</td>
<td align="right">{$xmlObj->agreementTerms->paymentAmount}</td>
</tr>
<tr>
<td align="left">Billing Frequency</td>
<td align="right">{$xmlObj->agreementTerms->billingFrequency}</td>
</tr>
<tr>
<td colspan="2"><br><br></td>
</tr>
<tr>
<td align="left" nowrap>First Payment Date</td>
<td align="right">{$xmlObj->agreementTerms->firstPaymentDate}</td>
</tr>
<tr>
<td align="left" nowrap>Membership Begins</td>
<td align="right">{$xmlObj->agreementTerms->membershipBegins}</td>
</tr>
<tr>
<td align="left" nowrap>Membership Expires</td>
<td align="right">{$xmlObj->agreementTerms->membershipExpires}</td>
</tr>
</table>

<div id="latefee">A late fee of $10.00 will be due for any payment 10 days past due.</div>

<div id="coupon">Coupon
<p>This is a paid in full contract.</p>
</div>

</div>

<div id="agreement">

<hr>

<p><strong>I have read this agreement and understand that once it is signed by me, it is a legally binding and enforceable
obligation and I agree to comply with all the provisions, terms and conditions herein. I acknowledge I have received a
copy of both pages of the membership agreement and that it shall be made effective on 2/7/2014</strong></p>

</div>

<div id="employeeName">{$xmlObj->waiverInfo->employeeName}</div>
<div id="memberName">{$xmlObj->waiverInfo->memberName}</div>

<img id="employeeSig" src="{$xmlObj->waiverInfo->employeeSignatureImagePath}" width="512" border="0" />
<img id="memberSig" src="{$xmlObj->waiverInfo->memberSignatureImagePath}" width="512" border="0" />

<div id="employeeSigLine"></div>
<div id="memberSigLine"></div>

<div id="employeeDateLine"></div>
<div id="memberDateLine"></div>

<div id="employeeDate">{$xmlObj->waiverInfo->employeeSignedDate}</div>
<div id="memberDate">{$xmlObj->waiverInfo->memberSignedDate}</div>

<div id="employeeDateLabel">Date</div>
<div id="memberDateLabel">Date</div>

</div>

<div class="clear"></div>

<div style="page-break-after:always"></div>

<table width="1000" border="0" id="tcTable">
<tr>
<td width="500">

<h2>TERMS AND CONDITIONS</h2>

<div id="tcColumn1">{$this->termsCol1}
</div>

</td>
<td>

<div id="tcColumn2">{$this->termsCol2}
</div>

</td>
</tr>
</table>

</div>

</body>
</html>

HTML;

	return $this->html;

	}

	public function outputPdf()
	{
		$dev = (strstr($_SERVER['REQUEST_URI'],"/dev/")) ? 'dev/' : '';
		$url = 'http://'. $_SERVER['HTTP_HOST'] ."/{$dev}components/fitness/waiver.php?output=pdf&dataname={$this->dataname}&customerId={$this->customerId}&arbId={$this->arbId}&locId={$this->locId}&employeeId={$this->employeeId}";

		//$pdfDir = "/tmp/lavu_fitness/waivers/{$this->locId}";
		$pdfDir = dirname(__FILE__) . "/../lavukart/companies/{$this->dataname}/waivers/{$this->locId}";
		$pdfName = "waiver_{$this->customerId}.pdf";
		$pdfPath = "{$pdfDir}/{$pdfName}";

		$pdfDestDir = dirname( __FILE__ ) . "/waivers";
		$pdfDestPath = "{$pdfDestDir}/{$pdfName}";

		error_log( "dev={$dev} url={$url} pdfPath={$pdfPath}" );  /** debug **/

		// Bail out if the PDF already exists - in part to guard against infinite loop stuff from happening again.
		if ( file_exists( $pdfPath ) ) {
			error_log( "DEBUG: waiver already exists ({$pdfPath})" );  /** debug **/
			return $pdfPath;
		}

		// Create the dir to house the PDF, if it doesn't already exist.
		if ( !file_exists( $pdfDir ) ) {
			if ( !mkdir( $pdfDir, 0775, true ) ) {
				die("Couldn't create directory: $pdfDir");
			}
		}
		
		// Create the waiver PDF using the command-line tool wkhtmltopdf.
		$url = lavuShellEscapeArg($url);
		$pdfPath = lavuShellEscapeArg($pdfPath);
		//$cmd = "/usr/local/bin/wkhtmltopdf \"{$url}\" {$pdfPath}";
		$cmd = "/usr/local/bin/wkhtmltopdf {$url} {$pdfPath}";
		$output = array();
		///error_log( 'DEBUG: cmd='. $cmd );  // debug
		///exec( $cmd, $output );
		lavuExec($cmd, $output);
		///error_log( "DEBUG: ". print_r( $output, 1 ) );  // debug

		// Return the path to the PDF if it was successfully created.
		return (file_exists( $pdfPath )) ? $pdfPath : '';
	}
}

?>
