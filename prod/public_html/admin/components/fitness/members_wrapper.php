<?php

require_once(dirname(__FILE__) . "/../order_addons/customer_functions.php");

$extra_vars = "";

class MembersWrapperSite {
    // Site vars
    var $page;
    var $title;
    var $msg;

    // App vars posted to every component.
    var $app;

    // Location info hash
    var $location_info;

    // Page content vars
    var $html;
    var $css;
    var $js;

    // Constructor
    public function __construct() {
        setlocale(LC_MONETARY, "en_US");

        // Site vars
        $this->page = (isset($_REQUEST["page"])) ? $_REQUEST["page"] : "";

        // Standard vars posted to every component from the app.
        $this->app["comp_name"] = (isset($_REQUEST["comp_name"])) ? $_REQUEST["comp_name"] : "";
        $this->app["server_id"] = (isset($_REQUEST["server_id"])) ? $_REQUEST["server_id"] : "";
        $this->app["server_name"] = (isset($_REQUEST["server_name"])) ? $_REQUEST["server_name"] : "";
        $this->app["loc_id"] = (isset($_REQUEST["loc_id"])) ? $_REQUEST["loc_id"] : "";
        $this->app["tab_name"] = (isset($_REQUEST["tab_name"])) ? $_REQUEST["tab_name"] : "";
        $this->app["cc"] = (isset($_REQUEST["cc"])) ? $_REQUEST["cc"] : "";
        $this->app["dn"] = (isset($_REQUEST["dn"])) ? $_REQUEST["dn"] : "";

        // HTML content vars
        $this->html['closeSearchButton']= create_padded_touch_area('<img src="images/close_X.png" id="close_button_id" style="visibility: hidden" />', "close_button_div_id", -20, -5, 35, 35);  //-32, -3, 35, 35
        $this->html['findCustomerButton'] = create_padded_touch_area('<img src="images/search_icon.png" id="search_button_id" style="margin-top:2px;"/>', "search_button_div_id", 26, -25, 35, 35);
        $this->html['addCustomerButton'] = create_padded_touch_area('<img src="images/grey_plus.png" id="add_customer_button_id" />', "add_customer_button_div_id", 65, -14, 35, 35);
    }

    public function appVarsQuerystring() {
        $querystring = "";
        foreach ($this->app as $key => $val) {
            $querystring .= "&" . urlencode($key) ."=". urlencode($val);
        }
        return $querystring;
    }
}


/* Site Logic */

$site = new MembersWrapperSite();


/* Site html */

echo <<<HTML

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Member Overlay Wrapper</title>

<style type="text/css">
<!--
body, div {
    -webkit-touch-callout: none;
    -webkit-user-select: none;
	font-family:Verdana,Arial;
}

body {
	background-color: transparent;
	margin-left: 0px;
	margin-top: 0px;
	margin-right: 0px;
	margin-bottom: 0px;
	-webkit-tap-highlight-color:rgba(0,0,0,0);
}
h2 {
    margin-left: 70px;
    color: #bbb;
}

.style4 {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 14px;
	font-weight: bold;
	color: #8896AD;
}
.style6 {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 24px;
	font-weight: bold;
	color: #8896AD;
}
.style8 {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 15px;
	font-weight: bold;
	color: #07142a;
}
-->
</style>
<script type="text/javascript">

    function showCloseButton() {
        // Show close button
        closeButton.style.visibility = "visible";
    }

    function hideCloseButton() {
        // Hide close button
        closeButton.style.visibility = "hidden";
    }

    /* From /components/order_addons/wrapper.php */

    blocking_navigations = true;
    function setBlockNavigations(isBlocking){
        //alert('Now setting to block navigations');
        //alert('blocking navigations: ' + isBlocking);
        blocking_navigations = isBlocking;
    }

    /* From /components/order_addons/customer_wrapper.php */

	function doSearch(){

	    if(nav_performing_block_buttons){
    	    return;
	    }

        showCloseButton();

	    blur_from_searchbox();
	    //alert('doing search');
	    setLoadingHidden(false);
		setval = document.getElementById("search_text_id").value;
		nav_performing_block_buttons = true;
        //setTimeout(function(){  nav_performing_block_buttons = false;}, 2000);
        //setTimeout(function(){  unlightAllButtons()}, 3000);
		if(setval!="SEARCH" && setval!="") {
		    window.location = "_DO:cmd=send_js&c_name=members&js=setBlockNavigations(true);";
		    setTimeout(function(){
                          run_com_command("members","page=search&mode=search{$extra_vars}&searchterm=" + setval + "&abc=123&mode=do_search");
    		           }, 200);

		}

        nav_performing_block_buttons = false;

		return false;
	}
	function run_com_command(comname,vars) {
		window.location = "_DO:cmd=load_url&c_name=" + comname + "&f_name=fitness/" + comname + ".php?" + vars;
	}
	function run_cmd(str) {
//		alert('wrapper cmd: ' + str);
	}

	function side_bar_letter_pressed(index){
	    //Where index 0 is A, index 1 is B etc.
	    //alert("You pushed: " + String.fromCharCode('A'.charCodeAt(0) + index) + " telling other script...");
	    //window.location = "_DO:cmd=send_js&c_name=" + comname + "&js=run_cmd('" + varstr + "')";
	    window.location = "_DO:cmd=send_js&c_name=members&js=letter_key_pressed_in_wrapper('" + String.fromCharCode('A'.charCodeAt(0) + index) + "');";
	}

    function set_side_bar_letters_visible(isVisible){
        var msg = isVisible ? 'true' : 'false';
        var letters_side_table = document.getElementById('letters_table');
        if(isVisible){
            letters_side_table.style.display = 'block';
        }else{
            letters_side_table.style.display = 'none';
        }
    }
    function exit_overlay(){

        window.location = "_DO:cmd=close_overlay";
    }
    //"close_overlay"
    //margin-left: -6px;

    function set_display_searchCustomersCreateACustomer_image(isDisplayed){
        var searchOrCreateDiv = document.getElementById('searchOrCreateCustomerBanner_div');
        if(isDisplayed){
            searchOrCreateDiv.style.display = 'block';
        }else{
            searchOrCreateDiv.style.display = 'none';
        }
    }
    function go_to_add_customer(){
        nav_performing_block_buttons = true;
        window.location = "_DO:cmd=send_js&c_name=members&js=go_to_add_customer_nav();";
    }

    function blur_from_searchbox(){
        var searchInputField = document.getElementById('search_text_id');
        searchInputField.blur();
    }

    function preload_images(){
    	preload_image = new Image();
    	preload_image.src = 'images/close_X.png';
    	preload_image.src = 'images/close_X_lit.png';
    	preload_image.src = 'images/grey_plus.png';
    	preload_image.src = 'images/grey_plus_lit.png';
    	preload_image.src = 'images/search_icon.png';
    	preload_image.src = 'images/search_icon_lit.png';

    	//HERE Switch all the images on the components then back again immediately.

	}
	var is_blocking_button_touches;
	function disableButtons(){
    	is_blocking_button_touches = true;
	}
	function enableButtons(){
    	is_blocking_button_touches = false;
	}

	function lightAllButtons(){
        var closeButton = document.getElementById('close_button_id');
        //closeButton.style.backgroundImage = "url(images/close_X_lit.png)";
        closeButton.src = 'images/close_X_lit.png';

        var searchButton = document.getElementById('search_button_id');
        //searchButton.style.backgroundImage = "url(images/search_icon_lit.png)";
        searchButton.src = 'images/search_icon_lit.png';

        var addButton = document.getElementById('add_customer_button_id');
        //addButton.style.backgroundImage = "url(images/grey_plus_lit.png)";
        addButton.src = 'images/grey_plus_lit.png';

        //setTimeout(function(){  nav_performing_block_buttons = false;}, 2000);
	}
	function unlightAllButtons(){
        var closeButton = document.getElementById('close_button_id');
        //closeButton.style.backgroundImage = "url(images/close_X.png)";
        closeButton.src = 'images/close_X.png';

        var searchButton = document.getElementById('search_button_id');
        //searchButton.style.backgroundImage = "url(images/search_icon.png)";
        searchButton.src = 'images/search_icon.png';

        var addButton = document.getElementById('add_customer_button_id');
        //addButton.style.backgroundImage = "url(images/grey_plus.png)";
        addButton.src = 'images/grey_plus.png';
	}

	function unlockButtonsDelayed(){
	    setTimeout(function(){ unlightAllButtons();}, 200);
    	setTimeout(function(){ nav_performing_block_buttons = false;}, 200);
	}

	var is_loading_label_hidden = true;
	var is_loading_timer_interval;
	var loading_label_tick_count = 0;
	function tickLoadingLabel(){
    	var myMod = loading_label_tick_count++ % 4;
    	var loadingLabelParagraph = document.getElementById('loading_paragraph_id');
    	var dots;
    	if(myMod == 0){
        	dots = '.';
    	}else if(myMod == 1){
        	dots = '..';
    	}else if(myMod == 2){
        	dots = '...';
    	}else if(myMod == 3){
        	dots = '';
    	}
    	loadingLabelParagraph.innerHTML = 'loading' + dots;
	}
	function setLoadingHidden(isHidden){
	    loading_label_tick_count = 0;
	    if(is_loading_label_hidden === isHidden){ return; }
    	is_loading_label_hidden = isHidden;
    	if(!is_loading_label_hidden){
        	is_loading_timer_interval = this.setInterval(function(){tickLoadingLabel();}, 400);
    	}else{
    	    var loadingLabelParagraph = document.getElementById('loading_paragraph_id');
        	window.clearInterval(is_loading_timer_interval);
        	loadingLabelParagraph.innerHTML = '&nbsp;';
    	}
	}

    // Comment out refresh() during development to allow for refreshing page by simplying clicking on the Members tab.
    // Note: the refresh() function also seems to get called when the PIN lock-out screen happens.

    function refresh() {
        startListeningForKEI();
        return 1;
    }

    /* KEI badge-ins */

    function startListeningForKEI() {
        window.location = "_DO:cmd=startListeningForKEI";

        return 1;
    }

    function stopListeningForKEI() {
        window.location = "_DO:cmd=stopListeningForKEI";

        return 1;
    }

    function handleKEI(string) {
        //alert("KEI=" + string );

        newSignIn(string);

        return 1;
    }

    function newSignIn(badgeId) {
        //run_com_command("members", "page=signinuser&rfid="+ encodeURIComponent(badgeId) +"{$site->appVarsQuerystring()}");
        run_com_command("members", "page=signinuser&rfid="+ encodeURIComponent(badgeId));
    }

    // The changeTitleFor*() were made separate functions in case it gets called by other components.

    function changeTitleForNewMember() {
        document.getElementById("pageTitle").innerHTML = 'New Member';
    }

    function changeTitleForMemberBilling() {
        document.getElementById("pageTitle").innerHTML = 'Member Billing';
    }

    function changeTitleForMemberProfile() {
        document.getElementById("pageTitle").innerHTML = 'Member Profile';
    }

    function changeTitleForMemberHistory() {
        document.getElementById("pageTitle").innerHTML = 'Member History';
    }

    function changeTitleForViewInvoice(orderId) {
        orderId = (typeof orderId !== 'undefined') ? orderId : '(Invoice Number)';
        document.getElementById("pageTitle").innerHTML = 'Viewing Invoice '+ orderId;
    }

    function changeTitleForRecentMemberSignins() {
        document.getElementById("pageTitle").innerHTML = 'Recent Member Sign-ins';
    }

    function changeTitleForRecentMemberSignups() {
        document.getElementById("pageTitle").innerHTML = 'Recent Member Sign-ups';
    }

    function changeTitleForMembershipExpirationDate() {
        document.getElementById("pageTitle").innerHTML = 'Expiring Memberships';
    }

    function changeTitleForCreditCardExpirationDate() {
        document.getElementById("pageTitle").innerHTML = 'Expiring Credit Cards';
    }

    function changeTitleForOutstandingBalances() {
        document.getElementById("pageTitle").innerHTML = 'Outstanding Balances';
    }

    // Save this last page we loaded for the membesr_signups component so we can reload it if we ever get asked to refresh that component.
    var lastLoadedMemberSignupsPage = '';
    var lastLoadedTitleFunction = function () {};

    function loadNewMember(customerId, invoiceId) {
        changeTitleForNewMember();
        window.location = "_DO:cmd=load_url&c_name=members_signins&f_name=fitness/members_signins.php?page=newuser" +"{$site->appVarsQuerystring()}";
    }

    function loadMemberBilling(customerId) {
        changeTitleForMemberBilling();
        window.location = "_DO:cmd=load_url&c_name=members_signins&f_name=fitness/members_signins.php?page=memberbilling&customer_id=" + encodeURIComponent(customerId);
    }

    function loadMemberProfile(customerId) {
        changeTitleForMemberProfile();
        window.location = "_DO:cmd=load_url&c_name=members_signins&f_name=fitness/members_signins.php?page=memberprofile&customer_id=" + encodeURIComponent(customerId);
    }

    function loadMemberHistory(customerId) {
        changeTitleForMemberHistory();
        window.location = "_DO:cmd=load_url&c_name=members_signins&f_name=fitness/members_signins.php?page=memberhistory&customer_id=" + encodeURIComponent(customerId);
    }

    function loadInvoiceInfo(customerId, invoiceId) {
        window.location = "_DO:cmd=load_url&c_name=members_signins&f_name=fitness/members_signins.php?page=invoice&customer_id=" + encodeURIComponent(customerId) +"&invoice_id="+ encodeURIComponent(invoiceId) +"{$site->appVarsQuerystring()}";
    }

    function loadRecentMemberSignins() {
        unlightAllButtons();
        lastLoadedTitleFunction = changeTitleForRecentMemberSignins;
        lastLoadedTitleFunction();
        lastLoadedMemberSignupsPage = "_DO:cmd=load_url&c_name=members_signins&f_name=fitness/members_signins.php";
        window.location = lastLoadedMemberSignupsPage;
    }

    function loadRecentMemberSignups() {
        unlightAllButtons();
        lastLoadedTitleFunction = changeTitleForRecentMemberSignups;
        lastLoadedTitleFunction();
        lastLoadedMemberSignupsPage = "_DO:cmd=load_url&c_name=members_signins&f_name=fitness/members_signins.php?page=recentsignups";
        window.location = lastLoadedMemberSignupsPage;
    }

    function loadMembershipExpirationDate() {
        unlightAllButtons();
        lastLoadedTitleFunction = changeTitleForMembershipExpirationDate;
        lastLoadedTitleFunction();
        lastLoadedMemberSignupsPage = "_DO:cmd=load_url&c_name=members_signins&f_name=fitness/members_signins.php?page=expirations";
        window.location = lastLoadedMemberSignupsPage;
    }

    function loadCreditCardExpirationDate() {
        unlightAllButtons();
        lastLoadedTitleFunction = changeTitleForCreditCardExpirationDate;
        lastLoadedTitleFunction();
        lastLoadedMemberSignupsPage = "_DO:cmd=load_url&c_name=members_signins&f_name=fitness/members_signins.php?page=ccexpirations";
        window.location = lastLoadedMemberSignupsPage;
    }

    function loadMembershipOutstandingBalances() {
        unlightAllButtons();
        lastLoadedTitleFunction = changeTitleForOutstandingBalances;
        lastLoadedTitleFunction();
        lastLoadedMemberSignupsPage = "_DO:cmd=load_url&c_name=members_signins&f_name=fitness/members_signins.php?page=outstandingbalances";
        window.location = lastLoadedMemberSignupsPage;
    }

    function reloadLastLoadedMemberSignupsPage() {
        unlightAllButtons();

        // Default to the Recent Sign-ins screen, since that's the only screen they've seen if they haven't used any of the Home Buttons.
        if (lastLoadedMemberSignupsPage == "") {
            lastLoadedTitleFunction = changeTitleForRecentMemberSignins;
            lastLoadedMemberSignupsPage = "_DO:cmd=load_url&c_name=members_signins&f_name=fitness/members_signins.php";  // Tack on timestamp to keep server from caching
        }

        lastLoadedTitleFunction();
        window.location = lastLoadedMemberSignupsPage;
    }

    function afterNewBadgeSet(customerId) {
        startListeningForKEI();
        setTimeout( function(){ loadMemberProfile(customerId) }, 100 );
        return 1;
    }

</script>
</head>
<body onLoad="startListeningForKEI()">

<form name="submittingForm" method="post" action="" onSubmit="blur_from_searchbox(); return doSearch();">

<table border="0" style="position:absolute; top:0px; left:0px;">
<tr>
<td>
    <div style="margin-left: 10px">
    <!- CLOSE BUTTON ->
    {$site->html['closeSearchButton']}
    </div>
</td>
<td width="360">
<!-  SEARCH TEXT INPUTs WRAPPING IMAGE DIV  ->
<div id="search_text_wrapping_div_id" style="margin-left: 5px; background: url(images/search_box.png) no-repeat; background-size:90%; height:50px; width:315px; border: 0px solid gray; margin-top: 20px">
    <table>
        <tr>
            <td>
                <!-  SEARCH TEXT INPUT  ->
                <input id="search_text_id" name="search_input_field" type="text" placeholder="Search for Members" onSubmit="doSearch();"  onfocus="showCloseButton()" style="width:210px; height:23px; margin-top: -5px; margin-left: 0px; font-size:16px; background-color: rgba(0,0,0,0.0); border:0px"/>
            </td>
            <td valign="top" align="left">
                <!-  DO SEARCH BUTTON  (magnifying glass) ->
                &nbsp;
                {$site->html['findCustomerButton']}
            </td>
            <td>
                <!- ADD CUSTOMER BUTTON (plus sign) ->
                {$site->html['addCustomerButton']}
            </td>
        </tr>
    </table>
</div>

</td>
</tr>
</table>

<h2 id="pageTitle" style="position:absolute; top:0px; left:400px;">Recent Member Sign-ins</h2>

</form>

<script type='text/javascript'>


    function style_button_press_state(elem, isPressed){
        if(   (elem.id == 'search_button_id' || elem.id == 'search_button_div_id')&& document.getElementById("search_text_id").value == ''){
            return;
        }
        //alert(elem.id);
        var imageName;
        var elementToChange = document.getElementById( elem.id.replace("_div", "") );
        if(elementToChange.id == 'close_button_id'){        imageName = isPressed ? 'images/close_X_lit.png'     : 'images/close_X.png'; }
        if(elementToChange.id == 'search_button_id'){       imageName = isPressed ? 'images/search_icon_lit.png' : 'images/search_icon.png'; }
        if(elementToChange.id == 'add_customer_button_id'){ imageName = isPressed ? 'images/grey_plus_lit.png'   : 'images/grey_plus.png'; }
        if(isPressed == false){
            //imageName = "url(" + imageName + ")";
            //elem.style.backgroundImage = imageName;
            elementToChange.src = imageName;
        }else{
            //imageName = "url(" + imageName + ")";
            //elem.style.backgroundImage = imageName;
            elementToChange.src = imageName;
        }
    }

    var closeButton = document.getElementById("close_button_id");
    var searchInput = document.getElementById("search_text_id");

    var nav_performing_block_buttons = false;
    function style_button_press_start(event){

        //Handle the actual event
        if(!nav_performing_block_buttons){
            //nav_performing_block_buttons = true;
            style_button_press_state(this, true);

            if(this.id == 'add_customer_button_id'   || this.id == 'add_customer_button_div_id'){
                ////go_to_add_customer();
                // Load New Member creation screen in the members component.
                loadNewMember();
            }
            else if( this.id == 'close_button_id'    || this.id == 'close_button_div_id'){
                ////exit_overlay();

                // Blank out search field.
                searchInput.value = "";

                // Hide close button.
                closeButton.style.visibility = "hidden";

                // Get rid of search results by reloading page in members component.
                ///setTimeout( function() { run_com_command("members",""); }, 200);
                run_com_command("members","");

            }
            else if( (this.id == 'search_button_id'  || this.id == 'search_button_div_id') && document.getElementById("search_text_id").value != '' ){
                doSearch();
            }
        }
    }
    function style_button_press_end(event){
        style_button_press_state(this, false); 
    }
    //Button handlers
    var closeButton = document.getElementById('close_button_id');
    closeButton.addEventListener('touchstart', style_button_press_start);
    closeButton.addEventListener('touchmove' , style_button_press_start);
    var searchButton = document.getElementById('search_button_id');
    searchButton.addEventListener('touchstart', style_button_press_start);
    searchButton.addEventListener('touchmove' , style_button_press_start);
    var addButton = document.getElementById('add_customer_button_id');
    addButton.addEventListener('touchstart', style_button_press_start);
    addButton.addEventListener('touchmove' , style_button_press_start);
    //Button padding div handlers
    var closeButtonDiv = document.getElementById('close_button_div_id');
    closeButtonDiv.addEventListener('touchstart', style_button_press_start);
    closeButtonDiv.addEventListener('touchmove' , style_button_press_start);
    var searchButtonDiv = document.getElementById('search_button_div_id');
    searchButtonDiv.addEventListener('touchstart', style_button_press_start);
    searchButtonDiv.addEventListener('touchmove' , style_button_press_start);
    var addButtonDiv = document.getElementById('add_customer_button_div_id');
    addButtonDiv.addEventListener('touchstart', style_button_press_start);
    addButtonDiv.addEventListener('touchmove' , style_button_press_start);

</script>



<script type='text/javascript'>
   //var letters_side_table = document.getElementById('letters_table');
   //letters_side_table.style.display = 'none';
   set_side_bar_letters_visible(false);

   function handle_letter_table_wrapper_touch(event){
	   var touches = event.touches;
	   var touch = touches[0];
	   var y = touch.clientY - this.offsetTop;
	   y /= this.offsetHeight;
	   var letter = Math.min(Math.round(y * 26), 25);
	   side_bar_letter_pressed(letter) ;
	   return false;
   }
   /**var lettersWrapper = document.getElementById('letters_table_wrapper');
   lettersWrapper.addEventListener('touchmove',  handle_letter_table_wrapper_touch);
   lettersWrapper.addEventListener('touchstart', handle_letter_table_wrapper_touch);
   lettersWrapper.addEventListener('touchend',   handle_letter_table_wrapper_touch);**/

   preload_images();
   setTimeout(function(){
       lightAllButtons();
       setTimeout(function(){
	       unlightAllButtons();
       },1);
   }
   , 20);
</script>

</body>
</html>

HTML;


?>
