<?php
	require_once(dirname(__FILE__) . "/../comconnect.php");

class MembersCard {
	// Site vars
	var $page;
	var $title;
	var $msg;

    // App vars posted to every component.
    var $app;

	// Page content vars
	var $html;
	var $css;
	var $js;

	// Fitness component-specific vars
	var $rfid;
	var $customer_id;

	// Customer component-specific vars
	var $med_customer_id;
	var $med_posted;
	var $memberInfo;

	// Page-specific vars
	var $usersWithSameRfid;
	var $overrideConflict;

	public function __construct()
	{
		// Site vars
		$this->page = (isset($_REQUEST["page"])) ? $_REQUEST["page"] : "";

        // Standard vars posted to every component from the app.
        $this->app["comp_name"] = (isset($_REQUEST["comp_name"])) ? $_REQUEST["comp_name"] : "";
        $this->app["server_id"] = (isset($_REQUEST["server_id"])) ? $_REQUEST["server_id"] : "";
        $this->app["server_name"] = (isset($_REQUEST["server_name"])) ? $_REQUEST["server_name"] : "";
        $this->app["loc_id"] = (isset($_REQUEST["loc_id"])) ? $_REQUEST["loc_id"] : "";
        $this->app["tab_name"] = (isset($_REQUEST["tab_name"])) ? $_REQUEST["tab_name"] : "";
        $this->app["cc"] = (isset($_REQUEST["cc"])) ? $_REQUEST["cc"] : "";
        $this->app["dn"] = (isset($_REQUEST["dn"])) ? $_REQUEST["dn"] : "";

        // Location info, once we have the dataname and location ID.
        if ( isset( $_SESSION['timezone'] ) ) {
        	date_default_timezone_set( $_SESSION['timezone'] );
        }
        else if ( empty( $_SESSION['timezone'] ) && isset( $this->app["dn"] ) && isset( $this->app["loc_id"] )  ) {
        	$this->location_info = get_location_info( $this->app["dn"], $this->app["loc_id"] );  // From core_functions.php

	        // Default timezone, based on CP settings.
        	if ( !empty( $this->location_info['timezone'] ) ) {
	        	date_default_timezone_set( $this->location_info['timezone'] );
	        	$_SESSION['timezone'] = $this->location_info['timezone'];
        	}
        }

		// Fitness component-specific vars
		$this->rfid = (isset($_REQUEST["rfid"])) ? $_REQUEST["rfid"] : "";
		$this->customer_id = (isset($_REQUEST["customer_id"])) ? $_REQUEST["customer_id"] : "";

		if ($this->rfid != "") {
			// Strip the newline that may have been tacked on to the end from the KEI.
			$this->rfid = str_replace("\n", "", $this->rfid);
		}

		$this->usersWithSameRfid = array();
		$this->overrideConflict = '';

	}

	public function fetchUsersWithSameRfid() {
		// Collect the number of users with that same rfid so we can display a warning message.
		$sameRfidQuery = lavu_query( "select * from `med_customers` where `rfid`='[rfid]'", array('rfid' => $this->rfid) );
		if ( $sameRfidQuery !== FALSE ) {
			while ( $row_read = mysqli_fetch_assoc($sameRfidQuery) ) {
				$this->usersWithSameRfid[$row_read['id']] = $row_read['f_name'] .' '. $row_read['l_name'];
			}
		}
		error_log( 'LAVUFITDEBUG: '. $this->usersWithSameRfid );  //debug
		return $this->usersWithSameRfid;
	}

	public function handleUsersWithSameRfid()
	{
		if ( ($this->page != 'setcard') || ($this->page == 'setcard' && !empty($_REQUEST['overrideconflict'])) ) {
			return;
		}

		$this->usersWithSameRfid = $this->fetchUsersWithSameRfid();

		error_log( "LAVUFITDEBUG: count(usersWithSameRfid)=". count($this->usersWithSameRfid) );  //debug

		if ( count($this->usersWithSameRfid) >= 1 ) {
			$this->page = 'conflict';
		}
	}

	public function setNewCardOnUser() {
		$clearConflictingRfidsResult = lavu_query( "update `med_customers` set `rfid` = '' where `rfid` = '[rfid]'", array('rfid' => $this->rfid) );
		if (!$clearConflictingRfidsResult) {
			die( "failed to clear existing RFIDs for rfid={$this->rfid} for customer_id={$this->customer_id}" );
		}

		// Set the new card's rfid on the user's database record.
		$newRfidUpdateResult = lavu_query( "update `med_customers` set `rfid` = '[rfid]' where `id` = '[id]'", array('rfid' => $this->rfid, 'id' => $this->customer_id) );
		if (!$newRfidUpdateResult) {
			die( "failed to set rfid={$this->rfid} for customer_id={$this->customer_id}" );
		}
	}

	public function insertMedActionLog( $customerId, $action, $orderId ) {
		// Set device_time based on the location's time zone information.
		$device_time = date("Y-m-d H:i:s");

		// Insert sign-in - we will already have the user's info if rfid was passed in.
		$query_vars = array(
			"action" => $action,
			"customer_id" => $customerId,
			"order_id" => $orderId,
			"restaurant_time" => date("Y-m-d H:i:s"),
			"user_id" => $this->app["server_id"],
			"loc_id" => $this->app["loc_id"],
			"device_time" => $device_time
		);
		$insert_query = "insert into `med_action_log` (`action`, `customer_id`, `order_id`, `restaurant_time`, `user_id`, `loc_id`, `device_time`) values ('[action]', '[customer_id]', '[order_id]', '[restaurant_time]', '[user_id]', '[loc_id]', '[device_time]')";
		$success = lavu_query($insert_query,$query_vars);
		if ($success) {
			$row_updated = lavu_insert_id();
			//$msg = "inserted new row: $row_updated";
			$this->msg = "inserted new row: $row_updated";
		}
		else {
			//$msg = "failed to insert new row";
			$this->msg = "failed to insert new row";
		}

		return $success;
	}

}


/* Site Logic */

$site = new MembersCard();

$site->handleUsersWithSameRfid();

// Routing Logic

if ($site->page == 'setcard') {

	$site->setNewCardOnUser();

	$site->insertMedActionLog( $site->customer_id, 'Member Badge Assigned', 'fitness_assignedrfid' );

	$site->js['run'] = <<<JS

	stopListeningForKEI();
	showBadgeIconAsSet("{$site->rfid}");
	setTimeout(closeOverlay, 800);

JS;

	$site->html['body'] = <<<HTML

<br>
<br>
<h1>The badge has been set.</h1>

HTML;

}
else if ( $site->page == 'conflict' ) {

	$site->overrideConflict = '1';

	$conflictingUsersNames = join(', ', $site->usersWithSameRfid);

	$site->html['body'] = <<<HTML

<br>
<br>
<h1>Badge already assigned<br>to <em>{$conflictingUsersNames}</em>.</h1>

<h1>Scan the badge again to reassign it to the current member instead.</h1>

HTML;

}
else {

	$site->html['body'] = <<<HTML

<br>
<br>
<h1>Scan the member's<br>new badge now.</h1>

HTML;

}


/* Site html */

echo <<<HTML

<html>
<head>
<style>
h1 {
	font-family: Verdana, Arial;
	font-size: 18px;
	color: #bbbbbb;
	text-align: center;
	padding: 5px;
}
p {
	font-family: Verdana, Arial;
	font-size: 12px;
	color: #bbbbbb;
	text-align: center;
	padding: 5px;
}
em {
	font-style: normal;
	color: #cccccc;
}
</style>

<script>

function showBadgeIconAsSet(rfid) {
	// Change the badge icon to be grayed out, if one is shown.
	window.location = '_DO:cmd=send_js&c_name=members&js=showBadgeIconAsSet("'+ rfid +'")';
}

function closeOverlay() {
	// Close the set card overlay.
	window.location = "_DO:cmd=close_overlay&c_name=members_wrapper&js=afterNewBadgeSet('{$site->customer_id}')";
}

/* KEI badge-ins */

function startListeningForKEI() {
    window.location = "_DO:cmd=startListeningForKEI";
}

function stopListeningForKEI() {
    window.location = "_DO:cmd=stopListeningForKEI";
}

function handleKEI(string) {
    //alert("KEI=" + string );  /** debug **/

	// Reload the page with the querystring variables to make the page update the user's data with the new RFID card.
    window.location = "?page=setcard&overrideconflict={$site->overrideConflict}&customer_id="+ encodeURIComponent("{$_REQUEST['customer_id']}") +"&rfid="+ encodeURIComponent(string);

    return 1;
}

</script>
</head>

<body onload="startListeningForKEI()">

{$site->html['body']}

<script>
{$site->js['run']}
</script>

</body>

</html>

HTML;
?>
