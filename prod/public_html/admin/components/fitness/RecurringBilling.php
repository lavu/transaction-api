<?php

$upone = (strstr(__FILE__, '/dev/') ? '/..' : '');
require_once(dirname(__FILE__) . $upone . "/../../cp/resources/core_functions.php");
require_once(dirname(__FILE__) . $upone . "/../../components/order_addons/customer_functions.php");  // For get_med_customer_fields() but include this broke everything
require_once(dirname(__FILE__) . $upone . "/../../lib/gateway_lib/usaepay.php");
require_once('FitnessWaiver.php');


class RecurringBilling
{
	var $test = false;
	var $debug = false;
	var $bool2str = array( true => 'true', false => 'false' );

	var $dataname;
	var $locationInfo;

	/**
	 * Function getLocationInfo()
	 *
	 * Get the location info associative array.  It just sets the member variable $locationInfo using core_functions.php's
	 * get_location_info(), it it wasn't already set, and then returns that array.
     *
	 * @param string $locationId - the location ID.
     *
	 * @return array $locationInfo - location info associative array.
	 */

	public function getLocationInfo($dataname, $locationId)
	{
		// Complain about any missing required input parameters.
		if ( empty($dataname) || empty($locationId) ) {
			$this->errorLog( __METHOD__ . " missing one or more required input parameter(s) dataname={$dataname} locationId={$locationId}" );
		}

		// Only populate $locationInfo if it isn't already set.
		if ( !isset($locationInfo) ) {
			$locationInfo = get_location_info($dataname, $locationId);
		}

		return $locationInfo;
	}


	/**
	 * Function createInvoices()
	 *
	 * Create all recurring billing invoices for today.
     *
	 */

	public function createInvoices()
	{
		$this->createInvoicesForDate( date( 'j', time() ) );
	}

	/**
	 * Function createInvoicesForDate()
	 *
	 * Create all recurring billing invoices for a specific invoice day of the month.
	 *
	 * @param string $dayOfMonth - the invoice day of month used to find all billing profiles for which invoices will be created.
     *
	 */

	public function createInvoicesForDate( $dayOfMonth )
	{
		$this->log( "Creating invoices for dayOfMonth={$dayOfMonth}" );

		$billingProfiles = $this->getActiveBillingProfiles( array( 'bill_event_offset' => $dayOfMonth ) );

		foreach ( $billingProfiles as $billingProfile ) {
			$this->createInvoiceForBillingProfile( $billingProfile );
		}
	}

	/**
	 * Function createInvoicesForNextBillDate()
	 *
	 * Create all recurring billing invoices that fall on or before a specific next_bill_date.
	 *
	 * @param string $nextBillDate - the next_bill_date used to find all billing profiles for which invoices will be created.
     *
	 * @return array $createdInvoices - array of the created invoice numbers
	 */

	public function createInvoicesForNextBillDate( $nextBillDate )
	{
		$this->log( "Creating invoices for nextBillDate={$nextBillDate}" );

		$billingProfiles = $this->getActiveBillingProfiles( array( 'next_bill_date' => $nextBillDate ) );

		$createdInvoices = array();
		foreach ( $billingProfiles as $billingProfile ) {
			$createdInvoices[] = $this->createInvoiceForBillingProfile( $billingProfile );
		}

		return $createdInvoices;
	}

	/**
	 * Function createInvoicesForBillingProfileId()
	 *
	 * Create an invoice for a specific recurring billing profile - regardless of their invoice day of the month.
     *
	 * @param string $billingProfileTableId - the med_biling table ID for the ARB.
     *
	 * @return array $createdInvoices - array of the created invoice numbers
	 */

	public function createInvoicesForBillingProfileId( $billingProfileTableId )
	{
		$billingProfiles = $this->getActiveBillingProfiles( array( 'id' => $billingProfileTableId ) );

		//$this->debugPrint( $billingProfiles );

		$createdInvoices = array();
		foreach ( $billingProfiles as $billingProfile ) {
			$createdInvoices[] = $this->createInvoiceForBillingProfile( $billingProfile );
		}

		return $createdInvoices;
	}

	/**
	 * Function createInvoiceForBillingProfile()
	 *
	 * 
	 * Our "invoices" are really just rows in the orders tables.
     *
	 * @param array $billingProfile
     *
	 * @return string $invoiceNumber - Invoice number of created invoice or blank for none
	 */

	public function createInvoiceForBillingProfile( $billingProfile )
	{
		if ( $billingProfile['status'] != 'active' ) {
			$this->log( "Skipping creating invoice for inactive billing profile ID={$billingProfile['id']}" );
			return '';
		}

		// Parse the invoice template XML from the billing profile.
		$invoiceTemplate = simplexml_load_string( $billingProfile['invoice_template'] );

		///$this->debugPrint( $invoiceTemplate );

		if ( empty($invoiceTemplate) ) {
			$this->errorLog( "Missing invoice_template for billingProfileId={$billingProfile['id']}" );
			return '';
		}

		// The invoice number defaults to 1 but is changed to the actual next number in the sequence if
		// they've already created an invoice and saved that order_id in med_billing.last_billed_order_id.
		$invoiceNum = 1;
		if ( $billingProfile['last_billed_order_id'] != '' ) {
			preg_match( '/[-](\d{3})$/', $billingProfile['last_billed_order_id'], $matches );
			$invoiceNum = (int) $matches[1];
			++$invoiceNum;
		}

		$orderId = (string) $invoiceTemplate['order_id'];
		$orderId = str_replace( 'x', $billingProfile['order_id'], $orderId );
		$orderId = str_replace( 'nnn', sprintf( '%03d', $invoiceNum ), $orderId );

		// Check to make sure an invoice doesn't already exist for this recurring biling charge

		$this->debugLog( "Checking for pre-existing invoice created for {$orderId}" );

		$orderQuerySql = "select * from `orders` where `order_id` = '[order_id]' order by id";
		$orderQueryVars = array( 'order_id' => $orderId );
		$orderQueryResults = lavu_query( $orderQuerySql, $orderQueryVars );
		$orderQueryResultsNumRows = mysqli_num_rows( $orderQueryResults );

		$this->debugLog( "orderQueryResultsNumRows={$orderQueryResultsNumRows}" );

		if ( $orderQueryResultsNumRows > 0 ) {
			$this->errorLog( "Invoice {$orderId} already exists for billingProfileId={$billingProfile['id']}" );
			return '';
		}

		//----------------------------------------- Add the order

		// Get the column name of the email field in the med_customers table.
		$emailColumnName = 'email';  // Default to the column name "email", to support Lavu Give convention.
		foreach ( get_med_customer_fields() as $fieldIndex => $fieldInfo ) {
			if ( $fieldInfo['title'] == 'Email' || $fieldInfo['title'] == 'E-mail' || $fieldInfo['title'] == 'Email Address' || $fieldInfo['title'] == 'E-mail Address' ) {
				$emailColumnName = $fieldInfo['column'];
			}
		}

		// Get the user's info from med_customers so we can get their current name for the original_id.
		$medCustomerId = $billingProfile['customer_id'];
		$medCustomerQuerySql = "select `f_name`, `l_name`, `{$emailColumnName}` from `med_customers` where `id` = '{$medCustomerId}'";
		$medCustomerQueryResults = lavu_query( $medCustomerQuerySql );
		if ( $medCustomerQueryResults === FALSE ) die( "Query error with medCustomerQuerySql={$medCustomerQuerySql}: ". lavu_dberror() );
		$medCustomerQueryResultsNumRows = mysqli_num_rows( $medCustomerQueryResults );
		if ( $medCustomerQueryResults <= 0 ) {
			$this->errorLog( "Didn't find any locations rows" );
			return;
		}
		$medCustomer = mysqli_fetch_assoc( $medCustomerQueryResults );

		$fullName = $medCustomer['f_name'] .' '. $medCustomer['l_name'];
		$originalId = (isset($invoiceTemplate['original_id']) && !empty($invoiceTemplate['original_id']))? (string) $invoiceTemplate['original_id'] : '19|o|Member|o|0|o|0';  // default to the original_id template for fitness|tithe if the XML template doesn't have it
		$originalId = str_replace( '|o|Member|o|0|o|0', "|o|Member|o|{$fullName}|o|{$medCustomerId}", $originalId );

		$ordersInsertVars = array(
			'tablename' => 'Invoice',
			'location_id' => $billingProfile['location_id'],
			'location' => $this->getLocationName( $billingProfile['location_id'] ),
			'subtotal' => (string) $invoiceTemplate['subtotal'],
			'taxrate' => (string) $invoiceTemplate['tax_rate'],
			'tax' => (string) $invoiceTemplate['tax'],
			'total' => (string) $invoiceTemplate['total'],
			'cashier' => 'customers_billing',
			'server' => 'customers_billing',
			'register' => 'customers_billing',
			'register_name' => 'customers_billing',
			'email' => $medCustomers[$emailColumnName],
			'server_id' => '',
			'cash_paid' => '',
			'change_amount' => '',
			'cash_applied' => '',
			'card_paid' => '',
			'gift_certificate' => '',
			'guests' => '1',
			'discount' => '',
			'discount_type' => '',
			'discount_value' => '',
			'discount_sh' => '',
			'original_id' => '',
			'order_id' => $orderId,
			'original_id' => $originalId
		);
		$dbColumnNames = array_keys( $ordersInsertVars );
		$ordersInsertCols = implode( '`, `', $dbColumnNames );
		$ordersInsertVals = implode( "]', '[", $dbColumnNames );
		$ordersInsertSql = "insert into `orders` (`opened`, `closed`, `{$ordersInsertCols}`) values (now(), '0000-00-00 00:00:00', '[{$ordersInsertVals}]')";
		$ordersInsertOk = lavu_query( $ordersInsertSql, $ordersInsertVars );
		if ( !$ordersInsertOk ) {
			$this->errorLog( "Failed to insert row into orders:". lavu_dberror() );
			return '';
		}

		//$this->debugPrint( $ordersInsertSql );


		//----------------------------------------- Add the order contents

		foreach ( $invoiceTemplate->item as $invoiceItemTemplate ) {

			$invoiceItemTemplate = (array) $invoiceItemTemplate;

			$this->debugPrint( $invoiceItemTemplate );

			// TO DO : check to make sure that these invoice items don't already exist for this recurring biling charge

			$orderContentsInsertVars = array(
				'loc_id' => $billingProfile['location_id'],
				'sent' => '0',
				'order_id' => $orderId,
				'item' => (string) $invoiceItemTemplate['name'],
				'price' => (string) $invoiceItemTemplate['amount'],
				'quantity' => (string) $invoiceItemTemplate['quantity'],
				'options' => (string) $invoiceItemTemplate['options'],
				'special' => '',
				'modify_price' => '',
				'check' => '1',
				'seat' => '1',
				'item_id' => (string) $invoiceItemTemplate['item_id'],
				'printer' => '0',
				'apply_taxrate' => (string) $invoiceItemTemplate['tax_rate'],
				'tax_rate1' => (string) $invoiceItemTemplate['tax_rate'],
				'course' => '1',
				'subtotal' => (string) $invoiceItemTemplate['subtotal'],
				'discount_amount' => '',
				'discount_value' => '',
				'discount_type' => '',
				'after_discount' => '',
				'subtotal_with_mods' => (string) $invoiceItemTemplate['subtotal'],
				'tax_amount' => (string) $invoiceItemTemplate['tax'],
				'notes' => '',
				'total_with_tax' => (string) $invoiceItemTemplate['total'],
				'after_gratuity' => '',
				'void' => '0',
				'discount_id' => '',
				'server_time' => date("Y-m-d H:i:s"),
				'device_time' => date("Y-m-d H:i:s"),
				'hidden_data4' => (string) $invoiceItemTemplate['hidden_data4'],  // TO DO : finalize
				'super_group_id' => (string) $invoiceItemTemplate['super_group_id']
			);

			$this->debugPrint( $orderContentsInsertVars );

			$dbColumnNames = array_keys( $orderContentsInsertVars );
			$orderContentsInsertCols = implode( '`, `', $dbColumnNames );
			$orderContentsInsertVals = implode( "]', '[", $dbColumnNames );
			$orderContentsInsertSql = "insert into `order_contents` (`{$orderContentsInsertCols}`) values ('[{$orderContentsInsertVals}]')";
			$orderContentsInsertOk = lavu_query( $orderContentsInsertSql, $orderContentsInsertVars );
			if ( !$orderContentsInsertOk ) {
				$this->errorLog( "Failed to insert row into order_contents:". lavu_dberror() );
				return '';
			}

			$this->debugPrint( $orderContentsInsertSql );
		}


		//----------------------------------------- Invoice creation successful

		// Update last_billed_order_id value in the ARB table now that we've created the next invoice in the sequence.
		$nextBillDate = $this->calculateNextBillDate( $billingProfile['next_bill_date'], $billingProfile['start_date'], $billingProfile['bill_frequency'], $billingProfile['bill_event_offset'] );
		$lastBilledOrderUpdate = lavu_query( "update `med_billing` set last_billed_order_id = '[new_last_billed_order_id]', next_bill_date='[next_bill_date]' where id = '[id]' and last_billed_order_id = '[old_last_billed_order_id]'", array( 'id' => $billingProfile['id'], 'old_last_billed_order_id' => $billingProfile['last_billed_order_id'], 'new_last_billed_order_id' => $orderId, 'next_bill_date' => $nextBillDate ) );
		if ( $lastBilledOrderUpdate === FALSE ) {
			$this->errorLog( "Failed to update last_billed_order_id to {$orderId} or last_bill_date to {$nextBillDate} for id={$billingProfile['id']}:". lavu_dberror() );
			return '';
		}

		return $orderId;

	}

	/**
	 * Function payInvoicesForBillDate()
	 *
	 * Pay all unpaid invoices for all recurring billing profiles with a specific invoice day of the month.
	 *
	 * @param string $dayOfMonth - the invoice day of month used to find all billing profiles for which invoices will be paid.
     *
	 */

	public function payInvoicesForBillDate( $dayOfMonth )
	{
		$billingProfiles = $this->getActiveBillingProfiles( array( 'bill_event_offset' => $dayOfMonth ) );

		//$this->debugPrint( $billingProfiles );

		foreach ( $billingProfiles as $billingProfile ) {
			foreach ( $this->getUnpaidInvoiceIdsForBillingProfile( $billingProfile ) as $orderId ) {
				$this->payInvoice( $orderId, $billingProfile );
			}
		}
	}

	/**
	 * Function payInvoicesForBillingProfileId()
	 *
	 * Pay any unpaid invoices for a specific recurring billing profile - regardless of their invoice day of the month.
     *
	 * @param string $dataname
	 * @param string $billingProfileTableId - the database table ID for the billing profile for which we want to pay invoices.
     *
	 */

	public function payInvoicesForBillingProfileId( $dataname, $billingProfileTableId )
	{
		$this->dataname = $dataname;

		$billingProfiles = $this->getActiveBillingProfiles( array( 'id' => $billingProfileTableId  ) );

		$this->debugPrint( $billingProfiles );

		foreach ( $billingProfiles as $billingProfile ) {
			$unpaidInvoices = $this->getUnpaidInvoiceIdsForBillingProfile( $billingProfile );

			$this->errorLog( 'Found '. count( $unpaidInvoices ) ." unpaid invoice(s) for billingProfileId={$billingProfileTableId}" );

			foreach ( $unpaidInvoices as $orderId ) {
				$this->payInvoice( $orderId, $billingProfile );
			}
		}
	}

	/**
	 * Function payInvoice()
	 *
	 * Pay the current invoice credit card transactions for outstanding recurring billing invoices.
     *
	 * @param string $orderId - The order id for the invoice to pay.
	 */

	public function payInvoice( $orderId, $billingProfile )
	{
		/* Load invoice and run credit card transaction */

		$invoice = $this->getInvoice( $orderId );

		if ( $invoice === null ) {
			$this->errorLog( "Couldn't load invoice for orderId={$orderId} billingProfileId={$billingProfile['id']}" );
			return;
		}

		// Only attempt credit card transactions when we have a credit card on file.
		if ( $billingProfile['cc_ref_num'] != '' ) {

			$invoicePaid = $this->runCreditCardTxn( $orderId, $invoice, $billingProfile, $ccTxnResults );

			if ( !$invoicePaid ) {
				$this->errorLog( "Credit card transaction failed for orderId={$orderId} billingProfileId={$billingProfile['id']}" );
				return;
			}

			/* Save record of the payment and mark the invoice as paid. */

			$paymentRecordInserted = $this->insertPaymentRecord( $orderId, $billingProfile, $ccTxnResults );

			if ( !$paymentRecordInserted ) {
				$this->errorLog( "Payment record insert failed for orderId={$orderId} billingProfileId={$billingProfile['id']}" );
				return;
			}

			$this->debugPrint( $ccTxnResults );

			$invoiceMarkedPaid = $this->updateInvoiceAsPaid( $orderId, $billingProfile, $ccTxnResults );

			if ( !$invoiceMarkedPaid ) {
				$this->errorLog( "Invoice could not be updated as paid for orderId={$orderId} billingProfileId={$billingProfile['id']}" );
				return;
			}
		}
	}

	/**
	 * Function runCreditCardTxn()
	 *
	 * Run the credit card transaction to pay an invoice using the credit card on file.
     *
	 * @param string $orderId - The order id for the invoice to pay.
	 */

	public function runCreditCardTxn( $orderId, $invoice, $billingProfile, &$ccTxnResults )
	{

		$usaEpay = new umTransaction;
		$usaEpay->usesandbox = $this->test;

		$integrationInfo = get_integration_from_location( $billingProfile['location_id'], "poslavu_{$this->dataname}_db" );

		$usaEpay->key = $integrationInfo['integration1'];  // was '_5wJbi8Za37Oz946z9eNY4TrNl3wBJN5'
		$usaEpay->pin = $integrationInfo['integration2'];  // was '1234'
		$usaEpay->ip = $_SERVER['REMOTE_ADDR'];
		$usaEpay->amount = $this->getUnpaidAmountOnInvoice( $invoice );
		$usaEpay->invoice = $orderId;
		$usaEpay->description = $this->getLocationName( $billingProfile['location_id'] ) .' '. $billingProfile['name'];
		$usaEpay->refnum = $billingProfile['cc_ref_num'];

		$ccTxnOk = $usaEpay->ProcessQuickSale();

		if ( $ccTxnOk ) {
			// TO DO : write history events or something
			$this->log( "Ran recurring credit card transaction of {$usaEpay->amount} for invoice {$orderId} from billingProfileId={$billingProfile['id']}" );

		}
		else {
			$this->errorLog( "Credit card processing failed for orderId={$orderId} billingProfileId={$billingProfile['id']} result={$usaEpay->result} error={$usaEpay->error}" );
		}

		// Pass the USAePay Library objects out of this functions so we can work with it if we need to.
		$ccTxnResults = $usaEpay;

		return $ccTxnOk;
	}


	/**
	 * Function calculateNextBillDate()
	 *
	 * Calculate the next bill date after the reference date based on the provided Start Date, Bill Frequency, and Bill Event Offset.
     *
	 * @param string $referenceDate - this is the date (in YYYY-mm-dd format) after which we're calculating the next bill date
	 * @param string $startDate - arb start_date in YYYY-mm-dd format
	 * @param string $billFrequency - arb bill_frequency: daily|monthly|weekly|bi-weekly|yearly
	 * @param string $billEventOffset - string of a the arb bill_event_date integer 
     *
	 * @return string $firstBillDate - in YYYY-mm-dd format
	 */

	public function calculateNextBillDate( $referenceDate, $startDate, $billFrequency, $billEventOffset )
	{
		$nextBillDate = '';

		if ( empty( $startDate ) || empty( $billFrequency ) || empty( $billEventOffset ) ) {
			$this->errorLog( "Missing one or more required input parameter(s) startDate={$startDate} billFrequency={$billFrequency} billEventOffset={$billEventOffset}" );
			return $nextBillDate;
		}

		$dayOfTheWeekMap = array('Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday');

		// Make sure the reference date takes place after the arb start_date, otherwise just have to use the arb start_date.
		$startDateTimestamp = strtotime( $startDate );
		$referenceDateTimestamp = strtotime( $referenceDate );
		$refDateToUseTimestamp = ($referenceDateTimestamp < $startDateTimestamp) ? $startDateTimestamp : $referenceDateTimestamp;
		$refDateToUseComponents = getdate( $refDateToUseTimestamp );
		$refDateToUse = date( 'Y-m-d', $refDateToUseTimestamp );

		if ( $billFrequency == 'monthly' ) {
			// We try to create the next_bill_date by starting out with the arb start_date and replacing the day of month with the bill_event_offset.
			// But if that date is before the start_date (which means that invoice day of month would've happened before the member even signed up)
			// then we push the date forward to the next billFrequency interval.
			$nextBillDateTimestamp = mktime( 0, 0, 0, intval( $refDateToUseComponents['mon'] ), intval( $billEventOffset ), intval( $refDateToUseComponents['year'] ) );
			if ( ($nextBillDateTimestamp < $startDateTimestamp) || (date( 'Y-m-d', $refDateToUseTimestamp ) == date( 'Y-m-d', $nextBillDateTimestamp ) ) ) {
				$nextBillDateTimestamp = mktime( 0, 0, 0, intval( $refDateToUseComponents['mon'] ) + 1, intval( $billEventOffset ), intval( $refDateToUseComponents['year'] ) );
			}
			$nextBillDate = date( 'Y-m-d', $nextBillDateTimestamp );
		}
		else if ( $billFrequency == 'daily' ) {
			$nextBillDateTimestamp = strtotime( "{$refDateToUse} tomorrow" );
			$nextBillDate = date( 'Y-m-d', $nextBillDateTimestamp );
		}
		else if ( $billFrequency == 'weekly' ) {
			// Get name of day of the week of bill_event_date, then use it in the strtotime() to have it calculate the next day of the week name after the reference date to use.
			$dayOfTheWeekName = $dayOfTheWeekMap[ intval( $billEventOffset ) ];
			$nextBillDateTimestamp = strtotime( "{$refDateToUse} next {$dayOfTheWeekName}" );
			$nextBillDate = date( 'Y-m-d', $nextBillDateTimestamp );
		}
		else if ( $billFrequency == 'bi-weekly' || $billFrequency == 'biweekly' ) {
			// Do the same thing as the weekly block above...
			$dayOfTheWeekName = $dayOfTheWeekMap[ intval( $billEventOffset ) ];
			$nextBillDateTimestamp = strtotime( "{$refDateToUse} next {$dayOfTheWeekName}" );

			// ...but then make sure it's an even number of weeks after the arb start date otherwise add another week to it.
			$timeDifferenceBetweenStartAndNextBillDate = $nextBillDateTimestamp - $startDateTimestamp;
			$numWeeksDifference = floor( $timeDifferenceBetweenStartAndNextBillDate / 604800 );
			$numWeeksDifferenceIsEvenNumWeeks = $numWeeksDifference % 2;
			if ( $numWeeksDifferenceIsEvenNumWeeks ) {
				$refDateToUse = date( "Y-m-d", $nextBillDateTimestamp );
				$nextBillDateTimestamp = strtotime( "{$refDateToUse} next {$dayOfTheWeekName}" );
			}

			$nextBillDate = date( 'Y-m-d', $nextBillDateTimestamp );
		}
		else if ( $billFrequency == 'yearly' ) {
			// This is calculated similarly to the monthly section, where we try creating a next_bill_date starting with the start_date and then
			// advance to the next interval if it's before the reference date.
			$startDateComponents = getdate( $startDateTimestamp );
			$nextBillDateTimestamp = mktime( 0, 0, 0, intval( $startDateComponents['mon'] ), intval( $billEventOffset ), intval( $refDateToUseComponents['year'] ) );
			if ( $nextBillDateTimestamp < $refDateToUseTimestamp ) {
				$nextBillDateTimestamp = mktime( 0, 0, 0, intval( $startDateComponents['mon'] ), intval( $billEventOffset ), intval( $refDateToUseComponents['year'] ) + 1 );
			}

			$nextBillDate = date( 'Y-m-d', $nextBillDateTimestamp );
		}
		else {
			$this->errorLog( "Unrecognized billFrequency: {$billFrequency}" );
		}

		return $nextBillDate;
	}


	/**
	 * Function localDateTime()
	 *
	 * Returns local date/time string based on location's timezone setting.
	 *
	 * @param string $dataname
	 * @param string $location_id
	 *
	 * @return string $return_time - date/time in format: Y-m-d H:i:s
	 */

	public function localDateTime($dataname, $location_id)
	{
	        $return_time = date("Y-m-d H:i:s");

	        $locationInfo = $this->getLocationInfo($dataname, $location_id);
	        if ( !empty($locationInfo['timezone']) ) {
	                // Change the timezone to the location's timezone and then restore it to whatever it was before, so we don't mess up any other jc_addons.
	                $previousTimezone = date_default_timezone_get();
	                date_default_timezone_set( $locationInfo['timezone'] );
	                $return_time = date( "Y-m-d H:i:s" );
	                date_default_timezone_set( $previousTimezone );
	        }

	        return $return_time;
	}


	/**
	 * Function createProfile()
	 *
	 * Insert row into recurring billing table.
     *
	 * @param array $args - requires $args keys customerId, orderId, profileName, recurringAmount
     *
	 * @return int $insertedRowId
	 */

	public function createProfile( $args )
	{
		if ( !isset( $args['dataname'] ) || !isset( $args['customerId'] ) || !isset( $args['orderId'] ) || !isset( $args['locationId'] ) || !isset( $args['profileName'] ) || !isset( $args['recurringAmount'] ) ) {
			$this->errorLog( "Couldn't create billing profile due to missing input parameter(s): dataname={$dataname} customerId={$customerId} orderId={$orderId} profileName={$profileName} recurringAmount={$recurringAmount}" );
			return null;
		}

		$this->dataname = $args['dataname'];

		$this->debugLog( "customerId={$args['customerId']} orderId={$args['orderId']} profileName={$args['profileName']}" );

		// Get the ref num and expiration date for the credit card on file.
		$ccRefNum = '';
		$ccExpDate = '';
		$ccQuerySql = "select * from `cc_transactions` where `order_id` = '[order_id]'";
		$ccQueryVars = array( 'order_id' => $args['orderId'] );
		$ccQueryResults = lavu_query( $ccQuerySql, $ccQueryVars );
		$ccQueryResultsNumRows = mysqli_num_rows( $ccQueryResults );
		if ( $ccQueryResultsNumRows ) {
			$ccRow = mysqli_fetch_assoc( $ccQueryResults );
			$ccRefNum = $ccRow['transaction_id'];
			$ccCardType = $ccRow['card_type'];
			$ccNumLast4 = $ccRow['card_desc'];
			$ccExpDate = $ccRow['exp'];
		}

		$this->debugLog( "ccQueryResultsNumRows={$ccQueryResultsNumRows} ccRefNum={$ccRefNum} ccNumLast4={$ccNumLast4} ccExpDate={$ccExpDate} createuser={$args['createUser']}" );

		// Default values for non-required input parameters
		if ( empty( $args['startDate'] ) ) {
			$args['startDate'] = $this->localDateTime($this->dataname, $args['locationId']);
			//error_log( "DEBUG: startDate=". $args['startDate'] ." date=". date( "Y-m-d H:i:s" ) ." locationInfo[timezone]=". $locationInfo['timezone'] ." phpTimezone=". date_default_timezone_get() );  //debug
			}
		if ( empty( $args['endDate'] ) && !empty( $args['endDateModifier'] ) && $args['endDateModifier'] != 'open' ) {
			$args['endDate'] = date( "Y-m-d H:i:s", strtotime( $args['startDate'] . ' +' . $args['endDateModifier'] ) );
		}
		if ( empty( $args['billEventOffset'] ) ) {
			$args['billEventOffset'] = '1';
		}
		if ( empty( $args['billFrequency'] ) ) {
			$args['billFrequency'] = 'monthly';
		}
		if ( empty( $args['firstBillDate'] ) ) {
			$args['firstBillDate'] = $this->calculateNextBillDate( date( 'Y-m-d'), $args['startDate'], $args['billFrequency'], $args['billEventOffset'] );
		}
		if ( empty( $args['nextBillDate'] ) ) {
			$args['nextBillDate'] = $args['firstBillDate'];
		}

		$abpInsertVars = array(
			'createtime' => $args['startDate'],
			'customer_id' => $args['customerId'],
			'order_id' => $args['orderId'],
			'location_id' => $args['locationId'],
			'type' => 'billing_profile',
			//'name' => $this->getLocationName( $args['locationId'] ) .' '. $args['profileName'],
			'name' => $args['profileName'],
			'status' => 'active',
			'recurring_amount' => $args['recurringAmount'],
			'invoice_template' => $this->generateInvoiceTemplateXml( $args['locationId'], $args['orderId'] ),
			'start_date' => $args['startDate'],
			'end_date' => $args['endDate'],
			'first_bill_date' => $args['firstBillDate'],
			'next_bill_date' => $args['nextBillDate'],
			'bill_frequency' => $args['billFrequency'],
			'bill_event_offset' => $args['billEventOffset'],
			'cc_ref_num' => $ccRefNum,
			'cc_card_type' => $ccCardType,
			'cc_num_last4' => $ccNumLast4,
			'cc_exp_date' => $ccExpDate
		);
		$abpInsertSql = "insert into `med_billing` (`createtime`, `updatetime`, `customer_id`, `order_id`, `location_id`, `type`, `name`, `status`, `recurring_amount`, `invoice_template`, `start_date`, `end_date`, `first_bill_date`, `next_bill_date`, `bill_frequency`, `bill_event_offset`, `cc_ref_num`, `cc_card_type`, `cc_num_last4`, `cc_exp_date`) values ('[createtime]', '[updatetime]', '[customer_id]', '[order_id]', '[location_id]', '[type]', '[name]', '[status]', '[recurring_amount]', '[invoice_template]', '[start_date]', '[end_date]', '[first_bill_date]', '[next_bill_date]', '[bill_frequency]', '[bill_event_offset]', '[cc_ref_num]', '[cc_card_type]', '[cc_num_last4]', '[cc_exp_date]')";
		$abpInsertOk = lavu_query( $abpInsertSql, $abpInsertVars );

		$this->debugLog( "abpInsertOk={$this->bool2str[$abpInsertOk]}");

		$billingProfileId = null;
		if ( $abpInsertOk ) {
			$billingProfileId = lavu_insert_id();
		}
		else {
			$this->errorLog( "failed to insert row into med_billing:". lavu_dberror() );
		}

		return $billingProfileId;
	}


	/**
	 * Function createProfileAndWaiver()
	 *
	 * Insert row into recurring billing table and create fitness waiver PDF.
     *
	 * @param array $args - requires $args keys for createProfile() and createWaiver().
	 * @return int $billingProfileId - new table ID for billing profile
	 */

	public function createProfileAndWaiver( $args )
	{
		$billingProfileId = $this->createProfile( $args );
		$this->createWaiver( array_merge( $args, array( 'billingProfileId' => $billingProfileId ) ) );

		return $billingProfileId;
	}


	/**
	 * Function createWaiver()
	 *
	 * Create fitness waiver PDF.
     *
	 * @param array $args - requires $args keys for customerId, orderId, and employeeId.
	 */

	public function createWaiver( $args )
	{
		if ( !isset( $args['customerId'] ) || !isset( $args['billingProfileId'] ) || !isset( $args['orderId'] ) || !isset( $args['locationId'] ) || !isset( $args['employeeId'] ) ) {
			$this->errorLog( "Couldn't create waiver due to missing input parameter(s): customerId={$customerId} billingProfileId={$billingProfileId} orderId={$orderId} profileName={$profileName} recurringAmount={$recurringAmount} employeeId={$employeeId}" );
			return null;
		}

		$waiver = new FitnessWaiver( array(
			'customerId' => $args['customerId'],
			'arbId' => $args['billingProfileId'],
			'locId' => $args['locationId'],
			'employeeId' => $args['employeeId']
		) );

		$waiver->storeWaiverXml();
	}


	/**
	 * Function cancelProfile()
	 *
	 * Cancel the recurring billing profile for the specified table ID.
     *
	 * @param string $id - billing profile table ID
	 *
	 * @return bool $cancelProfileUpdateOk - whether the cancellation was successful
	 */

	public function cancelProfile( $id )
	{
		$cancelProfileUpdateOk = lavu_query( "update `med_billing` set status = 'cancelled' where id = '{$id}' and status != 'cancelled'" );

		if ( !$cancelProfileUpdateOk ) {
			$this->errorLog( "Failed to cancel billing profile ID {$id}:". lavu_dberror() );
			return false;
		}

		$this->log( "Cancelled billing profile ID {$id}" );

		return $cancelProfileUpdateOk;
	}

	/**
	 * Function reactivateProfile()
	 *
	 * Reactivate the cancelled recurring billing profile for the specified table ID.
     *
	 * @param string $id - billing profile table ID
	 *
	 * @return bool $reactivateProfileUpdateOk - whether the reactivation was successful
	 */

	public function reactivateProfile( $id )
	{
		$reactivateProfileUpdateOk = lavu_query( "update `med_billing` set status = 'active' where id = '{$id}' and status != 'active'" );

		if ( !$reactivateProfileUpdateOk ) {
			$this->errorLog( "Failed to reactivate billing profile ID {$id}:". lavu_dberror() );
			return false;
		}

		$this->log( "Reactivated billing profile ID {$id}" );

		return $reactivateProfileUpdateOk;
	}

	/**
	 * Function updateProfile()
	 *
	 * Update the specified fields for the recurring billing profile for the specified table ID.
     *
	 * @param string $billingProfileId - recurring billing profile ID
	 * @param array $args - array where array keys are column names to change and array values are new values to set them to.
	 *
	 * @return bool $updateProfileUpdateOk - whether the billing profile update was successful
	 */

	public function updateProfile( $billingProfileId, $args )
	{
		if ( !isset( $billingProfileId ) || !isset( $args ) ) {
			$this->errorLog( "Couldn't update billing profile due to missing input parameter(s): billingProfileId={$billingProfileId} count(args)=". count($args) );
			return null;
		}

		$setClause = '';
		foreach ( $args as $key => $val ) {
			if ( $setClause != '' ) {
				$setClause .= ', '. $setClause;
			}
			$setClause .= "`{$key}` = '". ConnectionHub::getConn('rest')->escapeString($val) ."'";
		}

		$updateProfileUpdateOk = lavu_query( "update `med_billing` set $setClause where id = '{$billingProfileId}'" );

		if ( !$updateProfileUpdateOk ) {
			$this->errorLog( "Failed to update billing profile ID {$billingProfileId} with ". $setClause . lavu_dberror() );
			return false;
		}

		$this->log( "Updated billing profile ID {$id} with ". $setClause );

		return $updateProfileUpdateOk;
	}

	public function issueCredit( $orderId )
	{
		// TO DO : get cc ref number using $orderId

		// TO DO : run USAePay PHP lib CreditVoid
	}

	/**
	 * Function generateInvoiceTemplateXml()
	 *
	 * Generate invoice template XML by pulling order_contents table using order_id.
     *
	 * @param string $locationId - The location ID, for getting the original_id template.
	 * @param string $orderId - The order ID for the invoice to pay.
     *
	 */

	public function generateInvoiceTemplateXml( $locationId, $orderId )
	{
		// Get the Original ID template from components_package.  But we'll first need the Components Package ID from locations.

		$locationInfoQuerySql = "select `component_package` from `locations` where `id` = '{$locationId}'";
		$locationInfoQueryResults = lavu_query( $locationInfoQuerySql );
		if ( $locationInfoQueryResults === FALSE ) die( "Query error with locationInfoQuerySql={$locationInfoQuerySql}: ". lavu_dberror() );
		$locationInfoQueryResultsNumRows = mysqli_num_rows( $locationInfoQueryResults );

		if ( $locationInfoQueryResults <= 0 ) {
			$this->errorLog( "Didn't find any locations rows" );
			return;
		}

		$locationInfo = mysqli_fetch_assoc( $locationInfoQueryResults );

		$componentPackageQuerySql = "select `order_addons` from `component_packages` where `id` = '{$locationInfo['component_package']}'";
		$componentPackageQueryResults = mlavu_query( $componentPackageQuerySql );
		if ( $componentPackageQueryResults === FALSE ) die( "Query error with componentPackageQuerySql={$componentPackageQuerySql}: ". mlavu_dberror() );
		$componentPackageQueryResultsNumRows = mysqli_num_rows( $componentPackageQueryResults );

		if ( $componentPackageQueryResults <= 0 ) {
			$this->errorLog( "Didn't find any component_packages rows" );
			return;
		}

		$componentPackage = mysqli_fetch_assoc( $componentPackageQueryResults );
		$originalId = $componentPackage['order_addons'];

		//$this->debugLog( "orderId={$orderId} orderContentsQueryResultsNumRows={$orderContentsQueryResultsNumRows}" );

		$orderTotal = 0.00;
		$orderSubtotal = 0.00;
		$orderTax = 0.00;
		$orderTaxRate = 0.0;

		$orderContentsQuerySql = "select * from `order_contents` where `order_id` = '{$orderId}' order by id";
		$orderContentsQueryResults = lavu_query( $orderContentsQuerySql );
		$orderContentsQueryResultsNumRows = mysqli_num_rows( $orderContentsQueryResults );

		$this->debugLog( lavu_dberror() );

		if ( $orderContentsQueryResultsNumRows <= 0 ) {
			$this->errorLog( "Didn't find any order_contents rows" );
			return;
		}

		while ( $orderContents = mysqli_fetch_assoc( $orderContentsQueryResults ) ) {

			if ( $orderContents['hidden_data5'] == 'recurring_item' ) {
				$amount = $orderContents['price'];
				$quantity = $orderContents['quantity'];
				$subtotal = $orderContents['subtotal'];
				$tax = $orderContents['tax_amount'];
				$total = $orderContents['total_with_tax'];

				$taxRate = $orderContents['tax_rate1'];
				$orderTaxRate = $orderContents['tax_rate1'];

				// Accumulate totals for the entire order with each invoice item (to be used on <invoice> tag) but allow
				// the recurring amount to be overridden by a value stored in hidden_data4 (Special Details 1 in CP/V2).
				// We also have to recalcuate the subtotal, total, and tax when overriding since they won't match the order contents.

				if ( $orderContents['hidden_data4'] != '' ) {
					$hidden_data4 = explode( '|', $orderContents['hidden_data4'] );
					$amount = $hidden_data4[0];
					$this->debugLog( "amount={$amount}" );  /** debug **/

					$subtotal = round( floatval( $amount ) * floatval( $quantity ), 2);
					$tax = round( $subtotal * floatval( $taxRate ), 2);
					$total = round( $subtotal + $tax, 2 );

					$orderSubtotal += $subtotal;
					$orderTax += $tax;
					$orderTotal += ($subtotal + $tax);
				}
				else {
					$orderSubtotal += floatval( $orderContents['subtotal'] );
					$orderTax += floatval( $orderContents['tax_amount'] );
					$orderTotal += floatval( $orderContents['total_with_tax'] );
				}

				// TO DO : let hidden detail 2 (whatever field that is) override name (?)

				$super_group_id = 

				$invoiceTemplateXml .=<<<XML
	<item>
		<item_id>{$orderContents['id']}</item_id>
		<name>{$orderContents['item']}</name>
		<amount>{$amount}</amount>
		<quantity>{$quantity}</quantity>
		<subtotal>{$subtotal}</subtotal>
		<total>{$total}</total>
		<tax>{$tax}</tax>
		<tax_rate>{$taxRate}</tax_rate>
		<options>{$orderContents['options']}</options>
		<super_group_id>{$orderContents['super_group_id']}</super_group_id>
	</item>
XML;
			}
		}

		$orderSubtotal = round( $orderSubtotal, 2 );
		$orderTax = round( $orderTax, 2);
		$orderTotal = round( $orderTotal, 2 );

		$invoiceTemplateXml = <<<XML
<invoice order_id="4-x-nnn" subtotal="{$orderSubtotal}" total="{$orderTotal}" tax_rate="{$orderTaxRate}" tax="{$orderTax}" original_id="{$originalId}" >{$invoiceTemplateXml}
</invoice>
XML;

		return $invoiceTemplateXml;
	}

	private function log( $msg )
	{
		error_log( __CLASS__ .' '. $msg );
	}

	private function debugPrint( $msg )
	{
		if ( $this->debug ) {
			echo 'DEBUG:<pre>', print_r( $msg, 1 ), '</pre>';
		}
	}

	private function debugLog( $msg )
	{
		if ( $this->debug ) {
			error_log( 'DEBUG: '. __CLASS__ .' '. $msg );
		}
	}

	private function errorLog( $msg )
	{
		error_log( __CLASS__ .' '. $msg );
	}

	/** 
	  * Function getAllBillingProfiles()
	  * 
	  * Query med_billing for all billing profiles.
      *
      * @return (array) returns a numerically indexed array of billing profile associative arrays
	  */

	public function getAllBillingProfiles($sortOrder = 'desc')
	{
		$billingProfiles = array();

		$billingProfileQuerySql = "select * from `med_billing` order by id {$sortOrder}";
		$billingProfileQueryResults = lavu_query( $billingProfileQuerySql, $args );
		$billingProfileQueryNumRows = mysqli_num_rows( $billingProfileQueryResults );
		while ( $row = mysqli_fetch_assoc( $billingProfileQueryResults ) ) {
			$billingProfiles[] = $row;
		}

		return $billingProfiles;
	}

	/** 
	  * Function getActiveBillingProfiles()
	  * 
	  * Query med_billing for all non-cancelled billing profiles that have the specified bill day of month.
      *
      * @param (string) $args - an associative array of additional search values where the keys are column names and values are database values
      * @return (array) returns a numerically indexed array of billing profile associative arrays
	  */

	public function getActiveBillingProfiles( $args )
	{
		$billingProfiles = array();

		$sqlCriteria = '';
		foreach ( $args as $key => $val ) {
			$sqlCriteria .= "and `{$key}` = '[{$key}]'";
		}

		$billingProfileQuerySql = "select * from `med_billing` where `status` = 'active' and `end_date` >= current_date() {$sqlCriteria}";
		$billingProfileQueryResults = lavu_query( $billingProfileQuerySql, $args );
		$billingProfileQueryNumRows = mysqli_num_rows( $billingProfileQueryResults );
		if ( $billingProfileQueryNumRows ) {
			$billingProfiles[] = mysqli_fetch_assoc( $billingProfileQueryResults );
		}

		return $billingProfiles;
	}

	/**
	  * Function getBillingProfileForCustomerId()
	  *
	  * Query med_billing for the billing profile for the supplied Customer ID.
	  * Returns the first billing profile found if the Customer has multiple profiles.
      *
      * @param (string) $args - an associative array of additional search values where the keys are column names and values are database values
      * @return (array) returns a numerically indexed array of billing profile associative arrays
	  */

	public function getBillingProfileForCustomerId( $customerId )
	{
		$billingProfile = null;

		$billingProfileQuerySql = "select * from `med_billing` where customer_id = '[customer_id]' order by id limit 1";
		$billingProfileQueryVars = array( 'customer_id' => $customerId );
		$billingProfileQueryResults = lavu_query( $billingProfileQuerySql, $billingProfileQueryVars );
		if ( $billingProfileQueryResults === FALSE ) die( "Error with query {$billingProfileQuerySql}: ". lavu_dberror() );
		$billingProfileQueryNumRows = mysqli_num_rows( $billingProfileQueryResults );
		if ( $billingProfileQueryNumRows ) {
			$billingProfile = mysqli_fetch_assoc( $billingProfileQueryResults );
		}
		else {
			$this->errorLog( "Can't find billing profile for customerId={$customerId}" );
		}

		return $billingProfile;
	}

	/**
	  * Function getBillingProfileById()
	  *
	  * Query med_billing for the billing profile for the supplied Billing Profile ID.
      *
      * @param (string) $billingProfileId - Billing Profile ID.
      * @return (array) returns the associative array of the billing profile
	  */

	public function getBillingProfileById( $billingProfileId )
	{
		$billingProfile = null;

		$billingProfileQuerySql = "select * from `med_billing` where id = '[id]'";
		$billingProfileQueryVars = array( 'id' => $billingProfileId );
		$billingProfileQueryResults = lavu_query( $billingProfileQuerySql, $billingProfileQueryVars );
		if ( $billingProfileQueryResults === FALSE ) die( "Query error with billingProfileQuerySql={$billingProfileQuerySql}: ". lavu_dberror() );
		$billingProfileQueryNumRows = mysqli_num_rows( $billingProfileQueryResults );
		if ( $billingProfileQueryNumRows ) {
			$billingProfile = mysqli_fetch_assoc( $billingProfileQueryResults );
		}
		else {
			$this->errorLog( "Can't find billing profile for billingProfileId={$billingProfileId}" );
		}

		return $billingProfile;
	}

	/**
	  * Function getAllBillingProfilesForCustomer()
	  *
	  * Query med_billing for all billing profile for the supplied Customer ID.
      *
      * @param (string) $args - an associative array of additional search values where the keys are column names and values are database values
	  *
      * @return (array) $billingProfiles - returns an array of the customer's billing profiles ordered by newest to oldest
	  */

	public function getAllBillingProfilesForCustomer( $customerId )
	{
		$billingProfiles = array();

		$billingProfilesQuerySql = "select * from `med_billing` where customer_id = '[customer_id]' order by id desc";
		$billingProfilesQueryVars = array( 'customer_id' => $customerId );
		$billingProfilesQueryResults = lavu_query( $billingProfilesQuerySql, $billingProfilesQueryVars );
		$billingProfilesQueryNumRows = mysqli_num_rows( $billingProfilesQueryResults );
		if ( $billingProfilesQueryNumRows ) {
			while ( $row = mysqli_fetch_assoc( $billingProfilesQueryResults ) ) {
				$billingProfiles[] = $row;
			}
		}
		else {
			$this->errorLog( "Can't find billing profiles for customerId={$customerId}" );
		}

		return $billingProfiles;
	}

	/** 
	  * Function getActiveBillingProfiles()
	  * 
	  * Query med_billing for all non-cancelled billing profiles that have the specified bill day of month.
      *
      * @param (string) $args - an associative array of additional search values where the keys are column names and values are database values
      *
      * @return (array) returns a numerically indexed array of billing profile associative arrays
	  */

	public function getInvoice( $orderId )
	{
		$invoice = null;

		$invoiceQuerySql = "select * from `orders` where `order_id` = '{$orderId}'";
		$invoiceQueryResults = lavu_query( $invoiceQuerySql );
		$invoiceQueryNumRows = mysqli_num_rows( $invoiceQueryResults );
		if ( $invoiceQueryNumRows > 1 ) {
			$this->errorLog( "Got multiple invoices for orderId={$orderId}" );
			$invoice = mysqli_fetch_assoc( $invoiceQueryResults );
		}
		else if ( $invoiceQueryNumRows == 1 ) {
			$invoice = mysqli_fetch_assoc( $invoiceQueryResults );
		}

		return $invoice;
	}

	/** 
	  * Function insertPaymentRecord()
	  * 
	  * Add a record for our successful credit card payment into cc_transactions.
      *
      * @param (string) $orderId - the invoice's order id that was paid
      * @param (string) $billingProfile - an associative array containing the recurring billing profile's database values
	  *
      * @return (boolean) returns whether the insert was successful
	  */

	private function insertPaymentRecord( $orderId, $billingProfile, $ccTxnResults )
	{
		$ccTransactionInsertVars = array(
			'loc_id' => $billingProfile['location_id'],
			'order_id' => $orderId,
			'check' => '1',
			'amount' => $ccTxnResults->amount,
			'transaction_id' => $ccTxnResults->refnum,
			'preauth_id' => $ccTxnResults->refnum,
			'auth_code' => $ccTxnResults->authcode,
			'card_type' => $billingProfile['cc_card_type'],
			'first_four' => '',  // First Four Digits of Card - deemed unnecessary here
			'card_desc' => $billingProfile['cc_num_last4'],
			'card_type' => $billingProfile['cc_card_type'],
			'transtype' => 'Sale',
			'register' => 'customers_billing',
			'register_name' => 'customers_billing',
			'server_name' => 'customers_billing',
			'gateway' => 'USAePay',
			'info' => '',  // Name on Card - usually only needed by loyaltree but deemed unnecessary here
			'exp' => $billingProfile['cc_exp_date'],
			'datetime' => date("Y-m-d H:i:s"),
			'pay_type' => 'Card',
			'pay_type_id' => '2',
			'total_collected' => $ccTxnResults->amount,
			'action' => 'Sale',
			'server_id' => '0',
			'for_deposit' => '0',
			'got_response' => '1',
			'processed' => '0',  // richard said this should be 0 since it doesn't belong to a batch.
			'is_deposit' => '0',
			'server_time' => date("Y-m-d H:i:s")
		);
		$insertVarKeys = array_keys( $ccTransactionInsertVars );
		$ccTransactionInsertCols = implode( '`, `', $insertVarKeys );
		$ccTransactionInsertVals = implode( "]', '[", $insertVarKeys );
		$ccTransactionInsertSql = "insert into `cc_transactions` (`{$ccTransactionInsertCols}`) values ('[{$ccTransactionInsertVals}]')";
		$ccTransactionInsertOk = lavu_query( $ccTransactionInsertSql, $ccTransactionInsertVars );
		if ( !$ccTransactionInsertOk ) {
			$this->errorLog( "Failed to insert row into cc_transactions:". lavu_dberror() );
			return false;
		}

		$this->log( "Inserted record for payment on invoice {$orderId} for billingProfileId={$billingProfile['id']}" );

		return true;
	}

	/** 
	  * Function updateInvoiceAsPaid()
	  * 
	  * Update the invoice to show it as a paid and closed order.
      *
      * @param (string) $orderId - the invoice's order id that was paid
      * @param (string) $billingProfile - an associative array containing the recurring billing profile's database values
      *
      * @return (boolean) returns whether the insert was successful
	  */

	private function updateInvoiceAsPaid( $orderId, $billingProfile, $ccTxnResults )
	{
		$invoiceUpdateOk = lavu_query( "update `orders` set closed=now(), send_status=1, card_paid='{$ccTxnResults->amount}' where order_id='{$orderId}'" );

		$this->debugLog( "invoiceUpdateOk={$invoiceUpdateOk} check=". (!$invoiceUpdateOk) );

		if ( !$invoiceUpdateOk ) {
			$this->errorLog( "Failed to update orders for orderId={$orderId}:". lavu_dberror() );
			return false;
		}

		$this->log( "Invoice {$orderId} updated as paid for billingProfileId={$billingProfile['id']}" );

		return $invoiceUpdateOk;
	}

	/** 
	  * Function getAllInvoicesForBillingProfile()
	  * 
	  * Get a list of unpaid invoices for the given billing profile.
      *
      * @param (string) $billingProfile - an associative array containing the recurring billing profile's database values
      * @param (string) $orderStr - asc|desc; the SQL order by clause to control the sort order of the results
      *
      * @return array - array of all invoices.
	  */

	public function getAllInvoicesForBillingProfile( $billingProfile, $orderStr = 'asc' )
	{
		$invoices = array();

		$allInvoicesQuerySql = "select * from `orders` where `order_id` like '4-{$billingProfile['order_id']}-%' order by order_id {$orderStr}";
		$allInvoicesQueryResults = lavu_query( $allInvoicesQuerySql );
		$allInvoicesQueryNumRows = mysqli_num_rows( $allInvoicesQueryResults );

		while ( $row = mysqli_fetch_assoc( $allInvoicesQueryResults ) ) {
			$invoices[] = $row;
		}

		return $invoices;
	}

	/** 
	  * Function getAllUnpaidInvoicesId()
	  * 
	  * Get a list of IDs for all unpaid invoices.
      *
      * @return array - list of unpaid invoices with the keys order_id, originalOrderId, and unpaidAmount.
	  */

	public function getAllUnpaidInvoicesId()
	{
		$invoices = array();

		$unpaidOrdersQuerySql = "select order_id, substring_index(substring_index(order_id, '-', -3), '-', 2) as originalOrderId, convert(total, decimal(30, 2)) - (convert(cash_paid, decimal(30, 2)) + convert(card_paid, decimal(30, 2))) as unpaidAmount from `orders` where `order_id` like '4-%' and closed='0000-00-00 00:00:00' and (convert(cash_paid, decimal(30, 2)) + convert(card_paid, decimal(30, 2))) < convert(total, decimal(30, 2)) order by order_id";
		$unpaidOrdersQueryResults = lavu_query( $unpaidOrdersQuerySql );
		$unpaidOrdersQueryNumRows = mysqli_num_rows( $unpaidOrdersQueryResults );

		$maxPaymentThreshold = round( floatval( $billingProfile['total'] ) * 2.0, 2 );
		$totalAcrossInvoices = 0.00;

		while ( $row = mysqli_fetch_assoc( $unpaidOrdersQueryResults ) ) {
			$invoices[] = $row;
		}

		return $invoices;
	}

	/** 
	  * Function getUnpaidInvoiceIdsForBillingProfile()
	  * 
	  * Get a list of unpaid invoices for the given billing profile.
      *
      * @param (string) $billingProfile - an associative array containing the recurring billing profile's database values
      *
      * @return array - list of order IDs for the unpaid invoices.
	  */

	private function getUnpaidInvoiceIdsForBillingProfile( $billingProfile )
	{
		$invoices = array();

		$unpaidOrdersQuerySql = "select * from `orders` where `order_id` like '4-{$billingProfile['order_id']}-%' and closed='0000-00-00 00:00:00' and (convert(cash_paid, decimal(30, 2)) + convert(card_paid, decimal(30, 2))) < convert(total, decimal(30, 2)) order by order_id";
		$unpaidOrdersQueryResults = lavu_query( $unpaidOrdersQuerySql );
		$unpaidOrdersQueryNumRows = mysqli_num_rows( $unpaidOrdersQueryResults );

		$maxPaymentThreshold = round( floatval( $billingProfile['total'] ) * 2.0, 2 );
		$totalAcrossInvoices = 0.00;

		while ( $row = mysqli_fetch_assoc( $unpaidOrdersQueryResults ) ) {
			if ( ($totalAcrossInvoices + floatval( $row['total_with_tax'] ) ) <= $maxPaymentThreshold ) {
				$invoices[] = $row['order_id'];
				$totalAcrossInvoices = round( $totalAcrossInvoices + floatval( $row['total_with_tax'] ), 2 );
			}
			// FUTURE TO DO : add support for partial payments to this logic
		}

		return $invoices;
	}

	/** 
	  * Function getUnpaidAmountForBillingProfile()
	  * 
	  * Get the total unpaid amount for all unpaid invoices for the given billing profile.
      *
      * @param (string) $billingProfile - an associative array containing the recurring billing profile's database values
      *
      * @return (float) $unpaidAmount - total unpaid amount of all invoices for the billing profile
	  */

	public function getUnpaidAmountForBillingProfile( $billingProfile )
	{
		$unpaidAmount = 0.00;

		$unpaidInvoices = $this->getUnpaidInvoiceIdsForBillingProfile( $billingProfile );

		$this->debugLog( '# unpaid invoices='. count( $unpaidInvoices ) );  /** debug **/

		foreach ( $unpaidInvoices as $orderId ) {
			$invoice = $this->getInvoice( $orderId );
			$unpaidAmount += $this->getUnpaidAmountOnInvoice( $invoice );
		}

		return $unpaidAmount;
	}

	/** 
	  * Function getUnpaidAmountForCustomerId()
	  * 
	  * Get a list of unpaid invoices for the given Customer ID.
      *
      * @param (string) $customerId - med_customer table ID
      *
      * @return (float) $unpaidAmount - total unpaid amount of all invoices for the Customer ID
	  */

	public function getUnpaidAmountForCustomerId( $customerId )
	{
		$unpaidAmount = 0.00;

		$billingProfiles = $this->getAllBillingProfilesForCustomer( $customerId );

		foreach ( $billingProfiles as $billingProfile ) {
			$unpaidAmount += $this->getUnpaidAmountForBillingProfile( $billingProfile );
		}

		return $unpaidAmount;
	}

	/** 
	  * Function getLocationName()
	  * 
	  * Look-up location name using the location ID, which is stored in the database.
	  * It was decided not to store the location name since it wasn't available when
	  * the billing profile gets created and since it might change.
      *
      * @param string $billingProfile - an associative array containing the recurring billing profile's database values
      *
      * @return string - list of order IDs for the unpaid invoices.
	  */

	private function getLocationName( $locationId )
	{
		$locationQuerySql = "select * from `locations` where `id` = '{$locationId}'";
		$locationQueryResults = lavu_query( $locationQuerySql );
		$locationQueryNumRows = mysqli_num_rows( $locationQueryResults );

		$row = mysqli_fetch_assoc( $locationQueryResults );

		return $row['title'];
	}

	/** 
	  * Function getUnpaidAmountOnInvoice()
	  * 
	  * Calculate the unpaid amount on the specified invoice.
      *
      * @param array $invoice - an associative array representing the invoice (aka order)
      *
      * @return float $unpaidAmount - unpaid amount on invoice
	  */

	public function getUnpaidAmountOnInvoice( $invoice )
	{
		$unpaidAmount = 0.00;

		$total = isset( $invoice['total'] ) ? floatval( $invoice['total'] ) : 0.00;
		$cash_paid = isset( $invoice['cash_paid'] ) ? floatval( $invoice['cash_paid'] ) : 0.00;
		$card_paid = isset( $invoice['card_paid'] ) ? floatval( $invoice['card_paid'] ) : 0.00;

		$unpaidAmount = $total - $card_paid - $cash_paid;

		return ($unpaidAmount < 0.00) ? 0.00 : $unpaidAmount;
	}

}

?>
