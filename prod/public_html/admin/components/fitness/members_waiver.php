
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<title>Untitled Document</title>

		<style type="text/css">
<!--

body {
	background-color: transparent;
	margin-left: 0px;
	margin-top: 0px;
	margin-right: 0px;
	margin-bottom: 0px;
	-webkit-tap-highlight-color:rgba(0,0,0,0);
}

.style4 {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 14px;
	font-weight: bold;
	color: #8896AD;
}
.style6 {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 24px;
	font-weight: bold;
	color: #8896AD;
}
.style8 {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 15px;
	font-weight: bold;
	color: #07142a;
}
.creditnumber {
	font-family: Verdana, Geneva, sans-serif;
	font-size: 13px;
	font-weight: bold;
	color: #3d4f6d;
}
.nametext {
	font-family: Verdana, Geneva, sans-serif;
	font-size: 13px;
	font-weight: bold;
}
.subtitles {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 16px;
	font-weight: bold;
	color: #5A75A0;
}
.subtitles_dark {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 16px;
	font-weight: bold;
	color: #2f3e55;
}
.info_text1 {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 16px;
	color: #233756;
}
.info_text2 {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 15px;
	color: #5A75A0;
}
.info_text3 {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 15px;
	color: #233756;
}
-->
		</style>
	</head>

	<body>
		<table width="542" cellspacing="0" cellpadding="6">
			<tr>
				<td align="center">
					<table width="532" border="0" cellspacing="0" cellpadding="0">
						<tr><td class='info_text1'>
						<?php 



							require_once(dirname(__FILE__) . "/../comconnect.php"); 
							if(reqvar("customer_id")) $customer_id = reqvar("customer_id");
							else if (isset($_POST['mod_info'])) $customer_id = extractCustomerIDfromModInfo($_POST['mod_info'], "MEMBER:");

							$show_waiver = false;
							if(is_numeric($customer_id) && $customer_id!="")
							{
								$show_waiver = true;
								$date_created = date("Y-m-d H:i:s");
							}
							else
							{
								$customer_query = lavu_query("select * from `med_customers` where `id`='[1]'",$customer_id);
								if(mysqli_num_rows($customer_query))
								{
									$customer_read = mysqli_fetch_assoc($customer_query);
									$date_created = $customer_read['date_created'];
									$show_date_created = $date_created;
									$dcparts = explode(" ",$date_created);
									if(count($dcparts) > 1)
									{
										$dparts = explode("-",$dcparts[0]);
										$tparts = explode(":",$dcparts[1]);
										if(count($dparts) > 2 && count($tparts) > 2)
										{
											$dcts = mktime($tparts[0],$tparts[1],$tparts[2],$dparts[1],$dparts[2],$dparts[0]);
											$show_date_created = date("m/d/Y h:i a",$dcts);
										}
									}

									//echo "<b>---------- Waiver as of $show_date_created ----------</b><br><br>";
								}
							}

							if($show_waiver)
							{
								$waiver_query = lavu_query("SELECT * FROM `lk_waivers` WHERE `loc_id` = '[1]' ORDER BY `datetime` desc, `id` DESC",$loc_id,$date_created);
								if($waiver_query !== false && mysqli_num_rows($waiver_query))
								{
									while ($waiver_read = mysqli_fetch_assoc($waiver_query))
									{
										// The 'type' key not being set is supposed to indicate that there is no `type` column in the `lk_waivers` table, which would mean all rows are waivers.
										if (!isset($waiver_read['type']) || $waiver_read['type'] == 'waiver')
										{
											echo nl2br( $waiver_read['adult'] );
											break;
										}
									}
								}
								else
								{
									echo "No Waiver on file as of " . $date_created;
								}
							}
                        ?>
                        </td></tr>
                    </table>
                </td>
            </tr>
        </table>
	</body>
</html>
