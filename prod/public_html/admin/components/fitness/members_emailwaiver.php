<?php 

require_once(dirname(__FILE__) . "/../comconnect.php");
require_once(dirname(__FILE__) . "/../comconnect_legacy.php");
//require_once(dirname(__FILE__) . "/../order_addons/customer_functions.php");
require_once(dirname(__FILE__) . "/FitnessWaiver.php");

$waiver_html = '';
echo <<<HTML

<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<title>Untitled Document</title>

		<style type="text/css">
		</style>
	</head>

	<body>

HTML;

$page = (isset($_REQUEST['page'])) ? $_REQUEST['page'] : '';
$email = (isset($_REQUEST['email'])) ? $_REQUEST['email'] : '';
$customer_id = (isset($_REQUEST['customer_id'])) ? $_REQUEST['customer_id'] : '';
$arb_id = (isset($_REQUEST['arb_id'])) ? $_REQUEST['arb_id'] : '';
$loc_id = (isset($_REQUEST['loc_id'])) ? $_REQUEST['loc_id'] : '';
$dataname = (isset($_REQUEST['dn'])) ? $_REQUEST['dn'] : '';
$server_id = (isset($_REQUEST['server_id'])) ? $_REQUEST['server_id'] : '';
$contract_supported = (isset($_REQUEST['contract_supported'])) ? $_REQUEST['contract_supported'] : '';
$contract_signed = (isset($_REQUEST['contract_signed'])) ? $_REQUEST['contract_signed'] : '';

if ( $page == 'emailwaiver' ) {

	/* Create the waiver PDF */

	$waiver = new FitnessWaiver( array(
		'dataname' => $dataname,
		'customerId' => $customer_id,
		'arbId' => $arb_id,
		'locId' => $loc_id,
		'employeeId' => $server_id
	) );

	$fileatt = $waiver->outputPdf();


	/* Email the waiver PDF */

	if ( !empty( $fileatt ) ) {

		// Settings
		$from = "Lavu Fit";  // TO DO : use location name
		$subject = "{$form} Membership Waiver";
		$fileatttype = "application/pdf";
		$fileattname = "waiver_{$customer_id}.pdf";
		$headers = "From: $from";

		// File
		$file = fopen($fileatt, 'rb');
		$data = fread($file, filesize($fileatt));
		fclose($file);

		// This attaches the file
		$semi_rand = md5(time());
		$mime_boundary = "==Multipart_Boundary_x{$semi_rand}x";
		$headers .= "\nMIME-Version: 1.0\n" .
		"Content-Type: multipart/mixed;\n" .
		" boundary=\"{$mime_boundary}\"";
		$message = "This is a multi-part message in MIME format.\n\n" .
		"-{$mime_boundary}\n" .
		"Content-Type: text/plain; charset=\"iso-8859-1\n" .
		"Content-Transfer-Encoding: 7bit\n\n" .
		$message .= "\n\n";

		$data = chunk_split(base64_encode($data));
		$message .= "--{$mime_boundary}\n" .
		"Content-Type: {$fileatttype};\n" .
		" name=\"{$fileattname}\"\n" .
		"Content-Disposition: attachment;\n" .
		" filename=\"{$fileattname}\"\n" .
		"Content-Transfer-Encoding: base64\n\n" .
		$data . "\n\n" .
		"-{$mime_boundary}-\n";
	}

	// Send the email if the waiver exists
	if ( !empty( $fileatt ) && mail( $email, $subject, $message, $headers ) ) {

		echo <<<HTML

	<h1 style="margin-top:50px" class="style6">This member's waiver has been emailed.</h1>

HTML;

	} else {

		echo <<<HTML

	<h1 style="margin-top:50px" class="style6">There was an error sending the mail.</h1>

HTML;

	}
}
else if ( $page != 'emailwaiver' && $email == '' ) {

	echo <<<HTML

	<h1 style="margin-top:50px" class="style6">This member's waiver has been signed.</h1>

HTML;

	if ($dataname == "lavu_fitness" || $dataname == "sityodtong_inc") {

		echo <<<HTML

	<h1 style="margin-top:50px" class="style4">(Member is missing Email Address so waiver cannot be emailed.)</h1>

HTML;

	}


}
else {

	echo <<<HTML

	<h1 style="margin-top:50px" class="style6">This member's waiver has been signed.</h1>

HTML;

	if ($dataname == "lavu_fitness" || $dataname == "sityodtong_inc") {

		echo <<<HTML

	<form>
	<input type="hidden" name="page" value="emailwaiver" />
	<input type="hidden" name="customer_id" value="{$customer_id}" />
	<input type="hidden" name="dn" value="{$dataname}" />
	<input type="hidden" name="arb_id" value="{$arb_id}" />
	<input type="hidden" name="loc_id" value="{$loc_id}" />
	<input type="hidden" name="server_id" value="{$server_id}" />
	<input type="hidden" name="email" value="{$email}" />
	<input style="margin-top:50px" type="submit" value="Email Signed Waiver to Member" />
	</form>

HTML;

	}

       if ( $contract_supported && $contract_signed ) {

               echo <<<HTML

       <h1 style="margin-top:50px" class="style6">This member's contract has been signed.</h1>

HTML;

       }


}

echo <<<HTML

	</body>
</html>

HTML;
