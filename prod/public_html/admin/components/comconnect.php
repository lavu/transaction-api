<?php
require_once("/home/poslavu/public_html/admin/cp/resources/lavuquery.php");
require_once("/home/poslavu/public_html/admin/cp/resources/core_functions.php");

session_start();

$company_code_full = get_requested_company_code();
$_SESSION['company_code'] = $company_code_full;
$company_code_info = get_company_code_info($company_code_full);
$company_code = $company_code_info['cc'];
$company_code_key = $company_code_info['key'];

$company_query = ConnectionHub::getConn('poslavu')->legacyQuery("SELECT `id`, `data_name` FROM `poslavu_MAIN_db`.`restaurants` WHERE company_code = '[1]'",$company_code);
if(!mysqli_num_rows($company_query)) {
	exit();
}
// initialize connection to restaurant DB:

$conn = ConnectionHub::getConn('rest');
$company_read = mysqli_fetch_assoc($company_query);
$conn->selectDN($company_read['data_name']);

$get_location_config = lavu_query("SELECT `setting`, `value` FROM `config` WHERE `type` = 'location_config_setting' AND `location` = '[1]'", $locationid);
$config_settings = array();
while ($config_info = mysqli_fetch_assoc($get_location_config)) {
	$config_settings[$config_info['setting']] = $config_info['value'];
}

if(lsecurity_id($company_code_key)!=$company_read['id'])
{
	if(isset($_GET['debug'])) echo "Security Fail: $company_code $company_code_key";
	exit();
}

// sent by the app:
// comp_name
// cc
// dn
// loc_id
// server_id
// server_name
// tab_name

$server_id = 0;
if(reqvar("server_id")) $server_id = reqvar("server_id");
else if(sessvar_isset("server_id")) $server_id = sessvar("server_id");
set_sessvar("server_id",$server_id);

$server_name = "";
if(reqvar("server_name")) $server_name = reqvar("server_name");
else if(sessvar_isset("server_name")) $server_name = sessvar("server_name");
set_sessvar("server_name",$server_name);

$loc_id = 0;
if(reqvar("loc_id")) $loc_id = reqvar("loc_id");
else if(sessvar_isset("loc_id")) $loc_id = sessvar("loc_id");
set_sessvar("loc_id", $loc_id);

$dataname = "";
if(reqvar("dn")) $dataname = reqvar("dn");
else if(sessvar_isset("dataname")) $dataname = sessvar("dataname");
set_sessvar("dataname", $dataname);
$data_name = $dataname;

//Inserted by Brian D.  Now need location_info and language pack in components.
//We need to load these two for in-app components to work.
global $location_info;
global $active_language_pack;
load_location_info();
load_active_language_pack();

function load_location_info(){
	global $location_info;
	$result = lavu_query("SELECT * FROM `locations`");
	$location_info = mysqli_fetch_assoc($result);
	return $location_info;
}
function load_active_language_pack(){
	global $location_info;
	global $active_language_pack;
	global $active_language_pack_id;
	$active_language_pack_id = $location_info['use_language_pack'];
	$active_language_pack = getLanguageDictionary($location_info['id']);
	if (empty($active_language_pack)) {
		//From core_functions...
		$active_language_pack = load_language_pack($location_info['use_language_pack'], $location_info['id']);
	}
}
//End insert.

function get_com_urlpath()
{
	$com_urlpath = str_replace($_SERVER['DOCUMENT_ROOT'],"",dirname(__FILE__));
	return $com_urlpath;
}

function com_memvar($key,$def="")
{
	if(isset($_POST[$key])) $_SESSION[$key] = $_POST[$key];
	else if(isset($_GET[$key])) $_SESSION[$key] = $_GET[$key];
	return (isset($_SESSION[$key]))?$_SESSION[$key]:$def;
}

$access_level = 1;
if($server_id)
{
	$server_query = lavu_query("SELECT * from `users` WHERE `id`='[1]'",$server_id);
	if(mysqli_num_rows($server_query))
	{
		$server_read = mysqli_fetch_assoc($server_query);
		$access_level = $server_read['access_level'];
	}
}
?>
