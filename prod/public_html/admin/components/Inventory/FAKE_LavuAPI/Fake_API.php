<?php

/**
 * Created by PhpStorm.
 * User: Leif
 * Date: 4/12/17
 * Time: 3:33 PM
 */
class Fake_API
{

	private static $returnArray;
	private static function initializeReturnArray(){
		self::$returnArray = array();
		self::$returnArray['Status'] = "Success";
	}

	public static function apiQuery($JSONRequest, $methodName){
		self::initializeReturnArray();
		$arrayRequest = json_decode($JSONRequest, true);
		switch($methodName){
			case "addInventoryItems":
				self::addInventoryItems(self::$returnArray, $arrayRequest);
				break;
			case "copyInventoryItems":
				self::copyInventoryItems(self::$returnArray, $arrayRequest);
				break;
			case "archiveInventoryItemsByID":
				self::archiveInventoryItems(self::$returnArray, $arrayRequest);
				break;
			case "wasteInventoryItems":
				self::wasteInventoryItems(self::$returnArray, $arrayRequest);
				break;
			case "transferInventoryItems":
				self::transferInventoryItems(self::$returnArray, $arrayRequest);
				break;
			default:
				self::$returnArray['Status']  = "Failure";
				self::$returnArray['Action'] = "Unknown Request";
				self::$returnArray['DataResponse'] = 400;
				break;
		}

		return json_encode(self::$returnArray);
	}

	private static function addInventoryItems(&$returnArray, $inventoryItemArray){
		$returnArray['RequestID'] = rand(0,999999);
		$returnArray['Action'] = $inventoryItemArray['Action'];
		$returnArray['DataResponse'] = "Inserted " . count($inventoryItemArray['Params']['Items']) . " InventoryItems";
	}

	private static function copyInventoryItems(&$returnArray, $inventoryItemArray){
		$returnArray['RequestID'] = rand(0,999999);
		$returnArray['Action'] = $inventoryItemArray['Action'];
		$returnArray['DataResponse'] = "Copied " . count($inventoryItemArray['Params']['Items']) . " InventoryItems";
	}

	private static function archiveInventoryItems(&$returnArray, $archiveInventoryItemIDArray){
		$returnArray['RequestID'] = rand(0,999999);
		$returnArray['Action'] = $archiveInventoryItemIDArray['Action'];
		$returnArray['DataResponse'] = "Archived " . count($archiveInventoryItemIDArray['Params']['Items']) . " InventoryItems";
	}

	private static function wasteInventoryItems(&$returnArray, $idAndQuantityArray){
		$returnArray['RequestID'] = rand(0,999999);
		$returnArray['Action'] = $idAndQuantityArray['Action'];
		$returnArray['DataResponse'] = "Wasted " . count($idAndQuantityArray['Params']['Items']) . " InventoryItems";
	}

	private static function transferInventoryItems(&$returnArray, $idAndQuantityArray){
		$returnArray['RequestID'] = rand(0,999999);
		$returnArray['Action'] = $idAndQuantityArray['Action'];
		$returnArray['DataResponse'] = "Transferred " . count($idAndQuantityArray['Params']['Items']) . " InventoryItems";
	}
}