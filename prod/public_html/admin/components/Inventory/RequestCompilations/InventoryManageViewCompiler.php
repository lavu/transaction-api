<?php
require_once __DIR__ . "/../RequestFormatters/BasicFormatter.php";
require_once __DIR__ . "/../RequestFormatters/InventoryItemRequestFormatter.php";
require_once __DIR__ . "/../RequestFormatters/InventoryVendorItemRequestFormatter.php";
require_once __DIR__ . "/../RequestFormatters/InventoryPurchaseOrderItemRequestFormatter.php";

require_once __DIR__ . "/../InventoryObjectControllers/InventoryItemController.php";
require_once __DIR__ . "/../InventoryObjectControllers/InventoryVendorItemController.php";
require_once __DIR__ . "/../InventoryObjectControllers/InventoryPurchaseOrderItemController.php";

/**
 * Created by PhpStorm.
 * User: Leif
 * Date: 4/24/17
 * Time: 11:20 AM
 */
class InventoryManageViewCompiler extends BasicFormatter
{
	/**
	 * @return string
	 */
	public function getAllManageViewRows($archived = false,$request = array())
	{
		$inventoryItemController = new InventoryItemController();
		$inventoryCategoryController = new InventoryCategoryController();
		$inventoryStorageLocationController = new InventoryStorageLocationController();
		$inventoryVendorController = new InventoryVendorController();

		if($archived === false){
			$inventoryItems = self::decodeLavuAPIResponse($inventoryItemController->getAllInventoryItems($request));
			$categories = self::decodeLavuAPIResponse($inventoryCategoryController->getAllCategories());
			$storageLocations = self::decodeLavuAPIResponse($inventoryStorageLocationController->getAllStorageLocations());
			$vendors = self::decodeLavuAPIResponse($inventoryVendorController->getAllVendors());
			//if min & max search filters are applied total count is being send.
			if(isset($request['stock_unit']) && $request['stock_unit'] != "" && ($request['low_stock_min'] != '' || $request['low_stock_max'] != '')){
				$requestinvcount[0]['inventoryitemcount'] = count($inventoryItems);
				$inventoryItems = array_slice($inventoryItems,$request['limit']);
			}else{
				$requestinvcount = InventoryQueries::getAllInventoryItemsCount($request);
			}
		}else{
			$inventoryItems = self::decodeLavuAPIResponse($inventoryItemController->getAllArchivedInventoryItems());
			$categories = self::decodeLavuAPIResponse($inventoryCategoryController->getAllCategoriesWithArchives());
			$storageLocations = self::decodeLavuAPIResponse($inventoryStorageLocationController->getAllStorageLocationsWithArchives());
			$vendors = self::decodeLavuAPIResponse($inventoryVendorController->getAllVendorsWithArchives());

			$request = array('_deleted'=>'1');
			$requestinvcount = InventoryQueries::getAllInventoryItemsCount($request);
		}


		//We are purposefully using null for beginning and ending dates. These are explicitly sent so that in the event
		//that requirements change and dates may be set for average periodic in the mange view, all we'll need to do is
		//populate these dates.
		$beginDate = null;
		$endDate = null;
		$costData = self::decodeLavuAPIResponse($inventoryItemController->getCostData());
		$costCalculator = new CostCalculator();
		foreach($inventoryItems as $i=>$key){
			$id = $inventoryItems[$i]['id'];
			$quantityOnHand = $inventoryItems[$i]['quantityOnHand'];
			$cost = self::decodeLavuAPIResponse($inventoryItemController->calculateValueForItems(array('Items'=>array('id'=>$id))));
			$inventoryItems[$i]['cost'] = $cost[$id]['value'];
			$unitMeasure = InventoryQueries::getUnitMeasureSymbol($inventoryItems[$i]['recentPurchaseUnitID'],$inventoryItems[$i]['categoryID']);
			if (!empty($unitMeasure)) {
				$inventoryItems[$i]['unitSymbol'] = $unitMeasure['symbol'];
				$inventoryItems[$i]['unitName'] = $unitMeasure['name'];
			}
		}

		//$inventoryItems = self::joinFieldsOrRowsFromTable2ToTable1($inventoryItems, $categories,        'categoryID',       'id', array('name'),    array('categoryName'));
		//$inventoryItems = self::joinFieldsOrRowsFromTable2ToTable1($inventoryItems, $storageLocations,  'storageLocation',  'id', array('name'),    array('storageLocationName'));
		//$inventoryItems = self::joinFieldsOrRowsFromTable2ToTable1($inventoryItems, $vendors,           'primaryVendorID',  'id', array('name'),    array('vendorName'));

		//Updated By Dhruvpalsinh on 30-01-2019
		$invcount = $requestinvcount[0]['inventoryitemcount'];
		
		#if sorting is being applied on value on inventory list view
		$excludeSortingFields = array("storageLocation","cost","categoryID","primaryVendorID","unitSymbol","quantityOnHand");
		
		if(isset($request['short_by']) && in_array($request['short_by'],$excludeSortingFields)){
			if($request['short_by'] == 'categoryID'){
				$request['short_by'] = "categoryName";
			}elseif($request['short_by'] == 'primaryVendorID'){
					$request['short_by'] = "vendorName";
			}elseif($request['short_by'] == 'storageLocation'){
				$request['short_by'] = "storageLocationName";
			}elseif($request['short_by'] == 'quantityOnHand'){
				$request['short_by'] = "perQuantityOnHand";
			}
			$inventoryItems = $this->getSortedValue($inventoryItems, $request['short_by'],$request['order_by'], $request['limit']);
		}
		
		return self::encodeLavuAPIResponse($inventoryItems,$invcount);
	}

	public function getInventoryItemDrillDownsByID($request)
	{
		$itemFormatter = new InventoryItemRequestFormatter();
		$itemController = new InventoryItemController();
		$formattedRequestForInventoryItems = $itemFormatter->formatGetInventoryItemsByID($request);
		$inventoryItems = self::decodeLavuAPIResponse($itemController->getInventoryItemsByID($formattedRequestForInventoryItems));
		if (isset($inventoryItems['id'])) {
			$id = $inventoryItems['id'];
			$vendorItems = self::decodeLavuAPIResponse((new InventoryVendorItemController())->getLinkedVendorItemsForInventoryItems(array($id)));
			$inventoryItems['vendor_items'] = $vendorItems;
		}
		return self::encodeLavuAPIResponse($inventoryItems);
	}

	public function getInventoryItemLinkedItems($request){
	    $itemController = new InventoryVendorItemController();
	    $vendorItems = self::decodeLavuAPIResponse(($itemController->getLinkedVendorItemsForInventoryItems($request)));
	    return self::encodeLavuAPIResponse($vendorItems);
    }
	/** //TODO: redo this list.
	 * Items that will be passed from the front end whenever a Manage View Add Item is requested:
	"itemInstance": optional, (Link VendorItemsToInventoryItems),

	"name": required, (create InventoryItems),
	"lowQuantity": default 0 (86 Count in the front end) (create InventoryItems),
	"critical_item": default 0 (defaults to not a critical item) (create InventoryItems),
	"reOrderQuantity": default 0, (create InventoryItems),
	"primaryVendorId": required, (create InventoryItems),
	"categoryID": required, (create InventoryItems),
	"storageLocation": required, (create InventoryItems),
	"salesQuantity": default 0, (create InventoryItems - quantity)
	"salesUnitID": required, (create InventoryItems)

	inventoryItem id's should be returned for vendor_item.
	"BIN": optional, (create VendorItems),
	"serial": optional,(create VendorItems),
	"SKU": optional,(create VendorItems),
	-> Vendor Items should return id's for use in the purchaseOrderItems
	"purchaseCost": required, (create purchaseOrderItems),
	"purchaseQuantity": default 0,  (create purchaseOrderItems->quantityOrdered)
	"purchaseUOMId": required, (create purchaseOrderItems->purchaseUnitID)
	 */
	public function createManageViewAddItem($request){
 		$queryIDs = array();
		$request = self::changePassedVariableNameToUsableVariableName($request, 'primaryVendorID', 'vendorID');

		$itemFormatter = new InventoryItemRequestFormatter();
		$itemController = new InventoryItemController();
		$formattedInventoryItemRequest = $itemFormatter->formatCreateInventoryItems($request);
		$response = $itemController->createInventoryItems($formattedInventoryItemRequest);
		$response = json_decode(json_encode($response),true);

		$resultArray = $response;
		$rollbackArray = $response;

		$queryIDs = self::addRequestIDsToArray($queryIDs, $rollbackArray);
		if(self::rollbackOnFailElseContinue($queryIDs,$rollbackArray) === true){
			return self::formattedFailureResponse($queryIDs, $rollbackArray);
		}

		$vendorItemRequestFormatter = new InventoryVendorItemRequestFormatter();
		$vendorItemController = new InventoryVendorItemController();

		$vendorItemRequest = self::appendIDsToFutureRequests($request, $response, 'inventoryItemID');

		foreach($vendorItemRequest as $i => $vendorItemRq){
			$vendorItemRequest[$i]['primary_vendor_id'] = $vendorItemRequest[$i]['vendorID'];
		}

		$formattedVendorItemRequest = $vendorItemRequestFormatter->formatCreateVendorItems($vendorItemRequest);
		$response = $vendorItemController->createVendorItems($formattedVendorItemRequest);
		$response = json_decode(json_encode($response),true);

		$queryIDs = self::addRequestIDsToArray($queryIDs, $rollbackArray);
		if(self::rollbackOnFailElseContinue($queryIDs,$rollbackArray) === true){
			return self::formattedFailureResponse($queryIDs, $rollbackArray);
		}

		$purchaseOrderItemFormatter = new InventoryPurchaseOrderItemRequestFormatter();
		$purchaseOrderItemController = new InventoryPurchaseOrderItemController();

		$purchaseOrderItemRequest = self::appendIDsToFutureRequests($request,$response,'vendorItemID');
		//$formattedPurchaseOrderItemRequest = $purchaseOrderItemFormatter->formatCreatePurchaseOrderItems($purchaseOrderItemRequest);
		//$response = $purchaseOrderItemController->createPurchaseOrderItems($formattedPurchaseOrderItemRequest);
		$response = json_decode(json_encode($response),true);

		$queryIDs = self::addRequestIDsToArray($queryIDs, $rollbackArray);
		if(self::rollbackOnFailElseContinue($queryIDs,$rollbackArray)){
			return self::formattedFailureResponse($queryIDs,$rollbackArray);
		}else{
//			$result = self::formattedSuccessResponse($queryIDs);
			$result = $this->useIncrementalIDArrayInResult($resultArray);
			return $result;
		}
	}

	private function useIncrementalIDArrayInResult($result){
		$id = $result['DataResponse']['DataResponse']['InsertID'];
		$idString = $id."";
		for($i = 1; $i < $result['DataResponse']['DataResponse']['RowsUpdated']; $i++){
			$idString .= ",".($id + $i);
		}
		$result['DataResponse']['DataResponse']['InsertID'] = $idString;
		return $result;
	}

/**
 * Data being sent.
 * [{
	* "linkingItem":"testItemLink",
	* "name":"2",
	* "orderedPrice":"50",
	* "receivedPrice":"60",
	* "id":"2",
	* "quantity":"50",
	* "primaryVendorID":"2",
	* "categoryID":"3",
	* "storageLocation":"1",
	* "recentPurchaseUnitID":"2",
	* "date":"2017-05-11T15:02:40.732Z"
 * },
 */


/**
 * receive without purchase order will do a few things
 * 1. it may or may not link/associate the received item(s) to vendor(s)
 * 2. add purchased quantity to on-hand quantity
 * 3. add a Receive without PO action to the audit trail, which includes cost per unit, and the quantity of those units.
 *
 */
	public function receiveWithoutPurchaseOrder($receivedItems){ //receiving should include "purchase cost" as "cost"
		$inventoryItemFormatter = new InventoryItemRequestFormatter();
		$inventoryItemController = new InventoryItemController();

		$inventoryAuditRequestFormatter = new InventoryAuditRequestFormatter();
		$inventoryAuditController = new InventoryAuditController();

		for($i = 0; $i<count($receivedItems);$i++){
			$receivedItems[$i]['id'] = $receivedItems[$i]['inventoryItemID'];
		}

		$inventoryItemUpdate = $inventoryItemFormatter->formatReceiveWithoutPO($receivedItems); //TODO: List vars for this.
		$result = self::decodeLavuAPIResponse($inventoryItemController->receiveWithoutPurchaseOrder($inventoryItemUpdate));

		$auditRequest = $inventoryAuditRequestFormatter->formatCreateAuditRequest($receivedItems, 'receiveWithoutPO');
		self::decodeLavuAPIResponse($inventoryAuditController->createReceiveWithoutPOAudit($auditRequest));

		return self::encodeLavuAPIResponse($result);;
	}

	public function copyInventoryItems($request)
	{
		$itemController = new InventoryItemController();
		$inventoryItems = self::decodeLavuAPIResponse($itemController->getInventoryItemsByIDs($request));
		$sUOMS = self::decodeLavuAPIResponse($itemController->getStandardUnitConversion());
		if(!empty($inventoryItems)) {
			foreach ($inventoryItems as $key => $itemsDetails) {
				$args['inventory_id'] = $itemsDetails['id'];
				$args['vendor_id'] = $itemsDetails['primaryVendorID'];
				$vendorItems = self::decodeLavuAPIResponse((new InventoryVendorItemController())->getVendorItemsByVendorAndInventoryID($args));
				$inventoryItems[$key]['name'] = $itemsDetails['name'] . " - Copy";
				$inventoryItems[$key]['SKU'] = $vendorItems[0]['SKU'];
				$inventoryItems[$key]['barcode'] = $vendorItems[0]['barcode'];
				$inventoryItems[$key]['BIN'] = $vendorItems[0]['BIN'];
				$inventoryItems[$key]['serialNumber'] = $vendorItems[0]['serialNumber'];
				$inventoryItems[$key]['inventoryOverage'] = 0;
				$inventoryItems[$key]['vendorID'] = $itemsDetails['primaryVendorID'];
				if ($itemsDetails['recentPurchaseUnitID'] != $itemsDetails['salesUnitID']) {
					$unitIdArray['pUOM'] = $itemsDetails['salesUnitID'];
					$unitIdArray['sUOM'] = $itemsDetails['recentPurchaseUnitID'];
					$unitIdArray['sUOMS'] = $sUOMS;
					$conversion = self::decodeLavuAPIResponse($itemController->getUnitConversion($unitIdArray));
					$inventoryItems[$key]['quantityOnHand'] = $itemsDetails['quantityOnHand'] * $conversion;
				}
				unset($inventoryItems[$key]['id']);

			}
			return $this->createManageViewAddItem($inventoryItems);
		}
	}

	public function exportInventoryItems()
	{
		$fileName = 'inventory_items.csv';
		$inventoryItems = self::decodeLavuAPIResponse($this->getAllManageViewRows());
		$data = 'No Record found';
		if (!empty($inventoryItems)) {
			$data = implode(',', str_replace(',', ' ', array_keys($inventoryItems[0]))) . "\n";
			foreach ($inventoryItems as $details) {
				$data .=  implode(',', str_replace(',', ' ', $details)) . "\n";
			}
		}
		$result = self::exportCsv($data, $fileName);
		return true;
	}
	/**
	 * params: $inventoryItems
	 * This function will call while sorting is applied on value on inventory list page
	 * It will create a temporary table and insert all inventory data
	 * Apply the sorting on cost and return the result 
	 * return $inventoryItems
	 */
	public function getSortedValue($inventoryItems, $sort_by,$order_by, $limit){
		$limit = " LIMIT ".$limit;
		$temoQuery = " CREATE TEMPORARY TABLE temp_inventory_search (
			`id` int NOT NULL,
			`name` varchar(255),
			`categoryID` int,
			`categoryName` varchar(255),
			`cost` decimal(20,5),
			`criticalItem` tinyint(1),
			`inventoryOverage` double unsigned NOT NULL DEFAULT '0',
			`lowQuantity` double unsigned NOT NULL DEFAULT '0',
			`primaryVendorID` int(11) unsigned DEFAULT '0',
			`quantityOnHand` double unsigned DEFAULT '0',
			`perQuantityOnHand` double unsigned DEFAULT '0',
			`reOrderQuantity` double unsigned NOT NULL DEFAULT '0',
			`recentPurchaseCost` double unsigned DEFAULT '0',
			`recentPurchaseQuantity` double unsigned DEFAULT '0',
			`recentPurchaseUnitID` int(11) unsigned DEFAULT '0',
			`salesUnitID` int(11) unsigned DEFAULT '0',
			`storageLocation` int(11) unsigned DEFAULT '0',
			`storageLocationName` varchar(255),
			`vendorName` varchar(255),
			`wasteCost` int,
			`unitName` varchar(40),
			`unitSymbol` varchar(20),
			`_deleted` tinyint(1) unsigned NOT NULL DEFAULT '0',
			PRIMARY KEY(id))";

		#Create temporary table
		$tempTable = DBConnection::clientQuery($temoQuery);

		$insertQuery = "INSERT into temp_inventory_search VALUES ";
		$insertQueryVal = "";
		$count = 0;
		foreach($inventoryItems as $record){
			if($count > 0) {
				$insertQueryVal .=',';
			}
			$insertQueryVal .= '('.$record['id'].',
			"'.addslashes($record['name']).'",
			"'.$record['categoryID'].'",
			"'.$record['categoryName'].'",
			"'.$record['cost'].'",
			"'.$record['criticalItem'].'",
			"'.$record['inventoryOverage'].'",
			"'.$record['lowQuantity'].'",
			"'.$record['primaryVendorID'].'",
			"'.$record['quantityOnHand'].'",
			"'.$record['perQuantityOnHand'].'",
			"'.$record['reOrderQuantity'].'",
			"'.$record['recentPurchaseCost'].'",
			"'.$record['recentPurchaseQuantity'].'",
			"'.$record['recentPurchaseUnitID'].'",
			"'.$record['salesUnitID'].'",
			"'.$record['storageLocation'].'",
			"'.$record['storageLocationName'].'",
			"'.$record['vendorName'].'",
			"'.$record['wasteCost'].'",
			"'.$record['unitName'].'",
			"'.$record['unitSymbol'].'",
			"'.$record['_deleted'].'")';
			$count++;
		}

		$truncateTempTable = DBConnection::clientQuery("truncate temp_inventory_search");

		$insertData = DBConnection::clientQuery($insertQuery.$insertQueryVal);
		#Select inventory serach data from temporary table
		$selectQuery = "SELECT * FROM temp_inventory_search ORDER BY $sort_by $order_by $limit ";
		$newInventoryItemsObj = DBConnection::clientQuery($selectQuery);

		return  MarshallingArrays::resultToArray($newInventoryItemsObj);
	}
}
