<?php
require_once __DIR__ . "/../RequestFormatters/BasicFormatter.php";

require_once __DIR__ . "/../InventoryObjectControllers/InventoryItemController.php";
require_once __DIR__ . "/../InventoryObjectControllers/InventoryVendorController.php";
require_once __DIR__ . "/../InventoryObjectControllers/InventoryVendorItemController.php";
require_once __DIR__ . "/../InventoryObjectControllers/InventoryPurchaseOrderItemController.php";
require_once __DIR__ . "/../InventoryObjectControllers/InventoryPurchaseOrderController.php";
require_once __DIR__. "/../../../api_private/Components/Inventory/InventoryQueries.php";

/**
 * Created by PhpStorm.
 * User: Leif
 * Date: 5/2/17
 * Time: 3:02 PM
 */
class InventoryPurchaseOrderViewCompiler extends BasicFormatter
{
	/**
	 * Returns items for the purchase order view, which displays purchase order information.
	 * return Purchase Order Number, Invoice Number, Date Created, Vendor, Status.
	 */
	public function getPurchaseOrderMainView($pid = array()){
		$purchaseOrderController = new InventoryPurchaseOrderController();
		$purchaseOrderItemController = new InventoryPurchaseorderItemController();
		$vendorController = new InventoryVendorController();
		$purchaseOrders =       self::decodeLavuAPIResponse($purchaseOrderController->getAllPurchaseOrders($pid));
		$purchaseOrderItems =   self::decodeLavuAPIResponse($purchaseOrderItemController->getAllPurchaseOrderItems());
		$vendors =              self::decodeLavuAPIResponse($vendorController->getAllVendors());
		$purchaseOrders =       self::joinFieldsOrRowsFromTable2ToTable1($purchaseOrders, $vendors,  'vendorID', 'id', array('name'),  array('vendor'));
		$purchaseOrders =       self::joinTable2RowsIntoTable1IfFieldsMatch($purchaseOrders, $purchaseOrderItems, 'purchaseOrderItems', 'id', 'purchaseOrderID');

		$purchaseOrders =       self::addStatusesToPurchaseOrders($purchaseOrders);
		$purchaseOrders =       self::getPurchaseOrderTotals($purchaseOrders);

		$resultArray = array();

		for($i = 0; $i < count($purchaseOrders); $i++){
			$po = $purchaseOrders[$i];
			$iRes = array();
			$iRes['orderNumber'] = $po['id'];
			$iRes['invoiceNumber'] = $po['invoiceNumber'];
			$iRes['dateCreated'] = $po['orderDate'];
			$iRes['vendorName'] = $po['vendor'];
			$iRes['poTotalAmount'] = $po['totalAmount'];
			$iRes['status'] = $po['status'];
			$resultArray[$i] = $iRes;
		}

		$porowResult = InventoryQueries::getAllCount('inventory_purchaseorder',0);
		$pordercount = $porowResult[0]['count'];
		return self::encodeLavuAPIResponse($resultArray,$pordercount);
	}

	/**
	 * Initial state is pending
	 * 'pending' if order date created, sent, received, receiveDate are all 0, accepted is -1. (initially it is pending)
	 *
	 * Next state is either declined or accepted
	 * 'declined' if order date is created and accepted is 0.
	 * 'accepted' if order date is created and accepted is 1.
	 *
	 * 'sent' if order date is created, accepted is 1, and sent is 1.
	 *
	 * 'partial' if order date is created, receive date is set, sent is set to 1, accepted is set to 1, and order items are missing
	 *
	 * 'closed'  if order date is created, receive date is set, sent is set to 1, accepted is set to 1
	 */
	public static function addStatusesToPurchaseOrders($purchaseOrders){
		for($i = 0; $i < count($purchaseOrders); $i++){
			if(isset($purchaseOrders[$i]['purchaseOrderItems'])){
				 $missingItems = self::purchaseOrderHasMissingItems($purchaseOrders[$i]['purchaseOrderItems']);
			}else{
				$missingItems = false;
			}
			$purchaseOrders[$i]['status'] = 'pending';

//			if($purchaseOrders[$i]['accepted'] == 1){
//				$purchaseOrders[$i]['status'] = 'accepted';
//			}
//			else if($purchaseOrders[$i]['accepted'] == 0){ //TODO: we need vendor integration for this, so comment out then.
//				$purchaseOrders[$i]['status'] = 'declined';
//				continue; //no more stages after declining, so exit the loop.
//			}

			if($purchaseOrders[$i]['sent'] == 1){
				$purchaseOrders[$i]['status'] = 'sent';
			}

			if($purchaseOrders[$i]['receiveDate'] != '0000-00-00 00:00:00'){
				if($missingItems === true){
					$purchaseOrders[$i]['status'] = 'partial';
				}
				else
				{
					$purchaseOrders[$i]['status'] = 'closed';
				}
			}
		}
		return $purchaseOrders;
	}

	public static function purchaseOrderHasMissingItems($purchaseOrderItems){
		$missingItems = false;
		if(isset($purchaseOrderItems))
		{
			for ($j = 0; $j < count($purchaseOrderItems); $j++)
			{
				if ($purchaseOrderItems[$j]['quantityReceived'] < $purchaseOrderItems[$j]['quantityOrdered'])
				{
					$missingItems = true;
					break;
				}
			}
		}
		return $missingItems;
	}

	public static function getPurchaseOrderTotals($purchaseOrders){
		for($i = 0; $i < count($purchaseOrders); $i++){
			$cost = 0;
			if(isset($purchaseOrders[$i]['purchaseOrderItems']))
			{
				$items = $purchaseOrders[$i]['purchaseOrderItems'];

				for ($j = 0; $j < count($items); $j++)
				{
					$itemsJ = $items[$j];
					if ($itemsJ['quantityReceived'] > 0)
					{
						$cost += ($itemsJ['quantityReceived'] * $itemsJ['purchaseUnitCost']);
					} else
					{
						$cost += ($itemsJ['quantityOrdered'] * $itemsJ['purchaseUnitCost']);
					}
				}
			}
			$purchaseOrders[$i]['totalAmount'] = $cost;
		}
		return $purchaseOrders;
	}

	public function getPurchaseOrderCreationWindowData(){
		$vendorController = new InventoryVendorController();
		$vendorItemController = new InventoryVendorItemController();
		$inventoryItemController = new InventoryItemController();

		$vendorItems = self::decodeLavuAPIResponse($vendorItemController->getAllVendorItems());
		$inventoryItems = self::decodeLavuAPIResponse($inventoryItemController->getAllInventoryItems());
		$vendors = self::decodeLavuAPIResponse($vendorController->getAllVendors());

		$vendorItems = self::joinFieldsOrRowsFromTable2ToTable1($vendorItems,$inventoryItems,'inventoryItemID','id',array(),array('inventoryItem'));
		$vendors =  self::joinTable2RowsIntoTable1IfFieldsMatch($vendors, $vendorItems, 'vendorItems', 'id', 'vendorID');

		return self::encodeLavuAPIResponse($vendors);
	}

	public function getPurchaseOrderDrilldown($purchaseID){
		$poController = new InventoryPurchaseOrderController();
		$purchaseOrderItemController = new InventoryPurchaseOrderItemController();
		$vendorController = new InventoryVendorController();
		$vendorItemController = new InventoryVendorItemController();
		$inventoryItemController = new InventoryItemController();
		$unitController = new InventoryUnitController();
		$itemController = new InventoryItemController();

		$purchaseID = $this->formatIDArray($purchaseID);
		$purchaseOrder = self::decodeLavuAPIResponse($poController->getPurchaseOrderByID($purchaseID));
		
		$poID = array();
		$poID[] = array('id'=>$purchaseOrder['id']);

		$purchaseOrderItems =   self::decodeLavuAPIResponse($purchaseOrderItemController->getItemsForPurchaseOrder($poID));

		$vendorItems =          self::decodeLavuAPIResponse($vendorItemController->getAllVendorItems());
		$vendors =              self::decodeLavuAPIResponse($vendorController->getAllVendors());
		$vendorItems =   self::joinFieldsOrRowsFromTable2ToTable1($vendorItems, $vendors,'vendorID','id',array('name'),array('vendorName'));

		$items = self::decodeLavuAPIResponse($itemController->getAllInventoryItems());
		$vendorItems = self::joinFieldsOrRowsFromTable2ToTable1($vendorItems, $items,'inventoryItemID','id',array('name'),array('inventoryItemName'));

		$purchaseOrderItems =   self::joinFieldsOrRowsFromTable2ToTable1($purchaseOrderItems, $vendorItems,'vendorItemID','id',array(),array('vendorItems'));
		$units = self::decodeLavuAPIResponse($unitController->getAllUnits());
		$purchaseOrderItems =   self::joinFieldsOrRowsFromTable2ToTable1($purchaseOrderItems, $units,'purchaseUnitID','id',array('name'),array('purchaseUnitName'));

		$inventoryItems =       self::decodeLavuAPIResponse($inventoryItemController->getAllInventoryItems());
		$purchaseOrderItems =   self::joinFieldsOrRowsFromTable2ToTable1($purchaseOrderItems, $inventoryItems,'inventoryItemID','id',array(),array('inventoryItems'));

		$purchaseOrder['purchaseOrderItems'] = $purchaseOrderItems;
		$purchaseOrder = self::addStatusesToPurchaseOrders(array($purchaseOrder));

		return self::encodeLavuAPIResponse($purchaseOrder);
	}

	public function exportPurchaseOrders()
	{
		$fileName = 'Orders.csv';
		$purchaseOrders = self::decodeLavuAPIResponse(self::getPurchaseOrderMainView());
		$data = 'No Record found';
		if (!empty($purchaseOrders)) {
			$data = implode(',', str_replace(',', ' ', array_keys($purchaseOrders[0]))) . "\n";
			foreach ($purchaseOrders as $details) {
				$data .=  implode(',', str_replace(',', ' ', $details)) . "\n";
			}
		}
		$result = self::exportCsv($data, $fileName);
		return true;
	}
}