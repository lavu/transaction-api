<?php
require_once __DIR__ . "/../RequestFormatters/BasicFormatter.php";

require_once __DIR__ . "/../InventoryObjectControllers/InventoryItemController.php";
require_once __DIR__ . "/../InventoryObjectControllers/InventoryVendorController.php";
require_once __DIR__ . "/../InventoryObjectControllers/InventoryVendorItemController.php";
require_once __DIR__ . "/../InventoryObjectControllers/InventoryPurchaseOrderItemController.php";
require_once __DIR__ . "/../InventoryObjectControllers/InventoryPurchaseOrderController.php";
require_once __DIR__ . "/../InventoryObjectControllers/InventoryWasteController.php";

/**
 * Created by PhpStorm.
 * User: Leif
 * Date: 5/12/17
 * Time: 2:16 PM
 */
class InventoryDashboardViewCompiler extends BasicFormatter
{
	//get low stock items
	public function getInventoryDashboardView(){
		$inventoryItemController = new InventoryItemController();
		$lowStockItems = self::decodeLavuAPIResponse($inventoryItemController->getAllLowStockInventoryItems());

		$inventoryTransferController = new InventoryTransferController();
		$pendingTransfers = self::decodeLavuAPIResponse($inventoryTransferController->getAllPendingTransfers());

		$inventoryPurchaseOrderController = new InventoryPurchaseOrderController();
		$inTransitOrders = self::decodeLavuAPIResponse($inventoryPurchaseOrderController->getExpectedPurchaseOrders());

		$wasteController = new InventoryWasteController();
		$waste = self::decodeLavuAPIResponse($wasteController->getInventoryWasteCosts());

		$costData = self::decodeLavuAPIResponse($inventoryItemController->calculateTotalValue(array()));
		$costTimeLine = self::decodeLavuAPIResponse($inventoryItemController->getCostData());
		$costCalculator = new CostCalculator();
		$allItems = self::decodeLavuAPIResponse($inventoryItemController->getAllInventoryItems());

		$displayItems = $this->getOldestInventoryItemsByCategoryAfterDate($costTimeLine, 5);
		$result = array();
		$result['lowStockItems']    = count($lowStockItems);
		$result['pendingTransfers'] = $pendingTransfers;
		$result['totalValue']       = $costCalculator->calculateFIFOCostForAllInventoryItemsOnHand($allItems, $costData);
		$result['inTransitOrders']  = $inTransitOrders;
		$result['waste']            = $waste;
		$result['displayItems']     = $displayItems;

		return self::encodeLavuAPIResponse($result);
	}

	/**
	 * Category is gathered from the use first inventory dashboard setting, set in the CP.
	 * Date range is current day back to the number of days set in the display items that are x days old setting in the
	 * CP.
	 */
	private function getOldestInventoryItemsByCategoryAfterDate($costData, $numberOfItemsToRetrieve){
		$returnedItems = array();
		$inventoryItemController = new InventoryitemController();
		$vendorController = new InventoryVendorController();
		$settingsController = new InventorySettingsController();
		$settings = self::decodeLavuAPIResponse($settingsController->getInventorySettings());
		$use_first_inventory_categoryID = null;
		$display_items_that_are_x_days_old = null;

		foreach($settings as $i=>$key){ //pull out the settings we need.
			if ($settings[$i]['setting'] == 'use_first_inventory'){
				$use_first_inventory_categoryID = $settings[$i]['value'];
			}else if ($settings[$i]['setting'] == 'display_items_that_are_x_days_old'){
				$display_items_that_are_x_days_old = $settings[$i]['value'];
			}
		}
		$defaultTimezone = date_default_timezone_get();
		date_default_timezone_set("UTC");
		$oldestDate = gmdate('Y-m-d', strtotime('-'.$display_items_that_are_x_days_old.' days', strtotime(gmdate('Y-m-d'))));
		date_default_timezone_set($defaultTimezone);

		$usedItems = array();
		for($i = count($costData); $i > 0; $i--){
			if (isset($costData[$i]['date']) && strtotime($oldestDate) <= strtotime($costData[$i]['date'])) {
				$itemID = $costData[$i]['inventoryItemID'];
				$item = self::decodeLavuAPIResponse($inventoryItemController->getInventoryItemsByID(array(0=>array('id'=>$itemID))));

				if($item['categoryID'] == $use_first_inventory_categoryID && !in_array($itemID, $usedItems) && $item['_deleted'] != 1)
				{
					$usedItems[] = $itemID;
					$returnedItem = array();
					$vendor = self::decodeLavuAPIResponse($vendorController->getVendorsByID(array(0=>array('id'=>$item['primaryVendorID'])))); //needs to return an array
					$returnedItem['id'] = $item['id'];
					$returnedItem['vendorName'] = $vendor['name'];
					$returnedItem['itemName'] = $item['name'];
					$returnedItem['quantity'] = $item['convertedQuantityOnHand'];
					$returnedItems[] = $returnedItem;
					if ($numberOfItemsToRetrieve == 0)
					{
						break;
					}
				}
			}
		}
		return $returnedItems;
	}

	private function getTotalInventoryValue($inventoryItems){
		$totalValue = 0;
		for($i = 0; $i < count($inventoryItems); $i++){
			$item = $inventoryItems[$i];
			$totalValue += $item['quantityOnHand'] * $item['recentPurchaseCost'];
		}
		return $totalValue;
	}

	public function getLowStockLevel(){
		$inventoryItemController = new InventoryItemController();
		$lowStockItems = self::decodeLavuAPIResponse($inventoryItemController->getAllLowStockInventoryItems());
		$result = array();
		$result['lowStockItems'] = count($lowStockItems);
		return self::encodeLavuAPIResponse($result);
	}

	public function getInventoryValue(){
		$inventoryItemController = new InventoryItemController();
		$costData = self::decodeLavuAPIResponse($inventoryItemController->calculateTotalValue(array()));
		$costCalculator = new CostCalculator();
		$allItems = self::decodeLavuAPIResponse($inventoryItemController->getAllInventoryItems());

		$result = array();
		$result['totalValue'] = $costCalculator->calculateFIFOCostForAllInventoryItemsOnHand($allItems, $costData);

		return self::encodeLavuAPIResponse($result);
	}

	public function getPendingTransfers(){
		$inventoryTransferController = new InventoryTransferController();
		$pendingTransfers = self::decodeLavuAPIResponse($inventoryTransferController->getAllPendingTransfers());
		$result = array();
		if(empty($pendingTransfers)) {
			$result['pendingTransfers'] = 0;
		}
		$result['pendingTransfers'] = count($pendingTransfers);
		return self::encodeLavuAPIResponse($result);
	}

	public function getWasteValue(){
		$wasteController = new InventoryWasteController();
		$waste = self::decodeLavuAPIResponse($wasteController->getInventoryWasteCosts());
		$result = array();
		$result['waste'] = $waste;

		return self::encodeLavuAPIResponse($result);
	}

	public function getpurchaseOrders(){
		$inventoryPurchaseOrderController = new InventoryPurchaseOrderController();
		$inTransitOrders = self::decodeLavuAPIResponse($inventoryPurchaseOrderController->getExpectedPurchaseOrders());
		$result = array();
		$result['inTransitOrders']  = $inTransitOrders;

		return self::encodeLavuAPIResponse($result);
	}

	public function getdisplayItems(){
		$inventoryItemController = new InventoryItemController();
		$costTimeLine = self::decodeLavuAPIResponse($inventoryItemController->getCostData());
		$displayItems = $this->getOldestInventoryItemsByCategoryAfterDate($costTimeLine, 5);
		$result = array();
		$result['displayItems']     = $displayItems;
		return self::encodeLavuAPIResponse($result);
	}

//	public function getUseFirstInventory(){
//
//	}
	//TODO: Later: get use-first inventory


}