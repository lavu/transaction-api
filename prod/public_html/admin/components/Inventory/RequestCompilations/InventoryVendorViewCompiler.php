<?php
require_once __DIR__ . "/../RequestFormatters/BasicFormatter.php";

require_once __DIR__ . "/../InventoryObjectControllers/InventoryItemController.php";
require_once __DIR__ . "/../InventoryObjectControllers/InventoryVendorController.php";
require_once __DIR__ . "/../InventoryObjectControllers/InventoryVendorItemController.php";
require_once __DIR__ . "/../InventoryObjectControllers/InventoryPurchaseOrderItemController.php";
require_once __DIR__ . "/../InventoryObjectControllers/InventoryPurchaseOrderController.php";
require_once __DIR__ . "/../InventoryObjectControllers/InventoryUnitController.php";

/**
 * Created by PhpStorm.
 * User: Leif
 * Date: 5/4/17
 * Time: 11:23 AM
 *
 * This function is currently unused; InventoryVendorController->getVendorsOrderedItems() but was kept for now in case it's used in the future
 */
class InventoryVendorViewCompiler extends BasicFormatter
{
	//pulls the latest items ordered from the vendor.
	public function getLatestVendorSpecificOrderedItemsByVendorID($idList){
		$ids = $this->formatIDArray($idList);
		$resultArray = array();
		foreach($ids as $key=>$vendor)
		{
			$purchaseOrderItemController = new InventoryPurchaseOrderItemController();
			$purchaseOrderController = new InventoryPurchaseOrderController();
			$purchaseUnitController = new InventoryUnitController();

			$purchaseOrder = self::decodeLavuAPIResponse($purchaseOrderController->getLatestPurchaseOrderForVendorID($vendor));
			$purchaseOrder = array_shift($purchaseOrder);

			$purchaseUnits =  self::decodeLavuAPIResponse($purchaseUnitController->getAllUnits());

			$purchaseOrderItems =  self::decodeLavuAPIResponse($purchaseOrderItemController->getItemsForPurchaseOrder($purchaseOrder['id']));
			$purchaseOrderItems = self::joinFieldsOrRowsFromTable2ToTable1($purchaseOrderItems, $purchaseUnits, 'purchaseUnitID','id', array('symbol'),array('purchaseUnitName'));
			$purchaseOrder['purchaseOrderItems'] = $purchaseOrderItems;

			$resultArray[] = $purchaseOrder;
		}

		// TO DO : get purchaseorder items by vendor id., then pull out the ones with the latest date.

		return self::encodeLavuAPIResponse($resultArray);
	}

	//needs to return order#, invoice#, status, order date, and order total
	public function getVendorSpecificPreviousOrders($idList){
		$ids = $this->formatIDArray($idList);
		$resultArray = array();
		foreach($ids as $key=>$vendor)
		{
			$purchaseOrderItemController = new InventoryPurchaseOrderItemController();
			$purchaseOrderController = new InventoryPurchaseOrderController();
			$purchaseUnitController = new InventoryUnitController();
			$purchaseOrderViewCompiler = new InventoryPurchaseOrderViewCompiler();

			$purchaseOrders = self::decodeLavuAPIResponse($purchaseOrderController->getPurchaseOrdersForVendorID(array($vendor)));
			$purchaseOrders = array_slice($purchaseOrders, 0, 10, true);

			$purchaseOrderOrderedCost = 0;
			$purchaseOrderReceivedCost = 0;
			for($i = 0; $i < count($purchaseOrders); $i++){
				$purchaseOrder = $purchaseOrders[$i];
				$purchaseOrderItems =  self::decodeLavuAPIResponse($purchaseOrderItemController->getPurchaseOrderItemsByPurchaseOrderID(array(array("id"=>$purchaseOrder['id']))));
				$purchaseOrder['purchaseOrderItems'] = $purchaseOrderItems;
				for($j = 0; $j < count($purchaseOrderItems); $j++){
					$purchaseOrderOrderedCost  += ((double) $purchaseOrderItems[$j]['purchaseUnitCost'] * (double) $purchaseOrderItems[$j]['quantityOrdered']);
					$purchaseOrderReceivedCost += ((double) $purchaseOrderItems[$j]['purchaseUnitCost'] * (double) $purchaseOrderItems[$j]['quantityReceived']);
				}
				$purchaseOrders[$i]['orderedCost'] = $purchaseOrderOrderedCost;
				$purchaseOrders[$i]['receivedCost'] = $purchaseOrderReceivedCost;
				$purchaseOrders[$i]['purchaseOrderItems'] = $purchaseOrderItems;
				if($purchaseOrderReceivedCost <= 0)
				{
					$purchaseOrders[$i]['totalAmount'] = $purchaseOrderOrderedCost;
				}else{
					$purchaseOrders[$i]['totalAmount'] = $purchaseOrderReceivedCost;
				}
			}

			$purchaseOrders = $purchaseOrderViewCompiler::addStatusesToPurchaseOrders($purchaseOrders);
			$totalAmount = 0.00;
			foreach($purchaseOrders as $i=>$order){
				$totalAmount += (double)$order['receivedCost'];
			}
		}

		for($i = 0; $i < count($purchaseOrders); $i++){
			$resultArray[] = $purchaseOrders[$i];
		}
//			$resultArray['totalAmount'] = $totalAmount;

		return self::encodeLavuAPIResponse($resultArray);
	}

	public function exportVendors()
	{
		$fileName = 'Vendors.csv';
		$vendorController = new InventoryVendorController();
		$vendors = self::decodeLavuAPIResponse($vendorController->getAllVendors());
		$data = 'No Record found';
		if (!empty($vendors)) {
			$data = implode(',', str_replace(',', ' ', array_keys($vendors[0]))) . "\n";
			foreach ($vendors as $details) {
				$data .=  implode(',', str_replace(',', ' ', $details)) . "\n";
			}
		}
		$result = self::exportCsv($data, $fileName);
		return true;
	}
}