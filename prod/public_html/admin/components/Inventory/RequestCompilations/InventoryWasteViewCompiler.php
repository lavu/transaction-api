<?php
require_once __DIR__ . "/../RequestFormatters/BasicFormatter.php";
require_once __DIR__ . "/../InventoryObjectControllers/InventoryItemController.php";
require_once __DIR__ . "/../InventoryObjectControllers/InventoryAuditController.php";
//TODO: Uncoment below line after caching stable
//require_once __DIR__ . "/../Inventory86CountViewCompiler.php";

/**
 * Created by PhpStorm.
 * User: Leif
 * Date: 5/3/17
 * Time: 9:22 AM
 */
class InventoryWasteViewCompiler extends BasicFormatter
{

	public function performInventoryWaste($request){
		$itemFormatter = new InventoryItemRequestFormatter();
		$auditFormatter = new InventoryAuditRequestFormatter();
		$auditController = new InventoryAuditController();
		$itemController = new InventoryItemController();

		$createOverageAudit = false;
		$requestWithOverage = array();
		$overageAudits = array();
		$sUOMS = self::decodeLavuAPIResponse($itemController->getStandardUnitConversion());
		for($i = 0; $i < count($request); $i++){
			$dbItem = self::decodeLavuAPIResponse($itemController->getInventoryItemsByID(array(array('id'=>$request[$i]['id']))));
			$quantityOnHand = $dbItem['quantityOnHand'];
			if ($quantityOnHand > 0) {
				$request[$i]['inventoryItemID'] = $request[$i]['id'];
				$quantity = $request[$i]['quantity'];
				$unitIdArray['pUOM'] = $dbItem['recentPurchaseUnitID'];
				$unitIdArray['sUOM'] = $dbItem['salesUnitID'];
				$unitIdArray['sUOMS'] = $sUOMS;
				$conversion = self::decodeLavuAPIResponse($itemController->getUnitConversion($unitIdArray));
				$convertedQuantity = $quantity * $conversion;
				$cost = $request[$i]['cost'];
				$request[$i]['cost'] = round($quantity * $cost, 2);
				$overageQuantity = $convertedQuantity - $quantityOnHand;
				if ($overageQuantity > 0 && $quantityOnHand > 0) {
					//create overage audit using overage quantity instead of quantity
					$createOverageAudit = true;
					$requestWithOverage[$i] = $request[$i];
					$convertedOverageQuantity = round($overageQuantity / $conversion, 2);
					// If overage audit then calculate cost based on overage quantity
					$requestWithOverage[$i]['cost'] = $convertedOverageQuantity * $cost;
					$requestWithOverage[$i]['quantity'] = round($convertedOverageQuantity, 2);
					$overageAudits[] = $requestWithOverage[$i];
					$convertedQuantityOnHand = round($quantityOnHand / $conversion, 2);
					$request[$i]['quantity'] = $convertedQuantityOnHand;
					$request[$i]['cost'] = $convertedQuantityOnHand * $cost;
				}
			} else {
				unset($request[$i]);
			}
		}
		if (!empty($request)) {
			$item = $itemFormatter->formatWasteInventoryItems($request);

			$audit = $auditFormatter->formatCreateAuditRequest($request, 'waste');
			$auditController->createWasteAudit($audit);
			if ($createOverageAudit) {
				$overageAudit = $auditFormatter->formatCreateAuditRequest($requestWithOverage, 'waste_overage');
				$auditController->createWasteAudit($overageAudit);
			}
			$result = self::decodeLavuAPIResponse($itemController->wasteInventoryItems($item));
			return self::encodeLavuAPIResponse($result);
		} else {
			return self::encodeLavuAPIResponse(array("error" => "There is no waste item."));
		}
	}

    /**
     * Get all inventory item from menuitem_86, modifier_86, forced_modifier_86 table. Multiply waste quantity with
     * quantityUsed and then pass all parameter to get isnsert into inventory_audit.
     * @param $request [{"id":740, "wasteQuantity":3, "userID": 43456, "wasteReason":"Damage", "forcedModifierID":
     * [{"id":57,"quantity":1},{"id":58,"quantity":2},{"id":59, "quantity":5}], "modifierID":[{"id":16,"quantity":1},
     * {"id":34,"quantity":2}]}]
     * @return json string
     */
    public function createItemWasteAudits($request) {
        $inventoryItems = new InventoryItemController();
        $inventoryAuditController = new InventoryAuditController();

        $wasteQuantity = ($request[0]['wasteQuantity'] != "" && $request[0]['wasteQuantity'] > 0) ? $request[0]['wasteQuantity'] : 1;
        $inventoryAuditMenuItemsRequest = self::createMenuItemWasteAudits($request, $wasteQuantity);
        $inventoryAuditForcedModifierRequest = self::createForcedModifierWasteAudits($request, $wasteQuantity);
        $inventoryAuditModifierRequest = self::createModifierWasteAudits($request, $wasteQuantity);
        // Merge all inventory item ids gets from 86Count table
        $inventoryAuditRequest = array_merge($inventoryAuditMenuItemsRequest, $inventoryAuditForcedModifierRequest, $inventoryAuditModifierRequest);
        $requestAuditWasteResponse = array();
        if (!empty($inventoryAuditRequest)) {
            foreach ($inventoryAuditRequest as $inventoryDetails) {
                foreach ($inventoryDetails as $iid => $details) {
                    $inventoryIds[$iid]['id'] = $iid;
                }

            }

            // Get all inventory items data based on given inventory item ids
            $inventoryItemsResponse = self::decodeLavuAPIResponse($inventoryItems->getInventoryItemsByIDs($inventoryIds));
            // Add quantity of same inventory item
            foreach ($inventoryItemsResponse as $inventoryValues) {
                $inventoryUnitCostDetails[$inventoryValues['id']]['cost'] = $inventoryValues['recentPurchaseCost'];
                $inventoryUnitCostDetails[$inventoryValues['id']]['recentPurchaseUnitID'] = $inventoryValues['recentPurchaseUnitID'];
                $inventoryUnitCostDetails[$inventoryValues['id']]['quantityOnHand'] = $inventoryValues['quantityOnHand'];
            }

            $sUOMS = self::decodeLavuAPIResponse($inventoryItems->getStandardUnitConversion());
            $inventoryAuditCost = array();
            foreach ($inventoryAuditRequest as $inventoryDetails) {
                foreach ($inventoryDetails as $iid => $details) {
                    $unitIdArray['pUOM'] = $inventoryUnitCostDetails[$iid]['recentPurchaseUnitID'];
                    $unitIdArray['sUOM'] = $details['unitID'];
                    $unitIdArray['sUOMS'] = $sUOMS;
                    $conversion = self::decodeLavuAPIResponse($inventoryItems->getUnitConversion($unitIdArray));
                    $inventoryAuditCost[$iid][$details['unitID']]['cost'] = $inventoryUnitCostDetails[$iid]['cost'];;
                    $inventoryAuditCost[$iid][$details['unitID']]['quantity'] += $details['quantity'];
                    $inventoryAuditCost[$iid][$details['unitID']]['conversion'] = $conversion;
                }
            }

            foreach ($inventoryAuditCost as $inventoryItemId => $auditDetails) {
                foreach ($auditDetails as $unit => $details) {
                    $inventoryItemAuditData['inventoryItemID'] = $inventoryItemId;
                    $inventoryItemAuditData['unitID'] = $unit;
                    $inventoryItemAuditData['userID'] = $request[0]['userID'];
                    $inventoryItemAuditData['userNotes'] = $request[0]['wasteReason'];
                    $inventoryItemAuditData['datetime'] = $request[0]['datetime'];
                    $quantity = $details['quantity'];
                    $cost = $details['cost'];
                    $conversion = $details['conversion'];
                    $convertedQuantity = $quantity / $conversion;
                    $inventoryItemAuditData['cost'] = round($convertedQuantity * $cost, 2);
                    $inventoryItemAuditData['quantity'] = $quantity;

                    $overageQuantity = round($quantity - $inventoryUnitCostDetails[$inventoryItemId]['quantityOnHand'], 2);
                    if ($overageQuantity > 0) {
                        //create overage audit using overage quantity instead of quantity
                        $inventoryItemAuditData['action'] = 'waste_overage';
                        $inventoryItemAuditData['quantity'] = $overageQuantity;
                        $convertedOverageQuantity = $overageQuantity / $conversion;
                        // If overage audit then calculate cost based on overage quantity
                        $inventoryItemAuditData['cost'] = round($convertedOverageQuantity * $cost, 2);
                        $requestAuditOverage[$inventoryItemId] = $inventoryItemAuditData;
                        $quantityOnHand = $inventoryUnitCostDetails[$inventoryItemId]['quantityOnHand'];
                        $convertedQuantityOnHand = $quantityOnHand / $conversion;
                        $inventoryItemAuditData['quantity'] = $quantityOnHand;
                        // If overage audit then calculate waste cost on quantityOnHand
                        $inventoryItemAuditData['cost'] = round($convertedQuantityOnHand * $cost, 2);
                    }
                    $inventoryItemAuditData['action'] = 'waste';
                    $requestAuditWaste[$inventoryItemId] = $inventoryItemAuditData;
                }
            }

            //Get inventory_audit request data without inventory id as keys
            $requestAuditWasteRequest = array_values($requestAuditWaste);
            $requestAuditOverageRequest = array_values($requestAuditOverage);

            // Check if no audit waste request then no need to hit api call
            if (!empty($requestAuditWasteRequest)) {
                $requestAuditWasteResponse = $inventoryAuditController->createWasteAudit($requestAuditWasteRequest);
            }
            // Check if no audit waste overage request then no need to hit api call
            if (!empty($requestAuditOverageRequest)) {
                $requestAuditWasteResponse = $inventoryAuditController->createWasteAudit($requestAuditOverageRequest);
            }
        }

        return $requestAuditWasteResponse;
    }

    public function createMenuItemWasteAudits($request, $wasteQuantity) {
        $inventory86CountViewController = new Inventory86CountViewCompiler();
        $inventoryAuditMenuItemsRequest = array();
        if(isset($request[0]['id']) && $request[0]['id'] !="") {
            $munuItemID[0]['id'] = $request[0]['id'];
            $inventory86CountMenuItemResponse = $inventory86CountViewController->getMenuItem86CountQuantity($munuItemID, true);
            foreach ($inventory86CountMenuItemResponse as $id => $menuItemsObj) {
                foreach ($menuItemsObj as $menuDetails) {
                    $usedQuantity = $wasteQuantity * ($menuDetails['quantity'] != "" && $menuDetails['quantity'] > 0) ? $menuDetails['quantity'] : 1;
                    $inventoryAuditMenuItemsRequest[$id][$menuDetails['inventoryItemID']]['quantity'] = $wasteQuantity * $usedQuantity;
                    $inventoryAuditMenuItemsRequest[$id][$menuDetails['inventoryItemID']]['unitID'] = $menuDetails['unitID'];
                }
            }
        }
        return $inventoryAuditMenuItemsRequest;
    }

    public function createForcedModifierWasteAudits($request, $wasteQuantity) {
        $inventory86CountViewController = new Inventory86CountViewCompiler();
        $inventoryAuditForcedModifierRequest = array();
        if(isset($request[0]['forcedModifierID']) && !empty($request[0]['forcedModifierID'])) {
            $forcedModifierIdsRequest = array();
            $forcedModifierQuantityRequest = array();
            foreach ($request[0]['forcedModifierID'] as $key => $forceModifierData) {
                $forcedModifierIdsRequest[$key]['id'] = $forceModifierData['id'];
                $forcedModifierQuantityRequest[$forceModifierData['id']] = (isset($forceModifierData['quantity']) && $forceModifierData['quantity'] != '') ? $forceModifierData['quantity'] : 1;
            }

            // Get All inventory item id of given forced modifier id
            $inventory86CountForcedModifierResponse = $inventory86CountViewController->getForcedModifier86CountQuantity($forcedModifierIdsRequest, true);
            foreach($inventory86CountForcedModifierResponse as $fid => $inventoryForcedModifierObj) {
                foreach ($inventoryForcedModifierObj as $iid => $forcedModifierObj) {
                    $forcedQuantity = ($forcedModifierQuantityRequest[$fid] != "" && $forcedModifierQuantityRequest[$fid] > 0) ? $forcedModifierQuantityRequest[$fid] : 1;
                    $usedQuantity = ($forcedModifierObj['quantity'] != "" && $forcedModifierObj['quantity'] > 0) ? $forcedModifierObj['quantity'] : 1;
                    $inventoryAuditForcedModifierRequest[$fid][$iid]['quantity'] += $wasteQuantity * $usedQuantity * $forcedQuantity;
                    $inventoryAuditForcedModifierRequest[$fid][$iid]['unitID'] = $forcedModifierObj['unitID'];
                }
            }
        }
        return $inventoryAuditForcedModifierRequest;
    }

    public function createModifierWasteAudits($request, $wasteQuantity) {
        $inventory86CountViewController = new Inventory86CountViewCompiler();
        $inventoryAuditModifierRequest = array();
        if(isset($request[0]['modifierID']) && !empty($request[0]['modifierID'])) {
            $modifierIdsRequest = array();
            $modifierIdsQuantityRequest = array();
            foreach ($request[0]['modifierID'] as $key => $modifierData) {
                $modifierIdsRequest[$key]['id'] = $modifierData['id'];
                $modifierIdsQuantityRequest[$modifierData['id']] = (isset($modifierData['quantity']) && $modifierData['quantity'] != '') ? $modifierData['quantity'] : 1;
            }
            // Get All inventory item id of given modifier id
            $inventory86CountModifierResponse = $inventory86CountViewController->getModifier86CountQuantity($modifierIdsRequest, true);
            foreach($inventory86CountModifierResponse as $mid => $inventoryModifierObj) {
                foreach ($inventoryModifierObj as $iid => $modifierObj) {
                    $modifierQuantity = ($modifierIdsQuantityRequest[$mid] != "" && $modifierIdsQuantityRequest[$mid] > 0) ? $modifierIdsQuantityRequest[$mid] : 1;
                    $usedQuantity = ($modifierObj['quantity'] != "" && $modifierObj['quantity'] > 0) ? $modifierObj['quantity'] : 1;
                    $inventoryAuditModifierRequest[$mid][$iid]['quantity'] = $wasteQuantity * $usedQuantity * $modifierQuantity;
                    $inventoryAuditModifierRequest[$mid][$iid]['unitID'] = $modifierObj['unitID'];
                }
            }
        }
        return $inventoryAuditModifierRequest;
    }
}