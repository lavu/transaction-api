<?php
require_once __DIR__ . "/../RequestFormatters/BasicFormatter.php";

require_once __DIR__ . "/../InventoryObjectControllers/InventoryItemController.php";
require_once __DIR__ . "/../InventoryObjectControllers/InventoryCategoryController.php";
require_once __DIR__ . "/../InventoryObjectControllers/InventoryStorageLocationController.php";
require_once __DIR__ . "/../InventoryObjectControllers/InventoryUnitController.php";

require_once __DIR__ . "/../InventoryObjectControllers/InventoryAuditController.php";
require_once __DIR__ . "/../RequestFormatters/InventoryAuditRequestFormatter.php";

/**
 * Created by PhpStorm.
 * User: Leif
 * Date: 5/9/17
 * Time: 4:26 PM
 */

//Need to Get:
//By inventory Item: Item name, location, category, sales uom, (purchase uom as well to be safe) quantityOnHand
class InventoryReconciliationViewCompiler extends BasicFormatter
{
	public function getReconciliationRows($request = array()){
		$inventoryItemController =              new InventoryItemController();
		$inventoryCategoryController =          new InventoryCategoryController();
		$inventoryStorageLocationController =   new InventoryStorageLocationController();
		$inventoryUnitController =              new InventoryUnitController();
		$inventoryItems =   self::decodeLavuAPIResponse($inventoryItemController->getAllInventoryItems($request));
		$categories =       self::decodeLavuAPIResponse($inventoryCategoryController->getAllCategories());
		$storageLocations = self::decodeLavuAPIResponse($inventoryStorageLocationController->getAllStorageLocations());
		$units =            self::decodeLavuAPIResponse($inventoryUnitController->getAllUnits());

		$inventoryItems = self::joinFieldsOrRowsFromTable2ToTable1($inventoryItems, $categories,        'categoryID',           'id', array('name'),    array('category'));
		$inventoryItems = self::joinFieldsOrRowsFromTable2ToTable1($inventoryItems, $storageLocations,  'storageLocation',      'id', array('name'),    array('location'));
		$inventoryItems = self::joinFieldsOrRowsFromTable2ToTable1($inventoryItems, $units,             'recentPurchaseUnitID', 'id', array('name'),    array('purchaseUnitName'));
		$inventoryItems = self::joinFieldsOrRowsFromTable2ToTable1($inventoryItems, $units,             'salesUnitID',          'id', array('name'),    array('salesUnitName'));
		for($i = 0; $i <count($inventoryItems); $i++){
			$inventoryItems[$i]['UOM'] = $inventoryItems[$i]['salesUnitName'];
			$inventoryItems[$i]['stock'] = $inventoryItems[$i]['quantityOnHand'];
			$inventoryItems[$i]['count'] = "";
			$inventoryItems[$i]['varianceQty'] = 0;
			$inventoryItems[$i]['status'] ='equal';
		}

		//Updated By Dhruvpalsinh on 30-01-2019
		$requestinvcount = InventoryQueries::getAllInventoryItemsCount();
		$invcount = $requestinvcount[0]['inventoryitemcount'];
		return self::encodeLavuAPIResponse($inventoryItems,$invcount);
	}


	//TODO: implement rollback functionality as needed. Look at InventoryManageViewCompiler.
	public function performReconciliation($request){
		//pass in id's and quantities, unit id's, get current timestamp.
		//action will be something like "reconciliation"
		//TODO: get user id from the session.

		foreach($request as $i => $req){
			$request[$i]['id'] = $request[$i]['inventoryItemID'];
			$request[$i]['quantityOnHand'] = $request[$i]['quantity'];
		}
		$itemFormatter = new InventoryItemRequestFormatter();
		$itemRequest = 	$itemFormatter->formatReconciliation($request); //Item should have all fields to update

		$auditFormatter = new InventoryAuditRequestFormatter();
		$auditRequest = $auditFormatter->formatCreateAuditRequest($request, 'reconciliation');
		$auditController = new InventoryAuditController();
		$auditController->createReconciliationAudit($auditRequest);

		$itemController = new InventoryItemController();
		$result = self::decodeLavuAPIResponse($itemController->updateInventoryItems($itemRequest));

		return self::encodeLavuAPIResponse($result);
	}

	public function exportReconciliation()
	{
		$fileName = 'Reconciliation.csv';
		$reconciliation = self::decodeLavuAPIResponse(self::getReconciliationRows());
		$data = 'No Record found';
		if (!empty($reconciliation)) {
			$data = implode(',', str_replace(',', ' ', array_keys($reconciliation[0]))) . "\n";
			foreach ($reconciliation as $details) {
				$data .=  implode(',', str_replace(',', ' ', $details)) . "\n";
			}
		}
		$result = self::exportCsv($data, $fileName);
		return true;
	}
}