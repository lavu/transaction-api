<?php
require_once __DIR__ . "/../RequestFormatters/BasicFormatter.php";
require_once __DIR__ . "/../RequestFormatters/InventoryItemRequestFormatter.php";

require_once __DIR__ . "/../InventoryObjectControllers/InventoryItemController.php";

class InventoryReportCompiler extends BasicFormatter
{
	/**
	 * Inventory Report -- Food and Beverage
	 * @return array
	 */
	public function getFoodAndBeverageReport($requestArray)
	{
		$reportValues = array();

		$inventoryReportFormatter = new InventoryReportRequestFormatter();
		$inventoryReportsController = new InventoryReportsController();

		foreach ($requestArray as $request)
		{
			// LP-1039 -- Cost of Goods Sold (%) = Usage Amount / Sales Revenue

			$dateArgs  = array('startDate' => $requestArray[0]['startDate'],
								'endDate' => $requestArray[0]['endDate'],
								'startTime' => $requestArray[0]['startTime'],
								'endTime' => $requestArray[0]['endTime'],
								'mode' => $requestArray[0]['mode']);
			$salesRevenues = self::decodeLavuAPIResponse($inventoryReportsController->getSalesRevenueForTimePeriod($dateArgs));  // TO DO : fix this returning empty array upon no results
			$salesRevenue = isset($salesRevenues[0]) ? $salesRevenues[0]['revenue'] : 0.00;

			$usageAmount = 0.00;
			$usageItems = $this->getUsage($requestArray);

			foreach ($usageItems as $usageItem)
			{
				$usageAmount += (double) $usageItem['SUExtendedCost'];
			}

			$costOfGoodsSold = ($salesRevenue == 0) ? 0.00 : (double) $usageAmount / (double) $salesRevenue;  // Don't divide by zero

			$reportValues[] = array(
				'UsageAmount'     => number_format($usageAmount, 2, '.', ''),
				'SalesRevenue'    => number_format($salesRevenue, 2, '.', ''),
				'CostOfGoodsSold' => number_format($costOfGoodsSold, 2, '.', ''),
			);
		}

		return self::encodeLavuAPIResponse($reportValues);
	}

	/**
	 * Inventory Report -- Usage
	 * @return array
	 */
	public function getUsageReport($requestArray)
	{
		$reportValues = $this->getUsage($requestArray);
		return self::encodeLavuAPIResponse($reportValues);
	}

	/**
	 * Inventory Report -- Low Inventory
	 * @return array
	 */
	public function getLowInventoryReport()
	{
		$reportValues = array();

		$inventoryItemController = new InventoryItemController();
		$inventoryAuditController = new InventoryAuditController();

		$auditFormatter = new InventoryAuditRequestFormatter();

		$categories = $this->getInventoryItemCategories();
		$units = $this->getStandardUOMs(true);
		$auditEventCriteria = array('action' => '86count');
		$auditEventTypes = array('Quantity Decreased by Menu Item', 'Quantity Decreased by Modifier', 'Quantity Decreased by Forced Modifier', 'Offline Quantity Decreased by Menu Item', 'Offline Quantity Decreased by Modifier', 'Offline Quantity Decreased by Forced Modifier');

		// Get 86count audit events
		$auditRequest = $auditFormatter->formatGetAuditsServiceRequest(array('Items' => $auditEventCriteria));
		$auditEvents = self::decodeLavuAPIResponse($inventoryAuditController->getCriteriaAudits($auditRequest));
		$auditEvents = array_shift($auditEvents);

		// Order 86count audit events in descending order by datetime so we can step backwards through time
		usort($auditEvents, function($a, $b){
			return strtotime($b['datetime']) - strtotime($a['datetime']);
		});

		$lowStockInventoryItems = self::decodeLavuAPIResponse($inventoryItemController->getAllLowStockInventoryItems());

		foreach ($lowStockInventoryItems as $lowStockInventoryItem)
		{
			// LP-1041 -- Date, Item, Category, PU Threshold, Qty. on Hand, PAR

			// Category
			$lowStockInventoryItem['category'] = isset($categories[$lowStockInventoryItem['categoryID']]) ? $categories[$lowStockInventoryItem['categoryID']] : '';
			//Comment out below code because there is no date column in inventory_items table so daterange is not reflecting for report.(Ref. LP-8891)
			// Now we are showing all inventory items which is Lower then low quantity
/*
			// Calculate the date the quantity dipped into the low stock threshold by stepping the qtyOnHand backwards through time using 86 count audits
			$lowStockDate = '';
			$qtyOnHand = $lowStockInventoryItem['quantityOnHand'];
			for ($i = 0; $i < count($auditEvents); $i++) {
				$auditEvent = $auditEvents[$i];

				// Use all 86count audit events related to decreasing quantity that fall in our date range
				if ($lowStockInventoryItem['id'] == $auditEvent['inventoryItemID'] && in_array($auditEvent['userNotes'], $auditEventTypes))
				{
					$qtyOnHand += $auditEvent['quantity'];
				}

				if ($qtyOnHand >= $lowStockInventoryItem['lowQuantity'])
				{
					$lowStockDate = date('m/d/Y', strtotime($auditEvent['datetime']));
					break;
				}
			}
*/
			$salesConversion = isset($units[$lowStockInventoryItem['salesUnitID']]) ? $units[$lowStockInventoryItem['salesUnitID']] : 1;
			$purchaseConversion = isset($units[$lowStockInventoryItem['recentPurchaseUnitID']]) ? $units[$lowStockInventoryItem['recentPurchaseUnitID']] : 1;
			$conversion = $purchaseConversion / $salesConversion;
			$convertedQuantity = $lowStockInventoryItem['quantityOnHand'] * $conversion;
			$convertedReOrderQuantity = $lowStockInventoryItem['reOrderQuantity'] * $conversion;
			if (empty($lowStockDate)) $lowStockDate = date('m/d/Y');

			// Add row to report values
			$reportValues[] = array(
				'Item'         => $lowStockInventoryItem['name'],
				'Date'         => $lowStockDate,
				'Category'     => $lowStockInventoryItem['category'],
				'PU Threshold' => $convertedReOrderQuantity ,
				'Qty'          => $convertedQuantity ,
			);
		}

		return self::encodeLavuAPIResponse($reportValues);
	}

	/**
	 * Inventory Report -- Waste
	 * @return array
	 */
	public function getWasteReport($requestArray)
	{
		$reportValues = array();

		$inventoryAuditController = new InventoryAuditController();
		$inventoryItemController = new InventoryItemController();

		$inventoryAuditFormatter = new InventoryAuditRequestFormatter();
		$inventoryItemFormatter = new InventoryItemRequestFormatter();
		$inventoryReportFormatter = new InventoryReportRequestFormatter();

        $uoms = $this->getStandardUOMs();
		$users = $this->getUsers();
		$categories = $this->getInventoryItemCategories();
		foreach ($requestArray as $request)
		{
			$wasteAuditsArgs = array(array('actions' => array('waste', 'wasteOverage'),
											'startDate' => $requestArray[0]['startDate'],
											'endDate' => $requestArray[0]['endDate'],
											'startTime' => $requestArray[0]['startTime'],
											'endTime' => $requestArray[0]['endTime'],
											'mode' => $requestArray[0]['mode']));
			$wasteAuditsRequest = $inventoryAuditFormatter->formatGetAuditsByActionsAndDatesRequest($wasteAuditsArgs);
			$wasteAuditsResponse = self::decodeLavuAPIResponse($inventoryAuditController->getAuditsByActionsAndDates($wasteAuditsRequest));
			$wasteAudits = array_shift($wasteAuditsResponse);

			foreach ($wasteAudits as $wasteAudit)
			{
				// LP-1042 -- Date Range, Item, Category, SU Qty Wasted, Reason, Who

				// Category - need to get InventoryItem to get its categoryID
				$inventoryItem = self::decodeLavuAPIResponse($inventoryItemController->getInventoryItemsByID(array(array('id' => $wasteAudit['inventoryItemID']))));
				$inventoryItem['category'] = isset($categories[$inventoryItem['categoryID']]) ? $categories[$inventoryItem['categoryID']] : '';
				if (empty($inventoryItem['name'])) $inventoryItem['name'] = 'Inventory Item ID '. $wasteAudit['inventoryItemID'];

				$unit = isset($uoms[$wasteAudit['unitID']]) ? $uoms[$wasteAudit['unitID']] : '';
				// User
				$wasteAudit['user'] = isset($users[$wasteAudit['userID']]) ? $users[$wasteAudit['userID']] : 'User ID '. $wasteAudit['userID'];

				// Add row to report values
				$reportValues[] = array(
					'Item'       => $inventoryItem['name'],
					'Date'       => date('m/d/Y', strtotime($wasteAudit['datetime'])),
					'Category'   => $inventoryItem['category'],
					'Qty Wasted' => $wasteAudit['quantity'],
                    'unit' => $unit,
                    //'cost' => $wasteAudit['cost'],
					'Reason'     => $wasteAudit['userNotes'],
					'User'       => $wasteAudit['user']
				);
			}
		}
		return self::encodeLavuAPIResponse($reportValues);
	}

	/**
	 * Inventory Report -- Transfer
	 * @return array
	 */
	public function getTransferReport($requestArray)
	{
		$reportValues = array();

		$inventoryAuditController = new InventoryAuditController();
		$inventoryEnterpriseController = new InventoryEnterpriseController();
		$inventoryItemController = new InventoryItemController();
		$inventoryTransferController = new inventoryTransferController();
		$inventoryTransferItemController = new inventoryTransferItemController();

		$inventoryAuditFormatter = new InventoryAuditRequestFormatter();
		$inventoryItemFormatter = new InventoryItemRequestFormatter();
		$inventoryTransferFormatter = new inventoryTransferRequestFormatter();
		$inventoryReportFormatter = new InventoryReportRequestFormatter();

		$users = $this->getUsers();
		$categories = $this->getInventoryItemCategories();

		foreach ($requestArray as $request)
		{
			$transferAuditsArgs = array(array('actions' => array('receiveTransfer', 'transferSent'),
					                          'startDate' => $requestArray[0]['startDate'],
					                          'endDate' => $requestArray[0]['endDate'],
					                          'startTime' => $requestArray[0]['startTime'],
					                          'endTime' => $requestArray[0]['endTime'],
					                          'mode' => $requestArray[0]['mode']));
			$transferAuditsRequest = $inventoryAuditFormatter->formatGetAuditsByActionsAndDatesRequest($transferAuditsArgs);
			$transferAuditsResponse = self::decodeLavuAPIResponse($inventoryAuditController->getAuditsByActionsAndDates($transferAuditsRequest));
			$transferAudits = array_shift($transferAuditsResponse);

			foreach ($transferAudits as $transferAudit)
			{
				// Get the single TransferItem using the TransferItemID on audit
				$transferItemArgs = array('id' => $transferAudit['transferItemID']);
				$transferItemResponse = self::decodeLavuAPIResponse($inventoryTransferItemController->getTransferItemByID(array($transferItemArgs)));
				$transferItem = array_shift($transferItemResponse);
				$transferItem = array_shift($transferItem);  // TO DO : fix this

				// Get Transfer and all of its TransferItems
				$transferArgs = array('id' => $transferItem['transferID']);
				$transferRequest = $inventoryTransferFormatter->formatGetTransfersByID(array($transferArgs));
				$transferResponse = self::decodeLavuAPIResponse($inventoryTransferController->getTransfersByID($transferRequest));
				$transfer = array_shift($transferResponse);
				$transferItems = isset($transfer['transferItems']) ? $transfer['transferItems'] : array();

				// LP-1043 -- Transfer In/Out, Date Requested, Item, Category, Shipped to (Transferee Location), Qty Ordered, Qty Received, Date Received, Received By

				// Get restaurant IDs for ShippedFrom, ShippedTo and Transfer In/Out
				$originRestaurant = array_shift(self::decodeLavuAPIResponse($inventoryEnterpriseController->getRestaurantByID(array('id' => $transfer['originRestaurantID']))));
				$destRestaurant   = array_shift(self::decodeLavuAPIResponse($inventoryEnterpriseController->getRestaurantByID(array('id' => $transfer['destinationRestaurantID']))));
				$transfer['originRestaurant'] = $originRestaurant['company_name'];
				$transfer['destinationRestaurant'] = $destRestaurant['company_name'];
				$transfer['type'] = (admin_info('dataname') == $destRestaurant['data_name']) ? 'Transfer In' : 'Transfer Out';

				foreach ($transferItems as $transferItem)
				{
					// Category - need to get InventoryItem to get its CategoryID
					$inventoryItem = self::decodeLavuAPIResponse($inventoryItemController->getInventoryItemsByID(array(array('id' => $transferItem['inventoryItemID']))));
					$inventoryItem['category'] = isset($categories[$inventoryItem['categoryID']]) ? $categories[$inventoryItem['categoryID']] : '';
					if (empty($inventoryItem['name'])) $inventoryItem['name'] = 'Inventory Item ID '. $transferItem['inventoryItemID'];

					// User
					$transferItem['user'] = isset($users[$transferAudit['userID']]) ? $users[$transferAudit['userID']] : 'User ID '. $transferAudit['userID'];

					$transferItem['transferType'] = admin_info("dataname");

					// Add row to report values
					$reportValues[] = array(
						'Item'               => $inventoryItem['name'],
						'Category'           => $inventoryItem['category'],
						'TransferType'       => $transfer['type'],
						'TransferNumber'     => $transfer['transferNumber'],
						'TransferItemNumber' => $transferItem['transferItemNumber'],
						'DateRequested'      => date('m/d/Y', strtotime($transfer['orderDate'])),
						'ShippedFrom'        => $transfer['originRestaurant'],
						'ShippedTo'          => $transfer['destinationRestaurant'],
						'QtyOrdered'         => $transferItem['quantityTransferred'],
						'QtyReceived'        => $transferItem['quantityReceived'],
						'DateReceived'       => date('m/d/Y', strtotime($transferAudit['datetime'])),
						'ReceivedBy'         => $transferItem['user'],
					);
				}
			}
		}

		return self::encodeLavuAPIResponse($reportValues);
	}

	/**
	 * Inventory Report -- Variance
	 * @return array
	 */
	public function getVarianceReport($requestArray)
	{
		$reportValues = array();

		$inventoryAuditController = new InventoryAuditController();
		$inventoryItemController = new InventoryItemController();

		$inventoryAuditFormatter = new InventoryAuditRequestFormatter();
		$inventoryItemFormatter = new InventoryItemRequestFormatter();
		$inventoryReportFormatter = new InventoryReportRequestFormatter();
		$users = $this->getUsers();
		$categories = $this->getInventoryItemCategories();

		foreach ($requestArray as $request)
		{
			$startDate = $inventoryReportFormatter->formatReportStartDate($request);
			$endDate   = $inventoryReportFormatter->formatReportEndDate($request);

			$reconAuditsArgs = array(array('actions' => array('reconciliation'),
					                       'startDate' => $requestArray[0]['startDate'],
					                       'endDate' => $requestArray[0]['endDate'],
					                       'startTime' => $requestArray[0]['startTime'],
					                       'endTime' => $requestArray[0]['endTime'],
					                       'mode' => $requestArray[0]['mode']));
			$reconAuditsRequest = $inventoryAuditFormatter->formatGetAuditsByActionsAndDatesRequest($reconAuditsArgs);
			$reconAuditsResponse = self::decodeLavuAPIResponse($inventoryAuditController->getAuditsByActionsAndDates($reconAuditsRequest));
			$reconAudits = array_shift($reconAuditsResponse);
			foreach ($reconAudits as $reconAudit)
			{
				// Category - need to get InventoryItem to get its CategoryID
				$inventoryItem = self::decodeLavuAPIResponse($inventoryItemController->getInventoryItemsByID(array(array('id' => $reconAudit['inventoryItemID']))));
				$inventoryItem['category'] = isset($categories[$inventoryItem['categoryID']]) ? $categories[$inventoryItem['categoryID']] : '';
				if (empty($inventoryItem['name'])) $inventoryItem['name'] = 'Inventory Item ID '. $reconAudit['inventoryItemID'];

				// User
				$reconAudit['user'] = isset($users[$reconAudit['userID']]) ? $users[$reconAudit['userID']] : 'User ID '. $reconAudit['userID'];

				// Add row to report values
				$reportValues[] = array(
					'Item'               => $inventoryItem['name'],
					'Category'           => $inventoryItem['category'],
					'Date'               => date('m/d/Y', strtotime($reconAudit['datetime'])),
					'QtyOnHand'          => $reconAudit['quantity'],
					'Variance'           => $reconAudit['userNotes'],
					'ReconBy'            => $reconAudit['user'],
				);
			}
		}

		return self::encodeLavuAPIResponse($reportValues);
	}

	private function updateGetUsage($requestArray){

    }
	/**
	 * Usage calculator method -- Moved into separate method because it was needed by both Usage Reportand Food and Beverage Report
	 * @return array
	 */
	private function getUsage($requestArray)
	{
		$inventoryAuditController = new InventoryAuditController();
		$inventoryItemController = new InventoryItemController();
		$formatter = new InventoryReportRequestFormatter();
		$auditFormatter = new InventoryAuditRequestFormatter();
		$inventoryItemFormatter = new InventoryItemRequestFormatter();

		$dawnOfTime = DAWN_OF_TIME;
		$auditEventCriteria = array('action' => '86count');
		$auditEventTypes = array('Quantity Decreased by Menu Item', 'Quantity Decreased by Modifier', 'Quantity Decreased by Forced Modifier', 'Offline Quantity Decreased by Menu Item', 'Offline Quantity Decreased by Modifier', 'Offline Quantity Decreased by Forced Modifier');

		$categories = $this->getInventoryItemCategories();
		$uoms = $this->getStandardUOMs();

		$reportValues = array();
		foreach ($requestArray as $request)
		{
			$now = gmdate("Y-m-d H:i:s");
			$startDate = $formatter->formatReportStartDate($request);
			$endDate   = $formatter->formatReportEndDate($request);
			$dateArgs  = array('actions' => array('86count'),
					           'startDate' => $requestArray[0]['startDate'],
					           'endDate' => $requestArray[0]['endDate'],
					           'startTime' => $requestArray[0]['startTime'],
					           'endTime' => $requestArray[0]['endTime'],
					           'mode' => $requestArray[0]['mode']);
			$auditsForTimePeriod = self::decodeLavuAPIResponse($inventoryAuditController->getAuditsByActionsAndDates(array('Items'=>$dateArgs)));
			$itemIDs = array();
			$quantities = array();
			$inventoryItems = array();
			$costs = array();
            foreach($auditsForTimePeriod[0] as $audit){
                $currID = $audit['inventoryItemID'];
                if(!in_array($currID, $itemIDs)){
                    $itemIDs[] = $currID;
                }
                if(array_key_exists($currID,$quantities)){
                  if ($audit['userNotes'] == 'Quantity Increased by Menu Item' || $audit['userNotes'] == 'Quantity Voided by Menu Item' || $audit['userNotes'] == 'Quantity Voided by Forced Modifier' || $audit['userNotes'] == 'Quantity Increased by Forced Modifier' || $audit['userNotes'] == 'Quantity Voided by Modifier' || $audit['userNotes'] == 'Quantity Increased by Modifier') {
                  $quantities[$currID] -= $audit['quantity'];
                  } else {
                   $quantities[$currID] += $audit['quantity'];
                  }
                }
                else{
                    $quantities[$currID] = $audit['quantity'];
                }
				if(array_key_exists($currID, $costs)){
				  if ($audit['userNotes'] == 'Quantity Increased by Menu Item' || $audit['userNotes'] == 'Quantity Voided by Menu Item' || $audit['userNotes'] == 'Quantity Voided by Forced Modifier' || $audit['userNotes'] == 'Quantity Increased by Forced Modifier' || $audit['userNotes'] == 'Quantity Voided by Modifier' || $audit['userNotes'] == 'Quantity Increased by Modifier') {
                    $costs[$currID] -= $audit['cost'];
                  } else {
				  $costs[$currID] += $audit['cost'];
                  }
                }
                else{
                    $costs[$currID] = $audit['cost'];
                }
                if(!array_key_exists($currID, $inventoryItems)){
                    $inventoryItemRequest = $inventoryItemFormatter->formatGetInventoryItemsByID(array(array('id' => $currID)));
                    $currItem = self::decodeLavuAPIResponse($inventoryItemController->getInventoryItemsByID($inventoryItemRequest));
                    $inventoryItems[$currID] = $currItem;
                }
            }
            //error_log("INVENTORY ITEMS: ".print_r($inventoryItems, 1));
            //$costTimeLine = self::decodeLavuAPIResponse($inventoryItemController->getCostTimeline(array('itemIDs'=>$itemIDs, 'quantities'=>$quantities)));
			// LP-1040 -- Using Usage Amount formula from LP-1039 and processing the data very similarly to logic in getFoodAndBeverageReport()
			// except we're storing per item cost in array by item ID for comparison after all items have been collected

			$usageCostDataItems = array();

			foreach($inventoryItems as $item) {
                $inventoryItemID = $item['id'];
                $quantityUsed = $quantities[$inventoryItemID];
                $currQuantity = 0;
                $totalValue = 0.00;
                // Translate ID fields to values using look-up tables
                $item['category'] = isset($categories[$item['categoryID']]) ? $categories[$item['categoryID']] : '';
                $item['uom'] = isset($uoms[$item['salesUnitID']]) ? $uoms[$item['salesUnitID']] : '';
                $item['cost'] = $costs[$item['id']] / $quantities[$item['id']]; //cost per item in sales quantity (average)
                //If Inventory item is either voided or removed then do not display it in report
                if ($quantityUsed > 0) {
                // Make an entry for our inventory item, if one doesn't already exist
                if (!isset($usageCostDataItems[$inventoryItemID])) {
                    $usageCostDataItems[$inventoryItemID] = array(
                        'InventoryItemID' => $inventoryItemID,
                        'Item' => $item['name'],
                        'Category' => $item['category'],
                        'SalesUnitOfMeasure' => $item['uom'],
                        'QtyUsed' => 0,
                        'SUCost' => $item['cost'],
                        'SUExtendedCost' => $costs[$item['id']],
                    );
                }
                // Tally our quantity for this audit event and update our SU cost (by averaging since the last one) and then re-calculating the SU Extended Cost
                $usageCostDataItems[$inventoryItemID]['QtyUsed'] = $quantityUsed;
            }
               // $usageCostDataItems[$inventoryItemID]['SUExtendedCost'] = $usageCostDataItems[$inventoryItemID]['SUCost'] * (double)$usageCostDataItems[$inventoryItemID]['QtyUsed'];
            }

			$reportValues = array_values($usageCostDataItems);
		}

		return $reportValues;
	}

	/**
	 * Helper function
	 * @return boolean
	 */
	private function dateFallsInTimePeriod($dateToCheck, $startDate, $endDate)
	{
		$dateToCheckTs = strtotime($dateToCheck);
		$startDateTs   = empty($startDate) ? '' : strtotime($startDate);
		$endDateTs     = empty($endDate)   ? '' : strtotime($endDate);

		return ( (empty($startDate) || $startDateTs <= $dateToCheckTs) && (empty($endDate) || $endDateTs >= $dateToCheckTs) );
	}

	/**
	 * Helper function
	 * @return float $costOfPurchase
	 */
	private function calculatePurchaseCost($costData, $startDate, $endDate)
	{
		$costOfPurchases = 0.00;

		if ($costData['receivedAs'] == 'purchaseOrder' && $this->dateFallsInTimePeriod($costData['date'], $startDate, $endDate)) {
			$costOfPurchases = floatval($costData['unitCost']) * floatval($costData['quantity']);
		}

		return $costOfPurchases;
	}

	/**
	 * Helper function
	 * @return float $costOfPurchase
	 */
	private function calculateUnitCostForAuditEvent($qtySequence, $unitCostChangesForInventoryItem)
	{
		if (empty($unitCostChangesForInventoryItem)) {
			return 0.00;
		}

		// Initialize counter for how many quantity counts remain for which we have to find the unitCost
		$qtyRemaining = $qtySequence;

		// Make sure the array is sorted by the integer keys
		ksort($unitCostChangesForInventoryItem);

		$unitCostTotal = 0.00;
		$unitCostAddends = 0;

		list($qtyRangeStart, $unitCost)   = each($unitCostChangesForInventoryItem);
		list($qtyRangeEnd, $nextUnitCost) = each($unitCostChangesForInventoryItem);
		$i = 0;
		while ($qtyRemaining > 0 && $qtyRangeStart !== false && $qtyRangeEnd !== false && ++$i < 20) {
			if ($qtyRangeEnd === false) break;

			// Find the quantity range(s) our quantity run falls into so we can get the right unit cost(s)
			if ($qtyRangeStart <= $qtySequence && $qtySequence < $qtyRangeEnd) {
				$unitCostTotal += $unitCost;
				$unitCostAddends++;
				$qtySequence++;
				$qtyRemaining--;
			}
			// If our quantity sequence number has exceeded the bounds of the current range we're working on, advance to the next range
			else if ($qtySequence > $qtyRangeEnd) {
				$qtyRangeStart = $qtyRangeEnd;
				$unitCost = $nextUnitCost;
				list($qtyRangeEnd, $nextUnitCost) = each($unitCostChangesForInventoryItem);
			}
		}

		// Support finding average of unitCost if our qtySequence spans across two or more unitCost changes - but defend against dividing by zero
		$unitCost = ($unitCostAddends == 0) ? 0.00 : $unitCostTotal / $unitCostAddends;

		return $unitCost;
	}

	/**
	 * Helper function that calculates the quantity sequence number of our audit event based on how many reductive audit events have occurred for our InventoryItem ID before ours.
	 * @return int $quantitySequence
	 */
	private function calculateQuantitySequence($auditEvent, $allAuditEvents)
	{
		$quantitySequence = 1;

		// TO DO : add support for recon audit events (need to ask how to handle recon events re-writing history by changing quantity counts)

		// Make sure the allAuditEvents array is ordered by datetime acending
		usort($allAuditEvents, function($a, $b){
			return strtotime($a['datetime']) - strtotime($b['datetime']);
		});

		// Calculate what quantity reduction number we are by counting the number of quantity reduction events before ours
		$allAuditEventsCount = count($allAuditEvents);
		for ($i = 0; $i < $allAuditEventsCount; $i++) {
			$currAuditEvent = $allAuditEvents[$i];

			// Stop going through audit events once we pass any audit event for any InventoryItem ID that is older than our event
			if ( ($currAuditEvent['id'] == $auditEvent['id']) || (strtotime($currAuditEvent['datetime']) > strtotime($auditEvent['datetime'])) ) {
				break;
			}

			// Only advance the quantity sequence for audit events for our InventoryItem ID
			if ($auditEvent['inventoryItemID'] == $currAuditEvent['inventoryItemID']) {
				$quantitySequence += (int) $currAuditEvent['quantity'];
			}
		}

		return $quantitySequence;
	}

	/**
	 * Helper function that gets all non-deleted InventoryItem categories from API and puts them in an associative array where ID is key and name is value
	 * @return array $categories
	 */
	private function getInventoryItemCategories()
	{
		$inventoryCategoryController = new InventoryCategoryController();

		$categoryRows = self::decodeLavuAPIResponse($inventoryCategoryController->getAllCategories());

		$categories = array();
		foreach ($categoryRows as $categoryRow) {
			$categories[$categoryRow['id']] = $categoryRow['name'];
		}

		return $categories;
	}

	/**
	 * Helper function that gets all non-deleted InventoryItem categories from API and puts them in an associative array where ID is key and name is value
	 * @return array $categories
	 */
	private function getStandardUOMs($conversions = null)
	{
		$inventoryUnitController = new InventoryUnitController();

		$uomRows = self::decodeLavuAPIResponse($inventoryUnitController->getAllStandardUnitsOfMeasure());

		$uoms = array();

		foreach ($uomRows as $uomRow) {
            if ($conversions) {
                $uoms[$uomRow['id']] = $uomRow['conversion'];
            } else {
                $uoms[$uomRow['id']] = $uomRow['symbol'];
            }
        }
		return $uoms;
	}
    //Removed unused method with errors and warnings.
	/**
	 * Helper function that gets all non-deleted InventoryItem categories from API and puts them in an associative array where ID is key and name is value
	 * @return array $categories
	 */
	private function getUsers()
	{
		$inventoryReportsController = new InventoryReportsController();

		$userRows = self::decodeLavuAPIResponse($inventoryReportsController->getUsers());

		$users = array();
		foreach ($userRows as $userRow) {
			$fullName = '';
			if (isset($userRow['f_name'])) $fullName .= $userRow['f_name'];
			if (isset($userRow['l_name'])) $fullName .= $userRow['l_name'];
			if (empty($fullName)) $fullName = $userRow['username'];
			$users[$userRow['id']] = $fullName;
		}

		return $users;
	}


}