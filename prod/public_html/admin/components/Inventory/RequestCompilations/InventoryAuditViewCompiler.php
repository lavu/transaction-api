<?php
//require_once __DIR__ . "/../InventoryObjectControllers/InventoryItemController.php";
//require_once __DIR__ . "/../InventoryObjectControllers/InventoryAuditController.php";

/**
 * Created by PhpStorm.
 * User: Hayat
 * Date: 10/07/17
 * Time: 12:40 PM
 */
class InventoryAuditViewCompiler extends BasicFormatter
{

    /**
     * Get all inventory item from menuitem_86, modifier_86, forced_modifier_86 table. Multiply waste quantity with
     * quantityUsed and then pass all parameter to get isnsert into inventory_audit.
     * @param $request [{"id":740, "quantity":2, "userID": 43456, "wasteReason":"Damage"}]
     * @return json string
     */
	public function createItemWasteAudits($request){
        $inventory86CountController = new Inventory86CountController();
        $munuItemID[0]['id'] = $request[0]['id'];
        $inventory86CountResponse = self::decodeLavuAPIResponse($inventory86CountController->get86CountMenuItem($munuItemID));
        foreach($inventory86CountResponse as $key => $menuItemsObj) {

            $inventoryAuditRequest[$key]['inventoryItemID'] = $menuItemsObj['inventoryItemID'];
            $inventoryAuditRequest[$key]['action'] = 'waste';
            $inventoryAuditRequest[$key]['quantity'] = $request[0]['quantity'] * $menuItemsObj['quantityUsed'];
            $inventoryAuditRequest[$key]['unitID'] = $menuItemsObj['unitID'];
            $inventoryAuditRequest[$key]['userID'] = $request[0]['userID'];
            $inventoryAuditRequest[$key]['userNotes'] = $request[0]['wasteReason'];
        }

        $inventoryAuditController = new InventoryAuditController();
        $inventoryAuditResponse = $inventoryAuditController->createWasteAudit($inventoryAuditRequest);
        return $inventoryAuditResponse;
	}

}