<?php
require_once __DIR__. "/../InventoryObjectControllers/InventoryItemController.php";
/**
 * Created by PhpStorm.
 * User: Leif
 * Date: 6/23/17
 * Time: 2:51 PM
 */
class CostCalculator extends BasicFormatter
{
	private $costingMethod;
	public function __construct()
	{
		$this->costingMethod = (new InventoryItemController())->getCostingMethod();
	}

	public function calculateCost($costData, $quantityOnHand, $id, $beginDate=null, $endDate=null){
		$method = $this->decodeLavuAPIResponse($this->costingMethod);
		switch($method['value']){
			case FIFO:
				return $this->calculateFIFOCostForSingleItem($costData, $quantityOnHand, $id);
			case AVERAGE_COST_PERIODIC:
				return $this->calculateAveragePeriodicCostDataForSingleItem($costData, $quantityOnHand, $id, $beginDate, $endDate);
			default:
				return $this->calculateFIFOCostForSingleItem($costData, $quantityOnHand, $id);;
		}
	}

	public function calculateTotalCost($inventoryItems, $costData, $beginDate=null, $endDate=null){
		$method = $this->decodeLavuAPIResponse($this->costingMethod);
		switch($method['value']){
			case FIFO:
				return $this->calculateFIFOCostForAllInventoryItemsOnHand($inventoryItems, $costData);
			case AVERAGE_COST_PERIODIC:
				return $this->calculateAveragePeriodicCostDataForAllInventoryItems($inventoryItems, $costData, $beginDate, $endDate);
			default:
				return $this->calculateFIFOCostForAllInventoryItemsOnHand($inventoryItems, $costData);
		}
	}

	public function calculateFIFOCostForSingleItem($costData, $quantityOnHand, $id){
		$cost = 0;
		for($i = 0; $i < count($costData); $i++){
			//it is important that this is not a foreach loop, because order matters, and you can't count on foreach.
			if($costData[$i]['inventoryItemID'] == $id)
			{
				$quantity = $costData[$i]['quantity'];
				$unitCost = $costData[$i]['unitCost'];

				if( ($quantityOnHand - $quantity) <= 0){
					//use quantityOnHand if there is any less, and return the cost;

					if($quantityOnHand > 0){
						$cost += ( $quantityOnHand * $unitCost );
					}
					return $cost;
				}else if($quantity > 0){
					$cost += ( $quantity * $unitCost );
				}
				$quantityOnHand -= $quantity;
			}
		}
		//This function should never reach this point.
		return $cost;
	}

	public function calculateFIFOCostForAllInventoryItemsOnHand($inventoryItems, $costData){
		$usedItems = array();
		$totalCost = 0;
		foreach($costData as $cost){
		    $totalCost += $cost;
        }

		return $totalCost;
	}

	/**
	 * In order for this function to work correctly with beginning/ending dates, they must be convertible into dates
	 * using strtotime.
	 * If no dates are supplied to this function, it will use quantityOnHand to determine the latest received order
	 * and discontinue calculations past that order.
	 */
	private function calculateAveragePeriodicCostDataForSingleItem($costData,$quantityOnHand, $id, $beginDate=null, $endDate=null){
			if(!is_null($beginDate) && !is_null($endDate)){
				$beginDate = strtotime($beginDate);
				$endDate = strtotime($endDate);
			}
			$quantitySum = 0;
			$unitCostSum = 0;
			for ($i = 0; $i < count($costData); $i++)
			{
				if ($costData[$i]['inventoryItemID'] == $id)
				{
					$date = strtotime($costData[$i]['date']);
					$quantity = $costData[$i]['quantity'];

					//If endDate is left out it will calculate for all after the begin date. if beginDate is left out
					//it will calculate all before the endDate. If both are left out it will calculate based off of quantity on hand.
					if(
						(!is_null($beginDate) && $beginDate <= $date) && (!is_null($endDate) && $date <= $endDate )
						|| (is_null($beginDate) && is_null($endDate)) )
					{
						$unitCostSum += $costData[$i]['unitCost'];
						$quantitySum += $quantity;
					}
					if ((is_null($beginDate) && is_null($endDate)) && ($quantity - $quantityOnHand >= 0)) //Basing off of FIFO rather than dates. Calculating full orders; not ending with exact quantity on hand.
					{
						break;
					}else if( (!is_null($beginDate)) && $date <= $beginDate){ //can break out if we've passed the date range (since it's fifo, begin date is reached after endDate)
						break;
					}
				}
			}
			if($quantitySum == 0 || $unitCostSum == 0){
				return 0.00;
			}
			//This function should never reach this point.
			return 1.00 * ($unitCostSum / $quantitySum);

	}

	/**
	 * In order for this function to work correctly with beginning/ending dates, they must be convertible into dates
	 * using strtotime.
	 */
	private function calculateAveragePeriodicCostDataForAllInventoryItems($inventoryItems, $costData, $beginDate=null, $endDate=null){
		if(!is_null($beginDate) && !is_null($endDate)){
			$beginDate = strtotime($beginDate);
			$endDate = strtotime($endDate);
		}
		if(is_null($endDate)){ //This function should be used in the dashboard and should always be periodic, so there should always be an end date.
			$endDate = time();
		}
		$quantitySum = 0;
		$unitCostSum = 0;
		$usedItems = array();
		for($i = 0; $i < count($inventoryItems); $i++)
		{
			$usingItem = $inventoryItems[$i]['id'];
			if (!in_array($usingItem, $usedItems))
			{ //We only want to work on items we haven't already calculated cost for.
				$usedItems[] = $usingItem;
				for ($i = 0; $i < count($costData); $i++)
				{
					if ($costData[$i]['inventoryItemID'] == $usingItem)
					{
						$date = strtotime($costData[$i]['date']);
						$quantity = $costData[$i]['quantity'];

						//If endDate is left out it will calculate for all after the begin date. if beginDate is left out
						//it will calculate all before the endDate. If both are left out it will calculate based off of quantity on hand.
						if ((!is_null($beginDate) && $beginDate <= $date) && (!is_null($endDate) && $date <= $endDate))
						{
							$unitCostSum += $costData[$i]['unitCost'];
							$quantitySum += $quantity;
						}
						if ( (!is_null($beginDate)) && $date <= $beginDate)
						{ //can break out if we've passed the date range (since it's fifo, begin date is reached after endDate)
							break;
						}
					}
				}
			}
		}
		if($quantitySum == 0 || $unitCostSum == 0){
			return 0.00;
		}
		return 1.00 * ($unitCostSum / $quantitySum);
	}
}