<?php
require_once __DIR__ . "/../RequestFormatters/BasicFormatter.php";
require_once __DIR__ . "/../InventoryObjectControllers/InventoryItemController.php";
require_once __DIR__ . "/../InventoryObjectControllers/InventoryAuditController.php";
require_once __DIR__ . "/../RequestFormatters/InventoryAuditRequestFormatter.php";

/**
 * Created by PhpStorm.
 * User: hayat
 * Date: 7/20/17
 * Time: 3:55 PM
 */
class Inventory86CountViewCompiler extends BasicFormatter
{
	/**
	 * This function is used to update reserve quantity into 86Count table
	 * When user add item we will add quantity and deduct when remove from order
	 * @param $request
	 * @return array
	 */
	public function updateReserveQuantity($request)
	{
		$count86Controller = new Inventory86CountController();
		$formatter = new Inventory86CountRequestFormatter();

		$addedItemArr = $request[0]['added'];
		$removedItemArr = $request[0]['removed'];
		$menuItemStockresponse['item_id'] = array();
		$forcedModifierStockresponse['forced_modifier_id'] = array();
		$modifierStockresponse['optional_modifier_id'] = array();
		$removedMenuItemStockResponse['item_id'] = array();
		$removedForcedModifierStockResponse['forced_modifier_id'] = array();
		$removedModifierStockResponse['optional_modifier_id'] = array();
		$inStock = 'IN_STOCK';
		$lowStock = 'LOW_STOCK';
		$outStock = 'OUT_STOCK';

		// First processing removed functionality because if item is being removed from App then it should also removed from
		// reservation so as to avaible for other item.
		// Processing removed menu item
		if (!empty($removedItemArr['item_id']) || !empty($removedItemArr['forced_modifier_id']) || !empty($removedItemArr['optional_modifier_id'])) {
			if (isset($removedItemArr['item_id']) && !empty($removedItemArr['item_id'])) {
				$removedMenuItemIDs = self::getMenuItem86CountQuantity($removedItemArr['item_id'], true);
				$removedMenuItemIDsRequest = array();
				foreach ($removedMenuItemIDs as $key => $removedMenuItemIDsArray) {
					$removedMenuItemIDsRequest = array_merge($removedMenuItemIDsRequest, array_values($removedMenuItemIDsArray));
				}
				$menuItemformattedRequest = $formatter->formatDeduct86CountQuantity($removedMenuItemIDsRequest);
				if (isset($removedItemArr['action']) && $removedItemArr['action'] == '') {
					$count86Controller->removedMenuItemReserveQuantity($menuItemformattedRequest);
				}
				$menuItemsStatusRequest[0]['item_id'] = $removedItemArr['item_id'];
				$removedMenuItemStockResponse = self::decodeLavuAPIResponse($count86Controller->getMenuStatus($menuItemsStatusRequest));
			}
			if (isset($removedItemArr['forced_modifier_id']) && !empty($removedItemArr['forced_modifier_id'])) {
				$removedForcedModifierIDs = self::getForcedModifier86CountQuantity($removedItemArr['forced_modifier_id'], true);
				$removedForcedModifierIDsRequest = array();
				foreach ($removedForcedModifierIDs as $key => $removedForcedModifierIDsArray) {
					$removedForcedModifierIDsRequest = array_merge($removedForcedModifierIDsRequest, array_values($removedForcedModifierIDsArray));

				}
				$forcedModifierformattedRequest = $formatter->formatDeduct86CountQuantity($removedForcedModifierIDsRequest);
				if (isset($removedItemArr['action']) && $removedItemArr['action'] == '') {
					$count86Controller->removedForcedModifierReserveQuantity($forcedModifierformattedRequest);
				}
				$forcedModifiersStatusRequest[0]['forced_modifier_id'] = $removedItemArr['forced_modifier_id'];
				$forcedModifiersStatusRequest[0]['item_id'] = $removedItemArr['item_id'];
				$removedForcedModifierStockResponse = self::decodeLavuAPIResponse($count86Controller->getMenuStatus($forcedModifiersStatusRequest));
			}

			if (isset($removedItemArr['optional_modifier_id']) && !empty($removedItemArr['optional_modifier_id'])) {
				$removedModifierIDs = self::getModifier86CountQuantity($removedItemArr['optional_modifier_id'], true);
				$removedModifierIDsRequest = array();
				foreach ($removedModifierIDs as $key => $removedModifierIDsArray) {
					$removedModifierIDsRequest = array_merge($removedModifierIDsRequest, array_values($removedModifierIDsArray));

				}
				$modifierformattedRequest = $formatter->formatDeduct86CountQuantity($removedModifierIDsRequest);
				if (isset($removedItemArr['action']) && $removedItemArr['action'] == '') {
					$count86Controller->removedModifierReserveQuantity($modifierformattedRequest);
				}
				$modifiersStatusRequest[0]['optional_modifier_id'] = $removedItemArr['optional_modifier_id'];
				$modifiersStatusRequest[0]['item_id'] = $removedItemArr['item_id'];
				$removedModifierStockResponse = self::decodeLavuAPIResponse($count86Controller->getMenuStatus($modifiersStatusRequest));
			}

			$response = array_merge(array("action" => "removed"), $removedMenuItemStockResponse, $removedForcedModifierStockResponse, $removedModifierStockResponse);
		}

		// Processing add menu item
		if (!empty($addedItemArr['item_id']) || !empty($addedItemArr['forced_modifier_id']) || !empty($addedItemArr['optional_modifier_id'])) {
			if (isset($addedItemArr['item_id']) && !empty($addedItemArr['item_id'])) {
				$menuItemsStatusRequest[0]['item_id'] = $addedItemArr['item_id'];
				$itemIdsDetailsMap = array();
				foreach($addedItemArr['item_id'] as $key => $itemDetails) {
					$itemIdsDetailsMap[$itemDetails['id']] = $itemDetails;
				}
				$menuItemStockresponse = array();
				$menuItemStockresponse = self::decodeLavuAPIResponse($count86Controller->getMenuStatus($menuItemsStatusRequest));
				if (!empty($menuItemStockresponse['item_id'])) {
					$menuItemStocked = array();
					foreach ($menuItemStockresponse['item_id'] as $key => $itemIds) {
						if ($itemIds['status'] != $outStock) {
							$menuItemStocked[$key] = $itemIdsDetailsMap[$itemIds['id']];
						}
					}
					$addedMenuItemIDs = self::getMenuItem86CountQuantity($menuItemStocked, true);
					$addedMenuItemIDsRequest = array();
					foreach ($addedMenuItemIDs as $key => $addedMenuItemIDsArray) {
						$addedMenuItemIDsRequest = array_merge($addedMenuItemIDsRequest, array_values($addedMenuItemIDsArray));

					}
					$menuItemformattedRequest = $formatter->formatDeduct86CountQuantity($addedMenuItemIDsRequest);
					$count86Controller->addMenuItemReserveQuantity($menuItemformattedRequest);
				}
			}

			if (isset($addedItemArr['forced_modifier_id']) && !empty($addedItemArr['forced_modifier_id'])) {
				$forcedModifiersStatusRequest[0]['forced_modifier_id'] = $addedItemArr['forced_modifier_id'];
				$forcedModifiersStatusRequest[0]['item_id'] = $addedItemArr['item_id'];
				$forcedModifiersIdsDetailsMap = array();
				foreach($addedItemArr['forced_modifier_id'] as $key => $fModifiersDetails) {
					$forcedModifiersIdsDetailsMap[$fModifiersDetails['id']] = $fModifiersDetails;
				}
				$forcedModifierStockresponse = array();
				$forcedModifierStockresponse = self::decodeLavuAPIResponse($count86Controller->getMenuStatus($forcedModifiersStatusRequest));
				if (!empty($forcedModifierStockresponse['forced_modifier_id'])) {
					$forcedModifierStocked = array();
					foreach ($forcedModifierStockresponse['forced_modifier_id'] as $key => $fmodifiersIds) {
						if ($fmodifiersIds['status'] != $outStock) {
							$forcedModifierStocked[$key] = $forcedModifiersIdsDetailsMap[$fmodifiersIds['id']];
						}
					}
					$addedForcedModifierIDs = self::getForcedModifier86CountQuantity($forcedModifierStocked, true);
					$addedForcedModifierIDsRequest = array();
					foreach ($addedForcedModifierIDs as $key => $addedForcedModifierIDsArray) {
						$addedForcedModifierIDsRequest = array_merge($addedForcedModifierIDsRequest, array_values($addedForcedModifierIDsArray));

					}
					$forcedModifierformattedRequest = $formatter->formatDeduct86CountQuantity($addedForcedModifierIDsRequest);
					$count86Controller->addforcedModifierReserveQuantity($forcedModifierformattedRequest);
				}
			}

			if (isset($addedItemArr['optional_modifier_id']) && !empty($addedItemArr['optional_modifier_id'])) {
				$modifiersStatusRequest[0]['optional_modifier_id'] = $addedItemArr['optional_modifier_id'];
				$modifiersStatusRequest[0]['item_id'] = $addedItemArr['item_id'];
				$modifiersIdsDetailsMap = array();
				foreach($addedItemArr['optional_modifier_id'] as $key => $modifiersDetails) {
					$modifiersIdsDetailsMap[$modifiersDetails['id']] = $modifiersDetails;
				}
				$modifierStockresponse = array();
				$modifierStockresponse = self::decodeLavuAPIResponse($count86Controller->getMenuStatus($modifiersStatusRequest));
				if (!empty($modifierStockresponse['optional_modifier_id'])) {
					$modifierStocked = array();
					foreach ($modifierStockresponse['optional_modifier_id'] as $key => $modifiersIds) {
						if ($modifiersIds['status'] != $outStock) {
							$modifierStocked[$key] = $modifiersIdsDetailsMap[$modifiersIds['id']];
						}
					}
					$addedModifierIDs = self::getModifier86CountQuantity($modifierStocked, true);
					$addedModifierIDsRequest = array();
					foreach ($addedModifierIDs as $key => $addedModifierIDsArray) {
						$addedModifierIDsRequest = array_merge($addedModifierIDsRequest, array_values($addedModifierIDsArray));

					}
					$modifierformattedRequest = $formatter->formatDeduct86CountQuantity($addedModifierIDsRequest);
					$count86Controller->addModifierReserveQuantity($modifierformattedRequest);
				}
			}
			$response = array_merge(array("action" => "added"), $menuItemStockresponse, $forcedModifierStockresponse, $modifierStockresponse);
		}
		return self::encodeLavuAPIResponse($response);
	}


	/**
	 * This function is used to deduct quantity from reserve and quantityOnHand based on quantityUsed from 86Count and
	 * ineventory_item table
	 * @param $request
	 * @return array
	 */
	public function updateDeliverQuantity($request)
	{
		$count86Controller = new Inventory86CountController();
		$auditController = new InventoryAuditController();

		$formatter = new Inventory86CountRequestFormatter();
		$auditFormatter = new InventoryAuditRequestFormatter();

		$addedItemArr = $request[0]['added'];
		$removedItemArr = $request[0]['removed'];
		$totalUpdate = 0;
		$sentMenuItemIDsResponse['RowsUpdated'] = 0;
		$sentForcedModifierIDsResponse['RowsUpdated'] = 0;
		$sentModifierIDsResponse['RowsUpdated'] = 0;
		$removedMenuItemIDsResponse['RowsUpdated'] = 0;
		$removedForcedModifierIDsResponse['RowsUpdated'] = 0;
		$removedModifierIDsResponse['RowsUpdated'] = 0;

		// Processing sent menu item if confirm deliver to kitchen
		if (!empty($addedItemArr['item_id']) || !empty($addedItemArr['forced_modifier_id']) || !empty($addedItemArr['optional_modifier_id'])) {
			$sentMenuItemIDs = array();
			$sentForcedModifierIDs = array();
			$sentModifierIDs = array();
			if (isset($addedItemArr['item_id']) && !empty($addedItemArr['item_id'])) {
				$sentMenuItemIDs = self::getMenuItem86CountQuantity($addedItemArr['item_id'], true);
				$sentMenuItemIDsRequest = array();
				foreach ($sentMenuItemIDs as $key => $sentMenuItemIDsArray) {
					$sentMenuItemIDsRequest = array_merge($sentMenuItemIDsRequest, array_values($sentMenuItemIDsArray));

				}
				$menuItemformattedRequest = $formatter->formatDeduct86CountQuantity($sentMenuItemIDsRequest);
				$sentMenuItemIDsResponse = $count86Controller->deductMenuItemQuantity($menuItemformattedRequest);

				// Create audit event
				$auditRequest = $auditFormatter->formatCreate86CountAuditRequest($menuItemformattedRequest, '86count', 'Quantity Decreased by Menu Item');
				$auditController->createAudits($auditRequest);
			}
			if (isset($addedItemArr['forced_modifier_id']) && !empty($addedItemArr['forced_modifier_id'])) {
				$sentForcedModifierIDs = self::getForcedModifier86CountQuantity($addedItemArr['forced_modifier_id'], true);
				$sentForcedModifierIDsRequest = array();
				foreach ($sentForcedModifierIDs as $key => $sentForcedModifierIDsArray) {
					$sentForcedModifierIDsRequest = array_merge($sentForcedModifierIDsRequest, array_values($sentForcedModifierIDsArray));

				}
				$forcedModifierformattedRequest = $formatter->formatDeduct86CountQuantity($sentForcedModifierIDsRequest);
				$sentForcedModifierIDsResponse = $count86Controller->deductForcedModifierItemQuantity($forcedModifierformattedRequest);

				// Create audit event
				$auditRequest = $auditFormatter->formatCreate86CountAuditRequest($forcedModifierformattedRequest, '86count', 'Quantity Decreased by Forced Modifier');
				$auditController->createAudits($auditRequest);
			}

			if (isset($addedItemArr['optional_modifier_id']) && !empty($addedItemArr['optional_modifier_id'])) {
				$sentModifierIDs = self::getModifier86CountQuantity($addedItemArr['optional_modifier_id'], true);
				$sentModifierIDsRequest = array();
				foreach ($sentModifierIDs as $key => $sentModifierIDsArray) {
					$sentModifierIDsRequest = array_merge($sentModifierIDsRequest, array_values($sentModifierIDsArray));

				}
				$modifierformattedRequest = $formatter->formatDeduct86CountQuantity($sentModifierIDsRequest);
				$sentModifierIDsResponse = $count86Controller->deductModifierItemQuantity($modifierformattedRequest);

				// Create audit event
				$auditRequest = $auditFormatter->formatCreate86CountAuditRequest($modifierformattedRequest, '86count', 'Quantity Decreased by Modifier');
				$auditController->createAudits($auditRequest);
			}

		}

		// Processing sent menu item if get request to cancele deliver order
		if (!empty($removedItemArr['item_id']) || !empty($removedItemArr['forced_modifier_id']) || !empty($removedItemArr['optional_modifier_id']) || !empty($removedItemArr['isVoided'])) {
			$sentMenuItemIDs = array();
			$sentForcedModifierIDs = array();
			$sentModifierIDs = array();
			if (isset($removedItemArr['item_id']) && !empty($removedItemArr['item_id'])) {
				$sentMenuItemIDs = self::getMenuItem86CountQuantity($removedItemArr['item_id'], true);
				$sentMenuItemIDsRequest = array();
				foreach ($sentMenuItemIDs as $key => $sentMenuItemIDsArray) {
					$sentMenuItemIDsRequest = array_merge($sentMenuItemIDsRequest, array_values($sentMenuItemIDsArray));

				}
				$menuItemformattedRequest = $formatter->formatDeduct86CountQuantity($sentMenuItemIDsRequest);
				if (isset($removedItemArr['isVoided']) && !empty($removedItemArr['isVoided'])) {
					//To add Logs of Voided Inventory Item for Menu Item
					$auditRequestRemove = $auditFormatter->formatCreate86CountAuditRequest($menuItemformattedRequest, '86count', 'Quantity Voided by Menu Item');
					$auditController->createAudits($auditRequestRemove);
				} else {
					$removedMenuItemIDsResponse = $count86Controller->addQuantityOnHand($menuItemformattedRequest);
					//To Add Logs of Removed Inventory Item for Menu Item
					$auditRequestRemove = $auditFormatter->formatCreate86CountAuditRequest($menuItemformattedRequest, '86count', 'Quantity Increased by Menu Item');
					$auditController->createAudits($auditRequestRemove);
				}
			}
			if (isset($removedItemArr['forced_modifier_id']) && !empty($removedItemArr['forced_modifier_id'])) {
				$sentForcedModifierIDs = self::getForcedModifier86CountQuantity($removedItemArr['forced_modifier_id'], true);
				$sentForcedModifierIDsRequest = array();
				foreach ($sentForcedModifierIDs as $key => $sentForcedModifierIDsArray) {
					$sentForcedModifierIDsRequest = array_merge($sentForcedModifierIDsRequest, array_values($sentForcedModifierIDsArray));

				}
				$forcedModifierformattedRequest = $formatter->formatDeduct86CountQuantity($sentForcedModifierIDsRequest);
				if (isset($removedItemArr['isVoided']) && !empty($removedItemArr['isVoided'])) {
					//To Add Logs of Voided Inventory Item for Force Modifier
					$auditRequestVoidedForceMod = $auditFormatter->formatCreate86CountAuditRequest($forcedModifierformattedRequest, '86count', 'Quantity Voided by Forced Modifier');
					$auditController->createAudits($auditRequestVoidedForceMod);
				} else {
					$removedForcedModifierIDsResponse = $count86Controller->addQuantityOnHand($forcedModifierformattedRequest);
					//To Add Logs of Removed Inventory Item Qty For Force Modifier
					$auditRequesRemovedForceMod = $auditFormatter->formatCreate86CountAuditRequest($forcedModifierformattedRequest, '86count', 'Quantity Increased by Forced Modifier');
					$auditController->createAudits($auditRequesRemovedForceMod);
				}
			}

			if (isset($removedItemArr['optional_modifier_id']) && !empty($removedItemArr['optional_modifier_id'])) {
				$sentModifierIDs = self::getModifier86CountQuantity($removedItemArr['optional_modifier_id'], true);
				$sentModifierIDsRequest = array();
				foreach ($sentModifierIDs as $key => $sentModifierIDsArray) {
					$sentModifierIDsRequest = array_merge($sentModifierIDsRequest, array_values($sentModifierIDsArray));

				}
				$modifierformattedRequest = $formatter->formatDeduct86CountQuantity($sentModifierIDsRequest);
				if (isset($removedItemArr['isVoided']) && !empty($removedItemArr['isVoided'])) {
					//To Add Logs of Voided Inevntory Item for Optional Mofifiers
					$auditRequestVoidedOptMod = $auditFormatter->formatCreate86CountAuditRequest($modifierformattedRequest, '86count', 'Quantity Voided by Modifier');
					$auditController->createAudits($auditRequestVoidedOptMod);
				} else {
					$removedModifierIDsResponse = $count86Controller->addQuantityOnHand($modifierformattedRequest);
					//To Add Logs of Removed Inevntory Item for Optional Mofifiers
					$auditRequestRemovedOptMod = $auditFormatter->formatCreate86CountAuditRequest($modifierformattedRequest, '86count', 'Quantity Increased by Modifier');
					$auditController->createAudits($auditRequestRemovedOptMod);
				}
			}
		}

		$totalUpdate = self::decodeLavuAPIResponse($sentMenuItemIDsResponse)['RowsUpdated'] +
			self::decodeLavuAPIResponse($sentForcedModifierIDsResponse)['RowsUpdated'] +
			self::decodeLavuAPIResponse($sentModifierIDsResponse)['RowsUpdated'] +
			self::decodeLavuAPIResponse($removedMenuItemIDsResponse)['RowsUpdated'] +
			self::decodeLavuAPIResponse($removedForcedModifierIDsResponse)['RowsUpdated'] +
			self::decodeLavuAPIResponse($removedModifierIDsResponse)['RowsUpdated'];
		return self::encodeLavuAPIResponse(array('RowsUpdated' => $totalUpdate));

	}

	/**
	 * This function is used to deduct quantity from quantityOnHand based on quantityUsed from 86Count and
	 * ineventory_item table in offline mode
	 * @param $request
	 * @return array
	 */
	public function updateQuantityOnHand($request)
	{
		$count86Controller = new Inventory86CountController();
		$auditController = new InventoryAuditController();

		$formatter = new Inventory86CountRequestFormatter();
		$auditFormatter = new InventoryAuditRequestFormatter();

		$addedItemArr = $request[0]['added'];
		$removedItemArr = $request[0]['removed'];
		$totalUpdate = 0;
		$sentMenuItemIDsResponse = array();
		$sentForcedModifierIDsResponse = array();
		$sentModifierIDsResponse = array();
		$removedMenuItemIDsResponse = array();
		$removedForcedModifierIDsResponse = array();
		$removedModifierIDsResponse = array();

		// Processing sent menu item if confirm deliver to kitchen
		if (!empty($addedItemArr['item_id']) || !empty($addedItemArr['forced_modifier_id']) || !empty($addedItemArr['optional_modifier_id'])) {
			$sentMenuItemIDs = array();
			$sentForcedModifierIDs = array();
			$sentModifierIDs = array();
			if (isset($addedItemArr['item_id']) && !empty($addedItemArr['item_id'])) {
				$sentMenuItemIDs = self::getMenuItem86CountQuantity($addedItemArr['item_id'], true);
				$sentMenuItemIDsRequest = array();
				foreach ($sentMenuItemIDs as $key => $sentMenuItemIDsArray) {
					$sentMenuItemIDsRequest = array_merge($sentMenuItemIDsRequest, array_values($sentMenuItemIDsArray));

				}
				$menuItemformattedRequest = $formatter->formatDeduct86CountQuantity($sentMenuItemIDsRequest);
				$sentMenuItemIDsResponse = $count86Controller->deductMenuItemQuantityOnHand($menuItemformattedRequest);

				// Create audit event
				$auditRequest = $auditFormatter->formatCreate86CountAuditRequest($menuItemformattedRequest, '86count', 'Offline Quantity Decreased by Menu Item');
				$auditController->createAudits($auditRequest);
			}
			if (isset($addedItemArr['forced_modifier_id']) && !empty($addedItemArr['forced_modifier_id'])) {
				$sentForcedModifierIDs = self::getForcedModifier86CountQuantity($addedItemArr['forced_modifier_id'], true);
				$sentForcedModifierIDsRequest = array();
				foreach ($sentForcedModifierIDs as $key => $sentForcedModifierIDsArray) {
					$sentForcedModifierIDsRequest = array_merge($sentForcedModifierIDsRequest, array_values($sentForcedModifierIDsArray));

				}
				$forcedModifierformattedRequest = $formatter->formatDeduct86CountQuantity($sentForcedModifierIDsRequest);
				$sentForcedModifierIDsResponse = $count86Controller->deductForcedModifierItemQuantityOnHand($forcedModifierformattedRequest);

				// Create audit event
				$auditRequest = $auditFormatter->formatCreate86CountAuditRequest($forcedModifierformattedRequest, '86count', 'Offline Quantity Decreased by Forced Modifier');
				$auditController->createAudits($auditRequest);
			}

			if (isset($addedItemArr['optional_modifier_id']) && !empty($addedItemArr['optional_modifier_id'])) {
				$sentModifierIDs = self::getModifier86CountQuantity($addedItemArr['optional_modifier_id'], true);
				$sentModifierIDsRequest = array();
				foreach ($sentModifierIDs as $key => $sentModifierIDsArray) {
					$sentModifierIDsRequest = array_merge($sentModifierIDsRequest, array_values($sentModifierIDsArray));

				}
				$modifierformattedRequest = $formatter->formatDeduct86CountQuantity($sentModifierIDsRequest);
				$sentModifierIDsResponse = $count86Controller->deductModifierItemQuantityOnHand($modifierformattedRequest);

				// Create audit event
				$auditRequest = $auditFormatter->formatCreate86CountAuditRequest($modifierformattedRequest, '86count', 'Offline Quantity Decreased by Modifier');
				$auditController->createAudits($auditRequest);
			}

		}

		// Processing sent menu item if get request to cancele deliver order
		if (!empty($removedItemArr['item_id']) || !empty($removedItemArr['forced_modifier_id']) || !empty($removedItemArr['optional_modifier_id'])) {
			$sentMenuItemIDs = array();
			$sentForcedModifierIDs = array();
			$sentModifierIDs = array();
			if (isset($removedItemArr['item_id']) && !empty($removedItemArr['item_id'])) {
				$sentMenuItemIDs = self::getMenuItem86CountQuantity($removedItemArr['item_id'], true);
				$sentMenuItemIDsRequest = array();
				foreach ($sentMenuItemIDs as $key => $sentMenuItemIDsArray) {
					$sentMenuItemIDsRequest = array_merge($sentMenuItemIDsRequest, array_values($sentMenuItemIDsArray));

				}
				$menuItemformattedRequest = $formatter->formatDeduct86CountQuantity($sentMenuItemIDsRequest);
				$removedMenuItemIDsResponse = $count86Controller->addQuantityOnHand($menuItemformattedRequest);
			}
			if (isset($removedItemArr['forced_modifier_id']) && !empty($removedItemArr['forced_modifier_id'])) {
				$sentForcedModifierIDs = self::getForcedModifier86CountQuantity($removedItemArr['forced_modifier_id'], true);
				$sentForcedModifierIDsRequest = array();
				foreach ($sentForcedModifierIDs as $key => $sentForcedModifierIDsArray) {
					$sentForcedModifierIDsRequest = array_merge($sentForcedModifierIDsRequest, array_values($sentForcedModifierIDsArray));

				}
				$forcedModifierformattedRequest = $formatter->formatDeduct86CountQuantity($sentForcedModifierIDsRequest);
				$removedForcedModifierIDsResponse = $count86Controller->addQuantityOnHand($forcedModifierformattedRequest);
			}

			if (isset($removedItemArr['optional_modifier_id']) && !empty($removedItemArr['optional_modifier_id'])) {
				$sentModifierIDs = self::getModifier86CountQuantity($removedItemArr['optional_modifier_id'], true);
				$sentModifierIDsRequest = array();
				foreach ($sentModifierIDs as $key => $sentModifierIDsArray) {
					$sentModifierIDsRequest = array_merge($sentModifierIDsRequest, array_values($sentModifierIDsArray));

				}
				$modifierformattedRequest = $formatter->formatDeduct86CountQuantity($sentModifierIDsRequest);
				$removedModifierIDsResponse = $count86Controller->addQuantityOnHand($modifierformattedRequest);
			}

		}

		$totalUpdate = self::decodeLavuAPIResponse($sentMenuItemIDsResponse)['RowsUpdated'] +
			self::decodeLavuAPIResponse($sentForcedModifierIDsResponse)['RowsUpdated'] +
			self::decodeLavuAPIResponse($sentModifierIDsResponse)['RowsUpdated'] +
			self::decodeLavuAPIResponse($removedMenuItemIDsResponse)['RowsUpdated'] +
			self::decodeLavuAPIResponse($removedForcedModifierIDsResponse)['RowsUpdated'] +
			self::decodeLavuAPIResponse($removedModifierIDsResponse)['RowsUpdated'];
		return self::encodeLavuAPIResponse(array('RowsUpdated' => $totalUpdate));

	}

	/*
	 * This function is used to calculate total quantityUsed based on menu item id and inventory quantityUsed
	 * @param $menuItemQuantityArray
	 * @param bool $menuID if we want menuItemID
	 * @return array
	 */
	public function getMenuItem86CountQuantity($menuItemQuantityArray, $menuID = false)
	{
		$inventory86CountController = new Inventory86CountController();
		$menuItemIDsRequest = array();
		$menuItemIDsQuantity = array();
		$inventory86CountMenuItemResponse = array();
		if (!empty($menuItemQuantityArray)) {
			foreach ($menuItemQuantityArray as $key => $menuItemArray) {
				$menuItemIDsRequest[$key]['id'] = $menuItemArray['id'];
				if (!isset($menuItemIDsQuantity[$menuItemArray['id']])) {
					$menuItemIDsQuantity[$menuItemArray['id']] = 0;
				}
				if (!isset($menuItemArray['quantity'])) {
					$menuItemArray['quantity'] = 0;
				}
				$menuItemIDsQuantity[$menuItemArray['id']] += $menuItemArray['quantity'];
			}
			$inventory86CountMenuItemResponse = self::decodeLavuAPIResponse($inventory86CountController->get86CountMenuItem($menuItemIDsRequest));
		}
		$inventory86CountMenuItemIDsQuantityUsedResponse = array();
		//TODO:- Need to removed
		//$sUOMS = self::decodeLavuAPIResponse($inventoryItems->getStandardUnitConversion());
		if (!empty($inventory86CountMenuItemResponse)) {
			//If we need menuID in this customize array and need record segregated by menuItemID
			foreach ($inventory86CountMenuItemResponse as $inventory86CountMenuItemArr) {
				//Commented below block because not using it
				//$args[0]['id'] = $inventory86CountMenuItemArr['inventoryItemID'];
				//$inventoryItemsResponse = self::decodeLavuAPIResponse($inventoryItems->getInventoryItemsByID($args));
				//TODO:- Need to removed
				/*
				$unitIdArray['pUOM'] = $inventoryItemsResponse['recentPurchaseUnitID'];
				$unitIdArray['sUOM'] = $inventoryItemsResponse['salesUnitID'];
				$unitIdArray['sUOMS'] = $sUOMS;
				$unitConversion = self::decodeLavuAPIResponse($inventoryItems->getUnitConversion($unitIdArray));
				*/
				$menuItemQuantity = ($menuItemIDsQuantity[$inventory86CountMenuItemArr['menuItemID']] > 0) ? $menuItemIDsQuantity[$inventory86CountMenuItemArr['menuItemID']] : 1;
				//TODO:- Need to removed
				//$menuItemQuantityQuantityUsed =  round(($menuItemQuantity * $inventory86CountMenuItemArr['quantityUsed']) / $unitConversion, 3);
				$menuItemQuantityQuantityUsed = $menuItemQuantity * $inventory86CountMenuItemArr['quantityUsed'];
				if ($menuID) {
					$inventory86CountMenuItemIDsQuantityUsedResponse[$inventory86CountMenuItemArr['menuItemID']][$inventory86CountMenuItemArr['inventoryItemID']]['id'] = $inventory86CountMenuItemArr['menuItemID'];
					$inventory86CountMenuItemIDsQuantityUsedResponse[$inventory86CountMenuItemArr['menuItemID']][$inventory86CountMenuItemArr['inventoryItemID']]['inventoryItemID'] = $inventory86CountMenuItemArr['inventoryItemID'];
					if (!isset($inventory86CountMenuItemIDsQuantityUsedResponse[$inventory86CountMenuItemArr['menuItemID']][$inventory86CountMenuItemArr['inventoryItemID']]['quantity'])) {
						$inventory86CountMenuItemIDsQuantityUsedResponse[$inventory86CountMenuItemArr['menuItemID']][$inventory86CountMenuItemArr['inventoryItemID']]['quantity'] = 0;
					}
					$inventory86CountMenuItemIDsQuantityUsedResponse[$inventory86CountMenuItemArr['menuItemID']][$inventory86CountMenuItemArr['inventoryItemID']]['quantity'] += $menuItemQuantityQuantityUsed;
					$inventory86CountMenuItemIDsQuantityUsedResponse[$inventory86CountMenuItemArr['menuItemID']][$inventory86CountMenuItemArr['inventoryItemID']]['unitID'] = $inventory86CountMenuItemArr['unitID'];
				} else {
					$inventory86CountMenuItemIDsQuantityUsedResponse[$inventory86CountMenuItemArr['inventoryItemID']]['inventoryItemID'] = $inventory86CountMenuItemArr['inventoryItemID'];
					if (!isset($inventory86CountMenuItemIDsQuantityUsedResponse[$inventory86CountMenuItemArr['inventoryItemID']]['quantity'])) {
						$inventory86CountMenuItemIDsQuantityUsedResponse[$inventory86CountMenuItemArr['inventoryItemID']]['quantity'] = 0;
					}
					$inventory86CountMenuItemIDsQuantityUsedResponse[$inventory86CountMenuItemArr['inventoryItemID']]['quantity'] += $menuItemQuantityQuantityUsed;
					$inventory86CountMenuItemIDsQuantityUsedResponse[$inventory86CountMenuItemArr['inventoryItemID']]['unitID'] = $inventory86CountMenuItemArr['unitID'];
				}

			}
		}
		return $inventory86CountMenuItemIDsQuantityUsedResponse;
	}

	/*
	 * This function is used to calculate total quantityUsed based on forced modifier id and inventory quantityUsed
	 * @param $forcedModifierQuantityArray
	 * @param bool $forcedModifierID if we want forced_modifierID
	 * @return array
	 */
	public function getForcedModifier86CountQuantity($forcedModifierQuantityArray, $forcedModifierID)
	{
		$inventory86CountController = new Inventory86CountController();
		$forcedModifierIDsRequest = array();
		$forcedModifierIDsQuantity = array();
		$inventory86CountForcedModifierResponse = array();
		if (!empty($forcedModifierQuantityArray)) {
			foreach ($forcedModifierQuantityArray as $key => $forcedModifierArray) {
				$forcedModifierIDsRequest[$key]['id'] = $forcedModifierArray['id'];
				if (!isset($forcedModifierIDsQuantity[$forcedModifierArray['id']])) {
					$forcedModifierIDsQuantity[$forcedModifierArray['id']] = 0;
				}
				if (!isset($forcedModifierArray['quantity'])) {
					$forcedModifierArray['quantity'] = 0;
				}
				$forcedModifierIDsQuantity[$forcedModifierArray['id']] += $forcedModifierArray['quantity'];
			}
			$inventory86CountForcedModifierResponse = self::decodeLavuAPIResponse($inventory86CountController->get86CountForcedModifier($forcedModifierIDsRequest));
		}
		$inventory86CountForcedModifierIDsQuantityUsedResponse = array();
		//TODO:- Need to removed
		//$sUOMS = self::decodeLavuAPIResponse($inventoryItems->getStandardUnitConversion());
		if (!empty($inventory86CountForcedModifierResponse)) {
			//If we need forced_modifierID in this customize array and need record segregated by forced_modifierID
			foreach ($inventory86CountForcedModifierResponse as $inventory86CountForcedModifierArr) {
				//Commented below block because not using it
				//$args[0]['id'] = $inventory86CountForcedModifierArr['inventoryItemID'];
				//$inventoryItemsResponse = self::decodeLavuAPIResponse($inventoryItems->getInventoryItemsByID($args));
				/*
				$unitIdArray['pUOM'] = $inventoryItemsResponse['recentPurchaseUnitID'];
				$unitIdArray['sUOM'] = $inventoryItemsResponse['salesUnitID'];
				$unitIdArray['sUOMS'] = $sUOMS;
				$unitConversion = self::decodeLavuAPIResponse($inventoryItems->getUnitConversion($unitIdArray));
				*/
				$forcedModifierQuantity = ($forcedModifierIDsQuantity[$inventory86CountForcedModifierArr['forced_modifierID']] > 0) ? $forcedModifierIDsQuantity[$inventory86CountForcedModifierArr['forced_modifierID']] : 1;
				//TODO:- Need to removed
				//$forcedModifierQuantityQuantityUsed = round(($forcedModifierQuantity * $inventory86CountForcedModifierArr['quantityUsed']) / $unitConversion, 3);
				$forcedModifierQuantityQuantityUsed = $forcedModifierQuantity * $inventory86CountForcedModifierArr['quantityUsed'];
				if ($forcedModifierID) {
					$inventory86CountForcedModifierIDsQuantityUsedResponse[$inventory86CountForcedModifierArr['forced_modifierID']][$inventory86CountForcedModifierArr['inventoryItemID']]['id'] = $inventory86CountForcedModifierArr['forced_modifierID'];
					$inventory86CountForcedModifierIDsQuantityUsedResponse[$inventory86CountForcedModifierArr['forced_modifierID']][$inventory86CountForcedModifierArr['inventoryItemID']]['inventoryItemID'] = $inventory86CountForcedModifierArr['inventoryItemID'];
					if (!isset($inventory86CountForcedModifierIDsQuantityUsedResponse[$inventory86CountForcedModifierArr['forced_modifierID']][$inventory86CountForcedModifierArr['inventoryItemID']]['quantity'])) {
						$inventory86CountForcedModifierIDsQuantityUsedResponse[$inventory86CountForcedModifierArr['forced_modifierID']][$inventory86CountForcedModifierArr['inventoryItemID']]['quantity'] = 0;
					}
					$inventory86CountForcedModifierIDsQuantityUsedResponse[$inventory86CountForcedModifierArr['forced_modifierID']][$inventory86CountForcedModifierArr['inventoryItemID']]['quantity'] += $forcedModifierQuantityQuantityUsed;
					$inventory86CountForcedModifierIDsQuantityUsedResponse[$inventory86CountForcedModifierArr['forced_modifierID']][$inventory86CountForcedModifierArr['inventoryItemID']]['unitID'] = $inventory86CountForcedModifierArr['unitID'];
				} else {
					$inventory86CountForcedModifierIDsQuantityUsedResponse[$inventory86CountForcedModifierArr['inventoryItemID']]['inventoryItemID'] = $inventory86CountForcedModifierArr['inventoryItemID'];
					if (!isset($inventory86CountForcedModifierIDsQuantityUsedResponse[$inventory86CountForcedModifierArr['inventoryItemID']]['quantity'])) {
						$inventory86CountForcedModifierIDsQuantityUsedResponse[$inventory86CountForcedModifierArr['inventoryItemID']]['quantity'] = 0;
					}
					$inventory86CountForcedModifierIDsQuantityUsedResponse[$inventory86CountForcedModifierArr['inventoryItemID']]['quantity'] += $forcedModifierQuantityQuantityUsed;
					$inventory86CountForcedModifierIDsQuantityUsedResponse[$inventory86CountForcedModifierArr['inventoryItemID']]['unitID'] = $inventory86CountForcedModifierArr['unitID'];
				}
			}
		}
		return $inventory86CountForcedModifierIDsQuantityUsedResponse;
	}

	/*
	 * This function is used to calculate total quantityUsed based on forced modifier id and inventory quantityUsed
	 * @param $modifierIDsQuantityArray
	 * @param bool $modifierID if we want modifierID
	 * @return array
	 */
	public function getModifier86CountQuantity($modifierIDsQuantityArray, $modifierID = false)
	{
		$inventory86CountController = new Inventory86CountController();
		$modifierIDsRequest = array();
		$modifierIDsQuantity = array();
		$inventory86CountModifierResponse = array();
		if (!empty($modifierIDsQuantityArray)) {
			foreach ($modifierIDsQuantityArray as $key => $modifierArray) {
				$modifierIDsRequest[$key]['id'] = $modifierArray['id'];
				if (!isset($modifierIDsQuantity[$modifierArray['id']])) {
					$modifierIDsQuantity[$modifierArray['id']] = 0;
				}
				if (!isset($modifierArray['quantity'])) {
					$modifierArray['quantity'] = 0;
				}
				$modifierIDsQuantity[$modifierArray['id']] += $modifierArray['quantity'];
			}
			$inventory86CountModifierResponse = self::decodeLavuAPIResponse($inventory86CountController->get86CountModifier($modifierIDsRequest));
		}

		$inventory86CountModifierIDsQuantityUsedResponse = array();
		//TODO:- Need to removed
		//$sUOMS = self::decodeLavuAPIResponse($inventoryItems->getStandardUnitConversion());
		if (!empty($inventory86CountModifierResponse)) {
			//If we need modifierID in this customize array and need record segregated by modifierID
			foreach ($inventory86CountModifierResponse as $inventory86CountModifierArr) {
				//Commented below block because not using it
				//$args[0]['id'] = $inventory86CountModifierArr['inventoryItemID'];
				//$inventoryItemsResponse = self::decodeLavuAPIResponse($inventoryItems->getInventoryItemsByID($args));
				//TODO:- Need to removed
				/*
				$unitIdArray['pUOM'] = $inventoryItemsResponse['recentPurchaseUnitID'];
				$unitIdArray['sUOM'] = $inventoryItemsResponse['salesUnitID'];
				$unitIdArray['sUOMS'] = $sUOMS;
				$unitConversion = self::decodeLavuAPIResponse($inventoryItems->getUnitConversion($unitIdArray));
				*/
				$modifierQuantity = ($modifierIDsQuantity[$inventory86CountModifierArr['modifierID']] > 0) ? $modifierIDsQuantity[$inventory86CountModifierArr['modifierID']] : 1;
				//TODO:- Need to removed
				//$modifierQuantityQuantityUsed = round(($modifierQuantity * $inventory86CountModifierArr['quantityUsed']) / $unitConversion, 3);
				$modifierQuantityQuantityUsed = $modifierQuantity * $inventory86CountModifierArr['quantityUsed'];
				if ($modifierID) {
					$inventory86CountModifierIDsQuantityUsedResponse[$inventory86CountModifierArr['modifierID']][$inventory86CountModifierArr['inventoryItemID']]['id'] = $inventory86CountModifierArr['modifierID'];
					$inventory86CountModifierIDsQuantityUsedResponse[$inventory86CountModifierArr['modifierID']][$inventory86CountModifierArr['inventoryItemID']]['inventoryItemID'] = $inventory86CountModifierArr['inventoryItemID'];
					if (!isset($inventory86CountModifierIDsQuantityUsedResponse[$inventory86CountModifierArr['modifierID']][$inventory86CountModifierArr['inventoryItemID']]['quantity'])) {
						$inventory86CountModifierIDsQuantityUsedResponse[$inventory86CountModifierArr['modifierID']][$inventory86CountModifierArr['inventoryItemID']]['quantity'] = 0;
					}
					$inventory86CountModifierIDsQuantityUsedResponse[$inventory86CountModifierArr['modifierID']][$inventory86CountModifierArr['inventoryItemID']]['quantity'] += $modifierQuantityQuantityUsed;
					$inventory86CountModifierIDsQuantityUsedResponse[$inventory86CountModifierArr['modifierID']][$inventory86CountModifierArr['inventoryItemID']]['unitID'] = $inventory86CountModifierArr['unitID'];
				} else {
					$inventory86CountModifierIDsQuantityUsedResponse[$inventory86CountModifierArr['inventoryItemID']]['inventoryItemID'] = $inventory86CountModifierArr['inventoryItemID'];
					if (!isset($inventory86CountModifierIDsQuantityUsedResponse[$inventory86CountModifierArr['inventoryItemID']]['quantity'])) {
						$inventory86CountModifierIDsQuantityUsedResponse[$inventory86CountModifierArr['inventoryItemID']]['quantity'] = 0;
					}
					$inventory86CountModifierIDsQuantityUsedResponse[$inventory86CountModifierArr['inventoryItemID']]['quantity'] += $modifierQuantityQuantityUsed;
					$inventory86CountModifierIDsQuantityUsedResponse[$inventory86CountModifierArr['inventoryItemID']]['unitID'] = $inventory86CountModifierArr['unitID'];
				}
			}
		}
		return $inventory86CountModifierIDsQuantityUsedResponse;
	}

	/**
	 * This function is used to get status based on menu and its quantity
	 * @param $request
	 * @return array
	 */
	public function getMenuStatus($request)
	{
		$count86Controller = new Inventory86CountController();
		$menuStatusResponse = $count86Controller->getMenuStatus($request);
		return $menuStatusResponse;
	}

	/**
	 * This function is used to get status based on menu and its quantity and 86count lowQuantity
	 * @param $request
	 * @return array
	 */
	public function getMenuLowQuantityStatus($request)
	{
		$count86Controller = new Inventory86CountController();
		$formatter = new Inventory86CountRequestFormatter();
		$addedMenuItemArr = $request[0]['item_id'];
		$addedForcedModifierArr = $request[0]['forced_modifier_id'];
		$addedModifierArr = $request[0]['optional_modifier_id'];
		$menuStatus = array();
		// Processing menu item status
		if (!empty($addedMenuItemArr)) {
			$formattedRequest = $formatter->formatMenuItemsStatus($addedMenuItemArr);
			$menuItemStatusResponse = self::decodeLavuAPIResponse($count86Controller->getMenuItemsLowQuantityStatus($formattedRequest));
			$index = 0;
			foreach ($menuItemStatusResponse as $menuItemId => $status) {
				$menuStatus['item_id'][$index]['id'] = $menuItemId;
				$menuStatus['item_id'][$index]['status'] = $status;
				$index++;
			}
		}
		// Processing forced modifier status
		if (!empty($addedForcedModifierArr)) {
			$formattedRequest = $formatter->formatForcedModifiersStatus($addedForcedModifierArr);
			$forcedModifierStatusResponse = self::decodeLavuAPIResponse($count86Controller->getForcedModifiersLowQuantityStatus($formattedRequest));
			$index = 0;
			foreach ($forcedModifierStatusResponse as $forcedModifierId => $status) {
				$menuStatus['forced_modifier_id'][$index]['id'] = $forcedModifierId;
				$menuStatus['forced_modifier_id'][$index]['status'] = $status;
				$index++;
			}
		}
		// Processing modifier status
		if (!empty($addedModifierArr)) {
			$formattedRequest = $formatter->formatModifiersStatus($addedModifierArr);
			$modifierStatusResponse = self::decodeLavuAPIResponse($count86Controller->getModifiersLowQuantityStatus($formattedRequest));
			$index = 0;
			foreach ($modifierStatusResponse as $modifierId => $status) {
				$menuStatus['optional_modifier_id'][$index]['id'] = $modifierId;
				$menuStatus['optional_modifier_id'][$index]['status'] = $status;
				$index++;
			}
		}
		return self::encodeLavuAPIResponse($menuStatus);
	}

	/*
	Description:- This method is used get status of all inventory items which is related to menu items, forced modifier and
	modifier based on quantityOnHand, quantityUsed and reservation as well as get status of all inventory items which is related to menu items, forced modifier
	and modifier based on quantityOnHand, quantityUsed and 86count
	Input:-
	Output:- json string {"DataResponse":{"DataResponse":{"86CountStatus":{"menuItem":{"12":"In_Stock","32":"In_Stock"},"forcedModifier":{"42":"In_Stock","62":"In_Stock"},"optionalModifier":{"52":"In_Stock","72":"In_Stock"}},"86CountInventoryStatus":{"menuItem":{"12":"In_Stock","32":"In_Stock"},"forcedModifier":{"Cheeze":"In_Stock","Salad":"In_Stock"},"optionalModifier":{"Pepperoni":"In_Stock","Red Onion":"In_Stock"}}},"RequestID":0,"Status":"Success"}
	*/
	public function getInventoryStatus()
	{
		$count86Controller = new Inventory86CountController();
		$menuItemsStatusResponse = self::decodeLavuAPIResponse($count86Controller->getMenuItemsStatus());
		$forcedModifiersStatusResponse = self::decodeLavuAPIResponse($count86Controller->getForcedModifiersStatus());
		$modifiersStatusResponse = self::decodeLavuAPIResponse($count86Controller->getModifiersStatus());
		$menuItemsLowQuantityStatusResponse = self::decodeLavuAPIResponse($count86Controller->getMenuItemsLowQuantityStatus());
		$forcedModifiersLowQuantityStatusResponse = self::decodeLavuAPIResponse($count86Controller->getForcedModifiersLowQuantityStatus());
		$modifiersLowQuantityStatusResponse = self::decodeLavuAPIResponse($count86Controller->getModifiersLowQuantityStatus());

		$inventoryResponse['86CountStatus']['menuItem'] = $menuItemsStatusResponse;
		$inventoryResponse['86CountStatus']['forcedModifier'] = $forcedModifiersStatusResponse;
		$inventoryResponse['86CountStatus']['optionalModifier'] = $modifiersStatusResponse;
		$inventoryResponse['86CountInventoryStatus']['menuItem'] = $menuItemsLowQuantityStatusResponse;
		$inventoryResponse['86CountInventoryStatus']['forcedModifier'] = $forcedModifiersLowQuantityStatusResponse;
		$inventoryResponse['86CountInventoryStatus']['optionalModifier'] = $modifiersLowQuantityStatusResponse;
		return self::encodeLavuAPIResponse($inventoryResponse);
	}

	// Get stock status of given menu item id which has quantity 1.
	public function getMenuItemStatus($request)
	{
		$response = array();
		if ($request != '') {
			$request = createRequestFromURLParams($request);
			$formatter = new Inventory86CountRequestFormatter();
			$controller = new Inventory86CountController();
			$formattedRequest = $formatter->formatMenuItemsStatus($request);
			$response = $controller->getMenuItemsStatus($formattedRequest);
		}
		return $response;
	}

	// Get stock status of given optional modifier id which has quantity 1.
	public function getModifierStatus($request)
	{
		$response = array();
		if ($request != '') {
			$request = createRequestFromURLParams($request);
			$formatter = new Inventory86CountRequestFormatter();
			$controller = new Inventory86CountController();
			$formattedRequest = $formatter->formatModifiersStatus($request);
			$response = $controller->getModifiersStatus($formattedRequest);
		}
		return $response;
	}

	// Get stock status of given forced modifier id which has quantity 1.
	public function getForcedModifierStatus($request)
	{
		$response = array();
		if ($request != '') {
			$request = createRequestFromURLParams($request);
			$formatter = new Inventory86CountRequestFormatter();
			$controller = new Inventory86CountController();
			$formattedRequest = $formatter->formatModifiersStatus($request);
			$response = $controller->getForcedModifiersStatus($formattedRequest);
		}
		return $response;
	}

	// Get low stock status of given menu item id which has quantity 1.
	public function getMenuItemLowStockStatus($request)
	{
		$response = array();
		if ($request != '') {
			$request = createRequestFromURLParams($request);
			$formatter = new Inventory86CountRequestFormatter();
			$controller = new Inventory86CountController();
			$formattedRequest = $formatter->formatMenuItemsStatus($request);
			$response = $controller->getMenuItemsLowQuantityStatus($formattedRequest);
		}
		return $response;
	}

	// Get low stock status of given optional modifier id which has quantity 1.
	public function getModifierLowStockStatus($request)
	{
		$response = array();
		if ($request != '') {
			$request = createRequestFromURLParams($request);
			$formatter = new Inventory86CountRequestFormatter();
			$controller = new Inventory86CountController();
			$formattedRequest = $formatter->formatModifiersStatus($request);
			$response = $controller->getModifiersLowQuantityStatus($formattedRequest);
		}
		return $response;
	}

	// Get low stock status of given forced modifier id which has quantity 1.
	public function getForcedModifierLowStockStatus($request)
	{
		$response = array();
		if ($request != '') {
			$request = createRequestFromURLParams($request);
			$formatter = new Inventory86CountRequestFormatter();
			$controller = new Inventory86CountController();
			$formattedRequest = $formatter->formatForcedModifiersStatus($request);
			$response = $controller->getForcedModifiersLowQuantityStatus($formattedRequest);
		}
		return $response;
	}

	/*
	Description:- This method is used to get status of all inventory items which is related to menu items, forced modifier and
	modifier based on quantityOnHand, quantityUsed and reservation as well as get status of all inventory items which is related to menu items, forced modifier
	and modifier based on quantityOnHand, quantityUsed and 86count
	Input:-
	Output:- json string {"DataResponse":{"DataResponse":{"86CountStatus":{"menuItem":{"12":"In_Stock","32":"In_Stock"},"forcedModifier":{"42":"In_Stock","62":"In_Stock"},"optionalModifier":{"52":"In_Stock","72":"In_Stock"}},"86CountInventoryStatus":{"menuItem":{"12":"In_Stock","32":"In_Stock"},"forcedModifier":{"Cheeze":"In_Stock","Salad":"In_Stock"},"optionalModifier":{"Pepperoni":"In_Stock","Red Onion":"In_Stock"}}},"RequestID":0,"Status":"Success"}
	*/
	public function getInventoryStatusByCategory($categoryIds)
	{
		$menuItemsStatusResponse = array();
		$forcedModifiersStatusResponse = array();
		$modifiersStatusResponse = array();
		$menuItemsLowQuantityStatusResponse = array();
		$forcedModifiersLowQuantityStatusResponse = array();
		$modifiersLowQuantityStatusResponse = array();
		$count86Controller = new Inventory86CountController();
		$menuResponse = self::decodeLavuAPIResponse($count86Controller->getMenuByCategory($categoryIds));
		if (!empty($menuResponse['menu_item'])) {
			$ids = implode(';', $menuResponse['menu_item']);
			$menuItemsStatusResponse = self::decodeLavuAPIResponse(self::getMenuItemStatus($ids));
			$menuItemsLowQuantityStatusResponse = self::decodeLavuAPIResponse(self::getMenuItemLowStockStatus($ids));
		}

		if (!empty($menuResponse['forced_modifier'])) {
			$ids = implode(';', $menuResponse['forced_modifier']);
			$forcedModifiersStatusResponse = self::decodeLavuAPIResponse(self::getForcedModifierStatus($ids));
			$forcedModifiersLowQuantityStatusResponse = self::decodeLavuAPIResponse(self::getForcedModifierLowStockStatus($ids));
		}

		if (!empty($menuResponse['optional_modifier'])) {
			$ids = implode(';', $menuResponse['optional_modifier']);
			$modifiersStatusResponse = self::decodeLavuAPIResponse(self::getModifierStatus($ids));
			$modifiersLowQuantityStatusResponse = self::decodeLavuAPIResponse(self::getModifierLowStockStatus($ids));
		}

		$inventoryResponse['86CountStatus']['menuItem'] = $menuItemsStatusResponse;
		$inventoryResponse['86CountStatus']['forcedModifier'] = $forcedModifiersStatusResponse;
		$inventoryResponse['86CountStatus']['optionalModifier'] = $modifiersStatusResponse;
		$inventoryResponse['86CountInventoryStatus']['menuItem'] = $menuItemsLowQuantityStatusResponse;
		$inventoryResponse['86CountInventoryStatus']['forcedModifier'] = $forcedModifiersLowQuantityStatusResponse;
		$inventoryResponse['86CountInventoryStatus']['optionalModifier'] = $modifiersLowQuantityStatusResponse;

		return self::encodeLavuAPIResponse($inventoryResponse);
	}
}
