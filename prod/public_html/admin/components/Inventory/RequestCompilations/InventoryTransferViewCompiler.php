<?php

/**
 * Created by Leif Guillermo, with help from his cat Xander.
 * User: Leif
 * Date: 5/2/17
 * Time: 3:06 PM
 */

/**
 *Transfers

Transfer number: System Generated, increments with each add of a transfer.
Start Date: Date request was made, system generated
Last Action Date: Time stamp, from audit trail.
To/From location:  May need database table created for this.??????
Stage/Status: closed/partially delivered/ accepted/declined/requested.

Create/New Transfer (V1) can add multiple items
From Location (may be pre-filled with current location need an api call)
To Location (may be pre-filled with current location need an api call)

Auto Populated = AP

Item Name (If this is set, auto populate)
AP Inventory ID
Quantity AP PurchaseUOM
TotalTransferCost = Quantity*PricePerUnit
AP BIN
AP Serial Number
AP SKU

Close Transfer

accept/decline transfer
decline transfer declines the whole transfer.

Create Transfer Request (Version 2)
 */
require_once __DIR__. "/../InventoryObjectControllers/InventoryTransferController.php";
require_once __DIR__. "/../InventoryObjectControllers/InventoryEnterpriseController.php";
require_once __DIR__. "/../RequestFormatters/InventoryTransferRequestFormatter.php";
require_once __DIR__. "/../../../cp/resources/core_functions.php";

class InventoryTransferViewCompiler extends BasicFormatter
{
	public function getTransfersByID($request){
		$controller = new InventoryTransferController();
		$result = self::decodeLavuAPIResponse($controller->getTransfersByID(self::formatIDArray($request)));
		$result = array_map(array($this, "setPartialFieldInSingleTransfer"), $result);
		return self::encodeLavuAPIResponse($result);
	}

	private function setPartialFieldInSingleTransfer($transfer){
		$partial = 0;
		if($transfer['received'] == 1){
			$transferItems = $transfer['transferItems'];
			for($i = 0; $i < count($transferItems); $i++){
				if($transferItems[$i]['quantityTransferred'] > $transferItems[$i]['quantityReceived']){
					$partial = 1;
					break;
				}
			}
		}
		$transfer['partial'] = $partial;
		return $transfer;
	}

	public function getAllTransferViewRows($request = array()){

		$inventoryTransferController = new InventoryTransferController();
		$inventoryEnterpriseController = new InventoryEnterpriseController();
		$inventoryTransferItemController = new InventoryTransferItemController();

		#get all transfers data
		$transfers = self::decodeLavuAPIResponse($inventoryTransferController->getAllTransfers($request));

		#get all transfers items
		$transferItems = self::decodeLavuAPIResponse($inventoryTransferItemController->getAllTransferItems());

		#get all chain restaurance
		$allChainedRestaurants = self::decodeLavuAPIResponse($inventoryEnterpriseController->getAllChainedRestaurants());

		$thisRestaurant = self::decodeLavuAPIResponse($inventoryEnterpriseController->getRestaurantIDAndTitle());
		$thisRestaurantID = $thisRestaurant[0]['restaurantid'];

		$transfers = self::joinFieldsOrRowsFromTable2ToTable1($transfers,$transferItems,'id','transferID',array(),array('transferItems'));
	
		$resultArray = array();
		for($i = 0; $i < count($transfers); $i++){
			$originRestaurant = $this->getRestaurantNameByID($transfers[$i]['originRestaurantID'], $allChainedRestaurants);
			$destinationRestaurant = $this->getRestaurantNameByID($transfers[$i]['destinationRestaurantID'], $allChainedRestaurants);
			$resultArray[$i]['id']               = $transfers[$i]['id'];
			$resultArray[$i]['transfer_id']      = $transfers[$i]['transferNumber'];
			$resultArray[$i]['date_created']     = $transfers[$i]['orderDate'];

			if(isset($request['stage']) && $request['stage'] !=''){
				$resultArray[$i]['transfer_stage']  = 	$this->getSearchedTransferStage($transfers[$i], $request);
			}else{
				$resultArray[$i]['transfer_stage']  = 	$this->getTransferStage($transfers[$i]);
			}

			$resultArray[$i]['last_action_date'] = $this->getLastActionDate($transfers[$i]);
			$resultArray[$i]['transferItemsCount'] = $transfers[$i]['transferItemsCount'];
			$tfl = $this->getToFromLocation($transfers[$i],$thisRestaurantID,$originRestaurant,$destinationRestaurant);
			$resultArray[$i]['to_from'] = $tfl['toOrFrom'];
			$resultArray[$i]['location'] = $tfl['location'];
		}

		#if sorting is being applied on value on inventory list view
		$excludeSortingFields = array("location","transfer_stage");
		if(isset($request['sort_by']) && in_array($request['sort_by'],$excludeSortingFields)){
			$resultArray = $this->getSortedValue($resultArray, $request['sort_by'],$request['order_by'], $request['limit']);
		}
		
		//Updated By Dhruvpalsinh on 30-01-2019
		$resulttransfercount = InventoryQueries::getAllTransfersCount($request);
		$transfercount= $resulttransfercount[0]['transferCount'];
		return self::encodeLavuAPIResponse($resultArray,$transfercount);
	}

	private function getRestaurantNameByID($id, $allRestaurants){
		for($i = 0; $i < count($allRestaurants); $i++){
			if($id == $allRestaurants[$i]['id']){
				return $allRestaurants[$i]['name'];
			}
		}
	}
	/**
	 * Description:- show transaction stage as per search filter
	 * Input:- selected stages
	 * Output:- return transaction stage
	 * Issue: LP-10195
	 */
	public function getSearchedTransferStage($transfer,$request){
		$tranferStage = explode(",",$request['stage']);

		if(isset($transfer['transferItems']))
		{
			$missingItems = $this->transferHasMissingItems($transfer['transferItems']);
		}else{
			$missingItems = false;
		}

		if($transfer['received'] == 1){
			if($missingItems === false && in_array('closed', $tranferStage)){
				return 'closed';
			}elseif($missingItems === true && in_array('partial', $tranferStage)){
				return 'partial';
			}
			$stage = true;
		}

		if($transfer['sent'] == 1 && in_array('sent', $tranferStage)){
			return 'sent';
		}

		if($transfer['accepted'] == 1 && in_array('accepted', $tranferStage)){
			return 'accepted';
		}

		if($transfer['accepted'] == 0 && in_array('declined', $tranferStage)){
			return 'declined';
		}

		if(in_array('pending', $tranferStage)){
			return 'pending';
		}
	}
	private function getTransferStage($transfer){
		if(isset($transfer['transferItems']))
		{
			$missingItems = $this->transferHasMissingItems($transfer['transferItems']);
		}else{
			$missingItems = false;
		}

		$transfer['stage'] = 'pending';

		if($transfer['accepted'] == 1){
			$transfer['stage'] = 'accepted';
		}
		else if($transfer['accepted'] == 0){
			return 'declined';

		} //if accepted == -1, stage is still pending.

		if($transfer['sent'] == 1){
			$transfer['stage'] = 'sent';
		}

		if($transfer['received'] == 1){
			if($missingItems === true){
				$transfer['stage'] = 'partial';
			}
			else
			{
				$transfer['stage'] = 'closed';
			}
		}

		return $transfer['stage'];
	}
	private function transferHasMissingItems($transferItems){
		for($i = 0; $i < count($transferItems); $i++){
			$transferItem = $transferItems;
			$quantityTransfered = $transferItem['quantityTransferred'];
			$quantityReceived = $transferItem['quantityReceived'];
			if($quantityReceived < $quantityTransfered){
				return true;
			}
		}
		return false;
	}

	private function getLastActionDate($transfers){
		$lastAction = $transfers['lastAction'];
		if($lastAction == "0000-00-00 00:00:00"){
			$lastAction = $transfers['orderDate'];
		}
		return $lastAction;
	}

	private function getToFromLocation($transfers, $thisLocationID, $originRestaurantName, $destinationRestaurantName){
		$destinationLocationID = $transfers['destinationRestaurantID'];
		$originLocationID = $transfers['originRestaurantID'];

		$toFromLocation = array();
		if($thisLocationID == $destinationLocationID){
			$toFromLocation['toOrFrom'] = "from";
			$toFromLocation['location'] = $originRestaurantName;
		}else if($thisLocationID == $originLocationID){
			$toFromLocation['toOrFrom'] = "to";
			$toFromLocation['location'] = $destinationRestaurantName;
		}
		return $toFromLocation;
	}

	/**
	 *  Updates inventory item quantity.
	 *  Updates Transfer status
	 *  Creates a new audit.
	 */
	public function receiveTransfers($request){
		$auditFormatter = new InventoryAuditRequestFormatter();
		$inventoryItemFormatter = new InventoryItemRequestFormatter();
		$transferItemFormatter = new InventoryTransferItemRequestFormatter();

		$inventoryItemController = new InventoryItemController();
		$transferController = new InventoryTransferController();
		$transferItemController = new InventoryTransferItemController();
		$auditController = new InventoryAuditController();

		for($i=0; $i < count($request); $i++){
			$externalItem = $this->decodeLavuAPIResponse($inventoryItemController->getInventoryItemsByTransferItemNumbers(array('transferItemNumber'=>$request[$i]['transferItemNumber'])));
            $externalItemName = $externalItem['name'];
			$localItem = $this->decodeLavuAPIResponse($inventoryItemController->getInventoryItemsByName(array("name"=>$externalItemName)));
			if($localItem === "Item_DNE"){
				$newItem = $externalItem;
				unset($newItem['id']);
				$newItem['quantityOnHand'] = 0; // setting the on hand quantity of the item to be created to 0, since it is a brand new item.
				$localItem = $this->decodeLavuAPIResponse($inventoryItemController->createInventoryItems(array($newItem)));
                $transferItemID= $localItem['InsertID'];
			}
			else{
			    $transferItemID = $localItem[0]['id'];
            }
			$request[$i]['inventoryItemID'] = $transferItemID;
		}

		//update transfer
		//$transferResponse = self::decodeLavuAPIResponse($transferController->receiveTransfers($request));

		//update inventory item quantities
		$transferResponse = self::decodeLavuAPIResponse($transferController->receiveTransfers($request));
        $inventoryItemRequest = $inventoryItemFormatter->formatQuantityAdjustmentWithUnit($request);
        $inventoryItemController->addQuantitiesToInventoryItems($inventoryItemRequest);


		//Update transfer items
		$auditRequest = array();
		foreach($request as $i=>$requestItem){
			$requestItem['cost'] = $requestItem['cost'] * $requestItem['quantity']; //For audit, we're tracking total cost.

			// Get TransferItem using TransferID and InventoryItemID to set transferItemID for audit (needed for Transfer report)
			$transferItemRequest = $transferItemFormatter->formatGetTransferItemByTransferIDAndInventoryItemID(array($requestItem));
			$transferItemResponse = self::decodeLavuAPIResponse($transferItemController->getTransferItemByTransferIDAndInventoryItemID($transferItemRequest));
			$transferItem = array_shift($transferItemResponse);
			$requestItem['tranferItemID'] = $requestItem['inventoryItemID'];
			$auditRequest[] = $requestItem;
		}

		//create row in audit table.
		$auditRequest = $auditFormatter->formatCreateAuditRequest($auditRequest,'receiveTransfer');
		$auditController->createTransferReceivedAudit($auditRequest);

		return self::encodeLavuAPIResponse($transferResponse);
	}

	private function generateFakeData(){
		$transfers = array();
		$transfers[0]['id'] = 1;
		$transfers[0]['transferNumber'] = 9;
		$transfers[0]['sent'] = 1;
		$transfers[0]['received'] = 0;
		$transfers[0]['orderDate'] = "2017-05-06 03:37:12";
		$transfers[0]['lastAction'] = "2017-05-06 03:37:12";
		$transfers[0]['destinationRestaurantID'] = 1;
		$transfers[0]['originRestaurantID'] = 2;

		$transfers[1]['id'] = 2;
		$transfers[1]['transferNumber'] = 10;
		$transfers[1]['sent'] = 0;
		$transfers[1]['received'] = 0;
		$transfers[1]['orderDate'] = "2017-05-07 04:15:12";
		$transfers[1]['lastAction'] = "2017-05-07 04:15:12";
		$transfers[1]['destinationRestaurantID'] = 2;
		$transfers[1]['originRestaurantID'] = 1;

		$transfers[2]['id'] = 3;
		$transfers[2]['transferNumber'] = 11;
		$transfers[2]['sent'] = 1;
		$transfers[2]['received'] = 1;
		$transfers[2]['orderDate'] = "2017-05-08 11:15:12";
		$transfers[2]['lastAction'] = "2017-05-08 11:15:12";
		$transfers[2]['destinationRestaurantID'] = 3;
		$transfers[2]['originRestaurantID'] = 1;
		return $transfers;
	}

	public function exportCSVTransfers()
	{
		$fileName = 'Transfers.csv';
		$transferItems = self::decodeLavuAPIResponse(self::getAllTransferViewRows());
		$data = 'No Record found';
		if (!empty($transferItems)) {
			$data = implode(',', str_replace(',', ' ', array_keys($transferItems[0]))) . "\n";
			foreach ($transferItems as $details) {
				$data .=  implode(',', str_replace(',', ' ', $details)) . "\n";
			}
		}
		$result = self::exportCsv($data, $fileName);
		return true;
	}

	 /**
	 * Description:- sThis function will call while sorting is applied on value on inventory list page and 
	 * It will create a temporary table and insert all inventory data
	 * Input:- $inventoryItems, $sort_by,$order_by, $limit
	 * Output:- return inventoryItems
	 * Issue: LP-10195
	 */

	public function getSortedValue($inventoryItems, $sort_by,$order_by, $limit){
		$limit = " LIMIT ".$limit;
		if($sort_by == 'location'){
			$sort_by = 'to_from, location';
		}

		$temoQuery = " CREATE TEMPORARY TABLE IF NOT EXISTS temp_inventory_transfers (
			`id` int NOT NULL,
			`transfer_id` varchar(20),
			`date_created` datetime,
			`transfer_stage` varchar(20),
			`last_action_date` datetime,
			`transferItemsCount` int,
			`location` varchar(20),
            `to_from` varchar(20),
			PRIMARY KEY(id))";

		#Create temporary table
		$tempTable = DBConnection::clientQuery($temoQuery);

		$insertQuery = "INSERT into temp_inventory_transfers VALUES ";
		$insertQueryVal = "";
		$count = 0;
		foreach($inventoryItems as $record){
			$record['location'] = $record['location'] ? $record['location'] : 'null';
			if($count > 0) {
				$insertQueryVal .=',';
			}
			$insertQueryVal .= '('.$record['id'].',
			"'.$record['transfer_id'].'",
			"'.$record['date_created'].'",
			"'.$record['transfer_stage'].'",
			"'.$record['last_action_date'].'",
			"'.$record['transferItemsCount'].'",
			"'.$record['location'].'",
            "'.$record['to_from'].'")';
			$count++;
		}

		$truncateTempTable = DBConnection::clientQuery("truncate temp_inventory_transfers");

		$insertData = DBConnection::clientQuery($insertQuery.$insertQueryVal);
		#Select inventory serach data from temporary table
		$selectQuery = "SELECT * FROM temp_inventory_transfers ORDER BY $sort_by $order_by $limit ";
		$newInventoryItemsObj = DBConnection::clientQuery($selectQuery);

		return  MarshallingArrays::resultToArray($newInventoryItemsObj);
	}
}