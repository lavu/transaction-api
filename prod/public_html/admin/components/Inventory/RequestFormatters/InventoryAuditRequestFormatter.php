<?php

require_once __DIR__."/BasicFormatter.php";
require_once __DIR__."/../../../cp/resources/core_functions.php";
/**
 * Created by PhpStorm.
 * User: Leif
 * Date: 5/9/17
 * Time: 6:06 PM
 */
class InventoryAuditRequestFormatter extends BasicFormatter
{
	public function formatCreateAuditRequest($request, $action){
		if(!isset($action)){
			$action = API_OPTIONAL_FIELD;
		}
		//pass in id's and quantities, unit id's, get current timestamp.
		//action will be something like "reconciliation"
		$returnValue = array();
		foreach($request as $key=>$val){ //key is an array index, value is an object.
			$fields = array(
				'inventoryItemID'   => self::getPostVarAndCheckIsSet($val, 'inventoryItemID',   API_OPTIONAL_FIELD),
				'action'            => self::getPostVarAndCheckIsSet($val, 'action',            $action),
				'quantity'          => self::getPostVarAndCheckIsSet($val, 'quantity',          API_OPTIONAL_FIELD),
				'unitID'            => self::getPostVarAndCheckIsSet($val, 'unitID',            API_OPTIONAL_FIELD),
				'cost'              => self::getPostVarAndCheckIsSet($val, 'cost',              API_OPTIONAL_FIELD),
				'userID'            => self::getPostVarAndCheckIsSet($val, 'userID',            (int)sessvar("admin_loggedin")), //TODO Verify definitively that admin_loggedin is correct.
				'userNotes'         => self::getPostVarAndCheckIsSet($val, 'userNotes',         ""),
//				'datetime'          => self::getPostVarAndCheckIsSet($val, 'datetime',          gmdate("Y-m-d H:i:s")),
				'transferItemID'    => self::getPostVarAndCheckIsSet($val, 'transferItemID',    API_OPTIONAL_FIELD),
                '_deleted'          => self::getPostVarAndCheckIsSet($val, '_deleted',          API_OPTIONAL_FIELD)
			);
			$fields['datetime'] = gmdate("Y-m-d H:i:s");
			$returnValue = self::pushPostBodyFieldsToArray($fields, $returnValue);
		}
		return $returnValue;

	}

	// This is used to add audits with multiple same or different action
    // According to jira description "Needs at least the action, item, quantity, adjustment, user ID and timestamp"
    public function formatCreateAuditServiceRequest($request){
        $returnValue = array();
        foreach($request as $key=>$val){ //key is an array index, value is an object.
            $fields = array(
                'inventoryItemID'   => self::getPostVarAndCheckIsSet($val, 'inventoryItemID',   API_REQUIRED_FIELD),
                'action'            => self::getPostVarAndCheckIsSet($val, 'action',            API_REQUIRED_FIELD),
                'quantity'          => self::getPostVarAndCheckIsSet($val, 'quantity',          API_REQUIRED_FIELD),
                'unitID'            => self::getPostVarAndCheckIsSet($val, 'unitID',            API_REQUIRED_FIELD),
                'cost'              => self::getPostVarAndCheckIsSet($val, 'cost',              API_REQUIRED_FIELD),
                'userID'            => self::getPostVarAndCheckIsSet($val, 'userID',            API_REQUIRED_FIELD), //TODO Verify definitively that admin_loggedin is correct.
                'userNotes'         => self::getPostVarAndCheckIsSet($val, 'userNotes',         ""),
				'datetime'          => self::getPostVarAndCheckIsSet($val, 'datetime',          API_REQUIRED_FIELD),
                '_deleted'          => self::getPostVarAndCheckIsSet($val, '_deleted',          API_OPTIONAL_FIELD)
            );
            $returnValue = self::pushPostBodyFieldsToArray($fields, $returnValue);
        }
        return $returnValue;

    }

    // This is used to store menu items waste quantity records in inventory_audit table
    public function formatCreateWasteAuditRequest($request){
        //pass in id's and quantities, unit id's, get current timestamp.
        //action will be something like "reconciliation"
        $returnValue = array();
        foreach($request as $key=>$val){ //key is an array index, value is an object.
            $fields = array(
                'id'                        => self::getPostVarAndCheckIsSet($val, 'id',                    API_REQUIRED_FIELD),
                'wasteQuantity'             => self::getPostVarAndCheckIsSet($val, 'wasteQuantity',              API_REQUIRED_FIELD),
                'userID'                    => self::getPostVarAndCheckIsSet($val, 'userID',                API_REQUIRED_FIELD),
                'wasteReason'               => self::getPostVarAndCheckIsSet($val, 'wasteReason',           API_REQUIRED_FIELD),
                'wasteReason'               => self::getPostVarAndCheckIsSet($val, 'wasteReason',           API_REQUIRED_FIELD),
                'forcedModifierID'          => self::getPostVarAndCheckIsSet($val, 'forcedModifierID',      API_OPTIONAL_FIELD),
                'modifierID'                => self::getPostVarAndCheckIsSet($val, 'modifierID',            API_OPTIONAL_FIELD),
            );
            $returnValue = self::pushPostBodyFieldsToArray($fields, $returnValue);
        }
        return $returnValue;

    }

    public function formatCreate86CountAuditRequest($request, $action, $userNotes){
        if(!isset($action)){
            $action = API_OPTIONAL_FIELD;
        }
        //pass in id's and quantities, unit id's, get current timestamp.
        //action will be something like "reconciliation"
        $returnValue = array();
        foreach($request as $key=>$val){ //key is an array index, value is an object.
            $fields = array(
                'inventoryItemID'   => self::getPostVarAndCheckIsSet($val, 'inventoryItemID',   API_OPTIONAL_FIELD),
                'action'            => self::getPostVarAndCheckIsSet($val, 'action',            $action),
                'quantity'          => self::getPostVarAndCheckIsSet($val, 'quantity',          API_OPTIONAL_FIELD),
                'unitID'            => self::getPostVarAndCheckIsSet($val, 'unitID',            API_OPTIONAL_FIELD),
                'cost'              => self::getPostVarAndCheckIsSet($val, 'cost',              API_OPTIONAL_FIELD),
                'userID'            => self::getPostVarAndCheckIsSet($val, 'userID',            API_OPTIONAL_FIELD),
                'userNotes'         => self::getPostVarAndCheckIsSet($val, 'userNotes',         $userNotes),
                '_deleted'          => self::getPostVarAndCheckIsSet($val, '_deleted',          API_OPTIONAL_FIELD)
            );
            $fields['datetime'] = gmdate("Y-m-d H:i:s");
            $returnValue = self::pushPostBodyFieldsToArray($fields, $returnValue);
        }
        return $returnValue;
    }

    // This is used to get audits with different criteria like userID, datetime, and Item ID
    // According to jira description "Needs at least the action, item, quantity, adjustment, user ID and timestamp"
    public function formatGetAuditsServiceRequest($request){
        $returnValue = array();
        foreach($request as $key=>$val){ //key is an array index, value is an object.
            $fields = array(
                'inventoryItemID'   => self::getPostVarAndCheckIsSet($val, 'inventoryItemID',   API_OPTIONAL_FIELD),
                'userID'            => self::getPostVarAndCheckIsSet($val, 'userID',            API_OPTIONAL_FIELD), //TODO Verify definitively that admin_loggedin is correct.
                'datetime'          => self::getPostVarAndCheckIsSet($val, 'datetime',          API_OPTIONAL_FIELD),
                'action'            => self::getPostVarAndCheckIsSet($val, 'action',            API_OPTIONAL_FIELD),
            );
            $returnValue = self::pushPostBodyFieldsToArray($fields, $returnValue);
        }
        return $returnValue;

    }

    public function formatGetAuditsByActionsAndDatesRequest($request){
        // actions should be an array of strings corresponding to desired audit actions
        $returnValue = array();
        foreach($request as $key=>$val){ //key is an array index, value is an object.
            $fields = array(
				'actions'  => self::getPostVarAndCheckIsSet($val, 'actions', API_REQUIRED_FIELD),
				'startDate'  => self::getPostVarAndCheckIsSet($val, 'startDate', API_REQUIRED_FIELD),
				'endDate' => self::getPostVarAndCheckIsSet($val, 'endDate', API_REQUIRED_FIELD),
				'startTime' => self::getPostVarAndCheckIsSet($val, 'startTime', API_REQUIRED_FIELD),
				'endTime' => self::getPostVarAndCheckIsSet($val, 'endTime', API_REQUIRED_FIELD),
				'mode' => self::getPostVarAndCheckIsSet($val, 'mode', API_REQUIRED_FIELD),
            );
            $returnValue = self::pushPostBodyFieldsToArray($fields, $returnValue);
        }
        return $returnValue;

    }


}
