<?php

require_once __DIR__ . "/BasicFormatter.php";
/**
 * Created by PhpStorm.
 * User: Leif
 * Date: 7/20/17
 * Time: 2:45 PM
 */
class InventoryModifierRequestFormatter extends BasicFormatter
{
	public function formatGetAllModifiersByInventoryItemID($request){
		return $this->formatIDArray($request);
	}

	public function formatGetAllForcedModifiersByInventoryItemID($request){
		return $this->formatIDArray($request);
	}
}