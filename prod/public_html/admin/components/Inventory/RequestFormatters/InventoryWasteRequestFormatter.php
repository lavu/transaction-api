<?php
/**
 * Created by PhpStorm.
 * User: hayat
 * Date: 7/15/17
 * Time: 3:49 PM
 */

class InventoryWasteRequestFormatter extends BasicFormatter
{
    // This is used to store menu items waste quantity records in inventory_audit table
    public function formatCreateWasteAuditRequest($request){
        //pass in id's and quantities, unit id's, get current timestamp.
        //action will be something like "reconciliation"
        $returnValue = array();
        foreach($request as $key=>$val){ //key is an array index, value is an object.
            $fields = array(
                'id'                        => self::getPostVarAndCheckIsSet($val, 'id',                    API_REQUIRED_FIELD),
                'wasteQuantity'             => self::getPostVarAndCheckIsSet($val, 'wasteQuantity',         API_REQUIRED_FIELD),
                'userID'                    => self::getPostVarAndCheckIsSet($val, 'userID',                API_REQUIRED_FIELD),
                'wasteReason'               => self::getPostVarAndCheckIsSet($val, 'wasteReason',           API_REQUIRED_FIELD),
                'wasteReason'               => self::getPostVarAndCheckIsSet($val, 'wasteReason',           API_REQUIRED_FIELD),
                'forcedModifierID'          => self::getPostVarAndCheckIsSet($val, 'forcedModifierID',      API_OPTIONAL_FIELD),
                'modifierID'                => self::getPostVarAndCheckIsSet($val, 'modifierID',            API_OPTIONAL_FIELD),
            );
            $fields['datetime'] = gmdate("Y-m-d H:i:s");
            $returnValue = self::pushPostBodyFieldsToArray($fields, $returnValue);
        }
        return $returnValue;

    }
}