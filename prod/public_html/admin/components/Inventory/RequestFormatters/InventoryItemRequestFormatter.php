<?php
require_once __DIR__ . "/BasicFormatter.php";
/**
 * Created by PhpStorm.
 * User: Leif
 * Date: 4/19/17
 * Time: 9:17 AM
 */
/**
 * Requests having to do with Inventory Items as their main Inventory Object should go through this formatter.
 * Please follow the method naming convention of format{action}{Object}{optional:For{SuperAction}{SuperObject}}
 */
class InventoryItemRequestFormatter extends BasicFormatter
{
	/**
	 * @param $request
	 * @return array
	 */
	public function formatCreateInventoryItems($request){
		$returnValue = array();
		foreach($request as $key=>$val){ //key is an array index, value is an object.
			$fields = array(
				'name' =>                   self::getPostVarAndCheckIsSet($val, 'name',                     API_REQUIRED_FIELD),
				'categoryID' =>             self::getPostVarAndCheckIsSet($val, 'categoryID',               API_OPTIONAL_FIELD),
				'salesUnitID' =>            self::getPostVarAndCheckIsSet($val, 'salesUnitID',              API_OPTIONAL_FIELD),
				'storageLocation' =>        self::getPostVarAndCheckIsSet($val, 'storageLocation',          API_OPTIONAL_FIELD),
				'primaryVendorID' =>        self::getPostVarAndCheckIsSet($val, 'primaryVendorID',          API_OPTIONAL_FIELD),
				'recentPurchaseUnitID' =>   self::getPostVarAndCheckIsSet($val, 'recentPurchaseUnitID',     API_OPTIONAL_FIELD),
				'recentPurchaseCost' =>     self::getPostVarAndCheckIsSet($val, 'recentPurchaseCost',       0),
				'quantityOnHand' =>         self::getPostVarAndCheckIsSet($val, 'quantityOnHand',           0),
				'lowQuantity' =>            self::getPostVarAndCheckIsSet($val, 'lowQuantity',              0),
				'reOrderQuantity' =>        self::getPostVarAndCheckIsSet($val, 'reOrderQuantity',          0),
				'criticalItem' =>           self::getPostVarAndCheckIsSet($val, 'criticalItem',             0),
				'_deleted' =>               self::getPostVarAndCheckIsSet($val, '_deleted',                 API_OPTIONAL_FIELD),
			);
			$returnValue = self::pushPostBodyFieldsToArray($fields, $returnValue);
		}
		return $returnValue;
	}

	/**
	 * @param $request
	 * @return array
	 */
	public function formatUpdateInventoryItems($request){
		$returnValue = array();
		foreach($request as $key=>$val){ //key is an array index, value is an object.
			$fields = array(
				'id' =>                     self::getPostVarAndCheckIsSet($val, 'id',                       API_REQUIRED_FIELD),
				'name' =>                   self::getPostVarAndCheckIsSet($val, 'name',                     API_OPTIONAL_FIELD),
				'categoryID' =>             self::getPostVarAndCheckIsSet($val, 'categoryID',               API_OPTIONAL_FIELD),
				'salesUnitID' =>            self::getPostVarAndCheckIsSet($val, 'salesUnitID',              API_OPTIONAL_FIELD),
				'storageLocation' =>        self::getPostVarAndCheckIsSet($val, 'storageLocation',          API_OPTIONAL_FIELD),
				'primaryVendorID' =>        self::getPostVarAndCheckIsSet($val, 'primaryVendorID',          API_OPTIONAL_FIELD),
				'recentPurchaseUnitID' =>   self::getPostVarAndCheckIsSet($val, 'recentPurchaseUnitID',     API_OPTIONAL_FIELD),
				'recentPurchaseCost' =>     self::getPostVarAndCheckIsSet($val, 'recentPurchaseCost',       API_OPTIONAL_FIELD),
				'recentPurchaseQuantity'=>  self::getPostVarAndCheckIsSet($val, 'recentPurchaseQuantity',   API_OPTIONAL_FIELD),
				//'quantityOnHand' =>         self::getPostVarAndCheckIsSet($val, 'quantityOnHand',           API_OPTIONAL_FIELD),
				'lowQuantity' =>            self::getPostVarAndCheckIsSet($val, 'lowQuantity',              API_OPTIONAL_FIELD),
				'reOrderQuantity' =>        self::getPostVarAndCheckIsSet($val, 'reOrderQuantity',          API_OPTIONAL_FIELD),
				'criticalItem' =>           self::getPostVarAndCheckIsSet($val, 'criticalItem',             API_OPTIONAL_FIELD),
				'_deleted' =>               self::getPostVarAndCheckIsSet($val, '_deleted',                 API_OPTIONAL_FIELD),
			);
			$returnValue = self::pushPostBodyFieldsToArray($fields, $returnValue);
		}
		return $returnValue;
	}
    /**
     * @param $request
     * @return array
     */
    public function formatReconciliation($request){
        $returnValue = array();
        foreach($request as $key=>$val){ //key is an array index, value is an object.
            $fields = array(
                'id' =>                     self::getPostVarAndCheckIsSet($val, 'id',                       API_REQUIRED_FIELD),
                'name' =>                   self::getPostVarAndCheckIsSet($val, 'name',                     API_OPTIONAL_FIELD),
                'categoryID' =>             self::getPostVarAndCheckIsSet($val, 'categoryID',               API_OPTIONAL_FIELD),
                'salesUnitID' =>            self::getPostVarAndCheckIsSet($val, 'salesUnitID',              API_OPTIONAL_FIELD),
                'storageLocation' =>        self::getPostVarAndCheckIsSet($val, 'storageLocation',          API_OPTIONAL_FIELD),
                //'primaryVendorID' =>        self::getPostVarAndCheckIsSet($val, 'primaryVendorID',          API_OPTIONAL_FIELD),
                'recentPurchaseUnitID' =>   self::getPostVarAndCheckIsSet($val, 'unitID',     API_OPTIONAL_FIELD),
                'recentPurchaseCost' =>     self::getPostVarAndCheckIsSet($val, 'recentPurchaseCost',       API_OPTIONAL_FIELD),
                'recentPurchaseQuantity'=>  self::getPostVarAndCheckIsSet($val, 'recentPurchaseQuantity',    API_OPTIONAL_FIELD),
                'quantityOnHand' =>         self::getPostVarAndCheckIsSet($val, 'quantityOnHand',           API_OPTIONAL_FIELD),
                //'lowQuantity' =>            self::getPostVarAndCheckIsSet($val, 'lowQuantity',              API_OPTIONAL_FIELD),
               // 'reOrderQuantity' =>        self::getPostVarAndCheckIsSet($val, 'reOrderQuantity',          API_OPTIONAL_FIELD),
                'criticalItem' =>           self::getPostVarAndCheckIsSet($val, 'criticalItem',             API_OPTIONAL_FIELD),
                '_deleted' =>               self::getPostVarAndCheckIsSet($val, '_deleted',                 API_OPTIONAL_FIELD),
            );
            $returnValue = self::pushPostBodyFieldsToArray($fields, $returnValue);
        }
        return $returnValue;
    }

	/**
	 * @param $request
	 * @return array
	 */
	public function formatCopyInventoryItems($request){
		return $this->formatIDArray($request);
	}

	/**
	 * @param $request
	 * @return array
	 */
	public function formatDuplicateInventoryItems($request){
		return $this->formatUpdateInventoryItems($request);
	}

	public function formatAddQuantityToInventoryItems($request){
		return $this->formatIDAndQuantityArray($request);
	}

    /**
     * @param $request
     * @return array
     */
    public function formatQuantityAdjustmentWithUnit($request){
        $returnValue = array();
        $returnValue["Items"] = array();
        foreach($request as $key=>$val){
            $fields = array(
                'id'        => self::getPostVarAndCheckIsSet($val, 'inventoryItemID', API_REQUIRED_FIELD),
                'quantity'  => self::getPostVarAndCheckIsSet($val, 'quantity', API_REQUIRED_FIELD),
                'unitID'    => self::getPostVarAndCheckIsSet($val, 'unitID', API_REQUIRED_FIELD)
            );
            $returnValue[] = $fields;
        }
        return $returnValue;
    }
    /**
     * @param $request
     * @return array
     */
    public function formatReceiveWithoutPO($request){
        $returnValue = array();
        foreach($request as $key=>$val){
            $fields = array(
                'id'        => self::getPostVarAndCheckIsSet($val, 'inventoryItemID', API_REQUIRED_FIELD),
                'quantity'  => self::getPostVarAndCheckIsSet($val, 'quantity', API_REQUIRED_FIELD),
                'unitID'    => self::getPostVarAndCheckIsSet($val, 'unitID', API_REQUIRED_FIELD),
                'costPerUnit' => self::getPostVarAndCheckIsSet($val, 'cost', API_REQUIRED_FIELD),
            );
            $returnValue[] = $fields;
        }
        return $returnValue;
    }
	public function formatWasteInventoryItems($request){
		return self::formatQuantityAdjustmentWithUnit($request);
	}

	public function formatArchiveInventoryItems($request){
		return $this->formatIDArray($request);
	}

	public function formatGetInventoryItemsByID($request){
		return $this->formatIDArray($request);
	}
}
