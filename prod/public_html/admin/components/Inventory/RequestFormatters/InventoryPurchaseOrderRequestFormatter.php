<?php
require_once __DIR__."/BasicFormatter.php";
require_once __DIR__."/InventoryPurchaseOrderItemRequestFormatter.php";

/**
 * Created by PhpStorm.
 * User: Leif
 * Date: 4/19/17
 * Time: 9:17 AM
 */
/**
 * Requests that have Purchase Orders as their main Inventory object should go through this formatter.
 * Please follow the method naming convention of format{action}{Object}{optional:For{SuperAction}{SuperObject}}
 */
class InventoryPurchaseOrderRequestFormatter extends BasicFormatter
{

	public function formatCopyPurchaseOrder($request){
		return $this->formatIDArray($request);
	}

	public function formatUpdatePurchaseOrderStatuses($request){
		return $this->utilityCreateOrReceivePurchaseOrder($request, 'id', array());
	}

	public function formatCreatePurchaseOrder($request){
		return $this->utilityCreateOrReceivePurchaseOrder($request, 'vendorID', null, 'formatCreatePurchaseOrderItems');
	}

	public function formatReceivePurchaseOrder($request){
		return $this->utilityCreateOrReceivePurchaseOrder($request, 'id', array(), 'formatUpdatePurchaseOrderItems');
	}

	private function utilityCreateOrReceivePurchaseOrder($request, $idField, $noPOIsField, $createOrUpdatePurchaseOrderFunction=null){ //should fail for creation, and not cause issues if it's for receiving.
		$returnValue = array();
		$purchaseOrderItemFormatter =  new InventoryPurchaseOrderItemRequestFormatter();
		foreach($request as $key=>$val){ //key is an array index, value is an object.
			$fields = array(
				$idField =>                 self::getPostVarAndCheckIsSet($val,  $idField,            API_REQUIRED_FIELD),
				'invoiceNumber' =>          self::getPostVarAndCheckIsSet($val, 'invoiceNumber',      API_OPTIONAL_FIELD),
				'orderDate'     =>          self::getPostVarAndCheckIsSet($val, 'orderDate',          API_OPTIONAL_FIELD),
				'sent'          =>          self::getPostVarAndCheckIsSet($val, 'sent',               API_OPTIONAL_FIELD),
				'accepted'      =>          self::getPostVarAndCheckIsSet($val, 'accepted',           API_OPTIONAL_FIELD),
				'purchaseOrderItems' =>     self::getPostVarAndCheckIsSet($val, 'purchaseOrderItems', API_REQUIRED_FIELD),
			);

			if($createOrUpdatePurchaseOrderFunction === 'formatUpdatePurchaseOrderItems'){
				$poReceivedItems = $purchaseOrderItemFormatter->itemsQuantityGreaterThan0($fields['purchaseOrderItems']);
				if(count($fields['purchaseOrderItems']) > 0 && $poReceivedItems)
				{
					$fields['receiveDate'] = gmdate("Y-m-d H:i:s");
				}
			}
			if(isset($fields['purchaseOrderItems']) && is_array($fields['purchaseOrderItems']) && isset($createOrUpdatePurchaseOrderFunction))
			{
				$fields['purchaseOrderItems'] = $purchaseOrderItemFormatter->$createOrUpdatePurchaseOrderFunction($fields['purchaseOrderItems']);
			}else{
				$fields['purchaseOrderItems'] = $noPOIsField;
			}
			$returnValue = self::pushPostBodyFieldsToArray($fields, $returnValue);
		}
		return $returnValue;
	}

}