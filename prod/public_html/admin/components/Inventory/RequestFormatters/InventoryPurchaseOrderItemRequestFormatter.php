<?php
require_once __DIR__."/BasicFormatter.php";
/**
 * Created by PhpStorm.
 * User: Leif
 * Date: 4/24/17
 * Time: 4:05 PM
 */
class InventoryPurchaseOrderItemRequestFormatter extends BasicFormatter
{
	public function formatCreatePurchaseOrderItems($itemList){
		$returnValue = array();
		foreach($itemList as $key=>$val){ //key is an array index, value is an object.
			$fields = array(
				'purchaseOrderID'   => self::getPostVarAndCheckIsSet($val, 'purchaseOrderID',     API_OPTIONAL_FIELD),
				'vendorItemID'      => self::getPostVarAndCheckIsSet($val, 'vendorItemID',        API_OPTIONAL_FIELD),
				'purchaseUnitID'    => self::getPostVarAndCheckIsSet($val, 'purchaseUnitID',      API_OPTIONAL_FIELD),
				'quantityOrdered'   => self::getPostVarAndCheckIsSet($val, 'quantityOrdered',     API_OPTIONAL_FIELD),
				'purchaseUnitCost'  => self::getPostVarAndCheckIsSet($val, 'purchaseUnitCost',    API_OPTIONAL_FIELD),
				'quantityReceived'  => self::getPostVarAndCheckIsSet($val, 'quantityReceived',    API_OPTIONAL_FIELD),
				'SKU'               => self::getPostVarAndCheckIsSet($val, 'SKU',                 API_OPTIONAL_FIELD),
				'_deleted'          => self::getPostVarAndCheckIsSet($val, '_deleted',            API_OPTIONAL_FIELD),
			);
			$returnValue = self::pushPostBodyFieldsToArray($fields, $returnValue);
		}
		return $returnValue;
	}

	public function formatUpdatePurchaseOrderItems($itemList){
		$returnValue = array();
		foreach($itemList as $key=>$val){ //key is an array index, value is an object.
			$fields = array(
				'id'                => self::getPostVarAndCheckIsSet($val, 'id',                  API_REQUIRED_FIELD),
				'purchaseOrderID'   => self::getPostVarAndCheckIsSet($val, 'purchaseOrderID',     API_OPTIONAL_FIELD),
				'vendorItemID'      => self::getPostVarAndCheckIsSet($val, 'vendorItemID',        API_OPTIONAL_FIELD),
				'inventoryItemID'   => self::getPostVarAndCheckIsSet($val, 'inventoryItemID',        API_OPTIONAL_FIELD),
				'purchaseUnitID'    => self::getPostVarAndCheckIsSet($val, 'purchaseUnitID',      API_OPTIONAL_FIELD),
				'quantityOrdered'   => self::getPostVarAndCheckIsSet($val, 'quantityOrdered',     API_OPTIONAL_FIELD),
				'purchaseUnitCost'  => self::getPostVarAndCheckIsSet($val, 'purchaseUnitCost',    API_OPTIONAL_FIELD),
				'quantityReceived'  => self::getPostVarAndCheckIsSet($val, 'quantityReceived',    API_OPTIONAL_FIELD),
				'SKU'               => self::getPostVarAndCheckIsSet($val, 'SKU',                 API_OPTIONAL_FIELD),
				'_deleted'          => self::getPostVarAndCheckIsSet($val, '_deleted',            API_OPTIONAL_FIELD),
			);
			$returnValue = self::pushPostBodyFieldsToArray($fields, $returnValue);
		}
		return $returnValue;
	}

	public function itemsQuantityGreaterThan0($itemList){

		foreach($itemList as $i=>$val){ //key is an array index, value is an object.
			if($val['quantityReceived'] > 0){
				return true;
			}
		}
		return false;
	}
}