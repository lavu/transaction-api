<?php
require_once __DIR__."/BasicFormatter.php";
/**
 * Created by PhpStorm.
 * User: Hayat
 * Date: 09/06/17
 * Time: 10:18 PM
 */
/**
 * Requests having to do with Vendors as their main Inventory Object should go through this formatter.
 * Please follow the method naming convention of format{action}{Object}{optional:For{SuperAction}{SuperObject}}
 */
class Inventory86CountRequestFormatter extends BasicFormatter
{
	public function formatCreate86CountMenuItems($request){
		$returnValue = array();
		foreach($request as $key=>$val){ //key is an array index, value is an object.
			$fields = array(
				'menuItemID'            => self::getPostVarAndCheckIsSet($val, 'menuItemID',                    API_REQUIRED_FIELD),
				'inventoryItemID'       => self::getPostVarAndCheckIsSet($val, 'inventoryItemID',               API_REQUIRED_FIELD),
				'quantityUsed'          => self::getPostVarAndCheckIsSet($val, 'quantityUsed',                  API_REQUIRED_FIELD),
				'criticalItem'          => self::getPostVarAndCheckIsSet($val, 'criticalItem',                  API_REQUIRED_FIELD),
				'unitID'                => self::getPostVarAndCheckIsSet($val, 'unitID',                        API_REQUIRED_FIELD),
				'reservation'           => self::getPostVarAndCheckIsSet($val, 'reservation',                   API_REQUIRED_FIELD),
				'_deleted'              => self::getPostVarAndCheckIsSet($val, '_deleted',                      API_OPTIONAL_FIELD),
			);
			$returnValue = self::pushPostBodyFieldsToArray($fields, $returnValue);
		}
		return $returnValue;
	}

    public function formatCreate86CountModifiers($request){
        $returnValue = array();
        foreach($request as $key=>$val){ //key is an array index, value is an object.
            $fields = array(
                'modifierID'            => self::getPostVarAndCheckIsSet($val, 'modifierID',                    API_REQUIRED_FIELD),
                'inventoryItemID'       => self::getPostVarAndCheckIsSet($val, 'inventoryItemID',               API_REQUIRED_FIELD),
                'quantityUsed'          => self::getPostVarAndCheckIsSet($val, 'quantityUsed',                  API_REQUIRED_FIELD),
                'criticalItem'          => self::getPostVarAndCheckIsSet($val, 'criticalItem',                  API_REQUIRED_FIELD),
                'unitID'                => self::getPostVarAndCheckIsSet($val, 'unitID',                        API_REQUIRED_FIELD),
                'reservation'           => self::getPostVarAndCheckIsSet($val, 'reservation',                   API_REQUIRED_FIELD),
                '_deleted'              => self::getPostVarAndCheckIsSet($val, '_deleted',                      API_OPTIONAL_FIELD),
            );
            $returnValue = self::pushPostBodyFieldsToArray($fields, $returnValue);
        }
        return $returnValue;
    }

    public function formatCreate86CountForcedModifiers($request){
        $returnValue = array();
        foreach($request as $key=>$val){ //key is an array index, value is an object.
            $fields = array(
                'forced_modifierID'     => self::getPostVarAndCheckIsSet($val, 'forced_modifierID',             API_REQUIRED_FIELD),
                'inventoryItemID'       => self::getPostVarAndCheckIsSet($val, 'inventoryItemID',               API_REQUIRED_FIELD),
                'quantityUsed'          => self::getPostVarAndCheckIsSet($val, 'quantityUsed',                  API_REQUIRED_FIELD),
                'criticalItem'          => self::getPostVarAndCheckIsSet($val, 'criticalItem',                  API_REQUIRED_FIELD),
                'unitID'                => self::getPostVarAndCheckIsSet($val, 'unitID',                        API_REQUIRED_FIELD),
                'reservation'           => self::getPostVarAndCheckIsSet($val, 'reservation',                   API_REQUIRED_FIELD),
                '_deleted'              => self::getPostVarAndCheckIsSet($val, '_deleted',                      API_OPTIONAL_FIELD),
            );
            $returnValue = self::pushPostBodyFieldsToArray($fields, $returnValue);
        }
        return $returnValue;
    }

    public function formatUpdate86CountMenuItems($request){
        $returnValue = array();
        foreach($request as $key=>$val){ //key is an array index, value is an object.
            $fields = array(
                'menuItemID'            => self::getPostVarAndCheckIsSet($val, 'menuItemID',                    API_REQUIRED_FIELD),
                'inventoryItemID'       => self::getPostVarAndCheckIsSet($val, 'inventoryItemID',               API_REQUIRED_FIELD),
                'quantityUsed'          => self::getPostVarAndCheckIsSet($val, 'quantityUsed',                  API_REQUIRED_FIELD),
                'criticalItem'          => self::getPostVarAndCheckIsSet($val, 'criticalItem',                  API_REQUIRED_FIELD),
                'unitID'                => self::getPostVarAndCheckIsSet($val, 'unitID',                        API_REQUIRED_FIELD),
                'reservation'           => self::getPostVarAndCheckIsSet($val, 'reservation',                   API_REQUIRED_FIELD),
                '_deleted'              => self::getPostVarAndCheckIsSet($val, '_deleted',                      API_OPTIONAL_FIELD),
            );
            $returnValue = self::pushPostBodyFieldsToArray($fields, $returnValue);
        }
        return $returnValue;
    }

    public function formatUpdate86CountModifiers($request){
        $returnValue = array();
        foreach($request as $key=>$val){ //key is an array index, value is an object.
            $fields = array(
                'modifierID'            => self::getPostVarAndCheckIsSet($val, 'modifierID',                    API_REQUIRED_FIELD),
                'inventoryItemID'       => self::getPostVarAndCheckIsSet($val, 'inventoryItemID',               API_REQUIRED_FIELD),
                'quantityUsed'          => self::getPostVarAndCheckIsSet($val, 'quantityUsed',                  API_REQUIRED_FIELD),
                'criticalItem'          => self::getPostVarAndCheckIsSet($val, 'criticalItem',                  API_REQUIRED_FIELD),
                'unitID'                => self::getPostVarAndCheckIsSet($val, 'unitID',                        API_REQUIRED_FIELD),
                'reservation'           => self::getPostVarAndCheckIsSet($val, 'reservation',                   API_REQUIRED_FIELD),
                '_deleted'              => self::getPostVarAndCheckIsSet($val, '_deleted',                      API_OPTIONAL_FIELD),
            );
            $returnValue = self::pushPostBodyFieldsToArray($fields, $returnValue);
        }
        return $returnValue;
    }

    public function formatUpdate86CountForcedModifiers($request){
        $returnValue = array();
        foreach($request as $key=>$val){ //key is an array index, value is an object.
            $fields = array(
                'forced_modifierID'     => self::getPostVarAndCheckIsSet($val, 'forced_modifierID',             API_REQUIRED_FIELD),
                'inventoryItemID'       => self::getPostVarAndCheckIsSet($val, 'inventoryItemID',               API_REQUIRED_FIELD),
                'quantityUsed'          => self::getPostVarAndCheckIsSet($val, 'quantityUsed',                  API_REQUIRED_FIELD),
                'criticalItem'          => self::getPostVarAndCheckIsSet($val, 'criticalItem',                  API_REQUIRED_FIELD),
                'unitID'                => self::getPostVarAndCheckIsSet($val, 'unitID',                        API_REQUIRED_FIELD),
                'reservation'           => self::getPostVarAndCheckIsSet($val, 'reservation',                   API_REQUIRED_FIELD),
                '_deleted'              => self::getPostVarAndCheckIsSet($val, '_deleted',                      API_OPTIONAL_FIELD),
            );
            $returnValue = self::pushPostBodyFieldsToArray($fields, $returnValue);
        }
        return $returnValue;
    }

    public function formatMenuItemsStatus($request){
        $returnValue = array();
        foreach($request as $key=>$val){ //key is an array index, value is an object.
            $fields = array(
                'id'            => self::getPostVarAndCheckIsSet($val, 'id',          API_REQUIRED_FIELD),
                'quantity'      => self::getPostVarAndCheckIsSet($val, 'quantity',              1),
            );
            $returnValue = self::pushPostBodyFieldsToArray($fields, $returnValue);
        }
        return $returnValue;
    }

    public function formatModifiersStatus($request){
        $returnValue = array();
        foreach($request as $key=>$val){ //key is an array index, value is an object.
            $fields = array(
                'id'            => self::getPostVarAndCheckIsSet($val, 'id',          API_OPTIONAL_FIELD),
                'quantity'      => self::getPostVarAndCheckIsSet($val, 'quantity',              1),
            );
            $returnValue = self::pushPostBodyFieldsToArray($fields, $returnValue);
        }
        return $returnValue;
    }

    public function formatForcedModifiersStatus($request){
        $returnValue = array();
        foreach($request as $key=>$val){ //key is an array index, value is an object.
            $fields = array(
                'id'            => self::getPostVarAndCheckIsSet($val, 'id',          API_OPTIONAL_FIELD),
                'quantity'      => self::getPostVarAndCheckIsSet($val, 'quantity',              1),
            );
            $returnValue = self::pushPostBodyFieldsToArray($fields, $returnValue);
        }
        return $returnValue;
    }

    public function formatDeduct86CountQuantity($request){
        $returnValue = array();
        foreach($request as $key=>$val){ //key is an array index, value is an object.
            $fields = array(
                'id'                => self::getPostVarAndCheckIsSet($val, 'id',                        API_REQUIRED_FIELD),
                'inventoryItemID'   => self::getPostVarAndCheckIsSet($val, 'inventoryItemID',           API_REQUIRED_FIELD),
                'quantity'          => self::getPostVarAndCheckIsSet($val, 'quantity',                  API_REQUIRED_FIELD),

            );
            $returnValue = self::pushPostBodyFieldsToArray($fields, $returnValue);
        }
        return $returnValue;
    }

    public function formatIds($request){
        $returnValue = array();
        foreach($request as $key=>$val){ //key is an array index, value is an object.
            $fields = array(
                'id'            => self::getPostVarAndCheckIsSet($val, 'id',          API_REQUIRED_FIELD),
            );
            $returnValue = self::pushPostBodyFieldsToArray($fields, $returnValue);
        }
        return $returnValue;
    }
}