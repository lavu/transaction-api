<?php
require_once __DIR__."/BasicFormatter.php";
/**
 * Created by PhpStorm.
 * User: Leif
 * Date: 4/19/17
 * Time: 9:18 AM
 */
/**
 * Requests having to do with Vendor Items as their main Inventory Object should go through this formatter.
 * Please follow the method naming convention of format{action}{Object}{optional:For{SuperAction}{SuperObject}}
 */
class InventoryVendorItemRequestFormatter extends BasicFormatter
{

	public function isLinkingItem($request){
		$returnValue = array();
		foreach($request as $key=>$val){
			if(isset($val['linkedItemID'])){
				$fields = array(
				'inventoryItemID' =>    self::getPostVarAndCheckIsSet($val, 'linkedItemID',    API_OPTIONAL_FIELD),
				'vendorID' =>           self::getPostVarAndCheckIsSet($val, 'vendorID',  API_OPTIONAL_FIELD),
				'SKU' =>                self::getPostVarAndCheckIsSet($val, 'SKU',                API_OPTIONAL_FIELD),
				'barcode' =>            self::getPostVarAndCheckIsSet($val, 'barcode',            API_OPTIONAL_FIELD),
				'BIN' =>                self::getPostVarAndCheckIsSet($val, 'BIN',                API_OPTIONAL_FIELD),
				'serialNumber' =>       self::getPostVarAndCheckIsSet($val, 'serialNumber',       API_OPTIONAL_FIELD),
				'_deleted' =>           self::getPostVarAndCheckIsSet($val, '_deleted',           API_OPTIONAL_FIELD),
				);
				return self::pushPostBodyFieldsToArray($fields, $returnValue);
			}else{
				return false;
			}
		}
	}

	/**
	 *  Vendor Item Table Columns:
	 *
	 *
	 */
	public function formatCreateVendorItems($request){
		$returnValue = array();
		foreach($request as $key=>$val){ //key is an array index, value is an object.
			$fields = array(
				'inventoryItemID' =>    self::getPostVarAndCheckIsSet($val, 'inventoryItemID',    API_OPTIONAL_FIELD),
				'vendorID' =>           self::getPostVarAndCheckIsSet($val, 'primary_vendor_id',  API_OPTIONAL_FIELD),
				'SKU' =>                self::getPostVarAndCheckIsSet($val, 'SKU',                API_OPTIONAL_FIELD),
				'barcode' =>            self::getPostVarAndCheckIsSet($val, 'barcode',            API_OPTIONAL_FIELD),
				'BIN' =>                self::getPostVarAndCheckIsSet($val, 'BIN',                API_OPTIONAL_FIELD),
				'serialNumber' =>       self::getPostVarAndCheckIsSet($val, 'serialNumber',       API_OPTIONAL_FIELD),
				'_deleted' =>           self::getPostVarAndCheckIsSet($val, '_deleted',           API_OPTIONAL_FIELD),
			);
			$returnValue = self::pushPostBodyFieldsToArray($fields, $returnValue);
		}
		return $returnValue;
	}

	/**
	 *  Vendor Item Table Columns:
	 *
	 *
	 */
	public function formatUpdateVendorItems($request){
		$returnValue = array();
		foreach($request as $key=>$val){ //key is an array index, value is an object.
			$fields = array(
				'id' =>                 self::getPostVarAndCheckIsSet($val, 'id',                 API_REQUIRED_FIELD),
				'inventoryItemID' =>    self::getPostVarAndCheckIsSet($val, 'inventoryItemID',    API_OPTIONAL_FIELD),
				'vendorID' =>           self::getPostVarAndCheckIsSet($val, 'primary_vendor_id',  API_OPTIONAL_FIELD),
				'SKU' =>                self::getPostVarAndCheckIsSet($val, 'SKU',                API_OPTIONAL_FIELD),
				'barcode' =>            self::getPostVarAndCheckIsSet($val, 'barcode',            API_OPTIONAL_FIELD),
				'BIN' =>                self::getPostVarAndCheckIsSet($val, 'BIN',                API_OPTIONAL_FIELD),
				'serialNumber' =>       self::getPostVarAndCheckIsSet($val, 'serialNumber',       API_OPTIONAL_FIELD),
				'_deleted' =>           self::getPostVarAndCheckIsSet($val, '_deleted',           API_OPTIONAL_FIELD),
			);
			$returnValue = self::pushPostBodyFieldsToArray($fields, $returnValue);
		}
		return $returnValue;
	}


	public function formatLinkVendorItemsToInventoryItems($request){
		$returnValue = array();
		foreach($request as $key=>$val){ //key is an array index, value is an object.
			$fields = array(
				'inventoryItemID'   =>    self::getPostVarAndCheckIsSet($val, 'inventoryItemID',    API_REQUIRED_FIELD),
				'vendorID'      =>    self::getPostVarAndCheckIsSet($val, 'vendorID',       API_REQUIRED_FIELD),
                'SKU'      =>    self::getPostVarAndCheckIsSet($val, 'SKU',       API_OPTIONAL_FIELD),
                'BIN'      =>    self::getPostVarAndCheckIsSet($val, 'BIN',       API_OPTIONAL_FIELD),
                'barcode'      =>    self::getPostVarAndCheckIsSet($val, 'barcode',       API_OPTIONAL_FIELD),
			);
			$returnValue = self::pushPostBodyFieldsToArray($fields, $returnValue);
		}
		return $returnValue;
	}
}