<?php
require_once __DIR__."/RequestFormatterUtil.php";
/**
 * Created by PhpStorm.
 * User: Leif
 * Date: 4/18/17
 * Time: 4:24 PM
 */
/**
 * Requests that don't have any definitive Inventory Object type, and are basic formatting such as an array if id's
 * should go through this formatter.
 * Please follow the method naming convention of format{action}{Object}{optional:For{SuperAction}{SuperObject}}
 */
class BasicFormatter extends RequestFormatterUtil
{
	/**
	 * @param $request
	 * @return array
	 */
	public function formatIDArray($request){
		$returnValue = array();
		foreach($request as $key=>$val){
			$fields = array(
				'id' => self::getPostVarAndCheckIsSet($val, 'id', API_REQUIRED_FIELD)
			);
			$returnValue = self::pushPostBodyFieldsToArray($fields, $returnValue);
		}
		return $returnValue;
	}

	/**
	 * @param $request
	 * @return array
	 */
	public function formatIDAndQuantityArray($request){
		$returnValue = array();
		foreach($request as $key=>$val){
			$fields = array(
				'id'        => self::getPostVarAndCheckIsSet($val, 'id', API_REQUIRED_FIELD),
				'quantity'  => self::getPostVarAndCheckIsSet($val, 'quantity', API_REQUIRED_FIELD)
			);
			$returnValue = self::pushPostBodyFieldsToArray($fields, $returnValue);
		}
		return $returnValue;
	}

	public function formattedSuccessResponse($queryIDs){
		//TODO: if count(queryIDs) === 0 then... fail?
		$id = min($queryIDs);
		$count = count($queryIDs);
		$result = array();
		$result['DataResponse']['DataResponse']['InsertID'] = $id; //TODO: This isn't right. should be able to handle multiple tables.
		$result['DataResponse']['DataResponse']['RowsUpdated'] = $count;
		$result['RequestID'] = 0;
		$result['Status'] = "Success";
		return $result; //TODO respond with formatted response.
	}


	//TODO: put this into one of the request formatters (util?) sets the idType id value.
	public function appendIDsToFutureRequests($request,$response, $idType){
		$firstID = $response['DataResponse']['DataResponse']['InsertID'];
		$idCount = $response['DataResponse']['DataResponse']['RowsUpdated'];
		for($i=0;$i<$idCount;$i++){
			$request[$i][$idType] = $firstID+$i;
		}
		return $request;
	}

	/**Failure functions*/
	public function formattedFailureResponse($queryIDs, $response){
		return json_encode(array("Status"=>"Failure")); //TODO response with formatted response.
	}

	//TODO: Place where it belongs (probably compiler util).
	public function rollbackOnFailElseContinue($queryIDs, $response){
		if(self::lavuAPIResponseIsFailure($response)){
			self::requestRollBackOnIDs($queryIDs);
			return true;
		}
		return false;
	}

	//TODO: Place where it belongs (probably compiler util).
	public function lavuAPIResponseIsFailure($response){
		return ($response['Status'] !== "Success");
	}

	public function addRequestIDsToArray($queryIDs, $response){
		//TODO Ensure this works
		$firstID = $response['DataResponse']['DataResponse']['InsertID'];
		$idCount = $response['DataResponse']['DataResponse']['RowsUpdated'];
		if(!isset($queryIDs)){$queryIDs = array();}
		for($i = 0; $i < $idCount; $i++)
		{
			$queryIDs[] = ($i+$firstID);
		}
		return $queryIDs; //TODO: place in compiler util.
	}

	//TODO: Figure out where this belongs. probably compiler util. Takes id's and performs API request to roll back
	//database queries for those IDs.
	public function requestRollBackOnIDs($ids){
		//TODO
	}

	/**
	 * This function is used to generate csv file and download into local
	 * @param $data string comma separated records of the csv file
	 * @param $filename string csv filename
	 * @return array
	 */
	public function exportCsv($data, $filename) {
		header('Content-Type: text/csv; charset=utf-8');
		header('Content-Disposition: attachment; filename='.$filename);
		$efl = fopen('php://output', 'w');
		fwrite($efl, $data);
		fclose($efl);
		exit;
	}

}