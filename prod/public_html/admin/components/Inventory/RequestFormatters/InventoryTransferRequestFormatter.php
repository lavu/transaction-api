<?php
require_once __DIR__."/BasicFormatter.php";
require_once __DIR__."/InventoryTransferItemRequestFormatter.php";
/**
 * Created by PhpStorm.
 * User: Leif
 * Date: 4/19/17
 * Time: 1:55 PM
 */
class InventoryTransferRequestFormatter extends BasicFormatter
{
	/**
	 * @param $request
	 * @return array
	 */
	public function formatCreateTransfers($request)
	{
		return $this->formatCreateOrUpdateTransfers($request, null, false, 'formatCreateTransferItems');
	}
	public function formatUpdateTransfer($request){
		return $this->formatCreateOrUpdateTransfers($request, array(), true, 'formatUpdateTransferItems');
	}

	public function formatCreateOrUpdateTransfers($request, $noTransferItemsField, $includeID, $createOrReceiveItemsFunction){
		$returnValue = array();
		$transferItemRequestFormatter =  new InventoryTransferItemRequestFormatter();
		foreach($request as $key=>$val){ //key is an array index, value is an object.
			$fields = array(
				'destinationRestaurantID'   => self::getPostVarAndCheckIsSet($val, 'destinationRestaurantID',   API_REQUIRED_FIELD),
				'originRestaurantID'        => self::getPostVarAndCheckIsSet($val, 'originRestaurantID',        API_REQUIRED_FIELD),
				'transferNumber'            => self::getPostVarAndCheckIsSet($val, 'transferNumber',            API_REQUIRED_FIELD),
				'orderDate'                 => self::getPostVarAndCheckIsSet($val, 'orderDate',                 API_OPTIONAL_FIELD),
				'sent'                      => self::getPostVarAndCheckIsSet($val, 'sent',                      0),
				'received'                  => self::getPostVarAndCheckIsSet($val, 'received',                  0),
				'accepted'                  => self::getPostVarAndCheckIsSet($val, 'accepted',                  -1),
				'transferItems'             => self::getPostVarAndCheckIsSet($val, 'transferItems',             API_REQUIRED_FIELD),
				'_deleted'                  => self::getPostVarAndCheckIsSet($val, '_deleted',                  API_OPTIONAL_FIELD),
			);
			$fields = $this->setTransferStatusTimestamps($fields, $val);
			$fields['lastAction'] = gmdate("Y-m-d H:i:s");
			if($includeID === true){
				$fields['id'] = self::getPostVarAndCheckIsSet($val, 'id',   API_REQUIRED_FIELD);
			}
			if(isset($fields['transferItems']) && is_array($fields['transferItems']))
			{
				$fields['transferItems'] = $transferItemRequestFormatter->$createOrReceiveItemsFunction($fields['transferItems']);
			}else{
				$fields['transferItems'] = $noTransferItemsField;
			}
			$returnValue = self::pushPostBodyFieldsToArray($fields, $returnValue);
		}
		return $returnValue;
	}

	private function setTransferStatusTimestamps($fields, $val){
		$sent = $fields['sent'];
		$received = $fields['received'];
		$accepted = $fields['accepted'];
		if($accepted == 1 && $sent == 1 && $received == 1 && !isset($val['received_ts'])){
			$fields['received_ts'] = gmdate("Y-m-d H:i:s");
		}else if($accepted == 1 && $sent == 1  && !isset($val['shipped_ts'])){
			$fields['shipped_ts'] = gmdate("Y-m-d H:i:s");
		}else if($accepted == 1  && !isset($val['accepted_ts'])){
			$fields['accepted_ts'] = gmdate("Y-m-d H:i:s");
		}
		return $fields;
	}

	public function formatGetTransfersByID($request){
		return $this->formatIDArray($request);
	}
	public function formatCopyTransfers($request){
		return $this->formatIDArray($request);
	}


	public function formatImportTransfers($request){
		$returnValue = array();
		foreach($request as $key=>$val){ //key is an array index, value is an object.
			$fields = array(

			);
			$returnValue = self::pushPostBodyFieldsToArray($fields, $returnValue);
		}
		return $returnValue;
	}

	public function formatExportTransfers($request){
		$returnValue = array();
		foreach($request as $key=>$val){ //key is an array index, value is an object.
			$fields = array(
				//TODO
			);
			$returnValue = self::pushPostBodyFieldsToArray($fields, $returnValue);
		}
	return $returnValue;
}
}