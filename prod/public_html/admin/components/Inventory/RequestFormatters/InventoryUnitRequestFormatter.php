<?php
require_once __DIR__ . "/BasicFormatter.php";
/**
 * Created by PhpStorm.
 * User: Leif
 * Date: 4/19/17
 * Time: 11:14 AM
 */
class InventoryUnitRequestFormatter extends BasicFormatter
{
	public function formatCreateUnits($request){
		$returnValue = array();
		foreach($request as $key=>$val){ //key is an array index, value is an object.
			$fields = array(
				'name'          => self::getPostVarAndCheckIsSet($val, 'name',        API_REQUIRED_FIELD),
				'symbol'        => self::getPostVarAndCheckIsSet($val, 'symbol',      API_REQUIRED_FIELD),
				'categoryID'    => self::getPostVarAndCheckIsSet($val, 'categoryID',  API_REQUIRED_FIELD),
				'conversion'    => self::getPostVarAndCheckIsSet($val, 'conversion',  API_REQUIRED_FIELD),
				'_deleted'      => self::getPostVarAndCheckIsSet($val, '_deleted',    API_OPTIONAL_FIELD)
			);
			$returnValue = self::pushPostBodyFieldsToArray($fields, $returnValue);
		}
		return $returnValue;
	}
}