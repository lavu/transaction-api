<?php
require_once __DIR__ . "/BasicFormatter.php";
/**
 * Created by PhpStorm.
 * User: Leif
 * Date: 4/19/17
 * Time: 11:10 AM
 */
class InventoryCategoryRequestFormatter extends BasicFormatter
{
	public function formatCreateInventoryCategories($request){
		$returnValue = array();
		foreach($request as $key=>$val){ //key is an array index, value is an object.
			$fields = array(
				'name'        => self::getPostVarAndCheckIsSet($val, 'name',         API_REQUIRED_FIELD),
				'description' => self::getPostVarAndCheckIsSet($val, 'description',  API_OPTIONAL_FIELD),
				'_deleted'    => self::getPostVarAndCheckIsSet($val, '_deleted',     API_OPTIONAL_FIELD)
			);
			$returnValue = self::pushPostBodyFieldsToArray($fields, $returnValue);

		}
		return $returnValue;

	}
}