<?php
require_once __DIR__."/BasicFormatter.php";
/**
 * Created by PhpStorm.
 * User: Leif
 * Date: 4/19/17
 * Time: 9:18 AM
 */
/**
 * Requests having to do with Vendors as their main Inventory Object should go through this formatter.
 * Please follow the method naming convention of format{action}{Object}{optional:For{SuperAction}{SuperObject}}
 */
class InventoryVendorRequestFormatter extends BasicFormatter
{
	public function formatCreateVendors($request)
	{
		return $this->formatCreateOrUpdateVendors($request, false);
	}

	public function formatUpdateVendors($request){
		return $this->formatCreateOrUpdateVendors($request, true);
	}

    // Make all field as API_REQUIRED_FIELD because we need all column even it is empty (Ticket:- LP-2716)
    // We will have to take care required field in validation
	public function formatCreateOrUpdateVendors($request, $includeID){
		$returnValue = array();
		foreach($request as $key=>$val){ //key is an array index, value is an object.
			$fields = array(
				'name'                  => self::getPostVarAndCheckIsSet($val, 'name',                  API_REQUIRED_FIELD),
				'contactName'           => self::getPostVarAndCheckIsSet($val, 'contactName',           API_REQUIRED_FIELD),
				'contactPhone'          => self::getPostVarAndCheckIsSet($val, 'contactPhone',          API_REQUIRED_FIELD),
				'contactEmail'          => self::getPostVarAndCheckIsSet($val, 'contactEmail',          API_REQUIRED_FIELD),
				'contactFax'            => self::getPostVarAndCheckIsSet($val, 'contactFax',            API_REQUIRED_FIELD),
				'address'               => self::getPostVarAndCheckIsSet($val, 'address',               API_REQUIRED_FIELD),
				'city'                  => self::getPostVarAndCheckIsSet($val, 'city',                  API_REQUIRED_FIELD),
				'stateProvinceRegion'   => self::getPostVarAndCheckIsSet($val, 'stateProvinceRegion',   API_REQUIRED_FIELD),
				'zipcode'               => self::getPostVarAndCheckIsSet($val, 'zipcode',               API_REQUIRED_FIELD),
				'country'               => self::getPostVarAndCheckIsSet($val, 'country',               API_REQUIRED_FIELD),
				'_deleted'              => self::getPostVarAndCheckIsSet($val, '_deleted',              API_OPTIONAL_FIELD),
			);
			if($includeID === true){
				$fields['id'] = self::getPostVarAndCheckIsSet($val, 'id',   API_REQUIRED_FIELD);
			}
			$returnValue = self::pushPostBodyFieldsToArray($fields, $returnValue);
		}
		return $returnValue;
	}

	public function formatIDsFromRequest($request){
		return $this->formatIDArray($request);
	}
}