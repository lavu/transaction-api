<?php
require_once __DIR__ . "/BasicFormatter.php";
/**
 * Created by PhpStorm.
 * User: Leif
 * Date: 5/13/17
 * Time: 3:16 PM
 */
class InventoryTransferItemRequestFormatter extends BasicFormatter
{
	public function formatCreateTransferItems($itemList){
		$returnValue = array();
		foreach($itemList as $key=>$val){ //key is an array index, value is an object.
			$fields = array(
				'transferID'            => self::getPostVarAndCheckIsSet($val, 'transferID',            API_REQUIRED_FIELD),
				'inventoryItemID'       => self::getPostVarAndCheckIsSet($val, 'inventoryItemID',       API_REQUIRED_FIELD),
				'transferUnitID'        => self::getPostVarAndCheckIsSet($val, 'transferUnitID',        API_REQUIRED_FIELD),
				'quantityTransferred'   => self::getPostVarAndCheckIsSet($val, 'quantityTransferred',   API_REQUIRED_FIELD),
				'transferUnitCost'      => self::getPostVarAndCheckIsSet($val, 'transferUnitCost',      API_REQUIRED_FIELD),
				'quantityReceived'      => self::getPostVarAndCheckIsSet($val, 'quantityReceived',      0),
				'SKU'                   => self::getPostVarAndCheckIsSet($val, 'SKU',                   API_OPTIONAL_FIELD),
				'_deleted'              => self::getPostVarAndCheckIsSet($val, '_deleted',              API_OPTIONAL_FIELD),
			);
			$returnValue = self::pushPostBodyFieldsToArray($fields, $returnValue);
		}
		return $returnValue;
	}

	public function formatUpdateTransferItems($itemList){
		$returnValue = array();
		foreach($itemList as $key=>$val){ //key is an array index, value is an object.
			$fields = array(
				'id'                    => self::getPostVarAndCheckIsSet($val, 'id',                    API_REQUIRED_FIELD),
				'transferID'            => self::getPostVarAndCheckIsSet($val, 'transferID',            API_OPTIONAL_FIELD),
				'inventoryItemID'       => self::getPostVarAndCheckIsSet($val, 'inventoryItemID',       API_OPTIONAL_FIELD),
				'transferUnitID'        => self::getPostVarAndCheckIsSet($val, 'transferUnitID',        API_OPTIONAL_FIELD),
				'quantityTransferred'   => self::getPostVarAndCheckIsSet($val, 'quantityTransferred',   API_OPTIONAL_FIELD),
				'transferUnitCost'      => self::getPostVarAndCheckIsSet($val, 'transferUnitCost',      API_OPTIONAL_FIELD),
				'quantityReceived'      => self::getPostVarAndCheckIsSet($val, 'quantityReceived',      API_OPTIONAL_FIELD),
				'SKU'                   => self::getPostVarAndCheckIsSet($val, 'SKU',                   API_OPTIONAL_FIELD),
				'_deleted'              => self::getPostVarAndCheckIsSet($val, '_deleted',              API_OPTIONAL_FIELD),
				'transferItemNumber'    => self::getPostVarAndCheckIsSet($val, 'transferItemNumber',    API_REQUIRED_FIELD),
			);
			$returnValue = self::pushPostBodyFieldsToArray($fields, $returnValue);
		}
		return $returnValue;
	}

	public function formatGetTransferItemByTransferIDAndInventoryItemID($itemList){
		$returnValue = array();
		foreach($itemList as $key=>$val){ //key is an array index, value is an object.
			$fields = array(
				'transferID'            => self::getPostVarAndCheckIsSet($val, 'transferID',            API_REQUIRED_FIELD),
				'inventoryItemID'       => self::getPostVarAndCheckIsSet($val, 'inventoryItemID',       API_REQUIRED_FIELD),
				'_deleted'              => self::getPostVarAndCheckIsSet($val, '_deleted',              API_OPTIONAL_FIELD),
			);
			$returnValue = self::pushPostBodyFieldsToArray($fields, $returnValue);
		}
		return $returnValue;
	}

}