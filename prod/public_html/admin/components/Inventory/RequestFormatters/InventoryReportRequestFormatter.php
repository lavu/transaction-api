<?php
require_once __DIR__."/BasicFormatter.php";
/**
 * Requests having to do with Vendors as their main Inventory Object should go through this formatter.
 * Please follow the method naming convention of format{action}{Object}{optional:For{SuperAction}{SuperObject}}
 */
class InventoryReportRequestFormatter extends BasicFormatter
{
    public function formatReportStartDate($request) {
        return $request['startDate'];
    }

    public function formatReportEndDate($request) {
        return $request['endDate'];
   }
}