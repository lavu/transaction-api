<?php

require_once __DIR__ . "/BasicFormatter.php";
/**
 * Created by PhpStorm.
 * User: Leif
 * Date: 7/20/17
 * Time: 10:15 AM
 */
class InventoryMenuItemRequestFormatter extends BasicFormatter
{
	public function formatGetMenuItemsByInventoryItemIDs($request){
		return $this->formatIDArray($request);
	}
}