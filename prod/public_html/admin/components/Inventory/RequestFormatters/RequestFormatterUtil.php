<?php
/**
 * Created by PhpStorm.
 * User: Leif
 * Date: 4/18/17
 * Time: 2:52 PM
 */
class RequestFormatterUtil
{

	public static function changePassedVariableNameToUsableVariableName($request, $fromName, $toName){
		for($i = 0; $i < count($request); $i++){
			if (isset($request[$i][$fromName]))
			{
				$request[$i][$toName] = $request[$i][$fromName];
			}
		}
		return $request;
	}

	/**
	 * @param $request
	 * @param $key
	 * @param $default
	 * @return string
	 */
	public static function getPostVarAndCheckIsSet($request, $key, $default)
	{
		if(isset($request[$key]) && $request[$key] !== "")
		{
			return $request[$key];
		}
		else if($default === API_REQUIRED_FIELD)
		{
			return null; //Lavu-API will check for null and produce failure response.
		}
		else if($default === API_OPTIONAL_FIELD)
		{
			return API_MISSING_OPTIONAL_FIELD;
		}
		else
		{
			return $default;
		}
	}

	/**
	 * Checks to see if the fields are missing or not, and pushes them to the returned array if they are not missing. This
	 * should be used if there are optional fields.
	 * @param $fields
	 * @param $returnArray
	 * @return array
	 */
	public static function pushPostBodyFieldsToArray($fields, $returnArray)
	{
		$saveArray = array();
		foreach($fields as $key=>$val)
		{

			if($val !== API_MISSING_OPTIONAL_FIELD)
			{
				$saveArray[$key] = $val;
			}

		}
		array_push($returnArray, $saveArray);
		return $returnArray;
	}

	public static function decodeLavuAPIResponse($response){ //Returns an indexed array [0],[1],...
		$response = json_decode(json_encode($response), true); //Needed to convert from object to array.
		if (isset($response['DataResponse']['DataResponse'])) {
			return $response['DataResponse']['DataResponse'];
		}
		return $response;
	}

	public static function encodeLavuAPIResponse($response, $count = null){
		$result = array();
		$result['DataResponse']['DataResponse'] = $response;
		$result['RequestID'] = 0;
		$result['Status'] = "Success";
		if($count) {
		 	$result['totalRecords'] = $count;	
		}	
		return $result;
	}

	/**
	 * @param $table1
	 *  Table to join in-to.
	 * @param $table2
	 *  Table to pull data from.
	 * @param $table1MatchingField
	 *  The value of this field in table1 is checked for matching against table2's $table2MatchingField value.
	 * @param $table2MatchingField
	 *  The value of this field in table2 is checked for matching against table1's $table1MatchingField value.
	 * @param $pullArrayFromTable2
	 *  If $table1MatchingField matches $table2MatchingField, table2's values for the columns specified in this array
	 *  are pulled from table2 for insertion into table1.
	 *  If this is an empty array, the whole row will be pulled, and $pushArrayToTable1 must be a one-element-array.
	 * @param $pushArrayToTable1
	 *  If $table1MatchingField matches $table2MatchingField, table2 elements will be inserted into table1, where the
	 *  elements correspond by index from $pullArrayFromTable2 to $pushArrayToToTable1. Table1 will then have new
	 *  elements in it where those element's keys will be the values in the $pushArrayToTable1 array.
	 * @return array
	 */
	public static function joinFieldsOrRowsFromTable2ToTable1($table1, $table2, $table1MatchingField, $table2MatchingField, $pullArrayFromTable2, $pushArrayToTable1){
		for($i = 0; $i < count($table1); $i++){
			if(isset($table1[$i]))
			{
				$currentTable1Element = $table1[$i];

				if (isset($currentTable1Element[$table1MatchingField]))
				{
					$table1Value = $currentTable1Element[$table1MatchingField]; //value of the field from table 2 to pull out.

					for ($j = 0; $j < count($table2); $j++)
					{
						$table2Element = $table2[$j];
						if ($table1Value == $table2Element[$table2MatchingField])
						{
							for ($k = 0; $k < count($pushArrayToTable1); $k++)
							{
								if (count($pullArrayFromTable2) === 0 && count($pushArrayToTable1) === 1)
								{
									$currentTable1Element[$pushArrayToTable1[$k]] = $table2Element;
								} else
								{
									$currentTable1Element[$pushArrayToTable1[$k]] = $table2Element[$pullArrayFromTable2[$k]];
								}
							}
							break;
						}
					}
				}
				$table1[$i] = $currentTable1Element;
			}
		}
		return $table1;

	}

	/**
	 * @param $table1
	 * @param $table2
	 * @param $table1FieldToInsert
	 * @param $table1MatchingField
	 * @param $table2MatchingField
	 * @return array
	 * Returns $table1 with, in each table1 row a $table2 row is inserted as an array field with key
	 * $table1FieldToInsert if $table1MatchingField and $table2MatchingField match.
	 */
	public static function joinTable2RowsIntoTable1IfFieldsMatch($table1, $table2, $table1FieldToInsert, $table1MatchingField, $table2MatchingField){
		for($i = 0; $i < count($table1); $i++){


			for($j = 0; $j < count($table2); $j++){

				if(isset($table2[$j][$table2MatchingField])){

					if($table2[$j][$table2MatchingField] == $table1[$i][$table1MatchingField]){
						//initialize an array if we are going to be adding sub-items and it has not already been
						//initialized.
						if(!isset($table1[$i][$table1FieldToInsert])){
							$table1[$i][$table1FieldToInsert] = array();
						}
						$table1[$i][$table1FieldToInsert][] = $table2[$j];
					}
				}
			}


		}
		return $table1;
	}


}