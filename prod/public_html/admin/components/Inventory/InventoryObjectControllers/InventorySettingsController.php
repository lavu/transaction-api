<?php
require_once __DIR__."/ControllerUtil.php";

/**
 * Created by PhpStorm.
 * User: Leif
 * Date: 7/12/17
 * Time: 3:03 PM
 */
class InventorySettingsController extends ControllerUtil
{
	public function getInventorySettings(){
		$result = self::performInventoryAPICall(array(), "getInventorySettings");
		return $result;
	}
}