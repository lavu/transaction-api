<?php
require_once __DIR__."/ControllerUtil.php";
/**
 * Created by PhpStorm.
 * User: Leif
 * Date: 7/20/17
 * Time: 10:19 AM
 */
class InventoryMenutItemController extends ControllerUtil
{
	public function getMenuItemsByInventoryItemID($idArray){
		$result = self::performInventoryAPICall($idArray, "getMenuItemsByInventoryItemID");
		return $result;
	}

	public function getAllMenuItems(){
		$result = self::performInventoryAPICall(array(), "getAllMenuItems");
		return $result;
	}
}