<?php
require_once __DIR__."/ControllerUtil.php";
/**
 * Created by PhpStorm.
 * User: Leif
 * Date: 4/19/17
 * Time: 1:54 PM
 */
class InventoryTransferController extends ControllerUtil
{
	public function createTransfers($request){
		$result = self::performInventoryAPICall($request, "createTransfers");
		return $result;
	}

	public function copyTransfers($request){
		$result = self::performInventoryAPICall($request, "copyTransfers");
		return $result;
	}

	public function receiveTransfers($request){
		$result = self::performInventoryAPICall($request, "receiveTransfers");
		return $result;
	}

	public function importTransfers($request){
		$result = self::performInventoryAPICall($request, "importTransfers");
		return $result;
	}

	public function exportTransfers($storageLocationArray){
		$result = self::performInventoryAPICall($storageLocationArray, "exportTransfers");
		return $result;
	}

	public function getAllTransfers($request = array()){
		$result = self::performInventoryAPICall($request, "getAllTransfers");
		return $result;
	}

	public function getAllPendingTransfers(){
		$result = self::performInventoryAPICall(array(), "getAllPendingTransfers");
		return $result;
	}

	public function getAllReceivedTransfers(){
		$result = self::performInventoryAPICall(array(), "getAllReceivedTransfers");
		return $result;
	}

	public function getTransfersByID($request){
		$result = self::performInventoryAPICall($request, "getTransferByID");
		return $result;
	}

	public function updateTransfers($request){
		$result = self::performInventoryAPICall($request, "updateTransfers");
		return $result;
	}

	public function getAllTransferLocations(){
		$result = self::performEnterpriseAPICall(array(),"getAllChainedRestaurants");
		return $result;
	}
}