<?php
/**
 * Created by PhpStorm.
 * User: Leif Guillermo
 * Date: 4/6/17
 * Time: 9:13 AM
 */

require_once __DIR__."/../../../api_private/api.php";
require_once __DIR__."/../../../cp/resources/core_functions.php";

/**
 * Class ControllerUtil
 */
class ControllerUtil{
	/**
	 * @param $paramArray
	 * @param $methodName
	 * @return string
	 */
	protected function performInventoryAPICall($paramArray, $methodName){
		$jsonAPIRequest = self::createJSONForQuery($paramArray, $methodName, 'Inventory');
		return $this->makeAPICall($jsonAPIRequest,$methodName);
	}

	/**
	 * @param $paramArray
	 * @param $methodName
	 * @return string
	 */
	protected function performEnterpriseAPICall($paramArray, $methodName){
		$jsonAPIRequest = self::createJSONForQuery($paramArray, $methodName, 'Enterprise');
		return $this->makeAPICall($jsonAPIRequest,$methodName);
	}

	/**
	 * @param $paramArray
	 * @param $methodName
	 * @return string
	 */
	protected function performRedisAPICall($paramArray, $methodName){
		$jsonAPIRequest = self::createJSONForQuery($paramArray, $methodName, 'Redis');
		return $this->makeAPICall($jsonAPIRequest, $methodName);
	}

	/**
	 * @param $jsonAPIRequest
	 * @param $methodName
	 * @param bool $fakeAPICall
	 * @return string
	 */
	private function makeAPICall($jsonAPIRequest, $methodName, $fakeAPICall=false){
		$result = json_decode(Internal_API::apiQuery($jsonAPIRequest));
		if($fakeAPICall === true)
		{
			require_once __DIR__."/../FAKE_LavuAPI/Fake_API.php";
			$result = FAKE_API::apiQuery($jsonAPIRequest, $methodName);
		}
		return $result;
	}

	/**
	 *
	 * @param $paramArray
	 * @param $function
	 * @param $component
	 * @param $apiVersion
	 * @return string
	 */
	protected function createJSONForQuery($paramArray, $function, $component, $apiVersion=1){
		$userInfo = array("dataname" =>  admin_info("dataname"));
		$jsonArr = array();
		$jsonArr['ApiVersion']      = $apiVersion;
		$jsonArr['Component']       = $component;
		$jsonArr['Action']          = $function;
		$jsonArr['Auth']            = $userInfo;
		$jsonArr['Params']['Items'] = $paramArray;
		return json_encode($jsonArr);
	}

}