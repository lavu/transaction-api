<?php
require_once __DIR__."/ControllerUtil.php";
/**
 * Created by PhpStorm.
 * User: Leif
 * Date: 4/11/17
 * Time: 2:47 PM
 */
class InventoryPurchaseOrderController extends ControllerUtil
{

	public function createPurchaseOrders($purchaseOrderArray){
		$result = self::performInventoryAPICall($purchaseOrderArray, "createPurchaseOrders");
		return $result;
	}

	public function copyPurchaseOrders($idArray){
		$result = self::performInventoryAPICall($idArray, "copyPurchaseOrders");
		return $result;
	}

	public function getPurchaseOrderByID($idArray){
		$result = self::performInventoryAPICall($idArray, "getPurchaseOrder");
		return $result;
	}

	public function getAllPurchaseOrders($pid = array()){
		$result = self::performInventoryAPICall($pid, "getAllPurchaseOrders");
		return $result;
	}

	public function receivePurchaseOrders($idAndQuantityArray){
		$result = self::performInventoryAPICall($idAndQuantityArray, "receivePurchaseOrders");
		return $result;
	}

	public function getExpectedPurchaseOrders(){
		$result = self::performInventoryAPICall(array(), "getExpectedPurchaseOrders");
		return $result;
	}

    public function getPurchaseOrderDetails($args){
        $result = self::performInventoryAPICall($args, "getPurchaseOrderDetails");
        return $result;
    }

	public function updatePurchaseOrderStatuses($request){
		return self::performInventoryAPICall($request, 'updatePurchaseOrders');
	}

	public function getLatestPurchaseOrderForVendorID($request) {
		return self::performInventoryAPICall($request, 'getOrderedItemsForVendorID');
	}

	public function getPurchaseOrdersForVendorID($request) {
		return self::performInventoryAPICall($request, 'getAllPurchaseOrdersForVendorID');
	}

	public function getPurchaseOrderItemsByPurchaseOrderID($request) {
		return self::performInventoryAPICall($request, 'getOrderedItemsForVendorID');
	}
}