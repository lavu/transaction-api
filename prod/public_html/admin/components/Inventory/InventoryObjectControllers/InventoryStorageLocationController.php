<?php
require_once __DIR__."/ControllerUtil.php";
/**
 * Created by PhpStorm.
 * User: Leif
 * Date: 4/19/17
 * Time: 11:36 AM
 */
class InventoryStorageLocationController extends ControllerUtil
{
	public function createStorageLocations($storageLocationArray){
		$result = self::performInventoryAPICall($storageLocationArray, "createStorageLocations");
		return $result;
	}

	public function getAllStorageLocations(){
		$result = self::performInventoryAPICall(array(),"getAllStorageLocations");
		return $result;
	}

	public function getAllStorageLocationsWithArchives(){
		$result = self::performInventoryAPICall(array(),"getAllStorageLocationsWithArchives");
		return $result;
	}
}