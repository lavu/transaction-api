<?php
require_once __DIR__."/ControllerUtil.php";
/**
 * Created by PhpStorm.
 * User: Leif
 * Date: 4/19/17
 * Time: 12:21 PM
 */
class InventoryUnitCategoryController extends ControllerUtil
{
	public function createUnitCategories($vendorItemArray){
		$result = self::performInventoryAPICall($vendorItemArray, "createUnitCategories");
		return $result;
	}
}