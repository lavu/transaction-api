<?php
require_once __DIR__."/ControllerUtil.php";
/**
 * Created by PhpStorm.
 * User: Leif
 * Date: 4/20/17
 * Time: 11:18 AM
 */
class InventoryPurchaseOrderItemController extends ControllerUtil
{

	public function createPurchaseOrderItems($request){
		$result = self::performInventoryAPICall($request, "createPurchaseOrderItems");
		return $result;
	}

	public function updatePurchaseOrderItems($request){
		$result = self::performInventoryAPICall($request, "updatePurchaseOrderItems");
		return $result;
	}

	public function getItemsForPurchaseOrder($idArray){
		$result = self::performInventoryAPICall($idArray, "getItemsForPurchaseOrder");
		return $result;
	}

	public function getAllPurchaseOrderItems(){
		$result = self::performInventoryAPICall(array(), "getAllPurchaseOrderItems");
		return $result;
	}

	public function getAllPurchaseUnitsOfMeasure(){
		$result = self::performInventoryAPICall(array(), "getAllPurchaseUnitsOfMeasure");
		return $result;
	}

	public function getPurchaseOrderItemsByPurchaseOrderID($idArray){
		$result = self::performInventoryAPICall($idArray, "getPurchaseOrderItems");
		return $result;
	}
}