<?php
require_once __DIR__."/ControllerUtil.php";

class InventoryReportsController extends ControllerUtil
{
	/**
	 * @param $args array with required keys: startDate, endDate
	 * @return string
	 */
	public function getSalesRevenueForTimePeriod($args){
		$result = self::performInventoryAPICall($args, "getSalesRevenueForDateRange");
		return $result;
	}

	/**
	 * @param $idAndQuantityArray
	 * @return string
	 */
	public function getUsers(){
		$result = self::performInventoryAPICall(array(), "getUsers");
		return $result;
	}
}