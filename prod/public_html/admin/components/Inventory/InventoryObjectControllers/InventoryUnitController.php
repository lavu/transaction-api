<?php
require_once __DIR__."/ControllerUtil.php";
/**
 * Created by PhpStorm.
 * User: Leif
 * Date: 4/19/17
 * Time: 11:15 AM
 */

//TODO maybe we should standardize saying create{API}{Object} and essentially include the API to not confuse future Lavu-API Requests.
class InventoryUnitController extends ControllerUtil
{
	public function createUnits($vendorItemArray){
		$result = self::performInventoryAPICall($vendorItemArray, "createUnits");
		return $result;
	}

	public function getAllUnits(){
		$result = self::performInventoryAPICall(array(), "getAllUnits");
		return $result;
	}

    public function getAllUnitsByCategory($request){
        $result = self::performInventoryAPICall($request, "getAllUnitsByCategory");
        return $result;
    }

    public function getCustomUnitsOfMeasure(){
	    $result = self::performInventoryAPICall(array(), "getCustomUnitsOfMeasure");
	    return $result;
    }

    public function getAllStandardUnitsOfMeasure() {
		$result = self::performInventoryAPICall(array(), "getAllStandardUnitsOfMeasure");
		return $result;
    }

}