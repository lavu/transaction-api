<?php
require_once __DIR__."/ControllerUtil.php";
/**
 * Created by PhpStorm.
 * User: Leif
 * Date: 5/8/17
 * Time: 9:27 AM
 */
class InventoryEnterpriseController extends ControllerUtil
{

	public function getCountryNamesFull(){
		$result = self::performEnterpriseAPICall(array(), "getCountryNamesFull");
		return $result;
	}

	public function getCountryNamesAbbreviated(){
		$result = self::performEnterpriseAPICall(array(), "getCountryNamesAbbreviated");
		return $result;
	}

	public function getCountries(){
		$result = self::performEnterpriseAPICall(array(), "getCountries");
		return $result;
	}

	public function getChainIDByRestaurantID($idArray){
		$result = self::performEnterpriseAPICall($idArray, "getChainIDByRestaurantID");
		return $result;
	}

	public function getRestaurantByID($idArray){
		$result = self::performEnterpriseAPICall($idArray, "getRestaurantByID");
		return $result;
	}

	public function getAllChainedRestaurants(){
		$result = self::performEnterpriseAPICall(array(), "getAllChainedRestaurants");
		return $result;
	}

	public function getRestaurantIDAndTitle(){
		$result = self::performEnterpriseAPICall(array(), "getRestaurantIDAndTitle");
		return $result;
	}

	public function listChainedLocations(){
		$result = self::performEnterpriseAPICall(array(), "listChainedLocations");
		return $result;
	}

	public function getLanguageInfo(){
	    $result = self::performEnterpriseAPICall(array(), "getLanguageInfo");
	    return $result;
    }
}