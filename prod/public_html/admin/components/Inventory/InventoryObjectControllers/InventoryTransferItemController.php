<?php
require_once __DIR__."/ControllerUtil.php";
/**
 * Created by PhpStorm.
 * User: Leif
 * Date: 4/19/17
 * Time: 1:54 PM
 */
class InventoryTransferItemController extends ControllerUtil
{

	public function getAllTransferItems(){
		$result = self::performInventoryAPICall(array(), "getAllTransferItems");
		return $result;
	}
	public function getTransferItemByID($request){
		$result = self::performInventoryAPICall($request, "getTransferItemByID");
		return $result;
	}
	public function getTransferItemByTransferIDAndInventoryItemID($request){
		$result = self::performInventoryAPICall($request, "getTransferItemByTransferIDAndInventoryItemID");
		return $result;
	}
	public function updateTransferItems($request){
		$result = self::performInventoryAPICall($request, "updateTransferItems");
		return $result;
	}
}