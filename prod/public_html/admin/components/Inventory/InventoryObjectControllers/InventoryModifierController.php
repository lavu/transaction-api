<?php
require_once __DIR__."/ControllerUtil.php";
/**
 * Created by PhpStorm.
 * User: Leif
 * Date: 7/20/17
 * Time: 2:36 PM
 */
class InventoryModifierController extends ControllerUtil
{

	public function getAllModifiers(){
		$result = self::performInventoryAPICall(array(), "getAllModifiers");
		return $result;
	}
	public function getAllModifiersByInventoryItemID($ids){
		$result = self::performInventoryAPICall($ids, "getAllModifiersByInventoryItemID");
		return $result;
	}
	public function getAllForcedModifiers(){
		$result = self::performInventoryAPICall(array(), "getAllForcedModifiers");
		return $result;
	}
	public function getAllForcedModifiersByInventoryItemID($ids){
		$result = self::performInventoryAPICall($ids, "getAllForcedModifiersByInventoryItemID");
		return $result;
	}


}