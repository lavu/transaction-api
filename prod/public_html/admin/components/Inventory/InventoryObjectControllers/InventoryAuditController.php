<?php
require_once __DIR__."/ControllerUtil.php";
/**
 * Created by PhpStorm.
 * User: Leif
 * Date: 5/9/17
 * Time: 6:57 PM
 */
class InventoryAuditController extends ControllerUtil
{
	public function createReconciliationAudit($auditArray){
		$result = self::performInventoryAPICall($auditArray, "createReconciliationAudit");
		return $result;
	}

	public function createWasteAudit($auditArray){
		$result = self::performInventoryAPICall($auditArray, "createWasteAudit");
		return $result;
	}

	public function createReceiveWithoutPOAudit($auditArray){
		$result = self::performInventoryAPICall($auditArray, "createReceiveWithoutPOAudit");
		return $result;
	}

	public function createTransferUpdateAudit($auditArray){
		$result = self::performInventoryAPICall($auditArray, "createTransfersUpdateAudit");
		return $result;
	}

	public function createTransferReceivedAudit($auditArray){
		$result = self::performInventoryAPICall($auditArray, "createTransfersReceivedAudit");
		return $result;
	}

    public function createAudits($auditArray){
        $result = self::performInventoryAPICall($auditArray, "createAudits");
        return $result;
    }

    public function getAudits($auditArray){
        $result = self::performInventoryAPICall($auditArray, "getAudits");
        return $result;
    }

    public function getCriteriaAudits($auditArray){
        $result = self::performInventoryAPICall($auditArray, "getCriteriaAudits");
        return $result;
    }

    public function getAuditsByActionsAndDates($auditArray){
        $result = self::performInventoryAPICall($auditArray, "getAuditsByActionsAndDates");
        return $result;
    }


}