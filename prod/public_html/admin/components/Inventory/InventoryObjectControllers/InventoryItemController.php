<?php
require_once __DIR__."/ControllerUtil.php";
/**
 * Created by PhpStorm.
 * User: Leif Guillermo
 * Date: 3/14/17
 * Time: 11:36 AM
 */

/**
 * Each API call has it's own function. I've decided this is best for quick debugging, as you can easily insert
 * error_logs into any desired function to see the result and/or the request to the Lavu-API. You can do this even if
 * there are multiple requests being sent and you want to see results of a select few of them. Also, this allows us to
 * abstract away the api-method names, so end users don't have to worry about those.
 */
class InventoryItemController extends ControllerUtil
{
	/**
	 * @param $inventoryItemArray
	 * @return string
	 */
	public function createInventoryItems($inventoryItemArray){
		$result = self::performInventoryAPICall($inventoryItemArray, "createInventoryItems");
		return $result;
	}
	/**
	 * @param $inventoryItemArray
	 * @return string
	 */
	public function copyInventoryItems($inventoryItemArray){
		$result = self::performInventoryAPICall($inventoryItemArray, "copyInventoryItems");
		return $result;
	}

	public function updateInventoryItems($inventoryItemArray){
		$result = self::performInventoryAPICall($inventoryItemArray, "updateInventoryItems");
		return $result;
	}

	/**
	 * @param $archiveInventoryItemIDArray
	 * @return string
	 */
	public function archiveInventoryItems($archiveInventoryItemIDArray){
		$result = self::performInventoryAPICall($archiveInventoryItemIDArray, "archiveInventoryItemsByID");
		return $result;
	}

	public function addQuantitiesToInventoryItems($idAndQuantityArray){
		$result = self::performInventoryAPICall($idAndQuantityArray, "addQuantitiesToInventoryItems");
		return $result;
	}

	public function receiveWithoutPurchaseOrder($rwpoArray){
        $result = self::performInventoryAPICall($rwpoArray, "receiveWithoutPO");
        return $result;
    }

	/**
	 * @param $assocID
	 * @return string
	 */
	public function getInventoryItemsByID($assocID){
		$result = self::performInventoryAPICall($assocID, "getInventoryItemsByID");
		return $result;
	}

    /**
     * This function is used get records for more than one inventory ids
     * @param $assocID
     * @return string
     */
    public function getInventoryItemsByIDs($assocID){
        $result = self::performInventoryAPICall($assocID, "getInventoryItemsByIDs");
        return $result;
    }

	/**
	 * @return string
	 */
	public function getAllInventoryItems($pageid = array()){
		$result = self::performInventoryAPICall($pageid, "getAllInventoryItems");
		return $result;
	}

	public function getExternalInventoryItems($externalID){
		$result = self::performInventoryAPICall($externalID, "getExternalInventoryItems");
		return $result;
	}

	/**
	 * @return string
	 */
	public function getAllArchivedInventoryItems(){
		$result = self::performInventoryAPICall(array(), "getAllArchivedInventoryItems");
		return $result;
	}

	/**
	 * @param $idAndQuantityArray
	 * @return string
	 */
	public function wasteInventoryItems($idAndQuantityArray){
		$result = self::performInventoryAPICall($idAndQuantityArray, "wasteInventoryItems");
		return $result;
	}

	/**
	 * @return string
	 */
	public function getAllSalesUnitsOfMeasure(){
		$result = self::performInventoryAPICall(array(), "getAllSalesUnitsOfMeasure");
		return $result;
	}

	/**
	 * @return string
	 */
	public function getAllInventoryItemCounts(){
		$result = self::performInventoryAPICall(array(), "getInventoryItemCounts");
		return $result;
	}

	/**
	 * @return string
	 */
	public function getAllLowStockInventoryItems(){
		$result = self::performInventoryAPICall(array(), "getAllLowStockInventoryItems");
		return $result;
	}

	public function getCostData(){
		$result = self::performInventoryAPICall(array(), "getCostData");
		return $result;
	}

	public function getCostTimeline($itemArray){
        $result = self::performInventoryAPICall($itemArray, "getCostTimeline");
        return $result;
    }


	public function getWasteCostData(){
		$result = self::performInventoryAPICall(array(), "getWasteCostData");
		return $result;
	}

	public function getCostingMethod(){
		$result = self::performInventoryAPICall(array(), "getCostingMethod");
		return $result;
	}

	/*Gets the inventory item from the origin database*/
	public function getInventoryItemsByTransferItemNumbers($transferItemNumbers){
		$result = self::performInventoryAPICall($transferItemNumbers, "getInventoryItemsByTransferItemNumbers");
		return $result;
	}

	public function getInventoryItemsByName($nameArray){
		$result = self::performInventoryAPICall($nameArray, "getInventoryItemsByName");
		return $result;
	}

	public function getLegacyItems(){
	    $result = self::performInventoryAPICall(array(), "getLegacyInventoryItems");
	    return $result;
    }

    public function getStandardUnitConversion(){
        $result = self::performInventoryAPICall(array(), "getStandardUnitConversion");
        return $result;
    }

    public function getUnitConversion($unitIdArray){
        $result = self::performInventoryAPICall($unitIdArray, "getUnitConversion");
        return $result;
    }

    public function pauseMigration($itemArray){
        $result = self::performInventoryAPICall($itemArray, "pauseMigration");
        return $result;
    }

    public function resumeMigration(){
        $result = self::performInventoryAPICall(array(), "resumeMigration");
        return $result;
    }

    public function finalizeMigration($itemArray){
        $result = self::performInventoryAPICall($itemArray, "finalizeMigration");
        return $result;
    }

    public function startMigration($useArray){
        $result = self::performInventoryAPICall($useArray, "startMigration");
        return $result;
    }


    public function calculateValueForItems($itemArray){
        $result = self::performInventoryAPICall($itemArray, "getItemValue");
        return $result;
    }

    public function calculateTotalValue($itemArray){
        $result = self::performInventoryAPICall($itemArray, "getValue");
        return $result;
    }

	public function deleteAllInventory(){
		$result = self::performInventoryAPICall(array(), "deleteAllInventory");
		return $result;
	}

}
