<?php
require_once __DIR__."/ControllerUtil.php";
/**
 * Created by PhpStorm.
 * User: hayat
 * Date: 12/5/17
 * Time: 2:38 PM
*/
class InventoryLocationController extends ControllerUtil
{
    public function getAllChainedLocations(){
        $result = self::performEnterpriseAPICall(array(),"getAllChainedLocations");
        return $result;
    }

    public function changeLocation($request){
        $result = self::performEnterpriseAPICall($request,"changeLocation");
        return $result;
    }
}
