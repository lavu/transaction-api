<?php
require_once __DIR__."/ControllerUtil.php";
/**
 * Created by PhpStorm.
 * User: Hayat
 * Date: 06/09/17
 * Time: 10:50 PM
 */
class Inventory86CountController extends ControllerUtil
{
	public function createInventory86CountMenuItems($count86MenuItemsArray){
		$result = self::performInventoryAPICall($count86MenuItemsArray, "create86CountMenuItems");
		return $result;
	}

    public function createInventory86CountModifiers($count86ModifiersArray){
        $result = self::performInventoryAPICall($count86ModifiersArray, "create86CountModifiers");
        return $result;
    }

    public function createInventory86CountForcedModifiers($count86ForcedModifiersArray){
        $result = self::performInventoryAPICall($count86ForcedModifiersArray, "create86CountForcedModifiers");
        return $result;
    }

    public function getMenuItemsStatus($menuItemsIdsArray = array()){
        $result = self::performInventoryAPICall($menuItemsIdsArray, "getMenuItemsStatus");
        return $result;
    }

    public function getModifiersStatus($modifiersIdsArray = array()){
        $result = self::performInventoryAPICall($modifiersIdsArray, "getModifiersStatus");
        return $result;
    }

    public function getForcedModifiersStatus($forcedModifiersIdsArray = array()){
        $result = self::performInventoryAPICall($forcedModifiersIdsArray, "getForcedModifiersStatus");
        return $result;
    }

    public function deductMenuItemQuantity($menuItemIdsQuantityArray){
        $result = self::performInventoryAPICall($menuItemIdsQuantityArray, "deductMenuItemQuantity");
        return $result;
    }

    public function deductModifierItemQuantity($menuItemIdsQuantityArray){
        $result = self::performInventoryAPICall($menuItemIdsQuantityArray, "deductModifierItemQuantity");
        return $result;
    }

    public function deductForcedModifierItemQuantity($menuItemIdsQuantityArray){
        $result = self::performInventoryAPICall($menuItemIdsQuantityArray, "deductForcedModifierItemQuantity");
        return $result;
    }

	public function deductMenuItemQuantityOnHand($menuItemIdsQuantityArray){
		$result = self::performInventoryAPICall($menuItemIdsQuantityArray, "deductMenuItemQuantityOnHand");
		return $result;
	}

	public function deductModifierItemQuantityOnHand($menuItemIdsQuantityArray){
		$result = self::performInventoryAPICall($menuItemIdsQuantityArray, "deductModifierItemQuantityOnHand");
		return $result;
	}

	public function deductForcedModifierItemQuantityOnHand($menuItemIdsQuantityArray){
		$result = self::performInventoryAPICall($menuItemIdsQuantityArray, "deductForcedModifierItemQuantityOnHand");
		return $result;
	}

    public function addQuantityOnHand($menuItemIdsQuantityArray){
        $result = self::performInventoryAPICall($menuItemIdsQuantityArray, "addQuantityOnHand");
        return $result;
    }

    public function get86CountMenuItem($menuItemsIdsArray){
        $result = self::performInventoryAPICall($menuItemsIdsArray, "get86CountMenuItem");
        return $result;
    }

    public function get86CountModifier($modifiersIdsArray){
        $result = self::performInventoryAPICall($modifiersIdsArray, "get86CountModifier");
        return $result;
    }

    public function get86CountForcedModifier($forcedModifiersIdsArray){
        $result = self::performInventoryAPICall($forcedModifiersIdsArray, "get86CountForcedModifier");
        return $result;
    }

    public function delete86CountRowsMenuItems($menuItemsIdsArray){
        $result = self::performInventoryAPICall($menuItemsIdsArray, "delete86CountRowsMenuItems");
        return $result;
    }

    public function delete86CountRowsModifiers($modifiersIdsArray){
        $result = self::performInventoryAPICall($modifiersIdsArray, "delete86CountRowsModifiers");
        return $result;
    }

    public function delete86CountRowsForcedModifiers($forcedModifiersIdsArray){
        $result = self::performInventoryAPICall($forcedModifiersIdsArray, "delete86CountRowsForcedModifiers");
        return $result;
    }

    public function delete86CountMenuItems($menuItemsIdsArray){
        $result = self::performInventoryAPICall($menuItemsIdsArray, "delete86CountMenuItems");
        return $result;
    }

    public function delete86CountModifiers($modifiersIdsArray){
        $result = self::performInventoryAPICall($modifiersIdsArray, "delete86CountModifiers");
        return $result;
    }

    public function delete86CountForcedModifiers($forcedModifiersIdsArray){
        $result = self::performInventoryAPICall($forcedModifiersIdsArray, "delete86CountForcedModifiers");
        return $result;
    }
    public function update86CountMenuItems($menuItemsIdsArray){
        $result = self::performInventoryAPICall($menuItemsIdsArray, "update86CountMenuItems");
        return $result;
    }

    public function update86CountModifiers($modifiersIdsArray){
        $result = self::performInventoryAPICall($modifiersIdsArray, "update86CountModifiers");
        return $result;
    }

    public function update86CountForcedModifiers($forcedModifiersIdsArray){
        $result = self::performInventoryAPICall($forcedModifiersIdsArray, "update86CountForcedModifiers");
        return $result;
    }

    public function addMenuItemReserveQuantity($forcedModifiersIdsArray){
        $result = self::performInventoryAPICall($forcedModifiersIdsArray, "addMenuItemReserveQuantity");
        return $result;
    }

    public function addModifierReserveQuantity($forcedModifiersIdsArray){
        $result = self::performInventoryAPICall($forcedModifiersIdsArray, "addModifierReserveQuantity");
        return $result;
    }

    public function addforcedModifierReserveQuantity($forcedModifiersIdsArray){
        $result = self::performInventoryAPICall($forcedModifiersIdsArray, "addforcedModifierReserveQuantity");
        return $result;
    }

    public function removedMenuItemReserveQuantity($forcedModifiersIdsArray){
        $result = self::performInventoryAPICall($forcedModifiersIdsArray, "removedMenuItemReserveQuantity");
        return $result;
    }

    public function removedModifierReserveQuantity($forcedModifiersIdsArray){
        $result = self::performInventoryAPICall($forcedModifiersIdsArray, "removedModifierReserveQuantity");
        return $result;
    }

    public function removedforcedModifierReserveQuantity($forcedModifiersIdsArray){
        $result = self::performInventoryAPICall($forcedModifiersIdsArray, "removedforcedModifierReserveQuantity");
        return $result;
    }

    public function getMenuItemsLowQuantityStatus($menuItemsIdsArray = array()){
        $result = self::performInventoryAPICall($menuItemsIdsArray, "getMenuItemsLowQuantityStatus");
        return $result;
    }

    public function getModifiersLowQuantityStatus($modifiersIdsArray = array()){
        $result = self::performInventoryAPICall($modifiersIdsArray, "getModifiersLowQuantityStatus");
        return $result;
    }

    public function getForcedModifiersLowQuantityStatus($forcedModifiersIdsArray = array()){
        $result = self::performInventoryAPICall($forcedModifiersIdsArray, "getForcedModifiersLowQuantityStatus");
        return $result;
    }

    public function getInventorysReorderStatus($forcedModifiersIdsArray = array()){
        $result = self::performInventoryAPICall($forcedModifiersIdsArray, "getInventorysReorderStatus");
        return $result;
    }

    // Get status of menu item, forced modifier and modifier, considering inventory items quantity if same inventory used in
    // menu item, forced modifier and modifier
    public function getMenuStatus($menuIdsArray){
        $result = self::performInventoryAPICall($menuIdsArray, "getMenuStatus");
        return $result;
    }

	// Get status of given category's menu item, forced modifier and modifier, considering inventory items quantity if same inventory used in
	// menu item, forced modifier and modifier
	public function getMenuByCategory($categoryIdsArray){
		$result = self::performInventoryAPICall($categoryIdsArray, "getMenuByCategory");
		return $result;
	}
}