<?php
require_once __DIR__."/ControllerUtil.php";
/**
 * Created by PhpStorm.
 * User: Leif
 * Date: 5/11/17
 * Time: 3:21 PM
 */
class InventoryMiscController extends ControllerUtil
{
	public function getMonetaryInformation(){
		$result = self::performInventoryAPICall(array(), "getMonetaryInformation");
		return $result;
	}

	public function getMonetarySymbol(){
		$result = self::performInventoryAPICall(array(), "getMonetarySymbol");
		return $result;
	}

	public function getMonetarySymbolAlignment(){
		$result = self::performInventoryAPICall(array(), "getMonetarySymbolAlignment");
		return $result;
	}

	public function getMonetaryDecimalSeparator(){
		$result = self::performInventoryAPICall(array(), "getMonetaryDecimalSeparator");
		return $result;
	}

	public function getMonetaryThousandsSeparator(){
		$result = self::performInventoryAPICall(array(), "getMonetaryThousandsSeparator");
		return $result;
	}

	public function getAllPurchaseOrderStatuses(){
		$result = array();
		$statuses = array(
			'pending', 'sent', 'partial', 'closed'
		);
		$result['DataResponse']['DataResponse'] = $statuses;
		$result['Status'] = "Success";
		return $result;
	}

	public function getAllTransferStatuses(){
		$result = array();
		$statuses = array(
			'pending', 'accepted', 'declined', 'sent', 'partial', 'closed'
		);
		$result['DataResponse']['DataResponse'] = $statuses;
		$result['Status'] = "Success";
		return $result;
	}

    public function getConfigSetting($configArray){
        $result = self::performInventoryAPICall($configArray, "getConfigSetting");
        return $result;
    }

    public function createOrUpdateConfigSetting($configArray){
        $result = self::performInventoryAPICall($configArray, "createOrUpdateConfigSetting");
        return $result;
    }

	public function isLavuAdmin(){
		$result = array();
		$adminInfo =  (admin_info("lavu_admin")) ? true : false;
		$result['DataResponse']['DataResponse'] = array('isLavuAdmin' => $adminInfo);
		$result['Status'] = "Success";
		return $result;
	}
}