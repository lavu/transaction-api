<?php
require_once __DIR__."/ControllerUtil.php";
/**
 * Created by PhpStorm.
 * User: Leif
 * Date: 4/19/17
 * Time: 11:10 AM
 */
class InventoryCategoryController extends ControllerUtil
{
	public function createInventoryCategories($categoryArray){
		$result = self::performInventoryAPICall($categoryArray, "createCategories");
		return $result;
	}

	/**
	 * @return string
	 */
	public function getAllCategories(){
		$result = self::performInventoryAPICall(array(), "getAllCategories");
		return $result;
	}

	/**
	 * @return string
	 */
	public function getAllCategoriesWithArchives(){
		$result = self::performInventoryAPICall(array(), "getAllCategoriesWithArchives");
		return $result;
	}
}