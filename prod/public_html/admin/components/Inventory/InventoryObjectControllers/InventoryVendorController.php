<?php
require_once __DIR__."/ControllerUtil.php";
/**
 * Created by PhpStorm.
 * User: Chris Aitken
 * Date: 4/19/17
 * Time: 10:48 AM
 */
class InventoryVendorController extends ControllerUtil
{
	public function createInventoryVendors($vendorArray){
		$result = self::performInventoryAPICall($vendorArray, "createVendors");
		return $result;
	}

	public function copyVendors($vendorArray){
	    $result = self::performInventoryAPICall($vendorArray, "copyVendors");
	    return $result;
	}

	public function archiveVendors($vendorArray){
	    $result = self::performInventoryAPICall($vendorArray, "archiveVendors");
	    return $result;
	}

	public function getAllVendors($page = array()){
		$result = self::performInventoryAPICall($page, "getAllVendors");
		return $result;
	}

	public function getAllVendorsWithArchives(){
		$result = self::performInventoryAPICall(array(), "getAllVendorsWithArchives");
		return $result;
	}

	public function getVendors($vendorArray){
		$result = self::performInventoryAPICall($vendorArray, "getVendors");
		return $result;
	}

	public function getVendorsOrderedItems($vendorArray){
	    $result = self::performInventoryAPICall($vendorArray, "getOrderedItemsForVendorID");
	    return $result;
	}

	public function updateVendors($vendorArray){
	    $result = self::performInventoryAPICall($vendorArray, "updateVendors");
	    return $result;
	}

	public function getVendorsByID($idArray){
		$result = self::performInventoryAPICall($idArray, 'getVendorsByID');
		return $result;
	}

	public function getVendorsByName($names){
		$result = self:: performInventoryAPICall($names, 'getVendorsByName');
		return $result;
	}
}