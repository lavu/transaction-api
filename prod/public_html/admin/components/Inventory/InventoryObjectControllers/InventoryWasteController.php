<?php
require_once __DIR__."/ControllerUtil.php";
/**
 * Created by PhpStorm.
 * User: Leif
 * Date: 5/13/17
 * Time: 8:56 PM
 */
class InventoryWasteController extends ControllerUtil
{
	public function getInventoryWasteCosts(){
		$result = self::performInventoryAPICall(array(), "getInventoryWasteCosts");
		return $result;
	}

    public function getAllWasteReasons(){
        $result = self::performInventoryAPICall(array(), "getAllWasteReasons");
        return $result;
    }

}