<?php
require_once __DIR__."/ControllerUtil.php";
/**
 * Created by PhpStorm.
 * User: Leif
 * Date: 4/17/17
 * Time: 3:29 PM
 */
class InventoryVendorItemController extends ControllerUtil
{
	public function createVendorItems($vendorItemArray){
		$result = self::performInventoryAPICall($vendorItemArray, "createVendorItems");
		return $result;
	}

	public function updateVendorItems($vendorItemArray){
		$result = self::performInventoryAPICall($vendorItemArray, "updateVendorItems");
		return $result;
	}

	public function getAllVendorItems(){
		$result = self::performInventoryAPICall(array(), "getAllVendorItems");
		return $result;
	}

	public function linkVendorItemsToInventoryItems($vendorItemIDInventoryItemIDArray){
		$result = self::performInventoryAPICall($vendorItemIDInventoryItemIDArray, "linkVendorItemsToInventoryItems");
		return $result;
	}

	public function getLinkedVendorItemsForInventoryItems($inventoryItemIDArray){
		$result = self::performInventoryAPICall($inventoryItemIDArray, "getLinkedVendorItemsForInventoryItems");
		return $result;
	}

	public function getVendorItemsByVendorID($vendorIDArray){
		$result = self::performInventoryAPICall($vendorIDArray, "getVendorItemsByVendorID");
		return $result;
	}

	public function getVendorItemsByVendorAndInventoryID($inventoryVendorIDArray){
		$result = self::performInventoryAPICall($inventoryVendorIDArray, "getVendorItemsByVendorAndInventoryID");
		return $result;
	}
}