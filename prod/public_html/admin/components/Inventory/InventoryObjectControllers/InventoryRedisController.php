<?php
require_once __DIR__."/ControllerUtil.php";
/**
 * Created by PhpStorm.
 * User: Leif
 * Date: 4/19/17
 * Time: 11:10 AM
 */
class InventoryRedisController extends ControllerUtil
{
	public function getInventory($iventoryIdsArray = array()){
		$result = self::performRedisAPICall($iventoryIdsArray, "getInventory");
		return $result;
	}

	public function setInventory($iventoryDetails = array()){
		$result = self::performRedisAPICall($iventoryDetails, "setInventory");
		return $result;
	}

	public function getInventory86Count($tableName, $ids = array()){
		$args['table86'] = $tableName;
		$args['ids'] = $ids;
		$result = self::performRedisAPICall($args, "getInventory86Count");
		return $result;
	}

	public function setInventory86Count($tableName, $ids = array()){
		$args['table86'] = $tableName;
		$args['ids'] = $ids;
		$result = self::performRedisAPICall($args, "setInventory86Count");
		return $result;
	}

	public function getAllRedisKeys($redisKeys = array()){
		$result = self::performRedisAPICall($redisKeys, "getAllRedisKeys");
		return $result;
	}

	public function checkRedisKeys($redisKeys = array()){
		$result = self::performRedisAPICall($redisKeys, "checkRedisKeys");
		return $result;
	}

	public function deleteRedisKeys($redisKeys = array()){
		$result = self::performRedisAPICall($redisKeys, "deleteRedisKeys");
		return $result;
	}

	public function getMenuItems($ids = array()){
		$args['ids'] = $ids;
		$result = self::performRedisAPICall($args, "getMenuItems");
		return $result;
	}

	public function getForcedModifiers($ids = array()){
		$args['ids'] = $ids;
		$result = self::performRedisAPICall($args, "getForcedModifiers");
		return $result;
	}

	public function getModifiers($ids = array()){
		$args['ids'] = $ids;
		$result = self::performRedisAPICall($args, "getModifiers");
		return $result;
	}

	public function setMenuItems($ids = array()){
		$args['ids'] = $ids;
		$result = self::performRedisAPICall($args, "setMenuItems");
		return $result;
	}

	public function setForcedModifiers($ids = array()){
		$args['ids'] = $ids;
		$result = self::performRedisAPICall($args, "setForcedModifiers");
		return $result;
	}

	public function setModifiers($ids = array()){
		$args['ids'] = $ids;
		$result = self::performRedisAPICall($args, "setModifiers");
		return $result;
	}

	public function getCategoryMenus($categoryMenus = array()){
		$result = self::performRedisAPICall($categoryMenus, "getCategoryMenus");
		return $result;
	}

	public function setCategoryMenus($categoryMenus = array()){
		$result = self::performRedisAPICall($categoryMenus, "setCategoryMenus");
		return $result;
	}

	public function deleteMenuCategoryKey($redisKeys = array()){
		$result = self::performRedisAPICall($redisKeys, "deleteMenuCategoryKey");
		return $result;
	}

	public function getComboItems($ids = array()){
		$args['ids'] = $ids;
		$result = self::performRedisAPICall($args, "getComboItems");
		return $result;
	}

	public function setComboItems($ids = array()){
		$args['ids'] = $ids;
		$result = self::performRedisAPICall($args, "setComboItems");
		return $result;
	}

	public function getLanguageDictionary($data){
		$result = self::performRedisAPICall($data, "getLanguageDictionary");
		return $result;
	}

	public function setLanguageDictionary($data){
		$result = self::performRedisAPICall($data, "setLanguageDictionary");
		return $result;
	}

	public function getCacheData($data){
		$result = self::performRedisAPICall($data, "getCacheData");
		return $result;
	}

	public function setCacheData($data){
		$result = self::performRedisAPICall($data, "setCacheData");
		return $result;
	}

}