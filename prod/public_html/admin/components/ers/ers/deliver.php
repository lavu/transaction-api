<?php
	session_start();
	require_once(dirname(__FILE__) . "/../comconnect.php");

	$api_dataname = "";
	if(isset($_SESSION['api_dataname']) && isset($_POST['dn']) && $_SESSION['api_dataname']==$_POST['dn'])
	{
		$api_dataname = $_SESSION['api_dataname'];
		$api_key = $_SESSION['api_key'];
		$api_token = $_SESSION['api_token'];
		
		$getvars = "";
		foreach($_GET as $key => $val)
		{
			if($getvars=="") $getvars .= "?";
			else $getvars .= "&";
			$getvats .= $key . "=" . $val;
		}
		$postvars = $_POST;
		$postvars['dataname'] = $api_dataname;
		$postvars['key'] = $api_key;
		$postvars['token'] = $api_token;
	}
	else if(isset($_POST['dn']))
	{
		$api_query = lavu_query("select AES_DECRYPT(`api_token`,'lavutokenapikey') as `api_token`, AES_DECRYPT(`api_key`,'lavuapikey') as `api_key` from `locations` where `id`='[1]'",$_POST['loc_id']);
		if(mysqli_num_rows($api_query))
		{
			$api_read = mysqli_fetch_assoc($api_query);
			
			$api_dataname = $_POST['dn'];
			$api_key = $api_read['api_key'];
			$api_token = $api_read['api_token'];
			
			$_SESSION['api_dataname'] = $api_dataname;
			$_SESSION['api_key'] = $api_key;
			$_SESSION['api_token'] = $api_token;
			
			$getvars = "";
			$postvars = array();
			$postvars['dataname'] = $api_dataname;
			$postvars['key'] = $api_key;
			$postvars['token'] = $api_token;
		}
	}
	else if(isset($_SESSION['api_dataname']))
	{
		$api_dataname = $_SESSION['api_dataname'];
		$api_key = $_SESSION['api_key'];
		$api_token = $_SESSION['api_token'];
		$getvars = "";
		$postvars = $_POST;
		$postvars['dataname'] = $api_dataname;
		$postvars['key'] = $api_key;
		$postvars['token'] = $api_token;
	}
	
	function api_build_key($i_time) {
		global $api_key;
		
		$s_minute = date('i',strtotime( (-(int)date('i')%10).' minutes' ));
		$s_time = date('Y').date('d').$s_minute;
		$s_seed = $api_key.$s_time.'Brian Dennedy';
		$s_key_time = md5($s_seed);
		return rawurlencode($s_key_time);
	}
	
	if(isset($_REQUEST['show_route']))
	{
		$route_mode_found = false;
		$route_mode = "";
		$all_getvars = "";
		foreach($_REQUEST as $gkey => $gval) 
		{
			if($gkey=="route_mode") 
			{
				$route_mode_found = true;
				$route_mode = $gval;
			}
			if($route_mode_found)
			{
				$all_getvars .= "&" . $gkey . "=" . urlencode($gval);
			}
		}
		if(1==1)
		{
			if($route_mode=="list")
			{
				$route_url = "https://lavuroute.ershost.com/maps/rwlavu_disabled_orders.php?view_delivered=1";
			}
			else
			{
				$route_url = "https://lavuroute.ershost.com/maps/show.php";
			}
			
			if(strpos($route_url,"?")===false)
				$route_url .= "?";
			else
				$route_url .= "&";
			echo "<script language='javascript'>";
			echo " 	function load_routing() { ";
			echo "		window.location = '" . $route_url . "app_mode=on&date=" . date('Y-m-d') . "&read_from_lavu=1&dataname=" . rawurlencode($api_dataname) . "&key=" . api_build_key(strtotime('now')) . "&source=cp" . $all_getvars . "'; ";
			echo "  } ";
			echo "</script>";
			echo "<body onload='load_routing()'>";
			echo "<table width='100%' height='100%' bgcolor='#eeeeee'><tr><td align='right' valign='top' onclick='window.location = \"_DO:cmd=close_overlay\"'>";
			echo "&nbsp;<br><b><font style='font-family:Verdana; font-size:24px; color:#aaaaaa'>Close X</font></b><br>&nbsp;";
			echo "</td></tr></table>";
			echo "</body>";
		}
		else
		{
			echo "<table width='100%' height='100%' bgcolor='#cccccc' style='border:solid 2px #777777'><tr><td>";
			echo "<input type='button' value='Close X' onclick='window.location = \"_DO:cmd=close_overlay\"' />";
			echo "<br>" . $all_getvars;
			echo "</td></tr></table>";
		}
	}
	else
	{
		$can_print_through_app = "no";
		$docmds_str = (isset($_POST['available_DOcmds']))?$_POST['available_DOcmds']:"";
		$docmds = explode(",",$docmds_str);
		//echo "docmds: " . count($docmds) . "<br>";
		for($i=0; $i<count($docmds); $i++)
		{
			if($docmds[$i]=="print") $can_print_through_app = "yes";
		}

		echo "<script language='javascript'>";
		echo "	window.location = 'https://lavuroute.ershost.com/maps/rwlavu.php?app_mode=on&date=" . date('Y-m-d') . "&read_from_lavu=1&dataname=" . rawurlencode($api_dataname) . "&key=" . api_build_key(strtotime('now')) . "&source=cp&can_print=$can_print_through_app'; ";
		echo "</script>";
	}
?>