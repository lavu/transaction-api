<?php
	session_start();
	require_once(dirname(__FILE__) . "/../comconnect.php");

	$api_dataname = "";
	if(isset($_SESSION['api_dataname']) && isset($_POST['dn']) && $_SESSION['api_dataname']==$_POST['dn'])
	{
		$api_dataname = $_SESSION['api_dataname'];
		$api_key = $_SESSION['api_key'];
		$api_token = $_SESSION['api_token'];
		
		$getvars = "";
		foreach($_GET as $key => $val)
		{
			if($getvars=="") $getvars .= "?";
			else $getvars .= "&";
			$getvats .= $key . "=" . $val;
		}
		$postvars = $_POST;
		$postvars['dataname'] = $api_dataname;
		$postvars['key'] = $api_key;
		$postvars['token'] = $api_token;
	}
	else if(isset($_POST['dn']))
	{
		$api_query = lavu_query("select AES_DECRYPT(`api_token`,'lavutokenapikey') as `api_token`, AES_DECRYPT(`api_key`,'lavuapikey') as `api_key` from `locations` where `id`='[1]'",$_POST['loc_id']);
		if(mysqli_num_rows($api_query))
		{
			$api_read = mysqli_fetch_assoc($api_query);
			
			$api_dataname = $_POST['dn'];
			$api_key = $api_read['api_key'];
			$api_token = $api_read['api_token'];
			
			$_SESSION['api_dataname'] = $api_dataname;
			$_SESSION['api_key'] = $api_key;
			$_SESSION['api_token'] = $api_token;
			
			$getvars = "";
			$postvars = array();
			$postvars['dataname'] = $api_dataname;
			$postvars['key'] = $api_key;
			$postvars['token'] = $api_token;
		}
	}
	else if(isset($_SESSION['api_dataname']))
	{
		$api_dataname = $_SESSION['api_dataname'];
		$api_key = $_SESSION['api_key'];
		$api_token = $_SESSION['api_token'];
		$getvars = "";
		$postvars = $_POST;
		$postvars['dataname'] = $api_dataname;
		$postvars['key'] = $api_key;
		$postvars['token'] = $api_token;
	}

	if($api_dataname!="")
	{
		if($getvars=="") $getvars .= "?";
		else $getvars .= "&";
		$getvars .= "PHPSESSID=" . session_id();
		session_write_close(); // necessary when on the same server to prevent locked session file
		
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, "https://www.eventrentalsystems.com/cp/poslavu_api.php".$getvars);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $postvars);
		$response = curl_exec($ch);
	
		echo $response;
	}
?>
