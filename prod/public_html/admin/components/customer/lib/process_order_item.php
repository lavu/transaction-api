<?php

//  Close info array.
//  0 - closed time
//  1 - discount
//  2 - discount label
//  3 - cash paid
//  4 - card paid
//  5 - alt paid
//  6 - server name
//  7 - tax amount
//  8 - total
//  9 - PIN used
// 10 - tax rate
// 11 - send to email
// 12 - gratuity
// 13 - cashier ID
// 14 - paid by gift certificate
// 15 - gratuity percent
// 16 - refund amount
// 17 - refund gift certificate amount
// 18 - reopened datetime
// 19 - subtotal
// 20 - deposit status
// 21 - active register
// 22 - discount value
// 23 - discount type
// 24 - refund cc amount
// 25 - rounded amount
// 26 - auto gratuity is taxed
// 27 - active register name
// 28 - tax exempt
// 29 - exemption id
// 30 - exemption name
// 31 - alt refund amount
/*
locationid: 1
 order_id: 1-75
 customerID: 5 //<----Here
content_id: 208
checked_out: 0
item_id: 689
subtotal_with_mods: 45.1
allow_deposit: 0
deposit_info:
item: Herbs
options: Customer:Billy boggy Froggy (5)
quantity: 1
hidden_value:
hidden_value2:
Close info:
	*/				
				
	function process_order_item($locationid, $order_id, $extract, $close_info, $data_name) {//$item_info?
	
		global $location_info;
		
		//$orderCustomerIdArrStr = $extract['order_customer_id'];
		//$orderCustomerIdParts = explode('|o|', $orderCustomerIdArrStr);
		//$customerID = $orderCustomerIdParts[3];
		$item_id = $extract["item_id"];
		//$order_id = $extract["order_id"];
		$content_id = $extract["content_id"];
		$checked_out = $extract["checked_out"];
	
		$subtotal_with_mods = $extract["subtotal_with_mods"];
		$allow_deposit = $extract["allow_deposit"];
		$deposit_info = $extract["deposit_info"];
		$item = $extract["item"];
		$options = $extract["options"];
		
		//edited by Brian D, we get $customerID from extract now
		$oid = $extract['order_customer_id'];
		if (!empty($oid) && (strstr($oid, "|o|Customer|o|") || strstr($oid, "|o|Member|o|"))) {
			$oid_parts = explode("|o|", $oid);
			$customerID = $oid_parts[3];
		}
				
		if (empty($customerID) && isset($options)) {
					
			//get customer id from string
			$IDPieces = explode(")", $options);
			$explodedIDPieces = $IDPieces[0];
			$piece = explode("(", $explodedIDPieces);
			//$customerID = $piece[1];//edited by Brian D, we get $customerID from extract now
					
		}//if
		
		$quantity = $extract["quantity"];
		$hidden_value = $extract["hidden_value"];
		$hidden_value2 = $extract["hidden_value2"];
		$device_time = date("Y-m-d H:i:s");
		$local_time = $close_info["0"];
		$tax_amount = $close_info["7"];
		$PIN_used = $close_info["9"];
		$tax_rate = $close_info["10"];
		$total = $close_info["8"];
		$cashier_id = $close_info["13"];
		
		$getUnitCostResults = lavu_query("SELECT price FROM menu_items WHERE id='[1]'",$item_id);
		$getUnitCostRows = mysqli_fetch_assoc($getUnitCostResults);
		$unit_cost = $getUnitCostRows['price'];
		
		/*
		lavu_query("INSERT INTO `med_action_log` " .
		              "(action, customer_id, order_id, restaurant_time, user_id, loc_id, quantity, device_time, tax_amount, total_cost, tax_rate, item_id, PIN_used, unit_cost) " .
		              "VALUES (\"Purchased $quantity $item\", \"$customerID\", \"$order_id\", \"$local_time\", \"$cashier_id\", \"$locationid\", \"$quantity\", \"$device_time\", \"$tax_amount\", \"$total\", \"$tax_rate\", \"$item_id\", \"$PIN_used\", \"$unit_cost\")");
		*/
		$insertArray = array();
		$insertArray['action'] = "Purchased $quantity $item";
		$insertArray['customer_id'] = $customerID;
		$insertArray['order_id'] = $order_id;
		$insertArray['restaurant_time'] = $local_time;
		$insertArray['user_id'] = $cashier_id;
		$insertArray['loc_id'] = $locationid;
		$insertArray['quantity'] = $quantity;
		$insertArray['device_time'] = $device_time;
		$insertArray['tax_amount'] = $tax_amount;
		$insertArray['total_cost'] = $total;
		$insertArray['tax_rate'] = $tax_rate;
		$insertArray['item_id'] = $item_id;
		$insertArray['PIN_used'] = $PIN_used;
		$insertArray['unit_cost'] = $unit_cost;
		lavu_query("INSERT INTO `med_action_log` " .
		              "(action, customer_id, order_id, restaurant_time, user_id, loc_id, quantity, device_time, tax_amount, total_cost, tax_rate, item_id, PIN_used, unit_cost) " .
		              "VALUES ('[action]', '[customer_id]', '[order_id]', '[restaurant_time]', '[user_id]', '[loc_id]', '[quantity]', '[device_time]', '[tax_amount]', '[total_cost]', '[tax_rate]', '[item_id]', '[PIN_used]', '[unit_cost]')", $insertArray);
		              
		              			

        //I don't know what $message is for.
		$message = "locationid: $locationid \n order_id: $order_id \n customerID: $customerID \n";
		foreach($extract as $key => $value){
			$message .= $key . ": " . $value . "\n";
		}//foreach
		$message .= "Close info: \n ";
		
		foreach($close_info as $key => $value){
			$message .= $key . ": " . $value . "\n";
		}
		$message .= "order_id: $order_id \n leftBracketPos: $leftBracketPosition \n";	
}

?>