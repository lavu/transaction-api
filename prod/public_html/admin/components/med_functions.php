<?php
require_once(dirname(__FILE__) . "/../comconnect.php");
?>

<?php

function populateForm($label, $type, $id, $customerID, $loc_id){
			/* Function takes in input from the config table, and prints the relevent form element to the screen */
			$valuesResult = lavu_query("SELECT * FROM config WHERE id=\"[1]\"", $id);
			$valuesRow = mysqli_fetch_assoc($valuesResult);
			$value2 = $valuesRow['value2'];//this is the field that stores customer information in the med_customers db
			$getCustInfoResult = lavu_query("SELECT $value2 FROM med_customers WHERE locationid=\"[1]\" AND id=\"[2]\"",$loc_id,$customerID);
			$getCustInfoRow = mysqli_fetch_assoc($getCustInfoResult);
			$value = $getCustInfoRow[$value2];
			
			$value4 = $valuesRow['value4'];//options
			$value5 = $valuesRow['value5'];
			$elementName = createFormElementName($label);
			//echo "$elementName<br>";
			if ($type == "list_menu"){
						
						if ($value4 == "useTool"){
									
									if ($value5 == "State"){
												
												State($value); //what should the input be?
												return;
									}//if
									
									if ($value5 == "Last Name"){
												//nothing needs to be done
												return;
									}//if
									
									if ($value5 == "First Name"){
												//nothing needs to be done
												return;
									}//if
									
						}//if
						
						if ($value4 != "useTool"){
												
												$printListMenu = "<select class=\"myInput\" name=\"$elementName\"><option value=\"\"></value>";
												
												$listMenuOptionsString = $value4;
												$listMenuOptionsArray = explode(",", $listMenuOptionsString);
												for ($i=0;$i<count($listMenuOptionsArray);$i++){
															
															$curVal = $listMenuOptionsArray[$i];
															$curLabel = createFormElementLabel($curVal);
															if ($curVal != $value){
																		$printListMenu .= "<option value=\"$curVal\">$curLabel</option>";
															}//if
															
															if ($curVal == $value){
																		$printListMenu .= "<option value=\"$curVal\" selected=\"selected\">$curLabel</option>";
															}//if
															
												}//for
												
												$printListMenu .= "</select>";
												echo $printListMenu;
												
												return;
						
						}//if
						
						
			}//if - list_menu
			
			if ($type == "input"){
				
					echo "
								<input type=\"text\" class=\"myInput\" value=\"$value\" name=\"$elementName\" id=\"$elementName\" />
					";
					
					return;
			}//if
			
			
			if ($type == "radio"){
					
					
					
					$valuesArray = explode(",", $value4);
					$formString;
			
					$radioGroupName = createFormElementLabel($label);
					for ($i=0;$i<count($valuesArray);$i++){
								$curVal = $valuesArray[$i];
								$curLabel = createFormElementLabel($curVal);
							echo "
							<div class=\"leftForm1\">
										<label>$curLabel</label>
							</div>";
							
							echo "<div class=\"rightForm1\">";
							
										if ($curVal == $value){			
											
													echo "<input value=\"$curVal\" type=\"radio\" name=\"$elementName\" checked=\"checked\"/>";
										}//if
										
										if ($curVal != $value){
													echo "<input value=\"$curVal\" type=\"radio\" name=\"$elementName\" />";
										}//if
										
							echo "</div>
							<div class=\"breakLine\"></div>
							";
							
					}//for
					
					
					return;
			}//if
			
			
}//populateForm()


function createFormElementLabel($label){
	/* function returns the label in the form that can be used for a form element name (eg, takes out spaces and replaces them with underscores). 
			For example, the label "On or Off" will be returned as "On_or_Off", so this can be used to gather $_POST[] data from forms. */
			
			$pieces = explode("_", $label);
			$label = implode(" ", $pieces);
			
			return $label; //finish this function.
	
	
}//createFormElementLabel()


function createFormElementName($label){
	/* Function replaces spaces with underscore so that way label names can be used as names form form elements*/
			$pieces = explode(" ", $label);
			$formElementName = implode("_",$pieces);
			return $formElementName;
			
}//makeElementName()


function State($selectedState){
	
	$state = $selectedState;//just so i don't have to readjust variables
	
	$state_list = "AL:Alabama,
AK:Alaska,
AZ:Arizona,
AR:Arkansas,
AK:Alaska,
AZ:Arizona,
AR:Arkansas,
CA:California,
CO:Colorado, 
CT:Connecticut, 
DE:Delaware, 
DC:District Of Columbia, 
FL:Florida, 
GA:Georgia, 
HI:Hawaii, 
ID:Idaho, 
IL:Illinois, 
IN:Indiana, 
IA:Iowa, 
KS:Kansas, 
KY:Kentucky, 
LA:Louisiana, 
ME:Maine, 
MD:Maryland, 
MA:Massachusetts, 
MI:Michigan, 
MN:Minnesota, 
MS:Mississippi, 
MO:Missouri, 
MT:Montana, 
NE:Nebraska, 
NV:Nevada, 
NH:New Hampshire, 
NJ:New Jersey, 
NM:New Mexico, 
NY:New York, 
NC:North Carolina, 
ND:North Dakota, 
OH:Ohio, 
OK:Oklahoma, 
OR:Oregon, 
PA:Pennsylvania, 
RI:Rhode Island, 
SC:South Carolina, 
SD:South Dakota, 
TN:Tennessee, 
TX:Texas, 
UT:Utah, 
VT:Vermont, 
VA:Virginia, 
WA:Washington, 
WV:West Virginia, 
WI:Wisconsin, 
WY:Wyoming,
";
 
$states = explode(",",$state_list);
$state_options = "<select class=\"myInput\" id=\"State\" name=\"State\">";
$state_options .= "<option></option>";

for($i=0; $i<count($states); $i++){
	$current_state = explode(":",$states[$i]);
	if(count($current_state) > 1){
		$state_short = trim($current_state[0]);
		$state_name = trim($current_state[1]);
		if($state_short==$state){
					$selected_code = " selected";
		}//if
		
		$state_options .= "<option value=\"$state_short\"$selected_code>$state_name</option>";
		$selected_code = "";
	
	}//if
}//for

$state_options .= "</select>";

echo $state_options;
	
return;	
	
}//State()


function generateFormValidationJS($loc_id){
	/* This function generates a javascript that forces required fields to be filled in before the form is submitted*/
	
			$javascript = "<script type=\"text/javascript\">
			
			function validateForm(){
						//var thing = document.getElementById(\"First_Name\").value;
						//alert(thing);
						var formIsValid = true;
						var fillInTheseFields = \"\";
										";
			
			$requiredResult = lavu_query("SELECT * FROM config WHERE location=\"[1]\" AND setting=\"GetCustomerInfo\" AND value6=\"requiredField\"", $loc_id);
			while ($requiredRows = mysqli_fetch_assoc($requiredResult)){
						$label = $requiredRows['value'];
						$type = $requiredRows['type'];
						$formElementName = createFormElementName($label);
						
						
						if ($type == "input"){
									$javascript .= "if (document.getElementById(\"$formElementName\").value ==\"\"){
																				formIsValid = false;
																				fillInTheseFields += \"\\n $label\";
													  			}//if
													 			 ";
						}//if
						
						
						if ($type == "list_menu"){
									$javascript .= "if (document.getElementById(\"$formElementName\").value ==\"\"){
																				formIsValid = false;
																				fillInTheseFields += \"\\n $label\";
													  			}//if
													 			 ";
						}//if
						
						
						if ($type == "radio"){
									$javascript .= "if (document.getElementById(\"$formElementName\").value ==\"\"){
																				formIsValid = false;
																				fillInTheseFields += \"\\n $label\";
													  			}//if
													 			 ";
						}//if
						
						
			}//while
						
						
						
						$formElementName = createFormElementName($label);
						
		
			$javascript .= "
											if (formIsValid != true){
														alert(\"The following fields are required: \" + fillInTheseFields);
														return false;
											}//if
												}//validateForm();";
			
			
			$javascript .= "
											function getCheckedRadioValue(input){
												/* This function return the value of the selected radio button, given the name of the radio buttons. */
												for (i=0; i<=input.length; i++){
															
															var str = \"document.viewByForm.\" + input + \"[\" + i + \"].checked\";
			
															var curButton = eval(str);
						
															if (curButton == true){
																	var str2 = \"document.viewByForm.\" + input + \"[\" + i + \"].value\";
																	var curButtonVal = eval(str2);
																
																	return curButtonVal;
															}//if
												}//for
			
												/*if it gets this far, nothing was checked, a script can check for this value during form validation*/
												UhOh = \"Nothing Was Selected\";
												return UhOh;
			
											}//getCheckedRadioValue()
											";
			
			$javascript .= "</script>";
			
			echo $javascript;
			
}//generateFormValidationJS()


?>