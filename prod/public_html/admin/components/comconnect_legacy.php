<?php
require_once('/home/poslavu/public_html/admin/cp/resources/core_functions.php');

function extractCustomerIDfromModInfo($mod_info, $prefix) {

	$id = "";
	/* TODO lavu_json_decode is in core_functions */
	$mod_info_array = lavu_json_decode($mod_info, $output_steps=false);
	foreach ($mod_info_array as $mod) {
		if (!empty($mod['title'])) {
			if (strstr($mod['title'], $prefix)) {
				$info_parts = explode(" (#", $mod['title']);
				if (count($info_parts) > 1) {
					$id_parts = explode(")", $info_parts[1]);
					$id = trim($id_parts[0]);
					if (is_numeric($id) && !empty($id)) break;
					else $id = "";
				}
			}
		}
	}

	return $id;
}

