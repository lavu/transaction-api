<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<title>Untitled Document</title>

		<style type="text/css">
<!--
body {
	background-color: transparent;
	margin-left: 0px;
	margin-top: 0px;
	margin-right: 0px;
	margin-bottom: 0px;
	-webkit-tap-highlight-color:rgba(0,0,0,0);
}

.creditnumber {font-family: Verdana, Geneva, sans-serif;
	font-size: 13px;
	font-weight: bold;
	color: #3d4f6d;
}
.nametext {font-family: Verdana, Geneva, sans-serif;
	font-size: 13px;
	font-weight: bold;
}
.subtitles {font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 14px;
	font-weight: bold;
	color: #5A75A0;
}
.style411 {font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 14px;
	font-weight: bold;
	color: #5A75A0;
}
.gridposition {font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 14px;
	font-weight: bold;
	color: #5A75A0;
}
.icontext {font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 11px;
	font-weight: bold;
	color: #5A75A0;
}
.subtitles1 {font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 13px;
	font-weight: bold;
	color: #5A75A0;
}
.title1 {	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 18px;
	font-weight: bold;
	color: #5A75A0;
}
-->
		</style>
	</head>
	
<?php	
	require_once(dirname(__FILE__) . "/../comconnect.php");
	
	$mode = reqvar("mode");
	$locationid = reqvar("loc_id");
	$eventid = sessvar("eventid");
	
	$area_mode = com_memvar("area_mode","all");
	$area_mode_track = com_memvar("area_mode_track","all");
	$area_mode_room = com_memvar("area_mode_room","all");
	
	if($mode=="set_track")
	{
		$trackid = reqvar("set_trackid");
	}
	else
	{
		if(sessvar_isset("trackid"))
			$trackid = sessvar("trackid");
		else
			$trackid = false;
	}
	if($trackid)
	{
		$track_query = lavu_query("select * from `lk_tracks` where id='[1]' and `locationid`='[2]'",$trackid,$locationid);
		if(!mysqli_num_rows($track_query))
			$trackid = false;
	}
	
	if(!$trackid)
	{
		$track_query = lavu_query("select * from `lk_tracks` where `locationid`='$locationid' and `_deleted`!='1' order by id asc limit 1");
	}
	if(!mysqli_num_rows($track_query))
	{
		$track_title = "&nbsp;";
	}
	else
	{	
		$track_read = mysqli_fetch_assoc($track_query);
		$time_start = $track_read['time_start'];
		$time_end = $track_read['time_end'];
		$time_span = $track_read['time_interval'];
		$trackid = $track_read['id'];
		$track_title = $track_read['title'];
	}
	
	$year = reqvar("year");
	$month = reqvar("month");
	$day = reqvar("day");
	
	if(!$year && $eventid)
	{
		$event_query = lavu_query("select * from `lk_events` where id='[1]'",$eventid);
		if(mysqli_num_rows($event_query))
		{
			$event_read = mysqli_fetch_assoc($event_query);
			$set_date = $event_read['date'];
			$date_parts = explode("-",$set_date);
			if(count($date_parts) > 2)
			{
				$year = $date_parts[0];
				$month = $date_parts[1];
				$day = $date_parts[2];
			}
		}
	}
	if(!$year) 
	{
		$year = date("Y");
		$month = date("m");
		$day = date("d");
	}
	$date_ts = mktime(0,0,0,$month,$day,$year);
	$show_date = date("M d",$date_ts);
	$cur_date = date("Y-m-d",$date_ts);
	$cal_code = "_DO:cmd=create_overlay&f_name=lavukart/calendar_wrapper.php;lavukart/calendar.php&c_name=calendar_wrapper;calendar&dims=112,200,423,374;112,200,423,374?tab_name=new_schedule&highlight_date=".$cur_date;
?>

	<body>
		<table width="474" height="598" border="0" cellpadding="0" cellspacing="0" >
			<tr>
				<td width="107" height="536" align="left" valign="top">
					<table>
                    	<tr>
                        	<td>&nbsp;</td>
                        	<td height="52" valign="middle"><a href="<?php echo $cal_code?>"><img src="images/btn_calendar.png" width="34" height="34" border="0" /></a></td>
							<td class="subtitles1" valign="middle" onClick="window.location = '<?php echo $cal_code?>'"><?php echo "<b>".$show_date."</b>"?></td>
                        </tr>
                    </table>
                    <!--
					<table width="68" border="0" cellpadding="0" cellspacing="0">
						<tr><td height="120" align="center" valign="middle" class="subtitles">&nbsp;</td></tr>
						<tr><td height="32" align="center" valign="middle" class="subtitles"><a href="_DO:cmd=create_overlay&f_name=lavukart/calendar_wrapper.php;lavukart/calendar.php&c_name=calendar_wrapper;calendar&dims=112,200,423,374;112,200,423,374"><img src="images/btn_calendar.png" width="68" height="68" border="0"/></a></td></tr>
						<tr><td height="32" align="center" valign="middle"><span class="icontext">CALENDAR</span></span></td></tr>
						<tr><td height="20" align="center" valign="middle" class="subtitles">&nbsp;</td></tr>
						<tr><td height="32" align="center" valign="middle" class="subtitles"><a href="_DO:cmd=create_overlay&f_name=lavukart/newevent_wrapper.php;lavukart/select_track.php&c_name=select_track_wrapper;select_track&dims=112,200,423,374;112,200,423,374?locid=<?php echo $_REQUEST['loc_id']; ?>"><img src="images/btn_select_track.png" width="68" height="68" border="0"/></a></td></tr>
						<tr><td height="32" align="center" valign="middle"><span class="icontext">TRACKS</span></span></td></tr>
					</table>
                    -->
                    &nbsp;
				</td>
                <td width='260' align='left' valign='top'>
                	<table>
                        <tr>
                        	<td width='12'>&nbsp;</td>
                            <?php
								function output_abbrev_cell($track_read,$tracktype="")
								{
									if(isset($track_read['abbreviation']) && $track_read['abbreviation']!="")
										$abbrev = $track_read['abbreviation'];
									else
									{
										$abbrev = ucfirst(substr($track_read['title'],0,3));
									}
									$onclick = "window.location = '_DO:cmd=load_url&c_name=schedule;schedule_wrapper&f_name=lavukart/schedule.php;lavukart/schedule_wrapper.php?area_mode=".$tracktype."&area_mode_".$tracktype."=".$track_read['id']."'";
									return "<td width='48' height='52' align='center' valign='middle' class='subtitles1' onclick=\"$onclick\">$abbrev</td>";
								}
								
								if($area_mode=="all" || $area_mode=="track")
								{
									$track_query = lavu_query("select * from `lk_tracks` where `locationid`='$locationid' and `_deleted`!='1' order by id asc");
									while($track_read = mysqli_fetch_assoc($track_query))
									{
										if($area_mode_track=="all" || $area_mode_track==$track_read['id'])
											echo output_abbrev_cell($track_read,"track");
									}
								}
								if($area_mode=="all" || $area_mode=="room")
								{
									$room_query = lavu_query("select * from `lk_rooms` where `locationid`='$locationid' and `_deleted`!='1' order by id asc");
									while($room_read = mysqli_fetch_assoc($room_query))
									{
										if($area_mode_room=="all" || $area_mode_room==$room_read['id'])
											echo output_abbrev_cell($room_read,"room");
									}
								}
								if($area_mode!="all")
								{
									$onclick = "window.location = '_DO:cmd=load_url&c_name=schedule_wrapper;schedule&f_name=lavukart/schedule_wrapper.php;lavukart/schedule.php?area_mode=all&area_mode_track=all&area_mode_room=all'";
									$abbrev = "View All";
									echo "<td width='120' height='52' align='center' valign='middle' class='subtitles1' onclick=\"$onclick\">$abbrev</td>";
								}
							?>
                            
                            <!--<td width='48' align='center' valign='middle' class='subtitles1'>Tr2</td>
                            <td width='48' align='center' valign='middle' class='subtitles1'>Rm1</td>
                            <td width='48' align='center' valign='middle' class='subtitles1'>Rm2</td>-->
                        </tr>
                    </table>
                </td>
				<td align="left" valign="top" width="105">
					<!--<table width="395" height="598" border="0" cellspacing="0" cellpadding="0">
						<tr>
							<td height="40">
								<table width="392" border="0" cellspacing="0" cellpadding="0">
									<tr>
										<td width="119" height="46">
											<table width="119" border="0" cellspacing="0" cellpadding="8">
												<tr><td width="119" class="title1" onClick="window.location = '<?php echo $cal_code?>'"><?php echo "<b>".$show_date."</b>"?></td></tr>
											</table>
										</td>
										<td width="233" >
											<table width="233" border="0" cellpadding="8" cellspacing="0">
												<tr><td align="center" class="title1"><font color="#4A5580"><?php echo "<b>".$track_title."</b>"?></font></td></tr>
											</table>
										</td>
										<td width="80">&nbsp;</td>
									</tr>
								</table>
							</td>
						</tr>
						<tr>
							<td height="535" align="left" valign="top">
								<table cellspacing="0" cellpadding="0">
									<tr>
										<td></td>
									</tr>
								</table>
							</td>
						</tr>
						<tr><td height="23" background="images/line_bot.png"></td></tr>
					</table>-->
				</td>
			</tr>
		</table>
	</body>
</html>