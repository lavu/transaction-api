// JavaScript Document

/*
*	This file is used bu parts_info.php and parts_addPart.php
*
*/


function validateAddForm(){
		
		/* if they don't enter anything the formating in ensureZeroInFrontOfDecimal() enters 0.00 and the forms submits successfully, this is bad cause they might think they entered a value.
				in short, if they did not enter a value for the CostPerPart field, do not allow the formatting to execute
		 */
		
		if (document.service_types_addServiceTypeForm.manHours.value != ""){
					ensureZeroInFrontOfDecimal();//format at very beginning
		}//if
		
		
		
		name = document.service_types_addServiceTypeForm.name.value;
		description = document.service_types_addServiceTypeForm.Description.value;
		parts = document.service_types_addServiceTypeForm.parts.value;
		manHours = document.service_types_addServiceTypeForm.manHours.value;
		
		
		
		var canSubmitForm = true;
		
		if (name == ""){
					canSubmitForm = false;
		}//if
		
		if (description == ""){
					canSubmitForm = false;
		}//if
		
		if (parts == ""){
					canSubmitForm = false;
		}//if
		
		if (manHours == ""){
					canSubmitForm = false;
		}//if
		
		
		
		if (canSubmitForm == false){
					window.location = '_DO:cmd=alert&title=Missing Values&message=Please fill in all fields.';
					return false;
		}//if
		
		
		
		
		return true;
		
}//validateAddForm()


function ensureZeroInFrontOfDecimal(){
		/* want two decimal places for any money entered into database */
		 	
				var val = document.service_types_addServiceTypeForm.manHours.value; //get the new value
				
				//force two decimals
				val = parseFloat(val);
				val = val.toFixed(2);
				document.service_types_addServiceTypeForm.manHours.value = val;
				
				
	}//ensureZeroInFrontOfDecimal()




