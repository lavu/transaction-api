<?php
	session_start();
	require_once(dirname(__FILE__) . "/../comconnect.php");
	$locationid = sessvar("loc_id");
	
	/* This file show the history of the parts that were added to the selected kart */
	
	
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title></title>


<style type="text/css">

.row{
		width:600px;
		font-family:Verdana, Geneva, sans-serif;
	color:#333;
	
	font-size:10pt;
		border:0px #666666 solid;
		 
}

.rowElement{
	float:left;
}

div{
	font-size:10pt;
}

.re1{
	
	width:100px;
	text-align:center;
	/*background-color:#FF3;*/
}

.re2{
	padding-left:2px;
	width:80px;
	text-align:left;
	/*background-color:#00CC66;*/
}

.re3{
	/*background-color:#690;*/
	width:40px;
	text-align:center;
}

.re4{
	/*background-color:#9FF;*/
	width:180px;
	
}


</style>


<style type="text/css">
.centerCell{
	width:412px;
	
	background-image:url(images/tab_v1_mid.png);
	background-repeat:repeat-y;
	
	height:auto;
	
}



.topCap{
	background-image:url(images/tab_v1_top.png);
	background-repeat:no-repeat;
	height:9px;
}


.bottomCap{
	background-image:url(images/tab_v1_bot.png);
	background-repeat:no-repeat;
	height:9px;
}

.header{
	color:#5a75a0;
	font-weight:700;
}


</style>


</head>

<body>

<?php
$kart_id = reqvar("kart_id");//$_SESSION['kartID'];

//echo "kart_id: $kart_id<br/>";
$res1 = lavu_query("SELECT * FROM lk_service WHERE locationid=\"[1]\" AND kart_id=\"[2]\" ORDER BY datetime", $locationid,$kart_id);
echo "<div class=\"row header\">";
				echo "<div class=\"rowElement re1\">Date</div>";
				echo "<div class=\"rowElement re2\" style='text-indent:15px;'>Part</div>";
				echo "<div class=\"rowElement re3\">Qty</div>";
				echo "<div class=\"rowElement re4\" style=\"text-align:center;\">Desc.</div>";
					echo "<div style=\"clear:both;\"></div>";
echo "</div>";
	echo "<div style=\"clear:both;\"></div>";
while ($row1 = mysqli_fetch_assoc($res1)){
			$date = $row1['datetime'];
			$dateArr = explode(" ",$date);
			$dateDisp= $dateArr[0];
			$dateDispArr = explode("-",$dateDisp);
			$dateYear = $dateDispArr[0];
			$dateMonth = $dateDispArr[1];
			$dateDay = $dateDispArr[2];
			$dateDisp = $dateMonth."-".$dateDay."-".$dateYear;
			
			$partNumber = $row1['custom_part_id'];
			$partName = $row1['part_name'];
			$quantity = $row1['quantity'];
			$description = $row1['description'];
			
			echo "<div class=\"row\">";
					echo "<div class='topCap'></div>";
					echo "<div class='centerCell'>";
							echo "<div class=\"rowElement re1\" style=\"font-weight:600;\">$dateDisp</div>";
							echo "<div class=\"rowElement re2\">$partNumber</div>";
							echo "<div class=\"rowElement re3\">$quantity</div>";
							echo "<div class=\"rowElement re4\">$partName</div>";
							echo "<div style=\"clear:both\"></div>";
					echo "</div><!--centerCell-->";
					echo "<div class='bottomCap'></div>";
			echo "</div><!--row-->";
			echo "<div style=\"clear:both;\"></div>";
}//while
?>


</body>
</html>