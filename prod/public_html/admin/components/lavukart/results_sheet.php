<?php
	$use_new_sheet = true;
	//if(strpos($company_code,"jersey"))
		//$use_new_sheet = true;
		
	if(isset($trackid))
	{
		$track_query = lavu_query("select * from `lk_tracks` where `id`='[1]'",$trackid);
		if(mysqli_num_rows($track_query))
		{
			$track_read = mysqli_fetch_assoc($track_query);
			$track_name = $track_read['title'];
			$mult_percent = $track_read['print_ratio'];
			if($mult_percent=="old") $use_new_sheet = false;
		}
	}
		
	if($use_new_sheet)//isset($_GET['test']) && $_GET['test']==1)
	{
		if(isset($_GET['test']) && is_numeric($_GET['test']))
		{
			$display_multiplier = $_GET['test'];
			$lookup_multiplier = false;
		}
		else
		{
			$display_multiplier = 1;
			$lookup_multiplier = true;
		}
		if(isset($_GET['eventid']))
		{
			$event_query = lavu_query("select * from `lk_events` where `id`='[1]'",$_GET['eventid']);
			if(mysqli_num_rows($event_query))
			{
				$event_read = mysqli_fetch_assoc($event_query);
				$trackid = $event_read['trackid'];
				$_GET['trackid'] = $trackid;
			}
		}
		if(isset($trackid))
		{
			$track_query = lavu_query("select * from `lk_tracks` where `id`='[1]'",$trackid);
			if(mysqli_num_rows($track_query))
			{
				$track_read = mysqli_fetch_assoc($track_query);
				$track_name = $track_read['title'];
				$mult_percent = $track_read['print_ratio'];
				$mult_percent = trim(str_replace("%","",$mult_percent));
				if(is_numeric($mult_percent) && $lookup_multiplier)
				{
					$display_multiplier = $mult_percent / 100;
					if(isset($_GET['test2'])) echo "multiplier: " . $display_multiplier . " percentage: " . $mult_percent;
				}
			}
		}
	
		function mult_css_properties($code,$tags,$mult)
		{
			return mult_tag_properties($code,$tags,$mult,":");
		}
		
		function mult_tag_properties($code,$tags,$mult,$sep="=")
		{
			$tags = explode(",",$tags);
			for($t=0; $t<count($tags); $t++)
			{
				$tag = trim($tags[$t]);
				if($tag!="")
				{
					$code = str_replace($tag . " ".$sep,$tag . $sep,$code);
					$code_parts = explode($tag . $sep,$code);
					$setcode = $code_parts[0];
					for($i=1; $i<count($code_parts); $i++)
					{
						$str = $code_parts[$i];
						$append = $str;
						if(strlen($str) > 0)
						{
							$firstchar = substr(trim($str),0,1);
							if($firstchar=="'" || $firstchar=='"')
							{
								$str_parts = explode($firstchar,$str);
								$setnum = trim($str_parts[1]);
								if(is_numeric($setnum))
								{
									$append = $str_parts[0] . $firstchar . ($setnum * $mult) . $fistchar;
									for($n=2;$n<count($str_parts);$n++)
									{
										$append .= $firstchar . $str_parts[$n];
									}
								}
							}
							else if(is_numeric($firstchar))
							{
								$x=0;
								$setnum = "";
								$ws = "";
								while($x<strlen($str) && (is_numeric($str[$x]) || ($str[$x]==" " && trim($setnum==""))))
								{
									if($str[$x]==" ") $ws .= " ";
									$setnum .= $str[$x];
									$x++;
								}
								if(is_numeric(trim($setnum)))
								{
									$append = $ws . (trim($setnum) * $mult) . substr($str,$x);
								}
							}
						}
						$setcode .= $tag . $sep . $append;
					}
					$code = $setcode;
				}
			}
			$code = str_replace("ratio=R","ratio=".$mult,$code);
			return $code;
		}
	
		$fp = fopen("results_sheet_template2.php","r");
		$sheet_template = fread($fp,filesize("results_sheet_template2.php"));
		fclose($fp);
		
		$sheet_parts = explode("<!--startsheet-->",$sheet_template);
		if(count($sheet_parts) > 1)
		{
			$sheet_start = $sheet_parts[0];
			$sheet_parts = explode("<!--endsheet-->",$sheet_parts[1]);
			$sheet_mid = $sheet_parts[0];
			if(count($sheet_parts) > 1)
			{
				$sheet_end = $sheet_parts[1];
			}
			else $sheet_end = "";
		}
		else
		{
			$sheet_start = "";
			$sheet_mid = $sheet_parts[0];
			$sheet_end = "";
			echo "Invalid sheet type";
			exit();
		}
		$sheet_template = $sheet_mid;
		
		$display_racers = array();
		foreach($all_laps as $schedid => $laps)
		{
			$rkart = $racer_info[$schedid]['kartid'];
			$rplace = $rinfo_pos[$rkart];
			$display_racers[$rplace] = array($racer_info[$schedid]['name'],$racer_info[$schedid]['bestlap']);
			//$all_best_times .= "<nobr>".$racer_info[$schedid]['name'] . ": " . $racer_info[$schedid]['bestlap'] . "</nobr><br>";
		}
		
		$best_lap_ofweek = array(array(0,0));
		$week_ago = date("Y-m-d",mktime(0,0,0,date("m"),date("d")-7,date("Y")));
		$today = date("Y-m-d");
		$best_query = lavu_query("select * from `lk_race_results` where `date`>='[1]' and `date`<='$today' and `trackid`='[2]' and `bestlap`>10 order by (`bestlap` * 1) asc limit 25",$week_ago,$trackid); // bestlap must be greater than 10 seconds
		$best_count = 0;
		while($best_read = mysqli_fetch_assoc($best_query))
		{
			$cust_query = lavu_query("select * from `lk_customers` where `id`='[1]'",$best_read['customerid']);
			if(mysqli_num_rows($cust_query))
			{
				$best_count++;
				$cust_read = mysqli_fetch_assoc($cust_query);
				$best_lap_ofweek[] = array(trim($cust_read['f_name'] . " " . $cust_read['l_name']),$best_read['bestlap']);
			}
		}
		
		echo mult_css_properties($sheet_start,"font-size",$display_multiplier);
		//echo $sheet_start;
		$scount = 0;
		//foreach($all_laps as $schedid => $laps)
		//{
			//$sched_query = lavu_query("select * from `lk_event_schedules` LEFT JOIN `lk_customers` ON `lk_event_schedules`.`customerid`=`lk_customers`.`id` LEFT JOIN `lk_events` ON `lk_event_schedules`.`eventid`=`lk_events`.`id` where `lk_event_schedules`.`id`='[1]'",$schedid);
		foreach($ordered_racers as $rinfo)
		{
			$scount++;
			$schedid = $rinfo['schedid'];
			$sched_query = lavu_query("select * from `lk_event_schedules` LEFT JOIN `lk_customers` ON `lk_event_schedules`.`customerid`=`lk_customers`.`id` LEFT JOIN `lk_events` ON `lk_event_schedules`.`eventid`=`lk_events`.`id` where `lk_event_schedules`.`id`='[1]'",$schedid);
			if(mysqli_num_rows($sched_query))
			{
				$sched_read = mysqli_fetch_assoc($sched_query);
				$sheet = $sheet_template;
				
				$rr_query = lavu_query("select * from `lk_race_results` where `eventid`='[1]' and `customerid`='[2]'",$sched_read['eventid'],$sched_read['customerid']);
				if(mysqli_num_rows($rr_query))
				{
					$rr_read = mysqli_fetch_assoc($rr_query);
					$set_rank = $rr_read['ranking'];
				}
				
				$laps = $all_laps[$schedid];
				$placed_pos = $rinfo_pos[$sched_read['kartid']];
				
				if($placed_pos > 10 && $placed_pos < 20) $att = "th";
				else if($placed_pos % 10 == 1) $att = "st";
				else if($placed_pos % 10 == 2) $att = "nd";
				else if($placed_pos % 10 == 3) $att = "rd";
				else $att = "th";
				
				$lap_times = "";
				for($n=0; $n<count($laps); $n++)
				{
					if(isset($laps[$n]))
					{
						$lap_times .= "<nobr>".($n + 1) . ": " . $laps[$n] . "</nobr><br>";
					}
				}
				
				$edate_parts = explode("-",$event_date);
				$show_event_date = $edate_parts[1] . "/" . $edate_parts[2] . "/" . $edate_parts[0];
				
				$show_rank = "";
				$show_tip = "";
				if(isset($set_rank))
				{
					if($set_rank==1) 
					{
						$show_rank = "Amateur";
						$show_tip = "Follow a faster driver to learn their race lines, braking points and overall driving technique. Maintaining momentum is the key to going faster.";
					}
					else if($set_rank==2) 
					{
						$show_rank = "Intermediate";
						$show_tip = "Consider being smoother with your steering. Try not to over steer in the corners or make sharp, abrupt turns. Try to also be smoother on the brakes.";
					}
					else if($set_rank==3) 
					{
						$show_rank = "Expert";
						$show_tip = "Consider using less brakes and trying to get back into the throttle earlier while simultaneously trying to minimize sliding too much through turns.";
					}
				}
				$visit_query = lavu_query("select distinct `date` from `lk_race_results` where `customerid`='[1]'",$sched_read['customerid']);
				$show_visits = mysqli_num_rows($visit_query);
				$total_races_query = lavu_query("select * from `lk_race_results` where `customerid`='[1]'",$sched_read['customerid']);
				$show_total_races = mysqli_num_rows($total_races_query);
				$exp_query = lavu_query("select * from `lk_race_results` where `customerid`='[1]' and `ranking`='3'",$sched_read['customerid']);
				$show_exp_count = mysqli_num_rows($exp_query);
				$int_query = lavu_query("select * from `lk_race_results` where `customerid`='[1]' and `ranking`='2'",$sched_read['customerid']);
				$show_int_count = mysqli_num_rows($int_query);
				$ama_query = lavu_query("select * from `lk_race_results` where `customerid`='[1]' and `ranking`='1'",$sched_read['customerid']);
				$show_ama_count = mysqli_num_rows($ama_query);
												
				$tr = array();
				$tr[] = array("fullname",trim($sched_read['f_name'] . " " . $sched_read['l_name']));
				$tr[] = array("racer name",$sched_read['racer_name']);						
				$tr[] = array("race name",$event_type_title);
				$tr[] = array("race format",$event_race_format);
				$tr[] = array("date",$show_event_date);
				$tr[] = array("visits",$show_visits);
				$tr[] = array("rank",$show_rank);
				$tr[] = array("track name",$track_name);
				$tr[] = array("all best times",$all_best_times);
				$tr[] = array("lap times",$lap_times);
				$tr[] = array("placed",$placed_pos);
				$tr[] = array("race date",display_date($sched_read['date']));
				
				$tr[] = array("tip","To improve you should...
$show_tip ");
				$tr[] = array("chart","http://admin.poslavu.com/components/lavukart/race_scores.php?mode=results_graph&cc=".$company_code . "_key_".$company_code_key."&loc_id=".$_GET['loc_id']."&trackid=".$_GET['trackid']."&eventid=".$_GET['eventid']."&showsched=$schedid");
				$tr[] = array("chart_new","http://admin.poslavu.com/components/lavukart/race_scores.php?mode=results_graph&cc=".$company_code . "_key_".$company_code_key."&loc_id=".$_GET['loc_id']."&trackid=".$_GET['trackid']."&eventid=".$_GET['eventid']."&showsched=$schedid&graphtype=new");
				
				$tr[] = array("exp_count",$show_exp_count);
				$tr[] = array("int_count",$show_int_count);
				$tr[] = array("ama_count",$show_ama_count);
				$tr[] = array("total_races",$show_total_races);
				
				for($n=1; $n<=14; $n++)
				{
					if(isset($display_racers[$n]) && is_array($display_racers[$n]))
					{
						$tr[] = array("racer".$n,$display_racers[$n][0]);
						$tr[] = array("rtime".$n,$display_racers[$n][1]);
						$tr[] = array("p".$n,$n);
					}
					else
					{
						$tr[] = array("racer".$n,"&nbsp;");
						$tr[] = array("rtime".$n,"&nbsp;");
						$tr[] = array("p".$n,"");
					}
				}
				for($n=1; $n<=5; $n++)
				{
					if(isset($best_lap_ofweek[$n]) && is_array($best_lap_ofweek[$n]))
					{
						$tr[] = array("blracer".$n,$best_lap_ofweek[$n][0]);
						$tr[] = array("bltime".$n,$best_lap_ofweek[$n][1]);
						$tr[] = array("bl".$n,$n);
					}
					else
					{
						$tr[] = array("blracer".$n,"&nbsp;");
						$tr[] = array("bltime".$n,"&nbsp;");
						$tr[] = array("bl".$n,"");
					}
				}
				$cups = array(0,4,4,4,4);
				for($n=1; $n<=4; $n++)
				{
					for($m=1; $m<=5; $m++)
					{
						if($cups[$n]>=$m)
							$tr[] = array("t".$n."_".$m,"<img src=\"images/resultsheet/cup.png\" width=\"20\" height=\"18\">");
						else
							$tr[] = array("t".$n."_".$m,"&nbsp;");
					}
				}
				for($n=0; $n<count($tr); $n++)
				{
					$sheet = str_replace("[".$tr[$n][0]."]",$tr[$n][1],$sheet);
				}
				
				if($scount < count($ordered_racers)) echo "<div style='page-break-after:always'>"; else echo "<div>";
				//echo $sheet;
				echo mult_tag_properties($sheet,"width,height",$display_multiplier);
				echo "</div>";
			}
		}		
		echo $sheet_end;
	}
	else
	{
		$fp = fopen("results_sheet_template.php","r");
		$sheet_template = fread($fp,filesize("results_sheet_template.php"));
		fclose($fp);
	
		$all_best_times = "";
		foreach($all_laps as $schedid => $laps)
		{
			$all_best_times .= "<nobr>".$racer_info[$schedid]['name'] . ": " . $racer_info[$schedid]['bestlap'] . "</nobr><br>";
		}
		
		$scount = 0;
		foreach($all_laps as $schedid => $laps)
		{
			$scount++;
			$sched_query = lavu_query("select * from `lk_event_schedules` LEFT JOIN `lk_customers` ON `lk_event_schedules`.`customerid`=`lk_customers`.`id` LEFT JOIN `lk_events` ON `lk_event_schedules`.`eventid`=`lk_events`.`id` where `lk_event_schedules`.`id`='[1]'",$schedid);
			if(mysqli_num_rows($sched_query))
			{
				$sched_read = mysqli_fetch_assoc($sched_query);
				$sheet = $sheet_template;
				
				$placed_pos = $rinfo_pos[$sched_read['kartid']];
				
				if($placed_pos > 10 && $placed_pos < 20) $att = "th";
				else if($placed_pos % 10 == 1) $att = "st";
				else if($placed_pos % 10 == 2) $att = "nd";
				else if($placed_pos % 10 == 3) $att = "rd";
				else $att = "th";
				
				$lap_times = "";
				for($n=0; $n<count($laps); $n++)
				{
					if(isset($laps[$n]))
					{
						$lap_times .= "<nobr>".($n + 1) . ": " . $laps[$n] . "</nobr><br>";
					}
				}
				
				$tr = array();
				$tr[] = array("fullname",trim($sched_read['f_name'] . " " . $sched_read['l_name']));
				$tr[] = array("racer name",$sched_read['racer_name']);						
				$tr[] = array("all best times",$all_best_times);
				$tr[] = array("lap times",$lap_times);
				$tr[] = array("placed",$placed_pos . $att);
				$tr[] = array("race date",display_date($sched_read['date']));
				$tr[] = array("chart","http://admin.poslavu.com/components/lavukart/race_scores.php?mode=results_graph&cc=".$company_code . "_key_".$company_code_key."&loc_id=".$_GET['loc_id']."&trackid=".$_GET['trackid']."&eventid=".$_GET['eventid']."&showsched=$schedid");
				
				for($n=0; $n<count($tr); $n++)
				{
					$sheet = str_replace("[".$tr[$n][0]."]",$tr[$n][1],$sheet);
				}
				
				if($scount > 1) echo "<div style='page-break-after:always'></div>";
				echo $sheet;
			}
		}
	}	
?>