<?php
			session_start();
			require_once(dirname(__FILE__) . "/../comconnect.php");
			require_once(dirname(__FILE__) . "/./part_functions.php");
			
			if ($access_level < 3){
					echo "<script type='text/javascript'>
						alert('You do not have sufficient priveleges to access this tab.');
					</script>";
					exit();
			}//if
			
?>

<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<title>Garage List</title>

		<style type="text/css">
<!--
body {
	background-color: transparent;
	
	margin-left: 0px;
	margin-top: 0px;
	margin-right: 0px;
	margin-bottom: 0px;
	-webkit-tap-highlight-color:rgba(0,0,0,0);
	font-family:Verdana, Geneva, sans-serif;
	color:#333;
}


table, th 
{
border: none;
border-collapse:collapse;
/*table-layout:fixed;*/
padding:0;
width:412px;

font-size:10pt;

margin:0;


}

td{
	border:0px solid #033;
	border-collapse:collapse;
	padding:0;
	height:auto;
	font-size:10pt;
	margin:0;
}


tr{
			height:auto;
			width:412px;
			
		
}

div{
		font-size:10pt;
}


.trCaps{
	height:9px; 
	width:412px;
	
}

.centerCell{
	width:412px;
	
	background-image:url(images/tab_v1_mid.png);
	background-repeat:repeat-y;
	
	height:auto;
	
}

.topCap{
	background-image:url(images/tab_v1_top_612.png);
	background-repeat:no-repeat;
	height:9px;
}


.bottomCap{
	background-image:url(images/tab_v1_bot_612.png);
	background-repeat:no-repeat;
	height:9px;
}



-->
		</style>
		

<script type="text/javascript">
	


	function changeBackgroundImage(id){
			/* function changes background image to 'lit' or 'unlit' depending on what is appropriate, the id input parameter is the id assigned to each <table></table> element. it is the same as the part id since all parts have unique 	          IDs*/
			
			var lastRowLit = document.getElementById('lastTRLit').value;
			
			/* If a row is lit this hidden field will contain a value, the value is the id of the last selected <tr></tr> */
			if (lastRowLit != ""){
					
					/* change back to 'unlit' image */
						var topCapID = "topCap" + lastRowLit;
					
						document.getElementById(topCapID).style.backgroundImage="url('images/tab_v1_top_612.png')";
			
						document.getElementById(lastRowLit).style.backgroundImage="url('images/tab_v1_mid_612.png')";
			
						var botCapID = "botCap" + lastRowLit;
						document.getElementById(botCapID).style.backgroundImage="url('images/tab_v1_bot_612.png')";
		 
			}//if
			
			/* Change background image of current selected table row to the 'lit' image */
			var topCapID = "topCap" + id;
			document.getElementById(topCapID).style.backgroundImage="url('images/tab_v1_toplit_612.png')";
			
			document.getElementById(id).style.backgroundImage="url('images/tab_v1_midlit_612.png')";
			
			var botCapID = "botCap" + id;
			document.getElementById(botCapID).style.backgroundImage="url('images/tab_v1_botlit_612.png')";
		
			/* store id in hidden text field so we can switch that row off later */
			document.getElementById('lastTRLit').value = id; 
			
	}//changeBackgroundImage()
	
	function passDataToGarageInfo(category){
		//alert(category);
				window.location = "_DO:cmd=load_url&c_name=parts_info&f_name=lavukart/parts_info.php?category=" + category + "&loadRightSide=true";
	}//passDataToGarageInfo()

</script>		

	</head>
	
	<body>
	
	
	
	
	
	
	<form action="" method="post" onSubmit="" name="garageListForm" id="frm1">
	
	
	
		<?php
		
	
		
		
				echo "<input type=\"hidden\" name=\"lastTRLit\" id=\"lastTRLit\">"; /*Store the id of the last <tr></tr> element selected so we can change the background image back to unlit */
				
		?>
		
	<?php
		/*
		
	*/
	?>
				
							
		<?php
				
				$locationid = sessvar("loc_id");
	
	
				
				/* This code was written to clean up parts list data which currently has a bunch of unwanted characters. The entry in the config database is inserted and it's presence keeps this code from ever running more than once. To run the code again, delete the entry in the config table and click the 'Parts' tab in the app */
$checkUpDateResult = lavu_query("SELECT * FROM config WHERE location=\"[1]\" AND type=\"CleanDBData\" AND setting=\"parts_list\"",$locationid);
$checkUpDateRows = mysqli_fetch_assoc($checkUpDateResult);

if (!isset($checkUpDateRows['id'])){

			lavu_query("INSERT INTO config (location,type,setting) VALUES(\"[1]\", \"CleanDBData\", \"parts_list\")",$locationid);
			$sanitizeDBResults = lavu_query("SELECT * FROM lk_parts WHERE locationid=\"[1]\"",$locationid);
				
			while ($cleanDB = mysqli_fetch_assoc($sanitizeDBResults)){
					//clean database of problematic characters
					$name = clean($cleanDB['name']);
					$description = clean($cleanDB['description']);
					$qty = clean($cleanDB['qty']);
					$cost = clean($cleanDB['cost']);
					$id = clean($cleanDB['id']);
					$cleanCategory = clean($cleanDB['category']);		
					$custom_part_id = clean($cleanDB['custom_part_id']);
					
					if ($custom_part_id == ""){
							lavu_query("UPDATE lk_parts SET custom_part_id='[1]' WHERE id='[2]' AND locationid='[3]'",$id,$id,$locationid);
					}//if
					
				lavu_query("UPDATE lk_parts SET name=\"[1]\", description=\"[2]\", qty=\"[3]\", cost=\"[4]\" WHERE id=\"[5]\" AND locationid=\"[6]\"",$name,$description,$qty,$cost,$id,$locationid);
				
				lavu_query("UPDATE lk_parts SET category=\"[1]\" WHERE id=\"[2]\" AND locationid=\"[3]\"",$cleanCategory,$id,$locationid);
							
			}//while
}//if


/* This script runs once (ever), and builds the categories list in the lk_categories and config table. The entry input into the config table keeps this script from running again */
/* UPDATE: The entry in the config table now only serves to keep this code from running. The categories list is now stored in lk_categories */
//   NO LONGER NEED CATEGORIES
$checkCategoryPresentResult = lavu_query("SELECT * FROM config WHERE location=\"[1]\" AND type=\"CategoryListIsSet\"",$locationid);
	$checkCategoryPresentRows = mysqli_fetch_assoc($checkCategoryPresentResult);
	
	if (!isset($checkCategoryPresentRows['id'])){
				lavu_query("DELETE FROM lk_categories WHERE locationid='[1]'",$locationid);
				$results = lavu_query("SELECT DISTINCT category FROM lk_parts WHERE locationid=\"[1]\"", $locationid);
				$curVal;
		
				while($getCatRow = mysqli_fetch_assoc($results)){
							$curVal .= clean($getCatRow['category']) . ",";
							$goesInto_lk_categories = clean($getCatRow['category']);
							lavu_query("INSERT INTO lk_categories (Category, locationid) VALUES ('[1]','[2]')",$goesInto_lk_categories,$locationid);
				}//while
				
				//$curVal .= "All Parts";
				lavu_query("INSERT INTO lk_categories (Category, locationid) VALUES ('All Parts','[1]')",$locationid);
				
			lavu_query("INSERT INTO config (location,type,setting) VALUES(\"[1]\", \"CategoryListIsSet\", \"categories\")",$locationid);
			lavu_query("UPDATE config set value3=\"$curVal\" WHERE location=\"[1]\" AND setting=\"categories\"",$locationid);
		
	}//if

				
				
				
			/*	
				$categoryResults = lavu_query("SELECT * FROM config WHERE location=\"[1]\" AND setting=\"categories\"", $locationid);
				$categoryRows = mysqli_fetch_assoc($categoryResults);
				$categoriesString = $categoryRows['value3'];
				$categoriesArray = explode(",",$categoriesString);
				sort($categoriesArray);
				trim($categoriesArray);
				$lastLetter = chr(96);// !?
				*/
				$resGetCategories = lavu_query("SELECT * FROM lk_categories WHERE locationid='[1]' ORDER BY Category",$locationid);
				$categoriesArray = array();
				$catIndex = 0;
				while ($rowGetCategory = mysqli_fetch_assoc($resGetCategories)){
							$categoriesArray[$catIndex] = trim($rowGetCategory['Category']);
							$catIndex++;
				}//while
				echo "<a name=\"Number\"></a>";
			$categoriesArray[0] = "All Parts by Name";
			$categoriesArray[1] = "All Parts by ID";
			/*	
				echo "<div class=\"topCap\" id=\"topCapAllParts\"></div>";
				echo "<div id=\"AllParts\" onclick=\"passDataToGarageInfo('AllParts'); changeBackgroundImage('AllParts');\" style=\"background-image:url(images/tab_v1_mid_612.png); background-repeat:repeat-y; width:612px; height:auto; \">";	
				echo "<div style=\"width:240px; padding-left:5px; float:left; font-weight:700;\">All Parts</div>";
				echo "<div style=\"clear:both;\"></div>";
				echo "</div>";
				echo "<div class=\"bottomCap\" id=\"botCapAllParts\"></div>";
			*/
			
			
				
				for ($i=0;$i<count($categoriesArray);$i++){
				
				
				
			
				
		
						$category = $categoriesArray[$i];
						$id = $category;
					
		
						$topCapID = "topCap" . $id;
						$botCapID = "botCap" . $id;
						
						echo "<div class=\"topCap\" id=\"$topCapID\"></div>";

						
							
						echo "<div id=\"$id\" onclick=\"passDataToGarageInfo('$id'); changeBackgroundImage('$id');\" style=\"background-image:url(images/tab_v1_mid_612.png); background-repeat:repeat-y; width:612px; height:auto; \">";	
					
						
							
							
							
							if ( (isAlphaCharacter($category{0})) && (strtolower($lastLetter) != strtolower($category{0}))){
									$lastLetter = insertAnchor($category,$lastLetter);
							}//if
							
							echo "<div style=\"width:240px; padding-left:5px; float:left; font-weight:700;\">" . $category . "&nbsp;</div>";
			
							echo "<div style=\"clear:both;\"></div>";
							echo "</div>";
					
						echo "<div class=\"bottomCap\" id=\"$botCapID\"></div>";
				}//for
				
		
			
		?>
							
				
		
	</form>
		
		
		
	<script type="text/javascript">window.location = "parts_list.php#b";</script>
	
</body>
</html>
<?php



?>