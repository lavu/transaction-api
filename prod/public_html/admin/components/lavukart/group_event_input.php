<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<title>Group Event Input</title>

		<style type="text/css">
<!--

body {
	background-color: transparent;
	margin-left: 0px;
	margin-top: 0px;
	margin-right: 0px;
	margin-bottom: 0px;
	-webkit-tap-highlight-color:rgba(0,0,0,0);
}

.style4 {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 14px;
	font-weight: bold;
	color: #8896AD;
}
.style6 {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 24px;
	font-weight: bold;
	color: #8896AD;
}
.style8 {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 15px;
	font-weight: bold;
	color: #07142a;
}
.creditnumber {
	font-family: Verdana, Geneva, sans-serif;
	font-size: 13px;
	font-weight: bold;
	color: #3d4f6d;
}
.nametext {
	font-family: Verdana, Geneva, sans-serif;
	font-size: 13px;
	font-weight: bold;
}
.nametext_grey {
	font-family: Verdana, Geneva, sans-serif;
	font-size: 13px;
	font-weight: bold;
	color: #777777;
}
.subtitles {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 14px;
	font-weight: bold;
	color: #5A75A0;
}
.info_text1 {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 16px;
	color: #233756;
}

-->
		</style>
		<script language="javascript">
			var event_date = "";
			var event_time = "";
		</script>
	</head>

<?php 
	require_once(dirname(__FILE__) . "/../comconnect.php"); 
	$cm = reqvar("cm");
	$tab_name = reqvar("tab_name");
	$mode= reqvar("mode");
	
	$loc_id = 0;
	if(reqvar("loc_id")) $loc_id = reqvar("loc_id");
	else if(sessvar_isset("loc_id")) $loc_id = sessvar("loc_id");
	set_sessvar("loc_id", $loc_id);
	
	$server_id = 0;
	if(reqvar("server_id")) $server_id = reqvar("server_id");
	else if(sessvar_isset("server_id")) $server_id = sessvar("server_id");
	set_sessvar("server_id",$server_id);

	$track_id = 0;
	if(reqvar("track_id")) $track_id = reqvar("track_id");
	else if(sessvar_isset("track_id")) $track_id = sessvar("track_id");
	set_sessvar("track_id", $track_id);

	$room_id = 0;
	if(reqvar("room_id")) $room_id = reqvar("room_id");
	else if(sessvar_isset("room_id")) $room_id = sessvar("room_id");
	set_sessvar("room_id", $room_id);
	
	$quantity = 1;
	if (reqvar("quantity")) $quantity = reqvar("quantity");
	else if (sessvar_isset("quantity")) $quantity = sessvar("quantity");
	set_sessvar("quantity", $quantity);

	$special = "";
	if (reqvar("special")) $special = reqvar("special");
	else if (sessvar_isset("special")) $special = sessvar("special");
	set_sessvar("special", $special);

	$detail1 = "";
	if (reqvar("detail1")) $detail1 = reqvar("detail1");
	else if (sessvar_isset("detail1")) $detail1 = sessvar("detail1");
	set_sessvar("detail1", $detail1);
	
	$info_form = array();
	$info_form[] = array("Title","title","text");
	$info_form[] = array("Name","cust_name","text");
	$info_form[] = array("Phone","cust_phone","tel");
	$info_form[] = array("Email","cust_email","email");
	//$info_form[] = array("Customer Sales Rep","cust_rep");
	
	$year = reqvar("year");
	$month = reqvar("month");
	$day = reqvar("day");
	$time = reqvar("time");
	$time_array = explode(":", $time);
	$display = "";

	if ($mode == "") {

		if (($special == "group event") || ($special == "")) {
			$get_rooms = lavu_query("SELECT * FROM `lk_rooms` WHERE `locationid` = '[1]' AND `_deleted` != '1' ORDER BY `_order`, `id` ASC", $loc_id);
			$row_count = @mysqli_num_rows($get_rooms);
			if ($row_count > 0) {
				$row = 0;
				if ($row_count > 1) {
					$display .= "<tr><td height='8'></td></tr>
					<tr><td align='center' class='nametext'>Room: <select id='set_room' onchange='window.location = \"group_event_input.php?room_id=\" + document.getElementById(\"set_room\").value'>";
				}
				while ($room_info = mysqli_fetch_assoc($get_rooms)) {
					$row++;
					if (($row == 1) && ($room_id == 0))  {
						$room_id = $room_info['id'];
						set_sessvar("room_id", $room_id);
					}
					if ($row_count > 1) {
						$selected = "";
						if ($room_info['id'] == $room_id) {
							$selected = " SELECTED";
						}
						$display .= "<option value='".$room_info['id']."'$selected>".$room_info['title']."</option>";
					}
				}
				if ($row_count > 1) {
					$display .= "</select></td></tr>";
				}
			}
		} else if ($special == "race sequence") {
			$get_tracks = lavu_query("SELECT * FROM `lk_tracks` WHERE `locationid` = '[1]' AND `_deleted` != '1' ORDER BY `_order`, `id` ASC", $loc_id);
			$row_count = @mysqli_num_rows($get_tracks);
			if ($row_count > 0) {
				$row = 0;
				if ($row_count > 1) {
					$display .= "<tr><td height='8'></td></tr>
					<tr><td align='center' class='nametext'>Track: <select id='set_track' onchange='window.location = \"group_event_input.php?track_id=\" + document.getElementById(\"set_track\").value'>";
				}
				while ($track_info = mysqli_fetch_assoc($get_tracks)) {
					$row++;
					if (($row == 1) && ($track_id == 0))  {
						$track_id = $track_info['id'];
						set_sessvar("track_id", $track_id);
					}
					if ($row_count > 1) {
						$selected = "";
						if ($track_info['id'] == $track_id) {
							$selected = " SELECTED";
						}
						$display .= "<option value='".$track_info['id']."'$selected>".$track_info['title']."</option>";
					}
				}
				if ($row_count > 1) {
					$display .= "</select></td></tr>";
				}
			}
		}
	
		$display .= "<tr><td height='8'></td></tr>
		<tr><td width='423' height='349' align='center' style='background:URL(images/inner_cal_bg.png); background-repeat:no-repeat;'><iframe id='calendar_iframe' frameborder='0' scrolling='no' width='418' height='500' src='inner_calendar.php' allowtransparency='true'></iframe></td></tr>";

	} else if ($mode == "get_info") {
	
		$all_kart_users = sprintf("SELECT `id`, `f_name`, `l_name` FROM `users`");
		$sales_rep_query = sprintf("SELECT `id`, `f_name`, `l_name` FROM `users` WHERE `id`=%s",ConnectionHub::getConn('rest')->escapeString($server_id));
		
		$users_result = lavu_query($all_kart_users);
		$sales_rep_result = lavu_query($sales_rep_query);
		
		$rep_name = array();
		while ($row = mysqli_fetch_assoc($sales_rep_result)) {		
			$rep_name[] = array($row['f_name'],$row['l_name']);
		}
	
		$display = "<tr><td align='center' style='padding:10px 0px 8px 0px;'><span class='subtitles'>Date:</span> <span class='info_text1'>$year-$month-$day</span> &nbsp;&nbsp; <span class='subtitles'>Time:</span> <span class='info_text1'>".date("g:ia", mktime($time_array[0], $time_array[1], 0, $month, $day, $year))."</span></td></tr>
		<tr>
			<td align='center'>
				<form name='info_form' action='group_event_input.php' method='post'>
					<input type='hidden' name='mode' value='submit'>
					<input type='hidden' name='event_date' value='$year-".str_pad($month, 2, "0", STR_PAD_LEFT)."-".str_pad($day, 2, "0", STR_PAD_LEFT)."'>
					<input type='hidden' name='event_time' value='$time'>
					<table width='425' cellspacing='0' cellpadding='0'>";
		foreach ($info_form as $info) {

			/* only display value of logged in user in the Customer Rep field
			if ($info[1]=='cust_rep') {
				
			$display .= "<tr>
							<td width='100' height='46'>
								<table width='100' border='0' cellspacing='0' cellpadding='8'>
									<tr><td align='right' class='subtitles'>".$info[0]."</td></tr>
								</table>
							</td>
							<td width='313' style='background:URL(images/tab_bg2.png); background-repeat:no-repeat;'>
								<table width='313' border='0' cellspacing='0' cellpadding='8'>								
								<tr><td class='nametext'><input type='text' name='".$info[1]."' class='nametext' style='border:none; background: URL(win_overlay.png);' size='30' value='".$rep_name[0][0].' '.$rep_name[0][1]."'></td></tr>
								</table>
							</td>
						</tr>";
			} else {*/


			$display .= "<tr>
							<td width='100' height='46'>
								<table width='100' border='0' cellspacing='0' cellpadding='8'>
									<tr><td align='right' class='subtitles'>".$info[0]."</td></tr>
								</table>
							</td>
							<td width='313' style='background:URL(images/tab_bg2.png); background-repeat:no-repeat;'>
								<table width='313' border='0' cellspacing='0' cellpadding='8'>								
								<tr><td class='nametext'><input type='".$info[2]."' name='".$info[1]."' class='nametext' style='border:none; background: URL(win_overlay.png);' size='30'></td></tr>
								</table>
							</td>
						</tr>";

		} //end foreach
		
		
		// display dropdown of users in system
		$display .= "<tr>
						<td width='100' height='46'>
							<table width='100' border='0' cellspacing='0' cellpadding='8'>
								<tr><td align='right' class='subtitles'>Customer Sales Rep</td></tr>
							</table>
						</td>
						<td width='313' style='background:URL(images/tab_bg2.png); background-repeat:no-repeat;'>
							<table width='313' border='0' cellspacing='0' cellpadding='8'>								
							<tr><td class='nametext'><select name='cust_rep' class='nametext' style='border:none; background: URL(win_overlay.png);'><option value=''>-- Please Select --</option>";
							// get users to populate customer-rep dropdown
							$users_result = lavu_query("SELECT id, firstname as f_name, lastname as l_name FROM lk_sales_reps where `loc_id`='[1]'",$loc_id);
							while ($row = mysqli_fetch_assoc($users_result)) {
								if ($row['id'] == $server_id) {
									$display .= "<option value='".$row['f_name'].' '.$row['l_name']."' selected='selected'>".$row['f_name'].' '.$row['l_name']. "</option>";
								} else {
									
									$display .= "<option value='".$row['f_name'].' '.$row['l_name']."'>".$row['f_name'].' '.$row['l_name']. "</option>";
								}
								
							}
							$display .= "</select></td></tr>
							</table>
						</td>
					</tr>"; 

		$checklist_value = "";
		$checklist_value_escaped = str_replace("\"","&quot;",$checklist_value);
		
		$checklist_code = "";
		$checklist_code .= "<input type='hidden' name='checklist' id='checklist' value=\"$checklist_value_escaped\">";
		$checklist_code .= "<table>";
		$checklist_js = "";
		$checklist_js .= "set_val = ''; set_desc = ''; ";
		$cl_cols = 0;
		$cl_query = lavu_query("select * from `config` where `location`='[1]' and `setting`='Group Event Checklist'",$loc_id);
		while($cl_read = mysqli_fetch_assoc($cl_query))
		{
			if($cl_cols==0)
			{
				$check_list .= "<tr>";
			}
			$clitem_id = $cl_read['id'];
			$clitem_name = "checklist_item_" . $cl_read['id'];
			if(strpos($checklist_value_ids,"[".$clitem_id."]")!==false)
				$clitem_code = " checked";
			else $clitem_code = "";
			
			$checklist_js .= "if(document.getElementById('$clitem_name').checked) { set_val += '[".$clitem_id."]'; if(set_desc!='') set_desc += ', '; set_desc += \"".str_replace("\"","&quot;",$cl_read['value'])."\"; } ";
			
			$checklist_code .= "<td align='right' style='width:140px' valign='middle' class='checklist_subtitles'>" . $cl_read['value'] . "</td><td valign='middle'><input name='$clitem_name' id='$clitem_name' type='checkbox' onclick='set_checklist_value(); document.getElementById(\"update_btn\").style.display = \"inline\";' style='width:32px; height:32px;'".$clitem_code."></td>";
			$cl_cols++;
			if($cl_cols > 1)
			{
				$cl_cols = 0;
				$checklist_code .= "</tr>";
			}
			else $checklist_code .= "<td width='12'>&nbsp;</td>";
		}
		$checklist_code .= "</table>";
		$jsfunc = "";
		$jsfunc .= "<script language='javascript'>";
		$jsfunc .= "function set_checklist_value() { ";
		$jsfunc .= $checklist_js;
		//$jsfunc .= "  alert('checkbox pressed'); ";
		$jsfunc .= "  document.getElementById('checklist').value = set_val + '|' + set_desc; ";
		$jsfunc .= "} ";
		$jsfunc .= "</script>";
		$checklist_code = $jsfunc . $checklist_code;
		
		$display .= "<tr>
							<td width='100' height='150' valign='top'>
								<table width='100' border='0' cellspacing='0' cellpadding='8'>
									<tr><td align='right' class='subtitles'>Notes</td></tr>
								</table>
							</td>
							<td width='313' style='background:URL(images/tab_bg2_area.png); background-repeat:no-repeat;'>
								<table width='313' border='0' cellspacing='0' cellpadding='8'>
									<tr><td class='nametext'><textarea name='notes' class='nametext' style='border:none; background: URL(win_overlay.png);' rows='7' cols='30' /></textarea></td></tr>
								</table>
							</td>
						</tr>
						<tr>
							<td width='440' valign='top' colspan='2' align='right'>
								$checklist_code
							</td>
						</tr>
						<tr><td align='center' valign='middle' colspan='2' style='padding:20px 0px 20px 0px'><img src='images/btn_submit.png' width='120px' onclick='document.info_form.submit();'></td></tr>
					</table>
				</form>
			</td>
		</tr>";

	} else if ($mode == "submit") {
	
		if ($special == "race sequence") {
			$room_id = "";
		}
			
		$q_fields = "";
		$q_values = "";
		$vars = array();
		$vars['checklist'] = $_POST['checklist'];
		$vars['title'] = $_POST['title'];
		$vars['event_date'] = $_POST['event_date'];
		$vars['event_time'] = str_replace(":", "", $_POST['event_time']);
		$vars['duration'] = $detail1;
		$vars['cust_name'] = $_POST['cust_name'];
		$vars['cust_phone'] = $_POST['cust_phone'];
		$vars['cust_email'] = $_POST['cust_email'];
		$vars['notes'] = $_POST['notes'];
		$vars['loc_id'] = $loc_id;
		$vars['server_id'] = $server_id;
		$vars['track_id'] = $track_id;
		$vars['room_id'] = $room_id;
		$vars['checked_out'] = "0";
		$vars['cust_rep'] = $_POST['cust_rep'];
		$keys = array_keys($vars);
		foreach ($keys as $key) {
			if ($q_fields != "" ) { $q_fields .= ", "; $q_values .= ", "; }
			$q_fields .= "`$key`";
			$q_values .= "'[$key]'";
		}
		$create_event = lavu_query("INSERT INTO `lk_group_events` ($q_fields, `created`) VALUES ($q_values, now())", $vars);
		$new_event_id = ConnectionHub::getConn('rest')->insertID();
	
		$date_parts = explode("-", $_POST['event_date']);
		$time_parts = explode(":", $_POST['event_time']);		
		$event_ts = mktime($time_parts[0], $time_parts[1], 0, $date_parts[1], $date_parts[2], $date_parts[0]);
		
		if ($special == "race sequence") {

			$get_track_info = lavu_query("SELECT * FROM `lk_tracks` WHERE `id` = '[1]'", $track_id);
			$track_info = mysqli_fetch_assoc($get_track_info);

			$get_sequence_info = lavu_query("SELECT * FROM `lk_event_sequences` WHERE (`title` LIKE '[1]' OR `id` = '[1]') AND `locationid` = '[2]' AND `_deleted` != '1'", $detail1, $loc_id);
			if (@mysqli_num_rows($get_sequence_info) > 0) {
				$sequence_info = mysqli_fetch_assoc($get_sequence_info);
				$event_list = explode(",", $sequence_info['event_list']);
				$race_sets = ceil($quantity / $sequence_info['slots']);
				$new_group_id = assignNext("groupid", "lk_events", "locationid", $loc_id);
				$step = 0;
				foreach ($event_list as $etid) {
					for ($s = 1; $s <= $race_sets; $s++) {
						$set_ts = ($event_ts + ($track_info['time_interval'] * 60 * $step));
						$q_fields = "";
						$q_values = "";
						$vars = array();
						$vars['locationid'] = $loc_id;
						$vars['type'] = $etid;
						$vars['date'] = $_POST['event_date'];
						$vars['time'] = date("Hi", $set_ts);
						$vars['ts'] = $set_ts;
						$vars['trackid'] = $track_id;
						$vars['groupid'] = $new_group_id;
						$keys = array_keys($vars);
						foreach ($keys as $key) {
							if ($q_fields != "" ) { $q_fields .= ", "; $q_values .= ", "; }
							$q_fields .= "`$key`";
							$q_values .= "'[$key]'";
						}
						$schedule_event = lavu_query("INSERT INTO `lk_events` ($q_fields) VALUES ($q_values)", $vars);
						$step++;
					}
				}
			}
		}

		$lk_log_this = lavu_query("INSERT INTO `lk_action_log` (`action`, `customer_id`, `order_id`, `time`, `user_id`, `credits`, `loc_id`, `group_event_id`) VALUES ('Group Event Scheduled: ".str_replace("'", "''", $_POST['title'])."', '0', '0', now(), '".$server_id."', '0', '".$loc_id."', '".$new_event_id."')");
		
		header("Location: _DO:cmd=event_info&event_title=".$_POST['title']."&customer_name=".$_POST['cust_name']."&event_id=".$new_event_id."&event_date=".$_POST['event_date']."&event_time=".date("g:ia", $event_ts));
		exit;
	}

?>
	<body>
		<table width="426" border="0" cellspacing="0" cellpadding="0">
			<?php echo $display; ?>
		</table>
	</body>
</html>
