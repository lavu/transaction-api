<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<title>Untitled Document</title>

		<style type="text/css">
<!--
body {
	background-color: transparent;
	margin-left: 0px;
	margin-top: 0px;
	margin-right: 0px;
	margin-bottom: 0px;
}

.creditnumber {font-family: Verdana, Geneva, sans-serif;
	font-size: 13px;
	font-weight: bold;
	color: #3d4f6d;
}
.nametext {font-family: Verdana, Geneva, sans-serif;
	font-size: 13px;
	font-weight: bold;
}
.subtitles {font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 14px;
	font-weight: bold;
	color: #5A75A0;
}
.style411 {font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 14px;
	font-weight: bold;
	color: #5A75A0;
}
.gridposition {font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 14px;
	font-weight: bold;
	color: #5A75A0;
}
.icontext {font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 11px;
	font-weight: bold;
	color: #5A75A0;
}
.subtitles1 {font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 13px;
	font-weight: bold;
	color: #5A75A0;
}
.title1 {	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 18px;
	font-weight: bold;
	color: #5A75A0;
}
-->
		</style>
	</head>
	
<?php	
	require_once(dirname(__FILE__) . "/../comconnect.php");
	
	$mode = reqvar("mode");
	$locationid = reqvar("loc_id");
	$eventid = sessvar("eventid");
	
	if($mode=="set_track")
	{
		$trackid = reqvar("set_trackid");
	}
	else
	{
		if(sessvar_isset("trackid"))
			$trackid = sessvar("trackid");
		else
			$trackid = false;
	}
	if($trackid)
	{
		$track_query = lavu_query("select * from `lk_tracks` where id='[1]' and `locationid`='[2]'",$trackid,$locationid);
		if(!mysqli_num_rows($track_query))
			$trackid = false;
	}
	
	if(!$trackid)
	{
		$track_query = lavu_query("select * from `lk_tracks` where `locationid`='$locationid' and `_deleted`!='1' order by id asc limit 1");
	}
	if(!mysqli_num_rows($track_query))
	{
		$track_title = "&nbsp;";
	}
	else
	{	
		$track_read = mysqli_fetch_assoc($track_query);
		$time_start = $track_read['time_start'];
		$time_end = $track_read['time_end'];
		$time_span = $track_read['time_interval'];
		$trackid = $track_read['id'];
		$track_title = $track_read['title'];
	}
	
	$year = reqvar("year");
	$month = reqvar("month");
	$day = reqvar("day");
	
	if(!$year && $eventid)
	{
		$event_query = lavu_query("select * from `lk_events` where id='[1]'",$eventid);
		if(mysqli_num_rows($event_query))
		{
			$event_read = mysqli_fetch_assoc($event_query);
			$set_date = $event_read['date'];
			$date_parts = explode("-",$set_date);
			if(count($date_parts) > 2)
			{
				$year = $date_parts[0];
				$month = $date_parts[1];
				$day = $date_parts[2];
			}
		}
	}
	if(!$year) 
	{
		$year = date("Y");
		$month = date("m");
		$day = date("d");
	}
	$date_ts = mktime(0,0,0,$month,$day,$year);
	$show_date = date("M d",$date_ts);
?>

	<body>
		<table width="474" height="598" border="0" cellpadding="0" cellspacing="0" >
			<tr>
				<td width="77" height="536" align="center" valign="top">
					<table width="68" border="0" cellpadding="0" cellspacing="0">
						<tr><td height="120" align="center" valign="middle" class="subtitles">&nbsp;</td></tr>
						<tr><td height="32" align="center" valign="middle" class="subtitles"><a href="_DO:cmd=create_overlay&f_name=lavukart/calendar_wrapper.php;lavukart/calendar.php&c_name=calendar_wrapper;calendar&dims=112,200,423,374;112,200,423,374"><img src="images/btn_calendar.png" width="68" height="68" border="0"/></a></td></tr>
						<tr><td height="32" align="center" valign="middle"><span class="icontext">CALENDAR</span></span></td></tr>
						<tr><td height="20" align="center" valign="middle" class="subtitles">&nbsp;</td></tr>
						<tr><td height="32" align="center" valign="middle" class="subtitles"><a href="_DO:cmd=create_overlay&f_name=lavukart/newevent_wrapper.php;lavukart/select_track.php&c_name=select_track_wrapper;select_track&dims=112,200,423,374;112,200,423,374?locid=<?php echo $_REQUEST['loc_id']; ?>"><img src="images/btn_select_track.png" width="68" height="68" border="0"/></a></td></tr>
						<tr><td height="32" align="center" valign="middle"><span class="icontext">TRACKS</span></span></td></tr>
					</table>
				</td>
				<td align="left" valign="top">
					<table width="395" height="598" border="0" cellspacing="0" cellpadding="0">
						<tr>
							<td height="40">
								<table width="392" border="0" cellspacing="0" cellpadding="0">
									<tr>
										<td width="119" height="46">
											<table width="119" border="0" cellspacing="0" cellpadding="8">
												<tr><td width="119" class="title1"><?php echo "<b>".$show_date."</b>"?></td></tr>
											</table>
										</td>
										<td width="233" >
											<table width="233" border="0" cellpadding="8" cellspacing="0">
												<tr><td align="center" class="title1"><font color="#4A5580"><?php echo "<b>".$track_title."</b>"?></font></td></tr>
											</table>
										</td>
										<td width="80">&nbsp;</td>
									</tr>
								</table>
							</td>
						</tr>
						<tr>
							<td height="535" align="left" valign="top">
								<table cellspacing="0" cellpadding="0">
									<tr>
										<td><img src="images/line.png" width="1" height="535"></td>
									</tr>
								</table>
							</td>
						</tr>
						<tr><td height="23" background="images/line_bot.png"></td></tr>
					</table>
				</td>
			</tr>
		</table>
	</body>
</html>