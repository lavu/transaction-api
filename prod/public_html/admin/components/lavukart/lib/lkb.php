<?php
	ini_set('display_errors',0);
	//error_reporting(E_ALL|E_STRICT);
	$isLocalServer = $_SERVER['DOCUMENT_ROOT'] == '/poslavu-local';
	if($isLocalServer){
		require_once("/poslavu-local/lib/info_inc.php");
	}else{
		require_once("/home/poslavu/public_html/admin/cp/resources/core_functions.php");
	}
	require_once("../../../lib/ml_connect.php");
	function getFullStateName($state) {
		if (strtoupper($state) == "AL") { $return_state = "Alabama"; }
		else if (strtoupper($state) == "AK") { $return_state = "Alaska"; }
		else if (strtoupper($state) == "AZ") { $return_state = "Arizona"; }
		else if (strtoupper($state) == "AR") { $return_state = "Arkansas"; }
		else if (strtoupper($state) == "CA") { $return_state = "California"; }
		else if (strtoupper($state) == "CO") { $return_state = "Colorado"; }
		else if (strtoupper($state) == "CT") { $return_state = "Connecticut"; }
		else if (strtoupper($state) == "DE") { $return_state = "Delaware"; }
		else if (strtoupper($state) == "FL") { $return_state = "Florida"; }
		else if (strtoupper($state) == "GA") { $return_state = "Georgia"; }
		else if (strtoupper($state) == "HI") { $return_state = "Hawaii"; }
		else if (strtoupper($state) == "ID") { $return_state = "Idaho"; }
		else if (strtoupper($state) == "IL") { $return_state = "Illinois"; }
		else if (strtoupper($state) == "IN") { $return_state = "Indiana"; }
		else if (strtoupper($state) == "IA") { $return_state = "Iowa"; }
		else if (strtoupper($state) == "KS") { $return_state = "Kansas"; }
		else if (strtoupper($state) == "KY") { $return_state = "Kentucky"; }
		else if (strtoupper($state) == "LA") { $return_state = "Louisiana"; }
		else if (strtoupper($state) == "ME") { $return_state = "Maine"; }
		else if (strtoupper($state) == "MD") { $return_state = "Maryland"; }
		else if (strtoupper($state) == "MA") { $return_state = "Massachusetts"; }
		else if (strtoupper($state) == "MI") { $return_state = "Michigan"; }
		else if (strtoupper($state) == "MN") { $return_state = "Minnesota"; }
		else if (strtoupper($state) == "MS") { $return_state = "Montana"; }
		else if (strtoupper($state) == "MO") { $return_state = "Missouri"; }
		else if (strtoupper($state) == "MT") { $return_state = "Montana"; }
		else if (strtoupper($state) == "NE") { $return_state = "Nebraska"; }
		else if (strtoupper($state) == "NV") { $return_state = "Nevada"; }
		else if (strtoupper($state) == "NH") { $return_state = "New Hampshire"; }
		else if (strtoupper($state) == "NJ") { $return_state = "New Jersey"; }
		else if (strtoupper($state) == "NM") { $return_state = "New Mexico"; }
		else if (strtoupper($state) == "NY") { $return_state = "New York"; }
		else if (strtoupper($state) == "NC") { $return_state = "North Carolina"; }
		else if (strtoupper($state) == "ND") { $return_state = "North Dakota"; }
		else if (strtoupper($state) == "OH") { $return_state = "Ohio"; }
		else if (strtoupper($state) == "OK") { $return_state = "Oklahoma"; }
		else if (strtoupper($state) == "OR") { $return_state = "Oregon"; }
		else if (strtoupper($state) == "PA") { $return_state = "Pennsylvania"; }
		else if (strtoupper($state) == "RI") { $return_state = "Rhode Island"; }
		else if (strtoupper($state) == "SC") { $return_state = "South Carolina"; }
		else if (strtoupper($state) == "SD") { $return_state = "South Dakota"; }
		else if (strtoupper($state) == "TN") { $return_state = "Tennessee"; }
		else if (strtoupper($state) == "TX") { $return_state = "Texas"; }
		else if (strtoupper($state) == "UT") { $return_state = "Utah"; }
		else if (strtoupper($state) == "VT") { $return_state = "Vermont"; }
		else if (strtoupper($state) == "VA") { $return_state = "Virginia"; }
		else if (strtoupper($state) == "WA") { $return_state = "Washington"; }
		else if (strtoupper($state) == "WV") { $return_state = "West Virginia"; }
		else if (strtoupper($state) == "WI") { $return_state = "Wisconsin"; }
		else if (strtoupper($state) == "WY") { $return_state = "Wyoming"; }
		else { $return_state = $state; }
		return $return_state;
	}
	$mode = $_REQUEST['m'];
	$maindb = "poslavu_MAIN_db";
	//Could no longer piggy back on other requests.  Must check if the registration app should
	//bipass the signature/waiver process.
	if($mode == "bypasses_signature_and_waiver"){
		$rdb = "poslavu_".$_REQUEST['dn']."_db";
		lavu_connect_dn($_REQUEST['dn'],$rdb);
		$result = lavu_query("SELECT * FROM `config` WHERE `setting`='reg_app_bypass_sign_and_waivers'");
		//---------------------------
		///Now piggybacking 'reg_app_ask_keep_address_fields'
		$confAddressQueryStr = "SELECT `value2` FROM `config` WHERE `setting`='reg_app_ask_keep_address_fields'";
		$saveAddressResult = lavu_query($confAddressQueryStr, $rdb);
		$appendSaveAddressResult = '0';
		if($saveAddressResult && mysqli_num_rows($saveAddressResult)){
			$ask_row = mysqli_fetch_assoc($saveAddressResult);
			$appendSaveAddressResult = $ask_row['value2'];
		}
		//---------------------------
		//determine if query was successful
		if(!$result){
			echo 'error|result returned false';
		}else{
			if(!mysqli_num_rows($result)){
				echo 'success|DNE|' . $appendSaveAddressResult;
			}else{
				$resultArr = mysqli_fetch_assoc($result);
				if ($resultArr['value2'] == '') {
					$resultArr['value2'] = '0';
				}
				echo 'success|' . $resultArr['value2'] . '|' . $appendSaveAddressResult;
			}
		}
		exit();
	}
	if ($mode == "set_reg_dn") {	
		$post_username = $_REQUEST['un'];
		$post_password = $_REQUEST['pw'];
		$set_dataname = "0";
		$get_dev = (isset($_REQUEST['get_dev']))?$_REQUEST['get_dev']:0;
		$set_dev = "|*|";
		$cust_query = mlavu_query("select * from `poslavu_MAIN_db`.`customer_accounts` where `username`='[1]'",$post_username);
		$logoImg = "|*|NOT_SET";
		if(mysqli_num_rows($cust_query)){
			$cust_read = mysqli_fetch_assoc($cust_query);
			$dataname = $cust_read['dataname'];
			$rdb = "poslavu_".$dataname."_db";
			lavu_connect_dn($dataname,$rdb);
			//Here we get the logo img.
			$logoImgQResult = mlavu_query("SELECT `logo_img` FROM `poslavu_MAIN_db`.`restaurants` WHERE `data_name` = '[1]'", $dataname);
			if($logoImgQResult && mysqli_num_rows($logoImgQResult)){
				$logoImgArr = mysqli_fetch_assoc($logoImgQResult);
				$logoImg = "|*|" . $logoImgArr['logo_img'];
			}			
			$user_query = lavu_query("select * from `users` where `username`='[1]' and (`password`=PASSWORD('[2]') or `password`=OLD_PASSWORD('[2]'))",$post_username,$post_password);
			if(mysqli_num_rows($user_query)) {
				$set_dataname = $dataname;
				if ($get_dev == 1) {
					$rest_query = mlavu_query("SELECT `dev` FROM `poslavu_MAIN_db`.`restaurants` WHERE `data_name` = '[1]'", $dataname);
					if (@mysqli_num_rows($rest_query) > 0) {
						$rest_info = mysqli_fetch_assoc($rest_query);
						if ($rest_info['dev'] == "1") {
							$set_dev = "|*|/dev";
						}
					}
				}
			}
		}
		echo $set_dataname.$set_dev.$logoImg;
		exit();
	} else if ($mode == "get_locations") {
		error_log("get_locations");
		$locations_return_string = 0;
		$dn = $_REQUEST['dn'];
		$rdb = "poslavu_".$dn."_db";
		lavu_connect_dn($dn,$rdb);
		$locations_string = "";
		$get_locations = lavu_query("SELECT * FROM `locations` ORDER BY `state`, `city`, `title` ASC");
		if (@mysqli_num_rows($get_locations) > 0) {
			while ($loc_info = mysqli_fetch_array($get_locations)) {
				$get_waivers = lavu_query("SELECT * FROM `lk_waivers` WHERE `loc_id` = '".$loc_info['id']."' and `_deleted`!='1' ORDER BY `id` DESC LIMIT 1");
				$waiver_info = mysqli_fetch_assoc($get_waivers);
				$alt_reg_bg = "";
				$get_alt_reg_bg = lavu_query("SELECT `value` FROM `config` WHERE `location` = '[1]' AND `type` = 'location_config_setting' AND `setting` = 'lk_alternate_registration_background'", $loc_info['id']);
				if (mysqli_num_rows($get_alt_reg_bg) > 0) {
					$info = mysqli_fetch_assoc($get_alt_reg_bg);
					$alt_reg_bg = $info['value'];
				}
				if ($loc_info['use_net_path']=="2" && ($_SERVER['REMOTE_ADDR']=="74.95.18.90"||$_SERVER['REMOTE_ADDR']=="67.42.135.99"||$_SERVER['REMOTE_ADDR']=="67.0.123.196")) {
					$loc_info['use_net_path'] = "1";
					$loc_info['net_path'] = "http://www.poslavu.com/lsvpn";
				}
				$locations_string .= $loc_info['id']."::".$loc_info['title']."::".getFullStateName(trim($loc_info['state']))."::".$loc_info['net_path']."::".$loc_info['use_net_path']."::".$waiver_info['version']."::".$waiver_info['adult']."::".$waiver_info['minor_parent']."::".$waiver_info['minor_participant']."::".$alt_reg_bg.";;";
			}
			$locations_return_string = "1|".substr($locations_string, 0, (strlen($locations_string) - 2));
		}
		echo $locations_return_string;
		exit;
	} else if ($mode == "get_states_and_sources") {
		$states_return_string = "";
		$sources_return_string = "";
		$rdb = "poslavu_".$_REQUEST['dn']."_db";
		lavu_connect_dn($_REQUEST['dn'],$rdb);
		$states_string = "";
		$get_states = lavu_query("SELECT * FROM `states` ORDER BY `state_name` ASC");
		if (@mysqli_num_rows($get_states) > 0) {
			while ($extract = mysqli_fetch_array($get_states)) {
				$states_string .= $extract['state_name'].";;";
			}
			$states_return_string = substr($states_string, 0, (strlen($states_string) - 2));
		} else {
			$states_return_string = "no_states";
		}
		$sources_string = "";
		$get_sources = lavu_query("SELECT * FROM `lk_source_list` ORDER BY `_order` ASC");
		if (@mysqli_num_rows($get_sources) > 0) {
			while ($extract = mysqli_fetch_assoc($get_sources)) {
				$sources_string .= $extract['title'].";;";
			}
			$sources_return_string = substr($sources_string, 0, (strlen($sources_string) - 2));
		} else {
			$sources_return_string = "no_sources";
		}
		//PIGGY BACKING HERE, WE ADD THE USER DEFINED FIELDS TO PASS DOWN TO THE APP.
		//To keep things simple we can just pull the entire config table and pass it as JSON.
		$qResult = lavu_query("SELECT * FROM `config` WHERE `setting` = 'GetCustomerInfo' ORDER BY `value10` * 1 ASC");
		//Now we iterate through the rows and we add each array that has not been deleted
		$fullJSONObject = array();
		$currRow;
		while($currRow = mysqli_fetch_assoc($qResult)){
			if(!$currRow['_deleted']){
				$fullJSONObject[] = $currRow;
			}
		}
		$jsonReturnVal = json_encode($fullJSONObject);
		//End Piggy backing
		////////////////////////////////////////////////////////////////////////////
		echo "1|".$states_return_string."|".$sources_return_string."|".$jsonReturnVal;
		exit;
	} else if ($mode == "check_racer_name") {
		$rdb = "poslavu_".$_REQUEST['dn']."_db";
		lavu_connect_dn($_REQUEST['dn'],$rdb);
		$check_racer_name = lavu_query("SELECT * FROM `lk_customers` WHERE `f_name` = '[1]' AND `l_name` = '[2]' AND `birth_date` = '[3]'", $_REQUEST['f_name'], $_REQUEST['l_name'], $_REQUEST['birth_date']);
		if (@mysqli_num_rows($check_racer_name) > 0) {
			echo "1|0|Duplicate Entry|Please check with attendant. The system shows you may already be in the system.";
			exit;
		} else {
			echo "1|1";
			exit;
		}
	} else if ($mode == "save_reg_info") {
		$rdb = "poslavu_".str_replace("'","",str_replace("`","",$_REQUEST['dn']))."_db";
		lavu_connect_dn(str_replace("'","",str_replace("`","",$_REQUEST['dn'])),$rdb);
		//Comming from the LKart app
		$qvars = array();
		$qvars['rdb'] = $rdb; //stands for 'restaurants db'.
		$qvars['f_name'] = $_REQUEST['f_name'];
		$qvars['l_name'] = $_REQUEST['l_name'];
		$qvars['racer_name'] = $_REQUEST['racer_name'];
		$qvars['birth_date'] = $_REQUEST['birth_date'];
		$qvars['email'] = $_REQUEST['email'];
		$qvars['phone_number'] = $_REQUEST['phone_number'];
		$qvars['state'] = $_REQUEST['state'];
		$qvars['postal_code'] = $_REQUEST['postal_code'];
		$qvars['country'] = $_REQUEST['country'];
		$qvars['source'] = $_REQUEST['source'];
		$qvars['license_exp'] = $_REQUEST['license_exp'];
		$qvars['loc_id'] = $_REQUEST['loc_id'];
		$qvars['minor_or_adult'] = $_REQUEST['minor_or_adult'];
		$qvars['waiver_version'] = $_REQUEST['waiver_version'];

		$loc_query = lavu_query("select * from `locations` where `id`='[1]'",$_REQUEST['loc_id']);
		$com_type;
		if(mysqli_num_rows($loc_query)){
			$loc_read = mysqli_fetch_assoc($loc_query);
			$com_type = $loc_read['component_package_code']; // options are "ers" for lasertag and "lavukart" for go-karts
		}else{
			$com_type = "";
		}
		if($com_type=="ers" || $com_type=="" || $com_type=="pizza" || $com_type=="customer" || $com_type=="medical"){
			require_once(dirname(__FILE__) . "/lkb_ers.php");
		}else{
			$check_for_duplicate = lavu_query("SELECT * FROM `lk_customers` WHERE `f_name` = '[f_name]' AND `l_name` = '[l_name]' AND `racer_name` = '[racer_name]' AND `birth_date` = '[birth_date]' AND `email` = '[email]' AND `phone_number` = '[phone_number]' AND `state` = '[state]' AND `postal_code` = '[postal_code]' AND `country` = '[country]' AND `source` = '[source]' AND `license_exp` = '[license_exp]'",$qvars);
			if (mysqli_num_rows($check_for_duplicate) > 0){
				$extract = mysqli_fetch_assoc($check_for_duplicate);
				echo "1|".$extract['id'];
				exit;
			}else{
				$create_account = lavu_query("INSERT INTO `lk_customers` (`f_name`, `l_name`, `racer_name`, `birth_date`, `email`, `phone_number`, `state`, `postal_code`, `country`, `source`, `license_exp`, `date_created`, `last_activity`, `locationid`, `minor_or_adult_at_signup`, `minor_or_adult`, `credits`, `confirmed_by_attendant`, `membership`, `total_visits`, `total_races`, `waiver_version`) VALUES ('[f_name]', '[l_name]', '[racer_name]', '[birth_date]', '[email]', '[phone_number]', '[state]', '[postal_code]', '[country]', '[source]', '[license_exp]', now(), now(), '[loc_id]', '[minor_or_adult]', '[minor_or_adult]', '0', '0', 'Unqualified', '1', '0', '[waiver_version]')",$qvars);
				$racer_id = lavu_insert_id();
				$lk_log_this = lavu_query("INSERT INTO `lk_action_log` (`action`, `customer_id`, `order_id`, `time`, `user_id`, `credits`, `loc_id`) VALUES ('Created Account', '[1]', '0', now(), '0', '0', '[2]')",$racer_id,$_REQUEST['loc_id']);
				//mail($_REQUEST['email'], "Welcome to Pole Position Raceway", "Greetings ".ucwords($_REQUEST['f_name']).",\n\nWelcome to Pole Position Raceway, the number one chain of indoor kart tracks in America. We would like to invite you to check out www.PolePositionRaceway.com to learn more about our birthday party, team building and corporate event packages, along with news, updates and a wide range of interesting photos and videos. Also be sure the look out for our special e-mail offers that will keep you up to speed with discounts, upcoming public events, and other cool things happening around the country, and follow us on Facebook and Twitter.\n\n See you at the races!", "From: info@polepositionraceway.com");
				$get_location_info = lavu_query("SELECT * FROM `locations` WHERE `id` = '[1]'", $_REQUEST['loc_id']);
				if (@mysqli_num_rows($get_location_info) > 0){
					$location_info = mysqli_fetch_assoc($get_location_info);
					update_emailing_list("subscriberadd", "racer", $_REQUEST['dn'], $location_info, $racer_id);
				}
				echo "1|".$racer_id;
				exit;
			}
		}
	} else if ($mode == "signature_updated") {
		$rdb = "poslavu_".$_REQUEST['dn']."_db";
		lavu_connect_dn($_REQUEST['dn'],$rdb);
		$update_confirmed = lavu_query("UPDATE `lk_customers` SET `confirmed_by_attendant` = '[1]' WHERE `id` = '[2]'", $_REQUEST['server_id'], $_REQUEST['customer_id']);
		$lk_log_this = lavu_query("INSERT INTO `lk_action_log` (`action`, `customer_id`, `order_id`, `time`, `user_id`, `credits`, `loc_id`) VALUES ('Signature Updated', '[1]', '0', now(), '[2]', '0', '[3]')", $_REQUEST['customer_id'], $_REQUEST['server_id'], $_REQUEST['loc_id']);
		echo "1";
		exit;
	} else if ($mode == "get_kart_info") {
		$rdb = "poslavu_".$_REQUEST['dn']."_db";
		lavu_connect_dn($_REQUEST['dn'],$rdb);
		$get_kart_info = lavu_query("SELECT * FROM `lk_karts` WHERE `locationid` = '[1]' AND `lane` > '0' ORDER BY (lane+0), (position+0) ASC",$_REQUEST['loc_id']);
		if (@mysqli_num_rows($get_kart_info) > 0) {
			$kart_info_array = array();
			while ($data_obj = mysqli_fetch_object($get_kart_info)) {
				$kart_info_array[] = $data_obj;
			}
			echo '{"json_status":"success","kart_info":'.json_encode($kart_info_array).'}';
			exit;
		} else {
			echo '{"json_status":"NoKarts"}';
			exit;
		}
	} else if ($mode == "save_kart_info") {
			$rdb = "poslavu_".$_REQUEST['dn']."_db";
			$kart_info_array = explode(",", $_REQUEST['kart_info']);
			lavu_connect_dn($_REQUEST['dn'],$rdb);
			foreach ($kart_info_array as $kart) {
				$kart_data = explode("x", $kart);
				$update_kart = lavu_query("UPDATE `lk_karts` SET `lane` = '[1]', `position` = '[2]' WHERE `id` = '[3]'",$kart_data[1],$kart_data[2],$kart_data[0]);
			}
			echo "1";
			exit;
	} else if ($mode = "card_swipe") {
		$swipe_data = trim($_REQUEST['swipe_data'], " x");
		$swipe_tracks = explode("?", trim($_REQUEST['swipe_data'], " x"));
	}
?>