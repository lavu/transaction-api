<?php
	require_once(dirname(__FILE__) . "/../../comconnect.php");
	
	if(isset($_GET['save_notes']))
	{	
		$notes_date = urlvar("notes_date");
		$notes_time = urlvar("notes_time");
		$notes_track = urlvar("notes_track");
		$notes = urlvar("notes");
		
		$set_notes = $notes;
		$set_notes = str_replace("[amp]","&",$set_notes);
		$set_notes = str_replace("[nl]","\n",$set_notes);
		
		$success = lavu_query("update lk_events set `notes`='[1]' where `date`='[2]' and `time`='[3]' and `trackid`='[4]'",$set_notes,$notes_date,$notes_time,$notes_track);
		if($success)
		{
			echo "result=success&notes_date=".urlencode($notes_date)."&notes_time=".urlencode($notes_time)."&notes_track=".urlencode($notes_track)."&notes=".urlencode($set_notes);
		}
		else
		{
			echo "result=failed&message=Unable to save notes";
		}
	}
	else if(isset($area_mode))
	{
		// START html code for schedule
		
		$event_details_code = "";
		$event_details_code .= "<table cellspacing=0 cellpadding=0><tr><td>";
		$event_details_code .= "<div style='position:relative'><textarea cols='18' rows='3' style='background-color:#e2e2ea; border:solid 1px #7788aa' id='details_text_[time]' onkeyup='details_text_keyup(this)'>[textvalue]</textarea><div style='visibility:hidden; position:absolute; top:34px; left:75px; z-index:240' id='details_submit_[time]'><input type='button' value='submit' style='font-size:10px' onclick='details_submit_clicked(this)' id='details_submitbtn_[time]'/></div>";
		$event_details_code .= "</td><td>";
		$event_details_code .= "<textarea cols='18' rows='3' style='background-color:#dadae6; border:solid 1px #7788aa' id='details_text2_[time]' readonly></textarea>";
		$event_details_code .= "</td></tr></table>";
		$empty_details_code = "<textarea cols='39' rows='3' style='background-color:#d2d2d2; border:solid 1px #7788aa' disabled></textarea>";
		
		ob_start(); // START javascript for schedule
	?>
	
	<script language="javascript">
		function details_text_id_time(telem)
		{
			var telem_parts = telem.id.split('_');
			var telem_time = telem_parts[2];
			return telem_time;
		}
		function details_text_id_notes(telem)
		{
			var telem_parts = telem.id.split('_');
			var telem_time = telem_parts[2];
			return document.getElementById("details_text_" + telem_time).value;
		}
		function details_text_keyup(telem)
		{
			var telem_time = details_text_id_time(telem);
			document.getElementById("details_submit_" + telem_time).style.visibility = "visible";
		}
		function details_submit_clicked(telem)
		{
			var telem_time = details_text_id_time(telem);
			var telem_notes = details_text_id_notes(telem);
			telem_notes = telem_notes.replace(/&/,"[amp]");
			telem_notes = telem_notes.replace(/\n/,"[nl]");
			ajax_call("<?php echo get_com_urlpath()?>/lavukart/lib/kart_notes.php?save_notes=1&notes_date=" + use_event_date + "&notes_time=" + telem_time + "&notes_track=<?php echo $area_mode_track?>&notes=" + telem_notes);
		}
		function ajax_received(vars)
		{
			var result = vars["result"];
			
			if(result=="success")
			{
				var telem_date = vars["notes_date"];
				var telem_time = vars["notes_time"];
				document.getElementById("details_submit_" + telem_time).style.visibility = "hidden";
			}
			else if(result=="failed")
			{
				alert(vars["message"]);
			}
		}
	</script>
	
	<?php
		$kart_notes_js = ob_get_contents();
		ob_end_clean();
	}
?>
