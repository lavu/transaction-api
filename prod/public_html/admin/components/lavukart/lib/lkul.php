<?php

	ini_set('display_errors',0);
	//error_reporting(E_ALL|E_STRICT);

	if (!empty($_FILES)) {
		
		foreach($_FILES as $file_name => $file_array) {
			if (is_uploaded_file($file_array['tmp_name'])) {
			
				$filepath = "../companies/".$_GET['dn']."/signatures/";
				$filename = basename($_FILES['signature_image']['name']);
				if(!is_dir("../companies/".$_GET['dn'])) 
				{
					mkdir ("../companies/".$_GET['dn'],0755);
					mkdir ("../companies/".$_GET['dn']."/signatures",0755);
				}
				if (file_exists($filepath.$filename)) {
					$append = 0;
					$new_filename = $filename;
					$filename_parts = explode(".", $new_filename);
					$basename = $filename_parts[0];
					$ext = $filename_parts[1];
					while (file_exists($filepath.$new_filename)) {
						$append++;
						$new_filename = $basename."-".$append.".".$ext;
					}
					$successful_copy = copy($filepath.$filename, $filepath.$new_filename);
					$copy_succ_debug = $successful_copy ? 'true' : 'false';
				}
				$successful_move = move_uploaded_file($file_array['tmp_name'], $filepath.$filename) or die ("Couldn't copy/rename file");
				$move_succ_debug = $successful_move ? 'true' : 'false';
				//Changed By Brian D.------------
				//The below will not work on local server.
				////require_once($_SERVER['DOCUMENT_ROOT'] . "/cp/resources/core_functions.php");
				$isLocalServer = $_SERVER['DOCUMENT_ROOT'] == '/poslavu-local';
				if($isLocalServer){
    				require_once($_SERVER['DOCUMENT_ROOT'] . "/lib/info_inc.php");
                }
                else{
                    require_once($_SERVER['DOCUMENT_ROOT'] . "/cp/resources/core_functions.php");
                }
				//-------------------------------
				$loc_query = mlavu_query("select * from `poslavu_[1]_db`.`locations` where `component_package_code`!=''",$_GET['dn']);
				if(mysqli_num_rows($loc_query))
				{
					$loc_read = mysqli_fetch_assoc($loc_query);
					$comtype = $loc_read['component_package_code'];
					
					//Changed by Brian D. Now registrar need access to lkb_ers if comtype==""
					//if($comtype=="ers")// Now if comtype=="" we are then using the registrar app.
					if( $comtype=="ers" || $comtype=="" || $comtype=="customer")
					{
						require_once(dirname(__FILE__) . "/lkb_ers.php");
						
						$customerid = $filename;
						$customerid = str_replace("signature_","",$customerid);
						$customerid = str_replace("contract_","",$customerid);
						$customerid = str_replace(".jpg","",$customerid);
						if($customerid!="" && is_numeric($customerid))
						{
							set_signature_expiration("poslavu_".$_GET['dn']."_db",$customerid);
						}
					}
				}
	
				// crop 120 pixels off each side here, but only if cropping is taken out of the app during signature display
				
				echo "1";
				
			} else {

				echo "Upload failed...";
			}
		}

	} else {
		
		echo "file not found in post ... ";
	}
?>
