	function trim(stringToTrim) {
		return stringToTrim.replace(/^\s+|\s+$/g,"");
	}
	
	ajax_use_url = "";
	function set_ajax_url(set_url)
	{
		ajax_use_url = set_url;
	}
	
	ajax_use_interval = 1000;
	function set_ajax_interval(set_interval)
	{
		ajax_use_interval = set_interval;
	}
		
	function loadXMLDoc(ajax_url)
	{
	if (window.XMLHttpRequest)
	  {// code for IE7+, Firefox, Chrome, Opera, Safari
	  xmlhttp=new XMLHttpRequest();
	  }
	else
	  {// code for IE6, IE5
	  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
	  }
	xmlhttp.onreadystatechange=function()
	  {
	  if (xmlhttp.readyState==4 && xmlhttp.status==200)
		{
			data = xmlhttp.responseText;
			data_split = data.split("&");
			vars = new Array();
			//vars['server_time'] = "";
			//vars['laps_left'] = "";
			for(i=0; i<data_split.length; i++)
			{
				data_part = trim(data_split[i]);
				if(data_part!="")
				{
					data_parts = data_part.split("=");
					if(data_parts.length > 1)
					{
						data_key = data_parts[0];
						data_val = data_parts[1];
						vars[data_key] = data_val;
					}
				}
			}
			
			ajax_received(vars);
		}
	  }
	rnd = Math.floor(Math.random()*1111);
	xmlhttp.open("GET",ajax_url + "&rnd=" + rnd,true);
	xmlhttp.send();
	}
	function start_ajax_loop()
	{
		loadXMLDoc(ajax_use_url);
		setInterval(function(){loadXMLDoc(ajax_use_url)},ajax_use_interval);
	}
	function ajax_call(ajaxurl)
	{
		loadXMLDoc(ajaxurl);
	}
	
	window.onload = function() { start_ajax_loop(); }
