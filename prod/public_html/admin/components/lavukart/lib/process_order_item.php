<?php
	
	function process_order_item($loc_id, $order_id, $extract, $close_info, $data_name) {
	
		global $location_info;
					
		// check for need to apply LavuKart race credits or membership
		
		$set_hidden_data = "";
	
		if (strstr($extract['options'], "RACER:")) {
			$racer_id = substr($extract['options'], (strpos($extract['options'], "(") + 1), (strpos($extract['options'], ")") - (strpos($extract['options'], "(") + 1)));
			$get_racer_info = lavu_query("SELECT * FROM `lk_customers` WHERE `id` = '[1]'", $racer_id);
			if (@mysqli_num_rows($get_racer_info) > 0) {
				$extract_r = mysqli_fetch_assoc($get_racer_info);

				$set_confirmed = "";
				if (($extract_r['confirmed_by_attendant'] == "") || ($extract_r['confirmed_by_attendant'] == "0")) {
					$set_confirmed = ", `confirmed_by_attendant` = '".$close_info[13]."'";
					$signature = "Signature";
					if ($extract_r['minor_or_adult_at_signup'] == "minor") {
						$signature = "Signatures";
					}
					$lk_log_this = lavu_query("INSERT INTO `lk_action_log` (`action`, `customer_id`, `order_id`, `time`, `user_id`, `credits`, `loc_id`) VALUES ('".$signature." Confirmed', '".$racer_id."', '".$order_id."', now(), '".$close_info[13]."', '0', '".$loc_id."')");
				}
				
				$update_total_visits = "";
				$today = date("Y-m-d");
				$last_activity = explode(" ", $extract_r['last_activity']);
				if ($last_activity[0] != $today) {
					$update_total_visits = ", `total_visits` = `total_visits`+1";
				}

				if ($extract['hidden_value2'] == "membership") {

					$add_credits = ($extract['hidden_value'] * $extract['quantity']);
			
					$now = time();
					$update_membership = lavu_query("UPDATE `lk_customers` SET `credits` = (`credits`+".$add_credits.")".$set_confirmed.", `membership` = '".$extract['item']."', `membership_expiration` = '".date("Y-m-d", mktime(date("h", $now), date("i", $now), date("s", $now), date("m", $now), date("d", $now), (date("Y", $now) + 1)))."', `last_activity` = now()".$update_total_visits." WHERE `id` = '".$racer_id."'");
					
					$lk_log_this = lavu_query("INSERT INTO `lk_action_log` (`action`, `customer_id`, `order_id`, `time`, `user_id`, `credits`, `loc_id`) VALUES ('".$extract['item']."', '".$racer_id."', '".$order_id."', now(), '".$close_info[13]."', '".$add_credits."', '".$loc_id."')");
				
				} else if ($extract['hidden_value2'] == "race credits") {		

					$add_credits = ($extract['hidden_value'] * $extract['quantity']);
					
					$show_s = "";
					if ($add_credits > 1) {
						$show_s = "s";
					}
				
					$update_credits = lavu_query("UPDATE `lk_customers` SET `credits` = (`credits`+".$add_credits.")".$set_confirmed.", `last_activity` = now()".$update_total_visits." WHERE id = '".$racer_id."'");

					$lk_log_this = lavu_query("INSERT INTO `lk_action_log` (`action`, `customer_id`, `order_id`, `time`, `user_id`, `credits`, `loc_id`) VALUES ('Added ".$add_credits." Race Credit".$show_s."', '".$racer_id."', '".$order_id."', now(), '".$close_info[13]."', '".$add_credits."', '".$loc_id."')");
				}
				
				if (preg_match("/^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*$/i", $extract_r['email'])) {
					update_emailing_list("subscriberadd", "racer", $data_name, $location_info, $racer_id);
				}
				
				$set_hidden_data = ", `hidden_data1` = '".$extract['hidden_value']."', `hidden_data2` = '".$extract['hidden_value2']."'";
			}
		}
		
		// check for LavuKart group event info, payments, deposits
		
		if (($extract['hidden_value2'] == "group event") || ($extract['hidden_value2'] == "track rental")) {
			$info_parts = explode(":", $extract['options']);
			$event_id = str_replace("Event #", "", $info_parts[0]);
			$update_event_record = lavu_query("UPDATE `lk_group_events` SET `item_id` = '".$extract['item_id']."', `duration` = '".$extract['hidden_value']."', `order_id` = '".$order_id."', `checked_out` = '1' WHERE `id` = '".$event_id."'");
			$update_log_record = lavu_query("UPDATE `lk_action_log` SET `order_id` = '[1]' WHERE `group_event_id` = '[2]'", $order_id, $event_id);
			$set_hidden_data = ", `hidden_data2` = '".$extract['hidden_value2']."'";
		}
	}
?>