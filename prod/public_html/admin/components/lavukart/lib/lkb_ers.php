<?php


	function set_signature_expiration($rdb,$recordid)
	{

/*//WORKING HERE
		//We are storying the waiver they are signing.
		$getMostCurrentWaiverQuery = "SELECT * FROM `lk_waivers` WHERE `_deleted`='0' ORDER BY `id` DESC limit 1";
		$result = lavu_query($getMostCurrentWaiverQuery);
		if(!$result){ error_log('Mysql error in lkb_ers.php error:'.lavu_dberror()); }
		if($result && mysqli_num_rows($result))
*/

		$waiverVersionIDStr = 'not_set';
		if(empty($_REQUEST['waiver_version'])){
			$waiver_version_query = "SELECT * FROM `[1]`.`lk_waivers` WHERE `_deleted`='0' ORDER BY `id` DESC LIMIT 1";
			$result = mlavu_query($waiver_version_query, $rdb);
			if(!$result){ error_log("mysql error in ".__FILE__." lavu_dberror:".lavu_dberror());}
			if(mysqli_num_rows($result)){
				$row = mysqli_fetch_assoc($result);
				$waiverVersionIDStr = 'vers:'.$row['version'].';';
			}
		}


		$lsetting_query = mlavu_query("select * from `[1]`.`config` where `setting`='registration_expiration_info'", $rdb);

		if(mysqli_num_rows($lsetting_query))
		{
			$lsetting_read = mysqli_fetch_assoc($lsetting_query);
			$current_exp_value = $lsetting_read['value'];
			
			$exp_parts = explode(":",$current_exp_value);
			if(count($exp_parts) > 1)
			{
				$current_exp_type = $exp_parts[0];
				$current_exp_amount = $exp_parts[1];
			}
			else
			{
				$current_exp_type = "";
				$current_exp_amount = "";
			}
			
			if(is_numeric($current_exp_amount))
			{
				if($current_exp_type=="days") 
				{
					$set_exp_ts = mktime(0,0,0,date("m"),date("d") + $current_exp_amount,date("Y"));
				}
				else if($current_exp_type=="yearly") 
				{
					$set_exp_ts = mktime(0,0,0,1,$current_exp_amount,date("Y"));
					if($set_exp_ts <= time())
						$set_exp_ts = mktime(0,0,0,1,$current_exp_amount,date("Y") + 1);
				}
				else if($current_exp_type=="monthly") 
				{
					$set_exp_ts = mktime(0,0,0,date("m"),$current_exp_amount,date("Y"));
					if($set_exp_ts <= time())
						$set_exp_ts = mktime(0,0,0,date("m") + 1,$current_exp_amount,date("Y"));
				}
				else $set_exp_ts = 0;
				
				if($set_exp_ts > 0)
				{
					$set_exp_date = date("Y-m-d H:i:s",$set_exp_ts);
					mlavu_query("update `[1]`.`med_customers` set `waiver_expiration`='[2]' where `id`='[3]'",$rdb,$set_exp_date.'|'.$waiverVersionIDStr ,$recordid);
				}
			}
		}
	}
	



//We enter into the below scope if we are saving someone as a new med_customer, we detect this by seeing if rdb, f_name, and l_name are set in the $_REQUEST.
	if(isset($rdb) && isset($_REQUEST['f_name']) && isset($_REQUEST['l_name']))
	{
		//Job for Brian D.  Enter in this scope the post variables into the med_customers 
		//If successful return 1|$(id of new record)
		
		//We start by building the assoc array for the mlavu_query.//-----------------------------------
		$ersTypeQueryArray = array();
		$ersTypeQueryArray['f_name']  = $_REQUEST['f_name'];
		$ersTypeQueryArray['l_name']  = $_REQUEST['l_name'];
		$ersTypeQueryArray['field1']  = isset($_REQUEST['field1'])  && $_REQUEST['field1']  != "(null)" ? $_REQUEST['field1'] : "";
		$ersTypeQueryArray['field2']  = isset($_REQUEST['field2'])  && $_REQUEST['field2']  != "(null)" ? $_REQUEST['field2'] : "";//$_REQUEST['field2'];
		$ersTypeQueryArray['field3']  = isset($_REQUEST['field3'])  && $_REQUEST['field3']  != "(null)" ? $_REQUEST['field3'] : "";//$_REQUEST['field3'];
		$ersTypeQueryArray['field4']  = isset($_REQUEST['field4'])  && $_REQUEST['field4']  != "(null)" ? $_REQUEST['field4'] : "";//$_REQUEST['field4'];
		$ersTypeQueryArray['field5']  = isset($_REQUEST['field5'])  && $_REQUEST['field5']  != "(null)" ? $_REQUEST['field5'] : "";//$_REQUEST['field5'];
		$ersTypeQueryArray['field6']  = isset($_REQUEST['field6'])  && $_REQUEST['field6']  != "(null)" ? $_REQUEST['field6'] : "";//$_REQUEST['field6'];
		$ersTypeQueryArray['field7']  = isset($_REQUEST['field7'])  && $_REQUEST['field7']  != "(null)" ? $_REQUEST['field7'] : "";//$_REQUEST['field7'];
		$ersTypeQueryArray['field8']  = isset($_REQUEST['field8'])  && $_REQUEST['field8']  != "(null)" ? $_REQUEST['field8'] : "";//$_REQUEST['field8'];
		$ersTypeQueryArray['field9']  = isset($_REQUEST['field9'])  && $_REQUEST['field9']  != "(null)" ? $_REQUEST['field9'] : "";//$_REQUEST['field9'];
		$ersTypeQueryArray['field10'] = isset($_REQUEST['field10']) && $_REQUEST['field10'] != "(null)" ? $_REQUEST['field10'] : "";//$_REQUEST['field10'];
		$ersTypeQueryArray['field11'] = isset($_REQUEST['field11']) && $_REQUEST['field11'] != "(null)" ? $_REQUEST['field11'] : "";//$_REQUEST['field11'];
		$ersTypeQueryArray['field12'] = isset($_REQUEST['field12']) && $_REQUEST['field12'] != "(null)" ? $_REQUEST['field12'] : "";//$_REQUEST['field12'];
		$ersTypeQueryArray['field13'] = isset($_REQUEST['field13']) && $_REQUEST['field13'] != "(null)" ? $_REQUEST['field13'] : "";//$_REQUEST['field13'];
		$ersTypeQueryArray['field14'] = isset($_REQUEST['field14']) && $_REQUEST['field14'] != "(null)" ? $_REQUEST['field14'] : "";//$_REQUEST['field14'];
		$ersTypeQueryArray['locationid'] = $_REQUEST['loc_id'];
		$ersTypeQueryArray['date_created'] = date("Y-m-d H:i:s");
		$ersTypeQueryArray['last_activity'] = date("Y-m-d H:i:s");
		$ersTypeQueryArray['field15'] = $_REQUEST['minor_or_adult'];
		$ersTypeQueryArray['field16'] = $_REQUEST['waiver_version'];
		
		//$ersTypeQueryArray['id']      = $_REQUEST['id_loc'];
		//We build the query string programmatically.
		//Columns
		$dbKeys = "";
		foreach($ersTypeQueryArray as $key => $val){ $dbKeys .= " `$key`,"; }
		$dbKeys = substr_replace($dbKeys,"",-1);//Remove last comma.
		//Values
		$dbValues = "";
		foreach($ersTypeQueryArray as $key => $val){ $dbValues .= " '[$key]',"; }
		$dbValues = substr_replace($dbValues,"",-1);//Remove last comma.
		//restaurants db.
		$ersTypeQueryArray['rdb'] = $rdb;
		//Query
		$success = mlavu_query("INSERT INTO `[rdb]`.`med_customers` ($dbKeys) VALUES ($dbValues)", $ersTypeQueryArray);
		$new_recordid = mlavu_insert_id();
		
		set_signature_expiration($rdb,$new_recordid); // added by CF
		
		if($success){
			echo "1|" . $new_recordid;//Get's the id of the item just inserted.
		}
		else{
			echo "0|Error inserting";
		}
	}
?>
