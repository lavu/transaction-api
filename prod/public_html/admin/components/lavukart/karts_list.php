<?php
			require_once(dirname(__FILE__) . "/../comconnect.php");
			
			
			if ($access_level < 3){
					echo "<script type='text/javascript'>
						alert('You do not have sufficient priveleges to access this tab.');
					</script>";
					exit();
			}//if
			
?>

<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<title>Garage List</title>

		<style type="text/css">

body {
	background-color: transparent;
	margin-left: 60px;
	margin-top: 0px;
	margin-right: 0px;
	margin-bottom: 0px;
	-webkit-tap-highlight-color:rgba(0,0,0,0);
	font-family:Verdana, Geneva, sans-serif;
}



td{
	width:52px;
	height:46px;
	color:#fff;
	text-align:center;
}






.list{
	float:left;
	height:56px;
}








		</style>
		

<script type="text/javascript">
	
	
	function changeBackgroundImage(id){
			/* function changes background image to 'lit' or 'unlit' depending on what is appropriate, the id input parameter is the id assigned to each <td></td> element in the table.*/
			
				var lastRowLit = document.getElementById('lastTRLit').value;
			
			/* If a row is lit this hidden field will contain a value, the value is the id of the last selected <tr></tr> */
			if (lastRowLit != ""){
					
					/* change back to 'unlit' image */
						
						carPic = "car" + Math.round((1 + Math.random() * 8));
						
						eval("document.getElementById(lastRowLit).style.backgroundImage=\"url('images/\" + carPic + \".png')\";");
						document.getElementById(lastRowLit).style.color="#FFF";
			
			}//if
			
			/* Change background image of current selected table row to the 'lit' image */
			
			document.getElementById(id).style.backgroundImage="url('images/car_glow1.png')";
			document.getElementById(id).style.color="#000";
			
			/* store id in hidden text field so we can switch that row off later */
			document.getElementById('lastTRLit').value = id; 
			
			
	}//changeBackgroundImage()
	
	function passDataToOtherComponent(id){
				selected_id = document.getElementById("loc_id").value;
			//	alert(selected_id);
				window.location = "_DO:cmd=load_url&c_name=karts_info&f_name=lavukart/karts_info.php?id=" + id + "&loadRightSide=true&loc_id="+selected_id;
	}//passDataToGarageInfo()


function add_kart(){
		loc_id = document.getElementById("loc_id").value;
		//alert(loc_id);
		//window.location = "_DO:cmd=load_url&lavukart/karts_addKart.php&c_name=karts_info;parts_addKart&f_name=lavukart/karts_addKartWrapper.php?loc_id="+loc_id+"&cray=1;";
		
			window.location = "_DO:cmd=create_overlay&f_name=lavukart/karts_addKartWrapper.php;lavukart/karts_addKart.php&c_name=karts_addKartWrapper;parts_addKart&dims=112,200,423,374;112,200,423,374";
		
}//add_kart()

</script>		

	</head>
	
	<body>
	
	
	
	
	
	
	
	
	
	
	
		<?php
		
				echo "<input type=\"hidden\" name=\"lastTRLit\" id=\"lastTRLit\">"; /*Store the id of the last <td></td> element selected so we can change the background image back to unlit */
				
		?>
		
				
							
		<?php
				
				$locationid = sessvar("loc_id");
				$results = lavu_query("SELECT * FROM lk_karts WHERE locationid=\"[1]\" ORDER BY number*1", $locationid);//multiply numbers by 1 or it orders it as a string
				
				$carIDsAndNumbersArray = array();
				$index = 0;
				/* Build an associative array to call information from */
				while ($rows = mysqli_fetch_assoc($results)){	
							
							$tempCarID = $rows['id'];
							$tempCarNumber = $rows['number'];
													
							$carIDsAndNumbersArray[$index][0] = $tempCarNumber;
							$carIDsAndNumbersArray[$index][1] = $tempCarID;
							
							$index++;
							
				}//while
				
				//sort($carIDsAndNumbersArray); //mysql doesn't order it correctly for some reason
				
				echo "<table>";
				
				
				/* Print out all the karts after specifying number of columns */
				$numberOfColumns = 10; // 10 cars wide
				for ($i=0; $i < count($carIDsAndNumbersArray); $i+= $numberOfColumns){
							
							echo "<tr>";
							
							for ($x=$i; $x<($i+$numberOfColumns); $x++){
										
										//car numbers
										$tempCarNumber = $carIDsAndNumbersArray[$x][0];
										
										//car id
										$tempCarID = $carIDsAndNumbersArray[$x][1];
										$fieldID = "carID" . $tempCarID;
										
										if (isset($tempCarID)){
													$min = 1;
													$max = 9; //number of car images to randomly choose from
										
													//pictures are called eg. car1.png or car2.png, ... etc. There are 9 images at the time I wrote this
													$carRandomImage = "car" . rand($min, $max) . ".png";
										
													echo "<td onClick=\"passDataToOtherComponent($tempCarID); changeBackgroundImage('$tempCarID');\" id=\"$tempCarID\" style=\"background-image:url(images/" . $carRandomImage . ");\");\">$tempCarNumber</td>"; 
										}//if
										
										/* Once we run out of car id's in the $carIDsAndNumbersArray array, stop printing car pictures in the table */
										if (!isset($tempCarID)){
													echo "<td></td>";
										}//if
										
							}//for
						
							echo "</tr>";
							
				}//for
					
				echo "</table>";
		?>
			
			<input type="hidden" id="loc_id" value="<?php echo $locationid;?>"/>
	</form>
		
		
	</body>
</html>