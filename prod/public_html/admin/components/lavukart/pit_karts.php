<html>
	<head>
		<title>Untitled Document</title>
		<style type="text/css">
<!--
body {
	background-color: transparent;
	margin-left: 0px;
	margin-top: 0px;
	margin-right: 0px;
	margin-bottom: 0px;
}

.carnumber {	font-family: Verdana, Geneva, sans-serif;
	font-size: 13px;
	font-weight: bold;
	color: #FFF;
}
.creditnumber {	font-family: Verdana, Geneva, sans-serif;
	font-size: 13px;
	font-weight: bold;
	color: #3d4f6d;
}
.nametext {	font-family: Verdana, Geneva, sans-serif;
	font-size: 13px;
	font-weight: bold;
}
.nametext_grey {	font-family: Verdana, Geneva, sans-serif;
	font-size: 13px;
	font-weight: bold;
	color: #777777;
}
.gridposition {	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 14px;
	font-weight: bold;
	color: #5A75A0;
}
.icontext {font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 11px;
	font-weight: bold;
	color: #5A75A0;
}
.subtitles {	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 14px;
	font-weight: bold;
	color: #5A75A0;
}
.title1 {	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 18px;
	font-weight: bold;
	color: #5A75A0;
}
-->
		</style>
	</head>

<?php
	require_once(dirname(__FILE__) . "/../comconnect.php");
	
?>

	<body>
		<table border="0" cellpadding="0" cellspacing="0"><tr>
			<?php
				require_once(dirname(__FILE__) . "/lk_functions.php");
				$max_cols = 7;
				$col = 1;
				
				if(reqvar("loc_id")) $loc_id = reqvar("loc_id");
				else if(sessvar_isset("loc_id")) $loc_id = sessvar("loc_id");
				set_sessvar("loc_id", $loc_id);
			
				$max_group = 1;
				$kart_query = lavu_query("SELECT * FROM `lk_karts` WHERE `locationid` = '".sessvar("loc_id")."' order by (`number` * 1) asc");
				while($kart_read = mysqli_fetch_assoc($kart_query))
				{
					$nametext_class = "nametext_grey";
											
					$lane_calc = ($kart_read['lane'] % 6);	
					$kart_img = "car.png";	
					switch ($lane_calc) {
						 case 1 : $kart_img = "car1.png"; break;
						 case 2 : $kart_img = "car2.png"; break;
						 case 3 : $kart_img = "car3.png"; break;
						 case 4 : $kart_img = "car4.png"; break;
						 case 5 : $kart_img = "car6.png"; break;
						 case 6 : $kart_img = "car7.png"; break;
						 case 7 : $kart_img = "car8.png"; break;
						 case 8 : $kart_img = "car9.png"; break;
					}
					//$next_group = (((int)$kart_read['race_group'] % $max_group) + 1);

					echo "<td style='width:58px; height:42px' align='left' valign='top' class='subtitles'>
						<table width='44' height='36' border='0' cellspacing='0' cellpadding='6' background='images/".$kart_img."'>
							<tr><td align='center' valign='bottom' class='carnumber' onclick='window.location = \"_DO:cmd=send_js&c_name=pit_racers&js=assign_row_selected(".$kart_read['id'].")\";'>".$kart_read['number']."</td></tr>
						</table>
					</td>";
					$col++;
					if($col > $max_cols)
					{
						echo "</tr><tr>";
						$col = 1;
					}
				}
			?>
       </tr></table>
	</body>
</html>