// JavaScript Document



function addPartScript(){
		/* This function is triggered by the list/menu element in the form. It adds the select part*/
		
				/* If the part is already there, do not add it again or the page will not work anymore since identical names and ids get added */
				var curPartIDToBeAdded = document.addServiceForm.addPartsToService.value;
				
				/* Get part ids x quantity list */
				var hayStack = document.addServiceForm.partIDsAndQty.value;//find  'needle' in this haystack ha ha
				hayStackPieces = hayStack.split(",");
				
				for (i=0; i< hayStackPieces.length; i++){
							var curPartIDPieces = hayStackPieces[i].split("@");
							
							var curPartID = curPartIDPieces[0];
						
							if (curPartIDToBeAdded == curPartID){
										
										window.location = '_DO:cmd=alert&title=Part Already In Svc.&message=The part has already been added.';
										
										//reset the list menu for another selection
										document.getElementById('addPartsToService').value = 0;
										return false;
							}//if
							
				}//for
				
				
		
				var storeListMenuHTHML = document.getElementById('addPartsToService').innerHTML;
				
				var currentHTML = document.getElementById('holdSelectedParts').innerHTML;
				var partToAddID =  document.addServiceForm.addPartsToService.value;
			
				partName = getPartName(partToAddID);
				oPicID = "oPic" + partToAddID;
				
				/* If you change something here, remember to change the php version of this section in 	service_types_formInclude.php */
				currentHTML = currentHTML + "<tr><td width=\"300px\" style=\"text-align:right;\">" + partName + "</td><td width=\"100px\"><input type=\"hidden\" name=\"addThisPart" + partToAddID +"\" value=\"" + partToAddID +"\" /><div style=\"clear:both;\"></div> <input type=\"text\"  onchange=\"changeOPic(" + partToAddID + ");\" class=\"smallerInput\" value=\"1\" name=\"addPartToSvcQty_" + partToAddID + "\"/></td><td id=\"" + oPicID + "\"><img src=\"images/o_soft.png\" width=\"28\" height=\"29\" onclick=\"setQuantityToZero('" + partToAddID + "')\"></td><td width=\"50px\"><img src=\"images/btn_arrow_back.png\" width=\"37\" height=\"34\" onclick=\"subtractOne('" + partToAddID + "');\"></td><td width=\"50px\"><img src=\"images/btn_arrow.png\" width=\"37\" height=\"34\" onclick=\"addOne('" + partToAddID + "');\"></td></tr>";
				
				
		
				
				
				document.getElementById('holdSelectedParts').innerHTML = currentHTML;
				
			
				
				//reset the list menu for another selection
				document.getElementById('addPartsToService').value = 0;
				
				/* Add the new partID and quantity to the hidden text field so it can be submitted to database */
				var getPartIDandQtyList = document.addServiceForm.partIDsAndQty.value;
				newGetPartIDandQtyList = getPartIDandQtyList + partToAddID + "@" + "1,"; //one is the default quantity entered into text field
				
				document.addServiceForm.partIDsAndQty.value = newGetPartIDandQtyList;
				
	}//addPartScript()
	
	
	function getPartName(id){
		/* This functions parses the string from the storeValsAsString hidden text field and creates a array with array[0] = partId, array[1] = partName*/
		
		parseThisString = document.addServiceForm.storeValsAsString.value;
		
		parseThisStringSplit = parseThisString.split(",");
		
		for (i=0; i<parseThisStringSplit.length;i++){
					
				
					getCurrentPartIDAndName = parseThisStringSplit[i].split("@");
					
					curPartID = getCurrentPartIDAndName[0];
				
					if (id == curPartID){
								curPartName = getCurrentPartIDAndName[1];
								return curPartName;
					}//if
			
		}//for
		
		return false;
		
		
	}//createMemoryDB()
	
	
	function addOne(id){
		/* Function adds one to the current quantity */
		
				var qtyName = "addPartToSvcQty_" + id;
		
				var qty = eval("document.addServiceForm." + "addPartToSvcQty_" + id + ".value");
				qty = parseInt(qty, 10) + 1;
		
				eval("document.addServiceForm." + "addPartToSvcQty_" + id + ".value=qty;");
				
				//update the quantity in the name=partIDsAndQty hidden text field
				updateQuantity(id);
				
				setOPicToGrey(id);
		
	}//addOne()
	
	function subtractOne(id){
		/* Function subtracts one from the current quantity */
		
				var currentValue = eval("document.addServiceForm." + "addPartToSvcQty_" + id + ".value");
				var oPicID = "oPic" + id;
				// no negative numbers
				if (currentValue != 0){	
						var qtyName = "addPartToSvcQty_" + id;
		
						var qty = eval("document.addServiceForm." + "addPartToSvcQty_" + id + ".value");
						qty = parseInt(qty, 10) - 1;
						eval("document.addServiceForm." + "addPartToSvcQty_" + id + ".value=qty;");
						
						
						if (qty == "0"){	
									setOPicToOrange(id);
						}//if
				}//if
				
			
		
				//update the quantity in the name=partIDsAndQty hidden text field
				updateQuantity(id);
		
	}//subtractOne()
	
	
	function validateForm(){
			/* Ensure the user doesn't do anything too crazy when submitting the form */
			
		name = document.addServiceForm.name.value;
		description = document.addServiceForm.Description.value;
		
		manHours = document.addServiceForm.manHours.value;
		
		
		
		var canSubmitForm = true;
		
		if (name == ""){
					canSubmitForm = false;
		}//if
		
		if (description == ""){
					canSubmitForm = false;
		}//if
		
		if (manHours == ""){
					canSubmitForm = false;
		}//if
		
		if (isNaN(manHours) ==  true){
					alert("Entries for 'Man Hours' must be a number.");
					canSubmitForm = false;
					return false; //why go on?
		}//if
		
		if (canSubmitForm == false){
					window.location = '_DO:cmd=alert&title=Missing Values&message=Please fill in values for Name, Description, and Man hours.';
					return false;
		}//if
			
			/* Strip the last comma off of partIDsAndQty value. If we don't it adds a blank part */
			
			/* the listing of part ids and quantities (IDxquantity);*/
				var getPartIDandQtyList = document.addServiceForm.partIDsAndQty.value;
				var strLength = String(getPartIDandQtyList).length;
			
			
				var lastChar = getPartIDandQtyList.charAt(strLength-1);
		
				getPartIDandQtyListNew = "";
				
				
				if (lastChar == ","){
							for (i=0;i < (strLength-1);i++){
										getPartIDandQtyListNew = getPartIDandQtyListNew + String(getPartIDandQtyList).charAt(i);
							}//for
				}//if
		
				document.addServiceForm.partIDsAndQty.value = getPartIDandQtyListNew;
	
				return true;
				
	}//validateForm()
	
	
	function changeOPic(id){
		/* If the quantity text field was changed from 0 to any value with the keyboard, the o.png (orange) needs to be changed to o_soft.png (grey) */
				
				qty = eval("document.addServiceForm." + "addPartToSvcQty_" + id + ".value");
				
				/* Here we check to make sure they didn't enter a wierd value into the quantities field for the part that triggered this script */
				if (ensureQuantityFormat(qty) == false){
							alert("Entries for quantities must be positive whole numbers.");
							
							/*set the field to a non-harmful value, but it doesn't matter anyway cause we return false before the partIDsAndQuantities list gets updated*/
							eval("document.addServiceForm." + "addPartToSvcQty_" + id + ".value = 0");
							return false; //exit the script, do not update their bogus value into the partIDsAndQuantities list
				}//
				
				if (qty != 0){			
							updateQuantity(id); //update quantity change in parts list
							setOPicToGrey(id);
				}//if
				
				
	}//changeOPic()
	
	
	function ensureQuantityFormat(number){
		/* Called on values entered into the text field for Quantity, ensures proper format. Format is a whole number. */
		
			/* Is whole number */
			if (number == Math.round(number)){
				 return true;
			}//if 
			
			/* Is not whole number */
			if (number != Math.round(number)){
				return false;
			}//if 
			
}//ensureQuantityFormat()
	
	
	
	function updateQuantity(id){
				/* This function updates the quantity in the partIDsAndQty text field value */
				//alert("id: " + id);
				var curPartIDsAndQty = document.addServiceForm.partIDsAndQty.value;
				
				var pieces = curPartIDsAndQty.split(",");
					
				for (i=0; i<curPartIDsAndQty.length; i++){
					
							var curVal = pieces[i];
							var curValPieces = curVal.split("@");
							var curValID = curValPieces[0];
							//alert("newCurVal: " + curValID);
							if (curValID == id){
										var newQuantity = eval("document.addServiceForm." + "addPartToSvcQty_" + id + ".value");
										newCurVal = curValID + "@" + newQuantity;
									
										pieces[i] = newCurVal;
										newPartIDsAndQty = pieces.join(",");
										document.addServiceForm.partIDsAndQty.value = newPartIDsAndQty;
										return true;
							}//if
					
				}//for
				
				return false; //something didn't work right
				
	}//updateQuantity()
	
	
	function setOPicToGrey(id){
		/* turn the o pic to grey (o_soft.png). Used when the quantity value is changed from zero to some positive whole number */
		
				oPicID = "oPic" + id;
				document.getElementById(oPicID).innerHTML = "<img src=\"images/o_soft.png\" width=\"28\" height=\"29\" onclick=\"setQuantityToZero(" + id +")\">";
				
	}//setOPicToOrage()
	
	
	
	function setOPicToOrange(id){
		/* turn the o pic to orange (o.png). Used when the quantity value is set to zero */
		
				oPicID = "oPic" + id;
				document.getElementById(oPicID).innerHTML = "<img src=\"images/o.png\" width=\"28\" height=\"29\" onclick=\"setQuantityToZero(" + id +")\">";
				
	}//setOPicToOrage()
	
	
	function setQuantityToZero(id){
		/* When the "@" is clicked, set the quantity to 0 */
		eval("document.addServiceForm." + "addPartToSvcQty_" + id + ".value = 0");
		updateQuantity(id);
		
		setOPicToOrange(id);
		
	}//setQuantityToZero()
