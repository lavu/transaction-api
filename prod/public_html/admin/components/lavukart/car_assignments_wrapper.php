<html>
	<head>
		<title>Untitled Document</title>
		<style type="text/css">
<!--
body {
	background-color: transparent;
	margin-left: 0px;
	margin-top: 0px;
	margin-right: 0px;
	margin-bottom: 0px;
	-webkit-tap-highlight-color:rgba(0,0,0,0);
}

.carnumber {	font-family: Verdana, Geneva, sans-serif;
	font-size: 13px;
	font-weight: bold;
	color: #FFF;
}
.creditnumber {	font-family: Verdana, Geneva, sans-serif;
	font-size: 13px;
	font-weight: bold;
	color: #3d4f6d;
}
.nametext {	font-family: Verdana, Geneva, sans-serif;
	font-size: 13px;
	font-weight: bold;
}
.gridposition {	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 14px;
	font-weight: bold;
	color: #5A75A0;
}
.icontext {font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 11px;
	font-weight: bold;
	color: #5A75A0;
}
.subtitles {	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 14px;
	font-weight: bold;
	color: #5A75A0;
}
.title1 {	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 18px;
	font-weight: bold;
	color: #5A75A0;
}
-->
		</style>
	</head>

<?php
	require_once(dirname(__FILE__) . "/../comconnect.php");
	
	$eventid = 0;
	if(reqvar("eventid")) $eventid = reqvar("eventid");
	else if(sessvar_isset("eventid")) $eventid = sessvar("eventid");
	set_sessvar("eventid",$eventid);
	
	$print_event = reqvar("print");
	if($print_event)
	{
		if(strpos($company_code,"jersey")) $ploc = "p2r_new_jersey%20-%20NYJC";
		else if(strpos($company_code,"DEV")!==false) $ploc = "DEVKART%20-%20Dev%20East%20Side";
		else {
			if(reqvar("loc_id")==9)
				$ploc = "pole_position_raceway%20-%20Murrieta";
			else
				$ploc = "pole_position_raceway%20-%20Oklahoma";
		}
		
		$track_query = lavu_query("SELECT `lk_tracks`.`title` as `track_title` from `lk_events` LEFT JOIN `lk_tracks` ON `lk_events`.`trackid`=`lk_tracks`.`id` WHERE `lk_events`.`id`='[1]'",$eventid);
		if(mysqli_num_rows($track_query))
		{
			$track_read = mysqli_fetch_assoc($track_query);
			$track_title = $track_read['track_title'];
		}
		else $track_title = "";
		
		$print_to = "printer1";
		if(strpos($track_title,"2")!==false || strpos(strtolower($track_title),"second")!==false) $print_to = "printer2";
		else if(strpos($track_title,"3")!==false) $print_to = "printer3";
		else if(strpos($track_title,"4")!==false) $print_to = "printer4";
		
		//mail("corey@poslavu.com","print event","$company_code \n $ploc \nprint to: $print_to \ntrack: $track_title","From: info@poslavu.com");
			$ch = curl_init();
			curl_setopt($ch, CURLOPT_URL, "http://admin.poslavu.com/print/index.php?location=$ploc&print_to=".$print_to."&print=".$print_event);
			//curl_setopt($ch, CURLOPT_VERBOSE, 1);
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
			curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt($ch, CURLOPT_POST, 1);
			//curl_setopt($ch, CURLOPT_POSTFIELDS, $nvpreq);
			$httpResponse = curl_exec($ch);
	}

	$lane_number = 1;
	if(reqvar("lane_number")) $lane_number = reqvar("lane_number");
	else if(sessvar_isset("lane_number")) $lane_number = sessvar("lane_number");
	set_sessvar("lane_number",$lane_number);

	$race_title = "";
	$trackid = "";

	$get_race_info = lavu_query("SELECT `lk_event_types`.`title` as `title`, `lk_events`.`trackid` as `trackid` FROM `lk_events` LEFT JOIN `lk_event_types` ON `lk_events`.`type` = `lk_event_types`.`id` WHERE `lk_events`.`id` = '".$eventid."'");
	if (@mysqli_num_rows($get_race_info) > 0) {
		$extract = mysqli_fetch_array($get_race_info);
		$race_title = $extract['title'];
		$trackid = $extract['trackid'];
	}
	
	$startrace = reqvar("startrace");
	if($startrace && $startrace==$eventid)
	{
		lavu_query("update `lk_events` set `ms_start`='".date("Y-m-d H:i:s")."' where `id`='[1]'",$startrace);
	}
	$endrace = reqvar("endrace");
	if($endrace)
	{
		lavu_query("update `lk_events` set `ms_end`='".date("Y-m-d H:i:s")."' where `id`='[1]'",$endrace);
	}
	$continue_race = reqvar("continue_race");
	if($continue_race)
	{
		function time_to_ts($time)
		{
			$parts = explode(" ",$time);
			$date_parts = explode("-",$parts[0]);
			$time_parts = explode(":",$parts[1]);
			$ts = mktime($time_parts[0],$time_parts[1],$time_parts[2],$date_parts[1],$date_parts[2],$date_parts[0]);
			return $ts;
		}
		
		$e_query = lavu_query("SELECT * from `lk_events` where `id`='[1]'",$continue_race);
		if(mysqli_num_rows($e_query))
		{
			$e_read = mysqli_fetch_assoc($e_query);
			
			$ts_now = time();
			$ts_end = time_to_ts($e_read['ms_end']);
			$ts_span = $ts_now - $ts_end;
			$pause_data = $ts_now . "-" . $ts_end . "=" . $ts_span;
			lavu_query("update `lk_events` set `ms_end`='', `pauses`=CONCAT(`pauses`,'$pause_data ') where `id`='[1]'",$continue_race);
		}
	}
	
	$race_active = false;
	$active_query = lavu_query("select * from `lk_events` where `ms_start`!='' and `ms_end`='' and `trackid`='[1]'",$trackid);
	if(mysqli_num_rows($active_query))
	{
		$active_read = mysqli_fetch_assoc($active_query);
		$active_raceid = $active_read['id'];
		$race_active = true;
	}
	
	$event_exists = false;
	$event_active = false;
	$event_query = lavu_query("select * from `lk_events` where `id`='[1]'",$eventid);
	if(mysqli_num_rows($event_query))
	{
		$event_exists = true;
		$event_active = true;
		$event_read = mysqli_fetch_assoc($event_query);
		if($event_read['ms_end']!="")
			$event_active = false;
	}
?>

	<body>
		<table width="474" height="598" border="0" cellpadding="0" cellspacing="0" >
			<tr>
				<td height="40" colspan="2">
					<table width="474" border="0" cellpadding="0" cellspacing="0">
						<tr>
							<td width="79">&nbsp;</td>
							<td width="343" align="center" valign="middle" class="title1"><?php if($eventid > 0) echo $race_title." - Lane ".$lane_number; ?></td>
							<td width="52">&nbsp;</td>
						</tr>
					</table>
				</td>
			</tr>
			<tr>
				<td width="79" height="536" align="left" valign="top">
					<table width="79" border="0" cellpadding="0" cellspacing="0">
						<tr>
							<td width="11">&nbsp;</td>
							<td height="26">&nbsp;</td>
						</tr>
						<tr>
							<td width="11">&nbsp;</td>
							<td>
								<table width="68" border="0" cellpadding="0" cellspacing="0">
									<tr><td height="32" align="center" valign="middle" class="subtitles"><span class="icontext">RACES</span></td></tr>
									<tr><td align="center" valign="middle"><a onclick='location = "_DO:cmd=create_overlay&f_name=lavukart/events_overlay_wrapper.php;lavukart/events.php&c_name=events_overlay_wrapper;events&dims=263,90,494,598;348,130,420,540&scroll=0;1?test1=1&test2=2";'><img src="images/btn_races.png" width="68" height="68" /></a></td></tr>
								</table>
							</td>
						</tr>
						<?php if($event_exists) { ?>
						<tr>
							<td width="11">&nbsp;</td>
							<td height="110" align="left" valign="bottom">
								<table width="68" border="0" cellpadding="0" cellspacing="0">
									<tr><td height="32" align="center" valign="middle" class="subtitles"><a onclick='location = "_DO:cmd=show_pit_setup";'><img src="images/btn_grid.png" width="68" height="68" /></a></td></tr>
									<!--$ <tr><td height="32" align="center" valign="middle" class="subtitles"><a onclick='location = "_DO:cmd=create_overlay&f_name=lavukart/pit_grid_overlay_wrapper.php;lavukart/pit_grid.php&c_name=pit_grid_overlay_wrapper;pit_grid&dims=275,90,474,598;302,130,420,540&scroll=0;1?test1=1&test2=2";'><img src="images/btn_grid.png" width="68" height="68" /></a></td></tr> $-->
									<tr><td height="32" align="center" valign="middle"><span class="icontext">GRID</span></span></td></tr>
								</table>
							</td>
						</tr>
						<?php } ?>
						<?php if($event_active) {
									if($race_active) { 
						?>
						<tr>
							<td width="11">&nbsp;</td>
							<td height="110" align="left" valign="bottom">
								<table width="68" border="0" cellpadding="0" cellspacing="0">
									<tr><td height="32" align="center" valign="middle" class="subtitles">
                                    <a onclick='location = "_DO:cmd=load_url&c_name=car_assignments_wrapper&f_name=lavukart/car_assignments_wrapper.php?endrace=<?php echo $active_raceid?>";'><img src="images/btn_endrace.png" width="68" height="68" /></a></td></tr>
									<!--$ <tr><td height="32" align="center" valign="middle" class="subtitles"><a onclick='location = "_DO:cmd=create_overlay&f_name=lavukart/pit_grid_overlay_wrapper.php;lavukart/pit_grid.php&c_name=pit_grid_overlay_wrapper;pit_grid&dims=275,90,474,598;302,130,420,540&scroll=0;1?test1=1&test2=2";'><img src="images/btn_grid.png" width="68" height="68" /></a></td></tr> $-->
									<tr><td height="32" align="center" valign="middle"><span class="icontext">STOP</span></span></td></tr>
								</table>
							</td>
						</tr>
						<?php 		} else { ?>
						<tr>
							<td width="11">&nbsp;</td>
							<td height="110" align="left" valign="bottom">
								<table width="68" border="0" cellpadding="0" cellspacing="0">
									<tr><td height="32" align="center" valign="middle" class="subtitles"><a onclick='location = "_DO:cmd=load_url&c_name=car_assignments_wrapper&f_name=lavukart/car_assignments_wrapper.php?startrace=<?php echo $eventid?>";'><img src="images/btn_startrace.png" width="68" height="68" /></a></td></tr>
									<!--$ <tr><td height="32" align="center" valign="middle" class="subtitles"><a onclick='location = "_DO:cmd=create_overlay&f_name=lavukart/pit_grid_overlay_wrapper.php;lavukart/pit_grid.php&c_name=pit_grid_overlay_wrapper;pit_grid&dims=275,90,474,598;302,130,420,540&scroll=0;1?test1=1&test2=2";'><img src="images/btn_grid.png" width="68" height="68" /></a></td></tr> $-->
									<tr><td height="32" align="center" valign="middle"><span class="icontext">START</span></span></td></tr>
								</table>
							</td>
						</tr>
						<?php } } else if(!$race_active && $event_exists) { ?>
						<tr>
							<td width="11">&nbsp;</td>
							<td height="110" align="left" valign="bottom">
								<table width="68" border="0" cellpadding="0" cellspacing="0">
									<tr><td height="32" align="center" valign="middle" class="subtitles">
										<a onclick='location = "_DO:cmd=load_url&c_name=car_assignments_wrapper&f_name=lavukart/car_assignments_wrapper.php?continue_race=<?php echo $eventid?>";'><img src="images/btn_startrace.png" width="68" height="68" /></a>
								</td></tr>
									<!--$ <tr><td height="32" align="center" valign="middle" class="subtitles"><a onclick='location = "_DO:cmd=create_overlay&f_name=lavukart/pit_grid_overlay_wrapper.php;lavukart/pit_grid.php&c_name=pit_grid_overlay_wrapper;pit_grid&dims=275,90,474,598;302,130,420,540&scroll=0;1?test1=1&test2=2";'><img src="images/btn_grid.png" width="68" height="68" /></a></td></tr> $-->
									<tr><td height="32" align="center" valign="middle"><span class="icontext">CONTINUE</span></span></td></tr>
								</table>
							</td>
						</tr>
						<?php } ?>
						<?php if($event_exists) { ?>
						<tr>
							<td width="11">&nbsp;</td>
							<td height="110" align="left" valign="bottom">
								<table width="68" border="0" cellpadding="0" cellspacing="0">
									<tr><td height="32" align="center" valign="middle" class="subtitles"><a onclick='location = "_DO:cmd=load_url&c_name=car_assignments_wrapper&f_name=lavukart/car_assignments_wrapper.php?print=<?php echo $eventid?>";'><img src="images/btn_printer.png" width="68" height="68" /></a></td></tr>
									<!--$ <tr><td height="32" align="center" valign="middle" class="subtitles"><a onclick='location = "_DO:cmd=create_overlay&f_name=lavukart/pit_grid_overlay_wrapper.php;lavukart/pit_grid.php&c_name=pit_grid_overlay_wrapper;pit_grid&dims=275,90,474,598;302,130,420,540&scroll=0;1?test1=1&test2=2";'><img src="images/btn_grid.png" width="68" height="68" /></a></td></tr> $-->
									<tr><td height="32" align="center" valign="middle"><span class="icontext">PRINT RESULTS</span></span></td></tr>
								</table>
							</td>
						</tr>
						<?php } ?>
					</table>
				</td>
				<td align="left" valign="top">&nbsp;</td>
			</tr>
		</table>
	</body>
	<?php if($print_event) { ?>
	<script language="javascript">
		alert("Printing...");
	</script>
	<?php } ?>
</html>