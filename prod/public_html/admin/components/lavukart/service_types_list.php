<?php
			require_once(dirname(__FILE__) . "/../comconnect.php");
			require_once(dirname(__FILE__) . "/./part_functions.php");
			if ($access_level < 3){
					echo "<script type='text/javascript'>
						alert('You do not have sufficient priveleges to access this tab.');
					</script>";
					exit();
			}//if
?>

<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<title>Garage List</title>

		<style type="text/css">
<!--
body {
	background-color: transparent;
	margin-left: 60px;
	margin-top: 0px;
	margin-right: 0px;
	margin-bottom: 0px;
	-webkit-tap-highlight-color:rgba(0,0,0,0);
	font-family:Verdana, Geneva, sans-serif;
	color:#333;
}



table, th 
{
border: none;
border-collapse:collapse;
padding:0;
width:412px;
color:#333;
font-size:10pt;

margin:0;


}

td{
	border:0px solid #033;
	border-collapse:collapse;
	padding:0;
	color:#333;
	font-size:10pt;
	margin:0;
}


tr{
			height:auto;
			width:412px;
		
		
}

div{
	font-size:10pt;
}


.trCaps{
	height:9px; 
	width:412px;
	
}

.centerCell{
	width:412px;
	
	background-image:url(images/tab_v1_mid.png);
	background-repeat:repeat-y;
	
	height:auto;
	
}

.topCap{
	background-image:url(images/tab_v1_top.png);
	background-repeat:no-repeat;
	height:9px;
}


.bottomCap{
	background-image:url(images/tab_v1_bot.png);
	background-repeat:no-repeat;
	height:9px;
}

.allDivs{
	float:left;
	border:0px solid #000;
	padding-left:5px;
}

.leftForm{
	float:left;
	width:220px;
	text-align:right;
}

.rightForm{
	float:left;
	text-align:left;
	padding-left:5px;
	width:30px;
}

-->
		</style>
		
		

		

<script type="text/javascript">
	
	
	
	
function addService(){
		loc_id = document.getElementById("loc_id").value;
	//	alert(loc_id);
		window.location = "_DO:cmd=create_overlay&f_name=lavukart/service_types_addServiceTypeWrapper.php;lavukart/service_types_addServiceType.php&c_name=service_types_addServiceTypeWrapper;service_types_addServiceType&dims=107,150,774,748;107,150,774,748?loc_id="+loc_id;
}//addService()
	
	
	function changeBackgroundImage(id){
			/* function changes background image to 'lit' or 'unlit' depending on what is appropriate, the id input parameter is the id assigned to each <table></table> element. it is the same as the part id since all parts have unique 	          IDs*/
			
			var lastRowLit = document.getElementById('lastTRLit').value;
			
			/* If a row is lit this hidden field will contain a value, the value is the id of the last selected <tr></tr> */
			if (lastRowLit != ""){
					
					/* change back to 'unlit' image */
						var topCapID = "topCap" + lastRowLit;
					
						document.getElementById(topCapID).style.backgroundImage="url('images/tab_v1_top.png')";
			
						document.getElementById(lastRowLit).style.backgroundImage="url('images/tab_v1_mid.png')";
			
						var botCapID = "botCap" + lastRowLit;
						document.getElementById(botCapID).style.backgroundImage="url('images/tab_v1_bot.png')";
		 
			}//if
			
			/* Change background image of current selected table row to the 'lit' image */
			var topCapID = "topCap" + id;
			document.getElementById(topCapID).style.backgroundImage="url('images/tab_v1_toplit.png')";
			
			document.getElementById(id).style.backgroundImage="url('images/tab_v1_midlit.png')";
			
			var botCapID = "botCap" + id;
			document.getElementById(botCapID).style.backgroundImage="url('images/tab_v1_botlit.png')";
		
			/* store id in hidden text field so we can switch that row off later */
			document.getElementById('lastTRLit').value = id; 
			
	}//changeBackgroundImage()
	
	function passDataToPartInfo(id){
				loc_id = document.getElementById("loc_id").value;
				//alert(loc_id);
				window.location = "_DO:cmd=load_url&c_name=service_types_info&f_name=lavukart/service_types_info.php?id=" + id + "&loadRightSide=true&loc_id="+loc_id;
	}//passDataToGarageInfo()

</script>		

	</head>
	
	<body>
	
	<form action="" method="post" onSubmit="sayHi();" name="garageListForm" id="frm1">
	
	
	
		<?php
		
				echo "<input type=\"hidden\" name=\"lastTRLit\" id=\"lastTRLit\">"; /*Store the id of the last <tr></tr> element selected so we can change the background image back to unlit */
				
		?>
		
		<table>
				
							
		<?php
			
				$locationid = sessvar("loc_id");
			
				$results = lavu_query("SELECT * FROM lk_service_types WHERE locationid=\"[1]\" ORDER BY name", $locationid);
				
				while ($rows = mysqli_fetch_assoc($results)){	
						$id = $rows['id'];	
						
						$topCapID = "topCap" . $id;
						$botCapID = "botCap" . $id;
						
						echo "<div class=\"topCap\" id=\"$topCapID\"></div>";
						
			
							
						echo "<div id=\"$id\" onclick=\"passDataToPartInfo('$id'); changeBackgroundImage('$id');\" style=\"background-image:url(images/tab_v1_mid.png); background-repeat:repeat-y; width:612px;\">";
							
							
							
							//$rows = mysqli_fetch_assoc($results);
										
										
								
										echo "<div class=\"allDivs\" style=\"width:250px;\"><span style=\"font-weight:700;\">" . showQuote($rows['name']) . "</span></div>";
										
									//	echo "<div class=\"allDivs\" style=\"width:45px;\" >" . showQuote($rows['man_hours']) . "</div>";
							
							
										echo "<div class=\"allDivs\"  style=\"width:275px;\">";
							
										/* List parts. Parts are stored in database as quantity x part id */
										$partsStr = $rows['parts'];
										$partsStrExploded = explode(",", $partsStr);
							
										$lengthOfPartsStrExploded = count($partsStrExploded);
										//$storePartsQuantityAndID; //$storePartsQuantityAndID[0] = part id; $storePartsQuantityAndID[1] = quantity
							
										$totalCost = 0;
										for ($i=0; $i<$lengthOfPartsStrExploded; $i++){
													$currentPart = $partsStrExploded[$i];
													$currentPartExploded = explode("@", $currentPart);
													$partID = $currentPartExploded[0];
													$quantity = $currentPartExploded[1];
										
													$resultsGetPartInfo = lavu_query("SELECT * FROM lk_parts WHERE id=\"[1]\" AND locationid=\"[2]\"", $partID, $locationid);
										
										
													$partRows = mysqli_fetch_assoc($resultsGetPartInfo);
													$partName = showQuote($partRows['name']);
													$costOfThisPart = showQuote($partRows['cost']);
													$totalCost = $totalCost + ($quantity * $costOfThisPart); 
													echo "
													<div class=\"leftForm\">$partName</div>
													<div class=\"rightForm\"></div>
													<div style=\"clear:both;\"></div>";
											
										
										
										
										}//for
										
								echo "</div>";
							
							
							 
							
										echo "<div class=\"allDivs\"  style=\"width:55px; text-align:right;\" ></div>";
										echo "<div style=\"clear:both;\"></div>";
								echo "</div>";
							
					
						
						echo "<div class=\"bottomCap\" id=\"$botCapID\"></div>";
						
				}//while
				
		?>
							
				
		</table> 
		<input type="hidden" id="loc_id" value="<?php echo $locationid;?>"/>
		</form>
		
		
		
		
	</body>
</html>