<html>
	<head>
		<title>Untitled Document</title>
		<style type="text/css">
<!--
body {
	background-color: transparent;
	margin-left: 0px;
	margin-top: 0px;
	margin-right: 0px;
	margin-bottom: 0px;
}

.carnumber {	font-family: Verdana, Geneva, sans-serif;
	font-size: 13px;
	font-weight: bold;
	color: #FFF;
}
.creditnumber {	font-family: Verdana, Geneva, sans-serif;
	font-size: 13px;
	font-weight: bold;
	color: #3d4f6d;
}
.nametext {	font-family: Verdana, Geneva, sans-serif;
	font-size: 13px;
	font-weight: bold;
}
.nametext_grey {	font-family: Verdana, Geneva, sans-serif;
	font-size: 13px;
	font-weight: bold;
	color: #777777;
}
.gridposition {	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 14px;
	font-weight: bold;
	color: #5A75A0;
}
.icontext {font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 11px;
	font-weight: bold;
	color: #5A75A0;
}
.subtitles {	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 14px;
	font-weight: bold;
	color: #5A75A0;
}
.title1 {	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 18px;
	font-weight: bold;
	color: #5A75A0;
}
-->
		</style>
		<script language="javascript">
		
			<?php
				$row_selected = 1;
				if (isset($_POST['row_selected'])) {
					$row_selected = $_POST['row_selected'];
				}
				$js_schedule_ids = "";
				$js_next_row_select = "";
				echo "row_selected = ".$row_selected."; ";
			?>
		
			function deassign_kart(rowid) {
				window.location = "_DO:cmd=load_url&c_name=pit_racers&f_name=lavukart/pit_racers.php?deassign_kart=" + schedule_ids[rowid] + "&row_selected=" + row_selected;
			}
			
			function assign_row_selected(kartid) {
				window.location = "_DO:cmd=load_url&c_name=pit_racers&f_name=lavukart/pit_racers.php?assign_kart=" + kartid + "&set_scheduleid=" + schedule_ids[row_selected] + "&row_selected=" + next_row_select[row_selected];
			}
		</script>
	</head>

<?php
	require_once(dirname(__FILE__) . "/../comconnect.php");
	
	$eventid = 0;
	if(reqvar("eventid")) $eventid = reqvar("eventid");
	else if(sessvar_isset("eventid")) $eventid = sessvar("eventid");
	set_sessvar("eventid",$eventid);
	
	if(reqvar("loc_id")) $loc_id = reqvar("loc_id");
	else if(sessvar_isset("loc_id")) $loc_id = sessvar("loc_id");
	set_sessvar("loc_id", $loc_id);

	$max_group = 1;
	$get_karts = lavu_query("SELECT `position` FROM `lk_karts` WHERE `locationid` = '".sessvar("loc_id")."'");
	if (mysqli_num_rows($get_karts) > 0) {
		while ($extract_k = mysqli_fetch_array($get_karts)) {
			if ((floor((int)$extract_k['position'] / 2) > $max_group) && (floor((int)$extract_k['position'] / 2) <= 3)) {
				$max_group = floor((int)$extract_k['position'] / 2);
			}
		}
	}

	if(isset($_POST['assign_kart']))
	{
		$current_query = lavu_query("SELECT * FROM `lk_event_schedules` where `eventid`='[1]' and `kartid`='[2]'",$eventid,$_POST['assign_kart']);
		if(mysqli_num_rows($current_query))
		{
			lavu_query("UPDATE `lk_event_schedules` SET `kartid`='' where `eventid`='[1]' and `kartid`='[2]'",$eventid,$_POST['assign_kart']);
		}
		lavu_query("UPDATE `lk_event_schedules` SET `kartid`='[1]' where `id`='[2]'",$_POST['assign_kart'],$_POST['set_scheduleid']);
	}
	if (isset($_POST['deassign_kart'])) 
	{
		lavu_query("UPDATE `lk_event_schedules` SET `kartid`='' where `id`='[1]'",$_POST['deassign_kart']);
		//$deassign_racer = lavu_query("UPDATE `lk_event_schedules` SET `kartid` = '' WHERE `eventid` = '".$eventid."' AND `customerid` = '".$_POST['deassignid']."'");
	}
	
	$mode = reqvar("mode");
	
	if ($mode == "set_group") {
		//echo "here"; 
		//mail("richard@greenkeyconcepts.com","deBug","UPDATE `lk_event_schedules` SET `group` = '".reqvar("new_group")."' WHERE `customerid` = '".reqvar("customer_id")."' AND `eventid` = '".$eventid."'");
		lavu_query("UPDATE `lk_event_schedules` SET `group` = '".reqvar("new_group")."' WHERE `customerid` = '".reqvar("customer_id")."' AND `eventid` = '".$eventid."'");
	}

?>

	<body>
		<table width="417" border="0" cellpadding="0" cellspacing="0">
			<?php
				require_once(dirname(__FILE__) . "/lk_functions.php");
				$minrows = 12;
				$maxrows = get_slots_for_event($eventid);//10;
				$rowcount = 0;			
				$overbooked = 0;
				if($maxrows < $minrows)
					$minrows = $maxrows;

				$rlist = array();
				$get_racers = lavu_query("SELECT `lk_customers`.`membership_expiration` as `membership_expiration`, `lk_customers`.`date_created` as `date_created`, `lk_event_schedules`.`customerid` as customerid, `lk_event_schedules`.`kartid` as kartid, `lk_event_schedules`.`id` as event_schedule_id, `lk_event_schedules`.`group` as race_group, `lk_customers`.`f_name` as f_name, `lk_customers`.`l_name` as l_name, `lk_customers`.`credits` as credits, `lk_customers`.`birth_date` as birth_date, `lk_karts`.`lane` as kartlane, `lk_karts`.`number` as kartnumber FROM `lk_event_schedules` LEFT JOIN `lk_customers` ON `lk_event_schedules`.`customerid` = `lk_customers`.`id` LEFT JOIN `lk_karts` ON `lk_event_schedules`.`kartid` = `lk_karts`.`id` WHERE `eventid` = '".$eventid."'");
				while ($extract_r = mysqli_fetch_assoc($get_racers)) {
					$extract_r['pre_placement'] = 888;
					$extract_r['pre_placed'] = false;
					$rlist[] = $extract_r;
				}
				
				$event_query = lavu_query("SELECT * from `lk_events` where `id`='[1]'",$eventid);
				if(mysqli_num_rows($event_query))
				{
					$event_read = mysqli_fetch_assoc($event_query);
					$event_ts = $event_read['ts'];
					$debug = "";
					
					for($rr=0; $rr<count($rlist); $rr++)
					{
						//$debug .= "\n" . "select * from `lk_race_results` LEFT JOIN `lk_events` ON `lk_race_results`.`eventid`=`lk_events`.`id` where `lk_race_results`.`customerid`='".$rlist[$rr]['customerid']."' and `lk_events`.`ms_end`!='' and `lk_events`.`ts`>'".($event_ts * 1 - (60 * 60 * 12))."' order by `ts` desc limit 1";
						
						$results_query = lavu_query("select * from `lk_race_results` LEFT JOIN `lk_customers` ON `lk_race_results`.`customerid`=`lk_customers`.`id` LEFT JOIN `lk_events` ON `lk_race_results`.`eventid`=`lk_events`.`id` where `lk_race_results`.`customerid`='[1]' and `lk_events`.`ms_end`!='' and `lk_events`.`ts`<'[2]' and `lk_events`.`ts`>'[3]' order by `lk_events`.`ts` desc limit 1",$rlist[$rr]['customerid'],$event_ts,($event_ts * 1 - (60 * 60 * 12)));
						if(mysqli_num_rows($results_query))
						{
							$results_read = mysqli_fetch_assoc($results_query);
							$rlist[$rr]['pre_placement'] = $results_read['bestlap'];//place'];
							$rlist[$rr]['pre_placed'] = true;
							$debug .= "\n " . $rlist[$rr]['customerid'] . " " . $results_read['f_name'] . " " . $results_read['l_name'] . " index:$rr" . " = value:" . $results_read['place'];
						}
					}
										
					$r2_list = $rlist;
					$rlist = array();
					while(count($rlist) < count($r2_list))
					{
						$lowest = 99999;
						$lowest_index = "";
						for($n=0; $n<count($r2_list); $n++)
						{
							$r2num = $r2_list[$n]['pre_placement'];
							if($r2num!=98989 && ($r2num < $lowest))
							{
								$lowest = $r2num;
								$lowest_index = $n;
							}
						}
						$rlist[] = $r2_list[$lowest_index];
						$r2_list[$lowest_index]['pre_placement'] = 98989;
						$debug .= "\n add to list: index:" . $lowest_index . " value:$lowest)";
					}
					//if(sessvar("loc_id")==9)
						//mail("corey@meyerwind.com","placement",$debug,"From: info@poslavu.com");
				}
				
				//$get_racers = lavu_query("SELECT `lk_customers`.`membership_expiration` as `membership_expiration`, `lk_customers`.`date_created` as `date_created`, `lk_event_schedules`.`customerid` as customerid, `lk_event_schedules`.`kartid` as kartid, `lk_event_schedules`.`group` as race_group, `lk_customers`.`f_name` as f_name, `lk_customers`.`l_name` as l_name, `lk_customers`.`credits` as credits, `lk_customers`.`birth_date` as birth_date, `lk_karts`.`lane` as kartlane, `lk_karts`.`number` as kartnumber FROM `lk_event_schedules` LEFT JOIN `lk_customers` ON `lk_event_schedules`.`customerid` = `lk_customers`.`id` LEFT JOIN `lk_karts` ON `lk_event_schedules`.`kartid` = `lk_karts`.`id` WHERE `eventid` = '".$eventid."'");
				//while ($extract_r = mysqli_fetch_assoc($get_racers)) {
				
				$rowcount = 0;
				for($rr=0; $rr<count($rlist); $rr++)
				{
					$extract_r = $rlist[$rr];
				
					$birth_date = $extract_r['birth_date'];
					$birth_date1 = $birth_date;
					$bdates = explode("-",$birth_date);
					if(count($bdates) > 2)
					{
						$byear = ($bdates[0] % 100);
						if($byear < 10) $byear = "0" . ($byear * 1);
						$birth_date = $bdates[1] . "/" . $bdates[2] . "/" . $byear;
					}
					else $birth_date = "";
					if($birth_date1!="" && $birth_date1 > date("Y-m-d",mktime(0,0,0,date("m"),date("d"),date("Y") - 18)))
						$cust_is_minor = true;
					else
						$cust_is_minor = false;
											
					$rowcount++;
					if($rr==count($rlist)-1)
						$next_rowcount = 1;
					else
						$next_rowcount = $rowcount + 1;
					$js_next_row_select .= "next_row_select[$rowcount] = $next_rowcount; ";
					
					if($rowcount > $maxrows && $overbooked==0)
					{
						echo "<tr><td bgcolor='#880000' style='font-size:2px'>&nbsp;</td></tr>";
						$overbooked++;
					}
					echo "<tr>
						<td height='46' align='center' valign='top'>
							<table width='417' height='46' border='0' cellpadding='0' cellspacing='0'>
								<tr>";
					if (($extract_r['kartid'] == "") || ($extract_r['kartid'] == "0")) {
						$nametext_class = "nametext";
						echo "<td width='52' align='center' valign='middle' class='subtitles'>&nbsp;</td>";
					} else {
						$nametext_class = "nametext_grey";
												
						$lane_calc = ($extract_r['kartlane'] % 8);	
						$kart_img = "car.png";	
						switch ($lane_calc) {
							 case 1 : $kart_img = "car1.png"; break;
							 case 2 : $kart_img = "car2.png"; break;
							 case 3 : $kart_img = "car3.png"; break;
							 case 4 : $kart_img = "car4.png"; break;
							 case 5 : $kart_img = "car6.png"; break;
							 case 6 : $kart_img = "car7.png"; break;
							 case 7 : $kart_img = "car8.png"; break;
							 case 8 : $kart_img = "car9.png"; break;
						}
						$next_group = (((int)$extract_r['race_group'] % $max_group) + 1);

						echo "<td width='52' align='left' valign='top' class='subtitles' background='images/".$kart_img."' onclick='deassign_kart($rowcount);'>
							<table width='44' height='36' border='0' cellspacing='0' cellpadding='6'>
								<tr><td align='center' valign='bottom' class='carnumber'>".$extract_r['kartnumber']."</td></tr>
							</table>
						</td>";
					}
					$js_schedule_ids .= "schedule_ids[$rowcount] = '".$extract_r['event_schedule_id']."'; ";
					if($row_selected==$rowcount) $use_img = "tab_short_bg2_lit.png"; else $use_img = "tab_short_bg2.png";
					echo "<td width='51' align='center' valign='middle' style='background:URL(images/group_color_bg".$extract_r['race_group'].".png);' onClick='this.style.backgroundImage = \"URL(images/group_color_bg".$next_group.".png)\"; window.location = \"racers_for_this_race.php?mode=set_group&new_group=".$next_group."&customer_id=".$extract_r['customerid']."\";'>
										<table width='51' border='0' cellspacing='0' cellpadding='0'>
											<tr><td align='center' valign='middle'>&nbsp;</td></tr>
										</table>
									</td>
									<td id='kart_row_".$rowcount."' width='263' align='left' valign='middle' background='images/$use_img' onclick='if(row_selected) document.getElementById(\"kart_row_\" + row_selected).style.background = \"URL(images/tab_short_bg2.png)\"; row_selected = \"".$rowcount."\"; this.style.background = \"URL(images/tab_short_bg2_lit.png)\";'>
										<table width='263' border='0' cellspacing='0' cellpadding='8'>
											<tr><td class='".$nametext_class."'>";
											echo $extract_r['f_name']." ".$extract_r['l_name'];
											if($birth_date!="") 
											{
												if($cust_is_minor)
													echo " <font style='font-size:10px; color:#bb0000'>$birth_date</font>";
												else
													echo "";//" <font style='font-size:10px; color:#006600'>$birth_date</font>";
											}
											echo "</td></tr>
										</table>
									</td>
									<td width='52' align='left' valign='top' background='images/btn_credit.png' >
										<table width='50' height='46' border='0' cellspacing='0' cellpadding='6'>
											<tr><td align='center' valign='middle' class='creditnumber'>";
											
											$membership_expiration = $extract_r['membership_expiration'];
											$date_created = $extract_r['date_created'];
											if($date_created >= date("Y-m-d 00:00:00"))
												$cust_new = true;
											else
												$cust_new = false;
											if($membership_expiration!="" && $membership_expiration >= date("Y-m-d") && $membership_expration <= "9999-99-99")
												{$mem = true; $bgf = "images/btn_credit2.png";}
											else 	
												{$mem = false;$bgf = "images/btn_credit.png";}
											
											echo $extract_r['credits'];
											if($cust_new) echo "<font style='font-size:8px; color:#008800'>n</font>";
											if($mem) echo "<font style='font-size:8px; color:#000088'>M</font>";
											
											echo "</td></tr>
										</table>
									</td>
								</tr>
							</table>
						</td>
					</tr>";
				}
				for($r = $rowcount; $r < $minrows; $r++) {
					echo "<tr>
						<td height='46' align='center' valign='top'>
							<table width='417' height='46' border='0' cellpadding='0' cellspacing='0'>
								<tr>
									<td width='52' align='center' valign='middle' class='subtitles'>&nbsp;</td>
									<td width='51' align='center' valign='middle' background='images/tab_bg_51.png'>&nbsp;</td>
									<td width='262' align='left' valign='middle' background='images/tab_bg_262.png' >
										<table width='262' border='0' cellspacing='0' cellpadding='8'>
											<tr><td class='nametext'>&nbsp;</td></tr>
										</table>
									</td>
									<td width='52' align='left' valign='top' background='images/btn_credit.png' >
										<table width='50' height='46' border='0' cellspacing='0' cellpadding='6'>
											<tr><td align='center' valign='middle' class='creditnumber'>&nbsp;</td></tr>
										</table>
									</td>
								</tr>
							</table>
						</td>
					</tr>";
				}
			?>
    </table>
		<script language='javascript'>
			<?php
				echo "schedule_ids = new Array();\n" . $js_schedule_ids . "\n";
				echo "next_row_select = new Array();\n" . $js_next_row_select . "\n";
			?>
			row_selected = <?php echo $row_selected?>;
		</script>
	</body>
</html>