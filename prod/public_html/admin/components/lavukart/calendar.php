<?php
	require_once(dirname(__FILE__) . "/../comconnect.php");
	$year = reqvar("year",date("Y"));
	$month = reqvar("month",date("m"));
	$day = reqvar("day",date("d"));
	
	$highlight_date = reqvar("highlight_date",date("Y-m-d"));
	
	$pyear = $year;
	$pmonth = $month - 1;
	if($pmonth < 1) {$pmonth = 12;$pyear--;}
	$nyear = $year;
	$nmonth = $month + 1;
	if($nmonth > 12) {$nmonth = 1;$nyear++;}
	
	$tab_name = reqvar("tab_name");
	//echo "<br><br>" . $tab_name;
	if(isset($_GET['tab_name'])) $tab_name = $_GET['tab_name'];
	if ($tab_name == "Group Events") {
		$parent_component = "group_events";
		$parent_component_file = "lavukart/group_events.php";
	} else if ($tab_name == "new_pit") {
		$parent_component = "pit_schedule;pit_schedule_wrapper";
		$parent_component_file = "lavukart/pit_schedule.php;lavukart/pit_schedule_wrapper.php";
	} else if ($tab_name == "new_schedule") {
		$parent_component = "schedule;schedule_wrapper";
		$parent_component_file = "lavukart/schedule.php;lavukart/schedule_wrapper.php";
	} else {
		/*if($company_code=="pole_position_raceway" && date("Y-m-d") < "2011-01-14")
		{
			$parent_component = "events";
			$parent_component_file = "lavukart/events.php";
		}
		else
		{*/
			$parent_component = "events;events_wrapper";
			$parent_component_file = "lavukart/events.php;lavukart/events_wrapper.php";
		//}
		//mail("corey@meyerwind.com","cc: $company_code $parent_component","cc","From:info@poslavu.com");
	}
	
	function create_calendar($year,$month,$day,$highlight_date)
	{
		global $parent_component;
		global $parent_component_file;
	
		$js_cal_click = "cal_click";
		$calwidth = 51;
		$calheight = 48;
		$allow_past = true;
		
		$str .= "<script language='javascript'>";
		$str .= "function $js_cal_click(year, month, day) { ";
		$str .= "window.location = \"_DO:cmd=close_overlay&f_name=".$parent_component_file."&c_name=".$parent_component."?year=\" + year + \"&month=\" + month + \"&day=\" + day; ";
		$str .= "} ";
		$str .= "</script>";
		
		$str .= "<div id='calendar'>";
		$str .= "<table cellpadding=2><td class='caltext'>".date("M Y",mktime(0,0,0,$month,1,$year))."</td></table>";
		$str .= "<table cellspacing=0>";
		$r=1;$day=1;
		while($r<=6)
		{
			$str .= "<tr>";
			if($r==1)
			{
				$c=date("w",mktime(0, 0, 0, $month, 1, $year))+1;
				$x=1;
				while($x<$c)
				{
				  $str .= "<td width=$calwidth height=$calheight>&nbsp;</td>";
				  $x=$x+1;
				}
			}
			else $c=1;
			
			while($c<=7)
			{
				if($day<10) $day="0".$day;
				$datecheck = $year."-".$month."-".$day;
				
				if((($r+$c)%2)==0) 
					$sbg="#9fd4f0";
				else 
					$sbg="#5bb4e2";
					
				if(checkdate($month,$day,$year))
				{						
					$dayoftheweek = date("w",mktime(0,0,0,$month,$day,$year)) * 1 + 1;
	
					$msg = date("l",mktime(0,0,0,$month,$day,$year));
					$clr = "000000";
					
					if($allow_past || date("Y-m-d") <= date("Y-m-d",mktime(0,0,0,$month,$day,$year)))
						$allow = true;
					else
					{
						$allow = false;
						$clr = "666699";
					}
					
					$datenum = ($month * 100) + $day;
					$cal_js .= "cal_colors[$datenum] = '$sbg'; ";
					//if($month==$set_month && $year==$set_year && $day==$set_day)
					if($highlight_date==$year . "-" . $month . "-" . $day)//$month==date("m") && $year==date("Y") && $day==date("d"))
					{
						$extra_style_code = "background: URL(images/btn_cal_day.png); background-position:center center; background-repeat:no-repeat; ";
						//$extra_style_code = "border:solid 2px black; ";
						$sbg = "#00ff00";
						$cal_js .="selected_datenum = '$datenum'; ";
					}
					else $extra_style_code = "background: URL(images/btn_cal_day_lit.png); background-position:center center; background-repeat:no-repeat; ";

					if(date("Y-m-d")==$year . "-" . $month . "-" . $day)
						$extra_style_code .= "font-weight:bold; text-decoration:underline; ";
					$str .= "<td id='".$fieldname."_date_$datecheck' class='cal' title='$msg' alt='$msg'";
					$str .= " style='cursor:pointer; ";
					$str .= $extra_style_code;
					$str .= "'";
					if($allow)
						$str .= " onclick='$js_cal_click($year, $month, $day)'";
					$str .= " width=$calwidth height=$calheight align='center' valign='center'>";
					$str .= "<font color='#$clr'>";
					$str .= ($day*1);
					$str .= "</font>";
					$str .= "</td>";						
				}
				else $str .= "<td width=$calwidth height=$calheight>&nbsp;</td>";
	
				$c++;$day++;
			}
			$str .= "</tr>";
			$r++;
		}
		$str .= "</table>";
		$str .= "</div>";
		//$str .= "hd: " . $highlight_date;
		return $str;
	}	
?>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Untitled Document</title>
<style type="text/css">
<!--

body {
	-webkit-tap-highlight-color:rgba(0,0,0,0);
}

.caltext {
	font-family: "Trebuchet MS", Arial, Helvetica, sans-serifVerdana, Geneva, sans-serif;
	font-size: 16px;
	font-weight: bold;
	color: #154473;
}
-->
</style>
</head>

<body style='margin:0px'>
<table width="423" height="374" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="20" height="40">&nbsp;</td>
    <td width="120" align="center" valign="middle" onClick="window.location = '<?php echo "calendar.php?year=$pyear&month=$pmonth&day=$day&tab_name=$tab_name&highlight_date=$highlight_date"; ?>'">&nbsp;</td>
    <td width="134" onClick="window.location = '_DO:cmd=close_overlay'">&nbsp;</td>
    <td width="120" align="center" valign="middle" onClick="window.location = '<?php echo "calendar.php?year=$nyear&month=$nmonth&day=$day&tab_name=$tab_name&highlight_date=$highlight_date"; ?>'">&nbsp;</td>
    <td width="21">&nbsp;</td>
  </tr>
  <tr>
    <td colspan="5" align="center" valign="middle">
		<?php
        	echo create_calendar($year,$month,$day,$highlight_date);
		?>
    </td>
  </tr>
</table>
</body>

