// JavaScript Document

/*
*	This file is used bu parts_info.php and parts_addPart.php
*
*/


function validateAddForm(){
		
		/* if they don't enter anything the formating in ensureZeroInFrontOfDecimal() enters 0.00 and the forms submits successfully, this is bad cause they might think they entered a value.
				in short, if they did not enter a value for the CostPerPart field, do not allow the formatting to execute
		 */
		if (document.parts_addPartForm.CostPerPart.value != ""){
					ensureZeroInFrontOfDecimal();//format at very beginning
		}//if
		
		name = document.parts_addPartForm.name.value;
		description = document.parts_addPartForm.Description.value;
		quantity = document.parts_addPartForm.quantity.value;
		costPerPart = document.parts_addPartForm.CostPerPart.value;
		category = document.getElementById("Category").value;
		
		
		var canSubmitForm = true;
		
		if (name == ""){
					canSubmitForm = false;
		}//if
		
		if (category == "New Category"){
					
					var newCategory = document.getElementById("NewCategory").value;
					//alert("newCategory" + newCategory);
					//if (newCategory == ""){
					//			canSubmitForm = false;
					//}//if
					
		}//if
		
		if (description == ""){
					//canSubmitForm = false; //not required
		}//if
		
		if (quantity == ""){
					canSubmitForm = false;
		}//if
		
		if (costPerPart == ""){
					canSubmitForm = false;
		}//if
		
		if (canSubmitForm == false){
					window.location = '_DO:cmd=alert&title=Missing Values&message=Please fill in all fields.';
					return false;
		}//if
		
		
		
		if (ensureQuantityFormat(quantity) == false){
				window.location = '_DO:cmd=alert&title=Improper Format&message=Quantity must be entered as a whole number.';
				return false;
		}//if
		
		if (ensureCostPerPartFormat(costPerPart) == false){
				window.location = '_DO:cmd=alert&title=Improper Format&message=Please enter values for Cost Per Part in US Currency format (eg, 15.99). No dollar signs.';
				return false;			
		}//if
		
		return true;
		
}//validateAddForm()



function ensureCostPerPartFormat(number){
		/* Called on values entered into the text field for Cost Per Part, ensures proper format. Format is a currency format with two decimal places. Only numbers are allowed no dollar signs */
			
			index = 0;
			startCountingLengthAfterDecimal = false;
			lengthAfterDecimal = 0;
			/* Count how many numbers are after decimal place */
			for (i=0;i<number.length;i++){
						
						if (startCountingLengthAfterDecimal == true){
								lengthAfterDecimal = lengthAfterDecimal + 1;
						}//if
						
						if (number.charAt(i) == "."){
							  startCountingLengthAfterDecimal = true;
						}//if
			}//for
			
			/* If it's not a number return false */
			if (isNaN(number) == true){
				
					return false;
			}//if
			
			/* Only allow two zeros after decimal place */
			if (lengthAfterDecimal != 2){
					return false;
			}//if
			
			/* The number is in the proper currency format (eg 15.99) */
			return true;
			
}//ensureCostPerPartFormat()



function ensureQuantityFormat(number){
		/* Called on values entered into the text field for Quantity, ensures proper format. Format is a whole number. */
			
			/* Is whole number */
			if (number == Math.round(number)){
				 return true;
			}//if 
			
			/* Is not whole number */
			if (number != Math.round(number)){
				 return false;
			}//if 
			
}//ensureQuantityFormat()


function ensureZeroInFrontOfDecimal(){
		/* want two decimal places for any money entered into database */
		 	
				var val = document.parts_addPartForm.CostPerPart.value; //get the new value
				
				//force two decimals
				val = parseFloat(val);
				val = val.toFixed(2);
				document.parts_addPartForm.CostPerPart.value = val;
				
				
	}//ensureZeroInFrontOfDecimal()