<html>
	<head>
		<title>Pit Racers Wrapper</title>
		<style type="text/css">
<!--
body {
	background-color: transparent;
	margin-left: 0px;
	margin-top: 0px;
	margin-right: 0px;
	margin-bottom: 0px;
	-webkit-tap-highlight-color:rgba(0,0,0,0);
}

.carnumber {	font-family: Verdana, Geneva, sans-serif;
	font-size: 13px;
	font-weight: bold;
	color: #FFF;
}
.creditnumber {	font-family: Verdana, Geneva, sans-serif;
	font-size: 13px;
	font-weight: bold;
	color: #3d4f6d;
}
.nametext {	font-family: Verdana, Geneva, sans-serif;
	font-size: 13px;
	font-weight: bold;
}
.gridposition {	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 14px;
	font-weight: bold;
	color: #5A75A0;
}
.icontext {font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 11px;
	font-weight: bold;
	color: #5A75A0;
}
.subtitles {	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 14px;
	font-weight: bold;
	color: #5A75A0;
}
.title1 {	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 18px;
	font-weight: bold;
	color: #5A75A0;
}
-->
		</style>
	</head>

<?php
	ini_set("display_errors",1);
	require_once(dirname(__FILE__) . "/../comconnect.php");
	
	$eventid = 0;
	if(reqvar("eventid")) $eventid = reqvar("eventid");
	else if(sessvar_isset("eventid")) $eventid = sessvar("eventid");
	set_sessvar("eventid",$eventid);
	
	$print_event = reqvar("print");
	if($print_event)
	{
		if(strpos($company_code,"jersey")) $ploc = "p2r_new_jersey%20-%20NYJC";
		else if(strpos($company_code,"DEV")!==false) $ploc = "DEVKART%20-%20Dev%20East%20Side";
		else {
			if(reqvar("loc_id")==9)
				$ploc = "pole_position_raceway%20-%20Murrieta";
			else
				$ploc = "pole_position_raceway%20-%20Oklahoma";
		}
		
		$track_query = lavu_query("SELECT `lk_tracks`.`title` as `track_title` from `lk_events` LEFT JOIN `lk_tracks` ON `lk_events`.`trackid`=`lk_tracks`.`id` WHERE `lk_events`.`id`='[1]'",$eventid);
		if(mysqli_num_rows($track_query))
		{
			$track_read = mysqli_fetch_assoc($track_query);
			$track_title = $track_read['track_title'];
		}
		else $track_title = "";
		
		$print_to = "printer1";
		if(strpos($track_title,"2")!==false || strpos(strtolower($track_title),"second")!==false) $print_to = "printer2";
		else if(strpos($track_title,"3")!==false) $print_to = "printer3";
		else if(strpos($track_title,"4")!==false) $print_to = "printer4";
		
		$print_url_path = "http://admin.poslavu.com";
		if(isset($server_url_path)) $print_url_path = $server_url_path;
		
		//mail("corey@poslavu.com","print event","$company_code \n $ploc \nprint to: $print_to \ntrack: $track_title \npath: $print_url_path/print/index.php?location=$ploc&print_to=".$print_to."&print=".$print_event,"From: info@poslavu.com");
		//mail("corey@poslavu.com","print event","$company_code \n $ploc \nprint to: $print_to \ntrack: $track_title","From: info@poslavu.com");
			$ch = curl_init();
			curl_setopt($ch, CURLOPT_URL, $print_url_path . "/print/index.php?location=$ploc&print_to=".$print_to."&print=".$print_event);
			//curl_setopt($ch, CURLOPT_VERBOSE, 1);
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
			curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt($ch, CURLOPT_POST, 1);
			//curl_setopt($ch, CURLOPT_POSTFIELDS, $nvpreq);
			$httpResponse = curl_exec($ch);
			echo "<script language='javascript'>alert('Printing...');</script>";
	}

	$lane_number = 1;
	if(reqvar("lane_number")) $lane_number = reqvar("lane_number");
	else if(sessvar_isset("lane_number")) $lane_number = sessvar("lane_number");
	set_sessvar("lane_number",$lane_number);

	$race_title = "";
	$race_time = "";
	$trackid = "";
	$groupid = "";
	$typeid = "";
	$groupround = "";
	$use_slots = "";

	$get_race_info = lavu_query("SELECT `lk_tracks`.`min_lap_time` AS `min_lap_time`, `lk_event_sequences`.`slots` AS `sequence_slots`, `lk_event_types`.`slots` AS `event_slots`, `lk_event_types`.`title` as `title`, `lk_event_types`.`id` as `typeid`, `lk_event_types`.`score_by` as `score_by`, `lk_event_types`.`advance_by_position` as `advance_by_position`, `lk_events`.`heat` AS `heat`, `lk_events`.`groupid` as `groupid`, `lk_events`.`time` as `time`, `lk_events`.`trackid` as `trackid` FROM `lk_events` LEFT JOIN `lk_event_types` ON `lk_events`.`type` = `lk_event_types`.`id` LEFT JOIN `lk_event_sequences` ON `lk_event_sequences`.`id` = `lk_events`.`sequenceid` LEFT JOIN `lk_tracks` ON `lk_tracks`.`id` = `lk_events`.`trackid` WHERE `lk_events`.`id` = '".$eventid."'");
	if (@mysqli_num_rows($get_race_info) > 0) {
		$extract = mysqli_fetch_array($get_race_info);
		$race_title = $extract['title'];
		$race_time = $extract['time'];
		$min_lap_time = $extract['min_lap_time'];
		$trackid = $extract['trackid'];
		$groupid = $extract['groupid'];
		$typeid = $extract['typeid'];
		$groupround = $extract['heat'];
		$advance_by_position = $extract['advance_by_position'];
		$score_by = $extract['score_by'];
		$use_slots = (isset($extract['sequence_slots'])?$extract['sequence_slots']:$extract['event_slots']);
	}
	
	$advance_racers = reqvar("advance_racers");
	$advance_groupid = reqvar("advance_groupid");
	$next_round = reqvar("next_round");
	if($advance_racers)
	{
		$from_events = array();
		$to_events = array();
		$race_group_query = lavu_query("select * from `lk_events` where `groupid`='[1]' order by `time` DESC",$groupid);
		while($race_group_read = mysqli_fetch_assoc($race_group_query))
		{
			if($race_group_read['heat']==$groupround)
				$from_events[] = $race_group_read['id'];
			else if($race_group_read['heat']==$next_round)
				$to_events[] = $race_group_read['id'];
		}
		
		$all_racers = array();
		$total_racers = 0;
		$total_events = count($from_events);
		for($n=0; $n<count($from_events); $n++)
		{
			$from_eventid = $from_events[$n];
			
			if($advance_by_position=="Yes")
			{
				$sched_query = lavu_query("select * from `lk_event_schedules` where `eventid`='[1]' AND `advanceable` != 'no' order by id asc",$from_eventid);
				while($sched_read = mysqli_fetch_assoc($sched_query))
				{
					$sched_customerid = $sched_read['customerid'];
					$sched_eventid = $sched_read['eventid'];
					$sched_group = $sched_read['group'];
					
					$race_results_query = lavu_query("select * from `lk_race_results` where `eventid`='[1]' and `customerid`='[2]' AND `bestlap` >= '[3]'",$sched_eventid,$sched_customerid, $min_lap_time);
					if(mysqli_num_rows($race_results_query))
					{
						$race_results_read = mysqli_fetch_assoc($race_results_query);
						$all_racers[] = array($eventid,$sched_customerid,$race_results_read['place'],$race_results_read['bestlap'],$sched_group);
						$total_racers++;
					}
				}
			}
			else if(isset($to_events[$n]))
			{
				$to_eventid = $to_events[$n];

				//lavu_query("delete from `lk_event_schedules` where `eventid`='[1]'",$to_eventid);
				$sched_query = lavu_query("SELECT `lk_event_schedules`.`customerid`, `lk_event_schedules`.`group` FROM `lk_event_schedules` LEFT JOIN `lk_race_results` ON `lk_race_results`.`eventid` = `lk_event_schedules`.`eventid` AND `lk_race_results`.`customerid` = `lk_event_schedules`.`customerid` WHERE `lk_event_schedules`.`eventid` = '[1]' AND `lk_event_schedules`.`advanceable` != 'no' AND `lk_race_results`.`bestlap` >= '[2]' ORDER BY `lk_event_schedules`.`id` ASC", $from_eventid, $min_lap_time);
				while($sched_read = mysqli_fetch_assoc($sched_query))
				{
					$customerid = $sched_read['customerid'];
					$schedgroup = $sched_read['group'];
					$exist_query = lavu_query("select * from `lk_event_schedules` where `eventid`='[1]' and `customerid`='[2]'",$to_eventid,$customerid);
					if(!mysqli_num_rows($exist_query))
					{
						lavu_query("insert into `lk_event_schedules` (`eventid`,`customerid`,`group`) values ('[1]','[2]','[3]')",$to_eventid,$customerid,$schedgroup);
					}
				}
			}
		}
		
		if($advance_by_position=="Yes")
		{
			//if ($score_by==0 || $score_by=="")
				$order_determinate = 3;
			//else
				//$order_determinate = 2;
		
			$ordered_racers = array();
			for($n=0; $n<count($all_racers); $n++)
			{
				$current_racer = $all_racers[$n];
								
				$new_order = array();
				$placed = false;
				for($k=0; $k<count($ordered_racers); $k++)
				{
					if($current_racer[$order_determinate] * 1 < $ordered_racers[$k][$order_determinate] * 1 && !$placed)
					{
						$placed = true;
						$new_order[] = $current_racer;
					}
					$new_order[] = $ordered_racers[$k];
				}
				if($placed==false) $new_order[] = $current_racer;
				
				$ordered_racers = $new_order;
			}
			
			$event_index = 0;
			$racers_placed_for_event = 0;
			$racers_placed = 0;
			$racers_per_event = ceil((float)$total_racers / count($to_events));
			
			for($n=0; $n<count($ordered_racers); $n++)
			{
				if(isset($to_events[$event_index])) {
					$use_eventid = $from_events[$event_index];			
					$to_eventid = $to_events[$event_index];
					$customerid = $ordered_racers[$n][1];
					$schedgroup = $ordered_racers[$n][4];
					
					$exist_query = lavu_query("select * from `lk_event_schedules` where `eventid`='[1]' and `customerid`='[2]'",$to_eventid,$customerid);
					if(mysqli_num_rows($exist_query))
					{
						$exist_read = mysqli_fetch_assoc($exist_query);
						lavu_query("delete from `lk_event_schedules` where `id`='[1]'",$exist_read['id']);
					}
					lavu_query("insert into `lk_event_schedules` (`eventid`,`customerid`,`group`) values ('[1]','[2]','[3]')",$to_eventid,$customerid,$schedgroup);
					//echo "<br>" . $use_eventid . " to " . $to_eventid . " - " . $customerid . " - " . $ordered_racers[$n][3] . " - " . $schedgroup;
					
					$racers_placed++;
					$racers_placed_for_event++;
					if (($racers_placed_for_event >= $racers_per_event) || ($racers_placed_for_event >= $use_slots))
					{
						$event_index++;
						$racers_placed_for_event = 0;
					}
				}
			}
		}
		
		echo "<script language='javascript'>";
		echo "window.location = '_DO:cmd=load_url&c_name=pit_schedule&f_name=lavukart/pit_schedule.php'; ";
		echo "</script>";
		//echo "<br>From: ";
		//echo "advance racers: $advance_racers - $advance_groupid - $next_race_type<br>";
	}
	
	$groupround_count = "";
	$groupround_done = "";
	$groupround_index = "";
	$group_next_round = "";

	if($groupid!="")
	{
		$groupround_count = 0;
		$groupround_done = 0;
		$groupround_index = 0;
		$race_group_query = lavu_query("select * from `lk_events` where `groupid`='[1]' order by `time` asc",$groupid);
		while($race_group_read = mysqli_fetch_assoc($race_group_query))
		{
			if($race_group_read['heat']==$groupround)
			{
				$groupround_count++;
				if ($race_group_read['ms_end'] != "") {
					$groupround_done++;
				}
			}
			if($race_group_read['id']==$eventid)
			{
				$groupround_index = $groupround_count;
			}
			if($race_group_read['heat']!=$groupround && $groupround_count > 0 && $group_next_round=="")
			{
				$group_next_round = $race_group_read['heat'];
			}
		}
	}
	
	$startrace = reqvar("startrace");
	if($startrace && $startrace==$eventid)
	{
		lavu_query("update `lk_events` set `ms_start`='".date("Y-m-d H:i:s")."' where `id`='[1]'",$startrace);
	}
	$endrace = reqvar("endrace");
	if($endrace)
	{
		lavu_query("update `lk_events` set `ms_end`='".date("Y-m-d H:i:s")."' where `id`='[1]'",$endrace);
	}
	$continue_race = reqvar("continue_race");
	if($continue_race)
	{
		function time_to_ts($time)
		{
			$parts = explode(" ",$time);
			$date_parts = explode("-",$parts[0]);
			$time_parts = explode(":",$parts[1]);
			$ts = mktime($time_parts[0],$time_parts[1],$time_parts[2],$date_parts[1],$date_parts[2],$date_parts[0]);
			return $ts;
		}
		
		$e_query = lavu_query("SELECT * from `lk_events` where `id`='[1]'",$continue_race);
		if(mysqli_num_rows($e_query))
		{
			$e_read = mysqli_fetch_assoc($e_query);
			
			$ts_now = time();
			$ts_end = time_to_ts($e_read['ms_end']);
			$ts_span = $ts_now - $ts_end;
			$pause_data = $ts_now . "-" . $ts_end . "=" . $ts_span;
			lavu_query("update `lk_events` set `ms_end`='', `pauses`=CONCAT(`pauses`,'$pause_data ') where `id`='[1]'",$continue_race);
		}
	}
	
	$race_active = false;
	$active_query = lavu_query("select * from `lk_events` where `ms_start`!='' and `ms_end`='' and `trackid`='[1]'",$trackid);
	if(mysqli_num_rows($active_query))
	{
		$active_read = mysqli_fetch_assoc($active_query);
		$active_raceid = $active_read['id'];
		$race_active = true;
	}
	
	$event_exists = false;
	$event_active = false;
	$event_query = lavu_query("select * from `lk_events` where `id`='[1]'",$eventid);
	if(mysqli_num_rows($event_query))
	{
		$event_exists = true;
		$event_active = true;
		$event_read = mysqli_fetch_assoc($event_query);
		if($event_read['ms_end']!="")
			$event_active = false;
	}
?>

	<body>
		<table width="474" height="598" border="0" cellpadding="0" cellspacing="0" >
			<tr>
				<td width="79" height="486" align="left" valign="top">
					<table width="79" border="0" cellpadding="0" cellspacing="0">
						<tr>
							<td width="11">&nbsp;</td>
							<td height="26">&nbsp;</td>
						</tr>
						<tr>
							<td width="11">&nbsp;</td>
							<td>&nbsp;</td>
						</tr>
						<tr>
							<td width="11">&nbsp;</td>
							<td height="110" align="left" valign="bottom"></td>
						</tr>
					</table>
				</td>
				<td align="left" valign="top">
					<table width="395" height="508" border="0" cellspacing="0" cellpadding="0">
						<tr><td height="23" background="images/line_top.png"></td></tr>
						<tr><td height="482" align="left" valign="top">&nbsp;</td></tr>
						<tr><td height="8" background="images/line_bot.png"></td></tr>
					</table>
				</td>
			</tr>
            <tr>
            	<td height="50" colspan='2' align='right' valign="top">

					
                    <table>
                    	<tr>
							<td valign="top" class="title1" align='right'>
							<?php 
								if($eventid > 0) 
								{
									echo $race_title;
									if($groupround_count!="" && $groupround_count > 1)
										echo " <font style='font-size:10px'>(" . $groupround_index . "/" . $groupround_count . ")</font>";
									echo "<br>";
									echo display_time($race_time);
								}
							?></td>
                            <td width='11'>&nbsp;</td>
						<?php if($event_active) {
									if($race_active) { 
						?>
							<td width="11">&nbsp;</td>
							<td align="left">
								<table width="68" border="0" cellpadding="0" cellspacing="0">
									<tr><td height="32" align="center" valign="top" class="subtitles">
                                    <a onclick='location = "_DO:cmd=load_url&c_name=pit_racers_wrapper&f_name=lavukart/pit_racers_wrapper.php?endrace=<?php echo $active_raceid?>";'><img src="images/btn_endrace.png" width="68" height="68" /></a></td></tr>
									<!--$ <tr><td height="32" align="center" valign="middle" class="subtitles"><a onclick='location = "_DO:cmd=create_overlay&f_name=lavukart/pit_grid_overlay_wrapper.php;lavukart/pit_grid.php&c_name=pit_grid_overlay_wrapper;pit_grid&dims=275,90,474,598;302,130,420,540&scroll=0;1?test1=1&test2=2";'><img src="images/btn_grid.png" width="68" height="68" /></a></td></tr> $-->
									<!--<tr><td height="32" align="center" valign="middle"><span class="icontext">STOP</span></span></td></tr>-->
								</table>
							</td>
						<?php 		} else { ?>
							<td width="11">&nbsp;</td>
							<td align="left">
								<table width="68" border="0" cellpadding="0" cellspacing="0">
									<tr><td height="32" align="center" valign="top" class="subtitles"><a onclick='location = "_DO:cmd=load_url&c_name=pit_racers_wrapper&f_name=lavukart/pit_racers_wrapper.php?startrace=<?php echo $eventid?>";'><img src="images/btn_startrace.png" width="68" height="68" /></a></td></tr>
									<!--$ <tr><td height="32" align="center" valign="middle" class="subtitles"><a onclick='location = "_DO:cmd=create_overlay&f_name=lavukart/pit_grid_overlay_wrapper.php;lavukart/pit_grid.php&c_name=pit_grid_overlay_wrapper;pit_grid&dims=275,90,474,598;302,130,420,540&scroll=0;1?test1=1&test2=2";'><img src="images/btn_grid.png" width="68" height="68" /></a></td></tr> $-->
									<!--<tr><td height="32" align="center" valign="middle"><span class="icontext">START</span></span></td></tr>-->
								</table>
							</td>
						<?php } } else if(!$race_active && $event_exists) { ?>
							<td width="11">&nbsp;</td>
							<td align="left">
								<table width="68" border="0" cellpadding="0" cellspacing="0">
									<tr><td height="32" align="center" valign="top" class="subtitles">
										<a onclick='location = "_DO:cmd=load_url&c_name=pit_racers_wrapper&f_name=lavukart/pit_racers_wrapper.php?continue_race=<?php echo $eventid?>";'><img src="images/btn_continue_race.png" width="68" height="68" /></a>
								</td></tr>
									<!--<tr><td height="32" align="center" valign="middle"><span class="icontext">CONTINUE</span></span></td></tr>-->
								</table>
							</td>
                            <?php if($group_next_round!="" && $groupround_done==$groupround_count) { ?>
                            <td width="11">&nbsp;</td>
                            <td align="left">
                            	<table width="68" border="0" cellpaddin="0" cellspacing="0">
									<tr><td height="32" align="center" valign="middle" class="subtitles"><a onclick='location = "_DO:cmd=load_url&c_name=pit_racers_wrapper&f_name=lavukart/pit_racers_wrapper.php?advance_racers=<?php echo $eventid?>&advance_groupid=<?php echo $groupid?>&next_round=<?php echo $group_next_round; ?>"'><img src="images/btn_grid.png" width="68" height="68" /></a></td></tr>
                                </table>
                            </td>
                            <?php } ?>
						<?php } ?>
						<?php if($event_exists) { ?>
							<td width="11">&nbsp;</td>
							<td align="left">
								<table width="68" border="0" cellpadding="0" cellspacing="0">
									<tr><td height="32" align="center" valign="top" class="subtitles"><a onclick='location = "_DO:cmd=load_url&c_name=pit_racers_wrapper&f_name=lavukart/pit_racers_wrapper.php?print=<?php echo $eventid?>";'><img src="images/btn_printer.png" width="68" height="68" /></a></td></tr>
									<!--$ <tr><td height="32" align="center" valign="middle" class="subtitles"><a onclick='location = "_DO:cmd=create_overlay&f_name=lavukart/pit_grid_overlay_wrapper.php;lavukart/pit_grid.php&c_name=pit_grid_overlay_wrapper;pit_grid&dims=275,90,474,598;302,130,420,540&scroll=0;1?test1=1&test2=2";'><img src="images/btn_grid.png" width="68" height="68" /></a></td></tr> $-->
									<!--<tr><td height="32" align="center" valign="middle"><span class="icontext">PRINT RESULTS</span></span></td></tr>-->
								</table>
							</td>
						<?php } ?>
                       	<td width='7'>&nbsp;</td>
					</table>



                </td>
            </tr>
		</table>
	</body>
</html>