<?php
			require_once(dirname(__FILE__) . "/../comconnect.php");
			exit();
?>

<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<title>Garage List</title>

		<style type="text/css">
<!--
body {
	background-color: transparent;
	margin-left: 0px;
	margin-top: 0px;
	margin-right: 0px;
	margin-bottom: 0px;
	-webkit-tap-highlight-color:rgba(0,0,0,0);
	font-family:Verdana, Geneva, sans-serif;
}

.viewByPanel{
		 width:200px; 
		 padding-left:13px;
		 height:auto;
		 border-left:0px dotted #999999;
		 
}


.leftViewBy{
	float:left;
	text-align:right;
	width:105px;
}

.rightViewBy{
	float:left;
	text-align:left;
	padding-left:8px;
	width:50px;
}

.breakLine{
	clear:both;
	height:10px;
}

label{
	color: #5A75A0;
	font-size:10pt;
	font-family:Verdana, Geneva, sans-serif;
	font-weight:700;
}

h1{
	color:#5A75A0;
	font-size:11pt;
	font-weight:900;
}

fieldset{
	color:#5A75A0;
	font-size:11pt;
	font-weight:900;
}

.submitButton{
	background-image:url(images/btn_submit_100x33.png);
	background-repeat: no-repeat;
	width: 100px;
	height: 33px;
	border:none;
}


.myRadio{
	
	
	background-repeat:no-repeat;
	background-color:transparent;
	
	border:none;
	width:32px;
	height:32px;
}


fieldset{
	border:1px solid #999;
}

select{
	background-color:#0033FF;
}

</style>


<script type="text/javascript">

function processSelectedOption(input){
		/* This function is called to turn off or on all elements with a specified classname. */
		
		//this is class1
		if (input == "ByAllKarts"){
					
					//turn off (or hide) class2 elements
					turnOffTheseClassElements("class2");
					
					/* Set list menu under "Options" fieldset to the default value */
					document.getElementById("KartID").value = 0;
					
					//turn on (or display) class1 elements
					turnOnTheseClassElements("class1");
		
		}//if
		
		
		//if incomplete or complete radio buttons are checked under "View" */
		if ( (input == "complete") || (input == "incomplete") ){
					
					//turn off (or hide) class2 elements
					turnOffTheseClassElements("classServicedBy");
					
					/* Set list menu under "Options" fieldset to the default value */
					document.getElementById("servicedBy").value = 0;
					
					//turn on (or display) class1 elements
					//turnOnTheseClassElements("class1");
		
		}//if
		
		
		//this is classServicedBy
		if (input == "servicedBy"){
					
					//turn on (or display) classServicedBy elements
					turnOnTheseClassElements("classServicedBy");
		
		}//if
		
		
		//this is class2
		if (input == "ByKartNumber"){
					
					//turn off (or hide) class1 elements
					turnOffTheseClassElements("class1");
					
					//turn on (or display) class2 elements
					turnOnTheseClassElements("class2");
					
		}//if
	
		/* NOTE: If a checkbox is used by both options, no class is assigned to it since it must always be on */
		
}//processSelectedOption()


function turnOffTheseClassElements(turnTheseOff){
		/* This function hides all the elements that belong to the inputted class name */	
			
					var numberOfClassElements = document.getElementsByClassName(turnTheseOff).length;
					
					
					for (i=0; i<numberOfClassElements; i++){
					
								document.getElementsByClassName(turnTheseOff).item(i).style.display = "none";
					
					}//for
		
}//turnOffTheseClassElements()


function turnOnTheseClassElements(turnTheseOn){
		/* This function displays all the elements that belong to the inputted class name */	
			
					var numberOfClassElements = document.getElementsByClassName(turnTheseOn).length;
					
					
					for (i=0; i<numberOfClassElements; i++){
					
								document.getElementsByClassName(turnTheseOn).item(i).style.display = "inline";
					
					}//for
		
}//turnOffTheseClassElements()


function validateForm(){
	/* This function validates the form. It ensures the user does not submit incorrect combinations of values */
	
	
	
	return true;
	
}//validateForm()


function passDataToServiceList(){
			/* For the componenent on the right of service_info, they can select viewing options and how to organize the data, This script is called by the form viewByForm.*/
			
			if (validateForm() != true){
						
						/* If the form is not valid, exit this passDataToService() function because we don't want the form to submit */
						return false;
			}//if
			
			
			var view = "";
			var option = "";
			var selectedKartID = "";
			var by = "";
			var servicedBy = "";
			
			view = getCheckedRadioValue("View"); //possible values are complete or incomplete. in "View" fieldset, radio button group name is "View"
			
			option = getCheckedRadioValue("option"); //possible values are ByAllKarts or ByKartNumber. in "Option" fieldset
					
					if (option == "ByKartNumber"){
								//the user checked "Single Kart" under "Options" and now we get the kart id of the kart (number) they selected
								selectedKartID = document.getElementById("KartID").value;
					}//if
			
			by = getCheckedRadioValue("by"); //possible values are date_due, datetime, kart_number. in "By" fieldset
			
			servicedBy = document.getElementById("servicedBy").value;
			
			
			
			
			window.location = "_DO:cmd=load_url&c_name=service_list&f_name=lavukart/service_list.php?customViewSelection=true&view=" + view + "&option=" + option + "&selectedKartID=" + selectedKartID + "&by=" + by + "&servicedBy=" + servicedBy;
			
			/* The form does not need to submit since the javascript function was called */
			return false;
}//passDataToServiceList()




function getCheckedRadioValue(input){
			/* This function return the value of the selected radio button, given the name of the radio buttons. */
			for (i=0; i<=input.length; i++){
				var str = "document.viewByForm." + input + "[" + i + "].checked";
			
						var curButton = eval(str);
						
						if (curButton == true){
									var str2 = "document.viewByForm." + input + "[" + i + "].value";
									var curButtonVal = eval(str2);
									//alert("curButtonVal: " + curButtonVal);
									return curButtonVal;
						}//if
			}//for
			
			/*if it gets this far, nothing was checked, a script can check for this value during form validation*/
			UhOh = "Nothing Was Selected";
			return UhOh;
			
}//getCheckedRadioValue()


function preventFormFromSubmitting(){
	/* We don't want the form submitting because then the user looses all of their selections when the form refreshes */
			return false;
}//preventFormFromSubmitting()

</script>


<body>


<div class="viewByPanel">
			
				<form action="" onSubmit="return preventFormFromSubmitting();" id="viewByForm"  name="viewByForm" method="post">
			
			
				<fieldset>	
					<legend>View</legend>	
					
							<div class="leftViewBy">
										<label>Incomplete</label>
							</div>
							
							<div class="rightViewBy">
										<input class="myRadio" name="View" value="incomplete" onChange="processSelectedOption('incomplete');" type="radio">
							</div>
							
							<div class="breakLine"></div>
							
							
							<div class="leftViewBy">
										<label>Complete</label>
							</div>
							
							<div class="rightViewBy">
										<input class="myRadio" name="View" value="complete" onChange="processSelectedOption('complete');" type="radio">
							</div>
							
							<div class="breakLine"></div>
							
							
							<div class="leftViewBy">
										<label>Serviced by</label>
							</div>
			
							<div class="rightViewBy">
										<input class="myRadio" name="View" onChange="processSelectedOption('servicedBy');" value="servicedBy" type="radio">
							</div>
							<div class="breakLine"></div>
							
							
							<?php
							
echo "
				
				
				<span class=\"classServicedBy\" style=\"display:none;\">
				
				<div class=\"leftViewBy\">
							<label>Employee</label>
				</div>
				<div class=\"rightViewBy\">
							<select name=\"servicedBy\" id=\"servicedBy\" style=\"width:50px;\" >
										<option value=\"0\"></option>
";
						
	$locationid = sessvar("loc_id");
	$listOfEmployees = lavu_query("SELECT * FROM users WHERE loc_id=\"[1]\"", $locationid);
				
					
					

					while ($rowListOfEmployees = mysqli_fetch_assoc($listOfEmployees)){
								
								$employeeID = $rowListOfEmployees['id'];
								$employeeFirstName = $rowListOfEmployees['f_name'];
								$employeeLastName = $rowListOfEmployees['l_name'];
								
							
								
								if ($submittedBy == $employeeID){
											echo "<option value=\"$employeeID\" selected=\"selected\">$employeeFirstName $employeeLastName</option>";
								}//if
								
								if ($submittedBy != $employeeID){
											echo "<option value=\"$employeeID\">$employeeFirstName $employeeLastName</option>";
								}//if
								
					}//while
					
				
	echo "</select>
				</div><!--rightViewBy-->";
				
	echo "
			<div class=\"breakLine\"></div>
			</span><!--classServicedBy-->
	"; 
							
?>


							
							
					
				</fieldset>
				
				
				<fieldset>
					<legend>Option</legend>
					
					
							<div class="leftViewBy">
										<label>All Karts</label>
							</div>
							
							<div class="rightViewBy">
										<input class="myRadio" id="option" onChange="processSelectedOption('ByAllKarts');" name="option" value="ByAllKarts" type="radio">
							</div>
							
							<div class="breakLine"></div>
					
							<div class="leftViewBy">
										<label>Single Kart</label>
							</div>
							
							<div class="rightViewBy">
										<?php
													
													$locationid = sessvar("loc_id");
													
													echo "
																		<input class=\"myRadio\" id=\"option\" name=\"option\" value=\"ByKartNumber\" type=\"radio\" onchange=\"processSelectedOption('ByKartNumber');\"></div>
																		<div class=\"breakLine\"></div>";
																		
																		echo "
																		<span class=\"class2\" style=\"display:none;\">
																		<div class=\"leftViewBy\"><label>Kart &#35;</label></div>
																		<div class=\"rightViewBy\">
																		<select style=\"width:50px;\"  class=\"class2\" id=\"KartID\" name=\"KartID\"><option value=\"0\"></option>";
						
					
													$kartResults = lavu_query("SELECT * FROM lk_karts WHERE locationid=\"[1]\" ORDER BY number", $locationid);
				
													while ($rowKarts = mysqli_fetch_assoc($kartResults)){
								
																$curKartID = $rowKarts['id'];
																$curKartNumber = $rowKarts['number'];
								
																echo "<option value=\"$curKartID\">$curKartNumber</option>";
												
													}//while
					
				
													echo "</select></div>";
										
										
										?>
							
							
							<div class="breakLine"></div>
							</span>
					
				</fieldset>	
				
				
				
				
				
			
				
				<fieldset>
							
							<legend>By</legend>
					
							<span class="class1">
										<div class="leftViewBy">
													<label>Kart &#35;</label>
										</div>
							
										<div class="rightViewBy">
													<input class="myRadio class1"  name="by" value="kart_number" type="radio">
										</div>
							
										<div class="breakLine"></div>
							
							</span>
							
							
							
							<div class="leftViewBy">
										<label>Date assigned</label>
							</div>
							
							<div class="rightViewBy">
										<input class="myRadio"  name="by" value="datetime" type="radio">
							</div>
							
							<div class="breakLine"></div>
							
							
							<div class="leftViewBy">
										<label>Date Due</label>
							</div>
							
							<div class="rightViewBy">
										<input class="myRadio"  name="by" value="date_due" type="radio">
							</div>
							
							<div class="breakLine"></div>
							
							
							
							
							

			
			</fieldset>
			
			
			
	
					
						
			<br/>
			<input name="hi" type="submit" onClick="passDataToServiceList();" class="submitButton" value="">				
						
			</form>
			
	</div><!--viewByPanel-->

</body>
</html>