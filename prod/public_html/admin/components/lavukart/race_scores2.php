<?php require_once(dirname(__FILE__) . "/../comconnect.php"); ?>


<?php
  	//require_once("../comconnect.php");
			
	$eventid = reqvar("eventid");
	$trackid = reqvar("trackid");
	$mode = reqvar("mode");
	
	if($eventid=="current" || $eventid=="active")
	{
		$active_query = lavu_query("select * from `lk_events` where `ms_start`!='' and `ms_end`='' and `trackid`='[1]'",$trackid);
		if(lysql_num_rows($active_query))
		{
			$active_read = lysql_fetch_assoc($active_query);
			$eventid = $active_read['id'];
		}
	}
	
	if($mode=="kart_assignment")
	{
		$kart_count = 0;
		$sched_query = lavu_query("select `f_name`,`l_name`,`racer_name`,`lk_event_schedules`.`id` as `id`,`lk_karts`.`number` as `number` from `lk_event_schedules` LEFT JOIN `lk_customers` ON `lk_event_schedules`.`customerid`=`lk_customers`.`id`  LEFT JOIN `lk_karts` ON `lk_event_schedules`.`kartid`=`lk_karts`.`id` where `lk_event_schedules`.`eventid`='[1]'",$eventid);
		while($sched_read = lysql_fetch_assoc($sched_query))
		{
			$racer_name = $sched_read['racer_name'];
			if(trim($racer_name)=="")
				$racer_name = trim($sched_read['f_name'] . " " . $sched_read['l_name']);
			$racer_number = $sched_read['number'];
			$kart_count++;
			echo "<br>&kart_name".$kart_count."=".$racer_name."&";
			echo "<br>&kart_number".$kart_count."=".$racer_number."&";
		}
		echo "<br>&kart_count=".$kart_count."&";
	}
	else if($mode=="upcoming")
	{
		$year = date("Y");
		$month = date("m");
		$day = date("d");
		$hour = date("H");
		$minute = date("m");
		$mints = mktime($hour - 2,$minute,0,$month,$day,$year);
		$maxts = mktime($hour + 8,$minute,0,$month,$day,$year);
		
		$event_count = 0;
		$event_query = lavu_query("select `lk_events`.`ts` as `ts`, `lk_events`.`time` as time, `lk_events`.`id` as id, `lk_event_types`.`title` as `title` from `lk_events` left join `lk_event_types` on lk_events.type = lk_event_types.id where `ts`>='$mints' and `ts`<='$maxts' and `lk_events`.`ms_start`='' and `lk_events`.`trackid`='[1]' order by `ts` asc limit 6",$trackid);
		while($event_read = lysql_fetch_assoc($event_query))
		{
			$eventid = $event_read['id'];
			$event_count++;
			echo "<br>&event".$event_count."_name=" . date("g:i a",$event_read['ts'])."&";
			
			$racer_count = 0;
			$sched_query = lavu_query("select * from `lk_event_schedules` left join `lk_customers` on `lk_event_schedules`.`customerid`=`lk_customers`.`id` where `eventid`='$eventid'");
			while($sched_read = lysql_fetch_assoc($sched_query))
			{
				$racer_count++;
				$racer_name = $sched_read['racer_name'];
				if(trim($racer_name)=="")
					$racer_name = trim($sched_read['f_name'] . " " . $sched_read['l_name']);
				echo "<br>&event".$event_count."_racer".$racer_count."=" . $racer_name . "&";
			}
			echo "<br>&event".$event_count."_racer_count=" . $racer_count . "&";
		}
		echo "<br>&event_count=" . $event_count . "&";
	}
	else
	{
		if($eventid)
		{
			$display = "";
			$data = "";
			
			$event_title = "";
			$total_laps = 0;
			$event_query = lavu_query("select * from `lk_events` where `id`='[1]'",$eventid);
			if(lysql_num_rows($event_query))
			{
				$event_read = lysql_fetch_assoc($event_query);
				$event_title = display_time($event_read['time']);
				$event_type = $event_read['type'];
				
				$type_query = lavu_query("select * from `lk_event_types` where `id`='[1]'",$event_type);
				if(lysql_num_rows($type_query))
				{
					$type_read = lysql_fetch_assoc($type_query);
					$total_laps = $type_read['laps'];
				}
			}
			$data .= "&event_title=$event_title&";
			$data .= "&total_laps=$total_laps&";
			
			$track_first_pass = "";
			$sched_query = lavu_query("select `f_name`,`l_name`,`racer_name`,`lk_event_schedules`.`id` as `id`,`lk_karts`.`number` as `number` from `lk_event_schedules` LEFT JOIN `lk_customers` ON `lk_event_schedules`.`customerid`=`lk_customers`.`id`  LEFT JOIN `lk_karts` ON `lk_event_schedules`.`kartid`=`lk_karts`.`id` where `lk_karts`.`number`!='' and `lk_event_schedules`.`eventid`='[1]'",$eventid);
			if(lysql_num_rows($sched_query))
			{
				$racer_info = array();
				$lap_first_pass = array();
				$highest_lap = 0;
				
				while($sched_read = lysql_fetch_assoc($sched_query))
				{
					$schedid = $sched_read['id'];
					
					$racer_name = $sched_read['racer_name'];
					if(trim($racer_name)=="")
						$racer_name = trim($sched_read['f_name'] . " " . $sched_read['l_name']);
						
					$racer_info[$schedid]['name'] = $racer_name;
					$racer_info[$schedid]['number'] = $sched_read['number'];
					
					$r_lap = 0;
					$r_bestlap = 0;
					$r_lastlap = 0;
					$r_gap = 0;
					$r_passed = 0; 
					$r_first_pass = "";
					$lap_query = lavu_query("select * from `lk_laps` where `scheduleid`='[1]' order by `ms_this_lap` asc",$sched_read['id']);
					while($lap_read = lysql_fetch_assoc($lap_query))
					{
						if(is_numeric($lap_read['pass']) && $lap_read['pass'] < 999)
						{
							/*while($lap < $lap_read['pass'])
							{
								echo "<td>-</td>";
								$lap++;
							}
							echo "<td>".$lap_read['ms_duration']."</td>";*/
							
							if($r_lap > 0)
							{
								$r_passed = $lap_read['ms_this_lap'];
								$r_lastlap = $lap_read['ms_duration'];
								if($r_bestlap==0 || $r_bestlap > $r_lastlap) $r_bestlap = $r_lastlap;
							}
							else
							{
								$racer_info[$schedid]['first_pass'] = $r_first_pass;
							}
							$r_lap++;
							if(!isset($lap_first_pass[$r_lap]) || $lap_first_pass[$r_lap] > $r_passed) $lap_first_pass[$r_lap] = $r_passed;
							if($r_first_pass=="") $r_first_pass = $r_passed;
						}
					}
					
					$racer_info[$schedid]['lap'] = $r_lap;
					$racer_info[$schedid]['bestlap'] = number_format($r_bestlap,3,".",",");
					$racer_info[$schedid]['lastlap'] = number_format($r_lastlap,3,".",",");
					$racer_info[$schedid]['passed'] = $r_passed;
					
					if($track_first_pass=="" || $track_first_pass > $r_passed) $track_first_pass = $r_passed;
					if($r_lap > $highest_lap) $highest_lap = $r_lap;
				}
				
				$display .= "<style>";
				$display .= "  body, table {font-size:14px;} ";
				$display .= "</style>";
				
				for($n=0; $n<=$highest_lap; $n++)
				{
					if(isset($lap_first_pass[$n]))
					{
						$display .= "<br>" . $n . ": " . $lap_first_pass[$n];
					}
				}
				
				$display .= "<table cellpadding=7>";
				$display .= "<tr>";
				$display .= "<td>Racer</td><td>Number</td><td>Lap</td><td>Best Lap</td><td>Last Lap</td><td>Gap</td><td>Pass $track_first_pass</td>";
				$display .= "</tr>";
				
				$n = 0;
				$ordered_racers = array();
				foreach($racer_info as $key=>$rinfo)
				{
					$setlap = $rinfo['lap'];
					$setgap = number_format($rinfo['passed'] - $lap_first_pass[$setlap],3,".",",");
					$rinfo['gap'] = $setgap;// - number_format(($rinfo['first_pass'] - $track_first_pass),3,".",",");
					$placed = false;
					$ordracers = $ordered_racers;
					$ordered_racers = array();
					for($i=0; $i<count($ordracers); $i++)
					{
						//if(!$placed && ($rinfo['lap'] > $ordracers[$i]['lap'] || ($rinfo['lap']==$ordracers[$i]['lap'] && $rinfo['lastlap'] < $ordracers[$i]['lastlap'])))
						if(!$placed && ($rinfo['gap'] < $ordracers[$i]['gap'] && $rinfo['gap']!=""))
						{
							$ordered_racers[] = $rinfo;
							$placed = true;
						}
						$ordered_racers[] = $ordracers[$i];
					}
					if(!$placed) 
					{
						$ordered_racers[] = $rinfo;
					}
				}
					
				foreach($ordered_racers as $rinfo)
				{
					$display .= "<tr>";
					$display .= "<td>" . $rinfo['name'] . "</td>";
					$display .= "<td>" . $rinfo['number'] . "</td>";
					$display .= "<td>" . $rinfo['lap'] . "</td>";
					$display .= "<td>" . $rinfo['bestlap'] . "</td>";
					$display .= "<td>" . $rinfo['lastlap'] . "</td>";
					$display .= "<td>" . $rinfo['gap'] . "</td>";
					$display .= "<td>" . $rinfo['passed'] . "</td>";
					$display .= "</tr>";
					
					$n++;
					$data .= "&name_$n=".$rinfo['name'];
					$data .= "&number_$n=".$rinfo['number'];
					$data .= "&lap_$n=".$rinfo['lap'];
					$data .= "&bestlap_$n=".$rinfo['bestlap'];
					$data .= "&lastlap_$n=".$rinfo['lastlap'];
					$data .= "&gap_$n=".$rinfo['gap'];
					$data .= "&highlight_$n=1";
				}
				if($highest_lap==0) $highest_lap = 1;
				$laps_left = $total_laps - ($highest_lap - 1);
				if($laps_left < 0) $laps_left = 0;
				$data = "highest_lap=$highest_lap&laps_left=$laps_left&count=$n" . $data;
				$display .= "</table>";
				
				$display .= "<br>cc=" . $company_code_full;
				$display .= "<br>loc_id=" . reqvar("loc_id");
				$display .= "<br>eventid=" . reqvar("eventid");
			}
			else
			{
				$display .= "Nobody Scheduled";
				$data .= "&count=0&";
			}
			
			if($mode=="data")
			{
				echo $data;
			}
			else
			{
				echo $display;
			}
		}
	}
?>

