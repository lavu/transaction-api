<?php
	session_start();
	require_once(dirname(__FILE__) . "/../comconnect.php");
	require_once(dirname(__FILE__) . "/./part_functions.php");
	
	
	$kart_id = reqvar("id");
	//echo "kart_id: $kart_id<br/>";
	
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>



<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Untitled Document</title>
<link rel="stylesheet" href="service_types_formCSS3.css" type="text/css">		
		<link rel="stylesheet" href="parts_form_select_element2.css" type="text/css">		
	



<style type="text/css">

body{
	font-family:Verdana, Geneva, sans-serif;
	color: #5A75A0;
	-webkit-tap-highlight-color:rgba(0,0,0,0);
	
}

h3{
	
	color: #5A75A0;
	font-family:Verdana, Geneva, sans-serif;
}

label{
	font-family:Verdana, Geneva, sans-serif;
	font-weight:700;
}
.panel{
	width:600px; 
	display:none;
	height:auto;
}

.leftForm{
	float:left;
	width:100px;
	text-align:right;
}

.rightForm{
	float:left;
	width:120px;
	text-align:left;
	
}

.clearBoth{
	clear:both;
}

.littleInput{
	background-color:#e3e3e3;
	border:1px solid #999;
	background-repeat:no-repeat;
	background-position:top left;
}

.row{
	
	width:550px;
	height:auto;
	margin-top:4px;
	
	
}

.rowElement{
	float:left;
	margin:2px 5px 2px 5px;
	text-align:center;
}

.re1{
	width:110px;
	height:auto;
	
}

.re2{
	width:220px;
	height:auto;
	
}

.re3{
	width:75px;
	height:auto;
	
}

.re4{
	width:55px;
	height:auto;
	
}


</style>

<style type="text/css">

table, th 
{
border: none;
border-collapse:collapse;
padding:0;
width:412px;
color:#333;
font-size:10pt;

margin:0;


}

td{
	border:0px solid #033;
	border-collapse:collapse;
	padding:0;
	color:#333;
	font-size:10pt;
	margin:0;
}


tr{
			height:auto;
			width:412px;
		
		
}

div{
	font-size:10pt;
}

textarea{
	width:200px;
	height:45px;
}

.trCaps{
	height:9px; 
	width:412px;
	
}

.centerCell{
	width:412px;
	
	background-image:url(images/tab_v1_mid.png);
	background-repeat:repeat-y;
	
	height:auto;
	
}



.topCap{
	background-image:url(images/tab_v1_top.png);
	background-repeat:no-repeat;
	height:9px;
}


.bottomCap{
	background-image:url(images/tab_v1_bot.png);
	background-repeat:no-repeat;
	height:9px;
}

.allDivs{
	float:left;
	border:0px solid #000;
	padding-left:5px;
	 font-size:10pt; 
	 font-weight:600;
	 color:#333;
}

.leftForm{
	float:left;
	width:220px;
	text-align:right;
}

.rightForm{
	float:left;
	text-align:left;
	padding-left:5px;
	width:30px;
}



</style>
<script type="text/javascript">
	
	
	
	
	
	
	function changeBackgroundImage(id){
			/* function changes background image to 'lit' or 'unlit' depending on what is appropriate, the id input parameter is the id assigned to each <table></table> element. it is the same as the part id since all parts have unique 	          IDs*/
			
			var lastRowLit = document.getElementById('lastTRLit').value;
			
			/* If a row is lit this hidden field will contain a value, the value is the id of the last selected <tr></tr> */
			if (lastRowLit != ""){
					
					/* change back to 'unlit' image */
						var topCapID = "topCap" + lastRowLit;
					
						document.getElementById(topCapID).style.backgroundImage="url('images/tab_v1_top.png')";
			
						document.getElementById(lastRowLit).style.backgroundImage="url('images/tab_v1_mid.png')";
			
						var botCapID = "botCap" + lastRowLit;
						document.getElementById(botCapID).style.backgroundImage="url('images/tab_v1_bot.png')";
		 
			}//if
			
			/* Change background image of current selected table row to the 'lit' image */
			var topCapID = "topCap" + id;
			document.getElementById(topCapID).style.backgroundImage="url('images/tab_v1_toplit.png')";
			
			document.getElementById(id).style.backgroundImage="url('images/tab_v1_midlit.png')";
			
			var botCapID = "botCap" + id;
			document.getElementById(botCapID).style.backgroundImage="url('images/tab_v1_botlit.png')";
		
			/* store id in hidden text field so we can switch that row off later */
			document.getElementById('lastTRLit').value = id; 
			
	}//changeBackgroundImage()
	/*
	function passDataToPartInfo(serviceName,id){
			alert("serviceName: " + serviceName + "\nid: "+id);
				var answer = confirm(serviceName + "service was completed on this kart?");
				if (answer){
						alert("service added to kart history");
				}//if
				//window.location = "_DO:cmd=load_url&c_name=service_types_info&f_name=lavukart/service_types_info.php?id=" + id + "&loadRightSide=true";
	}//passDataToGarageInfo()
*/
</script>		


<script type="text/javascript">

function howdy(){
		alert("howdy");
}

function show(input){
	
		/* function displays the repair or maintenance panel */
		if (input == "repair"){
				document.getElementById("Repair").style.display = "inline";
				document.getElementById("repair").innerHTML = "<img src=\"images/btn_tgl_off_lit.png\" width=\"186\" height=\"45\" />";
				document.getElementById("maintenance").innerHTML = "<img src=\"images/btn_tgl_maint.png\" width=\"186\" height=\"45\" />";
				document.getElementById("Maintenance").style.display = "none";
		}//if
		
		if (input == "maintenance"){
				document.getElementById("Repair").style.display = "none";
				document.getElementById("maintenance").innerHTML = "<img src=\"images/btn_tgl_maint_lit.png\" width=\"186\" height=\"45\" />";
				document.getElementById("repair").innerHTML = "<img src=\"images/btn_tgl_off.png\" width=\"186\" height=\"45\" />";
				document.getElementById("Maintenance").style.display = "inline";
		}//if
	
		
}//show()

function nextRow(index){
		/* This function will validate the row entry data, and if it passes validation it adds the next row */
		
		numberOfRows = document.getElementById("holdRowindex").value;
	//	alert("index: " + index + "\n numRows: " +  numberOfRows);
		if (index == numberOfRows){
					addNewRow();	
		}//if
}//nextRow()

function getData(){
		/* Get the user input from each row before we overwrite the .innerHTML, this way we can put it back after we overwrite the.innerHTML */
		numRows = document.getElementById("holdRowindex").value;
		storeData = "";
		//alert("numRows: " + numRows);
		for (j=1;j<=numRows*1;j++){
			//	alert("j: " + j);
				curID1 = "partNumber_"+j;
				var curVal1 = document.getElementById(curID1).value;
				storeData += curVal1+",";
				curID2 = "PartDesc_"+j;
				var curVal2 = document.getElementById(curID2).value;
				storeData += curVal2+",";
				curID3 = "Qty_"+j;
				var curVal3 = document.getElementById(curID3).value;
				storeData += curVal3+"_";
		//		alert("storeData: " + storeData);
		}//for
		
		return storeData;
		
}//getData

function dontNeedService(lastDateServiced){
		//alert(lastDateServiced);
		/* Format this date */
		lastDateServicedArr = lastDateServiced.split("-");
		year = lastDateServicedArr[0];
		month = lastDateServicedArr[1];
		day = lastDateServicedArr[2];
		formattedDate = month+"-"+day+"-"+year;
	
		alert("This kart does not need service. It was last serviced on " + formattedDate);
}//dontNeedService()

function is_part_id(rowNum){
		/* Did they actually enter a valid part number? */
		
		curRowID = "partNumber_"+rowNum;
		curRowVal = document.getElementById(curRowID).value;
		validPartIDs = document.getElementById("customPartIDs").value;
		validPartsArr = validPartIDs.split(",");
		matchIsFound = false;
		index = 0;
		reachedEnd = false;
		
		arrVal = validPartsArr[index].toLowerCase();
				curRowValLowerCase = curRowVal.toLowerCase();
		//alert(arrVal + " x " + curRowValLowerCase);
		while ( (matchIsFound != true) && (reachedEnd == false)){
				arrVal = validPartsArr[index].toLowerCase();
				curRowValLowerCase = curRowVal.toLowerCase();
				
				if (arrVal == curRowValLowerCase){
						matchIsFound = true;
						return true;
				}//if
				
				if (index >= validPartsArr.length-1){
						reachedEnd = true;
				}//if
				
				index++;
		}//while
		alert("This is not a valid part ID.");
		return false;
		//alert(curRowVal);
}//is_part_id()

function addNewRow(){
			storeData = getData();
			curNumberOfRows = document.getElementById("holdRowindex").value;
			curNumberOfRows++;
			//alert("curNumRows: " + curNumberOfRows);
			var dispRowString = document.getElementById("holdRows").innerHTML;
			dispRowString += "<div class=\"row\" id=\"row_"+curNumberOfRows+"\">";
			dispRowString += "<div class=\"rowElement re1\">";
			dispRowString += "<input type=\"text\" class=\"littleInput\" onblur=\"is_part_id('"+curNumberOfRows+"');\" name=\"partNumber_"+curNumberOfRows+"\" id=\"partNumber_"+curNumberOfRows+"\" style=\"width:80px;\"/>";
			dispRowString += "<br/>";
							dispRowString += "<select class='styled-select-small' name=\"findByName_"+curNumberOfRows+"\" id=\"findByName_"+curNumberOfRows+"\" onchange=\"enterPartFromList('"+curNumberOfRows+"');\" style=\"width:80px;\">";
								dispRowString	+=	"<option value=\"\"></option>";
										<?php
											$getPartsRes = lavu_query("SELECT name,id,custom_part_id FROM lk_parts");
											while ($getPartsRow = mysqli_fetch_assoc($getPartsRes)){
														$partID = $getPartsRow['id'];
														$partName = $getPartsRow['name'];
														$custom_part_id = $getPartsRow['custom_part_id'];
														//$randThing = date("YmdHis").rand(0,9999999999);
														echo "dispRowString += \"<option value='$custom_part_id'>$custom_part_id: &nbsp;&nbsp; &nbsp;&nbsp;     $partName</option>\";";
											}//while
										?>
						dispRowString +=	"</select>";
						dispRowString += "</div>";
						
						
						
						dispRowString += "<div class=\"rowElement re2\">";
							dispRowString += "<textarea name=\"PartDesc_"+curNumberOfRows+"\" id=\"PartDesc_"+curNumberOfRows+"\" class='myTextArea'></textarea>";
						dispRowString += "</div>";
						
						
						
						dispRowString += "<div class=\"rowElement re3\">";
							
							dispRowString += "<select class='styled-select-small'  name=\"Qty_"+curNumberOfRows+"\" id=\"Qty_"+curNumberOfRows+"\"  onchange=\"nextRow('"+curNumberOfRows+"');\"/>";
									dispRowString += "<option value=\"0\">0</option>";
									<?php
											for ($i=1;$i<=10;$i++){
													echo "dispRowString += \"<option value='$i'>$i</option>\";";
											}//for
									?>
							dispRowString += "</select>";
						dispRowString += "</div>";
						
						dispRowString += "<div class=\"rowElement re4\">";
						dispRowString +=	"<img src=\"images/btn_x_30.png\" onclick=\"deleteRow('"+curNumberOfRows+"');\" width=\"28\" height=\"29\" />";
						dispRowString += "</div>";
						dispRowString += "<div style=\"clear:both;\"></div>";
			dispRowString += "</div><!--row-->";
			
			document.getElementById("holdRows").innerHTML = dispRowString;
			document.getElementById("holdRowindex").value = curNumberOfRows;//this needs to be above putData();
			putData(storeData);
}//addNewRow()

function deleteRow(rowNumber){
		//alert("rowNumber: " + rowNumber);
		numRows = document.getElementById("holdRowindex").value;
		if (rowNumber == numRows){
					//rowID = "row_"+rowNumber;
					//document.getElementById(rowID).style.display = "none";
					qtyID = "Qty_"+rowNumber;
					document.getElementById(qtyID).value = 0;
					
					partNumberID = "partNumber_"+rowNumber;
					document.getElementById(partNumberID).value = "";
					
					partDescID = "PartDesc_"+rowNumber;
					document.getElementById(partDescID).value = "";
		}//if
		
		if (rowNumber != numRows){
					rowID = "row_"+rowNumber;
					document.getElementById(rowID).style.display = "none";
					qtyID = "Qty_"+rowNumber;
					document.getElementById(qtyID).value = 0;
		}//if
		
}//deleteRow()


function enterPartFromList(rowNumber){
			//alert("rowNumber: " + rowNumber);
			curID = "findByName_"+rowNumber;
			partIdFromList = document.getElementById(curID).value;
			//alert("partID: " + partIdFromList);
			eval(document.getElementById(curID).value = "");
			eval(document.getElementById("partNumber_"+rowNumber).value = partIdFromList);
			
			//alert("partIDFOM: " + partIdFromList);
}//enterPartFromList()

function putData(data){
			numRows = document.getElementById("holdRowindex").value;
			dataRowsArr = data.split("_");
			//alert("data from putData(): " + data);
			
		//alert("numRows: " + numRows);
			for (j=1;j<numRows*1;j++){
				//	alert("j: " + j);
					dataThisRowArr = dataRowsArr[j-1].split(",");
				//	alert("dataThisRwos: " + dataThisRowArr[0]);
					curID1 = "partNumber_"+j;
					document.getElementById(curID1).value = dataThisRowArr[0];
					
					curID2 = "PartDesc_"+j;
					document.getElementById(curID2).value = dataThisRowArr[1];
					
					
					curID3 = "Qty_"+j;
					document.getElementById(curID3).value = dataThisRowArr[2];
					
					
			//		alert("storeData: " + storeData);
			}//for
}//putData()


function showCheckList(id){
				//alert("id: " + id);
					kartId = document.getElementById("kartID_maint").value;
					window.location = "_DO:cmd=create_overlay&f_name=lavukart/service_types_addServiceTypeWrapper.php;lavukart/service_showCheckList.php&c_name=checkList;checkList&dims=107,150,574,608;107,150,574,608&dont_block=1?id="+id+"&kartID="+kartId;
		}//service_types_addServiceTypeScript()
		

function validate(){
	
		numRows = document.getElementById("holdRowindex").value;
		//alert("wnumrwos: " + numRows);
		for (i=1;i<=numRows*1;i++){
				curQtyID = "Qty_"+i;
				curQtyVal = document.getElementById(curQtyID).value;
				curpartNumberID = "partNumber_"+i;
				curpartNumberVal = document.getElementById(curpartNumberID).value;
				//alert("curVal: " + curQtyVal);
				if ((curQtyVal == 0*1) && (curpartNumberVal != "") ){
						alert("You have a quantity of zero for one of your parts. It cannot be submitted to the database this way. Please click 'X' to remove the part if you wish to delete it.");
						return false;
				}
		}//for
		return true;
}//validate()

</script>


</head>

<body>
<h3>Add a Service Performed</h3>

<div style="width:600px; height:auto; "><a href="javascript:show('repair');"><div id="repair" style="float:left;"><img src="images/btn_tgl_off.png" width="186" height="45" /></div></a> <a href="javascript:show('maintenance');"><div id="maintenance" style="float:left;"><img src="images/btn_tgl_maint.png" width="186" height="45" /></div></a></div>
<div style="clear:both;"></div>

<div class="panel" name="Repair" id="Repair">


<form method="post" name="repair_form" onsubmit="return validate();" action="service_add_service_process.php">
<input type="hidden" value="<?php echo sessvar("loc_id"); ?>" name="loc_id"/>

<?php
		$fromMaintenance = reqvar("show_maintenance");
		echo "<input type='hidden' name='from_maintenance' id='from_maintenance' value='repair'/>";
		/* They want to be able to classify part added to each kart as part of a routine maintenance or just a random repair*/
		if ($fromMaintenance == "yes"){
				echo "<script type='text/javascript'>document.getElementById('from_maintenance').value = 'maintenance';</script>";
		}//if
		
?>


<div class="row">
			<div class="rowElement re1"><label>Part &#35;</label></div>
			<div class="rowElement re2"><label>Desc.</label></div>
			<div class="rowElement re3"><label>Qty.</label></div>
			<div class="rowElement re4"><label>Remove</label></div>
			<div style="clear:both;"></div>
</div><!--row-->

<div class="clearBoth"></div>






<span id="holdRows">

</span><!--holdRows-->

<div class="clearBoth"></div>

<div class="row">

		<div class="rowElement re1">
			<input type="submit" name="Submit" value="Add" class="submitButton"/>
		</div>
		<div class="rowElement re2"></div>
		<div class="rowElement re3"></div>
		<div class="rowElement re4"></div>
		<div style="clear:both;"></div>
</div><!--row-->

<input type="hidden" name="holdRowIndex" value="0" id="holdRowindex" />
<input type="hidden" name="kartID" value="<?php echo reqvar("id"); ?>" id="kartID" />

<?php
//echo "locationid: $locationid<br/>";
	$getCustPartIDRes = lavu_query("SELECT custom_part_id FROM lk_parts");
	$custPartsString = "";
	while ($custPartsRow = mysqli_fetch_assoc($getCustPartIDRes)){
			$custPartsString .= $custPartsRow['custom_part_id'].",";
				
	}//while
	/* FIX THIS !!! */ /* Need to be able to verify they entereed a valid id */
	
	echo "<input type=\"hidden\" name=\"customPartIDs\" id=\"customPartIDs\" value=\"$custPartsString\"/>";
?>



</form>

</div><!--Repair-->


<div class="panel" name="Maintenance" id="Maintenance">

		<form method="post" name="maintenance_form" action="">
	
		<h3>Routine Maintenance</h3>
		
		
					<?php
		
				echo "<input type=\"hidden\" name=\"lastTRLit\" id=\"lastTRLit\">"; /*Store the id of the last <tr></tr> element selected so we can change the background image back to unlit */
				
		?>
		
		<table>
				
							
		<?php
			
				$locationid = sessvar("loc_id");
			
				$results = lavu_query("SELECT * FROM lk_service_types WHERE locationid=\"[1]\" ORDER BY name", $locationid);
				
				while ($rows = mysqli_fetch_assoc($results)){	
						$id = $rows['id'];	
						$durationStr = $rows['duration'];
						$durationArr = explode("_",$durationStr);
						$durationClass = $durationArr[0];//month,day,week,...
						$durationQty = $durationArr[1];//how many months/days/weeks...
						
						
						$topCapID = "topCap" . $id;
						$botCapID = "botCap" . $id;
						
						
							
							/* We track when the last date this service was performed on this kart, and if, say a weekly service was performed within one week ago, the mechanic cannot open the pop up for the weekly service for this kart */
							$getLastServiceDate = lavu_query("select * from lk_service_complete WHERE service_type_id='[1]' AND kart_id='[2]' order by datetime DESC",$id,$kart_id);
							
							
							$service_performed_arr = array();
							$index = 0;
							while ($getLastServiceDateRow = mysqli_fetch_assoc($getLastServiceDate)){
												$service_performed_arr[$index] = $getLastServiceDateRow['datetime'];
							}//while
							$lastDatePerformedStr = $service_performed_arr[0];
							$lastDatePerformedArr = explode(" ",$lastDatePerformedStr);
							$lastDatePerformedDay = $lastDatePerformedArr[0];			
							
							$lastDateArr = explode("-",$lastDatePerformedDay);
							$curDateYear = $lastDateArr[0];//initially, these values are the day the service was last performed
							$curDateMonth = $lastDateArr[1];
							$curDateDay = $lastDateArr[2];
							$curDate = $curDateYear."-".$curDateMonth."-".$curDateDay;
							$lastServicedDate = $curDate;//we'll need this value later
							//echo "curdate:$curDate<br/>";
							$lastDayMkTime = mktime(0,0,0,$curDateMonth*1,$curDateDay*1-1,$curDateYear*1);//we have to take one day off to get the correct stop date
							$stopDate = getStopDateTS($lastDayMkTime,$durationClass,$durationQty);/*last day of the period where maintenance cannot be performed because it was already done for that week/month/day*/
							//echo "stopDate: $stopDate<br/>";
							$currentDate = date("Y-m-d");	
							$stopLoop = false;
							$serviceCanBePerformed = "Due for service";//assume it can be performed
							$index = 0;
							if ($curDate != "--"){
								while ($stopLoop == false){
						  		
								//	echo "$currentDate $curDate<br/>";
									if ($currentDate == $curDate){//today's date == the date we're looking at at this instant
											$serviceCanBePerformed = "Not due for service";
										//	echo "they match<br/>";
											$stopLoop = true;
									}//if
									
									if ($curDate == $stopDate){
											
										//	echo "out of bounds</br>";
											$stopLoop = true;
									}//if
									
									//get the next day formatted as yyyy-mm-ddd
							
									$curDateArr = explode("-",$curDate);
									//$curDateYear = $curDateArr[0];
									//$curDateMonth = $curDateArr[1];
									//$curDateDay = $curDateArr[2];
									$curDateMkTime = getdate(mktime(0,0,0,$curDateMonth*1,$curDateDay*1 + $index*1, $curDateYear*1));
									$curDate = $curDateMkTime[year]."-".pad($curDateMkTime[mon],2)."-".pad($curDateMkTime[mday],2);
									//echo "myCurDate: $curDate<br/>";
									$index++;
									
									//$stopLoop = true;
								}//while
							}//if
							if ($serviceCanBePerformed == "Due for service"){
									$serviceColor = "red";
							}//if
							if ($serviceCanBePerformed != "Due for service"){
									$serviceColor = "green";
							}//if
							
							
							
							
							echo "<div class=\"topCap\" id=\"$topCapID\"></div>";
						
			
							
						echo "<div id=\"$id\" onclick=\"";
						
						if ($serviceCanBePerformed == "Due for service"){
								echo "showCheckList('$id');"; 
						}//if
						
						if ($serviceCanBePerformed != "Due for service"){
								echo "dontNeedService('$lastServicedDate');"; 
						}//if
						
						echo "changeBackgroundImage('$id');\" style=\"background-image:url(images/tab_v1_mid.png); background-repeat:repeat-y; width:612px;\">";
							
							
										echo "<div class=\"allDivs\" style=\"width:400px;\"><span style=\"float:left;\">" . showQuote($rows['name']) . "</span><span style='margin-left:15px; color:$serviceColor; float:left; width:150px;'>$serviceCanBePerformed</span></div>";
										
									//	echo "<div class=\"allDivs\" style=\"width:45px;\" >" . showQuote($rows['man_hours']) . "</div>";
							
							
										echo "<div class=\"allDivs\"  style=\"width:275px;\">";
							
										/* List parts. Parts are stored in database as quantity x part id */
										$partsStr = $rows['parts'];
										$partsStrExploded = explode(",", $partsStr);
							
										$lengthOfPartsStrExploded = count($partsStrExploded);
										//$storePartsQuantityAndID; //$storePartsQuantityAndID[0] = part id; $storePartsQuantityAndID[1] = quantity
							
										$totalCost = 0;
										for ($i=0; $i<$lengthOfPartsStrExploded; $i++){
													$currentPart = $partsStrExploded[$i];
													$currentPartExploded = explode("@", $currentPart);
													$partID = $currentPartExploded[0];
													$quantity = $currentPartExploded[1];
										
													$resultsGetPartInfo = lavu_query("SELECT * FROM lk_parts WHERE id=\"[1]\" AND locationid=\"[2]\"", $partID, $locationid);
										
													
													
													$partRows = mysqli_fetch_assoc($resultsGetPartInfo);
													$partName = showQuote($partRows['name']);
													//$costOfThisPart = showQuote($partRows['cost']);
													//$totalCost = $totalCost + ($quantity * $costOfThisPart); 
													echo "
													<div class=\"leftForm\">$partName</div>
													<div class=\"rightForm\"></div>
													<div style=\"clear:both;\"></div>";
											
										
										
										
										}//for
										
								echo "</div>";
							
							
							 
							
										echo "<div class=\"allDivs\"  style=\"width:55px; text-align:right;\" ></div>";
										echo "<div style=\"clear:both;\"></div>";
								echo "</div>";
							
					
						
						echo "<div class=\"bottomCap\" id=\"$botCapID\"></div>";
						
				}//while
				
		?>
		
		<input type="hidden" name="kartID_maint" value="<?php echo reqvar("id"); ?>" id="kartID_maint" />
		
		</form>

</div><!--Maintenance-->


<?php
	
		$_SESSION['kartID'] = reqvar("id");
		//$locationid = sessvar("loc_id");
		//echo "kartID: $kartID<br/>
		//			locid: $locationid<br/>";
	
	
		$showMaintenance = reqvar("show_maintenance");
		//echo "showMaintenance: $showMaintenance<br/>";
		if ($showMaintenance == "yes"){
			//	$kart_id = reqvar("id");
				echo "<script type='text/javascript'>
				
						alert(\"You may now enter parts used during the maintenance, if any.\");
						show('repair');
						document.getElementById('kartID_maint').value = $kart_id;
						document.getElementById('kartID').value = $kart_id;
				</script>";
		}//if
?>


<script type="text/javascript">
	addNewRow();
	//addNewRow();
</script>
</body>
</html>


<?php
function getStopDateTS($lastDatePerformedTS,$class,$qty){
			/* Take in time stamp of date last performed, if it's a week/month/day, and how many weeks/months/days, return the date in format yyyy-mm-dd*/
			
			$dateInfo = getdate($lastDatePerformedTS);
			$dateMonth = $dateInfo[mon];
			$dateDay = $dateInfo[mday];
			$dateYear = $dateInfo[year];
			//	$lastDayMkTime = mktime(0,0,0,$curDateMonth*1,$curDateDay*1,$curDateYear*1);
			if ($class == "weeks"){
					$stopDate = mktime(0,0,0,$dateMonth*1,$dateDay*1 + 7*$qty,$dateYear*1);
			}//if
			
			if ($class == "months"){
					$stopDate = mktime(0,0,0,$dateMonth*1 + $qty*1,$dateDay*1,$dateYear*1);
			}//if
			
			if ($class == "days"){
					$stopDate = mktime(0,0,0,$dateMonth*1,$dateDay*1 + $qty*1,$dateYear*1);
			}//if
			
			$stopDateFormat = getdate($stopDate);
			$stopDateReturn = $stopDateFormat[year]."-".pad($stopDateFormat[mon],2)."-".pad($stopDateFormat[mday],2);
			
			return $stopDateReturn;
}//getStopDateTS()

function pad($num,$len){
		
		while (strlen($num) < $len){
				$num = "0".$num;
		}//while
		return $num;
		
}//pad()

?>