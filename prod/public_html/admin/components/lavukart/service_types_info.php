<?php
			require_once(dirname(__FILE__) . "/../comconnect.php");
			require_once(dirname(__FILE__) . "/./part_functions.php");
			session_start();
?>


<?php
		$var = reqvar("loadRightSide");
		if ($var != "true"){
				exit();
		}//if
		?>

<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<title>Garage Info</title>

		<style type="text/css">
<!--
body {
	background-color: transparent;
	margin-left: 0px;
	margin-top: 0px;
	margin-right: 0px;
	margin-bottom: 0px;
	-webkit-tap-highlight-color:rgba(0,0,0,0);
	
	font-family:Verdana, Geneva, sans-serif;
	color:#333;
}




.submitButton{
	/* also see .submitButton in service_types_formCSS3.css */
	background-image:url(images/btn_update_100x33.png);
	
}

.littleInput{
	/*background-color:#e3e3e3;*/
	border:0px solid #999;
	width:262px;
	height:47px;
	background-color:transparent;
	background-repeat:no-repeat;
	background-image:url(images/tab_bg_262.png);
	background-position:top left;
}




-->
		</style>
	
<link rel="stylesheet" href="service_types_formCSS3.css" type="text/css">		
		<link rel="stylesheet" href="parts_form_select_element2.css" type="text/css">		
	
<script type="text/javascript" src="service_types_form1.js"></script>		




<script type="text/javascript">

function deleteRow(id){
			answer = confirm("Do you wish to delete this entry?");
			if (answer){
						actualID = "row_"+id;
						rowIndex = document.getElementById("rowIndex").value;
						curNameID = "name_"+id;
						curCategoryID = "category_"+id;
						if (id == rowIndex){
									
									document.getElementById(curNameID).value = "Name";
									document.getElementById(curCategoryID).value = "";
						}//if
						if (id != rowIndex){
									document.getElementById(actualID).style.display = "none";
									document.getElementById(curNameID).value = "Name";
									document.getElementById(curCategoryID).value = "";
						}//if
			}//if
}//deleteRow()

function addRow(rowID){
			//alert("hello");
			
			
			
			//alert("rowID: " + rowID);
			
			rowIndex = document.getElementById("rowIndex").value;
			
			if (rowID != rowIndex){
					/* Only add a new row if the last row was updated */
					return false;
			}//if
			data = getData();
			rowIndex++;
			//alert("data1: " + data);
			str = document.getElementById("holdRows").innerHTML;
			str +=  "<div class=\"\" id='row_"+rowIndex+"'>";
			str +=	"<input type=\"text\" class=\"littleInput\" name=\"name_"+rowIndex+"\" value=\"Name\" onblur=\"adjustNameFieldVal(this.value,'"+rowIndex+"');\" onclick=\"adjustNameFieldVal(this.value,'"+rowIndex+"');\" onchange=\"addRow('"+rowIndex+"');\" id=\"name_"+rowIndex+"\" style=\"float:left;\"/>";
					
			str +=	"<select class='styled-select' name=\"category_"+rowIndex+"\" id=\"category_"+rowIndex+"\" style=\"float:left; display:none; margin-top:5px;\" onchange=\"addRow('"+rowIndex+"');\">";
			str +=	"<option value=\"\">Category</option>";
			
			<?php				
							$locationid = reqvar("loc_id");
							$res1 = lavu_query("SELECT * FROM lk_categories WHERE locationid=\"[1]\" ORDER BY Category",$locationid);
							while ($row1 = mysqli_fetch_assoc($res1)){
										echo "str += \"<option value='".$row1['Category']."'>".$row1['Category']."</option>\";";
							}//while
			?>						
			str += "</select>";	
			str += "<input type='hidden' name='service_info_"+rowIndex+"' id='service_info_"+rowIndex+"'/>";
			str += "<img src=\"images/btn_x_30.png\" style=\"margin-top:5px; margin-left:25px;\" onclick=\"deleteRow('"+rowIndex+"');\" width=\"28\" height=\"29\">";
	    str += "</div>";
			str += "<div style=\"clear:both;\">&nbsp;</div>";
			
			document.getElementById("rowIndex").value = rowIndex;
			//alert("rowIndex: " + rowIndex);
			document.getElementById("holdRows").innerHTML = str;
			putData(data);
}//addRow()

function adjustNameFieldVal(val, idNum){
			//alert("val: " + val + "\nidNum: " + idNum);
			curID = "name_"+idNum;
			
			if (val == ""){
						document.getElementById(curID).value = "Name";		
			}
			
			if (val == "Name"){
						document.getElementById(curID).value = "";		
			}
			
}//adjustNameFieldVal()


function putData(data){
			//alert("data; " + data);
			rowIndex = document.getElementById("rowIndex").value;
			dataArr = data.split("_");
			//alert("dataArr[0]: " + dataArr[0]);
			for (i=0;i<rowIndex-1;i++){
						//alert("works");
						curDataArr = dataArr[i].split(",");
					//	alert("curDataArr[0]: " + curDataArr);
						
						curNameID = "name_"+(i*1+1);
						document.getElementById(curNameID).value = curDataArr[0];
						//alert("[0], [1]: " + curDataArr[0] + ", " + curDataArr[1]);
						curCategoryID = "category_"+(i*1+1);
						//alert("curDataArr[1]: " + curDataArr[1]);
						document.getElementById(curCategoryID).value = curDataArr[1];
						
						curServiceEntryID = "service_info_"+(i*1+1);
						//alert("curDataArr[1]: " + curDataArr[1]);
						document.getElementById(curServiceEntryID).value = curDataArr[2];
						
						//data += curName + "," + curCategory + "_";
			}//for
			
}//putData()

function getData(){
			
			data = "";
			rowIndex = document.getElementById("rowIndex").value;
			
			for (i=1;i<=rowIndex;i++){
					
						curNameID = "name_"+i;
						curName = document.getElementById(curNameID).value;
						
						curCategoryID = "category_"+i;
						curCategory = document.getElementById(curCategoryID).value;
						
						curServiceEntryID = "service_info_"+i;
						curServiceEntryVal = document.getElementById(curServiceEntryID).value;
						
						data += curName + "," + curCategory + "," + curServiceEntryVal + "_";
			}//for
			//alert(data);
			return data;
			
}//getData()

</script>



	</head>
	
	
	
<body>
	
	<form action="service_types_updateService.php" method="post" name="addServiceForm" id="service_types_addServiceTypeForm" onSubmit="return validateForm();">
	
	
	<?php
				// this form is used in service_types_info.php and service_types_addServiceType.php, but they call different processing scripts
				//INCLUDE 'service_types_formInclude.php';
	?>
	
	<?php
			
			$serviceId = $_POST['id'];
			$_SESSION['serviceID'] = $serviceId;
			
			
			
			$results = lavu_query("SELECT * FROM lk_service_types WHERE id=\"[1]\" AND locationid=\"[2]\"", $serviceId, $locationid);
			
				
			$rows = mysqli_fetch_assoc($results);	
						
							
							
							$serviceName = showQuote($rows['name']);
							
							$duration = showQuote($rows['duration']);
							//echo "duraction: $duration<br/>";
	
echo "
	<div class=\"leftForm\">
				<label>Name</label>
	</div>
	
	<div class=\"rightForm\">
				<input class=\"littleInput\" type=\"text\" name=\"name\" value=\"$serviceName\" />
	</div>
	
	<div style=\"clear:both;\">&nbsp;</div>
	
	
	<div class=\"leftForm\">
				<label>Interval</label>
	</div>
	
	<div class=\"\">
			<select name=\"duration\" class='styled-select-150' id=\"duration\">
			
						<option value=\"rightForm\"></option>";
						
						for ($i=1;$i<30;$i++){
								$selected = "";
								if ("days_".$i == $duration){
										$selected = "selected";
								}//
								echo "<option value=\"days_".$i."\" $selected>$i days</option>";
						}//for
						
						for ($i=1;$i<=3;$i++){
								$selected = "";
								if ("weeks_".$i == $duration){
										$selected = "selected";
								}//
								echo "<option value=\"weeks_".$i."\" $selected>$i weeks</option>";
						}//for
						
						for ($i=1;$i<12;$i++){
								$selected = "";
								if ("months_".$i == $duration){
										$selected = "selected";
								}//
								echo "<option value=\"months_".$i."\" $selected>$i months</option>";
						}//for
										
						echo "</select>";
						
						echo "</div>";
						echo "<div style=\"clear:both;\">&nbsp;</div>";
						echo "	<span id=\"holdRows\">
						
					</span>";
	
	
	
	
		
?>
	
	
	
	</span><!--holdRows-->

	
		<input type="hidden" name="rowIndex" value="0" id="rowIndex"/>
		<input type="hidden" name="service_id" id="service_id" value="<?php echo $serviceId; ?>"/>
		<input type="submit" class="submitButton" value="" onClick="">
		
		
		
		<?php
				$resultsInfo = lavu_query("SELECT * FROM lk_service_info WHERE service_id=\"[1]\" AND locationid=\"[2]\"", $serviceId, $locationid);
				$index = 0;
				$dataStr = "";
				while ($rows = mysqli_fetch_assoc($resultsInfo)){
							$curName = $rows['name'];
							$curCat = $rows['category'];
							$curID = $rows['id'];
							$dataStr .= $curName.",".$curCat.",".$curID."_";
							$index++;
						//	echo "$curName: $curCat<br/>";
				}//while
	?>
				<input type="hidden" value="<?php echo $dataStr;?>" name="dataStr" id="dataStr"/>
			<script type="text/javascript">
					storedData = document.getElementById("dataStr").value;
					storedDataArr = storedData.split("_");
					arrLength = storedDataArr.length-1;
					//alert("length: " + arrLength);
					for (z=0;z<=arrLength;z++){
							addRow(z);
					}//for
					putData(document.getElementById("dataStr").value);
					//addRow(0);
			</script>
		<input type="hidden" name="loc_id" value="<?php echo $locationid;?>"/>
</form>

	
	
				
	
</body>
	
	
	
</html>