<html>
	<head>
		<title>Scoreboard</title>

<style type="text/css">
<!--
body {
	background-color: transparent;
	margin-left: 0px;
	margin-top: 0px;
	margin-right: 0px;
	margin-bottom: 0px;
}
.toptitles {
	font-family: Verdana, Geneva, sans-serif;
	font-size: 10px;
	font-weight: bold;
	color: #0D2960;
}

.carnumber {
	font-family: Verdana, Geneva, sans-serif;
	font-size: 16px;
	font-weight: bold;
	color: #FFF;
}
.creditnumber {	font-family: Verdana, Geneva, sans-serif;
	font-size: 13px;
	font-weight: bold;
	color: #3d4f6d;
}
.nametext {	font-family: Verdana, Geneva, sans-serif;
	font-size: 13px;
	font-weight: bold;
}
.gridposition {	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 14px;
	font-weight: bold;
	color: #5A75A0;
}
.icontext {font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 11px;
	font-weight: bold;
	color: #5A75A0;
}
.subtitles {	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 14px;
	font-weight: bold;
	color: #5A75A0;
}
.title1 {	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 18px;
	font-weight: bold;
	color: #5A75A0;
}
.subtitles1 {font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 13px;
	font-weight: bold;
	color: #5A75A0;
}
-->
</style>
	</head>

	<?php
  	require_once(dirname(__FILE__) . "/../comconnect.php");
			
		if(reqvar("loc_id")) $loc_id = reqvar("loc_id");
		else if(sessvar_isset("loc_id")) $loc_id = sessvar("loc_id");
		set_sessvar("loc_id", $loc_id);
		
		$trackid = sessvar("trackid");
		if(!$trackid)
		{
			$track_query = lavu_query("select * from `lk_tracks` where `locationid`='$loc_id' and `_deleted`!='1' order by id asc limit 1");
			if(mysqli_num_rows($track_query))
			{
				$track_read = mysqli_fetch_assoc($track_query);
				$trackid = $track_read['id'];
				set_sessvar("trackid",$trackid);
			}
		}
  ?>

	<script type="text/javascript">
	function trim(stringToTrim) {
		return stringToTrim.replace(/^\s+|\s+$/g,"");
	}
	function setrp(n,key,val)
	{
		//alert(key + "_" + n + " = " + val[key + "_" + n]);
		document.getElementById(key + "_" + n).innerHTML = val[key + "_" + n];
	}
	function clearrp(n,key)
	{
		document.getElementById(key + "_" + n).innerHTML = "&nbsp;";
	}
	function loadXMLDoc(set_eventid)
	{
	if (window.XMLHttpRequest)
	  {// code for IE7+, Firefox, Chrome, Opera, Safari
	  xmlhttp=new XMLHttpRequest();
	  }
	else
	  {// code for IE6, IE5
	  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
	  }
	xmlhttp.onreadystatechange=function()
	  {
	  if (xmlhttp.readyState==4 && xmlhttp.status==200)
		{
			data = xmlhttp.responseText;
			data_split = data.split("&");
			vars = new Array();
			vars['server_time'] = "";
			vars['laps_left'] = "";
			for(i=0; i<data_split.length; i++)
			{
				data_part = trim(data_split[i]);
				if(data_part!="")
				{
					data_parts = data_part.split("=");
					if(data_parts.length > 1)
					{
						data_key = data_parts[0];
						data_val = data_parts[1];
						vars[data_key] = data_val;
					}
				}
			}
			document.getElementById("lap_status").innerHTML = "LAPS LEFT: <font style='font-size:16px'>" + vars['laps_left'] + "</font>";
			document.getElementById("show_server_time").innerHTML = vars['server_time'];
			
			if(vars['count'])
			{
				for(i=1; i<=vars['count']; i++)
				{
					setrp(i,"number",vars);
					setrp(i,"name",vars);
					setrp(i,"lastlap",vars);
					setrp(i,"bestlap",vars);
					setrp(i,"gap",vars);
					setrp(i,"showlap",vars);
				}
				for(i=vars['count'] + 1; i<=12; i++)
				{
					clearrp(i,"number");
					clearrp(i,"name");
					clearrp(i,"lastlap");
					clearrp(i,"bestlap");
					clearrp(i,"gap");
					clearrp(i,"showlap");
				}
			}
		}
	  }
	rnd = Math.floor(Math.random()*1111);
	xmlhttp.open("GET","race_scores.php?cc=<?php echo $company_code_full?>&loc_id=<?php echo $loc_id?>&rnd=" + rnd + "&trackid=<?php echo $trackid?>&eventid=active&mode=data",true);
	xmlhttp.send();
	}
	</script>

	<body>
		<table width="964" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td height="42" align="center" class="title1">&nbsp;</td>
            <td align="left" valign="bottom" ><table width="900" border="0" cellspacing="0" cellpadding="8">
              <tr>
                <td width="32" class="toptitles">KART</td>
                <td width="246" class="toptitles">
                	<table cellspacing=0 cellpadding=0 width='100%'><td width='50%' class='toptitles'>RACER NAME</td><td width='50%' class='toptitles' id='show_server_time'>&nbsp;</td></table>
                </td>
                <td width="200" class="toptitles" id="lap_status">LAPS: </td>
                <td width="100" align="center" class="toptitles">LAST LAP</td>
                <td width="100" align="center" class="toptitles">FASTEST LAP</td>
                <td width="100" align="center" class="toptitles">GAP TIME</td>
                <td width="25" align="right" class="toptitles">LAP</td>
              </tr>
            </table></td>
          </tr>
          <?php 
		  	for($i=1; $i<=12; $i++) { ?>
          <tr>
            <td width="50" height="46" align="center" class="title1"><?php echo $i?></td>
            <td width="614" background="images/tab_wide_bg.png"><table width="900" height="44" border="0" cellspacing="0" cellpadding="8">
              <tr>
                <td width="32" align="center" valign="bottom" class="nametext"><span class="carnumber" id="number_<?php echo $i?>">&nbsp;</span></td>
                <td width="450" class="nametext" id="name_<?php echo $i?>">&nbsp;</td>
                <td width="100" class="nametext" id="lastlap_<?php echo $i?>">&nbsp;</td>
                <td width="100" class="nametext" id="bestlap_<?php echo $i?>">&nbsp;</td>
                <td width="100" class="nametext" id="gap_<?php echo $i?>">&nbsp;</td>
                <td width="21" align="center" valign="middle" class="nametext" id="showlap_<?php echo $i?>">&nbsp;</td>
              </tr>
            </table></td>
          </tr>
          <?php } ?>
          <tr>
            <td height="4" align="center" valign="middle"></td>
            <td  height="4"></td>
          </tr>
        </table>
	</body>
	<script language='javascript'>loadXMLDoc();</script>
	<script language='javascript'>setInterval(function(){loadXMLDoc()},1000);</script>
</html>