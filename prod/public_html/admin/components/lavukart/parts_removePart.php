<?php
session_start();
require_once(dirname(__FILE__) . "/../comconnect.php");
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Untitled Document</title>


<script type="text/javascript">
	
	function reloadGarageInfo(){
				
				window.location = "_DO:cmd=load_url&c_name=parts_list&f_name=lavukart/parts_list.php?";
				//window.location = "_DO:cmd=load_url&c_name=parts_info&f_name=lavukart/parts_info.php?";
	
	}//reloadGarageInfo()
	
</script>

</head>

<body>

<?php

	$deleteThisPartID = $_SESSION['partID'];
	
	$locationid = sessvar("loc_id");
	//mail("ck@poslavu.com","loc: $locationid and partID: $deleteThisPartID","");
	lavu_query("DELETE FROM lk_parts WHERE id='[1]' AND locationid=\"[2]\"" , $deleteThisPartID, $locationid);



	/* Now remove the deleted part from the 'parts' field in the lk_service_types database. */
	$results = lavu_query("SELECT * FROM lk_service_types WHERE locationid=\"[2]\"" , $locationid);
	
	while ($rows = mysqli_fetch_assoc($results)){
		
				$tempServiceId = $rows['id'];//service id
				$partsList = $rows['parts'];
				
				$partsArray = explode(",", $partsList);
				
				$keepThesePartIDsAndQuantities = array(); //This array stores the part IDs and quantities to be kept, (PartID1 x Quantity,PartID2 x Quanity, ...)
				$index = 0;
				for ($i=0; $i < count($partsArray); $i++){
					// id x qty
						$curPartPieces = explode("@", $partsArray[$i]);
						$curPartID = $curPartPieces[0];
						
						/* Keep all the partIDsxQuantities where the partID does not equal the id of the part that is being deleted */
						if ($curPartID != $curPartPieces){
									$keepThesePartIDsAndQuantities[$index] = $partsArray[$i];
									$index++;
						}//if
						
				}//for
				
				$submitPartsListToDB = implode(",", $keepThesePartIDsAndQuantities);
				lavu_query("UPDATE lk_service_types SET parts='[1]' WHERE id=\"[2]\" AND locationid=\"[6]\"", $submitPartsListToDB, $tempServiceId, $locationid);
		
	}//while

	unset($_SESSION['partID']);
	
?>

<script type="text/javascript">reloadGarageInfo();</script>

</body>
</html>