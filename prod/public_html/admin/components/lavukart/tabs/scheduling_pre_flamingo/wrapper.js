	var data_url= "./get_data.php";
	var screen_size= 800;
	var month=new Array();
		month[0]="Jan";
		month[1]="Feb";
		month[2]="Mar";
		month[3]="Apr";
		month[4]="May";
		month[5]="Jun";
		month[6]="Jul";
		month[7]="Aug";
		month[8]="Sep";
		month[9]="Oct";
		month[10]="Nov";
		month[11]="Dec";
	function initialize(){
		create_date();
		create_header();
	}

	function create_header(){
		var loc_data    = performAJAX(data_url, "function=get_tracks_and_rooms",false,"POST");
		var total_count= loc_data.tracks.length +loc_data.rooms.length;
		var elem= document.getElementsByClassName('header_container')[0];
		for(var t_counter=0; t_counter<loc_data.tracks.length; t_counter++){
			var header= document.createElement("span");
			header.className='track_header';
			header.style.width=calculate_width(loc_data,'track');
			header.innerHTML= loc_data.tracks[t_counter].title;
			header.setAttribute("id", "track_"+loc_data.tracks[t_counter].id);	
			elem.appendChild(header);
		}
		for(r_counter=0; r_counter<loc_data.rooms.length; r_counter++){
			var header= document.createElement("span");
			header.className='room_header';
			header.setAttribute("id", "room_"+loc_data.rooms[r_counter].id);
			header.innerHTML= loc_data.rooms[r_counter].title;
			header.style.width=calculate_width(loc_data,'room');
			elem.appendChild(header);
			
		}		
	}
	function calculate_width(loc_data, type){
		var room_width= (screen_size/((1.5*loc_data.tracks.length)+loc_data.rooms.length));
		
		if((loc_data.tracks.length + loc_data.rooms.length) > 1)	
			if( type=='track'){
				return ((screen_size-(room_width*loc_data.rooms.length))/loc_data.tracks.length)+"px";
			}else{
				return room_width+"px";
			}
		else{
			return screen_size+"px";
		}
	}
	function create_date(){
		var d=new Date();

		var n = month[d.getMonth()];
		document.getElementsByClassName("date_container_inner")[0].innerHTML=""+n +" "+d.getDate();
	}
	function open_picker(){

		try{
			event.stopPropagation();
			window.location = '_DO:cmd=send_js&c_name=p2r_scheduling&js=display_calendar()';
			//window.location='_DO:cmd=send_js&c_name=parts_info_wrapper&js=hideAZIcon()';
		}catch(err){
			alert(err);
		}
	}
	function change_date(mon, day){
		document.getElementsByClassName("date_container_inner")[0].innerHTML= month[mon]+" "+day;
	}
	function highlight_track_name(id){
		document.getElementById(id).style.backgroundColor='#dadada';
		document.getElementById(id).style.color='black';
	}
	function unhighlight_track_name(){
		var tracks = document.getElementsByClassName("track_header");
		var rooms  = document.getElementsByClassName("room_header");
		
		for(var i=0; i<tracks.length; i++){
			tracks[i].style.backgroundColor="";
			tracks[i].style.color='white';
		}
		for(var i=0; i<rooms.length; i++){
			rooms[i].style.backgroundColor="";
			rooms[i].style.color='white';
		}
	
	}
	//I like turtles
	function performAJAX(URL, urlString,asyncSetting,type, callback ){
		var returnVal='';
			
		$.ajax({ 
			url:   URL,
			type:  type,
			data:  urlString,
			async: asyncSetting,
			success: 
			function (string){
				//done_loading();
				if(check_json(string)){
	
					var obj= jQuery.parseJSON( string );
					if(obj.error){
						alert(obj.error);
						return;
					}
					if(callback){
						//var fn = window[callback];
					
						callback(obj);
					}else{
						returnVal= obj;
					}
				}else{
					alert(string);
				}
			}
		});	
		return returnVal; 
	}

	function check_json(text){
		
		if (/^[\],:{}\s]*$/.test(text.replace(/\\["\\\/bfnrtu]/g, '@').
		replace(/"[^"\\\n\r]*"|true|false|null|-?\d+(?:\.\d*)?(?:[eE][+\-]?\d+)?/g, ']').
		replace(/(?:^|:|,)(?:\s*\[)+/g, ''))) {
			return true;
		}else{
	  		return false;
	  	}
	}
	function refresh(){ //TODO
		return 1;
	}
	window.onload= initialize; 