<?php

	ini_set("display_errors",1);
	date_default_timezone_set('UTC');
	require_once(dirname(__FILE__)."/../../../comconnect.php");
	require_once("/poslavu-local/cp/resources/json.php");
		
	$loc_id= 1;	 	
	define("CHECK_INTERVAL", 1000000);
	
	if(isset($_POST['function'])){
		$fun = $_POST['function'];
		session_destroy();
		unset($_POST['function']);
		error_log("function: ". $fun." args: ".print_r($_POST,1));
		
		echo $fun($_POST);
	}else{
		echo '{"error":"No Function."}';
	}

	function get_active_tracks(){
		$query= lavu_query("SELECT * FROM `lk_tracks` WHERE `_deleted`='0'");
		$return_arr= array();
		while($res= mysqli_fetch_assoc($query)){
			$return_arr[]= $res;
		}
		return json_encode($return_arr);
		//return LavuJson::json_encode($return_arr);
	}
	function get_races($args){
		$date= $args['date'];
		$query = lavu_query("SELECT * FROM `lk_events` WHERE DATE(`date`) = DATE('[1]') AND `_deleted`='0'", $date);
		$races = array();
		while($res= mysqli_fetch_assoc($query)){
		
			$type			  = get_event_type($res['type']);
			$res['num_racers_remaining']= get_num_slots($res['type']) - get_num_racers($res['id']);
			$res['evt_type']     = $type['title'];
			$res['abbreviation'] = $type['abbreviation'];
			$res['color']		 = $type['color'];
			$races[] = $res;
		}
		//return LavuJson::json_encode($races);
		return json_encode($races);	
	}
	function get_rooms($args){
		$date= $args['date'];
		$query = lavu_query("SELECT * FROM `lk_group_events` WHERE DATE(`event_date`) = DATE('[1]') AND `room_id` !='' and `_deleted`='0'", $date);
		$rooms = array();
		while($res= mysqli_fetch_assoc($query)){
		    if($res['facility_buyout']==1){
		    	error_log('facility buyout');	
				$r_query = lavu_query("SELECT * FROM `lk_rooms` where `_deleted`!='1' ");
				while($each_room = mysqli_fetch_assoc($r_query)){
			 	   $room_data=get_room_name($each_room['id']); 
				   $res['room_id']=$each_room['id'];
				   $res['room_title']=$room_data['title'];    
				   $res['room_abbr']= $room_data['abbreviation'];
				   $res['notes']= rawurlencode($res['notes']);
				   $rooms[] = $res;
				}
		    }else{
				if($res['room_id']!=0){
					$room_data=get_room_name($res['room_id']); 
					$res['room_title']=$room_data['title'];
					$res['room_abbr']= $room_data['abbreviation'];
					$res['notes']= rawurlencode($res['notes']);
					$rooms[] = $res;
				}
		    }
		}
		return json_encode($rooms);		
	}
	function get_time_interval(){
		global $loc_id;
		$res = mysqli_fetch_assoc(lavu_query("SELECT * FROM `lk_tracks` WHERE `locationid`='[1]' AND `_deleted`!='1' ", $loc_id));
		//echo "your mom sucks";
		return '{"time_interval":"'.intval($res['time_interval']).'"}';
	}
	function get_num_slots($id){
		$res=  mysqli_fetch_assoc(lavu_query("SELECT * FROM `lk_event_types` WHERE `id`='[1]' AND `_deleted`='0'", $id));
		return $res['slots'];
	}
	function get_num_racers($id){
		return mysqli_num_rows(lavu_query("SELECT * FROM `lk_event_schedules` WHERE `eventid`='[1]' AND `_deleted`='0'", $id));
	}
	function get_event_type($id){
		$res=  mysqli_fetch_assoc(lavu_query("SELECT `abbreviation`,`title`,`color` FROM `lk_event_types` WHERE `id`='[1]' ", $id));
		return $res;
	}
	function get_tracks_and_rooms(){
		global $loc_id;
		$track_query = lavu_query("SELECT * FROM `lk_tracks` WHERE `_deleted`='0' AND `locationid`='[1]'",$loc_id);
		$room_query  = lavu_query("SELECT * FROM `lk_rooms` WHERE `_deleted`='0' AND `locationid`='[1]'",$loc_id);
		$tracks 	 = array();
		$rooms  	 = array();
		while($track_res = mysqli_fetch_assoc($track_query)){
			$tracks[]= $track_res;
		}
		while($room_res= mysqli_fetch_assoc($room_query)){
			$rooms[]= $room_res;
		}
		$return_arr= array("tracks"=>$tracks, "rooms"=>$rooms);
		//return LavuJson::json_encode($return_arr);
		return json_encode($return_arr);
	}
	function get_room_name($room_id){
		return mysqli_fetch_assoc(lavu_query("SELECT `title`, `abbreviation`,`color` FROM `lk_rooms` WHERE `id`= '[1]'", $room_id));
	}
	function get_event_types(){
		global $loc_id;
		//echo $loc_id;
		
		$event_query    = lavu_query("SELECT * FROM `lk_event_types` WHERE `_deleted`='0' and `locationid`='[1]'",$loc_id);
		$sequence_query = lavu_query("SELECT * FROM `lk_event_sequences` WHERE `_deleted`='0' and `locationid`='[1]'",$loc_id);
		
		$types_arr    = array();
		$sequence_arr = array();
		
		while($event_res = mysqli_fetch_assoc($event_query)){
			$types_arr[] = $event_res;
		}
		while($sequence_res = mysqli_fetch_assoc($sequence_query)){
			$sequence_arr[] = $sequence_res;
		}
		$return_arr=array("normal_races"=>$types_arr,"sequence_races"=>$sequence_arr);
		//error_log("The return array is :".print_r($return_arr,1));
		//echo LavuJson::json_encode($return_arr);
		//echo json_encode($return_arr);
		return json_encode($return_arr);
		//return LavuJson::json_encode($return_arr);
	}
	function get_racers_search($args){
		global $loc_id;
		$term	= $args['search_term'];
		$arr	= explode(" ", $args['search_term']);
		$f_name = $arr[0];
		$l_name = $f_name;
		if(isset($arr[1])){
			$l_name = $arr[1];
		}
		
		if($args['search_term'])
			
		$query= lavu_query("SELECT * FROM `lk_customers` WHERE (`f_name` LIKE '%[1]%' OR `l_name` LIKE '%[2]%' OR `racer_name` LIKE '%[3]%') AND `locationid` ='[4]' AND `_deleted` = '0' AND `credits` > '0'  limit 40", $f_name,$l_name,$term, $loc_id );
		$return_arr= array();
		while($res= mysqli_fetch_assoc($query)){
			
			$return_arr[]= $res;
		}
		if( lavu_dberror())
		    error_log(lavu_dberror());
		error_log(print_r($return_arr,1));
		return json_encode($return_arr);
		//return LavuJson::json_encode($return_arr);
	}
	function create_race($args){
		global $loc_id;
		//global $data_name;
		$args['loc_id']= $loc_id;
		//$args['date']= date('Y-m-d');
		$args['time_stamp']=gmmktime();
		if(!mysqli_num_rows(lavu_query("SELECT * FROM `lk_events` WHERE `date`='[1]' and `time`='[2]' and `trackid`='[3]' and `_deleted` != '1' ",$args['date'], $args['race_time'], $args['track_id']))){
			$query = lavu_query("INSERT INTO `lk_events` (`type`,`date`,`time`,`trackid`,`locationid`,`ts`) VALUES ('[race_type_id]','[date]','[race_time]','[track_id]','[loc_id]','[time_stamp]')", $args);
			if(lavu_dberror())
				return '{"error":"'.lavu_dberror().'"}';
			else
				return '{"id":"'.lavu_insert_id().'"}';
		}else{
			return '{"error":"A Race already exists at this time."}';	
		}
	}
	function create_sequence_race($args){
		global $loc_id;
		$counter        = 1;
		$args['loc_id'] = $loc_id;
		
		$sequence_info=mysqli_fetch_assoc(lavu_query("SELECT * FROM `lk_event_sequences` where `id`='[1]' ", $args['sequence_type_id']));
		$args['sequence']= $sequence_info['title'];
		$races= explode(",",$sequence_info['event_list']);
	
		$group_res= mysqli_fetch_assoc(lavu_query("SELECT MAX(`groupid`*1) as group_id FROM `lk_events`"));
		$args['group_id']= ($group_res['group_id']*1)+1;	//This is the new group id
	
		foreach($races as $race_id){

			$args['ts']      = time();
			$args['heat']    = $counter;
			$args['race_id'] = $race_id;
			
			$num_laps_arr = mysqli_fetch_assoc(lavu_query("SELECT * FROM `lk_event_types` WHERE `id`='[1]' ", $race_id));
			$num_inserts  = round(($args['num_racers'] / $num_laps_arr['slots']),0);
			if( $num_inserts < 1)
				$num_inserts = 1;
				
			for($i = 0; $i < $num_inserts; $i++){
				
				lavu_query("INSERT INTO `lk_events` (`locationid`, `type`, `date`, `time`, `ts`, `trackid`, `_deleted`, `groupid`, `group_racer_count`,`heat`,`sequence`, `sequenceid`) VALUES ('[loc_id]', '[race_id]', '[date]', '[time]', '[ts]', '[track_id]',0, '[group_id]', '[num_racers]', '[heat]', '[sequence]', '[sequence_type_id]')", $args);
				
				$args['time']= increment_race_time($args['date'], $args['time'],$args['track_id'], $args['interval']);
			}
			$counter++;
			
		}

		if(lavu_dberror())
			return '{"error":"'.lavu_dberror().'"}';
		else
			return '{"id":"'.lavu_insert_id().'"}';
		
	}
	function increment_race_time($date, $time, $track_id, $interval){
		$new_time  = date("Hi",strtotime("+".$interval." minutes", mktime(substr($time,0, -2), substr($time,2))));
		$race_exists=mysqli_num_rows(lavu_query("SELECT * FROM `lk_events` where `date`='[1]' AND `time`='[2]' and `trackid`='[3]'",$date, $new_time, $track_id));		
		if($race_exists){
			return increment_race_time($date, $new_time, $track_id, $interval);
		}else
			return $new_time;
	}
	function get_racers_for_race($args){
		$event_id   = $args['event_id'];	
		$race_data  = mysqli_fetch_assoc(lavu_query("SELECT * FROM `lk_events` WHERE `id`='[1]'", $event_id));
		$event_data = mysqli_fetch_assoc(lavu_query("SELECT * FROM `lk_event_types` WHERE `id`='[1]'", $race_data['type'])); 
		
		
		$query=lavu_query("SELECT * FROM `lk_event_schedules` WHERE `eventid`='[1]' AND `_deleted`='0' ", $event_id);
		
		$return_arr = array("race_data"=>$event_data);
		$temp_arr   = array();
		if( mysqli_num_rows($query)){
			while($result=mysqli_fetch_assoc($query)){
				$temp_arr[] = mysqli_fetch_assoc(lavu_query("SELECT * FROM `lk_customers` WHERE `id`='[1]'",$result['customerid']));
			}
			$return_arr['racer_data']= $temp_arr;
		}else{
			$added_credits_custs = array();
			$cust_id_arr 		 = array();
			$added_credits_query =lavu_query("SELECT `user_id`, `customer_id` AS 'id' FROM `lk_action_log` WHERE `time` > DATE_SUB(NOW(), INTERVAL 1 DAY) AND `action` LIKE \"%Race Credits%\"");
			while($added_credits_res= mysqli_fetch_assoc($added_credits_query)){
				if(!isset($added_credits_custs[$added_credits_res['id']])){
					$cust_id_arr[] = "'".$added_credits_res['id']."'";
					$added_credits_custs[$added_credits_res['id']]= $added_credits_res;
				}
			}
			if(count($cust_id_arr)){
				$temp_arr=array();
				$racer_info_query = lavu_query("SELECT `id`, `racer_name`,`birth_date`, `credits` FROM `lk_customers` WHERE `id` IN (".implode(',',$cust_id_arr) .") and `credits` > 0 ");
				while($racer_info = mysqli_fetch_assoc($racer_info_query)){
					
					$added_credits_custs[$racer_info['id']]['racer_name'] = $racer_info['racer_name'];
					$added_credits_custs[$racer_info['id']]['birth_date'] = $racer_info['birth_date'];
					$added_credits_custs[$racer_info['id']]['credits']    = $racer_info['credits'];
					
					$username= mysqli_fetch_assoc(lavu_query("SELECT * FROM `users` WHERE `id`='[1]'",$added_credits_custs[$racer_info['id']]['user_id']));
					$added_credits_custs[$racer_info['id']]['user_id']= $username['username'];
			
					$temp_arr[]= $added_credits_custs[$racer_info['id']];
				
				}
			
				$return_arr['racer_data_recent']= $temp_arr;
			}else{
				$return_arr['racer_data_recent']=array();
			}
		/*
				//$query= lavu_query("SELECT `racer_name`, SUM(`lk_customers`.`credits`)AS 'credits' ,`lk_action_log`.`user_id` as 'user_id', `lk_customers`.`id` as 'id'  FROM `lk_customers` LEFT JOIN `lk_action_log` ON `lk_customers`.`id` = `lk_action_log`.`customer_id` WHERE UNIX_TIMESTAMP(`last_activity`) > UNIX_TIMESTAMP(DATE_SUB(NOW(), INTERVAL 1 DAY)) AND `lk_action_log`.`order_id` LIKE \"%-%\" GROUP BY `racer_name` asc ");
				$day_custs_query= lavu_query("SELECT `racer_name`, SUM(`lk_customers`.`credits`) AS 'credits', `lk_customers`.`id` as 'id' FROM `lk_customers` WHERE `lk_customers`.`last_activity` > DATE_SUB(NOW(), INTERVAL 1 DAY) AND `credits` > 0 GROUP BY `racer_name` ");
				$day_custs     = array();
				$cust_info_arr = array();
				while($day_custs_res= mysqli_fetch_assoc($day_custs_query)){
					$day_custs[]= "'".$day_custs_res['id']."'";
					$cust_info_arr[$day_custs_res['id']] = $day_cust_res;
				}
				if(count($day_custs)){
					$query_str=  "SELECT `customer_id`,`user_id` FROM `lk_action_log` WHERE `order_id` NOT IN ('', '0') AND `customer_id` IN (".implode(',',$day_custs) .")";
					$query = lavu_query("SELECT `customer_id`,`user_id` FROM `lk_action_log` WHERE `order_id` NOT IN ('', '0') AND `customer_id` IN (".implode(',',$day_custs) .")");
					while($result=mysqli_fetch_assoc($query)){
						$cust_info_arr[$result['customer_id']]['user_id'] = $result['user_id'];
						$temp_arr[]= $cust_info_arr[$result['customer_id']];
	
						//$username= mysqli_fetch_assoc(lavu_query("SELECT * FROM `users` WHERE `id`='[1]'",$result['user_id']));
						//$result['user_id']= $username['username'];
						//$temp_arr[] = $result;

					}
	
					$return_arr['racer_data_recent']= $temp_arr;
				}
				else{
					$return_arr['racer_data_recent']=array();
				}*/

		}
		return json_encode($return_arr);
		//return LavuJson::json_encode($return_arr);
	}
	function add_racer($args){
		
		
		$event_type=mysqli_fetch_assoc(lavu_query("SELECT * FROM `lk_events` where `id`='[1]' ", $args['event_id']));
		$event_laps= mysqli_fetch_assoc(lavu_query("SELECT * FROM `lk_event_types` WHERE `id`='[1]'", $event_type['type']));
		if(mysqli_num_rows(lavu_query("SELECT * FROM `lk_event_schedules` where `eventid`='[1]'", $args['event_id'])) < $event_laps['slots'])
			lavu_query("INSERT INTO `lk_event_schedules` (`eventid`,`customerid`,`group`,`time_stamp`)VALUES ('[1]','[2]','1','[3]')", $args['event_id'], $args['racer_id'], mktime());
		else
			return '{"result": "full_race"}';
		
		if (lavu_dberror()){
			return '{"error":"'.lavu_dberror().'"}';
		}else
			return '{"result":"success"}';
	}
		function remove_racer($args){
		lavu_query("UPDATE `lk_event_schedules`  SET `_deleted` ='1', `time_stamp`='[3]' WHERE `eventid`='[1]' AND `customerid`='[2]'  ", $args['event_id'], $args['racer_id'], mktime());
		if (lavu_dberror()){
			return '{"error":"'.lavu_dberror().'"}';
		}else
			return '{"status":"success"}';

	}
	function move_race($args){
	    lavu_query("UPDATE `lk_events` set `time`='[1]', `ts`='[4]', `trackid`='[3]' WHERE `id`='[2]'", $args['time'], $args['event_id'], $args['track_id'], time());
		if (lavu_dberror()){
			return '{"error":"'.lavu_dberror().'"}';
		}else
			return '{"status":"success"}';

    }
    function get_month_races($args){
		$return_arr = array();
		$year  		= $args['year'];
		$month		= ($args['month']*1)+1; 
		
		if($month==12){
			$next_month=1;
			
		}else{
			$next_month=$month+1;
		}
		$date  	   = date("Y-m-d",strtotime("$year-$month-1")); 
		$next_date = date("Y-m-d",strtotime(($year+1)."-$next_month-1")); 
		//echo "date: ". $date. " next date: ". $next_date;
		$query= lavu_query("select * from `lk_group_events` where `event_date` >= date('[1]') and `event_date` < date('[2]')", $date, $next_date);
		while($res= mysqli_fetch_assoc($query)){
			$firstDay = "_".date('j',strtotime($res['event_date']));
			if(!isset($return_arr[$firstDay]))
				$return_arr[$firstDay]=array();
			array_push($return_arr[$firstDay], $res['event_time']);
		}
		return json_encode($return_arr);
		//return  LavuJson::json_encode($return_arr);
    }
    function create_room_event($args){
	    global $loc_id;
	    $args['loc_id']= $loc_id;
	    if(!isset($args['notes']))
	    	$args['notes']='';
	    $args['notes']= rawurldecode($args['notes']);
	    $args['time_stamp']= time();
		lavu_query("INSERT INTO `lk_group_events` (`item_id`, `title`,`event_date`,`event_time`,`duration`,`cust_name`,`cust_phone`,`cust_email`,`notes`,`loc_id`,`room_id`, `time_stamp`)VALUES 
												  ('[item_id]', '[title]','[event_date]','[event_time]','[duration]','[cust_name]','[cust_phone]','[cust_email]','[notes]','[loc_id]','[room_id]', '[time_stamp]')", $args);
		if(lavu_dberror())
			return '{"error":"'.lavu_dberror().'"}';
		else
			return '{"id":"'.lavu_insert_id().'"}';    
	}
    function remove_event($args){
	    $id   = $args['event_id'];
	    $type = $args['type'];
	    if($type=="room")
	    	lavu_query("UPDATE `lk_group_events` SET `_deleted`='1', `time_stamp`='[2]' WHERE `id`='[1]' LIMIT 1", $id, time());
	    else if($type=='track')
	    	lavu_query("UPDATE `lk_events` SET `_deleted`='1', `ts`='[2]' WHERE `id`='[1]' LIMIT 1", $id, time());
	    if(lavu_dberror())
			return '{"error":"'.lavu_dberror().'"}';
		else
			return '{"result":"success"}';
	    
    }
    function get_room_details($args){
    	$res=mysqli_fetch_assoc(lavu_query("SELECT * FROM `lk_group_events` where `id`='[1]'",  $args['event_id']));
		$res['notes'] = rawurlencode($res['notes']);
		//echo $res['notes'];
	    return json_encode($res);
	    //return LavuJson::json_encode($res);
    }
    function update_room_event($args){
    	$args['ts']=time();
    	//echo print_r($args,1);
    	$args['notes']= rawurldecode($args['notes']);
	    lavu_query("UPDATE `lk_group_events` set `time_stamp`='[ts]', `title`='[title]', `duration`='[duration]', `event_time`='[event_time]',`cust_name`='[cust_name]', `cust_email`='[cust_email]', `cust_phone`='[cust_phone]', `cust_email`='[cust_email]', `notes`='[notes]' WHERE `id`='[event_id]' LIMIT 1", $args);
	     if(lavu_dberror())
			return '{"error":"'.lavu_dberror().'"}';
		else
			return '{"result":"success"}';
	    
    }
    function save_note($args){
	    lavu_query("UPDATE `lk_events` SET `notes`='[1]', `ts`='[3]' where `id`='[2]'",rawurldecode($args['note']), $args['event_id'], time());
	    if(lavu_dberror())
			return '{"error":"'.lavu_dberror().'"}';
		else
			return '{"result":"success"}';
	    
    }
	/*
	function output_js($data, $var_name){
		echo "<script type='text/javascript'>";
		echo "var ".$var_name." = ".$data.";";
		echo "</script>";
	}*/

	/*
	function initialize_data(){
		$tracks			 = get_active_tracks();		
		$upcoming_races  = get_upcoming_races();
		output_js($tracks, "tracks");
		output_js($upcoming_races,"upcoming_races");		
	}*/
?>