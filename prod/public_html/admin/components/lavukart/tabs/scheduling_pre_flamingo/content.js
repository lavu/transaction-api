	var data_url		= "./get_data.php";
	var screen_size	    = 800;
	var interval    	= parseInt(performAJAX(data_url, "function=get_time_interval",false,"POST").time_interval);
	var _start_touch_id = 0;
	var _start_x_loc    = 0;
	var _start_y_loc    = 0;
	var _time_start		= 0;
	var _is_double_tap  = false;
	var _num_racers 	= "";
	var _csr_list		= new Array();
	var _debugging      = true;
	var current_ajax    = null;
	var day_names= new Array();
		day_names[0] = 'Sunday'; 
		day_names[1] = 'Monday'; 
		day_names[2] = 'Tuesday'; 
		day_names[3] = 'Wednesday'; 
		day_names[4] = 'Thursday'; 
		day_names[5] = 'Friday'; 
		day_names[6] = 'Saturday'; 
	
	var month_names=new Array();
		month_names[0]="January";
		month_names[1]="Febuary";
		month_names[2]="March";
		month_names[3]="April";
		month_names[4]="May";
		month_names[5]="June";
		month_names[6]="July";
		month_names[7]="August";
		month_names[8]="September";
		month_names[9]="October";
		month_names[10]="November";
		month_names[11]="December";
	
	var cal = get_cal();
	function initialize(date){
		//document.write("<div style='white';font-size:100;'>HELLO WORLD I AM FROM THE NEW FILE!!!</div>");
		if(!date){
			var d   = new Date();	
			var m   = (d.getMonth()+1);
			var day = d.getDate();
			 
			if(m < 10)
				m="0"+m;
			if(day < 10)
				day = "0"+day;	
			date= d.getFullYear()+"-"+m+"-"+day;
		
		}
		document.getElementsByClassName('content_container')[0].setAttribute("date",date);
		create_rows();
		get_races(date);
		get_rooms(date);
		initialize_listeners();
		update_races_completed();
		find_new_races(1);
		
	}
	function create_rows(){

		var loc_data    = performAJAX(data_url, "function=get_tracks_and_rooms",false,"POST");
		var container   = document.getElementsByClassName('content_container')[0];
			container.innerHTML='';
			 
		for(var hours = 0; hours < 18; hours++){
			var h = ((7+hours)%12)+1;
			for(minutes = 0; minutes < 60; minutes+=interval){
				var m=minutes;
				var am_pm= 'am';
				
				if( minutes < 10)
					m= "0"+minutes;
				if( hours >= 4 && hours < 16)
					 am_pm='pm';
				
				var row_elem  = document.createElement("div");
				row_elem.className='table_row';
				
				
				var time_span = document.createElement("div");
				
				var temp_hr= h;		// this will allow me to reference the Row by the time
				if( am_pm == 'pm' && temp_hr!=12)
					temp_hr+=12;	
				
				if(hours >= 16)
					temp_hr=hours-16;
					
				if(h==12 && am_pm=='am')
					temp_hr=0;
				
				if(parseInt(temp_hr) < 10)
					temp_hr="0"+temp_hr;
				
				var time_stamp= temp_hr+""+m;
				//if(time_stamp=='2350')
				//  console.log('your mom');
				
				time_span.innerHTML= "<div class='time_positioner'>"+h + ":" + m + " "+ am_pm+"</div>";

				row_elem.id= time_stamp;

				time_span.className='time_class';				
				row_elem.appendChild(time_span);
				
				for(var t_counter=0; t_counter<loc_data.tracks.length; t_counter++){
					var e= create_box("track",loc_data.tracks[t_counter].id);
					e.style.width=calculate_width(loc_data, 'track');
					row_elem.appendChild(e);
				}
				for(r_counter=0; r_counter<loc_data.rooms.length; r_counter++){
					var e= create_box("room",loc_data.rooms[r_counter].id);
					var w= calculate_width(loc_data,'room');
					e.style.width=w;
					row_elem.appendChild(e);
				}

				container.appendChild(row_elem);
			}		
		}	
	}
	function get_races(date){
		
		var races_ajax= performAJAX(data_url,"function=get_races&date="+date,false,"POST");
		//console.log(races_ajax);
		for( var i=0; i< races_ajax.length; i++){
			if( document.getElementById(races_ajax[i].time)){
				draw_track(races_ajax[i].time, races_ajax[i].trackid, races_ajax[i].id, races_ajax[i]);
			}
		}
	}
	function get_rooms(date){
		
		var rooms_ajax= performAJAX(data_url,"function=get_rooms&date="+date,false,"POST");
		
		for( var i=0; i< rooms_ajax.length; i++){
			var duration = rooms_ajax[i].duration;
			
			if(rooms_ajax[i].event_time.length==3)
				rooms_ajax[i].event_time= "0"+rooms_ajax[i].event_time;
			
			var index= get_column_offset(rooms_ajax[i].event_time, "room_id", rooms_ajax[i].room_id);

			var current_time = rooms_ajax[i].event_time;
			var first = true;
			var ele   = null;
			
			
			
			if(duration == interval){
				var inner_elem= document.createElement("div");
					inner_elem.className='room_event_start_end_inner';
					inner_elem.innerHTML=rooms_ajax[i].title;
					
					//inner_elem.addEventListener("touchstart", touch_start);
					//inner_elem.addEventListener("touchend",   touch_end);
					
				ele= document.getElementById(current_time).childNodes[index];
				ele.className += " room_event_start_end room_event_"+rooms_ajax[i].id;
				ele.removeChild(ele.childNodes[0]);
				ele.appendChild(inner_elem);
				continue;

			}
			while(duration > 0){
				var inner_elem= document.createElement("div");
				ele= document.getElementById(current_time).children[index];
				ele.className+=" room_event_"+rooms_ajax[i].id;
				if(first){
					
					inner_elem.className = 'room_event_start_inner';
					inner_elem.innerHTML = rooms_ajax[i].title;
					ele.className		+= " room_event_start"; 
					first=false;
				}else{
					inner_elem.className='room_event_mid_inner';
					ele.className+=" room_event_mid";	
					inner_elem.innerHTML='.';
					
				}
				//inner_elem.addEventListener("touchstart", touch_start);
				//inner_elem.addEventListener("touchend",   touch_end);
				
				ele.removeChild(ele.childNodes[0]);
				ele.appendChild(inner_elem);
				
				current_time=document.getElementById(current_time).nextSibling.id;
				duration=parseInt(duration)-parseInt(interval); 
				
			}
			ele.className="room_cell room_event_end room_event_"+rooms_ajax[i].id;
			inner_elem.className='room_event_end_inner';
			inner_elem.innerHTML='.';
			//inner_elem.addEventListener("touchstart", touch_start);
			//inner_elem.addEventListener("touchend",   touch_end);
			
			ele.removeChild(ele.childNodes[0]);
			ele.appendChild(inner_elem);
		}
	}
	function draw_track(time,trackid, eventid, data){
		
		if(!document.getElementById(time))
			console.log(time);
		
		for( var j=0; j<document.getElementById(time).childNodes.length; j++){
			var elem= document.getElementById(time).childNodes[j];
			if(elem.hasAttribute("track_id") && elem.getAttribute("track_id")==trackid){
				elem.className='track_cell_with_contents';
				/*
				elem.style.color='#a41625';
				elem.style.backgroundColor='#ffffff';
				*/
				elem.setAttribute("event_id",eventid);
				if(data.groupid){
					create_sequence_cell(elem, data);
				}else{
					create_normal_cell(elem, data);
				}
			} 
		}	
	}
	function create_sequence_cell(elem, ajax){
		var vertical_line= "<div class='vert_line' style='height: 40px;width: 5px; margin-left:5px; color: "+default_color(ajax.groupid+ajax.group_racer_count+ajax.trackid)+";background-color:"+default_color(ajax.groupid+ajax.group_racer_count+ajax.trackid)+";display: inline-block;'>.</div>"
		if( ajax.abbreviation)
			elem.innerHTML= vertical_line+ajax.abbreviation;
		else{	
			if(_debugging)
				elem.innerHTML= vertical_line+"<div class='race_type' onclick ='start_move(this)' ontouchend ='start_move(this)'>"+make_abbreviation(ajax.evt_type)+"</div>";
			else
				elem.innerHTML= vertical_line+"<div class='race_type' ontouchend ='start_move(this)'>"+make_abbreviation(ajax.evt_type)+"</div>";
		}if( ajax.color)
			elem.style.color=ajax.color;
		var img='';
		if(ajax.ms_end.length == 0){
			img= "<div class='num_racers_active'><p class='number_positioner'>"+ajax.num_racers_remaining+"</p></div>";		
		}else{
			img= "<div class='num_racers_inactive'><p class='number_positioner'>"+ajax.num_racers_remaining+"</p></div>";
		}
		var notes_area='';
		if(ajax.notes.length!=0){
			ajax.notes= decodeURIComponent(ajax.notes);
			if( ajax.notes.length > 50)
				ajax.notes= ajax.notes.substr(0, 47)+"...";
			notes_area = '<div class="note_container" style=" width:'+ (parseInt(elem.style.width)-100)+'"><input type="text" class="note_input" onfocus="open_note_editor(this)" onblur="close_note_editor(this)"/><div class="note_container_inner">'+ajax.notes+'</div></div>';
		}else{
			notes_area = '<div class="note_container" style=" width:'+ (parseInt(elem.style.width)-100)+'"><input type="text" class="note_input" onfocus="open_note_editor(this)" onblur="close_note_editor(this)"/></div>';
		}
		elem.innerHTML+=img+notes_area;

	}
	function create_normal_cell(elem, ajax){
		if(ajax.color){
			if(_debugging)
				var box= "<div class='containing_cell' onclick ='start_move(this)' ontouchend ='start_move(this)' style='background-color:"+ajax.color+";'><div class='race_type'>";
			else
				var box= "<div class='containing_cell' ontouchend ='start_move(this)' style='background-color:"+ajax.color+";'><div class='race_type'>";
			
		}else{
			if(_debugging)
				var box= "<div class='containing_cell' onclick = 'start_move(this)' ontouchend = 'start_move(this)'style='background-color:"+default_color(ajax.type)+";'><div class='race_type'>";
			else
				var box= "<div class='containing_cell' ontouchend = 'start_move(this)'style='background-color:"+default_color(ajax.type)+";'><div class='race_type'>";
		}
		if( ajax.abbreviation)
			elem.innerHTML = box+ajax.abbreviation+"</div></div>";
		else{

			elem.innerHTML = box+make_abbreviation(ajax.evt_type)+"</div></div>";
		
		}if( ajax.color)
			elem.style.color=ajax.color;
		var img='';
		if(ajax.ms_end.length == 0){
			img= "<div class='num_racers_active'><p class='number_positioner'>"+ajax.num_racers_remaining+"</p></div>";		
		}else{
			img= "<div class='num_racers_inactive'><p class='number_positioner'>"+ajax.num_racers_remaining+"</p></div>";
		}
		var notes_area='';
		if(ajax.notes.length!=0){
			ajax.notes= decodeURIComponent(ajax.notes);
			if( ajax.notes.length > 50)
				ajax.notes= ajax.notes.substr(0, 47)+"...";
			notes_area = '<div class="note_container" style=" width:'+ (parseInt(elem.style.width)-100)+'"><input type="text" class="note_input" onfocus="open_note_editor(this)"onblur="close_note_editor(this)" /><div class="note_container_inner">'+ajax.notes+'</div></div>';
		}else{
			notes_area = '<div class="note_container" style=" width:'+ (parseInt(elem.style.width)-100)+'"><input type="text" class="note_input" onfocus="open_note_editor(this)" onblur="close_note_editor(this)"/></div>';
		}
		elem.innerHTML+=img+notes_area;
	
	}
	function make_abbreviation(type){
	
		var clean= clean_type(type);
		var	num_words= clean.length;
		if(num_words==1)
			return type.substring(0, 2);
		else{
			var abbr='';
			for( var i=0; i<clean.length; i++)
				abbr+=clean[i].substring(0,1);
			return abbr;
		}
	}
	function default_color(id) {
		if( id){
			var r = (id*73 + 0)  % 256;
			var g = (id*100 + 0) % 256;
			var b = (id*53 + 200)% 256;
		} 
		return "rgb("+r+","+g+","+b+")";
	}
	function clean_type(type){
		if(type){
			s = type;
			s = s.replace(/(^\s*)|(\s*$)/gi,"");
			s = s.replace(/[ ]{2,}/gi," ");
			s = s.replace(/\n /,"\n");
			return s.split(' ');
		}else{
			return ["no","name"];
		}
	}	
	function display_calendar(year, month){
		if(year && month)
			draw_calendar(year, month);
		else{
			var date= new Date();
			draw_calendar(date.getFullYear(),date.getMonth())
		}
	}
	function draw_calendar(year, month){
				
		var elem  	     = document.getElementById("overlay");
		var month_str    = (parseInt(month)+1)+ "";
		var month_races  = get_month_races(year, month);
		var selected_day = get_selected_day();
		if(month_str.length < 2 ){
			month_str = "0" + month_str;
		}
		var start_day = new Date(year + "-" + month_str + "-01").getDay()+1;
		var table 	  = document.createElement("table");
		var d		  = new Date();
		var today	  = d.getDate();
	
		var month_name_row="<thead><tr><td colspan= '7' class='month_name'>"+month_names[month]+" "+year+"</td></tr></thead>";
		table.innerHTML=month_name_row;
	
		table.className='cal_table';
		//create header
		var head_row 	  = document.createElement("tr");
		var close    	  = document.getElementById("close_overlay");
		overlay.innerHTML ='';
		overlay.appendChild(close)
		for(var i = 0; i < 7; i++){
			var th= document.createElement("th");
			th.className='cal_header';
			th.appendChild(document.createTextNode(day_names[i]));
			head_row.appendChild(th);
		}
			table.appendChild(head_row);
		
		//create empty days for starting
		var tr = document.createElement("tr");
		for( var i = 0; i < start_day; i++){
			var td = document.createElement("td");
			td. className='cal_cell';
			tr.appendChild(td);
		}
		
		//create days
		for(var d = 1; d <= cal[year][month]; d++){
			if((start_day % 7) == 0){
				table.appendChild(tr);
				tr= document.createElement("tr");
			}
			start_day++;
			var td= document.createElement("td")
			td.className='cal_cell';
		
			td.setAttribute("day", 	 d    );
			td.setAttribute("year",  year );
			td.setAttribute("month", month);
			if(_debugging)
				td.addEventListener("click", change_day);
			td.addEventListener("touchend", change_day);
			td.style.fontWeight= 'bold';
			if(d == today && new Date().getMonth() == month && new Date().getFullYear() == year)
				td.className+=" current_date";
			if(d == (new Date(get_selected_day()).getDate()+1) && new Date().getMonth() == month && new Date().getFullYear() == year)
				td.className+=" selected_date";
			
			td.appendChild(document.createTextNode(d+''));
			
			if(month_races["_"+d]){
				
				var info = month_races["_"+d].length;
				
				var inn  = document.createElement("div");
					inn.style.color = "red"; 
					inn.style.textAlign= "center";
					inn.style.marginTop="-10px";
					inn.style.fontWeight="normal";
					inn.innerHTML   = "<br>"+info; 
				td.appendChild(inn);
				
			} 
			
			tr.appendChild(td);
		} 
		table.appendChild(tr);
		//previous month btn.
		var previous_month_btn = document.createElement("span");
		var previous_year      = parseInt(year);
		var previous_month     = 0;
		if(month==0){
			previous_year= parseInt(year)-1;
			previous_month=11;
		}else{
			previous_month= parseInt(month)-1;
		}
		if(previous_year > (new Date().getFullYear()-2)){ 
			previous_month_btn.setAttribute("year", 	 previous_year );
			previous_month_btn.setAttribute("month",	 previous_month);
			//previous_month_btn.addEventListener("click",   go_to_month );
			previous_month_btn.addEventListener("touchend",go_to_month );
			previous_month_btn.innerHTML='<';
			previous_month_btn.className='previous_month_btn';
			previous_month_btn.style.float='left';
			elem.appendChild(previous_month_btn);
		}
		//next_month_btn
	
	
		var  next_month_btn	= document.createElement("span");
		var next_year		= parseInt(year);
		var next_month		= 0;
		if(month == 11){		
			next_year= parseInt(year) + 1;
			next_month=0;
		
		}else{
			next_month= parseInt(month)+1;
		}
		
		if(next_year < (new Date().getFullYear()+2)){ 
			next_month_btn.setAttribute("year", next_year	    );
			next_month_btn.setAttribute("month",next_month	    );
			
			//next_month_btn.addEventListener("click", go_to_month);
			next_month_btn.addEventListener("touchend", go_to_month);
			next_month_btn.innerHTML='>';	
			next_month_btn.className='next_month_btn';
			next_month_btn.style.float='right';
			elem.appendChild(next_month_btn);
		}
		elem.appendChild(table);
		
		elem.style.display='block';	
		
		//elem.appendChild(close);
	}
	function go_to_month(event){
		var year= event.target.getAttribute("year");
		var month= event.target.getAttribute("month");
		
		draw_calendar(year, month);
	}	
	function get_selected_day(){
		return document.getElementsByClassName('content_container')[0].getAttribute("date");
	}
	function change_day(event){
	
		var tar= event.target;
			tar.className= 'cal_cell_selected';
		if(!event.target.getAttribute("month"))
			tar= event.target.parentNode;	
		
		
		var y    = tar.getAttribute("year" );
		var m    = parseInt(tar.getAttribute("month"))+1;
		var d    = tar.getAttribute("day"  );
		
		if(m < 10)
			m="0"+m;
		if(d < 10)
			d="0"+d;
		
		window.location = '_DO:cmd=send_js&c_name=p2r_scheduling_wrapper&js=change_date("'+tar.getAttribute("month")+'", "'+tar.getAttribute("day")+'")';
		
		initialize(y+"-"+m+"-"+d);
		close_overlay();
	
	}
	function get_cal(){
		var return_arr= [];
		var d= new Date();
		for(var y=d.getFullYear()-2; y < d.getFullYear()+2; y++){
			return_arr[y]=[];
			for( var m=0; m < 12; m++){
				return_arr[y][m]= daysInMonth(m+1,y);				
			}
		}
		return return_arr;
	}
	function get_month_races(year,month){
		return performAJAX(data_url,"function=get_month_races&year="+year+"&month="+month,false,"POST");
	}
	function daysInMonth(month,year) {
    	return new Date(year, month, 0).getDate();
	}
	function calculate_width(loc_data, type){
		var room_width= (screen_size/((1.5*loc_data.tracks.length)+loc_data.rooms.length));
		
		if((loc_data.tracks.length + loc_data.rooms.length) > 1)	
			if( type=='track'){
				return ((screen_size-(room_width*loc_data.rooms.length))/loc_data.tracks.length)+"px";
			}else{
				return room_width+"px";
			}
		else{
			return screen_size+"px";
		}
	}
	function create_date(){
		var d=new Date();
		var m = month[d.getMonth()];
		document.getElementsByClassName("date_container")[0].innerHTML=""+m +" "+d.getDay();
	}
	function create_box(type, id){
		var temp_elem = document.createElement("span");
		temp_elem.innerHTML='_';
		if(type=='track'){
			var elem = document.createElement("div");
			elem.className= "track_cell";
			elem.setAttribute(type+"_id", id)
			elem.innerHTML=".";
			return elem;
		}else if(type=='room'){
			var elem = document.createElement("div");
			elem.className= "room_cell";
			elem.setAttribute(type+"_id", id)
			elem.innerHTML=".";
			//elem.innerHTML= temp_elem.toString();
			return elem;
		}
	}
	function find_new_races(current_id){
		performAJAX("./check_new_events.php","function=check_new_events&first=1",true,"POST",update_grid);
	}
	function update_grid(data){
		var today=new Date();
		var month=today.getMonth()+1; 
		if( month < 10)
			month="0"+month;
		var date= today.getDate();
		if(date < 10)
			date= "0"+date;
		var date_string= get_selected_day();
		if(data){
			if(data.race_update.data){	
				if(Array.isArray(data.race_update.data)){
					for(var j=0; j<data.race_update.data.length; j++){
						var d= data.race_update.data[j];
						if(d.date == date_string){
							var elems= document.querySelectorAll("[event_id]");
							for( var i=0; i<elems.length; i++){
								if( elems[i].getAttribute("event_id")== d.event_id){
									reset_cell(elems[i]);
									break;
								}
							}
							d.evt_type = d.title;
							d.type	   = d.id;
							draw_track(d.time,d.trackid,d.event_id, d);
						}
					}
				}else{
					if( data.race_update.data.date == date_string){
						if( data.race_update.data.deleted){
							var events= document.querySelectorAll("[event_id]");
							for(var i=0; i< events.length; i++){
								if(events[i].getAttribute("event_id")== data.race_update.data.event_id){
									reset_cell(events[i]);
									break;
								}
							}
						}else{	
							var elems= document.querySelectorAll("[event_id]");
							for( var i=0; i<elems.length; i++){
								if( elems[i].getAttribute("event_id")== data.race_update.data.event_id){
									reset_cell(elems[i]);
									break;
								}
							}
							data.race_update.data.evt_type = data.race_update.data.title;
							data.race_update.data.type	   = data.race_update.data.id;
							draw_track(data.race_update.data.time,data.race_update.data.trackid, data.race_update.data.event_id, data.race_update.data);
						}
					}
				}
			}	
			if( data.racers_update.event_id) {
				var elem= document.querySelector('[event_id="'+data.racers_update.event_id+'"]');
	
				if(elem &&  data.racers_update.date == date_string){
					var index=1;
					if(elem.childNodes[0].className=='vert_line'){
						index=2;
					}
					elem.childNodes[index].removeChild(elem.childNodes[index].childNodes[0]);
					
					var new_content= document.createElement("p");
					new_content.innerHTML= data.racers_update.num_racers;
					new_content.className='number_positioner';
					if( data.racers_update.event_id > 0)
						elem.childNodes[index].className="num_racers_active"; 
					elem.childNodes[index].appendChild(new_content);
				}
			}
			if(data.room_update.id){
				if(data.room_update._deleted==1){
					if(document.getElementsByClassName("room_event_"+data.room_update.id).length) 
						do_double_tap(document.getElementsByClassName("room_event_"+data.room_update.id)[0], false); //remove the room 
				}else if(get_selected_day() == data.room_update.event_date){								  //add the room_events
					var par   = document.getElementById(data.room_update.event_time)
					var index = get_column_offset(data.room_update.event_time, "room_id", data.room_update.room_id);
					var d 	  = 0; 
					if( document.getElementsByClassName("room_event_"+data.room_update.id).length){// if we are updateing an existing event
						var e=  document.getElementsByClassName("room_event_"+data.room_update.id)[0];
						while(e){
							reset_cell(e);
							e = document.getElementsByClassName("room_event_"+data.room_update.id)[0];
						}
					}				
					if(data.room_update.duration == interval){
						par.childNodes[index].className='room_cell room_event_start_end room_event_'+data.room_update.id;
						par.childNodes[index].innerHTML="";
						var inner_elem= document.createElement("div");
							inner_elem.className= "room_event_start_end_inner";
							inner_elem.innerHTML= data.room_update.title;
						par.childNodes[index].appendChild(inner_elem);
						
					}else{
						while(d < data.room_update.duration){
							par.childNodes[index].innerHTML="";
							var inner_elem= document.createElement("div");
							if(d==0){
								par.childNodes[index].className='room_cell room_event_start room_event_'+data.room_update.id;
								inner_elem.className = "room_event_start_inner";
								inner_elem.innerHTML = data.room_update.title;
								
							}else if((d+interval)== data.room_update.duration){
								par.childNodes[index].className= "room_cell room_event_end room_event_"+data.room_update.id; 
								inner_elem.className = "room_event_end_inner";
								inner_elem.innerHTML=".";
							}else{
								par.childNodes[index].className= "room_cell room_event_mid room_event_"+data.room_update.id;
								inner_elem.className= "room_event_mid_inner";
								inner_elem.innerHTML=".";	
							}
							
							par.childNodes[index].appendChild(inner_elem);
							par= par.nextSibling;
							d+=interval;
						}
					}
					
				}	
			}
		}else{
			console.log('DATA is null: '+ data);
		}
		performAJAX("./check_new_events.php","function=check_new_events&sched_id="+data.racers_update.sched_id+"&event_stamp="+data.race_update.event_stamp+"&room_stamp="+data.room_update.lastmod,true,"POST",update_grid);
	}
	function reset_cell(cell){
		if(cell.className.indexOf('track_cell') !=-1){
			cell.removeAttribute("event_id");
			cell.innerHTML			   = '.';
			/*
			cell.style.backgroundColor = "rgba(255,255,255,.7)";
			cell.style.color		   = "rgba(255,255,255,.7)";
			*/
			cell.className	       	   = "track_cell";
		}else if(cell.className.indexOf('room_cell' !=-1)){
			cell.className='room_cell';
			cell.innerHTML='.';
		}
	}
	function initialize_listeners(){
		var track_cells= document.getElementsByClassName('track_cell');
		for(var i=0; i< track_cells.length; i++){
			document.getElementsByClassName('track_cell')[i].addEventListener("touchstart", touch_start);
			document.getElementsByClassName('track_cell')[i].addEventListener("touchend",   touch_end  );				
			if(_debugging){
				document.getElementsByClassName('track_cell')[i].addEventListener("mousedown",click_start);
				document.getElementsByClassName('track_cell')[i].addEventListener("mouseup"  ,click_end);
			}
		}
		var room_cells = document.getElementsByClassName('room_cell');
		for(var i=0; i< room_cells.length; i++){
			document.getElementsByClassName('room_cell')[i].addEventListener ("touchstart", touch_start);
			document.getElementsByClassName('room_cell')[i].addEventListener ("touchend",   touch_end  );
			if(_debugging){
				document.getElementsByClassName('room_cell')[i].addEventListener ("mousedown", click_start);
				document.getElementsByClassName('room_cell')[i].addEventListener ("mouseup"  , click_end);
			}
		}
	}
	function touch_start(event){
		_start_x_loc= event.touches[0].pageX;
		_start_y_loc= event.touches[0].pageY;
		var between=(new Date().getTime())-_time_start;
		//console.log("time_between: "+between);
		if(between < 250){
		    //console.log("double tap detected");
			do_double_tap(event.target, true);
			_is_double_tap=true;
		}else{ 
			
			_time_start = new Date().getTime();
			_is_double_tap=false;
		}
	}
	function touch_end(event){
		event.preventDefault();
		setTimeout(function(){
			if( (Math.abs(_start_x_loc-event.changedTouches[0].pageX) < 10 && Math.abs(_start_y_loc-event.changedTouches[0].pageY) < 10 )){	
				if( !_is_double_tap)
					cell_select(event);
			}else{

			}
		}, 250);
		
	}
	function click_start(event){
		_start_touch_id= event.target.parentElement.id;
		var between=(new Date().getTime())-_time_start;
		//console.log("time_between: "+between);
		if(between < 250){
		    //console.log("double tap detected");
			do_double_tap(event.target, true);
			_is_double_tap=true;
		}else{ 
			
			_time_start = new Date().getTime();
			_is_double_tap=false;
		}
	}
	function click_end(event){
		/*
		if(event.target.parentElement.id==_start_touch_id)
			cell_select(event);
		*/
		
		event.preventDefault();
		setTimeout(function(){
			if( event.target.parentElement){
				if( event.target.parentElement.id ==_start_touch_id){	
					if( !_is_double_tap)
						cell_select(event);
				}else{
	
				}
			}
		}, 250);
			
	}
	function cell_select(event){
		
		event.stopPropagation();
		
		var id   = '';
		var type = '';
		if(event.target.getAttribute("class") == 'track_cell'){
			
			if(event.target.getAttribute("event_id") != null){
				id   = event.target.getAttribute("event_id");
				type ='edit_note';	
			}else{
				id = event.target.getAttribute("track_id");
				type='track';	
			}
		}else if(event.target.getAttribute("class").indexOf('room_cell') > -1){
			id   = event.target.getAttribute("room_id");
			type ='room';
		}else if(event.target.className=='num_racers_active' || event.target.className == 'num_racers_inactive'){
			
			id= event.target.parentNode.getAttribute("track_id");
			type='track';	
		}else if( event.target.className == 'number_positioner'){
			id= event.target.parentNode.parentNode.getAttribute("track_id");
			type='track';	
		}else if(event.target.className=='note_container'){
			id   = event.target.parentNode.getAttribute("event_id");
			
			type ='edit_note';		
		}else if(event.target.className=='room_event_mid_inner'|| 
				 event.target.className=='room_event_start_inner' || 
				 event.target.className=='room_event_end_inner'|| 
				 event.target.className=='room_event_start_end_inner'){
			id   = event.target.parentNode.getAttribute("room_id");
			type ='room';
		}else{
			return;
		}
		var time= event.target.parentElement.id;
		if(!time)
			time = event.target.parentNode.parentNode.id;
		if(!time)
			time = event.target.parentNode.parentNode.parentNode.id;
		
		if(document.getElementsByClassName("cell_selected").length){
			var elem=document.getElementsByClassName("cell_selected")[0];
			var event_id= elem.getAttribute('event_id');
			var targ= event.target;
			move_race(time, event_id, id, elem, targ);
		}else if(type=='track'){
			
			var event_id = event.target.getAttribute("event_id");
			if(!event_id)
				event_id = event.target.parentNode.getAttribute("event_id");
		    if(!event_id)
		   		event_id = event.target.parentNode.parentNode.getAttribute("event_id");
		   		 
			show_overlay( id, time, type, event_id);	
		}else if(type=='room'){
			var ele= document.getElementsByClassName("create_room_event_start")[0];
			if(ele && ele.getAttribute("room_id")==id){
				var ta= event.target;
				if(event.target.className.indexOf("room_event")!=-1)
					ta= event.target.parentNode;
				
				end_selection(time, ta, id);
			}else{
				if(event.target.className.indexOf("room_event")!=-1)
					edit_room_event(event.target);
				else{
					if(document.getElementsByClassName("create_room_event_start").length)
						document.getElementsByClassName("create_room_event_start")[0].className="room_cell";
					else
						start_selection(time,event.target, id);
				}
			}
		}else if( type == 'edit_note' ){
			if(event.target.className=='note_container')
				open_note_editor(id, event.target.parentNode);
			else
				open_note_editor(id, event);
		}
		
	}
	function open_note_editor(elem){
		var inputs= document.getElementsByClassName("note_input");
		for(var i=0; i<inputs.length; i++)
			inputs[i].style.opacity=0;
		
		if(elem.nextSibling && elem.nextSibling.className=='note_container_inner')
			elem.value= elem.nextSibling.innerHTML;
		
		elem.addEventListener("keyup", send_note);
		elem.style.opacity='1';
		

	}
	function close_note_editor(elem){
		elem.style.opacity=0;
		var chil= document.createElement("div");
			chil.className='note_container_inner';
			chil.style.zIndex=0;
		
		var val= elem.value;
		if( val.length > 50)
				val= val.substr(0, 47)+"...";	
				
			chil.innerHTML=val;
			
		
		var childrens= elem.parentNode.childNodes;
		for(var i=0; i<childrens.length; i++)
			if(childrens[i].className=='note_container_inner')
				childrens[i].parentNode.removeChild(childrens[i]);
				
		elem.parentNode.appendChild(chil);

	}
	function send_note(event){
		if(event.keyCode==13){
		
			performAJAX(data_url, "function=save_note&note="+event.target.value+"&event_id="+event.target.parentNode.parentNode.getAttribute("event_id"),false,"POST");
			close_note_editor(event.target);
		}
	}
	function do_double_tap(tar, do_confirm){
		if( (tar.innerHTML!='.' || tar.className.indexOf('room_event')!=-1) &&( !do_confirm || confirm("Are you sure you want to delete this event?"))){
			if(tar.className.indexOf("room_event") > -1){
				
				var room_id_num=tar.className.replace(/\D/g,'');
				
				if(!room_id_num)
					room_id_num =tar.parentNode.className.replace(/\D/g,'');
				
				remove_event("room", room_id_num);			
				var ele= document.getElementsByClassName("room_event_"+room_id_num)[0];
				while(ele){
					reset_cell(ele);
					ele = document.getElementsByClassName("room_event_"+room_id_num)[0];
				}		
			}else{	// remove normal event
				if( tar.className=='note_input'){
					remove_event("track", tar.parentNode.parentNode.getAttribute("event_id"));
					reset_cell(tar.parentNode.parentNode);
				}else if( tar.className=='note_container' || tar.className=='containing_cell' || tar.className=='num_racers_active'){
					remove_event("track", tar.parentNode.getAttribute("event_id"));
					reset_cell(tar.parentNode);
				}else if( tar.className=='race_type' ||tar.className=='number_positioner'){
				    remove_event("track", tar.parentNode.parentNode.getAttribute("event_id"));
				    reset_cell(tar.parentNode);
                                }
				else{			
					remove_event("track", tar.getAttribute("event_id"));			
					reset_cell(tar);
				}
			}
		}
		_is_double_tap=false;
	}
	function remove_event(type,id){
		performAJAX(data_url, "function=remove_event&type="+type+"&event_id="+id,false,"POST");
	}
	function move_race(time, event_id, track_id, elem,targ){
		if(targ.getAttribute("event_id") == null && targ.className.indexOf("track_cell") != -1 ){
			performAJAX(data_url,"function=move_race&time="+time+"&event_id="+event_id+"&track_id="+track_id,false,"POST");
			targ.innerHTML = elem.innerHTML;
			/*targ.style.backgroundColor="white";*/
			targ.setAttribute("event_id",event_id);
			reset_cell(elem);
		}else if( elem.getAttribute('event_id') == targ.getAttribute('event_id')){
		    reset_cell(elem);
		}

	}
	function start_selection(time,elem, room_id){

		elem.className= "create_room_event_start room_cell";
		var inner_elem= document.createElement("div");
			inner_elem.className = 'room_event_start_inner';
			inner_elem.innerHTML = ".";
			//inner_elem.addEventListener("touchstart", touch_start);
			//inner_elem.addEventListener("touchend",   touch_end);
		elem.removeChild(elem.childNodes[0]);
		elem.appendChild(inner_elem);

	}
	function end_selection(end_time,end_elem,room_id){
	
		var start_elem = document.getElementsByClassName('create_room_event_start')[0];
		var start_time = start_elem.parentNode.id;
		if(start_elem.getAttribute("room_id")!= end_elem.getAttribute("room_id")){
			start_elem.className= "room_cell";
			return;
		}
		if(start_time == end_time){
			
			var url_args= "function=create_room_event&event_time="+start_time+"&duration="+interval+"&room_id="+room_id+"&event_date="+ get_selected_day();
			var room_event_id= performAJAX(data_url,url_args,false,"POST").id;
			end_elem.className= "room_cell room_event_start_end room_event_"+room_event_id;
			
			var inner_elem= document.createElement("div");
				inner_elem.className = 'room_event_start_end_inner';
				inner_elem.innerHTML = ".";
				//inner_elem.addEventListener("touchstart", touch_start);
				//inner_elem.addEventListener("touchend",   touch_end);
				
			end_elem.removeChild(end_elem.childNodes[0]);
			end_elem.appendChild(inner_elem);
			
			return;
			
		}
		if( start_time > end_time){	// flip the elements
			
			var temp   = start_time;
			start_time = end_time;
			end_time   = temp;		
			
			var temp_el = start_elem;
			start_elem  = end_elem;
			end_elem    = temp_el;
		}
		
		
		var index = get_column_offset(start_time, "room_id", room_id);
		var res   = check_overlapping(start_time, end_time, index);
		if (res){
			document.getElementsByClassName("create_room_event_start")[0].className="room_cell";
			//start_elem.className="room_cell";
			edit_room_event(res);
			return;
		}else{
			var url_args = "function=create_room_event&event_time="+start_time+"&duration="+get_duration(start_time, end_time)+"&room_id="+room_id+"&event_date="+ get_selected_day();
			var room_event_id = performAJAX(data_url,url_args,false,"POST").id;
			
			start_elem.className = "room_cell room_event_start room_event_"+room_event_id;
			var inner_elem= document.createElement("div");
				inner_elem.className = 'room_event_start_inner';
				inner_elem.innerHTML = ".";
				//inner_elem.addEventListener("touchend", cell_select);
				//inner_elem.addEventListener("click", cell_select);	
				
			start_elem.removeChild(start_elem.childNodes[0]);
			start_elem.appendChild(inner_elem);
			
			end_elem.className   = "room_cell room_event_end room_event_"+room_event_id;
			var inner_elem= document.createElement("div");
				inner_elem.className = 'room_event_end_inner';
				inner_elem.innerHTML = ".";
				//inner_elem.addEventListener("touchstart", touch_start);
				//inner_elem.addEventListener("touchend",   touch_end);
			end_elem.removeChild(end_elem.childNodes[0]);
			end_elem.appendChild(inner_elem);
			
			var par = document.getElementById(start_time);
			do{
				if( par.nextSibling.id < end_time){
					var el= par.nextSibling.childNodes[index];
					el.className= 'room_cell room_event_'+room_event_id+" room_event_mid";
					par= par.nextSibling;
					
					var inner_elem= document.createElement("div");
						inner_elem.className = 'room_event_mid_inner';
						inner_elem.innerHTML = ".";
					el.removeChild(el.childNodes[0]);
					el.appendChild(inner_elem);
					
					
					has_next_elem=true;
				}else
					has_next_elem=false;
				
			}while(has_next_elem);
		}
	}
	function get_column_offset(time, attribute_name, val){
		//This part gets the offset once that way we dont have to do it multiple times. 
		//console.log("time: "+time+" attribute_name: "+attribute_name+" val: "+val);
		
		var temp_arr = document.getElementById(time).childNodes;
		var index    = 0; 
		
		for( var t = 0; t< temp_arr.length; t++){
			if( temp_arr[t].getAttribute(attribute_name) && temp_arr[t].getAttribute(attribute_name) == val){
				return t;
			}
		}	
		return false;
	}
	function check_overlapping(start_time, end_time, index){
		//console.log(start_time+", "+end_time);
		var has_next_elem = true;
		var par 	      = document.getElementById(start_time);
		do{
			if( par.id <= end_time){
				var el= par.childNodes[index];
				if(el.className!=("create_room_event_start room_cell") && el.className.indexOf("room_event_")!=-1){
					return el;
				}
				par= par.nextSibling;
				has_next_elem=true;
			}else
				has_next_elem=false;
			
		}while(has_next_elem);
		return false;
	}
	function get_duration(start, end){
		var counter=1;
		while( document.getElementById(start).nextSibling.id != end){
			start=document.getElementById(start).nextSibling.id;
			counter++;
			if(counter > 500)
				return "failed";
		} 
		counter++;	
		
		return (counter*interval);
	}
	function edit_room_event(tar){
		var event_id = tar.className.replace(/\D/g,'');
		if(!event_id){
			event_id= tar.parentNode.className.replace(/\D/g,'');	//if they clicked the overlay thingy
		}
		var result   = performAJAX(data_url,"function=get_room_details&event_id="+event_id,false,"POST");
		var overlay  = document.getElementById("overlay");
				
		
		var close = document.getElementById("close_overlay");
		overlay.innerHTML='';
		overlay.appendChild(close);
		
		
		var form  = document.createElement("form");
			form.id='room_event_details'; 
		var table = document.createElement("table");
			table.className='room_event_table';
		
		//console.log(tar);
		if(!tar.getAttribute("room_id"))
			tar= tar.parentNode;
		
		tar.event_id= event_id;
		var form_elements= {
			"Title"        : create_form_element("text","title", decodeURIComponent(result.title), not_empty),
			"Event Time"   : create_form_element("select","event_time",result.event_time, available_times(tar.getAttribute("room_id"), event_id), tar),
			"Duration"     : create_form_element("select", "duration",result.duration, get_durations(tar.getAttribute("room_id"), event_id, result.event_time),tar),
			"Name"         : create_form_element("text", "cust_name",result.name,is_text),
			"Phone Number" : create_form_element("text", "cust_phone",result.cust_phone,not_empty),
			"Email Address": create_form_element("text", "cust_email",result.cust_email,is_email),
			"Notes"		   : create_form_element("textarea", "notes", decodeURIComponent(result.notes))
		};
		for(var index in form_elements){
			var tr = document.createElement("tr");
			
			var title_td = document.createElement("td");
				title_td.innerHTML = index;
				title_td.className = 'room_event_title';
		
			var input_td = document.createElement("td");
				input_td = form_elements[index];
				input_td.className= "room_event_input";
			
			tr.appendChild(title_td);
			tr.appendChild(input_td);
			table.appendChild(tr);
		}
		var tr = document.createElement("tr");
		
		var title_td = document.createElement("td");
			title_td.innerHTML = "<input type='hidden' name='event_id' value='"+event_id+"' /><input type='hidden' name='event_date' value='"+ get_selected_day()+"' />";
		
		var input_td = document.createElement("td");
			if(_debugging)
				input_td.innerHTML="<input type='button' value='Save Event' onmouseup=\"save_info($('#room_event_details'))\" onclick=\"save_info($('#room_event_details'))\">";
			else
				input_td.innerHTML="<input type='button' value='Save Event' onclick=\"save_info($('#room_event_details'))\">";
		
		tr.appendChild(title_td);
		tr.appendChild(input_td);
		table.appendChild(tr);
		
		form.appendChild(table);
		overlay.appendChild(form);
		
		
		overlay.style.display='block';
		
	}
	function save_info(form){
		var serialized = form.serialize();
		var result	   = performAJAX(data_url, "function=update_room_event&"+serialized,false,"POST");
		document.getElementsByClassName("room_event_"+form[0][7].value)[0].childNodes[0].innerHTML= document.getElementsByClassName("room_event_input")[0].value;		
		close_overlay();	
	}
	function create_form_element(type, name, default_val, validator, tar){
		var ele = null;
		if(type=='text'){
			ele= document.createElement("input");
			ele.setAttribute("type", type);
			ele.setAttribute("name", name);
		}else if(type=='select'){
			ele = document.createElement("select");
			ele.setAttribute("room_id",tar.getAttribute("room_id"));
			ele.setAttribute("event_id",tar.event_id);
			ele.setAttribute("name",name);
			ele.addEventListener("change", select_change);
			for(var i=0; i < validator.length; i++){
				var opt = document.createElement("option");
					opt.value=validator[i];
					var inner='';
					try{
						var first  = validator[i].substring(0,parseInt((validator[i].length/2)) );
						var second = validator[i].substring(parseInt((validator[i].length/2)));
						inner= first+":"+second;
					}catch(err){
						inner = validator[i];
					}
					
					
					opt.innerHTML=inner;
					
				ele.appendChild(opt);
			}
			
		}else if( type=='textarea'){
			ele = document.createElement(type);
			ele.setAttribute("name", name);
			ele.setAttribute("cols", "40");
			ele.setAttribute("rows", "5");
	    }
	    
	    if(default_val)
	    	ele.value= default_val;
	    if(validator)
	   		ele.addEventListener("keyup", validator);
		
		return ele;	
	}
	function select_change(event){
		var elems		= new Array();
		var cont 	    = null;
		var initial_val = null;
		if(event.target.getAttribute("name")=='event_time'){		// update duration
			if(!is_numeric(event.target.value)){
				alert("Invalid race time selection. Please choose a different time");
				event.target.value= document.getElementsByClassName("room_event_"+event.target.getAttribute("event_id"))[0].parentNode.id;
				return;
			}
			cont= event.target.parentNode.nextSibling.childNodes[1];
			initial_val = cont.value;
			elems= get_durations(event.target.getAttribute("room_id"), event.target.getAttribute("event_id"),event.target.value );
		}else if(event.target.getAttribute("name")=='duration'){ // update event times
			cont= event.target.parentNode.previousSibling.childNodes[1];
			initial_val = cont.value;
			elems= available_times(event.target.getAttribute("room_id"), event.target.getAttribute("event_id"));
		}	
		cont.innerHTML='';
		for(var i=0; i < elems.length; i++){
			var opt = document.createElement("option");
				opt.value=elems[i];
				opt.innerHTML=elems[i];
			if(elems[i] == initial_val)
				opt.setAttribute("selected");
			
			cont.appendChild(opt);
		}
	}
	function show_overlay(id, time, type, event_id){
		document.getElementById("overlay").style.display='block';
		var close= document.getElementById("close_overlay");
		overlay.innerHTML='';
		overlay.appendChild(close);
			close.style.top='5px';
	
		//window.location = '_DO:cmd=send_js&c_name=p2r_scheduling_wrapper&js=highlight_track_name("'+type+"_"+id+'")';
		document.getElementById(time).children[0].className+=		" time_selected";
		document.getElementById(time).children[1].style.marginLeft= '0';
		

		if(event_id){ // Show existing data
			edit_existing_event(event_id);
		}else{	// new Race/event
			show_event_type_buttons(id, time);
		}
	}
	function filter_by_csr(elem){
		var csr_list = get_csr_list();

		for(var i=0; i<csr_list.length; i++){
			if(elem.value == ''){ 
				var rows_to_hide= document.getElementsByClassName(csr_list[i]);
				for(var c=0; c< rows_to_hide.length; c++)
					rows_to_hide[c].style.display='block';
					
			}else if(csr_list[i] != elem.value){
				var rows_to_hide= document.getElementsByClassName(csr_list[i]);
				for(var c=0; c< rows_to_hide.length; c++)
					rows_to_hide[c].style.display='none';
			}else{
				var rows_to_hide= document.getElementsByClassName(csr_list[i]);
				for(var c=0; c< rows_to_hide.length; c++)
					rows_to_hide[c].style.display='block';
			}
		}
	}
	function show_search_bar(title, id){
		var search_bar_container= document.createElement("div");
		search_bar_container.className="search_bar_container";
		
		var title_container= document.createElement("div");
			title_container.style.fontSize='larger';
			title_container.style.textAlign='center';
			title_container.style.color='black';
			title_container.innerHTML=title+"<br>";
			
		
		var search_box= document.createElement("input");
			search_box.className='search_box';
			search_box.setAttribute("type","text");
			search_box.setAttribute("placeholder", "Add Racers");
			search_box.addEventListener("keyup",function(event){
				if(event.keyCode==13)
				    get_racers(document.getElementsByClassName("search_box")[0].value, id );
	      
			});
		var search_btn= document.createElement("input");
			search_btn.setAttribute("type", 'button');
			search_btn.className='do_search';
			search_btn.value='search';
			search_btn.addEventListener("click", 
				function(){
					get_racers(document.getElementsByClassName("search_box")[0].value, id ); 
					
				});	
		
		var advanced_search= document.createElement("select");
			advanced_search.className='csr_filter';
			advanced_search.setAttribute("onchange", "filter_by_csr(this); ");	
		var opt= document.createElement("option");
			opt.value="";
			opt.innerHTML="Filter By CSR";
		advanced_search.appendChild(opt);
		
		var csrs= get_csr_list();
		for( var i=0; i<csrs.length; i++){
			var opt= document.createElement("option");
				opt.value=csrs[i];
				opt.innerHTML= csrs[i];
			advanced_search.appendChild(opt);		
		}
		
		search_bar_container.appendChild(title_container);
		search_bar_container.appendChild(search_box);
		search_bar_container.appendChild(search_btn);
		search_bar_container.appendChild(advanced_search);
		
		
		
		document.getElementById("overlay").appendChild(search_bar_container);	
	}
	function get_csr_list(){
		return _csr_list;
	}
	function edit_existing_event(event_id){
		
		var data= performAJAX(data_url,"function=get_racers_for_race&event_id="+event_id,false,"POST");
		var checked='checked';
		if(!data.racer_data){
			var checked='';
			for( var i=0; i < data.racer_data_recent.length; i++){
				if(!inArray(data.racer_data_recent[i].user_id, _csr_list))
				_csr_list.push(data.racer_data_recent[i].user_id);			
			}
		
			data.racer_data= data.racer_data_recent;
		}
		show_search_bar(data.race_data['title'],event_id);
		var inner_container = '';
		if( document.getElementsByClassName("results_container").length){
			document.getElementsByClassName("results_container").innerHTML='';
			
			inner_container = document.getElementsByClassName("results_container")[0];
		}else{
			inner_container = document.createElement("div");
			inner_container.className='results_container';
		}
				
		for(var i=0; i < data.racer_data.length; i++){
			 
			var container   = document.createElement("div");
			var check_div   = document.createElement("div");
			var empty_div   = document.createElement("div");
			var name_div    = document.createElement("div");
			var credits_div = document.createElement("div");
			var birth_div	= document.createElement("div");
			
			check_div.className   = 'check_div';
			if(checked == '')
				check_div.innerHTML   = "<input type='checkbox' class='check_class' id='racer_id_"+data.racer_data[i].id+"' onchange='add_racer(\""+data.racer_data[i].id+"\",\""+event_id+"\", this)' "+checked+">";
			else
				check_div.innerHTML   = "<input type='checkbox' class='check_class' id='racer_id_"+data.racer_data[i].id+"' onchange='remove_racer(\""+data.racer_data[i].id+"\",\""+event_id+"\")' "+checked+">";
			
			name_div.className    = 'racer_name_div';
			name_div.innerHTML	  = data.racer_data[i].racer_name;
			
			birth_div.className    = 'b_day_div';
			birth_div.innerHTML	  = data.racer_data[i].birth_date;
				
			
			credits_div.className = 'num_credits_remaining';
			credits_div.innerHTML = data.racer_data[i].credits;
			
			container.className   = 'racer_row_container';
			if( data.racer_data[i].user_id)
				container.className+=" "+data.racer_data[i].user_id;
			container.appendChild(check_div  );
			container.appendChild(name_div   );
			container.appendChild(credits_div);
		
			inner_container.appendChild(container);
		}
		document.getElementById("overlay").appendChild(inner_container);
	}
	function inArray(needle, haystack) {
    	var length = haystack.length;
    	for(var i = 0; i < length; i++) {
        	if(haystack[i] == needle) 
        		return true;
		}
   		return false;
	}	
	function add_racer(racer_id, event_id, elem){
		var res = performAJAX(data_url,"function=add_racer&racer_id="+racer_id+"&event_id="+event_id,false,"POST");
		
		if(res.result == 'success')
			elem.setAttribute("onchange", "remove_racer('"+racer_id+"','"+event_id+"')")
		else if(res.result == 'full_race'){
			alert("Race Full");
			elem.checked=false;
		}
			
	}
	function remove_racer(racer_id, event_id){
		performAJAX(data_url,"function=remove_racer&racer_id="+racer_id+"&event_id="+event_id,true,"POST");
	}
	function show_event_type_buttons(track_id, time){
		var elem          = document.getElementById("overlay");
		var event_types   = performAJAX(data_url,"function=get_event_types",false,"POST");
		var container_div = document.createElement("div");
		var normal_types  = event_types.normal_races;
		var sequence_types= event_types.sequence_races;
		
		container_div.className = "event_button_container";
		for(var i=0; i < normal_types.length; i++){			//normal event creation 
			
			var button		 = document.createElement("div");
			button.className ='event_button';
			button.innerHTML = normal_types[i].title;
			
			button.setAttribute("event_id",  normal_types[i].id);
			button.setAttribute("slots", 	 normal_types[i].slots);
			button.setAttribute("race_time", time);
			button.setAttribute("track_id",  track_id);
			button.setAttribute("title",	 normal_types[i].title);
			
			button.addEventListener("touchend",select_event_type);
			if(_debugging)
				button.addEventListener("click",select_event_type);
			container_div.appendChild(button);
		}
		for(var i = 0; i < sequence_types.length; i++){	// sequence event creation 
			
			var button		 = document.createElement("div");
			button.className = 'sequence_button';
			button.innerHTML = sequence_types[i].title;
			
			button.setAttribute("event_id",  sequence_types[i].id);
			button.setAttribute("slots", 	 sequence_types[i].slots);
			button.setAttribute("race_time", time);
			button.setAttribute("track_id",  track_id);
			button.setAttribute("title",	 sequence_types[i].title);
			
			button.addEventListener("touchend",show_number_selector);
			if(_debugging)
				button.addEventListener("click",   show_number_selector);
			container_div.appendChild(button);
		
		}
		
		elem.appendChild(container_div);
	}
	function show_number_selector(event){
		
		var tar = event.target;
		var con = document.createElement("div");
			con.setAttribute("time", tar.getAttribute("race_time"));
			con.setAttribute("sequence_type_id",tar.getAttribute('event_id'));
			con.setAttribute("track_id",tar.getAttribute("track_id"));
			con.setAttribute("title",tar.getAttribute("title"));
			con.setAttribute("date",get_selected_day());
			con.addEventListener("touchend", create_sequence_race);
			if(_debugging)
				con.addEventListener("click", create_sequence_race);

			con.innerHTML= "Go";
			con.className= "number_pad_num number_pad_go";
			
		var del = document.createElement("div");
			del.addEventListener("touchstart", function(event){event.preventDefault()});
			del.addEventListener("touchend", remove_num);
			
			if(_debugging)
				del.addEventListener("click", remove_num);
			
			del.innerHTML="<";
			del.className= "number_pad_num number_pad_back";

		var overlay = document.getElementById("overlay");
		var cont	= document.createElement("div");
		var input 	= document.createElement("input");
		var num 	= 0; 

		cont.className="number_pad_container";
		var number_container= document.createElement("div");
			number_container.className="num_racers_container";
			
		for( var i = 9; i > -1; i--){
			var b= document.createElement("div");
			
			b.addEventListener("touchend", add_num);
			if(_debugging)
				b.addEventListener("click", add_num);
			
			b.innerHTML=i;
			b.className= "number_pad_num";
		
			if( i==0){
				cont.appendChild(con);
				cont.appendChild(b);
				cont.appendChild(del);
			}else{
				cont.appendChild(b);
			}
		}
		var close = document.getElementById("close_overlay");
		overlay.innerHTML = '';
		overlay.appendChild(close);
	
		overlay.appendChild(number_container);
		overlay.appendChild(cont);
		return num;
	}
	function add_num(event){
		event.preventDefault();
		
		if( _num_racers.toString().length < 3 && ((_num_racers+event.target.innerHTML)*1)<=128 ){
			_num_racers+=""+event.target.innerHTML;
			document.getElementsByClassName("num_racers_container")[0].innerHTML= _num_racers;
		}
	}
	function remove_num (event){
		event.preventDefault();
		_num_racers= _num_racers.substring(0,_num_racers.length-1);
		document.getElementsByClassName("num_racers_container")[0].innerHTML= _num_racers;
	}
	function create_sequence_race(event){
		
		if( _num_racers!=''){
			setTimeout(function(){
				event.preventDefault();
				var targ	  = event.target;
				var send_data = "function=create_sequence_race&date="+targ.getAttribute("date")+"&time="+targ.getAttribute("time")+"&sequence_type_id="+
								targ.getAttribute("sequence_type_id")+"&track_id="+targ.getAttribute("track_id")+"&num_racers="+_num_racers+"&interval=" + interval;
				//alert(send_data);
				performAJAX(data_url, send_data,false,"POST");
				close_overlay();
				_num_racers="";
			},250);
			
		}else{
			alert("Please select how many racers you want in this race.");
		}
	}
	function select_event_type(event){
		
		var tar	  		  = event.target;
		var time		  = tar.getAttribute("race_time");
		var event_type_id = tar.getAttribute('event_id');
		var track_id	  = tar.getAttribute("track_id");
		var title 		  = tar.getAttribute("title");
		var date 		  = get_selected_day();
		var url_string= "function=create_race&race_type_id="+event_type_id+"&race_time="+time+"&track_id="+track_id+"&date="+date;
	
		var new_id= performAJAX(data_url,url_string,false,"POST");
		//show_search_bar(title, new_id.id);
		edit_existing_event(new_id.id);
		document.getElementsByClassName("event_button_container")[0].style.display='none';
		//build_select_users_table(title, new_id.id);
	}
	function get_racers(search_term, id){
		if (search_term.length > 2){
			var result= performAJAX(data_url,"function=get_racers_search&search_term="+search_term,false,"POST");
			//console.log(result);
			build_list(result, id);
		}else	
			alert("Search Must be more than 2 characters");
		
	}
	function build_list(racer_data,event_id){
		
		var inner_container='';
		if( document.getElementsByClassName("results_container").length){
			document.getElementsByClassName("results_container")[0].innerHTML='';
			inner_container = document.getElementsByClassName("results_container")[0];
		}else{
			inner_container = document.createElement("div");
			inner_container.className='results_container';
		}
		for(var i=0; i<racer_data.length; i++){
			if( document.getElementById("racer_id_"+racer_data[i].id))
				continue;
			var container   = document.createElement("div");
			var check_div   = document.createElement("div");
			var b_day_div   = document.createElement("div");
			var name_div    = document.createElement("div");
			var credits_div = document.createElement("div");
			
			check_div.className= 'check_div';
			check_div.innerHTML= "<input type='checkbox' class='check_class' onchange='add_racer(\""+racer_data[i].id+"\",\""+event_id+"\", this)'>";
			
			name_div.className    = 'racer_name_div';
			name_div.innerHTML	  = racer_data[i].racer_name;
			
			b_day_div.className   = 'b_day_div';
			b_day_div.innerHTML   = racer_data[i].birth_date;
		
			credits_div.className = 'num_credits_remaining';
			credits_div.innerHTML = racer_data[i].credits;
			
			container.className   = 'racer_row_container';
			container.appendChild(check_div);
			container.appendChild(name_div   );
			container.appendChild(b_day_div);
			container.appendChild(credits_div);
			inner_container.appendChild(container);
		}
		document.getElementById("overlay").appendChild(inner_container);
	}
	function loading(){
		document.getElementById("loading").style.display='block';
	}
	function done_loading(){
		document.getElementById("loading").style.display='none';
	}
	function performAJAX(URL, urlString,asyncSetting,type, callback ){
		var returnVal='';
		//if(asyncSetting == false)
		//loading();
			
		current_ajax= $.ajax({ 
			url:   URL,
			type:  type,
			data:  urlString,
			async: asyncSetting,
			success: 
			function (string){
				//done_loading();
				//console.log(string);
				if(check_json(string)){
					var obj= {};
					if(string)
						obj = jQuery.parseJSON( string );
					else {
						
						obj.error = 'No results returned url string was: '+ urlString+ "The return string is: "+string;
					}
					if(obj.error){
						alert(obj.error);
						close_overlay();
						initialize(get_selected_day());
						return;
					}
					if(callback){
						callback(obj);
					}else{
						returnVal= obj;
					}
				}else{
					alert("error checking json: "+string);
				}
			}
		});	
		return returnVal; 
	}
	function start_move(elem){
		var selected_cell = document.getElementsByClassName("cell_selected")[0];
		if(selected_cell){
			selected_cell.className = selected_cell.className.replace(/\bcell_selected\b/,'')
			//selected_cell.style.backgroundColor='#ffffff';
			
		}
		//elem.parentNode.style.backgroundColor='#12111b';
		elem.parentNode.className='cell_selected '+elem.parentNode.className;
	}
	function close_overlay(elem){
		var overlay= document.getElementById("overlay");
		var close= document.getElementById("close_overlay");
		overlay.innerHTML='';
		//window.location = '_DO:cmd=send_js&c_name=p2r_scheduling_wrapper&js=unhighlight_track_name()';
		//document.getElementsByClassName("header_selected")[0].className=document.getElementsByClassName("header_selected")[0].className.replace(/\bheader_selected\b/,'')
		if(document.getElementsByClassName("time_selected").length) 
		document.getElementsByClassName("time_selected")[0].className=document.getElementsByClassName("time_selected")[0].className.replace(/\btime_selected\b/,'')
		if(close){
			overlay.appendChild(close);
			close.style.top='5px';
		}
		overlay.style.display='none';
	}
	function check_json(text){
		
		if (/^[\],:{}\s]*$/.test(text.replace(/\\["\\\/bfnrtu]/g, '@').
		replace(/"[^"\\\n\r]*"|true|false|null|-?\d+(?:\.\d*)?(?:[eE][+\-]?\d+)?/g, ']').
		replace(/(?:^|:|,)(?:\s*\[)+/g, ''))) {
			return true;
		}else{
	  		return false;
	  	}
	}

	/*************************HELPER VALIDATION FUNCTIONS ****************/
	function not_empty(){
		return true;
	}
	function is_numeric(n){
		return !isNaN(parseFloat(n)) && isFinite(n);
	}
	function is_date(){
		
	}
	function is_text(){
		
	}
	function is_email(){
		
	}
	function get_durations(room_id, evt_id, time){
		var ret_arr   = new Array();
		var rooms_all = document.getElementsByClassName("room_cell");
		var cur_duration = interval;
		
		ret_arr.push(cur_duration);
		for( var i = 0; i < rooms_all.length; i++){
			if( rooms_all[i].getAttribute("room_id") == room_id && rooms_all[i].parentNode.id > time){
				if(rooms_all[i].className == 'room_cell' || rooms_all[i].className.indexOf("room_event_"+evt_id)!=-1){
					cur_duration+=interval;	
					ret_arr.push(cur_duration);
				}else{
					break;
				}
			}
		}
		return ret_arr;
			
	}
	function available_times(room_id, evt_id){
		
		var ret_arr= new Array();
		var rooms_all= document.getElementsByClassName("room_cell");
		var size= document.getElementsByClassName("room_event_"+evt_id).length;
		for(var i = 0; i < rooms_all.length; i++){
			if( rooms_all[i].getAttribute("room_id") == room_id){
				
				if(rooms_all[i].className=='room_cell' || rooms_all[i].className.indexOf("room_event_"+evt_id)!=-1)
					ret_arr.push(rooms_all[i].parentNode.id);	
				else{
					ret_arr.push(rooms_all[i].parentNode.id.replace(/\d/g, function (digit) { return digit + "\u0336" }));
				}
			}
		}
		return ret_arr;
	}
	function update_schedule(){
		setTimeout(update_races_completed, 60000);
	}
	function update_races_completed(){
		var cells   	 = document.getElementsByClassName("track_cell_with_contents");
		var date    	 = new Date();
		var mins		 = date.getMinutes();
		var hrs 		 = date.getHours();
		if(mins < 10){
			mins= "0"+mins;
		}
		if(mins == 0)
			mins = 50;
		if(hrs < 10) 
			hrs = "0"+hrs;
		var current_time =  hrs+""+mins;
		
		for(var i=0; i< cells.length; i++){
			if(cells[i].innerHTML !='.'){
				var time  = cells[i].parentNode.id;
				if((parseInt(current_time) - parseInt(time)) > 0 && (parseInt(current_time) - parseInt(time)) > 10){
					cells[i].className='past_race '+ cells[i].className;
					cells[i].style.backgroundColor="gray";
					cells[i].style.color="white";
				}else if((parseInt(current_time) - parseInt(time)) > 0 && (parseInt(current_time) - parseInt(time)) < 10){
					cells[i].style.backgroundColor="#aecd37";
					cells[i].style.color='white';
				}
				/*else if((hrs == time.substring(0,2)) &&(time.substring(2) < mins && mins > time.substring(2)) ){
					cells[i].style.backgroundColor="#aecd37";
					cells[i].style.color='white';
				}*/
			}
		}
		update_schedule();
	}
	function refresh(){// TODO 
		current_ajax.abort();
		//window.location.refresh();
		performAJAX("./check_new_events.php","function=check_new_events",true,"POST",update_grid);	
		//performAJAX(data_url,"cancel_do=1",false,"POST");
		//performAJAX(data_url,"function=check_new_events&first=1",true,"POST",update_grid);
		return "1";
	}
	/*********************************************************************/
	window.onload= function(){
		var temp= new Image();
		temp.src= "./images/close_icon.png";
		initialize();
		var d   = new Date();
		var hr	= d.getHours();
		var min = d.getMinutes();
		if( hr < 10)
			hr= "0"+hr;
		min= Math.round(min / 10) * 10;
		if(min==60||min==0)
			min="00";
		
		setTimeout(function(){
			//console.log(hr+" "+min);
			if(document.getElementById(hr+""+min))
				document.getElementById(hr+""+min).scrollIntoView();
		}, 250);
		
	}; 
	
				
			/*
			var mai_day= "_"+d;
			if(month_races[mai_day]){
				var races_container= document.createElement("div");
				races_container.className="races_container";
				for(var hours = 0; hours < 16; hours++){
					var h = ((9+hours)%12)+1;
					for(minutes = 0; minutes < 60; minutes+=interval){
						if( minutes < 10)
							var time= h.toString()+"0"+minutes.toString();
						else
							var time= h.toString()+minutes.toString();
						
						var new_elem= document.createElement("div");
						
						if(month_races[mai_day].indexOf(time)!=-1){
							new_elem.className="full_race_time";
						}else{
							new_elem.className="empty_race_time";	
						}		
						races_container.appendChild(new_elem);
					}
				}
				td.appendChild(races_container);
			}*/