<?php
	ini_set("display_errors",1);
	$enable_logging =false;
	date_default_timezone_set('UTC');
	require_once(dirname(__FILE__)."/../../../comconnect.php");
	require_once("/poslavu-local/cp/resources/json.php");
	//global $loc_id;
	//$loc_id= 1;	
	define("CHECK_INTERVAL", 1000000);
	$loc_id = $server_remote_locationid;

	if(isset($_POST['function'])){
		$fun = $_POST['function'];
		session_destroy();
		unset($_POST['function']);
		if($enable_logging)
			error_log("function: ". $fun." args: ".print_r($_POST,1));
		$fun($_POST);
		exit;
	}else{
		echo '{"error":"No Function."}';
		exit;
	}

	function check_new_events($args){
		global $enable_logging;
		//error_log (CHECK_INTERVAL);
		$counter  	   = 0;
		$return_update = false;
		$event_stamp = -1;
		$sched_id    = -1;
		$room_stamp  = -1;
		
		if( isset($args['event_stamp']))
			$event_stamp = $args['event_stamp'];
		if(isset($args['sched_id']))
			$sched_id = $args['sched_id'];		
		
		if( isset($args['room_stamp']))
			$room_stamp= $args['room_stamp'];
			
		do{
			$sched_query = lavu_query("SELECT * FROM `lk_event_schedules` WHERE `time_stamp` > '[1]' order by `lastmod` desc ",$sched_id);
			if(lavu_dberror()){
				error_log("sched query: ".lavu_dberror());
			}
			$event_query = lavu_query("SELECT * FROM `lk_events` WHERE `lastmod` > '[1]' order by `ts` desc ", $event_stamp);
			if(lavu_dberror()){
				error_log("event_query error:".lavu_dberror());
			}
			$room_query  = lavu_query("SELECT * FROM `lk_group_events` WHERE `lastmod` >'[1]' order by `time_stamp` desc", $room_stamp);
			if(lavu_dberror()){
				error_log("room query error".lavu_dberror());
			}
			$return_arr  = array();
			if(mysqli_num_rows($sched_query)){
			
				$racers_arr  = array();
				$sched_res   = mysqli_fetch_assoc($sched_query);
				$race_data   = mysqli_fetch_assoc(lavu_query("SELECT * FROM `lk_events` WHERE `id` = '[1]'", $sched_res['eventid']));
				$total_laps  = mysqli_fetch_assoc(lavu_query("SELECT * FROM `lk_event_types` WHERE `id`='[1]'", $race_data['type']));
				$num_racers  = $total_laps['slots'] - mysqli_num_rows(lavu_query("SELECT * FROM `lk_event_schedules` WHERE `eventid` = '[1]' AND `_deleted`='0'", $sched_res['eventid']));
			
				$racers_arr = array(
									"date"   	 => $race_data['date'],
									"event_id"   => $sched_res['eventid'], 
									"num_racers" => $num_racers,
									"sched_id"   => $sched_res['time_stamp']
									
							  );
				$return_arr['racers_update'] = $racers_arr;
				$return_update = true;
			}else
				$return_arr['racers_update'] = array("sched_id" => $sched_id);
			
			if(mysqli_num_rows($event_query)){
				
				$event_res  = mysqli_fetch_assoc($event_query);
				
				if($event_res['_deleted'] == 1){
					$return_arr['race_update'] = array("data" => array("deleted" => 1, "event_id" => $event_res['id'], "date"=> $event_res['date']), "event_stamp" => $event_res['lastmod']);
					$return_update 			   = true;
				}else{
				
					if( $event_res['groupid']!=''){// get all the sequenced races;
						$q= lavu_query("SELECT * FROM `lk_events` WHERE `groupid`='[1]' ", $event_res['groupid']);
						$race_arr = array("data"=>array());
						
						while($res= mysqli_fetch_assoc($q)){
							$type_res   = mysqli_fetch_assoc(lavu_query("SELECT * FROM `lk_event_types` WHERE `id`='[1]'", $res['type']));
							$num_racers = mysqli_num_rows(lavu_query("SELECT * FROM `lk_event_schedules` WHERE `eventid` = '[1]' AND `_deleted`='0' ", $res['id']));
							
							$type_res['time']    		   = $res['time']; 
							$type_res['trackid'] 		   = $res['trackid'];
							$type_res['date']    		   = $res['date'];
							$type_res['notes']	 		   = rawurlencode($res['notes']);
							$type_res['groupid'] 		   = $res['groupid'];
							$type_res['group_racer_count'] = $res['group_racer_count'];
							$type_res['ms_start']		   = $res['ms_end'];
							$type_res['ms_end']			   = $res['ms_end'];
							$type_res['num_racers_remaining']= get_num_slots($event_res['type']) - $num_racers;
							$type_res['ts']				   = $res['ts'];
							$type_res['event_id']   	   = $res['id'];
							
							$race_arr['data'][]      = $type_res;
							$race_arr['event_stamp'] = $event_res['lastmod'];	
						}
						$return_arr['race_update'] = $race_arr; 
						$return_update = true;
					}else{
						
						$type_res   = mysqli_fetch_assoc(lavu_query("SELECT * FROM `lk_event_types` WHERE `id`='[1]'", $event_res['type']));
						$num_racers = mysqli_num_rows(lavu_query("SELECT * FROM `lk_event_schedules` WHERE `eventid` = '[1]' AND `_deleted`='0' ", $event_res['id']));
						
						$type_res['time']       = $event_res['time']; 
						$type_res['trackid']    = $event_res['trackid'];
						$type_res['date']       = $event_res['date'];
						$type_res['notes']	    = rawurlencode($event_res['notes']);
						$type_res['groupid']	= $event_res['groupid'];
						$type_res['group_racer_count'] = $event_res['group_racer_count'];
						$type_res['ms_end']		= $event_res['ms_end'];
						$type_res['ms_start']   = $event_res['ms_start'];
						$type_res['num_racers_remaining'] = get_num_slots($event_res['type']) - $num_racers;
						$type_res['ts']			= $event_res['ts'];
						$type_res['event_id']   = $event_res['id'];
						
						$race_arr = array();
						$race_arr['data']        = $type_res;
						$race_arr['event_stamp'] = $event_res['lastmod'];
		
						$return_arr['race_update'] = $race_arr; 
						$return_update = true;
	
					}				
				}
			}else
				$return_arr['race_update']=array("event_stamp"=>$event_stamp);
			
			if(mysqli_num_rows($room_query)){
				$res= mysqli_fetch_assoc($room_query);
				if($res['item_id']!='[item_id]'){
				
					$item_type = mysqli_fetch_assoc(lavu_query("SELECT * FROM `menu_items` where `id`='[1]'", $res['item_id']));
					$res['item_id']= $item_type['name'];
				}
				if(strlen($res['event_time'])==3)
					$res['event_time']= "0".$res['event_time'];
					
				$res['notes'] = rawurlencode($res['notes']);
			 	$return_arr['room_update']= $res;
			 	$return_update=true;
			}else{
				$return_arr['room_update'] = array("lastmod"=>$room_stamp);
			}
			
			if($return_update){
				if($enable_logging)				
					error_log(print_r($return_arr,1));
				echo json_encode($return_arr);
				return;
			}else{
				$counter++;
				if($counter < 50)
					usleep(CHECK_INTERVAL);
				
				//}else{
					if($enable_logging)
						error_log(print_r($args,TRUE));
					$return_val= json_encode(array("racers_update"=>array("sched_id"=>$_POST['sched_id']),"race_update"=>array("event_stamp"=>$_POST['event_stamp']),"room_update"=>array("lastmod"=>$_POST['room_stamp'])));
					if($enable_logging)
						error_log("Out of time ".$return_val);
					echo $return_val;
					return;
					//return json_encode($args);
					//return '{"racers_update":"'.$sched_id.'","race_update":"'.$event_stamp.'", "room_update":"'.$room_stamp.'"}';
					
				//}
			}
		}while(true);
	}
	function get_num_slots($id){
		$res=  mysqli_fetch_assoc(lavu_query("SELECT * FROM `lk_event_types` WHERE `id`='[1]' AND `_deleted`='0'", $id));
		return $res['slots'];
	}
?>