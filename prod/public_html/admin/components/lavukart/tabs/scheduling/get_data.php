<?php
	ini_set("display_errors",1);
	
	$enable_logging =false;
	date_default_timezone_set('UTC');
	require_once(dirname(__FILE__)."/../../../comconnect.php");
	$loc_id = $server_remote_locationid;
	require_once("/poslavu-local/cp/resources/json.php");
	define("CHECK_INTERVAL", 1000000);
	echo "here";
	if(isset($_POST['function'])){
		$fun = $_POST['function'];
		//session_destroy();
		unset($_POST['function']);
		if($enable_logging)
			error_log("function: ". $fun." args: ".print_r($_POST,1));
		
		echo $fun($_POST);
	}else{
		echo '{"error":"No Function."}';
	}

	function get_active_tracks(){
		$query= lavu_query("SELECT * FROM `lk_tracks` WHERE `_deleted`='0'");
		$return_arr= array();
		while($res= mysqli_fetch_assoc($query)){
			$return_arr[]= $res;
		}
		return json_encode($return_arr);

	}
	function get_races($args){
		$date= $args['date'];
		$query = lavu_query("SELECT * FROM `lk_events` WHERE DATE(`date`) = DATE('[1]') AND `_deleted`='0'", $date);
		$races = array();
		while($res= mysqli_fetch_assoc($query)){
		
			$type			  = get_event_type($res['type']);
			$res['num_racers_remaining']= get_num_slots($res['type']) - get_num_racers($res['id']);
			$res['evt_type']     = $type['title'];
			$res['abbreviation'] = $type['abbreviation'];
			$res['color']		 = $type['color'];
			$races[] = $res;
		}
		//return LavuJson::json_encode($races);
		return json_encode($races);	
	}
	function get_rooms($args){
		global $enable_logging;
		$date= $args['date'];
		$query = lavu_query("SELECT * FROM `lk_group_events` WHERE DATE(`event_date`) = DATE('[1]') AND `room_id` !='' and `_deleted`='0'", $date);
		$rooms = array();
		while($res= mysqli_fetch_assoc($query)){
		    if($res['facility_buyout']==1){
		    	if($enable_logging)
		    		error_log('facility buyout');	
				$r_query = lavu_query("SELECT * FROM `lk_rooms` where `_deleted`!='1' ");
				while($each_room = mysqli_fetch_assoc($r_query)){
			 		$room_data=get_room_name($each_room['id']); 
				    $res['room_id']=$each_room['id'];
				    $res['room_title']=$room_data['title'];    
				    $res['room_abbr']= $room_data['abbreviation'];
				    $res['notes']= rawurlencode($res['notes']);
					if($res['item_id']!='[item_id]'){
						$item_type = mysqli_fetch_assoc(lavu_query("SELECT * FROM `menu_items` where `id`='[1]'", $res['item_id']));
						$res['item_id']= $item_type['name'];
				 	}
				   $rooms[] = $res;
				}
		    }else{
				if($res['room_id']!=0){
					$room_data=get_room_name($res['room_id']); 
					$res['room_title']=$room_data['title'];
					$res['room_abbr']= $room_data['abbreviation'];
					$res['notes']= rawurlencode($res['notes']);
					if($res['item_id']!='[item_id]'){
						$item_type = mysqli_fetch_assoc(lavu_query("SELECT * FROM `menu_items` where `id`='[1]'", $res['item_id']));
						$res['item_id']= $item_type['name'];
					}
					$rooms[] = $res;
				}
		    }
		    
		}
		return json_encode($rooms);		
	}
	function get_time_interval(){
		return " the loc id is:". $loc_id;
		/*global $loc_id;
		$res = mysqli_fetch_assoc(lavu_query("SELECT * FROM `lk_tracks` WHERE `locationid`='[1]' AND `_deleted`!='1' ", $loc_id));
		return "the loc id is: $loc_id";
		return '{"time_interval":"'.intval($res['time_interval']).'"}';
	*/}
	function get_num_slots($id){
		$res=  mysqli_fetch_assoc(lavu_query("SELECT * FROM `lk_event_types` WHERE `id`='[1]' AND `_deleted`='0'", $id));
		return $res['slots'];
	}
	function get_num_racers($id){
		return mysqli_num_rows(lavu_query("SELECT * FROM `lk_event_schedules` WHERE `eventid`='[1]' AND `_deleted`='0'", $id));
	}
	function get_event_type($id){
		$res=  mysqli_fetch_assoc(lavu_query("SELECT `abbreviation`,`title`,`color` FROM `lk_event_types` WHERE `id`='[1]' ", $id));
		return $res;
	}
	function get_tracks_and_rooms(){
		global $loc_id;
		$track_query = lavu_query("SELECT * FROM `lk_tracks` WHERE `_deleted`='0' AND `locationid`='[1]'",$loc_id);
		$room_query  = lavu_query("SELECT * FROM `lk_rooms` WHERE `_deleted`='0' AND `locationid`='[1]'",$loc_id);
		$tracks 	 = array();
		$rooms  	 = array();
		while($track_res = mysqli_fetch_assoc($track_query)){
			$tracks[]= $track_res;
		}
		while($room_res= mysqli_fetch_assoc($room_query)){
			$rooms[]= $room_res;
		}
		$return_arr= array("tracks"=>$tracks, "rooms"=>$rooms);
		//return LavuJson::json_encode($return_arr);
		return json_encode($return_arr);
	}
	function get_room_name($room_id){
		return mysqli_fetch_assoc(lavu_query("SELECT `title`, `abbreviation`,`color` FROM `lk_rooms` WHERE `id`= '[1]'", $room_id));
	}
	function get_event_types(){
		global $loc_id;
		//echo $loc_id;
		
		$event_query    = lavu_query("SELECT * FROM `lk_event_types` WHERE `_deleted`='0' and `locationid`='[1]'",$loc_id);
		$sequence_query = lavu_query("SELECT * FROM `lk_event_sequences` WHERE `_deleted`='0' and `locationid`='[1]'",$loc_id);
		
		$types_arr    = array();
		$sequence_arr = array();
		
		while($event_res = mysqli_fetch_assoc($event_query)){
			$types_arr[] = $event_res;
		}
		while($sequence_res = mysqli_fetch_assoc($sequence_query)){
			$sequence_arr[] = $sequence_res;
		}
		$return_arr=array("normal_races"=>$types_arr,"sequence_races"=>$sequence_arr);
		//error_log("The return array is :".print_r($return_arr,1));
		//echo LavuJson::json_encode($return_arr);
		//echo json_encode($return_arr);
		return json_encode($return_arr);
		//return LavuJson::json_encode($return_arr);
	}
	function get_racers_search($args){
		global $loc_id;
		global $enable_logging;
		$term	= $args['search_term'];
		$type= mysqli_fetch_assoc(lavu_query("select `type` from `lk_events` where `id`='[1]'",$args['event_id']));
		$use_credits_res = mysqli_fetch_assoc(lavu_query("select `use_credits` from `lk_event_types` where `id`='[1]'",$type['type']));
		if($use_credits_res['use_credits'] =='' || $use_credits_res['use_credits']=='Yes'){
			$query= lavu_query("select * from `lk_customers` where `credits` > 0 AND (`racer_name` LIKE TRIM('%[1]%') or `f_name` LIKE TRIM('%[1]%') or `l_name` LIKE TRIM('%[1]%') or concat(TRIM(`f_name`),' ',TRIM(`l_name`)) LIKE TRIM('%[1]%') )ORDER by TRIM(`f_name`) LIKE TRIM('[1]') desc, `f_name` asc, `l_name` asc limit 100", $term);
		}else{
			$query= lavu_query("select * from `lk_customers` where (`racer_name` LIKE TRIM('%[1]%') or `f_name` LIKE TRIM('%[1]%') or `l_name` LIKE TRIM('%[1]%') or concat(TRIM(`f_name`),' ',TRIM(`l_name`)) LIKE TRIM('%[1]%') )ORDER by TRIM(`f_name`) LIKE TRIM('[1]') desc, `f_name` asc, `l_name` asc limit 100", $term);

		}
		
		$return_arr= array();
		//echo "";
		while($res= mysqli_fetch_assoc($query)){
			$return_arr[]= $res;
		}

		$min_activity = date("Y-m-d") . " 00:00:00";
		$max_activity = date("Y-m-d",mktime(12,0,0,date("m"),date("d") + 1,date("Y"))) . " 00:00:00";
		$racer_info_query = lavu_query("SELECT * FROM `lk_customers` WHERE `last_activity`>='[1]' and `last_activity`<='[2]' and `credits` > 0 order by `last_activity` desc",$min_activity,$max_activity);
		while($racer_info = mysqli_fetch_assoc($racer_info_query)){
			if(!in_array($racer_info, $return_arr) )
				$return_arr[]= $racer_info;
		}
		//error_log( print_r($return_arr,1));

		if( lavu_dberror())
		    error_log(lavu_dberror());
		if($enable_logging)
			error_log(print_r($return_arr,1));
		return json_encode($return_arr);
		//return LavuJson::json_encode($return_arr);
	}
	function create_timestamp_from_date_and_time($args)
	{
		$ts = "";
		if(isset($args['date'])) $dt = $args['date']; else $dt = "";
		if($dt=="" && isset($args['event_id']))
		{
			$event_query = lavu_query("select `date`,`id`,`time` from `lk_events` where `id`='[1]'",$args['event_id']);
			if(mysqli_num_rows($event_query))
			{
				$event_read = mysqli_fetch_assoc($event_query);
				$dt = $event_read['date'];
			}
		}
		if(isset($args['time'])) $tm = $args['time']; else $tm = "";
		if($tm=="" && isset($args['race_time'])) $tm = $args['race_time'];
		if($dt!="" && $tm!="")
		{
			$tm = str_replace(":","",$tm);
			if(strlen($tm) > 4) $tm = substr($tm,0,4);
			$tm = $tm * 1;
			$mins = $tm % 100;
			$hours = ($tm - $mins) / 100;
			if(strlen($dt) > 10) $dt = substr($dt,0,10);
			$dt_parts = explode("-",$dt);
			if(count($dt_parts) > 2)
			{
				$year = $dt_parts[0];
				$month = $dt_parts[1];
				$day = $dt_parts[2];
				
				$ts = mktime($hours,$mins,0,$month,$day,$year);
			}
			else $ts = "";
		}
		if($ts=="" && isset($args['ts'])) $ts = $args['ts'];
		
		return $ts;
	}
	function fix_timestamps_for_date($dt){
		$e_query = lavu_query("select * from `lk_events` where `date`='[1]' order by `ts` asc",$dt);
		while($e_read = mysqli_fetch_assoc($e_query))
		{
			if(($e_read['date'] * 1) % 100 != 0)
			{
				$eargs = array("time"=>$e_read['time'],"date"=>$e_read['date'],"event_id"=>$e_read['id']);
				$ets = create_timestamp_from_date_and_time($eargs);
				lavu_query("update `lk_events` set `ts`='[1]' where `id`='[2]'",$ets,$e_read['id']);
			}
		}
	}
	function create_race($args){
		global $loc_id;
		//global $data_name;
		$args['loc_id']= $loc_id;
		//$args['date']= date('Y-m-d');
		$args['time_stamp']=create_timestamp_from_date_and_time($args);//gmmktime();
		if(!mysqli_num_rows(lavu_query("SELECT * FROM `lk_events` WHERE `date`='[1]' and `time`='[2]' and `trackid`='[3]' and `_deleted` != '1' ",$args['date'], $args['race_time'], $args['track_id']))){
			$query = lavu_query("INSERT INTO `lk_events` (`type`,`date`,`time`,`trackid`,`locationid`,`ts`) VALUES ('[race_type_id]','[date]','[race_time]','[track_id]','[loc_id]','[time_stamp]')", $args);
			if(lavu_dberror())
				return '{"error":"'.lavu_dberror().'"}';
			else
				return '{"id":"'.lavu_insert_id().'"}';
		}else{
			return '{"error":"A Race already exists at this time."}';	
		}
	}
	function create_sequence_race($args){
		global $loc_id;
		$counter        = 1;
		$args['loc_id'] = $loc_id;
		
		$sequence_info=mysqli_fetch_assoc(lavu_query("SELECT * FROM `lk_event_sequences` where `id`='[1]' ", $args['sequence_type_id']));
		$args['sequence']= $sequence_info['title'];
		$races= explode(",",$sequence_info['event_list']);
	
		$group_res= mysqli_fetch_assoc(lavu_query("SELECT MAX(`groupid`*1) as group_id FROM `lk_events`"));
		$args['group_id']= ($group_res['group_id']*1)+1;	//This is the new group id
		$time = $args['time'];
		foreach($races as $race_id){

			$args['ts']      = create_timestamp_from_date_and_time($args);//time();
			$args['heat']    = $counter;
			$args['race_id'] = $race_id;
			
			$num_laps_arr = mysqli_fetch_assoc(lavu_query("SELECT * FROM `lk_event_types` WHERE `id`='[1]' ", $race_id));
			$num_inserts  = round(($args['num_racers'] / $num_laps_arr['slots']),0);
			if( $num_inserts < 1)
				$num_inserts = 1;
				
			for($i = 0; $i < $num_inserts; $i++){
				
				lavu_query("INSERT INTO `lk_events` (`locationid`, `type`, `date`, `time`, `ts`, `trackid`, `_deleted`, `groupid`, `group_racer_count`,`heat`,`sequence`, `sequenceid`) VALUES ('[loc_id]', '[race_id]', '[date]', '[time]', '[ts]', '[track_id]',0, '[group_id]', '[num_racers]', '[heat]', '[sequence]', '[sequence_type_id]')", $args);
				
				$args['time']= increment_race_time($args['date'], $args['time'],$args['track_id'], $args['interval']);
			}
			$counter++;
			
		}
		$args['event_time']=$time;
		$args['event_date']= $args['date'];
		if(!isset($args['room_id']))
			$args['room_id']='';
		if(!isset($args['item_id']))
			$args['item_id']='';
		if(!isset($args['event_time']))
			$args['event_time']='';
		$id = lavu_insert_id();	
		lavu_query("INSERT INTO `lk_group_events` ( `event_date`,`event_time`,`duration`,`cust_name`,`cust_phone`,`cust_email`,`notes`,`loc_id`,`room_id`, `time_stamp`)VALUES ('[event_date]','[event_time]','','[cust_name]','[cust_phone]','[cust_email]','[notes]','[loc_id]','[room_id]', '[time_stamp]')", $args);
		
		if(lavu_dberror())
			return '{"error":"'.lavu_dberror().'"}';
		else
			return '{"id":"'.$id.'"}';
		
	}
	function increment_race_time($date, $time, $track_id, $interval){
		$new_time  = date("Hi",strtotime("+".$interval." minutes", mktime(substr($time,0, -2), substr($time,2))));
		$race_exists=mysqli_num_rows(lavu_query("SELECT * FROM `lk_events` where `date`='[1]' AND `time`='[2]' and `trackid`='[3]' and `_deleted`!='1'",$date, $new_time, $track_id));		
		if($race_exists){
			return increment_race_time($date, $new_time, $track_id, $interval);
		}else
			return $new_time;
	}
	function get_racers_for_race($args){
		$event_id   = $args['event_id'];	
		$race_data  = mysqli_fetch_assoc(lavu_query("SELECT * FROM `lk_events` WHERE `id`='[1]'", $event_id));
		$event_data = mysqli_fetch_assoc(lavu_query("SELECT * FROM `lk_event_types` WHERE `id`='[1]'", $race_data['type'])); 
		
		
		$query=lavu_query("SELECT * FROM `lk_event_schedules` WHERE `eventid`='[1]' AND `_deleted`='0' ", $event_id);
		
		$return_arr = array("race_data"=>$event_data);
		$temp_arr   = array();
		if( mysqli_num_rows($query)){
			while($result=mysqli_fetch_assoc($query)){
				$temp_arr[] = mysqli_fetch_assoc(lavu_query("SELECT * FROM `lk_customers` WHERE `id`='[1]'",$result['customerid']));
			}
			$return_arr['racer_data']= $temp_arr;
		} //else{
			$added_credits_custs = array();
			$cust_id_arr 		 = array();
			$temp_arr=array();

			$mindt = date("Y-m-d") . " 00:00:00";
			$racer_info_query = lavu_query("SELECT `id`, `racer_name`,`birth_date`,`f_name`,`l_name`, `credits` FROM `lk_customers` WHERE `last_activity`>'$mindt' and `credits` > 0 order by `last_activity` desc ");
				while($racer_info = mysqli_fetch_assoc($racer_info_query)){
					$added_credits_custs[$racer_info['id']]['id']		  = $racer_info['id'];
					$added_credits_custs[$racer_info['id']]['racer_name'] = $racer_info['racer_name'];
					$added_credits_custs[$racer_info['id']]['birth_date'] = $racer_info['birth_date'];
					$added_credits_custs[$racer_info['id']]['credits']    = $racer_info['credits'];
					$added_credits_custs[$racer_info['id']]['f_name']     = $racer_info['f_name'];
					$added_credits_custs[$racer_info['id']]['l_name']     = $racer_info['l_name'];
					$user_id_res = mysqli_fetch_assoc(lavu_query("SELECT `user_id` FROM `lk_action_log` WHERE `customer_id`='[1]' order by `time` desc limit 1  ", $racer_info['id']));
					
					$username= mysqli_fetch_assoc(lavu_query("SELECT * FROM `users` WHERE `id`='[1]'",$user_id_res['user_id']));
					$added_credits_custs[$racer_info['id']]['user_id']= $username['username'];
					
					$temp_arr[]= $added_credits_custs[$racer_info['id']];
				
				}
			
				$return_arr['racer_data_recent']= $temp_arr;

		//}
		return json_encode($return_arr);
		//return LavuJson::json_encode($return_arr);
	}
	function add_racer($args){
		
		// TODO check for no credit race
		
		$event_type=mysqli_fetch_assoc(lavu_query("SELECT * FROM `lk_events` where `id`='[1]' ", $args['event_id']));
		$event_laps= mysqli_fetch_assoc(lavu_query("SELECT * FROM `lk_event_types` WHERE `id`='[1]'", $event_type['type']));
		$num_credits = mysqli_fetch_assoc(lavu_query("SELECT `credits` FROM `lk_customers` WHERE `id`='[1]'", $args['racer_id']));
		if($num_credits['credits'] >= 1 || $event_laps['use_credits']=='No'){
			if($event_laps['use_credits']=='Yes' || $event_laps['use_credits']=='')
				lavu_query("UPDATE `lk_customers` SET `credits`= (`credits` - 1),`total_races`= (`total_races` + 1) where `id`='[1]'", $args['racer_id']);
			
			//if(mysqli_num_rows(lavu_query("SELECT * FROM `lk_event_schedules` where `eventid`='[1]' and `_deleted` !='1'", $args['event_id'])) < $event_laps['slots']){
				if(!mysqli_num_rows(lavu_query("SELECT * FROM `lk_event_schedules` where `eventid`='[1]' and `_deleted` !='1' and `customerid` ='[2]'",$args['event_id'], $args['racer_id'])))
					lavu_query("INSERT INTO `lk_event_schedules` (`eventid`,`customerid`,`group`,`time_stamp`)VALUES ('[1]','[2]','1','[3]')", $args['event_id'], $args['racer_id'], mktime());
				else
					return '{"result": "duplicate_racer"}';
			//}else
			//	return '{"result": "full_race"}';
			
			if (lavu_dberror()){
				return '{"error":"'.lavu_dberror().'"}';
			}else{
				$num_credits = mysqli_fetch_assoc(lavu_query("select * from `lk_customers` where `id`= '[1]'", $args['racer_id']));
				return '{"result":"success", "num_credits": "'.$num_credits['credits'].'"}';
			}
				
		}else{
			return '{"result": "no_credits id : '.$args['racer_id'].'"}';
		}
	}
	function remove_racer($args){
		lavu_query("UPDATE `lk_event_schedules`  SET `_deleted` ='1', `time_stamp`='[3]' WHERE `eventid`='[1]' AND `customerid`='[2]'  ", $args['event_id'], $args['racer_id'], mktime());
		$type = mysqli_fetch_assoc(lavu_query("select `lk_events`.`type` from `lk_event_schedules` LEFT JOIN `lk_events` ON `lk_event_schedules`.`eventid` = `lk_events`.`id` where `lk_event_schedules`.`eventid`='[1]' limit 1",$args['event_id']));
		
		if(mysqli_num_rows(lavu_query("select * from `lk_event_types` where `id`='[1]' and `use_credits`='Yes' OR `use_credits`='' ", $type['type'])))
			lavu_query("UPDATE `lk_customers` SET `credits`= (`credits` + 1), `total_races`= (`total_races` - 1) where `id`='[1]'", $args['racer_id']);
		if (lavu_dberror()){
			return '{"error":"'.lavu_dberror().'"}';
		}else{
			$num_credits = mysqli_fetch_assoc(lavu_query("select * from `lk_customers` where `id`= '[1]'", $args['racer_id']));
			return '{"result":"success", "num_credits": "'.$num_credits['credits'].'"}';
		}
			

	}
	function move_race($args){
		$set_ts = create_timestamp_from_date_and_time($args);
	    lavu_query("UPDATE `lk_events` set `time`='[1]', `ts`='[4]', `trackid`='[3]' WHERE `id`='[2]'", $args['time'], $args['event_id'], $args['track_id'], $set_ts);
		if (lavu_dberror()){
			return '{"error":"'.lavu_dberror().'"}';
		}else
			return '{"status":"success"}';

    }
    function get_month_races($args){
		$return_arr = array();
		$year  		= $args['year'];
		$month		= ($args['month']*1)+1; 
		
		if($month==12){
			$next_month=1;
			
		}else{
			$next_month=$month+1;
		}
		$date  	   = date("Y-m-d",strtotime("$year-$month-1")); 
		$next_date = date("Y-m-d",strtotime(($year+1)."-$next_month-1")); 
		//echo "date: ". $date. " next date: ". $next_date;
		$query= lavu_query("select `event_time`, `duration`,`event_date` from `lk_group_events` where `event_date` >= date('[1]') and `event_date` < date('[2]') and `_deleted` !='1'", $date, $next_date);
		//echo "select `event_time`, `duration`,`event_date` from `lk_group_events` where `event_date` >= date('$date') and `event_date` < date('$next_date') and `_deleted` !='1'";
		while($res= mysqli_fetch_assoc($query)){
			$firstDay = "_".date('j',strtotime($res['event_date']));
			if(!isset($return_arr[$firstDay]))
				$return_arr[$firstDay]=array();
			array_push($return_arr[$firstDay], $res);
		}
		return json_encode($return_arr);
		//return  LavuJson::json_encode($return_arr);
    }
    function create_room_event($args){
	    global $loc_id;
	    $args['loc_id']= $loc_id;
	    if(!isset($args['notes']))
	    	$args['notes']='';
	    $args['notes']= rawurldecode($args['notes']);
	    $args['time_stamp']= time();
		if(!isset($args['room_id']))
			$args['room_id']='';
		if(!isset($args['item_id']))
			$args['item_id']='';
		if(!isset($args['event_time']))
			$args['event_time']='';
		lavu_query("INSERT INTO `lk_group_events` (`item_id`, `title`,`event_date`,`event_time`,`duration`,`cust_name`,`cust_phone`,`cust_email`,`notes`,`loc_id`,`room_id`, `time_stamp`)VALUES 
												  ('[item_id]', '[title]','[event_date]','[event_time]','[duration]','[cust_name]','[cust_phone]','[cust_email]','[notes]','[loc_id]','[room_id]', '[time_stamp]')", $args);
		if(lavu_dberror())
			return '{"error":"'.lavu_dberror().'"}';
		else
			return '{"id":"'.lavu_insert_id().'"}';    
	}
    function remove_event($args){
	    $id   = $args['event_id'];
	    $type = $args['type'];
	    if($type=="room")
	    	lavu_query("UPDATE `lk_group_events` SET `_deleted`='1' WHERE `id`='[1]' LIMIT 1", $id);
	    else if($type=='track'){
	    
			$set_time = create_timestamp_from_date_and_time($args);
	    	lavu_query("UPDATE `lk_events` SET `_deleted`='1' WHERE `id`='[1]' LIMIT 1", $id);
			$query = lavu_query("SELECT * FROM `lk_event_schedules` where `eventid`='[1]' and `_deleted` != '1'", $id);
			while($res = mysqli_fetch_assoc($query)){
				lavu_query("UPDATE `lk_customers` set `credits`= (`credits` + 1),`total_races`= (`total_races` - 1), `_deleted`='1' where `id`='[1]' ", $res['customerid']);
			}
	    }
	    if(lavu_dberror())
			return '{"error":"'.lavu_dberror().'"}';
		else
			return '{"result":"success"}';
	    
    }
    function get_room_details($args){
    	$res=mysqli_fetch_assoc(lavu_query("SELECT * FROM `lk_group_events` where `id`='[1]'",  $args['event_id']));
		if(!empty($res['checklist'])){
			$ids_and_names= explode("|",$res['checklist']);
			$names= explode(",", $ids_and_names[1]);
			$res['checklist']=$names;
		}
		$res['notes'] = rawurlencode($res['notes']);
		$query = lavu_query("SELECT `value` FROM `config` WHERE `setting` ='Group Event Checklist' and `type`='collection' and `_deleted` !='1'");
		
		$res['collections']= array();
		while($collections = mysqli_fetch_assoc($query)){
			$res['collections'][]= $collections['value'];
		}
		$res['types'] = array();
		$types_query  = lavu_query("SELECT `name`,`id` FROM `menu_items` where `hidden_value2` = 'group event' and `_deleted` != '1' ");
		while($types_res = mysqli_fetch_assoc($types_query)){
			$res['types'][] = array("value"=>$types_res['id'], "title"=>$types_res['name']);
		}
		$res['csr_list']= array();
		$csr_query=lavu_query("SELECT * FROM `users` WHERE `_deleted`!='1'");
		while($csr_res = mysqli_fetch_assoc($csr_query)){
			$res['csr_list'][]=array("title"=>$csr_res['f_name']." ".$csr_res['l_name'], "value"=>$csr_res['id']); 
		}
		$seq_query = lavu_query("SELECT * FROM `lk_event_sequences` WHERE `_deleted`!='1' ");
		
		return json_encode($res);
    }
    function update_room_event($args){
    	$args['ts']= time();
    	$i		   = 1; 
    	$checklist = array();
    	$id_str    = '';
    	$num_checks= mysqli_num_rows(lavu_query("SELECT `value` FROM `config` WHERE `setting` ='Group Event Checklist' and `type`='collection' and `_deleted` !='1'"));
    	while($i < $num_checks){
	    	if(array_key_exists("check_list_".$i, $args)){
		    	$conf_id_arr = mysqli_fetch_assoc(lavu_query("SELECT `id` FROM `config` where `setting`= 'Group Event Checklist' and `value`='[1]' and `type`='collection' and `_deleted` !='1'", $args["check_list_".$i])); 
				$id_str.="[".$conf_id_arr['id']."]";
				$checklist[]=$args["check_list_".$i];
	    	}
	    	$i++;
    	}
    	$args['checklist']= $id_str."|".implode(",",$checklist);
    	$args['notes']= rawurldecode($args['notes']);
	    lavu_query("UPDATE `lk_group_events` set `item_id`='[event_type]',`time_stamp`='[ts]', `title`='[title]', `duration`='[duration]', `event_time`='[event_time]',`cust_name`='[cust_name]', `cust_email`='[cust_email]', `cust_phone`='[cust_phone]', `cust_email`='[cust_email]', `notes`='[notes]',`checklist`='[checklist]',`cust_rep`='[csr]' WHERE `id`='[event_id]' LIMIT 1", $args);
	     if(lavu_dberror())
			return '{"error":"'.lavu_dberror().'"}';
		else
			return '{"result":"success"}';
	    
    }
    function merge_racers($args){
	    
	    $start_id = $args['start_id'];
	    $end_id   = $args['end_id'];
	    $query= lavu_query("SELECT * FROM `lk_event_schedules` where `eventid` = '[1]'", $start_id);
		if(lavu_dberror())
			echo lavu_dberror();
		//TODO: add no credit race check 	
	    while($res = mysqli_fetch_assoc($query)){
		  	$num_credits = mysqli_fetch_assoc(lavu_query("SELECT `credits` FROM `lk_customers` WHERE `id`='[1]'", $res['customerid']));
		  	if(!mysqli_num_rows(lavu_query("SELECT * FROM `lk_event_schedules` where `eventid`='[1]' and `customerid`='[2]' and `_deleted`!='1' ", $end_id, $res['customerid'])) && $num_credits['credits'] >= 1){
		   		lavu_query("INSERT INTO `lk_event_schedules` (`eventid`,`customerid`,`group`,`time_stamp`)VALUES ('[1]','[2]','1','[3]')", $end_id, $res['customerid'], mktime());
		   		lavu_query("UPDATE `lk_customers` SET `credits`= (`credits` - 1) ,`total_races`= (`total_races` + 1) where `id`='[1]'", $res['customerid']);
		   	}
	    }
	   	return '{"result":"success"}';   
    }
    function save_note($args){
	    lavu_query("UPDATE `lk_events` SET `notes`='[1]' where `id`='[2]'",rawurldecode($args['note']), $args['event_id']);
	    if(lavu_dberror())
			return '{"error":"'.lavu_dberror().'"}';
		else
			return '{"result":"success"}';
	    
    }
	function get_all_races_for_update($args){
		$query= lavu_query("SELECT * FROM `lk_events` WHERE `_deleted`='0' and `date`='[1]'",$args['date']);
		$return_arr = array();
		while($res = mysqli_fetch_assoc($query)){
			$return_arr[$res['id']+""]= array("ms_start"=>$res['ms_start'], "ms_end"=>$res['ms_end']);
		}
		//error_log(json_encode($return_arr));
		return json_encode($return_arr);
	}
	/*
	function output_js($data, $var_name){
		echo "<script type='text/javascript'>";
		echo "var ".$var_name." = ".$data.";";
		echo "</script>";
	}*/

	/*
	function initialize_data(){
		$tracks			 = get_active_tracks();		
		$upcoming_races  = get_upcoming_races();
		output_js($tracks, "tracks");
		output_js($upcoming_races,"upcoming_races");		
	}*/
?>