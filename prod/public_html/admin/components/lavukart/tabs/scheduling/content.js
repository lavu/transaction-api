
	var data_url		= "./get_data.php";
	var screen_size	    = 800;
	var interval    	= parseInt(performAJAX(data_url, "function=get_time_interval",false,"POST").time_interval);
	var _start_touch_id = 0;
	var _start_x_loc    = 0;
	var _start_y_loc    = 0;
	var _time_start		= 0;
	var _is_double_tap  = false;
	var _num_racers 	= "";
	var _csr_list		= new Array();
	var _debugging      = false;
	var current_ajax    = null;
	var _room_width		='0px';
	var _track_width 	='0px';
	var day_names= new Array();
		day_names[0] = 'Sunday'; 
		day_names[1] = 'Monday'; 
		day_names[2] = 'Tuesday'; 
		day_names[3] = 'Wednesday'; 
		day_names[4] = 'Thursday'; 
		day_names[5] = 'Friday'; 
		day_names[6] = 'Saturday'; 
		day_names[7] = 'Sunday'; 
	var month_names=new Array();
		month_names[0]="January";
		month_names[1]="Febuary";
		month_names[2]="March";
		month_names[3]="April";
		month_names[4]="May";
		month_names[5]="June";
		month_names[6]="July";
		month_names[7]="August";
		month_names[8]="September";
		month_names[9]="October";
		month_names[10]="November";
		month_names[11]="December";
	
	var cal = get_cal();
	function initialize(date){

		if(!date){
			var d   = new Date();	
			var m   = (d.getMonth()+1);
			var day = d.getDate();
			
			date= d.getFullYear()+"-"+prepad(m)+"-"+prepad(day);
			//console.log(date);
		}
		//console.log("The date is: "+date);
		document.getElementsByClassName('content_container')[0].setAttribute("date",date);
		
		create_rows();
		get_races(date);
		get_rooms(date);
		initialize_listeners();
		find_new_races(1);
		
		var konamiCode=[38,38,40,40,37,39,37,39];
		var counter=0;
		$(document).bind("keyup",function(e){

			if(counter==2 && e.keyCode==38){}

			else if(konamiCode[counter]==e.keyCode)
				counter++;
			else
				counter=0;

			if(counter==konamiCode.length){
				console.log('Debugging activated');
				_debugging=true;
				initialize_listeners();
			}
		});
	}
	function create_rows(){

		var loc_data    = performAJAX(data_url, "function=get_tracks_and_rooms",false,"POST");
		calculate_width(loc_data);
		var container   = document.getElementsByClassName('content_container')[0];
			container.innerHTML='';
			 
		for(var hours = 0; hours < 18; hours++){
			var h = ((7+hours)%12)+1;
			for(minutes = 0; minutes < 60; minutes+=interval){
				var m=minutes;
				var am_pm= 'am';
				
				if( minutes < 10)
					m= "0"+minutes;
				if( hours >= 4 && hours < 16)
					 am_pm='pm';
				
				var row_elem  = document.createElement("div");
				row_elem.className='table_row';
				
				
				var time_span = document.createElement("div");
				
				var temp_hr= h;		// this will allow me to reference the Row by the time
				if( am_pm == 'pm' && temp_hr!=12)
					temp_hr+=12;	
				
				if(hours >= 16)
					temp_hr=hours-16;
					
				if(h==12 && am_pm=='am')
					temp_hr=0;
				
				if(parseInt(temp_hr) < 10)
					temp_hr="0"+temp_hr;
				
				var time_stamp= temp_hr+""+m;
				//if(time_stamp=='2350')
				//  console.log('your mom');
				
				time_span.innerHTML= "<div class='time_positioner'>"+h + ":" + m + " "+ am_pm+"</div>";

				row_elem.id= time_stamp;

				time_span.className='time_class';				
				row_elem.appendChild(time_span);
				
				for(var t_counter=0; t_counter<loc_data.tracks.length; t_counter++){
					var e= create_box("track",loc_data.tracks[t_counter].id);
					//e.style.width=calculate_width(loc_data, 'track');
					e.style.width=_track_width;
					row_elem.appendChild(e);
				}
				for(r_counter=0; r_counter<loc_data.rooms.length; r_counter++){
					var e= create_box("room",loc_data.rooms[r_counter].id);
					//var w= calculate_width(loc_data,'room');
					e.style.width=_room_width;
					row_elem.appendChild(e);
				}
				container.appendChild(row_elem);
			}		
		}	
	}
	function get_races(date){
		
		var races_ajax= performAJAX(data_url,"function=get_races&date="+date,false,"POST");
		//console.log(races_ajax);
		for( var i=0; i< races_ajax.length; i++){
			if( document.getElementById(races_ajax[i].time)){
				draw_track(races_ajax[i].time, races_ajax[i].trackid, races_ajax[i].id, races_ajax[i]);
			}
		}
	}
	function get_rooms(date){
		
		var rooms_ajax= performAJAX(data_url,"function=get_rooms&date="+date,false,"POST");
		
		for( var i=0; i< rooms_ajax.length; i++){
			var duration = rooms_ajax[i].duration;
			
			if(rooms_ajax[i].event_time.length==3)
				rooms_ajax[i].event_time= "0"+rooms_ajax[i].event_time;
			
			var index= get_column_offset(rooms_ajax[i].event_time, "room_id", rooms_ajax[i].room_id);

			var current_time = rooms_ajax[i].event_time;
			var first = true;
			var ele   = null;
			
			if(duration == interval){
				var inner_elem= document.createElement("div");
					inner_elem.className='room_event_start_end_inner';
					inner_elem.innerHTML=rooms_ajax[i].title;
					
					//inner_elem.addEventListener("touchstart", touch_start);
					//inner_elem.addEventListener("touchend",   touch_end);
					
				ele= document.getElementById(current_time).childNodes[index];
				ele.className += " room_event_start_end room_event_"+rooms_ajax[i].id;
				ele.removeChild(ele.childNodes[0]);
				ele.appendChild(inner_elem);
	
			}else if(duration == 0 || rooms_ajax[i].order_id || duration=='' ){
				var ele= document.createElement("div");
				var inner_elem= document.createElement("div");
					inner_elem.className='room_event_start_end_inner';
				console.log(rooms_ajax[i].title);
				if( rooms_ajax[i].title =='[null]')
					rooms_ajax[i].title = 'EDIT ME';
					inner_elem.innerHTML="["+rooms_ajax[i].item_id+"] "+rooms_ajax[i].title;
					add_listener_to_cell(ele);
				ele.className += " room_event_start_end room_event_"+rooms_ajax[i].id+" room_event_divider";
				add_listener_to_cell(ele);
				ele.appendChild(inner_elem);					
				ele.style.width  = _track_width;
				ele.style.left   = "80px";
				ele.style.position = "relative";
				//ele.style.margin = "0 auto";
				ele.style.border = "";

				ele.style.backgroundColor = ""; 
				document.getElementsByClassName("content_container")[0].insertBefore(ele, document.getElementById(current_time))
				
			}
			else{
				while(duration > 0){
					var inner_elem= document.createElement("div");
					if(document.getElementById(current_time))
						ele= document.getElementById(current_time).children[index];
					else {
						console.log("current_time issue time was: ". current_time);
						continue;
					
					}
					ele.className+=" room_event_"+rooms_ajax[i].id;
					if(first){
						
						inner_elem.className = 'room_event_start_inner';
						inner_elem.innerHTML = rooms_ajax[i].title;
						ele.className		+= " room_event_start"; 
						first=false;
					}else{
						inner_elem.className='room_event_mid_inner';
						ele.className+=" room_event_mid";	
						inner_elem.innerHTML='.';
						
					}

					ele.removeChild(ele.childNodes[0]);
					ele.appendChild(inner_elem);
					
					current_time=document.getElementById(current_time).nextSibling.id;
					duration=parseInt(duration)-parseInt(interval); 
					
				}
				ele.className="room_cell room_event_end room_event_"+rooms_ajax[i].id;
				inner_elem.className='room_event_end_inner';
				inner_elem.innerHTML='.';				
				ele.removeChild(ele.childNodes[0]);
				ele.appendChild(inner_elem);
			}
		}
	}
	function draw_track(time,trackid, eventid, data){
		if(document.getElementById(time)){
			if(!document.getElementById(time)){
				if(time.length ==3)
					time = "0"+time;
			
			
			}
			
			for( var j=0; j<document.getElementById(time).childNodes.length; j++){
				var elem= document.getElementById(time).childNodes[j];
				if(elem.hasAttribute("track_id") && elem.getAttribute("track_id")==trackid){
					elem.className='track_cell_with_contents';
					elem.setAttribute("event_id",eventid);
					if(data.groupid){
						create_sequence_cell(elem, data);
					}else{
						create_normal_cell(elem, data);
					}
				} 
			}	
		}else{
			console.log("eventid: "+ eventid+ "'s time does not exist. time was: "+ time);
		}
	}
	function create_sequence_cell(elem, ajax){

		elem.innerHTML = '';
		var vert_line = document.createElement("div");
			vert_line.className				= 'vert_line';
			vert_line.style.color           = default_color(ajax.groupid+ajax.group_racer_count+ajax.trackid);
			vert_line.style.backgroundColor = default_color(ajax.groupid+ajax.group_racer_count+ajax.trackid);
			vert_line.innerHTML				= '.';
		var current_date = new Date();
		var hours   = current_date.getHours();
		var minutes = current_date.getMinutes();
		
		if( hours.length == 1)
			hours= "0"+hours; 
		if((""+minutes).length ==1)
			minutes="0"+minutes;
		var time = elem.previousElementSibling.parentNode.id;
		var cur_time = hours+""+minutes;			
		/*
		if((parseInt(cur_time) - parseInt(time)) > 0 && (parseInt(cur_time) - parseInt(time)) < 10){
			elem.className+=" current_race";
			elem.style.backgroundColor="#aecd37";
		}else if( parseInt(time) < parseInt(cur_time))
			elem.className+=" past_race";
		*/
		if(ajax.ms_end == '' && ajax.ms_start!=''){
				elem.className+=" current_race";
		elem.style.backgroundColor="#aecd37";
		}else if (ajax.ms_end != '' && ajax.ms_start!=''){
			elem.style.backgroundColor= '';
			elem.className+=" past_race";
		}

		if( ajax.abbreviation)
			elem.innerHTML= vertical_line+ajax.abbreviation;
		else{	
			var mover= document.createElement("div");
			mover.className = 'race_type';
			mover.innerHTML = make_abbreviation(ajax.evt_type);
			mover.addEventListener("touchend", start_move);
			if(_debugging)
				mover.addEventListener("click", start_move);
				
			elem.appendChild(vert_line);
			elem.appendChild(mover);
		}if( ajax.color)
			elem.style.color=ajax.color;
		
		var num_racers= document.createElement("div");
		if(ajax.ms_end.length == 0){
			num_racers.className = 'num_racers_active';
			var num_pos = document.createElement("p");
				num_pos.className = 'number_positioner';
				num_pos.innerHTML = ajax.num_racers_remaining;
			num_racers.appendChild(num_pos);
			
		}else{
			num_racers.className = 'num_racers_inactive';
			var num_pos = document.createElement("p");
				num_pos.className = 'number_positioner';
				num_pos.innerHTML = ajax.num_racers_remaining;
			num_racers.appendChild(num_pos);
		}
		
		var notes_area= document.createElement("div");
			notes_area.className = 'note_container';
			notes_area.style.width =  (parseInt(elem.style.width)-120);
		
		var input = document.createElement("input");
			input.type="text";
			input.className = 'note_input';
			
		notes_area.appendChild(input);
		
		if(ajax.notes.length!=0){
			ajax.notes= decodeURIComponent(ajax.notes);
			if( ajax.notes.length > 50)
				ajax.notes= ajax.notes.substr(0, 47)+"...";
			
			var note_container = document.createElement("div");
				note_container.className='note_container_inner';
				note_container.innerHTML= ajax.notes;

			notes_area.appendChild(note_container);
		}
		elem.appendChild(num_racers);
		elem.appendChild(notes_area);

	}
	function create_normal_cell(elem, ajax){
		elem.innerHTML = '';
		var containing_cell = document.createElement("div");
			containing_cell.className = "containing_cell";
			
		var race_type = document.createElement("div");
			race_type.className = 'race_type';
		
		var current_date = new Date();
			var hours   = current_date.getHours();
			var minutes = current_date.getMinutes();
			
			if( hours.length == 1)
				hours= "0"+hours; 
			if((""+minutes).length ==1)
				minutes="0"+minutes;
			var time = elem.previousElementSibling.parentNode.id;
			var cur_time = hours+""+minutes;			
			/*
			if((parseInt(cur_time) - parseInt(time)) > 0 && (parseInt(cur_time) - parseInt(time)) < 10){
				elem.className+=" current_race";
				//elem.style.backgroundColor = "#aecd37";
				elem.style.backgroundColor="#aecd37";
			
			}else if( parseInt(time) < parseInt(cur_time))
				elem.className+=" past_race";
			 	*/	
			if(ajax.ms_end == '' && ajax.ms_start!=''){
				elem.className+=" current_race";
			elem.style.backgroundColor="#aecd37";
			}else if (ajax.ms_end != '' && ajax.ms_start!=''){
				elem.style.backgroundColor= '';
				elem.className+=" past_race";
			} 	
			 	
		containing_cell.addEventListener("touchend", start_move);
		
		if(_debugging)
			containing_cell.addEventListener("click", start_move);	
		
		if(ajax.color)
			containing_cell.style.backgroundColor = ajax.color;
		else
			containing_cell.style.backgroundColor = default_color(ajax.type)
		
		
		if( ajax.abbreviation)
			race_type.innerHTML = ajax.abbreviation;
		else
			race_type.innerHTML = make_abbreviation(ajax.evt_type);
		
		containing_cell.appendChild(race_type);
		elem.appendChild(containing_cell);
		
		if( ajax.color)
			elem.style.color=ajax.color;
		
		var img = document.createElement("div");
		var num_positioner= document.createElement("p");
			num_positioner.innerHTML = ajax.num_racers_remaining;
			num_positioner.className = 'number_positioner';
		if(ajax.ms_end.length == 0)
			img.className = 'num_racers_active';
		else
			img.className = "num_racers_inactive";
		img.appendChild(num_positioner);
		
		var notes_area= document.createElement("div");
			notes_area.className = 'note_container';
			notes_area.style.width =  (parseInt(elem.style.width)-120);
		/*	
		var notes_area=document.createElement("div");
			notes_area.style.width=(parseInt(elem.style.width)-100);
		*/	
		var input = document.createElement("input");
			input.type="text";
			input.className = 'note_input';
			//input.addEventListener("focus", open_note_editor);
			//input.addEventListener("blur", close_note_editor);
	
		notes_area.appendChild(input);
		
		if(ajax.notes.length!=0){
			ajax.notes= decodeURIComponent(ajax.notes);
			if( ajax.notes.length > 50)
				ajax.notes= ajax.notes.substr(0, 47)+"...";
			
			var note_container = document.createElement("div");
				note_container.className='note_container_inner';
				note_container.innerHTML= ajax.notes;

			notes_area.appendChild(note_container);
		}
		elem.appendChild(img);
		elem.appendChild(notes_area);
	}
	function make_abbreviation(type){
	
		var clean= clean_type(type);
		var	num_words= clean.length;
		if(num_words==1)
			return type.substring(0, 2);
		else{
			var abbr='';
			for( var i=0; i<clean.length; i++)
				abbr+=clean[i].substring(0,1);
			return abbr;
		}
	}
	function default_color(id) {
		if( id){
			var r = (id*73 + 0)  % 256;
			var g = (id*100 + 0) % 256;
			var b = (id*53 + 200)% 256;
		} 
		return "rgb("+r+","+g+","+b+")";
	}
	function clean_type(type){
		if(type){
			s = type;
			s = s.replace(/(^\s*)|(\s*$)/gi,"");
			s = s.replace(/[ ]{2,}/gi," ");
			s = s.replace(/\n /,"\n");
			return s.split(' ');
		}else{
			return ["no","name"];
		}
	}	
	function display_calendar(year, month, day,type){
		if(year && month >=0 )
			draw_calendar(year, month, day, type);
		else{
			var date= new Date();
			draw_calendar(date.getFullYear(),date.getMonth(), date.getDay(), "month");
		}
	}
	function draw_calendar(year, month, day, type){
			
		var elem = document.getElementById("overlay");

		if( document.getElementsByClassName("cal_table")[0])
			document.getElementsByClassName("cal_table")[0].parentNode.removeChild(document.getElementsByClassName("cal_table")[0]);
		if( type == 'month' ){
			table = get_month_view(year, month);
		}else if(type == "week")
			table = get_week_view(year, month, day);
		else if (type == 'day')
			table = get_day_view(year, month,day);	
		var close = document.getElementById("close_overlay");
		elem.innerHTML ='';
		elem.appendChild(close)
		var previous_month_btn = document.createElement("span");
		var previous_year      = parseInt(year);
		var previous_month     = 0;
		
		if(month==0){
			previous_year = parseInt(year)-1;
			previous_month=11;
		}else{
			previous_month= parseInt(month)-1;
		}
		
		if(previous_year > (new Date().getFullYear()-2)){ 
			previous_month_btn.setAttribute("year", 	 previous_year );
			previous_month_btn.setAttribute("month",	 previous_month);
			
			if(_debugging){
				if(type=='month')
					previous_month_btn.addEventListener("click", go_to_month);
				else if (type=='week')
					previous_month_btn.addEventListener("click", go_to_week);
				else if( type=='day')
					previous_month_btn.addEventListener("click", go_to_day);
			}
			if(type=='month')
				previous_month_btn.addEventListener("touchend",go_to_month);
			else if (type=='week')
				previous_month_btn.addEventListener("touchend",go_to_week);
			else if( type=='day')
				previous_month_btn.addEventListener("touchend",go_to_day);
				
			previous_month_btn.addEventListener("touchend",go_to_month );
			previous_month_btn.innerHTML='&#x25C0;';
			previous_month_btn.className='previous_month_btn';
			previous_month_btn.style.float='left';
			elem.appendChild(previous_month_btn);
		}
		//next_month_btn
	
	
		var next_month_btn	= document.createElement("span");
		var next_year		= parseInt(year);
		var next_month		= 0;
		if(month == 11){		
			next_year= parseInt(year) + 1;
			next_month=0;
		
		}else{
			next_month= parseInt(month)+1;
		}
		
		if(next_year < (new Date().getFullYear()+2)){ 
			next_month_btn.setAttribute("year", next_year	    );
			next_month_btn.setAttribute("month",next_month	    );
			
			if(_debugging){
			
				if( type=='month')
					next_month_btn.addEventListener("click", go_to_month);
				else if( type=='week'){
					next_month_btn.addEventListener("click", go_to_week);
				}else if( type=='day'){
					next_month_btn.addEventListener("click", go_to_day);
				}
			}
			if( type=='month')
				next_month_btn.addEventListener("touchend", go_to_month);
			else if( type=='week'){
				next_month_btn.addEventListener("touchend", go_to_week);
			}else if( type=='day'){
				next_month_btn.addEventListener("touchend", go_to_day);
			}
			next_month_btn.innerHTML='&#x25B6;';	
			next_month_btn.className='next_month_btn';
			next_month_btn.style.float='right';
		
				elem.appendChild(next_month_btn);
			
		}
		
		var inner_cal_container=  document.getElementsByName("cal_container");
	
		if(inner_cal_container.length){
			inner_cal_container.innerHTML ='';
		}else{
			inner_cal_container= document.createElement("div");
			inner_cal_container.className= "cal_container";
			inner_cal_container.innerHTML ='';
			elem.appendChild(inner_cal_container);
		}
		inner_cal_container.appendChild(table);
		
		var view_container = document.createElement("div");
			view_container.className = 'calendar_view_container';
		
		var month_btn = document.createElement("div");
			month_btn.className = 'cal_view_btn month_btn';
			month_btn.innerHTML = 'Month';
			if(_debugging)
				month_btn.addEventListener("click", month_view);
			month_btn.addEventListener("touchend", month_view);
			
			view_container.appendChild(month_btn);
			
		var week_btn = document.createElement("div");
			week_btn.className = 'cal_view_btn week_btn';
			week_btn.innerHTML = "Week";
			if(_debugging)
				week_btn.addEventListener("click",week_view);
			week_btn.addEventListener("touchend", week_view);
			view_container.appendChild(week_btn);
			
		var day_btn = document.createElement("div");
			day_btn.className = 'cal_view_btn day_btn';
			day_btn.innerHTML = "Day";
			if(_debugging)
				day_btn.addEventListener("click",day_view);
			day_btn.addEventListener("touchend", day_view);
			view_container.appendChild(day_btn);
		
		if( type=='month')
			month_btn.setAttribute("selected");
		else if( type =='week')
			week_btn.setAttribute("selected");
		else if( type=='day')
			day_btn.setAttribute("selected");
		elem.appendChild(view_container);
		
		document.getElementById("overlay_container").style.display='block';	
		
		//elem.appendChild(close);
	}
	function get_month_view(year, month){
		var overlay = document.getElementById("overlay");
			overlay.style.width  = '';
			overlay.style.left	  = '';
			overlay.style.height = '550px';
		var month_str    = (parseInt(month)+1)+ "";
		var month_races  = get_month_races(year, month);
		var selected_day = get_selected_day();
		if(month_str.length < 2 ){
			month_str = "0" + month_str;
		}
		var start_day = new Date(year + "-" + month_str + "-01").getDay()+1;
		var table 	  = document.createElement("table");
		var d		  = new Date();
		var today	  = d.getDate();
	
		var month_name_row="<thead><tr><td colspan= '7' class='month_name'>"+month_names[month]+" "+year+"</td></tr></thead>";
		table.innerHTML=month_name_row;
	
		table.className='cal_table';
		//create header
		var head_row 	  = document.createElement("tr");

		for(var i = 0; i < 7; i++){
			var th= document.createElement("th");
			th.className='cal_header';
			th.appendChild(document.createTextNode(day_names[i]));
			head_row.appendChild(th);
		}
			table.appendChild(head_row);
		
		//create empty days for starting
		var tr = document.createElement("tr");
		for( var i = 0; i < start_day; i++){
			var td = document.createElement("td");
			td. className='cal_cell';
			tr.appendChild(td);
		}
		
		//create days
		for(var d = 1; d <= cal[year][month]; d++){
			if((start_day % 7) == 0){
				table.appendChild(tr);
				tr= document.createElement("tr");
			}
			start_day++;
			var td= document.createElement("td")
			td.className='cal_cell';
		
			td.setAttribute("day", 	 d    );
			td.setAttribute("year",  year );
			td.setAttribute("month", month);
			if(_debugging)
				td.addEventListener("click", change_day);
			td.addEventListener("touchend", change_day);
			td.style.fontWeight= 'bold';
			if(d == today && new Date().getMonth() == month && new Date().getFullYear() == year)
				td.className+=" current_date";
			if(d == (new Date(get_selected_day()).getDate()+1) && new Date().getMonth() == month && new Date().getFullYear() == year)
				td.className+=" selected_date";
			
			td.appendChild(document.createTextNode(d+''));
			
			if(month_races["_"+d]){
				
				var info = month_races["_"+d].length;
				
				var inn  = document.createElement("div");
					inn.style.color = "red"; 
					inn.style.textAlign= "center";
					inn.style.marginTop="-10px";
					inn.style.fontWeight="normal";
					inn.innerHTML   = "<br>"+info; 
				td.appendChild(inn);
				
			} 
			
			tr.appendChild(td);
		} 
		table.appendChild(tr);
		return table;
	}
	function get_week_view(){
		var d  =new Date(get_selected_day()+"T00:00:00Z");
		d.setDate(d.getDate()+1);
		var week_start_day = new Date(d.setDate(d.getDate() - d.getDay()));
		var month 	  	   = week_start_day.getMonth();
		var year  	  	   = week_start_day.getFullYear();
		var day       	   = week_start_day.getDate();
		
		var overlay = document.getElementById("overlay");
			overlay.style.width  = '';
			overlay.style.left	 = '';
			overlay.style.height = '550px';
		
		var start_day 	 = 0;
		var month_str    = (parseInt(month)+1)+ "";
		var month_races  = get_month_races(year, month);
		var selected_day = get_selected_day();
		
		if(month_str.length < 2 )
			month_str = "0" + month_str;

		var table 	  = document.createElement("table");
		var d		  = new Date();
		var today	  = d.getDate();
	
		var month_name_row="<thead><tr><td colspan= '7' class='month_name'>"+month_names[month]+" "+year+"</td></tr></thead>";
		table.innerHTML=month_name_row;
	
		table.className='cal_table';
		//create header
		var head_row 	  = document.createElement("tr");

		for(var i = 0; i < 7; i++){
			var th= document.createElement("th");
			th.className='cal_header';
			try{
				cal[year][month];
			}catch(err){
				console.log(err);
			}
			if(day > cal[year][month])
				day = 1;
			
			if( i==0){
				th.appendChild(document.createTextNode("Time"));
				head_row.appendChild(th);
				th= document.createElement("th");
				th.className='cal_header';
			}
			th.appendChild(document.createTextNode(day_names[i].substring(0,3) + " ("+ (day++) + ")"));
			head_row.appendChild(th);
		}
			table.appendChild(head_row);
		
		
		//create days
		for(var hours = 0; hours < 18; hours++){
			var h = ((7+hours)%12)+1;
			for(minutes = 0; minutes < 60; minutes+=interval){
				var m=minutes;
				var am_pm= 'am';
				
				if( minutes < 10)
					m= "0"+minutes;
					
				if( hours >= 4 && hours < 16)
					 am_pm='pm';
				
				var temp_hr= h;		
				
				if( am_pm == 'pm' && temp_hr!=12)
					temp_hr+=12;	
				
				if(hours >= 16)
					temp_hr=hours-16;
					
				if(h==12 && am_pm=='am')
					temp_hr=0;
				
				if(temp_hr.toString().length == 1)
					temp_hr="0"+temp_hr;
				
				var time_stamp= temp_hr+""+m;
			
				var td= document.createElement("td")
					td.className='week_cal_time_cell';
					
					if( h.toString().length ==1)
						td.innerHTML = "0"+h+':'+m+" "+ am_pm;
					else
						td.innerHTML =h+':'+m+" "+ am_pm;
					td.setAttribute("time_stamp",time_stamp);
				var tr = document.createElement("tr");	
					tr.appendChild(td);
					
				var cur_day =week_start_day.getDate();
				for( var i=0; i < 7; i++){
					var long_td = document.createElement("td");
						long_td.className = "week_cal_cell";
						
					if( month_races["_"+(cur_day+i)]){
						for(j in month_races["_"+(cur_day+i)]){
 
							if( ( month_races["_"+(cur_day+i)][j].event_time == time_stamp) && parseInt(month_races["_"+(cur_day+i)][j].duration) > 0){

								month_races["_"+(cur_day+i)][j].duration -= interval;
								var temp = month_races["_"+(cur_day+i)][j].event_time;
								var hr  = parseInt(temp.substring(0,2));
								var min = parseInt(temp.substring(2));
								if(min + interval >=60){
									min = (min+interval)%60;
									hr+=1;
								}else{
									min+= interval;
								}
								if( hr.toString().length ==1)
									hr = "0"+hr;
								if( min.toString().length ==1)
									min= "0"+min;
								temp = hr+""+min;	
								month_races["_"+(cur_day+i)][j].event_time= temp;
								long_td.className += " room_event";
								long_td.style.border= '1px solid #e82d2d';
								break;
							}
						}
					}
					tr.appendChild(long_td);
				}
				table.appendChild(tr);
				minutes = parseInt(minutes);
			}
		}
		
		return table;
	}
	function get_day_view(){
		var d 		  	   = new Date(get_selected_day()+"T00:00:00Z");
		d.setDate(d.getDate()+1);
		//if(d.getDate()!=1)
		//	d.setDate(d.getDate()+1);
		//var week_start_day = new Date(d.setDate(d.getDate() - d.getDay()));
		var month 	  	   = d.getMonth();
		var year  	  	   = d.getFullYear();
		var day       	   = d.getDate();
		var selected_day_num = (d.getDay());
		//var month_str       = (parseInt(month)+1)+ "";
		var overlay = document.getElementById("overlay");
			overlay.style.width  = '';
			overlay.style.left	 = '';
			overlay.style.height = '550px';
			var month_races  = get_month_races(year, month);
		/*
			var start_day 	 = 0;
			
			var month_races  = get_month_races(year, month);
			var selected_day_num = (new Date(get_selected_day())).getDay()+1;
			var selected_day = (new Date(get_selected_day())).getDate();
		*/
		//if(month_str.length < 2 )
		//	month_str = "0" + month_str;

		var table 	  = document.createElement("table");
		var d		  = new Date();
		var today	  = d.getDate();
	
		var month_name_row="<thead><tr><td colspan= '8' class='month_name'>"+month_names[month]+" "+year+"</td></tr></thead>";
		table.innerHTML=month_name_row;
	
		table.className='cal_table';
		//create header
		var head_row 	  = document.createElement("tr");

		var th= document.createElement("th");
			th.className='cal_header';
			th.appendChild(document.createTextNode("Time"));
		head_row.appendChild(th);
		
		th = document.createElement("th");
		th.className='cal_header';

		//console.log(day);
		
		th.appendChild(document.createTextNode(day_names[selected_day_num] +" ("+(day)+")"));
		th.setAttribute("colspan", 7);
		head_row.appendChild(th);

		table.appendChild(head_row);
		
		
		//create days
		for(var hours = 0; hours < 18; hours++){
			var h = ((7+hours)%12)+1;
			for(minutes = 0; minutes < 60; minutes+=interval){
				var m=minutes;
				var am_pm= 'am';
				
				if( minutes < 10)
					m= "0"+minutes;
					
				if( hours >= 4 && hours < 16)
					 am_pm='pm';
				
				var temp_hr= h;		
				
				if( am_pm == 'pm' && temp_hr!=12)
					temp_hr+=12;	
				
				if(hours >= 16)
					temp_hr=hours-16;
					
				if(h==12 && am_pm=='am')
					temp_hr=0;
				
				if(temp_hr.toString().length == 1)
					temp_hr="0"+temp_hr;
				
				var time_stamp= temp_hr+""+m;

			
				var td= document.createElement("td")
					td.className='day_cal_time_cell';
					
					if( h.toString().length ==1)
						td.innerHTML = "0"+h+':'+m+" "+ am_pm;
					else
						td.innerHTML =h+':'+m+" "+ am_pm;
					td.setAttribute("time_stamp",time_stamp);
				var tr = document.createElement("tr");
					tr.appendChild(td);
					
				//var cur_day =week_start_day.getDate();
				//for( var i=0; i < 7; i++){
					var long_td = document.createElement("td");
						long_td.className = "day_cal_cell";
						long_td.setAttribute("colspan", 7);
					if( month_races["_"+(day)]){
						for(j in month_races["_"+(day)]){
 
							if( ( month_races["_"+(day)][j].event_time == time_stamp) && parseInt(month_races["_"+(day)][j].duration) > 0){

								month_races["_"+(day)][j].duration -= interval;
								var temp = month_races["_"+(day)][j].event_time;
								var hr  = parseInt(temp.substring(0,2));
								var min = parseInt(temp.substring(2));
								if(min + interval >=60){
									min = (min+interval)%60;
									hr+=1;
							
								}else{
									min+= interval;
								}
								if( hr.toString().length ==1)
									hr = "0"+hr;
								if( min.toString().length ==1)
									min= "0"+min;
								temp = hr+""+min;	
								month_races["_"+(day)][j].event_time= temp;
								long_td.className += " room_event";
								long_td.style.border= '1px solid #e82d2d';
								
								break;
							}
						}
					}
					tr.appendChild(long_td);
				//}
				table.appendChild(tr);
				minutes = parseInt(minutes);
			}
		}
		
		return table;
	}
	function month_view(){
		var selected_day_arr = get_selected_day().split("-");
		display_calendar(selected_day_arr[0], parseInt(selected_day_arr[1])-1,parseInt(selected_day_arr[2])-1, "month" );
		
	}
	function week_view(){
		var selected_day_arr = get_selected_day().split("-");
		display_calendar(selected_day_arr[0], parseInt(selected_day_arr[1])-1,parseInt(selected_day_arr[2])-1, "week" );

		
	}
	function day_view(){
		var selected_day_arr = get_selected_day().split("-");
		display_calendar(selected_day_arr[0], parseInt(selected_day_arr[1])-1,parseInt(selected_day_arr[2])-1, "day" );
	}
	function go_to_week(event){
		var date  =new Date(get_selected_day()+"T00:00:00Z");
		var year  	  = date.getFullYear();
		var month 	  = date.getMonth()+1;
		var start_day = 1;
		var num_days_in_month = new Date(year, month, 0).getDate();
		if( event.target.className == "next_month_btn"){
			start_day = parseInt(document.getElementsByClassName("cal_table")[0].children[1].children[7].innerHTML.replace(/[^0-9]/g, '')) + 1;
			if( start_day < date.getDate()){
				start_day = start_day+1;
				month+=1;
			}
		}else{
			if( (parseInt(document.getElementsByClassName("cal_table")[0].children[1].children[1].innerHTML.replace(/[^0-9]/g, ''))-7) <= 0 ){
				start_day = parseInt(document.getElementsByClassName("cal_table")[0].children[1].children[1].innerHTML.replace(/[^0-9]/g, ''))-7;
				month-=1;
				start_day = num_days_in_month + start_day;
			}else{
				start_day = parseInt(document.getElementsByClassName("cal_table")[0].children[1].children[1].innerHTML.replace(/[^0-9]/g, ''))-7;
			}	
		}
		document.getElementsByClassName('content_container')[0].setAttribute("date", year+"-"+prepad(month)+"-"+prepad(start_day));
		display_calendar(year, month,start_day, "week");
	}
	function go_to_day(event){
		
		var date  =new Date(get_selected_day()+"T00:00:00Z");
		date.setDate(date.getDate()+1);
		var year  = date.getFullYear();
		var month = date.getMonth()+1;
		var selected_day = parseInt(date.getDate());
		var num_days_in_month = parseInt(new Date(year, month, 0).getDate())-1;
		if( event.target.className == "next_month_btn"){
			if( selected_day > num_days_in_month){
				selected_day = 1;
				month++;
			}else
				selected_day++;
		}else{
			if( (selected_day-1) < 1){
				selected_day = new Date(date.getFullYear(), (month - 1), 0).getDate();
				month--;	
			}else
				selected_day--;
		}

		document.getElementsByClassName('content_container')[0].setAttribute("date", year+"-"+prepad(month)+"-"+prepad(selected_day));
		display_calendar(year, month,selected_day, "day");
		
	}
	function go_to_month(event){
		
		var year= event.target.getAttribute("year");
		var month= event.target.getAttribute("month");
		
		display_calendar(year, month,1, "month");
	}	
	function get_selected_day(){
		return document.getElementsByClassName('content_container')[0].getAttribute("date");
	}
	function change_day(event){
	
		var tar= event.target;
			tar.className= 'cal_cell_selected';
		if(!event.target.getAttribute("month"))
			tar= event.target.parentNode;	
		
		
		var y    = tar.getAttribute("year" );
		var m    = parseInt(tar.getAttribute("month"))+1;
		var d    = tar.getAttribute("day"  );
		
		window.location = '_DO:cmd=send_js&c_name=p2r_scheduling_wrapper&js=change_date("'+tar.getAttribute("month")+'", "'+tar.getAttribute("day")+'")';
		
		initialize(y+"-"+prepad(m)+"-"+prepad(d));
		close_overlay();
	
	}
	function get_cal(){
		var return_arr= [];
		var d= new Date();
		for(var y=d.getFullYear()-2; y < d.getFullYear()+2; y++){
			return_arr[y]=[];
			for( var m=0; m < 12; m++){
				return_arr[y][m]= daysInMonth(m+1,y);				
			}
		}
		return return_arr;
	}
	function get_month_races(year,month){
		return performAJAX(data_url,"function=get_month_races&year="+year+"&month="+month,false,"POST");
	}
	function daysInMonth(month,year) {
    	return new Date(year, month, 0).getDate();
	}
	function calculate_width(loc_data){

		_room_width= (screen_size/((3.0*loc_data.tracks.length)+loc_data.rooms.length));
		
		if((loc_data.tracks.length + loc_data.rooms.length) > 1){
		
			_track_width =((screen_size-(_room_width*loc_data.rooms.length))/loc_data.tracks.length)+"px";
			_room_width+="px";
		}else{
			_room_width +="px";
			_track_width=_room_width;
		}
	}
	function create_date(){
		var d=new Date();
		var m = month[d.getMonth()];
		document.getElementsByClassName("date_container")[0].innerHTML=""+m +" "+d.getDay();
	}
	function create_box(type, id){
		var temp_elem = document.createElement("span");
		temp_elem.innerHTML='_';
		if(type=='track'){
			var elem = document.createElement("div");
			elem.className= "track_cell";
			elem.setAttribute(type+"_id", id)
			elem.innerHTML=".";
			return elem;
		}else if(type=='room'){
			var elem = document.createElement("div");
			elem.className= "room_cell";
			elem.setAttribute(type+"_id", id)
			elem.innerHTML=".";
			//elem.innerHTML= temp_elem.toString();
			return elem;
		}
	}
	function find_new_races(current_id){
		performAJAX("./check_new_events.php","function=check_new_events&first=1",true,"POST",update_grid);
		setTimeout(update_races_completed, 3000);
	}
	function update_grid(data){
		var today=new Date();
		var month=today.getMonth()+1; 
		if( month < 10)
			month="0"+month;
		var date= today.getDate();
		if(date < 10)
			date= "0"+date;
		var date_string= get_selected_day();
		if(data){
			if(data.race_update.data){	
				if(Array.isArray(data.race_update.data)){
					for(var j=0; j<data.race_update.data.length; j++){
						var d= data.race_update.data[j];
						if(d.date == date_string){
							var elems= document.querySelectorAll("[event_id]");
							for( var i=0; i<elems.length; i++){
								if( elems[i].getAttribute("event_id")== d.event_id){
									reset_cell(elems[i]);
									break;
								}
							}
							d.evt_type = d.title;
							d.type	   = d.id;
							draw_track(d.time,d.trackid,d.event_id, d);
						}
					}
				}else{
					if( data.race_update.data.date == date_string){
						if( data.race_update.data.deleted){
							var events= document.querySelectorAll("[event_id]");
							for(var i=0; i< events.length; i++){
								if(events[i].getAttribute("event_id")== data.race_update.data.event_id){
									reset_cell(events[i]);
									break;
								}
							}
						}else{	
							var elems= document.querySelectorAll("[event_id]");
							for( var i=0; i<elems.length; i++){
								if( elems[i].getAttribute("event_id")== data.race_update.data.event_id){
									reset_cell(elems[i]);
									break;
								}
							}
							data.race_update.data.evt_type = data.race_update.data.title;
							data.race_update.data.type	   = data.race_update.data.id;
							draw_track(data.race_update.data.time,data.race_update.data.trackid, data.race_update.data.event_id, data.race_update.data);
						}
					}
				}
			}	
			if( data.racers_update.event_id) {
				var elem= document.querySelector('[event_id="'+data.racers_update.event_id+'"]');
	
				if(elem &&  data.racers_update.date == date_string){
					var index=1;
					if(elem.childNodes[0].className=='vert_line'){
						index=2;
					}
					elem.childNodes[index].removeChild(elem.childNodes[index].childNodes[0]);
					
					var new_content= document.createElement("p");
					new_content.innerHTML= data.racers_update.num_racers;
					new_content.className='number_positioner';
					if( data.racers_update.event_id > 0)
						elem.childNodes[index].className="num_racers_active"; 
					elem.childNodes[index].appendChild(new_content);
				}
			}
			if(data.room_update.id && data.room_update.event_time !=''){
				if(data.room_update._deleted==1){
					if(document.getElementsByClassName("room_event_"+data.room_update.id).length) 
						do_double_tap(document.getElementsByClassName("room_event_"+data.room_update.id)[0], false); //remove the room 
				}else if(get_selected_day() == data.room_update.event_date){								  //add the room_events
					var par   = document.getElementById(data.room_update.event_time)
					var index = get_column_offset(data.room_update.event_time, "room_id", data.room_update.room_id);
					var d 	  = 0; 
					if( document.getElementsByClassName("room_event_"+data.room_update.id).length){// if we are updateing an existing event
						var e=  document.getElementsByClassName("room_event_"+data.room_update.id)[0];
						while(e){
							reset_cell(e);
							e = document.getElementsByClassName("room_event_"+data.room_update.id)[0];
						}
					}				
					if(data.room_update.duration == interval){
						par.childNodes[index].className='room_cell room_event_start_end room_event_'+data.room_update.id;
						
						par.childNodes[index].innerHTML="";
						var inner_elem= document.createElement("div");
							inner_elem.className= "room_event_start_end_inner";
							inner_elem.innerHTML= data.room_update.title;
							
						par.childNodes[index].appendChild(inner_elem);
						
					}else{
						if(data.room_update.duration==0 || data.room_update.duration=='' ||  data.room_update.order_id !=''){
							var ele= document.createElement("div");
							var inner_elem= document.createElement("div");
								inner_elem.className='room_event_start_end_inner';
						
							if(!data.room_update.item_id)
								data.room_update.item_id = 'EDIT ME';
							
							inner_elem.innerHTML="["+data.room_update.item_id+"] "+data.room_update.title;
							add_listener_to_cell(ele);
							ele.className += " room_event_start_end room_event_"+data.room_update.id+ " room_event_divider";
							ele.appendChild(inner_elem);					
							ele.style.width  = _track_width;
							ele.style.left   = "80px";
							ele.style.position = "relative";
							
							ele.style.border = "";
							ele.style.backgroundColor = ""; 
							if(par.previousElementSibling && par.previousSibling.className=='room_cell'){
								par.previousSibling.parentNode.removeChild(par.previousSibling);
							}
							document.getElementsByClassName("content_container")[0].insertBefore(ele, par)

						}else{
							while(d < data.room_update.duration){
								if(index)
									par.childNodes[index].innerHTML="";
								var inner_elem= document.createElement("div");
								if(d==0){
									par.childNodes[index].className='room_cell room_event_start room_event_'+data.room_update.id;
									inner_elem.className = "room_event_start_inner";
									//console.log(data.room_update.title);
									if(!data.room_update.item_id)
										data.room_update.item_id = 'EDIT ME';
									inner_elem.innerHTML = data.room_update.title;
										
															
								}else if((d+interval)== data.room_update.duration){
									par.childNodes[index].className= "room_cell room_event_end room_event_"+data.room_update.id; 
									inner_elem.className = "room_event_end_inner";
									inner_elem.innerHTML=".";
								}else{
									par.childNodes[index].className= "room_cell room_event_mid room_event_"+data.room_update.id;
									inner_elem.className= "room_event_mid_inner";
									inner_elem.innerHTML=".";	
								}
								
								par.childNodes[index].appendChild(inner_elem);
								par= par.nextSibling;
								d+=interval;
							}
						}
						
					}
					
				}	
			}
		}else{
			console.log('DATA is null: '+ data);
		}
		performAJAX("./check_new_events.php","function=check_new_events&sched_id="+data.racers_update.sched_id+"&event_stamp="+data.race_update.event_stamp+"&room_stamp="+data.room_update.lastmod,true,"POST",update_grid);
	}
	function reset_cell(cell){
		if(cell.className.indexOf('track_cell') !=-1){
			cell.removeAttribute("event_id");
			cell.innerHTML			   = '.';
			cell.removeAttribute("selected");
			cell.style.backgroundColor = '';
			cell.className	       	   = "track_cell";
		}else{
			
			if(cell.className.indexOf("room_event_divider")!=-1)
				cell.parentNode.removeChild(cell);
			else{
				//console.log(cell.tagName);
				if(cell.tagName!='INPUT'){
					
					cell.className='room_cell';
					cell.innerHTML='.';
				}
			}
		}
	}
	function initialize_listeners(){
		var track_cells= document.getElementsByClassName('track_cell');		
		for(var i=0; i< track_cells.length; i++){
			track_cells[i].addEventListener("touchstart", touch_start);
			track_cells[i].addEventListener("touchend",   touch_end  );				
			if(_debugging){
				track_cells[i].addEventListener("mousedown",click_start);
				track_cells[i].addEventListener("mouseup"  ,click_end);
			}
		}
		var track_cells_with_con= document.getElementsByClassName('track_cell_with_contents');
		
		for(var i=0; i< track_cells_with_con.length; i++){
			track_cells_with_con[i].addEventListener("touchstart", touch_start);
			track_cells_with_con[i].addEventListener("touchend",   touch_end  );				
			if(_debugging){
				track_cells_with_con[i].addEventListener("mousedown",click_start);
				track_cells_with_con[i].addEventListener("mouseup"  ,click_end);
			}
		}
		
		var room_cells = document.getElementsByClassName('room_cell');
		for(var i=0; i< room_cells.length; i++){
			document.getElementsByClassName('room_cell')[i].addEventListener ("touchstart", touch_start);
			document.getElementsByClassName('room_cell')[i].addEventListener ("touchend",   touch_end  );
			if(_debugging){
				document.getElementsByClassName('room_cell')[i].addEventListener ("mousedown", click_start);
				document.getElementsByClassName('room_cell')[i].addEventListener ("mouseup"  , click_end);
			}
		}
	}
	function add_listener_to_cell(ele){
		ele.addEventListener("touchstart", touch_start);
			ele.addEventListener("touchend",   touch_end  );				
			if(_debugging){
				ele.addEventListener("mousedown",click_start);
				ele.addEventListener("mouseup"  ,click_end);
			}
	}
	function touch_start(event){
		_start_x_loc= event.touches[0].pageX;
		_start_y_loc= event.touches[0].pageY;
		var between=(new Date().getTime())-_time_start;
		//console.log("time_between: "+between);
		if(between < 250){
		    //console.log("double tap detected");
			do_double_tap(event.target, true);
			_is_double_tap=true;
		}else{ 
			
			_time_start = new Date().getTime();
			_is_double_tap=false;
		}
	}
	function touch_end(event){
		event.preventDefault();
		setTimeout(function(){
			if( (Math.abs(_start_x_loc-event.changedTouches[0].pageX) < 10 && Math.abs(_start_y_loc-event.changedTouches[0].pageY) < 10 )){	
				if( !_is_double_tap)
					cell_select(event);
			}else{

			}
		}, 250);
		
	}
	function click_start(event){
		event.preventDefault();
		_start_touch_id= event.target.parentElement.id;
		var between=(new Date().getTime())-_time_start;
		//console.log("time_between: "+between);
		if(between < 250){
		    //console.log("double tap detected");
			do_double_tap(event.target, true);
			_is_double_tap=true;
		}else{ 
			
			_time_start = new Date().getTime();
			_is_double_tap=false;
		}
	}
	function click_end(event){
		/*
		if(event.target.parentElement.id==_start_touch_id)
			cell_select(event);
		*/
		
		event.preventDefault();
		setTimeout(function(){
			if( event.target.parentElement){
				if( event.target.parentElement.id ==_start_touch_id){	
					if( !_is_double_tap)
						cell_select(event);
				}else{
	
				}
			}
		}, 250);
			
	}
	function cell_select(event){
		
		var id   = '';
		var type = '';
		if(event.target.getAttribute("class") == 'track_cell'){
			event.stopPropagation();
			id = event.target.getAttribute("track_id");
			type='track';	
			
		}
		else if(event.target.getAttribute("class") == 'track_cell_with_contents' || event.target.getAttribute("class") == 'note_input'){
			id   = event.target.getAttribute("event_id");
			type ='edit_note';		
		}
		else if(event.target.getAttribute("class").indexOf('room_cell') > -1){
			event.stopPropagation();
			id   = event.target.getAttribute("room_id");
			type ='room';
		}else if(event.target.className=='num_racers_active' || event.target.className == 'num_racers_inactive'){
			event.stopPropagation();
			id= event.target.parentNode.getAttribute("track_id");
			type='track';	
		}else if( event.target.className == 'number_positioner'){
			event.stopPropagation();
			id= event.target.parentNode.parentNode.getAttribute("track_id");
			type='track';	
		}else if(event.target.className=='note_container'){
			event.stopPropagation();
			id   = event.target.parentNode.getAttribute("event_id");	
			type ='edit_note';		
		}else if(event.target.className=='room_event_mid_inner'|| 
				 event.target.className=='room_event_start_inner' || 
				 event.target.className=='room_event_end_inner'|| 
				 event.target.className=='room_event_start_end_inner'){
			event.stopPropagation();
			id   = event.target.parentNode.getAttribute("room_id");
			type ='room';
		}
		
		else{
			return;
		}
		
		var time= event.target.parentElement.id;
		if(!time)
			time = event.target.parentNode.parentNode.id;
		if(!time)
			time = event.target.parentNode.parentNode.parentNode.id;
		
		if(document.getElementsByClassName("cell_selected").length  && document.getElementsByClassName("cell_selected")[0].getAttribute('event_id') != event.target.parentNode.getAttribute('event_id')  ){
			var elem=document.getElementsByClassName("cell_selected")[0];
			var event_id= elem.getAttribute('event_id');
			var targ= event.target;
			move_race(time, event_id, id, elem, targ);
			
		}else if(type=='track'){
			
			var event_id = event.target.getAttribute("event_id");
			if(!event_id)
				event_id = event.target.parentNode.getAttribute("event_id");
		    if(!event_id)
		   		event_id = event.target.parentNode.parentNode.getAttribute("event_id");
		   		 
			show_overlay( id, time, type, event_id);	
		}else if(type=='room'){
			var ele= document.getElementsByClassName("create_room_event_start")[0];
			if(ele && ele.getAttribute("room_id")==id){
				var ta= event.target;
				if(event.target.className.indexOf("room_event")!=-1)
					ta= event.target.parentNode;
				
				end_selection(time, ta, id);
			}else{
				if(event.target.className.indexOf("room_event")!=-1)
					edit_room_event(event.target);
				else{
					if(document.getElementsByClassName("create_room_event_start").length)
						document.getElementsByClassName("create_room_event_start")[0].className="room_cell";
					else
						start_selection(time,event.target, id);
				}
			}
		}else if( type == 'edit_note' ){
			//if(event.target.className=='note_container')
			open_note_editor(event);
			event.target.focus();
			//else
			//	open_note_editor(id, event);
		}
	}
	function open_note_editor(event){
		
		var elem = event.target;
		var inputs= document.getElementsByClassName("note_input");
		for(var i=0; i<inputs.length; i++)
			inputs[i].style.opacity=0;
		
		if(elem.nextSibling && elem.nextSibling.className=='note_container_inner')
			elem.value= elem.nextSibling.innerHTML;
		
		elem.addEventListener("keyup", send_note);
		elem.style.opacity='1';
	}
	function close_note_editor(event){
		var elem = event.target;
		elem.style.opacity=0;
		var chil= document.createElement("div");
			chil.className='note_container_inner';
			chil.style.zIndex=0;
		
		var val= elem.value;
		if( val.length > 50)
				val= val.substr(0, 47)+"...";	
				
			chil.innerHTML=val;
			
		
		var childrens= elem.parentNode.childNodes;
		for(var i=0; i<childrens.length; i++)
			if(childrens[i].className=='note_container_inner')
				childrens[i].parentNode.removeChild(childrens[i]);
				
		elem.parentNode.appendChild(chil);

	}
	function send_note(event){
		if(event.keyCode==13){
		
			performAJAX(data_url, "function=save_note&note="+event.target.value+"&event_id="+event.target.parentNode.parentNode.getAttribute("event_id"),false,"POST");
			close_note_editor(event);
		}
	}
	function do_double_tap(tar, do_confirm){
		if( (tar.innerHTML!='.' || tar.className.indexOf('room_event')!=-1) &&( !do_confirm || confirm("Are you sure you want to delete this event?"))){
			if(tar.className.indexOf("room_event") > -1){
				
				var room_id_num=tar.className.replace(/\D/g,'');
				
				if(!room_id_num)
					room_id_num =tar.parentNode.className.replace(/\D/g,'');
				
				remove_event("room", room_id_num);			
				var ele= document.getElementsByClassName("room_event_"+room_id_num)[0];
				while(ele){
					reset_cell(ele);
					ele = document.getElementsByClassName("room_event_"+room_id_num)[0];
				}		
			}else{	// remove normal event
				if( tar.className=='note_input'){
					remove_event("track", tar.parentNode.parentNode.getAttribute("event_id"));
					reset_cell(tar.parentNode.parentNode);
				}else if( tar.className=='note_container' || tar.className=='containing_cell' || tar.className=='num_racers_active'){
					remove_event("track", tar.parentNode.getAttribute("event_id"));
					reset_cell(tar.parentNode);
				}else if( tar.className=='race_type' ||tar.className=='number_positioner'){
				    remove_event("track", tar.parentNode.parentNode.getAttribute("event_id"));
				    reset_cell(tar.parentNode);
                }else{			
					remove_event("track", tar.getAttribute("event_id"));			
					reset_cell(tar);
				}
			}
		}
		_is_double_tap=false;
	}
	function remove_event(type,id){
		performAJAX(data_url, "function=remove_event&type="+type+"&event_id="+id,false,"POST");
	}
	function move_race(time, event_id, track_id, elem,targ){
		if( targ.className == 'note_input' || targ.className == 'number_positioner')
			targ= targ.parentNode.parentNode;
		
			
		if(targ.getAttribute("event_id") == null && targ.className.indexOf("track_cell") != -1 ){
			performAJAX(data_url,"function=move_race&time="+time+"&event_id="+event_id+"&track_id="+track_id,false,"POST");
			targ.innerHTML = elem.innerHTML;
			targ.setAttribute("event_id",event_id);
			//var targ_date    = new Date(targ.previousElementSibling.children[0].innerHTML);
			var current_date = new Date();
			var hours   = current_date.getHours();
			var minutes = current_date.getMinutes();
			
			if( hours.length == 1)
				hours= "0"+hours; 
			if((""+minutes).length ==1)
				minutes="0"+minutes;
			
			var cur_time = hours+""+minutes;			
			/*
			if( parseInt(time) < parseInt(cur_time))
				targ.className+=" past_race";
			else if((parseInt(cur_time) - parseInt(time)) > 0 && (parseInt(cur_time) - parseInt(time)) < 10){
				elem.className+=" current_race";
				elem.style.backgroundColor= "#aecd37";
			}*/
			
			
			reset_cell(elem);
		}else{
			performAJAX(data_url,"function=merge_racers&start_id="+elem.getAttribute("event_id")+"&end_id="+targ.getAttribute("event_id"),false,"POST");
			show_overlay(targ.getAttribute("track_id"), targ.parentNode.id, "race", targ.getAttribute("event_id"));
			elem.removeAttribute("selected");
			elem.className = elem.className.replace(/\bcell_selected\b/,'');	
			elem.style.backgroundColor='';
			//reset_cell(elem);
			
		} 
		/*if( elem.getAttribute('event_id') == targ.getAttribute('event_id') || elem.getAttribute('event_id') == targ.parentNode.getAttribute('event_id')){
		    //elem.className= 'track_cell_with_contents';
		    //reset_cell(elem);
		}*/

	}
	function start_selection(time,elem, room_id){

		elem.className= "create_room_event_start room_cell";
		var inner_elem= document.createElement("div");
			inner_elem.className = 'room_event_start_inner';
			inner_elem.innerHTML = ".";
			//inner_elem.addEventListener("touchstart", touch_start);
			//inner_elem.addEventListener("touchend",   touch_end);
		elem.removeChild(elem.childNodes[0]);
		elem.appendChild(inner_elem);

	}
	function end_selection(end_time,end_elem,room_id){
	
		var start_elem = document.getElementsByClassName('create_room_event_start')[0];
		var start_time = start_elem.parentNode.id;
		if(start_elem.getAttribute("room_id")!= end_elem.getAttribute("room_id")){
			start_elem.className= "room_cell";
			return;
		}
		if(start_time == end_time){
			
			var url_args= "function=create_room_event&event_time="+start_time+"&duration="+interval+"&room_id="+room_id+"&event_date="+ get_selected_day();
			var room_event_id= performAJAX(data_url,url_args,false,"POST").id;
			end_elem.className= "room_cell room_event_start_end room_event_"+room_event_id;
			
			var inner_elem= document.createElement("div");
				inner_elem.className = 'room_event_start_end_inner';
				inner_elem.innerHTML = ".";
				//inner_elem.addEventListener("touchstart", touch_start);
				//inner_elem.addEventListener("touchend",   touch_end);
				
			end_elem.removeChild(end_elem.childNodes[0]);
			end_elem.appendChild(inner_elem);
			
			return;
			
		}
		if( start_time > end_time){	// flip the elements
			
			var temp   = start_time;
			start_time = end_time;
			end_time   = temp;		
			
			var temp_el = start_elem;
			start_elem  = end_elem;
			end_elem    = temp_el;
		}
		
		
		var index = get_column_offset(start_time, "room_id", room_id);
		var res   = check_overlapping(start_time, end_time, index);
		if (res){
			document.getElementsByClassName("create_room_event_start")[0].className="room_cell";
			//start_elem.className="room_cell";
			edit_room_event(res);
			return;
		}else{
			var url_args = "function=create_room_event&event_time="+start_time+"&duration="+get_duration(start_time, end_time)+"&room_id="+room_id+"&event_date="+ get_selected_day();
			var room_event_id = performAJAX(data_url,url_args,false,"POST").id;
			
			start_elem.className = "room_cell room_event_start room_event_"+room_event_id;
			var inner_elem= document.createElement("div");
				inner_elem.className = 'room_event_start_inner';
				inner_elem.innerHTML = ".";

			start_elem.removeChild(start_elem.childNodes[0]);
			start_elem.appendChild(inner_elem);
			
			end_elem.className   = "room_cell room_event_end room_event_"+room_event_id;
			var inner_elem= document.createElement("div");
				inner_elem.className = 'room_event_end_inner';
				inner_elem.innerHTML = ".";
				
			//inner_elem.addEventListener("touchstart", touch_start);
			//inner_elem.addEventListener("touchend",   touch_end);
			end_elem.removeChild(end_elem.childNodes[0]);
			end_elem.appendChild(inner_elem);
			
			var par = document.getElementById(start_time);
			do{
				if( par.nextSibling.id < end_time){
					var el= par.nextSibling.childNodes[index];
					el.className= 'room_cell room_event_'+room_event_id+" room_event_mid";
					par= par.nextSibling;
					
					var inner_elem= document.createElement("div");
						inner_elem.className = 'room_event_mid_inner';
						inner_elem.innerHTML = ".";
					el.removeChild(el.childNodes[0]);
					el.appendChild(inner_elem);
					
					
					has_next_elem=true;
				}else
					has_next_elem=false;
				
			}while(has_next_elem);
		}
	}
	function get_column_offset(time, attribute_name, val){
		//This part gets the offset once that way we dont have to do it multiple times. 
		//console.log("time: "+time+" attribute_name: "+attribute_name+" val: "+val);
		try{
			var temp_arr = document.getElementById(time).childNodes;
			var index    = 0; 
			
			for( var t = 0; t< temp_arr.length; t++){
				if( temp_arr[t].getAttribute(attribute_name) && temp_arr[t].getAttribute(attribute_name) == val){
					return t;
				}
			}	
		}catch(err){
			console.log(time);
		}
		
		return (t-1);
	}
	function check_overlapping(start_time, end_time, index){
		//console.log(start_time+", "+end_time);
		var has_next_elem = true;
		var par 	      = document.getElementById(start_time);
		do{
			if(par.id &&  par.id <= end_time){
				var el= par.childNodes[index];
				if(el.className!=("create_room_event_start room_cell") && el.className.indexOf("room_event_")!=-1){
					return el;
				}
				par= par.nextSibling;
				has_next_elem=true;
			}else
				has_next_elem=false;
			
		}while(has_next_elem);
		return false;
	}
	function get_duration(start, end){
		var counter=1;
		while( document.getElementById(start).nextSibling.id != end){
			start=document.getElementById(start).nextSibling.id;
			counter++;
			if(counter > 500)
				return "failed";
		} 
		counter++;	
		
		return (counter*interval);
	}
	function edit_room_event(tar){
		var event_id = tar.className.replace(/\D/g,'');
		if(!event_id){
			event_id= tar.parentNode.className.replace(/\D/g,'');	//if they clicked the overlay thingy
		}
		var result   = performAJAX(data_url,"function=get_room_details&event_id="+event_id,false,"POST");

		if(!tar.getAttribute("room_id"))
			room_id = result.room_id;
		else
			room_id = tar.getAttribute("room_id");

		var overlay  = document.getElementById("overlay");
		overlay.style.width = "820px";
		overlay.style.left = "96px";
		overlay.style.height ='550px';		
		
		var close = document.getElementById("close_overlay");
		overlay.innerHTML='';
		overlay.appendChild(close);
		
		
		var form  = document.createElement("form");
			form.id='room_event_details'; 
		var table = document.createElement("table");
			table.className='room_event_table';
		
		var r_event_table= document.createElement("div");
			form.appendChild(table);
			r_event_table.className = "table_container";
			
		//console.log(tar);
		if(!tar.getAttribute("room_id"))
			tar= tar.parentNode;
		
		tar.event_id= event_id;
		if( result.duration!=0){
			form_elements= {
				"Event Type"   : create_form_element("select","event_type",result.item_id, result.types,null, tar),
				"Title"        : create_form_element("text","title", decodeURIComponent(result.title),null, not_empty),
				"Event Time"   : create_form_element("select","event_time",result.event_time, available_times(room_id, event_id),null, tar),
				"Duration"     : create_form_element("select", "duration",result.duration, get_durations(room_id, event_id, result.event_time),null, tar),
				"Name"         : create_form_element("text", "cust_name",result.name,is_text),
				"Phone Number" : create_form_element("tel", "cust_phone",result.cust_phone,null, not_empty),
				"Email Address": create_form_element("email", "cust_email",result.cust_email,is_email),
				"CSR"   	   : create_form_element("select","csr",result.cust_rep, result.csr_list,null, tar),
				"Notes"		   : create_form_element("textarea", "notes", decodeURIComponent(result.notes),null,null,null),
				"Check List"   : create_form_element("checkboxes", "group_check_list",result.checklist,result.collections,null, null)
			};
		}else{
			form_elements= {
				"Event Type"   : create_form_element("select","event_type",result.item_id, result.types,null, tar),
				"Title"        : create_form_element("text","title", decodeURIComponent(result.title),null, not_empty),
				"Event Time"   : create_form_element("select","event_time",result.event_time, available_times(room_id, event_id),null, tar),
				"Name"         : create_form_element("text", "cust_name",result.name,is_text),
				"Phone Number" : create_form_element("tel", "cust_phone",result.cust_phone,null, not_empty),
				"Email Address": create_form_element("email", "cust_email",result.cust_email,is_email),
				"CSR"   	   : create_form_element("select","csr",result.cust_rep, result.csr_list,null, tar),
				"Notes"		   : create_form_element("textarea", "notes", decodeURIComponent(result.notes),null,null,null),
				"Check List"   : create_form_element("checkboxes", "group_check_list",result.checklist,result.collections,null, null)
			};
				
		}
		for(var index in form_elements){
			var tr = document.createElement("tr");
			
			var title_td = document.createElement("td");
				title_td.innerHTML = index;
				title_td.className = 'room_event_title';
		
			var input_td = document.createElement("td");
				input_td = form_elements[index];
				input_td.className= "room_event_input";
			
			tr.appendChild(title_td);
			tr.appendChild(input_td);
			table.appendChild(tr);
		}
		var tr = document.createElement("tr");
		
		var title_td = document.createElement("td");
			title_td.innerHTML = "<input type='hidden' name='event_id' value='"+event_id+"' /><input type='hidden' name='event_date' value='"+ get_selected_day()+"' />";
		
		var input_td = document.createElement("td");
			if(_debugging)
				input_td.innerHTML="<input type='button' value='Save Event' onmouseup=\"save_info($('#room_event_details'))\" onclick=\"save_info($('#room_event_details'))\">";
			else
				input_td.innerHTML="<input type='button' value='Save Event' onclick=\"save_info($('#room_event_details'))\">";
		
		tr.appendChild(title_td);
		tr.appendChild(input_td);
		table.appendChild(tr);
		
		r_event_table.appendChild(form);
		overlay.appendChild(r_event_table);
		
		
		document.getElementById("overlay_container").style.display='block';
		
	}
	function save_info(form){
		var serialized = form.serialize();
		var result	   = performAJAX(data_url, "function=update_room_event&"+serialized,false,"POST");
		document.getElementsByClassName("room_event_"+form[0][(form[0].length-3)].value)[0].childNodes[0].innerHTML= document.getElementsByClassName("room_event_input")[0].value;		
		close_overlay();	
	}
	function create_form_element(type, name, default_val, options, validator, tar){
		var ele = null;
		if(type=='text' || type=='tel' || type=='email'){
			ele= document.createElement("input");
			ele.setAttribute("type", type);
			ele.setAttribute("name", name);
		
		}else if(type=='select'){
			ele = document.createElement("select");
			ele.setAttribute("room_id",tar.getAttribute("room_id"));
			ele.setAttribute("event_id",tar.event_id);
			ele.setAttribute("name",name);
			ele.addEventListener("change", select_change);
			for(var i=0; i < options.length; i++){
				var opt = document.createElement("option");
					if(options[i].value)
						opt.value=options[i].value;
					else
						opt.value=options[i];
					var inner='';
					try{
						var first  = options[i].substring(0,parseInt((options[i].length/2)) );
						var non_military= first;
						if( non_military > 12)
							non_military -=12;
						var second = options[i].substring(parseInt((options[i].length/2)));
						inner= non_military+":"+second;
						if( first < 12 )
							inner+=" AM";
						else
							inner+=" PM";
							
					}catch(err){
						if( options[i].title)
							inner = options[i].title;
						else	
							inner = options[i];
					}
					
					
					opt.innerHTML=inner;
					
				ele.appendChild(opt);
			}
			
		}else if( type=='textarea'){
			ele = document.createElement(type);
			ele.setAttribute("name", name);
			ele.setAttribute("cols", "40");
			ele.setAttribute("rows", "20");
			ele.style.height='100px';
	    }else if(type =='checkboxes'){
	    	
	    	ele = document.createElement("td");
			//ele.style.height='90px';
			
			var table = document.createElement("table");
			
	    	for(var i=0; i < options.length; i++){
				
				var inner_table = document.createElement("table");
					inner_table.style.width='100%';
				var inner_tr    = document.createElement("tr");
				var title_td    = document.createElement("td");
					title_td.style.textAlign= "left";
					title_td.style.color ='black';
					title_td.style.fontSize= "small";
				var box_td      = document.createElement("td");
					box_td.style.textAlign= "right";
													
				var table_td = document.createElement("td");
					table_td.className = "checkbox_list";
				var box = document.createElement("input");
					box.type='checkbox';
					box.value = options[i];
					box.name = "check_list_"+i;
				if(default_val && default_val.length){
					for( var j=0; j< default_val.length; j++){
						if(options[i].indexOf(default_val[j])!=-1){
							box.setAttribute("checked",true);
							continue;
						}
					}
				}
					
				title_td.innerHTML = options[i];	
				box_td.appendChild(box);
				inner_tr.appendChild(title_td);
				inner_tr.appendChild(box_td);
				inner_table.appendChild(inner_tr);
				
				table_td.appendChild(inner_table);			
				
				if( i%2==0)
					table_tr = document.createElement("tr");
				table_tr.appendChild(table_td);	
				
				
				table.appendChild(table_tr);	
			}	
			ele.appendChild(table);
	    }
	    
	    if(default_val)
	    	ele.value= default_val;
	    if(validator)
	   		ele.addEventListener("keyup", validator);
		
		return ele;	
	}
	function select_change(event){
		var elems		= new Array();
		var cont 	    = null;
		var initial_val = null;
		if(event.target.getAttribute("name")=='event_time'){		// update duration
			if(!is_numeric(event.target.value)){
				alert("Invalid race time selection. Please choose a different time");
				event.target.value= document.getElementsByClassName("room_event_"+event.target.getAttribute("event_id"))[0].parentNode.id;
				return;
			}
			cont= event.target.parentNode.nextSibling.childNodes[1];
			initial_val = cont.value;
			elems= get_durations(event.target.getAttribute("room_id"), event.target.getAttribute("event_id"),event.target.value );
			cont.innerHTML='';
		}else if(event.target.getAttribute("name")=='duration'){ // update event times
			cont= event.target.parentNode.previousSibling.childNodes[1];
			initial_val = cont.value;
			elems= available_times(event.target.getAttribute("room_id"), event.target.getAttribute("event_id"));
			cont.innerHTML='';
		}	
		
		for(var i=0; i < elems.length; i++){
			var opt = document.createElement("option");
				opt.value=elems[i];
				opt.innerHTML=elems[i];
			if(elems[i] == initial_val)
				opt.setAttribute("selected",1);
			
			cont.appendChild(opt);
		}
	}
	function show_overlay(id, time, type, event_id){
	
		var close = document.getElementById("close_overlay");
			close.style.top='5px';
		
		var overlay = document.getElementById("overlay");
			document.getElementById("overlay_container").style.display='block';
			overlay.innerHTML='';
			overlay.appendChild(close);
	
			overlay.style.width  = (820 - (parseInt(_track_width)/2))+"px";
			overlay.style.left   = (96 + (parseInt(_track_width)/2))+"px";
			overlay.style.height = '475px';	
		//window.location = '_DO:cmd=send_js&c_name=p2r_scheduling_wrapper&js=highlight_track_name("'+type+"_"+id+'")';
		document.getElementById(time).children[0].className+=		" time_selected";
		document.getElementById(time).children[1].style.marginLeft= '0';
		

		if(event_id){ // Show existing data
			edit_existing_event(event_id);
		}else{	// new Race/event
			show_event_type_buttons(id, time);
		}
	}
	function filter_by_csr(elem){
		var csr_list = get_csr_list();

		for(var i=0; i<csr_list.length; i++){
			if(elem.value == ''){ 
				var rows_to_hide= document.getElementsByClassName(csr_list[i]);
				for(var c=0; c< rows_to_hide.length; c++)
					rows_to_hide[c].style.display='block';
					
			}else if(csr_list[i] != elem.value){
				var rows_to_hide= document.getElementsByClassName(csr_list[i]);
				for(var c=0; c< rows_to_hide.length; c++)
					rows_to_hide[c].style.display='none';
			}else{
				var rows_to_hide= document.getElementsByClassName(csr_list[i]);
				for(var c=0; c< rows_to_hide.length; c++)
					rows_to_hide[c].style.display='block';
			}
		}
	}
	function show_search_bar(title, id){
		var search_bar_container= document.createElement("div");
		search_bar_container.className="search_bar_container";
		
		var title_container= document.createElement("div");
			title_container.style.fontSize='larger';
			title_container.style.textAlign='center';
			title_container.style.color='black';
			title_container.innerHTML=title+"<br>";
			
		
		var search_box= document.createElement("input");
			search_box.className='search_box';
			search_box.setAttribute("type","text");
			search_box.setAttribute("placeholder", "Add Racers");
			search_box.addEventListener("keyup",function(event){
				if(event.keyCode==13)
				    get_racers(document.getElementsByClassName("search_box")[0].value, id );
	      
			});
		var search_btn= document.createElement("input");
			search_btn.setAttribute("type", 'button');
			search_btn.className='do_search';
			search_btn.value='search';
			search_btn.addEventListener("click", 
				function(){
					get_racers(document.getElementsByClassName("search_box")[0].value, id ); 
					
				});	
		
		var advanced_search= document.createElement("select");
			advanced_search.className='csr_filter';
			advanced_search.setAttribute("onchange", "filter_by_csr(this); ");	
		var opt= document.createElement("option");
			opt.value="";
			opt.innerHTML="Filter By CSR";
		advanced_search.appendChild(opt);
		
		var csrs= get_csr_list();
		for( var i=0; i<csrs.length; i++){
			var opt= document.createElement("option");
				opt.value=csrs[i];
				opt.innerHTML= csrs[i];
			advanced_search.appendChild(opt);		
		}
		
		search_bar_container.appendChild(title_container);
		search_bar_container.appendChild(search_box);
		search_bar_container.appendChild(search_btn);
		search_bar_container.appendChild(advanced_search);
		
		
		
		document.getElementById("overlay").appendChild(search_bar_container);	
	}
	function get_csr_list(){
		return _csr_list;
	}
	function edit_existing_event(event_id){
		
		var data= performAJAX(data_url,"function=get_racers_for_race&event_id="+event_id,false,"POST");
		
		if(data.racer_data_recent){
			
			var checked='';
			for( var i=0; i < data.racer_data_recent.length; i++){
				//if($.inArray(data.racer_data_recent[i], data.racer_data)){
				//	data.racer_data_recent.splice(i,1);
				//	continue;
				//}
				data.racer_data_recent[i].unchecked =1;
				if(!inArray(data.racer_data_recent[i].user_id, _csr_list))
					_csr_list.push(data.racer_data_recent[i].user_id);			
				
			}
			if( data.racer_data)
				data.racer_data = data.racer_data.concat(data.racer_data_recent);
			else
				data.racer_data=data.racer_data_recent;
		}
		show_search_bar(data.race_data['title'],event_id);
		var inner_container = '';
		if( document.getElementsByClassName("results_container").length){
			document.getElementsByClassName("results_container").innerHTML='';
			
			inner_container = document.getElementsByClassName("results_container")[0];
		}else{
			inner_container = document.createElement("div");
			inner_container.className='results_container';
		}
		var ids = [];
		for(var i=0; i < data.racer_data.length; i++){
			
			//if(container && container.getElementById("racer_id_" + data.racer_data[i].id)){
			//	console.log(data.racer_data[i].id);
			//}
			if( ids[data.racer_data[i].id])
				continue;
			else
				ids[data.racer_data[i].id]='1';
			var container   = document.createElement("div");
			
			var check_div   = document.createElement("div");
			var empty_div   = document.createElement("div");
			var name_div    = document.createElement("div");
			var f_name_div  = document.createElement("div");
			var l_name_div  = document.createElement("div");
			var credits_div = document.createElement("div");
			var birth_div	= document.createElement("div");
			
			check_div.className   = 'check_div';
			var check_box = document.createElement("input");
				check_box.setAttribute("type","checkbox");
				check_box.className ="check_class";
				check_box.id = "racer_id_"+data.racer_data[i].id;
				check_box.setAttribute("racer_id", data.racer_data[i].id);
				check_box.setAttribute("event_id", event_id);

				if(!data.racer_data[i].unchecked || data.racer_data[i].unchecked !=1){
					check_box.setAttribute("checked","checked");
					check_box.addEventListener("change", remove_racer);
				}else
					check_box.addEventListener("change", add_racer);

			check_div.appendChild(check_box);
	
			name_div.className    = 'racer_name_div';
			name_div.innerHTML	  = data.racer_data[i].racer_name;
			
			f_name_div.className  = 'first_name_div';
			f_name_div.innerHTML  = data.racer_data[i].f_name;
			
			l_name_div.className  = 'last_name_div';
			l_name_div.innerHTML  = data.racer_data[i].l_name;
			
			birth_div.className   = 'b_day_div';
			var b_date = new Date( data.racer_data[i].birth_date);
			var age =calc_age(b_date.getMonth(),b_date.getDay(),b_date.getFullYear());

			if(age < 16)
				birth_div.innerHTML	  = "<span style='color:#580000'>"+data.racer_data[i].birth_date+"</span>";
			else
				birth_div.innerHTML	  = data.racer_data[i].birth_date;
				
			
			credits_div.className = 'num_credits_remaining';
			credits_div.innerHTML = data.racer_data[i].credits;
			
			container.className   = 'racer_row_container';
			if( data.racer_data[i].user_id)
				container.className+=" "+data.racer_data[i].user_id;
			container.appendChild(check_div  );
			container.appendChild(name_div   );
			container.appendChild(f_name_div );
			container.appendChild(l_name_div );
			container.appendChild(birth_div );
			container.appendChild(credits_div);
		
			inner_container.appendChild(container);
		}
		document.getElementById("overlay").appendChild(inner_container);
	}
	function inArray(needle, haystack) {
    	var length = haystack.length;
    	for(var i = 0; i < length; i++) {
        	if(haystack[i] == needle) 
        		return true;
		}
   		return false;
	}	
	//function add_racer(racer_id, event_id, elem){
	function add_racer(event){
		var elem = event.target;
		var racer_id = elem.getAttribute("racer_id");
		var event_id = elem.getAttribute("event_id");
		var res = performAJAX(data_url,"function=add_racer&racer_id="+racer_id+"&event_id="+event_id,false,"POST");
		
		if(res.result == 'success'){
			var credits_elem= elem.parentNode.parentNode.getElementsByClassName("num_credits_remaining")[0];
			credits_elem.innerHTML = res.num_credits;
			elem.removeEventListener("change", add_racer);
			elem.addEventListener("change",remove_racer)
			//elem.checked=true; 
		}else if(res.result == 'full_race'){
			alert("Race Full");
			elem.checked=false;
		}else if( res.result =="duplicate_racer")
			console.log("racer already added");
			
	}
	function remove_racer(event){
		var elem = event.target;
		var racer_id = elem.getAttribute("racer_id");
		var event_id = elem.getAttribute("event_id");
		var res= performAJAX(data_url,"function=remove_racer&racer_id="+racer_id+"&event_id="+event_id,false,"POST");
		if(res.result=='success'){
			var credits_elem= elem.parentNode.parentNode.getElementsByClassName("num_credits_remaining")[0];
			elem.removeAttribute("checked");
			elem.removeEventListener("change", remove_racer);
			elem.addEventListener("change",add_racer)
			//elem.setAttribute("onchange", "add_racer('"+racer_id+"','"+event_id+"', this)")
			credits_elem.innerHTML = res.num_credits;
			//credits_elem.innerHTML= (credits_elem.innerHTML*1)+1;
		}
	}
	function show_event_type_buttons(track_id, time){
		var elem          = document.getElementById("overlay");
		var event_types   = performAJAX(data_url,"function=get_event_types",false,"POST");
		var container_div = document.createElement("div");
		var normal_types  = event_types.normal_races;
		var sequence_types= event_types.sequence_races;
		
		container_div.className = "event_button_container";
		for(var i=0; i < normal_types.length; i++){			//normal event creation 
			
			var button		 = document.createElement("div");
			button.className ='event_button';
			button.innerHTML = normal_types[i].title;
			
			button.setAttribute("event_id",  normal_types[i].id);
			button.setAttribute("slots", 	 normal_types[i].slots);
			button.setAttribute("race_time", time);
			button.setAttribute("track_id",  track_id);
			button.setAttribute("title",	 normal_types[i].title);
			
			button.addEventListener("touchend",select_event_type);
			if(_debugging)
				button.addEventListener("click",select_event_type);
			container_div.appendChild(button);
		}
		for(var i = 0; i < sequence_types.length; i++){	// sequence event creation 
			
			var button		 = document.createElement("div");
			button.className = 'sequence_button';
			button.innerHTML = sequence_types[i].title;
			
			button.setAttribute("event_id",  sequence_types[i].id);
			button.setAttribute("slots", 	 sequence_types[i].slots);
			button.setAttribute("race_time", time);
			button.setAttribute("track_id",  track_id);
			button.setAttribute("title",	 sequence_types[i].title);
			
			button.addEventListener("touchend",show_number_selector);
			if(_debugging)
				button.addEventListener("click",   show_number_selector);
			container_div.appendChild(button);
		
		}
		
		elem.appendChild(container_div);
	}
	function show_number_selector(event){
		
		var tar = event.target;
		var con = document.createElement("div");
			con.setAttribute("time", tar.getAttribute("race_time"));
			con.setAttribute("sequence_type_id",tar.getAttribute('event_id'));
			con.setAttribute("track_id",tar.getAttribute("track_id"));
			con.setAttribute("title",tar.getAttribute("title"));
			con.setAttribute("date",get_selected_day());
			con.addEventListener("touchend", create_sequence_race);
			if(_debugging)
				con.addEventListener("click", create_sequence_race);

			con.innerHTML= "Go";
			con.className= "number_pad_num number_pad_go";
			
		var del = document.createElement("div");
			del.addEventListener("touchstart", function(event){event.preventDefault()});
			del.addEventListener("touchend", remove_num);
			
			if(_debugging)
				del.addEventListener("click", remove_num);
			
			del.innerHTML="<";
			del.className= "number_pad_num number_pad_back";

		var overlay = document.getElementById("overlay");
		var cont	= document.createElement("div");
		var input 	= document.createElement("input");
		var num 	= 0; 

		cont.className="number_pad_container";
		var number_container= document.createElement("div");
			number_container.className="num_racers_container";
			
		for( var i = 9; i > -1; i--){
			var b= document.createElement("div");
			
			b.addEventListener("touchend", add_num);
			if(_debugging)
				b.addEventListener("click", add_num);
			
			b.innerHTML=i;
			b.className= "number_pad_num";
		
			if( i==0){
				cont.appendChild(con);
				cont.appendChild(b);
				cont.appendChild(del);
			}else{
				cont.appendChild(b);
			}
		}
		var close = document.getElementById("close_overlay");
		overlay.innerHTML = '';
		overlay.appendChild(close);
	
		overlay.appendChild(number_container);
		overlay.appendChild(cont);
		return num;
	}
	function add_num(event){
		event.preventDefault();
		
		if( _num_racers.toString().length < 3 && ((_num_racers+event.target.innerHTML)*1)<=128 ){
			_num_racers+=""+event.target.innerHTML;
			document.getElementsByClassName("num_racers_container")[0].innerHTML= _num_racers;
		}
	}
	function remove_num (event){
		event.preventDefault();
		_num_racers= _num_racers.substring(0,_num_racers.length-1);
		document.getElementsByClassName("num_racers_container")[0].innerHTML= _num_racers;
	}
	function create_sequence_race(event){
		
		if( _num_racers!=''){
			setTimeout(function(){
				event.preventDefault();
				var targ	  = event.target;
				var send_data = "function=create_sequence_race&date="+targ.getAttribute("date")+"&time="+targ.getAttribute("time")+"&sequence_type_id="+
								targ.getAttribute("sequence_type_id")+"&track_id="+targ.getAttribute("track_id")+"&num_racers="+_num_racers+"&interval=" + interval;
				//alert(send_data);
				performAJAX(data_url, send_data,false,"POST");
				close_overlay();
				_num_racers="";
			},250);
			
		}else{
			alert("Please select how many racers you want in this race.");
		}
	}
	function select_event_type(event){
		
		var tar	  		  = event.target;
		var time		  = tar.getAttribute("race_time");
		var event_type_id = tar.getAttribute('event_id');
		var track_id	  = tar.getAttribute("track_id");
		var title 		  = tar.getAttribute("title");
		var date 		  = get_selected_day();
		var url_string= "function=create_race&race_type_id="+event_type_id+"&race_time="+time+"&track_id="+track_id+"&date="+date;
	
		var new_id= performAJAX(data_url,url_string,false,"POST");
		//show_search_bar(title, new_id.id);
		edit_existing_event(new_id.id);
		document.getElementsByClassName("event_button_container")[0].style.display='none';
		//build_select_users_table(title, new_id.id);
	}
	function get_racers(search_term, id){
		if (search_term.length > 2){
			var result= performAJAX(data_url,"function=get_racers_search&search_term="+search_term+"&event_id="+id,false,"POST");
			//console.log(result);
			document.getElementsByClassName("results_container")[0].innerHTML ='';
			build_list(result, id);
		}else	
			alert("Search Must be more than 2 characters");
		
	}
	function build_list(racer_data,event_id){
		
		var inner_container='';
		var inner = document.getElementsByClassName("results_container")[0];
		if( document.getElementsByClassName("results_container").length){
			var inner_children = inner.children;
			for( var i=0; i<inner_children.length; i++){
				if(!(inner_children[i].getElementsByClassName("check_class")[0].hasAttribute("checked")))
					inner.children[i]='';
			}
			inner_container = document.getElementsByClassName("results_container")[0];
		}else{
			inner_container = document.createElement("div");
			inner_container.className='results_container';
		}
		for(var i=0; i<racer_data.length; i++){
			if( document.getElementById("racer_id_"+racer_data[i].id))
				continue;
			var container   = document.createElement("div");
			var check_div   = document.createElement("div");
			var b_day_div   = document.createElement("div");
			var name_div    = document.createElement("div");
			var l_name_div  = document.createElement("div");
			var f_name_div  = document.createElement("div");
			var credits_div = document.createElement("div");
			
			check_div.className= 'check_div';
			//check_div.innerHTML= "<input type='checkbox' class='check_class' onchange='add_racer(\""+racer_data[i].id+"\",\""+event_id+"\", this)'>";
			var check_box = document.createElement("input");
				check_box.setAttribute("type","checkbox");
				check_box.className ="check_class";
				check_box.id = "racer_id_"+racer_data[i].id;
				check_box.setAttribute("racer_id", racer_data[i].id);
				check_box.setAttribute("event_id", event_id);	
				check_box.addEventListener("change", add_racer);
				
			check_div.appendChild(check_box);

			
			name_div.className    = 'racer_name_div';
			name_div.innerHTML	  = racer_data[i].racer_name;
			
			f_name_div.className  = 'first_name_div';
			f_name_div.innerHTML  = racer_data[i].f_name;
			
			l_name_div.className  = 'last_name_div';
			l_name_div.innerHTML  = racer_data[i].l_name;
			
			b_day_div.className   = 'b_day_div';
			var b_date = new Date( racer_data[i].birth_date);
			var age =calc_age(b_date.getMonth(),b_date.getDay(),b_date.getFullYear());

			if(age < 16){
				b_day_div.style.color = '#580000';
				//b_day_div.innerHTML	  = "<span style='color:#580000'>"+racer_data[i].birth_date+"</span>";
			}//else
				b_day_div.innerHTML	  = racer_data[i].birth_date;

		
			credits_div.className = 'num_credits_remaining';
			credits_div.innerHTML = racer_data[i].credits;
			
			container.className   = 'racer_row_container';
			container.appendChild(check_div);
			container.appendChild(name_div   );
			container.appendChild(f_name_div );
			container.appendChild(l_name_div );
			container.appendChild(b_day_div);
			container.appendChild(credits_div);
			inner_container.appendChild(container);
		}
		document.getElementById("overlay").appendChild(inner_container);
	}
	function loading(){
		document.getElementById("loading").style.display='block';
	}
	function done_loading(){
		document.getElementById("loading").style.display='none';
	}
	function performAJAX(URL, urlString,asyncSetting,type, callback ){
		var returnVal='';
		//if(asyncSetting == false)
		//loading();
			
		current_ajax= $.ajax({ 
			url:   URL,
			type:  type,
			data:  urlString,
			async: asyncSetting,
			success: 
			function (string){
				//done_loading();
				//console.log(string);
				if(check_json(string)){
					var obj= {};
					if(string)
						obj = jQuery.parseJSON( string );
					else {
						
						obj.error = 'No results returned url string was: '+ urlString+ "The return string is: "+string;
					}
					if(obj.error){
						alert(obj.error);
						close_overlay();
						initialize(get_selected_day());
						return;
					}
					if(callback){
						callback(obj);
					}else{
						returnVal= obj;
					}
				}else{
					alert("error checking json: "+string);
				}
			}
		});	
		return returnVal; 
	}
	function start_move(event){
		event.preventDefault();
		var elem= event.target;
		if(elem.previousSibling && elem.previousSibling.className == 'vert_line')
			elem= elem.parentNode;
		else if(elem.className.indexOf('containing_cell') != -1)
			elem = elem.parentNode;	
		else if(elem.className == 'race_type')
			elem= elem.parentNode.parentNode;
		
		
		
		var selected_cell = document.getElementsByClassName("cell_selected")[0];
		if(selected_cell){		// if we already have a selected cell, then we are merging a race. 
			if(selected_cell != elem){
				var event_id= selected_cell.getAttribute('event_id');
				var time = elem.parentNode.id;
				var id = elem.getAttribute("track_id");
				move_race(time, event_id, id, selected_cell, elem);
			}else{
				elem.removeAttribute("selected");
				elem.className = elem.className.replace(/\bcell_selected\b/,'');	
				elem.style.backgroundColor='';
			}
			
		}else{
			if(elem.className.indexOf("current_race")== -1){
				elem.className = elem.className+' cell_selected';
				elem.setAttribute("selected",1);
			}else 
				alert("Cannot move currently running race");
		}
	}
	function close_overlay(elem){
		var overlay= document.getElementById("overlay");
		var close= document.getElementById("close_overlay");
		overlay.innerHTML='';
		//window.location = '_DO:cmd=send_js&c_name=p2r_scheduling_wrapper&js=unhighlight_track_name()';
		//document.getElementsByClassName("header_selected")[0].className=document.getElementsByClassName("header_selected")[0].className.replace(/\bheader_selected\b/,'')
		if(document.getElementsByClassName("time_selected").length) 
		document.getElementsByClassName("time_selected")[0].className=document.getElementsByClassName("time_selected")[0].className.replace(/\btime_selected\b/,'')
		if(close){
			overlay.appendChild(close);
			close.style.top='5px';
		}
		document.getElementById("overlay_container").style.display='none';
	}
	function check_json(text){
		
		if (/^[\],:{}\s]*$/.test(text.replace(/\\["\\\/bfnrtu]/g, '@').
		replace(/"[^"\\\n\r]*"|true|false|null|-?\d+(?:\.\d*)?(?:[eE][+\-]?\d+)?/g, ']').
		replace(/(?:^|:|,)(?:\s*\[)+/g, ''))) {
			return true;
		}else{
	  		return false;
	  	}
	}

	/*************************HELPER VALIDATION FUNCTIONS ****************/
	function not_empty(){
		return true;
	}
	function is_numeric(n){
		return !isNaN(parseFloat(n)) && isFinite(n);
	}
	function is_date(){
		
	}
	function is_text(){
		
	}
	function is_email(){
		
	}
	function get_durations(room_id, evt_id, time){
		var ret_arr   = new Array();
		var rooms_all = document.getElementsByClassName("room_cell");
		var cur_duration = interval;
		
		ret_arr.push(cur_duration);
		for( var i = 0; i < rooms_all.length; i++){
			if( rooms_all[i].getAttribute("room_id") == room_id && rooms_all[i].parentNode.id > time){
				if(rooms_all[i].className == 'room_cell' || rooms_all[i].className.indexOf("room_event_"+evt_id)!=-1){
					cur_duration+=interval;	
					ret_arr.push(cur_duration);
				}else{
					break;
				}
			}
		}
		return ret_arr;
			
	}
	function available_times(room_id, evt_id){
		
		var ret_arr= new Array();
		var rooms_all= document.getElementsByClassName("room_cell");
		var size= document.getElementsByClassName("room_event_"+evt_id).length;
		for(var i = 0; i < rooms_all.length; i++){
			if( rooms_all[i].getAttribute("room_id") == room_id){
				
				if(rooms_all[i].className=='room_cell' || rooms_all[i].className.indexOf("room_event_"+evt_id)!=-1)
					ret_arr.push(rooms_all[i].parentNode.id);	
				else{
					ret_arr.push(rooms_all[i].parentNode.id.replace(/\d/g, function (digit) { return digit + "\u0336" }));
				}
			}
		}
		return ret_arr;
	}
	function update_schedule(){
		setTimeout(update_races_completed, 10000);
	}
	function update_races_completed(){
		var all_races = performAJAX(data_url,"function=get_all_races_for_update&date="+get_selected_day(),false,"POST");
		var cells   	 = document.getElementsByClassName("track_cell_with_contents");
		for(var i=0; i< cells.length; i++){
			cells[i].className ='track_cell_with_contents';
			var race_info = all_races[cells[i].getAttribute("event_id")];
			try{
				//pseudo fix
				if(race_info){
					if(race_info.ms_end == '' && race_info.ms_start!=''){
						cells[i].className='current_race '+ cells[i].className;
						cells[i].style.backgroundColor = "#aecd37";
					}else if (race_info.ms_end != '' && race_info.ms_start!=''){
						cells[i].style.backgroundColor= '';
						cells[i].className='past_race '+ cells[i].className;
					}
				}
			}catch(e){
				console.log("error in update_race_completed: "+e+" The info was: ");
				console.log(race_info);
				console.log(cells[i].getAttribute("event_id"));
			}
		}
		
		/*
		var date    	 = new Date();
		var mins		 = date.getMinutes();
		var hrs 		 = date.getHours();
		if(mins < 10){
			mins= "0"+mins;
		}
		if(mins == 0)
			mins = 50;
		if(hrs < 10) 
			hrs = "0"+hrs;
		var current_time =  hrs+""+mins;
		
		for(var i=0; i< cells.length; i++){
			if(cells[i].innerHTML !='.'){
				var time  = cells[i].parentNode.id;
				if(((parseInt(current_time) - parseInt(time)) >= 0 && (parseInt(current_time) - parseInt(time)) <= 10) && cells[i].className.indexOf("current_race")==-1 ){
					cells[i].className='current_race '+ cells[i].className;
					cells[i].style.backgroundColor = "#aecd37";
				}else if( ((parseInt(current_time) - parseInt(time)) > 0 && (parseInt(current_time) - parseInt(time)) >= 10) && cells[i].className.indexOf("past_race")==-1 ){
					cells[i].className = cells[i].className.replace(/\bcurrent_race\b/,'');	
					cells[i].style.backgroundColor= '';
					cells[i].className='past_race '+ cells[i].className;
				

				}
				/*else if((hrs == time.substring(0,2)) &&(time.substring(2) < mins && mins > time.substring(2)) ){
					cells[i].style.backgroundColor="#aecd37";
					cells[i].style.color='white';
				}*/
		/*
			}
		}*/
		update_schedule();
	}
	function calc_age(birthMonth, birthDay, birthYear)
	{
	  todayDate = new Date();
	  todayYear = todayDate.getFullYear();
	  todayMonth = todayDate.getMonth();
	  todayDay = todayDate.getDate();
	  age = todayYear - birthYear; 
	
	  if (todayMonth < birthMonth - 1)
	  {
	    age--;
	  }
	
	  if (birthMonth - 1 == todayMonth && todayDay < birthDay)
	  {
	    age--;
	  }
	  return age;
	}
	function refresh(){// TODO 
		current_ajax.abort();
		//window.location.refresh();
		performAJAX("./check_new_events.php","function=check_new_events",true,"POST",update_grid);	
		//performAJAX(data_url,"cancel_do=1",false,"POST");
		//performAJAX(data_url,"function=check_new_events&first=1",true,"POST",update_grid);
		return "1";
	}
	function prepad(num){
		if ((typeof num )!="String")
			num = num+"";
		if( num.length ==1)
			num ="0"+num;
		
		return num; 
	}
	/*********************************************************************/
	window.onload= function(){
		var temp= new Image();
		temp.src= "./images/close_icon.png";
		initialize();
		var d   = new Date();
		var hr	= d.getHours();
		var min = d.getMinutes();
		if( hr < 10)
			hr= "0"+hr;
		min= Math.round(min / 10) * 10;
		if(min==60||min==0)
			min="00";
		
		setTimeout(function(){
			//console.log(hr+" "+min);
			if(document.getElementById(hr+""+min))
				document.getElementById(hr+""+min).scrollIntoView();
		}, 250);
		
	}; 
	
				
			/*
			var mai_day= "_"+d;
			if(month_races[mai_day]){
				var races_container= document.createElement("div");
				races_container.className="races_container";
				for(var hours = 0; hours < 16; hours++){
					var h = ((9+hours)%12)+1;
					for(minutes = 0; minutes < 60; minutes+=interval){
						if( minutes < 10)
							var time= h.toString()+"0"+minutes.toString();
						else
							var time= h.toString()+minutes.toString();
						
						var new_elem= document.createElement("div");
						
						if(month_races[mai_day].indexOf(time)!=-1){
							new_elem.className="full_race_time";
						}else{
							new_elem.className="empty_race_time";	
						}		
						races_container.appendChild(new_elem);
					}
				}
				td.appendChild(races_container);
			}*/