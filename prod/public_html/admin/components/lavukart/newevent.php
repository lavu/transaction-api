<?php
    //error_log("newevent dev");
	require_once(dirname(__FILE__) . "/../comconnect.php");
	
?>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>New Event</title>
<style type="text/css">
<!--

body, table {
	font-family:Verdana, Arial, Helvetica, sans-serif;
	-webkit-tap-highlight-color:rgba(0,0,0,0);
}

.caltext {
	font-family: "Trebuchet MS", Arial, Helvetica, sans-serifVerdana, Geneva, sans-serif;
	font-size: 16px;
	font-weight: bold;
	color: #154473;
}
-->
</style>
</head>

<body style='margin:0px; background:transparent'>

<table width="423" height="334" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td colspan="5" align="center" valign="top">
		<?php
			$date = reqvar("date");
			$time = reqvar("time");
			$time_span = reqvar("time_span");
			$locid = reqvar("locid");
			$trackid = reqvar("trackid");
			$parentcom = reqvar("parentcom","events");
			
			$scrolltop = reqvar("scrolltop");
			
			$date_parts = explode("-",$date);
			$year = $date_parts[0];
			$month = $date_parts[1];
			$day = $date_parts[2];
			
			$mode = reqvar("mode");
			if($mode=="add_event")
			{
				$set_date = $date;
				$t = $time;
				$set_type = reqvar("event_type");
				$set_locationid = $locid;
				$set_trackid = $trackid;			
				
				$dparts = explode("-",$set_date);
				$set_year = $dparts[0];
				$set_month = $dparts[1];
				$set_day = $dparts[2];
				$set_min = $t % 100;
				$set_hour = (($t - $set_min) / 100);
				$set_time = $t;
				$set_ts = mktime($set_hour,$set_min,0,$set_month,$set_day,$set_year);

				$exist_query = lavu_query("select * from `lk_events` where `date`='[1]' and (`time` * 1='[2]' * 1) and `trackid`='[3]'",$date,$time,$trackid);
				if(mysqli_num_rows($exist_query))
				{
					echo "<td>Cannot create event.<br>An Event already exists for this timeslot.</td>";
					return;
				}
				else
				{
					lavu_query("insert into `lk_events` (`locationid`,`type`,`date`,`time`,`ts`,`trackid`) values ('$set_locationid','$set_type','$set_date','$set_time','$set_ts','$set_trackid')");
					$new_eventid = lavu_insert_id();
					
					$set_desc = "New";
					$etype_query = lavu_query("select * from `lk_event_types` where `id`='[1]' AND `_deleted` != '1' ORDER BY `_order` ASC",$set_type);
					if(mysqli_num_rows($etype_query))
					{
						$etype_read = mysqli_fetch_assoc($etype_query);
						$set_desc = ucfirst(substr($etype_read['title'],0,3));
					}
					
					echo <<<JAVASCRIPT

				<script type='text/javascript'>
					window.location = "_DO:cmd=make_hidden&c_name=customers_wrapper_DO:cmd=send_js&c_name=schedule&js=new_race_created('{$set_date}','{$set_time}','{$new_eventid}','{$set_trackid}','{$set_type}','{$set_desc}','1')";	
				</script>
			</td>
		</tr>
	</table>
</body>
</html>
JAVASCRIPT;
				}
				return;
			}
			else if($mode=="add_seq_event")
			{
				function time_advance($time, $add)
				{
					$time_hours = $time - ($time % 100);
					$time_minutes = $time % 100;
					$add_hours = $add - ($add % 100);
					$add_minutes = $add % 100;
					$hours = $time_hours + $add_hours;
					$minutes = $time_minutes + $add_minutes;
					if($minutes >= 60)
					{
						$hours += 100;
						$minutes -= 60;
					}
					return $hours + $minutes;
				}
								
				$set_date = $date;
				$set_seq_type = reqvar("seq_event_type");
				$set_locationid = $locid;
				$set_trackid = $trackid;
				$t = $time;
				
				$dparts = explode("-",$set_date);
				$set_year = $dparts[0];
				$set_month = $dparts[1];
				$set_day = $dparts[2];
				
				$seq_query = lavu_query("select * from `lk_event_sequences` where `id`='[1]'",$set_seq_type);
				if(mysqli_num_rows($seq_query))
				{
					$seq_read = mysqli_fetch_assoc($seq_query);
					$seq_slots = $seq_read['slots'];
					$seq_event_list = explode(",",$seq_read['event_list']);
				
					$racer_count = reqvar("racer_count");
			
					$race_sets = 1;
					if ($seq_slots!=0 && $seq_slots!="" && $racer_count!=0 && $racer_count!="") $race_sets = ceil($racer_count / $seq_slots);
	
					$next_seq_id = 1;
					$max_seq_query = lavu_query("select max(`groupid` * 1) as `max_group_id` from `lk_events`");
					if(mysqli_num_rows($max_seq_query))
					{
						$max_seq_read = mysqli_fetch_assoc($max_seq_query);
						$max_seq_id = $max_seq_read['max_group_id'];
						if(is_numeric($max_seq_id)) $next_seq_id = ($max_seq_id * 1) + 1;
					}
					
					$js_update_script = "";
					for($i=0; $i<count($seq_event_list); $i++)
					{
						$set_type = trim($seq_event_list[$i]);
					
						$set_desc = "New";
						$etype_query = lavu_query("select * from `lk_event_types` where `id`='[1]' AND `_deleted` = '0' ORDER BY `_order` ASC",$set_type);
						if(mysqli_num_rows($etype_query))
						{
							$etype_read = mysqli_fetch_assoc($etype_query);
							$set_desc = ucfirst(substr($etype_read['title'],0,3));
						}

						for ($s = 0; $s < $race_sets; $s++) {
						
							$set_min = $t % 100;
							$set_hour = (($t - $set_min) / 100);
							$set_time = $t;
							$set_ts = mktime($set_hour,$set_min,0,$set_month,$set_day,$set_year);
							
							$check_time_slot = lavu_query("SELECT `id` FROM `lk_events` WHERE `locationid` = '[1]' AND `date` = '[2]' AND `time` = '[3]' AND `trackid` = '[4]'", $set_locationid, $set_date, $set_time, $set_trackid);
							if (mysqli_num_rows($check_time_slot)) {
								$s--;
							} else {
								echo "<br>" . $set_desc . " - " . $t;
								lavu_query("insert into `lk_events` (`locationid`,`type`,`date`,`time`,`ts`,`trackid`,`groupid`,`heat`,`sequence`,`sequenceid`,`group_racer_count`) values ('$set_locationid','$set_type','$set_date','$set_time','$set_ts','$set_trackid','$next_seq_id','".($i + 1)."','".$seq_read['title']."','".$seq_read['id']."','$racer_count')");
								$eventid = ConnectionHub::getConn('rest')->insertID();
								if($js_update_script=="")
									$js_update_autoopen = "1";
								else
									$js_update_autoopen = "0";
								$js_update_script = "_DO:cmd=send_js&c_name=schedule&js=new_race_created(\'$set_date\',\'$set_time\',\'$eventid\',\'$set_trackid\',\'$set_type\',\'$set_desc\',\'$js_update_autoopen\',\'$next_seq_id\')" . $js_update_script;
							}
								
							$t = time_advance($t, $time_span);
						}
					}
					echo "<script language='javascript'>";
					echo "  window.location = '_DO:cmd=make_hidden&c_name=customers_wrapper" . $js_update_script . "'; ";
					echo "</script>";
				}
				else echo "Cannot find Sequence";
				
				return;
			}
			
			if($date && $time)
			{
	        	echo "&nbsp;<br><span style='color:#ffffff'>New Event For <b>" . display_date($date) . " - " . display_time($time) . "</b>:</font>";
				echo "<br><br>";
				echo "<table cellspacing=4 cellpadding=12>";
				echo "<tr>";
				
				$exist_query = lavu_query("select * from `lk_events` where `date`='[1]' and (`time` * 1='[2]' * 1) and `trackid`='[3]'",$date,$time,$trackid);
				if(mysqli_num_rows($exist_query))
				{
					echo "<td>Cannot create event.<br>An Event already exists for this timeslot.</td>";
				}
				else
				{
					$col = 1;
					$type_query = lavu_query("select * from `lk_event_types` where `locationid`='[1]' and `_deleted`!='1' order by `_order` asc",$locid);
					while($type_read = mysqli_fetch_assoc($type_query))
					{
						if($parentcom=="schedule")
						{
							/*echo "<td style='border:solid 1px #777777; padding-left:40px; padding-right:40px' bgcolor='#ffffff' onclick='window.location = \"_DO:cmd=make_hidden&c_name=customers_wrapper_DO:cmd=load_url&c_name=".$parentcom."&f_name=lavukart/".$parentcom.".php?mode=add_event&auto_select_new=1&event_date=$date&event_time=$time&event_type=".$type_read['id']."&event_locationid=$locid&event_trackid=$trackid&year=$year&month=$month&day=$day&set_scrolltop=$scrolltop\"'>" . $type_read['title'] . "</td>";*/
							echo "<td style='border:solid 1px #777777; padding-left:40px; padding-right:40px' bgcolor='#ffffff' onclick='window.location = \"_DO:cmd=load_url&c_name=customers_wrapper&f_name=lavukart/newevent.php?mode=add_event&parentcom=".$parentcom."&auto_select_new=1&event_type=".$type_read['id']."&trackid=$trackid&date=$date&time=$time&time_span=$time_span&locid=$locid&set_scrolltop=$scrolltop\"'>" . $type_read['title'] . "</td>";
							//echo "<td style='border:solid 1px #777777; padding-left:40px; padding-right:40px' bgcolor='#ffffff' onclick='window.location = \"_DO:cmd=number_pad&c_name=customers_wrapper&title=Number%20of%20Racers&type=multi_digit&max_digits=3&send_cmd=cmd=load_url%26c_name=customers_wrapper%26f_name=lavukart/newevent.php%3Fmode=add_event%26parentcom=".$parentcom."%26auto_select_new=1%26event_type=".$type_read['id']."%26trackid=$trackid%26date=$date%26time=$time%26time_span=$time_span%26locid=$locid%26set_scrolltop=$scrolltop%26racer_count=ENTERED_NUMBER\";'>" . $type_read['title'] . "</td>";
						}
						else
						{
							echo "<td style='border:solid 1px #777777; padding-left:40px; padding-right:40px' bgcolor='#ffffff' onclick='window.location = \"_DO:cmd=close_overlay&c_name=".$parentcom."&f_name=lavukart/".$parentcom.".php?mode=add_event&event_date=$date&event_time=$time&event_type=".$type_read['id']."&event_locationid=$locid&event_trackid=$trackid&year=$year&month=$month&day=$day&set_scrolltop=$scrolltop\"'>" . $type_read['title'] . "</td>";
						}
						$col++;
						if($col>2) {echo "</tr><tr>"; $col = 1;}
					}
					$seq_type_query = lavu_query("select * from `lk_event_sequences` where `locationid`='[1]' order by `_order` asc",$locid);
					while($seq_type_read = mysqli_fetch_assoc($seq_type_query))
					{
						if($parentcom=="schedule")
						{
							//echo "<td style='border:solid 1px #777777; padding-left:40px; padding-right:40px' bgcolor='#ffffff' onclick='window.location = \"_DO:cmd=load_url&c_name=customers_wrapper&f_name=lavukart/newevent.php?mode=add_seq_event&parentcom=".$parentcom."&auto_select_new=1&seq_event_type=".$seq_type_read['id']."&trackid=$trackid&date=$date&time=$time&time_span=$time_span&locid=$locid&set_scrolltop=$scrolltop\"'>" . $seq_type_read['title'] . "</td>";
							echo "<td style='border:solid 1px #777777; padding-left:40px; padding-right:40px' bgcolor='#ffffff' onclick='window.location = \"_DO:cmd=number_pad&c_name=customers_wrapper&title=Number%20of%20Racers&type=multi_digit&max_digits=3&send_cmd=cmd=load_url%26c_name=customers_wrapper%26f_name=lavukart/newevent.php%3Fmode=add_seq_event%26parentcom=".$parentcom."%26auto_select_new=1%26seq_event_type=".$seq_type_read['id']."%26trackid=$trackid%26date=$date%26time=$time%26time_span=$time_span%26locid=$locid%26set_scrolltop=$scrolltop%26racer_count=ENTERED_NUMBER\";'>" . $seq_type_read['title'] . "</td>";
						}
						$col++;
						if($col>2) {echo "</tr><tr>"; $col = 1;}
					}
					echo "</tr>";
					echo "</table>";
				}
			}
			else
			{
				echo "Error: No Date/Time Sent";
			}
		?>
    </td>
  </tr>
</table>
</body>
