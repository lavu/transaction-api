<?php
	require_once(dirname(__FILE__) . "/../comconnect.php");
?>	

<html>
	<head>
		<title>Group Event Info</title>
		<style type="text/css">
<!--
body {
	background-color: transparent;
	margin-left: 0px;
	margin-top: 0px;
	margin-right: 0px;
	margin-bottom: 0px;
}

.creditnumber {
	font-family: Verdana, Geneva, sans-serif;
	font-size: 13px;
	font-weight: bold;
	color: #3d4f6d;
}
.gridposition {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 14px;
	font-weight: bold;
	color: #5A75A0;
}
.icontext {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 11px;
	font-weight: bold;
	color: #5A75A0;
}
.nametext {
	font-family: Verdana, Geneva, sans-serif;
	font-size: 13px;
	font-weight: bold;
}
.title {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 16px;
	font-weight: bold;
	color: #5A75A0;
}
.subtitles {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 13px;
	font-weight: bold;
	color: #5A75A0;
}
.checklist_subtitles {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 12px;
	font-weight: bold;
	color: #5A75A0;
}
.bg_signature_top {
	background-image:URL(images/bg_signature_top.png);
	background-repeat:no-repeat;
	width:400px;
	height:9px;
}
.bg_signature_mid {
	background-image:URL(images/bg_signature_mid.png);
	background-repeat:repeat-y;
	width:400px;
}
.bg_signature_bottom {
	background-image:URL(images/bg_signature_bottom.png);
	background-repeat:no-repeat;
	width:400px;
	height:9px;
}
-->
		</style>
		
<script type="text/javascript">
	
	function checkDurationTime(){
		/* This function ensures that a user chooses a duration time that is a multiple of the timepspan. If they choose a value that is not a multiple of 8 the event will be erased so we must prevent this */
			durationVal = document.getElementById("duration").value;
		//thing = durationVal % 8;
		//alert("thing: " + thing);
			if ( (durationVal % default_timespan)  != 0){
						window.location = '_DO:cmd=alert&title=Invalid Duration&message=Error: Please note the duration of the event must be a multiple of ' + default_timespan + '.';
						//thing = durationVal % 8;
						//alert("thing: " + thing);
						return false;
			}//if
			
				//document.info_form.submit();
				document.forms["info_form"].submit();
	}//checkDurationTime()
	
	
		
		
</script>
<?php

/*
	$use_roomid = "R".$contents[$i][1];
												$onclick = " onclick=\"empty_room_click('tab_bg_51_empty','$use_roomid','$rowcount','','cmd=make_hidden&c_name=customers','cmd=load_url&c_name=customers_wrapper&f_name=lavukart/schedule_group.php?group_event_id=NEW&event_date=".$date."&event_time=".$slot."&event_room=".$contents[$i][2]."&move_room=true"."&use_room_id=".$use_roomid."&time_span=".$time_span."&event_id=".$eventid."&selected_row=".$rowcount."-".$contents[$i][1]."&abc=123','$time_span','cmd=send_js&c_name=customers_wrapper&js=set_end_time(".$rowcount.")','cmd=set_scrollable&c_name=customers_wrapper&set_to=1')\"";
*/
	


	$double_click = reqvar("double_click");
	
	/*echo "<script type=\"text/javascript\">alert(\"double click:  $double_click\");</script>";*/
	
	if ($double_click == "true"){
			/*echo "<script type=\"text/javascript\">alert(\" == true\");</script>";*/
			$_SESSION['canMoveTheRoom'] = "true";
			$_SESSION['timesDoubleClicked'] = 1;
	}//if
	
	
	
	
	
	$should_move_room = reqvar("should_move_room");
	
	
	
		$event_date = reqvar("event_date");
	$event_time = reqvar("event_time");
	$time_span = reqvar("time_span");
	$move_room = reqvar("move_room");
	$event_id = reqvar("event_id");
	$group_event_id = reqvar("group_event_id");
	$ck_id = "must find this";
	
	/*echo "<script type=\"text/javascript\">alert(\"event_date: $event_date  event_time: $event_time;  time_span: $time_span  move_room: $move_room  event_id: $event_id  group_event_id: $group_event_id \");</script>";*/
	
	
	$room_move_from= reqvar("room_move_from");
	$room_move_to= reqvar("room_move_to");
	
	if ( ($room_move_from != "") && ($room_move_to != "") ){
				$use_date = reqvar("use_date");
				
				
				
				//$move_room_event_update_results = lavu_query("UPDATE `lk_group_events` SET event_time='$event_time', event_date='$event_date', room_id='$use_room_id' WHERE id='$ck_move_this_group_event_id'");
				$pieces_from = explode("-", $room_move_from);
				$db_look_up_room_id = convertWrongRoomID($pieces_from[0]);
				$db_look_up_room_time = $pieces_from[1];
				
				$pieces_to = explode("-", $room_move_to);
				$db_look_up_room_id_to = convertWrongRoomID($pieces_to[0]);
				$db_look_up_room_time_to = $pieces_to[1];
			/*	echo "<script type=\"text/javascript\">alert(\"db+look_up_room_id: $db_look_up_room_id; db_look_up_room_time: $db_look_up_room_time\");</script>";*/
				
				$get_info_query_results = lavu_query("SELECT * FROM `lk_group_events` WHERE `event_date`='$use_date' AND `room_id`='$db_look_up_room_id' AND `event_time`='$db_look_up_room_time'");
				
				$use_rows = mysqli_fetch_assoc($get_info_query_results);
				$use_id = $use_rows['id'];
				
			/*	echo "<script type=\"text/javascript\">alert(\"here we go: use_id: $use_id; use_date: $use_date; room_move_from: $room_move_from; room_move_to: $room_move_to; event_time: $db_look_up_room_time; event_date: $use_date; room_id: $db_look_up_room_id; id:\");</script>";
			*/
				//need date in here
				
				
				/* Make sure moving event will not overwrite other event*/
				
				//get event-to-be-moved info
				$move_room_event_results = lavu_query("SELECT * FROM `lk_group_events` WHERE id='$use_id'");
					
				$move_room_event_row = mysqli_fetch_assoc($move_room_event_results);
						
				
				$current_event_start_duration = $move_room_event_row['duration'];
				$current_event_start_id = $move_room_event_row['id'];
		
		/*
				echo "<script type=\"text/javascript\">alert(\"current_event_start_duration: $current_event_start_duration; db_look_up_room_time_to: $db_look_up_room_time_to; db_look_up_room_id_to: $db_look_up_room_id_to; current_event_start_id: $current_event_start_id;\");</script>"; 
			*/
			
				$getAllRoomResults = lavu_query("SELECT * FROM `lk_group_events` WHERE room_id='$db_look_up_room_id_to' AND event_date='$use_date' AND id!='$current_event_start_id' AND _deleted!='1'");
				
				$user_start_time_selected = strtotime($db_look_up_room_time_to);
				$user_end_time_selected = $user_start_time_selected + ($current_event_start_duration * 60);
				
				$canMoveHere = true; //assume you can move the event
				$stayInWhileLoop = true;
				while ( ($checkTimeRows = mysqli_fetch_assoc($getAllRoomResults) ) && ($stayInWhileLoop == true) ){
					
						$db_begin_time = strtotime($checkTimeRows['event_time']);
						$db_end_time = $db_begin_time + ($checkTimeRows['duration'] * 60);
						/*
					echo "<script type=\"text/javascript\">alert(\"user_start_time_selected: $user_start_time_selected; user_end_time_selected: $user_end_time_selected; db_begin_time: $db_begin_time; db_end_time: $db_end_time; \");</script>"; 
							*/
						if ( ($db_end_time > $user_start_time_selected) && ($db_begin_time < $user_end_time_selected) ){
											
													echo "<script type=\"text/javascript\">alert(\"Time conflict: Cannot move event here.\");</script>";
													
													$canMoveHere = false;
													$stayInWhileLoop = false; //break out of while loop
													
													
						}//if
					
				}//while
				
					/* END- Make sure moving event will not overwrite other event*/
				
				if ($canMoveHere == true){
							$move_room_event_update_results = lavu_query("UPDATE `lk_group_events` SET event_time='$db_look_up_room_time_to', event_date='$use_date', room_id='$db_look_up_room_id_to' WHERE id='$use_id'");
							/*	echo "<script type=\"text/javascript\">alert(\"use_date:$use_date; event_date: $event_date; db_look_up_room_id: $db_look_up_room_time; time_span: $time_span; use_id: $use_id; db_look_up_room_id: $db_look_up_room_id; use_date: $use_date; db_look_up_room_time_to: $db_look_up_room_time_to; time_span: $time_span; use_id: $use_id; db_look_up_room_id_to: $db_look_up_room_id_to\");</script>";  */
					//
				//$time_span = "0010";
					echo "<script language='javascript'>window.location = '_DO:cmd=send_js&c_name=schedule&js=group_event_moved(\'$event_date\',\'$db_look_up_room_time\',\'$time_span\',\'$use_id\',\'$db_look_up_room_id\',\'$use_date\',\'$db_look_up_room_time_to\',\'$time_span\',\'$use_id\',\'$db_look_up_room_id_to\',\'$current_event_start_duration\')';</script>";//first five are from, next five are to
				}//if
				
				
	}//if
	
	
	
	
	
	

?>
		<script language="javascript">
		
			function cancel_or_delete(CorD,group_event_id,event_date,event_time,time_span,event_room,selected_row) {
			
				window.location = "?CorD=" + CorD + "&group_event_id=" + group_event_id + "&event_date=" + event_date + "&event_time=" + event_time + "&time_span=" + time_span + "&event_room=" + event_room + "&selected_row=" + selected_row;
			}
			
			function time_advance(tm,ad)
			{
				tm_mins = tm % 100;
				tm_hours = (tm - tm_mins) / 100;
				ad_mins = ad % 100;
				ad_hours = (ad - ad_mins) / 100;
				
				hours = tm_hours + ad_hours;
				mins = tm_mins + ad_mins;
				
				if(ad < 0)
				{
					if(mins < 0)
					{
						hours = hours - 1;
						mins = mins + 60;
					}
				}
				else
				{
					if(mins >= 60)
					{
						hours = hours + 1;
						mins = mins - 60;
					}
				}
				return hours * 100 + mins * 1;
			}
			
			function set_end_time(set_slot) {
				set_dur = time_advance(set_slot, (0 - <?php echo $event_time?>));
				set_dur = time_advance(set_dur, <?php echo ((int)$time_span * 1)?>);
				set_dur_mins = set_dur % 100;
				set_dur_hours = (set_dur - set_dur_mins) / 100;
				set_dur = (set_dur_hours * 60) + set_dur_mins;
				document.getElementById("duration").value = set_dur;
			}
		
		
		
		</script>
		
		
	</head>

<?php

function convertWrongRoomID($input){
			/* This function was created because for room events, sometimes the first column is labeled as R0 and second as R1, and 
			sometimes they are labeled as 3 or 4. This function takes in R0 or R1 and returns 3 or 4 as the room id. Created by Chris Kirby */
			global $loc_id;
			$room_id_list = array();
			$room_query = lavu_query("select `id` as `id` from `lk_rooms` where `locationid`='$loc_id' and `_deleted`!='1' order by id asc");
			while($room_read = mysqli_fetch_assoc($room_query))
			{
					$room_id_list[] = $room_read['id'];
			}
			
			$get_room_offset = str_replace("R","",$input);
			if(isset($room_id_list[$get_room_offset])){
				/*$myVal = $room_id_list[$get_room_offset];
		echo "<script type=\"text/javascript\">alert(\"id: $myVal\");</script>"; */
					return $room_id_list[$get_room_offset];
			}
			else
					die("Error in convertWrongRoomID()");
}//convertWrongRoomID()



if ($move_room == "store"){
	
			$_SESSION['ck_store_group_event_id'] = $group_event_id;
			/*echo "<script type=\"text/javascript\">alert(\"test_do: $test_do; move_room: $move_room; event_date: $event_date; event_time: $event_time; time_span: $time_span; event_id: $event_id; group_event_id: $group_event_id; use_room_id: $use_room_id; ck_id: $ck_id\");</script>";*/
			/*echo "<script type=\"text/javascript\">alert(\"move_room=store and set.\");</script>";*/
}//if

if ($move_room == "true"){
	/*echo "<script type=\"text/javascript\">alert(\"move_room=true\");</script>";*/
	/* Move the room event */
	$group_event_id = reqvar("group_event_id");
	$use_room_id = reqvar("use_room_id");
	
	$ck_move_this_group_event_id = $_SESSION['ck_store_group_event_id'];
	$_SESSION['canMoveTheRoom'] = false;
			if ( (isset($ck_move_this_group_event_id)) && ($_SESSION['canMoveTheRoom'] == "true") ){
		  			//pull all the necessary info from database to update the event information.
						exit();
						$move_room_event_results = lavu_query("SELECT * FROM `lk_group_events` WHERE id='$ck_move_this_group_event_id'");
						$move_room_event_row = mysqli_fetch_assoc($move_room_event_results);
						//check if the event can be moved to the selected location (ie will it run over into another event?)*/
						//$current_event_time = $event_time; //the new time the user selected
						$current_event_duration = $move_room_event_row['duration'];
						$from_event_time = $move_room_event_row['event_time'];
						$current_event_date = $move_room_event_row['event_date'];
						$current_event_room_id = $move_room_event_row['room_id'];
						
						$use_room_id = convertWrongRoomID($use_room_id); //convert it to a 3 or 4 cause that's how it is in the database
						
						$utc_begin_event_time = convertMinutesAndHoursToUTC($event_time, strtotime($event_date)); //convert to seconds for utc
						$utc_end_event_time = convertMinutesAndHoursToUTC($event_time, strtotime($event_date)) + ($current_event_duration * 60 ); //convert to seconds for utc
						
					
					
					
						/* Determine if the event fits in the new location */

							
							//
							$getAllRoomEventsResults = lavu_query("SELECT * FROM `lk_group_events` WHERE _deleted != '1' AND room_id='$use_room_id' AND event_date='$current_event_date' AND id!='$ck_move_this_group_event_id'");
							
							$storeTakenTime = array(); //$storeTakenTime[0] = "beginning time; $storeTakenTime[1] = end time
							$index = 0;
							//$canMoveHere = true; //assume the event can go there
							$stayInWhileLoop = true;
							$canMoveHere = true;
							/*
							while ( ($rows = mysqli_fetch_assoc($getAllRoomEventsResults) )  ){
										
										$db_begin_time =   convertMinutesAndHoursToUTC($rows['event_time'], strtotime($rows['event_date']) );
										$db_end_time = $db_begin_time + ($rows['duration'] * 60 ); //convert to seconds
										$disp = date("m-d-Y->H:i", $db_begin_time);
										$disp1 = date("m-d-Y->H:i", $db_end_time);
								
							
										if ( ($db_end_time > $utc_begin_event_time) && ($db_begin_time < $utc_end_event_time) ){
											
													echo "<script type=\"text/javascript\">alert(\"Time conflict: Cannot move event here.\");</script>";
													
													$canMoveHere = false;
													$stayInWhileLoop = false; //break out of while loop
													
													//$storeTakenTime[0][$index] = $db_begin_time;
													//$storeTakenTime[1][$index] = $db_end_time;
													//$index++;
										}//if
																				
										
										
										
										
								//	ksort($storeTakenTime);
										
										
							}//while()
							*/
							if ($canMoveHere == true){
								$use_room_id_js = convertToJSRoomID($use_room_id);
								$current_event_room_id_js = convertToJSRoomID($current_event_room_id);
									/*echo "<script type=\"text/javascript\">alert(\"TO: event_date: $event_date; event_time: $event_time; time_span: $time_span; group_event_id: $group_event_id; event_room: $use_room_id; use_room_id: $use_room_id_js; FROM: event_date: $current_event_date; event_time: $from_event_time; time_span: $time_span; group_event_id: $ck_move_this_group_event_id; event_room: $current_event_room_id; use_room_id: $current_event_room_id_js \");</script>";*/
								
									$move_room_event_update_results = lavu_query("UPDATE `lk_group_events` SET event_time='$event_time', event_date='$event_date', room_id='$use_room_id' WHERE id='$ck_move_this_group_event_id'");
									
									echo "<script language='javascript'>window.location = '_DO:cmd=send_js&c_name=schedule&js=group_event_moved(\'$current_event_date\',\'$from_event_time\',\'$time_span\',\'$ck_move_this_group_event_id\',\'$current_event_room_id\',\'$event_date\',\'$event_time\',\'$time_span\',\'$ck_move_this_group_event_id\',\'$use_room_id\')';</script>";//first five are from, next five are to
									/* 			HERE   */
/* FINISH THIS!!!!! */
//JOB IS TO FILL IN THIS JAVASCRIPT COMMAND CORRECTLY
									/*echo "<script language='javascript'>window.location = '_DO:cmd=send_js&c_name=schedule&js=group_event_moved(\'$event_date\',\'$event_time\',\'$time_span\',\'$group_event_id\',\'$event_room\',\'$event_date\',\'$event_time\',\'$time_span\',\'$ck_move_this_group_event_id\',\'$use_room_id\')';</script>";//first five are from, next five are to*/
							
							}//if
						
						$_SESSION['canMoveTheRoom'] = "false";
						/*echo "<script type=\"text/javascript\">alert(\"event_date: $event_date; event_time: use_room_id: $use_room_id; $event_time; time_span: $time_span; move_room: $move_room; event_id: $event_id; group_event_id: $group_event_id\");</script>";
						
						echo "<script type=\"text/javascript\">alert(\"event_time: $current_event_time; duration: $current_event_duration; event_date: $current_event_date; room_id: $current_event_room_id; id_to_be_moved: $ck_move_this_group_event_id\");</script>";*/	
							unset($_SESSION['ck_store_group_event_id']);//should be at very end	
			}//if
	
			/*echo "<script type=\"text/javascript\">alert(\"move: $ck_move_this_group_event_id move_room: $move_room; event_date: $event_date; event_time: $event_time; time_span: $time_span; event_id: $event_id; group_event_id: $group_event_id; use_room_id: $use_room_id; ck_id: $ck_id\");</script>";*/
}//if
	
	function convertToJSRoomID($input){
				/* Function used to convert the database room id to the javascript room id, which are different. R0 is the first room event column in html and js, and is 3 in the ddtabase*/
				if ($input == "3"){
					$val = "R0";
					return $val;
				}//if
				
				if ($input == "4"){
					$val = "R1";
					return $val;
				}//if
				
	}//convertToJSRoomID()
	
	function convertMinutesAndHoursToUTC($minutesAndHours, $date){
		/* This function takes in a time of day (1024) for 10:24 am, and converts it to utc*/
		$minutes = substr($minutesAndHours, -2, 2);    //get farthest right two digits
		$hours = substr($minutesAndHours, -4, 2);    //get farthest left two digits
		
		$sec_minutes = $minutes * 60; //seconds
		$sec_hours = $hours * 3600;//seconds
		/*echo "<script type=\"text/javascript\">alert(\"minuts: $minutes; hours: $hours\");</script>";*/
		
		$date = $date + $sec_hours + $sec_minutes;
		
		return $date;
		
	}//convertMinutesAndHoursToUTC()
	
	$group_event_id = 0;
	if(reqvar("group_event_id")) $group_event_id = reqvar("group_event_id");
	else if(sessvar_isset("showing_group_event_id")) $group_event_id = sessvar("showing_group_event_id");
	set_sessvar("showing_group_event_id", $group_event_id);

	$server_id = 0;
	if(reqvar("server_id")) $server_id = reqvar("server_id");
	else if(sessvar_isset("server_id")) $server_id = sessvar("server_id");
	set_sessvar("server_id",$server_id);

	$event_room = reqvar("event_room");
	if(!$event_room && isset($_POST['room_id'])) $event_room = $_POST['room_id'];
	if(!$event_room && !$group_event_id)
	{
		echo "No Room Selected";
		exit();
	}
	
	$loc_id = 0;
	if(reqvar("loc_id")) $loc_id = reqvar("loc_id");
	else if(sessvar_isset("loc_id")) $loc_id = sessvar("loc_id");
	set_sessvar("loc_id", $loc_id);

	$dataname = "";
	if(reqvar("dn")) $dataname = reqvar("dn");
	else if(sessvar_isset("dataname")) $dataname = sessvar("dataname");
	set_sessvar("dataname", $dataname);
		
	$info_form = array();
	$info_form[] = array("Title", "title");
	$info_form[] = array("Room","room_id","hidden",$event_room);
	$info_form[] = array("Duration", "duration");//,"hidden",$time_span);
	$info_form[] = array("Name", "cust_name");
	$info_form[] = array("Phone", "cust_phone");
	$info_form[] = array("Email", "cust_email");
	//$info_form[] = array("Customer Sales Rep","cust_rep");	

	if (isset($_POST['save'])) {
		$info_form_save = array();
		$info_form_save = $info_form;
		$info_form_save[] = array("","item_id"); 
		$info_form_save[] = array("","notes"); 
		$info_form_save[] = array("","cust_rep"); 
		$info_form_save[] = array("", "checklist");
		if ($group_event_id == "NEW") {
			$info_form_save[] = array("", "event_date"); 
			$info_form_save[] = array("", "event_time");
			$info_form_save[] = array("", "server_id");
			$info_form_save[] = array("", "loc_id");
			$info_form_save[] = array("", "track_id");
		}
		
	  $vals = array();
		$save_fields = "";
		$save_values = "";
		$update_fields = "SET ";
		for ($c = 0; $c < count($info_form_save); $c++) {
	  	$iname = $info_form_save[$c][1];
	  	$vals[$iname] = $_POST[$iname];
			$save_fields .= "`$iname`";
			$save_values .= "'[$iname]'";
			$update_fields .= "`$iname` = '[$iname]'";
			if ($c < (count($info_form_save) - 1)) {
				$save_fields .= ", ";
				$save_values .= ", ";
				$update_fields .= ", ";
			}
		}
	
		if ($group_event_id == "NEW") {
			$create_group_event = lavu_query("INSERT INTO `lk_group_events` (".$save_fields.", `created`) VALUES (".$save_values.", now())", $vals);
			$group_event_id = lavu_insert_id();
			$new_group_event = true;
			
			/*if($create_group_event) echo "SUCCESS: $group_event_id"; else echo "FAILED";
			$ins_query = "<br>INSERT INTO `lk_group_events` (".$save_fields.", `created`) VALUES (".$save_values.", now())<br>";
			foreach($vals as $key=>$val)
			{
				$ins_query = str_replace("[".$key."]",$val,$ins_query);
			}
			echo $ins_query;*/
		} else {
			$update_group_event = lavu_query("UPDATE `lk_group_events` ".$update_fields." WHERE `id` = '".$group_event_id."'", $vals);
		}
	}
	
	if (reqvar("CorD")) {
		
		$CorD = reqvar("CorD");
		if ($CorD == 1) {
			$cancel_group_event = lavu_query("UPDATE `lk_group_events` SET `cancelled` = '1' WHERE `id` = '[1]'", $group_event_id);
			$lk_log_this = lavu_query("INSERT INTO `lk_action_log` (`action`, `time`, `user_id`, `loc_id`, `group_event_id`) VALUES ('Deleted group event', now(), '[1]', '[2]', '[3]')", $server_id, $loc_id, $group_event_id);
			
			echo "<script language='javascript'>window.location = '_DO:cmd=send_js&c_name=schedule&js=\'$event_date\',\'$event_time\',\'$time_span\',\'$group_event_id\',\'$event_room\')';</script>";
			exit();
		} else if ($CorD == 2) {
			$delete_group_event = lavu_query("UPDATE `lk_group_events` SET `_deleted` = '1' WHERE `id` = '[1]'", $group_event_id);
			$lk_log_this = lavu_query("INSERT INTO `lk_action_log` (`action`, `time`, `user_id`, `loc_id`, `group_event_id`) VALUES ('Cancelled group event', now(), '[1]', '[2]', '[3]')", $server_id, $loc_id, $group_event_id);
			
			echo "<script language='javascript'>window.location = '_DO:cmd=send_js&c_name=schedule&js=group_event_removed(\'$event_date\',\'$event_time\',\'$time_span\',\'$group_event_id\',\'$event_room\')';</script>";
			exit();
		}
		$group_event_id = "NEW";
		set_sessvar("showing_group_event_id", $group_event_id);
	}
?>
	<body>
	
		<table width="440" border="0" cellspacing="0" cellpadding="0">
			<?php			
				$checklist_value = "";
				$get_group_event_info = lavu_query("SELECT * FROM `lk_group_events` WHERE `id` = '$group_event_id'");
				if ((mysqli_num_rows($get_group_event_info) > 0) || (($group_event_id == "NEW") && ($event_date != ""))) {
					if (mysqli_num_rows($get_group_event_info) > 0) {
						$extract = mysqli_fetch_array($get_group_event_info);
						
						$checklist_value = $extract['checklist'];
						$checklist_parts = explode("|",$extract['checklist']);
						$checklist_value_ids = $checklist_parts[0];
					}
					if ($group_event_id == "NEW") {
						$form_title = "New Group Event: ";
						$show_event_time = $event_time;
					} else {
						$form_title = "Group Event: ";
						$show_event_time = str_replace(":","",$extract['event_time']);
						$event_room = $extract['room_id'];
					}
					echo "<tr><td colspan='2' class='title' align='center' style='padding:10px 0px 10px 0px'>".$form_title;
					if(isset($event_date) && $event_date!="")
						echo display_date($event_date) . " " . display_time($show_event_time);

					$loc_query = lavu_query("select * from `locations` where `id`='[1]'",$loc_id);
					$loc_read = mysqli_fetch_assoc($loc_query);
					$location_menu = $loc_read['menu_id'];
					
					$checklist_value_escaped = str_replace("\"","&quot;",$checklist_value);
					
					$checklist_code = "";
					$checklist_code .= "<input type='hidden' name='checklist' id='checklist' value=\"$checklist_value_escaped\">";
					$checklist_code .= "<table>";
					$checklist_js = "";
					$checklist_js .= "set_val = ''; set_desc = ''; ";
					$cl_cols = 0;
					$cl_query = lavu_query("select * from `config` where `location`='[1]' and `setting`='Group Event Checklist'",$loc_id);
					while($cl_read = mysqli_fetch_assoc($cl_query))
					{
						if($cl_cols==0)
						{
							$check_list .= "<tr>";
						}
						$clitem_id = $cl_read['id'];
						$clitem_name = "checklist_item_" . $cl_read['id'];
						if(strpos($checklist_value_ids,"[".$clitem_id."]")!==false)
							$clitem_code = " checked";
						else $clitem_code = "";
						
						$checklist_js .= "if(document.getElementById('$clitem_name').checked) { set_val += '[".$clitem_id."]'; if(set_desc!='') set_desc += ', '; set_desc += \"".str_replace("\"","&quot;",$cl_read['value'])."\"; } ";
						
						$checklist_code .= "<td align='right' style='width:140px' valign='middle' class='checklist_subtitles'>" . $cl_read['value'] . "</td><td valign='middle'><input name='$clitem_name' id='$clitem_name' type='checkbox' onclick='set_checklist_value(); document.getElementById(\"update_btn\").style.display = \"inline\";' style='width:32px; height:32px;'".$clitem_code."></td>";
						$cl_cols++;
						if($cl_cols > 1)
						{
							$cl_cols = 0;
							$checklist_code .= "</tr>";
						}
						else $checklist_code .= "<td width='12'>&nbsp;</td>";
					}
					$checklist_code .= "</table>";
					$jsfunc = "";
					$jsfunc .= "<script language='javascript'>";
					$jsfunc .= "function set_checklist_value() { ";
					$jsfunc .= $checklist_js;
					//$jsfunc .= "  alert('checkbox pressed'); ";
					$jsfunc .= "  document.getElementById('checklist').value = set_val + '|' + set_desc; ";
					$jsfunc .= "} ";
                    $jsfunc .= "</script>";
					$checklist_code = $jsfunc . $checklist_code;
					
					echo "</tr>
					<form name='info_form' action='' method='POST'><input type='hidden' name='time_span' value='$time_span'>
						<tr>
							<td width='127' height='46' valign='middle'>
								<table width='127' border='0' cellspacing='0' cellpadding='8'>
									<tr><td align='right' class='subtitles'>Event Type</td></tr>
								</table>
							</td>
							<td width='313' style='background:URL(images/tab_bg2.png); background-repeat:no-repeat;'>
								<table width='313' border='0' cellspacing='0' cellpadding='8'>
									<tr>
										<td class='nametext'>
											<select name='item_id' class='nametext' style='border:none; background: URL(win_overlay.png);'>";
					$get_group_event_items = lavu_query("SELECT * from `menu_items` where `menu_items`.`_deleted` = '0' AND (`menu_items`.`hidden_value2` = 'group event' OR `menu_items`.`hidden_value2` = 'facility buyout' OR `menu_items`.`hidden_value2` = 'track rental' OR `menu_items`.`hidden_value2` = 'race sequence') ORDER BY `menu_items`.`name` ASC");//`menu_items`.`id` as id, `menu_items`.`item_id` as `item_id`, `menu_items`.`name` as name FROM `menu_items` LEFT JOIN `menu_categories` ON `menu_items`.`category_id`=`menu_categories`.`id` LEFT JOIN `menu_groups` ON `menu_categories`.`group_id`=`menu_groups`.`id` WHERE `menu_groups`.`menu_id`='$location_menu' AND `menu_items`.`_deleted` = '0' AND `menu_items`.`hidden_value2` = 'group event' ORDER BY `menu_items`.`name` ASC");
					if (mysqli_num_rows($get_group_event_items) > 0) {
						$group_val_found = false;
						while ($extract_gei = mysqli_fetch_array($get_group_event_items)) {
							$selected = "";
							if ($extract_gei['id'] == $extract['item_id']) {
								$selected = " selected";
							}
							echo "<option value='".$extract_gei['id']."'".$selected.">".$extract_gei['name']."</option>";
						}
						if($extract['item_id']!="" && !$group_val_found)
						{
							$item_query = lavu_query("select * from `menu_items` where `id`='[1]'",$extract['item_id']);
							if(mysqli_num_rows($item_query))
							{
								$item_read = mysqli_fetch_assoc($item_query);
								echo "<option value='".$item_read['id']."' selected>".$item_read['name']."</option>";
							}
						}
					} else {
						echo "<option value='0'>Error loading group event items...</option>";
					}
					echo "</select>
										</td>
									</tr>
								</table>
							</td>
						</tr>";
				
					// get users to populate customer-rep dropdown
					$users_result = lavu_query("SELECT id, firstname as f_name, lastname as l_name FROM lk_sales_reps where `loc_id`='[1]'",$loc_id);

					foreach ($info_form as $info) {
						/* leave input incase of requirement change
						if($info[1]=='cust_rep'){

							echo "<tr>
								<td width='127' height='46'>
									<table width='127' border='0' cellspacing='0' cellpadding='8'>
										<tr><td align='right' class='subtitles'>".$info[0]."</td></tr>
									</table>
								</td>
								<td width='313' style='background:URL(images/tab_bg2.png); background-repeat:no-repeat;'>
									<table width='313' border='0' cellspacing='0' cellpadding='8'>
										<tr><td class='nametext'><input type=\"text\" name=\"".$info[1]."\" class=\"nametext\" style=\"border:none; background: URL(win_overlay.png);\" size=\"30\" value=\"".(isset($extract[$info[1]])?$extract[$info[1]]:$rep_name[0][0].' '.$rep_name[0][1])."\" onchange=\"document.getElementById('update_btn').style.display = 'inline';\"></td></tr>
									</table>
								</td>
							</tr>";
						} else { */
						if(isset($info[2]))
							$fieldtype = $info[2];
						else $fieldtype = "text";
						
						if($fieldtype=="hidden")
						{
							$set_value = (isset($extract[$info[1]])?$extract[$info[1]]:"");
							if($set_value=="" && isset($info[3])) $set_value = $info[3];
							echo "<input type='hidden' name='".$info[1]."' value=\"".$set_value."\" />";
						}
						else if($fieldtype=="text")
						{
							echo "<tr>
								<td width='127' height='46'>
									<table width='127' border='0' cellspacing='0' cellpadding='8'>
										<tr><td align='right' class='subtitles'>".$info[0]."</td></tr>
									</table>
								</td>
								<td width='313' style='background:URL(images/tab_bg2.png); background-repeat:no-repeat;'>
									<table width='313' border='0' cellspacing='0' cellpadding='8'>
										<tr><td class='nametext'><input type=\"text\" name=\"".$info[1]."\" id=\"".$info[1]."\" class=\"nametext\" style=\"border:none; background: URL(win_overlay.png);\" size=\"30\" value=\"".(isset($extract[$info[1]])?$extract[$info[1]]:"")."\" onchange=\"document.getElementById('update_btn').style.display = 'inline';\"></td></tr>
									</table>
								</td>
							</tr>";
						}
						//}
					} //end foreach
					
					echo "<tr>
						<td width='127' height='46'>
							<table width='127' border='0' cellspacing='0' cellpadding='8'>
								<tr><td align='right' class='subtitles'>Customer Sales Rep</td></tr>
							</table>
						</td>
							<td width='313' style='background:URL(images/tab_bg2.png); background-repeat:no-repeat;'>
								<table width='313' border='0' cellspacing='0' cellpadding='8'>								
								<tr><td class='nametext'><select name='cust_rep' class='nametext' style='border:none; background: URL(win_overlay.png);' onchange=\"document.getElementById('update_btn').style.display = 'inline';\"><option value=''>-- Please Select --</option>";
								// iterate thru users for dropdown
								// select current user logged in if new else on edit select user that was previously selected
								while ($row = mysqli_fetch_assoc($users_result)) {
									if (($row['f_name'].' '.$row['l_name']) == $extract['cust_rep']) {
										echo "<option value='".$row['f_name'].' '.$row['l_name']."' selected='selected'>".$row['f_name'].' '.$row['l_name']. "</option>";
									} elseif ($row['id'] == $server_id) {
											echo "<option value='".$row['f_name'].' '.$row['l_name']."' selected='selected'>".$row['f_name'].' '.$row['l_name']. "</option>";
									} else {

										echo "<option value='".$row['f_name'].' '.$row['l_name']."'>".$row['f_name'].' '.$row['l_name']. "</option>";
									}

								}
								echo "</select></td></tr>
								</table>
							</td>
						</tr>";
								
					echo "<tr>
							<td width='127' height='150' valign='top'>
								<table width='127' border='0' cellspacing='0' cellpadding='8'>
									<tr><td align='right' class='subtitles'>Notes</td></tr>
								</table>
							</td>
							<td width='313' style='background:URL(images/tab_bg2_area.png); background-repeat:no-repeat;'>
								<table width='313' border='0' cellspacing='0' cellpadding='8'>
									<tr><td class='nametext'><textarea name=\"notes\" class=\"nametext\" style=\"border:none; background: URL(win_overlay.png);\" rows=\"7\" cols=\"30\" onchange=\"document.getElementById('update_btn').style.display = 'inline';\" />".(isset($extract['notes'])?$extract['notes']:"")."</textarea></td></tr>
								</table>
							</td>
						</tr>
						<tr>
							<td width='440' valign='top' colspan='2' align='right'>
								$checklist_code
							</td>
						</tr>
						<tr><td>&nbsp;</td></tr>";
					$submit_button = "btn_update.png";
					$submit_display = " style='display:none'";
					if ($group_event_id == "NEW") {
						$submit_button = "btn_submit.png";
						$submit_display = "";
						echo "<input type='hidden' name='loc_id' value='".$loc_id."'>
						<input type='hidden' name='track_id' value='".sessvar("trackid")."'>
						";
					}
					/*if (!strstr($event_time, ":")) {
						$event_time = substr($event_time, 0, 2).":".substr($event_time, 2, 2);
					}*/
					echo "<tr>
							<td align='center' valign='middle' colspan='2'>
								<table cellspacing='0' cellpadding='0'>
									<tr id='update_btn'".$submit_display.">
										<td align='center' valign='middle' colspan='2'>
											<table cellspacing='0' cellpadding='0' width='430px'>
												<tr><td align='center'><img src='images/".$submit_button."' width='120px' onclick='checkDurationTime();'></td></tr>
											</table>
										</td>
									</tr>
								</table>
							</td>
						</tr>
						<input type='hidden' name='event_date' value='".$event_date."'>
						<input type='hidden' name='event_time' value='".$event_time."'>
						<input type='hidden' name='server_id' value='".$server_id."'>
						<input type='hidden' name='save' value='1'>
						<input type='hidden' name='group_event_id' value='".$group_event_id."'>
						<input type='hidden' name='selected_row' value='".reqvar("selected_row")."'>
						<tr><td>&nbsp;</td></tr>";
					if ($group_event_id != "NEW") { 
						echo "<tr><td align='center' colspan='2'>";
						echo "<a href='_DO:cmd=confirm&title=Cancel Group Event&message=cancel this group event&c_name=customers_wrapper&js_cmd=cancel_or_delete(\"1\",\"$group_event_id\",\"$event_date\",\"$event_time\",\"$time_span\",\"$event_room\",\"".reqvar("selected_row")."\")'>";
						echo "<img src='images/btn_cancel.png' height='30px' border='0'>";
						echo "</a>";
						echo " &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; ";
						echo "<a href='_DO:cmd=confirm&title=Delete Group Event&message=delete this group event&c_name=customers_wrapper&js_cmd=cancel_or_delete(\"2\",\"$group_event_id\",\"$event_date\",\"$event_time\",\"$time_span\",\"$event_room\",\"".reqvar("selected_row")."\")'>";
						echo "<img src='images/btn_delete.png' height='30px' border='0'>";
						echo "</a>";
						echo "</td></tr><tr><td>&nbsp;</td></tr>";
					}
					echo "</form>";
					
					/*if (is_file("companies/".$dataname."/signatures/signature_".$racer_id.".jpg")) {
						$display_signature = "<img src='companies/".$dataname."/signatures/signature_".$racer_id.".jpg' width='380'>";
					} else {
						$display_signature = "<span class='subtitles'>No signature image on record.</span>";
					}
					echo "<tr>
						<td align='center' colspan='2'>
							<table cellspacing='0' cellpadding='0'>
								<tr>
									<td width='50'>&nbsp;</td>
									<td width='400'>
										<table width='400' cellspacing='0' cellpadding='0'>
											<tr><td class='bg_signature_top'></td></tr>
											<tr><td class='bg_signature_mid' align='center'>".$display_signature."</td></tr>
											<tr><td class='bg_signature_bottom'></td></tr>
										</table>
									</td>									
								</tr>
							</table>
						</td>
					</tr>";*/
					
				} else {
					echo "<tr><td align='center'><font style='font-size:24px; color:#999999; font-family:Verdana,Arial'><br>No record selected...</font></td></tr>";
				}
			?>
			<tr><td>&nbsp;</td></tr>
		</table>
		<?php
			if (isset($_POST['save'])) {
				$date_parts = explode("-", $event_date);
				if ($new_group_event) {
					echo "<script language='javascript'>window.location = '_DO:cmd=send_js&c_name=schedule&js=group_event_created(\'$event_date\',\'$event_time\',\'$time_span\',\'$group_event_id\',\'$event_room\')';</script>";
					/*echo "<script language='javascript'>location = '_DO:cmd=alert&title=Group Event Created&c_name=customers_wrapper&f_name=lavukart/group_events.php?year=".$date_parts[0]."&month=".$date_parts[1]."&day=".$date_parts[2]."&selected_row=".reqvar("selected_row")."'</script>";*/
				} else {
					/*echo "<script language='javascript'>location = '_DO:cmd=alert&title=Update Successful&c_name=customers_wrapper&f_name=lavukart/group_events.php?year=".$date_parts[0]."&month=".$date_parts[1]."&day=".$date_parts[2]."&selected_row=".reqvar("selected_row")."';</script>";*/
				}
			}
			if (reqvar("CorD")) {
				$date_parts = explode("-", $event_date);
				$CorD = reqvar("CorD");
				if ($CorD == 1) {
					/*echo "<script language='javascript'>location = '_DO:cmd=alert&title=Group Event Cancelled&c_name=group_events&f_name=lavukart/group_events.php?year=".$date_parts[0]."&month=".$date_parts[1]."&day=".$date_parts[2]."&selected_row=".reqvar("selected_row")."'</script>";*/
				} else if ($CorD == 2) {
					/*echo "<script language='javascript'>location = '_DO:cmd=alert&title=Group Event Deleted&c_name=group_events&f_name=lavukart/group_events.php?year=".$date_parts[0]."&month=".$date_parts[1]."&day=".$date_parts[2]."&selected_row=".reqvar("selected_row")."';</script>";*/
				}
			}
			echo "<script language='javascript'>default_timespan = '" . ((int)$time_span) . "';</script>";
		?>
</body>
</html>
