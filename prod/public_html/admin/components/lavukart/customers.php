<html>
	<head>
		<title>Customers</title>

<style type="text/css">
<!--
body {
	background-color: transparent;
	margin-left: 0px;
	margin-top: 0px;
	margin-right: 0px;
	margin-bottom: 0px;
	-webkit-tap-highlight-color:rgba(0,0,0,0);
}

.creditnumber {
	font-family: Verdana, Geneva, sans-serif;
	font-size: 13px;
	font-weight: bold;
	color: #3d4f6d;
}
.groupnumber {
	font-family: Verdana, Geneva, sans-serif;
	font-size: 16px;
	font-weight: bold;
	color: #30613a;
}
.gridposition {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 14px;
	font-weight: bold;
	color: #5A75A0;
}
.icontext {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 11px;
	font-weight: bold;
	color: #5A75A0;
}
.nametext {
	font-family: Verdana, Geneva, sans-serif;
	font-size: 13px;
	font-weight: bold;
}
.subtitles {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 13px;
	font-weight: bold;
	color: #5A75A0;
}
.subtitlesB {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 14px;
	font-weight: bold;
	color: #5A75A0;
}
-->
</style>
	<script language="javascript">
		function remove_selected_event() {
			window.location = "_DO:cmd=load_url&c_name=customers&f_name=lavukart/customers.php?remove_entire_event=1";
		}
	</script>
	</head>

	<?php
		
      require_once(dirname(__FILE__) . "/../comconnect.php");
			
			if(reqvar("loc_id")) $loc_id = reqvar("loc_id");
			else if(sessvar_isset("loc_id")) $loc_id = sessvar("loc_id");
			set_sessvar("loc_id", $loc_id);

			$max_group = 1;
			$get_karts = lavu_query("SELECT `position` FROM `lk_karts` WHERE `locationid` = '".sessvar("loc_id")."'");
			if (mysqli_num_rows($get_karts) > 0) {
				while ($extract_k = mysqli_fetch_array($get_karts)) {
					if ((floor((int)$extract_k['position'] / 2) > $max_group) && (floor((int)$extract_k['position'] / 2) <= 3)) {
						$max_group = floor((int)$extract_k['position'] / 2);
					}
				}
			}
			
			
  ?>

	<body>

    	<?php
			$mode = reqvar("mode");
			
			
			
			if(reqvar("eventid")) $eventid = reqvar("eventid");
			else if(sessvar_isset("eventid")) $eventid = sessvar("eventid");
			set_sessvar("eventid",$eventid);
				//$event_msend = $exist_read['ms_end'];
				//	mail("ck@poslavu.com","val: $eventid","");		
			if(reqvar("remove_entire_event") && reqvar("remove_entire_event")==1)
			{
				$event_query = lavu_query("select * from lk_events where `id`='[1]'",$eventid);
				if(mysqli_num_rows($event_query))
				{
					$event_read = mysqli_fetch_assoc($event_query);
					$event_date = $event_read['date'];
					$event_trackid = $event_read['trackid'];
					$event_time = $event_read['time'] * 1;
					
					lavu_query("delete from `lk_events` where `id`='[1]'",$eventid);
					
					echo "<script language='javascript'>";
					echo " window.location = '_DO:cmd=send_js&c_name=schedule&js=remove_event(\'$event_date\',\'$event_trackid\',\'$event_time\')'; ";
					echo "</script>";
				}
				
				$eventid = 0;
				set_sessvar("eventid",0);
			}
			
			if(reqvar("schedule_move_from") && reqvar("schedule_move_to"))
			{
				$schedule_move_from = explode("-",reqvar("schedule_move_from"));
				$schedule_move_to = explode("-",reqvar("schedule_move_to"));
				$move_from_track = $schedule_move_from[0];
				$move_from_time = $schedule_move_from[1] * 1;
				$move_to_track = $schedule_move_to[0];
				$move_to_time = $schedule_move_to[1] * 1;
				$move_date = reqvar("use_date");
			
			/*	echo "<script type=\"text/javascript\">alert(\"move_to_track: $move_to_track\" );</script>";*/
				
				$event_query = lavu_query("select * from `lk_events` where `date`='[1]' and `time` * 1 ='[2]' and `trackid`='[3]'",$move_date,$move_from_time,$move_from_track);
				
				if(mysqli_num_rows($event_query))
				{
					$event_read = mysqli_fetch_assoc($event_query);
					$eventid = $event_read['id'];
						
					$exist_query = lavu_query("select * from `lk_events` where `date`='[1]' and `time` * 1='[2]' and `trackid`='[3]'",$move_date,$move_to_time,$move_to_track);
					if(mysqli_num_rows($exist_query))
					{
						$exist_read = mysqli_fetch_assoc($exist_query);
						$eventid = $exist_read['id'];
						
						$set_date = $exist_read['date'];
						$set_time = $exist_read['time'] * 1;
						$set_trackid = $exist_read['trackid'];
						$set_type = $exist_read['type'];
						$set_groupid = $exist_read['groupid'];
						$event_msend = $exist_read['ms_end'];
						
					
						// find description of event that is selected to copy to
						$set_desc = "New";
						$etype_query = lavu_query("select * from `lk_event_types` where `id`='[1]'",$set_type);
						if(mysqli_num_rows($etype_query))
						{
							$etype_read = mysqli_fetch_assoc($etype_query);
							$set_desc = ucfirst(substr($etype_read['title'],0,3));
						}
						
						//echo "<input type='button' onclick = \" ";
						
						echo "<script language='javascript'>";
						echo "window.location = '_DO:cmd=make_hidden&c_name=customers_wrapper_DO:cmd=send_js&c_name=schedule&js=new_race_created(\'$set_date\',\'$set_time\',\'$eventid\',\'$set_trackid\',\'$set_type\',\'$set_desc\',\'1\',\'$set_groupid\',\'\',\'".$move_from_track."-".$move_from_time."\')'; ";
						echo "</script>";
						
						//echo " \" value='Click'>";
						exit();
					}
					else
					{
						// move the event
						$set_date = $move_date;
						$set_time = $move_to_time;
						$set_trackid = $move_to_track;
						$set_type = $event_read['type'];
						$set_groupid = $event_read['groupid']; 
						
						$dparts = explode("-",$set_date);
						$t = $set_time;
						$set_year = $dparts[0];
						$set_month = $dparts[1];
						$set_day = $dparts[2];
						$set_min = $t % 100;
						$set_hour = (($t - $set_min) / 100);
						$set_time = $t;
						$set_ts = mktime($set_hour,$set_min,0,$set_month,$set_day,$set_year);
						
						lavu_query("update `lk_events` set `trackid`='[1]', `time`='[2]', `ts`='[3]' where `id`='[4]'",$move_to_track,$move_to_time,$set_ts,$eventid);
						
						// find description of event that was just moved
						$set_desc = "New";
						$etype_query = lavu_query("select * from `lk_event_types` where `id`='[1]'",$set_type);
						if(mysqli_num_rows($etype_query))
						{
							$etype_read = mysqli_fetch_assoc($etype_query);
							$set_desc = ucfirst(substr($etype_read['title'],0,3));
						}
						
						echo "<script language='javascript'>";
						echo "window.location = '_DO:cmd=make_hidden&c_name=customers_wrapper_DO:cmd=send_js&c_name=schedule&js=new_race_created(\'$set_date\',\'$set_time\',\'$eventid\',\'$set_trackid\',\'$set_type\',\'$set_desc\',\'1\',\'$set_groupid\',\'".$move_from_track."-".$move_from_time."\')'; ";
						echo "</script>";
						exit();
					}
				}
				else
				{
					//echo "Move from " . $schedule_move_from . " to " . $schedule_move_to . " on " . $move_date;
					exit();
				}
			}
			
			$get_use_credits = lavu_query("SELECT `lk_event_types`.`use_credits` AS `use_credits` FROM `lk_events` LEFT JOIN `lk_event_types` ON `lk_event_types`.`id` = `lk_events`.`type` WHERE `lk_events`.`id` = '[1]'", $eventid);
			$info = mysqli_fetch_assoc($get_use_credits);
			$use_credits = $info['use_credits'];
			set_sessvar("use_credits", $use_credits);
			
			if($mode=="assign" || $mode=="assign_multiple" || $mode=="remove")
			{		
				if($mode=="assign_multiple")
				{
					$racer_ids = reqvar("racer_ids");
					$racers = explode(",",$racer_ids);
				}
				else
				{
					$racer_name = reqvar("racer_name");
					$racer_id = reqvar("racer_id");
					$racers = array($racer_id);
				}
				
				for($i=0; $i<count($racers); $i++)
				{
					$racer_id = $racers[$i];
					$sched_query = lavu_query("select * from `lk_event_schedules` where `eventid`='$eventid' and `customerid`='$racer_id'");
					if(mysqli_num_rows($sched_query) < 1)
					{
						if($mode=="assign" || $mode=="assign_multiple")
						{
							lavu_query("insert into `lk_event_schedules` (`eventid`,`customerid`,`group`) values ('$eventid','$racer_id','1')");
							/* Get the type of race */
							$raceTypeRes = lavu_query("SELECT * FROM lk_events WHERE locationid=\"[1]\" AND id=\"[2]\"",$loc_id,$eventid);
							$raceTypeRow = mysqli_fetch_assoc($raceTypeRes);
							$raceType = $raceTypeRow['type'];
							$raceTypeNameRes = lavu_query("SELECT * FROM lk_event_types WHERE locationid=\"[1]\" AND slots!=\"\" AND id=\"[2]\"",$loc_id,$raceType);
							$raceTypeNameRow = mysqli_fetch_assoc($raceTypeNameRes);
							$title = $raceTypeNameRow['title'];
							/* Count how many people are in the race already */
							$resHowMany = lavu_query("SELECT * FROM lk_event_schedules WHERE eventid=\"[1]\"", $eventid);
							$numOfParticipants = mysqli_num_rows($resHowMany);
							/*Get max number of participants */
							
							
							$resHowManyMax = lavu_query("SELECT * FROM lk_event_types WHERE title=\"[1]\" AND locationid=\"[2]\"", $title, $loc_id);
							$resHowManyRow = mysqli_fetch_assoc($resHowManyMax);
							$howManyMax = $resHowManyRow['slots'];
							$emptySlotsLeft = $howManyMax*1 - $numOfParticipants*1;
											/*if ($i == count($racers)-1){
			 //echo "<script type=\"text/javascript\">window.location = '_DO:cmd=send_js&c_name=schedule&js=updateEmptySlots(\'$eventid\',\'$emptySlotsLeft\')';</script> ";
											}*/
											//if
							/*echo "<script type=\"text/javascript\">alert(\"numa: $emptySlotsLeft\");</script>";*/
							if($use_credits=="No")
							{
							}
							else
							{
								$num_credits = mysqli_fetch_assoc(lavu_query("SELECT * FROM `lk_customers` WHERE `racer_name` ='[1]'", $racer_name));
								if($num_credits['credits'] >= 0)
									lavu_query("update `lk_customers` set `credits` = `credits` - 1 where `id`='$racer_id'");
								else{
									lavu_query("insert into `lk_action_log`(`action`, `customer_id`, `order_id`, `loc_id`,`credits`,`time`) VALUES ('No Credits to Remove', '[1]','[2]','[3]','[4]',NOW())", $racer_id,$eventid, $loc_id,$num_credits['credits'] );
									error_log("ERROR: Trying to remove credits from a racer with no credits. racer_id: ". $racer_id. " event_id: ".$eventid);
								}
							}
						}
					}
					else
					{
						if($mode=="remove")
						{
							
							
									lavu_query("delete from `lk_event_schedules` where `eventid`='$eventid' and `customerid`='$racer_id'");
							
						
							/* Get the type of race */
																	$raceTypeRes = lavu_query("SELECT * FROM lk_events WHERE locationid=\"[1]\" AND id=\"[2]\"",$loc_id,$eventid);
																	$raceTypeRow = mysqli_fetch_assoc($raceTypeRes);
																	$raceType = $raceTypeRow['type'];
										$raceTypeNameRes = lavu_query("SELECT * FROM lk_event_types WHERE locationid=\"[1]\" AND slots!=\"\" AND id=\"[2]\"",$loc_id,$raceType);
																	$raceTypeNameRow = mysqli_fetch_assoc($raceTypeNameRes);
																	$title = $raceTypeNameRow['title'];
																	/* Count how many people are in the race already */
																	$resHowMany = lavu_query("SELECT * FROM lk_event_schedules WHERE eventid=\"[1]\"", $eventid);
																	$numOfParticipants = mysqli_num_rows($resHowMany);
																	/*Get max number of participants */
															//		mail("ck@poslavu.com","val3","");
																	
																	$resHowManyMax = lavu_query("SELECT * FROM lk_event_types WHERE title=\"[1]\" AND locationid=\"[2]\"", $title, $loc_id);
																	$resHowManyRow = mysqli_fetch_assoc($resHowManyMax);
																	$howManyMax = $resHowManyRow['slots'];
																	$emptySlotsLeft = $howManyMax*1 - $numOfParticipants*1;
							if ($i == count($racers)-1){
			 							echo "<script type=\"text/javascript\">window.location = '_DO:cmd=send_js&c_name=schedule&js=updateEmptySlots(\'$eventid\',\'$emptySlotsLeft\')';</script> ";
							}//if
							
							if($use_credits=="No")
							{
							}
							else
							{
							
								if ($event_msend == ""){
										$is_race_complete_res = lavu_query("SELECT * FROM lk_events WHERE id='[1]'",$eventid);
										$is_race_complete_row = mysqli_fetch_assoc($is_race_complete_res);
										$ms_end = $is_race_complete_row['ms_end'];
										if ($ms_end == ""){
											lavu_query("update `lk_customers` set `credits` = `credits` + 1 where `id`='$racer_id'");
										}//
								}//if
							}
						}
					}
				}
			} else if ($mode == "set_group") {
		 		lavu_query("UPDATE `lk_event_schedules` SET `group` = '".reqvar("new_group")."' WHERE `customerid` = '".reqvar("customer_id")."' AND `eventid` = '".$eventid."'");
			}
		?>
		<table width="395" border="0" cellpadding="0" cellspacing="0">
		<?php
            if($eventid)
            {
				require_once(dirname(__FILE__) . "/lk_functions.php");
				$minrows = 12;
				$maxrows = get_slots_for_event($eventid);//10;
				$rowcount = 0;
				$overbooked = 0;
								
				$get_event_info = lavu_query("SELECT `trackid`, `type`, `date`, `groupid`, `group_racer_count`, `heat`, `sequence`, `sequenceid` FROM `lk_events` WHERE `id` = '[1]'", $eventid);
				if (mysqli_num_rows($get_event_info) > 0) {
					$this_event_info = mysqli_fetch_assoc($get_event_info);
					$get_type_info = lavu_query("SELECT `title` FROM `lk_event_types` WHERE `id` = '[1]'", $this_event_info['type']);
					if (mysqli_num_rows($get_type_info) > 0) {
						$this_type_info = mysqli_fetch_assoc($get_type_info);
						$show_race_info = $this_type_info['title'];
						$click_cmd = "window.location = \"_DO:cmd=number_pad&c_name=customers&title=Round%20".$this_event_info['heat']."%20Racer%20Count&type=multi_digit&max_digits=3&send_cmd=cmd=make_hidden%26c_name=schedule%5F%44%4F%3Acmd=load_url%26c_name=schedule%26f_name=lavukart/schedule.php%3Fmode=edit_heat_racer_count%26ehrc_date=".$this_event_info['date']."%26ehrc_locid=$loc_id%26ehrc_trackid=".$this_event_info['trackid']."%26ehrc_groupid=".$this_event_info['groupid']."%26ehrc_heat=".$this_event_info['heat']."%26ehrc_sequenceid=".$this_event_info['sequenceid']."%26old_racer_count=".$this_event_info['group_racer_count']."%26set_racer_count=ENTERED_NUMBER%26set_scrolltop=".reqvar("scrolltop")."\"";
						if ($this_event_info['groupid'] != "") $show_race_info = "<span onclick='$click_cmd'><font size='+1'>".$this_event_info['sequence']." ".$this_event_info['groupid']." - Round ".$this_event_info['heat']."</font></span><br>".$this_type_info['title']." - <font size='-1' color='#444444'>".$this_event_info['group_racer_count']." Racers</font>";
						echo "<tr><td class='subtitlesB' align='center' style='padding: 2px 2px 5px 2px;'>$show_race_info</td></tr>";
					}
				}

				if($maxrows < $minrows)
					$minrows = $maxrows;
				
				$sched_query = lavu_query("select * from `lk_event_schedules` left join `lk_customers` on `lk_event_schedules`.`customerid`=`lk_customers`.`id` where `eventid`='$eventid'");
				while($sched_read = mysqli_fetch_assoc($sched_query))
				{
					$rowcount++;
					$fullname = trim($sched_read['f_name'] . " " . $sched_read['l_name']);
					$credits = $sched_read['credits'];
					$customerid = $sched_read['id'];
					$next_group = (((int)$sched_read['group'] % $max_group) + 1);
					if($credits=="") $credits = 0;
					
					$membership_expiration = $sched_read['membership_expiration'];
					$date_created = $sched_read['date_created'];
					if($date_created >= date("Y-m-d 00:00:00"))
						$cust_new = true;
					else
						$cust_new = false;
						$drawM = "null";
					if($membership_expiration!="" && $membership_expiration >= date("Y-m-d") && $membership_expration <= "9999-99-99")
						{	
							$mem = true; 
							$bgf = "images/btn_credit2.png"; 
							$drawM="blue";
						}
					else 	
						{	
								$mem = false;
								$bgf = "images/btn_credit.png"; 
								$drawM="no";
								
								if($membership_expiration!="" && $membership_expiration < date("Y-m-d")){
										$drawM = "red";
								}//if
								
						}
						
					$birth_date = $sched_read['birth_date'];
					$birth_date1 = $birth_date;
					$bdates = explode("-",$birth_date);
					if(count($bdates) > 2)
					{
						$byear = ($bdates[0] % 100);
						if($byear < 10) $byear = "0" . ($byear * 1);
						$birth_date = $bdates[1] . "/" . $bdates[2] . "/" . $byear;
					}
					else $birth_date = "";
					if($birth_date1!="" && $birth_date1 > date("Y-m-d",mktime(0,0,0,date("m"),date("d"),date("Y") - 18)))
						$cust_is_minor = true;
					else
						$cust_is_minor = false;
						
					if($rowcount > $maxrows && $overbooked==0)
					{
						echo "<tr><td bgcolor='#880000' style='font-size:2px'>&nbsp;</td></tr>";
						$overbooked++;
					}
					
				?>
					<tr>
						<td height="46" align="left" valign="top"><table width="395" height="46" border="0" cellpadding="0" cellspacing="0">
							<tr>
								<td width="30" align="center" valign="middle" class="subtitles">
								<?php
								$is_race_complete_res = lavu_query("SELECT * FROM lk_events WHERE id='[1]'",$eventid);
								$is_race_complete_row = mysqli_fetch_assoc($is_race_complete_res);
								$race_complete = $is_race_complete_row['ms_end'];
								if ($race_complete == ""){
								echo "<a href=\"customers.php?mode=remove&racer_name=$fullname&racer_id=$customerid\"><img src=\"images/btn_x_30.png\" width=\"30\" height=\"41\" border=\"0\"/></a>";
								}//if
								?>
								</td>
								<td width="51" align="center" valign="middle" style="background:URL(images/group_color_bg<?php echo $sched_read['group']; ?>.png);" onClick="this.style.backgroundImage = 'URL(images/group_color_bg<?php echo $next_group; ?>.png)'; window.location = 'customers.php?mode=set_group&new_group=<?php echo $next_group; ?>&customer_id=<?php echo $customerid; ?>';">
									<table width="51" border="0" cellspacing="0" cellpadding="0">
										<tr><td align="center" valign="middle" class="groupnumber" >&nbsp;</td></tr>
									</table>
								</td>
								<td width="262" align="left" valign="middle" background="images/tab_bg_262.png" >
									<table width="262" border="0" cellspacing="0" cellpadding="8">
										<tr><td class="nametext">
											&nbsp;
											<?php 
												echo $fullname; 
												if($birth_date!="") 
												{
													if($cust_is_minor)
														echo " <font style='font-size:10px; color:#bb0000'>$birth_date</font>";
													else
														echo "";//" <font style='font-size:10px; color:#006600'>$birth_date</font>";
												}
											?>
										</td></tr>
									</table>
								</td>
								<td width="52" align="left" valign="top" background="images/btn_credit.png" >
									<table width="50" height="46" border="0" cellspacing="0" cellpadding="6">
										<tr><td align="center" valign="middle" class="creditnumber">
										<?php
											echo $credits;
											if($cust_new) echo "<font style='font-size:8px; color:#008800'>n</font>";
											if($mem){
												 echo "<font style='font-size:8px; color:#000088'>M</font>";
											}//if
											if(!$mem){
												 
												 if ($drawM=="no"){
															echo "<font style='font-size:8px; color:#000088'></font>";//this doesn't do anything
												 }//if
												 if ($drawM=="red"){
												 			echo "<font style='font-size:8px; color:#bb0000'>M</font>";
												 }//if
											}//if
										?>
										</td></tr>
									</table>
								</td>
							</tr>
						</table></td>
					</tr>
					<?php
				}
				for($i=$rowcount; $i<$minrows; $i++)
				{
				?>
						<tr>
							<td height="46">
								<table width="395" height="46" border="0" cellpadding="0" cellspacing="0">
									<tr>
										<td width="30" align="center" valign="middle"><img src="images/btn_x_30.png" width="30" height="41" /></span></td>
										<td width="51" align="center" valign="middle" background="images/tab_bg_51.png">&nbsp;</td>
										<td width="262" background="images/tab_bg_262.png" >
											<table width="262" border="0" cellspacing="0" cellpadding="8">
												<tr><td class="nametext"></td></tr>
											</table>
										</td>
										<td width="52" align="left" valign="top" background="images/btn_credit.png">
											<table width="50" height="46" border="0" cellspacing="0" cellpadding="6">
												<tr><td align="center" valign="middle" class="creditnumber">&nbsp;</td></tr>
											</table>
										</td>
									</tr>
								</table>
							</td>
						</tr>
					<?php
				}
			}
	?>
		</table>
	
	</body>
</html>