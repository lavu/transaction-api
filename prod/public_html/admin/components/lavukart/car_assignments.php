<html>
	<head>
		<title>Untitled Document</title>
		<style type="text/css">
<!--
body {
	background-color: transparent;
	margin-left: 0px;
	margin-top: 0px;
	margin-right: 0px;
	margin-bottom: 0px;
	-webkit-tap-highlight-color:rgba(0,0,0,0);
}

.carnumber {	font-family: Verdana, Geneva, sans-serif;
	font-size: 13px;
	font-weight: bold;
	color: #FFF;
}
.creditnumber {	font-family: Verdana, Geneva, sans-serif;
	font-size: 13px;
	font-weight: bold;
	color: #3d4f6d;
}
.nametext {	font-family: Verdana, Geneva, sans-serif;
	font-size: 13px;
	font-weight: bold;
}
.gridposition {	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 14px;
	font-weight: bold;
	color: #5A75A0;
}
.icontext {font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 11px;
	font-weight: bold;
	color: #5A75A0;
}
.subtitles {	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 14px;
	font-weight: bold;
	color: #5A75A0;
}
.title1 {	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 18px;
	font-weight: bold;
	color: #5A75A0;
}
-->
		</style>
		<script language="javascript">
		
			function assign_kart(customerid) {
			
				window.location = "_DO:cmd=load_url&c_name=car_assignments&f_name=lavukart/car_assignments.php?assignid=" + customerid + "&position=" + row_selected;
			}
		</script>
	</head>

<?php
	require_once(dirname(__FILE__) . "/../comconnect.php");
	ini_set("display_errors","1");
	
	$eventid = 0;
	if(reqvar("eventid")) $eventid = reqvar("eventid");
	else if(sessvar_isset("eventid")) $eventid = sessvar("eventid");
	set_sessvar("eventid",$eventid);

	$lane_number = 1;
	if(reqvar("lane_number")) $lane_number = reqvar("lane_number");
	else if(sessvar_isset("lane_number")) $lane_number = sessvar("lane_number");
	set_sessvar("lane_number",$lane_number);
	
	$lane_calc = ($lane_number % 8);	
	$kart_img = "car.png";	
	switch ($lane_calc) {
		 case 1 : $kart_img = "car1.png"; break;
		 case 2 : $kart_img = "car2.png"; break;
		 case 3 : $kart_img = "car3.png"; break;
		 case 4 : $kart_img = "car4.png"; break;
		 case 5 : $kart_img = "car6.png"; break;
		 case 6 : $kart_img = "car7.png"; break;
		 case 7 : $kart_img = "car8.png"; break;
		 case 8 : $kart_img = "car9.png"; break;
	}

	$row_selected = 1;

	if (isset($_POST['assignid'])) {		
		$get_kart_id = lavu_query("SELECT * FROM `lk_karts` WHERE `lane` = '".$lane_number."' AND `locationid` = '".$_POST['loc_id']."' AND `position` = '".$_POST['position']."'");
		if (@mysqli_num_rows($get_kart_id) > 0) {
			$extract_kid = mysqli_fetch_array($get_kart_id);
			$reset_kart = lavu_query("UPDATE `lk_event_schedules` SET `kartid` = '' WHERE `eventid` = '".$eventid."' AND `kartid` = '".$extract_kid['id']."'");
			$assign_racer = lavu_query("UPDATE `lk_event_schedules` SET `kartid` = '".$extract_kid['id']."' WHERE `eventid` = '".$eventid."' AND `customerid` = '".$_POST['assignid']."'");
		}
		$row_selected = ($_POST['position'] + 1);
	}

	$get_last_kart = lavu_query("SELECT `position` FROM `lk_karts` WHERE `locationid` = '".$_POST['loc_id']."' AND `lane` = '".$lane_number."' ORDER BY `position` DESC LIMIT 1");
	if (@mysqli_num_rows($get_last_kart) > 0) {
		$extract_lk = mysqli_fetch_array($get_last_kart);
		$last_row = $extract_lk['position'];
	} else {
		$last_row = 0;
	}
	if (isset($_POST['rs'])) { $row_selected = $_POST['rs']; }
	if ($row_selected > $last_row) { $row_selected = 1; }	
?>

	<body>
	
		<table width="395" border="0" cellpadding="0" cellspacing="0">
			<?php
				$rowcount = 0;
				$get_karts = lavu_query("SELECT * FROM `lk_karts` WHERE `locationid` = '".$_POST['loc_id']."' AND `lane` = '".$lane_number."' ORDER BY `position` * 1 ASC");
				while ($extract_k = mysqli_fetch_array($get_karts)) {
					$rowcount++;
					if($extract_k['position']=="")
					{
						lavu_query("update `lk_karts` set `position`='$rowcount' where `id`='".$extract_k['id']."'");
					}
					$get_assignment = lavu_query("SELECT `lk_customers`.`f_name` as f_name, `lk_customers`.`l_name` as l_name, `lk_event_schedules`.`customerid` as customerid FROM `lk_event_schedules` LEFT JOIN `lk_customers` ON `lk_event_schedules`.`customerid` = `lk_customers`.`id` WHERE `lk_event_schedules`.`kartid` = '".$extract_k['id']."' AND `lk_event_schedules`.`eventid` = '".$eventid."'");
					$f_name = "";
					$l_name = "";
					$use_bg = "images/tab_bg2.png";
					if ($rowcount == $row_selected) {
						$use_bg = "images/tab_bg2_lit.png";
					}
					if (@mysqli_num_rows($get_assignment) > 0) {
						$extract_a = mysqli_fetch_array($get_assignment);
						$f_name = $extract_a['f_name'];
						$l_name = $extract_a['l_name'];
					}
					echo "<tr>
						<td height='46' align='left' valign='top'>
							<table width='395' height='46' border='0' cellpadding='0' cellspacing='0'>
								<tr>
									<td width='30' align='center' valign='middle' class='subtitles'><span class='gridposition'>".$extract_k['position']."</span></td>
									<td id='kart_row_".$rowcount."' width='313' align='left' valign='middle' style='background: URL(".$use_bg.");' onclick='if(row_selected) document.getElementById(\"kart_row_\" + row_selected).style.background = \"URL(images/tab_bg2.png)\"; row_selected = \"".$rowcount."\"; this.style.background = \"URL(images/tab_bg2_lit.png)\";'>
										<table width='313' border='0' cellspacing='0' cellpadding='8'>
											<tr><td class='nametext'>".$f_name." ".$l_name."</td></tr>
										</table>
									</td>
									<td width='52' align='left' valign='top' background='images/".$kart_img."' >
										<table width='44' height='36' border='0' cellspacing='0' cellpadding='6'>
											<tr><td align='center' valign='bottom' class='carnumber'>".$extract_k['number']."</td></tr>
										</table>
									</td>
								</tr>
							</table>
						</td>
					</tr>";
				}
			?>
		</table>
		<script language='javascript'>
			row_selected = <?php echo $row_selected?>;
			
			<?php
				if (isset($_POST['assignid'])) {
					echo "window.location = '_DO:cmd=load_url&c_name=racers_for_this_race&f_name=lavukart/racers_for_this_race.php?rs=".$row_selected."';";
				} else {
					echo "window.location = '_DO:cmd=send_js&c_name=racers_for_this_race&js=assign_rs(".$row_selected.")';";
				}
			?>
		</script>
	</body>
</html>
