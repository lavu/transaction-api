<?php
			require_once(dirname(__FILE__) . "/../comconnect.php");
			
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Untitled Document</title>
</head>

<body>


<?php

	$locationid = sessvar("loc_id");
	$serviceID = $_POST['serviceID']; //the unique id of the service
	
	
	
	/* the list menu says kart number, but the values are the kart id*/
	$kartID = $_POST['KartID'];
	
	$kartNumberResults = lavu_query("SELECT number FROM lk_karts WHERE id=\"[1]\"", $kartID);
	$kartNumberRows = mysqli_fetch_assoc($kartNumberResults);
	$kartNumber = $kartNumberRows['number'];
	
	
	/*the lk_service table stores value in service_type as serviceID-NameOfService, so we must format this now */
	$serviceTypeID = $_POST['serviceTypeID'];
	$getServiceTypeNameResults = lavu_query("SELECT name FROM lk_service_types WHERE locationid=\"[1]\" AND id=\"[2]\"", $locationid, $serviceTypeID);
	$getServiceTypeNameRows = mysqli_fetch_assoc($getServiceTypeNameResults);
	$serviceName = $getServiceTypeNameRows['name'];
	
	$serviceTypeSubmit2DB = $serviceTypeID . "-" . $serviceName;//properly formatted
	
	
	//$description; //updated near bottom of this file
	$notes = $_POST['notes'];
	//$parts; //updated near bottom of this file
	$dateDueMonth = $_POST['dateDueMonth'];
	/* If single digit we need a "0" in front to make sure there are two digits*/
	if (strlen($dateDueMonth) == 1){
				$dateDueMonth = "0" . $dateDueMonth;
	}//if
	
	
	$dateDueDay = $_POST['dateDueDay'];
	
	/* If single digit we need a "0" in front to make sure there are two digits*/
	if (strlen($dateDueDay) == 1){
				$dateDueDay = "0" . $dateDueDay;
	}//if
	
	$dateDueYear = $_POST['dateDueYear'];
	$dateDue = $dateDueYear . "-" . $dateDueMonth . "-" . $dateDueDay;
	//now get the time (H:i:s) from database
	$dateDueResults = lavu_query("SELECT date_due FROM lk_service WHERE locationid=\"[1]\" AND id=\"[2]\"", $locationid, $serviceID);
	$dateDueRows = mysqli_fetch_assoc($dateDueResults);
	$dateDueString = $dateDueRows['date_due'];
	$dateDueStringArray = explode(" ", $dateDueString);
	$dateDueTime = $dateDueStringArray[1]; //$dateDueStringArray[0] is the date we are about to overwrite
	$dateDueSubmitToDB = $dateDue . " " . $dateDueTime; //this goes to the lk_service table
	
	//$dateAssigned; //updated near bottom of this file
	$priority = $_POST['Priority'];
	//$manHours; //updated near bottom of this file
	
	$submittedBy = $_POST['submittedBy'];
	$servicedBy = $_POST['servicedBy'];
	
	$taskIsComplete = $_POST['taskComplete'];//if the  complete field in lk_service table is true this service task doesn't show up anymore in the service_list.php
	
	
	/* Lavu query only can handle five inputs at a time !? */
	$results1 = lavu_query("UPDATE lk_service SET kart_id='[1]', service_type='[2]', notes='[3]', complete='[4]'  WHERE id=\"[5]\" AND locationid=\"[6]\"", $kartID, $serviceTypeSubmit2DB, $notes, $taskIsComplete, $serviceID, $locationid);
	
	
	lavu_query("UPDATE lk_service SET kart_number='[1]' WHERE id=\"[2]\" AND locationid=\"[3]\"", $kartNumber, $serviceID, $locationid);
	
	
	lavu_query("SELECT * FROM lk_karts"); //this query only serves to clear the lavu_query function for the next update query
	
	$results2 = lavu_query("UPDATE lk_service SET date_due='[1]', priority='[2]', submitted_by='[3]', serviced_by='[4]'  WHERE id=\"[5]\" AND locationid=\"[6]\"", $dateDueSubmitToDB, $priority, $submittedBy, $servicedBy, $serviceID, $locationid);




/* If the user switched the service type we need to update the part_cost, parts, description, man_hours fields in the lk_service table*/
$serviceTypeWasChanged = $_POST['serviceTypeWasChanged']; //this hidden text field value is set to true when the service type is changed
if ($serviceTypeWasChanged == "true"){
	
			
			$getServiceTypeNameResults = lavu_query("SELECT * FROM lk_service_types WHERE locationid=\"[1]\" AND id=\"[2]\"", $locationid, $serviceTypeID);
			$getServiceTypeNameRows = mysqli_fetch_assoc($getServiceTypeNameResults);
	
			$serviceTypePartsStr = $getServiceTypeNameRows['parts'];
			$serviceTypeArray = explode(",", $serviceTypePartsStr);
			$storeFormattedPartsList;
			$partCost = 0;
	
			for ($i=0; $i<count($serviceTypeArray);$i++){
				
						$curArray = $serviceTypeArray[$i];
						$curPieces = explode("@", $curArray);
						$curID = $curPieces[0];
						$curQuantity = $curPieces[1];
				
				
						//get part name
						$resultsGetPartName = lavu_query("SELECT * FROM lk_parts WHERE id=\"[1]\" AND locationid=\"[2]\"", $curID, $locationid);
						$getPartNameRows = mysqli_fetch_assoc($resultsGetPartName);
						$curPartName = $getPartNameRows['name'];
						$curPartCost = $getPartNameRows['cost'];
				
						$partCost = $partCost + ($curPartCost * $curQuantity);
				
						$strIntoArray = $curPartName . "@" . $curID . "@" . $curQuantity; /* we just changed from partIDxQuantity,partIDxQuantity... to partNamexpartIDxQuantity,partNamexpartIDxQuantity...etc */
						$storeFormattedPartsList[$i] = $strIntoArray;
				
			}//for
	
			$serviceTypeDescription = $getServiceTypeNameRows['description'];
			$serviceTypeManHours = $getServiceTypeNameRows['man_hours'];
	
			$serviceTypePartsStr = implode(",", $storeFormattedPartsList); //build comma delimited parts list for storage in lk_service under parts field
			
			/* lavu_query() only takes 5 input parameters per query*/
			$results3 = lavu_query("UPDATE lk_service SET parts='[1]', part_cost='[2]', description='[3]' WHERE id=\"[4]\" AND locationid=\"[5]\"", $serviceTypePartsStr, $partCost, $serviceTypeDescription, $serviceID, $locationid);
			$results4 = lavu_query("UPDATE lk_service SET man_hours='[1]' WHERE id=\"[2]\" AND locationid=\"[3]\"", $serviceTypeManHours, $serviceID, $locationid);
}//if

	
	/* If nothing was updated $numberOfUpdatedRows will be 0 */
	if ($numberOfUpdatedRows == 0){
		/*	echo "<script type=\"text/javascript\">window.location = '_DO:cmd=alert&title=Update Failed&message=No changes were made.';</script>";*/
	}//if

?>

<script type="text/javascript">
		
		window.location = "_DO:cmd=load_url&c_name=service_list&f_name=lavukart/service_list.php"; //refresh service_list component (top window) and close overlay
		
</script>


</body>
</html>
