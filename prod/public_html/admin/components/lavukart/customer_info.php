<html>
	<head>
		<title>Customer Info</title>
		<style type="text/css">
<!--
body {
	background-color: transparent;
	margin-left: 0px;
	margin-top: 0px;
	margin-right: 0px;
	margin-bottom: 0px;
	-webkit-tap-highlight-color:rgba(0,0,0,0);
}

.creditnumber {
	font-family: Verdana, Geneva, sans-serif;
	font-size: 13px;
	font-weight: bold;
	color: #3d4f6d;
}
.gridposition {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 14px;
	font-weight: bold;
	color: #5A75A0;
}
.icontext {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 11px;
	font-weight: bold;
	color: #5A75A0;
}
.nametext {
	font-family: Verdana, Geneva, sans-serif;
	font-size: 13px;
	font-weight: bold;
}
.subtitles {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 13px;
	font-weight: bold;
	color: #5A75A0;
}
.bg_signature_top {
	background-image:URL(images/bg_signature_top.png);
	background-repeat:no-repeat;
	width:400px;
	height:9px;
}
.bg_signature_mid {
	background-image:URL(images/bg_signature_mid.png);
	background-repeat:repeat-y;
	width:400px;
}
.bg_signature_bottom {
	background-image:URL(images/bg_signature_bottom.png);
	background-repeat:no-repeat;
	width:400px;
	height:9px;
}
-->
		</style>
	</head>

<?php
	require_once(dirname(__FILE__) . "/../comconnect.php");
	require_once(dirname(__FILE__) . "/../../lib/ml_connect.php");

	function get_timestamp($dt) {
		$datetime_array = explode(" ", $dt);
		$date_array = explode("-", $datetime_array[0]);

		$ts = mktime(0, 0, 0, ($date_array[1]  * 1), ($date_array[2] * 1), ($date_array[0] * 1));
		return (int)$ts;
	}

	$racer_id = 0;
	if(reqvar("racer_id")) $racer_id = reqvar("racer_id");
	else if(sessvar_isset("showing_racer_id")) $racer_id = sessvar("showing_racer_id");
	set_sessvar("showing_racer_id",$racer_id);

	$loc_id = 0;
	if(reqvar("loc_id")) $loc_id = reqvar("loc_id");
	else if(sessvar_isset("loc_id")) $loc_id = sessvar("loc_id");
	set_sessvar("loc_id", $loc_id);

	/*$server_id = 0;
	if(reqvar("server_id")) $server_id = reqvar("server_id");
	else if(sessvar_isset("server_id")) $server_id = sessvar("server_id");
	set_sessvar("server_id",$server_id);

	$dataname = "";
	if(reqvar("dn")) $dataname = reqvar("dn");
	else if(sessvar_isset("dataname")) $dataname = sessvar("dataname");
	set_sessvar("dataname", $dataname);*/

	//------------ start: get states and sources
	$state_array = array();
	$source_array = array();

	$lkb_path = $_SERVER['HTTP_HOST'] . str_replace($_SERVER['DOCUMENT_ROOT'],"",dirname(__FILE__)) . "/lib/lkb.php?dn=".$dataname."&m=get_states_and_sources";
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL, $lkb_path);
	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
	curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	$ch_response = curl_exec($ch);
	//echo "ch_response: " . str_replace("<","&lt;",$ch_response) . "<br>";

	$ch_data = explode("|",$ch_response);
	if($ch_data[0]=="1")
	{
		if(count($ch_data) > 1)
		{
			$state_array = explode(";;",$ch_data[1]);
			$state_array = array_merge(array("","International"),$state_array); // add blank/international to beginning
			if(count($ch_data) > 2)
			{
				$source_array = explode(";;",$ch_data[2]);
				$source_array = array_merge(array(""),$source_array); // add blank to beginning
			}
		}
	}
	//------------ end: get states and sources
	$membership_array = array("Unqualified","Weekly Membership","Annual Membership");

	if($access_level * 1 >= 2)
		$include_credit_input = true;
	else
		$include_credit_input = false;

	$upcoming_events = array();
	$upcoming_events[''] = "Not tied to an event";
	$today = date("Y-m-d");
	if (isset($location_info) && ($location_info['timezone'] != "")) {
		$today = localize_date(date("Y-m-d H:i:s"), $location_info['timezone']);
	}
	$check_events = lavu_query("SELECT `id`, `title`, `event_date`, `event_time` FROM `lk_group_events` WHERE `event_date` = '$today' AND `cancelled` != '1' AND `_deleted` != '1' AND `loc_id` = '[1]' ORDER BY `event_date` ASC, (`event_time` * 1) ASC", $loc_id);
	if (@mysqli_num_rows($check_events) > 0) {
		while ($event_info = mysqli_fetch_assoc($check_events)) {
			$ddd = "";
			if (strlen($event_info['title']) > 15) $ddd = "...";
			$upcoming_events[$event_info['id']] = $event_info['event_date']." ".timize($event_info['event_time'])." - ".substr($event_info['title'], 0, 15).$ddd;
		}
	}

	$info_form = array();
	$info_form[] = array("Group Event","event_id","selectA",$upcoming_events);
	$info_form[] = array("First Name","f_name");
	$info_form[] = array("Last Name","l_name");
	$info_form[] = array("Racer Name","racer_name");
	$info_form[] = array("Membership","membership","select",$membership_array);
	$info_form[] = array("Expiration","membership_expiration","date",20,-20);
	$info_form[] = array("Date of Birth","birth_date","date",0,-100);
	$info_form[] = array("Email","email");
	$info_form[] = array("Postal Code","postal_code");
	$info_form[] = array("State","state","select",$state_array);
	$info_form[] = array("Country","country");
	$info_form[] = array("License","license_exp");
	$info_form[] = array("Source","source","select",$source_array);
	$info_form[] = array("Notes","notes","textarea");
	if($include_credit_input)
		$info_form[] = array("Credits","credits","number");
	$info_form[] = array("minor_or_adult","minor_or_adult","hidden");

	if (isset($_POST['save'])) {
	  $vals = array();
		$save_info = "SET ";
		for ($c = 0; $c < count($info_form); $c++) {
		  $iname = $info_form[$c][1];
		  $vals[$iname] = $_POST[$iname];
			$save_info .= "`$iname` = '[$iname]'";
			if ($c < (count($info_form) - 1)) {
				$save_info .= ", ";
			}
			if ($iname == "email" && preg_match("/^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*$/i", $_POST[$iname])) {
				$get_mailing_list_info = lavu_query("SELECT `ml_type`, `ml_un`, `ml_pw`, `ml_listid` FROM `locations` WHERE `id` = '[1]'", $loc_id);
				if (mysqli_num_rows($get_mailing_list_info)) {
					$mailing_list_info = mysqli_fetch_assoc($get_mailing_list_info);
					update_emailing_list("subscriberadd", "racer", $data_name, $mailing_list_info, $racer_id);
				}
			}
		}

		$update_customer = lavu_query("UPDATE `lk_customers` ".$save_info." WHERE `id` = '".$racer_id."'", $vals);

		if($include_credit_input && $_POST['credits'] != $_POST['previous_value_credits'])
		{
			if(is_numeric($_POST['credits']) && is_numeric($_POST['previous_value_credits']))
			{
				$credit_change = ($_POST['credits'] * 1 - $_POST['previous_value_credits'] * 1);
			}
			else $credit_change = "";
			$lvars = array();
			$lvars['action'] = "Customer Credit Change";
			$lvars['loc_id'] = $loc_id;
			$lvars['order_id'] = "";
			$lvars['time'] = date("Y-m-d H:i:s");
			$lvars['user'] = $server_name;
			$lvars['user_id'] = $server_id;
			$lvars['server_time'] = date("Y-m-d H:i:s");
			$lvars['item_id'] = "";
			$lvars['details'] = "Before: " . $_POST['previous_value_credits'] . "\nAfter: " . $_POST['credits'] . "\nAccount Id: " . $racer_id . "\nName: " . $_POST['f_name'] . " " . $_POST['l_name'];
			$lvars['details_short'] = $credit_change;
			$lvars['check'] = "";
			$lvars['device_udid'] = "";
			lavu_query("insert into `action_log` (`action`,`loc_id`,`order_id`,`time`,`user`,`user_id`,`server_time`,`item_id`,`details`,`details_short`,`check`,`device_udid`) values ('[action]','[loc_id]','[order_id]','[time]','[user]','[user_id]','[server_time]','[item_id]','[details]','[details_short]','[check]','[device_udid]')",$lvars);

			// START LP-2494
			require_once(__DIR__."/../../lib/jcvs/jc_func8.php");
			$currentActionLogId = lavu_insert_id();
			updateSignatureForNorwayActionLog($currentActionLogId);
			// END  LP-2494
		}




	}
?>
	<body>

		<table width="460" border="0" cellspacing="0" cellpadding="0">
			<?php
				$get_customer_info = lavu_query("SELECT * FROM `lk_customers` WHERE `id` = '".$racer_id."'");
				if (mysqli_num_rows($get_customer_info) > 0) {
					$extract = mysqli_fetch_array($get_customer_info);

					$b_ts = (get_timestamp($extract['birth_date']) * 1);
					$date_at_eighteen = mktime(0, 0, 0, date("m", $b_ts), date("d", $b_ts), (date("Y", $b_ts) + 18));
					if ($date_at_eighteen > time()) {
						$minor_or_adult = "minor";
					} else {
						$minor_or_adult = "adult";
					}

					echo "<form name='info_form' action='customer_info.php' method='POST'>";
					foreach ($info_form as $info) {
						$field_name = $info[1];
						$field_value = $extract[$field_name];
						$field_value_escaped = str_replace("\"","&quot;",$field_value);

						$field_type = (isset($info[2]))?$info[2]:"text";
						$field_props = (isset($info[3]))?$info[3]:"";
						$field_props2 = (isset($info[4]))?$info[4]:"";

						$select_style = "font-size:14px";

						if($field_type=="textarea")
						{
							$valign = "top";
							$element_height = 92;
							$element_bg = "images/tab_bg2_tall.png";
							$form_element = "<textarea name='".$field_name."' class='nametext' style='border:none; background: URL(images/transparent_pixel.png); width: 290px' rows='4' onchange='document.getElementById(\"update_btn\").style.display = \"inline\";'>" . $field_value . "</textarea>";
						}
						else if(substr($field_type, 0, 6)=="select")
						{
							$valign = "middle";
							$element_height = 46;
							$element_bg = "images/tab_bg2.png";
							$form_element = "";
							$form_element .= "<select name='".$field_name."' style='$select_style' onchange='document.getElementById(\"update_btn\").style.display = \"inline\";'>";
							$select_value_found = false;
							if(count($field_props) < 1)
								$form_element .= "<option value=''></option>";

							if ($field_type == "selectA") {

								$keys = array_keys($field_props);
								foreach ($keys as $key) {
									$form_element .= "<option value=\"".str_replace("\"","&quot;",$key)."\"";
									if($field_value==$key) 
									{ 
										$form_element .= " selected";
										$select_value_found = true;
									}
									$form_element .= ">".$field_props[$key]."</option>";
								}
								if(!$select_value_found && $field_value!="") {
									$lookup_event = lavu_query("SELECT `id`, `title`, `event_date`, `event_time` FROM `lk_group_events` WHERE `id` = '[1]'", $field_value);
									if (@mysqli_num_rows($lookup_event) > 0) {
										$this_event_info = mysqli_fetch_assoc($lookup_event);
										$ddd = "";
										if (strlen($this_event_info['title']) > 16) $ddd = "...";
										$form_element .= "<option value=\"".$this_event_info['id']."\" selected>".$this_event_info['event_date']." ".timize($this_event_info['event_time'])." - ".substr($this_event_info['title'], 0, 16).$ddd."</option>";
									} else {
										$form_element .= "<option value=\"\" selected>No upcoming events</option>";
									}
								}
								$form_element .= "</select>";

							} else {

								for($n=0; $n<count($field_props); $n++)
								{
									// $field_value_escaped

									$form_element .= "<option value=\"".str_replace("\"","&quot;",$field_props[$n])."\"";
									if($field_value==$field_props[$n]) 
									{ 
										$form_element .= " selected";
										$select_value_found = true;
									}
									$form_element .= ">".$field_props[$n]."</option>";
								}
								if(!$select_value_found && $field_value!="")
									$form_element .= "<option value=\"$field_value_escaped\" selected>$field_value</option>";
								$form_element .= "</select>";
							}
						}
						else if($field_type=="date")
						{
							$select_changecode = " onchange='document.getElementById(\"$field_name\").value = document.getElementById(\"year_$field_name\").value + \"-\" + document.getElementById(\"month_$field_name\").value + \"-\" + document.getElementById(\"day_$field_name\").value; document.getElementById(\"update_btn\").style.display = \"inline\";'";
							$valign = "middle";
							$element_height = 46;
							$element_bg = "images/tab_bg2.png";

							$form_element = "";

							$birthdate_parts = explode("-",$field_value);
							if($birthdate_parts > 2)
							{
								$bd_year = $birthdate_parts[0];
								$bd_month = $birthdate_parts[1];
								$bd_day = $birthdate_parts[2];
							}
							else
							{
								$bd_year = "";
								$bd_month = "";
								$bd_day = "";
							}
							$form_element .= "<table><tr><td>";

							//$element_height = 92; 
							//$form_element .= "<input type='text' name='$field_name' id='$field_name' value=\"$field_value_escaped\">";
							$form_element .= "<input type='hidden' name='$field_name' id='$field_name' value=\"$field_value_escaped\">";

							$form_element .= "<select name='month_$field_name' style='$select_style' id='month_$field_name' $select_changecode>";
							$form_element .= "<option value=''></option>";
							for($month=1; $month <= 12; $month++)
							{
								$month_ts = mktime(0,0,0,$month,1,2000);
								$selected = ($month==$bd_month*1)?" selected":"";
								$form_element .= "<option value='".date("m",$month_ts)."' $selected>".date("M",$month_ts)."</option>";
							}
							$form_element .= "</select>";
							$form_element .= "</td><td>/</td><td>";
							$form_element .= "<select name='day_$field_name' style='$select_style' id='day_$field_name' $select_changecode>";
							$form_element .= "<option value=''></option>";
							for($day=1; $day <= 31; $day++)
							{
								if($day < 10) $day_leading_zero = "0" . ($day * 1);
								else $day_leading_zero = $day;
								$selected = ($day==$bd_day*1)?" selected":"";
								$form_element .= "<option value='$day_leading_zero' $selected>$day</option>";
							}
							$form_element .= "</select>";
							$form_element .= "</td><td>/</td><td>";
							$form_element .= "<select name='year_$field_name' style='$select_style' id='year_$field_name' $select_changecode>";
							$form_element .= "<option value=''></option>";
							for($year=date("Y") + $field_props; $year >= date("Y") + $field_props2; $year--)
							{
								$selected = ($year==$bd_year*1)?" selected":"";
								$form_element .= "<option value='$year' $selected>$year</option>";
							}
							$form_element .= "</select>";
							$form_element .= "</td></tr></table>";
						}
						else if($field_type=="number")
						{
							$valign = "middle";
							$element_height = 46;
							$element_bg = "images/tab_bg2.png";
							$form_element = "<input type='text' name='".$field_name."' id='".$field_name."' class='nametext' style='border:none; background: URL(images/transparent_pixel.png);' size='30' value=\"".$field_value_escaped."\" onkeyup=\"document.getElementById('$field_name').value = document.getElementById('$field_name').value.replace(/[^0-9]/g, ''); document.getElementById('update_btn').style.display = 'inline';\">";
						}
						else if ($field_name=="minor_or_adult") {
							$form_element = "<input type='hidden' name='$field_name' id='$field_name' value='$minor_or_adult'>";
						}
						else
						{
							$valign = "middle";
							$element_height = 46;
							$element_bg = "images/tab_bg2.png";
							$form_element = "<input type='text' name='".$field_name."' class='nametext' style='border:none; background: URL(images/transparent_pixel.png);' size='30' value=\"".$field_value_escaped."\" onchange='document.getElementById(\"update_btn\").style.display = \"inline\";'>";
						}

						$form_element .= "<input type='hidden' name='previous_value_".$info[1]."' value=\"".$field_value_escaped."\">";
						if ($field_type != "hidden") {
							echo "<tr>
								<td width='147' height='$element_height' valign='$valign'>
									<table width='147' border='0' cellspacing='0' cellpadding='8'>
										<tr><td align='right' class='subtitles'>".$info[0]."</td></tr>
									</table>
								</td>
								<td width='313' background='$element_bg' style='background-repeat:no-repeat'>
									<table width='313' border='0' cellspacing='0' cellpadding='8'>
										<tr><td class='nametext'>";
										echo $form_element;
										echo "</td></tr>
									</table>
								</td>
							</tr>";
						} else {
							echo $form_element;
						}
					}


					echo "<tr><td>&nbsp;</td></tr>
						<tr><td align='center' valign='middle' colspan='2'><img src='images/btn_history.png' width='139px' onclick='window.location = \"_DO:cmd=create_overlay&f_name=lavukart/customer_history_overlay_wrapper.php;lavukart/customer_history.php&c_name=customer_history_overlay_wrapper;customer_history&dims=250,100,574,549;266,158,542,480&scroll=0;1?racer_id=".$racer_id."\";';> &nbsp;&nbsp;&nbsp;&nbsp; <img id='update_btn' src='images/btn_update.png' width='120px' onclick='document.info_form.submit();' style='display:none'></td></tr>
						<input type='hidden' name='save' value='1'>
						<input type='hidden' name='racer_id' value='".$_POST['racer_id']."'>
						<tr><td>&nbsp;</td></tr>
					</form>";

					if (reqvar("confirm_signature")) {

						$update_customer = lavu_query("UPDATE `lk_customers` SET `confirmed_by_attendant` = '".$server_id."' WHERE `id` = '".$racer_id."'");
						$signature = "Signature";
						if ($minor_or_adult  == "minor") {
							$signature = "Signatures";
						}
						$lk_log_this = lavu_query("INSERT INTO `lk_action_log` (`action`, `customer_id`, `order_id`, `time`, `user_id`, `credits`, `loc_id`) VALUES ('".$signature." Confirmed', '".$racer_id."', '0', now(), '".$server_id."', '0', '".$loc_id."')");
					}

					if (is_file($_SERVER['DOCUMENT_ROOT'] . "/components/lavukart/companies/".$dataname."/signatures/signature_".$racer_id.".jpg")) {
						$display_signature = "<img src='/components/lavukart/companies/".$dataname."/signatures/signature_".$racer_id.".jpg' width='380'>";
					} else {
						$display_signature = "<span class='subtitles'>No signature image on record.</span>";
					}
					echo "<tr>
						<td align='center' colspan='2'>
							<table cellspacing='0' cellpadding='0'>
								<tr>
									<td width='50'>&nbsp;</td>
									<td width='400'>
										<table width='400' cellspacing='0' cellpadding='0'>
											<tr><td class='bg_signature_top'></td></tr>
											<tr><td class='bg_signature_mid' align='center'>$display_signature</td></tr>
											<tr><td class='bg_signature_bottom'></td></tr>
										</table>
									</td>
								</tr>";

					if (($extract['confirmed_by_attendant'] == "") && (!reqvar("confirm_signature"))) {
						echo "<tr><td>&nbsp;</td></tr>
						<tr><td width='50'>&nbsp;</td><td align='center' width='400'><img src='images/btn_confirm_signature.png' width='342' onclick='window.location = \"customer_info.php?confirm_signature=1\";'></td></tr>";
					}
					echo "<tr><td>&nbsp;</td></tr>
					<tr><td align='center' colspan='2'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a onclick='window.location = \"_DO:cmd=update_signature&refresh_comp=customer_info&customer_id=$racer_id&minor_or_adult=$minor_or_adult\";'><img src='images/btn_update_signature.png' width='331px'></a></td></tr>";

					echo "</table>
						</td>
					</tr>";

				} else {
					echo "<tr><td align='center'><font style='font-size:24px; color:#999999; font-family:Verdana,Arial'><br>No record selected...</font></td></tr>";
				}
			?>
			<tr><td>&nbsp;</td></tr>
		</table>
		<?php
			if (isset($_POST['save'])) {
				echo "<script language='javascript'>location = '_DO:cmd=alert&title=Update Successful';</script>";
			}
			if (reqvar("confirm_signature")) {
				if ($minor_or_adult  == "minor") {
					$signature = "Signatures";
				} else {
					$signature = "Signature";
				}
				echo "<script language='javascript'>location = '_DO:cmd=alert&title=$signature Confirmed';</script>";
			}
		?>
</body>
</html>
