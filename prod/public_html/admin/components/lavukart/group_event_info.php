<html>
	<head>
		<title>Group Event Info</title>
		<style type="text/css">
<!--
body {
	background-color: transparent;
	margin-left: 0px;
	margin-top: 0px;
	margin-right: 0px;
	margin-bottom: 0px;
}

.creditnumber {
	font-family: Verdana, Geneva, sans-serif;
	font-size: 13px;
	font-weight: bold;
	color: #3d4f6d;
}
.gridposition {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 14px;
	font-weight: bold;
	color: #5A75A0;
}
.icontext {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 11px;
	font-weight: bold;
	color: #5A75A0;
}
.nametext {
	font-family: Verdana, Geneva, sans-serif;
	font-size: 13px;
	font-weight: bold;
}
.title {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 16px;
	font-weight: bold;
	color: #5A75A0;
}
.subtitles {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 13px;
	font-weight: bold;
	color: #5A75A0;
}
.bg_signature_top {
	background-image:URL(images/bg_signature_top.png);
	background-repeat:no-repeat;
	width:400px;
	height:9px;
}
.bg_signature_mid {
	background-image:URL(images/bg_signature_mid.png);
	background-repeat:repeat-y;
	width:400px;
}
.bg_signature_bottom {
	background-image:URL(images/bg_signature_bottom.png);
	background-repeat:no-repeat;
	width:400px;
	height:9px;
}
-->
		</style>
		<script language="javascript">
		
			function cancel_or_delete(CorD,group_event_id,event_date,selected_row) {
			
				window.location = "group_event_info.php?CorD=" + CorD + "&group_event_id=" + group_event_id + "&event_date=" + event_date + "&selected_row=" + selected_row;
			}
					
		</script>
	</head>

<?php
	require_once("../comconnect.php");
	
	$group_event_id = 0;
	if(reqvar("group_event_id")) $group_event_id = reqvar("group_event_id");
	else if(sessvar_isset("showing_group_event_id")) $group_event_id = sessvar("showing_group_event_id");
	set_sessvar("showing_group_event_id", $group_event_id);

	$server_id = 0;
	if(reqvar("server_id")) $server_id = reqvar("server_id");
	else if(sessvar_isset("server_id")) $server_id = sessvar("server_id");
	set_sessvar("server_id",$server_id);

	$loc_id = 0;
	if(reqvar("loc_id")) $loc_id = reqvar("loc_id");
	else if(sessvar_isset("loc_id")) $loc_id = sessvar("loc_id");
	set_sessvar("loc_id", $loc_id);

	$dataname = "";
	if(reqvar("dn")) $dataname = reqvar("dn");
	else if(sessvar_isset("dataname")) $dataname = sessvar("dataname");
	set_sessvar("dataname", $dataname);
	
	$event_date = reqvar("event_date");
	$event_time = reqvar("event_time");
	
	$info_form = array();
	$info_form[] = array("Title", "title");
	$info_form[] = array("Duration", "duration");
	$info_form[] = array("Name", "cust_name");
	$info_form[] = array("Phone", "cust_phone");
	$info_form[] = array("Email", "cust_email");
	//$info_form[] = array("Customer Sales Rep","cust_rep");	

	if (isset($_POST['save'])) {
		$info_form_save = array();
		$info_form_save = $info_form;
		$info_form_save[] = array("","item_id"); 
		$info_form_save[] = array("","notes"); 
		$info_form_save[] = array("","cust_rep"); 
		if ($group_event_id == "NEW") {
			$info_form_save[] = array("", "event_date"); 
			$info_form_save[] = array("", "event_time");
			$info_form_save[] = array("", "server_id");
			$info_form_save[] = array("", "loc_id");
			$info_form_save[] = array("", "track_id");
		}
		
	  $vals = array();
		$save_fields = "";
		$save_values = "";
		$update_fields = "SET ";
		for ($c = 0; $c < count($info_form_save); $c++) {
	  	$iname = $info_form_save[$c][1];
	  	$vals[$iname] = $_POST[$iname];
			$save_fields .= "`$iname`";
			$save_values .= "'[$iname]'";
			$update_fields .= "`$iname` = '[$iname]'";
			if ($c < (count($info_form_save) - 1)) {
				$save_fields .= ", ";
				$save_values .= ", ";
				$update_fields .= ", ";
			}
		}
	
		if ($group_event_id == "NEW") {
			$create_group_event = lavu_query("INSERT INTO `lk_group_events` (".$save_fields.", `created`) VALUES (".$save_values.", now())", $vals);
			$group_event_id = lavu_insert_id();
			$new_group_event = true;
		} else {
			$update_group_event = lavu_query("UPDATE `lk_group_events` ".$update_fields." WHERE `id` = '".$group_event_id."'", $vals);
		}
	}
	
	if (reqvar("CorD")) {
		
		$CorD = reqvar("CorD");
		if ($CorD == 1) {
			$cancel_group_event = lavu_query("UPDATE `lk_group_events` SET `cancelled` = '1' WHERE `id` = '[1]'", $group_event_id);
			$lk_log_this = lavu_query("INSERT INTO `lk_action_log` (`action`, `time`, `user_id`, `loc_id`, `group_event_id`) VALUES ('Deleted group event', now(), '[1]', '[2]', '[3]')", $server_id, $loc_id, $group_event_id);
		} else if ($CorD == 2) {
			$delete_group_event = lavu_query("UPDATE `lk_group_events` SET `_deleted` = '1' WHERE `id` = '[1]'", $group_event_id);
			$lk_log_this = lavu_query("INSERT INTO `lk_action_log` (`action`, `time`, `user_id`, `loc_id`, `group_event_id`) VALUES ('Cancelled group event', now(), '[1]', '[2]', '[3]')", $server_id, $loc_id, $group_event_id);
		}
		$group_event_id = "NEW";
		set_sessvar("showing_group_event_id", $group_event_id);
	}

?>
	<body>
		<table width="440" border="0" cellspacing="0" cellpadding="0">
			<?php
				$get_group_event_info = lavu_query("SELECT * FROM `lk_group_events` WHERE `id` = '".$group_event_id."'");
				if ((mysqli_num_rows($get_group_event_info) > 0) || (($group_event_id == "NEW") && ($event_date != ""))) {
					if (mysqli_num_rows($get_group_event_info) > 0) {
						$extract = mysqli_fetch_array($get_group_event_info);
					}
					if ($group_event_id == "NEW") {
						$form_title = "New Group Event: ";
						$show_event_time = $event_time;
					} else {
						$form_title = "Group Event: ";
						$show_event_time = str_replace(":","",$extract['event_time']);
					}
					echo "<tr><td colspan='2' class='title' align='center' style='padding:10px 0px 10px 0px'>".$form_title;
					if(isset($event_date) && $event_date!="")
						echo display_date($event_date) . " " . display_time($show_event_time);

					$loc_query = lavu_query("select * from `locations` where `id`='[1]'",$loc_id);
					$loc_read = mysqli_fetch_assoc($loc_query);
					$location_menu = $loc_read['menu_id'];
					echo "</tr>
					<form name='info_form' action='group_event_info.php' method='POST'>
						<tr>
							<td width='127' height='46' valign='middle'>
								<table width='127' border='0' cellspacing='0' cellpadding='8'>
									<tr><td align='right' class='subtitles'>Event Type</td></tr>
								</table>
							</td>
							<td width='313' style='background:URL(images/tab_bg2.png); background-repeat:no-repeat;'>
								<table width='313' border='0' cellspacing='0' cellpadding='8'>
									<tr>
										<td class='nametext'>
											<select name='item_id' class='nametext' style='border:none; background: URL(win_overlay.png);'>";
					$get_group_event_items = lavu_query("SELECT * from `menu_items` where `menu_items`.`_deleted` = '0' AND `menu_items`.`hidden_value2` = 'group event' ORDER BY `menu_items`.`name` ASC");//`menu_items`.`id` as id, `menu_items`.`item_id` as `item_id`, `menu_items`.`name` as name FROM `menu_items` LEFT JOIN `menu_categories` ON `menu_items`.`category_id`=`menu_categories`.`id` LEFT JOIN `menu_groups` ON `menu_categories`.`group_id`=`menu_groups`.`id` WHERE `menu_groups`.`menu_id`='$location_menu' AND `menu_items`.`_deleted` = '0' AND `menu_items`.`hidden_value2` = 'group event' ORDER BY `menu_items`.`name` ASC");
					if (mysqli_num_rows($get_group_event_items) > 0) {
						while ($extract_gei = mysqli_fetch_array($get_group_event_items)) {
							$selected = "";
							if ($extract_gei['id'] == $extract['item_id']) {
								$selected = " selected";
							}
							echo "<option value='".$extract_gei['id']."'".$selected.">".$extract_gei['name']."</option>";
						}
					} else {
						echo "<option value='0'>Error loading group event items...</option>";
					}
					echo "</select>
										</td>
									</tr>
								</table>
							</td>
						</tr>";
				
					// get users to populate customer-rep dropdown
					$all_kart_users = sprintf("SELECT id, f_name, l_name FROM users");
					$sales_rep_query = sprintf("SELECT id, f_name, l_name FROM users WHERE id=%s",ConnectionHub::getConn('rest')->escapeString($server_id));

					$users_result = lavu_query($all_kart_users);
					$sales_rep_result = lavu_query($sales_rep_query);

					$rep_name = array();
					while ($row = mysqli_fetch_assoc($sales_rep_result)) {

						$rep_name[] = array($row['f_name'],$row['l_name']);
					}

					foreach ($info_form as $info) {
						/* leave input incase of requirement change
						if($info[1]=='cust_rep'){

							echo "<tr>
								<td width='127' height='46'>
									<table width='127' border='0' cellspacing='0' cellpadding='8'>
										<tr><td align='right' class='subtitles'>".$info[0]."</td></tr>
									</table>
								</td>
								<td width='313' style='background:URL(images/tab_bg2.png); background-repeat:no-repeat;'>
									<table width='313' border='0' cellspacing='0' cellpadding='8'>
										<tr><td class='nametext'><input type=\"text\" name=\"".$info[1]."\" class=\"nametext\" style=\"border:none; background: URL(win_overlay.png);\" size=\"30\" value=\"".(isset($extract[$info[1]])?$extract[$info[1]]:$rep_name[0][0].' '.$rep_name[0][1])."\" onchange=\"document.getElementById('update_btn').style.display = 'inline';\"></td></tr>
									</table>
								</td>
							</tr>";
						} else { */
						
							echo "<tr>
								<td width='127' height='46'>
									<table width='127' border='0' cellspacing='0' cellpadding='8'>
										<tr><td align='right' class='subtitles'>".$info[0]."</td></tr>
									</table>
								</td>
								<td width='313' style='background:URL(images/tab_bg2.png); background-repeat:no-repeat;'>
									<table width='313' border='0' cellspacing='0' cellpadding='8'>
										<tr><td class='nametext'><input type=\"text\" name=\"".$info[1]."\" class=\"nametext\" style=\"border:none; background: URL(win_overlay.png);\" size=\"30\" value=\"".(isset($extract[$info[1]])?$extract[$info[1]]:"")."\" onchange=\"document.getElementById('update_btn').style.display = 'inline';\"></td></tr>
									</table>
								</td>
							</tr>";
						//}
					} //end foreach
					
					echo "<tr>
						<td width='127' height='46'>
							<table width='127' border='0' cellspacing='0' cellpadding='8'>
								<tr><td align='right' class='subtitles'>Customer Sales Rep</td></tr>
							</table>
						</td>
							<td width='313' style='background:URL(images/tab_bg2.png); background-repeat:no-repeat;'>
								<table width='313' border='0' cellspacing='0' cellpadding='8'>								
								<tr><td class='nametext'><select name='cust_rep' class='nametext' style='border:none; background: URL(win_overlay.png);'><option value=''>-- Please Select --</option>";
								// iterate thru users for dropdown
								// select current user logged in if new else on edit select user that was previously selected
								while ($row = mysqli_fetch_assoc($users_result)) {
									if (($row['f_name'].' '.$row['l_name']) == $extract['cust_rep']) {
										echo "<option value='".$row['f_name'].' '.$row['l_name']."' selected='selected'>".$row['f_name'].' '.$row['l_name']. "</option>";
									} elseif ($row['id'] == $server_id) {
											echo "<option value='".$row['f_name'].' '.$row['l_name']."' selected='selected'>".$row['f_name'].' '.$row['l_name']. "</option>";
									} else {

										echo "<option value='".$row['f_name'].' '.$row['l_name']."'>".$row['f_name'].' '.$row['l_name']. "</option>";
									}

								}
								echo "</select></td></tr>
								</table>
							</td>
						</tr>";
								
					echo "<tr>
							<td width='127' height='150' valign='top'>
								<table width='127' border='0' cellspacing='0' cellpadding='8'>
									<tr><td align='right' class='subtitles'>Notes</td></tr>
								</table>
							</td>
							<td width='313' style='background:URL(images/tab_bg2_area.png); background-repeat:no-repeat;'>
								<table width='313' border='0' cellspacing='0' cellpadding='8'>
									<tr><td class='nametext'><textarea name=\"notes\" class=\"nametext\" style=\"border:none; background: URL(win_overlay.png);\" rows=\"7\" cols=\"30\" onchange=\"document.getElementById('update_btn').style.display = 'inline';\" />".(isset($extract['notes'])?$extract['notes']:"")."</textarea></td></tr>
								</table>
							</td>
						</tr>
						<tr><td>&nbsp;</td></tr>";
					$submit_button = "btn_update.png";
					$submit_display = " style='display:none'";
					if ($group_event_id == "NEW") {
						$submit_button = "btn_submit.png";
						$submit_display = "";
						echo "<input type='hidden' name='loc_id' value='".$loc_id."'>
						<input type='hidden' name='track_id' value='".sessvar("trackid")."'>
						";
					}
					if (!strstr($event_time, ":")) {
						$event_time = substr($event_time, 0, 2).":".substr($event_time, 2, 2);
					}
					echo "<tr>
							<td align='center' valign='middle' colspan='2'>
								<table cellspacing='0' cellpadding='0'>
									<tr id='update_btn'".$submit_display.">
										<td align='center' valign='middle' colspan='2'>
											<table cellspacing='0' cellpadding='0' width='430px'>
												<tr><td align='center'><img src='images/".$submit_button."' width='120px' onclick='document.info_form.submit();'></td></tr>
											</table>
										</td>
									</tr>
								</table>
							</td>
						</tr>
						<input type='hidden' name='event_date' value='".$event_date."'>
						<input type='hidden' name='event_time' value='".$event_time."'>
						<input type='hidden' name='server_id' value='".$server_id."'>
						<input type='hidden' name='save' value='1'>
						<input type='hidden' name='group_event_id' value='".$group_event_id."'>
						<input type='hidden' name='selected_row' value='".reqvar("selected_row")."'>
						<tr><td>&nbsp;</td></tr>";
					if ($group_event_id != "NEW") { ?>
						<tr><td align='center' colspan='2'><a href='_DO:cmd=confirm&title=Cancel Group Event&message=cancel this group event&c_name=group_event_info&js_cmd=cancel_or_delete("1","<?php echo $group_event_id; ?>","<?php echo $event_date; ?>","<?php echo reqvar("selected_row"); ?>")'><img src='images/btn_cancel.png' height='30px' border='0'></a> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <a href='_DO:cmd=confirm&title=Delete Group Event&message=delete this group event&c_name=group_event_info&js_cmd=cancel_or_delete("2","<?php echo $group_event_id; ?>","<?php echo $event_date; ?>","<?php echo reqvar("selected_row"); ?>")'><img src='images/btn_delete.png' height='30px' border='0'></a></td></tr>
						<tr><td>&nbsp;</td></tr>
					<?php }
					echo "</form>";
					
				} else {
					echo "<tr><td align='center'><font style='font-size:24px; color:#999999; font-family:Verdana,Arial'><br>No record selected...</font></td></tr>";
				}
			?>
			<tr><td>&nbsp;</td></tr>
		</table>
		<?php
			if (isset($_POST['save'])) {
				$date_parts = explode("-", $event_date);
				if ($new_group_event) {
					echo "<script language='javascript'>location = '_DO:cmd=alert&title=Group Event Created&c_name=group_events&f_name=lavukart/group_events.php?year=".$date_parts[0]."&month=".$date_parts[1]."&day=".$date_parts[2]."&selected_row=".reqvar("selected_row")."'</script>";
				} else {
					echo "<script language='javascript'>location = '_DO:cmd=alert&title=Update Successful&c_name=group_events&f_name=lavukart/group_events.php?year=".$date_parts[0]."&month=".$date_parts[1]."&day=".$date_parts[2]."&selected_row=".reqvar("selected_row")."';</script>";
				}
			}
			if (reqvar("CorD")) {
				$date_parts = explode("-", $event_date);
				$CorD = reqvar("CorD");
				if ($CorD == 1) {
					echo "<script language='javascript'>location = '_DO:cmd=alert&title=Group Event Cancelled&c_name=group_events&f_name=lavukart/group_events.php?year=".$date_parts[0]."&month=".$date_parts[1]."&day=".$date_parts[2]."&selected_row=".reqvar("selected_row")."'</script>";
				} else if ($CorD == 2) {
					echo "<script language='javascript'>location = '_DO:cmd=alert&title=Group Event Deleted&c_name=group_events&f_name=lavukart/group_events.php?year=".$date_parts[0]."&month=".$date_parts[1]."&day=".$date_parts[2]."&selected_row=".reqvar("selected_row")."';</script>";
				}
			}
		?>
</body>
</html>
